Okay, we will come back to our study of the Damabada.
Today, we continue on with verses 51 and 52, which read as follows.
Here, we will come back to our study of the Damabada.
Here, we will come back to our study of Damabada.
Here, we will come back to our study of Damabada.
Here, we will come back to our study of Damabada.
Without a perfume, we will come back to our study of Damabada.
We will come back to our study of Damabada.
We will come back to our study of Damabada.
We will come back to our study of Damabada.
We will come back to our study of Damabada.
We will come back to our study of Damabada.
We will come back to our study of Damabada and Damabada.
We will come to our study of Damabada or Damabada.
standard verse, the sort of thing you'd expect from the Buddha's teaching, which places
so much emphasis on practice, on sincere appreciation and realization of the teachings,
as opposed to just intellectual appreciation or understanding.
It's an interesting story that goes along with this verse, interesting in that it,
as with many of the stories, there's not much explanation as to how the story relates.
In this case, it's not even immediately clear why the story is told as it is in relation
to the verse, but I'll explain it, I think I can't see where it's all coming from,
it's quite interesting actually.
It's a short story.
The story goes, and a little bit of background to the story is that in the Buddha's time,
there was a tradition to take the eight precepts for the Uposetendes, which would have been
at the very least the Poiade, but it seems to have potentially been a weekly thing where
on the eighth day, and then the fifteenth day of the lunar cycle, so every lunar month
has 30 days, 29 or 30 days, and so the first, after eight days, the eighth day was the
half moon, and then the fifteenth day would be the, if I got to write the full moon, and
then the 22nd day, I'm going to write no, fifteenth, plus eight, twenty-third day would
be the half moon again, and then the twenty-nines or the thirtieth day would be the
emptying, basically, a little bit of fudging there, it turns out to be 30 days.
And so it was, it would be standard for Kalyana, Kalyana, the people who were endowed
with virtue and goodness, but not yet enlightened, it would be their habit to go to
the, go to Jetawana or wherever the Buddha was staying or wherever the monks were staying
and request the precepts, just as we do now in Sri Lanka, they do on the Poiade, except
they only tend to keep them until 5 p.m. It's not true, some places like Mitre Gala
or Mahameyana, I think they do the whole 24 hours. But the, the, the background to this
story is that for enlightened people, enlightened beings, Anagami is, Anagami's on
up, they, it seems they would keep the eight precepts naturally. I'm not sure that's
a big, ubiquitous, but it does seem, or whether that's true for, for every Anagami, but
it seems to be according to the commentary, true that, and it may be in the, the particular
somewhere that Anagami always keeps at least the precepts. I think there's even some idea
being floated around there that they keep 10 precepts. You know, you have the story of
Takara who looked after his mother, but he didn't use money, it seems, he may not have
his money. Anyway, the, the point is they have natural morality, so they don't go to, they
don't go and request the precepts, they're always keeping them. And there was one
lay disciple of the Buddha who was Anagami, he was in this category, but he would still go
every, every, of course at a day to hear the Buddha's teaching. And here's where we
get to the background story. He went to the Buddha's, he went to hear the Buddha's
teaching and he paid respect to the Buddha and then he sat down to one side and then King
Pazainadi came along and he thought to himself, what do I do? Do I stand up for King
Pazainadi and, and, and, and pay respect to him if I don't, he'll get angry, but if I
do, then I'll be disrespecting the Buddha because it, it was apparently a custom, when
you're in the presence of a higher person, you, you would never, never stand up to salute
a lower person. And because he held the Buddha as higher than the Padaisaraj, the Buddha
was the Rajaraj, the Agaraj, the highest King, the King of the Dhamma and Pazainadi was just
a Padaisaraj, which is the King of the country. So because he held the Buddha higher,
he thought, well, I'll have to just sit still, even though I, I know Pazainadi's going to
be an angry because King Pazainadi wasn't, it wasn't enlightened by any means. And Pazainadi
saw this and saw his, one of his, his, when do you call people under the King, subjects,
one of his subjects, why were English as terrible? One of his subjects failing to stand
up and salute him. And he got very angry. And the Buddha, of course, and understanding
what was going on in the Pazainadi's mind, he extolled the virtues of this, this
late disciple. Oh, how great he is, how he, this is, this is my late disciple. He's a very,
he's very wise and humble and so on. Patient doesn't get angry easily. And Pazainadi
was appeased for the time being, but then later on, Pazainadi saw this late disciple
walking near the palace. And so he had his, his guards call him in. And he was walking
with his sunshade and sandals on. Immediately he took down his sunshade and took up his sandals
and walked and paid respect to the King instead to do one side. And the King gets all
sarcastic and says, oh, so I see you, that was nice of you. Why did, why did you pay,
why did you take up your scent, take down the sunshade and take up your sandals and pay
respect to me? Because you're the King. And he said, oh, so today you, you realize that
did you, you just realized that today, did you, that I'm the King. This guy's an honor
gummy. So there's nothing's going to phase in me. It's not like, oh, I'm sorry. He says,
no, I always knew that you were the King. I said, what do you mean? I saw you, you didn't
even stand up to pay respect to me when you were sitting with the Buddha. And so I explained
in, when like, adult would explain to a child, patiently, words that he could understand.
I was sitting with the Agaraj, you're just, I can couldn't stand up and pay respect to
the Badesaraj, the King of the country when I was sitting with the highest King. And
but saying that anyway, there's a point to this story. It's kind of weird how this
all fits in here because it doesn't have anything to do with the verse. But the point being
that he then tries to use this, this, to shame this layman into teaching his queens because
he has this idea that Buddha had said that, oh, this is, he's a very wise, wise meditator
and wise teacher. So he says, all right, fine. Well, to make it up for me, you can, I want
you to teach Malika and Wasabakatya as a name. Wasabakatya, which was this, the daughter of
Mahānāma, who got us in trouble in the last verse with Vitudaba, we, we, whatever his name
was. Wasabakatya. We, you know, there's actually different ways to say his name. We do
that. Different spellings. But yeah, we do that. We do that, though, as the son of Wasar
son, the whole thing, the same idea, if you remember, the vicinity, married Malika and
Malika. And then he, and then he, what was this was one of our recent verses. He, the
monks wouldn't stay and eat his food and he thought, well, if I marry one of the sakias,
then they'll respect me. I don't think I'm related to the Buddha. So they'll come and
stay with me, which of course was a silly idea. But he did and he demanded a wife and
they sent him the daughter of a slave who had Mahānāma had fooled around as a slave
and had a daughter and she was quite beautiful. And so they sent her along and it caused
big problems when they found out and presented his son, killed all the sakias as a result.
So that's who was the Bhakakthianas. So he said, he wanted to teach his two queens. There
is two queens wanted to learn the dhamma. They were both quite interested and so he needed
a teacher and he said, you can come and teach me, teach them. And this layman, he was,
he was adamant. He said, no, I can't do that. And he said, what do you mean you can't
do that? I'm the king. And he said, look, if I knew that, it's very dangerous for layman
to be in the queen, in the king's haram. Any rumor that goes around could get him in big
trouble. It's not appropriate for me to do it. I shouldn't be doing it when it's the
work of a monk. So the sanity argued with him and finally said, fine, then you may go
and then he went to the Buddha and asked the Buddha to send a monk. The Buddha sent Anand
Anandana ended up being the teacher of Malika and wasbakatya. So that's the the background
story. Anandana tries to teach the story that leads up to the verses Anandana while he is teaching
this, teaching the dhamma to these two queens and of course all their attendants. He finds that
Malika is actually quite quick. She's she's very quick to memorize the teaching, quick to
she's very good at retaining it, keeping it in mind, pondering it. And moreover, she seems inclined
to put it into practice. Whereas wasbakatya doesn't remember anything, doesn't pay attention.
She's like one of those people, do they say that keep it in their lap?
When they're sitting there, they can remember it, but as soon as they stand up, it falls under
their lap. It's like they keep the teachings in their lap, they say.
Sort of the implication seems to be that she wasn't paying attention. She wasn't really
sincerely interested, which is the case you can't expect everyone to.
Not everyone responds to the dhamma the same response or appreciates the teaching the same way.
Much of the teaching is uninteresting on the sensual level. It doesn't inspire excitement in the
mind. It doesn't, some of it is dry and dull and requires great patience and depth of mind to
appreciate it. I would say actually the teaching overall is quite inspiring, but you have to be
interested in meditation. You have to be able to free your mind from the sort of things that
queens are not free from. Queens will doft and you would assume because they just have to sit
around and look pretty in those times. It's quite a horrible sort of life to imagine
sitting around looking pretty all day. That's your job, like a flower. Hence the verse,
you know, when the Buddha comes out with the, he likens these two queens to flowers.
But the point would be that it would be very easy to get caught up in the vanity of it
and the attachment to sensual pleasures and with sensual desire, beauty and scent and so on.
And so the Buddha, so the Ananda came back every day from King Saram to having taught these
two queens and the Buddha asked them, how's it going with it, with the two kwith,
Malika and Wasamakatya? Are they learning the teaching? Are they putting it into practice? Are they
are they remembering it? Are they, are they respectful, suck atchas, the word suck atchas kind of
respectful, attentive? Are they putting a sincere? Are they being sincere about it?
And Ananda said, well, Malika is very sincere about it and she's learning it and keeping
in mind and seems to be putting it into practice. But Wasamakatya, but you're relative,
the daughter of your relative, not nati, nita or something like that. The daughter of your
relative, you're the saki and Wasamakatya is, she's not picking it up. And the Buddha
then the Buddha gives the verse he says, yes, well this is the way it is, not all flowers have
beautiful scent and in the same way. Not everyone puts the teachings into practice or
or he you could think of it as kind of an admonishment. But the interesting thing here is,
if you look at the background it was at first I couldn't see how this all fit together for the
verse because it's not quite, it's not quite clear that there was anything to do with practice
Ananda was going to teach them the teaching but it doesn't say anything about them putting it into
practice. And it doesn't certainly doesn't say anything about them speaking. But there's another
interesting way of looking at this story. You have actually, you have another two people, right?
You're talking about the two queens as being flowers or you could say speech as being like a flower.
And so it's kind of an admonishment for Ananda to give to the two queens
not to just be content with hearing the dhamma, not to just be content with learning the dhamma
but should actually put it into practice. It's useless to them, apala hotti. It's useless,
good speech is useless if you don't put it into practice. But there's another two people and
that's the layman and Ananda. And why you can see this is because the beginning story talks
about how, for an enlightened being, Ananda got me, they don't even have to hear that, they don't
have to say I'm keeping the precepts for example which is what the commentary says, it says
they just keep it naturally, they just practice it, practice the teachings naturally.
So he, he, and he wasn't teaching in, he wasn't, this layman was, was,
he was not reciting it and who knows maybe he didn't have so much great knowledge,
and he wasn't a monk. But here he was practicing it quite well, practicing it almost to perfection.
And then we have Ananda and you see this occurring more than just in this instance where the Buddha
will give these subtle poaks to Ananda, because Ananda was, was brilliant right, he was at the top of
the class, photographic memory. And yet here he was sitting at the bottom of the totem pole,
it was just a soda pun, which is nothing to shake a stick, more than you can shake a stick at it.
A soda pun is something quite special, it's a great, great thing. And he praised Ananda on
many occasions for his learning and for his wisdom. But it seems to me that a better way of
looking at this story is the Buddha taking a chance to remind Ananda and to use, even use Ananda
as an example in this case. Anyway, the verse stands on its own, the two verses stand very much
on their own and as sort of a epitome of much of what the Buddha reminded his students,
and which we try to remind Buddhists and remind ourselves in our own practice,
that it's not good enough for us to have an intellectual appreciation of the teachings.
This isn't what the Buddha would want for us. And these kind of verses are in this way
good to keep in mind and recall to mind, they have a great power to kick you in the button,
set you in motion, set us on the path. We realize that appreciation of the teachings isn't enough.
Even teaching the teachings is certainly not enough. It's being able to recite the teachings,
so if you can tell me what the four noble truths are, let us know what the Buddha wanted for us.
We see this very much in cultural Buddhism, where we have Buddhist societies and it's not
just in Asia but also here in North America, where you become Buddhist and somehow that takes
on a meaning for you, just being Buddhist somehow takes on a meaning for you as opposed to practicing
Buddhism. I've talked about this before, how it doesn't seem right to call yourself a Buddhism
for any reason, call yourself a Buddhist for any reason other than the fact that you're practicing
the Buddhist teaching. Just like you couldn't call yourself a Marxist unless you were
acting according to the teachings of Karl Marx. I've seen this, I've lived in Thailand,
I've been involved with many, in Sri Lanka, with many different cultural Buddhist communities
even here in the West. And you'll see this on all levels of the Buddha's teaching,
morality for example, once I went to a dinner, a lunch lunch. It's at some late people's house,
I won't say which community, which cultural group it was. They invited the monks to their house
and so we sat down and I was invited to give the five precepts because they always like to see
that Canadian monk gives the precepts. So I gave them the five precepts, everyone took the precepts
and then they fed us. We ate and right after we ate, we were sitting there and they took all
our food away and then they brought up food where everyone else. And I sat there and we were just
kind of winding up and getting ready to leave and then I saw the coolers of beer come out
and they started passing around bottles of beer, too. And I thought it'd been about a half an hour,
half an hour after I gave them the five precepts. This is an extreme example of
practice they're useless, they're just like a flower that has no smell. So taking the five precepts,
taking the eight precepts is not the important thing. So this we have in that we have, this is what
I scolded everyone about always that. That you can't just take the precepts without having
any of any intention of keeping them. We call them bojana and you have to staple the whole
we call that. Otherwise it's not really keeping it.
In the same goes for concentration, there are for meditation. It's easy to have an understanding
of meditation and to know the technique.
It's my very easy to teach meditation, teaching meditation and practicing meditation
or two very different skills. There was a meditation teacher in our monastery who was still smoking
cigarettes. And I thought that was quite odd on teaching
objectivity. But you know this is the case, it's two very different skills. Teaching meditation
is difficult, but it's difficult in many ways it's difficult in a different way from actually
practicing. So you see this often, people after they finish meditation course, they run back,
rush back home to try to teach other friends meditation and wind up spending more time trying
to convince others to meditate rather than doing the hard work of trying to cultivate their own
meditation practice. I think everyone in this room knows about Anapanasati about practicing
before the 4th Sati Bhatana, most of the people in this room could rattle off the 4th Sati
Bhatana for me. But it's quite a different thing from actually being in the practice. If you look
through all of the Buddha's teaching, so much of it is just about how to sit in meditation,
how to be objective, how to be here and now, the 5 Khandas, the 5 aggregates. It does
nothing to know what they are, a rope of it and a sannyas and kara nyan until you actually look at them.
But I think it's more, I think most pronounced is this
concept of knowing the teaching but not putting into practice is under the wisdom,
the heading of wisdom. Quite often, we'll see Buddhists talking the talk, but not walking the
walk, the very erudite scholars teaching Buddhism in university giving dhamataks that are just top-notch,
but not practicing themselves. They've given stories about this before, there's the story of
Duchapotila, how he memorized the whole tepitika. So he knew everything, and yet he
memorized, they'll call them empty book. And this monk who was a teacher and all of his students
became enlightened, I've told you that one before. And then finally he ran off into the forest and
started meditating and he was such a good teacher that when he started crying, the angels started
crying. And he said, why are you crying? And the angel said, well, you're such a good teacher,
I thought if you're crying, it must be the way to become enlightened. So I cried along with him.
But he was such a good teacher. He knew everything. So this is a real danger in Buddhism
because it can be so easily misconstrued as a philosophical teaching or an intellectual teaching.
And people can very easily get caught up in learning the Buddha's teaching, studying the Buddha's
teaching. And lots of crazy things happen when you just focus on the study, I've seen
so this doesn't just go with the large populations of Buddhists in Asia. But this man from England,
he said he'd read the tepitika twice. He told me he'd read the tepitika twice in Pali,
read the tepitika in Pali twice. And he had some weird ideas. He had this idea that becoming enlightened
was becoming enlightened by hearing the Buddha's voice. It's by hearing the Buddha's voice that you
could imagine. Having read the tepitika twice, he had this view that there was some magic quality
to the Buddha's voice that enlightened people. And that's what the Buddha, that's what is Buddhism.
Scary stuff. He also thought that just as a side note we had an argument, not an argument,
it was a friendly argument. He's actually well-known Buddha's scholar and I met him in Bangkok,
whom he spent an evening together. Remember, who is it? Mahakasapat. He's going on arms round,
and the leper drops a finger in his bowl. There's a leper and his finger falls. He's giving
food to Mahakasapat and his finger falls off, into the bowl of food.
And how did the conversation start? He said, we're talking about mediating, I think.
And he said, well, you know, our hands. We were basically saying that vegetarianism is not.
There's not certainly not spelled out. Obviously, the Buddha didn't require vegetarianism
from his students. That was David Datta. And he said, yeah, I mean, Mahakasapat,
or I don't know, I think it was when we were talking about how hard it would be to be a monk.
And he said, I don't think I can ever be a monk. You know, what you have to go through,
like he said, like Mahakasapat, when he had to eat the leper's finger.
And I said, what? I don't wait a minute. This isn't the story I remember.
And he said, yeah, remember the finger fell off and it says that he picked it up and ate it.
And I said, no, I don't think that's what it says. I think it says he took the finger out and
ate the food. And so we weren't upset. It wasn't an argument or anything. It was just a debate
where like, he was like, oh, really? And he said, oh, so I said, wait a minute, let's go back
and we'll look at the text. And I argued, I said, well, you know, because there's a rule
against eating human flesh for one thing. Monk cannot eat human flesh. It's against the rules.
And so we went back and looked at the text and it actually doesn't say.
It says the finger fell in. He took it out. He wasn't, the big thing was he wasn't moved.
He wasn't turned off by it. He just took it out and ate it.
But it's quite clear, you know, it's quite clearly could be the case that and should be the case
that he took the finger out and ate his food. You know, a finger went into his food. He took the
finger out and ate it. That's what it says. So it's actually could be construed either way,
which is quite funny. Anyway, a little bit off topic, but an interesting story. He was a nice guy,
but certainly didn't have any more talking, but I was talking about meditation. I said,
if you were tried and he said, no, I tried to do it and he's from England and he said,
I tried and I just, I can't sit still. Which of course is the reason why you take up meditation
because you can't sit still. You know, to learn. But this is a, this is quite problem,
this is a big problem in Sri Lanka. I'd have to say, and I think everyone, we all kind of agree,
except this is that there's a lot of study going on there. Sri Lanka is the number one,
I think maybe Burma, I'm sure Burma must compete, but from what I've seen Sri Lankans are very
versed and well versed in the Dharma. It's quite, it's incredible. I was talking to a bus driver about
the Dharma. I remember coming back from candy and I was sitting in the front seat and he turned to
me and we started talking and I was just asking about the Dharma and he was giving me answers and
he knows things that I don't know what the Dharma was quite surprising. And that wasn't a nice
related case. People know the Dharma there, but there's not so much meditation. There is,
but not to the level that you think. You'd think that in a country where 75% of the people
maybe 70% or 5% of the 75% of the people, or 50% of the population knows the Dharma very well,
that there'd be a lot more meditation. But again, it's a different skill set. It's one thing
to appreciate the Dharma and it's a dangerous thing actually to get intoxicated by knowledge.
You know what are the four noble truths? And it seems like a simple thing. Four noble truths,
everyone knows what those are. Every Buddhist in Sunday school. Those are the four noble truths
there. No? I did. What are the four noble truths? Unlike. I'm going to show your brother up next
seat. Four noble truths. You don't know the four noble truths. That's okay because we're not so
concerned about that. We're concerned much more about meditation practice.
But if I asked any of the adults in the room, everyone would be able to tell me what are the
four noble truths. But that doesn't mean you understand the four noble truths. Actually,
just those four noble truths is the core Buddhism. If you can understand those four noble truths,
you never have to learn anything else for the rest of your some sariq existence,
which would be very short in fact. It would be very short anyway because you'd be an arahat.
Understanding the four noble truths is the key to becoming enlightened. And there's just four of
them. Quite simple. You better learn them. It's not to say that you shouldn't learn or memorize
the Buddhist teaching. But you should put them into practice. For example, the first noble truth
is the truth of suffering. So we know that what Jhaati Bhiduka, birth is suffering, all they just
suffering, sickness is suffering, death is suffering, getting what you don't want is suffering,
not getting what you want is suffering. Is it when you get what you want? Is that suffering?
When you don't get what you want, is that suffering? When you get what you want, is that suffering?
When you get what you don't want, is that suffering? Like a knuckle sandwich,
from a monk, I would never do it. You know that. When you get what you don't want, is that suffering?
What's your least favorite food? Do you like liver? What if you got liver would that be suffering?
Yeah, you see. Not getting what you don't want. We know this. But this isn't the noble truth
of suffering. The noble truth of suffering is the understanding that all things are unsatisfying,
the Bhantupada and the Kanda, the five aggregates. It's understanding that the five aggregates
are impermanence of suffering and oneself. So it's basically the practice of Yipasana meditation,
or the realization that comes from the practice of Yipasana meditation.
And this sort of gets closer to the point is that often people will practice meditation
looking for or thinking about analyzing everything based on the three characteristics,
as opposed to just experiencing them. So it's one thing to sit here and think, oh, that's impermanent.
Yes, that sees that means it's impermanent and so on. And to say yes, yes, therefore it's unsatisfying
and it doesn't belong to me. It's quite another thing to sit here and be subject to impermanence.
Not know what's coming next, which is horrifying. And that's what creates all of our suffering. You
don't know what's going to come next. Pain, maybe that pain will come back, that pain that was just
there. Maybe it'll suddenly get hot. Suddenly get cold. So just a second ago it was enjoyable,
maybe I told a joke and it was kind of funny. Now that joke is gone, things have changed.
It's a incipient and dull. When can I leave? When can we go home?
This is where the meditation starts with this stuff, dealing with impermanence,
dealing with the difficulty of reality.
And this is where the learning starts in changing our understanding of reality.
Understanding that reality is stable, satisfying, controllable.
And that we can come to a state where we're always going to be at peace and happy.
That we can find something and find a way to live our lives in stability.
Most adults in the world have a big problem with this. Children don't have such a big problem
trying to find stability because they're trying to learn a lot. They're quite unstable.
But once you become an adult, you really start to look for stability,
try to find a good job, a family, and how to settle down, find something stable.
And so adults get devastated by change and this is where depression comes from and
stress, anxiety, and so on, worrying about the things that we love, things that we cling to.
So the only thing I want to stress here is that it's important to put the teachings into
practice. It's important to differentiate between an intellectual appreciation and ability to
rattle off and even explain and talk about the four noble truths, the three characteristics,
the eightfold noble path, and to actually put it into practice.
And most importantly, I would say the three characteristics because they're the most poorly
understood and the most practical. They're the front line. They're what you first experience when
you sit down and when you watch your stomach, for example, you're sitting here watching the stomach
rise and fall. Wondering where the three characteristics come in, wondering where wisdom comes in.
And watching your stomach doesn't seem like a good way to bring about profound realization of the
truth. It's kind of like if you ever saw the karate kid, you probably didn't. The original
karate kid. You saw the original with the Italian kid? Where he does this? What was this one called?
Up, down again, what would he call this one? Wax on, wax off, wax off. And he thinks it's so
stupid. What am I going to get out of this? This is very similar to the mind state of the
meditator in the nature of meditation. The wisdom that comes from meditation is something very
mundane or very ordinary. It's ordinary that you would almost miss it. Or it's so inherent
or so close to us that we miss it. But it's what sets us off all the time. When the things that we
think of as stable change, someone you love changes or disappears, runs away, dies, get sick,
something you love breaks, malfunctions is stolen. You've ever had something stolen from you. It's
a horrible feeling. I just feel like you're violated, like someone violated your body weight. This
was part of me having things change. This place, this wonderful monastery, it would burn down,
whatever, burn, burn down tomorrow. Then what would we do?
You never know what's going to happen in the future. So the meditation is not about thinking
about these things. It's about viscerally realizing this. But absolutely everything that we cling to,
even our pleasures, our identities, who we are, who we think we are, change.
To see that the things that we find pleasure and brother are actually a cause for stress and
suffering. They force us to seek, they push us, they compel us to seek, we become drug addicts,
we've been addicted to our desires. And to see that the things that we try to control, the things
that we identify with are actually out of our control. People, we try to control our children,
we try to control our loved ones, our friends. We try to control our lives, we try to control our
our machines. We're seeing someone yelling at their car, kicking the car, stupid.
Let's see if someone hit their computer because it's not working.
A punch a hole in the computer screen because it's not working. When you throw your game boy down,
when you get a game boy and you lose and you throw it down,
we want to control, we want to be in charge and we're not able to deal with not being in control
when things are not in our control. It's very difficult. So just watching your stomach is a
very good lesson in these three things. When you see that it's changing all the changing constantly,
never the same. Sometimes it'll be smooth, sometimes it'll be tense, sometimes it'll be long,
sometimes it's short, not under your control, not satisfying, not stable. Just the stomach,
you can become enlightened. It's like a pill. My teacher said it's like a medicinal pill. It's
got everything in it. You just take one pill, like the matrix, take the red pill.
It's just one pill. It's very simple. We have many more, obviously the four foundations of
mindfulness. You should be mindful of pain and happiness and thoughts and emotions as well.
But any one of these things, most especially the body and for that reason, the stomach,
have an incredible power to bring about enlightenment. They help you to see things that are so
obvious that you almost miss them. You think there's something missing. But if you practice
continuously watch your mind, watch your experience, you come to learn such profound things about
yourself. Anyway, this is what is meant by the second verse, kubato, one who puts into practice
the words. So the important point of this verse is that we should never be content with just
knowing the Buddhist teaching. Any of it, none of it is for the purpose of knowing and
and sharing with others. It's for the purpose of bringing home to yourself. If you want to spread
Buddhism, first you have to spread it to yourself. There's a teacher in Thailand who said this to me
talking about spreading Buddhism is the first you have to spread it to yourself. It's very apt.
And it's very much how the Buddha would have it. He was never concerned about
making teachers. He was about teaching people. You all be students. The most important is to be
to be a student, to learn teachings. Because someone who has learned the teachings is naturally
a teacher. It's an example and is a signpost. And it's so in line with the teachings that they
give rise to. Subhasita wa tsubasita wa chata, all the time. Proper speech, good speech,
they give rise to the teachings. They declare the teachings in its purity because they practice for
themselves. So that's the meaning of this verse. And obviously a practical teaching of the Buddha
is something that we can all keep in mind, something that we should constantly remind ourselves.
It's not enough to know the meditation. The question is, are we putting it to practice on a daily
basis? This is much more important. The Buddha said a person who knows the teaching, but never puts
it into practice. This is not someone who has lived the holy life. It's still a waste of their holy
life. But a person who even for a moment cultivates loving kindness or mindfulness. Just for one
instant, he says their spiritual life is not wasted. So it's not to discourage everyone or I'm not
here to scold everyone or you're not practicing. It's to encourage us in putting into practice. The
teachings moment by moment by moment. Even one moment that's a Mahakusla. They say one moment of
mindfulness kills off seven lifetimes worth of defilement because of the seven jawana.
If you're mindful those seven jawana are neutralized and those seven jawana could give rise to seven
births despite one moment of mindfulness. And it's a cumulative power or the wholesomeness of
the mindfulness. It's just something that we shouldn't think of doing when we go to a monasteries
or even just in the morning or in the evening when we do formal meditation, we should be
constantly striving to cultivate mindfulness throughout our lives throughout our days.
This is what the Buddha would have of us. This is the emphasis here. Clearly stated here and then
many, many other places in some of the other Dhamapana verses as well. We don't want to just be a
beautiful flower being able to spout beautiful words. If they're not put into practice, they're
meaningless. And this is the point. You can take the example of the two queens and with the one
queen, he was wasting his breath. They were beautiful words. He was teaching and these are beautiful
words that just float in the air like a beautiful flower. But the Buddha said they're useless
like flower without because they're useless because they're not being put into practice. Whereas
with the other queen, because she was putting in them into practice, they were like a beautiful
flower with a beautiful smell, a beautiful scent. It means there was actually something that came
of the flower. There was something that came of the teachings, some fruit that came from it.
There's another part of it that for a teacher, the good, the good doesn't come from the teaching,
the good comes from the practicing. So I'd like to thank everyone for coming tonight. We have a
large crowd here and everyone for tuning in on YouTube. And please take this as an admonishment for
all of us not to be content with just learning the teachings, not to be content with just coming and
hearing the teachings, but that we should put these teachings into practice and give purpose,
bring purpose and meaning to our lives, give purpose and meaning to the Buddha's teachings,
the purpose and the meaning of the Buddha's teachings isn't in the words. It's beautiful as they
might be. It's in our practice of the Buddha's teaching and the Buddha's words, which we should
strive to do, strive to cultivate for ourselves, not just intellectually, but practically
morality concentration of wisdom. Thank you for tuning in and wishing you all the best on your path.
