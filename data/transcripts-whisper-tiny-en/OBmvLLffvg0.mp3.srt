1
00:00:00,000 --> 00:00:02,820
Welcome back to Ask a Monk.

2
00:00:02,820 --> 00:00:09,500
This is, I think, a short, hopefully a short video talking about the, it's an old question

3
00:00:09,500 --> 00:00:18,060
that I've just been putting off about the, the working of karma in regards to a holocaust.

4
00:00:18,060 --> 00:00:25,860
How does karma work when you're talking about mass suffering?

5
00:00:25,860 --> 00:00:29,140
Mass realizations of, of a certain state.

6
00:00:29,140 --> 00:00:34,260
I think actually a holocaust is a bad example, but it does highlight some of part of the

7
00:00:34,260 --> 00:00:35,260
answer.

8
00:00:35,260 --> 00:00:39,980
So I'll take it in as a part of it, but let's consider several, in several examples.

9
00:00:39,980 --> 00:00:50,460
The holocaust at tsunami and the planes crashing into the world trade center.

10
00:00:50,460 --> 00:00:52,660
Let's examine all three of these.

11
00:00:52,660 --> 00:00:57,380
The holocaust is actually the easiest one to see because the answer in all three cases

12
00:00:57,380 --> 00:01:02,220
that not no one dies in the same way, no one experiences things in the same way or has

13
00:01:02,220 --> 00:01:04,220
the same sort of suffering.

14
00:01:04,220 --> 00:01:08,580
This is really easy to see in a holocaust because even though so many people were killed

15
00:01:08,580 --> 00:01:12,940
in similar ways, none of them died in the same way and none of them experienced it in the

16
00:01:12,940 --> 00:01:13,940
same way.

17
00:01:13,940 --> 00:01:17,300
Some of them might have been calm, some of them would have been very frightened, some

18
00:01:17,300 --> 00:01:22,860
of them would have been enraged and so on.

19
00:01:22,860 --> 00:01:27,540
The death of each person is totally dependent on their mind state.

20
00:01:27,540 --> 00:01:31,940
So to say that all of these people died in the same way and therefore it's hard to believe

21
00:01:31,940 --> 00:01:39,260
that it was all based on their karma is really a superficial examination.

22
00:01:39,260 --> 00:01:44,540
Tsunami is another good example because you think all of these people drown, well that's

23
00:01:44,540 --> 00:01:50,620
not karma, that's an act of nature, but it really is, it's hard to say one way or the

24
00:01:50,620 --> 00:01:58,380
other because the people died based on their individual situations and though everyone,

25
00:01:58,380 --> 00:02:03,580
it's true that many people drowned, each individual's drowning was quite different.

26
00:02:03,580 --> 00:02:13,100
So their experience of the affair or the event was would be quite different as well.

27
00:02:13,100 --> 00:02:18,340
Also their experience and their response to it would have been quite different.

28
00:02:18,340 --> 00:02:24,940
They said some people will be quite disturbed by it, some people will be quite calm or reasonably

29
00:02:24,940 --> 00:02:30,140
calm, some people will die instantly, some people would take a long time to die and so

30
00:02:30,140 --> 00:02:31,140
on.

31
00:02:31,140 --> 00:02:35,580
It totally depends, I think it mainly depends on the person's karma and you may say there

32
00:02:35,580 --> 00:02:42,380
are other, you know in fact the word karma, it's first of all as I've said the Buddha

33
00:02:42,380 --> 00:02:46,820
didn't talk about karma, he talked about the mind and the intentions that we have in

34
00:02:46,820 --> 00:02:51,780
the mind and then he said only a Buddha could possibly understand karma, it's one of those

35
00:02:51,780 --> 00:02:55,060
things that you should never think about the workings of karma, what caused this, what

36
00:02:55,060 --> 00:02:59,860
caused that, because all it means is that things go by cause and effect and the mind is

37
00:02:59,860 --> 00:03:05,460
casually affecting, that's all it means, the mind is not an epiphenomenon, it's not something

38
00:03:05,460 --> 00:03:13,980
that is created by the brain, it's an active participant in reality, but what causes lead

39
00:03:13,980 --> 00:03:19,580
to what results is so intricately intertwined that you can never really break it apart,

40
00:03:19,580 --> 00:03:25,100
so you can say what was the karma that caused those people to be brought to the tsunami,

41
00:03:25,100 --> 00:03:33,380
well you're talking about a whole confluence I think is the word of different causes that

42
00:03:33,380 --> 00:03:38,740
bring everyone together just in that one instant and it's just a fluctuation of the

43
00:03:38,740 --> 00:03:43,140
universe, it's scientific, science would explain this tsunami and all the people dying

44
00:03:43,140 --> 00:03:47,740
in terms of atoms coming together and just came to get happened to come together in that

45
00:03:47,740 --> 00:03:55,220
way, karma simply explains the part that the mind played and all that to bring people

46
00:03:55,220 --> 00:04:00,580
together into this one place and then to separate and again as I say it's not really that

47
00:04:00,580 --> 00:04:05,300
they were all in one place or one experience because each person's experience would be

48
00:04:05,300 --> 00:04:12,060
indeed quite different, the third example is in a plane crash which is the hardest to see

49
00:04:12,060 --> 00:04:18,060
I think, but even there you still have differences of experience and more importantly

50
00:04:18,060 --> 00:04:25,860
differences of reaction to the experience and therefore different results and you could

51
00:04:25,860 --> 00:04:37,900
say different karma that led to it, but there is an element of group and mutual karma

52
00:04:37,900 --> 00:04:43,460
and it has to do with how we are intertwined with each other, why we are born to the parents

53
00:04:43,460 --> 00:04:47,860
that we are, why we live in the same house with certain people, why we work with certain

54
00:04:47,860 --> 00:04:54,260
people are coming together and why wouldn't we equally as isn't it strange that we should

55
00:04:54,260 --> 00:04:58,540
all be working in the same job or isn't it strange that we should all be born into the

56
00:04:58,540 --> 00:05:04,620
same family, it's all the same, it all has to do with our path and we meet up and we

57
00:05:04,620 --> 00:05:13,620
experience certain events, death being one of them, sometimes tragic and simultaneous death,

58
00:05:13,620 --> 00:05:18,860
but it all depends on so many different factors that bring you together and of course

59
00:05:18,860 --> 00:05:26,980
it has to do with the nature of the physical body which isn't able to bring or prevents

60
00:05:26,980 --> 00:05:32,500
the mind from bringing about results in spontaneous, if we are all mental you would receive

61
00:05:32,500 --> 00:05:37,980
the karmic results much quicker for instance angels and ghosts and so on, they receive

62
00:05:37,980 --> 00:05:45,620
karmic results much quicker when an angel gets angry, I think they fall, I think it

63
00:05:45,620 --> 00:05:50,460
angry enough they die simply by being angry, now it takes a lot of anger for a human

64
00:05:50,460 --> 00:05:55,100
being to die, but for an angel it's not that much because it changes their mind and

65
00:05:55,100 --> 00:06:03,220
because they're mostly mental with a very limited physical, simply a little amount of anger

66
00:06:03,220 --> 00:06:09,300
can cause them to change their state because the mind is very quick to change, but the

67
00:06:09,300 --> 00:06:15,900
body is not, because the body is so coarse and as I said before has such inertia, you

68
00:06:15,900 --> 00:06:22,380
can take a long time for it to catch up with us and as a result it can all build up,

69
00:06:22,380 --> 00:06:26,820
can build up until the body has a chance in the case of a plane crash and all this build

70
00:06:26,820 --> 00:06:31,300
up of karma and so on that brings people together and pushes them all into the same

71
00:06:31,300 --> 00:06:36,100
position and then suddenly snaps, this is the way the physical works because of the build

72
00:06:36,100 --> 00:06:44,700
up of power and the capacity it has to collect static energy, though that's how the physical

73
00:06:44,700 --> 00:06:51,140
works, the physical has so much static energy and this is how nuclear atomic bombs are created

74
00:06:51,140 --> 00:06:57,620
because there's so much energy and matter and this is all just a part of it, how matter

75
00:06:57,620 --> 00:07:03,980
works in ways that somehow mask the workings of the mind because of simply because of inertia

76
00:07:03,980 --> 00:07:09,660
and static energy and so that's really all that it shouldn't be a great mystery as to

77
00:07:09,660 --> 00:07:17,860
why people all die together and it shouldn't be certainly a disprove of the workings

78
00:07:17,860 --> 00:07:21,980
of karma because karma is such a Buddhist, the concept of karma is such an intricate

79
00:07:21,980 --> 00:07:27,660
thing, all you can say is that when you do good things, your life changes and your path

80
00:07:27,660 --> 00:07:34,340
changes in a good way, when you do bad things, it changes in a bad way, you know what exactly

81
00:07:34,340 --> 00:07:43,060
that will be and the scale and the nature is very, very, very difficult to understand

82
00:07:43,060 --> 00:07:47,700
because it involves so many different things working and once and obviously it could

83
00:07:47,700 --> 00:07:54,140
never be so simple as day leads to B and C leads to D because you have so many different

84
00:07:54,140 --> 00:08:00,580
causes every moment creating new cause, creating new effects and so on.

85
00:08:00,580 --> 00:08:06,380
So I hope that helps to give an explanation of how karma can possibly work in the case

86
00:08:06,380 --> 00:08:12,540
of a Holocaust, it doesn't seem to be at all out of line with the idea of cause and

87
00:08:12,540 --> 00:08:18,980
effect, it obviously is cause and effect, there is obviously something that is a cause

88
00:08:18,980 --> 00:08:19,980
for this.

89
00:08:19,980 --> 00:08:24,180
It's something that we don't see, we don't think about this whole idea of karma, how

90
00:08:24,180 --> 00:08:29,580
all of these people who died and the Holocaust could possibly be guilty of something but

91
00:08:29,580 --> 00:08:34,940
it really, if you look at how many people are guilty of such horrible atrocities even

92
00:08:34,940 --> 00:08:43,740
in this life and where it can possibly lead them in the future and how, where these people

93
00:08:43,740 --> 00:08:50,500
will be going in the future, you know you can have people who are developing on wholesome

94
00:08:50,500 --> 00:08:56,620
mind states and setting themselves up for great suffering in the future.

95
00:08:56,620 --> 00:09:00,940
On a massive scale this sort of thing occurs people who engage in racism, people who engage

96
00:09:00,940 --> 00:09:07,340
in bigotry, you know engage in, who have great hatred in their mind, I mean even those places

97
00:09:07,340 --> 00:09:12,860
where people don't actively hurt others but simply build up and cultivate prejudice and hate.

98
00:09:12,860 --> 00:09:17,540
I think there are a lot of people like this in the world, there's cultures, have like

99
00:09:17,540 --> 00:09:21,820
this in the world and I'm not going to name names but many cultures in many different

100
00:09:21,820 --> 00:09:27,580
places, in many different countries in the world have a great amount of hate and prejudice

101
00:09:27,580 --> 00:09:35,500
for things that are foreign or for a certain group of people and so on, certain type of people.

102
00:09:35,500 --> 00:09:40,740
And that's that sort of thing that will lead you to become a victim of your own anger

103
00:09:40,740 --> 00:09:47,580
and your own hatred and of course other people's taking advantage of that, taking advantage

104
00:09:47,580 --> 00:09:56,300
of your or becoming a part of that stream of karmic energy.

105
00:09:56,300 --> 00:10:25,580
So that's my take on the Holocaust so thanks for tuning in and all the best.

