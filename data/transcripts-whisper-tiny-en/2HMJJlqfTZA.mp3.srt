1
00:00:00,000 --> 00:00:07,000
Okay, good evening, everyone.

2
00:00:07,000 --> 00:00:17,400
Welcome to our evening demonstration.

3
00:00:17,400 --> 00:00:29,000
So on Sunday, we had a big celebration of the Buddha's birthday in Mississauga, and it's

4
00:00:29,000 --> 00:00:35,700
quite impressive every year, all the different Buddhist communities come out and put on

5
00:00:35,700 --> 00:00:56,000
costumes, traditional costumes, and have a parade and chanting and cultural displays, tents,

6
00:00:56,000 --> 00:01:08,000
displaying, promoting all the many different Buddhist organizations around.

7
00:01:08,000 --> 00:01:19,000
And so last year, I submitted a cultural display or an exhibition, part of the show,

8
00:01:19,000 --> 00:01:26,000
and I just put down meditation.

9
00:01:26,000 --> 00:01:29,000
Because it seemed to me that, as I said when I got up and stage, I forgot about it.

10
00:01:29,000 --> 00:01:33,000
And when I finally saw the program, I saw that I was done in the program.

11
00:01:33,000 --> 00:01:37,000
I thought they'd been a mistake.

12
00:01:37,000 --> 00:01:42,000
So after all, there's dancing groups.

13
00:01:42,000 --> 00:01:52,000
And here I was in line, got up on stage, sat there in front of, oh, maybe a thousand, a couple thousand people.

14
00:01:52,000 --> 00:01:57,000
Maybe a thousand, probably not more, maybe less.

15
00:01:57,000 --> 00:02:04,000
And I just taught a little meditation.

16
00:02:04,000 --> 00:02:13,000
What I said was, you know, when the Buddha passed away, before he passed away, all the angels

17
00:02:13,000 --> 00:02:24,000
from all over the universe came down and brought with some flowers from heaven.

18
00:02:24,000 --> 00:02:30,000
Though heavenly flowers are not like human, like flowers from the human world.

19
00:02:30,000 --> 00:02:36,000
Their beauty is, of course, beyond anything that we have here, and they scattered them throughout the forest.

20
00:02:36,000 --> 00:02:41,000
This is what the legends say.

21
00:02:41,000 --> 00:02:48,000
And the Buddha saw this, and he thought to himself, this isn't, well, he said to Anandai, he said,

22
00:02:48,000 --> 00:02:52,000
this isn't how you honor someone.

23
00:02:52,000 --> 00:02:56,000
This is now you honor a Buddha.

24
00:02:56,000 --> 00:03:03,000
And the commenter, there's this one commenter, and it says, what the Buddha was thinking was,

25
00:03:03,000 --> 00:03:08,000
Yahweh-hee-imach-tas-o-barisa.

26
00:03:08,000 --> 00:03:19,000
For as long as my fourfold followers, so monks and nuns and laymen and laywomen,

27
00:03:19,000 --> 00:03:33,000
Mamimaya-pati-pati-bhujaya-bhujisanti, as long as they pay homage to me through the homage of practice.

28
00:03:33,000 --> 00:03:46,000
Pati-pati-bhujaya-pati-bhujaya-pati-bhujaya-bhujaya-bhujaya-bhujaya-bhujaya-bhujaya-bhujaya.

29
00:03:46,000 --> 00:03:56,000
Werojisati will shine light on this world.

30
00:03:56,000 --> 00:04:05,000
We'll continue to shine, just like the full moon in the middle of the sky.

31
00:04:05,000 --> 00:04:17,000
It shines on the earth.

32
00:04:17,000 --> 00:04:26,000
When we talk about paying homage to the Buddha, which is something we talk about, and something we think about,

33
00:04:26,000 --> 00:04:36,000
there's many reasons for that. One is simply because of what a beautiful and pure person he was.

34
00:04:36,000 --> 00:04:43,000
If you have a chance to read his teachings or even read about his life, it's quite remarkable.

35
00:04:43,000 --> 00:04:53,000
The depth of his pronunciation, his compassion, his dedication to truth,

36
00:04:53,000 --> 00:05:02,000
his dedication to peace, and to bringing peace to the earth.

37
00:05:02,000 --> 00:05:07,000
But mostly we pay respect because we've through our practice,

38
00:05:07,000 --> 00:05:18,000
we've come to appreciate what he's given to us, what he's done for us.

39
00:05:18,000 --> 00:05:28,000
By paying homage to him, it's such an affirmation of what a good thing we've found.

40
00:05:28,000 --> 00:05:32,000
This expression of gratitude.

41
00:05:32,000 --> 00:05:40,000
Of course, for any famous person, there's always many, many people who come and pay homage to him.

42
00:05:40,000 --> 00:05:49,000
I mean, the Buddha was concerned that before he passed away, people should be clear that it wasn't through flowers,

43
00:05:49,000 --> 00:05:57,000
or candles, or incense, or bowing down, or doing chanting, or any of this,

44
00:05:57,000 --> 00:06:01,000
that one truly pays homage to him.

45
00:06:01,000 --> 00:06:14,000
He said, through practicing the dhamma, whatever person doesn't matter if they're a monk or a nun or an ordinary man or an ordinary woman.

46
00:06:14,000 --> 00:06:20,000
Whoever practices the dhamma in line with the dhamma to realize the dhamma,

47
00:06:20,000 --> 00:06:29,000
such a person pays the proper respect and homage to the Buddha.

48
00:06:29,000 --> 00:06:37,000
There was a story of this monk who liked to look at the Buddha.

49
00:06:37,000 --> 00:06:46,000
He liked to follow the Buddha around and was just in great bliss because the Buddha was quite beautiful apparently.

50
00:06:46,000 --> 00:06:57,000
So this monk, Waka Lee, he liked to walk around after the Buddha and wherever the Buddha was, he would just sit there and stare at the Buddha.

51
00:06:57,000 --> 00:07:05,000
He was kind of infatuated or, well, he was very happy to see the Buddha.

52
00:07:05,000 --> 00:07:07,000
I gave him great bliss.

53
00:07:07,000 --> 00:07:09,000
The Buddha saw, well, this isn't the way.

54
00:07:09,000 --> 00:07:11,000
He's not going to become enlightened this way.

55
00:07:11,000 --> 00:07:15,000
So the Buddha said to him, famous words, he said,

56
00:07:15,000 --> 00:07:28,000
and you go, Waka Lee, dhamma, passati, so among passati, or something like that.

57
00:07:28,000 --> 00:07:30,000
I can't remember the exact following.

58
00:07:30,000 --> 00:07:34,000
Whoever sees the dhamma, sees me, it's basically what he said.

59
00:07:34,000 --> 00:07:39,000
So it always comes back to the dhamma or the dharma.

60
00:07:39,000 --> 00:07:46,000
And so this is what I talked about on Sunday. I'd like to share with you a little bit of dhamma.

61
00:07:46,000 --> 00:07:49,000
Five dhammas, for us to reflect upon.

62
00:07:49,000 --> 00:07:57,000
I think this is a really useful thing for all of us to hear and those of you who are coming to the end of your course,

63
00:07:57,000 --> 00:08:02,000
as well as those of you who have maybe never practiced meditation.

64
00:08:02,000 --> 00:08:04,000
Remember what it is for practicing.

65
00:08:04,000 --> 00:08:06,000
What is the dhamma?

66
00:08:06,000 --> 00:08:10,000
The dhamma is reality.

67
00:08:10,000 --> 00:08:15,000
Dhamma in this sense means something that really exists.

68
00:08:15,000 --> 00:08:17,000
The dhamma means to hold.

69
00:08:17,000 --> 00:08:25,000
So it holds an existence, unlike imagination, which doesn't actually hold anything.

70
00:08:25,000 --> 00:08:32,000
And you think about a dragon or you think about the Easter Bunny.

71
00:08:32,000 --> 00:08:36,000
They don't hold any reality.

72
00:08:36,000 --> 00:08:39,000
They don't hold up in reality.

73
00:08:39,000 --> 00:08:44,000
The dhamma is a dhamma is truly real.

74
00:08:44,000 --> 00:08:46,000
Real in what sense?

75
00:08:46,000 --> 00:08:50,000
Well, real in the sense that you can experience it.

76
00:08:50,000 --> 00:08:52,000
So what are the dhammas?

77
00:08:52,000 --> 00:08:53,000
These five dhammas.

78
00:08:53,000 --> 00:08:55,000
They're adapted from the force at the baton of it.

79
00:08:55,000 --> 00:09:00,000
Let's just talk about them as the five dhammas.

80
00:09:00,000 --> 00:09:02,000
The first one is the body.

81
00:09:02,000 --> 00:09:08,000
So you can try this, close your eyes and reflect on your body.

82
00:09:08,000 --> 00:09:15,000
There is physical reality.

83
00:09:15,000 --> 00:09:19,000
There is the hardness and the softness of the seat that you're sitting on.

84
00:09:19,000 --> 00:09:21,000
That's a feeling.

85
00:09:21,000 --> 00:09:24,000
There's the heat and the cold and the room around you.

86
00:09:24,000 --> 00:09:28,000
There's tension in your back and in your legs.

87
00:09:28,000 --> 00:09:32,000
Maybe tension in your head.

88
00:09:32,000 --> 00:09:37,000
This is all physical.

89
00:09:37,000 --> 00:09:41,000
So the Buddha taught us to be mindful if you're sitting to say yourself,

90
00:09:41,000 --> 00:09:43,000
sitting, sitting.

91
00:09:43,000 --> 00:09:46,000
You're standing, say standing, standing, or walking,

92
00:09:46,000 --> 00:09:49,000
walking, lying, lying.

93
00:09:49,000 --> 00:09:55,000
Or whoever your body is deported.

94
00:09:55,000 --> 00:10:05,000
If we do this, we practice meditation in order to stay close to reality.

95
00:10:05,000 --> 00:10:10,000
As we do that, our minds are going to stay with the body.

96
00:10:10,000 --> 00:10:15,000
When you're going to learn about reality.

97
00:10:15,000 --> 00:10:17,000
It sounds kind of banal, I think.

98
00:10:17,000 --> 00:10:21,000
If you've never practiced meditation, you don't realize the significance of doing that.

99
00:10:21,000 --> 00:10:24,000
First of all, how challenging it is.

100
00:10:24,000 --> 00:10:28,000
The second of all, how important that challenges.

101
00:10:28,000 --> 00:10:31,000
How much you learn about yourself.

102
00:10:31,000 --> 00:10:36,000
When you do just that, when you look at the body.

103
00:10:36,000 --> 00:10:39,000
Because with the body, you see all these other realities.

104
00:10:39,000 --> 00:10:41,000
The second reality is the feelings.

105
00:10:41,000 --> 00:10:44,000
So you start to see pain, and you see happiness,

106
00:10:44,000 --> 00:10:47,000
so you'll see calm.

107
00:10:47,000 --> 00:10:52,000
Some people feel very calm when they meditate.

108
00:10:52,000 --> 00:10:56,000
Some people feel a lot of pain when they meditate.

109
00:10:56,000 --> 00:10:58,000
You'll see all this.

110
00:10:58,000 --> 00:11:03,000
And pain like the body or feelings, happiness like the body.

111
00:11:03,000 --> 00:11:08,000
All these feelings, they teach you about yourself.

112
00:11:08,000 --> 00:11:11,000
You get to see how you react to feelings.

113
00:11:11,000 --> 00:11:18,000
You get to see how you interact with feelings.

114
00:11:18,000 --> 00:11:23,000
When you focus on the pain and say pain, pain,

115
00:11:23,000 --> 00:11:27,000
you're learning about feelings, and therefore you're learning about yourself.

116
00:11:27,000 --> 00:11:33,000
How you relate to the feeling.

117
00:11:42,000 --> 00:11:47,000
The third reality is the mind.

118
00:11:47,000 --> 00:11:50,000
As you focus on the body and the feelings,

119
00:11:50,000 --> 00:11:53,000
you're also going to notice that you're thinking.

120
00:11:53,000 --> 00:11:55,000
You'll notice your thoughts more clearly.

121
00:11:55,000 --> 00:11:58,000
Thoughts about the past or future.

122
00:11:58,000 --> 00:12:02,000
Thoughts about the present or thoughts of imagination.

123
00:12:02,000 --> 00:12:06,000
You'll see them clearly.

124
00:12:06,000 --> 00:12:08,000
You'll be able to watch them.

125
00:12:08,000 --> 00:12:10,000
You'll be able to learn about them.

126
00:12:10,000 --> 00:12:12,000
What do we think about?

127
00:12:12,000 --> 00:12:17,000
How often do we think? How long do we think?

128
00:12:17,000 --> 00:12:20,000
How obsessed we get in thoughts.

129
00:12:20,000 --> 00:12:22,000
How we react to our thoughts.

130
00:12:22,000 --> 00:12:25,000
How we react to our memories.

131
00:12:25,000 --> 00:12:28,000
All the memories that come up when you meditate.

132
00:12:28,000 --> 00:12:30,000
How we react to them.

133
00:12:30,000 --> 00:12:32,000
You'll start to see this.

134
00:12:32,000 --> 00:12:36,000
You'll see relationships between the darmus.

135
00:12:36,000 --> 00:12:46,000
The fourth reality is our reactions, our emotions,

136
00:12:46,000 --> 00:12:49,000
not just our reactions, but our mind state.

137
00:12:49,000 --> 00:12:52,000
So liking and disliking these reactions,

138
00:12:52,000 --> 00:12:56,000
but my boredom, sadness, fear, depression, frustration.

139
00:12:56,000 --> 00:13:11,000
On the other side, desire, wishfulness, loneliness,

140
00:13:11,000 --> 00:13:15,000
pining away over things.

141
00:13:15,000 --> 00:13:19,000
And then there's drowsiness and distraction

142
00:13:19,000 --> 00:13:23,000
and doubt and confusion and worry and all those.

143
00:13:23,000 --> 00:13:28,000
And these are all real. These are very important.

144
00:13:28,000 --> 00:13:31,000
The most remarkable thing about the meditation is how it works

145
00:13:31,000 --> 00:13:33,000
to free you from these.

146
00:13:33,000 --> 00:13:38,000
It works to sort out your emotions, sort out your mind states,

147
00:13:38,000 --> 00:13:41,000
so that if you're worried and if you get good

148
00:13:41,000 --> 00:13:46,000
and learn the ins and outs of your mind,

149
00:13:46,000 --> 00:13:50,000
just saying to yourself worried and thinking about

150
00:13:50,000 --> 00:13:54,000
acknowledging the thinking about whatever's making you worry and so on.

151
00:13:54,000 --> 00:13:57,000
You work it out.

152
00:13:57,000 --> 00:14:02,000
You're able to free yourself from the worry, from the stress,

153
00:14:02,000 --> 00:14:10,000
from boredom, sadness, fear, anxiety, depression,

154
00:14:10,000 --> 00:14:16,000
even addiction, just by being mindful.

155
00:14:16,000 --> 00:14:20,000
Not even by trying to fix anything.

156
00:14:20,000 --> 00:14:24,000
The greatness of the reality is that when you see it,

157
00:14:24,000 --> 00:14:28,000
when you see it, you free yourself from the power it has for you.

158
00:14:28,000 --> 00:14:34,000
You free yourself from all the suffering that comes from ignorance.

159
00:14:34,000 --> 00:14:36,000
That's really the major quality of ignorance

160
00:14:36,000 --> 00:14:39,000
is that it makes you suffer.

161
00:14:39,000 --> 00:14:41,000
Because of course no one wants to suffer,

162
00:14:41,000 --> 00:14:46,000
so if you actually knew the truth, you would never suffer.

163
00:14:46,000 --> 00:14:48,000
It's the brilliance of it,

164
00:14:48,000 --> 00:14:51,000
because that no one else makes us suffer.

165
00:14:51,000 --> 00:14:57,000
It may cause ourselves suffering.

166
00:14:57,000 --> 00:15:01,000
And the fifth dharma is the senses,

167
00:15:01,000 --> 00:15:10,000
seeing, hearing, smelling, tasting, feeling.

168
00:15:10,000 --> 00:15:14,000
This is the outside, the external.

169
00:15:14,000 --> 00:15:17,000
The rest of them are mostly internal.

170
00:15:17,000 --> 00:15:22,000
But the doors, the external are the senses.

171
00:15:22,000 --> 00:15:25,000
So this is where we easily get lost.

172
00:15:25,000 --> 00:15:28,000
As soon as you open your eyes, your mind is

173
00:15:28,000 --> 00:15:30,000
leaping out through your eyes

174
00:15:30,000 --> 00:15:34,000
into all the things that you experience.

175
00:15:34,000 --> 00:15:38,000
So we use mindfulness as a guard.

176
00:15:38,000 --> 00:15:43,000
Mindfulness as a doorkeeper.

177
00:15:43,000 --> 00:15:48,000
When you see, say to yourself, seeing, seeing, hearing, hearing,

178
00:15:48,000 --> 00:15:59,000
smelling, smelling, tasting, so on.

179
00:15:59,000 --> 00:16:04,000
And you learn about the reality of your own experience.

180
00:16:04,000 --> 00:16:08,000
You learn everything you need to know about the universe.

181
00:16:08,000 --> 00:16:13,000
It's quite remarkable.

182
00:16:13,000 --> 00:16:17,000
You have done monasticity, so monasticity.

183
00:16:17,000 --> 00:16:21,000
You see the Buddha.

184
00:16:21,000 --> 00:16:33,000
You come to see, seeing the Buddha.

185
00:16:33,000 --> 00:16:38,000
And you pay the highest homage, the highest tribute,

186
00:16:38,000 --> 00:16:41,000
the highest respect.

187
00:16:41,000 --> 00:16:45,000
And I mean, what's most important about that

188
00:16:45,000 --> 00:16:47,000
is that you feel reassured.

189
00:16:47,000 --> 00:16:49,000
That people like the Buddha are really,

190
00:16:49,000 --> 00:16:52,000
this is, you know, people as wise as that,

191
00:16:52,000 --> 00:16:56,000
praised that what you're doing now.

192
00:16:56,000 --> 00:17:04,000
When you feel kind of low about yourself

193
00:17:04,000 --> 00:17:11,000
or feeling inadequate, you can be reassured

194
00:17:11,000 --> 00:17:16,000
and proud or confident, encouraged by the fact

195
00:17:16,000 --> 00:17:21,000
that what you're doing was praised by the Buddha.

196
00:17:21,000 --> 00:17:24,000
What you're doing is what the Buddha taught us to do,

197
00:17:24,000 --> 00:17:28,000
to all these religious ceremonies with flowers

198
00:17:28,000 --> 00:17:30,000
and candles and incense.

199
00:17:30,000 --> 00:17:32,000
That's all fine, but it's not really,

200
00:17:32,000 --> 00:17:36,000
well, it's not at all what the Buddha taught us to do.

201
00:17:36,000 --> 00:17:41,000
But it's said the best way to pay respect is through the dhamma.

202
00:17:41,000 --> 00:17:48,000
And the best way to be sure you're doing the right thing.

203
00:17:48,000 --> 00:17:53,000
After I gave the talk, someone came from one of the

204
00:17:53,000 --> 00:17:56,000
Chinese monasteries, I think, and invited me.

205
00:17:56,000 --> 00:17:59,000
They want me to go to their monastery and teach meditation.

206
00:17:59,000 --> 00:18:01,000
He said, I learned so much.

207
00:18:01,000 --> 00:18:04,000
I was only up there for less than five minutes.

208
00:18:04,000 --> 00:18:06,000
I think maybe five minutes.

209
00:18:06,000 --> 00:18:08,000
And he said, oh, I learned so much.

210
00:18:08,000 --> 00:18:10,000
It was quite interesting.

211
00:18:10,000 --> 00:18:12,000
The dhamma is not something hard to teach.

212
00:18:12,000 --> 00:18:16,000
It's not hard, it's not hard to understand.

213
00:18:16,000 --> 00:18:20,000
But we get lost in sidetracked and, you know,

214
00:18:20,000 --> 00:18:27,000
so much of what we call Buddhism is just a fluff.

215
00:18:27,000 --> 00:18:30,000
I think it's really great by doing this,

216
00:18:30,000 --> 00:18:32,000
and we set up a meditation tent.

217
00:18:32,000 --> 00:18:34,000
Now, there's copycats.

218
00:18:34,000 --> 00:18:36,000
People are stealing the idea,

219
00:18:36,000 --> 00:18:38,000
or we've competition.

220
00:18:38,000 --> 00:18:40,000
There was apparently another, there was, on Sunday,

221
00:18:40,000 --> 00:18:42,000
there was another meditation tent,

222
00:18:42,000 --> 00:18:44,000
teaching loving kindness.

223
00:18:44,000 --> 00:18:46,000
Apparently, meditation's, and all the monasteries are now

224
00:18:46,000 --> 00:18:48,000
calling themselves meditation centers.

225
00:18:48,000 --> 00:18:50,000
I'm not the only one, it's not copying me,

226
00:18:50,000 --> 00:18:58,000
but there's been several, it's picking up the pace.

227
00:18:58,000 --> 00:19:01,000
More and more centers practicing mindfulness,

228
00:19:01,000 --> 00:19:06,000
and everybody else scrambling to get into it.

229
00:19:06,000 --> 00:19:09,000
All because really, all because the Buddha was so clear.

230
00:19:09,000 --> 00:19:11,000
I think right before he passed away the fact

231
00:19:11,000 --> 00:19:14,000
that he was so clear about this, you know,

232
00:19:14,000 --> 00:19:19,000
very important, and because of the greatness of what he taught

233
00:19:19,000 --> 00:19:25,000
and how he can still realize it today.

234
00:19:25,000 --> 00:19:29,000
So there you go, a little bit of dharma for tonight.

235
00:19:29,000 --> 00:19:31,000
Thank you all for tuning in.

236
00:19:31,000 --> 00:19:48,000
Have a good night.

