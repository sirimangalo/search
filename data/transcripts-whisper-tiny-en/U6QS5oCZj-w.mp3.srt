1
00:00:00,000 --> 00:00:05,220
Hello and welcome back to our study of the Dhamapada. Today we continue on with

2
00:00:05,220 --> 00:00:32,720
first number 107 which reads as follows.

3
00:00:32,720 --> 00:00:46,420
And one might and a being, you know, whatever being might, a ging-pari-kari-wani, live in the forest,

4
00:00:46,420 --> 00:00:54,720
carrying, tending to a fire. So it might live in a forest for a hundred years.

5
00:00:54,720 --> 00:01:06,720
A kanjabawi-tatana misses the same as yesterday's first. Muhu-tam-poo-jay, one should pay homage to one who has developed themselves.

6
00:01:06,720 --> 00:01:23,220
Just for one moment, Sai-yewa-poo-jana-seyo, this poo-jah is greater, greater than the one who for a hundred years practices this sacrifice.

7
00:01:23,220 --> 00:01:37,220
Now, not a terribly meaningful verse for those of us who aren't familiar with living in the forest tending to a fire.

8
00:01:37,220 --> 00:01:44,220
But we can generalize and talk about it. The story as well is quite similar to yesterday as it's almost identical.

9
00:01:44,220 --> 00:01:53,220
The difference here is the sorry puta instead of going to talk to his uncle. He goes and talks to his nephew.

10
00:01:53,220 --> 00:02:00,220
And he asks his nephew so nephew. His nephew is also a Brahman, his whole family, of course, with Brahman.

11
00:02:00,220 --> 00:02:10,220
He says, what sort of King Brahmana-kusulankarosi would sort of wholesomeness or good deeds do you do?

12
00:02:10,220 --> 00:02:30,220
And so he says, oh every month, Brahmana-seyo-kun-kum-pussul, every month I kill a, having killed a busu livestock, like a pet of some sort, some kind of domesticated animal or some animal.

13
00:02:30,220 --> 00:02:33,220
The sheep may be a goat probably.

14
00:02:33,220 --> 00:02:44,220
A ging-bri-bri-chara-mi. I use it to tend to a fire or burn it in a fire. Maybe make a burnt offering.

15
00:02:44,220 --> 00:02:53,220
It sounds like he just burns it in a fire, kind of an Abrahamic sort of practice.

16
00:02:53,220 --> 00:02:59,220
It's with the Jewish people would do. It's very similar.

17
00:02:59,220 --> 00:03:09,220
But fire sacrifice seems to have originally been burning goats and so on, all over the world.

18
00:03:09,220 --> 00:03:14,220
Somehow this was a big thing to do.

19
00:03:14,220 --> 00:03:18,220
And then he says, for what purpose do you?

20
00:03:18,220 --> 00:03:32,220
For what purpose do you do that? And he says, oh, for the same as before. Oh, for the Brahmaloka-mago-kiri.

21
00:03:32,220 --> 00:03:39,220
So I have heard where it is said that this is the path to the Brahmaloka.

22
00:03:39,220 --> 00:03:44,220
And so he put this as, who told you that? Same as yesterday, same as last time.

23
00:03:44,220 --> 00:03:50,220
And again, it was my teachers. So for the first one, it was for last one.

24
00:03:50,220 --> 00:03:55,220
It was easy to kind of get a sense that his teachers were these naked ascetics.

25
00:03:55,220 --> 00:04:01,220
Last time it was giving every month, giving a thousand pieces of money worth of food or whatever.

26
00:04:01,220 --> 00:04:06,220
Maybe even just money directly to these naked ascetics.

27
00:04:06,220 --> 00:04:11,220
And so one would think that they were probably his teachers who were teaching in this.

28
00:04:11,220 --> 00:04:15,220
In this case, the question is, why are his teachers teaching such things?

29
00:04:15,220 --> 00:04:20,220
There's various reasons. And we can extrapolate this.

30
00:04:20,220 --> 00:04:22,220
To sort of generalize in terms of religion.

31
00:04:22,220 --> 00:04:25,220
Why do people teach such ridiculous things?

32
00:04:25,220 --> 00:04:29,220
I mean, it's understandable why people believe them.

33
00:04:29,220 --> 00:04:35,220
We just tend to believe things based on tradition or based on the authority of our teachers.

34
00:04:35,220 --> 00:04:40,220
Even though we ourselves have no sense of the causal relationship.

35
00:04:40,220 --> 00:04:45,220
So this is why religions do such wide array of practices.

36
00:04:45,220 --> 00:04:49,220
Because we're just told to buy our priests or buy our leaders.

37
00:04:49,220 --> 00:04:54,220
The question is, why do the leaders teach such things?

38
00:04:54,220 --> 00:04:57,220
Sometimes it's just for gain.

39
00:04:57,220 --> 00:04:59,220
I mean, I've seen it even in Buddhism.

40
00:04:59,220 --> 00:05:04,220
People come to the monasteries, the temples.

41
00:05:04,220 --> 00:05:10,220
And they ask the teacher, what should I do? Someone passed away, what should I do?

42
00:05:10,220 --> 00:05:13,220
And I've seen teachers, I've seen monks.

43
00:05:13,220 --> 00:05:16,220
You can just see they're the wheels turning in their head.

44
00:05:16,220 --> 00:05:18,220
Kind of in a bit of panic.

45
00:05:18,220 --> 00:05:25,220
Scrambling to find something that looks or sounds kind of mystical.

46
00:05:25,220 --> 00:05:27,220
It really comes down to that.

47
00:05:27,220 --> 00:05:30,220
Just making something up on the spur of the moment.

48
00:05:30,220 --> 00:05:39,220
To appease people's desire for some ritual, something to solve their problems.

49
00:05:39,220 --> 00:05:50,220
Sri Damika, a very wonderful sort of populist Buddhist monk and teacher from Sri Lanka.

50
00:05:50,220 --> 00:05:54,220
He said, someone asked him about these emulates that monks give out.

51
00:05:54,220 --> 00:05:57,220
And he said, well, they have absolutely no meaning.

52
00:05:57,220 --> 00:06:04,220
Sometimes for people who are new or who don't really have a good understanding of cause and effect,

53
00:06:04,220 --> 00:06:06,220
you have to give them something.

54
00:06:06,220 --> 00:06:08,220
So he took advantage.

55
00:06:08,220 --> 00:06:09,220
He says, you see this?

56
00:06:09,220 --> 00:06:12,220
I want you to keep it, hold on to it and it will keep you safe.

57
00:06:12,220 --> 00:06:14,220
He says, sometimes you have to do that.

58
00:06:14,220 --> 00:06:19,220
That's sort of like a dumbo in his feather if you ever saw the Disney movie.

59
00:06:19,220 --> 00:06:23,220
Sometimes people need their magical feather.

60
00:06:23,220 --> 00:06:32,220
So there is a defense of it, but I don't think it's a very strong defense,

61
00:06:32,220 --> 00:06:37,220
especially when it becomes one's main, absolutely no defense,

62
00:06:37,220 --> 00:06:40,220
when it becomes one's main religious practice.

63
00:06:40,220 --> 00:06:47,220
And not only is there no defense, but it's a blame-worthy

64
00:06:47,220 --> 00:06:51,220
when it involves unholesome, clearly unholesome acts.

65
00:06:51,220 --> 00:06:59,220
It's one thing to tell people that offering food to a statue is going to be to their benefit

66
00:06:59,220 --> 00:07:06,220
or pouring water on the root of a tree is going to do this or do that.

67
00:07:06,220 --> 00:07:08,220
That's kind of innocent.

68
00:07:08,220 --> 00:07:15,220
And who knows, maybe the angels will get involved and help out with it if they...

69
00:07:15,220 --> 00:07:24,220
There's this interesting thing, Buddhists in Sri Lanka are very keen to pour water

70
00:07:24,220 --> 00:07:26,220
at the roots of the Bodhi tree.

71
00:07:26,220 --> 00:07:31,220
So the pour water, the Bodhi tree that go around is very important ceremony.

72
00:07:31,220 --> 00:07:37,220
And they do this with the understanding that it leads to pregnancy.

73
00:07:37,220 --> 00:07:40,220
And it apparently has some measure of success.

74
00:07:40,220 --> 00:07:44,220
I mean, it's anecdotal and scientists may be able to study it

75
00:07:44,220 --> 00:07:48,220
and find that in fact, there is no correlation.

76
00:07:48,220 --> 00:07:52,220
But it's an interesting idea that there might be some correlation

77
00:07:52,220 --> 00:07:55,220
because how would that relate to the Buddhist teaching?

78
00:07:55,220 --> 00:07:57,220
It's certainly not a Buddhist teaching.

79
00:07:57,220 --> 00:07:59,220
That's such a thing as possible.

80
00:07:59,220 --> 00:08:01,220
And yet we have in the Dhammapada stories.

81
00:08:01,220 --> 00:08:03,220
Remember the first story that we did.

82
00:08:03,220 --> 00:08:06,220
I don't know if I actually brought up and brought up the whole backstory.

83
00:08:06,220 --> 00:08:08,220
But this guy does basically that.

84
00:08:08,220 --> 00:08:15,220
He goes to a tree and he cleans up around this great tree in the forest

85
00:08:15,220 --> 00:08:20,220
and puts up banners and flags and a wall around it to protect it.

86
00:08:20,220 --> 00:08:24,220
And then makes a promise to the tree that if he gets a son or a daughter,

87
00:08:24,220 --> 00:08:28,220
he'll come back and do pay great homage and respect to the tree.

88
00:08:28,220 --> 00:08:33,220
And sure enough, his wife gives birth right quite quickly after that.

89
00:08:33,220 --> 00:08:37,220
So people from Sri Lanka have told me that this actually works.

90
00:08:37,220 --> 00:08:40,220
I was trying to figure out exactly how it might work in reality

91
00:08:40,220 --> 00:08:43,220
because there has to be a causal relationship.

92
00:08:43,220 --> 00:08:45,220
If it can't just be magic, there's no such thing.

93
00:08:45,220 --> 00:08:51,220
And it's not really accepted in Buddhism.

94
00:08:51,220 --> 00:08:55,220
So the idea that somehow you could do some ritual and then it could work.

95
00:08:55,220 --> 00:08:59,220
The only way it could work, and I'm thinking it actually could,

96
00:08:59,220 --> 00:09:01,220
is if the angels got involved,

97
00:09:01,220 --> 00:09:07,220
saying because there's got to be something to do with angels hanging out at the Bodhi tree.

98
00:09:07,220 --> 00:09:14,220
And if the lay people come to the Bodhi tree and they do this

99
00:09:14,220 --> 00:09:18,220
and they make a sincere wish and they have a sincerely good heart,

100
00:09:18,220 --> 00:09:20,220
it's kind of like the angels can look down and say,

101
00:09:20,220 --> 00:09:22,220
oh, those are nice people.

102
00:09:22,220 --> 00:09:24,220
Well, my lifespan is almost up.

103
00:09:24,220 --> 00:09:28,220
I think maybe they even hang out looking for parents.

104
00:09:28,220 --> 00:09:31,220
And when the angels know that their lifespan is almost up,

105
00:09:31,220 --> 00:09:35,220
they try to find suitable parents to go and be reborn.

106
00:09:35,220 --> 00:09:36,220
I mean, something like that.

107
00:09:36,220 --> 00:09:39,220
Somehow the angels, the day was get involved.

108
00:09:39,220 --> 00:09:44,220
But I give that only as an example of how there might be some causal relationship.

109
00:09:44,220 --> 00:09:48,220
But there has to be something like that or else it's just ridiculous.

110
00:09:48,220 --> 00:09:51,220
And so for the most part it is, especially,

111
00:09:51,220 --> 00:09:55,220
it's beyond ridiculous when it comes down to killing animals,

112
00:09:55,220 --> 00:09:59,220
killing your fellow living beings and saying that somehow the past,

113
00:09:59,220 --> 00:10:00,220
the Brahma world.

114
00:10:00,220 --> 00:10:03,220
So Saripu had to rightly says to him,

115
00:10:03,220 --> 00:10:07,220
he says, who taught you your teachers and he says your teachers haven't a clue.

116
00:10:07,220 --> 00:10:12,220
And so the reason why the teachers might have been teaching it may have just been,

117
00:10:12,220 --> 00:10:16,220
because you see, if you sound like you know what you're doing,

118
00:10:16,220 --> 00:10:19,220
but if you set up all these complex rituals people think,

119
00:10:19,220 --> 00:10:24,220
oh, well, this person we need him because he leads us

120
00:10:24,220 --> 00:10:27,220
in these very important rituals, and only he knows the right rituals,

121
00:10:27,220 --> 00:10:29,220
when in fact they just make them up.

122
00:10:29,220 --> 00:10:32,220
And so I think that's what you get.

123
00:10:32,220 --> 00:10:38,220
I'm pretty sure that's what you get in a lot of the rituals at the time of the Buddha,

124
00:10:38,220 --> 00:10:40,220
and even after the Buddha.

125
00:10:40,220 --> 00:10:44,220
But definitely very much before the Buddha came around.

126
00:10:44,220 --> 00:10:49,220
Like we've studied these in university when I was taking Indian religion many years ago.

127
00:10:49,220 --> 00:10:52,220
And some of the rituals are just silly.

128
00:10:52,220 --> 00:10:57,220
They take, it seems like, originally they were horrific.

129
00:10:57,220 --> 00:11:00,220
These horrific sacrifices a lot of killing.

130
00:11:00,220 --> 00:11:05,220
And so they would take a goat up to the altar and cut its head off.

131
00:11:05,220 --> 00:11:09,220
And there was something about burying a live turtle under the altar,

132
00:11:09,220 --> 00:11:13,220
just burying it alive and killing it and suffocating it under the altar.

133
00:11:13,220 --> 00:11:17,220
I mean, for no reason, I mean, why would you bury a live turtle

134
00:11:17,220 --> 00:11:19,220
under a stone altar?

135
00:11:19,220 --> 00:11:21,220
But it was an important part of building the altar.

136
00:11:21,220 --> 00:11:26,220
And then there was butter that had to be poured into the fire,

137
00:11:26,220 --> 00:11:28,220
and that kind of thing.

138
00:11:28,220 --> 00:11:29,220
And it evolved.

139
00:11:29,220 --> 00:11:32,220
So that eventually, probably with the advent of Buddhism,

140
00:11:32,220 --> 00:11:36,220
Jainism, and a lot of the movements that were anti torture,

141
00:11:36,220 --> 00:11:38,220
or anti cruelty.

142
00:11:38,220 --> 00:11:43,220
Eventually, by the time we came to the form that we studied today,

143
00:11:43,220 --> 00:11:46,220
what they do is they bring a goat to the altar,

144
00:11:46,220 --> 00:11:49,220
and they have to show the altar to the goat.

145
00:11:49,220 --> 00:11:51,220
Like, the goat has to see the altar.

146
00:11:51,220 --> 00:11:53,220
And then they take the goat away.

147
00:11:53,220 --> 00:11:55,220
There's a sacrificial pole.

148
00:11:55,220 --> 00:11:58,220
There's a special wooden pole that has to be there as well,

149
00:11:58,220 --> 00:12:02,220
and they have to bring the goat and show the goat that pole.

150
00:12:02,220 --> 00:12:05,220
So as long as the goat sees the pole, they've done enough,

151
00:12:05,220 --> 00:12:07,220
and they take the goat away.

152
00:12:07,220 --> 00:12:10,220
And you can imagine some guys saying, oh, well, we can't kill them anymore.

153
00:12:10,220 --> 00:12:14,220
Well, let's just tell everyone, oh, it's enough that the goat sees the pole.

154
00:12:14,220 --> 00:12:17,220
I mean, it's something that they just come up with.

155
00:12:17,220 --> 00:12:19,220
And say, oh, no, no, no.

156
00:12:19,220 --> 00:12:21,220
People say, well, we don't really want to kill the goat.

157
00:12:21,220 --> 00:12:25,220
Well, it's OK, as long as he sees the post, somehow that's meaningful.

158
00:12:25,220 --> 00:12:27,220
It's no longer about killing.

159
00:12:27,220 --> 00:12:34,220
So I mean, good on them that their ridiculous sacrifices have become innocent.

160
00:12:34,220 --> 00:12:36,220
That's one step in the right direction.

161
00:12:36,220 --> 00:12:39,220
The other thing is they, instead of burying a turtle under the altar,

162
00:12:39,220 --> 00:12:42,220
they take a lump of butter, and they bury it under the altar.

163
00:12:42,220 --> 00:12:45,220
It's a substitute for a live turtle.

164
00:12:45,220 --> 00:12:50,220
That's a good thing for all the poor little turtle turtles.

165
00:12:50,220 --> 00:12:56,220
But definitely, innocent doesn't mean beneficial worthwhile.

166
00:12:56,220 --> 00:13:00,220
And in this guy's case, he's got real problems with his killing,

167
00:13:00,220 --> 00:13:04,220
every month killing a living being and feeding it to the fire.

168
00:13:04,220 --> 00:13:06,220
Like if it was just butter, he was feeding to the fire,

169
00:13:06,220 --> 00:13:09,220
then you could say, well, that's kind of innocent.

170
00:13:09,220 --> 00:13:12,220
But even still, it's not the way to the Brahma world.

171
00:13:12,220 --> 00:13:17,220
It's not any connection with being reborn as a Brahma, as a God.

172
00:13:17,220 --> 00:13:22,220
So it takes him to see the Buddha and the Buddha.

173
00:13:22,220 --> 00:13:26,220
He tells the Buddha what he does, and the Buddha says,

174
00:13:26,220 --> 00:13:30,220
you can do that for a hundred years, every month.

175
00:13:30,220 --> 00:13:35,220
And it wouldn't be worth the thousandth part,

176
00:13:35,220 --> 00:13:41,220
if he were to just pay respect to an enlightened being for one moment.

177
00:13:41,220 --> 00:13:45,220
So again, talking about homage.

178
00:13:45,220 --> 00:13:50,220
And the Buddha specifies my students.

179
00:13:50,220 --> 00:13:53,220
So it is a bold claim.

180
00:13:53,220 --> 00:13:58,220
And as I said last time, it seems like a bit of a bias claim at first.

181
00:13:58,220 --> 00:14:00,220
Blush, because anyone could say that.

182
00:14:00,220 --> 00:14:03,220
Well, of course, everyone's going to say their own students.

183
00:14:03,220 --> 00:14:06,220
But it has to be true at some point.

184
00:14:06,220 --> 00:14:11,220
So if your students are enlightened, and if your people really are,

185
00:14:11,220 --> 00:14:15,220
if you're talking about a group of people who really are enlightened,

186
00:14:15,220 --> 00:14:19,220
then the Buddha wasn't afraid of making these bold and sort of bragging claims.

187
00:14:19,220 --> 00:14:22,220
Most full claims.

188
00:14:22,220 --> 00:14:25,220
Because if you look at it another way, of course,

189
00:14:25,220 --> 00:14:28,220
it's leaving you wide open to attack.

190
00:14:28,220 --> 00:14:31,220
And the Buddha fully welcomed what he's saying.

191
00:14:31,220 --> 00:14:33,220
He's really, it's called the lion's roar.

192
00:14:33,220 --> 00:14:36,220
He was really putting it out there.

193
00:14:36,220 --> 00:14:40,220
He's making a claim, a boast, and a challenge.

194
00:14:40,220 --> 00:14:42,220
You know, prove me wrong.

195
00:14:42,220 --> 00:14:49,220
That's basically saying, it's claiming something very, very boldly.

196
00:14:49,220 --> 00:14:53,220
And that's really what the purpose and the meaning there is.

197
00:14:53,220 --> 00:14:57,220
That's not the Buddha bragging or forget gain of any sort.

198
00:14:57,220 --> 00:15:00,220
There's no sense that the Buddha even had any need.

199
00:15:00,220 --> 00:15:05,220
I mean, even if you don't follow Buddhism, there was no real evidence

200
00:15:05,220 --> 00:15:14,220
to support Dad when he was well taken care of and well supported.

201
00:15:14,220 --> 00:15:19,220
But what he's doing is instead making just this bold claim

202
00:15:19,220 --> 00:15:22,220
that could then be challenged.

203
00:15:22,220 --> 00:15:25,220
And of course, the Brahmin would have to be impressed by that

204
00:15:25,220 --> 00:15:27,220
because he couldn't challenge it.

205
00:15:27,220 --> 00:15:29,220
And looking at the Buddhist monks, he would have to agree

206
00:15:29,220 --> 00:15:34,220
that, well, yes, indeed, there's something special about many

207
00:15:34,220 --> 00:15:36,220
of these monks who have developed themselves.

208
00:15:36,220 --> 00:15:38,220
And so that's what the verse actually says.

209
00:15:38,220 --> 00:15:39,220
Babi Datta, no.

210
00:15:39,220 --> 00:15:43,220
To one, if you pay homage to one or to those who are

211
00:15:43,220 --> 00:15:46,220
of developed self.

212
00:15:46,220 --> 00:15:49,220
So again, not to get too much into it.

213
00:15:49,220 --> 00:15:51,220
I think the more important aspect of this,

214
00:15:51,220 --> 00:15:53,220
that differentiates it from the last one,

215
00:15:53,220 --> 00:15:56,220
is talking about sacrifice, which I've done.

216
00:15:56,220 --> 00:15:59,220
But I think we can extrapolate that to talk about

217
00:15:59,220 --> 00:16:02,220
religious practices in general.

218
00:16:02,220 --> 00:16:05,220
It's not enough that we have a religious practice.

219
00:16:05,220 --> 00:16:08,220
And I think there's a lot of this in the world.

220
00:16:08,220 --> 00:16:10,220
Just because something is a spiritual practice

221
00:16:10,220 --> 00:16:14,220
or a religious practice doesn't make it really all that valuable.

222
00:16:14,220 --> 00:16:17,220
And certainly an understanding that the different

223
00:16:17,220 --> 00:16:20,220
spiritual practices have different values.

224
00:16:20,220 --> 00:16:24,220
So it's not to say that either you're practicing to become

225
00:16:24,220 --> 00:16:27,220
enlightened or you're doing useless things.

226
00:16:27,220 --> 00:16:30,220
There are certain religious practices that are valuable,

227
00:16:30,220 --> 00:16:32,220
but just not as valuable.

228
00:16:32,220 --> 00:16:36,220
So feeding of sacrificial fire is pretty useless.

229
00:16:36,220 --> 00:16:38,220
But practicing loving kindness, for example,

230
00:16:38,220 --> 00:16:39,220
is quite valuable.

231
00:16:39,220 --> 00:16:44,220
Practicing charity, giving money to the poor, is valuable.

232
00:16:44,220 --> 00:16:47,220
It's less valuable than, well, for example,

233
00:16:47,220 --> 00:16:50,220
paying homage to one who deserves it,

234
00:16:50,220 --> 00:16:55,220
but even paying homage to one who is worthy of homage

235
00:16:55,220 --> 00:16:59,220
is far less valuable than actually becoming worthy

236
00:16:59,220 --> 00:17:01,220
of respect to yourself.

237
00:17:01,220 --> 00:17:03,220
My teacher said that he said,

238
00:17:03,220 --> 00:17:06,220
going to see an enlightened being

239
00:17:06,220 --> 00:17:09,220
or paying homage to an enlightened being

240
00:17:09,220 --> 00:17:13,220
is not worth the smallest part of becoming

241
00:17:13,220 --> 00:17:15,220
practicing to become an enlightened being yourself.

242
00:17:15,220 --> 00:17:18,220
It's in a very famous talk that he gave

243
00:17:18,220 --> 00:17:22,220
on the force of Deepatana.

244
00:17:22,220 --> 00:17:28,220
But yeah, he made that very important statement.

245
00:17:28,220 --> 00:17:32,220
But yeah, it's good, but it's not worth the smallest part.

246
00:17:32,220 --> 00:17:37,220
It's far less good than actually practicing

247
00:17:37,220 --> 00:17:39,220
to become one yourself.

248
00:17:39,220 --> 00:17:41,220
So it's a matter of degrees.

249
00:17:41,220 --> 00:17:44,220
And that's something for us to keep in mind

250
00:17:44,220 --> 00:17:47,220
in our spiritual practice that some people

251
00:17:47,220 --> 00:17:49,220
will put a lot of emphasis on chanting.

252
00:17:49,220 --> 00:17:51,220
Some people put a lot of emphasis on charity

253
00:17:51,220 --> 00:17:53,220
or social work.

254
00:17:53,220 --> 00:17:56,220
And it's all about our priorities.

255
00:17:56,220 --> 00:17:59,220
Some people put a lot of emphasis on study.

256
00:17:59,220 --> 00:18:01,220
And in the end, we have to ask ourselves,

257
00:18:01,220 --> 00:18:02,220
what is our goal?

258
00:18:02,220 --> 00:18:05,220
And we have to do those things to engage

259
00:18:05,220 --> 00:18:07,220
and put primer emphasis on those things

260
00:18:07,220 --> 00:18:09,220
that lead us to that goal.

261
00:18:09,220 --> 00:18:11,220
For just giving and giving and giving,

262
00:18:11,220 --> 00:18:13,220
what is the purpose of that?

263
00:18:13,220 --> 00:18:17,220
If we're just chanting and chanting or this or that,

264
00:18:17,220 --> 00:18:20,220
what is the purpose of all these things?

265
00:18:20,220 --> 00:18:23,220
And on the other hand, if we do have a set purpose

266
00:18:23,220 --> 00:18:29,220
and we do things for that purpose,

267
00:18:29,220 --> 00:18:31,220
we have to differentiate between those things

268
00:18:31,220 --> 00:18:33,220
that actually lead to the purpose.

269
00:18:33,220 --> 00:18:36,220
But then there are wide array of things

270
00:18:36,220 --> 00:18:39,220
that can help us, that can actually be a benefit

271
00:18:39,220 --> 00:18:41,220
to help us realize our goals.

272
00:18:41,220 --> 00:18:44,220
But charity can be useful for meditation.

273
00:18:44,220 --> 00:18:47,220
Morality, of course, is essential for meditation.

274
00:18:47,220 --> 00:18:51,220
These kind of things study is also quite important,

275
00:18:51,220 --> 00:18:54,220
but putting them in context.

276
00:18:54,220 --> 00:18:57,220
So many religious practices have to be taken

277
00:18:57,220 --> 00:19:01,220
as a support rather than a main goal

278
00:19:01,220 --> 00:19:04,220
or a main practice, a main focus.

279
00:19:04,220 --> 00:19:06,220
Anyway, things to consider.

280
00:19:06,220 --> 00:19:09,220
It's just some of the ideas that arise

281
00:19:09,220 --> 00:19:12,220
based on this verse.

282
00:19:12,220 --> 00:19:15,220
And the importance of quality rather than quantity.

283
00:19:15,220 --> 00:19:17,220
You can do a lot, a lot of deeds,

284
00:19:17,220 --> 00:19:20,220
but it doesn't make any of them good or worthwhile.

285
00:19:20,220 --> 00:19:24,220
You can work for 100 years, work very, very hard

286
00:19:24,220 --> 00:19:26,220
and have nothing to show for it.

287
00:19:26,220 --> 00:19:29,220
Or meaningless things to show for it.

288
00:19:29,220 --> 00:19:32,220
Whereas you can just for one moment do the right thing

289
00:19:32,220 --> 00:19:35,220
and have it worse, far more,

290
00:19:35,220 --> 00:19:37,220
than those 100 years.

291
00:19:37,220 --> 00:19:40,220
Basically, what's being said, you're very useful teaching.

292
00:19:40,220 --> 00:19:42,220
Anyway, so that's the demo fata for tonight.

293
00:19:42,220 --> 00:20:08,220
Thank you for tuning in, wishing you all the best.

