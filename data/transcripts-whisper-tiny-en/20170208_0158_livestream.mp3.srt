1
00:00:00,000 --> 00:00:27,120
Okay, good evening, everyone, and welcome to our evening and I'm a session.

2
00:00:27,120 --> 00:00:39,040
I'd like to talk a little bit about some sara. Some sara is a word. I think it's one

3
00:00:39,040 --> 00:00:44,000
of the more commonly known words in Buddhism, even if you're not a Buddhist, you know, a little

4
00:00:44,000 --> 00:00:54,520
bit about Buddhism. You've probably heard this word before. We have in the in the

5
00:00:54,520 --> 00:01:04,520
Dhamapada. I think it's in the Dhamapada actually, I'm not sure.

6
00:01:04,520 --> 00:01:10,520
Dhamapada actually, I'm not sure. I think it's in the Dhamapada actually, I'm not sure.

7
00:01:10,520 --> 00:01:25,680
Not seeing these four noble truths, we have wandered on. In this law, we have wandered

8
00:01:25,680 --> 00:01:40,680
on for a long time. Dhamapada, I'll this long road and some sara, tasu tasu, tasu,

9
00:01:40,680 --> 00:01:58,160
from life to life, birth to birth. So some sara is the universe as we know it. Doesn't actually

10
00:01:58,160 --> 00:02:10,160
refer to the physical universe. There is no physical universe in Buddhism. It's not real. Physical

11
00:02:10,160 --> 00:02:16,560
space is something that is dependent on or derived from matter, which of course is still

12
00:02:16,560 --> 00:02:22,800
dependent on experience. So in a sense, when we, the world, the universe and all the planets

13
00:02:22,800 --> 00:02:29,880
and so on, it's only there because we experience it. You know, it only exists in the context

14
00:02:29,880 --> 00:02:45,720
of our experience. Quantum physics has something to say along those lines, I think. But

15
00:02:45,720 --> 00:02:56,800
you know what samsara is in Buddhism is this rebirth, the path of the individual, the

16
00:02:56,800 --> 00:03:10,520
path of beings, birth and death, really. And some sara is really all about being born again.

17
00:03:10,520 --> 00:03:22,920
Some sara means when you die, you start all over, or you continue on in whatever way.

18
00:03:22,920 --> 00:03:27,480
And so we talk about the many realms of rebirth. There are the 31 realms in terabyte of

19
00:03:27,480 --> 00:03:33,640
Buddhism, but I think that's just a convention. My guess is you can subdivide into many,

20
00:03:33,640 --> 00:03:40,640
many more realms. The hell realms, for example, there's many, many, many different hells that

21
00:03:40,640 --> 00:03:46,480
one can go to. But at the top, you have the Brahma realm, which if you practice somewhat

22
00:03:46,480 --> 00:03:53,800
of meditation, you go there. Below that, you have the angel realms, the day water realms,

23
00:03:53,800 --> 00:03:59,520
where if you do lots of super good deeds, you're a very kind and gentle and generous person

24
00:03:59,520 --> 00:04:10,200
and moral and ethical. You go to these heavens, then we have the human realms. If you're

25
00:04:10,200 --> 00:04:17,640
reasonably ethical and moral and good, you'll be born to the human. And the animal realms,

26
00:04:17,640 --> 00:04:21,760
where if you're full of delusion, you'll go there. We have the ghost realms. If you're

27
00:04:21,760 --> 00:04:30,440
full of greed, you'll go there. We have the hell realms, where if you're really, if you're

28
00:04:30,440 --> 00:04:45,840
full of anger, fear, spite, hatred, malice, all this, you'll go to hell. It's in a conventional

29
00:04:45,840 --> 00:04:58,920
sense. This is what samsara means. We wander around aimlessly, really, you know, we have

30
00:04:58,920 --> 00:05:06,960
goals and aims from time to time, but they're short-sighted and meaningless in the larger

31
00:05:06,960 --> 00:05:12,720
context of samsara, of our wandering from life to life. So sometimes we're angels, sometimes

32
00:05:12,720 --> 00:05:20,280
we're gods, often we're humans, and often we're animals, ghosts, or even sometimes

33
00:05:20,280 --> 00:05:36,320
born in hell. At somewhat of a foreign paradigm, I think, for the modern world, so you become

34
00:05:36,320 --> 00:05:43,000
accustomed to thinking in terms of the physical world and its conceptual world, really, where

35
00:05:43,000 --> 00:05:50,200
we conceive of space and time, and we have all sorts of formulas and theories about space

36
00:05:50,200 --> 00:06:03,080
time. In fact, both space and time are not quite what we think they are. We'll not really

37
00:06:03,080 --> 00:06:11,320
what we think they are at all. Space is just a derivation of matter, and time is really

38
00:06:11,320 --> 00:06:16,640
just about the changes inherent in the present moment, which is the only time that really

39
00:06:16,640 --> 00:06:32,640
exists. So if you become familiar with this experiential paradigm of being reborn

40
00:06:32,640 --> 00:06:40,520
again and again, this way of looking at the world, it really puts everything into perspective

41
00:06:40,520 --> 00:06:45,760
in a way that many of us don't think. I think for a lot of people it goes well beyond

42
00:06:45,760 --> 00:06:50,160
their reasons for practicing meditation. You don't find many people nowadays saying,

43
00:06:50,160 --> 00:06:54,600
hey, I'm going to go meditate, so I can be free from all the many realms of rebirth. Most

44
00:06:54,600 --> 00:07:00,160
of us don't even have a conception of them. They either were other realms or being reborn

45
00:07:00,160 --> 00:07:14,000
even at all, perhaps. Nonetheless, it being the nature of reality is understood by Buddhists

46
00:07:14,000 --> 00:07:21,200
and by the Buddha. It's useful for us to think of it as the larger picture in the conventional

47
00:07:21,200 --> 00:07:26,640
sense. It's useful because it helps you. I really understand the depth of our practice

48
00:07:26,640 --> 00:07:32,400
that, yes, it's about stress reduction, yes, it's about overcoming our defilements, but ultimately

49
00:07:32,400 --> 00:07:42,120
there's a broader context that we've been traveling, we've been wandering on at infinitum.

50
00:07:42,120 --> 00:07:57,080
I didn't infinitum, but no end in sight, no beginning in sight. I don't know how long we've

51
00:07:57,080 --> 00:08:12,960
been going, perhaps it's eternal. In the end, it becomes meaningless. So you realize that all

52
00:08:12,960 --> 00:08:20,800
of this is quite exciting, really, to think about all the many realms you can be born

53
00:08:20,800 --> 00:08:29,560
in. If it's something new, but as you consider, as you learn and as you meditate, you

54
00:08:29,560 --> 00:08:40,560
start to see that it's all just wandering on. Meaningless, pointless, useless. But again,

55
00:08:40,560 --> 00:08:46,560
I think that goes far beyond what most people think about when they undertake the practice

56
00:08:46,560 --> 00:08:52,600
of meditation. Unfortunately, you don't have to think that being something that's profound

57
00:08:52,600 --> 00:09:00,560
on the level of a Buddha to think about these things and to really realize that this

58
00:09:00,560 --> 00:09:06,200
is enough, that they've had enough with some sara. But I'm sorry, I mean, that's just

59
00:09:06,200 --> 00:09:12,280
the conceptual nature. In reality, some sara, if you want to go deeper, it's just the

60
00:09:12,280 --> 00:09:18,400
cycle. So we often hear about the cycle of birth, all-day sickness and death. That's what

61
00:09:18,400 --> 00:09:23,720
we're talking about. That no matter where we go, there is always going to be this being

62
00:09:23,720 --> 00:09:31,560
starting on the new journey and going through the motions of learning and adapting and

63
00:09:31,560 --> 00:09:40,880
clinging and settling down in some sort of measure of stability, only to be uprooted again

64
00:09:40,880 --> 00:09:58,520
at the moment of death and be cast out and sent on our way to wander again. And so if we

65
00:09:58,520 --> 00:10:04,080
understand, if we want to understand in a much easier way to understand it is this way

66
00:10:04,080 --> 00:10:09,880
in terms of one life, it's still conceptual, but much more useful, which more useful to

67
00:10:09,880 --> 00:10:16,600
think about what it means to be alive, no matter where we're born, what is it, what is

68
00:10:16,600 --> 00:10:22,280
involved? And we look and we see we're making the same mistakes again and again. If this

69
00:10:22,280 --> 00:10:26,920
has been infinite, if we're doing this forever, it's really quite shameful that we're

70
00:10:26,920 --> 00:10:35,920
still making so many mistakes. And it's certainly a sign that this isn't the ideal.

71
00:10:35,920 --> 00:10:49,160
This can't be as good as it gets. But if we really want to understand some sort, of

72
00:10:49,160 --> 00:10:55,280
course, we have to go beyond concepts. And so the ultimate way, the third way we can understand

73
00:10:55,280 --> 00:11:06,680
some sorrows in terms of a moment-to-moment wandering on of sorts, this mindless zombie-like

74
00:11:06,680 --> 00:11:12,520
state that we find ourselves in, where we go through the motions of indulging and clinging

75
00:11:12,520 --> 00:11:22,320
and judging and avoiding and running and chasing, the good and the bad, out of delusion

76
00:11:22,320 --> 00:11:31,480
and out of blindness, not seeing clearly, because we don't see the formal truths that

77
00:11:31,480 --> 00:11:37,480
we don't have to even think about being reborn or going to this realm or that realm, ultimately

78
00:11:37,480 --> 00:11:44,480
it's all just experience. No matter where we go, we're not going to be free from the senses

79
00:11:44,480 --> 00:11:58,120
from experience. And so this is really what we had come to understand through the meditation

80
00:11:58,120 --> 00:12:06,280
that the reality has just made up of experiences and experiences continue on and on and

81
00:12:06,280 --> 00:12:11,880
we wander on and on. We wander through experiences without any real rhyme or reason. So

82
00:12:11,880 --> 00:12:17,320
you start to see in meditation, right, that, oh, I'm wandering around like a lost person

83
00:12:17,320 --> 00:12:30,880
and you start to cultivate this sense of direction, a compass, a way to go, a path, and

84
00:12:30,880 --> 00:12:36,800
you start to see the right way, the right path, not as going here or going there, but

85
00:12:36,800 --> 00:12:43,440
it's stopping, it's not wandering anymore. It's heading in the right direction, if you

86
00:12:43,440 --> 00:12:54,560
will, in the sense of actually not going anywhere. And so we focus on the five aggregates,

87
00:12:54,560 --> 00:12:59,040
the four setipatana, the six senses however you want to describe it, but we focus on

88
00:12:59,040 --> 00:13:05,040
experience and so our act of being objective of reminding ourselves and of learning to

89
00:13:05,040 --> 00:13:13,000
see clearly. This is about really understanding, well, seeing through the concept of

90
00:13:13,000 --> 00:13:21,480
some sara entirely. Just see that what we mean by some, what we call some sara is just

91
00:13:21,480 --> 00:13:29,920
our own delusions and our own conceptions of our experiences, which turn out to be impermanent

92
00:13:29,920 --> 00:13:40,520
and unsatisfying and controllable, not belonging to us, not being amenable to our wishes,

93
00:13:40,520 --> 00:13:49,960
our expectations. And we see that we're lost at our ambitions and our drives and our wants

94
00:13:49,960 --> 00:13:57,480
and our wishes are all merely a cause for more stress and suffering. And so we start to settle

95
00:13:57,480 --> 00:14:05,640
down. You don't have to worry about leaving some sara. Some sara just leaves you as you let

96
00:14:05,640 --> 00:14:15,680
go of it. Some sara leaves you. That's the truth. Become less and less inclined to pull

97
00:14:15,680 --> 00:14:24,360
it in, to take it in, to build it up. Less and less inclined to create some sara around

98
00:14:24,360 --> 00:14:37,000
you until eventually you stop. You cease. You liberate yourself. And there you go. There's

99
00:14:37,000 --> 00:14:43,400
the demo for tonight. A little bit about some sara. Thank you all for tuning in. Have a good

100
00:14:43,400 --> 00:15:00,240
day.

101
00:15:00,240 --> 00:15:03,240
.

102
00:15:03,240 --> 00:15:08,240
.

103
00:15:08,240 --> 00:15:11,240
.

104
00:15:11,240 --> 00:15:16,240
.

105
00:15:16,240 --> 00:15:19,240
..

106
00:15:19,240 --> 00:15:24,240
..

107
00:15:24,240 --> 00:15:28,240
..

108
00:15:28,240 --> 00:15:29,240
..

109
00:15:29,240 --> 00:15:56,000
If you have questions, I'll be just stick around, it's been a long day, so please don't

110
00:15:56,000 --> 00:16:03,000
get me with anything hard. I was teaching meditation, five minute meditation lessons,

111
00:16:03,000 --> 00:16:32,000
I can't comprehend it with my low brain power tonight. I didn't have my juice this evening.

112
00:16:32,000 --> 00:16:38,000
Should the foot be placed flat on the ground or in the steps?

113
00:16:38,000 --> 00:16:46,000
For the first step, there are different levels stages to the walking step, but the first step,

114
00:16:46,000 --> 00:16:53,000
stepping right, stepping left, it should be put down flat. Eventually we get onto the second step,

115
00:16:53,000 --> 00:17:04,000
the fourth step, but the time you get to the fifth and the sixth step, there's stages to it.

116
00:17:04,000 --> 00:17:19,000
But to get there, you have to do a course. You can do an online course, and I can teach you all the steps.

117
00:17:19,000 --> 00:17:27,000
Okay, I used up the last of my brain power trying to figure out that question, which admittedly wasn't much.

118
00:17:27,000 --> 00:17:34,000
So I'm going to say goodnight. Oh, one more, okay. Is it possible to be mindful in your sleep? No, not really.

119
00:17:34,000 --> 00:17:41,000
I don't even mean maybe technically, but I wouldn't rely on it.

120
00:17:41,000 --> 00:17:48,000
Goodnight, everyone. See you all Friday because of course tomorrow and Thursday I'm not here.

121
00:17:48,000 --> 00:17:51,000
So be well.

