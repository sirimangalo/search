1
00:00:00,000 --> 00:00:10,000
If one has a preference for solitude, should that person make some effort to spend time in community in order to let go of partiality?

2
00:00:10,000 --> 00:00:14,000
We've got to love the tricky questions, no?

3
00:00:14,000 --> 00:00:21,000
As we always talk about, it's actually a part of the whole question of what about the desire to meditate, right?

4
00:00:21,000 --> 00:00:25,000
You have the desire to meditate, so how can you ever become free from desire?

5
00:00:25,000 --> 00:00:28,000
But this is a little bit different.

6
00:00:28,000 --> 00:00:35,000
You really have to break that question apart in order to answer it.

7
00:00:35,000 --> 00:00:39,000
So we're talking about what we're talking about preference is preferences.

8
00:00:39,000 --> 00:00:48,000
And it is quite common for people to have, I would say in this case, a version to society,

9
00:00:48,000 --> 00:01:01,000
for whatever reason, sometimes they feel shy, sometimes they feel annoyed by other people,

10
00:01:01,000 --> 00:01:09,000
sometimes they feel nervous or scared or whatever, for many different reasons people will often prefer seclusion.

11
00:01:09,000 --> 00:01:15,000
So we have this problem because the Buddha said seclusion is a good thing.

12
00:01:15,000 --> 00:01:23,000
He said many times would praise seclusion as ideal.

13
00:01:23,000 --> 00:01:29,000
So here we have a question, what about these people who are addicted to seclusion?

14
00:01:29,000 --> 00:01:34,000
Shouldn't they then adjust themselves so that they're no longer attached to seclusion?

15
00:01:34,000 --> 00:01:40,000
Or what about monks who run off into the forest and don't want to be around people?

16
00:01:40,000 --> 00:01:49,000
So my answer for this would be, no, that they should still stay in seclusion.

17
00:01:49,000 --> 00:01:56,000
And the point is regardless of what your attachment is, whether it's an aversion or whether it's an addiction,

18
00:01:56,000 --> 00:01:59,000
it's something that arises in the mind.

19
00:01:59,000 --> 00:02:05,000
The problem exists within yourself, it doesn't have anything to do with people, it doesn't have anything to do with society,

20
00:02:05,000 --> 00:02:11,000
or the situations that you're in, it has to do with how your mind works, how your mind reacts to things.

21
00:02:11,000 --> 00:02:17,000
And in fact, how your mind reacts to moment to moment experiences, which occur wherever you are,

22
00:02:17,000 --> 00:02:26,000
whether you're alone or whether you're with other people, these states are the same in every situation.

23
00:02:26,000 --> 00:02:39,000
So you don't have to experience every situation or every life experience in order to understand reality.

24
00:02:39,000 --> 00:02:45,000
I've had some monks tell me that they have to disrobe because they don't have enough life experience.

25
00:02:45,000 --> 00:02:52,000
And so they want to go and drink alcohol and have girlfriends and so on, because they never had the chance.

26
00:02:52,000 --> 00:02:55,000
And so how can they understand, how can they fully appreciate suffering?

27
00:02:55,000 --> 00:03:04,000
If they don't go out and experience, it's a wonderful excuse, these are the kind of people that should be lawyers.

28
00:03:04,000 --> 00:03:15,000
But the truth is that we're talking about the way the mind works,

29
00:03:15,000 --> 00:03:21,000
the way the brain works, the way it experiences seeing, the way it reacts to seeing, hearing, smelling, tasting, feeling and thinking.

30
00:03:21,000 --> 00:03:23,000
And it does this on a moment to moment basis.

31
00:03:23,000 --> 00:03:36,000
The reason why the Buddha preferred solitude is or encouraged solitude is not because he was partial or a virtue to society,

32
00:03:36,000 --> 00:03:41,000
or he thought that people should cultivate some kind of partiality towards seclusion.

33
00:03:41,000 --> 00:03:52,000
But it's because the only reasonable way that most people can come to look at their own minds is to be secluded from other human beings.

34
00:03:52,000 --> 00:04:02,000
Because when you're around other people, rather than look at things normally, a person who hasn't cultivated their mind,

35
00:04:02,000 --> 00:04:08,000
will forget themselves immediately, will begin to wonder what the other person thinks.

36
00:04:08,000 --> 00:04:10,000
And this is why we hate it so much.

37
00:04:10,000 --> 00:04:18,000
This is why in general, or not in general, this is why many people are horrified to go out in public.

38
00:04:18,000 --> 00:04:24,000
More and more these days, in fact, as we become more accustomed to using the internet and staying by ourselves,

39
00:04:24,000 --> 00:04:34,000
we become less and less able to survive in this environment of having to react and having to be smart and funny,

40
00:04:34,000 --> 00:04:44,000
and having to be socially skilled, to be able to converse with other people,

41
00:04:44,000 --> 00:04:51,000
to be able to empathize with other people, and to interact with other people.

42
00:04:51,000 --> 00:04:56,000
So you're dealing on a very conceptual level, and there's a lot of ego involved in conceit,

43
00:04:56,000 --> 00:05:03,000
conceit thinking, I'm better, I'm worse, and I'm equal, and so on, holding yourself up, judging yourself against others.

44
00:05:03,000 --> 00:05:13,000
There's jealousy, there's many unwholesome states, but there's also, in general, a awareness of only concepts,

45
00:05:13,000 --> 00:05:21,000
as beings and people and possessions that I possess this state and that state.

46
00:05:21,000 --> 00:05:27,000
So regardless of what the attachment is, even if it's an attachment to meditation,

47
00:05:27,000 --> 00:05:29,000
here's one good answer to that one.

48
00:05:29,000 --> 00:05:36,000
If we desire to meditate, should we then stop meditating, or doesn't that mean that the meditation is pointless?

49
00:05:36,000 --> 00:05:40,000
No, because any desire that you have will be rooted out through meditation,

50
00:05:40,000 --> 00:05:44,000
even if it's a desire for meditation, if such a thing could exist.

51
00:05:44,000 --> 00:05:48,000
Even if you have this lust for meditating, once you sit down and meditate,

52
00:05:48,000 --> 00:05:53,000
you'll be able to see that lust, and you'll see that the lust is not useful for you, and you'll let go of it,

53
00:05:53,000 --> 00:05:58,000
but you'll only get that if you practice meditation, and not the only way to meditate,

54
00:05:58,000 --> 00:06:02,000
but the best way to practice meditation is in seclusion.

55
00:06:02,000 --> 00:06:05,000
So if you have attachment to seclusion by all means,

56
00:06:05,000 --> 00:06:10,000
you'll see yourself until you learn about that attachment, until you understand how your mind works,

57
00:06:10,000 --> 00:06:14,000
and you'll see that as a result of spending time on a mountain,

58
00:06:14,000 --> 00:06:19,000
if you're truly dedicated towards to insight meditation,

59
00:06:19,000 --> 00:06:26,000
you'll find yourself much better, much more confident and much more sure of yourself in social situations.

60
00:06:26,000 --> 00:06:29,000
You'll talk less, you'll be quiet, people will think you're weird,

61
00:06:29,000 --> 00:06:34,000
they might even be afraid of you, and you might not fit in at all in certain social situations,

62
00:06:34,000 --> 00:06:40,000
but you'll never be worried or attached or aversive towards it.

63
00:06:40,000 --> 00:06:46,000
You'll never have any desire for it, and you will incline much more towards seclusion.

64
00:06:46,000 --> 00:06:48,000
It's just being the default.

65
00:06:48,000 --> 00:06:53,000
You'll never have any desire to be around people or any feelings of loneliness,

66
00:06:53,000 --> 00:06:58,000
as a result of seeing things clearly and letting go of the idea of self.

67
00:06:58,000 --> 00:07:04,000
Of course, the biggest thing is that it helps you let go of the ego, the idea that there is an eye,

68
00:07:04,000 --> 00:07:08,000
and us and them, and me, and mine, and so on.

69
00:07:08,000 --> 00:07:15,000
And so for sure, even the attachment to seclusion can be rooted out through proper seclusion.

70
00:07:15,000 --> 00:07:18,000
Of course, I should let Paul and Janie talk.

71
00:07:18,000 --> 00:07:23,000
I'm sure she has something to say, but just to go the other way,

72
00:07:23,000 --> 00:07:32,000
that it is possible to be in seclusion and actually become more corrupt,

73
00:07:32,000 --> 00:07:34,000
or to become more attached.

74
00:07:34,000 --> 00:07:39,000
It's possible for someone to go into seclusion and fail at this.

75
00:07:39,000 --> 00:07:43,000
If you're not practicing in sight meditation, then there is no reason to go into solitude.

76
00:07:43,000 --> 00:07:50,000
A person who goes into solitude and finds all sorts of wonderful experiences of solitude

77
00:07:50,000 --> 00:07:55,000
will become even more averse to being around people or not will,

78
00:07:55,000 --> 00:08:04,000
but can, and it's possible that they can have a very lopsided view of reality.

79
00:08:04,000 --> 00:08:08,000
And they'll find that they can only be happy when they're in solitude.

80
00:08:08,000 --> 00:08:14,000
But with the qualification that if they're practicing in sight meditation,

81
00:08:14,000 --> 00:08:16,000
then the best thing they can do is seclude themselves

82
00:08:16,000 --> 00:08:21,000
and find the time and the place to develop in sight meditation

83
00:08:21,000 --> 00:08:24,000
can be practiced in any situation,

84
00:08:24,000 --> 00:08:29,000
but is best practiced when you are just in your room alone

85
00:08:29,000 --> 00:08:32,000
with your mind learning about it.

86
00:08:32,000 --> 00:08:36,000
No, nothing more to say on that.

87
00:08:36,000 --> 00:08:46,000
Sorry, I tried to cover too much.

