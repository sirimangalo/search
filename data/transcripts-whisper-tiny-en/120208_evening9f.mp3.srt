1
00:00:00,000 --> 00:00:24,000
Okay, so last night we talked about the benefits of the goals of the practice of meditation.

2
00:00:24,000 --> 00:00:32,000
It wasn't pretty lofty aim as well.

3
00:00:32,000 --> 00:00:53,000
Pretty hefty goals to aspire towards the purification of the mind, overcoming all mental development.

4
00:00:53,000 --> 00:00:56,000
So the next question is, well, what do we do?

5
00:00:56,000 --> 00:01:05,000
What can we do or how do we progress to attain these goals?

6
00:01:05,000 --> 00:01:08,000
What do we mean by progress?

7
00:01:08,000 --> 00:01:12,000
How do we measure our progress?

8
00:01:12,000 --> 00:01:20,000
Just a question that meditators always want an answer to.

9
00:01:20,000 --> 00:01:24,000
I have to tell how far we progress.

10
00:01:24,000 --> 00:01:27,000
Am I on the right track?

11
00:01:27,000 --> 00:01:46,000
Am I doing well?

12
00:01:46,000 --> 00:01:57,000
Well, if we want to know how successful we are, we can look at the goals and see how close we are to the goals.

13
00:01:57,000 --> 00:02:01,000
But sometimes it's not clear. Sometimes we don't know.

14
00:02:01,000 --> 00:02:11,000
We can't see how much purity and how much defilement we have in the mind.

15
00:02:11,000 --> 00:02:19,000
And even if we can see, we still have to know how to go forward, how to move closer to the goal.

16
00:02:19,000 --> 00:02:21,000
What do we do that?

17
00:02:21,000 --> 00:02:25,000
What is it that we do that brings?

18
00:02:25,000 --> 00:02:36,000
That's closer to the goal.

19
00:02:36,000 --> 00:02:47,000
So we have to understand the path, we have to understand the path of progress.

20
00:02:47,000 --> 00:02:54,000
One way of explaining and understanding the Buddha's teaching the path of the Buddha is

21
00:02:54,000 --> 00:03:06,000
coming closer to the core of existence or the essential quality of existence.

22
00:03:06,000 --> 00:03:15,000
Moving away from that which is unessential.

23
00:03:15,000 --> 00:03:25,000
And trying to attain the essence.

24
00:03:25,000 --> 00:03:35,000
The meaning is that there's a great amount in this world that is unessential,

25
00:03:35,000 --> 00:03:42,000
or another way of putting it is leading nowhere, leads to no end,

26
00:03:42,000 --> 00:03:49,000
leads to no benefit.

27
00:03:49,000 --> 00:03:55,000
This is the path of addiction, craving more and more,

28
00:03:55,000 --> 00:04:00,000
chasing after central desires, central pleasures.

29
00:04:00,000 --> 00:04:04,000
It leads us on and on and on and builds up.

30
00:04:04,000 --> 00:04:09,000
We can build up forever for as long as we can build it up.

31
00:04:09,000 --> 00:04:19,000
Until finally we add some point, can't attain what would we strive for.

32
00:04:19,000 --> 00:04:23,000
And then it all comes crashing down.

33
00:04:23,000 --> 00:04:31,000
And we can build and build and build it.

34
00:04:31,000 --> 00:04:34,000
All of the development work that we do in the world to build the world,

35
00:04:34,000 --> 00:04:43,000
and build society, all the same.

36
00:04:43,000 --> 00:04:45,000
So what is it that's essential?

37
00:04:45,000 --> 00:05:00,000
Even living a life of a monk, living a life of poverty and chastity and renunciation of simplicity.

38
00:05:00,000 --> 00:05:04,000
Even still we have to wake up in the morning.

39
00:05:04,000 --> 00:05:15,000
We have to go into the village for food every day, every day.

40
00:05:15,000 --> 00:05:20,000
To no end, this is not the essential.

41
00:05:20,000 --> 00:05:26,000
Some people think becoming a monk, and this is the reach, the goal of the reach, the pinnacle of existence.

42
00:05:26,000 --> 00:05:31,000
But you really haven't.

43
00:05:31,000 --> 00:05:34,000
This isn't the essence.

44
00:05:34,000 --> 00:05:37,000
The most light is just a light here.

45
00:05:37,000 --> 00:05:43,000
Still have to wear clothes, you still have to go for food, just to eat, and use the washroom.

46
00:05:43,000 --> 00:05:47,000
You still get a little second back.

47
00:05:47,000 --> 00:05:52,000
So to evolve of existence, all of life, what is the essence?

48
00:05:52,000 --> 00:05:55,000
What is it that is essential?

49
00:05:55,000 --> 00:06:02,000
You would have gave us very clear teaching on what is essential.

50
00:06:02,000 --> 00:06:08,000
It's very much a core of the Buddha's teaching that we hear about quite often.

51
00:06:08,000 --> 00:06:12,000
Five things.

52
00:06:12,000 --> 00:06:16,000
Morality, this is essential.

53
00:06:16,000 --> 00:06:21,000
Concentration or focus, this is essential.

54
00:06:21,000 --> 00:06:24,000
Wisdom is essential.

55
00:06:24,000 --> 00:06:32,000
And liberation, and the knowledge of liberation.

56
00:06:32,000 --> 00:06:36,000
These five things are essential.

57
00:06:36,000 --> 00:06:41,000
This is what we understand to be the essence of existence.

58
00:06:41,000 --> 00:06:49,000
I think it goes back to what we were talking about when I talked about earlier about how.

59
00:06:49,000 --> 00:06:53,000
When your mind is pure, when you develop in this way,

60
00:06:53,000 --> 00:06:56,000
because these are things that anyone can have.

61
00:06:56,000 --> 00:07:06,000
It's not a specifically Buddhist or a monastic part.

62
00:07:06,000 --> 00:07:13,000
It's not specific to anyone group of people or any type of person.

63
00:07:13,000 --> 00:07:18,000
It's the essence and everything else is essential.

64
00:07:18,000 --> 00:07:26,000
You can be among, you can be a lay person to find the essence of existence.

65
00:07:26,000 --> 00:07:30,000
And to put it another way to find success in the practice

66
00:07:30,000 --> 00:07:35,000
and to find to reach the goals that we talked about yesterday.

67
00:07:35,000 --> 00:07:40,000
To find true peace and happiness and freedom from suffering.

68
00:07:40,000 --> 00:07:43,000
When you do these five things.

69
00:07:43,000 --> 00:07:48,000
So first, we need morality with this morality.

70
00:07:48,000 --> 00:07:52,000
On the one hand, it means we have to keep precepts.

71
00:07:52,000 --> 00:07:55,000
We have to at least keep the five precepts.

72
00:07:55,000 --> 00:08:02,000
Better if we can keep the eight precepts, or even the ten, or the monastic precepts.

73
00:08:02,000 --> 00:08:06,000
This is a way of ordering our minds, ordering our lives.

74
00:08:06,000 --> 00:08:12,000
To keep ourselves out of trouble, so we don't say or do bad things.

75
00:08:12,000 --> 00:08:14,000
But they're really just pencils.

76
00:08:14,000 --> 00:08:16,000
The rules are really just pencils.

77
00:08:16,000 --> 00:08:19,000
It's up to us to fill in the gaps.

78
00:08:19,000 --> 00:08:24,000
If you just put pencils down, you can't keep animals in.

79
00:08:24,000 --> 00:08:30,000
You can't keep anything coralda.

80
00:08:30,000 --> 00:08:33,000
You need a fence.

81
00:08:33,000 --> 00:08:36,000
The rules are not a fence.

82
00:08:36,000 --> 00:08:43,000
No matter how many rules you take or practices are even taking the ascetic practices.

83
00:08:43,000 --> 00:08:45,000
These are just fence posts.

84
00:08:45,000 --> 00:08:54,000
They don't really stop you from doing and saying bad things or things that you regret later.

85
00:08:54,000 --> 00:08:57,000
The fence is the fence of mindfulness.

86
00:08:57,000 --> 00:09:02,000
So everything starts with mindfulness when you walk and know that you're walking,

87
00:09:02,000 --> 00:09:07,000
when you talk and know that you're talking, you know, aware of what's going on in your body,

88
00:09:07,000 --> 00:09:12,000
and with your speech and in your mind.

89
00:09:12,000 --> 00:09:15,000
This is where more true morality starts.

90
00:09:15,000 --> 00:09:18,000
When you catch yourself wanting to say this,

91
00:09:18,000 --> 00:09:21,000
wanting to say that, wanting to do this, wanting to do that.

92
00:09:21,000 --> 00:09:24,000
Can you stop yourself?

93
00:09:24,000 --> 00:09:27,000
And eventually clarify your mind to mindfulness,

94
00:09:27,000 --> 00:09:39,000
to the point where you don't want to do or say things that are source of regret.

95
00:09:39,000 --> 00:09:43,000
So even just walking and meditation, we're developing morality,

96
00:09:43,000 --> 00:09:49,000
we're changing our behavior, our character,

97
00:09:49,000 --> 00:09:58,000
doing away with all sorts of bad character traits.

98
00:09:58,000 --> 00:10:03,000
All of the things that we know in our lives that we regret or that we feel,

99
00:10:03,000 --> 00:10:04,000
we don't like about ourselves.

100
00:10:04,000 --> 00:10:07,000
We do away with them in the meditation practice.

101
00:10:07,000 --> 00:10:10,000
We make our minds clearer and calmer,

102
00:10:10,000 --> 00:10:15,000
do away a piece by piece with anger and greed and delusion.

103
00:10:15,000 --> 00:10:23,000
So we don't do things that are regrettable or say things that we regret.

104
00:10:23,000 --> 00:10:25,000
This is morality.

105
00:10:25,000 --> 00:10:26,000
Focus.

106
00:10:26,000 --> 00:10:33,000
Focus means you can practice training your mind to focus on a single object,

107
00:10:33,000 --> 00:10:37,000
to fix your mind on something.

108
00:10:37,000 --> 00:10:41,000
But this is a kind of artificial focus,

109
00:10:41,000 --> 00:10:46,000
artificial concentration because everything else is excluded.

110
00:10:46,000 --> 00:10:50,000
It can be useful in the beginning, but eventually we want to be able to be in focus all the time.

111
00:10:50,000 --> 00:10:58,000
So our mind is in clear focus no matter what we do, no matter what we encounter.

112
00:10:58,000 --> 00:11:00,000
And we gain this through meditation as well,

113
00:11:00,000 --> 00:11:02,000
and we're walking and know that the foot is moving,

114
00:11:02,000 --> 00:11:05,000
we're clearly focused on the foot.

115
00:11:05,000 --> 00:11:08,000
And we teach ourselves to be clearly focused.

116
00:11:08,000 --> 00:11:15,000
So there's no arising of liking, disliking, drowsiness, distraction, death.

117
00:11:15,000 --> 00:11:18,000
There are no bad thoughts here.

118
00:11:18,000 --> 00:11:23,000
We don't have to talk anymore about doing or saying bad things with a focused mind,

119
00:11:23,000 --> 00:11:27,000
when your mind sees clearly everything,

120
00:11:27,000 --> 00:11:35,000
every experience that doesn't even arise greed or anger or delusion in the mind.

121
00:11:35,000 --> 00:11:54,000
But it's not possible for there to arise bad speech or bad deeds.

122
00:11:54,000 --> 00:11:57,000
And the mind is clear.

123
00:11:57,000 --> 00:12:02,000
We get there simply for walking and from sitting when you watch the stomach rising,

124
00:12:02,000 --> 00:12:05,000
and falling, and you know the rising and the falling.

125
00:12:05,000 --> 00:12:09,000
When you teach yourself this way of interacting with reality,

126
00:12:09,000 --> 00:12:15,000
with everything, with your thoughts, with your emotions, with your feelings,

127
00:12:15,000 --> 00:12:24,000
with things you hear and see and taste and smell, and feel and think.

128
00:12:24,000 --> 00:12:33,000
And you purify your mind and your mind becomes clear and set on clarity.

129
00:12:33,000 --> 00:12:39,000
When this happens, it leads to the third one is wisdom.

130
00:12:39,000 --> 00:12:44,000
Development of wisdom, you can read books or you can listen to talks,

131
00:12:44,000 --> 00:12:49,000
and you can gain some sort of intellectual appreciation of the truth.

132
00:12:49,000 --> 00:12:55,000
It's not the same. You can see it's not the same as experiencing it for yourself.

133
00:12:55,000 --> 00:12:59,000
When you experience it for yourself, you realize that all of the book learning,

134
00:12:59,000 --> 00:13:06,000
all of the study that you've done was something like the peel of the apple.

135
00:13:06,000 --> 00:13:17,000
It's such a very small and meaningless or insignificant part of the apple and the truth.

136
00:13:17,000 --> 00:13:22,000
It's part of wisdom.

137
00:13:22,000 --> 00:13:29,000
When you realize the true wisdom that comes from meditation practice,

138
00:13:29,000 --> 00:13:35,000
then you understand the difference.

139
00:13:35,000 --> 00:13:37,000
So how does this come in meditation?

140
00:13:37,000 --> 00:13:43,000
When you see things clearly you change the way you look at

141
00:13:43,000 --> 00:13:47,000
and all objects of experience.

142
00:13:47,000 --> 00:13:50,000
The things that you like used to like and were attached to,

143
00:13:50,000 --> 00:13:52,000
you see how meaningless it was.

144
00:13:52,000 --> 00:13:57,000
There was nothing there of any value of any significant.

145
00:13:57,000 --> 00:14:01,000
The things that you were partial towards,

146
00:14:01,000 --> 00:14:07,000
there's nothing intrinsically better about that.

147
00:14:07,000 --> 00:14:14,000
I'm trying to claim to attract it about that.

148
00:14:14,000 --> 00:14:20,000
You realize how much suffering comes from clinging to things that are insubstantial.

149
00:14:20,000 --> 00:14:23,000
The things that you were averse towards are afraid of.

150
00:14:23,000 --> 00:14:26,000
You come to see that there's nothing to be afraid of.

151
00:14:26,000 --> 00:14:29,000
There's nothing there to be afraid of.

152
00:14:29,000 --> 00:14:31,000
And there's nothing here to be afraid.

153
00:14:31,000 --> 00:14:36,000
There's no eye that we have to protect.

154
00:14:36,000 --> 00:14:39,000
When we have pain and we don't like the pain,

155
00:14:39,000 --> 00:14:43,000
we come to see that there's no eye who is receiving the pain.

156
00:14:43,000 --> 00:14:47,000
There's no being who is feeling the pain.

157
00:14:47,000 --> 00:14:51,000
There's nothing to avoid.

158
00:14:51,000 --> 00:14:56,000
There's nothing being afflicted by the pain.

159
00:14:56,000 --> 00:14:58,000
It's just arising and ceasing.

160
00:14:58,000 --> 00:15:00,000
It's an experience.

161
00:15:00,000 --> 00:15:03,000
Given that there's nothing affecting,

162
00:15:03,000 --> 00:15:13,000
we're not truly losing any idea of being angry at a drug center.

163
00:15:13,000 --> 00:15:16,000
In brief, you come to see impermanence

164
00:15:16,000 --> 00:15:20,000
that everything inside of you and in the world around you is changing.

165
00:15:20,000 --> 00:15:25,000
It's not substantial.

166
00:15:25,000 --> 00:15:27,000
It has no essence.

167
00:15:27,000 --> 00:15:31,000
You see that everything inside of you and in the world around you is suffering.

168
00:15:31,000 --> 00:15:36,000
It's not painful in and of itself,

169
00:15:36,000 --> 00:15:40,000
but when you cling to it, it's like fire.

170
00:15:40,000 --> 00:15:44,000
When you try to adjust it or fix it or make it

171
00:15:44,000 --> 00:15:52,000
or change it in any way, you only meet with stress and aggravation.

172
00:15:52,000 --> 00:15:57,000
And then everything inside of us in the world around us is uncontrollable.

173
00:15:57,000 --> 00:15:59,000
It doesn't belong to us.

174
00:15:59,000 --> 00:16:01,000
It isn't an entity or self.

175
00:16:01,000 --> 00:16:04,000
It doesn't belong to any entity or self.

176
00:16:04,000 --> 00:16:06,000
It's just experience.

177
00:16:06,000 --> 00:16:11,000
It arises and ceases. It comes and goes.

178
00:16:11,000 --> 00:16:13,000
And so you stop trying to cling.

179
00:16:13,000 --> 00:16:15,000
You stop trying to change.

180
00:16:15,000 --> 00:16:21,000
You stop trying to hold and fix.

181
00:16:21,000 --> 00:16:26,000
And you really do just let things go as they're going to go.

182
00:16:26,000 --> 00:16:31,000
You interact rather than react.

183
00:16:31,000 --> 00:16:33,000
When this comes, you're free.

184
00:16:33,000 --> 00:16:43,000
This is the fourth, fourth essential reality.

185
00:16:43,000 --> 00:16:46,000
Remember the said panea, yep, but he said 15.

186
00:16:46,000 --> 00:16:51,000
That's through wisdom that may become pure.

187
00:16:51,000 --> 00:16:56,000
You know, freedom that we're looking for.

188
00:16:56,000 --> 00:17:05,000
When you have the purity of mind, you've become free from all defilements of mind.

189
00:17:05,000 --> 00:17:06,000
This is true freedom.

190
00:17:06,000 --> 00:17:14,000
When we talk about freedom, we always think that I can do anything and everything.

191
00:17:14,000 --> 00:17:16,000
But you can already do anything.

192
00:17:16,000 --> 00:17:19,000
You can develop in any way, but this is unessential.

193
00:17:19,000 --> 00:17:21,000
It doesn't lead anywhere.

194
00:17:21,000 --> 00:17:24,000
Whether you do something or don't do something,

195
00:17:24,000 --> 00:17:27,000
it's really of no consequence.

196
00:17:27,000 --> 00:17:29,000
When you do something, you're still existent.

197
00:17:29,000 --> 00:17:31,000
It's still existent.

198
00:17:31,000 --> 00:17:34,000
You haven't changed that fact.

199
00:17:34,000 --> 00:17:38,000
When you understand existent, then you're free.

200
00:17:38,000 --> 00:17:42,000
Then nothing in existence can harm you.

201
00:17:42,000 --> 00:17:46,000
If someone tries to hurt you or if something comes that causes harm to you,

202
00:17:46,000 --> 00:17:49,000
you know that there's nothing being harmed.

203
00:17:49,000 --> 00:17:52,000
There's only the experience, and it arises in safety.

204
00:17:52,000 --> 00:17:57,000
So you're not afraid to see that there's nothing to be afraid of.

205
00:17:57,000 --> 00:18:20,000
And the final essence or essential reality that we strive for is the knowledge.

206
00:18:20,000 --> 00:18:23,000
This reassurance that we have.

207
00:18:23,000 --> 00:18:28,000
So this is the fifth one.

208
00:18:28,000 --> 00:18:32,000
This is the final stage where after becoming free, you know that you're free.

209
00:18:32,000 --> 00:18:34,000
This is the stage of enlightenment.

210
00:18:34,000 --> 00:18:41,000
When one is aware and knows for oneself that they have become free from all suffering and stress,

211
00:18:41,000 --> 00:18:48,000
whether they're able to live in the world, live.

212
00:18:48,000 --> 00:18:58,000
In counter any kind of existence or experience without suffering or stress,

213
00:18:58,000 --> 00:19:09,000
without attachment or clinging without aversion or desire or delusion.

214
00:19:09,000 --> 00:19:15,000
So how these come about, once wisdom arises, they're naturally the mind begins to let go.

215
00:19:15,000 --> 00:19:19,000
You can see this already.

216
00:19:19,000 --> 00:19:24,000
When you begin to practice, you see that there's a clinging in the mind,

217
00:19:24,000 --> 00:19:30,000
and when you focus on it, you change the way the mind's understanding of the experience,

218
00:19:30,000 --> 00:19:39,000
or you bring about understanding until the mind is clear about the experience.

219
00:19:39,000 --> 00:19:47,000
But as you continue to do this continuously, develop this quality.

220
00:19:47,000 --> 00:19:50,000
Eventually the mind completely lets go of reality.

221
00:19:50,000 --> 00:19:57,000
There's this final release where the mind lets go.

222
00:19:57,000 --> 00:20:04,000
And there's into a state that is free from the arising of mundane experience,

223
00:20:04,000 --> 00:20:10,000
where the arising of seeing or hearing or smelling or tasting or feeling.

224
00:20:10,000 --> 00:20:13,000
It's free from all of that.

225
00:20:13,000 --> 00:20:19,000
It's like a released.

226
00:20:19,000 --> 00:20:25,000
And then after that experience there arises this knowledge that one has entered into this experience.

227
00:20:25,000 --> 00:20:28,000
That one has had this experience.

228
00:20:28,000 --> 00:20:36,000
And the mind is immediately falls into this comparison, the knowledge of the difference between that state

229
00:20:36,000 --> 00:20:41,000
and the ordinary state of experience.

230
00:20:41,000 --> 00:20:45,000
And this is what leads to enlightenment.

231
00:20:45,000 --> 00:20:52,000
There's clarity of the clarity, there's clear knowledge in the mind

232
00:20:52,000 --> 00:20:58,000
that what was experienced is true happiness and true peace,

233
00:20:58,000 --> 00:21:03,000
and that any happiness or peace that people claim to exist in the world

234
00:21:03,000 --> 00:21:07,000
can never compare to that piece of that happiness.

235
00:21:07,000 --> 00:21:14,000
There's unshakable knowledge and vision in this fact.

236
00:21:14,000 --> 00:21:19,000
This is what leads to enlightenment and knowledge of enlightenment.

237
00:21:19,000 --> 00:21:24,000
This one has experience for oneself the true peace and happiness,

238
00:21:24,000 --> 00:21:28,000
which is the freedom from the arising of experience.

239
00:21:28,000 --> 00:21:35,000
When it has that peace and that happiness,

240
00:21:35,000 --> 00:21:38,000
one never, never strives for.

241
00:21:38,000 --> 00:21:45,000
It can never believe that there would be another kind of peace or happiness greater than it.

242
00:21:45,000 --> 00:21:53,000
Because any other piece for happiness is always dependent on the reason phenomenon, coming and going.

243
00:21:53,000 --> 00:21:58,000
It can never truly satisfy.

244
00:21:58,000 --> 00:22:04,000
So this is the path and the progress.

245
00:22:04,000 --> 00:22:08,000
In the beginning of our practice we develop morality,

246
00:22:08,000 --> 00:22:12,000
so we try to catch ourselves when we're going to say or do bad things,

247
00:22:12,000 --> 00:22:15,000
or things that we'll regret later.

248
00:22:15,000 --> 00:22:17,000
When we've done things that we regret, then we say,

249
00:22:17,000 --> 00:22:22,000
okay, so next time I'm going to be careful and watch myself and try to be mindful of that,

250
00:22:22,000 --> 00:22:26,000
be mindful before I fall into that.

251
00:22:26,000 --> 00:22:30,000
Once we continue that, and again and again,

252
00:22:30,000 --> 00:22:33,000
bringing our mind back to the foot, back to the hand,

253
00:22:33,000 --> 00:22:36,000
keeping our mind with the object.

254
00:22:36,000 --> 00:22:38,000
We develop focus and concentrate.

255
00:22:38,000 --> 00:22:40,000
Once we develop focus and concentration,

256
00:22:40,000 --> 00:22:42,000
we begin to see things clearly.

257
00:22:42,000 --> 00:22:46,000
The defilements and the hindrance is in the mind to appear wanting this,

258
00:22:46,000 --> 00:22:49,000
wanting that, just like in this, just like in that.

259
00:22:49,000 --> 00:22:52,000
Drowsiness, distraction, doubt.

260
00:22:52,000 --> 00:22:55,000
They all leave the mind.

261
00:22:55,000 --> 00:22:59,000
Once they leave the mind, the mind is able to see

262
00:22:59,000 --> 00:23:02,000
the objects of experience clearly.

263
00:23:02,000 --> 00:23:06,000
Once one sees clear and clear and develops wisdom,

264
00:23:06,000 --> 00:23:09,000
the mind eventually lets go once the mind lets go.

265
00:23:09,000 --> 00:23:13,000
Sessation, and then the knowledge that cessation has occurred.

266
00:23:13,000 --> 00:23:17,000
That knowledge is the knowledge of enlightenment.

267
00:23:17,000 --> 00:23:19,000
This is the progress of the path.

268
00:23:19,000 --> 00:23:26,000
Quite in brief, but it's a teaching of the Buddha on what is the essential aspect of existence,

269
00:23:26,000 --> 00:23:29,000
those things that are essential.

270
00:23:29,000 --> 00:23:33,000
This is important to keep in mind when we think about what we're going to do today,

271
00:23:33,000 --> 00:23:42,000
tomorrow for our lives, what we want to do as monastics or as lay people.

272
00:23:42,000 --> 00:23:46,000
We see what is most important, most essential,

273
00:23:46,000 --> 00:23:50,000
and try to find a way to devote ourselves to these things.

274
00:23:50,000 --> 00:23:57,000
It's worth appreciating that everyone, all of us,

275
00:23:57,000 --> 00:24:03,000
that we can be confident in ourselves, confident of ourselves.

276
00:24:03,000 --> 00:24:09,000
We are proud of ourselves, and that we have taken up the practice of meditation,

277
00:24:09,000 --> 00:24:14,000
and here we are coming together to meditate every day every day.

278
00:24:14,000 --> 00:24:18,000
We can feel good about the fact that at least to some extent

279
00:24:18,000 --> 00:24:22,000
we're developing these essential qualities of mind,

280
00:24:22,000 --> 00:24:31,000
and morality, concentration, wisdom, and leading ourselves closer and closer to freedom from suffering.

281
00:24:31,000 --> 00:24:38,000
We can be reassured that what we're doing in our life is not a waste.

282
00:24:38,000 --> 00:24:43,000
We're doing something that is of true value and benefit.

283
00:24:43,000 --> 00:24:49,000
We're reminding ourselves and feeling confident about what we're doing.

284
00:24:49,000 --> 00:24:53,000
We're happy about this, and not looking for other things we're trying to find.

285
00:24:53,000 --> 00:24:57,000
Another way of ourselves.

286
00:24:57,000 --> 00:25:05,000
The practice of the Buddha's teaching is to become content with simplicity,

287
00:25:05,000 --> 00:25:08,000
content with the life of mental development,

288
00:25:08,000 --> 00:25:13,000
always thinking the most important is to make yourself a better person,

289
00:25:13,000 --> 00:25:16,000
or to purify your mind further and further,

290
00:25:16,000 --> 00:25:18,000
develop your mind further and further.

291
00:25:18,000 --> 00:25:22,000
So this is what we're undertaking.

292
00:25:22,000 --> 00:25:27,000
And I just think we've had the talk for tonight,

293
00:25:27,000 --> 00:25:51,000
so thank you for listening.

