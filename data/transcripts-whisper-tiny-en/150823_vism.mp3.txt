Okay welcome everyone continuing our study of the VCD manga Robin you want to start us off?
Sure we're on page 216 number 94. This is chapter 7. Did anyone need the link or everyone's
also like that? Okay one moment.
Okay and David would you be able to begin with 94? Okay how's my sound? Very good thanks.
Okay fit for gifts. As to fit for gifts etc what should be brought and given is a gift.
The meaning is what is to be brought even from far away and donated to the virtuous.
It is a term for the full requisites. The community is fit to receive that gift because it makes it bear great fruit.
Thus it is fit for gifts.
Or alternatively all kinds of property even when the bringer comes from far away can be given here.
Thus the community can be given to. Or it is fit to be given to by Saka and others.
Thus it can be given to. And the brahman's fire is called to be given or sacrificed to.
For they believe that what is sacrificed to it brings great fruit.
But if something is to be sacrificed for the sake of the great fruit brought by what is sacrificed to it.
Then surely the community should be sacrificed to.
For what is sacrificed or given to the community has great fruit.
According as it is said.
For anyone to serve the fire out in the woods a hundred years.
For pay one moment's homage to men of self development. His homage would by far excel is a hundred years of sacrifice.
And the words.
Aha, Veniya, to be sacrificed to which is used in the schools.
It is the same meaning as the word.
Who in Aya fit for gifts used here.
There is only the mere trifling differences of syllables. So it is fit for gifts.
Thank you. Naga, can you read six ninety six.
For hospitality is what a donation to visitors is covered.
Prepared with all owners.
With the sake of dear and beloved relatives and friends who will have come from all quarters.
But even more than to such objects across the body.
It is fitting that it should be given also to the community.
For there is no object of hospitality to, to hospitality self fit to receive hospitality as a community.
Since it is encountered after an interval between Vida and Moses is wholly endearing and vulnerable qualities.
So it is fit for hospitality.
Since the hospitality is fit to be given to it and it is fit to receive it.
But those who take the text to them.
Aha, Veniya, haven't you.
But to be given hospitality.
To profit that the community is worthy to be placed first.
And so what is to be finished first of all.
We got here and given.
Sub Mohammed wa hetha.
And for that reason it is fit to be given hospitality to.
Not going now or since it is worth to be given to all aspect aspects of.
It is thus fit to be given hospitality to the whole one here, and here this is called
looking at the same sense.
Fit for offering.
Offering is what a gift is called that is to be given out of faith in the world to come.
The community is worthy of that offering, or is helpful to that offering because it purifies it by making it
a great fruit.
Thus it is fit for offering.
Fit for salutation.
It is worthy of being accorded by the whole world.
The red rental, salutation consisting in placing both hands.
Almost the other above the head.
Thus it is fit for reverential salutation.
As an incomparable field of merit for the world, as a place without equal in the world for growing merit, just as the place for growing the kings or the ministers,
rice or corn, is the king's rice field or the king's corn field.
So the community is the place for growing the whole world's merit.
For the world's various kinds of merit leading to welfare and happiness grow with the community as their support.
Therefore, the community is an incomparable field of merit for the world.
As long as he collects the special qualities of Sangha in this way, classed as having entered on the good way, etc.
Then on that occasion his mind is not obsessed by greed or obsessed by hate or obsessed by delusion.
His mind has rectitude on that occasion being inspired by the Sangha.
So when he has suppressed the hindrances in the way already described the John factors rise in a single conscious moment,
but owing to the profundity of communities, special qualities, whores, owing to his being occupied in collecting special qualities of many sorts,
the John is only obsessed and does not reach absorption and that excess John itself is known as the collection of Sangha.
Because it rises with the collection of the community's special qualities as the means.
When a vehicle is devoted to this recollection of the community, he is respectful and differential towards the community, he attends forness of faith and so on, he has much happiness and bliss.
He conquers fear and dread. He is able to endure pain. He comes to feel as if he were living in the community's presence and his body when the recollection of the Sangha's special qualities dwells in it.
He becomes as worthy of veneration as an Upa-sata house where the community has met, his mind tends towards the attainment of the community's special qualities.
When he encounters an opportunity for transgressions, he has awareness of cons, cons, consize and shame as vividly as if he were faced to face with the community.
And if he penetrates no higher, he is at least headed for a happy destiny.
Now, when a man is truly wise, he consent tests will surely be this recollection of the Sangha blessed with such mighty potency.
This is the section dealing with the recollection of the community in the detailed explanation.
The recollection of virtue. When he wants to develop the recollection of virtue should go into solitary retreat and recollect his own different kinds of virtue in their special qualities of being unturned, etc., as follows.
Indeed, my various kinds of virtues are unturned, unwatched, unmodeled, liberating, praised by the wise, not adhered to and conducive to concentration.
And the layman should recollect them in the form of layman's virtues, while one gone forth into homelessness should recollect them in the form of the virtue of those gone forth.
I have a quick question about the parley pronunciation in the recollection of the Sangha.
So, when I've heard it chanted by ties, the words sound more like ahunayo parunayo.
So, that last E sounds more like an I, but my understanding of parley pronunciation is that it should be ahunayo parunayo.
It's a good question. You want to know what it is? It's nuya, it's nuya, it's not an E or an I, it's nuya.
And why that is? Because tie has its own special vowels, and one of the vowels in tie is the vowel ui, which you get when you combine the vowel for A and a Y, the consonant for Y, when you put the A and the Y together, the A sound and the Y sound together.
You get nuya, which is just a quirk of the tie language, having many, many vowels that aren't in other languages.
So, it's just a corruption of tie. I've heard people even try to explain it that that's how it really should be pronounced, don't pronounce it as nuya.
It should be pronounced as nuya, which is not, not certainly not correct, because it's not nuya, it's nuya.
Okay, thank you very much.
Whether they are the virtues of layman, or of those gone forth, when no one of them is broken in the beginning or in the end, not being torn like a cloth ragged at the ends, then they are unt torn.
When no one of them is broken in the middle, not being rent like a cloth that is punctured in the middle, then they are unrent.
When they are not broken twice or thrice in succession, not being blotched like a cow whose body is some such colour as black or red, with
discrepent coloured oblong or round patch appearing on her back or belly, then they are unbloched.
When they are not broken or over at intervals, not being mottled like a cow speckled with discrepent coloured spots, then they are unmottled.
Or in general, they are on term, on brands, unbloched, on mottled, when they are undamaged by the seven bones of sexuality, and by anger and enmity and the other evil things.
The same virtues are liberating since they liberate by praying from the slavery of craving. They are praised by the wise because they are praised by such wise men as enlightened ones.
They are not adhered to, since they are not adhered to with craving and false view, or because of the impossibility of misapprehending that there is this flaw in your virtues.
They are conducive to concentration, since they can do to access concentration and absorption concentration, or to the path concentration or fruition concentration.
As long as you collect these own virtues in their special qualities of being undrawn, etc. In this way, then, on this occasion, on that occasion, his mind is not obsessed by green or obsessed by hate or obsessed by dilution.
His mind has rectitude on that occasion being inspired by virtue.
So, when he was surprised with hindrances, in the way of already described, the Jana, Dr. Sarai's in single conscious mood, but owing to the quantity of the virtues, special qualities, are only owned to his being, occupied in the collecting special qualities of many sorts.
The Jana is only accessed in this knowledge of absorption, in that access Jana itself is known as the collection of virtue, to, because it arises with the virtues, special qualities, and some needs.
And one of the cues devoted to this recollection of virtue is respect for the training. He lives in communion with his fellow, with his fellows in the life of purity.
He is said to us and welcoming. He is devoid of the fear of self-approachance and so on. He sees far in the slightest fault. He attains fullness of faith and so on. He is much happiness and gladness.
And if he demonstrates no higher, he is at least headed for a happy destiny.
Now, when a man is truly wise, his constant task will surely be his recollection of his virtue, plus with such mighty potency.
This is the section dealing with recollection of virtue in a detailed explanation.
One who wants to develop the recollection of generosity should be naturally devoted to generosity and the constant practice of giving and sharing.
Or alternatively, if he is one who is starting the development of it, he should make the resolution from now on when there is anyone present to receive it.
I shall not eat even a single mouthful without having given a gift. And that very day he should give a gift by sharing according to his means and his ability, with those who have distinguished qualities.
When he has apprehended the sign in that, he should go into solitary retreat and recollect his own generosity in its special qualities of being free from the stain of avarice, etc., as follows.
It is gained from me. It is great gain from me that in a generation obsessed by the step, stain of avarice.
I abide with my heart free from stain by avarice and am freely generous and open-handed. Did I delight in relinquishing?
Expect to be asked and rejoice in giving and sharing.
I have a question. If everybody is not living in the pedestrian and the area, I have to work in the real world in there so much hatred.
So, when you have to practice, it is just compression and compression you have to practice towards the hatred. Sometimes I find it difficult. And I feel like quitting more.
Or quitting work. Or quitting practicing.
Hello?
Could you maybe type that in? Where are you?
Oh, good.
She deafened herself. I think she didn't hear me. I asked if you meant you want to quit work or quit practicing.
I didn't hear you, so I doubt you did.
Oh, that's okay. Pantay was asking me if your question was about quitting work or quitting work.
I can not quit work because I need to work. So, I keep practicing.
If you go there to be with me or go out to me in reaching these people with compression, not say anything negative to them, but listen what they are saying to me and keep practicing.
But at the end of the day, I find myself in peace that it all is almost. I feel maybe it is for me to practice.
But sometimes I find it difficult to practice.
Please mute yourself when you're finished talking.
You really should set up, push the dog.
Well, my guess my question is what you mean in regards to practicing because it's not correct to suggest that the Buddha was with you.
Certainly not correct to suggest that God is with you. Those aren't Buddhist practices or Buddhist premises on much to practice.
The Buddha said you have to practice for yourself.
So, my question is I guess what exactly are you practicing when you somehow expect God or Buddha to be with you?
I don't want to take Buddha with me because now I'm at a place that I want to be on my own.
So, I keep trying to not to bring them.
It's like I feel alone. I'm doing that, but at the same time I also try not to bring Buddha or Jesus to me. I feel like in a way I recognize that I'm on my own. That's that's where I feel right now.
That's the way things are. We're born alone. We die alone. That's nature. When practice is not difficult, but something being difficult doesn't mean it's not worth doing.
There's no direct connection between something being difficult and something being worth doing. In fact, for most of us, that which is worth doing is often the most difficult.
So, good luck with you. I hope it all works out, but you have to be patient and not expect too much.
Expectations will ruin things.
Hearing this gain for me, it is my gain advantage. The intention is I surely partake of those kinds of gain for a giver that have been commended by the Blessed One as follows.
One who gives life by giving food, shall have life by the divine or human, and a giver is loud and frequent by many. And one who gives is ever loud according to the wise man's law and so on.
It is great again for me. It is grand again for me that this dispensation or the human state has been gained by me. Why? Because of the fact that I abide with my my free from standby.
Ever rest and rejoice the rejoice by rejoice in giving and sharing.
Sorry, which one are we at? 110 on page 220.
Hearing obsessed by the state of avarice is overwhelmed by the state of avarice. Generation means beings so called owing to the fact that they are being generated. So the meaning here is this, among beings who are overwhelmed by the state of avarice, which is one of the dark states that corrupt the natural transparency of consciousness and which has the characteristic of inability to bear sharing one's own good fortune with others.
Free from staying by avarice because of being both free from avarice and from the other stains, greed, hate and the rest.
I abide with my heart. I abide with my consciousness of the kind already stated is the meaning. But in the suta, I lived the home life with my heart free, etc. is said because it was taught there as a mental abiding to depend on constantly to Mahanama the sakyan.
Who was a stream emperor asking about and abiding to depend on. There the meaning is I live overcoming.
Really generous, liberally generous, open-handed, with hands that are purified, where is meant is with hands that are always watched in order to give gifts carefully with ones on hands.
That I delight in ruling pushing. The act of ruling pushing is ruling pushing. The meaning is giving up. To the light in reliquing, relinquing is to delight in constant devotion to that relinquing.
Expect to be asked accustomed to be asked because of giving whatever other acts for is the meaning.
It is a reading. In which case, it is devoted to sacrifice. In other words, to sacrifice in and rejoice in sharing. The meaning is, he recollects through those.
I give gifts and I share out what is to be used by myself and I rejoice in both.
As long as he recollects his own generosity and its special qualities of freedom from stained by avarice, etc. In this way, then on that occasion, his mind is not obsessed by grief or obsessed by hate or obsessed by delusion.
His mind has rectitude on that occasion being inspired by generosity. So, when he has supposed to be hindrances in the way already described, the John effectors arise in a single conscious moment.
But owing to the profundity of the generosity's special qualities, or owing to his being occupied in recollecting the generosity's special qualities of many sorts, the John is only access and does not reach absorption.
And that access John is known as recollection of generosity too, because it arises with the generosity's special qualities as the means.
And then, as he goes to his repulsion of generosity, he becomes ever more intent on generosity, his preferences for non-grade, he acts in conformity with having kindness, his fearless, he has much happiness and sadness.
And he, and he can penetrate no higher, he's at least headed for a happy destiny. Now, when a man is truly wise, his constant task force should be, his recollection of his giving, best to be such mighty potency.
This is the section dealing with the recollection of generosity in the detailed explanation.
All of these potential valid meditation would be to pick one of these or go through each one of the factors that are explained here.
Starting with lava, what the may, lava, what the may, it is indeed a gain for me. And it's a great gain for me. And each of the, each of the qualities.
When we got the Malamat Chai Reena, Chaitasa, we Harami, I don't know. I'm with the Jago, right? Some of these qualities.
It was similar to our quote of the day, the other day too, about giving with one's own hands. The five types, the five qualities of a soup, sappurisa dalen, a gift from a good person.
There's quite a bit about giving charities and an important thing. I mean, it's partly important to mention it because, because of how important it is to keep the religion going.
But it certainly is a great spiritual practice. I mean, for opening the heart for cultivating. I mean, because Buddhism is all about letting go, right? Charity falls hand in hand with that. And if one is not charitable, it's a good sign that they're not capable of letting go.
Quinn, can you read 115?
Yeah, sure. One who wants to develop the recollection of deities should possess the special qualities of faith, etc.
That are evoked by means of the noble path, and he should go into solitary retreat and recollect his own special qualities of faith, etc. with deities standing as witness as follows.
There are deities of the realm of the four kings. There are deities of the realm of the 33. There are deities who are gone to die in bliss, who are contented, who delight in creating, who will power over other creations.
There are deities of the realm of the spirit to knew. There are deities higher than that. And those deities were possessed of faith, such that, on dying here, they were reborn there, and such faith is present in me too.
In those deities were possessed of virtue of learning, of generosity, of understanding, such that when they died here, they were reborn there, and such understanding is present in me too.
In the Suta, however, it is said, on the occasion, Mahanama, on which a noble disciple recollects the faith, the virtue, the learning, the generosity, and the understanding that are both his own, and of those deities.
On that occasion, his mind is not obsessed by greed, although this is said, it should nevertheless be understood as said for the purpose of showing that the special qualities of faith, etc., in oneself are those in the deities, making the deities stand as witness.
For it is said, definitely, in the commentary, he recollects his own special qualities, making the deities stand as witnesses.
As long as in the prior stage, he recollects the deities, special qualities of faith, etc., and in the later stage, he recollects the special qualities of faith, etc., existing in himself, then, on that occasion, his mind is not obsessed by greed,
or obsessed by hate, or obsessed by delusion, his mind is rectitude on that occasion, being inspired by deities.
So, when he has suppressed the hindrances in the way, he already stated, the janitors rise in a single conscious moment, but owing to the profound deity of the special qualities of faith, etc., or owing to his being occupied in recollecting special qualities of many thoughts, the janitors only access and does not reach absorption.
And that access janitor itself is known as recollection of deities too, because it arises with the deities, special qualities, as the means.
But which deities are these that we are recollecting?
Well, although it does include brahma realm, right? So, it is angels in God. I mean, I always translate
data as angels, because it is separate from brahmas, but this includes both means it is not deities, it is higher beings, divine beings, you can say.
So, no one in particular, just the people are for the deities, for particular realm.
And the morality, maybe, that is what you recollect from the name.
Thank you.
And what was the question?
Yeah, I was just confused as to whether these were, you know, particular individual deities or angels or bromos or just kind of in general.
For human, above human.
Thank you, Bhante.
And when a pickle is devoted to this recollection of deities, he becomes dearly loved by deities. He obtains even greater forness of faith. He has much happiness and gladness.
And if he penetrates no higher, he is at least headed for a happy destiny.
Now, when a man is truly wise, his constant text will surely be this recollection of deities, blessed with such mighty potency.
This is the section dealing with the recollection of deities in the detailed explanation.
Bhante, can you read one 19?
Yes, sorry.
Now, in setting forth the detail of these recollections, after the words, his mind has rectitude on the education being inspired by the perfect one.
It has added, when a noble disciples mind has rectitude Mahanama, the meaning inspires him, the law inspires him, and the application of the law makes him glad.
When he is glad, happiness is born in him.
Here in the meaning inspires him should be understood as said of contentment inspired by the meaning beginning.
This blessed one is such, since he is, etc.
The law inspires him, is said to contentment, said of contentment inspired by the text.
The application of the law makes him glad, is said of both.
And when in the case of the recollection of deities inspired by deities is said,
this should be understood as said either of the consciousness that occurs in the price stage inspired by deities,
or of the consciousness that occurs in the later stage inspired by the special qualities that are similar to those of the deities,
and are productive of the deity's state.
This six recollections succeed only in noble disciples.
Disciples, for the special qualities of the enlightened one, the law, and the community are evident to them.
And they possess the beauty with the special qualities of intoneness, etc.
The generosity that is free from a stain by avarice, and the special qualities of faith, etc.
Similar to those of deities.
And in the Mahamnamasuta, they are expounded in detail by the blessed one, in order to show a stream winner and abiding to depend upon when he asked for one.
Also, in the Devasuta, they are expounded in order that a noble disciple should purify his consciousness by means of the political action and so attained further clarification in the ultimate sense that's there because a noble disciple recollect the perfect name in this way.
The blessed one is such things he's accomplished, his mind has rectitude on that occasion.
He has announced, God free from emerge from keepredity, keepredity, because it's a turn for the five chords of sense, desire.
Some beings gain purity here by making this recollection.
They are expounded as the realization of the wide open through the susceptibility of purification that exists in the ultimate sense only in a noble disciple thus.
It is wonderful, friends, it is marvelous how the realization of the wide open in the crowded house life has been discovered by the blessed one who knows and sees accomplished and fully enlightened for the purification of beings, for the surmounting of sorrow and lamentation, for the ending of pain and grief, for the attainment of the true way, for the realization of Nibbana, that is to say the six stations of recollection,
which six, what six, your friends are noble disciple recollect the perfect one, etc.
Some beings are susceptible to purification in this way.
Also in the post that they are expounded in order to show the greatness of the fruit of the impositor, as the mind purifying meditation subject for a noble disciple, who is subsaved in the impositor and what is number one's opposite of Osaka.
It is the gradual cleansing of the mind still solid by imperfections and what is the gradual cleansing of the mind still solid by imperfections.
Here, Osaka, a noble disciple recollect the perfect one, etc.
In the book of 11, when a noble disciple has asked, when a vulnerable sur in what way should we abide, who abide in various ways.
They are expounded to him in order to show the way of abiding in this way, one who has faith is successful.
One who has no faith, one who is energetic, one whose mindfulness is established, one who is concentrated, one who has understanding is successful.
My Hanama, not one who has no understanding, having established yourself in these five things.
My Hanama, you should develop six things here.
My Hanama, you should recollect the perfect one, that the last one is such a thing since.
Still, though this is so, they can be brought to mind by an ordinary man too, if he possesses the special qualities of purified virtue and the rest.
For when he is recollecting the special qualities of the Buddha, etc., even only according to hearsay.
His consciousness settles down by virtue of which the hindrances are suppressed.
In his supreme gladness, he initiates insight, and he even attains to our hunch.
That venerable one, it seems, saw a figure of the enlightened one, created by Mara.
He thought, how could this appear despite its having greed, hate and delusion?
What can the blessed ones goodness have been like?
That he was quite without greed, hate and delusion?
He acquired happiness with the blessed one as object, and by augmenting his insight he reached our hunchhip.
The seventh chapter called the description of six recollections in the treaties on the development of concentration in the path of purification,
composed for the purpose of gladening good people.
That's all for that then, take a break.
We'll stop recording there anyway.
