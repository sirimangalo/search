1
00:00:00,000 --> 00:00:08,000
To be, to teach in a way that their students can apprehend.

2
00:00:16,000 --> 00:00:22,000
I mean, I guess I would even go so far as to say, if you want to be loved by your students,

3
00:00:23,000 --> 00:00:28,000
make sure you're cultivating mindfulness, you know, because you want to be present

4
00:00:28,000 --> 00:00:31,000
and don't want to come across as egotistical.

5
00:00:31,000 --> 00:00:32,000
You don't want to have an agenda.

6
00:00:32,000 --> 00:00:34,000
You don't want to be cruel.

7
00:00:35,000 --> 00:00:45,000
You know, giving overly critical and overly overly harsh grades is not helping anybody.

8
00:00:46,000 --> 00:00:47,000
I had one professor.

9
00:00:47,000 --> 00:00:50,000
It seemed like she just wanted to teach me a lesson.

10
00:00:50,000 --> 00:00:51,000
Because I was Buddhist.

11
00:00:51,000 --> 00:00:52,000
I think it was a religious study.

12
00:00:52,000 --> 00:00:58,000
It was really a ridiculous situation, but I felt that she was actively, anyway,

13
00:00:58,000 --> 00:01:09,000
she was very critical and many years ago vindictive if it felt like...

14
00:01:09,000 --> 00:01:23,000
But students do respond to the compassion and to kindness and to a professor

15
00:01:23,000 --> 00:01:29,000
who seems to be working for the students in the sense of really understanding

16
00:01:29,000 --> 00:01:34,000
that they're there to help the students learn.

17
00:01:34,000 --> 00:01:39,000
Not just to pad their egos, boost their egos.

18
00:01:39,000 --> 00:01:42,000
Not just to make money.

19
00:01:44,000 --> 00:01:47,000
Again, so I don't need a dhamma.

20
00:01:47,000 --> 00:01:50,000
If you want harmony,

21
00:01:52,000 --> 00:01:56,000
you've got to work at it and cultivate it in your students as well.

22
00:01:56,000 --> 00:02:03,000
Anyone who teaches anything is always in a position to impart values,

23
00:02:03,000 --> 00:02:10,000
their own personality to their students.

24
00:02:10,000 --> 00:02:17,000
So that's a heavy burden.

25
00:02:17,000 --> 00:02:27,000
Yeah, well, I don't think you really have to respect teachers who are unkind or cruel.

26
00:02:27,000 --> 00:02:33,000
Not any more than you respect everyone.

27
00:02:33,000 --> 00:02:36,000
We should respect everyone, to some extent.

28
00:02:36,000 --> 00:02:43,000
But there's a difference between respecting everyone and esteeming them.

29
00:02:43,000 --> 00:02:48,000
It wouldn't esteem someone who was cruel.

30
00:02:48,000 --> 00:02:51,000
I had an argument with this recently.

31
00:02:51,000 --> 00:02:59,000
I'm of the sort of the school that if they're my teacher, I'm going to respect the position

32
00:02:59,000 --> 00:03:05,000
and respect them as a teacher in the sense of giving them the honor and deserve,

33
00:03:05,000 --> 00:03:08,000
you know, the honor that comes.

34
00:03:08,000 --> 00:03:18,000
So I will hold them up in a technical position of superiority.

35
00:03:18,000 --> 00:03:29,000
I won't interrupt their class and I won't disrupt their class.

36
00:03:29,000 --> 00:03:56,000
I think that's important and useful.

