1
00:00:00,000 --> 00:00:06,800
Okay, I'm thinking about teaching elementary students meditation especially those who have emotional

2
00:00:06,800 --> 00:00:11,760
disturbances such as strong anger, defiance, hyperactivity.

3
00:00:11,760 --> 00:00:15,440
What is the best technique are they too young to learn?

4
00:00:15,440 --> 00:00:23,160
Okay, I probably should do a separate video just to say this but let me just say it again

5
00:00:23,160 --> 00:00:29,480
and again and all of them because it's something that I believe quite strongly and I've

6
00:00:29,480 --> 00:00:39,440
said it now tonight a few times is that don't go where the real problems are, I would

7
00:00:39,440 --> 00:00:47,120
recommend against trying to deal with people who have serious problems.

8
00:00:47,120 --> 00:00:56,240
It's very altruistic to do so but it's missing one important, very important point.

9
00:00:56,240 --> 00:01:12,560
The amount of effort you put in to helping someone who is in a very bad state is compared

10
00:01:12,560 --> 00:01:17,400
with the benefit of helping them, the potential benefit.

11
00:01:17,400 --> 00:01:22,520
The effort level is incredibly high.

12
00:01:22,520 --> 00:01:27,920
The amount of benefit that you can give to them is very low.

13
00:01:27,920 --> 00:01:38,240
There's no miracles, a person who has a lot of mental trouble is not likely to in most

14
00:01:38,240 --> 00:01:43,960
cases become fully enlightened being in this life.

15
00:01:43,960 --> 00:01:47,920
It just doesn't happen this month that I was talking about as an example of that.

16
00:01:47,920 --> 00:01:51,880
He practiced meditation, he's been practicing, if he still alive, he's been practicing

17
00:01:51,880 --> 00:01:56,920
for 20 years or something and he's still taking baby steps.

18
00:01:56,920 --> 00:02:01,000
He's still blowing up, he's still got the same problems.

19
00:02:01,000 --> 00:02:03,240
It's a very, very slow path.

20
00:02:03,240 --> 00:02:09,760
Everyone who practices meditation is going to go through the same conditions again and

21
00:02:09,760 --> 00:02:18,000
again and again until finally they're worked out, worn down, broken apart and discarded.

22
00:02:18,000 --> 00:02:32,240
But until that point they will come back again and again and again until they become weaker

23
00:02:32,240 --> 00:02:39,760
and weaker until they disappear.

24
00:02:39,760 --> 00:02:44,000
You get a far less return for your effort.

25
00:02:44,000 --> 00:02:49,880
If you take the same amount of effort and you apply it to people who are exceptional

26
00:02:49,880 --> 00:03:02,440
gifted, brilliant, focused, people who have potential and you teach them meditation.

27
00:03:02,440 --> 00:03:13,760
First of all, very little effort, second of all, incredible results that they will

28
00:03:13,760 --> 00:03:23,880
be, they will grasp it much easier, pick it up and in many cases it's like planting

29
00:03:23,880 --> 00:03:24,880
a seed.

30
00:03:24,880 --> 00:03:35,400
You don't have to make it grow, it grows by itself, it goes viral because they pick it

31
00:03:35,400 --> 00:03:37,520
up and they become you.

32
00:03:37,520 --> 00:03:43,800
They become the person who is spreading the teaching.

33
00:03:43,800 --> 00:03:55,440
You help such people who are teachers and those teachers go and help and go and teach.

34
00:03:55,440 --> 00:04:04,520
So at the very least what you'll get theoretically is a bunch of people who are very

35
00:04:04,520 --> 00:04:11,840
gifted who will then go out and pass along what you've passed along to people who are

36
00:04:11,840 --> 00:04:19,720
less gifted and on and on down the line and there will be people who are actually working

37
00:04:19,720 --> 00:04:31,320
with these kind of people who have serious problems and they will have based on the teachings

38
00:04:31,320 --> 00:04:36,640
that you have been giving on a higher level, they will do the grunt work of dealing with

39
00:04:36,640 --> 00:04:48,440
these people and it will create more benefit in the long run, even for those people

40
00:04:48,440 --> 00:04:57,240
who are in a bad way because you are helping people who are in a position to help others

41
00:04:57,240 --> 00:05:07,280
will create a chain, it's like a pyramid scheme, it will multiply and everyone will benefit.

42
00:05:07,280 --> 00:05:11,000
What I'm saying is that people who come to practice meditation with me end up becoming

43
00:05:11,000 --> 00:05:19,720
social workers or teachers and so I don't have to go out and teach those people, I teach

44
00:05:19,720 --> 00:05:26,080
the people who will go out and teach them which is to me far better use of one's time.

45
00:05:26,080 --> 00:05:32,160
The basic point being to try to pick the gifted, pick the ones who are you going to get

46
00:05:32,160 --> 00:05:38,040
the best return for your effort because not only it's not being selfish, it's not being

47
00:05:38,040 --> 00:05:44,720
discriminatory, it's being realistic and understanding that we're really like in a war

48
00:05:44,720 --> 00:05:53,240
here, we don't have the luxury to pick and choose, we have to go for the most benefit

49
00:05:53,240 --> 00:06:02,600
because death waits for no one and nothing waits, the universe will continue rolling on in

50
00:06:02,600 --> 00:06:07,040
its way and if we don't keep up, we're going to get left behind.

51
00:06:07,040 --> 00:06:11,320
This is why people get burnt out when they deal with such people, now this is only half

52
00:06:11,320 --> 00:06:18,880
of the answer, my answer is don't teach children, this is only in relation to the part

53
00:06:18,880 --> 00:06:23,240
of especially those who have emotional disturbances.

54
00:06:23,240 --> 00:06:28,920
You're welcome to do it, I'm not going to try to tell you what to do, just give you

55
00:06:28,920 --> 00:06:34,760
some idea maybe it will help to change your focus, I would think it would be much for

56
00:06:34,760 --> 00:06:41,960
my point of view, much more inspiring to teach the gifted children and the way that you

57
00:06:41,960 --> 00:06:50,280
teach children, the question as to whether they're too young, well the theory, the tradition

58
00:06:50,280 --> 00:06:56,120
goes seven years, everything's seven, so at seven years they're old enough to understand

59
00:06:56,120 --> 00:07:02,080
which is most children already, but anything below seven years is considered to be too

60
00:07:02,080 --> 00:07:10,040
young, but at seven years this is the cutoff and then they can start learning.

61
00:07:10,040 --> 00:07:16,640
The way that these children is to introduce to them the concept of mindfulness, the concept

62
00:07:16,640 --> 00:07:32,600
of recognition, teaching them to recognize and to clearly perceive the experience in

63
00:07:32,600 --> 00:07:41,800
front of them, or anything really, and I tried an experiment once with some kids in a

64
00:07:41,800 --> 00:07:50,560
Thai time monastery in Los Angeles, I had them focus on a cat, I had them think of a cat

65
00:07:50,560 --> 00:07:56,920
and say to themselves in their mind cat, because this was a room of like a hundred kids

66
00:07:56,920 --> 00:08:01,720
and they were unruly, these were kids who come to the temple on Saturdays and Sundays

67
00:08:01,720 --> 00:08:10,960
and get whatever lesson that is possible to give them, but it's mainly, well whatever,

68
00:08:10,960 --> 00:08:21,000
it is what it is, and so they were like anything I would say, they would repeat it and

69
00:08:21,000 --> 00:08:27,960
they would make fun of it and laugh at it, and I wasn't upset by that, but I was prepared

70
00:08:27,960 --> 00:08:38,000
in advance because I was aware of the situation, so you can't lead these people into directly

71
00:08:38,000 --> 00:08:42,320
into meditation, you can't say start watching the stomach say rising, give them something

72
00:08:42,320 --> 00:08:47,400
that's going to appeal to them, so I thought well let's do some simple kid stuff, think

73
00:08:47,400 --> 00:09:02,960
of a cat and say to yourself cat, cat, cat, and then dog, dog, I had them do dog, dog

74
00:09:02,960 --> 00:09:09,240
and it's kind of fun at first, it seems like I'm just playing a game with them, but actually

75
00:09:09,240 --> 00:09:15,960
they start to conceive and they start to focus and their mind starts to quiet down, and

76
00:09:15,960 --> 00:09:22,400
it did work, I think it worked quite well, it's something that you'd have to try long term,

77
00:09:22,400 --> 00:09:27,840
but the point of it is to get them around slowly to the point of being able to watch and

78
00:09:27,840 --> 00:09:33,600
observe their own experience, first a cat and then a dog, and then I had them focus on

79
00:09:33,600 --> 00:09:38,880
their parents, and this is important, and this really got me brownie points with the parents,

80
00:09:38,880 --> 00:09:49,080
the parents are all sitting listening as well, and as I said think of your mother and

81
00:09:49,080 --> 00:09:55,880
say to yourself mother, mother, mother, and then perfect your father and say to yourself

82
00:09:55,880 --> 00:10:02,920
father and father, and then I did, then I came to the, what is actually the beginnings

83
00:10:02,920 --> 00:10:07,760
of Buddhist meditation, it's the focusing on the Buddha, right, you have this famous Buddhist

84
00:10:07,760 --> 00:10:14,040
meditation of saying yourself Buddha, Buddha, so I had them do that, I said picture the

85
00:10:14,040 --> 00:10:19,360
Buddha now, because the Buddha being golden Buddha image, so think of the Buddha and say

86
00:10:19,360 --> 00:10:27,160
Buddha, Buddha, Buddha, Buddha, Buddha, Buddha, Buddha, and so they did this, and then eventually

87
00:10:27,160 --> 00:10:34,160
I got around, I think after the Buddha then it was right to focusing on the body, and

88
00:10:34,160 --> 00:10:42,160
then it was watching the stomach, watching your breath, I brought it to them that way, I think

89
00:10:42,160 --> 00:10:47,440
that it's just a simple point that the idea of starting with something cartoonish that

90
00:10:47,440 --> 00:10:56,800
they can relate to, and of course it depends on the age level, but the important point

91
00:10:56,800 --> 00:11:09,760
is to bring the concept to them, find a way to explain mindfulness to them, and I think

92
00:11:09,760 --> 00:11:14,720
that's clearly explained by using the cat example, the dog exam, because you're just focusing

93
00:11:14,720 --> 00:11:20,720
on something and you're seeing this as a cat, this is a dog, and then you have, they're

94
00:11:20,720 --> 00:11:27,320
building, what you're doing this, they're building up this ability, and all you do is

95
00:11:27,320 --> 00:11:57,160
you bring that ability, that proficiency, or however, back to focus on reality.

