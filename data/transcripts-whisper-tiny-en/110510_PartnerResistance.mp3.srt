1
00:00:00,000 --> 00:00:04,160
Oh, welcome back to Ask A Moog.

2
00:00:04,160 --> 00:00:11,200
Next question, I've been studying the teachings and meditation for a few months now, but

3
00:00:11,200 --> 00:00:16,640
my partner doesn't like it and makes me feel bad like I'm doing something wrong as a person

4
00:00:16,640 --> 00:00:21,040
in general, even if I'm more compassionate towards others.

5
00:00:21,040 --> 00:00:30,040
Do you have any advice?

6
00:00:30,040 --> 00:00:34,040
Well, it's actually not that difficult of a problem.

7
00:00:34,040 --> 00:00:40,760
A worse problem would be if you actually believed your partner, which it doesn't sound

8
00:00:40,760 --> 00:00:41,760
like you do.

9
00:00:41,760 --> 00:00:49,360
And if so, if you're in doubt, then I would say that's much more important to address.

10
00:00:49,360 --> 00:00:59,200
Whether you really think that the meditation is beneficial and it sounds like you do, though

11
00:00:59,200 --> 00:01:05,800
I wouldn't say that the most important benefit is being compassionate towards others because

12
00:01:05,800 --> 00:01:09,120
you have to be very compassionate towards yourself as well.

13
00:01:09,120 --> 00:01:16,440
The most important is to gain understanding about reality and that really leads to compassion.

14
00:01:16,440 --> 00:01:22,200
And I suppose that it's probably implicit in what you're saying that you've learned

15
00:01:22,200 --> 00:01:28,680
more and come to understand yourself better and understand what is a benefit, what is right

16
00:01:28,680 --> 00:01:32,440
and what is proper and what is wrong and what is improper and therefore you're acting

17
00:01:32,440 --> 00:01:38,840
in a much more proper way or somewhat more proper and therefore you're more interested

18
00:01:38,840 --> 00:01:44,840
in helping yourself and helping other people, which makes you more compassionate.

19
00:01:44,840 --> 00:01:49,800
So being able to see that benefit is really an important thing.

20
00:01:49,800 --> 00:01:56,600
The question of whether to listen to your partner, therefore it doesn't arise.

21
00:01:56,600 --> 00:02:03,720
The question you're asking is what to do about their feelings.

22
00:02:03,720 --> 00:02:06,960
And you should get that straight because it will make you feel a lot better.

23
00:02:06,960 --> 00:02:10,640
You won't have to be concerned about what they say.

24
00:02:10,640 --> 00:02:15,440
If they're making you feel bad, then it means they're getting to you.

25
00:02:15,440 --> 00:02:22,000
It means you're still clinging somehow to their words and to their views and their ideas.

26
00:02:22,000 --> 00:02:26,000
If it's clear in your mind what is right and what is wrong, you have no one's able

27
00:02:26,000 --> 00:02:28,760
to make you feel good or bad.

28
00:02:28,760 --> 00:02:35,800
You are at peace with yourself and you're happiness doesn't depend on other people or

29
00:02:35,800 --> 00:02:38,280
externalities.

30
00:02:38,280 --> 00:02:47,760
So what to do when someone else doesn't like what you don't like, what you know to be

31
00:02:47,760 --> 00:02:50,920
right and proper and useful and beneficial?

32
00:02:50,920 --> 00:02:56,400
Well, the easiest question, the easiest answer and it's probably not the one you prefer

33
00:02:56,400 --> 00:03:05,040
is to leave your partner and I think that's just so obvious and it's probably not the

34
00:03:05,040 --> 00:03:11,760
answer you're looking for but that would imply that somehow you have some other attachment

35
00:03:11,760 --> 00:03:17,720
to this person because the meditation has great benefit to you in your life and it's

36
00:03:17,720 --> 00:03:24,600
bringing you some, well it will bring you peace and happiness whether you can see this

37
00:03:24,600 --> 00:03:30,720
or not I'm not sure but once you practice more you can see how much benefit the meditation

38
00:03:30,720 --> 00:03:36,360
brings to you.

39
00:03:36,360 --> 00:03:42,160
To stay with someone who doesn't agree with that or who believes the opposite and it

40
00:03:42,160 --> 00:03:50,240
is actually hurting you or that it is making you a bad person or so on doesn't make sense

41
00:03:50,240 --> 00:03:58,720
because if you stay with them it's going to conflict with your own benefit so obviously

42
00:03:58,720 --> 00:04:02,360
there must be something else that there must be a clinging to this person or could

43
00:04:02,360 --> 00:04:08,160
be perhaps that there is some structural reason for being in a relationship in terms

44
00:04:08,160 --> 00:04:15,200
of stability and so on or it could be to help that person because that person needs

45
00:04:15,200 --> 00:04:21,960
you and so on so there could be many reasons none of what I'm aware of.

46
00:04:21,960 --> 00:04:32,400
So the more important question then is how to live with this person, how to live with

47
00:04:32,400 --> 00:04:40,240
a person who doesn't agree with what you know to be proper and beneficial and there

48
00:04:40,240 --> 00:04:47,440
are many ways I think the first one is sort of a compromise, I say leave this person

49
00:04:47,440 --> 00:04:55,120
well it doesn't have to be complete often you can take time apart and you can distance

50
00:04:55,120 --> 00:05:01,000
yourself to some extent from the person not dropping them but you can you know take time

51
00:05:01,000 --> 00:05:05,360
out for yourself and say you want to be alone for some time when you have time alone

52
00:05:05,360 --> 00:05:08,720
then they don't see you they're not aware of what you're doing and it doesn't upset

53
00:05:08,720 --> 00:05:13,440
them what you do if you have the ability to take time alone where you're not with this

54
00:05:13,440 --> 00:05:19,360
person even though you're still in a relationship then it can be a real halfway I mean it

55
00:05:19,360 --> 00:05:25,360
might lead to you breaking up but it might also lead to you coming to some better understanding

56
00:05:25,360 --> 00:05:31,480
of each other's position and could even make the relationship stronger and more in line

57
00:05:31,480 --> 00:05:36,160
with what is truly right and beneficial.

58
00:05:36,160 --> 00:05:41,480
Now I'm guessing that it has something to do with religion because it usually does if

59
00:05:41,480 --> 00:05:46,600
people believe that meditation makes you a bad person then it usually there's two others

60
00:05:46,600 --> 00:05:53,520
two reasons one is it one is it conflicts with their beliefs their religion or two and

61
00:05:53,520 --> 00:05:59,360
this is probably not your case given that you are benefiting from it the one person the

62
00:05:59,360 --> 00:06:07,080
person is practicing meditation is practicing it incorrectly and is giving rise to states

63
00:06:07,080 --> 00:06:13,080
that are disturbing so people when they begin to practice meditation are they have the

64
00:06:13,080 --> 00:06:16,800
best of intentions and eventually they'll get good at it but when they're not good at it

65
00:06:16,800 --> 00:06:21,920
it can lead to great stress and conflict inside as you start to learn how to deal with

66
00:06:21,920 --> 00:06:26,880
the brain it's with the mind it's like learning to drive a car when you know be learning

67
00:06:26,880 --> 00:06:32,760
to drive a manual transmission car the people in the car are going to have to put up with

68
00:06:32,760 --> 00:06:37,800
a lot of jerking in the beginning so that can cause conflict but I would imagine that

69
00:06:37,800 --> 00:06:41,800
in your case it has something to do with religion or belief or I don't know it could even

70
00:06:41,800 --> 00:06:50,400
be that the person is a scientist and atheist and but sorry I'm an atheist but is a person

71
00:06:50,400 --> 00:06:56,800
who a secularist I suppose someone who doesn't believe in the existence of the mind

72
00:06:56,800 --> 00:07:07,480
or the benefits of meditation and things you're being brainwashed and so on but I this

73
00:07:07,480 --> 00:07:12,680
is you know in this case you really have to you know it's something that's really going

74
00:07:12,680 --> 00:07:17,080
to take time and it may never be sorted out it may eventually mean that you have to

75
00:07:17,080 --> 00:07:26,760
part ways but the the best way to deal with this to approach this is to walk around the

76
00:07:26,760 --> 00:07:36,000
person to practice around the person you don't have to be sitting on a cushion in a silent

77
00:07:36,000 --> 00:07:40,240
room to be meditating you can meditate in a chair you can meditate whether there are other

78
00:07:40,240 --> 00:07:44,280
people in the room you can meditate while there's noise you can meditate anywhere at any

79
00:07:44,280 --> 00:07:51,560
time it's ideal to have solitude it's ideal to have quiet it's ideal to be sitting across

80
00:07:51,560 --> 00:07:59,280
like it on a cushion but none of these are absolutely necessary and so in a rather than

81
00:07:59,280 --> 00:08:04,240
bemoaning the fact that you're unable to pursue the ideal or trying to pursue the ideal

82
00:08:04,240 --> 00:08:10,840
and as a result bring in conflict with the people around you you can incorporate it into

83
00:08:10,840 --> 00:08:16,280
your relationship with them well they are watching TV television you can be sitting quietly

84
00:08:16,280 --> 00:08:21,960
meditating with your with your eyes open with your eyes closed you can be in another room

85
00:08:21,960 --> 00:08:28,520
you can be be doing anything or be in any in any sort of position you don't have to make

86
00:08:28,520 --> 00:08:33,640
it obvious that you're meditating or you don't have to say to them look I need my half an hour

87
00:08:33,640 --> 00:08:37,640
now could you please leave the room could you please turn off the television etc etc

88
00:08:37,640 --> 00:08:44,280
because eventually you're going to realize that all of that is a part of your meditation

89
00:08:44,280 --> 00:08:51,000
it's a part of the practice that we're following and eventually you'll be able to you know

90
00:08:51,000 --> 00:08:55,240
if you're successful in the practice you'll be able to deal with it all you'll be able

91
00:08:55,240 --> 00:09:03,400
to overcome your versions and attachments to these things so you can live your life with this

92
00:09:03,400 --> 00:09:07,240
person you can go if suppose it's a religious thing and they want you to go to church and

93
00:09:07,240 --> 00:09:11,240
you don't go to church you can go to church and while they're doing that thing you can sit

94
00:09:11,240 --> 00:09:16,920
and meditate and whatever you can you know if they're singing their praises to God you can

95
00:09:16,920 --> 00:09:23,240
sing your praises to God and watch your lips moving as as you sing watch the lips moving feel

96
00:09:23,240 --> 00:09:28,840
the lips moving and just be aware of what's happening and you're standing when you hear the

97
00:09:28,840 --> 00:09:34,760
sound hearing hearing you know you can even just mouth something and take it as a mouth meditation

98
00:09:34,760 --> 00:09:42,760
or whatever I mean examples we do walking and sitting meditation in this frustration but

99
00:09:42,760 --> 00:09:48,840
all of these are just examples you can do meditation in any way at any in any form you can do

100
00:09:48,840 --> 00:09:53,720
driving meditation whatever you do in life when you're if you're if you're the cook in the family

101
00:09:53,720 --> 00:09:59,080
you can do cooking meditation if you're the if you're working in an office job you can do

102
00:09:59,080 --> 00:10:05,880
office meditation or you you know you can take time out of your work to and do five minutes

103
00:10:05,880 --> 00:10:13,080
ten minutes try to work around the person so that they're not even aware that you're meditating

104
00:10:13,080 --> 00:10:20,280
that's much better because what's really going to change them is the strength in your mind

105
00:10:20,280 --> 00:10:26,440
once your mind becomes strong once you become sure there's no way especially if they're a person

106
00:10:26,440 --> 00:10:33,000
who who can't see the benefit of meditation who are so blind that they're unable to see the the

107
00:10:33,000 --> 00:10:38,200
the benefits of it there's no way that they can fight again there's no way that they can compete

108
00:10:38,200 --> 00:10:44,360
against your strength you have the strength of mind because you know you're you're practicing

109
00:10:44,360 --> 00:10:49,560
every day to strengthen your mind and to clarify your mind and there and then what are they doing

110
00:10:49,560 --> 00:10:57,800
you know that their mind is we'll constantly be wavering and you know it may not be that case

111
00:10:57,800 --> 00:11:01,720
right now and maybe that they have the strength and you don't and therefore you're wavering

112
00:11:01,720 --> 00:11:08,280
and you're not sure what to do but that's the the goal if you can get to the point where your

113
00:11:08,280 --> 00:11:13,880
mind doesn't waver then they will have to capitulate eventually they will realize it will come up

114
00:11:13,880 --> 00:11:19,480
in their mind and they'll realize the wrongness of their beliefs and their ideas and eventually

115
00:11:19,480 --> 00:11:24,680
they'll even become interested in the meditation because they'll see how much strength confidence and

116
00:11:24,680 --> 00:11:30,200
peace it brings to you and no matter what their religious views are so I mean that's the deal is

117
00:11:30,200 --> 00:11:38,120
that views and opinions and beliefs are our our our source of strength in a sense and so you have

118
00:11:38,120 --> 00:11:44,600
to get quite powerful to be able to overcome those those views which will eventually you know they

119
00:11:44,600 --> 00:11:50,360
don't jive when they don't jive with reality that person will have to let go but it can take time

120
00:11:50,360 --> 00:11:54,600
people can hold on to it's amazing the beliefs people can hold on to even in the face

121
00:11:55,640 --> 00:12:07,000
even when those beliefs fly directly in the face of reality so good luck and the most obvious

122
00:12:07,000 --> 00:12:14,120
answers to always try to surround yourself with people who are meditating this is a very important

123
00:12:14,120 --> 00:12:18,600
part of the Buddhist teaching to surround yourself with people who are interested in meditation

124
00:12:18,600 --> 00:12:23,880
who are meditating who believe in the benefits and see the benefits of meditation practice

125
00:12:24,520 --> 00:12:33,400
and always strive to to avoid people who are who don't see the benefits you know it's basically

126
00:12:34,120 --> 00:12:41,400
choosing people who can see whose beliefs and opinions don't fly in the face of reality you don't

127
00:12:41,400 --> 00:12:50,680
conflict with with the truth and try to stick only to people who's understanding and beliefs

128
00:12:50,680 --> 00:12:56,200
and views are in line with reality because meditation is a great thing and anyone who believes

129
00:12:56,200 --> 00:13:03,960
others otherwise is is missing something so this is an answer to your question this has been

130
00:13:03,960 --> 00:13:19,880
another episode of Ask A Month wishing you all peace happiness and feeling from suffering

