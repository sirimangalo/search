1
00:00:00,000 --> 00:00:10,720
constructs and you become disenchanted at the knee-bitten that they do okay.

2
00:00:10,720 --> 00:00:15,000
So the second thing that we pass in a means is not just the three characteristics, it

3
00:00:15,000 --> 00:00:23,840
also refers to this process of realization that comes as you start to see the three characteristics.

4
00:00:23,840 --> 00:00:30,880
So it's a real shift because much of what I'm saying might sound foreign to many of you.

5
00:00:30,880 --> 00:00:34,200
I mean, this is the kind of thing that should sound wrong.

6
00:00:34,200 --> 00:00:38,480
I mean, the idea that the body isn't under control, the idea that things aren't stable,

7
00:00:38,480 --> 00:00:42,840
the idea that you can't find satisfaction or happiness in the world.

8
00:00:42,840 --> 00:00:45,000
This shouldn't sound quite wrong to us.

9
00:00:45,000 --> 00:00:52,000
I mean, it's normal for that to be the case because her ordinary understanding of reality

10
00:00:52,000 --> 00:00:53,880
is very different.

11
00:00:53,880 --> 00:00:59,320
We think, you know, some sorrow is wonderful.

12
00:00:59,320 --> 00:01:08,160
So much, so much to be found, so much to be sought out and gained.

13
00:01:08,160 --> 00:01:13,840
But as you make this process so many first step through the door and you start to see things

14
00:01:13,840 --> 00:01:25,360
clearly, we pass in by using mindfulness.

