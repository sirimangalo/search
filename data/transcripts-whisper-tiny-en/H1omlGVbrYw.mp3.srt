1
00:00:00,000 --> 00:00:21,000
Good evening, everyone, broadcasting live May 1st, 2016.

2
00:00:21,000 --> 00:00:30,000
Today's quote is from the Anguitranikaya, again, this time the book of fours.

3
00:00:30,000 --> 00:00:38,000
And again, the quote only shows only singles out part of the supta.

4
00:00:38,000 --> 00:00:41,000
Again, this isn't the words of the Buddha.

5
00:00:41,000 --> 00:00:50,000
This is the words of Ananda.

6
00:00:50,000 --> 00:00:59,000
Ananda was the attendant and cousin of the Buddha,

7
00:00:59,000 --> 00:01:05,000
and he spent much of the Buddha's later life caring for him

8
00:01:05,000 --> 00:01:15,000
and following him around and listening to and gathering and remembering the Buddha's teachings.

9
00:01:15,000 --> 00:01:22,000
And a very good memory, it seems.

10
00:01:22,000 --> 00:01:30,000
So unsurprising that people would come to him for the dhamma, perhaps after the Buddha passed away

11
00:01:30,000 --> 00:01:39,000
because Ananda is one of the great monks who outlived the Buddha.

12
00:01:39,000 --> 00:01:46,000
Sorry put in Mogulana, the Buddha's two cheap disciples passed away before the Buddha did.

13
00:01:46,000 --> 00:01:54,000
But Ananda lived on, Ananda was lived to 120, so the legend goes.

14
00:01:54,000 --> 00:02:13,000
And before he died, he knew that both sides of his family were going to fight over his relics.

15
00:02:13,000 --> 00:02:18,000
So if he died, when he was sick and dying, he went to stay with his family,

16
00:02:18,000 --> 00:02:24,000
but both families wanted to stay with him, to stay with them.

17
00:02:24,000 --> 00:02:25,000
Maybe just making this up.

18
00:02:25,000 --> 00:02:34,000
But at the end result, he flew up into the air and spontaneously combusted

19
00:02:34,000 --> 00:02:44,000
and made a determination so that half of his bones went on either side of the fence.

20
00:02:44,000 --> 00:02:49,000
Something like that, the river, maybe I think it was a river.

21
00:02:49,000 --> 00:02:54,000
There's a lot of stories like this.

22
00:02:54,000 --> 00:02:58,000
But Ananda is a special monk.

23
00:02:58,000 --> 00:03:09,000
The idea we get about him is he was a very compassionate and interested in helping people

24
00:03:09,000 --> 00:03:18,000
who were maybe at a hard time or he was moved to compassion in ways that other monks

25
00:03:18,000 --> 00:03:20,000
maybe weren't.

26
00:03:20,000 --> 00:03:28,000
And so he was moved to compassion, moved by compassion to speak up for the biquinis,

27
00:03:28,000 --> 00:03:33,000
even after the Buddha cautioned against ordaining women.

28
00:03:33,000 --> 00:03:41,000
And Ananda pushed the Buddha to ordain women, of course that's a whole issue.

29
00:03:41,000 --> 00:03:48,000
But there is an argument to be had for why the Buddha was hesitant.

30
00:03:48,000 --> 00:03:57,000
I mean, whether you agree or not, the point being that it makes it more difficult.

31
00:03:57,000 --> 00:04:01,000
You can say, well, that's no excuse, and it really isn't in the end.

32
00:04:01,000 --> 00:04:11,000
Certainly, when you're trying to be celibate and the majority of those trying to be celibate are men,

33
00:04:11,000 --> 00:04:18,000
our heterosexual men bringing women into the mix makes it more difficult.

34
00:04:18,000 --> 00:04:20,000
When it was hesitant.

35
00:04:20,000 --> 00:04:25,000
And also in India, women were perhaps sheltered, so there's arguments.

36
00:04:25,000 --> 00:04:39,000
But Ananda was not having any of it and really convinced the Buddha to ask.

37
00:04:39,000 --> 00:04:44,000
And he taught laymen, laywomen, he was big on teaching people.

38
00:04:44,000 --> 00:04:47,000
It was really helpful towards women especially.

39
00:04:47,000 --> 00:04:53,000
It actually got him in some trouble sometimes because women would take fancy too.

40
00:04:53,000 --> 00:04:57,000
But he was never moved by that.

41
00:04:57,000 --> 00:05:09,000
He was never even claimed that he had never had any sexual desire for a woman as a monk.

42
00:05:09,000 --> 00:05:15,000
Anyway, all this is beside the point.

43
00:05:15,000 --> 00:05:23,000
There are these four Ananda, so they came to Ananda and then they just taught them these four.

44
00:05:23,000 --> 00:05:29,000
What are called the Pari-sundi, Padha-niyang-an.

45
00:05:29,000 --> 00:05:32,000
Pari-sundi means purity.

46
00:05:32,000 --> 00:05:41,000
Padha-niyam means something you should strive for.

47
00:05:41,000 --> 00:05:45,000
And the Ananda is just members or things.

48
00:05:45,000 --> 00:05:53,000
There are these four things regarding which you should strive for the purity, for the purification of.

49
00:05:53,000 --> 00:05:58,000
So, four things you should try to purify.

50
00:05:58,000 --> 00:06:01,000
You should strive to purify.

51
00:06:01,000 --> 00:06:12,000
What are these four?

52
00:06:12,000 --> 00:06:25,000
The striving for the purification of virtue and that's where the quote comes from.

53
00:06:25,000 --> 00:06:34,000
Number two, Chitapati-sundi, Padha-niyang-an.

54
00:06:34,000 --> 00:06:42,000
Striving for the purification of mind.

55
00:06:42,000 --> 00:06:50,000
Number three, Deepati-sundi, Padha-niyang-an.

56
00:06:50,000 --> 00:06:56,000
Striving for the purification of view.

57
00:06:56,000 --> 00:07:01,000
And number four, we put Deepati-sundi, Padha-niyang-an.

58
00:07:01,000 --> 00:07:10,000
Striving for the purification of release, freedom.

59
00:07:10,000 --> 00:07:19,000
So, these four, we should strive to purify, strive to complete.

60
00:07:19,000 --> 00:07:25,000
So, the first one that the quote talks about is purification of morale of virtue.

61
00:07:25,000 --> 00:07:31,000
See that.

62
00:07:31,000 --> 00:07:38,000
And this is, we talked a little bit about this last night.

63
00:07:38,000 --> 00:07:43,000
The precepts and rules and keeping rules.

64
00:07:43,000 --> 00:07:50,000
In general, rules, if you have the right rules, they tend to be a good guide for your behavior.

65
00:07:50,000 --> 00:07:52,000
And so, we purify them.

66
00:07:52,000 --> 00:07:57,000
We look and see which rules we're breaking, which rules we tend to break.

67
00:07:57,000 --> 00:08:03,000
And then we work to stop breaking those rules.

68
00:08:03,000 --> 00:08:14,000
So, what is impure in regards to our unfulfilled, in regards to our Pari-purang-wah-bari-purisam.

69
00:08:14,000 --> 00:08:22,000
I will fulfill those virtues or those rules, specifically dealing with rules.

70
00:08:22,000 --> 00:08:28,000
See kapada, see kapada that are not fulfilled.

71
00:08:28,000 --> 00:08:35,000
I'll strive to fulfill them. So, some people keep the five precepts and then they don't keep the eight precepts

72
00:08:35,000 --> 00:08:38,000
and then they strive to keep the eight precepts.

73
00:08:38,000 --> 00:08:43,000
Some people keep the eight precepts and then they strive to keep the 10 precepts

74
00:08:43,000 --> 00:08:50,000
and then some people keep the 10 precepts and then they strive to become a monk and keep many more precepts.

75
00:08:51,000 --> 00:08:56,000
And then you have the precepts and then you break them and then you strive not to break them

76
00:08:56,000 --> 00:09:00,160
They work to purify them.

77
00:09:00,160 --> 00:09:09,160
And then there's this curious statement, those that are fulfilled, tat, tat, tat, banyaya,

78
00:09:09,160 --> 00:09:13,920
anungay, anungahizam.

79
00:09:13,920 --> 00:09:25,520
I will support with wisdom.

80
00:09:25,520 --> 00:09:36,960
What those that are fulfilled with wisdom, tat, tat, tat, here, there, or again and again,

81
00:09:36,960 --> 00:09:40,720
where appropriate.

82
00:09:40,720 --> 00:09:48,480
So the commentary doesn't give much explanation of what this means, but I guess it means,

83
00:09:48,480 --> 00:09:53,280
because it's easy to keep the letter of the rule, right?

84
00:09:53,280 --> 00:09:57,520
And so you can say, I'm keeping all these rules fine, that you can keep the five precepts,

85
00:09:57,520 --> 00:10:01,920
and still be a bad person, keeping the five precepts.

86
00:10:01,920 --> 00:10:06,080
So it's about filling in the gaps.

87
00:10:06,080 --> 00:10:11,440
And it's about fulfilling the purpose of the precepts with wisdom.

88
00:10:11,440 --> 00:10:15,760
So you might not kill, but you can still be very angry, and you can still want to hurt or

89
00:10:15,760 --> 00:10:25,360
even kill, and you might not steal, but you still covet that of others and are jealous and so on.

90
00:10:25,360 --> 00:10:33,200
You don't cheat, but you still feel desire, and you don't have the understanding of

91
00:10:33,200 --> 00:10:39,120
hurting others, of what it means to hurt others and what is the result of hurting others.

92
00:10:39,120 --> 00:10:45,720
So you use wisdom to augment that, because keeping the precepts, there's this idea that

93
00:10:45,720 --> 00:10:52,760
even if it's painful, keeping the precepts is a good thing.

94
00:10:52,760 --> 00:10:53,760
People might question that.

95
00:10:53,760 --> 00:11:02,520
They think, well, if whatever you're doing is causing you suffering, why would you do it?

96
00:11:02,520 --> 00:11:07,800
But then we can ask the question, suppose there's an alcoholic or a drug addict, and if

97
00:11:07,800 --> 00:11:13,000
they stop taking drugs and they go through withdrawal, would you say that's a reason to go

98
00:11:13,000 --> 00:11:16,880
back to taking drugs, of course, and that's really all this is.

99
00:11:16,880 --> 00:11:22,000
It's a kind of a withdrawal when you keep precepts, when you keep these rules that say

100
00:11:22,000 --> 00:11:29,720
the eight precepts are so on, it's quite unpleasant for the first time, this is because

101
00:11:29,720 --> 00:11:34,560
you're going through withdrawal, doesn't mean that it's wrong.

102
00:11:34,560 --> 00:11:38,880
In fact, it's a good sign, a sign that you're actually dealing with the problem instead

103
00:11:38,880 --> 00:11:46,160
of placating it with your addiction.

104
00:11:46,160 --> 00:11:52,600
So that's the first one, see the body sudhi padha nyanga.

105
00:11:52,600 --> 00:12:06,760
Second one is the thing that should be ought to be purified, which we call the mind, the

106
00:12:06,760 --> 00:12:13,560
chitapari sita, the mind, or not the mind mind, it's a difficult one to translate, because

107
00:12:13,560 --> 00:12:21,280
literally it does mean the mind or mind, but it's not exactly that, it's the purification

108
00:12:21,280 --> 00:12:27,800
of mind states, because it's not the end of the line, chitapari sudhi is just, it's

109
00:12:27,800 --> 00:12:35,560
actually just a concentration, so your mind is pure, that's great, but it's not your

110
00:12:35,560 --> 00:12:36,560
whole mind.

111
00:12:36,560 --> 00:12:43,320
It's not the mind in an abstract sense, it's just a mind in a absolute sense, in a sense

112
00:12:43,320 --> 00:12:47,400
of this mind being pure, this moment of mind.

113
00:12:47,400 --> 00:12:54,200
So as you practice meditation, you'll find, after some time after you become proficient,

114
00:12:54,200 --> 00:13:00,600
you'll find these states arising, where you're peaceful, where your mind just feels completely

115
00:13:00,600 --> 00:13:08,960
free from attachment or craving or aversion, your mind will be in a clear state, in a pure

116
00:13:08,960 --> 00:13:09,960
state.

117
00:13:09,960 --> 00:13:17,840
And we talk about the four jhanas, and they're just explanations about these sorts of states

118
00:13:17,840 --> 00:13:24,640
of mind, where you don't have any light king or dislike king or drowsiness or distraction

119
00:13:24,640 --> 00:13:27,560
or doubt.

120
00:13:27,560 --> 00:13:34,560
No pleasant, no like, you know, judgment, no reaction, just purity of mind.

121
00:13:34,560 --> 00:13:40,960
That's quite useful, because it allows you to see clearly, if you then focus that on reality,

122
00:13:40,960 --> 00:13:45,600
you'll be able to see things in ways that you weren't able to see things when you were

123
00:13:45,600 --> 00:13:50,520
judging them, when you were reacting to them.

124
00:13:50,520 --> 00:13:58,600
So that's really what the meditation is all about.

125
00:13:58,600 --> 00:14:05,000
Meditation is getting yourself in the frame of mind, so that you can accomplish the third

126
00:14:05,000 --> 00:14:12,680
one, which is purification of you, views and opinions, clearing yourselves up, clearing

127
00:14:12,680 --> 00:14:25,480
yourself up, your mind up, in regards to your beliefs, your opinions, your things you hold

128
00:14:25,480 --> 00:14:34,000
to be true, and this is really the heart of the matter, because it's not about calming

129
00:14:34,000 --> 00:14:42,800
yourself down, it's about rightening yourself, purifying the source, because the source

130
00:14:42,800 --> 00:14:50,200
of our mind states, the source which determines whether we're going to have a wholesome

131
00:14:50,200 --> 00:14:57,720
mind or an unwholesome mind, a pure mind or an impure mind, is our view, our outlook.

132
00:14:57,720 --> 00:15:04,240
So if you have the view that certain things are worth attaining, you'll strive after

133
00:15:04,240 --> 00:15:05,240
them.

134
00:15:05,240 --> 00:15:10,120
If you have a view that certain things are worth avoiding, you'll avoid them.

135
00:15:10,120 --> 00:15:18,560
If the view that it's right to be angry and greedy and deluded, then you'll do things

136
00:15:18,560 --> 00:15:23,400
that give rise to those states, and you'll give rise to those states.

137
00:15:23,400 --> 00:15:34,240
If you have views that are, views that greed, anger and delusion are wrong, or you see

138
00:15:34,240 --> 00:15:38,560
them as being a problem, then you're less likely to give rise to those emotions.

139
00:15:38,560 --> 00:15:41,160
So it's about purifying your view.

140
00:15:41,160 --> 00:15:47,600
And in fact, it's not about attaining any one view or any outlook, it's not about acquiring

141
00:15:47,600 --> 00:15:53,680
an outlook, it's about acquiring an understanding of things as they are.

142
00:15:53,680 --> 00:15:59,880
So it's not really a view in the end, except in the literal sense that you're able to view

143
00:15:59,880 --> 00:16:02,160
things as they are.

144
00:16:02,160 --> 00:16:04,720
Your view is in line with reality.

145
00:16:04,720 --> 00:16:05,960
That's really all we're aiming for.

146
00:16:05,960 --> 00:16:13,560
We're not aiming to claim anything until the claim we make is that if you look, you'll

147
00:16:13,560 --> 00:16:15,040
see things as they are.

148
00:16:15,040 --> 00:16:20,960
Once you see things as they are, you will agree with us that they are this way, that we

149
00:16:20,960 --> 00:16:24,960
can all come to an agreement on the way things are.

150
00:16:24,960 --> 00:16:31,880
A big part of this, as I've mentioned before, is a paradigm shift, and that's what I tried

151
00:16:31,880 --> 00:16:37,480
to explain in the second volume on how to meditate, and I think the first chapter, one

152
00:16:37,480 --> 00:16:45,000
of the first chapters, is that we tend to look at reality in terms of

153
00:16:45,000 --> 00:16:49,360
the world around us, obviously, and who doesn't, right?

154
00:16:49,360 --> 00:16:54,840
If you ask what is real of this room, you know, we're sitting in this room, but in fact,

155
00:16:54,840 --> 00:16:58,120
that's really just a projection.

156
00:16:58,120 --> 00:17:05,240
All of that is still dependent upon something more basic, and that's where we shift our

157
00:17:05,240 --> 00:17:06,240
focus.

158
00:17:06,240 --> 00:17:10,880
So instead of looking at the world in terms of the room around us, we look at in terms

159
00:17:10,880 --> 00:17:18,520
of our perception of what then becomes the room around us, and so I experience when

160
00:17:18,520 --> 00:17:25,840
base reality on experience, and that's the beginning of right view.

161
00:17:25,840 --> 00:17:31,880
Because without that, there's no way we can be sure that we're all going to come to

162
00:17:31,880 --> 00:17:37,640
the same conclusion, because the room here is abstract.

163
00:17:37,640 --> 00:17:41,880
Some people think it's nice, I don't like it.

164
00:17:41,880 --> 00:17:45,440
Our perception of the room can be quite different, some people looking at it from under

165
00:17:45,440 --> 00:17:47,760
actions, some people from in it.

166
00:17:47,760 --> 00:17:58,000
This is what leads to, in the world that leads to so much conflict and confusion and

167
00:17:58,000 --> 00:18:02,160
misunderstanding and so on.

168
00:18:02,160 --> 00:18:07,480
But you can't have that when you look at experience, experience is more basic, and it

169
00:18:07,480 --> 00:18:09,960
doesn't change based on your perception of it.

170
00:18:09,960 --> 00:18:11,120
It is what it is.

171
00:18:11,120 --> 00:18:19,040
You might want it to be otherwise, but it is what is real.

172
00:18:19,040 --> 00:18:23,400
So that's what comes and goes and changes.

173
00:18:23,400 --> 00:18:31,880
That's a deep tube body, certainly.

174
00:18:31,880 --> 00:18:37,200
Number four, we move deep bodies with ease, as purification of freedom or purification

175
00:18:37,200 --> 00:18:40,320
through freedom, maybe.

176
00:18:40,320 --> 00:18:41,320
It's the fourth.

177
00:18:41,320 --> 00:18:45,640
Once you have right view, right view is for what, right view is for freedom.

178
00:18:45,640 --> 00:18:52,240
We want to see things as they are, so we can free ourselves from the prison that we've

179
00:18:52,240 --> 00:18:58,240
placed ourselves in, this prison of desire and aversion and delusion.

180
00:18:58,240 --> 00:19:07,160
We trap ourselves, we bind ourselves to certain the way we think things should be and the

181
00:19:07,160 --> 00:19:15,880
way we think things shouldn't be by the things we think I am and the things I am not,

182
00:19:15,880 --> 00:19:27,560
the things I want to be, the things I don't want to be, that kind of thing.

183
00:19:27,560 --> 00:19:36,440
And so there's fourth one, and then there's this, with the other three, with purification

184
00:19:36,440 --> 00:19:50,960
of Sila, Jita, and Diti, one Raja, Raja, Niyi, Sudhame, Sudhita, Niyi Raja, Diti, one

185
00:19:50,960 --> 00:19:58,560
becomes dispassionate about dhammas, about things that one might be passionate about.

186
00:19:58,560 --> 00:20:03,000
We more Jani, Sudhame, Sudhita, Niyi, Sudhame, Sudhita, Niyi, Diti, one frees one's house

187
00:20:03,000 --> 00:20:09,200
from dhammas, from things that one should free oneself from.

188
00:20:09,200 --> 00:20:17,080
And having done so, Sama, we mutting positive one touches or attains right release, right

189
00:20:17,080 --> 00:20:18,080
freedom.

190
00:20:18,080 --> 00:20:23,280
We mutting positive, we mutting positive, we mutting positive, this is called purification,

191
00:20:23,280 --> 00:20:28,400
so we release around release.

192
00:20:28,400 --> 00:20:45,920
So these are the four Pari, Sudhita, Niyi, that we're taught by that, blessed one who

193
00:20:45,920 --> 00:20:55,080
is nose and seers and enlightened self, a self enlightened Buddha.

194
00:20:55,080 --> 00:21:01,320
They were taught for the purification of beings, for the overcoming of sorrow, lamentation

195
00:21:01,320 --> 00:21:14,600
and despair, for the eradication, for the overcoming of, so I'm going to take a

196
00:21:14,600 --> 00:21:24,240
sigh of the destruction of physical pain and mental pain, for giving up for free oneself,

197
00:21:24,240 --> 00:21:33,160
for attaining the right path and for seeing it for oneself, nibhana.

198
00:21:33,160 --> 00:21:36,960
That's a standard, that's actually what the Buddha taught, the Buddha, it's a quote from

199
00:21:36,960 --> 00:21:45,720
the Bahni Phaataan say Tha anangvizu�idya, Soka paretan in Pakistan at

200
00:21:45,720 --> 00:21:51,200
younger man 5.

201
00:21:51,200 --> 00:22:11,980
There's a lot of these where the Buddha would, and then there's taking this from the Buddha's

202
00:22:11,980 --> 00:22:22,860
teaching, and this is four things that really condense the path, because when you're teaching you have to

203
00:22:22,860 --> 00:22:31,960
remind people the road map, the direction, where are we going? And this is a complete road map. You start

204
00:22:31,960 --> 00:22:39,320
with Sila and then with morality, you train yourself in right behavior and right speech, to keep

205
00:22:39,320 --> 00:22:45,720
yourself ethical, moral virtues, and then you begin to focus the mind, and that's what you practice

206
00:22:45,720 --> 00:22:51,280
meditation. And as you do that, you start to see things clearly. You start to see what are the

207
00:22:51,280 --> 00:22:59,920
four noble truths in the end. You see Nama, Rupa, you see three characteristics, you see the four

208
00:22:59,920 --> 00:23:09,880
noble truths, and then finally we mutty have become free, quite simple. It's good to have these

209
00:23:09,880 --> 00:23:19,120
guides, because it keeps us, it reminds us of very straight and straightforward nature of the

210
00:23:19,120 --> 00:23:25,420
Buddha's teaching, not getting caught up in details or getting off track. So easy for us to get

211
00:23:25,420 --> 00:23:32,860
in, let our minds allow us to wander and get lost, and from the wrong path, the path that

212
00:23:32,860 --> 00:23:42,160
leads to stress and suffering. It's good for us to remember these things. Okay, so that's the

213
00:23:42,160 --> 00:23:59,500
Nama for this evening. Do we have any questions?

214
00:23:59,500 --> 00:24:15,560
Okay, who couldn't quit talking even when another guy was shooting dung into his room? Huh,

215
00:24:15,560 --> 00:24:33,800
that's from the Jatakas. Which Jatakana? Are fully enlightened beings a moral, a good question?

216
00:24:33,800 --> 00:24:51,680
No, I wouldn't, I guess I wouldn't say that. It really depends how you look at it, and it's

217
00:24:51,680 --> 00:25:07,680
just words, right? And so some people might want to say that. Some people might want to say

218
00:25:07,680 --> 00:25:15,640
that, but they're actually quite moral. The enlightened being is that it keeps the precepts

219
00:25:15,640 --> 00:25:25,840
purely and will not break from them. But that's, but that's really because there's a sense

220
00:25:25,840 --> 00:25:41,480
that morality is intrinsic to reality. Reality is not a moral. Reality is moral. So from

221
00:25:41,480 --> 00:25:49,560
a Dhamapada, I don't think the story itself, maybe it is. Pretty sure it's from the Jatakas,

222
00:25:49,560 --> 00:26:00,680
though. Let me see. I'm pretty sure it's a Jatakas story. Maybe in the Dhamapada as well.

223
00:26:00,680 --> 00:26:11,200
I mean, I may be wrong. But I know the story. I can't think off the top of my head, but the

224
00:26:11,200 --> 00:26:23,160
exact Jatakana. Well, the Jatakas are on the internet. Just read them. Read them and tell

225
00:26:23,160 --> 00:26:35,800
us which one it is. There's only five hundred and forty seven of them. Fifty seven. I can

226
00:26:35,800 --> 00:26:57,080
probably find them yet. I have that.

227
00:27:05,800 --> 00:27:32,000
Yeah. When we drop the ball of Dhamapada.

228
00:27:32,000 --> 00:27:56,120
There's too many of them. There's too many of them. Maybe there's some Dhamapada. If anybody

229
00:27:56,120 --> 00:28:16,400
finds it, please pass along. Yeah, I'm pretty sure because it starts with trying to get

230
00:28:16,400 --> 00:28:26,280
this guy to be quiet. Every time he talks, he should follow Dhamapada into his mouth. I'm

231
00:28:26,280 --> 00:28:33,880
pretty sure it's a cause for telling a story of the past. Pretty sure it's a cause for a Jatakas,

232
00:28:33,880 --> 00:28:50,440
for telling a Jatakas. Anyway, yes, we should purify our virtue and we have to work at it.

233
00:28:50,440 --> 00:29:11,600
Anyway, any other questions? Not, and so you have a good night. See you all tomorrow.

234
00:29:11,600 --> 00:29:28,320
Okay, good night.

