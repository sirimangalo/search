1
00:00:00,000 --> 00:00:21,000
If one is able to be mindful of thoughts, should one still ignore them and try to be only mindful of breathing or can one meditate using mindfulness of thoughts, or in general mindfulness of anything that arises.

2
00:00:21,000 --> 00:00:36,000
In general, it could be said, be mindful on that which is the most prominent in the present moment.

3
00:00:36,000 --> 00:00:50,000
When the breath is the most prominent on the breath, if there is anything else, if there is thinking going on, of course, say a thinking, thinking, thinking, thinking, thinking.

4
00:00:50,000 --> 00:01:02,000
And everything else that occurs anywhere in the body or in the mind can be noticed.

5
00:01:02,000 --> 00:01:11,000
We have the four foundations of mindfulness, body, feeling, mind and mind objects.

6
00:01:11,000 --> 00:01:35,000
So there is nothing wrong to note mindfulness of thoughts or to be mindful of breathing.

7
00:01:35,000 --> 00:01:46,000
The only one condition I would add is, if there is nothing prominent, we tend to always go back to the breath.

8
00:01:46,000 --> 00:01:54,000
And this is why people miss, this is a question that is asked more common than one might think.

9
00:01:54,000 --> 00:02:03,000
Because there is the misunderstanding that we are encouraging people to ignore things that are practiced somehow, ignore it, stick with the breath.

10
00:02:03,000 --> 00:02:05,000
And that is not what we teach.

11
00:02:05,000 --> 00:02:16,000
The teaching is that when there is nothing prominent, when there is nothing else to be mindful of, in order to stay in the present moment and not get lost in thought, come back again to the breath.

12
00:02:16,000 --> 00:02:20,000
And stick with it, develop because you are developing wholesome qualities.

13
00:02:20,000 --> 00:02:26,000
Just fall naturally back into what is here, what is now, what is going on.

14
00:02:26,000 --> 00:02:40,000
Because it is a good exercise, it is something that is prominent, something that is obvious, something that, as I have said before, can have incredible results in terms of teaching about impermanent suffering and so.

15
00:02:40,000 --> 00:02:48,000
The one complaint people always have, as I have said before about this practice, is how unpleasant it is to watch the stomach.

16
00:02:48,000 --> 00:03:00,000
You feel like you are forcing someone asked even today on the forum, can I do Tibetan mantras because this rising falling thing, I just feel like I am controlling the breath and I can't stop controlling it.

17
00:03:00,000 --> 00:03:11,000
That is the point, the point is to see that, to see, first of all, that you are controlling it, that you can't control the controlling, and that the controlling leads to suffering.

18
00:03:11,000 --> 00:03:19,000
The purpose of the practice is wisdom is insight, to see things as they are, not to develop anything, to cultivate anything, to create anything.

19
00:03:19,000 --> 00:03:23,000
The only thing we are trying to create is clear awareness and understanding.

20
00:03:23,000 --> 00:03:30,000
So, the more you see that you are being stupid about this, that you are practicing incorrectly, the better for you.

21
00:03:30,000 --> 00:03:35,000
Because that is the way you react to everything, that is how you react in daily life.

22
00:03:35,000 --> 00:03:41,000
That is how you react to any experience.

23
00:03:41,000 --> 00:03:48,000
So, gotten totally off track here, but interesting anyway.

24
00:03:48,000 --> 00:03:51,000
The stomach is a very useful object for this.

25
00:03:51,000 --> 00:03:55,000
It allows you to see these things quite clearly.

26
00:03:55,000 --> 00:04:02,000
And it may be often because of our aversion to looking at something like this and to see these things.

27
00:04:02,000 --> 00:04:08,000
That we feel like it is somehow ignoring the rest of the experience and make them up with this question.

28
00:04:08,000 --> 00:04:15,000
So, be careful and be clear that, of course, you can focus on the thoughts, but it is going to have the same results.

29
00:04:15,000 --> 00:04:18,000
When you focus on the stomach, you are going to see impermanence suffering and not self.

30
00:04:18,000 --> 00:04:19,000
Not much fun.

31
00:04:19,000 --> 00:04:22,000
When you focus on the thoughts, you are also going to experience that.

32
00:04:22,000 --> 00:04:27,000
You are going to feel like you are trying to control your thoughts, like you can't control your thoughts, you are thinking too much.

33
00:04:27,000 --> 00:04:32,000
It is not comfortable, it is not permanent, it is not stable, how horrible.

34
00:04:32,000 --> 00:04:35,000
Until finally, you see it again and again.

35
00:04:35,000 --> 00:04:41,000
And you realize how mistaken you are and how wrong your behavior is when you let go.

36
00:04:41,000 --> 00:04:55,000
You adjust and you start to just let the thoughts go and not dwell on them, not create any ideas about them or opinions or judgments about them and so on.

37
00:04:55,000 --> 00:05:01,000
So, yeah, but Palangani had already answered the question itself.

