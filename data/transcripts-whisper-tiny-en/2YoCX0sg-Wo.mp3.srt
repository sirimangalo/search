1
00:00:00,000 --> 00:00:02,760
Okay, welcome back to Ask a Monk.

2
00:00:02,760 --> 00:00:14,040
Next question on good books to study on Buddhism and what parts of the Topitika I would recommend.

3
00:00:14,040 --> 00:00:19,960
I'm often cautious to recommend anything because there's such a wide audience out there

4
00:00:19,960 --> 00:00:35,040
and I think my tastes are rather specific or specific, but if you agree with the sorts of things

5
00:00:35,040 --> 00:00:43,680
that I teach and say and believe and purport to propound, if you appreciate the meditation

6
00:00:43,680 --> 00:00:47,800
teachings that I've posted and you think that's a good method, then I would recommend

7
00:00:47,800 --> 00:00:51,160
to look into similar teachings.

8
00:00:51,160 --> 00:01:01,800
The best by far in my mind that anyone could ever read after they've started this sort

9
00:01:01,800 --> 00:01:09,880
of meditation is the teachings of Mahasizaya.ma, H-A-S-I, one word, and then the next word

10
00:01:09,880 --> 00:01:18,240
Sayada, S-A-D-A-Y-A-W. Sayada means teacher, Mahasizaya actually means big drum, it was the

11
00:01:18,240 --> 00:01:23,920
name of the place where he became a teacher, but that's the name he's known by and he's passed

12
00:01:23,920 --> 00:01:31,640
away, but he was an excellent teacher and well revered, there's a website with his books

13
00:01:31,640 --> 00:01:34,160
in a PDF form.

14
00:01:34,160 --> 00:01:39,280
The thing about him is he was both scholarly and meditative, so in Burma there are scholars

15
00:01:39,280 --> 00:01:45,120
that are mind-voggling, they can memorize the whole tupitika to an extent.

16
00:01:45,120 --> 00:01:53,920
Mahasizaya wasn't such a person, but he was very well-learned, even by Burmese standards,

17
00:01:53,920 --> 00:02:01,200
but his focus was meditation, so even though he was a novice, he went through all the studies.

18
00:02:01,200 --> 00:02:05,480
Later on he realized that the most important thing was to practice them, which is not common

19
00:02:05,480 --> 00:02:14,320
actually, my surprise people, but many monks fail to realize that the most important thing

20
00:02:14,320 --> 00:02:18,040
about the Buddha's teaching is the practice of it, and go on to practice, so he went

21
00:02:18,040 --> 00:02:26,840
off to practice with his monk in the cave, and that's when he really started to teach,

22
00:02:26,840 --> 00:02:31,640
to give something, and so these books that he's written are all after that point when

23
00:02:31,640 --> 00:02:38,400
he started practicing and teaching, and realized this sort of way of practice, the benefit

24
00:02:38,400 --> 00:02:44,920
of it, and he's helped millions of people directly and indirectly with his books, with his

25
00:02:44,920 --> 00:02:52,000
teachings, and their exceptional, I've seen many teachers, I'm not going to talk about

26
00:02:52,000 --> 00:02:55,080
teachers in other traditions, because I don't read them, and you're asking me so I

27
00:02:55,080 --> 00:03:01,960
wouldn't recommend them, not that there's necessarily anything wrong with them, but even

28
00:03:01,960 --> 00:03:07,520
in this tradition, I've never read or come across any teacher who's able to put the

29
00:03:07,520 --> 00:03:14,400
Buddha's teaching together in such a masterful way, so that's what I would recommend

30
00:03:14,400 --> 00:03:19,920
as far as what is not, you know, direct teachings of the Buddha.

31
00:03:19,920 --> 00:03:26,920
As far as the Topithika and what are the direct teachings of the Buddha, I would recommend

32
00:03:26,920 --> 00:03:34,760
the Majimani Kaya, I think, because it's not too deep, and by that I simply mean there's

33
00:03:34,760 --> 00:03:40,160
not too much, because if you look at, say, the Sanyuta Nikaya, you've got millions of

34
00:03:40,160 --> 00:03:45,840
teachings, or non-millions, but thousands of teachings, and it's really difficult to digest,

35
00:03:45,840 --> 00:03:53,840
but on the other hand it's broad enough with 152 discourses, the Majimani Kaya, the middle

36
00:03:53,840 --> 00:04:00,480
length discourses of the Buddha, translation by Bhikkhu Bodhi, easy to find on the internet.

37
00:04:00,480 --> 00:04:03,240
I would recommend reading that.

38
00:04:03,240 --> 00:04:11,680
Also the Diga Nikaya, but I think maybe the translation available is not as good.

39
00:04:11,680 --> 00:04:18,400
I would recommend actually all of the Topithika, I suppose, but you know, the start with

40
00:04:18,400 --> 00:04:22,520
the Majimani Kaya, that's all I would say, but keep reading.

41
00:04:22,520 --> 00:04:27,320
The problem, of course, is that translations are always makeshift, even good translations,

42
00:04:27,320 --> 00:04:34,160
so in the end you're better off to learn Pali and to start to actually read with the

43
00:04:34,160 --> 00:04:39,320
Buddha taught, and then even just reading a few suitors is quite profound because it's

44
00:04:39,320 --> 00:04:42,040
in the original language.

45
00:04:42,040 --> 00:04:49,080
If you can't get to that point, then go with the translations, start with the Majimani Kaya.

46
00:04:49,080 --> 00:04:53,880
Another thing that the Buddha didn't, that isn't Buddha watch, and I, in terms of the actual

47
00:04:53,880 --> 00:05:00,200
words of the Buddha, but is based on the commentaries, is the Visudimaga or the path of

48
00:05:00,200 --> 00:05:08,960
purification, which if you follow the sort of teaching that I teach and profound, is a

49
00:05:08,960 --> 00:05:15,520
great guide, it's a completely practical guide to the development of not only the meditation

50
00:05:15,520 --> 00:05:20,480
that I follow, but many different kinds of meditation, and you'll get a good overview of

51
00:05:20,480 --> 00:05:26,080
the different kinds of meditation it talks about, just about everything, there's detailed

52
00:05:26,080 --> 00:05:31,160
descriptions of how to gain magical powers and fly through the air and read people's minds

53
00:05:31,160 --> 00:05:40,880
and remember past lives and all these things that are good, but superfluous, I suppose,

54
00:05:40,880 --> 00:05:46,560
not necessary, but are nice, interesting things to study about.

55
00:05:46,560 --> 00:06:16,400
So a good book, Visudimaga Path of Purification, I hope that helps, thanks for the question.

