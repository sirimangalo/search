1
00:00:00,000 --> 00:00:07,800
So everyone, just a short announcement that we will be starting to broadcast

2
00:00:07,800 --> 00:00:13,080
a month radio from my YouTube channel. So that's why you're being redirected here

3
00:00:13,080 --> 00:00:19,880
if you've gone to radio.serimongolow.org. This YouTube channel is my YouTube

4
00:00:19,880 --> 00:00:24,160
channel is now going to hopefully be the location for future month radio

5
00:00:24,160 --> 00:00:30,800
broadcast because we're now able to use Google's facilities for that

6
00:00:30,800 --> 00:00:36,640
purpose. So I hope to see you there this evening. 7.30 p.m. Sri Lankan time,

7
00:00:36,640 --> 00:00:44,920
which is 2 p.m. UTC and you have to figure out the appropriate time in your area.

8
00:00:44,920 --> 00:00:49,680
So I hope to see you there. It's the same time as normal, so if you've been visiting

9
00:00:49,680 --> 00:00:54,200
if you've been joining before, then hopefully it will be a smooth transition.

10
00:00:54,200 --> 00:01:21,080
So I'm looking forward to it and see you all later. Thanks.

