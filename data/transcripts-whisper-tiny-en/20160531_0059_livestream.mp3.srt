1
00:00:00,000 --> 00:00:20,000
Good evening, everyone broadcasting live May 30th.

2
00:00:20,000 --> 00:00:27,760
This will be our last broadcast for a while.

3
00:00:27,760 --> 00:00:35,760
We'll do some audio broadcasts from Asia, but probably no video broadcasts for a while.

4
00:00:35,760 --> 00:00:47,760
I'm heading off to Thailand, Sri Lanka, and maybe even Taiwan, but I'll be back in June

5
00:00:47,760 --> 00:00:54,760
June 29th, so for a whole month.

6
00:00:54,760 --> 00:01:06,760
I'm going to see my teacher to just to let him know about our new monastery, our new center here,

7
00:01:06,760 --> 00:01:14,760
to get his blessing, to offer him some robes,

8
00:01:14,760 --> 00:01:25,760
and make a stop in Sri Lanka, just because it's Sri Lanka.

9
00:01:25,760 --> 00:01:32,760
Do some teaching in Sri Lanka as well, I guess.

10
00:01:32,760 --> 00:01:38,760
Then come back, and there's lots of people already signed up for meditation courses in July and August,

11
00:01:38,760 --> 00:01:45,760
so lots to do over the summer.

12
00:01:45,760 --> 00:01:52,760
Today's quote is about bathing.

13
00:01:52,760 --> 00:01:56,760
And this quote comes from the what,

14
00:01:56,760 --> 00:02:04,760
the whatupa mess would discourse on the simile of the cloth,

15
00:02:04,760 --> 00:02:21,760
just a rather famous suit among Buddhists that talks about purity.

16
00:02:21,760 --> 00:02:30,760
It's an important simile that a clean cloth, a pure cloth,

17
00:02:30,760 --> 00:02:34,760
can be dyed in any color or dye,

18
00:02:34,760 --> 00:02:37,760
and it takes the dye of what, it takes the color well,

19
00:02:37,760 --> 00:02:40,760
because it's beautiful no matter what color you dye in it,

20
00:02:40,760 --> 00:02:44,760
and you dip it in.

21
00:02:44,760 --> 00:02:52,760
It's a dirty cloth, an impure cloth.

22
00:02:52,760 --> 00:02:55,760
Likewise, it doesn't matter what dye you dip it in.

23
00:02:55,760 --> 00:03:06,760
It's not going to be beautiful, it doesn't matter what color.

24
00:03:06,760 --> 00:03:15,760
What is the cause? Why is that? Because of the purity of the cloth,

25
00:03:15,760 --> 00:03:16,760
and the mind is the same.

26
00:03:16,760 --> 00:03:19,760
And he gives this simile,

27
00:03:19,760 --> 00:03:44,760
and he also gives it a sugative patikanga.

28
00:03:44,760 --> 00:03:49,760
An unpleasant future,

29
00:03:49,760 --> 00:03:52,760
patikanga without doubt,

30
00:03:52,760 --> 00:03:57,760
is to be had without doubt.

31
00:03:57,760 --> 00:04:02,760
Jiteya Sanghidite for a mind that is undefiled,

32
00:04:02,760 --> 00:04:11,760
that is pure happiness is future without doubt.

33
00:04:11,760 --> 00:04:18,760
Life is unsure of how do I make TV down.

34
00:04:18,760 --> 00:04:21,760
Think of all the changes in our lives,

35
00:04:21,760 --> 00:04:25,760
all the changes we've gone through,

36
00:04:25,760 --> 00:04:32,760
all the uncertainty we've had to face.

37
00:04:32,760 --> 00:04:42,760
And without a guide, without a compass of some sort,

38
00:04:42,760 --> 00:04:49,760
the future is very much uncertain.

39
00:04:49,760 --> 00:04:51,760
And seeing this and knowing this,

40
00:04:51,760 --> 00:04:58,760
we try to find, we try to direct and control our futures.

41
00:04:58,760 --> 00:05:03,760
But mostly at the way we work it is based on what we want,

42
00:05:03,760 --> 00:05:08,760
based on our preconceived desires and ideas.

43
00:05:08,760 --> 00:05:16,760
And that doesn't ever make us happy.

44
00:05:16,760 --> 00:05:20,760
We try to get happiness,

45
00:05:20,760 --> 00:05:26,760
but trying to get happiness doesn't make us happy.

46
00:05:26,760 --> 00:05:31,760
Well, that's just make us want happiness more.

47
00:05:31,760 --> 00:05:36,760
So there's nothing wrong with wanting happiness, right?

48
00:05:36,760 --> 00:05:43,760
Theoretically, wanting happiness is the right idea.

49
00:05:43,760 --> 00:05:45,760
Everyone should want to be happy.

50
00:05:45,760 --> 00:05:54,760
You can open the window if you want.

51
00:05:54,760 --> 00:05:57,760
We go about it the wrong way.

52
00:05:57,760 --> 00:06:03,760
We put a tad purity for a very important reason.

53
00:06:03,760 --> 00:06:07,760
What she states in this, she makes clear.

54
00:06:07,760 --> 00:06:11,760
Which is why we teach the concept of karma.

55
00:06:11,760 --> 00:06:15,760
It's not to make us unhappy.

56
00:06:15,760 --> 00:06:18,760
The law of karma is to help us find happiness.

57
00:06:18,760 --> 00:06:21,760
It's to provide that compass.

58
00:06:21,760 --> 00:06:24,760
You don't have to worry about so many things in life.

59
00:06:24,760 --> 00:06:26,760
You don't have to worry about what you're going to get.

60
00:06:26,760 --> 00:06:28,760
It's going to come your way.

61
00:06:28,760 --> 00:06:30,760
It's going to change.

62
00:06:30,760 --> 00:06:35,760
Our lives are always going to change.

63
00:06:35,760 --> 00:06:39,760
All we have to worry about is the source.

64
00:06:39,760 --> 00:06:47,760
We can't control, we can't be sure what's going to come to us in life.

65
00:06:47,760 --> 00:06:50,760
We can be sure of one thing.

66
00:06:50,760 --> 00:06:54,760
If the mind is pure, we'll be happy in the future.

67
00:06:54,760 --> 00:06:58,760
If the mind is impure, we will suffer from it.

68
00:06:58,760 --> 00:07:00,760
We'll be upset.

69
00:07:00,760 --> 00:07:01,760
We'll be unhappy.

70
00:07:01,760 --> 00:07:04,760
We'll have expectations that are unmet.

71
00:07:04,760 --> 00:07:10,760
We'll disappointments and displeasure.

72
00:07:10,760 --> 00:07:14,760
But if the mind is pure, we will do good things for ourselves.

73
00:07:14,760 --> 00:07:16,760
We will do good things to help others.

74
00:07:16,760 --> 00:07:19,760
We will make good friends wherever we go.

75
00:07:19,760 --> 00:07:21,760
So who those friends will be?

76
00:07:21,760 --> 00:07:30,760
Won't be able to choose necessarily.

77
00:07:30,760 --> 00:07:33,760
But we will choose those who we meet.

78
00:07:33,760 --> 00:07:40,760
We will choose to associate with those who we meet to our pure.

79
00:07:40,760 --> 00:07:45,760
If our minds are impure, we will choose to associate with those who are impure.

80
00:07:45,760 --> 00:07:53,760
We'll hurt ourselves, we'll hurt each other.

81
00:07:53,760 --> 00:07:55,760
Goodness, purity.

82
00:07:55,760 --> 00:08:04,760
All these things that we fail often to grasp, to grasp all of them.

83
00:08:04,760 --> 00:08:07,760
The right in front of us.

84
00:08:07,760 --> 00:08:13,760
We all know what goodness is, we all know what purity is.

85
00:08:13,760 --> 00:08:16,760
We fail to see it.

86
00:08:16,760 --> 00:08:19,760
We fail to grasp.

87
00:08:19,760 --> 00:08:24,760
And so the Buddha gives this discourse and he talks about related things.

88
00:08:24,760 --> 00:08:27,760
It's a very good discussion, very much worth reading.

89
00:08:27,760 --> 00:08:38,760
But then he gets to this quote and he talks about how one becomes pure and through insight meditation and so on.

90
00:08:38,760 --> 00:09:08,760
But then he says,

91
00:09:08,760 --> 00:09:15,760
And he uses the word bathing specifically because there's this one guy sitting in his audience.

92
00:09:15,760 --> 00:09:23,760
So he wants to reach out to him because the commentary says he knows that this guy is going

93
00:09:23,760 --> 00:09:26,760
as the potential to become in light.

94
00:09:26,760 --> 00:09:28,760
So he specifically mentions this.

95
00:09:28,760 --> 00:09:37,760
And right away this Brahman, Sundarika, Bharadwarja, he asks the Buddha a question.

96
00:09:37,760 --> 00:09:40,040
And he says,

97
00:09:40,040 --> 00:09:46,480
plants are going a very important way to consider what the Buddhists spoke to.

98
00:09:46,480 --> 00:10:04,040
When they

99
00:10:04,040 --> 00:10:05,080
the Buddha says,

100
00:10:05,080 --> 00:10:13,840
He says, what's with the Bahuqah river Brahman?

101
00:10:13,840 --> 00:10:17,600
What's so special about it?

102
00:10:17,600 --> 00:10:24,800
What is, what will the Bahuqah river do?

103
00:10:24,800 --> 00:10:25,800
What is it?

104
00:10:25,800 --> 00:10:29,760
What good is it?

105
00:10:29,760 --> 00:10:36,760
He says, what is the Bahuqah river?

106
00:10:36,760 --> 00:10:45,760
What is the Bahuqah river?

107
00:10:45,760 --> 00:11:14,480
The Bahuqah river is understood, is agreed to some at that, some at that means it's agreed

108
00:11:14,480 --> 00:11:36,960
to be, it's thought to be liberating, it's thought to be, it's thought to be before goodness

109
00:11:36,960 --> 00:11:53,720
by many people, it's thought to remove, to wash away, see, the Bahuqah river means evil

110
00:11:53,720 --> 00:12:03,560
demons, evil deeds that have been done and it's thought to wash them away.

111
00:12:03,560 --> 00:12:10,120
This was a thing in India, using this sort of language is important to address the idea

112
00:12:10,120 --> 00:12:15,720
that it's still prevalent in India that the rivers will wash away your evil deeds, wash

113
00:12:15,720 --> 00:12:19,040
away your impurities.

114
00:12:19,040 --> 00:12:24,760
And again, talking about faith, faith does this, people have faith in this, I actually

115
00:12:24,760 --> 00:12:29,840
cultivate wholesomeness because they have this faith, unfortunately, they cultivate also

116
00:12:29,840 --> 00:12:35,200
a lot of unwholesomeness with their wrong views, which causes them to ignore the importance

117
00:12:35,200 --> 00:12:43,520
of actually getting beyond things like faith, because faith is good, not nearly enough,

118
00:12:43,520 --> 00:12:48,960
especially when it's in the wrong things.

119
00:12:48,960 --> 00:12:54,760
So Buddha shakes his head, I assume, and I don't think he would shake his head, but

120
00:12:54,760 --> 00:13:01,040
Buddha says, all these rivers do nothing.

121
00:13:01,040 --> 00:13:10,640
Nitham Pibhalo, Pakhando, Kankakamo, Nesujati, Kankakamo, Nesujati, Kankakamo, Nesujati, if one

122
00:13:10,640 --> 00:13:18,520
is to constantly consistantly, forever, if one is ever to be in these rivers, all these

123
00:13:18,520 --> 00:13:28,840
bhukha river, the adi-kakant, adi-kakha river, Gai river, Sundarikarana, Kankakamo, Nesujati,

124
00:13:28,840 --> 00:13:36,320
it can't purify black kama, evil deeds.

125
00:13:36,320 --> 00:13:40,160
King Sundarikha Krizati, what will the Sundarikha river do?

126
00:13:40,160 --> 00:13:46,760
King Bhagai, Payaga, over the Payaga river do, over the bhukha river do, over the bhukha river

127
00:13:46,760 --> 00:14:12,520
where in Kata, Kibhizanarana, Nahinansodhi, Babakaminam, Kibhizan, Kibhizan is cruel, he is

128
00:14:12,520 --> 00:14:20,240
cruel, they cannot purify an evil doer, a man who has done cruel and brutal deeds.

129
00:14:20,240 --> 00:14:23,840
So what is true purity?

130
00:14:23,840 --> 00:14:36,600
Sudhasa vaisandha, Bhagum, Bhagu, Sudhasu bhukhizadha, one pure and hard as evermore, the feast

131
00:14:36,600 --> 00:14:50,520
of spring, the holy day, Bhagu is a special holy day, so for brahmanas on the moon

132
00:14:50,520 --> 00:15:01,080
of Bhagu, Bhaguna, they have a day of purification, it's a special purification day and

133
00:15:01,080 --> 00:15:07,120
probably an Hinduism, they still consider it to be a day of purification, so the Buddha

134
00:15:07,120 --> 00:15:14,920
says one who is pure of mind is always, always has a holy day.

135
00:15:14,920 --> 00:15:20,720
One fair and act, one pure and hard brings his virtue to perfection, it is here brahmana

136
00:15:20,720 --> 00:15:25,560
that you should be to make yourself a refuge for all beings.

137
00:15:25,560 --> 00:15:33,440
If you speak no false hood nor work harm for living beings nor take what is offered not,

138
00:15:33,440 --> 00:15:39,520
with faith and free from avarice would need for you to go to Gaya, for any well will

139
00:15:39,520 --> 00:15:44,880
be your Gaya.

140
00:15:44,880 --> 00:15:58,160
If you speak in Gaya, you speak in Gaya, you speak in Gaya, you speak in Gaya.

141
00:15:58,160 --> 00:16:02,240
Actually, I think Tamika gets it better than Bodhi.

142
00:16:02,240 --> 00:16:06,680
I'm not sure my Bodhi translates to that.

143
00:16:06,680 --> 00:16:19,440
Bodhupana means well, your well will be your Gaya, might as well be Gaya, meaning the

144
00:16:19,440 --> 00:16:29,040
water in your well is the same as Gaya, it's the same water, it's just water and it means

145
00:16:29,040 --> 00:16:36,560
that you have Gaya everywhere you go, this idea of purification.

146
00:16:36,560 --> 00:16:41,760
People have strange ideas of purification, a lot of people believe in the importance of purifying

147
00:16:41,760 --> 00:16:52,680
the body, it's funny how much emphasis we put on the body, healthy body, healthy mind

148
00:16:52,680 --> 00:17:02,080
they say, which you know arguably there is some benefit to having a healthy body or taking

149
00:17:02,080 --> 00:17:06,480
care of your body, but it's funny that no one thinks to actually make the mind

150
00:17:06,480 --> 00:17:10,960
healthy or make the mind pure, and it's not true, but we put far less emphasis on making

151
00:17:10,960 --> 00:17:13,240
the mind healthy.

152
00:17:13,240 --> 00:17:19,240
The mind we want just to get what we want, even our spirituality tends to be based on seeking

153
00:17:19,240 --> 00:17:27,480
pleasure, seeking happiness, which you know, as I said, it's a sense of the good to want

154
00:17:27,480 --> 00:17:28,480
to be happy, right?

155
00:17:28,480 --> 00:17:34,360
Nobody wants to suffer, problem is one thing isn't enough and if your focus is happiness

156
00:17:34,360 --> 00:17:43,200
you'll never be happy, it's funny how your focus is trying to find happiness, your focus

157
00:17:43,200 --> 00:17:52,040
is the happiness, it's a problem because happiness doesn't need to happiness, happiness

158
00:17:52,040 --> 00:18:01,280
might lead to, it's a good, it might lead to bad, it's not happiness that leads to happiness

159
00:18:01,280 --> 00:18:06,480
just like it's not suffering that leads to suffering, it's goodness that leads to happiness,

160
00:18:06,480 --> 00:18:15,240
it's evil that leads to suffering, this is why we don't have to worry about our future,

161
00:18:15,240 --> 00:18:20,080
we can't be sure what it's going to be like, sometimes that's disconcerting to know

162
00:18:20,080 --> 00:18:28,520
that our future might not be what we thought it was going to be, so we have to reassess

163
00:18:28,520 --> 00:18:35,080
and re-adjust, re-evaluate what's important about life, it's not the situation, it's not

164
00:18:35,080 --> 00:18:41,320
whether I go to Asia or not, it's not whether I have a meditation center here in Hamilton

165
00:18:41,320 --> 00:18:50,440
or somewhere else, whether I get this job or that job, it's not whether I have these friends

166
00:18:50,440 --> 00:19:00,400
or this family, it's about your mind, it's about your outlook, it's about what you bring

167
00:19:00,400 --> 00:19:08,120
to the situation, how you approach it, how you live, how you are, who you are, what

168
00:19:08,120 --> 00:19:16,200
you are, are you pure in mind with good intentions for yourself and others, or corrupt

169
00:19:16,200 --> 00:19:22,000
in mind with evil intentions for yourself and others, this is what we have to look at, this

170
00:19:22,000 --> 00:19:29,360
is what meditation is all about, it's about cultivating this purity, striving to be pure

171
00:19:29,360 --> 00:19:38,760
in heart, pure in mind, that's our quote for tonight, it's about bathing, we should all

172
00:19:38,760 --> 00:19:54,600
be sure to bathe internally, let's see if we have some questions, okay we have a question

173
00:19:54,600 --> 00:20:00,240
about karma, we have a bunch of green people which is always nice to see, most of you

174
00:20:00,240 --> 00:20:06,400
are green, thank you, that's appreciated, we meditate together, sometimes it's a hardship

175
00:20:06,400 --> 00:20:11,120
to have to meditate at the same time and I know for many people it's not possible because

176
00:20:11,120 --> 00:20:16,640
we're a global, it's middle of the night for some people I understand, but that people

177
00:20:16,640 --> 00:20:24,080
make the effort to come and meditate together is very much appreciated, so blame and good

178
00:20:24,080 --> 00:20:28,520
bad, aren't these concepts just an excuse to judge yourself or others, I mean the thing

179
00:20:28,520 --> 00:20:33,760
about karma is it's not that it's a good thing, you know it's not that we we're happy

180
00:20:33,760 --> 00:20:40,400
that it's this way, it's that man that surreal kind of a real bummer that it's this way,

181
00:20:40,400 --> 00:20:51,040
it's a bummer that we have to now deal with the things that we've done in the past, it's

182
00:20:51,040 --> 00:20:55,160
a bummer that we have to feel the effects of karma, it would be great if we didn't

183
00:20:55,160 --> 00:21:02,000
have to, okay, I mean I suppose in terms of the world now it would be horrific because

184
00:21:02,000 --> 00:21:14,200
we kill each other and do whatever we want, but it's not that way, I mean I guess it's

185
00:21:14,200 --> 00:21:22,960
how you look at it, it's actually quite liberating to know that if you get mugged that

186
00:21:22,960 --> 00:21:27,680
it's actually because you mugged people in the past, it's not always, it's hardly ever

187
00:21:27,680 --> 00:21:34,440
quite so simple, it's hardly ever anywhere near as simple as that, but they're often trends

188
00:21:34,440 --> 00:21:39,080
of that sort of wouldn't that be great to know, oh wow, that's what Buddhist do, when

189
00:21:39,080 --> 00:21:43,320
they get mugged they say it's probably my karma I probably mugged that person in the

190
00:21:43,320 --> 00:21:49,440
past life, it's not, I mean that's a gross oversimplification and ignores the fact that

191
00:21:49,440 --> 00:21:56,960
the person, the other person is cultivating new karma, but you know it's quite freeing,

192
00:21:56,960 --> 00:22:01,800
it's again gets back to not worrying so much about your circumstances and getting a sense

193
00:22:01,800 --> 00:22:08,360
that all of that is, or much of that is preordained, much of our circumstances is going

194
00:22:08,360 --> 00:22:12,320
to be beyond our control, things are going to happen that we didn't foresee and couldn't

195
00:22:12,320 --> 00:22:18,240
foresee, a lot of that's just because of how we've situated ourselves, the situation

196
00:22:18,240 --> 00:22:28,520
we've got ourselves in, I think I need Robin back on here to point the questions out

197
00:22:28,520 --> 00:22:34,480
to me, the meditation leads someone to be immune to hostility of society and helps someone

198
00:22:34,480 --> 00:22:38,680
become a better version of themselves, yeah I saw that, but this was a person who hadn't

199
00:22:38,680 --> 00:22:47,320
meditated with us, thanks where did everyone go, so I didn't want to answer because

200
00:22:47,320 --> 00:22:53,120
I figured if they started to meditate they'd get the answer, because I think the answer

201
00:22:53,120 --> 00:23:07,640
is simply yes, where the weekly practice interview happened during June, should the

202
00:23:07,640 --> 00:23:12,680
time's going to be wonky but I think the times are okay for me, I think one of them's

203
00:23:12,680 --> 00:23:21,280
like 530 or something in Sri Lanka, luckily Thailand is 12 hours or 11 hours from me time

204
00:23:21,280 --> 00:23:24,640
wise, but Sri Lanka's a little bit, it's just going to mean they'll have interviews

205
00:23:24,640 --> 00:23:31,440
at 5.30, starting at 5.30 am, this is the evening interview, this will be at 5.30 am,

206
00:23:31,440 --> 00:23:41,040
which is fine, of course, the time's going to be all strange anyway, you know how the brain

207
00:23:41,040 --> 00:23:48,000
is when they fly halfway across the world, it's going to be okay though, wow we've got

208
00:23:48,000 --> 00:23:55,840
a huge list today, most that's 30 of people meditated here, some people more than one

209
00:23:55,840 --> 00:24:16,280
that's all for the questions, I'm going to say good night and wishing you all the best

210
00:24:16,280 --> 00:24:22,040
for those of you who rely on YouTube to get these teachings, well you might have to come

211
00:24:22,040 --> 00:24:29,760
on over to meditation.ceremungal.org, I'm guessing some of it's going to be audio for the

212
00:24:29,760 --> 00:24:36,360
next month, we won't have so much video, I'm not bringing my video camera, a video camera

213
00:24:36,360 --> 00:24:55,800
to tap into Asia, maybe someone else will record some talks I give, otherwise you just have to

214
00:24:55,800 --> 00:25:08,320
tell me how many videos on YouTube already, just go watch some of the old ones, anyway,

215
00:25:08,320 --> 00:25:27,560
take care, upload out a link for the latest updates, please do subscribe, I know

