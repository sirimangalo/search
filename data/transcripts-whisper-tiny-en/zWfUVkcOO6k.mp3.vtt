WEBVTT

00:00.000 --> 00:13.400
Okay, I know everyone, I know the video quality is not going to be very good, but I'm not

00:13.400 --> 00:16.960
really interested in that.

00:16.960 --> 00:36.320
I'm recording this to satisfy a requirement for some study I've been doing.

00:36.320 --> 00:46.880
But I'm doing the study because I think it's worthwhile, and I think it's something that

00:46.880 --> 00:49.840
Buddhist we often neglect.

00:49.840 --> 01:04.600
I think Buddhist monks often neglect this aspect of Buddhism, and that's the Buddhist relationship

01:04.600 --> 01:09.920
with nature.

01:09.920 --> 01:12.960
So this first part, I'm going to break it up in the three parts, I'm going to try and

01:12.960 --> 01:24.040
keep this fairly short, but the Buddhist relationship with nature can I think be broken up

01:24.040 --> 01:32.680
into at least three parts, there's three aspects to it that I've thought of.

01:32.680 --> 01:39.800
And the first one especially relates to climate change.

01:39.800 --> 01:47.040
So you can think of this as my thoughts on climate change.

01:47.040 --> 01:52.840
So I've done videos before, at least one video on nature, Buddhism in nature, in which

01:52.840 --> 02:00.520
I probably said something like nature and Buddhism, yeah, everything's in that nature, right?

02:00.520 --> 02:04.680
We're a part of nature.

02:04.680 --> 02:06.880
And that's true, I think.

02:06.880 --> 02:16.040
I think there's no, it's not hard to understand how even the artificial environments that

02:16.040 --> 02:21.760
we find ourselves in are like this room for example, it's still a part of nature.

02:21.760 --> 02:29.720
In the same way as an antil or a termite mound is a part of nature.

02:29.720 --> 02:39.840
And there's no denying that one useful classification of nature and natural is that which

02:39.840 --> 02:49.200
is separate from humanity, that which is not us, right?

02:49.200 --> 02:56.400
So the things we create are artificial, so I think the useful distinction and the things

02:56.400 --> 03:10.200
which we don't create directly are natural.

03:10.200 --> 03:31.560
And as Buddhists in Buddhism is so deeply concerned with the human, that we do often ignore

03:31.560 --> 03:41.240
or minimalize, minimalize, minimize, trivialize, I think it's the word of nature, trivialize

03:41.240 --> 03:52.040
the role of nature and the importance of nature, the state of nature.

03:52.040 --> 04:01.840
So we say things and I've said things like, the only important quality or the only important

04:01.840 --> 04:21.160
focus for a Buddhist is on human concern, greed, anger, delusion, and I think that's important

04:21.160 --> 04:42.800
because you can't actually save the planet, eventually looking at the big long-term picture.

04:42.800 --> 04:51.120
The planet is doomed anyway, and nature is just a temporary condition that we've

04:51.120 --> 04:52.120
find ourselves in.

04:52.120 --> 04:59.840
We have these surrounding this environment, trees, and so on, they're all temporary anyway.

04:59.840 --> 05:19.240
We're not going to have forests and waterfalls and rain and sun and snow or whatever.

05:19.240 --> 05:24.360
So I think being a hardcore environmentalist, trying to save the planet is certainly not

05:24.360 --> 05:27.560
Buddhist.

05:27.560 --> 05:36.040
But I think what happens as a result is, as I said, we trivialize nature.

05:36.040 --> 05:40.960
And some might argue that that's not a problem for the reason I think that I've stated.

05:40.960 --> 05:47.040
And for a more important reason, and a more very Buddhist reason, I think a defendable

05:47.040 --> 05:57.520
reason, defensible reason, is that, and I firmly believe this, is that if we have no

05:57.520 --> 06:07.160
greed, anger, delusion, if human beings didn't have any of that, then there would be

06:07.160 --> 06:18.120
no problem with the environment.

06:18.120 --> 06:37.480
It's unsustainable.

06:37.480 --> 06:44.160
And so I think that's a ridiculous, we often have to hear that comment, but it's a bit ridiculous

06:44.160 --> 06:50.400
because it's not going to happen, it doesn't happen, you can't imagine such a situation

06:50.400 --> 06:56.200
pulling more and more humans into practice only brings other beings along with them, as

06:56.200 --> 07:02.280
humans become more pure than the animals and the beings surrounding them become pulled

07:02.280 --> 07:09.240
in, become more pure as well, are therefore born as humans, and so you'd be pulling

07:09.240 --> 07:18.120
along basically the whole of set to look, the whole of the universe of beings, which

07:18.120 --> 07:26.200
is really without end, as far as we're told, as far as we've been taught.

07:26.200 --> 07:38.200
But nonetheless, a pure state of mind would a general purity of consciousness on this

07:38.200 --> 07:46.800
planet, would never have got us into this situation, it would not be consuming beyond our

07:46.800 --> 07:51.680
means of production, and by means of production really in terms of the planet.

07:51.680 --> 07:58.440
Our planet can no longer sustain us as humans, and we're relying on it for air and water

07:58.440 --> 08:06.800
and food, and it's not able to sustain us simply because of greed, because of greed, anger

08:06.800 --> 08:12.920
and delusion and anger, it's in terms of us not being able to share with each other.

08:12.920 --> 08:19.920
Delusion is in terms of clinging to things that self-me and mind and issues of nationality

08:19.920 --> 08:27.680
and trade, and everything that backs up our greed, I deserve this, and just the delusion

08:27.680 --> 08:42.960
that doesn't comprehend the idea of sharing, the idea of being content of happiness.

08:42.960 --> 08:49.000
So you could argue that Buddhists should not focus on the environment, they should focus

08:49.000 --> 08:57.800
on themselves and on each other, and on humans, because if humans were better, we would

08:57.800 --> 09:10.280
not be in this mess, but, and I think this is where we sometimes fail.

09:10.280 --> 09:16.600
With our narrow-minded, almost blindered focus on personal development and on helping other

09:16.600 --> 09:35.880
people develop, we can easily become selective in our practice, in our mental development.

09:35.880 --> 09:43.360
With the given that environmental degradation, as it's happening all over the world, it

09:43.360 --> 09:48.760
really is incredible how we're destroying the environment, if you're interested in that

09:48.760 --> 10:01.840
sort of thing, that there's no question that we're failing as human beings in reducing

10:01.840 --> 10:07.760
greed, anger and delusion, okay, that's a given, so the question, or in regards to how

10:07.760 --> 10:20.960
Buddhists are doing. With our focus often on personal development, we miss many of the

10:20.960 --> 10:27.160
greed, anger and delusion-based activities that we also perform.

10:27.160 --> 10:34.080
What I mean to say is that when you get into a life of waste, which many Buddhists

10:34.080 --> 10:44.000
are, and if you go to heavily Buddhist populated areas of the world, like Thailand, Sri Lanka,

10:44.000 --> 10:59.680
Burma, China, there is often a callous indifference to nature by Buddhists and an ignorance,

10:59.680 --> 11:03.880
not just of environmental issues, but an ignorance of how one's own activities are based

11:03.880 --> 11:09.720
on greed, anger and delusion. We take for granted our use of water, our incredible waste

11:09.720 --> 11:18.120
of water. We are not mindful, and we're not mindful in ways that Buddhists in the time

11:18.120 --> 11:28.840
of the Buddha were. We've come so far, then we've become so entrenched in ways that

11:28.840 --> 11:37.600
it seems normal, and that we've completely ignored, often even as Buddhist monks, the

11:37.600 --> 11:43.960
ways of the Buddha and his followers. Like a really classic example that people bring

11:43.960 --> 11:57.840
is when robes were offered to monks in the time of the Buddha, on and once received 500

11:57.840 --> 12:01.400
robes or something. I mean, they used this word 500, so I don't know if that was exactly.

12:01.400 --> 12:06.880
It was a lot of robes. That's not hard to think. It was a king that offered to 500 robes.

12:06.880 --> 12:12.400
He gave to his ministers or someone to go, go and offer these robes to the monk. The

12:12.400 --> 12:16.640
person who was charged with the task, but, oh, well, Anand as a good monk, I'll just

12:16.640 --> 12:24.400
give all 500 to him. When the king was very angry, he heard about this. He was angry, not

12:24.400 --> 12:31.360
with his minister, the person who was charged with the task, but he was angry with Anand

12:31.360 --> 12:35.560
that for accepting the robes. And so he went to Anand and he said, what are you doing

12:35.560 --> 12:43.400
accepting all these robes? How can you be so greedy? Anand and I said, oh, well, I know

12:43.400 --> 12:50.800
lots of monks. Anand was quite popular, and he said, I'll use these robes and give them

12:50.800 --> 12:56.760
to all the monks who need robes. Anand was famous for taking care of other monks and going

12:56.760 --> 13:02.600
around and finding monks who need robes. And the king said, well, but those monks surely

13:02.600 --> 13:07.760
already have robes. What are they going to do with those robes? Anand and I proceeded

13:07.760 --> 13:11.920
to answer the king's questions. He said, with those old robes, well, they'd cut them

13:11.920 --> 13:18.040
up and make smaller, the lower robes, which are smaller, and their older lower robes,

13:18.040 --> 13:24.000
they then cut up and make into bandages, or different kinds of cloth, accessory cloths.

13:24.000 --> 13:31.080
And then their old accessory cloths and bandages, they would pull apart and use for patches.

13:31.080 --> 13:35.880
They would cut apart and use to patch up robes. And their old patches, if they had any,

13:35.880 --> 13:41.320
they would pull apart and make thread out of. And the old thread, they would, that they

13:41.320 --> 13:47.480
had that was being replaced. They would use to make bricks. They would pound it up with clay

13:47.480 --> 13:52.840
and use it to build monasteries. When the king was so impressed, that he gave on under

13:52.840 --> 14:06.160
500 more robes. It's a classic story, I think, of what's this word. The efficiency,

14:06.160 --> 14:13.520
it's not the right word, but how sort of contentment and the use of resources is the

14:13.520 --> 14:26.840
point in Buddhism. In Buddhist monks were very conscientious of wasting water, of cutting

14:26.840 --> 14:34.360
down trees, and that sort of thing. I don't think they were obsessed and environmentalist

14:34.360 --> 14:39.920
in that sense, but they were conscientious and conscious of it. And most importantly, conscious

14:39.920 --> 14:50.720
of the relationship between environmental degradation and our mind. So in modern times,

14:50.720 --> 14:56.640
what we see is all of us, even as Buddhists, are incredibly wasteful. We waste food, we waste

14:56.640 --> 15:06.480
water, we waste electricity, we waste a lot. And so the point of this, this is the first

15:06.480 --> 15:15.360
part. And the first part, as I said, is somewhat, it might seem somewhat banal and almost

15:15.360 --> 15:19.680
unimportant from a Buddhist perspective. It says, I said, Buddhists, we often rush ahead

15:19.680 --> 15:27.920
and say, that's just do the important stuff. I have mental issues that I want to deal with.

15:27.920 --> 15:37.720
But I think it's important, not of ultimate concern, but important for us, as Buddhists,

15:37.720 --> 15:44.400
living our lives, to be conscientious of the environment and conscious that the degradation

15:44.400 --> 15:54.120
that we see is directly related to our greed, anger, and delusion. And so when we say

15:54.120 --> 15:59.360
that the nature is something separate from humans, we have to remember that that's an artificial

15:59.360 --> 16:06.840
separation. And that just because it's not human doesn't mean it's not affected by our actions.

16:06.840 --> 16:13.640
And it doesn't mean that we're not affected by our interactions with it. So recognizing

16:13.640 --> 16:21.960
the greed involved with wasting water and just the deluded nature of how you don't

16:21.960 --> 16:33.720
care about what you're doing. You're not recognizing what you're doing, how you're using

16:33.720 --> 16:39.400
not what is necessary, but what you want when we take long, hot showers and so on. The

16:39.400 --> 16:44.840
environmentalists would say, you're wasting electricity and you're destroying the environment.

16:44.840 --> 16:50.120
But the Buddhists should say, not only that you're doing your greedy about that shower,

16:50.120 --> 16:56.640
you're attached to the hot, pleasant sensation of the shower, but also that that's going

16:56.640 --> 17:03.280
to be reflected in your environment. One of the problems with greed, the problem with greed

17:03.280 --> 17:09.320
and anger and delusion, of course, is they lead to suffering. And one of the direct examples

17:09.320 --> 17:13.280
of this, that I think we have to be clearer. We want to talk about karma and people are

17:13.280 --> 17:18.400
always asking about what is karma, how can you show it? The degradation of the environment

17:18.400 --> 17:25.760
is a clear example of the results of our karma. There's no question about that. So environmentalism

17:25.760 --> 17:31.960
and environmental protectionism and sustainability and so on is a Buddhist issue in the sense

17:31.960 --> 17:39.720
that it is a clear example of the results of an wholesome karma and wholesome activity.

17:39.720 --> 17:44.240
Something for us to keep in mind, we should not ignore and neglect environmental issues

17:44.240 --> 17:51.360
thinking that it's only about the mind. We should put them in the right place in terms

17:51.360 --> 18:06.200
of the results of unwholesomeness. And our living in this time is a sign of our own attachment

18:06.200 --> 18:12.040
to this sort of state and our deserving this sort of state because of our own greed and

18:12.040 --> 18:16.720
delusion. It's something for us to be mindful of and to be conscious of. And conscientious

18:16.720 --> 18:23.200
of our activities. If everyone did this, if everyone was conscientious about environmental

18:23.200 --> 18:30.240
issues and conscientious to the point where we understood that these actions are based

18:30.240 --> 18:35.600
on greed, these actions are based on anger, our relationship with the environment in

18:35.600 --> 18:43.920
terms of wasting is completely Buddhist in the sense of being unwholesome. And if we were

18:43.920 --> 18:48.720
to think in that way and be conscientious, I mean, I think it would expand our practice.

18:48.720 --> 18:53.120
It would help us see aspects of our practice that we're missing. Oh yes, I'm clinging

18:53.120 --> 18:57.040
to this wonderful warm shower, et cetera, et cetera.

18:57.040 --> 19:01.480
Clinging to almond milk apparently is one. We're here at the center, we have almond milk

19:01.480 --> 19:08.320
and I wanted to talk to our people about it. I don't think you have to be obsessed with

19:08.320 --> 19:13.720
learning about these things. But it's an example of how our societies become quite wasteful

19:13.720 --> 19:18.400
because apparently almonds are a terrible thing to be harvesting for their milk. They

19:18.400 --> 19:24.720
use a lot of water. That's the idea. So you might see, well, that's really so far from

19:24.720 --> 19:29.320
and you're thinking, but you never talk about these sorts of things. You never have before

19:29.320 --> 19:38.920
a delved so far from meditation practice. Well, I was driven to do this based on requirements

19:38.920 --> 19:44.600
for some study I'm doing at this university. This is the end. After this, I can go back

19:44.600 --> 19:55.000
to the forest on my own. I don't think I'm going to be met with much contention here.

19:55.000 --> 20:01.280
I think all of you are this sort of sensitive person who respects the environment. There

20:01.280 --> 20:06.080
are many other reasons for respecting it in terms of our ability to have forests for people

20:06.080 --> 20:14.720
like us to go back to and meditate in. The Buddha, even in his time, he complained. He said,

20:14.720 --> 20:20.080
it's hard to find peaceful forests. If you look in India now, a lot of India that

20:20.080 --> 20:26.960
forests, I think, have just been cut down to many people. Not enough consciousness and

20:26.960 --> 20:37.320
conscientiousness in terms of not acting in ways. When you clear a forest, it's generally

20:37.320 --> 20:43.160
a degree. You don't have any need to do it, but if I do it, I can make a lot of money

20:43.160 --> 20:50.880
growing these crops and so on. I can better my position. It's part and parcel of this.

20:50.880 --> 20:54.040
If we were to be more conscientious, we wouldn't have to worry about the environment. This

20:54.040 --> 21:01.120
is true. But we have to look at it as connected. There is a connection between the nature

21:01.120 --> 21:06.560
of the environment and our meditation practice, the state of the environment. To the extent

21:06.560 --> 21:15.160
that we are contributing to that, it does relate to our meditation practice because it's

21:15.160 --> 21:22.360
based on greed and greed. That's the first part. I have two more things to say about

21:22.360 --> 21:26.760
nature that aren't so much related to climate change and so on. They're more Buddhist

21:26.760 --> 21:32.320
and you've probably heard me say things like them before. I gave this talk recently at

21:32.320 --> 21:39.040
the center, in one part. I didn't go in quite so much detail and I gave the talk here.

21:39.040 --> 21:45.200
I'm also trying out this new device. It makes it a lot easier for me to stream so I know

21:45.200 --> 21:53.600
the quality is not very good, but I don't mind. A few minds you can just be mindful of.

21:53.600 --> 22:01.360
And so it also means that I'll probably be able to do more videos. I'm thinking that

22:01.360 --> 22:06.600
I might look through the questions that people have asked and instead of doing Q&A sessions,

22:06.600 --> 22:12.040
I'll just, whenever I have time, I'll do a video on a question. I'll pick up a question

22:12.040 --> 22:17.840
and I'll say, hey, someone asked this question and I'm going to answer it now. One

22:17.840 --> 22:22.520
video on question, which was really a lot of people were commenting that that's what

22:22.520 --> 22:28.320
they prefer. I agree. One video per question is so much better format because then you

22:28.320 --> 22:36.640
can search them. The other thing is I can see, even on this little device, I can see your

22:36.640 --> 22:43.760
comments flashing up on my screen. They're overly my video. This is really a great

22:43.760 --> 22:51.440
way to do this. It's all good. 71 people I can see. There's 71 people watching, which

22:51.440 --> 22:58.760
is, well, it's inspiring. Thank you all for your interest in Buddhism. But this is a call

22:58.760 --> 23:03.880
to everyone, to be more conscientious and to do our part with the environment, not to become

23:03.880 --> 23:09.560
obsessed with it, of course, not to let it distract us from what's really important, but

23:09.560 --> 23:15.360
to recognize the connection and recognize that we do kind of have a duty, even outside

23:15.360 --> 23:20.080
of our practice, to conserve the environment simply because it allows us to continue

23:20.080 --> 23:26.440
practicing. It allows our children and the people who come after us, who are often us,

23:26.440 --> 23:29.920
right? If we're dying or reborn, assuming, well, we've got to come back and deal with

23:29.920 --> 23:39.760
all the crap that we've left. This is the thing that politicians and business people

23:39.760 --> 23:44.200
don't understand. Oh, yes. Maybe a hundred years there'll be problems. That's okay,

23:44.200 --> 23:52.680
I'll be dead. I don't know. You'll be born again. You'll get to inherit your own mess

23:52.680 --> 23:57.920
certainly. We are, what is said, we inherit our karma and this is a big part of it. We'll

23:57.920 --> 24:03.480
inherit the environment as well. Something, you know, that makes it important, not just

24:03.480 --> 24:10.680
because it relates to our practice, but because it allows us to practice. Healthy environment

24:10.680 --> 24:16.400
is in the texts as an important part. You know, they don't spell it out in terms of having

24:16.400 --> 24:21.480
healthy trees and healthy air because it wasn't a problem back there, but having a clean

24:21.480 --> 24:30.480
environment and a suitable environment is important. So that's the relating of Buddhism

24:30.480 --> 24:40.440
to nature. And I'm done. Now I've got to figure out how to stop this. Maybe the X doesn't. Are

24:40.440 --> 25:08.480
you sure you want to stop streaming? Okay. All the best everyone.

