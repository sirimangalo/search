1
00:00:00,000 --> 00:00:29,440
Yes, today we started talking about the 38 blessings which

2
00:00:29,440 --> 00:00:43,120
I learned Buddha taught us being blessings in an ultimate sense, in the highest sense.

3
00:00:43,120 --> 00:00:51,160
Then we continue on with the second verse, which has three more blessings in it, but

4
00:00:51,160 --> 00:01:00,880
it would be the same way that I see cards.

5
00:01:00,880 --> 00:01:18,080
But, you know what the ASO is or the living in a suitable country, a suitable place,

6
00:01:18,080 --> 00:01:19,880
suitable location.

7
00:01:19,880 --> 00:01:38,880
Pupi, takatapunyata, having done good deeds in the past, and atasamapanethi, to set oneself in the right direction.

8
00:01:38,880 --> 00:01:52,880
Pupi, takamangalamutamang, these are blessings in the highest side.

9
00:01:52,880 --> 00:02:03,880
Living in a suitable country, suitable location is a suitable place.

10
00:02:03,880 --> 00:02:14,880
It's another one of those things which is most important in living the spiritual life.

11
00:02:14,880 --> 00:02:27,880
It's most true for people coming from foreign countries who often times

12
00:02:27,880 --> 00:02:36,880
are not able to find the practice of Buddhist teaching in their home country,

13
00:02:36,880 --> 00:02:44,880
and it's necessary for them to fly long, long way.

14
00:02:44,880 --> 00:02:54,880
Travel far, all the way to Thailand or Burma, or the many countries where it is possible to practice.

15
00:02:54,880 --> 00:02:57,880
You have to find a suitable location.

16
00:02:57,880 --> 00:03:05,880
Even though they've come to Thailand, many people who come to Thailand to visit,

17
00:03:05,880 --> 00:03:14,880
many people still never think to come to practice and meditation.

18
00:03:14,880 --> 00:03:26,880
They're not able to find the right place or a suitable place to take up the practice.

19
00:03:26,880 --> 00:03:40,880
A suitable place means it's necessary to have a place where all of one's necessities are met,

20
00:03:40,880 --> 00:03:48,880
whether it's food and shelter,

21
00:03:48,880 --> 00:04:00,880
where the people are friendly and supportive.

22
00:04:00,880 --> 00:04:10,880
And where the teaching is suitable, suitable suited to one's temperament.

23
00:04:10,880 --> 00:04:22,880
Buddhism, there are four kinds of temperament based on, first of all, based on one's inclination.

24
00:04:22,880 --> 00:04:39,880
Some people, some different people have a temperament for the last or for passion,

25
00:04:39,880 --> 00:04:43,880
a person who is passionate.

26
00:04:43,880 --> 00:04:54,880
One group of people is set on views, and so they separate people out into temperament.

27
00:04:54,880 --> 00:04:58,880
These are the temperaments in Vipasana.

28
00:04:58,880 --> 00:05:03,880
In summit and meditation, of course, there are six different kinds of people.

29
00:05:03,880 --> 00:05:09,880
There's the classified within Vipasana meditation, the classified into four.

30
00:05:09,880 --> 00:05:17,880
People who are passionate, people who have wrong view,

31
00:05:17,880 --> 00:05:22,880
and people who have practiced, some people who have practiced Vipasana before.

32
00:05:22,880 --> 00:05:32,880
This means in earlier times or even in past life,

33
00:05:32,880 --> 00:05:40,880
for people who are passionate, it's necessary to teach mindfulness of the body,

34
00:05:40,880 --> 00:05:45,880
mindfulness of the feelings.

35
00:05:45,880 --> 00:05:53,880
If someone has mature wisdom, if one has weak wisdom,

36
00:05:53,880 --> 00:05:55,880
they teach mindfulness of the body.

37
00:05:55,880 --> 00:06:01,880
If one has strong wisdom, teach mindfulness of the feelings.

38
00:06:01,880 --> 00:06:06,880
Same with someone who has practiced some at that tranquility meditation before.

39
00:06:06,880 --> 00:06:14,880
Weak wisdom, teach body, mindfulness of the body, rising, falling.

40
00:06:14,880 --> 00:06:23,880
Strong wisdom, teach feelings, mindfulness of pain, aching.

41
00:06:23,880 --> 00:06:34,880
If someone has a set in wrong view, it's attached to wrong, respective views.

42
00:06:34,880 --> 00:06:37,880
If they have weak wisdom, teach mindfulness of the mind.

43
00:06:37,880 --> 00:06:42,880
If they have strong wisdom, teach mindfulness of the dhamma.

44
00:06:42,880 --> 00:06:49,880
The five hindrinses, the six senses and so on.

45
00:06:49,880 --> 00:06:53,880
Same if someone has practiced Vipasana in the past.

46
00:06:53,880 --> 00:06:57,880
If they have weak wisdom, teach mindfulness of the mind.

47
00:06:57,880 --> 00:07:04,880
If they have strong wisdom, mature wisdom, teach mindfulness of the dhamma.

48
00:07:04,880 --> 00:07:08,880
The dhammas.

49
00:07:08,880 --> 00:07:10,880
This is the suitable teaching.

50
00:07:10,880 --> 00:07:16,880
If they teach Vipasana, it's suitable for one's character type.

51
00:07:16,880 --> 00:07:22,880
Even though we give to meditators who come all form foundations of mindfulness,

52
00:07:22,880 --> 00:07:28,880
in a way this means it's suitable for all people, people of all character types.

53
00:07:28,880 --> 00:07:34,880
But also we have to know the difference between individuals.

54
00:07:34,880 --> 00:07:41,880
Remember where someone is coming from and adjust their practice suitably.

55
00:07:41,880 --> 00:07:50,880
For instance, old people, generally their wisdom is their intelligence, their mind is not.

56
00:07:50,880 --> 00:07:57,880
For some people talking about very old people, who's mind is failing you.

57
00:07:57,880 --> 00:08:02,880
When the mind starts to fail, then you have to give home your mindfulness of the body.

58
00:08:02,880 --> 00:08:10,880
The wisdom, wisdom element is not strong anymore.

59
00:08:10,880 --> 00:08:12,880
This is the suitable teaching.

60
00:08:12,880 --> 00:08:15,880
Also suitable means we have to live in this.

61
00:08:15,880 --> 00:08:18,880
We have to have the suitable insight as well.

62
00:08:18,880 --> 00:08:22,880
Our body has to be taken care of.

63
00:08:22,880 --> 00:08:29,880
And that our mind has to be established in goodness as well.

64
00:08:29,880 --> 00:08:35,880
Our mind cannot be running after sensual pleasures.

65
00:08:35,880 --> 00:09:00,880
We have to stop our mind from being distracted by the pleasures of the sense, being distracted by external phenomena.

66
00:09:00,880 --> 00:09:04,880
We have to keep our mind focused and fixed and so on.

67
00:09:04,880 --> 00:09:08,880
We have to live in a suitable body.

68
00:09:08,880 --> 00:09:13,880
Our body has to be well taken care of.

69
00:09:13,880 --> 00:09:19,880
Free from disease and so on.

70
00:09:19,880 --> 00:09:24,880
This is Patti Rope Patti, so it's a blessing in the highest sense.

71
00:09:24,880 --> 00:09:32,880
When we live in a suitable location externally, only when we live in a suitable location,

72
00:09:32,880 --> 00:09:36,880
when it's possible to practice, we pass in a meditation.

73
00:09:36,880 --> 00:09:42,880
Even sometimes we have a teacher, but we have no place to practice.

74
00:09:42,880 --> 00:09:52,880
We have a place to practice, but no teacher.

75
00:09:52,880 --> 00:09:58,880
We have a place to practice and a teacher, but no place to teach or so on.

76
00:09:58,880 --> 00:10:06,880
The location has to be suitable for the practice, so there's no food or there's no shelter or so on.

77
00:10:06,880 --> 00:10:20,880
We have to find a suitable place to practice meditation.

78
00:10:20,880 --> 00:10:29,880
This is a great blessing and allows us to develop ourselves and to become pure in our mind.

79
00:10:29,880 --> 00:10:38,880
The second blessing in this verse is the fifth blessing of overall.

80
00:10:38,880 --> 00:10:40,880
Who paid to cut upon you at that?

81
00:10:40,880 --> 00:10:45,880
Having done good deeds in the past.

82
00:10:45,880 --> 00:10:52,880
Good deeds are something which are only good in having done in the past.

83
00:10:52,880 --> 00:10:56,880
A good deed when done in the present moment.

84
00:10:56,880 --> 00:11:03,880
It can bring happiness, but really it's the result or the fruit of the deed,

85
00:11:03,880 --> 00:11:09,880
which shows whether it was good or whether it was bad.

86
00:11:09,880 --> 00:11:16,880
In fact, oftentimes people are ordinary people who have in practice,

87
00:11:16,880 --> 00:11:20,880
we pass in a meditation or who haven't developed their mind spiritually.

88
00:11:20,880 --> 00:11:29,880
Well, often these skeptical as to whether doing good deeds really does themselves.

89
00:11:29,880 --> 00:11:34,880
Sometimes we do good deeds again and again, and don't see any real result.

90
00:11:34,880 --> 00:11:44,880
You can see anything good coming back to us.

91
00:11:44,880 --> 00:11:52,880
This is generally because people having done good deeds or doing what they think are good deeds,

92
00:11:52,880 --> 00:11:55,880
and they don't look carefully and watch carefully.

93
00:11:55,880 --> 00:11:59,880
Sometimes the deeds which we are doing are not really good deeds.

94
00:11:59,880 --> 00:12:04,880
Our mind is not intent upon goodness,

95
00:12:04,880 --> 00:12:10,880
and sometimes we don't look carefully to see what is the real result of our deeds.

96
00:12:10,880 --> 00:12:23,880
Don't consider carefully to see that actually we have created the sort of

97
00:12:23,880 --> 00:12:31,880
the karmic, karmic loop of karmic potential in the world,

98
00:12:31,880 --> 00:12:41,880
which has created happiness in our mind and created goodness in the world around us.

99
00:12:41,880 --> 00:12:49,880
We change the world and we can see that the effect will come back to us in the future,

100
00:12:49,880 --> 00:12:53,880
both in this life and most likely in the next life.

101
00:12:53,880 --> 00:12:58,880
When we die our minds will be happy and will be at peace because of the good deeds which we have done.

102
00:12:58,880 --> 00:13:06,880
We will be scared or afraid or angry or upset because we haven't done any good deeds.

103
00:13:06,880 --> 00:13:14,880
Having done good deeds in the past is a blessing because of the effect that it has on the mind.

104
00:13:14,880 --> 00:13:22,880
Having done good deeds in the past has the effect of karmic and purifying the mind.

105
00:13:22,880 --> 00:13:27,880
In fact the word good deeds is a translation of the word bunyat.

106
00:13:27,880 --> 00:13:35,880
Bunyat is a word which means the purify.

107
00:13:35,880 --> 00:13:45,880
It is called bunyat because it purifies.

108
00:13:45,880 --> 00:13:58,880
It cleans the consciousness because it cleans one's being.

109
00:13:58,880 --> 00:14:08,880
The good things which we do is, for instance, generosity, giving support, giving help,

110
00:14:08,880 --> 00:14:18,880
giving items which are used to other people, giving with a pretty heart and soul.

111
00:14:18,880 --> 00:14:24,880
Morality, avoiding doing bad deeds, avoiding killing,

112
00:14:24,880 --> 00:14:29,880
refraining from killing, from stealing, from adultery, from lying,

113
00:14:29,880 --> 00:14:37,880
refraining from drugs and alcohol, or even from meditators

114
00:14:37,880 --> 00:14:44,880
taking on the life of chastity, not eating in the afternoon,

115
00:14:44,880 --> 00:14:49,880
avoiding entertainment and decoration,

116
00:14:49,880 --> 00:14:54,880
avoiding refraining from higher luxurious beds,

117
00:14:54,880 --> 00:15:01,880
and so on, for monks to hunt, for novices, 10 rows, for monks to 127 rows.

118
00:15:01,880 --> 00:15:05,880
If it is all morality it is a kind of bunyat.

119
00:15:05,880 --> 00:15:13,880
It is a kind of way of purifying one's mind, purifying one's self.

120
00:15:13,880 --> 00:15:21,880
When we have done this in the past, our minds will become, we have morality, our minds will be stable.

121
00:15:21,880 --> 00:15:26,880
Even when we give support to other people,

122
00:15:26,880 --> 00:15:30,880
and give support or help, or even teach to other people,

123
00:15:30,880 --> 00:15:35,880
this is a kind of gift in this purifies our mind.

124
00:15:35,880 --> 00:15:40,880
And the third type of bunyat, a good deed that we can do is meditation.

125
00:15:40,880 --> 00:15:46,880
For all of us we can feel already that this is true.

126
00:15:46,880 --> 00:15:53,880
When you practice meditation in the past, it is a blessing in the ultimate sense.

127
00:15:53,880 --> 00:15:58,880
If it is a purifies and cleanses the mind,

128
00:15:58,880 --> 00:16:02,880
then the mind is purified and cleansed already.

129
00:16:02,880 --> 00:16:08,880
When it can live one's life, in peace and happiness and comfort and well-being,

130
00:16:08,880 --> 00:16:14,880
it doesn't have to be bothered by greed or anger or delusion.

131
00:16:14,880 --> 00:16:21,880
When the mind is clear one's mind is pure.

132
00:16:21,880 --> 00:16:24,880
This is why having done these things is a good deed.

133
00:16:24,880 --> 00:16:29,880
Some people, they only look to a short distance.

134
00:16:29,880 --> 00:16:32,880
They practice meditation and they have to suffer,

135
00:16:32,880 --> 00:16:35,880
and they have to fight with all sorts of unpleasant conditions.

136
00:16:35,880 --> 00:16:42,880
And they think that meditation has no purpose.

137
00:16:42,880 --> 00:16:48,880
Really, they don't see that they are actually fighting or wrestling with themselves,

138
00:16:48,880 --> 00:16:53,880
wrestling with all of the bad things which exist inside.

139
00:16:53,880 --> 00:17:00,880
Working hard to be patient and overcome these bad and wholesome states.

140
00:17:00,880 --> 00:17:05,880
When that having done this, poor page or cut that, having done this already

141
00:17:05,880 --> 00:17:09,880
in the future they will, their life will be blessed.

142
00:17:09,880 --> 00:17:15,880
They will be blessed with peace, with happiness, with well-being.

143
00:17:15,880 --> 00:17:19,880
This is the second, fifth, the fifth blessing.

144
00:17:19,880 --> 00:17:26,880
The sixth is atasamapanithi, to set oneself on the right path,

145
00:17:26,880 --> 00:17:31,880
on the right, of course.

146
00:17:31,880 --> 00:17:35,880
And of course the right path of the right course of action,

147
00:17:35,880 --> 00:17:41,880
the right course to take in one's life.

148
00:17:41,880 --> 00:17:53,880
It is the path of the eight-fold noble path.

149
00:17:53,880 --> 00:17:57,880
Again, noble means is something which leads one to be noble,

150
00:17:57,880 --> 00:18:00,880
which one's mind to be free from greed,

151
00:18:00,880 --> 00:18:04,880
free from anger, free from delusion, free from jealousy and stinginess

152
00:18:04,880 --> 00:18:08,880
and conceit and wrong view and so on.

153
00:18:08,880 --> 00:18:11,880
Free from all the evil bad things in the world.

154
00:18:11,880 --> 00:18:22,880
It is the path which leads one to be one's mind to be pure.

155
00:18:22,880 --> 00:18:27,880
It is a path which leads one's mind to be pure and it has eight parts.

156
00:18:27,880 --> 00:18:36,880
Eight parts but in brief the eight-fold noble path can be summarized up in three parts.

157
00:18:36,880 --> 00:18:40,880
The first part, morality.

158
00:18:40,880 --> 00:18:48,880
Morality is right action, right speech, right speech, right action, right livelihood.

159
00:18:48,880 --> 00:19:03,880
There is morality, concentration, concentration is right effort, right mindfulness, right concentration.

160
00:19:03,880 --> 00:19:09,880
And the third one, wisdom, wisdom is right view and right thought.

161
00:19:09,880 --> 00:19:13,880
To the eight parts of the evil noble path,

162
00:19:13,880 --> 00:19:18,880
in brief to make it easy to understand it is set in three parts.

163
00:19:18,880 --> 00:19:24,880
Morality keeping the body and the speech, pure.

164
00:19:24,880 --> 00:19:31,880
And keeping the mind from running off into defilements,

165
00:19:31,880 --> 00:19:37,880
guarding the mind from falling into evil.

166
00:19:37,880 --> 00:19:44,880
Starting the mind keeping it with the body, keeping it with the feeling.

167
00:19:44,880 --> 00:19:49,880
Remind the abdominals.

168
00:19:49,880 --> 00:19:54,880
And morale and concentration, keeping the mind fixed on the object.

169
00:19:54,880 --> 00:19:58,880
Once the mind is not running here, not running there.

170
00:19:58,880 --> 00:20:03,880
The ones body and one's speech are calm or quiet or tranquil.

171
00:20:03,880 --> 00:20:08,880
One can develop great states of concentration.

172
00:20:08,880 --> 00:20:11,880
The ones mind becomes fixed, becomes focused.

173
00:20:11,880 --> 00:20:15,880
You can set on a single object.

174
00:20:15,880 --> 00:20:17,880
We acknowledge rising.

175
00:20:17,880 --> 00:20:18,880
It's fixed on the rising.

176
00:20:18,880 --> 00:20:19,880
We acknowledge falling.

177
00:20:19,880 --> 00:20:21,880
It's fixed on the power.

178
00:20:21,880 --> 00:20:24,880
This is called Luckin Upanishan.

179
00:20:24,880 --> 00:20:27,880
Luckin Upanishan.

180
00:20:27,880 --> 00:20:33,880
It's meditation on the three characteristics.

181
00:20:33,880 --> 00:20:41,880
Meditation on the impermanence meditation on suffering meditation.

182
00:20:41,880 --> 00:20:45,880
Jana, it's Jana based on impermanence based on suffering.

183
00:20:45,880 --> 00:20:47,880
Based on non-sap.

184
00:20:47,880 --> 00:20:50,880
I mean to see the truth about reality.

185
00:20:50,880 --> 00:20:55,880
It's a kind of Jana concentration.

186
00:20:55,880 --> 00:21:02,880
We acknowledge rising and falling.

187
00:21:02,880 --> 00:21:07,880
The wisdom which arises to see impermanence suffering.

188
00:21:07,880 --> 00:21:12,880
This is the wisdom part.

189
00:21:12,880 --> 00:21:17,880
It leads us to see also the form of truth.

190
00:21:17,880 --> 00:21:21,880
When we see impermanence suffering in non-sap,

191
00:21:21,880 --> 00:21:26,880
we come to let go of the cause of suffering.

192
00:21:26,880 --> 00:21:32,880
It's become to abandon craving for the body craving for the mind,

193
00:21:32,880 --> 00:21:36,880
craving for the whole world around us.

194
00:21:36,880 --> 00:21:42,880
The mind becomes set and established.

195
00:21:42,880 --> 00:21:51,880
It's fixed like a stone, like a rock.

196
00:21:51,880 --> 00:21:59,880
It's solid like a rock and moved by the winds of change and moved by the ways of the world.

197
00:21:59,880 --> 00:22:04,880
The mind becomes fixed and firm.

198
00:22:04,880 --> 00:22:11,880
Not running here and there after all of the objects of the sense.

199
00:22:11,880 --> 00:22:14,880
The mind sees the truth of suffering.

200
00:22:14,880 --> 00:22:17,880
It's the cause of suffering.

201
00:22:17,880 --> 00:22:24,880
By abandoning the cause, one follows the right path.

202
00:22:24,880 --> 00:22:28,880
By abandoning craving, this is following the right path.

203
00:22:28,880 --> 00:22:38,880
The mind is moral, has morality, concentration, and wisdom.

204
00:22:38,880 --> 00:22:43,880
And when reaches the goal of life which is the cessation of suffering.

205
00:22:43,880 --> 00:22:47,880
The end of suffering, no more suffering.

206
00:22:47,880 --> 00:22:50,880
No more sorrow, lamentation, and despair.

207
00:22:50,880 --> 00:22:58,880
No more bodily suffering, no more mental suffering.

208
00:22:58,880 --> 00:23:01,880
No more not getting what we want.

209
00:23:01,880 --> 00:23:03,880
No more getting what we don't want.

210
00:23:03,880 --> 00:23:08,880
No more having our wishes.

211
00:23:08,880 --> 00:23:11,880
Unfulfilled.

212
00:23:11,880 --> 00:23:19,880
Unfulfilled.

213
00:23:19,880 --> 00:23:21,880
Breathe no more suffering.

214
00:23:21,880 --> 00:23:26,880
No more body suffering, no more mind suffering.

215
00:23:26,880 --> 00:23:32,880
The end peace and happiness at all times.

216
00:23:32,880 --> 00:23:36,880
These three are three more blessings.

217
00:23:36,880 --> 00:23:41,880
Among the 38 blessings that the Lord would attack.

218
00:23:41,880 --> 00:23:45,880
You can see that these ones have a lot to do with us in the meditation.

219
00:23:45,880 --> 00:23:48,880
We practice meditation.

220
00:23:48,880 --> 00:23:52,880
Important to live in the right place.

221
00:23:52,880 --> 00:23:57,880
We practice meditation, our minds become pure.

222
00:23:57,880 --> 00:24:01,880
We follow the right path.

223
00:24:01,880 --> 00:24:06,880
The pinnacle of the Lord Buddhist teaching is we pass in a meditation.

224
00:24:06,880 --> 00:24:09,880
It's the highest.

225
00:24:09,880 --> 00:24:17,880
We pass in that to see clearly this with the Lord Buddha taught that we should not be confused or

226
00:24:17,880 --> 00:24:25,880
misunderstanding about the things around us or the things inside of ourselves.

227
00:24:25,880 --> 00:24:32,880
So here's all the more reason that we continue to practice.

228
00:24:32,880 --> 00:24:41,880
We pass in the meditation so that we come to gain the highest blessings in the Lord.

229
00:24:41,880 --> 00:25:04,880
The highest blessings which exist in an ultimate sin.

