1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Damukanda.

2
00:00:05,000 --> 00:00:12,000
Today we continue on with verse 128, which reads as follows.

3
00:00:12,000 --> 00:00:41,000
This is a very similar to our last verse, not in the middle.

4
00:00:41,000 --> 00:00:59,000
Not in the middle of the sky, not in the middle of the end of the sky, right?

5
00:00:59,000 --> 00:01:09,000
Not in the middle of the ocean, not in a cave in the mountain.

6
00:01:09,000 --> 00:01:18,000
If one plunges into the cave in the mountain, one cannot find a place on earth.

7
00:01:18,000 --> 00:01:47,000
Another very well-known story, part of the Buddha's life.

8
00:01:47,000 --> 00:01:54,000
And it'd be nice if there were more stories about the Buddha's life.

9
00:01:54,000 --> 00:01:57,000
I've talked about this several times.

10
00:01:57,000 --> 00:02:00,000
Well, that's basically what we're doing here.

11
00:02:00,000 --> 00:02:06,000
We're telling the story of the Buddha's life piece by piece, but I think part of why several

12
00:02:06,000 --> 00:02:13,000
many people are fascinated by these stories is because they're part of the Buddha story that people have never heard.

13
00:02:13,000 --> 00:02:19,000
And that's because the most famous Buddha stories are not really about the Buddha at all.

14
00:02:19,000 --> 00:02:21,000
They're about the Bodhisattva.

15
00:02:21,000 --> 00:02:24,000
They're about the quest to become a Buddha.

16
00:02:24,000 --> 00:02:33,000
But the 45 years that the Buddha actually taught is neglected.

17
00:02:33,000 --> 00:02:40,000
And it's partly because of the types of Buddhism that have spread and become popular.

18
00:02:40,000 --> 00:02:46,000
But it's a shame because he did spend 45 years teaching.

19
00:02:46,000 --> 00:02:53,000
And if the stories are any indication, lots of exciting things happened.

20
00:02:53,000 --> 00:03:03,000
One of the exciting things that happened was his son, if you remember from the Bodhisattva story that we all hear about,

21
00:03:03,000 --> 00:03:10,000
the Buddha left behind some version, say, in the middle of the night.

22
00:03:10,000 --> 00:03:25,000
And his wife, who he left behind as a prince, eventually became monks themselves.

23
00:03:25,000 --> 00:03:35,000
So Rahul became a novice monk, and his order became a bikini.

24
00:03:35,000 --> 00:03:42,000
And lots of the saki and the relatives of the Buddha also became monks.

25
00:03:42,000 --> 00:03:52,000
There was Anurudhan and Upali and Devodatta.

26
00:03:52,000 --> 00:03:55,000
And many, many more.

27
00:03:55,000 --> 00:04:01,000
And they were criticized for the many people were saying,

28
00:04:01,000 --> 00:04:03,000
well, this monk is like the Pine Biper.

29
00:04:03,000 --> 00:04:07,000
He's leading all of our sons away.

30
00:04:07,000 --> 00:04:10,000
He's like a cult leader.

31
00:04:10,000 --> 00:04:15,000
Of course, anyone would start to get any community would start to get concerned.

32
00:04:15,000 --> 00:04:19,000
If a religious leader started to take away their young men,

33
00:04:19,000 --> 00:04:24,000
and for the beginning it was all young men,

34
00:04:24,000 --> 00:04:33,000
and just threatening their society.

35
00:04:33,000 --> 00:04:36,000
But the monk came to the Buddha and asked him,

36
00:04:36,000 --> 00:04:43,000
and the Buddha said, just tell them that they go forth according to the Dhamma.

37
00:04:43,000 --> 00:04:46,000
Dhamma was an important word for people.

38
00:04:46,000 --> 00:04:50,000
They wanted to follow the Dhamma, so it was quite clever actually.

39
00:04:50,000 --> 00:04:56,000
And when they told people this, these monks go forth according to the Dhamma,

40
00:04:56,000 --> 00:05:03,000
then people thought that people understood that actually this is a challenge really.

41
00:05:03,000 --> 00:05:06,000
And they had to then point out a flaw in the Buddha's teaching,

42
00:05:06,000 --> 00:05:09,000
which of course they couldn't easily find.

43
00:05:09,000 --> 00:05:11,000
Anyway, that's not the story today.

44
00:05:11,000 --> 00:05:14,000
Today's story, but the background is there,

45
00:05:14,000 --> 00:05:18,000
that Yesodra's father, Supa Buddha.

46
00:05:18,000 --> 00:05:23,000
That's where the story is about.

47
00:05:23,000 --> 00:05:30,000
He was less than thrilled that his daughter and his grandson

48
00:05:30,000 --> 00:05:34,000
renounced the world, and he was very angry.

49
00:05:34,000 --> 00:05:41,000
He was quite awful about it.

50
00:05:41,000 --> 00:05:50,000
And so when the Buddha was visiting his family,

51
00:05:50,000 --> 00:05:52,000
when he tried to go for arms with the monks,

52
00:05:52,000 --> 00:05:57,000
Supa Buddha set up a seat in the middle of the road,

53
00:05:57,000 --> 00:06:01,000
in the middle of the street between houses,

54
00:06:01,000 --> 00:06:03,000
and sort of drinking alcohol.

55
00:06:03,000 --> 00:06:05,000
And I guess with his friends,

56
00:06:05,000 --> 00:06:07,000
you'll just set up a sort of a party.

57
00:06:07,000 --> 00:06:10,000
And when the monks came, you refused to move,

58
00:06:10,000 --> 00:06:12,000
standing right in the middle of the road,

59
00:06:12,000 --> 00:06:14,000
and they couldn't go for arms.

60
00:06:14,000 --> 00:06:16,000
Somehow he was gutted their way.

61
00:06:23,000 --> 00:06:28,000
And they said to him, hey, the Buddha has come.

62
00:06:28,000 --> 00:06:29,000
What's going on?

63
00:06:29,000 --> 00:06:31,000
And he says, tell him to go this way.

64
00:06:31,000 --> 00:06:32,000
I'm older than him.

65
00:06:32,000 --> 00:06:35,000
I'm not going to make way for him.

66
00:06:35,000 --> 00:06:36,000
So he wouldn't get out of his way.

67
00:06:36,000 --> 00:06:38,000
And the Buddha just turned around and walked away.

68
00:06:38,000 --> 00:06:42,000
Made no complaint.

69
00:06:42,000 --> 00:06:43,000
This went back to the monastery,

70
00:06:43,000 --> 00:06:48,000
or went another way.

71
00:06:50,000 --> 00:06:55,000
And Supa Buddha sent someone out one of his spies

72
00:06:55,000 --> 00:06:56,000
to go and see.

73
00:06:56,000 --> 00:06:58,000
He said, I'll go and see what the Buddha says.

74
00:06:58,000 --> 00:07:00,000
We're going to go, because he's got more in store.

75
00:07:00,000 --> 00:07:03,000
This wasn't all he had planned.

76
00:07:03,000 --> 00:07:06,000
He's got more plans than this.

77
00:07:06,000 --> 00:07:10,000
So he says, go and spy on the Buddha.

78
00:07:10,000 --> 00:07:12,000
See what he says about this.

79
00:07:12,000 --> 00:07:14,000
Because we're going to use it against him.

80
00:07:14,000 --> 00:07:16,000
It's the idea.

81
00:07:16,000 --> 00:07:20,000
And when the Buddha was on his way back, he smiled.

82
00:07:20,000 --> 00:07:22,000
And there's several stories where the Buddha smiled.

83
00:07:22,000 --> 00:07:25,000
And the Buddha smiling is not like an ordinary person smiling.

84
00:07:25,000 --> 00:07:27,000
It means something.

85
00:07:27,000 --> 00:07:30,000
And so Ananda noticed that he was smiling

86
00:07:30,000 --> 00:07:32,000
and said, Reverend,

87
00:07:32,000 --> 00:07:36,000
why are you smiling?

88
00:07:36,000 --> 00:07:38,000
It's kind of perverse.

89
00:07:38,000 --> 00:07:42,000
I want to say that smiling of a Buddha is not

90
00:07:42,000 --> 00:07:45,000
the smile of an ordinary being.

91
00:07:45,000 --> 00:07:48,000
They smile at extraordinary things

92
00:07:48,000 --> 00:07:54,000
when something is extreme.

93
00:07:54,000 --> 00:07:58,000
Because it's out of the ordinary,

94
00:07:58,000 --> 00:08:01,000
often in a bad way.

95
00:08:01,000 --> 00:08:08,000
And he says, Ananda, the super Buddha,

96
00:08:08,000 --> 00:08:10,000
he's committed a grievous sin.

97
00:08:10,000 --> 00:08:12,000
He's done something very, very terrible,

98
00:08:12,000 --> 00:08:14,000
very bad karma for him.

99
00:08:14,000 --> 00:08:17,000
To block a Buddha, blocking those people

100
00:08:17,000 --> 00:08:20,000
who wanted to give arms to the Buddha

101
00:08:20,000 --> 00:08:24,000
is a fairly special being.

102
00:08:24,000 --> 00:08:27,000
And he made a prediction.

103
00:08:27,000 --> 00:08:29,000
And I'm going to gloss over some of this

104
00:08:29,000 --> 00:08:31,000
because a lot of these stories, I believe,

105
00:08:31,000 --> 00:08:33,000
are exaggerated.

106
00:08:33,000 --> 00:08:36,000
That might put me in bad graces with some people.

107
00:08:36,000 --> 00:08:40,000
But I'm going to, as I've done before,

108
00:08:40,000 --> 00:08:45,000
I'm going to exaggerate some of the aspects

109
00:08:45,000 --> 00:08:47,000
that they talk about them.

110
00:08:47,000 --> 00:08:50,000
But basically, he says he's going to die

111
00:08:50,000 --> 00:08:53,000
and go to hell in seven days.

112
00:08:53,000 --> 00:08:55,000
But he makes prediction.

113
00:08:55,000 --> 00:08:57,000
He says he's going to fall down the stairs

114
00:08:57,000 --> 00:09:00,000
in his palace.

115
00:09:00,000 --> 00:09:05,000
And he's going to die at the bottom of the steps

116
00:09:05,000 --> 00:09:08,000
of his seven story palace.

117
00:09:12,000 --> 00:09:15,000
In seven days.

118
00:09:15,000 --> 00:09:18,000
Within seven days or something like that.

119
00:09:22,000 --> 00:09:24,000
And so, despite here, this then goes back

120
00:09:24,000 --> 00:09:28,000
to Super Buddha and tells him in Super Buddha.

121
00:09:28,000 --> 00:09:34,000
He says, well, Buddha's never,

122
00:09:34,000 --> 00:09:38,000
what Buddha's going to happen is going to happen.

123
00:09:38,000 --> 00:09:43,000
But he didn't say he's going to die in seven days.

124
00:09:43,000 --> 00:09:46,000
He said, at that bottom of the stairs,

125
00:09:46,000 --> 00:09:47,000
he's going to die.

126
00:09:47,000 --> 00:09:49,000
So if I don't go to the bottom of the stairs,

127
00:09:49,000 --> 00:09:52,000
if I don't go down those stairs,

128
00:09:52,000 --> 00:09:55,000
I won't happen.

129
00:09:55,000 --> 00:10:00,000
And he said, I'm going to do what I'm going to do.

130
00:10:00,000 --> 00:10:02,000
It's going to make it impossible for me

131
00:10:02,000 --> 00:10:04,000
to go down those stairs.

132
00:10:04,000 --> 00:10:10,000
And I'm going to show that the Buddha is a liar.

133
00:10:10,000 --> 00:10:16,000
And so he orders his men to remove the stairs.

134
00:10:16,000 --> 00:10:19,000
I guess he had another set of stairs or something.

135
00:10:19,000 --> 00:10:22,000
He moved these main stairs.

136
00:10:22,000 --> 00:10:26,000
Seven floors of them, apparently.

137
00:10:26,000 --> 00:10:30,000
And bar up all the doors.

138
00:10:30,000 --> 00:10:35,000
And put guards at each of the levels

139
00:10:35,000 --> 00:10:38,000
to stop him from going through the doors

140
00:10:38,000 --> 00:10:43,000
where the stairs used to be once they were removed.

141
00:10:43,000 --> 00:10:45,000
Making it impossible.

142
00:10:45,000 --> 00:10:47,000
And he lives up on the top level.

143
00:10:47,000 --> 00:10:51,000
And he makes sure that he doesn't go down these stairs.

144
00:10:51,000 --> 00:10:55,000
He tells us guards, if ever I'm even thinking about going

145
00:10:55,000 --> 00:11:00,000
through those doors,

146
00:11:00,000 --> 00:11:02,000
he stopped me.

147
00:11:02,000 --> 00:11:08,000
He tells the guards to grab him and stop him.

148
00:11:08,000 --> 00:11:14,000
And the Buddha hears about this.

149
00:11:14,000 --> 00:11:17,000
And he says, this is the quote.

150
00:11:17,000 --> 00:11:24,000
He says, let him not be content with ascending

151
00:11:24,000 --> 00:11:26,000
to the topmost floor of his palace.

152
00:11:26,000 --> 00:11:30,000
Let him soar aloft and sit in the air.

153
00:11:30,000 --> 00:11:34,000
So even if he doesn't stay at the top

154
00:11:34,000 --> 00:11:38,000
and never come down, let him fly through the air,

155
00:11:38,000 --> 00:11:40,000
let him put to see in a boat,

156
00:11:40,000 --> 00:11:43,000
or let him enter the bowels of a mountain.

157
00:11:43,000 --> 00:11:46,000
There's no equivocation in the words of the Buddha.

158
00:11:46,000 --> 00:11:47,000
He will enter.

159
00:11:47,000 --> 00:11:49,000
He will die.

160
00:11:49,000 --> 00:11:51,000
He will enter into the earth.

161
00:11:51,000 --> 00:11:55,000
He will pass away right where I said he would.

162
00:11:55,000 --> 00:11:58,000
I mean, the actual story is that the earth opened up

163
00:11:58,000 --> 00:12:00,000
and swallowed him and he was born

164
00:12:00,000 --> 00:12:02,000
in how that happened to five different people,

165
00:12:02,000 --> 00:12:04,000
I think, in the Buddhist time.

166
00:12:04,000 --> 00:12:06,000
The Buddha was one of them.

167
00:12:06,000 --> 00:12:08,000
Not convinced that it actually happens,

168
00:12:08,000 --> 00:12:10,000
but that's what the story says.

169
00:12:10,000 --> 00:12:11,000
It may have happened.

170
00:12:11,000 --> 00:12:17,000
It's just kind of incredibly hard to believe.

171
00:12:17,000 --> 00:12:21,000
And then he tells this verse.

172
00:12:21,000 --> 00:12:25,000
And the verse is telling something different,

173
00:12:25,000 --> 00:12:30,000
and saying that you can't avoid death.

174
00:12:30,000 --> 00:12:35,000
But it fits in because it's not only that you can't avoid death,

175
00:12:35,000 --> 00:12:39,000
it's that you can't avoid in general the consequences of your deeds.

176
00:12:39,000 --> 00:12:42,000
I mean, stopping a spiritual teacher like the Buddha,

177
00:12:42,000 --> 00:12:46,000
someone's so pure and so good and so wise.

178
00:12:46,000 --> 00:12:49,000
Getting in their way, I mean, if you read the text,

179
00:12:49,000 --> 00:12:51,000
it's a bad thing.

180
00:12:51,000 --> 00:12:58,000
But emotionally or intuitively,

181
00:12:58,000 --> 00:13:01,000
you can understand how it would be a bad thing to do.

182
00:13:01,000 --> 00:13:03,000
Pretty hard karma, bad karma.

183
00:13:03,000 --> 00:13:06,000
I mean, karma in Buddhism is like that.

184
00:13:06,000 --> 00:13:10,000
If you hurt an evil person, it's not as bad.

185
00:13:10,000 --> 00:13:12,000
As if you hurt a pure person.

186
00:13:12,000 --> 00:13:15,000
Pure is hurting someone who is pure as far worse.

187
00:13:15,000 --> 00:13:17,000
Hirting someone who doesn't deserve it.

188
00:13:17,000 --> 00:13:21,000
But moreover, hurting someone who is pure who is good,

189
00:13:21,000 --> 00:13:25,000
who is spiritually enlightened.

190
00:13:25,000 --> 00:13:29,000
The Buddha is just the pinnacle of that.

191
00:13:29,000 --> 00:13:31,000
So that's it for the verse,

192
00:13:31,000 --> 00:13:34,000
but then it tells the story kind of as an afterthought.

193
00:13:34,000 --> 00:13:36,000
It just stops.

194
00:13:36,000 --> 00:13:38,000
It just tells the story and doesn't explain it at all.

195
00:13:38,000 --> 00:13:43,000
It says, in the middle of the night,

196
00:13:43,000 --> 00:13:50,000
his horse, one of his expensive horses broke loose

197
00:13:50,000 --> 00:13:55,000
and was running around in the bottom floor of the palace.

198
00:13:55,000 --> 00:14:00,000
I guess that was an open area where the stables were or whatever.

199
00:14:00,000 --> 00:14:06,000
So he heard this ruckus and he asked them

200
00:14:06,000 --> 00:14:08,000
what's going on and they said,

201
00:14:08,000 --> 00:14:10,000
oh, you're your horse.

202
00:14:10,000 --> 00:14:12,000
So he wanted to catch him.

203
00:14:12,000 --> 00:14:19,000
He stood up and started towards the stairs.

204
00:14:19,000 --> 00:14:21,000
I guess he didn't have another set of stairs.

205
00:14:21,000 --> 00:14:24,000
The idea was he was just going to not come down for seven days

206
00:14:24,000 --> 00:14:28,000
and at the end of seven days he would open the door.

207
00:14:28,000 --> 00:14:32,000
But he forgot, of course, and he got to the door.

208
00:14:32,000 --> 00:14:34,000
But here's where the funny thing goes.

209
00:14:34,000 --> 00:14:38,000
I mean because he was supposed to have these guards watching.

210
00:14:38,000 --> 00:14:42,000
But when he gets to the doors,

211
00:14:42,000 --> 00:14:45,000
I mean it says things like the stairs appear

212
00:14:45,000 --> 00:14:47,000
and the doors open of their own accord.

213
00:14:47,000 --> 00:14:49,000
I'm not convinced that that happens,

214
00:14:49,000 --> 00:14:51,000
but maybe it does.

215
00:14:51,000 --> 00:14:53,000
I think something you can argue might happen

216
00:14:53,000 --> 00:14:55,000
is the angels get involved

217
00:14:55,000 --> 00:14:59,000
and they push open the doors.

218
00:14:59,000 --> 00:15:03,000
But the kind of thing that would happen,

219
00:15:03,000 --> 00:15:05,000
not saying that that's what happened

220
00:15:05,000 --> 00:15:07,000
and that it's what happened in this case,

221
00:15:07,000 --> 00:15:08,000
but the kind of thing that would happen

222
00:15:08,000 --> 00:15:10,000
from being such an evil, evil person

223
00:15:10,000 --> 00:15:14,000
is that maybe the guards didn't listen to them.

224
00:15:14,000 --> 00:15:17,000
Maybe they didn't remove the staircase,

225
00:15:17,000 --> 00:15:21,000
especially if they were albuddhist or keen about the Buddha

226
00:15:21,000 --> 00:15:26,000
so they wouldn't want to get involved with this.

227
00:15:26,000 --> 00:15:29,000
And it says that the guards actually,

228
00:15:29,000 --> 00:15:31,000
the story says that the guards actually threw him down the stairs,

229
00:15:31,000 --> 00:15:34,000
which gives me that idea that the guards were probably

230
00:15:34,000 --> 00:15:37,000
pretty upset at Super Buddha at this point.

231
00:15:37,000 --> 00:15:40,000
So they actually, the doors open,

232
00:15:40,000 --> 00:15:42,000
the guards threw him down the first,

233
00:15:42,000 --> 00:15:44,000
the seventh floor, they threw him down the sixth floor

234
00:15:44,000 --> 00:15:46,000
and he tumbled down the stairs.

235
00:15:46,000 --> 00:15:49,000
The sixth floor threw him down to the fifth floor

236
00:15:49,000 --> 00:15:51,000
and they actually threw him down the stairs,

237
00:15:51,000 --> 00:15:53,000
seven flights of stairs,

238
00:15:53,000 --> 00:15:56,000
at which point he was swallowed by the earth

239
00:15:56,000 --> 00:15:59,000
or he died at the bottom of the stairs,

240
00:15:59,000 --> 00:16:01,000
right where the Buddha had said.

241
00:16:01,000 --> 00:16:04,000
And he descended there in and was reborn

242
00:16:04,000 --> 00:16:08,000
in a VC health and that's how the story ends.

243
00:16:08,000 --> 00:16:10,000
Just like that.

244
00:16:10,000 --> 00:16:16,000
So it's one of those sort of fantastic stories.

245
00:16:16,000 --> 00:16:20,000
It doesn't say too much about the verse.

246
00:16:20,000 --> 00:16:23,000
Except to say that there's this idea

247
00:16:23,000 --> 00:16:25,000
that you can't avoid karma.

248
00:16:25,000 --> 00:16:26,000
The only way to,

249
00:16:26,000 --> 00:16:32,000
there are ways you can mitigate

250
00:16:32,000 --> 00:16:36,000
and like if you have negative karma,

251
00:16:36,000 --> 00:16:41,000
positive karma will cancel it out sometimes.

252
00:16:41,000 --> 00:16:43,000
But it does just that.

253
00:16:43,000 --> 00:16:46,000
And it's the power of the good karma that cancels it out.

254
00:16:46,000 --> 00:16:50,000
You can't escape the karma entirely.

255
00:16:50,000 --> 00:16:54,000
You have to do something to mitigate it.

256
00:16:54,000 --> 00:16:58,000
Or to pass away into enlightenment first.

257
00:16:58,000 --> 00:17:02,000
There's this idea of when you become enlightened

258
00:17:02,000 --> 00:17:04,000
and you aren't reborn.

259
00:17:04,000 --> 00:17:07,000
All the future results of your deeds

260
00:17:07,000 --> 00:17:09,000
don't have time to come to fruition.

261
00:17:09,000 --> 00:17:11,000
But as long as you're in some sorrow

262
00:17:11,000 --> 00:17:13,000
there's going to be room.

263
00:17:13,000 --> 00:17:14,000
Everything has an effect.

264
00:17:14,000 --> 00:17:15,000
That's the point.

265
00:17:15,000 --> 00:17:17,000
I mean, it's like physics.

266
00:17:17,000 --> 00:17:19,000
Everything has its power.

267
00:17:19,000 --> 00:17:21,000
And the power doesn't just go away

268
00:17:21,000 --> 00:17:23,000
and get forgotten about it.

269
00:17:23,000 --> 00:17:25,000
It doesn't just disappear.

270
00:17:25,000 --> 00:17:30,000
It has an effect.

271
00:17:30,000 --> 00:17:31,000
And that's a part of,

272
00:17:31,000 --> 00:17:33,000
it's a way of looking at this verse.

273
00:17:33,000 --> 00:17:36,000
I mean, the most obvious way to look at the verse

274
00:17:36,000 --> 00:17:37,000
is the idea of death.

275
00:17:37,000 --> 00:17:39,000
How death comes to us all.

276
00:17:39,000 --> 00:17:42,000
And therefore, we shouldn't be negligent.

277
00:17:42,000 --> 00:17:44,000
We shouldn't waste our lives.

278
00:17:44,000 --> 00:17:49,000
We should work to do what we can while we're still alive.

279
00:17:49,000 --> 00:17:51,000
Because we don't know when death is coming.

280
00:17:51,000 --> 00:17:53,000
We don't know where we don't know how.

281
00:17:53,000 --> 00:17:57,000
We don't know where we're going when we pass away.

282
00:17:57,000 --> 00:18:00,000
But in the context of the story

283
00:18:00,000 --> 00:18:02,000
there's another interesting point here.

284
00:18:02,000 --> 00:18:10,000
That the reason we die

285
00:18:10,000 --> 00:18:13,000
and the reason death is inevitable

286
00:18:13,000 --> 00:18:16,000
is because of the karma of being reborn.

287
00:18:16,000 --> 00:18:20,000
The karma of clinging

288
00:18:20,000 --> 00:18:22,000
at the moment of death

289
00:18:22,000 --> 00:18:26,000
and therefore creating a new body.

290
00:18:26,000 --> 00:18:29,000
That means we have to die.

291
00:18:29,000 --> 00:18:32,000
I mean, it's an example of karma

292
00:18:32,000 --> 00:18:36,000
of the results of karma that it's inevitable.

293
00:18:36,000 --> 00:18:38,000
That it's easy to see

294
00:18:38,000 --> 00:18:40,000
inevitable results of karma.

295
00:18:40,000 --> 00:18:43,000
But karma in general is like that.

296
00:18:43,000 --> 00:18:46,000
So he's getting thrown down the stairs.

297
00:18:46,000 --> 00:18:48,000
What is inevitable?

298
00:18:48,000 --> 00:18:50,000
So we talk in interesting,

299
00:18:50,000 --> 00:18:52,000
an interesting question

300
00:18:52,000 --> 00:18:58,000
that is whether our lives are deterministic.

301
00:18:58,000 --> 00:19:01,000
And I've talked about this before

302
00:19:01,000 --> 00:19:04,000
that I don't think determinism is the right way to look at it

303
00:19:04,000 --> 00:19:06,000
because it requires a framework,

304
00:19:06,000 --> 00:19:10,000
a universe, a four-dimensional space-time universe.

305
00:19:10,000 --> 00:19:13,000
That's all really up in the mind.

306
00:19:13,000 --> 00:19:16,000
And if you look at the universe

307
00:19:16,000 --> 00:19:18,000
in terms of the present moment

308
00:19:18,000 --> 00:19:21,000
and determinism doesn't really,

309
00:19:21,000 --> 00:19:25,000
it's saying too much.

310
00:19:25,000 --> 00:19:27,000
It's hard to get your mind around it,

311
00:19:27,000 --> 00:19:31,000
but that's because we focus on the idea of a four-dimensional reality

312
00:19:31,000 --> 00:19:34,000
of a universe existing around us.

313
00:19:34,000 --> 00:19:36,000
So we can think in terms of billiard balls,

314
00:19:36,000 --> 00:19:39,000
hitting each other and causing effect.

315
00:19:39,000 --> 00:19:41,000
It's causing effect like that.

316
00:19:41,000 --> 00:19:44,000
And Buddhism certainly subscribes to the idea of cause and effect,

317
00:19:44,000 --> 00:19:46,000
but I think it stops there

318
00:19:46,000 --> 00:19:51,000
that there is an effect of our actions

319
00:19:51,000 --> 00:19:53,000
in the present moment.

320
00:19:53,000 --> 00:19:57,000
There is things go according to cause and effect.

321
00:19:57,000 --> 00:20:01,000
But determinism is to be deterministic

322
00:20:01,000 --> 00:20:04,000
is to set yourself in the mind

323
00:20:04,000 --> 00:20:08,000
with a concept of things being fixed,

324
00:20:08,000 --> 00:20:12,000
things existing that are fixed.

325
00:20:12,000 --> 00:20:15,000
Or our bill.

326
00:20:15,000 --> 00:20:18,000
Yeah, fixed in terms of the result.

327
00:20:18,000 --> 00:20:20,000
But at the same time,

328
00:20:20,000 --> 00:20:23,000
what there is is it seems that it is possible

329
00:20:23,000 --> 00:20:24,000
to predict the future.

330
00:20:24,000 --> 00:20:26,000
The Buddha is able to do it.

331
00:20:26,000 --> 00:20:29,000
And people are able to see things that in the future.

332
00:20:29,000 --> 00:20:32,000
And the future seems to in some way,

333
00:20:32,000 --> 00:20:34,000
I mean, maybe it could be looking at it

334
00:20:34,000 --> 00:20:37,000
is that the future is able to affect the past.

335
00:20:37,000 --> 00:20:40,000
That's a way of looking at quantum physics, for example.

336
00:20:40,000 --> 00:20:42,000
Like, why when you measure something,

337
00:20:42,000 --> 00:20:46,000
does it affect something that's already been measured?

338
00:20:46,000 --> 00:20:49,000
When you affect one thing after the fact,

339
00:20:49,000 --> 00:20:54,000
it affects something that it affects something in the past.

340
00:20:54,000 --> 00:20:56,000
Strange things happen.

341
00:20:56,000 --> 00:21:00,000
So the future may be able to affect the past.

342
00:21:00,000 --> 00:21:02,000
Not so important for us,

343
00:21:02,000 --> 00:21:06,000
but what is important is this.

344
00:21:06,000 --> 00:21:08,000
So we don't want to go too far in terms of thinking.

345
00:21:08,000 --> 00:21:10,000
It's all deterministic.

346
00:21:10,000 --> 00:21:12,000
It's all wrong view.

347
00:21:12,000 --> 00:21:13,000
No question.

348
00:21:13,000 --> 00:21:16,000
And it's a very bad view to have practically speaking

349
00:21:16,000 --> 00:21:20,000
as well, because it makes you look on a lack of days

350
00:21:20,000 --> 00:21:21,000
like a lazy basically.

351
00:21:24,000 --> 00:21:26,000
But at the same time, we have to understand

352
00:21:26,000 --> 00:21:32,000
that our actions have consequences that are fixed.

353
00:21:32,000 --> 00:21:35,000
If you do this, this is going to happen.

354
00:21:35,000 --> 00:21:38,000
Well, when you do this, this comes with it.

355
00:21:38,000 --> 00:21:39,000
That kind of thing.

356
00:21:42,000 --> 00:21:44,000
And so how this relates to our meditation,

357
00:21:44,000 --> 00:21:48,000
well, talked about how meditation teaches you about karma,

358
00:21:48,000 --> 00:21:52,000
more importantly it purifies our karma.

359
00:21:52,000 --> 00:21:57,000
The most important aspect of meditation is it purifies the mind.

360
00:21:57,000 --> 00:21:59,000
And so it prepares you for death.

361
00:21:59,000 --> 00:22:05,000
Death becomes not a scary thing, not a powerful thing.

362
00:22:05,000 --> 00:22:07,000
Death just becomes another moment

363
00:22:07,000 --> 00:22:14,000
because you see that we're actually born and die in every moment.

364
00:22:14,000 --> 00:22:17,000
You train your mind, you purify your mind,

365
00:22:17,000 --> 00:22:21,000
you come to see your body and your mind clearly.

366
00:22:21,000 --> 00:22:27,000
The moments of experience.

367
00:22:27,000 --> 00:22:30,000
And then you don't have to escape.

368
00:22:30,000 --> 00:22:33,000
We're always looking for escape or a shelter,

369
00:22:33,000 --> 00:22:36,000
whether it be in the heavens or in the mountain

370
00:22:36,000 --> 00:22:38,000
or in a palace.

371
00:22:38,000 --> 00:22:42,000
Find a way to shut ourselves out from karma.

372
00:22:42,000 --> 00:22:45,000
Like super Buddha tried to do.

373
00:22:45,000 --> 00:22:47,000
I mean, it's just one example,

374
00:22:47,000 --> 00:22:49,000
but we do this all the time.

375
00:22:49,000 --> 00:22:54,000
Try and find ways to create security in the safety.

376
00:22:54,000 --> 00:22:56,000
So you don't need to do that.

377
00:22:56,000 --> 00:23:00,000
Once you're pure in the mind, clear in the mind,

378
00:23:00,000 --> 00:23:03,000
you can live on the street.

379
00:23:03,000 --> 00:23:07,000
You can live in poverty.

380
00:23:07,000 --> 00:23:10,000
You can be sick.

381
00:23:10,000 --> 00:23:15,000
You can be injured.

382
00:23:15,000 --> 00:23:18,000
You can be hungry and thirsty and pain.

383
00:23:18,000 --> 00:23:21,000
And you can be at war.

384
00:23:21,000 --> 00:23:25,000
You can be a victim of violence.

385
00:23:25,000 --> 00:23:29,000
And still be invincible.

386
00:23:29,000 --> 00:23:31,000
Then nothing overpowers you.

387
00:23:31,000 --> 00:23:34,000
Then the Buddha said, you actually don't die for anything.

388
00:23:34,000 --> 00:23:39,000
We talk about, you know.

389
00:23:39,000 --> 00:23:41,000
Anyway, you don't.

390
00:23:41,000 --> 00:23:44,000
You become free from death.

391
00:23:44,000 --> 00:23:46,000
Death cannot overpower you.

392
00:23:46,000 --> 00:23:56,000
Death doesn't overpower the one who is free from the fear of death.

393
00:23:56,000 --> 00:24:01,000
Free from the attachment to life.

394
00:24:01,000 --> 00:24:05,000
So that's the dumb Buddha for tonight.

395
00:24:05,000 --> 00:24:07,000
Thank you all for tuning in.

396
00:24:07,000 --> 00:24:30,000
See you all next time.

