1
00:00:00,000 --> 00:00:10,000
Welcome back to Ask A Month. After a long break, I'm back, I'll be trying to give regular

2
00:00:10,000 --> 00:00:17,000
teachings and a regular dates to the Ask A Month program. There are lots of questions to answer now.

3
00:00:17,000 --> 00:00:21,000
So hopefully I'll get around to some of them in your future.

4
00:00:21,000 --> 00:00:28,000
I'm settling down here in Sri Lanka, so you're going to see hopefully some of the

5
00:00:28,000 --> 00:00:32,000
things that Sri Lankan countries have in these next videos.

6
00:00:32,000 --> 00:00:39,000
So the next question comes from Foster's Blue, who says,

7
00:00:39,000 --> 00:00:46,000
I find my interest in reading and focusing in Zen, also Tibetan Buddhism and Christianity.

8
00:00:46,000 --> 00:00:52,000
Interesting, Islam and Sufi writings have caused me to wonder if I will have to choose,

9
00:00:52,000 --> 00:00:57,000
or may I learn from various paths.

10
00:00:57,000 --> 00:01:04,000
That's a good, really good question. It's one that was asked quite often in Buddhist time,

11
00:01:04,000 --> 00:01:11,000
because like today in that time there were many different paths, many different teachers.

12
00:01:11,000 --> 00:01:27,000
And I think it's important to agree that I think most of us can agree that it's unhealthy and

13
00:01:27,000 --> 00:01:35,000
unuseful to dogmatically cling to a specific path and say this path is right, and all others are wrong.

14
00:01:35,000 --> 00:01:45,000
And this isn't what the Buddha did, the Buddha himself was a very open-minded person who said

15
00:01:45,000 --> 00:01:51,000
some of the things that other teachers teach we agree with, some of the things that other teachers teach we don't agree with.

16
00:01:51,000 --> 00:01:59,000
And so he was never going to dogmatically say, only my teachings are right and every other teaching is wrong.

17
00:01:59,000 --> 00:02:07,000
The point is that you can find wisdom everywhere, and as you say, all of these of these paths have something interesting,

18
00:02:07,000 --> 00:02:17,000
and something true in them. So on the one end it's obviously wrong to dogmatically stick to a specific path.

19
00:02:17,000 --> 00:02:25,000
On the other hand, what you often see is nowadays, as we start to learn more about the various religious traditions,

20
00:02:25,000 --> 00:02:35,000
and as they come into contact, you'll find people trying to smooth things over and make things easy by saying that all traditions are the same,

21
00:02:35,000 --> 00:02:50,000
or all traditions have some equal value. And this, the Buddha didn't agree with, and this was one of the things that the Buddha tried very clearly to rectify.

22
00:02:50,000 --> 00:02:56,000
But this believed that all any teacher who said they were enlightened was enlightened and so on.

23
00:02:56,000 --> 00:03:02,000
Or all teachings were which would be given an equal footing.

24
00:03:02,000 --> 00:03:11,000
And this is what we've done today. We've established the various religious traditions up as the world's major religions,

25
00:03:11,000 --> 00:03:18,000
and we don't want to make any distinction between them, which of course is unhealthy and improper.

26
00:03:18,000 --> 00:03:28,000
We should see them for what they are and scientifically take them apart and look at them in all their pieces,

27
00:03:28,000 --> 00:03:33,000
and come to see what are their strengths and strengths and their weaknesses and compare them.

28
00:03:33,000 --> 00:03:40,000
And this is really what I would recommend is that you compare objectively the various religious traditions.

29
00:03:40,000 --> 00:03:52,000
What the Buddha said was that if you do this, that you'll come to see that his teaching is the most deep profound and the one that leads to the ultimate goal.

30
00:03:52,000 --> 00:03:59,000
So this is a very bold statement and it's one that I would say most religious traditions make.

31
00:03:59,000 --> 00:04:04,000
So the Buddha was not unlike other teachers and saying that his was the best.

32
00:04:04,000 --> 00:04:12,000
So what he said is that you rather than accepting his teaching dogmatically, this was an inappropriate thing to do.

33
00:04:12,000 --> 00:04:18,000
One should examine objectively all of the religious traditions and decide which one is best.

34
00:04:18,000 --> 00:04:33,000
The Buddha simply said that you'll come to see that the most comprehensive, objective, scientific and practical in terms of verifiable in terms of bringing results.

35
00:04:33,000 --> 00:04:35,000
What was the Buddha's teaching?

36
00:04:35,000 --> 00:04:42,000
So we should never simply try to pick and choose from various religious traditions.

37
00:04:42,000 --> 00:04:51,000
We should understand that it could very well be that some of the teachers or the leaders of these various religious traditions were not on the same level.

38
00:04:51,000 --> 00:05:05,000
That just because someone is the leader of a religion or the leader of some group or because it is well liked by the majority of the people in the world or followed by a large group of people.

39
00:05:05,000 --> 00:05:09,000
It doesn't mean that it is going to, it doesn't mean that it is true.

40
00:05:09,000 --> 00:05:16,000
It's possible that people are deluding themselves as possible that the majority of people are following the wrong path.

41
00:05:16,000 --> 00:05:27,000
And objectively and scientifically we should never take something simply on faith or even on the acceptance by the majority.

42
00:05:27,000 --> 00:05:42,000
We should study and examine it and try to see where it leads and what it brings and practice it and see for ourselves what it leads to.

43
00:05:42,000 --> 00:05:52,000
The point being that if you pick and choose from various traditions, now supposing that certain teachings are not in line with the truths,

44
00:05:52,000 --> 00:05:56,000
supposing that there are certain teachings that are leading you in the wrong direction.

45
00:05:56,000 --> 00:06:04,000
And they are going to come in conflict with those teachings that are leading you to the truth, leading you to reality, leading you to understanding, to enlightenment.

46
00:06:04,000 --> 00:06:18,000
So rather than simply picking and choosing, one should eventually either make up one's mind that none of the teachings, no teacher,

47
00:06:18,000 --> 00:06:27,000
and none of the teachings that exist in the world lead one to the ultimate realization of the truth and have a full and complete path for me to follow.

48
00:06:27,000 --> 00:06:33,000
And therefore I should make do with what I have and find the true path myself.

49
00:06:33,000 --> 00:06:53,000
Or accept that one of the teachings, one of the paths that one of the teachers that has taught since the beginning of time did have the right answer and did teach the way out of suffering and the way to true realization of reality and understanding and enlightenment and so on.

50
00:06:53,000 --> 00:07:06,000
And follow that path once you have explored and understood and examined, because if you find such a path, then there's no reason to take from other paths.

51
00:07:06,000 --> 00:07:26,000
Except sort of ancillary to somehow add something to the main path, rather than going about coming and going from one path to the other, you have the right path in front of you.

52
00:07:26,000 --> 00:07:45,000
Why would you pick and choose? When, quite likely, these other paths that you've decided that are not leading to ultimate reality and ultimate understanding are just going to conflict with this one.

53
00:07:45,000 --> 00:07:58,000
Now, it might be that some people say that there are different paths to the same goal. And I'm willing to accept this, that the very well could be different paths to the same goal.

54
00:07:58,000 --> 00:08:14,000
But it's important not to get too caught up in this sort of idea of different paths to the same goal, which we hear a lot, because it's not really, we're not talking about a mountain where you can approach it from all different types.

55
00:08:14,000 --> 00:08:29,000
We're talking about reality and reality is one. There can only be one reality, either it's true or it's not, whether it's reality or it's not. There can be a million falsehoods infinite number of false paths.

56
00:08:29,000 --> 00:08:53,000
We see such a diversity of paths, because we're seeing a lot of paths that are teaching something based on speculation that there is this, this God or this heaven or so on and so on. And as a result, they conflict because of the multiplicity of a solution of mental creation, things that have nothing to do with reality can be multiple.

57
00:08:53,000 --> 00:09:22,000
Reality has to, by its very nature, by its very definition, be one, be that which is real. And the truth has to be one, so the realization of the truth and true peace and happiness has to come through understanding and through somehow harmonizing oneself with this reality so that one act based on understanding based on those acts and speech and thought.

58
00:09:22,000 --> 00:09:45,000
That would lead one to peace, happiness and freedom from suffering. So whereas there may be many different religious traditions that are saying the same thing in different ways, it's quite more likely that many of them, and as we see many of them are teaching, the false that are teaching something based on faith or based on illusion.

59
00:09:45,000 --> 00:10:06,000
So they may have something that is beneficial, but the core doctrine or the core theory or dogma is based on a supposition or a belief and not based on the reality.

60
00:10:06,000 --> 00:10:29,000
So I would say, don't believe anyone's path is being the truth just dogmatically, but don't pick and choose unless your plan is to eventually create or find one path for yourself that is going to lead you to peace and happiness and freedom from suffering.

61
00:10:29,000 --> 00:10:42,000
And the other thing is don't, I would not accept a postmodernist view of things where you say, what is good for me is good for me or what I think is good is good.

62
00:10:42,000 --> 00:10:49,000
And just because we agree with something or something agrees with us, therefore it is right for us.

63
00:10:49,000 --> 00:11:08,000
This is the final warning that I would give that picking and choosing is nice when you're learning or when you're trying to figure out what sort of a path you're looking for and what is the differences between the paths and trying to find a framework for the path.

64
00:11:08,000 --> 00:11:20,000
But if eventually all you're doing is simply augmenting or verifying your own beliefs, your own views, your own opinions, your own ego, really.

65
00:11:20,000 --> 00:11:31,000
And picking up things simply because they agree with your way of life, agree with the things that you are attached to or you cling to or you believe it.

66
00:11:31,000 --> 00:11:56,000
Then there's no spiritual development at all, you're simply reifying your ego, your defile, your attachments, your cleaning state of mind and you're not learning to understand something, you're not broadening your horizons, you're not developing spiritual.

67
00:11:56,000 --> 00:12:17,000
So, eventually you're going to have to examine your own beliefs and your own views and hold them up against the various religious traditions and come to see which one is objectively true,

68
00:12:17,000 --> 00:12:26,000
which one objectively does lead to peace, happiness, freedom from suffering, spiritual development and lightning and so on.

69
00:12:26,000 --> 00:12:55,000
So, hope that helped and just a caution not to not to pick and choose, the spiritual path is not a buffet, something that you have to dive into and follow to your utmost and really have to give up all of the things that you believe in, all the things that you've all done to eventually you're going to have to let go of everything so that you can come to be free, totally incomplete.

70
00:12:55,000 --> 00:13:10,000
And so, as a result, you really do need to pick one specific path and follow it diligently, not going a short ways and then backing off and going up another path and then backing off.

71
00:13:10,000 --> 00:13:24,000
You're going to have to figure out which path is right for you, whether it's some path that you've worked about your own or whether it's one of the paths that has been established that you realize is the truth.

72
00:13:24,000 --> 00:13:41,000
And it's based on reality and has a verifiable goal and conclusion and has a detailed teaching that leads you to that conclusion, which I would recommend is the Buddhist teaching that's my own following.

73
00:13:41,000 --> 00:13:56,000
But the other thing I would say is that you probably, I would boast, I suppose, that you won't find such a claim in the other religions where as most paths will say something like this or ours is the best.

74
00:13:56,000 --> 00:14:12,000
And you won't find this objective explanation where you say find the one that is most objective and most verifiable in the year and now and most scientific.

75
00:14:12,000 --> 00:14:24,000
And that's our, you'll find in several people say believe this, this is where we believe it's the truth and if you don't believe it, this and this is going to happen.

76
00:14:24,000 --> 00:14:39,000
And one that's based on inference or extrapolation where you have some special experience and you say that's this God or that's that God or that's, you know, heaven or or this realization that realization.

77
00:14:39,000 --> 00:14:52,000
The Buddhist teaching is really scientific and something that allows you to realize the truth without extrapolating or jumping to any conclusion whatsoever.

78
00:14:52,000 --> 00:15:10,000
And what we say is that your draw will be verifiable and will be based on an exact one to one correlation with your experiences when you experience something you'll have a conclusion that that is so it is like that because you've experienced as opposed to extrapolating.

79
00:15:10,000 --> 00:15:39,000
So my post I would say a Buddhism is that it is that and it is the teaching that is most complete and as the as the most detailed and accurate and scientific teaching that it has verifiable results and leads to a verifiable goal and works for those who put their effort into it.

80
00:15:39,000 --> 00:16:04,000
So I guess ultimately my answer is no you don't have to pick you don't have to pick and choose in the end you're going to come to Buddhism if you've if you've been objective and if you've come to come if you've been honest with yourself that eventually you'll come to realize that with the Buddha was an enlightened being and that's anyone who is enlightened will teach these things because this is the truth.

81
00:16:04,000 --> 00:16:33,000
So really the word Buddhism in fact is simply a word that means the teaching of the enlightened one so it's the meaning is that this teaching is the teaching of those people who have for themselves come to realize the truth and it's not because of faith it's not it's not God that came down and told them these things or it wasn't given from anywhere it was something that was realized for themselves.

82
00:16:33,000 --> 00:16:53,000
So this is therefore the point of saying that not all religious traditions are the same because if someone is not enlightened they might still teach and they might still gain acceptance from a larger number of people but that doesn't mean that they're enlightened that doesn't mean that they're teaching is right and good if they're not not yet enlightened.

83
00:16:53,000 --> 00:17:12,000
This is the criteria of being Buddhism or not Buddhism something is Buddhism if it's based on the teaching of someone who is totally perfectly enlightened someone who has come to realize the truth the whole truth for themselves and has come to be fully free from clinging free from craving free from suffering.

84
00:17:12,000 --> 00:17:27,000
So I hope that helps and I'm sure it's not exactly what you're looking for I know we like to pick and choose and be free in our pursuits this is why there's the rise of postmodernism.

85
00:17:27,000 --> 00:17:49,000
But there you have it this is Ask a Monk thanks for tuning in and we'll see you all the best.

