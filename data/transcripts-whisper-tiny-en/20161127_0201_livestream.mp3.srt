1
00:00:00,000 --> 00:00:26,000
Good evening, everyone, and welcome to our evening broadcast.

2
00:00:26,000 --> 00:00:46,000
So last night we talked about the bad things that come of meditation, the necessary evil, I suppose,

3
00:00:46,000 --> 00:01:09,000
or the results of the practice in terms of how it allows you to see the problems that create suffering.

4
00:01:09,000 --> 00:01:23,000
So the negative side of the practice, but of course it's not all negative.

5
00:01:23,000 --> 00:01:36,000
In fact there's great benefit, great goodness involved in seeing the problems in this way.

6
00:01:36,000 --> 00:01:45,000
It allows you to realize the benefits of the practice, what's your many?

7
00:01:45,000 --> 00:02:12,000
So I thought I'd go over those tonight.

8
00:02:15,000 --> 00:02:28,000
Perhaps someone whose voice isn't working, just thought I'd send them a message, I think.

9
00:02:28,000 --> 00:02:43,000
So the first way of understanding benefits, of course we're going by the Buddhist texts themselves.

10
00:02:43,000 --> 00:03:04,000
We have what we call the five aims of the practice of Satipatana, but it gave us a list of five reasons for practicing mindfulness.

11
00:03:04,000 --> 00:03:30,000
Five bold claims, I suppose, that is indeed the path of mindfulness that leads to these five benefits.

12
00:03:30,000 --> 00:03:52,000
And so the first one is the purity, the purity of mind, the benefit of seeing the negative aspects of experience,

13
00:03:52,000 --> 00:04:02,000
is that it cleanses any sort of desire or attachment from the mind.

14
00:04:02,000 --> 00:04:21,000
The mind is no longer left wanting, left hungry, left discontent in its search for satisfaction and pleasure.

15
00:04:21,000 --> 00:04:31,000
Through wisdom, through seeing things as they are, the mind slowly begins to let go.

16
00:04:31,000 --> 00:05:00,000
We can sit, grasp it, we can sit, drive to obtain and attain and become, specifically the mind becomes free from greed, from anger and from delusion, the three roots of all evil.

17
00:05:00,000 --> 00:05:26,000
And so the key here is that we don't, we don't artificially construct the freedom from defilement,

18
00:05:26,000 --> 00:05:33,000
and not trying to control our mind to not become angry or greedy or deluded.

19
00:05:33,000 --> 00:05:50,000
The really important and I would say, recently profound point of this teaching is that it's not by controlling the mind or by fixing the problem.

20
00:05:50,000 --> 00:06:03,000
That something so simple and so benign as mindfulness turns out to be the key that unlocks the door to freedom from suffering.

21
00:06:03,000 --> 00:06:13,000
It gets us out of the burning building of defilements.

22
00:06:13,000 --> 00:06:34,000
If you want something, you don't have to tell yourself no, you just have to study and learn about your desires and the object of your desire until you realize the truth, which is that that thing is not worth clinging to.

23
00:06:34,000 --> 00:06:45,000
So the freedom is much more like boredom. Freedom from suffering is like boredom in the sense that you get fed up.

24
00:06:45,000 --> 00:06:50,000
You don't exactly get bored, but you get bored of suffering.

25
00:06:50,000 --> 00:06:58,000
You suffer again and again and suffer so much that you finally say, phew, enough of that.

26
00:06:58,000 --> 00:07:12,000
Freedom, freedom isn't from running away or avoiding or controlling or contriving. It doesn't come from tricks or artifices.

27
00:07:12,000 --> 00:07:18,000
At the nibindatiduke is the mango is the dia.

28
00:07:18,000 --> 00:07:31,000
Then one becomes, I want to say sick, but not sick, but becomes disenchanted with suffering. I think we become sick of it.

29
00:07:31,000 --> 00:07:43,000
This is the path of purification.

30
00:07:43,000 --> 00:07:50,000
It's the first benefit. That in and of itself is the major benefit of meditation, purity.

31
00:07:50,000 --> 00:08:10,000
So I talk again and again about goodness, trying to instill in all of us the sense of importance or the focus on the importance of goodness over happiness.

32
00:08:10,000 --> 00:08:26,000
The importance of wholesomeness, the importance of the instigation of positive states.

33
00:08:26,000 --> 00:08:31,000
Suppose they're just waiting for them to come to you, waiting for happiness to drop in your lap.

34
00:08:31,000 --> 00:08:50,000
Like some miracle from God to actually cultivate the causes of happiness.

35
00:08:50,000 --> 00:09:13,000
I've been looking recently at these various, there are, I would say, counterculture, I suppose, on the internet dedicated to goodness.

36
00:09:13,000 --> 00:09:26,000
It's counterculture because the internet I think exploded mainly on negative aspects of humanity, mostly greed, instant gratification.

37
00:09:26,000 --> 00:09:35,000
But there are some aspects, some parts of the internet. I found one today, I've subscribed to what is it, uplifting news.

38
00:09:35,000 --> 00:09:48,000
So news that is actually about the good things that people do. But recently, I found something called wholesome memes.

39
00:09:48,000 --> 00:09:57,000
So it's a new take on memes that's just all about good things. It's really quite wonderful and endearing.

40
00:09:57,000 --> 00:10:15,000
So I posted one today, a fun drinking game, a fun drinking game. Take a shot of water every two hours, so to keep yourself healthy and hydrated.

41
00:10:15,000 --> 00:10:42,000
I have no idea that's not actually wholesome, it's my only point is transforming our minds from a focus on gratification and running away from our problems, to keeping moral precepts, like not drinking alcohol, etc.

42
00:10:42,000 --> 00:10:51,000
It's actually doing something about it, you know, practicing meditation.

43
00:10:51,000 --> 00:11:00,000
And so that's the major benefit, the major benefit is not happiness, it's purity. You don't worry about the happiness part.

44
00:11:00,000 --> 00:11:13,000
We want true purity, not a purity that is controlled or artificial.

45
00:11:13,000 --> 00:11:21,000
True purity is the source of true happiness.

46
00:11:21,000 --> 00:11:24,000
But there are lots more benefits and they come from purity.

47
00:11:24,000 --> 00:11:34,000
The second one is overcoming sorrow, limitation, and despair, and included in here would be all sorts of mental illness.

48
00:11:34,000 --> 00:11:48,000
Meditation cures the mind of mental illnesses which end up being no more than simple obsessions over desires, and versions, and delusions.

49
00:11:48,000 --> 00:12:12,000
And when you see with wisdom that the things worth clinging to, problems like depression, anxiety, worry, when you stop seeing things as me or mine, myself, all of these states are no longer relevant.

50
00:12:12,000 --> 00:12:20,000
They're no longer useful, no longer seen as useful or seen as, right? Once you realize that something doesn't belong to you, why would you worry about it?

51
00:12:20,000 --> 00:12:31,000
The story of this man who drives the ox, he wakes up early in the morning and drives his ox up to the pasture, and then it gets light, and he realizes it isn't his ox.

52
00:12:31,000 --> 00:12:41,000
And so he drives it back and forgets about it.

53
00:12:41,000 --> 00:12:52,000
Like the woman who picks up the wrong baby and starts to nurse it until she realizes it isn't hers, and puts it back down and forgets about it.

54
00:12:52,000 --> 00:13:02,000
And other stories like this, that similes like this in the R2D manga, describing this, and this release.

55
00:13:02,000 --> 00:13:29,000
And even schizophrenia, I talked several times about this, how it seems to me that schizophrenia, though, accompanied with incredible paranoia, I think in most cases, crippling paranoia, I think the paranoia aspect could be ameliorated, if not cured, to the extent that the person is able to deal with their hallucinations mindfully.

56
00:13:29,000 --> 00:13:37,000
Certainly not easy, but potential is there.

57
00:13:37,000 --> 00:13:44,000
The third benefit is suffering, overcoming suffering, mental suffering, physical suffering.

58
00:13:44,000 --> 00:13:48,000
Once your mind is pure, you don't suffer.

59
00:13:48,000 --> 00:13:52,000
The physical suffering, of course, you can't remove it.

60
00:13:52,000 --> 00:13:57,000
Just because your mind is pure doesn't mean you're not going to feel physical pain,

61
00:13:57,000 --> 00:14:06,000
but there's no mental pain, there's no disliking of the pain.

62
00:14:06,000 --> 00:14:10,000
And so physical pain becomes just another sensation.

63
00:14:10,000 --> 00:14:20,000
Once you're able to see it clearly and remove that kernel of dislike that makes it painful in the first place.

64
00:14:20,000 --> 00:14:30,000
And then it's only just a sensation, like any other sensation. Happiness is a sensation. Pleasure is a sensation. Pain is also a sensation.

65
00:14:30,000 --> 00:14:38,000
Very happiness is in the mind, but pleasure and pain, or Socrates said something like this.

66
00:14:38,000 --> 00:14:43,000
If you read the apology of Socrates, he says something about pleasure and pain.

67
00:14:43,000 --> 00:14:44,000
That's interesting.

68
00:14:44,000 --> 00:14:51,000
I mean, probably not in the coming from the same place, but it's so much philosophical about.

69
00:14:51,000 --> 00:15:00,000
I think about there being very little difference.

70
00:15:00,000 --> 00:15:07,000
The fourth benefit is that you get on the right path through the practice of insight meditation.

71
00:15:07,000 --> 00:15:14,000
You enter into the right path.

72
00:15:14,000 --> 00:15:19,000
The key here is that the right path turns out not to be what you do with your life.

73
00:15:19,000 --> 00:15:26,000
Who you are, what you are, but how you live your life.

74
00:15:26,000 --> 00:15:30,000
You don't have to fret or worry about what you do.

75
00:15:30,000 --> 00:15:37,000
If you wind up living on a park bench, live on a park bench mindfully.

76
00:15:37,000 --> 00:15:43,000
If you're living in a mansion, do it mindfully.

77
00:15:43,000 --> 00:15:53,000
If you're a doctor or a lawyer, be a doctor, be a mindful doctor or a mindful lawyer to the extent possible.

78
00:15:53,000 --> 00:16:00,000
I think it's possible, mindful lawyers and mindful doctors, mindful politicians.

79
00:16:00,000 --> 00:16:08,000
Wouldn't it be great if our politicians all just sat down and meditate it?

80
00:16:08,000 --> 00:16:14,000
My mom was sending me pictures of Donald Trump's head on a Buddhist monk's body.

81
00:16:14,000 --> 00:16:16,000
Not sure how I feel about that.

82
00:16:16,000 --> 00:16:24,000
I'm not sure how I should feel about that, but not sure.

83
00:16:24,000 --> 00:16:30,000
Wouldn't it be something?

84
00:16:30,000 --> 00:16:37,000
The right way, the right way, it sounds like such an arrogant thing to say.

85
00:16:37,000 --> 00:16:43,000
If anyone understands, if you understand mindfulness, it doesn't seem unreasonable at all.

86
00:16:43,000 --> 00:17:04,000
Anyone without mindfulness cannot ever be said to be on the right path, no matter what they do, no matter how kind they seem to be without mindfulness they can't perform real kindness.

87
00:17:04,000 --> 00:17:13,000
And so what happens is through the meditation, you develop habits, you break down bad habits and you cultivate good habits.

88
00:17:13,000 --> 00:17:20,000
And your mind changes, your path changes, not just in this life.

89
00:17:20,000 --> 00:17:32,000
So that's the thing about habits, especially deeply, spiritual habits, or deeply meaningful habits like meditation habits.

90
00:17:32,000 --> 00:17:42,000
When it's based on wisdom or ignorance, it's that they affect the change, the foundation of who you are.

91
00:17:42,000 --> 00:17:50,000
In this life and in the next, a summing loca parameter.

92
00:17:50,000 --> 00:17:54,000
In this life and the next and all lives to come.

93
00:17:54,000 --> 00:18:04,000
That's the greatness of meditation is that karma isn't like a bank where you do all these good deeds and you can cash in later on.

94
00:18:04,000 --> 00:18:06,000
Karma changes you.

95
00:18:06,000 --> 00:18:10,000
True karma isn't the act, it's the mind state.

96
00:18:10,000 --> 00:18:21,000
And so meditation is like karma to the end's degree because every moment you're creating wholesome karma.

97
00:18:21,000 --> 00:18:27,000
You're changing who you are on an accelerated level, accelerated pace.

98
00:18:27,000 --> 00:18:32,000
Instead of every time you do a good deed, there's some wholesomeness involved.

99
00:18:32,000 --> 00:18:41,000
Every moment that you say rising or you say falling or you say sitting or you say pain, every time you recognize something as it is,

100
00:18:41,000 --> 00:18:45,000
you're changing who you are, you're cultivating good karma.

101
00:18:45,000 --> 00:19:02,000
This is how encouraged we should be by the practice with the understanding that we're creating good karma every moment that we're mindful.

102
00:19:02,000 --> 00:19:17,000
Now the Buddha said that.

103
00:19:17,000 --> 00:19:23,000
And the final benefit, of course, is the realization of freedom.

104
00:19:23,000 --> 00:19:34,000
Now everything up until this one sort of talks about freedom, but nibana, nirvana is true freedom and it's on a different level than the rest of them.

105
00:19:34,000 --> 00:19:36,000
It comes out of the rest.

106
00:19:36,000 --> 00:19:48,000
Once you begin to follow the right path and you've freed yourself from mental illness of all kinds, major, minor.

107
00:19:48,000 --> 00:19:56,000
The mind lets go, the mind begins to see clearly and more clearly until finally there's an epiphany.

108
00:19:56,000 --> 00:19:58,000
It really isn't epiphany.

109
00:19:58,000 --> 00:20:00,000
Maybe not in the sense that we think about it.

110
00:20:00,000 --> 00:20:15,000
There's no real intellectualizing, but it's just a moment of clarity where the mind says to itself not so many words, but it just clicks.

111
00:20:15,000 --> 00:20:18,000
Sabhe, dhamma, namma, namma, bhini, vaisaya.

112
00:20:18,000 --> 00:20:22,000
All dhammas are not worth clinging to and let's go.

113
00:20:22,000 --> 00:20:25,000
Poof.

114
00:20:25,000 --> 00:20:29,000
It's like turning off the lights.

115
00:20:29,000 --> 00:20:34,000
It's like unplugging your computer, pressing the power button and it all goes quiet.

116
00:20:34,000 --> 00:20:41,000
It all goes cold, cool.

117
00:20:41,000 --> 00:20:46,000
The heat is dissipated.

118
00:20:46,000 --> 00:20:55,000
Noise and chaos and suffering is all.

119
00:20:55,000 --> 00:21:02,000
It's all destroyed or done away with.

120
00:21:02,000 --> 00:21:04,000
This is the main goal of meditation.

121
00:21:04,000 --> 00:21:05,000
There is this goal.

122
00:21:05,000 --> 00:21:08,000
It's something to be feared, not something to worry about.

123
00:21:08,000 --> 00:21:12,000
Stay it and you can think of it like a meal.

124
00:21:12,000 --> 00:21:14,000
All you have to do is taste nibana.

125
00:21:14,000 --> 00:21:23,000
If you don't like the taste, you can go back and say, this enlightenment thing is not for me.

126
00:21:23,000 --> 00:21:36,000
But it's kind of go quickly and possible to do that because anyone who tastes nibana is so real

127
00:21:36,000 --> 00:21:49,000
that it's impossible for you to see other than that nibana, nibana is the highest happiness.

128
00:21:49,000 --> 00:21:51,000
That's what they say.

129
00:21:51,000 --> 00:21:58,000
That the person who sees nibana, it's impossible because it's changed fundamentally the nature of their mind.

130
00:21:58,000 --> 00:22:05,000
Through wisdom, not through any mystical, magical power, but through some higher form of wisdom

131
00:22:05,000 --> 00:22:17,000
that's just simply seeing true peace and the realization that all this stuff we called peace or happiness before that was not true peace, not true happiness.

132
00:22:17,000 --> 00:22:35,000
It's a categorical or irrevocable wisdom that comes from attaining nibana or realizing nibana for yourself.

133
00:22:35,000 --> 00:22:36,000
So there you go.

134
00:22:36,000 --> 00:22:46,000
These benefits should be familiar to many of you already, but I like talking about them for my own benefit.

135
00:22:46,000 --> 00:22:49,000
It's good for us to remember good things.

136
00:22:49,000 --> 00:23:00,000
It gives us an opportunity to meditate here together and meditate in a sort of special sense of maybe a western sense of the word to meditate and mold these things over.

137
00:23:00,000 --> 00:23:11,000
To step back from our practice and reflect on what we might be doing wrong and how to adjust our practice and to reaffirm what we're doing right.

138
00:23:11,000 --> 00:23:16,000
And to encourage us in it.

139
00:23:16,000 --> 00:23:18,000
So there you go.

140
00:23:18,000 --> 00:23:20,000
That's the dhamma for this evening.

141
00:23:20,000 --> 00:23:27,000
Thank you all for coming up.

142
00:23:27,000 --> 00:23:41,000
And I'm going to, I'm going to ask to leave early tonight, there are some things that I have to do.

143
00:23:41,000 --> 00:23:49,000
So no questions tonight if you have any questions, come on back hopefully tomorrow.

144
00:23:49,000 --> 00:23:56,000
Although this next week or so it might be a little bit if you have to see how things go.

145
00:23:56,000 --> 00:24:02,000
Anyway, lots of see you all in the future.

146
00:24:02,000 --> 00:24:27,000
Good night.

