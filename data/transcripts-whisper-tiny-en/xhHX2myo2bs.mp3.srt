1
00:00:00,000 --> 00:00:04,340
Hello and welcome back to our study of the Dhamapada.

2
00:00:04,340 --> 00:00:10,980
Today we continue on with verse number ninety-nine, which reads as follows.

3
00:00:10,980 --> 00:00:25,060
Ramaniyani Aranyani, Yatana Ramati Janou, Vittaraga Ramisanti, Natekama Gavizinou, which means

4
00:00:25,060 --> 00:00:38,500
those forests are delightful, where there is no one, no being, Ramati Janou, no being, no person who delights.

5
00:00:38,500 --> 00:00:52,660
Vittaraga Ramizanti, those free from passion will delight Natekama Gavizinou, not those who are gone to,

6
00:00:52,660 --> 00:01:03,300
or are seeking out Gavizat, not those who seek out pleasure, so quite an instructive verse.

7
00:01:03,300 --> 00:01:08,460
It came quite similar to our last verse, if you're keeping up the last one was about delightful

8
00:01:08,460 --> 00:01:14,740
forests as well, but that was about where an Arahan dwells, anywhere an Arahan dwells

9
00:01:14,740 --> 00:01:17,980
that's a delightful place.

10
00:01:17,980 --> 00:01:22,340
So this verse actually doesn't really have a place in the Arahan Dawaga except that it follows

11
00:01:22,340 --> 00:01:27,060
after the last verse, which didn't talk about Arahan's.

12
00:01:27,060 --> 00:01:31,340
Of course this one does talk about enlightened beings, and the story is about Arahan

13
00:01:31,340 --> 00:01:34,580
ship, so let's get right to the story.

14
00:01:34,580 --> 00:01:40,500
Here we have a story of a monk who was practicing meditation in the forest, he had found

15
00:01:40,500 --> 00:01:50,180
this delightful forest in which to meditate, he had thought to himself, oh here's a place

16
00:01:50,180 --> 00:01:58,140
where I will certainly progress in my practice and so he strove to his utmost in this forest

17
00:01:58,140 --> 00:02:08,500
doing walking and sitting meditation, but fortune had it, or bad fortune it was, that

18
00:02:08,500 --> 00:02:17,460
a court is in or a prostitute happened to think the same thing, boy this forest would

19
00:02:17,460 --> 00:02:24,420
be a nice place to meet a client, a good place, so two kinds of delighting or two kinds

20
00:02:24,420 --> 00:02:35,860
of pleasure, two kinds of enjoying where, well they had the same idea, but for two different

21
00:02:35,860 --> 00:02:37,660
types of enjoyment.

22
00:02:37,660 --> 00:02:42,860
So the monk was here sitting in the forest and the prostitute or court is in or however

23
00:02:42,860 --> 00:02:50,900
you want to translate it, was also in the forest with an appointment with one of her clients,

24
00:02:50,900 --> 00:02:55,060
and so she was quite near where the monk was and she was sitting in the forest patiently

25
00:02:55,060 --> 00:03:03,460
waiting for her client and as it turned out her client didn't show up, so she was left

26
00:03:03,460 --> 00:03:10,580
sitting in this forest waiting for her client, until finally impatient she realized

27
00:03:10,580 --> 00:03:18,260
she is certainly not going to show up, might as well just go back home, and on her way

28
00:03:18,260 --> 00:03:27,980
back home, lo and behold she spied a monk, I guess he was somewhat handsome or at least

29
00:03:27,980 --> 00:03:34,900
looked like a suitable client, not someone who might be gullible enough or might be be

30
00:03:34,900 --> 00:03:44,820
geiled by her ways, and so she decided in her mind, she made up her mind in a very who

31
00:03:44,820 --> 00:03:52,620
could say a fairly unwholesome decision to seduce the monk, so she went up to this monk

32
00:03:52,620 --> 00:03:57,940
who was sitting there valiantly striving in meditation, he wasn't enlightened, so there

33
00:03:57,940 --> 00:04:10,420
was hope for her, and so she came up close where the monk was and cleared her throat,

34
00:04:10,420 --> 00:04:16,060
the monk looked up and saw her and she let her robe slip a little bit and showed a little

35
00:04:16,060 --> 00:04:26,300
bit of just sitting as though she was just there to perhaps meditate or to enjoy the

36
00:04:26,300 --> 00:04:35,020
peace and quiet, but moving her skirts ever so slightly to show her legs and shifting

37
00:04:35,020 --> 00:04:45,340
around and showing off her attributes, and sure enough the monk found his concentration

38
00:04:45,340 --> 00:04:57,460
beginning to leave her, he found himself suddenly rather disturbed, and not sure it's an

39
00:04:57,460 --> 00:05:02,700
interesting thing that he says, because he says though he's never felt such desire before,

40
00:05:02,700 --> 00:05:11,900
because his whole body begins to shake, and maybe he's sweat comes out of his armpits,

41
00:05:11,900 --> 00:05:18,980
sorry, his body heats up, and he becomes confused, he doesn't know what it is, he doesn't

42
00:05:18,980 --> 00:05:24,980
understand what's happening to him, so he actually says what is this, something that

43
00:05:24,980 --> 00:05:35,900
King Nokowi dang, what is this, what the heck is going on, anyway he was quite disturbed,

44
00:05:35,900 --> 00:05:44,220
and probably in a bit of trouble, who knows what would have happened if it weren't

45
00:05:44,220 --> 00:05:53,580
for our favorite savior, our savior, the one who would always save the monks, who saved

46
00:05:53,580 --> 00:06:02,060
the monks from themselves, the Buddha, the Buddha was watching out for him, and it's again

47
00:06:02,060 --> 00:06:09,700
one of these cases where you might believe or you might not believe, it might be unbelievable

48
00:06:09,700 --> 00:06:21,540
to some people, but the Buddha had this power in his mind, the ability to sense or a heightened

49
00:06:21,540 --> 00:06:27,180
awareness of reality, so the story goes that he knew what was going on, he was able

50
00:06:27,180 --> 00:06:34,220
to understand and further more able to communicate with this monk, if you don't like that,

51
00:06:34,220 --> 00:06:39,260
if that sounds far fetched and implausible about that, it's fine, it's not a matter, it's

52
00:06:39,260 --> 00:06:46,580
more of the meaning behind the story, but for most of us, there's a sense that there is

53
00:06:46,580 --> 00:06:58,940
more possible than most of us are able to understand, so these ideas of ESP or telepathy,

54
00:06:58,940 --> 00:07:08,020
clairvoyance, clairaudience, mind reading, even prediction of the future, these kind of things,

55
00:07:08,020 --> 00:07:14,860
there is some sense that to some extent the mind that is cultivated can engage in these

56
00:07:14,860 --> 00:07:25,220
things, and there's accounts by the tongue of meditators who claim to have these powers.

57
00:07:25,220 --> 00:07:33,540
They seem to be a bit unpredictable and certainly hard to capture by doctors looking

58
00:07:33,540 --> 00:07:39,820
to prove or disprove, so there's been experiments that have been done, and if you can learn

59
00:07:39,820 --> 00:07:46,180
about some of these experiments that they've done in textbooks and often are inconclusive,

60
00:07:46,180 --> 00:07:52,020
it's a hard thing to test because it takes an exceptional frame of mind and it exceptions

61
00:07:52,020 --> 00:07:58,780
are not something that science does best with, of course, it's easiest to prove or disprove

62
00:07:58,780 --> 00:08:06,780
something that applies to the majority, because then you can get statistically meaningful

63
00:08:06,780 --> 00:08:12,180
results, but if you're dealing with a one in a million thing, something that most people

64
00:08:12,180 --> 00:08:17,180
can't do, it's a little bit harder, of course most people just deny this is the possibility

65
00:08:17,180 --> 00:08:21,260
outright, but then most people don't practice meditation and don't have a sense of the

66
00:08:21,260 --> 00:08:26,660
power of the mind anyway, either way it's not the point of the story, the point is with

67
00:08:26,660 --> 00:08:30,580
the Buddha taught to this monk, so if you don't like the actual story you can pretend

68
00:08:30,580 --> 00:08:35,340
it's a story about the Buddha being with this monk and he sees what's going on, maybe

69
00:08:35,340 --> 00:08:39,980
he was meditating right beside the monk, that's not how the story goes, but you can look

70
00:08:39,980 --> 00:08:45,260
at it that way, it's the same meaning, because the Buddha turns to the monk and says,

71
00:08:45,260 --> 00:08:50,780
around Maniyani, around Nyani, it says, you want to know what's going on, why you're

72
00:08:50,780 --> 00:08:57,380
not happy, he says you can only really enjoy the forest if you're not full of lust.

73
00:08:57,380 --> 00:09:02,740
There's more to this first actually than the story tells, but this is when the Buddha

74
00:09:02,740 --> 00:09:08,140
said to have told this first, but there's a deeper meaning here, because being alone

75
00:09:08,140 --> 00:09:13,100
in the forest is tough, if you go off into the forest thinking I'm going to be free

76
00:09:13,100 --> 00:09:19,420
from all my problems, you learn pretty quick that all your problems come with you, and

77
00:09:19,420 --> 00:09:26,380
if you're not able to overcome this desire, it doesn't matter whether you're in the forest

78
00:09:26,380 --> 00:09:33,860
or in the city, it makes the point here that the works has to be done, going into the

79
00:09:33,860 --> 00:09:39,700
forest is great, it's a great opportunity to focus your mind on the essentials, it's a great

80
00:09:39,700 --> 00:09:49,340
way to protect yourself from yourself, so when you get angry, when you have lust, there's

81
00:09:49,340 --> 00:09:55,100
less of opportunity to act out on it, so it is much easier to be patient with, so you want

82
00:09:55,100 --> 00:09:59,100
something, well if you know you're not going to get it, it's much easier to be mindful,

83
00:09:59,100 --> 00:10:14,780
it's much easier to let it go, so in essence it's protecting you, or it's a guard from

84
00:10:14,780 --> 00:10:20,460
yourself, so like training wheels on the bike, being in the forest makes it easier, especially

85
00:10:20,460 --> 00:10:25,620
for new meditators, but it's never going to be comfortable, you're never going to be happy,

86
00:10:25,620 --> 00:10:30,300
and you're never going to become free unless you can do the work, the forest won't do the

87
00:10:30,300 --> 00:10:37,780
work for you, so it says, yeah, da-na-na-na-na-na-na-na-na, where there's no one delighting, it's

88
00:10:37,780 --> 00:10:42,620
a play on words, the word-na-na-na-na-na is the same root as Ramani, Ramani is a forest

89
00:10:42,620 --> 00:10:49,540
that is delightful, it's delightful when no one's delighting, so the difference is delighting

90
00:10:49,540 --> 00:10:57,900
in sensuality and delighting in freedom, and delighting is probably a bad word, but being

91
00:10:57,900 --> 00:11:03,740
happy might be the best, it's pleasant, pleasant might be a good word, it's pleasant

92
00:11:03,740 --> 00:11:12,300
where no one seeks pleasure might be the best translation here, those who are free from

93
00:11:12,300 --> 00:11:21,180
passion are pleased, might be a good one, but the point, the point being, and the key to

94
00:11:21,180 --> 00:11:28,700
this first is this idea of what can truly please, or what can truly make you happy, that's

95
00:11:28,700 --> 00:11:38,060
the key here, and it's the key to solving this monk's problem, because his mind, so

96
00:11:38,060 --> 00:11:45,100
assumedly, is inclining to find pleasure with his core descent and his prostitute, it's

97
00:11:45,100 --> 00:11:54,380
inclining towards the pleasure of a body that is attractive, probably because, or we

98
00:11:54,380 --> 00:12:03,220
understand because of the mind's inclination, or the mind's recognition of it as an

99
00:12:03,220 --> 00:12:10,380
object of pleasure, so we find pleasurable stimulation from either the same gender or the

100
00:12:10,380 --> 00:12:18,300
opposite gender, depending on our sexual preference, but we find it just in vision because

101
00:12:18,300 --> 00:12:26,860
of lifetime after lifetime, or scientists would say, because of our biology, which is

102
00:12:26,860 --> 00:12:38,100
programmed to recognize the object as an object of pleasure, a place that is to be enjoyed,

103
00:12:38,100 --> 00:12:47,220
so it leads to physical pleasure, it leads to sounds, pleasant sounds, sounds of love and

104
00:12:47,220 --> 00:12:53,620
of enjoyment and laughter and so on, all of these pleasant sounds that we so enjoy, pleasant

105
00:12:53,620 --> 00:12:59,180
sights, pleasant sounds, pleasant smells, so as a result as soon as you see that someone

106
00:12:59,180 --> 00:13:07,420
and you say that's a woman, the sign of the woman for a man inflames the mind, so this

107
00:13:07,420 --> 00:13:14,220
is where his mind was inclined towards, but he realized something it seems, not only

108
00:13:14,220 --> 00:13:19,100
was he caught up in it, but he realized that it was actually disturbing it, he said, what's

109
00:13:19,100 --> 00:13:24,060
going on, what's the meaning of this, and the Buddha straightens out the meaning, they

110
00:13:24,060 --> 00:13:29,740
saying that actually our point's out that, hey look what's going on with you, look what

111
00:13:29,740 --> 00:13:34,460
they look where this is leading, and it's another useful thing about the forest is that

112
00:13:34,460 --> 00:13:40,140
you get to a different state of calm, a different state of peace, so you have pleasure in

113
00:13:40,140 --> 00:13:48,020
a sense that is, you could say wholesome, it's the pleasure of peace, the pleasure of a calm

114
00:13:48,020 --> 00:13:53,900
state, now I think you could still argue that it would lead to attachment because you

115
00:13:53,900 --> 00:14:02,940
can get attached to calm and peaceful states, but there are aspects of it that are wholesome

116
00:14:02,940 --> 00:14:09,180
where the mind is calm and undisturbed, where the mind is fixed and focused, so actually

117
00:14:09,180 --> 00:14:17,860
considered to be wholesome, useful, and that's in contrast with trying to find pleasure

118
00:14:17,860 --> 00:14:25,060
in last in the sensuality, because all the chemicals that are involved, all the hormones

119
00:14:25,060 --> 00:14:30,420
that are involved are actually quite disturbing, and they inflame the mind, and they lead

120
00:14:30,420 --> 00:14:40,900
to jealousy and irritation, they lead to frustration, they lead eventually to a lot of anger

121
00:14:40,900 --> 00:14:47,420
states, unpleasant states, and so he was seeing in a sense the unpleasantness of it,

122
00:14:47,420 --> 00:14:58,140
it's really the key to this problem of sensuality, this problem of desire, that's the fact,

123
00:14:58,140 --> 00:15:05,180
the simple fact that it's essential for us to learn that these things can't satisfy us,

124
00:15:05,180 --> 00:15:11,620
that they don't satisfy us, that they don't make us happy, you can take them, it's

125
00:15:11,620 --> 00:15:20,660
the crux of the problem, really, it's like the answer to this question, why am I not happy?

126
00:15:20,660 --> 00:15:26,380
I'm indulging, I'm enjoying, I'm engaging, I have everything anyone could want, why am

127
00:15:26,380 --> 00:15:32,220
I not happy, some people I don't think ever come to that even, they don't ever realize

128
00:15:32,220 --> 00:15:38,100
that they're not happy, that this argument with people before, where they say, well,

129
00:15:38,100 --> 00:15:42,140
we get this question a lot actually, what's wrong with sensual pleasures, if it makes

130
00:15:42,140 --> 00:15:46,180
you happy, well, if it really made you happy, there'd be nothing wrong with it, the problem

131
00:15:46,180 --> 00:15:52,140
is that it doesn't, and it's funny that often we don't even realize that, those of us

132
00:15:52,140 --> 00:15:57,700
who do are the ones inclined towards spirituality, we wouldn't say that's a better thing

133
00:15:57,700 --> 00:16:07,140
to be displeased by these things, because it also answers the question as to why we, or

134
00:16:07,140 --> 00:16:14,380
answers a more mundane question of how to deal with loss, how to deal with depression,

135
00:16:14,380 --> 00:16:21,900
how to deal with really the results of the intense attachment to craving that we have, people

136
00:16:21,900 --> 00:16:28,660
who feel low self-esteem because they can't get a romantic partner who feel like they're

137
00:16:28,660 --> 00:16:35,620
ugly, like they're unattractive, because of an intense attachment to the, just the idea

138
00:16:35,620 --> 00:16:44,420
of having sensual or romantic relations, people who get sick or who get overweight because

139
00:16:44,420 --> 00:16:52,980
of their attachment to food, people become depressed when things don't go their way or

140
00:16:52,980 --> 00:17:06,900
even get depressed when things do, who become addicted to drugs, addicted to all sorts

141
00:17:06,900 --> 00:17:17,660
of things, addicted to entertainment, and just never see it, never get a sense that something

142
00:17:17,660 --> 00:17:23,740
is wrong, but eventually you have to deal with it, so it's an answer to the question

143
00:17:23,740 --> 00:17:32,580
of the eventual loss when someone passes away, someone you love dies, excruciating loss,

144
00:17:32,580 --> 00:17:39,580
and we're so blind really, in general, that it's confusing to us, no, it's not even

145
00:17:39,580 --> 00:17:48,820
confusing to us, it's as though that's a normal part of life, it's become normal to feel

146
00:17:48,820 --> 00:17:55,460
to mourn when you lose something, as though it was normal to cling, and as though this

147
00:17:55,460 --> 00:18:02,420
is the best we can expect, so we get so caught up in our attachment, so lost in our

148
00:18:02,420 --> 00:18:08,140
attachments, that we don't even think to free ourselves from our attachments, instead

149
00:18:08,140 --> 00:18:13,380
we want something that frees us from the suffering, the problem is that that one leads

150
00:18:13,380 --> 00:18:19,860
to the other, you know, when you lose someone you love, well the real problem was that

151
00:18:19,860 --> 00:18:24,340
when you say you love them, you're actually saying you're attached to them, because love

152
00:18:24,340 --> 00:18:31,300
isn't something that makes you suffer, love is the intention to help others, it's the

153
00:18:31,300 --> 00:18:37,140
friendliness that we have, it's the desire for their welfare, you know, there's nothing

154
00:18:37,140 --> 00:18:45,180
to do with loss, loss and suffering caused by loss is because of possessiveness, the idea

155
00:18:45,180 --> 00:18:51,300
that it was ours, it was mine, so it has to do with ego and it has to do with attachment,

156
00:18:51,300 --> 00:18:57,820
but that's a very far removed state, if you're at a get to that point, which most of

157
00:18:57,820 --> 00:19:05,020
us do, it means we're already very much lost in the attachment, what we're talking about

158
00:19:05,020 --> 00:19:19,620
in this verse is the fight to not have the attachment in the first place, and the observation

159
00:19:19,620 --> 00:19:26,780
that it's so much more primal or primary, when you first want something, when you first

160
00:19:26,780 --> 00:19:32,660
like and it has to something, most of us don't see this, most of us are lost, get on,

161
00:19:32,660 --> 00:19:39,180
get, have to get it later on when we lose something that we love or when something goes

162
00:19:39,180 --> 00:19:51,020
wrong, when we can't get what we want, but the truth is that compared what this monk

163
00:19:51,020 --> 00:19:55,260
was seeing, and the reason he was seeing it was because he was meditating, is that compared

164
00:19:55,260 --> 00:20:02,980
to the peace of not wanting, wanting is something that inflames the mind, so one who seeks

165
00:20:02,980 --> 00:20:13,780
out, come, come, seeks out sensuality, will never be pleased, will never be happy, not

166
00:20:13,780 --> 00:20:25,860
de Ramesanti, not those who are gamma-gavicina, not those who seek out calm, seek out

167
00:20:25,860 --> 00:20:34,740
sensuality, they will not be at peace, not in the forest, they won't be at peace anywhere,

168
00:20:34,740 --> 00:20:41,340
but the key is that you see it when you have something better, when you have something

169
00:20:41,340 --> 00:20:43,900
better to compare it to.

170
00:20:43,900 --> 00:20:46,860
If you don't have something better to compare it to, you often think this is as good

171
00:20:46,860 --> 00:20:56,980
as it gets, this is the best I can expect, is this unsatisfying, fleeting moments of pleasure

172
00:20:56,980 --> 00:21:06,300
that punctuate our lives of toil and strife, where we have to work long hours, or we

173
00:21:06,300 --> 00:21:13,300
have to work, meaningless, we have to work in the world, we have to do things, spend

174
00:21:13,300 --> 00:21:28,140
our time just for these fleeting moments of pleasure, and so the key here is to see that

175
00:21:28,140 --> 00:21:34,700
there is something better, and to see that it's not, it's not as good as it gets, it's

176
00:21:34,700 --> 00:21:40,900
not the best, it's not true pleasure, it's not true happiness, the true happiness is

177
00:21:40,900 --> 00:21:47,940
beyond that, it's only for those who give it up, this is the claim, and this is the

178
00:21:47,940 --> 00:21:54,140
core of this verse, the core of the Buddha's teaching really, is that happiness is to be

179
00:21:54,140 --> 00:22:01,740
gotten through letting go, it's a claim that is backed by meditation, but it's the argument

180
00:22:01,740 --> 00:22:08,060
that we give, as to why people should practice meditation, you know, are you satisfied

181
00:22:08,060 --> 00:22:15,820
with your life, no, there is a way to go beyond this, if you can see how unsatisfying

182
00:22:15,820 --> 00:22:20,740
these things are, there is a path, there is a practice that gets you past them, gets

183
00:22:20,740 --> 00:22:26,300
you beyond them, it's very simple, it's not religious or not about believing in magical powers

184
00:22:26,300 --> 00:22:32,740
like the Buddha can read people's minds or project an image of himself across the cross

185
00:22:32,740 --> 00:22:43,180
space, it's not about that, it's about seeing the limitation of sensuality and the ultimate

186
00:22:43,180 --> 00:22:51,820
unsatisfactory nature, of sensual pleasures which are rather than satisfying their addictive

187
00:22:51,820 --> 00:22:57,500
and they lead to greater and greater states of want and need until eventually the want

188
00:22:57,500 --> 00:23:03,660
and the need comes crashing down, can't be fulfilled, and leads to suffering and in fact

189
00:23:03,660 --> 00:23:11,340
in and of itself is a disturbance, so this is what you really see in meditation, as you

190
00:23:11,340 --> 00:23:17,460
see it at a very primal level, that even any kind of touching the Buddha compared it

191
00:23:17,460 --> 00:23:23,220
to feces, you wouldn't even want to touch it, so now you can say, it's only a little

192
00:23:23,220 --> 00:23:28,540
bit of feces, so that's okay, the smallest bit of feces, it's not worth touching in the

193
00:23:28,540 --> 00:23:37,740
same way, even just the mere seeing of something or the mere reacting to something that

194
00:23:37,740 --> 00:23:45,060
you see rather, seeing this beautiful woman and then reacting to the reaction in and

195
00:23:45,060 --> 00:23:51,020
of itself, even before we decide whether you can get it or not get it, not having it,

196
00:23:51,020 --> 00:23:55,580
this is what leads us to want to get us, because not having it is suffering, here his body

197
00:23:55,580 --> 00:24:01,420
is shaking in what we would call anticipation, but it's actually in withdrawal, because he

198
00:24:01,420 --> 00:24:09,180
doesn't have it yet, the body is saying, get that, get that, get that, and that's what

199
00:24:09,180 --> 00:24:14,620
leads to suffering, that's what is suffering really, they're not having, and so then you

200
00:24:14,620 --> 00:24:18,980
would chase after and get it, and then you'd suffer, meaning you'd have to disrobe from

201
00:24:18,980 --> 00:24:25,060
being a monk, you'd have to get involved with many different things, but most importantly

202
00:24:25,060 --> 00:24:31,620
he chased after it, get it, get the pleasure, and then suffer when he didn't have it again,

203
00:24:31,620 --> 00:24:38,780
wanting it, so this relates, this relates directly to our meditation, this is here we have

204
00:24:38,780 --> 00:24:45,180
a story of a meditator, who had to deal with this, so the story goes that he was able

205
00:24:45,180 --> 00:24:48,860
to listen to hear what the Buddha was saying, and he was able to get what the Buddha

206
00:24:48,860 --> 00:24:55,660
was saying, and he was able to meditate, as I've talked about meditating on the aspects

207
00:24:55,660 --> 00:25:01,380
of desire, the aspects of attachment, so there is the desire that's a part of it, but

208
00:25:01,380 --> 00:25:07,060
there's much more involved, there's the physical sensations, this heat in the body or

209
00:25:07,060 --> 00:25:12,820
the tension in the body or whatever physical aspects there are, then there's the pleasure,

210
00:25:12,820 --> 00:25:16,900
which is fleeting, but it's there, when you want something that's often a pleasure associated

211
00:25:16,900 --> 00:25:22,740
with it, oh good, I'm going to get that, this sort of excitement will be there, then

212
00:25:22,740 --> 00:25:28,420
there's the image, the image itself that you see, you see this beautiful woman, beautiful

213
00:25:28,420 --> 00:25:35,860
man, beautiful thing, beautiful food, whatever it is, that's another part of it, and they

214
00:25:35,860 --> 00:25:41,700
work in a sequence, so you'll see the food, gives you pleasure, you want it, you chase,

215
00:25:41,700 --> 00:25:53,460
you give rise to an intention, wanting, chasing, getting, and then the cycle continues,

216
00:25:53,460 --> 00:25:58,820
so if you cut it off at any place, you catch all of these, break it into its parts, and

217
00:25:58,820 --> 00:26:04,140
catch what is really happening in that moment, and then that's all it is, because the

218
00:26:04,140 --> 00:26:13,100
enjoyment is not any one of those things, the cycle itself is not any one of those things,

219
00:26:13,100 --> 00:26:17,340
so when you focus only on one of them, you've broken the illusion, you've broken the

220
00:26:17,340 --> 00:26:27,180
delusion, you've pierced this misconception of attractiveness, there's nothing attractive

221
00:26:27,180 --> 00:26:31,580
about any one of the things, you see something, it's just light, there's nothing attractive,

222
00:26:31,580 --> 00:26:37,660
you recognize it, you want it, each one of those things is just an individual state,

223
00:26:37,660 --> 00:26:42,540
that's what we do in meditation, so we remind ourselves this is seeing, this is feeling,

224
00:26:42,540 --> 00:26:51,500
feeling, this is wanting, wanting, thinking, thinking, and so on, liking, liking, and if you do that,

225
00:26:51,500 --> 00:26:55,500
if you go back and forth between each and one of these things, you can overcome any addiction,

226
00:26:55,500 --> 00:27:01,580
you really can, again it's just about doing the work, it's not about living in the forest,

227
00:27:01,580 --> 00:27:08,540
obviously that doesn't work all the time, even if there's not women coming up to meet, up to

228
00:27:08,540 --> 00:27:16,780
seduce you, yeah, which does happen, but even if there's not, no matter what, there's only one

229
00:27:16,780 --> 00:27:24,780
who, only a person who can read that, read that Aga, who can free themselves from passion, give up

230
00:27:24,780 --> 00:27:30,460
the passions in the mind, only they will be happy, and actually this verse doesn't say it, but

231
00:27:30,460 --> 00:27:37,020
doesn't matter in the forest or anywhere, it's not about being in the forest, although there is

232
00:27:37,020 --> 00:27:41,740
something, the verse is also saying that the forest is actually harder to be in, there's no way

233
00:27:41,740 --> 00:27:47,820
anyone who is stuck on sensuality can be happy in the forest because there's nothing very sensual

234
00:27:47,820 --> 00:27:55,820
in the forest, there's no bright lights or pretty colors or beautiful attractive human beings

235
00:27:55,820 --> 00:28:02,140
or food or this or that, it's actually quite hard living in the forest as we talked about in the

236
00:28:02,140 --> 00:28:09,500
last verse, but it's delightful for those who are free from sensuality, why because it's peaceful,

237
00:28:09,500 --> 00:28:16,620
because you don't have people coming to bother you, because you are able to keep your mind

238
00:28:19,660 --> 00:28:27,900
at ease, at peace, anyway, that's the dhammapada for tonight, so we're quite a useful message for

239
00:28:27,900 --> 00:28:43,820
all of us, thank you all for tuning in, I wish you all good practice, and be well.

