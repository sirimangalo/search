Next question from Charlie Lee 24, sometimes in my meditation I notice a feeling of itch or something and if I try to investigate it saying to myself itchy itchy it immediately goes away.
When I return to the main object it comes back that bothers me a little, what should I do?
Yeah, welcome to the world of meditation. Life has a rhythm to it and our problem is we're out of rhythm with life.
Our inability to accept reality, that's what it means. It means we're not in tune, we're, we don't jive, we don't harmonize with the way things are.
We can't accept reality. We generally think that we have some kind of control over reality and this is why we don't accept.
We live our lives thinking that we're in control and that the objects of the sense belong to us and so we go about our lives trying to influence things.
Now there is a process of cause and effect by which the mind is able to, on a microscopic scale, change things and so we get this idea that we're able to do that on a macroscopic level that we're able to say no more itching.
We're able to say no more pain or so on. We're able to get the things that we want. But the reality of it is we're only able to move in a certain direction.
We can build up to the point where there is no itching. You can develop yourself in a way that you're able to put off itching for some time.
On the microscopic level you can see that when you say itching it goes away for a second. But that's a microscopic, they call it a process one intervention and quantum physics.
So one of the things I'm reading about. But it's so miniscule that all you've done is put a decide for a second.
Once you come back to the main object, it's come back. Now the point is not to build up some kind of state where in you're no longer itching.
Because that in and of itself is still temporary. It has a certain power that you've built up in the brain to shut these things off.
And it doesn't last because there's so many other factors, physical factors involved that it's unsustainable.
The point is to stop living our lives in this way and stop trying to control things and to get ourselves to the point where we're simply observing.
The problem is not the itching, the problem is the bothering. What you're seeing is the way things work. Reality is not under your control.
What you're doing there is you're turning off the itching for a moment. You're altering the function of the brain so it doesn't experience the itch.
And then turning that functioning off by going back to the rising and the following, the itch comes back. You go back, the itch goes, you come back, the itch comes back.
And that's reality. That's the way things work. That's how the mind works. All you could do is force the itch away for a long time.
Force the unpleasant situation away by building up these mind-states and these interventions.
That's not an ultimate solution. The ultimate solution is to accept and to say, okay, now I'm itching.
And if it goes, it comes. Practicing pretty much exactly the way you're doing it.
And changing the way you react to it. Because you're practicing correctly. When you practice this way, you start to see your anger.
You start to see your frustration, your inability to accept the way reality works or the way things are.
As you see it again and again and again, and as you straighten out your mind, acknowledging as well frustrated, frustrated, angry, angry, upset, upset, you start to see that you're doing something that's creating suffering for yourself.
You're reacting in a way that's unpleasant. Now no one wants to create suffering for themselves. And this is very true, even on an ultimate level of the brain that our minds don't want to create suffering for ourselves.
Because we don't see things clearly. Once you show yourself again and again and again and prove to yourself that what you're doing is directly creating suffering for yourself by getting angry, by getting upset, you won't get upset anymore.
Until eventually it's itching. It goes away. Come back rising. It comes back. You go back and say it's itching. And there's no frustration. There's no upset. You become in tune with reality and you're in tune with the rhythm.
So it's like itching, itching, rising, falling. And it's not rhythmic. It's not like that. But as a course example, there is...
Well, you have to think of it like a rhythm. That's the way it is. That's natural. In the same way that a rhythm is natural.
And though it's not rhythmic, it is what it is. And that's reality. And you have two choices. You can always be trying to change it to be in some specific set way. This is the way it shouldn't be in no other way.
Or you can accept everything for what it is. The path of Buddhism is to accept things as they are. So keep going. Your practice is fine. The problem is the frustration.
And that will go as you acknowledge the object and as you acknowledge the frustration, saying to yourself, frustrated, frustrated, and itching.
Your mind will ease up and you'll be able to see it just for one again. Thanks for the question.
