1
00:00:00,000 --> 00:00:09,000
I think a very important question, you have any experience or advice that you can share with people who suffer from depression.

2
00:00:09,000 --> 00:00:16,000
A breath, meditation and mindfulness helped me to be more balanced.

3
00:00:16,000 --> 00:00:20,000
How do I progress further from here?

4
00:00:20,000 --> 00:00:26,000
Start looking at the depression.

5
00:00:26,000 --> 00:00:34,000
You only suffer from depression because you cling to it because you see it as a problem.

6
00:00:34,000 --> 00:00:42,000
If you see it as a condition, as an experience and moreover, if you see it as something that you can learn from,

7
00:00:42,000 --> 00:00:44,000
then you find that it's not really that scary.

8
00:00:44,000 --> 00:00:46,000
Don't try to balance yourself.

9
00:00:46,000 --> 00:00:49,000
Learn about your imbalance.

10
00:00:49,000 --> 00:00:54,000
But that's great. I mean, breath meditation, mindfulness will help to balance you.

11
00:00:54,000 --> 00:00:57,000
Learning about it will help to balance you.

12
00:00:57,000 --> 00:00:59,000
Just don't be afraid of it.

13
00:00:59,000 --> 00:01:00,000
Yeah.

14
00:01:00,000 --> 00:01:07,000
The fact that you're asking the question, this is hard because you're not here with me meditating.

15
00:01:07,000 --> 00:01:15,000
So all these questions, specific questions, it's hard to give to know if you're giving the exact advice that the person needs.

16
00:01:15,000 --> 00:01:22,000
When you seem to feel like you've hit a block, you say, okay, it's helped me.

17
00:01:22,000 --> 00:01:24,000
But what's next?

18
00:01:24,000 --> 00:01:29,000
And that's kind of, you know, the idea is what I get from that is that you're still depressed, right?

19
00:01:29,000 --> 00:01:31,000
You're better able to deal with it, but you're still depressed.

20
00:01:31,000 --> 00:01:35,000
So what's the next step that's really going to help you get rid of your depression?

21
00:01:35,000 --> 00:01:39,000
And that shows that you're kind of missing.

22
00:01:39,000 --> 00:01:51,000
You may be kind of missing the point and the point is not to strive for balance, not to strive for escape from the depression, but to learn about it.

23
00:01:51,000 --> 00:01:59,000
Because you wouldn't ask the question, what's next or how do I progress further if all of your problems were gone, right?

24
00:01:59,000 --> 00:02:05,000
So if you still have problems, then the only thing I say can suggest is to look at them.

25
00:02:05,000 --> 00:02:10,000
Stop being afraid of them. Stop trying to get away from them. Start learning about them.

26
00:02:10,000 --> 00:02:19,000
Start examining them. Think of it as a laboratory as your playground, your school.

27
00:02:19,000 --> 00:02:24,000
The playground is a bad word.

28
00:02:24,000 --> 00:02:43,000
You touched on this question before.

