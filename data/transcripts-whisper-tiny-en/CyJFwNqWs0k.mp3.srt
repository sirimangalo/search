1
00:00:00,000 --> 00:00:23,840
Good evening everyone, welcome to evening dumber.

2
00:00:23,840 --> 00:00:36,840
Good night we're looking at the Sabasa-Sutta, Majiminikaya, number two.

3
00:00:36,840 --> 00:00:45,440
We're looking at, we know the na-bhahatava.

4
00:00:45,440 --> 00:00:55,640
Again we're dealing with many different aspects of eradicating defilements.

5
00:00:55,640 --> 00:01:02,440
And again by defilements we mean it in a very technical sense, a defilement that makes something

6
00:01:02,440 --> 00:01:14,440
impure and in the case of the mind it's that which leads to stress and suffering.

7
00:01:14,440 --> 00:01:25,640
Those aspects of the mind that are of no use to us and are in fact to our detriment.

8
00:01:25,640 --> 00:01:29,280
They sell the mind, they taint the mind.

9
00:01:29,280 --> 00:01:43,120
They would cause us to get caught up in addiction and diversion and stress and suffering.

10
00:01:43,120 --> 00:01:53,480
So if you recall the most important practice for doing away with defilements of course is

11
00:01:53,480 --> 00:01:59,880
seeing, it's the first one of the seven.

12
00:01:59,880 --> 00:02:05,760
And so we talk about how mindfulness is all about learning to see clearly.

13
00:02:05,760 --> 00:02:12,840
Why are we mindful, really to see clearly, to see things as they are and this is what we call

14
00:02:12,840 --> 00:02:15,160
wipasana.

15
00:02:15,160 --> 00:02:20,360
When you're mindful you start to learn and understand yourself, but there's actually two

16
00:02:20,360 --> 00:02:29,720
aspects to the practice that really go hand in hand and are equally important.

17
00:02:29,720 --> 00:02:35,160
And it's one way of looking at what we call, we know the section we're looking at today.

18
00:02:35,160 --> 00:02:40,440
We know the means to dispel.

19
00:02:40,440 --> 00:02:48,160
And so the question is, why don't we see clearly anyway, why don't we see clearly normally?

20
00:02:48,160 --> 00:02:54,760
Why do we need to do this special practice to see clearly?

21
00:02:54,760 --> 00:03:04,520
And it's because we have darkness, we have clouds, we have defilements covering our mind.

22
00:03:04,520 --> 00:03:10,000
So our final goal is to be free from defilements because they're what causes suffering.

23
00:03:10,000 --> 00:03:14,880
And in order to be free from defilements, the funny thing is we actually have to be free

24
00:03:14,880 --> 00:03:19,440
from defilements.

25
00:03:19,440 --> 00:03:24,240
It's important, I mean, it's useful to understand this, what this means.

26
00:03:24,240 --> 00:03:30,360
When we talk about being free from defilements in the sense of our goal, we're not actually

27
00:03:30,360 --> 00:03:38,240
talking about right here and now being free from defilements in that comes anyway, right?

28
00:03:38,240 --> 00:03:43,000
From time to time, we're not angry, we're not greedy, there are times where our minds

29
00:03:43,000 --> 00:03:44,800
are quite pure.

30
00:03:44,800 --> 00:03:54,760
Now, what we're talking about is, in fact, a brooding of the causes of defilements, mainly

31
00:03:54,760 --> 00:03:56,720
ignorance.

32
00:03:56,720 --> 00:04:03,200
Once you see clearly, then the defilements don't come back.

33
00:04:03,200 --> 00:04:09,680
For example, if there's something that you dislike, by understanding that thing that you dislike

34
00:04:09,680 --> 00:04:14,960
and understanding that disliking, you come to see that the thing that you dislike wasn't

35
00:04:14,960 --> 00:04:17,160
worth disliking.

36
00:04:17,160 --> 00:04:23,240
And the dislike had no benefit or purpose and was, in fact, harmful to you.

37
00:04:23,240 --> 00:04:26,600
When you see that, then you're disinclined with it.

38
00:04:26,600 --> 00:04:32,680
But in order to see that, you have to be free, your mind has to be free from defilements.

39
00:04:32,680 --> 00:04:35,360
And so really, that's a lot about what this suit does about it.

40
00:04:35,360 --> 00:04:45,200
It's about the different ways in which we support our minds to see clearly.

41
00:04:45,200 --> 00:05:00,800
We cultivate and we protect our minds and encourage our minds to see more clearly.

42
00:05:00,800 --> 00:05:06,560
And so a big part of mindfulness is not just about seeing clearly, but as, in fact, about

43
00:05:06,560 --> 00:05:12,240
purifying the mind momentarily, to be called momentary concentration.

44
00:05:12,240 --> 00:05:17,200
So there's a lot of talk and Buddhism about entering into the Johnus, and Buddha talked

45
00:05:17,200 --> 00:05:25,440
a lot about these, and he explained it as momentary or temporary cessation of defilements.

46
00:05:25,440 --> 00:05:37,000
It's a pure state, a Johnus, a state that is free from any kind of desire or aversion.

47
00:05:37,000 --> 00:05:43,160
And so that's what we're doing when we're being mindful, we're cultivating this momentarily.

48
00:05:43,160 --> 00:05:45,240
And this is what allows us to see clearly.

49
00:05:45,240 --> 00:05:53,200
Mindfulness is, in fact, a form of abandoning unwholesome mind states.

50
00:05:53,200 --> 00:05:59,120
So for example, when you see something and normally that seeing, you see a plate of food

51
00:05:59,120 --> 00:06:07,120
and normally that would lead to a craving for the food or a desire for the food.

52
00:06:07,120 --> 00:06:15,600
When you say to yourself seeing, seeing you abandon that, you dispel that.

53
00:06:15,600 --> 00:06:21,760
When you want something and you say to yourself wanting, wanting you dispel the wanting,

54
00:06:21,760 --> 00:06:24,320
that's a form of cleansing the mind.

55
00:06:24,320 --> 00:06:30,480
But I think what this section is talking about is something a little different that is also

56
00:06:30,480 --> 00:06:31,480
quite useful.

57
00:06:31,480 --> 00:06:37,440
And remember these later sections are, except for the last one, then we'll do next time,

58
00:06:37,440 --> 00:06:45,960
are mainly about auxiliary practices, ancillary practices, practices that are supportive.

59
00:06:45,960 --> 00:06:52,600
And so with, we know it and I hear we're talking more about, and the commentary reaffirms

60
00:06:52,600 --> 00:07:03,200
us, we're talking about intellectually or reflect, or flexibly reflecting on your experiences

61
00:07:03,200 --> 00:07:09,200
and making a determined effort to abandon.

62
00:07:09,200 --> 00:07:14,120
When you're meditating, it's easy to say be mindful, but there's a lot of things that

63
00:07:14,120 --> 00:07:19,680
aren't quite strong and just tear you away from the practice.

64
00:07:19,680 --> 00:07:27,920
Maybe you get caught up in the past or the future, it's very common, you start obsessing

65
00:07:27,920 --> 00:07:34,320
over something you did or something that happened to you in the past, grieving about the

66
00:07:34,320 --> 00:07:40,200
past or hating yourself, angry at yourself because of the past, that kind of feeling

67
00:07:40,200 --> 00:07:47,040
guilty about the past, or worried about the future, worry, fear, ambition about the future,

68
00:07:47,040 --> 00:07:53,680
you can make plans, a ten-year plan while you're sitting in meditation, or maybe even

69
00:07:53,680 --> 00:08:03,760
just daydreams thinking up stories, telling yourself stories or fantasies in your mind.

70
00:08:03,760 --> 00:08:12,880
And these things are pernicious or are stubborn, and so we know denies is this sort of

71
00:08:12,880 --> 00:08:13,880
thing.

72
00:08:13,880 --> 00:08:18,800
It's of dealing with this and saying to yourself, look, this is useless, this is not

73
00:08:18,800 --> 00:08:24,280
to my benefit, these things are a cause for stress and they're distracting me from my

74
00:08:24,280 --> 00:08:26,320
practice.

75
00:08:26,320 --> 00:08:32,520
There are three kinds of thoughts that we have to do this with.

76
00:08:32,520 --> 00:08:43,000
So the Pauli says, it's a bikhu, patisankayon is so reflecting wisely, upa nang kam wita kang,

77
00:08:43,000 --> 00:08:56,520
and a risen thought relating to sensuality, nadhi wa saithi pajahati vinodeti, bianthi karoti

78
00:08:56,520 --> 00:09:04,240
a nambavangameti, it's nice to read all that, it's actually, the commentary does a really

79
00:09:04,240 --> 00:09:09,960
good job explaining all these synonyms, really, they're all saying the same thing over and

80
00:09:09,960 --> 00:09:16,800
over again, nadhi wa saithi, adhi wa saithi is referring to an earlier section on being

81
00:09:16,800 --> 00:09:26,960
patient with bearing with, so these things one doesn't bear with, one doesn't endure these

82
00:09:26,960 --> 00:09:27,960
kind of thoughts.

83
00:09:27,960 --> 00:09:35,600
If one is caught up in sensuality thinking about sights and sounds and smells and tastes

84
00:09:35,600 --> 00:09:43,320
and feelings and thoughts, sensual experiences that are pleasant, one gets caught up in

85
00:09:43,320 --> 00:09:49,960
those, one vanishes them, one doesn't endure that thought and let it continue, right?

86
00:09:49,960 --> 00:09:59,920
It doesn't get caught up in the thought, pajahati, one abandons it, right, yeah, abandons.

87
00:09:59,920 --> 00:10:12,400
We know deity, dispels it, bianthi karoti makes it go away, nambavangameti, causes it to

88
00:10:12,400 --> 00:10:25,680
go to oblivion, basically saying the same thing and there's different ways, it's basically

89
00:10:25,680 --> 00:10:33,040
talking about reassuring yourself or giving yourself a talk, pep talk and say, look,

90
00:10:33,040 --> 00:10:41,120
these thoughts don't go there, it's making a decision, really, they have come here to

91
00:10:41,120 --> 00:10:50,000
meditate or I'm, what am I doing as a human being, I've been born as a human being, I should

92
00:10:50,000 --> 00:10:53,760
make the, I'll take this opportunity that I've come in contact with the Buddhist teaching

93
00:10:53,760 --> 00:11:05,760
and I have my health to keep my mind focused on reality.

94
00:11:05,760 --> 00:11:13,520
So with, with sensuality and the second one is with ill will or anger and the thoughts

95
00:11:13,520 --> 00:11:25,480
of anger when we are upset at someone or thinking about enemy and anonymity and revenge

96
00:11:25,480 --> 00:11:35,680
and that kind of thing and the third one is we hings wanting to harm others or I don't

97
00:11:35,680 --> 00:11:42,920
really know, they always get confused about the difference between those two but basically

98
00:11:42,920 --> 00:11:51,440
anything to do with greed, anger and delusion, any kind of bad intention in the mind and

99
00:11:51,440 --> 00:11:59,160
that's right, that's what he says at the end up, panupane, papake, acusle, dame, a risen evil

100
00:11:59,160 --> 00:12:18,160
unwholesome dummers, beautiful unwholesome dummers, they don't sound all that bad, the things

101
00:12:18,160 --> 00:12:23,680
I've been talking about, for most, most people I often get comments, people say, I

102
00:12:23,680 --> 00:12:33,040
don't understand what's so wrong with this entertainment and this thinking and remembering

103
00:12:33,040 --> 00:12:43,720
and planning and so on, what's so wrong with it, we have to remember what we're talking

104
00:12:43,720 --> 00:12:50,160
about here, this practice may not appeal to everyone, I mean it certainly doesn't appeal

105
00:12:50,160 --> 00:13:01,760
to everyone, it does make a claim to be the best course of action for everyone but certainly

106
00:13:01,760 --> 00:13:09,480
many people will not see that and will not be interested in it in that way but what we're

107
00:13:09,480 --> 00:13:18,480
talking about here is the purification of the mind, we are talking about uprooting in

108
00:13:18,480 --> 00:13:28,560
the sense of it, never coming back again, things like addiction, aversion, any kind of anger,

109
00:13:28,560 --> 00:13:38,840
frustration, they never come back, that's what we're aiming for, any kind of lust, passion,

110
00:13:38,840 --> 00:13:47,440
ambition, suffering, any kind of suffering, there will be no more suffering in the mind

111
00:13:47,440 --> 00:13:58,480
and it's really something quite lofty, profound, lofty anyway, it's a lofty goal and it

112
00:13:58,480 --> 00:14:04,320
may not be to everyone's taste and I think it's important to align those two, if you don't

113
00:14:04,320 --> 00:14:15,760
see the problem with fantasizing or getting caught up in thoughts of anger or revenge

114
00:14:15,760 --> 00:14:21,920
or so on, well then maybe it's because you're not really interested, you're not interested

115
00:14:21,920 --> 00:14:31,760
in the goal of Buddhism, I mean what we're talking about is not becoming a normal human

116
00:14:31,760 --> 00:14:37,120
being, this isn't just something that's meant to take you out of extreme state-state

117
00:14:37,120 --> 00:14:44,320
so suffering and allow you to live your life normally, this is about becoming a noble individual,

118
00:14:44,320 --> 00:14:54,320
really taking you out of ordinary humanity, the ups and the downs of life, it's rising

119
00:14:54,320 --> 00:15:04,600
above the roller coasters of pleasure and pain or not pleasure and pain but pleased

120
00:15:04,600 --> 00:15:16,600
and being pleased and displeased, being happy and unhappy, there'll be no more happy and unhappy,

121
00:15:16,600 --> 00:15:23,000
just be happy really, I mean in the sense that we know it but there will be always happiness

122
00:15:23,000 --> 00:15:33,360
in the sense of freedom from suffering, in the sense of peace in the mind, we're aiming

123
00:15:33,360 --> 00:15:43,560
for this peace for this most sublime state, so for this section what we're dealing with

124
00:15:43,560 --> 00:15:55,200
here is making a decision, one of my here for and a decision that these states are a waste

125
00:15:55,200 --> 00:16:01,400
of time and pulling yourself back to the present moment and then of course dealing with

126
00:16:01,400 --> 00:16:17,320
them mindfully, a thought is just a thought and of course in a deep sense there's no need

127
00:16:17,320 --> 00:16:25,200
for the intellectualization because you see that a thought is just a thought and experience

128
00:16:25,200 --> 00:16:38,240
is just an experience and you see that there is no inherent attractiveness or repulsiveness

129
00:16:38,240 --> 00:16:48,080
to any experience and that every experience is neutral, it is what it is and we've simply

130
00:16:48,080 --> 00:17:02,640
developed excitement, different kinds of excitement that that causes us to hurt ourselves

131
00:17:02,640 --> 00:17:09,840
and experiences don't actually cause us stress or suffering, it's our reactions to

132
00:17:09,840 --> 00:17:20,000
them, it's our states of excitement and allowing ourselves to make more out of things than

133
00:17:20,000 --> 00:17:28,960
they actually are, I mean all of this kind of teaching and reminding yourself of these

134
00:17:28,960 --> 00:17:33,760
sorts of concepts is really what the Buddha is talking about, the sort of thing he's talking

135
00:17:33,760 --> 00:17:41,400
about here I think, talking about telling yourself and teaching yourself and pointing

136
00:17:41,400 --> 00:17:49,600
out, yeah this is not useful, being able to decide this path that I'm going down in my

137
00:17:49,600 --> 00:17:57,200
mind, this way that I'm focusing my mind, this way of acting, way of behaving mentally

138
00:17:57,200 --> 00:18:05,360
is wrong, wrong for me, it doesn't bring me happiness, it doesn't bring me peace, it doesn't

139
00:18:05,360 --> 00:18:14,480
make me a better person for myself or others in any way until you abandon it and switch

140
00:18:14,480 --> 00:18:21,080
to being mindful instead, instead of focusing on the content of thought and getting caught

141
00:18:21,080 --> 00:18:28,520
up in it in any way, a thought is just a thought, I mean it's very important Buddhist

142
00:18:28,520 --> 00:18:34,040
theory, Buddhist concept if you want to understand this, what the Buddha taught or how the

143
00:18:34,040 --> 00:18:40,560
Buddha taught or the Buddha's exhortation, it was that seeing should just be seeing, hearing

144
00:18:40,560 --> 00:18:47,400
should just be hearing, sensing should be sensing, thinking should be thinking, no more

145
00:18:47,400 --> 00:18:55,160
no less, and if we look at reality in this way, we'll see some very novel, we'll gain some

146
00:18:55,160 --> 00:19:05,360
novel realizations that allow us to free our minds, really truly free our minds and feel

147
00:19:05,360 --> 00:19:13,720
the freedom, purity.

148
00:19:13,720 --> 00:19:20,240
So one more section, the next time we'll have the last section, and maybe we'll go back

149
00:19:20,240 --> 00:19:33,560
to the Dhamapada, that's all for tonight, we'll take some questions, how can someone

150
00:19:33,560 --> 00:19:37,920
know if she is escaping her life or if she is really following your inner call to become

151
00:19:37,920 --> 00:19:52,360
a nun, came up without reading the long paragraph that you wrote, it's interesting to

152
00:19:52,360 --> 00:20:05,360
read, I think, I think if you're in tension, I mean the best, there are many reasons to

153
00:20:05,360 --> 00:20:10,200
our day, and people are ordained for all sorts of reasons, and it makes for a very colorful

154
00:20:10,200 --> 00:20:17,480
community, but if you really want to ordain for the right reasons, it should be because

155
00:20:17,480 --> 00:20:22,160
you want to practice meditation, that and only that.

156
00:20:22,160 --> 00:20:28,240
If you have some other romantic ideas about living the monastic life or living in the forest

157
00:20:28,240 --> 00:20:35,320
or that kind of thing, it's problematic and it will be a problem for you and your community

158
00:20:35,320 --> 00:20:45,800
when it leads to communities that are often off-base, so you have to ask yourself why

159
00:20:45,800 --> 00:20:51,520
you're doing it and be clear about why you're doing it, that being said, there are people

160
00:20:51,520 --> 00:21:02,360
who are dain for mundane reasons and end up going on to becoming enlightened, it's not

161
00:21:02,360 --> 00:21:04,480
something you should worry too much about.

162
00:21:04,480 --> 00:21:09,180
If you'd like to ordain, well, just make sure you have good intentions and you're not

163
00:21:09,180 --> 00:21:17,520
ordaining for bad reasons, and really it's a good thing to do if you can find a place.

164
00:21:17,520 --> 00:21:23,720
Big problem is nowadays, in modern times it's not easy to find a great place to ordain,

165
00:21:23,720 --> 00:21:32,120
of course it's not easy to get to one of the places, good places that do exist.

166
00:21:32,120 --> 00:21:37,960
When noting anger, the thoughts and burning sensations get more vivid as raises fear would

167
00:21:37,960 --> 00:21:44,800
set the protect us in this situation, yeah, I mean you'd note the fear, really that's

168
00:21:44,800 --> 00:21:49,440
what it's all about, it's about vividly experiencing these things, the thoughts and the

169
00:21:49,440 --> 00:21:55,800
burning sensations, they're not the problem, the problem is your reactions, your fear

170
00:21:55,800 --> 00:22:04,280
and so on, and so do that, go through that to the point where you see that, and you start

171
00:22:04,280 --> 00:22:11,200
to become resilient so that these thoughts and burning sensations aren't, don't have power

172
00:22:11,200 --> 00:22:22,680
over you, trying to sit in full lotus position, I asked them because I'd learn more

173
00:22:22,680 --> 00:22:27,800
of my practices challenging, but it's irrelevant to have this kind of goal, okay, you learn

174
00:22:27,800 --> 00:22:36,640
more if your practice is challenging, this isn't really I think a great attitude because

175
00:22:36,640 --> 00:22:42,000
it's understandable, but it's another one of these, I would caution that this is probably

176
00:22:42,000 --> 00:22:51,680
another one of these, let's make it quicker, kind of states, let's do something, find

177
00:22:51,680 --> 00:22:57,600
a trick to improve my practice, and there are no tricks to improving your practice, trying

178
00:22:57,600 --> 00:23:03,200
to find a trick is in and of itself going to hinder your practice, you should practice

179
00:23:03,200 --> 00:23:11,280
some mundane old practice, sit with your legs, one leg in front of the other and just

180
00:23:11,280 --> 00:23:18,280
slog through it without trying to find any trick, the full lotus position is arguably

181
00:23:18,280 --> 00:23:24,760
better for some type of practice, I would argue that it's not better for we pass in a

182
00:23:24,760 --> 00:23:31,640
practice for a couple of reasons, for this reason because it's just a trick, it's not

183
00:23:31,640 --> 00:23:39,920
inside doesn't have anything to do with that, and because since you don't have these deep

184
00:23:39,920 --> 00:23:49,080
powerful states of concentration, it's going to aggravate, it potentially causes aggravation

185
00:23:49,080 --> 00:23:55,800
in your joints, if you're in the full lotus, it's not to say you shouldn't, if you

186
00:23:55,800 --> 00:24:02,400
want to sit in the full lotus, I'm not going to tell you, no that's bad, but I would caution

187
00:24:02,400 --> 00:24:10,640
against it generally, especially if it's just because you want to challenge yourself,

188
00:24:10,640 --> 00:24:15,440
challenge yourself by not challenging yourself.

189
00:24:15,440 --> 00:24:24,400
Does this recent eclipse have any meaning, I think it means that the moon came between

190
00:24:24,400 --> 00:24:32,080
the earth and the sun, I'm not sure about that, it could also mean that Rahu has eaten

191
00:24:32,080 --> 00:24:42,520
the, eats the sun, swallows the sun for a period of time, Rahu is a demon or God, you

192
00:24:42,520 --> 00:24:52,320
pick, dear Bante is Dukkha and suffering the same, well suffering is an English word,

193
00:24:52,320 --> 00:24:58,760
and I'm not being facetious by saying that because Dukkha is translated in different

194
00:24:58,760 --> 00:25:05,400
ways, but generally the answer I would give is yes, but it's hard to say, it depends

195
00:25:05,400 --> 00:25:11,200
what you mean by suffering, and maybe depends what you mean by Dukkha as well, but generally

196
00:25:11,200 --> 00:25:17,080
that's Dukkha is translated as suffering, does the term Buddha nature also play a role in

197
00:25:17,080 --> 00:25:31,280
Teravada, not really, not really, in my opinion, I don't think it does.

198
00:25:31,280 --> 00:25:55,920
So that's all the questions, thank you all for tuning in, have a good night.

