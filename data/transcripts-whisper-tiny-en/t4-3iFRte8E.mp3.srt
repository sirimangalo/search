1
00:00:00,000 --> 00:00:15,000
If you are constantly noting, thinking, thinking, value and innovation, how will you be able to form my ears for my ears?

2
00:00:15,000 --> 00:00:19,000
That's exactly the point.

3
00:00:19,000 --> 00:00:28,000
Now, we don't want to form ideas and bringing it into your mind, because that makes it so much easier.

4
00:00:28,000 --> 00:00:30,000
And here's out the mind.

5
00:00:30,000 --> 00:00:34,000
Then there will be only that, what is necessary.

6
00:00:34,000 --> 00:00:37,000
That, what is going on in the present moment.

7
00:00:37,000 --> 00:00:39,000
Only.

8
00:00:39,000 --> 00:00:47,000
The same thinking, maybe to be bringing what I think is actually, for exactly this,

9
00:00:47,000 --> 00:01:00,000
we may call these ideas that are popping up and these plans that we're making to come to hold, to stop.

10
00:01:00,000 --> 00:01:13,000
And because they are more bothering than helping, I remember as well in that first retreat that I did.

11
00:01:13,000 --> 00:01:16,000
Then I struggled a lot with it.

12
00:01:16,000 --> 00:01:20,000
And I was kind of predicted to my thoughts.

13
00:01:20,000 --> 00:01:22,000
I wanted to bring it.

14
00:01:22,000 --> 00:01:27,000
And it was very important for me to know what I think about things.

15
00:01:27,000 --> 00:01:30,000
I wanted to have my opinions.

16
00:01:30,000 --> 00:01:37,000
And it is actually not so important to have opinions about things.

17
00:01:37,000 --> 00:01:43,000
Or to have ideas.

18
00:01:43,000 --> 00:01:54,000
When you are in the present moment, then the idea comes in the moment that's necessary.

19
00:01:54,000 --> 00:01:57,000
Then the idea will be there when you have it.

20
00:01:57,000 --> 00:02:07,000
And when you don't need it, your mind is not part of it with producing ideas and fantasies and daydreams and whatsoever.

21
00:02:07,000 --> 00:02:17,000
So once you get over that aversion to let go of your thinking,

22
00:02:17,000 --> 00:02:23,000
and when you understand that your thinking is actually not very, very important,

23
00:02:23,000 --> 00:02:31,000
then it becomes more quiet and more peaceful in your mind,

24
00:02:31,000 --> 00:02:39,000
and you can develop ideas when you know them.

25
00:02:39,000 --> 00:02:54,000
I think the mic was turned off when I spoke.

26
00:02:54,000 --> 00:03:00,000
Did anybody hear what I just said?

27
00:03:00,000 --> 00:03:10,000
Well, they heard you through my mic, so maybe we can leave it.

28
00:03:10,000 --> 00:03:13,000
You want to say it well again?

29
00:03:13,000 --> 00:03:14,000
No, no, no.

30
00:03:14,000 --> 00:03:15,000
You just make a short...

31
00:03:15,000 --> 00:03:18,000
I'll see if I can edit the audio there.

32
00:03:18,000 --> 00:03:19,000
Yeah, they heard.

33
00:03:19,000 --> 00:03:25,000
So I can probably adjust the audio later.

34
00:03:25,000 --> 00:03:29,000
I've missed forgotten the question.

35
00:03:29,000 --> 00:03:35,000
It comes down to many questions like this.

36
00:03:35,000 --> 00:03:38,000
It comes down to an understanding of what's important.

37
00:03:38,000 --> 00:03:42,000
Another good example is the video on physical exercise,

38
00:03:42,000 --> 00:03:47,000
which got probably among the most down votes of any of my videos,

39
00:03:47,000 --> 00:03:49,000
which I'm not upset about at all.

40
00:03:49,000 --> 00:03:50,000
I think it's kind of funny.

41
00:03:50,000 --> 00:03:55,000
But it's kind of disturbing that people don't get that...

42
00:03:55,000 --> 00:03:59,000
This is a guy living off in the forest eating one meal a day,

43
00:03:59,000 --> 00:04:05,000
practicing to become free from Samsara.

44
00:04:05,000 --> 00:04:09,000
Our take on the universe, and I hope I can speak for all...

45
00:04:09,000 --> 00:04:13,000
I assume I speak for all of us when I say this,

46
00:04:13,000 --> 00:04:24,000
our take on the universe is far different from the way that most people will look at the world.

47
00:04:24,000 --> 00:04:27,000
Most are people in society.

48
00:04:27,000 --> 00:04:34,000
We're not concerned about building up society or building up government

49
00:04:34,000 --> 00:04:39,000
or building up the world or creating or progressing

50
00:04:39,000 --> 00:04:44,000
or at technological advancement or any of these things.

51
00:04:44,000 --> 00:04:48,000
We're not even really interested in being born a human again.

52
00:04:48,000 --> 00:04:50,000
We're not interested in the fate of humanity.

53
00:04:50,000 --> 00:04:54,000
We're not interested in the fate of the human race.

54
00:04:54,000 --> 00:05:00,000
Not really interested in the fact that the earth is eventually going to burn up into the sun.

55
00:05:00,000 --> 00:05:05,000
We're not thinking, oh, we have to go and colonize other planets or so on.

56
00:05:05,000 --> 00:05:12,000
Our understanding or the Buddhist understanding of life

57
00:05:12,000 --> 00:05:17,000
tries to encompass the whole universe and encompass reality as a whole.

58
00:05:17,000 --> 00:05:26,000
We have no need to develop ideas or views or opinions about anything.

59
00:05:26,000 --> 00:05:31,000
We know that there's this whole question of the...

60
00:05:31,000 --> 00:05:34,000
We give up sexual activity.

61
00:05:34,000 --> 00:05:37,000
That's going against nature and what would happen to the human race?

62
00:05:37,000 --> 00:05:39,000
Not really of interest.

63
00:05:39,000 --> 00:05:41,000
2012.

64
00:05:41,000 --> 00:05:49,000
Maybe what our planet is going to collide with another planet and just mysteriously appears at a nowhere.

65
00:05:49,000 --> 00:05:51,000
It's not really of interest.

66
00:05:51,000 --> 00:05:55,000
It's of interest in the sense that, yeah, anything could happen and I could die tomorrow.

67
00:05:55,000 --> 00:05:57,000
It's an important thing to know.

68
00:05:57,000 --> 00:06:01,000
What should I do? Keep meditating. Start meditating harder.

69
00:06:01,000 --> 00:06:05,000
But beyond that, there's no interest.

70
00:06:05,000 --> 00:06:17,000
Our answers and our encouragement will often be at odds with people's idea of what's right.

71
00:06:17,000 --> 00:06:19,000
Don't exercise.

72
00:06:19,000 --> 00:06:21,000
What good is exercise to you?

73
00:06:21,000 --> 00:06:23,000
What good is exercise?

74
00:06:23,000 --> 00:06:25,000
Whatever.

75
00:06:25,000 --> 00:06:27,000
Even don't practice yoga.

76
00:06:27,000 --> 00:06:28,000
Don't practice yoga.

77
00:06:28,000 --> 00:06:30,000
How narrow minded of your son.

78
00:06:30,000 --> 00:06:36,000
Well, obviously you can exercise, you can practice yoga, you can think, you can develop ideas,

79
00:06:36,000 --> 00:06:38,000
depending on your goals.

80
00:06:38,000 --> 00:06:44,000
If it's your goal to get whatever the result of having lots of ideas and opinions is,

81
00:06:44,000 --> 00:06:46,000
then go for it.

82
00:06:46,000 --> 00:06:51,000
That power to you, but it's not our path and it's not going to lead you to more importantly,

83
00:06:51,000 --> 00:06:54,000
it's not going to lead you to peace happiness and freedom from suffering.

84
00:06:54,000 --> 00:06:57,000
It'll lead you to think more, to have more ideas.

85
00:06:57,000 --> 00:07:01,000
If you want to practice exercise fine, if you want to practice yoga go for it.

86
00:07:01,000 --> 00:07:03,000
Power to you.

87
00:07:03,000 --> 00:07:07,000
But if you ask me whether yoga is a good thing and exercise is a good thing, I'd say no.

88
00:07:07,000 --> 00:07:11,000
It's not really that any use at all.

89
00:07:11,000 --> 00:07:16,000
But that seems often so harsh and gets people all upset about it,

90
00:07:16,000 --> 00:07:20,000
but you have to understand what I mean by having no meaning or no purpose,

91
00:07:20,000 --> 00:07:23,000
because nothing in the universe is any purpose.

92
00:07:23,000 --> 00:07:25,000
Purpose doesn't have a place in the universe.

93
00:07:25,000 --> 00:07:26,000
The universe is what it is.

94
00:07:26,000 --> 00:07:30,000
It goes according to its nature and it goes according to cause and effect.

95
00:07:30,000 --> 00:07:31,000
Anything could happen.

96
00:07:31,000 --> 00:07:35,000
It could turn into anything at all.

97
00:07:35,000 --> 00:07:43,000
And so if you want to create something, go ahead and create developing ideas and concepts and views.

98
00:07:43,000 --> 00:07:45,000
Create something.

99
00:07:45,000 --> 00:07:48,000
The Buddhist practice is exercise, create something.

100
00:07:48,000 --> 00:07:49,000
Yoga will create something.

101
00:07:49,000 --> 00:07:52,000
It creates what we call sankara.

102
00:07:52,000 --> 00:08:01,000
And our teaching is that we only develop these things out of ignorance,

103
00:08:01,000 --> 00:08:04,000
because we believe incorrectly.

104
00:08:04,000 --> 00:08:09,000
Or our belief that these things have some purpose or some benefit is based on ignorance.

105
00:08:09,000 --> 00:08:12,000
They don't actually have any intrinsic benefit.

106
00:08:12,000 --> 00:08:13,000
They don't lead to anything.

107
00:08:13,000 --> 00:08:15,000
Nothing does.

108
00:08:15,000 --> 00:08:24,000
And so it's so holistic and so all-encompassing that you really have to understand that when you hear these kinds of answers.

109
00:08:24,000 --> 00:08:27,000
How can we do without views and opinions?

110
00:08:27,000 --> 00:08:32,000
Well, because like everything else in the universe, they're meaningless.

111
00:08:32,000 --> 00:08:35,000
Life is meaningless.

112
00:08:35,000 --> 00:08:39,000
It doesn't mean that you should kill yourself because that would be pointless as well.

113
00:08:39,000 --> 00:08:45,000
It means you should learn to let go and learn to just live and not cling to this type of life.

114
00:08:45,000 --> 00:08:47,000
No, this is the way it should be.

115
00:08:47,000 --> 00:08:50,000
This is what makes me happy because it won't make you happy.

116
00:08:50,000 --> 00:08:54,000
Or no, do everything I can to stop this from coming and then I'll be happy.

117
00:08:54,000 --> 00:08:55,000
And you won't be happy.

118
00:08:55,000 --> 00:08:59,000
You'll be more stressed out and more depressed and more upset.

119
00:08:59,000 --> 00:09:08,000
Letting go and seeing, I guess the pointlessness of life doesn't make you turn away from or doesn't make you averse to life.

120
00:09:08,000 --> 00:09:10,000
It doesn't make life boring.

121
00:09:10,000 --> 00:09:19,000
It just means that you live with life as opposed to fighting against it and trying to keep it in a certain way and keep it from being in a certain way.

122
00:09:19,000 --> 00:09:27,000
So a lot of talking about the more general point here.

123
00:09:27,000 --> 00:09:37,000
But the point being that if in the world there is some reason that you see the need in a conventional sense to think and to develop views,

124
00:09:37,000 --> 00:09:38,000
then go for it.

125
00:09:38,000 --> 00:09:42,000
Yeah, if you're working in an office, you probably need opinions about things.

126
00:09:42,000 --> 00:09:43,000
What do you think about this?

127
00:09:43,000 --> 00:09:45,000
What should we do this or this?

128
00:09:45,000 --> 00:09:48,000
If you don't have an opinion, you're probably going to get fired.

129
00:09:48,000 --> 00:09:49,000
That kind of thing.

130
00:09:49,000 --> 00:09:52,000
In a conventional sense, all of these things are useful.

131
00:09:52,000 --> 00:09:56,000
In a conventional sense, yoga is useful, physical exercise is useful.

132
00:09:56,000 --> 00:09:59,000
Thinking is useful, ideas are useful, views are useful.

133
00:09:59,000 --> 00:10:04,000
Even craving is useful, ambition is useful in society.

134
00:10:04,000 --> 00:10:05,000
All of these things are useful.

135
00:10:05,000 --> 00:10:08,000
But if you ask us, if you ask me, I'm going to say they're all useless.

136
00:10:08,000 --> 00:10:12,000
They're not going to lead to your overall well-being, peace, happiness, and suffering.

137
00:10:12,000 --> 00:10:14,000
What will is to give them all up.

138
00:10:14,000 --> 00:10:20,000
Go to the forest, start practicing meditation, put on some robes, eat one meal a day, and let go.

139
00:10:20,000 --> 00:10:21,000
Start to fly.

140
00:10:21,000 --> 00:10:23,000
I always come back to this.

141
00:10:23,000 --> 00:10:25,000
We're birds clinging to the side of the cliff.

142
00:10:25,000 --> 00:10:28,000
Thinking that if we let go, we're going to fall.

143
00:10:28,000 --> 00:10:31,000
The truth is when you let go, you can start to fly.

144
00:10:31,000 --> 00:10:33,000
That's what we start to do.

145
00:10:33,000 --> 00:10:36,000
I hope that was related to the question.

