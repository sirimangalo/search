1
00:00:00,000 --> 00:00:27,160
Okay, good evening, everyone.

2
00:00:27,160 --> 00:00:45,920
We're broadcasting live, January 23rd, 2016.

3
00:00:45,920 --> 00:00:56,480
Today's quote is from the Anguttarani Kaya, Book of Forest, with four qualities the wise intelligent

4
00:00:56,480 --> 00:01:05,400
worthy person proceeds through life with firm foundations, smoothly and reproachably not

5
00:01:05,400 --> 00:01:15,240
censured by the wise, but for with good conduct of body, speech and mind and gratitude with

6
00:01:15,240 --> 00:01:35,280
great for us, tonight we're broadcasting from the main hall, otherwise known as the

7
00:01:35,280 --> 00:01:46,840
living room of our house and monastery, we have two meditators, one from Finland, one from

8
00:01:46,840 --> 00:02:01,280
the Ukraine, listening, we have a live audience tonight, it's pretty awesome that we

9
00:02:01,280 --> 00:02:07,280
have people staying here with us, it's all of those of you who have supported this place,

10
00:02:07,280 --> 00:02:15,280
you can know that it's being put to the use, we have people coming next month and already

11
00:02:15,280 --> 00:02:27,280
in March, we have people coming, we also have a webcam, but didn't set it up in time,

12
00:02:27,280 --> 00:02:33,800
so maybe by tomorrow we'll have a webcam, if you want to take a peek at us, sitting

13
00:02:33,800 --> 00:02:50,200
here in the hall, so the quote is for things that lead one to proceed through life with

14
00:02:50,200 --> 00:02:57,000
firm foundations, smoothly or approachably not censured by the wise, so for things that

15
00:02:57,000 --> 00:03:13,880
make life go smoothly, first three are good deeds, body, speech and mind, kaya kamma, kuant

16
00:03:13,880 --> 00:03:25,760
kamma, noh kamma, these are the three doors, every karma that we perform, every act

17
00:03:25,760 --> 00:03:34,520
that we perform has to be through one of the three doors, they're just, it's just a convention,

18
00:03:34,520 --> 00:03:44,520
it is a conventionally the three doors that it's useful to allow us to identify quickly

19
00:03:44,520 --> 00:03:56,040
our actions, am I committing an act, a wholesome act or an unwholesome act with my body,

20
00:03:56,040 --> 00:04:03,280
with my speech or even in the mind, I think significant here is that the mind is given

21
00:04:03,280 --> 00:04:11,560
its own door, which is significant in that it shows that karma can be performed without

22
00:04:11,560 --> 00:04:34,880
even acting or speaking, just by thinking, and in fact all three doors require the mind,

23
00:04:34,880 --> 00:04:42,960
acting is only wholesome, if it has a wholesome mind associated with it, simply acting,

24
00:04:42,960 --> 00:04:51,320
going through the motions is not wholesome or unwholesome, it's only the intention in the

25
00:04:51,320 --> 00:04:57,840
mind that gives it the wholesome or unwholesome quality, and that's how meditation affects

26
00:04:57,840 --> 00:05:09,600
our lives, think how does it, what good does it do to walk and sit, alone in your room,

27
00:05:09,600 --> 00:05:14,800
especially when this kind of meditation isn't so calming as you might fish or you get

28
00:05:14,800 --> 00:05:19,480
both, it's very easy to be turned off in this meditation, it's not something that everyone

29
00:05:19,480 --> 00:05:28,800
will want to practice, it's not something everyone will continue to practice, once they

30
00:05:28,800 --> 00:05:35,080
get a feel for the nature of it, it's too scary to look at the negative aspects of the

31
00:05:35,080 --> 00:05:46,040
mind, it's too difficult to painful, it's everyone who wonder why we do it.

32
00:05:46,040 --> 00:05:57,400
And this is, this is it really the mind, it's the prominence of the mind, the importance

33
00:05:57,400 --> 00:06:12,840
of the mind is, how our bad habits are actually habits that we've cultivated, we act based

34
00:06:12,840 --> 00:06:18,720
on habit, we get angry based on habit, we are greedy and attached to things based on

35
00:06:18,720 --> 00:06:26,880
habit, we are arrogant and conceited based on habit, so we can change these habits, if

36
00:06:26,880 --> 00:06:35,800
we learn to be objective, we can start to incline our minds in a different way, incline

37
00:06:35,800 --> 00:06:45,800
our minds towards patience, incline our minds towards pronunciation, incline our minds

38
00:06:45,800 --> 00:06:58,080
towards humility and wisdom, incline our minds towards freedom.

39
00:06:58,080 --> 00:07:03,920
So when you wonder what you're doing in the meditation, just remember every moment when

40
00:07:03,920 --> 00:07:12,840
your mind is clear when your mind is pure, that's a karma, when all karma, it's a wholesome

41
00:07:12,840 --> 00:07:19,520
karma, what that means is it's cultivating a habit, so you may not see the results today,

42
00:07:19,520 --> 00:07:28,800
maybe not even tomorrow, but you have no reason to doubt, no room for doubt that it's

43
00:07:28,800 --> 00:07:42,640
changing, it's changing your habit, it's getting cultivating a new, better habit.

44
00:07:42,640 --> 00:07:46,520
And the fourth one in this list is actually interesting, it looks like it's kind of just

45
00:07:46,520 --> 00:07:53,920
thrown in there, and the angutanikai is kind of like this, there's the book of twos,

46
00:07:53,920 --> 00:08:07,880
the book of one, the book of twos, all the way up to the book of 11 years, and some of

47
00:08:07,880 --> 00:08:11,560
these lists are repeated, it will be in the book of fours, and then the book of fives

48
00:08:11,560 --> 00:08:16,520
will be the same list, but one more thing is added on, then the book of six, they add

49
00:08:16,520 --> 00:08:22,120
a sixth thing on, it kind of goes like that, sometimes the same list is in three, two or

50
00:08:22,120 --> 00:08:29,560
three different books, just with one more thing, so it seems that these lists were catered

51
00:08:29,560 --> 00:08:42,000
to individuals, and so this one may have just been catered to a group of monks, a group

52
00:08:42,000 --> 00:08:49,880
of lay people talking about gratitude, but gratitude doesn't get so much, airtime and

53
00:08:49,880 --> 00:09:01,840
Buddhism, but in this context it is aptly added to this list, because it really is a

54
00:09:01,840 --> 00:09:09,000
very powerful mind state to be grateful, you know Buddhism way often in all different

55
00:09:09,000 --> 00:09:16,440
schools and traditionally, if you go to a traditional Buddhist culture, or often is a sense

56
00:09:16,440 --> 00:09:23,920
of thanking people who do you harm, you thank them for doing you harm because it's

57
00:09:23,920 --> 00:09:32,920
a chance for you to practice patience, it was a Sri Lankan monk once, he said he doesn't

58
00:09:32,920 --> 00:09:40,080
like going to Thailand, he doesn't like going to Thailand, or any of the Southeast Asian

59
00:09:40,080 --> 00:09:44,360
countries, but his reasoning was interesting, he said he doesn't like going there because

60
00:09:44,360 --> 00:09:50,760
he can't practice maita, when he goes to Thailand, the people are too nice to him, he's

61
00:09:50,760 --> 00:09:59,120
that, too nice to him and so he can't cultivate love because of course in Sri Lanka, or

62
00:09:59,120 --> 00:10:05,560
not of course, but in Sri Lanka, all they treat the monks, not badly that they really

63
00:10:05,560 --> 00:10:14,480
put them through their paces, if a monk is misbehaving, they'll call them not on it, they'll

64
00:10:14,480 --> 00:10:24,120
argue with the monks, this is a fairly famous Buddhist monk, he said, it's common in Zen

65
00:10:24,120 --> 00:10:29,040
Buddhism for example, they go around hitting the meditators to make them straight in their

66
00:10:29,040 --> 00:10:35,880
legs, make sure they sit up straight and they're going to walk, and then they have to put

67
00:10:35,880 --> 00:10:45,480
their hands together and say thank you, but why this is interesting is I think it points

68
00:10:45,480 --> 00:10:51,920
to something that the power of gratitude, the power of gratitude specifically, not in

69
00:10:51,920 --> 00:10:58,440
a practice sense in terms of cultivating humility and so on, I mean it's often, it can

70
00:10:58,440 --> 00:11:06,560
lead to fake humility even where you say thank you and it's kind of pretense like you're

71
00:11:06,560 --> 00:11:14,280
trying to be a good person, but you're really angry at the person, but it's important

72
00:11:14,280 --> 00:11:20,720
because it smooths things over, if you try, if you're grateful to people, it's a really

73
00:11:20,720 --> 00:11:26,480
good way to make friends, to keep friends, to solidify friendship, it says I see what

74
00:11:26,480 --> 00:11:33,920
you did, I recognize what you did, I know I've been, I mean as a Buddhist monk we have

75
00:11:33,920 --> 00:11:41,600
a lot to be grateful for, people give us food, they give us shelter, and I know I've

76
00:11:41,600 --> 00:11:52,200
been guilty of in gratitude to supporters and mostly to supporters, I've been supported

77
00:11:52,200 --> 00:12:04,080
when I was in Thailand, I was in Sri Lanka, I was in California, I was supported, really

78
00:12:04,080 --> 00:12:09,960
well in California, I've supported so many different people, I know at times it's easy

79
00:12:09,960 --> 00:12:16,800
to be here, forget, and it's a lesson to be learned because it's unfair to the people

80
00:12:16,800 --> 00:12:26,040
who are supporting you, so now I try, I'm not perfect, but I try as you, keep track if someone

81
00:12:26,040 --> 00:12:33,600
sends me a gift, I have a whole stack full of cards that people have sent me, I try not

82
00:12:33,600 --> 00:12:42,800
to throw them out because it's a good reminder to say thank you to people, it's a good

83
00:12:42,800 --> 00:12:49,240
way to live your life, and if they do the smallest things for you to be grateful, it's

84
00:12:49,240 --> 00:12:56,120
part of being mindful, it's convention, and if you're certainly not deep teaching for

85
00:12:56,120 --> 00:13:04,440
meditative, I think you could argue that a meditative becomes more grateful, remember when

86
00:13:04,440 --> 00:13:08,480
I did my first meditation course, I started crying and the humans said what's wrong

87
00:13:08,480 --> 00:13:16,280
and I said I miss my parents, I really felt grateful to them, and I felt like a real jerk

88
00:13:16,280 --> 00:13:21,720
that I hadn't been grateful to them, when I was young, they take our parents, parents

89
00:13:21,720 --> 00:13:27,520
are a good example, and some parents are really horrible to their kids, but many parents

90
00:13:27,520 --> 00:13:34,400
were good to their children, the children just don't appreciate it, that was one of

91
00:13:34,400 --> 00:13:43,720
those kids I think, I think I really appreciated not fully anyway, and the sacrifices

92
00:13:43,720 --> 00:13:52,520
my parents went through, then it causes friction, and because they feel depressed, they

93
00:13:52,520 --> 00:13:59,280
are not appreciated, so that they expect anything in return, but it's depressing, it's

94
00:13:59,280 --> 00:14:10,120
sad that you have your children be selfish and ungrateful, so I think it's something that

95
00:14:10,120 --> 00:14:15,240
comes from meditation certainly, it's something that comes from meditation, and mind becomes

96
00:14:15,240 --> 00:14:20,080
pure, you're able to see things clear.

97
00:14:20,080 --> 00:14:25,440
The word for gratitude and poly is cut the new and cut the weighty, that's the reason

98
00:14:25,440 --> 00:14:30,920
there's two words here, because he's translating both, cut the new, cut the weighty,

99
00:14:30,920 --> 00:14:36,280
so cut the new, and yeah, it means knowledge, and you means one who knows, one who has

100
00:14:36,280 --> 00:14:41,720
knowledge, cut that means what is done, so it's a very much, the way it's word it is

101
00:14:41,720 --> 00:14:48,320
kind of in terms of wisdom or might even mindfulness, cut the new one who knows what has

102
00:14:48,320 --> 00:14:54,760
been done, so the implication is that if you're ungrateful, it's because you just don't

103
00:14:54,760 --> 00:15:16,800
keep an awareness of it, you don't pay attention to it, and cut the weighty, it means

104
00:15:16,800 --> 00:15:25,080
basically it means the same thing, grammatically it means the same thing, cut the weighty

105
00:15:25,080 --> 00:15:30,880
weight, that means knowledge, it's a different kind of knowledge, but it's still translated

106
00:15:30,880 --> 00:15:40,600
similarly, the weighty, one who has knowledge, awareness, awareness is conscious of it,

107
00:15:40,600 --> 00:15:48,000
one who is conscious of what has been done, so it could be synonymous, but the commentary

108
00:15:48,000 --> 00:15:54,720
has got the weighty, cut the new means knowledge, it means gratitude, thankfulness,

109
00:15:54,720 --> 00:16:01,360
being thankful, and cut the weighty means the desire to pay them back, to do something

110
00:16:01,360 --> 00:16:20,760
to express your gratitude, so that's the fourth one, and then the thing will stop there,

111
00:16:20,760 --> 00:16:39,840
thank you all for tuning in, and have a good night.

112
00:16:39,840 --> 00:16:54,960
Thank you.

