1
00:00:00,000 --> 00:00:06,860
So, just in regards to the question on non-self and reincarnation, there's a good book that

2
00:00:06,860 --> 00:00:11,200
is of interest in regards to this issue of non-self.

3
00:00:11,200 --> 00:00:19,800
It's certainly not for a seven-year-old, but the Mahasizaya does book this course on non-self.

4
00:00:19,800 --> 00:00:27,200
And in there, he actually expands on what Patwapani, and he said it made me think of it because

5
00:00:27,200 --> 00:00:31,600
he says there's at least three types of self.

6
00:00:31,600 --> 00:00:34,320
I think there may be more, but I can remember three of them.

7
00:00:34,320 --> 00:00:44,000
One is the self that, as you say, that controls the controlling self, the Lord's self.

8
00:00:44,000 --> 00:00:46,400
This is sami at that.

9
00:00:46,400 --> 00:00:56,080
And then there is the nitya, dar, I don't know, the self that is permanent.

10
00:00:56,080 --> 00:01:00,880
So some people postulate a self that controls, some people postulate a self that is permanent,

11
00:01:00,880 --> 00:01:05,800
some people postulate the self that experiences, as the one who experiences, and I think

12
00:01:05,800 --> 00:01:13,440
there are other examples, but it's definitely a book worth reading and it's a little bit

13
00:01:13,440 --> 00:01:19,240
difficult and repetitive to go through because he's going through each aspect, but it's

14
00:01:19,240 --> 00:01:21,120
very much in line with the meditation practice.

15
00:01:21,120 --> 00:01:25,120
And now I remember what I wanted to say about reincarnation.

16
00:01:25,120 --> 00:01:30,280
Albert Einstein was once asked about the theory of relativity.

17
00:01:30,280 --> 00:01:34,320
He was, he was became very famous for the theory of relativity and apparently he was

18
00:01:34,320 --> 00:01:45,360
asked by an interviewer, by a reporter, to explain relativity in a few sentences.

19
00:01:45,360 --> 00:01:53,080
And he looked at the guy and he said, you know, if you, even if you had a solid grounding

20
00:01:53,080 --> 00:01:59,520
in theoretical physics, it would take me a couple of days to explain the theory of relativity

21
00:01:59,520 --> 00:02:02,800
to you.

22
00:02:02,800 --> 00:02:09,680
And this is the, the, the profundity of his, his realization that it's not something

23
00:02:09,680 --> 00:02:17,240
that an ordinary person can understand and we're so deeply rooted in, in delusions

24
00:02:17,240 --> 00:02:27,280
where they are in, from, from physics point of view in, in, in our biological and, and

25
00:02:27,280 --> 00:02:32,800
genetic predisposition, that we can't understand thing.

26
00:02:32,800 --> 00:02:37,800
We're not programmed to understand reality.

27
00:02:37,800 --> 00:02:41,720
So we can give a similar answer, you know, seven year old, it's very rare that would

28
00:02:41,720 --> 00:02:46,960
be very rare to find a seven year old who could have any idea about reincarnation.

29
00:02:46,960 --> 00:02:52,480
So it's not something that you asked, the, the question was, can you please explain reincarnation

30
00:02:52,480 --> 00:02:58,040
like you're seven year old, there's not much we could do to explain the, the, the workings

31
00:02:58,040 --> 00:02:59,840
of reality.

32
00:02:59,840 --> 00:03:05,600
If you mean to give the simplest explanation of it possible, I can do that.

33
00:03:05,600 --> 00:03:11,000
I'm not sure that it's going to help you and, and I think only intensive practice will

34
00:03:11,000 --> 00:03:15,000
help you to, to understand what, what I'm even talking about.

35
00:03:15,000 --> 00:03:19,960
But it's that reality is what you're experiencing right now.

36
00:03:19,960 --> 00:03:23,040
Reality is you're seeing, you're hearing, you're smelling, you're tasting, you're feeling

37
00:03:23,040 --> 00:03:24,040
and thinking.

38
00:03:24,040 --> 00:03:30,480
So ask yourself whether that's going to cease when you die and you should be able to

39
00:03:30,480 --> 00:03:32,320
see that it's, that's an uncertainty.

40
00:03:32,320 --> 00:03:39,720
You don't really know the, the, the best way to go about asking that question and investing

41
00:03:39,720 --> 00:03:44,480
in that question is first to give up all of your predispose knowledge, all of your ideas

42
00:03:44,480 --> 00:03:48,720
about, you know, I saw that person die and therefore death is the end so that they're not

43
00:03:48,720 --> 00:03:49,720
thinking anymore.

44
00:03:49,720 --> 00:03:50,720
Give that all up.

45
00:03:50,720 --> 00:03:55,720
Give up all of your, all of the scientific observations that people have, have, have

46
00:03:55,720 --> 00:04:01,320
had and all of the, the experiments and so on that have been done that actually have,

47
00:04:01,320 --> 00:04:07,280
you know, evidence on both sides, you know, all this investigation into past life memories

48
00:04:07,280 --> 00:04:11,040
and so on, could also be used on the other side, but give that all up.

49
00:04:11,040 --> 00:04:16,360
Give up even the knowledge that people do die and just ask yourself or, or investigate

50
00:04:16,360 --> 00:04:19,920
this idea of death, whether it's possible that this could happen in the future, you'll

51
00:04:19,920 --> 00:04:25,520
see that actually there's no aspect of experience that could admit of such a thing.

52
00:04:25,520 --> 00:04:29,400
And so the strange thing would be that if it did cease and that was the profundity of what

53
00:04:29,400 --> 00:04:35,640
the Buddha found is that he found a way by which this experience ceases and it's not death.

54
00:04:35,640 --> 00:04:38,600
It's nibana.

55
00:04:38,600 --> 00:04:45,680
And, you know, the, the, the point was already made that we don't believe in, in reincarnation.

56
00:04:45,680 --> 00:04:47,400
We simply don't believe in death.

57
00:04:47,400 --> 00:04:52,080
The experience continues on as long as there's craving in the mind and we've talked

58
00:04:52,080 --> 00:04:53,080
about that.

59
00:04:53,080 --> 00:04:54,080
I did a video on this.

60
00:04:54,080 --> 00:04:55,080
You can look at a nature of reality.

61
00:04:55,080 --> 00:05:00,360
I think we've talked about this on the ask, the, ask that Siri Mangala form.

62
00:05:00,360 --> 00:05:06,600
So there is, the information is out there from our point of view, my point of view.

63
00:05:06,600 --> 00:05:22,660
Now what I'm going to do here is I'm going to be able to make that.

