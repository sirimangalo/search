Do Buddhists believe in qi as Taoist do, and if so is there a method of generating this energy in oneself through meditation, etc.
I feel like I've been seeking protection through meditation.
Till I realize that I shouldn't be seeking protection, but maybe rather generating this power within myself.
Do you think Paul?
Protection through meditation, I think is definitely possible.
I find it protects me a lot.
It keeps me on even qi, so it uses the amount of suffering like I've been.
I'm not sure if Buddhist believe in qi, I don't think I personally believe in qi.
Some of the years...
In some of the martial arts, they have some people teach this qi as some kind of energy, and some of the doctors just look at it as blood flow.
I've seen that talk before where it's not a mystical energy, but it's a blood flowing in your body.
But yeah, I don't ask for the energy part, I don't think that would be more of a Buddhist view.
It's almost like you're saying it's a soul, or like a entity.
All of these questions, all of these questions that start the Buddhists believe should be answered as a case three dominant.
There's a wonderful video teaching that is, I think, quite famous out there, talking to you in Malaysia, I think Singapore or Malaysia, I'm not sure.
Where he's talking about, because he wrote a book called What Buddhists Believe.
And he said I was interviewed once by someone about this book, and he said, so you've written this book, What Buddhists Believe, so tell me, what do Buddhists believe?
And he said, absolutely nothing.
And the man said, then why did you write this book?
And he said, and I told him, that's exactly why I wrote this book, to show you that there's nothing to believe.
You don't need to believe Buddhists taught Buddhism, the Buddha taught to know, to understand.
But I think we shouldn't miss this point that Buddhists don't believe anything, because literally it's very important.
This kind of question is kind of awkward for a Buddhist, because we're like, well, how do you explain to this person that that's not what Buddhism is about?
It's not about yes or no answers to questions of belief, it's about seeing the truth of mundane reality.
It's about letting go rather than taking on some sort of belief in something external to the six senses.
Buddhism is in fact about reducing reality to ultimate experience, to seeing hearing, smelling, tasting, feeling thinking.
It's about getting rid of concepts like chi or even the breath, for example.
The reason why we focus on the breath is to get rid of the concept of the breath, to come to see it as just physical and mental experiences, but the breath itself doesn't exist.
So things like the chakras, things like chi, and so on and so on, and so on. Do you believe in this? Do you believe in God? Do you believe in ghosts? Do you believe in God?
Goodness, don't believe in anything. We make every effort to stop believing in absolutely everything. And being facetious, because of course, the answer is yes, we believe in God, yes, we believe in ghosts, yes, we believe in X, Y, and Z.
When I do, it's the same as the question of astrology. I mean, what you're asking is like, wow, now I have to have an opinion on this, now I have to go out and study chi, energy, and give you an answer, because it's not Buddhism.
There's nothing Buddhist about me as to whether I believe in chi or not. It has more to do with whether I've investigated it and have any reason, totally apart from Buddhism to believe in it, because Buddha didn't teach this sort of thing, the Buddha taught, as I said, to give up our beliefs, to give up our obsession with concepts and to come to see ultimate reality for what it is.
So, the only reason I might believe in chi is because I had studied it, which wouldn't have come about in Buddhism.
So, I guess what I'd like to impress is an understanding of exactly what the Buddha taught and what Buddhism focuses on, so that people will not have to ask these sorts of questions.
Because it's understandable, you think, wow, look at the Buddha's teaching is so big, I wonder if anywhere in there he taught this, or anywhere in there he taught that.
But take it from me, don't take it from me in any basic understanding of the Buddha's teaching, or what it should give you, or what you should learn from Buddhism, is that it just doesn't apply.
It has no place in the Buddha Dam, so, could chi exist, yeah, sure, I mean the Buddha may have even, I've never come across the Buddha, but he may have even talked about it, but it's not, I guess the point is that it's not really a question about Buddhism, isn't that, isn't that, is that it?
Whether chi energy exists and you can use it for this or that, it's like asking whether nuclear power exists and whether you can use it for this or that, it's nothing to do with the path.
So, I'm not trying to criticize the answer, I can understand where these questions come from, I'm trying to criticize the question, just trying to help to get back on.
