1
00:00:00,000 --> 00:00:04,000
Hello and welcome back to our study of the Dhamupada.

2
00:00:04,000 --> 00:00:11,000
Today we continue on with 1st number 66, which reads as follows.

3
00:00:34,000 --> 00:00:45,000
Fools who are of poor wisdom, fair or travel or live.

4
00:00:45,000 --> 00:00:55,000
Amitaneva atana as their own enemy, or as someone who is not their own friend.

5
00:00:55,000 --> 00:01:02,000
So people who are evil, who are fools, act, otherwise than their own friend,

6
00:01:02,000 --> 00:01:05,000
or not a friend, are not a friend in themselves.

7
00:01:05,000 --> 00:01:11,000
Kron'ta papakan kamang, they're in doing evil deans,

8
00:01:11,000 --> 00:01:21,000
or because of their foolishness and their lack of wisdom.

9
00:01:21,000 --> 00:01:24,000
They perform evil deans.

10
00:01:24,000 --> 00:01:29,000
Young hote katuka, along which are of bitter fruit.

11
00:01:29,000 --> 00:01:39,000
So a person who is foolish and of little wisdom acts,

12
00:01:39,000 --> 00:01:42,000
and is not a friend in themselves, not as a friend in themselves,

13
00:01:42,000 --> 00:01:47,000
or as their own enemy, because they do evil deans, which are bitter fruit.

14
00:01:47,000 --> 00:01:53,000
They experience the results of their bad deans, which are bitter.

15
00:01:53,000 --> 00:01:54,000
So this is the verse.

16
00:01:54,000 --> 00:01:59,000
The story is actually, it's interesting.

17
00:01:59,000 --> 00:02:06,000
This particular verse isn't at all related to the actual story.

18
00:02:06,000 --> 00:02:09,000
It's related to a past story.

19
00:02:09,000 --> 00:02:13,000
A story of reason why things were the way they were.

20
00:02:13,000 --> 00:02:16,000
The actual story is about Super Buda the leper,

21
00:02:16,000 --> 00:02:22,000
and he once went to listen to the Buda teaching

22
00:02:22,000 --> 00:02:27,000
and sat at the outside of the Buddha's, of the assembly.

23
00:02:27,000 --> 00:02:30,000
And while the Buddha was teaching,

24
00:02:30,000 --> 00:02:38,000
he actually internalized or understood the Buddha's teaching,

25
00:02:38,000 --> 00:02:40,000
and he became a sotapana.

26
00:02:40,000 --> 00:02:42,000
He saw Nimbana while he was sitting there.

27
00:02:42,000 --> 00:02:44,000
He realized cessation.

28
00:02:44,000 --> 00:02:47,000
He had an experience of the cessation of suffering.

29
00:02:47,000 --> 00:02:51,000
Just while he was sitting there listening to the Buddha's teaching.

30
00:02:51,000 --> 00:02:58,000
And it was a wonderful thing for him,

31
00:02:58,000 --> 00:03:00,000
and he went home,

32
00:03:00,000 --> 00:03:04,000
and he thought that he should go and let the Buddha know

33
00:03:04,000 --> 00:03:07,000
that he had become a sotapana.

34
00:03:07,000 --> 00:03:14,000
But first, he was approached by Saka.

35
00:03:14,000 --> 00:03:18,000
This guy, I think he's taken, he's appeared before, no?

36
00:03:18,000 --> 00:03:20,000
More than once.

37
00:03:20,000 --> 00:03:22,000
Saka's this guy who comes and tests Budas.

38
00:03:22,000 --> 00:03:24,000
He's actually a sotapana himself.

39
00:03:24,000 --> 00:03:26,000
He's someone who's seen Nimbana,

40
00:03:26,000 --> 00:03:28,000
but he's this angel up in heaven.

41
00:03:28,000 --> 00:03:31,000
He's the head of the angels of the 33,

42
00:03:31,000 --> 00:03:34,000
the Dabbatings and angels.

43
00:03:34,000 --> 00:03:37,000
So he comes down and he says,

44
00:03:37,000 --> 00:03:43,000
huh, here's a guy who is hard up on his luck.

45
00:03:43,000 --> 00:03:47,000
He's suffering from this crippling debilitating disease.

46
00:03:47,000 --> 00:03:50,000
He thought to himself, well, there's anyone I could test.

47
00:03:50,000 --> 00:03:56,000
I wonder how he would react if I were to test him.

48
00:03:56,000 --> 00:04:00,000
And so he comes to Supa Buda and he says to him.

49
00:04:00,000 --> 00:04:03,000
He comes as a rich man,

50
00:04:03,000 --> 00:04:05,000
and dressed as a rich man, and he says to him,

51
00:04:05,000 --> 00:04:08,000
Supa Buda, I'll give you limitless wealth.

52
00:04:08,000 --> 00:04:11,000
If you renounce the Buddha, if you say,

53
00:04:11,000 --> 00:04:13,000
Budo Nabudho.

54
00:04:13,000 --> 00:04:16,000
The Buddha, that guy over there who I think is the Buddha.

55
00:04:16,000 --> 00:04:18,000
It's not really the Buddha.

56
00:04:18,000 --> 00:04:20,000
I want you to pronounce him saying,

57
00:04:20,000 --> 00:04:22,000
yeah, he's not the Buddha.

58
00:04:22,000 --> 00:04:25,000
And I want you to say that the Dhamma is not the Dhammo.

59
00:04:25,000 --> 00:04:27,000
Dhammo, the Dhamma is not the Dhamma.

60
00:04:27,000 --> 00:04:30,000
The Sanghon, the Sanghon, the Sanghon,

61
00:04:30,000 --> 00:04:33,000
that group of enlightened beings are not actually enlightened.

62
00:04:33,000 --> 00:04:36,000
You say that, and I'll give you limitless wealth.

63
00:04:36,000 --> 00:04:39,000
He says, you know, you're hard up on it.

64
00:04:39,000 --> 00:04:41,000
You can't possibly make a living.

65
00:04:41,000 --> 00:04:44,000
You can't possibly live and comfort.

66
00:04:44,000 --> 00:04:46,000
And here I'm offering you the life

67
00:04:46,000 --> 00:04:48,000
which you were never able to have,

68
00:04:48,000 --> 00:04:50,000
offering you great wealth.

69
00:04:50,000 --> 00:04:53,000
All you have to do is say,

70
00:04:53,000 --> 00:04:58,000
Budo Nabudho, the Dhammon, the Dhammo, the Sanghon, and the Sanghon.

71
00:04:58,000 --> 00:05:01,000
And Supa Buda looks at him as though he's crazy,

72
00:05:01,000 --> 00:05:04,000
and says, what are you talking about?

73
00:05:04,000 --> 00:05:07,000
I have limitless wealth.

74
00:05:07,000 --> 00:05:09,000
I have the highest wealth.

75
00:05:09,000 --> 00:05:13,000
He said, a person who is accomplished

76
00:05:13,000 --> 00:05:17,000
like me in the teachings.

77
00:05:17,000 --> 00:05:19,000
It was realized the teachings of the Buda

78
00:05:19,000 --> 00:05:22,000
has the greatest wealth.

79
00:05:22,000 --> 00:05:27,000
And he lists off the seven Aria, Aria, Aria,

80
00:05:27,000 --> 00:05:31,000
Aria, Ariadana, Ariadana,

81
00:05:31,000 --> 00:05:36,000
the seven noble treasures or noble wealth.

82
00:05:36,000 --> 00:05:38,000
And we have a verse here,

83
00:05:38,000 --> 00:05:43,000
Sada Dhanang, Sila Dhanang.

84
00:05:43,000 --> 00:05:49,000
Whoa, heree, heree,

85
00:05:49,000 --> 00:05:53,000
whoa, tap, yang, Tanang.

86
00:05:53,000 --> 00:05:58,000
Sutta, Tanang, Ja, Ja, Goja.

87
00:05:58,000 --> 00:06:03,000
Panya, we satamang, Tanang.

88
00:06:03,000 --> 00:06:05,000
Sorry, it's written in David Nagari,

89
00:06:05,000 --> 00:06:08,000
so I have a little bit of trouble reading it.

90
00:06:08,000 --> 00:06:09,000
But these are the seven wealth.

91
00:06:09,000 --> 00:06:11,000
Sada is confidence.

92
00:06:11,000 --> 00:06:13,000
So a person who is,

93
00:06:13,000 --> 00:06:16,000
a person who has become a Sotabana

94
00:06:16,000 --> 00:06:18,000
has perfect faith, perfect confidence

95
00:06:18,000 --> 00:06:20,000
in the Buddha, in the Dhammon, the Sanghat,

96
00:06:20,000 --> 00:06:22,000
can never waver.

97
00:06:22,000 --> 00:06:26,000
So they would never have any reason to think

98
00:06:26,000 --> 00:06:28,000
that the Buddha, the Dhammon, the Sanghon,

99
00:06:28,000 --> 00:06:32,000
wasn't what they claim to me.

100
00:06:32,000 --> 00:06:35,000
Sila means they couldn't lie,

101
00:06:35,000 --> 00:06:38,000
so he couldn't possibly tell an untruth.

102
00:06:38,000 --> 00:06:42,000
He couldn't possibly have such a corrupt practice

103
00:06:42,000 --> 00:06:45,000
even just to denounce the Buddha,

104
00:06:45,000 --> 00:06:49,000
just to just pretend that the Buddha, the Dhammon,

105
00:06:49,000 --> 00:06:52,000
the Sanghat were not his refuge,

106
00:06:52,000 --> 00:06:58,000
were not his highest object of reverence.

107
00:06:58,000 --> 00:07:03,000
Here he means the disgust

108
00:07:03,000 --> 00:07:07,000
at the thought of doing an evil deed.

109
00:07:07,000 --> 00:07:11,000
So even just the thought of doing it would discuss or repel.

110
00:07:11,000 --> 00:07:13,000
It would be totally uninteresting.

111
00:07:13,000 --> 00:07:16,000
It would be contrary, like a magnet.

112
00:07:16,000 --> 00:07:20,000
It's not as though the person who is a Sotabana or an enlightened being

113
00:07:20,000 --> 00:07:24,000
gets disgusted or revolted wanting to throw up,

114
00:07:24,000 --> 00:07:27,000
but they are actually repulsed like a magnet

115
00:07:27,000 --> 00:07:29,000
from doing evil deeds.

116
00:07:29,000 --> 00:07:31,000
Totally averse in that and see no reason to do it.

117
00:07:31,000 --> 00:07:34,000
It's just contrary to their nature.

118
00:07:34,000 --> 00:07:36,000
Ohtabat means fear.

119
00:07:36,000 --> 00:07:38,000
Maybe it's not quite fear.

120
00:07:38,000 --> 00:07:39,000
It's not that they're afraid,

121
00:07:39,000 --> 00:07:42,000
but there is another type of being averse.

122
00:07:42,000 --> 00:07:45,000
They're averse to the results of bad karma.

123
00:07:45,000 --> 00:07:47,000
So seeing the results of the bad karma,

124
00:07:47,000 --> 00:07:50,000
they have no intention of doing bad deeds.

125
00:07:50,000 --> 00:07:52,000
Here you know what the Buddha goes together.

126
00:07:52,000 --> 00:07:55,000
Here he is, just a version towards the deed.

127
00:07:55,000 --> 00:07:58,000
Ohtabat is a version towards bad results.

128
00:07:58,000 --> 00:08:00,000
Knowing that bad deeds have bad results.

129
00:08:00,000 --> 00:08:02,000
They don't perform them.

130
00:08:02,000 --> 00:08:07,000
Based on the aversion towards the future results

131
00:08:07,000 --> 00:08:11,000
or the repulsion from it.

132
00:08:11,000 --> 00:08:14,000
Sootam means learning.

133
00:08:14,000 --> 00:08:16,000
So all this studying that we're doing of the Dhamabana,

134
00:08:16,000 --> 00:08:20,000
this is a great wealth.

135
00:08:20,000 --> 00:08:24,000
But I think we'd have to go by the very basic

136
00:08:24,000 --> 00:08:26,000
because Sootamat actually hadn't heard.

137
00:08:26,000 --> 00:08:27,000
He hadn't learned much.

138
00:08:27,000 --> 00:08:29,000
Sootamat is generally the word Sootamat.

139
00:08:29,000 --> 00:08:31,000
Generally refers to Bahu Soota

140
00:08:31,000 --> 00:08:33,000
where you've learned a lot.

141
00:08:33,000 --> 00:08:36,000
But here because Sootamat hasn't learned a lot.

142
00:08:36,000 --> 00:08:41,000
It means he's attained the knowledge.

143
00:08:41,000 --> 00:08:44,000
Sootamat actually means what is heard.

144
00:08:44,000 --> 00:08:49,000
Directly translated it comes from Sada or Sota.

145
00:08:49,000 --> 00:08:51,000
Sota means the year.

146
00:08:51,000 --> 00:08:54,000
It's the same word as Sota.

147
00:08:54,000 --> 00:08:58,000
Sootamat means that which is heard.

148
00:08:58,000 --> 00:09:04,000
So it just means having heard what is truly worth hearing.

149
00:09:04,000 --> 00:09:07,000
So they have heard that what should be heard.

150
00:09:07,000 --> 00:09:11,000
And indeed Supabana had heard in the essence

151
00:09:11,000 --> 00:09:14,000
in the ultimate, that which was of ultimate benefit

152
00:09:14,000 --> 00:09:16,000
from the Buddha.

153
00:09:16,000 --> 00:09:19,000
So by hearing about the forest at the Patana and the four

154
00:09:19,000 --> 00:09:23,000
of us and hearing about the three characteristics

155
00:09:23,000 --> 00:09:25,000
of impermanent suffering and on self

156
00:09:25,000 --> 00:09:27,000
and actually sitting and practicing,

157
00:09:27,000 --> 00:09:30,000
he gained this.

158
00:09:30,000 --> 00:09:32,000
He was able to become enlightened

159
00:09:32,000 --> 00:09:33,000
because of what he had heard.

160
00:09:33,000 --> 00:09:34,000
So that's what it means.

161
00:09:34,000 --> 00:09:36,000
This great treasure.

162
00:09:36,000 --> 00:09:42,000
The meaning is it's only one who has heard the Dhamma

163
00:09:42,000 --> 00:09:45,000
can become enlightened following a Buddha, of course.

164
00:09:45,000 --> 00:09:49,000
The only other alternative to this treasure

165
00:09:49,000 --> 00:09:51,000
would be to become a Buddha oneself,

166
00:09:51,000 --> 00:09:53,000
either a privately enlightened Buddha

167
00:09:53,000 --> 00:09:56,000
or a fully enlightened Buddha who teaches others.

168
00:09:56,000 --> 00:09:59,000
But besides those two options which are incredibly difficult

169
00:09:59,000 --> 00:10:03,000
to attain, one the only other option is Suta.

170
00:10:03,000 --> 00:10:06,000
So yes, every enlightened being besides a Buddha

171
00:10:06,000 --> 00:10:10,000
and a private Buddha has this treasure.

172
00:10:10,000 --> 00:10:13,000
It's a very valuable treasure having heard the Dhamma.

173
00:10:13,000 --> 00:10:18,000
Jaga means generosity or renunciation.

174
00:10:18,000 --> 00:10:23,000
It's often in ordinary or mundane terms described

175
00:10:23,000 --> 00:10:26,000
as generosity, the ability to give things

176
00:10:26,000 --> 00:10:31,000
to others, the desire to give or the impulsion

177
00:10:31,000 --> 00:10:34,000
to help others and to be generous and charitable.

178
00:10:34,000 --> 00:10:38,000
But on an ultimate level it means the ability to give up,

179
00:10:38,000 --> 00:10:39,000
not clinging to things.

180
00:10:39,000 --> 00:10:42,000
So Jaga is that which is given up wrong view

181
00:10:42,000 --> 00:10:45,000
and given up attachment, which, of course,

182
00:10:45,000 --> 00:10:47,000
enlightened being has done.

183
00:10:47,000 --> 00:10:51,000
Because they've come to see that these things are of no value,

184
00:10:51,000 --> 00:10:52,000
of no purpose.

185
00:10:52,000 --> 00:10:55,000
When compared with Nibana, they're like garbage.

186
00:10:55,000 --> 00:10:57,000
When compared with true peace and true happiness,

187
00:10:57,000 --> 00:11:01,000
which only an enlightened being can compare to.

188
00:11:01,000 --> 00:11:04,000
They're worthless.

189
00:11:04,000 --> 00:11:06,000
So they're able to give up.

190
00:11:06,000 --> 00:11:08,000
Even the Suta Pandan is able to give up.

191
00:11:08,000 --> 00:11:12,000
Finally, Banya, which means it's usually translated as wisdom.

192
00:11:12,000 --> 00:11:15,000
But here means it couldn't just be understood

193
00:11:15,000 --> 00:11:18,000
to mean having seen clearly.

194
00:11:18,000 --> 00:11:22,000
The highest type of wisdom is just the realization of Nibana.

195
00:11:22,000 --> 00:11:24,000
But there is much more wisdom involved there.

196
00:11:24,000 --> 00:11:27,000
The other very important wisdom is the realization of suffering

197
00:11:27,000 --> 00:11:29,000
and the cause of suffering.

198
00:11:29,000 --> 00:11:32,000
The realization is that everything is worthless.

199
00:11:32,000 --> 00:11:34,000
When you realize that nothing is worth clinging to this,

200
00:11:34,000 --> 00:11:37,000
it's when the mind is released.

201
00:11:37,000 --> 00:11:39,000
Those are the seven great treasures.

202
00:11:39,000 --> 00:11:41,000
And he scolds Saka and says,

203
00:11:41,000 --> 00:11:42,000
get out of here.

204
00:11:42,000 --> 00:11:43,000
Who are you?

205
00:11:43,000 --> 00:11:45,000
Who do you think you are asking me?

206
00:11:45,000 --> 00:11:46,000
This is ridiculous.

207
00:11:46,000 --> 00:11:50,000
There's no way I could possibly have any interest

208
00:11:50,000 --> 00:11:51,000
in those things.

209
00:11:51,000 --> 00:11:54,000
So Saka is delighted.

210
00:11:54,000 --> 00:11:56,000
And he goes to see the Buddha and the Buddha.

211
00:11:56,000 --> 00:11:57,000
And he tells the Buddha and the Buddha.

212
00:11:57,000 --> 00:12:00,000
And then Buddha says, oh, even if you'd offered him

213
00:12:00,000 --> 00:12:03,000
a hundred or a thousand times more than you had,

214
00:12:03,000 --> 00:12:05,000
there's no way you could have...

215
00:12:05,000 --> 00:12:07,000
Someone like him, like Super Buddha,

216
00:12:07,000 --> 00:12:15,000
could never become enticed to say such a thing.

217
00:12:15,000 --> 00:12:17,000
So then the story goes on.

218
00:12:17,000 --> 00:12:24,000
It talks about how he gets killed by a female...

219
00:12:24,000 --> 00:12:29,000
A female cow who is actually not a cow.

220
00:12:29,000 --> 00:12:31,000
She's an ogre.

221
00:12:31,000 --> 00:12:33,000
And she was in the past life.

222
00:12:33,000 --> 00:12:37,000
He and some other guys actually raped her

223
00:12:37,000 --> 00:12:39,000
and stole her treasure.

224
00:12:39,000 --> 00:12:41,000
It was a terrible story, but not really that interest.

225
00:12:41,000 --> 00:12:42,000
Much interest to us.

226
00:12:42,000 --> 00:12:45,000
What's interesting is then the monks ask,

227
00:12:45,000 --> 00:12:48,000
so how did Super Buddha become a leper?

228
00:12:48,000 --> 00:12:50,000
And the Buddha tells the story.

229
00:12:50,000 --> 00:12:51,000
It's actually brief.

230
00:12:51,000 --> 00:12:53,000
I think he just goes...

231
00:12:53,000 --> 00:12:55,000
He doesn't even tell the story very detailed.

232
00:12:55,000 --> 00:12:57,000
It just says, oh, in a past life,

233
00:12:57,000 --> 00:13:02,000
he was a Brahmin or something.

234
00:13:02,000 --> 00:13:06,000
And he saw a private Buddha, a fully enlightened Buddha,

235
00:13:06,000 --> 00:13:09,000
and he spit on him.

236
00:13:09,000 --> 00:13:13,000
He was looking at this guy and coming for arms,

237
00:13:13,000 --> 00:13:17,000
and he thought, well, what a disgusting, terrible person.

238
00:13:17,000 --> 00:13:22,000
There's a smelly recluse who is coming and begging for arms.

239
00:13:22,000 --> 00:13:25,000
And he spit on him in a past life.

240
00:13:25,000 --> 00:13:27,000
And because of that karma,

241
00:13:27,000 --> 00:13:28,000
where's the key?

242
00:13:28,000 --> 00:13:30,000
Because of that karma, he became a leper

243
00:13:30,000 --> 00:13:31,000
and then the Buddha gives this verse.

244
00:13:31,000 --> 00:13:33,000
And so the verse is actually related to the fact

245
00:13:33,000 --> 00:13:36,000
that Super Buddha became a leper

246
00:13:36,000 --> 00:13:38,000
because of his bad deeds.

247
00:13:38,000 --> 00:13:40,000
So we have two aspects to this story.

248
00:13:40,000 --> 00:13:42,000
To this verse, there's the story,

249
00:13:42,000 --> 00:13:45,000
which actually deals with a very positive subject.

250
00:13:45,000 --> 00:13:48,000
And then there's the very brief thing at the end,

251
00:13:48,000 --> 00:13:50,000
which deals with the negative aspect.

252
00:13:50,000 --> 00:13:53,000
And that's where the verse comes in.

253
00:13:53,000 --> 00:13:58,000
So we can take two lessons from this passage.

254
00:13:58,000 --> 00:14:03,000
The first is that nothing compares to true understanding

255
00:14:03,000 --> 00:14:05,000
and true wisdom.

256
00:14:05,000 --> 00:14:08,000
True, nothing compares to the treasures inside.

257
00:14:08,000 --> 00:14:14,000
That obviously nothing can compare to our own self-realization.

258
00:14:14,000 --> 00:14:17,000
We are the only thing that can make us happy.

259
00:14:17,000 --> 00:14:20,000
Things in the world around us can never hope to satisfy us.

260
00:14:20,000 --> 00:14:23,000
Any treasures, any pleasures that we may gain

261
00:14:23,000 --> 00:14:25,000
in the world around us,

262
00:14:25,000 --> 00:14:31,000
any possessions or attainments that are external,

263
00:14:31,000 --> 00:14:35,000
can never compare to the great wealth and attainment

264
00:14:35,000 --> 00:14:38,000
that we gain within to our own development

265
00:14:38,000 --> 00:14:45,000
and our own practice, our own realization of the truth.

266
00:14:45,000 --> 00:14:49,000
That's the first lesson we take away from this in the second lesson.

267
00:14:49,000 --> 00:14:51,000
And I just touched on it briefly,

268
00:14:51,000 --> 00:14:53,000
but is in the actual verse itself.

269
00:14:53,000 --> 00:15:00,000
And it's a reminder for all of us

270
00:15:00,000 --> 00:15:03,000
on the downfalls of evil deeds

271
00:15:03,000 --> 00:15:07,000
that if we do get involved with unholesomeness.

272
00:15:07,000 --> 00:15:09,000
Even though it may seem sweet

273
00:15:09,000 --> 00:15:13,000
and there's another verse that talks about how sweet

274
00:15:13,000 --> 00:15:16,000
our bad deeds can seem when we do them.

275
00:15:16,000 --> 00:15:21,000
They're cultivating either aversion or attachment.

276
00:15:21,000 --> 00:15:26,000
So a person who spits on others is obviously corrupting their minds.

277
00:15:26,000 --> 00:15:30,000
There are such people in the world who are so arrogant

278
00:15:30,000 --> 00:15:33,000
and reactionary

279
00:15:33,000 --> 00:15:41,000
and unable to deal with or to cope with that which is unpleasant

280
00:15:41,000 --> 00:15:46,000
or that which is, in this case actually that which is pure

281
00:15:46,000 --> 00:15:48,000
because they're so impure.

282
00:15:48,000 --> 00:15:54,000
It is unable to even stand the sight of an enlightened being

283
00:15:54,000 --> 00:15:58,000
because he himself was so impure.

284
00:15:58,000 --> 00:16:01,000
Of course this is, it's really natural

285
00:16:01,000 --> 00:16:06,000
and the mind is so preoccupied for example with sensual pleasures.

286
00:16:06,000 --> 00:16:08,000
So this person probably would have been a rich,

287
00:16:08,000 --> 00:16:09,000
super boon in the past.

288
00:16:09,000 --> 00:16:12,000
They probably would have been rich and had great luxury

289
00:16:12,000 --> 00:16:15,000
and great satisfaction in his life.

290
00:16:15,000 --> 00:16:17,000
Probably everyone was doing what he asked.

291
00:16:17,000 --> 00:16:21,000
He was always getting what he wanted and then he's challenged

292
00:16:21,000 --> 00:16:26,000
by this enlightened being who obviously wants nothing

293
00:16:26,000 --> 00:16:28,000
and there's that piece with himself

294
00:16:28,000 --> 00:16:31,000
and it's a direct challenge to his whole way of life.

295
00:16:31,000 --> 00:16:36,000
It's visceral when you see such people who are a challenge,

296
00:16:36,000 --> 00:16:40,000
the idea of that there might be something wrong with

297
00:16:40,000 --> 00:16:43,000
or there might be something lacking in one's life.

298
00:16:43,000 --> 00:16:49,000
It's a real threat because we build our whole lives up on the belief

299
00:16:49,000 --> 00:16:52,000
that sensuality is going to satisfy us

300
00:16:52,000 --> 00:16:54,000
and that arrogance is going to serve us well.

301
00:16:54,000 --> 00:16:59,000
That our pride and our passion and all of these are ambition.

302
00:16:59,000 --> 00:17:03,000
All of these things, our beliefs and our religions and so on

303
00:17:03,000 --> 00:17:06,000
are going to satisfy us.

304
00:17:06,000 --> 00:17:09,000
And when this is threatened, unless it's really true

305
00:17:09,000 --> 00:17:12,000
that we found something, unless that which we hold on

306
00:17:12,000 --> 00:17:16,000
to is really and truly satisfying, then it shakes us.

307
00:17:16,000 --> 00:17:18,000
It upsets us.

308
00:17:18,000 --> 00:17:23,000
And so as a result, he was evil and mean to this enlightened being.

309
00:17:23,000 --> 00:17:26,000
And the Buddha said, this is the point.

310
00:17:26,000 --> 00:17:30,000
Something is called an evil deed when it brings bitter fruit.

311
00:17:30,000 --> 00:17:33,000
The point is that which brings fruit,

312
00:17:33,000 --> 00:17:37,000
that which brings pain and suffering.

313
00:17:37,000 --> 00:17:39,000
Again, we're talking about past lives,

314
00:17:39,000 --> 00:17:45,000
but as the thing about Buddhism is there is no real past life.

315
00:17:45,000 --> 00:17:48,000
It's not an idea of a past life because in this life,

316
00:17:48,000 --> 00:17:50,000
it obviously holds true.

317
00:17:50,000 --> 00:17:55,000
It's a fact that you do bad things and your mind becomes corrupt.

318
00:17:55,000 --> 00:17:56,000
It's not a good for you.

319
00:17:56,000 --> 00:18:00,000
It's not like you kill someone and poof you, you're murdered.

320
00:18:00,000 --> 00:18:04,000
And actually, it's not like that with past lives as well.

321
00:18:04,000 --> 00:18:09,000
The Buddha was often, or the Buddha remarked on more than one occasion.

322
00:18:09,000 --> 00:18:11,000
Karma is not just one to one.

323
00:18:11,000 --> 00:18:13,000
It's not like you kill someone in your past life

324
00:18:13,000 --> 00:18:15,000
and you get murdered in this life.

325
00:18:15,000 --> 00:18:19,000
But the bad intentions that you have don't get erased.

326
00:18:19,000 --> 00:18:22,000
They're built up in this life and you can see it.

327
00:18:22,000 --> 00:18:23,000
They change who you are.

328
00:18:23,000 --> 00:18:25,000
They affect your judgment.

329
00:18:25,000 --> 00:18:30,000
And they interact with all the other causes and effects in the world.

330
00:18:30,000 --> 00:18:32,000
And they color your judgment.

331
00:18:32,000 --> 00:18:35,000
It doesn't mean you don't still have the ability to decide

332
00:18:35,000 --> 00:18:37,000
and make decisions, but they color it.

333
00:18:37,000 --> 00:18:42,000
They change your likelihood of you making one decision over another.

334
00:18:42,000 --> 00:18:43,000
And that's karma.

335
00:18:43,000 --> 00:18:45,000
It changes you.

336
00:18:45,000 --> 00:18:48,000
We aren't unaffected by the things that we do.

337
00:18:48,000 --> 00:18:53,000
So a person who does evil in this life or in a past life

338
00:18:53,000 --> 00:19:01,000
is going to, in some way, have to have to meet with.

339
00:19:01,000 --> 00:19:04,000
Have to meet with some effect.

340
00:19:04,000 --> 00:19:08,000
Or it's going to change the future outcomes.

341
00:19:08,000 --> 00:19:12,000
Now, the idea of why the bitter fruit last from one life

342
00:19:12,000 --> 00:19:17,000
into the next, and this is often a cause of skepticism

343
00:19:17,000 --> 00:19:21,000
or confusion for people at least,

344
00:19:21,000 --> 00:19:26,000
is actually the moment of death is a very important point,

345
00:19:26,000 --> 00:19:28,000
but only conceptually.

346
00:19:28,000 --> 00:19:32,000
Only conceptually, only because we've built up this artificial

347
00:19:32,000 --> 00:19:33,000
course body.

348
00:19:33,000 --> 00:19:37,000
The body is actually an artificial construction that we've created.

349
00:19:37,000 --> 00:19:41,000
It doesn't make sense, even if you just think about it.

350
00:19:41,000 --> 00:19:47,000
The idea of why we're here and why we're born into the human realm.

351
00:19:47,000 --> 00:19:51,000
It's actually something that evolution talks about this as well.

352
00:19:51,000 --> 00:19:56,000
The idea of natural selection talks about how it's been selected

353
00:19:56,000 --> 00:20:02,000
over time, just not randomly, but by trial and error.

354
00:20:02,000 --> 00:20:07,000
In Buddhism, we take that concept and apply it fully to the mind as well.

355
00:20:07,000 --> 00:20:10,000
The mind has been doing natural selection.

356
00:20:10,000 --> 00:20:14,000
The mind has been going according to trial and error and it's been building up

357
00:20:14,000 --> 00:20:17,000
until we finally built up this physical body,

358
00:20:17,000 --> 00:20:20,000
which now seems to serve us fairly well,

359
00:20:20,000 --> 00:20:22,000
and so we continue on with it.

360
00:20:22,000 --> 00:20:27,000
We, from lifetime after lifetime, we're repeatedly born as a human

361
00:20:27,000 --> 00:20:31,000
or maybe as an animal or sometimes in hell,

362
00:20:31,000 --> 00:20:34,000
sometimes in heaven, but mainly always coming back to this form

363
00:20:34,000 --> 00:20:38,000
that we've built up and has evolved over time.

364
00:20:38,000 --> 00:20:42,000
But the problem with it is that it's like a wave,

365
00:20:42,000 --> 00:20:45,000
so it builds up, builds up, it builds up, and then it crashes,

366
00:20:45,000 --> 00:20:48,000
and you're left with nothing, and you have to start all over again.

367
00:20:48,000 --> 00:20:53,000
I was opposed to some other states, like being an angel or

368
00:20:53,000 --> 00:20:57,000
not even being in hell, which transfers immediately.

369
00:20:57,000 --> 00:21:02,000
These are states of where the shift is fluid,

370
00:21:02,000 --> 00:21:06,000
with a human, you build something up, and then it's totally erased.

371
00:21:06,000 --> 00:21:11,000
So that moment of death is incredibly important,

372
00:21:11,000 --> 00:21:15,000
and at that moment you're left without the body,

373
00:21:15,000 --> 00:21:17,000
without the physical world, and without everything

374
00:21:17,000 --> 00:21:21,000
that you've built up externally to support you.

375
00:21:21,000 --> 00:21:24,000
So again, why internal wealth is such a great thing,

376
00:21:24,000 --> 00:21:28,000
and external wealth is, of course, useless and meaningless,

377
00:21:28,000 --> 00:21:31,000
because all of that deserts you.

378
00:21:31,000 --> 00:21:35,000
All of that was probably a great help in stopping

379
00:21:35,000 --> 00:21:37,000
bad karma from bringing its results,

380
00:21:37,000 --> 00:21:40,000
so you can kill and you can steal and you can do all sorts of bad things

381
00:21:40,000 --> 00:21:43,000
as long as you have power and influence externally.

382
00:21:43,000 --> 00:21:45,000
All of the good things in the world help you at that time,

383
00:21:45,000 --> 00:21:48,000
but that disappears, that's cut off when your body dies.

384
00:21:48,000 --> 00:21:50,000
You're cut off from the world, you can't move,

385
00:21:50,000 --> 00:21:54,000
you can't talk, you can't interact with the world.

386
00:21:54,000 --> 00:21:58,000
But what's left is the mind, and so all of the good and bad

387
00:21:58,000 --> 00:22:01,000
qualities in the mind, these come to the front,

388
00:22:01,000 --> 00:22:03,000
and this is all that you are left with,

389
00:22:03,000 --> 00:22:06,000
and these things that have been influencing,

390
00:22:06,000 --> 00:22:09,000
but we're on the sort of on the backburn,

391
00:22:09,000 --> 00:22:13,000
or took a backseat role when compared with the body.

392
00:22:13,000 --> 00:22:18,000
Now they're in the front, and so if you're a corrupt and evil person,

393
00:22:18,000 --> 00:22:20,000
this is where they're able to have fruit.

394
00:22:20,000 --> 00:22:24,000
So this is why people, this is why it's so much more evident

395
00:22:24,000 --> 00:22:27,000
at the moment of death and why we say that people are born sick

396
00:22:27,000 --> 00:22:30,000
and born ill and born into bad situations.

397
00:22:30,000 --> 00:22:35,000
Had an interesting conversation a couple of days ago with someone who said,

398
00:22:35,000 --> 00:22:41,000
they talked to someone who had complained about how

399
00:22:41,000 --> 00:22:46,000
it seemed like they were being blamed

400
00:22:46,000 --> 00:22:48,000
for the bad things that happened to them.

401
00:22:48,000 --> 00:22:52,000
This person had gone through the killing fields in Cambodia,

402
00:22:52,000 --> 00:22:57,000
and they were complaining about how Buddhism is really kind of

403
00:22:57,000 --> 00:22:59,000
unhelpful for people.

404
00:22:59,000 --> 00:23:02,000
It would be much more helpful, and the idea

405
00:23:02,000 --> 00:23:06,000
in a theistic religion, it's much more helpful to be forgiven,

406
00:23:06,000 --> 00:23:10,000
and have God say, no, you're not responsible for these bad things.

407
00:23:10,000 --> 00:23:15,000
It was a tester or so on and so on, and all you have to do is believe.

408
00:23:15,000 --> 00:23:17,000
And you'll go to heaven.

409
00:23:17,000 --> 00:23:20,000
You'll be forgiven and you're certainly not responsible.

410
00:23:20,000 --> 00:23:25,000
Which, honestly, as I said at the time,

411
00:23:25,000 --> 00:23:31,000
it's much more a question of what's true.

412
00:23:31,000 --> 00:23:35,000
It's all nice and good if a person is a mass murderer.

413
00:23:35,000 --> 00:23:38,000
It's all nice and good to say, oh, it's okay.

414
00:23:38,000 --> 00:23:40,000
We'll let you off. You don't worry about it.

415
00:23:40,000 --> 00:23:45,000
If you've done bad deeds, we're not going to get upset at you.

416
00:23:45,000 --> 00:23:47,000
Of course, that's actually a good thing.

417
00:23:47,000 --> 00:23:50,000
It's a good thing not to get upset at someone who has done bad

418
00:23:50,000 --> 00:23:53,000
deeds. You should actually feel sorry for them.

419
00:23:53,000 --> 00:23:57,000
But it doesn't stop the fact that they've done something terrible.

420
00:23:57,000 --> 00:24:01,000
And so if a person has done bad things in their past life,

421
00:24:01,000 --> 00:24:07,000
it's worth telling them the truth.

422
00:24:07,000 --> 00:24:10,000
It's not better to hide that from them.

423
00:24:10,000 --> 00:24:14,000
But as I thought about it, you know, more importantly,

424
00:24:14,000 --> 00:24:25,000
is the fact that it really is a system that makes sense.

425
00:24:25,000 --> 00:24:30,000
It's not like we made the system up,

426
00:24:30,000 --> 00:24:34,000
whereas it seems that arbitrarily you have all these religions

427
00:24:34,000 --> 00:24:38,000
that are claiming something and claiming to be able to take your pain away

428
00:24:38,000 --> 00:24:42,000
and have this magic place in the sky that you can go to

429
00:24:42,000 --> 00:24:46,000
that erases all your troubles, which is, you know,

430
00:24:46,000 --> 00:24:49,000
any good salesman will have a pitch like that.

431
00:24:49,000 --> 00:24:54,000
Buddhism doesn't have that, but Buddhism offers an explanation

432
00:24:54,000 --> 00:24:57,000
that is very much in line with experience

433
00:24:57,000 --> 00:25:00,000
and very much in line with even the physical world,

434
00:25:00,000 --> 00:25:04,000
and the fact that just like the physical world,

435
00:25:04,000 --> 00:25:08,000
which seems very much according to go according to cause and effect,

436
00:25:08,000 --> 00:25:11,000
the mind as well goes according to cause and effect,

437
00:25:11,000 --> 00:25:13,000
and the mind is merely a part of the system.

438
00:25:13,000 --> 00:25:16,000
And so bad deeds lead to bad result,

439
00:25:16,000 --> 00:25:21,000
a person who is experiencing bad deeds.

440
00:25:21,000 --> 00:25:25,000
In fact, it seems quite liberating to think

441
00:25:25,000 --> 00:25:29,000
that all of the bad things that I've done have a reason,

442
00:25:29,000 --> 00:25:32,000
and there's no mystery,

443
00:25:32,000 --> 00:25:36,000
whereas if I were to say that God is the reason why I was born crippled

444
00:25:36,000 --> 00:25:41,000
or I was born disabled or is the reason why in the killing fields,

445
00:25:41,000 --> 00:25:45,000
these children six months old babies were smashed against trees

446
00:25:45,000 --> 00:25:50,000
and skewered on bayonets and thrown live into fires,

447
00:25:50,000 --> 00:25:57,000
I don't understand how you could find the explanation

448
00:25:57,000 --> 00:26:00,000
that God was doing this to be preferable,

449
00:26:00,000 --> 00:26:05,000
for example, as much more preferable to have an answer

450
00:26:05,000 --> 00:26:09,000
and to be able to make some kind of sense out of the situation,

451
00:26:09,000 --> 00:26:13,000
where it would make perfectly sense if someone was doing all those horrible things,

452
00:26:13,000 --> 00:26:19,000
like skearing babies on bayonets, that they should actually come to experience

453
00:26:19,000 --> 00:26:23,000
or to realize the fruit, the bitter fruit of their deeds,

454
00:26:23,000 --> 00:26:26,000
which Buddhism offers, whether you believe it or not,

455
00:26:26,000 --> 00:26:29,000
Buddhism offers something that it's not sugar coated

456
00:26:29,000 --> 00:26:32,000
and it doesn't make things more pleasant necessarily,

457
00:26:32,000 --> 00:26:36,000
but it sure as heck makes a lot of sense internally,

458
00:26:36,000 --> 00:26:40,000
whether you believe it or not, it is cogent

459
00:26:40,000 --> 00:26:43,000
and it's a wonderful system.

460
00:26:43,000 --> 00:26:48,000
It's a wonderful theory, even if it's not true,

461
00:26:48,000 --> 00:26:50,000
even if it weren't true.

462
00:26:50,000 --> 00:26:52,000
It's the theory that even in that case,

463
00:26:52,000 --> 00:26:55,000
even if you put aside whether you believe it or not,

464
00:26:55,000 --> 00:26:57,000
it's a theory that makes, I would say, the most sense,

465
00:26:57,000 --> 00:27:03,000
and is actually able to explain a heck of a lot about the universe

466
00:27:03,000 --> 00:27:08,000
and to provide a cogent explanation of the way things work.

467
00:27:08,000 --> 00:27:12,000
So a very good verse as they all are

468
00:27:12,000 --> 00:27:16,000
and I'm not opportunity to talk about some of these important concepts.

469
00:27:16,000 --> 00:27:20,000
So I hope that's useful. Thanks for tuning in, sorry I've been away.

470
00:27:20,000 --> 00:27:22,000
Let's see if I can keep this up.

471
00:27:22,000 --> 00:27:25,000
Thank you all for tuning in and for your encouragement to keep going.

472
00:27:25,000 --> 00:27:28,000
I wish you all the best to find true peace, happiness,

473
00:27:28,000 --> 00:27:53,000
and freedom from suffering. Thank you for tuning in.

