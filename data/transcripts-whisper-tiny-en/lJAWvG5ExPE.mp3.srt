1
00:00:00,000 --> 00:00:06,000
What is the cure for complacency in practice?

2
00:00:06,000 --> 00:00:09,000
Good question.

3
00:00:09,000 --> 00:00:13,000
Urgency, the sense of urgency.

4
00:00:13,000 --> 00:00:19,000
Association, it's always about association with people who have a sense of urgency.

5
00:00:19,000 --> 00:00:24,000
When you're alone, it's very easy to become complacent and not only complacent, but somehow

6
00:00:24,000 --> 00:00:33,000
resigned because practice is difficult, and without support, without mutual, interactive support,

7
00:00:33,000 --> 00:00:43,000
it's easy to get weighed down, bogged down by the central world and people who are in distant

8
00:00:43,000 --> 00:00:44,000
client of practice.

9
00:00:44,000 --> 00:00:46,000
You start to say, well, they're not practice.

10
00:00:46,000 --> 00:00:50,000
At least I'm as good as everybody else or so on.

11
00:00:50,000 --> 00:00:52,000
So that's the biggest thing.

12
00:00:52,000 --> 00:00:54,000
It's association with good people.

13
00:00:54,000 --> 00:01:04,000
Part of that can be listening to talks or reading books, studying, getting teachings that are inclined.

14
00:01:04,000 --> 00:01:07,000
A big one is to practice those meditations.

15
00:01:07,000 --> 00:01:19,000
But here meditation, just in the sense of reflections, recollections of death,

16
00:01:19,000 --> 00:01:28,000
the three characteristics in permanent suffering in non-self, in a conventional or conceptual

17
00:01:28,000 --> 00:01:29,000
context.

18
00:01:29,000 --> 00:01:33,000
The idea that everything is impermanent and all of these things that we're clinging to

19
00:01:33,000 --> 00:01:36,000
are going to have to change and so on.

20
00:01:36,000 --> 00:01:41,000
The realization that nothing is stable and that at any moment we could be subject to great

21
00:01:41,000 --> 00:01:54,000
suffering from any number of causes, sickness, accident, even natural disasters.

22
00:01:54,000 --> 00:02:04,000
A human made disasters, accidents, all sorts of things, robberies, crime, punishment,

23
00:02:04,000 --> 00:02:07,000
many, many anything could happen.

24
00:02:07,000 --> 00:02:11,000
And so thinking about these sorts of things, meditating on them, meditating on the inevitability

25
00:02:11,000 --> 00:02:17,000
of death, meditating on even, say, the repulsiveness of the body,

26
00:02:17,000 --> 00:02:25,000
meditating on the nature of the body parts as being not as desirable as we think they are,

27
00:02:25,000 --> 00:02:31,000
allows us to cultivate a sense of urgency and the need for practice.

28
00:02:31,000 --> 00:02:33,000
So you can read up about it.

29
00:02:33,000 --> 00:02:38,000
I would read the Visudhi manga, the parts about these various meditations and reading

30
00:02:38,000 --> 00:02:43,000
about that helps to cultivate a sense of urgency.

31
00:02:43,000 --> 00:02:47,000
But yeah, the biggest thing is to find a meditation center, do a meditation course,

32
00:02:47,000 --> 00:02:54,000
stay with the teacher, hang out with monks and meditators, meditators, meditators.

33
00:02:54,000 --> 00:02:57,000
And do the best you can.

34
00:02:57,000 --> 00:03:03,000
I mean, you're not going to be the perfect meditator where you're always gung-ho on the practice

35
00:03:03,000 --> 00:03:06,000
and sometimes you're going to be down about it.

36
00:03:06,000 --> 00:03:20,000
So try your best to cultivate as much impetus to practice as you can and do what you can.

37
00:03:20,000 --> 00:03:30,000
Some help there, but ultimately in the end it's hard and twisted and sometimes round about route.

38
00:03:30,000 --> 00:03:37,000
So effort will come the more you practice and will come in time.

39
00:03:37,000 --> 00:03:41,000
I think we often, it's true many people, very easy to become complacent.

40
00:03:41,000 --> 00:04:01,000
And so worth all of us, worth considering the means of becoming more energetic and more inclined and invigorated towards a practice.

41
00:04:01,000 --> 00:04:06,000
My art teacher, Ajentang, he was asked if I was sitting there and I was translating and someone said,

42
00:04:06,000 --> 00:04:14,000
you know, how do I do this? And he said, well, you just think like in the world, if you're in a worldly context.

43
00:04:14,000 --> 00:04:19,000
If you don't work hard, then you don't get paid.

44
00:04:19,000 --> 00:04:23,000
You can't succeed in the world if you don't work hard.

45
00:04:23,000 --> 00:04:25,000
How could it be any different in the practice?

46
00:04:25,000 --> 00:04:35,000
The problem is that people think of the dhamma as being some kind of hobby or some kind of side activity.

47
00:04:35,000 --> 00:04:38,000
Something that we do on the weekends maybe.

48
00:04:38,000 --> 00:04:44,000
And so by not taking it seriously, of course, it's just like a worldly pursuit.

49
00:04:44,000 --> 00:04:49,000
If you don't cultivate it, if you don't work hard at it, you can't hope to succeed.

50
00:04:49,000 --> 00:04:54,000
So reflections like that are, I think, quite helpful.

51
00:04:54,000 --> 00:05:10,000
That was his advice.

