1
00:00:00,000 --> 00:00:08,200
Analogy of watching the rising and falling of the abdomen, like a hunter waiting for the

2
00:00:08,200 --> 00:00:12,960
mind to show up with its tendencies, is very useful.

3
00:00:12,960 --> 00:00:18,960
I think it's in the commentaries, it may have been from the Buddha himself.

4
00:00:18,960 --> 00:00:23,840
I think the Buddha used it as well, but the commentaries talk about this.

5
00:00:23,840 --> 00:00:26,720
After a while there is nothing but a pleasant glow.

6
00:00:26,720 --> 00:00:31,080
Does wisdom arise spontaneously or do I need to do more?

7
00:00:31,080 --> 00:00:36,680
Oh, but if it were so easy we could just sit with a pleasant glow and wisdom would arise.

8
00:00:36,680 --> 00:00:47,680
No, that pleasant glow is a problem for you because you find it pleasant.

9
00:00:47,680 --> 00:01:05,600
The single most common hindrance in meditation that we come across is pleasant sensations.

10
00:01:05,600 --> 00:01:10,160
Whenever I go to give a talk somewhere, there will always be at least one person in the audience

11
00:01:10,160 --> 00:01:17,520
who has this problem, who has been practicing maybe for years and feels like they're

12
00:01:17,520 --> 00:01:23,560
not getting anywhere because they come to a situation where there is nothing but a pleasant

13
00:01:23,560 --> 00:01:24,560
glow.

14
00:01:24,560 --> 00:01:29,160
I've answered this question many times on YouTube as well, I'm sure many of you or some

15
00:01:29,160 --> 00:01:35,160
of you have listened to this answer and know what I'm going to say.

16
00:01:35,160 --> 00:01:42,080
The pleasant glow could be classified as biti rapture.

17
00:01:42,080 --> 00:01:46,880
It could be classified as suka happiness.

18
00:01:46,880 --> 00:01:53,040
It could be classified as tranquility, probably not.

19
00:01:53,040 --> 00:01:55,440
It depends exactly what the experience is.

20
00:01:55,440 --> 00:01:57,800
You describe it as a pleasant glow.

21
00:01:57,800 --> 00:02:01,000
I would call that rapture.

22
00:02:01,000 --> 00:02:06,040
I would guess that that's probably not just happiness, but it's kind of a static charge,

23
00:02:06,040 --> 00:02:08,280
this excitement in the mind.

24
00:02:08,280 --> 00:02:15,680
It may be kind of a common excitement, but there's an energy involved with it.

25
00:02:15,680 --> 00:02:22,320
Whatever you classify it as, it's an experience that comes as a byproduct of meditation

26
00:02:22,320 --> 00:02:23,560
practice.

27
00:02:23,560 --> 00:02:29,840
It is impermanent, it is unsatisfying in the sense that it can't last forever, and

28
00:02:29,840 --> 00:02:34,720
it's uncontrollable in the sense that you can't turn it on and turn it off.

29
00:02:34,720 --> 00:02:41,000
When you sit down and meditate, meditate sometimes or maybe all the time, it comes by itself.

30
00:02:41,000 --> 00:02:44,320
It's not yours to control.

31
00:02:44,320 --> 00:02:51,680
You happen to be in a situation where it comes often based on the activities that you undertake,

32
00:02:51,680 --> 00:02:53,560
but it is not under your control.

33
00:02:53,560 --> 00:03:05,320
To confirm that, you can give up your attachment to it and start to say to yourself, happy,

34
00:03:05,320 --> 00:03:14,280
happy, or liking, liking, or feeling, feeling, however it presents itself to you.

35
00:03:14,280 --> 00:03:23,560
Find if you're persistent in acknowledging both the happiness and the liking of it, just

36
00:03:23,560 --> 00:03:27,800
like everything else, it begins to disappear.

37
00:03:27,800 --> 00:03:31,080
When you don't do that, you're actually encouraging it.

38
00:03:31,080 --> 00:03:34,320
The question is, is that a bad thing?

39
00:03:34,320 --> 00:03:39,640
It's a bad thing if you want to gain wisdom, because no wisdom will not arise unless you're

40
00:03:39,640 --> 00:03:44,680
being objective, unless you have a clarity of mind, which you're lacking by enjoying this

41
00:03:44,680 --> 00:03:45,840
pleasant glow.

42
00:03:45,840 --> 00:03:53,640
Nothing wrong with a glow or a feeling or a sense of rapture.

43
00:03:53,640 --> 00:03:59,040
They can actually be quite useful because they provide you with energy and tranquility in

44
00:03:59,040 --> 00:04:00,040
the mind.

45
00:04:00,040 --> 00:04:04,880
But as soon as you begin to find it pleasant and enjoy it, even subtly, usually it is

46
00:04:04,880 --> 00:04:08,840
quite subtle and it doesn't feel like you actually are clinging to it.

47
00:04:08,840 --> 00:04:17,520
But if you're not acknowledging it, then in almost all cases, you're clinging to it.

48
00:04:17,520 --> 00:04:21,640
You can realize this by, at the very least, you have delusion about it, an idea that

49
00:04:21,640 --> 00:04:25,240
it's me, that it's mine.

50
00:04:25,240 --> 00:04:30,200
By acknowledging feeling, feeling, happy, happy, liking, liking, however it appears to

51
00:04:30,200 --> 00:04:35,480
you, you'll see that it ceases and you'll see that it isn't, you'll come to realize that

52
00:04:35,480 --> 00:04:41,560
it isn't actually something that you can depend upon, isn't actually the true, there isn't

53
00:04:41,560 --> 00:04:45,200
the path, it isn't the true goal of meditation.

54
00:04:45,200 --> 00:04:55,080
And that is a piece of wisdom, it's a understanding, so that is wisdom, and that is the

55
00:04:55,080 --> 00:05:01,640
path, and that will lead you closer to freedom from suffering.

56
00:05:01,640 --> 00:05:04,920
Wisdom is not something that you can call, that you can create, it's not something that

57
00:05:04,920 --> 00:05:11,400
you should try to actively cultivate in the sense of thinking or reflecting or puzzling

58
00:05:11,400 --> 00:05:15,880
out reality, asking questions about reality.

59
00:05:15,880 --> 00:05:23,520
It is something that will arise by itself, but it will only arise if you are objective,

60
00:05:23,520 --> 00:05:27,120
if you have the clarity of mind, which you don't have when you're clinging, when you're

61
00:05:27,120 --> 00:05:28,120
enjoying.

62
00:05:28,120 --> 00:05:32,880
So be very, very careful about pleasant sensations, they are not the path, you can, I did

63
00:05:32,880 --> 00:05:38,800
a video about the ten of them some time ago called, what the meditation is not, I think,

64
00:05:38,800 --> 00:05:42,600
and you can watch that if you want to learn about the others as well.

65
00:05:42,600 --> 00:05:46,200
There's lots of good things that can come up that will only get in your, the way of your

66
00:05:46,200 --> 00:06:16,160
practice if you cling to them.

