1
00:00:00,000 --> 00:00:18,000
Good evening everyone, welcome to evening dhamma, so we're continuing on today with

2
00:00:18,000 --> 00:00:34,000
JITA we should be the second of the seven purifications. JITA we should be JITA means mind, so the

3
00:00:34,000 --> 00:00:45,200
second one is actually the purification of of mind or the mind. And it's important to explain

4
00:00:45,200 --> 00:00:51,200
why this is the second purification instead of the seventh because it seems like well

5
00:00:51,200 --> 00:01:00,480
wouldn't that be the last step to purify the mind? The thing is in Buddhism in an ultimate sense

6
00:01:02,080 --> 00:01:10,800
the mind doesn't exist. So when we talk about purification of mind in an ultimate sense we mean the

7
00:01:10,800 --> 00:01:21,760
purification of mind states moments and your mind can be pure. Those mind states can be pure

8
00:01:23,520 --> 00:01:30,880
even for an unenlightened individual. It's not the last step, it's the first step

9
00:01:32,560 --> 00:01:35,280
and so it's not a goal in itself but it's a means.

10
00:01:35,280 --> 00:01:43,120
As when those mind state when the mind states are pure when you have moments of mental purity

11
00:01:44,400 --> 00:01:48,320
it's not the end. It doesn't mean your mind is not going to become impure again.

12
00:01:51,120 --> 00:02:01,760
But if applied in the right way, the purification of those mind states can lead

13
00:02:01,760 --> 00:02:18,560
to a shift in the nature of one's mental makeup to the point where a mental purity becomes more

14
00:02:18,560 --> 00:02:26,400
common and eventually becomes a permanent state. So the mind states that arise are always pure.

15
00:02:26,400 --> 00:02:33,680
But in the beginning it's a means. That purity of mind will allow us to see clearly.

16
00:02:35,920 --> 00:02:42,000
And so whenever we talk about meditation and this is a debate between some of the meditation,

17
00:02:42,000 --> 00:02:49,360
the genres and we pass on the, we should always keep that in mind that the whole purpose

18
00:02:49,360 --> 00:02:56,240
of sitting down to meditate in the first place is to attain this purity of mind

19
00:02:57,760 --> 00:03:05,440
to attain states and extended states of mental purity.

20
00:03:08,480 --> 00:03:16,640
And I've said this recently in our talk about the full noble path but it bears repeating

21
00:03:16,640 --> 00:03:25,040
that this is one part of the path where it appears quite clear. It's not just an opinion and it's

22
00:03:25,040 --> 00:03:36,880
not just an interpretation but it's quite clearly true that there are many ways and there were

23
00:03:36,880 --> 00:03:45,520
many ways and the Buddha taught in many ways by which one can cultivate, did the vicinity, purity of

24
00:03:45,520 --> 00:03:55,520
mind. To simulate, we oversimplify it perhaps, we simplify it into two ways. There are two ways

25
00:03:55,520 --> 00:04:02,160
to go about this. The first is to practice tranquility meditation. When you get really good at that,

26
00:04:02,160 --> 00:04:08,560
you're able to keep your mind pure and then you can shift that purity to focus on reality.

27
00:04:08,560 --> 00:04:14,800
But this is the kind of practice that isn't focused on reality. You focus on a concept,

28
00:04:14,800 --> 00:04:19,280
get your mind really tranquil and then okay, then apply it to ultimate reality.

29
00:04:20,400 --> 00:04:26,320
That was a common way it seems. It was a common sort of practice or many practices that were

30
00:04:26,320 --> 00:04:34,160
acknowledged to not lead to enlightenment and to the meta for example. They won't enlighten you but

31
00:04:34,800 --> 00:04:40,960
if you practice it diligently it leads you to the mental purity that can then be applied

32
00:04:42,320 --> 00:04:44,720
to focus on reality and lead to enlightenment.

33
00:04:49,200 --> 00:04:55,600
And many meditations like that. The other way of course is to begin by practicing sateemi

34
00:04:55,600 --> 00:05:04,400
practicing mindfulness of ultimate reality. Focusing on the problematic states, pain, stress,

35
00:05:05,200 --> 00:05:12,720
even defilement. And as a result of focusing on them, they slowly build up this mental purity

36
00:05:12,720 --> 00:05:20,320
but at the same time is building up moments, which is what we do, moments of mental

37
00:05:20,320 --> 00:05:26,640
purity rather than long extended periods. But those moments, because they're focused on reality,

38
00:05:27,440 --> 00:05:34,320
it actually saves time because those moments allow you in this glimpse into reality.

39
00:05:34,320 --> 00:05:39,200
And it takes time. You have to, in the beginning you can't see clearly even though you're focusing

40
00:05:39,200 --> 00:05:46,240
on reality because unlike with tranquility meditation your mind is not clear. There's not this pure

41
00:05:46,240 --> 00:05:53,440
reality of mind. So it takes time even though you're looking to see. And with samma

42
00:05:53,440 --> 00:05:59,840
temptation you're looking but you're looking at the wrong thing or you can see but you're not

43
00:05:59,840 --> 00:06:04,480
looking at the point. So the past and the meditation you're looking but in the beginning you

44
00:06:04,480 --> 00:06:10,240
can't see. With samma temptation you're not looking. You're looking at something else but you

45
00:06:10,240 --> 00:06:16,880
can see. You learn to see very quickly. So either way it has to come together in the end.

46
00:06:18,160 --> 00:06:20,560
And in the end what we are talking about is

47
00:06:24,800 --> 00:06:30,240
momentary concentration that's focused on ultimate reality. There has to be this.

48
00:06:31,200 --> 00:06:37,120
The jana is whenever you talk about these absorbed states. The mundane janas cannot

49
00:06:37,120 --> 00:06:46,320
but depending what you mean by jana in fact but trance states that are focused on a single object

50
00:06:46,320 --> 00:06:54,240
can't help you. You have to give them up relinquish them and focus what very well might be

51
00:06:54,240 --> 00:07:00,000
called the jana but focus your attention on reality to see impermanence suffering

52
00:07:00,000 --> 00:07:11,680
nonself to see the four noble truths. But when we describe these two paths there's a lot to

53
00:07:11,680 --> 00:07:20,560
say about the various ways and it's worth going quickly over just for the sake of completion

54
00:07:20,560 --> 00:07:30,560
go over the various ways one can practice meditation. The texts talk about 40 types of meditation.

55
00:07:30,560 --> 00:07:37,760
They're the 40 kamatana. Kamatana means meditation. Kamam means activity or action or work.

56
00:07:38,480 --> 00:07:47,200
Tana means bass. There's the basses for work or they're the building of bass or I don't know

57
00:07:47,200 --> 00:07:54,720
how exactly it's translated. Kamatana we translate as meditation. To work that you do.

58
00:07:55,680 --> 00:08:03,920
Something you have to work at in your mind. So 40 of them the first hand of the casino is just

59
00:08:03,920 --> 00:08:11,520
appears to have been a tool that was used. As far as I can think it's not really mentioned

60
00:08:11,520 --> 00:08:17,120
in the original texts doesn't appear that the Buddha talked about them very much if at all.

61
00:08:18,080 --> 00:08:26,400
In the text but it seems clearly to be it's the obvious answer to how to practice rudimentary

62
00:08:26,400 --> 00:08:31,120
meditation. I mean these are the very simplest of meditations meditating on a color,

63
00:08:31,840 --> 00:08:38,160
a light meditating on an element, earth, air, water, fire. They're the most simple and basic

64
00:08:38,160 --> 00:08:44,720
objects and it gives you the idea that you really can focus on anything as long as the object

65
00:08:44,720 --> 00:08:51,920
allows you to gain purity of mind. These ones probably being the best. The fire, a candle flame

66
00:08:51,920 --> 00:08:56,640
that kind of thing. And with these ones when you focus on it you would say to yourself fire,

67
00:08:56,640 --> 00:09:05,280
fire, earth, earth, white, white or so on. And by repeating that to you and to yourself just like

68
00:09:05,280 --> 00:09:11,760
we do with insight practice, your mind becomes focused on the object. But because this object

69
00:09:11,760 --> 00:09:18,400
is stable, satisfying, controllable, unlike our meditation it allows you to feel quite peaceful

70
00:09:18,400 --> 00:09:25,040
and calm. So you're able to see. Problem is you're not looking. White doesn't going to tell you

71
00:09:25,040 --> 00:09:37,280
anything about reality. There's a candle flame. There's 10 of them. One have to have to enumerate them.

72
00:09:37,280 --> 00:09:46,960
The second 10 are asuba. Asuba has to do with thinking about the body and the dead state

73
00:09:48,000 --> 00:09:53,760
and dead body. So the body is in various states of decomposition. We went over this

74
00:09:53,760 --> 00:10:03,440
in our study of the city manga in the book. Asuba is particularly useful for people who are

75
00:10:06,000 --> 00:10:12,240
overwhelmed by lust. Lust for the body, for sexual desire and so on. It's quite common.

76
00:10:13,120 --> 00:10:19,120
So it's a being useful meditation practice. So not only does this focus your mind, it helps

77
00:10:19,120 --> 00:10:27,520
retrain the mind and free the mind temporarily from sexual desire. So you think about the dead

78
00:10:27,520 --> 00:10:35,120
body. It's also good because of, well, you know, it's mostly to do with lotusomeness and seeing

79
00:10:35,120 --> 00:10:40,880
the body out for how disgusting it is. Because as you watch the body decompose and they would

80
00:10:40,880 --> 00:10:46,400
actually do this, they would find the dead body and sometimes people who looked after graveyards

81
00:10:46,400 --> 00:10:52,560
or so and channel grounds would let the monks know, hey, we got a ripe one for you. Come see this

82
00:10:52,560 --> 00:11:00,080
one. This will be good for you monks. Today would go and meditate walking, doing walking meditation

83
00:11:00,080 --> 00:11:09,840
perhaps and just focusing their attention on the corpse. It's a good means of letting go of the

84
00:11:09,840 --> 00:11:16,240
attachment to the body. If you've never looked at it at a dead body or the inside of a dead body,

85
00:11:16,240 --> 00:11:21,360
watch people cutting it open that kind of thing. It's quite a shock to actually see it. It's

86
00:11:21,360 --> 00:11:26,720
one thing to think about it, but it really makes you think about this body and the whole new light.

87
00:11:27,280 --> 00:11:36,400
So it's useful as a meditation in that way. There's 10 different stages. The third set of 10 are

88
00:11:36,400 --> 00:11:44,880
the anu sati. Anu sati really just means object of contemplation sati, right? Being something

89
00:11:44,880 --> 00:11:51,920
you're mindful of. So mindfulness of the Buddha, mindfulness of the Dharma, mindfulness of the Sangha,

90
00:11:53,280 --> 00:11:59,760
mindfulness of morality, thinking about how moral and ethical we are is a good way to calm your

91
00:11:59,760 --> 00:12:05,120
mind down. Chaga, nu sati where you think about all the good gifts you've given all the chair,

92
00:12:05,120 --> 00:12:13,280
how charitable you've been. It really calms the mind down. Day one of sati is apparently a

93
00:12:13,280 --> 00:12:17,760
thing where you think about the angels. It's actually something like focusing on the qualities of

94
00:12:17,760 --> 00:12:26,880
mind. It's an odd one, but it's in there. Modern anu sati is another one when you think about death.

95
00:12:26,880 --> 00:12:38,720
That's a good one that allows you to cultivate a sense of urgency. It really wakes you up. Oh, yes,

96
00:12:38,720 --> 00:12:50,400
I'm going to die. Think about your death. Meditation on death. Chaga got the sati as another.

97
00:12:50,400 --> 00:12:56,720
It's really, this is where you break the body up into its constituent parts, quesa, lomena, katantantu.

98
00:12:56,720 --> 00:13:01,360
If you've ever heard that kind of meditation, the 32 parts of the body. It's another good

99
00:13:02,000 --> 00:13:10,480
good one for people who have problems with sexual desires. Instead of thinking about the dead body,

100
00:13:10,480 --> 00:13:15,200
think about the parts of the living body. And you realize there's nothing really attractive

101
00:13:15,200 --> 00:13:24,480
about the human body. The hair on the hand is like, well, it's all kind of icky. If you've

102
00:13:24,480 --> 00:13:31,680
never, if you don't shower, if you don't wash it, it gets kind of yucky. And then if you look

103
00:13:31,680 --> 00:13:42,320
inside all the stuff inside us, talk about yucky. Disgusting. There's no sugar and spice inside.

104
00:13:42,320 --> 00:13:52,080
And there's anapana sati fits in here. People who practice anapana sati is probably the

105
00:13:52,080 --> 00:13:58,640
by far the most common and the one the Buddha recommended the most. And generally anapana sati

106
00:13:58,640 --> 00:14:04,880
was used to cultivate tranquility first, not inside. So it was meant to calm the mind into enter

107
00:14:04,880 --> 00:14:11,840
into a trance state. And then you could also apply the same technique or the same sort of

108
00:14:11,840 --> 00:14:16,960
technique, shift your focus and focus then on on ultimate reality.

109
00:14:20,640 --> 00:14:25,200
But the point with anapana sati is that you're focused on the concept of the breath.

110
00:14:25,200 --> 00:14:32,000
Breath is just a concept. Breath isn't real. It's real is the the element, the pressure. When you

111
00:14:32,000 --> 00:14:39,440
breathe, it's pressure, it's heat, cold, the cool feeling of the air flowing. There's the elements,

112
00:14:39,440 --> 00:14:47,680
the aspects of experience. But the breath going in and going out, the idea of something going

113
00:14:47,680 --> 00:14:55,520
in and out is just a concept. I'm so of you. Focus on saying in, out, in, out. It's really focused

114
00:14:55,520 --> 00:15:00,880
on a concept because that won't change. In will always be in, out will always be out.

115
00:15:02,320 --> 00:15:07,760
It's kind of splitting hairs because it can go both ways, but it certainly can be simply a means

116
00:15:07,760 --> 00:15:12,320
of cultivating tranquility, which is what it is in this context.

117
00:15:15,120 --> 00:15:20,560
And then there's one called up as samana sati, which means focusing on nibana as an object.

118
00:15:21,760 --> 00:15:25,600
And this I think only works for those people who have experienced nibana, but when you

119
00:15:25,600 --> 00:15:30,560
experience nibana, then when you think about it, it's apparently another way to calm the mind.

120
00:15:30,560 --> 00:15:37,360
I mean, it is calming to think of, of course, because it's such a sublime state.

121
00:15:40,800 --> 00:15:43,680
So that's 30. Then we have the four brahmui hara's.

122
00:15:45,120 --> 00:15:52,080
Maita karunamu di tao pei ka, which is love, compassion, joy, and equanimity.

123
00:15:52,720 --> 00:15:56,960
These are quite popular and work, quite popular and, of course, are really great

124
00:15:56,960 --> 00:16:03,200
meditations in their own right, very useful, and on a daily basis to sort of straighten

125
00:16:03,200 --> 00:16:09,840
out your attitude towards others. But the object is still beings, and because it's beings,

126
00:16:09,840 --> 00:16:14,960
it can't lead you to enlightenment, because those beings don't exist in our experience.

127
00:16:15,600 --> 00:16:20,240
Our experience is not of beings, our experiences of experiences.

128
00:16:20,240 --> 00:16:29,600
So focusing on beings is great to calm the mind, but it's not enough to become enlightened.

129
00:16:31,120 --> 00:16:35,840
Then we have the four arupa kamantanas, which are meditation,

130
00:16:36,800 --> 00:16:41,120
whether they're actually advanced meditations after you've cultivated jana, but

131
00:16:41,920 --> 00:16:47,520
they are separate meditations in themselves. So there's infinite space, someone focusing on infinite

132
00:16:47,520 --> 00:16:55,600
space, infinite consciousness, nothingness, and neither perception or non-perception. These are

133
00:16:56,480 --> 00:17:01,280
advanced meditation techniques, and I don't really want to go into them in much detail.

134
00:17:03,120 --> 00:17:11,600
And the last two are a hari patikulis anya, which is focusing on the repulsiveness of food,

135
00:17:11,600 --> 00:17:16,880
and watching how the food goes into your body, and so on, how it's in the mouth, and

136
00:17:18,080 --> 00:17:26,000
realizing how our attachment to food is also kind of silly, because food is not really that

137
00:17:26,000 --> 00:17:30,640
wonderful, not as wonderful as we think of it. It helps you let go of the attachment,

138
00:17:30,640 --> 00:17:37,920
addiction to food. And finally, dhatu manasika, which are dhatu vyatana kamantanas,

139
00:17:37,920 --> 00:17:46,000
which means to separate the body into elements. And dhatu manasika, dhatu vyatana,

140
00:17:46,640 --> 00:17:52,480
I think is what it's called. It can be used for samatana, but it can also be used for vyatana,

141
00:17:52,480 --> 00:17:58,320
and this is the key, because you can practice any one of these 40 meditation techniques to calm

142
00:17:58,320 --> 00:18:04,320
the mind down. But the visudhi maga does a good job. It goes through all these, and then at the

143
00:18:04,320 --> 00:18:10,240
very end, when it begins to talk about wisdom, it says, someone who practices samata cultivates

144
00:18:10,240 --> 00:18:17,680
one of those 40. Someone who practices vipasana starts with one of them, but focuses on it in such

145
00:18:17,680 --> 00:18:27,360
a way as to see reality. And that one is in the dhatu, the elements. And it says, even one who

146
00:18:27,360 --> 00:18:31,920
has practiced samatana, has to switch and focus on the elements. That's the way to do it.

147
00:18:31,920 --> 00:18:39,360
You can't easily focus on the mind in the beginning of your insight practice. It's not really

148
00:18:39,360 --> 00:18:46,560
clear. It's not accepted as the way of doing things. You probably could, but it's more tricky.

149
00:18:47,360 --> 00:18:51,200
And so the accepted way of doing things is to focus on the body. The body is here,

150
00:18:51,840 --> 00:18:58,080
easy to experience, easy to focus on. And you focus on that aspect of the body that is real,

151
00:18:58,080 --> 00:19:04,560
which is our experiences. So the earth element. The earth element means hard and soft when you

152
00:19:04,560 --> 00:19:14,080
feel the softness of the floor, the hardness of the chair that you're sitting in, that sort of

153
00:19:14,080 --> 00:19:20,640
thing, hardness and softness. The pressure, the pressure in your back when you sit up straight

154
00:19:20,640 --> 00:19:28,320
or the pressure in your legs when you sit cross-legged. The pressure in your stomach when it expands.

155
00:19:29,120 --> 00:19:34,960
The pressure in your head when you have a headache. The pressure is the air element. And the

156
00:19:34,960 --> 00:19:40,080
fire element is heat and cold when you feel heat when you feel cold.

157
00:19:42,800 --> 00:19:47,120
These are real. These are what we experience. These are the elements of physical experience.

158
00:19:47,120 --> 00:19:54,160
And when you focus on those, so when we watch the stomach rising, we're not

159
00:19:54,160 --> 00:19:59,520
technically practicing Ana Pandasatido. You couldn't say we are. But if you look at the way they

160
00:19:59,520 --> 00:20:05,520
was categorized in the ancient meditation manuals, no, we're practicing mindfulness of the elements.

161
00:20:05,520 --> 00:20:11,920
So watching the stomach rise is the air elements. And it's not air as in this puffy stuff.

162
00:20:11,920 --> 00:20:18,800
It's air as in the tension. When the stomach rises, there's a tension. When the stomach falls,

163
00:20:18,800 --> 00:20:23,360
there's a feeling of a release of tension. Those two experiences are very real.

164
00:20:24,560 --> 00:20:32,560
They're a physical, a physical, a phenomenon experience. They're physical phenomena.

165
00:20:34,320 --> 00:20:39,440
They arise and they cease. They're not under our control. They're not amenable to our control.

166
00:20:39,440 --> 00:20:45,600
They're not predictable. You watch your stomach. Sometimes it's smooth. Sometimes it's stuck.

167
00:20:45,600 --> 00:20:51,600
Sometimes it's pleasant. Sometimes it's unpleasant. And so it's a very good object for seeing

168
00:20:51,600 --> 00:20:57,360
impermanence, suffering, and non-self for learning to let go. I'm for learning to see how our

169
00:20:57,360 --> 00:21:04,960
mind clings, to watch how we cling to our experiences and to learn, therefore, thereby to see the

170
00:21:04,960 --> 00:21:11,440
mind. You watch the body in this way and you learn exactly how the mind works, because it's the

171
00:21:11,440 --> 00:21:20,320
mind that's doing the watching, right? Either way, this whole discussion talks about how to purify

172
00:21:20,320 --> 00:21:29,360
the mind. You practice in this way, eventually it leads to insight. If you practice samata,

173
00:21:29,360 --> 00:21:34,640
it's sort of round about your practice, seeing without looking. If it's we pass in it's direct,

174
00:21:34,640 --> 00:21:39,840
but it's looking without seeing, and eventually you get to the point where you're looking and

175
00:21:39,840 --> 00:21:47,040
you're seeing. And that's when you have this purity of mind that leads to insight. That's what

176
00:21:47,040 --> 00:21:56,880
we're aiming for. So the next, purifications three to seven, all have to do with wisdom. The first one

177
00:21:56,880 --> 00:22:05,760
was seala morality. The second one is samadhi concentration. And once you practice samadhi,

178
00:22:05,760 --> 00:22:13,200
then it leads to bhanya, or wisdom, which we'll deal with from here on in. So that's the

179
00:22:13,200 --> 00:22:28,080
demo for tonight. Thank you all for tuning in. We'll go to the questions page. See if there are any

180
00:22:28,080 --> 00:22:41,520
questions. Edit. Good evening edit. And it is one of my recent graduates here from our

181
00:22:41,520 --> 00:22:47,520
meditation center. I should have had an interview with her before she left, but I think we were too

182
00:22:47,520 --> 00:22:56,000
busy with trying to go see Niagara Falls. As I focus on an experience, rising, falling, sitting,

183
00:22:56,000 --> 00:22:59,840
touching there just a few moments in between these moments. There's nothing, no experience, not

184
00:22:59,840 --> 00:23:07,280
prominent. So it feels forced to me send my mind back to seek out what's happening. What is the

185
00:23:07,280 --> 00:23:14,880
object in meditation in this case? Is there anything to note? There is. You could. But I wouldn't

186
00:23:14,880 --> 00:23:22,080
unless it's prominent. Like it feels, starts to feel calm or quiet. Like your mind is

187
00:23:22,080 --> 00:23:27,520
perfectly quiet apart from that. Then you should note that stop rising, falling, sitting, touching.

188
00:23:27,520 --> 00:23:32,400
Instead, say calm, calm, quiet, quiet. But just thinking about the fact that there's stuff in

189
00:23:32,400 --> 00:23:39,200
between each of these notings isn't important to us. The noting isn't mindfulness. Noting is

190
00:23:39,200 --> 00:23:46,400
a tool that leads to mindfulness. It's more likely, more like tear a sun, the proximate cause of

191
00:23:46,400 --> 00:23:52,720
mindfulness. So it's enough to be noting, say, rising, falling, sitting, touching for you to see

192
00:23:52,720 --> 00:23:57,280
everything else as well. For you to be able to see all the things that you're not noting.

193
00:23:57,280 --> 00:24:06,320
So really just note some major aspects of the experience. And then if something distracts you

194
00:24:06,320 --> 00:24:13,440
from them, note that. Please do not ask questions. I just want to know our tradition.

195
00:24:14,560 --> 00:24:24,240
Okay, questions. Ideally are restricted to our Buddhist tradition, which means the meditation

196
00:24:24,240 --> 00:24:33,600
practice that I teach. So sorry, the meditation practice I teach or general Buddhist philosophy

197
00:24:33,600 --> 00:24:38,080
from a teravada point of view. So if you're interested in Buddhist concepts, generally I'm

198
00:24:38,080 --> 00:24:43,520
okay to answer them unless it's really, really theoretical or that kind of thing. But if it's about,

199
00:24:43,520 --> 00:24:48,480
hey, I practice this Zen technique or this Tibetan Buddhist technique. What do you think of this?

200
00:24:48,480 --> 00:24:57,120
Or any question about it is not something I'm into answering. So yes, general Buddhist philosophy,

201
00:24:57,120 --> 00:25:04,160
from my point of view, I'm open to it within reason. But if it's specifically about a different

202
00:25:04,160 --> 00:25:14,560
technique, then no, not open to that. The more I read the polycanon, I get the feeling that

203
00:25:14,560 --> 00:25:20,640
reality is breaking and that makes me afraid. Well, fear is a reality. So you focus on that as

204
00:25:20,640 --> 00:25:33,040
your meditation object. Meditation does not matter, but purely on the intellectual level.

205
00:25:33,040 --> 00:25:40,800
Yeah, we'll deal with the fear of the feeling of feeling of fear. That's reality. It's a part of

206
00:25:40,800 --> 00:25:47,280
reality. So focus on that reality. We'd say there's ourselves afraid, afraid, or if you're thinking

207
00:25:47,280 --> 00:25:53,440
about it, say thinking, thinking. I'm starting to feel apathetic in regards to the physical world

208
00:25:53,440 --> 00:25:59,280
what people communicate about, indeed, of their life. Things that have helped me so far with this

209
00:25:59,280 --> 00:26:06,480
is just more meditation. Yeah, absolutely. That's fine. I mean, what does it mean to be apathetic?

210
00:26:06,480 --> 00:26:15,840
It means you're no longer caught up in it. You have this freedom. Those things could never

211
00:26:15,840 --> 00:26:22,080
bring you why you become apathetic, because you realize they're just more stress. There are two

212
00:26:22,080 --> 00:26:27,120
kinds of apathy. One is through mindfulness and wisdom and one is through ignorance and delusion.

213
00:26:27,760 --> 00:26:32,080
So you have to be careful that if you're apathetic, then you note that and be mindful of that as well.

214
00:26:32,080 --> 00:26:38,800
But there arises a general, not apathy, but like I said, disinterest at least,

215
00:26:40,640 --> 00:26:47,040
sort of an aloofness or freedom whereby you're no longer caught up in it. But if it's this

216
00:26:47,040 --> 00:26:54,960
angst, this feeling of boredom or dislike, that's more preliminary. I mean, it is kind of

217
00:26:56,320 --> 00:27:01,040
wise in the sense that you realize these things are useless, but suppose people are talking about

218
00:27:01,040 --> 00:27:06,560
useful things and it really makes you upset or annoyed or just bored by it. So you have to be

219
00:27:06,560 --> 00:27:11,520
careful that it doesn't become a version or even ego thinking, I am so much better than these

220
00:27:11,520 --> 00:27:17,360
useless people who are talking about useless things, which that's unwholesome. So just be careful of

221
00:27:17,360 --> 00:27:25,040
that, but otherwise there's a certain, in a certain way, you become apathetic instead of sympathetic,

222
00:27:25,040 --> 00:27:33,360
sympathetic in the sense of getting caught up in them. What are your thoughts on doing the

223
00:27:33,360 --> 00:27:38,560
own chance? So this is an example of something not in our tradition. I don't, I get these questions.

224
00:27:38,560 --> 00:27:44,320
What are your thoughts on X and the thing is I don't think about X. In the context of tonight's

225
00:27:44,320 --> 00:27:50,000
talk, I mean, it's a bit interesting because yes, that's a sort of a tranquility meditation focusing

226
00:27:50,000 --> 00:27:57,840
on a concept. And so it works, it calms the mind down, but in terms of our practice, I mean,

227
00:27:57,840 --> 00:28:06,720
I just don't think about that sort of thing and it's not really in the purvey of purvey of what I do.

228
00:28:06,720 --> 00:28:22,640
And that's all the questions. So thank you all again for tuning in. Have a good night.

