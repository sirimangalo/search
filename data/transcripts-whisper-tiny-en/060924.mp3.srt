1
00:00:00,000 --> 00:00:21,520
Today I'll continue on with the very blessings of the Lord Buddha.

2
00:00:21,520 --> 00:00:46,840
Sometimes they're not directly related to the meditation practice, but we can understand

3
00:00:46,840 --> 00:00:58,840
these things as virtues and qualities of mind which come from mental development and

4
00:00:58,840 --> 00:01:12,760
can only come from mental development, which we cannot simply fake or pretend to possess.

5
00:01:12,760 --> 00:01:21,760
But until we practice to develop ourselves on the path of meditation and mindfulness,

6
00:01:21,760 --> 00:01:28,760
it's not very likely that these things would truly exist in our heart.

7
00:01:28,760 --> 00:01:37,760
So we can take it that these are the goals of successful insight meditation.

8
00:01:37,760 --> 00:01:43,760
We can practice strenuously to achieve them.

9
00:01:43,760 --> 00:01:53,760
So the next set of auspiciousness of blessings, good signs which sign that we are practicing correctly

10
00:01:53,760 --> 00:02:01,760
and have come to some sort of fruit in the practice as follows in Pauli.

11
00:02:01,760 --> 00:02:24,580
The true

12
00:02:24,580 --> 00:02:39,060
contentment, katanuta, gratitude, kalena tambu salamam, being able to hear the tambuat,

13
00:02:39,060 --> 00:02:48,260
or hearing the tambuat, hearing the truth at the right time. These are great, eight

14
00:02:48,260 --> 00:02:53,700
tambuam tambuat among these are great, spaciousness or great blessings or blessings in

15
00:02:53,700 --> 00:03:09,540
the truth. Now in order, karawat means respect. The word Buddha taught that all people

16
00:03:09,540 --> 00:03:16,980
should do well to pay respect to six things. The respect for these six things would

17
00:03:16,980 --> 00:03:24,500
bring great benefit to their lives. Bring great happiness to their minds. This is respect for the

18
00:03:24,500 --> 00:03:32,420
Buddha. Respect for the dhamma. Respect for the sanga. Respect for the training. Respect

19
00:03:32,420 --> 00:03:46,260
for a pamata, which we talked about yesterday, being hateful or mindful. And respect for

20
00:03:46,260 --> 00:04:02,420
hospitality. Respect for the Buddha means respect for the being or any being who has become enlightened

21
00:04:02,420 --> 00:04:12,100
for themselves. Becoming enlightened for oneself is not an easy thing to do. It takes at least

22
00:04:12,100 --> 00:04:22,340
four sankayat, four uncountable eons, sometimes eight, sometimes 20, sometimes many.

23
00:04:25,380 --> 00:04:39,700
The Lord Buddha got the Buddha, we know, took him four sankayat and 100,000 great, great,

24
00:04:39,700 --> 00:04:46,820
and a sankayat, four sankayat, I mean not seem like much, but when a sankayat they say

25
00:04:47,860 --> 00:04:56,260
a sankayat means you cannot count how long. It's uncountable. You can't count so many years, so many

26
00:04:56,260 --> 00:05:13,380
centuries, so many eons, but they have a simile that says, 100 years of human life on day in the

27
00:05:13,380 --> 00:05:22,260
angel world, in the heavens. In the heavens, one day in heaven is like 100 years in the human

28
00:05:22,260 --> 00:05:32,740
world. Now suppose there were a large hole, someone dug a very large hole or a canyon, you could

29
00:05:32,740 --> 00:05:39,620
say a canyon that was seven miles long and seven miles wide and seven miles deep.

30
00:05:39,620 --> 00:05:53,620
And every 100 angel years, where one day in the angel world is 100 human years, every 100 years

31
00:05:53,620 --> 00:06:02,580
in the angel world, an angel were to come and drop a sesame seed in this very large hole.

32
00:06:02,580 --> 00:06:13,540
And every 100 angel years, another sesame seed, another sesame seed, until the whole

33
00:06:15,700 --> 00:06:22,340
the entire hole was full of sesame seeds. And then one by one, they were to take the sesame

34
00:06:22,340 --> 00:06:33,540
seeds out again. They see the time it would take to fill the hole up and then empty the hole again,

35
00:06:36,100 --> 00:06:42,820
once every 100 angel years, once a sesame seed. This is the amount of time and this

36
00:06:43,940 --> 00:06:49,060
this task would be accomplished before an sankayat was finished.

37
00:06:49,060 --> 00:06:59,700
And the Lord Buddha had to take four, four periods, four uncountable periods of time.

38
00:06:59,700 --> 00:07:09,140
They become Buddha. This is something worthy of great respect, great reference. For all of us,

39
00:07:09,140 --> 00:07:17,060
maybe we, probably we don't have any such aspiration to become a Buddha, you have to know everything.

40
00:07:17,060 --> 00:07:21,140
For all of us, we are content with simply giving up everything.

41
00:07:23,140 --> 00:07:26,260
Buddha also gave up everything, but he also knew everything.

42
00:07:28,180 --> 00:07:31,700
If you want to know everything, it takes four, a sankayat takes a long time.

43
00:07:32,980 --> 00:07:35,140
To simply give up, everything doesn't take so long.

44
00:07:38,900 --> 00:07:42,420
And we started it here, we can come for 21 days and this is a start.

45
00:07:42,420 --> 00:07:49,060
We started in this life, in all of ourselves. This is respect for the Buddha,

46
00:07:49,620 --> 00:07:54,500
respect for his purity, Lord Buddha was completely pure,

47
00:07:57,220 --> 00:08:02,260
no greed, no anger, no delusion, not even a drop of imperfection.

48
00:08:02,260 --> 00:08:10,820
He had great wisdom, wisdom, they say, you can't find an end to the Buddha's wisdom.

49
00:08:12,340 --> 00:08:19,380
And he had great compassion, but not only did he realize that I'm before himself, but he also

50
00:08:19,380 --> 00:08:26,340
taught other people. When I invited to teach, he didn't refuse to teach,

51
00:08:26,340 --> 00:08:37,380
I never refused to give the right teaching, not for his own gain or because he was a duty or so on.

52
00:08:40,820 --> 00:08:45,220
But because he was invited to teach him because his heart was full of compassion,

53
00:08:46,180 --> 00:08:48,580
that when someone asked him to teach, he would never refuse.

54
00:08:48,580 --> 00:08:58,980
Respect for the Dhamma, his respect for the teaching of the Lord Buddha, even the Lord Buddha

55
00:08:58,980 --> 00:09:07,380
himself, respect for the Dhamma. Those things which the Lord Buddha realized for himself.

56
00:09:10,180 --> 00:09:15,140
And the one thing the Lord Buddha paid respect to, he said, I couldn't bear respect to anyone else

57
00:09:15,140 --> 00:09:20,260
in the world, there's no one else who's a Buddha. None of these other people are Buddha.

58
00:09:21,380 --> 00:09:26,980
So how could I respect to them? He said, I will respect the Dhamma, the truth which I have realized.

59
00:09:29,540 --> 00:09:38,020
And so he also paid respect for the Dhamma. The Dhamma is a two kind. We have Loki and

60
00:09:38,020 --> 00:09:45,700
Dhamma and Loki and Dhamma. Loki and Dhamma is things to be developed in the world.

61
00:09:45,700 --> 00:09:52,100
The truth of the world, or those good things, those good qualities which we have to develop in

62
00:09:52,100 --> 00:09:59,140
the world. We have the four foundations of mindfulness, mindfulness of the postures of the body and

63
00:09:59,140 --> 00:10:12,340
so on. Mindfulness of the feelings, pain and ignorance of the mind. Mindfulness of the mind,

64
00:10:12,340 --> 00:10:20,100
thinking about the past and future, good thinking of that thinking. Mindfulness of the Dhamma, the

65
00:10:20,100 --> 00:10:28,820
five hindrances by King, despite King, and Dhamma. These are called Loki and Dhamma, so things which are, we

66
00:10:28,820 --> 00:10:35,700
develop in the world. We come to see the truth of these things, the benefit of these things.

67
00:10:37,940 --> 00:10:41,540
When we use these things to see the truth, so they're called Dhamma.

68
00:10:41,540 --> 00:10:50,580
When we come to see impermanence, impermanence is a very important thing for us to see,

69
00:10:52,020 --> 00:10:58,500
and we won't hold on to bad things or good things. Sometimes something comes up and is very bad

70
00:10:58,500 --> 00:11:06,340
and we want to run away or we want to change or we want to fix things. We can't stand the way it is

71
00:11:06,340 --> 00:11:13,460
thinking if we don't do something, it might never change. The impermanence means we can see that it's

72
00:11:13,460 --> 00:11:22,260
changing all the time. We come to let go of those things that we hold on to because we see they're

73
00:11:22,260 --> 00:11:29,860
not permanent. Even happy you're good things. We thought it was good, so we'll try to get it again

74
00:11:29,860 --> 00:11:38,980
and again. We don't see that it's always impermanence, no matter what we do, we can never make it

75
00:11:38,980 --> 00:11:44,100
permanent. Whatever good or happy things we have, we have to let go of them as well.

76
00:11:45,700 --> 00:11:51,300
We'll go back to the past or look to the future for good things. When we let go of everything,

77
00:11:52,260 --> 00:11:53,940
then we'll be happy and then we'll be free.

78
00:11:53,940 --> 00:12:03,460
And then we can see suffering. Suffering is another very important thing,

79
00:12:06,180 --> 00:12:13,620
to see where is suffering. When we practice, we will see suffering in places we didn't think

80
00:12:13,620 --> 00:12:19,220
was suffering. The ordinary state of a human being is they know they are suffering,

81
00:12:19,220 --> 00:12:25,860
they know there is pain and there is upset in their lives, upset in their mind, even they don't

82
00:12:25,860 --> 00:12:34,580
admit it to other people, they still have inside. But they don't know what is the cause of suffering.

83
00:12:38,580 --> 00:12:43,300
They don't know what really is, what really are those things which are stressful.

84
00:12:43,300 --> 00:12:54,340
They don't know the truth about everything that they hold on to and attach to and want and need,

85
00:12:57,460 --> 00:13:02,900
get upset about and worry about and fret about and so on.

86
00:13:04,740 --> 00:13:09,620
They don't see that these things are actually suffering. When we attach, when we hold on,

87
00:13:09,620 --> 00:13:16,340
when we cling to anything in the world, good or bad, we can't see that this is suffering. This is why

88
00:13:16,340 --> 00:13:29,620
we suffer. We can't see these things are like a hot, cold or a hot ball of iron. That is something

89
00:13:29,620 --> 00:13:35,300
which will cause suffering. When we hold on to it, it will cause us. If we don't hold on to anything,

90
00:13:35,300 --> 00:13:41,060
there is no suffering in the world. This is a very peaceful place. If we don't hold on to anything,

91
00:13:42,340 --> 00:13:50,580
we are happy among so many miserable people. We are at peace amongst so many unpeaceful people.

92
00:13:50,580 --> 00:14:12,420
This is suffering and then non-self. Non-self is when learning to see that in truth of reality,

93
00:14:12,420 --> 00:14:21,860
there is only body in mind. Inside of ourselves or in other people as well or in the whole world

94
00:14:21,860 --> 00:14:29,700
around us, it is only body in mind. There is no him or her or they or them or us or we

95
00:14:29,700 --> 00:14:42,820
be good or bad, self or soul or so. All of these things are only concepts. The purpose of seeing

96
00:14:42,820 --> 00:14:50,740
non-self is so that we don't hold on to something as an entity, as an object. All the things

97
00:14:50,740 --> 00:15:00,900
that we are angry or upset about, this person, that person, there is no person. When we come to see

98
00:15:00,900 --> 00:15:08,980
that there is only seeing, hearing, smelling, tasting, feeling. Only body and feelings and

99
00:15:10,340 --> 00:15:18,180
perception, memory, thoughts, consciousness, pieces of all, all of many pieces that make up a whole

100
00:15:18,180 --> 00:15:31,060
and all of these pieces are only self or soul or me and them. We won't get angry at other people

101
00:15:31,060 --> 00:15:41,780
and see that other person is, same as me, only full of many things arising and ceasing or good

102
00:15:41,780 --> 00:15:52,740
things, sometimes bad things, sometimes we won't hold on to ourselves or other people. Good things

103
00:15:52,740 --> 00:16:02,020
come, we also won't think of, it is an entity. We see it as things with many states arising

104
00:16:02,020 --> 00:16:11,700
and ceasing, coming and going. We come to see that it is actually dissatisfied, it is non-self. It is not

105
00:16:11,700 --> 00:16:21,140
a real entity. It is only made up of body and mind. We will be acknowledging, hearing, smelling,

106
00:16:21,140 --> 00:16:33,540
tasting, feeling and so on. We will acknowledge every moment, every moment. We will come to see

107
00:16:33,540 --> 00:16:42,180
non-self. We will see non-self in everything. Nothing in the world is mine or me or belongs to me

108
00:16:42,180 --> 00:16:52,580
or is self or belongs to self. This is our Loki at Amman. This is the truth which we come to see

109
00:16:52,580 --> 00:16:59,460
in the world. Loki at Amman is above the world, outside of the world, outside of the universe,

110
00:17:02,180 --> 00:17:09,060
something which exists outside of mundane existence. This is the

111
00:17:09,060 --> 00:17:18,500
reality which they call nirvana or nirvana. It is the attainment of cessation of suffering.

112
00:17:22,340 --> 00:17:30,340
When we give up attachment to the world and the mind flies away at the moment, it is released

113
00:17:31,540 --> 00:17:38,420
even just for a moment. We see nirvana even just for a moment. This is great happiness, true

114
00:17:38,420 --> 00:17:44,500
happiness. This is the dhamma or something which is worthy of respect.

115
00:17:44,500 --> 00:18:14,340
When we see nirvana, they will continue to live their life as a normal.

