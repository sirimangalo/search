1
00:00:00,000 --> 00:00:06,000
Hello and welcome back to our study of the Dhamupada.

2
00:00:06,000 --> 00:00:16,000
Today we continue on with verse 2.53, which reads as follows.

3
00:00:16,000 --> 00:00:45,000
For one who is focused on the transgressions of others,

4
00:00:45,000 --> 00:01:07,000
always perceiving fault, for such a one, the ass of an increase.

5
00:01:07,000 --> 00:01:16,000
They are very far from the destruction of the Asuah.

6
00:01:16,000 --> 00:01:21,000
So this I think is our first verse that there really isn't a story behind.

7
00:01:21,000 --> 00:01:28,000
There is rather simply a person among,

8
00:01:28,000 --> 00:01:38,000
the monk who came to be known as Ujana Suny when he perceives fault.

9
00:01:38,000 --> 00:01:45,000
Well I guess the story is that he went around perceiving fault in all the monks.

10
00:01:45,000 --> 00:01:51,000
They didn't wear their robes right, they didn't perform their duties right,

11
00:01:51,000 --> 00:01:54,000
they didn't act right, they didn't speak right.

12
00:01:54,000 --> 00:01:58,000
Dwelling always on the faults of others.

13
00:01:58,000 --> 00:02:04,000
The monks brought it to the attention of the Buddha and the Buddha taught this verse.

14
00:02:04,000 --> 00:02:10,000
That's it.

15
00:02:10,000 --> 00:02:21,000
So the simple lesson, it's in pattern with the verse 2.52 and with this chapter

16
00:02:21,000 --> 00:02:24,000
of finding fault.

17
00:02:24,000 --> 00:02:31,000
The lesson is that finding fault is not a good thing.

18
00:02:31,000 --> 00:02:41,000
But there's much more to it than that, of course.

19
00:02:41,000 --> 00:02:45,000
We have to answer the question as to what's wrong with finding fault in others.

20
00:02:45,000 --> 00:02:57,000
And in fact, we have to qualify it because it's not true that finding fault in others is always wrong.

21
00:02:57,000 --> 00:03:07,000
And that's not exactly what the Buddha says in this verse.

22
00:03:07,000 --> 00:03:16,000
If we want to find out what's bad, why is it bad to find fault, for example?

23
00:03:16,000 --> 00:03:25,000
We have to be able to, as usual, distinguish between conventional reality and ultimate reality.

24
00:03:25,000 --> 00:03:35,000
When we say something is bad or good and conventional reality, there's meaning behind that, certainly.

25
00:03:35,000 --> 00:03:41,000
Taking this example in particular, finding fault in other monks can be a very good thing.

26
00:03:41,000 --> 00:03:45,000
It's good for communal harmony to point out each other's faults.

27
00:03:45,000 --> 00:03:50,000
At the end of the rains are three-month rains.

28
00:03:50,000 --> 00:04:01,000
When the rain is stopped and the monks are ready to head off, before they head off, there's an occasion for informing each other.

29
00:04:01,000 --> 00:04:06,000
If anything you might have done, you might have done wrong during your three-month retreat.

30
00:04:06,000 --> 00:04:26,000
You invite the monks invite each other to point out their faults, their transgressions.

31
00:04:26,000 --> 00:04:43,000
But of course, in practice, because of the categorical difference between ultimate reality and conventional reality, in practice,

32
00:04:43,000 --> 00:04:48,000
pointing out the faults of others often doesn't go very well.

33
00:04:48,000 --> 00:05:05,000
If it's not done well, and as this first points out, done well or not, if it's not done with the right intentions,

34
00:05:05,000 --> 00:05:13,000
if it's done purposefully in the sense of making it one's practice,

35
00:05:13,000 --> 00:05:19,000
and by extension not just making it one's practice to find faults in others, but in general,

36
00:05:19,000 --> 00:05:26,000
making it one's practice to exist in the external.

37
00:05:26,000 --> 00:05:33,000
So the obvious problem with, on an ultimate level with finding fault in others,

38
00:05:33,000 --> 00:05:50,000
is the corruption, the impurities of mind that generally and quite often take control of the mind when one enters into this fault finding mind.

39
00:05:50,000 --> 00:06:11,000
The Asala is the Buddha said, things like greed, you can be manipulative wanting to manipulate people for your own benefit, finding fault in them.

40
00:06:11,000 --> 00:06:19,000
Anger, you can want to hurt others, finding fault in them to make them feel suffering.

41
00:06:19,000 --> 00:06:30,000
And delusion can be simply out of conceit or ignorance, lack of consideration.

42
00:06:30,000 --> 00:06:37,000
I mean, not quite often, of course, conceit may be low self-esteem, of course, high self-esteem.

43
00:06:37,000 --> 00:06:43,000
I think you're better than someone worse than them until it agrees in for.

44
00:06:43,000 --> 00:06:56,000
The reason for going external wanting to show your own worth, for example.

45
00:06:56,000 --> 00:07:03,000
But there's an important point about being fixated on the external as opposed to the internal.

46
00:07:03,000 --> 00:07:12,000
If you're fixated on your own faults, that's of course a problem, but it lacks the problem or the danger

47
00:07:12,000 --> 00:07:19,000
of focusing on the faults of others, because we can say that finding faults in others is helpful.

48
00:07:19,000 --> 00:07:24,000
If you find fault in others, well that's a good thing for them.

49
00:07:24,000 --> 00:07:30,000
They also make you feel happy that you've done them a service, perhaps.

50
00:07:30,000 --> 00:07:34,000
But who's to say it actually benefits them? Who's to say they take it in the right way,

51
00:07:34,000 --> 00:07:41,000
and even if they take it in the right way, whether they're actually able to practice in such a way to better themselves,

52
00:07:41,000 --> 00:07:50,000
to change their behavior. It can be very important to let people know, but it's not the most important thing.

53
00:07:50,000 --> 00:07:59,000
The external, the practice of seeking good in the external is fraught with danger, with problem.

54
00:07:59,000 --> 00:08:07,000
You can't be sure what the result is going to be, and the result, the direct result, is not actually the greatest benefit

55
00:08:07,000 --> 00:08:19,000
that best it can be a catalyst for others to implement a change person on a personal level.

56
00:08:19,000 --> 00:08:36,000
Moreover, it's by its very nature, a difficult, challenging experience to keep pure.

57
00:08:36,000 --> 00:08:40,000
If you consider the difference, when you look at your own faults, you're looking at experience.

58
00:08:40,000 --> 00:08:45,000
Suppose you get angry, and you know that anger is a fault, and you maybe you feel bad about that.

59
00:08:45,000 --> 00:08:49,000
Well, that's not a good thing. It's not good to be angry at your anger, for example.

60
00:08:49,000 --> 00:08:56,000
But at the very least, it's a very primary experience. It's based on experiences.

61
00:08:56,000 --> 00:09:01,000
When you focus on someone else's problems, you don't actually experience anger.

62
00:09:01,000 --> 00:09:03,000
If they're angry, let's say.

63
00:09:03,000 --> 00:09:07,000
You want to tell someone about their anger. First, you have to process the experience.

64
00:09:07,000 --> 00:09:11,000
You have to think about the person. You have to think about what you're going to say, how you're going to say it.

65
00:09:11,000 --> 00:09:15,000
Your mind is in a complex state because of the conceptual nature.

66
00:09:15,000 --> 00:09:18,000
You have to conceive of the other person.

67
00:09:18,000 --> 00:09:28,000
And this is why it's prime breeding ground for unholesomeness, because we get lost in the concepts.

68
00:09:28,000 --> 00:09:33,000
We're no longer truly focused on our state of mind. We're too much focused on the other person.

69
00:09:33,000 --> 00:09:41,000
And so any bad habits we might have that we haven't worked out ourselves are easily able to overcome us.

70
00:09:41,000 --> 00:09:57,000
And this is why our work on the external level is quite often unsuccessful, tainted by our own.

71
00:09:57,000 --> 00:10:02,000
Our own defilement.

72
00:10:02,000 --> 00:10:09,000
And also why the reminded us or encouraged us to focus on our own faults.

73
00:10:09,000 --> 00:10:17,000
If everyone focuses on their own faults, of course there are far fewer faults defined in each other.

74
00:10:17,000 --> 00:10:21,000
But more importantly, it's the work that can actually be done.

75
00:10:21,000 --> 00:10:26,000
You can easily find the faults. Your own faults, much more easier.

76
00:10:26,000 --> 00:10:36,000
Much more easier than you can work out the faults of others.

77
00:10:36,000 --> 00:10:47,000
And this relates to this word as, well, I didn't translate it purposefully because it's not such a hard word to understand.

78
00:10:47,000 --> 00:10:55,000
It's just it's hard to understand the Buddha's use of it without explanation.

79
00:10:55,000 --> 00:11:03,000
It's one of those words that because it hasn't become a word used in this way in English,

80
00:11:03,000 --> 00:11:06,000
we don't really have a good catch word.

81
00:11:06,000 --> 00:11:11,000
All the translations I've seen I don't think are very accurate at all. They're not very literal.

82
00:11:11,000 --> 00:11:17,000
It refers to defilements, but it's not a word that means defilements,

83
00:11:17,000 --> 00:11:24,000
corruptions, impurities of the mind, things that cause suffering, problems for us, mental problems.

84
00:11:24,000 --> 00:11:31,000
It's a way of describing mental problems and in fact a way a word that the Buddha used quite often.

85
00:11:31,000 --> 00:11:39,000
Far more often than we actually teach this word.

86
00:11:39,000 --> 00:11:47,000
So it's not a word that means defilement. It's a way of describing defilements as being outflowings.

87
00:11:47,000 --> 00:11:57,000
As of what means literally something that pours out or leaks or oozes.

88
00:11:57,000 --> 00:12:05,000
Do you think of a leaky pot or an infected wound?

89
00:12:05,000 --> 00:12:11,000
If it's infected, it will never stop oozing, it will never stop leaking.

90
00:12:11,000 --> 00:12:23,000
But I think the real meaning of this word, why the Buddha used it, is related to this idea of not just externalizing,

91
00:12:23,000 --> 00:12:36,000
but going outside, going beyond leaking, going beyond the pure, simple, real,

92
00:12:36,000 --> 00:12:41,000
like reality based state of mind where you experience things as they are.

93
00:12:41,000 --> 00:12:48,000
This phrase experiencing things as they are is a very powerful and important statement.

94
00:12:48,000 --> 00:12:51,000
It's a bold claim that that's what we do in Buddhism.

95
00:12:51,000 --> 00:12:56,000
We try to experience things as it's not a very hard thing to understand,

96
00:12:56,000 --> 00:12:59,000
but it's very important.

97
00:12:59,000 --> 00:13:08,000
Buddhism is not about, and meditation is not about sequestering yourself away from reality by any means.

98
00:13:08,000 --> 00:13:20,000
It's about trying to experience reality deeper, more fundamental than the external conceptual world.

99
00:13:20,000 --> 00:13:27,000
We try to focus on the real world of relationships and think that people are meditator,

100
00:13:27,000 --> 00:13:30,000
leaving behind that. But in fact, it's quite the opposite.

101
00:13:30,000 --> 00:13:36,000
As we externalize ourselves, we ignore so much of what's real about ourselves.

102
00:13:36,000 --> 00:13:46,000
We ignore and we lose track of our emotions and therefore enable to navigate our relationships purely,

103
00:13:46,000 --> 00:13:56,000
because our minds are caught up in whatever whim is our habit of the moment.

104
00:13:56,000 --> 00:14:07,000
So this state of experiencing things as they really are is like the pot.

105
00:14:07,000 --> 00:14:11,000
And when we go outside of that, when we interpret, when we extrapolate,

106
00:14:11,000 --> 00:14:17,000
when we see something that isn't there or react in a way that is unwarranted,

107
00:14:17,000 --> 00:14:21,000
that's an Ahsawa, that's the meaning of Ahsawa going beyond.

108
00:14:21,000 --> 00:14:30,000
When we're not strong enough or pure enough to see things as they are,

109
00:14:30,000 --> 00:14:34,000
we see them as bad or good or me or my amigo beyond.

110
00:14:34,000 --> 00:14:37,000
We leak. That's the usage of the word Ahsawa.

111
00:14:37,000 --> 00:14:45,000
And the Buddha used it quite often, much more often than we actually hear this word Ahsawa in talks.

112
00:14:45,000 --> 00:14:53,000
I think also because it's mistranslated, not maliciously so, but in a way that hides the meaning here.

113
00:14:53,000 --> 00:14:57,000
It's a very technical, I think technical term.

114
00:14:57,000 --> 00:15:01,000
Because the word of course, defilement or impurity, it's emotionally charged.

115
00:15:01,000 --> 00:15:09,000
But on a technical level, it seems that the Buddha preferred when speaking technically to talk about it.

116
00:15:09,000 --> 00:15:25,000
Simply as leaking, as overflowing, not being in a good state, a healthy or composed state.

117
00:15:25,000 --> 00:15:28,000
It's a way of describing what defilements do.

118
00:15:28,000 --> 00:15:33,000
They lead us outward, they lead us to cling to things, to run away from things,

119
00:15:33,000 --> 00:15:40,000
they lead us away from our equilibrium.

120
00:15:40,000 --> 00:15:49,000
There are four types of Ahsawa, and I'll just talk about these because it gives us an idea of

121
00:15:49,000 --> 00:15:53,000
how the Buddha used this word and the Buddha's teachings on.

122
00:15:53,000 --> 00:15:58,000
What's wrong? What goes wrong when we find fault with others?

123
00:15:58,000 --> 00:16:09,000
Again, this idea of focusing on the faults of others and obsessing and always interested in the faults of others.

124
00:16:09,000 --> 00:16:14,000
It's really that which is the leak.

125
00:16:14,000 --> 00:16:19,000
It's not the telling people about their faults or even seeing the faults of others.

126
00:16:19,000 --> 00:16:25,000
It's where the mind becomes obsessed, where your intention, your inclination,

127
00:16:25,000 --> 00:16:30,000
is rather than seeing things, seeing reality, being with reality,

128
00:16:30,000 --> 00:16:41,000
is in a very conceptual and complex state of finding the faults in beings other than yourself.

129
00:16:41,000 --> 00:16:45,000
And pointing them out to others and talking about them,

130
00:16:45,000 --> 00:16:50,000
maybe gossiping about them, telling other people about them behind their back and so on and so on.

131
00:16:50,000 --> 00:17:00,000
It's very complicated. The state of mind behind all of those actions is complex and highly problematic.

132
00:17:00,000 --> 00:17:09,000
And so the four types, the four, you could say the four reasons why one might leak in this way are for sensuality,

133
00:17:09,000 --> 00:17:19,000
kama-sawa, because you want something from someone so you manipulate them.

134
00:17:19,000 --> 00:17:27,000
If I point out what they're doing wrong, they'll lie upon me, maybe they'll...

135
00:17:27,000 --> 00:17:32,000
If we're vying for a position and employment, for example,

136
00:17:32,000 --> 00:17:42,000
maybe I tell other people about their faults or I make them feel like they don't deserve it and then it will allow me to get what I want.

137
00:17:42,000 --> 00:17:48,000
Maybe more money or so on, you can manipulate people by belittling them.

138
00:17:48,000 --> 00:17:57,000
Baba-sawa is, I guess, more related to fault finding.

139
00:17:57,000 --> 00:18:04,000
Baba-sawa is relating to states or existence.

140
00:18:04,000 --> 00:18:13,000
So the meaning here is wanting something to be a certain way or not be a certain way and it relates to manipulation.

141
00:18:13,000 --> 00:18:24,000
You manipulate people or you point out people's faults because you don't want them to be a certain way or you want them to be a certain way.

142
00:18:24,000 --> 00:18:31,000
We want our children to be a certain way so we point out what they're doing wrong, right?

143
00:18:31,000 --> 00:18:38,000
Wrong is such an interesting concept as well. It relates to the third, which is deta-sawa.

144
00:18:38,000 --> 00:18:41,000
Our idea of what is wrong.

145
00:18:41,000 --> 00:18:49,000
So the reason why we leak, and in this case, why we find, why we obsess about the faults of others,

146
00:18:49,000 --> 00:18:59,000
relates to our idea, things must be this way and our very wrong view of that things must be a certain way in the first place.

147
00:18:59,000 --> 00:19:04,000
Finding fault is very much tied up with thinking things must be a certain way.

148
00:19:04,000 --> 00:19:18,000
You find this in Buddhist monasteries, often when monks have done their due diligence and learned about the way monasteries should be, then they get quite frustrated and upset when they find out the monasteries aren't that way.

149
00:19:18,000 --> 00:19:24,000
Organizations like our meditation organization, we often deal with this.

150
00:19:24,000 --> 00:19:30,000
People with frustration that things aren't a certain way or should be a certain way.

151
00:19:30,000 --> 00:19:36,000
It's really a wrong. It's related to wrong view.

152
00:19:36,000 --> 00:19:40,000
Reality things not shouldn't be a certain way or shouldn't be a certain way.

153
00:19:40,000 --> 00:19:42,000
They are a certain way.

154
00:19:42,000 --> 00:19:49,000
And the real problem in the world is that we can't understand things as being the way they are.

155
00:19:49,000 --> 00:19:54,000
We can't see things as being that way. We see them as being the wrong way.

156
00:19:54,000 --> 00:20:02,000
Some other way is being the right way and so we're constantly trying to fix and make things better and so on.

157
00:20:02,000 --> 00:20:06,000
And it's that fixing mindset that makes us leak.

158
00:20:06,000 --> 00:20:17,000
We go out, we lose all sorts of frustrations, anger and desires and ambitions and so on.

159
00:20:17,000 --> 00:20:26,000
And finally, oh we just, one big reason perhaps the biggest and overarching reason why we leak is just ignorance.

160
00:20:26,000 --> 00:20:41,000
Not realizing the suffering that comes from getting caught up in anything, clinging to anything, wanting for anything.

161
00:20:41,000 --> 00:21:05,000
And so the lesson for our practice is the reminder of this composed state and the danger of leaking, the danger of letting our minds go beyond what is real.

162
00:21:05,000 --> 00:21:15,000
Anytime we get caught up in interactions with the external world at all, even things simple things like eating, working, cleaning.

163
00:21:15,000 --> 00:21:29,000
We run the risk because of the complexity, we run the risk of leaving behind reality and getting caught up in manipulation, reaction and so on.

164
00:21:29,000 --> 00:21:53,000
And so if we're going to engage with other people at all or other things at all, we have to be very careful like carrying a fragile pot or vase or valuable possession, fragile possession to carry it with care and not let it break.

165
00:21:53,000 --> 00:22:06,000
As the Buddha likened, and this is probably a relation to the ass, the concept of the ass, so as Buddha likened it to a man who was carrying a big pot of oil.

166
00:22:06,000 --> 00:22:15,000
And he said, imagine there was a man who was told he had to carry a big pot of oil full to the brim.

167
00:22:15,000 --> 00:22:24,000
When he had to carry it throughout the entire city, and they ordered a man with a sword to walk behind him.

168
00:22:24,000 --> 00:22:33,000
And if he let anyone drop of oil spill, that man was to cut off this other man's head.

169
00:22:33,000 --> 00:22:54,000
And the Buddha said, now that man, do you think he would be distracted by any singing or dancing or amusement happening as he walked through the city or anyone calling out to him or any beautiful woman or man or anything that might distract him good food to eat?

170
00:22:54,000 --> 00:23:02,000
And no venerable, sir, yes. And well, you should think like this, you should be as careful as that man.

171
00:23:02,000 --> 00:23:09,000
We really have to, our pot is very fragile, easily broken. Of course, easily put back together.

172
00:23:09,000 --> 00:23:15,000
It's not the end of the world when you get distracted, obviously.

173
00:23:15,000 --> 00:23:27,000
It's just your concentration that's broken and you can regain it and build it and fortify it through the practice of mindfulness.

174
00:23:27,000 --> 00:23:41,000
Reminds us about the importance of our practice in worldly things as well when relating to others to not get into the fault finding mind for all these reasons because of greed, because of fear,

175
00:23:41,000 --> 00:23:57,000
because of feeling inferior or superior, because of views or desires, because of ignorance.

176
00:23:57,000 --> 00:24:06,000
Do not be ignorant. Do I have a sense of what's right and what's important?

177
00:24:06,000 --> 00:24:27,000
And when you do engage with others, whether it be finding fault or praise, you do so mindfully with a sense of it being appropriate, without any clinging, without any attachment and letting it go and continuing on once it's done.

178
00:24:27,000 --> 00:24:38,000
So an important lesson, a good opportunity to talk about the ass of what which are very important teaching. So that's the Dhamma Padafur tonight. Thank you all for listening.

