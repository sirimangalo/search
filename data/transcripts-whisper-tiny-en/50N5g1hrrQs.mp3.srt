1
00:00:00,000 --> 00:00:05,460
Bante, what is your opinion about sitting through pain?

2
00:00:05,460 --> 00:00:09,840
Many teachers advocate it because it creates equanimity, and you learn how to deal with your

3
00:00:09,840 --> 00:00:18,200
resistance, and others say it is extreme asceticism and to be avoided many things.

4
00:00:18,200 --> 00:00:29,680
Anyone who says that pain, that the forbearance or the bearing with pain is extreme

5
00:00:29,680 --> 00:00:41,160
extremism is an extreme, and therefore in the sense of bad practice is going against

6
00:00:41,160 --> 00:00:45,720
the Buddha's teaching, doesn't understand the Buddha's teaching.

7
00:00:45,720 --> 00:00:48,640
Maybe well-meaning, but misguided.

8
00:00:48,640 --> 00:00:57,160
The Buddha said clearly in several places that one, that forbearance, one should true

9
00:00:57,160 --> 00:01:07,520
patience means forbearing against pain, even such that it might take your life, even

10
00:01:07,520 --> 00:01:18,200
so extreme that it might kill you, and to understand this or to verify this, to argue

11
00:01:18,200 --> 00:01:32,040
this, we can point to the – what do we mean by – what is the Buddha's teaching?

12
00:01:32,040 --> 00:01:36,720
What is the path that the Buddha taught?

13
00:01:36,720 --> 00:01:45,120
It's the path to my understanding, is that it's the path to become completely objective,

14
00:01:45,120 --> 00:01:55,760
and let's say invincible to all experience such that no experience that arises could possibly

15
00:01:55,760 --> 00:02:01,440
be a cause for suffering, that there would be no clinging to any experience whatsoever,

16
00:02:01,440 --> 00:02:08,760
no reaction, no emotional reaction to any experience in the sense of being free, being

17
00:02:08,760 --> 00:02:14,800
at complete peace, complete peace, no matter what arises.

18
00:02:14,800 --> 00:02:28,720
You can't do that for as long as painful situations disturb you.

19
00:02:28,720 --> 00:02:37,840
So as long as you are a verse to any type of pain, or any situation, or even a verse

20
00:02:37,840 --> 00:02:44,640
to death, you can't attain the goal of the Buddha's teaching.

21
00:02:44,640 --> 00:02:46,640
This is only a part of the answer.

22
00:02:46,640 --> 00:02:52,840
The other part of the answer is that there certainly may be, along the way, to getting

23
00:02:52,840 --> 00:03:00,800
to that extreme state or that ultimate state of true invincibility, there may be cases

24
00:03:00,800 --> 00:03:12,160
where you should quite reasonably reduce or change the situation.

25
00:03:12,160 --> 00:03:18,120
So there may be situations that, first of all, are overwhelming, so for a novice meditator

26
00:03:18,120 --> 00:03:24,880
to insist that they sit still, no matter what sensations arise, is unreasonable.

27
00:03:24,880 --> 00:03:34,520
It is more likely to create unwholesumness, as long as one is affected by pain.

28
00:03:34,520 --> 00:03:39,280
So when pain, a little bit of pain comes up, they start to get upset.

29
00:03:39,280 --> 00:03:44,360
If you ask them to sit through extreme pain, instead of cultivating wholesome mindfulness

30
00:03:44,360 --> 00:03:53,200
patients, it could just cultivate a version, anxiety, even a dislike of the meditation practice.

31
00:03:53,200 --> 00:03:57,360
It can create very strong emotions, strong negative emotions.

32
00:03:57,360 --> 00:04:05,880
So for a beginner meditator, you have to know your limits and be able to push them, but

33
00:04:05,880 --> 00:04:12,000
not try to break through your limits, you know, not go beyond your limits.

34
00:04:12,000 --> 00:04:18,240
So when the pain gets truly unbearable, where you're just not able emotionally to handle

35
00:04:18,240 --> 00:04:23,000
it, you're fully reasonable to have the meditator move.

36
00:04:23,000 --> 00:04:26,080
So be mindful if you're for that move it.

37
00:04:26,080 --> 00:04:30,120
Eventually through repeated practice, the meditator will become more patient and more

38
00:04:30,120 --> 00:04:36,800
able to stand with the pain, as long as we understand that our goal is to become invincible

39
00:04:36,800 --> 00:04:44,600
to any situation, any state, you know, it's up to us how quickly we're going to get

40
00:04:44,600 --> 00:04:45,600
there.

41
00:04:45,600 --> 00:04:51,000
So we can admit that we're not able to deal with it, the pain, without taking that

42
00:04:51,000 --> 00:04:57,960
as a dogma, that when pain arises, it is proper practice, or is the path to enlightenment

43
00:04:57,960 --> 00:05:02,440
to avoid the pain.

44
00:05:02,440 --> 00:05:08,360
Understanding that eventually you will cultivate tolerance and patience and objectivity

45
00:05:08,360 --> 00:05:13,040
to the extent that the pain no longer bothers you and you no longer have to move.

46
00:05:13,040 --> 00:05:18,040
The second reason is that there are certain cases where pain is a sign of something, where

47
00:05:18,040 --> 00:05:22,880
it is a sign that you are about to break your leg, for example, you're about to

48
00:05:22,880 --> 00:05:29,240
spray in your ankle or your leg, or you're about to cause injury to yourself.

49
00:05:29,240 --> 00:05:32,360
And obviously in those cases, it's, you know, you're back injury.

50
00:05:32,360 --> 00:05:36,920
Some people have back problems if they were to continue sitting, it might actually injure

51
00:05:36,920 --> 00:05:37,920
them.

52
00:05:37,920 --> 00:05:42,160
If you have a case for that, first of all, you have to be clear that you're not just

53
00:05:42,160 --> 00:05:46,600
making excuses or you're not just a hypochondriac.

54
00:05:46,600 --> 00:05:54,800
If you actually have some prior injury, many people come to meditation with plates and

55
00:05:54,800 --> 00:06:03,080
their legs or their joints or their backs or so, I know serious injuries, these people

56
00:06:03,080 --> 00:06:11,560
can in certain cases reasonably should in certain cases change their position.

57
00:06:11,560 --> 00:06:16,880
Just the other situation where it's actually going to harm you.

58
00:06:16,880 --> 00:06:17,880
You can make a case for it.

59
00:06:17,880 --> 00:06:21,360
Now you can also make a case for just sitting through it, letting yourself go.

60
00:06:21,360 --> 00:06:27,040
There's the case of Jackupala who did walking and sitting meditation, even though he was

61
00:06:27,040 --> 00:06:31,320
in order to heal his eyes, he was to lie down instead of lying down.

62
00:06:31,320 --> 00:06:36,960
He just did walking and sitting for three months and his eyes were destroyed.

63
00:06:36,960 --> 00:06:40,120
He went blind as a result of his extreme practice.

64
00:06:40,120 --> 00:06:45,560
But he also became enlightened.

65
00:06:45,560 --> 00:06:55,800
As a means of letting go, one can even let go of one's own physical well-being.

66
00:06:55,800 --> 00:06:58,760
There's certainly nothing wrong with that.

67
00:06:58,760 --> 00:07:03,480
Now, the idea of extreme asceticism, the question is, well, then what is that?

68
00:07:03,480 --> 00:07:09,320
Extreme asceticism is the concept that there's some benefit to hurting yourself.

69
00:07:09,320 --> 00:07:14,160
The pain is somehow intrinsically beneficial.

70
00:07:14,160 --> 00:07:19,480
The pain is somehow a means inflicting pain upon yourself somehow leads to enlightenment.

71
00:07:19,480 --> 00:07:21,080
It doesn't.

72
00:07:21,080 --> 00:07:24,760
Objectivity leads to enlightenment.

73
00:07:24,760 --> 00:07:35,920
The ability to stay patient in regards to pleasant sensations, pleasant situations, and

74
00:07:35,920 --> 00:07:39,200
patient in regards to unpleasant situations equally.

75
00:07:39,200 --> 00:07:41,520
That is what leads to enlightenment.

76
00:07:41,520 --> 00:07:47,520
The extreme practice is thinking that there's some benefit in either pleasant or unpleasant

77
00:07:47,520 --> 00:07:48,520
situations.

78
00:07:48,520 --> 00:07:49,520
Both of those are extreme.

79
00:07:49,520 --> 00:07:51,280
There's no benefit in either.

80
00:07:51,280 --> 00:07:54,320
There's only benefit in being objective towards them.

81
00:07:54,320 --> 00:07:56,000
So it's quite a clear teaching.

82
00:07:56,000 --> 00:08:14,600
I don't think there's any reason to be confused.

