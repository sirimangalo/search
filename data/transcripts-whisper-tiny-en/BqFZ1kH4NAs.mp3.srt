1
00:00:30,000 --> 00:00:53,160
Okay, so here we are coming to the Srimahambodhi tree in Anaradapura where we will continue

2
00:00:53,160 --> 00:00:55,200
our meditation practice.

3
00:00:55,200 --> 00:01:00,760
You are on meditation course and those of us who are staying at the monastery and

4
00:01:00,760 --> 00:01:09,120
as long term meditators who come here to see one of the most important Buddhist sites

5
00:01:09,120 --> 00:01:17,400
that exist in the world who could say the most important Buddhist side outside of India.

6
00:01:17,400 --> 00:01:24,480
This is the site where the Bodhi tree that was taken from India.

7
00:01:24,480 --> 00:01:32,840
It was planted and that is the Bodhi tree that you have seen behind me.

8
00:01:32,840 --> 00:01:37,440
And more importantly it is the Bodhi tree that was brought back to India, to Bongaya

9
00:01:37,440 --> 00:01:46,920
where the original Bodhi tree stood when it was killed, there were various stories about

10
00:01:46,920 --> 00:01:53,600
how the Bodhi tree was killed and it was brought back at least on one occasion too.

11
00:01:53,600 --> 00:01:59,080
And on several occasions it was brought back to India to be re-planted.

12
00:01:59,080 --> 00:02:07,240
So this is actually the Bodhi ancestor and both the descendant and the ancestor of the Bodhi

13
00:02:07,240 --> 00:02:10,360
tree in India since it was brought back in.

14
00:02:10,360 --> 00:02:19,040
It is a very important place and has great significance for both the Sri Lankan Buddhists

15
00:02:19,040 --> 00:02:26,800
and Buddhists around the world.

16
00:02:26,800 --> 00:02:34,840
So today I thought I'd talk a little bit about what this means for us and what exactly

17
00:02:34,840 --> 00:02:40,040
the Bodhi tree means for Buddhists.

18
00:02:40,040 --> 00:02:48,000
Because on the one hand it doesn't mean anything, it's just a symbol, it's just a physical

19
00:02:48,000 --> 00:02:59,640
manifestation that occurred as a result of the Bodhi and his teachings and the followers

20
00:02:59,640 --> 00:03:06,200
who decided to bring the tree here to Sri Lanka as a symbol for the planting of Buddhism

21
00:03:06,200 --> 00:03:09,400
in this country.

22
00:03:09,400 --> 00:03:12,040
But in the end it is really just a tree.

23
00:03:12,040 --> 00:03:16,120
It doesn't have consciousness.

24
00:03:16,120 --> 00:03:21,480
So some people might not understand, even people who practice the Buddhist teachings, might

25
00:03:21,480 --> 00:03:33,720
not understand the significant or the importance of the tree.

26
00:03:33,720 --> 00:03:50,080
But on the other hand it really is a focusing entity just as a meditation objects, things

27
00:03:50,080 --> 00:03:56,440
that focus our attention bring our minds back to a single pointedness.

28
00:03:56,440 --> 00:04:04,480
Keep our minds from being distracted, keep our minds from being pulled away by the objects

29
00:04:04,480 --> 00:04:16,400
of the sense, pulled away by the world, pulled away by worldliness.

30
00:04:16,400 --> 00:04:22,360
And so for a cynical person they might say it's simply a physical object that really

31
00:04:22,360 --> 00:04:30,680
is meaningless and that people who come here to worship or make determinations under the

32
00:04:30,680 --> 00:04:34,120
Bodhi tree make wishes and so on.

33
00:04:34,120 --> 00:04:37,000
People who come to meditate here are just fooling themselves, they could do the same

34
00:04:37,000 --> 00:04:43,880
at home if they really want to meditate, they can meditate at home.

35
00:04:43,880 --> 00:04:52,280
But this is really, I think, overly cynical and disingenuous really because our mind

36
00:04:52,280 --> 00:04:59,160
relies on concept, our whole life depends on concept.

37
00:04:59,160 --> 00:05:06,560
And so depending on the concepts that we choose, the concepts that we live by, this

38
00:05:06,560 --> 00:05:10,600
will determine our practice, it will determine our course.

39
00:05:10,600 --> 00:05:15,440
So it's one thing to say, claim to know that everything is impermanence, suffering in

40
00:05:15,440 --> 00:05:16,440
non-self.

41
00:05:16,440 --> 00:05:21,880
It's only body and mind and the five aggregates and so on and it's only seeing hearings

42
00:05:21,880 --> 00:05:24,720
now interesting feeling and thinking.

43
00:05:24,720 --> 00:05:28,600
But unless you're actually an aura hunt and actually become enlightened, you're still

44
00:05:28,600 --> 00:05:34,360
living in concept and you'll say that things and then you'll still become intoxicated

45
00:05:34,360 --> 00:05:39,960
by concept, you'll become intoxicated by pleasant sights, pleasant sounds, pleasant

46
00:05:39,960 --> 00:05:49,200
smells and so on and you'll become, you'll still become deluded by other concepts by the

47
00:05:49,200 --> 00:05:56,400
concepts of a human being, of male and female and become intoxicated by food and drink

48
00:05:56,400 --> 00:06:04,320
and entertainment by beauty and so on.

49
00:06:04,320 --> 00:06:10,160
And so this concept of the tree, regardless of what you think of it, it has the effect

50
00:06:10,160 --> 00:06:14,280
of bringing our minds back to the dhamma, of reminding us of the dhamma.

51
00:06:14,280 --> 00:06:22,680
But a better object to be the inspiration for a dhamma talk, like this one, for example,

52
00:06:22,680 --> 00:06:28,240
without this tree, I wouldn't be giving this talk, I wouldn't have the significance

53
00:06:28,240 --> 00:06:37,520
in the meaning and this wouldn't capture the attention of the audience in the same way.

54
00:06:37,520 --> 00:06:43,760
But because this is such a holy place, it's considered to be such an important symbol

55
00:06:43,760 --> 00:06:48,960
or an important object, important entity, for Buddhist people.

56
00:06:48,960 --> 00:06:53,480
When we come here, we give our whole attention to the dhamma and we forget about the world,

57
00:06:53,480 --> 00:07:03,200
and forget about intoxicating sights and sounds and so on.

58
00:07:03,200 --> 00:07:09,680
So on the way up here, I was in order to prepare myself and get kind of in the mood myself,

59
00:07:09,680 --> 00:07:16,000
which I was reading through in the Gmanikaya and I just happened upon the dhamma diadasuta,

60
00:07:16,000 --> 00:07:25,520
which I thought would be a good coincidentally, a good basis for this talk, to talk about these sorts of things.

61
00:07:25,520 --> 00:07:33,120
Because in the dhamma diadasuta, the Buddha distinguishes between someone who is an heir of the Buddha

62
00:07:33,120 --> 00:07:37,000
in regards to material character, material requisite.

63
00:07:37,000 --> 00:07:42,240
And one who is an heir to the Buddha in regards to the dhamma.

64
00:07:42,240 --> 00:07:47,640
So the Buddha said, be an heir to my dhamma, inherit my dhamma, my teaching.

65
00:07:47,640 --> 00:07:58,880
Don't inherit my physical material wealth, my material riches.

66
00:07:58,880 --> 00:08:10,640
Be an inheritan, be an heir to my dhamma, to my teaching, inherit my dhamma, my teaching.

67
00:08:10,640 --> 00:08:16,840
And so this is useful to actually remind us not to take these symbols too seriously,

68
00:08:16,840 --> 00:08:25,640
or not to take the object, physical object too seriously.

69
00:08:25,640 --> 00:08:32,040
So how we should see the Bodhi tree is as a symbol and as something to remind us and something to encourage us in a good way.

70
00:08:32,040 --> 00:08:40,600
We shouldn't see it as the highest goal or the highest object of worship.

71
00:08:40,600 --> 00:08:44,280
Actually the highest object of worship in the Buddha's teaching is the dhamma.

72
00:08:44,280 --> 00:08:49,480
Even more so than the Buddha, certainly more so than the Bodhi tree.

73
00:08:49,480 --> 00:08:55,720
The Buddha himself, when he became enlightened, he thought, who will I worship now, who will I pay respect to?

74
00:08:55,720 --> 00:08:59,240
I mean that there's no one in the world who I can possibly pay respect to,

75
00:08:59,240 --> 00:09:01,040
who would be worthy of my respect.

76
00:09:01,040 --> 00:09:05,960
But this dhamma, this reality that I realized, this is worthy of my respect.

77
00:09:05,960 --> 00:09:15,880
So even the Buddha paid respect and put the dhamma as higher than himself.

78
00:09:15,880 --> 00:09:19,640
So it would be wrong for us to come here and think that by coming to Anaradapur,

79
00:09:19,640 --> 00:09:23,160
or by coming to be under the Bodhi tree, now we are real Buddhists,

80
00:09:23,160 --> 00:09:27,240
so now we're living by the dhamma, or we're heirs to the Buddha.

81
00:09:27,240 --> 00:09:35,320
We are descendants of the Buddha because we come and bow down and pay respect to the Bodhi tree.

82
00:09:35,320 --> 00:09:45,800
Rather we should take it as an encouragement to practice the Buddha's teaching and become true heirs to the Buddha.

83
00:09:45,800 --> 00:09:53,560
So he said in this way, the heirs to my dhamma don't be heirs to my material requisites,

84
00:09:53,560 --> 00:10:01,400
because the Buddha, in the Buddha's time and since the time in the Buddha,

85
00:10:01,400 --> 00:10:08,680
there has been quite a great gains accumulated by the Buddha and his followers.

86
00:10:08,680 --> 00:10:13,240
If you look, this is something, this is some great gain for us to have such a beautiful

87
00:10:13,240 --> 00:10:24,520
ancient Buddhist holy place, the stone and the architecture,

88
00:10:25,240 --> 00:10:32,360
and even the money in here that comes in and all of the requisites that we get by being involved

89
00:10:32,360 --> 00:10:36,280
with such a place involved with the Buddha's teaching.

90
00:10:36,280 --> 00:10:43,720
It's very easy for us to become intoxicated by the material gain,

91
00:10:45,080 --> 00:10:48,600
so even in the Buddha's time, amongst who were living with the Buddha,

92
00:10:48,600 --> 00:10:52,440
would find that they were quite well taken care of by the lay people.

93
00:10:58,200 --> 00:11:02,680
And this can happen even more so nowadays when the Buddha isn't around to remind us

94
00:11:02,680 --> 00:11:09,640
that entire monasteries will become intoxicated and caught up by gain and fame and praise

95
00:11:09,640 --> 00:11:16,280
and esteem by the lay people, so in a place like this he might find

96
00:11:19,880 --> 00:11:24,920
sometimes people becoming intoxicated by the opulence and the greatness.

97
00:11:26,680 --> 00:11:30,360
It can also happen that people become intoxicated, as I said, with the place

98
00:11:30,360 --> 00:11:33,480
itself thinking that by being and living in the holy place, there's somehow

99
00:11:34,280 --> 00:11:35,640
associated with the Buddha.

100
00:11:37,400 --> 00:11:42,040
The Buddha said like a spoon that never tastes the flavor of the soup, it doesn't matter how close

101
00:11:42,040 --> 00:11:49,800
you get. And you could live under the Modi tree like the birds and the monkeys and the squirrels

102
00:11:49,800 --> 00:11:55,240
and still never become enlightened like them. It's just like those people who believe that

103
00:11:55,240 --> 00:12:01,160
by bathing in the river Ganges, the Ganga River, by bathing in the river, you become purified.

104
00:12:01,160 --> 00:12:05,560
And the Buddha said, well in that case, why aren't the turtles and the fishes all pure?

105
00:12:05,560 --> 00:12:07,080
Why aren't they enlightened so?

106
00:12:11,720 --> 00:12:17,640
And so the material is just material. In the end it is just rope on nana and it's something

107
00:12:17,640 --> 00:12:23,240
that we should see that in this way. And so the Buddha described this, why this

108
00:12:23,240 --> 00:12:30,680
Suta, I think, is a really good basis for our discussion today is because the Buddha used this

109
00:12:30,680 --> 00:12:36,680
as a basis to remind the monks or tell them, relate to the monks, his own search.

110
00:12:38,040 --> 00:12:43,400
And this also helps to explain to the audience and people listening, what is the significance of

111
00:12:43,400 --> 00:12:48,280
the Modi tree? Where does it come from? The Buddha explained in the Damadehada Suta,

112
00:12:48,280 --> 00:12:56,360
gave one explanation of where this all came from, who he was and what his search was.

113
00:12:59,800 --> 00:13:05,480
He said, before I became enlightened, before he became enlightened, he also was searching for things

114
00:13:07,800 --> 00:13:11,640
for material things. He was also intoxicated by worldly things.

115
00:13:11,640 --> 00:13:20,280
And the way he put it was like this, when I was unenlightened,

116
00:13:22,120 --> 00:13:32,280
the things I was looking for were caught up with so much suffering and so much danger.

117
00:13:33,560 --> 00:13:37,800
He said, myself being subject to birth, I was seeking after things subject to birth.

118
00:13:37,800 --> 00:13:43,160
My self-subject to all the age, I was seeking after things subject to aging.

119
00:13:44,520 --> 00:13:49,320
My self-subject to sickness, I was seeking out things subject to sickness. My self-subject to death,

120
00:13:50,280 --> 00:13:52,280
I was seeking after things subject to death.

121
00:13:55,480 --> 00:14:02,280
It was ridiculous on both sides. My self-subject to sadness, sorrow, suffering.

122
00:14:02,280 --> 00:14:08,920
My self-subject to defilement, I was seeking after things subject to defilement.

123
00:14:12,840 --> 00:14:17,960
So the realization that he got was that this is absurd on both sides.

124
00:14:18,840 --> 00:14:22,760
Even if the things you were seeking after were permanent,

125
00:14:22,760 --> 00:14:31,720
if you yourself are subject to aging, if you yourself are subject to death, so you can't keep them.

126
00:14:32,280 --> 00:14:37,880
But not in none of them are themselves permanent, so either you die first or the things that you

127
00:14:37,880 --> 00:14:39,720
cling to disappear from.

128
00:14:43,640 --> 00:14:47,400
You yourself are defiled, you cling to other things that are defiled, you cling to people,

129
00:14:47,400 --> 00:14:55,640
you cling to your spouse, you cling to your family, to your friends, you cling to places,

130
00:14:55,640 --> 00:15:01,800
you cling to money, you cling to wealth and possessions. And all of these things are subject to

131
00:15:01,800 --> 00:15:08,280
defilement, subject to sickness, subject to death, subject to disappearance.

132
00:15:09,960 --> 00:15:12,040
If it's people, then they get old sick and die.

133
00:15:12,040 --> 00:15:19,320
And this is called the Anariya Parigasana, the ignoble quest.

134
00:15:20,920 --> 00:15:25,720
And he realized that this was a quest that was not of any benefit.

135
00:15:27,080 --> 00:15:32,040
And actually in Hindu and Buddhist culture, as the Buddha was coming from a Hindu culture,

136
00:15:32,040 --> 00:15:35,640
he would have realized that actually this isn't just a one-time thing.

137
00:15:35,640 --> 00:15:41,960
This is a cycle where continuing this cycle, it was something that was actually quite

138
00:15:43,880 --> 00:15:48,520
well known at the time, that this was the cycle of rebirth.

139
00:15:49,960 --> 00:15:56,040
If a person clings to these things, it's not just death once, it's death again and again and again,

140
00:15:56,040 --> 00:16:00,280
and again and again we cling to these things, we seek out for these things.

141
00:16:00,280 --> 00:16:06,040
We're born and we're taught from birth to seek out these things.

142
00:16:07,240 --> 00:16:10,680
As women were taught at the seekout men, as men were taught to seek out women,

143
00:16:11,400 --> 00:16:17,240
we're taught to seek out money, we're taught to seek out possessions,

144
00:16:18,280 --> 00:16:21,560
we're taught to seek out a house and a family to seek out children,

145
00:16:22,600 --> 00:16:25,320
we're taught to seek out luxury, we're taught to seek out wealth,

146
00:16:25,320 --> 00:16:31,960
we're taught to seek out learning and skill and handicraft, we're taught to seek out a job,

147
00:16:33,640 --> 00:16:37,640
and we will realize that actually what we're taught to seek out is all day sickness and death,

148
00:16:39,480 --> 00:16:41,800
we're taught to seek out negligence and defilement,

149
00:16:44,280 --> 00:16:49,400
and so we're taught to seek out rebirth, again and again and again.

150
00:16:49,400 --> 00:16:56,280
The things that we're taught in our culture, by our society, by media, by the world,

151
00:16:58,440 --> 00:17:01,480
we're taught to seek out a never-ending cycle of

152
00:17:02,680 --> 00:17:04,760
dissatisfaction and disappointment,

153
00:17:08,520 --> 00:17:12,520
and again and again and again we forget, so again and again we think this is something new and we cling

154
00:17:12,520 --> 00:17:21,480
to it and then find only disappointment, meaninglessness. If I'm nothing, if I'm no benefit

155
00:17:21,480 --> 00:17:25,400
and ourselves subjected to foundment, we become angry, we become greedy, we become

156
00:17:26,680 --> 00:17:32,760
addicted and attached and conceded and confused and run around in circles,

157
00:17:35,880 --> 00:17:41,400
and we call the suffering for ourselves suffering forever, and in the end it's

158
00:17:41,400 --> 00:17:47,320
meaning that and then we get old, we get sick and we die, and then it starts all over again.

159
00:17:50,440 --> 00:17:53,880
So then he thought to himself, well what is the answer, there's only one answer and that's

160
00:17:53,880 --> 00:18:00,520
to seek out what is free from this thing, to seek out something that is free from

161
00:18:00,520 --> 00:18:06,520
where it's free from old age, free from sickness, free from death, free from sorrow and free from

162
00:18:06,520 --> 00:18:17,720
the foundment, and so this is the story of the Buddha that when he was living as a prince,

163
00:18:18,520 --> 00:18:22,600
he decided that he would give up all of this, all of the luxury and the wonders that he had,

164
00:18:22,600 --> 00:18:34,600
the palace, his wife, his child, his father, his mother, his title as a prince,

165
00:18:34,600 --> 00:18:45,160
to give it all up, seeing that all of this was just ephemeral manifestations of physical and

166
00:18:45,160 --> 00:18:50,840
mental reality of experience, coming and going, none of it was going to last, none of it was going

167
00:18:50,840 --> 00:18:56,520
to satisfy him, but there's nothing in the world that could possibly satisfy him.

168
00:18:57,320 --> 00:19:02,760
So he decided to seek out the non arisen, to find something that hadn't arisen,

169
00:19:02,760 --> 00:19:10,840
because everything that arises passes away, and so he shaved off his head, and he shaved off his

170
00:19:10,840 --> 00:19:20,440
hair and beard, gave up his princely robe, princely clothes, put on rags, wandered for arms,

171
00:19:22,120 --> 00:19:27,800
and he says he wandered the whole country looking for a teacher, and he found two teachers,

172
00:19:27,800 --> 00:19:37,960
the first teacher he found, Alara Kalama, taught him how to gain some

173
00:19:40,040 --> 00:19:47,400
immaterial, formless John or to practice some kind of tranquility meditation,

174
00:19:48,280 --> 00:19:51,320
that focused the mind and to enter into the sphere of nothingness,

175
00:19:52,920 --> 00:19:56,760
and he practiced it, he learned it by heart, the teaching, and then he practiced it,

176
00:19:56,760 --> 00:20:02,680
and eventually he realized this state, so he went, the teacher would have been someone,

177
00:20:02,680 --> 00:20:09,080
some sort of Hindu yogi who had some high attainment, this attainment of the sphere of nothingness,

178
00:20:09,720 --> 00:20:15,880
because in India even now he will find such, such teacher, and he attained the same state

179
00:20:15,880 --> 00:20:20,760
that his teacher attained, but he realized that this wasn't something that was under arisen,

180
00:20:20,760 --> 00:20:24,920
it was something that was arisen, and it didn't lead to freedom from suffering,

181
00:20:24,920 --> 00:20:31,240
it only led to rebirth in the formless realm, and because of his attainments and his

182
00:20:31,240 --> 00:20:35,080
perfection of mind, he knew that he had been there before and passed away from it,

183
00:20:37,160 --> 00:20:42,200
he knew that this wasn't the way out of suffering, this wasn't eternal, because it arose,

184
00:20:42,200 --> 00:20:46,520
the perception of nothingness arose in his mind, and then it ceases,

185
00:20:46,520 --> 00:20:54,600
and so he left that teacher and he went to find another teacher and it was Utaka,

186
00:20:56,280 --> 00:21:02,840
Utaka Ramakupa, who himself hadn't realized any special attainment that had a teacher who had

187
00:21:02,840 --> 00:21:10,120
who had passed away, Rama, and he taught the bodies at the Rama's teachings that led all the way

188
00:21:10,120 --> 00:21:17,720
to the highest tranquility, spiritual attainment, which is the attainment of neither perception

189
00:21:17,720 --> 00:21:27,320
or non-perception, and he actually was able to attain this as well, even though his own teacher,

190
00:21:27,320 --> 00:21:36,360
Utaka Ramakupa wasn't able to attain this result, but he realized that this also wasn't

191
00:21:36,360 --> 00:21:43,720
was until end of suffering, it wasn't freedom from suffering, the state only led to the state

192
00:21:43,720 --> 00:21:48,680
of neither perception nor non-perception, which is the highest tranquility state,

193
00:21:50,440 --> 00:21:57,560
and if he died there, he would be born in the realm, the formless God realm of neither perception

194
00:21:57,560 --> 00:22:03,480
nor non-perception, but it's still a state that arises in the mind, it still has mind arising,

195
00:22:03,480 --> 00:22:13,320
and he realized that any state that he entered into was still subject to what you might call

196
00:22:13,320 --> 00:22:18,520
old age sickness and death, for me in the sense that it gets old, it starts to break up and it passes

197
00:22:18,520 --> 00:22:26,920
away, the state disappears, the state ends, and this later came to be the Buddhist teaching

198
00:22:26,920 --> 00:22:35,000
on Sankara Dooka, that even spiritual attainment of any kind, even the attainment of Godhood

199
00:22:36,520 --> 00:22:41,160
can be considered suffering, because it's something you have to work very, very hard at,

200
00:22:42,200 --> 00:22:45,640
they don't, it doesn't, the text don't talk about, and the Buddha didn't talk about how

201
00:22:45,640 --> 00:22:51,240
difficult it was, for him to reach those states, but he must have worked quite hard to purify his mind

202
00:22:51,240 --> 00:22:58,280
to focus his mind and be able to enter those states.

203
00:23:01,560 --> 00:23:06,520
And yet he realized that it was all pointless, and it had no lasting effect,

204
00:23:08,200 --> 00:23:16,200
it had the result of whatever work he had put into it, and when the work was finished and the

205
00:23:16,200 --> 00:23:31,960
results were finished, and so he decided that there was no way that he could find this teaching,

206
00:23:31,960 --> 00:23:36,920
you'd find this out from anyone else, he had to give up everything, he had to give up his

207
00:23:36,920 --> 00:23:41,080
teachers, he had to give up all of his learning, all of his knowledge, he had to, in the end,

208
00:23:41,080 --> 00:23:48,520
give up all of reality, all of experience, and he realized that every experience has to arise,

209
00:23:48,520 --> 00:23:54,600
that arises, has to see, and so he knew intuitively that this was something he had to find for himself,

210
00:23:55,560 --> 00:24:01,240
because there was no one on earth who could teach it, and so he went and found this place,

211
00:24:02,120 --> 00:24:06,040
he wandered through India, looking for, no longer looking for a teacher, but looking for a

212
00:24:06,040 --> 00:24:13,720
suitable place to strive, and he found a place that was most likely very similar to this place,

213
00:24:15,000 --> 00:24:22,200
in Sainani Gama, near Gaya, in a place that is now called Buddha Gaya,

214
00:24:23,880 --> 00:24:27,640
and it was near Uru Wala, at the time there was a place called Uru Wala near there,

215
00:24:27,640 --> 00:24:38,200
and this is where he laid out, he sat down to strive, under a tree, which later became called

216
00:24:38,200 --> 00:24:43,000
the Bodhi tree, and it's the type of tree that you see behind me, and that we have all around here,

217
00:24:45,560 --> 00:24:51,800
many Bodhi trees, and in India if you go to India especially in Uttar Pradesh and Bihar,

218
00:24:51,800 --> 00:24:58,600
you know, see Bodhi trees lining the road, it's the natural habitat of the Bodhi tree,

219
00:24:58,600 --> 00:25:04,440
so it was a tree that was actually quite common in that area, most likely, at least now it is,

220
00:25:04,440 --> 00:25:09,000
if you go there today you'll see Bodhi trees throughout the country,

221
00:25:13,800 --> 00:25:19,000
and he said, and I found that, I found the supreme freedom from bondage, I found

222
00:25:19,000 --> 00:25:25,400
enlightened, I found freedom from suffering from myself, and this is what I teach,

223
00:25:34,840 --> 00:25:38,840
and the suit goes on to talk about something that is even perhaps more important, it's the

224
00:25:38,840 --> 00:25:45,080
actual teaching and the actual development and cultivation of tranquility and insight,

225
00:25:45,080 --> 00:25:55,080
and it relates to the topic of this talk that I'm giving in regards to how important a place

226
00:25:55,080 --> 00:26:01,480
like this is, and as I said it's important because it takes us away from the world, it takes us

227
00:26:01,480 --> 00:26:09,160
away from our intoxications with the five senses, with sight, sound, smell, taste, and feeling,

228
00:26:09,160 --> 00:26:15,560
and this is the teaching that the Buddha gave in the Dhamma Dayatas with them, after reminding

229
00:26:15,560 --> 00:26:20,440
the monks of his attainment of enlightenment, which is really the significance of the Bodhi tree

230
00:26:20,440 --> 00:26:27,800
in a historical context, he began to explain to them the Dhamma that he had realized, and that is

231
00:26:27,800 --> 00:26:40,600
that there are these five kamungun, which can be translated as these chords or these ropes of

232
00:26:40,600 --> 00:26:47,240
sensuality, this is sights that are intoxicating, sounds that are intoxicating, smells that are

233
00:26:47,240 --> 00:26:51,400
intoxicating, tastes that are intoxicating, and feelings that are intoxicating,

234
00:26:51,400 --> 00:27:03,320
and he pointed this out as the defining factor of our spiritual life, or a defining factor of

235
00:27:03,320 --> 00:27:21,960
our spiritual life, he taught that any meditator who is still caught up in these things will not be

236
00:27:21,960 --> 00:27:28,520
able to understand the truth, will be caught up by Mara, he said just like a deer that is

237
00:27:28,520 --> 00:27:37,800
caught up in a snare or many snare, a heap of snare as he said, a person who is caught up with

238
00:27:37,800 --> 00:27:42,760
these things will never be able to become free, will never be able to live as they play, will

239
00:27:42,760 --> 00:27:49,880
never be free, the key to why we're not able to understand the truth is our infatuation with

240
00:27:49,880 --> 00:27:57,080
the senses, even as meditator, when we undertake a meditation course, the first thing we have to do

241
00:27:57,080 --> 00:28:03,400
is become free from these things, the senses, our first task is to pull ourselves out of these,

242
00:28:05,880 --> 00:28:09,640
so that the things that we see are just seeing, the things that we hear are just hearing,

243
00:28:11,400 --> 00:28:15,560
this is why the Buddha gave to Bahia, this is the very core of his teaching,

244
00:28:15,560 --> 00:28:22,360
deep pei, deep kamat dang dang, what do you see, let it be just seeing, what you hear, let it

245
00:28:22,360 --> 00:28:28,920
be just hearing, what you sense, let it be just sensing, if you can do this, there will be

246
00:28:28,920 --> 00:28:37,880
no attachment for you, you will find no self in these things, without doing this we can't

247
00:28:37,880 --> 00:28:42,920
hope to understand the truth, and we can't hope to live in freedom from suffering, we can't hope

248
00:28:42,920 --> 00:28:49,880
to understand our causes of suffering, we can't hope to understand our problem, our minds will be

249
00:28:49,880 --> 00:29:03,480
caught up by Mara, and so the way that we become free from suffering is to free ourselves

250
00:29:03,480 --> 00:29:12,040
from the infatuation with world in this infatuation with the world around us, our infatuation

251
00:29:12,040 --> 00:29:18,120
with sights and sounds and smells and tastes and feeling, our infatuation with objects of the sense,

252
00:29:18,120 --> 00:29:24,280
even our infatuation with ideas of becoming this and becoming that, of not being this and not being

253
00:29:27,080 --> 00:29:31,800
and this is what we do in the practice of meditation, this is why we teach the meditators to do

254
00:29:31,800 --> 00:29:39,400
something that seems so menial and even meat pointless, when you see something to just remind

255
00:29:39,400 --> 00:29:47,000
yourself seeing, when you hear something to remind yourself hearing, when you walk to just know

256
00:29:47,000 --> 00:29:52,920
that you're walking, when you stand to just know that you're standing, whenever experience arises

257
00:29:52,920 --> 00:29:59,080
to see it just for what it is, this is to pull ourselves out of the infatuation in the senses,

258
00:30:01,320 --> 00:30:06,840
in fact all of our addiction, all of our suffering, all of our problems in life,

259
00:30:08,920 --> 00:30:14,760
come back and stem from the five senses or the six senses, this is the Buddhist teaching on

260
00:30:14,760 --> 00:30:20,440
the teacher samupada, he said also in the diadis, the dhamma diadis, earlier, he said after he

261
00:30:20,440 --> 00:30:26,120
became enlightened, the reason why he didn't, he decided not to teach, the reason why he decided

262
00:30:26,120 --> 00:30:30,440
not to teach is because he saw that the world was infatuated with like world in this he called,

263
00:30:31,160 --> 00:30:36,120
caught up by world in this and he said there's no way they can understand dependent origination,

264
00:30:37,000 --> 00:30:40,600
this is actually the words that he used, was what they won't be able to understand is dependent

265
00:30:40,600 --> 00:30:46,680
origination, and by these words of the Buddha we can see how important this doctrine is of

266
00:30:46,680 --> 00:30:59,080
idhapachayata, the arising according to cause and effect, casual or co-erising,

267
00:30:59,960 --> 00:31:05,240
that some things arise based on other things, and without those causes arising, they will not

268
00:31:05,240 --> 00:31:15,800
arise the effect. This is the teaching on how namarupa leads to the six senses and the six senses

269
00:31:15,800 --> 00:31:22,200
lead to contact and contact leads to feeling and feeling leads to craving and craving leads to

270
00:31:22,200 --> 00:31:29,320
clinging and clinging leads to seeking, seeking leads to becoming leads to birth,

271
00:31:29,320 --> 00:31:42,440
and if we can understand this teaching, if we can practice and see experience piece by piece by

272
00:31:42,440 --> 00:31:47,640
piece that the sights and the sounds and the smells are just sights and sounds and smells,

273
00:31:48,280 --> 00:31:53,400
the feelings that arise are just feeling, when you see something they will arise a pleasurable

274
00:31:53,400 --> 00:32:05,560
or a displeasure, unpleasant sensation, or else they will arise a neutral sensation and then

275
00:32:05,560 --> 00:32:11,560
because of that they will arise craving for it or clinging to it, desire for it or attachment.

276
00:32:12,680 --> 00:32:15,400
When it's there you'll become attached to it, when it's gone you'll want,

277
00:32:15,400 --> 00:32:25,720
when it's there you'll become a verb and become displeased with it and when it's gone you'll become

278
00:32:25,720 --> 00:32:33,720
afraid of it. All of our problems in life they come down to this cause and effect relationship.

279
00:32:35,000 --> 00:32:39,080
So our practice is simply when we see to know that we're seeing, when we have the feelings,

280
00:32:39,080 --> 00:32:42,680
to know that we have the feeling, when we have the craving to know that we have the craving,

281
00:32:42,680 --> 00:32:47,640
when we have the clinging, when we want something, when we want to chase after something,

282
00:32:47,640 --> 00:32:53,000
when we're seeking something to know that we're seeking it, to break up the sequence,

283
00:32:53,560 --> 00:33:01,640
to change the sequence, to interfere with the sequence so that it doesn't continue,

284
00:33:02,120 --> 00:33:06,760
so that when seeing is just seeing and singing is not something pleasurable,

285
00:33:06,760 --> 00:33:13,800
when pleasure arises, it's only pleasure and do not, it's not something worth clinging to,

286
00:33:13,800 --> 00:33:19,640
when craving arises, to know that it's not worth clinging, when clinging arises, to know that

287
00:33:19,640 --> 00:33:25,000
it's not worth seeking, when seeking arises, to know that we should not continue seeking,

288
00:33:25,960 --> 00:33:29,000
to know that we are seeking and to see the suffering that comes from seeking.

289
00:33:31,240 --> 00:33:35,480
If we can break experience up in this way with everything that we're clinging to,

290
00:33:35,480 --> 00:33:41,160
things that we like and things that we dislike, our whole lives will become free from suffering,

291
00:33:42,120 --> 00:33:46,600
and we'll be able to realize the truth of the Buddhist teaching, the truth of reality,

292
00:33:47,720 --> 00:33:51,640
we'll be able to live according to the truth of reality, we'll be able to see this whole world

293
00:33:52,360 --> 00:33:58,520
just for what it is, just how we think we live our lives, we think that we live in the world,

294
00:33:58,520 --> 00:34:04,680
seeing, hearing, smelling, tasting, thinking, but so much of our lives is not here, it's not now,

295
00:34:05,400 --> 00:34:11,080
even though we're here and now, sitting on the sand, seeing, hearing, smelling, tasting, feeling,

296
00:34:11,080 --> 00:34:18,120
and thinking, our minds are so often caught up in what is just in the end the sixth sense,

297
00:34:18,120 --> 00:34:25,480
the sense of the mind thinking, caught up in ideas and papanjas, this diversification,

298
00:34:25,480 --> 00:34:32,200
or making more of things than they actually are, called the vitting projections and ideas and concept.

299
00:34:35,160 --> 00:34:38,520
The concepts like the Bodhi tree or concepts like the Buddha,

300
00:34:39,720 --> 00:34:44,360
even these concepts in the end we have to be free from, and we have to see that even here

301
00:34:44,360 --> 00:34:52,920
under the Bodhi tree, all that we have is body and mind, so this is perhaps one of the most ideal

302
00:34:52,920 --> 00:34:58,600
places for coming to realize this, because here we have this symbol of enlightenment to remind us,

303
00:34:58,600 --> 00:35:03,720
anytime our mind wanders, we have light, it's as though we have the Buddha watching over us,

304
00:35:03,720 --> 00:35:07,480
and we know that anytime our mind wanders into sensuality or aversion,

305
00:35:08,840 --> 00:35:14,120
we will become embarrassed and remind ourselves, and we will be reminded of the severity

306
00:35:14,120 --> 00:35:20,120
and the importance of our practice, and also it's a place that is quite peaceful and free from

307
00:35:20,120 --> 00:35:25,080
sensuality for this reason, because no one here dares to do or say things that are

308
00:35:26,360 --> 00:35:33,880
chaotic or destructive, and so we should rather than just coming here to bow down their

309
00:35:33,880 --> 00:35:38,600
respect, even though these are useful and worthwhile things, we should now take this opportunity,

310
00:35:39,240 --> 00:35:43,960
now we have this great opportunity, we should take it and practice meditation, so now

311
00:35:43,960 --> 00:35:51,160
we will continue on and together we can do a group meditation, we'll try to do first mind

312
00:35:51,160 --> 00:36:13,080
for frustration and then walking and then sitting, so that's all for today.

