1
00:00:00,000 --> 00:00:11,480
So today is Asala Habutcha in the formun of Asala, and it's the time when the Lord Buddha

2
00:00:11,480 --> 00:00:22,480
had traveled from Buddha, Gaya, which is the place where Lord Buddha became enlightened,

3
00:00:22,480 --> 00:00:36,480
all the way to the Isipatana, dear part, just outside of Varanasi, Varanasi as we chant them.

4
00:00:36,480 --> 00:00:41,240
And he went this distance because he was looking for someone who would be able to understand

5
00:00:41,240 --> 00:00:43,600
his teaching.

6
00:00:43,600 --> 00:00:50,680
At that time he figured that most people wouldn't be able to understand this very difficult

7
00:00:50,680 --> 00:00:54,360
to understand what she had discovered.

8
00:00:54,360 --> 00:01:00,240
And so in the beginning he thought not to teach until it came to him that there would

9
00:01:00,240 --> 00:01:09,680
be people who understood the teaching, who could understand the teaching.

10
00:01:09,680 --> 00:01:17,200
And first he thought of his old teachers who had taught him tranquility meditation, concentration

11
00:01:17,200 --> 00:01:25,120
meditation, meditation based on conceptual objects.

12
00:01:25,120 --> 00:01:30,920
But then he realized that they had passed away already and that they wouldn't be,

13
00:01:30,920 --> 00:01:35,600
they had gone on to be born in the Brahma realms where of course they couldn't practice

14
00:01:35,600 --> 00:01:36,600
inside meditation.

15
00:01:36,600 --> 00:01:48,200
They couldn't practice to understand about suffering and the cause of suffering.

16
00:01:48,200 --> 00:01:53,240
And so then he thought of these five monks who had been practicing asceticism within

17
00:01:53,240 --> 00:02:08,480
for six years and through his mental power he was able to determine that they were in

18
00:02:08,480 --> 00:02:25,440
self-modification.

19
00:02:25,440 --> 00:02:33,440
When he arrived there the five ascetics they made a determination among themselves that

20
00:02:33,440 --> 00:02:42,720
they would not receive their old teacher with any sort of measure of respect because what

21
00:02:42,720 --> 00:02:50,840
had happened was the Lord Buddha had given up austerity in favor of a more moderate way

22
00:02:50,840 --> 00:02:52,880
of practice.

23
00:02:52,880 --> 00:03:00,560
And basically what that means is he just gave up any attachment to such practices.

24
00:03:00,560 --> 00:03:08,320
The practices of not eating, the practices of torturing oneself in various ways where

25
00:03:08,320 --> 00:03:18,600
they would sit over a fire or hang upside down, hold one arm up for a year or so and expose

26
00:03:18,600 --> 00:03:27,600
themselves to cold, expose themselves to heat, expose themselves to sharp thorns and so on.

27
00:03:27,600 --> 00:03:32,280
And he had given all of this up and a part of it was going back to eating because he realized

28
00:03:32,280 --> 00:03:40,280
that in order to continue practicing for a long term, not eating is of course not a viable

29
00:03:40,280 --> 00:03:48,480
alternative and it leads directly to death which of course is not a very useful thing.

30
00:03:48,480 --> 00:03:55,480
And so the Lord Buddha started eating again and this upset these ascetics because they

31
00:03:55,480 --> 00:04:00,520
thought this was leading back to the indulgent life and for most people it probably would

32
00:04:00,520 --> 00:04:01,520
have.

33
00:04:01,520 --> 00:04:06,960
For most people for all of them who couldn't control themselves.

34
00:04:06,960 --> 00:04:11,360
Going back to eating would have of course allowed their defilements, the key laser which

35
00:04:11,360 --> 00:04:18,600
still existed and to rise again and without any tools any practice to allow them to control

36
00:04:18,600 --> 00:04:23,000
those or to remove those defilements.

37
00:04:23,000 --> 00:04:27,160
They would have may have very easily gone back to the home life and may have very easily

38
00:04:27,160 --> 00:04:33,960
become attracted to objects of the sands and given up their interest in the holy life

39
00:04:33,960 --> 00:04:39,440
or in the ascetic life.

40
00:04:39,440 --> 00:04:45,280
And so we can see the only way that these ascetics were able to keep there and the

41
00:04:45,280 --> 00:04:50,520
only way many people are able to keep there or their religious life is simply by suppressing

42
00:04:50,520 --> 00:04:55,560
or repressing their desires and they do this in various ways by torturing themselves.

43
00:04:55,560 --> 00:05:04,360
Under penins or often the easiest way to do it was with tranquility meditation or concentration

44
00:05:04,360 --> 00:05:10,360
meditation which these men may or may not have been practicing but at any rate their

45
00:05:10,360 --> 00:05:16,520
way was to torture themselves to keep themselves in a constant state of pain so that they

46
00:05:16,520 --> 00:05:24,640
wouldn't be able to think of pleasure and some thought of somehow suppress these feelings.

47
00:05:24,640 --> 00:05:29,320
But anyway when the Lord would have came they were very upset and they didn't want to

48
00:05:29,320 --> 00:05:33,800
have anything to do with it until they saw how mindful he was.

49
00:05:33,800 --> 00:05:39,280
And we can see how this affects the way people look at us and relate to us even to this

50
00:05:39,280 --> 00:05:40,280
day.

51
00:05:40,280 --> 00:05:45,320
We can realize that when remindful people are much more willing to listen to us and pay

52
00:05:45,320 --> 00:05:51,960
attention to us and treat us kindly and with respect as opposed to when we're on mindful

53
00:05:51,960 --> 00:05:57,720
and distracted we can see we're actually causing suffering for other people just by being

54
00:05:57,720 --> 00:06:00,960
around them and they're not comfortable or happy being around us.

55
00:06:00,960 --> 00:06:07,000
But in this case the Lord would have been so mindful and clearly aware that they couldn't

56
00:06:07,000 --> 00:06:14,560
help but undertake all of the tokens of respect like taking his robe, taking his

57
00:06:14,560 --> 00:06:21,160
robe, preparing a seed, standing up to greet him and so on.

58
00:06:21,160 --> 00:06:26,960
And when they sat down the Lord Buddha and tried to instruct them and explained to them

59
00:06:26,960 --> 00:06:31,800
that he had become enlightened and they said how in the world could you become enlightened

60
00:06:31,800 --> 00:06:38,080
you would never you hadn't become enlightened when you were undertaking the ascetic practices

61
00:06:38,080 --> 00:06:41,960
how could you become enlightened when you've given them up?

62
00:06:41,960 --> 00:06:47,760
The Lord Buddha said again you know I've listened to me, I really have become enlightened

63
00:06:47,760 --> 00:06:54,120
and for three times he told them three times they derided him, they chided him, they

64
00:06:54,120 --> 00:07:01,160
tried to they poked fun at him or they denied the possibility.

65
00:07:01,160 --> 00:07:06,400
Lord Buddha then asked them if he had ever said this before to them, if he had ever said

66
00:07:06,400 --> 00:07:10,440
such a thing before, I mean was this the kind of thing that he would do would be play

67
00:07:10,440 --> 00:07:15,880
these kind of practical jokes on them or overestimate himself.

68
00:07:15,880 --> 00:07:19,320
And they realized at that point it was a turning point, they realized that no Lord Buddha

69
00:07:19,320 --> 00:07:26,360
would never have, or their teacher would never have done this before, he wasn't a custom

70
00:07:26,360 --> 00:07:32,320
to playing jokes or lying or tricking or overestimating himself, in fact he had always

71
00:07:32,320 --> 00:07:37,160
been of course a very straight and honest sort of person.

72
00:07:37,160 --> 00:07:40,680
So at this point they changed and they were then from then on willing to listen to what

73
00:07:40,680 --> 00:07:45,320
he had to say and that's when he started the Dhamma Chaka Palat in the Supa which is

74
00:07:45,320 --> 00:07:50,720
the first discourse of the Lord Buddha, Dhamma Chaka Palat in the Bhavat and I mean's

75
00:07:50,720 --> 00:07:58,000
turning, Jaka is the wheel and Dhamma is the teaching or the truth.

76
00:07:58,000 --> 00:08:02,840
So this is the turning wheel of the Dhamma, the turning the wheel of the Dhamma, this is

77
00:08:02,840 --> 00:08:11,720
what this discourse is about and it's a fairly simple teaching, the beginning is a fairly

78
00:08:11,720 --> 00:08:21,760
widely cited teaching, is this talk about the Dway May Pikouwe, there are these two extremes

79
00:08:21,760 --> 00:08:27,000
Anta means end or it actually is the cognate I think of the English word end and if you

80
00:08:27,000 --> 00:08:35,880
look at it the word looks very similar and end and I believe it's the same word but

81
00:08:35,880 --> 00:08:41,480
here we're looking at ends we mean extremes so if we see, if we have a stick the stick

82
00:08:41,480 --> 00:08:47,120
has two ends and so here we're talking about the two extremes of the stick, the two extremes

83
00:08:47,120 --> 00:08:54,040
of the holy life in this case and of course these two extremes are torturing oneself

84
00:08:54,040 --> 00:09:00,800
and sensual indulging. Now the Lord Buddha didn't often come back to this teaching so

85
00:09:00,800 --> 00:09:05,960
it's important to understand that actually we're not dealing with an extremely core

86
00:09:05,960 --> 00:09:11,160
teaching of the Lord Buddha, it's not that this is a core principle of Buddhism and it's

87
00:09:11,160 --> 00:09:15,480
often, why I say that is because it's often misused, they say that while the Buddha taught

88
00:09:15,480 --> 00:09:22,680
the middle way and it's true he did and he repeats it several times in the Deepika and

89
00:09:22,680 --> 00:09:29,080
he uses it in different ways, sometimes it's not saying yes and not saying no but giving

90
00:09:29,080 --> 00:09:36,080
up the whole idea of the question in a specific case but here he's, of course, it's

91
00:09:36,080 --> 00:09:40,120
clear why he's saying this because he's talking to people who are torturing themselves.

92
00:09:40,120 --> 00:09:46,600
So first he says that yes clearly sensual pleasure is not a good thing and he does this

93
00:09:46,600 --> 00:09:51,000
to reaffirm the fact that he hasn't gone back to indulgence in sensual pleasure because

94
00:09:51,000 --> 00:09:55,280
of course indulgence in sensual pleasure is something which leads to addiction, which

95
00:09:55,280 --> 00:10:03,040
leads to attachment, which leads to all sorts of things like acquiring and all sorts

96
00:10:03,040 --> 00:10:15,640
of giving birth to children and death and all sorts of attachments and all sorts of

97
00:10:15,640 --> 00:10:22,880
things that bind, birth of ties but then he also then he goes on to say well then the

98
00:10:22,880 --> 00:10:26,800
other side of the coin is that torturing oneself is also useless, it's clear that

99
00:10:26,800 --> 00:10:30,480
we can torture ourselves until we die and we're still being nothing from it, he said

100
00:10:30,480 --> 00:10:36,840
it was dukho and something which causes suffering, analeo, he know, he know it's something

101
00:10:36,840 --> 00:10:42,320
which is inferior or no sorry it's dukho, the other one is he know this one is dukho

102
00:10:42,320 --> 00:10:48,560
which means it's painful, analeo is not noble, there's nothing noble about torturing yourself

103
00:10:48,560 --> 00:11:00,240
so he was giving a fairly harsh criticism of this sort of practice as well and then

104
00:11:00,240 --> 00:11:03,840
he goes on to explain the middle way and this is the core of Buddhism, it's sometimes

105
00:11:03,840 --> 00:11:08,800
called the middle way but it's most often called the eightfold noble way, it's a noble

106
00:11:08,800 --> 00:11:16,920
path, it's a path which leads one to become noble, it leads one to become enlightened

107
00:11:16,920 --> 00:11:24,160
and so this is the eightfold noble path, then he goes on to explain the four noble truths

108
00:11:24,160 --> 00:11:33,800
and he just sort of breaks into it, it's not really explained very clearly but he's

109
00:11:33,800 --> 00:11:40,160
basically from this point on here he's got them, he's explained to them about what is

110
00:11:40,160 --> 00:11:44,280
the right path that they should be falling, following and then once they understand that

111
00:11:44,280 --> 00:11:48,720
this is actually a pretty profound thing, torturing oneself is actually a pretty simple

112
00:11:48,720 --> 00:11:52,920
and kind of stupid thing to do but here we have something which is quite profound, it's

113
00:11:52,920 --> 00:11:59,920
got right view, right thought, right speech, right action, right livelihood, right effort,

114
00:11:59,920 --> 00:12:05,360
right mindfulness and right concentration, it's not something that's easily grasped, so

115
00:12:05,360 --> 00:12:10,560
now he's got them and then he starts to go on to the four noble truths and of course

116
00:12:10,560 --> 00:12:16,760
the four noble truths are Dukha, the truth of suffering, Dukha Samutaya, the cause of suffering,

117
00:12:16,760 --> 00:12:24,200
Dukkani Roda which is the cessation of suffering and Dukkani Roda but Gami Nipati but

118
00:12:24,200 --> 00:12:29,880
which is actually the path which leads the cessation of suffering which is actually

119
00:12:29,880 --> 00:12:36,040
the eight full noble path which he just discussed and each of these four noble truths

120
00:12:36,040 --> 00:12:45,000
is given in some, some short explanation is given for each of them, so the truth of suffering

121
00:12:45,000 --> 00:12:51,280
is that birth is suffering, Jatipitukha, Jalapitukha, Odeja suffering, Malanampitukha,

122
00:12:51,280 --> 00:12:59,960
Datta suffering, Soakapari Devadukha, Domenasupaya, Sapi Dukha, sorrow lamentation, pain, distress

123
00:12:59,960 --> 00:13:10,600
and despair or suffering, APIA, Hisampa Yoko Dukha, the association with things dislike, Dukha,

124
00:13:10,600 --> 00:13:19,160
Pia Yupa Yupa Yoko Dukha, this association with the separation from things which is

125
00:13:19,160 --> 00:13:27,240
like stress is suffering, that which is, not getting what one wants is suffering and

126
00:13:27,240 --> 00:13:34,000
then brief the five aggregates of clinging or strut are suffering.

127
00:13:34,000 --> 00:13:37,520
And this is the beginning, this last part is the beginning of meditation practice when

128
00:13:37,520 --> 00:13:42,200
we talk about the five aggregates and so he may have explained at this time what these

129
00:13:42,200 --> 00:13:48,480
five were and we don't have it recorded here, he may have just been understood by them

130
00:13:48,480 --> 00:13:53,200
that these five aggregates existed but it seems likely, more likely that the Lord would

131
00:13:53,200 --> 00:13:57,960
explain some of this out because the five aggregates of course were not as far as we

132
00:13:57,960 --> 00:14:01,240
understand they were not known before the time of the long Buddha.

133
00:14:01,240 --> 00:14:05,560
I took the Lord Buddha to explain these five things, this is one of the things we give

134
00:14:05,560 --> 00:14:14,800
the Lord Buddha great credit for, being able to separate the being out into five parts.

135
00:14:14,800 --> 00:14:21,560
And so then we have, this is the truth of suffering is actually pretty much everything

136
00:14:21,560 --> 00:14:26,360
that arises can be said to be the cause, we said to be suffering and what it means by

137
00:14:26,360 --> 00:14:32,160
suffering is something which causes us suffering when we hold on to it, when we cling

138
00:14:32,160 --> 00:14:39,440
to it and this is what it means, Upadhanakanda, Upadhana means the clinging, incandize

139
00:14:39,440 --> 00:14:40,440
the aggregates.

140
00:14:40,440 --> 00:14:47,680
So some people often accuse Buddhism of saying that life is suffering for instance and

141
00:14:47,680 --> 00:14:51,640
of course the Lord Buddha never once said that life is suffering, life doesn't have to

142
00:14:51,640 --> 00:14:56,480
be suffering but anything that we cling to that thing is only a cause of suffering for

143
00:14:56,480 --> 00:15:01,560
us, it's a painful thing, just like a fire is something that can be very painful if you

144
00:15:01,560 --> 00:15:07,120
touch it, if you grasp it, if you hold on to it, something that is very hot, something

145
00:15:07,120 --> 00:15:12,840
that is very sharp, if you hold on to it it can give you great pain and suffering.

146
00:15:12,840 --> 00:15:17,440
Of course if you don't hold on to it, if you don't cling to it then it can cause you

147
00:15:17,440 --> 00:15:21,040
suffering, it cannot cause you suffering.

148
00:15:21,040 --> 00:15:25,720
So this is the first noble truth, the second one is what is the cause of suffering and

149
00:15:25,720 --> 00:15:31,680
the cause of suffering is, and of course this clinging, actually that's been more clear,

150
00:15:31,680 --> 00:15:35,960
the cause of suffering in the Lord Buddha said is what is the cause of clinging and actually

151
00:15:35,960 --> 00:15:42,360
clinging itself has a cause and that cause is craving or wanting, desiring something.

152
00:15:42,360 --> 00:15:47,920
And so these things are, some people maybe think that these two things are the same

153
00:15:47,920 --> 00:15:54,320
and in terms of the Abhidhamma they're very similar but craving is a very small sort

154
00:15:54,320 --> 00:16:00,720
of wanting when you see something and you want it or you like it, it then leads to what

155
00:16:00,720 --> 00:16:04,480
we might say in English addiction.

156
00:16:04,480 --> 00:16:09,680
So the addiction is what we mean by Upadana, once we like something we may not be addicted

157
00:16:09,680 --> 00:16:14,680
to but once we get it again and again and again and we reinforce this liking that's

158
00:16:14,680 --> 00:16:20,560
what leads to a clinging and this state of not being able to let go and the needing of

159
00:16:20,560 --> 00:16:25,480
something to keep us happy when we don't get it of course we're very unhappy and this

160
00:16:25,480 --> 00:16:29,960
is why this is the cause of suffering.

161
00:16:29,960 --> 00:16:33,720
The third noble truth is the cessation of suffering and while this is what is the cessation

162
00:16:33,720 --> 00:17:01,280
of suffering, the cessation without any remainder of

163
00:17:01,280 --> 00:17:08,160
sin of craving, this is the cessation of suffering.

164
00:17:08,160 --> 00:17:13,760
The remainder less fading and cessation, renunciation, relinquishment, release and letting

165
00:17:13,760 --> 00:17:17,120
go of that very craving.

166
00:17:17,120 --> 00:17:23,120
And so this is the goal of insight meditation is to let go of the craving when we see

167
00:17:23,120 --> 00:17:31,240
impermanent, when we see suffering, when we see non-self in the things which we contemplate,

168
00:17:31,240 --> 00:17:37,440
this leads us to let go, it leads us to give up and to be able to be free from our attachment,

169
00:17:37,440 --> 00:17:39,840
free from suffering.

170
00:17:39,840 --> 00:17:43,080
And how do we do that and then of course the fourth noble truth we come back again to

171
00:17:43,080 --> 00:17:47,520
the middle way which is again the eightfold noble path.

172
00:17:47,520 --> 00:17:55,120
And Mahasi Sayada, he comments that probably the Lord Buddha gave him more detail in regards

173
00:17:55,120 --> 00:18:02,680
to the eightfold noble path just as he did in the Dhamma, in the Sati Bhattana Sutta,

174
00:18:02,680 --> 00:18:04,520
where we have two versions of it.

175
00:18:04,520 --> 00:18:08,960
Now the Dhamma Jakapawat in the Sutta we only have one version but it's also possible that

176
00:18:08,960 --> 00:18:14,000
the Lord Buddha actually gave it more detailed explanation and all we have is a sort of

177
00:18:14,000 --> 00:18:22,800
a streamlined version which is very easy for chanting.

178
00:18:22,800 --> 00:18:29,640
So after explaining the four noble truths then the Lord Buddha goes on to explain how he

179
00:18:29,640 --> 00:18:35,840
came to understood these noble truths and before he understood these noble truths, he

180
00:18:35,840 --> 00:18:39,320
never said he would never have said he was enlightened.

181
00:18:39,320 --> 00:18:47,040
Nehwa tawahana pikhavi, neither did I say that I was enlightened.

182
00:18:47,040 --> 00:18:52,000
When I had not, for as long as Yahweh Qui went to me pikhavi, for as long as I didn't

183
00:18:52,000 --> 00:18:59,080
understand these four noble truths, for so long did I, for up into that point I never

184
00:18:59,080 --> 00:19:04,480
said that I was Buddha, but once I understood these four noble truths from then on then

185
00:19:04,480 --> 00:19:11,320
I said that I was indeed a Buddha, fully enlightened.

186
00:19:11,320 --> 00:19:15,440
And then he goes on to say, what is the benefit of this Yahnan, Japanamaitasanan

187
00:19:15,440 --> 00:19:22,920
wa dapati, the knowledge and vision arose in me, a kupa me ri mutti, my release is unshakable.

188
00:19:22,920 --> 00:19:30,200
I am anti-machati, this is my last birth, nati da nipu napah wa ti, there is no further

189
00:19:30,200 --> 00:19:35,400
becoming, this is what he said.

190
00:19:35,400 --> 00:19:43,240
And the result of the teaching was that Anya kundanya came to understand what we

191
00:19:43,240 --> 00:19:54,480
call the eye of dhamma, and this is of course the most important phrase in the practice

192
00:19:54,480 --> 00:20:03,600
of Buddhism is that yankinti samutayya dhamma, sapan dangmi rota dhamma ti, whatever dhamma's,

193
00:20:03,600 --> 00:20:11,240
whatever is the subject or has as its nature arising, whatever is of the nature to

194
00:20:11,240 --> 00:20:20,600
arise, all of that, every such thing is of the nature to cease.

195
00:20:20,600 --> 00:20:25,320
And as Ajampamat said in Minnesota he was explaining this, he said, you know it seems

196
00:20:25,320 --> 00:20:33,840
very simple, very simple and basic teaching, everything that arises has to cease, but we

197
00:20:33,840 --> 00:20:39,240
don't really realize this, we don't really see this with our hearts, we can accept it

198
00:20:39,240 --> 00:20:46,440
intellectually, very easy, it's very easy to accept, but the truth is we don't really see

199
00:20:46,440 --> 00:20:49,960
this about things, when something goes up we hold on to it and then when it disappears

200
00:20:49,960 --> 00:20:54,880
we're upset, well clearly this means that we don't understand yet that everything has

201
00:20:54,880 --> 00:21:01,480
to cease, it's like we have this kid inside of us who is just learning and we already

202
00:21:01,480 --> 00:21:08,720
know in our mind all of these things, but our heart is like a child, it's something like

203
00:21:08,720 --> 00:21:12,560
this that we really don't understand what we think we understand.

204
00:21:12,560 --> 00:21:15,800
And so when we practice meditation we realize that we're actually clinging to things

205
00:21:15,800 --> 00:21:21,600
which are impermanent and the more we practice, the more we're able to see that these

206
00:21:21,600 --> 00:21:24,640
things, you know there's no reason to get upset about them because they're just going

207
00:21:24,640 --> 00:21:30,040
to cease, they're just going to fade away, but we can't just say this to ourselves

208
00:21:30,040 --> 00:21:34,360
and say yeah I know of course I understand that, as long as we're still suffering it's

209
00:21:34,360 --> 00:21:38,160
a sign that we still don't understand that, because we're still holding on to things not

210
00:21:38,160 --> 00:21:43,480
understanding that they have to change and they have to disappear and even though we

211
00:21:43,480 --> 00:21:48,640
may get what we want, that we can't have what we want all the time.

212
00:21:48,640 --> 00:21:52,600
And so this was considered the point where Kuntanya understood the dhamma, it's called

213
00:21:52,600 --> 00:21:57,480
the realization of Saudapana and as we understand it Nabi dhamma it means that he actually

214
00:21:57,480 --> 00:22:04,400
entered into the realization of Nabi dhamma or complete cessation where there was no

215
00:22:04,400 --> 00:22:10,400
arising, there was no thinking, there was none of the five aggregates, there was no, well

216
00:22:10,400 --> 00:22:15,600
the body would have still been there but there was no realization or perception of the

217
00:22:15,600 --> 00:22:22,600
body, there was no feeling, there was no memories arising, no thoughts arising and no

218
00:22:22,600 --> 00:22:29,680
consciousness perceiving anything, there was only, if you may say that there was the perception

219
00:22:29,680 --> 00:22:35,640
of Nabi dhamma but of course this is very much like a non-perception or a non-percipient

220
00:22:35,640 --> 00:22:41,880
state, but we have to be careful, we think of it actually we understand it to just a

221
00:22:41,880 --> 00:22:47,520
mean that the mind was free at that moment, just like the mind was not, the mind is something

222
00:22:47,520 --> 00:22:51,760
which grasps and which holds on to things at all times, here there was no grasping and

223
00:22:51,760 --> 00:23:00,120
no holding on, so the mind is like a bird flying free and not holding on to the tree and

224
00:23:00,120 --> 00:23:06,440
just for that moment that realization was enough to change on Kundanya's life and from

225
00:23:06,440 --> 00:23:15,400
that point on he was able to let go, able to be free at least to some extent and then

226
00:23:15,400 --> 00:23:20,400
of course for the next five days they went on to continue the practice with the Lord

227
00:23:20,400 --> 00:23:26,760
and Buddha had taught until they all, all of the five disciples became perfectly or became

228
00:23:26,760 --> 00:23:32,960
Arahans, became completely free from the Kilesa and the greed and anger and delusion,

229
00:23:32,960 --> 00:23:37,480
the defilements which exist inside themselves and so probably for the next day we will

230
00:23:37,480 --> 00:23:41,800
be going over some of the other teachings of the Lord Buddha and on the fifth day we

231
00:23:41,800 --> 00:23:48,640
will chant the Anatala kana suta, but for today that this is a summary of the Dhamma

232
00:23:48,640 --> 00:23:54,840
Jakapalat in the suta in brief and now we can start our meditation first with the mindful

233
00:23:54,840 --> 00:24:14,900
frustration and walking into the

