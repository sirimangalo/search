1
00:00:00,000 --> 00:00:04,000
Why is there ignorance in the first place?

2
00:00:04,000 --> 00:00:08,000
I understand that ignorance is the root of all suffering,

3
00:00:08,000 --> 00:00:11,000
and that the best way to overcome ignorance is to learn.

4
00:00:11,000 --> 00:00:13,000
So I'm wondering, what caused ignorance?

5
00:00:13,000 --> 00:00:17,000
Can we even know?

6
00:00:17,000 --> 00:00:21,000
But there's no original cause to some sorrow.

7
00:00:21,000 --> 00:00:29,000
I don't even think there's a sense that time is entirely linear.

8
00:00:29,000 --> 00:00:34,000
It seems like time somehow begins with the present moment,

9
00:00:34,000 --> 00:00:36,000
and so we're creating the past as we go,

10
00:00:36,000 --> 00:00:41,000
and the future is this also ethereal thing

11
00:00:41,000 --> 00:00:44,000
that we're somehow creating.

12
00:00:44,000 --> 00:00:47,000
Or even more interestingly,

13
00:00:47,000 --> 00:00:50,000
it seems like just as the past can affect the present,

14
00:00:50,000 --> 00:00:53,000
the future can also affect the present.

15
00:00:53,000 --> 00:00:55,000
So knowing quantum physics,

16
00:00:55,000 --> 00:01:00,000
they somehow have seen that it seems like the future is creating the past.

17
00:01:00,000 --> 00:01:07,000
Well, there is a sense that because it's possible to have a vision of the future

18
00:01:07,000 --> 00:01:10,000
to see what's going to happen before it happens.

19
00:01:10,000 --> 00:01:15,000
There's the sense that the future can actually affect the present

20
00:01:15,000 --> 00:01:20,000
by seeing the future, or the future can come back

21
00:01:20,000 --> 00:01:23,000
and you can actually see it before it happens.

22
00:01:23,000 --> 00:01:25,000
And the Buddha was able to do this,

23
00:01:25,000 --> 00:01:28,000
and it's possible through a certain type of limitation

24
00:01:28,000 --> 00:01:30,000
that you're able to do that.

25
00:01:30,000 --> 00:01:33,000
So anyway, not actually an answer to your question,

26
00:01:33,000 --> 00:01:37,000
but if you're looking for a first cause of ignorance,

27
00:01:37,000 --> 00:01:40,000
or a first cause of anything, there is no first cause.

28
00:01:40,000 --> 00:01:42,000
There is no sense of that.

29
00:01:42,000 --> 00:01:46,000
There's no sense that the universe is of that sort.

30
00:01:46,000 --> 00:01:53,000
Linear because it's actually illogical to think of,

31
00:01:53,000 --> 00:01:57,000
it's mind-boggling to think of it on infinite timeline,

32
00:01:57,000 --> 00:02:03,000
but it's even more difficult, more illogical to think of the first beginning,

33
00:02:03,000 --> 00:02:07,000
because then you have to think of something before that,

34
00:02:07,000 --> 00:02:09,000
or something outside of time.

35
00:02:09,000 --> 00:02:12,000
You have to think of,

36
00:02:12,000 --> 00:02:17,000
you have to try to understand what it could possibly mean to for time to begin.

37
00:02:17,000 --> 00:02:21,000
Now, I guess there's an understanding among material scientists

38
00:02:21,000 --> 00:02:25,000
or natural scientists, modern scientists,

39
00:02:25,000 --> 00:02:28,000
that time began with a big bang.

40
00:02:28,000 --> 00:02:30,000
So if the big bang was a singularity,

41
00:02:30,000 --> 00:02:32,000
then there was no time before that,

42
00:02:32,000 --> 00:02:37,000
but that's really just a time as a function of space,

43
00:02:37,000 --> 00:02:41,000
as a part of the four-dimensional reality

44
00:02:41,000 --> 00:02:45,000
that we find ourselves in.

45
00:02:45,000 --> 00:02:50,000
But again, that may be an hint of the fact

46
00:02:50,000 --> 00:02:54,000
that time is not exactly linear as we think it is.

47
00:02:54,000 --> 00:02:57,000
Time is not quite the way we think,

48
00:02:57,000 --> 00:03:00,000
so the idea that there might be something at the beginning

49
00:03:00,000 --> 00:03:05,000
is a bit of a misunderstanding of reality.

50
00:03:05,000 --> 00:03:09,000
We want to understand the proximate cause of ignorance,

51
00:03:09,000 --> 00:03:12,000
the Buddha said it's the five hindrances.

52
00:03:12,000 --> 00:03:14,000
And I was alerted to this.

53
00:03:14,000 --> 00:03:16,000
I've answered this question before and I said,

54
00:03:16,000 --> 00:03:19,000
well, there's no ignorance just is, right?

55
00:03:19,000 --> 00:03:24,000
Ignorance, you can't cause someone to not know something.

56
00:03:24,000 --> 00:03:28,000
If I'm thinking of a number between 1 and 10,

57
00:03:28,000 --> 00:03:32,000
and I ask you, what number am I thinking of?

58
00:03:32,000 --> 00:03:35,000
I didn't cause you to not know what number I'm thinking of.

59
00:03:35,000 --> 00:03:38,000
You never knew what number I was thinking of.

60
00:03:38,000 --> 00:03:41,000
It was something that was always there.

61
00:03:41,000 --> 00:03:44,000
The lack of knowledge was there in the beginning.

62
00:03:44,000 --> 00:03:47,000
So I'm not actually convinced by this person who called me out

63
00:03:47,000 --> 00:03:49,000
on that saying, you're wrong.

64
00:03:49,000 --> 00:03:51,000
There is a cause for ignorance.

65
00:03:51,000 --> 00:03:53,000
It's the five hindrances.

66
00:03:53,000 --> 00:03:56,000
But you have to understand there are twenty-four different

67
00:03:56,000 --> 00:03:59,000
kinds of causality in the Buddha's teaching.

68
00:03:59,000 --> 00:04:04,000
And so I was talking about something that created the ignorance

69
00:04:04,000 --> 00:04:09,000
that actually caused the ignorance to spring into existence.

70
00:04:09,000 --> 00:04:12,000
But the hindrances are a cause for ignorance,

71
00:04:12,000 --> 00:04:17,000
meaning because of the hindrances,

72
00:04:17,000 --> 00:04:19,000
we remain ignorant to the truth.

73
00:04:19,000 --> 00:04:23,000
Because of our lack of clarity, we remain ignorant.

74
00:04:23,000 --> 00:04:28,000
So it's like the fog is preventing us from seeing the road

75
00:04:28,000 --> 00:04:29,000
in front of us.

76
00:04:29,000 --> 00:04:32,000
But it didn't create that lack of knowledge, right?

77
00:04:32,000 --> 00:04:34,000
We never knew what the road in front of us looked like.

78
00:04:34,000 --> 00:04:37,000
But then we get to this point in the road.

79
00:04:37,000 --> 00:04:41,000
We still can't see it because there's the fog.

80
00:04:41,000 --> 00:04:44,000
So in that way, the hindrances are what is obscuring us

81
00:04:44,000 --> 00:04:46,000
from seeing the truth.

82
00:04:46,000 --> 00:04:50,000
But we didn't ever know.

83
00:04:50,000 --> 00:04:56,000
There wasn't a time or a point where we actually knew the truth.

84
00:04:56,000 --> 00:04:58,000
The four noble truths, for example.

85
00:04:58,000 --> 00:05:00,000
That isn't something that was ever there.

86
00:05:00,000 --> 00:05:03,000
The hindrances are just that which is in our way.

87
00:05:03,000 --> 00:05:07,000
And once we go to try and once we ask the question,

88
00:05:07,000 --> 00:05:08,000
what are the four noble truths?

89
00:05:08,000 --> 00:05:10,000
What is the truth of reality?

90
00:05:10,000 --> 00:05:12,000
We're not able to see it because of the five hindrances.

91
00:05:12,000 --> 00:05:13,000
That's all.

92
00:05:13,000 --> 00:05:18,000
An actual beginning to ignorance is something,

93
00:05:18,000 --> 00:05:28,000
you know, it's reasonably speaking a misleading question,

94
00:05:28,000 --> 00:05:33,000
misguided question because ignorance is something that was always there.

95
00:05:33,000 --> 00:05:36,000
Ignorance means a lack of something.

96
00:05:36,000 --> 00:05:38,000
It's like, why is there a lack of light?

97
00:05:38,000 --> 00:05:40,000
What caused there to be a lack of light?

98
00:05:40,000 --> 00:05:43,000
What caused there to be a lack of sound?

99
00:05:43,000 --> 00:05:45,000
Nothing caused it.

100
00:05:45,000 --> 00:05:51,000
A lack of something isn't really caused.

101
00:05:51,000 --> 00:05:53,000
Though you can say there's a reason for it,

102
00:05:53,000 --> 00:05:55,000
maybe we're in a soundproof room,

103
00:05:55,000 --> 00:05:57,000
and that's why there's no sound.

104
00:05:57,000 --> 00:06:00,000
But that's not really telling you why there's no sound.

105
00:06:00,000 --> 00:06:03,000
The no sound is the default.

106
00:06:03,000 --> 00:06:10,000
So I think it's easy to understand this and therefore,

107
00:06:10,000 --> 00:06:35,000
I think that's a proper answer to what is the cause of it.

