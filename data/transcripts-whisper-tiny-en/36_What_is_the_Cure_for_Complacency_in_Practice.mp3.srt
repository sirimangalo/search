1
00:00:00,000 --> 00:00:04,000
difficulties in meditation Q&A.

2
00:00:04,000 --> 00:00:08,000
What is the cure for complacency in practice?

3
00:00:08,000 --> 00:00:13,000
Question. What is the cure for complacency in practice?

4
00:00:13,000 --> 00:00:18,000
Answer. A sense of urgency is a cure for complacency.

5
00:00:18,000 --> 00:00:26,000
The easiest way to develop a sense of urgency is association with people who have a sense of urgency.

6
00:00:26,000 --> 00:00:34,000
When you are alone, it is very easy to become complacent, even resigned to failure, because the practice is difficult.

7
00:00:34,000 --> 00:00:45,000
Without encouragement and support from others in the path, it is easy to get weighed down by the central world and pulled away from the path by those who are disinclined to practice.

8
00:00:45,000 --> 00:00:51,000
associating with good people is the most important thing in the spiritual life.

9
00:00:51,000 --> 00:00:58,000
Listening to talks, reading books, or studying can also be a part of association.

10
00:00:58,000 --> 00:01:09,000
Another useful practice is meditative reflections on death and the free characteristics of impermanence, suffering, and oneself in a conventional context.

11
00:01:09,000 --> 00:01:14,000
Like the idea that all of the things we cling to will one day change and disappear.

12
00:01:14,000 --> 00:01:32,000
The realization that nothing is stable, and that at any moment we could be subject to great suffering from any number of causes, like sickness, accidents, natural and human-made disaster, robbery, crime and punishment, etc.

13
00:01:32,000 --> 00:01:54,000
Thinking about these sorts of things, meditating on the inevitability of suffering and death, meditating on the repulsiveness of the body, meditating on the nature of the body parts as being not as desirable, etc., allows to cultivate a sense of urgency and a sense of the importance of spiritual practice.

14
00:01:54,000 --> 00:02:04,000
Find a meditation center, do a meditation course, stay with a teacher, and do the best you can. Do not expect yourself to be the perfect meditator.

15
00:02:04,000 --> 00:02:13,000
Sometimes you may be discouraged or overwhelmed by worldly affairs, so just try your best to cultivate as much impetus to practice and do what you can.

16
00:02:13,000 --> 00:02:19,000
Ultimately, the path to enlightenment is a difficult and sometimes round about one.

17
00:02:19,000 --> 00:02:30,000
Our teacher Ajantong was asked the same question. He said, you can't succeed in the world if you don't work hard. How could it be any different in the practice?

18
00:02:30,000 --> 00:02:37,000
Sometimes we might think of the dharma as some kind of hobby or pastime, something that we do on the weekends or holidays.

19
00:02:37,000 --> 00:02:53,000
By not taking it seriously, just like any worldly pursuit, if you don't cultivate it, if you don't work hard to ditch, you cannot hope to succeed.

