1
00:00:00,000 --> 00:00:19,240
Okay, good evening, everyone, welcome to our evening, the dumbest session, tonight I'd like

2
00:00:19,240 --> 00:00:29,860
to talk a little bit about some sorrow, some sorrow, some sorrow as a word, some sorrow

3
00:00:29,860 --> 00:00:34,800
I think it's one of the more commonly known words in Buddhism, even if you're not a Buddhist,

4
00:00:34,800 --> 00:00:45,200
you know, a little bit about Buddhism, you probably heard this word before.

5
00:00:45,200 --> 00:00:56,300
We have in the in the Dhamabada, Jatunanga Riesetchan, I think it's in the Dhamabada actually, I'm not sure.

6
00:00:56,300 --> 00:01:04,500
Jatunanga Riesetchanan, Yatabhu, dangatasana, samsaritang, diyamatana, dasu, dasu, dasu,

7
00:01:04,500 --> 00:01:16,880
dasu, we have a jatisu, not seeing these four noble truths, we have wandered on, in this law we have

8
00:01:16,880 --> 00:01:30,960
wandered on for a long time, diyam, diyamantamam, all this long road and samsara,

9
00:01:30,960 --> 00:01:43,140
tasu, tasu, jatisu from life to life, verse to verse.

10
00:01:43,140 --> 00:01:53,620
So, samsara is the universe as we know it, it doesn't actually refer to the physical universe,

11
00:01:53,620 --> 00:02:01,400
there is no physical universe and Buddhism, it's not real.

12
00:02:01,400 --> 00:02:10,840
Physical space is something that is dependent on or derived from matter, which of course is still dependent on experience.

13
00:02:10,840 --> 00:02:19,640
So, in a sense, in the world, the universe and all the planets and so on, it's only there because we experience it.

14
00:02:19,640 --> 00:02:28,140
It only exists in the context of our experience.

15
00:02:28,140 --> 00:02:37,140
Quantum physics has something to say along those lines of it.

16
00:02:37,140 --> 00:02:51,140
You know what samsara is in Buddhism is this rebirth, the path of the individual, the path of being

17
00:02:51,140 --> 00:03:01,740
the birth and death really, samsara is really all about being born again.

18
00:03:01,740 --> 00:03:14,240
Samsara means when you die, you start on, or you continue on in whatever way.

19
00:03:14,240 --> 00:03:19,440
And so, we talk about the many realms of rebirth, there are the 31 realms in terabyte of Buddhism,

20
00:03:19,440 --> 00:03:22,640
but I think that's just a convention.

21
00:03:22,640 --> 00:03:26,240
My guess is, you can subdivide into many, many more realms.

22
00:03:26,240 --> 00:03:31,640
The hell realms, for example, there's many, many different hells that one can go to.

23
00:03:31,640 --> 00:03:41,140
But at the top, you have the Brahma realms, which if you practice somewhat to meditation, you go there.

24
00:03:41,140 --> 00:03:48,640
Below that, you have the angel realms, the day one realms, where if you do lots of super good deeds,

25
00:03:48,640 --> 00:03:53,940
you're a very kind and gentle and generous person, more on ethical.

26
00:03:53,940 --> 00:04:04,940
You go to these heavens, and we have the human realms, if you're human, if you're reasonably ethical and moral and good,

27
00:04:04,940 --> 00:04:07,940
you'll be born to the human.

28
00:04:07,940 --> 00:04:11,940
If you have the animal realms, or if you're full of delusion, you'll go there.

29
00:04:11,940 --> 00:04:16,940
We have the ghost realms, if you're full of greed, you'll go there.

30
00:04:16,940 --> 00:04:35,940
We have the hell realms, or if you're really, if you're full of anger, fear, spite, hatred and malice, all this, you'll go there.

31
00:04:35,940 --> 00:04:42,940
These are the conventional senses, this is what some sire means.

32
00:04:42,940 --> 00:04:54,940
We wander around aimlessly, really, about we have goals and aims from time to time, but they're short-sighted,

33
00:04:54,940 --> 00:05:01,940
and meaningless in the larger context of some sire, of our wandering from life to life.

34
00:05:01,940 --> 00:05:18,940
So sometimes we're angels, sometimes we're gods, often we're humans, and often we're animals, ghosts, even sometimes born now.

35
00:05:18,940 --> 00:05:33,940
It's somewhat of a foreign paradigm, I think, for the modern world. So we've become accustomed to thinking in terms of the physical world, this conceptual world, really.

36
00:05:33,940 --> 00:05:49,940
We conceive of space and time, and we have all sorts of formulas and theories about space-time.

37
00:05:49,940 --> 00:05:53,940
And in fact, both space and time are not quite what we think they are.

38
00:05:53,940 --> 00:06:18,940
We're not really what we think they are at all. Space is just a derivation of matter and time is just about the changes inherent in the present moment, which is the only time that really exists.

39
00:06:18,940 --> 00:06:28,940
So if you become familiar with this experiential paradigm of being reborn again, this way of looking at the world,

40
00:06:28,940 --> 00:06:34,940
it really puts everything into perspective in a way that many of us don't say.

41
00:06:34,940 --> 00:06:45,940
I think for a lot of people it goes well beyond their reasons for practicing meditation. You don't find many people nowadays saying, hey, I'm going to go meditate, so I can be free from all the many realms of rebirth,

42
00:06:45,940 --> 00:06:58,940
most of us don't even have a conception of the other realms, or being reborn even at all perhaps.

43
00:06:58,940 --> 00:07:07,940
Nonetheless, it being the nature of reality is understood by Buddhists and by the Buddha.

44
00:07:07,940 --> 00:07:14,940
It's useful for us to think of it as the larger picture in the conventional sense, it's useful because it helps.

45
00:07:14,940 --> 00:07:29,940
I really understand the depth of our practice, that yes, it's about stress reduction, yes, it's about overcoming our defilements, but ultimately there's a broader context that we've been travelling,

46
00:07:29,940 --> 00:07:43,940
we've been wandering on, add infinitum, add infinitum with you.

47
00:07:43,940 --> 00:07:55,940
With no end in sight, with no beginning in sight, we don't know how long we've been going, perhaps it's eternal.

48
00:07:55,940 --> 00:08:12,940
And in the end it becomes meaningless, so you realize that all of this is quite exciting really to think about all the many realms you can be born in.

49
00:08:12,940 --> 00:08:25,940
That's something new, but as you consider, as you learn and as you meditate, you start to see that it's all just wandering on.

50
00:08:25,940 --> 00:08:30,940
It's all meaningless, pointless, useless.

51
00:08:30,940 --> 00:08:39,940
But again, I think that goes far beyond what most people think about when they undertake the practice of meditation.

52
00:08:39,940 --> 00:08:55,940
Unfortunately, you don't have to think that being a minute, something that's profound on the level of a Buddha to think about these things and to really realize that this is enough that they've had enough with some sorrow.

53
00:08:55,940 --> 00:08:59,940
But I'm sorry, that's just the conceptual nature of it.

54
00:08:59,940 --> 00:09:04,940
In reality, some sorrow, if you want to go deeper, it's just the cycle.

55
00:09:04,940 --> 00:09:10,940
So we often hear about the cycle of birth, all day of sickness and death. That's what we're talking about.

56
00:09:10,940 --> 00:09:17,940
No matter where we go, there is always going to be this being starting from the new journey.

57
00:09:17,940 --> 00:09:30,940
Going through the motions of learning and adapting and clinging and settling down in some sort of measure of stability.

58
00:09:30,940 --> 00:09:41,940
Only to be uprooted again at the moment of death and be cast out and sent on our way to wandering again.

59
00:09:41,940 --> 00:09:56,940
And so if we understand, if we want to understand in a much easier way to understand it, there's this way in terms of one way.

60
00:09:56,940 --> 00:10:00,940
It's still conceptual, much more useful.

61
00:10:00,940 --> 00:10:06,940
It's more useful to think about what it means to be alive, no matter where we're born.

62
00:10:06,940 --> 00:10:08,940
What is involved?

63
00:10:08,940 --> 00:10:13,940
When we're looking, we're making the same mistakes again and again.

64
00:10:13,940 --> 00:10:16,940
This has been infinite that we're doing this forever.

65
00:10:16,940 --> 00:10:21,940
It's really quite shameful that we're still making so many mistakes.

66
00:10:21,940 --> 00:10:30,940
It's certainly a sign that this isn't the ideal.

67
00:10:30,940 --> 00:10:37,940
This can't be as good as it gets.

68
00:10:37,940 --> 00:10:43,940
But if we really want to understand some signs, of course we have to go beyond constant.

69
00:10:43,940 --> 00:10:53,940
And so the ultimate way, the third way we can understand some sorrows in terms of moment to moment wandering on of sorts.

70
00:10:53,940 --> 00:10:59,940
It's mindless, zombie-like state that we find ourselves in.

71
00:10:59,940 --> 00:11:09,940
We can go through the motions of indulging and clinging and judging and avoiding and running and chasing.

72
00:11:09,940 --> 00:11:12,940
We're going to the bad.

73
00:11:12,940 --> 00:11:14,940
Out of delusion and out of blindness.

74
00:11:14,940 --> 00:11:16,940
We're not seeing clearly.

75
00:11:16,940 --> 00:11:21,940
Because we don't see the far-normal truths.

76
00:11:21,940 --> 00:11:27,940
That we don't have to even think about being reborn or going to this realm or that realm.

77
00:11:27,940 --> 00:11:32,940
Ultimately it's all just experience.

78
00:11:32,940 --> 00:11:45,940
No matter where we go, we're not going to be free from the senses, from experience.

79
00:11:45,940 --> 00:11:52,940
This is really what we have to understand through the meditation.

80
00:11:52,940 --> 00:11:55,940
The reality is just made up of experiences.

81
00:11:55,940 --> 00:11:58,940
Experiences continue on and on and we wander on.

82
00:11:58,940 --> 00:12:02,940
We wander through experiences without any real rhyme or reason.

83
00:12:02,940 --> 00:12:05,940
So we start to see a meditation, right?

84
00:12:05,940 --> 00:12:08,940
100 around like a lost person.

85
00:12:08,940 --> 00:12:21,940
And you start to cultivate this sense of direction, a compass, a way to go, a path.

86
00:12:21,940 --> 00:12:25,940
You start to see the right way, the right path.

87
00:12:25,940 --> 00:12:28,940
Not as going here or going there, but as stopping.

88
00:12:28,940 --> 00:12:31,940
It's not wandering anymore.

89
00:12:31,940 --> 00:12:34,940
It's heading in the right direction, if you will.

90
00:12:34,940 --> 00:12:42,940
The sense of actually not going anywhere.

91
00:12:42,940 --> 00:12:46,940
So we focus on the five aggregates, the four-septipatana,

92
00:12:46,940 --> 00:12:49,940
the six senses however you want to describe it.

93
00:12:49,940 --> 00:12:51,940
But we focus on experience.

94
00:12:51,940 --> 00:12:55,940
And so our act of being objective of reminding ourselves

95
00:12:55,940 --> 00:12:58,940
and of learning to see clearly.

96
00:12:58,940 --> 00:13:03,940
This is about really understanding, well, seeing through the concept

97
00:13:03,940 --> 00:13:08,940
of some sari entirely.

98
00:13:08,940 --> 00:13:13,940
We mean by some, what we call some sari is just our own

99
00:13:13,940 --> 00:13:18,940
delusions and our own conceptions of our experiences.

100
00:13:18,940 --> 00:13:24,940
Which turned out to be impermanent and unsatisfying and controllable.

101
00:13:24,940 --> 00:13:28,940
Not belonging to us.

102
00:13:28,940 --> 00:13:33,940
Not being amenable to our wishes, our expectations.

103
00:13:33,940 --> 00:13:40,940
And we see that we're lost at our ambitions and our drives

104
00:13:40,940 --> 00:13:46,940
and our wants and our wishes are really a cause for more stress and suffering.

105
00:13:46,940 --> 00:13:49,940
And so we start to settle down.

106
00:13:49,940 --> 00:13:53,940
I don't have to worry about leaving some sari.

107
00:13:53,940 --> 00:13:58,940
Some sari just leaves you as you let go of it.

108
00:13:58,940 --> 00:14:00,940
Some sari leaves you.

109
00:14:00,940 --> 00:14:02,940
That's the truth.

110
00:14:02,940 --> 00:14:08,940
Become less and less inclined to pull it in to take it in

111
00:14:08,940 --> 00:14:11,940
to build it up.

112
00:14:11,940 --> 00:14:16,940
So less and less inclined to create some sari around you

113
00:14:16,940 --> 00:14:19,940
until eventually you stop.

114
00:14:19,940 --> 00:14:23,940
You cease.

115
00:14:23,940 --> 00:14:26,940
And liberate yourself.

116
00:14:26,940 --> 00:14:27,940
There you go.

117
00:14:27,940 --> 00:14:29,940
There's the demo for tonight.

118
00:14:29,940 --> 00:14:31,940
A little bit about some sari.

119
00:14:31,940 --> 00:14:33,940
Thank you all for tuning in.

120
00:14:33,940 --> 00:14:42,940
Have a good night.

