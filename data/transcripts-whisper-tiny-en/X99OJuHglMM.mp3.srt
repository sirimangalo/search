1
00:00:00,000 --> 00:00:28,800
Okay, good morning.

2
00:00:28,800 --> 00:00:35,800
What do you think about the world?

3
00:00:35,800 --> 00:00:43,800
Engaging with the world, leaving the world,

4
00:00:43,800 --> 00:00:50,800
clinging to the world, letting go of the world?

5
00:00:50,800 --> 00:00:58,800
What do you want to talk about knowing the world, understanding the world?

6
00:00:58,800 --> 00:01:18,800
Instead of Buddha as someone who knows all worlds, look how we do.

7
00:01:18,800 --> 00:01:23,800
You said, eta passatimanglokang.

8
00:01:23,800 --> 00:01:27,800
Come look at this world.

9
00:01:27,800 --> 00:01:36,800
Deck out like a king's chariot.

10
00:01:36,800 --> 00:01:43,800
The fools fought it.

11
00:01:43,800 --> 00:01:48,800
Get lost in him.

12
00:01:48,800 --> 00:01:57,800
Let this uncle reach out at them, but the wise, those who know reach out at them, those who know.

13
00:01:57,800 --> 00:02:09,800
Find nothing to cling to in no connection.

14
00:02:09,800 --> 00:02:18,800
Breathe himself from it.

15
00:02:18,800 --> 00:02:22,800
It's not about escaping the world so much,

16
00:02:22,800 --> 00:02:28,800
as it is about understanding and being free from its control,

17
00:02:28,800 --> 00:02:36,800
free from its power over us.

18
00:02:36,800 --> 00:02:41,800
Knowledge is power.

19
00:02:41,800 --> 00:02:54,800
Truth is power.

20
00:02:54,800 --> 00:03:01,800
The Buddha talked about three worlds.

21
00:03:01,800 --> 00:03:21,800
The world of space, Sataloka, the world of beings,

22
00:03:21,800 --> 00:03:44,800
and Sankar andoka, the world of formations, constructs.

23
00:03:44,800 --> 00:03:52,800
You know, the all three of these, to some extent, all three of them are worth understanding.

24
00:03:52,800 --> 00:04:07,800
The world of space, we've spent a lot of time as human being studying the world of space.

25
00:04:07,800 --> 00:04:16,800
All of the material sciences are about studying the world of space.

26
00:04:16,800 --> 00:04:22,800
Chemistry, biology, physics.

27
00:04:22,800 --> 00:04:34,800
From the smallest particle to the universe as a whole, we study it.

28
00:04:34,800 --> 00:04:43,800
That's not the sort of understanding the Buddha was referring to.

29
00:04:43,800 --> 00:04:55,800
It's understanding on a much more personal level, I think.

30
00:04:55,800 --> 00:05:04,800
About how we cling to and how we suffer from the world of space.

31
00:05:04,800 --> 00:05:07,800
We suffer when the world isn't the way we want it to be.

32
00:05:07,800 --> 00:05:13,800
It's too hot, it's too cold.

33
00:05:13,800 --> 00:05:17,800
We suffer when we miss places.

34
00:05:17,800 --> 00:05:27,800
We yearn for travel or we yearn to go home.

35
00:05:27,800 --> 00:05:37,800
We've never traveled the world, you might not experience this sense of being outside of your comfort zone.

36
00:05:37,800 --> 00:05:52,800
Outside of your territory, we grew up in a place when you move, you feel disconcerted.

37
00:05:52,800 --> 00:05:56,800
It's uncomfortable.

38
00:05:56,800 --> 00:06:03,800
The sort of discomfort is an important object of practice in Buddhism.

39
00:06:03,800 --> 00:06:07,800
It's not a sign that something's wrong.

40
00:06:07,800 --> 00:06:13,800
Well, it's a sign that something's wrong with us, I suppose.

41
00:06:13,800 --> 00:06:27,800
A sign that we're dependent, reliant upon our environment to be happy, to be at peace.

42
00:06:27,800 --> 00:06:39,800
We're inflexible, being inflexible is a very important concern in Buddhism.

43
00:06:39,800 --> 00:06:56,800
It belies a lack of understanding of impermanence of uncertainty, a lack of familiarity with the nature of life, the nature of impermanence.

44
00:06:56,800 --> 00:07:25,800
To some real extent, travel and diversity in our environment can be very useful to increasing our familiarity.

45
00:07:25,800 --> 00:07:43,800
And understanding with the nature of not only the world, but our engagement with it, how it affects us.

46
00:07:43,800 --> 00:07:54,800
Sataloka, Sataloka is of course, a little greater concern than it's put us. More important to understand it.

47
00:07:54,800 --> 00:08:11,800
Satam means of being sentient, being the world, the world of beings, human beings, animal beings.

48
00:08:11,800 --> 00:08:24,800
Beings that have mind, the understanding of personalities and character types.

49
00:08:24,800 --> 00:08:40,800
And goes with that saying, there's so much to learn from this, from our interactions with others, our relationships and our reactions,

50
00:08:40,800 --> 00:08:48,800
our prejudices and biases towards others.

51
00:08:48,800 --> 00:09:00,800
And their behavior towards us, that we can't predict or control.

52
00:09:00,800 --> 00:09:05,800
Again, travel can be very helpful in this way.

53
00:09:05,800 --> 00:09:13,800
It's quite eye-opening to go to another country and realize that people don't do things the way we do.

54
00:09:13,800 --> 00:09:27,800
What we took for granted about beings, human beings, turns out to be very much specific to our culture, society.

55
00:09:27,800 --> 00:09:35,800
You see how people in other places cling just as strongly to their way of doing things as we cling to ours.

56
00:09:35,800 --> 00:09:58,800
We cling to our Agasaloka. We call them countries or states, and we're proud of them.

57
00:09:58,800 --> 00:10:05,800
We come to see the world more clearly, if you travel, it can be very useful.

58
00:10:05,800 --> 00:10:20,800
These will to understand the world of space, the world of beings.

59
00:10:20,800 --> 00:10:46,800
Ultimately, the world of beings, it comes down to understanding psychology and understanding the way people's minds work, the way our mind works.

60
00:10:46,800 --> 00:11:14,800
Understanding people means it's understanding the mind, understanding prejudice and attachment.

61
00:11:14,800 --> 00:11:23,800
It means having this or that knowledge or understanding it means being understanding.

62
00:11:23,800 --> 00:11:34,800
A big part of Buddhism isn't about what you learn, but it's about being learned about being familiar.

63
00:11:34,800 --> 00:11:45,800
Having an outlook that is wise, that is aware.

64
00:11:45,800 --> 00:11:50,800
It brings it in terms of mindfulness.

65
00:11:50,800 --> 00:12:16,800
Mindfulness isn't about giving you this or that insight per se, it's about being insightful when you look at someone who's filled with meanness and anger, prejudice and so on.

66
00:12:16,800 --> 00:12:29,800
To see clearly, you don't have to like them, or even love them, talk about loving your enemies and so on.

67
00:12:29,800 --> 00:12:37,800
It's not really about that. You have to understand them. That's enough.

68
00:12:37,800 --> 00:12:46,800
His understanding is enough to be friendly, understanding the way their mind works, understanding the way your mind works.

69
00:12:46,800 --> 00:12:52,800
It's enough to teach you to be friendly even to your enemies. You don't have to love them.

70
00:12:52,800 --> 00:13:02,800
But you understand that the best way, of course, whether someone's an enemy or a friend, the best way, is to be friendly.

71
00:13:02,800 --> 00:13:11,800
The best way is to be kind, is to be compassionate, wanting to hurt others, wanting to be cruel to others.

72
00:13:11,800 --> 00:13:22,800
There's no one any good. It doesn't change. It doesn't improve the situation.

73
00:13:22,800 --> 00:13:38,800
Understanding allows you to speak the truth, allows you to stand for the truth, allows you to promote the truth.

74
00:13:38,800 --> 00:13:52,800
Let's see, to see the truth about yourself, about others, about your relationships.

75
00:13:52,800 --> 00:14:03,800
It helps you see through prejudice and different difference.

76
00:14:03,800 --> 00:14:13,800
So that rather than creating categories of people, stereotypes and so on, we come to see the behind stereotypes.

77
00:14:13,800 --> 00:14:34,800
We see how habits form, how people are really just the sum of their parts, the sum of their personality, some of their habits.

78
00:14:34,800 --> 00:14:43,800
But the third, the third loca, the third world, it's the most important.

79
00:14:43,800 --> 00:14:54,800
You can't talk as a Buddhist about the other two worlds without hinting at the importance of the third and its sunkara loca.

80
00:14:54,800 --> 00:15:10,800
So you see, these are three different ways of looking at the world. As a perspective on reality and being a material place, set the loca is conceptual,

81
00:15:10,800 --> 00:15:18,800
but it's a different kind of concept, the concept of beings that make up the world.

82
00:15:18,800 --> 00:15:35,800
Sunkara loca is not conceptual, it's what's behind all of our perceptions of the material world and the sentient being world.

83
00:15:35,800 --> 00:15:44,800
This is the world of experience, seeing, hearing, smelling, tasting, feeling, thinking.

84
00:15:44,800 --> 00:15:59,800
Ultimately, it all comes down to that. This is the building blocks of reality, the sunkara, the building blocks of the world.

85
00:15:59,800 --> 00:16:06,800
Just like a house is made up of pieces and when it comes together, you say, oh, there's a house.

86
00:16:06,800 --> 00:16:13,800
Likewise, with people, places, things.

87
00:16:13,800 --> 00:16:21,800
They're all made up of experiences.

88
00:16:21,800 --> 00:16:35,800
Because the people are in our minds. You think of a person that seems ridiculous to think that person doesn't exist, but

89
00:16:35,800 --> 00:16:44,800
the only place it exists for us is in our mind that the reality for them is changing all the time.

90
00:16:44,800 --> 00:16:50,800
Not just their mental, also their physical, but we get an idea in our mind of a person.

91
00:16:50,800 --> 00:16:58,800
What does it mean to person? One day to the next, they're different, let alone year after year.

92
00:16:58,800 --> 00:17:12,800
And then when they die, where did the person go? The person really existed, where did it go?

93
00:17:12,800 --> 00:17:21,800
Ultimately, the person is only their experiences and our experiences of them.

94
00:17:21,800 --> 00:17:34,800
At every moment of seeing, hearing, smelling, tasting, feeling, thinking, their arises.

95
00:17:34,800 --> 00:17:40,800
Their arises, the body and the mind, just for a moment.

96
00:17:40,800 --> 00:17:45,800
The body doesn't exist over time, not except as a concept.

97
00:17:45,800 --> 00:18:04,800
This is another perspective, a way of looking at the world from a point of view of our experiences.

98
00:18:04,800 --> 00:18:10,800
It's not really hard to understand, it's something that should be very familiar to us.

99
00:18:10,800 --> 00:18:16,800
There's nobody who's not familiar with seeing, hearing, smelling, tasting, feeling, thinking,

100
00:18:16,800 --> 00:18:27,800
some of them at least. Even blind people, they may not see, but they hear and smell.

101
00:18:27,800 --> 00:18:30,800
It doesn't matter who you are, you have experiences.

102
00:18:30,800 --> 00:18:39,800
The one thing we're all familiar with and on a very profound level,

103
00:18:39,800 --> 00:18:46,800
it's the only thing we can be sure of.

104
00:18:46,800 --> 00:18:55,800
This is quite profound because we can be sure of it.

105
00:18:55,800 --> 00:19:03,800
You might miss that, not realize that, but it might not realize why that's important.

106
00:19:03,800 --> 00:19:13,800
But the only thing we can know about this world is our experience of it.

107
00:19:13,800 --> 00:19:19,800
We can know seeing, knowing the sense that we know that that's what it is.

108
00:19:19,800 --> 00:19:23,800
We know that it's not anything else. We know hearing.

109
00:19:23,800 --> 00:19:26,800
We know that it's hearing.

110
00:19:26,800 --> 00:19:31,800
We might not know what we're hearing or that we're actually hearing what we think we're hearing.

111
00:19:31,800 --> 00:19:35,800
We're seeing, look, there are illusions.

112
00:19:35,800 --> 00:19:37,800
How do you know you're really seeing it?

113
00:19:37,800 --> 00:19:40,800
Well, we don't know what we're seeing, but we know that we're seeing.

114
00:19:40,800 --> 00:19:48,800
We know that there is seeing, smelling, tasting, feeling, thinking, that's it.

115
00:19:48,800 --> 00:19:52,800
That's all we can know for sure.

116
00:19:52,800 --> 00:20:02,800
And the importance of it, the profoundity of it,

117
00:20:02,800 --> 00:20:10,800
is that you can use it to base certainty on.

118
00:20:10,800 --> 00:20:15,800
You can build on that true understanding.

119
00:20:15,800 --> 00:20:20,800
That's what we talk about knowing the world, understanding the world.

120
00:20:20,800 --> 00:20:28,800
True understanding, the only true understanding there is, has to come from what you can actually know.

121
00:20:28,800 --> 00:20:44,800
And all Buddhist insight, all Buddhist understanding is based, therefore, on Sankara Loka, understanding Sankara.

122
00:20:44,800 --> 00:20:52,800
We talk about impermanence, suffering, and non-self. We're not talking about beings or things, places, people, whatever.

123
00:20:52,800 --> 00:20:54,800
We're talking about experiences.

124
00:20:54,800 --> 00:21:03,800
We're talking about the understanding that comes along with this perspective of seeing the world in terms of experience.

125
00:21:03,800 --> 00:21:09,800
Because when you do that, when you do see the world in terms of your experience,

126
00:21:09,800 --> 00:21:22,800
you lose quite quickly, and constantly lose, decrease your perception of things as stable, satisfying,

127
00:21:22,800 --> 00:21:27,800
me, mind, controllable.

128
00:21:27,800 --> 00:21:43,800
And there's nothing about experience that is stable, satisfying, or controllable.

129
00:21:43,800 --> 00:21:47,800
So this is where we base our practice. This is very important.

130
00:21:47,800 --> 00:21:55,800
Maybe not in the beginning, but as you practice, it's important to slowly understand what this means.

131
00:21:55,800 --> 00:22:01,800
So slowly get the meaning of this. In the beginning, you might not understand this.

132
00:22:01,800 --> 00:22:11,800
But as you look, this is what you'll start to see, and it's important that you understand where you're headed through the practice of mindfulness.

133
00:22:11,800 --> 00:22:19,800
That the goal really is to gain proficiency in this way of looking at the world.

134
00:22:19,800 --> 00:22:25,800
The ability to see the world for what it really is.

135
00:22:25,800 --> 00:22:31,800
There's experiences that arise and see.

136
00:22:31,800 --> 00:22:35,800
Some car, look at. So these are the three worlds.

137
00:22:35,800 --> 00:22:45,800
Again, I don't think Buddhism is a Buddhist practice anyway, it's about escaping the world, or engaging with the world.

138
00:22:45,800 --> 00:22:49,800
There's a debate in Buddhism should we engage, should we escape?

139
00:22:49,800 --> 00:22:56,800
Buddhism is the practice of understanding the world.

140
00:22:56,800 --> 00:23:03,800
It doesn't quite say what we should or shouldn't do, except that we should understand.

141
00:23:03,800 --> 00:23:12,800
Because understanding will affect everything we do and say and even think.

142
00:23:12,800 --> 00:23:16,800
Our understanding will set us free.

143
00:23:16,800 --> 00:23:26,800
Everything we then do and say and think will be free from the causes of suffering.

144
00:23:26,800 --> 00:23:32,800
Free from evil, free from harm towards ourselves or others.

145
00:23:32,800 --> 00:23:36,800
It will be a testament to the truth.

146
00:23:36,800 --> 00:23:43,800
It will be based in wisdom and knowledge and understanding.

147
00:23:43,800 --> 00:23:51,800
So, an important thing to consider is the world.

148
00:23:51,800 --> 00:24:15,800
That's the demo for this morning. Thank you for listening.

