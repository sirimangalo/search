1
00:00:00,000 --> 00:00:12,400
Good evening everyone, we're broadcasting live.

2
00:00:12,400 --> 00:00:13,400
How's the sound?

3
00:00:13,400 --> 00:00:17,400
Is that too loud?

4
00:00:17,400 --> 00:00:25,280
Someone said it was too quiet last night, so I'm trying something louder.

5
00:00:25,280 --> 00:00:38,680
But it started, I can't tell.

6
00:00:38,680 --> 00:00:50,880
Anyway, so tonight's quote is another good one, it's my treatment.

7
00:00:50,880 --> 00:01:14,000
Yes, are we coming into this story?

8
00:01:14,000 --> 00:01:25,600
with a sick person, who knows they are sick, right?

9
00:01:25,600 --> 00:01:29,120
We're in German, just a second.

10
00:01:32,640 --> 00:01:38,560
I know we're in German is when there is a tikichika,

11
00:01:38,560 --> 00:01:43,920
which is a physician, someone who treats

12
00:01:43,920 --> 00:01:48,480
sickness from the birth tikichichika.

13
00:01:48,480 --> 00:01:55,560
Just as with a sick person, when a physician or a doctor is there,

14
00:01:55,560 --> 00:02:06,760
a tikichapet, it doesn't have the doctor cure him.

15
00:02:06,760 --> 00:02:13,760
Nado also, so Tiki Tiki, and it's not the fault of that doctor.

16
00:02:17,760 --> 00:02:24,760
He hung even so, Kile, Sambjadi, Tukito, Pari, Lito.

17
00:02:24,760 --> 00:02:35,760
For one, for those who are sick with the defilements, with greed, with anger, with delusion,

18
00:02:36,760 --> 00:02:40,760
who suffer, who are feverish.

19
00:02:41,760 --> 00:02:46,760
Naga Oesetitang Ajaniyam, who don't seek out a teacher.

20
00:02:47,760 --> 00:02:52,760
Nado also, Nado also, so Vinai again.

21
00:02:52,760 --> 00:02:57,760
It's not the fault of the one who would lead them out to Vinai again.

22
00:02:58,760 --> 00:03:06,760
The one who would free them from defilements.

23
00:03:11,760 --> 00:03:14,760
This is from the Jataka. I'm not going to go through the Jataka.

24
00:03:14,760 --> 00:03:16,760
In fact, it's part of a larger quote.

25
00:03:16,760 --> 00:03:27,760
It's important here is the, this is a good quote.

26
00:03:27,760 --> 00:03:35,760
We sometimes put too much of an emphasis on the teacher.

27
00:03:35,760 --> 00:03:40,760
Someone came recently to see me and said he was looking for a good teacher.

28
00:03:40,760 --> 00:03:45,760
And I said to him, well, that's not the most important thing actually.

29
00:03:45,760 --> 00:03:47,760
Most important is a good teaching.

30
00:03:48,760 --> 00:03:51,760
Can you say it's important to have a good teacher? Yes, it's important.

31
00:03:52,760 --> 00:03:55,760
But it's much more important to have a good teaching.

32
00:03:55,760 --> 00:03:57,760
The real focus should be on the teaching.

33
00:03:57,760 --> 00:04:01,760
An ultimate, because if you have a good teacher teaching you the wrong thing,

34
00:04:01,760 --> 00:04:03,760
it's even more dangerous.

35
00:04:03,760 --> 00:04:09,760
Because they're so good at teaching something wrong that you can go the wrong way even quicker.

36
00:04:12,760 --> 00:04:18,760
And in the end, especially in this teaching, a teacher can only go so far.

37
00:04:18,760 --> 00:04:23,760
I get questions. People ask questions, send me questions.

38
00:04:23,760 --> 00:04:26,760
Some questions you just can't answer.

39
00:04:27,760 --> 00:04:30,760
Many questions. Many questions people are asking.

40
00:04:30,760 --> 00:04:34,760
You know, questions how to solve their problems, questions.

41
00:04:34,760 --> 00:04:37,760
What is someone who is saying, what is it like to perceive Northern Obama,

42
00:04:37,760 --> 00:04:43,760
or what is it like? What is it like to pursue the perception of a soda fountain?

43
00:04:43,760 --> 00:04:45,760
Can you answer that? You could.

44
00:04:45,760 --> 00:04:50,760
You can answer to some extent, but I get, if look at these questions and things, no,

45
00:04:50,760 --> 00:04:53,760
this isn't to come from a teacher.

46
00:04:53,760 --> 00:04:57,760
These questions have to come, the answers have to come from practice.

47
00:04:57,760 --> 00:05:04,760
The fact that we're asking questions, that sometimes you sign that we're not really practicing.

48
00:05:04,760 --> 00:05:15,760
What this is leading to, is that the teacher only shows the way a kata wrote at Hagata,

49
00:05:15,760 --> 00:05:18,760
even at the target at the Buddha.

50
00:05:18,760 --> 00:05:22,760
It's just one who shows the way, who points the way.

51
00:05:22,760 --> 00:05:28,760
So, hey, he keeps telling the subject to you.

52
00:05:28,760 --> 00:05:33,760
The work must be done by you.

53
00:05:37,760 --> 00:05:41,760
So, we have to see this in meditation.

54
00:05:41,760 --> 00:05:43,760
It's not going to be fun. It's not going to be easy.

55
00:05:43,760 --> 00:05:49,760
The teacher doesn't have any answers, or tricks, or means the best the teacher can do is encourage you.

56
00:05:49,760 --> 00:05:55,760
And that's good. It's good to get encouragement, but in the end, even that is a crunch.

57
00:05:55,760 --> 00:05:58,760
Even that is something you have to grow.

58
00:05:58,760 --> 00:06:02,760
You're completely alone in this practice. It'll make you cry.

59
00:06:02,760 --> 00:06:08,760
You know, it's good if it's math. It's bringing you to tears with a frustration,

60
00:06:08,760 --> 00:06:14,760
and just being overwhelmed by the enormity of the problem that you have in your mind.

61
00:06:14,760 --> 00:06:19,760
Let's sweat in tears.

62
00:06:19,760 --> 00:06:23,760
It's not even a difficult thing while you're doing is walking and sitting,

63
00:06:23,760 --> 00:06:27,760
right? It's not like that should be difficult.

64
00:06:27,760 --> 00:06:31,760
But that's the point. It's all about you. It's about your sickness.

65
00:06:31,760 --> 00:06:35,760
If you're sick, walking and sitting is painful.

66
00:06:35,760 --> 00:06:38,760
When you're sick with defamments equally,

67
00:06:38,760 --> 00:06:44,760
you're sick with greed, with anger, with delusion, just sitting still is problematic.

68
00:06:44,760 --> 00:06:49,760
It's difficult. It's unpleasant.

69
00:06:59,760 --> 00:07:04,760
So the doctor is there, the doctor is telling you what to do.

70
00:07:04,760 --> 00:07:11,760
If you don't do it, if you don't practice, if you don't cure your sickness,

71
00:07:11,760 --> 00:07:20,760
you're not doing so so to teach a game on the fault of that doctor.

72
00:07:20,760 --> 00:07:25,760
That's not actually what this quote says, if you don't seek out the teacher.

73
00:07:25,760 --> 00:07:29,760
So it is actually putting emphasis on the teacher.

74
00:07:29,760 --> 00:07:34,760
I kind of misinterpreted it.

75
00:07:34,760 --> 00:07:39,760
So there's even a bigger fault if you didn't seek out a teacher.

76
00:07:39,760 --> 00:07:41,760
Absolutely.

77
00:07:41,760 --> 00:07:47,760
If the teacher is present and if you don't listen to them, that's more important.

78
00:07:47,760 --> 00:07:50,760
Don't follow their teaching.

79
00:07:50,760 --> 00:07:55,760
But, right, so actually what this quote says is for those people who don't

80
00:07:55,760 --> 00:07:59,760
ever think to come and meditate.

81
00:07:59,760 --> 00:08:03,760
People in the world who are suffering from stress and anxiety,

82
00:08:03,760 --> 00:08:10,760
and where do they turn for a solution, they turn to pills, they turn to drugs, alcohol,

83
00:08:10,760 --> 00:08:15,760
they turn to entertainment, they turn to diversion, they turn to work,

84
00:08:15,760 --> 00:08:21,760
or something, anything to avoid having to deal with the problem.

85
00:08:21,760 --> 00:08:25,760
They hear about meditation, they hear about the Buddha,

86
00:08:25,760 --> 00:08:29,760
they hear about Buddhist teachers and so on.

87
00:08:29,760 --> 00:08:33,760
They don't understand the use.

88
00:08:33,760 --> 00:08:36,760
I can't.

89
00:08:36,760 --> 00:08:42,760
Sometimes don't even know that they're sick.

90
00:08:42,760 --> 00:08:46,760
I think there's more to this than that.

91
00:08:46,760 --> 00:08:51,760
I mean, in the context of a monastery, it makes good sense.

92
00:08:51,760 --> 00:08:54,760
I've got monks in the monastery who never go to see the teacher.

93
00:08:54,760 --> 00:09:02,760
Never go to ask for advice, who never go to undertake a meditation course.

94
00:09:02,760 --> 00:09:12,760
I think the deeper meaning or the deeper point has to be made is not enough to go and see the teacher.

95
00:09:12,760 --> 00:09:19,760
It's not enough to listen to the Buddha's teaching.

96
00:09:19,760 --> 00:09:27,760
If you don't put it into practice, the corollary there is that it's going to be tough.

97
00:09:27,760 --> 00:09:29,760
It's not something someone else can do for you.

98
00:09:29,760 --> 00:09:31,760
The teacher can't help you much.

99
00:09:31,760 --> 00:09:33,760
It's not much that can help you.

100
00:09:33,760 --> 00:09:39,760
It's not much that you can use as a trick or not to make it easier.

101
00:09:39,760 --> 00:09:43,760
In fact, just trying to make it easier is a problem.

102
00:09:43,760 --> 00:09:52,760
Trying to make it more pleasant, more stable, more agreeable.

103
00:09:52,760 --> 00:09:54,760
It just makes it harder in the end.

104
00:09:54,760 --> 00:09:57,760
It makes it less effective.

105
00:09:57,760 --> 00:10:01,760
We're looking for something called Anulomika kanti.

106
00:10:01,760 --> 00:10:10,760
It means patience.

107
00:10:10,760 --> 00:10:14,760
Anulomika means loma is the grain.

108
00:10:14,760 --> 00:10:17,760
Anulomika means with the grain.

109
00:10:17,760 --> 00:10:27,760
So, patience that accords with reality, but also with the path.

110
00:10:27,760 --> 00:10:33,760
It means it's in line with the truth.

111
00:10:33,760 --> 00:10:39,760
I guess we'll do the best way to put it.

112
00:10:39,760 --> 00:10:43,760
Meaning when you experience something, you have patience.

113
00:10:43,760 --> 00:10:51,760
Your patience is what brings you in line with the truth.

114
00:10:51,760 --> 00:10:56,760
Because without that patience, you have judgment rather than seeing things as they are.

115
00:10:56,760 --> 00:11:01,760
You think only of what they should be, what you'd like them to be.

116
00:11:01,760 --> 00:11:11,760
The change, you wish they wish they didn't enact, how you'd rather they were.

117
00:11:11,760 --> 00:11:14,760
So, what you want, what you don't want.

118
00:11:14,760 --> 00:11:19,760
Your urges, your desires, your needs, that kind of thing.

119
00:11:19,760 --> 00:11:22,760
Rather than just seeing things as they are.

120
00:11:22,760 --> 00:11:27,760
So, we need patience.

121
00:11:27,760 --> 00:11:29,760
That's the biggest fun.

122
00:11:29,760 --> 00:11:31,760
And it comes with practice.

123
00:11:31,760 --> 00:11:34,760
It comes with taking the regimen.

124
00:11:34,760 --> 00:11:39,760
That is, given by the teacher.

125
00:11:39,760 --> 00:11:41,760
So, that's my take on the quote.

126
00:11:41,760 --> 00:11:43,760
And actually, it's not quite worth the quote since.

127
00:11:43,760 --> 00:11:45,760
It's from the Jataka since.

128
00:11:45,760 --> 00:11:48,760
One of these past life stories.

129
00:11:48,760 --> 00:11:50,760
And there's definitely that aspect.

130
00:11:50,760 --> 00:11:53,760
You know, how many people don't even think to come and do a meditation course.

131
00:11:53,760 --> 00:12:00,760
You would never think, here we've put up this online meditation courses.

132
00:12:00,760 --> 00:12:05,760
And they're mostly empty these days.

133
00:12:05,760 --> 00:12:07,760
And I don't know how well they've received.

134
00:12:07,760 --> 00:12:09,760
I'm not giving that much on them.

135
00:12:09,760 --> 00:12:11,760
I just gave you the next exercise.

136
00:12:11,760 --> 00:12:13,760
But it puts the onus on the meditator.

137
00:12:13,760 --> 00:12:14,760
You have to do the work.

138
00:12:14,760 --> 00:12:16,760
I'm not going to give you much.

139
00:12:16,760 --> 00:12:19,760
I'll give you as the next exercise.

140
00:12:19,760 --> 00:12:23,760
Either you do it or you don't.

141
00:12:23,760 --> 00:12:27,760
We don't see a lot of people in these days even coming to meditate.

142
00:12:27,760 --> 00:12:30,760
Not that they're speaking.

143
00:12:30,760 --> 00:12:36,760
It's not so as hugely popular as one would hope.

144
00:12:36,760 --> 00:12:41,760
You know, we're very good at going to the doctor for our physical ailments.

145
00:12:41,760 --> 00:12:45,760
But we're not so concerned about our mental ailments.

146
00:12:45,760 --> 00:12:50,760
But we go to a physical doctor for them to fix our brains.

147
00:12:50,760 --> 00:12:53,760
Thinking that the brain is the problem.

148
00:12:53,760 --> 00:12:54,760
When is the brain?

149
00:12:54,760 --> 00:12:59,760
The brain is just a bunch of fat and cells.

150
00:12:59,760 --> 00:13:02,760
There's nothing in the brain.

151
00:13:02,760 --> 00:13:08,760
It's just a mindless organ.

152
00:13:08,760 --> 00:13:15,760
More chemicals in it just mixes up, mixes up with the chemicals.

153
00:13:15,760 --> 00:13:25,760
It's not the answer.

154
00:13:25,760 --> 00:13:34,760
So few are those who, few are those who are even stirred by things that are stirring.

155
00:13:34,760 --> 00:13:43,760
Who are moved by things that are moving or upset by things that they shouldn't be agitated by.

156
00:13:43,760 --> 00:13:51,760
But even fewer are those people who once they've been moved to action actually do act.

157
00:13:51,760 --> 00:14:00,760
They actually do go and find a teacher and listen carefully and appreciate and retain the teaching.

158
00:14:00,760 --> 00:14:05,760
And then actually put it into practice.

159
00:14:05,760 --> 00:14:08,760
That's where that's really curious.

160
00:14:08,760 --> 00:14:09,760
It's quite simple.

161
00:14:09,760 --> 00:14:11,760
So anyway, that's the quote.

162
00:14:11,760 --> 00:14:14,760
Some things to think about.

163
00:14:14,760 --> 00:14:17,760
Don't neglect going to see teachers.

164
00:14:17,760 --> 00:14:25,760
But my addition is, don't place too much emphasis on the teacher.

165
00:14:25,760 --> 00:14:27,760
Do the work.

166
00:14:27,760 --> 00:14:30,760
That's others to it.

167
00:14:30,760 --> 00:14:37,760
A lot of people just like to listen to talks and read books.

168
00:14:37,760 --> 00:14:42,760
It's very hollow, very empty in the end.

169
00:14:42,760 --> 00:14:48,760
It doesn't fill you up with wisdom.

170
00:14:48,760 --> 00:14:49,760
Okay.

171
00:14:49,760 --> 00:14:53,760
So what do we have questions?

172
00:14:53,760 --> 00:14:58,760
Is there a difference between karma and superstition making assumptions?

173
00:14:58,760 --> 00:15:04,760
It seems a lot of time people can use the law of universe karma past actions to do evils

174
00:15:04,760 --> 00:15:09,760
and make excuses for their own actions.

175
00:15:09,760 --> 00:15:13,760
You made me do this or you used to be like this.

176
00:15:13,760 --> 00:15:14,760
So that's your fault.

177
00:15:14,760 --> 00:15:18,760
I'm doing the best to make your life a living health.

178
00:15:18,760 --> 00:15:23,760
Yeah, well, there's lots of misunderstandings about karma.

179
00:15:23,760 --> 00:15:27,760
But we're getting on to something which is important.

180
00:15:27,760 --> 00:15:33,760
Is there anyone who thinks they know cause and effect of the nature of karma is the

181
00:15:33,760 --> 00:15:37,760
looting themselves, unless they're a Buddha.

182
00:15:37,760 --> 00:15:39,760
It's very complicated.

183
00:15:39,760 --> 00:15:43,760
So no one can say this is the reason why you're like this.

184
00:15:43,760 --> 00:15:50,760
We get a general sense and that general sense holds up, but it's not sure.

185
00:15:50,760 --> 00:15:53,760
We can still only guess and estimate.

186
00:15:53,760 --> 00:15:59,760
If someone is born sick, well there's often the reason, a mental reason for that.

187
00:15:59,760 --> 00:16:07,760
They were a mind that came into the womb was messed up and so it got messed up.

188
00:16:07,760 --> 00:16:15,760
It messed up the fetus and not only that, but it was drawn to a potentially problematic situation.

189
00:16:15,760 --> 00:16:18,760
It all came together wrong, which is karma.

190
00:16:18,760 --> 00:16:24,760
So we have this general idea that this is the way things go.

191
00:16:24,760 --> 00:16:32,760
But to actually understand how it works, it's not even necessary to understand how the intricacies of it go.

192
00:16:32,760 --> 00:16:33,760
What is the cause of this?

193
00:16:33,760 --> 00:16:35,760
What is the cause of that?

194
00:16:35,760 --> 00:16:47,760
What's important about karma is to see how your mind states affect the next moment, affect your mind, change your mind.

195
00:16:47,760 --> 00:16:49,760
When you get greedy, what is that like?

196
00:16:49,760 --> 00:16:52,760
When you're angry, what's the result of these things?

197
00:16:52,760 --> 00:16:55,760
When you're mindful, what's the result of that?

198
00:16:55,760 --> 00:17:00,760
And to understand that they change you are good and bad deeds, change us.

199
00:17:00,760 --> 00:17:09,760
They're habitual, they make their mark, they leave their mark on us.

200
00:17:09,760 --> 00:17:17,760
So that's the karma is the important, like the general sort of folk.

201
00:17:17,760 --> 00:17:24,760
Understand your karma is useful to understand evil leads to evil, good leads to good.

202
00:17:24,760 --> 00:17:30,760
But it's very general and vague.

203
00:17:30,760 --> 00:17:33,760
As meditators, what's important is this momentary understanding.

204
00:17:33,760 --> 00:17:49,760
When you watch your body and mind and you see how they interact and how they change based on your action.

205
00:17:49,760 --> 00:17:56,760
During sitting meditation, they might occur a loss of any definitively clear thought.

206
00:17:56,760 --> 00:18:01,760
There's awareness of tactile sensations and various ambience sounds.

207
00:18:01,760 --> 00:18:07,760
But a chaotic stream of extremely brief images.

208
00:18:07,760 --> 00:18:18,760
It becomes necessary to arbitrarily return to the clear thoughts we're rising upon.

209
00:18:18,760 --> 00:18:30,760
They can be physical, it can be mental.

210
00:18:30,760 --> 00:18:35,760
You're not always going to be able to be mindful clearly.

211
00:18:35,760 --> 00:18:45,760
For our purposes, the only thing we have to concern ourselves with is once we realize that it's happened.

212
00:18:45,760 --> 00:18:51,760
We realize that it's kind of day is there.

213
00:18:51,760 --> 00:18:56,760
When you realize, wow, my mind is kind of cloudy.

214
00:18:56,760 --> 00:19:05,760
It's to say to yourself, feeling, feeling, annoying, cloudy, cloudy even.

215
00:19:05,760 --> 00:19:11,760
Unclear, so confused.

216
00:19:11,760 --> 00:19:15,760
And that creates clarity as you do that.

217
00:19:15,760 --> 00:19:21,760
So don't just return to the rising and falling in the abdomen and try and be aware of your mind state.

218
00:19:21,760 --> 00:19:29,760
And doing that, you'll create clarity based on what's causing them.

219
00:19:29,760 --> 00:19:35,760
I mean, those kind of questions, again, it's like trying to understand karma.

220
00:19:35,760 --> 00:19:41,760
And it's just trying to, it is what it is, basically.

221
00:19:41,760 --> 00:19:45,760
And from a Buddhist point of view, you're not concerned about some kind of explanation of what it is.

222
00:19:45,760 --> 00:19:46,760
It's an experience.

223
00:19:46,760 --> 00:19:47,760
It's impermanent.

224
00:19:47,760 --> 00:19:48,760
It's unsatisfying.

225
00:19:48,760 --> 00:19:49,760
It's uncontrollable.

226
00:19:49,760 --> 00:19:54,760
That's all we need to know about it.

227
00:19:54,760 --> 00:20:04,760
And so by cultivating this remembrance, reminding yourself it is what it is, you'll get that clarity of mind.

228
00:20:04,760 --> 00:20:13,760
Any thoughts on explaining memory since things are impermanent?

229
00:20:13,760 --> 00:20:17,760
Where and how does the memory of them remain?

230
00:20:17,760 --> 00:20:18,760
Nothing remains.

231
00:20:18,760 --> 00:20:20,760
Everything arises and sees it.

232
00:20:20,760 --> 00:20:24,760
Memory is like an echo.

233
00:20:24,760 --> 00:20:25,760
That's it.

234
00:20:25,760 --> 00:20:27,760
And it's a very complicated echo.

235
00:20:27,760 --> 00:20:37,760
It's like the ripples, the changes that something makes.

236
00:20:37,760 --> 00:20:43,760
And when you apply the mind to that, there's the imprint in the universe, really.

237
00:20:43,760 --> 00:20:48,760
I mean, everything is, it's all one thing.

238
00:20:48,760 --> 00:20:50,760
The universe is one thing.

239
00:20:50,760 --> 00:20:55,760
So when something changes, when something occurs, it changes everything else.

240
00:20:55,760 --> 00:20:59,760
And you can see the echo or the imprint of it.

241
00:20:59,760 --> 00:21:02,760
You can, you know, much of this goes on in the brain.

242
00:21:02,760 --> 00:21:05,760
The brain is imprinted by experience.

243
00:21:05,760 --> 00:21:06,760
The brain doesn't store memory.

244
00:21:06,760 --> 00:21:07,760
It's the brain.

245
00:21:07,760 --> 00:21:09,760
It's just cells.

246
00:21:09,760 --> 00:21:14,760
It's proteins and fats and blood and that's what the brain is.

247
00:21:14,760 --> 00:21:23,760
But it's able to store changes, you know, an echo or an imprint of the experience

248
00:21:23,760 --> 00:21:26,760
that the mind and access.

249
00:21:26,760 --> 00:21:31,760
And it creates a new experience, which is like the old experience.

250
00:21:31,760 --> 00:21:34,760
That's how memory occurs.

251
00:21:34,760 --> 00:21:39,760
I mean, it's basically just how you'd think it occurs.

252
00:21:39,760 --> 00:21:45,760
Something special about it.

253
00:21:45,760 --> 00:21:48,760
But, right, somebody as far as impermanent.

254
00:21:48,760 --> 00:21:51,760
So if you're talking about from a momentary point of view,

255
00:21:51,760 --> 00:21:55,760
everything is, you know, it's still just cause and effect.

256
00:21:55,760 --> 00:21:57,760
It's one thing after another.

257
00:21:57,760 --> 00:22:03,760
And it's complicated enough to store as memories,

258
00:22:03,760 --> 00:22:07,760
meaning to give a new experience that is like an old experience.

259
00:22:07,760 --> 00:22:11,760
So one experience causes an experience that is like that.

260
00:22:11,760 --> 00:22:13,760
And then also the knowledge.

261
00:22:13,760 --> 00:22:18,760
This is like that, which is Sunday.

262
00:22:18,760 --> 00:22:22,760
But again, knowledge of how things work is not so useful.

263
00:22:22,760 --> 00:22:24,760
What's important is that they do what we're seeing,

264
00:22:24,760 --> 00:22:27,760
how they're seeing the way that they work.

265
00:22:27,760 --> 00:22:31,760
And understanding why they work that way.

266
00:22:41,760 --> 00:22:47,760
I can't wrap my head around the scientific certainty of the idea of reincarnation.

267
00:22:47,760 --> 00:22:50,760
Scientific certainty of the death of the universe.

268
00:22:50,760 --> 00:22:55,760
It is once all the stars burn out and all energy is lost.

269
00:22:55,760 --> 00:23:00,760
Well, you know, all energy is not lost.

270
00:23:00,760 --> 00:23:03,760
But energy is just a concept.

271
00:23:03,760 --> 00:23:10,760
It's just a word that we use to explain something that we're experiencing actually.

272
00:23:10,760 --> 00:23:14,760
Some orderliness.

273
00:23:14,760 --> 00:23:20,760
Some aspect of the nature of reality.

274
00:23:20,760 --> 00:23:23,760
But, you know, I've talked about rebirth a lot.

275
00:23:23,760 --> 00:23:25,760
It's not that we believe in reincarnation.

276
00:23:25,760 --> 00:23:27,760
It's that we don't believe in death.

277
00:23:27,760 --> 00:23:30,760
Death is just a concept.

278
00:23:30,760 --> 00:23:35,760
When you die, the mind and the body experience doesn't see its experience,

279
00:23:35,760 --> 00:23:40,760
just continues.

280
00:23:40,760 --> 00:23:43,760
There's no scientific certainty of it until you see it for yourself.

281
00:23:43,760 --> 00:23:46,760
When you, when you're reborn, then you'll have certainty of it.

282
00:23:46,760 --> 00:23:49,760
I mean, until you forget it, until you lose that.

283
00:23:49,760 --> 00:23:53,760
Because you don't have any of the echoes reminding you.

284
00:23:53,760 --> 00:23:57,760
It's very hard to remember past lives.

285
00:23:57,760 --> 00:24:01,760
Yeah, for humans anyway, because we were.

286
00:24:01,760 --> 00:24:05,760
We're caught up in these cycles of birth.

287
00:24:05,760 --> 00:24:09,760
Not like an angel or something or God.

288
00:24:09,760 --> 00:24:13,760
So, that's more smooth to transition.

289
00:24:13,760 --> 00:24:18,760
For us, the new brain is quite new.

290
00:24:18,760 --> 00:24:24,760
So, all the old imprints that create echoes of past lives are good.

291
00:24:24,760 --> 00:24:27,760
There's not about certainty to what understanding the nature.

292
00:24:27,760 --> 00:24:28,760
It's about argued.

293
00:24:28,760 --> 00:24:30,760
It's basically logic.

294
00:24:30,760 --> 00:24:36,760
The idea that there's death is what's uncertain.

295
00:24:36,760 --> 00:24:47,760
There's no real good evidence for death, except obviously it appears that the body has stopped working.

296
00:24:47,760 --> 00:24:55,760
But from the point of view, it had experienced that body didn't exist in the first place.

297
00:24:55,760 --> 00:24:57,760
There's only experiences.

298
00:24:57,760 --> 00:24:59,760
And they continue on.

299
00:24:59,760 --> 00:25:06,760
The only time they die is if you obtain the bond.

300
00:25:06,760 --> 00:25:17,760
What some people are appreciating you online, of course.

301
00:25:17,760 --> 00:25:25,760
It's a shame not more people join up, but maybe once I get back from Thailand to Asia.

302
00:25:25,760 --> 00:25:29,760
So, this Saturday, we have the big Buddha's celebration.

303
00:25:29,760 --> 00:25:33,760
And then next week, I'm off to Thailand and Sri Lanka.

304
00:25:33,760 --> 00:25:39,760
And maybe Taiwan actually looks like I may possibly take a trip to Taiwan.

305
00:25:39,760 --> 00:25:41,760
But nothing certain yet.

306
00:25:41,760 --> 00:25:45,760
I'll be back July 1st, I think.

307
00:25:45,760 --> 00:25:47,760
And then we'll start.

308
00:25:47,760 --> 00:25:57,760
No, before July 1st, May 29th, maybe.

309
00:25:57,760 --> 00:25:59,760
So, that's all for tonight then.

310
00:25:59,760 --> 00:26:01,760
Thank you all for tuning in.

311
00:26:01,760 --> 00:26:25,760
Thank you very much.

