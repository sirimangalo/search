The Buddhists see life as a kind of mistake that should sort itself out by obtaining
nibana.
If there was a way for life to avoid somehow suffering, or to somehow avoid suffering, would
it be okay for it to continue in the physical realm?
Well, if there is no suffering, then there wouldn't be any point, I guess, but you're
bound to have suffering in the physical realm.
Just because that's just the way it is.
Your body is uncomfortable, having to deal with things they don't like is uncomfortable.
You're bound to experience pain emotionally, mentally, physically.
So this just seems like the loss of fighting about things.
It doesn't really seem to matter if there wasn't suffering in the physical realm, because
there is and there has to be, there's no way that it couldn't be.
If I can get a little bit technical about the question, mistake kind of implies that at
one point, this idea that maybe one point in the past, we were in nibana and then we
oops, we somehow fell out of it, you know, reality or existence is, it exists, no.
And it always has, it is called existence and in an ultimate sense, that's really all
you can say about it is, it exists.
A few more things you can say, existence, the characteristic of existence is to arrive.
Anything that comes into existence, and maybe that's not quite correct, the word existence
isn't quite correct, but experience, no, let's put it this way, because this is what we
always say, reality is experience.
So experience is something that has been, or that exists, and experience, that which is
experience arises, so there is the arising, and that which arises is always also of the nature
disease.
This is actually what we're trying to realize in Buddhism.
If you realize just that fact, you become free from suffering, that, or you enter at least
and become a Sotopan, if you realize that everything young kimchi, samudaya, dhammang, sambantan,
niro, dhammang.
If you realize that, that's the entry into Sotopan, but it doesn't just mean you think
about it philosophically, it means you actually experience the cessation of everything.
So you realize that there's nothing that lasts.
We've never realized that, it's not something that is a part of experience, it's not something
that is, it's not a part of the loop, so when you get caught up in the loop, you'll never
be able to see this arising in sisu.
Because the loop is that of experiencing, clinging, acting, and then experiencing, clinging,
and acting.
The what starts with the kiles, the reacting, then there's the kama, the acting, and then
there's the vipaka, which is the experiencing, the result, and then when you experience
the result, you react and so on.
Because we're doing this, we never stop to actually look at what's making up the experience,
we're constantly reacting.
Because of that, we've always been in some sorry, it's only a person who is able to stop
the ferris wheel or the merry-go-round, and actually begin to look at what they're doing,
look at what's going on, is able to free themselves.
So it's not a mistake, it's an experience, it's the state of experiencing things.
Now Buddhism isn't a religion of complaining and saying, you know, there's something
wrong here, you have to do something, it's pointing out that this can never be stable,
it can never be satisfied, it has the nature to, or it's not static, so it has the
nature to lead in one direction or the other, if a person does good things, they become
a good person and become happy, and then if in when they become negligent, they fall
down and become unhappy, they do bad things and become unhappy.
So this is why it's, the reason why it's never possible to avoid suffering is because
it's not static, so even if you're able to avoid suffering for a billion billion billion
billion years, or a Google years, Googleplex years, let's say, or from the time of a big
bang to a big crunch, or a cold death, or whatever, if you were able, this, this, whatever
they conjecture, even that isn't freedom from suffering because it's still impermanent.
It's still not static, as long as there is the mind creating more karma, you know, based
on it's kile, so it's desires, it's partiality, there will be more karma, when there's
more karma, there will be more results and there will be a change.
The Buddha said, Manopobangamadam reality experience comes from the mind, so the only way
you could have something static is to get rid of the mind, if you get rid of the mind you
could have a robot that was perhaps always happy, but there would be, well, yeah, the robot
would be fine, you wouldn't, you wouldn't say the robot would be unhappy, this computer
is never unhappy, for example, microphone is never unhappy, it's got no problem,
because there's no mind, but the, the mind that experience is has a problem, because it's
clinging and always creating, it's going around in the circle, Buddhism teaches us to,
to stop that and to create a mind or to develop a experience of reality that doesn't
cling and that doesn't create, because it doesn't cling and doesn't create, it, it
becomes stable, it becomes satisfying, it becomes permanent, it becomes constant, so there
is, there is never any suffering and the experiences that have been created in the past
begin to fade away and in the end there is only this basic experience of seeing, hearing,
smelling, tasting, feeling, thinking which also disappears, and so the person who enters
into Nibbana does so because they've entered into a state of non-clinging of non-creating
and if you don't enter into that state you're always going to be creating, you're always
going to be changing things, you have to remember that Buddhism hasn't, hasn't avoided
this, the Buddha didn't, it didn't slip by him this idea of creating a stable and lasting
reality in Samsara, the Buddha actually remembered these kind of states and he talked about
these kind of states, the state of a Brahma for example, so these people talking about
maybe what if we put the human mind into a machine or we altered the brain in such a way,
fixed it up so that it never got old and so that it was constantly in some kind of pleasure,
this kind of pleasurable state and they say well there, there you go, there's something
that's transhumanism that you read about on our form, someone was asking this, but that's
pitiful in comparison with the age of a Brahma, a God that the Buddha was talking about
and remembered and explained how to get to, you don't need robots to do it, just leave
the body behind, develop the mind to the extent that it leaves behind the body and it
can do that for an entire kapa, an entire age, even longer I think, or certain Brahma
realms that are above the Big Bang, so even the Big Bang doesn't affect them, this
or the Big Crunch or whatever, the changing of the kapa doesn't affect them and then
there's maybe, I don't know, cosmology is, it's quite detailed actually, so this robot
is certainly not going to last for that long, but even a Brahma does and even a Brahma
is not free from suffering, you never can be, that's not going to say no to say by very
nature, experience is unsatisfying and uncontrollable, it's not static, it's not static
because there is the intentions, this is why the Buddha said the Kilesar, the problem,
the filaments in our mind, because we want something, because we are not satisfied with
things as they are, we make some attempt to change things, the only way to become satisfied
is to give up our desire for something that doesn't exist, to give up our desire for any
impermanent thing, and once we give up that desire, the mind naturally inclines towards
the Bhana and not towards the arising, how could it, it wouldn't ever give rise to the
intention to create anything at all, because it doesn't give rise to the intention to create,
it can't help, but the things that are created will not increase, there's no feeding
of the fires of the fire eventually goes out, that's the answer.
