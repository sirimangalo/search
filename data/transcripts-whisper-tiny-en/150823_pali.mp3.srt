1
00:00:00,000 --> 00:00:11,760
Okay, so we're starting recording Pauli. We've just looked at number 26 of the

2
00:00:11,760 --> 00:00:22,360
sentences on the last page. T-1-do-natsukang-la-be-la-be, which we found to mean that

3
00:00:22,360 --> 00:00:27,240
someone does someone whether it's I, we're not sure about the tense of the verb,

4
00:00:27,240 --> 00:00:36,240
living, doesn't obtain, don't obtain happiness, or use it comfort, but T-1-do-natsukang

5
00:00:36,240 --> 00:00:41,400
really just means happiness. I mean, could have different meanings, but it is

6
00:00:41,400 --> 00:00:47,800
the word that we use for happiness, the general positive feeling.

7
00:00:47,800 --> 00:00:58,320
So number 27 is a bit of a tricky because we've got some Sunday going on

8
00:00:58,320 --> 00:01:06,440
here. It should be such a, such a is a part of simple, that means if, and the hung

9
00:01:06,440 --> 00:01:28,680
means I, it's the subject of the center. So this means if I, if I am grasped in

10
00:01:28,680 --> 00:01:38,040
such a way, then I will not exist. I think this is, yeah, the root nya, which means to

11
00:01:38,040 --> 00:01:47,640
know. If I will have known that because it's a, it's called the Kalatipati, the final

12
00:01:47,640 --> 00:02:04,440
tense, right? We'll have known that I will tell number is it. If I had known this, if I

13
00:02:04,440 --> 00:02:25,120
will have known this, tongue might be the subject. Yeah, the subject of the second

14
00:02:25,120 --> 00:02:36,160
half is tongue, tongue is one one, not two one, it's not the object. So you, you, you

15
00:02:36,160 --> 00:02:40,600
missed something when you say, then I will not exist. What about the tongue, right?

16
00:02:40,600 --> 00:02:45,960
I believe that, I mean, it's a third person actually, it's third person Kalatipati,

17
00:02:45,960 --> 00:02:53,880
which means it's the past of the future. We'll have not, we'll have not been

18
00:02:53,880 --> 00:03:12,480
it's an odd one. So if I, if I will have known this, I don't know, I think it's

19
00:03:12,480 --> 00:03:34,880
an odd example, but if I will have known this, that tongue, now, but we will not have been.

20
00:03:34,880 --> 00:03:39,480
I mean, I guess you can interpret this to me and when there comes a time that I know

21
00:03:39,480 --> 00:03:47,480
something, you know, know that some problem will not be at that time. Once I understand

22
00:03:47,480 --> 00:03:52,840
things, you can say, once I will have understood the truth by that time, all of that

23
00:03:52,840 --> 00:04:15,280
suffering will not be. Yeah, I suppose in English the word once is useful for this tent.

24
00:04:15,280 --> 00:04:26,160
Once I have come to know this, then that will not, that suffering will have come to not

25
00:04:26,160 --> 00:04:36,440
be. Number 28.

26
00:04:36,440 --> 00:05:03,880
At least there's one familiar word, duka. What tents is duka? What is the number? Yes, it

27
00:05:03,880 --> 00:05:10,680
is ablative singular because you can become free from something much. The root much has

28
00:05:10,680 --> 00:05:38,560
to do with being free.

29
00:05:38,560 --> 00:06:08,000
And what's the subject of the sentence? I think he a new nut means not what if, but it's

30
00:06:08,000 --> 00:06:22,360
more like, what the heck, I should become free from suffering. I should free myself from suffering.

31
00:06:22,360 --> 00:06:43,560
I think, am I missing it from me, supposing it with something else? No, maybe you're right. It

32
00:06:43,560 --> 00:06:47,400
maybe it does mean what if I should become free from suffering. But it's kind of like

33
00:06:47,400 --> 00:07:03,800
a speculative, hey, what if I were to try this? I think. Not exactly. It's a, what do

34
00:07:03,800 --> 00:07:14,240
you call a rhetorical question, I think, because there's Januna hung Papa J. Yami. What the heck,

35
00:07:14,240 --> 00:07:19,440
you know, what, hey, what if I were to, hey, what if I were to ordain? Meaning, why

36
00:07:19,440 --> 00:07:30,360
don't I try that? Young does mean that what? Young means what? The new nut means kind

37
00:07:30,360 --> 00:07:44,640
of like, it's emphatic. Like, why not?

38
00:08:00,360 --> 00:08:29,440
Yeah, the folly English dictionary says, young nuna hung means now then, let me.

39
00:08:29,440 --> 00:08:40,040
Januna used in an exhortative sentence, well now, or rather, or rather let me, or so

40
00:08:40,040 --> 00:08:48,080
now, always in phrase, Januna hung now then, let me do this or that very frequently either

41
00:08:48,080 --> 00:08:58,320
with following potential, which is the septomy, right? For example, Januna hung Aranyang

42
00:08:58,320 --> 00:09:08,200
always, but we say young. Now then, let me go well in the forest, enter into the forest,

43
00:09:08,200 --> 00:09:17,640
or Aranya, the forest monastery. Young nuna hung, cut the command, put J. Now then, let

44
00:09:17,640 --> 00:09:32,720
me ask the deed that was done, ask what was done. But I think literally it is what

45
00:09:32,720 --> 00:09:41,200
if I were to, what if I should do this, but it's kind of a rhetoric rhetorical instigating

46
00:09:41,200 --> 00:10:03,760
or pushing exhorting yourself to do it.

47
00:10:03,760 --> 00:10:31,600
Close, they're both close, but it's not not, Brian's got it right, it's ma, so ma means

48
00:10:31,600 --> 00:10:38,880
don't, so it has to go, oh no, Ryan doesn't have it right either, you've got it wrong,

49
00:10:38,880 --> 00:10:47,800
ma isn't not, ma always goes with the imperative, which should give you a clue, always gives

50
00:10:47,800 --> 00:11:03,160
you a clue as to the case of the verb, but what that may be, no, it's not, oh what

51
00:11:03,160 --> 00:11:18,440
that is imperative.

52
00:11:34,160 --> 00:11:44,400
But it is past, it's a past imperative, how does that work, so it's actually not an

53
00:11:44,400 --> 00:11:59,280
enchantment case, anyway, ma means don't, don't be someone, don't be someone who has become

54
00:11:59,280 --> 00:12:05,240
something like that, the sense of using the past tense, but I mean literally it means

55
00:12:05,240 --> 00:12:16,000
don't be, we put this Arino, we put this Arino, I mean someone who, well it's remorseful,

56
00:12:16,000 --> 00:12:26,280
but it's more, it's deeper than we put this, so we put this Arino, sorry means one who

57
00:12:26,280 --> 00:12:41,320
reflects.

58
00:12:56,280 --> 00:13:11,040
One who becomes, Sarino is one who takes as a refuge sort of, Sarino, Sarino comes from

59
00:13:11,040 --> 00:13:19,400
Sar, the root Sar with a, whatever that suffix is that turns it into Sarino, one who

60
00:13:19,400 --> 00:13:25,240
is, or one who does, one who reflects, but in this case, one who takes as refuge kind of

61
00:13:25,240 --> 00:13:33,520
everyone who reflects upon, but it means one who relies, but it means, Patik is the meaning

62
00:13:33,520 --> 00:13:43,520
of relying upon or falling back upon, Ovi is in the sense of in a bad way, or in a messed

63
00:13:43,520 --> 00:13:53,640
up way, in an orderly, disorderly way, so literally it means one who's mind, one who's

64
00:13:53,640 --> 00:14:02,920
state of, no base state, but this Arino means fall back state kind of, one who's fall

65
00:14:02,920 --> 00:14:13,040
back state, or state of mind really, is messed up, meaning who is disturbed, and it comes

66
00:14:13,040 --> 00:14:19,600
to me remorseful, don't be someone who is remorseful, but cham means afterwards, always

67
00:14:19,600 --> 00:14:25,240
remember, my is not, it's not, it's don't, that'll help you a lot with, figuring out

68
00:14:25,240 --> 00:14:27,600
the flavor of the sentence.

69
00:14:27,600 --> 00:14:46,600
Yeah, the digital poly reader Ma comes up, it comes up as not, do not, let us hope not.

70
00:14:46,600 --> 00:14:57,840
Yeah, it should never be not, I don't can't think of where it would be that, it's always

71
00:14:57,840 --> 00:15:07,080
with imperative, I mean, here it's not with imperative, I think, I think that is a past

72
00:15:07,080 --> 00:15:14,800
tense verb, but it still gives the sense of imperative.

73
00:15:14,800 --> 00:15:19,120
This is actually a sentence of the Buddha, the rest of the many of them above are not actually,

74
00:15:19,120 --> 00:15:24,000
I think, from the Buddha, but this one is something the Buddha actually said, you know, meditate

75
00:15:24,000 --> 00:15:32,320
now, don't be remorseful later, that's the full quote.

76
00:15:32,320 --> 00:15:47,660
Very good.

77
00:15:47,660 --> 00:16:07,860
I don't know, I think it's been a long day and I've still got more, I've actually got

78
00:16:07,860 --> 00:16:14,380
to go back, I think let's stop the poly there because I've got other things happening

79
00:16:14,380 --> 00:16:22,420
today. And we'll figure out about next week, maybe we should discuss it during the week

80
00:16:22,420 --> 00:16:27,900
because we may have to just stop because it's getting close to the time where I have

81
00:16:27,900 --> 00:16:35,860
to move and do school and stuff. So, I mean, doing more poly means, we potentially

82
00:16:35,860 --> 00:16:46,860
it means just going through the textbook, which is easy for me. But if it means learning

83
00:16:46,860 --> 00:16:50,940
more about the compounds and so on, that might be a little more work and maybe a little

84
00:16:50,940 --> 00:16:54,780
more than I'm just taking on right now.

85
00:16:54,780 --> 00:17:01,580
That's what I was wondering if you wanted to take a hiatus from the poly for a few

86
00:17:01,580 --> 00:17:06,380
months, maybe, we're kind of at a breaking point being at the end of the textbook and

87
00:17:06,380 --> 00:17:10,300
you do have a lot of stuff going on with starting school and starting the monastery and

88
00:17:10,300 --> 00:17:11,300
all that.

89
00:17:11,300 --> 00:17:18,380
Yeah, again, it depends. If you guys just want to go through the Jekyll Poly text, it's

90
00:17:18,380 --> 00:17:22,980
a bit draining always to have to think, but that's not a big deal. In an hour of poly

91
00:17:22,980 --> 00:17:29,980
it's not going to cut into my life. But, you know, there may be Sundays where I can't

92
00:17:29,980 --> 00:17:35,540
do, but Sunday is usually a good day. So, it's not really a big deal. But maybe I'll

93
00:17:35,540 --> 00:17:42,380
have to say no to doing the rest of the textbooks. Maybe that's something for next summer.

94
00:17:42,380 --> 00:17:48,260
So, just do the translations from Jekyll Poly after the Visudemaka?

95
00:17:48,260 --> 00:17:54,260
Yeah, I mean, that's how you learn poly the best anyway. I mean, I can give you all the

96
00:17:54,260 --> 00:17:58,740
knowledge that's in the rest of the text or enough to get you by. I mean, obviously, I'm

97
00:17:58,740 --> 00:18:03,180
not a scholar or anything, but we can stumble through it and you'll learn poly naturally.

98
00:18:03,180 --> 00:18:08,740
When you get a feel for it. That sounds good. Yeah, if you have the time for that, that

99
00:18:08,740 --> 00:18:12,740
sounds a lot more laid back.

100
00:18:12,740 --> 00:18:18,980
Yeah, it's good for me, good practice for me, and we can just do that forever because

101
00:18:18,980 --> 00:18:23,740
there's 327 stories. We can go through them all.

102
00:18:23,740 --> 00:18:28,940
Yeah, considering it's taken us a few weeks to get as far as we have, which is what about

103
00:18:28,940 --> 00:18:30,740
three paragraphs. That'll take a while.

104
00:18:30,740 --> 00:18:45,020
Yeah, I mean, it's just fun. Be nice if we had a better platform, but I'll have to think

105
00:18:45,020 --> 00:18:46,020
about that.

106
00:18:46,020 --> 00:18:52,860
Yeah, definitely the open meeting. If we can figure out what's causing it to be buggy and

107
00:18:52,860 --> 00:18:55,860
work through that, that seems like it would be really good.

108
00:18:55,860 --> 00:19:01,060
You know, I think it's just always been buggy. I don't think it's that production stage.

109
00:19:01,060 --> 00:19:04,860
Oh, okay. Nothing we're doing wrong then.

110
00:19:04,860 --> 00:19:13,660
I don't think so. Okay, I'm going to stop recording. Thank you all for coming and see you

111
00:19:13,660 --> 00:19:19,060
all next week. If you're here. Yeah, well, maybe we can stick around a bit after this

112
00:19:19,060 --> 00:19:23,060
and talk about what we want to do next. Thank you, Bontay.

