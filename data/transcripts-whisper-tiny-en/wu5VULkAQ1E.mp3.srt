1
00:00:00,000 --> 00:00:10,000
I understand that one should let go of the past, but I have difficulty understanding how to let go of the future altogether, because of course one has to plan things.

2
00:00:16,000 --> 00:00:20,000
Well, the point is that you do the planning in the present moment.

3
00:00:20,000 --> 00:00:33,000
It's a bit misleading, you know, because you don't really want to give up the past.

4
00:00:34,000 --> 00:00:48,000
I guess the important distinction we have to make is between conventional reality and ultimate reality or our life in the world and our life as a meditator.

5
00:00:48,000 --> 00:00:52,000
Because from a meditative point of view, you take everything in the present moment.

6
00:00:53,000 --> 00:00:57,000
When you think about the past, you're actually thinking in the present moment, so you look at it as a thought.

7
00:00:58,000 --> 00:01:07,000
When you think about the future, when you plan about the future, all of the thoughts and emotions, they're all in the present moment, and so you take those as the meditation object.

8
00:01:08,000 --> 00:01:12,000
But on a conventional level, you're still doing planning and you're still remembering the past.

9
00:01:12,000 --> 00:01:14,000
Why is it even important to remember the past?

10
00:01:14,000 --> 00:01:18,000
Because otherwise you wouldn't be able to learn from your mistakes.

11
00:01:18,000 --> 00:01:24,000
You wouldn't be able to avoid things that are dangerous.

12
00:01:24,000 --> 00:01:29,000
If you saw an elephant coming, you'd be like, oh, forget about the past.

13
00:01:29,000 --> 00:01:36,000
You don't think about the fact that you've heard that elephants will kill you or whatever.

14
00:01:36,000 --> 00:01:41,000
If someone betrays you, then to avoid the person.

15
00:01:41,000 --> 00:01:46,000
Or if you find out that someone is a terrible, corrupt individual, then you have to remember that.

16
00:01:46,000 --> 00:01:50,000
You have to keep that in mind. You don't want to say, forgive and forget.

17
00:01:50,000 --> 00:01:52,000
Forgive, but don't ever forget.

18
00:01:52,000 --> 00:01:55,000
Because you'll get taken again.

19
00:01:55,000 --> 00:02:04,000
You'll fall into more suffering and have more problems on a conventional level.

20
00:02:04,000 --> 00:02:16,000
And so to live in the world, in the more worldly pursuits you have,

21
00:02:16,000 --> 00:02:20,000
the more you'll have to keep in mind the past and the future and plan.

22
00:02:20,000 --> 00:02:26,000
The point is that from a meditator, from a meditative point of view.

23
00:02:26,000 --> 00:02:33,000
There's a more important task that needs to be done, and that is to see the activities clearly.

24
00:02:33,000 --> 00:02:39,000
And so when you're planning, the meditative side of you will take the present moment.

25
00:02:39,000 --> 00:02:48,000
When you're thinking about the future, the meditative side of you will take the present moment, will purify the thoughts.

26
00:02:48,000 --> 00:03:00,000
The actual teaching on letting go of the past and letting go of the future is only fully applicable for a full-time meditator.

27
00:03:00,000 --> 00:03:06,000
Someone who is in intensive meditation has no need to plan anything.

28
00:03:06,000 --> 00:03:09,000
And should get to the point where they have no planning.

29
00:03:09,000 --> 00:03:11,000
They have no need for planning.

30
00:03:11,000 --> 00:03:12,000
Very minimal planning.

31
00:03:12,000 --> 00:03:17,000
Like you might say, planning to go for food.

32
00:03:17,000 --> 00:03:21,000
Or knowing that it's now the time to go for food or so on.

33
00:03:21,000 --> 00:03:23,000
Ideally, there is no such planning.

34
00:03:23,000 --> 00:03:28,000
And in fact, we get meditators to stay in their room sometimes.

35
00:03:28,000 --> 00:03:30,000
And we send them food.

36
00:03:30,000 --> 00:03:31,000
So there's a knock on their door.

37
00:03:31,000 --> 00:03:34,000
They don't even have to think about when food is going to come.

38
00:03:34,000 --> 00:03:41,000
This sort of person does not need any sort of planning or any knowledge of the past either.

39
00:03:41,000 --> 00:03:47,000
Because from a meditative, it's wrong to think that you need to know about the past in the future in terms of meditation.

40
00:03:47,000 --> 00:03:51,000
Now your question makes it clear that you're talking about worldly things.

41
00:03:51,000 --> 00:03:54,000
As a meditator, you don't need to plan.

42
00:03:54,000 --> 00:04:00,000
But some people still think that as a meditator, you have to be able to consider the past in the future.

43
00:04:00,000 --> 00:04:05,000
From a meditative point of view, this is from an inside point of view, this is not correct.

44
00:04:05,000 --> 00:04:08,000
This is not useful.

45
00:04:08,000 --> 00:04:12,000
It's not useful to think about the past or to think about the future when you're meditating.

46
00:04:12,000 --> 00:04:19,000
Even in terms of thinking about why am I the way I am now, or what will this lead to in the future?

47
00:04:19,000 --> 00:04:33,000
It's not the solution in terms of insight meditation to worry about the results of bad deeds, for example.

48
00:04:33,000 --> 00:04:40,000
Or to encourage yourself about the benefits of good deeds.

49
00:04:40,000 --> 00:04:51,000
It's not beneficial to say this suffering came from bad deeds, or this happiness came from good deeds, for example.

50
00:04:51,000 --> 00:04:58,000
These are not the meditative practice or the meditative process.

51
00:04:58,000 --> 00:05:03,000
They can be useful for meditators, especially in the beginning when you need this kind of encouragement,

52
00:05:03,000 --> 00:05:09,000
or when you need this kind of intellectual verification to get rid of your doubts, spell your doubts.

53
00:05:09,000 --> 00:05:12,000
But the practice of meditation is to see things as they are.

54
00:05:12,000 --> 00:05:20,000
If you look at the four noble truths, you don't practice them to understand the relationship between them.

55
00:05:20,000 --> 00:05:27,000
The relationship between them exists, you learn only one thing, and that is what is suffering.

56
00:05:27,000 --> 00:05:31,000
You understand that the things that you cling to are actually suffering.

57
00:05:31,000 --> 00:05:34,000
You don't worry about the clinging aspect, you worry about the suffering aspect.

58
00:05:34,000 --> 00:05:41,000
When you learn that something is suffering, the second noble truth is not that you learn what is the cause of it.

59
00:05:41,000 --> 00:05:44,000
The second noble truth is that you give up the cause, you give up the craving.

60
00:05:44,000 --> 00:05:47,000
Once you see that something is suffering, you give up the craving for it.

61
00:05:47,000 --> 00:05:49,000
This is the cessation of suffering.

62
00:05:49,000 --> 00:05:54,000
The practice of seeing suffering is the path that leads to the cessation of suffering.

63
00:05:54,000 --> 00:05:56,000
These are the four noble truths.

64
00:05:56,000 --> 00:05:58,000
It's totally in the present moment.

65
00:05:58,000 --> 00:06:02,000
You don't ever need to consider cause and effect.

66
00:06:02,000 --> 00:06:10,000
Not ever, but the core of the practice is not the consideration of cause and effect.

67
00:06:10,000 --> 00:06:19,000
Even though that can be useful on an intellectual level to do so.

68
00:06:19,000 --> 00:06:22,000
The actual practice is the present moment.

69
00:06:22,000 --> 00:06:26,000
Your question is easier, it's dealing with planning.

70
00:06:26,000 --> 00:06:32,000
I think I dealt with that just to see things on two different levels.

71
00:06:32,000 --> 00:06:40,000
When you're planning to try to plan mindfully and thereby incorporate the two facets of your existence together.

72
00:06:40,000 --> 00:06:45,000
You're able to incorporate your meditation as best as possible in your daily life.

73
00:06:45,000 --> 00:06:49,000
I think it goes without saying that it will benefit your planning.

74
00:06:49,000 --> 00:06:55,000
It will benefit your memories and interactions with the past as well.

75
00:06:55,000 --> 00:07:02,000
When you're mindful about these things, even though you have to deal with both the past and the future on the conventional level.

76
00:07:02,000 --> 00:07:06,000
Until you become a monk and go and live in the forest.

77
00:07:06,000 --> 00:07:13,000
Even then you have to, as long as you're still engaged in the world, you still have to plan things.

78
00:07:13,000 --> 00:07:18,000
You do it mindfully and you know that you're planning here in the present moment.

79
00:07:18,000 --> 00:07:20,000
This planning is not a future thing.

80
00:07:20,000 --> 00:07:26,000
Planning is a preparing thing. I'm doing something that is important to do here now.

81
00:07:26,000 --> 00:07:35,000
The other thing I would say just to add one more thing is that very little planning needs to be done as a meditator.

82
00:07:35,000 --> 00:07:38,000
Very little preparation needs to be done.

83
00:07:38,000 --> 00:07:42,000
Very little thought about the future needs to be made.

84
00:07:42,000 --> 00:07:51,000
It's an interesting aspect of wisdom that it just knows. It knows what is right. It doesn't have to think about what is right.

85
00:07:51,000 --> 00:07:56,000
When you know what is right, it's instantaneous.

86
00:07:56,000 --> 00:08:03,000
The thinking process is more often than not just getting in the way.

87
00:08:03,000 --> 00:08:14,000
The answer that comes from clarity of mind, from mindfulness, is more often than not the correct one.

88
00:08:14,000 --> 00:08:18,000
I might even say always the correct one.

89
00:08:18,000 --> 00:08:27,000
You do have to put some thought into things in terms of remembering things on an intellectual level.

90
00:08:27,000 --> 00:08:35,000
But the correct thing to say and to do is always accompanied with mindfulness.

91
00:08:35,000 --> 00:08:40,000
It's the mindfulness that's going to lead you to do and to say the correct thing.

92
00:08:40,000 --> 00:08:47,000
So even planning will become purified and will not be so much planning as it will be doing what is necessary.

93
00:08:47,000 --> 00:08:52,000
You need to fill this bag with things that are necessary for this situation.

94
00:08:52,000 --> 00:08:57,000
So you put everything in the bag and you know exactly what is necessary for this situation.

95
00:08:57,000 --> 00:08:59,000
There's nothing to do with thoughts about the future.

96
00:08:59,000 --> 00:09:02,000
Oh, what am I going to do when I get there? Where am I going to go or so on?

97
00:09:02,000 --> 00:09:08,000
It's applying a process to a problem.

98
00:09:08,000 --> 00:09:14,000
You have this problem that needs to be solved and it's the solution of the problem in the present moment.

99
00:09:14,000 --> 00:09:21,000
It becomes more like that for someone who is mindfulness, but doesn't deal with worries or concerns about the future.

100
00:09:21,000 --> 00:09:23,000
Thank you.

