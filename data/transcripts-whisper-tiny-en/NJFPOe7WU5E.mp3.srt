1
00:00:00,000 --> 00:00:07,000
How to deal with the fear of leaving Samsara in joining the song.

2
00:00:07,000 --> 00:00:20,000
I don't know. Fear, fear, afraid, afraid.

3
00:00:20,000 --> 00:00:28,000
I don't know. I'm leaving Samsara.

4
00:00:28,000 --> 00:00:35,000
Well, I don't think that you're guaranteed to leave Samsara if you become a monk.

5
00:00:35,000 --> 00:00:45,000
I mean, that's probably your goal in becoming a monk, but it's not guaranteed.

6
00:00:45,000 --> 00:00:48,000
And I don't think...

7
00:00:48,000 --> 00:00:55,000
I don't think that leaving Samsara is what would be...

8
00:00:55,000 --> 00:00:58,000
is what is making you afraid.

9
00:00:58,000 --> 00:01:04,000
I don't think anyone's like, oh no, I don't want to be born a million more times suffering.

10
00:01:04,000 --> 00:01:11,000
I think it's just... you're afraid of leaving all your attachments and the things that you like.

11
00:01:11,000 --> 00:01:16,000
It's not that you're afraid of not being born again.

12
00:01:16,000 --> 00:01:23,000
And for dealing with your attachments and such, it's all just...

13
00:01:23,000 --> 00:01:27,000
it's just you creating the drama of your life.

14
00:01:27,000 --> 00:01:33,000
You just paint this picture of how everything is, so...

15
00:01:33,000 --> 00:01:37,000
that you can claim to... it gives you a reason to claim to it.

16
00:01:37,000 --> 00:01:42,000
You have to... you have to make things important for yourself.

17
00:01:42,000 --> 00:01:47,000
You have to make things about yourself important about others and how they relate to you.

18
00:01:47,000 --> 00:01:58,000
And that's all it is, is you creating a drama. If you just see that there's nothing really solid about any of it, then...

19
00:01:58,000 --> 00:02:05,000
I don't think that's such a big deal.

20
00:02:05,000 --> 00:02:17,000
Well, I tried it, I joined the Sangha and couldn't leave Sangha by that, so it doesn't really work like that.

21
00:02:17,000 --> 00:02:24,000
There is probably always fear before you ordain and high expectations.

22
00:02:24,000 --> 00:02:30,000
And that is probably why the fear arises so... so much, so strong.

23
00:02:30,000 --> 00:02:37,000
Because there are these strong expectations that you leave some sorrow, and something is going to happen.

24
00:02:37,000 --> 00:02:43,000
And what Nagazena mentioned about the attachments.

25
00:02:43,000 --> 00:02:51,000
Of course, there is a fear that you cannot have these things anymore.

26
00:02:51,000 --> 00:02:56,000
I almost didn't ordain because I like bathing.

27
00:02:56,000 --> 00:03:04,000
And in a bathtub, I used to bathe every evening, almost every day.

28
00:03:04,000 --> 00:03:07,000
So I thought I couldn't live without it.

29
00:03:07,000 --> 00:03:11,000
And that really caused fear.

30
00:03:11,000 --> 00:03:15,000
Like, how can I live like that then?

31
00:03:15,000 --> 00:03:20,000
And you got to try. You just do it.

32
00:03:20,000 --> 00:03:23,000
And then you know it's possible. You can do it.

33
00:03:23,000 --> 00:03:36,000
Usually.

34
00:03:36,000 --> 00:03:45,000
Well, it's easy to want to become a monk.

35
00:03:45,000 --> 00:03:51,000
It's easy to want to join the Sangha.

36
00:03:51,000 --> 00:03:56,000
And it's a lot more difficult to actually do it.

37
00:03:56,000 --> 00:04:04,000
And it's even more difficult to enjoy and to be at peace with it.

38
00:04:04,000 --> 00:04:10,000
This is from experience because I've tried to help many people to become monks.

39
00:04:10,000 --> 00:04:13,000
And some people just aren't ready for it.

40
00:04:13,000 --> 00:04:20,000
Most people are in this state where they like the idea.

41
00:04:20,000 --> 00:04:30,000
Not most people, but most people who come across this question are at the state where they like the idea.

42
00:04:30,000 --> 00:04:36,000
And they understand theoretically how great it is.

43
00:04:36,000 --> 00:04:42,000
But emotionally and mentally, they're not really there.

44
00:04:42,000 --> 00:04:54,000
They're attachments and their views and opinions and they're often the relationships.

45
00:04:54,000 --> 00:04:59,000
They're attachment or the clinging to friends and family.

46
00:04:59,000 --> 00:05:04,000
They're so strong in them that even were they to our day.

47
00:05:04,000 --> 00:05:07,000
It would be miserable and it would be short.

48
00:05:07,000 --> 00:05:16,000
And they would quickly leave it.

49
00:05:16,000 --> 00:05:28,000
So I think there is room to say to start doing good deeds now.

50
00:05:28,000 --> 00:05:35,000
Because it's those good deeds that are going to be a support for you to join the Sangha

51
00:05:35,000 --> 00:05:38,000
and a support for you to give up that fear.

52
00:05:38,000 --> 00:06:00,000
Which is totally based on attachment and based on the aversion or the inability or expectations.

53
00:06:00,000 --> 00:06:08,000
So being charitable in terms of giving up, giving up your desires and your wants.

54
00:06:08,000 --> 00:06:16,000
Being moral in terms of abstaining from many different things that are unwholesome.

55
00:06:16,000 --> 00:06:23,000
This is a very important step because it's going to prepare you for a greater amount of giving up.

56
00:06:23,000 --> 00:06:31,000
And a greater amount of abstention which is the monkhood.

57
00:06:31,000 --> 00:06:38,000
I think you can't emphasize enough the importance of preparing for monasticism.

58
00:06:38,000 --> 00:06:42,000
It's not just a leap into freedom.

59
00:06:42,000 --> 00:06:53,000
It's the application for a course of training.

60
00:06:53,000 --> 00:06:58,000
It's the beginning when you are day and you begin to train.

61
00:06:58,000 --> 00:07:00,000
So I think there's room for that.

62
00:07:00,000 --> 00:07:07,000
But on the other hand, in terms of in relation to what was already said.

63
00:07:07,000 --> 00:07:13,000
It's not really that bad in the end and many of our fears.

64
00:07:13,000 --> 00:07:16,000
It turned out to be irrational.

65
00:07:16,000 --> 00:07:26,000
So for instance, I almost cried when I was a young monk and I didn't have any soap.

66
00:07:26,000 --> 00:07:32,000
And I felt so sad for myself that I had no soap.

67
00:07:32,000 --> 00:07:37,000
And then it just hit me like an epiphany.

68
00:07:37,000 --> 00:07:42,000
If you don't have any soap, then bathe without soap.

69
00:07:42,000 --> 00:07:46,000
It's funny how caught up we get.

70
00:07:46,000 --> 00:07:49,000
And it was that experience.

71
00:07:49,000 --> 00:07:54,000
Really that simple silly experience that set the tone for a lot of different things.

72
00:07:54,000 --> 00:07:56,000
Because then I didn't need laundry powder.

73
00:07:56,000 --> 00:08:01,000
If I didn't have laundry powder, I would wash my clothes without laundry powder.

74
00:08:01,000 --> 00:08:08,000
Which is also, these are just silly things, but the philosophical ramifications were

75
00:08:08,000 --> 00:08:10,000
you don't have a place to stay.

76
00:08:10,000 --> 00:08:12,000
We'll go and sleep on a park bench.

77
00:08:12,000 --> 00:08:14,000
And I've done that before.

78
00:08:14,000 --> 00:08:20,000
And that is totally, wonderfully liberating.

79
00:08:20,000 --> 00:08:23,000
I don't have enough food to eat.

80
00:08:23,000 --> 00:08:26,000
Well, then today I won't eat.

81
00:08:26,000 --> 00:08:30,000
And so on and so on.

82
00:08:30,000 --> 00:08:37,000
Eventually, you realize that you can do without anything.

83
00:08:37,000 --> 00:08:50,000
And the worst that's going to happen is that you die, which of course could happen to you anyways.

84
00:08:50,000 --> 00:08:53,000
And death isn't really that big of a deal.

85
00:08:53,000 --> 00:09:01,000
We often get this idea that we should cherish this life that we have as a human being.

86
00:09:01,000 --> 00:09:06,000
And so we think we should work to protect it, but it's actually not so clear cut.

87
00:09:06,000 --> 00:09:14,000
Because if you're doing, if you're practicing or non-siation, and your mind is becoming clear,

88
00:09:14,000 --> 00:09:18,000
then when you pass away, you'll only go to a better state.

89
00:09:18,000 --> 00:09:25,000
That is more conducive for the practice of meditation.

90
00:09:25,000 --> 00:09:30,000
Maybe born as an angel, or you might be born in a society where you can quickly become

91
00:09:30,000 --> 00:09:37,000
a monk and continue on with the practice.

92
00:09:37,000 --> 00:09:43,000
But even putting aside death, so many other things that we shouldn't be afraid of.

93
00:09:43,000 --> 00:09:50,000
People are afraid that if they don't like it, then they'll wind up 40 years old without any life skills.

94
00:09:50,000 --> 00:09:58,000
They'll want to disrupt and not be able to make it in life.

95
00:09:58,000 --> 00:10:07,000
But if you talk to those people who have done that, we've gotten to the point where they couldn't be a monk anymore and ended up disrobing.

96
00:10:07,000 --> 00:10:09,000
They actually don't regret it at all.

97
00:10:09,000 --> 00:10:16,000
They regret the fact that they hadn't disrobed or regret the decision to disrove.

98
00:10:16,000 --> 00:10:21,000
But they don't find that themselves helpless at all.

99
00:10:21,000 --> 00:10:26,000
They realize how much suffering there is in the world is compared to being a monk,

100
00:10:26,000 --> 00:10:30,000
but they're very much equipped to deal with this suffering.

101
00:10:30,000 --> 00:10:34,000
They might not get high paying jobs, but they're not concerned about it.

102
00:10:34,000 --> 00:10:44,000
Some people in Thailand who used to be monks, and now they collect garbage recyclables and sell recyclables and make money like that.

103
00:10:44,000 --> 00:10:49,000
So make very little money, but are totally content and they live in shacks.

104
00:10:49,000 --> 00:10:53,000
And just total poverty.

105
00:10:53,000 --> 00:10:59,000
But as a monk, it was even more poverty and you were happier.

106
00:10:59,000 --> 00:11:04,000
So as a monk, you only end in the morning.

107
00:11:04,000 --> 00:11:10,000
So you're able to deal with it much better.

108
00:11:10,000 --> 00:11:12,000
This is because ordination is a training.

109
00:11:12,000 --> 00:11:14,000
You should see it as a training course.

110
00:11:14,000 --> 00:11:18,000
It's not a decision taking a leap into this or into that.

111
00:11:18,000 --> 00:11:22,000
It's applying to your apply for a PhD course.

112
00:11:22,000 --> 00:11:28,000
You don't think, oh, if I join that PhD program, maybe I'll regret it.

113
00:11:28,000 --> 00:11:33,000
And all you get from a PhD program is more knowledge, more skills.

114
00:11:33,000 --> 00:11:39,000
And that's really all you get through the ordination.

115
00:11:39,000 --> 00:11:42,000
It's a training.

116
00:11:42,000 --> 00:11:47,000
And eventually you become a trainer, you become a teacher.

117
00:11:47,000 --> 00:11:52,000
And really great thing about being a monk, especially in a Buddhist society,

118
00:11:52,000 --> 00:11:57,000
is that old monks are trained much better than young monks.

119
00:11:57,000 --> 00:12:03,000
So the older you get, you just have to get old and people start to respect you

120
00:12:03,000 --> 00:12:05,000
and think, oh, you must be a veteran.

121
00:12:05,000 --> 00:12:11,000
A monk and they take care of you and they start to think of you as they are monk and so on.

122
00:12:11,000 --> 00:12:17,000
And so the retirement plan's not so bad from what I've seen.

123
00:12:17,000 --> 00:12:21,000
Obviously the medical plan is great.

124
00:12:21,000 --> 00:12:28,000
Monk medicine is a pretty easy to obtain.

125
00:12:28,000 --> 00:12:34,000
This fear is actually very impermanent.

126
00:12:34,000 --> 00:12:41,000
I noticed that for myself, I had fear before I ordained.

127
00:12:41,000 --> 00:12:49,000
And now when I think of disrobing or being caused to disrobe,

128
00:12:49,000 --> 00:12:57,000
I have the same fear arising of being a lay person again.

129
00:12:57,000 --> 00:13:07,000
So you have that fear because it's something that you don't know.

130
00:13:07,000 --> 00:13:13,000
You're making a step into something unexpected.

131
00:13:13,000 --> 00:13:22,000
And when you get to know what you're doing when you did that step and ordained,

132
00:13:22,000 --> 00:13:25,000
then you will lose that fear, of course.

133
00:13:25,000 --> 00:13:27,000
It's only there before.

134
00:13:27,000 --> 00:13:34,000
So once you are ordained, it cannot be there anymore because you've done it.

135
00:13:34,000 --> 00:13:39,000
And then it might even change and turn around.

136
00:13:39,000 --> 00:13:52,000
So the point is not the fear to deal with the fear before ordination or before living samsara.

137
00:13:52,000 --> 00:14:03,000
It's the fear in general we have to deal with.

138
00:14:03,000 --> 00:14:16,000
Thank you.

