1
00:00:00,000 --> 00:00:29,940
Okay, good evening, everyone.

2
00:00:29,940 --> 00:00:36,940
Welcome to our evening dhamma.

3
00:00:36,940 --> 00:00:39,940
Today is question and answer.

4
00:00:39,940 --> 00:00:44,940
So we're going to be going to the questions on our website.

5
00:00:44,940 --> 00:00:50,940
You're welcome to go to the website at meditation.ceremungalow.org.

6
00:00:50,940 --> 00:00:56,940
And you have to sign up. Just give your email.

7
00:00:56,940 --> 00:01:05,940
It's purposefully making things difficult because it's easy to ask questions.

8
00:01:05,940 --> 00:01:07,940
So we want to ensure a little bit of quality.

9
00:01:07,940 --> 00:01:09,940
We make it a little challenging.

10
00:01:09,940 --> 00:01:13,940
You have to sign up.

11
00:01:13,940 --> 00:01:17,940
And it also ensures that you're actually a little bit interested in our community.

12
00:01:17,940 --> 00:01:22,940
So if you're not interested in signing up for our website well,

13
00:01:22,940 --> 00:01:33,940
this question and answer session is probably not for you anyway.

14
00:01:33,940 --> 00:01:36,940
Which is fine trying to push anything on anybody.

15
00:01:36,940 --> 00:01:48,940
It's just trying to focus our efforts on what's most beneficial to our community.

16
00:01:48,940 --> 00:01:57,940
So get right into it and answering questions here.

17
00:01:57,940 --> 00:02:02,940
If it works.

18
00:02:02,940 --> 00:02:10,940
Which it doesn't.

19
00:02:10,940 --> 00:02:14,940
I've got the questions up. I just can't do the whole clicking thing.

20
00:02:14,940 --> 00:02:18,940
I'll just answer questions. I don't know what's wrong here.

21
00:02:18,940 --> 00:02:23,940
Website's stalled again.

22
00:02:23,940 --> 00:02:31,940
A teacher once told me that a neutral feeling is based on ignorance.

23
00:02:31,940 --> 00:02:35,940
Otherwise, I would be able to determine a feeling as pleasant or painful.

24
00:02:35,940 --> 00:02:38,940
He based this on the julawedah way to the suta,

25
00:02:38,940 --> 00:02:41,940
where it says passion lurks and present feelings,

26
00:02:41,940 --> 00:02:46,940
and ignorance in neutral feelings.

27
00:02:46,940 --> 00:02:50,940
What I see in meditation now is that although neutral feelings can lead to ignorance,

28
00:02:50,940 --> 00:02:54,940
it is not necessarily based on ignorance. Could you clarify this for me please?

29
00:02:54,940 --> 00:02:56,940
What do I miss? You didn't miss anything.

30
00:02:56,940 --> 00:03:00,940
It sounds like this teacher. If that's an accurate representation of what they said

31
00:03:00,940 --> 00:03:05,940
is missing something.

32
00:03:05,940 --> 00:03:14,940
If you look at the Abidama, a neutral feeling is...

33
00:03:14,940 --> 00:03:19,940
I guess a neutral feeling you could say is most susceptible to delusion.

34
00:03:19,940 --> 00:03:22,940
Or that's not even really fair.

35
00:03:22,940 --> 00:03:26,940
But the point is it's not as common.

36
00:03:26,940 --> 00:03:28,940
You might say.

37
00:03:28,940 --> 00:03:34,940
It's not just speculation, but it seems reasonable to suggest

38
00:03:34,940 --> 00:03:41,940
that a neutral feeling is not as susceptible to a reaction that is pleasant,

39
00:03:41,940 --> 00:03:46,940
or that is desired, that is desiring it.

40
00:03:46,940 --> 00:03:53,940
So, to have a neutral feeling give rise to desire is less common.

41
00:03:53,940 --> 00:03:55,940
It still happens. It happens all the time.

42
00:03:55,940 --> 00:03:57,940
If you're very calm, you can like it,

43
00:03:57,940 --> 00:04:00,940
and you can get attached to the feeling of a calm feeling.

44
00:04:00,940 --> 00:04:06,940
There's lots of calming sensations that you end up liking.

45
00:04:06,940 --> 00:04:09,940
And in the Abidama, it's quite clear.

46
00:04:09,940 --> 00:04:12,940
A feeling associated with...

47
00:04:12,940 --> 00:04:18,940
Or a mind-state associate with greed can be either associated with a neutral feeling or a pleasant feeling,

48
00:04:18,940 --> 00:04:24,940
meaning when you like something, it's either neutral or it's happy.

49
00:04:24,940 --> 00:04:25,940
One or the other.

50
00:04:25,940 --> 00:04:35,940
So, he's clearly wrong, according to the Abidama, because it's quite clear about the association with a neutral feeling in greed.

51
00:04:35,940 --> 00:04:41,940
And on the other hand, a neutral feeling can also be associated with a wholesome state.

52
00:04:41,940 --> 00:04:48,940
Hulsome states are either associated with a neutral feeling or a pleasant feeling.

53
00:04:48,940 --> 00:04:54,940
And then there's the 4th Jana, which is also associated with a neutral feeling.

54
00:04:54,940 --> 00:05:02,940
There's no pleasant feeling in the 4th Jana, and yet there's clearly no...

55
00:05:02,940 --> 00:05:07,940
There's no delusion in the Jana state.

56
00:05:07,940 --> 00:05:11,940
So, that's that.

57
00:05:11,940 --> 00:05:20,940
But as I say, neutral feelings are probably more likely to be associated with pure delusion in the sense that you get views about them,

58
00:05:20,940 --> 00:05:26,940
and that you have a neutral feeling and it gives rise to view, it gives rise to doubt, it gives rise to worry or whatever,

59
00:05:26,940 --> 00:05:32,940
but it's less strongly associated with greed, I would say.

60
00:05:32,940 --> 00:05:34,940
And it's certainly not associated with anger.

61
00:05:34,940 --> 00:05:37,940
It's hard to get angry at a calm feeling, right?

62
00:05:37,940 --> 00:05:39,940
It's something that makes you calm.

63
00:05:39,940 --> 00:05:42,940
It's not likely to give rise to anger as a result.

64
00:05:42,940 --> 00:05:46,940
So, there's a connection there, which is what the way Dallas it is,

65
00:05:46,940 --> 00:05:48,940
the true the way Dallas it is saying.

66
00:05:48,940 --> 00:05:50,940
That's all it's saying.

67
00:05:50,940 --> 00:05:52,940
It's not saying that all neutral feelings are.

68
00:05:52,940 --> 00:05:53,940
That would be absurd.

69
00:05:53,940 --> 00:06:02,940
Really absurd because the Jana's especially are associated with calm feelings and meditation is associated with calm feelings.

70
00:06:02,940 --> 00:06:13,940
So, your intuition and your experience, your conclusion based on your experience, is correct.

71
00:06:13,940 --> 00:06:18,940
Again, I can't do anything about these questions answering them or anything.

72
00:06:18,940 --> 00:06:24,940
The whole website thing is frozen for me, that I can read them.

73
00:06:24,940 --> 00:06:27,940
Is it considered idle chatter to greet people?

74
00:06:27,940 --> 00:06:29,940
I ask how they are doing and such?

75
00:06:29,940 --> 00:06:34,940
No. No, it's not considered idle chatter.

76
00:06:34,940 --> 00:06:39,940
There's a lot of talk that is functional and purposeful.

77
00:06:39,940 --> 00:06:43,940
Asking someone out they are is functional.

78
00:06:43,940 --> 00:06:51,940
At the very least it's functional in the sense that it adheres to convention.

79
00:06:51,940 --> 00:06:56,940
If you look in the suit, it's a very important part of a lot of the dialogues.

80
00:06:56,940 --> 00:07:01,940
The Buddha would meet someone in exchange pleasantries.

81
00:07:01,940 --> 00:07:15,940
How it's often described or translated some more denying kataa, some more denier to pleasantries.

82
00:07:15,940 --> 00:07:22,940
More does is to joy, some more does to, it's basically a pleasant dream.

83
00:07:22,940 --> 00:07:28,940
It's a fairly literal translation.

84
00:07:28,940 --> 00:07:32,940
Some more denier that should be enjoyed.

85
00:07:32,940 --> 00:07:34,940
It should be pleasing.

86
00:07:34,940 --> 00:07:37,940
How are you?

87
00:07:37,940 --> 00:07:40,940
Were they actually spell it out?

88
00:07:40,940 --> 00:07:43,940
Are you holding up?

89
00:07:43,940 --> 00:07:46,940
Are you not being overwhelmed by pain?

90
00:07:46,940 --> 00:07:49,940
The response is, yes, I'm holding up.

91
00:07:49,940 --> 00:07:51,940
I'm not dying or anything.

92
00:07:51,940 --> 00:08:00,940
It's fairly to the point, not very flowery or even all that pleasant.

93
00:08:00,940 --> 00:08:14,940
When the monks, because they're very serious about what they do and they're trying very hard not to allow their minds to get caught up in attachment.

94
00:08:14,940 --> 00:08:23,940
But no, idle chatter is something that is directed to no purpose.

95
00:08:23,940 --> 00:08:34,940
So after, or apart from exchanging pleasantries, which have a functional purpose and also a purpose of creating friendship and that kind of thing.

96
00:08:34,940 --> 00:08:37,940
Expressing friendship, expressing friendliness.

97
00:08:37,940 --> 00:08:41,940
But then if you start to talk about what is the topic of your conversation?

98
00:08:41,940 --> 00:08:46,940
If the topic of the conversation is each other's health and well-being and so on, that's positive.

99
00:08:46,940 --> 00:08:55,940
But if it's, you know, the weather or if it's politics, that kind of thing.

100
00:08:55,940 --> 00:09:03,940
Generally considered idle chatter.

101
00:09:03,940 --> 00:09:12,940
My question is that when using auditory tracks like waterfall sounds, can these be used as an object for meditating?

102
00:09:12,940 --> 00:09:16,940
Yes, that's under dumbah, it's under the fourth one.

103
00:09:16,940 --> 00:09:22,940
So it doesn't spell that out in the booklet until the end, but if you read the last chapter it makes mention of it.

104
00:09:22,940 --> 00:09:26,940
And we usually don't worry too much about that in the beginning, but yeah, as you practice on.

105
00:09:26,940 --> 00:09:36,940
I mean, we try to discourage the use of auditory tracks when meditating because it's a partiality.

106
00:09:36,940 --> 00:09:47,940
You're not allowing for ordinary sounds to rise or to bring your focus.

107
00:09:47,940 --> 00:09:52,940
Usually because they're unpleasant, they're undesirable, and that's what we want to learn about.

108
00:09:52,940 --> 00:09:54,940
We want to learn about our reactions.

109
00:09:54,940 --> 00:10:07,940
If you're trying to control what sounds you hear by playing only pleasant sounds, you're never going to really understand how the mind reacts to negative things.

110
00:10:07,940 --> 00:10:13,940
Is that Jantong's method different from others in my seat tradition?

111
00:10:13,940 --> 00:10:18,940
You know, there's some technical differences, I would say.

112
00:10:18,940 --> 00:10:24,940
Some of them are, you know, the be argument, so this teacher will say this, that teacher will say that.

113
00:10:24,940 --> 00:10:38,940
I think it is, there are some fair size differences, I would say, in regards to, especially the touching, Jantong, or whoever it is,

114
00:10:38,940 --> 00:10:46,940
whether it was him or his teachers who set this up, sort of elaborated on that and turned it into a systematic meditation.

115
00:10:46,940 --> 00:10:58,940
The technical aspects of the practice are far less important in terms of determining the tradition than the actual principles behind it.

116
00:10:58,940 --> 00:11:02,940
The principles are very much the same.

117
00:11:02,940 --> 00:11:12,940
By each teacher is going to teach different technical technique.

118
00:11:12,940 --> 00:11:21,940
Is it possible to remember only pleasant memories? Is it possible to throw away all unpleasant memories until there are only pleasant memories remaining?

119
00:11:21,940 --> 00:11:29,940
A memory, can a memory be pleasant? Yes, a memory can be pleasant.

120
00:11:29,940 --> 00:11:36,940
Let's go directly. Is it possible to remember only pleasant memories?

121
00:11:36,940 --> 00:11:38,940
For how long?

122
00:11:38,940 --> 00:11:45,940
I mean, temporarily, yes, it's theoretically possible. Is it possible to say for the rest of eternity,

123
00:11:45,940 --> 00:11:49,940
I'm never going to remember anything unpleasant? No, that's not.

124
00:11:49,940 --> 00:11:57,940
I mean, to only remember pleasant memories? No, that's not possible.

125
00:11:57,940 --> 00:12:07,940
The only way for that to conceivably happen is if you do away with the cause for the arising of unpleasant memory.

126
00:12:07,940 --> 00:12:13,940
That would mean becoming enlightened, right? Because what is the cause of unpleasant memories?

127
00:12:13,940 --> 00:12:26,940
There is the cultivation of evil, you know, when you unwholesome this, when you do something bad, it gives rise to unpleasant memories.

128
00:12:26,940 --> 00:12:34,940
Or simply having partiality of certain memories, right? You don't like someone.

129
00:12:34,940 --> 00:12:40,940
But that's the same thing, really. You don't like someone and the anger is what gives rise to a memory that's unpleasant.

130
00:12:40,940 --> 00:12:45,940
You remember someone and it's unpleasant.

131
00:12:45,940 --> 00:12:55,940
But to go a little deeper, our intention is not to get rid of unpleasant memories, but that's not where our focus is in our minds, right?

132
00:12:55,940 --> 00:13:04,940
Suppose theoretically you can say that, yes, in the end, by not doing an evil that you get rid of unpleasant memories, but that's not consequential.

133
00:13:04,940 --> 00:13:16,940
What's consequential is the fact that evil leads to unpleasant memories that unwholesome this is a cause for suffering.

134
00:13:16,940 --> 00:13:34,940
So we focus on our judgment of things. And this is very much done by stopping trying to change our perceptions, change our memories, stop trying to control our minds.

135
00:13:34,940 --> 00:13:54,940
So if your focus is to try and control which memories you remember, you're actually increasing your partiality, increasing your aversion, increasing your judgmental nature of the mind, which is counterproductive.

136
00:13:54,940 --> 00:13:58,940
So in mindfulness, we try to be open to the spectrum of experience.

137
00:13:58,940 --> 00:14:05,940
If you were only to remember pleasant experiences, it would prevent you from understanding how your mind reacts to unpleasant experiences.

138
00:14:05,940 --> 00:14:21,940
We're actually kind of keen to experience whatever unpleasantness there is in our minds and in our memories in order to learn to overcome our reactions to them.

139
00:14:21,940 --> 00:14:32,940
If you're not, if there's no adversity, you're never tested, you're never challenged, and you can't grow.

140
00:14:32,940 --> 00:14:54,940
I'm not sure I completely agree with that, and it's a very common design, but it's on the right track, I think.

141
00:14:54,940 --> 00:15:04,940
I wouldn't say it's not possible. In fact, it's generally more difficult because if only good things come out, it's hard to be objective, right?

142
00:15:04,940 --> 00:15:17,940
And it's easy to get complacent, lazy, it's hard to be clear of mind because everything's so easy. You're not challenged.

143
00:15:17,940 --> 00:15:26,940
As the primary focus for Buddhist meditators to make the mind understand the true nature of impermanence, so the true nature of impermanence sounds a little bit lofty to me.

144
00:15:26,940 --> 00:15:33,940
We're not trying to understand the true nature of impermanence, we're trying to understand the true nature of reality.

145
00:15:33,940 --> 00:15:39,940
This, I mean reality is not something loftier. The true nature of this, right?

146
00:15:39,940 --> 00:15:44,940
When I say this and you hear me say this, what is the true nature of that?

147
00:15:44,940 --> 00:15:50,940
When I squeeze my hands like this and I feel this pressure, what is the true nature of that?

148
00:15:50,940 --> 00:15:57,940
And the true nature of all of that, all of this, is it's impermanence. It's unsatisfying, it's uncontrollable.

149
00:15:57,940 --> 00:16:04,940
None of it is going to be stable, satisfying, controllable, which is how we normally try to approach the world.

150
00:16:04,940 --> 00:16:09,940
I mean, in terms of trying to achieve stability, satisfaction, control.

151
00:16:09,940 --> 00:16:16,940
So meditation helps us see that that's not possible, not within samsara.

152
00:16:16,940 --> 00:16:32,940
Everything is changing. And so the things that we cling to, the things that we strive for, we sort of get, well, we gradually reduce our obsession with them because we start to see that they're impermanent.

153
00:16:32,940 --> 00:16:41,940
So that's really the focus of the meditation practice.

154
00:16:41,940 --> 00:16:46,940
In ten precepts smoking is not mentioned wrong. Is it true that monks can smoke?

155
00:16:46,940 --> 00:16:52,940
Yes, it's true that monks can smoke. I mean, there's a lot of effort to try and start monks from smoking.

156
00:16:52,940 --> 00:17:03,940
That seems silly that that monks should be smoking, but many monasteries allow monks to smoke.

157
00:17:03,940 --> 00:17:12,940
In Thailand, it's not so common in Sri Lanka. In fact, I don't know if it even happens in Sri Lanka.

158
00:17:12,940 --> 00:17:20,940
Is it against the monks' precepts? I mean, I would be highly surprised if smoking cigarettes

159
00:17:20,940 --> 00:17:28,940
wouldn't have been something that the Buddha would have disallowed, right? I mean, it seems quite glaringly, clearly something that should be disallowed for monks.

160
00:17:28,940 --> 00:17:35,940
But I don't think there's any specific precept against it. I don't think it is encompassed by the fifth precept.

161
00:17:35,940 --> 00:17:40,940
Though some monks disagree, they say it should be included in the fifth precept as being wrong.

162
00:17:40,940 --> 00:17:44,940
If you take a vow for the fifth precept, you shouldn't smoke cigarettes, basically.

163
00:17:44,940 --> 00:17:50,940
I think that might be going a little too far, but I understand the line of thinking.

164
00:17:50,940 --> 00:18:00,940
Certainly not intoxicating the way alcohol is. That's just absurd.

165
00:18:00,940 --> 00:18:09,940
Okay, afraid of brainwashing myself because of the change in the way I'm used to receiving the sentences.

166
00:18:09,940 --> 00:18:17,940
It's deeply rooted in me to be that judgmental and absorbed an object.

167
00:18:17,940 --> 00:18:23,940
I don't quite know what you're asking me for. Some suit that video talk about what exactly

168
00:18:23,940 --> 00:18:29,940
about being afraid of brainwashing yourself. It's fear you should know the phrase.

169
00:18:29,940 --> 00:18:33,940
Brainwashing is good. It means washing your mind, purifying your mind.

170
00:18:33,940 --> 00:18:40,940
If you truly brainwashed, not in the way we normally use that word, it's a very good thing.

171
00:18:40,940 --> 00:18:44,940
It's disturbing because it changes some of who you are.

172
00:18:44,940 --> 00:18:50,940
We are attached very much to our personality.

173
00:18:50,940 --> 00:18:56,940
It can be scary to think of letting that go.

174
00:18:56,940 --> 00:19:04,940
Let's understand the moment. It's not in the end, it's not actually a problem.

175
00:19:04,940 --> 00:19:09,940
Follow up question regarding cause and effect. I don't know what your first question was, but that's okay.

176
00:19:09,940 --> 00:19:13,940
If everything has its cause, this kind of implies determinism.

177
00:19:13,940 --> 00:19:16,940
How do you marry this idea with a referee will?

178
00:19:16,940 --> 00:19:20,940
There'll also be something to start the cycle of cause and effect what would that be.

179
00:19:20,940 --> 00:19:30,940
Buddhism doesn't say that everything has its cause. Buddhism talks about causality, which is clearly evident to their patterns.

180
00:19:30,940 --> 00:19:33,940
Something comes and then something else comes.

181
00:19:33,940 --> 00:19:39,940
We don't advance it into one of these theories of free will or determinism.

182
00:19:39,940 --> 00:19:45,940
I think it's pretty clear that the Buddha didn't advance such theories purposefully

183
00:19:45,940 --> 00:19:50,940
than being outside of reality.

184
00:19:50,940 --> 00:19:55,940
When this comes, it doesn't matter whether that's an ultimate truth.

185
00:19:55,940 --> 00:19:59,940
All it matters is that you see that because it's all psychological.

186
00:19:59,940 --> 00:20:01,940
How does your mind perceive things?

187
00:20:01,940 --> 00:20:05,940
If you observe and your mind perceives again and again that this happens,

188
00:20:05,940 --> 00:20:09,940
it doesn't matter whether that's true that it's always going to be A than B.

189
00:20:09,940 --> 00:20:13,940
All that matters is your mind saw that enough so that it let go.

190
00:20:13,940 --> 00:20:17,940
Everyone said, okay, well I won't do A because B sucks.

191
00:20:17,940 --> 00:20:27,940
Buddhism isn't that kind of philosophy where it claims free will or determinism or that sort of thing.

192
00:20:27,940 --> 00:20:37,940
I think there are clear and obvious problems to either one of those presuppositions or stances, views.

193
00:20:37,940 --> 00:20:41,940
Buddhism is, the Buddha said, I teach suffering, the cause of suffering.

194
00:20:41,940 --> 00:20:44,940
He was very clever about this.

195
00:20:44,940 --> 00:20:48,940
He was asked to sort of sing and he would say, look, all I teach is suffering.

196
00:20:48,940 --> 00:20:53,940
The cause of suffering, the cessation of suffering and the path, the cessation of suffering.

197
00:20:53,940 --> 00:20:56,940
That's important because that's what really happens.

198
00:20:56,940 --> 00:20:58,940
Who cares what views you have?

199
00:20:58,940 --> 00:21:00,940
What good are they for you?

200
00:21:00,940 --> 00:21:05,940
What's really important is do you see things as they are?

201
00:21:05,940 --> 00:21:11,940
No, what's really important is how is your mind?

202
00:21:11,940 --> 00:21:15,940
And the whole practice is in order to purify the mind.

203
00:21:15,940 --> 00:21:19,940
It's not in order to gain this or that view.

204
00:21:19,940 --> 00:21:25,940
Right view in Buddhism is seeing things as they are.

205
00:21:25,940 --> 00:21:31,940
That's the ultimate goal.

206
00:21:31,940 --> 00:21:35,940
Is it problematic to only breathe through the nose during formal practice?

207
00:21:35,940 --> 00:21:37,940
No, that's normally how I would think you would breathe.

208
00:21:37,940 --> 00:21:40,940
Breathing through the mouth is also okay.

209
00:21:40,940 --> 00:21:43,940
Some people do, but it's a little bit less comfortable.

210
00:21:43,940 --> 00:21:46,940
I would say your mouth dries up.

211
00:21:46,940 --> 00:21:51,940
So I would actually recommend breathing through the nose unless it's inconvenient.

212
00:21:51,940 --> 00:21:55,940
It seems when the hands are held together and both thumbs are pointing up.

213
00:21:55,940 --> 00:21:57,940
Formal practice goes by easier.

214
00:21:57,940 --> 00:22:00,940
Should I do hand over hand so there isn't as much comfort?

215
00:22:00,940 --> 00:22:03,940
You're getting into this idea of not making things so easy.

216
00:22:03,940 --> 00:22:06,940
You shouldn't be worried about things being easy.

217
00:22:06,940 --> 00:22:11,940
A bigger problem than things being easy is trying to make them easy.

218
00:22:11,940 --> 00:22:13,940
Things being easy isn't a problem.

219
00:22:13,940 --> 00:22:21,940
Trying to make things easy and worrying about finding tricks or making things easier is problematic.

220
00:22:21,940 --> 00:22:26,940
I guess I would go a little bit further and say challenging yourself can be good.

221
00:22:26,940 --> 00:22:32,940
So you don't want things, you don't want to be lazy, but that's more about doing more meditation.

222
00:22:32,940 --> 00:22:35,940
You know, it's nothing to do with the hands.

223
00:22:35,940 --> 00:22:40,940
In the long term you'll see that that doesn't actually make things easier.

224
00:22:40,940 --> 00:22:47,940
It's a temporary, temporary effect that it's going to make things easy.

225
00:22:47,940 --> 00:22:52,940
I currently can only do lying meditation due to medical condition.

226
00:22:52,940 --> 00:22:57,940
I was wondering are the benefits from using the lion's pose?

227
00:22:57,940 --> 00:23:00,940
The main benefit is that it keeps you from falling asleep.

228
00:23:00,940 --> 00:23:03,940
I would say a strong position.

229
00:23:03,940 --> 00:23:08,940
It's not easy to stay for a long time, but you're fairly strong.

230
00:23:08,940 --> 00:23:11,940
You're not easily going to fall asleep.

231
00:23:11,940 --> 00:23:16,940
There's also someone who's sitting on your right side.

232
00:23:16,940 --> 00:23:19,940
You're not lying on the heart.

233
00:23:19,940 --> 00:23:22,940
He was trying to explain to me the different postures.

234
00:23:22,940 --> 00:23:23,940
It's time-month.

235
00:23:23,940 --> 00:23:25,940
That was quite interesting.

236
00:23:25,940 --> 00:23:29,940
If you're lying on the heart, it somehow makes you too hot inside.

237
00:23:29,940 --> 00:23:31,940
I think that's what he was saying.

238
00:23:31,940 --> 00:23:36,940
I think he actually had tied it in with lust or something like that, but it's interesting

239
00:23:36,940 --> 00:23:38,940
that the Buddha would lie on the right side.

240
00:23:38,940 --> 00:23:43,940
It may have had something to do with the heart being on the left side.

241
00:23:43,940 --> 00:23:51,940
But I wouldn't deeply think into that much more important is the state of your mind.

242
00:23:51,940 --> 00:23:54,940
Noting a loud during formal walking meditation has helped me.

243
00:23:54,940 --> 00:23:55,940
There are downsides to this.

244
00:23:55,940 --> 00:24:01,940
I can continue to use this, at least intermittently for some more time in my walking meditation.

245
00:24:01,940 --> 00:24:03,940
Here's an example of what not to do.

246
00:24:03,940 --> 00:24:07,940
I would say because it helped you in one way, made it easier.

247
00:24:07,940 --> 00:24:08,940
There you go.

248
00:24:08,940 --> 00:24:10,940
You're trying to make things easier.

249
00:24:10,940 --> 00:24:11,940
Why?

250
00:24:11,940 --> 00:24:14,940
What's wrong that you're trying to make things easier?

251
00:24:14,940 --> 00:24:17,940
The answer to that question will be what you should be mindful of.

252
00:24:17,940 --> 00:24:20,940
It's the difficult things that we really need to be mindful.

253
00:24:20,940 --> 00:24:23,940
There were the greatest benefit comes from.

254
00:24:23,940 --> 00:24:27,940
You talk about it helping you to make it more challenging.

255
00:24:27,940 --> 00:24:33,940
Even if it did, I'd be skeptical because you're distracting yourself from the actual object

256
00:24:33,940 --> 00:24:38,940
by focusing on the expression of words, which is a whole other experience.

257
00:24:38,940 --> 00:24:42,940
The words in the mind are able to be with the object.

258
00:24:42,940 --> 00:24:49,940
The words at the mouth may keep your focus better, but only because you're juggling.

259
00:24:49,940 --> 00:24:55,940
You're having to speak and move at the same time.

260
00:24:55,940 --> 00:24:56,940
That's it.

261
00:24:56,940 --> 00:24:58,940
That's all the questions.

262
00:24:58,940 --> 00:24:59,940
Not so many.

263
00:24:59,940 --> 00:25:01,940
That's maybe a good sign.

264
00:25:01,940 --> 00:25:05,940
Maybe everyone's doing such meditation that they started to realize.

265
00:25:05,940 --> 00:25:10,940
There are no questions that my meditation won't answer for me.

266
00:25:10,940 --> 00:25:17,940
It's not always true, but I do appreciate the questions and some really good ones this week.

267
00:25:17,940 --> 00:25:20,940
My answers were okay not too short.

268
00:25:20,940 --> 00:25:23,940
I didn't take too long.

269
00:25:23,940 --> 00:25:26,940
I guess that's all for tonight then.

270
00:25:26,940 --> 00:25:34,940
Have a good night.

271
00:25:34,940 --> 00:25:36,940
Thank you.

