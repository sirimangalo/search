Hello and welcome back to our study of the Dhamupanda.
Today we continue on with verses number 102 and 103, which read as follows.
The first one is actually similar.
Almost the same as the ones we've looked at in the past couple of sessions.
Whoever should speak, 100 Gattasatanga, 100 Gattas, 100 verses that are connected with the path of connected with the useless path.
So that they do in the wrong, mislead you.
Our useless basically.
One word of Dhamma is greater.
Very much the same as the past two verses, but 103 is different.
A well-coated verse. Who should conquer thousands of thousands?
A thousand times a thousand.
Sajasang Sajasana, Sangame, Manusi, Jene, in war, a thousand men conquer.
Who should a thousand thousand, a million men conquer in war?
A thousand men in war.
The one victory which is the victory over oneself.
That over oneself is better.
The victory over oneself, Sajasana, Manusi, that is the highest conquering in battle.
The conquering of oneself.
So who should conquer a million people, a million enemies in battle?
It's still better to conquer oneself.
This is the best conquering in war.
So these two verses we are told were recited in regards to Kundalakacy.
So Kundalakacy is Kaysai's hair.
Kaysai is one who has hair.
Kundalai is the spiral.
And so the story goes that there was a girl of sixteen years old who was the daughter of a rich merchant in Rajagaha.
And at the time there seemed to be, there was apparently a practice whereby women were kept in seclusion or kept locked up.
Because there was a belief that I think even the commentary espouses that,
quote unquote, when women reach this age they burn and long for men.
So there was the idea that she would probably be rather promiscuous.
I mean, it's actually quite fair.
It's just that it doesn't apply equally to, it doesn't apply only to women.
The same thing happens to men at that age.
So to be fair, they should be locking all the men up.
But society at that time being what it was, they locked the women up.
Well, really to keep them safe, not just because they didn't trust them, but because to protect them.
And to ensure that they married someone who was suitable rather than just going with whoever.
So they kept her on the seventh floor of their house.
They had a kind of a palatial residence with seven floors.
Maybe it just had many floors, but on the top floor.
And one day there was a parade, which apparently was another custom,
where they would parade criminals through the city.
And so they had caught this rather vicious criminal.
And we're parading him through the town, taking him to the place of execution.
It's a way of kind of boasting that they had caught bragging about it.
And sort of promoting the system of government, yes, yes, look, we've caught this guy.
Look how great we are.
And then they would take them and cut their heads off.
So they were taking this guy through the streets and whipping him and people throwing rotten vegetables at him and all.
And then they passed by this house where this woman, this girl, is bored to tears up on the top floor of her house.
And she looks down and she sees this man being dragged through the streets.
And immediately she falls in love with him.
If we were of that sort of bent, we might say that she had met her soul mate.
This man was her soul mate.
Of course in Buddhism we don't have such beliefs.
And with a good reason as this story actually well illustrates.
But her, this man was the man for her. This was, she'd never seen someone, never had someone have this effect on her.
And we all, I think without proper instruction, tend to get this sort of idea where, wow, it must mean something.
I fall in love with this person.
It must mean that they are the one for me.
We have all these songs to back that sort of idea up.
We even apply it to food and so on.
We think that it means something that I prefer this food, I prefer salty food, I prefer spicy food.
And somehow therefore that's what I should eat.
When in fact that could be the very food that kills you in the end.
And the same goes with many different things.
So our partiality, our preference for or against something, is often a very poor indicator of the benefits of it or the benefits of the object of our desires.
Nonetheless, she became instantly obsessed with this man and so she told her servants to call her parents and said,
and they came and she said, I must have that man as my husband.
And they said, what are you talking about? How could we possibly, are you crazy?
What do you want with such a person like that? Don't talk nonsense.
And she said, if I don't get him as my husband, I'll stop eating.
I will die here. I won't be able to live.
It won't be possible for me to continue living if I don't get him as my husband.
Only if I can have him as my soul mate, will I be able to live and so she refused to eat.
And she just claimed that no matter what she was going to starve herself.
So they, well, first the mother, so first the mother came and she said this to the mother and then the mother couldn't convince her.
She said, well, we'll give you anyone. You can have, we'll find a suitable husband for you.
Someone who is worthy of you. Not this terrible vicious thief that she wouldn't have it.
So she called the mother called the father. The father came and tried to bully her into it and convince her into it.
And then sort of let go of this man. And she wouldn't have any of it.
Which just goes to show you how pernicious clinging can be.
And so finally they, at their wits end, they called to some king's officer and gave him a thousand gold coins.
And said, we must have that man. And so the man, this king's man, took the money.
And replace, switch this, this thief, this terrible villain with some innocent person and had that innocent person executed.
And then sent the thief off to the rich man and then sent word onto the king that the villain had been executed.
And so this woman was ecstatic. This 16 year old girl really.
And so she married this man and strove to really please him.
Like she said, I've rescued you so that you can be my husband and now I will serve you as your faithful wife.
And she tried her best to please him and do everything for him. She cooked for him. She cleaned for him.
As she did everything, she was like the model sort of, well, I guess you could say that a slave, wife, no servant of sorts.
Out of her dedication and devotion and her sincere belief that this guy was meant for her.
And there was something to that love. Because she had been always taught that sort of thing.
I think young women in traditional societies were often taught that, well, you know, someday my prince will come and all that.
So this is where she was headed. That was how she had been taught.
And it's funny how women are portrayed just as a little aside here.
In the Buddhist texts, women are often portrayed a little bit with a little bit of sexism.
You can't deny it. Like that quote that I gave you. It's typical of the commonaries to talk about how insatiable women are.
But it's not exactly sexism. It's because this is the kind of thing that you have to remind monks of.
Male monks because that would have been the main audience for these texts.
For the most part, they would be men who were, you know, of a bent to want to incline towards attraction towards women.
You know, thinking this sort of thing. And so you have this constant reminder, you know, constant, but this, every so often you'll come across a passage like this that just says women are, you can't trust them.
They'll cheat on you. They'll, they're insatiable and they'll always want to have more children and so on and so on.
Talking about how, if you read these out of context or if you just read them at face value, then they really sort of paint women in a bad light.
But as to women themselves that we're going to see in this story, women are given, I think, a fairly fair treatment in, in the Dhamma in the Buddhist teaching.
That especially in the, in the society where they really weren't given us, you can see much of a fair treatment. They're being locked up and all that and having to wait on their husbands and so on.
So anyway, we'll see how this develops, but at the time this is what she thought was the right thing to do.
And so the story starts to sound like this is the moral of the story, which it's not, but it sounds to this point that this is the thing she got her husband and now her duty for her life is to, to devote herself to him.
And so really you would think that the average thief who was just about to be executed would be overjoyed with this, but what was going through the thief's mind.
The only thing he could think of it says is when can I kill this woman, steal her jewels and go back to living a free life where I can eat in any tavern or restaurant that I want and where I can make money.
However, I want usually by killing and raping and murdering people and no killing and raping and pillaging and robbing people.
So indeed that's what he schemed to do.
So for a few days he was thinking about how he could take advantage of the situation until finally he came across the plan.
He figured he saw what was going to work.
And so he laid down in bed and wouldn't get up and he said husband what's wrong.
And he said, I feel really sick, I'm really bad.
And he said why?
Well, I see the thing is, and he starts to spin his yarn.
The thing is, the reason I'm free, I mean thank you for freeing me, but the reason why I'm free is I made a vow to the spirit on the top of one of the mountains.
That if I got free I would give some offering to the angel, to the deity.
And that's who sent me you. You were sent by this angel, that's why I caught your eye that day.
And that's why now you're my wife.
And he said, I just feel really bad because I made that promise and I haven't fulfilled it.
And he said, well then let's go fulfill it.
And he said, oh really? You're willing to do that? Oh yes.
And he said, what do we need? Well, he said, well we should take some rice porridge with honey and the five kinds of flowers.
Whatever five kinds those are. And she agreed.
And so they went up to the top of the place that's called Robber's Peak.
Apparently there's a place called Robber's Peak which is where they would, in the past maybe they would throw people over the cliff.
So it was a, you could go up one side and then there would be a cliff face and they would take Robber, take villains up and they would throw them over the cliff and they would fall down to their death.
So he took her up there.
And told her, he said, bring all your jewels, leave all your servants behind but bring all your finery all your valuables.
No, he didn't say it like that. He said, put on all your finest jewels and clothes and so on and come with me.
And we'll go together. And so they went up and they even told the servants not to follow them.
And when they got to the top of the cliff, he just stood there.
And she said, okay so what are we going to do? And he's, how are we going to do this offering?
And he said, I don't really, I don't care about any offer, it was really just a lie.
I'm here to steal your jewels and kill you. So, sorry.
And it was just like a defining moment in this woman's life.
Like, can you imagine having really your whole world?
Every this man was her world, not for very long but it had just something that she had been preparing herself to dedicate her life to.
And to suddenly have it pulled out from underneath her and more over her whole outlook on life to have it just shattered.
This was my soul name. This is a joke. And it wasn't a joke. And he said, no, I'm ready to.
Ready to kill you. There's nothing, no need to be if to cry and this is it.
I'll take your jewels now and then throw you over the edge. That's all that's left now.
And so she said, look, take my jewels. There's no need to kill me. I've been kind to you.
I promise I won't tell anyone and he said, I can't trust you if I let you go.
You'll turn me in and they'll come looking for me and they'll execute me for real this time.
If I'm going to do this, I have to kill you. So, sorry. But that's the breaks.
And so suddenly her mind cleared up with this. She was she's freed from her stupor and she just just mentally hit herself over the head.
She was shaking her head at herself. How, how, how gullible she had been. How, how misled she had been. How intoxicated she had been.
But her wisdom came back. And she said, she said to herself, this wisdom is not not just for cooking and cleaning.
And so she thought of a plan and she said to him, okay, husband, if that's your wish, then that's your wife.
All I ask is that you allow me to pay obedient to you to pay respect to you.
And bow down to you and he said, fine, fine, whatever you must last last request and then you're gone.
And so she held her hands up in supplication and went around him three times and then embraced him from the front and then embraced him from behind.
And when she got behind him, she grabbed him in the back of his head and the lower back and pushed him over the edge.
And he fell to his death.
And so this woman now fully awakened in the mundane sense to her folly.
Her whole life had been her whole outlook on life and been a sham and been a lie. The whole idea of true love, love at first sight, etc., etc. was just a lie.
And so, and furthermore, she had just killed her husband and that wasn't likely to go over well if she went back home.
Instead, she threw her through her valuables away and went and found some rags and decided to go forth and she became a wonder.
As a wonder, she wandered through India until she came to a group of ascetics, female ascetics and she asked to join them.
And I guess there were female ascetics at that time. Or maybe there were male ascetics.
And asked them if she could join them. And this is I think where they actually pulled her hair out because that was the thing in India.
They would put boards, put two boards on their shoulder and they had a big pit.
And they put you in the pit and put boards on your shoulders and they'd stand on those boards.
And then they'd take a special comb and they'd actually pull your hair out by the roots.
And that's how they would ordain you. And I think that's how she got the name Kundalakacy.
So her hair ended up being stunned by that or turned and it became earlier.
Maybe she just had curly hair. I don't know.
Anyway, she went forth and they taught her 1,000 views or beliefs, 1,000 articles of faith.
So these were 1,000 teachings in this other religion.
We don't know what those 1,000 things were, but these were the 1,000 things.
And she became so skilled in them that she was able to use them and able to really be funnel any of her any person she debated with anyone who thought to question her.
So she became quite an exceptional debater or religious leader, preacher.
And finally, and so finally they said to her, well, now you can be a real teacher of our religion.
And so they sent her out into the world, into India.
And they gave her a branch of a rose apple tree, which was the tree of India.
And they said, take this branch and put it down and use this branch to challenge other ascetics.
And convert them to our doctrine.
And they said, if you find someone, if someone is able, if you have a lay, if they're a lay person, is able to defeat you in debate,
then you have to disrobe and become their servant.
If a monk is able to defeat you in debate, then you have to become their student.
That's your task.
That's what you have to look for, find someone who is better than you.
And so she went through it in India and she was so skilled in debate that no one could defeat her on any one of the 1,000 points.
And so eventually she got a real reputation and all the other ascetics were afraid to challenge her.
Everyone was afraid to challenge her.
And so she eventually got a little bit.
Well, anyway, she went wandering far and wide and eventually she came to Sawati, which would present a little bit more of a challenge to her.
And so she went for arms in Sawati.
But before she went into the city, she took her rose apple branch and she would change it.
You know, it had leaves on it.
So it was a fresh branch and she would cut down a new one when the old one withered out.
But somehow a symbol.
And so she stuck it in a pile of sand or stuck it in the earth and would leave it there.
And she told this boy, she said, look, if anyone wants to challenge me, tell them that they should do a trample that.
Or people just knew maybe that they should trample this branch if they wanted a challenge her.
And so she went for arms and left this branch there and everyone was afraid to go near it.
And these boys sort of gathered around it to see who would dare to challenge the famous, what they called her, the ascetic of the rose apple.
For some reason she was carrying this branch around and it was her symbol by which people would know that she had come and she was ready to challenge her.
And she was ready to challenge anyone.
And that morning, who should happen along, but the well-known monk was in Sariputa.
On that morning, Sariputa came by and saw the rose apple branch.
And that's the boys, well, what does this mean? Where does this come from? And the boys told him the story that this was from the rose apple ascetic.
She had come to challenge other ascetics to a debate and whoever would like to debate her should trample this.
And he said to the boys, well, then trample it, pull it up and step on it, step all over it.
And they said, we're afraid to, venerable sir.
I said, oh, don't be afraid. I'll answer her questions. Just go ahead.
And then he went and he sat down under a tree nearby.
And sure enough, the boys trampled on this branch and they stood there jumping on it and stomping on it.
And the ascetic came out and she saw this and she scolded them.
He said, what's the meaning of this? Come on, I'm not going to debate with you little boys.
And they said, he told me to do it. They pointed to the elder.
Sariputa. And she turned and she said, is that true? And he said, yes.
And he said, well, then will you debate with me? And she said, sure.
And so she said, come by my residence and will debate.
And so she went, she got ready, I guess, went and prepared herself in meanwhile.
These boys or whoever was around spread word and all the people of the city spread the word and
said, Sariputa is going to debate with the ascetic of the rose apple tree.
And no, this would be something to see. And so everyone was excited.
And so they all came along followed after the rose apple ascetic to Sariputa's residence.
And they sat outside of his good day and everyone sat down and listened.
And the woman said, the ascetic said to Sariputa, I'd like to ask you a question.
And they said, well, then ask.
And she asked him all, he asked some questions about each of the 1,000 beliefs.
And he was able to refute each and every one of them.
That no one had ever been able to refute even one of until she was out.
And he said, he said, so he only has these few questions.
Do you have any more?
After she asked the 1,000, right?
And he said, and she said, no, that's all.
Realizing that she had now finally been vested.
And he said, OK, well, then I'd like to ask you a question.
Will you answer me? And she said, yes, for sure.
And he said, what is one?
Now, this may not have quite the significance.
I think it actually sounds more profound than it actually is.
But the truth of this question is it's the first question that you're supposed to ask of a novice monk,
meaning it's the first thing that a novice Buddhist learns.
And don't worry if you've never heard of these questions there.
They're not so much used as far as I've seen, although this was the tradition.
These are the 10 questions that a novice should learn.
So it's what is one, what are two, what are three, what are four,
what is four, what is five, six, seven, eight, nine, ten, and it's just one to ten.
So he asked, economic, what is one?
And she couldn't figure out, she couldn't answer even this one simple question.
It was too simple.
It was too, too ambiguous, really.
Like, what could one be?
So do you say it's that?
You can always be attacked no matter which way you answer that.
And she said, I can't answer it.
And he said, please tell me, please tell me the answer.
And he said, well, if you're a Dane, under me,
then, or if you're a Dane in our religion youth,
then we can give you the answer for short.
And they said, she said, immediately, well, then, ordain me.
And so he sent her to the bikunis, and the bikunis are a Dane.
And she became known as kun-de-ce.
This is the story of kun-de-ce.
Within a few days, or a few weeks,
there were very short times she became in our hunt.
And she had all sorts of magical powers and supernatural faculties,
and of course, free from suffering.
And she became, I think, one of the great disciples of the Buddha.
Doesn't mention it here, but she is one of the well-known.
She's well-known for her wisdom.
She's well-known for her quick thinking,
but mostly known for the story,
well, for having killed her husband.
Of course, not the best thing to be known for,
or to have been such a fool.
But for waking up, and for quickly adapting to the new information,
once she saw how foolish she had been,
she was quickly able to adapt herself.
And that's admirable.
I mean, doing evil deeds is often just because of wrong view,
because you've been taught the wrong things.
But some people, once they realize the truth,
and realize the error of their ways, they don't change.
They're unable to change. They're not strong enough.
And so it's admirable that she was able to change.
And so profoundly changed that she actually became in our hunt.
And so the monks were talking about this.
They're talking about two things that were pretty impressive.
First of all, the first impressive thing was the fact
that all it took was one question.
And so this relates to the first first.
So they were talking about how incredible it was
that with just such a short teaching,
she was able to realize the truth.
And then the second thing is that
I'm actually not quite so impressive,
but it seemed to be an impressive thing
and the monks were talking about
how shrewd she was, how clever she was
at being able to outwit her husband and defeat him.
He was ready to kill her.
She ended up killing him.
And it's amazing that she was able to think so quickly
and defeat him.
And so, of course, the Buddha came in
and asked them what they were talking about
when he found out what they were talking about.
When they found out what he was talking about,
they said he pointed out both in regards to both things.
First he said, well, it's not a matter of quantity.
It's a matter of quality.
And we, of course, dealt with this before.
But then he talks about the second verse,
which is a new theme.
And he said, well, you know, defeating her husband
wasn't really that impressive.
Defeating oneself, conquering oneself.
That's impressive.
And so this verse is actually quite important.
And it's something that we always remind ourselves of.
Whenever we want to get in fights or win arguments
or when we feel good about ourselves
because we're successful, because we're competitive
for it, because we are able to defeat others in army
in conflict or in debate or in war.
And so this is contrary to the Buddha's teaching
that this kind of defeating other people is actually quite
meaningless.
It doesn't have any real benefit.
It's conquering ourselves.
This is the greatest conquest.
The greatest conquest is the teaching.
And so that's the Dhammapada verse that we have,
verses 102 and 103.
How it relates to our practice,
talking just about the second verse,
it's really a good way to describe what we're talking about
when we talk about meditation, self-conquest,
working to better oneself,
working to overcome the enemies inside.
He talks about the villains.
So they're talking about, oh,
she conquered this villain.
And the Buddha comes in and says, he actually says,
well, it's the villains in the heart.
It's the evil ones in the heart.
These enemies that are inside of us.
This is the real conquest that is praiseworthy,
that you should praise.
And on the fact that she was able to become an Arhan
to give up all of her defilements.
And so there's a difference between what we esteem in the world.
Most people will esteem you by the accomplishments
you've made because that's all we can see.
That's what we normally, what we are accustomed to.
We don't realize the profound benefit
and importance of self-conquest,
of bettering oneself and curing oneself
of the things that cause us suffering,
the bad habits and addictions and aversions
just all the things that hurt us and hurt others,
which is what we do in meditation.
We change the mind.
We tame the mind so that the mind just sees things
objectively without reacting.
And the Buddha said, the untrained mind is your worst enemy.
And the trained mind is your best friend.
That's what this verse is referring to.
So quite simple, that powerful, meaningful,
and something to remember whenever you feel like
you should gloat or feel like you should conquer others,
feel like you should fight,
feel like it's somehow meaningful to win an argument.
And remember, now I've just actually lost a more important battle.
And that's the battle with myself
because now I've given rise to ego and attachment and so on.
So that's the dumb-apada for tonight.
Thank you all for tuning in.
