1
00:00:00,000 --> 00:00:12,000
Ajentong wakes up, I don't know how long he sleeps, I'm told he sleeps three, four hours a day.

2
00:00:12,000 --> 00:00:17,440
He's up at around three, I suppose, and what he's doing, I'm not sure because you're

3
00:00:17,440 --> 00:00:21,360
not allowed to get close to him at that time, but there was a time where I would meet

4
00:00:21,360 --> 00:00:28,240
him at his door, so I would get up at 2 a.m. and go and meditate outside of his door,

5
00:00:28,240 --> 00:00:33,240
and at 4.30 he would come out, and I would walk with him to chanting.

6
00:00:33,240 --> 00:00:38,360
Was it 4.30 in the morning he would go to chanting?

7
00:00:38,360 --> 00:00:41,960
And then he'd give his talk and we'd do a group meditation, just ten minutes, walk in ten

8
00:00:41,960 --> 00:00:44,960
minutes, sitting.

9
00:00:44,960 --> 00:00:49,200
But everything he did was measured.

10
00:00:49,200 --> 00:00:54,640
There'd be times where he'd be sitting there, and there'd suddenly be a whirlwind of activity

11
00:00:54,640 --> 00:00:55,640
around him.

12
00:00:55,640 --> 00:01:01,640
The driver would come and say, okay Ajentong, time to go, walk in, walk out, Ajentong would

13
00:01:01,640 --> 00:01:04,440
sit there.

14
00:01:04,440 --> 00:01:09,200
Secretary would come in, okay Ajentong, time to go, getting ready, and he's so clear

15
00:01:09,200 --> 00:01:12,400
he knows exactly what's going on, and he doesn't have to listen to them because when

16
00:01:12,400 --> 00:01:15,200
the time comes he'll get up and he'll go.

17
00:01:15,200 --> 00:01:21,240
And then at the exact right time he looks up at the clock, looks down, says to himself,

18
00:01:21,240 --> 00:01:27,400
I'm assuming, says, intending to stand, intending to stand, intending, noting his intention

19
00:01:27,400 --> 00:01:32,600
and then standing up, standing, standing with perfect measured calm.

20
00:01:32,600 --> 00:01:39,320
I've never seen him off guard where he suddenly does something without being aware, clearly

21
00:01:39,320 --> 00:01:41,120
aware of what's going on.

22
00:01:41,120 --> 00:01:42,120
You can see it.

23
00:01:42,120 --> 00:01:44,560
There's no, there's no mistaking it.

24
00:01:44,560 --> 00:01:50,240
It's something that is clearly visible in everything he does, in how he eats, and how

25
00:01:50,240 --> 00:01:55,040
he walks, in how he talks.

26
00:01:55,040 --> 00:02:03,360
I've come to him asking a question about something fairly trivial, not a point or something

27
00:02:03,360 --> 00:02:09,920
where I just wanted an answer, and he'll give me like a thousand times what I was looking

28
00:02:09,920 --> 00:02:10,920
for.

29
00:02:10,920 --> 00:02:16,000
He'll hit me to the heart with something because he knows it's good for me, because

30
00:02:16,000 --> 00:02:18,000
he knows what I need to hear.

31
00:02:18,000 --> 00:02:23,040
He wouldn't just tell someone out of a desire for them to have knowledge.

32
00:02:23,040 --> 00:02:27,440
He will always say it in a way that helps the person, because he sees clearly what is

33
00:02:27,440 --> 00:02:30,560
beneficial to the person.

34
00:02:30,560 --> 00:02:35,000
I once wrote down in my book, I would write everything he said down in a book, or everything

35
00:02:35,000 --> 00:02:43,560
I notice, and I once wrote down, everyone who comes here gets something good.

36
00:02:43,560 --> 00:02:47,560
And that may not mean so much, but if you saw the kind of people who came to see him from

37
00:02:47,560 --> 00:02:58,760
politicians to crazy old monks and nuns, village people who had no interest in meditation,

38
00:02:58,760 --> 00:03:08,040
who wanted like crazy things, monks coming to him from all over the country, and big monks

39
00:03:08,040 --> 00:03:17,400
powerful monks, everyone who came to him left with a smile or left with, if they weren't

40
00:03:17,400 --> 00:03:24,000
smiling, they had been hit really hard, and they were still reeling in surprise, really,

41
00:03:24,000 --> 00:03:27,280
because you go there expecting, yeah, we'll just go and talk to him, and we'll get this

42
00:03:27,280 --> 00:03:36,520
or that, and it's a shock for many people to get what you get from him.

43
00:03:36,520 --> 00:03:53,640
He doesn't teach theory, he's got a surprising lack of theoretical doctrine that he teaches.

44
00:03:53,640 --> 00:03:59,560
Hardly ever hear him go on and, you know, detail the abbedam, I've rarely ever heard

45
00:03:59,560 --> 00:04:03,680
him talk about the abbedam yet he tells people to learn it.

46
00:04:03,680 --> 00:04:10,480
His way of teaching is, for that reason it often, people often miss what he's talking

47
00:04:10,480 --> 00:04:24,960
about, people who are deep in study and so on will often, in the beginning, they feel

48
00:04:24,960 --> 00:04:29,640
like they haven't gotten what they came for, because they're coming looking for a technical

49
00:04:29,640 --> 00:04:34,720
answer to their problem, a theoretical answer, and they don't get that.

50
00:04:34,720 --> 00:04:38,400
So he'll start talking about, not exactly, but, you know, some of the things he talks

51
00:04:38,400 --> 00:04:44,200
about, he harps on about, sorry, harps is not a good one, he goes on about food, he says

52
00:04:44,200 --> 00:04:48,240
how you have to know moderation and food, and for the longest time I thought this was

53
00:04:48,240 --> 00:04:55,320
really kind of, well mundane, why aren't there more deep and profound things to talk

54
00:04:55,320 --> 00:05:01,960
about, but he always says how important moderation in eating is.

55
00:05:01,960 --> 00:05:06,560
For example, of course I found out later, and I thought about it later, and I realized

56
00:05:06,560 --> 00:05:11,800
that actually it was probably partially because a lot of the monks in the monastery weren't

57
00:05:11,800 --> 00:05:18,400
keeping the rules on eating, and there was a lot of eating in the evening and cooking

58
00:05:18,400 --> 00:05:26,600
in the evening, and a lot of fat monks, some fat monks, and fat novices and so on, it's

59
00:05:26,600 --> 00:05:34,960
hard to imagine how you can get fat on this diet, but they have a different diet, but

60
00:05:34,960 --> 00:05:39,880
it would be very simple, and not only the eating, but just about everything, and often

61
00:05:39,880 --> 00:05:45,880
he would just, the teachings he'll give are just, you know, there's other things that

62
00:05:45,880 --> 00:05:51,560
you could learn from the very basics of Buddhism, you know, they have these basic texts,

63
00:05:51,560 --> 00:05:57,040
and he'll go on about them, he'll talk about the very beginning teachings, so say at

64
00:05:57,040 --> 00:06:02,680
the very beginning of the Vinnya, I even have some of these things he said memorized,

65
00:06:02,680 --> 00:06:08,600
because to me they're just great to hear, klinton, patwini, it says at the very beginning

66
00:06:08,600 --> 00:06:15,800
of the Vinnya, and in this he means the Vinnya as taught in Thailand, they have this textbook.

67
00:06:15,800 --> 00:06:25,920
Sing tea kwan success, the things that we should study, there's only three, but you see

68
00:06:25,920 --> 00:06:33,040
how profound it is to reiterate this, because here we are talking about things up in the

69
00:06:33,040 --> 00:06:46,360
sky, and he brings us down to earth, singing tea kwan success, misaam, sihan sihar, sihan

70
00:06:46,360 --> 00:06:55,360
a yung sihan, the morality, training in morality, samadhi sihar sihar sihar sihar

71
00:06:55,360 --> 00:07:04,280
a yung saimadhi, training in moral in concentration, or focus, and bhanya sihar, training

72
00:07:04,280 --> 00:07:06,960
in wisdom.

73
00:07:06,960 --> 00:07:10,720
And then he talks about, I can't remember in that talk or in other talks, normally he'll

74
00:07:10,720 --> 00:07:18,280
talk about what is morality, well morality is when you bring the mind back, keeping the

75
00:07:18,280 --> 00:07:24,040
mind from running away, keeping the mind from chasing after our desires, this is morality,

76
00:07:24,040 --> 00:07:29,600
this is when you let your mind go, this is where immorality comes from, so bringing it

77
00:07:29,600 --> 00:07:35,760
back, what does that lead to, it leads to concentration, what is concentration, concentration

78
00:07:35,760 --> 00:07:41,840
is keeping your mind on the object, focusing on the object, what is wisdom, wisdom is

79
00:07:41,840 --> 00:07:46,440
knowing the object, in fact the funny thing that teachers will always say is that wisdom

80
00:07:46,440 --> 00:07:53,360
is actually just, when you say rising and falling to yourself, that's actually wisdom,

81
00:07:53,360 --> 00:07:54,360
isn't it?

82
00:07:54,360 --> 00:07:57,400
Because it seems so stupid, it's like, well I know it's rising, right?

83
00:07:57,400 --> 00:08:02,040
The problem is you really don't, maybe for a second you experience the rising, but then

84
00:08:02,040 --> 00:08:09,040
you know, hey my stomach is rising, isn't that great or oh it's, it's all stuck, it's

85
00:08:09,040 --> 00:08:17,080
not deep enough, it's not smooth enough, and so on, and so on and so on, we don't really

86
00:08:17,080 --> 00:08:23,200
know it, when you know it, that's, in a sense banya, banya means to know thoroughly, when

87
00:08:23,200 --> 00:08:28,200
you know thoroughly that this is rising, that's wisdom, because that's dwelling in the present

88
00:08:28,200 --> 00:08:34,720
moment, it's being with reality, and our hand is an enlightened being is like that all

89
00:08:34,720 --> 00:08:43,240
the time, they're aware of the reality at all times, so Adjantong is a sort of no nonsense

90
00:08:43,240 --> 00:08:49,560
teacher, in fact I would say the theory that you get from him is, is, is not, there's

91
00:08:49,560 --> 00:08:58,840
not much, but it's incredibly clear, I, I had teachers before Adjantong, and I was able

92
00:08:58,840 --> 00:09:02,560
to grasp what they were teaching, but I couldn't make head or tail of it, I couldn't

93
00:09:02,560 --> 00:09:07,680
tell what was the first step, what was the second step, it seemed all jumbled and kind

94
00:09:07,680 --> 00:09:12,160
of ad hoc, you know, for this person I'm going to say this thing based on my experiences

95
00:09:12,160 --> 00:09:18,280
and so on, Adjantong, when I started practicing with him, everything was crystal clear,

96
00:09:18,280 --> 00:09:23,920
to the extent that I knew exactly how to teach people, and I knew what steps to lead people

97
00:09:23,920 --> 00:09:29,080
through from one day to the next, his teaching made perfect sense, and went in perfect

98
00:09:29,080 --> 00:09:36,880
order, and really everything that he taught was like that, or is like that, I'm assuming

99
00:09:36,880 --> 00:09:41,320
he's still teaching, I haven't been in touch for a while, you know, that's the thing

100
00:09:41,320 --> 00:09:48,120
about being a penniless monkey, you can't just fly and then go see your teacher, so now

101
00:09:48,120 --> 00:09:53,000
here I am in another country, someday hopefully I'll get back there, I should probably

102
00:09:53,000 --> 00:09:59,800
keep in touch, no, anyway, I assume that he's still there teaching, and his teaching is

103
00:09:59,800 --> 00:10:06,800
just, everything is, is the same, he will say the same thing a thousand times, without

104
00:10:06,800 --> 00:10:12,480
ever getting bored or ever having to alter it, he does alter it, and he does change,

105
00:10:12,480 --> 00:10:22,400
but he's consistent, he's incredibly consistent, and sometimes to the point where it's like,

106
00:10:22,400 --> 00:10:26,880
he's not trying to sell you himself, he's telling you the truth, and he'll say it

107
00:10:26,880 --> 00:10:30,400
to you a thousand times, he'll tell you it again and again and again, because there's

108
00:10:30,400 --> 00:10:34,880
nothing else, you're not going to get him to change his way of teaching, he tells you

109
00:10:34,880 --> 00:10:44,120
the truth, and he gives people very simple teachings, and they won't get the theory out of

110
00:10:44,120 --> 00:10:49,400
it, you know, they won't get an incredible amount of theory, and you won't hear him reciting

111
00:10:49,400 --> 00:10:54,120
long discourses or talking about the abbey dumber or so on, but he'll give you what you

112
00:10:54,120 --> 00:10:59,280
need at that moment, and that's a thousand percent clear, anyone can vouch for that, when

113
00:10:59,280 --> 00:11:04,440
they go to him they get what they need, and it takes them the next step, it's a lot

114
00:11:04,440 --> 00:11:11,320
like the practice, you know, when you practice meditation, if you haven't practiced intensively

115
00:11:11,320 --> 00:11:16,560
you might not have felt this yet, but when you enter into the meditation practice you have

116
00:11:16,560 --> 00:11:21,720
a lot of expectations of what you're going to get out of it, and those expectations are

117
00:11:21,720 --> 00:11:26,240
for the most part blown out of the water, and the greatest part of meditation is realizing

118
00:11:26,240 --> 00:11:35,480
that your purpose for meditating is wrong, and actually changing that, altering your path,

119
00:11:35,480 --> 00:11:41,480
it's a very profound part of the practice, it's not just learning more, it's learning

120
00:11:41,480 --> 00:11:50,280
that, learning what to look for, learning what to learn, and he does that, he changes

121
00:11:50,280 --> 00:11:55,680
people, he doesn't just give people the answers, he changes their questions, he changes

122
00:11:55,680 --> 00:12:01,600
what they're looking for, he's got to be the best teacher by far that I've ever met,

123
00:12:01,600 --> 00:12:05,000
and maybe there are, maybe I haven't met, and I just haven't learned from them, but

124
00:12:05,000 --> 00:12:10,480
I've been pretty, I think I've been fairly observant, and I've watched a lot of different

125
00:12:10,480 --> 00:12:15,720
monks, and a lot of different teachers, and I've never met anyone that I could say from

126
00:12:15,720 --> 00:12:23,560
my knowledge, from what I know of them compares to him, just in terms of his practice,

127
00:12:23,560 --> 00:12:30,440
I've only seen him get agitated once, and to me it was like a perfect sign that he never

128
00:12:30,440 --> 00:12:36,520
gets agitated, because his way of getting agitated was like to say something, and then

129
00:12:36,520 --> 00:12:42,760
go back to a total piece and call, what it was is I've given these CDs of one of his

130
00:12:42,760 --> 00:12:49,120
teachers, this really neat set of MP3 teachings, it was just like a ton of teachings

131
00:12:49,120 --> 00:12:58,240
from his, one of his teachers in Bangkok, who is very intellectual, and so it has a lot

132
00:12:58,240 --> 00:13:03,800
of long lengthy teachings, but they're really good, immediately he gets up and takes

133
00:13:03,800 --> 00:13:07,880
me into his room and he wants to listen to them, and they're like no, no, he's got no way

134
00:13:07,880 --> 00:13:10,960
to listen to them, and I hooked him up and he was able to listen, and they were very angry

135
00:13:10,960 --> 00:13:16,280
at me, because they didn't want him to listen to the people who take care of him, who

136
00:13:16,280 --> 00:13:22,000
I don't have much good to say about, didn't want him to listen at that time, and he wanted

137
00:13:22,000 --> 00:13:26,000
to listen, so there I was listening, I got in real trouble for this, I was yelled at

138
00:13:26,000 --> 00:13:32,440
and scolded, not knowing the right time and so on, but Adjantan got to listen to the

139
00:13:32,440 --> 00:13:37,800
dhamma, so I was happy, and so I sat there and they came in and said okay, Adjantan

140
00:13:37,800 --> 00:13:46,240
time to go, and this was a secretary and Adjantan said five minutes, give me five minutes,

141
00:13:46,240 --> 00:13:51,560
and then he went out, stormed out, boom, boom, boom, and then the driver comes in, okay,

142
00:13:51,560 --> 00:13:57,560
Adjantan time to go, Adjantan goes one minute, can't I just have one minute, and then

143
00:13:57,560 --> 00:14:03,840
he said, these guys, they don't have a clue, and then he went back to listening, and

144
00:14:03,840 --> 00:14:07,800
we sat there and we listened for a couple of minutes, boom, boom, boom, storming in and

145
00:14:07,800 --> 00:14:12,720
out, these guys who were totally furious, and they said okay, thank you very much, and

146
00:14:12,720 --> 00:14:19,840
he got up, and he went on, and so I tell that not to give you a sign that he, because

147
00:14:19,840 --> 00:14:25,440
I don't, I didn't, you know, it increased my respect for him to see him do that, he wasn't

148
00:14:25,440 --> 00:14:30,320
a pushover, but he certainly wasn't interested in these guys, he was just like, I'm

149
00:14:30,320 --> 00:14:38,800
listening, what a minute, it was really his whole body shook, it was really funny, yeah,

150
00:14:38,800 --> 00:14:46,200
I really love him, he's a wonderful person, I think of him as my father in the dhamma,

151
00:14:46,200 --> 00:15:15,280
or grandfather, maybe he's old enough, okay, that's an answer, hopefully, it's 15 minutes.

