1
00:00:00,000 --> 00:00:17,720
Namato. Namata, yes, sir. So welcome, everyone, to today's talk on insight meditation.

2
00:00:17,720 --> 00:00:31,080
And I thought that it would be good to sort of frame these talks as a sort of a meditation session.

3
00:00:31,080 --> 00:00:54,520
So I'd like to invite everyone, if you would, to set your animator to busy, if you're afraid, many of us might get.

4
00:00:54,520 --> 00:01:03,040
I am instant messages during this time, so we'll just set our avatar as to busy, if you like.

5
00:01:03,040 --> 00:01:14,280
And that way we can focus better on what's the task at hand.

6
00:01:14,280 --> 00:01:24,800
If you feel comfortable, you can try to sit on the floor, you can sit comfortably in a chair.

7
00:01:24,800 --> 00:01:33,200
And you can just close your eyes and listen to the talk, or keep your eyes open if it's more comfortable.

8
00:01:33,200 --> 00:01:40,040
But we're going to try to use this time as a sort of a guided meditation.

9
00:01:40,040 --> 00:01:46,560
It's just really how we should look at a Dharma talk anyway.

10
00:01:46,560 --> 00:01:57,040
We can see in the time of the Buddha, when he taught meditation, when he taught Buddhism,

11
00:01:57,040 --> 00:02:05,600
it was always with an emphasis on the practical, and very often after the Lord Buddha finished talking.

12
00:02:05,600 --> 00:02:08,440
By the time he finished his talk.

13
00:02:08,440 --> 00:02:15,800
And people who listen would have gained some realization through their practice during the time they were listening.

14
00:02:15,800 --> 00:02:28,280
So this is sort of the kind of thing that we hope for on maybe a smaller scale during the time of listening to talk on the Buddha's teaching,

15
00:02:28,280 --> 00:02:32,560
especially one about meditation.

16
00:02:32,560 --> 00:02:38,800
So I thought I'd take the opportunity today to sort of go over some meditation basics,

17
00:02:38,800 --> 00:02:45,520
and kind of take it as a guided meditation, sort of meditating as we go.

18
00:02:45,520 --> 00:02:51,840
So sit comfortably, try to get into a meditation position, and I'll just start explaining some things.

19
00:02:51,840 --> 00:02:57,480
For those of you who already have your own meditation practice, you're welcome to just meditate.

20
00:02:57,480 --> 00:03:09,960
Take up that practice. You can disregard whatever instructions seem contrary to the method that you follow.

21
00:03:09,960 --> 00:03:15,520
So meditation, the word meditation can mean different things to different people.

22
00:03:15,520 --> 00:03:27,440
For some people it means simply an escape, a retreat, a vacation from the troubles and difficulties that we encounter in our lives.

23
00:03:27,440 --> 00:03:41,440
But the way I like to look at meditation, and if you really look at the word meditation, it means to consider, to reflect, to examine,

24
00:03:41,440 --> 00:03:49,440
and to find a solution to the problems that exist in the mind.

25
00:03:49,440 --> 00:03:56,440
So it's not like running away from your problems, it's like facing them and learning more about them and coming to understand

26
00:03:56,440 --> 00:04:05,440
how the problem works, how the problem situation works, to create suffering for you.

27
00:04:05,440 --> 00:04:15,440
So this is on a very phenomenological level, so in the present moment, even right now, there are many things that have the potential to bring us suffering.

28
00:04:15,440 --> 00:04:33,440
Maybe sounds around you, maybe the feeling of the hardness of the chair or the stiffness in your back, or even pain in the head or joints, pain in the stomach, pain in different parts of the body.

29
00:04:33,440 --> 00:04:43,440
Memories that are coming back again and again, they can be fears or worries or stresses.

30
00:04:43,440 --> 00:04:51,440
It can be both physical and mental, things which bring us suffering.

31
00:04:51,440 --> 00:04:59,440
And the reason these things can bring us suffering is because we don't really understand them, we don't really see them for what they are.

32
00:04:59,440 --> 00:05:06,440
We mistake them for me, for mine, or something that I have to react to.

33
00:05:06,440 --> 00:05:19,440
And as a result, we do react, trying to change, trying to alter, trying to modify things to be the way we want them to.

34
00:05:19,440 --> 00:05:35,440
Meditation seeks to remedy that, to allow us to see things simply for what they are and not judge or stress out about, obsess over the things that we experience.

35
00:05:35,440 --> 00:05:48,440
And in classical Buddhism, the objects of meditation for which we're going to examine reality are called the four foundations of mindfulness.

36
00:05:48,440 --> 00:05:58,440
These are four parts of reality or four classifications of reality, as it pertains to meditation practice.

37
00:05:58,440 --> 00:06:07,440
So the first one is the body, and this is always considered to be the base meditation object.

38
00:06:07,440 --> 00:06:11,440
We always return to the body because it's always prominent.

39
00:06:11,440 --> 00:06:18,440
It's almost always easy to examine, easy to focus on.

40
00:06:18,440 --> 00:06:24,440
And as I've said before, as a result of looking at the body, we're going to understand the mind better.

41
00:06:24,440 --> 00:06:28,440
So here we're going to start focusing on the body in the tradition that I follow.

42
00:06:28,440 --> 00:06:35,440
We focus on the stomach because when we're sitting still, it's for the only thing that moves.

43
00:06:35,440 --> 00:06:37,440
For beginners, it might mean more in the chest.

44
00:06:37,440 --> 00:06:40,440
The stomach might not actually seem to move that much.

45
00:06:40,440 --> 00:06:49,440
If you want to become more comfortable with it, you can place your hand on the stomach until you can get a feel for its movement.

46
00:06:49,440 --> 00:06:58,440
Just place your hand there, and you'll feel the stomach rising in the body.

47
00:06:58,440 --> 00:07:04,440
Now the technique that we're going to use to remedy things is we're trying to straighten our minds out.

48
00:07:04,440 --> 00:07:14,440
The Lord Buddha's words is that a wise person straightens their mind just as a Fletcher straightens the arrow shaft,

49
00:07:14,440 --> 00:07:30,440
someone who plumbers on direct water or as a carpenter straightens wood, or so on.

50
00:07:30,440 --> 00:07:33,440
The wise straighten their mind.

51
00:07:33,440 --> 00:07:38,440
So the idea is that our minds are crooked in regards to these things are bent out of shame.

52
00:07:38,440 --> 00:07:48,440
And all we're going to do is straighten our minds out. Instead of seeing it as rising, we see it as good or bad as comfortable or uncomfortable as me as mine and so on.

53
00:07:48,440 --> 00:07:53,440
Trying to make it more comfortable, trying to make it more deep, more shallow.

54
00:07:53,440 --> 00:07:57,440
Trying to force it, trying to control it.

55
00:07:57,440 --> 00:08:01,440
So the tool we're going to use to straighten our minds is the mantra.

56
00:08:01,440 --> 00:08:06,440
And mantra is often understood to be something for focusing the mind on a concept.

57
00:08:06,440 --> 00:08:11,440
But here we're going to use it to focus the mind on reality.

58
00:08:11,440 --> 00:08:16,440
So when the stomach rises, we're going to use the mantra to remind ourselves of the reality of the rising,

59
00:08:16,440 --> 00:08:20,440
keeping our minds simply fixed on the reality of it.

60
00:08:20,440 --> 00:08:23,440
So our minds don't wander into evolution.

61
00:08:23,440 --> 00:08:27,440
When it rises, we say to ourselves rising.

62
00:08:27,440 --> 00:08:34,440
When it falls, we say to ourselves falling, simply reminding ourselves of what's going on.

63
00:08:34,440 --> 00:08:38,440
Not letting our minds make more of things than they actually are.

64
00:09:04,440 --> 00:09:33,440
And what we notice when we do this,

65
00:09:33,440 --> 00:09:37,440
then you can just keep doing it, keep acknowledging it as I speak.

66
00:09:37,440 --> 00:09:41,440
Or you can focus on the words being spoken.

67
00:09:41,440 --> 00:09:43,440
You can acknowledge them as well.

68
00:09:43,440 --> 00:09:49,440
You can acknowledge the sound is hearing, hearing, and so on.

69
00:09:49,440 --> 00:09:56,440
What we're going to notice is that we can't focus on the rising and falling not easily.

70
00:09:56,440 --> 00:10:01,440
Because there are many other things which take our attention away.

71
00:10:01,440 --> 00:10:14,440
And the first thought is to try to do away with these distractions from the breathing.

72
00:10:14,440 --> 00:10:18,440
And the force our minds to stay with the one object.

73
00:10:18,440 --> 00:10:21,440
But in insight meditation, we're not trying to do this.

74
00:10:21,440 --> 00:10:25,440
We're trying to understand reality and watch reality and not judge it.

75
00:10:25,440 --> 00:10:28,440
Not judge one thing is better than another.

76
00:10:28,440 --> 00:10:34,440
So we're going to take objects in sequence, whatever comes up, we're going to look at that.

77
00:10:34,440 --> 00:10:38,440
And so we have three other foundations of mindfulness, not just the body.

78
00:10:38,440 --> 00:10:43,440
When we're focusing on the rising and falling, this is one part.

79
00:10:43,440 --> 00:10:50,440
When we're focusing on the sound of the ear, this is body, and this is physical.

80
00:10:50,440 --> 00:10:55,440
But then there's also a second one which is feelings or sensations.

81
00:10:55,440 --> 00:11:03,440
So sometimes when we're sitting, we might feel pain in the legs or pain in the back or pain in the hand or pain in the stomach,

82
00:11:03,440 --> 00:11:10,440
pain in the chest, pain wherever.

83
00:11:10,440 --> 00:11:14,440
And this is a totally valid meditation object.

84
00:11:14,440 --> 00:11:21,440
It's not less valid than the rising and the falling of the abdomen.

85
00:11:21,440 --> 00:11:29,440
So whereas normally we'd let it disturb us, bring us stress and suffering.

86
00:11:29,440 --> 00:11:34,440
Now we're going to come to see it for what it is and remind ourselves that it's only pain.

87
00:11:34,440 --> 00:11:38,440
It's only a sensation, it's only a part of reality.

88
00:11:38,440 --> 00:11:44,440
There's no reason to judge it, no reason to get upset by it, no reason to obsess over it.

89
00:11:44,440 --> 00:11:54,440
And so we say to ourselves, pain, pain, pain staying with it no matter how strong it gets, no matter how bad it gets.

90
00:11:54,440 --> 00:11:58,440
Because when we're aware that it simply is pain, then it's neither good nor bad.

91
00:11:58,440 --> 00:12:00,440
It has no power over us.

92
00:12:00,440 --> 00:12:02,440
It simply is what it is.

93
00:12:02,440 --> 00:12:04,440
And we're with reality.

94
00:12:04,440 --> 00:12:06,440
We're with the reality of the object.

95
00:12:06,440 --> 00:12:09,440
We don't know anything besides the reality of it.

96
00:12:09,440 --> 00:12:11,440
There's no room in our minds.

97
00:12:11,440 --> 00:12:17,440
We're mindful, our minds are full of the reality.

98
00:12:17,440 --> 00:12:45,440
And we can just say that as long as there's pain there.

99
00:12:45,440 --> 00:12:59,440
And when it's gone, we just come back to the rising and the following.

100
00:12:59,440 --> 00:13:02,440
Come back to the body.

101
00:13:02,440 --> 00:13:04,440
But we also see that there's other things.

102
00:13:04,440 --> 00:13:05,440
The third one is the mind.

103
00:13:05,440 --> 00:13:09,440
So we're going to see that our mind is thinking a lot.

104
00:13:09,440 --> 00:13:18,440
Often the mind is wandering into the past, wandering into the future.

105
00:13:18,440 --> 00:13:29,440
The beginning of all diversification, where we begin to make more of something than it actually is,

106
00:13:29,440 --> 00:13:32,440
is with the mind where we start thinking about things.

107
00:13:32,440 --> 00:13:39,440
We start thinking about remembering things or worrying about the future or thinking about what's happening right now

108
00:13:39,440 --> 00:13:46,440
and starting to form judgments and views and ideas about it.

109
00:13:46,440 --> 00:13:47,440
So we can catch these as well.

110
00:13:47,440 --> 00:13:50,440
If we get better at this, we can start to catch the thoughts.

111
00:13:50,440 --> 00:13:54,440
And this is where mindfulness of the body is very useful

112
00:13:54,440 --> 00:14:00,440
because it's like we're waiting for the mind ready to catch it when it wanders away

113
00:14:00,440 --> 00:14:10,440
and we're retraining it, training it out of this obsession with distracted thought.

114
00:14:10,440 --> 00:14:17,440
And so we say to ourselves thinking, thinking, just reminding ourselves that we're thinking.

115
00:14:17,440 --> 00:14:21,440
And what happens then is that we're able to see it as something in the present

116
00:14:21,440 --> 00:14:28,440
and not follow it into the past or the future into delusion or end illusion.

117
00:14:28,440 --> 00:14:31,440
And then it disappears and we can come back to the rising of the following.

118
00:14:31,440 --> 00:14:34,440
This is without forcing it, without pushing it away.

119
00:14:34,440 --> 00:14:37,440
We're simply acknowledging it's existence.

120
00:14:37,440 --> 00:14:41,440
And simply by acknowledging it, it loses all power over us.

121
00:14:41,440 --> 00:14:49,440
And it disappears by itself.

122
00:14:49,440 --> 00:15:00,440
And the final one on a basic level is our emotions when we have anger, liking or disliking,

123
00:15:00,440 --> 00:15:13,440
greed or anger, liking, wanting, disliking anger, sadness, fear or worry, sadness.

124
00:15:13,440 --> 00:15:21,440
This is what comes from thinking, comes from, and this becomes from distracted thought.

125
00:15:21,440 --> 00:15:28,440
When we make more of something than it is, we have all these emotions that come up, judging things as good or bad and liking or disliking.

126
00:15:28,440 --> 00:15:30,440
And this is the real cause for suffering.

127
00:15:30,440 --> 00:15:36,440
This is where suffering really comes into existence through our judgment.

128
00:15:36,440 --> 00:15:52,440
Our judgment of things which are just arising and ceasing which have no intrinsic positive or negative value.

129
00:15:52,440 --> 00:16:01,440
So in order that these things should not grow and should not be able to feed on the experience, we focus on them.

130
00:16:01,440 --> 00:16:08,440
We look at them, we acknowledge them, we keep our minds full of the reality of them.

131
00:16:08,440 --> 00:16:13,440
So what happens then is they can't go back and find fuel in the object of our attention.

132
00:16:13,440 --> 00:16:15,440
They are the object of our attention.

133
00:16:15,440 --> 00:16:21,440
We focus on the anger, we say to ourselves, angry, angry.

134
00:16:21,440 --> 00:16:30,440
When we want something wanting, wanting, when we like something liking, when we feel sad, sad, when we feel depressed,

135
00:16:30,440 --> 00:16:33,440
depressed, depressed, depressed.

136
00:16:33,440 --> 00:16:40,440
Just keeping these things in mind and not letting ourselves then judge them thinking,

137
00:16:40,440 --> 00:16:42,440
oh now I have to do something about it.

138
00:16:42,440 --> 00:16:45,440
I am angry so I have to hurt someone or say something nasty.

139
00:16:45,440 --> 00:16:49,440
I want something so now I have to go after it.

140
00:16:49,440 --> 00:16:54,440
I feel depressed so now I have to take medication or go see a doctor.

141
00:16:54,440 --> 00:16:58,440
Really you don't have to do anything even about these emotions.

142
00:16:58,440 --> 00:17:05,440
Simply see them for what they are and not to disappear by themselves.

143
00:17:05,440 --> 00:17:08,440
It is because of our mind that always makes more of things than they are.

144
00:17:08,440 --> 00:17:11,440
This is why we get these emotions in the first place.

145
00:17:11,440 --> 00:17:14,440
But not seeing things as they are.

146
00:17:14,440 --> 00:17:15,440
So we focus on these as well.

147
00:17:15,440 --> 00:17:20,440
We focus on our thoughts and we focus on our emotions.

148
00:17:20,440 --> 00:17:30,440
We can try this now together just for a couple of minutes.

149
00:18:20,440 --> 00:18:39,440
Thank you very much.

150
00:18:39,440 --> 00:18:58,440
Thank you very much.

151
00:18:58,440 --> 00:19:24,440
Thank you very much.

152
00:19:24,440 --> 00:19:50,440
Thank you very much.

153
00:19:50,440 --> 00:20:15,440
And when there is nothing left to acknowledge we can then go back again to the rising of the falling of the stomach.

154
00:20:15,440 --> 00:20:21,440
Whenever something comes up again we go out and let it be our object of meditation.

155
00:20:21,440 --> 00:20:24,440
Just naturally taking whatever is present.

156
00:20:24,440 --> 00:20:47,440
When there is nothing left to come back again to the rising of the Carbon deficiency.

157
00:21:17,440 --> 00:21:43,840
Some other things that we have to watch out for in meditation are besides liking and disliking.

158
00:21:43,840 --> 00:21:56,440
Everyone is drowziness, meditating we can often feel tired, often feel like our minds are drowsy,

159
00:21:56,440 --> 00:22:01,880
feel like we're falling asleep.

160
00:22:01,880 --> 00:22:05,720
So this isn't something that we have to worry about, it's something that we use as a meditation

161
00:22:05,720 --> 00:22:10,880
object as well, and we come to just see it for what it is, it's an amazing thing about

162
00:22:10,880 --> 00:22:17,480
all of these things, including drowziness, is that once you start focusing on them.

163
00:22:17,480 --> 00:22:21,080
And if you're really good at catching them and seeing them for what they are, they just disappear,

164
00:22:21,080 --> 00:22:24,000
they don't have any power over you anymore.

165
00:22:24,000 --> 00:22:26,800
It's like a straightening out of the mind, it's like the only reason it was there in the

166
00:22:26,800 --> 00:22:32,040
first place, is because the mind was bent out of shape, and as soon as you can catch it up,

167
00:22:32,040 --> 00:22:40,080
it disappears, but you have to be good and you have to be consistent, and you have to stay

168
00:22:40,080 --> 00:22:53,240
with it, just saying to yourself, drowsy, drowsy, trying to catch up the drowsyness in your attention.

169
00:22:53,240 --> 00:22:56,640
The opposite, of course, is when the mind is too awake, when the mind is too alert,

170
00:22:56,640 --> 00:23:04,480
is too active, distracted, thinking a lot, thinking so many different things, not just

171
00:23:04,480 --> 00:23:09,000
thinking, but it means the mind is unfocused, it's a general state of mind.

172
00:23:09,000 --> 00:23:11,840
This can be a real hindrance to meditation.

173
00:23:11,840 --> 00:23:15,800
So in this happens, we just put everything aside and just focus on the state of being distracted,

174
00:23:15,800 --> 00:23:45,640
watching our mind run hither and thither, saying to ourselves, distracted and distracted.

175
00:23:45,640 --> 00:23:46,640
Thank you for watching.

176
00:23:46,640 --> 00:23:47,640
Thank you for watching.

177
00:23:47,640 --> 00:24:10,640
Thank you for watching.

178
00:24:10,640 --> 00:24:34,080
The final hindrance in meditation is probably the worst one, the one which causes the most

179
00:24:34,080 --> 00:24:42,720
trouble for, especially for beginner meditators, is doubt.

180
00:24:42,720 --> 00:24:46,600
When you have doubt about yourselves or doubt about the meditation practice, doubt about

181
00:24:46,600 --> 00:24:53,200
which way is right, which way is wrong, doubt about the things you're experiencing, confusion

182
00:24:53,200 --> 00:24:57,160
about what they might be, not being sure.

183
00:24:57,160 --> 00:25:02,520
When the mind is not sure, it's like a fork in the road and you don't know which one

184
00:25:02,520 --> 00:25:08,200
to take, you don't get anywhere.

185
00:25:08,200 --> 00:25:14,840
And often this is very difficult for meditators to know how to deal with doubt.

186
00:25:14,840 --> 00:25:19,360
Sometimes we go the wrong way and stop meditating.

187
00:25:19,360 --> 00:25:22,720
We think that the doubt has some significance and that means what we're doing is probably

188
00:25:22,720 --> 00:25:23,720
wrong.

189
00:25:23,720 --> 00:25:28,600
If it were right, we would never doubt it and that's not certainly not the case.

190
00:25:28,600 --> 00:25:34,600
We can be doing the perfectly right thing and still doubt it.

191
00:25:34,600 --> 00:25:39,800
And why that is, because doubt is just a quality of mind, it's something that we have

192
00:25:39,800 --> 00:25:43,440
and we carry around with this and we end up doubting just about everything.

193
00:25:43,440 --> 00:25:48,920
We don't give things a fair chance.

194
00:25:48,920 --> 00:25:54,800
We don't really try to understand the phenomenon and so we just doubt about it.

195
00:25:54,800 --> 00:25:59,200
So it's a real hindrance in our practice, we can't get anywhere.

196
00:25:59,200 --> 00:26:03,800
Instead of examining it and trying to see the truth about it, because after we see the

197
00:26:03,800 --> 00:26:11,680
truth, of course, we wouldn't need any doubt, whether it's good or bad.

198
00:26:11,680 --> 00:26:15,520
So for this reason, we don't give doubt any weight, any importance.

199
00:26:15,520 --> 00:26:19,560
We simply say to ourselves, doubting, doubting, just focusing on the doubting, put everything

200
00:26:19,560 --> 00:26:25,920
aside, put aside the practice, put aside the phenomenon, put aside the experience, just focus

201
00:26:25,920 --> 00:26:29,240
on the doubt itself.

202
00:26:29,240 --> 00:26:33,200
When you do this, you see how much better you feel when you give up doubt.

203
00:26:33,200 --> 00:26:38,440
When you just don't care right or wrong, you just be.

204
00:26:38,440 --> 00:26:42,120
You're not trying to prove anything, you're not trying to be anything.

205
00:26:42,120 --> 00:26:45,480
You're just here and now.

206
00:26:45,480 --> 00:26:51,640
And when the doubt disappears, you feel much more common, much more peaceful.

207
00:26:51,640 --> 00:26:56,680
So you realize there's really nothing, no need to doubt whatsoever, there's nothing

208
00:26:56,680 --> 00:27:09,160
doubt dubious about simply being with reality, and when these things are gone, then we

209
00:27:09,160 --> 00:27:16,360
can focus back on the practice, focus back on reality.

210
00:27:16,360 --> 00:27:19,520
And what we're going to do is we practice this, what's going to happen is our mind's

211
00:27:19,520 --> 00:27:24,520
going to straighten out and these hindrance has come less and less.

212
00:27:24,520 --> 00:27:28,760
There's less liking, less wanting for things.

213
00:27:28,760 --> 00:27:36,360
There's also less disliking or discomfort, depression, sadness, these things become less

214
00:27:36,360 --> 00:27:37,360
and less frequently.

215
00:27:37,360 --> 00:27:42,120
And they still come back again and again in waves because of all the many years we've

216
00:27:42,120 --> 00:27:53,120
developed and accumulated and encouraged them, but they come less and less frequently.

217
00:27:53,120 --> 00:27:58,640
There's less laziness or drowsiness, there's less mental distraction, there's less doubt.

218
00:27:58,640 --> 00:28:02,120
And so our mind is much calmer, much more peaceful than before.

219
00:28:02,120 --> 00:28:08,040
At this point, many meditators can think they've reached the ultimate goal and they become

220
00:28:08,040 --> 00:28:08,880
lazy.

221
00:28:08,880 --> 00:28:15,080
And as a result of the laziness, they slip back down and things get bad again, and when

222
00:28:15,080 --> 00:28:21,080
things get bad, they aren't ready to deal with it because they become lazy.

223
00:28:21,080 --> 00:28:26,080
The mind's were so peaceful, they thought that it was somehow permanent.

224
00:28:26,080 --> 00:28:30,360
And so what we do is even when the meditation starts to work, starts to bring bare

225
00:28:30,360 --> 00:28:33,520
fruit.

226
00:28:33,520 --> 00:28:36,000
We don't stop there, we focus on the fruit as well.

227
00:28:36,000 --> 00:28:41,240
When we feel calm, we say to ourselves, calm.

228
00:28:41,240 --> 00:28:45,120
When we feel happy, we say to ourselves, happy, happy taking it up as our meditation

229
00:28:45,120 --> 00:28:50,040
object, not making more of it than it is, not saying to ourselves, oh, this happiness

230
00:28:50,040 --> 00:28:55,680
means x, this peace means y.

231
00:28:55,680 --> 00:28:58,160
When you're happy, it's just happiness.

232
00:28:58,160 --> 00:29:01,040
When you're at peace, it's just peace.

233
00:29:01,040 --> 00:29:03,440
And you start to see that actually it's impermanent as well.

234
00:29:03,440 --> 00:29:07,760
It's also changing, also subject to the laws of nature.

235
00:29:07,760 --> 00:29:12,800
It comes because of some cause, generally the meditation practice, and when you stop

236
00:29:12,800 --> 00:29:19,120
practicing it disappears, it's not the goal, it's not the result which we're looking for.

237
00:29:19,120 --> 00:29:25,160
So we put it aside and come back to where we let it go when it's gone, it's gone, when

238
00:29:25,160 --> 00:29:27,520
it's there, when it's gone, it's gone.

239
00:29:27,520 --> 00:29:33,080
When we let it go according to nature, we don't mourn when it's not there, or seek

240
00:29:33,080 --> 00:29:46,640
after it when it's missing, because we don't attach to it when it's present.

241
00:29:46,640 --> 00:29:51,520
In this way, we can be comfortable with all experience, with all of reality, the whole

242
00:29:51,520 --> 00:30:02,200
diversity of experience, not segmenting our experience, or segmenting in reality into good

243
00:30:02,200 --> 00:30:04,480
and bad.

244
00:30:04,480 --> 00:30:30,400
Everything is what it is, and our happiness has nothing to do with experiential phenomenon.

245
00:30:30,400 --> 00:30:40,000
Second, Our

246
00:31:10,000 --> 00:31:39,840
So, I will stop there.

247
00:31:39,840 --> 00:31:46,840
As far as the teaching for today, if people have any questions, you're welcome to ask.

248
00:32:09,840 --> 00:32:28,840
Thank you all for coming.

249
00:32:28,840 --> 00:32:37,840
Is there a difference between a spoken mantra and concentration on concentrating on breath?

250
00:32:37,840 --> 00:32:44,840
Yes, I would say there is, because concentration is not the same as mindfulness.

251
00:32:44,840 --> 00:32:52,840
The word mindfulness has its own meaning, and as I've said, it's a fairly poor translation.

252
00:32:52,840 --> 00:32:59,840
Mindfulness is the remembering of something for what it is.

253
00:32:59,840 --> 00:33:10,840
So when we say to ourselves, rising, we're reminding ourselves of what it is, not allowing our minds to attach to it, to stay bent out of shape.

254
00:33:10,840 --> 00:33:16,840
The nature of the mind in its ordinary state is bent.

255
00:33:16,840 --> 00:33:20,840
This is how we come into this world. We don't come into the world pure.

256
00:33:20,840 --> 00:33:31,840
So what we're trying to do is train ourselves to straighten it out. It takes something beyond simply concentration.

257
00:33:31,840 --> 00:33:40,840
It takes work. It takes mindfulness.

258
00:33:40,840 --> 00:33:47,840
Concentrating on breath, generally uses a mantra anyway in Buddhist circles.

259
00:33:47,840 --> 00:33:54,840
Often they'll say Buddha, Buddha is the name of the Buddha. They'll use it for the breath.

260
00:33:54,840 --> 00:34:06,840
In the traditional text, which is probably more proper, they would use in-out-in-out, or bhassasami, sasami, breathing in, breathing out.

261
00:34:06,840 --> 00:34:20,840
Or they would count it, saying in one-out-one, in two-out-two, in three-out-three, in four-out-four, or just counting one, two-three, four up to ten and back down again.

262
00:34:20,840 --> 00:34:26,840
But this is an insight meditation because it's not awareness of the reality of it.

263
00:34:26,840 --> 00:34:35,840
It's just a concept which is used to calm the mind.

264
00:34:35,840 --> 00:34:49,840
One-out-one, Two-out-one in the world, are doing a lot of possibilities.

265
00:34:49,840 --> 00:35:15,220
welcome

266
00:35:15,220 --> 00:35:24,220
.

267
00:35:24,220 --> 00:35:30,720
..

268
00:35:30,720 --> 00:35:32,720
..

269
00:35:32,720 --> 00:35:35,720
..

270
00:35:35,720 --> 00:35:38,720
..

271
00:35:38,720 --> 00:35:41,720
..

272
00:35:41,720 --> 00:35:44,720
..

273
00:35:44,720 --> 00:35:54,220
..

274
00:35:54,220 --> 00:35:55,220
..

275
00:35:55,220 --> 00:35:56,220
..

276
00:35:56,220 --> 00:35:55,220
..

277
00:35:55,220 --> 00:35:56,220
..

278
00:35:56,220 --> 00:35:57,220
..

279
00:35:57,220 --> 00:35:57,220
..

280
00:35:57,220 --> 00:35:58,720
There's no questions.

281
00:35:58,720 --> 00:36:00,220
There's no questions.

282
00:36:00,220 --> 00:36:02,220
Please let me know otherwise.

283
00:36:02,220 --> 00:36:04,620
We'll just stop it there.

284
00:36:04,620 --> 00:36:06,520
Ah, here.

285
00:36:06,520 --> 00:36:13,220
Must one have a one pointed mind before conducting rebass in there?

286
00:36:13,220 --> 00:36:20,620
No, one must one need not have a one-pointed mind before conducting me pass in a.

287
00:36:20,620 --> 00:36:24,220
If that were the case, the Buddha would not have encouraged us to take the five hindrances

288
00:36:24,220 --> 00:36:31,540
as a meditation object in Vipassana because when one starts practicing the four foundations

289
00:36:31,540 --> 00:36:43,140
of mindfulness, one is including the five hindrances in one's awareness in one's attention.

290
00:36:43,140 --> 00:36:52,140
So, this is clear that at the beginning of meditation one is not one-pointed.

291
00:36:52,140 --> 00:37:01,140
Now, the reason I hesitated in answering is because according to the Abhidhamma, the mind is always one-pointed.

292
00:37:01,140 --> 00:37:04,140
Even the very distracted mind is still one-pointed.

293
00:37:04,140 --> 00:37:09,140
It's always fixed on a single object.

294
00:37:09,140 --> 00:37:17,140
Another reason why one might hesitate in answering this is because even though one begins to practice Vipassana meditation

295
00:37:17,140 --> 00:37:26,140
or begins to practice meditation without a single point, without a mind which is focused, which has great concentration

296
00:37:26,140 --> 00:37:34,140
that until through the practice of Vipassana, until concentration arises, Vipassana insight will not come.

297
00:37:34,140 --> 00:37:43,140
So, I guess it's just that the question is a little bit, could be a little bit misleading,

298
00:37:43,140 --> 00:37:45,140
I guess I understand.

299
00:37:45,140 --> 00:37:50,140
I understand where you're coming from and I imagine that my answer is direct,

300
00:37:50,140 --> 00:37:59,140
but really it is true that one must have a one-pointed mind before Vipassana insight arises.

301
00:37:59,140 --> 00:38:05,140
One need not have a one-pointed mind before beginning to practice to see clearly.

302
00:38:05,140 --> 00:38:08,140
Otherwise, you know, where would you start?

303
00:38:08,140 --> 00:38:14,140
And I guess the idea then would be that you'd have to practice meditation based on a concept.

304
00:38:14,140 --> 00:38:20,140
Meditation based on something that was not real before you could practice Vipassana, which certainly isn't the case.

305
00:38:20,140 --> 00:38:26,140
But what happens in Vipassana meditation is that the concentration comes first and then the insight surely,

306
00:38:26,140 --> 00:38:30,140
but the object is the same.

307
00:38:30,140 --> 00:38:32,140
So, that's the first question.

308
00:38:32,140 --> 00:38:37,140
The second question, are you going to your house and I'll have to the meditation?

309
00:38:37,140 --> 00:38:39,140
I could.

310
00:38:39,140 --> 00:38:47,140
I have to work on the chanting book next, but I can do that anytime.

311
00:38:47,140 --> 00:38:55,140
Gotcha.

312
00:38:55,140 --> 00:39:09,140
So, we can say that Vipassana helps to produce one-pointedness.

313
00:39:09,140 --> 00:39:13,140
Again, we have to ask what you mean by Vipassana.

314
00:39:13,140 --> 00:39:20,140
Do you mean the practice of meditation too see clearly or the experience of seeing clearly?

315
00:39:20,140 --> 00:39:25,140
And I guess the answer is yes in both cases.

316
00:39:25,140 --> 00:39:30,140
The meditation which we practice for the purpose of seeing clearly does bring a state of mind,

317
00:39:30,140 --> 00:39:33,140
which is more focused on single objects.

318
00:39:33,140 --> 00:39:37,140
But as I said, the mind is always one-pointed anyway.

319
00:39:37,140 --> 00:39:43,140
But in classical Buddhist doctrine, including the Buddhist speech himself,

320
00:39:43,140 --> 00:39:48,140
what do you mean by one-pointed is yes focused very strongly on a single object.

321
00:39:48,140 --> 00:39:53,140
And yes, the practice does lead to that because that's the only way we could see things clearly.

322
00:39:53,140 --> 00:39:59,140
The only way Vipassana can arise is through this single-pointed state of mind based on the phenomenon.

323
00:39:59,140 --> 00:40:08,140
But that's a Vipassana meditation focusing on the rising and the falling brings one-pointedness based on the rising and based on the falling.

324
00:40:08,140 --> 00:40:13,140
Once you gain insight, then of course your mind becomes even more one-pointed.

325
00:40:13,140 --> 00:40:20,140
Through the practice of through the gaining of insight, the mind becomes one-pointed on Nirvana or Nirvana.

326
00:40:20,140 --> 00:40:27,140
And that's the ultimate one-pointedness where the mind is no longer giving rise to any experience whatsoever,

327
00:40:27,140 --> 00:40:30,140
where there is only the cessation of suffering.

328
00:40:30,140 --> 00:40:32,140
And that's ultimate one-pointedness.

329
00:40:32,140 --> 00:40:35,140
And so the answer is yes in both cases.

330
00:40:35,140 --> 00:40:49,140
Though I'm not quite sure which one you're referring to.

331
00:40:49,140 --> 00:40:51,140
You're welcome.

332
00:40:51,140 --> 00:40:58,140
I guess it was one of the two.

333
00:40:58,140 --> 00:41:06,140
Yes.

334
00:41:06,140 --> 00:41:35,140
Thank you.

335
00:41:35,140 --> 00:42:02,140
Oh, and one thing I meant to mention is that people who are interested in this sort of meditation are welcome to look further at some of the other things I've said

336
00:42:02,140 --> 00:42:07,140
and some more techniques on how to practice this type of Vipassana meditation.

337
00:42:07,140 --> 00:42:31,140
On my YouTube channel, youtube.com.com.

338
00:42:31,140 --> 00:42:57,140
Thank you.

339
00:42:57,140 --> 00:43:23,140
Thank you.

340
00:43:23,140 --> 00:43:30,140
Okay, well thanks for coming everyone.

341
00:43:30,140 --> 00:43:32,140
I hope it's been useful.

342
00:43:32,140 --> 00:43:48,140
See you again next time.

