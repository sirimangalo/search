1
00:00:00,000 --> 00:00:11,000
Does the realization of impermanence of phenomena itself constitute nibana, or is nibana to be considered a part, as a part from phenomena?

2
00:00:11,000 --> 00:00:22,000
Yes, nibana is kandabimuti. It's emancipated or liberated or outside of the kandas, outside of the aggregate.

3
00:00:22,000 --> 00:00:28,000
So, the realization of impermanence has to occur based on something that's impermanent.

4
00:00:28,000 --> 00:00:57,000
If the realization of impermanence were nibana, then nibana would be the association or the observation of nibana is the observation of something that is permanent.

5
00:00:57,000 --> 00:01:08,000
It's satisfying and not self, but it's essential.

6
00:01:08,000 --> 00:01:13,000
So, there are different ways.

7
00:01:13,000 --> 00:01:25,000
You have to understand that what we mean by realization and Buddhism is the moment where you actually see something as being so it's not a state afterwards where you somehow say, wow, everything is impermanent.

8
00:01:25,000 --> 00:01:32,000
The realization occurs based on an object that is impermanent, so nibana is the next moment.

9
00:01:32,000 --> 00:01:40,000
If you really see something as impermanent, like for example, let's bring it back to practical, when you're watching the rising and falling and the stomach.

10
00:01:40,000 --> 00:01:55,000
There can come a point, the peak of the past and the meditation, where you're watching the stomach rising and falling, and suddenly it changes.

11
00:01:55,000 --> 00:02:02,000
It goes quickly, because usually you're watching it and suddenly it speeds up or it changes radically.

12
00:02:02,000 --> 00:02:11,000
And that moment, because the meditators mind is so sharp, that moment leads to nibana, the next moment there's a cessation experience.

13
00:02:11,000 --> 00:02:17,000
It can also occur with suffering, so watching the rising and falling and suddenly it becomes unbearable.

14
00:02:17,000 --> 00:02:31,000
It's uncomfortable, or there's pain throughout the body to the extent that the mind is no longer interested, it's no longer attached to the body,

15
00:02:31,000 --> 00:02:37,000
or to the sankara's in it. Let's go in the next moment, there's a nibana experience in nibana.

16
00:02:37,000 --> 00:02:45,000
And it can also happen through non-self, so for example, where you're watching the rising and falling and suddenly the rising and falling appears to be going by itself.

17
00:02:45,000 --> 00:02:58,000
It very clearly is not going according to our control, and there's this feeling of that, and the next moment is nibana, the cessation experience.

18
00:02:58,000 --> 00:03:01,000
So it's the moment after the realization.

19
00:03:01,000 --> 00:03:08,000
The realization itself, according to the texts, is called Anulomanyan, the knowledge of conformity.

20
00:03:08,000 --> 00:03:11,000
It's two or three moments, or one moment, I can remember.

21
00:03:11,000 --> 00:03:18,000
It's in a very short period of time, where the meditator sees impermanence.

22
00:03:18,000 --> 00:03:29,000
Our suffering, or non-self, one of the three.

