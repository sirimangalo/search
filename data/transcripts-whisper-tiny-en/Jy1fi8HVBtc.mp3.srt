1
00:00:00,000 --> 00:00:22,760
Good evening everyone, welcome to our evening session. We all gather together locally

2
00:00:22,760 --> 00:00:37,680
around the internet. To this end of the Dhamma, to discuss the Dhamma, to practice the Dhamma.

3
00:00:37,680 --> 00:00:47,200
Today I got word of a word just yesterday. Last night I think about a talk,

4
00:00:47,200 --> 00:00:56,560
I was being held at McMaster University, something about Buddhism in Myanmar. When I got there,

5
00:00:56,560 --> 00:01:02,520
it was a pleasant surprise they were talking about our tradition, talking about our meditation

6
00:01:02,520 --> 00:01:13,080
tradition that was rather involved, but much of it was based around the practice of mindfulness,

7
00:01:13,080 --> 00:01:23,880
of insight based on the four foundations of mindfulness. I wrote you in a scholarly setting

8
00:01:23,880 --> 00:01:30,600
I was quite incredible to people coming from University of Toronto and the speaker came

9
00:01:30,600 --> 00:01:43,720
from University of Virginia, I think. It's quite encouraging and the talk was lectured

10
00:01:43,720 --> 00:01:49,040
in the way you call it the speaker. It's quite encouraging as well. He talked a lot about

11
00:01:49,040 --> 00:01:58,160
how mindfulness has evolved, the practice has evolved as it's moved and over time and the use

12
00:01:58,160 --> 00:02:08,200
of the word mindfulness, how it's become such a catch word. There are issues and questions

13
00:02:08,200 --> 00:02:28,680
of authenticity and keeping with the tradition, keeping with the original teachings. The

14
00:02:28,680 --> 00:02:38,120
idea that the word mindfulness has come to mean something different over time. As a result,

15
00:02:38,120 --> 00:02:42,600
meditation practice has changed and people's ideas of what it means to practice, but it's

16
00:02:42,600 --> 00:02:49,880
teaching has changed. But nonetheless, to hear people and to see a picture of Mahasi Sayyada

17
00:02:49,880 --> 00:03:00,280
upon the word is quite, quite heartwarming. Even just to hear people talk about him and to

18
00:03:00,280 --> 00:03:08,520
talk about the use of insight meditation, the practice of insight meditation to have a chance

19
00:03:08,520 --> 00:03:22,440
to talk about the discussion afterwards, got onto the topic of what is mindfulness and one

20
00:03:22,440 --> 00:03:31,120
of the members of the audience was quite bold in his conception that mindfulness isn't

21
00:03:31,120 --> 00:03:37,840
good or bad. Mindfulness just fixes your attention on an object or something or reinforces

22
00:03:37,840 --> 00:03:44,040
I'm not actually quite clear what is. I don't think I quite agree with it, but the question

23
00:03:44,040 --> 00:03:54,240
came up whether the idea was that a sniper, a killer, a soldier, can be quite mindful

24
00:03:54,240 --> 00:04:03,640
of what they're doing. The cat can be quite, the cat idea was that it was quite focused.

25
00:04:03,640 --> 00:04:12,440
And yet, if you know anything about the Abidhamma, these are wholesome mind states. So we

26
00:04:12,440 --> 00:04:18,960
have this idea that concentration is wholesome, mindfulness is wholesome, confidence is

27
00:04:18,960 --> 00:04:25,680
wholesome. So what about these people who are confident, these evil people who are confident

28
00:04:25,680 --> 00:04:37,680
about their evil? People who are confident in scamming, manipulating, in oppressing others.

29
00:04:37,680 --> 00:04:52,160
And so I had to give a bit of a correction, I thought to explain something that's interesting

30
00:04:52,160 --> 00:04:57,200
to us because it's an interesting question of karma, what is karma? What is good karma

31
00:04:57,200 --> 00:05:03,760
and bad karma? How can you say something is good karma when bad karma, when there's

32
00:05:03,760 --> 00:05:18,080
so much, it's quite complicated. It actually sounds like some sort of magic, something

33
00:05:18,080 --> 00:05:23,880
very magical or spiritual. When you kill, it sounds nice, actually. When you kill, it's

34
00:05:23,880 --> 00:05:28,840
bad karma and bad things are going to happen to you. You're going to get retribution.

35
00:05:28,840 --> 00:05:36,920
So if it takes on this sort of mystical sort of air, this idea that there's some

36
00:05:36,920 --> 00:05:46,360
karmic power or force, but it's not exactly how it works. I mean, karma is, with much

37
00:05:46,360 --> 00:05:53,800
of the Buddhist teachings, is really to be understood as a meditator, as someone practicing

38
00:05:53,800 --> 00:06:03,040
mindfulness. Karmo was this concept of it was around before the Buddha. It became enlightened

39
00:06:03,040 --> 00:06:09,720
when they had the wrong idea. So he was just explaining to them, what is actually potent?

40
00:06:09,720 --> 00:06:19,280
And so what is actually potent is intention. And so at first you think, well, if you're

41
00:06:19,280 --> 00:06:31,320
intent to kill, okay, then then killing is bad, but it's not even that simple.

42
00:06:31,320 --> 00:06:36,040
Because every moment, if you think about it, when you intend to kill someone, that's

43
00:06:36,040 --> 00:06:42,840
one intention, but the next moment you intend to pick up a knife, the next moment you intend

44
00:06:42,840 --> 00:06:49,240
to go and find the person you're going to kill. And every moment, there's an intention.

45
00:06:49,240 --> 00:06:57,680
It's not even exactly intention, it's your volition or your bent, your inclination at

46
00:06:57,680 --> 00:07:10,280
that moment, your state of mind, really. Chitana. And so I was saying, well, my understanding

47
00:07:10,280 --> 00:07:15,400
from the Abhidhamma, I didn't want to sort of preach as a meditation teacher, but I

48
00:07:15,400 --> 00:07:25,360
can preach to all of you. So as we understand it as meditators, karma is every moment.

49
00:07:25,360 --> 00:07:29,640
So if you say a killer has mindfulness, well, they don't have mindfulness at the moment

50
00:07:29,640 --> 00:07:35,240
when they want to kill. At that moment, there's no mindfulness. They are. I'm actually

51
00:07:35,240 --> 00:07:42,360
not sure. There's no wholesomeness. It's funny now, my Abhidhamma is not clear, but they

52
00:07:42,360 --> 00:07:46,040
were saying that mindfulness is also always wholesome according to the Abhidhamma. I'm not

53
00:07:46,040 --> 00:07:54,080
so sure. Look at this thing and get myself in trouble. Doesn't really matter. We're not

54
00:07:54,080 --> 00:08:01,320
really interested in the technicalities, not right now. Please forgive me for being, for

55
00:08:01,320 --> 00:08:11,200
being imperfect. But the point being, no action is hard to find an action that's entirely

56
00:08:11,200 --> 00:08:16,520
wholesome or something, because wholesome really is referring to the individual that

57
00:08:16,520 --> 00:08:24,000
which brings you happiness, peace, goodness, or, well, bring good things to you. In other

58
00:08:24,000 --> 00:08:31,720
words, things that bring success. So a person's ability to, to successfully shoot someone else.

59
00:08:31,720 --> 00:08:38,160
It actually requires moments of wholesomeness, moments where you are, where you are focused

60
00:08:38,160 --> 00:08:46,560
and concentrated, present, confident. With the problem is, there's so much overarching and so

61
00:08:46,560 --> 00:08:57,800
many moments of very strong ignorance, hatred, disregard for people, for life. You see?

62
00:08:57,800 --> 00:09:01,600
And this is, this is true because we're not always thinking about killing a person, often

63
00:09:01,600 --> 00:09:10,280
we're thinking about lifting up our gun and so on. It'd be mostly unwholesome, but there

64
00:09:10,280 --> 00:09:17,280
will still be moments of wholesomeness. I mean, an easier example might be when you do

65
00:09:17,280 --> 00:09:22,400
something good, when a person, when you give a gift to someone, you think, well, that's

66
00:09:22,400 --> 00:09:32,280
wholesome, right? I say, giving is good. Not exactly. Not necessarily. Not entirely.

67
00:09:32,280 --> 00:09:36,880
Remember when we used to give gifts to Adjantag, everyone is so worried. I have to do it

68
00:09:36,880 --> 00:09:44,000
just right. I started thinking, you know, this worry is not wholesome. We're getting all this

69
00:09:44,000 --> 00:09:47,320
stuff, sitting there, we have to sit around and wait for Adjantag to arrive and he's

70
00:09:47,320 --> 00:09:52,640
always late. Mm, kind of grumpy. I think, well, that's not wholesome.

71
00:09:52,640 --> 00:09:58,920
It's much more complicated, you see? Meditation, maybe people say, yes, I'm going to

72
00:09:58,920 --> 00:10:05,720
meditate, oh, good. There's a lot of unwholesomeness that comes up for meditation. You see,

73
00:10:05,720 --> 00:10:10,280
it's not something to be afraid of. It's not like, oh, well, I better not go and meditate

74
00:10:10,280 --> 00:10:16,400
or I'll get angry, right? It's not magic. It's not a demon that you have to be afraid

75
00:10:16,400 --> 00:10:26,320
of. It's science. It's really psychology. You know, there are things that tear your mind

76
00:10:26,320 --> 00:10:34,840
apart, that remove your mind's ability to function, that deal it, debilitate, that

77
00:10:34,840 --> 00:10:49,760
decompass it, that make you incapacitate you, weaken the mind, cripple the mind, shrink

78
00:10:49,760 --> 00:10:57,600
the mind, but those are just moments and in meditation we're very much interested in studying

79
00:10:57,600 --> 00:11:06,960
this, we're not going to be afraid of it. It's like you're studying diseases or you're

80
00:11:06,960 --> 00:11:13,480
studying pain, you know, we want to understand it. So we're willing to allow ourselves,

81
00:11:13,480 --> 00:11:18,640
give ourselves the room to be angry, give ourselves the space to commit on wholesome

82
00:11:18,640 --> 00:11:24,080
karma, even as meditators. Meditation isn't entirely wholesome. There's a lot of unholesomeness,

83
00:11:24,080 --> 00:11:30,200
an insight meditation, getting angry, it's just why people are skeptical about it sometimes

84
00:11:30,200 --> 00:11:34,600
and they think you have to do with the Buddha said, you know, the Buddha was inclined

85
00:11:34,600 --> 00:11:41,280
to have its meditators because it's much more wholesome to do some at the first focus

86
00:11:41,280 --> 00:11:46,760
your mind and separate your mind away from unholesomeness. It takes a lot of time and a lot

87
00:11:46,760 --> 00:11:54,760
of effort and a lot of ideal conditions. So nowadays it's much more difficult for people

88
00:11:54,760 --> 00:11:59,840
whose minds are consumed and if so much ignorance that we're not even able to tell that

89
00:11:59,840 --> 00:12:06,520
these are bad, these things are bad. That's much quicker and simpler for us to just

90
00:12:06,520 --> 00:12:24,560
muddle through it and see directly, oh yeah, causing myself harm. So good and bad are

91
00:12:24,560 --> 00:12:30,400
momentary, it's quite scary actually if you think about it our whole day, every day throughout

92
00:12:30,400 --> 00:12:41,560
our day we're doing countless unholesome deeds, lots of wholesome deeds as well, hopefully.

93
00:12:41,560 --> 00:12:48,800
But those deeds are just momentary means we have moments of wholesomeness and unholesomeness.

94
00:12:48,800 --> 00:12:55,240
Karma is only really scary when it becomes a habit, a chin to come, it's called or when

95
00:12:55,240 --> 00:12:59,960
it's extreme, when we let it get to the point where we do something, we have a moment

96
00:12:59,960 --> 00:13:10,880
that's just so unforgivable, like killing your parents or hurting a Buddha that kind of

97
00:13:10,880 --> 00:13:19,640
thing, dropping a rock on the Buddha. But that's when it's only really, you know, karma

98
00:13:19,640 --> 00:13:24,840
is not something to be afraid of, something to understand, something to go beyond really.

99
00:13:24,840 --> 00:13:29,560
It's also not something to get caught up in to think, I'm going to do lots of good karma

100
00:13:29,560 --> 00:13:40,560
in that'll make me happy. We have to go beyond karma by studying it and this is, so it

101
00:13:40,560 --> 00:13:44,920
is everywhere. So in everything we do when we give, there's good karma, there's bad karma

102
00:13:44,920 --> 00:13:50,120
when we kill, there's mostly bad karma, but there's still going to be some moments potentially

103
00:13:50,120 --> 00:14:03,040
of wholesomeness. It couldn't still be moments of wholesomeness. I don't want to get in

104
00:14:03,040 --> 00:14:14,560
trouble here with these radical ideas, but it seems to me that there would be moments.

105
00:14:14,560 --> 00:14:19,600
But the point is that only a meditator can truly understand this, when you're meditating

106
00:14:19,600 --> 00:14:25,120
able to see that which causes you suffering, that which causes you peace. I remember when

107
00:14:25,120 --> 00:14:33,600
I was hunting, when I was a teenager, and I was sitting up in a tree with a crow with

108
00:14:33,600 --> 00:14:40,280
a bow. It was the most awful thing to think of now, and I had to sit for a long time

109
00:14:40,280 --> 00:14:49,280
and wait, and I ended up being quite a spiritual experience. This bird came, landed really

110
00:14:49,280 --> 00:14:57,040
right above my head, and listening to the birds, floating by, watching the sunset, watching

111
00:14:57,040 --> 00:15:05,760
it get dark, just sitting alone in the forest up in a tree, starts to get dark. I didn't

112
00:15:05,760 --> 00:15:11,760
quite get, before I got dark, these deer came out into the clearing, and that was, oh,

113
00:15:11,760 --> 00:15:16,800
I'm going to get ready now, it's the time, but they turned out they were female and

114
00:15:16,800 --> 00:15:23,400
young, and you're not allowed to kill them, and you're a special permit. So, okay, I knew

115
00:15:23,400 --> 00:15:29,440
I was, and they came right up and they started eating from the tree, right? I was sitting

116
00:15:29,440 --> 00:15:34,120
and then I was sitting here, and I was looking into the eyes of this female deer, and

117
00:15:34,120 --> 00:15:40,920
she looked at me, and I looked at her, and she chewing, and it was awful to think back

118
00:15:40,920 --> 00:15:47,360
that that was the sort of thing I was interested in, but just to point out, karma is not

119
00:15:47,360 --> 00:15:55,760
so simple. You think of Angulimala, and all of his intent to kill, and then he ended

120
00:15:55,760 --> 00:16:01,240
up becoming an our hunt, not because of all the killing, but because during that time,

121
00:16:01,240 --> 00:16:08,720
he came to understand suffering. You could even argue that through doing evil deeds a

122
00:16:08,720 --> 00:16:14,120
person realizes how awful they are. Sometimes it takes doing an evil theme. Sometimes it

123
00:16:14,120 --> 00:16:18,600
takes someone to be addicted to drugs in order to know that drugs are wrong. I don't want

124
00:16:18,600 --> 00:16:23,200
to give the impression that there's nothing wrong with evil, and this is a good thing

125
00:16:23,200 --> 00:16:29,200
to do, but it's complicated. The Buddha said himself, understanding karma, only a Buddha

126
00:16:29,200 --> 00:16:37,040
can really understand it, and even the understanding that we have is his status. It's not

127
00:16:37,040 --> 00:16:41,480
so easy to understand. I talk about this. I thought it'd be good to talk about because

128
00:16:41,480 --> 00:16:46,000
I want to impress upon you that this is a good way of describing what we're doing in

129
00:16:46,000 --> 00:16:51,480
the practice. We're sorting it out. We're coming to see our habits, the karma that we've

130
00:16:51,480 --> 00:16:57,280
built up as habits, and realizing that some of it's not useful, not beneficial. Some of

131
00:16:57,280 --> 00:17:03,960
it's outright harmful. It's not even about asking whether this is a good deed or that

132
00:17:03,960 --> 00:17:10,000
it's a good deed. It's about understanding mind-states and seeing for ourselves what's

133
00:17:10,000 --> 00:17:15,680
a good mind-state, what's a bad mind-state. You can see clearly because this one leads

134
00:17:15,680 --> 00:17:23,000
to suffering, this one leads to happiness. It's undeniable and it doesn't change. You can

135
00:17:23,000 --> 00:17:28,080
ever get angry and then say, oh, that was fun. I'm angry and look at how much happiness

136
00:17:28,080 --> 00:17:36,200
that brought me. Greed will never make you satisfied. It doesn't have that. It has a very

137
00:17:36,200 --> 00:17:44,360
distinct nature that doesn't change. That's the law of karma. That's what we mean. It's

138
00:17:44,360 --> 00:17:54,040
a law. It might as well be a law because it doesn't ever change. Anyway, so in meditation

139
00:17:54,040 --> 00:18:06,080
just being mindful, watching, and seeing, we see three things. We see the defilement. We

140
00:18:06,080 --> 00:18:13,480
see the karma. We see the result. This is the wheel of karma. We've never heard of this.

141
00:18:13,480 --> 00:18:26,080
There's Kilesa, Kama, Vibhaka, or Jetana, Kilesa, Kama, Vibhaka. Then you have the unwholesome

142
00:18:26,080 --> 00:18:31,760
mind-states that leads you to do things. It leads you to kill. It leads you to steal.

143
00:18:31,760 --> 00:18:42,200
It's even to think bad thoughts. And then there's the result. So we see in meditation

144
00:18:42,200 --> 00:18:49,360
how different mind-states bring different results. It's chaotic. It's not like it's

145
00:18:49,360 --> 00:18:55,160
going to show it in some charge or something. You're going to be able to charge it. But

146
00:18:55,160 --> 00:19:02,800
you're going to send. You'll start to see thinking a lot. Thinking a lot gives me a headache.

147
00:19:02,800 --> 00:19:14,560
It has dilution. Frustration makes me tired. It makes me feel hot and sick inside. Greed

148
00:19:14,560 --> 00:19:23,880
makes me feel kind of dirty or greasy or unpleasant. Makes me feel agitated. Makes me feel

149
00:19:23,880 --> 00:19:38,440
hot as well. It burns us up inside. Then we start to naturally, naturally incline away

150
00:19:38,440 --> 00:19:49,880
from bad karma, but also away from trying to make good karma. As we start to understand

151
00:19:49,880 --> 00:19:56,720
karma more and more, we start to lose our ambitions. We start to lose the need, the drive

152
00:19:56,720 --> 00:20:02,600
to do this or that. But we become more content and more resolved. It's not that we lose our

153
00:20:02,600 --> 00:20:18,600
effort or become lazy. We become more intent on being, not becoming, not just being, not even

154
00:20:18,600 --> 00:20:29,040
being, but in the sense of stopping, staying put and becoming very interested and focused

155
00:20:29,040 --> 00:20:46,480
and all of our energy goes into stopping, into staying, into being, just being isn't

156
00:20:46,480 --> 00:20:54,800
the right word, but what it means is not, not being, not striving, not reaching. In the

157
00:20:54,800 --> 00:21:08,960
sense, striving to, striving to stop striving, striving to stop karma. Can you wait?

158
00:21:08,960 --> 00:21:17,600
Some scattered thoughts. Interesting, very interesting topic. And again, just want to think

159
00:21:17,600 --> 00:21:24,200
about how wonderful it is that meditation is taking off or has taken off and is really

160
00:21:24,200 --> 00:21:35,040
part of the world. You know, we think of the world as being full of problems, full of

161
00:21:35,040 --> 00:21:46,640
dangers, full of unwholesome is really. And there's a lot of, I guess, for lack of a better

162
00:21:46,640 --> 00:21:51,480
world, where there's a lot of evil in the world, if you look at it that way. I don't

163
00:21:51,480 --> 00:21:58,760
think it's so useful to dwell upon the evil. There's a lot of talk about activism and

164
00:21:58,760 --> 00:22:06,840
getting involved. I'm not going to say anything. I'm going to criticize it, but I think

165
00:22:06,840 --> 00:22:15,440
it's, there's room for arguably the, you know, the idea of focusing on the good, like

166
00:22:15,440 --> 00:22:23,760
Pollyanna, sort of, if you read the book, Pollyanna, I was in a book. Yeah. You know,

167
00:22:23,760 --> 00:22:28,960
sometimes if you focus on the good, it becomes your universe. And Buddhism, I think there's,

168
00:22:28,960 --> 00:22:36,880
there's room to talk about this sort of attitude. If we think about all the, all the

169
00:22:36,880 --> 00:22:41,640
meditation that goes on and we gain confidence and encouragement from that and we work

170
00:22:41,640 --> 00:22:47,480
and we focus all of our efforts, not on changing the world or correcting people's

171
00:22:47,480 --> 00:22:54,160
wrong views, but, but work to support people and to support the practice of meditation,

172
00:22:54,160 --> 00:23:01,480
to encourage others to meditate, encourage the cultivation of right view. I think this

173
00:23:01,480 --> 00:23:10,200
is what has, has had this great and lasting impact, bringing goodness to people. You know,

174
00:23:10,200 --> 00:23:18,880
it's never going to fix the world, but it's certainly a good way to live. I think that's

175
00:23:18,880 --> 00:23:26,840
the point. We can't fix the world. We can't change the world, but we can live well. We can

176
00:23:26,840 --> 00:23:35,160
set ourselves in what's right. We can set ourselves on the right path. And we can have

177
00:23:35,160 --> 00:23:43,160
our lives be an outpouring of goodness. There's this through many things, but really the

178
00:23:43,160 --> 00:23:49,520
core of it is we do it through meditation. So it's awesome to see so many people interested

179
00:23:49,520 --> 00:23:58,920
here and, and interested around the world. It sounds like from what we hear, we hear reports,

180
00:23:58,920 --> 00:24:06,880
more and more of people interested in meditation, mindfulness. There you go. That's the

181
00:24:06,880 --> 00:24:36,720
time of our tonight. Thank you all for tuning in.

