1
00:00:00,000 --> 00:00:05,840
Hello, everyone.

2
00:00:05,840 --> 00:00:10,120
This is Adder, a volunteer for Siri-Mungalo International.

3
00:00:10,120 --> 00:00:15,560
Since the breakout of the novel coronavirus as a global pandemic, our Center and Ontario

4
00:00:15,560 --> 00:00:19,160
Canada has been closed to meditators.

5
00:00:19,160 --> 00:00:25,360
We are pleased to announce that in accordance with Phase 2 of Ontario's reopening initiative,

6
00:00:25,360 --> 00:00:30,600
we will be opening the Center to incoming meditators in a limited capacity.

7
00:00:30,600 --> 00:00:35,720
In order to ensure physical distancing and individual washroom facilities, we are accepting

8
00:00:35,720 --> 00:00:41,440
a maximum of two meditators at a time, and will take precautions to minimize the risk

9
00:00:41,440 --> 00:00:44,400
of spreading the virus within the center.

10
00:00:44,400 --> 00:00:50,640
Due to international travel restrictions, only those currently residing in Canada may attend.

11
00:00:50,640 --> 00:00:55,040
Our volunteers have been making an effort to reach out to those who were previously scheduled

12
00:00:55,040 --> 00:00:56,600
for courses.

13
00:00:56,600 --> 00:01:02,360
If you would like to attend an in-person course, please visit our course page on our website

14
00:01:02,360 --> 00:01:05,880
at SiriMungalo.org slash courses.

15
00:01:05,880 --> 00:01:06,880
Thank you.

16
00:01:06,880 --> 00:01:29,600
May you be happy and peaceful.

