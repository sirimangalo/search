1
00:00:00,000 --> 00:00:23,060
Good evening everyone, welcome to our evening dumber.

2
00:00:23,060 --> 00:00:35,060
We are looking at purification through knowledge and vision.

3
00:00:35,060 --> 00:00:51,060
That's the name. Let's see official name. It's the ancient name of this last purification.

4
00:00:51,060 --> 00:01:01,060
So the sixth purification is called Patipada nyanadasana vesudhi.

5
00:01:01,060 --> 00:01:12,060
Patipada means practice. Nyanadasana vesudhi means not purification through knowledge and vision.

6
00:01:12,060 --> 00:01:20,060
So Patipada nyanadasana vesudhi. Number seven is just nyanadasana vesudhi.

7
00:01:20,060 --> 00:01:25,060
It's the same name. It's just got Patipada removed.

8
00:01:25,060 --> 00:01:31,060
Patipada.

9
00:01:31,060 --> 00:01:39,060
Which I guess is to just emphasize that this knowledge and vision is the real knowledge and vision.

10
00:01:39,060 --> 00:01:47,060
It's the one that counts. It's the one that means something.

11
00:01:47,060 --> 00:01:56,060
It's knowledge and vision that leads to that constitutes freedom.

12
00:01:56,060 --> 00:02:03,060
So there are five kinds of freedom in Buddhism.

13
00:02:03,060 --> 00:02:20,060
This purification is the most important and it's ironically it's the one we have the least to say about.

14
00:02:20,060 --> 00:02:40,060
At least to talk about because it's pretty much beyond conception.

15
00:02:40,060 --> 00:02:55,060
It's hard to understand. So we talk about it in context, the context of the five types of freedom.

16
00:02:55,060 --> 00:03:11,060
The first type of freedom is called, we come binamimudhi. We come binamim suppression.

17
00:03:11,060 --> 00:03:13,060
Freedom through suppression.

18
00:03:13,060 --> 00:03:19,060
And we're talking about freedom. We mean freedom from suffering.

19
00:03:19,060 --> 00:03:28,060
This one should be familiar to us. We cover up, we come binam, it's covering up.

20
00:03:28,060 --> 00:03:37,060
We have lots of ways of covering up, avoiding the problem.

21
00:03:37,060 --> 00:03:54,060
We immerse ourselves in things and diversion, play or work, relationships, externalizing.

22
00:03:54,060 --> 00:04:03,060
Get caught up in society, get a job, get a career, anything to have to face yourself.

23
00:04:03,060 --> 00:04:15,060
Or we become more and more inclined in society to avoid ourselves.

24
00:04:15,060 --> 00:04:22,060
We are taught to in many ways externalize and a person who spends too much time with themselves.

25
00:04:22,060 --> 00:04:31,060
There must be something wrong with them because no one, you know, this isn't the way one should live one's life.

26
00:04:31,060 --> 00:04:37,060
We're obsessing over one's own mind, right?

27
00:04:37,060 --> 00:04:48,060
We're expected to get beyond our problems, to deal with our problems and move on.

28
00:04:48,060 --> 00:04:53,060
As though dealing with our problems was something quite preliminary.

29
00:04:53,060 --> 00:05:03,060
Yes, yes, you're fine, now go and do what's really important.

30
00:05:03,060 --> 00:05:12,060
As a result, we walk around and we see everyone walking around with smiles on their faces, laughing and joking.

31
00:05:12,060 --> 00:05:22,060
And inside screaming and yelling and helped me help me because we're all lost.

32
00:05:22,060 --> 00:05:27,060
It's quite depressing because it seems like everyone else is so happy.

33
00:05:27,060 --> 00:05:38,060
I'm not happy why is everyone else so happy and so we put on a good act.

34
00:05:38,060 --> 00:05:43,060
We even convince ourselves that we're happy.

35
00:05:43,060 --> 00:05:59,060
We lower our standards, we've been beat down so much that we learn ways to cope and we accept a very limited sort of happiness,

36
00:05:59,060 --> 00:06:03,060
limited sort of freedom.

37
00:06:03,060 --> 00:06:11,060
But there are ways, there are systematic ways of suppressing your problems and the Buddha taught these.

38
00:06:11,060 --> 00:06:19,060
They're actually quite useful in a temporary sense.

39
00:06:19,060 --> 00:06:27,060
We're all these ways of suppressing in the world, but the highest form of suppression is through what we call the Johnus.

40
00:06:27,060 --> 00:06:40,060
The summit of Johnus, where one tranquilizes the mind, calms the mind down, fixes and focuses the mind on a single object.

41
00:06:40,060 --> 00:06:46,060
The mind is fixed and focused, all of the defilements are suppressed.

42
00:06:46,060 --> 00:06:56,060
Very useful in the short term, not so useful in the long term, not the goal, not true of freedom.

43
00:06:56,060 --> 00:07:13,060
The second type of purification of freedom is, the second type of freedom is Tadhanga, we mutty, which is through opposites.

44
00:07:13,060 --> 00:07:29,060
And this one should be familiar to Buddhists, this idea that there are certain types of meditation that redirect the mind when one is angry, one cultivates.

45
00:07:29,060 --> 00:07:58,060
One is lustful or passionate, one cultivates dispassion through looking at the body, for example, and seeing how the body is not something beautiful or wonderful, and it's made up of icky stuff, fear, conquer fear by mindfulness of the Buddha, for example.

46
00:07:58,060 --> 00:08:10,060
All the way down to, so these are also quite useful, and they're useful beyond just suppressing, they're useful for retraining the mind, looking at the world differently.

47
00:08:10,060 --> 00:08:14,060
You don't like this person, we'll learn to love them, it does change you.

48
00:08:14,060 --> 00:08:22,060
It's not just about suppressing the anger, it's about finding a new way to look at the world, and this is conventionally useful.

49
00:08:22,060 --> 00:08:28,060
It's actually quite supportive of our practice, the Buddha certainly taught several meditations like this.

50
00:08:28,060 --> 00:08:37,060
The four Brahma Mihara's are a good example.

51
00:08:37,060 --> 00:08:52,060
And the highest form of opposites, of freedom through opposites, through opposition, is the seeing the three characteristics.

52
00:08:52,060 --> 00:09:01,060
We oppose the delusion, we oppose the delusion through wisdom, which means the delusion of permanent stability.

53
00:09:01,060 --> 00:09:20,060
I'll hold on to this because it will last, it will protect me or satisfy me or stay with me, be mine, overcome that through seeing impermanence.

54
00:09:20,060 --> 00:09:33,060
So through seeing suffering, seeing that things can't satisfy us, seeing that they're unsatisfying, that they're stressful, clinging to them is stress.

55
00:09:33,060 --> 00:09:38,060
It's an important point because all that we've talked about up until now is just this, is the opposition.

56
00:09:38,060 --> 00:09:41,060
It's not actually true of freedom.

57
00:09:41,060 --> 00:09:48,060
You can get all the way through the repassant of knowledge and go back to being a terrible person, not in this lifetime.

58
00:09:48,060 --> 00:09:53,060
But certainly in lifetimes to come, it's not a guarantee.

59
00:09:53,060 --> 00:09:55,060
It's a guarantee in this lifetime apparently.

60
00:09:55,060 --> 00:10:02,060
They say if you just make it to the second stage of knowledge, here what's called a tula-sotapana.

61
00:10:02,060 --> 00:10:07,060
Tula means little, and soapana means one who has found the stream.

62
00:10:07,060 --> 00:10:17,060
And why the term is used is because apparently a person who's reached the second of 16 stages of knowledge.

63
00:10:17,060 --> 00:10:24,060
In this lifetime, when they die from this life, they won't be born in any of the states of loss.

64
00:10:24,060 --> 00:10:35,060
So it's like a sotapana because the sotapana will never be born in hell or as an animal or so on ever.

65
00:10:35,060 --> 00:10:41,060
But a tula-sotapana in this lifetime for sure, because they've understood karma, they've understood cause and effect,

66
00:10:41,060 --> 00:10:48,060
they've created such wholesomeness that it protects them.

67
00:10:48,060 --> 00:10:56,060
But unless that knowledge can go away, you're not in this lifetime, in future lifetimes.

68
00:10:56,060 --> 00:11:00,060
Most definitely, it's just called the danga vimutians.

69
00:11:00,060 --> 00:11:07,060
Through seeing clearly, you free yourself from delusion obviously.

70
00:11:07,060 --> 00:11:11,060
It's like when you shine light in, the darkness goes away.

71
00:11:11,060 --> 00:11:15,060
But if you take the light away, the darkness, of course, comes back.

72
00:11:15,060 --> 00:11:22,060
The danga vimutianu.

73
00:11:22,060 --> 00:11:24,060
Samu-cheda vimutianu.

74
00:11:24,060 --> 00:11:27,060
Samu-cheda means cheda is cut.

75
00:11:27,060 --> 00:11:30,060
The root cheda means cutting.

76
00:11:30,060 --> 00:11:33,060
To cut in regards to cutting.

77
00:11:33,060 --> 00:11:38,060
Samu-cheda means totally.

78
00:11:38,060 --> 00:11:42,060
I think it means off in this case.

79
00:11:42,060 --> 00:11:46,060
Samu-cheda means totally cutting off.

80
00:11:46,060 --> 00:11:48,060
Real freedom.

81
00:11:48,060 --> 00:11:52,060
It's like burning your bridge, you can't go back.

82
00:11:52,060 --> 00:11:56,060
People are scared of nibana because,

83
00:11:56,060 --> 00:11:59,060
what if I don't like it there?

84
00:11:59,060 --> 00:12:02,060
What if I want to go back?

85
00:12:02,060 --> 00:12:04,060
No, there's no going back.

86
00:12:04,060 --> 00:12:07,060
Scary, no?

87
00:12:07,060 --> 00:12:09,060
It's not scary.

88
00:12:09,060 --> 00:12:11,060
It's quite understandable.

89
00:12:11,060 --> 00:12:12,060
It's not a mistake.

90
00:12:12,060 --> 00:12:13,060
You can't say whoops.

91
00:12:13,060 --> 00:12:14,060
I made a mistake.

92
00:12:14,060 --> 00:12:15,060
Let me back.

93
00:12:15,060 --> 00:12:19,060
It's not because it's the depth by definition.

94
00:12:19,060 --> 00:12:22,060
It's through perfect sight.

95
00:12:22,060 --> 00:12:25,060
Not just this wisdom that we've been talking about,

96
00:12:25,060 --> 00:12:29,060
but this is where we get into the seventh purification.

97
00:12:29,060 --> 00:12:33,060
Knowledge and vision.

98
00:12:33,060 --> 00:12:34,060
You know.

99
00:12:34,060 --> 00:12:37,060
You can't accidentally let go of something.

100
00:12:37,060 --> 00:12:39,060
It's not possible.

101
00:12:39,060 --> 00:12:41,060
You can't mistakenly, and then later decide,

102
00:12:41,060 --> 00:12:44,060
oh no, that was really good for me.

103
00:12:44,060 --> 00:12:48,060
You can't even let go of it if you still have any potential to think

104
00:12:48,060 --> 00:12:51,060
it's good for you.

105
00:12:51,060 --> 00:12:53,060
The way you can suppress.

106
00:12:53,060 --> 00:12:59,060
These are the first two types of freedom.

107
00:12:59,060 --> 00:13:04,060
You can force yourself to give things up.

108
00:13:04,060 --> 00:13:08,060
I don't want to feel this depression,

109
00:13:08,060 --> 00:13:10,060
so I'll take the pill,

110
00:13:10,060 --> 00:13:13,060
and now I'm not depressed again.

111
00:13:13,060 --> 00:13:17,060
You can pretend to be very happy.

112
00:13:17,060 --> 00:13:22,060
Look at how happy I am, and you can suppress the unhappiness.

113
00:13:22,060 --> 00:13:25,060
Totally possible.

114
00:13:25,060 --> 00:13:28,060
But that's not how need by nowhere.

115
00:13:28,060 --> 00:13:31,060
That's not sustainable by any means.

116
00:13:31,060 --> 00:13:35,060
It's not real freedom.

117
00:13:35,060 --> 00:13:38,060
Some more che-do-imoutes totally cut off,

118
00:13:38,060 --> 00:13:42,060
because you know, you see.

119
00:13:42,060 --> 00:13:46,060
I can't explain why it is that way.

120
00:13:46,060 --> 00:13:48,060
It just is that way.

121
00:13:48,060 --> 00:13:51,060
When you see something so clearly,

122
00:13:51,060 --> 00:13:54,060
it's like a tipping point.

123
00:13:54,060 --> 00:13:57,060
Or, you know, they like in it to rubbing two sticks together,

124
00:13:57,060 --> 00:13:59,060
and then eventually lights on fire.

125
00:13:59,060 --> 00:14:02,060
But I think a tipping point makes a little more sense,

126
00:14:02,060 --> 00:14:05,060
because you're pushing, pushing, pushing, pushing, pushing,

127
00:14:05,060 --> 00:14:09,060
and the booty is this kind of imagery as well.

128
00:14:09,060 --> 00:14:11,060
You get to the tipping point,

129
00:14:11,060 --> 00:14:14,060
and then you don't have to push anymore.

130
00:14:14,060 --> 00:14:18,060
It's going, it's going to fall in that direction.

131
00:14:18,060 --> 00:14:20,060
It's going to fall.

132
00:14:20,060 --> 00:14:24,060
You get to the tipping point.

133
00:14:24,060 --> 00:14:26,060
Well, be it as it may.

134
00:14:26,060 --> 00:14:31,060
In the mind, what happens is there comes this aha moment.

135
00:14:31,060 --> 00:14:35,060
So it's kind of like rubbing two sticks together in the ignition.

136
00:14:35,060 --> 00:14:38,060
But it's more like the straw that broke the camel's back,

137
00:14:38,060 --> 00:14:41,060
as they say, in terms of knowledge.

138
00:14:41,060 --> 00:14:45,060
Like, you see, okay, yes, it's impermanent.

139
00:14:45,060 --> 00:14:47,060
Yes, it's impermanent.

140
00:14:47,060 --> 00:14:48,060
It's impermanent.

141
00:14:48,060 --> 00:14:50,060
It's impermanent.

142
00:14:50,060 --> 00:14:54,060
And it's like a critical mass,

143
00:14:54,060 --> 00:14:57,060
like a feedback loop that just gets stronger

144
00:14:57,060 --> 00:14:59,060
and stronger and stronger and stronger,

145
00:14:59,060 --> 00:15:02,060
and like a sonic boom almost.

146
00:15:02,060 --> 00:15:05,060
The sound waves build up and build up the knowledge,

147
00:15:05,060 --> 00:15:07,060
builds up and builds up until the mind snaps,

148
00:15:07,060 --> 00:15:10,060
and says, I can get it.

149
00:15:10,060 --> 00:15:12,060
It's impermanent.

150
00:15:12,060 --> 00:15:14,060
Well, it's not even a thought, right?

151
00:15:14,060 --> 00:15:17,060
It's at that point the mind snaps.

152
00:15:17,060 --> 00:15:19,060
And it lets go.

153
00:15:19,060 --> 00:15:22,060
It's like, right?

154
00:15:22,060 --> 00:15:25,060
Mind, completely, let's go.

155
00:15:25,060 --> 00:15:27,060
Even just for a moment.

156
00:15:31,060 --> 00:15:35,060
And that moment is called,

157
00:15:35,060 --> 00:15:37,060
some would change the remote team.

158
00:15:37,060 --> 00:15:41,060
It doesn't even, even by definition,

159
00:15:41,060 --> 00:15:43,060
it's hard to understand why that is.

160
00:15:43,060 --> 00:15:45,060
Okay, so what happened?

161
00:15:45,060 --> 00:15:47,060
You let go for a moment,

162
00:15:47,060 --> 00:15:49,060
just a moment, right?

163
00:15:49,060 --> 00:15:51,060
But that letting go,

164
00:15:51,060 --> 00:15:53,060
we'll see, and we'll talk about,

165
00:15:53,060 --> 00:15:55,060
because when we talk about the other two,

166
00:15:55,060 --> 00:15:57,060
this is the third type of freedom.

167
00:15:57,060 --> 00:16:01,060
It's freedom because of what comes next.

168
00:16:01,060 --> 00:16:04,060
It's the freedom of the path.

169
00:16:04,060 --> 00:16:07,060
It's called the path because it's what does the work,

170
00:16:07,060 --> 00:16:11,060
because of what comes next.

171
00:16:11,060 --> 00:16:13,060
So this moment where this realization

172
00:16:13,060 --> 00:16:16,060
where finally you see impermanence

173
00:16:16,060 --> 00:16:17,060
or suffering or non-self,

174
00:16:17,060 --> 00:16:19,060
it's one of the three characteristics

175
00:16:19,060 --> 00:16:21,060
will become very clear just in a moment.

176
00:16:21,060 --> 00:16:23,060
And it's just like the last,

177
00:16:23,060 --> 00:16:25,060
the final straw.

178
00:16:25,060 --> 00:16:27,060
You're done.

179
00:16:27,060 --> 00:16:29,060
I'm out of here.

180
00:16:29,060 --> 00:16:33,060
Mind, let's go, leaves.

181
00:16:33,060 --> 00:16:36,060
That's number three.

182
00:16:36,060 --> 00:16:38,060
Number four is the next moment.

183
00:16:38,060 --> 00:16:40,060
And moments that come after it

184
00:16:40,060 --> 00:16:44,060
where the mind experiences nibana.

185
00:16:44,060 --> 00:16:48,060
That's how we describe what happens,

186
00:16:48,060 --> 00:16:52,060
how we label what is happening.

187
00:16:52,060 --> 00:16:54,060
It's misleading because it's not like suddenly,

188
00:16:54,060 --> 00:16:56,060
ooh, I'm in nibana.

189
00:16:56,060 --> 00:16:58,060
It's not like that.

190
00:16:58,060 --> 00:17:01,060
Experience is nibana means experiences unbinding.

191
00:17:01,060 --> 00:17:04,060
It's like a release.

192
00:17:04,060 --> 00:17:06,060
So there's no seeing, no hearing,

193
00:17:06,060 --> 00:17:08,060
no smelling, no tasting, no feeling,

194
00:17:08,060 --> 00:17:12,060
and no thinking, not even any consciousness

195
00:17:12,060 --> 00:17:14,060
in the sense that we understand it,

196
00:17:14,060 --> 00:17:17,060
in the sense that we would be aware,

197
00:17:17,060 --> 00:17:20,060
not any consciousness in that sense.

198
00:17:20,060 --> 00:17:29,060
And that can last a long time.

199
00:17:29,060 --> 00:17:31,060
It can last up to seven days,

200
00:17:31,060 --> 00:17:36,060
apparently.

201
00:17:36,060 --> 00:17:39,060
And that's called,

202
00:17:39,060 --> 00:17:41,060
and this is the key.

203
00:17:41,060 --> 00:17:43,060
This one is the key.

204
00:17:43,060 --> 00:17:45,060
I mean, the last one is the key,

205
00:17:45,060 --> 00:17:49,060
but this one is called the fruit.

206
00:17:49,060 --> 00:17:52,060
This is the experience.

207
00:17:52,060 --> 00:17:54,060
The path is what cuts off,

208
00:17:54,060 --> 00:17:57,060
but this is the cut-off point,

209
00:17:57,060 --> 00:17:58,060
or the cut-off period,

210
00:17:58,060 --> 00:18:01,060
where you've been cut off.

211
00:18:01,060 --> 00:18:06,060
But the past said he removed him.

212
00:18:06,060 --> 00:18:07,060
Number four.

213
00:18:07,060 --> 00:18:09,060
And then the mind comes back,

214
00:18:09,060 --> 00:18:12,060
and of course then there's more stress

215
00:18:12,060 --> 00:18:14,060
and what we would call suffering,

216
00:18:14,060 --> 00:18:19,060
but in the sense of more existence.

217
00:18:19,060 --> 00:18:22,060
But something's changed.

218
00:18:22,060 --> 00:18:23,060
Why it's cutting off?

219
00:18:23,060 --> 00:18:26,060
Why we call that third one cutting off

220
00:18:26,060 --> 00:18:28,060
is because when you come back,

221
00:18:28,060 --> 00:18:36,060
you have a knowledge that something happened.

222
00:18:36,060 --> 00:18:38,060
That experience, I mean,

223
00:18:38,060 --> 00:18:42,060
there's not even really a thought about it,

224
00:18:42,060 --> 00:18:45,060
but there's this piece in your mind.

225
00:18:45,060 --> 00:18:52,060
There's this profound and new piece.

226
00:18:52,060 --> 00:18:57,060
You feel like a new person in many ways.

227
00:18:57,060 --> 00:19:00,060
And then you see, oh, I've still got lots of work left to do,

228
00:19:00,060 --> 00:19:05,060
but something's changed.

229
00:19:05,060 --> 00:19:07,060
It's cut off because you've seen,

230
00:19:07,060 --> 00:19:15,060
you've experienced for yourself true freedom.

231
00:19:15,060 --> 00:19:21,060
You've experienced what is outside of some sara.

232
00:19:21,060 --> 00:19:23,060
And so by comparison,

233
00:19:23,060 --> 00:19:26,060
some sara loses all of its color,

234
00:19:26,060 --> 00:19:28,060
all of its attraction,

235
00:19:28,060 --> 00:19:30,060
or some of its attraction,

236
00:19:30,060 --> 00:19:32,060
over time,

237
00:19:32,060 --> 00:19:36,060
through repeated experiences of the alternative.

238
00:19:36,060 --> 00:19:41,060
More and more of our attachment to some sara is cut off.

239
00:19:41,060 --> 00:19:43,060
Not by accident,

240
00:19:43,060 --> 00:19:47,060
but by undeniable understanding

241
00:19:47,060 --> 00:19:51,060
that there's something better.

242
00:19:51,060 --> 00:19:58,060
Until finally, one frees oneself from all attachment to some sara,

243
00:19:58,060 --> 00:20:01,060
and attains the fifth of freedom,

244
00:20:01,060 --> 00:20:04,060
which is called nisadurnumumutim,

245
00:20:04,060 --> 00:20:07,060
which is through escape.

246
00:20:07,060 --> 00:20:12,060
Once one is completely free from any attachment to anything,

247
00:20:12,060 --> 00:20:16,060
then there is complete freedom.

248
00:20:16,060 --> 00:20:21,060
So the seventh stage of knowledge is these last three.

249
00:20:21,060 --> 00:20:23,060
It's knowledge and vision.

250
00:20:23,060 --> 00:20:27,060
It's this claim that Buddhism makes that

251
00:20:27,060 --> 00:20:29,060
some sara is not the end,

252
00:20:29,060 --> 00:20:31,060
not the be-all end-all.

253
00:20:31,060 --> 00:20:36,060
It's not the final form.

254
00:20:36,060 --> 00:20:39,060
It's not the ultimate state.

255
00:20:39,060 --> 00:20:42,060
Some sara is like a school.

256
00:20:42,060 --> 00:20:45,060
We're here until you graduate.

257
00:20:45,060 --> 00:20:48,060
It's a claim that we make.

258
00:20:48,060 --> 00:20:51,060
Either way, it's true.

259
00:20:51,060 --> 00:20:57,060
It's considered to be the best way to look at life as a school,

260
00:20:57,060 --> 00:20:59,060
as a learning experience,

261
00:20:59,060 --> 00:21:04,060
because clearly you can become a better person,

262
00:21:04,060 --> 00:21:06,060
and you can become a worse person,

263
00:21:06,060 --> 00:21:10,060
meaning you can cultivate happiness

264
00:21:10,060 --> 00:21:11,060
and freedom from suffering,

265
00:21:11,060 --> 00:21:13,060
and you can cultivate suffering

266
00:21:13,060 --> 00:21:16,060
and unhappiness.

267
00:21:16,060 --> 00:21:19,060
So clearly learning how to be happy

268
00:21:19,060 --> 00:21:21,060
and free from suffering is

269
00:21:21,060 --> 00:21:27,060
but can't be anything better than that.

270
00:21:27,060 --> 00:21:30,060
And so it's just a matter of understanding the implications

271
00:21:30,060 --> 00:21:32,060
of what that means,

272
00:21:32,060 --> 00:21:35,060
that it's not some simple,

273
00:21:35,060 --> 00:21:38,060
mundane, short-term goal,

274
00:21:38,060 --> 00:21:40,060
freedom from suffering.

275
00:21:40,060 --> 00:21:44,060
It's a long path that leans right out.

276
00:21:44,060 --> 00:21:45,060
It really happened.

277
00:21:45,060 --> 00:21:47,060
There really is.

278
00:21:47,060 --> 00:21:50,060
There is freedom from suffering.

279
00:21:50,060 --> 00:21:51,060
It exists.

280
00:21:51,060 --> 00:21:56,060
It is real.

281
00:21:56,060 --> 00:22:02,060
And that's the final purification.

282
00:22:02,060 --> 00:22:06,060
So not a lot more to say about it than that.

283
00:22:06,060 --> 00:22:08,060
Thank you all for tuning in.

284
00:22:08,060 --> 00:22:29,060
It's the demo for tonight.

285
00:22:29,060 --> 00:22:32,060
Let's go look at the questions page.

286
00:22:32,060 --> 00:22:34,060
Did it get fixed?

287
00:22:34,060 --> 00:22:37,060
Looks like it got fixed.

288
00:22:37,060 --> 00:22:39,060
It's just working.

289
00:22:39,060 --> 00:22:42,060
Anapanaseti has 16 steps,

290
00:22:42,060 --> 00:22:45,060
repeated to 70,

291
00:22:45,060 --> 00:22:49,060
and it's possible to not processing an order.

292
00:22:49,060 --> 00:22:55,060
I don't teach that form of anapanaseti.

293
00:22:55,060 --> 00:22:57,060
Can we do a simulation of relinquishment

294
00:22:57,060 --> 00:23:00,060
before actually getting there?

295
00:23:00,060 --> 00:23:03,060
No.

296
00:23:03,060 --> 00:23:06,060
No, there's no such thing as a simulation.

297
00:23:06,060 --> 00:23:10,060
I mean, in some sense you can live like a noble being,

298
00:23:10,060 --> 00:23:15,060
but really quite sure what you're talking about.

299
00:23:15,060 --> 00:23:17,060
We have a mainstream entry.

300
00:23:17,060 --> 00:23:18,060
Does that mean in future birth,

301
00:23:18,060 --> 00:23:21,060
the first three fetters are already broken?

302
00:23:21,060 --> 00:23:23,060
No, they're already broken.

303
00:23:23,060 --> 00:23:30,060
They will be broken every lifetime to come.

304
00:23:30,060 --> 00:23:34,060
Can meditation solve any problem?

305
00:23:34,060 --> 00:23:37,060
I'm sure you could probably theoretically think of a problem

306
00:23:37,060 --> 00:23:40,060
that it couldn't solve.

307
00:23:40,060 --> 00:23:44,060
Meditation and the word meditation means many different things.

308
00:23:44,060 --> 00:23:47,060
Obviously the mind is the most powerful tool we have,

309
00:23:47,060 --> 00:23:51,060
so it's hard to imagine a problem that meditation of some sort

310
00:23:51,060 --> 00:23:55,060
couldn't solve, but theoretically there are problems.

311
00:23:55,060 --> 00:24:00,060
I'll give you a theoretically a problem that is unsolvable.

312
00:24:00,060 --> 00:24:04,060
How do we move an immovable object?

313
00:24:04,060 --> 00:24:08,060
Meditation can't solve that problem.

314
00:24:08,060 --> 00:24:14,060
But, I mean, practically speaking,

315
00:24:14,060 --> 00:24:17,060
things that are problems to us, yes.

316
00:24:17,060 --> 00:24:21,060
Yes, and in fact, let's look at the immovable object problem.

317
00:24:21,060 --> 00:24:23,060
It's only a problem because you want to move it.

318
00:24:23,060 --> 00:24:25,060
If you stopped wanting to move it,

319
00:24:25,060 --> 00:24:26,060
there would be no problem.

320
00:24:26,060 --> 00:24:28,060
And meditation does that for you,

321
00:24:28,060 --> 00:24:30,060
even that problem is solved.

322
00:24:30,060 --> 00:24:32,060
I mean, meditation doesn't solve your problems.

323
00:24:32,060 --> 00:24:34,060
The meditation that we practice.

324
00:24:34,060 --> 00:24:37,060
Meditation helps you see that your problems are not problems

325
00:24:37,060 --> 00:24:40,060
and the problem is that you think they're problems.

326
00:24:40,060 --> 00:24:44,060
And when you let go of that, then you have no problem.

327
00:24:44,060 --> 00:24:51,060
So, yes, pretty much can solve for you from problems

328
00:24:51,060 --> 00:25:02,060
because it stopped looking at the most problems.

329
00:25:02,060 --> 00:25:11,060
Well, that was a few questions.

330
00:25:11,060 --> 00:25:13,060
Can't get rid of this last one, but that's all.

331
00:25:13,060 --> 00:25:16,060
Thank you all for coming out.

332
00:25:16,060 --> 00:25:20,060
Have a good night.

333
00:25:20,060 --> 00:25:22,060
Thank you.

