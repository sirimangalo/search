1
00:00:00,000 --> 00:00:09,960
I got a question for you Lucas Lukas is the is truth within

2
00:00:10,960 --> 00:00:15,920
I've got this theme. It's not a trick question. I'm just asking. Do you think truth is within?

3
00:00:18,520 --> 00:00:20,520
Wow, I mean

4
00:00:21,840 --> 00:00:23,840
Yeah, you could say but you want to

5
00:00:23,840 --> 00:00:28,720
About like even deeper onto it to it. No, not just yes, just I guess

6
00:00:29,480 --> 00:00:31,480
Hopefully yes, or no

7
00:00:32,760 --> 00:00:39,640
I think truth I think I think the only thing you want to achieve and we want to be is

8
00:00:40,480 --> 00:00:42,480
Summer inside you

9
00:00:42,480 --> 00:00:49,960
Okay, I'd just rather than outside. I actually have another question because someone asked this recently if truth is within and so we're

10
00:00:49,960 --> 00:00:56,680
I'm gonna just assume that it is if truth is it in then why do we have to look outside of ourselves to learn that

11
00:00:58,640 --> 00:01:00,640
This is what someone asked me

12
00:01:01,480 --> 00:01:03,480
Wow

13
00:01:05,800 --> 00:01:08,000
There is a thing that we actually

14
00:01:08,000 --> 00:01:18,920
It's right well because there is a theory that we actually create this reality with the lower minds

15
00:01:19,920 --> 00:01:23,200
You know in a broad explanation that

16
00:01:24,960 --> 00:01:31,160
We talked a few times about it about the reality and things so

17
00:01:34,040 --> 00:01:36,040
We don't know actually

18
00:01:36,040 --> 00:01:40,760
Why do they say that we need to look outside to to find it?

19
00:01:43,280 --> 00:01:45,280
I guess they mean I

20
00:01:45,280 --> 00:01:50,080
Guess their point was that truth isn't inside truth is in something outside like maybe God

21
00:01:51,920 --> 00:01:56,720
So they're showing how absurd Buddhism is and this is what they said Buddhism is is absurd because

22
00:01:58,240 --> 00:02:00,960
You know in order to learn that truth is is

23
00:02:00,960 --> 00:02:07,560
Inside you have to find that truth outside you have to have someone tell you truth is within

24
00:02:08,160 --> 00:02:11,320
That's because you were looking outside to start with

25
00:02:13,360 --> 00:02:18,080
You had to start looking inside you would have found it yourself

26
00:02:18,240 --> 00:02:23,640
But there's I think there's another answer because you can look inside and not know how to see not know how to look right

27
00:02:24,080 --> 00:02:26,160
You can you can look inside and

28
00:02:26,160 --> 00:02:30,600
And guidance you know us. It's a problem

29
00:02:32,080 --> 00:02:36,680
Funny thing is that if you look all outside you look through your inside

30
00:02:38,160 --> 00:02:42,440
Okay, so as everything you experience experience through yourself

31
00:02:43,040 --> 00:02:49,680
So it's it's kind of like the same thing you cannot experience outside without inside

32
00:02:50,280 --> 00:02:52,600
Cannot experience inside without outside

33
00:02:52,600 --> 00:02:55,280
You know I'm trying to say that

34
00:02:56,400 --> 00:02:58,400
you're kind of

35
00:03:00,160 --> 00:03:02,160
Working together

36
00:03:02,160 --> 00:03:04,160
That's the one one device

37
00:03:05,760 --> 00:03:07,760
It's like

38
00:03:07,760 --> 00:03:16,760
If you don't know eyes how it can investigate outside know the up to your eyes and what you think is inside

39
00:03:16,760 --> 00:03:22,760
You know, I mean you see something and you see about it

40
00:03:24,320 --> 00:03:26,320
I think the whole you know

41
00:03:26,720 --> 00:03:31,000
First of all the whole purpose of telling people to look inside is as Clemente said

42
00:03:31,760 --> 00:03:37,120
It's telling people who are looking outside and right it's telling people who are looking for God or looking to

43
00:03:37,800 --> 00:03:43,360
Materialism or looking for some truth. They're happiness outside themselves telling them to look back in well

44
00:03:43,360 --> 00:03:48,360
How can you how can you do that without relying on the external right you have to do that externally?

45
00:03:48,960 --> 00:03:52,800
Yeah, because there's a kind of thing that we are separated from outside

46
00:03:52,800 --> 00:03:54,480
But we are not

47
00:03:54,480 --> 00:04:01,200
I mean the reason the reason why I thought of this question was was to do with this some idea of of helping people for

48
00:04:03,640 --> 00:04:07,080
Helping people to look inside helping people with nervousness the question right?

49
00:04:07,080 --> 00:04:13,880
You know, there's the two aspects if you can tell a person well just see it like this and it's in this but

50
00:04:14,280 --> 00:04:17,800
But if they could they would have right so they need the the guidance

51
00:04:18,480 --> 00:04:22,520
What there is the aspect that's needed is some sort of guidance

52
00:04:22,720 --> 00:04:27,920
You have to give them a means to do that whatever it is in of course in our tradition

53
00:04:27,920 --> 00:04:32,720
We'll have them say nervous nervous or there if someone's talking to them hearing hearing

54
00:04:32,720 --> 00:04:39,600
What I'm feeling the heart beat and the tension in the stomach and pain and the head and so

55
00:04:40,320 --> 00:04:45,320
Being aware of their movements. I don't want to I don't want to I don't want to give it in fine

56
00:04:46,320 --> 00:04:48,320
in my opinion

57
00:04:49,120 --> 00:04:55,320
Oh, maybe I shouldn't but in some cases taking drugs is a good way to go

58
00:04:55,720 --> 00:04:57,720
So what's your confidence?

59
00:04:57,720 --> 00:04:59,720
If you experience

60
00:05:01,440 --> 00:05:05,360
I mean if you experience out of logic here, okay

61
00:05:06,360 --> 00:05:11,160
You might you might get some audience now like

62
00:05:12,720 --> 00:05:16,920
That's my that's only my opinion like if it's just

63
00:05:16,920 --> 00:05:33,640
Like drugs mentioned many people work this word or what tune in turn on I'm trying to

64
00:05:36,320 --> 00:05:42,800
More a little bit more specific which drugs do you particularly recommend

65
00:05:42,800 --> 00:05:47,480
done now it's like let's say like try to

66
00:05:48,120 --> 00:05:50,120
That's something try to

67
00:05:50,120 --> 00:05:52,120
try to know to try to

68
00:05:53,760 --> 00:05:54,760
Try to try to

69
00:05:54,760 --> 00:06:04,240
try to get down and go into public and talk to them and and let's say let's see how much insecurity you are and see how

70
00:06:05,040 --> 00:06:07,040
ridiculous that is

71
00:06:07,040 --> 00:06:12,240
You know what that sounds like it's a baton prock if

72
00:06:17,960 --> 00:06:19,960
It's done

73
00:06:19,960 --> 00:06:49,200
All right, I think we're gonna quit there while we're only modern modesty behind

