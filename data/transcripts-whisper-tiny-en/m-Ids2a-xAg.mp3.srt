1
00:00:00,000 --> 00:00:27,520
Okay, good evening everybody, broadcasting live February 21st.

2
00:00:27,520 --> 00:00:48,080
Ladies' quote is on the fruits of goodness, the fruits of good deeds.

3
00:00:48,080 --> 00:01:10,520
I was asked today about the problem to do with being a good person, and I get asked this often.

4
00:01:10,520 --> 00:01:24,240
If you're doing good deeds, if you don't, react, you don't stand up for yourself.

5
00:01:24,240 --> 00:01:38,360
If you don't hold on to the things that hold on to the things, then you risk getting

6
00:01:38,360 --> 00:01:45,280
trod upon, and the people put the walk all over you.

7
00:01:45,280 --> 00:01:50,400
How do you stop people from walking all over you?

8
00:01:50,400 --> 00:02:04,000
It's a sphere that we have, it's reasonable, because our hold on many things as human beings

9
00:02:04,000 --> 00:02:15,280
is tenuous.

10
00:02:15,280 --> 00:02:23,520
The karma that God has here isn't generally very strong, or can be someone weak, so even

11
00:02:23,520 --> 00:02:36,480
life itself is precarious.

12
00:02:36,480 --> 00:02:43,040
It's a bit of a dilemma, because we would argue that holding on to good things, holding

13
00:02:43,040 --> 00:02:53,160
on to your life is bad, bad because we would argue that it leads to suffering, but then

14
00:02:53,160 --> 00:03:00,280
you say, well, how can guarding your life, how can protecting your life lead to suffering?

15
00:03:00,280 --> 00:03:10,920
Well, in the end, that's the case, fear of death is suffering, we ask anyone do they want

16
00:03:10,920 --> 00:03:17,800
to die, they usually the answer is no, and only do they not want to die, but they want

17
00:03:17,800 --> 00:03:24,280
very much not to die, and that's where the problem arises, because then any time your life

18
00:03:24,280 --> 00:03:32,440
is threatened, you have to suffer, so we live our lives fighting, fighting to stay alive,

19
00:03:32,440 --> 00:03:45,000
may not seem like it, but it's kind of like a constant struggle, just to stay alive.

20
00:03:45,000 --> 00:03:50,400
Even if you're well off, you have to, every day you have to rush out, every day I have

21
00:03:50,400 --> 00:04:01,400
to rush out, to take an antidote for this disease that we all have called hunger, just

22
00:04:01,400 --> 00:04:13,920
to stay alive, every day I have to take this antidote, can't get around, it's a chronic,

23
00:04:13,920 --> 00:04:20,960
there's no cure for this sickness, and that's just one of many, you know, we need air,

24
00:04:20,960 --> 00:04:28,720
we need water, and those are the basics, you know, when you cross the street you might

25
00:04:28,720 --> 00:04:35,040
get run over, you walk in the wrong place at the wrong time, you might be robbed or beaten

26
00:04:35,040 --> 00:05:01,120
or stabbed, raped, killed, maybe blown up by a bomb, any number of things can happen,

27
00:05:01,120 --> 00:05:09,840
so we can't, we can't find peace this way, and though I would argue that I think you

28
00:05:09,840 --> 00:05:19,760
could argue that it's a good idea to stay alive, it's as much a good idea to stop clinging

29
00:05:19,760 --> 00:05:27,240
to life, I say, well, doesn't an R-100 want to stay alive, I don't think it's not one thing

30
00:05:27,240 --> 00:05:38,640
that they protect their life, it's an interesting theory and it may appear like that, but

31
00:05:38,640 --> 00:05:45,400
there's clearly in everything we do, there's the ability to do things without desire,

32
00:05:45,400 --> 00:05:54,560
just as a matter of course, it's reasonable to be walking down a path and you come upon

33
00:05:54,560 --> 00:06:03,400
and say a wild elephant, it's reasonable to come around, now it's unreasonable to run like

34
00:06:03,400 --> 00:06:09,200
a child and cry about it, to tremble in fear, there's no reason, there's no point in

35
00:06:09,200 --> 00:06:32,320
there's no benefit, but that being said, there is the sense that yes, if people try to walk

36
00:06:32,320 --> 00:06:37,720
all over you, the best thing for you to do is let them, and this is hard and this seems

37
00:06:37,720 --> 00:06:50,160
unreasonable, I think, but all of that speaks to what's being said in this quote, not

38
00:06:50,160 --> 00:06:57,320
said in this quote, but what this quote, the hints at, this quote makes some bold claims

39
00:06:57,320 --> 00:07:09,800
that good deeds have good results, and so this guy is Sina, Sina, but he is a general,

40
00:07:09,800 --> 00:07:19,040
Sina is the army, Sia is his name, he was a general, I don't know which army, but I

41
00:07:19,040 --> 00:07:28,280
can probably look him up, let's look him up here, Sina means lion, a little chewy, he

42
00:07:28,280 --> 00:07:37,280
was a little chewy and in ways, way silly, so he goes to the border and he asks him

43
00:07:37,280 --> 00:07:58,400
can you see, Saka nuko, bande, bhagava, sanditikang, dhanapalang, paniapetum, sakai, is it possible,

44
00:07:58,400 --> 00:08:07,440
paniapetum, tusho, dhanapalang, a fruit of charity, so that the subject here is actually

45
00:08:07,440 --> 00:08:34,240
charity, sanditikang, that is observable, the dhanapai, sanditikum, sanditikum, so it means you can

46
00:08:34,240 --> 00:08:42,440
see, you can observe it, you can realize it, not something you have to believe in, not

47
00:08:42,440 --> 00:08:52,520
something you have to take on faith, and the Buddha says saka, sia, it is possible, sia,

48
00:08:52,520 --> 00:09:09,920
dhanapetum, bhau, bhau, no genasapiohotimanapal, one who gives dhanapeti, dhanapeti is a master

49
00:09:09,920 --> 00:09:34,360
of dhanapeti, there is one who is a master of dhanapeti, but I don't want to talk specifically

50
00:09:34,360 --> 00:09:42,520
about charity, I've always loved to focus too much on charity, but goodness, this supply

51
00:09:42,520 --> 00:09:47,080
is equally to goodness, even though he was acting, it is the kind of thing that people

52
00:09:47,080 --> 00:09:54,200
would be concerned about, because for lay people in India and even lay Buddhists, a

53
00:09:54,200 --> 00:10:00,040
big part of their religious spirituality is charity, they're not able to become monks,

54
00:10:00,040 --> 00:10:05,080
they're not able to even maybe take meditation courses, so they give charity for this kind

55
00:10:05,080 --> 00:10:09,800
of reason, but it applies to goodness, the point that we should focus on is goodness, meditation,

56
00:10:09,800 --> 00:10:14,960
it's goodness, so what is the benefit of goodness in general?

57
00:10:14,960 --> 00:10:25,280
Well, the first one is bhau no genasapiohotimanapal, one becomes dear and pleasing to many

58
00:10:25,280 --> 00:10:37,640
people, to the multitude, to the general populace, and so this is the first, I mean,

59
00:10:37,640 --> 00:10:45,080
all four of these are arguments as to why we shouldn't worry about being stepped on,

60
00:10:45,080 --> 00:10:51,160
why we shouldn't be worried about letting people walk all over us, why we shouldn't

61
00:10:51,160 --> 00:11:01,200
worry about our possessions or our, worry about being too good is what I mean, so worry

62
00:11:01,200 --> 00:11:08,040
about the negative consequences of seeing the good of people, being nice, of being like

63
00:11:08,040 --> 00:11:13,320
a polyanna, because we think polyanna what a stupid story, if you ever know the story

64
00:11:13,320 --> 00:11:19,720
and polyanna, it's kind of a wonderful story about this girl who just sees the good in

65
00:11:19,720 --> 00:11:25,760
people and only sees the good and just does all sorts of good things and changes all sorts

66
00:11:25,760 --> 00:11:32,320
of really nasty people to be nice, and you think, wow, that doesn't happen with it,

67
00:11:32,320 --> 00:11:37,800
it doesn't happen because we aren't nice, we aren't polyanna, where it's just a story

68
00:11:37,800 --> 00:11:48,440
but it really is something I think we can appreciate, that's goodness, goodness, there's

69
00:11:48,440 --> 00:11:52,760
the first reason is that you say, well, people are going to walk all over me, not if most

70
00:11:52,760 --> 00:12:01,480
people are holding you dear, and what happens when people hold you dear, do they let other

71
00:12:01,480 --> 00:12:10,840
people walk all over you? Obviously, they don't, they cherish you and they treat you well,

72
00:12:10,840 --> 00:12:15,600
and then on top of that, they protect you for mothers, they become true friends.

73
00:12:15,600 --> 00:12:23,360
The only way to gain true friends is to be a good person, you can buy friendship, you

74
00:12:23,360 --> 00:12:35,840
can buy love, you can buy respect. So, the idea is that performing good deeds of all

75
00:12:35,840 --> 00:12:42,480
kinds protects you, this is what it said, dhammo hawe rakati dhamma jari, when you practice

76
00:12:42,480 --> 00:12:49,120
is the dhamma, the dhamma protects, the dhamma protects a person who practices the dhamma,

77
00:12:49,120 --> 00:12:54,240
this is what he meant by that, the power of goodness is real, you're a good person, everyone

78
00:12:54,240 --> 00:13:01,680
respects you, people go out of their way to protect you, to support you, to help you, to

79
00:13:01,680 --> 00:13:11,840
be kind here, to speak well with you. So, it's the first one, and then Pune al-Japarang,

80
00:13:11,840 --> 00:13:37,200
furthermore, beyond this, daya kang dhanapatim santosapurisa, sub-purisa and bhajanti.

81
00:13:37,200 --> 00:13:43,280
People who are, this is the word we used, just recently I gave a talk on these, right,

82
00:13:43,280 --> 00:13:51,280
the seven sub-purisa dhamma, probably a little translation would be gentleman, but it means

83
00:13:51,280 --> 00:13:59,600
like a high class individual or a good person, good fellow, bhajanti they associate, such good

84
00:13:59,600 --> 00:14:23,520
people associate with one who is a daya kang, giver dhanapati, and so when it's surrounded by good people,

85
00:14:23,520 --> 00:14:29,360
when it associates with good and wise people, people who are again good, and so, you know, supportive

86
00:14:31,440 --> 00:14:36,240
and trustworthy, so one doesn't have friends who will walk all over you, one doesn't have

87
00:14:36,960 --> 00:14:37,760
friendamis.

88
00:14:37,760 --> 00:14:56,080
Number three, kalyanoh kittizandoh abungachati, daya kasandanapatimoh for a daya ka, giver andanapatim

89
00:14:56,080 --> 00:15:07,040
one who is accomplished in daya kalyanoh beautiful kittizandoh report kittizandas, kittiz fame,

90
00:15:07,840 --> 00:15:23,360
sandas who sound, so it's like a celebrity or a person, a good port, I guess, you know,

91
00:15:23,360 --> 00:15:30,080
I can't think of the right word, and bhajanti is spread, so their fame is spread,

92
00:15:31,040 --> 00:15:37,520
good things are said about such a person, all these things are basically what I was talking about

93
00:15:37,520 --> 00:15:45,760
before, good things come to those who do good things, and so, there's worry that,

94
00:15:45,760 --> 00:15:55,120
there's worry that if we don't stop clinging, if we don't cling, if we stop clinging,

95
00:15:57,520 --> 00:16:03,440
it's like I was envision this, it's more like these beings holding onto the side of a cliff,

96
00:16:03,440 --> 00:16:09,520
afraid we're going to fly, afraid we're going to fall, like a bird clinging to the side of its nest,

97
00:16:09,520 --> 00:16:16,320
afraid the baby bird afraid that the mother kicks it out of the nest, it's going to fall,

98
00:16:18,000 --> 00:16:23,360
success or afraid that if we let go of all the things that we cling to, we're going to have

99
00:16:23,360 --> 00:16:28,320
nothing, it stands to reason, you have to work hard for it means do you stop working and not going

100
00:16:28,320 --> 00:16:33,040
to get it, it's actually not the case, the fact that you're obsessing over these things

101
00:16:33,040 --> 00:16:38,960
is actually stopping your energy, it's keeping you from good things, keeping you from flying,

102
00:16:41,520 --> 00:16:43,200
and when you give up, you have everything,

103
00:16:43,200 --> 00:17:12,640
and so, the first one, whatever part is there,

104
00:17:12,640 --> 00:17:20,640
some people, they go towards whether it be a katea, a person, a group of nobles,

105
00:17:21,280 --> 00:17:26,960
they had a brahmana person, or whether it be a community of brahmanas,

106
00:17:27,840 --> 00:17:36,800
gahapatipari, gahapatipari, some, whether it be a community of householders,

107
00:17:36,800 --> 00:17:46,560
gahapatipari, some, or whether it be a person of someones, of recklessness.

108
00:17:47,920 --> 00:17:52,400
We saw Aradou, again, this word we had, and we see Demaga study recently,

109
00:17:53,200 --> 00:17:57,680
means one is confident, one is unshaken, unshaken.

110
00:17:57,680 --> 00:18:10,800
We saw Aradou, upasangamati, a monk, a monk, upasangamati, one approaches and untrembling,

111
00:18:15,680 --> 00:18:20,160
and the shame, we saw Aradou, confident.

112
00:18:22,480 --> 00:18:26,160
So, one has confident, a good person is confident, they're radiant,

113
00:18:26,160 --> 00:18:31,760
people see this, you can see this in the day, they have no fear where everything goes,

114
00:18:31,760 --> 00:18:34,480
no fear of being chastised because they are good people.

115
00:18:38,880 --> 00:18:44,320
So, it changes, you know, doing good deeds makes you stronger, makes you less susceptible to

116
00:18:44,320 --> 00:18:51,680
people walking. Aradou and Tong watched over a series of weeks, during the time when I was

117
00:18:51,680 --> 00:18:57,760
sitting with him day on the everyday, these relatives of his, you know, people who found him

118
00:18:58,880 --> 00:19:06,320
distant relatives would come every week and ask for money, tell them all, they had this problem

119
00:19:06,320 --> 00:19:10,160
and that problem, who knows what they were really doing with the money, but he'd look at them,

120
00:19:12,400 --> 00:19:17,600
and the gall of these people, because he saw right through them, but as soon as he wouldn't,

121
00:19:17,600 --> 00:19:25,360
no judgment whatsoever, give them money, and every time they came back, until finally his secretary

122
00:19:25,360 --> 00:19:30,960
just blew up with them and yell, sorry, locked the door and I was sitting outside, they didn't let

123
00:19:30,960 --> 00:19:37,760
us in and basically told them what's what, I don't think in the end it worked, because they knew

124
00:19:37,760 --> 00:19:49,600
that and Tong would never back down, so he'd give them money whenever they have. And that's,

125
00:19:49,600 --> 00:19:55,040
people think that's a weakness, but if you watch it, there's such a strength there. He was so far

126
00:19:55,040 --> 00:20:04,160
about these people, but so far about the concerns of worrying about it, giving them stuff.

127
00:20:04,160 --> 00:20:11,120
But it's really terrible because that money wasn't really meant for that, it could be put to such

128
00:20:11,120 --> 00:20:17,840
better use. Again, I'm sorry, I don't need to talk about monks using money, but it's common

129
00:20:17,840 --> 00:20:24,480
knowledge monks in Thailand use money, I don't touch money, but my teacher does and all the monks

130
00:20:24,480 --> 00:20:30,480
there and they, it's just, you really can't get by in Thailand without it. Everyone's constantly

131
00:20:30,480 --> 00:20:38,000
trying to give them monks money and it's kind of shame, shame. Anyway, and that's another reason for

132
00:20:38,000 --> 00:20:45,920
not using money is because people can't take advantage of it as you, but anyway, it changes you,

133
00:20:45,920 --> 00:20:53,600
goodness changes you, makes you more confident. And number five, diet go down,

134
00:20:53,600 --> 00:21:20,880
diet is good to look at the diet, and the example you mean, that you're not doing good for

135
00:21:20,880 --> 00:21:29,160
the place, sagan in heaven, sagan will come in the world of heaven.

136
00:21:29,160 --> 00:21:30,480
So good deeds lead to heaven.

137
00:21:30,480 --> 00:21:34,960
Anyone who says the Buddha didn't teach about heaven, that he only talked about, he only

138
00:21:34,960 --> 00:21:44,360
talked about this life, and I don't really know what they're talking about.

139
00:21:44,360 --> 00:21:50,840
Heaven happens a great place to go because if you're born in heaven, it's very

140
00:21:50,840 --> 00:21:52,880
easy to practice meditation.

141
00:21:52,880 --> 00:21:57,840
Heavens full of Buddhists, there's this wonderful time, teacher at one of our gentong

142
00:21:57,840 --> 00:22:04,680
teachers, I never met him, but they recorded all of his talks on cassette tape and transferred

143
00:22:04,680 --> 00:22:08,600
them to MP3 later on.

144
00:22:08,600 --> 00:22:14,680
And he says, don't be born as a human, yep, it could have been con, this word con, depending

145
00:22:14,680 --> 00:22:17,880
how you spell it, I think, actually I'm not even sure if you have to change this

146
00:22:17,880 --> 00:22:25,160
spelling, that con means person, but it also means to stir con, when you con something

147
00:22:25,160 --> 00:22:32,240
stir it, and it's a con, yep, it could be, no, no, no, no, don't be born a human,

148
00:22:32,240 --> 00:22:38,800
the human means con, the human is a con, con, they were young.

149
00:22:38,800 --> 00:22:48,840
The stir means to, that means it means, I think, I think about this right, Jung means

150
00:22:48,840 --> 00:23:01,080
busy, Jung means crazy, busy, awesome, all mixed up con, don't be born a human, he says,

151
00:23:01,080 --> 00:23:04,920
if he want to be born anyway, be born in heaven, that's what all the Buddhists are,

152
00:23:04,920 --> 00:23:18,080
they're all up there in heaven, so you don't have to worry, I mean good people don't

153
00:23:18,080 --> 00:23:23,200
have to worry about being, about dying, you have to worry about getting on the right path,

154
00:23:23,200 --> 00:23:28,720
but up in heaven a lot of opportunity, if you look at even in the Buddhist time, the

155
00:23:28,720 --> 00:23:34,360
number of angels have became enlightened, but even now, you think we've got, still got

156
00:23:34,360 --> 00:23:41,800
the Buddhist teaching here, imagine how organized they are at heaven, with the arising

157
00:23:41,800 --> 00:23:46,880
of the Buddhist teaching, how well organized they are, how many people up in heaven

158
00:23:46,880 --> 00:23:57,000
are practicing and becoming enlightened, it's probably a lot.

159
00:23:57,000 --> 00:24:04,000
So Mahabhikavehita punya nam, don't know because they are afraid of punya, don't think

160
00:24:04,000 --> 00:24:11,960
that you're going to be taken advantage of, so if you worry it's only going to be suffering,

161
00:24:11,960 --> 00:24:17,120
then you're just clinging to the side of the cliffs, clinging to the nest, afraid to fall,

162
00:24:17,120 --> 00:24:26,280
like a bird, ignoring it, the fact that it has wings, so that's the dhamma for tonight,

163
00:24:26,280 --> 00:24:43,720
thank you all for tuning in, I'll post the hangout if anybody wants to come on, come

164
00:24:43,720 --> 00:25:12,880
on if you've got questions, if you come on and ask questions, yes, I'm in here the question,

165
00:25:12,880 --> 00:25:18,280
no, I just, I think I pressed the link just before you said, come on, if you have a

166
00:25:18,280 --> 00:25:28,960
questions, I just want to say hello and thank you for the dhamma talk, I'd say, welcome.

167
00:25:28,960 --> 00:25:32,800
These quotes are really helpful because I'm not sure I'd be able to think of something

168
00:25:32,800 --> 00:25:39,160
to say every day, if I didn't have a little prompt, this is from the book, what the

169
00:25:39,160 --> 00:25:46,720
Buddha said, still, no, it's a Buddha wetch in it, it's called Buddha wetch in it is the

170
00:25:46,720 --> 00:25:57,000
book, 365 quotes, not the quotes I would have probably chosen, but you see they're fairly

171
00:25:57,000 --> 00:26:05,480
heavy dhamma oriented or charity oriented or you know, they're soft core Buddhism, not

172
00:26:05,480 --> 00:26:12,600
all that many of them are, it shouldn't be so hard, I definitely appreciate the stuff you

173
00:26:12,600 --> 00:26:23,320
had to the quotes there, it gives a lot of sense, yeah, sorry, I don't really have any

174
00:26:23,320 --> 00:26:32,720
questions that just want to say hello and thank you, let me just pop back, today, what

175
00:26:32,720 --> 00:26:43,040
happened today, today and a couple of, from Sri Lanka came to offer breakfast and lunch

176
00:26:43,040 --> 00:26:50,240
and just to be in touch, and so they said they'd like to offer food every once a week,

177
00:26:50,240 --> 00:27:01,800
which is great, they just want to be in touch with a monk, and then two other guys came

178
00:27:01,800 --> 00:27:09,720
to my students from McMaster, and they showed one of them how to meditate and they want

179
00:27:09,720 --> 00:27:15,800
to come back and do of course some months from now, four months they send, so that'd be

180
00:27:15,800 --> 00:27:32,800
over the summer probably, sounds wonderful, and someone tonight told me that through this

181
00:27:32,800 --> 00:27:45,120
practice they overcame alcoholism, so that was great, and survived chemotherapy by meditating

182
00:27:45,120 --> 00:27:53,920
through it, wow, yeah, that's very interesting, I mean, I don't know physically but mentally,

183
00:27:53,920 --> 00:28:03,040
and as far as dealing with the pain and stress and you know, yeah, I couldn't imagine

184
00:28:03,040 --> 00:28:10,760
meditation doing anything but good for such patients, yeah, all these, we should maybe have

185
00:28:10,760 --> 00:28:25,440
a page of testimonials, just testimonials, yeah, definitely, when people can just, I don't

186
00:28:25,440 --> 00:28:31,360
know, maybe a page is overkill, maybe just have people, yeah, we should have a page

187
00:28:31,360 --> 00:28:42,360
but I post them or something, not letting people post them, but I'll say if I have time,

188
00:28:42,360 --> 00:28:54,360
sounds like wonderful idea, maybe I'm going to remind you of that it's such a good idea.

189
00:28:54,360 --> 00:29:09,360
I'll say I have a question, okay, what would you recommend you do if you realize that

190
00:29:09,360 --> 00:29:18,200
one of your friends is a clinical sociopath or narcissist, so somebody who has no conscience

191
00:29:18,200 --> 00:29:24,800
and basically lives by the opposite of the five precepts, and doesn't sound like a very

192
00:29:24,800 --> 00:29:30,880
good friend, why, why, doesn't sound like something I'd want as a friend, someone I'd

193
00:29:30,880 --> 00:29:38,800
want as a friend, I think a big problem is we choose friends arbitrarily, why are you friends

194
00:29:38,800 --> 00:29:44,720
with people if they are narcissists and so is he, so is he best, well this particular person

195
00:29:44,720 --> 00:29:52,240
was a co-worker of mine, and these types of people are generally very charming in nature,

196
00:29:52,240 --> 00:29:58,720
so it was not something that I realized until, you know, our friendship had developed.

197
00:29:58,720 --> 00:30:03,280
Chalk it up for experience and move on, I mean, not to be cruel, you don't have to avoid

198
00:30:03,280 --> 00:30:10,760
the person, I would, to some extent, you know, at least not actively seek this person out

199
00:30:10,760 --> 00:30:18,200
and be careful around them, it's certainly, there'll be mindful around them, but don't

200
00:30:18,200 --> 00:30:23,800
let it, don't suffer because of it, don't lose sleep over it, don't get upset when

201
00:30:23,800 --> 00:30:31,800
they try to take advantage of you, just, you know, don't have anything to do with such

202
00:30:31,800 --> 00:30:32,800
people.

203
00:30:32,800 --> 00:30:40,840
Because part of being mindful is learning not to engage, because we react to people, people

204
00:30:40,840 --> 00:30:50,920
like that are really good at getting at you, right, evoking a response, they'll say something

205
00:30:50,920 --> 00:30:56,080
ridiculous and you'll attack them for it and then they'll bait and switch, right?

206
00:30:56,080 --> 00:30:59,920
They bait you in with something and then, oh, a sob story and then you feel sorry for

207
00:30:59,920 --> 00:31:04,440
them and by the time you realize that they're just playing it, you have to be mindful

208
00:31:04,440 --> 00:31:05,440
of that.

209
00:31:05,440 --> 00:31:11,960
You can't let your emotions have a lot to do with it and, you know, we're not perfect,

210
00:31:11,960 --> 00:31:16,880
so you're going to make mistakes, most important is to not suffer because of it.

211
00:31:16,880 --> 00:31:24,840
And the only way to really not suffer is to learn to let go, to learn to not cling, stop

212
00:31:24,840 --> 00:31:30,800
trying to protect yourself.

213
00:31:30,800 --> 00:31:31,800
Thank you, Bob.

214
00:31:31,800 --> 00:31:38,360
Yeah, I mean, to what extent you're able to do that in lay life, it's tug of war, you know,

215
00:31:38,360 --> 00:31:41,720
or kind of caught in the middle of trying to live the domo life and trying to live in

216
00:31:41,720 --> 00:31:49,480
the world, so it doesn't mean you have to give up everything, but the extent that you

217
00:31:49,480 --> 00:31:56,200
can give up, it is actually to your benefit.

218
00:31:56,200 --> 00:32:02,600
Good question.

219
00:32:02,600 --> 00:32:04,800
Thank you.

220
00:32:04,800 --> 00:32:11,480
Okay, so that's all, I'm going to say good night.

221
00:32:11,480 --> 00:32:15,480
See you're good night, Panda.

222
00:32:15,480 --> 00:32:20,500
Good night.

