I have the same question as Brenna. Is it necessary for me to sub-localize a description of the object I am aware of?
I am able to perceive objects without necessarily naming them.
Sometimes finding an English word to fit the thought is distracting.
Again, this is only because you're untrained because it's new because it's something that you're probably not very good at,
which is natural when you first start something.
Eventually, you get better at recognizing things as they are.
There's not that many experiences that after a good many hours of doing this.
You get pretty good at finding names for just about everything.
It's not like there's millions of different words that you could possibly have to use.
But we don't call it sub-localizing.
We call it reminding yourself, which is indeed how we understand the word set-p,
which doesn't really mean to remember in the sense of re-reifying the object.
Instead of going the next step of liking or disliking it, you say to yourself,
you're saying, instead of saying, this is good, this is bad, you say this is this, that's the mindfulness.
It's actually reminding yourself.
Mindfulness of the Buddha is to remind yourself of the Buddha, to remember the Buddha.
But you do that by saying to yourself,
Buddha, Buddha, Bhagava, Bhagava, Arahang, Arahang.
It's all the qualities of the Buddha that remind you of the Buddha,
so keep you focused on the idea of the Buddha with mindfulness of the body into the same.
But you remind yourself of the body into it.
Otherwise, we don't consider it in this tradition to be meditation.
Consider it just sitting there zoning out, which isn't kamatana,
it isn't training, it isn't insight meditation.
Not.
