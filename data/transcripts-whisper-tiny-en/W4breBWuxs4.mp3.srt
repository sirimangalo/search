1
00:00:00,000 --> 00:00:05,600
difficult to describe in short in meditation my chin sank to the breast and my head started

2
00:00:05,600 --> 00:00:13,680
describing a certain kind of orbiting circling and tilting and he goes on to describe the

3
00:00:14,800 --> 00:00:21,360
that particular tilting motion vibrating or rocking would it be?

4
00:00:21,360 --> 00:00:27,760
I don't know. It's broken. Slow and small increments felt like the outside of his head was orbiting.

5
00:00:29,440 --> 00:00:34,080
Well inside there is a fixed and stable mass. This sort of thing.

6
00:00:36,320 --> 00:00:43,600
Peti this is generally referred to as Peti, a part of rapture. Rapture comes in meditation

7
00:00:44,640 --> 00:00:50,880
if you're in a course it comes after a few days. Not for everyone but if it comes it comes after a few

8
00:00:50,880 --> 00:00:58,000
days maybe five days let's say. And so for some people the Peti is getting caught up

9
00:01:00,560 --> 00:01:05,200
getting in falling into a habit or becoming accustomed

10
00:01:06,560 --> 00:01:11,520
you might say like falling into a rat but it can be positive. You can fall into the

11
00:01:11,520 --> 00:01:20,240
rat of meditation like get into the groove this is the word. You get into a groove and so if it's

12
00:01:20,240 --> 00:01:25,280
with the mindfulness then it's quite useful. You're in raptured in just being mindful

13
00:01:25,280 --> 00:01:32,080
and the mindfulness becomes easier and easier and clearer and clearer but if you get in a groove

14
00:01:32,080 --> 00:01:42,240
like this person's talking about it's just at that end it doesn't lead anywhere. So you get

15
00:01:42,240 --> 00:01:47,440
into a groove. Many people rock back in front of this is what is most common. Some people slowly

16
00:01:47,440 --> 00:01:53,680
slowly go in one direction and go down lying on their back or so and yeah some people start to go

17
00:01:53,680 --> 00:02:04,000
in circles. So you have something like that. This is the point one of the more crucial points

18
00:02:04,000 --> 00:02:08,720
of the meditation practice according to the V-sudimanga called magma magna de sine v-sudi

19
00:02:09,440 --> 00:02:17,760
where one purifies one's knowledge and vision by just making a decision or gaining understanding

20
00:02:17,760 --> 00:02:23,520
of what is and what is not the path. So at this point the meditator doesn't know what is the path

21
00:02:23,520 --> 00:02:29,360
and what is not the path and they have to clarify for themselves and realize for themselves

22
00:02:30,240 --> 00:02:36,080
and get themselves on the right path and stop following the wrong path. So at this point the

23
00:02:36,080 --> 00:02:43,360
meditator will be afflicted by these sorts of experiences and will take them to be the path.

24
00:02:43,360 --> 00:02:48,960
They will have lots of energy sometimes. That video I did called what the meditation is not.

25
00:02:50,080 --> 00:02:54,240
I don't think it was actually quite the problem with that was I still didn't have a partner

26
00:02:54,240 --> 00:02:58,320
account. So I only took 10 minutes to talk about something that probably should have taken

27
00:02:59,200 --> 00:03:05,920
maybe 20 or a half an hour. I've given audio talks on the 10 imperfections of insight I think.

28
00:03:05,920 --> 00:03:13,120
Maybe I can find one of those but otherwise I'll try to do another video about the 10 imperfections.

29
00:03:14,080 --> 00:03:17,040
If you look up, look it up on the internet you might find it.

30
00:03:20,720 --> 00:03:27,520
But there are basically the count 10 categories of things that can come. You feel very happy,

31
00:03:27,520 --> 00:03:35,120
you feel very calm, you have these rapturous experiences, you get a lot of energy, you have a

32
00:03:35,120 --> 00:03:43,280
very strong knowledge, you realize many things that you didn't think of before you have to run

33
00:03:43,280 --> 00:03:49,440
your life or even things about Buddha's teaching and meditation and you get caught up in these

34
00:03:49,440 --> 00:03:54,720
experiences and run with them and so rather than being mindful of them and being objective about them

35
00:03:54,720 --> 00:04:04,640
you cling to them, subtly cling to them and as a result get off track. So the key is this teaching

36
00:04:05,840 --> 00:04:12,320
that I mentioned yesterday, always remember this is not that, always remind yourself this is not that

37
00:04:12,960 --> 00:04:23,280
whatever it is it's not that and it should be observed objectively just as anything else.

38
00:04:23,280 --> 00:04:34,160
This is the task that we're given in the meditation is to set ourselves in objectivity and

39
00:04:34,160 --> 00:04:41,600
to become purely aware and to give up our personalities and our subjectivity.

40
00:04:45,680 --> 00:04:47,520
Does that seem to answer what they were talking about?

41
00:04:47,520 --> 00:04:55,200
I guess they continue on and they said it sort of felt like a tarant, but they're not sure.

42
00:04:57,680 --> 00:05:03,360
Don't worry about a trance, put it out of your mind. Let's talk about meditation. There's too

43
00:05:03,360 --> 00:05:09,120
much in Buddhism, but trance isn't Janus as far as I'm concerned. Janus meditate. If your mind

44
00:05:09,120 --> 00:05:14,000
becomes calm, great. If it doesn't become calm, great. If your mind becomes calm, that's your mind

45
00:05:14,000 --> 00:05:19,040
being calm. If your mind becomes clear, that's your mind becoming clear. Good things, great things.

46
00:05:19,040 --> 00:05:22,960
Don't give them labels. Don't say this is this, that is that. It is what it is.

47
00:05:24,480 --> 00:05:30,240
That you'll do the best that way. It's my opinion, but there I do it. It is what it is.

48
00:05:31,040 --> 00:05:35,920
That's the key. I mean, I can call it PT. You can call it a trance in the end. What's going on?

49
00:05:35,920 --> 00:05:40,240
See it for what it is. One thing that I've said and I think is very repeating is

50
00:05:40,240 --> 00:05:49,840
that sometimes you have to tell it to stop. These PT or rapture states that you get into,

51
00:05:50,400 --> 00:05:55,760
they become very, very physical, where you're not really in charge of it anymore. So you have to

52
00:05:55,760 --> 00:06:01,600
really make a demand on your body and tell yourself, stop. Sometimes it's not enough to say

53
00:06:01,600 --> 00:06:10,000
spinning, spinning, or feeling, feeling, because it's already gotten on its way. You have to

54
00:06:10,000 --> 00:06:15,600
actually tell it to stop. If you say stop in your mind and to just firmly and politely,

55
00:06:16,640 --> 00:06:22,720
no, no, stop. Tell it to stop. Then go on with your meditation.

56
00:06:24,400 --> 00:06:32,320
I think I've experienced a sort of thing that he's describing. Like you said, it's kind of a physical

57
00:06:32,320 --> 00:06:41,760
thing. I think it's really a meditation at all. It's just kind of a physical phenomenon.

58
00:06:48,960 --> 00:06:53,600
I mean, there is the mental part of it, but it's very, very subtle to

59
00:06:55,040 --> 00:07:01,280
the attachment can be very subtle. You just have to give yourself a slap in the face and say,

60
00:07:01,280 --> 00:07:03,840
stop. Back to work.

