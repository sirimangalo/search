1
00:00:00,000 --> 00:00:06,160
Wisdom from a pleasant glow. After watching the rising and falling of the abdomen for a while,

2
00:00:06,160 --> 00:00:11,280
there is nothing but a pleasant glow. Does wisdom arise spontaneously, or do I need to do more?

3
00:00:13,920 --> 00:00:18,160
That pleasant glow can become a problem for you, because you find it pleasant.

4
00:00:18,160 --> 00:00:22,400
The experience of sensations as pleasant is one of the most common hindrances.

5
00:00:23,520 --> 00:00:27,280
Even meditators practicing for years might feel like they are not progressing,

6
00:00:27,280 --> 00:00:33,600
because they get stuck on calm or pleasant experiences. The pleasant glow could be

7
00:00:33,600 --> 00:00:40,000
peti, which is rapture, suka, which is happiness, or basadi, which is tranquility.

8
00:00:41,440 --> 00:00:47,280
Whatever you classify it as, it is an experience that comes as a byproduct of meditation practice.

9
00:00:47,280 --> 00:00:51,600
It is impermanent, it is unsatisfying in the sense that it cannot last forever,

10
00:00:51,600 --> 00:00:55,280
and it is uncontrollable in the sense that you cannot turn it on or off.

11
00:00:55,280 --> 00:01:00,240
When you sit down and meditate, it comes by itself, and it is not yours to control.

12
00:01:00,800 --> 00:01:06,960
To confirm these things, say to yourself, happy, happy, feeling, feeling, liking, liking,

13
00:01:06,960 --> 00:01:12,720
or however it presents itself to you. If you are persistent in acknowledging both the happiness

14
00:01:12,720 --> 00:01:17,520
and the liking of it, you will see that just like everything else that arises, it is impermanent,

15
00:01:17,520 --> 00:01:22,960
unsatisfying, and uncontrollable. When you do not maintain mindfulness in this way,

16
00:01:22,960 --> 00:01:28,480
you are likely to cling to it, wisdom will not arise unless you are objective and clear of mind,

17
00:01:28,480 --> 00:01:33,200
which is not the case if you are enjoying pleasant feelings. There is nothing wrong with such

18
00:01:33,200 --> 00:01:39,120
feelings. It can actually be quite useful, because they provide you with energy and tranquility

19
00:01:39,120 --> 00:01:45,280
in the mind. But as soon as you find yourself enjoying them, even subtly, then you are clinging to it.

20
00:01:45,280 --> 00:01:50,320
This can be quite subtle, it can be obscured by delusions about it like the idea that the

21
00:01:50,320 --> 00:01:56,400
pleasant feeling is me or mine, by acknowledging the sensation, feeling, feeling, happy,

22
00:01:56,400 --> 00:02:02,480
happy, liking, liking, or however it may appear to you, this delusion ceases, and you will gain

23
00:02:02,480 --> 00:02:08,880
an objective perspective on the arising and passing of experiences. You will come to see through

24
00:02:08,880 --> 00:02:14,160
experience that pleasant feelings are not actually something that you can depend on. They are

25
00:02:14,160 --> 00:02:21,680
not the path or goal of meditation. Seeing that is wisdom, which is the path. Such understanding

26
00:02:21,680 --> 00:02:27,040
leads you closer to freedom from suffering. Such wisdom cannot be created. It is not something

27
00:02:27,040 --> 00:02:31,920
you can actively cultivate by thinking, reflecting or asking questions about reality.

28
00:02:32,880 --> 00:02:38,000
It is something that will arise by itself, but it will only arise if you are objective and clear

29
00:02:38,000 --> 00:02:44,240
in your mind. So be careful about pleasant sensations as they are not the path, and like many other

30
00:02:44,240 --> 00:03:14,080
positive experiences will only get in the way of your practice if you cling to them.

