1
00:00:00,000 --> 00:00:02,000
Mm-hmm.

2
00:00:02,000 --> 00:00:04,000
Mm-hmm.

3
00:00:04,000 --> 00:00:06,000
Mm-hmm.

4
00:00:06,000 --> 00:00:09,000
Mm-hmm.

5
00:00:09,000 --> 00:00:35,000
Mm-hmm.

6
00:00:35,000 --> 00:00:39,000
I'm sorry about that.

7
00:00:39,000 --> 00:00:41,000
In the...

8
00:00:41,000 --> 00:00:43,000
In the...

9
00:00:43,000 --> 00:00:45,000
In the chakakaka,

10
00:00:45,000 --> 00:00:47,000
Suta, it talks about the eye,

11
00:00:47,000 --> 00:00:49,000
and the still-able forms not being self,

12
00:00:49,000 --> 00:00:51,000
because their eyes and cease.

13
00:00:51,000 --> 00:00:54,000
I can see how eye consciousness arises and ceases,

14
00:00:54,000 --> 00:00:58,000
but it seems to me my eye arose while I was in the womb,

15
00:00:58,000 --> 00:01:01,000
and will not likely cease until after I die.

16
00:01:01,000 --> 00:01:04,000
And some visible forms, say Iraq,

17
00:01:04,000 --> 00:01:06,000
have been around many thousands of years.

18
00:01:06,000 --> 00:01:09,000
Many material objects I won't observe

19
00:01:09,000 --> 00:01:11,000
arising and ceasing in my lifetime.

20
00:01:11,000 --> 00:01:14,000
So how is it possible to see these things as impermanent

21
00:01:14,000 --> 00:01:16,000
and not self then?

22
00:01:16,000 --> 00:01:20,000
And sort of related to this are the eye and internal eye-based,

23
00:01:20,000 --> 00:01:21,000
the same thing.

24
00:01:21,000 --> 00:01:24,000
You mentioned they were different in a previous answer,

25
00:01:24,000 --> 00:01:27,000
but I didn't understand that difference.

26
00:01:27,000 --> 00:01:31,000
I mean, it's all just very technical.

27
00:01:31,000 --> 00:01:33,000
I wouldn't overthink this.

28
00:01:33,000 --> 00:01:57,000
It means...

