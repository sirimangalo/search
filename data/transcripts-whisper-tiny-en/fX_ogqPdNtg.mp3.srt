1
00:00:00,000 --> 00:00:14,640
Okay, good evening. So tonight we are looking at the Dhamma Padad first 194, which goes

2
00:00:14,640 --> 00:00:30,120
as follows. So kobu dhanamupadhu. So kasadhamma desana. So kasang has sasamagi, samaghanan tapo

3
00:00:30,120 --> 00:00:48,280
so-called, which means happiness is the arising of a Buddha. Happiness is the teaching of the

4
00:00:48,280 --> 00:01:05,720
Dhamma. Happiness is the community that is harmonious. Happiness or happiness is the harmony of a

5
00:01:05,720 --> 00:01:24,360
community. The exertion, samaghanan tapo so-called, the exertion of those who have samaghi who have

6
00:01:24,360 --> 00:01:36,920
harmony is happiness. This is a fairly popular verse. It's one when I was in Thailand we would

7
00:01:36,920 --> 00:01:46,280
chant it every morning. I'm not sure why we chanted it every morning, but it is a fairly

8
00:01:46,280 --> 00:01:53,480
beautiful verse and it's a good reminder. I think one of the reasons for chanting it is when you're

9
00:01:53,480 --> 00:02:02,760
in a monastery, harmony is very important. There's not much of a story behind this. It's another

10
00:02:02,760 --> 00:02:12,520
one of these verses that just arose from a question or no arose from some talk. So the story

11
00:02:12,520 --> 00:02:24,440
goes that the monks were sitting in the Dhamma Hall, a group of monks, and they were asking,

12
00:02:24,440 --> 00:02:32,440
one of them asked, what do you think is the great happiness? The greatest suka. And I say suka because

13
00:02:32,440 --> 00:02:43,080
the word had some sort of colloquial meaning. It literally means happiness, I think, but

14
00:02:44,680 --> 00:02:49,880
couldn't be translated as pleasure. So it's not quite clear what they were talking about, but

15
00:02:52,520 --> 00:02:56,360
I don't know how philosophical it was because their answers were kind of odd for monks.

16
00:02:56,360 --> 00:03:01,960
One of the monks said, well I think the highest suka is rulership.

17
00:03:05,480 --> 00:03:09,800
I suppose we could assume that they were talking about happiness and they just hadn't made the

18
00:03:09,800 --> 00:03:19,160
connection between a more refined sort of happiness. We would often talk more about suffering, right?

19
00:03:19,160 --> 00:03:26,840
So maybe they just heard about suffering and they didn't associate a practice of Buddhism with

20
00:03:26,840 --> 00:03:32,440
happiness. Maybe they hadn't found any happiness in the Buddha's teaching. They maybe saw that it

21
00:03:32,440 --> 00:03:38,440
was good or beneficial, but they were still suffering, maybe they were still suffering,

22
00:03:40,840 --> 00:03:46,200
struggling with the practice. So when they thought about happiness, they were first one said

23
00:03:46,200 --> 00:03:57,080
rulership, being a king, being in charge. The second one said, or some said

24
00:04:00,680 --> 00:04:06,040
gamma, which means sensuality. It said sensuality is the highest suka.

25
00:04:07,240 --> 00:04:13,080
This is attachment to sights and sounds as well as the pleasure that you get from them. So

26
00:04:13,080 --> 00:04:21,880
sensual pleasure. And some other said things like food, getting the right food, being able to eat

27
00:04:21,880 --> 00:04:28,200
good food, pizza every day, or cheeseburgers, or whatever it is, the food that you like.

28
00:04:32,840 --> 00:04:40,520
And the Buddha heard them talking. He came in and said, what is it that you were talking about

29
00:04:40,520 --> 00:04:48,920
when I came in? And they told them and he said, something like that, what are you saying?

30
00:04:51,320 --> 00:04:57,880
Literally, what are you saying? He said, all that sort of happiness, all of those kinds of

31
00:04:57,880 --> 00:05:11,080
suka are bound up, are caught up or they belong to periapana. They belong to the suffering of

32
00:05:11,080 --> 00:05:24,280
what the suffering of the what that means around, it means a sort of a circular path, the cycle,

33
00:05:24,280 --> 00:05:31,080
well, literally probably cycle. And he's maybe referring to some sara, but this is what the

34
00:05:31,080 --> 00:05:36,040
Buddha said. And he said, real happiness is in the need of this verse.

35
00:05:40,680 --> 00:05:47,080
So I think we can get two sorts of lessons out of this. The first comes from the story where the

36
00:05:47,080 --> 00:05:56,280
Buddha points this out about how these types of happiness that these monks were coming up with

37
00:05:56,280 --> 00:06:03,400
are not really happiness. He says, those belong to the realm of suffering. But he used the

38
00:06:03,400 --> 00:06:09,560
word, what the cycle of suffering? Because it has to be admitted that there's pleasure involved

39
00:06:09,560 --> 00:06:14,680
with all those things. Even though there's pleasure involved with all those things,

40
00:06:14,680 --> 00:06:22,680
they're caught up in a cycle, meaning they involve an inevitability or an inevitable state of

41
00:06:22,680 --> 00:06:30,680
suffering. When we talk about happiness, it's a hard thing to pin down, just like suffering,

42
00:06:30,680 --> 00:06:36,360
I think it's a hard thing to pin down. How would you define suffering or how would you define

43
00:06:36,360 --> 00:06:42,680
happiness? I don't think we have to. I don't think this is a sort of philosophical thing.

44
00:06:42,680 --> 00:06:53,160
I think we can get a fairly easy agreement about suffering and happiness. If we talk about the

45
00:06:53,160 --> 00:06:58,840
kinds of things that are suffering, we're going to all get a sense of what we mean. There's no real

46
00:06:59,640 --> 00:07:05,240
ambiguity there and the same with happiness. The problem comes when we

47
00:07:05,240 --> 00:07:17,400
conflate what we want with what makes us happy or with the cycle of addiction with happiness.

48
00:07:18,520 --> 00:07:25,960
We think that because we want something, it's a part of it. That thing, the acquiring of that

49
00:07:25,960 --> 00:07:35,800
thing is happiness. The problem is that that sort of thinking leads us to a lot of suffering. It's

50
00:07:35,800 --> 00:07:41,800
involved with this cycle. It may be very true that anyone would say, getting their food that you

51
00:07:41,800 --> 00:07:48,360
want or being in charge of other people, controlling other people, or just indulging in

52
00:07:48,360 --> 00:07:52,440
central pleasure, that there's pleasure, that there's a pleasant feeling, there's happiness,

53
00:07:52,440 --> 00:07:59,400
there there's suka there. Maybe we could all agree on that. But when you think like that,

54
00:07:59,400 --> 00:08:08,360
and when you follow and pursue that path, you end up in a cycle of suffering, what the dukha?

55
00:08:13,560 --> 00:08:19,720
And so we'd be better off thinking of happiness as this sort of answer to the question,

56
00:08:19,720 --> 00:08:26,040
are you happy? When we ask someone about their life and we say, okay, you're doing this, but are you

57
00:08:26,040 --> 00:08:36,840
happy? And well, we'd have to be honest with ourselves and be actually mindful to be able to

58
00:08:36,840 --> 00:08:42,440
have the ability to see whether we're happy because I think it is quite common for people to

59
00:08:42,440 --> 00:08:48,440
not really be sure if they're happy or not really know if they're happy. Many people who come to

60
00:08:48,440 --> 00:08:55,160
meditate are quite sure that they're not happy and they know that they're not happy. Another problem

61
00:08:55,160 --> 00:09:06,280
is that we experience things differently and so some people are very naturally inclined to greater

62
00:09:06,280 --> 00:09:14,120
happiness, greater pleasure. This is asked to do it the way the brain is wired even. For some

63
00:09:14,120 --> 00:09:24,520
people it's very hard for them to feel happy. And so happiness becomes a little bit more complicated

64
00:09:24,520 --> 00:09:32,120
than simply saying, well, this makes me happy or that makes me happy. And not only do we have to

65
00:09:32,120 --> 00:09:36,680
look at the bigger picture of the overall picture of our life, but we also have to, I think,

66
00:09:36,680 --> 00:09:44,360
look very much at our direction. So if you say, I'm very happy, I kill and I steal and I lie and

67
00:09:44,360 --> 00:09:50,280
I cheat, but I'm very happy. It could be because your brain might be very much wired to enjoy life

68
00:09:50,280 --> 00:09:57,400
and to enjoy things and to put aside and to minimize any problems that you might have. But still

69
00:09:57,400 --> 00:10:03,960
all those things that you're doing are building up to potential, a great potential for suffering.

70
00:10:03,960 --> 00:10:09,480
They're corrupting your mind, they're changing your brain how it works. Even just indulging in a

71
00:10:09,480 --> 00:10:19,240
addictive substances or activities is changing the way your brain works, the way your mind works.

72
00:10:19,240 --> 00:10:25,720
And eventually makes it harder for you to be happy. Why? Because it taxes and it changes and it

73
00:10:25,720 --> 00:10:33,720
kind of stretches the brain systems. You need more stimuli to get the same amount of pleasure.

74
00:10:37,080 --> 00:10:44,600
So while I think happiness is easy for us to agree on, it's much harder to understand

75
00:10:45,400 --> 00:10:49,640
and to get beyond this idea that this will make me happy or that will make me happy.

76
00:10:49,640 --> 00:10:59,640
And we have to look at, again, the overall picture, but also where we're going.

77
00:11:02,360 --> 00:11:08,680
And so in meditation practice it can be sometimes quite unpleasant as you have to face things

78
00:11:08,680 --> 00:11:17,160
that are unpleasant, but there's a very strong theory in place here that I think is very profound

79
00:11:17,160 --> 00:11:25,560
and important is that we're headed in a direction. We're doing these things for reasons,

80
00:11:26,200 --> 00:11:33,800
we're staying with pain, we're being objective with unpleasant thoughts, even our emotions,

81
00:11:33,800 --> 00:11:41,480
we're not judging or trying to change them. For a reason, we have a plan and this is our path.

82
00:11:41,480 --> 00:11:52,840
We, in many ways, we welcome the unpleasantness. Why? Because that's the thing that we are weak on.

83
00:11:53,880 --> 00:11:58,040
If we spend all our time seeking out the pleasurable, well, that's what we're already,

84
00:11:59,240 --> 00:12:05,080
that's not really a problem. The problem is where we find suffering. What is the aspect of the cycle

85
00:12:05,720 --> 00:12:09,640
that causes us suffering? And if we understand that, we can free ourselves from it and then

86
00:12:09,640 --> 00:12:17,000
the rest is just happiness. So happiness is not just about being happy and this is why I've

87
00:12:17,000 --> 00:12:24,200
said before, happiness doesn't lead to happiness. Because it's about a path, it's about the overall

88
00:12:24,200 --> 00:12:29,720
picture, it's about something more than just finding what makes you happy. So that's the first

89
00:12:29,720 --> 00:12:37,160
lesson, I think, that kind of frames meditation practice, why are we practicing what we're doing?

90
00:12:37,160 --> 00:12:45,880
There's something more than just enjoying life as though that we're possible to do directly.

91
00:12:47,000 --> 00:12:54,920
We're trying to overcome the things that get in the way of us being happy. We're trying to change

92
00:12:55,560 --> 00:13:02,440
and open up our minds to be less judgmental, less partial, less reactionary,

93
00:13:02,440 --> 00:13:16,120
less controlling, less demanding and happier. The second lesson comes, I think, from the verse

94
00:13:16,120 --> 00:13:23,000
and that's, of course, dealing with these four types of happiness. So we can see these as four

95
00:13:23,000 --> 00:13:28,200
types of happiness. I think maybe it's a little clearer to see them as four requirements

96
00:13:28,200 --> 00:13:35,000
for the practice of happiness. The first is the arising of a Buddha. Without the arising of the

97
00:13:35,000 --> 00:13:44,440
Buddha, I think we take it quite for granted the profound impact the Buddha has had on the world.

98
00:13:46,520 --> 00:13:50,520
In the West, we're a little more familiar with maybe the impact that Jesus had on the world.

99
00:13:50,520 --> 00:13:58,760
And so if you think of the impact that Christianity has had on the world, not all good, but quite

100
00:13:58,760 --> 00:14:06,600
profound or any religious teaching, what is the effect that the Buddha has had? We wouldn't even

101
00:14:06,600 --> 00:14:11,320
have the concept of mindfulness as a meditation practice if it weren't for the Buddha.

102
00:14:11,320 --> 00:14:18,440
We certainly wouldn't have the teaching on the four noble truths.

103
00:14:20,600 --> 00:14:26,760
We wouldn't have this idea of letting go and so I mean we would have basic teachings,

104
00:14:26,760 --> 00:14:30,360
good teachings, but we would have a lot of bad mixed in there.

105
00:14:32,600 --> 00:14:39,160
So the arising of the Buddha is great happiness. It gives us a practice that is pure

106
00:14:39,160 --> 00:14:49,160
and clear and simple and not easy but conducive to true happiness and without it,

107
00:14:50,360 --> 00:14:58,040
without this practice. It's not like the Buddha just found and just picked a teaching.

108
00:14:58,040 --> 00:15:02,120
It's not like he went out and said oh wow, this is what I should be doing, this is what they're

109
00:15:02,120 --> 00:15:09,080
doing over there. So it's not like we will always have the option. It's not like the Buddha

110
00:15:09,080 --> 00:15:13,720
just taught us to do something that was already there. He actually came up with something new.

111
00:15:14,680 --> 00:15:19,240
So meaning that what I mean is that when the Buddha is gone we won't even have the option to practice.

112
00:15:19,240 --> 00:15:23,560
It's not just that we won't have anyone telling us to practice. We won't even know anything

113
00:15:23,560 --> 00:15:30,280
about it. The concept of practicing mindfulness may be someone will come up with it on their own,

114
00:15:30,280 --> 00:15:39,400
just fleeting, but for anyone to be clearly aware and capable of presenting and teaching and

115
00:15:39,400 --> 00:15:48,120
spreading the teaching that just won't be there once the Buddha is gone. So this whole path will

116
00:15:48,120 --> 00:15:59,640
disappear. The great loss of happiness and the potential for peace and freedom from suffering.

117
00:16:04,040 --> 00:16:08,760
The second is of course the teaching of the Dharma. So even with the arising of the Buddha,

118
00:16:10,600 --> 00:16:17,480
many people didn't get to hear him speak even now, today, 2500 years later. The teachings have

119
00:16:17,480 --> 00:16:23,880
spread, but there are still many people in the world who haven't heard the teaching and so they're

120
00:16:23,880 --> 00:16:29,080
hearing different teachings probably and they're coming up with teachings on their own,

121
00:16:31,000 --> 00:16:37,880
coming up with practices on their own, or just wasting away because they never had the

122
00:16:37,880 --> 00:16:47,480
instruction, they never had anyone explained to them a reason for keeping even ethical

123
00:16:47,480 --> 00:16:54,520
precepts or practicing meditation, they never had any guidance for how to direct their mind or

124
00:16:54,520 --> 00:17:02,360
how to see clearly. So hearing the Dharma is not something we should take for granted either,

125
00:17:02,360 --> 00:17:08,520
the great happiness that comes from hearing the Dharma because of course that's

126
00:17:08,520 --> 00:17:20,120
necessary to be happy. If you don't ever learn the truth and we're not not exactly claiming that

127
00:17:20,120 --> 00:17:25,160
we have the truth or something, which just means whatever is the truth. If you don't have someone

128
00:17:25,160 --> 00:17:34,520
explaining it to you, or if you don't ever hear it or learn it for yourself, you can't possibly

129
00:17:34,520 --> 00:17:43,560
find true happiness. You'll always be engaging in wrong practices and some good some bad. You'll

130
00:17:43,560 --> 00:17:57,080
just be a mix of good inventancies caught up in the cycle of suffering. The third aspect of

131
00:17:57,080 --> 00:18:06,120
happiness that's required is the Sukhasan kasasam or ki. The requirement for a community is let's

132
00:18:06,120 --> 00:18:12,680
say requirement, let's just say it's a part of happiness because it is possible to practice alone,

133
00:18:12,680 --> 00:18:18,920
but you can also say well that's a sort of a community that is in harmony. Like when you're alone

134
00:18:18,920 --> 00:18:26,600
in your room, you're a community of one that is quite harmonious, or you can look even deeper and say

135
00:18:27,720 --> 00:18:33,720
harmonious, there's internal harmony as well, are we conflicted when you're doubting the practice,

136
00:18:35,240 --> 00:18:39,640
you're of two minds about the practice, right? Maybe you think,

137
00:18:39,640 --> 00:18:45,160
after you think it's good, after you think it's bad, like part of you, sorry, inside part of you

138
00:18:45,160 --> 00:18:49,880
think it's bad, part of you think it's good, maybe part of you think you can do it, part of

139
00:18:49,880 --> 00:18:57,240
you think you're not capable of doing it, part of you is keen to do it, part of you is lazy to do it,

140
00:18:58,440 --> 00:19:07,640
then you have a disharmonious community. But I think the Buddha was talking about a real community

141
00:19:07,640 --> 00:19:15,000
though we can't extrapolate, but having a community, having a meditation community can be a

142
00:19:15,000 --> 00:19:23,320
monastery or here a meditation center is a great gain for all of us because it encourages us to practice.

143
00:19:24,760 --> 00:19:31,160
We have other people who are doing the same things, so we have evidence that it is possible

144
00:19:32,280 --> 00:19:35,720
when you see someone who has been practicing for longer than you and you see how

145
00:19:35,720 --> 00:19:48,760
fluent or how well-trained they are in it, how skilled they are in the practice, it's quite

146
00:19:48,760 --> 00:19:58,040
encouraging. It shows the potential for you to continue having other people around practicing

147
00:19:58,040 --> 00:20:04,040
is a great thing, but also the aspect of it being harmonious. So we're all practicing the same

148
00:20:04,040 --> 00:20:16,200
way, the same teaching, and that we all have similar qualities of mind. So one that we have a

149
00:20:16,200 --> 00:20:21,240
community too, that it's harmonious. The third quality I would say is not only that we're harmonious,

150
00:20:21,240 --> 00:20:27,880
but Samagi means it has the other implication, manga. Samagi literally means harmony,

151
00:20:27,880 --> 00:20:34,600
literally means being on the same path, or people who are on the same path, but it's more

152
00:20:35,240 --> 00:20:42,600
another important quality, it's not just any path. So if you're a harmonious bunch of thieves who

153
00:20:42,600 --> 00:20:48,680
are all stealing, or if you're a harmonious group of cannibals, I don't know how that would work,

154
00:20:48,680 --> 00:21:00,760
but harmonious group of hunters that say, not a lot of happiness there, but a harmonious group

155
00:21:00,760 --> 00:21:07,560
of Buddhist practitioners, of meditators, mind people who are mindful, so you're not only on the

156
00:21:07,560 --> 00:21:17,800
same path, but you're all on the path of mindfulness. There's a great happiness there. It means that

157
00:21:18,520 --> 00:21:24,280
we're not going to be distracting each other, or manipulating each other, or hurting each other,

158
00:21:25,160 --> 00:21:30,360
or going to be giving each other the space and the encouragement and the appreciation and the

159
00:21:30,360 --> 00:21:44,840
kindness that allows us to practice. And Samagi nantaposu kul, tapo comes from tapao meaning

160
00:21:44,840 --> 00:21:51,320
heat literally, but it came to mean much more in India even before the time of the Buddha. It means

161
00:21:52,120 --> 00:21:57,320
exertion, or it had a more religious significance, like asceticism,

162
00:21:57,320 --> 00:22:07,960
austerity, like torturing yourself. The Buddha called patience the highest form of torture,

163
00:22:07,960 --> 00:22:15,800
or tapao. The highest form of religious practice, you might say. So the patience,

164
00:22:16,600 --> 00:22:21,080
but here I think it means more like exertion, because that's really literally what the idea is

165
00:22:21,080 --> 00:22:29,640
when you work hard, you generate heat, so it came to mean work, exertion. So the work that we do,

166
00:22:31,960 --> 00:22:39,560
the fact that we're all working, the practice, and the engagement with the practice, and the

167
00:22:39,560 --> 00:22:54,760
intention, the inclination that we all have to practice is quite powerful.

168
00:22:56,440 --> 00:23:04,360
Thinking more broadly, it might be hard to see on this limited scale, there's only a few of us,

169
00:23:04,360 --> 00:23:12,600
but thinking more broadly. Imagine first just simply of all of a society where to

170
00:23:15,720 --> 00:23:25,000
understand the concepts of mindfulness, or to look at experiences and situations and problems

171
00:23:25,000 --> 00:23:33,160
of conflicts in terms of now I'm angry at you and trying to appreciate your anger and see it

172
00:23:33,160 --> 00:23:37,480
clearly so that you're not reacting to it, right? Rather than saying, you make me angry and I've

173
00:23:37,480 --> 00:23:45,400
got to kill you for that or hurt you for that or yell at you for that. Imagine a world where

174
00:23:45,400 --> 00:23:55,720
people were all meditating, imagine a world, society, even a city, if you think of all the

175
00:23:55,720 --> 00:24:04,440
conflicts that we have in this world, how many of them would be solved. So having people around

176
00:24:04,440 --> 00:24:13,960
you that are mindful and this is only a microcosm here where you're only here for short times

177
00:24:13,960 --> 00:24:20,120
and people are coming and going. But this idea of having a community we shouldn't take for granted,

178
00:24:20,120 --> 00:24:24,920
which you consider a part of our practice to encourage other people to practice.

179
00:24:30,200 --> 00:24:35,560
But another part of this is an exhortation, well not just to be Buddhists or not just to be

180
00:24:40,840 --> 00:24:46,200
not just to be in agreement that the practice is a good thing, but to actually practice.

181
00:24:46,200 --> 00:24:54,200
And so the great power that you are generating through your practice, the clarity of mind and

182
00:24:55,400 --> 00:25:04,200
wisdom becomes a communal thing. It's something that makes this a great place, a place of

183
00:25:04,200 --> 00:25:15,240
great benefit and great happiness. So appreciation, I don't think there's any

184
00:25:18,200 --> 00:25:25,000
any really deep teaching to be had here beyond that. The Dhamapada verses are often for

185
00:25:25,000 --> 00:25:37,400
encouragement. And so I think this is a very good verse for encouragement and a reminder

186
00:25:40,280 --> 00:25:47,640
of the sorts of qualities of a community, a reminder of the great

187
00:25:47,640 --> 00:25:56,440
fortune that we have to be here in a time where the Buddha has arisen, the Dhamma is being taught.

188
00:25:57,960 --> 00:26:05,320
We have other people practicing. We have other people who agree with the practice and who are

189
00:26:05,320 --> 00:26:13,720
putting out the practice or undertaking the practice. So that's the Dhamapada verse for tonight

190
00:26:13,720 --> 00:26:18,680
on happiness. What is true happiness when it comes from

191
00:26:19,960 --> 00:26:28,120
comes from progress. It's not something that you gain just by getting what you want,

192
00:26:29,400 --> 00:26:36,360
something that you gain by changing the nature of your relationship to reality.

193
00:26:36,360 --> 00:26:44,520
So rather than getting what you want all the time, the whole concept of wanting is brought into

194
00:26:44,520 --> 00:26:54,760
question. That's the real Dhamapada where you work to change the whole equation. So it's no longer

195
00:26:54,760 --> 00:27:02,280
am I getting what I want and how much of what I want? Am I getting to an understanding and

196
00:27:02,280 --> 00:27:09,880
an appreciation and the letting go of wanting? When you have a freedom from wanting, well,

197
00:27:09,880 --> 00:27:19,560
you have a freedom from not getting what you want. It's really a solution to the problem of how

198
00:27:19,560 --> 00:27:26,840
you can never be without what you want. Much better than things like rulership, if you think of

199
00:27:26,840 --> 00:27:33,720
happiness that these monks were talking about, you imagine being a ruler of a whole country or

200
00:27:33,720 --> 00:27:41,400
even a ruler of the whole world, just thinking about it and it sounds nice to be in control of

201
00:27:41,400 --> 00:27:47,560
people. If you think of what it is you're in control of, while the world is at least 50%

202
00:27:47,560 --> 00:27:58,040
evil and so you're basically taking ownership of a lot of garbage, a lot of trouble and conflict

203
00:27:58,040 --> 00:28:03,000
and suffering that you have to then deal with to imagine being in charge of that.

204
00:28:05,240 --> 00:28:12,520
Sensuality is really the number one enemy that we point to in Buddhism. Sensuality is the low

205
00:28:12,520 --> 00:28:18,840
hanging fruit to talk about because there's no question that it doesn't lead to true

206
00:28:18,840 --> 00:28:29,240
happiness. It's just a cycle of addiction. Food, I think, is even base right there. They hear the

207
00:28:29,240 --> 00:28:35,480
Buddha say, what are you talking about? I think it's not a surprise that he would say such a thing

208
00:28:35,480 --> 00:28:43,320
because food is sort of another obvious one. It's obvious that people are not happy or unhappy

209
00:28:43,320 --> 00:28:47,880
because they're getting the food that they want. Yes, if you're hungry all the time it's hard to be

210
00:28:47,880 --> 00:28:57,880
happy but in a deeper level it's not food. It's not even whether you're properly nourished or not

211
00:28:57,880 --> 00:29:05,320
in the body. There are many people who are very well-nourished and still suffering greatly

212
00:29:07,320 --> 00:29:15,080
but even on your deathbed when you're not able to eat. If your mind is pure you can be free

213
00:29:15,080 --> 00:29:31,000
from suffering. Anyway, that's the Damapada. Thank you all for listening. Have a good night.

