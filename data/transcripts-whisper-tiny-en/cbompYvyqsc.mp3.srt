1
00:00:00,000 --> 00:00:21,120
Good evening everyone, welcome to our live broadcast.

2
00:00:21,120 --> 00:00:33,000
So the Buddha's teaching is, if you've spent any time looking into it, it's quite a vast ocean

3
00:00:33,000 --> 00:00:49,760
of teachings and histories and stories and there's a lot there.

4
00:00:49,760 --> 00:00:55,600
You can make it somewhat daunting to try and understand even the simple aspects of how

5
00:00:55,600 --> 00:01:02,920
to practice meditation.

6
00:01:02,920 --> 00:01:09,440
So there are different ways of dealing with this, of approaching the Buddha's teaching when

7
00:01:09,440 --> 00:01:16,120
one explaining when teaching.

8
00:01:16,120 --> 00:01:22,440
One of them is, we see this in the Buddha's teaching, one of the ways is to pick one

9
00:01:22,440 --> 00:01:27,960
quality and talk about it at great length.

10
00:01:27,960 --> 00:01:36,400
So this is we, the zooming on a teaching.

11
00:01:36,400 --> 00:01:42,600
And they're called suttas and it's a good word whether or not it's what it originally

12
00:01:42,600 --> 00:01:43,600
meant.

13
00:01:43,600 --> 00:01:50,960
Suttamings of thread, so the idea is that you can start from one point and you can connect

14
00:01:50,960 --> 00:01:56,240
it to the rest of the whole.

15
00:01:56,240 --> 00:02:05,680
There are many ways of looking at Buddhism, looking at the Buddha's teaching.

16
00:02:05,680 --> 00:02:23,920
And there are many ways of approaching the same concept or the same idea, the same core.

17
00:02:23,920 --> 00:02:29,240
Another way of going about it, something that I'd like to look at today is the idea,

18
00:02:29,240 --> 00:02:36,280
giving an overview of the Buddha's teaching.

19
00:02:36,280 --> 00:02:42,080
And as with finding a single thread, there are also many ways of looking at the entire

20
00:02:42,080 --> 00:02:44,600
capestry of the Buddha's teaching.

21
00:02:44,600 --> 00:02:52,560
So there are many summaries, many ways of summarizing the Buddha's teaching, but I think

22
00:02:52,560 --> 00:03:01,840
one of the most macroscopic or all-encompassing is what the Buddha called the Bodhi

23
00:03:01,840 --> 00:03:18,840
Bhakya dhamma, Bodhi meaning enlightenment, Bhakya having a part, Bodhi Bhangya

24
00:03:18,840 --> 00:03:26,880
mean, Bhakya, having a part in enlightenment.

25
00:03:26,880 --> 00:03:33,600
Why I think this is one of the most all-encompassing is because there's 37 of them.

26
00:03:33,600 --> 00:03:41,880
So it's a list of lists, actually, it's a group of lists in total enumerating 37 dumbmas,

27
00:03:41,880 --> 00:03:45,320
37 aspects of the Buddha's teaching.

28
00:03:45,320 --> 00:03:51,560
I don't want to go into detail with these, but with each of them, but hopefully give

29
00:03:51,560 --> 00:03:58,720
a bit of an overview because they're all very much related to meditation, so it gives

30
00:03:58,720 --> 00:04:01,960
us a good idea of the sorts of things that we're cultivating.

31
00:04:01,960 --> 00:04:06,520
And the best way to look at this is not to think, wow, I have to study 37 individual

32
00:04:06,520 --> 00:04:15,880
teachings and that's how I get a complete understanding of practice in these two, enlightenment,

33
00:04:15,880 --> 00:04:24,840
but rather to understand that when we practice mindfulness, when we practice the force

34
00:04:24,840 --> 00:04:29,760
at Deepatana, there's a lot more that comes along with it, there's a lot of great things

35
00:04:29,760 --> 00:04:35,960
that come along with it, sort of help give us an idea of what it's like to practice

36
00:04:35,960 --> 00:04:42,920
mindfulness or what it should be like, which should be involved, so we can have some encouragement

37
00:04:42,920 --> 00:04:48,400
that we're practicing properly.

38
00:04:48,400 --> 00:04:59,160
So the first four of these are the force at Deepatana, the basis of the meditation practice.

39
00:04:59,160 --> 00:05:02,520
And these ones are obvious, this is where if you're not incorporating these four into

40
00:05:02,520 --> 00:05:10,240
your practice, well, it's hard to say that you're actually practicing to become enlightened.

41
00:05:10,240 --> 00:05:16,400
So it has to start here, these are the ones that it should be easy to understand how

42
00:05:16,400 --> 00:05:28,320
this relates to our practice or the body, so when we watch the movements of the body or

43
00:05:28,320 --> 00:05:36,080
the breath as it relates to the body, the stomach rising and falling, for example, mindfulness

44
00:05:36,080 --> 00:05:41,960
of the body when we watch the feelings and we're mindful, we remind ourselves, this is

45
00:05:41,960 --> 00:05:49,360
feeling, we need to know to wait and know, but see, we have it.

46
00:05:49,360 --> 00:05:56,920
This is pain, this is pleasure, this is calm, the reminding ourselves, this is the basis

47
00:05:56,920 --> 00:06:03,760
of the practice because it puts us in a position to be able to see things objectively

48
00:06:03,760 --> 00:06:10,440
without judgment, it reminds us, not me, it's not mine, it's not good, it's not bad,

49
00:06:10,440 --> 00:06:16,360
it focuses our attention on objective reality.

50
00:06:16,360 --> 00:06:26,360
The guy, we did not jit down the mind focusing on the mind and dumbas when we focus on the

51
00:06:26,360 --> 00:06:33,600
hindrances or the senses or the aggregates, this is all into the four foundations of mindfulness,

52
00:06:33,600 --> 00:06:42,520
so this is the basis, this one is clear how this is in our practice, it should be clear,

53
00:06:42,520 --> 00:06:49,600
and it's not, wow, this is where you have to start your learning.

54
00:06:49,600 --> 00:06:57,360
The second one is called the sum of baton, the four right exertions, it's another one of

55
00:06:57,360 --> 00:07:04,640
the Buddhist teachings and it also partakes of enlightenment or has a place apart, but

56
00:07:04,640 --> 00:07:08,360
what that means is that when you practice while you're cultivating these four as well, which

57
00:07:08,360 --> 00:07:14,080
will be encouraged that we're not just being mindful, we also have right effort, we're

58
00:07:14,080 --> 00:07:23,520
exerting ourselves properly, why, how do we know this?

59
00:07:23,520 --> 00:07:31,960
Right effort is not just about pushing yourself hard or working hard or hard or at

60
00:07:31,960 --> 00:07:39,960
something, it's about being meticulous in cultivating wholesomeness and maintaining wholesomeness.

61
00:07:39,960 --> 00:07:48,480
The four sum of baton are the effort to guard against unwholesomeness and to eradicate

62
00:07:48,480 --> 00:07:55,640
it when it's arisen, and the effort to cultivate wholesomeness and to protect it once

63
00:07:55,640 --> 00:07:56,640
it's arisen.

64
00:07:56,640 --> 00:08:02,080
So there's very much about practicing mindfulness, as I was saying, the objectivity that

65
00:08:02,080 --> 00:08:10,080
comes with that's the goodness and the judgment, the partiality, the aversion and the

66
00:08:10,080 --> 00:08:14,200
diction, these are the bad that we try to do away with.

67
00:08:14,200 --> 00:08:19,360
So simply by being mindful, you have already a right exertion and you're doing the right

68
00:08:19,360 --> 00:08:26,800
work and working in the right direction.

69
00:08:26,800 --> 00:08:34,000
The third is the Edi Bada, this is the third set, there's four of these as well, so

70
00:08:34,000 --> 00:08:37,720
we're up to 12.

71
00:08:37,720 --> 00:08:43,000
The Edi Bada are perhaps a little more prescriptive in the sense that they'll make and

72
00:08:43,000 --> 00:08:47,800
break your practice, it's possible to practice without them, but it's not possible to succeed

73
00:08:47,800 --> 00:08:53,680
without them, so this is a measure of our success, if you're wondering how to succeed

74
00:08:53,680 --> 00:08:58,920
in practicing mindfulness, well listen up, the Edi Bada, what you need, the first one

75
00:08:58,920 --> 00:09:06,120
is chanda, you need to like what you're doing, not exactly like, but you need to be

76
00:09:06,120 --> 00:09:15,320
content with it, chanda is this sort of catch-all phrase that can refer to desire, but it

77
00:09:15,320 --> 00:09:20,680
means that you're excited or energetic about what you're doing, there's something that's

78
00:09:20,680 --> 00:09:27,880
pushing you to do it, you have no interest in mindfulness, well, how can you possibly succeed,

79
00:09:27,880 --> 00:09:28,880
right?

80
00:09:28,880 --> 00:09:34,480
And this is what we see, you know, when you try to push mindfulness on others or when people

81
00:09:34,480 --> 00:09:39,440
bring their children to come in practice or their husbands and wives, try to drag their

82
00:09:39,440 --> 00:09:45,320
friends into practicing, you can say, well, it's very low success rate, the most of the

83
00:09:45,320 --> 00:09:55,920
time, there's no chanda involved, there's no interest in it, interest isn't something

84
00:09:55,920 --> 00:10:00,680
you can give to someone else, it's something that must be cultivated, so you have to ask

85
00:10:00,680 --> 00:10:06,760
yourself, what is worth being interested in, is mindfulness worth being interested in, am I

86
00:10:06,760 --> 00:10:11,960
interested in it, you're not interested in mindfulness, well, that's quite a scary thing,

87
00:10:11,960 --> 00:10:18,560
maybe it's time that you started evaluating your focus in life, what is it that you're

88
00:10:18,560 --> 00:10:23,320
interested in, why aren't you interested in mindfulness?

89
00:10:23,320 --> 00:10:28,080
Second one is effort, well, this just goes back to the second set, you need to put out

90
00:10:28,080 --> 00:10:31,320
effort and you need to work at it.

91
00:10:31,320 --> 00:10:39,840
And the third one is jitta, jitta means you have to keep it in mind and to keep your

92
00:10:39,840 --> 00:10:46,640
mind on the practice, if you're interested in it but you never pay attention, you never

93
00:10:46,640 --> 00:10:57,280
actually put your mind to it, you know, no, you won't succeed.

94
00:10:57,280 --> 00:11:06,920
And the fourth is we mung saw, which is sort of this discrimination where you evaluate

95
00:11:06,920 --> 00:11:11,680
your practice, this is the ability to step back and correct your practice, so jitta means

96
00:11:11,680 --> 00:11:23,520
pushing ahead, but dhamma means pushing ahead, but mung saw means adjusting, not just

97
00:11:23,520 --> 00:11:29,200
pushing ahead all the time, but pushing ahead and then realizing you're of course going

98
00:11:29,200 --> 00:11:34,080
in the other direction, so seeing what you're doing wrong, the ability to see the things

99
00:11:34,080 --> 00:11:39,920
that you're missing, ability to evaluate, wait a second, I'm not really being mindful

100
00:11:39,920 --> 00:11:47,800
here, or to succeed, you need these four, but these come up in the practice and they're

101
00:11:47,800 --> 00:11:53,600
the challenge that a meditator faces, cultivating all four of these, this is how you wrestle

102
00:11:53,600 --> 00:12:00,240
with the practice until you progress to succeed.

103
00:12:00,240 --> 00:12:11,080
The fourth and fifth set are the injury and the bala, and it's the same but different

104
00:12:11,080 --> 00:12:18,800
way of looking at the same five things, so you're sanda, vitya, sati, samadhi, banga.

105
00:12:18,800 --> 00:12:26,200
Sanda means confidence, you need confidence, vitya is effort, sati is mindfulness, samadhi

106
00:12:26,200 --> 00:12:34,680
is concentration, and banya is wisdom.

107
00:12:34,680 --> 00:12:38,400
And so here you start to say, oh, there's a lot of things to keep in mind, but it's not

108
00:12:38,400 --> 00:12:43,040
really like that, the point is that when you practice again, this five come up, I've

109
00:12:43,040 --> 00:12:48,280
talked about this extensively, that you shouldn't try to cultivate any one of these besides

110
00:12:48,280 --> 00:12:51,680
mindfulness, mindfulness is the balancing one.

111
00:12:51,680 --> 00:12:56,280
When you're being mindful, there's confidence, you're confident because you can see

112
00:12:56,280 --> 00:13:02,800
this is this, perfect, the most perfect confidence because you're actually knowing something.

113
00:13:02,800 --> 00:13:07,920
There's no belief involved, there's no logic or argumentation involved.

114
00:13:07,920 --> 00:13:15,000
This is this, when seeing is seeing, you can't argue with that, so you have confidence.

115
00:13:15,000 --> 00:13:20,840
And also that you see the way seeing works and hearing works and how the mind interacts

116
00:13:20,840 --> 00:13:27,920
with the body and there's perfect confidence because you're seeing it, you're knowing it.

117
00:13:27,920 --> 00:13:31,480
Effort comes because just by being mindful, there's right effort already, we've talked

118
00:13:31,480 --> 00:13:32,480
about that.

119
00:13:32,480 --> 00:13:38,480
So then mindfulness, concentration, while you're focused, when you say to yourself,

120
00:13:38,480 --> 00:13:45,480
rising, falling or seeing or hearing the mantra, the repetition and the reminding of yourself

121
00:13:45,480 --> 00:13:52,480
keeps you focused and five wisdom, wisdom is the understanding that comes from being mindful

122
00:13:52,480 --> 00:13:57,320
because when you look, you see, when you see, you know, if you want to know, you have

123
00:13:57,320 --> 00:14:03,320
to look, if you have to see, if you want to see, you have to look, look, see, you know,

124
00:14:03,320 --> 00:14:12,120
this is the progression, the order of progression, but you can't look and not see, and

125
00:14:12,120 --> 00:14:20,120
you can't see and not know, it's sufficient, sufficient to see and sufficient to look.

126
00:14:20,120 --> 00:14:24,360
So you don't have to go looking, is this true, is that too?

127
00:14:24,360 --> 00:14:32,280
There is impermanence, where is suffering, if you look, you'll see it.

128
00:14:32,280 --> 00:14:38,680
So these are the inria, we all have these faculties, Bala refers to the power, so the Buddha

129
00:14:38,680 --> 00:14:43,600
separated these out, and we talked about Indra, he would talk about the people's different

130
00:14:43,600 --> 00:14:51,640
levels of these things and how they can even become imbalanced, but Bala means the power,

131
00:14:51,640 --> 00:14:55,720
these are when they are well balanced, so he would talk about these, when he talked about

132
00:14:55,720 --> 00:15:02,960
the qualities of a Bhavadha, an enlightened being or a powerful meditator, they have these

133
00:15:02,960 --> 00:15:10,280
five powers, based on the faculties, that's the same five, so we add another 10 and we

134
00:15:10,280 --> 00:15:21,940
can get 22, the next set is called the Bodhjanga, Bodhjanga, this is another, another

135
00:15:21,940 --> 00:15:31,640
use of the word Bodhi, Bodhi, or Bodhjanga, Bodhjamini, it's the same as the word Bodhjaya,

136
00:15:31,640 --> 00:15:41,720
that means to wake up or to become enlightened or to know something, to come to know.

137
00:15:41,720 --> 00:15:47,280
So anger means it, just the factors, or anger actually means like a finger, well, no

138
00:15:47,280 --> 00:15:52,520
anger just means factor, remember.

139
00:15:52,520 --> 00:15:58,080
So the Bodhjanga is the factors of enlightenment, a smaller list than the 37, but another

140
00:15:58,080 --> 00:16:04,320
useful one as well, as you can see, all of these are ways of talking about the Bodhjaya,

141
00:16:04,320 --> 00:16:14,720
but they all come together as one, so the seven Bodhjanga are again sati, then we have

142
00:16:14,720 --> 00:16:28,000
Vyddhyya, the Sati, then the Dhamavichya, Vyddhyya, Bhiti, Bhasadi, Samadhyana, Bhaka, so Sati

143
00:16:28,000 --> 00:16:34,740
with sati is mindfulness, the factors of enlightenment start with mindfulness, and then

144
00:16:34,740 --> 00:16:38,720
you have Dhamavicayya, Vyddhyya, Vyddhyya, Vyddhyya, Vyddhyya, Vyddhyya, Vyddhyya, Vyddh, these

145
00:16:38,720 --> 00:16:43,280
three are the effort based ones, Dhamavicayya means the investigation or the learning,

146
00:16:43,280 --> 00:16:50,940
I think the seeing that comes from being mindful, really has the effort, the energetic

147
00:16:50,940 --> 00:17:00,380
nature of being mindful, and PT is the sort of excitement or the energy, the static

148
00:17:00,380 --> 00:17:11,080
charge that comes from the rapture, the groove, getting into a groove, sort of

149
00:17:11,080 --> 00:17:19,560
as PT, and building up the power, and then you have bus and d, which means

150
00:17:19,560 --> 00:17:24,800
tranquility, samadhi, it's just concentration again, and upika, which is

151
00:17:24,800 --> 00:17:33,680
equanimity. So the second, third, and fourth, these are effort based, the fifth,

152
00:17:33,680 --> 00:17:38,600
sixth, and seventh, these are tranquility based, and the effort and tranquility

153
00:17:38,600 --> 00:17:43,520
have to balance. If you're, this is where the one place where the Buddha actually

154
00:17:43,520 --> 00:17:52,000
said you could adjust, and you should cultivate the former three, if you're

155
00:17:52,000 --> 00:18:00,760
feeling trouser or lazy, and you should cultivate the last three, if you're

156
00:18:00,760 --> 00:18:07,080
feeling distracted or restless. But then he says you know mindfulness, mindfulness is

157
00:18:07,080 --> 00:18:13,720
what is always useful. Satin Shukohang bikame sadatikan wa dāmi, mindfulness is

158
00:18:13,720 --> 00:18:18,800
the one you use, when you use it, the other rest do balance out, just by being

159
00:18:18,800 --> 00:18:25,560
mindful. You can, you can adjust them, but it does leave room for that. He says,

160
00:18:25,560 --> 00:18:34,680
you know, focus on investigation and effort and rapture, if you're feeling

161
00:18:34,680 --> 00:18:41,120
drowsy, and focus on the tranquility, concentration, and the

162
00:18:41,120 --> 00:18:49,280
tranquility, if you're feeling restless. But if you don't really need to go that far,

163
00:18:49,280 --> 00:18:54,400
many times the Buddha would just talk about mindfulness. It's good to know and

164
00:18:54,400 --> 00:18:59,960
it's good to see when you're, again, using women's sadity, which one is in excess

165
00:18:59,960 --> 00:19:09,720
or in deficiency. So you can apply mindfulness properly. Ultimately, what we're

166
00:19:09,720 --> 00:19:15,840
looking for is this last one, a big, a big, and in terms of mindfulness or

167
00:19:15,840 --> 00:19:19,360
practice of mindfulness, it means seeing things objectively without any

168
00:19:19,360 --> 00:19:24,600
judgment, seeing everything that arises simply as it is. When you stop

169
00:19:24,600 --> 00:19:31,920
reacting to your experiences, that's the highest state of insight meditation.

170
00:19:31,920 --> 00:19:36,880
It's the peak, the pinnacle, so what we have to get to in order to become a

171
00:19:36,880 --> 00:19:44,120
light. Anyway, these seven are very much present in our practice. So there we go.

172
00:19:44,120 --> 00:19:49,600
We've added another seven quality. The final aid is the the eight full

173
00:19:49,600 --> 00:19:56,400
noble path. So this one is should be a very familiar to everybody. How this

174
00:19:56,400 --> 00:20:00,680
appears in your practice, it can often confound people. There's eight. It's the most

175
00:20:00,680 --> 00:20:05,480
so far. It's a lot. And so you think, well, you have to study a lot to learn about

176
00:20:05,480 --> 00:20:10,360
all aid of these. But again, it's not really like that in a practical sense. They

177
00:20:10,360 --> 00:20:15,600
really do come together. The easiest way to understand how they come together

178
00:20:15,600 --> 00:20:23,080
is to condense them down to the three trainings. The full noble path in its

179
00:20:23,080 --> 00:20:31,320
base is really only three things. We have morality or behavior. We have

180
00:20:31,320 --> 00:20:38,760
concentration and wisdom. Morality, concentration, wisdom. These are the

181
00:20:38,760 --> 00:20:42,920
basis of the full noble path. And you boil it down, that's where it's full

182
00:20:42,920 --> 00:20:48,240
noble path talks about. And that's useful because in a practical sense, you

183
00:20:48,240 --> 00:20:53,080
don't see, oh, here's right, right view, here's right thought and so on. It's not

184
00:20:53,080 --> 00:20:57,840
quite so clear. In practice, they sort of do all come together. It's very

185
00:20:57,840 --> 00:21:01,120
difficult to pick them out. Certainly, you don't see right action and right

186
00:21:01,120 --> 00:21:06,640
livelihood and right effort and right speech and right mindfulness, right

187
00:21:06,640 --> 00:21:12,800
concentration. Picking them apart is possible, but not so useful. So

188
00:21:12,800 --> 00:21:20,480
morality is the body and speech involved with meditation, which it's right

189
00:21:20,480 --> 00:21:26,000
because it's not there. You're not speaking. You're not acting. And moreover,

190
00:21:26,000 --> 00:21:35,840
your mind is not misbehaving. It's not creating unethical acts of body or

191
00:21:35,840 --> 00:21:40,880
speech. And you're by unethical. We're really in an ultimate sense,

192
00:21:40,880 --> 00:21:47,840
mean based on defilement. So when you walk quickly or when you wander around

193
00:21:47,840 --> 00:21:53,840
confused, those are all expressions of an unethical mind. They're

194
00:21:53,840 --> 00:22:01,400
considered to be unethical actions. When you make a fist, that's unethical.

195
00:22:01,400 --> 00:22:06,080
When you're in an end of anger, when you tense up out of fear, that's

196
00:22:06,080 --> 00:22:11,280
unethical. These are all unethical, meaning they're unethical, meaning they

197
00:22:11,280 --> 00:22:18,120
cause you suffering. That's all to mean. And so as you practice mindfulness,

198
00:22:18,120 --> 00:22:25,960
you become ethical. There's no more eating on mindfulness. There's no more

199
00:22:25,960 --> 00:22:30,920
speaking on mindfulness, drinking, walking, standing, sitting and lying, are

200
00:22:30,920 --> 00:22:35,400
all done mindfully. It's all of our actions. And when we speak, we speak

201
00:22:35,400 --> 00:22:43,480
mindfully. All of this is from the practice of becoming ethical.

202
00:22:43,480 --> 00:22:47,000
Concentration, of course. That should be fairly obvious if we've already talked

203
00:22:47,000 --> 00:22:52,200
about it. You become concentrated. But you become concentrated on

204
00:22:52,200 --> 00:22:57,560
on reality, which allows for wisdom to arise. The third one.

205
00:22:57,560 --> 00:23:01,720
Wisdom being right view and right thought. So our thoughts are right and our

206
00:23:01,720 --> 00:23:07,960
view is right. We have right view because we see things as they are.

207
00:23:07,960 --> 00:23:18,120
So the idea here is to give an overview. I was to see that, wow, there's a lot

208
00:23:18,120 --> 00:23:23,880
involved and get some encouragement as to the greatness of the Buddha's

209
00:23:23,880 --> 00:23:29,240
teaching. It really did cover a lot. I would have really did go in depth. He

210
00:23:29,240 --> 00:23:36,040
didn't just give us be mindful and we have to go on faith that that's

211
00:23:36,040 --> 00:23:40,200
somehow the right path. No, he was quite detailed.

212
00:23:40,200 --> 00:23:45,720
Look, when you're mindful, there's at least 37 things. There's a lot of

213
00:23:45,720 --> 00:23:50,920
different things that are involved here. A lot of good that comes from it.

214
00:23:50,920 --> 00:23:56,520
And it's quite complete in terms of talking about the various aspects of

215
00:23:56,520 --> 00:24:01,400
the mind, various aspects of experience.

216
00:24:01,400 --> 00:24:06,920
Also, it can give us some, it's good to go in detail like this because

217
00:24:06,920 --> 00:24:12,600
it maybe reminds us of some qualities of mind that we were

218
00:24:12,600 --> 00:24:20,280
ignoring or dismissing. They were overlooking. So it helps us look over our

219
00:24:20,280 --> 00:24:25,640
practice and to see maybe what we're lacking and to adjust our practice

220
00:24:25,640 --> 00:24:29,800
accordingly by being mindful of things. Simply by being mindful of things

221
00:24:29,800 --> 00:24:33,480
that we weren't being so mindful of.

222
00:24:34,840 --> 00:24:40,920
So there you go. A short exposition on the 37

223
00:24:40,920 --> 00:24:44,520
states that partake of enlightenment.

224
00:24:44,520 --> 00:25:04,360
That's the demo for tonight. Thank you all for tuning in.

