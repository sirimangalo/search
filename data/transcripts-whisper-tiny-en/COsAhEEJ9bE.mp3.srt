1
00:00:00,000 --> 00:00:13,320
okay good evening everyone welcome to our evening dumber and happy New Year to

2
00:00:13,320 --> 00:00:22,360
those who celebrate the Chinese New Year I don't personally celebrate the

3
00:00:22,360 --> 00:00:31,180
Chinese New Year it's not as good as I don't think we really have a New Year

4
00:00:31,180 --> 00:00:40,300
but Chinese New Year's in February ties Sri Lankan, Laos and Cambodian

5
00:00:40,300 --> 00:00:55,420
Burmese New Year are all in in and around April Western New Year of

6
00:00:55,420 --> 00:01:16,860
course is one month ago but I genuinely had this to say about the New Year he said

7
00:01:16,860 --> 00:01:23,580
may it have more to do may it have more meaning than simply in regards to the

8
00:01:23,580 --> 00:01:32,860
New Year because there's nothing new there's nothing new about this year

9
00:01:35,380 --> 00:01:47,700
seasons come and seasons go year in year out to cycle more things change the

10
00:01:47,700 --> 00:01:52,580
more they stay the same whatever that really means to me I'd rather suppose say

11
00:01:52,580 --> 00:02:05,460
things may change but they still just stay the same all these cliches it's all

12
00:02:05,460 --> 00:02:19,460
been done before but we have always the opportunity to make something new this

13
00:02:19,460 --> 00:02:31,820
is the great potential of now the potential to go in any direction to steer

14
00:02:31,820 --> 00:02:38,900
ourselves in any direction and so things do change this year does have many

15
00:02:38,900 --> 00:02:44,180
things that are different from last year for many of us

16
00:02:44,180 --> 00:02:53,060
but his point was let us not get caught up in our old habits let us leave

17
00:02:53,060 --> 00:03:12,540
behind the the rut the cycle the the loops that we get caught up in

18
00:03:12,540 --> 00:03:19,260
that causes suffering again and again let us not say oh another day another

19
00:03:19,260 --> 00:03:23,820
year for us to do the same thing a new year for us to make a massive things

20
00:03:23,820 --> 00:03:32,980
once more for us to suffer so reflecting back on a year is a good it's a

21
00:03:32,980 --> 00:03:39,220
useful tool to reflect and to think how that year went and to look forward and

22
00:03:39,220 --> 00:03:46,620
think of how how we can what we can do to better ourselves you know that

23
00:03:46,620 --> 00:03:51,620
which was good about last year let us keep it and improve upon it that which is

24
00:03:51,620 --> 00:04:00,380
was problematic about the last year let us change let us find something new

25
00:04:00,380 --> 00:04:06,300
the other aspect of new is that no matter what good or bad comes good or bad

26
00:04:06,300 --> 00:04:12,540
karma it's all still in the cycle the only thing that is truly new and is

27
00:04:12,540 --> 00:04:21,100
truly new is freedom freedom from the cycle because if we'd had freedom

28
00:04:21,100 --> 00:04:27,180
already we wouldn't be coming back to the cycle again and again so we strive

29
00:04:27,180 --> 00:04:31,140
to find what is truly new and that is the freedom from the cycle of some

30
00:04:31,140 --> 00:04:36,540
sorry freedom from suffering freedom from our bad habits freedom from our

31
00:04:36,540 --> 00:04:44,980
attachments freedom from those things those parts of ourselves that we

32
00:04:44,980 --> 00:04:48,140
better be better off without

33
00:04:48,140 --> 00:05:06,220
let us make something new but I think an accusation could be level that for

34
00:05:06,220 --> 00:05:10,700
most of us we aren't actually interested in the in the day or the year or any

35
00:05:10,700 --> 00:05:18,260
of that in fact I would I would accuse us of it's not a bad

36
00:05:18,260 --> 00:05:23,900
accusation it's just it's a mild accusation that we don't care so much

37
00:05:23,900 --> 00:05:32,260
about the holidays you don't care so much about our holidays themselves we

38
00:05:32,260 --> 00:05:42,260
care about them as an opportunity for for a celebration for recognition for

39
00:05:42,260 --> 00:05:50,660
not for recognition for community for the cultivation of certain activities

40
00:05:50,660 --> 00:05:53,900
qualities etc

41
00:05:54,900 --> 00:06:00,460
unfortunately a lot of the times the qualities that we are most interested in

42
00:06:00,460 --> 00:06:07,380
cultivating are the bad ones right so New Year's is a time of debauchery what we

43
00:06:07,380 --> 00:06:19,660
call celebration well it turns out to be a great big hedonistic pleasure

44
00:06:19,660 --> 00:06:25,060
fest that ends up with a lot of suffering there's it's one of the most dangerous

45
00:06:25,060 --> 00:06:33,940
days of the year and the western New Year's it's a time when there's a lot of

46
00:06:33,940 --> 00:06:37,660
drunk driving a lot of accidents

47
00:06:43,620 --> 00:06:48,940
but holidays in general are a time for us more than they are for the day you

48
00:06:48,940 --> 00:06:55,020
know celebrating the day seems kind of weird no the day doesn't

49
00:06:55,020 --> 00:07:00,300
certainly doesn't care usually the person that we're commemorating doesn't care

50
00:07:00,300 --> 00:07:05,740
Martin Luther King name one I'm pretty sure it's not such a big deal to him

51
00:07:05,740 --> 00:07:13,180
anymore but it's for us right it's for us to remember for us in fact often

52
00:07:13,180 --> 00:07:24,300
just for us to have a reason to gather and to cultivate whatever it is that

53
00:07:24,300 --> 00:07:31,740
we agree upon so it's Buddhists and you know where this is leading a holiday

54
00:07:31,740 --> 00:07:36,620
is much more to do with us being holy than about worrying about the

55
00:07:36,620 --> 00:07:42,580
whether the day is holy or not back the Buddha was rather critical of or not

56
00:07:42,580 --> 00:07:48,940
critical but pointed in in this regard he said any day

57
00:07:48,940 --> 00:07:55,300
there's then there's nakata nakata is I don't know what the word how the word nakata

58
00:07:55,300 --> 00:07:59,820
is a star I think or it has to do with the stars astrology or something

59
00:07:59,820 --> 00:08:07,340
anyway nowadays and Buddhist cultures it refers to refers to astrology you

60
00:08:07,340 --> 00:08:12,380
know fortune telling telling your horoscopes

61
00:08:12,380 --> 00:08:22,940
but the Buddha said a good star a lucky star but by star means lucky

62
00:08:22,940 --> 00:08:35,820
conjointing of the stars such a thing and a lucky day a lucky moment but

63
00:08:35,820 --> 00:08:42,020
does it mean a good moment so nakata nakata nakata nakata a good blessing or

64
00:08:42,020 --> 00:08:54,300
auspiciousness omen a good omen so mohos has never get this right there a lucky

65
00:08:54,300 --> 00:09:05,660
moment a good moment so papa dang so kano a good moment

66
00:09:05,660 --> 00:09:20,580
is when one was holy when one cultivates holiness so you remember the

67
00:09:20,580 --> 00:09:28,820
the kadikarata suta those of you who have heard this of course it's a

68
00:09:28,820 --> 00:09:36,580
memorable suta in the midgy minikai it occurs several times and repeatedly it

69
00:09:36,580 --> 00:09:40,820
was gathered together because this was a suta that many of the Buddha the

70
00:09:40,820 --> 00:09:47,300
Buddha and many of his disciples would repeat and so you have you have it

71
00:09:47,300 --> 00:09:59,980
repeatedly in the in the canon this is for the Buddha says a dita nanwakami napati kankai nagata

72
00:09:59,980 --> 00:10:05,940
one should not go back to the past and bring up the past or worry about the

73
00:10:05,940 --> 00:10:14,780
future yeah that dita nakata nakata nakata nakata nakata nakata nakata nakata nakata nakata

74
00:10:14,780 --> 00:10:22,060
in the future has not yet come but jupandana jayoda mung tata tata vipassate

75
00:10:22,060 --> 00:10:27,140
whatever arises in front of you but jupana means present but literally means

76
00:10:27,140 --> 00:10:38,340
what what arises in front of your face right in front of you tata tata vipassate

77
00:10:38,340 --> 00:10:45,340
let's see all of that clearly vipassate is where we get the word vipassana

78
00:10:49,820 --> 00:10:55,980
I'm going to talk about this this is as how you have a good day how you have a

79
00:10:55,980 --> 00:11:02,500
auspicious day special day a holy day really so it's really talking about

80
00:11:02,500 --> 00:11:11,220
holiday in Buddhism we have holidays many days that mark the procession of

81
00:11:11,220 --> 00:11:20,420
the year and the moon every every full moon is considered a holy day but

82
00:11:20,420 --> 00:11:25,060
just following indivism really and so the question was with the with the

83
00:11:25,060 --> 00:11:33,020
holidays of indivism they have all these various rituals and activities and in

84
00:11:33,020 --> 00:11:37,940
Indian society that these former Hindus were asking the Buddha what do we do

85
00:11:37,940 --> 00:11:42,620
what do we do as Buddhists on the full moon and the Buddha said well that the

86
00:11:42,620 --> 00:11:49,300
tradition of the noble ones is that on a holiday you would keep

87
00:11:49,300 --> 00:11:57,140
strict meditation practice meaning you wouldn't you wouldn't have sex or

88
00:11:57,140 --> 00:12:03,580
romantic activity you would only eat in the morning you wouldn't engage in

89
00:12:03,580 --> 00:12:09,620
entertainment or beautification and you would sleep on the floor you would

90
00:12:09,620 --> 00:12:16,300
live your life as a monastic or as a recluse as a renunciation really for

91
00:12:16,300 --> 00:12:21,740
that day so in effect you would you would become a holy person for that day

92
00:12:21,740 --> 00:12:27,340
this is the idea just so much better than anything else you could do giving

93
00:12:27,340 --> 00:12:32,460
gifts to the Buddha offering flowers to the Buddha

94
00:12:32,460 --> 00:12:36,980
Buddha himself said this isn't the way you honor a Buddha this isn't the way

95
00:12:36,980 --> 00:12:47,900
you have a holiday so we much better looking at the Buddha's teaching I mean

96
00:12:47,900 --> 00:12:58,220
using this this pattern for keeping ethical precepts and meditator precepts

97
00:12:58,220 --> 00:13:03,980
really and taking on renunciation precepts for the day and practicing the Buddha

98
00:13:03,980 --> 00:13:07,340
teaching being in the present moment we take these two together this idea of

99
00:13:07,340 --> 00:13:14,100
the keeping of the precepts for meditators and along with the idea of

100
00:13:14,100 --> 00:13:22,940
practicing insight meditation in the present moment it creates it makes it a

101
00:13:22,940 --> 00:13:28,100
holy day and couldn't make any day really a holy day but it's a great excuse

102
00:13:28,100 --> 00:13:40,740
and a great means of cultivating energy confidence psychologically having a

103
00:13:40,740 --> 00:13:47,340
day having a special day and where you can keep in mind for instance today is

104
00:13:47,340 --> 00:13:52,220
the new year so keeping that in mind psychologically we have this idea that

105
00:13:52,220 --> 00:13:58,660
okay you know what this is a good excuse to start and so on the new year now

106
00:13:58,660 --> 00:14:04,540
we make something new leave the past behind right this is what the new year

107
00:14:04,540 --> 00:14:10,260
really supposed to be it's psychologically a tool for us to say to ourselves

108
00:14:10,260 --> 00:14:16,460
let the old be old let's bring in the new do something new it's a great way

109
00:14:16,460 --> 00:14:22,860
to motivate us towards self-cultivation

110
00:14:36,980 --> 00:14:46,420
the Buddha was very much about the here and now any moment not make excuses

111
00:14:46,420 --> 00:14:53,420
not necessarily to wait for holidays to do good things

112
00:14:53,420 --> 00:14:57,220
they said that this makes us invincible staying in the present moment letting

113
00:14:57,220 --> 00:15:03,820
go of the past in the future being here and now this is unassailable

114
00:15:03,820 --> 00:15:19,620
sung here on the san kupan

115
00:15:19,620 --> 00:15:24,660
they said you have to do it today you know really we shouldn't wait for holidays

116
00:15:24,660 --> 00:15:30,860
the Buddha said Ajayva Kichamata Kojanya Maranang Sway who knows whether

117
00:15:30,860 --> 00:15:37,860
death will come tomorrow

118
00:15:41,980 --> 00:15:43,420
so we work now

119
00:15:43,420 --> 00:15:46,580
we work to better ourselves

120
00:15:46,580 --> 00:15:53,580
trying at all times to

121
00:15:53,580 --> 00:16:00,580
free ourselves to find happiness doing what's in our own best interest

122
00:16:00,580 --> 00:16:05,700
making ourselves holy coming better people

123
00:16:05,700 --> 00:16:11,900
it's always nice to have this time to take a break to step back

124
00:16:11,900 --> 00:16:16,020
and to say here this is what

125
00:16:16,020 --> 00:16:20,620
this is where I'm headed in the new year this is for the next 360

126
00:16:20,620 --> 00:16:25,620
some days this is the direction I'm going to head

127
00:16:25,620 --> 00:16:27,620
gives us a starting point

128
00:16:27,620 --> 00:16:37,420
so I'd like to wish you all a happy new year

129
00:16:37,420 --> 00:16:42,300
and I wish for you all to find holiness for yourselves

130
00:16:42,300 --> 00:16:47,220
find the present moment to learn how to always

131
00:16:47,220 --> 00:16:50,220
live in in holiness

132
00:16:50,220 --> 00:16:54,220
have a holy day every day

133
00:16:54,220 --> 00:16:57,020
there's the there's my

134
00:16:57,020 --> 00:17:01,500
I know rather short but I don't have a lot to say and I'm here every night

135
00:17:01,500 --> 00:17:05,980
so almost every night so there's the demo for this evening

136
00:17:05,980 --> 00:17:12,980
wishing you all the best

