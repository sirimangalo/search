1
00:00:00,000 --> 00:00:03,840
Hello and welcome back to Ask a Monk.

2
00:00:03,840 --> 00:00:10,080
Today's question is part of the two part question that I got mixed up last time and I ended

3
00:00:10,080 --> 00:00:12,560
up answering two different questions together.

4
00:00:12,560 --> 00:00:20,640
So this is answering the first part of someone's two part question that I missed.

5
00:00:20,640 --> 00:00:26,760
And the first part was in regards to remembering one's past lives.

6
00:00:26,760 --> 00:00:32,800
In regards to remembering one's past lives we get generally two questions, again and again.

7
00:00:32,800 --> 00:00:37,600
The first question is about how we can believe in past lives.

8
00:00:37,600 --> 00:00:38,600
What is the proof?

9
00:00:38,600 --> 00:00:40,120
What is the basis of past lives?

10
00:00:40,120 --> 00:00:46,200
Because obviously in Buddhism we're both the boast is that we don't take things on faith

11
00:00:46,200 --> 00:00:48,240
and so on.

12
00:00:48,240 --> 00:00:53,320
Actually there are many things that we could take on faith so to speak.

13
00:00:53,320 --> 00:00:58,400
I think past lives is actually partially under that category.

14
00:00:58,400 --> 00:01:06,840
We take a lot of things on faith because we believe in the knowledge and the wisdom of

15
00:01:06,840 --> 00:01:08,320
those people who have realized them.

16
00:01:08,320 --> 00:01:12,720
Like if you tell me what Australia is like, well I've never been to Australia but I believe

17
00:01:12,720 --> 00:01:13,720
you.

18
00:01:13,720 --> 00:01:14,720
I take it on faith.

19
00:01:14,720 --> 00:01:21,560
It's not something crucial and it's a sort of a faith that is based on the fact that

20
00:01:21,560 --> 00:01:27,200
I've never been there and I maybe don't have the ability to go there.

21
00:01:27,200 --> 00:01:32,360
If it were crucial to my practices to whether Australia was like this or like that then

22
00:01:32,360 --> 00:01:37,320
I'd probably want to go there and verify and see for myself that past lives could be

23
00:01:37,320 --> 00:01:40,480
the same if someone hasn't remembered their past lives.

24
00:01:40,480 --> 00:01:43,960
They might take it on faith so because it's not crucial.

25
00:01:43,960 --> 00:01:51,480
If it were crucial, this is the second question, is it not a beneficial part of the path

26
00:01:51,480 --> 00:01:57,280
to remembering your past lives and if it is then then why don't we teach it?

27
00:01:57,280 --> 00:02:00,280
Why don't we practice to remember our past lives?

28
00:02:00,280 --> 00:02:05,040
I think the answer is that we don't take it to be a crucial part of the path.

29
00:02:05,040 --> 00:02:10,240
So the person's logic who asked the person who asked this question, their logic was that

30
00:02:10,240 --> 00:02:16,760
there's the saying that we are what we are now because of what we were before and so we

31
00:02:16,760 --> 00:02:20,880
will be what we will be is because of who we are now.

32
00:02:20,880 --> 00:02:27,600
So if we learn about the past, the mistakes we've made and even the lessons we've learned

33
00:02:27,600 --> 00:02:33,560
and we'll be able to avoid mistakes and having to relearn the same lessons again for the

34
00:02:33,560 --> 00:02:37,520
future so that our future will be better.

35
00:02:37,520 --> 00:02:44,200
And I think there is some validity to that and so if the question is simply whether remembering

36
00:02:44,200 --> 00:02:50,160
one's past lives is beneficial, I think there's a lot of things that are beneficial

37
00:02:50,160 --> 00:02:57,000
to the meditation, to the practice of becoming enlightened, besides simply meditation

38
00:02:57,000 --> 00:03:04,760
that if you remember your past lives, if you are able to have all these magical powers

39
00:03:04,760 --> 00:03:11,240
that they talk about of being able to see far away, see Heaven, see Hell, to see beings

40
00:03:11,240 --> 00:03:20,000
being born and passing away, there's so many things that therapy is useful, regression

41
00:03:20,000 --> 00:03:24,480
therapy where you go back to when you were a young person or even now that regression

42
00:03:24,480 --> 00:03:29,040
therapy, regressing the past lives, going back and remembering your past lives through

43
00:03:29,040 --> 00:03:37,000
hypnosis or so on, it's said to allow people to become more comfortable and more understanding

44
00:03:37,000 --> 00:03:45,480
in regards to why they are the way they are so that they don't, they understand better

45
00:03:45,480 --> 00:03:49,160
the situation that they're in and they don't become upset by it.

46
00:03:49,160 --> 00:03:53,600
They can see that there's actually a cause and effect relationship and I mean seeing that

47
00:03:53,600 --> 00:03:59,280
sort of cause and effect relationship is really what the Buddha's teaching is about so

48
00:03:59,280 --> 00:04:02,280
it's quite useful I think.

49
00:04:02,280 --> 00:04:08,840
It's not crucial though and in the end it shouldn't be taken for a core practice because

50
00:04:08,840 --> 00:04:11,800
it's too abstract.

51
00:04:11,800 --> 00:04:18,240
The remembering of one's past lives as a cause, having a cultural relationship to the present

52
00:04:18,240 --> 00:04:25,080
is not really what we mean by cause and effect, the cause and effect that we're talking

53
00:04:25,080 --> 00:04:30,520
about is what's occurring here and now at every moment and you'll never become enlightened

54
00:04:30,520 --> 00:04:39,160
by this conceptual analysis of your life that you used to be like this and now you're

55
00:04:39,160 --> 00:04:46,000
like this and so on, it's far too abstract and the concentration and the intensity of

56
00:04:46,000 --> 00:04:53,160
the experience is not anywhere near strong enough to allow you to really understand the

57
00:04:53,160 --> 00:04:58,280
truth of reality that everything is impermanent and that there is nothing worth clinging

58
00:04:58,280 --> 00:05:06,440
to and so on and this is mainly because it's dealing with the past and the future, it's

59
00:05:06,440 --> 00:05:10,960
dealing with concept, it's not dealing with what you're seeing here and now so it's not

60
00:05:10,960 --> 00:05:15,240
the mind doesn't really get it, you can tell yourself that oh yes I was a bad person in

61
00:05:15,240 --> 00:05:19,880
the past that's why I have problems now therefore I shouldn't be a bad person now because

62
00:05:19,880 --> 00:05:26,520
I'll have a bad future, it really doesn't have any lasting impact on the mind or it

63
00:05:26,520 --> 00:05:33,720
has a limited impact on the mind as compared to looking to see what are the causes and

64
00:05:33,720 --> 00:05:38,760
effects of your actions in the present moment when I get angry what's it like, when I

65
00:05:38,760 --> 00:05:43,760
want something what's it like, when I am loving and kind what's it like, when I am clearly

66
00:05:43,760 --> 00:05:50,960
aware what's it like, looking at what does it mean to see something, to hear something

67
00:05:50,960 --> 00:05:56,920
and coming to change the way we relate to the world around us, this is really a lot more

68
00:05:56,920 --> 00:06:03,720
what the Buddha was teaching about, the Buddha said one should not go back to the past

69
00:06:03,720 --> 00:06:10,920
or worry about the future because what's in the past is gone and you know dwelling on

70
00:06:10,920 --> 00:06:18,480
it and thinking over it is only going to be a limited value if any and what's in the future

71
00:06:18,480 --> 00:06:23,440
hasn't come yet but if you can see clearly what's in the present moment this is where

72
00:06:23,440 --> 00:06:31,680
the cause and effect is occurring, this is empirical, this is you know scientific not based

73
00:06:31,680 --> 00:06:39,680
on faith so really whether we remember our past lives or not, if we come to see who we

74
00:06:39,680 --> 00:06:46,640
are right now then we don't have to worry about the future either and in fact I think

75
00:06:46,640 --> 00:06:53,400
a real part of the Buddha's teaching is giving up the future, not worrying about

76
00:06:53,400 --> 00:07:00,760
even who you're going to be in the future, not living in terms of past, present, future,

77
00:07:00,760 --> 00:07:05,480
living simply in terms of the here and now, who I am here and now what's happening here

78
00:07:05,480 --> 00:07:09,280
and now not worrying about where's this going to lead, what's going to happen, the dangers

79
00:07:09,280 --> 00:07:16,800
and the problems and so on, coming to see things here and now as they are and I think

80
00:07:16,800 --> 00:07:21,480
in the end you really give up past and future and you don't ever have any thoughts

81
00:07:21,480 --> 00:07:29,480
about what the future holds and what was in the past, so I think it's misleading to go

82
00:07:29,480 --> 00:07:35,760
back to the past because it's going to send you equally as far into the future, worrying

83
00:07:35,760 --> 00:07:42,960
or wondering or trying, planning to try to create a better life in the future and well

84
00:07:42,960 --> 00:07:47,440
that might be useful in the short term, eventually you forget it anyway because it's not

85
00:07:47,440 --> 00:07:52,960
empirical wisdom, it's conceptual, it's the same as thinking back to when we were young

86
00:07:52,960 --> 00:08:01,800
and all the mistakes we made, well yeah it made us more wise in a worldly sense and it made

87
00:08:01,800 --> 00:08:07,680
us more able to live and to react to the situations around us but it didn't help to stop

88
00:08:07,680 --> 00:08:12,520
us from getting greedy, getting angry, becoming attached and worried and stressed and

89
00:08:12,520 --> 00:08:18,320
so on, it's not strong enough, it's helpful but it's helpful on a limited scale so if

90
00:08:18,320 --> 00:08:22,440
you're really interested and I know there are a lot of people who are interested in learning

91
00:08:22,440 --> 00:08:28,800
about your past lives, there are ways of doing that, ways of practicing that are taught

92
00:08:28,800 --> 00:08:35,840
by taught in the textbooks really, these Buddha ancient Buddhist textbooks that have

93
00:08:35,840 --> 00:08:43,120
been around for so long and have been used by people to realize their past lives and

94
00:08:43,120 --> 00:08:53,200
so on, you're welcome to look into that but I don't think it will lead you to the goal

95
00:08:53,200 --> 00:08:58,760
and I think whether or not you've realized your past lives, there's still something

96
00:08:58,760 --> 00:09:02,880
much more important to do and especially for people who have limited time and energy

97
00:09:02,880 --> 00:09:08,760
and effort, it would be much better off to focus on what's really important and that's

98
00:09:08,760 --> 00:09:14,080
the cause and effect relationship that's going on here and now that has caused you to

99
00:09:14,080 --> 00:09:21,200
make plenty of mistakes in the past and will continue to have you make mistakes into the

100
00:09:21,200 --> 00:09:27,840
future unless you can come to see clearly on a phenomenological level what is the truth

101
00:09:27,840 --> 00:09:39,320
of reality, so beneficial yes, necessary no and probably not worth the effort for most

102
00:09:39,320 --> 00:09:45,640
people unless you've done a lot of time and good concentration and the peaceful surrounding

103
00:09:45,640 --> 00:09:53,160
and so on where you can really devote your time to that, okay so thanks for the question

104
00:09:53,160 --> 00:10:00,160
there you have it, all the best

