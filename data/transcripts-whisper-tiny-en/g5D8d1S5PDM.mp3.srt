1
00:00:00,000 --> 00:00:13,000
Hi, so today I thought I would talk about the first step in meditation or the first stage of progress.

2
00:00:13,000 --> 00:00:21,000
We understand that in the practice of meditation we are trying to go through a series of insights or realizations,

3
00:00:21,000 --> 00:00:33,000
starting with a simple realization of who and what we are, and sort of where our problems and deficiencies lie,

4
00:00:33,000 --> 00:00:42,000
and how we can help to remedy this, how we can begin to take the steps necessary to change this about ourselves.

5
00:00:42,000 --> 00:00:54,000
The first step in meditation practice is simply this basic awareness of who and what we are, of what is the reality that we are facing,

6
00:00:54,000 --> 00:01:03,000
sort of separation between what is a concept and what is reality.

7
00:01:03,000 --> 00:01:14,000
So, whereas normally in our ordinary everyday discourse we understand there to be a self or a soul or a being.

8
00:01:14,000 --> 00:01:25,000
You understand that I am a person, I am this, I am that, and we see this about everything around us as well.

9
00:01:25,000 --> 00:01:36,000
So when we look at something, we look at all of the things around us and we say this is a tree or this is a car or so on.

10
00:01:36,000 --> 00:01:44,000
And so we look at the world in terms of all of these concepts that we have created, these names which we have for everything.

11
00:01:44,000 --> 00:01:49,000
And we have to understand that in the meditation practice we are going to look at reality in a very different way.

12
00:01:49,000 --> 00:01:57,000
And the first thing that we realize in the practice of meditation is that reality has actually made up a very simple building blocks.

13
00:01:57,000 --> 00:02:05,000
Just as in science they understand the physical reality to be made up of atoms and subatomic particles.

14
00:02:05,000 --> 00:02:21,000
In the meditation practice we come to see reality based on the mind and the body working together as a set of building blocks as well.

15
00:02:21,000 --> 00:02:25,000
And these building blocks break down into two things.

16
00:02:25,000 --> 00:02:29,000
And as I have said already these two things are the body and the mind.

17
00:02:29,000 --> 00:02:36,000
The basic practice, the first stage of meditation in basic practice is to realize this.

18
00:02:36,000 --> 00:02:43,000
It is to come to understand that in every experience of reality there are only two things.

19
00:02:43,000 --> 00:02:52,000
So for instance when we see something normally we say I see a car, I see a bus, I see a person, I see this or that.

20
00:02:52,000 --> 00:02:58,000
When we start to practice meditation we come to see that in actual fact all there is the seeing.

21
00:02:58,000 --> 00:03:03,000
So this is we know from science that there is the light touching the eye then there is the eye.

22
00:03:03,000 --> 00:03:08,000
So we say this is the body or this is form, this is material.

23
00:03:08,000 --> 00:03:13,000
But then there is something else there that allows us to know that we are seeing.

24
00:03:13,000 --> 00:03:17,000
That allows there to be a knowledge of the seeing and this is called the mind.

25
00:03:17,000 --> 00:03:20,000
When we come to practice meditation we can see this.

26
00:03:20,000 --> 00:03:25,000
For instance when we watch the rising and the falling of the abdomen when we are sitting in meditation.

27
00:03:25,000 --> 00:03:31,000
And we say to ourselves rising, falling, rising, falling.

28
00:03:31,000 --> 00:03:34,000
We can see that there is a mind aware of the rising and falling.

29
00:03:34,000 --> 00:03:38,000
If the mind is not there we don't know that the stomach is rising and falling.

30
00:03:38,000 --> 00:03:44,000
For instance when the mind is distracted thinking something else we can see that there is no awareness.

31
00:03:44,000 --> 00:03:48,000
The only time that there is the awareness of the rising and falling is when the mind is there as well.

32
00:03:48,000 --> 00:03:54,000
So in the process of acknowledging rising and falling we can see there are two different things.

33
00:03:54,000 --> 00:04:00,000
Now this seems like a very simple thing to say but it is something that we don't ever pay attention to.

34
00:04:00,000 --> 00:04:04,000
We don't come to see that this is actually the building blocks of reality.

35
00:04:04,000 --> 00:04:06,000
That reality is not just the physical.

36
00:04:06,000 --> 00:04:13,000
Reality is not just what we are taught in school in terms of the science of the building blocks of the universe.

37
00:04:13,000 --> 00:04:20,000
Actually the universe comes from the mind that there is this aspect of the universe which we call the mind.

38
00:04:20,000 --> 00:04:29,000
We come to see that when we are walking there is the foot moving and there is the ground when we touch the floor and this is all material.

39
00:04:29,000 --> 00:04:31,000
But then there is also the knowing of it.

40
00:04:31,000 --> 00:04:45,000
And this becomes crucially important as we begin to progress in the meditation practice because it is the mind of course which has all of these mental sicknesses or what we label in the meditation practice is mental sicknesses.

41
00:04:45,000 --> 00:04:50,000
These are sicknesses and there are things which create suffering.

42
00:04:50,000 --> 00:04:53,000
So the suffering doesn't come from the physical even though we might have pain or so on.

43
00:04:53,000 --> 00:05:01,000
It is only when we get upset, when we get worried, when we get stressed, when we get afraid, when we have all of these negative emotions arise.

44
00:05:01,000 --> 00:05:04,000
That is when real suffering arises for us.

45
00:05:04,000 --> 00:05:13,000
So the first thing that we should start to become aware of when we undertake the practice of meditation is the separation.

46
00:05:13,000 --> 00:05:20,000
But coexistence or co-interdependence between body and mind that they work together.

47
00:05:20,000 --> 00:05:23,000
And as we go on we will come to see more and more about the mind.

48
00:05:23,000 --> 00:05:32,000
But I thought just to sort of get an idea of how we are going to approach reality or what we are going to start looking at.

49
00:05:32,000 --> 00:05:36,000
This is called the first stage of insight knowledge in the meditation practice.

50
00:05:36,000 --> 00:05:40,000
It is the simple awareness, the simple realization that there really is no eye.

51
00:05:40,000 --> 00:05:43,000
There is no knee and in everything around us it is the same.

52
00:05:43,000 --> 00:05:49,000
There is really just the experience seeing, hearing, smelling, tasting, feeling and thinking.

53
00:05:49,000 --> 00:05:54,000
And the building blocks which make up this which are in essence the body and the mind.

54
00:05:54,000 --> 00:06:04,000
So this is sort of used as an introduction to the progress, the path of progress which we are embarking upon in the meditation practice.

55
00:06:04,000 --> 00:06:13,000
And I think this is an important addition to the videos on how to meditate. If you would like more information, you are welcome to get a touch with me.

56
00:06:13,000 --> 00:06:19,000
You can contact me over YouTube or over Skype or so on.

57
00:06:19,000 --> 00:06:23,000
I am happy to go through these stages and help you to progress in them.

58
00:06:23,000 --> 00:06:32,000
As for the later on stages I think it is important to understand that you do need a teacher to accurately assess your practice and lead you on in the practice.

59
00:06:32,000 --> 00:06:41,000
So that you are able to realize the higher stages and I probably won't be going into too much more detail about the later stages of practice over YouTube.

60
00:06:41,000 --> 00:06:46,000
But if you like instruction on meditation, I am happy to teach people over Skype.

61
00:06:46,000 --> 00:06:51,000
You can Skype me and you that have more just my, the same as my YouTube channel.

62
00:06:51,000 --> 00:06:53,000
And I will be happy to answer your questions.

63
00:06:53,000 --> 00:07:01,000
So thanks for tuning in and I will try to add more information as I can think of things that may be useful for the meditators.

64
00:07:01,000 --> 00:07:06,000
I hope again that the meditation is able to bring you peace, happiness and freedom from suffering.

65
00:07:06,000 --> 00:07:31,000
Thanks for listening. Thanks for watching. Have a good day.

