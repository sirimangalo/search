1
00:00:00,000 --> 00:00:21,120
So tonight's question is about protecting one's practice as a lay person, the person

2
00:00:21,120 --> 00:00:33,400
asking expresses their distress at and seeing how easy it is to fall away from the practice,

3
00:00:33,400 --> 00:00:42,360
even for those who have done practice before, but find themselves back in worldly life and

4
00:00:42,360 --> 00:00:53,680
find it very hard to keep up their practice, so I thought about this and there were three

5
00:00:53,680 --> 00:00:58,160
things I can say about this, that's what I came up with.

6
00:00:58,160 --> 00:01:05,520
The first thing I would say is that in some respects, and this is probably a little bit

7
00:01:05,520 --> 00:01:11,240
of a disappointment, but in some respects this question is outside of the sort of question

8
00:01:11,240 --> 00:01:20,200
I'm comfortable answering, but I think there's at least a small lesson there and that's

9
00:01:20,200 --> 00:01:34,600
in regards to our practice and our perspective as Buddhist practitioners.

10
00:01:34,600 --> 00:01:46,880
I teach meditation, I think it helps my practice, it fulfills my sort of duty I can say to other

11
00:01:46,880 --> 00:01:56,920
beings, it feels comfortable, something I can do for others and it's not a great distraction

12
00:01:56,920 --> 00:02:03,880
from my own spiritual development, but that's the same for anyone and that's for

13
00:02:03,880 --> 00:02:12,800
people who are interested in practicing, trying to help people who are not interested

14
00:02:12,800 --> 00:02:17,720
in practicing find ways to be interested in practicing, has always felt a little bit

15
00:02:17,720 --> 00:02:24,200
outside of the realm of what is useful and beneficial.

16
00:02:24,200 --> 00:02:29,080
So we're starting to enter into that realm I think with this question.

17
00:02:29,080 --> 00:02:34,840
We'll ask me often, I often get adults wanting me to teach their children how to meditate

18
00:02:34,840 --> 00:02:45,280
and I've always found that profoundly wrong or fundamentally unpleasant, not unpleasant

19
00:02:45,280 --> 00:02:56,200
word is improper, giving an awkward sort of feeling like not quite right for this reason.

20
00:02:56,200 --> 00:03:01,800
Because you're talking about people who don't want to meditate, but want to want to meditate

21
00:03:01,800 --> 00:03:10,160
and hear their parents want them to want to meditate, so if you take a person who's not

22
00:03:10,160 --> 00:03:15,480
interested in meditation but really would like to be interested in meditation, that's

23
00:03:15,480 --> 00:03:27,960
the sort of problem that comes out, you know, lay Buddhists who like the idea of meditating

24
00:03:27,960 --> 00:03:35,560
but find themselves distracted by so many things, so there's certainly no sense of criticizing

25
00:03:35,560 --> 00:03:45,440
or blaming anyone who gets distracted by worldly life, but there should be I think

26
00:03:45,440 --> 00:03:53,720
no expectation for anyone but themselves to find a way to overcome that.

27
00:03:53,720 --> 00:04:00,880
To some extent, I think we have to understand that the Buddha wasn't even a savior.

28
00:04:00,880 --> 00:04:05,320
The Buddha didn't teach lay people and he taught some things that were quite useful for

29
00:04:05,320 --> 00:04:16,160
lay people and very much outside of the purview of the realm or the limit or the range

30
00:04:16,160 --> 00:04:29,680
of meditation practice, but even the Buddha had very little to say beyond describing right

31
00:04:29,680 --> 00:04:36,400
ways to live your life.

32
00:04:36,400 --> 00:04:43,240
There's no limit to our potential to help other beings if we want to go further, we could

33
00:04:43,240 --> 00:04:49,760
help beings in hell, we could help animals figure out how to get dogs to be interested

34
00:04:49,760 --> 00:04:55,000
in good things that would eventually perhaps allow them to be born as humans.

35
00:04:55,000 --> 00:05:01,400
And in the week, there's no limit to the amount of help we could give to other beings, but

36
00:05:01,400 --> 00:05:02,720
that's not certainly my goal.

37
00:05:02,720 --> 00:05:08,320
I don't think it's a goal that the Buddha advised in his followers.

38
00:05:08,320 --> 00:05:12,880
And so I think it's a good lesson for us to remind ourselves of a certain perspective

39
00:05:12,880 --> 00:05:19,480
and a balance, and the balance always has to be weighted in favor of our own spiritual

40
00:05:19,480 --> 00:05:20,480
development.

41
00:05:20,480 --> 00:05:26,120
It really helps others if our attention is always on helping others.

42
00:05:26,120 --> 00:05:30,520
There's of course helping anyone yourself or another person depends upon your own spiritual

43
00:05:30,520 --> 00:05:37,680
development, and one's own spiritual development has to require preponderance of attention

44
00:05:37,680 --> 00:05:47,440
to one's own practice, first thing I'll say, but one other thing that's also a little

45
00:05:47,440 --> 00:05:55,640
bit discouraging, I think, is that fundamentally we're asking, the question is asking,

46
00:05:55,640 --> 00:06:05,080
or in a part of it asking, or something that is impossible or contradictory, it's

47
00:06:05,080 --> 00:06:11,640
like the expression of having your cake and eating it too, it's like saying, how can

48
00:06:11,640 --> 00:06:14,880
I be a really good meditator and live in the world?

49
00:06:14,880 --> 00:06:21,920
Honestly, you can't, and the part of one of the most important, one important support

50
00:06:21,920 --> 00:06:27,280
of meditation practice is leaving the world physically, of course, mentally much more

51
00:06:27,280 --> 00:06:35,680
important, and technically, is it possible for someone to live a spiritual life in the

52
00:06:35,680 --> 00:06:36,680
world?

53
00:06:36,680 --> 00:06:43,400
Technically, it's possible, but to find a way for an ordinary average person like you

54
00:06:43,400 --> 00:06:54,000
or me to do that, and then be distressed by how difficult it is, and wonder how you can

55
00:06:54,000 --> 00:06:56,480
make it easier, it's really not the case.

56
00:06:56,480 --> 00:07:01,040
How you can make it easier is to be a lot stronger, a lot more perfect, and a lot more

57
00:07:01,040 --> 00:07:10,360
pure, which I'm not expecting anyone to be, I don't consider myself to be like that.

58
00:07:10,360 --> 00:07:19,200
It's always by people who practice Buddhism by the Buddha himself and described, been

59
00:07:19,200 --> 00:07:34,400
said that lay life is a dusty road, it's all confined and restricted and mixed up, it's

60
00:07:34,400 --> 00:07:41,840
not like, in the olden days, in the time of the Buddha, when there was a lot of traffic

61
00:07:41,840 --> 00:07:48,200
there would be a great amount of dust, you'd have horses and just people walking and it

62
00:07:48,200 --> 00:07:54,040
would be a crowded road, it would be very dusty and dry and not very pleasant because

63
00:07:54,040 --> 00:07:58,840
people would be defecating on the side of the road and that sort of thing, garbage on

64
00:07:58,840 --> 00:08:10,520
the side of the road, and they say in comparison, leaving home and going to live in seclusion

65
00:08:10,520 --> 00:08:22,040
a simple life is the open air, it feels much more fresh, much more pure and free, and

66
00:08:22,040 --> 00:08:30,640
so to ask about how to make meditation in lay life easier is problematic at its core

67
00:08:30,640 --> 00:08:38,080
because I think you're much better served understanding how difficult it is and it's for

68
00:08:38,080 --> 00:08:45,400
some people it's not possible for them to leave it behind and so important for them

69
00:08:45,400 --> 00:08:49,080
will be to understand that it's not going to be easy and I think part of our practice

70
00:08:49,080 --> 00:08:57,600
is always about understanding and coming to terms with difficulty and I think to some

71
00:08:57,600 --> 00:09:07,640
extent, all ever trying to make your practice easier is problematic so instead of looking

72
00:09:07,640 --> 00:09:12,960
at it as something, wow this is really difficult and I'm struggling, try and reframe

73
00:09:12,960 --> 00:09:18,600
it and learn to be mindful of the struggle and mindful of what it is that is causing

74
00:09:18,600 --> 00:09:28,320
the struggle and so on, which brings me to the third thing which is more hopeful, a little

75
00:09:28,320 --> 00:09:32,840
bit critical as well, I don't mean to be entirely critical but it's critical in general

76
00:09:32,840 --> 00:09:42,360
of us as Buddhists, as meditators, we often focus solely on meditation practice and so

77
00:09:42,360 --> 00:09:47,520
you come here to practice and it can be forgiven because that's how we teach you and

78
00:09:47,520 --> 00:09:51,320
that's really what we give you so you come here you learn how to meditate and you say

79
00:09:51,320 --> 00:09:56,960
okay, I've done meditation, then you go home and you try to continue to meditate like

80
00:09:56,960 --> 00:10:04,860
that and it doesn't work or it's problematic and this is because even here meditation

81
00:10:04,860 --> 00:10:12,640
requires support, much of your support is given to you artificially, your livelihood

82
00:10:12,640 --> 00:10:17,760
is pure here and you don't have to even go to work for it, you don't have to demean

83
00:10:17,760 --> 00:10:25,320
yourself doing something meaningless in order to get food and shelter and so on but your

84
00:10:25,320 --> 00:10:34,000
environment is pure, there's no distractions, there's nothing pulling you here or there,

85
00:10:34,000 --> 00:10:40,240
desire, there's no conflict with other people, there's no manipulation or anything like

86
00:10:40,240 --> 00:10:47,920
that and so it feels like wow this is okay all I have to do is meditate but you go home

87
00:10:47,920 --> 00:10:58,680
and none of those supports are there and of course even here there is, there can be

88
00:10:58,680 --> 00:11:03,960
the lack of support, many people come to practice and aren't able to continue or finish

89
00:11:03,960 --> 00:11:11,360
the course because they lack the support, so it's been good in general for us to do

90
00:11:11,360 --> 00:11:18,080
this correspondence course, the online course because it first of all provides a filter

91
00:11:18,080 --> 00:11:23,920
if you're really not ready to do a course here, here you'll figure it out in the online

92
00:11:23,920 --> 00:11:33,920
course, if you're not able to do that then you would make it, it means it means

93
00:11:33,920 --> 00:11:41,360
it selects the people who are really keen to do it but it also prepares you, it's a great

94
00:11:41,360 --> 00:11:48,160
way to understand the dynamics of the practice and what is necessary to use as a support

95
00:11:48,160 --> 00:11:56,960
how to begin to organize your life in a way that is supportive of meditation practice but

96
00:11:56,960 --> 00:12:05,600
deeper than that there are qualities of individuals that are required and there's something

97
00:12:05,600 --> 00:12:14,520
we call Upanishaya, Upanishaya is what you come into this world with, well that's how it's

98
00:12:14,520 --> 00:12:22,480
talked about in Thai I think, probably the word is we say it, we say it means Upanishaya

99
00:12:22,480 --> 00:12:26,680
maybe as well anyway there's this idea of what you bring into this life meaning some

100
00:12:26,680 --> 00:12:33,800
people are just never going to be into practice or capable of practicing, capable of being

101
00:12:33,800 --> 00:12:42,480
interested in practicing and for most of us we're kind of halfway we have some interest

102
00:12:42,480 --> 00:12:48,520
and capability of practicing but it's rough and it's difficult, that's because we haven't

103
00:12:48,520 --> 00:12:54,480
brought a great support into this life and it can also be because we're not developing

104
00:12:54,480 --> 00:13:04,080
or our development of support is often incomplete or partial and so these are things like

105
00:13:04,080 --> 00:13:10,480
being a good person, being kind, being generous, having a pure heart because what we mean

106
00:13:10,480 --> 00:13:14,760
by being a good person is really that, having a pure mind and a pure heart and the pure

107
00:13:14,760 --> 00:13:21,960
or your foundation as an individual is of course the easier it is to have pure thoughts,

108
00:13:21,960 --> 00:13:32,200
clear thoughts, steady and coherent thoughts and so this applies to lay people, it's a

109
00:13:32,200 --> 00:13:39,920
big reason I think why you can, I can agree that much of the teaching that is given to

110
00:13:39,920 --> 00:13:47,200
lay people, lay Buddhists is proper, we often hear criticism of monks who teach lay people

111
00:13:47,200 --> 00:13:52,400
only very shallow dhamma and they should be teaching deeper dhamma and it's true to some

112
00:13:52,400 --> 00:14:00,560
extent but it's also forgivable because really things like being generous and keeping

113
00:14:00,560 --> 00:14:06,920
morality and practicing basic meditation like loving kindness and so on may not get you

114
00:14:06,920 --> 00:14:16,840
to nibana but it certainly creates a great foundation, it is wrong to stop there and

115
00:14:16,840 --> 00:14:23,320
never hint at something more, you should always be leaving the door open for people who

116
00:14:23,320 --> 00:14:28,680
are ready to make the jhamma because they are always, of course, lay many lay people to

117
00:14:28,680 --> 00:14:35,200
be a monk to want to practice meditation but the foundation is equally important and it's

118
00:14:35,200 --> 00:14:42,280
not just the foundation, it's a support, I would make this analogy of a tree, if you want

119
00:14:42,280 --> 00:14:48,920
to grow a tree from a sapling, you can't just plant it and leave it, you need support

120
00:14:48,920 --> 00:14:53,520
through the winters especially here in Canada we know this because even just the snow

121
00:14:53,520 --> 00:14:58,520
and the cold you have to protect it from them and do other sticks to prop it up and

122
00:14:58,520 --> 00:15:08,040
so on and so even when you're practicing meditation especially not here where you have everything

123
00:15:08,040 --> 00:15:15,720
sort of laid out for you, you need to set up your life in ways that are wholesome, conduct

124
00:15:15,720 --> 00:15:22,680
good works, good deeds, do good things for the world around you even in terms of teaching

125
00:15:22,680 --> 00:15:28,720
helping other people come to the practice can be a very good way to support your own practice

126
00:15:28,720 --> 00:15:34,800
because it creates, it gives you this environment, it surrounds you with people who are interested

127
00:15:34,800 --> 00:15:46,440
in meditation, it challenges you and tests you, it keeps you straight, another important

128
00:15:46,440 --> 00:15:53,520
one is something like your association with other people, not something like that in and

129
00:15:53,520 --> 00:16:00,640
of itself is one of the most important qualities, right so having teaching other people

130
00:16:00,640 --> 00:16:06,240
is a part of that but just associating with other practitioners, having a meditation

131
00:16:06,240 --> 00:16:11,600
group that you go to sit with or whatever, being surrounded by other people is very important

132
00:16:11,600 --> 00:16:20,000
for people who are in a good place where you live, what you do, your work, there's so

133
00:16:20,000 --> 00:16:26,640
much and it starts to get as I said outside of the purview of what I, the realm of what

134
00:16:26,640 --> 00:16:33,840
I consider mind duty as a, as a meditation teacher but it's just the general understanding

135
00:16:33,840 --> 00:16:38,080
that there's more to the practice than the practice you need, it's why I teach the

136
00:16:38,080 --> 00:16:43,320
Sabasa who took quite often because it reminds us of all the different things we need

137
00:16:43,320 --> 00:16:49,880
to, different aspects of our life that we need to consider as a protection for our

138
00:16:49,880 --> 00:17:00,360
people. And the last part of that is something that I think could really help is our closeness

139
00:17:00,360 --> 00:17:10,240
with Buddhism which to some extent is artificial, right our connection with concepts like

140
00:17:10,240 --> 00:17:16,320
the concept of the Buddha, the Dhamma, the Sangha, even just the statues, having some

141
00:17:16,320 --> 00:17:25,680
kind of image in our mind, the visualization of a Buddha image, the association with monks

142
00:17:25,680 --> 00:17:37,280
and chanting and rituals, candles, flowers, incense, these kind of things and the sort

143
00:17:37,280 --> 00:17:43,120
of determinations that we make may I always be close to the Buddha sassana, they can often

144
00:17:43,120 --> 00:17:50,640
be artificial, but they can also be quite helpful meaning a person could be a very good

145
00:17:50,640 --> 00:17:57,560
person, have a very good qualities of mind and to some extent in the headget here because

146
00:17:57,560 --> 00:18:02,280
it's not entirely true but to some extent even though they have great good qualities if

147
00:18:02,280 --> 00:18:09,960
they've never had any connection with Buddhism as a concept, as a construct, as an institution,

148
00:18:09,960 --> 00:18:14,480
they may just never meet up with another Buddhist, they may not have the karmic connection

149
00:18:14,480 --> 00:18:23,440
or the direction to ever find a way to go further because goodness in and of itself

150
00:18:23,440 --> 00:18:28,920
doesn't necessarily on a conventional level lead to enlightenment, they could just lead

151
00:18:28,920 --> 00:18:34,560
you to happy rebirths, heaven and so on. There are many non-Buddhists who are good people,

152
00:18:34,560 --> 00:18:40,640
Christians and Muslims and Jews and Hindus and all religions really have these people

153
00:18:40,640 --> 00:18:45,680
and because these religions have a part of them that teach us goodness, some people

154
00:18:45,680 --> 00:18:51,440
take them up on that and cultivate it really quite well but they never make it to Buddhism

155
00:18:51,440 --> 00:19:02,960
often because of their connection, the images of the crucifix or any other religious symbol

156
00:19:02,960 --> 00:19:09,280
artificially, it sends them in that direction and it inclines them to meet up with those

157
00:19:09,280 --> 00:19:14,880
people, the dhamma wheel if you have these sort of symbols. So what I'm trying, what this

158
00:19:14,880 --> 00:19:19,720
all means and how this carries out in practice is that things like getting involved with

159
00:19:19,720 --> 00:19:25,800
Buddhist culture can be very useful, culture, Buddhist culture can be a very useful thing

160
00:19:25,800 --> 00:19:32,240
as a shall, as a means of supporting what is really and truly Buddhism. And a part of this

161
00:19:32,240 --> 00:19:38,360
is, as I said, the determinations, I think a part of this is where we make a determination

162
00:19:38,360 --> 00:19:45,640
for example, people make determinations to be reborn in the time of Matea, the next Buddha.

163
00:19:45,640 --> 00:19:52,240
That's a good determination to make, I don't think it's any in any way a replacement

164
00:19:52,240 --> 00:19:57,680
for actual practice, it should never be, but as a part of our practice to remember and

165
00:19:57,680 --> 00:20:04,200
to think and to even people will send prayers to Matea and not prayers, but in the sense

166
00:20:04,200 --> 00:20:11,640
of chanting and making a determination, paying respect to Matea, paying respect to the

167
00:20:11,640 --> 00:20:18,400
Buddha, the dhamma, the sanga in the form of chanting can be very helpful to set your

168
00:20:18,400 --> 00:20:25,480
mind in a way that's close to the Buddha's teaching. And it's not just for a future,

169
00:20:25,480 --> 00:20:30,600
it's for your own present life, this isn't just so that one day I might be reborn

170
00:20:30,600 --> 00:20:36,920
in with Matea, it keeps you close to the Buddha's ass and it keeps you in touch with

171
00:20:36,920 --> 00:20:42,520
other Buddhists, even if they aren't practicing, maybe they've studied Buddhism and can

172
00:20:42,520 --> 00:20:47,960
remind you of certain things. We often learn a lot as meditators from scholars because

173
00:20:47,960 --> 00:20:55,040
they have a lot of knowledge and they're able to explain things in a very, sometimes

174
00:20:55,040 --> 00:21:03,600
not always, but often in a very clear and logical manner. But the other side of that

175
00:21:03,600 --> 00:21:11,520
and it's the last thing I'll say is that to some extent it's just our purity of mind

176
00:21:11,520 --> 00:21:17,120
and to some extent goodness is enough. Really Buddhism ultimately isn't an artificial,

177
00:21:17,120 --> 00:21:25,440
religious thing institution, it's just about seeing reality. And so in regards to life as

178
00:21:25,440 --> 00:21:35,920
practice being difficult in lay life and the struggle to many, in many respects this is just

179
00:21:35,920 --> 00:21:47,440
our failing as Buddhist practitioners and it's what we have to recognize that we are in

180
00:21:47,440 --> 00:21:57,360
complete. Often we make goals, we fixate on the goal and I'm going to practice and it's

181
00:21:57,360 --> 00:22:02,080
going to work and I'm going to become enlightened. When in fact, it may take your lifetimes

182
00:22:02,080 --> 00:22:06,720
to become fully enlightened and it certainly may take much more than just sitting down

183
00:22:06,720 --> 00:22:13,040
and practicing meditation, not to trivialize the core importance of meditation, of course,

184
00:22:13,040 --> 00:22:24,800
it's by far the most important thing. But rather to say that our meditation might be difficult

185
00:22:24,800 --> 00:22:30,560
and problematic, take the example of a person who stops meditating. Once to get back into meditation

186
00:22:30,560 --> 00:22:37,200
it finds themselves distracted by so many things. They're perspective I think it's important

187
00:22:37,200 --> 00:22:42,960
that it changes, that they rather than think of it, oh I'm not meditating and therefore I'm

188
00:22:42,960 --> 00:22:47,360
failing or so on or this person is not meditating and therefore they're failing. We should look

189
00:22:47,360 --> 00:22:56,000
rather at our situation and how we can get closer to freedom from suffering and that's not

190
00:22:56,000 --> 00:23:04,000
always through meditation but I think it always is sort of things like mindfulness. Walking down

191
00:23:04,000 --> 00:23:08,720
the street and mindfully. Having a different perspective instead of saying I'm a failure so I

192
00:23:08,720 --> 00:23:13,840
just might as well give up to say no. When I was walking down the street I was mindful right now

193
00:23:13,840 --> 00:23:20,400
I'm walking on the street and I can be mindful. We don't need Buddhist rituals and they certainly

194
00:23:20,400 --> 00:23:27,680
don't save us from our lack of meditation practice. But mindfulness is much more important of course

195
00:23:29,280 --> 00:23:33,920
than rituals and actually in some ways more important than what we call practice. A person

196
00:23:33,920 --> 00:23:38,880
can do hours and hours of meditation practice and never get anywhere if they're not actually being

197
00:23:38,880 --> 00:23:45,920
mindful. My teacher said this and he said in opposition there's a story of one monk who

198
00:23:45,920 --> 00:23:52,080
became enlightened with three steps I think it was. Walking down the street if you say walking

199
00:23:52,080 --> 00:23:55,440
walking you can become enlightened just there if conditions are right.

200
00:23:58,880 --> 00:24:08,960
So I don't, to some extent that isn't entirely satisfying answer I don't think but

201
00:24:08,960 --> 00:24:17,040
I think for the reasons and based on the explanation we have to understand clearly this

202
00:24:17,760 --> 00:24:29,600
concept of worldly life and enlightenment and all the points in between and how we can connect

203
00:24:29,600 --> 00:24:38,080
the thoughts from one to the other. So ultimately what I do is teach, try to teach meditation

204
00:24:38,080 --> 00:24:45,200
practice to those who are interested and I think at the very least helping people understand

205
00:24:45,200 --> 00:24:49,840
how to be mindful which of course is applicable anywhere. Practice, practiceable,

206
00:24:51,680 --> 00:24:56,560
practiceable anywhere. Practiceable even the word.

207
00:24:56,560 --> 00:25:10,880
Anyway, that's the answer to that question. Thank you for asking. Thank you for listening.

