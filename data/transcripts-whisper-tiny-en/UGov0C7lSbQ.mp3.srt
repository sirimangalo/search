1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamupada.

2
00:00:05,000 --> 00:00:13,000
Today we continue on with verses 133 and 134, which read as follows.

3
00:00:13,000 --> 00:00:30,000
Today we will continue on with the Dhamupada and the Dhamupada and the Dhamupada.

4
00:00:30,000 --> 00:00:45,000
Today we will continue on with the Dhamupada and the Dhamupada and the Dhamupada.

5
00:00:45,000 --> 00:00:58,000
Which translate roughly as do not speak harshly to anyone.

6
00:00:58,000 --> 00:01:07,000
Having been spoken to, they may speak back to you.

7
00:01:07,000 --> 00:01:10,000
They may reply in kind.

8
00:01:10,000 --> 00:01:17,000
Dukha, he is sadharma, Dhamupada, angry words are painful.

9
00:01:17,000 --> 00:01:21,000
But in Dhamdha we say you tell them.

10
00:01:21,000 --> 00:01:29,000
And you may be struck, may come to blows,

11
00:01:29,000 --> 00:01:34,000
maybe struck us a result, or maybe hit and return.

12
00:01:34,000 --> 00:01:39,000
May lead to violence perhaps.

13
00:01:39,000 --> 00:01:47,000
Satyane is yet done, and if you can keep yourself still,

14
00:01:47,000 --> 00:01:58,000
consul bhatoyata, like a broken bell, a broken gong,

15
00:01:58,000 --> 00:02:08,000
asapatosi nimana, this is the way you will attain nimana.

16
00:02:08,000 --> 00:02:15,000
And there will be no anger found in you.

17
00:02:15,000 --> 00:02:19,000
So this has to do with harsh speech.

18
00:02:19,000 --> 00:02:28,000
And the origin story is relatively long compared to some of the stories recently and fairly short.

19
00:02:28,000 --> 00:02:31,000
I don't have to go into all the detail.

20
00:02:31,000 --> 00:02:39,000
But briefly, to sum up, there was a monk called Konnadhan.

21
00:02:39,000 --> 00:02:52,000
And everywhere he went, he was followed by a vision of a woman.

22
00:02:52,000 --> 00:02:56,000
So quite curious, whenever he would go for arms,

23
00:02:56,000 --> 00:02:59,000
people would see this woman following behind him.

24
00:02:59,000 --> 00:03:01,000
And they would say, here's a portion for you,

25
00:03:01,000 --> 00:03:04,000
and here's a portion for your lady friend.

26
00:03:04,000 --> 00:03:06,000
And he never saw this woman.

27
00:03:06,000 --> 00:03:09,000
He would turn around and see no woman.

28
00:03:09,000 --> 00:03:11,000
But everybody else saw this woman.

29
00:03:11,000 --> 00:03:18,000
And so word kind and got around.

30
00:03:18,000 --> 00:03:20,000
And the monk's got upset.

31
00:03:20,000 --> 00:03:23,000
But it tells a story of how this happened.

32
00:03:23,000 --> 00:03:26,000
So in ancient, and the story is actually somewhat interesting.

33
00:03:26,000 --> 00:03:32,000
And it's all quite fanciful, I mean, for a modern skeptical audience,

34
00:03:32,000 --> 00:03:38,000
I think there's a lot of distrust of these sort of magical type tales.

35
00:03:38,000 --> 00:03:41,000
How could there be this woman?

36
00:03:41,000 --> 00:03:49,000
Well, the story goes that in the time of Buddha digai, digai,

37
00:03:49,000 --> 00:03:51,000
one of the past Buddhas.

38
00:03:51,000 --> 00:03:58,000
So in another age, in another universe, perhaps.

39
00:03:58,000 --> 00:04:06,000
Why does it say digai?

40
00:04:06,000 --> 00:04:09,000
Or a cusp, I don't know, a cusp of our digai.

41
00:04:09,000 --> 00:04:13,000
Anyway, there were these two monks who were so close.

42
00:04:13,000 --> 00:04:14,000
It was as though they were brothers.

43
00:04:14,000 --> 00:04:16,000
They would do everything together.

44
00:04:16,000 --> 00:04:20,000
They were the best of friends and the closest of companions

45
00:04:20,000 --> 00:04:25,000
and so they would always come out and come out together

46
00:04:25,000 --> 00:04:29,000
and go for their meals together and go to meetings together.

47
00:04:29,000 --> 00:04:32,000
Meditate together, that kind of thing.

48
00:04:32,000 --> 00:04:36,000
And it happens that there was an angel, a goddess.

49
00:04:36,000 --> 00:04:42,000
A female angel, somehow, who saw them and thought to herself,

50
00:04:42,000 --> 00:04:48,000
I wonder if there's a way that I can split these two up.

51
00:04:48,000 --> 00:04:55,000
Listen carefully, this gives some insight into the true nature of the divine.

52
00:04:55,000 --> 00:05:05,000
We always, many other religions worship or pray to or expect protection from angelic beings.

53
00:05:05,000 --> 00:05:10,000
Well, it's not always there in inclination.

54
00:05:10,000 --> 00:05:13,000
So this just on a whim, this angel thought,

55
00:05:13,000 --> 00:05:16,000
I wonder if I can split these two up.

56
00:05:16,000 --> 00:05:26,000
And so one day when they were walking along the highway or the forest,

57
00:05:26,000 --> 00:05:32,000
one of the monks said, hold on, I've got to go use,

58
00:05:32,000 --> 00:05:35,000
what do you say, attend the needs of nature?

59
00:05:35,000 --> 00:05:38,000
I think it says in the English,

60
00:05:38,000 --> 00:05:44,000
you had to pass water, you had to urinate.

61
00:05:44,000 --> 00:05:47,000
So he went off into the bushes.

62
00:05:47,000 --> 00:05:52,000
And as he was coming out, this angel appeared behind him,

63
00:05:52,000 --> 00:06:00,000
as a beautiful woman, half naked and adjusting her robe and fixing her hair.

64
00:06:00,000 --> 00:06:05,000
As if coming out of the bush, the elder had just been in.

65
00:06:05,000 --> 00:06:13,000
And the other monks saw this and immediately suspected the worst in the first monk,

66
00:06:13,000 --> 00:06:22,000
accused him of breaking the discipline,

67
00:06:22,000 --> 00:06:27,000
accused him of having intimate relations with the woman.

68
00:06:27,000 --> 00:06:29,000
And the mother monk denied it, of course.

69
00:06:29,000 --> 00:06:32,000
And they ended up completely splitting.

70
00:06:32,000 --> 00:06:35,000
And this monk went before the other monks,

71
00:06:35,000 --> 00:06:39,000
and accused him and all the monks.

72
00:06:39,000 --> 00:06:42,000
He interrogated this other monk who denied it all.

73
00:06:42,000 --> 00:06:45,000
So they couldn't come to a decision over it,

74
00:06:45,000 --> 00:06:49,000
but completely destroyed their friendship.

75
00:06:49,000 --> 00:06:57,000
Now this angel realized and began to feel the effects of how evil it was,

76
00:06:57,000 --> 00:06:59,000
the thing that she had done.

77
00:06:59,000 --> 00:07:00,000
She wasn't malicious.

78
00:07:00,000 --> 00:07:09,000
She was just somewhat deluded and perhaps impetuous or careless,

79
00:07:09,000 --> 00:07:16,000
intoxicated perhaps in her powers and her great own greatness.

80
00:07:16,000 --> 00:07:18,000
But she felt terribly guilty.

81
00:07:18,000 --> 00:07:22,000
And so she went down and told the monk,

82
00:07:22,000 --> 00:07:26,000
the monk who had accused the second monk who had accused the first monk,

83
00:07:26,000 --> 00:07:28,000
told them what she had done.

84
00:07:28,000 --> 00:07:36,000
And this monk believed her, but they say it didn't ever fix their friendship.

85
00:07:36,000 --> 00:07:39,000
They ended up never becoming friends,

86
00:07:39,000 --> 00:07:41,000
fast friends again, because they didn't.

87
00:07:41,000 --> 00:07:42,000
They couldn't.

88
00:07:42,000 --> 00:07:50,000
It was like the first one felt like yet the other one had betrayed him

89
00:07:50,000 --> 00:07:56,000
by believing such a thing could be possible.

90
00:07:56,000 --> 00:07:59,000
So those two monks were reborn according to their garment.

91
00:07:59,000 --> 00:08:03,000
Whether the goddess was born in hell

92
00:08:03,000 --> 00:08:08,000
and suffered there for a period of an interval between two Buddhas

93
00:08:08,000 --> 00:08:10,000
was born as a man.

94
00:08:10,000 --> 00:08:14,000
And eventually became our protagonist,

95
00:08:14,000 --> 00:08:19,000
our antagonist, the name of this story,

96
00:08:19,000 --> 00:08:20,000
the name of this story,

97
00:08:20,000 --> 00:08:25,000
Kṛṣṇṣṇḍhāna.

98
00:08:25,000 --> 00:08:27,000
I became a man.

99
00:08:27,000 --> 00:08:32,000
It was born a boy and grew up and became a monk.

100
00:08:32,000 --> 00:08:35,000
But everywhere he went, this woman followed him.

101
00:08:35,000 --> 00:08:40,000
And so the monks were really upset about this

102
00:08:40,000 --> 00:08:43,000
and thought, we kind of do something

103
00:08:43,000 --> 00:08:45,000
because everyone's starting to talk

104
00:08:45,000 --> 00:08:48,000
and they're going to think that we,

105
00:08:48,000 --> 00:08:50,000
that women, women follow us around.

106
00:08:50,000 --> 00:08:52,000
This monk is going with a woman.

107
00:08:52,000 --> 00:08:54,000
So they went to an Atapindika,

108
00:08:54,000 --> 00:08:58,000
who was the owner of the donor of the monastery.

109
00:08:58,000 --> 00:09:01,000
And they said, look, you have to kick this monk out.

110
00:09:01,000 --> 00:09:03,000
They're not sure why they would do this

111
00:09:03,000 --> 00:09:05,000
because the Buddha was there.

112
00:09:05,000 --> 00:09:09,000
But this, I've seen this happen before.

113
00:09:09,000 --> 00:09:13,000
The monks go to lay people trying to solve the monastic problems.

114
00:09:13,000 --> 00:09:16,000
But An Atapindika was having none of it.

115
00:09:16,000 --> 00:09:20,000
He said, well, isn't the Buddha in Savatī?

116
00:09:20,000 --> 00:09:22,000
He said, well, the Buddha will know what to do.

117
00:09:22,000 --> 00:09:23,000
He did nothing.

118
00:09:23,000 --> 00:09:27,000
So they went to Visaka, who was the other chief flame disciple.

119
00:09:27,000 --> 00:09:28,000
She also did nothing.

120
00:09:28,000 --> 00:09:32,000
So finally, they went to the king for some reason.

121
00:09:32,000 --> 00:09:37,000
And they told the king about this, that there was this monk

122
00:09:37,000 --> 00:09:41,000
who had a woman following him around everywhere.

123
00:09:41,000 --> 00:09:44,000
He went, and the king should do something about it.

124
00:09:44,000 --> 00:09:48,000
And the king, of course, being just an ordinary worldling

125
00:09:48,000 --> 00:09:52,000
was taken by their words and went to the monastery

126
00:09:52,000 --> 00:09:55,000
and brought some men and surrounded this monk's hut

127
00:09:55,000 --> 00:09:58,000
and ordered him to come out.

128
00:09:58,000 --> 00:10:00,000
And the monk looked out and saw the king.

129
00:10:00,000 --> 00:10:05,000
And so he went, and he came out.

130
00:10:05,000 --> 00:10:07,000
So the other came out.

131
00:10:07,000 --> 00:10:09,000
And the king saw them.

132
00:10:09,000 --> 00:10:14,000
Immediately saw that there was a woman behind this monk.

133
00:10:14,000 --> 00:10:18,000
And so the others, when he saw the king,

134
00:10:18,000 --> 00:10:20,000
he went back in and sat on a seat.

135
00:10:20,000 --> 00:10:23,000
And when the king went in, didn't see the woman.

136
00:10:23,000 --> 00:10:25,000
And so he looked everywhere.

137
00:10:25,000 --> 00:10:28,000
He looked under everything in all corners of the room,

138
00:10:28,000 --> 00:10:30,000
up in the rafters everywhere.

139
00:10:30,000 --> 00:10:31,000
I couldn't find this woman.

140
00:10:31,000 --> 00:10:32,000
He said, where is she?

141
00:10:32,000 --> 00:10:33,000
Where is she?

142
00:10:33,000 --> 00:10:35,000
The woman.

143
00:10:35,000 --> 00:10:37,000
And this goes on.

144
00:10:37,000 --> 00:10:40,000
And eventually, he figures out that it's just

145
00:10:40,000 --> 00:10:44,000
some kind of strange phantom phenomenon.

146
00:10:44,000 --> 00:10:49,000
And so he says, look, because he takes the monk out.

147
00:10:49,000 --> 00:10:51,000
He brings the monk back in, doesn't see the woman.

148
00:10:51,000 --> 00:10:53,000
He says, it's not real.

149
00:10:53,000 --> 00:10:55,000
And so he tells the monk, he says, look,

150
00:10:55,000 --> 00:10:57,000
you're always going to have this trouble if this keeps

151
00:10:57,000 --> 00:10:58,000
happening.

152
00:10:58,000 --> 00:11:00,000
So you come to my home for arms.

153
00:11:00,000 --> 00:11:02,000
You come to the royal palace for arms.

154
00:11:02,000 --> 00:11:04,000
And the other monks found out about this.

155
00:11:04,000 --> 00:11:05,000
And they were incensed.

156
00:11:05,000 --> 00:11:09,000
They said, you know, not only is this monk corrupt.

157
00:11:09,000 --> 00:11:11,000
But now the king is corrupt as well.

158
00:11:11,000 --> 00:11:18,000
And so they were bad-nosing the king and bad-nosing this monk.

159
00:11:18,000 --> 00:11:21,000
And they told this, they came to this monk.

160
00:11:21,000 --> 00:11:23,000
And they said, well, so now you're the king's bastard,

161
00:11:23,000 --> 00:11:25,000
they said.

162
00:11:25,000 --> 00:11:31,000
And the monk, you know, fairly reasonably, although

163
00:11:31,000 --> 00:11:36,000
wrongly, became incensed at these monks.

164
00:11:36,000 --> 00:11:39,000
And shot back at them.

165
00:11:39,000 --> 00:11:41,000
You heard the ones who corrupt.

166
00:11:41,000 --> 00:11:43,000
You're the bastards.

167
00:11:43,000 --> 00:11:44,000
You can sort with women.

168
00:11:44,000 --> 00:11:45,000
You're the ones.

169
00:11:45,000 --> 00:11:47,000
If they said all these things that weren't true.

170
00:11:47,000 --> 00:11:49,000
And these monks were shocked.

171
00:11:49,000 --> 00:11:51,000
And they went to see the Buddha.

172
00:11:51,000 --> 00:11:53,000
And they told the Buddha this.

173
00:11:53,000 --> 00:11:56,000
And the Buddha called this monk to him and asked him if it

174
00:11:56,000 --> 00:11:57,000
was true.

175
00:11:57,000 --> 00:12:00,000
And yes, it's strange.

176
00:12:00,000 --> 00:12:01,000
Why did you do that?

177
00:12:01,000 --> 00:12:03,000
Well, they said these things to me.

178
00:12:03,000 --> 00:12:05,000
Well, the Buddha said what they were saying these things

179
00:12:05,000 --> 00:12:06,000
because of what they'd seen.

180
00:12:06,000 --> 00:12:08,000
Now, did you see them concerning with women?

181
00:12:08,000 --> 00:12:10,000
Did you see them?

182
00:12:10,000 --> 00:12:15,000
Do you have any reason to say these things?

183
00:12:15,000 --> 00:12:17,000
They said, no, actually.

184
00:12:17,000 --> 00:12:25,000
Actually, I just made that all up.

185
00:12:25,000 --> 00:12:33,000
And the Buddha said, for you, this bad thing has happened to you

186
00:12:33,000 --> 00:12:37,000
because of things you've done in your past life.

187
00:12:37,000 --> 00:12:40,000
There's something that you have to bear with.

188
00:12:40,000 --> 00:12:43,000
And so then he told the spirituals.

189
00:12:43,000 --> 00:12:45,000
They don't speak harshly.

190
00:12:45,000 --> 00:12:49,000
So the story is interesting.

191
00:12:49,000 --> 00:12:53,000
And it provides a little bit of interesting context.

192
00:12:53,000 --> 00:13:02,000
And the general philosophical topic here is about misconception.

193
00:13:02,000 --> 00:13:07,000
And what sort of misconception is important?

194
00:13:07,000 --> 00:13:10,000
And what sort is not?

195
00:13:10,000 --> 00:13:15,000
You see, these monks who saw this woman behind the monk

196
00:13:15,000 --> 00:13:19,000
and they had no reason to doubt it.

197
00:13:19,000 --> 00:13:24,000
Their claim that this man was being followed by a monk was wrong

198
00:13:24,000 --> 00:13:31,000
but reasonable was in an ultimate sense.

199
00:13:31,000 --> 00:13:35,000
On an ultimate level, it was justified.

200
00:13:35,000 --> 00:13:42,000
It is indeed the monks misunderstanding, the monks wrong.

201
00:13:42,000 --> 00:13:44,000
It doesn't even seem like an misunderstanding.

202
00:13:44,000 --> 00:13:46,000
It seems like he premeditated.

203
00:13:46,000 --> 00:13:50,000
His misunderstanding wasn't in terms of these other monks being

204
00:13:50,000 --> 00:13:54,000
bastards or being followed around by women

205
00:13:54,000 --> 00:13:56,000
associating with women.

206
00:13:56,000 --> 00:13:58,000
He knew that was not true.

207
00:13:58,000 --> 00:14:00,000
But it still it's a misunderstanding.

208
00:14:00,000 --> 00:14:02,000
But it's a deeper misunderstanding.

209
00:14:02,000 --> 00:14:05,000
It's a misunderstanding that somehow saying these words

210
00:14:05,000 --> 00:14:09,000
getting angry even is somehow the right answer.

211
00:14:09,000 --> 00:14:12,000
But there's some benefit to these things.

212
00:14:12,000 --> 00:14:17,000
This is fairly important because Buddhism still considers

213
00:14:17,000 --> 00:14:20,000
all evil to come from misunderstanding.

214
00:14:20,000 --> 00:14:22,000
You don't just get angry.

215
00:14:22,000 --> 00:14:25,000
You get angry because you think somehow that it's good to get angry.

216
00:14:25,000 --> 00:14:27,000
You don't understand anger.

217
00:14:27,000 --> 00:14:33,000
It's crucial because our ordinary way of dealing with anger

218
00:14:33,000 --> 00:14:36,000
is when we feel that it's wrong is to suppress it.

219
00:14:36,000 --> 00:14:37,000
Same with greed.

220
00:14:37,000 --> 00:14:40,000
When you want something, the only way to deal with that is to

221
00:14:40,000 --> 00:14:43,000
suppress your urge.

222
00:14:43,000 --> 00:14:46,000
So we believe the anger is wrong.

223
00:14:46,000 --> 00:14:48,000
The root is the anger.

224
00:14:48,000 --> 00:14:49,000
It's not.

225
00:14:49,000 --> 00:14:51,000
The anger isn't the root problem.

226
00:14:51,000 --> 00:14:54,000
The root problem is you don't understand greed.

227
00:14:54,000 --> 00:14:55,000
You don't understand anger.

228
00:14:55,000 --> 00:15:00,000
You don't understand the things that you're getting greedy and angry about.

229
00:15:00,000 --> 00:15:03,000
When you want something, it's because you think it is stable,

230
00:15:03,000 --> 00:15:05,000
satisfying control.

231
00:15:05,000 --> 00:15:10,000
When you dislike something, it's because you feel like you can make it

232
00:15:10,000 --> 00:15:11,000
stable.

233
00:15:11,000 --> 00:15:17,000
You can change it to be stable, satisfying and controllable.

234
00:15:17,000 --> 00:15:24,000
And only when you see clearly, when you come to see the truth about this,

235
00:15:24,000 --> 00:15:32,000
the truth about this, because we don't really know about the other set of

236
00:15:32,000 --> 00:15:35,000
monks, whether they had a reaction to these things.

237
00:15:35,000 --> 00:15:40,000
So they had this experience where they saw this woman or they even heard about

238
00:15:40,000 --> 00:15:42,000
this woman following the monk around.

239
00:15:42,000 --> 00:15:45,000
It says they actually saw it.

240
00:15:45,000 --> 00:15:48,000
But we don't know what their reaction was.

241
00:15:48,000 --> 00:15:55,000
Their reaction was neutral and objective over that their reaction was angry.

242
00:15:55,000 --> 00:15:57,000
But it's reasonable.

243
00:15:57,000 --> 00:16:02,000
Their misunderstanding was only in an abstract sense in terms of concepts.

244
00:16:02,000 --> 00:16:07,000
So the concepts, they got the concepts from.

245
00:16:07,000 --> 00:16:12,000
They thought this was a real woman when in fact it was just a phantom moment.

246
00:16:12,000 --> 00:16:16,000
But in an ultimate sense, they were saying it was factual.

247
00:16:16,000 --> 00:16:22,000
This monk appears to be followed around by woman.

248
00:16:22,000 --> 00:16:30,000
And so how this relates in general to all of us, because I don't think these circumstances are all that common.

249
00:16:30,000 --> 00:16:34,000
But obviously, the general circumstances of finding yourself being accused

250
00:16:34,000 --> 00:16:41,000
wrongfully, of finding yourself being abused, finding yourself being manipulated

251
00:16:41,000 --> 00:16:49,000
or taking advantage of finding yourself being unfairly treated.

252
00:16:49,000 --> 00:16:52,000
Now sometimes it's with reason.

253
00:16:52,000 --> 00:16:55,000
Sometimes justified.

254
00:16:55,000 --> 00:16:57,000
Sometimes you really are being unfairly treated.

255
00:16:57,000 --> 00:16:59,000
Sometimes there's malicious intent.

256
00:16:59,000 --> 00:17:03,000
But sometimes there's not.

257
00:17:03,000 --> 00:17:10,000
And whether there is or there isn't, actually is not the important.

258
00:17:10,000 --> 00:17:12,000
And the most crucial aspect.

259
00:17:12,000 --> 00:17:17,000
If you know you're being wrongfully treated, this monk should have, you know,

260
00:17:17,000 --> 00:17:20,000
he was fully in his right to say I'm not being followed by a woman.

261
00:17:20,000 --> 00:17:22,000
And that's when he didn't begin it.

262
00:17:22,000 --> 00:17:23,000
Yes, that's not true.

263
00:17:23,000 --> 00:17:24,000
There is no woman.

264
00:17:24,000 --> 00:17:27,000
I've never consorted with a woman.

265
00:17:27,000 --> 00:17:29,000
It's just not true.

266
00:17:29,000 --> 00:17:43,000
But I mean, the important point is you can't control the results of your situation.

267
00:17:43,000 --> 00:17:46,000
You can't always solve everything.

268
00:17:46,000 --> 00:17:51,000
And everyone has experiences of misunderstanding.

269
00:17:51,000 --> 00:17:53,000
Sometimes you're reconcilable.

270
00:17:53,000 --> 00:18:00,000
Sometimes to the point that people actually think you're a bad person, as in this case,

271
00:18:00,000 --> 00:18:03,000
when in fact you've done nothing wrong.

272
00:18:03,000 --> 00:18:06,000
Just because you didn't have a chance to clear your name,

273
00:18:06,000 --> 00:18:16,000
it does speak to the importance of trying to explain yourself.

274
00:18:16,000 --> 00:18:20,000
But in the end, you're never going to fix everything.

275
00:18:20,000 --> 00:18:23,000
You can't fix life.

276
00:18:23,000 --> 00:18:29,000
And so you're always going to be, it's just another situation where there's going to be the potential for suffering.

277
00:18:29,000 --> 00:18:42,000
Whether it's going to be an unpleasant experience, an experience that has the potential for the arising of anger, of dislike, of aversion.

278
00:18:42,000 --> 00:18:50,000
Of course, became terribly averse, simply based on his, not on his misunderstanding in the situation,

279
00:18:50,000 --> 00:18:59,000
but on his misunderstanding of the right, of the reality of the situation, therefore the right responds.

280
00:18:59,000 --> 00:19:02,000
Or there are the nature of reality.

281
00:19:02,000 --> 00:19:06,000
He didn't understand that getting anger was a bad thing.

282
00:19:06,000 --> 00:19:12,000
He didn't understand what that others might, whether it be results, there will be consequences.

283
00:19:12,000 --> 00:19:13,000
Dukkahi, as I say, tukkahi,

284
00:19:13,000 --> 00:19:23,000
Sarambhakata, there indeed.

285
00:19:23,000 --> 00:19:31,000
Angry words are unpleasant, are stressful, are suffering. Dukkahi.

286
00:19:31,000 --> 00:19:35,000
It might lead to blows even.

287
00:19:35,000 --> 00:19:41,000
The next verse is actually sort of a generally good Buddhist.

288
00:19:41,000 --> 00:19:45,000
What the word is the saying, or aphorism, is that the word?

289
00:19:45,000 --> 00:19:52,000
If you keep yourself silent as a broken gong, you have already reached, already reached.

290
00:19:52,000 --> 00:19:54,000
Don't know about that.

291
00:19:54,000 --> 00:19:59,000
You reach, I don't think it's already opatosis past.

292
00:19:59,000 --> 00:20:06,000
Let's see. Let's see. Let's see. Let's see.

293
00:20:06,000 --> 00:20:07,000
Maybe.

294
00:20:07,000 --> 00:20:10,000
The Arahan keeps himself silent.

295
00:20:10,000 --> 00:20:17,000
Remember the story of this Arahan, who was accused of stealing a ruby, I think we had this story already, right?

296
00:20:17,000 --> 00:20:24,000
And it turned out this bird had eaten it, but he wouldn't turn in the bird because he knew if he did the bird would be killed.

297
00:20:24,000 --> 00:20:40,000
And he didn't want to be responsible for that bad karma, so he kept completely silent even as he was being tortured as the thief, as the suspect for the thief.

298
00:20:40,000 --> 00:20:43,000
Be silent as a broken gong.

299
00:20:43,000 --> 00:20:45,000
This speaks to patience.

300
00:20:45,000 --> 00:20:47,000
Be patient with some sorrow.

301
00:20:47,000 --> 00:20:52,000
There's another story of this novice who had his eye poked out.

302
00:20:52,000 --> 00:20:58,000
And so he only did his cover of his eye, and then when he went to go off from water to his teacher, he would poke his eye out.

303
00:20:58,000 --> 00:21:00,000
He offered with one hand.

304
00:21:00,000 --> 00:21:03,000
When the teacher was, why are you offering with one hand?

305
00:21:03,000 --> 00:21:05,000
Well, my other hand is occupied.

306
00:21:05,000 --> 00:21:08,000
But he was an Arahan as well.

307
00:21:08,000 --> 00:21:10,000
And so he also wasn't upset.

308
00:21:10,000 --> 00:21:19,000
And the teacher was mortified, and he begged forgiveness, and the novice said,

309
00:21:19,000 --> 00:21:24,000
it's not your fault then, or it's the fault of some sorrow.

310
00:21:24,000 --> 00:21:27,000
And again, this is the case with this story.

311
00:21:27,000 --> 00:21:31,000
Our situations are the best answers.

312
00:21:31,000 --> 00:21:33,000
They're the fault of some sorrow.

313
00:21:33,000 --> 00:21:35,000
It's the way things go.

314
00:21:35,000 --> 00:21:37,000
It's maybe not the way things had to go.

315
00:21:37,000 --> 00:21:39,000
They could have gone differently.

316
00:21:39,000 --> 00:21:43,000
But the point is that it's how some sorrow works.

317
00:21:43,000 --> 00:21:46,000
Sometimes misunderstandings arise.

318
00:21:46,000 --> 00:21:49,000
It's not always clear.

319
00:21:49,000 --> 00:21:51,000
It's not always pleasant.

320
00:21:51,000 --> 00:21:56,000
Unpleasantness can arise from many different things from natural disasters,

321
00:21:56,000 --> 00:21:58,000
from human violence.

322
00:21:58,000 --> 00:22:02,000
And it can arise from simple misunderstandings.

323
00:22:02,000 --> 00:22:04,000
You might call it a sort of structural violence,

324
00:22:04,000 --> 00:22:11,000
where the structure, rather than the people, is a cause for suffering.

325
00:22:11,000 --> 00:22:14,000
There's no one to blame for it, not directly.

326
00:22:14,000 --> 00:22:25,000
But suffering is potentially inbuilt into the structure.

327
00:22:25,000 --> 00:22:28,000
So in terms of our meditation, we try to just be mindful.

328
00:22:28,000 --> 00:22:33,000
Rather than trying to fix things, because fixes are not consistent,

329
00:22:33,000 --> 00:22:35,000
they're not consistently attainable.

330
00:22:35,000 --> 00:22:36,000
You can't clump.

331
00:22:36,000 --> 00:22:41,000
You can't always fix your problems.

332
00:22:41,000 --> 00:22:47,000
And so trying to make that a solution is the wrong path,

333
00:22:47,000 --> 00:22:50,000
the wrong way to be.

334
00:22:50,000 --> 00:22:53,000
Instead of trying to find solutions to our problems,

335
00:22:53,000 --> 00:22:56,000
we learn to unravel them and to just be with them,

336
00:22:56,000 --> 00:23:00,000
such as to stop seeing them as problems.

337
00:23:00,000 --> 00:23:03,000
To see through the problems to the ultimate reality

338
00:23:03,000 --> 00:23:08,000
and to understand the experiences and the

339
00:23:08,000 --> 00:23:13,000
objects of experience objectively,

340
00:23:13,000 --> 00:23:16,000
without judgment, without partiality,

341
00:23:16,000 --> 00:23:18,000
without expectation.

342
00:23:18,000 --> 00:23:21,000
This is what we do throughout the meditation.

343
00:23:21,000 --> 00:23:23,000
We become like a broken gong,

344
00:23:23,000 --> 00:23:26,000
but it just sits there, it doesn't ever ring.

345
00:23:26,000 --> 00:23:29,000
It doesn't ever react, you know?

346
00:23:29,000 --> 00:23:33,000
You're a rung of broken gong, it doesn't reverberate.

347
00:23:33,000 --> 00:23:36,000
Just like that, there's no anger, there's no reaction.

348
00:23:36,000 --> 00:23:39,000
When people are nice to you,

349
00:23:39,000 --> 00:23:43,000
you don't become elated when they speak well of you,

350
00:23:43,000 --> 00:23:48,000
when they praise you, and when they accuse you,

351
00:23:48,000 --> 00:23:52,000
when they vilify you.

352
00:23:52,000 --> 00:23:54,000
This is like a gong, it doesn't,

353
00:23:54,000 --> 00:23:56,000
no matter how you hit it, it doesn't reverberate.

354
00:23:56,000 --> 00:23:59,000
It was broken.

355
00:23:59,000 --> 00:24:02,000
So, that is our number for tonight.

356
00:24:02,000 --> 00:24:04,000
Thank you all for tuning in.

357
00:24:04,000 --> 00:24:06,000
We should be all the best.

