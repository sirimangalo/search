1
00:00:00,000 --> 00:00:03,000
Hi, welcome back to Ask a Monk.

2
00:00:03,000 --> 00:00:08,000
Today's question comes from a good friend of mine, Jerry Epson.

3
00:00:08,000 --> 00:00:12,000
And he asks, what is the nature of heaven in Buddhism?

4
00:00:12,000 --> 00:00:15,000
Or what is the meaning of the word heaven?

5
00:00:15,000 --> 00:00:21,000
And what meaning does it have for Buddhists?

6
00:00:21,000 --> 00:00:28,000
First of all, the idea of heaven as a place can be somewhat misleading.

7
00:00:28,000 --> 00:00:34,000
It's important that we understand, what do we mean by place or by space?

8
00:00:34,000 --> 00:00:39,000
Space in Buddhism is an extension of one's experience.

9
00:00:39,000 --> 00:00:43,000
So all of my experience takes up a certain amount of space.

10
00:00:43,000 --> 00:00:47,000
My being, my universe, has a certain space.

11
00:00:47,000 --> 00:00:51,000
And everyone around me also takes up a piece of space.

12
00:00:51,000 --> 00:00:55,000
And so our interactions create the world around us.

13
00:00:55,000 --> 00:01:03,000
We create the idea of three-dimensional space based on our experience.

14
00:01:03,000 --> 00:01:15,000
So in that sense, heaven is simply another level of experience that is made up of the experiences of the beings in that realm.

15
00:01:15,000 --> 00:01:24,000
And the difference being simply that the experience of reality of those beings is much more subtle

16
00:01:24,000 --> 00:01:28,000
and more refined and more pure than the experience at this level.

17
00:01:28,000 --> 00:01:36,000
So the reason we meet with each other here is because our experiences is coarse.

18
00:01:36,000 --> 00:01:48,000
And so we meet on the same level for those beings who have more pure and refined minds that arise above and they meet in another level.

19
00:01:48,000 --> 00:01:51,000
That's in terms of space.

20
00:01:51,000 --> 00:01:59,000
So you can think of hell as being below and heaven as being above as is generally the case.

21
00:01:59,000 --> 00:02:07,000
But more important to understand in terms of what we mean by heaven and hell and the various realms of existence,

22
00:02:07,000 --> 00:02:14,000
is simply that in terms of purity of mind that for every individual we're on a path,

23
00:02:14,000 --> 00:02:26,000
we purify our minds or we sell their minds and the more defiled our minds become, the lower we sink and the more course our reality becomes.

24
00:02:26,000 --> 00:02:35,000
As we complicate our minds and make our minds more course and more twisted or however you want to look at it,

25
00:02:35,000 --> 00:02:43,000
we become equally more dense and more physical and more subject to suffering.

26
00:02:43,000 --> 00:02:52,000
You could say it's similar to a person who does bad deeds and is as a result placed in jail and has to spend some time in jail.

27
00:02:52,000 --> 00:02:58,000
As a human being our past deeds have led us to this level of course.

28
00:02:58,000 --> 00:03:09,000
If we had done, if we had been more course in our past lives, we would have sunken lower to become animals or even to go to a place very similar to hell.

29
00:03:09,000 --> 00:03:17,000
If it was extreme, if we were murderers or thieves or very immoral people,

30
00:03:17,000 --> 00:03:24,000
would it does to your mind the very real effect that it has on your mind causes you to sink lower.

31
00:03:24,000 --> 00:03:32,000
By the same token, when you purify your mind as you do good deeds and try to help other people and your mind becomes purer,

32
00:03:32,000 --> 00:03:42,000
you feel lighter, you feel more free in the mind and less attached to things, you rise up higher.

33
00:03:42,000 --> 00:03:51,000
Now the reason why it doesn't happen where human being does good deeds and they start floating off the ground is because of the weight of the karma.

34
00:03:51,000 --> 00:04:01,000
In this sense it's similar to being thrown in jail because people in jail they can reform themselves but they suddenly break out of the jail.

35
00:04:01,000 --> 00:04:06,000
They have to live out the term according to their sentence and the same is true for human beings.

36
00:04:06,000 --> 00:04:18,000
We have a very weighty circle of karma that we've performed in the past that is having these intense results that has placed us in this physical existence.

37
00:04:18,000 --> 00:04:30,000
That is manifesting itself as our human existence and until that has run its course, it's not very likely that we're going to just rise up to heaven or so on.

38
00:04:30,000 --> 00:04:42,000
That being said, this is a good way to explain why good people have a very great difficulty surviving in the world.

39
00:04:42,000 --> 00:04:52,000
People say, yeah, you have to fight dirty, you have to it's a doggy dog world if you're not trying to cheat everybody else, you won't get ahead and so on.

40
00:04:52,000 --> 00:05:03,000
That may be true but that's really just a sign that this is not a very pure state of existence and those people with pure states of existence are rising above it.

41
00:05:03,000 --> 00:05:21,000
The person who does good things is eventually ejected into a higher realm which is really the case with much more simple and pure realms if you're born in a level of purity and you die or you leave from that state.

42
00:05:21,000 --> 00:05:28,000
It's merely a shift down or a shift up, it's not a death, it doesn't have to be a set length of time.

43
00:05:28,000 --> 00:05:33,000
You can leave it any time, you can go up and down and beings in heaven are said to do this.

44
00:05:33,000 --> 00:05:45,000
They're said to go from one level to another quite to smoothly based on their deeds, based on their state of mind and based on the fulfillment of certain garments when they've lived.

45
00:05:45,000 --> 00:05:59,000
I would live there, the acts which brought them to the state of purity, they can sink down and if they do more good deeds eventually they'll be able to rise up when the old karma has ceased.

46
00:05:59,000 --> 00:06:09,000
It's really based on cause and effect, it's not something magical where there's someone judging you okay now it's your time to go up to heaven, now it's time to go down to hell.

47
00:06:09,000 --> 00:06:21,000
It's based on what you do good deeds or do bad deeds and when the good and bad deeds you've done before the effect of them is expired.

48
00:06:21,000 --> 00:06:40,000
In this sense it's really here and now heaven is just the change of this to something more pure, hell is the change of this to something more impure, more coarse.

49
00:06:40,000 --> 00:06:54,000
Our karma, the result of our karma that has led us to be born as a human being and has created this reality is up. When it gets to its end then our mind is free and goes according to the next karma.

50
00:06:54,000 --> 00:07:13,000
We grasp onto something either pure or impure, it's able to rise out just as a person who leaves who is led out of jail is free to go on and become a good better person or go back into bad deeds and have to come back and spend more time in jail.

51
00:07:13,000 --> 00:07:37,000
I hope that helps make sense, heaven can be thought of as a place but only in the sense of it being a conglomeration of equally pure or impure minds or beings, but it should be better thought of as a state of mind where the mind is pure and is freed from the impurity of the human

52
00:07:37,000 --> 00:07:43,000
or the effects of our impurity that has led us to be born and spend and have to spend time as a human being.

53
00:07:43,000 --> 00:07:50,000
Finally, that being said, it's important to understand that heaven isn't the be all end all.

54
00:07:50,000 --> 00:08:05,000
What you can say is that people who practice meditation or practice on the right path do generally go to heaven a lot easier because their minds are pure, but it's also possible to go to heaven with on a temporary purity.

55
00:08:05,000 --> 00:08:15,000
If you work really hard on a mundane level and develop very pure states of mind, you can do it by suppressing the bad states.

56
00:08:15,000 --> 00:08:31,000
You can practice certain types of meditation that give you very pure states of mind and help you to rise above the impure state part of your mind temporarily like a transcendental meditation and so on and can actually be born and haven't even for long periods of time.

57
00:08:31,000 --> 00:08:45,000
But once that wears thin and the results of all of that work wears out and obviously you wouldn't be doing that work in heaven, you'd just be enjoying your time in this blissful state of being.

58
00:08:45,000 --> 00:08:58,000
The bad stuff comes back, the bad stuff is there and when the results lose their power and the results are worn out, you'll come crashing back down.

59
00:08:58,000 --> 00:09:27,000
So heaven is not the goal, it's a sign that the mind is pure, but it's important to understand that the important thing is to purify the mind and sometimes that's easier done in a place like the human world because we're able to see the ups and the downs of life, a person who's born in heaven due to some temporary deed or some mundane goodness that they've performed and that has elevated their minds to that level.

60
00:09:27,000 --> 00:09:33,000
It can often become intoxicated by it and they forget to develop themselves further.

61
00:09:33,000 --> 00:09:47,000
If a person is born again and again as a human being or even just is in one life a human being during this life we're much better able to see when I do bad deeds, bad things come to me when I do good deeds, good things come to me.

62
00:09:47,000 --> 00:09:53,000
If you're born in heaven you can do bad deeds and because of the power of your past good deeds, you don't have to pay for them right away.

63
00:09:53,000 --> 00:09:57,000
So it can be dangerous.

64
00:09:57,000 --> 00:10:06,000
On the other hand one of the good things about a place like heaven is you can sit in meditation quite easily and without all the pain and suffering.

65
00:10:06,000 --> 00:10:13,000
They say if you're really dedicated to meditation being born in heaven can be a real great blessing.

66
00:10:13,000 --> 00:10:22,000
Another thing is because there are obviously many meditators who have gone up to heaven and so it's very easy to find a group of people to meditate with.

67
00:10:22,000 --> 00:10:29,000
And very easy to sit in meditation because your body doesn't complain as much.

68
00:10:29,000 --> 00:10:36,000
You have a very pure state of being and you can sit for months on end and contemplate the reality in front of you.

69
00:10:36,000 --> 00:10:46,000
So there's really no saying good or bad. It's good in a mundane sense but it's basically just another state of purity.

70
00:10:46,000 --> 00:10:59,000
And until you can really understand reality for what it is, it's really a meaningless judgment as to where you are on the scale.

71
00:10:59,000 --> 00:11:05,000
Until it becomes a permanent thing where your mind is truly pure and you have no more clinging.

72
00:11:05,000 --> 00:11:15,000
You're not just suppressing your bad thoughts but you're actually understanding them and you're actually letting go of them and you're actually giving them up and they no longer arise again.

73
00:11:15,000 --> 00:11:29,000
At that point then not only are you born say in heaven but eventually you're free from all of this arising of experience.

74
00:11:29,000 --> 00:11:37,000
You come back to the center and leave behind all of the outside world.

75
00:11:37,000 --> 00:11:47,000
So that doesn't sound too fruity. It's not one of the things I normally talk about but you asked and that's my understanding of heaven in terms of Buddhism.

76
00:11:47,000 --> 00:11:58,000
Most important thing being, it's just a state of reality and if you enter into that state of reality you should meditate on it and you should try to understand it and see it clearly.

77
00:11:58,000 --> 00:12:18,000
It's a sign that your mind is pure in some way or another and the sign that you're on the right path but it only serves you best to continue to develop greater and greater states of purity not to become content with being born in heaven or so on.

78
00:12:18,000 --> 00:12:28,000
So thanks for the question. Keep on coming.

