1
00:00:00,000 --> 00:00:13,360
No it's that. Good evening everyone broadcasting live October, November 9th, 2015, fifth,

2
00:00:13,360 --> 00:00:28,080
number fifth, that's correct. And I think we've got proper sound going on. You want to

3
00:00:28,080 --> 00:00:36,160
see hello Robin, so I can test it. I don't know. Why don't you come in through.

4
00:00:40,400 --> 00:00:41,920
Maybe you're not allowed enough.

5
00:00:46,320 --> 00:00:51,040
Oh wait, I see. Look back. This is where I have to come from.

6
00:00:51,040 --> 00:01:02,000
There. Okay, now say hello. Hello? Yes. That's okay. Now we're both, now we both should be coming

7
00:01:02,000 --> 00:01:12,880
through the audio broadcast. Okay. Questions? Yes. First of all, let's do the announcements first

8
00:01:12,880 --> 00:01:19,360
because tonight we don't have a demo pod of video. So what's going on here? We have two things.

9
00:01:19,360 --> 00:01:26,960
Well, actually only one thing, but I have two posters, but I don't think it's worth showing

10
00:01:26,960 --> 00:01:36,800
posters is it. You guys want to say our new posters? Yes. You can comment on the screen share.

11
00:01:36,800 --> 00:01:43,040
There.

12
00:01:51,360 --> 00:01:55,280
I want to get something and I'll hit them, hit people, make them wake up and say,

13
00:01:55,280 --> 00:02:00,400
hey, why aren't you all meditating? Yeah. Why do you go out and drinking every night?

14
00:02:00,400 --> 00:02:07,200
Whatever you say you do. That's very nice. Is that the first one?

15
00:02:07,200 --> 00:02:35,760
Did it change? No, not yet. This one isn't me. This is Carolyn, one of our

16
00:02:35,760 --> 00:02:39,600
exact members, but I did the poster for her. She did a poster, but then it,

17
00:02:41,920 --> 00:02:50,240
well, I redid it. Came out nice. Yeah. The McMaster Buddhism logo is a little bit like

18
00:02:50,240 --> 00:02:55,840
the Ceremon Go. It's not the McMaster Buddhism logo. That's the McMaster students union clubs.

19
00:02:55,840 --> 00:03:06,480
They forced us to put their logo on our posters. The clubs is very hard to read, but that's not

20
00:03:06,480 --> 00:03:15,760
important to me because their logo isn't all that important. But yeah, so we're having a campfire.

21
00:03:15,760 --> 00:03:20,000
This is our one big announcement. This was someone's ideas. I think it was Carolyn's idea

22
00:03:20,000 --> 00:03:26,880
to go and have a campfire in the evening and talk about Buddhism and maybe do some meditation together.

23
00:03:28,720 --> 00:03:34,080
I didn't nature. The monks aren't actually allowed to sit around campfires together.

24
00:03:34,080 --> 00:03:38,320
It's an interesting thing. I have to look up whether I can take part in the campfire,

25
00:03:38,960 --> 00:03:46,000
but it's a curious rule. They're warming up, presenting. It's a curious rule.

26
00:03:46,000 --> 00:03:51,520
And the speculation is that it's somehow sitting around a campfire somehow leads to

27
00:03:51,520 --> 00:03:55,920
negligence. It's just not an appropriate behavior for a Buddhist monk.

28
00:03:57,200 --> 00:04:04,720
But whatever. I think I'll be okay with attending because I'm the only monk and I'm going

29
00:04:04,720 --> 00:04:09,600
to talk to lay people not to talk to monks. And if there happens to be a campfire there?

30
00:04:09,600 --> 00:04:16,080
Yeah. I think the idea is they're going to have s'mores or something.

31
00:04:18,800 --> 00:04:31,760
It's sort of in the vein of lay Buddhism. The Adjantag group has Buddhist songs that they sing

32
00:04:31,760 --> 00:04:39,920
and Buddhist sing alongs and Buddhist plays that are kind of humorous about Buddhism. You know,

33
00:04:39,920 --> 00:04:45,040
you wonder where the line is going to be drawn, but sometimes you just

34
00:04:46,960 --> 00:04:55,200
accept the Gosulupaya. The whole skillful means to get people interested in.

35
00:04:55,200 --> 00:05:03,040
So it's the idea is to create a community of like-minded individuals. Again, this isn't my idea,

36
00:05:03,040 --> 00:05:10,720
but it's interesting. So that's part of what we'll be doing. That's next Friday.

37
00:05:14,480 --> 00:05:19,520
So that's a couple of things that have been going on here. What else?

38
00:05:19,520 --> 00:05:27,040
Tomorrow I'm going to London. For Saturday I'm going to London. Ontario to someone's house.

39
00:05:28,400 --> 00:05:35,680
Oh, and we've got 27 of 28 slots filled on the appointments page.

40
00:05:35,680 --> 00:05:40,080
That's amazing. It means I'm meeting 27 different people a week.

41
00:05:42,080 --> 00:05:46,480
Which is pretty good. That's great.

42
00:05:46,480 --> 00:05:59,280
So that's all. Any announcements from the volunteer group?

43
00:06:02,960 --> 00:06:07,680
If anyone has been following along on Facebook and hasn't really noticed much activity,

44
00:06:07,680 --> 00:06:12,240
it's because most of our activity now is on a website called slack.com.

45
00:06:12,240 --> 00:06:18,320
And it's kind of taken the place of those email blasts that we're going out and taken a place

46
00:06:18,320 --> 00:06:26,160
with a lot of the talk on Facebook. So if anyone is interested, just send me a message,

47
00:06:26,160 --> 00:06:31,760
I'll post the information into the meditator chat panel as well, because it's still a very

48
00:06:31,760 --> 00:06:35,520
active group. We just have kind of changed our communication means a little bit.

49
00:06:35,520 --> 00:06:41,120
Ready for questions, Bante?

50
00:06:43,520 --> 00:06:43,920
Okay.

51
00:06:43,920 --> 00:07:13,760
Okay. Then we will serve. What would be your translation of Lord Buddha's final words? Thank you.

52
00:07:14,080 --> 00:07:24,880
Somebody come to fulfillment or fulfill your goals or come to become fulfilled.

53
00:07:26,000 --> 00:07:33,840
Appamadina with or in regards to, you could say,

54
00:07:33,840 --> 00:07:46,000
Appamada. So come to fulfillment in regards to vigilance or mindfulness. So become sober,

55
00:07:46,000 --> 00:07:54,160
really sober up if you wanted to, sort of a grass, grass, and paraphrase.

56
00:07:54,160 --> 00:08:05,920
All this stuff, all this stuff is going to disappear sober up.

57
00:08:08,720 --> 00:08:18,560
They're all stuff disappears, all stuff, all stuff, breaks, all stuff breaks,

58
00:08:18,560 --> 00:08:24,640
stuff breaks sober up. There you go. It's about as grass as I can get.

59
00:08:27,840 --> 00:08:31,200
There's Buddhism in a nutshell. Stuff breaks sober up.

60
00:08:33,680 --> 00:08:35,440
There's a meme for you.

61
00:08:35,440 --> 00:08:39,520
And it's actually a fairly literal translation. Interestingly, stuff,

62
00:08:39,520 --> 00:08:44,480
Sankara is stuff. It depends what you mean by stuff, but it's one way of

63
00:08:44,480 --> 00:08:56,160
stuff. Stuff is a good translation of Sankara, breaks, why I mean it's a, you know, it's more

64
00:08:56,160 --> 00:09:01,840
like dissipates or ceases, but fades away, breaks is, I think, good.

65
00:09:06,240 --> 00:09:12,080
Sober, because Bhamada has to do with mud, mud is the root of being intoxicated. So

66
00:09:12,080 --> 00:09:18,560
Bhamada is a kind of mental intoxication, upper mod is sobering up, or non-intoxication,

67
00:09:18,560 --> 00:09:32,480
un-intoxicated. And sampadeta means it's in the sense of samp, and I have samps, but sampa is like

68
00:09:32,480 --> 00:09:41,200
stronger. Sam is, is fully sampad, but no, no, Bhad is not strong, sorry. Bhad is to become

69
00:09:42,560 --> 00:09:53,360
sampad, become full. So sampagity is to become full, or to fulfill, or to succeed.

70
00:09:55,120 --> 00:10:01,440
So it does have a sense of up, I think, but not exactly, but there you go.

71
00:10:01,440 --> 00:10:06,320
There's my translation. Stuff breaks sober up.

72
00:10:10,240 --> 00:10:16,160
Can make a poster for that too? Put it on Facebook. Stuff breaks sober up the Buddha.

73
00:10:16,800 --> 00:10:23,120
And then if anyone calls you out on it, then we'll get it sent to fake Buddha quotes and have him

74
00:10:23,760 --> 00:10:27,680
go at it, and then we can all laugh at him when he says it's fake.

75
00:10:27,680 --> 00:10:35,120
You know, he's a good friend. Don't do that. But it'd be funny to have that conversation.

76
00:10:40,080 --> 00:10:45,920
Hello, Bhante. I recently attained third path by following instructions from Mahasisada's

77
00:10:45,920 --> 00:10:52,080
practical insight meditation, but I still feel pleasant sensations when I perceive pleasant objects,

78
00:10:52,080 --> 00:10:55,920
and feel sensations of wanting to incline towards pleasant objects,

79
00:10:55,920 --> 00:11:00,960
although not attached in the same way as before. Could you please share your own experience

80
00:11:00,960 --> 00:11:06,400
post third path with respect to sensual desire and whether fourth path makes a difference in

81
00:11:06,400 --> 00:11:13,680
perceiving pleasant objects? Although I still experience theorizing of a thought that

82
00:11:14,320 --> 00:11:20,800
that object is pleasant when contracting pleasant objects, contacting pleasant objects.

83
00:11:20,800 --> 00:11:26,080
Yeah, I mean, it doesn't sound like you've reached the third path. The third

84
00:11:26,080 --> 00:11:32,400
fruit, actually, path is just one moment. So if you've attained the third path, you've also

85
00:11:32,400 --> 00:11:38,960
attained the third fruit. This path is only one moment. It's followed immediately,

86
00:11:38,960 --> 00:11:51,840
necessarily, by third fruit. Yeah, any sensations of wanting to incline towards pleasant objects

87
00:11:51,840 --> 00:12:02,240
are a sign of low by men of Kamaraga. So this is my feeling is that it's someone who hasn't

88
00:12:02,240 --> 00:12:12,400
attained anigami. That's to my own attainment. How do you know I've attained third path?

89
00:12:12,400 --> 00:12:22,880
Whoever told you that? It's not something, not something ordinary to become an anigami.

90
00:12:22,880 --> 00:12:30,400
It's actually probably pretty rare in this day and age, but I have a policy not to talk about

91
00:12:30,400 --> 00:12:37,760
myself. So I can't answer. Next question. Dear Bhante, my other leg is on the path.

92
00:12:39,440 --> 00:12:45,600
My other leg is on the path, the other one in sensual world, when trying to progress on the path,

93
00:12:45,600 --> 00:12:51,680
for example, practicing a lot during a few days and being mostly alone, and afterwards handling

94
00:12:51,680 --> 00:12:57,280
lay-life issues, going to the bank and meeting people, etc. I feel miserable. I see how worthless

95
00:12:57,280 --> 00:13:02,400
everything is. Then I feel I'm making people, I meet, feel miserable just by interacting with them

96
00:13:02,400 --> 00:13:08,720
in the way of not being capable to show my interest whatsoever. Then making them miserable makes

97
00:13:08,720 --> 00:13:15,520
me want to adjust myself by what I feel is creating delusional ideals about reality in order to

98
00:13:15,520 --> 00:13:21,520
make those people find me less intimidating so they will feel better. And this seems to take me

99
00:13:21,520 --> 00:13:28,000
backwards on the path. So this is a kind of a sea-song motion. What can I do? Eventually, I must

100
00:13:28,000 --> 00:13:33,920
totally dive into the path and let go completely, but I can't do that yet. How to look at the situation?

101
00:13:38,160 --> 00:13:44,640
Well, you go from where you are, no? You can't start somewhere else, but

102
00:13:44,640 --> 00:13:51,840
the path starts from where you are. That's really the important thing to get across.

103
00:13:54,000 --> 00:14:00,720
Because we moan and complain because we're not able to start where we want,

104
00:14:02,480 --> 00:14:07,760
but the path is right in front of you. It starts right where you are. It doesn't mean you have to

105
00:14:07,760 --> 00:14:14,320
become a monk. It's not all or nothing. That would be jumping from where you are to

106
00:14:14,320 --> 00:14:21,120
somewhere else. In fact, becoming a monk often doesn't solve the problem at all. The only

107
00:14:21,120 --> 00:14:30,400
way out is through the practice of mindfulness. So the Buddha's advice still applies stuff breaks

108
00:14:30,400 --> 00:14:40,960
sober up, which means all of the things that you make you feel miserable. That's what you have

109
00:14:40,960 --> 00:14:45,920
to get sober about. You have to realize that it's impermanent. It breaks. It happens. That's part

110
00:14:45,920 --> 00:14:50,160
of nature. Teach yourself that. Teach yourself to let go.

111
00:14:55,600 --> 00:15:01,680
The part about I feel I'm making the people I meet, feel miserable just by interacting with them

112
00:15:01,680 --> 00:15:06,400
and not being able to show my interest whatsoever in what they're doing. It

113
00:15:06,400 --> 00:15:20,720
happens. Sometimes you have to give a cursory approval of people's interests or something

114
00:15:20,720 --> 00:15:27,520
interest people you have to smile and say good for you. People say I just got married and you

115
00:15:27,520 --> 00:15:31,600
can say congratulations because that's what people say even though you think, oh no, marriage

116
00:15:31,600 --> 00:15:40,240
is a good thing. Marriage is better than the alternative of promiscuity and so on because marriage

117
00:15:40,240 --> 00:15:48,000
is a teamwork kind of thing and it's a morality kind of thing. Of course, it's not better than

118
00:15:48,000 --> 00:15:58,880
the alternative, which is not to become asexual. If we're a layperson, marriage is reasonable, I think.

119
00:15:58,880 --> 00:16:11,040
Agentong is very interesting because the people around him have very little patience.

120
00:16:11,040 --> 00:16:15,840
It's sad for some of the people. I mean, some good people are sometimes around him but sometimes

121
00:16:16,880 --> 00:16:23,440
men are just to get surrounded by not so mindful people and there will be always very angry

122
00:16:23,440 --> 00:16:29,200
at the people who try to come and waste his time. But Agentong, just I would have to sit there for hours

123
00:16:30,080 --> 00:16:35,920
and sometimes one of the old nuns would come up and just talk is you're off about nothing,

124
00:16:35,920 --> 00:16:42,400
about meaningless things, like maybe their aches and pains and the medicines that they're taking

125
00:16:42,400 --> 00:16:46,080
and talking about how they saw someone that they both know because these are people who have

126
00:16:46,080 --> 00:16:51,600
known Agentong for 30, 40 years and so they're talking to him about, you know, I met this person

127
00:16:51,600 --> 00:16:56,640
and you remember this person and now they're doing this right now and he just sits and he'll

128
00:16:56,640 --> 00:17:05,360
actually, oh, oh, oh, oh, oh, oh, oh. So you learn this very important word for a Buddhist monk

129
00:17:05,360 --> 00:17:13,280
and it's mm, mm, mm, mm, mm, mm, mm, mm, mm, mm, mm, mm. It's very non-committal. Someone

130
00:17:13,280 --> 00:17:24,480
asked you, is that good? You think that's good? Isn't that awesome? So now when you make that sound,

131
00:17:24,480 --> 00:17:32,720
we know. We know. Yeah, it's funny because I was told that it actually sounds kind of weird,

132
00:17:32,720 --> 00:17:38,480
why are you going? What does it mean to say? But it's a very common thing in Thailand actually.

133
00:17:38,480 --> 00:17:46,000
I think that's what Agentong does. What is kind of perfect to your acknowledging someone said

134
00:17:46,000 --> 00:17:51,280
something, but you're not, you're not really approving or disapproving.

135
00:17:55,120 --> 00:17:59,920
So what time is daily dhama really? It didn't happen at the time the app indicated.

136
00:18:00,560 --> 00:18:05,360
That's right. The app is still on the app is still on the app is still on the app.

137
00:18:05,360 --> 00:18:09,920
Don't usually happen until February. That's the answer.

138
00:18:12,720 --> 00:18:19,280
Actually that's funny because that means what? No, that means it's, yeah,

139
00:18:19,280 --> 00:18:24,320
I got lazy and just changed it in the JavaScript. But if I changed it in the database,

140
00:18:24,320 --> 00:18:40,240
that would work. I think why did it work like that? Should have changed it in the PHP.

141
00:18:41,440 --> 00:18:46,800
I can do that. I can fix that I think. It's not like I have a million other things to be doing.

142
00:18:46,800 --> 00:19:04,400
I think so. Oh no. Since changing from an Asian diet to a Western one, have you observed any

143
00:19:04,400 --> 00:19:11,360
changes in the way your body and mind is functioning? Yeah, I mean a lot of the Asian diet that I

144
00:19:11,360 --> 00:19:20,240
would have from time to time. It wasn't about being an Asian diet. There's some Asian societies

145
00:19:20,240 --> 00:19:25,200
or less health conscious than the Western. That's actually maybe not fair to say. I just

146
00:19:26,160 --> 00:19:31,920
find more health alternatives here. I don't know. That's not even fair to say because Western food

147
00:19:31,920 --> 00:19:37,920
can be really terrible as well. It's not like that. It's more like I've been in places where

148
00:19:37,920 --> 00:19:44,080
the food that I ate was not very healthy. You can feel that that changes your body. It's an

149
00:19:44,080 --> 00:19:49,520
interesting state that's actually can be quite useful for your practice to be in the state of

150
00:19:49,520 --> 00:19:59,280
under nutrition. In other places where I get really rich food and look and some Swiss food,

151
00:19:59,840 --> 00:20:05,200
Los Angeles comes to mind. When I was in LA, I got the top creme of the creme of food,

152
00:20:05,200 --> 00:20:12,160
all super healthy, super nutritious. But it's not to say that that's better for your practice.

153
00:20:12,160 --> 00:20:17,840
That connects the need to negligence. Whatever. It's just food.

154
00:20:17,840 --> 00:20:32,800
And that is the last question. That was the last question.

155
00:20:32,800 --> 00:20:56,160
Oh, so is it wrong? Does that mean the website got it wrong as well at time? We're going to have

156
00:20:56,160 --> 00:21:03,680
this thing because it looks like I didn't actually change it. It looks like I was preparing to

157
00:21:03,680 --> 00:21:33,520
change it and then I didn't. Yeah, I can fix that.

158
00:21:33,680 --> 00:21:37,920
Should be able to fix it. I'll take it up anyway. So that's all for tonight then.

159
00:21:38,560 --> 00:21:43,520
Yes. Well, just there was a lot of question earlier about time changes and things.

160
00:21:43,520 --> 00:21:49,120
The individual meetings that you're doing, did they change with the time change?

161
00:21:50,160 --> 00:21:52,880
There's been some confusion there, but I think we've got it worked out.

162
00:21:55,680 --> 00:22:00,240
Anyway, that's not perfect. No, I'm just wondering for my breaks.

163
00:22:00,240 --> 00:22:05,440
Yes. So it does change with the daylight savings time or the lack of?

164
00:22:05,440 --> 00:22:15,280
Well, it won't change here. I mean, for me, 7 a.m. and 6 p.m. Those are the time. 7 a.m. 7 a.m. 7 a.m.

165
00:22:15,280 --> 00:22:21,280
7 a.m. 7 30 6 p.m. 6 30. So figure that out. Those are the times.

166
00:22:22,080 --> 00:22:25,280
I don't think there was an issue and a different issue today. It wasn't to do with that.

167
00:22:25,280 --> 00:22:30,080
Oh, okay. Good vision. Anyway, good night, everyone.

168
00:22:31,440 --> 00:22:34,480
Thank you. I should have a demo product again tomorrow.

169
00:22:34,480 --> 00:22:58,320
Okay, good night. Good night. Thank you.

