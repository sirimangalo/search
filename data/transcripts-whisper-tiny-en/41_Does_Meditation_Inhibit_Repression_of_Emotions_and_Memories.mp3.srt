1
00:00:00,000 --> 00:00:07,000
Does meditation inhibit repression of emotions and memories?

2
00:00:07,000 --> 00:00:08,000
Question.

3
00:00:08,000 --> 00:00:16,000
In the context of my daily meditation practice, I find the concept of repression of emotions hard to grasp.

4
00:00:16,000 --> 00:00:23,000
Do you think meditation inhibits people from being able to repress emotions or memories?

5
00:00:23,000 --> 00:00:25,000
Answer.

6
00:00:25,000 --> 00:00:29,000
Repression is a bit of a difficult concept.

7
00:00:29,000 --> 00:00:34,000
It is not dealt with the same in Buddhism as it is in ordinary discourse.

8
00:00:34,000 --> 00:00:41,000
Repression is usually seen as a negative thing, something that causes problems.

9
00:00:41,000 --> 00:00:50,000
Digging up is often seen in ordinary discourse as a good thing to do, to come to terms with our repressed emotions.

10
00:00:50,000 --> 00:00:58,000
There are some parallels to this in Buddhism, but it is not understood quite the same.

11
00:00:58,000 --> 00:01:05,000
Momentary repression of emotions is actually technically what we are doing in the practice of meditation.

12
00:01:05,000 --> 00:01:15,000
When you practice loving kindness meditation, you are repressing or suppressing the anger through the force of loving kindness,

13
00:01:15,000 --> 00:01:18,000
through the practice of mindfulness.

14
00:01:18,000 --> 00:01:25,000
When you say pain, pain, or so on, the anger is being repressed.

15
00:01:25,000 --> 00:01:35,000
It is understood as prevention, stopping it from arising as technically there is no emotion being suppressed and there is no suppressor.

16
00:01:35,000 --> 00:01:41,000
There are only momentary experiences that arise and cease.

17
00:01:41,000 --> 00:01:52,000
Technically, when you repress something, all that happens is you create a specific mind state that prevents another mind state from arising.

18
00:01:52,000 --> 00:02:00,000
What we call repression generally has to do with anger, aversion towards the mind state.

19
00:02:00,000 --> 00:02:03,000
Something arises and you get angry about it.

20
00:02:03,000 --> 00:02:07,000
The anger does not actually repress the emotion.

21
00:02:07,000 --> 00:02:11,000
It has already been replaced with the anger.

22
00:02:11,000 --> 00:02:17,000
Whatever it was that you disliked or were afraid of, etc., was not repressed.

23
00:02:17,000 --> 00:02:23,000
It triggered that response and ceased when the response arose.

24
00:02:23,000 --> 00:02:35,000
You might say to yourself, no, no, no, don't let it come up thinking that somehow you are going to prevent something that has already arisen.

25
00:02:35,000 --> 00:02:40,000
You see it as a problem, though it has already passed.

26
00:02:40,000 --> 00:02:43,000
What this does is create a habit.

27
00:02:43,000 --> 00:02:55,000
Every time the emotion comes up rather than dealing with it, you react with aversion, anger, fear, etc., and cut off the emotion.

28
00:02:55,000 --> 00:03:04,000
Suppose you experience lust arising, you see a beautiful woman or a beautiful man and lust arises.

29
00:03:04,000 --> 00:03:14,000
Then you might get angry at yourself and feel guilty thinking, how horrible, what a sin, what a negative mind state.

30
00:03:14,000 --> 00:03:20,000
When this anger arises, the lust has no potential to continue for the moment.

31
00:03:20,000 --> 00:03:29,000
Now you have a different problem, these feelings of guilt, anger, self-hatred, and so on.

32
00:03:29,000 --> 00:03:39,000
As this becomes habitual, it can seem like you are repressing the lust, but you are actually just changing the focus of the mind,

33
00:03:39,000 --> 00:03:44,000
complicating the experience rather than understanding it objectively.

34
00:03:44,000 --> 00:03:50,000
The lust in the first place is a habit that has developed over time.

35
00:03:50,000 --> 00:03:54,000
There is no reason to have attraction to the human body.

36
00:03:54,000 --> 00:04:01,000
Science can explain attraction away based on genes, hormones, and so on.

37
00:04:01,000 --> 00:04:09,000
But from a Buddhist point of view, looking at things experientially, in the moment-to-moment mind states,

38
00:04:09,000 --> 00:04:15,000
there is no reason why we should come to this state of finding the human body pleasant.

39
00:04:15,000 --> 00:04:17,000
It is a contrived state.

40
00:04:17,000 --> 00:04:25,000
The human body, genes, and hormones are all part of this contrived state of existence that we have built up,

41
00:04:25,000 --> 00:04:32,000
that we have cultivated as a part of what scientists call natural selection.

42
00:04:32,000 --> 00:04:46,000
Emotions like attraction and desire are simple habits, but the evolution of human society has complicated them with habits of guilt, self-consciousness, etc.

43
00:04:46,000 --> 00:04:51,000
You might say that this is something that we have cultivated as a species.

44
00:04:51,000 --> 00:04:57,000
Now humans have innately in them this feeling of guilt towards sexuality.

45
00:04:57,000 --> 00:05:01,000
This is how what we call repression works.

46
00:05:01,000 --> 00:05:04,000
It is just a more complicated habit.

47
00:05:04,000 --> 00:05:11,000
In meditation we sometimes have to deal with incredibly complicated habits and mind states.

48
00:05:11,000 --> 00:05:16,000
We cannot simply deal with the lust or hatred that we have in the mind.

49
00:05:16,000 --> 00:05:24,000
We have to look at how we are reacting to these emotions themselves because our habits and our minds are so complex.

50
00:05:24,000 --> 00:05:35,000
Our habits are so complex that we might first get angry, and then we might be angry that anger has arisen, or afraid of it, or depressed by it.

51
00:05:35,000 --> 00:05:43,000
And then maybe we try to divert ourselves by finding something pleasurable, and so on, and so on.

52
00:05:43,000 --> 00:05:51,000
We have tricks and defense mechanisms and all sorts of habits that we have developed so haphazardly.

53
00:05:51,000 --> 00:05:56,000
We have to muck through them piece by piece in our meditation practice.

54
00:05:56,000 --> 00:06:04,000
Repression is only a part of the complexity of our minds, and it is really an inexact term.

55
00:06:04,000 --> 00:06:11,000
What we should talk about is that complex chains of reactivity are minds are habitually inclined towards.

56
00:06:11,000 --> 00:06:18,000
First we have ordinary lust or anger arising, and then we have our responses to it.

57
00:06:18,000 --> 00:06:23,000
We have to be able to deal skillfully with both and unravel them together.

58
00:06:23,000 --> 00:06:31,000
Not just dealing with the lust or what one might call the repression, but dealing with both together.

59
00:06:31,000 --> 00:06:39,000
You must also deal with positive mind states being mindful of everything piece by piece.

60
00:06:39,000 --> 00:06:44,000
This is a very important topic to understand from a meditative point of view.

61
00:06:44,000 --> 00:06:49,000
To understand that it is not as simple as just repression.

62
00:06:49,000 --> 00:06:59,000
It is much more complicated, and rather than trying to say, I am repressing, or I have this tendency.

63
00:06:59,000 --> 00:07:03,000
We should look at the experiences moment to moment.

64
00:07:03,000 --> 00:07:13,000
Meditation is, in one sense, simply becoming increasingly skillful at facing the convolutions and knots in the mind without perpetuating them.

65
00:07:13,000 --> 00:07:28,000
When the Buddha was asked who could untangle the inner tangle, the complex reactions we have been discussing, and the outer tangle, the objects that trigger our reactions.

66
00:07:28,000 --> 00:07:38,000
He said that a wise and energetic BQ will be able to untangle the tangle by means of morality, concentration, and wisdom.

67
00:07:38,000 --> 00:07:47,000
Briefly, from a meditative point of view, morality is developed by bringing the mind back to the object.

68
00:07:47,000 --> 00:07:56,000
Concentration is the focus that arises when you do that, and wisdom is what you see when your mind is in focus.

69
00:07:56,000 --> 00:08:18,000
The meditation practice, then, is the development of morality, concentration, and wisdom that slowly allows you to simplify and purify your habits, bringing your mind to experience reality as it is, instead of reacting and then reacting to your reactions.

