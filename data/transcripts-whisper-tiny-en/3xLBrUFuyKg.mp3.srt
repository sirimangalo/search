1
00:00:00,000 --> 00:00:06,720
Hi. So in this video I will be talking about some of the important qualities of

2
00:00:06,720 --> 00:00:10,400
meditation which we need to keep in mind during the time that we do the walking

3
00:00:10,400 --> 00:00:14,360
or the sitting meditation. So it's not enough for us to say that we're doing the

4
00:00:14,360 --> 00:00:18,960
walking, we're doing the sitting on a regular basis and expect that we should

5
00:00:18,960 --> 00:00:23,600
all receive the same benefits from the practice no matter what's going on in

6
00:00:23,600 --> 00:00:27,560
our minds or how we're carrying out the practice. Our practice has to have

7
00:00:27,560 --> 00:00:32,960
certain important qualities to it in order for us to consider that our

8
00:00:32,960 --> 00:00:37,680
practice is a success in order for us to really get something out of it. The

9
00:00:37,680 --> 00:00:42,120
first important quality of meditation practice is that we have to make the

10
00:00:42,120 --> 00:00:45,520
acknowledgement in the present moment. Our minds should always be in the here

11
00:00:45,520 --> 00:00:50,400
and now. We cannot let our minds fall into the past or go ahead into the future.

12
00:00:50,400 --> 00:00:54,080
We shouldn't think about how many more minutes are left in the practice or

13
00:00:54,080 --> 00:00:57,640
how many minutes we've practiced or so on. Our minds should always be in the

14
00:00:57,640 --> 00:01:05,080
present moment. We should also be continuously in the present moment. So not

15
00:01:05,080 --> 00:01:09,320
only practicing this moment or this moment but that we are practicing

16
00:01:09,320 --> 00:01:13,440
continuously from one moment to the next. At the moment when we finish the

17
00:01:13,440 --> 00:01:18,480
walking meditation we should try to continue our awareness and our

18
00:01:18,480 --> 00:01:22,960
acknowledgement of the present moment into the sitting meditation. Once we

19
00:01:22,960 --> 00:01:26,520
finish the sitting meditation when we're going to get up and go on with our

20
00:01:26,520 --> 00:01:32,280
lives we should try to continue into our lives. Carry on continuously. During

21
00:01:32,280 --> 00:01:35,520
the time that we're doing the walking meditation we should try to keep our

22
00:01:35,520 --> 00:01:40,000
minds in the present moment through the whole whole of the practice. When we do

23
00:01:40,000 --> 00:01:42,800
the sitting trying to keep it on the rising and falling again and again

24
00:01:42,800 --> 00:01:49,200
continuously. Just like the rain falling it is said that meditation practice is

25
00:01:49,200 --> 00:01:54,120
like rain falling. Every moment that we're aware is like one drop of rain and

26
00:01:54,120 --> 00:01:57,840
though it may not seem like that much once we're mindful again and again and

27
00:01:57,840 --> 00:02:03,240
again and again and again just like rain falling it can flood and create a

28
00:02:03,240 --> 00:02:07,960
huge flood or fill up a lake. In the same way our awareness again and again in

29
00:02:07,960 --> 00:02:12,880
one moment after one moment can create very strong states of concentration

30
00:02:12,880 --> 00:02:18,000
based on the present moment so that we're very clearly aware and we create a

31
00:02:18,000 --> 00:02:24,160
great understanding and clarity of mind in regards to reality.

32
00:02:24,160 --> 00:02:29,200
Another important point is the actual acknowledgement or the quality of the

33
00:02:29,200 --> 00:02:35,160
acknowledgement. We have to first of all have a certain amount of effort. It takes

34
00:02:35,160 --> 00:02:39,480
effort for us to do this again and again and we have to acknowledge this and we

35
00:02:39,480 --> 00:02:43,520
can't just let ourselves say rising, falling, rising, falling and let the mind

36
00:02:43,520 --> 00:02:48,480
drift. We have to push the mind and keep the mind with the present moment, with

37
00:02:48,480 --> 00:02:52,640
the object as it arises, whatever object that is. So if it's the rising and the

38
00:02:52,640 --> 00:02:56,880
falling in the beginning, staying with it again and again, sending the mind out

39
00:02:56,880 --> 00:03:01,440
instead of keeping the mind up here or at the mouth, sending the mind out to

40
00:03:01,440 --> 00:03:05,720
the object again and again and again and keeping the mind very sort of a

41
00:03:05,720 --> 00:03:13,360
strong state of mind, keeping our mind strong and focused on the present moment.

42
00:03:13,360 --> 00:03:17,320
Once we do this then we'll begin to know, become aware of the object and this is

43
00:03:17,320 --> 00:03:20,920
another important thing that we're actually aware of the object as it is

44
00:03:20,920 --> 00:03:24,480
occurring so that we're not just saying rising, falling but once we push our

45
00:03:24,480 --> 00:03:29,880
minds to the object that we're actually know from the beginning to the end of

46
00:03:29,880 --> 00:03:35,080
the of the motion. If it's pain that we actually are aware of the pain, if it's

47
00:03:35,080 --> 00:03:39,640
thoughts that we're actually aware of the thought. Once we become aware of the

48
00:03:39,640 --> 00:03:42,800
thought then we can make the acknowledgement and this is of course the most

49
00:03:42,800 --> 00:03:47,520
important part is that actually we grasp and we fix the mind on that

50
00:03:47,520 --> 00:03:52,440
awareness, not letting it continue on into liking or disliking or creating all

51
00:03:52,440 --> 00:04:00,240
sorts of mental activity or judgments about the object. The final important

52
00:04:00,240 --> 00:04:07,080
fundamental quality of practice is called balancing the faculties. So it's

53
00:04:07,080 --> 00:04:11,160
recognized that all people have certain faculties in their mind. These are

54
00:04:11,160 --> 00:04:17,320
qualities of mind which are very beneficial. Altogether there are five, these are

55
00:04:17,320 --> 00:04:24,520
the confidence, effort, mindfulness, concentration and wisdom and these have to

56
00:04:24,520 --> 00:04:29,040
we say these have to be balanced with each other. For instance some people

57
00:04:29,040 --> 00:04:34,320
might have strong confidence but they may have low wisdom. This means that

58
00:04:34,320 --> 00:04:39,560
they have what we call blind faith. They believe things simply because of this

59
00:04:39,560 --> 00:04:45,160
is what they've been told or because of some desire to believe not because of

60
00:04:45,160 --> 00:04:51,240
any rational understanding. Some people have strong wisdom but low faith so

61
00:04:51,240 --> 00:04:56,400
they doubt everything and they refuse to believe anything even from a

62
00:04:56,400 --> 00:05:00,680
respected authority or so on or they refuse to believe anything because they

63
00:05:00,680 --> 00:05:04,120
themselves have never experienced it because they've never undertaken the

64
00:05:04,120 --> 00:05:11,480
meditation practice. Some people have strong effort but weak concentration in

65
00:05:11,480 --> 00:05:17,000
this case the mind becomes distracted where the mind is not focused, can't focus on

66
00:05:17,000 --> 00:05:23,400
anything. Some people have strong concentration but weak effort and this

67
00:05:23,400 --> 00:05:28,680
puts makes people lazy or drowsy, keeps us from sending the mind on and on and

68
00:05:28,680 --> 00:05:32,640
on and keeping in the present, keeping up with the present moment and we find

69
00:05:32,640 --> 00:05:38,480
ourselves getting drowsy and falling asleep. The way we balance these we

70
00:05:38,480 --> 00:05:43,080
balance confidence with confidence with wisdom and effort with

71
00:05:43,080 --> 00:05:47,840
concentration. We use mindfulness. We use this clear thought. The effort

72
00:05:47,840 --> 00:05:51,720
sending the mind to the object, the awareness of the object, the focusing on

73
00:05:51,720 --> 00:05:55,800
the object and the clear knowing of the object. These are all of the mental

74
00:05:55,800 --> 00:06:01,200
faculties put into one. Confidence, effort, mindfulness, concentration and wisdom.

75
00:06:01,200 --> 00:06:07,480
Once we've balanced the four outside faculties with the faculty of mindfulness

76
00:06:07,480 --> 00:06:11,120
or the clear awareness, they will work together and they will create a very

77
00:06:11,120 --> 00:06:16,160
strong state of mind where we have strong confidence, we have strong effort, we

78
00:06:16,160 --> 00:06:22,080
are clearly mindful and we have strong concentration and we have very sure

79
00:06:22,080 --> 00:06:26,640
wisdom because we see for ourselves and we understand for ourselves that they

80
00:06:26,640 --> 00:06:31,520
need to rely on someone else. At that time this is when the mind is able to

81
00:06:31,520 --> 00:06:35,880
overcome states of suffering, overcome states of depression, it's able to

82
00:06:35,880 --> 00:06:43,040
like a strong man is able to bend an iron bar. We also can bend and shape and

83
00:06:43,040 --> 00:06:48,720
mold our minds and bring our minds back to a state of peace and happiness

84
00:06:48,720 --> 00:06:54,840
overcoming all sorts of unpleasant states. This is a basic understanding of

85
00:06:54,840 --> 00:06:59,600
some of the fundamental qualities of meditation which we need to keep in

86
00:06:59,600 --> 00:07:04,680
mind. We need to be in the present moment and practice continuously. We need to

87
00:07:04,680 --> 00:07:09,840
make the clear thought and we can't just say to ourselves rising falling, we

88
00:07:09,840 --> 00:07:13,640
have to actually know what's going on in the present moment and we have to

89
00:07:13,640 --> 00:07:18,760
balance the faculties. This is another in the series on how to meditate and

90
00:07:18,760 --> 00:07:24,520
this is sort of a addition to the actual technique of meditation which is

91
00:07:24,520 --> 00:07:32,120
meant to give a better, stronger quality and a greater benefit to the

92
00:07:32,120 --> 00:07:36,360
meditation practice. So I hope that through this you are able to find peace and

93
00:07:36,360 --> 00:07:41,120
happiness and freedom from suffering. Thank you again for tuning in and all the

94
00:07:41,120 --> 00:07:51,120
best.

