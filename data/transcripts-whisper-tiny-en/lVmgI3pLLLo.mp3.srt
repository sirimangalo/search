1
00:00:00,000 --> 00:00:26,440
So, everyone, welcome to our weekly Q&A demo session.

2
00:00:30,000 --> 00:00:39,440
Today I thought we do a little meditating together.

3
00:00:39,440 --> 00:00:53,680
I've been asked again to do a meditation session with the Tourism Authority of Thailand.

4
00:00:53,680 --> 00:01:09,920
So, we'll be doing that this next week.

5
00:01:09,920 --> 00:01:14,920
I have to record a video for them and then we'll be doing live.

6
00:01:14,920 --> 00:01:26,240
So, we'll be doing a live stream of people who can join.

7
00:01:26,240 --> 00:01:42,320
Today I thought we would meditate together.

8
00:01:42,320 --> 00:01:48,640
So, come on in, close your eyes.

9
00:01:48,640 --> 00:02:00,960
Take up a posture that allows you to be at rest, but be alert and focus on your body

10
00:02:00,960 --> 00:02:14,200
in your mind.

11
00:02:14,200 --> 00:02:28,680
Today in the study group, we read a student by Sariputa and discourse by the venerable Sariputa

12
00:02:28,680 --> 00:02:46,120
and Buddhas, chief disciple, who took up the Buddha's teaching and expanded upon it, showing

13
00:02:46,120 --> 00:03:02,720
that every aspect of the Buddha's teaching is a fractal that could be magnified and expanded

14
00:03:02,720 --> 00:03:20,160
upon and that it all ends up being a part of ordinary experience.

15
00:03:20,160 --> 00:03:36,720
When we think about the Buddha, we think about how the Buddha encouraged us to relate

16
00:03:36,720 --> 00:03:52,200
to experience, to free our minds from greed and anger and delusion.

17
00:03:52,200 --> 00:04:02,000
We remember the Buddha as a pure example, an example of purity, an example of the experience

18
00:04:02,000 --> 00:04:17,280
of reality simply as it is without judgment or reaction or bias or skew in this perception.

19
00:04:17,280 --> 00:04:26,920
When we think of the dhamma, we think of reality, we think of truth, think of what is.

20
00:04:26,920 --> 00:04:35,960
The dhamma of the Buddha was not, his beliefs, his theories, his philosophies, which

21
00:04:35,960 --> 00:04:58,000
is discoveries, his understanding, his knowledge.

22
00:04:58,000 --> 00:05:07,440
We think of the Buddha, we think of those who have meditated and we think of ourselves emulating

23
00:05:07,440 --> 00:05:21,440
following in the footsteps, of those who have practiced rightly.

24
00:05:21,440 --> 00:05:44,280
We think of living up to the ideal of the pure being, living with a clear mind and above

25
00:05:44,280 --> 00:06:06,920
all we focus on our own mundane experience to become one who sees the truth, sees reality,

26
00:06:06,920 --> 00:06:20,440
sees the dhamma, does it mean to see the dhamma, does it mean to believe it, doesn't

27
00:06:20,440 --> 00:06:34,560
mean to understand it rationally, but does it see the dhamma, sees teachings, goes beyond

28
00:06:34,560 --> 00:06:47,520
knowledge in an intellectual realm, the false knowing for yourself because you've seen.

29
00:06:47,520 --> 00:06:58,520
So today let's just spend a little time together.

30
00:06:58,520 --> 00:07:04,960
We'll go for about 10 minutes, just practicing together.

31
00:09:58,520 --> 00:09:59,520
If you like this video, please like, share and subscribe.

32
00:09:59,520 --> 00:10:00,520
Thank you for watching.

33
00:10:00,520 --> 00:10:01,520
If you want to see more videos, please like, share and subscribe.

34
00:10:01,520 --> 00:10:02,520
Thank you for watching.

35
00:10:02,520 --> 00:10:03,520
Thank you for watching.

36
00:10:03,520 --> 00:10:29,520
Thank you for watching.

37
00:10:33,520 --> 00:10:34,520
Thank you for watching.

38
00:10:34,520 --> 00:10:54,520
Thank you for watching.

39
00:11:04,520 --> 00:11:05,520
Thank you.

40
00:11:34,520 --> 00:11:35,520
Bye.

41
00:12:04,520 --> 00:12:05,520
Bye.

42
00:12:34,520 --> 00:12:35,520
Bye.

43
00:13:04,520 --> 00:13:05,520
Bye.

44
00:13:34,520 --> 00:13:35,520
Bye.

45
00:14:04,520 --> 00:14:05,520
Bye.

46
00:14:34,520 --> 00:14:35,520
Bye.

47
00:14:35,520 --> 00:14:55,520
Bye.

48
00:15:05,520 --> 00:15:06,520
Bye.

49
00:15:06,520 --> 00:15:06,520
Bye.

50
00:15:06,520 --> 00:15:26,520
Bye.

51
00:15:36,520 --> 00:15:37,520
Bye.

52
00:15:37,520 --> 00:15:37,520
Bye.

53
00:15:37,520 --> 00:15:57,520
Bye.

54
00:16:08,520 --> 00:16:29,520
Let's go.

55
00:16:29,520 --> 00:16:41,200
keep them closed as you prefer, but I'll have to move on to taking questions. So if you have questions,

56
00:16:42,960 --> 00:16:50,160
feel free to type them into the chat. Let's meet today in our

57
00:16:50,160 --> 00:17:07,440
Chris and we have Olu in Olivia. We'll be helping us to organize and ask the questions.

58
00:17:11,440 --> 00:17:17,360
But by all means, if you don't have questions or if you ask your question, just close your eyes

59
00:17:17,360 --> 00:17:27,200
and continue to meditate with us. We can meditate and listen. Our mind is not capable of doing

60
00:17:27,200 --> 00:17:34,720
two things at once, but it is capable of switching activities very quickly. So in between our

61
00:17:34,720 --> 00:17:42,240
thinking and in between our listening for comprehension, we can listen for understanding,

62
00:17:42,240 --> 00:17:52,240
practicing together like this is a valuable exercise. It's not something we should depend upon.

63
00:17:52,240 --> 00:18:00,080
We should be able to practice on our own, but it's really the best way to come together as a

64
00:18:00,080 --> 00:18:06,400
group to provide encouragement to remind us that we're not alone,

65
00:18:11,760 --> 00:18:20,000
to appreciate the plausibility of practice that it is possible. We see others doing it.

66
00:18:20,000 --> 00:18:29,760
We feel not alone, we feel encouraged and supported in our practice.

67
00:18:32,800 --> 00:18:42,240
We fulfill the qualifications as a group of the word Sangha,

68
00:18:42,240 --> 00:18:54,560
even though we come to the practice, not as an area, but as a putujana, as an ordinary

69
00:18:54,560 --> 00:19:05,360
worldly person with misunderstandings and delusions and ignorance. But because of our intentions

70
00:19:05,360 --> 00:19:13,200
in the right direction, we become Kalyana, we become Kalyana, putujana, an ordinary person,

71
00:19:13,200 --> 00:19:21,760
but not so ordinary because of our beauty, our beautiful mind, our beautiful intention

72
00:19:21,760 --> 00:19:38,480
to see things clearly. So we'll take questions, we'll see how many there are, maybe slow.

73
00:19:39,440 --> 00:19:44,880
Slow coming is fine, we'll just take it slow. I'm ready whenever you are, Chris.

74
00:19:44,880 --> 00:19:56,080
Okay, let's begin. What does it mean to note with discernment? Does discernment mean absence of thoughts?

75
00:20:00,480 --> 00:20:08,000
discernment means absence of delusion, discernment means a strength of mind,

76
00:20:08,880 --> 00:20:12,000
discernment is something that you don't have when you begin to meditate.

77
00:20:12,000 --> 00:20:20,720
You're not able to discern the qualities of things. You need some experience.

78
00:20:24,880 --> 00:20:32,240
And so through the process of systematic, repeated observation,

79
00:20:33,680 --> 00:20:38,640
you begin to discern and your noting has a power to it.

80
00:20:38,640 --> 00:20:47,920
There seems such a banality to noting things like rising, falling, but if and when you ever can

81
00:20:51,840 --> 00:20:58,960
note with discernment where you really just know rising is rising, falling is falling

82
00:20:58,960 --> 00:21:12,560
with a clarity and a purity and an objectivity that you've cultivated over the practice,

83
00:21:13,760 --> 00:21:21,840
you can never attain that state of discernment, that discernment

84
00:21:21,840 --> 00:21:30,320
that breaks free from some sorrow that causes the mind to relinquish its grasp,

85
00:21:37,920 --> 00:21:40,720
that discernment which frees the mind from suffering,

86
00:21:40,720 --> 00:21:49,920
allows one to experience true cessation of suffering, true peace, true freedom.

87
00:21:55,760 --> 00:22:01,040
So it doesn't have anything to do with thoughts per se, but doesn't necessarily mean the absence of

88
00:22:01,040 --> 00:22:21,840
thought. For one who is not scholarly or one who is not inclined towards studying the

89
00:22:21,840 --> 00:22:28,320
scriptures, is it correct to say that their faith in the Buddha, Dhamma and Sangha can guide their

90
00:22:28,320 --> 00:22:45,760
practice. I'm not sure that faith is something that can really guide you. Faith is a dangerous

91
00:22:45,760 --> 00:22:54,000
thing as a guide because it's a certainty, so if the faith isn't based on wisdom,

92
00:22:54,000 --> 00:23:09,440
faith is less of a guide than it is a leader. Faith is leading the charge, it has a power to it.

93
00:23:14,480 --> 00:23:19,280
The take a leader of an army, the leader of the army is able to organize the troops,

94
00:23:19,280 --> 00:23:28,720
but even a leader needs a guide to tell them, give them directions for where to go, where to lead

95
00:23:28,720 --> 00:23:36,720
the army. Faith is ready to lead the charge, but wisdom knows which way the enemy lies,

96
00:23:37,520 --> 00:23:48,000
knows how to, which way to march. So I wouldn't take your faith in the Buddha, the Dhamma,

97
00:23:48,000 --> 00:24:00,720
the Sangha is a guide, it's a useful quality, but it has to be coupled with wisdom on all levels,

98
00:24:00,720 --> 00:24:07,440
so there has to be some understanding intellectually of the Buddha's teaching in so far as it involves

99
00:24:09,040 --> 00:24:15,680
understanding of practice, how to practice. It's quite simple, really. You don't need a lot of

100
00:24:15,680 --> 00:24:24,720
intellectual understanding. Faith has to be guided by the knowledge of how to practice,

101
00:24:26,240 --> 00:24:34,960
and as you progress, faith is guided by the understanding that you gain the proficiency,

102
00:24:36,320 --> 00:24:43,760
the skill of the mind. The mind becomes more familiar with reality,

103
00:24:43,760 --> 00:24:48,560
it gets better at being present, being objective,

104
00:24:51,600 --> 00:24:55,440
being free from the filement and thus free from suffering,

105
00:24:58,160 --> 00:25:03,120
and which of course in turn increases our faith because it no longer relies on

106
00:25:05,600 --> 00:25:08,480
speculation or conjecture or belief

107
00:25:08,480 --> 00:25:20,000
that it has as its pace, the experience of things as they are.

108
00:25:26,640 --> 00:25:30,960
So don't be discouraged, it's certainly not about skull elasticism or

109
00:25:30,960 --> 00:25:40,720
any studying in the scriptures. That's one way of gaining the intellectual understanding,

110
00:25:40,720 --> 00:25:46,000
you need, but it's not the most efficient way. Most efficient is to find a teacher

111
00:25:51,520 --> 00:25:58,480
to provide you with practical instruction on how to see the things the Buddha taught for

112
00:25:58,480 --> 00:26:10,240
yourself, and let your confidence in that teaching guide you,

113
00:26:11,360 --> 00:26:17,120
that guide you, but propel you with the teaching as a guide you see,

114
00:26:20,240 --> 00:26:26,560
and then your faith increases, and that propels you and keeps you on track, keeps you,

115
00:26:26,560 --> 00:26:29,840
keeps you going, faith keeps you going,

116
00:26:31,360 --> 00:26:39,840
wisdom keeps you going in the right direction, so wisdom is more of the guide.

117
00:26:42,640 --> 00:26:49,040
How should we be mindful of having fun? Is it technically a combination of happy and liking?

118
00:26:49,040 --> 00:26:56,000
Yes, that's pretty perceptive, I think.

119
00:26:57,680 --> 00:27:02,000
What we call fun involves liking and it involves happiness,

120
00:27:03,920 --> 00:27:08,480
so happiness is not a problem, but liking is a problem, liking becomes addictive,

121
00:27:08,480 --> 00:27:14,960
and it relates to negligence and what we call intoxication, the mind is not

122
00:27:14,960 --> 00:27:27,680
perceptive, it's not alert or where, awake, it's distracted and predicted, and inclined towards

123
00:27:27,680 --> 00:27:30,880
seeking out things that can't satisfy.

124
00:27:38,880 --> 00:27:44,080
Someone told me I should not do meditation because I am not spiritually stable

125
00:27:44,080 --> 00:27:48,640
and that demons can attach to me because of this. What is your advice about this?

126
00:27:56,560 --> 00:28:01,040
Well, I can't comment on your situation. It may be that you have issues that

127
00:28:02,640 --> 00:28:07,440
issues of spiritual instability, it's not really how I would describe things,

128
00:28:07,440 --> 00:28:18,800
it's dangerous to use words like spirituality or spiritual stability.

129
00:28:21,760 --> 00:28:27,040
That's even worse, I think, to delve into the world of demons.

130
00:28:27,760 --> 00:28:30,880
I don't think we should ever be concerned with demons.

131
00:28:30,880 --> 00:28:38,240
I don't think we should be concerned with demons that might attach to us.

132
00:28:39,280 --> 00:28:47,840
What we should be concerned with is our state of mind and my general advice for people is that

133
00:28:48,960 --> 00:28:53,920
in meditation isn't something you undertake, it should be obvious, but it often isn't.

134
00:28:54,800 --> 00:28:57,760
Meditation isn't something that you should undertake because

135
00:28:57,760 --> 00:29:06,560
you were things like spiritually stable or calm, quiet,

136
00:29:08,160 --> 00:29:13,680
sometimes people's complaint is that they couldn't imagine meditating because they're not

137
00:29:14,400 --> 00:29:17,760
calm enough, they can't settle down enough.

138
00:29:19,440 --> 00:29:23,520
And that's of course a misunderstanding of what we mean when we talk about meditation.

139
00:29:23,520 --> 00:29:32,560
Meditation is a training to cultivate that sort of spiritual stability, mental stability,

140
00:29:36,000 --> 00:29:37,440
the mental peace of mind

141
00:29:41,440 --> 00:29:43,760
is a part of the ultimate goal.

142
00:29:43,760 --> 00:29:53,360
And the practice isn't ever expected to be peaceful or stable.

143
00:29:55,760 --> 00:29:59,840
It's meant to be chaotic as you look at how the mind works,

144
00:30:00,400 --> 00:30:05,600
the mind often works in quite a chaotic manner and it's not something you're ever necessarily going

145
00:30:05,600 --> 00:30:14,320
to liberate yourself from. What you're going to liberate yourself from is not the chaos. It's

146
00:30:14,320 --> 00:30:21,440
the reactions to it, some sara experience, life is chaotic.

147
00:30:26,160 --> 00:30:31,120
So don't be discouraged when there's not stability, don't be discouraged even when your mind is

148
00:30:31,120 --> 00:30:41,120
apparently the mind of an insane person, you often feel as you practice that your mind is not

149
00:30:41,120 --> 00:30:43,920
unwieldy, it's not manageable.

150
00:30:48,320 --> 00:30:53,440
So never really be discouraged when there's instability of mind when it appears that

151
00:30:53,440 --> 00:31:00,080
you have craziness inside. Don't even be discouraged if you're assaulted by demons.

152
00:31:01,200 --> 00:31:08,480
Some people hear voices, some people see things, sometimes it's hard to pass it off as hallucinations.

153
00:31:10,480 --> 00:31:16,240
But hallucinations or reality is not really, there's not really a difference

154
00:31:16,240 --> 00:31:24,000
because underpinning at all is our experience and our reactions to that experience,

155
00:31:24,640 --> 00:31:31,360
no matter whether we consider it to be real or imaginary hallucination, none of that really has

156
00:31:31,360 --> 00:31:38,480
any bearing on truth which is that this is an experience either way, it's the same.

157
00:31:38,480 --> 00:31:46,960
So we know to ourselves seeing and hearing, if you're afraid or worried, you know that,

158
00:31:46,960 --> 00:31:48,960
if you like or dislike, you know that.

159
00:31:52,880 --> 00:31:56,240
Meditation is about cultivating spiritual stability, so

160
00:32:01,040 --> 00:32:05,040
don't think of lack of that as being a reason not to meditate,

161
00:32:05,040 --> 00:32:11,200
check quite the opposite. Lack of stability is a reason my meditation is so important

162
00:32:12,000 --> 00:32:15,760
because without it, you just go on living your life lots of

163
00:32:16,720 --> 00:32:22,240
the hard way is not doing things that hurt you and hurt others because of your instability.

164
00:32:23,280 --> 00:32:28,000
You really need to meditate, you need mindfulness if you're ever going to find

165
00:32:28,000 --> 00:32:35,520
spiritual stability.

166
00:32:39,760 --> 00:32:45,120
I have moved to a different country due to a demanding job, so I stopped the course in the

167
00:32:45,120 --> 00:32:51,680
tenth week. I could not practice two hours a day anymore, would it be possible to get back on track

168
00:32:51,680 --> 00:32:59,360
and finish the course? Yes. Yes, we don't really have a hard and fast rule there.

169
00:33:00,480 --> 00:33:10,080
We do have a very strict rule so far about the time. Now two hours a day is what we expect you

170
00:33:10,080 --> 00:33:15,360
to get to at the end of the course, so probably at that point I was telling you you should be

171
00:33:15,360 --> 00:33:20,960
working up to it, but even at the beginning of the course you have to be prepared to work up to

172
00:33:20,960 --> 00:33:27,200
that. You can't be in a situation where you're not going to be able to do two hours a day,

173
00:33:28,080 --> 00:33:32,720
so it's good to wait until you have the opportunity. I mean continue to meditate, but

174
00:33:34,240 --> 00:33:40,000
wait once you have the opportunity to be able to do an hour a day and work up to two hours a day,

175
00:33:40,000 --> 00:33:44,560
then you can continue on. You can just continue where we left off.

176
00:33:44,560 --> 00:33:56,000
They add home courses and like the intensive course, but even the intensive course when you

177
00:33:56,000 --> 00:34:02,960
come to our center and practice intensively, even that can be potentially continued,

178
00:34:02,960 --> 00:34:15,760
even that can be split up into segments. Really the most important thing is that you keep up

179
00:34:16,400 --> 00:34:20,720
mindfulness, even even when you're not on a course.

180
00:34:20,720 --> 00:34:36,160
Is noting liking, calm, and disliking equivalent to the experiences of pleasant, neutral,

181
00:34:36,160 --> 00:34:52,080
and unpleasant.

182
00:35:06,160 --> 00:35:31,760
I was talking. I'll ask again, is noting liking, calm, and disliking equivalent to the experiences

183
00:35:31,760 --> 00:35:42,720
of pleasant, neutral, and unpleasant? Sorry, I think I was muted. No, they're not equivalent,

184
00:35:42,720 --> 00:35:51,760
so you can experience pleasant, unpleasant, and neutral feelings without liking and disliking.

185
00:35:52,640 --> 00:35:56,400
When you're neutral, a neutral feeling and calm, yes, those are equivalent,

186
00:35:56,400 --> 00:36:04,240
but liking and disliking our reactions. Now, there are certain

187
00:36:07,680 --> 00:36:10,960
pleasant experiences that are accompanied by liking,

188
00:36:12,320 --> 00:36:17,920
and there are certain unpleasant experiences that are accompanied by disliking, but that's not

189
00:36:17,920 --> 00:36:27,040
always the case. It's not necessarily the case. It's important that we separate our notings. If you

190
00:36:27,040 --> 00:36:34,480
like something note to liking, but if you're just happy, just not happy. If there's pleasure,

191
00:36:34,480 --> 00:36:40,640
you can say pleasure. There's pain, you can say pain. That's not necessarily the

192
00:36:40,640 --> 00:36:46,000
disliking of the pain. Of course, as a beginner, that often seems like it would be impossible.

193
00:36:46,000 --> 00:36:52,880
You can get to an experience where there's only pain without disliking of it.

194
00:36:53,760 --> 00:36:58,720
As in sports, I think it's common for people to experience pain without disliking,

195
00:36:58,720 --> 00:37:06,000
just because the mind is set in such a way as it's not phased by it. It's actually a little bit

196
00:37:06,000 --> 00:37:15,200
heady or high on the adrenaline and so on, so the pain is experienced just as pain.

197
00:37:17,520 --> 00:37:26,240
Now, in meditation, it's quite different from athleticism, but the state of mind, the frame of

198
00:37:26,240 --> 00:37:46,480
mind is very similar because of the acceptance. I often feel inferior to other people or ashamed

199
00:37:46,480 --> 00:37:58,000
of myself because of constant comparison and low self-esteem. What is the proper way to note this?

200
00:37:58,000 --> 00:38:04,960
Well, not the shame. That's a big one. The sadness or the disliking or the depression or whatever.

201
00:38:08,480 --> 00:38:16,000
But a big part of low self-esteem is conceit. It's the opposite of how we normally understand

202
00:38:16,000 --> 00:38:25,360
conceit, conceit being pride and a high self-esteem, but any kind of esteem of self, whether it's

203
00:38:25,360 --> 00:38:34,640
high or low, is the same mindset. It's based on the same delusions about self, the same

204
00:38:34,640 --> 00:38:49,360
clinging to or perversion of reality, into twisting reality, into a me and a mine.

205
00:38:49,360 --> 00:39:10,960
It's our way of looking at things that adds non-existent value to things. This is me, this is mine,

206
00:39:10,960 --> 00:39:19,680
this is mine. I'm better than someone. I'm worse than someone. Good, I'm bad.

207
00:39:23,920 --> 00:39:31,040
See, Buddhism in non-self is not really about whether there is or isn't a self or a soul or an ego.

208
00:39:31,040 --> 00:39:38,000
It's about our perspective. It's about having a perspective that's really based on reality without

209
00:39:38,000 --> 00:39:44,640
all this baggage of this is me and I am this and I'm better and worse and whatever.

210
00:39:46,640 --> 00:39:52,560
Self-esteem is just extra baggage, low self-esteem, high self-esteem.

211
00:39:55,280 --> 00:40:01,600
Because it's delusion, it's not exactly something you can note. It's rather something that goes away

212
00:40:01,600 --> 00:40:14,080
as you note because it is a delusion, it is an obscurity, it is a darkness and a blindness.

213
00:40:18,480 --> 00:40:24,800
Conceived can only exist in conjunction with ignorance and misunderstanding.

214
00:40:24,800 --> 00:40:31,280
I'll have skewed perception or perception that is nothing to do with reality.

215
00:40:32,000 --> 00:40:38,160
It's not based on reality that we gain conceit. There's no reality and the I am better,

216
00:40:38,160 --> 00:40:56,000
I am worse. These are just our own mental machinations.

217
00:40:56,000 --> 00:41:02,080
You don't necessarily have to be concerned with noting it. You can note things like knowing

218
00:41:02,080 --> 00:41:07,520
when you're aware that you are conceited or so on. Of course, not any reactions to it,

219
00:41:07,520 --> 00:41:14,160
but ultimately the conceit goes away as your clarity of mind comes. It's one of the

220
00:41:14,160 --> 00:41:20,560
later ones to disappear completely, so it wouldn't be too discouraged by it. It's one that's

221
00:41:20,560 --> 00:41:31,680
very deep and very challenging to overcome. But slowly but surely, as your clarity of mind improves,

222
00:41:31,680 --> 00:41:38,640
the conceit has no rest, it has no place to, no purchase in the mind.

223
00:41:49,760 --> 00:41:56,480
How do I note or choose what to note when the sensations get very fast? Sometimes I don't have the

224
00:41:56,480 --> 00:42:02,480
time to say the words in my mind. No, don't let them bully you. Don't let experiences bully you.

225
00:42:03,360 --> 00:42:09,840
They're not in charge, you are. Take your time, pick something. If there's lots of some things,

226
00:42:09,840 --> 00:42:19,120
just pick something. If they're really too quick to be mindful of something, then just note the

227
00:42:19,120 --> 00:42:25,920
quickness. If your mind is restless or distracted, not that. It's chaotic, not that. You can say

228
00:42:25,920 --> 00:42:33,920
chaos or overwhelmed, if you're overwhelmed. Knowing, if you're aware, just aware that it's so fast,

229
00:42:33,920 --> 00:42:40,320
just don't be of note the awareness, knowing, knowing. It's just a reaffirmation. Yep, it's like that.

230
00:42:42,160 --> 00:42:50,240
We reaffirm things with our noting, it's because we want to avoid any kind of judgment or reaction.

231
00:42:50,240 --> 00:42:57,920
Don't let your mind start to react. Like, oh, this is a problem. What am I going to do?

232
00:42:58,960 --> 00:43:02,800
No, it's not a problem. It's an experience. Just try and note the experience.

233
00:43:06,320 --> 00:43:11,840
Don't let them put on that reality bully you. That's where strength comes from. When you look

234
00:43:11,840 --> 00:43:21,840
it in the eye and you stare it down and you don't blink.

235
00:43:25,840 --> 00:43:31,200
How do we make the jump to avoiding the pitfalls that taper the benefits of meditation

236
00:43:31,200 --> 00:43:35,840
while we don't have the wisdom to make some of these jumps outside of the intellectual?

237
00:43:35,840 --> 00:43:46,080
Is there a leap of faith? I don't think there ever has to be a leap of faith. No,

238
00:43:46,880 --> 00:43:54,960
there has to be a bit of a suspending of judgment, but that's what anything you undertake.

239
00:43:55,920 --> 00:44:03,440
You have to try things when meditation is something new. So you have to take

240
00:44:03,440 --> 00:44:10,080
an inevitably and anything. There's that much of a leap of faith. It's not a leap of faith,

241
00:44:10,080 --> 00:44:20,800
it's a suspending of judgment. You work like a scientist, a scientist,

242
00:44:21,680 --> 00:44:26,560
reserves judgment, but it doesn't mean they turn off their judgment.

243
00:44:26,560 --> 00:44:33,040
They turn on instead their observational skills.

244
00:44:38,880 --> 00:44:48,800
So I think, and I say this a lot, that there's an over emphasis in people's minds and over

245
00:44:48,800 --> 00:44:55,120
interest, an excessive interest in results.

246
00:44:58,960 --> 00:45:09,920
So all this talk about the benefits of meditation is really dangerous. It's really unhelpful.

247
00:45:09,920 --> 00:45:17,680
It's really a part of the problem, our focus on benefits. What's it going to give me?

248
00:45:20,320 --> 00:45:24,800
It's a part of what you can't be focused on if you want it to actually give you something,

249
00:45:25,680 --> 00:45:29,520
kind of ironic, kind of not kind of tricky paradoxical,

250
00:45:31,280 --> 00:45:32,480
makes things very tricky.

251
00:45:32,480 --> 00:45:39,360
But it's a part that that is an important part of the paradigm shift,

252
00:45:40,880 --> 00:45:43,360
important part of the change in the way we look at things,

253
00:45:44,960 --> 00:45:50,240
meditation is about changing the way I look at the we look at things, part of that change has to be

254
00:45:50,240 --> 00:45:57,200
our estimation of things like meditation or anything like meditation.

255
00:45:57,200 --> 00:46:02,960
We have to change from esteeming things, judging them based on

256
00:46:05,440 --> 00:46:10,080
their results were based on

257
00:46:10,080 --> 00:46:26,000
the results that we perceive as we practice them. Meaning, of course, meditation,

258
00:46:27,680 --> 00:46:34,560
the claim is that it does bring positive change, but there's no mystery behind that or no

259
00:46:34,560 --> 00:46:44,000
uncertainty or certainly no faith required to appreciate that. Because as the paradigm shift occurs,

260
00:46:44,000 --> 00:46:50,240
as you start to see things differently, see things just as experiences.

261
00:46:52,080 --> 00:46:54,560
It's not even so much seeing them differently. It's just seeing

262
00:46:55,760 --> 00:47:00,800
often for the first time, like just focusing on your stomach, but you never

263
00:47:00,800 --> 00:47:06,560
knew so much about what it's like for the stomach to rise and what it's like for the stomach to fall

264
00:47:06,560 --> 00:47:12,000
and the chaos and the variation and the uncertainty involved in that simple movement.

265
00:47:14,160 --> 00:47:18,800
It's very unlikely that we had such knowledge, so it's not about seeing things differently,

266
00:47:18,800 --> 00:47:30,800
per se, it's about seeing things more clearly. And that actual shift of clarity

267
00:47:34,480 --> 00:47:41,760
is when it brings about the benefit. It brings about the benefit simply because of what it is

268
00:47:41,760 --> 00:47:51,440
to see clearly. Seeing clearly is different from reacting and judging. So there's never a leap

269
00:47:51,440 --> 00:48:05,360
of faith. There's not any need or any need to doubt. There's not any room to doubt

270
00:48:05,360 --> 00:48:16,880
to the benefits. Because you're cultivating and you're experiencing the clarity and

271
00:48:19,520 --> 00:48:27,760
a purity of mind, which is really the prime benefit in itself. So the best way to approach

272
00:48:27,760 --> 00:48:32,320
the practice is not in terms of what's going to give me, but in terms of what's it like.

273
00:48:32,320 --> 00:48:42,000
When you practice in this way, what's it like, and you should hopefully see that it's quite pure,

274
00:48:43,120 --> 00:48:46,800
it's quite clear in mind, quite objective in mind.

275
00:48:49,280 --> 00:48:54,240
And when you know that it's like that, you shouldn't have any reason to waver or doubt about

276
00:48:54,240 --> 00:49:04,640
what might come from that. It's unreasonable to suggest that clarity and purity of mind would

277
00:49:04,640 --> 00:49:10,880
ever lean to anything harmful or anything bad or anything but goodness and happiness.

278
00:49:14,880 --> 00:49:16,480
You don't really need faith for that.

279
00:49:16,480 --> 00:49:29,680
I'm starting to note in between the segments of walking and sitting meditation. For example,

280
00:49:29,680 --> 00:49:36,400
rising, hearing, falling, hearing. Is this correct? Or am I not staying with the sensations?

281
00:49:37,680 --> 00:49:42,880
I would try during the formal practice. I would try not to do that. Try to note the hearing

282
00:49:42,880 --> 00:49:47,760
until it goes away and if after a long time it doesn't go away, then go back and start with

283
00:49:47,760 --> 00:49:55,600
the rising each time. Just a little more focused that way.

284
00:50:00,080 --> 00:50:04,560
When returning to the stomach, should we note waiting while waiting for the rising?

285
00:50:04,560 --> 00:50:18,480
Yeah, that's a good note. It can also just not sitting. Many times the path seems to take away

286
00:50:18,480 --> 00:50:24,640
from pleasurable experiences that one equates with happiness. I find myself having resentment,

287
00:50:24,640 --> 00:50:35,680
doubts. Should I take them as doubt and anger or something else? That's what they are, doubt and anger.

288
00:50:35,680 --> 00:50:40,480
That's what we're trying to see. The problem isn't the pleasure. The problem is that when we don't

289
00:50:40,480 --> 00:50:52,560
get our pleasure, we're whiny little wretches. We are not satisfied. We're not at peace.

290
00:50:52,560 --> 00:51:00,080
We want to see that. It's simulating this. We're simulating this by preventing ourselves from

291
00:51:01,200 --> 00:51:06,400
seeking out pleasure. When you force yourself to sit still or walk back and forth, you're

292
00:51:08,720 --> 00:51:16,400
testing yourself. You'll see the reactions in the mind. It's very useful to see those

293
00:51:16,400 --> 00:51:24,160
because they're not going to, they're not a foreign element in our lives. They're a part of how

294
00:51:24,960 --> 00:51:31,920
we live our lives with resentment and doubt and confusion and judgment.

295
00:51:31,920 --> 00:51:47,600
Sometimes I feel I'm missing out on potential benefits since I very rarely note the rising

296
00:51:47,600 --> 00:51:53,360
and falling with continuity. I almost feel like I'm noting the stomach less as I continue.

297
00:51:53,360 --> 00:51:59,120
Do you have any advice? Well, try and always come back to this stomach. There's nothing special

298
00:51:59,120 --> 00:52:04,960
about it, but it is a good base object. So try and note something and when that something's gone,

299
00:52:06,160 --> 00:52:11,920
possible try and come back to the stomach. It takes practice and at certain stages you're going to be

300
00:52:11,920 --> 00:52:16,320
very distracted. So just be patient with that and note it as best you can.

301
00:52:18,240 --> 00:52:24,880
The big part of our practice is catching things that we're forgetting to note. We're finding our

302
00:52:24,880 --> 00:52:30,800
practice, fine tuning it. So try and be discerning there in terms of what you might be missing,

303
00:52:32,560 --> 00:52:36,720
catching though when something is experienced and you didn't note it.

304
00:52:38,320 --> 00:52:44,080
First book in our book, the first chapter in our booklet is probably the most important for this

305
00:52:44,080 --> 00:52:57,040
reason because it outlines various sorts of things you can be mindful of. Try not to forget any of them.

306
00:52:57,040 --> 00:53:02,640
When I manage to detach myself from worldly things, in a short time this attachment returns with

307
00:53:02,640 --> 00:53:16,880
much more intensity as if they were avenging. What is this? How to prevent it?

308
00:53:18,880 --> 00:53:26,720
Part of it is our letting our guard down. So you become, through meditation practice, you become

309
00:53:26,720 --> 00:53:38,560
more accepting, ordinarily we're wary and we are reactionary and so it's

310
00:53:38,560 --> 00:53:43,920
remindfulness. You become more relaxed, less tense, less stressed, less reactionary.

311
00:53:46,560 --> 00:53:51,760
And so our ordinary emotions were not prepared for them. Ordinarily we're prepared to fight them

312
00:53:51,760 --> 00:53:59,680
or to engage in them or so on. When you're not ready to do that, you find yourself caught off

313
00:53:59,680 --> 00:54:09,600
guard. It's not a bad thing, it's just a challenge. So don't be discouraged by that. It's just

314
00:54:10,880 --> 00:54:19,120
your new situation how you've situated yourself. And if you can stand and bear the

315
00:54:19,120 --> 00:54:26,560
onslaught of the emotions and the attachments, you'll find great benefit from it because there's

316
00:54:26,560 --> 00:54:35,360
nothing behind the intensity. There's no meaning behind it as though this means something,

317
00:54:35,360 --> 00:54:42,560
this means that I should follow it or this means that I'm hopeless. There's nothing hopeless

318
00:54:42,560 --> 00:54:50,880
about attachment. It's never hopeless. It's just attachment. It's just something to

319
00:54:52,720 --> 00:54:57,440
something you can take as an object, something to see clearly, something to understand.

320
00:54:59,600 --> 00:55:07,280
It's not about preventing it. It's about dealing with it, living through it without

321
00:55:07,280 --> 00:55:12,400
clinging, seeing it clearly, understanding it.

322
00:55:20,000 --> 00:55:25,360
I used to get so calm in meditation that I could see all thoughts coming before they come.

323
00:55:25,360 --> 00:55:39,200
I have lost that ability and my mind is constantly so noisy now. Any advice?

324
00:55:39,200 --> 00:55:53,360
I'll be patient with that. Being calm can often be a habit or a result of habits of mind that

325
00:55:55,520 --> 00:56:02,480
corral the mind into a calm state. Mindfulness doesn't really do that. It opens the flood gates

326
00:56:02,480 --> 00:56:10,240
in many ways to chaos and disturbance. Mindfulness can actually instigate that because you're

327
00:56:10,240 --> 00:56:14,240
no longer working forcefully to corral the mind into a calm state.

328
00:56:21,760 --> 00:56:26,160
The calmness isn't really the ability. It's not an important ability

329
00:56:26,160 --> 00:56:33,680
if in mindfulness practice, a much better and more useful ability is the ability to be mindful of

330
00:56:33,680 --> 00:56:40,720
chaos because the chaos has caused us. There are reasons why the mind is chaotic and in turmoil

331
00:56:41,440 --> 00:56:47,920
are not mindfulness, but they're there. Mindfulness is in fact doing a lot of reordering

332
00:56:47,920 --> 00:56:56,880
because wisdom reorders, wisdom evaluates and filters. Wisdom adjusts the mind

333
00:56:58,560 --> 00:57:01,920
by itself without any further action.

334
00:57:05,280 --> 00:57:11,760
So our observation of the chaos is an important part of the practice as the mind is,

335
00:57:11,760 --> 00:57:19,440
as the wisdom is allowed to refine the state of mind, refine the quality of the mind,

336
00:57:19,440 --> 00:57:20,720
and the habits of the mind.

337
00:57:24,960 --> 00:57:33,040
Noisy isn't a problem. You're disliking of the noise, that's the problem.

338
00:57:33,040 --> 00:57:43,600
How do I become mindful of emotion of worry? If it is intertwined with thoughts,

339
00:57:43,600 --> 00:57:48,000
and I have trouble separating one from the other.

340
00:57:48,000 --> 00:57:56,080
Meditation is a challenge. You don't have to exactly separate them. You just have to

341
00:57:56,080 --> 00:58:04,080
recognize them and note one of them, note whatever is clearest. It will get easier to discern

342
00:58:04,080 --> 00:58:15,760
as you go along. That brings our Tier 1 questions to an end, Dante, and we've reached the hour.

343
00:58:17,040 --> 00:58:25,440
Wonderful. Good timing. Well, good questions. Thank you all. Thank you for your help.

344
00:58:25,440 --> 00:58:32,240
Sahandu, thank you all for joining. I have a meditator waiting for me here at our center,

345
00:58:32,240 --> 00:58:36,240
so I'm going to head off. I wish you all a good week.

346
00:58:36,240 --> 00:58:54,000
May the benefits of this session bring happiness and peace and freedom to all of us. Sahandu, Sahandu.

