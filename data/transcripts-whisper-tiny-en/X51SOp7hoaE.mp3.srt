1
00:00:00,000 --> 00:00:05,000
What do you think about the hippie culture?

2
00:00:05,000 --> 00:00:07,000
Last question of the night.

3
00:00:07,000 --> 00:00:08,000
It's a bizarre one.

4
00:00:08,000 --> 00:00:10,000
I don't really have an answer, no?

5
00:00:10,000 --> 00:00:13,000
Right, the hippie culture.

6
00:00:13,000 --> 00:00:16,000
The hippie culture, not that I was a part of it,

7
00:00:16,000 --> 00:00:18,000
but I did read a lot about it,

8
00:00:18,000 --> 00:00:21,000
and I've got pictures to attest to the fact that I

9
00:00:21,000 --> 00:00:24,000
was a wannabe hippie with bell bottoms

10
00:00:24,000 --> 00:00:29,000
and long hair and drugs, soft drugs anyway.

11
00:00:29,000 --> 00:00:31,000
And free love.

12
00:00:31,000 --> 00:00:33,000
I remember going to these folk festivals

13
00:00:33,000 --> 00:00:35,000
with my parents who, you know, my father

14
00:00:35,000 --> 00:00:38,000
were both hippies, you know.

15
00:00:38,000 --> 00:00:40,000
We got to these folk festivals,

16
00:00:40,000 --> 00:00:44,000
and eventually I managed to hook up with a whole bunch of women.

17
00:00:44,000 --> 00:00:46,000
And I think some of them liked me,

18
00:00:46,000 --> 00:00:49,000
and most of them were just kind of putting up with me,

19
00:00:49,000 --> 00:00:52,000
but we'd like to walk around kind of days

20
00:00:52,000 --> 00:00:55,000
with our arms around each other,

21
00:00:55,000 --> 00:01:00,000
and everyone, my parents got really scared

22
00:01:00,000 --> 00:01:02,000
because they thought we were having,

23
00:01:02,000 --> 00:01:05,000
they thought no, I was going to get someone pregnant

24
00:01:05,000 --> 00:01:09,000
or something my father came up to me with a bunch of condoms anyway.

25
00:01:09,000 --> 00:01:11,000
This is maybe too much information.

26
00:01:11,000 --> 00:01:15,000
Point, far too much information, sorry about it.

27
00:01:15,000 --> 00:01:19,000
The point is I do have something to say

28
00:01:19,000 --> 00:01:21,000
about the hippie culture,

29
00:01:21,000 --> 00:01:30,000
because I think I do have some ownership

30
00:01:30,000 --> 00:01:32,000
of the term from my past.

31
00:01:32,000 --> 00:01:35,000
Even though I wasn't during the 60s,

32
00:01:35,000 --> 00:01:37,000
I was about as hippie as you could get

33
00:01:37,000 --> 00:01:40,000
in the place and time that I grew up.

34
00:01:47,000 --> 00:01:48,000
So two things.

35
00:01:48,000 --> 00:01:51,000
First of all, the love aspect is great.

36
00:01:51,000 --> 00:01:57,000
The free love, the peace,

37
00:01:57,000 --> 00:02:02,000
the whole John Lennon stuff,

38
00:02:02,000 --> 00:02:05,000
things that John Lennon stood for

39
00:02:05,000 --> 00:02:09,000
after he left the Beatles.

40
00:02:09,000 --> 00:02:12,000
Because he was really one of the epitomies

41
00:02:12,000 --> 00:02:15,000
of the hippie culture, I think.

42
00:02:15,000 --> 00:02:18,000
Yeah, I don't know.

43
00:02:18,000 --> 00:02:22,000
All you need is love,

44
00:02:22,000 --> 00:02:25,000
but his later stuff like,

45
00:02:25,000 --> 00:02:27,000
all we are saying is give peace a chance,

46
00:02:27,000 --> 00:02:30,000
that kind of stuff.

47
00:02:30,000 --> 00:02:33,000
So some of these concepts are really good

48
00:02:33,000 --> 00:02:35,000
and really beneficial.

49
00:02:35,000 --> 00:02:37,000
Now the problem is,

50
00:02:37,000 --> 00:02:40,000
love is not all you need.

51
00:02:40,000 --> 00:02:42,000
It's not enough.

52
00:02:42,000 --> 00:02:46,000
And when you still have lots of defilements,

53
00:02:46,000 --> 00:02:50,000
it doesn't, the reality is not like the concepts.

54
00:02:50,000 --> 00:02:54,000
Now, you might say that they stole some of that from Hinduism

55
00:02:54,000 --> 00:02:57,000
because there was a lot of,

56
00:02:57,000 --> 00:03:02,000
the influx of Hinduism, Zen Buddhism also was big.

57
00:03:02,000 --> 00:03:05,000
And those times, which would have brought Buddhist principles

58
00:03:05,000 --> 00:03:10,000
of peace and enlightenment,

59
00:03:10,000 --> 00:03:17,000
letting go, giving up freedom, for example.

60
00:03:17,000 --> 00:03:20,000
And so what you find found in the hippie culture

61
00:03:20,000 --> 00:03:28,000
was a lot of angry, selfish, indulgent people

62
00:03:28,000 --> 00:03:35,000
who really didn't have the peace, happiness and freedom

63
00:03:35,000 --> 00:03:38,000
that they were talking about.

64
00:03:38,000 --> 00:03:41,000
John Lennon's a good example.

65
00:03:41,000 --> 00:03:46,000
He was in many times he was a violent individual.

66
00:03:46,000 --> 00:03:48,000
He was not a good example of,

67
00:03:48,000 --> 00:03:50,000
and this is what, you know, there's a quote.

68
00:03:50,000 --> 00:03:55,000
He said, you know, I'm so full of anger.

69
00:03:55,000 --> 00:03:57,000
That's why I'm on about peace all the time

70
00:03:57,000 --> 00:04:02,000
because of how much anger I have inside.

71
00:04:02,000 --> 00:04:04,000
So he actually tried to do meditation.

72
00:04:04,000 --> 00:04:07,000
They did a lot of drugs in India and meditation.

73
00:04:07,000 --> 00:04:19,000
And so it's not adequate.

74
00:04:19,000 --> 00:04:23,000
There was no core to the hippie culture,

75
00:04:23,000 --> 00:04:25,000
to the hippie movement.

76
00:04:25,000 --> 00:04:28,000
Even though they thought they had good principles,

77
00:04:28,000 --> 00:04:30,000
you know, core principles,

78
00:04:30,000 --> 00:04:35,000
there was not enough enlightenment in it.

79
00:04:35,000 --> 00:04:40,000
So the minds of the people were not pure.

80
00:04:40,000 --> 00:04:44,000
And it just became a feel-good hedonistic movement,

81
00:04:44,000 --> 00:04:47,000
which is how we took it, how I was.

82
00:04:47,000 --> 00:04:52,000
Obviously very hedonistic.

83
00:04:52,000 --> 00:04:56,000
And so as a result, many of the people who were exhippies

84
00:04:56,000 --> 00:04:58,000
ended up becoming corporate

85
00:04:58,000 --> 00:05:04,000
and buying into society because it was just a fad.

86
00:05:04,000 --> 00:05:10,000
The hardcore hippies became the hippies, right?

87
00:05:10,000 --> 00:05:13,000
Abby Hoffman generation.

88
00:05:13,000 --> 00:05:15,000
Angry, Abby Hoffman who said,

89
00:05:15,000 --> 00:05:18,000
it's time for the flower children to grow thorns.

90
00:05:18,000 --> 00:05:21,000
And so they became violent and they tried to secede

91
00:05:21,000 --> 00:05:25,000
from, they actually did secede from the United States of America.

92
00:05:25,000 --> 00:05:28,000
It's quite an interesting story.

93
00:05:28,000 --> 00:05:32,000
And failed miserably because of the anger and the violence

94
00:05:32,000 --> 00:05:37,000
and the suffering.

95
00:05:37,000 --> 00:05:46,000
So it was an influx of many good qualities.

96
00:05:46,000 --> 00:05:54,000
But it's also a good example of how

97
00:05:54,000 --> 00:05:57,000
even positive qualities aren't enough.

98
00:05:57,000 --> 00:06:00,000
Even positive qualities have to be let go of.

99
00:06:00,000 --> 00:06:03,000
If you cling to positive qualities,

100
00:06:03,000 --> 00:06:05,000
you'll just end up becoming as angry as people

101
00:06:05,000 --> 00:06:08,000
who cling to negative qualities.

102
00:06:08,000 --> 00:06:12,000
So this idea of peace put them at war

103
00:06:12,000 --> 00:06:17,000
with the rest of the world or with the rest of America

104
00:06:17,000 --> 00:06:22,000
or with government, for example.

105
00:06:22,000 --> 00:06:26,000
People who hold peace rallies, right?

106
00:06:26,000 --> 00:06:31,000
Anti-war activists and so on.

107
00:06:31,000 --> 00:06:34,000
Which I think is a lesson that was learned.

108
00:06:34,000 --> 00:06:38,000
And so when you see these occupy Wall Street demonstrations,

109
00:06:38,000 --> 00:06:41,000
you actually feel a little bit more confident about them

110
00:06:41,000 --> 00:06:44,000
because they seem to have learned something.

111
00:06:44,000 --> 00:06:47,000
And they're much more Buddhist, which is really cool.

112
00:06:47,000 --> 00:06:52,000
You see them all sitting in meditation in the park, for example.

113
00:06:52,000 --> 00:06:57,000
As you know, they did in the 60s, but I would say there's probably less...

114
00:06:57,000 --> 00:06:58,000
I don't know.

115
00:06:58,000 --> 00:07:00,000
I mean, let's hope this is one of the lessons.

116
00:07:00,000 --> 00:07:02,000
But this is one of the lessons we should learn.

117
00:07:02,000 --> 00:07:07,000
And if people out there are involved in things like occupy Wall Street or something,

118
00:07:07,000 --> 00:07:09,000
it's really a bad premise.

119
00:07:09,000 --> 00:07:14,000
And it's not really Buddhist to occupy anything or to strive

120
00:07:14,000 --> 00:07:20,000
to force anything or to even get caught up and buy into this process at all.

121
00:07:20,000 --> 00:07:23,000
Even concern ourselves with it.

122
00:07:23,000 --> 00:07:32,000
But for people living in society, the lesson we can learn is to internalize it.

123
00:07:32,000 --> 00:07:39,000
To actually be the peace that we're railing on about.

124
00:07:39,000 --> 00:07:44,000
So be the change that you wish to see in the world.

125
00:07:44,000 --> 00:07:58,000
And if there had been more of an emphasis on purity and less of an emphasis on love, I suppose.

126
00:07:58,000 --> 00:08:00,000
Then people would...

127
00:08:00,000 --> 00:08:02,000
It's like getting your priorities wrong.

128
00:08:02,000 --> 00:08:09,000
The whole emphasis on the qualities of mind of an enlightened being

129
00:08:09,000 --> 00:08:11,000
as opposed to the emphasis on becoming enlightened.

130
00:08:11,000 --> 00:08:16,000
Because only enlightened being has pure love, pure compassion.

131
00:08:16,000 --> 00:08:20,000
For the rest of us, it's tainted and trying to develop love.

132
00:08:20,000 --> 00:08:28,000
This is why we don't encourage for most people to cultivate loving kindness meditation

133
00:08:28,000 --> 00:08:35,000
as an intensive practice.

134
00:08:35,000 --> 00:08:38,000
Because if your mind is not pure, it becomes corrupted.

135
00:08:38,000 --> 00:08:43,000
If you start sending love out to people of the opposite gender or people who you're attracted to,

136
00:08:43,000 --> 00:08:45,000
then this is what it becomes.

137
00:08:45,000 --> 00:08:46,000
This is how gurus arise.

138
00:08:46,000 --> 00:08:49,000
They teach love and then they wind up sleeping with their students.

139
00:08:49,000 --> 00:08:54,000
This was the Zen Center of San Francisco, San Francisco Zen Center.

140
00:08:54,000 --> 00:08:56,000
Very famous place.

141
00:08:56,000 --> 00:09:00,000
And then they found out that this guy was having affairs with all of his students.

142
00:09:00,000 --> 00:09:03,000
And he was an alcoholic besides.

143
00:09:03,000 --> 00:09:08,000
So that's what I would just say about the hippie generation.

144
00:09:08,000 --> 00:09:11,000
It's not nothing profound, I suppose.

145
00:09:11,000 --> 00:09:15,000
But nothing that people probably don't think themselves as Buddhists.

146
00:09:15,000 --> 00:09:21,000
But I would say it's because of the lack of wisdom,

147
00:09:21,000 --> 00:09:25,000
the lack of understanding.

148
00:09:25,000 --> 00:09:33,000
It was kind of a, it seems like it's kind of everyone on board kind of thing.

149
00:09:33,000 --> 00:09:39,000
Get everyone to get involved with the movement and to just be happy.

150
00:09:39,000 --> 00:09:45,000
As opposed to requiring, being disciplined,

151
00:09:45,000 --> 00:09:55,000
and requiring the purity of land.

152
00:09:55,000 --> 00:10:00,000
So it ended up getting derailed if it was ever on rails.

153
00:10:00,000 --> 00:10:21,000
I don't know.

