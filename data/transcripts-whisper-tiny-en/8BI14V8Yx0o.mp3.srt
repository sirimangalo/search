1
00:00:00,000 --> 00:00:09,280
One will come to our live broadcast.

2
00:00:09,280 --> 00:00:14,960
Funny thing is when I say that there's not actually anyone watching, it does let you

3
00:00:14,960 --> 00:00:18,000
to know that this was a live broadcast.

4
00:00:18,000 --> 00:00:26,000
Or maybe people are watching as soon as they start broadcasting.

5
00:00:26,000 --> 00:00:35,000
I've never actually watched one of these live, obviously.

6
00:00:35,000 --> 00:00:44,000
So today we are one, two, chapter eight.

7
00:00:44,000 --> 00:00:50,000
And we are going to number 76.

8
00:00:50,000 --> 00:00:55,000
And these are really interesting, I like these ones.

9
00:00:55,000 --> 00:00:59,000
Apamatica.

10
00:00:59,000 --> 00:01:00,000
Apamatica.

11
00:01:00,000 --> 00:01:02,000
And these are insignificant.

12
00:01:02,000 --> 00:01:05,000
Apamatica is a big way perihani.

13
00:01:05,000 --> 00:01:10,000
For now, these are these.

14
00:01:10,000 --> 00:01:12,000
Perihani.

15
00:01:12,000 --> 00:01:17,000
No, this.

16
00:01:17,000 --> 00:01:20,000
Perihani means loss.

17
00:01:20,000 --> 00:01:28,000
Apamatica means a little, a little importance, a little measure.

18
00:01:28,000 --> 00:01:35,000
Literally means a little measure for little significance.

19
00:01:35,000 --> 00:01:41,000
A small matter insignificant.

20
00:01:41,000 --> 00:01:45,000
This loss is insignificant, monks.

21
00:01:45,000 --> 00:01:55,000
And is to say the loss of relatives.

22
00:01:55,000 --> 00:02:04,000
Apamatica.

23
00:02:04,000 --> 00:02:18,000
Apamatica.

24
00:02:18,000 --> 00:02:23,000
Apamatica.

25
00:02:23,000 --> 00:02:44,000
I don't know what this word means.

26
00:02:44,000 --> 00:02:53,000
No, anyway, I'm assuming it means the opposite of Apamatica.

27
00:02:53,000 --> 00:03:00,000
He says the worst.

28
00:03:00,000 --> 00:03:01,000
The worst.

29
00:03:01,000 --> 00:03:02,000
That is the worst.

30
00:03:02,000 --> 00:03:12,000
The worst of losses is the loss of wisdom.

31
00:03:12,000 --> 00:03:22,000
Leave it to the Buddha to just mess with our sense of priority.

32
00:03:22,000 --> 00:03:25,000
The tell us.

33
00:03:25,000 --> 00:03:35,000
Leave it to the Buddha to be the one to tell all those grieving windows and orphans.

34
00:03:35,000 --> 00:03:44,000
Well, the bereaved to tell them their loss is insignificant.

35
00:03:44,000 --> 00:03:51,000
Buddhism works on really on a whole other level.

36
00:03:51,000 --> 00:04:00,000
Apart from mundane concerns, it's the most hard core of religion.

37
00:04:00,000 --> 00:04:14,000
The shame that it's been watered down and a lot of people, a lot of Buddhists focus on a sort of soft core version of Buddhism.

38
00:04:14,000 --> 00:04:31,000
That focused on worldly things and social welfare therapy, stress relief.

39
00:04:31,000 --> 00:04:38,000
Buddhism deals on a whole other level beyond worldly concerns.

40
00:04:38,000 --> 00:04:45,000
We think about the Buddha's story himself, what he went through, the losses that he went through.

41
00:04:45,000 --> 00:04:52,000
That's really the point is this is so much bigger, so much more profound and just our ordinary concern.

42
00:04:52,000 --> 00:04:56,000
We worry about the most ridiculous of things.

43
00:04:56,000 --> 00:05:12,000
Many, we recognize this ridiculous. We worry about a pimple, we worry about our weight, we worry about what other people think of us.

44
00:05:12,000 --> 00:05:20,000
We worry about our grains, we worry about our employment, we worry about money.

45
00:05:20,000 --> 00:05:29,000
We worry about things and we put an increasing level of value on these things.

46
00:05:29,000 --> 00:05:32,000
The pimple might be most ridiculous.

47
00:05:32,000 --> 00:05:34,000
It's not really worth worrying about.

48
00:05:34,000 --> 00:05:36,000
What other people think about me?

49
00:05:36,000 --> 00:05:38,000
Well, that's a little bit more important.

50
00:05:38,000 --> 00:05:42,000
Then my study and my work and money and these things.

51
00:05:42,000 --> 00:05:54,000
And way up here is our relatives, our own health, our own life, our life of other people, our relationships.

52
00:05:54,000 --> 00:06:01,000
Things that we believe have some sort of meaning, importance, worth worrying about.

53
00:06:01,000 --> 00:06:06,000
And that the loss is somehow significant, it's really not.

54
00:06:06,000 --> 00:06:13,000
This is the Buddha's teaching here. These things are still a very insignificant.

55
00:06:13,000 --> 00:06:24,000
They're completely insignificant. There's no meaning. There's no value to that loss.

56
00:06:24,000 --> 00:06:27,000
Or if there is a value, it's far, far.

57
00:06:27,000 --> 00:06:33,000
It's not that there isn't a value. It's just compared to things that are actually important.

58
00:06:33,000 --> 00:06:35,000
It's insignificant.

59
00:06:35,000 --> 00:06:38,000
I had, I mentioned this before, I think.

60
00:06:38,000 --> 00:06:46,000
But when I was taking Buddhism in university, one of my friends was a Catholic.

61
00:06:46,000 --> 00:06:50,000
Came up to me after a class once and just put it in.

62
00:06:50,000 --> 00:06:55,000
It's such a relief.

63
00:06:55,000 --> 00:06:59,000
The idea that you could keep coming back again and again.

64
00:06:59,000 --> 00:07:02,000
You don't just have one chance.

65
00:07:02,000 --> 00:07:08,000
That's Buddhism. And it's true. It really is a mind opener.

66
00:07:08,000 --> 00:07:13,000
It's a relief from all the worries and stresses, all the things we have.

67
00:07:13,000 --> 00:07:18,000
I mean, imagine being Catholic and having to worry about going to hell for eternity.

68
00:07:18,000 --> 00:07:21,000
The things we worry about.

69
00:07:21,000 --> 00:07:28,000
But as it really blows the roof off of all that.

70
00:07:28,000 --> 00:07:32,000
Because in the face of eternity, what does it all mean?

71
00:07:32,000 --> 00:07:37,000
Even hell isn't terribly significant.

72
00:07:37,000 --> 00:07:41,000
Because it comes and it goes.

73
00:07:41,000 --> 00:07:49,000
You think of eternity when you think of time erasing all things.

74
00:07:49,000 --> 00:07:52,000
It's not just that in 100 years we're all going to be dead.

75
00:07:52,000 --> 00:07:55,000
That's for sure.

76
00:07:55,000 --> 00:07:58,000
But in the thousand years no one will remember us.

77
00:07:58,000 --> 00:08:03,000
In 100,000 years there will be no trace of us.

78
00:08:03,000 --> 00:08:10,000
In a million years there will be no one.

79
00:08:10,000 --> 00:08:14,000
The universe will be nothing like it is now.

80
00:08:14,000 --> 00:08:17,000
And all of that is coming. All of that is real.

81
00:08:17,000 --> 00:08:20,000
All of that is in the near future.

82
00:08:20,000 --> 00:08:27,000
A million years goes by in the flash.

83
00:08:27,000 --> 00:08:31,000
And then it comes again and again.

84
00:08:31,000 --> 00:08:36,000
And it's not just a long time.

85
00:08:36,000 --> 00:08:39,000
Eternity is not just a long time.

86
00:08:39,000 --> 00:08:45,000
But it means that there can be no meaning to these things.

87
00:08:45,000 --> 00:08:50,000
To some event.

88
00:08:50,000 --> 00:08:56,000
And these things that we cry over that we mourn.

89
00:08:56,000 --> 00:09:01,000
That we worry and fret about.

90
00:09:01,000 --> 00:09:05,000
Consider to be important.

91
00:09:05,000 --> 00:09:09,000
There's only one category of things that can be important.

92
00:09:09,000 --> 00:09:15,000
Those are the things that we carry with qualities.

93
00:09:15,000 --> 00:09:18,000
Wisdom.

94
00:09:18,000 --> 00:09:25,000
It's a difference between worrying about the ocean.

95
00:09:25,000 --> 00:09:30,000
And how you can across the ocean and worrying about the boat.

96
00:09:30,000 --> 00:09:35,000
Our lives are like an ocean. The world, the universe.

97
00:09:35,000 --> 00:09:39,000
Our experience of some sorrow is like an ocean.

98
00:09:39,000 --> 00:09:48,000
And we worry about the weather, we worry about the direction.

99
00:09:48,000 --> 00:09:53,000
And much more important is the boat, the ship, the vehicle.

100
00:09:53,000 --> 00:09:57,000
Wisdom is the vehicle.

101
00:09:57,000 --> 00:10:00,000
We worry too much about the circumstances in our life.

102
00:10:00,000 --> 00:10:01,000
Oh, am I going to get this?

103
00:10:01,000 --> 00:10:03,000
Am I going to get that?

104
00:10:03,000 --> 00:10:11,000
If you get the things you want and you're not ready to accept and to own them.

105
00:10:11,000 --> 00:10:14,000
To deal with them.

106
00:10:14,000 --> 00:10:16,000
People wish for promotion.

107
00:10:16,000 --> 00:10:17,000
What are the questions?

108
00:10:17,000 --> 00:10:18,000
Are you ready for a promotion?

109
00:10:18,000 --> 00:10:19,000
They wish for a relationship.

110
00:10:19,000 --> 00:10:24,000
Are you able to?

111
00:10:24,000 --> 00:10:28,000
How are you going to deal with the things that you get?

112
00:10:28,000 --> 00:10:32,000
How are you going to live with them?

113
00:10:32,000 --> 00:10:36,000
But always getting what you want is not the answer.

114
00:10:36,000 --> 00:10:42,000
If you're spoiled and the things that you get spoil you rotten,

115
00:10:42,000 --> 00:10:45,000
you end up being a curse.

116
00:10:45,000 --> 00:10:48,000
The only thing that's really important is how you respond,

117
00:10:48,000 --> 00:10:53,000
how you react, how you interact with them is the most important.

118
00:10:53,000 --> 00:11:00,000
Because it determines how you're going to interact with the things that you get.

119
00:11:00,000 --> 00:11:03,000
Loss of relatives.

120
00:11:03,000 --> 00:11:04,000
It's meaningless.

121
00:11:04,000 --> 00:11:05,000
Loss of wisdom.

122
00:11:05,000 --> 00:11:07,000
The person who has no wisdom.

123
00:11:07,000 --> 00:11:10,000
I'm not quite sure what is meant by loss of wisdom.

124
00:11:10,000 --> 00:11:13,000
The wisdom is not an easy thing to lose once you get it.

125
00:11:13,000 --> 00:11:14,000
It is possible.

126
00:11:14,000 --> 00:11:16,000
Monday wisdom.

127
00:11:16,000 --> 00:11:25,000
But still, it's one of those things that's so valuable because you can't lose it.

128
00:11:25,000 --> 00:11:28,000
It's hard to lose anyway.

129
00:11:28,000 --> 00:11:31,000
It's not like concentration.

130
00:11:31,000 --> 00:11:35,000
If you fix and focus your mind so that you never get angry and never get greedy,

131
00:11:35,000 --> 00:11:37,000
you can do it.

132
00:11:37,000 --> 00:11:42,000
But it's not as powerful as wisdom because it comes in a ghost, but wisdom.

133
00:11:42,000 --> 00:11:44,000
It's very deep.

134
00:11:44,000 --> 00:11:46,000
And super Monday wisdom.

135
00:11:46,000 --> 00:11:51,000
The wisdom of a soda pan even.

136
00:11:51,000 --> 00:11:56,000
There's no going back from that.

137
00:11:56,000 --> 00:12:00,000
But I think the point is the difference between wisdom and relatives.

138
00:12:00,000 --> 00:12:03,000
Don't worry so much about people.

139
00:12:03,000 --> 00:12:05,000
Write so much about even people.

140
00:12:05,000 --> 00:12:11,000
It's a shock because there are many more things that could have compared it to.

141
00:12:11,000 --> 00:12:16,000
They say even relatives, which is one thing that we mourn the most.

142
00:12:16,000 --> 00:12:19,000
Just the one thing in this life that we mourn the most.

143
00:12:19,000 --> 00:12:26,000
If your father, your mother, your child, your sister, your brother, someone you love,

144
00:12:26,000 --> 00:12:30,000
one of your relatives dies.

145
00:12:30,000 --> 00:12:32,000
It's one of the worst, right?

146
00:12:32,000 --> 00:12:40,000
And Monday says insignificant.

147
00:12:40,000 --> 00:12:45,000
And then he says the same about wealth and the same about fame.

148
00:12:45,000 --> 00:12:50,000
In significant is the loss of wealth insignificant is the loss of fame.

149
00:12:50,000 --> 00:12:52,000
The worst thing to lose is wisdom.

150
00:12:52,000 --> 00:12:54,000
There's a quote.

151
00:12:54,000 --> 00:12:56,000
Here's a Buddha quote.

152
00:12:56,000 --> 00:13:01,000
And I want to look at this concept of losing wisdom.

153
00:13:01,000 --> 00:13:06,000
What exactly does he mean by?

154
00:13:06,000 --> 00:13:33,000
I mean, you can follow away from just the point that you follow away from wisdom.

155
00:13:33,000 --> 00:13:37,000
You become lost.

156
00:13:37,000 --> 00:13:42,000
I bet the commentary does say, but I don't know if I have time to do it.

157
00:13:42,000 --> 00:13:51,000
I know it.

158
00:13:51,000 --> 00:13:54,000
The absence of wisdom is worst.

159
00:13:54,000 --> 00:13:58,000
Wisdom is most important is the point here.

160
00:13:58,000 --> 00:14:03,000
There are relatives, wealth, even fame.

161
00:14:03,000 --> 00:14:06,000
And then he says, therefore you should train yourselves,

162
00:14:06,000 --> 00:14:11,000
thus we will increase in wisdom.

163
00:14:11,000 --> 00:14:15,000
It isn't such a way that you should train yourself.

164
00:14:15,000 --> 00:14:17,000
You don't worry about your circumstance.

165
00:14:17,000 --> 00:14:18,000
It's even people.

166
00:14:18,000 --> 00:14:20,000
People come and go.

167
00:14:20,000 --> 00:14:24,000
There's no question that all the people we know are going to be separated from.

168
00:14:24,000 --> 00:14:34,000
Born alone, we die alone, born alone, die alone again.

169
00:14:34,000 --> 00:14:40,000
The people we're with, they come and they go.

170
00:14:40,000 --> 00:14:43,000
We're like, what is this?

171
00:14:43,000 --> 00:14:45,000
I think it's a poem or something.

172
00:14:45,000 --> 00:14:49,000
We're like trains passing in the night or something like that.

173
00:14:49,000 --> 00:14:53,000
The very brief time we have together.

174
00:14:53,000 --> 00:14:56,000
And we make the mistake of getting,

175
00:14:56,000 --> 00:14:58,000
that sounds cruel, I suppose.

176
00:14:58,000 --> 00:15:03,000
But we do make the mistake of getting attached and as a result we suffer.

177
00:15:03,000 --> 00:15:06,000
And that sounds like a terrible thing to say.

178
00:15:06,000 --> 00:15:09,000
But if you look at it from the point of view of a Buddha,

179
00:15:09,000 --> 00:15:12,000
who sees people doing this again and again and again and again,

180
00:15:12,000 --> 00:15:15,000
and again and again and again and again and again.

181
00:15:15,000 --> 00:15:18,000
And it comes to think, you know, what is the point?

182
00:15:18,000 --> 00:15:20,000
Is this really?

183
00:15:20,000 --> 00:15:22,000
It seems worth it.

184
00:15:22,000 --> 00:15:25,000
It seems to us who can't remember anything.

185
00:15:25,000 --> 00:15:29,000
That this one life is somehow rich, valuable.

186
00:15:29,000 --> 00:15:35,000
But if you look at it objectively,

187
00:15:35,000 --> 00:15:38,000
there's a lot of suffering in life.

188
00:15:38,000 --> 00:15:40,000
And then to do it again and again and again,

189
00:15:40,000 --> 00:15:46,000
you start to wonder, what's the real, what's really the point?

190
00:15:46,000 --> 00:15:49,000
And with the precarious nature of human life

191
00:15:49,000 --> 00:15:54,000
and the potential to be reborn as an animal or in hell,

192
00:15:54,000 --> 00:15:57,000
it's actually quite scary.

193
00:15:57,000 --> 00:15:59,000
Because we don't know what we're going to be in our next life.

194
00:15:59,000 --> 00:16:01,000
Which direction we're going to go?

195
00:16:01,000 --> 00:16:04,000
Again, because our vehicle is uncertain.

196
00:16:04,000 --> 00:16:09,000
For fixed and focused on our experiences,

197
00:16:09,000 --> 00:16:12,000
our minds can easily become corrupted.

198
00:16:12,000 --> 00:16:17,000
So we focus on wisdom.

199
00:16:17,000 --> 00:16:19,000
How should we increase in wisdom?

200
00:16:19,000 --> 00:16:26,000
The basic, basic fundamental wisdom that we're looking for

201
00:16:26,000 --> 00:16:28,000
is simply to see things as they are.

202
00:16:28,000 --> 00:16:29,000
This is important to understand.

203
00:16:29,000 --> 00:16:31,000
Wisdom isn't theory.

204
00:16:31,000 --> 00:16:33,000
That's not real wisdom.

205
00:16:33,000 --> 00:16:36,000
We learned that we should be manga on Sunday

206
00:16:36,000 --> 00:16:39,000
that there's three types of wisdom.

207
00:16:39,000 --> 00:16:43,000
Suptame up anya from hearing.

208
00:16:43,000 --> 00:16:45,000
That's what you hear me say.

209
00:16:45,000 --> 00:16:47,000
You hear the Buddha say, or you read.

210
00:16:47,000 --> 00:16:52,000
Didn't tame up anya from thinking.

211
00:16:52,000 --> 00:16:55,000
When you think and you use logic in reason.

212
00:16:55,000 --> 00:16:57,000
But the third one, power anya up anya,

213
00:16:57,000 --> 00:17:01,000
comes just from seeing, from watching,

214
00:17:01,000 --> 00:17:04,000
what you learn about yourself when you meditate.

215
00:17:04,000 --> 00:17:07,000
Oh boy, I've got anger and issues.

216
00:17:07,000 --> 00:17:10,000
Oh boy, look at me hurting myself.

217
00:17:10,000 --> 00:17:13,000
Why am I doing that?

218
00:17:13,000 --> 00:17:15,000
Seeing the nature of your mind,

219
00:17:15,000 --> 00:17:17,000
seeing I'm hurting myself.

220
00:17:17,000 --> 00:17:21,000
No one else is hurting me.

221
00:17:21,000 --> 00:17:26,000
And the wisdom.

222
00:17:26,000 --> 00:17:32,000
To see the things that we cling to are not worth clinging to.

223
00:17:32,000 --> 00:17:33,000
Very simple wisdom.

224
00:17:33,000 --> 00:17:36,000
In fact, just knowing this is rising,

225
00:17:36,000 --> 00:17:39,000
this is falling, that's wisdom.

226
00:17:39,000 --> 00:17:45,000
It's the difference between seeing things as they are

227
00:17:45,000 --> 00:17:47,000
and judging them.

228
00:17:47,000 --> 00:17:49,000
When you don't know this is rising,

229
00:17:49,000 --> 00:17:51,000
you know it as good and as bad as me,

230
00:17:51,000 --> 00:17:52,000
it's mine and all the bad stuff.

231
00:17:52,000 --> 00:17:53,000
It's all the problems.

232
00:17:53,000 --> 00:17:56,000
It's kind of attachment to version.

233
00:17:56,000 --> 00:17:58,000
When you just know this is rising,

234
00:17:58,000 --> 00:18:00,000
this is fine.

235
00:18:00,000 --> 00:18:03,000
Your mind is pure, there's wisdom there.

236
00:18:03,000 --> 00:18:05,000
You do it enough.

237
00:18:05,000 --> 00:18:07,000
It starts to sink in.

238
00:18:07,000 --> 00:18:09,000
Oh, this is just rising.

239
00:18:09,000 --> 00:18:10,000
Oh, I see.

240
00:18:10,000 --> 00:18:15,000
It's not really good or bad or me or mine.

241
00:18:15,000 --> 00:18:16,000
I don't have to control it.

242
00:18:16,000 --> 00:18:18,000
I don't have to force it.

243
00:18:18,000 --> 00:18:21,000
I don't have to be in charge.

244
00:18:21,000 --> 00:18:26,000
Pain, thoughts, emotions.

245
00:18:26,000 --> 00:18:29,000
None of these things are me or mine.

246
00:18:29,000 --> 00:18:31,000
None of them are good or bad.

247
00:18:31,000 --> 00:18:33,000
They start to see that.

248
00:18:33,000 --> 00:18:35,000
Oh, this is just pain.

249
00:18:35,000 --> 00:18:37,000
This is just thoughts.

250
00:18:37,000 --> 00:18:40,000
Let me start to let it go.

251
00:18:40,000 --> 00:18:47,000
Anyway, so that's the little bit of dumb for tonight.

252
00:18:47,000 --> 00:18:51,000
We'll stop there and let's see if we have some questions.

253
00:18:51,000 --> 00:18:54,000
Robin, you don't have to come here every night.

254
00:18:54,000 --> 00:18:56,000
I know, you know, I don't have a day job,

255
00:18:56,000 --> 00:18:58,000
so this is fine for me to come every night,

256
00:18:58,000 --> 00:19:01,000
but please don't feel obligated to be here.

257
00:19:01,000 --> 00:19:06,000
If some days you are, that's fine.

258
00:19:06,000 --> 00:19:08,000
I'll show up when I can one day.

259
00:19:08,000 --> 00:19:10,000
Great.

260
00:19:10,000 --> 00:19:14,000
So someone sent me a question to ask you.

261
00:19:14,000 --> 00:19:18,000
And it is how to know in day to day to practice

262
00:19:18,000 --> 00:19:20,000
where we have to stick to conventions

263
00:19:20,000 --> 00:19:23,000
and can't go with Paramata,

264
00:19:23,000 --> 00:19:26,000
like while reading, studying, and talking with someone.

265
00:19:26,000 --> 00:19:29,000
And I have to admit, I don't know what Paramata is.

266
00:19:29,000 --> 00:19:32,000
Paramata, she's missing a T there.

267
00:19:32,000 --> 00:19:37,000
Paramata means meaning or nature.

268
00:19:37,000 --> 00:19:43,000
Meaning, it really means Paramah means ultimate or highest.

269
00:19:43,000 --> 00:19:46,000
So Paramata, Dhamma is a Dhamma in the highest sense,

270
00:19:46,000 --> 00:19:48,000
or the highest meaning.

271
00:19:48,000 --> 00:19:50,000
It means this ultimate reality.

272
00:19:50,000 --> 00:19:53,000
That's the translation of the word Paramata Dhamma.

273
00:19:53,000 --> 00:19:55,000
So the Abidhamma deals in Paramata Dhamma.

274
00:19:55,000 --> 00:19:57,000
Real Dhamma.

275
00:19:57,000 --> 00:19:59,000
The suit does deal in concept.

276
00:19:59,000 --> 00:20:01,000
I deal with people, places, things.

277
00:20:01,000 --> 00:20:03,000
But the Abidhamma just deals with qualities.

278
00:20:03,000 --> 00:20:05,000
Read the Abidhamma.

279
00:20:05,000 --> 00:20:08,000
It's quite breathtaking for a Buddhist.

280
00:20:08,000 --> 00:20:11,000
For anyone else, it just seems dense and readable

281
00:20:11,000 --> 00:20:12,000
and boring.

282
00:20:12,000 --> 00:20:15,000
But for a Buddhist, it's breathtaking.

283
00:20:15,000 --> 00:20:18,000
Learning about these Dhammas and lists and lists

284
00:20:18,000 --> 00:20:21,000
and different aspects of reality.

285
00:20:21,000 --> 00:20:28,000
Right, so you don't have to stick to conventions.

286
00:20:28,000 --> 00:20:30,000
The reality is still there.

287
00:20:30,000 --> 00:20:32,000
When you're talking to someone, you're lips are moving.

288
00:20:32,000 --> 00:20:33,000
There's a feeling of that.

289
00:20:33,000 --> 00:20:35,000
There are thoughts going through your mind.

290
00:20:35,000 --> 00:20:36,000
There are emotions.

291
00:20:36,000 --> 00:20:40,000
You can be mindful of all of those, even while you're talking.

292
00:20:40,000 --> 00:20:42,000
In between.

293
00:20:42,000 --> 00:20:44,000
You know, the point is when you're reading,

294
00:20:44,000 --> 00:20:46,000
you're not trying to practice meditation.

295
00:20:46,000 --> 00:20:48,000
You're trying to imbibe some knowledge.

296
00:20:48,000 --> 00:20:53,000
So you can't meditate at the moment when you're trying to

297
00:20:53,000 --> 00:20:55,000
understand something.

298
00:20:55,000 --> 00:20:58,000
But in between moments, you can be mindful.

299
00:20:58,000 --> 00:21:00,000
And especially in between sessions,

300
00:21:00,000 --> 00:21:03,000
so you do five minutes of studying, five minutes of meditation,

301
00:21:03,000 --> 00:21:07,000
half an hour of meditating, five minutes of half an hour of studying,

302
00:21:07,000 --> 00:21:09,000
five minutes of meditating.

303
00:21:09,000 --> 00:21:12,000
If you do breaks from meditation,

304
00:21:12,000 --> 00:21:16,000
or even just every moment that you remember to be mindful,

305
00:21:16,000 --> 00:21:18,000
just be mindful.

306
00:21:18,000 --> 00:21:22,000
You have learned yesterday or the day before.

307
00:21:22,000 --> 00:21:28,000
A moment of goodness is

308
00:21:28,000 --> 00:21:32,000
makes your life not a waste.

309
00:21:40,000 --> 00:21:42,000
We don't have any other questions.

310
00:21:42,000 --> 00:21:44,000
Just one more and one more.

311
00:21:44,000 --> 00:21:46,000
One more.

312
00:21:46,000 --> 00:21:49,000
One day is there a cumulative effect of wisdom over multiple births,

313
00:21:49,000 --> 00:21:51,000
coming across the dhamma in this life,

314
00:21:51,000 --> 00:21:53,000
completely by chance.

315
00:21:53,000 --> 00:21:55,000
Seems complete by chance.

316
00:21:55,000 --> 00:21:57,000
It's probably not by chance.

317
00:21:57,000 --> 00:21:58,000
Well, it could be.

318
00:21:58,000 --> 00:22:01,000
Things can just conspire to come together.

319
00:22:01,000 --> 00:22:03,000
I mean, usually not, you know.

320
00:22:03,000 --> 00:22:06,000
Now, and I think an orthodox Buddhist would,

321
00:22:06,000 --> 00:22:09,000
would scold me for even suggesting it.

322
00:22:09,000 --> 00:22:11,000
Because there's so many things that have to come together.

323
00:22:11,000 --> 00:22:12,000
You have to be born a human.

324
00:22:12,000 --> 00:22:15,000
You have to be born a human in the time of the Buddha.

325
00:22:15,000 --> 00:22:18,000
You have to have all your good faculties.

326
00:22:18,000 --> 00:22:20,000
You have to be able to understand.

327
00:22:20,000 --> 00:22:23,000
And you have to not be sickly and weak.

328
00:22:23,000 --> 00:22:28,000
Or worse, be corrupt of mind.

329
00:22:28,000 --> 00:22:31,000
And then you have to have the opportunity.

330
00:22:31,000 --> 00:22:35,000
It just seems like just a chance, right?

331
00:22:35,000 --> 00:22:39,000
Like I have four brothers.

332
00:22:39,000 --> 00:22:40,000
I have three brothers.

333
00:22:40,000 --> 00:22:44,000
None of them are really interested in Buddhism or spirituality

334
00:22:44,000 --> 00:22:46,000
even the way I am.

335
00:22:46,000 --> 00:22:49,000
And they all seem like nice people.

336
00:22:49,000 --> 00:22:52,000
And I suppose I can understand the different ways

337
00:22:52,000 --> 00:22:55,000
in which we've connected and probably were born together

338
00:22:55,000 --> 00:22:56,000
for those reasons.

339
00:22:56,000 --> 00:23:01,000
But none of them have sort of the inclination to

340
00:23:01,000 --> 00:23:05,000
or ever had the inclination towards the sort of things

341
00:23:05,000 --> 00:23:08,000
that I have had an inclination for.

342
00:23:08,000 --> 00:23:14,000
So I wouldn't say it's a chance.

343
00:23:14,000 --> 00:23:16,000
I mean, consider becoming a monk.

344
00:23:16,000 --> 00:23:18,000
If you look at all the people who try to become a monk,

345
00:23:18,000 --> 00:23:21,000
this isn't a brag, because I'm nothing.

346
00:23:21,000 --> 00:23:25,000
I'm not some special person, but there's a group of us

347
00:23:25,000 --> 00:23:27,000
who really had an easy time becoming monks.

348
00:23:27,000 --> 00:23:29,000
And that's those of us who are still monks.

349
00:23:29,000 --> 00:23:32,000
And it seems like we must have done something different

350
00:23:32,000 --> 00:23:36,000
in past lives, because I never wanted this role,

351
00:23:36,000 --> 00:23:37,000
but I never have.

352
00:23:37,000 --> 00:23:42,000
And it's just been, it's just never really been an issue

353
00:23:42,000 --> 00:23:43,000
in my mind.

354
00:23:43,000 --> 00:23:46,000
As soon as I learned about becoming a monk,

355
00:23:46,000 --> 00:23:51,000
but other people who have even students

356
00:23:51,000 --> 00:23:53,000
of mine who have ordained, you can see there's something

357
00:23:53,000 --> 00:23:55,000
very different going on in their minds.

358
00:23:55,000 --> 00:23:57,000
They can't do it, they can't take it.

359
00:23:57,000 --> 00:24:00,000
They just roam very quickly.

360
00:24:00,000 --> 00:24:04,000
Or after some time, they just can't take it.

361
00:24:04,000 --> 00:24:06,000
I's not to say that.

362
00:24:06,000 --> 00:24:12,000
But I would say very little of what we come across in our lives

363
00:24:12,000 --> 00:24:14,000
is most likely coincidence.

364
00:24:14,000 --> 00:24:18,000
Sure, it's hard to predict in its complex,

365
00:24:18,000 --> 00:24:22,000
but the big themes of our lives are probably

366
00:24:22,000 --> 00:24:26,000
very much associated with past lives.

367
00:24:26,000 --> 00:24:29,000
But simply to answer a question, wisdom, of course,

368
00:24:29,000 --> 00:24:31,000
has an effect over multiple births.

369
00:24:31,000 --> 00:24:35,000
I mean, wisdom has an effect on the future.

370
00:24:35,000 --> 00:24:37,000
The future includes next lives.

371
00:24:37,000 --> 00:24:41,000
So everything we do, effects are a future in some way

372
00:24:41,000 --> 00:24:44,000
or not a bigger small.

373
00:24:44,000 --> 00:24:45,000
And it's more complicated.

374
00:24:45,000 --> 00:24:49,000
They are the effects affect each other.

375
00:24:49,000 --> 00:25:00,000
So that's it, two questions.

376
00:25:00,000 --> 00:25:04,000
I must have answered them already.

377
00:25:04,000 --> 00:25:05,000
I think so.

378
00:25:05,000 --> 00:25:08,000
You have a lot of questions for last few nights, so.

379
00:25:08,000 --> 00:25:09,000
That's fine.

380
00:25:09,000 --> 00:25:11,000
I'm not really concerned about getting questions.

381
00:25:11,000 --> 00:25:14,000
It's nice when they come, but I was thinking,

382
00:25:14,000 --> 00:25:17,000
and this is really, really this is for my one meditator

383
00:25:17,000 --> 00:25:19,000
who's here and when meditators come,

384
00:25:19,000 --> 00:25:21,000
we'll warm editors come.

385
00:25:21,000 --> 00:25:23,000
This is a chance to for me to give them a little bit

386
00:25:23,000 --> 00:25:25,000
of them every day.

387
00:25:25,000 --> 00:25:28,000
So the fact that I'm broadcasting in this kind of,

388
00:25:28,000 --> 00:25:31,000
actually secondary, I think that's fair to say,

389
00:25:31,000 --> 00:25:35,000
even though you compare one person to hundreds and thousands

390
00:25:35,000 --> 00:25:38,000
of people who watch YouTube, my YouTube videos.

391
00:25:38,000 --> 00:25:42,000
Maybe it's not fair to say then, but it's a whole

392
00:25:42,000 --> 00:25:47,000
different issue because as you know, doing the meditation courses

393
00:25:47,000 --> 00:25:49,000
on the whole level, right?

394
00:25:49,000 --> 00:25:52,000
It's much more the important work that we're doing.

395
00:25:52,000 --> 00:25:59,000
It's very nice to listen to your demo talk slide,

396
00:25:59,000 --> 00:26:03,000
but it's nice to listen to the my YouTube as well.

397
00:26:03,000 --> 00:26:05,000
We have a new question.

398
00:26:05,000 --> 00:26:06,000
We do.

399
00:26:06,000 --> 00:26:10,000
Is the experience of attachment then suffering the loss

400
00:26:10,000 --> 00:26:13,000
required in order to rise above ignorance?

401
00:26:13,000 --> 00:26:14,000
No.

402
00:26:14,000 --> 00:26:16,000
Don't think that it's required to rise above ignorance

403
00:26:16,000 --> 00:26:18,000
is seeing things as they are.

404
00:26:18,000 --> 00:26:24,000
Seeing that the things that you, well, you know,

405
00:26:24,000 --> 00:26:29,000
not exactly, but the point is you have to have ignorance

406
00:26:29,000 --> 00:26:32,000
in order to let go of ignorance.

407
00:26:32,000 --> 00:26:39,000
And it's actually a good question,

408
00:26:39,000 --> 00:26:42,000
but maybe not for the reason that you think it's,

409
00:26:42,000 --> 00:26:46,000
you know, because you can have ignorance technically

410
00:26:46,000 --> 00:26:49,000
without craving, but I think that's just academic

411
00:26:49,000 --> 00:26:52,000
because ignorance leads to craving.

412
00:26:52,000 --> 00:26:54,000
We like in dislike things.

413
00:26:54,000 --> 00:26:57,000
We have partialities because we don't understand

414
00:26:57,000 --> 00:27:02,000
that things are impermanent and satisfying and controllable.

415
00:27:02,000 --> 00:27:05,000
So all you have to do is see that things are impermanent

416
00:27:05,000 --> 00:27:07,000
and satisfying and uncontrollable.

417
00:27:07,000 --> 00:27:12,000
And if you see that, that's considered the dispelling of ignorance

418
00:27:12,000 --> 00:27:15,000
and as a result of dispelling that ignorance

419
00:27:15,000 --> 00:27:18,000
about things, about experiences,

420
00:27:18,000 --> 00:27:21,000
you won't give rise to attachment.

421
00:27:21,000 --> 00:27:24,000
So it's not about suffering the loss.

422
00:27:24,000 --> 00:27:26,000
That's all sutta and dinta.

423
00:27:26,000 --> 00:27:28,000
It's all thinking, you know, you think,

424
00:27:28,000 --> 00:27:30,000
yeah, you know, it's not worth clinging to

425
00:27:30,000 --> 00:27:33,000
because when I clung to it before I suffered,

426
00:27:33,000 --> 00:27:35,000
that's not really what we're talking about.

427
00:27:35,000 --> 00:27:37,000
You don't have to see it like that.

428
00:27:37,000 --> 00:27:40,000
You just have to see that the things that you cling to

429
00:27:40,000 --> 00:27:42,000
are not worth clinging to.

430
00:27:42,000 --> 00:27:44,000
I mean, or sorry, you have to see

431
00:27:44,000 --> 00:27:46,000
that the objects of experience are impermanent

432
00:27:46,000 --> 00:27:47,000
because they're impermanent.

433
00:27:47,000 --> 00:27:49,000
They're not worth clinging to you.

434
00:27:49,000 --> 00:27:51,000
You don't have to cling to them to know

435
00:27:51,000 --> 00:27:53,000
that they're not worth clinging to.

436
00:27:53,000 --> 00:27:55,000
Once you see them clearly,

437
00:27:55,000 --> 00:27:57,000
you'd think it would be absurd to cling to such a thing.

438
00:27:57,000 --> 00:27:58,000
Why?

439
00:27:58,000 --> 00:27:59,000
Because it comes and it goes.

440
00:27:59,000 --> 00:28:01,000
It's gone in a moment.

441
00:28:01,000 --> 00:28:04,000
It's really not the sort of thing that you could possibly cling to.

442
00:28:04,000 --> 00:28:09,000
One day, I think we missed one last night,

443
00:28:09,000 --> 00:28:13,000
or maybe it came in after we signed off,

444
00:28:13,000 --> 00:28:16,000
but is it bad if you're a meditator

445
00:28:16,000 --> 00:28:17,000
and you don't get out that often

446
00:28:17,000 --> 00:28:19,000
and you don't socialize much?

447
00:28:19,000 --> 00:28:23,000
I've been told that it's unhealthy for me to do that.

448
00:28:23,000 --> 00:28:26,000
But if you don't, if you don't socialize,

449
00:28:26,000 --> 00:28:28,000
right, that if you don't go out.

450
00:28:28,000 --> 00:28:31,000
And I'm going to say,

451
00:28:31,000 --> 00:28:34,000
Ah, I got a tabraumma or something,

452
00:28:34,000 --> 00:28:36,000
and I don't know if it's quite too full grammar.

453
00:28:36,000 --> 00:28:40,000
One is like brahma.

454
00:28:40,000 --> 00:28:42,000
Brahma means God.

455
00:28:42,000 --> 00:28:45,000
When you're alone, you're like brahma.

456
00:28:45,000 --> 00:28:50,000
I got the ved.

457
00:28:50,000 --> 00:28:53,000
Like the angels are two.

458
00:28:53,000 --> 00:28:55,000
When you're together with another person,

459
00:28:55,000 --> 00:28:58,000
it's like being an angel.

460
00:28:58,000 --> 00:29:08,040
People is like a village, four people are more, it's like it's an uproar, like a

461
00:29:08,040 --> 00:29:10,960
commotion.

462
00:29:10,960 --> 00:29:17,760
Being alone is great, being on socialized on the rainbow.

463
00:29:17,760 --> 00:29:22,440
I don't think it's fair to say that you become a more...

464
00:29:22,440 --> 00:29:25,360
I mean, you have to find...

465
00:29:25,360 --> 00:29:26,840
I suppose that's not quite fair to say.

466
00:29:26,840 --> 00:29:30,200
I think it's a question is, if you're alone, what are you doing?

467
00:29:30,200 --> 00:29:34,680
So just being alone in and of itself is not good, or it's not not good.

468
00:29:34,680 --> 00:29:38,880
It may not be bad, but there's nothing good about it.

469
00:29:38,880 --> 00:29:40,360
It depends what you're doing alone.

470
00:29:40,360 --> 00:29:47,160
The point is that being alone allows you to do much more soul searching, so to speak,

471
00:29:47,160 --> 00:29:53,240
so learning about yourself, and that's what's important about being alone.

472
00:29:53,240 --> 00:29:59,000
You can't easily do that when you're with that, but being with good people, on the

473
00:29:59,000 --> 00:30:05,120
other hand, can encourage you to spend time alone when you have good friends, but not

474
00:30:05,120 --> 00:30:06,120
socializing.

475
00:30:06,120 --> 00:30:11,600
I think it still stands because if you're talking about getting out and socializing,

476
00:30:11,600 --> 00:30:16,040
the secret that most people don't know, it's not really worth it.

477
00:30:16,040 --> 00:30:20,640
This idea that someone's socializing is going to make you happy, it's really not.

478
00:30:20,640 --> 00:30:27,560
It's going to make you all neurotic, worried about what other people think of you, and addicted

479
00:30:27,560 --> 00:30:34,640
to company and enjoyment and pleasure and even drugs and alcohol and all sorts of things.

480
00:30:34,640 --> 00:30:43,320
I think the person that asked that question is, is one of our younger meditators, and that's

481
00:30:43,320 --> 00:30:47,880
something that happens, you know, families worry about you that you're not going out and

482
00:30:47,880 --> 00:30:48,880
doing.

483
00:30:48,880 --> 00:30:54,880
Tell them, tell them it's okay, you're going to do this, okay, why not, kids?

484
00:30:54,880 --> 00:31:03,880
I don't know how well that would go over and most of them.

485
00:31:03,880 --> 00:31:12,880
All right, we'll have more questions, you know where I'll be tomorrow.

486
00:31:12,880 --> 00:31:15,880
Good night, everyone.

487
00:31:15,880 --> 00:31:22,880
Thank you, John, thank you, good night.

