1
00:00:00,000 --> 00:00:09,000
Ready to go live?

2
00:00:09,000 --> 00:00:12,000
Yes.

3
00:00:12,000 --> 00:00:27,000
Okay, good evening, everyone broadcasting live, December 5th, it's the King of Thailand's

4
00:00:27,000 --> 00:00:32,000
birthday today.

5
00:00:32,000 --> 00:00:42,000
Wonder what's happening in the kingdom?

6
00:00:42,000 --> 00:00:44,000
What's news today?

7
00:00:44,000 --> 00:00:48,000
Any interesting news?

8
00:00:48,000 --> 00:00:50,000
I didn't watch too much news today.

9
00:00:50,000 --> 00:00:55,000
I was out most of the day getting things done.

10
00:00:55,000 --> 00:00:59,000
All the coverage here is still on the shootings in California.

11
00:00:59,000 --> 00:01:04,000
Oh, right.

12
00:01:04,000 --> 00:01:14,000
Getting a lot of calls.

13
00:01:14,000 --> 00:01:18,000
I've gotten calls from people today and yesterday.

14
00:01:18,000 --> 00:01:21,000
Hey, I found you on meetup.

15
00:01:21,000 --> 00:01:26,000
Ah, good meetup is really actually a big thing.

16
00:01:26,000 --> 00:01:27,000
Yeah.

17
00:01:27,000 --> 00:01:34,000
Oh, tomorrow, I think I'd better skip the Visudimaga class.

18
00:01:34,000 --> 00:01:37,000
We better just cancel it, I guess.

19
00:01:37,000 --> 00:01:46,000
Because there's a interfaith meeting, meeting of the interfaith

20
00:01:46,000 --> 00:01:50,000
peace group in Hamilton.

21
00:01:50,000 --> 00:01:55,000
One of the leaders is an ex from McMaster professor.

22
00:01:55,000 --> 00:01:59,000
Her husband was one of my professors 16 years ago.

23
00:01:59,000 --> 00:02:02,000
And I've just reconnected with him.

24
00:02:02,000 --> 00:02:04,000
So I'm on their email list.

25
00:02:04,000 --> 00:02:07,000
And there was one a few weeks ago and I didn't go.

26
00:02:07,000 --> 00:02:13,000
But tomorrow, I think someone's coming maybe to pick me up here on their way.

27
00:02:13,000 --> 00:02:19,000
So I should probably go.

28
00:02:19,000 --> 00:02:23,000
Sure, I'll send out a message to cancel the class for tomorrow.

29
00:02:23,000 --> 00:02:28,000
Yeah, but I think I'll be able to make it for the two meetings.

30
00:02:28,000 --> 00:02:31,000
Is that the thing? Is that 230?

31
00:02:31,000 --> 00:02:32,000
Oh, yeah.

32
00:02:32,000 --> 00:02:34,000
Yeah, I'll make it for the meetings.

33
00:02:34,000 --> 00:02:36,000
Maybe I have to leave the second one a bit early.

34
00:02:36,000 --> 00:02:39,000
I don't know.

35
00:02:39,000 --> 00:02:45,000
That sounds good.

36
00:02:45,000 --> 00:02:49,000
Next week, I have exams, one exam.

37
00:02:49,000 --> 00:02:53,000
And then I have to start studying for the other two.

38
00:02:53,000 --> 00:03:01,000
So I have to read Descartes, Locke, and Spinoza.

39
00:03:01,000 --> 00:03:05,000
So that's good to read about.

40
00:03:05,000 --> 00:03:07,000
Sort of.

41
00:03:07,000 --> 00:03:12,000
I mean, some of the things these guys believe are kind of weird.

42
00:03:12,000 --> 00:03:16,000
But it's good to know.

43
00:03:16,000 --> 00:03:22,000
It gives you language, you know, learning the way Western philosophers thought

44
00:03:22,000 --> 00:03:26,000
and spoke and the things they discussed.

45
00:03:26,000 --> 00:03:29,000
It allows us to explain Buddhism.

46
00:03:29,000 --> 00:03:30,000
And many of them were influenced.

47
00:03:30,000 --> 00:03:31,000
Not many of them.

48
00:03:31,000 --> 00:03:33,000
Some of them were influenced by Buddhism.

49
00:03:33,000 --> 00:03:38,000
So some of them have actually interesting ways of describing and explaining things

50
00:03:38,000 --> 00:03:44,000
that we also describe and explain, but in a Western context.

51
00:03:44,000 --> 00:03:47,000
So there's benefit there.

52
00:03:47,000 --> 00:03:50,000
Was the most modern philosopher that you're studying?

53
00:03:50,000 --> 00:03:52,000
Well, it's early modern philosophy.

54
00:03:52,000 --> 00:03:57,000
So these three are the cusp of early modern thought.

55
00:03:57,000 --> 00:04:04,000
When people started to question the church and started to try to find ways of knowing

56
00:04:04,000 --> 00:04:11,000
that we're outside of the Bible says.

57
00:04:11,000 --> 00:04:19,000
So they mostly believed in God still, but they started to question many things.

58
00:04:19,000 --> 00:04:30,000
Started to figure out how to know, how to learn.

59
00:04:30,000 --> 00:04:35,000
Probably wouldn't take the class again if I had to do it over.

60
00:04:35,000 --> 00:04:38,000
Is there a part two with more modern philosophy?

61
00:04:38,000 --> 00:04:41,000
There's more philosophy courses I can take.

62
00:04:41,000 --> 00:04:46,000
Kind of turned off of philosophy courses, though.

63
00:04:46,000 --> 00:04:51,000
Too much too speculative, really, for me.

64
00:04:51,000 --> 00:04:59,000
Because Buddhist, there's a woman of Chinese ethnicity.

65
00:04:59,000 --> 00:05:04,000
I guess you could say she's Canadian, but her parents, I think, are from China.

66
00:05:04,000 --> 00:05:09,000
In my class, and I asked her what she thought of the class.

67
00:05:09,000 --> 00:05:13,000
I think she said she kind of gave a disgusted noise.

68
00:05:13,000 --> 00:05:21,000
And we were talking about I asked her if she's in Eastern philosophy, Chinese philosophy.

69
00:05:21,000 --> 00:05:26,000
And I said, yeah, I think I had enough with Western philosophy.

70
00:05:26,000 --> 00:05:29,000
Maybe I should have taken that one instead.

71
00:05:29,000 --> 00:05:32,000
I'm not sure why I didn't.

72
00:05:32,000 --> 00:05:40,000
Maybe it's a night class or something.

73
00:05:40,000 --> 00:05:44,000
You know, I think I'll probably stick with it for another semester,

74
00:05:44,000 --> 00:05:50,000
because stopping in January seems a bit troublesome.

75
00:05:50,000 --> 00:05:57,000
I mean, they're paid for my tuition for the government paid for my tuition for the whole year.

76
00:05:57,000 --> 00:06:02,000
I'm not sure how they look at it if I dropped out.

77
00:06:02,000 --> 00:06:04,000
It gets complicated, I think.

78
00:06:04,000 --> 00:06:08,000
I think so.

79
00:06:08,000 --> 00:06:10,000
Have you signed up for classes for next semester?

80
00:06:10,000 --> 00:06:11,000
Is it too early?

81
00:06:11,000 --> 00:06:13,000
I have, but I'm probably going to change it.

82
00:06:13,000 --> 00:06:19,000
I'm probably going to take the second half of Latin.

83
00:06:19,000 --> 00:06:20,000
Because it is useful.

84
00:06:20,000 --> 00:06:22,000
It's the base of English.

85
00:06:22,000 --> 00:06:25,000
It's one of the bases of English.

86
00:06:25,000 --> 00:06:27,000
But also because it's pretty easy.

87
00:06:27,000 --> 00:06:32,000
I'm currently getting, I think, 103% in the class.

88
00:06:32,000 --> 00:06:36,000
I've got over 100% in each of the quizzes.

89
00:06:36,000 --> 00:06:39,000
I wasn't going to take the second half.

90
00:06:39,000 --> 00:06:42,000
But then now I thought, well, it's so easy.

91
00:06:42,000 --> 00:06:47,000
It's one last thing I have to really expend a lot of energy in.

92
00:06:47,000 --> 00:06:50,000
They don't have poly or Sanskrit?

93
00:06:50,000 --> 00:06:55,000
They have Sanskrit, but I was afraid to take it because my Sanskrit is very rusty.

94
00:06:55,000 --> 00:06:58,000
And this was going to be my first goal.

95
00:06:58,000 --> 00:07:00,000
So I can't take it now in January.

96
00:07:00,000 --> 00:07:03,000
I'd have to start it next semester, next September.

97
00:07:03,000 --> 00:07:06,000
Probably should have taken it in retrospect.

98
00:07:06,000 --> 00:07:08,000
But I was cautious.

99
00:07:08,000 --> 00:07:13,000
Also, I was thinking I'd be in Stony Creek.

100
00:07:13,000 --> 00:07:18,000
Yeah.

101
00:07:18,000 --> 00:07:20,000
Now it's good to be cautious, you first semester bad.

102
00:07:20,000 --> 00:07:24,000
I didn't want to get overwhelmed.

103
00:07:24,000 --> 00:07:25,000
So I came back.

104
00:07:25,000 --> 00:07:28,000
I could take it fourth year Sanskrit.

105
00:07:28,000 --> 00:07:32,000
It's really hardcore.

106
00:07:32,000 --> 00:07:40,000
I might not be going to take it because I can't remember much of Sanskrit grammar.

107
00:07:40,000 --> 00:07:43,000
I'd be really rusty.

108
00:07:43,000 --> 00:07:48,000
They don't have anything more introductory than fourth year?

109
00:07:48,000 --> 00:07:52,000
Well, it's strange how they made it the first year.

110
00:07:52,000 --> 00:07:54,000
The first class is third year.

111
00:07:54,000 --> 00:07:56,000
And the second class is fourth year.

112
00:07:56,000 --> 00:07:58,000
There's no first or second year.

113
00:07:58,000 --> 00:08:02,000
I think the point being that it's more designed,

114
00:08:02,000 --> 00:08:04,000
they don't want first year's taking it.

115
00:08:04,000 --> 00:08:08,000
I think they did that because half the people end up failing the course.

116
00:08:08,000 --> 00:08:13,000
Or when I took it, I think half the people dropped out or failed it.

117
00:08:13,000 --> 00:08:18,000
And that's when it was said as a third year class.

118
00:08:18,000 --> 00:08:25,000
So I think the point is if they made it a first year class,

119
00:08:25,000 --> 00:08:28,000
it's just a recipe for disaster.

120
00:08:28,000 --> 00:08:29,000
Yeah.

121
00:08:29,000 --> 00:08:30,000
And make it a third year class.

122
00:08:30,000 --> 00:08:32,000
I guess there's a lot of prerequisites.

123
00:08:32,000 --> 00:08:37,000
I think you might have to be in the for here to take it or something.

124
00:08:37,000 --> 00:08:42,000
You ready for it?

125
00:08:42,000 --> 00:08:45,000
Ready for a question, by the way?

126
00:08:45,000 --> 00:08:46,000
Sure.

127
00:08:46,000 --> 00:08:50,000
How does one note wanting to not do something as in trying to suppress a behavior?

128
00:08:50,000 --> 00:08:54,000
Can I say not wanting?

129
00:08:54,000 --> 00:08:59,000
Sure.

130
00:08:59,000 --> 00:09:11,000
There was once this non-time woman who I think she wasn't a nun in the beginning.

131
00:09:11,000 --> 00:09:16,000
I said that sounds very good at ordaining people.

132
00:09:16,000 --> 00:09:18,000
She was bulimic, I think.

133
00:09:18,000 --> 00:09:21,000
And so she couldn't keep food down.

134
00:09:21,000 --> 00:09:24,000
She would throw it out.

135
00:09:24,000 --> 00:09:28,000
And maybe bulimic or maybe she just couldn't keep it down.

136
00:09:28,000 --> 00:09:32,000
Let me give you a second.

137
00:09:32,000 --> 00:09:34,000
And so she explained this to him.

138
00:09:34,000 --> 00:09:40,000
And he had her noting a chin or a chin throwing up,

139
00:09:40,000 --> 00:09:46,000
throwing up vomiting, vomiting.

140
00:09:46,000 --> 00:09:49,000
And she started.

141
00:09:49,000 --> 00:09:54,000
She was practicing, but it was really difficult for her.

142
00:09:54,000 --> 00:10:03,000
And so she came back one day and said, Jan, Jan, I want to die.

143
00:10:03,000 --> 00:10:07,000
She said, I can't take it.

144
00:10:07,000 --> 00:10:08,000
That's something.

145
00:10:08,000 --> 00:10:10,000
I said, I want to die.

146
00:10:10,000 --> 00:10:16,000
Or something like, I'm not, I think it started off.

147
00:10:16,000 --> 00:10:23,000
I'm not, because of my why, which means I can't take it basically.

148
00:10:23,000 --> 00:10:26,000
And he said, I want to, he said, I can't take it.

149
00:10:26,000 --> 00:10:27,000
Just say to yourself, can't take it.

150
00:10:27,000 --> 00:10:29,000
Can't take it.

151
00:10:29,000 --> 00:10:31,000
And he said, Jan, I want to kill myself.

152
00:10:31,000 --> 00:10:32,000
I want to die.

153
00:10:32,000 --> 00:10:33,000
He said, I want to die.

154
00:10:33,000 --> 00:10:34,000
I want to die.

155
00:10:34,000 --> 00:10:35,000
I want to die.

156
00:10:35,000 --> 00:10:37,000
I just, Jan, I don't want to live.

157
00:10:37,000 --> 00:10:38,000
Not wanting to live.

158
00:10:38,000 --> 00:10:39,000
No, I don't.

159
00:10:39,000 --> 00:10:42,000
He really doesn't get faced.

160
00:10:42,000 --> 00:10:46,000
He gave her no quarter.

161
00:10:46,000 --> 00:10:49,000
And finally, she started laughing.

162
00:10:49,000 --> 00:10:51,000
And she broke down.

163
00:10:51,000 --> 00:10:55,000
Like, she let go a little bit, you know, and started laughing.

164
00:10:55,000 --> 00:10:58,000
And Jan, and he started laughing with her.

165
00:10:58,000 --> 00:11:02,000
And I think then she became a nun.

166
00:11:02,000 --> 00:11:05,000
She ended up becoming a nun.

167
00:11:05,000 --> 00:11:14,000
She really was able to overcome it.

168
00:11:14,000 --> 00:11:18,000
But that's, it was kind of being facetious, I think.

169
00:11:18,000 --> 00:11:21,000
I mean, you really shouldn't say, I don't want to live.

170
00:11:21,000 --> 00:11:22,000
Not wanting to live.

171
00:11:22,000 --> 00:11:23,000
Not wanting to live.

172
00:11:23,000 --> 00:11:24,000
Not wanting to live.

173
00:11:24,000 --> 00:11:26,000
It should be a little more specific than that.

174
00:11:26,000 --> 00:11:28,000
Like, you're upset or sad or depressed.

175
00:11:28,000 --> 00:11:30,000
That's a bit better.

176
00:11:30,000 --> 00:11:37,000
But you see how sometimes that's the best way to,

177
00:11:37,000 --> 00:11:41,000
when people are stubborn, especially, you don't want to live.

178
00:11:41,000 --> 00:11:43,000
Well, they just say, not wanting to live.

179
00:11:43,000 --> 00:11:44,000
Not wanting to live.

180
00:11:44,000 --> 00:11:48,000
I mean, it's kind of this kind of question.

181
00:11:48,000 --> 00:11:50,000
It's very much about semantics.

182
00:11:50,000 --> 00:11:52,000
It's not really a real question.

183
00:11:52,000 --> 00:11:55,000
It's not really an important one.

184
00:11:55,000 --> 00:11:56,000
I'm sorry.

185
00:11:56,000 --> 00:11:58,000
I don't mean to pick too critical.

186
00:11:58,000 --> 00:12:01,000
But like, if you look closer, you'll see that there's

187
00:12:01,000 --> 00:12:06,000
realities that are a bit more specific and real than

188
00:12:06,000 --> 00:12:09,000
whether you want something or you don't want it.

189
00:12:09,000 --> 00:12:12,000
And so I'll just words, what's the state of mind?

190
00:12:12,000 --> 00:12:14,000
What's the question?

191
00:12:14,000 --> 00:12:15,000
What's going on in the mind?

192
00:12:15,000 --> 00:12:16,000
That's the question.

193
00:12:16,000 --> 00:12:18,000
If it's, if you want something, if you want,

194
00:12:18,000 --> 00:12:21,000
or if you have desire or rise, then it's desire.

195
00:12:21,000 --> 00:12:23,000
If you have a version or rise in its aversion,

196
00:12:23,000 --> 00:12:25,000
they're very distinct mind states.

197
00:12:25,000 --> 00:12:26,000
And you can tell which one.

198
00:12:26,000 --> 00:12:28,000
And often, they're mixed up with each other.

199
00:12:28,000 --> 00:12:29,000
There's desire.

200
00:12:29,000 --> 00:12:30,000
Then there's aversion.

201
00:12:30,000 --> 00:12:32,000
And there's delusion.

202
00:12:32,000 --> 00:12:38,000
And there's lots of states.

203
00:12:38,000 --> 00:12:41,000
A follow-up comment from the questioner.

204
00:12:41,000 --> 00:12:44,000
It says, it says, it's clearly wanting,

205
00:12:44,000 --> 00:12:49,000
but he's not sure if he needs to emphasize the negation.

206
00:12:49,000 --> 00:12:52,000
I think you have to say to yourself thinking,

207
00:12:52,000 --> 00:12:54,000
thinking, wondering, wondering,

208
00:12:54,000 --> 00:12:57,000
doubting, doubting sounds more important.

209
00:13:07,000 --> 00:13:10,000
I have a 10-year-old niece who always asks

210
00:13:10,000 --> 00:13:12,000
me questions about Buddhism.

211
00:13:12,000 --> 00:13:15,000
I was thinking about getting our book on Japikha stories.

212
00:13:15,000 --> 00:13:17,000
I think you have experienced teaching kids.

213
00:13:17,000 --> 00:13:20,000
What do you think is it age appropriate?

214
00:13:20,000 --> 00:13:22,000
Can you recommend any additions?

215
00:13:22,000 --> 00:13:28,000
There are Japikhas for children by Ken and Risako Kawasaki.

216
00:13:28,000 --> 00:13:31,000
You find them on BuddhaNet.net.

217
00:13:31,000 --> 00:13:34,000
Japikhas stories for kids.

218
00:13:34,000 --> 00:13:36,000
Most of them are pretty good.

219
00:13:36,000 --> 00:13:39,000
They're retold in a way that kids

220
00:13:39,000 --> 00:13:42,000
find helpful.

221
00:13:42,000 --> 00:13:46,000
Someone once sent me a book for children.

222
00:13:46,000 --> 00:13:48,000
Japikhas stories are retold.

223
00:13:48,000 --> 00:13:49,000
There was a monk.

224
00:13:49,000 --> 00:13:53,000
It's actually even by this book, apparently.

225
00:13:53,000 --> 00:14:00,000
It's like Buddhist bedtime stories or something.

226
00:14:00,000 --> 00:14:05,000
But yeah, the one by Ken and Risako Kawasaki

227
00:14:05,000 --> 00:14:12,000
are probably a good bed.

228
00:14:16,000 --> 00:14:19,000
If you're looking for that.

229
00:14:19,000 --> 00:14:21,000
I have you.

230
00:14:21,000 --> 00:14:26,000
Another thing is maybe show her my DVD on how to meditate for kids.

231
00:14:26,000 --> 00:14:29,000
There's some good lessons in there on different ways of meditating.

232
00:14:29,000 --> 00:14:34,000
Opening kids' eyes up, getting them an idea of what meditation is.

233
00:14:34,000 --> 00:14:37,000
It's not just the insight meditation.

234
00:14:37,000 --> 00:14:47,000
It's sort of a meditation primer for kids for videos.

235
00:14:47,000 --> 00:14:48,000
It's really worth it.

236
00:14:48,000 --> 00:14:50,000
I think it's good.

237
00:14:50,000 --> 00:14:54,000
I've seen good results with it for a three and a five-year-old kid

238
00:14:54,000 --> 00:14:59,000
that they were able to understand.

239
00:14:59,000 --> 00:15:01,000
Or just my book on how to meditate.

240
00:15:01,000 --> 00:15:09,000
11, she can already start to learn how to meditate from that book

241
00:15:09,000 --> 00:15:17,000
that probably.

242
00:15:17,000 --> 00:15:26,000
If an habit dies or leaves some monastery, how is the next habit selected?

243
00:15:26,000 --> 00:15:32,000
Habits aren't a part of Buddhism.

244
00:15:32,000 --> 00:15:36,000
We don't have habits in the vignaya.

245
00:15:36,000 --> 00:15:39,000
So that's totally dependent on the country.

246
00:15:39,000 --> 00:15:41,000
There's nothing to do with Buddhism directly.

247
00:15:41,000 --> 00:15:43,000
Many monks have complained about that.

248
00:15:43,000 --> 00:15:44,000
They're not many monks.

249
00:15:44,000 --> 00:15:47,000
Some monks have complained about that.

250
00:15:47,000 --> 00:15:51,000
That the habits in most countries have too much power.

251
00:15:51,000 --> 00:15:54,000
It's not according to the vignaya at all.

252
00:15:54,000 --> 00:15:55,000
According to the vignaya.

253
00:15:55,000 --> 00:16:00,000
The sanga has to be in charge, not for one person.

254
00:16:00,000 --> 00:16:03,000
Isn't habit different than a head monk?

255
00:16:03,000 --> 00:16:06,000
There's no such thing as a head monk or an habit.

256
00:16:06,000 --> 00:16:09,000
That term doesn't exist.

257
00:16:09,000 --> 00:16:11,000
Okay.

258
00:16:11,000 --> 00:16:13,000
It goes by seniority always.

259
00:16:13,000 --> 00:16:19,000
So if it's more senior monk comes, he's more senior.

260
00:16:19,000 --> 00:16:23,000
But it goes by seniority in some cases for decision-making.

261
00:16:23,000 --> 00:16:34,000
It has to go by the sanga.

262
00:16:34,000 --> 00:16:37,000
And even still, you know, there's not.

263
00:16:37,000 --> 00:16:41,000
Well, if you're staying in a place where there's not for monks,

264
00:16:41,000 --> 00:16:42,000
then you can't have a sanga.

265
00:16:42,000 --> 00:16:44,000
So then it's just, you know, you try to get along

266
00:16:44,000 --> 00:16:47,000
and try to be respectful to your seniors.

267
00:16:47,000 --> 00:17:00,000
That kind of thing.

268
00:17:00,000 --> 00:17:01,000
Well, we heard those terms all the time.

269
00:17:01,000 --> 00:17:02,000
Head monk and habit.

270
00:17:02,000 --> 00:17:07,000
So it's just something that just kind of came to be,

271
00:17:07,000 --> 00:17:09,000
but it's not really supposed to be.

272
00:17:09,000 --> 00:17:13,000
Not really.

273
00:17:13,000 --> 00:17:17,000
You have to be careful. It can be an ego trip.

274
00:17:17,000 --> 00:17:23,000
And then the head monk.

275
00:17:23,000 --> 00:17:25,000
Seniority with monks.

276
00:17:25,000 --> 00:17:28,000
Is it years as a monk or years of life?

277
00:17:28,000 --> 00:17:30,000
It's moments as a monk.

278
00:17:30,000 --> 00:17:33,000
So I was ordained with 18 monks.

279
00:17:33,000 --> 00:17:38,000
And I think I was the second to last.

280
00:17:38,000 --> 00:17:42,000
So all the others were senior to me.

281
00:17:42,000 --> 00:17:48,000
Actually, I'm not sure if that's true because we ordained in three.

282
00:17:48,000 --> 00:17:49,000
So I don't know how that works.

283
00:17:49,000 --> 00:17:54,000
I think it still works according to a, according to,

284
00:17:54,000 --> 00:17:56,000
there's an order.

285
00:17:56,000 --> 00:17:59,000
But we ordained three at a time.

286
00:17:59,000 --> 00:18:01,000
So I'm not sure how that quaver acts.

287
00:18:01,000 --> 00:18:02,000
Anyway, none of those.

288
00:18:02,000 --> 00:18:03,000
I'm the only one left.

289
00:18:03,000 --> 00:18:05,000
I'm the only one left of those 18.

290
00:18:05,000 --> 00:18:07,000
Something you're the senior.

291
00:18:07,000 --> 00:18:09,000
Well, they're all disrobed.

292
00:18:09,000 --> 00:18:10,000
Yes.

293
00:18:10,000 --> 00:18:12,000
But yeah, you have to.

294
00:18:12,000 --> 00:18:14,000
So when you meet a monk, you have to ask when they were ordained.

295
00:18:14,000 --> 00:18:16,000
And then sometimes you have to calculate.

296
00:18:16,000 --> 00:18:19,000
Usually you say how many, how many rains are you?

297
00:18:19,000 --> 00:18:22,000
And then even if it's an equal number,

298
00:18:22,000 --> 00:18:25,000
then you have to ask when would they were ordained?

299
00:18:25,000 --> 00:18:28,000
I'm older than most monks.

300
00:18:28,000 --> 00:18:30,000
My age.

301
00:18:30,000 --> 00:18:34,000
Because I wasn't most monks at least in Thailand.

302
00:18:34,000 --> 00:18:38,000
Because most Thai monks ordained before the rains.

303
00:18:38,000 --> 00:18:40,000
But I ordained for the king's birthday.

304
00:18:40,000 --> 00:18:42,000
Oh, today's my birthday.

305
00:18:42,000 --> 00:18:44,000
No, I ordained yesterday.

306
00:18:44,000 --> 00:18:45,000
December 4th.

307
00:18:45,000 --> 00:18:46,000
I miss my birthday.

308
00:18:46,000 --> 00:18:48,000
Oh, congratulations.

309
00:18:48,000 --> 00:18:50,000
No, I stopped thinking about it.

310
00:18:50,000 --> 00:18:52,000
So I was trying to think how many is that?

311
00:18:52,000 --> 00:18:53,000
That's 14 now.

312
00:18:53,000 --> 00:18:55,000
14 years a month yesterday.

313
00:18:55,000 --> 00:18:57,000
Because I ordained for the king's birthday,

314
00:18:57,000 --> 00:18:58,000
which is today.

315
00:18:58,000 --> 00:19:01,000
But we ordained the day before because on the king's birthday,

316
00:19:01,000 --> 00:19:03,000
there's too much going on.

317
00:19:03,000 --> 00:19:10,000
So yeah, my birthday was yesterday.

318
00:19:10,000 --> 00:19:12,000
Well, happy belated birthday, man.

319
00:19:12,000 --> 00:19:17,000
Thank you.

320
00:19:17,000 --> 00:19:22,000
Do you have a sangha in Hamilton?

321
00:19:22,000 --> 00:19:24,000
Well, there's in Stony Creek.

322
00:19:24,000 --> 00:19:28,000
There's at least three, sometimes four monks.

323
00:19:28,000 --> 00:19:32,000
So together with them we can form a sangha.

324
00:19:32,000 --> 00:19:34,000
Then there's a Lao Shin monk.

325
00:19:34,000 --> 00:19:36,000
And there's a bunch more Lao Shin monks.

326
00:19:36,000 --> 00:19:40,000
And it's for Lincoln monks in Toronto area.

327
00:19:40,000 --> 00:19:43,000
There's lots of monks around if we want to perform a sangha.

328
00:19:43,000 --> 00:19:49,000
But I'm alone, so I don't have a sangha here.

329
00:19:49,000 --> 00:19:54,000
Do any of them get together to recite the Kathimokha?

330
00:19:54,000 --> 00:19:59,000
Not that I know of.

331
00:19:59,000 --> 00:20:09,000
So I'm going to try to give you the cake emote

332
00:20:09,000 --> 00:20:12,000
in the meditation chat, but it's not working.

333
00:20:12,000 --> 00:20:13,000
Doesn't work.

334
00:20:13,000 --> 00:20:15,000
No, I had noticed that too.

335
00:20:15,000 --> 00:20:19,000
I was trying to do that for someone's birthday at one point.

336
00:20:19,000 --> 00:20:28,000
But I can fix that.

337
00:20:28,000 --> 00:20:31,000
Looks like a cake to you.

338
00:20:31,000 --> 00:20:39,000
Must just not be showing it for some reason on the strain.

339
00:20:39,000 --> 00:20:46,000
No.

340
00:20:46,000 --> 00:20:55,000
That the cakes then did.

341
00:20:55,000 --> 00:20:58,000
Thank you.

342
00:20:58,000 --> 00:21:03,000
Thank you.

343
00:21:03,000 --> 00:21:30,000
Thank you.

344
00:21:30,000 --> 00:21:53,000
Thank you.

345
00:21:53,000 --> 00:22:18,000
Let's find that symbol.

346
00:22:18,000 --> 00:22:37,000
Mm-hmm.

347
00:22:37,000 --> 00:23:06,000
So strange.

348
00:23:06,000 --> 00:23:16,000
It should work, actually.

349
00:23:16,000 --> 00:23:26,000
Looks like it's funny looking at programming code.

350
00:23:26,000 --> 00:23:32,000
It's all gibberish.

351
00:23:32,000 --> 00:23:35,000
Computers are crazy things.

352
00:23:35,000 --> 00:23:36,000
Yes, they are.

353
00:23:36,000 --> 00:23:41,000
We have some fairly inappropriate emoticons for our meditation chat panel.

354
00:23:41,000 --> 00:23:48,000
There's like a cocktail and they can get rid of them.

355
00:23:48,000 --> 00:23:51,000
Someone, I can't quite see what they are.

356
00:23:51,000 --> 00:23:54,000
Maybe a pizza.

357
00:23:54,000 --> 00:24:16,000
That's fine.

358
00:24:16,000 --> 00:24:26,000
Lots of angry faces, devil.

359
00:24:26,000 --> 00:24:35,000
Ten not to use those.

360
00:24:35,000 --> 00:24:59,000
Thank you.

361
00:24:59,000 --> 00:25:28,840
Oh, I wonder if, you know, I don't know, I don't know, I don't know what's happening.

362
00:25:28,840 --> 00:25:32,840
I'm too dumb for this.

363
00:25:32,840 --> 00:25:35,840
I don't really know what's going on.

364
00:25:35,840 --> 00:25:43,840
The place that...

365
00:25:43,840 --> 00:25:45,840
Oh wait, it's insert smiling that we want.

366
00:25:45,840 --> 00:25:46,840
This is the problem.

367
00:25:46,840 --> 00:25:52,840
Message, vowel, plus s, insert smiling.

368
00:25:52,840 --> 00:25:55,840
Okay, so each smiley box,

369
00:25:55,840 --> 00:26:00,840
we have to find the smiley box.

370
00:26:00,840 --> 00:26:05,840
Oh, but there's a populate smiley thing with it.

371
00:26:05,840 --> 00:26:09,840
So we populate the box.

372
00:26:09,840 --> 00:26:12,840
Is this done on WordPress as well, Bunty?

373
00:26:12,840 --> 00:26:16,840
No, this is just something I put together.

374
00:26:16,840 --> 00:26:23,840
Probably not something we want to work on in a live forum.

375
00:26:23,840 --> 00:26:29,840
Everyone's trying the cake.

376
00:26:29,840 --> 00:26:31,840
All right, I'll figure it out.

377
00:26:31,840 --> 00:26:34,840
Can you work on it and also answer a question at the same time?

378
00:26:34,840 --> 00:26:36,840
I shouldn't do two things at once.

379
00:26:36,840 --> 00:26:38,840
What's the question?

380
00:26:38,840 --> 00:26:42,840
Is intense fear common in meditation?

381
00:26:42,840 --> 00:26:50,840
Any question that asks whether something's common is really not really...

382
00:26:50,840 --> 00:26:53,840
You can't really answer that.

383
00:26:53,840 --> 00:26:55,840
I mean, there's no such thing as common.

384
00:26:55,840 --> 00:26:59,840
Everyone has different experiences.

385
00:26:59,840 --> 00:27:02,840
What would it matter if it was common?

386
00:27:02,840 --> 00:27:03,840
What does that mean?

387
00:27:03,840 --> 00:27:05,840
It doesn't really mean anything.

388
00:27:05,840 --> 00:27:09,840
Maybe that's another way of saying this is happening to me.

389
00:27:09,840 --> 00:27:13,840
Am I doing something wrong?

390
00:27:13,840 --> 00:27:18,840
I mean, even asking whether you're doing something wrong is in this understanding.

391
00:27:18,840 --> 00:27:22,840
Generally, because if you're being mindful, then you're doing it right.

392
00:27:22,840 --> 00:27:25,840
If you're not being mindful, then it doesn't matter what happens.

393
00:27:25,840 --> 00:27:29,840
Doing it wrong doesn't have anything to do with what happens.

394
00:27:29,840 --> 00:27:35,840
It's how you react to what happens.

395
00:27:35,840 --> 00:27:38,840
That is a good point.

396
00:27:38,840 --> 00:27:42,840
If you're doing it right, you'd think why is this fear coming up in me?

397
00:27:42,840 --> 00:27:44,840
I'm meditating, but that's really it.

398
00:27:44,840 --> 00:28:09,840
The meditation often helps you to see things that you don't want to see.

399
00:28:09,840 --> 00:28:21,840
Can we meditate to help others be mindful?

400
00:28:21,840 --> 00:28:23,840
Not directly.

401
00:28:23,840 --> 00:28:26,840
I mean, if you're mindful, it encourages others, other people,

402
00:28:26,840 --> 00:28:29,840
and it provides a good example for them.

403
00:28:29,840 --> 00:28:34,840
But I don't really see how it would be possible to certainly

404
00:28:34,840 --> 00:28:50,840
nothing about meditating to help, and why would that be something you think possible?

405
00:28:50,840 --> 00:28:57,840
It may be helping someone meditating to give a wish that someone could be mindful.

406
00:28:57,840 --> 00:29:04,840
Maybe like sending a wish that someone can be mindful is that possible?

407
00:29:04,840 --> 00:29:12,840
Well, if wishes were fishes, then we'd have a fry.

408
00:29:12,840 --> 00:29:15,840
It must be a Canadian saying.

409
00:29:15,840 --> 00:29:19,840
That's an American saying, and we're told that I'm an American.

410
00:29:19,840 --> 00:29:21,840
I don't know.

411
00:29:21,840 --> 00:29:22,840
It could be.

412
00:29:22,840 --> 00:29:25,840
I'm sure I haven't heard all the things.

413
00:29:25,840 --> 00:29:28,840
Okay, you know what's happening?

414
00:29:28,840 --> 00:29:31,840
As nothing to do with the smileies, the smileies are being put correctly.

415
00:29:31,840 --> 00:29:33,840
So I can turn all those things into cakes.

416
00:29:33,840 --> 00:29:39,840
I do have to figure out some things going on somewhere else.

417
00:29:39,840 --> 00:29:46,840
We're formatting the chat improperly.

418
00:29:46,840 --> 00:29:52,840
Okay, so for our meditation group, we have smileies that indicate swearing,

419
00:29:52,840 --> 00:29:55,840
marching, angry, and the devil.

420
00:29:55,840 --> 00:29:58,840
I think we need some new smileies.

421
00:29:58,840 --> 00:30:03,840
Vamiting.

422
00:30:03,840 --> 00:30:13,840
Okay, you know what I'm going to do?

423
00:30:13,840 --> 00:30:23,840
This is good.

424
00:30:23,840 --> 00:30:28,840
You're all going to get a bunch of alerts now that are going to make you think it's

425
00:30:28,840 --> 00:30:29,840
growing crazy.

426
00:30:29,840 --> 00:30:40,840
It's going to go crazy for a bit just because I have to test and see what's going on.

427
00:30:40,840 --> 00:30:44,840
Just don't refresh your brows and that's fine.

428
00:30:44,840 --> 00:30:49,840
Yeah, I didn't do that.

429
00:30:49,840 --> 00:31:17,840
Thank you.

430
00:31:17,840 --> 00:31:36,840
All right, that's it.

431
00:31:36,840 --> 00:31:55,840
Thank you.

432
00:31:55,840 --> 00:32:21,840
Oh, there's two men right there.

433
00:32:21,840 --> 00:32:31,840
Okay, it is removing that thing from...

434
00:32:31,840 --> 00:32:37,840
Is the cake, the only one not working, did anyone notice anything else not working?

435
00:32:37,840 --> 00:32:47,840
I think that's the only one I've come across.

436
00:32:47,840 --> 00:33:10,840
Thank you.

437
00:33:10,840 --> 00:33:24,840
Oh, well.

438
00:33:24,840 --> 00:33:26,840
Beyond me.

439
00:33:26,840 --> 00:33:27,840
No cake.

440
00:33:27,840 --> 00:33:28,840
Not the cake.

441
00:33:28,840 --> 00:33:31,840
It's a good lesson for us all.

442
00:33:31,840 --> 00:33:32,840
Let me remove that.

443
00:33:32,840 --> 00:33:36,840
We want to remove the martini.

444
00:33:36,840 --> 00:33:43,840
There's a bunch of punching and swearing and puking.

445
00:33:43,840 --> 00:33:48,840
Puking is a puking as part of the meditation process.

446
00:33:48,840 --> 00:33:49,840
That's true.

447
00:33:49,840 --> 00:33:52,840
I'm not sure that anyone's ever used that in chat though.

448
00:33:52,840 --> 00:33:54,840
But it's kind of cool.

449
00:33:54,840 --> 00:33:58,840
It does take the all characters.

450
00:33:58,840 --> 00:34:08,840
We've got faces and then we've got drink, probably the drink one with it.

451
00:34:08,840 --> 00:34:11,840
Is it a parenthesis?

452
00:34:11,840 --> 00:34:16,840
It looks like double parenthesis D.

453
00:34:16,840 --> 00:34:22,840
Ninja can you keep it in?

454
00:34:22,840 --> 00:34:29,840
It's an ankle.

455
00:34:29,840 --> 00:34:31,840
It's a high B.

456
00:34:31,840 --> 00:34:36,840
We got rid of the finger, the bandit, the drunk smoking beer and mooning.

457
00:34:36,840 --> 00:34:39,840
Those ones are gone.

458
00:34:39,840 --> 00:34:44,840
As I've deleted already.

459
00:34:44,840 --> 00:34:48,840
Pizza, coffee, phone.

460
00:34:48,840 --> 00:34:54,840
Punching right down from the green one.

461
00:34:54,840 --> 00:34:57,840
What's the code for that?

462
00:34:57,840 --> 00:35:00,840
Double parenthesis punch.

463
00:35:00,840 --> 00:35:01,840
Punch.

464
00:35:01,840 --> 00:35:02,840
Punch.

465
00:35:02,840 --> 00:35:07,840
You must have missed it.

466
00:35:07,840 --> 00:35:09,840
Maybe it's up here.

467
00:35:09,840 --> 00:35:12,840
Oh, it's a puking.

468
00:35:12,840 --> 00:35:19,840
I don't think anyone's ever used that.

469
00:35:19,840 --> 00:35:24,840
I don't think anyone's ever used that.

470
00:35:24,840 --> 00:35:31,840
I don't think anyone's ever used that.

471
00:35:31,840 --> 00:35:34,840
Get the cash money sign there.

472
00:35:34,840 --> 00:35:44,840
Well, sometimes I have to talk about money.

473
00:35:44,840 --> 00:35:47,840
Anyway, we're kind of just wasting time, aren't we?

474
00:35:47,840 --> 00:35:50,840
Well, I think there was another question.

475
00:35:50,840 --> 00:35:51,840
I'm sorry.

476
00:35:51,840 --> 00:35:55,840
I'm falling down on my duties here.

477
00:35:55,840 --> 00:35:59,840
When meeting someone for the first time, what question do you find most helpful to

478
00:35:59,840 --> 00:36:11,840
develop an understanding between each other?

479
00:36:11,840 --> 00:36:13,840
It's an interesting question.

480
00:36:13,840 --> 00:36:15,840
I don't think of such things.

481
00:36:15,840 --> 00:36:17,840
I don't worry about such things.

482
00:36:17,840 --> 00:36:20,840
I think it should be mindful when you meet people.

483
00:36:20,840 --> 00:36:22,840
It's more important.

484
00:36:22,840 --> 00:36:25,840
I mean, could you imagine if you had one question that you asked everybody?

485
00:36:25,840 --> 00:36:30,840
Wouldn't it come up really like a canned, you know?

486
00:36:30,840 --> 00:36:31,840
I like a pickup line.

487
00:36:31,840 --> 00:36:32,840
Yeah.

488
00:36:32,840 --> 00:36:36,840
Oh, you must say that to all the people.

489
00:36:36,840 --> 00:36:44,840
You must say that to all the Buddhists.

490
00:36:44,840 --> 00:36:48,840
I don't have a list of questions that I have a list of questions that I ask meditators.

491
00:36:48,840 --> 00:36:52,840
I've got some fairly wrote questions there.

492
00:36:52,840 --> 00:36:56,840
When I meet people, I try to be as natural as I can.

493
00:36:56,840 --> 00:36:58,840
I think being natural is important.

494
00:36:58,840 --> 00:37:04,840
Not having an agenda, not having anything in behind, you know?

495
00:37:04,840 --> 00:37:06,840
Not coming as a Buddhist even.

496
00:37:06,840 --> 00:37:13,840
Coming at people from a native point of view of human being,

497
00:37:13,840 --> 00:37:16,840
as natural as possible.

498
00:37:16,840 --> 00:37:21,840
So that's what we're aiming for, is nature beyond nature.

499
00:37:21,840 --> 00:37:25,840
Because nature, as we know it, is almost up.

500
00:37:25,840 --> 00:37:27,840
It's not really natural.

501
00:37:27,840 --> 00:37:32,840
But the nature that we understand is like rape is natural,

502
00:37:32,840 --> 00:37:33,840
and murder is natural.

503
00:37:33,840 --> 00:37:35,840
Is it all part of the natural world?

504
00:37:35,840 --> 00:37:38,840
But they're very unnatural things.

505
00:37:38,840 --> 00:37:43,840
So, through meditation, we become quite natural in a sense,

506
00:37:43,840 --> 00:37:49,840
in a sense of eddies and without pretense or agenda,

507
00:37:49,840 --> 00:37:52,840
without artifice, without any sort.

508
00:38:11,840 --> 00:38:14,840
I think we should just say good night.

509
00:38:14,840 --> 00:38:16,840
Thank you, Bhante.

510
00:38:16,840 --> 00:38:19,840
Thank you, Robin. Thanks, everyone. Have a good night.

511
00:38:19,840 --> 00:38:22,840
Good night.

512
00:38:22,840 --> 00:38:25,840
Oh, we had that.

513
00:38:25,840 --> 00:38:27,840
We have a quote, right?

514
00:38:27,840 --> 00:38:29,840
I'm sorry, I ended the broadcast down.

515
00:38:29,840 --> 00:38:31,840
That's what we're supposed to do, right?

516
00:38:31,840 --> 00:38:34,840
I'm supposed to look at that quote.

517
00:38:34,840 --> 00:38:43,840
Well, after a member, next time we have to remember to look at the quote.

518
00:38:43,840 --> 00:38:48,840
And I'll try to make videos.

519
00:38:48,840 --> 00:38:50,840
I'll try to maybe add a video.

520
00:38:50,840 --> 00:38:54,840
Maybe Saturday night, we can have a video.

521
00:38:54,840 --> 00:38:57,840
I'm recording, start to do Buddhism 101 again.

522
00:38:57,840 --> 00:39:01,840
Maybe I can do that Saturday.

523
00:39:01,840 --> 00:39:02,840
Let's see.

524
00:39:02,840 --> 00:39:04,840
Good night.

525
00:39:04,840 --> 00:39:14,840
Signing off.

