1
00:00:00,000 --> 00:00:07,840
What part does music play in lay life and monastic life?

2
00:00:07,840 --> 00:00:11,840
Is any sort of music forbidden or encouraged?

3
00:00:11,840 --> 00:00:17,440
It doesn't play any part, and I'd say it's not encouraged.

4
00:00:17,440 --> 00:00:23,760
It's indulged in that so music is not beneficial.

5
00:00:23,760 --> 00:00:32,520
Music is something that excites the one's desire and one's attachment.

6
00:00:32,520 --> 00:00:34,160
Sorry.

7
00:00:34,160 --> 00:00:36,040
I was in musician, I was in a band.

8
00:00:36,040 --> 00:00:45,760
I played the piano, I played classical guitar, I played, I had a Les Paul 76 Deluxe.

9
00:00:45,760 --> 00:00:53,600
I was in a band, I had long hair, and then I was a punk rocker.

10
00:00:53,600 --> 00:01:08,720
Well, heavily in the music, more than most people I was in, I don't know the word.

11
00:01:08,720 --> 00:01:16,720
Words always escape me, I was in, I was on the tip of my tongue, starts with an E, I think.

12
00:01:16,720 --> 00:01:30,920
These people who really know their stuff, a snob kind of, kind of a music snob, you know,

13
00:01:30,920 --> 00:01:39,320
even though I know that, and I gave it up, I don't listen to music at all, I'm not,

14
00:01:39,320 --> 00:01:44,080
I'm certainly not less happy for it, I was miserable, I was horrible.

15
00:01:44,080 --> 00:01:52,080
I was really, yeah, if anyone ever says I don't look so, I don't look happy enough.

16
00:01:52,080 --> 00:01:56,960
You should have seen me before, I was really depressed, and that's when I was in the thick

17
00:01:56,960 --> 00:02:03,880
of it, no sex, drugs and rock and roll, I had everything, I had everything that was supposed

18
00:02:03,880 --> 00:02:09,840
to make you happy, and I was miserable, it's probably because I was miserable that I was

19
00:02:09,840 --> 00:02:18,200
so inclined towards it. I spent some time recently writing down, because I often have

20
00:02:18,200 --> 00:02:22,800
to think back what year was this, so I spent some time writing down what happened every

21
00:02:22,800 --> 00:02:29,240
year of my life, that I can remember, and it was interesting in the teenage years how it

22
00:02:29,240 --> 00:02:36,640
was just so dark, so hard to remember. Of course, childhood, early childhood was even worse,

23
00:02:36,640 --> 00:02:42,920
but in a different way, because teenagers, they are getting further away now, but they're

24
00:02:42,920 --> 00:02:51,640
not that far, but it's funny how dark they are, how that's how I remember them actually,

25
00:02:51,640 --> 00:02:59,640
that's how I remarked upon them, how dark it was, how what a whole, my teenage existence

26
00:02:59,640 --> 00:03:05,600
was, that's why I think we go for drugs and sex and rock and roll, not because we're

27
00:03:05,600 --> 00:03:13,840
happy, because we're the kind of people who find happiness, because we're miserable,

28
00:03:13,840 --> 00:03:26,400
and we're like drug addicts, yep, sorry, I mean, such a bummer now, many monks, party

29
00:03:26,400 --> 00:03:42,400
poopers.

