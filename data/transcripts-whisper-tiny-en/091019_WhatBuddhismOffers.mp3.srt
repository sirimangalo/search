1
00:00:00,000 --> 00:00:07,340
Okay, welcome everyone. Today is going to be the first in a series of internet-based

2
00:00:07,340 --> 00:00:16,320
dummatocks. So we're broadcasting live here from Grover Beach today. So today I

3
00:00:16,320 --> 00:00:22,360
will be talking about some of the basics of Buddhism. Sort of where Buddhism

4
00:00:22,360 --> 00:00:31,080
comes from and what Buddhism means for us in our daily lives. What it means for us

5
00:00:31,080 --> 00:00:41,480
in modern, in the modern times. So where Buddhism comes from this is a very broad

6
00:00:41,480 --> 00:00:47,840
sort of question in one which can give many different answers.

7
00:00:47,840 --> 00:00:59,600
So from a historical perspective, we understand that Buddhism came from India. It

8
00:00:59,600 --> 00:01:07,600
was started by a man named Siddhartha Gautama who was born in a place which

9
00:01:07,600 --> 00:01:19,720
is now in Nepal and who left the home life to seek out freedom from all the

10
00:01:19,720 --> 00:01:26,800
age, freedom from sickness and freedom from death. And was successful in this

11
00:01:26,800 --> 00:01:34,880
quest that after a period of six years as the tradition goes. He was able

12
00:01:34,880 --> 00:01:40,640
to understand for himself how the world works, how reality works, the nature

13
00:01:40,640 --> 00:01:47,600
of reality. He understood why we suffer. What is the cause of all of our

14
00:01:47,600 --> 00:01:54,440
sufferings? And he understood the way to be free from those causes,

15
00:01:54,440 --> 00:02:02,880
if we need to be free from suffering. So in brief this is an understanding of

16
00:02:02,880 --> 00:02:11,720
where Buddhism comes from. It comes from this one man's realizations, practicing

17
00:02:11,720 --> 00:02:18,600
alone in the forest. And there are many different other answers that can be

18
00:02:18,600 --> 00:02:23,360
given. You could give the answer on a prehistoric basis. So we have these

19
00:02:23,360 --> 00:02:33,800
who are often called Buddhist legends. We have the legends of the Buddha's past lives which

20
00:02:33,800 --> 00:02:42,560
for most Buddhists are more than legends or the facts of the experience of the

21
00:02:42,560 --> 00:02:49,200
Bodhisattva leading up to his enlightenment. So we understand in Buddhism that

22
00:02:49,200 --> 00:02:54,800
time doesn't begin when we're born, that actually part of the nature of reality

23
00:02:54,800 --> 00:02:59,840
is that the mind just continues on and on and actually it's really just the

24
00:02:59,840 --> 00:03:04,440
present moment. There is no past, there is no future and this present moment is

25
00:03:04,440 --> 00:03:15,640
just continuing on and on. It's sort of a permanent moment and so the idea

26
00:03:15,640 --> 00:03:26,440
of past lives is not a strange or mythical sort of, it's not an illusion, it's a

27
00:03:26,440 --> 00:03:33,840
reality. We understand the death itself is the illusion. So we have these stories of

28
00:03:33,840 --> 00:03:37,960
the Buddha's past lives or the Bodhisattva's past lives. Bodhisattva means one

29
00:03:37,960 --> 00:03:43,360
who is looking to become enlightened or trying to become enlightened. And so

30
00:03:43,360 --> 00:03:49,200
when we look at it from this perspective Buddhism actually starts quite a bit

31
00:03:49,200 --> 00:03:55,840
earlier than 2500 years ago and it starts with the aspiration of the

32
00:03:55,840 --> 00:04:02,880
Bodhisattva at the feet of fully enlightened being where he saw someone who

33
00:04:02,880 --> 00:04:08,440
would become fully enlightened countless and countless eons ago before the

34
00:04:08,440 --> 00:04:18,720
Big Bang before countless Big Bangs. When he made this

35
00:04:18,720 --> 00:04:32,320
observation to become a fully enlightened being himself and so from that point

36
00:04:32,320 --> 00:04:40,080
on he worked and developed himself in many of the different lives and we have

37
00:04:40,080 --> 00:04:47,200
these stories written down 500 in some different stories. And I recorded a talk

38
00:04:47,200 --> 00:04:54,160
where I was reading from one of those stories the longest of the stories the

39
00:04:54,160 --> 00:05:00,160
most of the jatika which I've put on the internet. Just to give an idea of sort

40
00:05:00,160 --> 00:05:10,480
of the wonderful nature of this being and the incredible work that he did and

41
00:05:10,480 --> 00:05:16,520
what it took him to become fully enlightened. And so this is an important part of

42
00:05:16,520 --> 00:05:23,040
where Buddhism comes from and we often overlook is this prehistoric aspect.

43
00:05:23,040 --> 00:05:29,920
Pre-historic meaning before we can remember before most of us have before

44
00:05:29,920 --> 00:05:40,800
society can remember before we have historical records and a secular on a

45
00:05:40,800 --> 00:05:50,800
secular level. But another way of answering the question where Buddhism comes

46
00:05:50,800 --> 00:05:58,840
from is from the point of view of reality or the point of view of nature. And here

47
00:05:58,840 --> 00:06:02,960
the answer which many Buddhists like to give is that really Buddhism comes

48
00:06:02,960 --> 00:06:08,440
from within. That we have the truth inside of ourselves. Buddha means one who

49
00:06:08,440 --> 00:06:14,280
knows, one who has come to know, one who has come to realize the truth for

50
00:06:14,280 --> 00:06:21,720
themselves is not one who creates the truth and not one who makes something up

51
00:06:21,720 --> 00:06:31,240
or creates something like a kingdom or a palace or so on. But someone who

52
00:06:31,240 --> 00:06:36,920
realizes the truth of what's already there. So in this sense Buddhism is within

53
00:06:36,920 --> 00:06:41,760
ourselves and where it comes from is the deep exploration of our own being of

54
00:06:41,760 --> 00:06:48,000
who we are and what we are and what is real and what is illusion and being able

55
00:06:48,000 --> 00:06:56,920
to see the difference between the two. So in this sense Buddhism is

56
00:06:56,920 --> 00:07:12,560
incredibly pertinent to any era, any time it can never get old. As long as

57
00:07:12,560 --> 00:07:15,760
there is reality there can always be the realization and the understanding of

58
00:07:15,760 --> 00:07:23,680
reality, Buddhism cannot be something which gets old in time where this or

59
00:07:23,680 --> 00:07:31,120
that practice no longer apply. Because it doesn't rely on any specific practice,

60
00:07:31,120 --> 00:07:35,800
it doesn't rely on any specific technique. So we have lots of techniques and

61
00:07:35,800 --> 00:07:42,560
these techniques may even change over time as people change as the way we look at

62
00:07:42,560 --> 00:07:48,840
the world changes. So this is why when we teach Buddhism sometimes teaching the

63
00:07:48,840 --> 00:07:53,960
way they teach in other countries doesn't work, we have to adapt it and so on.

64
00:07:53,960 --> 00:08:02,880
Although in general it's more of a generation thing where the things they

65
00:08:02,880 --> 00:08:06,640
practiced in the time of the Buddha are quite difficult for us to practice nowadays.

66
00:08:06,640 --> 00:08:13,440
So many of these things have been dropped or modified and nowadays we're lucky if

67
00:08:13,440 --> 00:08:19,360
we can even sit down for about an hour or sit down for 30 minutes or so and the

68
00:08:19,360 --> 00:08:23,720
Buddha's time they would say it for days and days on end and so they had the

69
00:08:23,720 --> 00:08:30,000
potential to gain great, incredible states of consciousness and super

70
00:08:30,000 --> 00:08:34,840
power and so on. This is all well documented in the text and there are still

71
00:08:34,840 --> 00:08:39,080
people to this day who can realize these states but for most of us the

72
00:08:39,080 --> 00:08:43,160
idea is to get the bare minimum something for most of us it's just something

73
00:08:43,160 --> 00:08:48,680
that can help us in our lives. For those of us who don't have time to leave the

74
00:08:48,680 --> 00:08:58,080
home life or who don't have the opportunity to sit and meditate for days on end.

75
00:08:58,080 --> 00:09:04,320
We're just looking for something that can help us in our lives and there's

76
00:09:04,320 --> 00:09:10,680
no reason why this shouldn't be seen as a perfectly reasonable

77
00:09:10,680 --> 00:09:17,520
interpretation of what is Buddhism. Because Buddhism it's like a well that's

78
00:09:17,520 --> 00:09:22,320
inside of us and we can tap into it whenever we want and however much we

79
00:09:22,320 --> 00:09:35,280
choose. So then this is starting to answer the question of how Buddhism is

80
00:09:35,280 --> 00:09:52,160
useful for us especially in modern times. We look at many of the external

81
00:09:52,160 --> 00:09:57,880
appearances of Buddhism and it sometimes seems a little bit archaic. People

82
00:09:57,880 --> 00:10:03,600
even say this about monks and the monks life which actually I would tend to

83
00:10:03,600 --> 00:10:08,800
disagree with. It doesn't seem like there's anything incompatible in the

84
00:10:08,800 --> 00:10:14,560
monks life with modern society but no more so than in the time of the Buddha.

85
00:10:14,560 --> 00:10:21,120
But many of the external practices, rights and rituals and so on just seem

86
00:10:21,120 --> 00:10:30,680
quite foreign. Often the culture that surrounds Buddhism has come to the

87
00:10:30,680 --> 00:10:40,280
forefront. But really as most Buddhist teachers will explain since Buddhism is

88
00:10:40,280 --> 00:10:43,720
within us and it's a realization of the reality. It's something we can

89
00:10:43,720 --> 00:10:51,800
practice anywhere, anytime, any place, doing anything. It's something you can

90
00:10:51,800 --> 00:10:55,960
practice washing dishes. It's something you can practice driving your car.

91
00:10:55,960 --> 00:10:59,440
It's something you can practice dealing with when you're dealing with people

92
00:10:59,440 --> 00:11:05,320
and something you can practice when you're all alone. It's something you

93
00:11:05,320 --> 00:11:07,960
can practice when you're happy. It's something you can practice when you're

94
00:11:07,960 --> 00:11:14,200
sad. It's something you can practice in any situation. The Buddha said that the

95
00:11:14,200 --> 00:11:21,560
practice of sati, this practice of remembering yourself, bringing yourself back

96
00:11:21,560 --> 00:11:26,640
to the present moment. It's something that's useful at all times. It's never

97
00:11:26,640 --> 00:11:31,440
never without benefit. There could be no situation where it was

98
00:11:31,440 --> 00:11:42,720
unbeneficial or useless. So we have lots of examples of this in modern times.

99
00:11:42,720 --> 00:11:49,000
People will practice meditation when they're driving, for instance, they'll be

100
00:11:49,000 --> 00:11:54,000
driving on the highway and find themselves getting angry, getting frustrated,

101
00:11:54,000 --> 00:12:01,600
and then they can look at themselves and examine this state and see why

102
00:12:01,600 --> 00:12:10,080
this is arising. I'm to see how the mind has really fallen into a

103
00:12:10,080 --> 00:12:15,320
delusion of sorts, which has given rise to this anger. That really there's no

104
00:12:15,320 --> 00:12:20,520
reason to be angry. There's nothing particularly worth getting angry about

105
00:12:20,520 --> 00:12:25,080
in the experience. It's all just in our mind. We're giving rise to all of these

106
00:12:25,080 --> 00:12:33,000
different mind states. Like I'm going to be late or that person cut me off or so

107
00:12:33,000 --> 00:12:36,400
and instead of just seeing things for what they are and letting them be the

108
00:12:36,400 --> 00:12:43,200
way they are, we're not happy and we're not able to accept reality where if

109
00:12:43,200 --> 00:12:52,200
we were just simply able to accept things as they are for what they are, then

110
00:12:52,200 --> 00:12:59,160
we wouldn't have any reason to get angry or frustrated. So the way we do this is

111
00:12:59,160 --> 00:13:03,320
simply looking at the experience. We're not trying to change it because that's

112
00:13:03,320 --> 00:13:06,960
the whole problem in the first place. It's trying to change or alter an

113
00:13:06,960 --> 00:13:16,040
experience to be the way we think we should it be. We think it should be. So given

114
00:13:16,040 --> 00:13:20,600
that that's the problem, we're going to stop that. We're not going to attack the

115
00:13:20,600 --> 00:13:26,080
experience, attack our problem with another problem. We're going to change the

116
00:13:26,080 --> 00:13:31,840
way we react or the way we respond to the reality and this is by simply

117
00:13:31,840 --> 00:13:41,800
experiencing and accepting and letting it come in. So when we see things, we're

118
00:13:41,800 --> 00:13:46,680
looking at them, just seeing them for what they are, seeing, reminding ourselves

119
00:13:46,680 --> 00:13:52,640
that that's seeing, not letting our mind, not giving our mind a chance, not giving

120
00:13:52,640 --> 00:13:59,280
our mind an opportunity to make more of it than it actually is. When we see

121
00:13:59,280 --> 00:14:05,480
we just say to ourselves seeing, reminding ourselves that that's seeing and then

122
00:14:05,480 --> 00:14:10,680
when we do get angry, we same thing we're not judging the anger either because

123
00:14:10,680 --> 00:14:14,040
the anger is the judgment, that's the problem. If we just go about judging the

124
00:14:14,040 --> 00:14:18,280
anger, then we're just creating more anger. So we just say to ourselves angry

125
00:14:18,280 --> 00:14:21,680
reminding ourselves that this is anger that we shouldn't get even upset about

126
00:14:21,680 --> 00:14:27,880
the anger because that's really what happens is we get angry and then we don't

127
00:14:27,880 --> 00:14:31,720
like being angry and it makes us more angry. Why is this person doing something to

128
00:14:31,720 --> 00:14:36,640
make me angry and that makes us more angry that we blow up? When we look at the

129
00:14:36,640 --> 00:14:40,400
anger, we forget all about the reason why we're angry. We don't make the

130
00:14:40,400 --> 00:14:44,360
connection anymore. This is bad. This shouldn't be like this and so on. We just

131
00:14:44,360 --> 00:14:51,160
see things for what they are. We see the anger for what it is. So we say to ourselves

132
00:14:51,160 --> 00:14:56,840
angry, angry, angry, angry, just repeating the word to ourselves because this is

133
00:14:56,840 --> 00:15:03,080
what reminds us. This is what tells us, no, this is anger. Not this is good, this is

134
00:15:03,080 --> 00:15:08,960
bad, this is me, this is mine, this shouldn't be, this should be and so on. There's

135
00:15:08,960 --> 00:15:15,280
no room for that anymore. The mind is full. This is what we mean by mindful. The

136
00:15:15,280 --> 00:15:21,920
mind is full with what is real. Full with a clear thought of this is this,

137
00:15:21,920 --> 00:15:30,480
that is that. It is what it is. Many people use this when they're talking to

138
00:15:30,480 --> 00:15:34,920
people. When they're in conversation, sometimes someone will start saying

139
00:15:34,920 --> 00:15:40,280
yelling and saying nasty things. That's going to be quite difficult but if it's

140
00:15:40,280 --> 00:15:44,040
something that happens repeatedly, eventually you can just set yourself up for it and

141
00:15:44,040 --> 00:15:47,480
you know that it's going to come and you just say, okay, next time this comes,

142
00:15:47,480 --> 00:15:51,680
I'm going to sit down and be mindful instead of responding and then they're

143
00:15:51,680 --> 00:15:56,120
just sit there and say hearing, hearing, hearing, just knowing that they're

144
00:15:56,120 --> 00:16:01,000
sound arising and the funny thing about this, you can still tell what the person

145
00:16:01,000 --> 00:16:06,400
is saying. You can even process it but because your mind is so clear and so pure,

146
00:16:06,400 --> 00:16:12,320
you see it for what it is. You've got no reason to get angry, you can see why

147
00:16:12,320 --> 00:16:17,760
would I get angry? This is a sound arising at the ear. This person has their

148
00:16:17,760 --> 00:16:22,280
anger. If I were to get angry, that would be only a cause for more suffering. It

149
00:16:22,280 --> 00:16:26,480
wouldn't help the situation because your mind is clear, you're able to see all

150
00:16:26,480 --> 00:16:30,840
this. You're able to see things for what they are and not get carried away by

151
00:16:30,840 --> 00:16:46,520
them. Many people use this to overcome states of mental illness.

152
00:16:46,520 --> 00:16:52,760
I use this term broadly. I've gotten some negative comments about using this term

153
00:16:52,760 --> 00:16:57,760
for things like depression, anxiety, insomnia and so on. The point of view of

154
00:16:57,760 --> 00:17:03,880
Buddhism, any state of mind which causes suffering is mental illness. We don't

155
00:17:03,880 --> 00:17:12,920
separate. I think that negative criticism has come because people have some

156
00:17:12,920 --> 00:17:17,400
stigma attached to this word, mental illness that it's only for those serious

157
00:17:17,400 --> 00:17:22,840
conditions but serious or severe or not severe, serious or not serious, it's

158
00:17:22,840 --> 00:17:28,040
the same condition. The same condition that leads people to kill themselves or

159
00:17:28,040 --> 00:17:33,680
to hurt or kill other people is present in all of us to a certain degree.

160
00:17:33,680 --> 00:17:38,920
It's the same state just on a smaller level. We can call these all mental

161
00:17:38,920 --> 00:17:46,000
illnesses. Means we're ill, we're not well. So every time we get depressed you

162
00:17:46,000 --> 00:17:49,760
could call it. That's all I mean. It's a mental illness. It's a state which is

163
00:17:49,760 --> 00:17:54,400
not well. If anyone were to stay that someone who's depressed as well, I would

164
00:17:54,400 --> 00:18:01,000
say that they're not correct. This person has an illness even though it might

165
00:18:01,000 --> 00:18:11,760
just be temporary. People use meditation to deal with all of these states of

166
00:18:11,760 --> 00:18:16,480
depression, states of anxiety, states of fear, states of nervousness, states of

167
00:18:16,480 --> 00:18:29,160
insomnia, states of extreme boredom and apathy and so on, states of lethargy

168
00:18:29,160 --> 00:18:34,120
and laziness. All of these states which we don't want and we don't like but

169
00:18:34,120 --> 00:18:40,440
they keep coming back again and again to us. And it's amazing once you start

170
00:18:40,440 --> 00:18:46,640
immeditating how easy it is really to start to tackle these emotions and

171
00:18:46,640 --> 00:19:00,680
incredible how off-track we are with our obsession with physical cure such as

172
00:19:00,680 --> 00:19:10,720
medication or exercise. The obvious cures that are often prescribed by

173
00:19:10,720 --> 00:19:21,480
therapists or psychotherapists or psychologists is to take medication or

174
00:19:21,480 --> 00:19:26,080
to exercise or do something physical. Change the physical. They'll often say

175
00:19:26,080 --> 00:19:30,600
it's a chemical imbalance in the brain and so on. And while certainly this

176
00:19:30,600 --> 00:19:35,880
can be true that the brain, this is certainly true that the brain has some

177
00:19:35,880 --> 00:19:40,760
chemicals in it that become balanced or imbalanced but it's not until the

178
00:19:40,760 --> 00:19:48,520
mind takes these up that it really becomes a problem. That's how we react to the

179
00:19:48,520 --> 00:19:57,200
the level of chemicals in the brain. If we react in a negative way then we get

180
00:19:57,200 --> 00:20:03,360
upset. If we react in a positive way then we become attached to it, attached to

181
00:20:03,360 --> 00:20:15,920
the experience. So meditations is the perfect way to overcome these

182
00:20:15,920 --> 00:20:21,880
states. When we just sit down and become aware of them we just say to ourselves

183
00:20:21,880 --> 00:20:27,520
depressed. You have to practice it see you have to get good at it. This is why

184
00:20:27,520 --> 00:20:31,920
we do these exercises like sitting in meditation for 10 minutes, 20 minutes,

185
00:20:31,920 --> 00:20:37,240
30 minutes, one hour or so on. Because we're training ourselves to have a

186
00:20:37,240 --> 00:20:41,080
sharp mind so that when these things do come we can pierce them and see them

187
00:20:41,080 --> 00:20:45,600
clearly. Really see them for what they are. And when you're able to do that then

188
00:20:45,600 --> 00:20:48,800
when you feel depressed you just say to yourself depressed and you just know

189
00:20:48,800 --> 00:20:56,520
it for what it is. You know to yourself I'm depressed. This is depression. And

190
00:20:56,520 --> 00:20:59,440
whilst for most of us that would be devastating. It would be like well the what

191
00:20:59,440 --> 00:21:06,960
do I do about it? What's next? What's the next step? We go crazy. Which we do

192
00:21:06,960 --> 00:21:11,360
for almost anything. We got these chemicals going on in our brain going up, going

193
00:21:11,360 --> 00:21:15,520
down and oh now they're up. Oh I have to chase after it. Oh now they're down. I

194
00:21:15,520 --> 00:21:21,120
have to do something drastic or even kill myself.

195
00:21:24,760 --> 00:21:28,200
And all it takes is a little bit of meditation and you start to see oh yeah

196
00:21:28,200 --> 00:21:37,440
sometimes down sometimes up sometimes down sometimes up down up down. So I mean

197
00:21:37,440 --> 00:21:43,440
really the answer is so what's the difference? The difference is all in our

198
00:21:43,440 --> 00:21:49,560
minds. Well I don't like being upset. Well yeah that's the problem. There's

199
00:21:49,560 --> 00:21:52,280
nothing wrong with being upset. It's the problem that you don't like it. The

200
00:21:52,280 --> 00:21:55,840
problem is that we have this stigma attached to these things. That's really the

201
00:21:55,840 --> 00:22:01,800
problem. If we were to just say oh now I'm upset. Oh now I'm angry. Then it

202
00:22:01,800 --> 00:22:09,920
would it would fizzle out by itself. It eventually disappears anyway. We forget

203
00:22:09,920 --> 00:22:12,800
that these things just come and go over their own nature and we start to think

204
00:22:12,800 --> 00:22:16,760
boy I bet I can do something about that or boy I got to do something about that.

205
00:22:16,760 --> 00:22:25,800
Got to fix it. You can't fix it. It's part of nature. And so this is this is

206
00:22:25,800 --> 00:22:32,920
really the most gratifying sphere in which Buddhism is able to help the

207
00:22:32,920 --> 00:22:39,400
world. Especially today where they say half the people are on some half the

208
00:22:39,400 --> 00:22:44,720
people in the world or in the I don't know or in the Western world or in the

209
00:22:44,720 --> 00:22:53,400
developed world are on some kind of medication for a mental problem which is

210
00:22:53,400 --> 00:22:58,520
incredible to hear. I can imagine that it's actually that high but there's an

211
00:22:58,520 --> 00:23:07,240
incredible number of people taking physical receiving physical therapy for

212
00:23:07,240 --> 00:23:14,080
a mental condition. The state which the body can never hope to cure. You can

213
00:23:14,080 --> 00:23:18,840
try to balance out the chemical so they never go out of balance but just one

214
00:23:18,840 --> 00:23:22,000
wrong step in there out of balance again. I've seen people taking these

215
00:23:22,000 --> 00:23:26,600
medication tell me that they think they're gonna kill themselves on the

216
00:23:26,600 --> 00:23:32,760
medication because the balance is so delicate. When you put this aside and stop

217
00:23:32,760 --> 00:23:36,720
worrying about chemicals in the brain and so on. Just worry about how you

218
00:23:36,720 --> 00:23:41,320
respond to them. It can be actually fun when you look at these chemicals

219
00:23:41,320 --> 00:23:47,320
and see oh look at the chemicals going on because you can feel that right. You

220
00:23:47,320 --> 00:23:52,000
can feel the chemicals over there changing. The same is with lust when you feel

221
00:23:52,000 --> 00:23:59,240
these hormones coming on and oh now there's these hormones and you just see

222
00:23:59,240 --> 00:24:04,760
them for what they are. I mean big deal. It's just a body bodily sensation.

223
00:24:04,760 --> 00:24:15,680
It's a bodily chemical reaction. And all of this all of the ways that we use

224
00:24:15,680 --> 00:24:20,200
Buddhism whether it be in our daily life when we're walking around just knowing

225
00:24:20,200 --> 00:24:24,200
that we're walking and talking to people being aware that we're talking or

226
00:24:24,200 --> 00:24:31,400
listening. Or if we actually using it to solve some kind of problem in our lives

227
00:24:31,400 --> 00:24:36,200
with other people or with ourselves or internally with ourselves. All of these

228
00:24:36,200 --> 00:24:42,200
are leading somewhere. The most important thing that Buddhism has to offer to

229
00:24:42,200 --> 00:24:46,960
society is not this. It's not simply that it's gonna make us good citizens or

230
00:24:46,960 --> 00:24:52,720
good people in the world that it's going to start to take us out of our routine

231
00:24:52,720 --> 00:24:59,400
or take us out of our rut. And we're gonna be able to see things in a way that

232
00:24:59,400 --> 00:25:03,000
we didn't see them before. We're able to understand things in a way that we

233
00:25:03,000 --> 00:25:06,000
didn't understand them before. And this is gonna lead us somewhere. It's not

234
00:25:06,000 --> 00:25:08,400
just gonna be oh well that's good you know I practiced a little bit of

235
00:25:08,400 --> 00:25:15,240
meditation. Now I'm I'm a Buddhist. It's gonna lead us on. And so the funny

236
00:25:15,240 --> 00:25:19,440
thing about this is you know okay it's like pulling a thread on your sweater.

237
00:25:19,440 --> 00:25:26,680
You got this knitted sweater and you pull one of the loose yarn and suddenly

238
00:25:26,680 --> 00:25:30,320
it just all starts to unravel. And this is really what happens in Buddhism

239
00:25:30,320 --> 00:25:36,000
that really you have to pull everything out. And in the end it's kind of a

240
00:25:36,000 --> 00:25:40,400
problem for many people. They don't know how they're going to juggle their

241
00:25:40,400 --> 00:25:48,320
complicated lives with something so simple as Buddhism as the Buddhist

242
00:25:48,320 --> 00:25:53,160
teaching. And I think this is probably what leads people that say that oh

243
00:25:53,160 --> 00:25:58,520
Buddhism is not practical and modern times or so. It's an ancient thing for

244
00:25:58,520 --> 00:26:02,720
people living in the forest. But that's really not true. I mean the problem is

245
00:26:02,720 --> 00:26:07,080
not Buddhism. The problem is the way we live our lives. And we have to have this.

246
00:26:07,080 --> 00:26:12,800
We have to have that living beyond our means most of the time. And it's not to

247
00:26:12,800 --> 00:26:16,640
say that we're going to take Buddhism to the the Buddha's teaching to it's

248
00:26:16,640 --> 00:26:26,040
ultimate goal which is perfect freedom from suffering. But it does mean that

249
00:26:26,040 --> 00:26:29,280
we're going to have to probably change many things about the way we live our

250
00:26:29,280 --> 00:26:33,880
lives if we want to get any real progress. Sometimes we'll have to take time out

251
00:26:33,880 --> 00:26:41,520
of our work, take time away from our family. We have to make time and really do

252
00:26:41,520 --> 00:26:47,640
something to change the way we look at the world, change who we are, change how

253
00:26:47,640 --> 00:26:56,240
we respond to things. And for this it's it's going to take something more than

254
00:26:56,240 --> 00:27:12,320
just daily daily practice once in a while. So so this is why we do things like

255
00:27:12,320 --> 00:27:23,000
practicing meditation in a retreat setting. And we take time to go for sometimes

256
00:27:23,000 --> 00:27:29,840
even three days people will take off. We like to have people come for 10 days or

257
00:27:29,840 --> 00:27:38,120
even 20 days. If we do a basic meditation course we're often teaching for two or

258
00:27:38,120 --> 00:27:41,800
three weeks before the person where we can let the person go and say okay you've

259
00:27:41,800 --> 00:27:46,320
got a good foundation now. That's generally what it takes for people is two or

260
00:27:46,320 --> 00:27:53,920
three weeks. And the reason we're doing this is we're really trying to go the

261
00:27:53,920 --> 00:28:00,240
next step or go get something out of it. And it's you just get to this point in

262
00:28:00,240 --> 00:28:08,680
your lives that you just say I can't keep doing this. I can't just you know

263
00:28:08,680 --> 00:28:12,520
make two steps forward one step back or so in three steps forward two steps

264
00:28:12,520 --> 00:28:18,920
back. If I keep doing this all the time I'm never going to get anywhere. And it's

265
00:28:18,920 --> 00:28:22,160
not that there's anything wrong with this kind of practice. It's that the

266
00:28:22,160 --> 00:28:28,240
mind starts to see this incredible benefit in the practice. And so you just

267
00:28:28,240 --> 00:28:32,680
start saying to yourself wow this is really worth my attention. This is really

268
00:28:32,680 --> 00:28:38,680
something that I have to put more attention into. It's like prioritizing

269
00:28:38,680 --> 00:28:44,920
things. You start this new thing which you've taken up meditation Buddhism it's

270
00:28:44,920 --> 00:28:48,960
just so profound that all of the other things which you thought were worthwhile

271
00:28:48,960 --> 00:28:56,240
start to seem kind of pale in comparison. And so people do take the time off

272
00:28:56,240 --> 00:29:03,560
take the time out and set themselves in a retreat setting. Sign themselves up

273
00:29:03,560 --> 00:29:11,400
find a way to be with a teacher in a monastery or so on to do a course. And this

274
00:29:11,400 --> 00:29:17,920
is really what Buddhism has to offer it has to offer these these courses training

275
00:29:17,920 --> 00:29:26,200
courses which leave you in a much better state than when you started. We we

276
00:29:26,200 --> 00:29:30,680
often go into these retreats with scattered mind and unable to sit and

277
00:29:30,680 --> 00:29:34,080
thinking that we're never going to make it through the course doubting ourselves

278
00:29:34,080 --> 00:29:42,840
maybe even doubting the path and the practice. And it's just like taking a

279
00:29:42,840 --> 00:29:49,600
swim in the ocean where it just gets deeper and deeper and deeper. Your mind

280
00:29:49,600 --> 00:29:54,920
just gets deeper and deeper slowly training yourself seeing things that you

281
00:29:54,920 --> 00:29:59,200
never you never even thought you'd see. You know you have this idea that Buddhism

282
00:29:59,200 --> 00:30:02,000
is going to bring you wisdom and understanding and freedom from suffering but

283
00:30:02,000 --> 00:30:07,360
it's just amazes you it's it's like a flash of light these things that you

284
00:30:07,360 --> 00:30:11,200
start to see start to realize. It's like it didn't even realize that it was

285
00:30:11,200 --> 00:30:15,520
going to that wasn't even what you were looking for. It's like you were

286
00:30:15,520 --> 00:30:21,560
looking for a couple of pennies or a couple of dollars and it's like you won

287
00:30:21,560 --> 00:30:30,080
the lottery or or whatever you know it's looking for some stones and you find

288
00:30:30,080 --> 00:30:41,600
gold and it really changes who we are it can change people from white from

289
00:30:41,600 --> 00:30:47,280
black to white turn them into a whole different person. Often to the point

290
00:30:47,280 --> 00:30:55,680
where you have to change even the people you associate with. Sometimes people

291
00:30:55,680 --> 00:31:02,000
finish the course and they're no longer able to be compatible with the people

292
00:31:02,000 --> 00:31:07,680
who they they used to be compatible with. People who were engaged in things

293
00:31:07,680 --> 00:31:14,000
which now seem immoral or useless or futile or detrimental to one's

294
00:31:14,000 --> 00:31:20,160
spiritual health and you talk to those people and try to convince them but

295
00:31:20,160 --> 00:31:26,240
they're not interested. They're not in the same path as you and you have to

296
00:31:26,240 --> 00:31:31,360
understand everyone's all different. We can't expect that everyone you know

297
00:31:31,360 --> 00:31:35,280
they're just going to take up the practice just the way we did we're all at

298
00:31:35,280 --> 00:31:41,920
different places in our lives and so we have to change not only the things

299
00:31:41,920 --> 00:31:46,320
in the situation that we live and we also have to sometimes change even the

300
00:31:46,320 --> 00:31:53,120
people we associate with. It really just changes who we are changes many things

301
00:31:53,120 --> 00:31:58,080
about who we are and and not in some dogmatic or religious way it's you know

302
00:31:58,080 --> 00:32:05,000
seeing that our actions are incompatible with our goals our our actions and

303
00:32:05,000 --> 00:32:09,280
our goals even before we practice are incompatible we have these goals and

304
00:32:09,280 --> 00:32:13,600
we're acting in ways that are incompatible with them and by goals here the

305
00:32:13,600 --> 00:32:18,880
obvious one is happiness. We all have the goal to be happy no matter what else

306
00:32:18,880 --> 00:32:26,480
we want to do we we all want happiness. We all want to be free from suffering

307
00:32:26,480 --> 00:32:33,920
peaceful and happy and yet we do these things all the time we're engaged in

308
00:32:33,920 --> 00:32:38,480
things which only lead to our suffering only lead to our detriment only lead to

309
00:32:38,480 --> 00:32:45,920
our sadness and unhappiness and so this is why we have to change not because of

310
00:32:45,920 --> 00:32:54,240
any Buddhist or dogmatic thing it's because we see that these things are

311
00:32:54,240 --> 00:32:59,120
we're acting in a contrary in a way contrary to our best interest and the

312
00:32:59,120 --> 00:33:04,400
interests of anyone really we're hurting ourselves and we're hurting

313
00:33:04,400 --> 00:33:10,720
other people and when we see this this is this is what makes us want to change

314
00:33:10,720 --> 00:33:16,640
so for anyone who says that Buddhism is something that is I would

315
00:33:16,640 --> 00:33:21,760
dated or so and I think that this this is just shows that we don't

316
00:33:21,760 --> 00:33:27,360
understand Buddhism or perhaps it shows that the person has a very strong

317
00:33:27,360 --> 00:33:33,520
attachment to a certain way of life which is incompatible with reality or

318
00:33:33,520 --> 00:33:39,280
incompatible with true happiness and peace and so they're they're set on

319
00:33:39,280 --> 00:33:46,720
causing suffering for themselves and others so this is just a brief talk on

320
00:33:46,720 --> 00:33:53,440
some of the ways that Buddhism is able to help us and

321
00:33:53,440 --> 00:33:57,360
what Buddhism really has to offer for modern times

322
00:33:57,360 --> 00:34:02,160
along with an understanding of where true Buddhism comes from and that is

323
00:34:02,160 --> 00:34:05,120
I think in one way you could say it comes from within us

324
00:34:05,120 --> 00:34:09,920
that we don't have to think of Buddhism as Indian or even religious or

325
00:34:09,920 --> 00:34:17,520
anything simply means to know to see to understand things as they are

326
00:34:17,520 --> 00:34:21,280
so that's the talk for today I'd like to thank everyone for tuning in

327
00:34:21,280 --> 00:34:37,200
and hope to come to have y'all here next time that's all for now have a good day

