1
00:00:00,000 --> 00:00:08,000
Where to place observation while watching the stomach during meditation?

2
00:00:08,000 --> 00:00:13,000
Where to place observer?

3
00:00:13,000 --> 00:00:17,000
What are you watching while you're just watching the stomach?

4
00:00:17,000 --> 00:00:22,000
An easy way is to lie on your back if it's really painful to do this

5
00:00:22,000 --> 00:00:26,000
because our bodies are so stressed out and tense,

6
00:00:26,000 --> 00:00:29,000
lie down on your back just to confirm it,

7
00:00:29,000 --> 00:00:32,000
and you'll see it can be quite surprising for people.

8
00:00:32,000 --> 00:00:36,000
People will come to ask for advice and say,

9
00:00:36,000 --> 00:00:38,000
I really can't do the rising and falling.

10
00:00:38,000 --> 00:00:40,000
It just doesn't happen for me.

11
00:00:40,000 --> 00:00:41,000
When you explain to them,

12
00:00:41,000 --> 00:00:44,000
this is because your body is in an unnatural state,

13
00:00:44,000 --> 00:00:47,000
and why should I believe you?

14
00:00:47,000 --> 00:00:49,000
Have them lie down on their back and suddenly boom,

15
00:00:49,000 --> 00:00:52,000
their stomach goes like a baby.

16
00:00:52,000 --> 00:00:56,000
It's like magic all of a sudden because then they're relaxed

17
00:00:56,000 --> 00:00:58,000
and so you're able to show them that

18
00:00:58,000 --> 00:01:01,000
it's only because of your stress and tension that it's difficult.

19
00:01:01,000 --> 00:01:05,000
The rising and falling, it's part of the problem

20
00:01:05,000 --> 00:01:08,000
why nowadays it's so much...

21
00:01:12,000 --> 00:01:14,000
What's the word?

22
00:01:14,000 --> 00:01:17,000
People don't really like this watching the stomach thing,

23
00:01:17,000 --> 00:01:23,000
it wouldn't have been the case in times gone by

24
00:01:23,000 --> 00:01:26,000
when we would naturally breathe through the stomach

25
00:01:26,000 --> 00:01:28,000
and it isn't the case for people living in the forest

26
00:01:28,000 --> 00:01:32,000
who live simple, peaceful lives because they breathe naturally.

27
00:01:32,000 --> 00:01:35,000
Like a baby, they'll breathe from the abdomen.

28
00:01:35,000 --> 00:01:39,000
It's quite a natural thing.

29
00:01:39,000 --> 00:01:42,000
You just observe that movement,

30
00:01:42,000 --> 00:01:45,000
but maybe the reason why you're asking the question

31
00:01:45,000 --> 00:01:47,000
is because you're not able to observe it

32
00:01:47,000 --> 00:01:48,000
and it's kind of confusing,

33
00:01:48,000 --> 00:01:50,000
so I'm trying lying on your back

34
00:01:50,000 --> 00:01:53,000
is a good way to start until eventually

35
00:01:53,000 --> 00:01:54,000
you can do it sitting up.

36
00:01:54,000 --> 00:01:56,000
You can also, of course, put your hand on the stomach

37
00:01:56,000 --> 00:01:58,000
which helps you in the beginning.

38
00:01:58,000 --> 00:02:00,000
Part of it is going to be going through

39
00:02:00,000 --> 00:02:02,000
the stage of feeling the tension.

40
00:02:02,000 --> 00:02:04,000
Eventually the tension will come up

41
00:02:04,000 --> 00:02:06,000
in your shoulders, in your back

42
00:02:06,000 --> 00:02:09,000
and you'll be able to see what the real problem is.

43
00:02:09,000 --> 00:02:11,000
I'm sitting still so long the tension builds up

44
00:02:11,000 --> 00:02:12,000
and builds up

45
00:02:12,000 --> 00:02:16,000
and then as you acknowledge the tension, tens, tens,

46
00:02:16,000 --> 00:02:19,000
the body releases and lets go

47
00:02:19,000 --> 00:02:21,000
and then you'll find that actually the stomach

48
00:02:21,000 --> 00:02:24,000
is much easier to be mindful of

49
00:02:24,000 --> 00:02:27,000
and also as you stop forcing the stomach

50
00:02:27,000 --> 00:02:29,000
and start letting it rise and fall as it will

51
00:02:29,000 --> 00:02:50,000
and it becomes easier to acknowledge.

