1
00:00:00,000 --> 00:00:08,000
My next question, why don't modern day our hands let themselves be scientifically tested to show the effect of meditation?

2
00:00:08,000 --> 00:00:10,000
What are we going to have here?

3
00:00:14,000 --> 00:00:18,000
To show the effects of meditation on the mind.

4
00:00:18,000 --> 00:00:22,000
Well, I heard that my teacher...

5
00:00:22,000 --> 00:00:34,000
First of all, how do we know who is in our hands around today?

6
00:00:34,000 --> 00:00:44,000
Very difficult to know whether there are any or where they are, they might be off in the forest.

7
00:00:44,000 --> 00:00:53,000
Meditating happily and peacefully and with no cares.

8
00:00:53,000 --> 00:00:58,000
And enlightened being.

9
00:00:58,000 --> 00:01:06,000
But I heard a story that my teacher went to have himself set up with an EKG.

10
00:01:06,000 --> 00:01:10,000
Or some sort of brainwave scan.

11
00:01:10,000 --> 00:01:13,000
And I've seen various meditators do these things.

12
00:01:13,000 --> 00:01:18,000
When they hooked him up to it and he had sat in meditation, they couldn't find a brainwave.

13
00:01:18,000 --> 00:01:21,000
This is what I heard.

14
00:01:21,000 --> 00:01:24,000
So, I mean, I'm not saying anything because of that.

15
00:01:24,000 --> 00:01:31,000
But except that my teacher has apparently been asked and submitted to it.

16
00:01:31,000 --> 00:01:37,000
So, I don't think it's unheard of of experienced meditators submitting themselves to that.

17
00:01:37,000 --> 00:01:39,000
And I don't think there's anything wrong with it.

18
00:01:39,000 --> 00:01:43,000
It's been an interesting study.

19
00:01:43,000 --> 00:01:48,000
And of course, they've done that with Tibetan Buddhists and they've found that true enough.

20
00:01:48,000 --> 00:01:52,000
All that development of compassion has made them more compassionate.

21
00:01:52,000 --> 00:02:02,000
All of those years of compassion has made them more compassionate, which is what you'd expect.

22
00:02:02,000 --> 00:02:09,000
But, you know, there was this question of this question was asked.

23
00:02:09,000 --> 00:02:17,000
There was this bet post over this man who promised a million dollars or some huge amount of money to anyone who could show him the psychic power.

24
00:02:17,000 --> 00:02:23,000
And he still hasn't had to pay up.

25
00:02:23,000 --> 00:02:26,000
So, I don't know what that says about psychic powers.

26
00:02:26,000 --> 00:02:32,000
But in Buddhism, it's considered Buddhist monks aren't allowed to show their psychic powers.

27
00:02:32,000 --> 00:02:38,000
So, it's against our rules to do it in the first place for a million or a billion or however much money.

28
00:02:38,000 --> 00:02:43,000
And so, from a Buddhist point of view that the answer to why no one has come forth is pretty easy.

29
00:02:43,000 --> 00:02:49,000
Why no Buddhist monk has come forth is pretty easy because, well, first of all, they're not allowed to.

30
00:02:49,000 --> 00:02:52,000
And second of all, that money is nothing to them.

31
00:02:52,000 --> 00:02:55,000
Money is something that they've decided to give up.

32
00:02:55,000 --> 00:03:06,000
But as to why a person wouldn't come forth and prove to people is because of the result of it.

33
00:03:06,000 --> 00:03:15,000
That this sort of bragging or boasting about your attainments doesn't make people more interested in meditation.

34
00:03:15,000 --> 00:03:21,000
It makes people more interested in the arahant and it brings great fame and renowned to the person.

35
00:03:21,000 --> 00:03:33,000
The Buddha called it, compared it to a prostitute or a, what do you call it, a dance dancing.

36
00:03:33,000 --> 00:03:38,000
The strip tease person who shows their private parts for money.

37
00:03:38,000 --> 00:03:40,000
This is how the Buddha compared it.

38
00:03:40,000 --> 00:03:46,000
So, he said, this is not something that we should encourage in people.

39
00:03:46,000 --> 00:03:49,000
That's not something that really helps.

40
00:03:49,000 --> 00:03:55,000
The important thing is to encourage those people who want to practice, encourage them in meditation.

41
00:03:55,000 --> 00:04:02,000
And the other thing is we don't go looking or trying to bring people to the practice who aren't really interested in it.

42
00:04:02,000 --> 00:04:07,000
We put it out there and say this practice exists and let people come to it.

43
00:04:07,000 --> 00:04:18,000
It doesn't help to show off or to put yourself out there and make a big scene about it.

44
00:04:18,000 --> 00:04:25,000
So, I can't see it really helping or coming to an arahant that I should do this for the benefit of Buddhism.

45
00:04:25,000 --> 00:04:27,000
Because it's not really for the benefit.

46
00:04:27,000 --> 00:04:36,000
It just makes people who are not really keen on practice, come and get involved and, you know, brings a lot of fame and gain and so on.

47
00:04:36,000 --> 00:04:39,000
And therefore, a lot of people who are interested in fame and gain.

48
00:04:39,000 --> 00:04:44,000
So, I don't see it as something that an arahant and the other thing is arahant of no desires.

49
00:04:44,000 --> 00:04:46,000
So, what would it be to them?

50
00:04:46,000 --> 00:04:53,000
It sounds kind of weird, like, contradictive that an arahant would go out and do that.

51
00:04:53,000 --> 00:04:54,000
See, see?

52
00:04:54,000 --> 00:04:59,000
Well, the other thing is an arahant will never try to, a true arahant.

53
00:04:59,000 --> 00:05:04,000
Mahasim Sayyada gave this great, he wrote this great passage about the arahant.

54
00:05:04,000 --> 00:05:09,000
And he said, an arahant, anyone who calls themselves an arahant, it's very hard to believe that they could be an arahant.

55
00:05:09,000 --> 00:05:13,000
Because an arahant doesn't have any interest in letting people know that they're an arahant.

56
00:05:13,000 --> 00:05:20,000
They want to show everyone, in fact, an arahant will be very much keep it to themselves.

57
00:05:20,000 --> 00:05:22,000
And you see this carried out in the scriptures.

58
00:05:22,000 --> 00:05:23,000
There's stories about this.

59
00:05:23,000 --> 00:05:30,000
So, whether they're true or not, but if you go by the scriptures, you can see many stories as for people I would ask monks.

60
00:05:30,000 --> 00:05:32,000
And the monks would always equivocate.

61
00:05:32,000 --> 00:05:34,000
Even there was this laywoman.

62
00:05:34,000 --> 00:05:42,000
I think we gave a Dhammapada story about her who became an anagami.

63
00:05:42,000 --> 00:05:48,000
And this monk came and asked her, so are you the woman who can read people's minds?

64
00:05:48,000 --> 00:05:50,000
And she wouldn't answer him.

65
00:05:50,000 --> 00:05:52,000
Can you read my mind or something?

66
00:05:52,000 --> 00:05:55,000
And she said, oh, why do you ask that?

67
00:05:55,000 --> 00:05:57,000
Well, you've given me everything.

68
00:05:57,000 --> 00:05:58,000
I want everything.

69
00:05:58,000 --> 00:06:00,000
I want you prepared for me.

70
00:06:00,000 --> 00:06:04,000
And she said, oh, people prepare things who haven't become, who haven't gained magical powers.

71
00:06:04,000 --> 00:06:05,000
People prepare the right things.

72
00:06:05,000 --> 00:06:07,000
She said, well, I don't care about those people.

73
00:06:07,000 --> 00:06:08,000
I'm asking you.

74
00:06:08,000 --> 00:06:11,000
And still she wouldn't answer him.

75
00:06:11,000 --> 00:06:18,000
So, this is clearly from the Orthodox response.

76
00:06:18,000 --> 00:06:21,000
This is not the case.

77
00:06:21,000 --> 00:06:27,000
An enlightened being would never talk or even hint about their attainments.

78
00:06:27,000 --> 00:06:31,000
They would act as an example to people.

79
00:06:31,000 --> 00:06:36,000
But I can see in our hand if people wanted to hook them up to an EKG.

80
00:06:36,000 --> 00:06:38,000
I can see in our hand doing that.

81
00:06:38,000 --> 00:06:41,000
It's like, yeah, okay.

82
00:06:41,000 --> 00:06:47,000
I don't think they would necessarily refuse or so on if people came to them.

83
00:06:47,000 --> 00:07:11,000
Anyway.

