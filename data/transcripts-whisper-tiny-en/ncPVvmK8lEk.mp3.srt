1
00:00:00,000 --> 00:00:04,340
Hello, good evening.

2
00:00:04,340 --> 00:00:11,080
So today I'll be answering a question about the Satipatana Sutta.

3
00:00:11,080 --> 00:00:18,040
It's the discourse on mindfulness.

4
00:00:18,040 --> 00:00:24,880
In the Satipatana Sutta it's broken up into sections and at the end of each section there's

5
00:00:24,880 --> 00:00:33,480
a repeated phrase and the Buddha says several very important things.

6
00:00:33,480 --> 00:00:39,440
It's important that we don't gloss over or we don't skip over what the Buddha says

7
00:00:39,440 --> 00:00:45,000
about the practice of mindfulness.

8
00:00:45,000 --> 00:00:50,040
At the end of each section so we have the four foundations of mindfulness, body, feelings,

9
00:00:50,040 --> 00:00:54,520
mind, dhamma, kaya, vidana, jita, dhamma.

10
00:00:54,520 --> 00:01:07,000
And then kaya and dhamma are split up into sections.

11
00:01:07,000 --> 00:01:17,240
And one part of the ending of each section says that, yeah, at the beginning actually

12
00:01:17,240 --> 00:01:26,880
words says itya jita dhamma, kaya, kaya, nupasi, and so on.

13
00:01:26,880 --> 00:01:29,680
Adjita dhamma means internally.

14
00:01:29,680 --> 00:01:41,720
Thus one is mindful internally and then he says bhiddhava or externally and then he says

15
00:01:41,720 --> 00:01:49,760
internally or externally or internally and externally.

16
00:01:49,760 --> 00:01:56,840
And so the question came when it's been asked before, what is meant by internally and

17
00:01:56,840 --> 00:01:59,080
externally?

18
00:01:59,080 --> 00:02:03,120
How do you be mindful of something internally and how do you be mindful of something externally?

19
00:02:03,120 --> 00:02:05,880
What's the difference?

20
00:02:05,880 --> 00:02:10,640
Most puzzling is how you would be mindful of something externally.

21
00:02:10,640 --> 00:02:16,560
Based on our practice, it's very much internal.

22
00:02:16,560 --> 00:02:23,680
Based on most people's understanding of meditation, it's the inner way, the inward path.

23
00:02:23,680 --> 00:02:29,440
So the idea that you could somehow meditate on something outside of yourself or be mindful

24
00:02:29,440 --> 00:02:34,920
at least of something outside of yourself is a bit confusing.

25
00:02:34,920 --> 00:02:44,400
So I can give three possible answers to how one can practice external mindfulness.

26
00:02:44,400 --> 00:02:50,320
And this is based a little bit on the commentaries, a little bit on different sutas and

27
00:02:50,320 --> 00:02:57,800
a little bit on my own understanding of the Buddha's teaching.

28
00:02:57,800 --> 00:03:09,280
The first way that one can practice mindfulness externally is in the practice of Yipasana.

29
00:03:09,280 --> 00:03:14,160
The second way someone can practice mindfulness of something externally is in the practice

30
00:03:14,160 --> 00:03:30,480
of samata and the third way that one can practice mindfulness of something that is externally

31
00:03:30,480 --> 00:03:34,120
is very in the practice of Yipasana.

32
00:03:34,120 --> 00:03:40,560
The first one in preparation for Vipasana is what I meant.

33
00:03:40,560 --> 00:03:46,480
So in preparation for Vipasana, in the practice of samata, or you could even say in preliminary

34
00:03:46,480 --> 00:03:52,080
practice of samata and in the actual practice of Yipasana.

35
00:03:52,080 --> 00:03:56,360
So let's play now this work.

36
00:03:56,360 --> 00:04:01,920
In preparation for the practice of Yipasana or in conjunction, what I meant by in the practice

37
00:04:01,920 --> 00:04:08,880
of samata is in the practice of Yipasana is as a support for one's practice of Yipasana.

38
00:04:08,880 --> 00:04:11,840
And this is what the sub-commentary actually says about this.

39
00:04:11,840 --> 00:04:19,600
It makes a note that actually this external idea isn't actually meditation at all.

40
00:04:19,600 --> 00:04:26,160
Or it says it can't lead to tranquility of mind, it can't lead to the focus and concentration

41
00:04:26,160 --> 00:04:30,040
that's needed to develop insight.

42
00:04:30,040 --> 00:04:35,240
But it's something that's useful in insight in a way that a lot of things are in the

43
00:04:35,240 --> 00:04:42,920
sutas and just in the practical reality, along our paths to enlightenment.

44
00:04:42,920 --> 00:04:49,200
So we have to differentiate between what is meditation, what is not meditation in regards

45
00:04:49,200 --> 00:04:51,160
in this regards.

46
00:04:51,160 --> 00:05:02,120
And by the sub-commentaries explanation, there's this idea that our reflections on external

47
00:05:02,120 --> 00:05:07,640
realities can often be quite valuable in our own practice of mindfulness.

48
00:05:07,640 --> 00:05:14,480
Even though it's not mindfulness in the same way, it's mindfulness in a way that allows

49
00:05:14,480 --> 00:05:22,360
us to understand and appreciate reality better and more clearly.

50
00:05:22,360 --> 00:05:28,680
So when you focus on someone else's body, you can focus on and each section has this.

51
00:05:28,680 --> 00:05:31,600
So you can focus on their breath.

52
00:05:31,600 --> 00:05:36,920
You can focus on their postures when they're walking, so watch someone else walk as

53
00:05:36,920 --> 00:05:42,040
they're walking walking.

54
00:05:42,040 --> 00:05:49,520
And you can focus on, of course, a dead body or someone else's parts of someone else's

55
00:05:49,520 --> 00:05:56,920
body, their hair and their nails and their teeth and their skin and so on.

56
00:05:56,920 --> 00:06:00,480
And some of this might sound quite weird, like why would you focus on someone else's breathing,

57
00:06:00,480 --> 00:06:02,920
why would you focus on someone else's walking?

58
00:06:02,920 --> 00:06:11,600
My idea in regards to this is that given that each, the end of each section is the same

59
00:06:11,600 --> 00:06:19,120
for each section and yet each section is a different sort of practice that in some ways

60
00:06:19,120 --> 00:06:29,480
one wants to say that the words are only meant to describe, and they do this quite well,

61
00:06:29,480 --> 00:06:34,720
to describe the general idea of how one is mindful.

62
00:06:34,720 --> 00:06:40,840
And so at the end of each section, he says, one is mindful externally, internally.

63
00:06:40,840 --> 00:06:49,080
Once he's the beginning, the arising and the ceasing of things, one sees that there is

64
00:06:49,080 --> 00:06:53,320
just the body, there is just the feelings, they are what they are.

65
00:06:53,320 --> 00:06:59,480
And so it describes how one is mindful, but it may very well be that some sections are much

66
00:06:59,480 --> 00:07:04,920
more useful when practiced externally, and that's granted that.

67
00:07:04,920 --> 00:07:09,760
It's a good point that some of the objects of meditation are internal, which is mostly

68
00:07:09,760 --> 00:07:15,200
what we do, of course, walking, walking, it really only makes sense if you're paying attention

69
00:07:15,200 --> 00:07:20,320
to your own movement because you can't feel someone else's walking.

70
00:07:20,320 --> 00:07:25,400
With the breath, when there are different kinds of breath meditation, with our breath meditation,

71
00:07:25,400 --> 00:07:28,720
it doesn't make sense that you should focus on someone else's rising and falling because

72
00:07:28,720 --> 00:07:32,040
you won't actually experience rising and falling.

73
00:07:32,040 --> 00:07:36,680
You won't experience the tension, you won't experience the fluidity.

74
00:07:36,680 --> 00:07:42,200
But in regarding a corpse, which is a part of the suta, it can be quite useful.

75
00:07:42,200 --> 00:07:48,480
Useful, let's say, for Vipasana, because if you're looking at someone else's dead body

76
00:07:48,480 --> 00:07:58,320
and bloated body and then flesh being rotted, maggots coming out of it, don't say that

77
00:07:58,320 --> 00:08:05,160
that's just a concept, that's quite powerful as a reflection, it sets a very good tone

78
00:08:05,160 --> 00:08:10,280
for your own practice, it leads to a revolution, makes you realize that actually this body

79
00:08:10,280 --> 00:08:16,720
is just like that, hey, there's bones in this body as well and so on.

80
00:08:16,720 --> 00:08:22,200
And even mindfulness of the parts of the body, if you look at someone else's, suppose

81
00:08:22,200 --> 00:08:27,360
there's someone who you're very much attracted to, you can focus on their hair and we

82
00:08:27,360 --> 00:08:32,080
think, oh, their hair is quite beautiful, we're going to hand some, but then you study

83
00:08:32,080 --> 00:08:37,720
their hair in a Buddhist way and you think, oh, yes, this hair of theirs is planted like

84
00:08:37,720 --> 00:08:45,280
rice in their scalp, which is not just mud and water, knowing the scalp there's blood

85
00:08:45,280 --> 00:08:52,920
and there's oil and there's flakes of skin and so on.

86
00:08:52,920 --> 00:08:59,520
And the hair doesn't smell like rice does, no, the hair smells like body, it smells

87
00:08:59,520 --> 00:09:02,360
putrid after some time.

88
00:09:02,360 --> 00:09:08,440
The nails, they turn yellow, they get dirt and skin and particles and all sorts of stuff

89
00:09:08,440 --> 00:09:14,840
underneath them, they get chipped and broken and so on, so you look at someone's beautiful

90
00:09:14,840 --> 00:09:21,640
nails and then you think about their nails in a different way, it can be very useful,

91
00:09:21,640 --> 00:09:26,000
not just for some of the meditation but also for repass in the meditation, it's not

92
00:09:26,000 --> 00:09:32,400
rebass in it, it's not mindfulness in the way that we narrowly understand it, but there's

93
00:09:32,400 --> 00:09:37,800
a mindfulness there, you're contemplating, you're reflecting in a way that's going to be

94
00:09:37,800 --> 00:09:44,040
quite useful and this is important to understand because our practice cannot be my

95
00:09:44,040 --> 00:09:51,680
opic in the way of just, you know, just dusting up a hot tub, just focusing on we pass

96
00:09:51,680 --> 00:09:57,480
and not seeing clearly, we have to also be aware of things in our life and relate to things

97
00:09:57,480 --> 00:10:04,760
in our lives and reflect, we have to make a clear delineation between actual mindfulness

98
00:10:04,760 --> 00:10:13,280
practice and mindful reflection, you know, where you say, Ajiram, what the yankakayo,

99
00:10:13,280 --> 00:10:18,760
this before long this body will lie on the earth just like that body there, that's

100
00:10:18,760 --> 00:10:25,640
a reflection, it's not actually being mindful of, you know, arising and ceasing phenomena,

101
00:10:25,640 --> 00:10:30,040
but it's an external reflection, focusing on the body and seeing the body dead and focusing

102
00:10:30,040 --> 00:10:35,320
on the body and seeing it ugly and so on.

103
00:10:35,320 --> 00:10:39,480
These things can be a great support for repass in a meditation and can actually be a real

104
00:10:39,480 --> 00:10:46,760
catalyst for true insight, there are cases in the sutas, there's a case of a monk who

105
00:10:46,760 --> 00:10:51,520
saw a woman walking down the street, but he had been so fixed on the contemplation of

106
00:10:51,520 --> 00:10:57,240
bones that all he saw was her teeth when she smiled when she laughed and when he saw

107
00:10:57,240 --> 00:11:02,960
her teeth, he just saw her, he reflected and he absorbed into that and saw this woman

108
00:11:02,960 --> 00:11:09,920
as just a second bones walking down the street and she walked past him and he kept walking

109
00:11:09,920 --> 00:11:16,920
on arms around and a man came and he said, hey have you seen my wife, it's chasing after

110
00:11:16,920 --> 00:11:25,040
some reason and he says, oh, I just saw a bag of bones, or I just saw a pile of bones,

111
00:11:25,040 --> 00:11:32,440
and he said, because that's all he actually saw and this monk when that happened, he

112
00:11:32,440 --> 00:11:37,520
became enlightened just by seeing the woman's teeth.

113
00:11:37,520 --> 00:11:41,240
Some people might say, how is it possible? I've been asked this several times, how is

114
00:11:41,240 --> 00:11:48,200
it possible that this woman's teeth he became enlightened? No, it's not directly possible

115
00:11:48,200 --> 00:11:53,320
that that should be enlightened and enlightening experience, but it's the reflection. It sets

116
00:11:53,320 --> 00:12:05,560
you in the mood, potentially perfect mood of seeing things just as they are, just the

117
00:12:05,560 --> 00:12:17,000
last straw that you needed to see clearly. So we have to separate between meditation

118
00:12:17,000 --> 00:12:22,760
and not meditation, but we also have to see that many times there's going to be some

119
00:12:22,760 --> 00:12:29,720
interaction with our non-meditative qualities. The sub-commentary seems to be saying that

120
00:12:29,720 --> 00:12:38,440
it's for the purpose of supporting insight.

121
00:12:38,440 --> 00:12:45,120
But external can very much be a support or a preliminary for summit to meditation. It often

122
00:12:45,120 --> 00:12:50,840
quite often is. In classical summit to meditation, you would focus on an external object

123
00:12:50,840 --> 00:12:57,040
like a disc or something. In the Saty bitanasu, you would be mindful of a dead body for

124
00:12:57,040 --> 00:13:06,440
the purpose of gaining this fixed concentration on the idea of the dead body. You would

125
00:13:06,440 --> 00:13:17,120
focus on one of the parts of the body and you would come to be absorbed in that hair

126
00:13:17,120 --> 00:13:23,920
or teeth or so on. And it could be an object of meditation that would lead to tranquility.

127
00:13:23,920 --> 00:13:37,080
But the third way that this external idea of external practice could be thought of as

128
00:13:37,080 --> 00:13:41,800
could be understood is in the actual practice of insight meditation. What I mean by this

129
00:13:41,800 --> 00:13:47,880
is externally and internally, if you're going to look at the arising and ceasing of things,

130
00:13:47,880 --> 00:13:55,880
then it has to be based on experience. And so this is the clue because externally and

131
00:13:55,880 --> 00:14:00,120
internally, as we normally think of them, have nothing to do with experience. They have

132
00:14:00,120 --> 00:14:10,480
to do with entities. And so understanding the difference between entities and experiences

133
00:14:10,480 --> 00:14:17,160
is important. So it's important to understand how this can be a practice of true mindfulness.

134
00:14:17,160 --> 00:14:25,160
So external entities, you have people and places and things and you see them. You see the

135
00:14:25,160 --> 00:14:30,880
person walking. Even if you think of it just as walking, it's a thing. And it's not actually

136
00:14:30,880 --> 00:14:36,440
an experience of walking. It's an experience of seeing. So if you talk about something

137
00:14:36,440 --> 00:14:44,920
external, it's an experience of seeing. It's an experience of conceiving. But in every

138
00:14:44,920 --> 00:14:53,960
experience, there is, in one way, there is an external and an internal. And that is in

139
00:14:53,960 --> 00:15:02,080
regards to the senses. So our practice of mindfulness involves external stimuli and internal

140
00:15:02,080 --> 00:15:08,840
sense basis and the contact between those two. No, you don't see anything unless there's

141
00:15:08,840 --> 00:15:14,440
light touching the eye. You don't hear anything unless you're sound touching the ear.

142
00:15:14,440 --> 00:15:20,960
And so the sound is external. The light is external. And these are, you know, it seems

143
00:15:20,960 --> 00:15:30,680
quite simple. But it describes a framework that is unlike how we normally perceive reality.

144
00:15:30,680 --> 00:15:36,600
Our normal perception of reality is people places and things. So understanding and looking

145
00:15:36,600 --> 00:15:45,760
at reality in terms of sights and the eye, sounds and the ear, smells and the nose, tastes

146
00:15:45,760 --> 00:15:54,200
and the tongue, feelings and the body and thoughts and the mind. It's something that the

147
00:15:54,200 --> 00:16:01,280
Buddha taught again and again and again, quite often. It's one of the most often taught

148
00:16:01,280 --> 00:16:07,040
teachings on the six senses, internal and external. And they're actually called that.

149
00:16:07,040 --> 00:16:18,080
There's the internal sense basis and the external sense objects. And so there's a pair

150
00:16:18,080 --> 00:16:25,000
of them. And so sometimes, and this is really very close to what the language of the Satyabhata

151
00:16:25,000 --> 00:16:31,960
understood. Sometimes, when sees the object and the light touching the eye, as it experiences

152
00:16:31,960 --> 00:16:41,120
that, sometimes someone is aware of it from internally of the eye seeing. It's just a perspective

153
00:16:41,120 --> 00:16:45,840
same with the, so sometimes someone is aware of the fact that the ear is receiving the sound.

154
00:16:45,840 --> 00:16:59,400
Sometimes sees it more as the sound that touches the ear. But understanding and seeing

155
00:16:59,400 --> 00:17:06,720
reality in terms of this framework gets you so much closer to what's really happening.

156
00:17:06,720 --> 00:17:13,160
It gets you to a state of existence, a state of being, a state of interaction with the

157
00:17:13,160 --> 00:17:20,440
world, that is perfect, that is clear, that is free from any kind of judgment, partiality

158
00:17:20,440 --> 00:17:28,560
or ignorance. It brings us closer to reality, brings us closer to what's really going on.

159
00:17:28,560 --> 00:17:36,080
Because our idea of seeing someone, seeing something is an abstraction. It's based first

160
00:17:36,080 --> 00:17:41,640
on the light touching the eye and second on the processing of it. And it leads third to

161
00:17:41,640 --> 00:17:50,200
the judgment and so on. Because experience is momentary, our sights and sounds and smells,

162
00:17:50,200 --> 00:17:59,960
they come and they go. And because they are simple, they are basic, they are fundamental

163
00:17:59,960 --> 00:18:06,840
building blocks. There's no possibility, there's no potential for clinging to them.

164
00:18:06,840 --> 00:18:12,800
When you're aware, when you're observing from the point of view of light touching the

165
00:18:12,800 --> 00:18:22,960
eye, there's no room for any attachment. And so this is, I think, the best way, it may

166
00:18:22,960 --> 00:18:29,120
not be what the Buddha actually meant when he said or what the words in each of these

167
00:18:29,120 --> 00:18:34,760
sections actually means this specific teaching. But it is a good opportunity for us to look

168
00:18:34,760 --> 00:18:43,320
at an in-repassing a meditation based on the four foundations of mindfulness. To look

169
00:18:43,320 --> 00:18:53,400
at how we perceive reality, how reality presents itself to us. And that is through these

170
00:18:53,400 --> 00:18:59,000
six senses. So when we say to ourselves, seeing, seeing, there's actually three things.

171
00:18:59,000 --> 00:19:04,160
There's the light, there's the eye, and there's the consciousness. Because even if there's

172
00:19:04,160 --> 00:19:09,280
light touching the eye, if your mind is somewhere else, you can't see. Even when they're

173
00:19:09,280 --> 00:19:15,480
sound touching the ear, when there's sound waves touching the ear, sometimes you're so

174
00:19:15,480 --> 00:19:23,240
focused, you don't even hear someone when they call you. Quite often we don't notice

175
00:19:23,240 --> 00:19:29,840
smells and tastes and feelings until they become unpleasant and then we realize when they

176
00:19:29,840 --> 00:19:37,720
become acute. So in reality, there are these three things. And together this is contact.

177
00:19:37,720 --> 00:19:41,560
And the Buddha talked about these again and again. And quite clear that all he was trying

178
00:19:41,560 --> 00:19:48,800
to do was describe reality so that we could know it when we saw it. And so that we could

179
00:19:48,800 --> 00:19:56,560
change the way we look at things. Like if someone looking at your car, for example, and

180
00:19:56,560 --> 00:20:04,040
they want to show you, they want to point out what's wrong with it so you can fix it.

181
00:20:04,040 --> 00:20:13,280
To show you a new way of looking at things. The way we look at things is problematic.

182
00:20:13,280 --> 00:20:24,280
It's imprecise. So it's not that seeing people in places and things is wrong. It's just

183
00:20:24,280 --> 00:20:33,680
that it's unable to provide the clarity that we need to change. If and when we become

184
00:20:33,680 --> 00:20:42,560
free from all wrongness, all imperfections in our mind, all bad habits, then we would

185
00:20:42,560 --> 00:20:50,600
be fine to focus on abstract entities. But because they're abstract, they leave room for

186
00:20:50,600 --> 00:20:57,280
misunderstanding. And when we have misunderstanding, this is where it gives rise to bad habits,

187
00:20:57,280 --> 00:21:06,960
bad ideas. So describing and illustrating and laying out all of the building blocks of

188
00:21:06,960 --> 00:21:14,320
reality and how the mind interacts with them in terms of the six senses internally and externally

189
00:21:14,320 --> 00:21:23,320
is a very good example. It's allows us to become closer to reality. So there you go. There's

190
00:21:23,320 --> 00:21:30,440
three ways you could understand it. One, it's useful to reflect on external things. Two, you

191
00:21:30,440 --> 00:21:37,200
take the external object as a basis for sound for tranquility. And three, it's nothing to do

192
00:21:37,200 --> 00:21:42,040
with external objects at all. It has to do with external aspects of experience. And that

193
00:21:42,040 --> 00:21:48,520
may very well be what the put is referring to here. Either way, all three are valid. And that's

194
00:21:48,520 --> 00:22:16,440
an answer to the question. So thank you for listening. I wish you all the best.

