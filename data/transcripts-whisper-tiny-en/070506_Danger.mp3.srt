1
00:00:00,000 --> 00:00:15,000
Normally before we practice together, I'll take the opportunity to give a short talk on

2
00:00:15,000 --> 00:00:21,080
the Buddhist teaching.

3
00:00:21,080 --> 00:00:36,080
It's important to have this sort of background on the Buddhist teaching before we start to practice.

4
00:00:36,080 --> 00:00:42,080
Because it's easy to say that we're practicing, we're easy to go off and say that you're practicing,

5
00:00:42,080 --> 00:00:52,080
but in the Buddhist teaching, we have what is right practice and what is called wrong practice.

6
00:00:52,080 --> 00:01:03,080
It's important that we always have this intellectual understanding first of what is the right path

7
00:01:03,080 --> 00:01:05,080
and what is the wrong path.

8
00:01:05,080 --> 00:01:11,080
So that when we do set out to practice, we can make sure that we are practicing on the right path.

9
00:01:11,080 --> 00:01:20,080
And we don't fall off onto the wrong path. If we do get lost on the wrong path, we'll be able to correct ourselves

10
00:01:20,080 --> 00:01:28,080
based on our understanding on an intellectual, intellectual level.

11
00:01:28,080 --> 00:01:40,080
So what I thought I would talk about today is a reason or reasons why we should practice meditation.

12
00:01:40,080 --> 00:01:50,080
The reasons why we should undertake the practice of the Buddhist teaching.

13
00:01:50,080 --> 00:02:04,080
And the best reason a reason which is most often given is that on the road ahead of us, there is great danger.

14
00:02:04,080 --> 00:02:11,080
We are faced with great danger in our individual futures.

15
00:02:11,080 --> 00:02:25,080
Each and every one of us has danger which we have to face until we can become safe and free from danger.

16
00:02:25,080 --> 00:02:38,080
And so the different differentiation between people is that some people see this danger and some people don't see the danger.

17
00:02:38,080 --> 00:02:54,080
So before we continue on with our lives, it's important that we understand the dangers which we are facing so that we can undertake to clear up or to free ourselves from these dangers.

18
00:02:54,080 --> 00:03:14,080
And the way we do this, of course, is by practicing to purify our minds and to remove from our minds all sorts of unwholesome, unskillful tendencies or mind states.

19
00:03:14,080 --> 00:03:24,080
So the Lord Buddha's teaching on danger, we have a great number of dangers ahead of us.

20
00:03:24,080 --> 00:03:31,080
It's important to get an understanding of these before we start practicing or before we continue on with our lives.

21
00:03:31,080 --> 00:03:44,080
And this gives us a good reason to live our lives in a very careful skillful or wholesome way.

22
00:03:44,080 --> 00:03:54,080
Because when we fall onto the wrong path, then these dangers come and turn into suffering for us.

23
00:03:54,080 --> 00:04:07,080
The first dangers we have the danger of birth, the danger of old age, the danger of sickness and the danger of death.

24
00:04:07,080 --> 00:04:13,080
This is the first set of dangers which we have ahead of us.

25
00:04:13,080 --> 00:04:25,080
But in this lifetime, there's no birth ahead of us. We've already met with birth. It's no longer a danger. It's now a reality. We have been born.

26
00:04:25,080 --> 00:04:37,080
And because of our birth, we have to face with all sorts of suffering and discomfort, which comes along with being human, which comes along with being born.

27
00:04:37,080 --> 00:04:50,080
But the dangers we still have ahead of us are old age, sickness and death. And these are dangers which, in this lifetime, we cannot avoid.

28
00:04:50,080 --> 00:05:08,080
But there's a danger for us in that if they catch us unaware or unprepared, then there's a great danger that we will suffer and become in great stress because of these things.

29
00:05:08,080 --> 00:05:33,080
So if we get old and we're not prepared for old age, we're not prepared to be old and bent and aching with all sorts of ailments, which come along with being old, having a poor memory, having rotting teeth and so on.

30
00:05:33,080 --> 00:05:42,080
False teeth, bent back, arthritis, and all the many ailments which come along with being old.

31
00:05:42,080 --> 00:05:57,080
If we're not prepared for these things, if we don't have a mind which is well trained and able to deal with such uncomfortable, undesirable circumstances, then this could be a great danger to us.

32
00:05:57,080 --> 00:06:10,080
This could be something which could cause great suffering. So it's a danger which, even in this lifetime, can be avoided, the danger of suffering because of old age.

33
00:06:10,080 --> 00:06:35,080
Poor suffering because of sickness. If we're not ready for cancer and then we happen to fall under the big fall victim to cancer or if we have diabetes or any sort of heart disease or old sickness which comes from old age and so on.

34
00:06:35,080 --> 00:06:44,080
All sorts of cold and flu and fever and viruses of all sorts.

35
00:06:44,080 --> 00:06:50,080
Whenever these things come, they will bring us great suffering.

36
00:06:50,080 --> 00:07:17,080
And if we're not ready for death, then when death comes, of course death will bring us great stress and suffering. If we're not ready to leave, if we're afraid of death, if we are not able to come to terms with the state or the act of dying, then this could be something which could bring great suffering to us.

37
00:07:17,080 --> 00:07:28,080
Often the danger in all of these things doesn't come from any sort of greed or attachment.

38
00:07:28,080 --> 00:07:34,080
It simply comes from not understanding these things. For instance, not understanding death.

39
00:07:34,080 --> 00:07:44,080
Not knowing what to expect, not knowing what's happening, not being able to come to grips with what's happening at the time of death.

40
00:07:44,080 --> 00:08:00,080
And so when we die, we die afraid, we die confused simply because we are not able to process the phenomena which are coming into our minds.

41
00:08:00,080 --> 00:08:10,080
We're not able to deal with the impermanence with something new. This is something unusual, something out of the ordinary.

42
00:08:10,080 --> 00:08:16,080
In the practice of meditation, we come to break everything down into its ultimate experience.

43
00:08:16,080 --> 00:08:26,080
When we say rising, falling, or when we have pain, when we say pain, pain or thinking for afraid or angry or upset or confused.

44
00:08:26,080 --> 00:08:39,080
When we break things down into their ultimate reality, then in the end anything which arises no matter how strange or abnormal it may be, it comes to be very normal and very ordinary.

45
00:08:39,080 --> 00:08:43,080
And very easy to deal with.

46
00:08:43,080 --> 00:08:48,080
So at the time of dying, there are still the same familiar phenomena.

47
00:08:48,080 --> 00:08:53,080
At the time of sickness, there's the same pain, aching.

48
00:08:53,080 --> 00:09:02,080
When we don't let these things become conceptual, like I have cancer or I am dying.

49
00:09:02,080 --> 00:09:14,080
When we keep it at an ultimate level of ultimate reality, there is pain, there is fear, there is anger, there is upset, there is worry and so on.

50
00:09:14,080 --> 00:09:19,080
When we simply say to ourselves, worry, worry, afraid, afraid.

51
00:09:19,080 --> 00:09:31,080
Then we don't get caught off guard by what seems to be something new, which seems to be something unordinary, extraordinary.

52
00:09:31,080 --> 00:09:37,080
So this is the first good reason to practice meditation, because of the danger that is inherent in these things.

53
00:09:37,080 --> 00:09:50,080
And of course, if we don't practice and we happen to be born again, well we could be born again exactly the way we are or we could be born again in a state of greater suffering.

54
00:09:50,080 --> 00:09:56,080
In a place where food is scarce, luxury is non-existent.

55
00:09:56,080 --> 00:10:03,080
We could be reborn as an animal, we could be reborn as a ghost, we could be reborn in health.

56
00:10:03,080 --> 00:10:06,080
We could be reborn any number of places.

57
00:10:06,080 --> 00:10:09,080
And so this is the danger of rebirth.

58
00:10:09,080 --> 00:10:14,080
And of course, the danger of all day sickness and death, which would follow.

59
00:10:14,080 --> 00:10:20,080
So we're in danger of this, we're in danger of being born again and again and again and again.

60
00:10:20,080 --> 00:10:36,080
When we die, if our minds are not clear, we're in danger of getting stuck back into the same old set of suffering, set of dangers, or even worse, depending on our state of mind.

61
00:10:36,080 --> 00:10:42,080
But if we train our minds and purify our minds and at the moment when we die, we may not even have to be born again.

62
00:10:42,080 --> 00:10:49,080
And if we are born again, we will be born in a pure place according to our state of mind when we die.

63
00:10:49,080 --> 00:10:59,080
So this is one, the first good reason, the first set of dangers which we can avoid through the practice of meditation.

64
00:10:59,080 --> 00:11:09,080
Another set of dangers is dangers which are existent in this very life, in our everyday life.

65
00:11:09,080 --> 00:11:19,080
And this is the danger of self, of blame that comes from self.

66
00:11:19,080 --> 00:11:29,080
So at the Nuhua, tapay, the danger of rebuking ourselves or blaming ourselves.

67
00:11:29,080 --> 00:11:34,080
So the danger of feeling guilty for bad things which we have done.

68
00:11:34,080 --> 00:11:43,080
And then Baranuhua, tapayat, tapayat, the danger of other people blaming us.

69
00:11:43,080 --> 00:11:48,080
The danger of receiving blame from other people.

70
00:11:48,080 --> 00:12:01,080
And then tapayat, the danger of punishment and tukatipayat in the danger of rebirth in the bad state.

71
00:12:01,080 --> 00:12:07,080
This is another set of four dangers which are ahead of us.

72
00:12:07,080 --> 00:12:11,080
So the danger of feeling guilty or blaming ourselves, this is ever present.

73
00:12:11,080 --> 00:12:17,080
Whenever we do bad things, we wish always the guilt which follows.

74
00:12:17,080 --> 00:12:19,080
If we are a good person, we feel guilty.

75
00:12:19,080 --> 00:12:25,080
If we are really able to deprive the brave person, we might not even be able to feel guilty.

76
00:12:25,080 --> 00:12:34,080
But it will still make us think deeper and deeper and dirty and defile our minds, further and further,

77
00:12:34,080 --> 00:12:37,080
and cause great mental suffering.

78
00:12:37,080 --> 00:12:41,080
At the least, we feel afraid all the time when we have done bad things,

79
00:12:41,080 --> 00:12:45,080
we feel afraid of blame from other people.

80
00:12:45,080 --> 00:12:50,080
But most ordinary people, they will feel guilty themselves.

81
00:12:50,080 --> 00:12:55,080
And this is a danger in our future.

82
00:12:55,080 --> 00:13:01,080
Because we are not perfect and we haven't cleaned our mind of bad tendencies.

83
00:13:01,080 --> 00:13:08,080
So we might still have the opportunity to get angry or upset at someone else and feel guilty and upset when we do so.

84
00:13:08,080 --> 00:13:12,080
This is a danger which is ahead of us.

85
00:13:12,080 --> 00:13:17,080
And the danger of receiving blame from other people, of course.

86
00:13:17,080 --> 00:13:20,080
This is equally if not more common.

87
00:13:20,080 --> 00:13:29,080
It's so easy to blame other people and we find that everyone around us will take the time to blame us for the bad things we do.

88
00:13:29,080 --> 00:13:34,080
Sometimes even when we don't do anything bad and they perceive it as bad,

89
00:13:34,080 --> 00:13:37,080
this is a danger that other people will blame us.

90
00:13:37,080 --> 00:13:42,080
And if we are not ready and if we are not able to deal with this in a calm and rational way,

91
00:13:42,080 --> 00:13:44,080
we might get angry and upset.

92
00:13:44,080 --> 00:13:52,080
We might feel great suffering when other people blame us for bad things we've done or things which they perceive to be bad.

93
00:13:52,080 --> 00:13:54,080
It can be the same for ourselves.

94
00:13:54,080 --> 00:13:58,080
We can blame ourselves for things which were not our fault.

95
00:13:58,080 --> 00:14:01,080
And if our minds are not well trained, we will do so.

96
00:14:01,080 --> 00:14:07,080
We will blame ourselves for things which were innocent or innocent deeds.

97
00:14:07,080 --> 00:14:13,080
So this is a reason to train our minds so that we will not have to blame ourselves

98
00:14:13,080 --> 00:14:17,080
and we will not blame ourselves for things which we didn't do.

99
00:14:17,080 --> 00:14:24,080
And we will not do bad things for which we could be truly blame worthy.

100
00:14:24,080 --> 00:14:32,080
Then there's the punishment in this life which is legal punishment or punishment for other people

101
00:14:32,080 --> 00:14:36,080
when we do bad things and they might inflict punishment.

102
00:14:36,080 --> 00:14:47,080
Our parents, our spouses, our friends, the police, the country and so on,

103
00:14:47,080 --> 00:14:52,080
even to the point of other countries when they go into war with us.

104
00:14:52,080 --> 00:14:57,080
This is a real danger where we can even receive punishment for things we never did.

105
00:14:57,080 --> 00:14:59,080
This is a real danger.

106
00:14:59,080 --> 00:15:09,080
If we're not ready to deal with punishment, we might be upset and angry and sad and depressed by these things.

107
00:15:09,080 --> 00:15:13,080
And the fourth one is the punishment in the next life.

108
00:15:13,080 --> 00:15:19,080
When we die and our minds are full of anger or full of greed or full of confusion,

109
00:15:19,080 --> 00:15:23,080
we will go to a place of suffering.

110
00:15:23,080 --> 00:15:28,080
We could go to be born in hell, we could be born as an animal, we could be born as a ghost.

111
00:15:28,080 --> 00:15:32,080
We're born in this life because our mind is like this.

112
00:15:32,080 --> 00:15:41,080
If our mind becomes full of anger or hatred, we will be born in a place of anger and hatred in hell.

113
00:15:41,080 --> 00:15:46,080
If we have greed, we'll be born as a ghost, if we have delusion, we'll be born as an animal.

114
00:15:46,080 --> 00:15:53,080
If these things are very strong, we'll be born in sufferings, states of existence.

115
00:15:53,080 --> 00:15:59,080
And this is a real danger in our future. If we still have these states of mind arising,

116
00:15:59,080 --> 00:16:03,080
then it could be a real cause for us to go to these places.

117
00:16:03,080 --> 00:16:05,080
This is a second set of four.

118
00:16:05,080 --> 00:16:17,080
The third set is a set of metaphors having to do with the ocean.

119
00:16:17,080 --> 00:16:22,080
And when we're on the ocean, when we're sailing on a ship in the ocean,

120
00:16:22,080 --> 00:16:34,080
there are four dangers, or suppose we're on a raft, on a raft in the river or in the ocean.

121
00:16:34,080 --> 00:16:36,080
And we're trying to reach the shore.

122
00:16:36,080 --> 00:16:39,080
While there are four dangers which might stop us from reaching the shore,

123
00:16:39,080 --> 00:16:44,080
and the first one is the waves.

124
00:16:44,080 --> 00:16:52,080
And the second one is crocodiles. The third one is whirlpools,

125
00:16:52,080 --> 00:16:59,080
and the fourth one is sharks. These are four dangers on the ocean.

126
00:16:59,080 --> 00:17:04,080
So these four are also dangers stopping us from reaching our goal,

127
00:17:04,080 --> 00:17:10,080
from reaching our goal of peace and happiness and freedom from suffering.

128
00:17:10,080 --> 00:17:16,080
The first one waves is the...

129
00:17:16,080 --> 00:17:18,080
They're called the Eight World Lead Hammons.

130
00:17:18,080 --> 00:17:22,080
The Eight World Lead Attachments, you could stay.

131
00:17:22,080 --> 00:17:27,080
Or eight things which we get caught up in. They're worldly things.

132
00:17:27,080 --> 00:17:31,080
So when we come to practice, we try to leave behind worldly things.

133
00:17:31,080 --> 00:17:34,080
We need a low-key of each atom and a sun.

134
00:17:34,080 --> 00:17:37,080
We try to do away with our attachment and our aversion.

135
00:17:37,080 --> 00:17:40,080
The worldly things, but if we get caught up in them again,

136
00:17:40,080 --> 00:17:44,080
they can lead us down or lead us away from our goal.

137
00:17:44,080 --> 00:17:53,080
And these are fame, fame, praise, gain and happiness.

138
00:17:53,080 --> 00:17:59,080
When we have fame, or many people know us, know who we are,

139
00:17:59,080 --> 00:18:02,080
we can get caught up in this.

140
00:18:02,080 --> 00:18:05,080
And when we are praised, when people say good things about us,

141
00:18:05,080 --> 00:18:11,080
we can become quite up in this and become lazy as a result.

142
00:18:11,080 --> 00:18:15,080
When we have gain, when we have lots of wealth,

143
00:18:15,080 --> 00:18:21,080
we can become complacent, and we don't think about following the path.

144
00:18:21,080 --> 00:18:26,080
Or when we have happiness, when our lives are happy and comfortable,

145
00:18:26,080 --> 00:18:31,080
and we don't right now, I have any states of suffering.

146
00:18:31,080 --> 00:18:37,080
Or rising. But the Lord Buddha said these are like a wave,

147
00:18:37,080 --> 00:18:40,080
because there are four other demos which follow after them.

148
00:18:40,080 --> 00:18:45,080
Wave means they could be swept away at any time.

149
00:18:45,080 --> 00:18:47,080
They could sweep you away at any time.

150
00:18:47,080 --> 00:18:56,080
So fame also has being infamous as its opposite,

151
00:18:56,080 --> 00:18:58,080
or being unknown.

152
00:18:58,080 --> 00:19:01,080
Sometimes nobody will know who we are, everyone will forget about us.

153
00:19:01,080 --> 00:19:04,080
And then we will feel upset.

154
00:19:04,080 --> 00:19:12,080
We get caught up in the waves of the waves of the world.

155
00:19:12,080 --> 00:19:16,080
Sometimes people will blame us, they will say bad things about us.

156
00:19:16,080 --> 00:19:19,080
Sometimes we may lose our belongings.

157
00:19:19,080 --> 00:19:22,080
We might even lose everything that we own.

158
00:19:22,080 --> 00:19:25,080
And if we have happiness, then for sure there will be sometimes

159
00:19:25,080 --> 00:19:27,080
where we will have unhappiness.

160
00:19:27,080 --> 00:19:30,080
So Lord Buddha said these eight things we are never sure.

161
00:19:30,080 --> 00:19:31,080
They are like a wave.

162
00:19:31,080 --> 00:19:35,080
They can sweep us away or be swept away at any time.

163
00:19:35,080 --> 00:19:38,080
This is one danger on the path.

164
00:19:38,080 --> 00:19:40,080
Another danger is the crocodiles,

165
00:19:40,080 --> 00:19:46,080
and this is getting lazy or getting indolent.

166
00:19:46,080 --> 00:19:51,080
Thinking only about our, as they say,

167
00:19:51,080 --> 00:19:55,080
and I think only about our mouth and about our stomach.

168
00:19:55,080 --> 00:20:00,080
When we can, we aren't able to deal with difficulty.

169
00:20:00,080 --> 00:20:04,080
We aren't able to deal with hardship.

170
00:20:04,080 --> 00:20:06,080
So when we come to a meditation center,

171
00:20:06,080 --> 00:20:10,080
and we aren't able to stay in a simple dwelling,

172
00:20:10,080 --> 00:20:13,080
we aren't able to deal with simple accommodations.

173
00:20:13,080 --> 00:20:15,080
We aren't able to deal with hard floors,

174
00:20:15,080 --> 00:20:24,080
or poor weather, or little food, or strange food.

175
00:20:24,080 --> 00:20:28,080
We aren't able to deal with hardships.

176
00:20:28,080 --> 00:20:30,080
If we're going to truly be free from suffering,

177
00:20:30,080 --> 00:20:35,080
we have to learn to let go of our attachment to comfort,

178
00:20:35,080 --> 00:20:41,080
our attachment to luxury.

179
00:20:41,080 --> 00:20:45,080
So sometimes it can be a good thing when we have to endure these hardships.

180
00:20:45,080 --> 00:20:47,080
We should be very patient and endure,

181
00:20:47,080 --> 00:20:52,080
even if we happen to not eat, to not have any food,

182
00:20:52,080 --> 00:20:55,080
then we should, if there is no possibility to get food,

183
00:20:55,080 --> 00:20:59,080
we should be able to stay calm and stay in peace with that.

184
00:20:59,080 --> 00:21:01,080
But of course, here we have food every day,

185
00:21:01,080 --> 00:21:03,080
and there's no need to go without food.

186
00:21:03,080 --> 00:21:05,080
It also doesn't help us to go without food,

187
00:21:05,080 --> 00:21:07,080
so we do eat.

188
00:21:07,080 --> 00:21:10,080
But we have to be able to deal with hardship

189
00:21:10,080 --> 00:21:14,080
and any painful feelings which might arise when we practice.

190
00:21:14,080 --> 00:21:17,080
We shouldn't try to avoid these things.

191
00:21:17,080 --> 00:21:19,080
This is like a crocodile.

192
00:21:19,080 --> 00:21:25,080
Something which is a danger which will eat us up inside.

193
00:21:25,080 --> 00:21:30,080
If we are always thinking about comfort and luxury.

194
00:21:30,080 --> 00:21:32,080
The third one is whirlpools.

195
00:21:32,080 --> 00:21:38,080
And the whirlpool is our objects of desire,

196
00:21:38,080 --> 00:21:46,080
and it says to do with romance or lust or thoughts of sexuality,

197
00:21:46,080 --> 00:21:51,080
thoughts of the opposite gender or thoughts of someone,

198
00:21:51,080 --> 00:21:54,080
the object of our desire.

199
00:21:54,080 --> 00:21:57,080
And this is like a whirlpool, something which drags us down,

200
00:21:57,080 --> 00:22:00,080
so if we aren't able to dispel these thoughts,

201
00:22:00,080 --> 00:22:02,080
they can drag us down.

202
00:22:02,080 --> 00:22:04,080
Right now we're practicing the holy life,

203
00:22:04,080 --> 00:22:07,080
the Brahmacharya means celibacy.

204
00:22:07,080 --> 00:22:10,080
And this is something very high that's called the Brahmacharya,

205
00:22:10,080 --> 00:22:15,080
the life of Brahmach, the life of God.

206
00:22:15,080 --> 00:22:18,080
Because God of course is celibacy.

207
00:22:18,080 --> 00:22:23,080
So we're following after the life of God.

208
00:22:23,080 --> 00:22:27,080
And when we start to fall back into these thoughts of sensuality,

209
00:22:27,080 --> 00:22:33,080
thoughts of sexuality, this is like a whirlpool which drags us down.

210
00:22:33,080 --> 00:22:41,080
And we lose our holy life, our holiness.

211
00:22:41,080 --> 00:22:47,080
And the fourth danger in this final and the final of all the dangers

212
00:22:47,080 --> 00:22:49,080
is the shark.

213
00:22:49,080 --> 00:22:57,080
And the shark is all other pleasant phenomena which may arise.

214
00:22:57,080 --> 00:23:02,080
So apart from other people or the objects of our desire,

215
00:23:02,080 --> 00:23:09,080
these can be objects of desire as for food or pleasant sights or pleasant sounds

216
00:23:09,080 --> 00:23:11,080
or pleasant smells.

217
00:23:11,080 --> 00:23:15,080
And when these things arise, some meditators might get intoxicated

218
00:23:15,080 --> 00:23:16,080
or caught up in these.

219
00:23:16,080 --> 00:23:19,080
When we see bright colors or lights,

220
00:23:19,080 --> 00:23:25,080
this can be something, this can be a danger to our practice.

221
00:23:25,080 --> 00:23:30,080
And when we smell smells or when we hear sounds,

222
00:23:30,080 --> 00:23:32,080
this can be a danger.

223
00:23:32,080 --> 00:23:34,080
When we hear music and we get caught up by the music,

224
00:23:34,080 --> 00:23:37,080
or when we taste the food and we enjoy the food

225
00:23:37,080 --> 00:23:40,080
and we forget to be mindful, tasting, tasting,

226
00:23:40,080 --> 00:23:44,080
we can become obsessed with the taste and always be thinking about it.

227
00:23:44,080 --> 00:23:47,080
And we find it very difficult for us to concentrate.

228
00:23:47,080 --> 00:23:50,080
This is the fourth danger on the ocean.

229
00:23:50,080 --> 00:23:54,080
So these four are a danger on the practice on the path.

230
00:23:54,080 --> 00:23:58,080
And these we have to be aware of before going to follow the path.

231
00:23:58,080 --> 00:24:03,080
And of course, we follow the path to overcome all danger.

232
00:24:03,080 --> 00:24:12,080
Now this teaching on danger is to help us to become one that the Lord Buddha called,

233
00:24:12,080 --> 00:24:16,080
one who sees the danger in some sorrow,

234
00:24:16,080 --> 00:24:21,080
sees the danger in being old, getting sick and dying again and again and again.

235
00:24:21,080 --> 00:24:26,080
He's the danger in simply living our lives without having trained ourselves.

236
00:24:26,080 --> 00:24:30,080
And this makes us with the Lord Buddha called a Bhikkhu.

237
00:24:30,080 --> 00:24:36,080
A Bhikkhu is a word which was used for a monk or for someone who begged for food.

238
00:24:36,080 --> 00:24:40,080
Of course, in Buddhism, the monks don't beg for food.

239
00:24:40,080 --> 00:24:44,080
The word Bhikkhu here means, samsar a paayan ikati,

240
00:24:44,080 --> 00:24:48,080
one who sees the danger in some sorrow.

241
00:24:48,080 --> 00:24:55,080
So this teaching is given for the purposes of helping the meditators see the danger

242
00:24:55,080 --> 00:24:59,080
and not be caught off guard by the dangers of samsarar,

243
00:24:59,080 --> 00:25:02,080
the dangers which exist in the world around us.

244
00:25:02,080 --> 00:25:08,080
And that's the teaching that I wish to give on this day.

245
00:25:08,080 --> 00:25:12,080
So now we'll continue with mindful frustration walking

246
00:25:12,080 --> 00:25:37,080
and sitting all together until two o'clock.

