1
00:00:00,000 --> 00:00:07,360
Okay, I want to ask about Merritt. I often help a friend, but sometimes I do not want to help her, however

2
00:00:07,360 --> 00:00:13,940
I do it all the time because she needs it. Do I get Merritt helping someone even though I don't want to help?

3
00:00:13,940 --> 00:00:40,060
Well, I'll stand. Well, no, right away we have to fall back on the Abidama. The theoretical answer that we have,

4
00:00:40,060 --> 00:00:50,060
no, it's not even the theoretical answer. The correct answer is that goodness the word Merritt or Pune comes from the mind,

5
00:00:50,060 --> 00:01:05,560
and whatever arises in the mind at any given moment determines whether the action is wholesome or unwholesome is good or bad.

6
00:01:05,560 --> 00:01:17,920
So it actually is a little more complicated than just asking whether you get Merritt or don't get Merritt, because at every moment you're

7
00:01:17,920 --> 00:01:28,980
accumulating either good qualities of mind or bad qualities of mind. But when easy way to think of this is in terms of our habits, ask yourself how do

8
00:01:28,980 --> 00:01:42,760
your habits accumulate? How do you develop habits? If you think about it, you can see that it's from the repeated performance of a

9
00:01:42,760 --> 00:01:51,320
specific act with a specific mind state. So if you get angry often you'll become an angry person. If you cling to something or you

10
00:01:51,320 --> 00:02:01,760
strive after something often you'll become addicted to it. And so we can see that the habits are developing moment to

11
00:02:01,760 --> 00:02:08,820
moment to moment. Everything that we do every moment that we give rise to a certain mind state develops it further into a habit.

12
00:02:08,820 --> 00:02:18,720
And that's how goodness works. Goodness is good because it changes you. The word Pune or Merritt that we always hear really

13
00:02:18,720 --> 00:02:26,020
means goodness. The Buddhist definition of it is that it is something that purifies the mind. Pune it to you. We so

14
00:02:26,020 --> 00:02:36,420
date it. It's something that cleans and purifies one's being. So the meaning there is that it changes your habit. It develops

15
00:02:36,420 --> 00:02:53,520
a habit of clarity of mind and all the associated states like kindness and compassion and wisdom and mindfulness and so on. So at the time when

16
00:02:53,520 --> 00:02:59,120
you're helping the person what's going on in your mind. If you don't want to help them then at that moment there is some kind of

17
00:02:59,120 --> 00:03:09,240
aversion. Quite possibly some kind of aversion. If there is some kind of aversion that is an unwholesome state. That's the opposite of

18
00:03:09,240 --> 00:03:19,960
goodness. That's a badness. If you simply know with wisdom that helping this person is not really doing them any good and it's

19
00:03:19,960 --> 00:03:27,480
simply wasting my time and it's making them more dependent. It's building up the habit of dependency in their mind and

20
00:03:27,480 --> 00:03:40,760
so on. But then you in the desire to develop goodness you give up your your own happiness in your own peace in order

21
00:03:40,760 --> 00:03:50,800
to in order to develop patience in order to develop renunciation and to to give up your own attachment to your

22
00:03:50,800 --> 00:03:59,680
happiness in peace. You sacrifice yourself and you do this good deed for a person. No, not only because they have

23
00:03:59,680 --> 00:04:09,120
asked and because because they are requesting this then you're actually you're actually developing goodness by

24
00:04:09,120 --> 00:04:17,760
developing patience and by developing renunciation by developing the patience that avoids the aversion because if you say no

25
00:04:17,760 --> 00:04:24,840
to them then you get in an argument and you feel bad or you feel you you get it somehow being selfish

26
00:04:24,840 --> 00:04:35,000
and then there is the associated states of aversion or or anger towards the person or so on. But you can only answer

27
00:04:35,000 --> 00:04:40,640
it based on what arises in the mind. So at the moment when you're giving rise to patience and and being

28
00:04:40,640 --> 00:04:46,520
patient with the person and noting your anger and noting your frustration with them and your your aversion towards

29
00:04:46,520 --> 00:04:52,280
helping them then then you're developing goodness. But at the moment when you're angry and and thinking about the

30
00:04:52,280 --> 00:05:02,800
bad stuff you're developing unwholesomeness. So and as far as the you know the classic understanding of goodness

31
00:05:02,800 --> 00:05:09,080
whether whether the actual help that you're giving is a good thing. Well it depends on whether you see it as a good

32
00:05:09,080 --> 00:05:18,080
fact if you do something for the person and you are angry about it. You say here take this and and you're upset about it.

33
00:05:20,960 --> 00:05:26,900
Then well at the very moment when you're upset you're developing unwholesomeness. It can still be that you develop

34
00:05:26,900 --> 00:05:34,960
goodness. There's a story in there's there's stories like this in the dhamapada. There's this or not the dhamapada in the

35
00:05:34,960 --> 00:05:49,880
various commentaries. There's one about this woman who something like this this but Jacob Buddha was looking for mud and so she

36
00:05:49,880 --> 00:06:00,200
gave him some mud but she was very angry at him. Well she did it she she scolded him and and she she insulted him as she

37
00:06:00,200 --> 00:06:10,160
did it. As a result in the later life she was born very ugly. There's a bunch of bunch of pop I think was her name is

38
00:06:10,160 --> 00:06:18,120
something where her five was in her five faculties or something about her the five parts of her body were all out of

39
00:06:18,120 --> 00:06:30,160
whack her eyes were kind of out of whack like kind of like a Picasso Picasso Pablo Picasso painting. But her touch

40
00:06:30,160 --> 00:06:36,600
was irresistible because she had off because she had actually gone ahead and offered this this mud into this this

41
00:06:36,600 --> 00:06:48,320
Buddha. I can't remember what the mud was for. He wanted to make a walking path or something. Her touch was irresistible and so she

42
00:06:48,320 --> 00:06:55,960
touched this she she once was when she was a child she was walking along and she happened to touch this this

43
00:06:55,960 --> 00:07:03,800
prince who was on his horse. She touched his leg or I don't know touch somehow touched his hand and he fell in love

44
00:07:03,800 --> 00:07:10,680
with her immediately. And later on she she she happened to touch another king and he fell in love with her and so these two

45
00:07:10,680 --> 00:07:20,040
kingdoms the story is a lot much longer than that and I just heard it from another monk in Thai quite some time ago. They

46
00:07:20,040 --> 00:07:26,360
ended up fighting the two kingdoms went to war over this woman who was you know they because of their

47
00:07:26,360 --> 00:07:33,080
out of their love for her and she was she was an incredibly ugly person or you know in a standard sense of the

48
00:07:33,080 --> 00:07:40,640
word. So normally would be considered by people to be quite ugly and yet she her touch was irresistible. So

49
00:07:40,640 --> 00:07:47,480
it was like the combination of unwholesomeness and wholesomeness. How that works is because it's it's based on

50
00:07:47,480 --> 00:07:52,560
the mind from one moment to the next your mind is changing. So you can say you don't want to help them but

51
00:07:52,560 --> 00:07:57,360
that's not all that's going through your mind. There are there are many wholesome qualities, patience,

52
00:07:57,360 --> 00:08:08,280
pronunciation and generosity of course you when you do something that's difficult to do and you help someone the

53
00:08:08,280 --> 00:08:14,280
Buddha said even when you help an animal who has no morality and no goodness in their very little goodness in

54
00:08:14,280 --> 00:08:23,000
their mind you still get a great great peace of mind and the great purity of mind as a result. When you

55
00:08:23,000 --> 00:08:28,760
give to someone who has who is immoral a person who has no morality you can even a better result when you

56
00:08:28,760 --> 00:08:39,320
give to someone who is moral and support them and help them it's a far better result. So you know we do have to make

57
00:08:39,320 --> 00:08:44,520
a set of priorities in terms of how we're going to expend our energies but you certainly do get merit

58
00:08:44,520 --> 00:08:53,000
from helping a person but but you also get demerits or or unwholesomeness from the disliking of it. So

59
00:08:53,000 --> 00:08:57,800
it's just part of one of those things in the world the world is not cut and dry if you want

60
00:08:57,800 --> 00:09:06,840
simple you have to move to the forest where your biggest quandary is where to go on arms round or so and

61
00:09:06,840 --> 00:09:12,440
how to deal with mosquitoes and so on. In the world there are much more difficult dilemmas and in

62
00:09:12,440 --> 00:09:17,720
the end it ends up being partly wholesome and partially unwholesome. Really the best that you should

63
00:09:18,520 --> 00:09:25,960
strive for is to have the wholesomeness outweigh the unwholesomeness and whatever unwholesomeness arises

64
00:09:25,960 --> 00:09:32,200
in your mind maybe you even say no to some people you are still for the most part doing good

65
00:09:32,200 --> 00:09:37,240
deeds you're practicing meditation you're giving charity and help to people and you're keeping the

66
00:09:37,240 --> 00:09:43,400
moral precepts you're not killing or stealing or or intentionally hurting other people but you know

67
00:09:43,400 --> 00:09:47,960
it may be that you develop unwholesomeness saying no to someone but if you compare that to all the

68
00:09:47,960 --> 00:09:52,920
goodness that you have all the good things that you're doing and and another fact that maybe you do

69
00:09:52,920 --> 00:09:58,600
help them from time to time even though you might get angry or it might be accompanied with a

70
00:09:58,600 --> 00:10:06,440
version towards helping the person it's quite insignificant really in comparison as long as you're

71
00:10:06,440 --> 00:10:10,440
out weighing it with lots of good things and of course the other thing is is that it can be based

72
00:10:10,440 --> 00:10:18,600
on wisdom if you know that the person is helpless or unhelpable or if you know that your time is

73
00:10:18,600 --> 00:10:25,880
better spent with other things you know this idea of triaging you the doctor will never help the

74
00:10:25,880 --> 00:10:32,760
person who will die anyway right if they have time they'll take care of them but in war when you have

75
00:10:33,480 --> 00:10:38,200
they they have triaging because they can't help everyone so you don't you don't waste your time

76
00:10:39,160 --> 00:10:42,920
helping the person who's going to die anyway to die peacefully you've got to take care of the

77
00:10:42,920 --> 00:10:48,680
people who might live and the people who are going to get better by themselves you also don't waste

78
00:10:48,680 --> 00:10:53,640
your time making it easier for them or so and you see that person most likely will live and you

79
00:10:53,640 --> 00:10:59,160
leave them alone that person most likely will die you leave them alone this person it's not sure

80
00:10:59,960 --> 00:11:08,760
those people you focus on so that's do you by chance remember the dhamma part of us we were

81
00:11:08,760 --> 00:11:18,280
talking about the other day yesterday I think that it fits kind of in here on the part

82
00:11:18,280 --> 00:11:27,720
that she is not wanting to help sometimes but things that the other person needs it but I think

83
00:11:27,720 --> 00:11:37,160
it's not always the case that people need help sometimes it's important that they need to learn

84
00:11:37,160 --> 00:11:48,120
to help themselves exactly but I do not remember the dhamma part of us unfortunately now

85
00:11:49,320 --> 00:11:54,200
never never sacrifice your own good for the good of another no matter how great it's quite

86
00:11:54,200 --> 00:11:59,640
pertinent if you believe if you agree with the Buddha and that it's quite a useful verse to think

87
00:11:59,640 --> 00:12:07,720
about once you know either what is in your own true benefit then you should develop yourself in

88
00:12:07,720 --> 00:12:15,640
that develop yourself your own benefit but you know the that you have to explain that

89
00:12:15,640 --> 00:12:23,160
person and actually helping other people is of benefit to ourselves so it's not the case that we

90
00:12:24,760 --> 00:12:29,480
it means we don't help other people thinking up I do that I won't be able to help myself

91
00:12:29,480 --> 00:12:44,040
so much you have to be careful that helping other people is a necessary part of your own

92
00:12:44,040 --> 00:12:48,920
mental development and if you don't help them it's actually to your detriment sometimes because it

93
00:12:48,920 --> 00:13:04,840
involves a different version

