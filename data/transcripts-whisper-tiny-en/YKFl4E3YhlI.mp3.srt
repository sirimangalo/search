1
00:00:00,000 --> 00:00:05,000
I am welcome back to Ask a Monk. Today's question comes from

2
00:00:05,000 --> 00:00:08,000
Ithong Tien, who asks,

3
00:00:08,000 --> 00:00:12,000
I'm practicing Anapanasati using counting technique

4
00:00:12,000 --> 00:00:17,000
by Adjan Ayasaro, which he said to count from 1 to 5

5
00:00:17,000 --> 00:00:21,000
start over from 1 to 6, 1 to 7 so on.

6
00:00:21,000 --> 00:00:24,000
It's hard to be aware of the in-out breath.

7
00:00:24,000 --> 00:00:28,000
I tend to lose my counting. How to be aware of breath and count

8
00:00:28,000 --> 00:00:32,000
at the same time.

9
00:00:32,000 --> 00:00:36,000
Okay. First of all,

10
00:00:36,000 --> 00:00:40,000
if you're practicing a technique under a specific teacher,

11
00:00:40,000 --> 00:00:42,000
the best thing is to ask a teacher.

12
00:00:42,000 --> 00:00:46,000
Or the second best thing, someone who is close to him.

13
00:00:46,000 --> 00:00:50,000
I don't know this teacher. You might be referring to Adjan Jayasaro

14
00:00:50,000 --> 00:00:56,000
with a J. Who I am familiar with, but don't know too much

15
00:00:56,000 --> 00:01:01,000
about and certainly don't follow or don't teach the same technique,

16
00:01:01,000 --> 00:01:04,000
which is fine. I mean, I'm not discounting this technique,

17
00:01:04,000 --> 00:01:09,000
but it's hard for me to go into detail.

18
00:01:09,000 --> 00:01:12,000
If I can give you some basics of Anapanasati,

19
00:01:12,000 --> 00:01:17,000
Anapanasati in the commentaries is considered to be a

20
00:01:17,000 --> 00:01:21,000
tranquility meditation.

21
00:01:21,000 --> 00:01:24,000
So it's only for the purpose of quieting the mind.

22
00:01:24,000 --> 00:01:27,000
You should use it as a preliminary technique,

23
00:01:27,000 --> 00:01:30,000
before moving on to, as I've said before,

24
00:01:30,000 --> 00:01:35,000
to examine the physical and the mental aspects of the breathing

25
00:01:35,000 --> 00:01:40,000
or of the body and the mind in general.

26
00:01:40,000 --> 00:01:45,000
Because breath itself, the breath is a concept.

27
00:01:45,000 --> 00:01:47,000
It's not something that we actually experience.

28
00:01:47,000 --> 00:01:49,000
You don't experience the breath.

29
00:01:49,000 --> 00:01:51,000
That's a concept. What you experience is the sensation

30
00:01:51,000 --> 00:01:55,000
of the nose and the chest or in the stomach.

31
00:01:55,000 --> 00:02:00,000
And so we would focus on the experience of the heart or the cold,

32
00:02:00,000 --> 00:02:04,000
instead of saying in, out or even counting one, two, three, four.

33
00:02:04,000 --> 00:02:06,000
These are concepts.

34
00:02:06,000 --> 00:02:09,000
So for insect meditation, we don't use them.

35
00:02:09,000 --> 00:02:12,000
For Anapanasati, if that's your choice,

36
00:02:12,000 --> 00:02:15,000
if your choice is to calm the mind in the beginning.

37
00:02:15,000 --> 00:02:19,000
First of all, starting at one to five is fine.

38
00:02:19,000 --> 00:02:22,000
Don't go up more than one to ten.

39
00:02:22,000 --> 00:02:27,000
The texts are pretty clear about five being less than five being too little,

40
00:02:27,000 --> 00:02:30,000
more than ten being too much, and they give their reasons.

41
00:02:30,000 --> 00:02:35,000
But just to let you know that you shouldn't be going past ten.

42
00:02:35,000 --> 00:02:39,000
Count from one to five is fine. Count from one to ten is fine.

43
00:02:39,000 --> 00:02:42,000
You don't have to switch. You don't have to go one to five, one to six,

44
00:02:42,000 --> 00:02:44,000
one to seven, one to eight, one to nine, one to ten.

45
00:02:44,000 --> 00:02:46,000
You can start at one to ten if you like.

46
00:02:46,000 --> 00:02:50,000
That's the example that's given in the texts.

47
00:02:50,000 --> 00:02:52,000
The other thing is that when you're doing counting,

48
00:02:52,000 --> 00:02:56,000
you shouldn't be aware of the whole motion of the breath.

49
00:02:56,000 --> 00:03:00,000
You should pick one spot, be it at the nose,

50
00:03:00,000 --> 00:03:02,000
in the chest, or in the stomach.

51
00:03:02,000 --> 00:03:06,000
And acknowledge that it's going in, saying to yourself,

52
00:03:06,000 --> 00:03:12,000
one, one, one, one, the whole time it's going in.

53
00:03:12,000 --> 00:03:16,000
Or do it on the out breath, one, one, one, one.

54
00:03:16,000 --> 00:03:18,000
In daily out breath is finished.

55
00:03:18,000 --> 00:03:21,000
They actually specify, there's no need to do both of them.

56
00:03:21,000 --> 00:03:24,000
You can just acknowledge the in breaths,

57
00:03:24,000 --> 00:03:26,000
or acknowledge the out breaths, one or the other.

58
00:03:26,000 --> 00:03:28,000
If you pick both of them, just keep saying one, one,

59
00:03:28,000 --> 00:03:30,000
one until the first breath is finished.

60
00:03:30,000 --> 00:03:33,000
Then go on to two, two, two, two, two, two, two, two, two.

61
00:03:33,000 --> 00:03:35,000
And then three, three, three, three.

62
00:03:35,000 --> 00:03:37,000
This is called slow counting.

63
00:03:37,000 --> 00:03:39,000
This is how you start.

64
00:03:39,000 --> 00:03:41,000
After that, you can do quick counting,

65
00:03:41,000 --> 00:03:44,000
just to say one, you know that it's going in.

66
00:03:44,000 --> 00:03:48,000
Then again, two, three, four, and so on.

67
00:03:48,000 --> 00:03:51,000
This is just counting one time.

68
00:03:51,000 --> 00:03:54,000
But counting is only the preliminary practice.

69
00:03:54,000 --> 00:03:58,000
It's a suggestion just to quiet your mind,

70
00:03:58,000 --> 00:04:00,000
because the mind is thinking it's similar to the technique

71
00:04:00,000 --> 00:04:02,000
that we use when we see rising, falling,

72
00:04:02,000 --> 00:04:03,000
or we say pain, pain.

73
00:04:03,000 --> 00:04:06,000
It's a mantra that focuses your mind.

74
00:04:06,000 --> 00:04:08,000
In some of the meditation, you don't need it after a while.

75
00:04:08,000 --> 00:04:13,000
You leave it up and just follow the motion of the breath

76
00:04:13,000 --> 00:04:15,000
in and out.

77
00:04:15,000 --> 00:04:20,000
If you want to use the breath for insight on the other hand,

78
00:04:20,000 --> 00:04:23,000
then you have to focus on something that is really,

79
00:04:23,000 --> 00:04:26,000
you have to focus on either the stomach moving in and out

80
00:04:26,000 --> 00:04:29,000
or the heat and cold at the nostrils.

81
00:04:29,000 --> 00:04:32,000
And so that's why we have meditators focus on the stomach

82
00:04:32,000 --> 00:04:35,000
rising, falling, rising, falling.

83
00:04:35,000 --> 00:04:40,000
Because that's the reality of the breath.

84
00:04:40,000 --> 00:04:44,000
So your question, how to be aware of the breath

85
00:04:44,000 --> 00:04:46,000
and count at the same time.

86
00:04:46,000 --> 00:04:49,000
It really depends what you want out of it.

87
00:04:49,000 --> 00:04:51,000
If you're just doing some of that,

88
00:04:51,000 --> 00:04:53,000
you don't really have to be aware of the breath.

89
00:04:53,000 --> 00:04:55,000
You have to know that the breath is going in

90
00:04:55,000 --> 00:04:59,000
and use this number one, one, one, just to know

91
00:04:59,000 --> 00:05:01,000
that it's going in, not to follow it.

92
00:05:01,000 --> 00:05:04,000
The text is quite specific about not following the breath,

93
00:05:04,000 --> 00:05:09,000
knowing that it's going in at the tip of the nose generally

94
00:05:09,000 --> 00:05:11,000
or the out breath or both of them

95
00:05:11,000 --> 00:05:15,000
and starting with the counting and eventually giving it out.

96
00:05:15,000 --> 00:05:17,000
And there's quite a technique there.

97
00:05:17,000 --> 00:05:20,000
It's a very subtle technique

98
00:05:20,000 --> 00:05:23,000
and one that takes a lot of practice to develop,

99
00:05:23,000 --> 00:05:25,000
but it can lead to great states of calm.

100
00:05:25,000 --> 00:05:27,000
If you're a goal and the other hand is insight,

101
00:05:27,000 --> 00:05:29,000
then you don't have to focus on the breath at all.

102
00:05:29,000 --> 00:05:32,000
Focus at either at the nose in the same way,

103
00:05:32,000 --> 00:05:34,000
but not focusing on the fact that breath is going in,

104
00:05:34,000 --> 00:05:37,000
focus on the sensation of heat or cool

105
00:05:37,000 --> 00:05:39,000
and not thinking about the breath at all

106
00:05:39,000 --> 00:05:42,000
or focus on the stomach rising and falling.

107
00:05:42,000 --> 00:05:44,000
So it's not directly focusing on the breath,

108
00:05:44,000 --> 00:05:47,000
you're focusing on the movements of the body,

109
00:05:47,000 --> 00:05:50,000
which are part of the respiratory system.

110
00:05:50,000 --> 00:05:53,000
So I hope that helps to give you some little,

111
00:05:53,000 --> 00:05:55,000
what information I can,

112
00:05:55,000 --> 00:05:57,000
but the best thing is if you want to practice

113
00:05:57,000 --> 00:05:59,000
on up on the sati according to this Adjan

114
00:05:59,000 --> 00:06:00,000
that you're talking about,

115
00:06:00,000 --> 00:06:05,000
and you should contact him or someone in his group.

116
00:06:05,000 --> 00:06:06,000
Okay, so I hope that helps.

117
00:06:06,000 --> 00:06:31,000
Thanks for the question and keep practicing.

