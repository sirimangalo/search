Good evening, everyone.
It's, quote, there's about wealth.
Many of you probably are aware Buddhism is not a well-thorean to religion, doesn't place
much importance on wealth.
Can't think of any religion that does.
I think Buddhism especially has some very fairly pointed things to say, or at least has
a reputation of probably being anti-wealth.
I think, I mean monks are supposed to be monks who are the spokespeople of the religion
are supposed to live in poverty.
They are not allowed to touch money or valuable objects and gold and jewels.
They have to wear these robes, they have to eat the food that were given, they have poverty
is a big part of Buddhism.
Contentment is the greatest wealth.
Oh, I had my timer set right in the drum.
Contentment is the greatest form of wealth.
What is, what wealth is supposed to do is fulfill our wants, our needs, and so ordinary wealth
is able to temporarily fulfill our wants and our desires and our needs, which our needs
anyway make it an important thing.
So in this quote that we have tonight, the Buddha acknowledges that wealth is good for you.
It can make you happy, it can make your parents happy, it can make your spouse, your children,
your children, some workers happy, your friends in companions happy, and you can use it
to give charity or religious donations is what the quote refers to, but charity is general.
So if you do, if obviously poverty is a problem, but the dangers of that wealth is that
it's temporary, it disappears.
Wealth can be incredibly dangerous, right?
More wealth you are, the more trapped you are by it can be.
Anyways wealthy people are trapped or can be trapped by their money.
There was an interesting story read about people who were in the lottery that apparently
winning the lottery is one of the worst things that can happen to you.
You look it up, it's quite interesting what happens to people with the statistics are for
people who have won the lottery, homicide, suicide, a lot of robbery, you lose friends,
you lose your family, lose your life, wealth that kind of wealth can be dangerous,
but even putting that aside, worldly wealth, though it's necessary to maintain livelihood
and this is where Buddhism does acknowledge the need for wealth, even monks have to have
balls, they have to have things, they have to have tools, the things that allow them to continue
their life.
But beyond that wealth isn't actually able to provide contentment, it isn't actually able
to satisfy, it isn't able to bring happiness.
So the happiness that it can bring is the ability to live one's life without worry
without fear, and potentially without having to go hungry, without having to be cold
or hot, without having to suffer through lack of the amenities or the necessities, but
as far as fulfilling our wants, our desires, there's no wealth, no amount of wealth that
can do that.
And so in terms of our wants, this is why contentment is the greatest form of wealth.
Because that's what we're looking for from wealth, we have to understand that the problem
is we want something, this is an issue.
When you want something, it's actually a problem.
If you don't get it, to the extent that you want it, it's going to cause suffering for
you until you get it.
If you want it alive, it can cause great suffering.
If you want it a little about just a little suffering until you get it, of course, once
you get it, then you're pleased.
With that pleasure, reinforces the desire, and so you want it more, and it becomes more
acute, and eventually you don't get what you want and do suffer, even so you're constantly
living your life chasing after your desires, contentment on the other hand accomplishes
the same thing, but it does it by going the other way, by destroying the wanting.
When you remove the wanting, without reinforcing it, without following it, then you weaken
it.
When you can find peace without the things you want, and you can give up your wants and
find happiness without needing this or that to make you happy.
Before in concept, to many of us, we're so used to finding happiness in things, in experiences.
It becomes kind of religious or a view that we have, our belief, and if you challenge
that, people will...there's a tendency to react quite negatively, we will get upset,
you know, again, turned off by the things you're saying, you say, find happiness by letting
go, by not clinging, by giving up your desires, where a few people will be interested.
As I've talked about recently, most of us don't even understand that statement.
Again, we can't even comprehend it, or so fix, fixated on our desires, and it hard to
even comprehend the idea of being happy without...it's not maybe that's not giving people
enough credit, but especially all of you here, anyone who's watching this, has an understanding
of some understanding of the problem, they've had suffering in their lives, and so they
can see how our wants will be one thing to be a certain way when we don't get that, it's
quite suffering, quite a bit of suffering, great suffering.
And so we turn the meditation thinking to find contentment, this is really what it is.
Because it hasn't come to you while here's the...here's an idea, the reason why meditation
might be interesting is to find contentment, to be able to live without need.
So how does it do this meditation, teaches us objectivity, it's like straightening the mind.
The mind is bent and crooked all out of shape, so you have to strike it and straighten it.
And every moment that you see things as they are, you're straightening the mind.
Not just every moment you're meditating, that's not the case, but well you're meditating
when you find a moment where you just see something as it is when you say rise, and you're
just aware of the rising.
That moment is a pure state, and it's just straight.
The mind is straight, is wholesome as pure, and you cultivate this cultivation of this
state these states, which is...this is the path to contentment.
There's an at time in that moment, there's no need for anything, there's no want for anything.
When you cultivate these moments after moments consecutively, you're slowly become content.
Now you're dealing with so many likes and dislikes, so it's not easy, and it's not very
comfortable.
It's mostly not content, but in those moments you can see the contentment.
That's what we're fighting for, it's what we're working for.
It takes time, but through practice, you call this is what you cultivate.
This is the direction that you steer yourself towards.
I don't want to talk too much about wealth, I don't like talking about money, and so on,
but contentment, that's where it's at.
There's your dhamma bit for tonight.
Open up to hang out, if anybody wants to.
Come on and ask questions, if you don't have any questions, then just say good night.
Met a couple of Sri Lankan people on the street today, and they're coming tomorrow morning
to visit.
It's nice, people in the community, there actually are people on this area.
There's a Sri Lankan monk, or Bangladeshian monk in Toronto, said he was going to get
me in touch with this Sri Lankan community here, and he never did.
He's very busy though, but apparently there's quite a few Sri Lankan people in Hamilton.
There's a some western people coming out to meditate to visit, so this place could become
a real part of the community to see how it goes.
I gave meditation booklet to the woman who serves me at Tim Hortons, a really good group
of people at Tim Hortons.
That's funny.
They've gotten to know them.
How many of you have to do with the pain?
Probably when you're echoing me, do you have something else on, like YouTube or the audio
stream?
I don't know.
They're rubbing.
She doesn't hear me, I don't think.
Hello.
Hello.
Salty Banthay.
Hi.
Hi, sorry.
It's been so long, I forgot that I have to turn the abona.
Sorry about that.
So I had a question.
Okay.
Okay.
So we have a teaching that we hear all the time.
We heard it pretty very similarly.
So let's see.
I'll only be seeing, like hearing, only be hearing.
Let's see.
I'll only be smiling.
And I mean, it sounds good.
But with my heart, yeah.
I forgot that I had to turn the abona.
I had a question.
Okay.
If you're going to come on the hangout, you have to turn the other thing off first.
Let's see, I'll only be seeing.
Hello.
Okay.
Okay.
So with this teaching, I mean it sounds good.
But my common sense tells me that we have to judge what comes in contact with our senses
to be safe if you're truly just tasting, tasting, but not judging the taste.
You could be eating rancid food.
If you're smelling, but not recognizing the smell, your house could be burning.
I mean, how does this teaching work in the real world where we do have to evaluate our surroundings?
Yeah.
I mean, it's not the whole truth.
It's not like a way of living your life, but it's the necessary activity that you need
to perform during a meditation practice.
Because if you don't get to the point where seeing is just seeing, you'll never get to
the root.
But I mean, even in Iran, has to think and has to process as the talk has to interact with
people.
Has to judge.
Seeing is not just seeing, seeing is a pit in front of you that you have to go around.
But you don't have that in minutes.
It's still seeing, you see the pit and you say seeing, seeing.
And then there's still the thoughts that arise based on the pit.
They arise anyway.
The point is your mind is straight.
So those thoughts are free from, oh, my gosh, it's a pit.
They can still arise and they still will arise.
They weren't as bad.
So it's only part of the picture.
Those thoughts of, oh, that's a pit.
Well, hey, wait, you're not seeing seeing.
But when you said seeing is just seeing, when you said seeing seeing, it neutralizes the experience.
At least for a moment, you could still get react, but the first moment of seeing the pit or the tiger or whatever
has neutralized the fear and the shock.
I mean, that being said, to some extent, there is a sense of having to stay neutral.
Like, of course, if it's rancid food, you know that it's rancid food.
You have no reason to eat it.
But if it's a tiger, and the tiger is going to eat you.
Well, you run, but once it catches you, seeing, let's see, just be seeing pain, just be pain.
So you kind of have to go in and out of this.
Is that would that be correct?
Not even.
It's arguable that, yeah, you'd have to go a little, but it's not really out of it.
Because your mind is pure.
It's like when you walk to walking meditation, you're only saying,
step being right, stepping left, but you're aware of a lot else.
You're aware of the shifting of the body.
You know, you may be aware of stray thoughts, but you don't have to note them all.
As long as you keep your mind straight, as long as you keep your mind pure.
And so by noting every second or so, you know, frequently, your mind will say, stay straight.
So it's important to do this, but it's not everything, especially if you're live.
But that being said, when you actually do a meditation, or the best way to understand it,
is during a meditation, if you want to follow it, where you can follow it perfectly strictly.
When you're sitting with your eyes closed, well, that's the time where you're going to look at these things.
But all it takes is a moment where I tell the story about this monk who is teaching,
I was watching, and he says, hearing, hearing, and then...
Well, he was teaching.
It's probably one of the most impressive things I've ever seen.
So all it was is in that moment for him hearing was just hearing, as he was teaching it.
And that's what it means.
My challenge is just to make it enough of a part of my day to ever, to ever get to that point.
Okay, I'm doing it.
One of our recent meditators finished, of course, did okay, finished it.
It was a little bit disappointed, I think, with the results.
I was pretty sure he got where he was supposed to be.
I mean, you can't tell.
It's hard for him to tell, it's hard for me to tell.
But then he emailed me, and he'd gotten stuck on a flight.
And he was on this plane for like nine hours or something, on the runway.
It's an insane amount of time sitting on the runway.
And what he described to me, what happened on the runway, suddenly he came back and...
I was just listening.
I said, that sounds exactly like that.
On the plane, leaving to go home.
You never know.
You never know.
And when you're ready.
Sometimes you get so stressed during the course.
Or you get stressed during the course, and it inhibits it prevents.
You're from your worried, or near the end it can get stressful.
And if you let it get to you, if you're not mindful, then it just happens when you relax.
And he was like, Anna, right?
Anna was doing walking meditation all night, trying to become enlightened.
And nothing.
And then it was almost dawn, and he felt like it got nowhere.
And he was really stressed out.
And then he thought, and he said, oh, I'm pushing too hard.
And so he laid down.
And before his head touched the pillow, he was not.
Hi, Tom.
Hi, Bata.
Do you have a question?
Wow.
All right.
Nice to see you.
Good to see you here.
Thank you, Bante.
All right.
Welcome.
Have a good night, everyone.
Good morning, Bata.
