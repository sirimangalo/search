1
00:00:00,000 --> 00:00:06,000
Hi, welcome back to Ask a Monk. Today I thought I would answer three questions in a row

2
00:00:06,000 --> 00:00:12,620
because they seem fairly simple to answer and it wouldn't. I don't think it

3
00:00:12,620 --> 00:00:19,300
would be worth making a video about each one. The first one is from Shah's

4
00:00:19,300 --> 00:00:25,260
Ruzal. Dear Yutta Damo, could you explain the importance of the Bodhi tree? The

5
00:00:25,260 --> 00:00:29,320
Bodhi tree, what is meant by the Bodhi tree, is a fig, a sort of a fig tree that

6
00:00:29,320 --> 00:00:35,320
is found in India and it happens to me the tree that the historical Buddha

7
00:00:35,320 --> 00:00:40,440
sat under to become enlightened. Now according to the legends or what we have

8
00:00:40,440 --> 00:00:45,660
of the Buddha's teaching, in the other world periods and ancient times,

9
00:00:45,660 --> 00:00:52,240
other Buddhas arose and they all had their own type of tree and basically what

10
00:00:52,240 --> 00:00:55,360
it means is that the followers people who came after would always venerate that

11
00:00:55,360 --> 00:01:01,440
sort of tree because it was in the memory of the Buddha. So whenever we see

12
00:01:01,440 --> 00:01:08,120
this tree, we'll generally try to protect it and maybe put a terrace around it

13
00:01:08,120 --> 00:01:13,960
and it came to be to signify the Buddha himself. So instead of making a

14
00:01:13,960 --> 00:01:17,880
statue of the Buddha, they would plant a Bodhi tree or make a carving of a Bodhi

15
00:01:17,880 --> 00:01:25,720
leaf or you'll see people with Bodhi leaves or leaves from this fig tree or

16
00:01:25,720 --> 00:01:32,600
mala is made of the seeds or so and so basically it just represents the

17
00:01:32,600 --> 00:01:36,120
Buddha, where the Buddha became enlightened and the Buddha sat down to meditate

18
00:01:36,120 --> 00:01:40,800
for the final time before he was to become enlightened to realize truth that he

19
00:01:40,800 --> 00:01:45,640
then taught to people for the rest of his life. It was under a Bodhi tree, this

20
00:01:45,640 --> 00:01:51,480
type of tree and so it's called a Bodhi tree. The word Bodhi means enlightenment or

21
00:01:51,480 --> 00:01:57,480
wisdom, so it's the enlightened tree of enlightenment and the original one is in

22
00:01:57,480 --> 00:02:05,680
India. It's in Buddha Gaiya which is south of a city called Gaiya in the province

23
00:02:05,680 --> 00:02:13,720
of Bihar and actually as I'm sure people can tell you I'm not so clear on it but

24
00:02:13,720 --> 00:02:20,440
I think this is the fourth generation of in a sequence of trees. It was

25
00:02:20,440 --> 00:02:26,280
killed, cut down and replanted from Sri Lanka. The Sri Lankans took a

26
00:02:26,280 --> 00:02:32,320
sap cut from the original Bodhi tree, brought it to Sri Lanka and when the

27
00:02:32,320 --> 00:02:38,800
Bodhi tree was killed in Gaiya in India, they brought it back from Sri Lanka.

28
00:02:38,800 --> 00:02:46,680
So you'll find the Maha Bodhi tree there which is probably the most it's

29
00:02:46,680 --> 00:02:49,720
sort of like the Buddhist mecha I guess you could say the most important Buddhist

30
00:02:49,720 --> 00:02:57,040
spot in the world. It's where Buddhists every all year round will go to to pay

31
00:02:57,040 --> 00:03:02,400
respect to the Buddha and to his enlightenment. Next question is from

32
00:03:02,400 --> 00:03:10,360
Yitong Tien. Do you make your own food? No I don't make my own food. There are

33
00:03:10,360 --> 00:03:14,280
some videos about the arms round that I've done so you can take a look at those.

34
00:03:14,280 --> 00:03:25,800
As a monk we aren't allowed to store food, cook food or store food, that's about it.

35
00:03:25,800 --> 00:03:29,960
We're not allowed to store food overnight, we're not allowed to cook food, we're

36
00:03:29,960 --> 00:03:35,560
not allowed to and we're not allowed to take food for ourselves. So we couldn't

37
00:03:35,560 --> 00:03:40,440
go into a store and buy food or even go into someone's cupboard and if they

38
00:03:40,440 --> 00:03:44,040
given us permission to go and take food. We're only allowed to eat food that

39
00:03:44,040 --> 00:03:51,560
has been given freely out of faith or as arms. So in India people would give

40
00:03:51,560 --> 00:03:59,120
food as a tradition they would give it to. Some people of all religious beliefs

41
00:03:59,120 --> 00:04:04,280
they would believe that they're doing something good, something some sort of

42
00:04:04,280 --> 00:04:10,400
spiritually beneficial practice. It's a moral. It's a charitable practice.

43
00:04:10,400 --> 00:04:13,840
It's something that people do because it makes them happy and it makes them

44
00:04:13,840 --> 00:04:19,120
feel like good people. It makes them feel like that they're doing something of

45
00:04:19,120 --> 00:04:23,400
words so they would give that. And even nowadays Buddhists who do the same

46
00:04:23,400 --> 00:04:27,840
thing they maybe aren't able to become monks themselves or aren't able to

47
00:04:27,840 --> 00:04:35,560
even go to practice meditation but they're able to practice at home and to do

48
00:04:35,560 --> 00:04:40,040
good deeds to support the monks and to support people who are practicing

49
00:04:40,040 --> 00:04:46,160
meditation. So I go to the restaurants, the local Asian restaurants and they put

50
00:04:46,160 --> 00:04:53,280
food in my bowl every day and I've been doing that for the past nine years in

51
00:04:53,280 --> 00:04:59,680
Thailand and now in America. So no, we're not allowed to cook food. The third

52
00:04:59,680 --> 00:05:06,320
question is I realized that I'm easily angered and I've had a hard time to quit

53
00:05:06,320 --> 00:05:10,240
smoking because of it. What kind of meditation should I do to help with this?

54
00:05:10,240 --> 00:05:14,400
What kind of Tai Chi could I also do? Your answer would mean a lot. Thanks. This is

55
00:05:14,400 --> 00:05:21,080
from 915 River. And the reason I'm not going to go into detail about this is

56
00:05:21,080 --> 00:05:26,400
because really I've given a specific set of meditation and I think it really does

57
00:05:26,400 --> 00:05:29,840
help with anger if you're to put into practice the teachings that I have

58
00:05:29,840 --> 00:05:34,160
posed and the teachings on how to meditate which I assume you haven't looked

59
00:05:34,160 --> 00:05:42,480
at since you're asking this question. You'll see that there's both the way of

60
00:05:42,480 --> 00:05:46,480
meditating and the reasons why we meditate and one of the reasons is to over

61
00:05:46,480 --> 00:05:50,920
some things like anger to give up addictions like smoking. If you were interested

62
00:05:50,920 --> 00:05:56,080
in specifically in smoking, I think it's an interesting adaptation of the

63
00:05:56,080 --> 00:06:00,400
meditation practice. I see that's not exactly what you're asking but I'm

64
00:06:00,400 --> 00:06:04,120
giving up smoking doesn't have just to do with anger it has to do with feelings

65
00:06:04,120 --> 00:06:07,600
of guilt which you have to do away with first once you've given up the feelings

66
00:06:07,600 --> 00:06:13,240
of guilt and then go on to the craving once you're able to accept the craving

67
00:06:13,240 --> 00:06:16,880
and to just see it from what it is then go even deeper on to the feeling. The

68
00:06:16,880 --> 00:06:21,200
happy feeling that comes with the craving, the pleasure that comes with wanting.

69
00:06:21,200 --> 00:06:25,840
And when you can just experience the pleasure and say yourself happy happy

70
00:06:25,840 --> 00:06:32,680
or pleasure and feeling feeling then you can do away with the craving and

71
00:06:32,680 --> 00:06:35,400
do away with the needing and do away with the getting and do away with the

72
00:06:35,400 --> 00:06:41,560
smoking when you're able to bring it right back to the both thoughts about

73
00:06:41,560 --> 00:06:46,920
smoking and the feelings about the pleasant feeling of excitement and you

74
00:06:46,920 --> 00:06:55,320
get when you think of smoking. And as far as Tai Chi, I don't teach Tai Chi so

75
00:06:55,320 --> 00:07:00,680
you're on your own there. You have to find someone who teaches that, okay? So

76
00:07:00,680 --> 00:07:06,600
there's three questions that deserve answers but I think the answers are

77
00:07:06,600 --> 00:07:13,600
pretty simple so there you have it. Thanks for the questions and keep practicing.

