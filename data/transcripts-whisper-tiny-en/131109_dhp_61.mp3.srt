1
00:00:00,000 --> 00:00:07,000
Hello and welcome back to our study of the Dampanda.

2
00:00:07,000 --> 00:00:14,000
Today we begin the Balawaga, the chapter on fools.

3
00:00:14,000 --> 00:00:24,000
So all the verses in here, in this chapter we'll have something to do with fools or foolishness.

4
00:00:24,000 --> 00:00:31,000
Verse 61, today which reads as follows,

5
00:00:54,000 --> 00:01:06,000
if when wandering or faring through some sorrow and doesn't find someone who is one's equal or better,

6
00:01:06,000 --> 00:01:10,000
or at least equal to one's self.

7
00:01:10,000 --> 00:01:29,000
One should therefore, then one should then fare on a loan with steadfast resolution.

8
00:01:29,000 --> 00:01:38,000
Nati Bali is a Hayata for there is no association with fools.

9
00:01:38,000 --> 00:01:45,000
So the story behind this verse goes that when the Buddha was dwelling in Sahwati,

10
00:01:45,000 --> 00:01:50,000
Mahakasapa was staying in his cave above Rajagahat,

11
00:01:50,000 --> 00:01:53,000
which you can still see today if you go there.

12
00:01:53,000 --> 00:01:59,000
Apparently they have a place that they pinpointed as his cave.

13
00:01:59,000 --> 00:02:03,000
Anyway, you can still go into Rajagahat.

14
00:02:03,000 --> 00:02:12,000
Up on the mountain above Rajagahat, he would go every day down the mountain into the city for arms.

15
00:02:12,000 --> 00:02:19,000
And at the time he had two students, two novices staying with him.

16
00:02:19,000 --> 00:02:21,000
And they would attend upon him.

17
00:02:21,000 --> 00:02:32,000
And one of them was a good student and one of them was a rather recalcitrant student.

18
00:02:32,000 --> 00:02:41,000
He was lazy in performing his duties and in practicing the dhamma.

19
00:02:41,000 --> 00:02:55,000
And so how it would go is the one student who was quite diligent and faithful and obedient and so on.

20
00:02:55,000 --> 00:03:02,000
He would prepare the elders' water in the morning and his tooth stick, tooth brushing sticks.

21
00:03:02,000 --> 00:03:08,000
And he would wait upon his elder and perform all these duties and he would heat up water for the elders' bath

22
00:03:08,000 --> 00:03:13,000
and leave the water out in the morning.

23
00:03:13,000 --> 00:03:20,000
And the other disciple would watch and would come or would sleep in and then would come out when everything was done.

24
00:03:20,000 --> 00:03:25,000
And look and see if everything was done and go to tell the elder.

25
00:03:25,000 --> 00:03:32,000
But in both the tooth sticks have been put out and water has been put out for washing and for bathing.

26
00:03:32,000 --> 00:03:39,000
Please come and it's time to do your morning routine.

27
00:03:39,000 --> 00:03:49,000
And so the elder, as a result, got to thinking that this student who kept coming to tell him and invite him to use the things that have been set out for him.

28
00:03:49,000 --> 00:03:53,000
Had performed duties himself.

29
00:03:53,000 --> 00:03:57,000
And the other student who had actually done all the work sat back and watched.

30
00:03:57,000 --> 00:04:04,000
And after some time realized what this, the other student was doing that he was taking credit for the work that he hadn't done.

31
00:04:04,000 --> 00:04:07,000
He said, well, I know what to do then.

32
00:04:07,000 --> 00:04:17,000
And so the morning, when it was time for the elders' bath, he went to the room with the boiler water.

33
00:04:17,000 --> 00:04:23,000
And he put just a little bit of water in the bottom of the big pot and a big cauldron for boiling bath water.

34
00:04:23,000 --> 00:04:25,000
And he did that.

35
00:04:25,000 --> 00:04:29,000
And so you could see the steam coming up but there was very little water in it.

36
00:04:29,000 --> 00:04:35,000
And the other novice, when he came over and saw the steam coming out, he said, oh, I must have put out the water.

37
00:04:35,000 --> 00:04:39,000
And he went to tell the elder, remember, well, sir, your bath water is ready.

38
00:04:39,000 --> 00:04:40,000
I'm going to take your bath.

39
00:04:40,000 --> 00:04:44,000
And the elder went into the bathroom and said, okay, bring out the water.

40
00:04:44,000 --> 00:04:54,000
And the novice went into the boiling room and looked in the cauldron and realized that there was just a very little bit of water and got very angry.

41
00:04:54,000 --> 00:04:55,000
And shouted.

42
00:04:55,000 --> 00:05:00,000
And he said, that scoundrel, he only put a very little bit of water in.

43
00:05:00,000 --> 00:05:03,000
And so he rushed off to the river to get more water.

44
00:05:03,000 --> 00:05:05,000
Where the river was.

45
00:05:05,000 --> 00:05:09,000
And rushed off to get water from wherever they get the water.

46
00:05:09,000 --> 00:05:14,000
Maybe he must have had to gone down into Rajika had to go there.

47
00:05:14,000 --> 00:05:17,000
And the elder watched him go.

48
00:05:17,000 --> 00:05:24,000
And when he came back, the elder said, what did you mean by that?

49
00:05:24,000 --> 00:05:29,000
Are you saying that you didn't do any of this and you've been taking credit for this all the time?

50
00:05:29,000 --> 00:05:31,000
And he said, that's not how the elder scolded him.

51
00:05:31,000 --> 00:05:34,000
And so that's not how a monk should behave.

52
00:05:34,000 --> 00:05:38,000
One should not take credit for something that one hasn't done.

53
00:05:38,000 --> 00:05:45,000
And the novice got very angry and went off enough.

54
00:05:45,000 --> 00:05:49,000
And began to develop a grudge against the elder.

55
00:05:49,000 --> 00:05:54,000
The next day, or on subsequent days, they went off.

56
00:05:54,000 --> 00:06:00,000
When they went off for arms, I guess the next day, when they went for arms, he has a result.

57
00:06:00,000 --> 00:06:05,000
He decided that not to go on arms with the elder.

58
00:06:05,000 --> 00:06:09,000
And so when the elder and the other novice went off for arms, they went alone.

59
00:06:09,000 --> 00:06:17,000
And the disobedient novice, he stayed back and was just sulked at the kuti at the cave.

60
00:06:17,000 --> 00:06:20,000
And the elder went with his other novice.

61
00:06:20,000 --> 00:06:27,000
And then when they were gone, he went off to one of the elder's supporters' houses.

62
00:06:27,000 --> 00:06:34,000
He went down into Rajika had his own and went to one of the elder's supporters and said to them that the elder was sick.

63
00:06:34,000 --> 00:06:36,000
He said, oh, where's the elder?

64
00:06:36,000 --> 00:06:37,000
Why are you coming along?

65
00:06:37,000 --> 00:06:40,000
He said, oh, he's sick, but he needs some special food.

66
00:06:40,000 --> 00:06:47,000
And please give him very delicate and good tasting food and so on.

67
00:06:47,000 --> 00:06:50,000
So he can recover from his sickness.

68
00:06:50,000 --> 00:06:52,000
And so they gave him all this special food,

69
00:06:52,000 --> 00:06:54,000
and prepared all this special food for him.

70
00:06:54,000 --> 00:06:58,000
And he took it back and aided himself.

71
00:06:58,000 --> 00:07:03,000
And the elder went on arms round with the other novice.

72
00:07:03,000 --> 00:07:10,000
And happened to get a full set of robes, new nice robes from one family.

73
00:07:10,000 --> 00:07:13,000
And so he gave them to the novice that went with him.

74
00:07:13,000 --> 00:07:18,000
And the novice changed out of his own set and put on this new set of robes.

75
00:07:18,000 --> 00:07:21,000
And they went back to the monastery.

76
00:07:21,000 --> 00:07:29,000
On yet another day, the elder went to see, happened to go to see this supporter who had been tricked.

77
00:07:29,000 --> 00:07:33,000
And they said to the other, oh, are you a vendor buster? How are you doing?

78
00:07:33,000 --> 00:07:34,000
We heard that you were sick.

79
00:07:34,000 --> 00:07:38,000
But we gave food to the novice when he came.

80
00:07:38,000 --> 00:07:44,000
Hope the food helped you to get better.

81
00:07:44,000 --> 00:07:47,000
And the elder just stood there and didn't say anything.

82
00:07:47,000 --> 00:07:49,000
We received food from him and went off.

83
00:07:49,000 --> 00:07:53,000
And they went back to the monastery and he confronted this novice.

84
00:07:53,000 --> 00:07:58,000
And he said to him, is it true that you've been going around?

85
00:07:58,000 --> 00:08:01,000
Is it true that you went to these people and told them that I was sick?

86
00:08:01,000 --> 00:08:03,000
And convinced him to give you all this special food.

87
00:08:03,000 --> 00:08:05,000
And he said, what do you mean?

88
00:08:05,000 --> 00:08:09,000
It's just a scolded him.

89
00:08:09,000 --> 00:08:13,000
And my husband scolded him and said, that's not something that a monk should do.

90
00:08:13,000 --> 00:08:17,000
And the novice was, he was totally fed up at this point.

91
00:08:17,000 --> 00:08:19,000
He said, over a little bit of water, the elder gets upset.

92
00:08:19,000 --> 00:08:21,000
And then he gets upset about a little bit of food.

93
00:08:21,000 --> 00:08:26,000
And on top of that, he's been giving, he gives a robe a full set of robes to the other novice.

94
00:08:26,000 --> 00:08:29,000
So this novice got very, very angry.

95
00:08:29,000 --> 00:08:32,000
And then he said, I know what I'm going to do.

96
00:08:32,000 --> 00:08:35,000
And the next morning when the elder went off on Armstrong,

97
00:08:35,000 --> 00:08:40,000
he went around the cave and broke up all the pots and everything.

98
00:08:40,000 --> 00:08:44,000
Just tore up everything and ripped up everything, all the bedding and everything.

99
00:08:44,000 --> 00:08:46,000
And then set the whole place on fire.

100
00:08:46,000 --> 00:08:48,000
And then he ran off.

101
00:08:48,000 --> 00:08:52,000
And I think ended up in hell as a result.

102
00:08:52,000 --> 00:08:59,000
And the elder came back and saw this and fixed it all up as best he could.

103
00:08:59,000 --> 00:09:02,000
And of course went on with his life.

104
00:09:02,000 --> 00:09:05,000
So this is a story of these two novices.

105
00:09:05,000 --> 00:09:08,000
One who was a good companion.

106
00:09:08,000 --> 00:09:09,000
And there's a bad companion.

107
00:09:09,000 --> 00:09:11,000
And eventually the story got back to the Buddha,

108
00:09:11,000 --> 00:09:16,000
one of the monks from Rajivat wandered all the way to Sawati.

109
00:09:16,000 --> 00:09:20,000
And Buddha asked him, how is Mahakasupa doing?

110
00:09:20,000 --> 00:09:25,000
And he said, oh, he's doing fine, but he happened to have these two students.

111
00:09:25,000 --> 00:09:31,000
And one of them was very, very good and followed the elder's instruction

112
00:09:31,000 --> 00:09:34,000
and advice and practiced the elder's teachings.

113
00:09:34,000 --> 00:09:38,000
But the other one was an evil scoundrel.

114
00:09:38,000 --> 00:09:47,000
And not only did he not practice, but he also ended up destroying the elder's residence.

115
00:09:47,000 --> 00:09:50,000
And the Buddha said, oh, yes, yes.

116
00:09:50,000 --> 00:09:53,000
The elder is better off without this novices.

117
00:09:53,000 --> 00:09:55,000
And he told the story of the past.

118
00:09:55,000 --> 00:09:57,000
This is the introduction to one of the jataka stories,

119
00:09:57,000 --> 00:10:00,000
where he was a monkey and he tore up this bird's nest.

120
00:10:00,000 --> 00:10:01,000
I'm not going to tell the story.

121
00:10:01,000 --> 00:10:02,000
It's not much to it.

122
00:10:02,000 --> 00:10:07,000
But he said, basically, you know, this novices, this is the way he was.

123
00:10:07,000 --> 00:10:10,000
And he said, better off without him.

124
00:10:10,000 --> 00:10:15,000
He said, there's no association with fools, if the only people you can hang out with

125
00:10:15,000 --> 00:10:20,000
and rely upon are fools, well, you're better off without them.

126
00:10:20,000 --> 00:10:22,000
And then he told this verse.

127
00:10:22,000 --> 00:10:25,000
So a simple story, a story that I'm sure we can all relate to.

128
00:10:25,000 --> 00:10:30,000
That's all living in caves, having novices attend upon us.

129
00:10:30,000 --> 00:10:34,000
No, all of us having good friends and bad friends, people who we can rely upon

130
00:10:34,000 --> 00:10:39,000
and people who we shouldn't rely upon, people who help us up

131
00:10:39,000 --> 00:10:46,000
and people who drag us down.

132
00:10:46,000 --> 00:10:53,000
One of the most important aspects of our practice is the association with good people.

133
00:10:53,000 --> 00:10:57,000
I'm going to send it to all of the holy life, all of the spiritual life.

134
00:10:57,000 --> 00:11:00,000
The spiritual life is all about associating with the right people,

135
00:11:00,000 --> 00:11:07,000
having a good friend, having someone to guide you and teach you and raise you up.

136
00:11:07,000 --> 00:11:12,000
In many places, he said that friends are good friends are most valuable

137
00:11:12,000 --> 00:11:15,000
and do not associate with fools.

138
00:11:15,000 --> 00:11:17,000
Of course, this is the famous, the mongolisture,

139
00:11:17,000 --> 00:11:21,000
the first thing that the Buddha says is not associating with fools.

140
00:11:21,000 --> 00:11:26,000
This is the greatest blessing.

141
00:11:26,000 --> 00:11:28,000
So how does this go?

142
00:11:28,000 --> 00:11:30,000
The verse is broken down quite well by the commentary.

143
00:11:30,000 --> 00:11:35,000
So I'll go through that first and then talk a little bit about how it relates to meditation,

144
00:11:35,000 --> 00:11:37,000
particularly.

145
00:11:37,000 --> 00:11:43,000
First of all, the word Chārung, Chārung, which means to fair,

146
00:11:43,000 --> 00:11:51,000
to wander, to travel, to move about, to live your life, basically.

147
00:11:51,000 --> 00:11:56,000
The commentary says it doesn't refer to moving about with your body.

148
00:11:56,000 --> 00:12:02,000
It refers to the movements of the mind or the way of the mind.

149
00:12:02,000 --> 00:12:05,000
So there's nothing to do with being close to someone.

150
00:12:05,000 --> 00:12:08,000
It's not like if you shouldn't sit on the subway next to someone,

151
00:12:08,000 --> 00:12:10,000
unless you're sure they're a wise person.

152
00:12:10,000 --> 00:12:17,000
It also doesn't necessarily mean that you can't work or live or cohabitate with such people.

153
00:12:17,000 --> 00:12:23,000
So it says Manasachārung, which means the fairing of the mind.

154
00:12:23,000 --> 00:12:26,000
Don't let them into your heart, basically.

155
00:12:26,000 --> 00:12:36,000
Don't take their counsel or take them up as your intimate associates.

156
00:12:36,000 --> 00:12:42,000
And then what is meant by Sayyang, Sadhitimatthinos,

157
00:12:42,000 --> 00:12:45,000
what is meant by one who is greater than you or one who is equal to you.

158
00:12:45,000 --> 00:12:50,000
The commentary says it's specifically in regards to Sīla-samādīpanya,

159
00:12:50,000 --> 00:12:52,000
morality, concentration, and wisdom.

160
00:12:52,000 --> 00:12:55,000
So if someone has greater morality than you,

161
00:12:55,000 --> 00:12:58,000
if there's someone who is practicing higher morality,

162
00:12:58,000 --> 00:13:02,000
for instance, monks who are practicing celibacy and poverty,

163
00:13:02,000 --> 00:13:15,000
and who are taking themselves out of all of the distracting activities

164
00:13:15,000 --> 00:13:17,000
that lay people will engage in,

165
00:13:17,000 --> 00:13:21,000
then it's something that can help us to cultivate concentration ourselves.

166
00:13:21,000 --> 00:13:30,000
If we associate with such people, if we are around people who are around people who are equal to us,

167
00:13:30,000 --> 00:13:35,000
then it will not, our morality will not fade away.

168
00:13:35,000 --> 00:13:38,000
If we have, if we're around people who are greater concentration,

169
00:13:38,000 --> 00:13:40,000
then their concentration will be a support for us.

170
00:13:40,000 --> 00:13:42,000
It will be an example for us.

171
00:13:42,000 --> 00:13:48,000
It will be something that we can rely upon or we can emulate

172
00:13:48,000 --> 00:13:52,000
the cultivate concentration ourselves, and if we hang around people who are wisdom,

173
00:13:52,000 --> 00:14:00,000
greater wisdom than us, then it will be something that inspires us and teaches us.

174
00:14:00,000 --> 00:14:05,000
So the reason for the best thing is to hang out with someone

175
00:14:05,000 --> 00:14:08,000
who associate with people who are greater than you,

176
00:14:08,000 --> 00:14:12,000
because you will develop what the good qualities of morality, concentration,

177
00:14:12,000 --> 00:14:17,000
wisdom will increase before around people who are

178
00:14:17,000 --> 00:14:20,000
a greater morality concentration in wisdom than us.

179
00:14:20,000 --> 00:14:23,000
And if we are around people who are of equal,

180
00:14:23,000 --> 00:14:27,000
an equal level to us, then we can be assured

181
00:14:27,000 --> 00:14:31,000
that we at least won't fall away from our morality concentration.

182
00:14:31,000 --> 00:14:33,000
Wisdom and as a result through our own practice,

183
00:14:33,000 --> 00:14:38,000
we'll be able to develop.

184
00:14:38,000 --> 00:14:41,000
The point is that the people around us won't drag us down.

185
00:14:41,000 --> 00:14:44,000
If you're around people who are inferior to you,

186
00:14:44,000 --> 00:14:49,000
and if you rely upon them and if you emulate them and cultivate their qualities,

187
00:14:49,000 --> 00:14:56,000
then you will fall away from your own morality and concentration.

188
00:14:56,000 --> 00:15:02,000
Because it comes under attack, if you let them into close.

189
00:15:02,000 --> 00:15:06,000
Now, if you can't find someone who is greater or someone who is equal to you,

190
00:15:06,000 --> 00:15:11,000
the Buddha said, actually, it's preferable to hang out on your own.

191
00:15:11,000 --> 00:15:17,000
Now, the key word here is Dalham,

192
00:15:17,000 --> 00:15:20,000
because Dalham means steadfast to resolute

193
00:15:20,000 --> 00:15:23,000
and the point is that you actually need resolution.

194
00:15:23,000 --> 00:15:29,000
If you're on your own, many people are always contacting us constantly.

195
00:15:29,000 --> 00:15:32,000
If you get these emails from people saying that they're all alone

196
00:15:32,000 --> 00:15:35,000
and they need some guidance and the moaning,

197
00:15:35,000 --> 00:15:38,000
the fact that they're not able to find a sangha community.

198
00:15:38,000 --> 00:15:42,000
And the Buddhists give reassurance and we should reassure such people

199
00:15:42,000 --> 00:15:46,000
that you can practice on your own. You don't really need so much guidance.

200
00:15:46,000 --> 00:15:49,000
It just takes more resolution

201
00:15:49,000 --> 00:15:51,000
because if you're not surrounded by examples

202
00:15:51,000 --> 00:15:54,000
or people who are reminding you to...

203
00:15:54,000 --> 00:15:56,000
reminding you of what you're doing,

204
00:15:56,000 --> 00:15:59,000
reminding you of the cultivation of greater morality concentration.

205
00:15:59,000 --> 00:16:02,000
It's easier for yourself to fall away.

206
00:16:02,000 --> 00:16:07,000
It's like a plant or a young tree that requires a cage

207
00:16:07,000 --> 00:16:10,000
or some kind of support to keep it from falling over

208
00:16:10,000 --> 00:16:13,000
to the people from breaking in the wind and so on.

209
00:16:13,000 --> 00:16:15,000
That support is helpful

210
00:16:15,000 --> 00:16:21,000
unless the tree itself is able to maintain its own strength and grow on its own.

211
00:16:21,000 --> 00:16:27,000
So the benefit of having a good companion

212
00:16:27,000 --> 00:16:31,000
is that they support you through your growth.

213
00:16:31,000 --> 00:16:36,000
Not Timbali, Sahayata, why is it that we should stay on our own?

214
00:16:36,000 --> 00:16:38,000
If we can't find such a person

215
00:16:38,000 --> 00:16:40,000
because the association of fools,

216
00:16:40,000 --> 00:16:42,000
there is no association with fools.

217
00:16:42,000 --> 00:16:47,000
The meaning being that none of the good things that come from

218
00:16:47,000 --> 00:16:50,000
associating with good people, morality, concentration,

219
00:16:50,000 --> 00:16:57,000
wisdom, all of the good kata, what do the ten types of profitable speech?

220
00:16:57,000 --> 00:17:01,000
All of the dutanga practices, all of the

221
00:17:01,000 --> 00:17:06,000
we passed on the inside and even the attainment of Nibana

222
00:17:06,000 --> 00:17:07,000
is difficult.

223
00:17:07,000 --> 00:17:11,000
It becomes difficult to become threatened by fools.

224
00:17:11,000 --> 00:17:13,000
Maybe surrounded by people who are distracting you,

225
00:17:13,000 --> 00:17:18,000
who are talking about useless things and who are indulging in

226
00:17:18,000 --> 00:17:23,000
harmful pursuits, killing and stealing and lying and cheating and so on.

227
00:17:23,000 --> 00:17:25,000
All of these things will drag you down.

228
00:17:25,000 --> 00:17:27,000
We'll make it more difficult for you to practice

229
00:17:27,000 --> 00:17:32,000
and believe in cause you to follow away from the practice.

230
00:17:32,000 --> 00:17:35,000
This is quite a simple meaning,

231
00:17:35,000 --> 00:17:42,000
but specifically in regards to the meditation practice.

232
00:17:42,000 --> 00:17:46,000
The point is that people will distract us,

233
00:17:46,000 --> 00:17:50,000
and people will be a cause for us to stall your mind.

234
00:17:50,000 --> 00:17:54,000
The question that always comes up is whether or not

235
00:17:54,000 --> 00:17:59,000
this means that we should ignore people who are in trouble

236
00:17:59,000 --> 00:18:03,000
morally or have poor concentration, people who are on the wrong path

237
00:18:03,000 --> 00:18:05,000
shouldn't we help such people.

238
00:18:05,000 --> 00:18:07,000
And of course this is the exception.

239
00:18:07,000 --> 00:18:11,000
The Buddha said Anyatta Anudya Anyatta Anukampa,

240
00:18:11,000 --> 00:18:17,000
which means that you shouldn't hang out

241
00:18:17,000 --> 00:18:32,000
with a person who should not be associated with.

242
00:18:32,000 --> 00:18:36,000
Except in the case of pity or compassion.

243
00:18:36,000 --> 00:18:42,000
So the commentary explains which is exactly how it should be explained,

244
00:18:42,000 --> 00:18:45,000
that if you have compassion, if out of compassion for someone

245
00:18:45,000 --> 00:18:48,000
it's possible or not we should have the compassion.

246
00:18:48,000 --> 00:18:52,000
So if you think that a person is capable

247
00:18:52,000 --> 00:18:54,000
and you think you're able to help them,

248
00:18:54,000 --> 00:18:56,000
then you should hang out with them

249
00:18:56,000 --> 00:18:58,000
and you should try to support their practice

250
00:18:58,000 --> 00:19:00,000
and you should help them to cultivate.

251
00:19:00,000 --> 00:19:03,000
If you're not able to, if you try and you fail

252
00:19:03,000 --> 00:19:09,000
and it seems like this person is not going to develop themselves,

253
00:19:09,000 --> 00:19:14,000
then of course then you should stay away from them actually.

254
00:19:14,000 --> 00:19:18,000
And you should go your own way because they will drag you down.

255
00:19:18,000 --> 00:19:23,000
So in meditation this is especially important

256
00:19:23,000 --> 00:19:29,000
and this is why, because meditation is the direct cultivation

257
00:19:29,000 --> 00:19:32,000
of wholesal mental states.

258
00:19:32,000 --> 00:19:37,000
So if you're constantly, if you're distracted

259
00:19:37,000 --> 00:19:44,000
or led astray by other people, then your mind becomes impossible

260
00:19:44,000 --> 00:19:47,000
to cultivate wholesome states of mind,

261
00:19:47,000 --> 00:19:55,000
this is why the Buddha often talks about staying alone

262
00:19:55,000 --> 00:20:00,000
even away from good friends, because even wholesome talk,

263
00:20:00,000 --> 00:20:13,000
if engaged in over much can lead to distraction as well.

264
00:20:13,000 --> 00:20:17,000
Because meditation is the cultivation of the mind

265
00:20:17,000 --> 00:20:26,000
in our environment and our engagement with the environment

266
00:20:26,000 --> 00:20:35,000
and with the world around us is at most important.

267
00:20:35,000 --> 00:20:39,000
So we have to be careful how we engage with the world around us.

268
00:20:39,000 --> 00:20:45,000
So for a meditator even good people we should only engage in in moderation

269
00:20:45,000 --> 00:20:49,000
and much less much more so.

270
00:20:49,000 --> 00:21:01,000
We should be careful when we associate or interact with foolish people.

271
00:21:01,000 --> 00:21:05,000
They will distract us, they take away our focus of mind,

272
00:21:05,000 --> 00:21:07,000
they take away our clarity of mind,

273
00:21:07,000 --> 00:21:11,000
and they stop us from seeing things as they are.

274
00:21:11,000 --> 00:21:13,000
We're not able to stay in the present

275
00:21:13,000 --> 00:21:16,000
because such people are constantly in the past or in the future.

276
00:21:16,000 --> 00:21:20,000
We're not able to stay in reality as such people are always talking about concepts

277
00:21:20,000 --> 00:21:24,000
and we're not able to stay with what is important

278
00:21:24,000 --> 00:21:27,000
because such people are always engaged and concerned with

279
00:21:27,000 --> 00:21:33,000
and intend upon what is unimportant or is unessential.

280
00:21:33,000 --> 00:21:38,000
So unless we can help such people,

281
00:21:38,000 --> 00:21:40,000
and unless we can help such people easily

282
00:21:40,000 --> 00:21:42,000
without interrupting our own practice,

283
00:21:42,000 --> 00:21:44,000
we should be very careful to stay on our own.

284
00:21:44,000 --> 00:21:47,000
Many people, this is, I think, an important reminder.

285
00:21:47,000 --> 00:21:50,000
If you can't find someone who can lead you,

286
00:21:50,000 --> 00:21:52,000
who can help you, can support your practice,

287
00:21:52,000 --> 00:21:54,000
stay on your own, you're better off on your own

288
00:21:54,000 --> 00:21:59,000
because just having friends isn't just the fact

289
00:21:59,000 --> 00:22:03,000
that you have friends alone isn't necessarily a useful or a good thing.

290
00:22:03,000 --> 00:22:05,000
Something that leads to your happiness.

291
00:22:05,000 --> 00:22:06,000
You need good friends.

292
00:22:06,000 --> 00:22:09,000
You need friends who lead you to good things.

293
00:22:09,000 --> 00:22:14,000
So it helps you to support you who encourage you

294
00:22:14,000 --> 00:22:16,000
in your own spiritual development.

295
00:22:16,000 --> 00:22:19,000
So simple teaching, but one that we should all keep in mind

296
00:22:19,000 --> 00:22:23,000
and a very important aspect of our life is who we associate with.

297
00:22:23,000 --> 00:22:25,000
So that's the teaching for tonight.

298
00:22:25,000 --> 00:22:28,000
Thank you all for tuning in and wish you all peace, happiness,

299
00:22:28,000 --> 00:22:43,000
and freedom from suffering. Thank you.

