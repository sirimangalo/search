1
00:00:00,000 --> 00:00:06,560
Okay, this is a question in response to a comment made during one of the meditation sessions.

2
00:00:06,560 --> 00:00:13,000
Someone mentioned that he should meditate more than reading or listening to Buddhist

3
00:00:13,000 --> 00:00:16,160
talks because it is also a pleasure.

4
00:00:16,160 --> 00:00:23,320
Do you suggest us to stop reading or watching Buddhist YouTube talks and just meditating

5
00:00:23,320 --> 00:00:24,320
for now?

6
00:00:24,320 --> 00:00:27,920
Just meditate for now.

7
00:00:27,920 --> 00:00:35,760
But not all pleasure is wrong, in fact no pleasure is wrong, pleasure isn't the problem.

8
00:00:35,760 --> 00:00:41,000
Not all pleasure is associated with negative mind states, there is a difference between

9
00:00:41,000 --> 00:00:48,480
pleasure and attachment or pleasure in liking if you want to say.

10
00:00:48,480 --> 00:00:53,080
There is also a kind of, you could say there is a kind of liking that is in a roundabout

11
00:00:53,080 --> 00:00:55,480
way beneficial.

12
00:00:55,480 --> 00:01:01,040
So people who like to practice meditation, people who like to listen to the Buddha's teaching

13
00:01:01,040 --> 00:01:08,000
who are waiting for these sessions because it stimulates them and so on, it is kind of

14
00:01:08,000 --> 00:01:10,040
a negative thing.

15
00:01:10,040 --> 00:01:13,880
Because what they are really attached to is the feelings and maybe when I joke or when

16
00:01:13,880 --> 00:01:20,560
they hear stories, there are these Buddhist teachers who give talks and everyone likes

17
00:01:20,560 --> 00:01:22,600
to hear their stories.

18
00:01:22,600 --> 00:01:26,160
And if you read some of the old Buddhist texts, there is a lot of nice stories in there

19
00:01:26,160 --> 00:01:27,160
and it is stimulating.

20
00:01:27,160 --> 00:01:30,640
In the mind it makes you feel happy, it makes you feel sad.

21
00:01:30,640 --> 00:01:35,680
It is like when we watch a movie, when we read a novel or something, it stimulates you.

22
00:01:35,680 --> 00:01:39,640
So that is kind of negative that aspect.

23
00:01:39,640 --> 00:01:46,280
And in a roundabout way, it can be helpful because of the content, because actually when

24
00:01:46,280 --> 00:01:58,160
you get here, the focus changes or the focus is different.

25
00:01:58,160 --> 00:02:01,040
So the point really is in the content.

26
00:02:01,040 --> 00:02:06,040
If the content of the teachings is truly the Buddha's teaching and you truly are paying

27
00:02:06,040 --> 00:02:19,160
attention and making effort to understand and are conforming your mind to the teachings,

28
00:02:19,160 --> 00:02:23,600
trying to get your head around it, then for sure it is a good thing to do all of that.

29
00:02:23,600 --> 00:02:26,840
I mean you can become enlightened listening to the dhamma.

30
00:02:26,840 --> 00:02:30,600
I hope that was kind of clear when we were talking about the last question.

31
00:02:30,600 --> 00:02:35,680
As you are listening, just listening to me talk can be a meditation practice in itself because

32
00:02:35,680 --> 00:02:49,920
you are going back again to your moment-to-moment experience and just becoming aware of

33
00:02:49,920 --> 00:02:56,680
the practical aspects of the teaching.

34
00:02:56,680 --> 00:03:03,280
But as to the amount, yeah you really need to meditate more than you go on YouTube and

35
00:03:03,280 --> 00:03:05,120
listen to my talks.

36
00:03:05,120 --> 00:03:12,120
I would say if you have watched all my talks, that is probably too much.

37
00:03:12,120 --> 00:03:16,120
The best thing would be if you could watch a selection of them and then go meditate or

38
00:03:16,120 --> 00:03:27,440
I don't know, it depends if, but not just talking about my talks, but you use it in moderation.

39
00:03:27,440 --> 00:03:35,200
And I guess I've only got what, 300 and some videos, if it was just mine, then one a day,

40
00:03:35,200 --> 00:03:41,120
you've got a whole year's worth or two a day or whatever, and then there's other teachings

41
00:03:41,120 --> 00:03:43,960
from all sorts of different traditions, all different teachers.

42
00:03:43,960 --> 00:03:51,000
It has to be in moderation and I think there are people out there.

43
00:03:51,000 --> 00:04:01,000
This is an endemic, this is a problem in Buddhism that people become intellectual Buddhists

44
00:04:01,000 --> 00:04:11,600
and do a lot of studying and sometimes a lot of talking, discussing, arguing, debating,

45
00:04:11,600 --> 00:04:13,000
thinking.

46
00:04:13,000 --> 00:04:16,160
If you watched my talk, this really can be what really I like, this talk, this really

47
00:04:16,160 --> 00:04:20,440
can meditators who I've talked to, really like this one about, I really like it too.

48
00:04:20,440 --> 00:04:21,440
I give this talk often.

49
00:04:21,440 --> 00:04:23,440
It's not my talk actually.

50
00:04:23,440 --> 00:04:28,640
First of all, it's based on the Sutta and second of all, it's a talk my teacher always gives.

51
00:04:28,640 --> 00:04:35,640
That's why I picked it up on the five types of people in the world, only one of which

52
00:04:35,640 --> 00:04:40,880
is said to be someone, or five types of Buddhists, only one of which is said to be someone

53
00:04:40,880 --> 00:04:43,280
who lives by the Dhamma.

54
00:04:43,280 --> 00:04:48,280
So it's, I think the title is called One Who Lives by the Dhamma or something like that.

55
00:04:48,280 --> 00:04:55,440
Nepali is Dhamma Rihari and so a person who just studies the Buddhist teaching, this

56
00:04:55,440 --> 00:05:02,520
isn't considered to be a Dhamma Rihari person who studies and then goes and teaches

57
00:05:02,520 --> 00:05:10,400
others, this isn't a Dhamma Rihari person who thinks about the teachings, goes and considers

58
00:05:10,400 --> 00:05:16,080
and reflects and so on, this isn't a Dhamma Rihari person who chants and recites and memorizes

59
00:05:16,080 --> 00:05:17,080
the teachings.

60
00:05:17,080 --> 00:05:23,320
This is also isn't a Dhamma Rihari, but a person who does two things, it's considered to

61
00:05:23,320 --> 00:05:28,000
be someone who lives by the Dhamma person who takes the teachings and the Buddha uses the

62
00:05:28,000 --> 00:05:33,600
word studies, a person after they study, after they become full of knowledge of the

63
00:05:33,600 --> 00:05:35,160
Buddha's teaching.

64
00:05:35,160 --> 00:05:40,440
So he says, he's clear that the knowledge is a good thing.

65
00:05:40,440 --> 00:05:46,480
Then they go off and practice tranquility meditation, calm their mind down and develop

66
00:05:46,480 --> 00:05:47,480
insight.

67
00:05:47,480 --> 00:05:58,000
Utherin-chasa-pañaya-a-tung-pa-jana-ti, they come to see clearly the meaning of that knowledge,

68
00:05:58,000 --> 00:06:09,480
the meaning or the essence, the realization of what is meant by the teachings.

69
00:06:09,480 --> 00:06:14,040
So we can memorize the teachings and we can logically accept them and we hear any

70
00:06:14,040 --> 00:06:20,760
Jang-du-kang-an-a-ti and permanence, suffering non-sop, but the actual meaning we get from

71
00:06:20,760 --> 00:06:31,300
the practice and Utherin-chasa-pañaya, Utherin-chasa-pañaya, Utherin-chasa-pañaya, Utherin-chasa-pañaya,

72
00:06:31,300 --> 00:06:40,200
Utherin-chasa-pañaya, Utherin-a-tung, I think, is to look at the grammar, but a higher meaning

73
00:06:40,200 --> 00:06:49,680
with Utherin-chasa-pañaya, meaning that is higher than that knowledge, something like that.

74
00:06:49,680 --> 00:06:54,640
So yeah, don't watch too much, don't let it, don't let yourself be just as Buddha said

75
00:06:54,640 --> 00:07:02,400
at the end of the sutta, he said, don't be negligent, don't let, don't just be a person

76
00:07:02,400 --> 00:07:07,920
who studies a lot or so on, go and meditate after, go and meditate after you've studied

77
00:07:07,920 --> 00:07:11,200
or based on your studies.

78
00:07:11,200 --> 00:07:15,040
So if you're not meditating at all, then you've got a problem, we're just studying and

79
00:07:15,040 --> 00:07:21,080
so on, or if you're only meditating a little bit, but the purpose is to meditate, studying

80
00:07:21,080 --> 00:07:24,280
is just a roadmap.

81
00:07:24,280 --> 00:07:28,640
If you don't read the roadmap, it's hard to find your way, but if you just sit there reading

82
00:07:28,640 --> 00:07:44,760
the roadmap, it's a problem.

