1
00:00:00,000 --> 00:00:10,000
Some people are born with all these good qualities from day one.

2
00:00:10,000 --> 00:00:17,000
Is that purely due to their same sorry training?

3
00:00:17,000 --> 00:00:25,000
I think it's going to be a mixture because they'll be influenced by their past karma,

4
00:00:25,000 --> 00:00:30,000
but then what's currently happening is going to influence them too.

5
00:00:30,000 --> 00:00:36,000
So a person could be seeing that so it makes it hard if you say day one

6
00:00:36,000 --> 00:00:39,000
because you don't know how a person is from day one,

7
00:00:39,000 --> 00:00:42,000
you're not going to know how a person is until they're older

8
00:00:42,000 --> 00:00:45,000
and have been raised by their parents, by their teachers,

9
00:00:45,000 --> 00:00:50,000
by their friends, by all these different factors that come into play.

10
00:00:50,000 --> 00:00:59,000
So it's going to be a mix of what is currently coming in now in the past.

11
00:00:59,000 --> 00:01:08,000
And this formulates a whole new things that so keeps the becoming happening over and over again

12
00:01:08,000 --> 00:01:13,000
is that they're constantly feeding all these new things into it,

13
00:01:13,000 --> 00:01:15,000
just like with the thinking question.

14
00:01:15,000 --> 00:01:19,000
Formulating all these different tasks and ideas and opinions about this or that,

15
00:01:19,000 --> 00:01:23,000
you're constantly feeding something new to a rise.

16
00:01:23,000 --> 00:01:28,000
So I think it's going to be a mix.

17
00:01:28,000 --> 00:01:31,000
What is a misnot deterministic?

18
00:01:31,000 --> 00:01:36,000
It's not, this person wasn't like that from day one,

19
00:01:36,000 --> 00:01:39,000
they did develop those qualities.

20
00:01:39,000 --> 00:01:44,000
Throughout their life, another thing I wanted to say just quickly is

21
00:01:44,000 --> 00:01:49,000
day one is actually a bit of a, it's not early enough actually because

22
00:01:49,000 --> 00:01:51,000
they're already developing in the womb.

23
00:01:51,000 --> 00:01:57,000
It was an interesting study done about babies in the womb

24
00:01:57,000 --> 00:02:03,000
that when they came out, when they were first born,

25
00:02:03,000 --> 00:02:08,000
they would cry in the accent of their parents

26
00:02:08,000 --> 00:02:11,000
because they had heard this sound, the voice of their mother

27
00:02:11,000 --> 00:02:14,000
and they would cry in that accent, according to that accent.

28
00:02:14,000 --> 00:02:17,000
And this study went on and on about the various things,

29
00:02:17,000 --> 00:02:21,000
you know, based on the foods that the parents ate while they were in the womb,

30
00:02:21,000 --> 00:02:27,000
based on the stress or relaxation of the parents and so on.

31
00:02:27,000 --> 00:02:31,000
The other thing I wanted to say, I don't know if Paulini has anything

32
00:02:31,000 --> 00:02:38,000
you can add after, is that karma,

33
00:02:38,000 --> 00:02:42,000
I don't think we should understand karma to be something magical.

34
00:02:42,000 --> 00:02:46,000
Like you kill someone and then the next life you're going to be killed.

35
00:02:46,000 --> 00:02:50,000
It's pretty clear that the Buddha was going to try to point this out as well.

36
00:02:50,000 --> 00:02:58,000
He said sometimes a person can do a very simple negative act and go to help for it.

37
00:02:58,000 --> 00:03:02,000
Someone can do a lot of horrible deeds and not go to help for it.

38
00:03:02,000 --> 00:03:06,000
But I said I see this, I see how some people do a lot of bad deeds

39
00:03:06,000 --> 00:03:10,000
and still in the end possible that they could even wind up in heaven.

40
00:03:10,000 --> 00:03:15,000
And the person can do a very trifling evil deed and still wind up in help.

41
00:03:15,000 --> 00:03:19,000
Part of this is because of course because of the nature of how the mind takes.

42
00:03:19,000 --> 00:03:25,000
If someone does a lot of bad deeds and then is remorseful and sees the danger in it,

43
00:03:25,000 --> 00:03:30,000
or the harm and what they've done, and strives to change themselves.

44
00:03:30,000 --> 00:03:34,000
They can overcome it and they can end up becoming a good person.

45
00:03:34,000 --> 00:03:40,000
And actually blocking out the karma or reducing interest on.

46
00:03:40,000 --> 00:03:48,000
Of course this is a spectrum that is a controversial subject of whether you can actually cut off karma or so on.

47
00:03:48,000 --> 00:03:56,000
But the other part of it is that first of all karma is only one of the forces that work in the universe.

48
00:03:56,000 --> 00:04:00,000
And second of all, karma is natural. It's a part of nature.

49
00:04:00,000 --> 00:04:08,000
It only talks about the inclination or the tendency or the effect.

50
00:04:08,000 --> 00:04:12,000
The addition that an act will put into the pot.

51
00:04:12,000 --> 00:04:20,000
But when you have all these different factors at play, this one act may not appear to have a negative consequence at all.

52
00:04:20,000 --> 00:04:26,000
It's like if you put salt into a dish, it still may turn out that the dish doesn't taste salty at all.

53
00:04:26,000 --> 00:04:30,000
Depending on what else is in the dish and when you taste it, you may not notice the salt.

54
00:04:30,000 --> 00:04:33,000
It may not be the first thing that you notice.

55
00:04:33,000 --> 00:04:37,000
There may be so much sugar that it actually tastes sweet.

56
00:04:37,000 --> 00:04:41,000
And it may be that there's so much water that you can't actually taste the salt.

57
00:04:41,000 --> 00:04:49,000
However, the many different factors, it can also happen that the salt reacts with something and turns into something totally different.

58
00:04:49,000 --> 00:05:00,000
So, obviously, the obvious answer to your question is that yes, that person's probably done some pretty good things to be born.

59
00:05:00,000 --> 00:05:05,000
A genius or an angel or so on.

60
00:05:05,000 --> 00:05:10,000
Why some children are angelic, some other children are terrors and seems to have nothing to do with.

61
00:05:10,000 --> 00:05:17,000
They're bringing nothing to do with their parents, even their genetics or so on.

62
00:05:17,000 --> 00:05:23,000
In certain cases, why different children come out of the woman aren't crying at all.

63
00:05:23,000 --> 00:05:27,000
Some of them cry all night, some cry when they get in the car.

64
00:05:27,000 --> 00:05:34,000
My brother and I were like this, my older brother, the only way they could put him to sleep was to get him in the car and drive him around.

65
00:05:34,000 --> 00:05:39,000
As soon as I got in the car, I would start wailing and crying and I hated to be in the car.

66
00:05:39,000 --> 00:05:40,000
Totally different.

67
00:05:40,000 --> 00:05:44,000
My older brother, two years separated.

68
00:05:44,000 --> 00:05:53,000
This kind of thing surely has to do with some karmic or some inherent qualities.

69
00:05:53,000 --> 00:05:54,000
They call wasana.

70
00:05:54,000 --> 00:06:02,000
Wasana means those things that you carry with you or that you bring from one life to the next.

71
00:06:02,000 --> 00:06:05,000
But it doesn't have to be totally that way.

72
00:06:05,000 --> 00:06:14,000
Things can twist and turn in all sorts of ways, it's just the way physics works or how science works.

73
00:06:14,000 --> 00:06:16,000
It happens quite scientifically.

74
00:06:16,000 --> 00:06:20,000
This is why the Buddha said karma is something that you really can't understand.

75
00:06:20,000 --> 00:06:27,000
Only a Buddha could possibly understand and know what deed leads to what result and what has led this person to be like this.

76
00:06:27,000 --> 00:06:37,000
It's an example of how difficult it is to predict the weather seven days in advance.

77
00:06:37,000 --> 00:06:41,000
Just so many different factors to consider.

78
00:06:41,000 --> 00:06:46,000
But simple answer is, yeah, it most likely has to do with it.

79
00:06:46,000 --> 00:06:50,000
I'm sorry, training, but not entirely.

80
00:06:50,000 --> 00:06:58,000
I was not going to say no point in doubt quite correctly.

81
00:06:58,000 --> 00:07:01,000
People can take lemons and make lemonade.

82
00:07:01,000 --> 00:07:05,000
People with bad lives can turn out to be great people.

83
00:07:05,000 --> 00:07:09,000
Some people with great lives can turn out to be bad people.

84
00:07:09,000 --> 00:07:12,000
People can change.

85
00:07:12,000 --> 00:07:16,000
There's an example is the Tamiya Jotika.

86
00:07:16,000 --> 00:07:19,000
Tamiya came from hell and was born with prince in the same kingdom where he had been a king.

87
00:07:19,000 --> 00:07:23,000
So he had been a terrible person before he had gone to hell.

88
00:07:23,000 --> 00:07:32,000
But as a result of that, he realized that he had to do something to change himself.

89
00:07:32,000 --> 00:07:36,000
Or else he was going to grow up and be the king again and start killing people.

90
00:07:36,000 --> 00:07:41,000
So he was sitting on his king's lap when he was like, what, six months old or three months old.

91
00:07:41,000 --> 00:07:49,000
And the king, they brought these prisoners to the king and they said, the king said, chop off that man's head.

92
00:07:49,000 --> 00:07:53,000
Give this man 10 lashes and excommunicate this man or whatever.

93
00:07:53,000 --> 00:07:57,000
So take all his money, put him in, throw him in the dungeon, torture him.

94
00:07:57,000 --> 00:08:05,000
And this baby, who's the bodhisattva, became quite scared.

95
00:08:05,000 --> 00:08:18,000
There was an angel there in the white umbrella and the angel told him that he was his fate or something to become the king.

96
00:08:18,000 --> 00:08:27,000
And he got totally scared and the angel said, but if you pretend to be deaf and if you pretend to be dumb,

97
00:08:27,000 --> 00:08:34,000
they won't make your king though, they'll cast you away and you won't have to become king and then you won't have to do all sorts of bad deeds and go to hell.

98
00:08:34,000 --> 00:08:41,000
And so thinking about hell, he was like, he was still young enough to remember it.

99
00:08:41,000 --> 00:08:50,000
He hadn't gone through the horrors of being a young child and forgetting about all that.

100
00:08:50,000 --> 00:09:00,000
And so he took this to heart and from that point on, if you read the Taimi Adjada, it's wonderful to hear that they thought this baby doesn't cry out.

101
00:09:00,000 --> 00:09:05,000
Well, is there something wrong with him? Maybe he's mute.

102
00:09:05,000 --> 00:09:11,000
And so they put lots of sweets around and what is that to thought to himself?

103
00:09:11,000 --> 00:09:18,000
If I cry for those sweets, I'm going to wind up in hell and so he wouldn't take the sweets and so they tried to starve him.

104
00:09:18,000 --> 00:09:29,000
And make him cry and he felt so much hunger but he thought to himself, the pain of hell is far worse than the pain of this hunger.

105
00:09:29,000 --> 00:09:31,000
So he forbear with it.

106
00:09:31,000 --> 00:09:37,000
And for 16 years they tested him again and again and again, thinking there's nothing wrong with him.

107
00:09:37,000 --> 00:09:42,000
So he's a perfectly, the doctors came and said he's perfectly fine and so on.

108
00:09:42,000 --> 00:09:53,000
But for 16 years he can say he took a very bad situation that he had got himself into and turned it around, made some new good excellent karma.

109
00:09:53,000 --> 00:10:07,000
And in the end they threw him out. They decided they were going to kill him. So they sent him off with his guard, with his caretaker to be hit over the head and put in a shallow grave and buried there.

110
00:10:07,000 --> 00:10:14,000
He says this son is useless. He's inauspicious and he's going to create trouble for the kingdom.

111
00:10:14,000 --> 00:10:18,000
So when he gets to the forest he stands up.

112
00:10:18,000 --> 00:10:25,000
So he tests out his strength, find out he still can move, he's still strong. And he says to the guy he says,

113
00:10:25,000 --> 00:10:28,000
thanks for taking me here. You can go home now.

114
00:10:28,000 --> 00:10:33,000
The guy looks up and who's that? Who are you? He didn't ever see him talk before.

115
00:10:33,000 --> 00:10:39,000
He says, I'm your prince. I've decided not to become a kingdom going to go forth.

116
00:10:39,000 --> 00:10:42,000
And so he sends him back home and goes off and becomes an ascetic.

117
00:10:42,000 --> 00:10:45,000
Then the whole kingdom follows him and eventually they all become ascetic.

118
00:10:45,000 --> 00:10:52,000
This is the Tamiya Jataka very famous point being he takes a very bad situation, even bad karma you might say.

119
00:10:52,000 --> 00:11:01,000
And turns it into a wonderful thing and now he winds up being emptying a whole kingdom and turning them all into ascetics.

120
00:11:01,000 --> 00:11:10,000
Instead of following his karma and becoming king again and carrying on the horrible tradition of his father.

121
00:11:10,000 --> 00:11:15,000
I don't have anything to add to that.

