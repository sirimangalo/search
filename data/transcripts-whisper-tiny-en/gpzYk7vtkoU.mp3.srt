1
00:00:00,000 --> 00:00:08,440
Where some people fade when they are praying with music in church, some told me that

2
00:00:08,440 --> 00:00:14,760
Holy Spirit goes into their body when they fall down, but I find it weird, is it because

3
00:00:14,760 --> 00:00:18,120
of emotional, too high or any other reason?

4
00:00:18,120 --> 00:00:27,000
There's an interesting documentary called Jesus Camp that my uncle showed me when I was in

5
00:00:27,000 --> 00:00:35,880
Thailand. That actually shows examples of six-year-old kids with the Holy

6
00:00:35,880 --> 00:00:46,360
Spirit, shaking them up, shaking them down. It's amazing how naive people can be

7
00:00:46,360 --> 00:00:51,320
know how easy it is to convince people that's gone, that's the Holy Spirit.

8
00:00:51,320 --> 00:00:55,240
It's like this faith healing people putting out laying on hands. Sorry, I'm

9
00:00:55,240 --> 00:01:03,600
going to get called out for this, but by making fun of other religions, this is the

10
00:01:03,600 --> 00:01:12,920
religious thing. I don't really, maybe I shouldn't even go this far, but the

11
00:01:12,920 --> 00:01:19,800
point is that we can be so gullible. I have been incredibly gullible in my day,

12
00:01:19,800 --> 00:01:36,560
but that's all it is really. We have false attributions and associations, even

13
00:01:36,560 --> 00:01:41,960
false memories of how things happened and so on. So many things that create

14
00:01:41,960 --> 00:01:58,600
belief, create unsubstantiated belief. And the best way, the way that those people

15
00:01:58,600 --> 00:02:06,840
who promote atheism tend to deal with, is to point out how everything you're

16
00:02:06,840 --> 00:02:13,800
saying is found elsewhere. And so the healing that people say is because Jesus

17
00:02:13,800 --> 00:02:22,320
Christ, gave them the power to heal, is found in, I would say hundreds of

18
00:02:22,320 --> 00:02:28,120
non-Christian circles. How many other religions out there who claim to

19
00:02:28,120 --> 00:02:34,360
practice faith healing? Reiki students do it and they don't generally believe in

20
00:02:34,360 --> 00:02:47,960
Jesus Christ. But rapture, and that's what this is called, that you're asking

21
00:02:47,960 --> 00:02:54,640
about, is far more common. It's like bright lights. People say I saw

22
00:02:54,640 --> 00:03:00,120
bright light, and it was the light of God. Except when I went to Jom-Tong for my

23
00:03:00,120 --> 00:03:05,320
first meditation course, after a few days, I saw bright lights. And I wasn't

24
00:03:05,320 --> 00:03:12,360
Christian. I was Taoist at the time. Was it Jesus trying to tell me that he

25
00:03:12,360 --> 00:03:17,640
wants me to meditate? And then, if you watch Jesus camp, you'll see them go

26
00:03:17,640 --> 00:03:24,960
like this. They're standing in front of the, in front of the parliament

27
00:03:24,960 --> 00:03:29,800
buildings, protesting, protesting abortion, which is a good thing to

28
00:03:29,800 --> 00:03:36,000
protest. I'm with them on that actually. I'm not protesting, but to be

29
00:03:36,000 --> 00:03:42,600
against, I'm totally against that. I'm with them on this one, but so they, and

30
00:03:42,600 --> 00:03:47,320
they're, the rapture comes to them, shaking. You can see them all. It's kind of

31
00:03:47,320 --> 00:03:52,360
funny watching them bob their heads. Let's say this is the holy ghost to

32
00:03:52,360 --> 00:03:59,360
enter their body, right? Except my first meditation course, I went way more than

33
00:03:59,360 --> 00:04:06,040
they went. I didn't ride on the floor like little kids, but let's just scary, but

34
00:04:06,040 --> 00:04:11,840
it's all rapture. And there's no way that Jesus Christ entered my body with

35
00:04:11,840 --> 00:04:17,600
the Holy Spirit. Unless he thought, here's a, here's a, I don't know what to do

36
00:04:17,600 --> 00:04:26,920
you want to tell me. But it sure felt good. And so I kept doing it to

37
00:04:26,920 --> 00:04:31,080
that. Well, this is great. And then I went to tell my teacher, and they totally

38
00:04:31,080 --> 00:04:35,880
demolished me and said, that's useless, that's dangerous and so on, made me

39
00:04:35,880 --> 00:04:40,280
give it up. It's not actually dangerous, but I, I can't remember what they said,

40
00:04:40,280 --> 00:04:45,040
but intended to believe those things were dangerous. It's not really dangerous.

41
00:04:45,040 --> 00:04:51,680
It's just useless. It just gets you stuck in, in the circle. Rapture is caused.

42
00:04:51,680 --> 00:04:55,440
What does it cause by rapture is caused by concentration through the practice

43
00:04:55,440 --> 00:05:01,320
of meditation, you enter into concentrated states. As you enter into concentrated

44
00:05:01,320 --> 00:05:07,560
states, you, you tend to do the same things repeatedly, right? The same

45
00:05:07,560 --> 00:05:11,400
mind states tend to arise more repeatedly than, than an ordinary state where

46
00:05:11,400 --> 00:05:20,120
you're in, in a state of greater chaos. So you're, it's very easy to get into a

47
00:05:20,120 --> 00:05:25,640
repetitious state, where you can, where you cultivate the same state again and again.

48
00:05:25,640 --> 00:05:30,400
And so it can be as simple as, as leaning like this. And it feels good. Oh, that was

49
00:05:30,400 --> 00:05:37,720
nice. And so unconsciously, you find yourself doing it again and again and again

50
00:05:37,720 --> 00:05:42,600
and again and again until eventually it, it's very easy to get into this, this

51
00:05:42,600 --> 00:05:49,840
shaking state. I could do it right now if I wanted. It's not difficult. And so

52
00:05:49,840 --> 00:05:53,240
so many meditators fall into this. And if you take it further and further and

53
00:05:53,240 --> 00:05:56,920
further, it can actually cause you to convulse on the floor and, and all sorts

54
00:05:56,920 --> 00:06:02,320
of crazy things. It's getting stuck in a rut. It's when the mind gets into a

55
00:06:02,320 --> 00:06:10,640
rut. And then sometimes it can't even get out. It gets, it gets caught in it.

56
00:06:14,640 --> 00:06:18,640
The, but, you know, in certain sects of Christianity, they're actually encouraged

57
00:06:18,640 --> 00:06:24,760
to develop it and to cultivate it. So it's, that's why the convulsions come

58
00:06:24,760 --> 00:06:31,720
and so on. It's like these dance, it's like dancing really. It's dancing in

59
00:06:31,720 --> 00:06:36,040
general. Ordinary people dancing. When they dance, they just start convulsing,

60
00:06:36,040 --> 00:06:39,400
right? If you go to a dance, you see a lot of people with the Holy Ghost and

61
00:06:39,400 --> 00:06:44,040
because they're just shaking all over the place, you know, they're shaking to the

62
00:06:44,040 --> 00:06:50,120
rhythm. They're encouraging this kind of rapture of state because that's

63
00:06:50,120 --> 00:06:53,520
really what it is. That's why dancing feels so great because you're in the

64
00:06:53,520 --> 00:07:01,600
state of rapture. Christianity has just found one other way to develop what is in

65
00:07:01,600 --> 00:07:04,280
the end, just a silly and meaningless state. It doesn't make them better people.

66
00:07:04,280 --> 00:07:09,200
It doesn't make them wiser. It doesn't make them more Christian. And it certainly

67
00:07:09,200 --> 00:07:13,880
doesn't mean that the Holy Ghost has entered into their body. It comes back to

68
00:07:13,880 --> 00:07:19,880
again to the false attribution. It, it happened. That's all you can say. You can

69
00:07:19,880 --> 00:07:25,160
say it was the Holy Ghost. I can say it was Satan. No, which is funny. They

70
00:07:25,160 --> 00:07:29,560
actually seem to do that sometimes. When I heal people, it's, it's Jesus. When

71
00:07:29,560 --> 00:07:46,800
you heal people, it's Satan. Anyway, no Christian bashing.

