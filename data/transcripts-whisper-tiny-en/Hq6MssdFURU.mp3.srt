1
00:00:00,000 --> 00:00:27,440
Good evening everyone, welcome to our evening broadcast, the Daily Dhamma.

2
00:00:27,440 --> 00:00:53,560
As promised today we are going to talk about science.

3
00:00:53,560 --> 00:01:01,920
Science is the pursuit of knowledge, science involves the idea that there are things that

4
00:01:01,920 --> 00:01:17,480
we don't know with the implication that we should know.

5
00:01:17,480 --> 00:01:28,520
It's an aspect of nature, it's a part of nature, the state of knowledge, knowledge is

6
00:01:28,520 --> 00:01:32,120
a part of the mind's activity.

7
00:01:32,120 --> 00:01:38,160
There are certain things that we know and there are certain things that we don't know.

8
00:01:38,160 --> 00:01:49,320
On the other hand there's a whole lot of things that we think we know, that maybe we don't

9
00:01:49,320 --> 00:02:01,240
actually know.

10
00:02:01,240 --> 00:02:10,080
And in fact if curiously enough it appears that all of science is merely belief in the

11
00:02:10,080 --> 00:02:11,080
end.

12
00:02:11,080 --> 00:02:24,040
When we talk about science we are really just talking about belief.

13
00:02:24,040 --> 00:02:34,000
Now some belief is resting on what we call evidence and some belief does not rest on evidence.

14
00:02:34,000 --> 00:02:40,080
And so certain religions have actually, interestingly, strangely from a Buddhist perspective

15
00:02:40,080 --> 00:02:50,160
and from a scientific perspective, have postulated certain truths without evidence.

16
00:02:50,160 --> 00:02:54,960
And have postulated a way of understanding truth without evidence.

17
00:02:54,960 --> 00:03:07,120
In other words belief, belief in something without adequate evidence that having admitted

18
00:03:07,120 --> 00:03:14,360
that there is insubstantial evidence, this isn't a boundary for belief.

19
00:03:14,360 --> 00:03:23,840
I think Buddhism would agree with science that this is not a good idea, this is not a really

20
00:03:23,840 --> 00:03:30,520
honest sort of belief that one should hold.

21
00:03:30,520 --> 00:03:37,440
And so science sets itself apart from other beliefs in that it sets a standard for what

22
00:03:37,440 --> 00:03:45,880
one might call adequate evidence, that's a rather high bar for what one might call adequate

23
00:03:45,880 --> 00:03:46,880
evidence.

24
00:03:46,880 --> 00:03:50,960
The science added at its best is quite rigorous.

25
00:03:50,960 --> 00:03:58,680
Now in practice of course as any scientist, as many scientists have told me in practice

26
00:03:58,680 --> 00:04:09,240
it's rarely as airtight as one might hope and often monetary concerns and concerns

27
00:04:09,240 --> 00:04:19,400
ego gets involved and this is the first problem that we find with science, even when

28
00:04:19,400 --> 00:04:34,760
it is at its most rigorous, is that in the quest for knowledge, in our ordinary quest

29
00:04:34,760 --> 00:04:45,400
for knowledge we bring along our personalities, we bring along culture borrowing from what

30
00:04:45,400 --> 00:04:56,320
we talked about yesterday, most scientists bring along culture unknowingly or knowingly

31
00:04:56,320 --> 00:05:11,680
and so at science at its worst involves corrupt scientists, bias exhibiting bias because

32
00:05:11,680 --> 00:05:23,000
of where the money is coming from or in order to get funding or in order to for prestige

33
00:05:23,000 --> 00:05:25,280
and so on.

34
00:05:25,280 --> 00:05:39,200
But science at its best, most science at its best is still coupled with the baggage of culture.

35
00:05:39,200 --> 00:05:45,720
This is generally the culture of what we call physicalism.

36
00:05:45,720 --> 00:05:55,080
So we're born into this world, not with a blank slate and nor are we given the opportunity

37
00:05:55,080 --> 00:06:02,520
to examine the world objectively, we're immediately thrust into a paradigm of people,

38
00:06:02,520 --> 00:06:10,040
people places and things, concepts, our parents don't send out to teach us the abidama

39
00:06:10,040 --> 00:06:19,000
when we're first born, now they teach us, this is apple, this is birdie, this is

40
00:06:19,000 --> 00:06:34,960
mama, this is dada, you just sort of a relational paradigm, where things are spatially

41
00:06:34,960 --> 00:06:49,160
and in terms of their importance and their quality of good and bad, yummy, yummy food,

42
00:06:49,160 --> 00:07:01,280
good girl, good boy, bad girl, bad boy, we're taught, we're taught reality, most especially

43
00:07:01,280 --> 00:07:08,480
when we're taught many unfortunate things as we grow up, but most unfortunate is our

44
00:07:08,480 --> 00:07:18,280
reliance, you know that the human being relies very much on temporal spatial reality means

45
00:07:18,280 --> 00:07:26,160
time and space, we rely on the past and the future just to get by, you know, how many

46
00:07:26,160 --> 00:07:32,640
of us could live our lives without relying on plans for the future or lessons learned

47
00:07:32,640 --> 00:07:45,160
in the past, even our social relationships would fall apart, we'd have to shave our heads,

48
00:07:45,160 --> 00:07:52,400
put on rags and go live in the forest or something, we wouldn't be able to survive

49
00:07:52,400 --> 00:08:06,160
in space, we have to think in terms of things, to think in terms of the physical world,

50
00:08:06,160 --> 00:08:18,900
possessions, belongings, relationships with other people, we're thrust into an impersonal

51
00:08:18,900 --> 00:08:29,640
reality, which is actually somewhat surprisingly counterintuitive, so most people would

52
00:08:29,640 --> 00:08:36,920
say it is completely intuitive, it in fact is from the point of view of the baby who comes

53
00:08:36,920 --> 00:08:45,440
out of the womb and sees stuff, lights, shapes, movements, right, and here's sounds, what

54
00:08:45,440 --> 00:08:53,680
does the baby make of all these things, well, they carry over from their past lives, potentially

55
00:08:53,680 --> 00:09:00,520
beliefs, but to some extent they do have this, there is somewhat of a clean slate you

56
00:09:00,520 --> 00:09:10,800
might argue, they're seeing light, mama, mother will coup at the baby and so on, and they

57
00:09:10,800 --> 00:09:19,080
feel sensations, the harsh, the harsh cloth, they're so different from the womb that they

58
00:09:19,080 --> 00:09:24,440
are the warmth of the mother's womb, the pain for the baby at childbirth, I don't know

59
00:09:24,440 --> 00:09:35,640
if children experience pain coming out of the vaginal cavity, or whatever you call it,

60
00:09:35,640 --> 00:09:48,760
must be quite shocking, little bright lights, the sounds, and then they have to make

61
00:09:48,760 --> 00:09:55,280
sense, what does this all mean and what they're taught, what is impressed upon them and

62
00:09:55,280 --> 00:10:02,160
must be helped along by having lived this life, this sort of life again and again, it's

63
00:10:02,160 --> 00:10:08,000
not simply seeing hearing, smelling, tasting, feeling, thinking we're given this artificial

64
00:10:08,000 --> 00:10:17,000
paradigm of space and time, and the very basis is just seeing hearing, smelly, it's

65
00:10:17,000 --> 00:10:26,080
just experience, and so the first problem with science is that it tends to be very, very

66
00:10:26,080 --> 00:10:37,840
much enmeshed in this paradigm of external reality, instead of focusing on personal experience,

67
00:10:37,840 --> 00:10:50,520
personal experiences seen as unreliable, prone to subjectivity, partiality or bias or errors

68
00:10:50,520 --> 00:11:19,240
in judgment, and the second problem with science, this is the first way that it differs

69
00:11:19,240 --> 00:11:25,960
from Buddhism, Buddhism, and we'll talk about Buddhist science, Buddhism claims some to some extent

70
00:11:25,960 --> 00:11:35,160
to sort of a science, but the other problem is that Buddhism isn't actually about knowledge,

71
00:11:35,160 --> 00:11:40,560
wisdom isn't the goal, I often talk about and say that wisdom is the goal, but it's just

72
00:11:40,560 --> 00:11:46,440
shorthand for describing what the real goal is and the real goal is happiness, peace, freedom

73
00:11:46,440 --> 00:11:56,080
from suffering, and that's an important difference because science is getting obsessed with

74
00:11:56,080 --> 00:12:07,680
the knowledge, irrespective of whether it actually makes you happy or brings peace, right?

75
00:12:07,680 --> 00:12:12,600
If there's a discovery to be had, there's a scientist who will, all scientists will jump

76
00:12:12,600 --> 00:12:17,160
at the opportunity to be the one to discover, right?

77
00:12:17,160 --> 00:12:20,800
And so to some extent, there's a sense that it makes you happy to be the one who discovers

78
00:12:20,800 --> 00:12:28,480
something, but that's not what Buddhism is about, what is a message about this obsession

79
00:12:28,480 --> 00:12:35,080
with knowledge, and so we talk about the four noble truths and the important word here

80
00:12:35,080 --> 00:12:42,120
that we often gloss over is noble, Arya, that's what makes them special, not that they're

81
00:12:42,120 --> 00:12:50,760
the truth, we're not so concerned about the truth honestly, we're concerned about becoming

82
00:12:50,760 --> 00:12:58,880
free from suffering because you can't really ever know the truth, we say the truth is

83
00:12:58,880 --> 00:13:04,760
that everything nothing is worth clinging to, well you can't know that, right?

84
00:13:04,760 --> 00:13:08,280
There could be something around the next corner, if you just meditated a little bit longer

85
00:13:08,280 --> 00:13:14,560
maybe you'd find something that was worth clinging to.

86
00:13:14,560 --> 00:13:18,600
Meditation even relies, what we talk about the four noble truths even they rely on belief.

87
00:13:18,600 --> 00:13:28,900
It's called Anumanah, Anumanah means in inference, you have to infer, so it's described

88
00:13:28,900 --> 00:13:37,120
as that when a person realizes the four noble truths, it's an epiphany, it's not a realization

89
00:13:37,120 --> 00:13:43,760
of actual knowledge because you can't actually know, but you get to the point where you

90
00:13:43,760 --> 00:13:51,160
have sufficient evidence, very much like science, but it's sufficient for the mind, where

91
00:13:51,160 --> 00:13:57,280
the mind finally says, I get it, it's true, nothing is worth clinging to, now it doesn't

92
00:13:57,280 --> 00:14:13,200
yet know that because it hasn't seen all phenomena, it has only experienced one phenomenon.

93
00:14:13,200 --> 00:14:25,120
Now maybe you could argue, maybe you could argue that you're seeing that one phenomenon

94
00:14:25,120 --> 00:14:34,000
so clearly that you understand the nature of phenomena, because all phenomena are the same,

95
00:14:34,000 --> 00:14:42,160
all phenomena are the same, but it's still got to be an inference of sorts because theoretically

96
00:14:42,160 --> 00:14:47,560
you could be something, and this is why scientists intellectually they say, you can never

97
00:14:47,560 --> 00:14:51,880
prove a theory, you can only disprove it, and you're always just waiting for the disprove,

98
00:14:51,880 --> 00:15:00,560
and if there is never a disprove, well then you accept it for provisionally.

99
00:15:00,560 --> 00:15:06,760
Meditation isn't like that, there is a clarity, a perfect clarity that comes, but it's

100
00:15:06,760 --> 00:15:15,520
only in relation to one thing, but there's something quite special about this, you see,

101
00:15:15,520 --> 00:15:21,080
I mean it's a very different state than intellectually saying, oh yes, the speed of light

102
00:15:21,080 --> 00:15:32,120
is whatever equals MC squared, matter, and this kind of intellectual reasoning, it doesn't

103
00:15:32,120 --> 00:15:38,520
have any profound impact on your state of mind, though it may change the way you look

104
00:15:38,520 --> 00:15:44,920
and the way you think about reality, it doesn't actually change your outlook, the nature

105
00:15:44,920 --> 00:15:54,000
of your mind, the foreignable truths are quite special in that they do change, that seeing

106
00:15:54,000 --> 00:16:04,880
certain realities have a fundamental impact on the nature of the mind, so in Buddhism

107
00:16:04,880 --> 00:16:13,040
we talk about the power of knowledge, the power of insight or wisdom, but really we're

108
00:16:13,040 --> 00:16:23,400
talking about knowledge, nyana, there are certain knowledges that, well there's a certain type

109
00:16:23,400 --> 00:16:29,000
of knowledge that doesn't have the same impact, so we call them sutamayapanya, this is

110
00:16:29,000 --> 00:16:33,240
knowledge that you've heard about and jintamayapanya, as knowledge that you've thought

111
00:16:33,240 --> 00:16:38,800
up on your own, these ones don't have a profound impact on your life, even knowledge

112
00:16:38,800 --> 00:16:43,760
of Buddhism, there are Buddhists who know very, very much about people who have read the

113
00:16:43,760 --> 00:16:49,200
whole of the Buddhist teaching once, twice over, but that doesn't have a profound impact

114
00:16:49,200 --> 00:16:54,400
on your mind, it may seem like it does, you may feel wow, reading all this Buddhist

115
00:16:54,400 --> 00:17:01,720
teachings, it's really changed my outlook on reality and okay, so to some extent it might,

116
00:17:01,720 --> 00:17:11,120
but that's not the effect of insight meditation practice, it's not the same thing, it's

117
00:17:11,120 --> 00:17:22,840
at the same as observing reality moment by moment, watching experiences arise and cease,

118
00:17:22,840 --> 00:17:29,760
and really viscerally feeling the change come about in the way you look at reality, seeing

119
00:17:29,760 --> 00:17:39,160
your ego, seeing your attachment, seeing your dissatisfaction and your irritability and

120
00:17:39,160 --> 00:17:50,040
inability to bear with reality and overcoming it, changing, watching your mind change as

121
00:17:50,040 --> 00:18:01,080
you see your flaws, as you see your mistakes, as I said last night I think I said that

122
00:18:01,080 --> 00:18:11,040
where once the scientist and the lab rat, that's a crucial distinction is we're experimenting

123
00:18:11,040 --> 00:18:18,800
on ourselves, we're not experimenting but we're studying ourselves, and so this is

124
00:18:18,800 --> 00:18:23,880
the arguably something that Buddhism has over science, that science doesn't actually

125
00:18:23,880 --> 00:18:30,000
address one's culture, doesn't actually address the paradigm, the way one looks at reality

126
00:18:30,000 --> 00:18:40,000
in general, it generally doesn't, it makes certain assumptions, it looks at reality in a

127
00:18:40,000 --> 00:18:52,640
certain way, Buddhism is simply the science of the individual, knowledge about the individual

128
00:18:52,640 --> 00:19:06,520
about one's own mind, science of experience maybe, and so what we're concerned with is

129
00:19:06,520 --> 00:19:16,440
merely an understanding of our own habits, our own behaviors, nature of our own mind,

130
00:19:16,440 --> 00:19:21,680
how we like and dislike certain things, how we build up habits based on likes and dislikes,

131
00:19:21,680 --> 00:19:35,800
how we cultivate ego and identification, all of this is the object of insight meditation,

132
00:19:35,800 --> 00:19:55,920
and so this is important as meditators that we don't get obsessed with knowledge, even

133
00:19:55,920 --> 00:20:00,800
with insight knowledge, it's not about those aha moments where you get it where you realize

134
00:20:00,800 --> 00:20:07,440
oh, I get, I understand, I see impermanence, I see suffering, I see non-self, meditators

135
00:20:07,440 --> 00:20:16,480
will often describe these experiences, kind of excited and happy, but it's not that

136
00:20:16,480 --> 00:20:23,600
experience, that it's not the knowledge that's important, it's the state of one's mind,

137
00:20:23,600 --> 00:20:32,040
so even insight knowledge is not something you should cling to or see as the goal, it's

138
00:20:32,040 --> 00:20:48,080
encouraging of course to see these things, but even when we see reality, even that can

139
00:20:48,080 --> 00:20:55,160
become an object of clinging if we like or if we're excited about it, and we have to

140
00:20:55,160 --> 00:21:00,840
remind ourselves that it's about us, it's not about that knowledge, it's about how we react

141
00:21:00,840 --> 00:21:12,000
to it, how we react to reality, how we interact with our experience, don't get caught

142
00:21:12,000 --> 00:21:20,920
up in the intellectualizing or thinking about meditation practice, even when you experience

143
00:21:20,920 --> 00:21:26,720
knowledge, the Buddha said it's like a raft, a raft has a purpose, and once its purpose

144
00:21:26,720 --> 00:21:33,440
is over, you don't pick the raft up and walk away with it, you throw it away, when

145
00:21:33,440 --> 00:21:38,880
you use a raft across the river, well good for you, good for the raft, that was a good

146
00:21:38,880 --> 00:21:43,920
raft, a good raft doesn't mean you pick it up and carry it with you, knowledge is the

147
00:21:43,920 --> 00:21:55,320
same, throw it away once you cross the river, keep going, so there you go, some brief thoughts

148
00:21:55,320 --> 00:22:02,200
about science, Buddhism could in some ways be considered a science, in other ways it's

149
00:22:02,200 --> 00:22:10,320
not to talk about the science, it's all about the results, so we only worry about a certain

150
00:22:10,320 --> 00:22:19,480
type of science, what of course is the tool, without that science, without that knowledge,

151
00:22:19,480 --> 00:22:23,400
without the realizations you could never become enlightened, that's the other side of

152
00:22:23,400 --> 00:22:27,640
this coin is that anyone who thinks they're just going to sit and through calming the

153
00:22:27,640 --> 00:22:34,120
mind somehow become enlightened as a engine for a rude awakening, well no, he's in for years

154
00:22:34,120 --> 00:22:40,600
and years of pointless exercise, where they just sit in our calm and peaceful and become

155
00:22:40,600 --> 00:22:48,200
attached and even egotistical about their peace, meditation, meditation of that sort can't

156
00:22:48,200 --> 00:22:56,600
for you from suffering, without science, without knowledge, without realizing the truth,

157
00:22:56,600 --> 00:23:02,560
and permanent suffering non-self, that nothing is worth clinging to, even calm and peace

158
00:23:02,560 --> 00:23:14,040
and tranquility, without realizing that you can't become enlightened, so there you go, there's

159
00:23:14,040 --> 00:23:32,080
our bit of dhamma for tonight, are you leaving tomorrow or soon, okay, I want to say hello

160
00:23:32,080 --> 00:23:40,000
to everyone, hello everyone, tell us about what happened, tell us a little bit about just

161
00:23:40,000 --> 00:23:45,280
what happened, why am I having you say hello, well I'm finishing, come close, come this

162
00:23:45,280 --> 00:23:55,320
side, I'm finishing my stay here and introduce yourself, my name is Will Will Stevenson

163
00:23:55,320 --> 00:24:07,720
I'm from New York and I just completed my program, how is it, very revealing for me, you

164
00:24:07,720 --> 00:24:13,800
know, now when I hear the talks, they mean different things to me, because I've directly

165
00:24:13,800 --> 00:24:22,200
experienced some things I had only an intellectual appreciation for before, and you know

166
00:24:22,200 --> 00:24:29,840
I'm probably, I've learned about myself that I am one of the types of people that cling

167
00:24:29,840 --> 00:24:38,320
some to the intellectual appreciation, you know, I thought through knowledge that a greater

168
00:24:38,320 --> 00:24:46,480
appreciation for peace would come, but that is certainly not the case, you know, and really

169
00:24:46,480 --> 00:24:55,760
only through direct observation of the simple things of life and a very kind of I guess

170
00:24:55,760 --> 00:25:03,640
myopic controlled situation as it, you know, again it's not, you would say that tonight

171
00:25:03,640 --> 00:25:14,000
it's not like I had aha moments, it's really just exposed to me that it's the tip of the iceberg,

172
00:25:14,000 --> 00:25:22,560
you know, so now I'm, we've moved from the unknown unknowns into the unknown unknowns,

173
00:25:22,560 --> 00:25:27,560
right, tip of the iceberg, well that's it, that is really what the foundation course

174
00:25:27,560 --> 00:25:32,480
it should show you the iceberg and say this is what you've got to work on, it's a little

175
00:25:32,480 --> 00:25:39,440
oh, but it'll scare you, yeah, yeah, the first step, it's a good way, you know, thank

176
00:25:39,440 --> 00:25:52,280
you well, thank you so much, there you go, it's still alive, it's not, it's not such a scary

177
00:25:52,280 --> 00:26:00,360
thing to do the course, people do survive it, I've never had to bury anyone, so my teachers

178
00:26:00,360 --> 00:26:06,760
said, you know, he's say if you die, I'll do your funeral for free, I mean the joke is that

179
00:26:06,760 --> 00:26:13,240
monks go to funerals, they do ceremonies for when people die, so and it costs a lot of money,

180
00:26:13,240 --> 00:26:17,200
you don't, you pay the monk, you give monks usually donations a little bit, but it's not

181
00:26:17,200 --> 00:26:25,960
such as that, there's of course lots of costs that go in and, anyway, it says I'll do

182
00:26:25,960 --> 00:26:39,800
the chanting free, or I think it's more than that he would probably arrange the funeral,

183
00:26:39,800 --> 00:26:46,000
we'd arrange a funeral, if anyone died meditating, we'd for sure, it should roll, right,

184
00:26:46,000 --> 00:26:54,920
for oven, at least we could do, it's on our waiver, our waiver says you can, you have

185
00:26:54,920 --> 00:27:04,680
to be aware that meditation can lead to death, among other things, we just, we were discussing

186
00:27:04,680 --> 00:27:09,840
this, the question of how do we, how do we work on, you know, how do we make sure to

187
00:27:09,840 --> 00:27:15,040
not, you know, get into legal battles and so on, you know, how do we, how do we avoid having

188
00:27:15,040 --> 00:27:18,280
problems with medicine, well, let's, let's just put a waiver and say, you know, it's

189
00:27:18,280 --> 00:27:23,680
complete to death and dismemberment and so on, and you're aware and you're not going to

190
00:27:23,680 --> 00:27:43,040
hold this responsible, even if it kills you, so far no one's died, did I see a question

191
00:27:43,040 --> 00:27:55,200
there, can we take scientific knowledge as wisdom, isn't wisdom all about good judgment

192
00:27:55,200 --> 00:28:02,840
and knowing what is beneficial, I mean, this is semantics, it depends what you mean by wisdom,

193
00:28:02,840 --> 00:28:10,200
I think the Buddha would, let me see, now I think in orthodox Buddhism, yes, it would

194
00:28:10,200 --> 00:28:14,160
have to be something to do with Buddhism actually, it would have to be in line with the

195
00:28:14,160 --> 00:28:23,440
Buddha's teaching, so knowledge of, knowledge of the atom bomb isn't actually wisdom and

196
00:28:23,440 --> 00:28:27,600
so that's really, I may, I kind of mentioned that I said it's, you know, we're not obsessed

197
00:28:27,600 --> 00:28:34,720
with knowledge or obsessed with what is beneficial or focused on, on that the results of

198
00:28:34,720 --> 00:28:45,160
good knowledge, nyanapanya and we do refer to wisdom, nyanamundapadi, takumundapadi, nyanamundapadi,

199
00:28:45,160 --> 00:28:51,760
yeah, so they're all, I mean, they're also anonymous, they're just, I mean, they're used

200
00:28:51,760 --> 00:28:58,200
synonymously, they're not actually synonymously, nyanam means knowledge, it's very clear,

201
00:28:58,200 --> 00:29:04,640
nyanapanya, interestingly enough, is just nyanapanya with a pot on the front, so panyanana,

202
00:29:04,640 --> 00:29:12,520
the difference is the pot, because the root is nyanana is turning the root to know into

203
00:29:12,520 --> 00:29:21,440
knowledge, and panya is adding, adding an A on the end, discerning nyanana, and then

204
00:29:21,440 --> 00:29:27,800
the pot becomes, so pot is separate, panya is actually, if, in the dictionary, it would be different,

205
00:29:27,800 --> 00:29:35,520
nyanana will just be any type of knowledge, but panya is like wisdom, true knowledge, that

206
00:29:35,520 --> 00:29:45,160
kind of thing, readya, readya is a little bit different flavor, so readya comes from weed,

207
00:29:45,160 --> 00:29:54,040
which is where you get the word wadana, so wadana means something that you experience,

208
00:29:54,040 --> 00:29:59,640
so weedja is like experience, it's used, it's translated often, it's knowledge, and it's

209
00:29:59,640 --> 00:30:07,640
used as knowledge, but it's a knowledge that you get from experience, so it's a different,

210
00:30:07,640 --> 00:30:14,080
a whole different root, but, you know, culturally, even in the Buddhist time, it had come

211
00:30:14,080 --> 00:30:18,640
out to me in the same thing, or be used in the same way, although there was something

212
00:30:18,640 --> 00:30:29,280
widja is a little bit more experiential, I guess, but special in a way, or is it weight

213
00:30:29,280 --> 00:30:49,400
as a second, readya? Okay, we had a few more questions here on the website, let's go through

214
00:30:49,400 --> 00:30:56,840
them quickly. How important is chanting to the meditation practice, are there any signs

215
00:30:56,840 --> 00:31:03,440
when one reaches a state of true awareness, those are two different questions, chanting is

216
00:31:03,440 --> 00:31:08,520
not really important except the mantra that we use is kind of important, so meditation

217
00:31:08,520 --> 00:31:13,720
is kind of a chant, say it's a pain, pain when you feel pain, but it's a different kind

218
00:31:13,720 --> 00:31:17,720
of chant because the object is what you're experiencing, so it keeps you focused on what

219
00:31:17,720 --> 00:31:23,000
you're experiencing, any other kind of chanting is just going to be a type of meditation

220
00:31:23,000 --> 00:31:29,520
sort of that is conceptual, like 80-piece of bhagavas, mindfulness of the Buddha, that's

221
00:31:29,520 --> 00:31:36,520
good, it's useful, but it's only conventionally useful.

222
00:31:36,520 --> 00:31:40,160
Are there any signs when one reaches a state of true awareness? Yes, when one reaches

223
00:31:40,160 --> 00:31:45,800
a state of true awareness having realized nibhana, when we'll have a sense that something

224
00:31:45,800 --> 00:31:51,840
has changed, when we'll have a sense that things are not as they were before, when we'll

225
00:31:51,840 --> 00:31:55,720
have a clear understanding that something is missing, something has gone, something has

226
00:31:55,720 --> 00:32:03,640
changed, and also have a clear sense of what is left to do, so when we'll have generally

227
00:32:03,640 --> 00:32:10,280
a good moral ethic and we'll be able to differentiate between what is the path and what

228
00:32:10,280 --> 00:32:20,400
is not the path, and when we'll have perfect confidence in the right path and in Buddhist

229
00:32:20,400 --> 00:32:28,800
meditation, because one will come to see that it actually does lead to the goal, is that

230
00:32:28,800 --> 00:32:32,960
a proper way to use or incorporate prayer beads in our meditation practice, I'm unsure

231
00:32:32,960 --> 00:32:37,040
of the purpose of the beads, think in advance, but they don't really have a purpose in

232
00:32:37,040 --> 00:32:43,120
our meditation, they were used sort of for counting, right? So certain meditations involve

233
00:32:43,120 --> 00:32:49,720
counting like you're chanting the Buddha's name 108 times, so you do that one bead

234
00:32:49,720 --> 00:32:55,040
for every time. There's different conventional ways they can be used, it's just in our

235
00:32:55,040 --> 00:33:03,760
tradition we don't use them. Are there other means, aside from walking meditation and

236
00:33:03,760 --> 00:33:09,840
cultivate effort, would a hand motion, didn't we talk about this, didn't I answer this question?

237
00:33:09,840 --> 00:33:15,840
So there'd be some effort that you'd get from that, but it's not the same as walking.

238
00:33:15,840 --> 00:33:27,440
What are your thoughts on the formless Janus? I don't really think about them too much. Do

239
00:33:27,440 --> 00:33:30,960
you see any value in practicing these kinds of meditations? You seem to see it trying

240
00:33:30,960 --> 00:33:36,400
this kind of long way around, see if the more important goal inside, yes, that's still

241
00:33:36,400 --> 00:33:49,360
my perspective, inquire about the stillness, silence of the mind and body while sitting.

242
00:33:49,360 --> 00:34:05,760
I don't see a question. Okay, we're sitting there as space of observation, at times

243
00:34:05,760 --> 00:34:19,920
those magical experiences occur after prolonged periods, see them arise and fall.

244
00:34:19,920 --> 00:34:23,920
Yeah well, I mean I've talked about it in the imperfections of insight, stillness, quiet

245
00:34:23,920 --> 00:34:31,960
is silence, stillness is a, it's called busity, still a very low level of insight. So if

246
00:34:31,960 --> 00:34:38,880
you feel calm or, you know, if you're quiet or still, you should say quiet, quiet or still,

247
00:34:38,880 --> 00:34:43,280
just stay noting it. I don't know what anything else that comes if you see something

248
00:34:43,280 --> 00:34:54,880
seeing, seeing a few years. If you feel bliss or rapture, you can say feeling, feeling.

249
00:34:54,880 --> 00:34:58,880
Like everything else, it's impermanent suffering and non-self, it's not going to satisfy

250
00:34:58,880 --> 00:35:04,400
you, it's not something you can cling to or something you should identify with as this

251
00:35:04,400 --> 00:35:10,960
is me, this I am. You should see it as impermanent and satisfying and uncontrollable,

252
00:35:10,960 --> 00:35:23,800
let it go. Okay, so there are questions.

253
00:35:23,800 --> 00:35:32,680
Thank you everyone for coming. Wish you all a good night.

