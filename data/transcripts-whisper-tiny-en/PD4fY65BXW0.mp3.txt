Okay, and another question today. This one from BD-951.
Thanks for the videos. Can you speak about working with physical pain?
How important does it not to move during meditation?
You recommend any type of stretching or yoga?
Thanks. Okay, working with physical pain.
Well, short and sweet, I think there are recognized to be two types of pain.
The kind of pain that is going to hurt but not injure you.
And the kind of pain that is not only going to hurt but is going to injure you or is associated with an injury.
Pain that is just going to hurt, it doesn't matter how long you sit with it.
It is just going to hurt and when you get up it is going to go away.
Another type of pain, if you sit for too long it is going to injure you or it is going to have severe consequences.
It may be that if you are really serious about meditation that there isn't this sort of distinction but I think for most people
you have to be realistic that there is only a certain level of pain that you can take.
If you can take it and if you know that it is just pain, like for most of us sitting cross-legged leads to some pain in the legs
or pain in the back or in the shoulders or so on, all of that kind of pain is going to go away.
It is not going to lead to your injury unless you have say a bad back and it is just so excruciatingly painful
that sit there is just going to whatever cause you to pass out.
I am not even sure about that. There are other pains where people have injuries where they have been in an accident
or had a sports injury or something and I think it is not advisable for them to try to work through the pain.
For most natural pains that occur in meditation in the shoulders, in the back, in the legs,
they are almost entirely stress-related and so there is something that we do have to work through in meditation.
Obviously the technique is very simple and it is just to see the pain from what it is.
The way we overcome pain is to overcome the disliking of it is to learn to see it in a different way
that it is just pain, to be able to accept that part of reality when it really is painful,
to be able to accept it and to not live our lives running away from the pain, to running away from things that we don't like.
When we run away from things, obviously we create a habit of a version where every time the pain comes up,
it becomes more and more of a problem and we become more and more adverse to it and less and less able to take even the smallest amount of pain.
We are trying to go in the other way in the other direction and be able to accept more and more of reality, even the more uncomfortable part.
In this sense, this sort of pain is very much associated with things like hunger or heat, cold,
which are all things that we can put up with but we don't want to.
Loud noises when people are saying bad things or speaking loudly,
we are trying to meditate and they are making loud noise in the background.
None of these things are going to hurt us.
You wouldn't sit there through the loud noise and somehow be harmed by it in any way,
but you get more and more frustrated.
A lot of our pain is very similar and we have to be honest with ourselves
and ask ourselves, is this pain really going to cause a permanent injury?
Is it really going to hurt me if I sit through it?
If it's not, then it's certainly in your best interest to overcome the aversion to the pain.
The only time that wouldn't be the case is if you are reasonably sure that sitting through the pain is just going to aggravate an injury or so on.
That can be generally the case of, as I said, old injuries may be in the case of back problems,
although I'm not sure about that.
I would think that someone with a bad book back could eventually work it out in meditation
as it's often stress related.
But then there are back injuries like chronic back injuries based on old age and so on
that probably can be worked out through meditation and therefore you should think about sitting in a chair or up against the wall,
which is totally fine.
That being said, even the next question, how important is it not to move during meditation?
It's quite important because as I said, this is a sort of aversion.
But that being said, you have to know your limits and if it's just driving you insane
and it's too powerful for you.
Like you consider, okay, in my practice I'm here, the pain is up here.
I'm not ready to sit through this pain. I know that I probably should, but realistically speaking, I'm not going to be able to.
And then you can move. If you do move, you shouldn't feel guilty about it.
You should accept the fact that you're only able to at this point accept a certain amount of pain.
So when you do move, what is important, then what you should be very careful to do, is to acknowledge the movements.
So as you stretch, say to yourself, stretching, stretching, when you slouch, slouching,
slouching, when you lift your leg lifting, lifting and move it out moving, moving.
If you grasp, grasp your leg grasping, lifting, placing and so on, being mindful of the movements.
And then you can say, you're not really cheating or you're not really outside of the meditation.
At least you're being mindful and your mindfulness is continuous.
You should acknowledge from the very beginning of disliking, disliking and wanting to move the leg wanting, wanting and lifting and placing.
But if you can, the best thing is to say to yourself, disliking, disliking, disliking, disliking until the disliking goes away.
When it's gone, go back to the pain and say to yourself, pain, reassuring yourself that it's just pain.
It's nothing intrinsically bad when it's gone, which it will eventually go away.
Because the great thing is that if you do stick with it, it'll eventually go away and not come back.
Because you're losing the stress, you're giving up, you're letting go, and your body becomes less stressful.
Your shoulders, your back, your legs, they become more relaxed.
And as a result, these stress pains don't come back.
Finally, do you recommend any type of stretching or yoga?
No, I don't because I think these are a means of escaping, a means of finding a state of being that is pleasurable, that is peaceful, that is free from this sort of thing.
And it's a compartmentalizing of reality, it's saying, okay, when this comes, I have to find a way to get away from it.
I have to not allow this state of reality to arise.
So stretching is an artificial, it's a construct, it's an artificial bodily state.
It's trying to create a state of body that is healthy, that is perfect, and so on.
And I think we would all have to agree that this body is not perfect, and there's no way to create that state.
Because it's an artificially constructed state, it's not sustainable, and it's not natural.
Natural is to let the body be as it is.
It's a difference of opinion, and I'm sure yoga teachers would say something completely different, and you know, power to them.
We all have different ideas.
But I've seen clearly that I don't need yoga to be flexible, to have a calm bodily state free from these stresses and free from the pains.
It's the same as a massage.
People who give massages, they say, yeah, it's necessary to have a massage every week or so on.
And I've never had a massage in my life, and I used to have lots of stress problems, and the shoulders, and the back, and the legs when I first started meditating, it was torture.
But after you practice honestly, you don't need any of this.
Your body is very flexible, it's very loose, your breathing is very deep, it's very natural.
And it comes by itself, there is no creating of this state.
It's bringing the body back to a natural state, a healthy state, which takes no training of the body whatsoever.
When the mind lets go of the body, the body naturally comes back to its state.
So I don't recommend any sort of artificial bodily, even exercise, I don't recommend it.
If you want to do it, living in the world, if it's necessary for your job or so on.
But if it's just to make you look more attractive to each their own, the most important in the meditation is to free the mind from clinging.
So especially clinging from clinging to the body, it's one of the great clinging.
So in the end we do have to let go, and we have to be prepared for whatever comes trying to be.
To make our mind so strong that nothing can ever face us, to make our mind so clearly aware and so wise and understanding about reality that whatever comes we're able to accept it.
So pain is a difficult one, but that's really one of the big reasons why we're practicing for physical and pain and mental pain.
To become free from the power of these things, the power that they have over us.
The only way they have power over us is because we give it to them, we say they're bad, we say this is bad, this is unacceptable.
And as a result we create friction, we create a state of aversion towards them.
Okay, so I hope that helped and good luck dealing with the pain, whatever pain you might have, and may you in the end, I'll be free from pain and suffering.
