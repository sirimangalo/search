1
00:00:00,000 --> 00:00:07,580
So, first question, what do you think about 2012?

2
00:00:07,580 --> 00:00:18,480
You know, in the Buddhist era, it's 2556 or 2555, so yeah, 2012 doesn't mean all that

3
00:00:18,480 --> 00:00:20,480
much to me.

4
00:00:20,480 --> 00:00:26,020
I guess the question is, question you're trying to ask is, do you think the world's

5
00:00:26,020 --> 00:00:30,720
going to end this year, or what do you think of these crazy people who think the world's

6
00:00:30,720 --> 00:00:34,280
going to end this year?

7
00:00:34,280 --> 00:00:40,280
I mean, the funny thing is that we have such a bad short term, a bad long term memory

8
00:00:40,280 --> 00:00:51,200
that we forget that every 12 years or so we have this kind of end of the world panic, and

9
00:00:51,200 --> 00:00:56,800
it seems to be some sort of genetic fault of humankind.

10
00:00:56,800 --> 00:01:00,680
And if you look in history, you'll see this again and again and again that, you know,

11
00:01:00,680 --> 00:01:07,480
all of Nostradamus's predictions, they predicted in this year or that year there would

12
00:01:07,480 --> 00:01:11,760
be this or there would be that based on Nostradamus's predictions or this prediction or that

13
00:01:11,760 --> 00:01:17,040
prediction, you know, of course in the year 1000, and there were huge predictions about

14
00:01:17,040 --> 00:01:19,240
the end of the world and so on.

15
00:01:19,240 --> 00:01:23,560
And if you look at the Bible itself, it's just, you know, it's just full of these.

16
00:01:23,560 --> 00:01:31,720
The apocalypse was supposed to come something like 20 years or within the lifetime of the

17
00:01:31,720 --> 00:01:36,760
people who had met Christ and of course they had to kind of fudge that and reinterpret

18
00:01:36,760 --> 00:01:42,000
it because obviously the end of the world didn't come.

19
00:01:42,000 --> 00:01:52,400
So this is some sort of, it's actually I think as a result it's fairly common in Judeo-Christian

20
00:01:52,400 --> 00:01:59,840
circles or Christian circles I guess, in particular Christian countries and Christian societies.

21
00:01:59,840 --> 00:02:06,040
Because of the way the Bible is so worded, I've heard of Christians who, I heard of Christians

22
00:02:06,040 --> 00:02:13,040
who near 2000, of course, this being Y2K bug that those of you who are old enough to

23
00:02:13,040 --> 00:02:21,120
remember, can remember how I've said everyone or how concerned everyone was, this might

24
00:02:21,120 --> 00:02:24,000
be the end of the world.

25
00:02:24,000 --> 00:02:31,640
And I'd heard of Christians who actually stopped eating or gave up their work and just

26
00:02:31,640 --> 00:02:36,680
gave everything up because they thought for sure after 2000 years this was the end, the end

27
00:02:36,680 --> 00:02:38,800
was coming.

28
00:02:38,800 --> 00:02:46,560
I spent the year 2000 with my uncle and we were drinking alcohol and smoking marijuana

29
00:02:46,560 --> 00:02:54,120
and then five days later, I remember that night, quite quite well, he's apart from the

30
00:02:54,120 --> 00:02:57,000
fact that I was stoned and drunk.

31
00:02:57,000 --> 00:03:04,800
And we were sitting on my uncle's lawn and we just watched the time ago, of course,

32
00:03:04,800 --> 00:03:11,720
the other funny thing is that the year 2000 or the near doesn't occur the same all over

33
00:03:11,720 --> 00:03:12,720
the world.

34
00:03:12,720 --> 00:03:17,000
But there we were sitting and watching the clock when we say, well, yeah, that's the

35
00:03:17,000 --> 00:03:25,320
year 2000 and the funny thing is that this was five days before my life changed.

36
00:03:25,320 --> 00:03:29,640
So the world did kind of end for me because five days later I went to practice meditation

37
00:03:29,640 --> 00:03:37,200
and just had a huge wake-up call to the realization that I was really on the wrong path

38
00:03:37,200 --> 00:03:46,000
and making a lot of errors and judgment and mistakes, just misunderstandings about what

39
00:03:46,000 --> 00:03:51,720
is progress and what is development and what is the path that leads to peace, happiness and

40
00:03:51,720 --> 00:03:53,920
freedom from suffering.

41
00:03:53,920 --> 00:04:03,000
So from that day on, I haven't indulged in such things, didn't take any more alcohol

42
00:04:03,000 --> 00:04:13,600
or drugs or didn't get involved in anything in the world two years later, I was among.

43
00:04:13,600 --> 00:04:21,400
But yeah, year 2000 can't be among the skeptics or the ridiculous maybe because it's

44
00:04:21,400 --> 00:04:28,760
a little bit silly, but the one thing I wanted to say from a Buddhist point of view is that

45
00:04:28,760 --> 00:04:39,800
you'd have to have some pretty awesome, karmic causes to bring about the consequences

46
00:04:39,800 --> 00:04:45,400
of the end of the world, to suddenly end the human race.

47
00:04:45,400 --> 00:04:53,200
I mean it's one thing to explain a tsunami based on karma, it kind of makes sense or it's

48
00:04:53,200 --> 00:04:57,880
plausible, not that it makes sense, it's a plausible.

49
00:04:57,880 --> 00:05:03,640
You could plausibly come up with an explanation based on karma, but to come up with an explanation

50
00:05:03,640 --> 00:05:09,280
for the end of humanity based on karma is a little bit difficult.

51
00:05:09,280 --> 00:05:14,080
So I would say the humanity is probably going to peter out and eventually disappear based

52
00:05:14,080 --> 00:05:22,800
on the changes in people's intentions and behaviors and based on the gradual shift in karma

53
00:05:22,800 --> 00:05:33,120
or people's actions, I think the idea that anything changes suddenly, the point that

54
00:05:33,120 --> 00:05:37,400
we're missing is that change doesn't come suddenly.

55
00:05:37,400 --> 00:05:45,080
And this is a problem, a misunderstanding that occurs throughout our lives, in all spheres

56
00:05:45,080 --> 00:05:47,640
of activity.

57
00:05:47,640 --> 00:05:55,600
We are constantly under the impression when something changes that it's going to change

58
00:05:55,600 --> 00:06:05,040
suddenly, that suddenly everything is going to change in a big way.

59
00:06:05,040 --> 00:06:11,280
What we realize is that actually it happens gradually, that actually the things end up being

60
00:06:11,280 --> 00:06:18,840
a lot more the same than they do being different and what we continue to see in the world

61
00:06:18,840 --> 00:06:25,560
is a mixture of good things and bad things because the human realm in a Buddhist sense

62
00:06:25,560 --> 00:06:36,280
is the nexus, the joining point because you can't become a angel, no, you can't become

63
00:06:36,280 --> 00:06:41,040
a brahma, a god from the lower realms.

64
00:06:41,040 --> 00:06:47,560
You can become an angel, but you can't leave the sensual sphere from the lower realms

65
00:06:47,560 --> 00:06:49,960
as an animaler in hell or so on.

66
00:06:49,960 --> 00:06:53,320
The best you can hope for is to become a human or an angel.

67
00:06:53,320 --> 00:07:01,760
And likewise you can't go from the root of the rupa, the rupa, the rupa, the formless

68
00:07:01,760 --> 00:07:03,480
realms, the god realms.

69
00:07:03,480 --> 00:07:10,000
You can't go from there to become an animal, say, or you can't go straight to hell.

70
00:07:10,000 --> 00:07:14,120
You have to pass through the human realm, so it's in summer in the middle and I think

71
00:07:14,120 --> 00:07:16,840
it's always going to be as long as it exists.

72
00:07:16,840 --> 00:07:21,720
Of course there are said to be times where the human realm doesn't exist or there are

73
00:07:21,720 --> 00:07:29,600
no humans in existence, but during the time of the existence of humankind it's going to

74
00:07:29,600 --> 00:07:38,440
always be a place of mixture, of happiness and peace and suffering and turmoil, goodness

75
00:07:38,440 --> 00:07:40,120
and evil.

76
00:07:40,120 --> 00:07:45,000
And so that's what you tend to see in your own mind as a human being and it's what you

77
00:07:45,000 --> 00:07:50,040
tend to see in the world around you that the world always has good in it and the world

78
00:07:50,040 --> 00:07:51,680
always has evil in it.

79
00:07:51,680 --> 00:07:55,440
They say they're in the suit does, they say there comes a time or there will come a

80
00:07:55,440 --> 00:08:03,320
time, sometime in the future, I'm talking about apocalyptic prophecies where the world

81
00:08:03,320 --> 00:08:14,000
will become a war zone and people will have swords as a habit, will have weapons as a

82
00:08:14,000 --> 00:08:19,960
habit or whatever weapons they use, it will be just something that everyone carries like

83
00:08:19,960 --> 00:08:27,320
today in some countries you see people walking down the streets with guns strapped to their

84
00:08:27,320 --> 00:08:34,280
legs and of course there was a terrible catastrophe in America that I heard about just

85
00:08:34,280 --> 00:08:46,560
recently, that kind of hints at this sort of a state, the possibility of such a state

86
00:08:46,560 --> 00:08:55,200
where people just go into movie theaters and open fire on all of the patron guests.

87
00:08:55,200 --> 00:08:59,200
But they say this becomes the norm and people just go around slaughtering each other

88
00:08:59,200 --> 00:09:10,960
and copulating with each other not having any thought of propriety of one's neighbor's

89
00:09:10,960 --> 00:09:16,760
wife and neighbor's husband and so on, it's a teacher's wife and so on and so the text

90
00:09:16,760 --> 00:09:24,760
say one doesn't think about propriety and just copulation rape perhaps becomes rampant

91
00:09:24,760 --> 00:09:35,600
as a matter of course and as a norm and so the world becomes a very horrible place

92
00:09:35,600 --> 00:09:41,760
but at the same time as this is all going on there is a group of people who go off into

93
00:09:41,760 --> 00:09:47,560
the forest and say we don't want to kill so the point being that there is still even

94
00:09:47,560 --> 00:09:51,960
at that horrible horrific time that's supposed to come sometime in the future has said

95
00:09:51,960 --> 00:09:59,520
to have been prophesied that even at that time there is said to be a substantial number

96
00:09:59,520 --> 00:10:05,160
of people who go in the opposite direction, leave home, leave society, go off into the

97
00:10:05,160 --> 00:10:10,840
forest and practice meditation or just live a peaceful life maybe doing chanting or prayer

98
00:10:10,840 --> 00:10:18,000
or whatever and waiting out this horrific period and eventually the people who are killing

99
00:10:18,000 --> 00:10:26,040
each other, killing each other off and it's not a very beneficial evolutionary trait

100
00:10:26,040 --> 00:10:36,560
to kill each other to kill your own species so they end up, what do you call, dead end

101
00:10:36,560 --> 00:10:43,240
I guess, I don't know what the technical biological evolutionary term is and those who

102
00:10:43,240 --> 00:10:49,720
go off in the forest and wind up being the more prolific and eventually things take

103
00:10:49,720 --> 00:10:57,240
a turn around and these people start to realize that this is actually a very important

104
00:10:57,240 --> 00:11:04,720
thing, they come to realize again the importance of not killing how logically it kind

105
00:11:04,720 --> 00:11:08,760
of makes sense if you stop killing each other your lifespan is going to increase and you're

106
00:11:08,760 --> 00:11:20,960
going to prosper and so they start making rules again, they start developing morality

107
00:11:20,960 --> 00:11:27,240
they say let's keep this as a rule not to kill people and then eventually they make

108
00:11:27,240 --> 00:11:32,080
more rules and get back all of their morality and stop doing evil things and as a result

109
00:11:32,080 --> 00:11:36,400
they prosper further and further and then eventually another Buddha arises this is what

110
00:11:36,400 --> 00:11:44,080
the Buddhist prophecy of the future is and you can read about it in the Chakkawat-desi

111
00:11:44,080 --> 00:11:54,800
Hanada sutai believe yeah in the digany kai no I hope it's an a digany kai not the

112
00:11:54,800 --> 00:12:01,440
great scholar I should be but a great sutai and a very interesting prophecy of the future

113
00:12:01,440 --> 00:12:09,960
that rivals this whole 2012 thing or puts the 2012 thing to shame because of its intricacy

114
00:12:09,960 --> 00:12:15,920
it may not happen exactly as it's been prophesied but you've got to admire the intricacy

115
00:12:15,920 --> 00:12:21,440
of it it's not like just hey in 2012 suddenly the volcanoes are going to erupt and the

116
00:12:21,440 --> 00:12:25,440
media is going to hit the world and another planet is going to hit the earth not just

117
00:12:25,440 --> 00:12:33,640
the silly things and my own prophecies and end of calendars and so on it actually is a

118
00:12:33,640 --> 00:12:40,520
lot more delicate than that and you're going to actually see the decline of humankind

119
00:12:40,520 --> 00:12:51,520
first and then an eventual return to glory so that's my take on that sort of question

120
00:12:51,520 --> 00:12:58,520
and then that sort of topic

