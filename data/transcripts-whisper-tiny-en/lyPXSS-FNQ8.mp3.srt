1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:05,000 --> 00:00:13,000
Today we continue on with first number 111, which reads as follows.

3
00:00:13,000 --> 00:00:42,000
This is the Dhamapada, which means if one should live a hundred years without wisdom,

4
00:00:42,000 --> 00:00:53,000
without wisdom, a little understanding and unfocused, unconcentrated.

5
00:00:53,000 --> 00:01:13,000
As you can see, it is a life of one day for one who is wise and meditates.

6
00:01:13,000 --> 00:01:16,000
Very similar to our last verse.

7
00:01:16,000 --> 00:01:19,000
As I said, we're going to have a series of these.

8
00:01:19,000 --> 00:01:22,000
So we'll probably go through them quite quickly.

9
00:01:22,000 --> 00:01:27,000
The story for this one also is quite short.

10
00:01:27,000 --> 00:01:34,000
So here we have a story about Kundana, but it's a monk named Kundana,

11
00:01:34,000 --> 00:01:40,000
not Mahakundana, Mahakundana, the Kundana that many Buddhists are familiar with.

12
00:01:40,000 --> 00:01:47,000
This one is Khanuk, Kundana, Khanuk, and Khanuk means Stamp.

13
00:01:47,000 --> 00:01:53,000
So he was Kundana of the Stamp.

14
00:01:53,000 --> 00:02:05,000
He was an arrow hunt, and he was dwelling in the forest, and he lived alone for much of the time.

15
00:02:05,000 --> 00:02:15,000
He became an arrow hunt while he was in the forest, and he was on his way back to talk to the Buddha,

16
00:02:15,000 --> 00:02:19,000
to let the Buddha know that he had become an arrow hunt.

17
00:02:19,000 --> 00:02:26,000
And on his way back, he became tired, and so he stopped and sat down on a rock,

18
00:02:26,000 --> 00:02:30,000
and went entered into a meditative state.

19
00:02:30,000 --> 00:02:36,000
Maybe he entered into Nibana, maybe just a jhana, probably entered into a cessation,

20
00:02:36,000 --> 00:02:40,000
instead of attainment of Nibana.

21
00:02:40,000 --> 00:02:45,000
We call Palasamapati, when someone who has become a Sautapana, Sakadagami,

22
00:02:45,000 --> 00:02:51,000
Anagami or Narahant, they can enter into a state of reviewing Nibana,

23
00:02:51,000 --> 00:02:54,000
going over the path that they have attained.

24
00:02:54,000 --> 00:02:58,000
Not the path going over the fruition that they have attained.

25
00:02:58,000 --> 00:03:08,000
And so they enter into cessation for ten minutes, one hour, ten hours, twenty-four hours.

26
00:03:08,000 --> 00:03:21,000
And during that time, it's complete cessation, so they are completely oblivious to the world around them.

27
00:03:21,000 --> 00:03:23,000
Completely oblivious.

28
00:03:23,000 --> 00:03:30,000
There is no seeing, hearing, smelling, tasting, feeling, or thinking.

29
00:03:30,000 --> 00:03:33,000
No even sense of time passing.

30
00:03:33,000 --> 00:03:38,000
It seems as though that time is just missing, except there's a huge sense of peace

31
00:03:38,000 --> 00:03:44,000
that it lasts even after the person comes out of the state.

32
00:03:44,000 --> 00:03:52,000
And so during that time, he would have gone in for maybe a day, twenty-four hours or something.

33
00:03:52,000 --> 00:04:00,000
And during that time, a group of bandits, a large group of bandits, similar to in our last story.

34
00:04:00,000 --> 00:04:03,000
But these guys had plundered a whole village.

35
00:04:03,000 --> 00:04:10,000
They had destroyed and robbed an entire village, and gone in with their guns.

36
00:04:10,000 --> 00:04:18,000
They didn't have guns with their bows and arrows and their swords and killed and raped and pillaged.

37
00:04:18,000 --> 00:04:24,000
Or maybe just robbed, because they don't seem to have been all that evil.

38
00:04:24,000 --> 00:04:29,000
So they came up and it was getting dark, and so they wanted to rest as well.

39
00:04:29,000 --> 00:04:35,000
And seeing this flat rock that the elder had sat on, they saw this rock, and they saw the elder.

40
00:04:35,000 --> 00:04:40,000
But it was getting dark, and so they thought he was a tree stump.

41
00:04:40,000 --> 00:04:52,000
And so the head bandit took his sack with all his valuables that he had robbed.

42
00:04:52,000 --> 00:04:56,000
And actually placed it on the elder's head.

43
00:04:56,000 --> 00:05:02,000
He just plopped it down on top of him, maybe on his shoulders or something, just over top of him.

44
00:05:02,000 --> 00:05:07,000
And the other robbers piled up their sacks around the original one,

45
00:05:07,000 --> 00:05:15,000
and they stumped, sort of covering the monk completely.

46
00:05:15,000 --> 00:05:17,000
And burying him in their sacks.

47
00:05:17,000 --> 00:05:25,000
So there were 500 small bandits, or a large number, probably.

48
00:05:25,000 --> 00:05:35,000
And usually you think 500 might be a bit of an exaggeration for one band of bandits, but a lot is the idea.

49
00:05:35,000 --> 00:05:42,000
And then they slept, and spent the night.

50
00:05:42,000 --> 00:05:46,000
And the elder spent the night, completely oblivious as to what was going on.

51
00:05:46,000 --> 00:05:51,000
And the morning they wake up, let's light out, and they start to take the sacks off one by one,

52
00:05:51,000 --> 00:05:55,000
and they achieve picks up his sack.

53
00:05:55,000 --> 00:06:02,000
Or as they get close, you see, it's not a stump, and he pulls his sack off, and it's a monk, sitting in meditation.

54
00:06:02,000 --> 00:06:10,000
Completely unmoved, you know, stiff and unaffected.

55
00:06:10,000 --> 00:06:13,000
And they were spooked, and they thought it was some kind of evil spirit.

56
00:06:13,000 --> 00:06:15,000
And so they started to run away.

57
00:06:15,000 --> 00:06:18,000
And at that moment the elder came out of the attainment.

58
00:06:18,000 --> 00:06:20,000
And so he turned and looked at them, and he said,

59
00:06:20,000 --> 00:06:21,000
oh, don't be afraid.

60
00:06:21,000 --> 00:06:22,000
I'm a monk.

61
00:06:22,000 --> 00:06:30,000
I'm not here to curse you.

62
00:06:30,000 --> 00:06:40,000
And they were shocked and moved, you know, reasonably so.

63
00:06:40,000 --> 00:06:45,000
Seeing some obviously an incredibly powerful meditator,

64
00:06:45,000 --> 00:06:51,000
who was spent the whole night covered in sacks full of heavy valuables.

65
00:06:51,000 --> 00:06:55,000
Again, it speaks to this, the power of the attainment.

66
00:06:55,000 --> 00:07:00,000
There's this idea of the power of true meditative attainment.

67
00:07:00,000 --> 00:07:02,000
It makes one invincible.

68
00:07:02,000 --> 00:07:05,000
You know, I mentioned this Tibetan monk,

69
00:07:05,000 --> 00:07:07,000
so it's supposed to go into transits.

70
00:07:07,000 --> 00:07:10,000
And then they go and sit in snow banks.

71
00:07:10,000 --> 00:07:14,000
And they go into transits, and maybe the fire element,

72
00:07:14,000 --> 00:07:17,000
and they actually melt all the snow around them.

73
00:07:17,000 --> 00:07:19,000
And they have contests to see who can melt the most snow.

74
00:07:19,000 --> 00:07:21,000
I don't know, this is what I've heard.

75
00:07:21,000 --> 00:07:25,000
But there's lots of fun things that supposedly happen.

76
00:07:25,000 --> 00:07:30,000
And if you want to test, you can meditate for yourself

77
00:07:30,000 --> 00:07:32,000
and see what sort of things occur.

78
00:07:32,000 --> 00:07:36,000
A lot of people experience us, what we call astral travel.

79
00:07:36,000 --> 00:07:41,000
Some people are able to see things far away,

80
00:07:41,000 --> 00:07:43,000
hear things far away.

81
00:07:43,000 --> 00:07:45,000
Some people are able to remember past lives.

82
00:07:45,000 --> 00:07:47,000
Some people can see into the future

83
00:07:47,000 --> 00:07:50,000
very strange things can happen when you meditate.

84
00:07:50,000 --> 00:07:51,000
Whereas it's not the goal.

85
00:07:51,000 --> 00:07:53,000
It's not why we meditate.

86
00:07:53,000 --> 00:07:58,000
But anyway, here we have this idea of being somehow invincible

87
00:07:58,000 --> 00:08:02,000
when you enter into these states, physically invincible.

88
00:08:02,000 --> 00:08:05,000
Covered in heavy, heavy things.

89
00:08:05,000 --> 00:08:07,000
Not being disturbed.

90
00:08:07,000 --> 00:08:09,000
And so they were sort of impressed.

91
00:08:09,000 --> 00:08:15,000
And made up their minds to become monks.

92
00:08:15,000 --> 00:08:20,000
They had had abandoned it.

93
00:08:20,000 --> 00:08:22,000
I'm going to become a monk under there.

94
00:08:22,000 --> 00:08:24,000
I'm going to become a follower of this guy

95
00:08:24,000 --> 00:08:27,000
because he's got the power.

96
00:08:27,000 --> 00:08:30,000
Here we thought we were powerful.

97
00:08:30,000 --> 00:08:34,000
We have this power over people making them afraid

98
00:08:34,000 --> 00:08:38,000
and being able to take their belongings from them.

99
00:08:38,000 --> 00:08:42,000
But that's not real power, not compared to this guy.

100
00:08:42,000 --> 00:08:45,000
Clearly, he has far more power than us.

101
00:08:45,000 --> 00:08:51,000
And so they became monks.

102
00:08:51,000 --> 00:08:53,000
And so he made monks of the monks,

103
00:08:53,000 --> 00:08:55,000
just as Sankitja had done.

104
00:08:55,000 --> 00:08:59,000
And he went with them to the teacher.

105
00:08:59,000 --> 00:09:02,000
And when they got to the Buddha,

106
00:09:02,000 --> 00:09:04,000
the Buddha noticed and said,

107
00:09:04,000 --> 00:09:07,000
oh, Kundana, you've got some students with you, have you.

108
00:09:07,000 --> 00:09:11,000
And Kundana told them what had happened

109
00:09:11,000 --> 00:09:14,000
in the Buddha looks at the monks.

110
00:09:14,000 --> 00:09:18,000
And he said, is this truly what happened?

111
00:09:18,000 --> 00:09:21,000
And the guy's like, well, yeah.

112
00:09:21,000 --> 00:09:22,000
It's a true.

113
00:09:22,000 --> 00:09:25,000
We're a bunch of a bunch of bandits.

114
00:09:25,000 --> 00:09:29,000
Well, yeah, you know.

115
00:09:29,000 --> 00:09:32,000
And the Buddha shook his head and said,

116
00:09:32,000 --> 00:09:36,000
better for you to live one moment,

117
00:09:36,000 --> 00:09:38,000
better for you to live,

118
00:09:38,000 --> 00:09:42,000
single day meditating, being engaged in things

119
00:09:42,000 --> 00:09:44,000
that are truly to your benefit.

120
00:09:44,000 --> 00:09:47,000
And that truly bring happiness to you,

121
00:09:47,000 --> 00:09:49,000
into your mind.

122
00:09:49,000 --> 00:09:52,000
It truly brings good things.

123
00:09:52,000 --> 00:09:54,000
Better to live one day like that,

124
00:09:54,000 --> 00:09:56,000
than to live a hundred years evil

125
00:09:56,000 --> 00:10:03,000
and committing such foolishness as you've been engaged in.

126
00:10:03,000 --> 00:10:09,000
And so he made a connection.

127
00:10:09,000 --> 00:10:11,000
Me made the connection to the Dhamma.

128
00:10:11,000 --> 00:10:14,000
They say, that's what these stories at the end

129
00:10:14,000 --> 00:10:17,000
had always said he connected it with the Dhamma.

130
00:10:17,000 --> 00:10:19,000
Whatever the situation was,

131
00:10:19,000 --> 00:10:23,000
he points out the Dhamma in regards to the situation.

132
00:10:23,000 --> 00:10:26,000
And as a result, having made the connection,

133
00:10:26,000 --> 00:10:28,000
he spoke this verse,

134
00:10:28,000 --> 00:10:33,000
and so on.

135
00:10:33,000 --> 00:10:35,000
So a short story this time.

136
00:10:35,000 --> 00:10:37,000
And a verse very similar to our last one.

137
00:10:37,000 --> 00:10:39,000
There's not that much to say.

138
00:10:39,000 --> 00:10:44,000
The difference here is where as the last one was...

139
00:10:44,000 --> 00:10:59,000
The last one was in regards to morality.

140
00:10:59,000 --> 00:11:02,000
Do see love.

141
00:11:02,000 --> 00:11:04,000
This one is in regards to wisdom.

142
00:11:04,000 --> 00:11:07,000
In fact, it's interesting because the three things go together.

143
00:11:07,000 --> 00:11:12,000
These two verses point out both sides of it.

144
00:11:12,000 --> 00:11:16,000
So concentration is the defining factor here.

145
00:11:16,000 --> 00:11:20,000
But you have wisdom on the one hand and morality on the other.

146
00:11:20,000 --> 00:11:23,000
And these three make up the three trainings of the Buddha.

147
00:11:23,000 --> 00:11:27,000
Together they describe the Buddha's teaching.

148
00:11:27,000 --> 00:11:29,000
They describe the full noble path.

149
00:11:29,000 --> 00:11:32,000
So they are the path of practice.

150
00:11:32,000 --> 00:11:33,000
Yes.

151
00:11:33,000 --> 00:11:35,000
One way of explaining it is you start with morality

152
00:11:35,000 --> 00:11:38,000
and you become focused as a result of being ethical.

153
00:11:38,000 --> 00:11:43,000
Your mind becomes focused because you cease in engaging in things

154
00:11:43,000 --> 00:11:46,000
that distract your mind or disturb the mind.

155
00:11:46,000 --> 00:11:51,000
And once your mind is focused, in focus, then you can see clearly.

156
00:11:51,000 --> 00:11:53,000
Then that's wisdom.

157
00:11:53,000 --> 00:11:55,000
So these three go together.

158
00:11:55,000 --> 00:11:57,000
So it's rhetoric.

159
00:11:57,000 --> 00:11:59,000
You don't have to pick either or.

160
00:11:59,000 --> 00:12:01,000
The Buddha isn't saying,

161
00:12:01,000 --> 00:12:03,000
either you can do concentration and wisdom

162
00:12:03,000 --> 00:12:06,000
or you can do concentration and morality.

163
00:12:06,000 --> 00:12:08,000
He's just talking about good things.

164
00:12:08,000 --> 00:12:13,000
And he's addressing a group of people

165
00:12:13,000 --> 00:12:21,000
who could best appreciate certain aspects of the teaching.

166
00:12:21,000 --> 00:12:24,000
So for some people ethics is more important,

167
00:12:24,000 --> 00:12:26,000
more interesting and more applicable.

168
00:12:26,000 --> 00:12:29,000
For some people wisdom will be more applicable.

169
00:12:29,000 --> 00:12:34,000
So these verses are tuned to their audience, usually.

170
00:12:34,000 --> 00:12:41,000
And probably in this case, after he dates an hours on a,

171
00:12:41,000 --> 00:12:44,000
all at the end of his teaching,

172
00:12:44,000 --> 00:12:49,000
all 500 of the bikhus became our aunts.

173
00:12:49,000 --> 00:12:53,000
Isn't that awesome?

174
00:12:53,000 --> 00:12:56,000
It'd be nice if that sort of thing happened these days

175
00:12:56,000 --> 00:12:59,000
after teaching about the dhamma,

176
00:12:59,000 --> 00:13:01,000
we all became our aunts.

177
00:13:01,000 --> 00:13:02,000
But you can't blame the dhamma.

178
00:13:02,000 --> 00:13:04,000
You can't blame the dhamma.

179
00:13:04,000 --> 00:13:06,000
You have to blame yourself that our minds are so clouded

180
00:13:06,000 --> 00:13:10,000
that it takes much more than just the simple teaching.

181
00:13:10,000 --> 00:13:15,000
But there's something to this idea

182
00:13:15,000 --> 00:13:18,000
that simple teachings can lead to enlightenment

183
00:13:18,000 --> 00:13:22,000
because the truth is not hard to understand.

184
00:13:22,000 --> 00:13:25,000
It's actually quite easy.

185
00:13:25,000 --> 00:13:27,000
And once you've understood it,

186
00:13:27,000 --> 00:13:31,000
it's almost as though you don't know how you can miss it.

187
00:13:31,000 --> 00:13:37,000
For the most part, we are clear about reality.

188
00:13:37,000 --> 00:13:40,000
This is we add on so much more.

189
00:13:40,000 --> 00:13:43,000
Or we take or we ignore certain things.

190
00:13:43,000 --> 00:13:47,000
Like for example, talking about freewill and determinism,

191
00:13:47,000 --> 00:13:50,000
if it's about freewill, then we add a soul into the equation.

192
00:13:50,000 --> 00:13:54,000
If we believe in determinism, then we ignore the mind,

193
00:13:54,000 --> 00:13:57,000
ignore the choice.

194
00:13:57,000 --> 00:14:07,000
We ignore the first person nature of reality

195
00:14:07,000 --> 00:14:10,000
when we think of it as just physical.

196
00:14:10,000 --> 00:14:15,000
If when we are addicted to something,

197
00:14:15,000 --> 00:14:18,000
we tend to know that the addiction is causing a suffering,

198
00:14:18,000 --> 00:14:22,000
and yet we don't stop.

199
00:14:22,000 --> 00:14:28,000
We're like a matter like insane people.

200
00:14:28,000 --> 00:14:33,000
Remember angry when we're angry at someone we...

201
00:14:33,000 --> 00:14:38,000
You make me so angry as though when someone makes us angry,

202
00:14:38,000 --> 00:14:42,000
you make me so angry as though we're blaming them for our own suffering.

203
00:14:42,000 --> 00:14:46,000
Because we're suffering when we're angry we suffer.

204
00:14:46,000 --> 00:14:51,000
So the teaching is actually quite simple.

205
00:14:51,000 --> 00:14:56,000
If you receive these simple teachings and you become in tune with them.

206
00:14:56,000 --> 00:15:01,000
If you're hearing it from the Buddha, it's strong enough to jar you out of your state

207
00:15:01,000 --> 00:15:06,000
and really bring you in line with this truth.

208
00:15:06,000 --> 00:15:11,000
It's so forceful and so confidently delivered by someone who is so pure

209
00:15:11,000 --> 00:15:15,000
and so perfect that it really changes you.

210
00:15:15,000 --> 00:15:19,000
There's some kind of a magic in teaching in that way

211
00:15:19,000 --> 00:15:27,000
when you're taught and when it's delivered with confidence.

212
00:15:27,000 --> 00:15:32,000
You know the confidence of the Buddha, the power of the Buddha.

213
00:15:32,000 --> 00:15:37,000
So there's that where to blame for not being born in the time of the Buddha.

214
00:15:37,000 --> 00:15:44,000
I think actually listen to him instead you have to listen to me.

215
00:15:44,000 --> 00:15:47,000
But the dhamma remains the same and the path is still there.

216
00:15:47,000 --> 00:15:52,000
It just might be more difficult of a climb, more difficult of a path.

217
00:15:52,000 --> 00:15:58,000
But if we don't walk it then we can be sure to fail.

218
00:15:58,000 --> 00:16:02,000
If we practice we can't be sure if and when we'll succeed.

219
00:16:02,000 --> 00:16:08,000
But if we don't practice we can be sure that we will fail.

220
00:16:08,000 --> 00:16:10,000
So there's that.

221
00:16:10,000 --> 00:16:12,000
Anyway.

222
00:16:12,000 --> 00:16:18,000
That's the dhamma path of teaching for tonight that we should be wise and meditate.

223
00:16:18,000 --> 00:16:24,000
We practice these two things together not just meditating, not just focusing our minds

224
00:16:24,000 --> 00:16:27,000
but also cultivating wisdom.

225
00:16:27,000 --> 00:16:32,000
Again not just being concerned with wisdom without meditating and thinking that we know everything,

226
00:16:32,000 --> 00:16:35,000
studying and thinking and so on.

227
00:16:35,000 --> 00:16:38,000
But actually coming to see for ourselves.

228
00:16:38,000 --> 00:16:42,000
And when we meditate this is what we see. This is what happens.

229
00:16:42,000 --> 00:16:45,000
We see impermanence, we see suffering, we see non-self.

230
00:16:45,000 --> 00:16:53,000
We see through our delusions of stability, satisfaction and control.

231
00:16:53,000 --> 00:16:55,000
Or self.

232
00:16:55,000 --> 00:16:58,000
And we come to see that things are not the way we think.

233
00:16:58,000 --> 00:17:00,000
They're not worth clinging to.

234
00:17:00,000 --> 00:17:01,000
They have nothing.

235
00:17:01,000 --> 00:17:03,000
No characteristic of them.

236
00:17:03,000 --> 00:17:05,000
That makes them worth clinging to.

237
00:17:05,000 --> 00:17:10,000
We start to see things as being without value, without merit.

238
00:17:10,000 --> 00:17:12,000
Not not a stable.

239
00:17:12,000 --> 00:17:15,000
We see that our own mind and everything in the world around this.

240
00:17:15,000 --> 00:17:27,000
It's not a stable and dependable refuge or platform on which to base our goals and aspirations

241
00:17:27,000 --> 00:17:29,000
and our desires.

242
00:17:29,000 --> 00:17:30,000
And so we give them up.

243
00:17:30,000 --> 00:17:33,000
We give up our desire for such things.

244
00:17:33,000 --> 00:17:35,000
And that's the best way to live.

245
00:17:35,000 --> 00:17:37,000
If you live your life one day, like that.

246
00:17:37,000 --> 00:17:39,000
It's far more valuable.

247
00:17:39,000 --> 00:17:43,000
It's on a whole other league.

248
00:17:43,000 --> 00:17:45,000
From living and living well.

249
00:17:45,000 --> 00:17:48,000
People talk about living.

250
00:17:48,000 --> 00:17:50,000
Living the good life.

251
00:17:50,000 --> 00:17:52,000
Carpedium sees the day.

252
00:17:52,000 --> 00:17:55,000
Eat, drink and be merry.

253
00:17:55,000 --> 00:18:01,000
Like angels in heaven who enjoy their pleasant things up in heaven.

254
00:18:01,000 --> 00:18:04,000
And they think, well, that's what it's all about.

255
00:18:04,000 --> 00:18:06,000
People will say, this is what it's all about.

256
00:18:06,000 --> 00:18:07,000
I know I used to be there.

257
00:18:07,000 --> 00:18:11,000
We'd party and dance and sing.

258
00:18:11,000 --> 00:18:13,000
I remember going to dances when I was younger

259
00:18:13,000 --> 00:18:15,000
and everyone was so excited and so much.

260
00:18:15,000 --> 00:18:18,000
Because there's so much chemicals in the brain.

261
00:18:18,000 --> 00:18:22,000
There's adrenaline and so on.

262
00:18:22,000 --> 00:18:23,000
And don't put me in.

263
00:18:23,000 --> 00:18:27,000
I don't know what kinds of chemicals involved in dancing

264
00:18:27,000 --> 00:18:29,000
and enjoying yourself and partying

265
00:18:29,000 --> 00:18:33,000
and drinking and laughing and joking and playing music

266
00:18:33,000 --> 00:18:39,000
and playing musical instruments and so on.

267
00:18:39,000 --> 00:18:41,000
And then it doesn't make you happier.

268
00:18:41,000 --> 00:18:45,000
You don't actually feel happier afterwards.

269
00:18:45,000 --> 00:18:48,000
So that kind of life is actually pointless.

270
00:18:48,000 --> 00:18:51,000
And the end you finish and you die.

271
00:18:51,000 --> 00:18:53,000
And that was it.

272
00:18:53,000 --> 00:18:55,000
You've gained nothing.

273
00:18:55,000 --> 00:18:58,000
In fact, most likely you've cultivated all sorts of addiction

274
00:18:58,000 --> 00:19:00,000
and attachment and aversion.

275
00:19:00,000 --> 00:19:03,000
That's going to cause you problems in this life

276
00:19:03,000 --> 00:19:05,000
and in the next.

277
00:19:05,000 --> 00:19:08,000
So there's that.

278
00:19:08,000 --> 00:19:10,000
Better to be mindful.

279
00:19:10,000 --> 00:19:12,000
Better to be wise.

280
00:19:12,000 --> 00:19:14,000
Better to be focused.

281
00:19:14,000 --> 00:19:18,000
Better to see things as they are.

282
00:19:18,000 --> 00:19:20,000
So that's the demo conduit for today.

283
00:19:20,000 --> 00:19:21,000
Thank you all for tuning in.

284
00:19:21,000 --> 00:19:36,000
Have a good day.

