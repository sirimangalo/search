1
00:00:00,000 --> 00:00:07,000
And the cause for suffering, when it says explain that the cause for suffering is everything

2
00:00:07,000 --> 00:00:16,000
or one thing. One thing to do is there a way we know what it is that we have to be this way and again.

3
00:00:16,000 --> 00:00:22,000
He said when this sees us, when there is no more one thing than no more attachment.

4
00:00:22,000 --> 00:00:27,000
No more attachment to anything. Then there's the cause.

5
00:00:27,000 --> 00:00:35,000
And I always say this is like a bird. I can imagine a bird leading to the side of it.

6
00:00:35,000 --> 00:00:40,000
And we're like that bird leading to the side of the boat like that.

7
00:00:40,000 --> 00:00:44,000
All in all, this is what important thing you can get.

8
00:00:44,000 --> 00:00:47,000
Because we're so afraid of it all.

9
00:00:47,000 --> 00:00:50,000
Even things are like this world, maybe.

10
00:00:50,000 --> 00:00:54,000
Or maybe because we're afraid of it like what it's like.

11
00:00:54,000 --> 00:00:57,000
Or maybe this can be the target path.

12
00:00:57,000 --> 00:00:58,000
And then that's all that's happening.

13
00:00:58,000 --> 00:01:00,000
Whatever is in mind.

14
00:01:00,000 --> 00:01:01,000
What does it mean?

15
00:01:01,000 --> 00:01:03,000
Whatever it means.

16
00:01:03,000 --> 00:01:05,000
How can we?

17
00:01:05,000 --> 00:01:08,000
All of my days we just hear a whole lane.

18
00:01:08,000 --> 00:01:10,000
How can we?

19
00:01:12,000 --> 00:01:14,000
But we don't realize that we're like a bird.

20
00:01:14,000 --> 00:01:16,000
We're not like that.

21
00:01:16,000 --> 00:01:19,000
We're not like a handful of people inside of it.

22
00:01:19,000 --> 00:01:20,000
We're actually a bird.

23
00:01:20,000 --> 00:01:23,000
Excuse me, that's all we can find.

24
00:01:23,000 --> 00:01:26,000
We can find above all of it.

25
00:01:28,000 --> 00:01:31,000
So the meditation is in case you doesn't fly.

26
00:01:31,000 --> 00:01:33,000
You can fly above all of it.

27
00:01:33,000 --> 00:01:35,000
You can use all of it.

28
00:01:35,000 --> 00:01:37,000
You can catch it into any of it.

29
00:01:37,000 --> 00:01:39,000
All of our lights in this bag.

30
00:01:39,000 --> 00:01:41,000
Let go of it.

31
00:01:41,000 --> 00:01:43,000
You can fly above them.

32
00:01:43,000 --> 00:01:46,000
We're like a bird flying in green.

33
00:01:46,000 --> 00:01:48,000
We need no tricks.

34
00:01:48,000 --> 00:01:51,000
This is the bird.

35
00:01:51,000 --> 00:01:52,000
This is the bird.

36
00:01:52,000 --> 00:01:53,000
There's no tricks.

37
00:01:53,000 --> 00:01:54,000
There's no tricks.

38
00:01:54,000 --> 00:01:56,000
We don't care for what's problems.

39
00:01:56,000 --> 00:02:01,000
This is the basis of the first discourse.

40
00:02:01,000 --> 00:02:04,000
And this is what's most important about tonight.

41
00:02:04,000 --> 00:02:07,000
This was the ninth where the third path is first.

42
00:02:07,000 --> 00:02:09,000
And this is the most profound feature.

43
00:02:09,000 --> 00:02:12,000
So the thing that we always put go back.

44
00:02:12,000 --> 00:02:15,000
We always try to get to it.

45
00:02:15,000 --> 00:02:18,000
So that's the next position.

46
00:02:18,000 --> 00:02:21,000
The ninth is the first machine.

47
00:02:21,000 --> 00:02:30,000
And I guess the third one more thing that's important.

48
00:02:30,000 --> 00:02:36,000
This was the path that we rise to the first person for hundreds of years.

49
00:02:38,000 --> 00:02:40,000
And that is one end.

50
00:02:40,000 --> 00:02:42,000
The final is one end of the five months.

51
00:02:42,000 --> 00:02:45,000
You actually see that you understand.

52
00:02:45,000 --> 00:02:47,000
You're right there.

53
00:02:47,000 --> 00:02:49,000
You're going to multiply the bird.

54
00:02:49,000 --> 00:02:52,000
You're still going to divide the bird.

55
00:02:52,000 --> 00:02:55,000
You're still going to divide the bird.

56
00:02:55,000 --> 00:02:58,000
You realize that when there is no point in the end,

57
00:02:58,000 --> 00:03:00,000
you can see that there is no point in the end.

58
00:03:00,000 --> 00:03:02,000
That's the final.

59
00:03:02,000 --> 00:03:04,000
You can see that the bird is the first.

60
00:03:04,000 --> 00:03:06,000
You can see some of the end of the bird.

61
00:03:06,000 --> 00:03:08,000
So that's the final.

62
00:03:08,000 --> 00:03:13,000
That's the final.

63
00:03:13,000 --> 00:03:16,000
This means that everything is right.

64
00:03:16,000 --> 00:03:18,000
That is far.

65
00:03:18,000 --> 00:03:19,000
Everything is going to rise.

66
00:03:19,000 --> 00:03:21,000
It has to see.

67
00:03:21,000 --> 00:03:23,000
There's nothing in existence for it.

68
00:03:23,000 --> 00:03:25,000
It has risen.

69
00:03:25,000 --> 00:03:27,000
Well, not this way.

70
00:03:27,000 --> 00:03:30,000
It will be this crazy thing that people can see.

71
00:03:30,000 --> 00:03:35,000
It was able to let them realize that even there are fragments.

72
00:03:35,000 --> 00:03:40,000
You can see that about this guy.

73
00:03:40,000 --> 00:03:45,000
It has.

74
00:03:45,000 --> 00:03:49,000
No point in the end.

75
00:03:49,000 --> 00:03:50,000
When they move down the field,

76
00:03:50,000 --> 00:03:52,000
it stays down to the environment.

77
00:03:52,000 --> 00:03:54,000
So that's the final.

78
00:03:54,000 --> 00:03:56,000
I'm not really going to call this any before.

79
00:03:56,000 --> 00:03:57,000
I'm going to call it any.

80
00:03:57,000 --> 00:04:01,000
Is that because nobody can stop, or just stop.

81
00:04:01,000 --> 00:04:02,000
I'm going to go in.

82
00:04:02,000 --> 00:04:04,000
I'm going to give it to this day.

83
00:04:04,000 --> 00:04:05,000
It's no longer.

84
00:04:05,000 --> 00:04:08,000
That was the day when the person will be able to,

85
00:04:08,000 --> 00:04:10,000
still go in the day.

86
00:04:10,000 --> 00:04:13,000
Still giving, isn't happening.

87
00:04:13,000 --> 00:04:14,000
It's a big point.

88
00:04:14,000 --> 00:04:15,000
It's a big point.

89
00:04:15,000 --> 00:04:16,000
It's a big point.

90
00:04:16,000 --> 00:04:17,000
It's a big point.

91
00:04:17,000 --> 00:04:19,000
So this is when it's over.

92
00:04:19,000 --> 00:04:20,000
It's a big point.

93
00:04:20,000 --> 00:04:21,000
It's a big point.

94
00:04:21,000 --> 00:04:22,000
It's a big point.

95
00:04:22,000 --> 00:04:23,000
It's a big point.

96
00:04:23,000 --> 00:04:24,000
We're going to do it.

97
00:04:24,000 --> 00:04:26,000
We're going to do it.

98
00:04:26,000 --> 00:04:28,000
We're going to do it.

99
00:04:28,000 --> 00:04:29,000
And we're going to do it.

100
00:04:29,000 --> 00:04:34,000
And then we go in a way to go in the way into the very end.

101
00:04:34,000 --> 00:04:35,000
Maybe the next one clear.

102
00:04:35,000 --> 00:04:36,000
.

103
00:04:36,000 --> 00:04:37,000
A little Jordan user, who walked around.

104
00:04:37,000 --> 00:04:38,000
Lots of the..

105
00:04:38,000 --> 00:04:39,000
He walked around.

106
00:04:39,000 --> 00:04:41,000
Just the camera sixty.

107
00:04:41,000 --> 00:04:42,000
That..

108
00:04:42,000 --> 00:04:43,000
That way.

109
00:04:43,000 --> 00:04:44,000
And I from his one word thing is that I think it's plain but it's enough.

110
00:04:44,000 --> 00:04:45,000
Wow.

111
00:04:45,000 --> 00:04:46,000
I had a feel.

112
00:04:46,000 --> 00:05:08,280
There are three parts to this, there's the incense, candles, and the flowers.

113
00:05:08,280 --> 00:05:15,920
How many incense are they?

114
00:05:15,920 --> 00:05:17,920
How many incense?

115
00:05:17,920 --> 00:05:19,920
How many candles?

116
00:05:19,920 --> 00:05:22,920
Those are one.

117
00:05:22,920 --> 00:05:26,920
You know what, they're gonna sit one.

118
00:05:26,920 --> 00:05:28,920
I think it's a...

119
00:05:28,920 --> 00:05:30,920
I think it's too many incense.

120
00:05:30,920 --> 00:05:32,920
How many flowers?

121
00:05:32,920 --> 00:05:35,920
That's okay.

122
00:05:39,920 --> 00:05:43,920
What color is this flower anymore?

123
00:05:43,920 --> 00:05:46,920
Over a few of them.

124
00:05:46,920 --> 00:05:50,920
The important thing is that there's different colors and also things in the white.

125
00:05:50,920 --> 00:05:53,920
If you should have to remember, never put one color.

126
00:05:53,920 --> 00:05:57,920
You have to put more than one color.

127
00:05:57,920 --> 00:05:59,920
These three, what do they represent?

128
00:05:59,920 --> 00:06:01,920
Anybody?

129
00:06:01,920 --> 00:06:03,920
So...

130
00:06:07,920 --> 00:06:09,920
Anybody know?

131
00:06:09,920 --> 00:06:11,920
Anybody?

132
00:06:11,920 --> 00:06:13,920
No.

133
00:06:13,920 --> 00:06:15,920
No.

134
00:06:15,920 --> 00:06:17,920
Okay.

135
00:06:21,920 --> 00:06:25,920
And I said that I said that this is...

136
00:06:25,920 --> 00:06:28,920
You said that the white is...

137
00:06:28,920 --> 00:06:30,920
I think in the...

138
00:06:30,920 --> 00:06:44,920
Okay, there are three.

