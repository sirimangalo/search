1
00:00:00,000 --> 00:00:06,400
Okay, welcome back to our study of the Damabanda.

2
00:00:06,400 --> 00:00:14,500
Today we continue on with verse number 64, which reads as follows.

3
00:00:14,500 --> 00:00:24,500
Now we are going to see the past study.

4
00:00:24,500 --> 00:00:30,500
Now we are going to see the past study.

5
00:00:30,500 --> 00:00:37,500
So we are going to see the past study.

6
00:00:37,500 --> 00:00:44,500
Now we are going to see the past study.

7
00:00:44,500 --> 00:00:53,500
Now we are going to see the past study.

8
00:00:53,500 --> 00:01:08,500
Now, so that among which are not in such a person, such a fool, even though they spend their whole life with the wise person, doesn't realize the damma for themselves.

9
00:01:08,500 --> 00:01:28,500
That means super asangita, just as the spoon in the soup, doesn't taste the soup.

10
00:01:28,500 --> 00:01:40,500
So this is a verse based on another very short story of the venerable Dai Yi, who gets a bit of a fool.

11
00:01:40,500 --> 00:01:52,500
I'm not sure if this is Lalo Dai Yi or just another Wu Dai Yi, but it's one of those people who never really caught on to the actual teaching.

12
00:01:52,500 --> 00:01:58,500
It may not have been a bad monk, but it just stuck around.

13
00:01:58,500 --> 00:02:03,500
It didn't really have a clue as to what the Buddha taught and certainly hadn't realized it for himself.

14
00:02:03,500 --> 00:02:16,500
But when the elders left the Damma Hall, and there was nobody around, he liked to go and sit up in the Damasana.

15
00:02:16,500 --> 00:02:30,500
The Damasana chair, they have a special chair where someone when they're giving a Damatok sits, so that even a younger monk would be sitting higher than the senior monks for the time that they were giving the Damatok.

16
00:02:30,500 --> 00:02:43,500
So whoever's teaching the Damma, they would have them sit higher as a show of respect.

17
00:02:43,500 --> 00:02:50,500
So he used to go and sit on it, and pretend like he was some great howl-dern, he knew what he was doing.

18
00:02:50,500 --> 00:02:59,500
So one day he's doing this and a bunch of visiting monks come in, and they see him up on the Damatok, and they go, this must be some great howl-dern.

19
00:02:59,500 --> 00:03:14,500
And so they all go up and they sit around him and ask him all sorts of questions about the aggregates and very deep subjects, but the five aggregates and the twelve.

20
00:03:14,500 --> 00:03:22,500
I had done a twelve basis, and the eighteen, that was the eighteen element.

21
00:03:22,500 --> 00:03:31,500
I asked him all sorts of questions, and he's like, I don't know. He turns out to be a fool and not have any answers.

22
00:03:31,500 --> 00:03:43,500
And the other monks are appalled because the monastery was staying with the monastery of the Buddha, so he said to themselves, how can it be?

23
00:03:43,500 --> 00:03:57,500
Here you are, how long have you been a monk? And I guess he was an elder, so he'd been a monk for at least ten years, or at least he'd been there for some time, and they were appalled that he had been with the Buddha all that time and hadn't learned anything.

24
00:03:57,500 --> 00:04:05,500
And so criticizing him, they went over to pay respect to the Buddha.

25
00:04:05,500 --> 00:04:18,500
They waited until the appropriate time and made an appointment to see the Buddha, and went in and paid respect to the Buddha, and said, when the Buddha was served, there's this monk, and he's sitting up in the dhamma chair, and we asked him all these questions, and he had in the clue.

26
00:04:18,500 --> 00:04:32,500
And when we're thinking, how can it be that someone who'd been with you so long, living in the same monastery as the blessed one himself, still wasn't able to realize the dhamma firm.

27
00:04:32,500 --> 00:04:56,500
And the Buddha taught them, given his teaching, and spoke this verse, he said, even if a fool, even if they lived together with a white, with white people for a hundred years, because they're a fool, they never realized that the dhamma no matter how, how much time they spent.

28
00:04:56,500 --> 00:05:11,500
And then he said, it's just like the spoon, when you put a spoon in the soup, it never has an opportunity to taste the soup no matter if you keep it in the soup for a hundred years.

29
00:05:11,500 --> 00:05:15,500
So that's the verse, that's the story, and that's the verse.

30
00:05:15,500 --> 00:05:22,500
The actual teaching behind it is obviously more generally applicable than just the story.

31
00:05:22,500 --> 00:05:28,500
It's a caution to us, and some people might even be surprised that this is possible.

32
00:05:28,500 --> 00:05:38,500
How could it be that you could spend so much time even your whole life with white people and never become wise?

33
00:05:38,500 --> 00:05:42,500
And the analogy of the spoon in the soup is quite apt.

34
00:05:42,500 --> 00:05:56,500
It's because of the nature of the people. If a person is of a nature to be cruel and lustful and arrogant and deluded and all these bad things.

35
00:05:56,500 --> 00:06:14,500
So it performs unwholesome acts and it's unwholesome in their speech and unguarded in their behavior and unwholesome in the mind.

36
00:06:14,500 --> 00:06:21,500
If they're inclined towards unwholesomeness, then physical presence doesn't help them.

37
00:06:21,500 --> 00:06:29,500
You see this acutely with even people who can answer questions and do have knowledge of the dhamma.

38
00:06:29,500 --> 00:06:49,500
People who have studied a lot but have never practiced can still inside be incredibly clueless to the truth.

39
00:06:49,500 --> 00:07:04,500
So they have trouble answering questions in a cogent fashion that they aren't able to put together the Buddha's teaching into a way that's understandable because they themselves don't understand it.

40
00:07:04,500 --> 00:07:18,500
So when asked deep questions, even people who have studied a lot, it's easy to tell whether they have or not practiced based on their ability to explain the teaching and the cogent man.

41
00:07:18,500 --> 00:07:28,500
Meditation is really the key. I always try to bring these videos back to our meditation practice and it's the meditation that allows you to taste the soup so to speak.

42
00:07:28,500 --> 00:07:42,500
If a person isn't practicing the teachings, it's like reading a map but never driving or reading the driver's manual and never actually getting behind the wheel.

43
00:07:42,500 --> 00:08:00,500
Our knowledge of the Buddha's teaching and even our irrational understanding and agreement of the Buddha's teaching, it's similar to any philosophy where you will find philosophers passionately arguing for a certain philosophy but not following it themselves.

44
00:08:00,500 --> 00:08:15,500
It's like doctors who smoke cigarettes or themselves have poor health habits.

45
00:08:15,500 --> 00:08:21,500
It's one way of putting it but it can just be a superficial understanding or a lack of understanding.

46
00:08:21,500 --> 00:08:31,500
There are many reasons for this. There are monks who become monks for the wrong reason.

47
00:08:31,500 --> 00:08:38,500
Because they have nowhere else to go. There are social outcasts and so on in Asian countries.

48
00:08:38,500 --> 00:08:49,500
It's often poor people who ordain as novices, novices will become novices not because they have this keen interest at seven years old to become enlightened.

49
00:08:49,500 --> 00:08:57,500
Because at seven years old they don't have enough food to eat and their parents can't feed them so they go to the monastery and they're promised a meal and shelter.

50
00:08:57,500 --> 00:09:07,500
Getting off on the wrong foot like this, they grow up in the wrong way. By the time they become monks, it's in trained in their own way.

51
00:09:07,500 --> 00:09:12,500
They're trained in the wrong way. They're set in being a career monk.

52
00:09:12,500 --> 00:09:21,500
As a result, they don't have the inclination or they're in urine. You can say against the realization.

53
00:09:21,500 --> 00:09:26,500
In urine, I don't know if that's right. We're looking for like insulated against it.

54
00:09:26,500 --> 00:09:30,500
They're not able to realize it because of the interest.

55
00:09:30,500 --> 00:09:44,500
No, they're in urine. I think it's the word I can. But they're far from it. They're not able to connect with the dhamma because they're way of thinking about it as a livelihood.

56
00:09:44,500 --> 00:10:01,500
It's an intellectual pursuit or as a culture. There's so much ingrained in them that is wrong.

57
00:10:01,500 --> 00:10:16,500
In order to remove right view, then it is to remove wrong view. They're paraphrasing.

58
00:10:16,500 --> 00:10:23,500
But the point is, the person with wrong view, you can teach them meditation quite easily because they're wrong views. It's very easy to change.

59
00:10:23,500 --> 00:10:31,500
It's very hard to teach because no matter what you say, they say, yes, I know. They don't realize that they don't really know what they're talking about.

60
00:10:31,500 --> 00:10:39,500
They know everything but they know nothing.

61
00:10:39,500 --> 00:10:55,500
And then there are monks who are corrupt who are just using it as a livelihood or even engaging in evil practices.

62
00:10:55,500 --> 00:11:11,500
We're talking last night about the four types of lotus. There are just some people who will never get the dhamma and will realize it. Maybe their minds are in the wrong place.

63
00:11:11,500 --> 00:11:35,500
They want to learn people who are foolish, who are engaged in wasting their time and who are too lazy to actually put the teachings into practice. Again, it's not a criticism or it's not really a want to go around pointing fingers, but it's an admonishment to all of us to take this seriously.

64
00:11:35,500 --> 00:11:45,500
And we should never be complacent just because we're Buddhists or obviously just because we're monks. I think that's often the case. There are people who think that just because we're monks who are perfect.

65
00:11:45,500 --> 00:11:55,500
Of course, and there's the opposite case, people who don't never give monks a break and are always criticizing the smallest faults in monks.

66
00:11:55,500 --> 00:12:05,500
People like that don't realize how difficult it is to be alone. But it certainly isn't enough. It's not enough to call ourselves Buddhists.

67
00:12:05,500 --> 00:12:13,500
They people are the same. It's very easy to become complacent thinking that I'm Buddhist now and that's enough.

68
00:12:13,500 --> 00:12:21,500
It's certainly not enough. A person, the Buddha said a person can live their whole life with a wise person and never become wise. It has all to do with our own practice.

69
00:12:21,500 --> 00:12:33,500
Good companions are only useful if we emulate them, if we follow them, if we learn from them. Otherwise we become a burden on them and they're better off without us.

70
00:12:33,500 --> 00:12:45,500
So our practice of cultivating wholesome actions, wholesome speech, wholesome thoughts, and cultivating wisdom.

71
00:12:45,500 --> 00:13:03,500
It's all, the burden is all placed on us. It's at every moment how we act, how we speak, how we think, whether we have mindfulness, whether we have awareness, whether we're cultivating insight and wisdom for ourselves.

72
00:13:03,500 --> 00:13:09,500
That's the determination of whether we taste the soup. Otherwise we're just like a spoon.

73
00:13:09,500 --> 00:13:17,500
It can be surrounded by the teachings but never realize the teachings for yourself.

74
00:13:17,500 --> 00:13:25,500
Still not a bad thing. There's lots of good that can come from being surrounded by the Buddha's teaching and being a monk and so on.

75
00:13:25,500 --> 00:13:29,500
But to realize the Buddha's teaching you have to practice for yourself.

76
00:13:29,500 --> 00:13:36,500
The Buddha said, even the Buddha is just one who shows the way it's us who have to walk the path.

77
00:13:36,500 --> 00:13:42,500
Simple teaching, something is a reminder for us and it's a nice analogy for us to remember.

78
00:13:42,500 --> 00:13:48,500
The simile of the spoon in the soup. Tomorrow we have the other side of the coin, those who can taste the soup.

79
00:13:48,500 --> 00:13:52,500
So tune in, not tomorrow, next time. So tune in next time.

80
00:13:52,500 --> 00:14:01,500
Thank you all for keeping up and for the positive feedback. I appreciate it and I'm glad to know that these videos are a benefit, which for everyone out there too.

81
00:14:01,500 --> 00:14:08,500
Be able to put the teachings of the Buddha into practice and find true peace, happiness and freedom from suffering.

