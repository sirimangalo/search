1
00:00:00,000 --> 00:00:07,000
Hello and welcome back to ask a month. Today I'll be answering the question and regards

2
00:00:07,000 --> 00:00:21,000
to physical exercise and whether it is important to give up or is it proper to give up physical

3
00:00:21,000 --> 00:00:28,000
exercise and concern about the body as well being and health and welfare or is it important

4
00:00:28,000 --> 00:00:35,000
to look after the body and therefore important for us to engage in physical exercise?

5
00:00:35,000 --> 00:00:42,000
And basically this is the question of what role does physical exercise play in the life

6
00:00:42,000 --> 00:00:50,000
of saying enlightened being to put it one way? I think I have a pretty definite answer on this

7
00:00:50,000 --> 00:00:59,000
and that is that it doesn't play any part. The idea of outright physical exercise seems to

8
00:00:59,000 --> 00:01:07,000
be a little bit silly really as I think it does for anyone who has taken up a lifestyle

9
00:01:07,000 --> 00:01:22,000
where we eat food simply enough to live. And this may seem a little bit odd for most people

10
00:01:22,000 --> 00:01:27,000
because most of us think that that is how we eat. We all eat simply enough to live,

11
00:01:27,000 --> 00:01:34,000
but really we don't. I think in general we eat far more than is necessary to live, but

12
00:01:34,000 --> 00:01:39,000
the amount that we eat is generally necessary to keep up our lifestyles which tend to include

13
00:01:39,000 --> 00:01:47,000
things like physical exercise, physical exertion and a lot of mental stress, a lot of emotional

14
00:01:47,000 --> 00:01:54,000
stress and so on. And so as a result of that, the food is barely adequate.

15
00:01:54,000 --> 00:02:05,000
For many people the food is in fact is far too much. People who become obese or who get

16
00:02:05,000 --> 00:02:12,000
sicknesses because of the fact that they are eating beyond their needs and their requirements.

17
00:02:12,000 --> 00:02:21,000
But even for those people who are eating enough and what seems to be enough to live by,

18
00:02:21,000 --> 00:02:32,000
it means enough to live that lifestyle by a lifestyle of one who is well built and has a good shape.

19
00:02:32,000 --> 00:02:39,000
I remember thinking how silly it was the first time I thought of this question when one of the meditators

20
00:02:39,000 --> 00:02:47,000
he became a monk and I think after he was a monk he was talking about how

21
00:02:47,000 --> 00:02:54,000
as a monk he felt kind of sad that he couldn't do the same things he did as a layperson

22
00:02:54,000 --> 00:02:58,000
when he was meditating as a layperson in our monastery.

23
00:02:58,000 --> 00:03:01,000
This was in what voice would happen in Chiang Mai, Thailand.

24
00:03:01,000 --> 00:03:08,000
Every day he would go running I think or jogging down the steps and this is a very large staircase,

25
00:03:08,000 --> 00:03:15,000
thousand steps or something and then jogging all the way back up every day and I said why would you do that?

26
00:03:15,000 --> 00:03:23,000
I wanted to keep in shape and it just struck me as so odd.

27
00:03:23,000 --> 00:03:27,000
I think it's something that most people wouldn't find funny but I found it incredibly funny and I thought

28
00:03:27,000 --> 00:03:29,000
I said to him I said what shape?

29
00:03:29,000 --> 00:03:32,000
What shape do you want to be in a square shape?

30
00:03:32,000 --> 00:03:34,000
A round shape?

31
00:03:34,000 --> 00:03:35,000
A triangle shape?

32
00:03:35,000 --> 00:03:39,000
Is there some specific shape that you want to keep in?

33
00:03:39,000 --> 00:03:48,000
Obviously we understand this expression and what it means but if you think about it it really is meaningless.

34
00:03:48,000 --> 00:03:51,000
What's the difference between this shape and that shape?

35
00:03:51,000 --> 00:03:58,000
The obvious answer being that our attachment to the body leads us to which for it to be in a certain shape

36
00:03:58,000 --> 00:04:02,000
and our attachment to central pleasure and romance and so on,

37
00:04:02,000 --> 00:04:06,000
leads us to want to have a body that is attractive to others as well.

38
00:04:06,000 --> 00:04:15,000
This is the only reason for wanting to be in a certain shape but this is one part of the argument

39
00:04:15,000 --> 00:04:21,000
but the claim that people make is that this is necessary for your physical well-being.

40
00:04:21,000 --> 00:04:27,000
As I said it's necessary if you want to eat the amount of food that you are eating.

41
00:04:27,000 --> 00:04:35,000
If a person eats a lot of food then it will be necessary and they go hand-at-hand with a person wants to exercise

42
00:04:35,000 --> 00:04:38,000
in good shape then they have to eat a lot.

43
00:04:38,000 --> 00:04:43,000
If a person wants to eat a lot and wants to eat rich foods and wants to eat all day

44
00:04:43,000 --> 00:04:48,000
as monks and meditators we only eat once in the morning so it seems really ridiculous

45
00:04:48,000 --> 00:04:53,000
to consider doing any real exercise.

46
00:04:53,000 --> 00:04:58,000
But if you want to eat a lot and if you have a lifestyle where you eat three times a day

47
00:04:58,000 --> 00:05:03,000
or four times a day or have all sorts of snacks and so on,

48
00:05:03,000 --> 00:05:09,000
then yeah you really need to exercise because your body is being polluted by all of these toxins

49
00:05:09,000 --> 00:05:15,000
and excess, even excess of good things.

50
00:05:15,000 --> 00:05:19,000
So it's really only, in short it's really only because of our lifestyle

51
00:05:19,000 --> 00:05:25,000
that we think that somehow it's going to exercise is going to be somehow necessary

52
00:05:25,000 --> 00:05:27,000
or a part of our lives.

53
00:05:27,000 --> 00:05:31,000
If you're meditating and especially if you're doing

54
00:05:31,000 --> 00:05:35,000
the walking meditation or just being mindful in your daily life

55
00:05:35,000 --> 00:05:40,000
and doing all your activities with mindfulness then your body will be in such a state

56
00:05:40,000 --> 00:05:46,000
that the blood is able to flow and digest the system is able to work properly

57
00:05:46,000 --> 00:05:50,000
and you'll find it incredible how healthy you are

58
00:05:50,000 --> 00:05:54,000
and how little sickness you have in the body, your body is fit

59
00:05:54,000 --> 00:06:01,000
and I know monks that are 80, 90 years old and still quite fit and able

60
00:06:01,000 --> 00:06:04,000
because they're also good meditators and because of their meditation practice

61
00:06:04,000 --> 00:06:09,000
they're able to keep themselves in good shape

62
00:06:09,000 --> 00:06:13,000
or in good physical conditions.

63
00:06:13,000 --> 00:06:19,000
I think there's room for physical exercise if you're sick

64
00:06:19,000 --> 00:06:24,000
or if your body is out of its ordinary condition

65
00:06:24,000 --> 00:06:28,000
it might be a necessary part of your rehabilitation to undergo exercise

66
00:06:28,000 --> 00:06:30,000
even if a person is obese.

67
00:06:30,000 --> 00:06:34,000
Obesity can be a real problem because it can lead to high blood pressure,

68
00:06:34,000 --> 00:06:38,000
it can lead to whatever high cholesterol, I don't know what exactly

69
00:06:38,000 --> 00:06:44,000
there are a lot of sicknesses, diabetes and so on comes from obesity

70
00:06:44,000 --> 00:06:49,000
so in that case it might be necessary and advisable for you to undertake

71
00:06:49,000 --> 00:06:52,000
this sort of thing temporarily while you're overcoming your condition

72
00:06:52,000 --> 00:06:56,000
and at the same time undertaking a more reasonable lifestyle

73
00:06:56,000 --> 00:06:59,000
because ultimately that's what's the most important

74
00:06:59,000 --> 00:07:03,000
taking care of your body means not putting things into it

75
00:07:03,000 --> 00:07:06,000
that are going to pollute it, that are going to make it unhealthy

76
00:07:06,000 --> 00:07:10,000
because it's like a car you have to be careful of the gas

77
00:07:10,000 --> 00:07:14,000
you have to make sure you have high quality gasoline

78
00:07:14,000 --> 00:07:19,000
and you have engine oil all the time and so on

79
00:07:19,000 --> 00:07:22,000
and that you take care of it, taking care of your body

80
00:07:22,000 --> 00:07:25,000
is far more important than something like exercise

81
00:07:25,000 --> 00:07:30,000
and in fact exercise can have the effect of weakening the body

82
00:07:30,000 --> 00:07:34,000
or stressing the body in some sense

83
00:07:34,000 --> 00:07:36,000
I mean people say that it's good for cardiovascular health

84
00:07:36,000 --> 00:07:41,000
if you do jogging or so on

85
00:07:41,000 --> 00:07:45,000
and I don't know if I would exactly dispute that

86
00:07:45,000 --> 00:07:49,000
but I do know that a person in the meditate doesn't have a problem with cardiovascular health

87
00:07:49,000 --> 00:07:54,000
doesn't have a problem with digestive health doesn't have a problem with obesity

88
00:07:54,000 --> 00:07:58,000
and so on if they're undertaking meditation and the meditator's lifestyle

89
00:07:58,000 --> 00:08:01,000
I mean we can all eat, most of us can eat a lot less

90
00:08:01,000 --> 00:08:05,000
I can because I eat very little as it is, I eat just enough to live

91
00:08:05,000 --> 00:08:10,000
and I've come to realize that that's really a proper way to live

92
00:08:10,000 --> 00:08:13,000
just enough that if I didn't eat as much

93
00:08:13,000 --> 00:08:17,000
it would be difficult for me to walk up the stairs every day

94
00:08:17,000 --> 00:08:20,000
but if I eat as much then okay I can walk up the stairs

95
00:08:20,000 --> 00:08:24,000
and I can walk around and I can have energy to teach

96
00:08:24,000 --> 00:08:28,000
and energy to do this and that and energy to do walking meditation

97
00:08:28,000 --> 00:08:31,000
and that's enough for me

98
00:08:31,000 --> 00:08:35,000
if I eat more then I become actually quite lethargic

99
00:08:35,000 --> 00:08:40,000
and you feel unhealthy having gotten used to

100
00:08:40,000 --> 00:08:45,000
this lifestyle of eating just enough to survive

101
00:08:45,000 --> 00:08:49,000
and one final note is that in regards to this idea

102
00:08:49,000 --> 00:08:54,000
that certain exercises might be important to maintain

103
00:08:54,000 --> 00:08:57,000
a certain level of physical health

104
00:08:57,000 --> 00:09:01,000
is that I wouldn't really take that as a very good excuse

105
00:09:01,000 --> 00:09:04,000
I mean I would say in that case if people thought that

106
00:09:04,000 --> 00:09:07,000
somehow their health was going to be affected

107
00:09:07,000 --> 00:09:09,000
if they didn't exercise

108
00:09:09,000 --> 00:09:14,000
their diet was okay but they still felt that cardiovascular health

109
00:09:14,000 --> 00:09:17,000
or this other some kind of system in the body

110
00:09:17,000 --> 00:09:24,000
would be benefited by exercise or some sort

111
00:09:24,000 --> 00:09:27,000
I would give the same answers I give to people who ask

112
00:09:27,000 --> 00:09:29,000
whether yoga is a good idea

113
00:09:29,000 --> 00:09:31,000
stretching is a good idea

114
00:09:31,000 --> 00:09:32,000
some people will say

115
00:09:32,000 --> 00:09:35,000
yoga is a spiritual practice and then I've given an answer

116
00:09:35,000 --> 00:09:38,000
that if it's a spiritual practice which it should be considered

117
00:09:38,000 --> 00:09:40,000
then you're following that spiritual path

118
00:09:40,000 --> 00:09:44,000
and I don't think it's the same spiritual path as the Buddha taught

119
00:09:44,000 --> 00:09:48,000
but if you're just undertaking yoga for the purpose of stretching

120
00:09:48,000 --> 00:09:53,000
then what you're actually getting involved in is this attachment to the body

121
00:09:53,000 --> 00:09:58,000
which I think is part of this question in regards to exercising

122
00:09:58,000 --> 00:10:03,000
is that if it becomes an important part of your life

123
00:10:03,000 --> 00:10:06,000
that your body should be in a healthy condition

124
00:10:06,000 --> 00:10:09,000
and so therefore you go out of your way to do things

125
00:10:09,000 --> 00:10:13,000
simply for the purpose of keeping the body

126
00:10:13,000 --> 00:10:17,000
in a heightened state of health or some perfectly healthy state

127
00:10:17,000 --> 00:10:20,000
then you're really missing a part of the Buddha's teaching

128
00:10:20,000 --> 00:10:25,000
that is the physical body is not going to last forever

129
00:10:25,000 --> 00:10:29,000
and our attachment to the physical body is only going to cause a suffering

130
00:10:29,000 --> 00:10:33,000
because eventually it's going to break and it's going to fail and break down

131
00:10:33,000 --> 00:10:35,000
eventually we're going to have to let it go

132
00:10:35,000 --> 00:10:40,000
so if it comes to just doing the wise things that keep your body healthy

133
00:10:40,000 --> 00:10:44,000
like eating right and doing making movements

134
00:10:44,000 --> 00:10:46,000
like doing the walking meditation

135
00:10:46,000 --> 00:10:48,000
not simply sitting and lying down all day

136
00:10:48,000 --> 00:10:55,000
but actually walking doing some simple walking to keep the body moving

137
00:10:55,000 --> 00:10:59,000
then there's really no problem when it comes to doing something like

138
00:10:59,000 --> 00:11:04,000
intentional stretching like yoga and stretching yourself out

139
00:11:04,000 --> 00:11:11,000
then it seems to me more in the line of

140
00:11:11,000 --> 00:11:14,000
the non-acceptance of the current state of the body

141
00:11:14,000 --> 00:11:20,000
where if you have aches and pains instead of coming to accept

142
00:11:20,000 --> 00:11:24,000
and bear with and eventually really overcome these states

143
00:11:24,000 --> 00:11:28,000
you try to work your way out of them

144
00:11:28,000 --> 00:11:33,000
you try to get yourself into a state where those experiences don't arise

145
00:11:33,000 --> 00:11:36,000
the same goes with someone who wants to do jogging because they say

146
00:11:36,000 --> 00:11:39,000
I feel great after I jog and I feel great

147
00:11:39,000 --> 00:11:42,000
when I build up my muscles and I feel great and so on

148
00:11:42,000 --> 00:11:47,000
and this freedom from any kind of sickness

149
00:11:47,000 --> 00:11:52,000
another example is people who instead of just eating ordinary food

150
00:11:52,000 --> 00:11:56,000
they have very special eye diets because they know this is good for you

151
00:11:56,000 --> 00:12:01,000
I know quite a few people like this who you know they take

152
00:12:01,000 --> 00:12:05,000
have known people who have taken these very special diets

153
00:12:05,000 --> 00:12:08,000
and then a couple of them died of cancer

154
00:12:08,000 --> 00:12:12,000
three people I know that they all died of cancer so in the end

155
00:12:12,000 --> 00:12:15,000
it wasn't really of any benefit to them at all

156
00:12:15,000 --> 00:12:19,000
you could say in some sense it gave them pleasure and it gave them

157
00:12:19,000 --> 00:12:22,000
sense of health and well being but in the end

158
00:12:22,000 --> 00:12:26,000
that only became a disadvantage to them

159
00:12:26,000 --> 00:12:30,000
because when they die when they're dying of cancer they have to

160
00:12:30,000 --> 00:12:33,000
see that the opposite effect

161
00:12:33,000 --> 00:12:36,000
regardless of what they do their body of deteriorating

162
00:12:36,000 --> 00:12:39,000
and that's really a nature of reality

163
00:12:39,000 --> 00:12:44,000
so we should not be overly concerned with our diet

164
00:12:44,000 --> 00:12:46,000
we overly concerned with our health

165
00:12:46,000 --> 00:12:50,000
we should try to minimize the impact that these things have on our lives

166
00:12:50,000 --> 00:12:52,000
not eating too much

167
00:12:52,000 --> 00:12:55,000
not getting working too much

168
00:12:55,000 --> 00:12:58,000
working out really at all

169
00:12:58,000 --> 00:13:01,000
unless it's a necessary for your job

170
00:13:01,000 --> 00:13:04,000
and of course a lot of this is more

171
00:13:04,000 --> 00:13:08,000
inclined towards the lifestyle of a monk if you're living in your life

172
00:13:08,000 --> 00:13:10,000
I mean I'm not saying it's unethical to do

173
00:13:10,000 --> 00:13:11,000
act as an exercise

174
00:13:11,000 --> 00:13:15,000
I'm just saying my advice as a monk living in the forest

175
00:13:15,000 --> 00:13:18,000
in the cave

176
00:13:18,000 --> 00:13:22,000
it's not really an important part of our lives

177
00:13:22,000 --> 00:13:25,000
and if we're looking to go further on this path

178
00:13:25,000 --> 00:13:27,000
we really should work towards giving it up

179
00:13:27,000 --> 00:13:30,000
and giving up all the things that surround

180
00:13:30,000 --> 00:13:34,000
exercise and physical health and so on

181
00:13:34,000 --> 00:13:38,000
I've often mentioned just as a final parting note

182
00:13:38,000 --> 00:13:41,000
good for thought that it would be really interesting

183
00:13:41,000 --> 00:13:44,000
I think to together some sort of sickness

184
00:13:44,000 --> 00:13:47,000
I think even now that if I had cancer

185
00:13:47,000 --> 00:13:50,000
that would be a really unique experience

186
00:13:50,000 --> 00:13:53,000
to have people who are always trying to avoid these things

187
00:13:53,000 --> 00:13:55,000
are really missing the point

188
00:13:55,000 --> 00:13:58,000
and missing out on something quite incredible

189
00:13:58,000 --> 00:14:00,000
the experience of the die of cancer

190
00:14:00,000 --> 00:14:04,000
what I think the incredible chance and opportunity

191
00:14:04,000 --> 00:14:07,000
for us to come to understand the nature of reality

192
00:14:07,000 --> 00:14:09,000
that it's not what we think it is

193
00:14:09,000 --> 00:14:11,000
it's not this wonderful body

194
00:14:11,000 --> 00:14:13,000
and how we can make it strong and so on

195
00:14:13,000 --> 00:14:17,000
it's actually quite fluid

196
00:14:17,000 --> 00:14:20,000
and uncertain and changing

197
00:14:20,000 --> 00:14:23,000
and it's quite dynamic

198
00:14:23,000 --> 00:14:25,000
you could be dead tomorrow

199
00:14:25,000 --> 00:14:26,000
it's not certain

200
00:14:26,000 --> 00:14:29,000
so if you're not able to experience

201
00:14:29,000 --> 00:14:31,000
the whole range of reality

202
00:14:31,000 --> 00:14:32,000
the whole range of experience

203
00:14:32,000 --> 00:14:33,000
then you're really going to get stuck

204
00:14:33,000 --> 00:14:35,000
and fall into suffering

205
00:14:35,000 --> 00:14:37,000
we should try to condition ourselves

206
00:14:37,000 --> 00:14:39,000
out of this

207
00:14:39,000 --> 00:14:40,000
or take us out of this conditioning

208
00:14:40,000 --> 00:14:43,000
so that we are in a condition

209
00:14:43,000 --> 00:14:46,000
to accept the whole range of experience

210
00:14:46,000 --> 00:14:47,000
which includes sickness

211
00:14:47,000 --> 00:14:49,000
which includes old age

212
00:14:49,000 --> 00:14:50,000
which includes death

213
00:14:50,000 --> 00:14:52,000
and if we can do that

214
00:14:52,000 --> 00:14:53,000
then where could we be born

215
00:14:53,000 --> 00:14:55,000
where could we arise

216
00:14:55,000 --> 00:14:56,000
where could we go

217
00:14:56,000 --> 00:14:57,000
what could we meet with

218
00:14:57,000 --> 00:14:58,000
that would ever give us

219
00:14:58,000 --> 00:15:00,000
that would ever cause us

220
00:15:00,000 --> 00:15:01,000
stressing suffering

221
00:15:01,000 --> 00:15:03,000
if we can just come to accept

222
00:15:03,000 --> 00:15:04,000
things as they are

223
00:15:04,000 --> 00:15:07,000
and to interact with reality

224
00:15:07,000 --> 00:15:09,000
rather than react to it

225
00:15:09,000 --> 00:15:11,000
and to be

226
00:15:11,000 --> 00:15:13,000
then it's really not much

227
00:15:13,000 --> 00:15:15,000
that could ever cause us

228
00:15:15,000 --> 00:15:16,000
any sort of difficulty

229
00:15:16,000 --> 00:15:18,000
or suffering

230
00:15:18,000 --> 00:15:20,000
so hope that helps

231
00:15:20,000 --> 00:15:21,000
as usual

232
00:15:21,000 --> 00:15:22,000
I know there are going to be people

233
00:15:22,000 --> 00:15:24,000
who disagree

234
00:15:24,000 --> 00:15:26,000
to get all of the arms of others

235
00:15:26,000 --> 00:15:27,000
but all of us

236
00:15:27,000 --> 00:15:28,000
to reach their own

237
00:15:28,000 --> 00:15:29,000
and this is my

238
00:15:29,000 --> 00:15:30,000
ask a monk

239
00:15:30,000 --> 00:15:31,000
so he has to make this

240
00:15:31,000 --> 00:15:32,000
my answer

241
00:15:32,000 --> 00:15:54,000
is not the best

