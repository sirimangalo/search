1
00:00:00,000 --> 00:00:04,580
Hello, and welcome back to our study of the Dhamapanda.

2
00:00:04,580 --> 00:00:10,340
Today we continue on with verse number 68, which reads as follows.

3
00:00:10,340 --> 00:00:22,360
Tanchya Kamankatansad, who young katwananutapati, yasapati tosumannu vipakan bhakti sai wati,

4
00:00:22,360 --> 00:00:29,060
which means it's actually the same as the last verse, so there's a chat in here.

5
00:00:29,060 --> 00:00:35,800
That means end, you know, so the last one was about problem of doing bad deeds,

6
00:00:35,800 --> 00:00:46,040
when this one is tanchya, and that Kamankatansad, who when done is well done.

7
00:00:46,040 --> 00:00:55,760
Young katwananutapati, which when performed, doesn't burn one up, no, doesn't cause them to burn up.

8
00:00:55,760 --> 00:01:04,800
Yasapati, yasapati, thoseumannu, which one, the performing of which, because of which,

9
00:01:04,800 --> 00:01:14,240
one is delighted, and sumannu of good mind or a happy mind.

10
00:01:14,240 --> 00:01:18,400
Vipakan bhakti sai wati, when they receive the results.

11
00:01:18,400 --> 00:01:27,400
So, when you receive the results of a deed, and it's pleasant and peaceful, and gives you a,

12
00:01:27,400 --> 00:01:30,040
makes you happy in mind, happy at heart.

13
00:01:30,040 --> 00:01:34,400
This is a well done deed.

14
00:01:34,400 --> 00:01:40,840
Tanchya katwananutapati, yasapati is well done, sadhu.

15
00:01:40,840 --> 00:01:46,840
So, the story behind this verse is in the time of the Buddha,

16
00:01:46,840 --> 00:01:55,920
in the kingdom of King Bimbisara, in Rajagaha, which is a kingdom that's now a city in

17
00:01:55,920 --> 00:02:01,800
northeastern India, northern India, in the province of Bihar.

18
00:02:01,800 --> 00:02:09,960
It's one of the places we go on our pilgrimage of the holy sites.

19
00:02:09,960 --> 00:02:17,600
Bimbisara was a sotapana, when he first met the Buddha, and he listened to the Buddha's

20
00:02:17,600 --> 00:02:24,520
teaching, he realized the nibana at the moment of listening to the Buddha's teaching.

21
00:02:24,520 --> 00:02:28,560
So, he was a special king and said, therefore, a special kingdom.

22
00:02:28,560 --> 00:02:36,680
And it so happened that in this kingdom, there was a gardener named Sumana, and the story

23
00:02:36,680 --> 00:02:46,760
goes that every day he would collect eight measures of flowers, every morning, and offer

24
00:02:46,760 --> 00:02:56,040
them to the king, for which he would receive eight katwananut, one to eight gold coins.

25
00:02:56,040 --> 00:03:00,680
And one day, as he was collecting his flowers in the morning, he saw the Buddha coming

26
00:03:00,680 --> 00:03:08,720
for arms, followed by 500 monks, or a large company of monks.

27
00:03:08,720 --> 00:03:12,880
And he'd never seen the Buddha before, I guess, and this morning, when he saw the

28
00:03:12,880 --> 00:03:22,160
morning, when he saw the Buddha, it overwhelmed him with a great joy of seeing such a

29
00:03:22,160 --> 00:03:26,880
spiritually enlightened being, being in the presence of someone who is so powerful and

30
00:03:26,880 --> 00:03:37,280
so cultivated, it just struck him quite deeply, moved him.

31
00:03:37,280 --> 00:03:42,160
And so immediately he thought, what could he do to reverence this person?

32
00:03:42,160 --> 00:03:49,160
Because this is a tradition in religious cultures like India, too.

33
00:03:49,160 --> 00:03:55,880
For ordinary people who maybe didn't have their sights set on a spiritual life to pay

34
00:03:55,880 --> 00:03:56,880
reverence to them.

35
00:03:56,880 --> 00:04:03,360
They figured, the idea is, well, we can't live our lives like you, but we can honor

36
00:04:03,360 --> 00:04:04,360
your life.

37
00:04:04,360 --> 00:04:09,560
And in a way, there's something to that, it's a reaffirmation of what you believe in.

38
00:04:09,560 --> 00:04:16,720
So a person who pays respect or reverence is something that is worthy of reverence.

39
00:04:16,720 --> 00:04:19,360
It sets in their mind, it affirms in their mind what they believe in.

40
00:04:19,360 --> 00:04:24,560
So I believe in what this person is doing, my contribution to their cause, and so on and

41
00:04:24,560 --> 00:04:25,560
so on.

42
00:04:25,560 --> 00:04:30,920
And if it seems somehow strange that this is what he would immediately think, it's what

43
00:04:30,920 --> 00:04:36,920
religious people often think, religious lay people that that, even though they themselves

44
00:04:36,920 --> 00:04:40,760
can't live the life, they want to support it.

45
00:04:40,760 --> 00:04:46,840
And by supporting it, it affirms in their mind their dedication to that type of a path.

46
00:04:46,840 --> 00:04:51,880
And this is the good karma, so that in the future, good things come from it.

47
00:04:51,880 --> 00:04:54,200
So immediately he thought, what can I do?

48
00:04:54,200 --> 00:05:00,160
And he looks and he sees these flowers that he's preparing to offer to the king and he

49
00:05:00,160 --> 00:05:05,760
thinks to himself, these flowers, I'll give these flowers to the Buddha and just the

50
00:05:05,760 --> 00:05:11,080
thought arises in his mind and then immediately the second thought arises, but these are

51
00:05:11,080 --> 00:05:16,640
the flowers that I'm supposed to give to the king.

52
00:05:16,640 --> 00:05:20,480
And so he's got this dilemma because kings in the time of the Buddha, of course, were

53
00:05:20,480 --> 00:05:29,240
known to be rather harsh in their meeting out of justice or in their judgments of their

54
00:05:29,240 --> 00:05:30,800
citizens.

55
00:05:30,800 --> 00:05:39,000
So there was some fear from the king, from the king's reaction, so he thought to himself

56
00:05:39,000 --> 00:05:47,960
if the king could have me beheaded or executed, exiled, banished from the land.

57
00:05:47,960 --> 00:05:51,880
But as this thought, as this worry, as this fear is coming up in his mind, he shakes

58
00:05:51,880 --> 00:05:55,560
it off and he says, I don't care.

59
00:05:55,560 --> 00:05:57,720
This is it.

60
00:05:57,720 --> 00:06:06,280
If the king doesn't like it, if the king vanishes me, what can the king do for me?

61
00:06:06,280 --> 00:06:09,400
Most of the king can do for me is give me wealth in this life.

62
00:06:09,400 --> 00:06:14,600
He can give me these aikahapana every day and it allows me to feed my family.

63
00:06:14,600 --> 00:06:16,040
That's all he can give me.

64
00:06:16,040 --> 00:06:21,640
But the Buddha, this guy, who I've heard about, but I've never seen until today, what

65
00:06:21,640 --> 00:06:27,080
he can give me, what he can give me is something spiritual and by my dedication and

66
00:06:27,080 --> 00:06:34,320
my devotion to him, it's something that could last lifetimes, millions of countless

67
00:06:34,320 --> 00:06:38,120
lifetimes into the future.

68
00:06:38,120 --> 00:06:45,360
And so he makes this mind and he says, if he makes a decision, if my mind is pleased

69
00:06:45,360 --> 00:06:47,240
by this, I will do it.

70
00:06:47,240 --> 00:06:53,360
And immediately this pleasure and this joy comes up just from the idea of offering, paying

71
00:06:53,360 --> 00:07:01,120
respect to such a worthy object.

72
00:07:01,120 --> 00:07:06,760
And so he makes the decision and he offers the flowers and in India they would place

73
00:07:06,760 --> 00:07:08,840
the flowers on his feet or so on.

74
00:07:08,840 --> 00:07:13,440
The story in the book, I'm not going to really subscribe to it.

75
00:07:13,440 --> 00:07:19,760
I'm not saying it didn't happen, but it may not have happened that way.

76
00:07:19,760 --> 00:07:24,120
So the way they say it happened in the book is when he threw them up, he threw two measures

77
00:07:24,120 --> 00:07:29,880
above the Buddha and they hung their suspended and he threw two measures over the left

78
00:07:29,880 --> 00:07:35,400
side and the left side of the Buddha was a curtain of flowers and then he threw them up

79
00:07:35,400 --> 00:07:38,400
on the right side and they stayed suspended on the right side and he threw two measures

80
00:07:38,400 --> 00:07:44,240
on the back and so there was like this cave of flowers surrounding the Buddha that was

81
00:07:44,240 --> 00:07:48,560
just floating there and then he walked through the city with these flowers to spend

82
00:07:48,560 --> 00:07:49,560
it around him.

83
00:07:49,560 --> 00:07:57,040
But I'm going to sort of not deny that, but we're going to blur them over that, they're

84
00:07:57,040 --> 00:08:01,200
going to skip over that and say he offered these flowers, you know, offer the flowers

85
00:08:01,200 --> 00:08:08,120
to the Buddha and it was a great thing, it's a great sacrifice because it's not that

86
00:08:08,120 --> 00:08:14,440
he was obligated to the king, it's that kings, you don't enter into the contract the

87
00:08:14,440 --> 00:08:17,000
same way as he would with an ordinary person.

88
00:08:17,000 --> 00:08:22,120
If a king expects something, usually they get it, whether or not you've promised or

89
00:08:22,120 --> 00:08:25,920
whether or not it's not like these flowers belong to the king, you know, the story of

90
00:08:25,920 --> 00:08:29,520
Kudutra, which is I think also in here somewhere.

91
00:08:29,520 --> 00:08:34,240
She actually stole flowers from the queen, we went through it right, this was the story

92
00:08:34,240 --> 00:08:37,640
of Samawati, we've already been there.

93
00:08:37,640 --> 00:08:43,880
So Kudutra was her maid and she stole flowers from, she stole money from the queen and

94
00:08:43,880 --> 00:08:48,320
instead of buying flowers, she'd only buy half the flowers and give her half the flowers,

95
00:08:48,320 --> 00:08:51,040
the other half the money she would steal for herself.

96
00:08:51,040 --> 00:08:56,960
And one day she listened to the Buddha's teaching and came back with all the flowers

97
00:08:56,960 --> 00:09:04,480
and the queen as well, why is there twice as many flowers today and Kudutra had become

98
00:09:04,480 --> 00:09:09,640
a sotepana, so she had no interest in lying whatsoever.

99
00:09:09,640 --> 00:09:15,040
So she told Samawati, queen Samawati, I've been stealing from you, every day I've stolen

100
00:09:15,040 --> 00:09:20,040
half the flowers, stolen half the money and bought you half the flowers.

101
00:09:20,040 --> 00:09:21,040
So what's different today?

102
00:09:21,040 --> 00:09:25,080
Well today I heard the teachings of the Buddha and there's no way I would steal after

103
00:09:25,080 --> 00:09:27,280
that.

104
00:09:27,280 --> 00:09:33,200
And Samawati was of course a great being and she wasn't angry at all and instead she

105
00:09:33,200 --> 00:09:37,720
took Kudutra as her teacher and asked Kudutra to go and learn all of the Buddha's teaching

106
00:09:37,720 --> 00:09:39,720
and come back and teach them.

107
00:09:39,720 --> 00:09:43,560
So that was a bad thing that she had done, Kudutra was a thief in the beginning.

108
00:09:43,560 --> 00:09:47,000
He's not a thief here, he's not stealing these, he's the gardener and as far as when

109
00:09:47,000 --> 00:09:52,720
to understand from the story there is, but he's got an unspoken contractor, maybe even

110
00:09:52,720 --> 00:09:59,400
a contract with a king to sell them to the king for eight gahapana.

111
00:09:59,400 --> 00:10:01,320
He doesn't do that, offers them to the Buddha.

112
00:10:01,320 --> 00:10:05,600
And then he goes, pays respect to the Buddha, bows down at his feet and goes back to

113
00:10:05,600 --> 00:10:08,680
his wife, goes back home.

114
00:10:08,680 --> 00:10:11,880
His wife greets him at the door and normally he'd bring the flowers home, prepare them

115
00:10:11,880 --> 00:10:14,320
and offer to them to the king.

116
00:10:14,320 --> 00:10:16,760
She asks him, where are your flowers today?

117
00:10:16,760 --> 00:10:22,920
And he says, well I saw the great fully enlightened Buddha walking for arms and I offered

118
00:10:22,920 --> 00:10:31,160
the flowers to him and his wife is incensed by this.

119
00:10:31,160 --> 00:10:35,040
His wife is a simpleton, according to the book, Moghapurisa, probably is when it says

120
00:10:35,040 --> 00:10:38,360
I didn't check the poly.

121
00:10:38,360 --> 00:10:44,760
And she's upset by it and she says, are you crazy?

122
00:10:44,760 --> 00:10:52,440
We could be killed, the kings are dangerous people, he could have us beheaded, he could

123
00:10:52,440 --> 00:11:01,120
have us exiled and he repeated what he had said, he said, you know, whatever the king

124
00:11:01,120 --> 00:11:05,760
can do to me, it's nothing compared to what the Buddha can do for us.

125
00:11:05,760 --> 00:11:10,360
But she wouldn't have any of it, she took her children and left him and went to see

126
00:11:10,360 --> 00:11:16,240
the king and told the king exactly what happened and said, explained to the king what she

127
00:11:16,240 --> 00:11:24,240
had said to him, how please if you're going to punish my husband, consider that he's

128
00:11:24,240 --> 00:11:28,640
no longer my husband, I've left him, me and my children are now separated from him, we had

129
00:11:28,640 --> 00:11:34,160
nothing to do with this, if you're going to punish him, punish him alone.

130
00:11:34,160 --> 00:11:40,400
Of course, King Bimbissara is, as I said, quite a special being and so he wasn't angry

131
00:11:40,400 --> 00:11:46,960
at all, but he realized that this, he realized what had happened and he knew the garden

132
00:11:46,960 --> 00:11:52,480
or someone who was a great man and hearing this of course, it made him only think higher

133
00:11:52,480 --> 00:11:58,080
of the garden or seminar and so he realized this woman is probably not right for the garden

134
00:11:58,080 --> 00:12:06,600
or anyway, she's a Mogapurisa and so he pretends to be angry and he said, oh, he did

135
00:12:06,600 --> 00:12:07,600
what?

136
00:12:07,600 --> 00:12:11,640
And she repeats and he said, are you saying that he took the flowers and he was going

137
00:12:11,640 --> 00:12:14,400
to offer to me and she said yes.

138
00:12:14,400 --> 00:12:18,160
And he said, well, thank you, you've done the right thing by leaving him, of course,

139
00:12:18,160 --> 00:12:21,760
she has because it's good for him, good for Sumana.

140
00:12:21,760 --> 00:12:26,600
You've done the right thing by leaving him, leave it to me, I'll see that he gets his

141
00:12:26,600 --> 00:12:38,200
just desserts and the woman goes away satisfied thinking that she's won the day.

142
00:12:38,200 --> 00:12:41,800
And Bimbissara immediately, the King Bimbissara immediately gets his retinue and goes to

143
00:12:41,800 --> 00:12:46,040
see the Buddha, pays respect to the Buddha and they walk together.

144
00:12:46,040 --> 00:12:50,640
The Buddha, of course, realizing that the king, knowing that the king is at peace and has no

145
00:12:50,640 --> 00:13:01,840
resentment for this flower situation and so they walk together and they walk back to

146
00:13:01,840 --> 00:13:06,880
the king's palace and they sit down outside of the palace and the people are all able

147
00:13:06,880 --> 00:13:14,720
to see these flowers suspended above and around the Buddha, just keep over that.

148
00:13:14,720 --> 00:13:18,640
But then the Buddha calls Sumana over the gardener.

149
00:13:18,640 --> 00:13:25,160
He calls someone to bring the gardener Sumana to them and the Buddha asks, what is it that

150
00:13:25,160 --> 00:13:26,160
you said?

151
00:13:26,160 --> 00:13:27,160
What did you think?

152
00:13:27,160 --> 00:13:30,160
What were you thinking to yourself when you offered me these flowers that you normally

153
00:13:30,160 --> 00:13:36,720
offered to the king and Sumana is with the king sitting right there, he said, I thought

154
00:13:36,720 --> 00:13:41,320
to myself, no matter what the king can do for me, the best he can offer me is physical

155
00:13:41,320 --> 00:13:42,320
wealth.

156
00:13:42,320 --> 00:13:47,120
But the Buddha, what the Buddha can offer me, what I can get from paying respect, paying

157
00:13:47,120 --> 00:13:53,480
homage to such a great being, that is something that will last me much longer, it will

158
00:13:53,480 --> 00:13:59,080
be much more lasting and the far greater benefit.

159
00:13:59,080 --> 00:14:07,360
And being beside him, he immediately spoke up and praised him and offered him eight elephants,

160
00:14:07,360 --> 00:14:13,880
eight horses, eight male servants, eight female servants, eight concubines from the Royal

161
00:14:13,880 --> 00:14:21,640
Haram and the eight other stuff, all sorts of things, eight of each and made him a rich

162
00:14:21,640 --> 00:14:28,280
man, eight measures of kahapana, eight thousands of treasures or something, not so many,

163
00:14:28,280 --> 00:14:30,920
made him a rich man.

164
00:14:30,920 --> 00:14:33,400
And that was that.

165
00:14:33,400 --> 00:14:42,880
And so the Buddha went back to the monastery and the monks, Ananda, Ananda asked the Buddha

166
00:14:42,880 --> 00:14:44,720
that he thought, wow, this must be a great thing.

167
00:14:44,720 --> 00:14:48,680
I wonder what he's going, what benefit would he get from this deed?

168
00:14:48,680 --> 00:14:52,040
And the Buddha, he asked the Buddha, and the Buddha said, Ananda, don't think that this

169
00:14:52,040 --> 00:14:55,080
is just an ordinary deed that the gardener did.

170
00:14:55,080 --> 00:15:02,920
This is something that with his true, truly pleased and delighted mind, delighting and

171
00:15:02,920 --> 00:15:05,360
goodness, delighting in the good deeds.

172
00:15:05,360 --> 00:15:09,240
In the future, in some future time, he's going to be reborn as a particular Buddha,

173
00:15:09,240 --> 00:15:16,080
on the Buddha foretold that someday, probably, Sumana is still up in heaven somewhere,

174
00:15:16,080 --> 00:15:21,240
waiting to be reborn as a private Buddha, or maybe it's already happened.

175
00:15:21,240 --> 00:15:25,640
But someday, he will become a particular Buddha.

176
00:15:25,640 --> 00:15:29,760
Then one day, the monks were sitting in the hall of Dhamma talking about this, saying,

177
00:15:29,760 --> 00:15:30,760
wow, it's great.

178
00:15:30,760 --> 00:15:34,320
It's amazing how he could do such a good thing, isn't it great?

179
00:15:34,320 --> 00:15:39,760
He would be doing a good deed, isn't it such a great thing to see the benefit?

180
00:15:39,760 --> 00:15:43,600
Great to see how good people are rewarded for their good deed.

181
00:15:43,600 --> 00:15:46,400
And the Buddha walked in and asked them, monks, what are you talking about?

182
00:15:46,400 --> 00:15:51,080
And the monks said, Ben or Mosur, we're talking about Stumana, the gardener, and they

183
00:15:51,080 --> 00:15:56,760
tell them what they were saying, and the Buddha said, indeed monks, a deed should be done.

184
00:15:56,760 --> 00:16:03,040
Those deeds should be done that need to happiness and peace, that when performed do not cause

185
00:16:03,040 --> 00:16:09,280
one to feel remorse, don't make one burn up in the mind.

186
00:16:09,280 --> 00:16:17,000
And then he said, the verse, Dancha, Kamang, Katang, Saa, Nhu, Yankatwana, Nutapati, Yesa, Parthi,

187
00:16:17,000 --> 00:16:21,480
those Sumana, Vipakam, Parthi, Savati.

188
00:16:21,480 --> 00:16:24,120
So how does this relate to our practice today?

189
00:16:24,120 --> 00:16:31,960
Well, good deeds of doing good deeds never gets old, this is one part of these stories.

190
00:16:31,960 --> 00:16:33,920
The virtues never change.

191
00:16:33,920 --> 00:16:39,160
So there's nothing different today about doing good deeds, any good deeds that you can do.

192
00:16:39,160 --> 00:16:43,880
Not just offering, not just giving, but any good deeds, helping other people, teaching

193
00:16:43,880 --> 00:16:45,680
other people.

194
00:16:45,680 --> 00:16:52,400
But no meditation is considered the greatest good deed, the greatest wholesomeness.

195
00:16:52,400 --> 00:17:02,920
And morality, keeping ethical precepts, stopping, abstaining from unwholesomeness.

196
00:17:02,920 --> 00:17:04,920
These are all good things.

197
00:17:04,920 --> 00:17:09,360
The point is that this is what leads to real happiness, and this is what the meditation

198
00:17:09,360 --> 00:17:10,360
shows us.

199
00:17:10,360 --> 00:17:11,360
It's amazing.

200
00:17:11,360 --> 00:17:14,960
People today, the happiness that we look for.

201
00:17:14,960 --> 00:17:23,640
And we do things knowing, really we know often, often we know intellectually, that these

202
00:17:23,640 --> 00:17:26,880
things that we're doing are not bringing us happiness, these activities that we engage

203
00:17:26,880 --> 00:17:33,040
in, the sources of happiness that we're pursuing are not actually making us happy, are not

204
00:17:33,040 --> 00:17:34,840
actually satisfying us in any way.

205
00:17:34,840 --> 00:17:40,680
They make us suffer physically, they make us suffer mentally, they cause arguments and

206
00:17:40,680 --> 00:17:43,040
conflict between people.

207
00:17:43,040 --> 00:17:46,600
They don't lead to happiness in any appreciable form.

208
00:17:46,600 --> 00:17:51,640
And yet we cling to them, we chase after them because of the way the mind works, because

209
00:17:51,640 --> 00:17:57,840
of the reward cycle, the chemicals in the brain, the pleasure that they bring for a brief

210
00:17:57,840 --> 00:18:05,440
moment reminds us and reaffirms the habit and causes us to seek them out again.

211
00:18:05,440 --> 00:18:08,880
These are the things that don't bring us happiness.

212
00:18:08,880 --> 00:18:17,680
What really and truly brings us happiness is goodness and so the Buddha said to focus our

213
00:18:17,680 --> 00:18:21,960
attention on good deeds, focus our efforts on good deeds.

214
00:18:21,960 --> 00:18:25,200
This is a great way to keep yourself from falling into doing bad deeds.

215
00:18:25,200 --> 00:18:32,760
If you're always caught up, if you're always engaged in doing good deeds and being kind

216
00:18:32,760 --> 00:18:40,520
and helpful to others, in being moral and ethical and in practicing meditation, cultivating

217
00:18:40,520 --> 00:18:46,720
your mind, then you won't have time and your mind won't sink back into unhosted this.

218
00:18:46,720 --> 00:18:52,280
Doing good deeds is the greatest way to prevent yourself from falling into bad deeds.

219
00:18:52,280 --> 00:19:00,320
So people often complain about how Buddhism today, populism seems to be all about generosity

220
00:19:00,320 --> 00:19:05,040
and giving, which you find in Buddhist countries, it's all about giving, giving, giving.

221
00:19:05,040 --> 00:19:10,560
So there's often this backlash and people think how it's not the Buddha's teaching

222
00:19:10,560 --> 00:19:15,560
and how you don't really need to do any good deeds, just practice meditation.

223
00:19:15,560 --> 00:19:19,760
Of course meditation is a good deed, but people who say you don't have to do good deeds,

224
00:19:19,760 --> 00:19:23,800
all you have to do is stop doing evil.

225
00:19:23,800 --> 00:19:28,880
Masi Sayana once pointed out, which I totally agree with.

226
00:19:28,880 --> 00:19:34,160
If you're not, as I said, if you're not doing good deeds, then it's very easy, or it's

227
00:19:34,160 --> 00:19:37,960
very difficult to keep yourself in doing bad deeds, and what are you going to do besides

228
00:19:37,960 --> 00:19:38,960
unhosted deeds?

229
00:19:38,960 --> 00:19:44,240
It's very difficult to keep yourself from them if you're not engaged in good deeds.

230
00:19:44,240 --> 00:19:49,080
So the benefits of goodness, it brings you happiness, it purifies the mind, it creates

231
00:19:49,080 --> 00:19:54,480
habits of goodness, it turns you into a better person, it brings physical well-being,

232
00:19:54,480 --> 00:19:59,160
it calms your body, it allows you to digest your food better, it allows you to sleep

233
00:19:59,160 --> 00:20:06,320
better, it brings your friends, it brings you riches, it brings you security and safety,

234
00:20:06,320 --> 00:20:11,480
it calms the world, it purifies the world, it makes.

235
00:20:11,480 --> 00:20:15,880
It brings goodness to the universe, it changes destiny.

236
00:20:15,880 --> 00:20:33,000
And deeds should never be seen as, should never be looked down upon, nah, as I go, nah

237
00:20:33,000 --> 00:20:38,680
nah ti chi tai pasa nam hi apakana mata tina, there's no such thing as a small gift

238
00:20:38,680 --> 00:20:44,320
if given with a pure heart, there's one thing, it's not what I was thinking about, nah

239
00:20:44,320 --> 00:20:52,520
ti, you shouldn't look at a karma, shouldn't look at a deed as being small, no deed

240
00:20:52,520 --> 00:21:00,080
is small, but that's about bad deeds, anyway there's lots of, lots of quotes about good

241
00:21:00,080 --> 00:21:08,240
deeds and bad deeds, karma is so important because it changes you, so if you're performing

242
00:21:08,240 --> 00:21:12,320
good deeds, it makes you a better person, it allows you to calm your mind and allows

243
00:21:12,320 --> 00:21:17,760
you to cultivate wholesomeness, if you're performing bad deeds, good luck with the meditation,

244
00:21:17,760 --> 00:21:24,920
not going to happen, not going to go very easily, so this is a part of our practice, it's

245
00:21:24,920 --> 00:21:30,680
really a fundamental step, becoming a better person, being generous and kind, being moral

246
00:21:30,680 --> 00:21:38,280
and ethical and being calm and composed in the mind, so that is the teaching for today

247
00:21:38,280 --> 00:21:45,800
and as with the one, together with the one last time, it makes a pair that we shouldn't

248
00:21:45,800 --> 00:21:52,200
just not do bad deeds, if a deed makes you suffer because of it and makes you feel guilty

249
00:21:52,200 --> 00:22:00,520
because of it, you shouldn't focus on doing good deeds, do those deeds that when done,

250
00:22:00,520 --> 00:22:06,200
you don't burn up, you don't feel guilty when you receive the results of them, it's

251
00:22:06,200 --> 00:22:15,080
happiness, it's peace, it's delight, happy at the results, so that's the teaching for today,

252
00:22:15,080 --> 00:22:44,360
wishing you all peace, happiness and freedom from suffering, see you next time.

