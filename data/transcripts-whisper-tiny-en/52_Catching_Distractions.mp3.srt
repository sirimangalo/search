1
00:00:00,000 --> 00:00:04,000
catching distractions.

2
00:00:04,000 --> 00:00:08,800
When I get distracted, I tell myself wandering, but when I do that, I find myself in a bubble

3
00:00:08,800 --> 00:00:13,600
in a sort of meta view of myself meditating, and soon I find it very difficult to go back

4
00:00:13,600 --> 00:00:18,960
wholeheartedly to rising falling. How do I remedy this?

5
00:00:18,960 --> 00:00:23,520
One of the biggest challenges in mindfulness meditation is catching the essence of your present

6
00:00:23,520 --> 00:00:27,600
experience. When you're mind-wonders, there are probably many associated

7
00:00:27,600 --> 00:00:33,280
mind states, arising in sequence. There may be enjoyment of the wandering thoughts, or a worry,

8
00:00:33,280 --> 00:00:38,080
fear, stress, etc., and these will contribute to your state of distraction.

9
00:00:39,200 --> 00:00:43,520
The fact that you cannot go back to the rising and falling means it is probable that there

10
00:00:43,520 --> 00:00:48,960
is some such mind state that you are not clearly aware of. It is important to see what is happening

11
00:00:48,960 --> 00:00:53,840
in the present moment, rather than to think that it is distracting you from your meditation and

12
00:00:53,840 --> 00:00:59,760
try to ignore it. It will just make it more difficult to stay focused. It is much more important

13
00:00:59,760 --> 00:01:04,160
that you watch what is really holding your attention and focus on it before you can even think about

14
00:01:04,160 --> 00:01:09,440
going on to note something else. When you have noted the thought, you can return to the rising

15
00:01:09,440 --> 00:01:14,480
and falling of the abdomen, but if you feel any emotion associated with the thought, you should

16
00:01:14,480 --> 00:01:20,160
focus on that first. If it is difficult to watch the rising and the falling, just focus on that state,

17
00:01:20,160 --> 00:01:26,160
even if it means saying to yourself unclear or distracted or cloudy or however it feels to you.

18
00:01:27,120 --> 00:01:31,360
Try not to focus so much of your attention on the rising and falling, but focus on whatever

19
00:01:31,360 --> 00:01:54,560
your mind is aware of and when that is gone, then go back to the rising and falling.

