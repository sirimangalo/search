1
00:00:00,000 --> 00:00:04,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:04,000 --> 00:00:11,000
Today we continue with verse 251, which reads as follows.

3
00:00:34,000 --> 00:00:42,000
There is no fire like passion.

4
00:00:42,000 --> 00:00:52,000
There is no grip as tight as anger.

5
00:00:52,000 --> 00:01:03,000
There is no net equal to ignorant delusion.

6
00:01:03,000 --> 00:01:12,000
There is no river like craving.

7
00:01:12,000 --> 00:01:23,000
This verse was taught in response to a story of five men.

8
00:01:23,000 --> 00:01:30,000
The story says came to listen to the Buddha's teaching.

9
00:01:30,000 --> 00:01:39,000
The story makes clear that the Buddha teaches the out of respect for the Dhamma.

10
00:01:39,000 --> 00:01:48,000
He doesn't always concern himself with the audience.

11
00:01:48,000 --> 00:01:55,000
He teaches out of respect for the Dhamma and to lay down the Dhamma.

12
00:01:55,000 --> 00:02:00,000
Sometimes the people listening might not understand it.

13
00:02:00,000 --> 00:02:08,000
The story goes that these five men came to listen to the Dhamma and

14
00:02:08,000 --> 00:02:09,000
of the five men.

15
00:02:09,000 --> 00:02:16,000
The first of them, while he was sitting there listening to the Dhamma fell asleep.

16
00:02:16,000 --> 00:02:23,000
He gets fell asleep sitting up right in the middle of the Buddha's Dhamma talk.

17
00:02:23,000 --> 00:02:27,000
Could you imagine?

18
00:02:27,000 --> 00:02:33,000
The second one, while listening to the Dhamma, wasn't really listening.

19
00:02:33,000 --> 00:02:38,000
Instead, he was poking at the ground.

20
00:02:38,000 --> 00:02:45,000
He was making little doodles in the ground, squiggly lines and etchings into the ground.

21
00:02:45,000 --> 00:02:52,000
You know, kind of doodling with his hand.

22
00:02:52,000 --> 00:02:56,000
Ever seen someone do that when they're sitting?

23
00:02:56,000 --> 00:03:00,000
They're clearly not listening.

24
00:03:00,000 --> 00:03:05,000
And the third man listening, I guess they were in the forest, but

25
00:03:05,000 --> 00:03:11,000
I guess he got bored because he saw this little tree next to him and he started shaking it.

26
00:03:11,000 --> 00:03:17,000
It's like shaking this tree.

27
00:03:17,000 --> 00:03:20,000
The forest went while the Buddha was talking.

28
00:03:20,000 --> 00:03:29,000
If you watch, he was staring up at the skies.

29
00:03:29,000 --> 00:03:37,000
I don't know what he was doing, but spent the whole time just looking up at the stars.

30
00:03:37,000 --> 00:03:45,000
And the fifth one sat upright and listened attentively, maybe even with his eyes open.

31
00:03:45,000 --> 00:03:52,000
Completely absorbed in memorizing and remembering what the Buddha taught.

32
00:03:52,000 --> 00:03:55,000
And Ananda was fanning the Buddha.

33
00:03:55,000 --> 00:04:02,000
Sometimes they would fan the Buddha to keep the flies off of him or because it was hot maybe.

34
00:04:02,000 --> 00:04:06,000
Maybe it was during the day, I don't know.

35
00:04:06,000 --> 00:04:13,000
And he noticed these five men and he watched them and he thought to himself,

36
00:04:13,000 --> 00:04:25,000
what the difference among mortals, among humans, among beings, world things.

37
00:04:25,000 --> 00:04:29,000
And he said to the Buddha, he said, it's amazing.

38
00:04:29,000 --> 00:04:41,000
Here we have this monumentous occasion, the preaching of the dhamma by the Lord Buddha himself.

39
00:04:41,000 --> 00:04:46,000
The Lord Buddha's teaching is like thunder, it's like a lion's roar.

40
00:04:46,000 --> 00:04:54,000
It's like an earth's earthquake, such a profound and important event.

41
00:04:54,000 --> 00:05:03,000
Only one of these guys was really paying attention. Why were the rest of them totally distracted by something else?

42
00:05:03,000 --> 00:05:13,000
And the Buddha said, oh, it's because of people's character and their inclinations.

43
00:05:13,000 --> 00:05:25,000
And he said, that one that fell asleep for 500 lifetimes, for countless many, many, many lifetimes.

44
00:05:25,000 --> 00:05:32,000
Every lifetime, he was born as a snake.

45
00:05:32,000 --> 00:05:35,000
And so that's what snakes do.

46
00:05:35,000 --> 00:05:40,000
And when it cools down, I guess, when it heats up, I don't know.

47
00:05:40,000 --> 00:05:47,000
It curled up in a ball and fell asleep.

48
00:05:47,000 --> 00:05:50,000
It used to be a snake, that's why.

49
00:05:50,000 --> 00:05:56,000
The second one, when it was digging in the earth, well, for lifetime after lifetime,

50
00:05:56,000 --> 00:06:01,000
he was born as an earthworm.

51
00:06:01,000 --> 00:06:09,000
And so he's just absorbed in the earth, digging in the earth, I guess.

52
00:06:09,000 --> 00:06:14,000
The third one, the one that was shaking the tree while lifetime after lifetime,

53
00:06:14,000 --> 00:06:16,000
he had been born a monkey.

54
00:06:16,000 --> 00:06:23,000
And so his inclination was to fix down the trees.

55
00:06:23,000 --> 00:06:31,000
And it's just thinking about climbing the tree, maybe I don't know.

56
00:06:31,000 --> 00:06:36,000
The fourth one, the one that was looking up at the sky's, well, for lifetime after lifetime,

57
00:06:36,000 --> 00:06:43,000
he had been born an astrologer, someone who finds patterns in the stars,

58
00:06:43,000 --> 00:06:49,000
divides people's fortunes from the stars.

59
00:06:49,000 --> 00:06:51,000
And so all he could think about was the stars.

60
00:06:51,000 --> 00:06:59,000
He was fascinated and absorbed by them.

61
00:06:59,000 --> 00:07:08,000
And the fifth one, the fifth one, in many lifetimes, he had been born a school of, sorry, a student of the Vedas,

62
00:07:08,000 --> 00:07:15,000
a Brahmin student, studied a student of philosophical texts and religious texts.

63
00:07:15,000 --> 00:07:22,000
So he was well inclined towards studying and memorizing.

64
00:07:22,000 --> 00:07:33,000
And so he had this positive quality of attention that he had developed lifetime after lifetime.

65
00:07:33,000 --> 00:07:36,000
And then it was amazed and he said,

66
00:07:36,000 --> 00:07:42,000
was it possible that they were just born one thing and the Buddha said, oh, well,

67
00:07:42,000 --> 00:07:48,000
it's not even, he said, it's not possible to know what they were in every lifetime.

68
00:07:48,000 --> 00:07:53,000
But guaranteed this is where their minds were.

69
00:07:53,000 --> 00:08:04,000
And then he said to Anandai, he said,

70
00:08:04,000 --> 00:08:09,000
this is the power of these four things.

71
00:08:09,000 --> 00:08:22,000
I've said they have the power to overcome and override any inclination towards goodness.

72
00:08:22,000 --> 00:08:27,000
They have the power to keep one from cultivating good things.

73
00:08:27,000 --> 00:08:29,000
Even when the Buddha is sitting right in front of them,

74
00:08:29,000 --> 00:08:34,000
they can overpower the inclination to listen to his teachings.

75
00:08:34,000 --> 00:08:39,000
And he taught this verse.

76
00:08:39,000 --> 00:08:46,000
So the first lesson, the lesson the story gives us is a reminder

77
00:08:46,000 --> 00:08:56,000
and an eye opener as to how lucky and how rare it is

78
00:08:56,000 --> 00:09:01,000
to be able to hear and appreciate the Buddha's teaching.

79
00:09:01,000 --> 00:09:05,000
But it's rare to even have the opportunity to hear and teaching of the Buddha.

80
00:09:05,000 --> 00:09:08,000
And none of us have it, of course, in this life.

81
00:09:08,000 --> 00:09:14,000
We have the rare opportunity to hear and study the Buddha's teaching.

82
00:09:14,000 --> 00:09:21,000
But we miss the opportunity to hear it from the Buddha himself.

83
00:09:21,000 --> 00:09:25,000
But even someone who's able to hear the Buddha's teaching,

84
00:09:25,000 --> 00:09:30,000
even someone who's able to study it,

85
00:09:30,000 --> 00:09:35,000
can gain no benefit from it if they're not inclined towards it,

86
00:09:35,000 --> 00:09:38,000
if they're not able to appreciate it.

87
00:09:38,000 --> 00:09:47,000
And if they're not in a state where they're able to pay attention.

88
00:09:47,000 --> 00:09:51,000
There are many people, Buddhists even, who appreciate the Buddha's teaching,

89
00:09:51,000 --> 00:09:53,000
but are unable to pay attention to it.

90
00:09:53,000 --> 00:09:57,000
You can see them doing these sorts of things during the Dhamma talk.

91
00:09:57,000 --> 00:10:04,000
Maybe distracted thinking about food or distracted thinking about work,

92
00:10:04,000 --> 00:10:11,000
distracted thinking about home, family, many, many different things.

93
00:10:11,000 --> 00:10:18,000
Without lacking the mental capacity to appreciate it, to understand it,

94
00:10:18,000 --> 00:10:24,000
because of various unwholesome inclinations habits,

95
00:10:24,000 --> 00:10:30,000
because of development, cultivation in the wrong direction.

96
00:10:30,000 --> 00:10:34,000
So this tells us two things. One, to appreciate the fact that we can,

97
00:10:34,000 --> 00:10:40,000
first of all, appreciate the Buddha's teaching,

98
00:10:40,000 --> 00:10:45,000
that we can understand it, and to appreciate the fact that

99
00:10:45,000 --> 00:10:51,000
that we have this opportunity, until not wasted or squander it.

100
00:10:51,000 --> 00:10:54,000
Because if we develop ourselves in the wrong way, the Buddha's teaching might come around,

101
00:10:54,000 --> 00:10:56,000
and we might just ignore it.

102
00:10:56,000 --> 00:10:59,000
You might just have no capacity to appreciate it.

103
00:10:59,000 --> 00:11:03,000
If you can appreciate and understand the teaching now,

104
00:11:03,000 --> 00:11:08,000
this is an important opportunity that you have.

105
00:11:08,000 --> 00:11:13,000
It should be an eye opener that we take for granted

106
00:11:13,000 --> 00:11:18,000
sometimes our mental capacity to understand things if we're able to.

107
00:11:18,000 --> 00:11:23,000
We're not realizing that some people will,

108
00:11:23,000 --> 00:11:26,000
and it's not just, we're not saying that they're stupid.

109
00:11:26,000 --> 00:11:30,000
I mean stupidity or lack of wisdom you might say is a part of it,

110
00:11:30,000 --> 00:11:33,000
but greed is a part of it, anger is a part of it.

111
00:11:33,000 --> 00:11:37,000
There's so many things just being distracted by other things.

112
00:11:37,000 --> 00:11:40,000
Many different reasons, if our mind is not,

113
00:11:40,000 --> 00:11:45,000
if our wholesome karma is not cultivated in the right direction,

114
00:11:45,000 --> 00:11:49,000
we might just come to the point,

115
00:11:49,000 --> 00:11:53,000
we just come to the Buddha's teaching and miss it entirely.

116
00:11:53,000 --> 00:11:55,000
There are people for whom this happens,

117
00:11:55,000 --> 00:11:58,000
and this could be you if you're not careful.

118
00:11:58,000 --> 00:12:01,000
But the second part of it I think is that

119
00:12:01,000 --> 00:12:05,000
we shouldn't take for granted that we do understand the Buddha's teaching.

120
00:12:05,000 --> 00:12:08,000
It's quite common for us to,

121
00:12:08,000 --> 00:12:16,000
for one to believe that because they're listening,

122
00:12:16,000 --> 00:12:20,000
because they're attending, because they're studying the Buddha's teaching

123
00:12:20,000 --> 00:12:21,000
that they understand it.

124
00:12:21,000 --> 00:12:23,000
And one of the big things you learn as a meditator

125
00:12:23,000 --> 00:12:26,000
is that you don't really understand the demo when you start.

126
00:12:26,000 --> 00:12:30,000
The understanding you thought you had was a very superficial understanding.

127
00:12:30,000 --> 00:12:35,000
And you gain a deeper and deeper understanding of the same things through practice.

128
00:12:35,000 --> 00:12:39,000
We shouldn't take for granted the intellectual learning that we have.

129
00:12:39,000 --> 00:12:44,000
Our capacity to understand things without mental development,

130
00:12:44,000 --> 00:12:49,000
intensive mental development is so superficial.

131
00:12:49,000 --> 00:12:54,000
It's easy to feel complacent and believe that you understand the teachings

132
00:12:54,000 --> 00:12:58,000
when in fact you've only scratched the surface.

133
00:12:58,000 --> 00:13:03,000
Just a reminder that understanding the demo doesn't just mean listening to it

134
00:13:03,000 --> 00:13:06,000
and the end of playing yourself.

135
00:13:06,000 --> 00:13:10,000
It doesn't just mean attending a demo talk,

136
00:13:10,000 --> 00:13:13,000
or some people you hear about,

137
00:13:13,000 --> 00:13:17,000
they'll turn on the demo talk while they're doing the dishes,

138
00:13:17,000 --> 00:13:20,000
or while they're at work,

139
00:13:20,000 --> 00:13:24,000
or while they're doing something else,

140
00:13:24,000 --> 00:13:29,000
which I think is not really the best idea.

141
00:13:29,000 --> 00:13:32,000
The demo is the first of all something that is sacred

142
00:13:32,000 --> 00:13:34,000
and something that should be respected,

143
00:13:34,000 --> 00:13:38,000
but that's not just a religious idea.

144
00:13:38,000 --> 00:13:40,000
It's not just out of respect.

145
00:13:40,000 --> 00:13:44,000
Although that is a wholesome quality.

146
00:13:44,000 --> 00:13:47,000
It's also because it's not easy to understand.

147
00:13:47,000 --> 00:13:51,000
If you want to understand, you can't just let the words go into your head

148
00:13:51,000 --> 00:13:55,000
and say, I understand the words and the syntax and the grammar.

149
00:13:55,000 --> 00:13:59,000
Your mind has to be in the right frame.

150
00:13:59,000 --> 00:14:02,000
It has to be in the right state.

151
00:14:02,000 --> 00:14:07,000
You have to have a clarity of mind and a proper perspective,

152
00:14:07,000 --> 00:14:16,000
a mindful presence in order to understand the demo.

153
00:14:16,000 --> 00:14:23,000
And the lesson of the verse is about what exactly the things are

154
00:14:23,000 --> 00:14:25,000
that keep us from understanding,

155
00:14:25,000 --> 00:14:27,000
not just understanding a demo talk,

156
00:14:27,000 --> 00:14:30,000
but understanding the truth.

157
00:14:30,000 --> 00:14:35,000
Keep us from realizing, appreciating,

158
00:14:35,000 --> 00:14:42,000
and understanding the nature of reality.

159
00:14:42,000 --> 00:14:54,000
And a reminder of how terrible and how great the power of these things is.

160
00:14:54,000 --> 00:15:00,000
So the Buddha said, there is no fire like passion.

161
00:15:00,000 --> 00:15:07,000
So ordinarily, fires, they burst up and they can consume everything.

162
00:15:07,000 --> 00:15:09,000
Well, they don't really consume everything.

163
00:15:09,000 --> 00:15:13,000
They leave behind charred ashes.

164
00:15:13,000 --> 00:15:18,000
And when they've taken up their fuel, they burn out.

165
00:15:18,000 --> 00:15:21,000
They might rage on for a while, but they burn out eventually.

166
00:15:21,000 --> 00:15:24,000
But passion never burns out.

167
00:15:24,000 --> 00:15:27,000
Never runs out of fuel, I guess.

168
00:15:27,000 --> 00:15:30,000
And it doesn't just burn sometimes.

169
00:15:30,000 --> 00:15:33,000
It will flare up at any moment.

170
00:15:33,000 --> 00:15:41,000
You can be trying to sleep in your passion, flares up.

171
00:15:41,000 --> 00:15:44,000
Trying to work.

172
00:15:44,000 --> 00:15:46,000
Trying to focus.

173
00:15:46,000 --> 00:15:49,000
Passion is something that consumes us.

174
00:15:49,000 --> 00:15:53,000
It drives us on, it's why people go into debt.

175
00:15:53,000 --> 00:15:58,000
It's why people, why we fight, why we manipulate, why we compete.

176
00:15:58,000 --> 00:16:04,000
It leads to ambition, it leads to violence.

177
00:16:04,000 --> 00:16:12,000
It leads to cruelty, it leads to miserliness and stinginess.

178
00:16:12,000 --> 00:16:15,000
It flames on, and it inflames the mind.

179
00:16:15,000 --> 00:16:25,000
Most importantly, it's like a fire that consumes our ability to see clearly.

180
00:16:25,000 --> 00:16:29,000
Until the fire of passion dies down, you can't really understand the truth.

181
00:16:29,000 --> 00:16:33,000
You can't really understand the truth of your own situation.

182
00:16:33,000 --> 00:16:35,000
You'll be blinded by passion.

183
00:16:35,000 --> 00:16:43,000
Passion blinds us to the suffering that we cause by chasing after the things we're passionate about.

184
00:16:43,000 --> 00:16:48,000
If you're passionate about something, you don't care who you're hurting other people or hurting yourself.

185
00:16:48,000 --> 00:16:56,000
You can make yourself sick because of your passion, because of lust, because of desire.

186
00:16:56,000 --> 00:16:58,000
Look at a drug addict.

187
00:16:58,000 --> 00:17:02,000
That's what happens to them, but it's not limited only to drug addicts.

188
00:17:02,000 --> 00:17:08,000
Meditation helps us see the danger.

189
00:17:08,000 --> 00:17:31,000
It helps us see this inflamed state that see the obstruction that passion is for us.

190
00:17:31,000 --> 00:17:37,000
It's like the greatest fire leaves nothing behind.

191
00:17:37,000 --> 00:17:53,000
There is no grip like anger, so the commentary compares the grips of a monster or the grip of a wild animal maybe.

192
00:17:53,000 --> 00:18:02,000
Someone can grab you, can hold you, can pin you down, keep you from doing the many things that you'd like to be doing.

193
00:18:02,000 --> 00:18:07,000
They can even drag you away in a direction you don't want to go.

194
00:18:07,000 --> 00:18:11,000
But none of those grips are like the grip of anger.

195
00:18:11,000 --> 00:18:14,000
Anger seizes us.

196
00:18:14,000 --> 00:18:24,000
Anger causes us to do and say things that are completely against our own benefit to the benefit of others.

197
00:18:24,000 --> 00:18:27,000
It blinds us.

198
00:18:27,000 --> 00:18:32,000
It drives us like a slave driver.

199
00:18:32,000 --> 00:18:41,000
People who have anger issues will tell you they didn't mean to, they didn't want to hurt others, but the anger just blind to you.

200
00:18:41,000 --> 00:18:43,000
Just fly into a rage.

201
00:18:43,000 --> 00:18:46,000
Anger makes you reckless.

202
00:18:46,000 --> 00:18:52,000
Anger seizes you.

203
00:18:52,000 --> 00:18:58,000
People who are habitually angry also become habitually unhealthy.

204
00:18:58,000 --> 00:19:05,000
They don't take care of their bodies and their bodies start to heat up.

205
00:19:05,000 --> 00:19:10,000
They're physical, even their physical form becomes unhealthy through anger.

206
00:19:10,000 --> 00:19:17,000
Anger is like a great sickness.

207
00:19:17,000 --> 00:19:22,000
It causes you to hurt yourself and hurt others.

208
00:19:22,000 --> 00:19:27,000
It's a very fearsome sort of thing, something that we should be terrified of.

209
00:19:27,000 --> 00:19:35,000
Our own anger, anger causes you to say things, causes you to do things.

210
00:19:35,000 --> 00:19:46,000
You have the best of intentions, but when anger comes, it's just like another person, like a chuckle and hide kind of thing.

211
00:19:46,000 --> 00:19:57,000
Number three, Nati Moha, Samangjalang, there is no net like delusion.

212
00:19:57,000 --> 00:20:06,000
This is a reference to the Brahmajala, I think, but it shows the meaning of the word Brahmajala.

213
00:20:06,000 --> 00:20:11,000
The net of delusion extends through the whole universe.

214
00:20:11,000 --> 00:20:18,000
Most nets, if you wriggle around, you might actually escape from them.

215
00:20:18,000 --> 00:20:25,000
And if you avoid them, you can find a way to just not get caught up in the net.

216
00:20:25,000 --> 00:20:32,000
If the person hunting birds will throw a net, some of the birds get caught, some of them don't.

217
00:20:32,000 --> 00:20:41,000
But the Brahmajala, the net of Samsara catches even Brahmav.

218
00:20:41,000 --> 00:20:44,000
You can go anywhere in Samsara, you won't escape delusion.

219
00:20:44,000 --> 00:20:52,000
Meaning, there's no path you can take that will free you because of delusion.

220
00:20:52,000 --> 00:20:58,000
You can't become free by going to the Brahmavraum because the Brahmava still has delusion.

221
00:20:58,000 --> 00:21:04,000
Read, anger, Brahmava doesn't have any of these, but the Brahmava still has delusion.

222
00:21:04,000 --> 00:21:07,000
They still can have wrong view.

223
00:21:07,000 --> 00:21:11,000
They still can have conceit.

224
00:21:11,000 --> 00:21:14,000
Ignorance, they still have ignorance.

225
00:21:14,000 --> 00:21:21,000
Many religious paths focus on spiritual paths focus on endeavor,

226
00:21:21,000 --> 00:21:29,000
cultivating meditative practices, spiritual practices based on effort, based on concentration,

227
00:21:29,000 --> 00:21:36,000
focusing the mind, calming the mind, and they'll never escape that way.

228
00:21:36,000 --> 00:21:38,000
The net still catches you.

229
00:21:38,000 --> 00:21:41,000
The only way to escape the net is wisdom.

230
00:21:41,000 --> 00:21:48,000
Overcoming ignorance, straightening one's delusion, correcting one's delusions.

231
00:21:48,000 --> 00:21:54,000
Wrong views, one's conceits, one's self-identity, and so on.

232
00:21:54,000 --> 00:21:58,000
Only by discarding all of these.

233
00:21:58,000 --> 00:22:01,000
One cast-offs the net of delusion.

234
00:22:01,000 --> 00:22:07,000
Delusion is the net that covers greed and anger rely on delusion.

235
00:22:07,000 --> 00:22:10,000
Without delusion there can be no greed, no anger.

236
00:22:10,000 --> 00:22:13,000
Without ignorance there can be no suffering.

237
00:22:13,000 --> 00:22:18,000
Without ignorance is the beginning.

238
00:22:18,000 --> 00:22:22,000
It's the net that we're trying to escape.

239
00:22:22,000 --> 00:22:24,000
So it's quite a meditation in that sense.

240
00:22:24,000 --> 00:22:25,000
It's quite simple.

241
00:22:25,000 --> 00:22:30,000
It's not about fixing our greed or problems or anger problems.

242
00:22:30,000 --> 00:22:34,000
It's about fixing our delusion, basically fixing our ignorance problems,

243
00:22:34,000 --> 00:22:35,000
about understanding.

244
00:22:35,000 --> 00:22:39,000
It's a very simple concept.

245
00:22:39,000 --> 00:22:43,000
And finally, nut-tit-tana-sama-na-di.

246
00:22:43,000 --> 00:22:47,000
Dun-na, there is no river like craving.

247
00:22:47,000 --> 00:22:50,000
So tana is more broad, I think, than raga.

248
00:22:50,000 --> 00:22:57,000
Raga is maybe a specific type, but raga is its own as its specific idea.

249
00:22:57,000 --> 00:23:04,000
Tana is any sort of clinging, any sort of flowing really.

250
00:23:04,000 --> 00:23:07,000
Like the ass of a river.

251
00:23:07,000 --> 00:23:16,000
Any kind of inclination we have desire for anything, any ambition,

252
00:23:16,000 --> 00:23:21,000
any clinging to anything really.

253
00:23:21,000 --> 00:23:28,000
The analogy of the imagery of a river is quite apt.

254
00:23:28,000 --> 00:23:37,000
Some sorrow or life existence is like a river blowing on.

255
00:23:37,000 --> 00:23:41,000
But tana is like the clinging to something in the river.

256
00:23:41,000 --> 00:23:43,000
It just makes no sense in the end.

257
00:23:43,000 --> 00:23:45,000
Because life continues on.

258
00:23:45,000 --> 00:23:47,000
Life goes on and on.

259
00:23:47,000 --> 00:23:50,000
And enlightened being is so much, so at peace, even alive,

260
00:23:50,000 --> 00:23:54,000
even though they still suffer from physical maladies,

261
00:23:54,000 --> 00:24:03,000
from being accosted by people and insects and the elements

262
00:24:03,000 --> 00:24:06,000
and old age sickness death.

263
00:24:06,000 --> 00:24:09,000
But they live in such peace because they go with the river.

264
00:24:09,000 --> 00:24:14,000
But the river of craving here is something different from that.

265
00:24:14,000 --> 00:24:17,000
The river of craving.

266
00:24:17,000 --> 00:24:20,000
Make us craving never ends.

267
00:24:20,000 --> 00:24:25,000
It never dries up, just like the fire of passion.

268
00:24:25,000 --> 00:24:29,000
Most rivers, if you've ever been to one of the big rivers in India,

269
00:24:29,000 --> 00:24:34,000
the river where the Buddha cut off his hair and crossed the river

270
00:24:34,000 --> 00:24:37,000
and went to the Bodhi tree.

271
00:24:37,000 --> 00:24:41,000
That river dries up most of the year,

272
00:24:41,000 --> 00:24:44,000
much of the year it's mostly dried up.

273
00:24:44,000 --> 00:24:47,000
So sometimes it floods, sometimes it dries.

274
00:24:47,000 --> 00:24:50,000
But the river of craving never dries up.

275
00:24:50,000 --> 00:24:55,000
You can't just wait for your craving to dry up thinking,

276
00:24:55,000 --> 00:24:59,000
it's okay if I cling to this or cling to that.

277
00:24:59,000 --> 00:25:01,000
Eventually I'll have enough.

278
00:25:01,000 --> 00:25:06,000
Once you've had enough, there's never enough for craving.

279
00:25:06,000 --> 00:25:08,000
Caving never has enough.

280
00:25:08,000 --> 00:25:10,000
It's habitual in fact.

281
00:25:10,000 --> 00:25:18,000
The more you incline towards something, the more you want it.

282
00:25:18,000 --> 00:25:22,000
So there is no river like craving.

283
00:25:22,000 --> 00:25:25,000
The lesson for us as meditators is,

284
00:25:25,000 --> 00:25:30,000
besides the lessons of remembering the great opportunity we have

285
00:25:30,000 --> 00:25:34,000
and appreciating the need for a depth of understanding

286
00:25:34,000 --> 00:25:37,000
that can't be found just in texts.

287
00:25:37,000 --> 00:25:41,000
I think it helps us to focus our attention

288
00:25:41,000 --> 00:25:45,000
on what is really important,

289
00:25:45,000 --> 00:25:49,000
reminding us that our progress in the practice

290
00:25:49,000 --> 00:25:53,000
really comes down to our freeing ourselves

291
00:25:53,000 --> 00:25:58,000
from these things, from passion, from anger, from delusion.

292
00:25:58,000 --> 00:26:02,000
From all the many forms that these take,

293
00:26:02,000 --> 00:26:10,000
we can have the delusion that these things are good.

294
00:26:10,000 --> 00:26:15,000
We can be angry when people suggest that we give up the things we crave

295
00:26:15,000 --> 00:26:18,000
or cling to.

296
00:26:18,000 --> 00:26:23,000
We're completely ignorant about the problem and the danger.

297
00:26:23,000 --> 00:26:27,000
Even ignorant about the existence of these things in ourselves.

298
00:26:27,000 --> 00:26:31,000
Part of our meditation practice is a big part of it.

299
00:26:31,000 --> 00:26:34,000
It's just coming to see what's inside,

300
00:26:34,000 --> 00:26:37,000
coming to see that we have these things.

301
00:26:37,000 --> 00:26:42,000
See that we have them, see the nature of them.

302
00:26:42,000 --> 00:26:49,000
Just really to see the chaos and the inconsistency in our minds.

303
00:26:49,000 --> 00:26:52,000
There's no rhyme or reason to these things.

304
00:26:52,000 --> 00:26:54,000
There's no reason to get angry.

305
00:26:54,000 --> 00:26:56,000
There's no reason to cling.

306
00:26:56,000 --> 00:26:59,000
There's no reason to believe.

307
00:26:59,000 --> 00:27:03,000
There's no validity to our beliefs and so on.

308
00:27:03,000 --> 00:27:06,000
So you don't have to actually fix any of these things.

309
00:27:06,000 --> 00:27:09,000
You just see that they're meaningless and useless.

310
00:27:09,000 --> 00:27:13,000
They're a waste of time, a waste of effort.

311
00:27:13,000 --> 00:27:15,000
They're embarrassing, really.

312
00:27:15,000 --> 00:27:23,000
You realize how embarrassing it is to get caught up in things that are so useless.

313
00:27:23,000 --> 00:27:30,000
You see that they're without value and you let them go naturally.

314
00:27:30,000 --> 00:27:31,000
So a good verse.

315
00:27:31,000 --> 00:27:36,000
One of those important verses that boldly claims

316
00:27:36,000 --> 00:27:44,000
what is the essence of spiritual religious life and the essence of

317
00:27:44,000 --> 00:27:48,000
the goal of existence.

318
00:27:48,000 --> 00:27:53,000
So that's the Dhammapada for today. Thank you for listening.

