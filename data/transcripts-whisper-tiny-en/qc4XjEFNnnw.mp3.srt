1
00:00:00,000 --> 00:00:06,000
Hey, good evening, everyone. Welcome to our evening dhamma session.

2
00:00:12,000 --> 00:00:16,000
So tonight we're looking at the first noble truth.

3
00:00:18,000 --> 00:00:26,000
For each of the noble truths, there is a concept or an action

4
00:00:26,000 --> 00:00:31,000
really associated with the truth.

5
00:00:31,000 --> 00:00:35,000
When I said yesterday that the truths are all about knowing,

6
00:00:35,000 --> 00:00:39,000
it's a bit more complicated. Each of the four noble truths

7
00:00:39,000 --> 00:00:43,000
has something to be done.

8
00:00:44,000 --> 00:00:49,000
It's called a kitcha, kitcha means something to be done.

9
00:00:49,000 --> 00:01:08,000
And the Buddha said that complete accomplishment in the four noble truths

10
00:01:08,000 --> 00:01:13,000
involves an understanding of the truth,

11
00:01:13,000 --> 00:01:16,000
an understanding of what needs to be done

12
00:01:16,000 --> 00:01:26,000
in regards to the truth. And knowledge that one is accomplished, the task.

13
00:01:26,000 --> 00:01:31,000
So these three things are called satcha, which means the truth itself.

14
00:01:31,000 --> 00:01:36,000
Kitcha, which means that which needs to be done in regards to the truth.

15
00:01:36,000 --> 00:01:41,000
And kata, kata means that which has been done.

16
00:01:41,000 --> 00:01:46,000
And so for each of the four noble truths,

17
00:01:46,000 --> 00:01:49,000
there's these three, three times four is twelve.

18
00:01:49,000 --> 00:01:57,000
The Buddha called this the twelve-fold accomplishment

19
00:01:57,000 --> 00:02:00,000
in regards to the four noble truths.

20
00:02:04,000 --> 00:02:08,000
So the first noble truth is suffering.

21
00:02:08,000 --> 00:02:13,000
The first noble truth is not that life is suffering.

22
00:02:13,000 --> 00:02:20,000
You read in various introductory texts the Buddhism that the Buddha said life is suffering

23
00:02:20,000 --> 00:02:23,000
and that the first noble truth is that life is suffering.

24
00:02:23,000 --> 00:02:27,000
That's not what the first noble truth is.

25
00:02:27,000 --> 00:02:33,000
The first noble truth is suffering.

26
00:02:33,000 --> 00:02:41,000
It's the truth of suffering, that there is suffering.

27
00:02:41,000 --> 00:02:46,000
In fact, the funny thing is the one thing the Buddha leaves out of his description of suffering is life.

28
00:02:46,000 --> 00:02:51,000
So today, let's say that life is suffering is quite a surprise

29
00:02:51,000 --> 00:02:54,000
because it's the one thing the Buddha left out.

30
00:02:54,000 --> 00:02:57,000
It's mainly because life isn't an event.

31
00:02:57,000 --> 00:03:00,000
Life isn't something you can say, hey, this is happiness,

32
00:03:00,000 --> 00:03:03,000
this is suffering, life is just a concept.

33
00:03:03,000 --> 00:03:08,000
Concepts are not suffering, concepts don't exist.

34
00:03:15,000 --> 00:03:25,000
The important thing about this truth is to identify and acknowledge

35
00:03:25,000 --> 00:03:31,000
that there's a problem.

36
00:03:31,000 --> 00:03:39,000
I acknowledge that there's something that needs to be,

37
00:03:39,000 --> 00:03:47,000
needs to be addressed.

38
00:03:47,000 --> 00:03:51,000
So the Buddha outlined, he gave examples of suffering,

39
00:03:51,000 --> 00:03:56,000
birth is suffering, birth is an interesting one in itself

40
00:03:56,000 --> 00:04:01,000
because it doesn't have to be specifically suffering,

41
00:04:01,000 --> 00:04:03,000
stressful.

42
00:04:03,000 --> 00:04:08,000
Birth can be, I mean it is, in many cases, stressful,

43
00:04:08,000 --> 00:04:11,000
but not in terms of the birth itself,

44
00:04:11,000 --> 00:04:13,000
just in terms of the mechanical process.

45
00:04:13,000 --> 00:04:17,000
It can be quite painful, obviously, quite stressful.

46
00:04:17,000 --> 00:04:21,000
I don't think that's what the Buddha is getting out here,

47
00:04:21,000 --> 00:04:23,000
or at least we can look at it on two levels.

48
00:04:23,000 --> 00:04:28,000
Birth is suffering because for the mother it can be quite stressful,

49
00:04:28,000 --> 00:04:33,000
even to the point of causing injury or death,

50
00:04:33,000 --> 00:04:38,000
and it can be painful and stressful for the baby coming out.

51
00:04:38,000 --> 00:04:42,000
So the birthing process itself is a part of life.

52
00:04:42,000 --> 00:04:46,000
It's really not a good start.

53
00:04:46,000 --> 00:04:49,000
We'll talk about the miracle of birth and so on and so on.

54
00:04:49,000 --> 00:04:54,000
I mean, it's a bit of a,

55
00:04:54,000 --> 00:04:57,000
it would be a miracle if a stork delivered a baby.

56
00:04:57,000 --> 00:04:59,000
That would be a miracle,

57
00:04:59,000 --> 00:05:03,000
but this bloody screaming

58
00:05:03,000 --> 00:05:11,000
child going through a passage that's way too small for it.

59
00:05:11,000 --> 00:05:15,000
It's not really anything miraculous about it.

60
00:05:15,000 --> 00:05:17,000
I mean, it's,

61
00:05:17,000 --> 00:05:20,000
where is something I suppose that seems miraculous,

62
00:05:20,000 --> 00:05:23,000
but as far as miracles go,

63
00:05:23,000 --> 00:05:25,000
it's not the most,

64
00:05:25,000 --> 00:05:29,000
it's not the most awe-inspiring, I think.

65
00:05:29,000 --> 00:05:31,000
Not for me anyway,

66
00:05:31,000 --> 00:05:33,000
I've never been through it myself,

67
00:05:33,000 --> 00:05:35,000
not in this lifetime.

68
00:05:35,000 --> 00:05:44,000
But birth is,

69
00:05:44,000 --> 00:05:46,000
I think in a general sense,

70
00:05:46,000 --> 00:05:50,000
birth is suffering because of what it entails.

71
00:05:50,000 --> 00:05:54,000
It entails the arising of new problems.

72
00:05:54,000 --> 00:05:56,000
It means failure,

73
00:05:56,000 --> 00:05:58,000
you know, if you're born again,

74
00:05:58,000 --> 00:06:00,000
everything birthday, that's a great thing.

75
00:06:00,000 --> 00:06:03,000
We celebrate it, we celebrate it every year.

76
00:06:03,000 --> 00:06:05,000
It's kind of funny, you know,

77
00:06:05,000 --> 00:06:07,000
Buddhist don't really celebrate birthdays,

78
00:06:07,000 --> 00:06:09,000
probably for this reason,

79
00:06:09,000 --> 00:06:12,000
because the first thing we're taught is that birth is suffering,

80
00:06:12,000 --> 00:06:16,000
so when are you going to celebrate?

81
00:06:16,000 --> 00:06:19,000
You're celebrating failure, really.

82
00:06:19,000 --> 00:06:22,000
We celebrate it thinking as birth is good,

83
00:06:22,000 --> 00:06:24,000
so good that I'm a human being,

84
00:06:24,000 --> 00:06:27,000
so good that I was born this life,

85
00:06:27,000 --> 00:06:30,000
the wonder, the potential,

86
00:06:30,000 --> 00:06:34,000
such a short-sighted view.

87
00:06:34,000 --> 00:06:36,000
Because, okay, I mean,

88
00:06:36,000 --> 00:06:40,000
yes, it's a novelty,

89
00:06:40,000 --> 00:06:46,000
but we live in kind of the opposite of Groundhog Day,

90
00:06:46,000 --> 00:06:48,000
that movie,

91
00:06:48,000 --> 00:06:52,000
instead of repeating the same thing over and over again

92
00:06:52,000 --> 00:06:53,000
and remembering it,

93
00:06:53,000 --> 00:06:57,000
we repeat it over and over and think it's new.

94
00:06:57,000 --> 00:06:59,000
I don't know if there's ever been a movie

95
00:06:59,000 --> 00:07:01,000
or a science fiction book about that,

96
00:07:01,000 --> 00:07:04,000
but we keep forgetting

97
00:07:04,000 --> 00:07:09,000
and doing the same thing over and over again.

98
00:07:09,000 --> 00:07:12,000
And so we're robots being,

99
00:07:12,000 --> 00:07:14,000
having our memories erased,

100
00:07:14,000 --> 00:07:18,000
and doing it again and again and again.

101
00:07:26,000 --> 00:07:27,000
If you're born a human,

102
00:07:27,000 --> 00:07:30,000
I mean, the kindest thing we could say is

103
00:07:30,000 --> 00:07:34,000
you're born here because you still have lessons to learn.

104
00:07:34,000 --> 00:07:36,000
Which really means you just failed.

105
00:07:36,000 --> 00:07:38,000
You took the exam at the end of life

106
00:07:38,000 --> 00:07:39,000
and you failed it,

107
00:07:39,000 --> 00:07:43,000
so you got to come back and redo the course.

108
00:07:43,000 --> 00:07:46,000
I didn't graduate to heaven

109
00:07:46,000 --> 00:07:49,000
or the Brahma world.

110
00:07:49,000 --> 00:07:57,000
Certainly didn't graduate to Nirvana.

111
00:07:57,000 --> 00:08:01,000
So birth is, birth is failure.

112
00:08:01,000 --> 00:08:03,000
It's cause of great suffering.

113
00:08:03,000 --> 00:08:06,000
You're born again means you have to go through it all over again,

114
00:08:06,000 --> 00:08:09,000
all the stress and suffering of life.

115
00:08:09,000 --> 00:08:13,000
Old age, sickness, death, these are suffering.

116
00:08:13,000 --> 00:08:16,000
I don't think we need to explain that very much.

117
00:08:16,000 --> 00:08:18,000
Old age, sickness, death,

118
00:08:18,000 --> 00:08:20,000
these are the things we fear in life.

119
00:08:20,000 --> 00:08:23,000
Clearly these are the suffering aspects.

120
00:08:23,000 --> 00:08:25,000
This is what the Buddha focused on

121
00:08:25,000 --> 00:08:27,000
because it's a clear example.

122
00:08:27,000 --> 00:08:29,000
It's very little argument I think

123
00:08:29,000 --> 00:08:33,000
that old age can be quite stressful

124
00:08:33,000 --> 00:08:35,000
even if you fear some.

125
00:08:35,000 --> 00:08:38,000
Just the thought of losing our faculties.

126
00:08:38,000 --> 00:08:42,000
All timers is terrifying.

127
00:08:42,000 --> 00:08:46,000
Having a stroke and being paralyzed.

128
00:08:46,000 --> 00:09:00,000
Sickness, death, death is so fearsome.

129
00:09:00,000 --> 00:09:02,000
And then we have the minor sufferings

130
00:09:02,000 --> 00:09:05,000
that are constant throughout life is

131
00:09:05,000 --> 00:09:15,000
sorrow, lamentation, pain, sorrow, lamentation, despair.

132
00:09:15,000 --> 00:09:19,000
Dukodoma nasupa.

133
00:09:19,000 --> 00:09:25,000
So kaparideva, dukodoma nasupa yasa.

134
00:09:25,000 --> 00:09:28,000
All these various.

135
00:09:28,000 --> 00:09:30,000
And you include in there, of course,

136
00:09:30,000 --> 00:09:33,000
all sorts of mental disturbance.

137
00:09:33,000 --> 00:09:37,000
Mental illness, we might say.

138
00:09:37,000 --> 00:09:39,000
Mental upset.

139
00:09:39,000 --> 00:09:43,000
Getting upset.

140
00:09:43,000 --> 00:09:47,000
A P.A. isambiogo dukodoma.

141
00:09:47,000 --> 00:09:50,000
Being associated with what you don't.

142
00:09:50,000 --> 00:09:59,000
What is not there, which is not pleasing to us.

143
00:09:59,000 --> 00:10:02,000
P.A. Yewi payogo dukodoma.

144
00:10:02,000 --> 00:10:09,000
Being dissociated or losing what is dear to you.

145
00:10:09,000 --> 00:10:13,000
There is a song that is quite pleasant.

146
00:10:13,000 --> 00:10:15,000
There is a song that where the

147
00:10:15,000 --> 00:10:18,000
four noble truths are actually sung.

148
00:10:18,000 --> 00:10:42,000
But it is kind of funny that it should be so pleasing

149
00:10:42,000 --> 00:10:49,000
to you.

150
00:10:49,000 --> 00:10:54,000
If you don't like it, you can do it.

151
00:10:54,000 --> 00:11:03,000
If you don't like it, you can do it.

152
00:11:03,000 --> 00:11:14,000
Being associated with what is under, being dissociated from what is dear.

153
00:11:14,000 --> 00:11:20,000
And whatever you want, not getting that is suffering.

154
00:11:20,000 --> 00:11:23,000
So these are all the examples of what is suffering.

155
00:11:23,000 --> 00:11:29,000
And it's a very coarse understanding of suffering.

156
00:11:29,000 --> 00:11:34,000
It's really not a noble truth of suffering in an ultimate sense.

157
00:11:34,000 --> 00:11:38,000
It's in a sense of what you realize through the practice.

158
00:11:38,000 --> 00:11:42,000
None of this is what you realize through the practice.

159
00:11:42,000 --> 00:11:45,000
What you realize is much more refined than that.

160
00:11:45,000 --> 00:11:47,000
It's what gives all of that meaning.

161
00:11:47,000 --> 00:11:53,000
And it gives all of that reality or existence.

162
00:11:53,000 --> 00:12:00,000
The five aggregates are dukha.

163
00:12:00,000 --> 00:12:04,000
Sankitinapan-chupa-dhanakan-dhukha.

164
00:12:04,000 --> 00:12:07,000
He breathes the five aggregates.

165
00:12:07,000 --> 00:12:08,000
Our suffering.

166
00:12:08,000 --> 00:12:10,000
Which really is everything.

167
00:12:10,000 --> 00:12:12,000
Everything that arises, right?

168
00:12:12,000 --> 00:12:15,000
The Buddha said, sambhi sankara dukha.

169
00:12:15,000 --> 00:12:18,000
All sankara is all formations.

170
00:12:18,000 --> 00:12:26,000
Every aspect of experience is dukha.

171
00:12:26,000 --> 00:12:34,000
And so that's getting close to saying that, well, life is suffering, right?

172
00:12:34,000 --> 00:12:36,000
Except that it's not.

173
00:12:36,000 --> 00:12:40,000
And so here, this is where we have to understand the truths in a different way.

174
00:12:40,000 --> 00:12:46,000
And we have to understand the word suffering in a different way.

175
00:12:46,000 --> 00:12:50,000
Because it's a funny thing to call something suffering, right?

176
00:12:50,000 --> 00:12:52,000
Is that table suffering?

177
00:12:52,000 --> 00:12:56,000
Is the sound that I hear suffering?

178
00:12:56,000 --> 00:12:58,000
It's kind of weird, you know?

179
00:12:58,000 --> 00:13:01,000
Sound is clearly not suffering.

180
00:13:01,000 --> 00:13:07,000
So we have to take dukha actually as an adjective.

181
00:13:07,000 --> 00:13:09,000
It's actually not a noun.

182
00:13:09,000 --> 00:13:11,000
We're not talking about suffering.

183
00:13:11,000 --> 00:13:17,000
We're talking about something being stressful for lack of a better word.

184
00:13:17,000 --> 00:13:23,000
We really don't have a perfect word with all the right connotations in English.

185
00:13:23,000 --> 00:13:28,000
Dukha, someone told me it means do is, of course, bad.

186
00:13:28,000 --> 00:13:30,000
It just means bad but cut.

187
00:13:30,000 --> 00:13:35,000
It's like the rotating of an axle.

188
00:13:35,000 --> 00:13:40,000
If the axle is well greased, it goes well.

189
00:13:40,000 --> 00:13:45,000
If it's poorly greased, it's rough, you know?

190
00:13:45,000 --> 00:13:50,000
So, frickative is that a word?

191
00:13:50,000 --> 00:13:55,000
Friction, it has to do with friction.

192
00:13:55,000 --> 00:14:04,000
It's not smooth, not efficient, not harmonious and peaceful.

193
00:14:04,000 --> 00:14:08,000
And so that gets to the point when we talk about the five aggregates.

194
00:14:08,000 --> 00:14:19,000
What we're saying is that they are stressful or they are a cause for stress.

195
00:14:19,000 --> 00:14:21,000
And the Buddha uses the word upa down a kanda.

196
00:14:21,000 --> 00:14:37,000
He makes clear that why they're a cause for a stress is because we cling to them.

197
00:14:37,000 --> 00:14:42,000
It's the clinging to the five aggregates that is stressful.

198
00:14:42,000 --> 00:14:50,000
So, when we talk about these things as being dukha experiences in really every sun-car, every formation, as being dukha,

199
00:14:50,000 --> 00:14:56,000
what we mean is, if you cling to it, it causes your suffering.

200
00:14:56,000 --> 00:15:04,000
What we mean is that it is something that it's like calling something poisonous.

201
00:15:04,000 --> 00:15:13,000
If you call this plant or this liquid poisonous, there's nothing poisonous about it.

202
00:15:13,000 --> 00:15:19,000
It's just a liquid, it just sits there, it doesn't poison anybody, it doesn't do anything.

203
00:15:19,000 --> 00:15:33,000
But when drunk, when ingested, it has the quality.

204
00:15:33,000 --> 00:15:41,000
It has that impact on being to kill the body, poison.

205
00:15:41,000 --> 00:15:57,000
In the same way, some cars have their nature or their impact, their effect on us is to cause stress.

206
00:15:57,000 --> 00:16:03,000
When clung to. So, what it's saying really is that they're not worth clinging to.

207
00:16:03,000 --> 00:16:12,000
They're not suka, they're not something that you can acquire and find happiness through.

208
00:16:12,000 --> 00:16:14,000
That's really the truth of suffering.

209
00:16:14,000 --> 00:16:21,000
So, how it plays out, of course, is that in our meditation, we start to see that the things that we thought were making us happy,

210
00:16:21,000 --> 00:16:28,000
where a source of happiness, hey, that came again, that makes me happy, are not.

211
00:16:28,000 --> 00:16:33,000
So, this is where the understanding comes in.

212
00:16:33,000 --> 00:16:42,000
So, the kitchen, that which is to be done in regards to suffering, is to see it completely.

213
00:16:42,000 --> 00:16:52,000
It's Barinana, I guess we'll be the word, Barinana as the word.

214
00:16:52,000 --> 00:17:00,000
Barin means like Barinana, Barin means around fully, completely.

215
00:17:00,000 --> 00:17:05,000
Barinana means to know as the root to know.

216
00:17:05,000 --> 00:17:08,000
Barinana means to know something completely.

217
00:17:08,000 --> 00:17:13,000
Barinana means it should be known completely.

218
00:17:13,000 --> 00:17:15,000
It should be fully known.

219
00:17:15,000 --> 00:17:21,000
But the task in regards to suffering that the Buddha gave is not to escape.

220
00:17:21,000 --> 00:17:24,000
And this is important, I've said this many times.

221
00:17:24,000 --> 00:17:27,000
That's very important for meditators.

222
00:17:27,000 --> 00:17:30,000
Be clear, you're not coming here to escape suffering.

223
00:17:30,000 --> 00:17:35,000
You're coming here because you want to escape suffering, but in fact that is part of the problem.

224
00:17:35,000 --> 00:17:46,000
Our desire to escape suffering is causing us suffering, because we're associated with that, which we don't like.

225
00:17:46,000 --> 00:17:54,000
When, on the other hand, you fully understand suffering, and you understand the things that are suffering, that are dukkah.

226
00:17:54,000 --> 00:17:58,000
They can cause you suffering because you don't cling to them.

227
00:17:58,000 --> 00:18:02,000
When we talk about the three characteristics, this is what we mean.

228
00:18:02,000 --> 00:18:13,000
Seeing that these things that you thought were stable, satisfying, controllable, are unstable, unsatisfying, uncontrollable, not worth clinging to.

229
00:18:13,000 --> 00:18:19,000
Not me, not mine, not myself.

230
00:18:19,000 --> 00:18:26,000
And once you've seen that, once you've fully understood that, really that you've done everything.

231
00:18:26,000 --> 00:18:33,000
The four noble truths are linked in this way. You don't realize one by one by one.

232
00:18:33,000 --> 00:18:35,000
They come in a package.

233
00:18:35,000 --> 00:18:42,000
And the vesudimaga explains it like when a candle burns.

234
00:18:42,000 --> 00:18:54,000
When the flame burns the candle, the wax is melted, the wick is burnt up, and the darkness is dispelled, and the light is produced.

235
00:18:54,000 --> 00:18:57,000
The four things happen all at once.

236
00:18:57,000 --> 00:19:00,000
It's just a simple symbol to explain that.

237
00:19:00,000 --> 00:19:02,000
The four noble truths happen all.

238
00:19:02,000 --> 00:19:15,000
Not at the same moment exactly, but it's at the same time, in the same sequence, in the single sequence.

239
00:19:15,000 --> 00:19:22,000
So once you understand suffering, the rest of the four noble truths are fallen to place.

240
00:19:22,000 --> 00:19:28,000
But that's the task to be done, not to escape suffering, but to understand it completely.

241
00:19:28,000 --> 00:19:43,000
To understand these things are suffering in an abstract sense, but more importantly to observe them and to see these things that I thought were sort of happiness.

242
00:19:43,000 --> 00:19:46,000
I understand now we're not going to bring me happiness.

243
00:19:46,000 --> 00:19:53,000
And when you let go of them, of course, the rest of the four noble truths come into play.

244
00:19:53,000 --> 00:19:58,000
So there you go. That's the first noble truth in brief.

245
00:19:58,000 --> 00:20:04,000
That's the dumb of her tonight. Thank you all for tuning in.

246
00:20:04,000 --> 00:20:11,000
If there are questions, I'll go and check. If you have questions, please go to our meditation site and post them there.

247
00:20:11,000 --> 00:20:15,000
I'll be checking every day. There are no questions today.

248
00:20:15,000 --> 00:20:23,000
I don't know, is this topic something that's scaring people away?

249
00:20:23,000 --> 00:20:28,000
That's okay. No audience is fine.

250
00:20:28,000 --> 00:20:36,000
If nobody comes or maybe I've just answered all the questions already.

251
00:20:36,000 --> 00:20:46,000
So that's all for tonight. Thank you all for tuning in. Have a good night.

