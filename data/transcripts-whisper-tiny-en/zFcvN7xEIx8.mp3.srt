1
00:00:00,000 --> 00:00:24,240
Okay, good evening everyone, welcome back.

2
00:00:24,240 --> 00:00:35,640
Tonight we are looking at questions, questions that have been asked on our website.

3
00:00:35,640 --> 00:00:39,840
This week I'm sort of taking off, so you'll notice I was in here on Saturday and then

4
00:00:39,840 --> 00:00:42,840
Monday again.

5
00:00:42,840 --> 00:00:50,840
But time here today, we only have a few questions, so it might be short, assuming because

6
00:00:50,840 --> 00:00:56,960
I've been away, people have been away, but a few questions might just mean that everyone's

7
00:00:56,960 --> 00:00:58,960
got all their answers, which is good.

8
00:00:58,960 --> 00:01:04,840
We have no questions, it's a quick, quick practice.

9
00:01:04,840 --> 00:01:09,760
Okay, but we'll go through the ones we have asking questions is good.

10
00:01:09,760 --> 00:01:20,680
If you have questions and you don't ask them, that's bad, because the doubt will.

11
00:01:20,680 --> 00:01:26,680
That will prevent you from progressing, so if you have questions, you should ask.

12
00:01:26,680 --> 00:01:33,320
You shouldn't just be coming up with questions just for the sake of asking, but when you

13
00:01:33,320 --> 00:01:35,720
have them, you should ask.

14
00:01:35,720 --> 00:01:39,560
So the first question is if I could go through the Dhamma Chaka Pawatana Sutta.

15
00:01:39,560 --> 00:01:46,320
I've done videos on this Sutta, I think the latest one I did, I was just looking, I did

16
00:01:46,320 --> 00:01:48,240
one in April.

17
00:01:48,240 --> 00:01:53,600
But it seems to me more recently I've been talking about it, I can't remember where, maybe

18
00:01:53,600 --> 00:01:58,200
I did one on the three, first three discourses or something.

19
00:01:58,200 --> 00:02:04,240
The heart of the Dhamma Chaka Pawatana Sutta is the four noble truths, so maybe I can go

20
00:02:04,240 --> 00:02:07,640
through them briefly.

21
00:02:07,640 --> 00:02:13,880
The four noble truths are the truth of suffering, the cause of suffering, the cessation

22
00:02:13,880 --> 00:02:20,360
of suffering, and the path that leads to the cessation of suffering.

23
00:02:20,360 --> 00:02:33,520
Each of the truths has a statement of truth, this is the truth, and each one has a action

24
00:02:33,520 --> 00:02:41,000
that needs to be performed in regards to the truth, and each one has a realization once

25
00:02:41,000 --> 00:02:44,560
that action has been performed.

26
00:02:44,560 --> 00:02:51,640
So the truth itself is important, these are the four truths that are noble, Buddhism

27
00:02:51,640 --> 00:02:58,840
doesn't recognize truth for truth sake, it's not just about understanding everything.

28
00:02:58,840 --> 00:03:04,520
In a way you could say that's what becoming a Buddha is all about, a Buddha is about becoming

29
00:03:04,520 --> 00:03:13,040
a fully enlightened Buddha apparently is about learning everything, or coming to know everything.

30
00:03:13,040 --> 00:03:21,680
But the important thing that a Buddha or our hunt follower comes to know, and really the

31
00:03:21,680 --> 00:03:27,880
only important truths are these four, suffering, the cause of suffering, the cessation

32
00:03:27,880 --> 00:03:33,800
of suffering, and the path that leads to the cessation of suffering.

33
00:03:33,800 --> 00:03:43,120
The action in regards to each one, so the truth of suffering is to be fully understood,

34
00:03:43,120 --> 00:03:49,240
the cause of suffering is to be abandoned, the cessation of suffering is to be realized

35
00:03:49,240 --> 00:04:00,920
or seen for oneself, experienced for oneself, and the path is to be cultivated.

36
00:04:00,920 --> 00:04:04,600
And so that's really the most important part of the teaching is what are we going to do

37
00:04:04,600 --> 00:04:07,760
about it, and that's the crux of it.

38
00:04:07,760 --> 00:04:16,840
The third part is when the realization once these actions have been performed is a description

39
00:04:16,840 --> 00:04:23,400
of the attainment of enlightenment, then the Buddha said, it's only when I got through

40
00:04:23,400 --> 00:04:32,920
all of this, learning the four noble truths, following the path, and attaining the cessation

41
00:04:32,920 --> 00:04:38,800
of suffering, and coming to that realization that I called myself a Buddha.

42
00:04:38,800 --> 00:04:42,520
And so I think that's really the most important part of the Suta, I mean there's a little

43
00:04:42,520 --> 00:04:49,040
bit more, but the key, this is the turning of the wheel of the dhamma, and turning the

44
00:04:49,040 --> 00:04:54,400
wheel of the dhamma, the wheel of the dhamma is those four noble truths, and by proclaiming

45
00:04:54,400 --> 00:05:00,360
them the Buddha did something that couldn't be turned back by anyone.

46
00:05:00,360 --> 00:05:09,320
He said in motion a process of enlightenment that was to last until today impossible to

47
00:05:09,320 --> 00:05:15,400
stop, even if someone wanted to stop people from becoming enlightened at that point,

48
00:05:15,400 --> 00:05:20,160
not possible, it would roll on and people would become enlightened just through the power

49
00:05:20,160 --> 00:05:26,760
of that, the expression of that teaching, the relating it, even to a such a small audience,

50
00:05:26,760 --> 00:05:31,320
although apparently there were lots of angels listening as well, and probably a lot of

51
00:05:31,320 --> 00:05:36,440
them became Suta Panoh, they're not sure about that.

52
00:05:36,440 --> 00:05:43,160
I can't remember the exact orthodoxy about that one.

53
00:05:43,160 --> 00:05:51,160
So but I won't go into detail because I have before, because here's a question about

54
00:05:51,160 --> 00:06:01,840
against a long one, relating to people's perceived lack of enthusiasm or arrogance because

55
00:06:01,840 --> 00:06:06,440
you're being mindful of your feelings, so I won't go into detail about your particular

56
00:06:06,440 --> 00:06:18,160
situation, but in, I mean, it's a clear example of how in ordinary life, the practice

57
00:06:18,160 --> 00:06:24,600
of mindfulness appears to have a disadvantage, sorry, can be to your detriment.

58
00:06:24,600 --> 00:06:31,800
There's certainly not a uncommon experience where people get angry or upset because

59
00:06:31,800 --> 00:06:37,760
of how you've changed or because of the way you act, I mean, I think to some extent

60
00:06:37,760 --> 00:06:48,600
that you shouldn't be surprised with that because a meditator is quite a different sort

61
00:06:48,600 --> 00:06:56,040
of person, and the state of mind of a meditator is quite a different state of mind from

62
00:06:56,040 --> 00:07:04,880
someone who is constantly engaging and cultivating a culture really, right?

63
00:07:04,880 --> 00:07:11,080
Because if you were living in Thailand or Japan, if you were to jump for joy when someone

64
00:07:11,080 --> 00:07:17,560
gave you good news, they might take the gift away from you, think you're crazy or not

65
00:07:17,560 --> 00:07:19,960
deserving of it, right?

66
00:07:19,960 --> 00:07:25,560
Whereas in the West, you have to be impressed, you have to express your, you have to be

67
00:07:25,560 --> 00:07:32,560
expressive or people think there's something wrong with you.

68
00:07:32,560 --> 00:07:42,040
So, there are some neutral aspects of culture, and I think that's probably where your

69
00:07:42,040 --> 00:07:53,480
solution lies in finding a way to be expressive but still mindful because I've seen

70
00:07:53,480 --> 00:08:01,240
it then, and it is possible, as meditators, it doesn't mean we can't be expressive,

71
00:08:01,240 --> 00:08:04,920
but there's a line, and culture generally crosses this line.

72
00:08:04,920 --> 00:08:15,400
So, Asian people tend to be under expressive and purposefully trying to be impressive by

73
00:08:15,400 --> 00:08:20,520
how calm and collected there, which is ego, which is arrogant, which is conceit and all

74
00:08:20,520 --> 00:08:27,000
that, it's ego mainly, and Westerners are generally more, well, it's a generalization.

75
00:08:27,000 --> 00:08:32,840
Other people, as I say Westerners, but in other people and other cultures and other situations,

76
00:08:32,840 --> 00:08:44,640
are expressive, generally based on a lust or a passion or enjoyment of being expressive,

77
00:08:44,640 --> 00:08:54,520
and so there's this communal reaction that stimulates the chemicals in the brain, really,

78
00:08:54,520 --> 00:09:01,440
where we laugh and jump and sing and smile and so on.

79
00:09:01,440 --> 00:09:09,880
Smiling isn't always wrong, but if you think about it, some of our smiling is the

80
00:09:09,880 --> 00:09:18,040
indulgence in addiction, and the night in people do smile, the Buddha smiled, there's

81
00:09:18,040 --> 00:09:27,240
even a special smile of the Buddha, and that they talk about, but there's a line, and so

82
00:09:27,240 --> 00:09:37,520
on the one hand, the first answer is that you have to expect a certain amount of dissonance,

83
00:09:37,520 --> 00:09:43,000
because you're not going to react in the ways that people expect you to.

84
00:09:43,000 --> 00:09:47,760
You're not going to engage, and that's common, and the meditators with their families

85
00:09:47,760 --> 00:09:51,400
employees with their bosses and that sort of thing.

86
00:09:51,400 --> 00:10:00,160
But on the other side, I think the actual conflict generally, except in the cases where

87
00:10:00,160 --> 00:10:05,160
we're talking about incredibly evil people who are, not even moderately evil people who

88
00:10:05,160 --> 00:10:16,240
have difficulty being around people who are mindful and get disturbed simply by your mindfulness.

89
00:10:16,240 --> 00:10:22,840
Even in that case, a mindful person should generally accept in cases of karma, and it's

90
00:10:22,840 --> 00:10:28,040
not always possible, but generally speaking, I would say more often than not, the problem

91
00:10:28,040 --> 00:10:39,360
comes because you're overdoing it, you're over-obsessed with your own practice to the extent

92
00:10:39,360 --> 00:10:54,000
that you're not yet capable or you're not acting mindfully in the situation by engaging

93
00:10:54,000 --> 00:10:57,280
mindfully with the other person.

94
00:10:57,280 --> 00:11:03,320
You have the sense that talking to people that's hard to be mindful, so I'll just

95
00:11:03,320 --> 00:11:12,080
not talk, or engaging with other people makes me unmindful, so I'll just not engage.

96
00:11:12,080 --> 00:11:17,280
When in fact, part of the challenge, and it is a real challenge, should be to engage

97
00:11:17,280 --> 00:11:26,600
mindfully, and I wouldn't be content with a lot of the dissonance that we encounter

98
00:11:26,600 --> 00:11:28,160
as meditators.

99
00:11:28,160 --> 00:11:32,880
I would try to learn how to be mindful and mindfully engage.

100
00:11:32,880 --> 00:11:39,160
It's not necessary, but it's going to be stressful, and it's going to actually interrupt

101
00:11:39,160 --> 00:11:40,840
your practice, right?

102
00:11:40,840 --> 00:11:46,520
The whole key from a meditator point of view is to do what doesn't disrupt your practice,

103
00:11:46,520 --> 00:11:55,120
and we think, and I would say erroneously, often, we think the way is to keep back, hold

104
00:11:55,120 --> 00:11:58,120
back, right?

105
00:11:58,120 --> 00:12:03,240
I would look at your situation, and this is generally applicable, I think it's an interesting

106
00:12:03,240 --> 00:12:10,640
question, and it's certainly worth investigating, but I would always look and see whether

107
00:12:10,640 --> 00:12:21,040
you're actually being mindful or whether you're just being defensive, and try and find a

108
00:12:21,040 --> 00:12:29,200
way to interact and react without reacting, without being emotionally involved or attached.

109
00:12:29,200 --> 00:12:37,760
You can be expressive, and I would argue that meditators often take the easy way out, right?

110
00:12:37,760 --> 00:12:42,200
It's very easy to just sit, and if someone's disturbing, you could just close your eyes

111
00:12:42,200 --> 00:12:47,120
and say hearing, hearing.

112
00:12:47,120 --> 00:12:52,920
I don't think many meditators would argue that that's the correct way, and yet it's the

113
00:12:52,920 --> 00:12:56,600
easy way, and it seems intellectually correct, right?

114
00:12:56,600 --> 00:12:58,560
Because I'm mindful.

115
00:12:58,560 --> 00:13:03,520
This person's yelling at me, but I'm just saying hearing, hearing.

116
00:13:03,520 --> 00:13:06,280
That's not the right way, and you'll feel it.

117
00:13:06,280 --> 00:13:12,440
If you're truly mindful, you'll feel that you'll feel the impulse to just interact, but

118
00:13:12,440 --> 00:13:16,800
to do it mindfully, and you'll feel how much more natural that is.

119
00:13:16,800 --> 00:13:20,920
We're not stones, we're not just rocks that sit around all the time.

120
00:13:20,920 --> 00:13:24,840
The Buddha wasn't, the Arahans weren't.

121
00:13:24,840 --> 00:13:29,440
There are certainly times and places where you're not going to not want to engage, where

122
00:13:29,440 --> 00:13:33,640
engage will only cause stress and suffering.

123
00:13:33,640 --> 00:13:39,320
Certainly there are people who shouldn't be engaged with and situations, where people

124
00:13:39,320 --> 00:13:44,000
are very angry and you just shouldn't engage with them, but I would say most of the time

125
00:13:44,000 --> 00:13:49,520
that's not the case, most of the time there is a skillful way to engage.

126
00:13:49,520 --> 00:13:55,520
Now hey, if you're not proficient enough, you can always retreat and admit that you're

127
00:13:55,520 --> 00:14:05,000
not strong enough, but you certainly have to be aware that eventually the better way

128
00:14:05,000 --> 00:14:11,440
is to engage, that would argue.

129
00:14:11,440 --> 00:14:16,480
Look at that when you're feeling this reaction from other people, because you can see

130
00:14:16,480 --> 00:14:21,560
it disturbs your mind, and rightly it should, meditation isn't static, it's not just about

131
00:14:21,560 --> 00:14:28,560
sitting on your mat, it's about learning to be mindful, in all situations.

132
00:14:28,560 --> 00:14:35,680
Here's a question, see dash, I'm very sorry, but your English is just not good enough.

133
00:14:35,680 --> 00:14:40,320
I can't understand what you're asking, there's another case of it.

134
00:14:40,320 --> 00:14:48,960
Something to do with being angry is a type of addiction, okay, which lies in fall.

135
00:14:48,960 --> 00:14:58,680
Being angry towards anything is like addiction, it's habitual, it becomes, you reinforce

136
00:14:58,680 --> 00:15:05,920
it the more you do it, so addictive, not exactly, but habitual, based on ignorance and

137
00:15:05,920 --> 00:15:10,160
the fact that you're not being mindful, as your mind, for these habits can't stand because

138
00:15:10,160 --> 00:15:16,280
they're stressful, and mindfulness shows you that's hurting me, and so you'll slowly

139
00:15:16,280 --> 00:15:22,840
give it up.

140
00:15:22,840 --> 00:15:27,280
This is another, or who is this, us and art.

141
00:15:27,280 --> 00:15:37,600
Good everything be a consequence of overthinking, I think you're overthinking it, no, everything

142
00:15:37,600 --> 00:15:43,600
is not a consequence of overthinking, if you want to pin, if you want to find the culprit

143
00:15:43,600 --> 00:15:52,800
and pin it on one culprit, it would be ignorance, the problems all come from the fact that

144
00:15:52,800 --> 00:15:59,000
we just don't understand, and by being mindful, by observing objectively, you'll start

145
00:15:59,000 --> 00:16:01,000
to understand.

146
00:16:01,000 --> 00:16:10,480
Overthinking is just one product of not understanding.

147
00:16:10,480 --> 00:16:15,240
If vignana is the result of contact between the sense organs and the scent object, as

148
00:16:15,240 --> 00:16:24,240
it arise along with vignana, there's a very theoretical question, but you know, I'm going

149
00:16:24,240 --> 00:16:32,080
to answer them as well, if vignana is the result of contact, then why does it come before

150
00:16:32,080 --> 00:16:40,640
contact in the vignana?

151
00:16:40,640 --> 00:16:50,320
So the vignana in the Petiche Semabad is referring to the moment of conception, now that's

152
00:16:50,320 --> 00:17:00,200
the orthodox interpretation, but it also in other places is referring to the momentary

153
00:17:00,200 --> 00:17:07,200
consciousness throughout life, but there it says, vignana pachea namarupa namarupa

154
00:17:07,200 --> 00:17:12,840
pachea vignana, so they go in a circle, body and mind create vignana, vignana creates body

155
00:17:12,840 --> 00:17:17,880
and mind, and that's how it goes, as in fact as far back as you go even before conception,

156
00:17:17,880 --> 00:17:21,400
that's all that's going on.

157
00:17:21,400 --> 00:17:36,600
That being said, Petiche Semabad is not the be-all-end-all-of-so-word of conditionality,

158
00:17:36,600 --> 00:17:43,440
and it's not linear for sure, I mean what you're seeing is that there's something

159
00:17:43,440 --> 00:17:50,120
hinky here, it's not exactly A leads to B leads to C leads to D as it says, because

160
00:17:50,120 --> 00:17:55,520
a lot of these things come together, we say, vignana, pachea namarupa, well that's all

161
00:17:55,520 --> 00:18:03,920
to do with the process of rebirth, but in the present moment, sadayatana, pachea, pasu,

162
00:18:03,920 --> 00:18:12,800
so the sixth sense is lead to contact, but in fact the moment of experience is the arising

163
00:18:12,800 --> 00:18:28,400
of consciousness with a sense object as its object, so the experience of seeing based

164
00:18:28,400 --> 00:18:36,000
on the light touching the eye, but all of that arises in one moment, and it arises together,

165
00:18:36,000 --> 00:18:42,480
so contact is a description of what's going on there, it's not that contact arises

166
00:18:42,480 --> 00:18:48,280
in fact, it's a contact is what's going on, it's a part of the experience, and vignana

167
00:18:48,280 --> 00:18:55,440
is right there in there as well, vignana arises with consciousness, vignana is a jita-sika,

168
00:18:55,440 --> 00:19:02,800
and jita-sika are qualities of consciousness, so when you're asking, there's a rise

169
00:19:02,800 --> 00:19:06,920
along with vignana, well yes, vignana always arises with consciousness, it's a quality

170
00:19:06,920 --> 00:19:14,280
of consciousness, and contact is something that happens when consciousness arises, consciousness

171
00:19:14,280 --> 00:19:23,440
arises together with its object, and the description of that is contact or it's described

172
00:19:23,440 --> 00:19:33,080
as contact, so hopefully that gives you some insight, again it's complicated and there's

173
00:19:33,080 --> 00:19:38,080
a lot to talk about, if you want to really understand consciousness and vignana and all

174
00:19:38,080 --> 00:19:42,240
that, because it's not like you see something and there's one consciousness, there's up

175
00:19:42,240 --> 00:19:47,960
to 17 consciousnesses for one act of seeing, which is what we're going to be talking

176
00:19:47,960 --> 00:19:56,360
about in the Abidama, so if you're there you'll hopefully get a glimpse of it, are there

177
00:19:56,360 --> 00:20:01,840
Arahans who would still pursue their sexual desires, while an Arahan by definition has

178
00:20:01,840 --> 00:20:09,800
no sexual desire, so it's not a question I can answer, it's not a valid question because

179
00:20:09,800 --> 00:20:17,680
they don't have any, is there a difference between walking meditation and mindful walking,

180
00:20:17,680 --> 00:20:26,840
I mean there are words, they both contain the word walking, depends what you mean by walking

181
00:20:26,840 --> 00:20:34,600
meditation, what you mean by mindful walking, I mean practically speaking if you just

182
00:20:34,600 --> 00:20:46,680
want to one answer, well it's going to be two answers, technically they're from my point

183
00:20:46,680 --> 00:20:50,040
of view they're different because when we say walking meditation we're talking about

184
00:20:50,040 --> 00:21:00,080
a very specific technique, whereas walking mindfully is just walking walking, but practically

185
00:21:00,080 --> 00:21:05,160
speaking they're exactly the same, mindfulness has nothing to do with what you're doing,

186
00:21:05,160 --> 00:21:11,240
mindfulness is a quality, another jidasika, a quality of mind that arises based on causes

187
00:21:11,240 --> 00:21:16,680
and conditions, so the hope is that it arises more when you're actually doing formal

188
00:21:16,680 --> 00:21:23,760
walking meditation, but nonetheless it arises, it can arise no matter what you're doing,

189
00:21:23,760 --> 00:21:28,220
it can arise when you're doing mindful walking, it can arise when you're doing formal

190
00:21:28,220 --> 00:21:35,800
walking meditation, so from an ultimate perspective it's either just concepts, there's

191
00:21:35,800 --> 00:21:41,280
no such thing as walking meditation or mindful walking, there's only experiences that arise

192
00:21:41,280 --> 00:21:47,840
and sees, and if there's mindfulness while it's cultivating good things and leading towards

193
00:21:47,840 --> 00:21:55,520
enlightenment, when I meditate I sometimes get overwhelmed by the meditation, I do not

194
00:21:55,520 --> 00:22:01,640
know why but I do, because of this I am unable to sit and meditate for longer periods of time,

195
00:22:01,640 --> 00:22:07,040
I generally only can meditate for five to ten minutes, while trying to do walking meditation

196
00:22:07,040 --> 00:22:14,440
as well, you might find that that helps, but it's not easy, you have to understand that

197
00:22:14,440 --> 00:22:21,200
meditation is challenging, it's meant to be challenging, so work on what it is that's

198
00:22:21,200 --> 00:22:25,600
challenging you, the overwhelmed feeling trying to say to yourself overwhelmed if you're

199
00:22:25,600 --> 00:22:30,400
restless, say restless, if you're anxious, say anxious, if you want to do something else,

200
00:22:30,400 --> 00:22:38,280
say wanting wanting, if so many distractions in our lives these days that it's very easy

201
00:22:38,280 --> 00:22:45,400
to get overwhelmed, it's hard to sit still, so it's a great skill to learn something to

202
00:22:45,400 --> 00:22:52,080
work at, if you want you could try doing a meditation course with me, maybe that would

203
00:22:52,080 --> 00:23:01,920
help, we do online meditation courses, you can find on this site, I have a way to practice

204
00:23:01,920 --> 00:23:06,440
with a nun who is very versed in abidama and also meditation very strictly aligned to the

205
00:23:06,440 --> 00:23:13,600
abidama, when you recommend me to try it, sure, sounds good, I'm a little wary of trying

206
00:23:13,600 --> 00:23:18,880
to tie the abidama to meditation, it's certainly possible but I would say most of the time

207
00:23:18,880 --> 00:23:23,960
it's more likely that it just gets very intellectual, so just be wary of that, hopefully

208
00:23:23,960 --> 00:23:29,960
you've got a good teacher and it's all good, but I wouldn't pay too much attention

209
00:23:29,960 --> 00:23:41,360
to the abidama trying to be mindful, don't worry about learning too much, let us know

210
00:23:41,360 --> 00:23:50,360
how it goes, okay that's all the questions for tonight, so we'll end early, thank you

211
00:23:50,360 --> 00:24:18,360
all for coming out, have a good night.

