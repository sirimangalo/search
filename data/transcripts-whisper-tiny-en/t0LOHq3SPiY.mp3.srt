1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Damapada.

2
00:00:05,000 --> 00:00:13,000
Today we continue with verse 239, which reads as follows.

3
00:00:13,000 --> 00:00:40,000
Anupumbina made havi, token, token, kanekani, kama-ro-rajata-seva, nindameh-malamatana, which means gradually the wise.

4
00:00:40,000 --> 00:01:01,000
Little by little moment by moment, purifies themselves from defilement, from impurity, just as a smith does with silver.

5
00:01:01,000 --> 00:01:24,000
So the story behind this one is that there was a Brahmin, some Brahmin, in Sawati, who went walking out of the city one day and saw the monks on their way into the city for arms and they would gather in a spot outside of the city.

6
00:01:24,000 --> 00:01:37,000
Because when they were in the forest, they would wear their robes. No, they would perhaps even not wear the upper robe, just wear the lower robe in the forest and carry the upper robe.

7
00:01:37,000 --> 00:01:43,000
Maybe not even take their outer robe, maybe take it.

8
00:01:43,000 --> 00:01:52,000
And when they get to a certain spot near the city, they would take the bowl, which would be under their arm, like in a bag.

9
00:01:52,000 --> 00:02:05,000
Take the bowl out, put the shoes into the same bowl, wipe the shoes off first, put it in the same bag, and they would adjust their robes.

10
00:02:05,000 --> 00:02:11,000
They put them over both shoulders to cover themselves before they went into the city.

11
00:02:11,000 --> 00:02:18,000
And he saw that as they were doing this, they were standing in the grass.

12
00:02:18,000 --> 00:02:25,000
And because it was early morning, there was due on the grass, and he saw that the robes got all wet.

13
00:02:25,000 --> 00:02:33,000
And he thought to himself, well really, this is unfortunate and someone should cut that grass.

14
00:02:33,000 --> 00:02:44,000
And so the monks went on their way and later that day he came back with some sort of instrument and cut the grass.

15
00:02:44,000 --> 00:02:53,000
A later day he came back and saw the monks gathering there again, and he saw that it had gotten muddy in that area.

16
00:02:53,000 --> 00:03:03,000
And he saw that while now the grass wasn't so wetting their robes, it was even worse really, because they were getting muddy.

17
00:03:03,000 --> 00:03:07,000
The robes were getting muddy if they ever dragged along the ground.

18
00:03:07,000 --> 00:03:16,000
You see, when I was in Sri Lanka, the monks had this way of tucking the robe in between their legs as they put it on very clever.

19
00:03:16,000 --> 00:03:27,000
But you can't always be perfect and sometimes the robe would drag on the floor and then it would get muddy and drag on the ground.

20
00:03:27,000 --> 00:03:34,000
And they thought to himself, someone should really spread some sand in that area.

21
00:03:34,000 --> 00:03:38,000
The sand would do the trick, and so the monks went on their way.

22
00:03:38,000 --> 00:03:48,000
He came back later with some sand and a wheelbarrow maybe, or something, and spread sand in the area.

23
00:03:48,000 --> 00:03:57,000
Came back a later day and saw that while now the monks had this nice sandy spot to put on their robes, but then it rained.

24
00:03:57,000 --> 00:04:09,000
I don't know, I'm sorry. And it was very hot out, and he thought to himself, as they were putting on their robes, he saw that they were getting drenched and sweat.

25
00:04:09,000 --> 00:04:23,000
And he thought these monks are all sweaty, and someone should put up a pavilion, and he came back later and maybe with some help, he erected a pavilion in that area for the monks.

26
00:04:23,000 --> 00:04:28,000
And then it rained, and he saw that, oh these monks are getting all wet.

27
00:04:28,000 --> 00:04:37,000
So he built a hall in the area, he thought to himself, really what we need is an actual hall for these monks to gather.

28
00:04:37,000 --> 00:04:45,000
Maybe after they come back for alms, they could sit in the hall and eat their food.

29
00:04:45,000 --> 00:04:56,000
And so he built this hall, and thought to himself, I should do now, as I should have a celebration.

30
00:04:56,000 --> 00:05:03,000
I should invite all the monks together to consecrate the opening of this hall.

31
00:05:03,000 --> 00:05:13,000
And he went to the monastery and invited the monks, they all gathered together with the Buddha, and he fed them food.

32
00:05:13,000 --> 00:05:25,000
And then before the Buddha was going to teach at the end of the meal, he said to the Buddha, he related to the Buddha when he had done step by step.

33
00:05:25,000 --> 00:05:29,000
And the Buddha said, that's the way.

34
00:05:29,000 --> 00:05:32,000
He said, sad, it's good.

35
00:05:32,000 --> 00:05:40,000
And then he related this verse, he said, this is how the wise behave, little by little moment by moment.

36
00:05:40,000 --> 00:05:49,000
And they free themselves from malamatana, from their own, from the defilements that exist inside themselves.

37
00:05:49,000 --> 00:05:52,000
Empirity is mala.

38
00:05:52,000 --> 00:05:59,000
This is the mala we've got, so there's going to be a lot about this word mala which applies to things like metal.

39
00:05:59,000 --> 00:06:04,000
It means impurities in the metal like silver.

40
00:06:04,000 --> 00:06:11,000
But of course, the mind has its own impurities.

41
00:06:11,000 --> 00:06:18,000
So the one thing that strikes about the story that, apart from the teachings of the verse,

42
00:06:18,000 --> 00:06:22,000
is this initiative that the Brahman had.

43
00:06:22,000 --> 00:06:28,000
When you read the story, it strikes you that, here's a man out for a stroll and saw these monks,

44
00:06:28,000 --> 00:06:39,000
and what is thought? The conscientiousness, the thoughtfulness in his mind that led to this initiative,

45
00:06:39,000 --> 00:06:49,000
thinking to himself, I'll do something for these religious, spiritual individuals.

46
00:06:49,000 --> 00:06:59,000
I know often many people are reluctant to engage in modern times.

47
00:06:59,000 --> 00:07:06,000
There's, of course, a great proportion of the society is secular.

48
00:07:06,000 --> 00:07:12,000
And so there's sometimes an aversion to helping or getting involved with religion.

49
00:07:12,000 --> 00:07:21,000
But if you look at how beautiful this example, this act was of this Brahman,

50
00:07:21,000 --> 00:07:33,000
it wasn't helping some priests in the temple or it wasn't about luxury or organized religion.

51
00:07:33,000 --> 00:07:42,000
Here he was helping these vernunciants, people who had left the world who were living off of alms,

52
00:07:42,000 --> 00:07:52,000
scraps of food that were given out of kindness, almost like beggars, only without the begging.

53
00:07:52,000 --> 00:07:57,000
And it's not like he gave them anything luxurious or did anything luxurious, he just thought.

54
00:07:57,000 --> 00:08:04,000
Here were some people who needed and deserved his help.

55
00:08:04,000 --> 00:08:10,000
And what did he do? He did something very simple, but something that he was equipped to do.

56
00:08:10,000 --> 00:08:19,000
And just going out of his way to do something is a very good example, something that we really need in the world.

57
00:08:19,000 --> 00:08:21,000
Of course.

58
00:08:21,000 --> 00:08:27,000
It's a good example of, I think, the right way to live.

59
00:08:27,000 --> 00:08:34,000
An important part of what goodness and betterment in the world looks like,

60
00:08:34,000 --> 00:08:39,000
how the world becomes a better place.

61
00:08:39,000 --> 00:08:54,000
And so I think if we focus on the verse, and we get three lessons, I think, in what this looks like,

62
00:08:54,000 --> 00:09:07,000
this work of bettering the world, of bettering ourselves, and of this gradual cultivation of goodness.

63
00:09:07,000 --> 00:09:15,000
The first lesson is that goodness doesn't come in grand gestures.

64
00:09:15,000 --> 00:09:36,000
That true goodness really has much more to do with small gestures, small but frequent and habitual gestures.

65
00:09:36,000 --> 00:09:45,000
So this idea that we sometimes overlook small gestures.

66
00:09:45,000 --> 00:09:50,000
We overlook the mundane goodness.

67
00:09:50,000 --> 00:09:59,000
And jump ahead to some radical transformation, the idea that we should do something radical.

68
00:09:59,000 --> 00:10:14,000
And I'm thinking particularly in relation to spirituality, we often have examples of people who leave their lives behind and decide to become monks.

69
00:10:14,000 --> 00:10:27,000
For example, decide to radically transform their lives externally when they don't yet have the habit of transforming their lives internally.

70
00:10:27,000 --> 00:10:35,000
So often, we see examples of people who radically transform their lives and are unable to sustain it,

71
00:10:35,000 --> 00:10:42,000
because they haven't cultivated goodness on a momentary gradual level.

72
00:10:42,000 --> 00:10:48,000
And so they actually fail and become discouraged.

73
00:10:48,000 --> 00:11:02,000
The second lesson is that not only does it happen with small gestures generally, but it also happens gradually, not quickly.

74
00:11:02,000 --> 00:11:10,000
So again, often we have the perception that goodness should come or change should come immediately.

75
00:11:10,000 --> 00:11:15,000
A big step, which would be a radical shift.

76
00:11:15,000 --> 00:11:42,000
And this is most easily observed in the idea that somehow we should transform, which would be able to switch on or switch off some quality that we like or don't like.

77
00:11:42,000 --> 00:11:53,000
So in the practice of meditation, we see this when good or bad qualities we want to arise or we want to get rid of.

78
00:11:53,000 --> 00:11:56,000
We think somehow we should be able to control this.

79
00:11:56,000 --> 00:11:59,000
We should be able to switch them on or switch them off.

80
00:11:59,000 --> 00:12:11,000
We see this in people looking for results in their meditation practice when they would like for some observable change to appear.

81
00:12:11,000 --> 00:12:27,000
We often focus too much on results, too much on some visible change that should occur rather than focusing on the actual activity.

82
00:12:27,000 --> 00:12:44,000
This example of the story is mundane, and there's no question that this is outside of the path which leads to enlightenment.

83
00:12:44,000 --> 00:12:57,000
There is something profound about his attitude which lends itself very nicely, very well, to the practice and the path leading to enlightenment.

84
00:12:57,000 --> 00:13:12,000
It's an approach that is both small, focused on the ordinary activities.

85
00:13:12,000 --> 00:13:26,000
Some grand radical act, and also it's patient, so it was doing good when he saw some good could be done.

86
00:13:26,000 --> 00:13:29,000
I think those two qualities are an important lesson.

87
00:13:29,000 --> 00:13:56,000
Not only should we see that good comes little by little and moment by moment, but that seeing or looking at goodness from that perspective is in itself the goal, the proper state.

88
00:13:56,000 --> 00:14:09,000
If we think about mindfulness meditation practice, it's always focused on the present moment, which really means that this perspective of moments of goodness.

89
00:14:09,000 --> 00:14:23,000
The perspective, not just that some great goodness, some profound change in who we are should come through a moment, but that that is the change that we should seek.

90
00:14:23,000 --> 00:14:36,000
The change from trying to gain something in the future, to being good and focused on the good here in the present.

91
00:14:36,000 --> 00:14:57,000
If you think about what it would mean if we were all thoughtful and conscientious like this Brahmin, not just in helping monks who go on arms round, but in everything we do, when you see someone on the side of the road who begging for food, thoughtfulness to give them some food.

92
00:14:57,000 --> 00:15:14,000
When someone you know needs your help, overcoming your selfishness to help them to support them, taking the initiative to better yourself moment by moment, not to say,

93
00:15:14,000 --> 00:15:30,000
oh, every year I'm going to go and do a meditation course and then that will change who I am or even every day I'm going to do an hour of meditation or two hours of meditation and that will change me.

94
00:15:30,000 --> 00:15:58,000
But to think every moment I'm going to try to approach that moment in a pure and wholesome way, token, token, kani, kani, that the practice of momentary and ordinary goodness is the way that we want to attain.

95
00:15:58,000 --> 00:16:20,000
The way of life of an enlightened one, it fits very well with our teaching on meditation that we should focus on the moment, not the past, not the future.

96
00:16:20,000 --> 00:16:47,000
It helps us to understand what we mean by momentary concentration because one of the great concerns that new meditators have in this tradition is the lack of deep, powerful concentration that they might find in other meditation traditions.

97
00:16:47,000 --> 00:17:14,000
It is possible to cultivate even fairly quickly a sort of an artificial state of concentration that comes on strong but evaporates just as quickly because it's artificial, which is very different from the habitual focus and concentration that comes from cultivating moment by moment.

98
00:17:14,000 --> 00:17:19,000
Clarity of mind, purity of mind, goodness of mind.

99
00:17:19,000 --> 00:17:43,000
And so this perspective of doing things not in some big or grand way, not quickly or rushed, not reaching ahead for some goal but moment by moment patiently in a very mundane and ordinary way.

100
00:17:43,000 --> 00:17:46,000
It's saying to ourselves, pain, pain.

101
00:17:46,000 --> 00:17:51,000
And then not jumping ahead and say, what did it get me?

102
00:17:51,000 --> 00:18:01,000
But cultivating it as the goal, the goal is to be that sort of person who is thoughtful and conscientious and present.

103
00:18:01,000 --> 00:18:22,000
And this is a good sign that this Brahman expressed or exhibited. It's a good sign to see that he had this conscientiousness he was thoughtful, he was present when he saw something he could do, he did it.

104
00:18:22,000 --> 00:18:27,000
I think it's an example we could all learn from.

105
00:18:27,000 --> 00:18:35,000
So fairly simple teaching, something that has profound implications on our understanding of the practice.

106
00:18:35,000 --> 00:18:56,000
I think that we look at it as a little by little practice, a moment by moment practice and a practice for itself that our way of practicing our approach to the practice should be momentary.

107
00:18:56,000 --> 00:19:10,000
It should be all about the moment, not about some grand or radical shift or some goal that we might gain in the future.

108
00:19:10,000 --> 00:19:18,000
We do good for goodness sake. We do it because of how it changes us.

109
00:19:18,000 --> 00:19:45,000
If we're always looking for the goal or some grand transformation or immediate transformation, that in itself is antithetical to the path because of the greed involved, because of the control we're trying to exert, the change we're trying to create.

110
00:19:45,000 --> 00:19:55,000
They say enlightenment is all about change, but that's actually not. Enlightenment is about the practice.

111
00:19:55,000 --> 00:20:13,000
Enlightenment is change, you do change, but you change from seeking out goals to being the goal, to living the goal, to living as a goal,

112
00:20:13,000 --> 00:20:29,000
to being present, having a goal in the here and now, to changing our present, changing our moment, living by little moment by moment.

113
00:20:29,000 --> 00:20:45,000
That's the Dhammapada. Simple teaching, thank you all for listening.

