Good evening and welcome back to our study of the Damapada.
Tonight we continue on with verse number 82, which reads as follows.
Here as a
be what rahado just as a lake, gambiro that is deep. We pass ano that is clear and now we know
that is undisturbed. Just as a calm, clear, deep lake, just like a calm, clear, deep lake,
even the manis at one. So are those who have having listened to the dhamma, or so are the wise,
having listened to the dhamma. So to the wise, having listened to the dhamma, we proceed anti,
become tranquil, become calm. So the idea here is of a lake that becomes calm. A person who listens to
the dhamma becomes calm just like a calm, deep lake. Gambiro is, depth is used the same way it is
in English to talk about a person's depth, their wisdom. So a wise person will be deep or found.
This story was, this verse was told in regards to a story about a woman named God and her mother.
Now, Kana was married to a man and of course, she would have gone to live in her husband's house
and one time she went back to visit her parents and stayed there for quite some time.
Eventually, her husband called for her and said, Mr. was missing her and asked her to return.
And Kana was preparing to return, but her mother said, oh no, you can't go back empty-handed
after all this time. You should bring something back to give to your husband as a gift.
So the mother was kind thinking, that good intentions. And Kana herself was a kind person.
She had good intentions, so she was happy to do this. But Kana was also beyond that,
she was a generous and compassionate person. And so she waited and when the cake was ready,
sorry, it was a cake. So the mother said, wait, I'll bake you a cake and you can bring the cake back to
your husband. So Kana waited and when the cake was ready, she was preparing to leave and then
all of a sudden the monk showed up, a Buddhist monk showed up at her door. And so she had
nothing else she thought, yeah, I'll give this cake to the monk, what a good opportunity to give
arms. And so she gave him the cake, which would have been a real fine for him because
cake wouldn't be something that you would get, especially a fine cake here that this woman was
ready to give to her husband. So she gave him the cake and went back to her mother and said,
look, I gave the cake to a monk and you can you make me another one? And her mother was so fine
with that. Oh, that's great. Good for you. Meanwhile, this monk did something that he probably
shouldn't have done. He was an older fellow and a little bit excited about the cake.
And so he went back to the monastery and told the other old monks about it. They sit around
a bunch of old old guys. Oh, yeah, I got a cake from this house. You got a cake. So a second monk
heard this and he went to the house. And he got there just as the cake was baked and the woman
was getting ready to return to her husband. And she saw him coming and again felt obligated
to give another cake. So she gave the new cake to the second monk. And she was still happy,
but she was like, okay, well, it's good that we're giving to monks and really better get going.
So she asked her mother, please, mother. One more time. That's all just last time. One more cake
and the mother was like, oh, well, okay, very well. Baked her a third cake. Of course, the second
monk went back to the monastery and told his friends. And a third monk came and getting a little
bit frustrated at this point, but still not wanting to turn anyone away empty handed. She gave
a third cake to the third monk. Meanwhile, her husband was kind of frustrated at this point.
I think this all happened on one day. It would sort of make more sense. It happened one per day,
but I think it was the same day. And so her husband sent a messenger and said, okay, wait,
wait, wait, wait, wait. She's coming, but I just have to wait a little bit longer because
I'm baking the second cake and then baking the third cake. And they baked the third cake in the
fourth month, a third monk came, right? And we said that third monk came, baked the fourth cake,
baked the third cake away, baked the fourth cake, a fourth monk came. And at this point,
the husband got fed up and was told that he should just find another wife. And indeed,
that's what he did. He figured that his wife was never coming home. And so he went and found a new
wife. Anyway, just a story of one of those things that happens, but the upshot of it was that
Kanan was not happy. And she blamed the monks. That she felt sort of obligated to give food to these
monks. And you could say the monks were to blame the Buddha. In fact, used this
opportunity or this situation to create a rule. We have one of our rules in regards to this
story. And the rule is that you have to have to take a limited number of cakes if people are
offering cake. And I guess by extension, anything that's sort of a fine food, there's a limit on what
you can take. And when you get something like that, you have to share it whenever you get
with the other monks. No, it's not like I got some. So you go and get your own cake and everyone
gets an entire cake for themselves. But anyway, kind of previous prior to this being a
gentle and kind person became a mean and vicious person towards the monks anyway. And whenever
she saw a monk, she would revile them, call them beggars and greedy and mean and nasty.
So she would say mean things to them.
It doesn't say what she actually said. But she said they have ruined these monks have ruined my
married life because her husband went and got another wife. Her husband could sort of do what they
wanted in India. Indeed, that's what he did in the fickle sort of fellow.
The Buddha found out about this and he went to see Kana and he asked her, is it true that this is
happening? And she said, yes, they destroyed my marriage and he said, well,
did they steal the cakes from you? Or did you give them willingly? I gave them to them.
No, did they even ask you for cake? No, they didn't ask me for cake.
And so the Buddha just pointed out how silly she was being. And he said, in that case,
who was to blame here? Who was the one who was to blame? Well, it's true. I guess I am to blame.
And because of that, the Buddha, once she accepted that, the Buddha taught her the dhamma
and she actually sitting there was able to put it into practice and become a sotupana.
And then she asked us forgiveness for being ruined to the monks, even though you could argue that
the monks didn't weren't completely properly behaved. It's true. They didn't even beg
for anything. They just came to see whether they were giving. And indeed, she kept giving.
I was up to her, she could have stopped and said no. On the other hand, it was kind of a neat
thing to do. I couldn't say that well. Either way, it wasn't a very neat thing for her to
revile and revile other people. And so this is why the Buddha taught this first.
The monks were talking about it. The Buddha taught about how she had been a mouse in the past
life, but I don't know. I'm not going to get into the Jataka story. She reviled a bunch of cats
and the cats ended up dying. There's not much point to the Jataka story, except she was reviling cats.
But then the Buddha taught the Jataka story, taught the verse. It's that our mind was
turbid and became calm as a lake of still water. And then he taught the verse.
Jatapi, rahado, gampiru, vipasanu, anavilo, and so on. Dhammani, sutwana is interesting. Dhammani means
the dhammas. It's plural. It's neutral plural. It probably doesn't mean much to most people,
but it's an interesting use of the word dhammani. But it means the dhammas, so truths, good teachings.
So how does this relate to our practice? Well, immediately comes to mind for me as the
whole concept of meditation interviews that we've been conducting. We have these weekly
interviews, so people have been making appointments on our meditation page. And then we have a
Google Hangout one-on-one and help people out, give them advice. And so I've been doing this for
a few weeks down. It's worked really great. But before that, I've done this in person. And
you know, just having someone there to remind you and to clarify your doubts is
it's quite a stabilizer for your practice. I know it's easy to get caught up and we get all
worked up about turning mountains into making mountains out of molehills,
turning something very small into something very big because it's very hard for us to see our
faults. It's very hard for us to guide ourselves on something, especially on something that is new
to us, something unfamiliar. But hearing the dhamma is a great thing and it's amazing how in the
Buddhist time just hearing the dhammas often enough to enlighten someone and people often
question how this can be possible. But this is a part of it, really, that the dhamma is something
that quiets your mind and focuses your mind. Hearing it is keeping it fresh. It's bringing it,
bringing your attention back again and again to the truth, to the good things.
So if someone teaches about the force of Di Patana or someone teaches about insight,
someone teaches about the five aggregates, the nature of reality, at the same time the mind of the
listener becomes not only calm but becomes clear. So I was able to see things clearly.
Can we talk about how a meditator knows when they're sitting, that they're sitting,
then everyone listening and suddenly becomes aware of the fact that they're sitting
and becomes aware of the present moment. So the mind of the listeners brought back again and
again to these and other good things. On top of that, listening is kind of a special interaction
because the listening lies anyway, because you're forced to stay still. Those of you listening at
home, there's something lacking here. You're still listening to the dhamma potentially, but you have
the ability to get up or talk to other people in the room, make fun of me, check Facebook,
make comments and so on because we can't see or hear you. But when you're sitting in a,
there's something about sitting in front of a teacher, listening to them teach,
that makes it a meditation in itself. If you can't move, you can't even respond. You just have to
receive, receive, receive. As a result, becoming enlightened, when listening to the dhamma talk is a
very viable thing, becoming so dependent, there's no reason to doubt that this was possible,
that the Buddha can teach and people would become enlightened while they were sitting there.
Sometimes all they needed was a little nudge and the Buddha was very good at nudging people
in the right direction. So this is why we encourage listening to talks. That's why we encourage
going to see people who can teach you the dhamma. That's why we encourage undertaking courses
under a teacher as opposed to just reading about them. But even reading, you know, there's something
just about hearing the truth, having someone teach you good things or reading or learning about
good thing. So the Buddha said better than a thousand words that are meaningless is one word
when you hear it that brings you peace. So quite a simple verse, a simple little story.
One other thing you could say about this in regards to the idea of how it can help an angry
person is often we wonder how to deal with conflict and how do I deal when someone's accusing me
of things shouldn't I fight back when someone's attacking me shouldn't I defend myself?
How do I convince someone of my position and so on? I think the key is in this verse, this is
a good point as well that you teach that you say the truth. When people hear the truth, they become
calm. You don't have to argue, you don't have to persuade. You listen and if you're mindful
and your heart is pure and clear, when you speak it's the truth and just hearing the truth comes
people. Arguing doesn't solve things, defeating the other person doesn't create peace.
But hearing the truth creates peace. When you give people the truth that they need to hear,
they're dumb, they're good teachings. It brings peace. That's why mindfulness is so important in
a conversation, mindfulness is so important in our dealings with other people. It's difficult,
but it is something that we should always think about when we're approaching a conversation,
especially one that we know is going to be hostile or fraught with emotion. We should bring
mindfulness to the four, parimukang, satin, gupada, pitua. Bring mindfulness to the four, first off,
and then engage in our activities. Anyway, that's dumbapada verse 82. Thank you all for tuning in.
