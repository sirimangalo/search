1
00:00:00,000 --> 00:00:04,440
Hello and welcome back to our study of the Damapanda.

2
00:00:04,440 --> 00:00:11,460
Today we continue on with 1st number 129, which reads as follows.

3
00:00:11,460 --> 00:00:35,760
The

4
00:00:35,760 --> 00:00:49,160
and thus, for or at the rod, the stick, the weapon, all shake when presented with

5
00:00:49,160 --> 00:00:52,600
the weapon, when threatened with the weapon.

6
00:00:52,600 --> 00:01:03,120
Sambibhayantimachunah, matunah, all fear from death, all are afraid of death.

7
00:01:03,120 --> 00:01:12,760
Dhanun, up among it, comparing to oneself, making a comparison to oneself.

8
00:01:12,760 --> 00:01:24,440
Nahanayat nagata yay, one should not strike nor kill.

9
00:01:24,440 --> 00:01:32,020
This is the first verse of the Danda-wanga.

10
00:01:32,020 --> 00:01:38,460
Danda literally means stick, but it can also mean weapon.

11
00:01:38,460 --> 00:01:42,820
In English, they used to translate it as the rod.

12
00:01:42,820 --> 00:01:55,800
The rod is a term in English denoting a sort of a symbolic weapon.

13
00:01:55,800 --> 00:01:58,600
Just like Danda.

14
00:01:58,600 --> 00:02:00,460
And so we have a couple of verses.

15
00:02:00,460 --> 00:02:09,320
The next verse is going to be the same, almost the same as this one.

16
00:02:09,320 --> 00:02:16,360
But this was told in regards to a very, very simple story, but interesting than that.

17
00:02:16,360 --> 00:02:23,040
The Buddha was in Jaita-wana, and there was a group of monks in the time of the Buddha

18
00:02:23,040 --> 00:02:32,800
known as the group of six, Chambangia, Chambangia, Bangia, Chambangia, Chambangia, Chambangia,

19
00:02:32,800 --> 00:02:38,080
Chambangia, those of the group, those of the group of six.

20
00:02:38,080 --> 00:02:42,760
So they were known as the group of six, and they were infamous.

21
00:02:42,760 --> 00:02:52,120
We did every bad thing imaginable, every bad thing possible, short of breaking the rules

22
00:02:52,120 --> 00:02:53,440
that would get them expelled.

23
00:02:53,440 --> 00:02:56,560
We broke all the rules, all the other rules.

24
00:02:56,560 --> 00:03:01,920
Tried to find ways around rules, and so they're the subject of a lot of rules.

25
00:03:01,920 --> 00:03:10,760
So it may be, and someone might argue, that they were just escape goat.

26
00:03:10,760 --> 00:03:17,760
In the Vinay, it seems kind of like they just didn't know who actually did the thing that

27
00:03:17,760 --> 00:03:21,640
caused the rule, so they just made up stories about the band of six, because they're

28
00:03:21,640 --> 00:03:22,640
really everywhere.

29
00:03:22,640 --> 00:03:23,640
And they did almost everything.

30
00:03:23,640 --> 00:03:28,640
They did lots and lots of bad things, but they very well might have.

31
00:03:28,640 --> 00:03:29,640
Long time ago.

32
00:03:29,640 --> 00:03:30,640
It doesn't really matter.

33
00:03:30,640 --> 00:03:32,400
It's important.

34
00:03:32,400 --> 00:03:41,840
It's important is, well, it's important is they, in this story, there was another group

35
00:03:41,840 --> 00:03:43,800
called the group of 17.

36
00:03:43,800 --> 00:03:44,800
I don't know.

37
00:03:44,800 --> 00:03:45,960
There were lots of numbered groups.

38
00:03:45,960 --> 00:03:51,600
There was the group of seven, not the group of, again, anyway, the group of 17, so among

39
00:03:51,600 --> 00:03:55,960
of 17, a group of 17 monks.

40
00:03:55,960 --> 00:04:00,520
And they had prepared lodging for themselves.

41
00:04:00,520 --> 00:04:05,040
And then the group of six came up to them and said, give us this lodging, we're seniors

42
00:04:05,040 --> 00:04:06,040
here.

43
00:04:06,040 --> 00:04:11,480
The group of 17 said, what, we just set this up, we spend all this time preparing it.

44
00:04:11,480 --> 00:04:12,480
We can't give it over.

45
00:04:12,480 --> 00:04:14,960
We're not giving it over to you.

46
00:04:14,960 --> 00:04:23,760
And so the group of six picked up sticks and started beating them, so they say happened.

47
00:04:23,760 --> 00:04:31,080
The band of 17 afraid of death, according to the English, that they screamed at the top of

48
00:04:31,080 --> 00:04:32,080
their lungs.

49
00:04:32,080 --> 00:04:39,040
I'm not sure that the poly mirrors that.

50
00:04:39,040 --> 00:04:48,560
Moderna biata, tita, having given rise to the fear of death, maha, we are along a great

51
00:04:48,560 --> 00:04:49,560
scream.

52
00:04:49,560 --> 00:04:55,640
Yeah, it says nothing about lungs, maha, we are along, we are on wings, so they screamed

53
00:04:55,640 --> 00:05:02,040
a great scream, something like that.

54
00:05:02,040 --> 00:05:09,880
And the Buddha heard this sound, and he asked them, what was that?

55
00:05:09,880 --> 00:05:13,760
And the monks told them, when the monks told them, he made a precept against hitting.

56
00:05:13,760 --> 00:05:21,800
He said, monks, henceforth, doesn't say, but from this day on, from this point on, any

57
00:05:21,800 --> 00:05:31,200
monk who strikes another human being, his guilty of an event, so monks aren't allowed

58
00:05:31,200 --> 00:05:32,200
to hit.

59
00:05:32,200 --> 00:05:45,640
But there was later an exception, or a monk, and he didn't self-defense, anyway.

60
00:05:45,640 --> 00:05:50,400
Then he thought the morals of story, he said, he should never.

61
00:05:50,400 --> 00:05:59,160
How could you hit someone, do you not fear violence yourself against yourself, do you

62
00:05:59,160 --> 00:06:05,720
not suffer when others hit you?

63
00:06:05,720 --> 00:06:13,200
And this is an interesting teaching, because pragmatically speaking, practically a utilitarian

64
00:06:13,200 --> 00:06:19,800
was they, well, yeah, but hitting me hurts me, hitting them doesn't hurt me, right?

65
00:06:19,800 --> 00:06:22,960
It's different.

66
00:06:22,960 --> 00:06:27,080
And so this idea, well, it's the golden rule, right, that what they call the golden rule

67
00:06:27,080 --> 00:06:33,440
do unto others, as you would have them do unto you, which on the surface doesn't seem

68
00:06:33,440 --> 00:06:36,760
to make sense, right?

69
00:06:36,760 --> 00:06:41,960
Seems like if you can get away with murder, why not, right?

70
00:06:41,960 --> 00:06:45,840
So it seems like it's, I heard you, I don't suffer from that.

71
00:06:45,840 --> 00:06:50,520
And of course, the standard Buddha stands for that everyone should come to everyone's mind,

72
00:06:50,520 --> 00:06:54,080
is, of course, you do, it affects your mind.

73
00:06:54,080 --> 00:07:05,920
But it's curious that, in fact, there seems to be something about something of how hurting

74
00:07:05,920 --> 00:07:14,760
others or doing unto others, something that you don't want to happen to you, that that

75
00:07:14,760 --> 00:07:28,920
aspect of karma actually has a power, that when you engage in, whether you're the victim

76
00:07:28,920 --> 00:07:36,360
or the aggressor, when you engage in this activity of killing, it makes an implant in

77
00:07:36,360 --> 00:07:47,840
your mind, it's implants in your mind, you become a part of that, you incline in that

78
00:07:47,840 --> 00:07:48,840
direction.

79
00:07:48,840 --> 00:07:55,720
But I mean to say is, killing leads you to enter into the state of being killed.

80
00:07:55,720 --> 00:08:01,240
I mean, there are some obvious ways in which this happens, like how could it be that

81
00:08:01,240 --> 00:08:04,840
killing leads you to be killed, right?

82
00:08:04,840 --> 00:08:07,960
If you're killed, well then you get accustomed to killing, you don't get accustomed to

83
00:08:07,960 --> 00:08:10,240
being killed.

84
00:08:10,240 --> 00:08:17,200
But you incline in that direction, so the most obvious way is, well, if you go around killing

85
00:08:17,200 --> 00:08:20,760
people, you're probably going to make some people fairly angry, you're going to make

86
00:08:20,760 --> 00:08:21,760
some enemies.

87
00:08:21,760 --> 00:08:30,480
So on a practical level, absolutely killing inclines you more towards people seeking vengeance.

88
00:08:30,480 --> 00:08:32,840
That's one way of a gross way of understanding it.

89
00:08:32,840 --> 00:08:36,000
But I think it's more subtle than that.

90
00:08:36,000 --> 00:08:42,840
Another more subtle way than that is how killing affects your mind, obviously, and how

91
00:08:42,840 --> 00:08:52,920
it, killing, harming, unwholesome deeds, how they dull your mind, and they twist your mind,

92
00:08:52,920 --> 00:08:57,000
they make you less careful, they make you more rash, you're more likely to do things

93
00:08:57,000 --> 00:09:11,600
that harm yourself, because you're not careful, you're inflamed in the mind, but when

94
00:09:11,600 --> 00:09:17,960
someone doesn't want to die, someone doesn't want to be hurt, and you hurt them.

95
00:09:17,960 --> 00:09:25,840
The power that has on your mind, I think you have to recognize the strength of the impact

96
00:09:25,840 --> 00:09:29,960
that that has on your mind, watching someone suffer, right?

97
00:09:29,960 --> 00:09:40,200
You don't want to suffer, and you can feel it in them, this empathy, right?

98
00:09:40,200 --> 00:09:50,200
And this compassion, this goodness, it gets drained away from you, that's maybe one

99
00:09:50,200 --> 00:09:54,480
way of looking at it is, we're full of goodness.

100
00:09:54,480 --> 00:10:02,600
But it's like a fuel tank, and it gets drained, it gets sucked dry, and until you become

101
00:10:02,600 --> 00:10:14,480
psychopathic, and you become cold, you become cruel, you become accustomed to the suffering

102
00:10:14,480 --> 00:10:22,720
of others, but in general, to suffering, and at the impression that that has changes, the

103
00:10:22,720 --> 00:10:27,880
person who kills for the first time, whether it be an animal or a human, it's very difficult,

104
00:10:27,880 --> 00:10:33,320
but to continue killing, it gets subsequently easier.

105
00:10:33,320 --> 00:10:43,160
And if you can find a way to hate a person, like through propaganda or just in the heat

106
00:10:43,160 --> 00:10:53,040
of passion, it gets easier to kill, the more evil you have in your mind, the easier

107
00:10:53,040 --> 00:11:00,160
it is to kill, obviously, so the more evil a person you are, the more easy it is to kill.

108
00:11:00,160 --> 00:11:08,280
And so killing has this effect on us, it hardens you, and that's quite scary for when

109
00:11:08,280 --> 00:11:14,440
it comes time, when you have to give up the protection of this body, when you die and

110
00:11:14,440 --> 00:11:20,640
all that's left is the mind, they say the mind is a terrible thing to taste, right?

111
00:11:20,640 --> 00:11:27,880
It can be, if your mind is in a bad way.

112
00:11:27,880 --> 00:11:32,640
And so that's the third way, is that when you do pass away, whether it affects you in

113
00:11:32,640 --> 00:11:37,120
this life, this life we're protected from karma to a great extent, because we have this

114
00:11:37,120 --> 00:11:45,280
course, physical body, that inhibits the mind, it inhibits the power of the mind, the

115
00:11:45,280 --> 00:11:52,640
mind doesn't have much power when it's in the body, when the body ceases, the mind

116
00:11:52,640 --> 00:11:59,720
is very quick, and if the mind is not well trained, it's not certain where it will go,

117
00:11:59,720 --> 00:12:07,080
except that it will follow its habits, it will follow its inclinations, and if your mind

118
00:12:07,080 --> 00:12:12,920
isn't inflamed with the suffering of others, then that suffering will consume you and

119
00:12:12,920 --> 00:12:21,000
you die.

120
00:12:21,000 --> 00:12:26,640
It seems, I think to many people, the idea of karma seems kind of artificial, it seems

121
00:12:26,640 --> 00:12:35,680
like a belief that you impose upon the world without any evidence, but I think I don't

122
00:12:35,680 --> 00:12:40,560
think, but that is where meditation comes in, and so I'm always trying to relate these

123
00:12:40,560 --> 00:12:45,880
verses back to our practice, how this one relates, or this idea relates to our practice,

124
00:12:45,880 --> 00:12:49,440
through the practice is how you see this, you see that it's not theoretical, it's not

125
00:12:49,440 --> 00:12:51,360
a belief.

126
00:12:51,360 --> 00:12:56,120
If you've ever done bad things to others, if you've ever harmed others, if you've done

127
00:12:56,120 --> 00:13:06,840
significant amount of evil, things that cause suffering, you feel it when you start to meditate,

128
00:13:06,840 --> 00:13:11,360
it comes up and you didn't really think about it before, you didn't think it would be

129
00:13:11,360 --> 00:13:16,080
a problem, but when you meditate, you can't escape it.

130
00:13:16,080 --> 00:13:19,640
Years later, you'll have these things come up.

131
00:13:19,640 --> 00:13:25,560
Couldn't been when you were young, when years ago, the first thing people think about

132
00:13:25,560 --> 00:13:29,600
when they start to meditate, one of the big things during the course is meditators remember

133
00:13:29,600 --> 00:13:37,680
their parents, especially if they've had, if they've been unkind to their parents.

134
00:13:37,680 --> 00:13:41,080
And so you think, well, you know, respecting your parents, that's just a tool of religion

135
00:13:41,080 --> 00:13:47,080
to keep people in line, it's really not because I'm not making this up, meditators come

136
00:13:47,080 --> 00:13:52,120
and they will cry because of the love they have their parents and the love their parents

137
00:13:52,120 --> 00:13:59,680
have for them and sort of betraying that love and being unkind to their parents.

138
00:13:59,680 --> 00:14:08,000
Any thing, any killing or harming they've done to others comes back, no, there's a fine

139
00:14:08,000 --> 00:14:12,600
line there, you don't want people to feel you don't want to dwell in the guilt, so

140
00:14:12,600 --> 00:14:21,640
we don't, the guilt doesn't really serve much purpose, but it's important to understand

141
00:14:21,640 --> 00:14:30,760
and there's a great thing about meditations that it teaches you and you don't have to,

142
00:14:30,760 --> 00:14:37,760
you don't have to believe in the negative consequences of killing, you don't have to take

143
00:14:37,760 --> 00:14:43,040
on the precepts blindly, that's good if you do take on them blindly because it'll protect

144
00:14:43,040 --> 00:14:47,880
you from what's actually truly problematic, like if you're killing, you don't know it's bad,

145
00:14:47,880 --> 00:14:52,320
that's actually worse because you'll do it wholeheartedly and then when you finally, if

146
00:14:52,320 --> 00:14:57,600
and when you finally figure out that it's wrong and you're able to do away with ignorance,

147
00:14:57,600 --> 00:15:04,880
it'll be that much worse because you think how could I have been so foolish once you finally

148
00:15:04,880 --> 00:15:10,200
wake up to the truth and that comes from meditation, as you meditate, you see things

149
00:15:10,200 --> 00:15:16,880
like this, you think about you cringe, you cringe thinking about the harm you've inflicted

150
00:15:16,880 --> 00:15:29,840
on others, a meditator will be, will recoil at the thought of harming another being, naturally,

151
00:15:29,840 --> 00:15:34,920
they don't, it's not something that I'm a Buddhist, so I should feel bad about this, it's

152
00:15:34,920 --> 00:15:37,120
not nothing like that.

153
00:15:37,120 --> 00:15:45,000
This is the claim that morality is therefore intrinsic to nature, to human nature, to the nature

154
00:15:45,000 --> 00:15:51,240
of the mind of being, what we call morality is actually an aspect of reality.

155
00:15:51,240 --> 00:15:56,680
It's intrinsic, it's like physics, the laws of gravity and so on.

156
00:15:56,680 --> 00:16:04,800
There are lots of morality, ethics, goodness, and some people object that the idea of good

157
00:16:04,800 --> 00:16:11,000
and evil, but they're just words, good leads to happiness, evil leads to suffering, that's

158
00:16:11,000 --> 00:16:17,920
something, if you want to suffer, then do evil, it's fine, but there's another curious

159
00:16:17,920 --> 00:16:26,080
aspect of reality and that's that we don't want to suffer, why that is, I don't know,

160
00:16:26,080 --> 00:16:34,200
but we have certain things that inherently we shy away from, suffering, and there's other

161
00:16:34,200 --> 00:16:42,080
things that we inherently strive for, that happiness, and I think that's inherent in the

162
00:16:42,080 --> 00:16:44,600
universe as well.

163
00:16:44,600 --> 00:16:54,440
It's not like you can, it's unsustainable to seek out suffering, you can do it, you can

164
00:16:54,440 --> 00:17:00,040
force yourself, but the nature and intrinsic nature of the mind is to seek out happiness

165
00:17:00,040 --> 00:17:11,360
and to try and find a way of freedom from suffering, and that is where morality comes in,

166
00:17:11,360 --> 00:17:16,960
because morality is that which is coherent, you want happiness, so you do things that make

167
00:17:16,960 --> 00:17:28,520
you happy, immorality is incoherent, it's inconsistent, you want to be happy, and yet you do

168
00:17:28,520 --> 00:17:36,800
things that cause suffering, cause you suffering, that harm your mind, that's immorality.

169
00:17:36,800 --> 00:17:43,640
So this is a good quote, this is a good sort of folk, folk Buddhism quote of the Buddha,

170
00:17:43,640 --> 00:17:51,120
it's also very powerful, sabbait, santit, dandas, all tremble at the rod, sabbait,

171
00:17:51,120 --> 00:18:00,160
hi, indiventunu, all are fear, death.

172
00:18:00,160 --> 00:18:04,880
Others they give.

173
00:18:04,880 --> 00:18:11,760
or kill harm or kill. So that's the demo pilot for this evening. Thank you for

174
00:18:11,760 --> 00:18:22,200
tuning in wishing you a good practice.

