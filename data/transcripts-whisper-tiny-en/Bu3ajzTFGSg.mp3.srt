1
00:00:00,000 --> 00:00:03,000
How do we know that meditation is working for us?

2
00:00:03,000 --> 00:00:09,000
I know it's not a magic pill, but is there a point when we should see a difference in ourselves?

3
00:00:12,000 --> 00:00:14,000
It's a bit difficult.

4
00:00:14,000 --> 00:00:16,000
It's difficult for two reasons.

5
00:00:16,000 --> 00:00:19,000
One, meditation works very slowly.

6
00:00:20,000 --> 00:00:26,000
And two, it's very difficult to see a change in yourself.

7
00:00:27,000 --> 00:00:28,000
Right?

8
00:00:28,000 --> 00:00:33,000
It's very difficult to see what's wrong with you to see your problems.

9
00:00:33,000 --> 00:00:35,000
It's very easy to see the problems that other people have.

10
00:00:35,000 --> 00:00:37,000
And their problems tend to be very easy to solve.

11
00:00:37,000 --> 00:00:38,000
Right?

12
00:00:38,000 --> 00:00:41,000
And to solve someone else's problems is very easy.

13
00:00:41,000 --> 00:00:45,000
Solving your own problems is quite difficult, because you don't really see them correctly.

14
00:00:45,000 --> 00:00:46,000
You think you do.

15
00:00:46,000 --> 00:00:51,000
You think you see, I have this problem, I have that problem, but it's quite superficial.

16
00:00:51,000 --> 00:00:53,000
You're not able to see that actually.

17
00:00:53,000 --> 00:00:56,000
Thinking like that is a part of the problem.

18
00:00:56,000 --> 00:01:03,000
What you should see is a change in understanding.

19
00:01:03,000 --> 00:01:06,000
It's the first most important thing to gain.

20
00:01:06,000 --> 00:01:09,000
It's really the final thing to gain as well.

21
00:01:09,000 --> 00:01:12,000
The key is that your understanding of things is changing.

22
00:01:12,000 --> 00:01:17,000
Not that you feel more happy or that you feel more at peace or so on,

23
00:01:17,000 --> 00:01:19,000
but that you have more understanding.

24
00:01:19,000 --> 00:01:26,000
And as a result, and you have a knowledge that as a result,

25
00:01:26,000 --> 00:01:31,000
you're more at peace, because it's very hard to convince yourself

26
00:01:31,000 --> 00:01:34,000
that you feel more peaceful, because it's impermanent.

27
00:01:34,000 --> 00:01:38,000
Sometimes you feel pain in a lot of pain, even as a meditator.

28
00:01:38,000 --> 00:01:43,000
Sometimes you feel quite stressed, even as a meditator.

29
00:01:43,000 --> 00:01:51,000
But when you think about the understanding that you've gained,

30
00:01:51,000 --> 00:01:59,000
then you can convince yourself that actually it's true that overall you must be.

31
00:01:59,000 --> 00:02:03,000
And you definitely are more peaceful, even though it's hard to tell for yourself,

32
00:02:03,000 --> 00:02:07,000
because you can think, gee, yesterday I was more peaceful today.

33
00:02:07,000 --> 00:02:09,000
I'm not peaceful or last week.

34
00:02:09,000 --> 00:02:15,000
Or a month ago, I was not peaceful, now I am peaceful, but you don't really have this kind of comparison.

35
00:02:15,000 --> 00:02:21,000
It's not a real comparison, so you can't really confirm that you're more peaceful than you were before.

36
00:02:21,000 --> 00:02:24,000
You're more happy than you were before.

37
00:02:24,000 --> 00:02:29,000
But you can convince yourself that it must be the case, because you know the nature of wisdom,

38
00:02:29,000 --> 00:02:31,000
the nature of the understanding.

39
00:02:31,000 --> 00:02:34,000
The understandings that you've gained have untied knots.

40
00:02:34,000 --> 00:02:37,000
They've changed the way you look at things.

41
00:02:37,000 --> 00:02:41,000
They've straightened and they've simplified your understanding of reality.

42
00:02:41,000 --> 00:02:45,000
And because of that simplification, there's less stress, there's less complication,

43
00:02:45,000 --> 00:02:47,000
less busyness and so on.

44
00:02:47,000 --> 00:02:50,000
So look for that, look for the understanding,

45
00:02:50,000 --> 00:02:55,000
the simplification of how you look at things, rather than making a big deal out of things,

46
00:02:55,000 --> 00:02:59,000
just the ability to just see things as they are naturally.

47
00:02:59,000 --> 00:03:09,000
And the ability to let go into not stress over things as much.

