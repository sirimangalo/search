1
00:00:00,000 --> 00:00:06,260
Hello, I'm Cresta Volunteer for Seramongaloe International.

2
00:00:06,260 --> 00:00:10,340
Across the world, organizations are making adjustments to aid in preventing the spread of

3
00:00:10,340 --> 00:00:11,940
the novel coronavirus.

4
00:00:11,940 --> 00:00:14,660
Seramongaloe International is no different.

5
00:00:14,660 --> 00:00:18,340
The Board of Directors is decided for the safety of everyone and in keeping with the best

6
00:00:18,340 --> 00:00:23,020
advice available to us to cancel all meditation courses at our center in Ontario, Canada,

7
00:00:23,020 --> 00:00:24,620
for the time being.

8
00:00:24,620 --> 00:00:28,460
Our tentative date for reopening is May 16th, subject to new information and ask things

9
00:00:28,460 --> 00:00:29,460
evolve.

10
00:00:29,460 --> 00:00:34,380
We recognize that this is inconvenient to many who have already been approved for courses

11
00:00:34,380 --> 00:00:37,900
and it's unfortunate to pass up on this opportunity to share the demo.

12
00:00:37,900 --> 00:00:42,260
However, we believe that it is the most responsible choice not to contribute to the spread

13
00:00:42,260 --> 00:00:44,060
of the virus.

14
00:00:44,060 --> 00:00:47,780
We urge any who have already made travel plans to reverse them.

15
00:00:47,780 --> 00:00:52,060
Our volunteers will happily aid you in rescheduling your in-person course for a future date when

16
00:00:52,060 --> 00:00:54,380
conditions have improved.

17
00:00:54,380 --> 00:00:58,660
Seramongaloe offers online meditation courses with venerable youth to demo scheduled through

18
00:00:58,660 --> 00:01:04,820
the Meditation Plus application at meditation.seramongaloe.org slash schedule with Google

19
00:01:04,820 --> 00:01:08,460
Hangouts providing live communication between student and teacher.

20
00:01:08,460 --> 00:01:12,100
Only headphones are speakers and a microphone as well as a modest internet connection

21
00:01:12,100 --> 00:01:15,340
or required to participate in this way.

22
00:01:15,340 --> 00:01:18,940
Any person who has been scheduled to visit the center itself may absolutely instead participate

23
00:01:18,940 --> 00:01:19,940
in online course.

24
00:01:19,940 --> 00:01:24,540
For more information, please visit seramongaloe.org slash meditation.

25
00:01:24,540 --> 00:01:28,500
We appreciate your understanding and hope that your practice continues and provides

26
00:01:28,500 --> 00:01:31,060
much benefit to you and to the whole world.

27
00:01:31,060 --> 00:02:00,140
May you be happy.

