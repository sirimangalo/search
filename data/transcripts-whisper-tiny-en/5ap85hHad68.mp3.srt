1
00:00:00,000 --> 00:00:29,080
Okay, good evening everyone, welcome to evening dhamma, I've forgotten to change the

2
00:00:29,080 --> 00:00:36,520
kind of about the YouTube thing. I can still change it after the fact. Tonight we

3
00:00:36,520 --> 00:00:49,920
are studying the Maha dukka khandasura, Majimani kaya number 13, if you're interested, and you

4
00:00:49,920 --> 00:00:55,960
should be interested, it's a good to read, if you're picky and choosy, if you're not

5
00:00:55,960 --> 00:01:02,600
going to read the whole of the Majimani kaya, which of course you should all do. But if you

6
00:01:02,600 --> 00:01:07,080
don't have the time where you want to start somewhere, I'm picking out the ones that I think

7
00:01:07,080 --> 00:01:16,840
are especially relevant to a broad audience. We've skipped the Satipatanasutas, don't

8
00:01:16,840 --> 00:01:21,000
skip that one, but we skipped it because they've already done recently a set of talks

9
00:01:21,000 --> 00:01:30,480
on it. Besides that one, I feel like I'm hitting the highlights. Maha dukka khandasura, it's a

10
00:01:30,480 --> 00:01:39,960
good, good title. The discourse on the great, no, no, he doesn't translate. The discourse

11
00:01:39,960 --> 00:01:48,480
on the greater mass of suffering or the great discourse, he's right, the greater discourse

12
00:01:48,480 --> 00:01:54,240
on the mass of suffering, I guess, then I think 14 is the juladukka khandasura does it? Is there

13
00:01:54,240 --> 00:02:02,320
one the other? Right, so this is the big one. The next one's going to be more brief, sounds

14
00:02:02,320 --> 00:02:14,000
quite ominous. This is the big talk on the mass of suffering dukka khanda, the mass of suffering.

15
00:02:14,000 --> 00:02:26,680
A cheery subject, right? So Buddhism gets a wrap for being obsessed with suffering. It's

16
00:02:26,680 --> 00:02:37,600
quite unique in that way from a European or even an East Asian perspective, I think. Maybe

17
00:02:37,600 --> 00:02:49,280
not from an Indian perspective. So the Buddha starts. A bunch of monks went into sabbatif

18
00:02:49,280 --> 00:02:57,000
for alms and that was too early, so they decided to go and visit the wonders of other

19
00:02:57,000 --> 00:03:05,640
sects, of other people who belong to other dispensations, which is quite remarkable, really

20
00:03:05,640 --> 00:03:13,960
sort of a multi, what we would be equivalent of a multi-faith talk or a multi-interreligious

21
00:03:13,960 --> 00:03:21,080
dialogue. There was a big thing because in India it wasn't so much that there were the

22
00:03:21,080 --> 00:03:30,760
big R religions or the big name religions. There were teachers and they all taught different

23
00:03:30,760 --> 00:03:41,360
things and so I think Buddhism stands out along with Zionism as sort of it's own being

24
00:03:41,360 --> 00:03:46,800
its own microcosm, but the people who would come to the Buddha and the atmosphere in

25
00:03:46,800 --> 00:03:52,840
India was kind of going to all sorts of teachers, right? That's what we get out of the texts

26
00:03:52,840 --> 00:04:00,200
that we have. There were lots of different teachers teaching various things and so going

27
00:04:00,200 --> 00:04:08,720
from one teacher to another and in this case talking with other people from other religions

28
00:04:08,720 --> 00:04:17,840
wasn't so much like an interreligious dialogue as it was talking to fellow seekers on the

29
00:04:17,840 --> 00:04:26,720
path, I think. Of course there was a sense that these guys had it all wrong because these

30
00:04:26,720 --> 00:04:31,080
were because we're talking about the monks, but I don't think it was as strong as saying

31
00:04:31,080 --> 00:04:38,240
hey I'm Buddhist and you're Hindu or something like that. No, it was like well we follow

32
00:04:38,240 --> 00:04:45,840
this teacher, you follow that teacher. Let's go and see what they say. Let's go and learn

33
00:04:45,840 --> 00:04:50,960
another perspective. I mean it kind of opens up the door for Buddhists to study other

34
00:04:50,960 --> 00:04:56,240
religions and I think really everyone to some extent should study other religions not

35
00:04:56,240 --> 00:05:03,400
because Buddhism is lacking anything or that because you might doubt Buddhism and think

36
00:05:03,400 --> 00:05:09,360
hey maybe that religion is better, but for people who are actually committed to Buddhism

37
00:05:09,360 --> 00:05:16,600
it's quite useful to study other religions. It helps you understand your own religion. It

38
00:05:16,600 --> 00:05:29,080
helps test you, your knowledge, your confidence, your wisdom, your ability to discern

39
00:05:29,080 --> 00:05:35,120
good from bad right from wrong. It broadens your horizons and helps you see it's not so

40
00:05:35,120 --> 00:05:41,880
much about Buddhist truth as it is about truth and wherever truth is found I can consider

41
00:05:41,880 --> 00:05:48,360
it truth. So anyway these monks went to see these other recklessness and the recklessness

42
00:05:48,360 --> 00:05:54,440
said some say something that's quite interesting. They say the Buddha, well go to man this

43
00:05:54,440 --> 00:06:01,760
guy go to man who we know as the Buddha. He describes the full understanding of sensual pleasures

44
00:06:01,760 --> 00:06:17,960
and material form and feeling. And so do we. And the word is bhanya bhaiti which I don't

45
00:06:17,960 --> 00:06:26,280
know really means describe. Let's see. He causes other people to know. So he proclaims

46
00:06:26,280 --> 00:06:43,640
or he pushes, this is what he has to make established. So he sets up this as a principle.

47
00:06:43,640 --> 00:06:49,800
The full understanding of sensual pleasures, the full understanding of material form and

48
00:06:49,800 --> 00:06:57,720
the full understanding of feeling. And we do, we do too. They say we do this very same thing.

49
00:06:57,720 --> 00:07:02,840
So what is the distinction? What is the difference between our teaching and the Buddha

50
00:07:02,840 --> 00:07:11,560
teaching? The Gautama's teaching. And the monks neither approved or disapproved of this.

51
00:07:11,560 --> 00:07:15,800
They didn't really know what to say. It seems. And so they went back and told the Buddha

52
00:07:15,800 --> 00:07:23,240
and the Buddha to explain to them what the response should be. Before we get into the response

53
00:07:23,240 --> 00:07:31,400
and that's of course the main body of the suta, I just want to stop and appreciate the

54
00:07:31,400 --> 00:07:39,880
miyu that was clearly surrounding or seems to be surrounding the Buddha. We only have these

55
00:07:39,880 --> 00:07:47,640
sources, Buddhist sources, giant sources, the Upanishads, that sort of thing to paint a picture

56
00:07:47,640 --> 00:07:55,240
of what was actually going on. But assuming this is what was going on. And regardless, I mean

57
00:07:55,240 --> 00:08:04,520
looking at the text that we have extent, this kind of question or this kind of debate is I think

58
00:08:04,520 --> 00:08:18,040
relatively unique. And I want to stress this because it's easy to make the mistake that

59
00:08:19,000 --> 00:08:31,320
religions are basically the same. Now we don't want to harp on how our religion is superior

60
00:08:31,320 --> 00:08:42,040
or it's the only religion. But to be really clear, there are very different types of what we

61
00:08:42,040 --> 00:08:49,080
might call religion in the world. And you might as an anthropologist look at similar aspects.

62
00:08:49,080 --> 00:08:54,360
An anthropology does I think a really good job at messing things up by focusing very much

63
00:08:54,360 --> 00:09:00,600
on what ordinary people do. And well that's quite interesting. It kind of messes up, messes things

64
00:09:00,600 --> 00:09:07,000
up for religion because religion isn't really about what ordinary people do. Religion tends to be

65
00:09:07,000 --> 00:09:15,000
quite difficult and require a lot of devotion. And so most people who follow a religion aren't

66
00:09:15,000 --> 00:09:20,920
actually very good practitioners of that religion. So if you look at the candles that people

67
00:09:20,920 --> 00:09:25,240
light and the statues that people worship, you're not looking at Buddhism obviously, right?

68
00:09:26,120 --> 00:09:29,320
And the same can I think be said of most religions. And when you look at it you say,

69
00:09:29,320 --> 00:09:33,640
well they're all the same because Christianity lights candles, Buddhists like candles,

70
00:09:33,640 --> 00:09:40,760
Christians do chanting, Buddhists do chanting. But as a Buddhist monk when people compare

71
00:09:40,760 --> 00:09:49,320
Christianity and Buddhism, it's quite hard to maintain composure. Well, it twings something inside

72
00:09:49,320 --> 00:09:58,760
because it's not nearly the truth. Who in the world in the time of the Buddha was asking questions

73
00:09:58,760 --> 00:10:09,080
like this? Certainly Jesus Christ wasn't. And we can go through the list, was let's say

74
00:10:09,080 --> 00:10:20,200
loud to, was he talking about these things? Not really. What about Mohammed came later but

75
00:10:20,200 --> 00:10:29,240
okay, look at Mohammed. So we have this phenomenon of religion that seems to capture people.

76
00:10:30,680 --> 00:10:35,800
And it has this essence no matter what kind of religion it is. But that has nothing to do with

77
00:10:35,800 --> 00:10:44,680
the actual content of the religion. And I guess what I really want to stress is how I think

78
00:10:44,680 --> 00:10:51,000
special from a Buddhist perspective, this time in India was that people were looking at the world

79
00:10:51,000 --> 00:11:02,200
in a very specific way. And I mean just to say this, to put your focus not on God or on

80
00:11:03,480 --> 00:11:12,600
heaven or on even goodness and evil, but the full understanding of a desire,

81
00:11:12,600 --> 00:11:19,480
material form and feeling. I mean it's quite foreign, it should be quite foreign to people who

82
00:11:19,480 --> 00:11:24,360
are familiar with other religions because this isn't the sort of thing that you would expect

83
00:11:25,160 --> 00:11:31,400
as a religious practice. It's the kind of thing that we see now in various sorts of therapy

84
00:11:32,120 --> 00:11:38,360
and even some philosophy. I mean I think the closest might, not the closest, but I think it's

85
00:11:38,360 --> 00:11:42,200
interesting to look at what the Greeks were doing. But even the Greeks, the Greeks and the

86
00:11:42,200 --> 00:11:49,240
Romans, I mean yeah, they talked some about virtue and about knowing yourself and that kind of

87
00:11:49,240 --> 00:11:56,840
thing. But still quite a different feel to it. And there was none of this systematic, not as far

88
00:11:56,840 --> 00:12:02,200
as I've studied. I'm sure people are going to tell me, you know, things like, wow there were

89
00:12:02,200 --> 00:12:10,760
Christian mystics and there were Muslim mystics and there are the Sikhs and of course there's

90
00:12:10,760 --> 00:12:19,800
East Asian mystics and so on. Then there's the Aboriginal shamans and so on. And it's not to say

91
00:12:19,800 --> 00:12:28,120
that I mean anyone can of course practice in this sort of spiritual way of, well, this way of studying

92
00:12:28,120 --> 00:12:38,280
the nature of the mind. But here we're talking about, we're talking about religious leaders

93
00:12:38,280 --> 00:12:51,560
promoting this as their religion. Not to harp too much on it, but I think it's quite interesting

94
00:12:51,560 --> 00:12:56,120
that this is important to understand the sort of religion that we're talking about when we talk

95
00:12:56,120 --> 00:13:05,560
about Buddhism and the religions of India. Nonetheless the Buddha is not having it. He says that

96
00:13:05,560 --> 00:13:11,960
well these guys are, they talk a good game, but you should ask them three questions.

97
00:13:13,560 --> 00:13:19,000
And these three things are I think quite interesting and pertinent for us as meditators. The first

98
00:13:19,000 --> 00:13:30,040
one is what is the gratification? What is the danger? And what is the escape? The gratification,

99
00:13:30,040 --> 00:13:36,200
the danger, and the escape in regards to these three things. So what is the, what is the gratification

100
00:13:36,200 --> 00:13:47,880
of sensual desire or sensual pleasure? What is the sensual sensuality really? What is the danger?

101
00:13:47,880 --> 00:13:56,840
And what is the escape from sensuality? What is the gratification of form? What is the danger in

102
00:13:56,840 --> 00:14:07,720
form? And what is the escape? And the same with feelings. And he says they won't be able to answer.

103
00:14:08,600 --> 00:14:18,760
And he says let me teach you this framework. What is the gratification, the danger, and the escape?

104
00:14:18,760 --> 00:14:27,080
So first let's break this down. When we talk about these three aspects of experience,

105
00:14:27,080 --> 00:14:36,280
which is what they really are, we're focusing in on the process or the aspects of the process

106
00:14:36,280 --> 00:14:44,680
of craving, of clinging, of desire, of suffering, really. Remember this is the discourse about

107
00:14:44,680 --> 00:14:57,000
suffering. And these three things are understood. And again, not just by the Buddha, but in this

108
00:14:57,000 --> 00:15:02,680
sort of menu in this environment that the Buddha found himself, and this was the sort of thing that

109
00:15:02,680 --> 00:15:08,920
remarkably, a lot of people were talking about it seems. That craving was the cause of suffering.

110
00:15:08,920 --> 00:15:20,680
That it was this thirst, this desire, this ambition, striving for things that couldn't satisfy us.

111
00:15:29,960 --> 00:15:34,600
And I think it's quite remarkable that this was the focus, that the focus wasn't on

112
00:15:34,600 --> 00:15:50,840
some sort of nebulous or theoretical goal. It was on a very real and visceral problem or way of

113
00:15:50,840 --> 00:15:59,320
looking at the problem. Because any other goal that you might have or ambition or belief,

114
00:15:59,320 --> 00:16:05,400
it's all going to be for the purpose of finding happiness or peace, which is of course

115
00:16:06,520 --> 00:16:08,440
the escape from suffering.

116
00:16:16,600 --> 00:16:23,560
So one of the three, the three are sensuality, sensuality refers to what goes through this,

117
00:16:23,560 --> 00:16:37,160
this sensuality is our desire, and the desire for science, the desire for sounds, the desire

118
00:16:37,160 --> 00:16:46,360
for tastes and smells. It's the actual liking of things, having partiality, having

119
00:16:46,360 --> 00:16:54,680
having favorites, something that you like, something that you dislike.

120
00:17:03,560 --> 00:17:13,000
Form is the object of the liking, so form is a beautiful body or a beautiful flower or it's

121
00:17:13,000 --> 00:17:22,600
a beautiful sound or it's a nice smell. It's even comfortable, soft touches.

122
00:17:24,760 --> 00:17:31,400
It's the physical aspect. I kind of mosquito.

123
00:17:31,400 --> 00:17:39,400
Well, in the last, if it doesn't bite me and give me West Nile virus, it'll be okay.

124
00:17:47,000 --> 00:17:54,920
And the third one feeling, feeling is the pleasure or the pleasurable feeling.

125
00:17:54,920 --> 00:17:58,600
And if it sounds like these are very much related, I mean they really are. What the

126
00:17:58,600 --> 00:18:04,520
Buddha is doing is he's pulling apart a really a single process, the process of the experience

127
00:18:04,520 --> 00:18:10,360
of craving. There are three at least three aspects. I would add a fourth in here and that's the

128
00:18:10,360 --> 00:18:16,680
thoughts. It's not really a part of the process, but it's important. What you think about things

129
00:18:16,680 --> 00:18:22,920
and as a result, your views about things and that really has an impact on the process, but it's

130
00:18:22,920 --> 00:18:30,520
not directly a part. So it's not wrong that it was not included here. But these three definitely

131
00:18:30,520 --> 00:18:35,800
are important. If anyone's concerned about addiction, people ask questions about addiction and we

132
00:18:35,800 --> 00:18:41,400
all have addictions. I mean, this is the practice that freezes from the me specifically

133
00:18:42,120 --> 00:18:47,320
is how we attack an addiction based on our practice around these three things.

134
00:18:47,320 --> 00:18:53,320
So what's the gratification of sensual pleasures?

135
00:18:59,320 --> 00:19:05,960
And the Buddha says well when you like things, when you want things, you get what you want.

136
00:19:05,960 --> 00:19:17,240
That's the gratification. You want pizza, you get pizza. You want music, you turn on

137
00:19:17,240 --> 00:19:25,400
music, you want nice smells, you wear perfume or cologne, news air freshener,

138
00:19:26,520 --> 00:19:32,360
all of these things. You can get what you want. That's what we do. That's gratification.

139
00:19:32,360 --> 00:19:47,800
To point out something that's actually quite important about the gratification, it's that

140
00:19:49,160 --> 00:19:57,960
simply acknowledging the gratification I think is groundbreaking. Often our approach towards

141
00:19:57,960 --> 00:20:07,480
addiction is about the cure. It's an ingrained conceiving of the addiction as something bad.

142
00:20:08,760 --> 00:20:15,080
Which, you know, it is bad, but the conceiving of it as something bad is a negative. It's kind

143
00:20:15,080 --> 00:20:23,160
of a fear or an aversion to it. Acknowledging the gratification is to acknowledge what's

144
00:20:23,160 --> 00:20:30,440
deeply rooted in the mind, and that's the belief that this is a good thing. And it's like

145
00:20:30,440 --> 00:20:35,320
therapy, you have to acknowledge. This is why a big part of our practice is simply acknowledging

146
00:20:35,320 --> 00:20:42,680
all the bad stuff and acknowledging it objectively without judgment. You don't have to say hey,

147
00:20:42,680 --> 00:20:50,760
this is bad. How can I get rid of this? And this is a mistake that people make, of course,

148
00:20:50,760 --> 00:20:59,080
is to hate themselves and hate their addictions and really cultivate a great amount of aversion

149
00:20:59,080 --> 00:21:04,840
towards something that they don't even believe because deeply in the mind, they believe that

150
00:21:04,840 --> 00:21:10,280
the things that they're addicted to are good are going to make them happy. It's not just physical.

151
00:21:10,280 --> 00:21:21,880
The addiction is based on a deep, rooted conception that something is stable, satisfying,

152
00:21:21,880 --> 00:21:30,200
or controlled by all the three. And so our first part of our practice is really

153
00:21:32,520 --> 00:21:38,600
about being mindful about the gratification, about the good side of these three things.

154
00:21:38,600 --> 00:21:47,320
The danger, though, is not something that we should understand intellectually. It's something that

155
00:21:47,320 --> 00:21:55,560
we start to see as we observe and we say, okay, so objectively we will look at this. Yes, there

156
00:21:55,560 --> 00:22:05,960
is this gratification. And you don't have to hate it because the theory is that it's not actually

157
00:22:05,960 --> 00:22:12,440
gratifying. The gratification is not actually satisfying, right? The result is not actually

158
00:22:12,440 --> 00:22:17,480
satisfactory. And this is because there is a danger that you will start to see.

159
00:22:19,000 --> 00:22:23,960
So he goes on and it's really worth reading. I won't go into great detail, but he talks at some

160
00:22:23,960 --> 00:22:30,600
length about the dangers. If you want all these things, then you've got to work for them.

161
00:22:31,160 --> 00:22:34,760
And just working for them. You've got to face mosquitoes to go off and work.

162
00:22:34,760 --> 00:22:39,880
You've got to face the heat and the cold. You've got to face a boss who yells at you.

163
00:22:43,320 --> 00:22:48,840
And then if you don't succeed, if your work isn't fruitful, there's always a great fear,

164
00:22:48,840 --> 00:22:53,640
especially with yourself employed. And if you're employed and you get fired,

165
00:22:54,280 --> 00:23:00,280
there's always this constant fear in this danger. And it's really a potential danger. You can be

166
00:23:00,280 --> 00:23:09,880
fired, laid off. You can not succeed in your business. And you work so hard for things,

167
00:23:09,880 --> 00:23:15,000
and then you don't get them. And it's a great suffering that comes, of course, from not getting

168
00:23:15,000 --> 00:23:23,080
what you want. And once you get the things that you want, then you have to protect them.

169
00:23:23,080 --> 00:23:28,680
So you get this nice house, and then it gets hit by a hurricane or earthquake,

170
00:23:28,680 --> 00:23:33,400
or you get all these nice things, and then you have to worry about locking your doors,

171
00:23:33,400 --> 00:23:35,080
because you have all these valuables.

172
00:23:45,160 --> 00:23:51,960
I think a lot of people who are convinced that sensuality,

173
00:23:51,960 --> 00:23:58,360
materiality, is a cause for suffering, or a cause for happiness,

174
00:23:59,560 --> 00:24:04,600
are convinced because of how they see the happiness in others. And they tend to generally think

175
00:24:04,600 --> 00:24:11,560
that there's just something wrong with them, that they can't be happy. And I do know some people

176
00:24:11,560 --> 00:24:19,320
who are just very happy with sensuality. But by and large, they tend to incline towards

177
00:24:19,320 --> 00:24:28,280
simple pleasures. And you'll see with time, you know, if you look at older people,

178
00:24:29,000 --> 00:24:35,960
they tend to be much more inclined to more less to sensuality, right? Because over time,

179
00:24:35,960 --> 00:24:45,160
it starts to wear away the goodness of happiness. And they've seen this. They lived your life

180
00:24:45,160 --> 00:24:52,200
and seeing the dangers again and again. Of course, through meditation, this process is accelerated.

181
00:24:53,400 --> 00:25:00,760
If you meditate intently enough, you can see firsthand the nature of addiction. You want something,

182
00:25:00,760 --> 00:25:07,000
you don't get it. It's stressful. You get what you don't want. It's stressful.

183
00:25:07,000 --> 00:25:15,000
Through meditation, you see how messed up the mind really is. And it's not able to sit still.

184
00:25:15,000 --> 00:25:20,600
It's not able to just be, it's, it's a slave to craving.

185
00:25:24,440 --> 00:25:29,640
This scape in all three of these cases is the same. It's the removal of the desire and lust

186
00:25:29,640 --> 00:25:35,320
for these three things. But what's interesting here is this is actually talking about this

187
00:25:35,320 --> 00:25:43,560
first one. It's talking about the removal of the desire and lust for the desire, really,

188
00:25:43,560 --> 00:25:50,280
for the liking of things. So we actually get attached to our liking. So we think it's good to

189
00:25:50,280 --> 00:25:56,920
like things or we have our partialities. And this is a great fear of medit and beginner meditators

190
00:25:56,920 --> 00:26:04,360
is that Buddhism talks about letting go of everything. What about all these things that I like,

191
00:26:04,360 --> 00:26:11,880
my, I'm going to put all my possessions and all the good things. Music, you want me to give up

192
00:26:11,880 --> 00:26:25,960
music. I want a horrific thought that can I? Buddhism doesn't, doesn't talk about giving up

193
00:26:25,960 --> 00:26:29,640
things that you want. It talks about looking at the things you want and understanding them

194
00:26:29,640 --> 00:26:35,720
and seeing the danger. Once you see the danger, then you of course are able to escape from it

195
00:26:37,320 --> 00:26:41,880
just by virtue of seeing the danger and understanding that it's not worth it.

196
00:26:44,840 --> 00:26:52,760
Material form the second one. So the gratification relates to, it's very similar to the first one,

197
00:26:52,760 --> 00:26:59,240
but here specifically talking about the object of your desires. So we get attached to objects,

198
00:26:59,240 --> 00:27:09,000
people, places, things, objects, materiality. And he gives the example of a beautiful woman.

199
00:27:09,720 --> 00:27:14,280
It's again probably talking to mostly men here. His audience might have been completely men.

200
00:27:15,880 --> 00:27:23,720
And he says, imagine there were a girl of the noble or brahmane class and he talked,

201
00:27:23,720 --> 00:27:34,120
he describes her in her dark, not dark north to fair, not thin nor fat. Beautiful basically.

202
00:27:36,120 --> 00:27:41,720
And he says the joy and gratification, the pleasure and joy that arise in dependence on that beauty

203
00:27:42,920 --> 00:27:48,120
or take a beautiful flower or a beautiful smell or good music, beautiful sound.

204
00:27:48,120 --> 00:27:54,840
This is the gratification. The Buddha doesn't deny this. He doesn't say bad, bad, wrong, wrong.

205
00:27:56,840 --> 00:27:59,240
He accepts that there is gratification.

206
00:28:01,320 --> 00:28:06,920
But he doesn't accept it. It's in any way satisfying or gratifying really.

207
00:28:08,840 --> 00:28:16,600
Because these moments do little more than to serve to stoke our desire. They don't actually

208
00:28:16,600 --> 00:28:24,200
satisfy again. It's like a dog with a bone. A dog can never be satiated from chewing on a blood

209
00:28:24,200 --> 00:28:32,840
smeared bone. It's even worse than that because there's the addiction that's associated with it.

210
00:28:34,680 --> 00:28:37,880
And you're cultivating the habit of one thing.

211
00:28:41,320 --> 00:28:44,760
And he says, what's the danger about in this instance? He talks about, well, what about the

212
00:28:44,760 --> 00:28:52,760
one this woman gets very old? And when she's dying, lying fouled in her own urine and excrement,

213
00:28:54,760 --> 00:29:01,320
the beauty is vanished. For flowers, the beauty vanishes. Everything has its

214
00:29:03,240 --> 00:29:09,880
vanishing as a danger, the change as a danger.

215
00:29:09,880 --> 00:29:17,320
And it relates to our desire, right? When you desire something, when you like something,

216
00:29:17,320 --> 00:29:23,480
when you want something, it all comes crashing down when things change. I mean, this goes on

217
00:29:23,480 --> 00:29:28,920
throughout our lives. During the times when you can't have what you want, there's stress,

218
00:29:28,920 --> 00:29:34,120
there's suffering, there's boredom, there's frustration, there's displeasure.

219
00:29:34,120 --> 00:29:43,320
Just having to sit through this talk in many ways is difficult because we would rather be doing

220
00:29:43,320 --> 00:29:47,320
something else, even though we might want to hear the Buddha's teaching.

221
00:29:50,200 --> 00:29:53,960
It's still not easy because our minds are jumping and saying, hey, hey,

222
00:29:53,960 --> 00:30:09,880
remember, remember those things that we liked? This is what desire does.

223
00:30:14,680 --> 00:30:19,000
And the third one, feelings. Now, feelings here is quite interesting and it makes this suit

224
00:30:19,000 --> 00:30:23,640
interesting in another way because it talks about the John, the Summit of John, as many people are

225
00:30:23,640 --> 00:30:30,920
interested in these as a part of the Buddhist path. He's probably the most critical he ever is

226
00:30:30,920 --> 00:30:36,760
about these things. I mean, mostly he talks about them with praise, but here he's dealing with

227
00:30:36,760 --> 00:30:44,280
apparently people from other sects who were practicing the John. There were religious practitioners

228
00:30:44,280 --> 00:30:51,160
who were practicing and entering into the various types of trance and absorption.

229
00:30:52,200 --> 00:30:58,120
And again, the Buddha acknowledges that this is the highest form of feeling.

230
00:30:59,320 --> 00:31:03,000
And there are many types of feelings and with any addiction there's going to be pleasure or

231
00:31:03,000 --> 00:31:09,160
there's going to be calm that comes from obtaining the object of your desire.

232
00:31:09,160 --> 00:31:15,560
But it's curious that he includes the John as in here because remember in this lake,

233
00:31:15,560 --> 00:31:20,760
a suit of people, he says, well, people think this is a facement. This is the way to

234
00:31:21,560 --> 00:31:28,120
root out your defilements to enter into the John as, but it's not true. It's a happy state here

235
00:31:28,120 --> 00:31:36,520
and now, and that's a good thing. But even that can be desired, of course. Many people desire

236
00:31:36,520 --> 00:31:39,640
and they hear about the John as and they want to practice them much more than they want to

237
00:31:39,640 --> 00:31:45,560
practice. We pass in there. It's always going through their minds. Why am I doing this again? Well,

238
00:31:45,560 --> 00:31:51,160
maybe it may be, maybe someone does better for me because they hear about some of them. It sounds

239
00:31:51,160 --> 00:32:00,040
so wonderful. And it is wonderful. It's a gratification. But he says, the problem with feelings

240
00:32:00,040 --> 00:32:05,880
and here he's including the John as is that they're impermanent, unsatisfying,

241
00:32:06,760 --> 00:32:14,680
and subject to change. They're not self. They have no entity to them. They come and they go.

242
00:32:15,720 --> 00:32:19,960
So you can practice meditation that's quite comfortable, quite pleasing. And if your meditation

243
00:32:19,960 --> 00:32:27,560
is quite pleasing, well, great, good for you. But it's not actually satisfying. There's

244
00:32:27,560 --> 00:32:31,720
not anything that comes from it. I mean, I think with all three of these, the thing that we have

245
00:32:31,720 --> 00:32:41,640
to ask ourselves is, what comes of it? What good is it? And we often fail to do this in regards

246
00:32:41,640 --> 00:32:54,040
to sensuality. Rather than having a coherent or a cogent, a clear picture, hey, this is good for

247
00:32:54,040 --> 00:33:00,440
me. And hey, this is a good thing. We're just stuck. And this is how an addict is. The addict

248
00:33:01,080 --> 00:33:07,640
usually knows that they're addicted. And it's just powerless to get out of it.

249
00:33:12,440 --> 00:33:17,000
And so some deep, this deep and clear awareness that

250
00:33:17,000 --> 00:33:24,920
these things that we're clinging to are not any good. I mean, what's worst is when people

251
00:33:24,920 --> 00:33:34,680
cultivate the view that these things are good and beneficial. Most of us are able to go meandering

252
00:33:34,680 --> 00:33:39,080
through life. Sometimes getting what we want. Sometimes not getting what we want and being

253
00:33:39,080 --> 00:33:44,440
content with that to an extent, you know, telling ourselves that this is as good as it gets.

254
00:33:44,440 --> 00:33:49,000
Sometimes good. Sometimes bad. Well, that's life, right?

255
00:33:50,840 --> 00:33:55,080
Other people are more ambitious and they try to root out all the bad and just always get what

256
00:33:55,080 --> 00:34:02,280
they want. Those are the people who are really desperate. That's the worst, of course, because it

257
00:34:02,280 --> 00:34:09,080
cultivates very strong states of addiction, drug addicts and that sort of thing. But for most of us,

258
00:34:09,080 --> 00:34:17,160
when we kind of muddle through life, not clearly. And I think that to this teaching is most

259
00:34:17,160 --> 00:34:22,040
useful for because it's clearly paints a picture of what's going on in our lives. These lives

260
00:34:22,040 --> 00:34:30,440
that we think are just fine are really not. They're really not satisfying. And meditation

261
00:34:30,440 --> 00:34:35,320
shows you that. If you don't believe me, if you think all life is fine and what's wrong

262
00:34:35,320 --> 00:34:42,760
with enjoying things, try meditating for practice meditation. I mean, I think a lot of people

263
00:34:42,760 --> 00:34:49,000
approach meditation and are not able to continue because it forces them to see the nature of

264
00:34:49,000 --> 00:34:54,280
their minds and that they just blame the meditations. They always do much work. It's not very

265
00:34:54,280 --> 00:35:03,720
much fun and that sort of thing. But you really can't deny it. You may be too weak to practice

266
00:35:03,720 --> 00:35:08,440
meditation, but it's very hard to deny that what it's showing you is the nature of your own

267
00:35:08,440 --> 00:35:12,760
mind as being weak, as being addicted and a stay of the craving.

268
00:35:21,640 --> 00:35:26,760
That's about all that's a simple suit done, but it's definitely worth reading. If you have time

269
00:35:26,760 --> 00:35:31,960
read through it and get a deeper understanding of it than what I've just talked about. But hopefully

270
00:35:31,960 --> 00:35:40,440
this has been a little bit of a sort of a flushing out of the suit door or a commentary on it that

271
00:35:40,440 --> 00:35:51,560
allows it to resonate with us as meditators. Again, focusing on these things. It's really describing

272
00:35:51,560 --> 00:35:56,520
the force at the batana. So we have the form when you're addicted to something. There's the visual

273
00:35:56,520 --> 00:36:02,600
right? You see something and with mindfulness, you note the seeing. You note that you're seeing

274
00:36:02,600 --> 00:36:09,400
something. There's the feelings, the pleasure and seeing beautiful things, the feeling that comes

275
00:36:09,400 --> 00:36:15,800
up. And so you note the feeling. There's the liking of it. And so you note the liking. Even

276
00:36:15,800 --> 00:36:25,560
noting the liking. Liking is addictive. Your habit for me. And so the fourth one is the thought. So I

277
00:36:25,560 --> 00:36:31,480
would argue that you also have to be careful about how you think about things and how your thoughts

278
00:36:31,480 --> 00:36:42,200
will lead you to or will reinforce your addiction. And again, it's not about hating these things.

279
00:36:42,200 --> 00:36:47,400
It's about understanding them. You understand the gratification when you like something. Don't

280
00:36:48,120 --> 00:36:53,800
be so hard on yourself. Look at that liking, study it objectively. Not thinking how can I get rid

281
00:36:53,800 --> 00:37:00,760
of this? But thinking what's it like? You'll see the danger. You'll see the bad side, the negative

282
00:37:00,760 --> 00:37:06,520
side. Even with the johnnons, the summit of johnnons, you study them mindfully. The feeling of

283
00:37:06,520 --> 00:37:12,120
being in the johnnons, you'd be mindful of it. And by study, it means be mindful of it. Cultivate

284
00:37:12,120 --> 00:37:19,400
sati. About the johnnons, you'll see. Yeah, they're impermanent, unsatisfying and uncontrollable.

285
00:37:19,400 --> 00:37:25,240
Not me, not mine, not myself, not the goal, not freedom from suffering.

286
00:37:28,760 --> 00:37:33,320
So there you go. That's the demo for tonight. Thank you all for tuning in.

287
00:37:33,320 --> 00:37:53,160
I'm going to go put a mosquito outside.

