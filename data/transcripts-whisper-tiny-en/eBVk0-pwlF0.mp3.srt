1
00:00:00,000 --> 00:00:07,240
During meditation, if one feels anger building up, breathing increases, frowning, mood

2
00:00:07,240 --> 00:00:13,640
changes and pain, should one try to actively discard them with positive thoughts,

3
00:00:13,640 --> 00:00:19,320
make the calming the breath, etc., or just watch it and see that also passes away.

4
00:00:19,320 --> 00:00:27,120
Do you want to start, do you want to finish?

5
00:00:27,120 --> 00:00:30,240
I think this could depend.

6
00:00:30,240 --> 00:00:35,640
If it's just happening in meditation, then I say stick with it and watch it.

7
00:00:35,640 --> 00:00:41,200
If a person is naturally angry person, maybe has an explosive temper, something just

8
00:00:41,200 --> 00:00:48,520
a normal eye, then I think practicing meta and such, along with their normal practice could

9
00:00:48,520 --> 00:00:54,400
be good, but if it's just something arising while you're sitting in meditation, then I just

10
00:00:54,400 --> 00:00:57,720
say, sit with it and watch it.

11
00:00:57,720 --> 00:01:05,080
It's a very good and succinct answer, I don't have much to add to it really, but I would

12
00:01:05,080 --> 00:01:19,640
say one thing is that on top of that, if it continues to come up in meditation, you can

13
00:01:19,640 --> 00:01:25,040
use loving kindness after you're supposed to practice, we pass in a meditation and watch

14
00:01:25,040 --> 00:01:30,200
the anger saying to yourself, angry, angry, being clear about it is just being something

15
00:01:30,200 --> 00:01:35,840
that is arisen and will cease and so on, not just being aware of the anger, but also being

16
00:01:35,840 --> 00:01:41,520
aware of the pain and the other things involved, because anger is only a very small part

17
00:01:41,520 --> 00:01:44,040
of what we ordinary consider to be angry.

18
00:01:44,040 --> 00:01:48,600
The breathing increases, that's not anger, that's breathing increasing, the frowning, that's

19
00:01:48,600 --> 00:01:52,800
not anger, that's physical, the pain in the head, the pain in the body, the tension in

20
00:01:52,800 --> 00:02:01,000
the body, that's also none of its anger, it's a physical and it's body and it's feelings.

21
00:02:01,000 --> 00:02:06,160
But when you see that this sort of thing is building up, you might want to incline

22
00:02:06,160 --> 00:02:14,000
towards augmenting your practice with loving kindness and it's for this reason that we

23
00:02:14,000 --> 00:02:22,000
have these meditations as the arakakamatana, the meditations which protect you.

24
00:02:22,000 --> 00:02:27,200
So we will practice loving kindness to all beings, to beings that we have problems with

25
00:02:27,200 --> 00:02:32,920
when you have a difficulty with someone that at the end of your meditation, you'll reaffirm,

26
00:02:32,920 --> 00:02:37,640
you might not even call it loving kindness, but you could say it's a reaffirmation of your

27
00:02:37,640 --> 00:02:43,200
intentions, you're setting your mind in the right way.

28
00:02:43,200 --> 00:02:47,040
Because you might be practicing, suppose you're practicing Vipasana meditation and when

29
00:02:47,040 --> 00:02:51,000
you think of someone, you get really angry and you say angry, angry and so your mind

30
00:02:51,000 --> 00:02:52,640
is bent out of shape.

31
00:02:52,640 --> 00:02:55,880
When your mind is bent out of shape, you're not really being mindful, you're saying angry

32
00:02:55,880 --> 00:02:57,880
angry but you're really angry.

33
00:02:57,880 --> 00:03:02,800
When you set your mind in the right way with loving kindness or what appears to be loving

34
00:03:02,800 --> 00:03:09,280
kindness, it's also a determination, a detan, saying yourself, no, my intention is that

35
00:03:09,280 --> 00:03:12,360
this person should be happy, not that they should suffer.

36
00:03:12,360 --> 00:03:16,320
My intention is that they should gain, not that they should lose and so on.

37
00:03:16,320 --> 00:03:20,960
When you send these thoughts, loving thoughts, you're reaffirming your intentions so that

38
00:03:20,960 --> 00:03:25,960
when you go back and practice Vipasana, you're clear about this and when the anger comes

39
00:03:25,960 --> 00:03:30,480
up, you're clear that that's not what you want and that really makes it easier.

40
00:03:30,480 --> 00:03:33,800
It's the difference between view and defilement.

41
00:03:33,800 --> 00:03:37,680
The view locks the defilement in place.

42
00:03:37,680 --> 00:03:44,160
When you believe that your anger is justified, this person hurt me and they should suffer,

43
00:03:44,160 --> 00:03:48,760
that proper for them to suffer because then it locks the anger.

44
00:03:48,760 --> 00:03:49,760
You can't deal with it.

45
00:03:49,760 --> 00:03:54,080
You can say angry, angry, angry as much as you want, it'll never go away.

46
00:03:54,080 --> 00:03:59,520
This is an example of how loving kindness can be useful not only to suppress and to change

47
00:03:59,520 --> 00:04:05,000
the mind states but also to change one's view because when you say to yourself, maybe

48
00:04:05,000 --> 00:04:12,320
be happy, you're inclined to a view that they should be happy, that it's proper for them

49
00:04:12,320 --> 00:04:13,320
to be happy.

50
00:04:13,320 --> 00:04:15,440
You're saying to yourself, this is what is correct.

51
00:04:15,440 --> 00:04:42,680
This is what I truly believe in as an example.

