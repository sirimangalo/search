1
00:00:00,000 --> 00:00:17,980
Good evening everyone, broadcasting live April 3rd, 2016, today's quote again from the

2
00:00:17,980 --> 00:00:31,480
Good morning. I think he's not doing a very good job of translating, in my opinion.

3
00:00:38,480 --> 00:00:44,080
I don't know where he would get in attention from, so we have to really ignore

4
00:00:44,080 --> 00:00:51,080
this quote, because he's done a poor job translating, there's no way around it.

5
00:00:51,080 --> 00:01:01,840
So the dumb is here, are the four dumbas that lead to, and not progress, they lead to

6
00:01:01,840 --> 00:01:11,760
benefit, the word is Hita, which just means welfare or benefit, and it's not

7
00:01:11,760 --> 00:01:18,760
even worldly progress, so he's not even translating, he's very much paraphrasing.

8
00:01:18,760 --> 00:01:40,760
I don't know this in the back story, anyway, not important.

9
00:01:40,760 --> 00:02:04,640
So in the reality that is seen, in other words, right now, without delay, without

10
00:02:04,640 --> 00:02:09,220
good, he's not in the next life. Dita dumb often means referring to this life, so

11
00:02:09,220 --> 00:02:15,140
Dita dumb, he die, he means benefit in this life. So it's not exactly worldly, but the

12
00:02:15,140 --> 00:02:25,160
point is, these are the things that lead to benefit in this life, because it doesn't

13
00:02:25,160 --> 00:02:33,920
take practice of the Buddha's teaching to succeed in this life, right? You can actually

14
00:02:33,920 --> 00:02:40,080
be a terrible, terrible person and still succeed to some extent in this life. Your life

15
00:02:40,080 --> 00:02:49,840
may be shorter than others, or it may eventually turn around, but even evil people.

16
00:02:49,840 --> 00:02:57,720
So for that reason, benefit in this life that only touches this life, it's not so much

17
00:02:57,720 --> 00:03:04,440
related to the dumb, and hence the use of the word worldly, but that's not exactly what

18
00:03:04,440 --> 00:03:10,440
it means. These are important, but they're more important than a broad sentence. The

19
00:03:10,440 --> 00:03:17,120
fore actually can be applied generally. I mean, they can be applied to a monk, they

20
00:03:17,120 --> 00:03:22,480
can be applied to meditators. It's not, it's mostly actually directed to lay people,

21
00:03:22,480 --> 00:03:30,240
and people living in the world, but nonetheless they're good to keep in mind. And these

22
00:03:30,240 --> 00:03:34,880
are actually one of the core teachings in cultural Buddhism, like in Thailand, it's a big

23
00:03:34,880 --> 00:03:42,680
one, is for something that used to learn in schools, and the novices learned in schools,

24
00:03:42,680 --> 00:03:52,760
one of the first things we learned. Tamiprayo nai lok nai, dhammas that have benefit in

25
00:03:52,760 --> 00:04:04,320
this life. Prayo tok nai prayo lok nai. So they pair with another set of dhammas which

26
00:04:04,320 --> 00:04:16,520
are those that have samparaya kita, samparaya kita, samparaya kita, samparaya kita, samparaya

27
00:04:16,520 --> 00:04:17,640
kita, samparaya kita, samparaya kita, samparaya kita, samparaya kita, sampika, samparaya kita.

28
00:04:17,640 --> 00:04:25,880
Which is the dhammas that have benefit in the next life. It's another set of fore dhammas.

29
00:04:25,880 --> 00:04:31,640
So we've talked about those as well. In fact, we even have a third set, so we can round

30
00:04:31,640 --> 00:04:36,160
often talk about all the dhammas that have benefit.

31
00:04:36,160 --> 00:04:40,160
Alumpo Chodok, this monk in Bangkok, he used to talk about this.

32
00:04:40,160 --> 00:04:43,800
Brehoglok, niprehoglok, niprehoglok, niprehoglok, niprehoglok, niprehoglok, niprehoglok, niprehoglok,

33
00:04:43,800 --> 00:04:50,800
the third one is soul, so it means the very highest benefit.

34
00:04:50,800 --> 00:04:59,240
Brehoglok, kunipan, that was leads to Nibana, Nirvana.

35
00:04:59,240 --> 00:05:02,000
But he's first for Uthana, sampana means effort.

36
00:05:02,000 --> 00:05:09,520
You have to be endowed with effort.

37
00:05:09,520 --> 00:05:15,320
Dita dhamma hitai, dita dhamma sukhai, if you want benefit, if you're looking for your

38
00:05:15,320 --> 00:05:23,880
welfare and happiness in the here and now, you need effort, you need to work.

39
00:05:23,880 --> 00:05:32,000
So he explains what that means, you have to make your living, you have to be skillful

40
00:05:32,000 --> 00:05:39,200
indulgent, possessing judgment, about it in order to carry it out at a range of problems.

41
00:05:39,200 --> 00:05:45,000
Initiative, bikaboditransits as initiative, and that's still not convinced, but mean

42
00:05:45,000 --> 00:05:54,160
some gumption, taking up the task, in all things, this is the case, the question is,

43
00:05:54,160 --> 00:06:00,360
are you making effort in your work?

44
00:06:00,360 --> 00:06:02,120
So this applies for meditators as well.

45
00:06:02,120 --> 00:06:10,440
If you want the meditation to be successful, you have to work at it, to some extent we

46
00:06:10,440 --> 00:06:25,720
can apply this.

47
00:06:25,720 --> 00:06:40,160
And this is actually related to, as I said, it was being talked to Dika Janu, who

48
00:06:40,160 --> 00:06:44,560
goes by the clan in Biaga, but anyway.

49
00:06:44,560 --> 00:06:50,040
So he says, we are sandalwood from Kasi, we were garland, scents and ungovernance, we

50
00:06:50,040 --> 00:06:57,440
received gold and silver, meaning we're not monks, we're not even spiritually religious

51
00:06:57,440 --> 00:06:58,440
people.

52
00:06:58,440 --> 00:07:04,920
We live in the world, we work, and we play, and we laugh, and we sing, and we eat, and

53
00:07:04,920 --> 00:07:09,800
we drink, and make merry, but what can we do?

54
00:07:09,800 --> 00:07:13,640
He says, teach us the dhamma in a way that will lead to our welfare and happiness in

55
00:07:13,640 --> 00:07:17,600
this present life and in future life.

56
00:07:17,600 --> 00:07:23,680
Dita dhamma sukaya, dita dhamma hittaya, and then the other one, so in future lives.

57
00:07:23,680 --> 00:07:33,240
Samparaya, sorry, Samparaya hittaya, Samparaya sukaya, it's the other one, so for the next

58
00:07:33,240 --> 00:07:34,240
time.

59
00:07:34,240 --> 00:07:38,440
So the Buddha separates them, because they're two different things.

60
00:07:38,440 --> 00:07:43,040
As I said, in this life it's more related to sort of functional things.

61
00:07:43,040 --> 00:07:47,760
So the first one is you have to work, you have to have the initiative, you can't just

62
00:07:47,760 --> 00:07:53,120
sit around and wait for happiness and welfare to come to you.

63
00:07:53,120 --> 00:07:58,640
If you want to succeed in life, you have to work.

64
00:07:58,640 --> 00:08:08,120
Number two, Arak has some Buddha, you have to guard or protect or save up, right?

65
00:08:08,120 --> 00:08:15,920
If the guard near wealth, like which you have amassed, you have to think to yourself, how

66
00:08:15,920 --> 00:08:21,160
can I prevent kings and thieves from taking it fire from burning it, floods from sweeping

67
00:08:21,160 --> 00:08:28,600
it off, and displeasing errors from taking it, you know, errors or errors can be displeasing

68
00:08:28,600 --> 00:08:37,240
errors as in people who inherit inherent things that maybe they come looking for their

69
00:08:37,240 --> 00:08:39,640
inheritance early or something.

70
00:08:39,640 --> 00:08:45,640
So what can I do to sustain my life?

71
00:08:45,640 --> 00:08:52,880
Again, a worldly thing, but it works for all of us, I have to concern myself about my

72
00:08:52,880 --> 00:08:59,280
robes and my belongings.

73
00:08:59,280 --> 00:09:02,840
But in regards to meditation, this is important as value.

74
00:09:02,840 --> 00:09:12,080
And not only do we have to work hard in meditation, but we have to be careful.

75
00:09:12,080 --> 00:09:18,160
Adjantang talks about this, similarly I think from the Risudimanga, someone rocking a

76
00:09:18,160 --> 00:09:23,680
cradle, when you're rocking, when you're in the olden days, they would have the baby

77
00:09:23,680 --> 00:09:28,680
in a hammock, maybe I don't know, or in a cradle, I guess, and they have it on a string

78
00:09:28,680 --> 00:09:35,000
so they could sit far away and they'd just pull on the string to keep the rocker going.

79
00:09:35,000 --> 00:09:40,760
And that would keep the baby asleep, but you had to be careful, you know, if you didn't

80
00:09:40,760 --> 00:09:45,360
keep your eye on it, the baby would wake up.

81
00:09:45,360 --> 00:09:51,440
So the idea is that meditation is like something very delicate that you have to keep your

82
00:09:51,440 --> 00:09:53,120
mind on.

83
00:09:53,120 --> 00:10:03,400
If you're not paying attention, the baby will wake up, you'll lose your mind from this

84
00:10:03,400 --> 00:10:09,760
very easy to get off track, you'll get off track, and you have to bring yourself back

85
00:10:09,760 --> 00:10:13,320
again and again, guarding.

86
00:10:13,320 --> 00:10:21,400
So it's analogous to the works on that level as well, in terms of anything, you could

87
00:10:21,400 --> 00:10:35,600
put this on any level, you need to have effort, and you have to be careful, guard, guard

88
00:10:35,600 --> 00:10:43,800
with you, have gained through your effort, so we don't squander, sometimes people come

89
00:10:43,800 --> 00:10:48,720
to meditate here and then they go out in the world and think they're invincible, because

90
00:10:48,720 --> 00:10:53,280
it's easier to be invincible here, there's not so much bothering you, and you go out there,

91
00:10:53,280 --> 00:10:58,200
you realize, oh, I didn't, maybe I didn't quite get as much out of the meditation as I

92
00:10:58,200 --> 00:11:03,800
thought, maybe we got this much, and you thought, even this much, and of course your expectations

93
00:11:03,800 --> 00:11:13,120
are not met, because you're not mindful, sometimes we go out and it's a shot, wake up

94
00:11:13,120 --> 00:11:18,120
call and you realize you have to guard yourself.

95
00:11:18,120 --> 00:11:24,400
Part of it is negligence, but that's the thing, you can't be negligent, you want to succeed

96
00:11:24,400 --> 00:11:33,640
in anything meditate in spiritual or worldly, you have to be careful.

97
00:11:33,640 --> 00:11:44,520
Number three is, you need good friends, Kalyana Mitata, and this of course goes with everything.

98
00:11:44,520 --> 00:11:49,480
In Thailand, in meditation this was a problem, too many people in the monastery, and it's

99
00:11:49,480 --> 00:11:57,480
easy to get caught up with the wrong people, easy to sit down and chat and stick it side

100
00:11:57,480 --> 00:12:03,840
and interact, and we sidetracked each other as well, luckily here in Hamilton we have very

101
00:12:03,840 --> 00:12:10,680
small centers, so the meditators come and they're not bothered by others.

102
00:12:10,680 --> 00:12:15,120
But by good friendship it means people who are practicing, who appreciate practice, who

103
00:12:15,120 --> 00:12:29,760
teach practice, who will accommodate you in your practice, people who care about you, care

104
00:12:29,760 --> 00:12:36,840
about your spiritual practice, so can offer you advice and you can give you the space you

105
00:12:36,840 --> 00:12:45,280
need and that kind of thing, people who can guide you and be an example for you, that's

106
00:12:45,280 --> 00:12:46,280
a good friend.

107
00:12:46,280 --> 00:12:51,360
The Buddhas are good friend really, that's why we think about the Buddha a lot, because

108
00:12:51,360 --> 00:12:56,800
is someone who when you think about him it gives you a good example, gives you something

109
00:12:56,800 --> 00:13:05,760
to relate your practice to someone who has gone the distance.

110
00:13:05,760 --> 00:13:20,040
Number four is samadhi vata, samadhi vita, samadhi vita, samadhi vita, samadhi vita,

111
00:13:20,040 --> 00:13:30,240
samadhi vita, samadhi vita, samadhi vita means living according to your means, so this

112
00:13:30,240 --> 00:13:36,240
is fairly worldly one, right, so with friendship of course that's obviously important in

113
00:13:36,240 --> 00:13:43,080
the world as well, if you don't have good friends you won't succeed, having to deal with

114
00:13:43,080 --> 00:13:46,800
the stress of enemies and having to deal with their problems, having to deal with people

115
00:13:46,800 --> 00:13:51,440
who lead you in the wrong direction, the way it applies at all, aspects, but samadhi

116
00:13:51,440 --> 00:13:59,200
vita is mostly related to the world, explicitly, this means living within your means, so

117
00:13:59,200 --> 00:14:07,680
if you want to succeed in the world you have to not only acquire and protect your wealth,

118
00:14:07,680 --> 00:14:14,720
but you have to not squander it yourself, you have to budget and you have to be content

119
00:14:14,720 --> 00:14:18,520
with what you have and that kind of thing, because if you live outside your means it's

120
00:14:18,520 --> 00:14:25,160
where people go into debt, it might seem like a trite sort of statement, but it's amazing

121
00:14:25,160 --> 00:14:31,760
how many people live outside their means, aren't able to stop themselves from borrowing

122
00:14:31,760 --> 00:14:41,200
money and getting into serious debt and that kind of thing, you have to be careful,

123
00:14:41,200 --> 00:14:51,000
but the simple word we can apply to meditation as well, because I want to really apply

124
00:14:51,000 --> 00:14:55,880
this to meditation, we want to be talking about what it's the most benefit, we don't have

125
00:14:55,880 --> 00:15:02,760
all that much time to focus too much on worldly things, even though they're helpful.

126
00:15:02,760 --> 00:15:07,000
And also I've got a meditator here listening, so I have to help him give him something

127
00:15:07,000 --> 00:15:22,280
to think about, but some edgymita means the way you're living to be balanced, not too

128
00:15:22,280 --> 00:15:36,960
much excess, but not too little, so the excess and the lack, both not enough too much,

129
00:15:36,960 --> 00:15:41,760
both are problematic, if you don't practice enough, if you practice too much, the practice

130
00:15:41,760 --> 00:15:49,800
without taking a break, it can also be to your detriment, but mostly living throughout the

131
00:15:49,800 --> 00:16:01,040
day balanced with a balanced mind, balancing your faculties, confidence, effort, mindfulness,

132
00:16:01,040 --> 00:16:09,280
concentration, and wisdom balancing them, not practicing too hard, not practicing too

133
00:16:09,280 --> 00:16:10,280
little.

134
00:16:10,280 --> 00:16:23,120
We know the work we have to do, so we do it consistently and systematically, that's what's

135
00:16:23,120 --> 00:16:32,240
important, anyway, those are the benefits in this world, mostly worldly things, the ones

136
00:16:32,240 --> 00:16:41,760
that lead the benefit in the next life, I think he then goes on, right, there are four

137
00:16:41,760 --> 00:16:47,640
other things that lead to happiness and welfare in future lives, so these are a little

138
00:16:47,640 --> 00:16:54,640
bit more spiritual, but this is more leading to heaven, let's see if I can get the

139
00:16:54,640 --> 00:17:02,960
Pauli, here we are, Sanda, Sampada, Sila, Sampada, Kaga, Sampada, Sampada, Sampada,

140
00:17:02,960 --> 00:17:12,920
Sampada, Sampada, so the four, these are the Samparaya, Samparaya, Samparaya, Samparaya,

141
00:17:12,920 --> 00:17:18,400
you need to benefit in the next life, so these are more spiritual, and these are, you

142
00:17:18,400 --> 00:17:27,280
can say, sort of, the positive mundane results, having being still mundane, but these are

143
00:17:27,280 --> 00:17:31,440
the sort of things that lead you to heaven, so there's a positive mundane results of

144
00:17:31,440 --> 00:17:38,240
spiritual practice, like meditation, but these, actually these four are not all that mundane

145
00:17:38,240 --> 00:17:42,560
at all, but they're sort of relating to going to heaven, because again he's telling

146
00:17:42,560 --> 00:17:51,560
this to a lame man, it's not probably meditating, but probably should, but anyway has,

147
00:17:51,560 --> 00:17:57,500
we're going to give some these four, Sanda means confidence, or you could say faith,

148
00:17:57,500 --> 00:18:03,520
Sanda, Sampada, you have right faith, faith in the right thing, faith in the Buddha,

149
00:18:03,520 --> 00:18:11,680
the Dhamma, Sangha, faith in spiritual teaching, faith in good people, faith in good things,

150
00:18:11,680 --> 00:18:18,520
confidence in yourself, confidence in good things about yourself, confidence in your ability

151
00:18:18,520 --> 00:18:23,320
to cultivate goodness.

152
00:18:23,320 --> 00:18:34,640
Number two, Sita, Sampada, morality endowed with morality, the five precepts, guarding

153
00:18:34,640 --> 00:18:45,120
your senses, considering the use of your requisite, so that even things you use, you don't

154
00:18:45,120 --> 00:18:58,240
use them for the wrong purposes, and right livelihood.

155
00:18:58,240 --> 00:19:06,040
Number three, Chaga, Sampada, endowed with generosity, or Chaga is maybe renunciation,

156
00:19:06,040 --> 00:19:09,160
or abandoning, giving up, I guess.

157
00:19:09,160 --> 00:19:13,960
I mean, obviously transcendent is generosity, and a mundane sense absolutely means it means

158
00:19:13,960 --> 00:19:21,600
giving gifts and supporting others and being charitable, but on a deeper level it means

159
00:19:21,600 --> 00:19:27,200
giving up, and you give up your desire or you give up, giving up your desires actually

160
00:19:27,200 --> 00:19:31,240
a great gift, because the less desire you have, the more you are able to give and give

161
00:19:31,240 --> 00:19:37,400
up and help and support others and do for others, but if you're always obsessed with

162
00:19:37,400 --> 00:19:44,520
your own benefit, we're a little time to give to others.

163
00:19:44,520 --> 00:19:50,960
Number four, Bhanya, Sampada, wisdom, one should be endowed with wisdom, this is a great

164
00:19:50,960 --> 00:19:51,960
benefit.

165
00:19:51,960 --> 00:19:57,160
It's also benefit for this life, but sometimes wise people still may not do that

166
00:19:57,160 --> 00:19:59,440
well in this life.

167
00:19:59,440 --> 00:20:05,920
Nonetheless they do well in their minds, and wisdom is the greatest, the most powerful

168
00:20:05,920 --> 00:20:13,320
weapon we have, or tool we have for benefit, for welfare, for happiness, because with wisdom

169
00:20:13,320 --> 00:20:18,480
you know right from wrong, wisdom you're able to rise above your problems when things

170
00:20:18,480 --> 00:20:22,960
go wrong, you're able to see them as they are, and it's not really even about rising above

171
00:20:22,960 --> 00:20:23,960
them.

172
00:20:23,960 --> 00:20:31,440
It's seeing through the cloud of ignorance that leads us to see things as problems, because

173
00:20:31,440 --> 00:20:36,280
there's no such thing, problem is just a name that we give to a label that we give to

174
00:20:36,280 --> 00:20:37,280
something.

175
00:20:37,280 --> 00:20:44,640
The truth is there's experience, wisdom helps us to see that, as a result we don't get

176
00:20:44,640 --> 00:20:49,880
attached to things, because at a wisdom we see that that's a problem, it doesn't lead

177
00:20:49,880 --> 00:20:51,680
to happiness.

178
00:20:51,680 --> 00:20:56,520
We don't get angry about things, because we see that that leads to problem, and we give

179
00:20:56,520 --> 00:21:02,000
up delusion, wisdom is the opposite of delusion, wisdom is like the bright light.

180
00:21:02,000 --> 00:21:08,400
When you shine the bright light, in the darkness goes away.

181
00:21:08,400 --> 00:21:19,200
So the Buddha said, vinayya lokayya bhijadongmanasan, giving up in this world greed and anger

182
00:21:19,200 --> 00:21:24,000
desire and aversion, and that Jantong, he said, why doesn't he ask, I think the common

183
00:21:24,000 --> 00:21:32,560
direct says, why doesn't, but I remember a talk at Jantong said, why doesn't the Buddha

184
00:21:32,560 --> 00:21:38,280
mention delusion, it's because mindfulness, this is from this sati batanas of the mindfulness

185
00:21:38,280 --> 00:21:40,120
is like a bright light.

186
00:21:40,120 --> 00:21:45,080
When you shine it in the darkness disappears, so there's only enough to mention delusion,

187
00:21:45,080 --> 00:21:51,360
and being mindful you're working to give up greed and anger, because you've removed

188
00:21:51,360 --> 00:22:03,720
the delusion during that time, it's wisdom, wisdom can arise about the greed and the anger.

189
00:22:03,720 --> 00:22:10,200
So those for that which leads to benefit in future lives, and the third group that I said,

190
00:22:10,200 --> 00:22:15,520
the one which leads to nymanas, the fourth, actually the fourth sati batana, I think.

191
00:22:15,520 --> 00:22:20,240
And I'm pretty sure that's what nong pochodong used to say.

192
00:22:20,240 --> 00:22:24,840
What are the four that lean to nymana, what doesn't say in this suit, and this only gives

193
00:22:24,840 --> 00:22:29,360
the two sets, but the third set you have to talk about, if you're looking for that which

194
00:22:29,360 --> 00:22:33,160
is the ultimate benefit, it's the fourth sati batana, because the Buddha said, ikayyanoi

195
00:22:33,160 --> 00:22:41,360
nymana, this is the one way, the direct way that leads to nymana, that's the fourth

196
00:22:41,360 --> 00:22:45,800
sati batana.

197
00:22:45,800 --> 00:22:55,440
Anyway, so another little bit of dhamma tonight is to have more work to do tonight, we're

198
00:22:55,440 --> 00:22:56,440
at crunch time.

199
00:22:56,440 --> 00:23:03,080
Tomorrow I don't know if I'll be able to broadcast the symposium on Tuesday.

200
00:23:03,080 --> 00:23:08,920
So, and people who've been emailing us, ask students, leave everything to the last moment

201
00:23:08,920 --> 00:23:13,840
I should have known, that they're all emailing us all days, and apologizing for waiting

202
00:23:13,840 --> 00:23:22,680
for the last minute, and hoping they can join the symposium anyway.

203
00:23:22,680 --> 00:23:37,760
So that's all for tonight, have a good night everyone.

