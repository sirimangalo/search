1
00:00:00,000 --> 00:00:04,240
Souls of the feet, from the souls of the feet upwards, down from the top of the air,

2
00:00:04,240 --> 00:00:07,920
from the highest part of the air, downwards, contained in the skin,

3
00:00:07,920 --> 00:00:13,040
laminated all around by the skin, reviewed, has full of many kinds of health.

4
00:00:13,040 --> 00:00:17,360
We see that this body is packed with the, filled the various kinds of beginning,

5
00:00:17,360 --> 00:00:23,200
the various kinds beginning with head hairs, how in this body they are a head hairs and so on.

6
00:00:23,200 --> 00:00:31,680
So this, this kind of meant it is, it is, it is thought of every monastery and you can

7
00:00:31,680 --> 00:00:35,600
know it as a thought to recite this, that it will pass again and again.

8
00:00:37,680 --> 00:00:43,840
There are means that are formed in this, in this, in this which is expressed thus,

9
00:00:43,840 --> 00:00:48,080
apart from the souls of feet and down from the top of the air and contained in the skin,

10
00:00:48,080 --> 00:00:55,120
as full of many kinds of health. Body is that the carcass, for it is the carcass, that is called body,

11
00:00:55,120 --> 00:01:02,320
because it is a conglomeration of field, because such five things as the head hairs, etc. and the

12
00:01:02,320 --> 00:01:09,760
hundred diseases beginning with high disease have, have a head hair origin. What is the carcass?

13
00:01:12,320 --> 00:01:17,760
Yeah, is it dead body? Yeah, it works. But yeah, it doesn't mean dead body.

14
00:01:17,760 --> 00:01:22,240
It doesn't mean it's very dead. The carcass is just the body, the palm eye, I don't know what I think.

15
00:01:22,880 --> 00:01:24,080
Are you skeleton?

16
00:01:26,560 --> 00:01:29,840
So it is just a body, not necessarily a dead body here.

17
00:01:29,840 --> 00:01:36,800
I think so. I found something that, that a culture, a bird crave, that's a dead body.

18
00:01:38,640 --> 00:01:41,120
Did put the perfect seeds in?

19
00:01:41,120 --> 00:01:47,600
It could be just a skeleton to specifically depend the bones.

20
00:01:48,240 --> 00:01:59,440
But here it is just just a body. Maybe in English has no other word for the body,

21
00:02:00,880 --> 00:02:07,120
because in in Pali, the first word is Gaia, and in the Gaia is defined by the,

22
00:02:07,120 --> 00:02:13,600
the another word Sarira. So the word Sarira is a synonym for Gaia in, in Pali.

23
00:02:14,560 --> 00:02:20,480
So there may not be another word for the body in, in English.

24
00:02:22,480 --> 00:02:29,520
So although it is, the word carcass is used, we should understand it is not a dead body,

25
00:02:30,720 --> 00:02:36,640
because we, we tried to see in our living body, the head hair body hair. So I tried to see

26
00:02:36,640 --> 00:02:42,240
the hypothesis of these things. I think purpose refers to the frame, you know,

27
00:02:42,240 --> 00:02:46,960
it can be a frame, the frame of the body was, it is the empty body posture.

28
00:02:46,960 --> 00:02:53,520
But yeah, just the body, just the frame, you know, not the frame here,

29
00:02:53,520 --> 00:03:01,120
but it is mainly just the body. Because we have to find that hair body, the flesh and all these

30
00:03:01,120 --> 00:03:10,320
things in the body. And because such fine things as we had hair, etcetera, the 100th disease

31
00:03:10,320 --> 00:03:19,920
beginning with eye disease has, it has their origin. So the word Gaia and Pali is explained

32
00:03:19,920 --> 00:03:33,920
to be a compound word, Gu, plus Aia. So Gu, plus Aia becomes Gaia according to his

33
00:03:33,920 --> 00:03:44,400
pali grammar. And Gu, he means vile, or Gu means despicable or something. And Aia means the

34
00:03:44,400 --> 00:03:53,520
place or the origin, the place of head hair and so on, and the 100th diseases. So this is,

35
00:03:53,520 --> 00:04:00,560
that is why the body is called Gaia and Pali. This is the explanation of the word.

36
00:04:02,160 --> 00:04:09,760
And then that you do pass, head hair body hairs. So no one who suggests throughout the whole of

37
00:04:09,760 --> 00:04:14,400
this pattern, long cutters, studying upward from this also to feed, studying downward from the

38
00:04:14,400 --> 00:04:19,120
top of the head and studying from this canal now, have a fine, even in my new test at them,

39
00:04:19,120 --> 00:04:24,800
at all, beautiful in it, such as a pearl, or a gem, or a pearl, I don't know what barrier is,

40
00:04:25,360 --> 00:04:30,320
or aloes, or sapran, or camp, or a tail compound. But the contrary is in fine snapping, but the

41
00:04:30,320 --> 00:04:36,800
various very melodorous, offensive, drag looking sort of field consisting of the head hair body

42
00:04:36,800 --> 00:04:44,320
hairs on the vest. Now, when you want to practice this kind of meditation, the first thing you have

43
00:04:44,320 --> 00:04:53,120
to do is recite. So this many different subject consists in giving it. So first you have to

44
00:04:53,120 --> 00:04:59,760
learn the seven full skill in learning, that means in learning the meditation subject. And they are

45
00:04:59,760 --> 00:05:07,840
proper recitation, one, two, mental recitation, and three, as to color, four, as to shape, five,

46
00:05:07,840 --> 00:05:13,520
as to direction, six, as to location, and seven, as to give the meditation, so you have to understand

47
00:05:13,520 --> 00:05:20,960
all these with regard to the two parts. So the first thing is, you learn, you learn this word by

48
00:05:20,960 --> 00:05:31,200
heart. The head hair body has made, beads, canes, canes, canes, canes, and so on.

49
00:05:31,200 --> 00:05:36,400
Sometimes the first definition is the dead body of an animal, especially once the food. But the

50
00:05:36,400 --> 00:05:43,200
second one is the living body of an human being. So could you use the other way? That is what we

51
00:05:43,200 --> 00:05:48,000
need here, the second, second meaning. Sometimes it means a framework for basic structure as of a

52
00:05:48,000 --> 00:06:01,120
ruling building, so it has to be accepted. So the first thing we have to do is recite.

53
00:06:02,320 --> 00:06:08,800
Even if one is master of the typical, the verbal recitation should still be done at the time of first

54
00:06:08,800 --> 00:06:22,480
giving attention. Even though you are well familiar with the three, three pedagars, you must do

55
00:06:22,480 --> 00:06:30,320
the recitation first. For the meditation subject only becomes evident to some true recitation,

56
00:06:30,320 --> 00:06:34,960
as it did to the two elders who learned the meditation subject from the elder Mahadeva of the

57
00:06:34,960 --> 00:06:40,560
Hill country. On being asked for the imagination subject, it seems the elder gave the text of the

58
00:06:40,560 --> 00:06:48,720
32 aspects saying, do only this recitation for four months. So you recite these 32 bus for four months.

59
00:06:49,440 --> 00:06:56,000
And these people, although they were familiar respectively with two and three pedagars, that means

60
00:06:57,280 --> 00:07:03,120
they know two pedagars, they know three pedagars. So they are another person, but this

61
00:07:03,120 --> 00:07:14,880
mark, this elder is telling them that you recite these 32 bus. They must already have learned

62
00:07:15,520 --> 00:07:24,560
these 32 bus actually, but they accept that advice. So although they were familiar respectively,

63
00:07:24,560 --> 00:07:31,200
definitely with two or three pedagars, it was only at the end of four months of recitation of

64
00:07:31,200 --> 00:07:38,960
the meditation subject that they became streamed and errors. Actually, it means they became streamed

65
00:07:38,960 --> 00:07:54,560
and errors just reciting the 32 cars. And here they became streamed and friends,

66
00:07:54,560 --> 00:08:05,120
not just by recitation. First, they did the recitation and then they reviewed each part as

67
00:08:07,280 --> 00:08:14,720
foul or lonesome and then they developed a wee bus on it. So with all people another, they can

68
00:08:14,720 --> 00:08:24,240
they know stream entry. So although it is set here, or it would appear that they just

69
00:08:24,240 --> 00:08:30,800
reside and became sort of banners, I mean stream entrance, but that is not the case. They recited

70
00:08:30,800 --> 00:08:37,760
and for four months they recited these back and forth, back and forth, and then they got the

71
00:08:37,760 --> 00:08:50,480
sign, got the bar site and so on. And they became sort of an ask after practicing three

72
00:08:50,480 --> 00:08:55,440
bus on our head. So without repostation, they can be more stream entrance, stream entry.

73
00:08:57,920 --> 00:09:05,920
And with right apprehension of the text, and that is not the translating of the Pali word,

74
00:09:05,920 --> 00:09:16,720
the Pali word, there is Padakim Naga Hita. That means since they take his advice with respect.

75
00:09:17,360 --> 00:09:26,320
But Dakim Naga, he means taking with respect. Now, they were landed, these two were landed months.

76
00:09:27,520 --> 00:09:34,080
But once the Disha told them, you recite the 32 parts for four months. They didn't say,

77
00:09:34,080 --> 00:09:39,360
we have already landed, and we don't have to recite the song like that. They have too much

78
00:09:39,360 --> 00:09:47,360
respect for the Disha to refuse. So following with respect, the advice of the Disha, they recited

79
00:09:47,360 --> 00:09:54,240
for, they just recited them for four months, and during that time they became stream enters.

80
00:09:55,840 --> 00:10:01,440
So with right apprehension of the text, there is not the right translation of the Pali word.

81
00:10:01,440 --> 00:10:08,080
Now, when he does the recitation, he should divide it up into the skin,

82
00:10:08,080 --> 00:10:14,480
hand step, et cetera, and do it for once and twice. Because in notes,

83
00:10:16,720 --> 00:10:22,160
it took me about one and a half hours. If you type in,

84
00:10:22,160 --> 00:10:34,480
so please look at the notes. The part is, the 32 parts, that's what he has,

85
00:10:34,480 --> 00:10:41,840
may teach skin. That is the skin patterns. That means the fight ending with skin.

86
00:10:42,720 --> 00:10:48,880
Then next one, flares, they use bone, bone oil, kidney. Five ending with kidney.

87
00:10:48,880 --> 00:10:59,440
And then five ending with lights. And then five ending with brain. And then

88
00:11:03,760 --> 00:11:17,040
five ending with fats. And then six ending with fats. And six ending with urine. So 32 parts

89
00:11:17,040 --> 00:11:25,600
are divided into five, five, five, five, five, six, six. And present, it doesn't

90
00:11:25,600 --> 00:11:36,960
should be done this way. Now, there is another book, another commentary written by the same order.

91
00:11:39,760 --> 00:11:45,920
And it is the commentary on the second book of a beta. In the second book of a beta, there is

92
00:11:45,920 --> 00:11:51,760
mention of the full, full traditions of mindfulness. And so these are mentioned there too.

93
00:11:53,360 --> 00:12:03,040
Now, the recitation I gave on these notes are the combination of statements found in two

94
00:12:03,040 --> 00:12:12,560
commentaries, both in the part of purification and the other book. So just in the book,

95
00:12:12,560 --> 00:12:22,400
a beta purification, there is no mention of how many days you are to recite each group

96
00:12:23,200 --> 00:12:34,080
and how to, how to recite forward and backward and so on. But there the man that is given. So

97
00:12:34,080 --> 00:12:44,480
I combine these two into one. And it will take how many days, 165 days just to recite. So you have

98
00:12:44,480 --> 00:12:53,760
to spend five and a half months just reciting verbally. It will take a long time to practice this

99
00:12:53,760 --> 00:13:04,160
meditation. So the first one, we say, head hairs, body hairs, new teeth skin. So forward, this, this,

100
00:13:04,160 --> 00:13:10,720
this way you will recite five days. So head hairs, body hairs, new teeth skin, head hairs, body hairs,

101
00:13:10,720 --> 00:13:24,880
chest teeth skin and thousands and thousands of times. And then the next five days, you, you go backwards.

102
00:13:24,880 --> 00:13:29,280
Skin teeth, new, body hairs, head hairs, skin teeth, new, body hairs, head hairs.

103
00:13:29,280 --> 00:13:36,080
Just very much recitation, just a, yeah, just bubble recitation. But when you, when you

104
00:13:36,080 --> 00:13:43,440
recite bubble recitation, you also know the meaning of the words. So the meanings may soak

105
00:13:43,440 --> 00:13:50,320
into your mind. And then the, the, they will come next, mental recitation, the first one,

106
00:13:50,320 --> 00:13:58,880
bubble recitation. And it, it alone will take 165 days. So, and then forward and backward.

107
00:13:58,880 --> 00:14:08,240
So that is, head hairs, body hairs, new teeth skin, skin teeth, new, head hairs, body hairs,

108
00:14:08,240 --> 00:14:15,360
like an old bag of food. So it will, for five days. So for the skin pent up, it will take 15 days.

109
00:14:16,960 --> 00:14:24,720
And then next, next penthouse, next penthouse is what? Kidney penthouse. So flesh,

110
00:14:24,720 --> 00:14:33,200
sinews, bone marrow, kidney. So forward for five days. But when you say backward,

111
00:14:35,440 --> 00:14:43,520
you say not just these five, but the previous five also. So the backward for kidney penthouse is

112
00:14:46,880 --> 00:14:52,000
kidney, bone marrow, bones, and new splres, skin teeth, new, body hairs, head hairs,

113
00:14:52,000 --> 00:15:00,320
back to number one. So if you look at the notes, I put the numbers, because it's very boring

114
00:15:00,320 --> 00:15:10,800
due to type these words again, even the numbers. So kidney penthouse at the last five days,

115
00:15:10,800 --> 00:15:19,200
six, seven, eight, nine, ten, ten, nine, eight, seven, six, five, four, three, two, one. And then after these two

116
00:15:19,200 --> 00:15:27,200
penthouse, you combine the two penthouse, and then recite them for 15 days. So that is one, two, three,

117
00:15:27,200 --> 00:15:31,760
four, five, six, seven, eight, nine, ten. It is four. So one, two, three, four, five, six, seven,

118
00:15:31,760 --> 00:15:39,280
nine, ten, and so on. And then backward, ten, nine, eight, seven, six, five, four, three, two, one.

119
00:15:39,280 --> 00:15:43,280
And then forward and backward, one, two, three, four, five, six, seven, nine, ten, ten,

120
00:15:43,280 --> 00:15:48,880
nine, eight, seven, six, five, four, three, two, one. That's 15 days. And then the next time

121
00:15:48,880 --> 00:15:55,120
that 11, 12, studying, putting 15, 11, 12, studying, putting 15, and so on. And then backward,

122
00:15:55,120 --> 00:16:00,960
you go back to number one until number one, 15, 14, 13, 12, 11, 10, 9,

123
00:16:00,960 --> 00:16:06,800
eight, seven, six, six, five, four, three, two, one. And then the third five days,

124
00:16:06,800 --> 00:16:16,240
you decide forward and backward. So this will, after each spender, you combine with the previous

125
00:16:16,240 --> 00:16:23,760
pandas. So two pandas combined together, three pandas together, four pandas together, five

126
00:16:23,760 --> 00:16:36,720
and five groups together, and six groups together. So you go this way, and it will take 165 days.

127
00:16:36,720 --> 00:16:40,240
Hold it.

128
00:16:40,240 --> 00:16:49,440
You have a good question. Most, most often day, if you're a mom, you have to go out for

129
00:16:49,440 --> 00:16:55,440
arms in the morning, and you don't, you cannot do, even when you're going for arms, you can,

130
00:16:55,440 --> 00:17:07,200
you can do recitation, or at least mental recitation, you can do. Because monks are taught,

131
00:17:07,200 --> 00:17:14,640
or instructed, to go with meditation, when they go to go for arms in the, in the village or

132
00:17:14,640 --> 00:17:22,000
in the city. So if if a monk goes without meditation, he's not supposed to be a practicing monk.

133
00:17:24,160 --> 00:17:30,800
So you go to the village or city with some, some kind of meditation, sometimes you may be

134
00:17:31,600 --> 00:17:37,840
practicing just mindfulness meditation, sometimes it's kind of meditation. So almost a whole day

135
00:17:37,840 --> 00:17:50,560
because this is a intensive practice, not just saying for a few minutes, and then give it up.

136
00:17:52,720 --> 00:17:59,040
So these are how to recite, how to do verbal recitation.

137
00:17:59,600 --> 00:18:04,640
The recitation should be done verbally in this way, a hundred times, a thousand times,

138
00:18:04,640 --> 00:18:10,960
even in a hundred thousand times, for a graphic to six. For it is true verbal recitation that

139
00:18:10,960 --> 00:18:19,440
the meditations have become familiar. This is spent 165 days, it cannot but become familiar,

140
00:18:20,080 --> 00:18:25,600
and the mind being that prevented from running here and there, the parts become evident and

141
00:18:25,600 --> 00:18:32,240
seemed like the fingers of a pair of class hands, like a row of fence posts.

142
00:18:32,240 --> 00:18:38,480
Then comes a mandal recitation should be done just as it is done verbally. For the verbal recitation

143
00:18:38,480 --> 00:18:43,040
is a condition for the mandal recitation and the mandal recitation is a condition for the

144
00:18:43,040 --> 00:18:48,240
penetration of the characteristic of foulness. So when you, when you recite it,

145
00:18:48,240 --> 00:18:54,400
mentally, again and again, the sign of foulness, we will, we will evidence to your mind.

146
00:18:56,080 --> 00:19:00,480
And then you have to review them as to color, the color of the head hairs, etc.

147
00:19:00,480 --> 00:19:04,960
Let's repeat the fine. It will be a black arm in this country,

148
00:19:08,560 --> 00:19:14,160
blonde and brunette, red, red, also.

149
00:19:21,840 --> 00:19:26,480
Oh, oh, yeah, no, no, no, anything that you're going to get any color.

150
00:19:26,480 --> 00:19:34,720
And as we say, this shape should be defined, they will be explained later in detail as to

151
00:19:34,720 --> 00:19:40,240
direction. Now in this body, upwards from the navel is the upper direction, upper part of the body

152
00:19:40,240 --> 00:19:45,280
and downwards from the downward direction, lower part of the body. So navel is supposed to be

153
00:19:45,280 --> 00:19:52,080
the mirror of the body. So the direction should be defined as, this part is in this direction.

154
00:19:52,080 --> 00:19:59,360
As to location, that is, they are placed. The location of this or that part should be defined as,

155
00:20:01,040 --> 00:20:08,160
this part is, oh, doesn't, this are, they are wrong places here, right?

156
00:20:08,880 --> 00:20:16,080
The location of this or that part should be defined as, this part is established in this location.

157
00:20:16,080 --> 00:20:24,720
As to delimitation, there are two kinds of delimitation that is, delimitation of the similar

158
00:20:24,720 --> 00:20:31,280
and delimitation of the, here in delimitation of the similar should be understood in this way.

159
00:20:31,280 --> 00:20:38,800
This part is delimited above and below and around by this. This is called, a delimitation of

160
00:20:38,800 --> 00:20:49,600
similar. It is like saying, this club is delimited by, on this side, by the kettle and on the other

161
00:20:49,600 --> 00:20:56,160
side by the microphone or something like that. So this is called, delimitation of similar. And of

162
00:20:56,160 --> 00:21:04,880
the similar means, this is here and not, it has here and not body here. And when you come to body,

163
00:21:04,880 --> 00:21:10,160
this is body here and not has here. So delimiting, like that is called, delimitation of the

164
00:21:10,160 --> 00:21:16,960
dissimilar. And dissimilar here means just not similar, but it does not mean opposite.

165
00:21:20,720 --> 00:21:25,040
So the delimitation of the dissimilar should be understood as, non-inta-mixedness,

166
00:21:26,080 --> 00:21:30,320
in this way, that has not body hairs and body hairs and not has hair and so on.

167
00:21:30,320 --> 00:21:35,840
When the teacher tells the skill in learning in seven ways, that he should do so knowing that

168
00:21:35,840 --> 00:21:41,680
in certain sodas, this meditation subject is expounded from the point of view of repostiveness.

169
00:21:41,680 --> 00:21:45,040
And in certain sodas, from the point of view of elements.

170
00:21:48,080 --> 00:21:54,480
For Indian Mahasadipatana soda, it is expounded only as repostiveness. So these,

171
00:21:54,480 --> 00:22:03,520
that the two parts of the body meditation are given there to develop the sense of repostiveness.

172
00:22:04,160 --> 00:22:09,840
Indian Maha, had to keep a dog on my soda as a name of a soda. And soda 28, that means

173
00:22:12,080 --> 00:22:18,320
middle length things, soda number 28. Indian Maha, who wrote water soda, okay,

174
00:22:18,320 --> 00:22:25,040
much of America. And it had to be Bangasara again, much of America. It is expounded as elements.

175
00:22:25,040 --> 00:22:32,640
So they are divided into two elements, actually. The first 20 belong to the other element,

176
00:22:32,640 --> 00:22:41,600
and the remaining belong to the water element. So the elements, they are mentioned there.

177
00:22:41,600 --> 00:22:50,240
In the Gaya Ghadasati soda, however, four genres are expounded with reference to one to whom it

178
00:22:50,240 --> 00:22:59,200
has appeared as a kalakasina. Now, when you practice the little parts of the body meditation,

179
00:22:59,200 --> 00:23:08,160
you cannot get second genre and so on. You can get only first genre. But in that soda,

180
00:23:08,160 --> 00:23:12,160
four genres are mentioned. And four genres are mentioned because

181
00:23:14,080 --> 00:23:23,120
to that month, these parts appear as color, not as parts of the body. So when they appear as

182
00:23:23,120 --> 00:23:30,960
color to him, he pick up the kalakasina. So kalakasina, but the decision can lead to all four or five

183
00:23:30,960 --> 00:23:38,400
genres. So that is why this said here, four genres are expounded with reference to one to whom it

184
00:23:38,400 --> 00:23:52,000
has appeared as a kalakas. In some of the meditation, it is said that you are not to pay attention

185
00:23:52,000 --> 00:24:02,480
to kalak, right? So if you pay attention to kalak, it becomes kalakasina meditation.

186
00:24:02,480 --> 00:24:10,720
So here it is here and it is an inside meditation subject that is expounded as elements

187
00:24:10,720 --> 00:24:19,920
and a serenity meditation subject that is expounded as repulsiveness. So it is an inside

188
00:24:19,920 --> 00:24:29,920
meditation subject that is expounded as elements. If we divide them into two elements, two kinds of

189
00:24:29,920 --> 00:24:36,480
elements and try to see the elements, try to contemplate on the elements, then it is serenity,

190
00:24:39,920 --> 00:24:46,400
it is inside meditation and it is serenity or some other

191
00:24:46,400 --> 00:24:52,640
meditation subject that is expounded as repulsiveness. So when you see they try to see the repulsiveness

192
00:24:52,640 --> 00:24:59,040
of these things, it is some other meditation. Consequently, it is only the serenity meditation subject

193
00:24:59,040 --> 00:25:05,120
that is further than here. So in this part of the book, only the some other meditation

194
00:25:05,120 --> 00:25:15,440
instruments and so we should understand this meditation here as serenity or some other meditation.

195
00:25:16,400 --> 00:25:18,880
And then 10 full skill in giving attention.

196
00:25:18,880 --> 00:25:34,160
Okay, I think we will do it next week. So next week,

197
00:25:34,160 --> 00:25:49,360
what do we end up? I think to the end of

198
00:25:53,440 --> 00:26:01,520
your good twenties and tases you get to breathing. We'll get to breathing? Yeah. So yes.

199
00:26:01,520 --> 00:26:11,280
Yes. Right. 285,

200
00:26:11,280 --> 00:26:19,960
Give me one.

201
00:26:19,960 --> 00:26:32,480
We're going to show here so thanks to Shina, if should we imagine as you know she right

202
00:26:32,480 --> 00:26:38,340
talked about a lot of suggestions that that we could see at the beginning of you.

203
00:26:38,340 --> 00:26:44,340
I don't know if you can see it like this.

204
00:26:44,340 --> 00:26:45,340
All right.

205
00:26:45,340 --> 00:26:50,340
This is how it's going to be drawable.

206
00:26:50,340 --> 00:26:54,340
I don't know when I'm going to try these shushings.

207
00:26:54,340 --> 00:26:55,340
I've seen it.

208
00:26:55,340 --> 00:26:57,340
I think it's the ashes.

209
00:26:57,340 --> 00:27:00,340
The ashes are collected and some, some,

210
00:27:00,340 --> 00:27:03,340
some to put it into a stuba.

211
00:27:03,340 --> 00:27:05,340
Some, like that.

212
00:27:05,340 --> 00:27:09,340
Sometimes it precisely ashes are hard,

213
00:27:09,340 --> 00:27:12,340
which I haven't seen since.

214
00:27:12,340 --> 00:27:13,340
Yeah.

215
00:27:13,340 --> 00:27:16,340
But, you know, if you don't want to,

216
00:27:16,340 --> 00:27:19,340
only, only the other hands,

217
00:27:19,340 --> 00:27:21,340
when, when they are created.

218
00:27:21,340 --> 00:27:23,340
Now, the body of the air hand.

219
00:27:23,340 --> 00:27:25,340
And you produce, you know,

220
00:27:25,340 --> 00:27:27,340
we call red extract.

221
00:27:27,340 --> 00:27:28,340
Mm-hmm.

222
00:27:28,340 --> 00:27:30,340
And maybe the rock.

223
00:27:30,340 --> 00:27:33,340
Small, small, like small rocks or seeds.

224
00:27:33,340 --> 00:27:35,340
And if you said that,

225
00:27:35,340 --> 00:27:37,340
if you're ready for the other hand,

226
00:27:37,340 --> 00:27:41,340
bigger than size than red except the food.

227
00:27:41,340 --> 00:27:43,340
Mm-hmm.

228
00:27:43,340 --> 00:27:47,340
Very except the Buddha in three sizes.

229
00:27:47,340 --> 00:27:50,340
Mm-hmm.

230
00:27:50,340 --> 00:27:52,340
Like sesame seeds.

231
00:27:52,340 --> 00:27:54,340
I mean, mustard seeds.

232
00:27:54,340 --> 00:27:56,340
And the size of,

233
00:27:56,340 --> 00:27:58,340
some of the size of mustard seeds.

234
00:27:58,340 --> 00:28:01,340
And some of the size of

235
00:28:01,340 --> 00:28:05,340
seed, broken rice.

236
00:28:05,340 --> 00:28:07,340
And some of the size of peas.

237
00:28:07,340 --> 00:28:09,340
These are the three sizes.

238
00:28:09,340 --> 00:28:08,340
Of the

239
00:28:08,340 --> 00:28:10,340
Valley of the Buddha.

240
00:28:10,340 --> 00:28:11,340
Mm-hmm.

241
00:28:11,340 --> 00:28:12,340
And the

242
00:28:12,340 --> 00:28:15,340
Valley of the other hands are set to be,

243
00:28:15,340 --> 00:28:17,340
uh, larger in size than.

244
00:28:17,340 --> 00:28:18,340
Mm-hmm.

245
00:28:18,340 --> 00:28:20,340
Mm-hmm.

246
00:28:20,340 --> 00:28:21,340
Mm-hmm.

247
00:28:21,340 --> 00:28:22,340
Mm-hmm.

248
00:28:22,340 --> 00:28:23,340
Mm-hmm.

249
00:28:23,340 --> 00:28:24,340
Mm-hmm.

250
00:28:24,340 --> 00:28:25,340
Mm-hmm.

251
00:28:25,340 --> 00:28:26,340
Mm-hmm.

252
00:28:26,340 --> 00:28:27,340
Mm-hmm.

253
00:28:27,340 --> 00:28:28,340
Mm-hmm.

254
00:28:28,340 --> 00:28:29,340
Mm-hmm.

255
00:28:29,340 --> 00:28:30,340
Mm-hmm.

256
00:28:30,340 --> 00:28:32,340
Mm-hmm.

257
00:28:32,340 --> 00:28:54,340
Mm-hmm.

258
00:28:54,340 --> 00:29:10,340
Mm-hmm.

259
00:29:10,340 --> 00:29:19,340
Mm-hmm.

260
00:29:19,340 --> 00:29:24,340
Mm-hmm.

261
00:29:24,340 --> 00:29:29,340
Mm-hmm.

262
00:29:29,340 --> 00:29:38,340
Mm-hmm.

263
00:29:38,340 --> 00:29:41,340
Mm-hmm.

264
00:29:41,340 --> 00:29:45,340
Mm-hmm.

