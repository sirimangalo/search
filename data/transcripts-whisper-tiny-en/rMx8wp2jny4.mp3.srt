1
00:00:00,000 --> 00:00:08,280
When I start my meditation session, I pretty much start to yawn straight away a number

2
00:00:08,280 --> 00:00:15,520
of times through the short session. When this happens, I just say yawning and continue

3
00:00:15,520 --> 00:00:18,400
on this way. Is this the right idea?

4
00:00:18,400 --> 00:00:35,040
Do you think, oh, oh, I think noting it is fine, but I'm not too sure on the reason for

5
00:00:35,040 --> 00:00:43,880
the yawning. Maybe the meditation has been taken place at the end of the day, or the physically

6
00:00:43,880 --> 00:00:51,560
tired, or are they finding that when they are meditating, it will go more into a relaxed

7
00:00:51,560 --> 00:00:57,200
state rather than a state of concentration and focus.

8
00:00:57,200 --> 00:01:03,360
Yeah, sometimes you might want to do walking instead if you're tired. Yawning can

9
00:01:03,360 --> 00:01:12,040
be rapture as well. Apparently, you can get into a state of BT and start yawning.

10
00:01:12,040 --> 00:01:17,200
We had this question about, what is a yawn? It's really a monk. He sent me an email and

11
00:01:17,200 --> 00:01:23,560
he asked me, what is a yawn? And I went to one monk and got an answer. And then he

12
00:01:23,560 --> 00:01:27,320
asked, what did another monk said? So I went to that monk and got an answer. And then he

13
00:01:27,320 --> 00:01:35,040
asked, what did I jantong? My teacher said, I went to my teacher and got another answer. And

14
00:01:35,040 --> 00:01:40,080
I actually went to the Topitica as well and looked it up and there's not much body

15
00:01:40,080 --> 00:01:48,280
yawn. What is yawn? What is yawn? The yawning word is a come from. Yeah, yawning

16
00:01:48,280 --> 00:01:54,840
is fine feeling. Go ahead. Oh, sorry. Yeah, I'm yawning physically means that your body

17
00:01:54,840 --> 00:02:04,000
needs oxygen. Why the body tends to you? So maybe some deep breaths or get some fresh air

18
00:02:04,000 --> 00:02:12,640
before meditating. Yeah, if you're doing stressful mind work, it can cause you to yawn.

19
00:02:12,640 --> 00:02:21,840
Your brain is working too hard. If I'm when you start to meditate you yawn because the

20
00:02:21,840 --> 00:02:28,920
oxygen is not getting to the brain or so. So something's triggering that. It can be

21
00:02:28,920 --> 00:02:44,280
if you're thinking too much. Yeah. So yeah, you don't always just have to acknowledge

22
00:02:44,280 --> 00:02:54,760
everything. And that's the key to it. But especially with fatigue when a person is nodding

23
00:02:54,760 --> 00:03:03,000
off, you can get up and walk, you can pull your ears, you can wash your face, you can

24
00:03:03,000 --> 00:03:10,120
recite chanting. I don't know when I was, when I was living as a meditator in Canada,

25
00:03:10,120 --> 00:03:18,480
we tried to do driving meditates, which is of course dangerous, especially at night. Because

26
00:03:18,480 --> 00:03:26,240
you can enter into states of concentration and fall asleep. So in order to combat that,

27
00:03:26,240 --> 00:03:30,120
I was thinking about what can I do with trying to think of different ways, you know, massaging

28
00:03:30,120 --> 00:03:34,080
my arms or something was working until I started doing really loud chanting, which

29
00:03:34,080 --> 00:03:39,440
is, of course, what people have been doing outside of the wood, teaching, right, that

30
00:03:39,440 --> 00:03:46,440
put on some music and sing along. That really helps to wake you up. Anyway, that's not

31
00:03:46,440 --> 00:03:50,840
what I would do if you're yawning. But yeah, you can start doing chanting, but that is

32
00:03:50,840 --> 00:04:17,960
something that that can help.

