1
00:00:00,000 --> 00:00:04,480
It's about what to do after becoming enlightened.

2
00:00:07,640 --> 00:00:10,640
It says, more and more people are waking up,

3
00:00:10,640 --> 00:00:11,640
spiritually.

4
00:00:11,640 --> 00:00:13,240
And for most of them, the question

5
00:00:13,240 --> 00:00:16,240
you can now what information about life

6
00:00:16,240 --> 00:00:19,040
after awakening is usually not made public,

7
00:00:19,040 --> 00:00:21,280
things are just as you are shanty.

8
00:00:21,280 --> 00:00:25,680
It's much often shared only between teachers and students.

9
00:00:25,680 --> 00:00:28,560
The end of your world, that's the title of the book,

10
00:00:28,560 --> 00:00:31,160
is his response to growing in need for direction

11
00:00:31,160 --> 00:00:33,160
in this three-fold path.

12
00:00:33,160 --> 00:00:36,200
It's interesting to see the question

13
00:00:36,200 --> 00:00:39,800
of whether you need guidance after enlightenment.

14
00:00:39,800 --> 00:00:42,040
Well, that's obviously the difference,

15
00:00:42,040 --> 00:00:46,120
I think, it's incredibly odd from a Buddhist point of view

16
00:00:46,120 --> 00:00:49,600
and enlightened being should need any guidance.

17
00:00:49,600 --> 00:00:51,760
If you need Buddhism, there's a difference

18
00:00:51,760 --> 00:00:57,000
between our heart and good, right?

19
00:00:57,000 --> 00:01:02,960
But in our hand doesn't work to become a Buddha,

20
00:01:02,960 --> 00:01:05,640
and they don't have any desire left

21
00:01:05,640 --> 00:01:08,560
so they don't have any intention to do anything.

22
00:01:08,560 --> 00:01:12,160
They don't need guidance because they're not going anywhere.

23
00:01:12,160 --> 00:01:15,720
They're just unhappy.

24
00:01:15,720 --> 00:01:20,640
But there's a difference between our heart and the Buddha, right?

25
00:01:20,640 --> 00:01:27,040
Yes, but not in the sense of needing guidance.

26
00:01:27,040 --> 00:01:31,640
Although at our hand would take guidance from a Buddha,

27
00:01:31,640 --> 00:01:35,080
but it would only be my functional.

28
00:01:35,080 --> 00:01:38,240
So if the Buddha said, no, don't say that, or do say this,

29
00:01:38,240 --> 00:01:40,920
or go do this, or go do that, they would do it.

30
00:01:40,920 --> 00:01:42,960
But not in order to become more enlightened

31
00:01:42,960 --> 00:01:52,680
or not because they were at a loss for what to do.

32
00:01:52,680 --> 00:01:55,120
Because maybe in our heart,

33
00:01:55,120 --> 00:02:00,800
we may not necessarily know what to do in each situation

34
00:02:00,800 --> 00:02:04,000
and be wildly real.

35
00:02:04,000 --> 00:02:05,600
But that wouldn't cause them any suffering.

36
00:02:05,600 --> 00:02:08,920
It wouldn't be a problem for them.

37
00:02:08,920 --> 00:02:12,880
It is a good point. There is maybe room for it.

38
00:02:12,880 --> 00:02:17,120
But we're rules that would benefit others

39
00:02:17,120 --> 00:02:24,160
not being enlightened, they like to be in support ourselves.

40
00:02:24,160 --> 00:02:30,680
It would be like a kind of does to benefit others

41
00:02:30,680 --> 00:02:34,800
once you reach enlightenment.

42
00:02:34,800 --> 00:02:37,800
That could be, you know, not all our hands

43
00:02:37,800 --> 00:02:40,080
are inclined to benefit others.

44
00:02:40,080 --> 00:02:44,680
So Marahan's just go off and live in the forest.

45
00:02:44,680 --> 00:02:47,520
So you could incline them to do that,

46
00:02:47,520 --> 00:02:51,880
or invite them to do that, or ask them to do that.

47
00:02:51,880 --> 00:02:53,800
And so there is something on there.

48
00:02:53,800 --> 00:02:57,920
But it sounds much like the idea of needing guidance,

49
00:02:57,920 --> 00:02:58,680
and so on.

50
00:02:58,680 --> 00:03:02,400
An Alhan doesn't need guidance.

51
00:03:02,400 --> 00:03:06,240
Anyway, it just seems like an odd.

52
00:03:06,240 --> 00:03:08,800
You know, the suspicion, of course, is that they're dealing

53
00:03:08,800 --> 00:03:10,480
with an enlightenment that we wouldn't consider

54
00:03:10,480 --> 00:03:11,960
to be enlightened.

55
00:03:11,960 --> 00:03:16,560
You try to, it has a chapter that is in this title.

56
00:03:16,560 --> 00:03:20,480
It's interesting for the members.

57
00:03:20,480 --> 00:03:21,160
I got it.

58
00:03:21,160 --> 00:03:22,640
I lost it.

59
00:03:22,640 --> 00:03:26,480
It's like the experience that many people

60
00:03:26,480 --> 00:03:31,600
seem to have the experience of enlightenment

61
00:03:31,600 --> 00:03:33,120
or something of experience.

62
00:03:33,120 --> 00:03:38,560
And then it fades away with a tiny.

63
00:03:38,560 --> 00:03:40,400
Yeah, that's common.

64
00:03:40,400 --> 00:03:43,160
That's a sign that it's not enlightened.

65
00:03:48,440 --> 00:03:54,080
It's also possible that you see, or experience new Ghana,

66
00:03:54,080 --> 00:03:57,480
and then you go back to normal life.

67
00:03:57,480 --> 00:03:58,640
Yeah, I'm not sure.

68
00:03:58,640 --> 00:04:00,440
And it's not hate.

69
00:04:00,440 --> 00:04:06,720
Yeah, so you may not have, you may not go back to Nirvana

70
00:04:06,720 --> 00:04:09,200
for a long time, maybe.

71
00:04:09,200 --> 00:04:10,800
It's certainly possible.

72
00:04:10,800 --> 00:04:14,080
But what doesn't leave you is the state of being

73
00:04:14,080 --> 00:04:15,440
a so dependent.

74
00:04:15,440 --> 00:04:18,840
And there's some quality of the mind involved in it

75
00:04:18,840 --> 00:04:19,760
that doesn't disappear.

76
00:04:23,520 --> 00:04:27,240
Totally unalterable.

77
00:04:27,240 --> 00:04:30,840
Almost as good.

78
00:04:30,840 --> 00:04:37,520
The state of being a sotepan, the fruit of a sotepan.

79
00:04:37,520 --> 00:04:39,800
I mean, the details of it.

80
00:04:39,800 --> 00:04:46,960
It's the sotepan that has no envy or envy or jealousy.

81
00:04:46,960 --> 00:04:47,600
What's the other?

82
00:04:47,600 --> 00:04:49,640
Stingingness or envy.

83
00:04:49,640 --> 00:04:55,080
They have no wrong view about the existence of a self.

84
00:04:55,080 --> 00:04:57,080
They have no attachment to rights and rituals.

85
00:04:57,080 --> 00:05:01,560
And they have no doubt about the Buddha, the Dhamma, and the Sangha.

86
00:05:01,560 --> 00:05:09,320
They're also said to keep Rya-kantasila in the pure

87
00:05:09,320 --> 00:05:13,360
sealer, pure five precepts, the morality

88
00:05:13,360 --> 00:05:16,200
that is dear to the noble ones, which

89
00:05:16,200 --> 00:05:17,440
said to be the five precepts.

90
00:05:17,440 --> 00:05:20,320
So the idea is that a sotepan would not break

91
00:05:20,320 --> 00:05:21,600
any of the five precepts.

92
00:05:21,600 --> 00:05:29,280
I think a question is the concept of the sort of kind of present

93
00:05:29,280 --> 00:05:32,760
in every tradition you could list the movement.

94
00:05:32,760 --> 00:05:33,720
I can't really say.

95
00:05:33,720 --> 00:05:38,880
I have a real difficulty with other tradition questions

96
00:05:38,880 --> 00:05:42,480
because I don't study them.

97
00:05:42,480 --> 00:05:43,640
Suppose maybe I should.

98
00:05:43,640 --> 00:05:47,200
But I think the reason for not is you just

99
00:05:47,200 --> 00:05:49,320
get so disgusted by how much disgusted

100
00:05:49,320 --> 00:05:52,200
but kind of turned off by how many traditions there are

101
00:05:52,200 --> 00:05:55,840
and how they just seem to just pop up every week.

102
00:05:55,840 --> 00:06:00,360
So a much more comfortable saying, I do what I do,

103
00:06:00,360 --> 00:06:02,600
and I have no problem you doing what you do.

104
00:06:02,600 --> 00:06:04,000
But it's not what I do.

105
00:06:04,000 --> 00:06:08,200
So don't try to convert me and we'll be friends.

106
00:06:14,040 --> 00:06:16,120
So is it?

107
00:06:16,120 --> 00:06:19,440
I think it is, I think, from what I understand

108
00:06:19,440 --> 00:06:23,560
most Buddhist schools, the ones that you hear about,

109
00:06:23,560 --> 00:06:28,000
are very much in line and have the same understanding.

110
00:06:28,000 --> 00:06:31,600
They just tend to focus on a different aspect.

111
00:06:31,600 --> 00:06:36,120
So in general, the Mahayana just focuses

112
00:06:36,120 --> 00:06:37,160
on becoming a Buddha.

113
00:06:37,160 --> 00:06:41,840
That's really the main difference to go into details

114
00:06:41,840 --> 00:06:44,480
and talk about no, but you really

115
00:06:44,480 --> 00:06:48,400
get into different monasteries, more than different schools.

116
00:06:48,400 --> 00:06:51,920
Because in general, Mahayana, they

117
00:06:51,920 --> 00:06:56,160
don't deny the teachings of the Teravada.

118
00:06:56,160 --> 00:06:59,480
They just don't focus on it.

119
00:06:59,480 --> 00:07:03,400
There's very little disagreement, if we just talk to each other.

120
00:07:03,400 --> 00:07:05,800
This was the thing we got doing as Buddhist news together.

121
00:07:05,800 --> 00:07:10,680
Me and Lama Kunga, we really agreed on mostly everything.

122
00:07:10,680 --> 00:07:14,840
We are a little bit, but people argue it's just constructive

123
00:07:14,840 --> 00:07:16,240
dialogue.

124
00:07:16,240 --> 00:07:24,240
But there were not surprises you as well.

125
00:07:24,240 --> 00:07:29,880
You think that you probably will be incompatible.

126
00:07:29,880 --> 00:07:34,760
And then you find out that you're really saying things

127
00:07:34,760 --> 00:07:41,760
very much in line with each other.

