Hi, and welcome to my latest installment of Ask a Monk.
So the latest question or set of questions comes from India.
Would I in asks, when do we say someone is enlightened?
Then what is the difference between a monk and enlightened?
When do we say someone is enlightened?
Well, we don't generally say when someone is enlightened,
when we don't generally say when we are enlightened,
if someone becomes enlightened, they generally wouldn't try to make that known.
But the meaning here is, I think, how do you know if you are enlightened
or what should be the criteria for enlightenment?
And basically speaking, the best way to tell if someone is enlightened
is that they have no greed, no anger, and no delusion.
So you have to look and see whether they are actions in their speech,
be lie states of greed, states of anger, states of delusion,
states of mental, defilement or pollution.
And more so in yourself, you have to look and ask yourself,
do I still have greed, do I still have anger, do I still have delusion?
Do these states have the potential still to arise?
Even though at certain times they disappear,
you have to ask yourself whether these things still have the potential to arise.
Is there still ignorance? Is there still conceit and so on?
The difference between a monk and an enlightened one is quite profound.
A monk is a conceptual state.
You know, you say you are a monk based on the ceremony and the social status
or the constructed process that you have gone through.
Becoming enlightened is totally ultimate reality.
It is brought about by meditation practice and realization of things as they are.
Becoming a monk is often compared to a vehicle.
So when we become a monk, it is like getting a super powered vehicle
or a very rugged and high powered automobile as opposed to an ordinary person
whose vehicle might be smaller and less rugged and coalesce quickly.
And why this is because for lay people there are many other external cares and worries
and concerns that they have to deal with.
So they have to get involved often with things that have no relationship with meditation practice
whereas a monk theoretically.
And again it is theoretical because many monks might be quite far from realizing the truth and becoming enlightened.
But theoretically they are surrounded by meditators and meditation teachings.
And so on a regular basis they are in touch with these things and it is quite conducive towards the development of tranquility and insight.
Question number two, how far do you think I as a lay person would go in the process?
Well that is totally up to you.
I mean lay people can become enlightened.
There is no question about that.
In fact there are many lay people who practice better than most monks.
But it has to be said that for most lay people it is an uphill struggle
because there are so many other concerns and it depends on you.
It depends on your life as a lay person.
If you have a family and children and a job and maybe even are involved in things which are antithetical to the meditation practice,
then it is always going to be a question of how many steps forward and how many steps back.
If on the other hand you can keep yourself away from these things then being a lay person shouldn't be a hindrance at all for you.
So it is all about how you organize your life and it is what you make it.
Being a lay person is not a barrier to enlightenment.
Number three, would I be able to visualize past lives and future lives if we succeeded in we pass in the meditation, insight meditation?
Short answer, no. It is true that sometimes in we pass in the meditation kind of accidentally meditators will bump into such things.
So when you are practicing we pass in the meditation because of your old character or maybe old meditation practice you have done,
or even just practicing sort of in a round about fashion, getting caught up in states of concentration, states of tranquility.
It is possible to fall into some memory, especially if there are strong memories or if you have been in a fatal accident in your past life or something like that.
These things would be very strong in this psyche.
Then these can come up and I have seen meditators who have related these sorts of things and they say they are quite sure that it is a memory.
It is nothing like a dream or a fantasy. It is quite certain in their minds that it is a past life.
So there are a lot of people who don't believe in this obviously you do.
And I think there is no question that there are people who can remember past lives as for future lives.
That is much more difficult. I do know cases of people who are able to predict the near future, like talking about the next five minutes or the next half hour.
As far as predicting future lives, I think it is a case of predicting something that is very uncertain, a lot like predicting the weather which can be predicted for more than seven days.
So past lives, yes, though indirectly and know not in terms of your success in the past and the meditation.
If you are more successful, you become probably the less of an issue it becomes though again there are people who incidentally develop this awareness.
As for future lives, probably not.
Okay, so those are answers to your questions. Please leave any further questions as comments, private message or send to my email.
Thanks for tuning in.
