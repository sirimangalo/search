Hello and welcome back to our study of the Dhamupada.
Today we continue on with verses 133 and 134, which read as follows.
Today we will continue on with the Dhamupada and the Dhamupada and the Dhamupada.
Today we will continue on with the Dhamupada and the Dhamupada and the Dhamupada.
Which translate roughly as do not speak harshly to anyone.
Having been spoken to, they may speak back to you.
They may reply in kind.
Dukha, he is sadharma, Dhamupada, angry words are painful.
But in Dhamdha we say you tell them.
And you may be struck, may come to blows,
maybe struck us a result, or maybe hit and return.
May lead to violence perhaps.
Satyane is yet done, and if you can keep yourself still,
consul bhatoyata, like a broken bell, a broken gong,
asapatosi nimana, this is the way you will attain nimana.
And there will be no anger found in you.
So this has to do with harsh speech.
And the origin story is relatively long compared to some of the stories recently and fairly short.
I don't have to go into all the detail.
But briefly, to sum up, there was a monk called Konnadhan.
And everywhere he went, he was followed by a vision of a woman.
So quite curious, whenever he would go for arms,
people would see this woman following behind him.
And they would say, here's a portion for you,
and here's a portion for your lady friend.
And he never saw this woman.
He would turn around and see no woman.
But everybody else saw this woman.
And so word kind and got around.
And the monk's got upset.
But it tells a story of how this happened.
So in ancient, and the story is actually somewhat interesting.
And it's all quite fanciful, I mean, for a modern skeptical audience,
I think there's a lot of distrust of these sort of magical type tales.
How could there be this woman?
Well, the story goes that in the time of Buddha digai, digai,
one of the past Buddhas.
So in another age, in another universe, perhaps.
Why does it say digai?
Or a cusp, I don't know, a cusp of our digai.
Anyway, there were these two monks who were so close.
It was as though they were brothers.
They would do everything together.
They were the best of friends and the closest of companions
and so they would always come out and come out together
and go for their meals together and go to meetings together.
Meditate together, that kind of thing.
And it happens that there was an angel, a goddess.
A female angel, somehow, who saw them and thought to herself,
I wonder if there's a way that I can split these two up.
Listen carefully, this gives some insight into the true nature of the divine.
We always, many other religions worship or pray to or expect protection from angelic beings.
Well, it's not always there in inclination.
So this just on a whim, this angel thought,
I wonder if I can split these two up.
And so one day when they were walking along the highway or the forest,
one of the monks said, hold on, I've got to go use,
what do you say, attend the needs of nature?
I think it says in the English,
you had to pass water, you had to urinate.
So he went off into the bushes.
And as he was coming out, this angel appeared behind him,
as a beautiful woman, half naked and adjusting her robe and fixing her hair.
As if coming out of the bush, the elder had just been in.
And the other monks saw this and immediately suspected the worst in the first monk,
accused him of breaking the discipline,
accused him of having intimate relations with the woman.
And the mother monk denied it, of course.
And they ended up completely splitting.
And this monk went before the other monks,
and accused him and all the monks.
He interrogated this other monk who denied it all.
So they couldn't come to a decision over it,
but completely destroyed their friendship.
Now this angel realized and began to feel the effects of how evil it was,
the thing that she had done.
She wasn't malicious.
She was just somewhat deluded and perhaps impetuous or careless,
intoxicated perhaps in her powers and her great own greatness.
But she felt terribly guilty.
And so she went down and told the monk,
the monk who had accused the second monk who had accused the first monk,
told them what she had done.
And this monk believed her, but they say it didn't ever fix their friendship.
They ended up never becoming friends,
fast friends again, because they didn't.
They couldn't.
It was like the first one felt like yet the other one had betrayed him
by believing such a thing could be possible.
So those two monks were reborn according to their garment.
Whether the goddess was born in hell
and suffered there for a period of an interval between two Buddhas
was born as a man.
And eventually became our protagonist,
our antagonist, the name of this story,
the name of this story,
Kṛṣṇṣṇḍhāna.
I became a man.
It was born a boy and grew up and became a monk.
But everywhere he went, this woman followed him.
And so the monks were really upset about this
and thought, we kind of do something
because everyone's starting to talk
and they're going to think that we,
that women, women follow us around.
This monk is going with a woman.
So they went to an Atapindika,
who was the owner of the donor of the monastery.
And they said, look, you have to kick this monk out.
They're not sure why they would do this
because the Buddha was there.
But this, I've seen this happen before.
The monks go to lay people trying to solve the monastic problems.
But An Atapindika was having none of it.
He said, well, isn't the Buddha in Savatī?
He said, well, the Buddha will know what to do.
He did nothing.
So they went to Visaka, who was the other chief flame disciple.
She also did nothing.
So finally, they went to the king for some reason.
And they told the king about this, that there was this monk
who had a woman following him around everywhere.
He went, and the king should do something about it.
And the king, of course, being just an ordinary worldling
was taken by their words and went to the monastery
and brought some men and surrounded this monk's hut
and ordered him to come out.
And the monk looked out and saw the king.
And so he went, and he came out.
So the other came out.
And the king saw them.
Immediately saw that there was a woman behind this monk.
And so the others, when he saw the king,
he went back in and sat on a seat.
And when the king went in, didn't see the woman.
And so he looked everywhere.
He looked under everything in all corners of the room,
up in the rafters everywhere.
I couldn't find this woman.
He said, where is she?
Where is she?
The woman.
And this goes on.
And eventually, he figures out that it's just
some kind of strange phantom phenomenon.
And so he says, look, because he takes the monk out.
He brings the monk back in, doesn't see the woman.
He says, it's not real.
And so he tells the monk, he says, look,
you're always going to have this trouble if this keeps
happening.
So you come to my home for arms.
You come to the royal palace for arms.
And the other monks found out about this.
And they were incensed.
They said, you know, not only is this monk corrupt.
But now the king is corrupt as well.
And so they were bad-nosing the king and bad-nosing this monk.
And they told this, they came to this monk.
And they said, well, so now you're the king's bastard,
they said.
And the monk, you know, fairly reasonably, although
wrongly, became incensed at these monks.
And shot back at them.
You heard the ones who corrupt.
You're the bastards.
You can sort with women.
You're the ones.
If they said all these things that weren't true.
And these monks were shocked.
And they went to see the Buddha.
And they told the Buddha this.
And the Buddha called this monk to him and asked him if it
was true.
And yes, it's strange.
Why did you do that?
Well, they said these things to me.
Well, the Buddha said what they were saying these things
because of what they'd seen.
Now, did you see them concerning with women?
Did you see them?
Do you have any reason to say these things?
They said, no, actually.
Actually, I just made that all up.
And the Buddha said, for you, this bad thing has happened to you
because of things you've done in your past life.
There's something that you have to bear with.
And so then he told the spirituals.
They don't speak harshly.
So the story is interesting.
And it provides a little bit of interesting context.
And the general philosophical topic here is about misconception.
And what sort of misconception is important?
And what sort is not?
You see, these monks who saw this woman behind the monk
and they had no reason to doubt it.
Their claim that this man was being followed by a monk was wrong
but reasonable was in an ultimate sense.
On an ultimate level, it was justified.
It is indeed the monks misunderstanding, the monks wrong.
It doesn't even seem like an misunderstanding.
It seems like he premeditated.
His misunderstanding wasn't in terms of these other monks being
bastards or being followed around by women
associating with women.
He knew that was not true.
But it still it's a misunderstanding.
But it's a deeper misunderstanding.
It's a misunderstanding that somehow saying these words
getting angry even is somehow the right answer.
But there's some benefit to these things.
This is fairly important because Buddhism still considers
all evil to come from misunderstanding.
You don't just get angry.
You get angry because you think somehow that it's good to get angry.
You don't understand anger.
It's crucial because our ordinary way of dealing with anger
is when we feel that it's wrong is to suppress it.
Same with greed.
When you want something, the only way to deal with that is to
suppress your urge.
So we believe the anger is wrong.
The root is the anger.
It's not.
The anger isn't the root problem.
The root problem is you don't understand greed.
You don't understand anger.
You don't understand the things that you're getting greedy and angry about.
When you want something, it's because you think it is stable,
satisfying control.
When you dislike something, it's because you feel like you can make it
stable.
You can change it to be stable, satisfying and controllable.
And only when you see clearly, when you come to see the truth about this,
the truth about this, because we don't really know about the other set of
monks, whether they had a reaction to these things.
So they had this experience where they saw this woman or they even heard about
this woman following the monk around.
It says they actually saw it.
But we don't know what their reaction was.
Their reaction was neutral and objective over that their reaction was angry.
But it's reasonable.
Their misunderstanding was only in an abstract sense in terms of concepts.
So the concepts, they got the concepts from.
They thought this was a real woman when in fact it was just a phantom moment.
But in an ultimate sense, they were saying it was factual.
This monk appears to be followed around by woman.
And so how this relates in general to all of us, because I don't think these circumstances are all that common.
But obviously, the general circumstances of finding yourself being accused
wrongfully, of finding yourself being abused, finding yourself being manipulated
or taking advantage of finding yourself being unfairly treated.
Now sometimes it's with reason.
Sometimes justified.
Sometimes you really are being unfairly treated.
Sometimes there's malicious intent.
But sometimes there's not.
And whether there is or there isn't, actually is not the important.
And the most crucial aspect.
If you know you're being wrongfully treated, this monk should have, you know,
he was fully in his right to say I'm not being followed by a woman.
And that's when he didn't begin it.
Yes, that's not true.
There is no woman.
I've never consorted with a woman.
It's just not true.
But I mean, the important point is you can't control the results of your situation.
You can't always solve everything.
And everyone has experiences of misunderstanding.
Sometimes you're reconcilable.
Sometimes to the point that people actually think you're a bad person, as in this case,
when in fact you've done nothing wrong.
Just because you didn't have a chance to clear your name,
it does speak to the importance of trying to explain yourself.
But in the end, you're never going to fix everything.
You can't fix life.
And so you're always going to be, it's just another situation where there's going to be the potential for suffering.
Whether it's going to be an unpleasant experience, an experience that has the potential for the arising of anger, of dislike, of aversion.
Of course, became terribly averse, simply based on his, not on his misunderstanding in the situation,
but on his misunderstanding of the right, of the reality of the situation, therefore the right responds.
Or there are the nature of reality.
He didn't understand that getting anger was a bad thing.
He didn't understand what that others might, whether it be results, there will be consequences.
Dukkahi, as I say, tukkahi,
Sarambhakata, there indeed.
Angry words are unpleasant, are stressful, are suffering. Dukkahi.
It might lead to blows even.
The next verse is actually sort of a generally good Buddhist.
What the word is the saying, or aphorism, is that the word?
If you keep yourself silent as a broken gong, you have already reached, already reached.
Don't know about that.
You reach, I don't think it's already opatosis past.
Let's see. Let's see. Let's see. Let's see.
Maybe.
The Arahan keeps himself silent.
Remember the story of this Arahan, who was accused of stealing a ruby, I think we had this story already, right?
And it turned out this bird had eaten it, but he wouldn't turn in the bird because he knew if he did the bird would be killed.
And he didn't want to be responsible for that bad karma, so he kept completely silent even as he was being tortured as the thief, as the suspect for the thief.
Be silent as a broken gong.
This speaks to patience.
Be patient with some sorrow.
There's another story of this novice who had his eye poked out.
And so he only did his cover of his eye, and then when he went to go off from water to his teacher, he would poke his eye out.
He offered with one hand.
When the teacher was, why are you offering with one hand?
Well, my other hand is occupied.
But he was an Arahan as well.
And so he also wasn't upset.
And the teacher was mortified, and he begged forgiveness, and the novice said,
it's not your fault then, or it's the fault of some sorrow.
And again, this is the case with this story.
Our situations are the best answers.
They're the fault of some sorrow.
It's the way things go.
It's maybe not the way things had to go.
They could have gone differently.
But the point is that it's how some sorrow works.
Sometimes misunderstandings arise.
It's not always clear.
It's not always pleasant.
Unpleasantness can arise from many different things from natural disasters,
from human violence.
And it can arise from simple misunderstandings.
You might call it a sort of structural violence,
where the structure, rather than the people, is a cause for suffering.
There's no one to blame for it, not directly.
But suffering is potentially inbuilt into the structure.
So in terms of our meditation, we try to just be mindful.
Rather than trying to fix things, because fixes are not consistent,
they're not consistently attainable.
You can't clump.
You can't always fix your problems.
And so trying to make that a solution is the wrong path,
the wrong way to be.
Instead of trying to find solutions to our problems,
we learn to unravel them and to just be with them,
such as to stop seeing them as problems.
To see through the problems to the ultimate reality
and to understand the experiences and the
objects of experience objectively,
without judgment, without partiality,
without expectation.
This is what we do throughout the meditation.
We become like a broken gong,
but it just sits there, it doesn't ever ring.
It doesn't ever react, you know?
You're a rung of broken gong, it doesn't reverberate.
Just like that, there's no anger, there's no reaction.
When people are nice to you,
you don't become elated when they speak well of you,
when they praise you, and when they accuse you,
when they vilify you.
This is like a gong, it doesn't,
no matter how you hit it, it doesn't reverberate.
It was broken.
So, that is our number for tonight.
Thank you all for tuning in.
We should be all the best.
