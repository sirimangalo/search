I started translating your book on meditation to German, is that okay with you?
And is it okay to distribute it then to my friends once if you're translating it?
I have no problem with people doing anything they want with my books or anything I make.
Go ahead.
I mean, I'm first of all wonderful, thank you, honored to have people even think about translating something and wrote.
It's like, wow, thank you.
That makes me happy to know.
And on two levels, on the level that like, wow, I did something that people thought was worth translating.
And on another level that, wow, I think this is worth translating and someone's doing it.
So, what a good thing they're doing, because it's not my teaching, it's my teacher's teaching and I guess the Buddha's teaching.
But just to be clear, I don't believe in copyright.
I don't think it's useful, except in a monetary sense.
If the market wants to, if economic systems want to say copyright is useful and for people to make money and make a living off of things,
then I'm not convinced, but I'm not going to fight against it.
And certainly, that has nothing to do with me or my work.
So, if you do something bad with my work and use it to, I don't know, distort the Buddha's teachings, then that's your bad karma.
I'll tell people not to read it or look at it or so.
But certainly, I would never sue you or even demand anything from you if you did that.
Now, if you're doing good things with it, if anyone does anything, if you do anything good with something that I've made, please, you don't have to ask me.
Let me know that would be nice, but only because that would make me feel happy.
If I found out that people were using my things for good use, that would make me happy.
It's the only reason, not because I demanded or because I think you have a duty to tell me, just because it would be nice, if I heard about it, I would like to hear about it.
So, yeah, go ahead with it. Thank you. The other thing is, if you do translate it, how to meditate book, please send it to me.
Because I can put it up on my website and other people can read it as well.
We've got Chinese and Spanish already, isn't it? It's wonderful. Someone actually translated it into Chinese. Tina Chen in their name. That was like, that was ultimate.
We have some of the trends that you work into Chinese just to see something you've written in Chinese.
I don't know, is that getting proud I don't know.
Thank you.
