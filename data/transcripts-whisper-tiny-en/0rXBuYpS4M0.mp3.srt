1
00:00:00,000 --> 00:00:07,120
Is there anything we can do in this life to guarantee that we will meet the Dhamma again

2
00:00:07,120 --> 00:00:09,280
in our next life?

3
00:00:09,280 --> 00:00:16,680
Well, simply being close to the Dhamma.

4
00:00:16,680 --> 00:00:28,760
Our minds tend, it's not entirely sufficient, but our minds tend towards the sorts of things

5
00:00:28,760 --> 00:00:33,200
that we are engaged in.

6
00:00:33,200 --> 00:00:41,160
So there's just so many factors that you can never really be sure.

7
00:00:41,160 --> 00:00:50,000
But what you can be sure is if your mind is not in line with the Dhamma, then it doesn't

8
00:00:50,000 --> 00:00:53,360
matter whether you meet with the Dhamma or not.

9
00:00:53,360 --> 00:00:59,520
So in fact, it's important to meet with the Dhamma, but it's far more important to be ready

10
00:00:59,520 --> 00:01:01,680
to meet with it.

11
00:01:01,680 --> 00:01:07,520
And that's, I think, something that is much more under our control than whether we actually

12
00:01:07,520 --> 00:01:08,520
meet with the Dhamma.

13
00:01:08,520 --> 00:01:15,120
So you can't say what your next life is going to be like, all of the factors are just

14
00:01:15,120 --> 00:01:21,800
so hard to predict and there's so many things involved with the chance of where are you

15
00:01:21,800 --> 00:01:31,800
going to be born, but you can affect where your mind is at that time.

16
00:01:31,800 --> 00:01:39,320
So in some cases, like in the case of Sumana the Gardiner, who I talked about in the last

17
00:01:39,320 --> 00:01:46,000
time, a part of a video, he met with the Buddha and he listened to the Buddha's teaching,

18
00:01:46,000 --> 00:01:52,320
I think, he paid great respect to the Buddha, but the Buddha's prediction was that he would

19
00:01:52,320 --> 00:01:55,240
never become enlightened under a Buddha.

20
00:01:55,240 --> 00:01:58,240
That eventually he would become enlightened by himself.

21
00:01:58,240 --> 00:02:03,560
He would become self enlightened as a particular Buddha, private Buddha, sometime in the

22
00:02:03,560 --> 00:02:04,560
future.

23
00:02:04,560 --> 00:02:11,800
So it's a reminder that it's actually meeting with the Dhamma in the sense of meeting

24
00:02:11,800 --> 00:02:21,120
with the past on teachings is not the most important thing.

25
00:02:21,120 --> 00:02:25,880
The most important thing is getting your mind in line with the Dhamma, which most likely

26
00:02:25,880 --> 00:02:27,560
will cause you to meet with the Dhamma.

27
00:02:27,560 --> 00:02:32,760
It's just not certain, but as I said, what is certain is if you're in a bad place, if

28
00:02:32,760 --> 00:02:38,920
your mind is in a bad place, it doesn't matter if you meet with the Dhamma because you'll

29
00:02:38,920 --> 00:02:41,360
never meet with the Dhamma, the truth.

30
00:02:41,360 --> 00:02:46,240
The Dhamma is on different levels, there's the teachings, and then there's the truth,

31
00:02:46,240 --> 00:02:51,080
which the teachings provide or help one realize.

32
00:02:51,080 --> 00:02:54,280
So someone can realize the truth without having to be taught in God.

33
00:02:54,280 --> 00:03:00,080
It's much more difficult, of course, but the worst is if your mind is not ready to understand

34
00:03:00,080 --> 00:03:07,120
and then it doesn't matter whether you receive the teachings or not, you'll never become

35
00:03:07,120 --> 00:03:08,160
enlightened.

36
00:03:08,160 --> 00:03:15,600
So the best we can do is to be ready for the teachings, to purify our minds and have our

37
00:03:15,600 --> 00:03:16,600
minds in a good place.

38
00:03:16,600 --> 00:03:22,360
So the worst and best we can do is now that we have met with the teachings, as to

39
00:03:22,360 --> 00:03:24,280
realize the Dhamma in this life.

40
00:03:24,280 --> 00:03:29,400
Obviously that's the best and thinking about the next life is a really bad place to focus

41
00:03:29,400 --> 00:03:35,920
our attentions, but a side product of that is, first of all, we'll be ready for the

42
00:03:35,920 --> 00:03:41,920
teachings, and second of all, provided our mind is involved enough with the teachings

43
00:03:41,920 --> 00:03:47,440
and involved with people who are meditating and so maybe helping out in a meditation

44
00:03:47,440 --> 00:03:54,600
center or teaching meditation or helping other people learn the Dhamma.

45
00:03:54,600 --> 00:03:59,240
For example, when you teach the Dhamma, you would think that that puts you in a position

46
00:03:59,240 --> 00:04:04,600
to learn the Dhamma in the future, that if karma is really all that it's cracked up to

47
00:04:04,600 --> 00:04:11,160
be, the best way to receive teachings is to give them, even in this life, you can see

48
00:04:11,160 --> 00:04:15,840
the best way to learn something is to teach it and many people will verify.

49
00:04:15,840 --> 00:04:23,520
So obviously practicing the Dhamma is the best way, but just remember that the most important

50
00:04:23,520 --> 00:04:29,760
thing is to be ready for it rather than to simply meet with it, though not to put down

51
00:04:29,760 --> 00:04:35,240
the chance of meeting with the Dhamma, it's a rare opportunity and it's a wonderful thing

52
00:04:35,240 --> 00:04:36,240
that we have.

53
00:04:36,240 --> 00:04:42,320
It's just that I would say it's not entirely certain, even if you are a practitioner because

54
00:04:42,320 --> 00:04:45,880
there's just so many variables involved with karma, it's something that we can't really

55
00:04:45,880 --> 00:04:51,000
understand and can't really predict unless we're fully enlightened Buddhas.

56
00:04:51,000 --> 00:05:06,600
So there you are.

