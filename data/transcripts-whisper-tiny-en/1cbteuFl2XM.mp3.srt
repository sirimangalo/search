1
00:00:00,000 --> 00:00:05,000
I understand desires, the root cause of evil, but I still keep on desiring.

2
00:00:05,000 --> 00:00:09,000
And the guilt that I'm still desiring makes me desire even more.

3
00:00:09,000 --> 00:00:11,000
How do I stop?

4
00:00:14,000 --> 00:00:17,000
It's true, no, the guilt will make you desire even more.

5
00:00:17,000 --> 00:00:22,000
Why that happens though is interesting, because guilt is suffering

6
00:00:22,000 --> 00:00:26,000
and suffering leads you to want happiness.

7
00:00:26,000 --> 00:00:30,000
If you're happy you won't seek out happiness.

8
00:00:30,000 --> 00:00:34,000
If you're happy you won't have any reason to desire.

9
00:00:34,000 --> 00:00:39,000
So it's not the guilt that makes you desire more, it's the suffering.

10
00:00:39,000 --> 00:00:43,000
You have to find happiness, it's the only answer.

11
00:00:43,000 --> 00:00:48,000
Your meditation shouldn't be a stressful or painful experience.

12
00:00:48,000 --> 00:00:53,000
Even though you might experience great pain and great stress in the meditation,

13
00:00:53,000 --> 00:00:59,000
your reaction to it should be the one that leads you to find peace and happiness.

14
00:00:59,000 --> 00:01:12,000
To pacify, to quench, to extinguish the fire of desire and guilt.

15
00:01:12,000 --> 00:01:16,000
This is why I say when you look at the desire comes up,

16
00:01:16,000 --> 00:01:21,000
you have to accept it and all the parts of it, especially the pleasure.

17
00:01:21,000 --> 00:01:25,000
Because once you reach the happiness, you don't need the desire anymore.

18
00:01:25,000 --> 00:01:31,000
So if you focus on whatever happiness comes from desire, which is the pleasure and so on,

19
00:01:31,000 --> 00:01:33,000
then the desire will disappear.

20
00:01:33,000 --> 00:01:36,000
If you just say to yourself, happy, happy or feeling,

21
00:01:36,000 --> 00:01:42,000
focusing on the actual experience,

22
00:01:42,000 --> 00:01:48,000
then you find the desire will quite quickly fade away.

23
00:01:48,000 --> 00:01:51,000
And when you have the suffering of the guilt,

24
00:01:51,000 --> 00:01:56,000
this is even more important because suffering is what makes you strive after happiness.

25
00:01:56,000 --> 00:02:02,000
And as long as you have suffering, you'll always be striving to remove it

26
00:02:02,000 --> 00:02:05,000
and to bring about happiness, pleasure and happiness.

27
00:02:05,000 --> 00:02:09,000
So when there's guilt, it's actually suffering.

28
00:02:09,000 --> 00:02:15,000
Focus on the suffering and react in such a way that you're okay with it,

29
00:02:15,000 --> 00:02:18,000
that your mind is no longer suffering as a result.

30
00:02:18,000 --> 00:02:24,000
Your mind is no longer reacting with suffering, which is quite clearly to be mindful of it.

31
00:02:24,000 --> 00:02:31,000
When you say to yourself, angry, angry, guilt, feeling, feeling of guilty, guilty or however,

32
00:02:31,000 --> 00:02:32,000
you say it.

33
00:02:32,000 --> 00:02:37,000
And when you clearly see the experience, you find your mind relaxing.

34
00:02:37,000 --> 00:02:39,000
You find your mind letting go.

35
00:02:39,000 --> 00:02:41,000
And it has to be repeated.

36
00:02:41,000 --> 00:02:42,000
It has to be repetitive.

37
00:02:42,000 --> 00:02:46,000
You have to continue to do this for a moment to moment because it'll come back.

38
00:02:46,000 --> 00:02:49,000
You can't say, okay, I did it and I solved it.

39
00:02:49,000 --> 00:02:53,000
Next moment, it's going to come back and it'll be even more persistent.

40
00:02:53,000 --> 00:03:20,000
So you have to be patient and work on it.

