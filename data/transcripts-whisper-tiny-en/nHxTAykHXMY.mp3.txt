Good evening and welcome back to our study of the Dhamapada.
Today we continue on with verse 101, which reads as follows.
Saha sampi jiigatha, Anatapada sangita, yi kangatha padang sayo, yang sutwa upasamati, which means
very much the same as what we, as the verse yesterday. So yesterday it was about a thousand
words. Saha sampi jiigatha padang, no, a jiwa ja no, a different jiwa ja, which I guess would
mean words. Let's see here. Saha sampi jiwa ja, so yesterday it was about words, a little
bit of a different, almost the same verse, but a little bit different. The idea is the
first one was about words today, talking about a gatha, which is a verse or a stanza.
Gatha is a whole, like this is a gatha, that I've just said is a stanza, I guess, or a verse,
whatever, however you say it. Saha sampi jiigatha, so the meaning is, if there should be a thousand
words that are unconnected with, or not connected. Again, it's a nata panda, so I may have been
right, a nata panda means the path of the useless path, that are connected with the useless
path, they're not connected with the useful path, so they don't lead you in a good way,
but they don't teach you something useful. They may be inclined you in the wrong direction
or direct your mind towards confusion or towards delusion. Make you angry or upset.
A kangata padangseyo, one gatha is better, one verse is better. Young supa opa sambati,
and one hears it, which hearing, one hearing, makes one tranquil. So again, the whole
idea is of the teaching not taking a lot of time. I'm not going to talk too much about
this verse because it's very similar to the last one, but that's okay because we've
got a whole story here to talk about, and there's another verse that we do want to talk
about that occurs in the, another quote that we want to talk about that occurs in the story.
So the story goes that there were a bunch of merchants or some group of men who went
sailing, probably to find their fortune, and halfway through their trip, out in the ocean
I guess, the ship sprung a leak and broke apart, and everyone was drowned except for a man
called Bahia dau jia. And if any of you know the story of the elder Bahia, that's what
the story is about. So Bahia being shipwrecked wound up naked, you know, in order to swim
to shore, he had to lose his clothes or his robes or whatever he was wearing, and swim naked
to shore, and he wound up in a place called Supagara, Supagara, Supagara. I don't know where
that is, but some ocean port on the Indian subcontinent, and being naked, he figured
he had to do something to cover his private parts. So he went and found a piece of bark
or something sort of really simple to cover his private parts, and then he picked up a piece
of a broken pot and went into the city, looking for arms, or went into the village looking
for arms. So out of an act of desperation, he became a beggar, and the funny thing was that
when he got to the village, people took one look at him in his sort of hard state of
being, you know, maybe he'd been on the ship and he had let his hair grow and his beard
grow, so he looked like an ascetic. And so they took one look at him wearing this bark,
this piece of bark, I guess there was like sort of a soft bark that you could actually
turn into cloth, so they had that wrapped around his private area, and using this simple,
you know, piece of garbage as a bowl, they thought, wow, here's a real ascetic. And
so the rumor started, they saw him, and so everyone gave him food and took care of him,
and the rumor started spreading that he was in our hunt. And it was really because of
the way he looked. So as time went on, people tried to give him good clothes, tried to
give him things that would improve his state, and he refused them, because he realized,
you know, the only reason I'm getting such good food from these people is because they
think I'm a holy man. And so he totally played up this ruse, which, you know, it speaks
volumes to, or it acts as sort of a commentary on the state of religion and India in
the time of the Buddha, and even the state of religion today, where there's so many charlatans
or phagocetics, who really have no interest in teaching, but put on the robes as a means
of livelihood, or they put on errors and pretending to be in line, pretending to be mindful
acting, saying things that are more mysterious or enigmatic to try and garner support.
And so in the beginning, it was just a ruse, and he was playing it up, acting like an
Arahan dressing like an, what people thought of as an Arahan, of course, in Buddhism,
there's no sense of this being within Arahan looked like or anything. But eventually,
people called him, people were so convinced that he was enlightened, that he himself
became started to believe it. And he started to feel like maybe he did have some spiritual
attainments after all. And so as time went on, he got right into the role and got a sense
that, well, maybe I am, maybe I am an Arahan, and he was so well taken care of by the people
that it just became sort of, he became a figure in this, this village or this society,
and he himself started to get stuck there. And he would have been quite stuck. If it weren't
for, the commentary says, one of his blood relatives, and they used this word blood relative
actually sort of with a double meaning or not with a double meaning, but yeah, they don't
really mean blood relative, but it has a different connotation. What they mean by blood
relative, what it said, is actually not referring to his blood relative. It's referring
to a fellow religious person in a past life, a fellow monk. So it happens that in the time
of the Buddha Kasypa, there were a group of seven monks, and this is actually a fairly
well-known story of these monks. This whole story is quite a well-known Buddhist story.
So if you're hearing it for the first time, remember it well, you can tell that at Buddhist
parties, tell it to all your Buddhist friends, it's a good story. So far, so good. So now
we have to go back to the past, though. Here we leave Bahia, where he's just starting
to give rise to this thought, maybe I am in our heart, and we have to explain what happened
next. By going back to the past, it happens that there were these seven monks in the time
of the Buddha Kasypa. It was actually after the time of the Buddha Kasypa, which is interesting.
It's a unique description of the corruption of Buddhism. So there were seven monks who were
noticing and really discouraged by the fact that the monks, the novices, even the lay
people were quite corrupt. The whole society had become corrupt, and so they said to
us themselves, and the quote is, so long as our religion has not yet disappeared, we will
make our own salvation sure. So they paid respect to the Jaitia, the big golden monument
in honor of the Buddha, and the Bodhi tree, and then went to the forest to a mountain,
and put this ladder up so they could climb up to this cliff, sort of this isolated cliff
halfway up to the cliff wall, just like a ledge, and then they cut off or threw down
the ladder, and they agreed amongst themselves that they would practice until death.
Either death or enlightenment was supernatural powers that would allow them to escape
their faith. So I complete an utter dedication, it's like do or die, really do or die.
And so they practiced, I guess the ledge was big enough for them all to do at least sitting
meditation, maybe they didn't do walking meditation, don't know how big the ledge was.
But the seven monks meditating arduously are with ardor, arduously, it's not the right word,
ardently, that's the word. And within a day, they were totally dedicated, and you can
imagine how that affected them, and encouraged them in their practice, actually could
be a useful sort of thing. I mean, don't quote me on that, don't go off, and try this
at home. That's one of those things you want to leave to the experts. We'll have to remember
these guys were monks, and maybe they didn't, probably they didn't actually have contact
with the Buddha Kasypa, this would have been after he had passed away. So nonetheless,
it's something interesting if someone were so inclined to, I don't can't believe I'm
saying that, probably I shouldn't encourage it. I mean, absolutely. I mean, it's something
that we would encourage in Buddhist countries, but under the interests of not being
sued, I think we will not encourage such activity ourselves. And the last interesting
thing that had happened, an interesting concept, and within a day, the eldest of them,
the most senior of them, became an arhat, and became an arhat with powers to levitate his
body and fly down and get food. And so he came back with food, this part you don't have
to believe, this part is, you know, where we can decide whether we want to believe or
not believe, it's not important, it's not an important part of the story. So if it strikes
you the wrong way, or if it makes you feel bad, or it makes me lose credibility in your
eyes, well, please consider this isn't the important part. They came back with food for
the other monks, and said to them, please eat with me. And the other, and you kind of get
the sense he may have just been being kind, but there was something to what he said, because
as an arhat, he could teach them, right? Especially if they were to take food, and then
he could probably give them some instruction. I'd imagine that was part of his thinking.
But the other monks took one look at him and said, excuse me, did we make this kind of
deal that if you became enlightened and we would start eating your food, like living off
of your enlightenment? And he said, no, we didn't make such a deal. And he said, well,
then if we become enlightened, we will go and get alms food on our own power. And so
sure enough, the second day, the second monk became an anagami, not an arhat, but an anagami.
Anagami is someone who has freed themselves from desire, central desire, and aversion.
So they still have some desire, desire for becoming wanting to be this, wanting to be that.
They're still the concept of conceit, not exactly self, but kind of a bit of an attachment
to ideas. But as a result, they are born in the purest of pure Brahma worlds. They're
called the pure abodes, and they live there for billions of years. Until finally, they
become an arhat into there. This realm is so pure that it can only lead to freedom. It might
take billions of years, but I don't know how long I'm assuming it's billions, lots and
lots of years. And he too gained magical power. So he too went down, brought food back,
and the two of them tried to convince the other five to eat, because by now, two days
without food or water, they were really doing badly. But the other five were adamant,
and they refused the food. And so for the next five days, they strove ardently. And after
the seventh day, they all died. All seven of them, actually, I think. It actually doesn't
talk about the other two. You wouldn't think the other two would have died. Maybe they
died as well. Let me see. Yeah, no, actually, the other two that became a light and probably
didn't die there. There's no sense. It wouldn't make any sense that they didn't, because
they were able to leave at that will. But the other five withered up and died. And
were born in the heavens. They were born in a good place, because their minds were
really set on good things, totally dedicated to goodness. I mean, there's nothing wrong
with such a thing. I mean, acceptance of far as people think it's suicide, but not really.
It's interesting, because in India, this was a big thing. Starving yourself to death was
considered to be valid religious practice, not in Buddhism, but in other religions. So these
monks weren't exactly starving themselves. They were just putting themselves in a position
where there was nothing would deter them, probably still not not not not not exactly
encouraged sort of practice for the reason that you've only got a but a week before
you die. So inside really is either do or die. Anyways, an interesting story quite well
known and discussed. But they were all born in heaven, and eventually became five of
the Bhutagotamas, five people at a time of the Bhutagotama. I'm not sure if I can't remember
about them all, but three of them, at least three of them became our hands. Anyway,
one of the five was Bahia, Dauru, Dauruji, India, our guy. So here he is, after all that,
kind of getting the idea that maybe I am an Rhaan, then it probably is coming partially
from his intense desire to be an Rhaan. So he starts to think, hey, maybe this is it,
maybe I found it, even after all these lifetimes it was still in his deep in his psyche.
And at that moment, we are told this, the number two elder who remember was born in the
Brahma realms in the Sudhavasa, the pure boats. And this is, I guess, lots and lots of
years later, but he's still up there, kind of gets word of it, or maybe he's been keeping
tabs on his brethren throughout Samsara. And so it just happens to get a sense of what's
going on with Bahia and looks down and starts shaking his head. And I don't know, Brahma's
have heads, but he realizes the need to go down and say something, go down and fix things,
make things right. And he says, well, he is a friend of mine, he is a relative brother
in the Dhamma. So I have an obligation, I should go and do something. So apparently this
Brahma, this God basically came down, maybe he stirred up a storm or thunderbolts in lightning,
thunder in lightningbolts. And says, Bahia, you are not in our hunt nor are you on the
path to become in our hunt. The practice that you have undertaken is not the practice
that leads one to an hour on. You don't have the qualities in our hunt. You are not an
hour on period. And Bahia is looking up at the great Brahma, a God, I'm telling him this.
And I think that would stir up just about anyone. And so he realizes, no, clearly I'm
not an hour on. This is correct. Sort of give it's a wake up call. If you don't like that
version of the story, you can pretend the story goes that someone came up to him and
woke him up and said, look, you don't look like an hour on, buddy, you're not an hour
on. Then he was, he was stirred and he asks his friend, do you know where there is? Okay,
maybe I'm not an hour on, but do you know of anyone who is? Someone I can go to? Because
it seems like a good thing to be. Something worth inclining towards. And you know, here
I am, everyone thinks I'm an hour on. It would probably be a good idea to actually become
one, so I'm not a hypocrite or a charlatan. And the deity tells him in Sawati, that's where
you've got to go. To the north of here, it's to the north. So this port, Suwadipa Suparaka,
Suparaka is in the south. And so he sends him on his way to the north. There's a city called
Sawati, that's where you have to go. There's a Buddha. And by drop his belongings there
and ran throughout the night and arrived at the city of Sawati. Just as he arrived at the
city of Sawati and went, asked around and went to, went up to some monks and asked where
the Buddha was. And they said, oh, he's just gone for arms into the city of Sawati. So
by here, ran towards the city and got to the Buddha just as he was, or as he was walking
through the streets. And when he sees the teacher there in the streets, he bows down,
prostrate himself at the Buddha's feet and says, venerable sir, I am at your mercy, please
teach me the dhamma. And he said, where have you, where are you from? He said, I come from
Suparaka. And he said, oh, you've come a long ways and sit down, rest a while. I'm on
arms around. Once I finish arms around, then we can, we can talk. When I return, just wait
here and when I return, I would teach you. But by here is having none of it. He says,
no, please now. I don't know when I'm going to die. I don't know when you're going to die.
I don't know what the future holds. Please, I mean, I don't need anything big. Don't need
a long teaching. Just give me a brief, brief teaching. It's fine. And his body is trembling
and shaking. He's just so excited. And just having seen the Buddha, they said, I found
finally also could probably have something to do with the fact that he lived with the
Buddha before. But he is ecstatic about this. And it says that that's, for that reason,
the Buddha turned him away and the Buddha put him off. The Buddha said, so again, the
Buddha says, look, it's not the right time. I'm going on arms. Wait, wait. And again,
Vahis says, please, please, please, please. And for a certain time, I'm going to put some
off. And this for a third time, but he says, no, really not to be put off. He requests
for a third time. But the Buddha, the Buddha actually, at this point, I'm going to
do this for a purpose, not actually intending to put him off, but to calm him down. So
by the third time, he had kind of focused his mind and calmed himself down and he was
ready to learn. And so the Buddha did give him what amounts to one of the really one of
the best simple explanations of Vipasana meditation. And so this is the teaching that I
think we have, we can talk a little bit about rather than actually dealing with the
Dhamma Pandavas tonight. And he says, ahuang bhahya, sikita bhang, what does it say?
Tasmatiha tay bhahya ahuang sikita bhang. Well, then bhahya, this is how you should train
yourself. And then he says, tay diktamatang bhavasati, suttay suttamatang bhavasati. The full verse
and the full teaching is in the uddhana, not in the dhammapada. So this is just a recalling
of the events that led up to the teaching. But the gist of it is, diktay diktamatang bhavasati,
let's see, let what is seen, just be what is seen. Let the scene just be seen, mata, mata
means strictly or just only, merely. And so what that, that really is the essence of
insight meditation, just hearing this, suttay suttamatang bhavasati and then he goes on to say,
if you do this, there will be no self for you. There will be no self in this, there
will be no self from this. You will have no attachment to here or there or anywhere. And
just from that very basic teaching, bhahya became an orat. I'll come back to the teaching
in the second, but we'll just finish our story here. After becoming an oratang, it turns
out that bhahya didn't have the good fortune. He had never given any gifts of monastic
requisites. He'd never given robes. He'd never given a ball. And so there's an idea that
this is somehow prerequisites for actually being able to ordain. It sounds kind of an improper
for me to say this as a monk, but it is sort of the truth. And I think it's worth
a bear's telling because it's the kind of thing that I personally think about. For those
people who become monks and are keen to stay as a monk, it's also a very useful thing
is to give robes to other monks and make that as a determination. So I'm always trying
myself to give robes and give monastic requisites with the intention that it should allow
me to always be to remain in this state of being a monk. It's a support for your own determination
to stay as a monk. So I think it's a useful thing to talk about, not that I'm saying,
you know, hey, give things to monks because, you know, wow, that's kind of crass, I suppose.
But nonetheless, that's what the commentary says and it seems reasonable because he wasn't
able to ordain. The Buddha said, he said, please ordain me and the Buddha didn't say,
come monk because he didn't have the good karma. In fact, if you want to guess what happened
to Bahia, it turns out he's one of the five that we talked about last night. If you
remember, we talked about last night as he was searching for a robe for a bowl and robes
to ordain with, he met our infamous cow, the cow that killed five, a number of people
in the Buddha's time. Bahia being one of them, gored him on her horn. But as I said,
it was supposedly not a cow. It was actually a demon in a cow form. Anyway, he died. And
the Buddha on his way back from alms around saw Bahia's body lying in a ditch or on the
side of the road and told the monks to take his body and to erect a stupa. And so they actually
call him Bahia Terra, which is interesting. He's an elder, even though he never ordained.
He's one of the 80 great disciples of the Buddha. He's considered to be the Buddha's chief
disciple in regards to development.
I thought it said here, probably does.
Ahia, preeminent among my disciples who are quick to learn the truth. So he was the one who
was able to learn the truth, the quickest, go from zero to Arahant in the shortest amount of time,
and the quickest. He was remarkable in that respect because he got very little teaching. Even
Sariputa heard a little bit of teaching and only became a sotapana. Kondaniya, when he heard
the Dhamma Chakapawat in the sotap, became a sotapana. Usually it would take a little bit more
teaching than this. But with Bahia, there was a brief teaching in the middle of the, in the middle
of the road. He was so dedicated that even though the short teaching, he was remarkable in that he
was able to become enlightened just by hearing. So the monks were surprised. They said, well,
how did he become an Arahant? They said, well, when I just taught him this teaching. And they said,
just with that short teaching, he became an Arahant. And again, the Buddha said, oh, no,
it's not about how much. It's not about much or little. And then he taught this verse. So has some
Pete. Jai Gata. You can teach someone a thousand Gata's, a thousand verses. And if they're useless,
no benefit. But one verse, if it's associated with the verse, it's worth a thousand verses,
thousand useless verses. So it's the quality, not the quantity. That's the verse. But what we have
to learn has much more to do with the teaching that he gave to Bahia. Because we've already talked
about this concept of expressed in the Dhamma Padavas. The teaching of Ditetitamat Dangbavi
Siti and just briefly, just to remark at how important that verse is. Because it describes the
essence of the practice of objectivity that we're trying to see things just as they are.
It's incomprehensible to most people. You think, well, that's ridiculous. Seeing, of course,
is just seeing. That's all it is. But that's only because we've never, if you've never practiced
in sight meditation, you've never actually explored the, in a, actually, really deeply come to
understand how the mind works. You know, we don't, so we don't connect our addictions, attachments,
our aversions, and fears, and worries, and so on, with the actual moment-to-moment interactions
of our mind, and that manner in which our mind interacts with reality. Once you have, it becomes clear
that really, that's the problem, is Ditetit is not just Ditetit. Ditetit is not just, what it's seen,
it's not just what it's seen. When you see something, you immediately make more of it than it
actually is. It becomes good, bad, me, mine, scary, a problem, troublesome, the goal becomes so
many different things. And we react accordingly, liking, disliking, becoming obsessed with it,
attached to it, clinging to it. So, really, the essence of our practice is to change that, to
change that habit, and in the process to see clearly that those things which we react to are not
worth reacting to. So, becoming naturally objective, to the point where naturally we see things
as they are, because we have no reason to see them as good or bad. You see that, because we see
that that doesn't help. You see that treating something is good or bad only gets you caught up in
problems. This is really essential to overcoming addiction, to overcoming aversion, to overcoming
depression and fear and anxiety. It's not to chase it away, not to get upset about it, but just
to see it as it is. To see every experience as it is when someone says something to you that you
might like or dislike. It's not the hearing that's the problem. If you just hear it, there's no problem,
it's the judging. Judging it as more than just sound, more than just words, more than just speech.
And then even when you like or dislike it, you judge the liking or disliking as meaningful,
you ascribe some extrinsic meaning to it. You say, oh, this is, I'd like this. Therefore,
I should chase after it. I dislike it. Therefore, I should tell that person to be quiet or go away.
So, the whole reason why we're practicing meditation is this. The whole essence of what we're
trying to accomplish with saying to ourselves, seeing or hearing, hearing is just this, that it
should just be what it is. Now, when you do this, it's going to be quite jarring in the beginning.
Why people are uncomfortable often practicing this meditation is because it's not comfortable.
It goes against our inclination to cling, to follow, to examine, to extrapolate, to continue.
And so, we've gotten into this rat of our habits. And when you start to break that up and just
say, seeing, seeing, it can be quite disruptive in the beginning.
But quickly, you should be able to see the difference. Once you get skilled at it,
which doesn't take very long, you should be able to see one moment where your mind is clear,
where you just see, seeing is just seeing. And you can tell the difference between that and our
and the alternative of attaching, of clinging, of making more of stings than they are.
This state of objectivity is clear, is calm, is pure. The mind is not solid, is not attached,
it's not suffering. The mind is at peace in that one moment. So, if you have pain and you see it
just as pain, you no longer suffer from the pain, just in that moment. When you hear something like,
see something, feel something that is unpleasant. When you say, when you experience it,
remind yourself and then you just see it as it is. You can see that it actually wasn't a problem
in the first place. With practice, this becomes our inclination, and eventually leads us to let go
completely and not cling to anything. And that is what leads us to true freedom. So, that is useful
to us, and great. This is a great example. It's a great, it's sort of an epic, one of those epic
stories that wouldn't become a movie in and of itself, I guess, but could be part of a movie about
the Buddha, the sort of vignette or this look at Bahia. That's a great story, especially the ending
where this very profound and concise teaching of the Buddha reminds us that seeing should just be
seeing, hearing should just be hearing. Feelings should just be hearing and thinking should be
just thinking. If you can do that, there will be no identifying, there will be no clinging. There
will be no suffering. So, that's the Dhammapada verse for today, and that's our teaching.
So, thank you all for tuning in, wish you all the best. Keep practicing.
