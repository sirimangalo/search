1
00:00:00,000 --> 00:00:09,720
Hi, this is a answer to a question on meditation and psychic diseases, mental diseases.

2
00:00:09,720 --> 00:00:15,800
This is a difficult question to answer in regards to how do we incorporate meditation into

3
00:00:15,800 --> 00:00:23,640
our lives when we've been diagnosed with a severe mental disturbance, you know, schizophrenia

4
00:00:23,640 --> 00:00:25,960
and so on.

5
00:00:25,960 --> 00:00:30,440
And the first thing I'd say is that I'm certainly not qualified to talk about such things.

6
00:00:30,440 --> 00:00:37,000
I don't really have any experience with people of this sort of disorder.

7
00:00:37,000 --> 00:00:46,840
I don't can't recall even a single person who I knew had such a disorder.

8
00:00:46,840 --> 00:00:51,320
Some cautious thoughts.

9
00:00:51,320 --> 00:00:56,600
I don't think it's something that should prevent you from meditating and I don't want

10
00:00:56,600 --> 00:01:02,960
to encourage you to meditate intensively because I don't know exactly what would happen,

11
00:01:02,960 --> 00:01:04,960
especially if you're not under a teacher.

12
00:01:04,960 --> 00:01:13,920
But I would say cautiously to approach the states that are arising when you feel dizzy,

13
00:01:13,920 --> 00:01:20,280
when you feel disoriented, when you feel strong emotions.

14
00:01:20,280 --> 00:01:26,200
Just as with everyone else, the meditation is there's only one practice that I would recommend

15
00:01:26,200 --> 00:01:30,080
and that's to acknowledge them and to try to understand them, to see them clear.

16
00:01:30,080 --> 00:01:37,960
And I think at least a sort of a slow process of this sort where you focus on the dizziness

17
00:01:37,960 --> 00:01:44,360
and say to yourself, does he dizzy and just try to see it for what it is and not cling

18
00:01:44,360 --> 00:01:49,800
to it, not make more of it than it is or not become worried or upset or stressed about it.

19
00:01:49,800 --> 00:01:58,840
I would say it's probably the attachment to these things or the encouragement or concern

20
00:01:58,840 --> 00:01:59,840
or worry or fear.

21
00:01:59,840 --> 00:02:06,480
All of the obsession over certain states that arise, that probably triggers mental instability

22
00:02:06,480 --> 00:02:14,160
or triggers a bout of mental instability.

23
00:02:14,160 --> 00:02:19,720
So just try to see things for what they are and that's the same advice I'd give to anyone

24
00:02:19,720 --> 00:02:28,040
and I don't think the meditation is something beyond the realm of someone who has been diagnosed

25
00:02:28,040 --> 00:02:29,640
with such a sickness.

26
00:02:29,640 --> 00:02:33,320
I'm not fond of diagnosis in general.

27
00:02:33,320 --> 00:02:43,520
I think they put labels where there's really a broad range of conditions that can come

28
00:02:43,520 --> 00:02:47,360
and go and so I wouldn't worry too much about that.

29
00:02:47,360 --> 00:02:53,240
I would acknowledge the state of your mind and be aware that you have to take it slowly

30
00:02:53,240 --> 00:02:59,160
and preferably be under a teacher, but if you can't be under a teacher, at least be honest

31
00:02:59,160 --> 00:03:02,160
with yourself and just see the things for what they are.

32
00:03:02,160 --> 00:03:04,840
Okay, so I hope that helps.

33
00:03:04,840 --> 00:03:20,440
If there's any more questions, keep them coming.

