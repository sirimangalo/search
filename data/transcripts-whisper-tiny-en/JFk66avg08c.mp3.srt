1
00:00:00,000 --> 00:00:10,000
Hi everyone. Another video log update here. A few things, mainly based on my last video update.

2
00:00:10,000 --> 00:00:19,000
First, the idea of the bus tour this year, we've had quite a few people respond from all over the East Coast, which is really neat.

3
00:00:19,000 --> 00:00:31,000
So, I've set up a preliminary sort of, it's a bit rough, but there's a Google map of all the destinations and possible destinations. We're putting in two categories.

4
00:00:31,000 --> 00:00:48,000
So, if you've already expressed some intention to set up some kind of event, like a talk or even a meditation course, in your area that, for us, for me to run,

5
00:00:48,000 --> 00:00:55,000
then I put you on the destination list. So, for sure, we'll try to get to those places that actually do set up such a thing.

6
00:00:55,000 --> 00:01:02,000
For those places where there are people who have just mentioned that they'd like to have us, but are not sure if they could send anything up.

7
00:01:02,000 --> 00:01:07,000
But, you know, if you're passing through kind of thing, I put on the possibilities.

8
00:01:07,000 --> 00:01:17,000
And I may have missed some. It's a little bit disorganized here. There's no computer, so I'm just running this all through a telephone, through a smartphone, actually.

9
00:01:17,000 --> 00:01:27,000
So, hopefully that works. You can check it out. The link will be in the description. You can also follow everything I do either on Facebook or Google Plus or on my web blog.

10
00:01:27,000 --> 00:01:39,000
I'm pretty well connected. So, yeah, that's great. Thanks, everyone, for responding and showing that it actually is something that people would like to see happen.

11
00:01:39,000 --> 00:01:52,000
So, let's see how it goes. It'll be an experiment anyway. And again, there's the website. You can go there and I've already put in some, just some bullet notes of the places that will potentially be going.

12
00:01:52,000 --> 00:02:07,000
And we'll be ending up in Florida near Tampa. So, if you're in that area, there's already some people in that area who have expressed interest in setting something up, and I'll probably be there for a few days, maybe even a week.

13
00:02:07,000 --> 00:02:21,000
Yeah, before heading back. So, yeah, if you're not on a list, and if you have a place that you'd like to see us come, head over to the website, jadica.ceremungalow.org. I'll put that in the description.

14
00:02:21,000 --> 00:02:30,000
And you can let us know if people have already used the website to do that, which shows that it works.

15
00:02:30,000 --> 00:02:41,000
So, that's the first thing. The second thing is the translation of my booklet. We've got ten translations, well, nine translations in the original English.

16
00:02:41,000 --> 00:02:50,000
So, I've got ten languages that we're going to put together as a set and offer to my teacher.

17
00:02:50,000 --> 00:02:57,000
Most of the people who have offered to translate are still underway and haven't been able to get it done in time, which is fine.

18
00:02:57,000 --> 00:03:02,000
Actually, I'd rather have a good translation and a quick translation. I think we all would.

19
00:03:02,000 --> 00:03:09,000
So, the Thai translation also, I'm going to have to go through it because it's probably because it's the only one I actually understand.

20
00:03:09,000 --> 00:03:16,000
And so, I feel the need to correct a lot of things that's probably going to take some time for me to get the time to do that.

21
00:03:16,000 --> 00:03:22,000
But we have a new Russian translation and a Dutch translation, apart from the original eight languages.

22
00:03:22,000 --> 00:03:31,000
They're ten and hopefully I'll get a picture of that or maybe even a video to show you of the box set that we'll be offering to my teacher.

23
00:03:31,000 --> 00:03:38,000
And, of course, you can download any of the ten languages from the website. I'll put that link in this video as well.

24
00:03:38,000 --> 00:03:48,000
In case you're just hearing about it now. Again, if there's a language that isn't on that webpage as potentially under translation,

25
00:03:48,000 --> 00:03:54,000
and you're ready to translate, ready to start translating into that language, please do let us know.

26
00:03:54,000 --> 00:04:08,000
And send me a note and I'll add you to the list and then you can send us the doc file or ODT file or something that we can edit and turn into a PDF and an EPUB to put on the website.

27
00:04:08,000 --> 00:04:17,000
So, that's great as well. It's been amazing to see how many languages, probably by the end we figure we'll get about 20 languages in total.

28
00:04:17,000 --> 00:04:24,000
It looks pretty good to get another ten at least. So, that's really neat to think of all those different languages.

29
00:04:24,000 --> 00:04:30,000
Another thing, the third thing that I wanted to talk about related to this is I'm planning to write a second book.

30
00:04:30,000 --> 00:04:37,000
Some people were asking about some teachings that were a little more advanced because obviously this booklet is just for beginners.

31
00:04:37,000 --> 00:04:43,000
So, the second, the sequel to the book would be sort of a map of the paths.

32
00:04:43,000 --> 00:04:51,000
I try to make it fairly general because again I don't want to go into great detail about those things in a meditator experiences,

33
00:04:51,000 --> 00:05:02,000
because without the active participation with a teacher, it can be not dangerous, at least, can lead you astray.

34
00:05:02,000 --> 00:05:08,000
And I don't want to lead people to think that a book can somehow take the place of a teacher.

35
00:05:08,000 --> 00:05:18,000
So, it'll be general, but I'll try to go through all the different stages and the concepts and principles that you have to keep in mind during the practice.

36
00:05:18,000 --> 00:05:30,000
Once you've begun to undertake the practice either intensively or day to day over months or years for those people who haven't had a chance to undertake an intensive course.

37
00:05:30,000 --> 00:05:42,000
So, look for that. Hopefully I'll have a couple of chapters up this year. Again, I'm studying this year, trying to get these poly exams that my teacher asked me to study for.

38
00:05:42,000 --> 00:05:54,000
So, be a little bit busy, but hopefully all these different projects will, some of them will actually come to fruit. I guess I can't say they all will, but that's hopefully at least some of it comes to fruit.

39
00:05:54,000 --> 00:06:05,000
Again, thanks everyone for the great support and response that you've given to all of these projects. Without that, none of this would happen.

40
00:06:05,000 --> 00:06:20,000
So, please keep up the appreciation, at least, let us know that you're using and taking part and benefiting from all of this stuff, just so I do keep doing it.

41
00:06:20,000 --> 00:06:34,000
And, show your support. If you want to support our organization, you're welcome to do that via our website or at least just let us know that you're out there and following along.

42
00:06:34,000 --> 00:06:46,000
Anyway, you can see that you are. It's a great thing to see. We've got millions of people or millions of views on YouTube and thousands of people following these videos, so it's quite encouraging.

43
00:06:46,000 --> 00:06:56,000
So, I'm wishing you all the best and please do keep it up, keep practicing, keep on the path and let's find peace, happiness and freedom from suffering together.

44
00:06:56,000 --> 00:07:20,000
Thanks, peace for all the best.

