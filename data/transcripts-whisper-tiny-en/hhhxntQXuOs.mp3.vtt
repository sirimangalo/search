WEBVTT

00:00.000 --> 00:04.280
Oh, welcome back to Ask A Moog.

00:04.280 --> 00:11.240
Next question, I've been studying the teachings and meditation for a few months now,

00:11.240 --> 00:16.280
but my partner doesn't like it and makes me feel bad like I'm doing something wrong as

00:16.280 --> 00:21.080
a person in general, even if I'm more compassionate towards others.

00:21.080 --> 00:30.080
Do you have any advice?

00:30.080 --> 00:34.080
Well, it's actually not that difficult of a problem.

00:34.080 --> 00:40.800
A worse problem would be if you actually believed your partner, which it doesn't sound

00:40.800 --> 00:47.200
like you do, and that may, you know, if so, if you're in doubt, then I would say that's

00:47.200 --> 00:56.120
much more important to address, whether you really think that the meditation is beneficial.

00:56.120 --> 01:03.360
And it sounds like you do, though I wouldn't say that the most important benefit is being

01:03.360 --> 01:08.000
compassionate towards others, because you have to be very compassionate towards yourself as

01:08.000 --> 01:09.160
well.

01:09.160 --> 01:16.480
The most important is to gain understanding about reality, and that really leads to compassion.

01:16.480 --> 01:22.480
And I suppose that it's probably implicit in what you're saying, that you've learned more

01:22.480 --> 01:28.320
and come to understand yourself better and understand, you know, what is a benefit, what

01:28.320 --> 01:32.160
is right, and what is proper, and what is wrong, and what is improper, and therefore you're

01:32.160 --> 01:38.480
acting in a much more proper way, or somewhat more proper, and therefore you're more

01:38.480 --> 01:44.920
interested in helping yourself and helping other people, which makes you more compassionate.

01:44.920 --> 01:49.640
So being able to see that benefit is really an important thing.

01:49.640 --> 01:55.560
So the question of whether to, the question of whether to listen to your partner, therefore

01:55.560 --> 02:03.760
it doesn't arise, the question you're asking is what to do about their feelings.

02:03.760 --> 02:07.000
And you should get that straight, because it will make you feel a lot better.

02:07.000 --> 02:10.680
You won't have to be concerned about what they say.

02:10.680 --> 02:15.520
If they're making you feel bad, then it means they're getting to you.

02:15.520 --> 02:22.040
It means you're still clinging somehow to their words and to their views and their ideas.

02:22.040 --> 02:26.120
If it's clear in your mind what is right and what is wrong, you have no one's able to

02:26.120 --> 02:28.800
make you feel good or bad.

02:28.800 --> 02:35.840
You are at peace with yourself, and you're happiness doesn't depend on other people or

02:35.840 --> 02:38.320
externalities.

02:38.320 --> 02:47.240
So what to do when someone else doesn't like what you, doesn't like what you know to

02:47.240 --> 02:50.680
be right and proper and useful and beneficial?

02:50.680 --> 02:56.440
Well, the easiest question, the easiest answer, and it's probably not the one you prefer

02:56.440 --> 03:01.040
is to leave your partner.

03:01.040 --> 03:07.600
And I think that's just so obvious, and it's probably not the answer you're looking for.

03:07.600 --> 03:14.280
But that would imply that somehow you have some other attachment to this person, because

03:14.280 --> 03:21.880
the meditation has great benefit to you in your life and it's bringing you some, well,

03:21.880 --> 03:26.360
it will bring you peace and happiness, whether you can see this or not, I'm not sure.

03:26.360 --> 03:32.080
But once you practice more, you can see how much benefit the meditation brings to you.

03:32.080 --> 03:42.120
So to stay with someone who doesn't agree with that or who believes the opposite, that

03:42.120 --> 03:49.640
it is actually hurting you or that it is making you a bad person or so on, doesn't make

03:49.640 --> 03:58.440
sense, because if you stay with them, it's going to conflict with your own benefit.

03:58.440 --> 04:02.320
So obviously there must be something else that there must be a clinging to this person or

04:02.320 --> 04:08.240
could be perhaps that there is some structural reason for being in a relationship in terms

04:08.240 --> 04:15.280
of stability and so on, or it could be to help that person, because that person needs

04:15.280 --> 04:17.320
you and so on.

04:17.320 --> 04:22.040
So there could be many reasons, none of which I'm aware of.

04:22.040 --> 04:32.440
So the more important question then is how to live with this person, how to live with

04:32.440 --> 04:40.040
a person who doesn't agree with what you know to be proper and beneficial.

04:40.040 --> 04:41.680
And there are many ways.

04:41.680 --> 04:48.760
I think the first one is sort of a compromise, I say leave this person, well, it doesn't

04:48.760 --> 04:56.280
have to be complete, often you can take time apart and you can distance yourself to some

04:56.280 --> 05:02.120
extent from the person, not dropping them, but you can take time out for yourself and say

05:02.120 --> 05:04.520
you want to be alone for some time.

05:04.520 --> 05:07.520
When you have time alone, then they don't see you, they're not aware of what you're

05:07.520 --> 05:10.000
doing and it doesn't upset them, what you do.

05:10.000 --> 05:14.520
If you have the ability to take time alone where you're not with this person, even though

05:14.520 --> 05:18.400
you're still in a relationship, then it can be a real halfway.

05:18.400 --> 05:23.400
I mean, it might lead to you breaking up, but it might also lead to you coming to some

05:23.400 --> 05:30.160
better understanding of each other's position and could even make the relationship stronger

05:30.160 --> 05:36.200
and more in line with what is truly right and beneficial.

05:36.200 --> 05:41.520
Now I'm guessing that it has something to do with religion because it usually does.

05:41.520 --> 05:47.520
If people believe that meditation makes you a bad person, then there's two reasons.

05:47.520 --> 05:54.280
One is it conflicts with their beliefs, their religion, or two, and this is probably not

05:54.280 --> 05:58.480
your case given that you are benefiting from it.

05:58.480 --> 06:04.640
The person who's practicing meditation is practicing it incorrectly and is giving rise

06:04.640 --> 06:09.800
to states that are disturbing.

06:09.800 --> 06:14.200
Some people, when they begin to practice meditation, they have the best of intentions and

06:14.200 --> 06:17.440
eventually they'll get good at it, but when they're not good at it, it can lead to

06:17.440 --> 06:20.120
great stress and conflict inside.

06:20.120 --> 06:24.120
As you start to learn how to deal with the brain, it's with the mind.

06:24.120 --> 06:30.160
It's like learning to drive a car, when learning to drive a manual transmission car.

06:30.160 --> 06:35.600
The people in the car are going to have to put up with a lot of jerking in the beginning.

06:35.600 --> 06:39.120
So that can cause conflict, but I would imagine that in your case it has something to do

06:39.120 --> 06:44.640
with religion or belief, or I don't know, it could even be that the person is a scientist

06:44.640 --> 06:53.480
and atheist, but I'm an atheist, but he's a person who, a secularist, I suppose, someone

06:53.480 --> 06:58.280
who doesn't believe in the existence of the mind, or the benefits of meditation and

06:58.280 --> 07:10.640
things you're being brainwashed and so on and so on, but this is, in this case you really

07:10.640 --> 07:14.600
have to, it's something that's really going to take time and it may never be

07:14.600 --> 07:15.600
sorted out.

07:15.600 --> 07:22.920
It may eventually mean that you have to part ways, but the best way to deal with this,

07:22.920 --> 07:29.960
to approach this, is to walk around the person, to practice around the person.

07:29.960 --> 07:37.360
You don't have to be sitting on a cushion in a silent room to be meditating.

07:37.360 --> 07:39.160
You can meditate in a chair.

07:39.160 --> 07:41.160
You can meditate whether there are other people in the room.

07:41.160 --> 07:46.720
You can meditate while there's noise, you can meditate anywhere at any time.

07:46.720 --> 07:51.600
It's ideal to have solitude, it's ideal to have quiet, it's ideal to be sitting across

07:51.600 --> 07:59.320
like it on a cushion, but none of these are absolutely necessary and so in a rather than

07:59.320 --> 08:04.320
bemoaning the fact that you're unable to pursue the ideal or trying to pursue the ideal

08:04.320 --> 08:10.960
and as a result bring in conflict with the people around you, you can incorporate it into

08:10.960 --> 08:12.760
your relationship with them.

08:12.760 --> 08:15.160
Well they are watching television.

08:15.160 --> 08:21.040
You can be sitting quietly meditating with your eyes, open with your eyes closed.

08:21.040 --> 08:28.000
You can be in another room, you can be doing anything or be in any sort of position.

08:28.000 --> 08:32.040
You don't have to make it obvious that you're meditating or you don't have to say to them,

08:32.040 --> 08:35.920
look, I need my half an hour now, could you please leave the room, could you please turn

08:35.920 --> 08:40.280
off the television, et cetera, et cetera?

08:40.280 --> 08:44.600
Because eventually you're going to realize that all of that is a part of your meditation.

08:44.600 --> 08:51.280
It's a part of the practice that we're following and eventually you'll be able to, if

08:51.280 --> 08:55.360
you're successful in the practice, you'll be able to deal with it all, you'll be able

08:55.360 --> 09:02.080
to overcome your versions and attachments to these things.

09:02.080 --> 09:06.080
You can live your life with this person, you can go, if suppose it's a religious thing

09:06.080 --> 09:09.440
and they want you to go to church and you don't go to church, you can go to church and

09:09.440 --> 09:14.480
while they're doing that thing you can sit and meditate and whatever.

09:14.480 --> 09:18.840
You know if they're singing their praises to God, you can sing your praises to God and watch

09:18.840 --> 09:25.320
your lips moving as you sing, watch the lips moving, feel the lips moving and just

09:25.320 --> 09:30.160
be aware of what's happening and you're standing and when you hear the sound hearing

09:30.160 --> 09:35.360
and you can even just mouth something and take it as a mouth meditation or whatever.

09:35.360 --> 09:43.200
I mean examples, we do walking and sitting meditation in this frustration but all of

09:43.200 --> 09:48.640
these are just examples, you can do meditation in any way, in any form.

09:48.640 --> 09:53.840
You can do driving meditation, whatever you do in life, if you're the cook in the family

09:53.840 --> 10:00.840
you can do cooking meditation, if you're working in an office job you can do office

10:00.840 --> 10:08.840
meditation or you can take time out of your work to do five minutes, try to work around

10:08.840 --> 10:13.440
the person so that they're not even aware that you're meditating.

10:13.440 --> 10:20.680
That's much better because what's really going to change them is the strength in your mind.

10:20.680 --> 10:26.200
Once your mind becomes strong, once you become sure, there's no way, especially if they're

10:26.200 --> 10:31.240
a person who can't see the benefit of meditation, who are so blind that they're unable

10:31.240 --> 10:34.200
to see the benefits of it.

10:34.200 --> 10:38.720
There's no way that they can fight, again there's no way that they can compete against

10:38.720 --> 10:39.720
your strength.

10:39.720 --> 10:45.720
You have the strength of mind because you're practicing every day to strengthen your

10:45.720 --> 10:52.320
mind and to clarify your mind and they're at them, what are they doing?

10:52.320 --> 10:58.600
Their mind will constantly be wavering and it may not be that case right now and maybe

10:58.600 --> 11:02.440
that they have the strength and you don't and therefore you're wavering and you're not

11:02.440 --> 11:07.360
sure what to do but that's the goal.

11:07.360 --> 11:11.800
If you can get to the point where your mind doesn't waver, then they will have to capitulate,

11:11.800 --> 11:15.920
eventually they will realize, they will come up in their mind and they'll realize the

11:15.920 --> 11:21.280
wrongness of their beliefs and their ideas and eventually they'll even become interested

11:21.280 --> 11:25.560
in the meditation because they'll see how much strength, confidence, and peace brings

11:25.560 --> 11:27.720
to you.

11:27.720 --> 11:31.800
No matter what their religious views are, so I mean that's the deal is that views and opinions

11:31.800 --> 11:39.280
and beliefs are a source of strength in a sense and so you have to get quite powerful

11:39.280 --> 11:45.400
to be able to overcome those views which will eventually, you know, they don't jive

11:45.400 --> 11:50.800
when they don't jive with reality, that person will have to let go but it can take time.

11:50.800 --> 11:55.800
People can hold on to, it's amazing the beliefs people can hold on to even in the face

11:55.800 --> 12:01.640
even when those beliefs fly directly in the face of reality.

12:01.640 --> 12:11.440
So good luck and the most obvious answer is to always try to surround yourself with people

12:11.440 --> 12:16.520
who are meditating, this is a very important part of the Buddhist teaching to surround

12:16.520 --> 12:20.760
yourself with people who are interested in meditation, who are meditating, who believe in

12:20.760 --> 12:29.400
the benefits and see the benefits of meditation practice and always strive to avoid people

12:29.400 --> 12:32.680
who don't see the benefits.

12:32.680 --> 12:38.920
You know it's basically choosing people who can see whose beliefs and opinions don't fly

12:38.920 --> 12:48.560
in the face of reality, who don't conflict with the truth, try to stick only to people

12:48.560 --> 12:54.440
who's understanding and beliefs and views are in line with reality, because meditation

12:54.440 --> 13:00.440
is a great thing and anyone who believes others, otherwise, is missing something.

13:00.440 --> 13:06.800
So this is an answer to your question, this has been another episode of Ask a Monk, wishing

13:06.800 --> 13:31.920
you all this happiness and freedom from suffering.

