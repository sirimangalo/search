1
00:00:00,000 --> 00:00:07,000
Okay, good morning.

2
00:00:07,000 --> 00:00:16,000
I'm not casting here from the Elkson again.

3
00:00:16,000 --> 00:00:44,000
I'm going to talk something on the indie part.

4
00:00:44,000 --> 00:00:59,000
It means power or strength or

5
00:00:59,000 --> 00:01:06,000
might, power and strength.

6
00:01:06,000 --> 00:01:12,000
Sometimes translated as success or dominance.

7
00:01:12,000 --> 00:01:25,000
Pada means the path or the steps that lead to something.

8
00:01:25,000 --> 00:01:33,000
So these are often translated as the four.

9
00:01:33,000 --> 00:01:47,000
It means of succeeding and bringing something to its culmination.

10
00:01:47,000 --> 00:02:03,000
To fruit, to bearing to having our work bare fruit.

11
00:02:03,000 --> 00:02:16,000
And so the most important aspect of these is how we apply them to our practice.

12
00:02:16,000 --> 00:02:35,000
We apply these in numbers in order to succeed in our practice.

13
00:02:35,000 --> 00:02:48,000
But another useful way of looking at them is in terms of how the practice helps us succeed.

14
00:02:48,000 --> 00:03:02,000
So how meditation practice actually helps us cultivate these numbers.

15
00:03:02,000 --> 00:03:12,000
It's helpful because sometimes we lose confidence in the practice.

16
00:03:12,000 --> 00:03:19,000
Find it hard to remember why we're torturing ourselves.

17
00:03:19,000 --> 00:03:32,000
We have a hard time sometimes building up the interest in the practice.

18
00:03:32,000 --> 00:03:40,000
So it's often good to talk about how the practice helps us in our lives and spiritually

19
00:03:40,000 --> 00:03:46,000
how it leads us to greater things.

20
00:03:46,000 --> 00:03:55,000
So we'll look at these two ways in terms of how the four idipada helps us practice.

21
00:03:55,000 --> 00:04:06,000
It also in terms of how the practice brings us closer to idi, closer to power or success.

22
00:04:06,000 --> 00:04:20,000
Those are just two, making sure our lives bare fruit are fruitful.

23
00:04:20,000 --> 00:04:35,000
So the first idipada is chanda, chanda which means desire or interest.

24
00:04:35,000 --> 00:04:38,000
And so it can be used in a bad way.

25
00:04:38,000 --> 00:04:49,000
We have something called chanda which means sensual desire which is in Buddhism of course

26
00:04:49,000 --> 00:04:59,000
considered to be an unhelpful, harmful.

27
00:04:59,000 --> 00:05:12,000
But then there's a dhamma tanda which is desire for truth.

28
00:05:12,000 --> 00:05:16,000
And so desire isn't a very good translation exactly.

29
00:05:16,000 --> 00:05:31,000
That's an interest or zeal.

30
00:05:31,000 --> 00:05:36,000
I guess colloquially we have to use the word desire.

31
00:05:36,000 --> 00:05:48,000
It's important to understand it as an attachment to meditating.

32
00:05:48,000 --> 00:05:58,000
It's the genuine desire to better oneself.

33
00:05:58,000 --> 00:06:07,000
And so this is the first means of succeeding in dhamma and in life.

34
00:06:07,000 --> 00:06:13,000
You need to be interested in what you're doing and what you're doing to succeed.

35
00:06:13,000 --> 00:06:22,000
I think that's difficult in inside meditation because it's not very pleasant sometimes.

36
00:06:22,000 --> 00:06:28,000
And also because there's nothing to cling to because meditation is about letting go.

37
00:06:28,000 --> 00:06:35,000
There's nothing during the practice that you can cling to and say yes, that's what I want.

38
00:06:35,000 --> 00:06:39,000
It's all about realizing that the things that you want are not worth clinging to.

39
00:06:39,000 --> 00:06:54,000
But it's a dhamma, nala nana, bhini, vaiya. No dhamma is worth clinging to.

40
00:06:54,000 --> 00:06:55,000
Nothing's worth clinging to.

41
00:06:55,000 --> 00:07:00,000
Not even meditation practice, not even goodness, nothing.

42
00:07:00,000 --> 00:07:10,000
Clinging isn't the past to happen.

43
00:07:10,000 --> 00:07:18,000
And so it's quite difficult as I said to cultivate this desire for the meditation.

44
00:07:18,000 --> 00:07:26,000
This is why the word desire is kind of unhelpful here because even wanting to meditate,

45
00:07:26,000 --> 00:07:34,000
usually misleading, misled, when someone wants to meditate, it's usually for the wrong reasons.

46
00:07:34,000 --> 00:07:40,000
Because wanting doesn't help you in your meditation.

47
00:07:40,000 --> 00:07:54,000
But the interest, the content and the zest, zeal for meditation, this is important.

48
00:07:54,000 --> 00:08:03,000
And we gain this in different ways through reminding ourselves of the benefits of meditation.

49
00:08:03,000 --> 00:08:07,000
We gain it through the meditation practice itself.

50
00:08:07,000 --> 00:08:16,000
You're seeing, because meditation leads you on, the truth leads you to greater truth.

51
00:08:16,000 --> 00:08:25,000
As you see, the truth encourages you.

52
00:08:25,000 --> 00:08:30,000
Through thinking about the Buddha, this gives encouragement about the example,

53
00:08:30,000 --> 00:08:36,000
through surrounding yourself with people who are good examples.

54
00:08:36,000 --> 00:08:41,000
People who remind you of the path and the goal.

55
00:08:41,000 --> 00:08:45,000
People who set an example for you.

56
00:08:45,000 --> 00:08:51,000
Through listening to the Dhamma, through studying the Dhamma, all of these things.

57
00:08:51,000 --> 00:09:04,000
Through thinking about death and the inevitability of danger and the dangers and the troubles that await us.

58
00:09:04,000 --> 00:09:14,000
Especially if we're not prepared.

59
00:09:14,000 --> 00:09:21,000
But it's important to recognize this and to recognize our discontent with the practice and to work through that.

60
00:09:21,000 --> 00:09:26,000
In fact, the discontent is probably more important, ultimately.

61
00:09:26,000 --> 00:09:29,000
Because ultimately, there's no desire for the practice.

62
00:09:29,000 --> 00:09:38,000
There's only a sort of sense of settling into a mindful state, which comes with getting rid of discontent.

63
00:09:38,000 --> 00:09:44,000
And so we have to work on this discontent, which is in two ways.

64
00:09:44,000 --> 00:09:52,000
There's the discontent with the practice because it's unpleasant, because it shows us things we'd rather not look at.

65
00:09:52,000 --> 00:09:56,000
And discontent based on desire, because we want something else.

66
00:09:56,000 --> 00:10:00,000
We want pleasures, we want an entertainment.

67
00:10:00,000 --> 00:10:11,000
So examining those, reminding ourselves, liking, wanting, wanting, and being able to see through them.

68
00:10:11,000 --> 00:10:22,000
To see that they're not actually satisfying what they can't actually make us happy, bring us through happiness.

69
00:10:22,000 --> 00:10:26,000
To see them from what they are is something that arises in the system.

70
00:10:26,000 --> 00:10:28,000
And let them go.

71
00:10:28,000 --> 00:10:37,000
And the mind settles into this sort of contentment, which is a sort of chandelion in itself.

72
00:10:37,000 --> 00:10:40,000
Anyway, we have to be careful to overcome discontent.

73
00:10:40,000 --> 00:10:45,000
But the great thing about meditation is that it brings us...

74
00:10:45,000 --> 00:10:47,000
It brings us chandelion.

75
00:10:47,000 --> 00:10:49,000
It brings us content...

76
00:10:49,000 --> 00:10:51,000
The right kind of desire.

77
00:10:51,000 --> 00:10:57,000
It refines our desires.

78
00:10:57,000 --> 00:11:04,000
So if you look at the nature of a human being when their children...

79
00:11:04,000 --> 00:11:09,000
As children, we have very coarse desires by ourselves,

80
00:11:09,000 --> 00:11:12,000
pleased by simple things.

81
00:11:12,000 --> 00:11:18,000
Bright colors and loud noises.

82
00:11:18,000 --> 00:11:20,000
And that changes over time.

83
00:11:20,000 --> 00:11:25,000
And why it changes is because our sense of pleasure and happiness,

84
00:11:25,000 --> 00:11:27,000
our desires become more refined.

85
00:11:27,000 --> 00:11:30,000
It becomes refined through experience.

86
00:11:30,000 --> 00:11:37,000
We come to realize that the happiness that we find as a child

87
00:11:37,000 --> 00:11:40,000
from bright lights and colors and sounds...

88
00:11:40,000 --> 00:11:43,000
It's actually quite coarse and disturbing.

89
00:11:43,000 --> 00:11:48,000
It's tiring, laughing all the time, giggling, screaming.

90
00:11:48,000 --> 00:11:51,000
All of these things are tiresome.

91
00:11:51,000 --> 00:11:55,000
So we lose interest in them very quickly.

92
00:11:55,000 --> 00:11:59,000
Part of the process of growing up is we're finding one's desires.

93
00:11:59,000 --> 00:12:04,000
And then you have teenagers who get interested in all sorts of drugs

94
00:12:04,000 --> 00:12:07,000
and entertainment, loud music and so on.

95
00:12:07,000 --> 00:12:10,000
And that changes.

96
00:12:10,000 --> 00:12:15,000
And children look at adults and think of them as kind of boring and dull.

97
00:12:15,000 --> 00:12:18,000
But a lot of that is because they've seen through it

98
00:12:18,000 --> 00:12:24,000
and realized how stressful it actually is to seek out entertainment

99
00:12:24,000 --> 00:12:25,000
on the time.

100
00:12:25,000 --> 00:12:27,000
So they refine their desires.

101
00:12:27,000 --> 00:12:28,000
This goes on through life.

102
00:12:28,000 --> 00:12:29,000
It's not the only process.

103
00:12:29,000 --> 00:12:33,000
There's a lot of repression that goes on as well.

104
00:12:33,000 --> 00:12:36,000
Part of it is the refinement of desires.

105
00:12:36,000 --> 00:12:40,000
And so in a way, meditation is just an extension of that

106
00:12:40,000 --> 00:12:43,000
or an acceleration of that process.

107
00:12:43,000 --> 00:12:49,000
Because in meditation, you clearly come to refine your ambitions

108
00:12:49,000 --> 00:12:53,000
and your desires to the point where you're kind of

109
00:12:53,000 --> 00:12:58,000
so many things that ordinary people think would bring happiness.

110
00:12:58,000 --> 00:13:01,000
And you start to realize that that is only a cause for more stress

111
00:13:01,000 --> 00:13:03,000
and suffering.

112
00:13:03,000 --> 00:13:05,000
So you give it up.

113
00:13:05,000 --> 00:13:14,000
In favor of more peaceful and more truly happy pursuits.

114
00:13:14,000 --> 00:13:16,000
You have a more simple life.

115
00:13:16,000 --> 00:13:20,000
You start giving away your possessions, not out of repression

116
00:13:20,000 --> 00:13:26,000
but because you realize how stressful it is to hold on to them.

117
00:13:26,000 --> 00:13:29,000
So this is how happiness brings about.

118
00:13:29,000 --> 00:13:36,000
And purifies your desires.

119
00:13:36,000 --> 00:13:38,000
The second one is really at.

120
00:13:38,000 --> 00:13:40,000
And I talked about this yesterday.

121
00:13:40,000 --> 00:13:45,000
I mentioned how important really it is.

122
00:13:45,000 --> 00:13:48,000
In order to succeed in anything you have to work at it.

123
00:13:48,000 --> 00:13:52,000
Meditation is no exception.

124
00:13:52,000 --> 00:13:55,000
Meditation isn't just about relaxing.

125
00:13:55,000 --> 00:13:58,000
There's a lot of tweaking that has to go on.

126
00:13:58,000 --> 00:14:03,000
Refining your practice doesn't mean just letting it go naturally.

127
00:14:03,000 --> 00:14:08,000
This is a mistake that so many people make in meditation.

128
00:14:08,000 --> 00:14:12,000
Thinking that if you just relax, it will all work itself out.

129
00:14:12,000 --> 00:14:15,000
But it obviously doesn't.

130
00:14:15,000 --> 00:14:18,000
Otherwise, we'll be enlightened by now.

131
00:14:18,000 --> 00:14:25,000
Because our natural way of processing things is to file this unwholesome.

132
00:14:25,000 --> 00:14:34,000
It's based on clinging, it's based on identification and so on.

133
00:14:34,000 --> 00:14:42,000
And so we have to work just to get to a state of normal.

134
00:14:42,000 --> 00:14:45,000
Just to get to a balanced state state.

135
00:14:45,000 --> 00:14:48,000
We have to do a lot of tweaking.

136
00:14:48,000 --> 00:14:53,000
It's like a tree leaning in one direction.

137
00:14:53,000 --> 00:14:57,000
If you just let it fall, it's going to fall in that direction.

138
00:14:57,000 --> 00:15:00,000
The problem is our minds are inclined in the wrong direction.

139
00:15:00,000 --> 00:15:02,000
So you can't just let them fall.

140
00:15:02,000 --> 00:15:12,000
You have to work hard to pull our minds in another direction.

141
00:15:12,000 --> 00:15:15,000
There's a lot of work that has to be done to do that.

142
00:15:15,000 --> 00:15:22,000
It doesn't have to be stressful exactly, but there should be a sense of work

143
00:15:22,000 --> 00:15:25,000
until our minds are inclined in the right direction.

144
00:15:25,000 --> 00:15:33,000
And then it's much more pleasant and peaceful.

145
00:15:33,000 --> 00:15:37,000
Once the mind gets on the right path, then it's just the same.

146
00:15:37,000 --> 00:15:43,000
But until that time you have to be clear in terms of

147
00:15:43,000 --> 00:15:51,000
realigning the mind.

148
00:15:51,000 --> 00:15:54,000
But meditation also brings effort.

149
00:15:54,000 --> 00:15:56,000
So the good meditation does for you.

150
00:15:56,000 --> 00:16:01,000
It helps us succeed in life because we have less dragging us down.

151
00:16:01,000 --> 00:16:04,000
All of these course desires are eliminated.

152
00:16:04,000 --> 00:16:07,000
So we have much more energy.

153
00:16:07,000 --> 00:16:12,000
They talk about meditators being zombies, but it's so far from the truth.

154
00:16:12,000 --> 00:16:20,000
The zombies are the ones that seek out pleasure and spend so much of their energy.

155
00:16:20,000 --> 00:16:24,000
On mindless pursuits.

156
00:16:24,000 --> 00:16:34,000
They end up being very dull and uninspired.

157
00:16:34,000 --> 00:16:39,000
Or someone who has clear meditation, they find they have great energy.

158
00:16:39,000 --> 00:16:44,000
And they're able to do whatever they'd like to accomplish.

159
00:16:44,000 --> 00:16:49,000
And they're able to accomplish great things because of their energy and their effort.

160
00:16:49,000 --> 00:16:55,000
That they are reserving, preserving through mindfulness.

161
00:16:55,000 --> 00:16:57,000
Just being mindful itself.

162
00:16:57,000 --> 00:17:00,000
Preserve so much energy.

163
00:17:00,000 --> 00:17:05,000
Because all the stress from liking and disliking and getting caught up in things.

164
00:17:05,000 --> 00:17:07,000
You can see that when you meditate.

165
00:17:07,000 --> 00:17:14,000
It wears you out and drags you down.

166
00:17:14,000 --> 00:17:19,000
This is the media.

167
00:17:19,000 --> 00:17:20,000
The third one, jitta.

168
00:17:20,000 --> 00:17:23,000
Jitta means to focus your mind.

169
00:17:23,000 --> 00:17:29,000
Keeping the task in mind.

170
00:17:29,000 --> 00:17:35,000
Keeping your mind on the object, on the task.

171
00:17:35,000 --> 00:17:38,000
So this is the immeditation.

172
00:17:38,000 --> 00:17:45,000
This is the aspect of keeping our focus on mindfulness, reminding ourselves again and again.

173
00:17:45,000 --> 00:17:54,000
The ability to be mindful to not forget yourself and realize that you've been eating

174
00:17:54,000 --> 00:17:59,000
or talking, walking or whatever.

175
00:17:59,000 --> 00:18:02,000
Even just sitting and not being mindful.

176
00:18:02,000 --> 00:18:04,000
The ability to catch yourself.

177
00:18:04,000 --> 00:18:14,000
And actually be mindful of the experience as they occur.

178
00:18:14,000 --> 00:18:16,000
This is something that grows over time.

179
00:18:16,000 --> 00:18:18,000
This is why we come back and do courses.

180
00:18:18,000 --> 00:18:20,000
This is why we try to do a daily practice.

181
00:18:20,000 --> 00:18:26,000
This is why we insist that meditators try to be mindful even when they're not practicing.

182
00:18:26,000 --> 00:18:29,000
It has to become habitual.

183
00:18:29,000 --> 00:18:36,000
And eventually it does, eventually it seeps into your mind and your brain.

184
00:18:36,000 --> 00:18:42,000
It becomes part of your life so that some people even get so that they dream mindfully.

185
00:18:42,000 --> 00:18:48,000
Not exactly mindfully, but they're repeating things in their dreams.

186
00:18:48,000 --> 00:18:55,000
Not mindfully, but it gets so drilled into them.

187
00:18:55,000 --> 00:19:04,000
It's only really in the beginning when it's still only a preliminary state.

188
00:19:04,000 --> 00:19:14,000
But the ability to recognize when you get angry, to catch it, to realize that it has a correct response

189
00:19:14,000 --> 00:19:17,000
is to say angry and go to remind yourself.

190
00:19:17,000 --> 00:19:24,000
When you want something to catch yourself and not run after.

191
00:19:24,000 --> 00:19:35,000
Obviously, to succeed in anything you need to keep it in mind and with meditations is equally true.

192
00:19:35,000 --> 00:19:41,000
So try even when you're not meditating, try to be mindful in between walking and sitting.

193
00:19:41,000 --> 00:19:47,000
Catch yourself as you're walking and as you sit down and so on.

194
00:19:47,000 --> 00:19:53,000
And as soon as you finish meditation, try to set yourself for the rest of the day.

195
00:19:53,000 --> 00:20:10,000
But meditation has, from the flip side, meditation actually helps our focus, of course.

196
00:20:10,000 --> 00:20:14,000
We're able to keep our minds on things, we can get less distracted.

197
00:20:14,000 --> 00:20:19,000
The great benefits of meditation is the ability to think clearer.

198
00:20:19,000 --> 00:20:22,000
The ability to solve problems better.

199
00:20:22,000 --> 00:20:30,000
To focus on work more sincerely, more greater dedication.

200
00:20:30,000 --> 00:20:34,000
To complete tasks with greater efficiency.

201
00:20:34,000 --> 00:20:36,000
Because our minds are clear.

202
00:20:36,000 --> 00:20:40,000
There's less garbage piling up.

203
00:20:40,000 --> 00:20:42,000
Getting into it.

204
00:20:42,000 --> 00:20:45,000
And we're able to clear it out.

205
00:20:45,000 --> 00:20:52,000
As it comes up and file things better, our minds become more sorted and more organized.

206
00:20:52,000 --> 00:20:55,000
Our memories improve.

207
00:20:55,000 --> 00:21:02,000
And so on.

208
00:21:02,000 --> 00:21:03,000
This is Jita.

209
00:21:03,000 --> 00:21:12,000
Jita is something that improves with meditation, something that improves our meditation.

210
00:21:12,000 --> 00:21:15,000
Number four is Mungsa.

211
00:21:15,000 --> 00:21:20,000
Mungsa is a complement to Jita.

212
00:21:20,000 --> 00:21:22,000
Jita means to keep things in mind.

213
00:21:22,000 --> 00:21:25,000
But Mungsa means to adjust.

214
00:21:25,000 --> 00:21:29,000
Mungsa means discernment.

215
00:21:29,000 --> 00:21:32,000
So it's not enough in any work.

216
00:21:32,000 --> 00:21:36,000
In most works, to just plow ahead on thinking.

217
00:21:36,000 --> 00:21:40,000
This is often a mistake people make in many realms.

218
00:21:40,000 --> 00:21:44,000
They just work without discernment.

219
00:21:44,000 --> 00:21:50,000
But even in menial tasks, even in physical work, physical labor.

220
00:21:50,000 --> 00:21:55,000
You can see the difference between someone who is discerning and someone who is not.

221
00:21:55,000 --> 00:22:00,000
The discerning person will be able to adjust their efforts.

222
00:22:00,000 --> 00:22:10,000
We'll be able to retrain their bodies to refine their effort.

223
00:22:10,000 --> 00:22:19,000
So Mungsa is being able to step back and observe your work, your practice, and adjust.

224
00:22:19,000 --> 00:22:22,000
It's wisdom.

225
00:22:22,000 --> 00:22:24,000
Not just plowing ahead in meditation.

226
00:22:24,000 --> 00:22:27,000
This is clearly not clearly.

227
00:22:27,000 --> 00:22:30,000
This is another mistake that people make.

228
00:22:30,000 --> 00:22:34,000
They plow ahead trying to do as many hours a day as they can.

229
00:22:34,000 --> 00:22:37,000
It's great if you can do lots and lots of hours of meditation.

230
00:22:37,000 --> 00:22:43,000
But it's quite possible to do lots and lots of hours of not meditating.

231
00:22:43,000 --> 00:22:47,000
Fake meditating.

232
00:22:47,000 --> 00:22:49,000
Just walking and sitting without benefit.

233
00:22:49,000 --> 00:22:51,000
And so you have to be able to adjust.

234
00:22:51,000 --> 00:22:57,000
That's why I said yesterday the ability to go into the next level, always the next level.

235
00:22:57,000 --> 00:23:03,000
Not to be content or not to be stuck on one level of ability.

236
00:23:03,000 --> 00:23:13,000
So you create, you cultivate a certain level of ability and then you think of that as sufficient.

237
00:23:13,000 --> 00:23:16,000
But the mind will constantly be surprising.

238
00:23:16,000 --> 00:23:20,000
You constantly have to adapt.

239
00:23:20,000 --> 00:23:23,000
Because it's really, these are the knots.

240
00:23:23,000 --> 00:23:24,000
They're not.

241
00:23:24,000 --> 00:23:27,000
They're what we've always been stuck on.

242
00:23:27,000 --> 00:23:29,000
They're the definition of problems.

243
00:23:29,000 --> 00:23:34,000
They're the sticking points in our mind.

244
00:23:34,000 --> 00:23:36,000
They are what confuses us.

245
00:23:36,000 --> 00:23:41,000
What does what challenges us?

246
00:23:41,000 --> 00:23:44,000
That's what we're focusing on.

247
00:23:44,000 --> 00:23:47,000
So the very definition of these things.

248
00:23:47,000 --> 00:23:51,000
The very nature of these things is going to challenge us.

249
00:23:51,000 --> 00:23:52,000
That's what they are.

250
00:23:52,000 --> 00:23:56,000
They're the challenges in our mind.

251
00:23:56,000 --> 00:23:59,000
We can't just plow through.

252
00:23:59,000 --> 00:24:02,000
We have to be discerning.

253
00:24:02,000 --> 00:24:05,000
In practice, this just means truly being mindful.

254
00:24:05,000 --> 00:24:10,000
Truly being, it's an aspect of mindfulness to clearly understand the experience.

255
00:24:10,000 --> 00:24:17,000
Some pajanya, to clearly see the experience.

256
00:24:17,000 --> 00:24:23,000
Don't get complacent and think you already know the experience.

257
00:24:23,000 --> 00:24:26,000
Every experience would be something.

258
00:24:26,000 --> 00:24:29,000
There's a aspect to it that is challenging.

259
00:24:29,000 --> 00:24:34,000
That's why it still comes up.

260
00:24:34,000 --> 00:24:41,000
Always look at every experience with a fresh mind trying to understand it.

261
00:24:41,000 --> 00:24:45,000
Trying to see it clearly as though it was something you'd never seen before.

262
00:24:45,000 --> 00:24:49,000
You'll see things that you've never really noticed before.

263
00:24:49,000 --> 00:24:53,000
It works.

264
00:24:53,000 --> 00:24:56,000
The discernment is an important part of meditation.

265
00:24:56,000 --> 00:25:00,000
Meditation also leads to discernment in our lives.

266
00:25:00,000 --> 00:25:14,000
In our spiritual path, in terms of spiritual well-being, discernment is our ability to solve our problems.

267
00:25:14,000 --> 00:25:18,000
It's amazing how quick the mind is able to fix things.

268
00:25:18,000 --> 00:25:28,000
The mind is clear you're able to solve problems that other people become totally crippled by.

269
00:25:28,000 --> 00:25:35,000
You're able to see through all of the delusion and illusion to see the truth of the situation.

270
00:25:35,000 --> 00:25:46,000
To realize the nature and the suspicion of the problem.

271
00:25:46,000 --> 00:25:53,000
You're able to put many pieces together in the mind in terms of worldly success.

272
00:25:53,000 --> 00:26:00,000
It's very easy to succeed in business and it's easy to build things, to create things.

273
00:26:00,000 --> 00:26:04,000
The mind works much clearer and much more efficiently.

274
00:26:04,000 --> 00:26:13,000
But it's also much more clever in terms of seeing the big picture and being able to work towards goals and so on.

275
00:26:13,000 --> 00:26:19,000
Life becomes a lot easier to live through the meditation practice.

276
00:26:19,000 --> 00:26:28,000
Because the mind becomes stronger and quicker, sharper, more clever.

277
00:26:28,000 --> 00:26:35,000
So on one side this is an admonishment to cultivate these four things for our practice.

278
00:26:35,000 --> 00:26:41,000
But on the other side it's encouragement that the practice is cultivating these things.

279
00:26:41,000 --> 00:26:44,000
Practice actually has a benefit.

280
00:26:44,000 --> 00:26:50,000
That's great benefit in our lives for those who are struggling with this.

281
00:26:50,000 --> 00:26:53,000
So that's it, I'm not sure today.

282
00:26:53,000 --> 00:26:55,000
Thank you for tuning in.

283
00:26:55,000 --> 00:27:18,000
Practice.

