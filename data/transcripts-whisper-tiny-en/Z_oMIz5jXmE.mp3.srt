1
00:00:00,000 --> 00:00:02,500
Alright, so that's what we are talking about.

2
00:00:03,800 --> 00:00:10,500
If I remember correctly in Asota, it's right in the King of Kui Tae Buddha

3
00:00:10,500 --> 00:00:14,000
when he sees a group of monks with the Buddha in the group.

4
00:00:16,300 --> 00:00:19,800
So it would seem that the Buddha had no extraordinary appearance,

5
00:00:19,800 --> 00:00:22,500
except for being peaceful and serene, etc.

6
00:00:22,500 --> 00:00:28,500
Yeah, I think Tanasaro brings up this argument.

7
00:00:29,500 --> 00:00:35,500
I'm not convinced, but then I'm always ready to contradict and fellow monk.

8
00:00:38,500 --> 00:00:41,500
So it's kind of evil, isn't it?

9
00:00:41,500 --> 00:00:47,500
But let's consider for a second that this is really just a little bit speculative.

10
00:00:47,500 --> 00:00:54,500
The king in question, I believe, is, I'm going to embarrass myself, was it a Jata-sattu?

11
00:00:54,500 --> 00:00:56,500
I think it was.

12
00:00:56,500 --> 00:00:59,500
It was either a Jata-sattu or it was King Pasainadi.

13
00:01:00,500 --> 00:01:03,500
And either way, they were a little bit foolish.

14
00:01:03,500 --> 00:01:05,500
A Jata-sattu killed his father.

15
00:01:05,500 --> 00:01:11,500
Pasainadi was just a little bit silly and not totally wholesome.

16
00:01:11,500 --> 00:01:20,500
And so it's no wonder that they couldn't tell the difference between the Buddha.

17
00:01:20,500 --> 00:01:30,500
The other thing is, as I understand, the commentary actually explains away that passage by saying,

18
00:01:31,500 --> 00:01:37,500
when he asks, who was the Buddha, it's not that he didn't recognize the Buddha.

19
00:01:37,500 --> 00:01:41,500
The commentary does go into detail with this. It's not that he didn't recognize the Buddha.

20
00:01:41,500 --> 00:01:43,500
How could you not recognize the Buddha?

21
00:01:43,500 --> 00:01:49,500
How could you say, you know, this is the...

22
00:01:49,500 --> 00:01:55,500
How could you just think of him as just another one of the monks?

23
00:01:55,500 --> 00:01:59,500
But he was saying that because he wanted to allow...

24
00:01:59,500 --> 00:02:05,500
That's right, he wanted to allow Gvaka, who was one of the ministers to King Pasainadi.

25
00:02:05,500 --> 00:02:10,500
He wanted to give him a chance to extoll the virtues of the Buddha.

26
00:02:10,500 --> 00:02:12,500
And it gives an interesting...

27
00:02:12,500 --> 00:02:16,500
Because it gives an interesting backing to this whole conversation that went on,

28
00:02:16,500 --> 00:02:22,500
about how he asked, who should we go to see this evening?

29
00:02:22,500 --> 00:02:24,500
He wanted to go see the Buddha.

30
00:02:24,500 --> 00:02:28,500
But he knew that he had killed his father who was a disciple of the Buddha,

31
00:02:28,500 --> 00:02:31,500
and that if he would just went and said, we want to see the Buddha,

32
00:02:31,500 --> 00:02:39,500
they'd be like, you know, you killed his father, one of his slaves,

33
00:02:39,500 --> 00:02:40,500
but there's something like that.

34
00:02:40,500 --> 00:02:43,500
So he wanted Gvaka to bring it up.

35
00:02:43,500 --> 00:02:46,500
He killed his father who was a disciple of the Buddha,

36
00:02:46,500 --> 00:02:49,500
and that if he would just pray, and said, we want to see the Buddha,

37
00:02:49,500 --> 00:02:50,500
then he'd be like...

38
00:02:50,500 --> 00:02:51,500
Somebody's got the YouTube.

39
00:02:51,500 --> 00:02:53,500
Please turn off the...

40
00:02:53,500 --> 00:02:55,500
Don't watch the video.

41
00:02:55,500 --> 00:02:56,500
Go to the comments.

42
00:02:56,500 --> 00:02:58,500
There's a link to the comments.

43
00:02:58,500 --> 00:03:02,500
Do you all comment or something like that?

44
00:03:02,500 --> 00:03:04,500
I want the video page.

45
00:03:04,500 --> 00:03:06,500
Here, if you want the comments page,

46
00:03:06,500 --> 00:03:08,500
I'm put it in again in the chat.

47
00:03:08,500 --> 00:03:10,500
I want to hang it.

48
00:03:10,500 --> 00:03:11,500
Anyway.

49
00:03:15,500 --> 00:03:17,500
So he wanted to go see the Buddha,

50
00:03:17,500 --> 00:03:21,500
and it gives a running commentary of why the king said the things that he did.

51
00:03:21,500 --> 00:03:23,500
And that's one of the answers.

52
00:03:23,500 --> 00:03:26,500
There's actually a more difficult passage to explain away

53
00:03:26,500 --> 00:03:29,500
if you're looking at it from that point of view.

54
00:03:29,500 --> 00:03:32,500
Is that the Buddha is said to have...

55
00:03:32,500 --> 00:03:35,500
One of his cousins looked very much like him.

56
00:03:37,500 --> 00:03:41,500
So there's a point there that can be made.

57
00:03:41,500 --> 00:03:44,500
But I'm a little bit leaning towards the idea

58
00:03:44,500 --> 00:03:48,500
that the Buddha did have the special characteristics,

59
00:03:48,500 --> 00:03:50,500
physical characteristics.

60
00:03:50,500 --> 00:03:53,500
There's also the example of...

61
00:03:53,500 --> 00:03:56,500
And I can't remember what suit it was,

62
00:03:56,500 --> 00:04:00,500
but this is shortly after the Buddha was enlightened.

63
00:04:00,500 --> 00:04:06,500
He was met someone that was looking for the Buddha

64
00:04:06,500 --> 00:04:11,500
and did not realize that he was talking to the Buddha.

65
00:04:11,500 --> 00:04:16,500
And so that kind of tells me that the Buddha had the appearance of a regular person.

66
00:04:16,500 --> 00:04:21,500
Because, you know, if he looked like he did that description,

67
00:04:21,500 --> 00:04:25,500
you know, get the fella's attention.

68
00:04:25,500 --> 00:04:28,500
So the fella was with the Buddha, looking for the Buddha,

69
00:04:28,500 --> 00:04:30,500
and didn't realize he was talking to it.

70
00:04:30,500 --> 00:04:32,500
But you probably know what suit it is.

71
00:04:32,500 --> 00:04:34,500
The one that got water shit made me.

72
00:04:34,500 --> 00:04:35,500
And it's...

73
00:04:35,500 --> 00:04:36,500
Yeah, that's right.

74
00:04:36,500 --> 00:04:37,500
That's right.

75
00:04:41,500 --> 00:04:43,500
Yeah, so there are some good points here.

76
00:04:43,500 --> 00:04:51,500
But, you know, when he's covered up by robes,

77
00:04:51,500 --> 00:04:55,500
I mean, I guess it does kind of put a damper on the commentary

78
00:04:55,500 --> 00:05:00,500
as the idea that there were six rays emitting from his body.

79
00:05:00,500 --> 00:05:01,500
And so on.

80
00:05:01,500 --> 00:05:02,500
Right.

81
00:05:04,500 --> 00:05:09,500
But, you know, it doesn't mean that he didn't have special characteristics on his body.

82
00:05:09,500 --> 00:05:12,500
Because if he's covered in a robe and so on.

83
00:05:12,500 --> 00:05:13,500
Yeah.

84
00:05:14,500 --> 00:05:18,500
I always think about these type of passages the other day.

85
00:05:18,500 --> 00:05:28,500
And Buddhism, you know, at the time that there were several different religions going on

86
00:05:28,500 --> 00:05:30,500
to correct the Buddhism was just one.

87
00:05:30,500 --> 00:05:31,500
Yeah.

88
00:05:31,500 --> 00:05:41,500
And, you know, I can see people try to spice up Buddhism in the past

89
00:05:41,500 --> 00:05:49,500
with these stories, you know, to make it, you know, the sound on par

90
00:05:49,500 --> 00:05:52,500
with some of the other religions going on.

91
00:05:52,500 --> 00:05:57,500
That's kind of what I have going on in my mind to explain these things

92
00:05:57,500 --> 00:06:03,500
that to me just really pop out, you know, as different from the rest.

93
00:06:03,500 --> 00:06:05,500
You know, their notes.

94
00:06:05,500 --> 00:06:06,500
Yeah.

95
00:06:06,500 --> 00:06:09,500
And I mean, again, the point is that they're not really all that important.

96
00:06:09,500 --> 00:06:18,500
But, you know, it may very well be just the qualities of mind that we have that are getting

97
00:06:18,500 --> 00:06:23,500
in the way of our ability to accept something that actually may be true.

98
00:06:23,500 --> 00:06:29,500
I mean, I'm with you and I understand how difficult it is to accept these things.

99
00:06:29,500 --> 00:06:36,500
And it's a perfectly valid argument in regards to him emitting rays that everyone could see.

100
00:06:36,500 --> 00:06:40,500
You know, maybe he emitted rays that only people with magical powers could see.

101
00:06:40,500 --> 00:06:42,500
So it just means that he had an aura.

102
00:06:42,500 --> 00:06:50,500
But, you know, the bottom line is not important, you know, you're correct on that.

103
00:06:50,500 --> 00:06:55,500
But there is another aspect that getting rid of doubt is important.

104
00:06:55,500 --> 00:07:00,500
Not exactly doubt, but getting rid of our propensity to go out of our way to doubt things

105
00:07:00,500 --> 00:07:07,500
and to be skeptical and to, you know, how it gets in the way of our appreciating,

106
00:07:07,500 --> 00:07:18,500
you know, the marble, some of the marvelous things for how it could.

107
00:07:18,500 --> 00:07:24,500
Just being careful, because I see so much, this is so much the tendency for people to go

108
00:07:24,500 --> 00:07:29,500
to the extreme of doubting, you know, things that are totally on a different level.

109
00:07:29,500 --> 00:07:35,500
Like, there's this one suit that talks about giving gifts to the sun-gend.

110
00:07:35,500 --> 00:07:44,500
And I think I mentioned it before, you know, it became one of the dead certain suitors that is not Buddhist teaching.

111
00:07:44,500 --> 00:07:50,500
And just the arguments were fairly ridiculous and totally inconclusive.

112
00:07:50,500 --> 00:07:56,500
And it's actually a suit that, if you read it in the right light, has beneficial qualities.

113
00:07:56,500 --> 00:08:06,500
So, more important than us discerning whether these things are right and wrong is to point out our own propensity to reject things.

114
00:08:06,500 --> 00:08:12,500
I mean, for me, part of enlightenment is to, you know, to come to accept things.

115
00:08:12,500 --> 00:08:16,500
And so I'm ready to say if the Buddha had a long time, so be it.

116
00:08:16,500 --> 00:08:21,500
If it turns out that he didn't, then it was just made up, as you say, then fine.

117
00:08:21,500 --> 00:08:26,500
I don't particularly need to disprove it.

118
00:08:26,500 --> 00:08:34,500
All of the things in the tepithika should be, you know, what to say.

119
00:08:34,500 --> 00:08:38,500
The Buddha's teaching should be realizable for yourself.

120
00:08:38,500 --> 00:08:46,500
So, it's not like if you start to accept things you're going to run into trouble.

121
00:08:46,500 --> 00:08:51,500
You accept them, you accept the Buddha's teaching and you put it into practice.

122
00:08:51,500 --> 00:08:55,500
And if it leads to results, you stick with it.

123
00:08:55,500 --> 00:08:58,500
If it doesn't lead to the results, then you give it up.

124
00:08:58,500 --> 00:09:03,500
And also, something's on the back burner doesn't necessarily mean it stays there.

125
00:09:03,500 --> 00:09:10,500
If later on through your insight, you know, you might take it off the back burner and bring it forward.

126
00:09:10,500 --> 00:09:17,500
But, you know, it shouldn't be a stumbling block.

127
00:09:17,500 --> 00:09:22,500
Moreover, as I said, I still do think it can be an interesting object of meditation.

128
00:09:22,500 --> 00:09:26,500
If you look at your doubts and come to let go of them.

129
00:09:26,500 --> 00:09:32,500
I mean, I'm just coming from the Buddhist societies that, you know, take these things to be totally true.

130
00:09:32,500 --> 00:09:38,500
You know, if most cultural Buddhists heard us talking like this, they would want to stone us

131
00:09:38,500 --> 00:09:41,500
for us heretics.

132
00:09:41,500 --> 00:09:51,500
But some of them are so pure in their ability to accept my teacher.

133
00:09:51,500 --> 00:09:52,500
He just accepts things.

134
00:09:52,500 --> 00:09:54,500
He'll just recite these things.

135
00:09:54,500 --> 00:09:55,500
He'll relate these things.

136
00:09:55,500 --> 00:09:58,500
How the Buddha had in this and this, he never goes into the long tongue.

137
00:09:58,500 --> 00:10:04,500
But many things that, you know, I just think, well, you know, maybe it wasn't really like that.

138
00:10:04,500 --> 00:10:09,500
But I give it to him because it's not hurting his practice to believe in this and that.

139
00:10:09,500 --> 00:10:12,500
And in fact, it's helping him because he doesn't have to think like we do.

140
00:10:12,500 --> 00:10:13,500
Like, is it true?

141
00:10:13,500 --> 00:10:16,500
Is it, you know, how do we explain this one?

142
00:10:16,500 --> 00:10:22,500
Or how do we find out these people who dissect the Topitika just to find out what is really the Buddha's teaching and what is not?

143
00:10:22,500 --> 00:10:24,500
And it's really not necessary.

144
00:10:24,500 --> 00:10:25,500
Right.

145
00:10:25,500 --> 00:10:28,500
Like, I give up a lot of things and put a lot of things as you say on the back burner.

146
00:10:28,500 --> 00:10:29,500
I guess that's it.

147
00:10:29,500 --> 00:10:34,500
You know, you say, well, I don't, you know, I don't know about that one.

148
00:10:34,500 --> 00:10:35,500
Okay.

149
00:10:35,500 --> 00:10:39,500
So let's put it aside for now and keep going.

150
00:10:39,500 --> 00:10:46,500
I think we're all on track.

151
00:10:46,500 --> 00:10:48,500
I hope that was helpful to the end person.

152
00:10:48,500 --> 00:10:51,500
But most important, we have to ask, what is helpful?

153
00:10:51,500 --> 00:10:52,500
Why are people asking these questions?

154
00:10:52,500 --> 00:10:54,500
Why are we here giving these answers?

155
00:10:54,500 --> 00:11:01,500
And that's why I want to point out this, this aspect of overcoming your doubt and letting go and just,

156
00:11:01,500 --> 00:11:07,500
because in the end, it's, if someone tells you that it tells you something that is totally false,

157
00:11:07,500 --> 00:11:15,500
you don't, you've given up a person who is enlightened doesn't see the need to discern whether what they're saying is true or false.

158
00:11:15,500 --> 00:11:22,500
They go as a matter of course and, and respond to things from a moment to moment basis.

159
00:11:22,500 --> 00:11:25,500
People might lie to them. People might cheat them.

160
00:11:25,500 --> 00:11:27,500
They're not upset. They're not offended by it.

161
00:11:27,500 --> 00:11:38,500
So what I mean is in general, we have to enlightenment is about giving up and letting go and not about,

162
00:11:38,500 --> 00:11:43,500
not about finding the answers to every question.

163
00:11:43,500 --> 00:11:54,500
So we got, like I called out recently, someone said, all this answering questions is, I think you're going about it the wrong way.

164
00:11:54,500 --> 00:12:00,500
If the answers are inside, why do we, why do we have to come out or something like that?

165
00:12:00,500 --> 00:12:16,500
So yeah, we have to be careful here, not too many questions.

