1
00:00:00,000 --> 00:00:06,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:06,000 --> 00:00:16,000
Today we continue with verse 215, which reads as follows.

3
00:00:16,000 --> 00:00:45,000
From desire comes sorrow from desire comes danger.

4
00:00:45,000 --> 00:01:08,000
When one is freed from desire, there is no sorrow from where can there be danger.

5
00:01:08,000 --> 00:01:22,000
This verse was taught in response to the story of Anitya Gandhi.

6
00:01:22,000 --> 00:01:24,000
Anitya Gandhi is a name.

7
00:01:24,000 --> 00:01:36,000
I'm not sure if it was really his name, but it means one who doesn't have a smell for women.

8
00:01:36,000 --> 00:01:47,000
I think it's possibly some kind of colloquialism from ancient India,

9
00:01:47,000 --> 00:01:51,000
like we would say when someone has a taste for something.

10
00:01:51,000 --> 00:01:55,000
That's only a guess, but it's based on the story.

11
00:01:55,000 --> 00:02:15,000
Because Anitya Gandhi means smell, and Anitya means woman, and is no, or not, one who doesn't have Anitya Gandhi was a man who lived in Sawati.

12
00:02:15,000 --> 00:02:31,000
But in past life, he had been a Brahma, a God, a being that lives in a realm without sensuality.

13
00:02:31,000 --> 00:02:45,000
It means in some far and distant time in the past, he had developed high states of concentration and absorption.

14
00:02:45,000 --> 00:03:02,000
He had been reborn in these great, profound, deep realms of existence and had lived there for

15
00:03:02,000 --> 00:03:19,000
a long time.

16
00:03:19,000 --> 00:03:35,000
And as a young man, from birth wanted to have nothing to do with women.

17
00:03:35,000 --> 00:03:41,000
Whenever a woman came close to him, he would scream, he would whale.

18
00:03:41,000 --> 00:03:55,000
When they tried to breastfeed him, they had to sort of cover themselves up and expose only their breasts so that he couldn't see the woman.

19
00:03:55,000 --> 00:04:19,000
Not sure how, again, it's just a story, but it's quite possible.

20
00:04:19,000 --> 00:04:29,000
This is what the story says, that he actually had a stronger version to them.

21
00:04:29,000 --> 00:04:37,000
But the point is that he had no desire for women.

22
00:04:37,000 --> 00:04:57,000
And so his parents tried to encourage him to get married, and so they would ask him sons and please we want you to carry on our lineage, have children, become a householder.

23
00:04:57,000 --> 00:05:01,000
When you're going to get married, when are you going to go find a woman?

24
00:05:01,000 --> 00:05:07,000
They tried, they knew that he had this aversion to women and they pushed him and pushed him.

25
00:05:07,000 --> 00:05:15,000
And finally he got fed up with it, and he built a statue.

26
00:05:15,000 --> 00:05:30,000
He created a statue of a woman, a replica of the most beautiful woman, and she was just this statue was the perfect figure of a woman.

27
00:05:30,000 --> 00:05:38,000
And so when his parents came and said, son, son, when you're going to find a wife, he said, look, show them the statue.

28
00:05:38,000 --> 00:05:47,000
And he said, if you can find me a woman, as beautiful as that, then I will get married.

29
00:05:47,000 --> 00:05:50,000
I will marry her.

30
00:05:50,000 --> 00:06:10,000
So I imagine some of the aversion probably wasn't that he was almost actual, probably was just, again, his life in the Brahma realms, caused him to break away from the ordinary attachment to sensuality that most people find themselves stuck in.

31
00:06:10,000 --> 00:06:25,000
And so he was able to see the flaws, I would say, in the human body. And so while everyone else was raving about the beauty, all he saw was the ugliness.

32
00:06:25,000 --> 00:06:39,000
And so hearing of this beauty, he created this statue and he said, look, there's no beauty that I see here, but if you find this beauty, something that is actually beautiful,

33
00:06:39,000 --> 00:06:44,000
there's perfect woman, then I will marry her.

34
00:06:44,000 --> 00:06:53,000
So the parents said, okay, well, there's got to be someone in the world that beautiful, our son is a very meritorious sort of person.

35
00:06:53,000 --> 00:07:07,000
He clearly has some strong qualities of good karma, so there's got to be someone out there who has the same level as him and perhaps has some connection with him from past lives.

36
00:07:07,000 --> 00:07:15,000
And so they took this statue in a cart and they traveled around the countryside looking for such a woman.

37
00:07:15,000 --> 00:07:23,000
There happens to have been such a woman in a city called Sagar, I think.

38
00:07:23,000 --> 00:07:36,000
And so they brought this statue there and when they went to the bathing place, this woman came inside and said, what are you doing out here?

39
00:07:36,000 --> 00:07:49,000
She had just bathed this, there was this young woman living on the top floor of a seven story mansion.

40
00:07:49,000 --> 00:08:02,000
There's this sort of stereotype or I guess it's an ancient tradition perhaps of keeping women young daughters secluded.

41
00:08:02,000 --> 00:08:07,000
And I think it's sort of the ideal of the perfect maiden, the perfect version.

42
00:08:07,000 --> 00:08:16,000
Virgin having no contact with men, having never had contact with men so she would be kept up in a tower.

43
00:08:16,000 --> 00:08:21,000
I think we hear about such stories like Rapunzel and so on.

44
00:08:21,000 --> 00:08:23,000
Anyway.

45
00:08:23,000 --> 00:08:32,000
And she had just bathed this, this was the maid of the house, the nurse maid or so on.

46
00:08:32,000 --> 00:08:39,000
She had just bathed her, her care, the daughter and then went down and went out to the waterhole or whatever.

47
00:08:39,000 --> 00:08:44,000
And she said, and she saw this statue and she said, what are you doing out here?

48
00:08:44,000 --> 00:08:51,000
I just bathed you and now you're out and about and she struck the girl, I don't know, hit her or whatever.

49
00:08:51,000 --> 00:08:56,000
And realized it was just a statue and said, what's this doing here?

50
00:08:56,000 --> 00:09:02,000
And the men who had been tasked to carry this around came and said, what are you doing?

51
00:09:02,000 --> 00:09:05,000
What do you have a statue of my daughter?

52
00:09:05,000 --> 00:09:10,000
My, it's not my daughter, my, my charge.

53
00:09:10,000 --> 00:09:15,000
And they said, your daughter looks like this, your charge looks like this.

54
00:09:15,000 --> 00:09:21,000
And she said, well, to be honest, that has nothing on, on my mistress.

55
00:09:21,000 --> 00:09:25,000
She is far more beautiful than that.

56
00:09:25,000 --> 00:09:27,000
And they said, oh, please, can you take us to her?

57
00:09:27,000 --> 00:09:35,000
And they went and they saw this girl and indeed she was the most beautiful girl in the realm.

58
00:09:35,000 --> 00:09:41,000
And so they traveled back, they, they talked with him about their plans and they traveled back.

59
00:09:41,000 --> 00:09:48,000
The young man, they told the young man and the parents about this, this woman.

60
00:09:48,000 --> 00:09:57,000
And then in Uganda became suddenly a desire arose in him that had never risen before.

61
00:09:57,000 --> 00:10:01,000
Suddenly this desire that he had never felt when he looked at another woman.

62
00:10:01,000 --> 00:10:03,000
We looked at other women.

63
00:10:03,000 --> 00:10:08,000
Suddenly it arose in him, this desire.

64
00:10:08,000 --> 00:10:15,000
He was anticipating always asking when did she come in the, send a message that she shouldn't come into carriage,

65
00:10:15,000 --> 00:10:19,000
but she was so fragile and they had kept her so secluded.

66
00:10:19,000 --> 00:10:24,000
But the story says quite matter of factly that she died along the way.

67
00:10:24,000 --> 00:10:33,000
She felt ill and died while she was traveling because the travel was just too, too, too hard for her.

68
00:10:33,000 --> 00:10:36,000
Simple as that.

69
00:10:36,000 --> 00:10:40,000
And then in Uganda was kept asking when is she coming?

70
00:10:40,000 --> 00:10:41,000
Is she coming today?

71
00:10:41,000 --> 00:10:46,000
And the parents tried to stall him, but eventually they had to let him go, let him know that.

72
00:10:46,000 --> 00:10:56,000
Unfortunately she was so close to being united in heavenly matrimony or whatever.

73
00:10:56,000 --> 00:11:00,000
And yet she died along the way.

74
00:11:00,000 --> 00:11:08,000
And then in Uganda became very sad and locked himself in his room, I guess.

75
00:11:08,000 --> 00:11:15,000
The Buddha found out about this and came for breakfast for food one day.

76
00:11:15,000 --> 00:11:19,000
They fed the Buddha and the Buddha asked them, where is the in Uganda?

77
00:11:19,000 --> 00:11:21,000
He's up in his room.

78
00:11:21,000 --> 00:11:25,000
And so they called him down and the Buddha said, what's wrong?

79
00:11:25,000 --> 00:11:27,000
He explained to the Buddha what's wrong?

80
00:11:27,000 --> 00:11:32,000
He said, oh, in Uganda, do you know why you are so sad?

81
00:11:32,000 --> 00:11:36,000
Do you know where this sadness comes from?

82
00:11:36,000 --> 00:11:43,000
And he said, you know, you tell me where it comes from and the Buddha said it comes from desire.

83
00:11:43,000 --> 00:11:47,000
And then he taught this verse.

84
00:11:47,000 --> 00:11:54,000
So there's a couple of things about this verse, about this story and this verse.

85
00:11:54,000 --> 00:12:02,000
The first is of course the obvious lesson that it gives us another facet, another angle,

86
00:12:02,000 --> 00:12:14,000
in the on the topic of desire, on the topic of suffering coming from desire.

87
00:12:14,000 --> 00:12:20,000
So we've talked about how the striving and the fighting over objects of desire can lead to suffering.

88
00:12:20,000 --> 00:12:23,000
We've talked about the loss of objects of desire.

89
00:12:23,000 --> 00:12:37,000
Here we have the inability to obtain the object of desire, which is again a common theme in life.

90
00:12:37,000 --> 00:12:49,000
People whose ambitions are unfulfilled, whose wishes are unfulfilled, whose love is unrequited.

91
00:12:49,000 --> 00:12:55,000
When we see it in the build-up in the anticipation, this cultivation of desire,

92
00:12:55,000 --> 00:13:01,000
the day after day where any Uganda is sitting at home, building.

93
00:13:01,000 --> 00:13:15,000
You know, day after day this anticipation and stoking up his desire to a fevered pitch until it is finally crushed.

94
00:13:15,000 --> 00:13:22,000
I've heard, I've talked with people who end up quite depressed and quite obsessed with something in the past

95
00:13:22,000 --> 00:13:27,000
where they feel was really their chance at happiness.

96
00:13:27,000 --> 00:13:35,000
And they get so stuck on that, that they're unable to free themselves from the cycle of depression.

97
00:13:35,000 --> 00:13:41,000
They feel hopeless and helpless because of course with the past you can't change it.

98
00:13:41,000 --> 00:14:00,000
And the more your key to success gets stuck in the past, the more desperate your situation becomes the harder it becomes to free yourself.

99
00:14:00,000 --> 00:14:12,000
So in all, we've looked at many aspects and it really is a good sort of multifaceted lesson in how desire leads to suffering.

100
00:14:12,000 --> 00:14:18,000
When we look at all these facets and when we look at all the ways in which we could suffer,

101
00:14:18,000 --> 00:14:24,000
based on what appears to me, the cause of happiness, which is obtaining the objects of your desire.

102
00:14:24,000 --> 00:14:40,000
We look at all the ways that we do suffer in life, based on them. We come to see that it's not insignificant for us to say that it is fraught with sorrow and danger.

103
00:14:40,000 --> 00:14:53,000
But the other lesson is a little less obvious but also quite important and profound and that is how divested from reality.

104
00:14:53,000 --> 00:15:02,000
That's the correct use of the term. Our perceptions of desire are...

105
00:15:02,000 --> 00:15:19,000
We think we desire people and experiences, I guess, like a state of being married, of being romantically involved.

106
00:15:19,000 --> 00:15:30,000
Experiences like sex and cuddling and so on. Interactions with other people.

107
00:15:30,000 --> 00:15:36,000
We think that these are the things that we desire.

108
00:15:36,000 --> 00:15:48,000
We think that we are desiring the beautiful shape, the sight, the sight, the sound, the smell, the taste, the feeling.

109
00:15:48,000 --> 00:15:52,000
We think our desire is something...

110
00:15:52,000 --> 00:16:13,000
We make some idea of desire that is far more complicated, complex, and perhaps even far more convincing than the reality.

111
00:16:13,000 --> 00:16:23,000
Convincing in the sense that it's easy to make a narrative out of a love affair.

112
00:16:23,000 --> 00:16:31,000
It's easy to make this story, if not given in a Buddhist context, would be a great tragedy.

113
00:16:31,000 --> 00:16:52,000
In a Buddhist context, it's a great victory in the sense for the woman, but for the man in the sense that he's come to realize that not only A was still very much susceptible to desire,

114
00:16:52,000 --> 00:17:01,000
but also that the desire was on a deeper level than the aversion that he had towards ugliness.

115
00:17:01,000 --> 00:17:08,000
The danger in desire was the...

116
00:17:08,000 --> 00:17:29,000
Of course, the potential for loss, the potential for obtaining and so on.

117
00:17:29,000 --> 00:17:48,000
But the reality is that we don't crave for any of these things, all of these narratives and stories and ideas we have of desire.

118
00:17:48,000 --> 00:17:58,000
They mask with the real truth, and that the desire is really, in some sense, much more similar to a drug addiction.

119
00:17:58,000 --> 00:18:05,000
Our desire for any kind of sensuality.

120
00:18:05,000 --> 00:18:11,000
And you can see this. In this story, he had never seen this woman.

121
00:18:11,000 --> 00:18:24,000
He had a statue that was supposed to look like her, but it's quite remarkable that his desire became so strong.

122
00:18:24,000 --> 00:18:31,000
Not that this is surprising, but because it's the sort of thing that we can probably identify with.

123
00:18:31,000 --> 00:18:47,000
We anticipate that we build up this idea in our minds that is nothing to do with the actual liking of what we're seeing.

124
00:18:47,000 --> 00:18:52,000
We say that it's this thing that we see that we want, but he's never even seen this woman.

125
00:18:52,000 --> 00:19:01,000
What he was anticipating was the pleasure that he would get from seeing her.

126
00:19:01,000 --> 00:19:10,000
And in fact, probably during the time that he was anticipating seeing her, he was actually already engaging in the drug addiction.

127
00:19:10,000 --> 00:19:13,000
Because his mind was producing the chemicals.

128
00:19:13,000 --> 00:19:27,000
As long as you can build this narrative up of beauty and of pleasure, you can feel the great pleasure that comes along with addiction, even without getting what you want.

129
00:19:27,000 --> 00:19:42,000
So the pleasure had nothing to do at that point with seeing or encountering this woman. It had to do with him triggering the feelings of pleasure in his brain, really.

130
00:19:42,000 --> 00:19:48,000
The brain chemicals that we become addicted to.

131
00:19:48,000 --> 00:19:57,000
Our desires are not related to what we think of as the objects of our desire.

132
00:19:57,000 --> 00:20:03,000
They're related in a secondary sense in terms of being a catalyst.

133
00:20:03,000 --> 00:20:09,000
So we being unable to manufacture these chemicals directly.

134
00:20:09,000 --> 00:20:18,000
We trigger them through this system of habits where in the past we've experienced pleasure through these things.

135
00:20:18,000 --> 00:20:25,000
So when we see them, we're able to give rise to the feeling of pleasure in the mind.

136
00:20:25,000 --> 00:20:36,000
When heterosexual men see women, they become triggers something in the brain.

137
00:20:36,000 --> 00:20:44,000
And they're able to experience this pleasure.

138
00:20:44,000 --> 00:20:49,000
When homosexual men see men, they have had some men.

139
00:20:49,000 --> 00:20:56,000
They become, or the sign of the man in the body, they become, likewise give rise to this trigger.

140
00:20:56,000 --> 00:21:01,000
We would say in Buddhism based on habits from past lives.

141
00:21:01,000 --> 00:21:18,000
When dogs see other dogs and they get the sign of a dog, they become intoxicated, the desire arises in the mind and the pleasure arises.

142
00:21:18,000 --> 00:21:35,000
And so this is a big part of why mindfulness is so powerful in helping us overcome not just desire, but aversion and all types of problems that are removed from reality.

143
00:21:35,000 --> 00:21:43,000
Because mindfulness is focusing on what's really happening. It's focusing on the very building blocks of the experience.

144
00:21:43,000 --> 00:21:49,000
It's showing us or it's allowing us to see what's happening when you see something that you like.

145
00:21:49,000 --> 00:21:59,000
That it's not the thing that you like, it's actually the feeling of pleasure that arises that you're able to give rise to.

146
00:21:59,000 --> 00:22:09,000
If it was the thing that brought us pleasure, then looking at it incessantly would always bring us pleasure, but we see that that's also not the case.

147
00:22:09,000 --> 00:22:16,000
And this is why often it's very difficult for romantic partners to stay faithful.

148
00:22:16,000 --> 00:22:18,000
It's one of the reasons.

149
00:22:18,000 --> 00:22:20,000
Because they become attracted to something else.

150
00:22:20,000 --> 00:22:23,000
Because of course the brain is the way it works.

151
00:22:23,000 --> 00:22:30,000
It's very hard for the same object to give rise to the same amount of pleasure.

152
00:22:30,000 --> 00:22:34,000
And so you need something new, something exciting.

153
00:22:34,000 --> 00:22:39,000
It has to be something that actually is able to turn the switch on.

154
00:22:39,000 --> 00:22:45,000
The mind becomes, in a sense, worn away.

155
00:22:45,000 --> 00:22:54,000
It's not exactly, but it becomes a belt in your car that becomes stretched and no longer works.

156
00:22:54,000 --> 00:22:58,000
And so it's like the mind gets tired of the same old thing.

157
00:22:58,000 --> 00:23:07,000
It's like this is why we feel we get bored of the same old thing, and so something that you used to look at and be quite excited by and turned on by.

158
00:23:07,000 --> 00:23:13,000
And therefore pleased by no longer gives you that pleasure and so you have to look elsewhere.

159
00:23:13,000 --> 00:23:21,000
So I drug addiction becomes worse and worse because the same amount of the drug can't give you the same amount of pleasure.

160
00:23:21,000 --> 00:23:28,000
When we see it with music, why we have to, if music was so pleasant, why couldn't we listen to the same song?

161
00:23:28,000 --> 00:23:33,000
It can't listen to the same song for hours and hours and days on end.

162
00:23:33,000 --> 00:23:37,000
You can, but the pleasure won't be the same as the first time.

163
00:23:37,000 --> 00:23:40,000
And so you have to vary it, go back and forth.

164
00:23:40,000 --> 00:23:50,000
And we learn to work with the mind to get as much pleasure as we can by switching objects.

165
00:23:50,000 --> 00:23:52,000
But that's the reality of it.

166
00:23:52,000 --> 00:23:54,000
And that's what mindfulness allows us to see.

167
00:23:54,000 --> 00:24:00,000
That's still not an easy thing to deal with because these addictions are real.

168
00:24:00,000 --> 00:24:10,000
But the point is that we have to recognize, if we want to be honest, we have to recognize that our desires don't come from people in places and things.

169
00:24:10,000 --> 00:24:17,000
They come really from just drug addictions to the brain chemicals that bring us pleasure.

170
00:24:17,000 --> 00:24:23,000
Being stimulated in various parts of the body, or at any of the various senses,

171
00:24:23,000 --> 00:24:30,000
gives rise to brain chemicals that are very difficult and challenging to overcome.

172
00:24:30,000 --> 00:24:35,000
And mindfulness isn't able immediately to do away with these.

173
00:24:35,000 --> 00:24:38,000
It's a big part of our practice as to be patients.

174
00:24:38,000 --> 00:24:43,000
But a very important first step is being able to see that that's actually the truth.

175
00:24:43,000 --> 00:24:49,000
And not simply turning them off, but becoming aware of them,

176
00:24:49,000 --> 00:25:01,000
becoming aware that it's not this romantic or high-minded narrative or story of a love affair or so on.

177
00:25:01,000 --> 00:25:04,000
It's merely drug addiction.

178
00:25:04,000 --> 00:25:09,000
Drug addiction with its many sorrows, not getting what you want, losing what you like,

179
00:25:09,000 --> 00:25:17,000
to compete for what you want, and so on, and all the stories that we've heard in this chapter.

180
00:25:17,000 --> 00:25:22,000
So with mindfulness, we cut straight to the reality.

181
00:25:22,000 --> 00:25:25,000
All of those stories have no meaning.

182
00:25:25,000 --> 00:25:29,000
What we're seeing is just seeing hearing when you say to yourself, seeing, seeing,

183
00:25:29,000 --> 00:25:39,000
you're able to experience the seeing, just the seeing, and it doesn't excite in the same way as before.

184
00:25:39,000 --> 00:25:45,000
When you have the desire and the pleasure, you're able to see that those are actually what the mind is fixating on.

185
00:25:45,000 --> 00:25:53,000
Whatever can give it that pleasure in the brain and in the mind, that's what it aims for.

186
00:25:53,000 --> 00:26:04,000
And then you're able to focus on those and not turn them off, but slowly, slowly attenuate them.

187
00:26:04,000 --> 00:26:13,000
Attenuate them mainly by seeing the main and the deepest part of the path is by seeing them just for what they are.

188
00:26:13,000 --> 00:26:17,000
And the importance there is that what they are is just an experience.

189
00:26:17,000 --> 00:26:24,000
That actually pleasure, and this is on a deeper level than any of these stories. Pleasure is just pleasure.

190
00:26:24,000 --> 00:26:30,000
There's nothing good, nothing bad about it, but there's desire and attachment.

191
00:26:30,000 --> 00:26:34,000
The liking of the pleasure also has no basis in reality.

192
00:26:34,000 --> 00:26:46,000
There's nothing intrinsically likable, lovable, desirable about pleasure, just like there's nothing intrinsically undesirable about pain.

193
00:26:46,000 --> 00:26:50,000
And being able to see through these is very powerful.

194
00:26:50,000 --> 00:26:53,000
It's what allows us to find real peace of mind.

195
00:26:53,000 --> 00:27:02,000
It's why a meditator, someone who has even practiced the way this young man had practiced, can find great states of peace and calm,

196
00:27:02,000 --> 00:27:08,000
simply by extracting themselves from that process of addiction.

197
00:27:08,000 --> 00:27:17,000
Now, as we can see in another lesson in the story is that simply extracting yourself from it doesn't last forever.

198
00:27:17,000 --> 00:27:28,000
It doesn't solve the problem, because eventually when you get in contact with it, something really, really enticing, then you become attached again.

199
00:27:28,000 --> 00:27:40,000
But we can see that from meditation, from mindfulness practice, that by observing it, by facing it, just like facing pain.

200
00:27:40,000 --> 00:27:46,000
By confronting the pleasure, we come to see that there's nothing special about it, not in the way we thought.

201
00:27:46,000 --> 00:27:56,000
There's certainly none of this narrative about falling in love and finding the girl or the boy or the person of your dreams.

202
00:27:56,000 --> 00:28:01,000
There's simply drugs and addiction.

203
00:28:01,000 --> 00:28:28,000
So, that's the Dhammapada for tonight. Thank you all for listening. We'll show you all the best.

