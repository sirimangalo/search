1
00:00:00,000 --> 00:00:10,280
We've been available to Saiyadur, Usela Nanda, at the Tagata Meditation Center, San Jose,

2
00:00:10,280 --> 00:00:15,320
California, on October 24, 1997.

3
00:00:15,320 --> 00:00:33,360
We finished the first step in the spiritual path of the follower of the Buddha.

4
00:00:33,360 --> 00:00:44,240
So the first step in the spiritual path is the purity of moral conduct, Usela.

5
00:00:44,240 --> 00:01:03,360
So by keeping Silla, by keeping precepts, one is able to control his or her bodily actions

6
00:01:03,360 --> 00:01:07,440
and verbal actions.

7
00:01:07,440 --> 00:01:17,040
So if a person can control his actions, if a person can keep himself from killing,

8
00:01:17,040 --> 00:01:26,600
leaving beings and so on, it is an accra- a great achievement.

9
00:01:26,600 --> 00:01:35,160
But on the spiritual path, just to keep precepts is not enough.

10
00:01:35,160 --> 00:01:47,720
Because even though we keep the precepts, in our minds, we can have impurities.

11
00:01:47,720 --> 00:01:57,760
In the minds, we may kill some living being or we may do something wrong in the minds.

12
00:01:57,760 --> 00:02:12,560
So in order to control our minds, we need to go to the next step, which is called samadhi.

13
00:02:12,560 --> 00:02:28,320
And Buddha said that when mind is developed, when mind is cultivated, then it brings happiness.

14
00:02:28,320 --> 00:02:40,080
So he said, no other thing, do I know that brings so much happiness as a mind that is developed,

15
00:02:40,080 --> 00:02:53,360
that is repeatedly cultivated, mind when developed and repeatedly cultivated brings happiness.

16
00:02:53,360 --> 00:03:01,080
So this means that if we want to gain happiness, we have to control our minds, we have

17
00:03:01,080 --> 00:03:10,680
to develop the mind, we have to repeatedly cultivate the mind.

18
00:03:10,680 --> 00:03:21,880
In order to develop our minds, we need to practice samadhi or concentration.

19
00:03:21,880 --> 00:03:34,160
Now, Buddha exalted minds to develop concentration, and instead, among whose mind is,

20
00:03:34,160 --> 00:03:41,560
with concentration, whose mind is concentrated, knows according to reality.

21
00:03:41,560 --> 00:03:53,440
So knows the things as they are, and how does he know, according to reality, he knows

22
00:03:53,440 --> 00:04:03,600
that the I is impermanent, he knows that the visible objects are impermanent, he knows

23
00:04:03,600 --> 00:04:13,320
that seeing consciousness is impermanent, he knows that the contact between the I and

24
00:04:13,320 --> 00:04:23,520
the visible objects is impermanent, and he knows that the sensation that arises out

25
00:04:23,520 --> 00:04:31,400
of the contact of these is impermanent.

26
00:04:31,400 --> 00:04:38,960
It is important that if we want to understand things as they really are, if we want to penetrate

27
00:04:38,960 --> 00:04:46,400
into the nature of things, if we want to see things as impermanent and so on, we need

28
00:04:46,400 --> 00:04:51,640
to develop samadhi.

29
00:04:51,640 --> 00:05:04,200
And before we develop samadhi, we must first know what samadhi is and what are its characteristic

30
00:05:04,200 --> 00:05:06,400
and so on.

31
00:05:06,400 --> 00:05:22,000
Now the what samadhi is translated as concentration, defined as wholesome one-pointedness

32
00:05:22,000 --> 00:05:39,440
or wholesome unification of mind, but samadhi can be divided into two parts, some and

33
00:05:39,440 --> 00:05:52,880
samadhi.

34
00:05:52,880 --> 00:06:05,280
And some here has two meanings, the first meaning is evenly, and the second meaning is

35
00:06:05,280 --> 00:06:18,960
rightly, so samadhi means something, some mental state that puts evenly and rightly.

36
00:06:18,960 --> 00:06:31,320
So put foot, that puts, or that pleases the cheetah and cheetah seekers, the consciousness

37
00:06:31,320 --> 00:06:41,280
and mental factors and pleases where on a single object.

38
00:06:41,280 --> 00:06:49,720
So samadhi means a mental factor that pleases or that keeps the consciousness and its

39
00:06:49,720 --> 00:07:03,240
concomitance on a single object.

40
00:07:03,240 --> 00:07:04,320
So we must understand that samadhi is a mental state or mental factor.

41
00:07:04,320 --> 00:07:12,600
And this mental factor keeps the consciousness and its concomitance on a single object

42
00:07:12,600 --> 00:07:27,680
evenly and rightly, evenly means to not let the concomitance cheetah.

43
00:07:27,680 --> 00:07:40,200
So it keeps all the concomitance collected and rightly means not let them be distracted to

44
00:07:40,200 --> 00:07:43,040
other objects.

45
00:07:43,040 --> 00:07:50,560
So the samadhi keeps the cheetah and cheetah seekers, the consciousness and mental

46
00:07:50,560 --> 00:08:03,680
factors collected and then it does not give, give them chance to, to be distracted.

47
00:08:03,680 --> 00:08:18,360
So this is what we call samadhi and the characteristic of samadhi is said to be non-destruction.

48
00:08:18,360 --> 00:08:30,080
So when there is samadhi, your mind is not distracted, your mind does not go to other objects

49
00:08:30,080 --> 00:08:35,440
but it stays on the meditation object.

50
00:08:35,440 --> 00:08:48,480
So its characteristic is non-destruction and when it stays on the object that means it does

51
00:08:48,480 --> 00:08:56,280
not let the concomitance to be distracted and so its function is said to be to eliminate

52
00:08:56,280 --> 00:09:17,000
destruction and when mind is kept on one object only, just not weaver, mind does not

53
00:09:17,000 --> 00:09:32,240
go here and there and so the manifestation of samadhi is non-wavering, non-shaking.

54
00:09:32,240 --> 00:09:40,360
Sometimes samadhi is compared to a flame of lamp in a place where there is no, no draft

55
00:09:40,360 --> 00:09:56,640
of air and its proximate course is, it is for wholesome samadhi, its proximate course

56
00:09:56,640 --> 00:10:15,520
is sukha bliss, sukha may mean happiness or comfort.

57
00:10:15,520 --> 00:10:28,560
So sukha comfort is necessary for us to get concentration.

58
00:10:28,560 --> 00:10:50,080
Now there are different kinds of samadhi and tonight we will deal with samata samadhi.

59
00:10:50,080 --> 00:11:01,040
The word samata and the word samadhi are often used synonymously.

60
00:11:01,040 --> 00:11:18,600
So samadhi means samata means samadhi, concentration, samata means making something

61
00:11:18,600 --> 00:11:34,920
calm, so something that calms down the mind is called samata, so it is the same as samadhi.

62
00:11:34,920 --> 00:11:45,320
Now as you know in Buddhism two kinds of meditation are taught, the samata meditation

63
00:11:45,320 --> 00:11:51,640
and Vipasana meditation.

64
00:11:51,640 --> 00:12:02,560
In both kinds of meditation we have to develop samadhi, but in samata meditation samadhi

65
00:12:02,560 --> 00:12:14,080
is more important, samadhi is predominant and there are two kinds of samadhi in samata

66
00:12:14,080 --> 00:12:29,400
meditation, the neighborhood concentration and absorption concentration.

67
00:12:29,400 --> 00:12:37,600
Neighborhood concentration means concentration which is in the neighborhood of jhana concentration.

68
00:12:37,600 --> 00:12:46,600
So the neighborhood concentration precedes jhana concentration and jhana concentration

69
00:12:46,600 --> 00:12:58,120
is a concentration when one gets or when one attains the highest state of consciousness

70
00:12:58,120 --> 00:13:01,200
called jhana.

71
00:13:01,200 --> 00:13:11,600
So samadhi in the jhana state is stronger than the samadhi in neighborhood state.

72
00:13:11,600 --> 00:13:21,720
And both the neighborhood concentration and jhana concentration are able to subdue or suppress

73
00:13:21,720 --> 00:13:27,480
the five mental hindrances.

74
00:13:27,480 --> 00:13:36,480
And samata meditation can lead you to the attainment of neighborhood concentration and jhana

75
00:13:36,480 --> 00:13:38,480
concentration.

76
00:13:38,480 --> 00:13:49,240
Then after the attainment of jhana you can further practice and attain what I call super

77
00:13:49,240 --> 00:13:58,360
normal knowledge as abinias.

78
00:13:58,360 --> 00:14:05,800
In Buddhism samadha meditation is taught not just for the sake of attaining neighborhood

79
00:14:05,800 --> 00:14:11,640
concentration and jhana concentration.

80
00:14:11,640 --> 00:14:22,240
This is taught in Buddhism so that the concentrations, the two kinds of concentrations

81
00:14:22,240 --> 00:14:39,160
gained through the samata meditation will be made into a basis for vipasana meditation.

82
00:14:39,160 --> 00:14:44,920
So there is no such thing as samata for samata sake only.

83
00:14:44,920 --> 00:14:58,440
So in Buddhism samata is taught as a preliminary or as a basis for vipasana meditation.

84
00:14:58,440 --> 00:15:09,560
Now the aim of Buddha's teachings is to get free from mental defalments.

85
00:15:09,560 --> 00:15:18,720
In order to get free from mental defalments one need to see the true nature of things,

86
00:15:18,720 --> 00:15:26,920
one need to see that things are impermanent and so on.

87
00:15:26,920 --> 00:15:38,400
And samata by itself cannot lead you to discovering these characteristics of mind and

88
00:15:38,400 --> 00:15:40,920
matter.

89
00:15:40,920 --> 00:15:49,880
Samata by itself cannot lead you to understand the true nature of things and so it cannot

90
00:15:49,880 --> 00:15:56,920
lead you to the eradication of mental defalments.

91
00:15:56,920 --> 00:16:08,600
But after you have developed strong samadhi by way of samata meditation you can make

92
00:16:08,600 --> 00:16:18,600
that samadhi or the jhana, the object of vipasana and then you practice vipasana on these

93
00:16:18,600 --> 00:16:27,800
objects and you go from one stage of vipasana to another until you reach the final goal.

94
00:16:27,800 --> 00:16:37,200
So samata by itself cannot lead you to the eradication of mental defalments but it can

95
00:16:37,200 --> 00:16:49,160
be a very helpful in the practice of vipasana meditation.

96
00:16:49,160 --> 00:17:01,200
Now samata meditation can be practiced taking many subjects.

97
00:17:01,200 --> 00:17:07,600
Really we see there are 40 subjects of samata meditation.

98
00:17:07,600 --> 00:17:18,080
That means you can practice any one of these 40 kinds of meditation.

99
00:17:18,080 --> 00:17:21,680
Please look at the handouts.

100
00:17:21,680 --> 00:17:30,680
The first are called tan casinas, the vat casina is very difficult to translate.

101
00:17:30,680 --> 00:17:40,680
So it is best left unsaturated and just called casina, the casina means all or whole.

102
00:17:40,680 --> 00:17:47,120
That means you put the mind on the whole of the casina object, so it is called casina.

103
00:17:47,120 --> 00:18:00,280
There are 10 kinds of casina subjects taught in Buddha's teachings and they are at casina,

104
00:18:00,280 --> 00:18:10,960
vatakasina, phyogasina, aogasina and then blue casina, yellow casina, red casina, white

105
00:18:10,960 --> 00:18:22,920
casina, space casina and light casina.

106
00:18:22,920 --> 00:18:36,480
So the first four are the four elements taught in Buddhism, earth, water, fire and air.

107
00:18:36,480 --> 00:18:45,000
And when you practice at casina and you are successful, then you will get first upachara

108
00:18:45,000 --> 00:18:50,440
samadhi, nibahud some nibahud concentration.

109
00:18:50,440 --> 00:19:01,840
And if you go on, then you can get all five janas, the first to fifth janas.

110
00:19:01,840 --> 00:19:11,120
All the tan casinas are the same, so by practicing any one of these casinas, you can

111
00:19:11,120 --> 00:19:22,120
get both concentrations, that is, nibahud concentration and jana concentration.

112
00:19:22,120 --> 00:19:34,440
If you want to practice at casina, then you will have to make a casina disc.

113
00:19:34,440 --> 00:19:41,160
You use some clay, preferably the color of dawn.

114
00:19:41,160 --> 00:19:52,080
So you make a disc about 10 inches in diameter and then you make it smooth and clean.

115
00:19:52,080 --> 00:20:01,840
And then you put it in front of you, not too far away from you, not too close to you.

116
00:20:01,840 --> 00:20:10,480
And then you put the mind on the whole of this casina disc and then say to yourself, earth,

117
00:20:10,480 --> 00:20:22,040
earth, thousands and thousands of times, actually you are memorizing that disc.

118
00:20:22,040 --> 00:20:28,280
Sometimes you open your eyes and look at it and say, sometimes you close your eyes.

119
00:20:28,280 --> 00:20:38,800
And so by practice, a time will come when you can see the other disc, even when your eyes

120
00:20:38,800 --> 00:20:39,800
are closed.

121
00:20:39,800 --> 00:20:46,200
So as clearly as when your eyes are open.

122
00:20:46,200 --> 00:20:58,520
So when you have got that sign, you call it a sign, then you leave the real disc alone

123
00:20:58,520 --> 00:21:10,200
and then try to dwell your mind on the image of the casina you have memorized.

124
00:21:10,200 --> 00:21:22,000
So if you go on contemplating on the image of mental image of that casina, that image becomes

125
00:21:22,000 --> 00:21:27,000
more and more refined.

126
00:21:27,000 --> 00:21:43,040
Actually the casina disc, the mental image will appear as the exact replica of the earth

127
00:21:43,040 --> 00:21:44,040
disc.

128
00:21:44,040 --> 00:21:50,520
If there are some blemishes on the disc, earth disc, then they will appear in the

129
00:21:50,520 --> 00:22:00,640
mental image, but when you reach the next stage where it is refined, then these blemishes

130
00:22:00,640 --> 00:22:12,520
disappear and the mental image becomes as smooth as and as clear as the disc of the moon

131
00:22:12,520 --> 00:22:16,040
and the sun and so on.

132
00:22:16,040 --> 00:22:25,840
Then you dwell on it again and again and then a type of consciousness arises in you that

133
00:22:25,840 --> 00:22:36,480
consciousness may have never been arisen in you before and that is what we call Jana consciousness.

134
00:22:36,480 --> 00:22:42,600
So at the moment of Jana consciousness and before reaching that Jana consciousness, your

135
00:22:42,600 --> 00:22:54,960
mind or your concentration is very good and your mind is able to be on the missing image

136
00:22:54,960 --> 00:23:05,040
of casina for a long time, so that is when you get Jana through the practice of earth

137
00:23:05,040 --> 00:23:14,680
casina, okay other casinas also, if you want to practice, say water casina, you may put

138
00:23:14,680 --> 00:23:24,960
some water in the pot and then look at it or you may build a fire and look at the fire

139
00:23:24,960 --> 00:23:39,960
through a hole made in the wall or somewhere and air casina, you may look at trees, the

140
00:23:39,960 --> 00:23:52,520
branches moving because of the air or you may feel the air that comes through the cracks

141
00:23:52,520 --> 00:24:02,160
in the wall or comes through a window and the blue casina, all right, that is a kala casina

142
00:24:02,160 --> 00:24:13,240
so you can make a disc or you can paint a circle and fill it with blue color or yellow

143
00:24:13,240 --> 00:24:29,200
color red color of white color, so in Burma, number five, Nila is translated as brown but

144
00:24:29,200 --> 00:24:40,840
in other countries it is translated as blue, so for bummies maybe they will paint brown

145
00:24:40,840 --> 00:24:49,520
and I think both are correct, so you can make any kara, any blue or brown, yellow red

146
00:24:49,520 --> 00:24:56,080
white and if you want to look at, if you want to practice space casina then you have

147
00:24:56,080 --> 00:25:05,340
to look at the space made by a hole or somewhere where there is space and the light

148
00:25:05,340 --> 00:25:13,840
casina, look at the light through the wall, I mean through the holes in the wall and

149
00:25:13,840 --> 00:25:22,320
so on, so you can take any one of them as the object of your meditation and practice

150
00:25:22,320 --> 00:25:34,880
samata on it and get the ubajara samadhi or the jana samadhi, the second group is called

151
00:25:34,880 --> 00:25:49,740
10 asubas, 10 fallness of the body, so by these practice people develop the sense of

152
00:25:49,740 --> 00:26:03,860
fallness in the corpses and also try to see the fallness in the living bodies, now there

153
00:26:03,860 --> 00:26:16,660
are 10 kinds of this fallness meditation depending on the 10 stages of decay in a corpse,

154
00:26:16,660 --> 00:26:30,340
now this meditation is not possible nowadays simply because we do not see corpses nowadays

155
00:26:30,340 --> 00:26:43,220
but during the time of the Buddha, the dead bodies are left at the cemetery to be decayed

156
00:26:43,220 --> 00:26:54,980
or to be eaten by dogs and birds and so it was possible to see corpses in different stages

157
00:26:54,980 --> 00:27:11,260
of decay, we can practice this kind of meditation by looking at pictures or just imagining

158
00:27:11,260 --> 00:27:22,860
seeing those pictures and then try to develop sense of fallness in the images as well

159
00:27:22,860 --> 00:27:37,460
as in our living bodies, so by practicing these fallness of the body meditation one can

160
00:27:37,460 --> 00:27:51,460
attain ubajara samadhi and fast jana only, so by the practice of this one cannot get second

161
00:27:51,460 --> 00:28:00,020
third, fourth and fifth janas, one can get only first jana, that is because the object

162
00:28:00,020 --> 00:28:10,060
is so gross that it always needs Vitaka, the initial application of mine to keep the

163
00:28:10,060 --> 00:28:23,060
mine on the object, so only the jana which is with taka, the initial application can be obtained

164
00:28:23,060 --> 00:28:33,900
in the practice of the fallness meditation, now we can just imagine about these corpses

165
00:28:33,900 --> 00:28:42,180
because nowadays we cannot see the corpses, so the first one is bloated corpse that means

166
00:28:42,180 --> 00:28:48,860
the cough that has become swelled and the second one is live it cause that means the cough

167
00:28:48,860 --> 00:29:00,380
that has become black and blue, number 3 is festering corpse that is oozing, I would

168
00:29:00,380 --> 00:29:14,500
have got that pus, oozing pus and number 4 is dismembered corpse, dismembered by enemies

169
00:29:14,500 --> 00:29:24,540
or vadara or dismembered and execution and so on, so we are not to be seen nowadays

170
00:29:24,540 --> 00:29:32,220
and number 5 is eaten corpse that is eaten by dogs, checkers, birds and so on, and number

171
00:29:32,220 --> 00:29:39,900
6 is scattered in pieces corpse, so after being eaten and the parts may be scattered

172
00:29:39,900 --> 00:29:47,860
here and there, and number 7 is mutilated and scattered in pieces corpse, again the body

173
00:29:47,860 --> 00:29:54,700
is mutilated and then parts pieces scattered here and there, number 8 is bloody corpse

174
00:29:54,700 --> 00:30:01,460
with blood oozing out, number 9 is warm infested corpse, and number 10 is scattered

175
00:30:01,460 --> 00:30:16,660
then, I think skeleton is possible or you can at least buy a plastic skeleton and practice

176
00:30:16,660 --> 00:30:28,980
farness meditation, although it is not the real one, I think it can help, so the 10

177
00:30:28,980 --> 00:30:41,340
baldness meditations are for getting rid of attachment to bodies, now the next group is

178
00:30:41,340 --> 00:30:49,180
called 10 annusities for 10 recollections, 10 kinds of recollections, so the number 1 is

179
00:30:49,180 --> 00:30:58,940
recollection of the Buddha, recollection of the Buddha means recollection of the qualities of the

180
00:30:58,940 --> 00:31:07,940
Buddha, the qualities of the Buddha, and traditionally there are 9 qualities of the Buddha

181
00:31:07,940 --> 00:31:22,860
and you can take any one of them or all 9 of them and dwell upon them again and again

182
00:31:22,860 --> 00:31:30,820
developing samadhi, recollection of the Dhamma means, recollection of the qualities

183
00:31:30,820 --> 00:31:37,580
of the Dhamma and the recollection of the Sangamins, the recollection of the qualities

184
00:31:37,580 --> 00:31:45,500
of the Sangam, and then number 4 is revelation of morality, that means recollection

185
00:31:45,500 --> 00:31:54,300
of one's own seala which is pure, so in order to practice this, first you have to be

186
00:31:54,300 --> 00:32:03,780
pure in your moral conduct, and then watching you in your moral conduct, a purity of

187
00:32:03,780 --> 00:32:15,900
moral conduct, you see my seala or my moral conduct is pure, and there are no breaks

188
00:32:15,900 --> 00:32:25,260
in the precepts and so on, and recollection of generosity, then you do some act of generosity

189
00:32:25,260 --> 00:32:37,100
and then recollect or contemplate on the generosity you have practiced, recollection

190
00:32:37,100 --> 00:32:46,180
of the divas, that means not thinking of the divas, that means recollection of the good

191
00:32:46,180 --> 00:33:08,260
qualities in you, like faith, generosity, wisdom and so on, but when you recollect on the

192
00:33:08,260 --> 00:33:19,660
good qualities in you, you make the divas, your witnesses or the evidence, that means

193
00:33:19,660 --> 00:33:27,580
these divas possessing these good qualities before they became divas, so these divas possessing

194
00:33:27,580 --> 00:33:38,220
these qualities become divas, and these qualities are in me also, so in that way first

195
00:33:38,220 --> 00:33:47,700
you think of the good qualities in the divas, and then you see those same good qualities

196
00:33:47,700 --> 00:33:54,500
in you, and that is called recollection of the divas, not recollection of the divas actually,

197
00:33:54,500 --> 00:34:03,340
but recollection of one's own good qualities taking divas as evidences, recollection

198
00:34:03,340 --> 00:34:12,740
of peace, means recollection of the peaceful qualities of nibana, it is said that this

199
00:34:12,740 --> 00:34:25,340
meditation can be successful, totally successful only for enlightened persons, but enlightened

200
00:34:25,340 --> 00:34:35,540
persons can also practice this meditation through hearsay, and recollection of dead,

201
00:34:35,540 --> 00:34:45,020
that you all know, that is about death which is certain and about life which is uncertain,

202
00:34:45,020 --> 00:34:53,700
and number nine is mindfulness of your pride with the body, that means recollection of the

203
00:34:53,700 --> 00:35:03,260
two parts of the body, and trying to see, let someness of these parts, and number ten

204
00:35:03,260 --> 00:35:10,340
is mindfulness of breathing, that means mindfulness of breathing in and out, so this mindfulness

205
00:35:10,340 --> 00:35:18,660
of breathing in and out can be a samata meditation, or vipasana meditation, depending

206
00:35:18,660 --> 00:35:26,020
on how you practice it.

207
00:35:26,020 --> 00:35:38,940
Now it is, when people practice, recollection of the Buddha in Bhama, they use a rosary,

208
00:35:38,940 --> 00:35:48,300
and there are nine attributes of the Buddha, and they see the attributes again and again with

209
00:35:48,300 --> 00:35:58,660
the rosary, so they may see at a ham, and then putting down one beat, samasambodha, another

210
00:35:58,660 --> 00:36:14,300
beat and so on, and the rosary has 108 beats, and so one round, to finish one round you

211
00:36:14,300 --> 00:36:30,060
see, and nine attributes, how many times, twelve times, so people use the rosaries for

212
00:36:30,060 --> 00:36:38,580
that purpose, but in the commentaries, there is no mention of rosaries when practicing

213
00:36:38,580 --> 00:36:44,780
recollection of the Buddha and so on, so you may or may not use rosaries when you practice

214
00:36:44,780 --> 00:36:57,780
the recollection of the Buddha and so on, so when you contemplate on the qualities of

215
00:36:57,780 --> 00:37:10,140
the Buddha, there is no attachment or no greed in your mind, there is no anger in your

216
00:37:10,140 --> 00:37:20,700
mind, there is no delusion in your mind, and so your mind becomes calm, calm down, and

217
00:37:20,700 --> 00:37:30,340
you get what is called upachara samadhi, nibhahut samadhi, although you get nibhahut

218
00:37:30,340 --> 00:37:45,100
samadhi, you will not get jhana through that meditation, it is because the qualities

219
00:37:45,100 --> 00:37:57,340
of the Buddhas are profound, and also the so many qualities that mind cannot be concentrated

220
00:37:57,340 --> 00:38:05,060
strong enough to get into the state of jhana, so by the regulation of, by the practice

221
00:38:05,060 --> 00:38:10,740
of the recollection of the Buddha, dharma, sangha, morality, generosity of the devas and

222
00:38:10,740 --> 00:38:18,820
the peace, you will get only nibhahut samadhi and not jhana samadhi, now here nibhahut

223
00:38:18,820 --> 00:38:27,500
samadhi, it is called nibhahut samadhi, because it resembles the real nibhahut samadhi,

224
00:38:27,500 --> 00:38:38,980
but since there is no jhana here, it is not the real nibhahut samadhi, how when you practice

225
00:38:38,980 --> 00:38:48,460
nibhana, mindfulness occupied with the body, you get both nibhahut samadhi and first jhana,

226
00:38:48,460 --> 00:38:55,380
because it is like the foundness meditation, so you get first jhana there, and mindfulness

227
00:38:55,380 --> 00:39:07,220
of breathing can lead to the attainment of both nibhahut concentration and all, that means

228
00:39:07,220 --> 00:39:14,900
for first to fifth jhana concentration.

229
00:39:14,900 --> 00:39:25,700
The next group is the phu apamanyas or the phu telemetables, not telemetables means without

230
00:39:25,700 --> 00:39:36,300
limits or limitless, these phu are more popularly known as phu bhama vyharas, phu

231
00:39:36,300 --> 00:39:47,260
defined abidings, when you practice any one of these, you are living like Brahma's, so

232
00:39:47,260 --> 00:39:57,580
they are called the living like Brahma's, the first one is loving kindness, you all know

233
00:39:57,580 --> 00:40:08,380
what loving kindness is, that is desire for the well-being of all beings, and compassion,

234
00:40:08,380 --> 00:40:18,860
compassion here means desire to remove the suffering of all beings, and number three

235
00:40:18,860 --> 00:40:26,980
appreciative joy, that means when other people are in heaviness, other people are prosperous,

236
00:40:26,980 --> 00:40:38,460
other people are successful, you are happy, it is equanimity, now number four is the most

237
00:40:38,460 --> 00:40:48,820
sublime of these four, and so when you practice equanimity, you do not feel loving kindness,

238
00:40:48,820 --> 00:40:58,220
or compassion, or appreciative joy, but you become neutral and you are not moved by either

239
00:40:58,220 --> 00:41:16,780
pleasant or unpleasant objects or either suffering or heaviness, these four are called

240
00:41:16,780 --> 00:41:30,860
limitless ones because the object of these phu that is beings must be without limits,

241
00:41:30,860 --> 00:41:39,340
that means when you practice loving kindness, meditation, you have to pervade all beings

242
00:41:39,340 --> 00:41:51,380
with thoughts of loving kindness, and compassion also you have to wish all beings in distress

243
00:41:51,380 --> 00:42:05,300
to be free from suffering, and number three you have to be happy with all beings who are

244
00:42:05,300 --> 00:42:15,580
in prosperity and success, and four and the equanimity also, you have to be neutral to

245
00:42:15,580 --> 00:42:28,980
all beings, so these four kinds of meditation take beings as the object, and the beings

246
00:42:28,980 --> 00:42:37,820
they take must be all beings without limits, and that is why they are called limitless

247
00:42:37,820 --> 00:42:47,460
ones, now when we practice loving kindness, meditation, first we begin with ourselves, and

248
00:42:47,460 --> 00:42:56,700
then we then thoughts to our teachers, to our parents and so on, so until we reach all

249
00:42:56,700 --> 00:43:05,020
beings, there is a limit, may my teachers, we were happy and these four, we are limiting

250
00:43:05,020 --> 00:43:13,060
to the teachers and so on, that is why we do not stop there, but we go until we reach

251
00:43:13,060 --> 00:43:22,540
all beings, so when you practice loving kindness, meditation, your mind must go to all beings

252
00:43:22,540 --> 00:43:32,340
without exception, and the same with compassion and so on, and practice of loving kindness

253
00:43:32,340 --> 00:43:42,620
can lead you to the attainment of neighborhood concentration, and then first to food channels,

254
00:43:42,620 --> 00:43:52,620
and passion the same appreciative joy, the same, and equanimity will lead you to the attainment

255
00:43:52,620 --> 00:44:01,980
of neighborhood concentration, and fifth chana concentration, now when you practice loving

256
00:44:01,980 --> 00:44:11,740
kindness, you say, you say to yourself, may all beings be happy and peaceful and so on,

257
00:44:11,740 --> 00:44:19,660
and what do you say when you practice compassion, now the object of compassion is beings

258
00:44:19,660 --> 00:44:31,100
who are suffering, so when you practice compassion, you say, may all beings, may all beings

259
00:44:31,100 --> 00:44:38,620
get free from this suffering, so may all beings get free from suffering, then what about

260
00:44:38,620 --> 00:44:53,540
number three, appreciative joy, sympathetic joy, sympathetic joy takes beings in prosperity

261
00:44:53,540 --> 00:45:01,100
of beings in success as objects, so you take them as objects against you, maybe not

262
00:45:01,100 --> 00:45:11,180
fall away from this prosperity, maybe not fall away from this heaviness, one, equanimity,

263
00:45:11,180 --> 00:45:23,820
and you want to practice equanimity, what do you say, in Bali you say kamasaka, beings

264
00:45:23,820 --> 00:45:32,580
have kama as their own property, that means you may do what you can see for beings, but

265
00:45:32,580 --> 00:45:40,620
if you cannot do any more and you have to accept it and you have to see all beings have

266
00:45:40,620 --> 00:45:48,300
kama as their property, and they suffer or they enjoy according to their own kama, and

267
00:45:48,300 --> 00:45:56,020
so there is no way of interfering with kama.

268
00:45:56,020 --> 00:46:04,620
The next group is only one, and that is one perception, perception of lotesomeness in

269
00:46:04,620 --> 00:46:25,260
food, now when those are thought of teaching beings, first he was decided not to teach,

270
00:46:25,260 --> 00:46:39,900
because his teachings were described as going upstream, so here this practice is to develop

271
00:46:39,900 --> 00:46:51,500
perception of lotesomeness in food, nobody will like this, this meditation, so when you

272
00:46:51,500 --> 00:47:01,820
go to your restaurant they will say enjoy your food, they want you to enjoy, one time I

273
00:47:01,820 --> 00:47:10,180
was at the retreat, and my attendant he was in America, so whenever he brought food to me

274
00:47:10,180 --> 00:47:22,660
and he said enjoy the food, but here Bora said don't enjoy the food, so you must develop

275
00:47:22,660 --> 00:47:31,580
lotesomeness in food, that is to do get rid of attachment to food, and lotesomeness

276
00:47:31,580 --> 00:47:39,940
of food and lotesomeness in food can be developed, thinking in many ways how you have

277
00:47:39,940 --> 00:47:51,220
to make effort to obtain, to get the food, and you have for a lady will, maybe you have

278
00:47:51,220 --> 00:48:00,660
to cook, and in cooking you have to suffer, and then you have to, monks have to go to the

279
00:48:00,660 --> 00:48:08,940
village and go around the village and accepting a little food at different houses, and

280
00:48:08,940 --> 00:48:17,580
then they have to look here and there to avoid straight dogs and oxen and horses and so

281
00:48:17,580 --> 00:48:27,420
on, and also when they eat, the food makes with saliva in their mouth, and there it becomes,

282
00:48:27,420 --> 00:48:34,140
if you can take some food you have chute outside, you will not want to put it again in your

283
00:48:34,140 --> 00:48:41,620
mouth, so load some of that time right, but when you do not look at it in your mouth

284
00:48:41,620 --> 00:48:49,620
it's okay, you can solo it, once you take it out you will not even want to see it, so

285
00:48:49,620 --> 00:48:59,780
lotesomeness of food should be developed in that way, but not so much that you throw up,

286
00:48:59,780 --> 00:49:11,660
but it is, it is a kind of meditation which helps you not to be attached to your food,

287
00:49:11,660 --> 00:49:22,860
and what concentration can you get from this meditation, only need to hold concentration,

288
00:49:22,860 --> 00:49:29,300
you cannot get jana concentration, because concentration cannot become strong enough to reach

289
00:49:29,300 --> 00:49:37,100
the stage of jana, you have to be thinking of different aspects of lotesomeness in food,

290
00:49:37,100 --> 00:49:44,060
and so your mind cannot be strongly concentrated, and so it cannot reach the stage of

291
00:49:44,060 --> 00:49:59,820
jana, and the next one is one analysis, that means analysis into the four elements, now

292
00:49:59,820 --> 00:50:07,540
in this meditation you can take the different parts of the body, like head hair, body hair,

293
00:50:07,540 --> 00:50:17,580
and then define them as this is at element, this is water element, and so on, there are

294
00:50:17,580 --> 00:50:30,500
32, 32 parts in the body, and 20 are said to be belong to at element, and the 12 to water

295
00:50:30,500 --> 00:50:41,220
element, so first you must understand the four elements, and then you try to understand

296
00:50:41,220 --> 00:50:55,260
that some characteristic is at element and so on, for example you feel the hardness

297
00:50:55,260 --> 00:51:03,380
somewhere here and there, and then you define that hardness as at element, and then

298
00:51:03,380 --> 00:51:12,780
you feel the cohesiveness or wetness, and then you define it as water element and so on,

299
00:51:12,780 --> 00:51:21,540
so you try to see the elements to everywhere your body included, and so you lose the sense

300
00:51:21,540 --> 00:51:36,900
of compactness, so it can help you to see another nature of things also, and by this meditation

301
00:51:36,900 --> 00:51:47,260
also you can get only neighborhood concentration and not jana concentration, because to

302
00:51:47,260 --> 00:51:54,860
understand the four elements is not easy, it is difficult, and they are profound, and so

303
00:51:54,860 --> 00:52:01,620
somebody cannot become strong enough to reach the state of jana, so let's group these

304
00:52:01,620 --> 00:52:12,380
the four immaterial states, now it is impossible to understand the immaterial states without

305
00:52:12,380 --> 00:52:23,140
a study of abidamam, so you have to study abidamam to understand the immaterial states,

306
00:52:23,140 --> 00:52:32,500
these are a kind of janas, this type of meditation leads to the attainment of what are

307
00:52:32,500 --> 00:52:46,260
called immaterial janas, so the yogis have to transcend the material states and concentrate

308
00:52:46,260 --> 00:52:56,140
on the infinite space and so on, so it is difficult to understand for those who are not

309
00:52:56,140 --> 00:53:04,780
initiated in abidamam, but I put them here because they are included in the 40 subjects

310
00:53:04,780 --> 00:53:16,300
of samatam meditation, so these are the 40 subjects of samatam meditation, so if you want

311
00:53:16,300 --> 00:53:24,140
to practice samatam meditation you can practice only one of them or a number of them, now

312
00:53:24,140 --> 00:53:41,500
among the 40 subjects, both are recommended for the monks to practice and to regard them

313
00:53:41,500 --> 00:53:56,500
as the protections, so these are called protections for monks, protection against mental

314
00:53:56,500 --> 00:54:11,500
departments, so these are the recollection of the Buddha, the collection of death, loving

315
00:54:11,500 --> 00:54:37,100
kindness and then foundness meditations and according to the commentaries, these 40 subjects

316
00:54:37,100 --> 00:54:55,820
of samatam meditation, what do you form in other teachings or at other times also and

317
00:54:55,820 --> 00:55:00,940
they are not like, we present our meditation, we present our meditation is found only during

318
00:55:00,940 --> 00:55:07,740
the time of the Buddha or we present again, we taught only by the Buddha, but samatam

319
00:55:07,740 --> 00:55:22,660
meditation was taught by other teachers also and so it is common to other teachings or other

320
00:55:22,660 --> 00:55:39,260
religions as well, so although our emphasis should be on we present our meditation, we are

321
00:55:39,260 --> 00:55:52,540
not to not to see that samatam meditation is nothing, samatam meditation is its own place

322
00:55:52,540 --> 00:56:02,380
in this spiritual path and also it has its value and that is why Buddha taught the samatam

323
00:56:02,380 --> 00:56:12,460
meditation also, the only thing we have to keep in mind is we are not to be satisfied with

324
00:56:12,460 --> 00:56:19,260
the practice of samatam meditation only, so we must practice samatam meditation whenever

325
00:56:19,260 --> 00:56:33,060
we can, but we must go to Vipasana so that we can get rid of mental defilements, so the aim

326
00:56:33,060 --> 00:56:39,540
of all Buddhism is to get rid of mental defilements and since it can be achieved only

327
00:56:39,540 --> 00:56:50,580
through Vipasana meditation, so Vipasana meditation becomes the top priority among the

328
00:56:50,580 --> 00:57:02,740
practices, but it does not mean that we must exclude samatana meditation from our practice,

329
00:57:02,740 --> 00:57:14,580
so if you can you should practice samatam of the samata also, you may want to know more

330
00:57:14,580 --> 00:57:22,220
about samatam meditation and if you want to know more about samatam meditation, I must

331
00:57:22,220 --> 00:57:36,780
refer you to Visodemaga, so Visodemaga is a book about 800 pages and samatam meditation

332
00:57:36,780 --> 00:57:47,620
may take about more than half, so if you are interested in understanding more about

333
00:57:47,620 --> 00:58:01,940
samatam meditation, you pick up Visodemaga and read it, okay.

