1
00:00:00,000 --> 00:00:06,520
Hello, and welcome back to our study of the Dhamapada.

2
00:00:06,520 --> 00:00:14,320
Today we continue on with verses 73 and 74, which read as follows.

3
00:00:14,320 --> 00:00:14,880
I will make up the

4
00:00:44,320 --> 00:01:02,600
Kitcha, Kitcha, Kitcha, Su, Kismiti, Iti, Balas, Sankapo, Itcha, Mano, Jai, Wadda, Titi,

5
00:01:02,600 --> 00:01:29,320
which means there's a lot in here. Santang, Babanamichi, Yeh, May they, you know, one might wish or one would wish, a fool would wish for people to hold them up for un for the kind of bavana.

6
00:01:29,320 --> 00:01:36,360
It's an interesting issue of the word bavana, bavana means cultivation. Cultivation that doesn't exist.

7
00:01:36,360 --> 00:01:42,480
They would wish for people to hold them up high falsely.

8
00:01:42,480 --> 00:01:55,120
Puri-kara, which means preeminence, bikusu, among monks. They would wish for eminence amongst other monks.

9
00:01:55,120 --> 00:02:00,600
So, to be number one, be the top.

10
00:02:00,600 --> 00:02:08,920
Ah, sesu, in regards to dwellings, they want to be Isariya, and they want to have power.

11
00:02:08,920 --> 00:02:15,720
They want to be in charge of dwellings, in charge of the monastery, in charge of monastery.

12
00:02:15,720 --> 00:02:29,680
Purja-pa-ra-kul-e-so-ja, and they wish to be, ah, Purja, they wish to be revered by other families, by lay people.

13
00:02:29,680 --> 00:02:38,120
And they think, mamewakata manyantu, may they think of it as, may they think that I have done it.

14
00:02:38,120 --> 00:02:52,560
May it be in people's minds, that I have done it, that it was done by me, both lay people and those who have gone forth.

15
00:02:52,560 --> 00:03:04,320
May they be under my power, in regards to, may the influence, may they rely upon me, in regards to what should be done and what should not be done.

16
00:03:04,320 --> 00:03:18,800
So, in regards to, ah, teachings, in regards to the, ah, exhortation, may they listen to me, may I be the one to tell people what to do.

17
00:03:18,800 --> 00:03:25,840
Kiss me, chi, every kind of, every kind of thing that should be done and should not be done.

18
00:03:25,840 --> 00:03:32,040
May they only listen to me, and may my word be the final word and what should not be done.

19
00:03:32,040 --> 00:03:37,360
Itimala sasankapur, these are the thoughts of the fool.

20
00:03:37,360 --> 00:03:49,760
Itchama, noja, one to tea, and such a person's, ah, desires and conceit increase.

21
00:03:49,760 --> 00:04:00,800
So, this is dealing with specifically ambition and conceit, these things that we can recognize in ourselves,

22
00:04:00,800 --> 00:04:08,720
they think many of us are quite interesting for religious figures, especially.

23
00:04:08,720 --> 00:04:18,880
So, story goes, that there was a lay person named Jita, who was a very nice person.

24
00:04:18,880 --> 00:04:27,720
They would listen to us, ah, sutta or discourse by Mahanama, one of the first five disciples of the Buddha,

25
00:04:27,720 --> 00:04:35,080
and became a sotapana, just by listening to the teaching and by, of course, practicing in regards to it,

26
00:04:35,080 --> 00:04:40,720
in, in, practicing based on the teaching.

27
00:04:40,720 --> 00:04:43,840
And as a result, became, of course, a devout Buddhist.

28
00:04:43,840 --> 00:04:49,000
And later on, heard the teachings of the two chief disciples.

29
00:04:49,000 --> 00:04:54,000
And so, our story begins with hymn listening to the two chief disciples,

30
00:04:54,000 --> 00:05:01,280
because once he became a devoted disciple of the Buddha, he looked after the monks,

31
00:05:01,280 --> 00:05:10,920
and he looked after one monk in particular, sudhamma, or a sudhamma came to live in the monastery near where Jita was staying.

32
00:05:10,920 --> 00:05:14,480
So, Jita was looking after this monk, sudhamma.

33
00:05:14,480 --> 00:05:20,760
And as things went, sudhamma, of course, became quite content with the arrangement,

34
00:05:20,760 --> 00:05:29,440
because Jita would have been attentive and kind and caring for the religious followers of the Buddha.

35
00:05:29,440 --> 00:05:35,520
And so, he would have gotten good food and nice dwelling, and all the things he needed to live as a monk

36
00:05:35,520 --> 00:05:38,800
would have been provided for him.

37
00:05:38,800 --> 00:05:43,720
And so, you can see where this is going, maybe guess.

38
00:05:43,720 --> 00:05:52,160
Jita, when one point listened to a talk by Sariputta, the two chief disciples came, and Sariputta gave this talk.

39
00:05:52,160 --> 00:05:58,800
And again, just by listening to the talk, he became a second agami, the second stage of enlightenment,

40
00:05:58,800 --> 00:06:03,320
by listening and by applying it, and practicing it.

41
00:06:03,320 --> 00:06:12,320
And he was so impressed, of course, and just elated that it immediately came to him that he should invite these two chief disciples of the Buddha

42
00:06:12,320 --> 00:06:15,840
to take the meal at his house.

43
00:06:15,840 --> 00:06:23,600
So, he went to them, and he bowed down, and he said, please accept the meal tomorrow at my house.

44
00:06:23,600 --> 00:06:32,040
And the two chief disciples accepted, they accepted by staying silent, which is sort of the way they would accept things

45
00:06:32,040 --> 00:06:40,360
and those monks would accept things, because he didn't want to seem to keen on it, make it appear like you were going to be a burden.

46
00:06:40,360 --> 00:06:46,520
He wanted to be clear, well, if this is what you want, then we will accept.

47
00:06:46,520 --> 00:06:56,000
But the other monk started, I guess, to have this sort of green-eyed monster arise in him.

48
00:06:56,000 --> 00:07:06,360
And then Jita, realizing that he missed something, he turned to the other elder, and he said, please, oh, please, you come as well.

49
00:07:06,360 --> 00:07:14,760
Almost as an afterthought, like he almost forgot, and Jita, the elder Sudhama wasn't happy about this.

50
00:07:14,760 --> 00:07:19,560
As a result, he got very angry inside, and he refused to go.

51
00:07:19,560 --> 00:07:23,800
He refused the invitation, he said, I'm not, you know, no, thank you.

52
00:07:23,800 --> 00:07:33,040
I said, oh, please, come, I would like you to come, I'm sorry, I meant to invite you as well.

53
00:07:33,040 --> 00:07:39,680
And again and again, he refused to go to the meal in the morning.

54
00:07:39,680 --> 00:07:48,040
So finally, Jita unable to persuade the elder left, and in the morning, the elder tried to stay and do his meditation,

55
00:07:48,040 --> 00:08:00,560
but couldn't get into a meditative state, so he made his way down to Jita's house, and his intention, he was so angry,

56
00:08:00,560 --> 00:08:03,560
his intention was actually to find fault with Jita.

57
00:08:03,560 --> 00:08:14,360
You know, if you aren't a recipient of people's good deeds, then, well, let's try and belittle them, right?

58
00:08:14,360 --> 00:08:24,360
If people are, this is a sign of a weak individual that they have to belittle the good deeds of others.

59
00:08:24,360 --> 00:08:33,360
And it feels threatened by them, so he went and he was looking for fault, and he criticized something, he said he's missing and I don't quite understand the nuance of it,

60
00:08:33,360 --> 00:08:39,960
but he said you're missing sesame cakes, which I guess is a real insult because you don't need sesame cakes.

61
00:08:39,960 --> 00:08:45,560
I mean, he had so many other foods that I guess it was meaningless that he had no sesame cakes.

62
00:08:45,560 --> 00:08:52,760
And so Jita turned around and called him a crow, and it sort of devolved from there,

63
00:08:52,760 --> 00:09:00,760
and the elder stormed off and went to see the Buddha and told him that Jita had, you know, called him a crow or something like that,

64
00:09:00,760 --> 00:09:13,760
and the Buddha turns around and blames it completely on the elder because this monk is being like behaving like a child, and he said you're the one who's blame, I don't blame Jita for what he said at all.

65
00:09:13,760 --> 00:09:24,760
He said you some silly little monk have insulted the true disciple, because Jita being even a layperson,

66
00:09:24,760 --> 00:09:33,760
the Buddha called him a true disciple because he was a sakadakami, he was actually enlightened individual.

67
00:09:33,760 --> 00:09:44,760
And so Dhamma realized his fault and realized that, oh, Jesus, the Buddha is blaming me for this one, better go and ask forgiveness.

68
00:09:44,760 --> 00:09:51,760
So he goes back to Jita to ask forgiveness, Jita doesn't forgive him, and he's confused as to why Jita refuses.

69
00:09:51,760 --> 00:09:59,760
He goes back, travels all the way alone, goes to see Jita and ask forgiveness, Jita refuses to forgive him,

70
00:09:59,760 --> 00:10:05,760
goes back to see the Buddha, the Buddha says to the Buddha, he refuses to accept my forgiveness.

71
00:10:05,760 --> 00:10:11,760
And the Buddha knows, knew that he wasn't going to forgive him, and knows why he didn't forgive him,

72
00:10:11,760 --> 00:10:13,760
and doesn't say anything.

73
00:10:13,760 --> 00:10:18,760
And he says, well, let's just let him stew for a while, and so Dhamma goes away, and he doesn't know what to do.

74
00:10:18,760 --> 00:10:23,760
The Buddha's anger, the Buddha is not angry, the Buddha is disappointed, and I'm not disappointed,

75
00:10:23,760 --> 00:10:35,760
but whatever you can say about the Buddha has disparaged him. Jita has disparaged him, he can't go back, he can't stay in the monastery.

76
00:10:35,760 --> 00:10:44,760
So finally, something's going inside of him, and he goes back to the Buddha, and he bows down, and he pleads, and he begs, and he says, please,

77
00:10:44,760 --> 00:10:53,760
I'm done a terrible thing, please, tell me how I can make this right.

78
00:10:53,760 --> 00:11:00,760
And the Buddha says, take a friend, go back to see Jita and ask forgiveness, but this time take a friend.

79
00:11:00,760 --> 00:11:07,760
And it's interesting that he says this, because when he goes back, Jita sees that there are two of them there, and he accepts it.

80
00:11:07,760 --> 00:11:14,760
It's almost as though there's a sense that the need to make it official.

81
00:11:14,760 --> 00:11:20,760
Like you really have to, it has to be clear that you have accepted the fault in this.

82
00:11:20,760 --> 00:11:27,760
And so by bringing a friend, there's a witness, and it's going to be known by the community.

83
00:11:27,760 --> 00:11:34,760
What happened? Like there was a witness to what happened, and Dhamma wouldn't be able to go away and say, it happened like this has happened like that.

84
00:11:34,760 --> 00:11:46,760
It was as though Jita was required a witness, because he had seen how this monk could be.

85
00:11:46,760 --> 00:11:54,760
Anyway, that's the origin story. It goes on and on after the, and then the Buddha then tells these two verses.

86
00:11:54,760 --> 00:12:02,760
And then the story goes on and on to tell about, I think past life stuff, but we're not so interested in that.

87
00:12:02,760 --> 00:12:05,760
Or there's more, he talks more about it anyway.

88
00:12:05,760 --> 00:12:11,760
What we're interested in is how these two verses relate to our practice.

89
00:12:11,760 --> 00:12:19,760
Again, it's a good verse for practice, because it's dealing with specific mind states.

90
00:12:19,760 --> 00:12:26,760
So we're dealing with, in this case, mainly with desire and conceit.

91
00:12:26,760 --> 00:12:36,760
But it's dealing with those desires in general to become something, to be something, to be special, and to be seen as special.

92
00:12:36,760 --> 00:12:44,760
We have this sense of a sort of a need to be something.

93
00:12:44,760 --> 00:12:48,760
We want people to look at us well.

94
00:12:48,760 --> 00:12:55,760
We want to have special people in our lives who smile when we enter the room and this kind of thing.

95
00:12:55,760 --> 00:12:58,760
People who look up to us.

96
00:12:58,760 --> 00:13:03,760
We tend to, of course, have this part of us that is, it has low self esteem.

97
00:13:03,760 --> 00:13:09,760
So we think little of ourselves, and as a result, need our ego to be boosted.

98
00:13:09,760 --> 00:13:17,760
And when like Sundama, it's our sense of importance is challenged.

99
00:13:17,760 --> 00:13:25,760
In jealousy arises, we get angry and upset.

100
00:13:25,760 --> 00:13:28,760
And so the first thing is the recognition.

101
00:13:28,760 --> 00:13:33,760
In regards to our practice, the first thing for us to do is to recognize these states when they come up.

102
00:13:33,760 --> 00:13:36,760
This is where meditation starts.

103
00:13:36,760 --> 00:13:43,760
It starts not by trying to change anything or to fix anything, but just to realize that we want these things.

104
00:13:43,760 --> 00:13:47,760
Sometimes it's in regards to people's esteem.

105
00:13:47,760 --> 00:13:50,760
Sometimes it's in regards to leadership.

106
00:13:50,760 --> 00:13:53,760
Wanting to be in control.

107
00:13:53,760 --> 00:13:55,760
Wanting to be the head of the house.

108
00:13:55,760 --> 00:13:57,760
Wanting to be the head of the monastery.

109
00:13:57,760 --> 00:13:59,760
Wanting to be the head of the family.

110
00:13:59,760 --> 00:14:00,760
Head of the company.

111
00:14:00,760 --> 00:14:04,760
Wanting people to notice the good work that you've done.

112
00:14:04,760 --> 00:14:06,760
Wanting to become something.

113
00:14:06,760 --> 00:14:12,760
It's really Baba Tanha that we're talking about, which is the desire for something to come in the future.

114
00:14:12,760 --> 00:14:19,760
It's the desire for being, or in the present, something you've gotten to not lose it.

115
00:14:19,760 --> 00:14:21,760
Boudja, we want to be worshiped.

116
00:14:21,760 --> 00:14:23,760
We want people to pay reverence.

117
00:14:23,760 --> 00:14:26,760
Well, this is more of course talking about monks.

118
00:14:26,760 --> 00:14:29,760
I think most lay people are at that level where they want to be worshiped.

119
00:14:29,760 --> 00:14:31,760
But monks fall into this.

120
00:14:31,760 --> 00:14:39,760
It's quite obscene how people can get off on this worship thing.

121
00:14:39,760 --> 00:14:44,760
They want wanting to people to bow down and feeling insulted when people don't bow down.

122
00:14:44,760 --> 00:14:47,760
It's hard as a monk you get used to people.

123
00:14:47,760 --> 00:14:53,760
If you live in a Buddhist society, you get used to people bowing to you and respecting you and being disrespectful.

124
00:14:53,760 --> 00:15:00,760
In general, people will respect religious individuals in Buddhist countries.

125
00:15:00,760 --> 00:15:09,760
Then you come to the west and they're like, hey, how are you?

126
00:15:09,760 --> 00:15:12,760
Good people asking you all sorts of strange questions.

127
00:15:12,760 --> 00:15:14,760
Like can monks get married?

128
00:15:14,760 --> 00:15:19,760
I've had women come up to, you know, women who get to know me and then start asking questions.

129
00:15:19,760 --> 00:15:22,760
I'm like, can monks get married?

130
00:15:22,760 --> 00:15:27,760
Are you going to stay a monk forever?

131
00:15:27,760 --> 00:15:30,760
No, but I'm a little bit off track.

132
00:15:30,760 --> 00:15:36,760
You're just talking specifically about the sort of sense of, it's quite common.

133
00:15:36,760 --> 00:15:39,760
It's interesting going from Thailand to Sri Lanka in Sri Lanka.

134
00:15:39,760 --> 00:15:45,760
I think people treat monks a lot more reasonably.

135
00:15:45,760 --> 00:15:48,760
And maybe recently is the bad word, but they treat people.

136
00:15:48,760 --> 00:15:50,760
The treat monks differently in Thailand.

137
00:15:50,760 --> 00:15:52,760
There's a real sense of reverence.

138
00:15:52,760 --> 00:15:57,760
Sometimes it's fake, sometimes it's just a show.

139
00:15:57,760 --> 00:16:03,760
But when you go to Sri Lanka, it's quite a shock to have people treat you like an ordinary individual.

140
00:16:03,760 --> 00:16:05,760
Monks are treated like.

141
00:16:05,760 --> 00:16:08,760
It's sort of like how you'd imagine they should be.

142
00:16:08,760 --> 00:16:11,760
They would have been in the Buddhist time.

143
00:16:11,760 --> 00:16:21,760
The Buddha himself was treated like a bam, you know, a beggar, times.

144
00:16:21,760 --> 00:16:24,760
Wanting people to know the deeds that you have done.

145
00:16:24,760 --> 00:16:26,760
Lay people fall into this as well.

146
00:16:26,760 --> 00:16:29,760
They go to monasteries and they donate this or they donate that.

147
00:16:29,760 --> 00:16:35,760
And they have a big celebration and they have to put themselves up at the front and they make speeches.

148
00:16:35,760 --> 00:16:38,760
They tell everyone, you know, it's hard to, it's hard.

149
00:16:38,760 --> 00:16:42,760
You don't want to sometimes, but you can see people, they somehow make it slip in.

150
00:16:42,760 --> 00:16:48,760
Or when someone else mentions, you know, it mentions it for them, they feel proud and puffed up.

151
00:16:48,760 --> 00:16:51,760
It's very dangerous. I mean, we fall into it.

152
00:16:51,760 --> 00:16:53,760
These aren't, I'm not talking about people being evil.

153
00:16:53,760 --> 00:16:56,760
We all have these things inside of us that we have to work on.

154
00:16:56,760 --> 00:17:01,760
Wanting people to be under your power.

155
00:17:01,760 --> 00:17:05,760
Wanting people to come to you and to have you as the,

156
00:17:05,760 --> 00:17:13,760
wanting to be the source of what's right and wrong, you know, to have the last say on things.

157
00:17:13,760 --> 00:17:17,760
So you get to say what should be done and what shouldn't be done.

158
00:17:17,760 --> 00:17:19,760
In the monastery.

159
00:17:19,760 --> 00:17:21,760
Very undemocratic.

160
00:17:21,760 --> 00:17:22,760
The sort of thing.

161
00:17:22,760 --> 00:17:25,760
I suppose this is not universal.

162
00:17:25,760 --> 00:17:29,760
Of course, some people are very good team players or don't want to be in charge.

163
00:17:29,760 --> 00:17:32,760
Some people want to hide in the background.

164
00:17:32,760 --> 00:17:34,760
I was like working behind the scenes.

165
00:17:34,760 --> 00:17:41,760
I think it's a very, it's so liberating to work behind the scenes because you don't have to work.

166
00:17:41,760 --> 00:17:42,760
You don't have to.

167
00:17:42,760 --> 00:17:44,760
You can't get puffed up in the same way.

168
00:17:44,760 --> 00:17:48,760
You can be proud of what you do behind the scenes and you can boast about it as well.

169
00:17:48,760 --> 00:17:53,760
But working behind the scenes is always more enjoyable.

170
00:17:53,760 --> 00:18:01,760
In Thailand, they call it a sticking gold on the back of the Buddha.

171
00:18:01,760 --> 00:18:05,760
So in Thailand, they put gold foil on the Buddha.

172
00:18:05,760 --> 00:18:14,760
And it's the same, so it's just a practice, but the saying is, it's an Indian.

173
00:18:14,760 --> 00:18:18,760
When you put gold on the back of the Buddha means doing something without what no one knows.

174
00:18:18,760 --> 00:18:21,760
Doing a good deed and not letting anyone know about it.

175
00:18:21,760 --> 00:18:34,760
It's actually interesting because it's not that we shouldn't, all of this is not to say that we shouldn't be famous or

176
00:18:34,760 --> 00:18:41,760
powerful, that we shouldn't do things in front of others, that we shouldn't do good deeds and let others know.

177
00:18:41,760 --> 00:18:44,760
All of this is in fact a good thing.

178
00:18:44,760 --> 00:18:49,760
Especially doing good deeds and letting people know about it.

179
00:18:49,760 --> 00:18:52,760
Letting people know about your good deeds is an awesome thing.

180
00:18:52,760 --> 00:18:54,760
It's just very dangerous, you see.

181
00:18:54,760 --> 00:18:56,760
The question is why are you doing it?

182
00:18:56,760 --> 00:18:57,760
Why is it a good thing?

183
00:18:57,760 --> 00:19:01,760
It's a good thing because it allows people to appreciate it.

184
00:19:01,760 --> 00:19:04,760
So Dhamma here should have been appreciating.

185
00:19:04,760 --> 00:19:12,760
He should have been overjoyed that Jita was, this lay person was inviting these monks who would have, you know,

186
00:19:12,760 --> 00:19:19,760
if he had stayed, he could have learned something wonderful, profound from these two enlightened beings.

187
00:19:19,760 --> 00:19:29,760
Instead, even then, you see, Jita, this example of Jita is still an interesting example because even if it makes people jealous,

188
00:19:29,760 --> 00:19:38,760
or often afraid to Buddhist especially, I think, can often be afraid to announce their good deeds because people would feel jealous

189
00:19:38,760 --> 00:19:42,760
or think they're just boasting or showing off.

190
00:19:42,760 --> 00:19:50,760
Even then, it's an opportunity for people to see and to come to terms with their own jealousy as Dhamma in the end hand.

191
00:19:50,760 --> 00:19:52,760
We should never be afraid of goodness.

192
00:19:52,760 --> 00:19:59,760
But this is a quote from Dhamma, be away by Yitapunyan and don't be afraid of good deeds.

193
00:19:59,760 --> 00:20:01,760
And certainly don't be afraid to let others know.

194
00:20:01,760 --> 00:20:05,760
It doesn't mean you always have to let people know.

195
00:20:05,760 --> 00:20:15,760
It's not something that is wrong because it allows others to appreciate and to take it as an example.

196
00:20:15,760 --> 00:20:22,760
Let's people feel what we call Mudita. Mudita is the third of the Brahma we hire us.

197
00:20:22,760 --> 00:20:26,760
And it's an opportunity for others to do good deeds themselves.

198
00:20:26,760 --> 00:20:29,760
Being powerful, being famous.

199
00:20:29,760 --> 00:20:34,760
I think sometimes about the internet, it's interesting how easy it is to become famous on the internet.

200
00:20:34,760 --> 00:20:40,760
And I think that's an example of potentially what the Buddha said, Asan Pang Bhawanam,

201
00:20:40,760 --> 00:20:49,760
in verse 73, where he says this false gains.

202
00:20:49,760 --> 00:20:53,760
Or he wanted people to pump you up falsely.

203
00:20:53,760 --> 00:20:57,760
So the internet is a chance to become famous when you may not even deserve it.

204
00:20:57,760 --> 00:21:04,760
I always think about these teachers in Asia who work so hard and the world may never know about them.

205
00:21:04,760 --> 00:21:06,760
Because they're doing their thing.

206
00:21:06,760 --> 00:21:14,760
All you have to do is go on YouTube and post some videos and suddenly you're famous.

207
00:21:14,760 --> 00:21:17,760
There's nothing wrong with fame. There's nothing wrong with power.

208
00:21:17,760 --> 00:21:20,760
There's nothing wrong with being in charge of a monastery.

209
00:21:20,760 --> 00:21:23,760
There's nothing wrong with people looking up to you.

210
00:21:23,760 --> 00:21:28,760
There's nothing wrong with being the boss in a company or this kind of thing.

211
00:21:28,760 --> 00:21:31,760
It's the wishes. It's your state of mind.

212
00:21:31,760 --> 00:21:33,760
And that's where meditation starts.

213
00:21:33,760 --> 00:21:38,760
And that's where it ends. Meditation is all about our state of mind.

214
00:21:38,760 --> 00:21:43,760
And it's not usually what you do, but how you do it.

215
00:21:43,760 --> 00:21:48,760
So that's what our training and meditation is.

216
00:21:48,760 --> 00:21:52,760
We're training ourselves so that when we walk, we're just walking.

217
00:21:52,760 --> 00:21:54,760
We just walk back and forth.

218
00:21:54,760 --> 00:21:59,760
And the point is to learn how to do something mindfully.

219
00:21:59,760 --> 00:22:02,760
To learn how to do something with a clear mind.

220
00:22:02,760 --> 00:22:08,760
The idea is you start with just taking steps.

221
00:22:08,760 --> 00:22:12,760
That seems kind of silly, but this is where we start.

222
00:22:12,760 --> 00:22:15,760
If you can do this, then the next step.

223
00:22:15,760 --> 00:22:19,760
You can take it into your life and you can talk to people mindfully.

224
00:22:19,760 --> 00:22:21,760
You can listen mindfully.

225
00:22:21,760 --> 00:22:23,760
You can work mindfully.

226
00:22:23,760 --> 00:22:26,760
You can live your life mindfully.

227
00:22:26,760 --> 00:22:29,760
You can learn how to do things.

228
00:22:29,760 --> 00:22:35,760
And with a pure mind, you can be a boss mindfully.

229
00:22:35,760 --> 00:22:37,760
You can run a monastery mindfully.

230
00:22:37,760 --> 00:22:40,760
You can be famous mindfully.

231
00:22:40,760 --> 00:22:42,760
All of this is possible.

232
00:22:42,760 --> 00:22:44,760
It takes the training of the mind.

233
00:22:44,760 --> 00:22:46,760
Meditation is simply that.

234
00:22:46,760 --> 00:22:50,760
It's nothing more, nothing less than the training of the mind.

235
00:22:50,760 --> 00:22:58,760
Training of the mind to be pure, to be clear, to be calm, to be clean.

236
00:22:58,760 --> 00:23:02,760
So that's how this verse relates.

237
00:23:02,760 --> 00:23:06,760
When we are in these positions, all you need is a clear mind.

238
00:23:06,760 --> 00:23:10,760
Conceited is not something that you can easily be mindful of.

239
00:23:10,760 --> 00:23:12,760
Because the conceited mind is unmindful.

240
00:23:12,760 --> 00:23:18,760
The same goes with desire, but desire is something you can actually feel more viscerally.

241
00:23:18,760 --> 00:23:23,760
And it's easier to be mindful wanting, wanting to remind yourself this is just wanting.

242
00:23:23,760 --> 00:23:28,760
If you have conceited, you can sometimes acknowledge knowing, knowing that you're conceited,

243
00:23:28,760 --> 00:23:30,760
or so on.

244
00:23:30,760 --> 00:23:35,760
If you get this feeling, this puffed up feeling in your test, you can acknowledge feeling,

245
00:23:35,760 --> 00:23:38,760
feeling as well.

246
00:23:38,760 --> 00:23:40,760
These are not good things.

247
00:23:40,760 --> 00:23:47,760
Conceited, as you can see, what leads to jealousy, what leads to anger, when you aren't

248
00:23:47,760 --> 00:23:54,760
appreciated, greed wanting to be ambition.

249
00:23:54,760 --> 00:23:56,760
People think, well, is it wrong to have ambition?

250
00:23:56,760 --> 00:23:59,760
I say, yeah, it's pretty wrong to have ambition.

251
00:23:59,760 --> 00:24:01,760
Mission is habit-forming.

252
00:24:01,760 --> 00:24:08,760
It makes you worry about and cling to good things.

253
00:24:08,760 --> 00:24:18,760
Cling to good states, and they're much better off to just live your life simply.

254
00:24:18,760 --> 00:24:23,760
If you have enough, if you are surviving, if you are alive and well,

255
00:24:23,760 --> 00:24:27,760
then able to develop yourself, able to better yourself,

256
00:24:27,760 --> 00:24:30,760
able to do good things, then that's enough.

257
00:24:30,760 --> 00:24:33,760
You don't need ambition.

258
00:24:33,760 --> 00:24:39,760
It allows us, as you weaken your ambition and strengthen your resolve.

259
00:24:39,760 --> 00:24:42,760
This is an interesting saying.

260
00:24:42,760 --> 00:24:44,760
Sounds sort of Buddhist.

261
00:24:44,760 --> 00:24:46,760
Anyway, that's all for today.

262
00:24:46,760 --> 00:24:49,760
Thank you for tuning in.

263
00:24:49,760 --> 00:24:51,760
We'll try and keep this up.

264
00:24:51,760 --> 00:24:55,760
We'll get through the Dhamabada as much, and as quickly as we can.

265
00:24:55,760 --> 00:24:56,760
Thank you.

266
00:24:56,760 --> 00:25:03,760
Thank you all, peace, happiness, and freedom.

