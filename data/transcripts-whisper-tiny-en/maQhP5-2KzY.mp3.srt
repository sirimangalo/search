1
00:00:00,000 --> 00:00:06,000
I said, meditation helped me to overcome myself in a way that the fear of death doesn't bother me.

2
00:00:06,000 --> 00:00:12,000
But people close to me are dying, and I have the urge to force Buddhism on them in my life.

3
00:00:12,000 --> 00:00:15,000
But am I in a position to help?

4
00:00:15,000 --> 00:00:21,000
Well, forcing things on people or yourself is non-Buddhist.

5
00:00:21,000 --> 00:00:25,000
So, you can't force Buddhism on people.

6
00:00:25,000 --> 00:00:29,000
It's a kind of oxymoronic or paradoxical.

7
00:00:29,000 --> 00:00:32,000
I don't know what you call it, but it's hypocritical, I suppose.

8
00:00:32,000 --> 00:00:34,000
Or it's counter-productive.

9
00:00:34,000 --> 00:00:35,000
So word.

10
00:00:35,000 --> 00:00:38,000
It's like forcing someone to let go.

11
00:00:38,000 --> 00:00:39,000
Let go.

12
00:00:39,000 --> 00:00:40,000
Let go.

13
00:00:40,000 --> 00:00:43,000
Make yourself let go kind of thing.

14
00:00:43,000 --> 00:00:45,000
You can't do it.

15
00:00:45,000 --> 00:00:49,000
Letting go has to come naturally.

16
00:00:49,000 --> 00:00:52,000
It has to come for oneself.

17
00:00:52,000 --> 00:00:56,000
So the best you can do is be an example for people.

18
00:00:56,000 --> 00:01:02,000
You'll be like a shady tree that people can rest under if they're feeling hot.

19
00:01:02,000 --> 00:01:05,000
But only if they're feeling hot.

20
00:01:05,000 --> 00:01:08,000
They'll come to it themselves.

21
00:01:08,000 --> 00:01:24,000
If they have questions and one answer, you can answer if you know the answer.

