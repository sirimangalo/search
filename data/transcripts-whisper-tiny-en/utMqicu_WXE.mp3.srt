1
00:00:00,000 --> 00:00:12,000
Okay, good evening everyone.

2
00:00:12,000 --> 00:00:21,000
Small crowd tonight.

3
00:00:21,000 --> 00:00:23,000
It's a great thing about giving daily talks.

4
00:00:23,000 --> 00:00:24,000
People can just come and go.

5
00:00:24,000 --> 00:00:26,000
You don't need a large crowd.

6
00:00:26,000 --> 00:00:31,000
Everybody doesn't have to get together all at once.

7
00:00:31,000 --> 00:00:35,000
Of course the other great thing is the meditators here get some

8
00:00:35,000 --> 00:00:40,000
little bit of dumb my every day.

9
00:00:40,000 --> 00:00:43,000
So I thought I'd spend some time going over the,

10
00:00:43,000 --> 00:00:50,000
I thought it might be good to begin to look at some of the

11
00:00:50,000 --> 00:00:52,000
basics of Buddhism.

12
00:00:52,000 --> 00:00:56,000
Some of the things we don't often talk about,

13
00:00:56,000 --> 00:01:00,000
because there's an assumption that people already know

14
00:01:00,000 --> 00:01:03,000
about them I think.

15
00:01:03,000 --> 00:01:05,000
And the side of them.

16
00:01:05,000 --> 00:01:09,000
Let me get into all sorts of specific teachings and

17
00:01:09,000 --> 00:01:12,000
or I do.

18
00:01:12,000 --> 00:01:16,000
I thought it might be nice to go over the basics because

19
00:01:16,000 --> 00:01:21,000
even for those people for whom it is,

20
00:01:21,000 --> 00:01:24,000
quite familiar.

21
00:01:24,000 --> 00:01:29,000
These teachings are still important to reflect upon

22
00:01:29,000 --> 00:01:35,000
and remind ourselves about.

23
00:01:35,000 --> 00:01:37,000
It's because you heard a teaching before.

24
00:01:37,000 --> 00:01:39,000
It doesn't mean you actually know it.

25
00:01:39,000 --> 00:01:43,000
It's quite different from actually understanding it.

26
00:01:43,000 --> 00:01:48,000
So hearing it again and again is repeated.

27
00:01:48,000 --> 00:01:50,000
So of course I thought we'd start with the four

28
00:01:50,000 --> 00:01:55,000
noble truths.

29
00:01:55,000 --> 00:02:00,000
They are the four noble truths of the prize.

30
00:02:00,000 --> 00:02:02,000
If you want to ask yourself,

31
00:02:02,000 --> 00:02:05,000
what are we trying to learn?

32
00:02:05,000 --> 00:02:07,000
What's the goal of meditation?

33
00:02:07,000 --> 00:02:10,000
Why are we doing this again?

34
00:02:10,000 --> 00:02:13,000
The answer is the four noble truths.

35
00:02:13,000 --> 00:02:17,000
To understand the four noble truths and more

36
00:02:17,000 --> 00:02:22,000
the answer lies in the four noble truths.

37
00:02:22,000 --> 00:02:26,000
Which have everything really to do with understanding.

38
00:02:26,000 --> 00:02:29,000
I mean there's much more to,

39
00:02:29,000 --> 00:02:33,000
it's more complicated than that.

40
00:02:33,000 --> 00:02:36,000
But the real key is understanding.

41
00:02:36,000 --> 00:02:43,000
It has to do with learning something.

42
00:02:50,000 --> 00:02:54,000
And so I talk about our situation as humans.

43
00:02:54,000 --> 00:02:57,000
Our situation as humans is in so many ways

44
00:02:57,000 --> 00:03:00,000
not what we think it is.

45
00:03:00,000 --> 00:03:06,000
We don't realize how much we're suffering.

46
00:03:06,000 --> 00:03:10,000
We think suffering is an extreme such condition

47
00:03:10,000 --> 00:03:13,000
that you can avoid for the most part.

48
00:03:13,000 --> 00:03:16,000
If you're unlucky you, it's only if you're unlucky

49
00:03:16,000 --> 00:03:23,000
that you can't avoid it.

50
00:03:23,000 --> 00:03:25,000
The four noble truths are so simple.

51
00:03:25,000 --> 00:03:29,000
It seems like something it should be easy to understand.

52
00:03:29,000 --> 00:03:33,000
In fact it's not incredibly difficult.

53
00:03:33,000 --> 00:03:37,000
Maybe it is the most difficult thing we could ever do,

54
00:03:37,000 --> 00:03:43,000
but it's still surprisingly doable.

55
00:03:43,000 --> 00:03:52,000
It's not impossible.

56
00:03:52,000 --> 00:03:59,000
But it's remarkable how ignorant we are.

57
00:03:59,000 --> 00:04:06,000
We live our lives not realizing how much we don't know.

58
00:04:06,000 --> 00:04:11,000
Just about ourselves.

59
00:04:11,000 --> 00:04:19,000
We don't even have this capacity to realize that we're ignorant.

60
00:04:19,000 --> 00:04:25,000
True ignorance is not even realizing how ignorant you are.

61
00:04:25,000 --> 00:04:27,000
You might hear about Buddhism.

62
00:04:27,000 --> 00:04:30,000
Most of them we begin to meditate.

63
00:04:30,000 --> 00:04:36,000
We have this idea that we're going to learn something.

64
00:04:36,000 --> 00:04:41,000
But it's shocking just how much we learn.

65
00:04:41,000 --> 00:04:51,000
It's being pulled out from drowning underwater

66
00:04:51,000 --> 00:04:57,000
and being pulled out of the pond.

67
00:04:57,000 --> 00:05:01,000
Suddenly you can breathe again.

68
00:05:01,000 --> 00:05:04,000
It's like you weren't alive before.

69
00:05:04,000 --> 00:05:10,000
Like you couldn't see before.

70
00:05:10,000 --> 00:05:13,000
So the first thing to understand about the four noble truths

71
00:05:13,000 --> 00:05:19,000
and Buddhism by extension is that it's about learning and understanding.

72
00:05:19,000 --> 00:05:35,000
Understanding the four noble truths is paramount.

73
00:05:35,000 --> 00:05:40,000
The next thing to know about the four noble truths

74
00:05:40,000 --> 00:05:45,000
is that what they describe is cause and defect.

75
00:05:45,000 --> 00:05:48,000
I don't think we have to be too,

76
00:05:48,000 --> 00:05:55,000
but it's often here going on about the importance of cause and defect.

77
00:05:55,000 --> 00:05:58,000
It's not really that big of a deal.

78
00:05:58,000 --> 00:06:02,000
What's more important is that we're talking about the cause of suffering.

79
00:06:02,000 --> 00:06:09,000
We do and the Buddha did and monks didn't go on about cause and defect as being important.

80
00:06:09,000 --> 00:06:14,000
Because there are people out there who don't understand about causality.

81
00:06:14,000 --> 00:06:22,000
It's a terrible wrong view to believe that things just happen randomly or by chance or by magic.

82
00:06:22,000 --> 00:06:29,000
A lot of our meditation is about understanding cause and defect

83
00:06:29,000 --> 00:06:32,000
in the sense that we're not in control.

84
00:06:32,000 --> 00:06:38,000
We can magically make things go away or make things come.

85
00:06:38,000 --> 00:06:43,000
Reality happens according to cause and defect.

86
00:06:43,000 --> 00:06:47,000
But for the four noble truths, it's somewhat more specific than that.

87
00:06:47,000 --> 00:06:52,000
I think we're not just talking about an understanding of cause and defect generally.

88
00:06:52,000 --> 00:06:58,000
Talking about the very specific understanding of what causes suffering.

89
00:06:58,000 --> 00:07:04,000
It's a simple formula.

90
00:07:04,000 --> 00:07:10,000
It's describing a problem and the cause of the problem.

91
00:07:10,000 --> 00:07:15,000
There's nothing really novel about that formula.

92
00:07:15,000 --> 00:07:17,000
There's a cause to the problem.

93
00:07:17,000 --> 00:07:18,000
Of course there is.

94
00:07:18,000 --> 00:07:20,000
It's not something new.

95
00:07:20,000 --> 00:07:24,000
Hey, suffering has a cause already.

96
00:07:24,000 --> 00:07:27,000
We're pretty clear about that.

97
00:07:27,000 --> 00:07:31,000
I think.

98
00:07:31,000 --> 00:07:33,000
And the cessation of suffering.

99
00:07:33,000 --> 00:07:36,000
So there's a way out of suffering.

100
00:07:36,000 --> 00:07:40,000
It can be free from suffering and there's a way to be free.

101
00:07:40,000 --> 00:07:47,000
The problem comes as we don't know the cause of either.

102
00:07:47,000 --> 00:07:51,000
So if you asked someone who knew nothing about Buddhism,

103
00:07:51,000 --> 00:07:55,000
even someone who did, because most people don't agree with Buddhism.

104
00:07:55,000 --> 00:08:01,000
Or don't have the understanding that it's true.

105
00:08:01,000 --> 00:08:04,000
Yes, then what is the cause of suffering?

106
00:08:04,000 --> 00:08:07,000
Ask a doctor who did the cause of suffering.

107
00:08:07,000 --> 00:08:11,000
Don't give you a complicated answer, I think.

108
00:08:11,000 --> 00:08:13,000
Ask yourself, right?

109
00:08:13,000 --> 00:08:22,000
What's the cause of suffering?

110
00:08:22,000 --> 00:08:26,000
Asotapana sees this, sees what is the cause of suffering.

111
00:08:26,000 --> 00:08:28,000
You really see it.

112
00:08:28,000 --> 00:08:30,000
But they can still get them.

113
00:08:30,000 --> 00:08:33,000
And they're still not clear, 100%.

114
00:08:33,000 --> 00:08:37,000
An aura and only an aura is completely clear.

115
00:08:37,000 --> 00:08:41,000
Because the cause of suffering.

116
00:08:41,000 --> 00:08:45,000
They understand perfectly clear what the Buddha taught.

117
00:08:45,000 --> 00:08:55,000
To be the cause of suffering.

118
00:08:55,000 --> 00:09:03,000
It even sounds, I think, a bit strange to say that craving is the cause of suffering, right?

119
00:09:03,000 --> 00:09:08,000
I think that craving is a good thing.

120
00:09:08,000 --> 00:09:30,000
And then we have this remarkable teaching about the cessation of suffering.

121
00:09:30,000 --> 00:09:34,000
I mean, the cessation of suffering itself is remarkable.

122
00:09:34,000 --> 00:09:38,000
It's not really what you think.

123
00:09:38,000 --> 00:09:42,000
Again, our idea of what is suffering and what is the cause of suffering.

124
00:09:42,000 --> 00:09:48,000
And what is the cessation of suffering is very different from the Buddha's.

125
00:09:48,000 --> 00:09:50,000
What is the cessation of suffering?

126
00:09:50,000 --> 00:09:54,000
Well, you know, if this headache would go away, or if my depression would go away,

127
00:09:54,000 --> 00:10:03,000
or if you would go away, that would be the end of suffering.

128
00:10:03,000 --> 00:10:07,000
I'm going to realize what the Buddha is actually talking about.

129
00:10:07,000 --> 00:10:13,000
And this is a surprise, I think, even up to the moment when you experience it.

130
00:10:13,000 --> 00:10:20,000
And you realize, wow, it's really possible for suffering completely deceased.

131
00:10:20,000 --> 00:10:28,000
It's possible to have complete freedom from suffering.

132
00:10:28,000 --> 00:10:35,000
It's even bigger problem is that we think of suffering as pleasant.

133
00:10:35,000 --> 00:10:40,000
Those things that are unsatisfying, I think of the most satisfying things that are stressful,

134
00:10:40,000 --> 00:10:44,000
we think of them as as good.

135
00:10:44,000 --> 00:10:53,000
So I told you that the Buddha gives up all the things that we want them cling to.

136
00:10:53,000 --> 00:10:58,000
It would be very frightening for most people, horrifying of that.

137
00:10:58,000 --> 00:11:10,000
It would feel like it was denying yourself the pleasure, the happiness.

138
00:11:10,000 --> 00:11:20,000
But as remarkable as that, the path is quite remarkable, I think.

139
00:11:20,000 --> 00:11:28,000
What's the path? What do you have to do to become free from suffering?

140
00:11:28,000 --> 00:11:31,000
It's not even a simple answer.

141
00:11:31,000 --> 00:11:41,000
We can simplify it, but it's quite complicated. There are eight parts.

142
00:11:41,000 --> 00:11:44,000
We'll talk about that. I'll go through each of these.

143
00:11:44,000 --> 00:11:46,000
I'd like to spend a day on each of these.

144
00:11:46,000 --> 00:11:53,000
Again, these won't be long talks, I don't think, but go over each one of them individually.

145
00:11:53,000 --> 00:11:56,000
One day, one talk, one truth.

146
00:11:56,000 --> 00:12:03,000
I thought before that, I'd spend a little time going over them all together.

147
00:12:03,000 --> 00:12:07,000
So we're talking about wisdom, we're talking about cause and defect,

148
00:12:07,000 --> 00:12:15,000
but specifically cause of suffering and the path that leads to the cessation of suffering.

149
00:12:15,000 --> 00:12:22,000
It's really like a doctor, a doctor identifies the illness

150
00:12:22,000 --> 00:12:33,000
and then finds the cause and then prescribes a course of medication,

151
00:12:33,000 --> 00:12:37,000
course of what you call practice.

152
00:12:37,000 --> 00:12:46,000
I guess are a course of, I don't know what they call it,

153
00:12:46,000 --> 00:12:57,000
a regimen that's what I'm thinking of, a regimen by which you can become free from the sickness.

154
00:12:57,000 --> 00:13:04,000
So the most important thing to know about the vulnerable truth is that they are the core of Buddhism.

155
00:13:04,000 --> 00:13:07,000
They are our goal.

156
00:13:07,000 --> 00:13:17,000
And so we do hear a lot about these and it takes a few minutes probably to explain to a non-Buddhist about them.

157
00:13:17,000 --> 00:13:22,000
But it's very important that we remind ourselves that this is what it is.

158
00:13:22,000 --> 00:13:26,000
If someone asks you what is Buddhism about,

159
00:13:26,000 --> 00:13:31,000
if you want to give them an in-depth answer, if they're really interested in something

160
00:13:31,000 --> 00:13:34,000
more than just don't do good,

161
00:13:34,000 --> 00:13:38,000
or don't do evil, be full of good and purify your mind,

162
00:13:38,000 --> 00:13:41,000
that's another way of describing it.

163
00:13:41,000 --> 00:13:44,000
Purification of mind is a good simple answer,

164
00:13:44,000 --> 00:13:49,000
but if you want to give them what is Buddhist theory,

165
00:13:49,000 --> 00:13:54,000
really the only thing that's necessary to talk about is the vulnerable truth.

166
00:13:54,000 --> 00:13:58,000
I would have said that I teach suffering, the cause of suffering,

167
00:13:58,000 --> 00:14:01,000
the cessation of suffering in the path that leads to the cessation of suffering.

168
00:14:01,000 --> 00:14:06,000
That's what I teach.

169
00:14:06,000 --> 00:14:08,000
Meaning that's all he taught really.

170
00:14:08,000 --> 00:14:10,000
I mean that was the only, he taught many things,

171
00:14:10,000 --> 00:14:12,000
but it was all ancillary.

172
00:14:12,000 --> 00:14:16,000
It was all incidental.

173
00:14:16,000 --> 00:14:21,000
He would teach these things because there's what someone that person needed to hear,

174
00:14:21,000 --> 00:14:29,000
but really what he was getting at is the vulnerable truth.

175
00:14:29,000 --> 00:14:32,000
I just already put the ones, gave a talk to someone,

176
00:14:32,000 --> 00:14:36,000
the Brahmin, about the Brahmin Viharas.

177
00:14:36,000 --> 00:14:41,000
I think it was, sorry, but the Brahmin was dying,

178
00:14:41,000 --> 00:14:44,000
and when he died he went to the Brahmin world.

179
00:14:44,000 --> 00:14:48,000
And, sorry, put the one to see the Buddha, the Buddha said,

180
00:14:48,000 --> 00:14:52,000
what did you teach him? Well, I taught him the Brahmin Viharas.

181
00:14:52,000 --> 00:14:55,000
But I said to him, well he's been born in the Brahmin world.

182
00:14:55,000 --> 00:14:59,000
But the implication was that the Buddha was disappointed,

183
00:14:59,000 --> 00:15:05,000
not disappointed, but was pointing out, sorry, put his mistake.

184
00:15:05,000 --> 00:15:08,000
So Sahiput apparently, this is tradition,

185
00:15:08,000 --> 00:15:12,000
the legend apparently sent his mind to the Brahmin realms

186
00:15:12,000 --> 00:15:19,000
and taught the Brahmin the foreignable truths.

187
00:15:19,000 --> 00:15:22,000
And there's something like after that point,

188
00:15:22,000 --> 00:15:28,000
once this story got around, tradition is that every talk

189
00:15:28,000 --> 00:15:34,000
that any monk would give would include the foreignable truths.

190
00:15:34,000 --> 00:15:43,000
So it's really a marker, or a test.

191
00:15:43,000 --> 00:15:46,000
You can test if the foreignable truths are present

192
00:15:46,000 --> 00:15:50,000
in whatever teaching, well that's the Buddha's teaching.

193
00:15:50,000 --> 00:15:54,000
If it's just about, if it's about something different,

194
00:15:54,000 --> 00:15:57,000
if the focus is not on the foreignable truths,

195
00:15:57,000 --> 00:16:01,000
then you can say that it's not,

196
00:16:01,000 --> 00:16:04,000
not that it's not a part of the Buddha's teaching,

197
00:16:04,000 --> 00:16:08,000
but it hasn't caught the core of the Buddha's teaching.

198
00:16:08,000 --> 00:16:13,000
So it isn't a teaching that will lead to freedom from suffering.

199
00:16:13,000 --> 00:16:18,000
It isn't a teaching that will lead to the goal of Buddhism.

200
00:16:18,000 --> 00:16:23,000
So, a little bit of basic information about the foreignable truths

201
00:16:23,000 --> 00:16:25,000
and useful to know, I think,

202
00:16:25,000 --> 00:16:28,000
and a good intro to what we'll now deal with every day

203
00:16:28,000 --> 00:16:31,000
and we'll go through them, not in very detail,

204
00:16:31,000 --> 00:16:34,000
but just something to talk about,

205
00:16:34,000 --> 00:16:40,000
something to think about, something to listen to.

206
00:16:40,000 --> 00:16:42,000
So thank you all for tuning in.

207
00:16:42,000 --> 00:16:45,000
That's the demo for tonight.

208
00:16:45,000 --> 00:16:48,000
I'll take questions, and you can both stay if you want,

209
00:16:48,000 --> 00:16:51,000
or know if you want.

210
00:16:51,000 --> 00:16:53,000
Do you want to say something?

211
00:16:53,000 --> 00:16:56,000
No. Tell us how your course was.

212
00:16:56,000 --> 00:16:59,000
I can say, come talk.

213
00:16:59,000 --> 00:17:02,000
Don't give details, don't ruin the surprise,

214
00:17:02,000 --> 00:17:06,000
but coming in, okay?

215
00:17:06,000 --> 00:17:12,000
Yeah, it was very difficult and very worthwhile.

216
00:17:12,000 --> 00:17:14,000
That pretty much sums it up.

217
00:17:14,000 --> 00:17:19,000
So, how you're feeling before you started the course,

218
00:17:19,000 --> 00:17:21,000
and after you finished the course,

219
00:17:21,000 --> 00:17:25,000
how do you have, and what way do you feel different?

220
00:17:25,000 --> 00:17:28,000
I guess I feel like I have more freedom,

221
00:17:28,000 --> 00:17:30,000
like I have the knowledge and awareness

222
00:17:30,000 --> 00:17:34,000
of how to experience my life

223
00:17:34,000 --> 00:17:37,000
instead of run away from it.

224
00:17:37,000 --> 00:17:38,000
Great.

225
00:17:38,000 --> 00:17:41,000
So would you recommend the course rather before?

226
00:17:41,000 --> 00:17:42,000
Absolutely.

227
00:17:42,000 --> 00:17:45,000
If they want to come here, if they don't want to,

228
00:17:45,000 --> 00:17:47,000
then that probably...

229
00:17:47,000 --> 00:17:53,000
Do you think it was a positive experience for you?

230
00:17:53,000 --> 00:17:55,000
It was an experience.

231
00:17:55,000 --> 00:17:57,000
I don't know. It was helpful.

232
00:17:57,000 --> 00:17:59,000
You helped with that sort of mean, but positive.

233
00:17:59,000 --> 00:18:00,000
It was beneficial.

234
00:18:00,000 --> 00:18:01,000
Yeah.

235
00:18:01,000 --> 00:18:02,000
Okay, that's all.

236
00:18:02,000 --> 00:18:04,000
Thank you.

237
00:18:04,000 --> 00:18:06,000
I just want to say positive,

238
00:18:06,000 --> 00:18:07,000
because why?

239
00:18:07,000 --> 00:18:09,000
Because I've been drilling into your head.

240
00:18:09,000 --> 00:18:11,000
No, no, everything.

241
00:18:11,000 --> 00:18:15,000
Be objective.

242
00:18:15,000 --> 00:18:17,000
All right, well, there you go.

243
00:18:17,000 --> 00:18:19,000
There's a...

244
00:18:19,000 --> 00:18:24,000
I mean to get everyone to come on here,

245
00:18:24,000 --> 00:18:25,000
but it's just...

246
00:18:25,000 --> 00:18:27,000
Sometimes I forget,

247
00:18:27,000 --> 00:18:30,000
and the timing's not always good.

248
00:18:30,000 --> 00:18:33,000
Another meditator left this morning,

249
00:18:33,000 --> 00:18:35,000
but he'd been here before.

250
00:18:38,000 --> 00:18:39,000
All right.

251
00:18:39,000 --> 00:18:41,000
So, are there any questions?

252
00:18:41,000 --> 00:18:43,000
Let's go see.

253
00:18:43,000 --> 00:18:48,000
One question.

254
00:18:48,000 --> 00:18:53,000
What should be done about past sufferings

255
00:18:53,000 --> 00:18:57,000
that we may have caused for others?

256
00:18:57,000 --> 00:19:00,000
I mean, technically,

257
00:19:00,000 --> 00:19:03,000
there's not much that you have to do.

258
00:19:03,000 --> 00:19:05,000
You're going to have guilt feelings,

259
00:19:05,000 --> 00:19:07,000
or fear,

260
00:19:07,000 --> 00:19:10,000
or worry,

261
00:19:10,000 --> 00:19:17,000
and the practice is to be mindful of those as experiences.

262
00:19:17,000 --> 00:19:19,000
Those things are happening now.

263
00:19:19,000 --> 00:19:22,000
The past is not here.

264
00:19:22,000 --> 00:19:24,000
But practically speaking,

265
00:19:24,000 --> 00:19:26,000
there are many things that can be done.

266
00:19:26,000 --> 00:19:28,000
I mean, asking forgiveness of the person.

267
00:19:28,000 --> 00:19:29,000
I mean, they're pretty standard.

268
00:19:29,000 --> 00:19:32,000
It's not something new to think about.

269
00:19:32,000 --> 00:19:36,000
It's important to realize you can't fix things.

270
00:19:36,000 --> 00:19:41,000
You can't remove suffering that you've caused to others.

271
00:19:41,000 --> 00:19:42,000
You can't fix it.

272
00:19:42,000 --> 00:19:43,000
It's happened.

273
00:19:43,000 --> 00:19:44,000
It's done.

274
00:19:44,000 --> 00:19:45,000
That's the thing about karma.

275
00:19:45,000 --> 00:19:47,000
That's why we talk a lot about karma

276
00:19:47,000 --> 00:19:48,000
is because it's permanent.

277
00:19:48,000 --> 00:19:49,000
I mean, it's...

278
00:19:49,000 --> 00:19:51,000
So, it's final.

279
00:19:51,000 --> 00:19:53,000
It can't be erased.

280
00:19:53,000 --> 00:19:56,000
That thing happened.

281
00:19:56,000 --> 00:19:59,000
But the restitution is possible.

282
00:19:59,000 --> 00:20:00,000
It's important to distinguish this.

283
00:20:00,000 --> 00:20:04,000
Don't try and make it all better.

284
00:20:04,000 --> 00:20:07,000
Or make it as though it didn't happen.

285
00:20:07,000 --> 00:20:09,000
It would be clear that I did this.

286
00:20:09,000 --> 00:20:13,000
Take ownership of the act.

287
00:20:13,000 --> 00:20:18,000
And take responsibility for restitution.

288
00:20:18,000 --> 00:20:21,000
If something has been done,

289
00:20:21,000 --> 00:20:23,000
then you think, well,

290
00:20:23,000 --> 00:20:27,000
what's the appropriate thing to do to make up for it?

291
00:20:27,000 --> 00:20:30,000
If you broke something of someone's,

292
00:20:30,000 --> 00:20:38,000
you can replace it.

293
00:20:38,000 --> 00:20:41,000
If you hurt someone,

294
00:20:41,000 --> 00:20:44,000
you can't unhurt them.

295
00:20:44,000 --> 00:20:46,000
But you can make a promise not to.

296
00:20:46,000 --> 00:20:49,000
You can apologize and make a promise not to.

297
00:20:49,000 --> 00:20:52,000
Intentionally hurt them in the future.

298
00:20:52,000 --> 00:20:56,000
So, I mean, there's definitely a place

299
00:20:56,000 --> 00:20:59,000
for going to find the people you've heard.

300
00:20:59,000 --> 00:21:02,000
But on an ultimate level,

301
00:21:02,000 --> 00:21:03,000
none of it really matters.

302
00:21:03,000 --> 00:21:06,000
And all you have to do is be mindful.

303
00:21:06,000 --> 00:21:09,000
But that includes the mindful activity.

304
00:21:09,000 --> 00:21:11,000
So, if they confront you,

305
00:21:11,000 --> 00:21:13,000
or if there's attention,

306
00:21:13,000 --> 00:21:15,000
then you relieve that.

307
00:21:15,000 --> 00:21:18,000
If they are angry at you,

308
00:21:18,000 --> 00:21:19,000
because of something you've done,

309
00:21:19,000 --> 00:21:24,000
then you act in such a way as to...

310
00:21:24,000 --> 00:21:28,000
Really, it has nothing to do with what you've done.

311
00:21:28,000 --> 00:21:30,000
But you accept their anger,

312
00:21:30,000 --> 00:21:32,000
and you apologize,

313
00:21:32,000 --> 00:21:34,000
but you act in the same way...

314
00:21:34,000 --> 00:21:36,000
As a mindful person,

315
00:21:36,000 --> 00:21:39,000
you act in the same way you would as if...

316
00:21:39,000 --> 00:21:41,000
If you had never hurt them,

317
00:21:41,000 --> 00:21:43,000
you're kind, you're compassionate,

318
00:21:43,000 --> 00:21:45,000
you're mindful,

319
00:21:45,000 --> 00:21:48,000
you're present, you're there for them in it.

320
00:21:48,000 --> 00:21:50,000
So, your behavior doesn't really change

321
00:21:50,000 --> 00:21:54,000
because of the fact that you hurt them.

322
00:21:54,000 --> 00:21:56,000
Or that you'd hurt them or not,

323
00:21:56,000 --> 00:21:58,000
you're still going to be kind,

324
00:21:58,000 --> 00:22:04,000
and mindful towards them.

325
00:22:07,000 --> 00:22:10,000
So, on the one hand,

326
00:22:10,000 --> 00:22:12,000
you don't want to obsess over

327
00:22:12,000 --> 00:22:14,000
trying to fix things you've done,

328
00:22:14,000 --> 00:22:15,000
because you can't fix them.

329
00:22:15,000 --> 00:22:17,000
But on the other hand,

330
00:22:17,000 --> 00:22:18,000
you don't want to say,

331
00:22:18,000 --> 00:22:19,000
oh, I'll just be mindful

332
00:22:19,000 --> 00:22:20,000
and this person comes and says,

333
00:22:20,000 --> 00:22:23,000
hey, you did this and this and this,

334
00:22:23,000 --> 00:22:24,000
and you're hearing,

335
00:22:24,000 --> 00:22:25,000
hearing, you don't want to be like that.

336
00:22:25,000 --> 00:22:28,000
Mindfulness has very much to do with

337
00:22:28,000 --> 00:22:31,000
our relationships with other people.

338
00:22:31,000 --> 00:22:33,000
You know?

339
00:22:33,000 --> 00:22:35,000
There are times if someone is yelling at you

340
00:22:35,000 --> 00:22:37,000
that you do just want to say hearing,

341
00:22:37,000 --> 00:22:42,000
but if they have a valid complaint,

342
00:22:42,000 --> 00:22:45,000
and there's something you can do to fix,

343
00:22:45,000 --> 00:22:50,000
to fix the situation,

344
00:22:50,000 --> 00:22:54,000
return a situation back to its original condition.

345
00:22:54,000 --> 00:22:57,000
Then you go and do it.

346
00:22:57,000 --> 00:22:58,000
You can't undo the karma.

347
00:22:58,000 --> 00:22:59,000
You've done something bad

348
00:22:59,000 --> 00:23:02,000
and that's always going to be the case,

349
00:23:02,000 --> 00:23:05,000
but you can restore the situation

350
00:23:05,000 --> 00:23:08,000
where things you can do and should do.

351
00:23:11,000 --> 00:23:14,000
So, neither too involved

352
00:23:14,000 --> 00:23:16,000
nor too detached,

353
00:23:16,000 --> 00:23:18,000
just mindful.

354
00:23:20,000 --> 00:23:21,000
And that was it.

355
00:23:21,000 --> 00:23:24,000
There was only one question tonight.

356
00:23:24,000 --> 00:23:28,000
So, thank you all for tuning in.

357
00:23:28,000 --> 00:23:52,000
Have a good night.

