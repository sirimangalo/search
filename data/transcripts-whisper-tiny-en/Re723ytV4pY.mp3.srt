1
00:00:00,000 --> 00:00:04,000
Is the sitting touching technique a must in meditation?

2
00:00:04,000 --> 00:00:09,000
If so, is it noted between breaths or instead of the breaths?

3
00:00:09,000 --> 00:00:12,000
There are different ways of approaching this.

4
00:00:12,000 --> 00:00:16,000
My teacher is emphatic that it's instead of the breath.

5
00:00:16,000 --> 00:00:19,000
And I think this is correct.

6
00:00:19,000 --> 00:00:25,000
It seems the most reasonable because the point is to be mindful of one thing at a time.

7
00:00:25,000 --> 00:00:28,000
You need concentration, you need your mind to be focused.

8
00:00:28,000 --> 00:00:32,000
When you focus on the rising, you should only be aware of the rising.

9
00:00:32,000 --> 00:00:36,000
When you focus on the falling you should only be aware of the falling.

10
00:00:36,000 --> 00:00:38,000
When you focus on sitting you should only be aware of sitting.

11
00:00:38,000 --> 00:00:40,000
When you focus on touching you should only be aware of touching.

12
00:00:40,000 --> 00:00:44,000
So we would say rising, falling.

13
00:00:44,000 --> 00:00:48,000
And then whether the breath rises and falls or not,

14
00:00:48,000 --> 00:00:52,000
however many times it arises and falls, we don't pay attention to it.

15
00:00:52,000 --> 00:01:00,640
We try as much time as it takes to be mindful that we're sitting.

16
00:01:00,640 --> 00:01:03,200
Once you're mindful that you're sitting, say to yourself,

17
00:01:03,200 --> 00:01:07,440
sitting, and then once you've done that,

18
00:01:07,440 --> 00:01:11,280
become aware of the spot on your body and focus on that and say to yourself,

19
00:01:11,280 --> 00:01:16,880
touching, rising, and then go back and look and wait for the next rising.

20
00:01:16,880 --> 00:01:19,440
So it wouldn't be instead of the breast.

21
00:01:19,440 --> 00:01:23,120
Now, is it, sorry, the other question, is it a must?

22
00:01:23,120 --> 00:01:25,200
No, it's certainly not a must.

23
00:01:25,200 --> 00:01:28,560
The only reason for adding something like sitting or something like touching

24
00:01:28,560 --> 00:01:32,960
is because your mind is becoming more powerful.

25
00:01:32,960 --> 00:01:38,960
Your mind is becoming more sharp and more adept at the meditation.

26
00:01:38,960 --> 00:01:44,400
So in ordinary daily meditation practice, you can just do rising, falling,

27
00:01:44,400 --> 00:01:54,160
rising, falling, and in fact, it is enough, but the added difficulty of going

28
00:01:54,160 --> 00:01:57,040
between different objects, so it serves two purposes.

29
00:01:57,040 --> 00:02:00,560
One, because it's many different objects, the mind is interested in it,

30
00:02:00,560 --> 00:02:02,960
and so it keeps the mind from running away.

31
00:02:02,960 --> 00:02:07,520
And the other one is that it forces the mind to be refined.

32
00:02:07,520 --> 00:02:12,880
Forces the mind to become more and more refined and focuses the practice to become

33
00:02:12,880 --> 00:02:14,080
more and more refined.

34
00:02:14,080 --> 00:02:16,320
Because otherwise, you'll never get through all the different points,

35
00:02:16,320 --> 00:02:18,880
so it will give you many different points on the body to be

36
00:02:18,880 --> 00:02:21,680
rising, falling, sitting, touching, rising, rising, falling, sitting, touching, right,

37
00:02:22,880 --> 00:02:26,400
and you wouldn't be able to do it if your mind were not refined.

38
00:02:26,400 --> 00:02:33,440
So it encourages the mind to give up and to pay no attention

39
00:02:33,440 --> 00:02:37,120
to the diversions and distractions that would otherwise be

40
00:02:37,120 --> 00:02:41,680
just away from the meditation, because you have something to do,

41
00:02:41,680 --> 00:02:45,600
you have work to do. So it forces the mind to become fine too,

42
00:02:45,600 --> 00:02:50,400
and it trains the mind just like in weightlifting.

43
00:02:50,400 --> 00:02:55,360
You could train yourself by just lifting small weights,

44
00:02:55,360 --> 00:02:59,600
but when you put more and more weight on,

45
00:02:59,600 --> 00:03:15,520
it trains you further and further, and it has a benefit as a result.

