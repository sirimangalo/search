1
00:00:00,000 --> 00:00:15,500
Explain in chapter 4, paragraph 63, or it's proximate cause is grounds for the initiation of energy.

2
00:00:15,500 --> 00:00:19,000
So, grounds for the initiation of energy also.

3
00:00:19,000 --> 00:00:23,000
I think we have met with it somewhere back.

4
00:00:23,000 --> 00:00:32,000
Now, if you want to read all these grounds, then this read book dialogues of the Buddha,

5
00:00:32,000 --> 00:00:49,000
part 3, page 239, etc., grounds for initiation of energy, I will tell you only two.

6
00:00:49,000 --> 00:00:50,000
There is something you have to do.

7
00:00:50,000 --> 00:00:52,000
You have to do something.

8
00:00:52,000 --> 00:00:56,000
Then, before doing that, say, you say to yourself,

9
00:00:56,000 --> 00:01:03,000
when I am doing that, that work, I will not get opportunity to practice the meditation.

10
00:01:03,000 --> 00:01:07,000
So, I must practice now before I get to that work.

11
00:01:07,000 --> 00:01:16,000
So, in this way, you initiate energy, you make effort and practice meditation.

12
00:01:16,000 --> 00:01:21,000
And then, after the work, then you practice meditation to say to yourself,

13
00:01:21,000 --> 00:01:25,000
during the work, I didn't have the opportunity to practice meditation.

14
00:01:25,000 --> 00:01:27,000
So, now, I must practice.

15
00:01:27,000 --> 00:01:31,000
So, in this way, you announce energy in yourself.

16
00:01:31,000 --> 00:01:36,000
So, with regard to work, there are two.

17
00:01:36,000 --> 00:01:40,000
With regard to going traveling, there are two.

18
00:01:40,000 --> 00:01:45,000
Before going on travel and after coming back on travel.

19
00:01:45,000 --> 00:01:48,000
And then, there is sickness.

20
00:01:48,000 --> 00:01:53,000
Before you get sickness, you practice meditation and after sickness,

21
00:01:53,000 --> 00:01:56,000
also you practice meditation.

22
00:01:56,000 --> 00:01:58,000
And then, going for arms.

23
00:01:58,000 --> 00:02:01,000
So, before going for arms, after going on.

24
00:02:01,000 --> 00:02:07,000
So, actually, every time you have to announce energy to practice.

25
00:02:07,000 --> 00:02:12,000
So, these are called the grounds for initiation of energy.

26
00:02:12,000 --> 00:02:17,000
When rightly initiated, it should be regarded as the root of all attainment and so on.

27
00:02:17,000 --> 00:02:19,000
And then, the next one is givida.

28
00:02:19,000 --> 00:02:24,000
Givida is like the givida in the corporeality.

29
00:02:24,000 --> 00:02:27,000
And then, Samari, we come to Samari.

30
00:02:27,000 --> 00:02:31,000
It puts consciously evenly on the object.

31
00:02:31,000 --> 00:02:34,000
Consciousness, evenly on the object.

32
00:02:34,000 --> 00:02:37,000
Or it puts it rightly on it.

33
00:02:37,000 --> 00:02:46,000
So, in the what Samari, you have the prefix Sam as AM.

34
00:02:46,000 --> 00:02:54,000
So, that S-A-M can mean evenly, semi, or rightly, semi.

35
00:02:54,000 --> 00:02:58,000
So, evenly, rightly.

36
00:02:58,000 --> 00:03:01,000
Or it is just a mere collecting of the mind.

37
00:03:01,000 --> 00:03:03,000
Thus, it is concentration.

38
00:03:03,000 --> 00:03:06,000
Its characteristic is non-wandering.

39
00:03:06,000 --> 00:03:08,000
It means itself non-wandering.

40
00:03:08,000 --> 00:03:11,000
Or its characteristic is non-destructing.

41
00:03:11,000 --> 00:03:15,000
That means with regard to connection states.

42
00:03:15,000 --> 00:03:20,000
So, it does not let connection states to be distracted.

43
00:03:20,000 --> 00:03:23,000
And it itself is non-wandering.

44
00:03:23,000 --> 00:03:29,000
Its function is to conglomerate connection states as water just bath powder.

45
00:03:29,000 --> 00:03:32,000
So, it keeps them together.

46
00:03:32,000 --> 00:03:35,000
It is manifested as fees.

47
00:03:35,000 --> 00:03:39,000
Usually, its approximate cost is bliss.

48
00:03:39,000 --> 00:03:41,000
But it is not always.

49
00:03:41,000 --> 00:03:46,000
Therefore, it is a usually.

50
00:03:46,000 --> 00:03:54,000
Because, Samari can be without Suka also, right?

51
00:03:54,000 --> 00:03:58,000
Fifth Jana.

52
00:03:58,000 --> 00:04:06,000
Like, it is to be regarded as steadiness of the mind, like the steadiness of the limbs flame

53
00:04:06,000 --> 00:04:08,000
when there is no draft.

54
00:04:08,000 --> 00:04:11,000
So, steadiness of the mind.

55
00:04:11,000 --> 00:04:12,000
That is Samari.

56
00:04:12,000 --> 00:04:27,000
So, Samari is a mental state which puts the mind on the object evenly and rightly.

57
00:04:27,000 --> 00:04:33,000
Even, he means keeping the cones and states together.

58
00:04:33,000 --> 00:04:36,000
And rightly means not itself being distracted.

59
00:04:36,000 --> 00:04:39,000
That is what we call Samari.

60
00:04:39,000 --> 00:04:41,000
On the object.

61
00:04:41,000 --> 00:04:44,000
It is on the object.

62
00:04:44,000 --> 00:04:49,000
And it also keeps the other mental factors together,

63
00:04:49,000 --> 00:04:52,000
not that they be scared of.

64
00:04:52,000 --> 00:04:55,000
That is what is called Samari.

65
00:04:55,000 --> 00:05:02,000
And the next one is Sadhana, faith or confidence.

66
00:05:02,000 --> 00:05:09,000
And with regard to its function that is to enter into,

67
00:05:09,000 --> 00:05:15,000
like the setting out across a flat and the S-N,

68
00:05:15,000 --> 00:05:17,000
the reference is given.

69
00:05:17,000 --> 00:05:21,000
But, I would like to give you another reference.

70
00:05:21,000 --> 00:05:25,000
That is, Expositor page 158.

71
00:05:25,000 --> 00:05:33,000
Please read for the meaning of the setting out across a flat.

72
00:05:33,000 --> 00:05:38,000
A brief, in brief, a brief man could take other people across the,

73
00:05:38,000 --> 00:05:41,000
across the flat or across the river.

74
00:05:41,000 --> 00:05:48,000
I mean crossing an abode or crossing by themselves.

75
00:05:48,000 --> 00:05:52,000
So, in the same way, when you have Sadhana, when you have faith or confidence,

76
00:05:52,000 --> 00:05:57,000
then you can plunge into things and then you can accomplish.

77
00:05:57,000 --> 00:06:04,000
So, Sadhana is compared to crossing the flat or one who crosses the flat or one who,

78
00:06:04,000 --> 00:06:09,000
himself crosses the flat and who takes others with him.

79
00:06:09,000 --> 00:06:12,000
That is what we call Sadhana.

80
00:06:12,000 --> 00:06:21,000
So, please read Expositor page 158.

81
00:06:21,000 --> 00:06:26,000
And then, it should be regarded as a hand,

82
00:06:26,000 --> 00:06:32,000
because it takes hold of profitable things as well and as seeds.

83
00:06:32,000 --> 00:06:42,000
So, for, for regarding as a hand, please read gradual sayings,

84
00:06:42,000 --> 00:06:48,000
third volume, page 245.

85
00:06:48,000 --> 00:06:53,000
If you have a hand, you can pick up things that are profitable to you that are good for you.

86
00:06:53,000 --> 00:06:58,000
In the same way, if you have faith, if you have confidence,

87
00:06:58,000 --> 00:07:03,000
you can get kusala. So, that is why it is compared to a hand.

88
00:07:03,000 --> 00:07:09,000
Sometimes it is compared to wealth and sometimes to a seed.

89
00:07:09,000 --> 00:07:14,000
And then, so this, I mean mindfulness.

90
00:07:14,000 --> 00:07:17,000
You know, all of you know about mindfulness.

91
00:07:17,000 --> 00:07:19,000
It has a characteristic of not wobbling.

92
00:07:19,000 --> 00:07:22,000
That means not floating on the surface.

93
00:07:22,000 --> 00:07:24,000
Its function is not too forget.

94
00:07:24,000 --> 00:07:26,000
That means not to lose the object.

95
00:07:26,000 --> 00:07:29,000
Not to forget means not to lose the object.

96
00:07:29,000 --> 00:07:33,000
And it is manifested as cutting and so on.

97
00:07:33,000 --> 00:07:37,000
And its proximate cause is the foundations of mindfulness.

98
00:07:37,000 --> 00:07:45,000
And here, foundations of mindfulness really means the objects of foundations of mindfulness.

99
00:07:45,000 --> 00:07:52,000
The body, feeling, consciousness, and say dhamma objects.

100
00:07:52,000 --> 00:07:57,000
It should be regarded as like a philla because it is firmly founded.

101
00:07:57,000 --> 00:08:01,000
Or as like a dog keeper because it guards the idol and so on.

102
00:08:01,000 --> 00:08:05,000
And then, two things, heavy and potaba.

103
00:08:05,000 --> 00:08:11,000
So, heavy is translated here as conscience and potaba as shame.

104
00:08:11,000 --> 00:08:15,000
I do not think this is quite correct.

105
00:08:15,000 --> 00:08:19,000
Potaba is fear or dread.

106
00:08:19,000 --> 00:08:22,000
Healy is shame.

107
00:08:22,000 --> 00:08:25,000
So, I think we should translate healy as shame or conscience.

108
00:08:25,000 --> 00:08:29,000
But potaba, something like dread or fear.

109
00:08:29,000 --> 00:08:35,000
And fear here means moral or fear.

110
00:08:35,000 --> 00:08:38,000
We are ashamed to do what is wrong.

111
00:08:38,000 --> 00:08:41,000
And that is, that is, healy or shame.

112
00:08:41,000 --> 00:08:47,000
And we are afraid to do what is wrong because we don't want to get the

113
00:08:47,000 --> 00:08:53,000
painful consequences of these actions.

114
00:08:53,000 --> 00:08:56,000
So, this is a term for anxiety about evil.

115
00:08:56,000 --> 00:09:00,000
Here, in conscience, as the characteristic of disgust at evil, while shame,

116
00:09:00,000 --> 00:09:05,000
that is, second one, as the characteristic of dread of dread.

117
00:09:05,000 --> 00:09:10,000
So, shame and dread are shame and fear.

118
00:09:10,000 --> 00:09:15,000
These two things.

119
00:09:15,000 --> 00:09:20,000
And then, a man rejects evil through conscience out of respect for himself,

120
00:09:20,000 --> 00:09:22,000
as the daughter of a good family does.

121
00:09:22,000 --> 00:09:27,000
He rejects evil through shame or the seconding fear.

122
00:09:27,000 --> 00:09:32,000
Out of respect for another, as a cultist and does.

123
00:09:32,000 --> 00:09:34,000
These two states.

124
00:09:34,000 --> 00:09:37,000
I say, we will strike out the...

125
00:09:37,000 --> 00:09:42,000
But these two states should be regarded as the guardians of the world.

126
00:09:42,000 --> 00:09:45,000
They are described as the guardians of the world.

127
00:09:45,000 --> 00:09:52,000
So long as these two reside in the minds of the minds of beings,

128
00:09:52,000 --> 00:09:55,000
the world will go on.

129
00:09:55,000 --> 00:10:00,000
When these two left the minds of the beings, then the world will become

130
00:10:00,000 --> 00:10:02,000
a mark or whatever.

131
00:10:02,000 --> 00:10:11,000
And differentiated, they will not act according to their conscience,

132
00:10:11,000 --> 00:10:19,000
so there will be no moral restrictions or something.

133
00:10:19,000 --> 00:10:24,000
So, human beings will become like animals and so on.

134
00:10:24,000 --> 00:10:27,000
And that is why they are called the guardians of the world.

135
00:10:27,000 --> 00:10:31,000
Moral shame and moral fear.

136
00:10:31,000 --> 00:10:40,000
That means, shame to do immoral things and fear to do these things.

137
00:10:40,000 --> 00:10:45,000
The next is Aloba Adosa and Amoha.

138
00:10:45,000 --> 00:10:47,000
Aloba non-grid.

139
00:10:47,000 --> 00:10:50,000
Adosa non-hate and Amoha non-delusion.

140
00:10:50,000 --> 00:10:53,000
So, Amoha non-delusion means what?

141
00:10:53,000 --> 00:10:55,000
Panya wisdom.

142
00:10:55,000 --> 00:10:59,000
So, Amoha non-delusion means what?

143
00:10:59,000 --> 00:11:01,000
Panya wisdom.

144
00:11:01,000 --> 00:11:06,000
So, Amoha and Panya are the same.

145
00:11:06,000 --> 00:11:11,000
But its function is not to lay hold like a liberated beaker.

146
00:11:11,000 --> 00:11:15,000
It is manifested as a state of not eating as a shelter,

147
00:11:15,000 --> 00:11:18,000
like that of a man who has fallen into field.

148
00:11:18,000 --> 00:11:24,000
I mean, not attached to or not adhering to something like that.

149
00:11:24,000 --> 00:11:29,000
So, when you fall into field, then you are not attached to field.

150
00:11:29,000 --> 00:11:33,000
You want to get rid of it as soon as possible.

151
00:11:33,000 --> 00:11:37,000
So, it is not being attached.

152
00:11:37,000 --> 00:11:40,000
And then, about two, three lines down.

153
00:11:40,000 --> 00:11:42,000
Its function is to remove annoyance.

154
00:11:42,000 --> 00:11:44,000
Its function is to remove fever.

155
00:11:44,000 --> 00:11:49,000
That means to remove heat.

156
00:11:49,000 --> 00:11:52,000
Not necessarily fever, but heat.

157
00:11:52,000 --> 00:11:54,000
That center wood does.

158
00:11:54,000 --> 00:11:59,000
Now, when people are hot, people use a center wood paste.

159
00:11:59,000 --> 00:12:02,000
They apply center wood paste to their bodies.

160
00:12:02,000 --> 00:12:05,000
And then, they become cool.

161
00:12:05,000 --> 00:12:11,000
It is very familiar thing in Burma.

162
00:12:11,000 --> 00:12:19,000
You may have seen Burma's girls with something like paste on their cheek.

163
00:12:19,000 --> 00:12:21,000
Its only used in Burma.

164
00:12:21,000 --> 00:12:23,000
Its like, what about this?

165
00:12:23,000 --> 00:12:29,000
Make up something you apply on your face.

166
00:12:29,000 --> 00:12:33,000
So, when you just hot, they apply that kind of thing,

167
00:12:33,000 --> 00:12:36,000
especially central center wood.

168
00:12:36,000 --> 00:12:39,000
They make a center wood into a paste or something.

169
00:12:39,000 --> 00:12:41,000
How do you call this?

170
00:12:41,000 --> 00:12:43,000
Making push to it.

171
00:12:43,000 --> 00:12:45,000
Criding, yeah.

172
00:12:45,000 --> 00:12:49,000
They usually do it on this.

173
00:12:49,000 --> 00:12:51,000
Water?

174
00:12:51,000 --> 00:12:54,000
Yeah, it has a flat stone.

175
00:12:54,000 --> 00:12:55,000
A flat stone.

176
00:12:55,000 --> 00:12:56,000
A flat stone.

177
00:12:56,000 --> 00:13:00,000
And then, we push it on our push on the stone so that with water,

178
00:13:00,000 --> 00:13:02,000
so we get it placed.

179
00:13:02,000 --> 00:13:06,000
And that place is applied to the face or to the other parts of the body.

180
00:13:06,000 --> 00:13:12,000
And it keeps them cool to something like that here.

181
00:13:12,000 --> 00:13:15,000
Non-delusion has the characteristic of penetrating things

182
00:13:15,000 --> 00:13:18,000
according to the individual assets and assets and so on.

183
00:13:18,000 --> 00:13:22,000
So, this is spania.

184
00:13:22,000 --> 00:13:25,000
It has a characteristic of shoe-up and nutrition.

185
00:13:25,000 --> 00:13:27,000
So, it will never miss.

186
00:13:27,000 --> 00:13:30,000
Pania, if it is real, Pania will never miss.

187
00:13:30,000 --> 00:13:33,000
And like the Pania, nutrition of an aeroshore by its

188
00:13:33,000 --> 00:13:34,000
careful archer, right?

189
00:13:34,000 --> 00:13:38,000
Its function is to eliminate the objective field.

190
00:13:38,000 --> 00:13:43,000
So, if this room is dark, we cannot see things in this room.

191
00:13:43,000 --> 00:13:46,000
So, when there is light, we see things clear.

192
00:13:46,000 --> 00:13:48,000
In the same way, when there is no Pania,

193
00:13:48,000 --> 00:13:50,000
we don't see things as they are.

194
00:13:50,000 --> 00:13:54,000
When Pania enters our minds, we see things as they are.

195
00:13:54,000 --> 00:13:57,000
So, it is like a lamp.

196
00:13:57,000 --> 00:14:01,000
It is manifested as non-bueltermine, like a kite in the forest.

197
00:14:01,000 --> 00:14:06,000
So, you may be lost in the forest, but if you have a kite,

198
00:14:06,000 --> 00:14:09,000
you will not get lost.

199
00:14:09,000 --> 00:14:15,000
The three should be regarded as a root of four that is profitable.

200
00:14:15,000 --> 00:14:22,000
And then, tranquility of body, tranquility of consciousness and so on.

201
00:14:22,000 --> 00:14:25,000
They are theirs.

202
00:14:25,000 --> 00:14:29,000
Here, body does not mean material body.

203
00:14:29,000 --> 00:14:33,000
Here, more body means three mental aggregates,

204
00:14:33,000 --> 00:14:36,000
feeling, perception and mental formations.

205
00:14:36,000 --> 00:14:40,000
So, they are here called in Pali Kaia.

206
00:14:40,000 --> 00:14:51,000
And the next prayer, lightness of the body, lightness of consciousness.

207
00:14:51,000 --> 00:14:59,000
There are several material qualities among the 24 material properties.

208
00:14:59,000 --> 00:15:06,000
And then, next is what, malleability of body and malleability of consciousness.

209
00:15:06,000 --> 00:15:12,000
And the next prayer, will-diness of body, will-diness of consciousness.

210
00:15:12,000 --> 00:15:17,000
And the next prayer, proficiency of body, proficiency of consciousness.

211
00:15:17,000 --> 00:15:24,000
Another next prayer, rectity to the body, rectity of consciousness.

212
00:15:24,000 --> 00:15:27,000
Now, we come to see, desire, chanda.

213
00:15:27,000 --> 00:15:31,000
Now, chanda is just a desire to act.

214
00:15:31,000 --> 00:15:33,000
The mere will.

215
00:15:33,000 --> 00:15:37,000
So, that Ziyu has a characteristic of desire to act.

216
00:15:37,000 --> 00:15:41,000
Its function is scanning for an object, searching for an object.

217
00:15:41,000 --> 00:15:44,000
It is manifested as need for an object.

218
00:15:44,000 --> 00:15:46,000
That same object is its proximate course.

219
00:15:46,000 --> 00:15:52,000
It should be regarded as an extending of the mental hand in the apprehending of an object.

220
00:15:52,000 --> 00:15:57,000
That means, if you want to pick up something, you could pour out your hand, something like that.

221
00:15:57,000 --> 00:16:03,000
So, it is not attachment, but it is just the mere will to do.

222
00:16:03,000 --> 00:16:06,000
It is a desire to do.

223
00:16:06,000 --> 00:16:09,000
Next is resolution.

224
00:16:09,000 --> 00:16:12,000
It has characteristic of conviction.

225
00:16:12,000 --> 00:16:14,000
Its function is not to grow.

226
00:16:14,000 --> 00:16:17,000
It is manifested as decisiveness.

227
00:16:17,000 --> 00:16:20,000
Its proximate course is a theme to be convinced about.

228
00:16:20,000 --> 00:16:25,000
It should be regarded like a boundary post or it is a gate post

229
00:16:25,000 --> 00:16:29,000
going to its immovableness with respect to the object.

230
00:16:29,000 --> 00:16:36,000
And the next is in Pali Manasikara.

231
00:16:36,000 --> 00:16:39,000
It is the maker of what is to be made.

232
00:16:39,000 --> 00:16:41,000
It is the maker in the mind.

233
00:16:41,000 --> 00:16:44,000
Now, this definition is difficult to translate.

234
00:16:44,000 --> 00:16:49,000
Actually, the Pali what is Manasik and Cara?

235
00:16:49,000 --> 00:16:53,000
Now, Cara means doing or making.

236
00:16:53,000 --> 00:16:55,000
And Manasik means in the mind.

237
00:16:55,000 --> 00:16:59,000
So, Manasikara means doing in the mind.

238
00:16:59,000 --> 00:17:01,000
That is one meaning.

239
00:17:01,000 --> 00:17:07,000
Or making in the mind.

240
00:17:07,000 --> 00:17:13,000
That means paying attention.

241
00:17:13,000 --> 00:17:19,000
The other meaning is making the mind.

242
00:17:19,000 --> 00:17:21,000
That means making the mind different.

243
00:17:21,000 --> 00:17:28,000
So, the third line.

244
00:17:28,000 --> 00:17:33,000
It makes the mind different from the previous life continuum mind.

245
00:17:33,000 --> 00:17:35,000
Thus, it is attention.

246
00:17:35,000 --> 00:17:38,000
So, after what does we should put also?

247
00:17:38,000 --> 00:17:41,000
Thus, also, it is attention.

248
00:17:41,000 --> 00:17:45,000
Because it is a different definition from the first one.

249
00:17:45,000 --> 00:17:49,000
The first one says that it is making in the mind.

250
00:17:49,000 --> 00:17:52,000
And the second one says making the mind.

251
00:17:52,000 --> 00:18:01,000
Making the mind in making the mind different.

252
00:18:01,000 --> 00:18:16,000
And this definition refers to the second and third kind of attention described later down

253
00:18:16,000 --> 00:18:18,000
towards the end of the paragraph.

254
00:18:18,000 --> 00:18:21,000
Now, there are three kinds of Manasikara.

255
00:18:21,000 --> 00:18:23,000
Let me use the Pali what?

256
00:18:23,000 --> 00:18:34,000
There are three kinds of Manasikara.

257
00:18:34,000 --> 00:18:49,000
Controller of objects, controller of cognitive series that means controller of cognitive series.

258
00:18:49,000 --> 00:18:52,000
What is cognitive series?

259
00:18:52,000 --> 00:18:53,000
That process.

260
00:18:53,000 --> 00:18:55,000
Controller of thought process.

261
00:18:55,000 --> 00:18:58,000
And controller of impressions.

262
00:18:58,000 --> 00:19:05,000
Now, controller of cognitive series means five-door Edwardine.

263
00:19:05,000 --> 00:19:10,000
So, five-door Edwardine is called controller of thought process.

264
00:19:10,000 --> 00:19:17,000
Because the thought process real thought process begins with that movement of consciousness.

265
00:19:17,000 --> 00:19:21,000
And controller of impressions means mind-door Edwardine.

266
00:19:21,000 --> 00:19:33,000
Because after the mind-door Edwardine comes in portions.

267
00:19:33,000 --> 00:19:36,000
But they are not meant here actually.

268
00:19:36,000 --> 00:19:52,000
What is meant is just attention, which is called controller of objects.

269
00:19:52,000 --> 00:19:54,000
So, there are three ways.

270
00:19:54,000 --> 00:19:57,000
And three kinds of Manasikara.

271
00:19:57,000 --> 00:20:00,000
Controller of objects.

272
00:20:00,000 --> 00:20:02,000
Controller of cognitive series.

273
00:20:02,000 --> 00:20:04,000
And controller of impressions.

274
00:20:04,000 --> 00:20:08,000
The last two are not meant here.

275
00:20:08,000 --> 00:20:12,000
So, the first one is what is meant here?

276
00:20:12,000 --> 00:20:14,000
The controller of objects.

277
00:20:14,000 --> 00:20:18,000
That means just paying attention.

278
00:20:18,000 --> 00:20:22,000
And then, the next one is specific neutrality.

279
00:20:22,000 --> 00:20:26,000
And it is often called upika.

280
00:20:26,000 --> 00:20:29,000
The Pali word is tatra majatada.

281
00:20:29,000 --> 00:20:31,000
Neutral in regard there too.

282
00:20:31,000 --> 00:20:34,000
Just be in the middle.

283
00:20:34,000 --> 00:20:39,000
Not falling into liking or disliking.

284
00:20:39,000 --> 00:20:44,000
It is the characteristic of conveying consciousness and consciousness concomitant evenly.

285
00:20:44,000 --> 00:20:46,000
So, making them evenly.

286
00:20:46,000 --> 00:20:49,000
Its function is to prevent deficiency and access.

287
00:20:49,000 --> 00:20:52,000
Or its function is to inhibit partiality.

288
00:20:52,000 --> 00:20:54,000
It is manifested as neutrality.

289
00:20:54,000 --> 00:20:57,000
It should be regarded as like a conductor or a driver.

290
00:20:57,000 --> 00:21:02,000
Who looks with a continuity on tara bridge progressing evenly?

291
00:21:02,000 --> 00:21:07,000
So, I compare this to cruise control.

292
00:21:07,000 --> 00:21:09,000
In a car.

293
00:21:09,000 --> 00:21:14,000
You put on the cruise control and you do not have to worry about speed.

294
00:21:17,000 --> 00:21:24,000
Here also, when the tara bridge means when causes are running evenly,

295
00:21:24,000 --> 00:21:27,000
you say it is drawing the car evenly.

296
00:21:27,000 --> 00:21:29,000
You do not have to worry about them.

297
00:21:29,000 --> 00:21:32,000
You just look on.

298
00:21:32,000 --> 00:21:38,000
And then, compression and gladness, cruna and mudita, they are described in the divine abodes.

299
00:21:38,000 --> 00:21:43,000
The only difference is, there they belong to genres.

300
00:21:43,000 --> 00:21:48,000
And here they belong to kama vajara.

301
00:21:48,000 --> 00:21:51,000
And on the next page,

302
00:21:51,000 --> 00:21:56,000
that should not be admitted for s to meaning now.

303
00:21:56,000 --> 00:22:01,000
Instead of saying s to meaning, we should say, in reality.

304
00:22:01,000 --> 00:22:05,000
S to meaning, not s to meaning, in reality.

305
00:22:05,000 --> 00:22:11,000
Non-hate itself is loving kindness and specific neutrality itself is equanimity.

306
00:22:11,000 --> 00:22:15,000
And then, we have abstinence, three abstinencees.

307
00:22:15,000 --> 00:22:18,000
I think you understand about them.

308
00:22:18,000 --> 00:22:31,000
And then, describes the mental states which arise with different types of consciousness.

309
00:22:31,000 --> 00:22:34,000
So, it may be confusing for you issues.

310
00:22:34,000 --> 00:22:41,000
You do not have the 89 types of consciousness in mind and also 52 types of consciousness.

311
00:22:41,000 --> 00:22:51,000
So, they can be studied from the manual of a vidama or during the vidama class here,

312
00:22:51,000 --> 00:22:54,000
I distribute the handouts.

313
00:22:54,000 --> 00:23:01,000
So, you may look at those handouts and then, breathe these passages.

314
00:23:01,000 --> 00:23:06,000
And then, we come to unprofitable Akusava.

315
00:23:06,000 --> 00:23:12,000
They are also on page 529 for a graph 159.

316
00:23:12,000 --> 00:23:15,000
That's because the unprofitable also.

317
00:23:15,000 --> 00:23:22,000
They are constants in constants and over whatever states.

318
00:23:22,000 --> 00:23:33,000
So, they clearly mentioned here.

319
00:23:33,000 --> 00:23:43,000
And then, down the page, we have the constants, lessness and shamelessness.

320
00:23:43,000 --> 00:23:51,000
Here also, we may see the first one, shamelessness and second one, fearlessness.

321
00:23:51,000 --> 00:23:59,000
Helika is shamelessness and Anotaba is fearlessness.

322
00:23:59,000 --> 00:24:08,000
And then, there is low bar and more hard describing a graph 161 and 162.

323
00:24:08,000 --> 00:24:16,000
And then, a grid is compared to a bird line or monkey line.

324
00:24:16,000 --> 00:24:26,000
And that is the explanation of the description of a grid-like monkey line can be read in kindred scenes.

325
00:24:26,000 --> 00:24:28,000
Book 5.

326
00:24:28,000 --> 00:24:34,000
Page 127.

327
00:24:34,000 --> 00:24:44,000
When a monkey is stuck to that line, it could not get itself free from that line, that sticky substance.

328
00:24:44,000 --> 00:24:50,000
In the same way, when you have creep, when you have a testament, then you cannot get away from it.

329
00:24:50,000 --> 00:24:59,000
Here, you are stuck to the object.

330
00:24:59,000 --> 00:25:07,000
And then, on paragraph 163, about five lines down, it is manifested as the essence of right theory.

331
00:25:07,000 --> 00:25:11,000
What is theory?

332
00:25:11,000 --> 00:25:13,000
Is it understanding?

333
00:25:13,000 --> 00:25:20,000
It is something that has not been proven yet.

334
00:25:20,000 --> 00:25:25,000
I see it.

335
00:25:25,000 --> 00:25:34,000
The word used here is petipati, and it means understanding or knowing.

336
00:25:34,000 --> 00:25:45,000
So, absence of right understanding.

337
00:25:45,000 --> 00:25:50,000
And then, wrong view, education, and so on.

338
00:25:50,000 --> 00:25:56,000
So, I think they are not difficult to understand.

339
00:25:56,000 --> 00:26:14,000
Now, I brought these two sheets, right?

340
00:26:14,000 --> 00:26:30,000
The sheet is because they are given in the order as in the manual of a beta one.

341
00:26:30,000 --> 00:26:36,000
And then, the Roman numerals are given in this book.

342
00:26:36,000 --> 00:26:49,000
Now, if you look at paragraph 166, end of 166, you find stiffness and topper given only one number.

343
00:26:49,000 --> 00:26:51,000
43, right?

344
00:26:51,000 --> 00:26:54,000
Roman numerals 43.

345
00:26:54,000 --> 00:27:05,000
And then, for Eka Gada, it gives two numbers here in this book.

346
00:27:05,000 --> 00:27:20,000
Actually, that should have only one number on page 532, bottom line steadiness of consciousness.

347
00:27:20,000 --> 00:27:31,000
Steadiness of consciousness is the same as concentration, which is number 8.

348
00:27:31,000 --> 00:27:41,000
It should not be given in separate number, because it is the same mental factor as concentration.

349
00:27:41,000 --> 00:27:52,000
So, stiffness and topper should be given in number 8.

350
00:27:52,000 --> 00:27:57,000
So, there may be some corrections to be made if you want it to be correct.

351
00:27:57,000 --> 00:28:09,000
But as it is, you can look at these sheets and then find out what is meant in the manual of a beta.

352
00:28:09,000 --> 00:28:20,000
Because, in this book, the mental states are given not in the group they belong.

353
00:28:20,000 --> 00:28:26,000
They are given as accompanying different types of consciousness.

354
00:28:26,000 --> 00:28:31,000
So, they are repeated in different types of consciousness.

355
00:28:31,000 --> 00:28:46,000
For example, contact is repeated on page 532, 533 and so on.

356
00:28:46,000 --> 00:28:51,000
And then, what else?

357
00:28:51,000 --> 00:28:59,000
We find another theory on page 533 for a graph 177.

358
00:28:59,000 --> 00:29:04,000
It should be regarded as obstructive of theory.

359
00:29:04,000 --> 00:29:11,000
Here also, I think, we should say obstructive of understanding.

360
00:29:11,000 --> 00:29:19,000
But in Burma, we understand this as meaning obstructive of practice.

361
00:29:19,000 --> 00:29:22,000
So, if you have doubt, then you do not practice.

362
00:29:22,000 --> 00:29:24,000
So, it obstructs your practice.

363
00:29:24,000 --> 00:29:29,000
So, you have doubt about the teaching, you have doubt about the efficacy of this method,

364
00:29:29,000 --> 00:29:31,000
then you will not practice it.

365
00:29:31,000 --> 00:29:35,000
So, it is obstructive to the practice.

366
00:29:35,000 --> 00:29:40,000
So, we interpret the word as practice in Burma.

367
00:29:40,000 --> 00:30:09,000
So, and then, the book describes,

368
00:30:09,000 --> 00:30:18,000
which mental factors accompany, which type of consciousness.

369
00:30:18,000 --> 00:30:22,000
And, as I said, they may be confusing.

370
00:30:22,000 --> 00:30:27,000
You do not have the 89 types of consciousness in mind.

371
00:30:27,000 --> 00:30:37,000
So, it is better to read the manual of a Peter Mayan, find out where which type of consciousness

372
00:30:37,000 --> 00:30:40,000
is accompanied, where which type of THC goes.

373
00:30:40,000 --> 00:30:58,000
Now, one thing, at the beginning, it is said altogether how many?

374
00:30:58,000 --> 00:31:06,000
The first type of consciousness accompanied by 36.

375
00:31:06,000 --> 00:31:09,000
A paragraph 133.

376
00:31:09,000 --> 00:31:14,000
Here, and firstly, those associated with the first sense, we have profitable consciousness

377
00:31:14,000 --> 00:31:19,000
amount to 36.

378
00:31:19,000 --> 00:31:21,000
Is that correct?

379
00:31:21,000 --> 00:31:22,000
Yes.

380
00:31:22,000 --> 00:31:28,000
Why 36 and not that the 8?

381
00:31:28,000 --> 00:31:35,000
Actually, they must be 38, right?

382
00:31:35,000 --> 00:31:36,000
Yes.

383
00:31:36,000 --> 00:31:37,000
Say, why 36?

384
00:31:37,000 --> 00:31:42,000
It is even fixed, may or may not be present.

385
00:31:42,000 --> 00:31:45,000
No.

386
00:31:45,000 --> 00:31:54,000
Open.

387
00:31:54,000 --> 00:32:14,000
You are talking about it.

388
00:32:14,000 --> 00:32:31,000
I will give you a hint.

389
00:32:31,000 --> 00:32:35,000
That is right.

390
00:32:35,000 --> 00:32:38,000
We are talking about formation aggregates here.

391
00:32:38,000 --> 00:32:48,000
In the manual, we are also talking about the mental factors.

392
00:32:48,000 --> 00:32:54,000
In the manual, you will find 38 key seekers accompanying this consciousness.

393
00:32:54,000 --> 00:32:55,000
That is correct.

394
00:32:55,000 --> 00:32:58,000
It is correct too.

395
00:32:58,000 --> 00:33:11,000
Here, the other is describing the mental formation aggregate.

396
00:33:11,000 --> 00:33:16,000
There are 52, but 52 are mental states.

397
00:33:16,000 --> 00:33:32,000
I mean, mental factors.

398
00:33:32,000 --> 00:33:35,000
This chapter is like a manual of a bit.

399
00:33:35,000 --> 00:33:45,000
You are really studying a bit.

400
00:33:45,000 --> 00:33:52,000
So, with these, I think you can hide out which is which.

401
00:33:52,000 --> 00:34:07,000
That is why there are no numbers for we, the, and the, and the, and the, and the, and the,

402
00:34:07,000 --> 00:34:21,000
Thin characteristics or whatever state and plus signs are incalene.

403
00:34:21,000 --> 00:34:27,000
Incalene means they do not accompany all those, and we will that type of consciousness

404
00:34:27,000 --> 00:34:29,000
sometimes only.

405
00:34:29,000 --> 00:34:36,000
Now, for example, number 25 and 26 on this, she cheated and made up.

406
00:34:36,000 --> 00:34:51,000
They accompanied the five prompted, five prompted types of consciousness, but not every time.

407
00:34:51,000 --> 00:35:01,000
Suppose, it wasn't maybe stealing something with actively stealing.

408
00:35:01,000 --> 00:35:07,000
Then, his consciousness may not be accompanied by Tina and made up.

409
00:35:07,000 --> 00:35:19,000
So, only when there is sleepiness or something, Tina and made a company, the consciousness, otherwise they don't.

410
00:35:19,000 --> 00:35:27,000
So, we are going to understand and fixed a chance.

411
00:35:27,000 --> 00:35:37,000
Next week, we will go up to the end of basis.

412
00:35:37,000 --> 00:36:06,000
So, now we have come to the end of five aggregates and next week, some other things to know about five aggregates.

413
00:36:06,000 --> 00:36:15,000
But, the detailed treatment of our aggregates are complete now.

414
00:36:15,000 --> 00:36:20,000
So, what are the five aggregates?

415
00:36:20,000 --> 00:36:27,000
Aggregate of meta, aggregate of feeling, aggregate of perception, aggregate of, I mean,

416
00:36:27,000 --> 00:36:38,000
mental formations and aggregate of consciousness. But, here, aggregate of consciousness is given before the three other aggregates.

417
00:36:38,000 --> 00:36:53,000
But, the usual order given in disorders is meta, feeling, perception, mental formations and consciousness.

418
00:36:53,000 --> 00:36:59,000
And, these five can be reduced to truth, nama and ruba.

419
00:36:59,000 --> 00:37:06,000
So, first, probability aggregates ruba and the other four nama.

420
00:37:06,000 --> 00:37:11,000
So, when we say nama and ruba, we mean these five aggregates.

421
00:37:11,000 --> 00:37:32,000
So, whether we say five aggregates or nama and ruba, we mean the same thing, accurate.

422
00:37:32,000 --> 00:37:48,000
So, let's see.

423
00:37:48,000 --> 00:38:11,000
Thank you.

