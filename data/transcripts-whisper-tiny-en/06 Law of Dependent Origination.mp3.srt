1
00:00:00,000 --> 00:00:23,520
Last time I talked about the law of gamma, the law of gamma is part of the law of cause

2
00:00:23,520 --> 00:00:37,400
and effect. To do I talk about another part of the law of cause and effect and that

3
00:00:37,400 --> 00:00:57,680
is dependent origination. With regard to the cause and effect, there are three views. The

4
00:00:57,680 --> 00:01:13,360
first is that everything both animated and animated is created by a god or a brahma.

5
00:01:13,360 --> 00:01:23,840
And the second view is that things or beings have been without cause. So that is no

6
00:01:23,840 --> 00:01:33,840
cause for the arising and disappearing. And the third view is that boot is few. And that

7
00:01:33,840 --> 00:01:48,560
that view is that it is not created by anybody or any god or brahma. And it is not

8
00:01:48,560 --> 00:02:02,000
cause less. But there are causes or conditions for everything to arise. The law of dependent

9
00:02:02,000 --> 00:02:20,720
origination teaches us that there is nothing that is absolute. So everything is relative

10
00:02:20,720 --> 00:02:34,240
and everything depends upon some other thing for it to arise. And dependent origination

11
00:02:34,240 --> 00:02:47,600
is the law of cause and effect for living beings or for the round of rebirth or round

12
00:02:47,600 --> 00:03:04,400
of existence. There is another teaching of law of cause and effect in Buddhism and that

13
00:03:04,400 --> 00:03:25,120
is the botana conditions or the cause of relations. Now botana conditions or cover

14
00:03:25,120 --> 00:03:37,760
or birth, animate and inanimate things or both living beings and non-living things. And

15
00:03:37,760 --> 00:03:51,680
also in patana, not only is it stated that something is conditioned by some other thing.

16
00:03:51,680 --> 00:04:02,840
But it also gives us how they are conditioned and what we or by what force of condition

17
00:04:02,840 --> 00:04:11,840
they are conditioned. So the law of dependent origination and the law of botana conditions

18
00:04:11,840 --> 00:04:24,200
are a little different. The teaching of dependent origination is very important in the

19
00:04:24,200 --> 00:04:36,040
Buddha's teachings because it makes Buddhism's unique amount of religions while finding

20
00:04:36,040 --> 00:04:51,080
the origination of things, most of the religions end up with a creation of a higher

21
00:04:51,080 --> 00:05:02,840
being or a creation of a God or a Brahma. But in Buddhism, the origination of beings

22
00:05:02,840 --> 00:05:11,400
especially with regard to dependent origination. So origination of beings is explained

23
00:05:11,400 --> 00:05:27,720
in a very reasonable way or in a way that is acceptable through all thinking lessons.

24
00:05:27,720 --> 00:05:44,720
The brief statement for the law of dependent origination is when this exists that comes

25
00:05:44,720 --> 00:05:56,240
to be because of this arising that arises. So this is the brief statement of the dependent

26
00:05:56,240 --> 00:06:11,520
origination. Something arises or happens dependent upon some other thing.

27
00:06:11,520 --> 00:06:19,760
And so whether a Buddha's appear in the world or not, there is always this structure

28
00:06:19,760 --> 00:06:29,760
of causation and this law of dependent origination. But Buddha's appear and then they

29
00:06:29,760 --> 00:06:38,160
discover the law of dependent origination and they refuse it or taught it to the world.

30
00:06:38,160 --> 00:06:55,040
Now, if dependent origination was known to the Buddha, even before his enlightenment in

31
00:06:55,040 --> 00:07:17,120
the discourse in the decanicaya, as well as in the Sayyutanicaya, that every body sada,

32
00:07:17,120 --> 00:07:27,520
from the eve of the enlightenment reflected the ball, the law of dependent origination

33
00:07:27,520 --> 00:07:41,200
and practices vipasana on the 12 factors in the formula of dependent origination. And

34
00:07:41,200 --> 00:07:48,520
as a result of the practice of vipasana on the factors of dependent origination, a

35
00:07:48,520 --> 00:07:55,080
body sada becomes a Buddha. So dependent origination, the law of dependent origination

36
00:07:55,080 --> 00:08:06,280
was known to the Buddha even before their enlightenment. Now, the formula of dependent

37
00:08:06,280 --> 00:08:22,040
origination begins with ignorance. But the line of investigation made by the body sada

38
00:08:22,040 --> 00:08:37,520
is mentioned in the discourses is different. What is sada sada sada sada sada sada sada sada

39
00:08:37,520 --> 00:08:56,660
sada sada sada sada sada sada sada sada sada mean ba sada, ba jama, ba jama folk

40
00:08:56,660 --> 00:09:23,140
Now, as beings, we are always experienced using, we are getting old, day by day, or hour

41
00:09:23,140 --> 00:09:29,020
by hour, minute by minute, second or second.

42
00:09:29,020 --> 00:09:38,140
So getting old is a fact of life, and it is with us all the time.

43
00:09:38,140 --> 00:09:50,940
And when this life will come to an end, so the end of the life which is called death

44
00:09:50,940 --> 00:09:55,140
is also inevitable.

45
00:09:55,140 --> 00:10:10,660
So there is this, getting old and dead, and this is a fact, and we cannot run away from

46
00:10:10,660 --> 00:10:13,220
these realities.

47
00:10:13,220 --> 00:10:30,180
So notice of us reflect, in this way, now there is this old age and death.

48
00:10:30,180 --> 00:10:37,220
What is the condition for this old age and death?

49
00:10:37,220 --> 00:10:43,340
What is there, thus old age and death arise?

50
00:10:43,340 --> 00:10:58,780
So they investigated and they found out that because there is birth, there is old age

51
00:10:58,780 --> 00:11:00,820
and there is death.

52
00:11:00,820 --> 00:11:17,900
As we were born as human beings, we get old, day by day, and at the end we will die.

53
00:11:17,900 --> 00:11:28,580
So the cause of old age and death and ultimate analysis is birth.

54
00:11:28,580 --> 00:11:36,940
Because there is a beginning, there is the continuation and there is the end.

55
00:11:36,940 --> 00:11:46,340
So birth or rebirth is a cause of our condition for old age and death.

56
00:11:46,340 --> 00:11:54,500
But bodhisattas do not stop at that, they went back what father.

57
00:11:54,500 --> 00:12:04,260
So they reflected, why is there a body, why is there a birth or rebirth, what is the

58
00:12:04,260 --> 00:12:11,180
condition for birth, what is the cause of birth.

59
00:12:11,180 --> 00:12:28,460
So when they try to find out the cause of death, they found that it was what is called becoming.

60
00:12:28,460 --> 00:12:37,140
Actually becoming here means something that makes some other thing become.

61
00:12:37,140 --> 00:12:42,500
In fact, it means comma.

62
00:12:42,500 --> 00:12:52,940
So they found that the cause of birth, the cause of rebirth is the comma.

63
00:12:52,940 --> 00:13:05,620
So you know there are two kinds of comma, wholesome comma and unwholesome comma, wholesome

64
00:13:05,620 --> 00:13:17,020
comma gives wholesome, pleasant results and unwholesome comma gives painful results.

65
00:13:17,020 --> 00:13:24,700
To be reborn as a human being is a pleasant result.

66
00:13:24,700 --> 00:13:37,340
And so we were born as human beings because we did some wholesome comma in the past.

67
00:13:37,340 --> 00:13:46,700
There were some beings who were born in hell and they were born in hell because they did

68
00:13:46,700 --> 00:13:52,300
some unwholesome comma in the past.

69
00:13:52,300 --> 00:14:11,740
So wholesome or unwholesome comma is the cause of the condition for rebirth as a human being

70
00:14:11,740 --> 00:14:14,860
or rebirth as any other being.

71
00:14:14,860 --> 00:14:27,700
Now where do we do or why do beings do both wholesome and unwholesome comma or what is the

72
00:14:27,700 --> 00:14:35,900
cause of our condition for the wholesome or unwholesome comma to arise.

73
00:14:35,900 --> 00:14:46,060
Now we do, sometimes we do wholesome comma, sometimes we do unwholesome comma because

74
00:14:46,060 --> 00:15:01,020
we have strong desire, a desire that becomes cleanly, that means that reaches the stage

75
00:15:01,020 --> 00:15:05,980
where it cannot let go.

76
00:15:05,980 --> 00:15:12,220
That strong desire is called Upadana or cleaning.

77
00:15:12,220 --> 00:15:22,700
And this cleaning is a condition for wholesome and unwholesome comma to arise.

78
00:15:22,700 --> 00:15:30,980
For example, we want to be reborn in a better existence, let's say we want to be reborn

79
00:15:30,980 --> 00:15:40,020
in a celestial world.

80
00:15:40,020 --> 00:15:48,220
So we hear from somebody that celestial world is much better than the human world.

81
00:15:48,220 --> 00:15:50,900
And so we have attachment to that celestial world.

82
00:15:50,900 --> 00:16:06,420
We want to be reborn there and so we have premium for rebirth in the celestial worlds.

83
00:16:06,420 --> 00:16:14,500
In order to be reborn in the celestial world, we must do something.

84
00:16:14,500 --> 00:16:24,460
Sometimes we may get good advice or correct advice and so we may do meritorious deeds

85
00:16:24,460 --> 00:16:29,580
which is wholesome comma.

86
00:16:29,580 --> 00:16:43,740
Sometimes we may get wrong instructions like sacrifice, a lamp or an animal.

87
00:16:43,740 --> 00:17:01,780
So if we do sacrifices with the hope of getting rebirth in the world of celestial beings,

88
00:17:01,780 --> 00:17:09,580
we do unwholesome comma because sacrifice means killing a being and killing a being is

89
00:17:09,580 --> 00:17:12,940
unwholesome comma.

90
00:17:12,940 --> 00:17:19,020
So sometimes we do wholesome comma and sometimes we do unwholesome comma because we have

91
00:17:19,020 --> 00:17:28,260
this strong and strong desire to be reborn in a better existence.

92
00:17:28,260 --> 00:17:38,940
Whether we do wholesome comma or unwholesome comma, that is always the result of that

93
00:17:38,940 --> 00:17:49,580
comma and as you know, the result of wholesome comma is a rebirth in the world of bliss

94
00:17:49,580 --> 00:17:58,660
that means human world or the world of divas and the result of Akushala and wholesome

95
00:17:58,660 --> 00:18:03,300
comma is reput in a full, woof of state.

96
00:18:03,300 --> 00:18:13,220
So because we have this strong desire to reach the better existence as we could have

97
00:18:13,220 --> 00:18:32,660
that comma and so the good of that comma is conditioned by the strong desire or cleanly.

98
00:18:32,660 --> 00:18:44,420
So what is the condition for changing?

99
00:18:44,420 --> 00:18:55,220
What causes clinging to a rice and they found out that ordinary desire is the condition

100
00:18:55,220 --> 00:18:57,220
for strong desire.

101
00:18:57,220 --> 00:19:04,620
That means first you have some ordinary desire for something and then you are attached

102
00:19:04,620 --> 00:19:11,180
to it and you cannot give it up or you cannot let it cool.

103
00:19:11,180 --> 00:19:16,900
And so when it reaches that state it is called drinking, so before it reaches that stage

104
00:19:16,900 --> 00:19:27,860
it is called craving or attachment or desire.

105
00:19:27,860 --> 00:19:40,020
So not too strong craving is a condition for strong craving or clinging.

106
00:19:40,020 --> 00:19:46,860
Suppose you see some beautiful thing at its store.

107
00:19:46,860 --> 00:19:58,700
So first you like it, it is a beautiful thing and then you cannot get away from it and

108
00:19:58,700 --> 00:20:04,060
you think that you must buy it and so you buy it.

109
00:20:04,060 --> 00:20:11,020
So at the first scene and you luggage it is like craving but later you cannot get away

110
00:20:11,020 --> 00:20:17,860
from it you must buy it and you buy it then it is clinging.

111
00:20:17,860 --> 00:20:23,100
So when it reaches the stage of clinging it cannot let go.

112
00:20:23,100 --> 00:20:36,580
So both craving and clinging are one and the same mental state, low bar or attachment

113
00:20:36,580 --> 00:20:41,500
but the degree of attachment is different.

114
00:20:41,500 --> 00:20:48,820
So at the stage of craving it is not too not so strong but when it reaches the stage

115
00:20:48,820 --> 00:20:53,700
of clinging it is so strong that it cannot let go.

116
00:20:53,700 --> 00:21:09,740
So the condition for strong attachment of pain is an ordinary not too strong attachment

117
00:21:09,740 --> 00:21:11,060
or craving.

118
00:21:11,060 --> 00:21:25,620
Now why is there desire or why do you like an object?

119
00:21:25,620 --> 00:21:33,340
You like the object because you feel good about that.

120
00:21:33,340 --> 00:21:40,500
There is a pleasant feeling in you with regard to that object that is why you like that

121
00:21:40,500 --> 00:21:41,500
object.

122
00:21:41,500 --> 00:21:55,140
Sometimes the object is not not beautiful you do not like it but when you see an object

123
00:21:55,140 --> 00:22:01,180
you do not like it you have a longing for an object you like it you have a longing for

124
00:22:01,180 --> 00:22:03,220
a beautiful object.

125
00:22:03,220 --> 00:22:13,860
So even when you see an ugly object there can be desire or craving not for that ugly

126
00:22:13,860 --> 00:22:18,660
object but for the beautiful object.

127
00:22:18,660 --> 00:22:27,780
So because there is this feeling pleasant feeling or unpleasant feeling or even neutral

128
00:22:27,780 --> 00:22:31,420
feeling there is this desire.

129
00:22:31,420 --> 00:22:42,260
So the feeling in the mind plus and unpleasant or neutral feeling is the condition for craving

130
00:22:42,260 --> 00:22:47,500
or desire.

131
00:22:47,500 --> 00:22:56,220
Now how is there a feeling usually good about something or bad about something so why what

132
00:22:56,220 --> 00:23:02,460
is the condition for that feeling?

133
00:23:02,460 --> 00:23:08,260
Because there is the contact with the object there is feeling.

134
00:23:08,260 --> 00:23:13,100
If there is no contact with the object there will be no feeling.

135
00:23:13,100 --> 00:23:20,580
So feeling is conditioned by what is called contact.

136
00:23:20,580 --> 00:23:30,660
That is not just putting meaning together but something that arises as a result of

137
00:23:30,660 --> 00:23:40,260
the meaning together of some things for example when we see something there is the

138
00:23:40,260 --> 00:23:49,100
something to be seen and there is there are the eyes and so when that something to be

139
00:23:49,100 --> 00:24:06,140
seen comes into the avenue of the eyes that is seen or seen consciousness.

140
00:24:06,140 --> 00:24:16,580
When these three arise the eye the object and the mind or the consciousness that

141
00:24:16,580 --> 00:24:28,700
sees there also arises what is called contact a mental state a joint something like joint.

142
00:24:28,700 --> 00:24:38,820
So because of that joint feeling follows and other mental states also follow.

143
00:24:38,820 --> 00:24:54,100
So the condition for feeling is contact with the object and why does contact a

144
00:24:54,100 --> 00:25:07,660
device contact arises because in the example of seeing because the eyes and the visible

145
00:25:07,660 --> 00:25:12,420
object come together.

146
00:25:12,420 --> 00:25:24,820
So contact is conditioned by the eye the sense organs and the corresponding objects and

147
00:25:24,820 --> 00:25:34,860
the sense organs and corresponding objects are called in this formula basis.

148
00:25:34,860 --> 00:25:45,260
So there are six internal basis and six external basis the eye is called a base, the ear

149
00:25:45,260 --> 00:25:51,340
is called a base, nose, tongue, body and mind is called a base and visible object is also

150
00:25:51,340 --> 00:25:53,580
called a base and so on.

151
00:25:53,580 --> 00:26:01,100
So from these basis come together there is contact.

152
00:26:01,100 --> 00:26:16,980
So contact is conditioned by the basis by the sense organs and they are corresponding objects.

153
00:26:16,980 --> 00:26:28,380
Now among these basis some are material properties and some are mental states.

154
00:26:28,380 --> 00:26:39,460
So when Bernie said was investigated why there are basis we found that because there

155
00:26:39,460 --> 00:26:44,820
are mental states and material properties there are basis.

156
00:26:44,820 --> 00:26:57,060
So the mental states and material properties are the condition for the basis.

157
00:26:57,060 --> 00:27:15,420
The mental states and material properties to arise now mental states or mental factors

158
00:27:15,420 --> 00:27:23,740
cannot arise if there is no consciousness.

159
00:27:23,740 --> 00:27:34,020
So when there is consciousness only when consciousness arises can be arise.

160
00:27:34,020 --> 00:27:44,220
Now consciousness and some mental states arise at the same time but consciousness is

161
00:27:44,220 --> 00:27:55,700
said to be the chief or the leader because if there is no consciousness no mental states

162
00:27:55,700 --> 00:28:10,660
can arise together with that consciousness and also at the moment of conception some

163
00:28:10,660 --> 00:28:20,340
material properties arise and these material properties arise only when the consciousness

164
00:28:20,340 --> 00:28:23,020
arises.

165
00:28:23,020 --> 00:28:38,700
So the condition for mental states and material properties to arise is said to be consciousness.

166
00:28:38,700 --> 00:28:46,820
So your consciousness means both consciousness at the moment of conception and consciousness

167
00:28:46,820 --> 00:28:52,620
and during life.

168
00:28:52,620 --> 00:29:03,460
Now your consciousness means the resultant consciousness that means consciousness that is the

169
00:29:03,460 --> 00:29:12,700
result of something that arises as a result of something.

170
00:29:12,700 --> 00:29:26,060
For example when we see something we like there is a seeming consciousness and that

171
00:29:26,060 --> 00:29:38,220
seeming consciousness arises as the result as a result of some wholesome karma in the past.

172
00:29:38,220 --> 00:29:45,060
When we see something we don't like there is another type of seeking consciousness and

173
00:29:45,060 --> 00:29:54,460
that seeming consciousness is the result of some unwholesome karma in the past and at the

174
00:29:54,460 --> 00:30:07,860
moment of conception the consciousness that arises is the direct result of karma in the

175
00:30:07,860 --> 00:30:10,020
past.

176
00:30:10,020 --> 00:30:22,740
So when bodhisattas infesticated five there is consciousness especially the result in consciousness

177
00:30:22,740 --> 00:30:33,460
they found out that because there was wholesome and wholesome karma in the past consciousness

178
00:30:33,460 --> 00:30:35,620
arises.

179
00:30:35,620 --> 00:30:51,500
So the condition for consciousness to arise or the cross for consciousness to arise is karma.

180
00:30:51,500 --> 00:31:11,300
And here karma is called sankara formations and why do wholesome and unwholesome karma arise

181
00:31:11,300 --> 00:31:15,620
because there is ignorance.

182
00:31:15,620 --> 00:31:27,860
Because we are ignorant of the true nature of things because we are ignorant of the impermanent

183
00:31:27,860 --> 00:31:40,020
suffering and no true nature of things we tend to do good or bad wholesome and wholesome

184
00:31:40,020 --> 00:31:44,940
karma.

185
00:31:44,940 --> 00:31:53,740
What understanding the true nature of things and not understanding the four noble truths

186
00:31:53,740 --> 00:32:04,380
is a condition for good or bad karma to arise and so ignorance is said to be of found

187
00:32:04,380 --> 00:32:15,180
to be a condition for wholesome or unwholesome karma.

188
00:32:15,180 --> 00:32:29,180
So this line of infestiguation ends with discovery of ignorance.

189
00:32:29,180 --> 00:32:41,460
So if we follow this line of infestiguation we are understanding the law of dependent

190
00:32:41,460 --> 00:32:50,300
origin notion backward from the end to the beginning.

191
00:32:50,300 --> 00:33:06,380
And the usual formula given in the discourses is from the beginning to the end.

192
00:33:06,380 --> 00:33:16,980
So when we go from the beginning to the end of this formula so we begin with ignorance.

193
00:33:16,980 --> 00:33:29,460
So because there is ignorance as a condition and there are formations or wholesome and

194
00:33:29,460 --> 00:33:38,500
wholesome karma and because there are wholesome and unwholesome karma as a condition

195
00:33:38,500 --> 00:33:41,900
there is consciousness and so on.

196
00:33:41,900 --> 00:33:51,900
So this way we can understand the dependent origin notion in any direction from the beginning

197
00:33:51,900 --> 00:33:58,540
to the end or from the end to the beginning or sometimes we may pick up something in the

198
00:33:58,540 --> 00:34:16,660
middle and then go forward or backward or through when we go backward we end with ignorance.

199
00:34:16,660 --> 00:34:31,340
Sometimes it is not the first cause, ignorance is not unconditioned, it is also a conditioned

200
00:34:31,340 --> 00:34:47,980
thing or as I was in in Pali.

201
00:34:47,980 --> 00:34:56,940
Now when there is ignorance there another ignorance follows and then another ignorance follows.

202
00:34:56,940 --> 00:35:03,820
So ignorance is set to be conditioned and the leader ignorance is set to be conditioned

203
00:35:03,820 --> 00:35:10,940
by former ignorance and so it goes on and on and on because ignorance is one of the

204
00:35:10,940 --> 00:35:15,460
asalaas or chains or kankas.

205
00:35:15,460 --> 00:35:22,900
So whenever there be the asalaas arise the ignorance also arises.

206
00:35:22,900 --> 00:35:35,300
So ignorance is set to be conditioned by the arising of kankas.

207
00:35:35,300 --> 00:35:49,100
Now when we look at these 12, oh look at that 12 steps, what 12 factors in this formula,

208
00:35:49,100 --> 00:36:00,340
ignorance, formations, consciousness, mental states and material properties as one and

209
00:36:00,340 --> 00:36:17,260
then basis, contact, feeling, craving, dreamy, becoming, rebirth and then old age and dead

210
00:36:17,260 --> 00:36:19,700
as one.

211
00:36:19,700 --> 00:36:42,420
Now when we look at these 12 factors we see that some factors belong to the present

212
00:36:42,420 --> 00:36:48,740
time or present period, some to the past and some to the future.

213
00:36:48,740 --> 00:37:00,060
And it is in this way that the dependent origination is explained in the commentaries.

214
00:37:00,060 --> 00:37:17,580
Now among the 12 factors the ignorance and mental formations belong to the past.

215
00:37:17,580 --> 00:37:34,100
And rebirth, old age and dead belong to the future.

216
00:37:34,100 --> 00:37:38,940
And the factors in between or to the eight factors in between belong to the present

217
00:37:38,940 --> 00:37:40,460
period.

218
00:37:40,460 --> 00:37:54,300
Now we must not misunderstand this statement, although these 12 factors are distributed

219
00:37:54,300 --> 00:38:17,740
to three periods, we must not understand that they are only in their respective temporal

220
00:38:17,740 --> 00:38:39,180
period because in this life, in a single life we all experience these 12 factors.

221
00:38:39,180 --> 00:38:54,620
In other words, we are not to understand by this commentarial explanation that we must

222
00:38:54,620 --> 00:39:09,140
go through three lives in order to complete the 12 factors mentioned in the formula.

223
00:39:09,140 --> 00:39:17,260
Because we can see that in this life, if this life is our present life, in this life

224
00:39:17,260 --> 00:39:25,780
we experience your ignorance, we experience sankaras, we experience any other things.

225
00:39:25,780 --> 00:39:36,260
So we experience all these 12 factors in this life, but for these 12 factors are distributed

226
00:39:36,260 --> 00:39:43,420
among the three time periods, then some belong to the past, some belong to the present.

227
00:39:43,420 --> 00:40:00,460
So the ignorance and sankaras we experience here could be the past if we take the mixed

228
00:40:00,460 --> 00:40:07,180
life as present.

229
00:40:07,180 --> 00:40:16,700
We both, all days and dead, are set to belong to future period, but that does not mean

230
00:40:16,700 --> 00:40:26,940
that we will experience, we both all days and dead, only in future lives, we will experience

231
00:40:26,940 --> 00:40:30,460
these in this life.

232
00:40:30,460 --> 00:40:42,620
But look at from the point of view of the past life, then the rebirth, old age and dead

233
00:40:42,620 --> 00:40:47,460
in this present life is future.

234
00:40:47,460 --> 00:40:57,100
So although these 12 factors are distributed among three periods, we all experience these

235
00:40:57,100 --> 00:41:05,300
12 factors in this very life, and we do not have to go through three lives to complete

236
00:41:05,300 --> 00:41:09,780
to experience all these factors.

237
00:41:09,780 --> 00:41:21,300
Because we must be very careful, because there are people who think that the commentary

238
00:41:21,300 --> 00:41:30,660
is thought that we must go through three lives in order to complete the cycle of dependent

239
00:41:30,660 --> 00:41:41,580
origin nation, and so they said the commentary was wrong.

240
00:41:41,580 --> 00:41:48,500
Actually, the commentary do not say that you have to go through three lives to complete

241
00:41:48,500 --> 00:41:52,300
and the cycle of what is dependent origin nation.

242
00:41:52,300 --> 00:41:57,780
What they said was these factors belong to the past, these to the present, and these

243
00:41:57,780 --> 00:41:59,820
to the future.

244
00:41:59,820 --> 00:42:08,020
And so in an actual practice, if we look at ourselves, we know that we will be experience

245
00:42:08,020 --> 00:42:12,220
all these factors in this very life.

246
00:42:12,220 --> 00:42:23,020
So we experience all these 12 factors in this very life, but when you look at these

247
00:42:23,020 --> 00:42:30,860
factors from the point of view of this life, from this 10 point of the next life, then

248
00:42:30,860 --> 00:42:39,060
they become present, past, and future.

249
00:42:39,060 --> 00:42:52,980
And in this formula, single conditions are given as a condition for the other condition.

250
00:42:52,980 --> 00:43:02,220
But in fact, according to the teachings of the Buddha, according to teachings of Buddhism,

251
00:43:02,220 --> 00:43:08,980
there is no one cause or one effect.

252
00:43:08,980 --> 00:43:15,220
So Buddhism accepts the multiplicity of causes and effects.

253
00:43:15,220 --> 00:43:32,380
That means let's say for mental formations to arise, ignorance is not the only condition.

254
00:43:32,380 --> 00:43:42,700
There are other conditions as well, like craving and other mental factors.

255
00:43:42,700 --> 00:43:51,940
But here, only the chief conditions of the chief causes are given.

256
00:43:51,940 --> 00:43:59,500
So we must understand that they are not the only conditions or the causes for the following

257
00:43:59,500 --> 00:44:01,900
conditions or following effectors.

258
00:44:01,900 --> 00:44:14,340
The event and origination is not easy to understand because you have to understand some

259
00:44:14,340 --> 00:44:23,100
abitama in order to understand the event and origination fully, I mean, to some extent.

260
00:44:23,100 --> 00:44:35,380
And also, you understand the event and origination more fully when you understand the

261
00:44:35,380 --> 00:44:41,460
law of Patanha conditions.

262
00:44:41,460 --> 00:44:50,900
As I said before, the event and origination explains only that something is conditioned

263
00:44:50,900 --> 00:44:53,540
by some other thing.

264
00:44:53,540 --> 00:45:04,020
But it does not teach us how they are conditioned and what way they are conditioned.

265
00:45:04,020 --> 00:45:13,340
Sometimes if we do not know how they are conditioned, we may misunderstand their relations.

266
00:45:13,340 --> 00:45:27,020
Some links are not the real cause and effect.

267
00:45:27,020 --> 00:45:34,860
Sometimes two links arise together at the same time, but no factor is called a condition

268
00:45:34,860 --> 00:45:38,700
and the other is called a condition.

269
00:45:38,700 --> 00:45:47,780
So in that case, we must understand it with reference to the Patanha relations.

270
00:45:47,780 --> 00:45:58,740
So the event and origination can be understood more fully only with reference to Patanha

271
00:45:58,740 --> 00:45:59,740
conditions.

272
00:45:59,740 --> 00:46:10,140
Otherwise, our understanding of the event and origination may not not not so complete.

273
00:46:10,140 --> 00:46:17,780
But you have to understand the event and origination without mixing it with Patanha

274
00:46:17,780 --> 00:46:21,300
relations is already difficult.

275
00:46:21,300 --> 00:46:33,060
So it is difficult because there are involved a bit of a terms and also we have to understand

276
00:46:33,060 --> 00:46:37,900
perhaps that these terms represent.

277
00:46:37,900 --> 00:46:51,660
So the general meaning of the dependent origination is that everything arises dependent

278
00:46:51,660 --> 00:46:53,700
upon some other thing.

279
00:46:53,700 --> 00:47:03,140
So there is nothing which arises without conditions and that is nothing which is created.

280
00:47:03,140 --> 00:47:15,780
So everything is relative and everything depends upon some other conditions to arise.

281
00:47:15,780 --> 00:47:22,100
So next month we will study the Patanha relations.

282
00:47:22,100 --> 00:47:29,940
So the difference between dependent origination and Patanha relations is something like

283
00:47:29,940 --> 00:47:38,620
saying these two persons, dependent origination is like saying these two persons are related.

284
00:47:38,620 --> 00:47:47,820
And Patanha conditions is like saying these two persons are related as father and son

285
00:47:47,820 --> 00:47:59,620
or as whole to brother and younger brother and so on.

286
00:47:59,620 --> 00:48:04,780
This is the origination.

287
00:48:04,780 --> 00:48:15,700
Now the solution or disappearance is also included in the formula of dependent origination.

288
00:48:15,700 --> 00:48:25,060
Actually, it is not dependent origination but dependent disappearing or something like that.

289
00:48:25,060 --> 00:48:40,100
And that goes similarly with the normal order of origination, that means because of the

290
00:48:40,100 --> 00:48:49,460
disappearance of ignorance, that is the disappearance of formations, because of the

291
00:48:49,460 --> 00:48:54,980
disappearance of emotions, that is the disappearance of consciousness and so on.

292
00:48:54,980 --> 00:49:04,140
So that is the, that is called the reverse order, but the reverse means not going backward,

293
00:49:04,140 --> 00:49:07,300
reverse your means disappearing.

294
00:49:07,300 --> 00:49:16,540
So it is the reverse of the opposite of arising.

295
00:49:16,540 --> 00:49:27,700
So when there is old age and death, those who are oppressed with old age and death who have

296
00:49:27,700 --> 00:49:39,140
ignorance arisen in them and so when suffering from old age and death, they suffer from

297
00:49:39,140 --> 00:49:44,820
what are called kankas and among the kankas there is ignorance and so when there is

298
00:49:44,820 --> 00:49:57,780
ignorance there are wholesome and unwholesome combined and so on and so this feel of life

299
00:49:57,780 --> 00:50:10,860
goes on and on if we cannot dedicate the root causes.

300
00:50:10,860 --> 00:50:16,620
If we cannot eradicate the mental defilements altogether.

301
00:50:16,620 --> 00:50:30,660
But even as we practice meditation, especially Vipasana meditation, we are cutting this feel

302
00:50:30,660 --> 00:50:38,900
at some place, we are not letting this feel go on and on when we practice Vipasana meditation

303
00:50:38,900 --> 00:50:49,820
and as you know, when you practice Vipasana meditation, you can break the link between

304
00:50:49,820 --> 00:50:58,340
especially feeling and craving.

305
00:50:58,340 --> 00:51:04,940
Although you have feeling, you will not have craving if you practice Vipasana meditation

306
00:51:04,940 --> 00:51:11,060
and if you practice mindfulness.

307
00:51:11,060 --> 00:51:24,740
So by the Vipasana meditation, we can cut or we can sever some links in this law of

308
00:51:24,740 --> 00:51:34,620
dependent origination, so you have a pleasant feeling and if you are not mindful, if you do

309
00:51:34,620 --> 00:51:43,420
not make notes of this pleasant feeling, you will like it and so when you like it, you

310
00:51:43,420 --> 00:51:53,180
let this dependent origination or this feel of life, who on and on and if you have unpleasant

311
00:51:53,180 --> 00:52:01,620
feeling, and then you know for all bliss and feeling, again, you are letting this wheel

312
00:52:01,620 --> 00:52:04,940
of life go on and on.

313
00:52:04,940 --> 00:52:11,820
You have a neutral feeling, neutral feeling is very good and so you will like it.

314
00:52:11,820 --> 00:52:17,060
So when you like it again, you are letting this if you go on and on.

315
00:52:17,060 --> 00:52:26,780
But when you practice Vipasana meditation and you may keep effort to be mindful whenever

316
00:52:26,780 --> 00:52:33,220
any feeling becomes prominent, then you are cutting this feel.

317
00:52:33,220 --> 00:52:41,700
You are not letting this wheel of life, wheel of life go on and on and so by the Vipasana

318
00:52:41,700 --> 00:52:48,300
meditation, we are trying to cut this feel of life.

319
00:52:48,300 --> 00:52:56,420
And if we are successful, totally successful with our practice, then if we reach the

320
00:52:56,420 --> 00:53:05,900
stage of our hands yet, then we will be able to cut this or destroy this feel of life

321
00:53:05,900 --> 00:53:19,300
all together, that describes these factors in the dependent origination as a mess of suffering.

322
00:53:19,300 --> 00:53:29,580
So this mess of suffering goes on and on and on, so long as we cannot get rid of mental

323
00:53:29,580 --> 00:53:41,100
defilements all together, we want to get out of this wheel of life, if we want to break

324
00:53:41,100 --> 00:53:51,740
this wheel of life and get out of suffering, then we should do something to stop this

325
00:53:51,740 --> 00:54:02,140
feel from rolling on and on.

326
00:54:02,140 --> 00:54:10,540
So when you practice Vipasana meditation, you are trying to cut this feel, you are trying

327
00:54:10,540 --> 00:54:16,180
to stop this feel from rolling on and on.

328
00:54:16,180 --> 00:54:25,060
So at every moment of good mindfulness, at every moment of understanding the true nature

329
00:54:25,060 --> 00:54:40,860
of things, you are going towards destroying this feel of life and to get for that purpose,

330
00:54:40,860 --> 00:54:44,140
we practice Vipasana meditation.

331
00:54:44,140 --> 00:54:53,980
And so we hope that one day we will be able to achieve our core and cut this wheel of

332
00:54:53,980 --> 00:55:00,540
existence or the round of rebut.

333
00:55:00,540 --> 00:55:16,300
It takes dependent origination as just arising of something dependent about some other

334
00:55:16,300 --> 00:55:23,660
thing than we could say so, but if we take dependent origination as the whole formula,

335
00:55:23,660 --> 00:55:40,060
I think we cannot, because at every moment we may not yet all these 12 factors.

336
00:55:40,060 --> 00:55:54,980
We find the word used as in the 5th average, right, Sankara Kanda, and here as a factor of

337
00:55:54,980 --> 00:56:02,620
the dependent origination, at least there are two meanings for this word Sankara.

338
00:56:02,620 --> 00:56:15,500
But what is the same, but by its foundation according to etymology, it means two things.

339
00:56:15,500 --> 00:56:25,060
One is something that, let us say, that makes that produces.

340
00:56:25,060 --> 00:56:32,540
So there is an active meaning and it can also be understood in a passive sense.

341
00:56:32,540 --> 00:56:37,580
That means something that is made or that is produced.

342
00:56:37,580 --> 00:56:43,900
So there are two meanings to the word Sankara, not only to their other meanings to it,

343
00:56:43,900 --> 00:56:49,500
but what is important to understand here is two meanings.

344
00:56:49,500 --> 00:56:55,540
Sankara is something that produces and also something that is produced.

345
00:56:55,540 --> 00:57:02,780
Now the word Sankara in dependent origination means gamma, right?

346
00:57:02,780 --> 00:57:06,500
So gamma is something that produces its results.

347
00:57:06,500 --> 00:57:13,900
So here we have to understand the word Sankara in an active sense.

348
00:57:13,900 --> 00:57:21,340
But when we say it's a based on kara and it all Sankara are impermanent, we have to

349
00:57:21,340 --> 00:57:25,940
understand it in a passive sense.

350
00:57:25,940 --> 00:57:34,740
Sankara means all that are conditioned, that are made, that are produced or that are conditioned.

351
00:57:34,740 --> 00:57:43,140
So whenever we see the word Sankara in the discourses, we have to be very careful, we have

352
00:57:43,140 --> 00:57:54,500
to know whether to be understood in the passive sense or passive sense.

353
00:57:54,500 --> 00:58:10,540
And Sankara Kanda, we should understand it to mean in an active sense because Sankara

354
00:58:10,540 --> 00:58:25,820
Kanda, any Sankara Kanda is headed by Kita, right, which is called Sankara or Kama.

355
00:58:25,820 --> 00:58:33,940
So in the word Sankara Kanda, we must understand it in an active sense.

356
00:58:33,940 --> 00:58:39,700
And also here in dependent origination, we understand it in an active sense.

357
00:58:39,700 --> 00:58:46,180
And in the verses in the Dhamabara, some based on kara and it and so on, we understand,

358
00:58:46,180 --> 00:58:50,220
we should understand it to in passive sense.

359
00:58:50,220 --> 00:58:56,340
So at least there are two many issues of the word Sankara.

360
00:58:56,340 --> 00:59:06,740
Whenever we see the word Sankara, we cannot translate it by just one word, one English

361
00:59:06,740 --> 00:59:11,340
word, one word, one word, one Vietnamese word.

362
00:59:11,340 --> 00:59:18,220
So we have to understand, according to context, whether it is to be understood in the

363
00:59:18,220 --> 00:59:20,820
active sense or passive sense.

364
00:59:20,820 --> 00:59:32,660
Now, nowadays, the word Sankara is translated as formations, and in it, the Vayana Dijoga

365
00:59:32,660 --> 00:59:42,100
also translated as formations, or kama formations, or mandar formations.

366
00:59:42,100 --> 00:59:48,620
I don't know whether that is correct or not.

367
00:59:48,620 --> 00:59:58,740
Can formation means both something that is formed and something that forms.

368
00:59:58,740 --> 01:00:01,740
No?

369
01:00:01,740 --> 01:00:06,020
So formation seems to mean something that is formed.

370
01:00:06,020 --> 01:00:33,140
So it is better to translate it as conditions and conditioning.

