1
00:00:00,000 --> 00:00:06,000
Speaking about alcohol, let's say I've got an advice for people they don't drink.

2
00:00:06,000 --> 00:00:10,000
Like they don't want to look too religious to others.

3
00:00:10,000 --> 00:00:14,000
Let's say because I certainly don't like somebody like anyone.

4
00:00:14,000 --> 00:00:16,000
Somebody wants to go out for somewhere.

5
00:00:16,000 --> 00:00:19,000
Just go drink two up to three beers.

6
00:00:19,000 --> 00:00:21,000
You're not going to get drunk.

7
00:00:21,000 --> 00:00:23,000
And it's a good solution.

8
00:00:23,000 --> 00:00:25,000
It's not bad.

9
00:00:25,000 --> 00:00:32,000
If you just get two beers, just to, you know, show that you can drink.

10
00:00:32,000 --> 00:00:36,000
But not just that you get drunk.

11
00:00:36,000 --> 00:00:41,000
But even if it were that simple, I would say no.

12
00:00:41,000 --> 00:00:51,000
If I go around telling people to, or simply put, you're setting a bad example for people.

13
00:00:51,000 --> 00:00:55,000
You are reinforcing the belief that drinking is okay.

14
00:00:55,000 --> 00:01:00,000
Even if, so even if you weren't getting drunk as a result of drinking two beers,

15
00:01:00,000 --> 00:01:05,000
which actually you are, even the smallest bit of alcohol, it makes you the smallest bit drunk.

16
00:01:05,000 --> 00:01:09,000
It's not a line or something.

17
00:01:09,000 --> 00:01:12,000
Whoops, now I'm drunk.

18
00:01:12,000 --> 00:01:14,000
It comes gradually.

19
00:01:14,000 --> 00:01:17,000
You're not going to get drunk.

20
00:01:17,000 --> 00:01:19,000
But do you understand what I'm saying?

21
00:01:19,000 --> 00:01:21,000
There is no such thing as drunk.

22
00:01:21,000 --> 00:01:23,000
Drunk is just a line in the sand.

23
00:01:23,000 --> 00:01:25,000
The more you drink, the more drunk you are.

24
00:01:25,000 --> 00:01:28,000
It's not a line where suddenly whoops, I'm drunk.

25
00:01:28,000 --> 00:01:30,000
Up until this point I was fine.

26
00:01:30,000 --> 00:01:31,000
But suddenly boom, I'm drunk.

27
00:01:31,000 --> 00:01:32,000
You know this, I think.

28
00:01:32,000 --> 00:01:33,000
It's poison.

29
00:01:33,000 --> 00:01:34,000
So it poisoned.

30
00:01:34,000 --> 00:01:37,000
So you have a little bit of poison, you're a little bit poisoned.

31
00:01:37,000 --> 00:01:39,000
I mean, I'm talking from experience.

32
00:01:39,000 --> 00:01:40,000
I've been there.

33
00:01:40,000 --> 00:01:42,000
I'm a bit of a lush myself.

34
00:01:42,000 --> 00:01:49,000
But I've been here with that person's coming from saying,

35
00:01:49,000 --> 00:01:55,000
so hang over, they don't have much to add to the discussion.

36
00:01:55,000 --> 00:01:58,000
Pickled.

37
00:01:58,000 --> 00:02:00,000
It's always unpleasant.

38
00:02:00,000 --> 00:02:01,000
It's always unpleasant.

39
00:02:01,000 --> 00:02:03,000
Somebody asked me to drink.

40
00:02:03,000 --> 00:02:05,000
But there's so much more than that as well.

41
00:02:05,000 --> 00:02:07,000
There's the crowd that you're hanging out with.

42
00:02:07,000 --> 00:02:09,000
You're hanging out with people who are drinking as well.

43
00:02:09,000 --> 00:02:11,000
And you're encouraging it.

44
00:02:11,000 --> 00:02:14,000
You're engaged in this situation.

45
00:02:14,000 --> 00:02:16,000
I mean, it's very hard.

46
00:02:16,000 --> 00:02:23,000
Because in real life, you meet with your friends.

47
00:02:23,000 --> 00:02:28,000
And they really got nothing else to do and get to all.

48
00:02:28,000 --> 00:02:30,000
And it's just kind of pathetic.

49
00:02:30,000 --> 00:02:37,000
And you see that you can't deal with your social life in the Buddhist way.

50
00:02:37,000 --> 00:02:39,000
Because people are just, you know what?

51
00:02:39,000 --> 00:02:41,000
I mean, let's go.

52
00:02:41,000 --> 00:02:45,000
Let's go and get drunk or let's smoke weed.

53
00:02:45,000 --> 00:02:47,000
The weed is all right.

54
00:02:47,000 --> 00:02:49,000
Because it's not messy.

55
00:02:49,000 --> 00:02:52,000
I mean, it doesn't give you.

56
00:02:52,000 --> 00:02:55,000
It might show you some reality, at least.

57
00:02:55,000 --> 00:02:58,000
Might show you that your mind is.

58
00:02:58,000 --> 00:03:00,000
What kind of marijuana?

59
00:03:00,000 --> 00:03:02,000
Yes, yes, yes, yes, yes.

60
00:03:02,000 --> 00:03:04,000
No, you're right.

61
00:03:04,000 --> 00:03:07,000
I mean, it might show you.

62
00:03:07,000 --> 00:03:14,000
I mean, some people say, hey, no, some people say,

63
00:03:14,000 --> 00:03:17,000
oh, I don't smoke really because I don't feel good.

64
00:03:17,000 --> 00:03:18,000
I say, you know what?

65
00:03:18,000 --> 00:03:21,000
Because every day, that's how we feel every day.

66
00:03:21,000 --> 00:03:23,000
That's what I think.

67
00:03:23,000 --> 00:03:32,000
So I think that marijuana is not that bad in social life.

68
00:03:32,000 --> 00:03:41,000
Yeah, I mean, if social life is the goal, it's not a goal.

69
00:03:41,000 --> 00:03:42,000
Well, it's not a goal.

70
00:03:42,000 --> 00:03:49,000
I mean, I mean, I don't have need for social life,

71
00:03:49,000 --> 00:03:54,000
but sometimes you have a front of your situation

72
00:03:54,000 --> 00:04:00,000
that you basically become a loner.

73
00:04:00,000 --> 00:04:03,000
That means wonderful.

74
00:04:03,000 --> 00:04:05,000
Yeah.

75
00:04:05,000 --> 00:04:08,000
What are people going to teach about?

76
00:04:08,000 --> 00:04:10,000
I mean, you know what I mean?

77
00:04:10,000 --> 00:04:14,000
Well, that doesn't sound very skillful of thought.

78
00:04:14,000 --> 00:04:15,000
Yeah.

79
00:04:15,000 --> 00:04:16,000
It doesn't sound very good.

80
00:04:16,000 --> 00:04:20,000
Well, I want them to think that I'm normal.

81
00:04:20,000 --> 00:04:24,000
Is that a skillful thought or an unskillful thought?

82
00:04:24,000 --> 00:04:27,000
Is that a skillful desire or an unskillful desire?

83
00:04:27,000 --> 00:04:30,000
You see, I have stopped drinking long time ago,

84
00:04:30,000 --> 00:04:32,000
but if somebody asked me, I came to a conclusion

85
00:04:32,000 --> 00:04:37,000
like, I'd rather have two beers and have it be off my back

86
00:04:37,000 --> 00:04:40,000
that why I don't drink and why I didn't see nothing.

87
00:04:40,000 --> 00:04:45,000
You always would open up a whole new line of conversation

88
00:04:45,000 --> 00:04:48,000
if you could talk to people about,

89
00:04:48,000 --> 00:04:52,000
no, a great reason if people want to avoid that kind of thing.

90
00:04:52,000 --> 00:04:56,000
You could say it's because I want to take care of the other

91
00:04:56,000 --> 00:04:57,000
people who are drinking.

92
00:04:57,000 --> 00:05:00,000
Be the designated driver, for example.

93
00:05:00,000 --> 00:05:07,000
Because, you know, I'm much happier when I can take care of people

94
00:05:07,000 --> 00:05:10,000
when I'm in full control of my senses.

95
00:05:10,000 --> 00:05:14,000
You know, what an opportunity you're missing by becoming drunk

96
00:05:14,000 --> 00:05:20,000
to not to laugh at drunk people I suppose,

97
00:05:20,000 --> 00:05:26,000
but not to help or to be the designated driver, for example.

98
00:05:26,000 --> 00:05:30,000
But, you know, you're in the wrong crowd if you've got it,

99
00:05:30,000 --> 00:05:32,000
you know, if your friends can't,

100
00:05:32,000 --> 00:05:33,000
well, is this thing?

101
00:05:33,000 --> 00:05:37,000
If your friends can't have fun without drinking,

102
00:05:37,000 --> 00:05:39,000
you need new friends.

103
00:05:39,000 --> 00:05:42,000
I mean, they're talking about population here.

104
00:05:42,000 --> 00:05:44,000
I'm not talking about friends.

105
00:05:44,000 --> 00:05:50,000
I'm talking about a whole society.

106
00:05:50,000 --> 00:05:51,000
Lots of good people.

107
00:05:51,000 --> 00:05:52,000
Look at the people here.

108
00:05:52,000 --> 00:05:53,000
We're not drinking.

109
00:05:53,000 --> 00:05:54,000
No.

110
00:05:54,000 --> 00:05:55,000
Yeah.

111
00:05:55,000 --> 00:05:56,000
I don't drink.

112
00:05:56,000 --> 00:05:58,000
Lots of good people here.

113
00:05:58,000 --> 00:06:00,000
Oh, there you go.

114
00:06:00,000 --> 00:06:01,000
Find the better society.

115
00:06:01,000 --> 00:06:02,000
Yeah.

116
00:06:02,000 --> 00:06:06,000
There's more to it than that.

117
00:06:06,000 --> 00:06:08,000
Really, the key is when you drink two beers,

118
00:06:08,000 --> 00:06:09,000
you become two beers drunk.

119
00:06:09,000 --> 00:06:11,000
It's a bad idea.

120
00:06:11,000 --> 00:06:14,000
And you, sorry about it, but if you have really liked beer,

121
00:06:14,000 --> 00:06:15,000
it's nice to hear about it.

122
00:06:15,000 --> 00:06:16,000
I'm not one.

123
00:06:16,000 --> 00:06:24,000
I'm just stating that it's a shame in many ways,

124
00:06:24,000 --> 00:06:34,000
because it does to your mind and your idea of what's right and so on.

125
00:06:34,000 --> 00:06:41,000
And, you know, the dulled state that it brings to drink.

126
00:06:41,000 --> 00:06:42,000
Yeah.

127
00:06:42,000 --> 00:06:45,000
And the opportunities that are missed to change your life,

128
00:06:45,000 --> 00:06:50,000
to change your society, that would be forced by having to confront this,

129
00:06:50,000 --> 00:06:53,000
by having to draw the line in the sand and say,

130
00:06:53,000 --> 00:06:55,000
no, I won't cross this.

131
00:06:55,000 --> 00:06:56,000
Yeah.

132
00:06:56,000 --> 00:06:57,000
I've had no problem.

133
00:06:57,000 --> 00:06:59,000
I'm so confident about it.

134
00:06:59,000 --> 00:07:00,000
Because I leave.

135
00:07:00,000 --> 00:07:02,000
But then start those conversations.

136
00:07:02,000 --> 00:07:05,000
I mean, start the conversation.

137
00:07:05,000 --> 00:07:10,000
I don't drink and here's the reason why.

138
00:07:10,000 --> 00:07:17,000
Yeah, but it sounds like it makes me feel like I'm holding it.

139
00:07:17,000 --> 00:07:19,000
You know what I mean?

140
00:07:19,000 --> 00:07:21,000
Choose the path.

141
00:07:21,000 --> 00:07:23,000
Choose the path that you want to follow.

142
00:07:23,000 --> 00:07:26,000
If you want to follow the social rights and friends and everything,

143
00:07:26,000 --> 00:07:30,000
along those lines and follow the path that you want to follow,

144
00:07:30,000 --> 00:07:33,000
the Buddha's teachings and follow that path.

145
00:07:33,000 --> 00:07:34,000
You have to make a choice.

146
00:07:34,000 --> 00:07:35,000
I don't have a problem.

147
00:07:35,000 --> 00:07:36,000
I don't know.

148
00:07:36,000 --> 00:07:37,000
I don't know.

149
00:07:37,000 --> 00:07:38,000
I don't know.

150
00:07:38,000 --> 00:07:39,000
I don't know.

151
00:07:39,000 --> 00:07:40,000
I don't know.

152
00:07:40,000 --> 00:07:41,000
I don't know.

153
00:07:41,000 --> 00:07:45,000
You see, I was in drinking for two years.

154
00:07:45,000 --> 00:07:47,000
I stopped drinking two years ago.

155
00:07:47,000 --> 00:07:49,000
And I committed myself to this very,

156
00:07:49,000 --> 00:07:51,000
because I used to drink a lot from Poland.

157
00:07:51,000 --> 00:07:54,000
I started drinking vodka when I was 11.

158
00:07:54,000 --> 00:07:57,000
So I know quite a lot about drinking.

159
00:07:57,000 --> 00:08:01,000
Not to mention my family.

160
00:08:01,000 --> 00:08:03,000
And many other families.

161
00:08:03,000 --> 00:08:10,000
But I'm confident that I can drink and still,

162
00:08:10,000 --> 00:08:13,000
and by no meaning, enjoy it.

163
00:08:13,000 --> 00:08:18,000
And I was like, I was stuck after two years not drinking.

164
00:08:18,000 --> 00:08:20,000
My mate asked me like, hey, let's go ahead.

165
00:08:20,000 --> 00:08:23,000
Because he had some problem with something.

166
00:08:23,000 --> 00:08:28,000
Let's have some, I hope, and let's go ahead and have a time.

167
00:08:28,000 --> 00:08:30,000
I have some beers.

168
00:08:30,000 --> 00:08:35,000
And I drank these two beers, three beers.

169
00:08:35,000 --> 00:08:40,000
And that was like, I didn't enjoy it, you know?

170
00:08:40,000 --> 00:08:41,000
Yes.

171
00:08:41,000 --> 00:08:42,000
I mean, I didn't like it.

172
00:08:42,000 --> 00:08:45,000
You're only talking from within your own vibes,

173
00:08:45,000 --> 00:08:47,000
or I don't mean to sound too hard.

174
00:08:47,000 --> 00:08:49,000
But I just like the argument.

175
00:08:49,000 --> 00:08:53,000
I mean to come down on you, but just the point

176
00:08:53,000 --> 00:08:59,000
that I would observe is that you can only look

177
00:08:59,000 --> 00:09:00,000
at our own position.

178
00:09:00,000 --> 00:09:02,000
We can only look from our own position.

179
00:09:02,000 --> 00:09:06,000
We can't look from outside and see what actually is going on.

180
00:09:06,000 --> 00:09:07,000
And I would be willing to bed.

181
00:09:07,000 --> 00:09:10,000
And I'm not trying to be hard, or like some religious person

182
00:09:10,000 --> 00:09:12,000
saying, no, bad, you.

183
00:09:12,000 --> 00:09:17,000
But I'd be willing to bed just as a kind of a friendly

184
00:09:17,000 --> 00:09:21,000
stranger, that if you did stop, if you did,

185
00:09:21,000 --> 00:09:28,000
go to the extent of not drinking those two beers.

186
00:09:28,000 --> 00:09:31,000
Or let's talk about someone who does that on a weekly basis,

187
00:09:31,000 --> 00:09:34,000
because for you, it might just be whenever the occasion occurs.

188
00:09:34,000 --> 00:09:36,000
But suppose someone said, well, every weekend,

189
00:09:36,000 --> 00:09:38,000
I have a couple of beers with my friends,

190
00:09:38,000 --> 00:09:41,000
that if they stopped doing that, it would change their life

191
00:09:41,000 --> 00:09:45,000
in some meaningful way, maybe not huge profoundly,

192
00:09:45,000 --> 00:09:49,000
but in a meaningful way, by stopping that their life would change.

193
00:09:49,000 --> 00:09:53,000
Their mind would be affected, and moreover,

194
00:09:53,000 --> 00:09:55,000
their surroundings would be affected.

195
00:09:55,000 --> 00:09:57,000
The other people would be affected.

196
00:09:57,000 --> 00:09:58,000
Their whole life would change.

197
00:09:58,000 --> 00:10:02,000
So the rules are seek out, but they're

198
00:10:02,000 --> 00:10:05,000
rules of training, or they're paths of training.

199
00:10:05,000 --> 00:10:09,000
They're for the purpose of leading you onward.

200
00:10:09,000 --> 00:10:13,000
They're not to just maintain the status quo and not go to hell.

201
00:10:13,000 --> 00:10:17,000
There are means of enlightening you.

202
00:10:17,000 --> 00:10:23,000
See la pari bawito, summatin mohap, alohoti,

203
00:10:23,000 --> 00:10:27,000
mohap, alohoti, mohanni sense.

204
00:10:27,000 --> 00:10:31,000
The concentration that comes from morality, concentration,

205
00:10:31,000 --> 00:10:34,000
morality is what leads to concentration,

206
00:10:34,000 --> 00:10:37,000
and the concentration which comes from morality,

207
00:10:37,000 --> 00:10:42,000
that is properly developed, is a great benefit.

