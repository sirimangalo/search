1
00:00:00,000 --> 00:00:29,640
Good evening and everyone broadcasting live, March 28th, tonight's verse quote is from

2
00:00:29,640 --> 00:00:44,700
Argentina and Al

3
00:00:44,700 --> 00:01:07,820
You are also a sateh, it's an interesting one, you are also a sateh, who is strong, who is

4
00:01:07,820 --> 00:01:14,820
excellent, exceptional, will stand.

5
00:01:14,820 --> 00:01:35,820
He will say, repeated the admonishing I shall speak repeatedly testing.

6
00:01:35,820 --> 00:02:00,820
Ning-dae-ha, ning-dae-ha, han-dah wak-karmi, pow-wa-hah ba-wa-ha, nang-dah wak-karmi.

7
00:02:00,820 --> 00:02:14,820
So, I will nig-dae-ha is telling you what not to do.

8
00:02:14,820 --> 00:02:19,820
I will speak to tell you what not to do, but by half of I have, I will tell you to do this,

9
00:02:19,820 --> 00:02:22,820
do this, urge you to do this to do that.

10
00:02:22,820 --> 00:02:29,820
So, there are two ways of teaching. One is by rebuking, no, don't do that.

11
00:02:29,820 --> 00:02:35,820
And the other is by exhorting, do that.

12
00:02:35,820 --> 00:02:41,820
This is what is meant to ning-dae-ha, ning-dae-ha, han-dah wak-harmi.

13
00:02:41,820 --> 00:02:46,820
When I speak to you, I will speak, sometimes rebuking, you don't do this.

14
00:02:46,820 --> 00:02:55,820
But by half of I, han-dah wak-karmi, I will speak, urging you on your sorrow so tacitty,

15
00:02:55,820 --> 00:03:03,820
and who is strong or who is exceptional, who is up for the task.

16
00:03:03,820 --> 00:03:06,820
They will bear, they will stand.

17
00:03:06,820 --> 00:03:12,820
They will be able to stand with stand the tree, the teaching.

18
00:03:12,820 --> 00:03:16,820
That is the very end of the quote, but it is the most interesting part.

19
00:03:16,820 --> 00:03:19,820
Because it is kind of unique.

20
00:03:19,820 --> 00:03:27,820
It says, I shall not treat you the way a potter treats them clay.

21
00:03:27,820 --> 00:03:46,820
Now, I will speak to you, and I will speak to you, and I will speak to you.

22
00:03:46,820 --> 00:04:02,820
There is a potter treat that went cloth, that went clay.

23
00:04:02,820 --> 00:04:21,820
I will speak to you, and I will speak to you.

24
00:04:21,820 --> 00:04:39,820
I will speak to you, and I will speak to you, and I will speak to you.

25
00:04:39,820 --> 00:05:01,820
A potter takes, takes wet clay in both hands, and says, ma vinta tu don't break.

26
00:05:01,820 --> 00:05:15,820
I may know this is made.

27
00:05:15,820 --> 00:05:16,820
I don't get it.

28
00:05:16,820 --> 00:05:22,820
Someone will have to explain that one to me.

29
00:05:22,820 --> 00:05:24,820
So, this is a budget teacher.

30
00:05:24,820 --> 00:05:32,820
The quote is all about, I mean, that part that I mangled with is extra, but the main quote

31
00:05:32,820 --> 00:05:38,820
is about love for the teacher.

32
00:05:38,820 --> 00:05:42,820
And the Buddha gives a specific take on the idea of love for the teacher.

33
00:05:42,820 --> 00:05:43,820
It is not.

34
00:05:43,820 --> 00:05:48,820
When you think of love for a teacher, you are sitting there staring at your teacher, looking at pictures

35
00:05:48,820 --> 00:05:53,820
of your teacher, thinking about your teacher boy, I love my teacher.

36
00:05:53,820 --> 00:05:57,820
Remember, what can you know?

37
00:05:57,820 --> 00:06:03,820
This is not the way one pays respect to the Buddha, to the teacher.

38
00:06:03,820 --> 00:06:08,820
So, how do you have love for your teacher?

39
00:06:08,820 --> 00:06:11,820
You listen to your teacher.

40
00:06:11,820 --> 00:06:21,820
You listen to your teacher, you lend an ear, and you prepare your mind for profound knowledge.

41
00:06:21,820 --> 00:06:26,820
You do not turn aside or move away from the instruction.

42
00:06:26,820 --> 00:06:29,820
That's how you love your teacher.

43
00:06:29,820 --> 00:06:39,820
It's important because the teacher is all about the teaching, and the teacher's work is to impart the teaching.

44
00:06:39,820 --> 00:06:46,820
Now, if you don't follow it, if you don't listen to it, that's really the worst thing for the teacher.

45
00:06:46,820 --> 00:06:53,820
It makes it very difficult because it's stressed and fatigued to the teacher.

46
00:06:53,820 --> 00:06:55,820
It's why many are enhanced, just don't teach.

47
00:06:55,820 --> 00:06:59,820
They say, you wouldn't understand if I taught you.

48
00:06:59,820 --> 00:07:05,820
You'd probably just throw it away like an old piece of trash.

49
00:07:05,820 --> 00:07:13,820
So, here we go.

50
00:07:13,820 --> 00:07:16,820
So, here we go.

51
00:07:16,820 --> 00:07:17,820
Someone teaches.

52
00:07:17,820 --> 00:07:18,820
Someone who teaches.

53
00:07:18,820 --> 00:07:20,820
This is for your benefit.

54
00:07:20,820 --> 00:07:21,820
This is for your happiness.

55
00:07:21,820 --> 00:07:22,820
This is a teacher.

56
00:07:22,820 --> 00:07:24,820
Someone who tells you this.

57
00:07:24,820 --> 00:07:28,820
Rightly identifies what is to your benefit and teaches it to you.

58
00:07:28,820 --> 00:07:33,820
Rightly identifies what is for your happiness and teaches it to you.

59
00:07:33,820 --> 00:07:36,820
That's how they're students.

60
00:07:36,820 --> 00:07:42,820
So, so, so, so, so, so, so, so, so.

61
00:07:42,820 --> 00:07:44,820
So, so, so, so, so.

62
00:07:44,820 --> 00:07:45,820
Listen well.

63
00:07:45,820 --> 00:07:47,820
So, don't hold the hand.

64
00:07:47,820 --> 00:07:50,820
This is the lending in the air.

65
00:07:50,820 --> 00:07:51,820
So, hold the hand.

66
00:07:51,820 --> 00:08:01,820
I think it means incline the air to put down to place the air on the object.

67
00:08:01,820 --> 00:08:04,820
And it's just a figure of speech, of course.

68
00:08:04,820 --> 00:08:11,820
I don't know if they put the air, but it means it's actually listening, trying to understand.

69
00:08:11,820 --> 00:08:17,820
Anya jitang upa, upa tappi nti.

70
00:08:17,820 --> 00:08:21,820
This is Anya on right knowledge.

71
00:08:21,820 --> 00:08:29,820
One, sit sets the mind on knowledge.

72
00:08:29,820 --> 00:08:39,820
Nya, chia wokam wokamma, sattu sattu sattu, they don't sattu sasana watanti.

73
00:08:39,820 --> 00:08:43,820
They don't work to turn aside.

74
00:08:43,820 --> 00:08:47,820
They don't dwell watanti, right?

75
00:08:47,820 --> 00:08:56,820
They don't live, you know, go about their business having put aside the sattu sasana,

76
00:08:56,820 --> 00:09:02,820
dispensation of the teaching of the teacher.

77
00:09:02,820 --> 00:09:10,820
I want kuhan and sattarang savaka, savaka, mita wataya,

78
00:09:10,820 --> 00:09:12,820
sumu dajaranti.

79
00:09:12,820 --> 00:09:16,820
It's not actually made the...

80
00:09:16,820 --> 00:09:22,820
They deport themselves.

81
00:09:22,820 --> 00:09:26,820
Mita wah.

82
00:09:26,820 --> 00:09:30,820
No sattu wah.

83
00:09:30,820 --> 00:09:34,820
The problem is one who has a friend.

84
00:09:34,820 --> 00:09:38,820
There's worth himself's friendly, I think.

85
00:09:38,820 --> 00:09:42,820
Not like an enemy, not hostile.

86
00:09:42,820 --> 00:09:46,820
With hostility.

87
00:09:46,820 --> 00:09:53,820
This is the Mahasunya, sunyatasuta.

88
00:09:53,820 --> 00:09:58,820
A lot of its repetitions, so you might think, well, you know, it's all the same,

89
00:09:58,820 --> 00:09:59,820
150 sutres.

90
00:09:59,820 --> 00:10:02,820
But each suthe is unique.

91
00:10:02,820 --> 00:10:06,820
Each suthe has something interesting to say.

92
00:10:06,820 --> 00:10:08,820
See what Bhikkhu Bodhi says?

93
00:10:08,820 --> 00:10:10,820
No, he doesn't say anything.

94
00:10:10,820 --> 00:10:13,820
Shall not treat you over here.

95
00:10:13,820 --> 00:10:18,820
Okay, the potter treats raw damn clay in the way he treats the baked potts.

96
00:10:18,820 --> 00:10:21,820
So after advising once, I shall not be silent.

97
00:10:21,820 --> 00:10:24,820
I shall advise and instruct and repeatedly applauding.

98
00:10:24,820 --> 00:10:27,820
Just as the potter tests the baked pots,

99
00:10:27,820 --> 00:10:30,820
puts aside those that are cracked, split, or faulty,

100
00:10:30,820 --> 00:10:33,820
and keeps only those that pass the test,

101
00:10:33,820 --> 00:10:36,820
so shall advise.

102
00:10:36,820 --> 00:10:41,820
And instruct, that's not what I read.

103
00:10:41,820 --> 00:10:47,820
No, he's avoiding the problem, I think.

104
00:10:47,820 --> 00:10:55,820
Just talking about baked pots, but what about damn clay?

105
00:10:55,820 --> 00:10:57,820
Not the study anymore.

106
00:10:57,820 --> 00:11:00,820
Anyway, it's interesting, similarly.

107
00:11:00,820 --> 00:11:04,820
The suthe is about emptiness.

108
00:11:04,820 --> 00:11:06,820
It's interesting.

109
00:11:06,820 --> 00:11:08,820
I've just been writing about emptiness.

110
00:11:08,820 --> 00:11:11,820
I almost finished my...

111
00:11:11,820 --> 00:11:17,820
Not actually all that long essay on the notice suthe.

112
00:11:17,820 --> 00:11:22,820
So part of it is about how emptiness isn't really that big of a part of the suthe.

113
00:11:22,820 --> 00:11:24,820
Suthe try and less yet.

114
00:11:24,820 --> 00:11:30,820
Do some wrangling and finessing and reinterpreting.

115
00:11:30,820 --> 00:11:36,820
Maybe that's neither here nor there.

116
00:11:36,820 --> 00:11:39,820
So what lesson do we take from this suthe?

117
00:11:39,820 --> 00:11:44,820
So this quote?

118
00:11:44,820 --> 00:11:46,820
And if you want to respect your teacher,

119
00:11:46,820 --> 00:11:49,820
you should listen and do what they say.

120
00:11:49,820 --> 00:11:51,820
And if you find that when you listen and do what they say,

121
00:11:51,820 --> 00:11:53,820
it's not working.

122
00:11:53,820 --> 00:11:55,820
No, you can ask questions.

123
00:11:55,820 --> 00:11:58,820
It's good to ask and get clarification,

124
00:11:58,820 --> 00:12:02,820
but you just feel like this teacher doesn't know what they're teaching,

125
00:12:02,820 --> 00:12:05,820
and this teaching is not useful to me.

126
00:12:05,820 --> 00:12:07,820
And just swing the stop.

127
00:12:07,820 --> 00:12:10,820
Don't waste your time, don't waste their time.

128
00:12:10,820 --> 00:12:12,820
But if you're going to do it,

129
00:12:12,820 --> 00:12:16,820
if you're going to follow a teacher's teaching,

130
00:12:16,820 --> 00:12:19,820
this is a good quote to keep in mind.

131
00:12:19,820 --> 00:12:23,820
I mean, really our teacher is the Buddha.

132
00:12:23,820 --> 00:12:26,820
So you should think in terms of the Buddha.

133
00:12:26,820 --> 00:12:29,820
You should prepare yourself for the Buddha's teaching,

134
00:12:29,820 --> 00:12:34,820
and you should try your best to respect the Buddha's teaching.

135
00:12:34,820 --> 00:12:37,820
You know, in Buddhist countries,

136
00:12:37,820 --> 00:12:40,820
they won't even put a dumb book on the floor.

137
00:12:40,820 --> 00:12:46,820
They will keep the books up high as a measure of their respect for it.

138
00:12:46,820 --> 00:12:48,820
But it's meant by this quote.

139
00:12:48,820 --> 00:12:50,820
I mean, that's fine and good.

140
00:12:50,820 --> 00:12:53,820
It's good to pay these, you know,

141
00:12:53,820 --> 00:12:56,820
to have in your mind a sense of respect.

142
00:12:56,820 --> 00:13:03,820
But the real importance is that you take the practice seriously.

143
00:13:03,820 --> 00:13:06,820
Keep it in mind,

144
00:13:06,820 --> 00:13:09,820
and you don't, you don't discard it.

145
00:13:09,820 --> 00:13:11,820
You don't think lightly of it.

146
00:13:11,820 --> 00:13:13,820
You don't read it, and then say,

147
00:13:13,820 --> 00:13:14,820
oh, well, I know all that.

148
00:13:14,820 --> 00:13:17,820
Oh, already what else is there.

149
00:13:17,820 --> 00:13:20,820
It's not about learning new teachings,

150
00:13:20,820 --> 00:13:22,820
it's about practicing,

151
00:13:22,820 --> 00:13:25,820
learning how to practice properly,

152
00:13:25,820 --> 00:13:26,820
how to live our lives,

153
00:13:26,820 --> 00:13:29,820
how to deport ourselves,

154
00:13:29,820 --> 00:13:34,820
how to progress,

155
00:13:34,820 --> 00:13:35,820
move forward,

156
00:13:35,820 --> 00:13:39,820
go forward in a way that improves our lives,

157
00:13:39,820 --> 00:13:41,820
that improves our minds,

158
00:13:41,820 --> 00:13:46,820
makes us better people, makes us more pure,

159
00:13:46,820 --> 00:13:49,820
more wise, you know, that teaches us,

160
00:13:49,820 --> 00:13:52,820
teaches us patience, teaches us

161
00:13:52,820 --> 00:13:55,820
about our minds, you know,

162
00:13:55,820 --> 00:13:58,820
teaches us the limits of our capabilities

163
00:13:58,820 --> 00:14:00,820
in terms of control,

164
00:14:00,820 --> 00:14:03,820
and in terms of change,

165
00:14:03,820 --> 00:14:05,820
so that we stop trying to force everything

166
00:14:05,820 --> 00:14:08,820
or control everything,

167
00:14:08,820 --> 00:14:11,820
we stop trying to fix everything,

168
00:14:11,820 --> 00:14:13,820
and we start to learn to let go

169
00:14:13,820 --> 00:14:19,820
and let things naturally go their way,

170
00:14:19,820 --> 00:14:24,820
take them out of what we understand to be me in mind.

171
00:14:24,820 --> 00:14:29,820
Anyway, it's an interesting quote.

172
00:14:29,820 --> 00:14:32,820
Yes, so to remember,

173
00:14:32,820 --> 00:14:35,820
love isn't about, love for a teacher isn't about,

174
00:14:35,820 --> 00:14:38,820
worshiping them or praising them,

175
00:14:38,820 --> 00:14:41,820
not following their teachings,

176
00:14:41,820 --> 00:14:43,820
not discarding it.

177
00:14:47,820 --> 00:14:50,820
So,

178
00:14:50,820 --> 00:14:53,820
if anybody has any questions,

179
00:14:53,820 --> 00:14:56,820
I can stick around for a bit.

180
00:14:56,820 --> 00:14:58,820
Otherwise,

181
00:14:58,820 --> 00:15:02,820
so questions you have to come on the hangout.

182
00:15:04,820 --> 00:15:07,820
I'm just supposed to link to the hangout.

183
00:15:07,820 --> 00:15:20,820
Tomorrow, five-minute meditation lessons.

184
00:15:20,820 --> 00:15:25,820
Oh, five-minute meditation lessons.

185
00:15:25,820 --> 00:15:27,820
Auntie?

186
00:15:27,820 --> 00:15:29,820
Look, you have to turn off the YouTube video

187
00:15:29,820 --> 00:15:30,820
if you come on the hangout.

188
00:15:30,820 --> 00:15:31,820
That's another thing,

189
00:15:31,820 --> 00:15:32,820
or whatever you're listening to,

190
00:15:32,820 --> 00:15:34,820
because otherwise it echoes.

191
00:15:34,820 --> 00:15:35,820
Oh, I got it.

192
00:15:35,820 --> 00:15:38,820
Can you hear me now?

193
00:15:38,820 --> 00:15:40,820
I can hear you, but I think you're echoing.

194
00:15:40,820 --> 00:15:41,820
Oh, okay.

195
00:15:41,820 --> 00:15:43,820
Okay, we're better now.

196
00:15:47,820 --> 00:15:48,820
Hello.

197
00:15:48,820 --> 00:15:49,820
Yeah.

198
00:15:49,820 --> 00:15:51,820
Can you hear me?

199
00:15:51,820 --> 00:15:52,820
Okay.

200
00:15:52,820 --> 00:15:53,820
Yes.

201
00:15:53,820 --> 00:15:54,820
Oh, sorry.

202
00:15:54,820 --> 00:15:55,820
So, okay.

203
00:15:55,820 --> 00:15:59,820
I want to ask you,

204
00:15:59,820 --> 00:16:01,820
how,

205
00:16:01,820 --> 00:16:03,820
okay, I'm just going to phrase it.

206
00:16:03,820 --> 00:16:08,820
How relevant is the role of the teacher

207
00:16:08,820 --> 00:16:11,820
in one's spiritual progress

208
00:16:11,820 --> 00:16:16,820
and can the student get so-called attached

209
00:16:16,820 --> 00:16:17,820
to having a teacher,

210
00:16:17,820 --> 00:16:20,820
and I can't do it on their own?

211
00:16:20,820 --> 00:16:22,820
Well, when we talk about teacher,

212
00:16:22,820 --> 00:16:25,820
there are two types or two ways of understanding that

213
00:16:25,820 --> 00:16:27,820
where most of it understand that, like,

214
00:16:27,820 --> 00:16:30,820
I'm your teacher, me this month, right?

215
00:16:30,820 --> 00:16:33,820
But we don't really,

216
00:16:33,820 --> 00:16:35,820
you know, that's only a provisional sense

217
00:16:35,820 --> 00:16:36,820
of the word.

218
00:16:36,820 --> 00:16:38,820
So it's important to understand first of all

219
00:16:38,820 --> 00:16:41,820
that the Buddhas are a teacher.

220
00:16:41,820 --> 00:16:44,820
And so when you ask how important is a teacher,

221
00:16:44,820 --> 00:16:46,820
you have to be able to separate those two

222
00:16:46,820 --> 00:16:50,820
because I'm not teaching you by teaching

223
00:16:50,820 --> 00:16:52,820
and teaching you the Buddha's teaching

224
00:16:52,820 --> 00:16:54,820
because my understanding of it is,

225
00:16:54,820 --> 00:16:56,820
of course, limited.

226
00:16:56,820 --> 00:16:58,820
Whatever I've realized for myself,

227
00:16:58,820 --> 00:16:59,820
that's only a portion of it.

228
00:16:59,820 --> 00:17:02,820
So rather than just trying to teach what I've learned,

229
00:17:02,820 --> 00:17:05,820
I broaden it and teach it with the Buddha taught.

230
00:17:05,820 --> 00:17:10,820
So why that's important to clarify is because,

231
00:17:10,820 --> 00:17:14,820
in that sense, a teacher is essential,

232
00:17:14,820 --> 00:17:17,820
the knowledge, and I don't mean me in that.

233
00:17:17,820 --> 00:17:19,820
Having the Buddha is essential

234
00:17:19,820 --> 00:17:21,820
because if you're talking in general

235
00:17:21,820 --> 00:17:22,820
on the spiritual practice,

236
00:17:22,820 --> 00:17:25,820
well, first of all, you're not only the teacher

237
00:17:25,820 --> 00:17:28,820
but you need a teacher whose teaching is

238
00:17:28,820 --> 00:17:31,820
true and beneficial.

239
00:17:31,820 --> 00:17:35,820
And that is what we claim we have in Buddhism.

240
00:17:35,820 --> 00:17:40,820
Now, as far as having somebody there,

241
00:17:40,820 --> 00:17:42,820
like me,

242
00:17:42,820 --> 00:17:44,820
then if you turn to me as a teacher,

243
00:17:44,820 --> 00:17:47,820
it's a bit different.

244
00:17:47,820 --> 00:17:51,820
And so the answer is different because you don't need me

245
00:17:51,820 --> 00:17:54,820
if you have both,

246
00:17:54,820 --> 00:17:56,820
well, to some extent you don't need me

247
00:17:56,820 --> 00:17:59,820
because you have the Buddha's teaching.

248
00:17:59,820 --> 00:18:01,820
You can just go and read the books or whatever.

249
00:18:01,820 --> 00:18:05,820
So the Buddha's teaching is accessible on your own.

250
00:18:05,820 --> 00:18:07,820
And in that sense, you don't need a teacher

251
00:18:07,820 --> 00:18:09,820
because you just go find the teachings

252
00:18:09,820 --> 00:18:11,820
from the real teacher, right?

253
00:18:11,820 --> 00:18:13,820
Which we have in books and whatever.

254
00:18:13,820 --> 00:18:16,820
But there are other reasons to have a teacher,

255
00:18:16,820 --> 00:18:18,820
like me, the small teacher,

256
00:18:18,820 --> 00:18:22,820
because they push you.

257
00:18:22,820 --> 00:18:26,820
And well, two things, they push you and they're able

258
00:18:26,820 --> 00:18:28,820
to clarify the teachings.

259
00:18:28,820 --> 00:18:30,820
Yeah, I was going to actually say that.

260
00:18:30,820 --> 00:18:33,820
I was going to say, you're a cat.

261
00:18:33,820 --> 00:18:34,820
I feel accountable.

262
00:18:34,820 --> 00:18:37,820
Someone used to just probably feel accountable to you.

263
00:18:37,820 --> 00:18:40,820
Yeah, that's the reason to have a,

264
00:18:40,820 --> 00:18:43,820
and it's so different practicing on your own

265
00:18:43,820 --> 00:18:45,820
and having someone that you're accountable to.

266
00:18:45,820 --> 00:18:48,820
But also someone who can explain the teaching

267
00:18:48,820 --> 00:18:50,820
and also apply it to you

268
00:18:50,820 --> 00:18:53,820
and be able to, as I said, criticize you

269
00:18:53,820 --> 00:18:55,820
and say, don't do that until you do that.

270
00:18:55,820 --> 00:18:57,820
You have to focus more on this

271
00:18:57,820 --> 00:19:00,820
and you have to stop or minimize that.

272
00:19:00,820 --> 00:19:03,820
So that's not essential,

273
00:19:03,820 --> 00:19:07,820
but it's a huge help.

274
00:19:07,820 --> 00:19:10,820
No, I'd say it's a huge help.

275
00:19:10,820 --> 00:19:11,820
Yeah.

276
00:19:11,820 --> 00:19:13,820
Someone but, yeah.

277
00:19:13,820 --> 00:19:15,820
Okay, thank you.

278
00:19:15,820 --> 00:19:16,820
What was the other question?

279
00:19:16,820 --> 00:19:17,820
Did you have another question?

280
00:19:17,820 --> 00:19:19,820
No, did you read the books?

281
00:19:19,820 --> 00:19:20,820
I read the books.

282
00:19:20,820 --> 00:19:22,820
Sir Front Dave and LA.

283
00:19:22,820 --> 00:19:23,820
Yes.

284
00:19:23,820 --> 00:19:25,820
And did you read the follow up,

285
00:19:25,820 --> 00:19:27,820
Bodey tree grows in LA?

286
00:19:27,820 --> 00:19:28,820
Yes.

287
00:19:30,820 --> 00:19:31,820
Oh, thank you, I've met him.

288
00:19:31,820 --> 00:19:32,820
I went to Sri Lanka.

289
00:19:32,820 --> 00:19:34,820
The first time I went to Sri Lanka

290
00:19:34,820 --> 00:19:37,820
was at his invitation.

291
00:19:37,820 --> 00:19:40,820
And then he stuck me with some guy who ended up

292
00:19:40,820 --> 00:19:43,820
really ruining my reputation

293
00:19:43,820 --> 00:19:45,820
and Sri Lanka for a bit.

294
00:19:45,820 --> 00:19:47,820
Well, ruining my reputation was

295
00:19:47,820 --> 00:19:52,220
fairly important Buddhist people. It's interesting.

296
00:19:52,220 --> 00:19:54,020
Were you a monk at the time or no?

297
00:19:54,020 --> 00:19:57,700
Yeah, that was not so many years ago.

298
00:19:57,700 --> 00:20:01,020
2009 maybe I guess.

299
00:20:01,020 --> 00:20:05,020
Sounds like a, I won't push, but sounds like an intriguing story.

300
00:20:05,020 --> 00:20:07,820
Yeah, it's interesting.

301
00:20:07,820 --> 00:20:11,100
OK, what do you think about, OK, this is kind of random.

302
00:20:11,100 --> 00:20:14,020
Maybe you don't want to answer, but what do you think about,

303
00:20:14,020 --> 00:20:19,580
like monks at write books and stuff, like this party,

304
00:20:19,580 --> 00:20:24,020
and then Dali Lama, and anyone else?

305
00:20:24,020 --> 00:20:26,260
And the nuns, a lot of them do.

306
00:20:26,260 --> 00:20:30,740
I don't really judge someone by whether they write a book or not.

307
00:20:33,740 --> 00:20:37,740
I thought that book had its positives and its negatives,

308
00:20:37,740 --> 00:20:40,100
and I don't really want to get into them,

309
00:20:40,100 --> 00:20:42,300
but it's not what you asked anyway.

310
00:20:42,300 --> 00:20:49,300
But I don't have an opinion on if someone writes a book or not.

311
00:20:49,300 --> 00:20:54,300
OK, thanks.

312
00:20:54,300 --> 00:21:05,300
I mean, obviously, I think the content of the book is going to tell me a lot about the person.

313
00:21:05,300 --> 00:21:10,300
I thought monks can't get into worldly pursuits, though.

314
00:21:10,300 --> 00:21:12,300
Yeah?

315
00:21:12,300 --> 00:21:16,300
So writing a book is kind of a worldly pursuit.

316
00:21:16,300 --> 00:21:17,300
Maybe not.

317
00:21:17,300 --> 00:21:18,300
Maybe not.

318
00:21:18,300 --> 00:21:21,300
I don't know about worldly things.

319
00:21:21,300 --> 00:21:26,300
Like if a monk wrote a romance novel, that's probably one of them.

320
00:21:26,300 --> 00:21:30,300
It's definitely a bad problem.

321
00:21:30,300 --> 00:21:32,300
Don't do that.

322
00:21:32,300 --> 00:21:37,300
But writes, you know, and so saffron days in LA,

323
00:21:37,300 --> 00:21:40,300
those two books are kind of somewhere in the middle.

324
00:21:40,300 --> 00:21:43,300
He's very good at tying everything back to the Buddhist teaching,

325
00:21:43,300 --> 00:21:45,300
but it's also...

326
00:21:45,300 --> 00:21:54,300
No, I mean, it's good in that it relates to every day life.

327
00:21:54,300 --> 00:22:00,300
I mean, I wasn't 100% on the books, but they had good things about them.

328
00:22:00,300 --> 00:22:05,300
He's good at what he does.

329
00:22:05,300 --> 00:22:07,300
I found it to be a good read.

330
00:22:07,300 --> 00:22:12,300
I didn't read the follow-up, though, so I'm looking forward to that.

331
00:22:12,300 --> 00:22:14,300
Yeah, he's in...

332
00:22:14,300 --> 00:22:17,300
He's in...

333
00:22:17,300 --> 00:22:18,300
I can't remember.

334
00:22:18,300 --> 00:22:19,300
He's Beverly Hills.

335
00:22:19,300 --> 00:22:21,300
Hollywood, I can.

336
00:22:21,300 --> 00:22:22,300
In the house.

337
00:22:22,300 --> 00:22:25,300
It's very famous.

338
00:22:25,300 --> 00:22:34,300
Anyway, I guess that is at all.

339
00:22:34,300 --> 00:22:36,300
Are there questions?

340
00:22:36,300 --> 00:22:38,300
You know, that was just kind of the book thing was on my mind,

341
00:22:38,300 --> 00:22:40,300
so that's why I mentioned it.

342
00:22:40,300 --> 00:22:41,300
Okay.

343
00:22:41,300 --> 00:22:44,300
But I guess the proceeds would go to something else.

344
00:22:44,300 --> 00:22:46,300
I'm sorry, I'm asking stupid questions.

345
00:22:46,300 --> 00:22:50,300
I just thought of it as a worldly thing, kind of, but I guess...

346
00:22:50,300 --> 00:22:52,300
Well, it was selling books.

347
00:22:52,300 --> 00:22:55,300
I guess you can argue that selling books is problematic.

348
00:22:55,300 --> 00:22:59,300
I would agree with that, but writing a book, no.

349
00:22:59,300 --> 00:23:01,300
No, because you've written books, too.

350
00:23:01,300 --> 00:23:05,300
Well, I mean, I could be wrong if you want to argue writing books is wrong.

351
00:23:05,300 --> 00:23:08,300
Just because I wrote one doesn't make it right.

352
00:23:08,300 --> 00:23:14,300
But personally, I don't really see the problem with it.

353
00:23:14,300 --> 00:23:17,300
I mean, if you can give a dumbah talk, why can't you write a book about the dumbah?

354
00:23:17,300 --> 00:23:19,300
What's different?

355
00:23:19,300 --> 00:23:23,300
Yeah, that's true.

356
00:23:23,300 --> 00:23:25,300
Okay, thank you.

357
00:23:25,300 --> 00:23:27,300
You're welcome.

358
00:23:27,300 --> 00:23:31,300
I'm going to go have a night. Good night, everyone.

359
00:23:31,300 --> 00:23:58,300
Good night.

