1
00:00:00,000 --> 00:00:08,560
Okay, so the question is, what is the meaning of proper faith in Buddhism?

2
00:00:08,560 --> 00:00:20,640
It's an interesting topic because it's highly polarized, polarizing in Buddhism.

3
00:00:20,640 --> 00:00:41,640
There are certain modern Buddhists, the modern meaning, let's say, western Buddhists in the sense that they've come to Buddhism without any cultural background or any traditional background.

4
00:00:41,640 --> 00:00:47,640
So they've come to it directly through the texts or through a modern teacher.

5
00:00:47,640 --> 00:00:56,640
And in general, there's a trend among such Buddhists to discount the concept of faith.

6
00:00:56,640 --> 00:01:07,640
They will say things like the Buddha taught us to question everything, not believe even him.

7
00:01:07,640 --> 00:01:26,640
And so give the general impression that doubt, skepticism and by extension doubt is useful and faith is useless or even harmful.

8
00:01:26,640 --> 00:01:29,640
Faith is against the Buddhist teaching.

9
00:01:29,640 --> 00:01:57,640
There's another extreme, you have traditional Buddhists who are also highly educated, but coming from another approach who feel that it's actually more through their intense or in intensive study of the Buddhist teaching.

10
00:01:57,640 --> 00:02:12,640
But I'd say you have to admit that it is also partially coming from their faith in the Buddhist teaching that they've had since they were young, most likely in most cases.

11
00:02:12,640 --> 00:02:28,640
Who will say that you need to establish faith first in the Buddhist teaching that faith is essential. If a person doesn't have faith, they cannot progress in the Buddhist teaching.

12
00:02:28,640 --> 00:02:44,640
But to deal with this, let's look at those two extremes and then try to find something that I would say is more in the middle, more moderate, and maybe a little more appropriate.

13
00:02:44,640 --> 00:02:57,640
So these are kind of straw men or straw people, straw arguments that I've set up, but I don't think they're far off the mark of characterizing the opinions of certain Buddhists.

14
00:02:57,640 --> 00:03:16,640
So first of all, by posting the first straw man, it shows the danger of discarding faith because that is a hindrance and faith is a profitable mental faculty.

15
00:03:16,640 --> 00:03:31,640
Now, this is important to understand it, it's important to properly understand because in Buddhism we don't think of things conceptually. There's no, it's not an intellectual argument that faith, whether faith is useful or harmful.

16
00:03:31,640 --> 00:03:46,640
If you say, but faith makes people believe in the wrong things, if you have faith in something that is untrue, it can lead to disaster. That's an intellectual argument. It doesn't say whether faith is profitable or harmful.

17
00:03:46,640 --> 00:03:53,640
Faith is profitable according to Buddhism for the simple reason that it supports the mind.

18
00:03:53,640 --> 00:04:14,640
If all other things considered equal, you're undertaking something, and you put aside whether that thing is right or wrong, the fact that you have faith in it is going to make it easier for you to succeed in that endeavor.

19
00:04:14,640 --> 00:04:25,640
Why? Because faith is a profitable mind state. Faith is something that supports the mind. That's what is meant by profitable in Buddhism. We don't look at the intellectual argument.

20
00:04:25,640 --> 00:04:45,640
It's the same thing about eating meat. Eating meat is comically neutral. It's ethically neutral because it's not an intellectual argument. The point is that during the moment that you're eating meat, there's no reason why you should have to be either a wholesome or unwholesome of intention.

21
00:04:45,640 --> 00:05:01,640
Doubt is considered to be unprofitable because it harms the mind. It scatters the mind. It prevents the mind, hinders the mind. It's a hindrance because it hinders the mind.

22
00:05:01,640 --> 00:05:18,640
If you're undertaking some activity, all other things considered equal, doubt is going to prevent you from succeeding in that activity.

23
00:05:18,640 --> 00:05:35,640
So, it's kind of a narrow definition. Broadly speaking, obviously faith can be dangerous. Obviously doubt can be useful. Doubt is something that intellectually speaking is useful in establishing what is true and good.

24
00:05:35,640 --> 00:05:54,640
But, practically speaking, from a point of view of actual progress, it's the opposite. That faith is actually useful and shouldn't be discounted.

25
00:05:54,640 --> 00:06:09,640
The reason why faith is so dangerous is because it's so powerful. If you have faith in the wrong thing, it can destroy you. It can destroy you to destroy other and harm other people greatly.

26
00:06:09,640 --> 00:06:24,640
But, on the other hand, if you have faith in the right thing, the power of it. So, it's like a power tool. It's something that is quite powerful. Doubt, on the other hand, is something that is harmful. But, in that sense, if you have faith in the wrong things,

27
00:06:24,640 --> 00:06:41,640
just sowing seeds of doubt can allow you to destroy something that is harmful. So, if you're talking about faith in wrong view in things that are faith in God or the South or so on, sowing seeds of doubt is harmful in a good way.

28
00:06:41,640 --> 00:06:50,640
That's the point here. So, it's important to understand what you're talking about and to be able to differentiate.

29
00:06:50,640 --> 00:07:03,640
Obviously, it is quite clear that the Buddha did not want to have us. Let's look at the other side then before I explain it.

30
00:07:03,640 --> 00:07:19,640
On the other side, if anyone who claims that you need perfect faith in the teachings or that faith in the teachings is necessary before you practice, I would say from my

31
00:07:19,640 --> 00:07:38,640
admittedly partial study of the Buddha's teachings, I haven't studied absolutely everything that the Buddha taught and commentaries and sub commentaries and so on.

32
00:07:38,640 --> 00:07:58,640
But, I have quite a broad-based education in the Buddha's teaching and it's quite clear to me that that is not what he taught. In fact, his teachings on faith, even when he said it's a good thing, are by no means

33
00:07:58,640 --> 00:08:11,640
front and center, or by no means core to the actual practice. I mean, it's right faith isn't in the eightfold noble path. It's not one of the things that the Buddha singled out as necessary.

34
00:08:11,640 --> 00:08:26,640
And, in fact, as we know in the columnists, for example, there are cases where he was critical of faith and pointed out that faith can point it out the dangers of faith, which are obvious.

35
00:08:26,640 --> 00:08:40,640
So, it came to my attention recently because someone was saying, well, this was what they were told. They were told that the first thing you have to study the Buddha's teachings until you gain this perfect faith in them through study.

36
00:08:40,640 --> 00:08:47,640
And the first thing, after thinking about it for a while, it wasn't the first thing, but at one point it came to me.

37
00:08:47,640 --> 00:09:03,640
And it was as I was typing a response to this person's question, and after I typed it out, I read it, what I had replied, and I said, well, that's exactly what I'd said is, there's no reason to believe someone when they tell you you have to believe something.

38
00:09:03,640 --> 00:09:17,640
The person is telling you, you have to believe in the Buddha's teaching, and they're saying believe me, believe what I say, you have to believe what the Buddha said. So, it's actually kind of a form of question begging.

39
00:09:17,640 --> 00:09:29,640
Well, in order to believe what the Buddha said, I have to first accept that believing what someone says completely is a good thing.

40
00:09:29,640 --> 00:09:38,640
So, if I'm the sort of person who doesn't believe what people say without reason, then there's no reason why I would accept what you say.

41
00:09:38,640 --> 00:09:43,640
It's like, why should I believe you when you say I should believe the Buddha? It was basically it.

42
00:09:43,640 --> 00:09:47,640
And I think it forms a kind of question begging, circular reasoning.

43
00:09:47,640 --> 00:09:55,640
I mean, like, why should I believe you in the first place? Let alone believing the Buddha.

44
00:09:55,640 --> 00:10:14,640
I think that's obviously, it's a valid argument. Why should I? Why you say this? What are your reasons for saying I should have faith in the Buddha, for example?

45
00:10:14,640 --> 00:10:19,640
And I think that's really how the Buddha would have us approach this.

46
00:10:19,640 --> 00:10:26,640
Obviously, the Buddha said, right view is important, and it's important from the beginning of the practice.

47
00:10:26,640 --> 00:10:36,640
But the question is whether right view means belief in the things that the Buddha was teaching.

48
00:10:36,640 --> 00:10:42,640
And so, it's not that you shouldn't believe anything or you go into it with an open mind.

49
00:10:42,640 --> 00:10:48,640
It's not that you should come and practice thinking that everything is possible.

50
00:10:48,640 --> 00:10:58,640
There are certain arguments and teachings that have to be presented to a person for their evaluation.

51
00:10:58,640 --> 00:11:03,640
But here is how I would say it has to be approached. It has to be approached proportionately.

52
00:11:03,640 --> 00:11:19,640
A person should have faith proportionately to the reasons presented for having or the evidence, proportion to the evidence.

53
00:11:19,640 --> 00:11:30,640
So, if someone comes up to you and says that there's a God who is going to judge everything you do and send you to hell or heaven based on those based on your deeds.

54
00:11:30,640 --> 00:11:35,640
Well, you should examine your reasons for believing that.

55
00:11:35,640 --> 00:11:47,640
If someone says, hey, if you practice the way I teach, it leads you to complete enlightenment and Nirvana and true freedom from suffering.

56
00:11:47,640 --> 00:11:53,640
And you never have any leads you to not be born again or so on and so on.

57
00:11:53,640 --> 00:11:56,640
Then you have to examine why you should believe that.

58
00:11:56,640 --> 00:11:59,640
Both of these statements are difficult to believe.

59
00:11:59,640 --> 00:12:08,640
And so I would say our appropriate response is to be skeptical towards those statements.

60
00:12:08,640 --> 00:12:10,640
I don't think there's anything wrong with that.

61
00:12:10,640 --> 00:12:18,640
I think someone who is skeptical about the reality of Nirvana, that's reasonable in the beginning.

62
00:12:18,640 --> 00:12:27,640
Because the alternative is to believe something disproportionate to the evidence.

63
00:12:27,640 --> 00:12:40,640
And so this is the other thing that I think needs to be pointed out is the consequences of believing something disproportionate to the evidence.

64
00:12:40,640 --> 00:12:57,640
So as long as our belief is proportionate to the evidence, we will have a sort of a equilibrium in our mind.

65
00:12:57,640 --> 00:13:00,640
There is a certain sense of contentment.

66
00:13:00,640 --> 00:13:06,640
So you'll never have a feeling of conflict internally.

67
00:13:06,640 --> 00:13:19,640
If you believe in something, and this is the curious thing, because obviously if you believe in something like the Supreme Creator God who's going to send you to Heaven or Hell, reality is going to come back and hit you hard.

68
00:13:19,640 --> 00:13:24,640
And you're going to be constantly bombarded by evidence to the contrary.

69
00:13:24,640 --> 00:13:36,640
For example, the widespread suffering in the world or the lack of any sort of evidence, you're going to be constantly bombarded with doubt.

70
00:13:36,640 --> 00:13:40,640
Because it's patently not true, it goes against reality.

71
00:13:40,640 --> 00:13:49,640
But the question is, what if you believe, and this is how this question arose, what if you believe disproportionately in something that is true?

72
00:13:49,640 --> 00:13:54,640
What happens? And what's wrong is what was presented to me.

73
00:13:54,640 --> 00:14:01,640
What's wrong with, as I said, believing completely the Buddha's teaching before you have any evidence to support it.

74
00:14:01,640 --> 00:14:15,640
If all you had was other people's say so, what is wrong with believing 100% in everything that the Buddha said?

75
00:14:15,640 --> 00:14:26,640
Now, more likely or more common is the idea that textual study is enough.

76
00:14:26,640 --> 00:14:28,640
So it's not actually without any evidence.

77
00:14:28,640 --> 00:14:32,640
But the evidence is arguments that are presented by the Buddha.

78
00:14:32,640 --> 00:14:44,640
Through reading the Buddha's teaching and the commentaries and sub-cominaries, Terri's, it is often claimed that that is enough evidence to provide reason to have perfect faith in the Buddha's teaching.

79
00:14:44,640 --> 00:14:47,640
So say that's also patently untrue.

80
00:14:47,640 --> 00:14:52,640
But let's talk about that after. First, let's talk about what if you had no evidence.

81
00:14:52,640 --> 00:14:58,640
But suppose someone told you, okay, you practice insight meditation and you become enlightened.

82
00:14:58,640 --> 00:15:02,640
What would be wrong with having total faith in that?

83
00:15:02,640 --> 00:15:12,640
The first thing that comes to mind is it actually, faith actually acts as an inhibitor to investigation.

84
00:15:12,640 --> 00:15:16,640
Why investigate something if you already know it to be true?

85
00:15:16,640 --> 00:15:21,640
And you find this with Buddhists. Often people read, so let's just lump it in there.

86
00:15:21,640 --> 00:15:26,640
People will do the reading and come up with these reasons to believe.

87
00:15:26,640 --> 00:15:29,640
Or maybe that's not even with reasons to believe.

88
00:15:29,640 --> 00:15:34,640
But they read about these teachings and it sounds good and the Buddha is so good at presenting it.

89
00:15:34,640 --> 00:15:37,640
And so they believe it.

90
00:15:37,640 --> 00:15:49,640
They come to this faith and they feel the rapture and the pleasure of hearing the Buddha's arguments and seeing how he debates and wins debates and so on.

91
00:15:49,640 --> 00:15:55,640
And they just feel how great it is and so they just believe with the Buddha taught.

92
00:15:55,640 --> 00:16:02,640
And you'll find that these people are quite often disinclined to actually practice the teachings.

93
00:16:02,640 --> 00:16:21,640
They feel content in their knowledge. And in fact, you find this with scholars quite often that the more they study, the less inclined they are to, or maybe not the less inclined, but it certainly doesn't make them more inclined.

94
00:16:21,640 --> 00:16:27,640
It certainly doesn't bring them to the practice. They can actually become complacent.

95
00:16:27,640 --> 00:16:47,640
But beyond that, a second problem is that even though suppose you have X and its in line with reality and you have perfect faith in it without any knowledge, the problem is that faith isn't enough to support that.

96
00:16:47,640 --> 00:16:50,640
It isn't enough even to bring you to that realization.

97
00:16:50,640 --> 00:16:58,640
So what you're dealing with is not only the faith in it, but you're dealing with a wrong view that actually contradicts it in the case of Buddhism.

98
00:16:58,640 --> 00:17:07,640
So suppose you have the idea of non-self and you have a person who believes 100% in the truth, or they say they do anyway.

99
00:17:07,640 --> 00:17:13,640
They put their faith in and they suppress any kind of doubt.

100
00:17:13,640 --> 00:17:24,640
The problem is that this doubt is going to act very similar to contradictory evidence.

101
00:17:24,640 --> 00:17:37,640
So even though there is no contradictory evidence, the more you look, or reality supports the idea of non-self, your own doubts and your own wrong views are going to conflict with your faith.

102
00:17:37,640 --> 00:17:50,640
And so you're constantly in a state of upset, of distress because of the conflict.

103
00:17:50,640 --> 00:18:01,640
It doesn't actually work. Faith isn't sufficient to propel a person towards truth.

104
00:18:01,640 --> 00:18:08,640
And in fact, the idea that faith is necessary from the beginning is I don't think supported at all in the text.

105
00:18:08,640 --> 00:18:30,640
Right view should be an understanding that one gains through the practice. So preliminary right view for a person who hasn't yet practiced meditation

106
00:18:30,640 --> 00:18:39,640
is not the view that Nirvana is true or so on. It's right view in a general sense of understanding reality.

107
00:18:39,640 --> 00:18:53,640
It's actually in relation to the building blocks of reality. It has to do with a vision of what is the nature and what is the makeup of experience.

108
00:18:53,640 --> 00:19:03,640
And so what a person should have faith in in the beginning is the basic nature of reality.

109
00:19:03,640 --> 00:19:09,640
And that's only important because otherwise you can't actually approach reality.

110
00:19:09,640 --> 00:19:22,640
So if a person is approaching reality from the point of view of people, places and things, or the point of view of an external three-dimensional reality and they're thinking in terms of concepts and in terms of dimensions.

111
00:19:22,640 --> 00:19:34,640
And so on. They'll never be able to experience their experiences. They'll never be able to observe their experiences objectively and clearly.

112
00:19:34,640 --> 00:19:44,640
Person has to shift their view. And so a preliminary right view that the Buddha talks about is shifting that view to see things in terms of experience.

113
00:19:44,640 --> 00:19:55,640
When you move to be aware of the movement, do not think of it as your foot moving or moving out, moving here, moving there or so on.

114
00:19:55,640 --> 00:20:06,640
It's in terms of the experience of the movement. When the stomach rises, it's the experience of the rising. When you feel pain, it's the experience of the pain and so on.

115
00:20:06,640 --> 00:20:14,640
And there is no need beyond that for faith in Nirvana, faith in this or that or the other thing.

116
00:20:14,640 --> 00:20:31,640
But proportionately speaking, there should be an amount of faith, a certain amount of faith, proportionate to the reasonable nature of the claims.

117
00:20:31,640 --> 00:20:48,640
And at that point, there is also the idea of authority. So a person teaches you meditation and you begin to realize the truth and the more they advise you, the more you understand.

118
00:20:48,640 --> 00:21:00,640
And so you begin to believe the things that they say. If they make claims about what's going to come next and they say, well, if you practice along this path, this happens.

119
00:21:00,640 --> 00:21:10,640
So you have some measure of faith and again, it's not all or none, but you have some measure of faith because they're reliable.

120
00:21:10,640 --> 00:21:15,640
And then it turns out to be true. So you have greater faith in the things that they say.

121
00:21:15,640 --> 00:21:22,640
It doesn't mean that you ever have to have full faith and you shouldn't and you can't because full faith in what someone says isn't enough.

122
00:21:22,640 --> 00:21:32,640
But again, even if it doesn't conflict with reality, it will conflict with your own lack of understanding and realization until you actually realize it for yourself.

123
00:21:32,640 --> 00:21:42,640
That's where there's a difference between what we call bhavana santam, which means faith or confidence based on meditation or actual development.

124
00:21:42,640 --> 00:21:57,640
That kind of faith is unshakable and that's really what the Buddha was talking about when he talked about right faith. It's something that is balanced with wisdom or combined with wisdom because it comes through seeing things as they are.

125
00:21:57,640 --> 00:22:02,640
You don't ever really have to believe things disproportionate to the evidence.

126
00:22:02,640 --> 00:22:14,640
But again, the danger being a disproportionate doubt, which is also quite common. People think it's one or the other. So if you don't believe something, you're skeptical of it.

127
00:22:14,640 --> 00:22:21,640
And so they'll have disproportionate doubt, all constantly doubting the things that they experience.

128
00:22:21,640 --> 00:22:35,640
So I just some thoughts on faith. I certainly don't believe one extreme or the other. I think I've said what I meant to say that faith is useful, beneficial, but dangerous if it's disproportionate.

129
00:22:35,640 --> 00:22:59,640
So

