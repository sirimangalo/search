1
00:00:00,000 --> 00:00:02,480
Hi, welcome back to Ask a Monk.

2
00:00:02,480 --> 00:00:07,640
Next question comes from Phyllis Sofia 312.

3
00:00:07,640 --> 00:00:15,440
Is it possible to locate a person in their next life?

4
00:00:15,440 --> 00:00:17,640
Very difficult.

5
00:00:17,640 --> 00:00:24,600
Yeah, technically, possible.

6
00:00:24,600 --> 00:00:31,880
But I would say, you know, if this is in the realm of very strong meditators or people

7
00:00:31,880 --> 00:00:37,080
who have special abilities from past lives, you know, there are people that they're claiming

8
00:00:37,080 --> 00:00:43,960
to be psychics able to communicate with the dead and, you know, obviously most of them are

9
00:00:43,960 --> 00:00:50,920
fakes, but there's the potential that someone is actually able to do this, even more so

10
00:00:50,920 --> 00:00:52,520
if they're practicing meditation.

11
00:00:52,520 --> 00:00:58,440
But you will find people who just have this ability carried over from past lives.

12
00:00:58,440 --> 00:01:04,480
I wouldn't rely on that too much because obviously most people are fakes.

13
00:01:04,480 --> 00:01:05,480
Can you do it?

14
00:01:05,480 --> 00:01:10,840
I mean, obviously if you turn, if you practice meditation for a while, develop yourself

15
00:01:10,840 --> 00:01:12,920
it's certainly possible.

16
00:01:12,920 --> 00:01:17,800
I'd like to propose some alternatives, though.

17
00:01:17,800 --> 00:01:24,360
First of all, because it's quite likely that you won't be able to contact the person and

18
00:01:24,360 --> 00:01:29,080
that you might also delude yourself into thinking that somehow you have made contact when

19
00:01:29,080 --> 00:01:33,440
in fact you haven't.

20
00:01:33,440 --> 00:01:39,680
And also because it's, maybe the question to be asked, you can ask yourself is whether

21
00:01:39,680 --> 00:01:41,640
it's really necessary.

22
00:01:41,640 --> 00:01:50,680
This is a challenge because we cling to people, but this is a part of, I was talking about

23
00:01:50,680 --> 00:01:58,040
in second life, if you watch my latest video in second life, October 10th, or something,

24
00:01:58,040 --> 00:02:05,320
10th, 9th, 10th.

25
00:02:05,320 --> 00:02:11,400
You know, our perception of reality is so narrow and so as a result we cling to things

26
00:02:11,400 --> 00:02:20,720
that can seem quite ridiculous after you start getting into meditation and objective

27
00:02:20,720 --> 00:02:23,240
observation of reality.

28
00:02:23,240 --> 00:02:37,440
You start to realize that humankind, you know, we're a part of a much larger group of beings

29
00:02:37,440 --> 00:02:47,800
and in a sense we're all family and we've all come from the same place and we're all

30
00:02:47,800 --> 00:02:49,520
in the same boat.

31
00:02:49,520 --> 00:02:58,760
So to focus on a single person who's left you is really counterproductive because it's

32
00:02:58,760 --> 00:03:06,120
going to create some sort of stress in your life unnecessarily, to no benefit except to

33
00:03:06,120 --> 00:03:11,560
appease our addiction to that one person.

34
00:03:11,560 --> 00:03:23,000
When in fact happiness and true and lasting happiness and peace is much better found in

35
00:03:23,000 --> 00:03:28,560
the friendship and harmony with the people who are around us.

36
00:03:28,560 --> 00:03:33,600
I mean, ask yourself what's wrong with the people around me, what's wrong with my relationship

37
00:03:33,600 --> 00:03:36,760
with them because actually they are perfect as well.

38
00:03:36,760 --> 00:03:45,360
They are on the same level as whoever you've left behind, they're just, well, not at

39
00:03:45,360 --> 00:03:46,360
the same level.

40
00:03:46,360 --> 00:03:55,600
They're equally important but they're at a different level of development and so try

41
00:03:55,600 --> 00:04:05,080
to align yourself with a certain type of people rather than a specific person, try to find

42
00:04:05,080 --> 00:04:14,360
people who are caring and respond to all people with love and compassion and harmony

43
00:04:14,360 --> 00:04:24,200
with the idea that they are people as well and finding, find a way to be in harmony

44
00:04:24,200 --> 00:04:33,360
with the people around you so that your relationships with them is what's most important

45
00:04:33,360 --> 00:04:41,000
because when it's not then you're removing yourself from reality and what happens then

46
00:04:41,000 --> 00:04:49,800
is generally our relationships with the people around us go to our ruin in favor of a relationship

47
00:04:49,800 --> 00:04:54,560
that we're not having with someone else, we are unable to stand the behavior of other

48
00:04:54,560 --> 00:04:55,560
people.

49
00:04:55,560 --> 00:05:01,320
We don't put any effort into the reality, the real relationships that we're in, the people

50
00:05:01,320 --> 00:05:09,200
who are really there with us and our attention is focused on our sorrow and sadness and

51
00:05:09,200 --> 00:05:16,680
our clinging to someone who's not there and someone who even if we could contact them

52
00:05:16,680 --> 00:05:21,600
is still not with this.

53
00:05:21,600 --> 00:05:26,080
This is one part of it, I think it's very important and quite difficult, this is a challenge

54
00:05:26,080 --> 00:05:31,240
to the way of looking at things so I hope you can accept the challenge and you can

55
00:05:31,240 --> 00:05:36,720
decide for yourself whether I'm right or not, whether this is an important thing or not.

56
00:05:36,720 --> 00:05:44,760
The other part of it is that accepting our limitations because I can say that but it doesn't

57
00:05:44,760 --> 00:05:49,760
make it true that suddenly you forget about that person and you're okay with accepting

58
00:05:49,760 --> 00:05:55,320
everybody, people come and go and you're able that's probably, it's generally a long

59
00:05:55,320 --> 00:06:00,320
way away from where we are when we start meditating.

60
00:06:00,320 --> 00:06:06,280
There are tricks, there are things that we can do to help ourselves help alleviate the

61
00:06:06,280 --> 00:06:13,520
pain during the time of loss or during the time of adaptation to loss and what we're

62
00:06:13,520 --> 00:06:19,440
still clinging and that is better than to find out where the person is, is to think

63
00:06:19,440 --> 00:06:25,320
of the person and to wish them peace, happiness and freedom from suffering because that

64
00:06:25,320 --> 00:06:31,120
is love and that is the most important part of our relationship with them.

65
00:06:31,120 --> 00:06:37,200
When we focus on that, we really, we lose the other part which is causing us suffering

66
00:06:37,200 --> 00:06:41,440
because love doesn't cling, love is not something that needs to think about the person.

67
00:06:41,440 --> 00:06:47,520
The more you love someone, it's not the case that the more you love someone, the more

68
00:06:47,520 --> 00:06:49,680
you need to be near them.

69
00:06:49,680 --> 00:06:53,120
What we're talking about in the case of needing to be near someone is attachment and I've

70
00:06:53,120 --> 00:06:57,200
talked about this before, love is wishing for them to be happy, attachment is wanting them

71
00:06:57,200 --> 00:07:02,520
to make you happy because it makes you happy when they're around and you cling to that.

72
00:07:02,520 --> 00:07:06,480
When we focus on love, wishing for them to be happy, wishing for them to find peace, thinking

73
00:07:06,480 --> 00:07:10,320
about them and the best time to do this is after you meditate once you've finished meditating

74
00:07:10,320 --> 00:07:12,640
before you open your eyes, make a wish.

75
00:07:12,640 --> 00:07:17,920
May I be happy for you from pain and suffering, may all the beings in this house, in this

76
00:07:17,920 --> 00:07:24,880
building, in this city, in this country, may all beings, may all be happy, find peace, happiness

77
00:07:24,880 --> 00:07:28,800
freedom from suffering, but specify that person as well.

78
00:07:28,800 --> 00:07:33,840
Somewhere in there, even at the beginning, may that person be happy, wherever they are

79
00:07:33,840 --> 00:07:38,800
now, may they be happy, may they find peace, may they be free from suffering.

80
00:07:38,800 --> 00:07:46,680
You'll find that's probably the best weapon to fight the craving, besides, of course, meditation

81
00:07:46,680 --> 00:07:56,160
itself and the eventual realization of objectivity and taking things as they come and

82
00:07:56,160 --> 00:08:00,800
realizing that we're all in this together, that that person is on their own now, they're

83
00:08:00,800 --> 00:08:05,560
in a whole different place and from a theoretical point of view, it's because that

84
00:08:05,560 --> 00:08:08,960
their term with us has ended.

85
00:08:08,960 --> 00:08:14,000
Our relationship only went so far, they have other business to take care of, and there

86
00:08:14,000 --> 00:08:16,120
are a whole other group of people.

87
00:08:16,120 --> 00:08:19,240
Maybe we're included in that, maybe they've already been born, and there have already

88
00:08:19,240 --> 00:08:23,200
been conceived, and there are in someone's womb nearby, and we're going to see them again

89
00:08:23,200 --> 00:08:24,200
as a child.

90
00:08:24,200 --> 00:08:28,440
We might not even recognize them, might not even know it's them, they might have been born

91
00:08:28,440 --> 00:08:34,040
as a dog in our house, they might be an angel in our house, they might be somewhere nearby,

92
00:08:34,040 --> 00:08:41,080
and they might be a part of our reality, but they've changed and they're in a new mode,

93
00:08:41,080 --> 00:08:44,040
and that's fine, we have to move on as well.

94
00:08:44,040 --> 00:08:52,360
Our story, our play, our life, is moving, is impermanent, and so I'm being able to deal

95
00:08:52,360 --> 00:09:02,640
with this and remove ourselves from this idea, false idea of the way things should be,

96
00:09:02,640 --> 00:09:08,040
to an understanding and the ability to adapt to the way things actually are.

97
00:09:08,040 --> 00:09:12,600
That will bring us real and true peace happiness and freedom from suffering, so I hope

98
00:09:12,600 --> 00:09:39,720
that helps, thanks for the question and keep practicing.

