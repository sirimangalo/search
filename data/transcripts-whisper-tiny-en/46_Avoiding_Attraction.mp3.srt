1
00:00:00,000 --> 00:00:03,120
Avoiding attraction.

2
00:00:03,120 --> 00:00:08,880
When I see and interact with a specific female, my mind scatters away while I am trying to stay mindful,

3
00:00:08,880 --> 00:00:14,080
would it be okay to disregard what I see and only meditate on what I hear when talking to her,

4
00:00:14,080 --> 00:00:16,720
or would that be cheating and not facing the problem?

5
00:00:19,040 --> 00:00:23,280
Yes, sometimes you will want to just focus on hearing instead.

6
00:00:23,280 --> 00:00:29,200
One of the parts of the comprehensive practice taught in the Sabasabasuta is sambara,

7
00:00:29,200 --> 00:00:34,240
which means the guarding of one's faculties, a very important part of one's practice.

8
00:00:35,440 --> 00:00:40,960
The Sabasabasuta says that in order to support your practice of seeing things as they are,

9
00:00:40,960 --> 00:00:45,600
you have to guard your senses. Of course, seeing does not mean looking with the eyes,

10
00:00:45,600 --> 00:00:49,360
rather it means directly understanding all experiences as they are.

11
00:00:50,320 --> 00:00:55,440
When you are unable to see certain experiences as they are, and they cause immediate reaction,

12
00:00:55,440 --> 00:01:00,080
this can be a great hindrance in the practice, so you should always guard your senses,

13
00:01:00,080 --> 00:01:05,120
letting in only those things that you are now able to deal with, one by one, in order to observe them

14
00:01:05,120 --> 00:01:11,120
clearly as they are. With the example of beauty, it is not reasonable to expect that someone

15
00:01:11,120 --> 00:01:15,360
should overcome their attachment to beauty while being surrounded by many beautiful things.

16
00:01:15,360 --> 00:01:19,680
They are much more likely to just give up the practice as they will be overwhelmed by desire.

17
00:01:20,560 --> 00:01:24,000
You have to be able to guard your senses, at least until you get to the point

18
00:01:24,000 --> 00:01:30,400
that you are able to deal with every situation mindfully. The monk Ananda asked the Buddha,

19
00:01:30,400 --> 00:01:35,600
what should we do in regards to women? And the Buddha said, the best thing is not see them.

20
00:01:36,240 --> 00:01:41,600
Ananda then asked, well, what should we do if we have to see women? And the Buddha said,

21
00:01:41,600 --> 00:01:47,920
well, then do not talk to them. Then Ananda asked, well, what if we have to talk to them?

22
00:01:47,920 --> 00:01:54,560
The Buddha said, then be very, very mindful. Some people might take the teaching on guarding the

23
00:01:54,560 --> 00:02:00,480
senses the wrong way and think that it teaches you to ignore experiences and not investigate them.

24
00:02:00,480 --> 00:02:06,080
In truth, it simply takes into account the practical reality, the impossibility of coming to see

25
00:02:06,080 --> 00:02:10,160
things as they are and letting things be as they are when you are surrounded by things that

26
00:02:10,160 --> 00:02:17,360
immediately and violently trigger partiality of any sort. In order to begin to understand the things

27
00:02:17,360 --> 00:02:22,080
that trigger partiality, first, you have to remove yourself from them. You have to begin from

28
00:02:22,080 --> 00:02:27,200
an empty slate and then face them one by one to the extent that you are able to observe them

29
00:02:27,200 --> 00:02:32,160
without being carried away. You have to think like a scientist and appreciate how much more

30
00:02:32,160 --> 00:02:38,480
difficult it is to understand reality than to just live it normally. In order to develop understanding,

31
00:02:38,480 --> 00:02:44,000
we have to observe experiences one by one. We cannot let everything in and expect to be able to

32
00:02:44,000 --> 00:02:51,200
understand it all clearly as it is. This sounds like that is the sort of thing you are trying to

33
00:02:51,200 --> 00:02:56,160
grasp. The best thing to do from a practical standpoint is to run away from this woman and not

34
00:02:56,160 --> 00:03:01,600
have any contact with her. That's most likely quite difficult. So if you do not run away and break

35
00:03:01,600 --> 00:03:07,200
contact with her, you should use your situation as a laboratory. Do not go out of your way to see her,

36
00:03:07,200 --> 00:03:12,960
nor go out of your way to avoid her. Try to see her in moderation and slowly develop mindfulness

37
00:03:12,960 --> 00:03:18,880
based on the experiences you have while around her. Once you have set it in your mind that

38
00:03:18,880 --> 00:03:24,240
this is a meditation object for me, then eventually with practice, every time you see the object

39
00:03:24,240 --> 00:03:30,640
of your desire, you will be able to be mindful. When you sit down on a meditation mat, immediately,

40
00:03:30,640 --> 00:03:36,160
your mind says, now I am meditating and you are on a meditation mat and your legs and hands are

41
00:03:36,160 --> 00:03:41,520
in a special posture. You are sitting in a special way, so you think this is meditation. On the

42
00:03:41,520 --> 00:03:46,800
other hand, when we do something like eating, we think this is eating and so we lose our mindfulness

43
00:03:46,800 --> 00:03:52,480
because we don't think of it as meditation. If we thought of eating as a meditation practice,

44
00:03:52,480 --> 00:03:57,520
we would eat the whole meal mindfully. In some Zen Buddhist centers, they practice eating in a

45
00:03:57,520 --> 00:04:03,120
very specific manner and as a result, they can eat very mindfully. So you can take your relationship

46
00:04:03,120 --> 00:04:08,800
with this woman as being a meditation object and every time you see her, it is a meditation

47
00:04:08,800 --> 00:04:14,480
session. The other thing I would recommend is to not be too hard on yourself. You will fail

48
00:04:14,480 --> 00:04:19,120
sometimes and you will fall into liking things and people and you may wind up being totally blown

49
00:04:19,120 --> 00:04:24,400
away and unmindful, but that is just another experience for you to learn from. If you react to

50
00:04:24,400 --> 00:04:30,000
your failures in a negative way, you will learn nothing from them. There is the saying we learn

51
00:04:30,000 --> 00:04:35,200
from our mistakes, but we do not always learn from our mistakes. We learn from our mistakes when we

52
00:04:35,200 --> 00:04:39,520
actually have the presence of mind to understand what went wrong and accept that we have made

53
00:04:39,520 --> 00:04:45,280
a mistake objectively without reaction. We do not learn anything from mistakes if we get angry

54
00:04:45,280 --> 00:04:49,920
and upset about them. At the time when you like this woman and are attracted to her beauty,

55
00:04:49,920 --> 00:04:55,120
you should not be saying to yourself, oh, I am such an evil nasty person. This is sinful and bad.

56
00:04:55,840 --> 00:05:01,040
It is important to accept when you make mistakes and to see the experience of making mistakes

57
00:05:01,040 --> 00:05:07,120
as it is. Preparing yourself is one thing but you must acknowledge when you do feel desire.

58
00:05:07,120 --> 00:05:13,600
You cannot stop yourself from experiencing it. Western society is so caught up with the sin of

59
00:05:13,600 --> 00:05:19,280
sexuality and things like masturbation. Masturbation, for example, is an issue that people feel

60
00:05:19,280 --> 00:05:23,840
utterly wretched about. They feel terribly guilty as though they have committed some cardinal sin

61
00:05:23,840 --> 00:05:28,720
by doing something that they actually like doing. Deep down, their heart says this is good,

62
00:05:28,720 --> 00:05:34,240
this is pleasant. So how can you lie to yourself in that way? If you still enjoy it, how can you say

63
00:05:34,240 --> 00:05:40,240
it is bad? This is what has led many Hindu gurus to lead their Western students in the other direction.

64
00:05:40,240 --> 00:05:44,960
There was even a Zen book that talked about a teacher who taught people to find liberation

65
00:05:44,960 --> 00:05:51,440
through sexuality. Some people even teach tantric Buddhism. There are Hindu gurus who believe that

66
00:05:51,440 --> 00:05:56,400
Westerners are so sexually repressed that they put them together in a room and tell them to have

67
00:05:56,400 --> 00:06:03,520
sexual orgies. We do not do this because it is reacting. It is reacting to things as being positive.

68
00:06:03,520 --> 00:06:08,480
Reacting to things as being negative is no good, but reacting to things as being positive

69
00:06:08,480 --> 00:06:14,080
is also not good, as it also leads to the cultivation of habitual tendencies that will create

70
00:06:14,080 --> 00:06:20,640
further expectation, clinging, craving, and disappointment, depression, sadness, and dissatisfaction

71
00:06:20,640 --> 00:06:26,720
when you are not able to obtain the objects of your desire. What we practice is called the middle way,

72
00:06:26,720 --> 00:06:32,320
neither torturing ourselves nor indulging in central pleasure. Once you have experienced

73
00:06:32,320 --> 00:06:38,000
lust and desire, you have experienced it. You cannot change that in retrospect, and yet that is what

74
00:06:38,000 --> 00:06:42,800
people often try to do. Get upset about it and then get the idea that they are repressing it.

75
00:06:43,440 --> 00:06:48,000
You cannot actually repress feelings. You just react to them negatively, which prevents

76
00:06:48,000 --> 00:06:53,040
completely any learning that might have come from the experience. At the same time, when you are

77
00:06:53,040 --> 00:06:57,440
attracted to someone, it can be the case where she is married and you feel guilty because she is

78
00:06:57,440 --> 00:07:02,080
untouchable or something. It could be that you are married or you are in a relationship with someone

79
00:07:02,080 --> 00:07:08,560
else, but you have to accept that you have these feelings. The Buddha said, there is no fire like

80
00:07:08,560 --> 00:07:15,760
lust. You cannot say to a great raging fire. Let it only stay here. Similarly, you cannot say,

81
00:07:15,760 --> 00:07:20,640
let me only have lust for my wife or my girlfriend and not have lust for this person or that person.

82
00:07:21,280 --> 00:07:26,480
Where there is the potential for lust to arise, it will arise and it is uncontrollable.

83
00:07:27,120 --> 00:07:32,320
Lust and desire in general are very dangerous. They are habit-forming and lead only to greater

84
00:07:32,320 --> 00:07:37,680
and greater emotions of the same type, which is uncontrollable and cannot be limited in any way,

85
00:07:37,680 --> 00:07:43,520
shape or form to one specific set of experiences. Lust moves from object to object,

86
00:07:43,520 --> 00:07:48,480
just like how fire burns from the grass to the bushes to the trees. Whatever is combustible

87
00:07:48,480 --> 00:07:53,120
will trigger fire. This is why men cheat on their wives and women cheat on their husbands.

88
00:07:53,120 --> 00:07:58,560
It is why we steal and manipulate others, even why we go to war. It is something that you cannot

89
00:07:58,560 --> 00:08:03,840
contain. You have to accept that fact and work to undo the habits. That is really the only way

90
00:08:03,840 --> 00:08:09,520
to deal with this. Start by limiting your interaction with this person to what is natural.

91
00:08:09,520 --> 00:08:13,840
When you know that she is there, you might not want to look in the beginning. You might just

92
00:08:13,840 --> 00:08:18,960
avert your eyes when you are not talking to her. Just be mindful of the yearning and agony of

93
00:08:18,960 --> 00:08:25,520
unrequited affection. The yearning and agony that comes from desire is actually an intense amount

94
00:08:25,520 --> 00:08:31,360
of suffering. It can ruin both your physical and mental health. It is quite valid practice to avoid

95
00:08:31,360 --> 00:08:36,720
and limit your contact. If something is dangerous, where you know you just cannot control yourself,

96
00:08:36,720 --> 00:08:42,240
then you should avoid that thing. Mental corruptions that should be removed by avoidance are called

97
00:08:42,240 --> 00:09:12,080
vinodana pātava. Those which are to be removed by guarding are called sambhara pātava.

