1
00:00:00,000 --> 00:00:06,480
and welcome to our live broadcast November 3rd, 2015.

2
00:00:06,480 --> 00:00:10,640
Again, we won't be doing the demo by the tonight, so

3
00:00:10,640 --> 00:00:14,400
tonight is just to say hello, Nate.

4
00:00:14,400 --> 00:00:20,720
For now, it's nice from my perspective to not.

5
00:00:20,720 --> 00:00:26,080
And I'll tap a bit of a break.

6
00:00:26,080 --> 00:00:28,000
You might have a couple questions though,

7
00:00:28,000 --> 00:00:30,840
because from the other news, that's the thing,

8
00:00:30,840 --> 00:00:36,240
is we'll answer questions and say stuff.

9
00:00:36,240 --> 00:00:40,360
There might be some things to talk about.

10
00:00:40,360 --> 00:00:43,240
There was a problem on the website,

11
00:00:43,240 --> 00:00:48,280
on the appointments page, because this whole daylight

12
00:00:48,280 --> 00:00:50,880
savings time that was the culprit.

13
00:00:50,880 --> 00:00:56,960
Well, the real culprit is not having a system to deal with

14
00:00:56,960 --> 00:01:01,680
daylight savings time changes, because we're dealing with

15
00:01:01,680 --> 00:01:04,040
UT, we're talking in UTC time.

16
00:01:04,040 --> 00:01:11,600
So I just hacked it and missed a step, which made, anyway,

17
00:01:11,600 --> 00:01:13,280
it's technical, but it's fixed now.

18
00:01:16,040 --> 00:01:21,600
Last night, I gave a talk to a whole roomful of community

19
00:01:21,600 --> 00:01:24,040
advisor, residents, community advisors, and

20
00:01:24,040 --> 00:01:28,920
cluster. This really neat. Actually, I wasn't really clear.

21
00:01:28,920 --> 00:01:31,200
I hadn't really been paying attention to what the meeting was.

22
00:01:31,200 --> 00:01:35,800
I just thought it was some run-on-of-the-mill conference,

23
00:01:35,800 --> 00:01:44,440
but it turns out this was a huge portion of the residents,

24
00:01:44,440 --> 00:01:52,520
people who work for the university to, like, their second year

25
00:01:52,520 --> 00:01:57,440
or upper students who work for the university to make

26
00:01:57,440 --> 00:02:02,040
sure that the resident's life is run smooth for people.

27
00:02:02,040 --> 00:02:06,520
So taking care of the new students, basically, living in their

28
00:02:06,520 --> 00:02:09,560
residences with them and taking care of any problems they

29
00:02:09,560 --> 00:02:13,200
might have, acting as support, and so on.

30
00:02:13,200 --> 00:02:16,960
So it's really the best group that you could possibly reach

31
00:02:16,960 --> 00:02:20,640
with meditation, because these are people who are,

32
00:02:20,640 --> 00:02:25,160
whose job really is to help others and who are generally the

33
00:02:25,160 --> 00:02:30,960
sort of people who like to help, who like to take care and to

34
00:02:30,960 --> 00:02:35,760
do good things. It tend to be really, really neat people.

35
00:02:35,760 --> 00:02:40,240
So it was really good group. They were really loud,

36
00:02:40,240 --> 00:02:42,560
but I think that's from living on residence, you know, and

37
00:02:42,560 --> 00:02:48,680
having to be the sort of the leader on the residents.

38
00:02:48,680 --> 00:02:52,880
So I told the most story about 16 years ago.

39
00:02:52,880 --> 00:02:55,480
I'm telling a lot of 16-year-old stories, because that's when

40
00:02:55,480 --> 00:03:04,040
I was in the university before I started practicing meditation.

41
00:03:04,040 --> 00:03:07,800
How I ended up looking after one of the community advisors

42
00:03:07,800 --> 00:03:12,520
one night when he was really drunk and his girl had

43
00:03:12,520 --> 00:03:18,640
broken up with him. I was using it as an example of how the

44
00:03:18,640 --> 00:03:22,160
caretakers have to be able to take care of themselves.

45
00:03:22,160 --> 00:03:26,400
And it was really, really, really went well.

46
00:03:26,400 --> 00:03:35,760
They really appreciated it, and I was able to explain it

47
00:03:35,760 --> 00:03:45,280
properly. And that was only a test run within the university,

48
00:03:45,280 --> 00:03:54,960
and there's going to be another beaker version in a week or

49
00:03:54,960 --> 00:04:01,600
two weeks, 11 days on the 14th. And there's these

50
00:04:01,600 --> 00:04:04,160
sort of people are coming from all around either the province

51
00:04:04,160 --> 00:04:10,880
or the country, probably just the province, but I can't remember.

52
00:04:10,880 --> 00:04:15,680
And so we will be presenting to them again.

53
00:04:15,680 --> 00:04:18,880
It's there these two CAs,

54
00:04:18,880 --> 00:04:22,880
I'm McMaster, organized it.

55
00:04:22,880 --> 00:04:31,920
And one of the reasons for Lincoln, Buddhist,

56
00:04:31,920 --> 00:04:35,280
the other one is Latino.

57
00:04:35,280 --> 00:04:41,280
But they're really nice, too, really nice people.

58
00:04:41,280 --> 00:04:46,640
And so we're doing it together.

59
00:04:46,640 --> 00:04:51,440
So that was nice.

60
00:04:57,360 --> 00:05:01,760
I guess if we have questions, let's take questions.

61
00:05:01,760 --> 00:05:04,240
We have some questions.

62
00:05:04,240 --> 00:05:07,360
During walking meditation, should the act of turning at the end of the

63
00:05:07,360 --> 00:05:11,280
path be divided into lifting, turning, lowering?

64
00:05:11,280 --> 00:05:13,040
Thank you, Bunthay.

65
00:05:13,040 --> 00:05:16,800
No, it should be turning.

66
00:05:16,800 --> 00:05:22,560
I mean, as it's written in the book, I guess the question is whether

67
00:05:22,560 --> 00:05:27,040
as you increase the walking step, do you also increase the steps it takes to

68
00:05:27,040 --> 00:05:32,480
turn around? And the answer is no, not generally, no.

69
00:05:32,480 --> 00:05:37,600
Especially because of how unbalanced that would potentially make you

70
00:05:37,600 --> 00:05:38,080
as you turn.

71
00:05:41,840 --> 00:05:44,720
One, they should meditators make an effort to suppress their

72
00:05:44,720 --> 00:05:48,640
sensual desires to some degree, or is it better for one's practice to

73
00:05:48,640 --> 00:05:53,600
simply follow these desires as usual, but try to remain mindful.

74
00:05:53,600 --> 00:05:56,960
Well, the best is not to do either, right?

75
00:05:56,960 --> 00:06:01,200
The ideal in everything is not to repress or to follow,

76
00:06:01,200 --> 00:06:03,520
but to understand and observe.

77
00:06:03,520 --> 00:06:06,960
It actually, technically, it's a kind of suppression, but it's a suppression

78
00:06:06,960 --> 00:06:10,240
through wisdom, suppression through insight.

79
00:06:10,240 --> 00:06:15,440
So you're suppressing them by seeing them, seeing

80
00:06:15,440 --> 00:06:17,920
the objects clearly as they are.

81
00:06:17,920 --> 00:06:21,040
And it's called suppression because it doesn't actually get rid of the

82
00:06:21,040 --> 00:06:24,480
problem. The problem can always come back.

83
00:06:24,480 --> 00:06:29,120
It's only when you attain the banana that the problem was really gone.

84
00:06:29,120 --> 00:06:33,520
But it is creating a new habit.

85
00:06:33,520 --> 00:06:38,720
And so it's not a matter of one another way being

86
00:06:38,720 --> 00:06:41,280
one or the other, which way one or the other.

87
00:06:41,280 --> 00:06:44,480
Neither one of them are really all that good.

88
00:06:44,480 --> 00:06:47,600
So if I were to have to pick, I mean, it really doesn't,

89
00:06:47,600 --> 00:06:51,600
it's really not fair either because the way the addiction system works,

90
00:06:51,600 --> 00:06:54,800
and the way the brain is set up, you can't really oppose

91
00:06:54,800 --> 00:07:00,320
your desires for very long. The yes, the funk, the yes,

92
00:07:00,320 --> 00:07:04,800
impulses apparently much, much stronger than the no impulse.

93
00:07:04,800 --> 00:07:08,160
So you can only say no for so long, and they've got,

94
00:07:08,160 --> 00:07:13,440
I mean, what I'm saying is neuroscientists have observed this

95
00:07:13,440 --> 00:07:16,240
back and can point out, according to the brain,

96
00:07:16,240 --> 00:07:19,760
chemists, the brain makeup, why it is.

97
00:07:19,760 --> 00:07:23,680
The no part is, is, tends to be fairly.

98
00:07:23,680 --> 00:07:27,600
Or it depends on the person, some people have a very strong no faculty,

99
00:07:27,600 --> 00:07:29,920
I think, or something like that.

100
00:07:29,920 --> 00:07:32,480
But in the end, the no faculty can't.

101
00:07:32,480 --> 00:07:35,600
It's unsustainable, as soon as you stop saying no,

102
00:07:35,600 --> 00:07:43,200
then you start saying yes, so it's not a sustainable solution.

103
00:07:43,200 --> 00:07:47,360
But probably, yes, better to say no than to say yes.

104
00:07:47,360 --> 00:07:51,760
I would have to say off the top of my head, I mean, even the Buddha seems to have

105
00:07:51,760 --> 00:07:56,160
appreciated that, that in a pinch, saying no is

106
00:07:56,160 --> 00:07:58,720
better than chasing after your desires.

107
00:07:58,720 --> 00:08:01,920
It's just, it's not a solution, and in the end, it can actually backfire

108
00:08:01,920 --> 00:08:07,440
if you rely on it too much.

109
00:08:14,640 --> 00:08:17,440
And that was it. I'll caught up with questions already.

110
00:08:17,440 --> 00:08:20,880
Oh, I thought some meant lots.

111
00:08:20,880 --> 00:08:24,240
I thought so too, and there was a, there were a lot of comments.

112
00:08:24,240 --> 00:08:30,320
Maybe, maybe time for random, random announcements, like,

113
00:08:30,320 --> 00:08:34,400
have you ever mentioned on, on the broadcast about the Saturday Day of Mindfulness

114
00:08:34,400 --> 00:08:37,040
for anyone who might be in the Hamilton area?

115
00:08:37,040 --> 00:08:41,520
No, we have. I mean, it's actually just a placeholder right now because we

116
00:08:41,520 --> 00:08:43,760
haven't actually done anything with it.

117
00:08:43,760 --> 00:08:47,360
This Saturday, I'm going, again, in the morning,

118
00:08:47,360 --> 00:08:52,960
I'm going to London. I think there might be someone trying to come again.

119
00:08:52,960 --> 00:08:55,200
They're Saturday, right? Yes.

120
00:08:55,200 --> 00:08:57,760
Yeah, there was someone signed up and she was going to bring her daughter

121
00:08:57,760 --> 00:09:01,120
London. You don't mean London, England.

122
00:09:01,120 --> 00:09:03,120
No, we have a London in Ontario.

123
00:09:03,120 --> 00:09:06,320
Okay. It's actually not that far away.

124
00:09:06,320 --> 00:09:08,640
Oh.

125
00:09:09,680 --> 00:09:11,840
Yeah.

126
00:09:12,640 --> 00:09:16,320
But in general, you know, for people that are in the Hamilton area,

127
00:09:16,320 --> 00:09:18,480
you're going to designate the Saturday days as,

128
00:09:23,440 --> 00:09:27,200
try to. Yes. So if you're in the Hamilton area,

129
00:09:27,200 --> 00:09:29,760
you can go to the monastery from, what is it, 9 to 3?

130
00:09:31,600 --> 00:09:33,360
Right, 9 to 4, maybe.

131
00:09:34,160 --> 00:09:36,240
9 to 3, 9 to 4, bring your lunch.

132
00:09:36,240 --> 00:09:44,480
Yeah. I mean, the idea was just to designate the Saturday as a sort of day that

133
00:09:44,480 --> 00:09:48,640
we would encourage people to come out together.

134
00:09:48,640 --> 00:09:51,920
So there would be a sense of meditating in a group.

135
00:09:51,920 --> 00:09:56,080
If you want to actually meet with me, it doesn't have to be on Saturday and

136
00:09:56,080 --> 00:09:59,680
it might not always be possible on Saturday.

137
00:09:59,680 --> 00:10:03,520
I'll be back in the afternoon on Saturday.

138
00:10:03,520 --> 00:10:07,200
And then on the 14th, which is a Saturday as well.

139
00:10:11,040 --> 00:10:13,840
Right. So this Saturday and then next Saturday as well.

140
00:10:14,880 --> 00:10:19,360
Next Saturday, I've been invited to Toronto and then back here for the

141
00:10:20,640 --> 00:10:22,400
CA conference in the afternoon.

142
00:10:23,520 --> 00:10:25,440
So this next Saturday is right out.

143
00:10:26,720 --> 00:10:28,960
So there's a couple of Saturday's booked up.

144
00:10:28,960 --> 00:10:33,360
But eventually, once funding it's sort of all the other Saturday obligations,

145
00:10:33,360 --> 00:10:35,520
there'll be a nice day of mindfulness every Saturday.

146
00:10:37,360 --> 00:10:38,080
That is nice.

147
00:10:42,000 --> 00:10:47,680
Have you thought about having any sort of a children's meditation on those days?

148
00:10:50,160 --> 00:10:52,560
Like where people would bring their children to learn meditation?

149
00:10:54,320 --> 00:10:56,560
Yeah, I mean, we don't have that much of a community yet.

150
00:10:56,560 --> 00:11:01,440
No, but for anyone who's in the area, there's a meetup group,

151
00:11:02,720 --> 00:11:03,840
a website meetup.

152
00:11:03,840 --> 00:11:07,760
So if you just Google meetup, Hamilton, what's it called?

153
00:11:07,760 --> 00:11:09,680
Hamilton meditation group or something?

154
00:11:11,840 --> 00:11:16,240
I mean, if people want to have not really, you know, adding more

155
00:11:18,400 --> 00:11:22,800
more events, we're trying that with the McMaster Buddhism Association.

156
00:11:22,800 --> 00:11:28,720
And if you set up an event, it's a bit of mental work to get it set up and then no one shows up.

157
00:11:28,720 --> 00:11:33,680
So I think for the time being, I'm just going to be a little bit more organic about it

158
00:11:33,680 --> 00:11:34,560
and people show up.

159
00:11:36,160 --> 00:11:37,200
We can work things out.

160
00:11:37,840 --> 00:11:40,800
Yeah, I'm doing quite a bit of scheduled stuff anyway.

161
00:11:41,360 --> 00:11:42,160
Definitely.

162
00:11:42,160 --> 00:11:47,520
This is just a good format just to let people in the Hamilton area know that there is

163
00:11:47,520 --> 00:11:53,440
some on the stereo there now. And then the other thing is the website,

164
00:11:53,440 --> 00:11:55,920
SiriMunglow.org has a nice new layout on it.

165
00:11:55,920 --> 00:11:59,280
If anybody hasn't looked at it for a while, we're trying to,

166
00:11:59,920 --> 00:12:03,840
trying to get that a little more organized to have some updated information.

167
00:12:03,840 --> 00:12:04,240
Right.

168
00:12:04,240 --> 00:12:08,000
I thought we changed the website a while ago and then when you mentioned that I looked and

169
00:12:08,000 --> 00:12:09,760
know it's still got the old layout.

170
00:12:09,760 --> 00:12:13,280
Yeah, quite sure I changed the layout quite some time ago.

171
00:12:13,280 --> 00:12:18,320
No, I've never seen a look anything different than, but I think it looks nice now,

172
00:12:18,320 --> 00:12:20,160
but a lot of pressure of a look.

173
00:12:21,600 --> 00:12:23,680
Yeah, it's the Nirvana theme.

174
00:12:23,680 --> 00:12:24,640
Did you notice?

175
00:12:24,640 --> 00:12:25,520
I didn't know.

176
00:12:26,400 --> 00:12:27,440
That's cool.

177
00:12:29,040 --> 00:12:29,520
Perfect.

178
00:12:30,560 --> 00:12:33,600
We're also using it for the McMaster Buddhism Association.

179
00:12:36,000 --> 00:12:42,560
And I think Anthony had posted something in Slack, the British Columbia Buddhist

180
00:12:42,560 --> 00:12:47,680
Wuhara is using the same theme. So everybody likes the Nirvana theme.

181
00:12:47,680 --> 00:12:52,480
Understandable.

182
00:12:58,640 --> 00:13:00,400
If there's anything the website is missing,

183
00:13:01,280 --> 00:13:06,080
if anyone wants to post a comment, I'm doing some minor editing and

184
00:13:06,080 --> 00:13:18,720
would be happy to add anything in it for missing something blatant.

185
00:13:36,080 --> 00:14:04,640
Okay, well, then it's all for tonight.

186
00:14:04,640 --> 00:14:11,920
Thank you all for tuning in. The appointment schedule is is working now, so if anyone wants to

187
00:14:12,640 --> 00:14:14,160
fill up the last three slots.

188
00:14:15,520 --> 00:14:18,800
So probably just drop out of university and add more slots to this though.

189
00:14:19,920 --> 00:14:20,640
More value.

190
00:14:21,360 --> 00:14:22,000
There you go.

191
00:14:26,080 --> 00:14:29,920
I don't know. There's something about getting my degree that still seems a little bit.

192
00:14:30,560 --> 00:14:32,640
Doesn't really like I would have never thought before.

193
00:14:32,640 --> 00:14:36,640
I heard about other monks getting a degree and I thought, well, what's that for?

194
00:14:37,920 --> 00:14:42,000
But everyone said, you know, being in the West, there's something about having a degree.

195
00:14:43,120 --> 00:14:45,280
So you still think it's a good thing to have?

196
00:14:47,600 --> 00:14:52,240
No, I don't know. I mean, I don't really care. I don't know. I'm not going to feel

197
00:14:53,040 --> 00:14:54,880
pleased or something when I get into grade.

198
00:14:55,440 --> 00:14:56,880
And then my father probably will.

199
00:14:56,880 --> 00:15:00,800
That was one of them since.

200
00:15:01,920 --> 00:15:05,680
Because I did make a promise of an agreement with him many years ago that I never kept.

201
00:15:08,080 --> 00:15:11,280
Oh, that was where he was going to stop hunting and you were going to get into degree?

202
00:15:12,000 --> 00:15:12,240
Yeah.

203
00:15:16,960 --> 00:15:17,520
I don't know.

204
00:15:20,800 --> 00:15:21,760
You have to think about it.

205
00:15:21,760 --> 00:15:26,960
And the next semester I might be able to take evening courses.

206
00:15:26,960 --> 00:15:30,160
Do some evening courses and like a course in Buddhism or something.

207
00:15:30,160 --> 00:15:33,040
That was the sort of the idea that I'd be able to study Buddhism.

208
00:15:33,920 --> 00:15:36,960
It's always nice to have some formal

209
00:15:42,240 --> 00:15:46,000
system. Today I had an interesting conversation with my philosophy,

210
00:15:46,000 --> 00:15:53,760
Kia. He's doing his thesis on pragmatism, color. Color in pragmatism,

211
00:15:53,760 --> 00:15:56,640
much, you know, probably doesn't mean much if unless you're philosopher

212
00:15:58,320 --> 00:16:02,320
didn't to me. But he explained pragmatism and it's very close to Buddhism.

213
00:16:02,320 --> 00:16:05,520
In fact, it's probably heavily influenced by Buddhism.

214
00:16:05,520 --> 00:16:08,400
And he was talking about like this guy, William James,

215
00:16:09,680 --> 00:16:12,400
who, you know, fairly famous guy.

216
00:16:12,400 --> 00:16:15,920
And he was influenced by Buddhism and we're talking about

217
00:16:17,200 --> 00:16:20,240
sort of the readings that we've been doing in our philosophy class.

218
00:16:21,760 --> 00:16:27,440
And how they relate or how they differ from pragmatism.

219
00:16:27,440 --> 00:16:30,560
So apparently Locke and John Locke.

220
00:16:32,800 --> 00:16:36,160
You see a pragmatist? I think in some ways he was a pragmatist.

221
00:16:36,160 --> 00:16:44,000
The pragmatism is just empirical. John Locke was an empiricist.

222
00:16:44,000 --> 00:16:48,080
So he only looked at what you experienced,

223
00:16:50,000 --> 00:16:55,760
what is readily apparent. So there was no sense of substance, things that actually exist.

224
00:16:55,760 --> 00:16:57,920
This is what I'm studying in philosophy, which is quite

225
00:16:59,440 --> 00:17:01,360
interesting or a Buddhist perspective.

226
00:17:01,360 --> 00:17:06,000
The idea of whether entities exist, right?

227
00:17:06,000 --> 00:17:11,920
So they cart and Spinoza, but the other two philosophers were looking at.

228
00:17:11,920 --> 00:17:13,680
They talk about substance.

229
00:17:15,680 --> 00:17:23,360
And Spinoza goes a step further and talks about something called modes and attributes.

230
00:17:23,360 --> 00:17:31,440
And it's all very weird. But then Locke comes along and says,

231
00:17:33,920 --> 00:17:38,240
what meaning is there in substance, which is, you know,

232
00:17:38,240 --> 00:17:39,520
it's a fairly Buddhist thing to say.

233
00:17:40,320 --> 00:17:46,080
Stop thinking of things as entities and more in terms of events.

234
00:17:46,080 --> 00:17:56,960
It's interesting. Did he have any knowledge of Buddhism to your knowledge or did he come

235
00:17:56,960 --> 00:18:01,040
Locke? I think he was a student of Hobbes, which was a materialist.

236
00:18:03,760 --> 00:18:07,040
And there was sort of a tendency, they were moving towards materialism or

237
00:18:09,040 --> 00:18:13,040
and they were very interested in the material world in ways that they hadn't been

238
00:18:13,040 --> 00:18:17,520
like they were trying to figure it out. They started realizing, hey, we can figure this out.

239
00:18:18,160 --> 00:18:25,280
We can actually come to understand the natural world.

240
00:18:26,640 --> 00:18:29,120
Whereas before it was just like, well, it must be an act of God.

241
00:18:30,080 --> 00:18:32,800
God is running things and they said, no, actually, it's not.

242
00:18:33,360 --> 00:18:39,760
It's actually running fairly mechanistic and then it led to Newton and this whole idea of mechanistic

243
00:18:39,760 --> 00:18:48,320
physics, which has now been, has been basically thrown out to some extent by quantum physics,

244
00:18:49,760 --> 00:18:51,920
which is non mechanistic, I think.

245
00:18:55,200 --> 00:18:59,200
The very big problem is it's a lot of thinking and they seem to be doing a lot of thinking.

246
00:19:01,040 --> 00:19:03,920
I think my brain stopped working in that way many years ago.

247
00:19:03,920 --> 00:19:09,120
That's the thing so much. Lots of thinking and philosophy.

248
00:19:14,160 --> 00:19:16,960
I'm learning Latin, which I'll probably never use.

249
00:19:19,040 --> 00:19:21,120
Are you able to keep straight the Latin and the poly?

250
00:19:22,800 --> 00:19:26,800
Yeah, I'm not learning poly, that'd be something if I could take poly first.

251
00:19:28,080 --> 00:19:29,120
I didn't probably be.

252
00:19:29,120 --> 00:19:35,520
See, that's the thing. If I do religious studies, I'll probably be able to use poly in the later

253
00:19:35,520 --> 00:19:42,240
courses. I was just kind of this year was trying to do some easier courses, maybe next semester.

254
00:19:43,200 --> 00:19:50,080
I've taken up for your Buddhism course, but I've done a couple of short papers this semester

255
00:19:50,080 --> 00:19:54,560
and that's kind of been good to get back into trying to write a formal paper.

256
00:19:54,560 --> 00:19:58,640
It's just a little kind of easy.

257
00:20:00,320 --> 00:20:05,680
So I have felt like it's been problematic. It's just kind of

258
00:20:06,560 --> 00:20:09,200
well, there are the things I could be doing, I suppose.

259
00:20:10,000 --> 00:20:11,040
Do you have to take a visit?

260
00:20:12,400 --> 00:20:14,080
No, why? Why would I have to take a visit?

261
00:20:14,080 --> 00:20:20,960
I did in college. They actually required us to have a couple of units of like a gym class,

262
00:20:20,960 --> 00:20:24,320
I remember to skiing and volleyball and swimming.

263
00:20:25,680 --> 00:20:29,040
I don't have such classes in university here, I don't think.

264
00:20:30,960 --> 00:20:34,560
Maybe in kinesiology, I don't know, I don't think so.

265
00:20:35,200 --> 00:20:42,240
I was an extracurricular intramural whatever you call it, you can do it all.

266
00:20:43,360 --> 00:20:48,400
Now, these are something that was required just for all degrees, it's kind of strange, but

267
00:20:48,400 --> 00:20:51,920
it's not a bad thing, they were enjoyable.

268
00:20:56,720 --> 00:20:57,920
There you go.

269
00:20:59,600 --> 00:21:02,720
There's a question, I'm not sure if it's a hypothetical question,

270
00:21:02,720 --> 00:21:08,240
one thing will we ever know the answer to what is one, two, three from the Dhammapanda?

271
00:21:08,240 --> 00:21:10,240
It's funny, I didn't even give you an answer, did I?

272
00:21:10,240 --> 00:21:16,560
Shoot, and I meant to as well, I just must have gotten caught up in something else,

273
00:21:18,400 --> 00:21:23,120
and it's good, it's good to leave you wondering, what is one?

274
00:21:24,240 --> 00:21:27,360
I wonder if there's a translation of it on the internet, there must be some one.

275
00:21:29,360 --> 00:21:32,240
Maybe someone can provide the translation of the

276
00:21:32,240 --> 00:21:41,520
kumara panda, so let's see if I can, I can't remember them all, because we don't really use them.

277
00:21:42,400 --> 00:21:46,240
I know the first one, economic king sub-based at Dahade, Titika,

278
00:21:48,000 --> 00:21:52,880
all being subsist on food, it's one, one is food,

279
00:21:52,880 --> 00:22:03,840
and interesting, again leading back to this idea of food,

280
00:22:05,440 --> 00:22:13,520
food being somehow important and somehow central, but by ahara it's different kinds of food,

281
00:22:13,520 --> 00:22:20,160
so there's the physical food and then there's a mental food basically, four kinds in total.

282
00:22:20,160 --> 00:22:29,920
Two is, I think namancha rupancha, namanchu rupancha, namanchu rupancha, namanchu rupa rupa,

283
00:22:35,920 --> 00:22:44,800
kumara panda, namancha rupancha, dini namma king, tiso vedana, the three, wadana, number three,

284
00:22:44,800 --> 00:22:52,160
jatara namma king, the four aria, what is the, what is four, four aria satcha,

285
00:22:53,280 --> 00:22:58,080
panchanamma king, panjupadana kanda, the five aggregates of clinging,

286
00:22:59,040 --> 00:23:06,240
chat namma king, the six, wad is six, jatajatikani ayatayatayatana,

287
00:23:07,360 --> 00:23:12,320
the six internal sense basis, so seeing hearing spelling tips of your thinking,

288
00:23:12,320 --> 00:23:17,680
satchanamma king, the seven bhodjanga, the seven factors of enlightenment,

289
00:23:18,800 --> 00:23:24,960
atanamma king, aria atang komago, the eight full noble path is eight.

290
00:23:26,960 --> 00:23:32,400
namamma king, namma satcha rasa, what is nine, the nine,

291
00:23:33,120 --> 00:23:38,400
abodes of beings, and I can't remember what those are, but it's basically human and so on,

292
00:23:38,400 --> 00:23:46,880
angel, namma, I can't remember, so fairly, it's a sort of a weird enumeration, but there are nine,

293
00:23:48,640 --> 00:23:57,120
sasa lasa, dasa namma king, the, what is ten, dasa, dasa hunga, dasa hungi, he,

294
00:23:57,120 --> 00:24:12,720
samanagato, araha, dvajati, the ten factors that have accomplished with, or associated with,

295
00:24:12,720 --> 00:24:28,640
what if one goes by, or then one has ten factors, one is an araha.

296
00:24:33,600 --> 00:24:36,320
So here we have the commentary, let's go through and see.

297
00:24:36,320 --> 00:24:47,040
dasa sambhikaveda, mai sussam, namma king, kussu waku dasa.

298
00:24:55,360 --> 00:25:03,600
Oh, I think it's the ten, the eight full number path plus samanayana and sambhayana, we mood tea.

299
00:25:03,600 --> 00:25:07,200
So right knowledge and right deliverance.

300
00:25:09,360 --> 00:25:25,200
sasa lasa, namma, yeah, the nine sata, the sata was a bit strange action.

301
00:25:25,200 --> 00:25:36,880
Yeah, it's the ten basic aspects of Buddhism, fairly core things that the Buddha apparently is

302
00:25:36,880 --> 00:25:56,400
supposed to have taught to rahula, these were for rahula, it's funny, I don't hear about them so often,

303
00:25:56,400 --> 00:26:08,400
I was claiming I had read a version where investigation was the first one and I was completely wrong,

304
00:26:08,400 --> 00:26:14,080
it was sati, as he said, of course, and once I looked it up, I was just remembering it completely wrong.

305
00:26:14,080 --> 00:26:27,520
So thank you for reminding me about that.

306
00:26:29,360 --> 00:26:31,840
Okay, you know, thank you. Have a good night.

307
00:26:31,840 --> 00:26:47,760
Okay, thank you, Bhante. Good night.

