1
00:00:00,000 --> 00:00:12,000
Oops, I forgot something.

2
00:00:12,000 --> 00:00:27,800
Okay, now everyone on broadcasting live daily broadcast just to say hello, we may be

3
00:00:27,800 --> 00:00:34,800
to provide some dhamma.

4
00:00:34,800 --> 00:00:38,800
Do you have any dhamma for us today, Robin?

5
00:00:38,800 --> 00:00:43,600
There's no dhamma, but it is Thanksgiving in the United States, so everyone is asleep after

6
00:00:43,600 --> 00:00:45,800
eating all their turkey, I think.

7
00:00:45,800 --> 00:00:50,800
Had three people, I wish me happy Thanksgiving today, and I don't know if it was the right

8
00:00:50,800 --> 00:00:55,800
thing to say, but I said we don't have Thanksgiving again, or we had it last month.

9
00:00:55,800 --> 00:01:03,800
Certainly, I mean, not that it would mean anything to me anyway, probably, but still

10
00:01:03,800 --> 00:01:11,800
I think it's kind of funny that people are wishing us Thanksgiving.

11
00:01:11,800 --> 00:01:15,800
I was in America, I guess people would, it would be proper, right?

12
00:01:15,800 --> 00:01:20,800
Yeah, Thanksgiving in America is one of those holidays that's not controversial.

13
00:01:20,800 --> 00:01:27,800
If there's so much ridiculous controversy with other holidays, like Donald Trump is all

14
00:01:27,800 --> 00:01:28,800
up in art.

15
00:01:28,800 --> 00:01:30,800
What you don't think Thanksgiving has any controversy associated?

16
00:01:30,800 --> 00:01:33,800
Well, no, okay, let me step back on that.

17
00:01:33,800 --> 00:01:40,800
Of course it does because of the Native American thing, but I was just thinking of the more political

18
00:01:40,800 --> 00:01:47,800
type of things like Donald Trump was up in arms because Starbucks has a plain red cup for their

19
00:01:47,800 --> 00:01:52,800
holiday cup this year, it doesn't say Merry Christmas or Happy Holidays or anything like

20
00:01:52,800 --> 00:01:57,800
that, and he was actually kind of going on and on about it how it was such a terrible thing

21
00:01:57,800 --> 00:02:01,800
and when he's president, everyone's going to say Merry Christmas, so it gets kind of ridiculous

22
00:02:01,800 --> 00:02:06,800
with holidays, but Thanksgiving, other than the whole Native American thing, is a pretty

23
00:02:06,800 --> 00:02:11,800
mild holiday where everybody's celebrating the same thing, more or less.

24
00:02:11,800 --> 00:02:15,800
I imagine the First Nations people would disagree with you on that one.

25
00:02:15,800 --> 00:02:18,800
I think so, too.

26
00:02:18,800 --> 00:02:21,800
I think that's not the very pleasant.

27
00:02:21,800 --> 00:02:24,800
I mean, the idea of Thanksgiving is pretty awesome.

28
00:02:24,800 --> 00:02:28,800
It's just how it arose, I think, or what it's associated with.

29
00:02:28,800 --> 00:02:38,800
It's somehow being thankful for having taken over some of the people's land and really invaded.

30
00:02:38,800 --> 00:02:41,800
It's a celebration of an invasion of that.

31
00:02:41,800 --> 00:02:45,800
I mean, not quite invasion because people here didn't have the idea of land ownership,

32
00:02:45,800 --> 00:02:47,800
but really invasion.

33
00:02:47,800 --> 00:03:03,800
And invasion like an invasive species that just comes in by force and by sheer inertia.

34
00:03:03,800 --> 00:03:10,800
Steam rollers over another culture and people.

35
00:03:10,800 --> 00:03:18,800
I mean, to passively wipe out another species to me, it doesn't seem problematic.

36
00:03:18,800 --> 00:03:30,800
If it's just, oh, they eventually just naturally became the dominant group.

37
00:03:30,800 --> 00:03:32,800
But that's not really how it happened.

38
00:03:32,800 --> 00:03:47,800
There was forced conversion and really theft and treachery, trickery to get their lands and to get them to sign horribly unfair treaties and so on.

39
00:03:47,800 --> 00:03:50,800
It's just really bad stuff.

40
00:03:50,800 --> 00:03:53,800
From what I hear.

41
00:03:53,800 --> 00:04:01,800
Well, in the name of imperialism, the name of the Queen and the name of the King.

42
00:04:01,800 --> 00:04:06,800
And slavery.

43
00:04:06,800 --> 00:04:12,800
A lot of bad stuff.

44
00:04:12,800 --> 00:04:19,800
Human beings can be so much more noble than animal and other animals.

45
00:04:19,800 --> 00:04:25,800
But they can be so much more evil.

46
00:04:25,800 --> 00:04:32,800
They were also convinced that they're, you know, their way was right.

47
00:04:32,800 --> 00:04:38,800
Well, you know, the pilgrims, the settlers, they were still convinced that their way was right.

48
00:04:38,800 --> 00:04:40,800
Well, I don't know.

49
00:04:40,800 --> 00:04:41,800
That's part of it.

50
00:04:41,800 --> 00:04:47,800
Yeah, but I think that's sort of from maybe from the just point of view or our culture point of view.

51
00:04:47,800 --> 00:04:50,800
But there was a lot of greed involved, I think as well.

52
00:04:50,800 --> 00:04:56,800
Like just sheer opportunism, oh, these people are naive in a sense.

53
00:04:56,800 --> 00:05:00,800
Which I guess you could say they were, but it just means that they were simple people.

54
00:05:00,800 --> 00:05:13,800
But weren't worried about land ownership and mineral rights and, you know, things like forest, you know, cutting down trees.

55
00:05:13,800 --> 00:05:18,800
They wouldn't, they didn't rape and pillaged land either.

56
00:05:18,800 --> 00:05:27,800
And so they, when people came and saw these natural resources that were maybe just starting to run short in Europe, I don't know.

57
00:05:27,800 --> 00:05:36,800
But we're harder to get at that old gold and whatever.

58
00:05:36,800 --> 00:05:42,800
Just land, you know.

59
00:05:42,800 --> 00:05:46,800
A lot of greed.

60
00:05:46,800 --> 00:05:52,800
And greed is the biggest reason for war.

61
00:05:52,800 --> 00:05:57,800
So usually the least publicized because everyone tries to excuse it with other reasons.

62
00:05:57,800 --> 00:06:13,800
But I think deep down most wars have been, have been fought out of greed, this is sheer greed.

63
00:06:13,800 --> 00:06:17,800
So on that pleasant note.

64
00:06:17,800 --> 00:06:21,800
So I guess Thanksgiving is a little more controversial than I was thinking.

65
00:06:21,800 --> 00:06:23,800
I think so.

66
00:06:23,800 --> 00:06:27,800
I mean, the concept is awesome, as I said.

67
00:06:27,800 --> 00:06:32,800
It's just, it used to be called Columbus, they didn't, or is that a different day.

68
00:06:32,800 --> 00:06:33,800
That's a different day.

69
00:06:33,800 --> 00:06:34,800
It's a different day.

70
00:06:34,800 --> 00:06:38,800
You've got a day that celebrates this horrible, horrible person.

71
00:06:38,800 --> 00:06:42,800
Well, maybe horrible, but pretty bad.

72
00:06:42,800 --> 00:06:48,800
Well, the day that we have this Columbus day, I believe, is the Canadian Thanksgiving.

73
00:06:48,800 --> 00:06:53,800
But our Thanksgiving is a month later.

74
00:06:53,800 --> 00:06:58,800
Yeah, you celebrate the guy who, I mean, he was an opportunist apparently.

75
00:06:58,800 --> 00:07:01,800
Just kind of looking for gold.

76
00:07:01,800 --> 00:07:08,800
And he thought he'd reached India, so that's why he called them Indians.

77
00:07:08,800 --> 00:07:14,800
Anyway, this is very skirting the edge of what is not Amazon.

78
00:07:14,800 --> 00:07:21,800
And better jump back in and see what, do we have any, the quote is kind of neat.

79
00:07:21,800 --> 00:07:26,800
Talks about grasping and worry.

80
00:07:26,800 --> 00:07:30,800
So we have the five aggregates, and that's what we cling to.

81
00:07:30,800 --> 00:07:35,800
The neat clinging is always done to one of the five, or combinations of the five.

82
00:07:35,800 --> 00:07:40,800
Or concepts that have arisen based on the five aggregates.

83
00:07:40,800 --> 00:07:45,800
So we cling to the body, and when it changes, we worry.

84
00:07:45,800 --> 00:07:49,800
I mean, if we, it upsets us, it disturbs us.

85
00:07:49,800 --> 00:07:52,800
Not just the body, but other physical forms as well.

86
00:07:52,800 --> 00:07:54,800
Our belongings are positioned.

87
00:07:54,800 --> 00:07:57,800
My robe.

88
00:07:57,800 --> 00:08:01,800
These little ties on the end of the robe.

89
00:08:01,800 --> 00:08:06,800
My, these little, these little ties here.

90
00:08:06,800 --> 00:08:14,800
And one of them up here ripped yesterday, or ripped out yesterday.

91
00:08:14,800 --> 00:08:17,800
And just sew it back.

92
00:08:17,800 --> 00:08:18,800
But that didn't upset me.

93
00:08:18,800 --> 00:08:20,800
That wasn't a big deal.

94
00:08:20,800 --> 00:08:23,800
It's easy to get upset.

95
00:08:23,800 --> 00:08:26,800
So I remember, I think when I first got this robe,

96
00:08:26,800 --> 00:08:31,800
like a few days later, it got some stain on it or something.

97
00:08:31,800 --> 00:08:32,800
Yeah.

98
00:08:35,800 --> 00:08:37,800
When I got,

99
00:08:41,800 --> 00:08:43,800
we cling to things.

100
00:08:43,800 --> 00:08:44,800
We cling to our possessions.

101
00:08:44,800 --> 00:08:46,800
We cling to people.

102
00:08:46,800 --> 00:08:51,800
We cling to form.

103
00:08:51,800 --> 00:08:56,800
And we cling to feelings.

104
00:08:56,800 --> 00:08:58,800
We cling to pleasant feelings.

105
00:08:58,800 --> 00:09:03,800
We try to contain them any way possible.

106
00:09:03,800 --> 00:09:09,800
And we're constantly on guard against painful feelings.

107
00:09:09,800 --> 00:09:11,800
We're often specific painful feelings.

108
00:09:11,800 --> 00:09:14,800
So if someone has maybe a bad back,

109
00:09:14,800 --> 00:09:18,800
then they're constantly worried about constantly.

110
00:09:18,800 --> 00:09:23,800
But again and again and again.

111
00:09:23,800 --> 00:09:25,800
I just, I'm a little bit of pain and immediately.

112
00:09:25,800 --> 00:09:32,800
It's, I don't know, my back problem becomes an obsession.

113
00:09:32,800 --> 00:09:39,800
I don't get very much caught up.

114
00:09:39,800 --> 00:09:42,800
They're attachment to feelings.

115
00:09:42,800 --> 00:09:45,800
They're attachment to perceptions.

116
00:09:45,800 --> 00:09:50,800
Recognition, I mean, it's, it's one not always by itself,

117
00:09:50,800 --> 00:09:57,800
but it's recognition that, that is the trigger

118
00:09:58,800 --> 00:10:01,800
for much of our addiction, much of our aversion.

119
00:10:01,800 --> 00:10:03,800
We recognize something.

120
00:10:03,800 --> 00:10:06,800
If you didn't recognize things,

121
00:10:06,800 --> 00:10:09,800
you wouldn't recognize a spider.

122
00:10:09,800 --> 00:10:11,800
People who are afraid of spiders.

123
00:10:11,800 --> 00:10:13,800
If you didn't recognize that that was a spider,

124
00:10:13,800 --> 00:10:15,800
it could walk all over you and say,

125
00:10:15,800 --> 00:10:17,800
you'd have no idea what was going on,

126
00:10:17,800 --> 00:10:20,800
but you'd feel something and no, just a feeling.

127
00:10:25,800 --> 00:10:26,800
If we weren't for recognition,

128
00:10:26,800 --> 00:10:30,800
if it weren't for our perceptions of things,

129
00:10:30,800 --> 00:10:35,800
we couldn't give rise to liking or disliking

130
00:10:35,800 --> 00:10:39,800
because we couldn't, we couldn't associate.

131
00:10:39,800 --> 00:10:46,800
Well, you see a piece of cheesecake

132
00:10:47,800 --> 00:10:48,800
and you associate it with cheesecake.

133
00:10:48,800 --> 00:10:52,800
You associate it with tastes and textures and feelings.

134
00:10:52,800 --> 00:10:55,800
And so it makes you feel happy just to see it,

135
00:10:55,800 --> 00:10:57,800
but if you didn't know what it was,

136
00:10:57,800 --> 00:10:59,800
if you couldn't recognize it.

137
00:10:59,800 --> 00:11:04,800
So recognition is actually quite neutral,

138
00:11:04,800 --> 00:11:09,800
but we cling to this.

139
00:11:09,800 --> 00:11:12,800
We agree for that simple recognition

140
00:11:12,800 --> 00:11:15,800
because there are many things that aren't inherently pleasant.

141
00:11:15,800 --> 00:11:19,800
Seeing a piece of cheesecake isn't inherently pleasant.

142
00:11:19,800 --> 00:11:22,800
Seeing a spider isn't inherently unpleasant.

143
00:11:22,800 --> 00:11:24,800
But because we reacted to them,

144
00:11:24,800 --> 00:11:27,800
because we cling to that,

145
00:11:27,800 --> 00:11:29,800
we recognize the recognition of a spider

146
00:11:29,800 --> 00:11:34,800
so it's the moment when we get upset.

147
00:11:34,800 --> 00:11:37,800
And then Sankara, we cling to them.

148
00:11:37,800 --> 00:11:45,800
We cling to the Sankars of comparison

149
00:11:45,800 --> 00:11:47,800
when you compare some things.

150
00:11:47,800 --> 00:11:53,800
So if you look at your body and you're really fat like me,

151
00:11:53,800 --> 00:11:55,800
then you start to worry about your weight

152
00:11:55,800 --> 00:11:59,800
and you have to go to diet and stop eating so much.

153
00:11:59,800 --> 00:12:01,800
You all get fat.

154
00:12:01,800 --> 00:12:03,800
Every time I end up,

155
00:12:03,800 --> 00:12:06,800
everywhere I go people always tell me I'm getting thinner.

156
00:12:06,800 --> 00:12:08,800
It's funny whenever I see people I haven't seen for a while,

157
00:12:08,800 --> 00:12:11,800
they keep telling me I'm getting thinner.

158
00:12:11,800 --> 00:12:14,800
People have been telling me for 15 years that I'm getting thinner,

159
00:12:14,800 --> 00:12:19,800
so we worry about these things.

160
00:12:19,800 --> 00:12:22,800
I'm too seeing them too fat.

161
00:12:22,800 --> 00:12:24,800
Some people have become an obsession

162
00:12:24,800 --> 00:12:28,800
so they become bulimic or anorexic.

163
00:12:28,800 --> 00:12:35,800
Too tall and too short.

164
00:12:35,800 --> 00:12:37,800
I'm cling to our thoughts about things.

165
00:12:37,800 --> 00:12:40,800
We cling to our emotions.

166
00:12:40,800 --> 00:12:47,800
I'm depressed and then I identify with it.

167
00:12:47,800 --> 00:12:50,800
Or I like something and it's like I like it.

168
00:12:50,800 --> 00:12:52,800
And you become attached to this desire

169
00:12:52,800 --> 00:12:54,800
and you say yes, got to get that desire

170
00:12:54,800 --> 00:12:58,800
or got to follow that desire whenever it comes.

171
00:12:58,800 --> 00:13:01,800
But this thing is to worry and stress

172
00:13:01,800 --> 00:13:04,800
when you can't get to what you want

173
00:13:04,800 --> 00:13:07,800
when you're denied.

174
00:13:07,800 --> 00:13:12,800
At least to anger, to grief, to sorrow, to suffering.

175
00:13:12,800 --> 00:13:16,800
In consciousness, while consciousness is another one of the neutral ones,

176
00:13:16,800 --> 00:13:20,800
there's nothing ostensibly problematic about consciousness,

177
00:13:20,800 --> 00:13:28,800
or consciousness, right, except that consciousness is like a capsule

178
00:13:28,800 --> 00:13:29,800
for all the rest.

179
00:13:29,800 --> 00:13:33,800
If it weren't for consciousness, you couldn't have judgments,

180
00:13:33,800 --> 00:13:39,800
you couldn't have any of the problems.

181
00:13:39,800 --> 00:13:42,800
It's like if you don't have a spider's web,

182
00:13:42,800 --> 00:13:43,800
you don't have a spider.

183
00:13:43,800 --> 00:13:47,800
You don't have an ant home, you don't have ants.

184
00:13:47,800 --> 00:13:52,800
If you don't have, it's like a breeding ground for,

185
00:13:52,800 --> 00:13:56,800
let's say for bacteria or something.

186
00:13:56,800 --> 00:14:00,800
Like these rusty nail.

187
00:14:00,800 --> 00:14:02,800
You know how everyone tells you

188
00:14:02,800 --> 00:14:03,800
you have to be very careful.

189
00:14:03,800 --> 00:14:05,800
You don't step on a rusty nail.

190
00:14:05,800 --> 00:14:09,800
And if you do, you have to get it cleaned out

191
00:14:09,800 --> 00:14:11,800
and you have to get a shot or something.

192
00:14:11,800 --> 00:14:14,800
You have to get a shot in case you step on a rusty nail.

193
00:14:14,800 --> 00:14:17,800
When I was younger, I did step, I lived on a farm

194
00:14:17,800 --> 00:14:19,800
and then lots of rusty nails around.

195
00:14:19,800 --> 00:14:24,800
I did step on a rusty nail, but I had my tennis shot.

196
00:14:24,800 --> 00:14:28,800
When I was in California, I stepped on a rusty

197
00:14:28,800 --> 00:14:29,800
something.

198
00:14:29,800 --> 00:14:31,800
Then that's a medal, something.

199
00:14:31,800 --> 00:14:34,800
I was walking barefoot in Tarzanah.

200
00:14:34,800 --> 00:14:39,800
I went every day to a French restaurant.

201
00:14:39,800 --> 00:14:44,800
It was the most exotic arms around ever.

202
00:14:44,800 --> 00:14:45,800
Every day I would go on an arms round

203
00:14:45,800 --> 00:14:48,800
and I would go to a French restaurant

204
00:14:48,800 --> 00:14:51,800
and a Thai restaurant.

205
00:14:51,800 --> 00:14:58,800
And I think some Thai people stopped me on the way as well.

206
00:14:58,800 --> 00:15:01,800
And then back at the house,

207
00:15:01,800 --> 00:15:05,800
some people at the house would offer arms as well.

208
00:15:05,800 --> 00:15:09,800
Every day I was having waffles or pancakes

209
00:15:09,800 --> 00:15:12,800
with mounds of real.

210
00:15:12,800 --> 00:15:14,800
It was high quality.

211
00:15:14,800 --> 00:15:19,800
This is LA and sort of Hollywood.

212
00:15:19,800 --> 00:15:21,800
High so French restaurant

213
00:15:21,800 --> 00:15:24,800
because it was owned actually by Thai people.

214
00:15:24,800 --> 00:15:27,800
But one day I stepped on a metal piece of metal

215
00:15:27,800 --> 00:15:32,800
and they took me right away to the emergency room.

216
00:15:32,800 --> 00:15:41,800
And it ended up costing like $500 to what?

217
00:15:41,800 --> 00:15:43,800
Can't remember just to have it checked out

218
00:15:43,800 --> 00:15:46,800
and then get me a tennis shot, I think.

219
00:15:46,800 --> 00:15:48,800
I was like $500.

220
00:15:48,800 --> 00:15:50,800
That sounds about right, yes.

221
00:15:50,800 --> 00:15:51,800
Two walk in.

222
00:15:51,800 --> 00:15:52,800
Two walk in the door.

223
00:15:52,800 --> 00:15:54,800
Yes, $500.

224
00:15:54,800 --> 00:15:57,800
No, Americans.

225
00:15:57,800 --> 00:16:01,800
Crazy people, crazy country.

226
00:16:01,800 --> 00:16:05,800
But my point there was totally off track

227
00:16:05,800 --> 00:16:09,800
is there's nothing wrong with rest.

228
00:16:09,800 --> 00:16:10,800
So it got me thinking.

229
00:16:10,800 --> 00:16:13,800
I had always thought that rest was

230
00:16:13,800 --> 00:16:15,800
rest gave you tetanus.

231
00:16:15,800 --> 00:16:16,800
Right?

232
00:16:16,800 --> 00:16:17,800
Rest gives you lock jaw

233
00:16:17,800 --> 00:16:19,800
because if you get rest in your bloodstream,

234
00:16:19,800 --> 00:16:22,800
it must cause what we call lock jaw or tetanus

235
00:16:22,800 --> 00:16:23,800
or your body.

236
00:16:23,800 --> 00:16:26,800
It's a really horrible sickness.

237
00:16:26,800 --> 00:16:28,800
But it's not actually the case.

238
00:16:28,800 --> 00:16:32,800
It's there's a virus known virus bacteria.

239
00:16:32,800 --> 00:16:37,800
And the bacteria lives in the rusty metal

240
00:16:37,800 --> 00:16:40,800
because rust makes holes in metal.

241
00:16:40,800 --> 00:16:45,800
So you have a metal nail and when the rust creates pockets

242
00:16:45,800 --> 00:16:47,800
and pockets where bacteria can live

243
00:16:47,800 --> 00:16:49,800
and one of the bacteria is that likes to live

244
00:16:49,800 --> 00:16:54,800
in those pockets or is potentially living in those pockets

245
00:16:54,800 --> 00:16:59,800
is the use tetanus bacteria or what they're called.

246
00:16:59,800 --> 00:17:01,800
And it's just so perfect

247
00:17:01,800 --> 00:17:03,800
because it's like injecting yourself

248
00:17:03,800 --> 00:17:05,800
with those bacteria when you get a nail

249
00:17:05,800 --> 00:17:07,800
right into your deep into your bloodstream.

250
00:17:07,800 --> 00:17:09,800
That is why you get it.

251
00:17:09,800 --> 00:17:10,800
You don't always get it.

252
00:17:10,800 --> 00:17:11,800
It's certainly not from the rest.

253
00:17:11,800 --> 00:17:14,800
So my point being consciousness,

254
00:17:14,800 --> 00:17:16,800
you could argue it's not a problem.

255
00:17:16,800 --> 00:17:17,800
It's neutral.

256
00:17:17,800 --> 00:17:20,800
But consciousness is like this rusty nail.

257
00:17:20,800 --> 00:17:22,800
It's got lots of bad things,

258
00:17:22,800 --> 00:17:26,800
potentially in it or it's very nature.

259
00:17:26,800 --> 00:17:31,800
Allows it to be a vessel for bad things.

260
00:17:31,800 --> 00:17:34,800
There's an analogy for you.

261
00:17:34,800 --> 00:17:36,800
So that's the dhamma.

262
00:17:36,800 --> 00:17:39,800
There's some dhamma today, you know.

263
00:17:39,800 --> 00:17:41,800
It's a good teaching.

264
00:17:41,800 --> 00:17:45,800
You've got to stand in awe of the Buddha

265
00:17:45,800 --> 00:17:49,800
and how powerful his teachings are.

266
00:17:49,800 --> 00:17:52,800
I always sounded like a religious zealot

267
00:17:52,800 --> 00:17:54,800
and I never thought I would be a religious zealot

268
00:17:54,800 --> 00:17:57,800
because I always made fun of religious zealots.

269
00:17:57,800 --> 00:18:00,800
But you know, if they were right

270
00:18:00,800 --> 00:18:05,800
then they would be justified to be zealots.

271
00:18:05,800 --> 00:18:10,800
It's just we're right.

272
00:18:10,800 --> 00:18:13,800
So I don't know.

273
00:18:13,800 --> 00:18:16,800
I mean, it don't mean it quite like that.

274
00:18:16,800 --> 00:18:18,800
It's not worth gloating over for sure.

275
00:18:18,800 --> 00:18:24,800
But it is awesome.

276
00:18:24,800 --> 00:18:26,800
Oh, Fernando sent me some subtitles.

277
00:18:26,800 --> 00:18:28,800
Okay, let me do that now.

278
00:18:28,800 --> 00:18:30,800
Let's see here.

279
00:18:30,800 --> 00:18:32,800
Let's do it while we're online.

280
00:18:32,800 --> 00:18:33,800
He sent me Spanish subtitles.

281
00:18:33,800 --> 00:18:37,800
This is an example to everyone.

282
00:18:37,800 --> 00:18:42,800
Oh, wait, no, don't go.

283
00:18:42,800 --> 00:18:47,800
So what we're going to do?

284
00:18:47,800 --> 00:18:54,800
Go find the video.

285
00:19:01,800 --> 00:19:06,800
We've still got this old series on how to meditate.

286
00:19:06,800 --> 00:19:15,800
And this one is how to meditate HD.

287
00:19:15,800 --> 00:19:18,800
Yeah.

288
00:19:18,800 --> 00:19:21,800
We should really redo these videos again, though.

289
00:19:21,800 --> 00:19:25,800
How many years old already?

290
00:19:25,800 --> 00:19:30,800
Set a video language, which language English?

291
00:19:30,800 --> 00:19:33,800
English or English, okay.

292
00:19:33,800 --> 00:19:35,800
That's funny.

293
00:19:35,800 --> 00:19:37,800
There's two language choices.

294
00:19:37,800 --> 00:19:38,800
Three language choices.

295
00:19:38,800 --> 00:19:42,800
English and English United Kingdom and French Canada.

296
00:19:42,800 --> 00:19:45,800
Why is America the default English now?

297
00:19:45,800 --> 00:19:48,800
The British aren't English enough?

298
00:19:48,800 --> 00:19:50,800
Is this still Google?

299
00:19:50,800 --> 00:19:51,800
Yeah.

300
00:19:51,800 --> 00:19:58,800
Anyway, English United Kingdom is the default.

301
00:19:58,800 --> 00:20:01,800
Okay, so we published, we've already published several.

302
00:20:01,800 --> 00:20:06,800
I remember I did a thing back in the day where I got people to send me some titles.

303
00:20:06,800 --> 00:20:12,800
So we've got English, Indonesian, Portuguese, Swedish, and Vietnamese.

304
00:20:12,800 --> 00:20:20,800
So I'm going to add Fernando's Spanish subtitles.

305
00:20:20,800 --> 00:20:23,800
Download them.

306
00:20:23,800 --> 00:20:28,800
Save them.

307
00:20:28,800 --> 00:20:30,800
How do you access these monthly updates?

308
00:20:30,800 --> 00:20:32,800
Are these on your channel as well?

309
00:20:32,800 --> 00:20:34,800
You go to CC, I think.

310
00:20:34,800 --> 00:20:37,800
Okay, so we've got Spanish.

311
00:20:37,800 --> 00:20:42,800
Spanish Latin Americans, Spanish Mexico, and Spanish Spain.

312
00:20:42,800 --> 00:20:44,800
Fernando's from Spain, I think, right?

313
00:20:44,800 --> 00:20:45,800
So they're going to say.

314
00:20:45,800 --> 00:20:47,800
I think Fernando's from Mexico.

315
00:20:47,800 --> 00:20:53,800
Oh, well, there's Fernando there for Fernando, which country is it?

316
00:20:53,800 --> 00:21:03,800
Well, for the flags, meditation segments, which language are you using Spanish Spain or Spanish Mexican?

317
00:21:03,800 --> 00:21:08,800
They're not that different, are they?

318
00:21:08,800 --> 00:21:10,800
I don't know.

319
00:21:10,800 --> 00:21:14,800
Most people in my area that speak Spanish are from Puerto Rico.

320
00:21:14,800 --> 00:21:29,800
I'm about 35% of the people in my town are from Puerto Rico, and that's a little different as well.

321
00:21:29,800 --> 00:21:31,800
Fernando said Latin America.

322
00:21:31,800 --> 00:21:34,800
So what, that's different from Spain?

323
00:21:34,800 --> 00:21:37,800
Yeah.

324
00:21:37,800 --> 00:21:44,800
Spanish, Latin America, there.

325
00:21:44,800 --> 00:22:00,800
Upload a file.

326
00:22:00,800 --> 00:22:05,800
All I mean, we need those as the city, the videos.

327
00:22:05,800 --> 00:22:11,800
I care, the como medita.

328
00:22:11,800 --> 00:22:17,800
And as the premier video, hablari, a care card, the quail,

329
00:22:17,800 --> 00:22:24,800
as a significado, the La Palabra meditas.

330
00:22:24,800 --> 00:22:37,800
That must be canned.

331
00:22:37,800 --> 00:22:46,800
It's very much like Latin, similar anyway.

332
00:22:46,800 --> 00:22:51,800
The word is probably canned.

333
00:22:51,800 --> 00:22:54,800
You know, a 29% of English comes from Latin.

334
00:22:54,800 --> 00:22:57,800
I'm going to learn that this week.

335
00:22:57,800 --> 00:23:01,800
I'm learning Latin is useful.

336
00:23:01,800 --> 00:23:15,800
The word possible comes from Pós, Pós, Pós, and sumas, sumas.

337
00:23:15,800 --> 00:23:22,800
Okay, it's up. I think, oh no, it's not.

338
00:23:22,800 --> 00:23:25,800
Okay, it's up.

339
00:23:25,800 --> 00:23:27,800
Check it out, Fernando.

340
00:23:27,800 --> 00:23:28,800
Check it out, everyone.

341
00:23:28,800 --> 00:23:34,800
Go to my first out of meditating and the tape video.

342
00:23:34,800 --> 00:23:38,800
How do you choose your language?

343
00:23:38,800 --> 00:23:41,800
Spanish.

344
00:23:41,800 --> 00:23:50,800
Hi, and welcome to this series of videos on how to meditate.

345
00:23:50,800 --> 00:23:53,800
And this is the first video I'll be talking about.

346
00:23:53,800 --> 00:23:54,800
What is the meaning?

347
00:23:54,800 --> 00:24:01,800
It has a thing, say, talking about the DVD.

348
00:24:01,800 --> 00:24:04,800
That's why people are still contacting me about the DVD,

349
00:24:04,800 --> 00:24:06,800
because these videos all talk about it.

350
00:24:06,800 --> 00:24:10,800
I'll have a thing about it.

351
00:24:10,800 --> 00:24:12,800
They'll remove that.

352
00:24:12,800 --> 00:24:14,800
We're not doing the DVDs in here.

353
00:24:28,800 --> 00:24:32,800
The website may mention the DVD as well.

354
00:24:32,800 --> 00:24:35,800
So, can we get rid of it?

355
00:24:35,800 --> 00:24:37,800
Yeah.

356
00:24:37,800 --> 00:24:42,800
It also mentions about the download again of BitTorrent.

357
00:24:42,800 --> 00:24:44,800
Is that still?

358
00:24:44,800 --> 00:24:45,800
I don't know.

359
00:24:45,800 --> 00:24:47,800
Is that still current?

360
00:24:47,800 --> 00:24:49,800
I think we can get rid of it.

361
00:24:49,800 --> 00:24:50,800
It's all on YouTube.

362
00:24:50,800 --> 00:24:51,800
That's good enough.

363
00:24:51,800 --> 00:24:54,800
We need to DVD.

364
00:24:54,800 --> 00:25:01,800
Mm-hmm.

365
00:25:01,800 --> 00:25:03,800
Yeah, so it's how to meditate.

366
00:25:03,800 --> 00:25:06,800
DVD is available via BitTorrent.

367
00:25:06,800 --> 00:25:11,800
So, I'm just getting rid of that.

368
00:25:30,800 --> 00:25:34,800
Okay, so how are we doing any questions?

369
00:25:34,800 --> 00:25:38,800
Now, you have answered all the questions, won't they?

370
00:25:38,800 --> 00:25:43,800
We finally come to that point, no?

371
00:25:43,800 --> 00:25:47,800
That's a good thing, right?

372
00:25:47,800 --> 00:25:51,800
It's a good thing.

373
00:25:51,800 --> 00:25:57,800
That's fine.

374
00:25:57,800 --> 00:26:01,800
Tomorrow we're going to try the medmod meditation mob thing again.

375
00:26:01,800 --> 00:26:05,800
We'll see if that works.

376
00:26:05,800 --> 00:26:07,800
How that works.

377
00:26:07,800 --> 00:26:11,800
This time people are actually going to come out.

378
00:26:11,800 --> 00:26:13,800
What else?

379
00:26:13,800 --> 00:26:16,800
Oh, I think I really mentioned.

380
00:26:16,800 --> 00:26:19,800
I think we're going to mention that the robot arrived.

381
00:26:19,800 --> 00:26:20,800
Sorry.

382
00:26:20,800 --> 00:26:22,800
I believe Aruna mentioned that the robot arrived.

383
00:26:22,800 --> 00:26:23,800
Oh, yeah.

384
00:26:23,800 --> 00:26:24,800
We've got it here.

385
00:26:24,800 --> 00:26:25,800
Good.

386
00:26:25,800 --> 00:26:28,800
Robes, I guess it's a set, right?

387
00:26:28,800 --> 00:26:29,800
Yeah.

388
00:26:29,800 --> 00:26:47,800
Okay, well, I'm going to go then.

389
00:26:47,800 --> 00:26:50,800
Thank you all for coming out, for tuning in.

390
00:26:50,800 --> 00:26:52,800
We're practicing with us.

391
00:26:52,800 --> 00:26:55,800
Be long list of people.

392
00:26:55,800 --> 00:26:57,800
Have a good night.

393
00:26:57,800 --> 00:27:00,800
Thank you, Robin, for joining me.

394
00:27:00,800 --> 00:27:28,800
Thank you, Bombay.

