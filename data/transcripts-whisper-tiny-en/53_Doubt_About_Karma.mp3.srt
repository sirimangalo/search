1
00:00:00,000 --> 00:00:07,440
doubt, question, and answer. Doubt about karma. If mind is just a concept, what is it that

2
00:00:07,440 --> 00:00:14,280
gets reborn or reincarnated that carries our karma? I'm a little confused. Nothing gets

3
00:00:14,280 --> 00:00:21,680
reborn, nothing gets reincarnated, nothing carries karma. Karma itself does not exist, confusion

4
00:00:21,680 --> 00:00:26,840
exists, confusion arises in the mind, and this is much more important than getting an answer

5
00:00:26,840 --> 00:00:31,880
to the question. You do not really need answers to these sorts of questions, getting answers

6
00:00:31,880 --> 00:00:36,840
to questions can assuaged out, but it can also lead to avoiding the real problem of habitual

7
00:00:36,840 --> 00:00:41,440
doubting. It is natural to think, well, I have doubt, so I need an answer to remove the

8
00:00:41,440 --> 00:00:48,120
doubt. If you look at the doubt, however, you can see that it is unhelpful, useless.

9
00:00:48,120 --> 00:00:53,360
The experience of enlightenment does away with doubt. A sotapana is free from the mental

10
00:00:53,360 --> 00:00:58,680
hindrance of doubt. Doubt is a hindrance at distracts and crushes the mind's potential,

11
00:00:58,680 --> 00:01:03,520
halts the spiritual progress in its tracks. Doubt stops you from seeing clearly and stops

12
00:01:03,520 --> 00:01:09,760
you from finding peace. Overcoming doubt is one way of understanding the goal of our practice,

13
00:01:09,760 --> 00:01:14,760
giving up the need for answers and solutions to questions and problems. In that sense,

14
00:01:14,760 --> 00:01:20,320
you do not need answers to any question, even the ones that seem so important. One common

15
00:01:20,320 --> 00:01:25,280
question that people seem to think is important is whether there is such a thing as rebirth.

16
00:01:25,280 --> 00:01:29,440
People have even told me that they cannot practice unless they have something to convince

17
00:01:29,440 --> 00:01:34,480
themselves of the truth of reincarnation and rebirth. They say that Buddhism makes no

18
00:01:34,480 --> 00:01:39,360
sense to them unless they can answer this question. This is a very pitiable state to be

19
00:01:39,360 --> 00:01:44,160
in, because they probably will never acquire the proof that they are looking for. Even

20
00:01:44,160 --> 00:01:48,640
material science does not provide proofs. The only way to prove what happens after this

21
00:01:48,640 --> 00:01:53,840
life would be to experience it for yourself, which puts you in the impossible position of having

22
00:01:53,840 --> 00:01:59,360
to wait until you pass away before you can practice. I think that often people simply are

23
00:01:59,360 --> 00:02:04,080
excited by the idea of remembering their past lives, which is of course just craving.

24
00:02:04,720 --> 00:02:09,440
To directly answer your question, it is not that Buddhists believe in rebirth, it is that

25
00:02:09,440 --> 00:02:15,120
Buddhists do not believe in death. This accurately reflects the Buddhist teaching as I understand

26
00:02:15,120 --> 00:02:20,080
them because death and Buddhism from an orthodox point of view occurs at every moment.

27
00:02:20,720 --> 00:02:26,000
This is what we mean when we see the mind does not exist. Mind is just a designation for the

28
00:02:26,000 --> 00:02:31,920
mental aspect of these momentary experiences, the awareness of an object that is part of any experience.

29
00:02:32,480 --> 00:02:37,200
When we talk about the moment of physical deaths, we are referring to a concept because in

30
00:02:37,200 --> 00:02:43,120
ultimate reality, nothing dies at that moment. The mind and body are constantly arising and ceasing

31
00:02:43,120 --> 00:02:49,680
together. The physical and mental are arising and ceasing, arising and ceasing, and that does not stop

32
00:02:49,680 --> 00:02:56,560
ever until you free yourself from craving. Doubt is insidious. Doubt is not a sign that you need

33
00:02:56,560 --> 00:03:02,720
an answer, it is a sign that you have a problem, and that problem is the doubt. Even if you are

34
00:03:02,720 --> 00:03:08,160
given answers to all your questions, you will likely still have doubt. The reason for this is not

35
00:03:08,160 --> 00:03:13,680
always that you are not given the right answers, but that rather that being given answers is not the

36
00:03:13,680 --> 00:03:20,000
answer. The answer is to focus on the doubt. You know you are in doubt, so focus on the doubt.

37
00:03:20,000 --> 00:03:24,960
Forget about what you are doubting about and focus on the doubt itself because once you

38
00:03:24,960 --> 00:03:30,960
free yourself from doubt, you have done all that you need to do. That being said, this specific

39
00:03:30,960 --> 00:03:36,160
question does have a specific answer, and it is an answer that can be helpful in leading one

40
00:03:36,160 --> 00:03:42,080
to focusing on experiences, of which doubt is one. In order to understand reality, we have to

41
00:03:42,080 --> 00:03:47,200
give up all of our preconceived notions about it, because all you can say for certain about reality

42
00:03:47,200 --> 00:03:53,520
without resorting to abstract conceptualization is that experience occurs. There is seen,

43
00:03:53,520 --> 00:03:58,240
there is hearing, there is smelling, there is tasting, there is feeling, and there is thinking.

44
00:03:58,240 --> 00:04:03,520
You can say that pretty reassuringly. The question of whether what you experience is real or not

45
00:04:03,520 --> 00:04:10,240
real or whether you are hallucinating is meaningless. The experience occurs as it occurs or does

46
00:04:10,240 --> 00:04:15,760
not occur at all. From this basic truth, you can extrapolate and say that it goes against your

47
00:04:15,760 --> 00:04:20,800
experience to suggest that experience ceases without remainder. When you watch your experiences

48
00:04:20,800 --> 00:04:26,640
arise and cease, you must admit that as far as you can see, experience continues indefinitely.

49
00:04:26,640 --> 00:04:31,520
As for what leads to rebirth, you can see that experience forms a sort of causal chain.

50
00:04:31,520 --> 00:04:37,520
Causes have effects, desire leads to fruition. For example, you want something which leads to

51
00:04:37,520 --> 00:04:43,280
thinking about it, which develops into actions and situations based on the desire. We can see in

52
00:04:43,280 --> 00:04:48,800
this life how craving leads to becoming. At the moment of death, it is not categorically different.

53
00:04:49,440 --> 00:04:54,560
Karma is the relationship between a state of mind, like craving and subsequent experiences.

54
00:04:55,120 --> 00:05:00,400
Wisdom, for example, leads to happiness and peace through seeing how craving leads to suffering.

55
00:05:00,400 --> 00:05:06,720
Karma is a relationship from one experience to the next, and as such, it does not ultimately exist

56
00:05:06,720 --> 00:05:12,640
as more than a concept. You can see for yourself that when this arises, that arises. With the

57
00:05:12,640 --> 00:05:34,480
arising of this, there is the arising of that, and when this does not arise, that does not arise.

