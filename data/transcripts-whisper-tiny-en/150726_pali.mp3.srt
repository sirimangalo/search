1
00:00:00,000 --> 00:00:06,080
Okay, so we're starting up with Polly.

2
00:00:06,080 --> 00:00:16,120
Today we continue on with verbs, a cayapo, and we are...

3
00:00:16,120 --> 00:00:18,480
We have done just to recap.

4
00:00:18,480 --> 00:00:22,800
We've talked about the different parts that we need for verbs, and the two basic things

5
00:00:22,800 --> 00:00:29,800
we went over are time, gala, and dad to the roots.

6
00:00:29,800 --> 00:00:43,800
So gala, there are not just the gala, but there are forms for optative and imperative as well.

7
00:00:43,800 --> 00:00:57,800
All in all, there are eight tenses that we consider.

8
00:00:57,800 --> 00:01:00,800
Page 43.

9
00:01:00,800 --> 00:01:05,800
But we also had a question on the homework that was in the text.

10
00:01:05,800 --> 00:01:13,800
Just wondering about the significance of the P and the A on the chart.

11
00:01:13,800 --> 00:01:15,800
One chart.

12
00:01:15,800 --> 00:01:22,800
The chart that we memorized for homework on Page 44, the whatama, whatama.

13
00:01:22,800 --> 00:01:24,800
Right, okay, so let's do that today.

14
00:01:24,800 --> 00:01:26,800
Let's get into that.

15
00:01:26,800 --> 00:01:28,800
Scroll up to Page 43.

16
00:01:28,800 --> 00:01:32,800
Yeah, and remember the eight things that verbs need.

17
00:01:32,800 --> 00:01:35,800
So we've done gala and dad to.

18
00:01:35,800 --> 00:01:38,800
And I skipped two in the middle there.

19
00:01:38,800 --> 00:01:42,800
So for each gala and gala means time, but as I said, not all of them are time.

20
00:01:42,800 --> 00:01:44,800
So let's think of it as tenses.

21
00:01:44,800 --> 00:01:46,800
Because there are two tenses that are not...

22
00:01:46,800 --> 00:01:48,800
Don't have any time.

23
00:01:48,800 --> 00:01:50,800
Although they are present tense.

24
00:01:50,800 --> 00:01:51,800
There are two...

25
00:01:51,800 --> 00:01:53,800
There's eight conjugations.

26
00:01:53,800 --> 00:01:57,800
Each one of these eight is going to have its own paradigm.

27
00:01:57,800 --> 00:02:00,800
Right, which you've memorized one of.

28
00:02:00,800 --> 00:02:03,800
Within those paradigms, numbers should be clear.

29
00:02:03,800 --> 00:02:06,800
You've got echo, chana, and bawwatchana.

30
00:02:06,800 --> 00:02:09,800
So if the subject and it agrees with the sentence.

31
00:02:09,800 --> 00:02:14,800
So if the subject is plural, the verb has to be in the plural form.

32
00:02:14,800 --> 00:02:15,800
Not the object.

33
00:02:15,800 --> 00:02:16,800
If the object...

34
00:02:16,800 --> 00:02:19,800
This is the same in English, the same in most languages.

35
00:02:19,800 --> 00:02:21,800
It has to agree with the subject.

36
00:02:21,800 --> 00:02:24,800
So number is easy to understand.

37
00:02:24,800 --> 00:02:25,800
Bada.

38
00:02:25,800 --> 00:02:29,800
Bada doesn't literally mean reference.

39
00:02:29,800 --> 00:02:34,800
But the usage of the word reference, that means referring to self.

40
00:02:34,800 --> 00:02:36,800
Or referring to other.

41
00:02:36,800 --> 00:02:40,800
So in English, we would say reflexive.

42
00:02:40,800 --> 00:02:44,800
There are two types of verbs.

43
00:02:44,800 --> 00:02:48,800
I believe it's reflexive, which means referring to yourself.

44
00:02:48,800 --> 00:02:51,800
Transitive, is that the other one?

45
00:02:51,800 --> 00:02:54,800
No, that's not reflexive.

46
00:02:54,800 --> 00:02:57,800
And...

47
00:02:57,800 --> 00:03:02,800
Can't remember.

48
00:03:02,800 --> 00:03:13,800
So theoretically, this means that one of them is going to be when you're talking about.

49
00:03:13,800 --> 00:03:22,800
Like, if you say I hate myself, that would be reflexive, I think.

50
00:03:22,800 --> 00:03:24,800
Or I dress myself.

51
00:03:24,800 --> 00:03:27,800
Or I dress.

52
00:03:27,800 --> 00:03:29,800
I get dressed.

53
00:03:29,800 --> 00:03:33,800
I believe that's reflexive.

54
00:03:33,800 --> 00:03:38,800
And if you say I dress someone else, that's the opposite.

55
00:03:38,800 --> 00:03:39,800
I think.

56
00:03:39,800 --> 00:03:42,800
But it's not really important because that's not how these are used.

57
00:03:42,800 --> 00:03:48,800
And, in fact, they're often used interchangeably.

58
00:03:48,800 --> 00:03:53,800
Much, much more common is the P form, which is parasapada.

59
00:03:53,800 --> 00:03:58,800
If you look at Undergala, on page 43 at the bottom there,

60
00:03:58,800 --> 00:04:14,800
it's talking about the two pad, the pad ends.

61
00:04:14,800 --> 00:04:21,800
So parasapada for other, which is whatever.

62
00:04:21,800 --> 00:04:25,800
At the open, that means for oneself.

63
00:04:25,800 --> 00:04:31,800
So parasapada is more common, but Atnopada is used.

64
00:04:31,800 --> 00:04:38,800
And how they are used is for passive or active.

65
00:04:38,800 --> 00:04:47,800
So Atnopada, in regular cases, Atnopada will only be used for passive sentences.

66
00:04:47,800 --> 00:04:52,800
And by passive, we would mean by me it is done, for example.

67
00:04:52,800 --> 00:04:55,800
Let's give an example here.

68
00:05:23,800 --> 00:05:26,800
Okay, we have these two.

69
00:05:26,800 --> 00:05:31,800
We have katu watcha kah, which we're going to learn about later, and kama watcha kah.

70
00:05:31,800 --> 00:05:36,800
Watcha kah is who is doing the, you know, watcha kah, what does it mean?

71
00:05:36,800 --> 00:05:38,800
Who is doing the speaking?

72
00:05:38,800 --> 00:05:41,800
But this is passive and active.

73
00:05:41,800 --> 00:05:42,800
Katu means the doer.

74
00:05:42,800 --> 00:05:49,800
So it's speech, taking the doer as the subject, and kama watcha kah means taking that,

75
00:05:49,800 --> 00:05:52,800
which is active upon as the subject.

76
00:05:52,800 --> 00:05:56,800
So let's show you what I mean.

77
00:05:56,800 --> 00:06:05,800
Sudhara oudhanam pachati.

78
00:06:05,800 --> 00:06:08,800
Sudhara means chef or cook.

79
00:06:08,800 --> 00:06:10,800
Oudhanam means rice.

80
00:06:10,800 --> 00:06:13,800
And pachati means he share it cooks.

81
00:06:13,800 --> 00:06:16,800
The root is pach, which is in regards to cooking.

82
00:06:16,800 --> 00:06:19,800
Pachati means he share it cooks.

83
00:06:19,800 --> 00:06:23,800
It's a first, a third person singular form of pach.

84
00:06:23,800 --> 00:06:27,800
What amada?

85
00:06:27,800 --> 00:06:29,800
So this is an active sentence.

86
00:06:29,800 --> 00:06:31,800
The subject of the sentence is the doer.

87
00:06:31,800 --> 00:06:33,800
Katu, katu means doer.

88
00:06:33,800 --> 00:06:36,800
So the doer here is the subject of the sentence.

89
00:06:36,800 --> 00:06:40,800
That seems obvious, and this shouldn't cause much trouble.

90
00:06:40,800 --> 00:06:44,800
But common in pali, which is not nearly as common in English,

91
00:06:44,800 --> 00:06:47,800
is this kind of sentence.

92
00:06:47,800 --> 00:07:14,800
Okay, I think Oudhanah is in neutral, which makes it Oudhanam.

93
00:07:14,800 --> 00:07:29,800
I don't think, or maybe you do have Oudhanah, just give me a second here.

94
00:07:29,800 --> 00:07:51,800
Yes, sir, it should be Oudhanah.

95
00:07:51,800 --> 00:07:58,800
Okay, so it actually is masculine.

96
00:07:58,800 --> 00:08:13,800
Right, so pachati at the end.

97
00:08:13,800 --> 00:08:23,800
Okay, the first sentence is active.

98
00:08:23,800 --> 00:08:34,800
Second sentence is passive, pseudo, the cook, pachati, cooks, Oudhanam, the rice.

99
00:08:34,800 --> 00:08:40,800
The second sentence, Sudhanam, means buy the cook, Oudhanah, the rice.

100
00:08:40,800 --> 00:08:45,800
But chia tae is cooked.

101
00:08:45,800 --> 00:08:52,800
So the first one uses the ending t, which is a parasapada ending under the pea, as you noticed.

102
00:08:52,800 --> 00:08:56,800
And the second sentence uses the tae ending.

103
00:08:56,800 --> 00:08:59,800
Tae is up to an Oudhanah.

104
00:08:59,800 --> 00:09:05,800
You'll also see that the root stem of the verb has changed.

105
00:09:05,800 --> 00:09:15,800
And this is because it is in a passive sentence, you add yia.

106
00:09:15,800 --> 00:09:22,800
I think you add yia.

107
00:09:22,800 --> 00:09:26,800
Yeah, we'll see below exactly how it works.

108
00:09:26,800 --> 00:09:35,800
But you add to the root, bach, you add yia, and then you add the ending for passive.

109
00:09:35,800 --> 00:09:39,800
Or sometimes, no, that's right, yia, you add yia for passive.

110
00:09:39,800 --> 00:09:46,800
So not only are you changing the ending to adenopada, but you're also adding yia in before.

111
00:09:46,800 --> 00:09:50,800
That's just a sign of the passive.

112
00:09:50,800 --> 00:09:52,800
Point here is that we're using different endings.

113
00:09:52,800 --> 00:10:00,800
So in a following, when we're following the rules, which often you'll see we want.

114
00:10:00,800 --> 00:10:02,800
This is how it's going to look.

115
00:10:02,800 --> 00:10:05,800
The dae endings will only be used for passive sentences.

116
00:10:05,800 --> 00:10:11,800
The passive sentences are common, but this is how they will be formed.

117
00:10:11,800 --> 00:10:18,800
They'll be formed with the adenopada endings.

118
00:10:18,800 --> 00:10:22,800
When we're not following the rules, you can't really tell which ending.

119
00:10:22,800 --> 00:10:27,800
You can predict which ending is going to be used.

120
00:10:27,800 --> 00:10:39,800
Except to say that barasapada endings, the first sentence type are more common.

121
00:10:39,800 --> 00:10:44,800
So along and short of it is you have to learn both endings.

122
00:10:44,800 --> 00:10:53,800
Put more emphasis on the barasapada endings, because we're going to come across them more often.

123
00:10:53,800 --> 00:10:57,800
So those two types of sentences we're going to learn about later.

124
00:10:57,800 --> 00:11:03,800
Not so important right now, but it's good to know the difference.

125
00:11:03,800 --> 00:11:14,800
That's why we use P&A.

126
00:11:14,800 --> 00:11:24,800
Okay, so that's P&A.

127
00:11:24,800 --> 00:11:26,800
Purisa should also be quite clear.

128
00:11:26,800 --> 00:11:32,800
Purisa is the third person, second person, first person, which we have in each paradigm.

129
00:11:32,800 --> 00:11:37,800
And again, the person in the verb will agree with the subject, obviously.

130
00:11:37,800 --> 00:11:42,800
If it's you doing something, if you're saying you will go, then you use second person.

131
00:11:42,800 --> 00:11:45,800
Then he's here, it went or goes.

132
00:11:45,800 --> 00:11:48,800
Then you use third person.

133
00:11:48,800 --> 00:11:51,800
If you're saying I go, then it's first person.

134
00:11:51,800 --> 00:11:54,800
The only thing to note is that they're backwards in Pauli.

135
00:11:54,800 --> 00:11:58,800
In Pauli, the first person, and that's the name of it.

136
00:11:58,800 --> 00:12:04,800
Put them up, Purisa, the first person, is equivalent to the third person in English.

137
00:12:04,800 --> 00:12:06,800
What do we call the third person?

138
00:12:06,800 --> 00:12:07,800
Is the first person?

139
00:12:07,800 --> 00:12:12,800
What do we call second person and second person when we call first person?

140
00:12:12,800 --> 00:12:13,800
Is third person?

141
00:12:13,800 --> 00:12:15,800
So third and first are switched.

142
00:12:15,800 --> 00:12:18,800
It's backwards in relation to the English.

143
00:12:18,800 --> 00:12:23,800
Something to keep in mind, although you'll rarely have to deal with those terms.

144
00:12:23,800 --> 00:12:50,800
It's not like they're going to show up in translations, but the grammar is just right opposite.

145
00:12:50,800 --> 00:12:54,800
Okay, now we'll get into something called vodka.

146
00:12:54,800 --> 00:13:01,800
We actually just got into it, but now let's get deeply into this idea of vodka.

147
00:13:01,800 --> 00:13:27,800
This is on page 50.

148
00:13:31,800 --> 00:13:39,800
Okay, it turns out that we've just learned about two types of vodka.

149
00:13:39,800 --> 00:13:46,800
Active and passive got to where the subject is the doer and kama, which where the subject

150
00:13:46,800 --> 00:13:51,800
is the thing that is not an action is performed upon.

151
00:13:51,800 --> 00:13:55,800
So it's the object of the verb.

152
00:13:55,800 --> 00:14:01,800
Those are pretty simple, we've just learned them above, and the form of the active, the form of the passive,

153
00:14:01,800 --> 00:14:03,800
the form of the active is simple, you put.

154
00:14:03,800 --> 00:14:14,800
Look at these examples.

155
00:14:14,800 --> 00:14:22,800
If you, for active, you put the subject in one one or one two in the first declension.

156
00:14:22,800 --> 00:14:27,800
Put the object in the second declension, and then the verb, simple.

157
00:14:27,800 --> 00:14:36,800
That's the most simple sentence you can have, pseudo-ordinambachati.

158
00:14:36,800 --> 00:14:42,800
And it uses the, as I've said, uses the parasapada ending.

159
00:14:42,800 --> 00:14:51,800
We're going to learn in the next section, we'll learn about the reason why the verb forms of one and two are different.

160
00:14:51,800 --> 00:14:53,800
Touch them one, two, three, four, and five.

161
00:14:53,800 --> 00:14:58,800
They all have different forms of the verb, not just the ending.

162
00:14:58,800 --> 00:15:00,800
Anyway.

163
00:15:00,800 --> 00:15:07,800
The form of a second of a kama vodka is to put the subjects in three, right?

164
00:15:07,800 --> 00:15:12,800
In third, because it's by the cook.

165
00:15:12,800 --> 00:15:18,800
This sentence says the rice is cooked, but chia tae means is cooked.

166
00:15:18,800 --> 00:15:21,800
The rice is cooked or was cooked.

167
00:15:21,800 --> 00:15:24,800
And so the subject has to go into the three one.

168
00:15:24,800 --> 00:15:30,800
And this is a bit awkward to say, we would never really say this kind of thing in English.

169
00:15:30,800 --> 00:15:35,800
Well, unless someone asked who it was cooked by, we'd say, oh, it was cooked by the cook.

170
00:15:35,800 --> 00:15:42,800
Well, it would instead say he cooked it or she cooked it.

171
00:15:42,800 --> 00:15:48,800
Well, Pauli is very common to have these passive types of sentences.

172
00:15:48,800 --> 00:15:54,800
It's much more common than in English and in places where it would be awkward to do so in English.

173
00:15:54,800 --> 00:15:56,800
But that's it, three, one.

174
00:15:56,800 --> 00:15:57,800
So that's pretty simple.

175
00:15:57,800 --> 00:16:01,800
The third, the third's type of sentence is very rare.

176
00:16:01,800 --> 00:16:06,800
You do come across it, it does exist, but not in standard sentences.

177
00:16:06,800 --> 00:16:13,800
It just literally means that something exists or something is.

178
00:16:13,800 --> 00:16:21,800
So tae naboyate means by him, it is existed means he exists.

179
00:16:21,800 --> 00:16:22,800
Babawajic.

180
00:16:22,800 --> 00:16:25,800
Babawam means existence.

181
00:16:25,800 --> 00:16:27,800
So it's speech about existence.

182
00:16:27,800 --> 00:16:32,800
There's no subject.

183
00:16:32,800 --> 00:16:40,800
Yeah, that's not the description.

184
00:16:40,800 --> 00:16:42,800
Dae naboyate.

185
00:16:42,800 --> 00:16:47,800
It's a very weird form from an English perspective.

186
00:16:47,800 --> 00:16:53,800
Numbers four and five are causative forms.

187
00:16:53,800 --> 00:16:58,800
Positive means the subject causes someone else to do something.

188
00:16:58,800 --> 00:17:02,800
We learned about these in our translation of Jacobala.

189
00:17:02,800 --> 00:17:05,800
Remember how the rich man got people to do things.

190
00:17:05,800 --> 00:17:07,800
He didn't do them himself.

191
00:17:07,800 --> 00:17:13,800
The verbs forms indicated that he was getting someone else to do these things.

192
00:17:13,800 --> 00:17:17,800
You see this in these, look at the verbs in number four and five.

193
00:17:17,800 --> 00:17:23,800
But it's been lengthened and there's an A there.

194
00:17:23,800 --> 00:17:28,800
Which actually comes from me within the end with a dot under it, right?

195
00:17:28,800 --> 00:17:29,800
The end with a dot under it.

196
00:17:29,800 --> 00:17:33,800
All it does is lengthen the root.

197
00:17:33,800 --> 00:17:42,800
And number five is adding apia, napea, napea.

198
00:17:42,800 --> 00:17:46,800
Which is hate to come.

199
00:17:46,800 --> 00:17:52,800
So number four is where the subject of the sentence is the person who causes

200
00:17:52,800 --> 00:17:54,800
someone else to do something.

201
00:17:54,800 --> 00:17:58,800
And the verb will change as a result as you can see.

202
00:17:58,800 --> 00:18:09,800
Number five is speech about the thing where the subject is the thing.

203
00:18:09,800 --> 00:18:11,800
Now this is translated wrong.

204
00:18:11,800 --> 00:18:16,800
Anyway, this subject about the thing that is being that someone is causing someone else to work upon.

205
00:18:16,800 --> 00:18:19,800
I think this is a pretty rare form.

206
00:18:19,800 --> 00:18:24,800
It can't take off hand or this is actually used, but it must be used in places.

207
00:18:24,800 --> 00:18:50,800
Let's re-translate it because that's a wrong translation.

208
00:18:50,800 --> 00:18:55,800
The rice is caused to be cooked by the cook.

209
00:18:55,800 --> 00:19:03,800
Yeah, while he's translating this kind of nothing to really list to it literally.

210
00:19:03,800 --> 00:19:22,800
So number four as well.

211
00:19:22,800 --> 00:19:31,800
So number four is the lord, the master, maybe you can say in this case it could just be the head of the house.

212
00:19:31,800 --> 00:19:35,800
It causes the rice to be cooked by the cook.

213
00:19:35,800 --> 00:19:40,800
Some may go the lord.

214
00:19:40,800 --> 00:19:45,800
Ordinal.

215
00:19:45,800 --> 00:19:48,800
Ordinal, sorry.

216
00:19:48,800 --> 00:20:02,800
The cause is the rice, but the to be cooked so down.

217
00:20:02,800 --> 00:20:04,800
Is that correct?

218
00:20:04,800 --> 00:20:09,800
In that case it would be the lord causes the cook to cook the rice.

219
00:20:09,800 --> 00:20:19,800
It's coming in second here.

220
00:20:39,800 --> 00:20:54,800
Okay, the lord causes the cook to cook the rice.

221
00:20:54,800 --> 00:20:57,800
It's how it should be.

222
00:20:57,800 --> 00:21:02,800
Maybe that was how it was.

223
00:21:02,800 --> 00:21:12,800
Sorry, the lord causes the cook to cook the rice, so they hear the cook is the second object of the sentence.

224
00:21:12,800 --> 00:21:24,800
So kind of a weird sentence, the lord causes the cook to cook the rice.

225
00:21:24,800 --> 00:21:34,800
Number five, by the lord, the rice, ordinal, is caused to be cooked by the cook.

226
00:21:34,800 --> 00:21:36,800
You've got to buy.

227
00:21:36,800 --> 00:21:43,800
It's an awkward one to translate unless you convert it to a passive voice.

228
00:21:43,800 --> 00:21:47,800
So number five is passive, but also it's passive causative.

229
00:21:47,800 --> 00:21:56,800
So the owner of the subject is not the door, the subject of this sentence is that which is being acted upon.

230
00:22:47,800 --> 00:22:55,800
Okay.

231
00:22:55,800 --> 00:23:18,800
Okay.

232
00:23:25,800 --> 00:23:51,800
Okay, so based on what's it got there.

233
00:23:51,800 --> 00:24:01,800
And different types of suffix.

234
00:24:01,800 --> 00:24:08,800
So what you're going to have is you're going to have the root in this case, butch, right?

235
00:24:08,800 --> 00:24:15,800
Then you're going to have the suffix, which we're dealing with now.

236
00:24:15,800 --> 00:24:19,800
We've actually learned a little bit about the suffix is already until it's played.

237
00:24:19,800 --> 00:24:23,800
And then you have the ending.

238
00:24:23,800 --> 00:24:27,800
So in practice, there are three parts to the verb.

239
00:24:27,800 --> 00:24:36,800
There's the root, the suffix, and then another suffix, which is the declension.

240
00:24:36,800 --> 00:24:40,800
What we're talking about here is terms of suffix is actually in the middle.

241
00:24:40,800 --> 00:24:48,800
It's in between the root and the actual declension suffix.

242
00:24:48,800 --> 00:24:53,800
When we learned about roots, we learned that the different groups of roots,

243
00:24:53,800 --> 00:25:00,800
if you remember from last week, have different suffixes.

244
00:25:00,800 --> 00:25:01,800
Right.

245
00:25:01,800 --> 00:25:09,800
So the first group you add up, right, butch.

246
00:25:09,800 --> 00:25:15,800
Butch is in the first group, is that correct?

247
00:25:15,800 --> 00:25:18,800
Butch is in the first group, so you add up.

248
00:25:18,800 --> 00:25:23,800
And then on top of, you add the declension, t.

249
00:25:23,800 --> 00:25:26,800
So you get patchity.

250
00:25:26,800 --> 00:25:35,800
For patchity logic, what it means uses them all should mean uses these suffixes above,

251
00:25:35,800 --> 00:25:37,800
based on the group.

252
00:25:37,800 --> 00:25:39,800
One, two, three, four, five, six, seven, eight.

253
00:25:39,800 --> 00:25:43,800
That's supposed to be referring to the eight types of root.

254
00:25:43,800 --> 00:25:48,800
So the first group uses the second group, you add a,

255
00:25:48,800 --> 00:25:52,800
and also add on in there.

256
00:25:52,800 --> 00:25:56,800
Like rich becomes rich, right.

257
00:25:56,800 --> 00:26:20,800
And then, leap becomes a leap, right a second.

258
00:26:20,800 --> 00:26:32,800
Yeah, and he's got this all mixed up.

259
00:26:32,800 --> 00:26:33,800
Or just like that.

260
00:26:33,800 --> 00:26:34,800
The second group, sorry.

261
00:26:34,800 --> 00:26:36,800
The second group just adds up.

262
00:26:36,800 --> 00:26:41,800
So you reach becomes rich, t.

263
00:26:41,800 --> 00:26:43,800
Much becomes much, t.

264
00:26:43,800 --> 00:26:44,800
And so on.

265
00:26:44,800 --> 00:26:49,800
They're also adding up in the second group.

266
00:26:49,800 --> 00:26:52,800
The third group adds up.

267
00:26:52,800 --> 00:27:01,800
So, yeah, which often changes to conform with the other one.

268
00:27:01,800 --> 00:27:08,800
Like sib becomes sibity, right.

269
00:27:08,800 --> 00:27:10,800
Sib, sib becomes sibity.

270
00:27:10,800 --> 00:27:12,800
Sib becomes sibity.

271
00:27:12,800 --> 00:27:15,800
Key becomes keyity.

272
00:27:15,800 --> 00:27:17,800
So you add, yeah, and then the ending.

273
00:27:17,800 --> 00:27:20,800
This each group has its own suffix.

274
00:27:20,800 --> 00:27:28,800
After the suffix, you add another suffix, the declension.

275
00:27:28,800 --> 00:27:32,800
But these are only used for katuajaka and hateu kamawajaka,

276
00:27:32,800 --> 00:27:35,800
the first and the fifth one.

277
00:27:35,800 --> 00:27:39,800
The second form, the passive form kamawajaka,

278
00:27:39,800 --> 00:27:41,800
doesn't use those.

279
00:27:41,800 --> 00:27:43,800
Instead, it adds, yeah.

280
00:27:43,800 --> 00:27:45,800
And so if you look at the example above,

281
00:27:45,800 --> 00:27:49,800
you get that's why you get fudge, ea, day.

282
00:27:49,800 --> 00:27:50,800
This is important.

283
00:27:50,800 --> 00:27:53,800
And this is a little bit, must be a little bit grueling to

284
00:27:53,800 --> 00:27:56,800
have to learn all these intricate details.

285
00:27:56,800 --> 00:28:00,800
But it's important that you're able to recognize ea

286
00:28:00,800 --> 00:28:03,800
to show you that this is a passive sentence.

287
00:28:03,800 --> 00:28:07,800
To help you figure out, because verbs are much harder to pick

288
00:28:07,800 --> 00:28:10,800
apart than nouns.

289
00:28:10,800 --> 00:28:15,800
So if you can remember all these verbs,

290
00:28:15,800 --> 00:28:18,800
remember all these, put together all these parts logically,

291
00:28:18,800 --> 00:28:21,800
then you can look at a verb like, butchia day.

292
00:28:21,800 --> 00:28:24,800
And you can see, okay, the root is butch.

293
00:28:24,800 --> 00:28:28,800
Ea is what we would use for a passive.

294
00:28:28,800 --> 00:28:32,800
And day is also a passive, usually a passive ending.

295
00:28:32,800 --> 00:28:36,800
So here you've got a passive third person singular form of

296
00:28:36,800 --> 00:28:41,800
a verb, butch means is cooked.

297
00:28:41,800 --> 00:28:43,800
I'm a logical, it's pretty easy.

298
00:28:43,800 --> 00:28:47,800
So you wouldn't add the, you take the directly onto the

299
00:28:47,800 --> 00:28:52,800
root and you add, yeah.

300
00:28:52,800 --> 00:28:55,800
But we're logical and directly onto the root.

301
00:28:55,800 --> 00:28:58,800
You put, yeah, that's why you have boo, which is just the

302
00:28:58,800 --> 00:29:01,800
root and yet day.

303
00:29:01,800 --> 00:29:06,800
Day is again, I don't know whether.

304
00:29:06,800 --> 00:29:14,800
Babawajika uses, I don't know whether it seems.

305
00:29:14,800 --> 00:29:23,800
He took it to logical, also doesn't use according to this.

306
00:29:23,800 --> 00:29:28,800
Doesn't use the group endings.

307
00:29:28,800 --> 00:29:51,800
Instead, it adds nae nae nae nae nae nae nae nae nae nae nae nae naeei nae nae nae nae nae.

308
00:29:51,800 --> 00:29:56,800
Most commonly me and nape.

309
00:29:56,800 --> 00:29:59,800
Nah, nape, nape, nape.

310
00:29:59,800 --> 00:30:05,800
I don't think of them being as common.

311
00:30:05,800 --> 00:30:11,800
It's usually how you're going to recognize a causative verb is the A,

312
00:30:11,800 --> 00:30:15,800
whether it's a, or just a.

313
00:30:15,800 --> 00:30:19,800
Most verbs don't go 80.

314
00:30:19,800 --> 00:30:23,800
Now, there is the exception as the 10th form, and it's a fairly big exception.

315
00:30:23,800 --> 00:30:25,800
Ginteti, you'll see often.

316
00:30:25,800 --> 00:30:29,800
If you look at the last group of verbs of roots,

317
00:30:29,800 --> 00:30:33,800
group 8,

318
00:30:33,800 --> 00:30:37,800
also adds a lot of the time.

319
00:30:37,800 --> 00:30:39,800
So ginteti, joreti,

320
00:30:39,800 --> 00:30:41,800
monteti.

321
00:30:41,800 --> 00:30:43,800
But I didn't have that.

322
00:30:43,800 --> 00:30:47,800
If an ordinary verb, a verb that's not in the group number 8,

323
00:30:47,800 --> 00:30:52,800
has a, a before the suffix, like 80 or 80 or something like that.

324
00:30:52,800 --> 00:30:54,800
A, b, t.

325
00:30:54,800 --> 00:30:57,800
Then you think it's causative.

326
00:30:57,800 --> 00:31:01,800
If you go back to the beginning of the Chacupala, you'll see that.

327
00:31:01,800 --> 00:31:05,800
That all those verbs were a, or a.

328
00:31:05,800 --> 00:31:19,800
Here, tukama vajakayu has its own ending's name, a, napia, napia.

329
00:31:19,800 --> 00:31:29,800
Plus, here, tukama vajakayu says he uses the ordinary suffixes,

330
00:31:29,800 --> 00:31:41,800
the one that they, for each of the 8, 8 groups.

331
00:31:41,800 --> 00:31:43,800
So ginteti.

332
00:31:43,800 --> 00:31:46,800
If you look at number 5, the example,

333
00:31:46,800 --> 00:31:50,800
samikayana, sunehana, hodana, pajapia,

334
00:31:50,800 --> 00:31:55,800
that is number 5 is not that common that I can think of.

335
00:31:55,800 --> 00:31:57,800
Number 4 is fairly common.

336
00:31:57,800 --> 00:31:59,800
Number 1 and 2 are very common.

337
00:31:59,800 --> 00:32:07,800
Number 1 is, of course, the most common.

338
00:32:07,800 --> 00:32:08,800
And that's it.

339
00:32:08,800 --> 00:32:12,800
That's all that this textbook covers.

340
00:32:12,800 --> 00:32:19,800
Your homework is to learn enough, probably,

341
00:32:19,800 --> 00:32:26,800
to translate all the sentences on page 51.

342
00:32:26,800 --> 00:32:30,800
And I'm quite sure that I haven't prepared you for that.

343
00:32:30,800 --> 00:32:34,800
So, next week we'll go through them.

344
00:32:34,800 --> 00:32:38,800
But I'd like you all to try and prepare yourself

345
00:32:38,800 --> 00:32:40,800
and figure it out on your own because I, you know,

346
00:32:40,800 --> 00:32:45,800
I haven't been able to do this good of a job.

347
00:32:45,800 --> 00:32:50,800
Or, sorry, when they have that strong,

348
00:32:50,800 --> 00:32:52,800
I'm going to have to go through these and make sure

349
00:32:52,800 --> 00:32:54,800
that you actually put them down.

350
00:32:54,800 --> 00:32:56,800
But that's wrong.

351
00:32:56,800 --> 00:33:24,800
I think some in here, as well.

352
00:33:26,800 --> 00:33:37,800
I'm just going to do it now.

353
00:33:37,800 --> 00:33:39,800
But I don't actually have names.

354
00:33:39,800 --> 00:33:41,800
But, you know, can this pass that back,

355
00:33:41,800 --> 00:33:44,800
that's okay.

356
00:33:44,800 --> 00:34:04,800
There must be some in here in there.

357
00:34:04,800 --> 00:34:15,800
Okay.

358
00:34:15,800 --> 00:34:43,800
I don't know about that.

359
00:34:43,800 --> 00:34:48,800
I don't know about that.

360
00:34:48,800 --> 00:35:15,800
What the heck does that mean?

361
00:35:18,800 --> 00:35:39,800
I don't know about that.

362
00:35:39,800 --> 00:36:03,800
I have to find the book that is around.

363
00:36:03,800 --> 00:36:13,800
Thank you so much.

364
00:36:33,800 --> 00:36:54,800
Thank you.

365
00:37:03,800 --> 00:37:22,800
Okay, I found it.

366
00:37:22,800 --> 00:37:41,800
Okay.

367
00:37:41,800 --> 00:38:00,800
I found it.

368
00:38:11,800 --> 00:38:18,800
Why don't we go through these then?

369
00:38:18,800 --> 00:38:24,800
Why don't you guys just start translating these?

370
00:38:24,800 --> 00:38:26,800
Ask me questions.

371
00:38:26,800 --> 00:38:31,800
Would that work?

372
00:38:31,800 --> 00:38:46,800
Why don't we talk this through everyone, try to work these out, pull out a piece of paper, and ask me questions on them.

373
00:39:01,800 --> 00:39:22,800
In the first sentence is the second word, the two-one version of Dhamma.

374
00:39:22,800 --> 00:39:27,800
That's correct.

375
00:39:27,800 --> 00:39:34,800
Do you have the digital poly reader installed?

376
00:39:34,800 --> 00:39:43,800
Would that be your recommendation for things like this?

377
00:39:43,800 --> 00:39:50,800
Yes.

378
00:39:50,800 --> 00:39:56,800
But do you say it doesn't mean speak, do you say it means teachers or preaches?

379
00:40:20,800 --> 00:40:27,800
You can just ask me what things mean, I can give you dictionary.

380
00:40:27,800 --> 00:40:53,800
Obviously you're not going to have all that.

381
00:40:57,800 --> 00:41:16,800
Thank you.

382
00:41:27,800 --> 00:41:34,800
Thank you.

383
00:41:34,800 --> 00:41:43,800
Thank you.

384
00:41:43,800 --> 00:42:06,800
Thank you.

385
00:42:06,800 --> 00:42:35,800
Thank you.

386
00:42:35,800 --> 00:42:54,800
The point here is that this is passive, so the Dhamma is not being known.

387
00:42:54,800 --> 00:43:13,800
The point may be right.

388
00:43:13,800 --> 00:43:32,800
By the Dhamma, the Dhamma is becoming visible.

389
00:43:32,800 --> 00:43:47,800
Thank you.

390
00:44:02,800 --> 00:44:23,800
Thank you.

391
00:44:32,800 --> 00:44:53,800
Thank you.

392
00:45:02,800 --> 00:45:23,800
Thank you.

393
00:45:33,800 --> 00:45:39,800
Thank you.

394
00:45:39,800 --> 00:45:42,800
That's interesting.

395
00:45:42,800 --> 00:45:51,800
I'm not actually sure which one that means.

396
00:45:51,800 --> 00:45:57,800
It's the tiny translation.

397
00:45:57,800 --> 00:46:07,800
This type of sentence is being made clear.

398
00:46:07,800 --> 00:46:14,800
The Dhamma is being made clear.

399
00:46:14,800 --> 00:46:31,800
The Taya says Thamma is making it clear, but it means broadcasting in Taya.

400
00:46:31,800 --> 00:46:52,800
It means to make clear or to become clear.

401
00:46:52,800 --> 00:47:03,800
But I'm not actually sure about this one.

402
00:47:03,800 --> 00:47:18,800
I want to say that this is actually saying that this Dhamma is teaching the Dhamma, making it clear to others.

403
00:47:18,800 --> 00:47:39,800
I think it's the 7-1 version of, but I don't know what KEN is.

404
00:47:39,800 --> 00:47:53,800
It's made by a non-profit foundation.

405
00:47:53,800 --> 00:48:19,800
I mean, it's better for me because Chrome does weird things, but Chrome is more sophisticated.

406
00:48:19,800 --> 00:48:29,800
I don't know how to use that, but I always forget to use Firefox.

407
00:48:29,800 --> 00:48:53,800
Philosophically Firefox is probably more philosophically sound.

408
00:48:53,800 --> 00:49:01,800
I stopped using them because it became really hard to watch anything on YouTube from Firefox for whatever reason.

409
00:49:01,800 --> 00:49:05,800
It was as if Google was just forcing people to use their own product to watch YouTube.

410
00:49:05,800 --> 00:49:29,800
They haven't been sneaky like that, but you shouldn't be using Flash anymore anyway.

411
00:49:29,800 --> 00:49:35,800
In Firefox respects your privacy and it's very, you know, they have no ulterior motives.

412
00:49:35,800 --> 00:49:36,800
It's below here.

413
00:49:36,800 --> 00:49:42,800
When you're on the internet, that can be important.

414
00:49:42,800 --> 00:49:44,800
Anyway, that's not the real important.

415
00:49:44,800 --> 00:49:53,800
I mean, if you want to use Chrome, I'm not saying it's not my job to talk about that, but just use Firefox for the digital file reader.

416
00:49:53,800 --> 00:49:55,800
You can use both.

417
00:49:55,800 --> 00:49:58,800
I know people are doing that.

418
00:49:58,800 --> 00:49:59,800
That sounds good.

419
00:49:59,800 --> 00:50:00,800
Thank you, Bante.

420
00:50:00,800 --> 00:50:04,800
Okay, so you're wrong.

421
00:50:04,800 --> 00:50:12,800
This is actually a case of Sunday.

422
00:50:12,800 --> 00:50:16,800
Feminine forms are less common than masculine.

423
00:50:16,800 --> 00:50:20,800
More words, I think, are masculine or neutered.

424
00:50:20,800 --> 00:50:25,800
So don't immediately assume that something is going to be a feminine 7-1.

425
00:50:25,800 --> 00:50:29,800
It's less likely.

426
00:50:29,800 --> 00:50:36,800
You'll start to recognize our words like gain, and words like a young.

427
00:50:36,800 --> 00:50:43,800
Now, if I had made you memorize all the paradigms, you would be clearer on a young.

428
00:50:43,800 --> 00:50:48,800
And you're going to have to become clearer on a young because it's a 1-1 form of yida.

429
00:50:48,800 --> 00:50:56,800
Which just means this.

430
00:50:56,800 --> 00:51:05,800
If you look at our paradigms for the pronouns, you'll have yida, which starts with a young.

431
00:51:05,800 --> 00:51:20,800
So a young is actually a pronoun that motivates tuple.

432
00:52:05,800 --> 00:52:31,800
Anyway, I think I'll just leave these with you guys then.

433
00:52:31,800 --> 00:52:40,800
You can go through and get as many as you can, and what you can't we can work on next week.

434
00:52:40,800 --> 00:52:53,800
And then we'll do some more translation and we'll figure out where to go from there.

435
00:52:53,800 --> 00:52:56,800
Okay, so let's end there.

436
00:52:56,800 --> 00:53:01,800
Thanks everyone for coming.

437
00:53:01,800 --> 00:53:03,800
Have yourselves in a good week.

438
00:53:03,800 --> 00:53:06,800
Sadubante, thank you.

439
00:53:06,800 --> 00:53:34,800
Thank you very much.

440
00:53:34,800 --> 00:53:37,800
We're having a meeting at four, right?

441
00:53:37,800 --> 00:53:43,800
So, should I meant to tell everyone in case that I just wanted to get involved?

442
00:53:43,800 --> 00:53:46,800
That's okay.

443
00:53:46,800 --> 00:53:55,800
Anyway, that's all for Paul.

