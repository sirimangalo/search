1
00:00:00,000 --> 00:00:03,960
Hello, welcome back to Askremunk.

2
00:00:03,960 --> 00:00:11,080
This is a response in regards to the question as to whether or not Buddhists must be

3
00:00:11,080 --> 00:00:15,840
vegetarian or what are my thoughts on vegetarianism.

4
00:00:15,840 --> 00:00:24,240
And this comes at an interesting time just as I was intending to reply to this question.

5
00:00:24,240 --> 00:00:35,240
That's now come up in the news that the founder of Facebook, Mark Zuckerberg, has adopted

6
00:00:35,240 --> 00:00:46,600
the philosophy that we're often confronted with that if one is going to consume the

7
00:00:46,600 --> 00:00:56,400
flesh of other animals then one should undertake to take the life of those animals oneself.

8
00:00:56,400 --> 00:01:04,000
So one should be the killer rather than as the theory goes allowing someone else to

9
00:01:04,000 --> 00:01:06,400
kill for you.

10
00:01:06,400 --> 00:01:22,160
So this makes this question quite topical or quite timely.

11
00:01:22,160 --> 00:01:30,760
So just to give my thoughts on vegetarianism it's not a simple discussion and this isn't

12
00:01:30,760 --> 00:01:40,960
a question in fact that should be taken lightly surprisingly it might seem like it's not

13
00:01:40,960 --> 00:01:52,920
a terribly important topic but it involves the very core issue of Buddhist ethics and

14
00:01:52,920 --> 00:02:00,560
an understanding of it requires an answer of this question requires an understanding

15
00:02:00,560 --> 00:02:11,160
of what exactly Buddhist ethics are and the sort of philosophy that the Buddha had.

16
00:02:11,160 --> 00:02:15,920
So first of all I want to say that this is probably one of those questions that's going

17
00:02:15,920 --> 00:02:22,800
to get negative responses from both sides from the people who believe Buddhists should

18
00:02:22,800 --> 00:02:29,800
be vegetarian and from those who believe not Buddhists but those who sort of believe

19
00:02:29,800 --> 00:02:41,040
that people like Mark Zuckerberg are correct that it's better to kill for you know better

20
00:02:41,040 --> 00:02:51,280
to be the person who does the killing rather than be hypocritical and eat meat but don't

21
00:02:51,280 --> 00:03:00,160
agree with killing if your stance is not that eating meat is okay then you are not therefore

22
00:03:00,160 --> 00:03:06,000
able to hold the stance that killing is not okay and therefore you should actually be

23
00:03:06,000 --> 00:03:12,320
better off to kill for yourself rather than happy to kill for you.

24
00:03:12,320 --> 00:03:20,120
So I don't know what to say that except that to be clear that I'm aware that this isn't

25
00:03:20,120 --> 00:03:24,280
going to be universally held and I don't want people to think that my view is the view

26
00:03:24,280 --> 00:03:37,160
held by all Buddhists or by all people who follow the Buddha's teaching or who consider

27
00:03:37,160 --> 00:03:41,360
themselves to be Buddhist let's put it that way.

28
00:03:41,360 --> 00:03:47,480
So let's talk we have to talk about two things here the first one is an understanding

29
00:03:47,480 --> 00:03:59,880
of Buddhist ethics, Buddhist ethics are based on ultimate reality and this shouldn't

30
00:03:59,880 --> 00:04:04,960
be a scary term as well if you've been following the meditation teachings that I've

31
00:04:04,960 --> 00:04:16,520
teach or that are presented by the Buddha himself what it means simply is the experience

32
00:04:16,520 --> 00:04:20,320
of reality around us in the present moment.

33
00:04:20,320 --> 00:04:26,920
So Buddhist ethics are not intellectual they're not passed down by some higher authority

34
00:04:26,920 --> 00:04:34,400
they're scientific something is unethical not because of specifically the suffering that

35
00:04:34,400 --> 00:04:42,400
it brings to the victim in the case where there is a victim or to other people they're

36
00:04:42,400 --> 00:04:51,040
not unethical because they because they call suffering they're unethical because they

37
00:04:51,040 --> 00:04:56,480
change the the mind of the person who wonder takes them they lead specifically they lead

38
00:04:56,480 --> 00:05:06,840
to one's own detriment something is ethical and simply because it by its very nature degrades

39
00:05:06,840 --> 00:05:21,280
the state of one's mind so when Mark Zuckerberg cuts the throat of a goat now immediately

40
00:05:21,280 --> 00:05:25,760
it comes to my mind is not the suffering that the goat is going through because that's

41
00:05:25,760 --> 00:05:30,480
actually relatively minor it's not a big deal to have your your throat slit that might

42
00:05:30,480 --> 00:05:37,000
seem like a horrific thing to have happened and indeed it is but relative to the state

43
00:05:37,000 --> 00:05:45,240
of mind of a person who would do that to another living being I find that far scarier

44
00:05:45,240 --> 00:05:51,680
and that's not a theoretical it's not an intellectual view it's one that comes from looking

45
00:05:51,680 --> 00:05:58,240
at my own history and the things that I've done and being able to compare them with

46
00:05:58,240 --> 00:06:03,160
these sorts of action I cringe at the thought of someone doing that to another living being

47
00:06:03,160 --> 00:06:11,920
and that comes from from cringing at my own actions and being able to see the well being

48
00:06:11,920 --> 00:06:16,560
able to see the the results of my own actions and how they've affected me and how wrong

49
00:06:16,560 --> 00:06:23,600
it was feeling feeling bad about them from the core of my being it comes from an ability

50
00:06:23,600 --> 00:06:30,200
to empathize which we gain through the meditation practice because someone who empathizes

51
00:06:30,200 --> 00:06:40,320
with the suffering of others will never do these things and it's only through the pushing

52
00:06:40,320 --> 00:06:46,440
aware the repression of our empathy for the suffering of others that were able to do these

53
00:06:46,440 --> 00:06:51,720
sorts of things it's a horrific thing to to to kill another living being and the fact

54
00:06:51,720 --> 00:07:00,080
that that's not clear to us is is just a sign that our mind that we haven't taken the

55
00:07:00,080 --> 00:07:06,360
time to look at the nature of our actions people who take an intellectual point of view

56
00:07:06,360 --> 00:07:11,120
will often see nothing wrong with killing and in fact will often postulate situations

57
00:07:11,120 --> 00:07:18,000
such as this where killing is the preferable method now when you look at it in this

58
00:07:18,000 --> 00:07:23,120
way and indeed if you you you should understand that this is how the Buddha would have us

59
00:07:23,120 --> 00:07:29,520
look at ethics then to compare the two situations before we talk about vegetarianism

60
00:07:29,520 --> 00:07:36,680
comparing the two situations one who a person who eats meat and a person who kills from

61
00:07:36,680 --> 00:07:41,560
this point of view now putting aside any any intellectual arguments and I'll try to get

62
00:07:41,560 --> 00:07:47,720
to that in a second simply from from the experiential point of view which is the true

63
00:07:47,720 --> 00:07:54,040
the proper view according to the Buddha whether you agree with it or not there it's a

64
00:07:54,040 --> 00:08:01,480
world of difference you can eat meat without any unwholesumness in your mind it's possible

65
00:08:01,480 --> 00:08:06,680
I think that that should go without saying that a person can be mindful of something

66
00:08:06,680 --> 00:08:13,960
you know no matter what it is putting it in your mouth swallowing it and and tasting

67
00:08:13,960 --> 00:08:21,320
it and so on and making use of the energy without having any malice or any any thoughts

68
00:08:21,320 --> 00:08:29,200
of causing suffering to any being at all and it's only when we become intellectual about

69
00:08:29,200 --> 00:08:36,040
it and when we make the the assumption for instance that more beings are going to have

70
00:08:36,040 --> 00:08:43,120
to die or someone is going to have to kill for us or you know I guess in the sense of people

71
00:08:43,120 --> 00:08:46,200
like Mark Zuckerberg and many people have this view that one should kill for one's

72
00:08:46,200 --> 00:08:57,280
up the idea that you don't you won't respect the significance of the act but really the

73
00:08:57,280 --> 00:09:04,800
person who kills cannot help but crush the you know experience a state of crushing and

74
00:09:04,800 --> 00:09:13,520
repressing or our natural empathy for each other as as living beings and will degrade

75
00:09:13,520 --> 00:09:17,960
the state of their mind and this is this is empirical this is something most people can't

76
00:09:17,960 --> 00:09:23,920
see I have a story that I always tell about when I was young and I was hunting with my

77
00:09:23,920 --> 00:09:31,240
father I wasn't always a Buddhist and I killed a deer actually once which is my claim

78
00:09:31,240 --> 00:09:39,720
to shame but it was very difficult it was a very difficult thing to to to pull the trigger

79
00:09:39,720 --> 00:09:44,600
which was strange because I had no qualms about it at the time I wasn't at all afraid

80
00:09:44,600 --> 00:09:54,960
of hurting other beings you know I had some empathy but not anything nearly as advances

81
00:09:54,960 --> 00:10:01,080
it should have been and so but nonetheless my whole body shook and it was very it was a difficult

82
00:10:01,080 --> 00:10:06,400
thing to do after that it became a lot easier after the first time as I got into it and

83
00:10:06,400 --> 00:10:13,160
got used to it became accustomed to it got a lot more difficult and the simply because of

84
00:10:13,160 --> 00:10:19,160
the repression that goes on and the degradation of the mind until you become numb to it

85
00:10:19,160 --> 00:10:26,840
it's also this is also something it's easy to see when you talk to to soldiers or when

86
00:10:26,840 --> 00:10:32,120
we take I took peace studies in university and we studied how we studied war so we studied

87
00:10:32,120 --> 00:10:37,720
how it is that you get people to kill and they found that in the first world war there

88
00:10:37,720 --> 00:10:47,760
was an incredibly low accuracy rate of you know for in terms of what do you call bullets

89
00:10:47,760 --> 00:10:54,480
shot and enemies killed or people killed and this was because they found out that most

90
00:10:54,480 --> 00:10:58,480
of the soldiers didn't want to kill their fellow human beings and they would shoot over

91
00:10:58,480 --> 00:11:04,760
each other's heads apparently this is a document in fact so this frustrated the leaders

92
00:11:04,760 --> 00:11:10,160
greatly and so they tried to figure out how it is that you can get your soldiers to kill

93
00:11:10,160 --> 00:11:18,120
how can you teach someone to kill and the way to do it is to destroy the empathy that

94
00:11:18,120 --> 00:11:23,240
they might have for the other the other human being by creating distance between them so

95
00:11:23,240 --> 00:11:28,800
this is why you have all these decorative derogatory terms for the enemy right why you

96
00:11:28,800 --> 00:11:35,560
hear that why we had in the early part of the last century a lot of propaganda and derogatory

97
00:11:35,560 --> 00:11:42,560
terms used to refer to other you know the enemies the you know the names for the Germans

98
00:11:42,560 --> 00:11:48,160
the names of the Japanese and so on and the movies that show them you know vicious

99
00:11:48,160 --> 00:11:54,960
and mean and cruel and trying to so they did everything they could to create a distance

100
00:11:54,960 --> 00:12:01,680
and destroy the ability to empathize with the other the other living being and they found

101
00:12:01,680 --> 00:12:08,480
that this help but they found that helped even more is when they started using more technologically

102
00:12:08,480 --> 00:12:14,400
advanced means of killing each other bombing because bombing then they they with the

103
00:12:14,400 --> 00:12:19,200
soldiers would say is it's just like a video game and so they had very little qualms about

104
00:12:19,200 --> 00:12:27,080
dropping bombs and so on I think this points quite clearly to to the the relationship

105
00:12:27,080 --> 00:12:34,360
between empathy and and the state of killing the active killing and how you how you need

106
00:12:34,360 --> 00:12:42,640
to crush your empathy in order to do it so I think people who use the computer a lot it

107
00:12:42,640 --> 00:12:49,280
gets easier for them because you're if you're not careful it's easy for you to build

108
00:12:49,280 --> 00:12:55,080
up intense states of concentration that are able to crush your your emotions crush your

109
00:12:55,080 --> 00:13:02,400
your sense of of of reality and empathy and so it's very easy to think logically that

110
00:13:02,400 --> 00:13:08,480
this is somehow a better way of doing things so okay that's on the one side but this also

111
00:13:08,480 --> 00:13:13,960
helps us to understand what the Buddhist answer to the question should we be vegetarians

112
00:13:13,960 --> 00:13:22,360
is the answer is no the answer is in regards to the requirement for being a vegetarian

113
00:13:22,360 --> 00:13:26,680
no there is no requirement and there shouldn't be a requirement and in fact I would

114
00:13:26,680 --> 00:13:32,280
postulate but this is actually a little bit of a different argument I would postulate

115
00:13:32,280 --> 00:13:41,680
that it's better not to be totally vegetarian what I would advocate is passive vegetarianism

116
00:13:41,680 --> 00:13:45,960
and that's in the sense of not desiring me but this is actually a different argument

117
00:13:45,960 --> 00:13:53,120
I want to finish the first argument the first argument is pointing out that in terms

118
00:13:53,120 --> 00:14:00,120
of the the empirical nature or the experiential nature of eating meat and killing it's

119
00:14:00,120 --> 00:14:05,640
two different completely different things a person who eats meat can do so free from guilt

120
00:14:05,640 --> 00:14:10,200
this is my understanding this is what we have in the earliest Buddhist texts that I'm

121
00:14:10,200 --> 00:14:17,640
aware of not in all Buddhist texts but this is the understanding that I follow and I think

122
00:14:17,640 --> 00:14:23,080
in my mind it's quite clear even though you know many people will disagree so the other

123
00:14:23,080 --> 00:14:39,160
argument that has to do with the the intellectual side of of eating meat so the argument

124
00:14:39,160 --> 00:14:48,760
goes that irregardless of the the states in the person's mind at the time that there

125
00:14:48,760 --> 00:15:00,280
isn't there is an ethical responsibility to to refrain from eating meat because animals will

126
00:15:00,280 --> 00:15:06,440
be killed so this is why people I think there's an understanding generally that you're

127
00:15:06,440 --> 00:15:13,000
not you're not eating meat viciously thinking you know die die die because you know it's

128
00:15:13,000 --> 00:15:18,600
already dead but the ethics that they're talking about which I submit is not really true Buddhist

129
00:15:18,600 --> 00:15:29,480
ethics it is that it's going to lead to beings being killed and if we don't eat meat and fewer

130
00:15:29,480 --> 00:15:35,480
beings will be killed now this is why I thought it's important to explain the nature of

131
00:15:35,480 --> 00:15:45,080
Buddhist ethics as as I follow it as I see it because this sort of ethics well this sort of

132
00:15:45,080 --> 00:15:54,280
ethics really doesn't hold up in my mind because all beings have to die as I said the real

133
00:15:54,280 --> 00:16:00,280
problem isn't that beings are killed or so on because we all have to die more suffering less

134
00:16:00,280 --> 00:16:06,280
suffering it is really all relative and it's really not the biggest problem because eventually

135
00:16:06,280 --> 00:16:12,280
we're all going to have to learn to understand and to let go and to not be affected by suffering so

136
00:16:13,240 --> 00:16:18,200
it's not necessarily the case that suffering is going to be a bad thing or something we have to

137
00:16:18,200 --> 00:16:24,680
worry about what we really have to worry about is the state of our minds so a person who

138
00:16:24,680 --> 00:16:34,680
eats meat does so with a pure mind a person who kills obviously does so with an impure mind

139
00:16:34,680 --> 00:16:40,920
and that's really where the difference lies a person who who eats meat is not thinking

140
00:16:43,800 --> 00:16:50,280
or has has no real connection with the the act of killing that goes on

141
00:16:50,280 --> 00:16:58,280
the idea that somehow you can stop beings from being killed is really a bit

142
00:16:59,640 --> 00:17:06,280
shortsighted I think considering the fact that when you look at the world around us the amount

143
00:17:06,280 --> 00:17:17,720
of killing that goes on is something in you know to an incredible the magnitude of the problem

144
00:17:17,720 --> 00:17:25,320
if you're a solution to or if your goal is to find a solution to the problem of killing

145
00:17:26,120 --> 00:17:31,480
then you know just stepping out your door is going to cause difficult for you because

146
00:17:31,480 --> 00:17:37,960
difficulty for you because of the you know the murder that goes on in every quarter by every

147
00:17:37,960 --> 00:17:44,360
type of being on on earth by most types of beings if you're not a predator you're prey

148
00:17:44,360 --> 00:17:51,640
yeah for the most part and so this points to the second part of the argument that Buddhism

149
00:17:52,280 --> 00:17:58,760
is a path towards simplicity this sort of intellectual argument has no place in Buddhism because

150
00:17:58,760 --> 00:18:04,360
we're not trying to change the world we're not trying to fix the world the world is by nature it's

151
00:18:04,360 --> 00:18:09,560
something that we've created out of our ignorance it's something that we've put together

152
00:18:09,560 --> 00:18:15,720
based on our craving based on our mission based on the idea that somehow we're going to

153
00:18:16,280 --> 00:18:25,400
create a stable and lasting state of existence and the with the Buddha realizes that you can't do

154
00:18:25,400 --> 00:18:32,600
it because everything is is unstable and changing and because as the more you try to cling to

155
00:18:32,600 --> 00:18:42,840
things is stable the more you create suffering this is another part of the argument the third

156
00:18:42,840 --> 00:18:49,480
thing that I would I wanted to say so the second is that we're not the sort of intellectual

157
00:18:49,480 --> 00:18:56,680
argument the point is that if you if you open up the can of worms of the intellectual

158
00:18:56,680 --> 00:19:04,920
responsibility to not eat meat then you'll never find a you'll never find rest because every part

159
00:19:04,920 --> 00:19:11,720
of your life in some way causes suffering for other beings when you drive a car you shouldn't drive

160
00:19:11,720 --> 00:19:18,600
a car because most likely the beings are going to die as a result of you driving a car you shouldn't

161
00:19:18,600 --> 00:19:24,600
use most beauty products or razor blades because they've all been tested on animals and there

162
00:19:24,600 --> 00:19:31,080
are people who who think like this but it goes to such you know there is no there is no line you

163
00:19:31,080 --> 00:19:39,880
can draw and I would submit that there there is no need to to find a line that the line is quite

164
00:19:39,880 --> 00:19:44,680
clear in in terms of where our intentions are and this is throughout the Buddha's teaching

165
00:19:44,680 --> 00:19:49,000
Buddha said when you step on an end if you didn't know you were stepping on the end you're not

166
00:19:49,000 --> 00:19:54,040
guilty of anything the point is what goes on in your mind because this is what's going to

167
00:19:54,040 --> 00:20:01,960
change who you are and this is what's going to affect your future it's what's going to to actually

168
00:20:01,960 --> 00:20:08,840
to to change the world because if our minds are pure we'll never will never have any desire to

169
00:20:08,840 --> 00:20:14,440
kill if our mind is pure what what will change the minds of other people we will help the people

170
00:20:14,440 --> 00:20:21,880
around us to to decide that killing is wrong and so on and so the problem will never arise the

171
00:20:21,880 --> 00:20:29,320
point is to stop people from wanting to kill not to stop people from wanting to to eat meat so

172
00:20:29,320 --> 00:20:33,960
but why I say I think passive vegetarian is vegetarianism is good and what that means

173
00:20:36,280 --> 00:20:42,920
there is a so this is the third part that passive vegetarianism I think is a good thing

174
00:20:42,920 --> 00:20:47,880
and I don't think this is nearly as important but it's something for us to think about

175
00:20:47,880 --> 00:20:57,240
because a person who wants to eat meat a person who is actively seeking out meat is doing something

176
00:20:57,240 --> 00:21:05,160
that is that should be we should all be aware of their guilty of of something and a fairly extreme

177
00:21:05,800 --> 00:21:11,960
form of karma of this type of karma the type of karma I'm thinking of is the is the is the

178
00:21:11,960 --> 00:21:19,080
karma based on greed based on on desire and it's a prime example of how desire leads to the

179
00:21:19,080 --> 00:21:30,440
degradation of our world our desire for meat has no doubt done a lot to to change the nature of

180
00:21:30,440 --> 00:21:38,040
this earth the fact that we do kill the fact that there is an incredible amount of killing

181
00:21:38,040 --> 00:21:49,080
killing going on is has degraded the world to to a great extent the the they said you know they

182
00:21:49,080 --> 00:21:55,960
say that the only reason people kill is because because people want you know there's a desire

183
00:21:55,960 --> 00:22:00,920
for meat and therefore people are killing but that's that's a simplistic thing to say people

184
00:22:00,920 --> 00:22:05,960
kill because they have they have defilements in their mind because they don't understand that

185
00:22:05,960 --> 00:22:11,000
killing is wrong they're the ones who are guilty of the killing the person who killed the people

186
00:22:11,000 --> 00:22:18,280
who have greed who who who pushed them to do it in the sense or or make it viable for them to do

187
00:22:19,320 --> 00:22:29,080
have have some responsibility for the the situation but but are not guilty of murdering

188
00:22:29,080 --> 00:22:37,080
so when a person desires to eat me if you're if you're someone who is is going out of your

189
00:22:37,080 --> 00:22:44,920
way to find me and this might even include buying meat although I would consider that to be a

190
00:22:44,920 --> 00:22:49,000
little more neutral in the sense that you go to the grocery store and you buy the food that

191
00:22:49,000 --> 00:22:55,880
you know is going to be healthy but when a person is actively seeking it out when a person is

192
00:22:55,880 --> 00:23:06,120
actively looking for meat or encouraging people asking for it and so on then there is a problem

193
00:23:06,120 --> 00:23:13,720
that there there is the degradation the the the only type of vegetarianism that I really

194
00:23:13,720 --> 00:23:22,760
wouldn't advise is the active vegetarianism where you you refuse meat when it's given to you

195
00:23:22,760 --> 00:23:29,880
and this is as monks this is the type of vegetarianism that we we don't subscribe to so

196
00:23:30,440 --> 00:23:39,000
as monks we do we are guilty if we ask for meat if we desire meat if we're looking for it

197
00:23:39,000 --> 00:23:47,720
then this is considered to be an offense against our our monastic code but we are expected to

198
00:23:47,720 --> 00:23:53,800
eat the food that people give us and I think this is the the line in the sand with the line

199
00:23:53,800 --> 00:24:02,440
that we should draw because at this point there is no guilt and no responsibility the the only

200
00:24:02,440 --> 00:24:07,480
way I can see that a person has responsibility is not because intellectually somehow they are

201
00:24:07,480 --> 00:24:15,480
contributing but because they are actively encouraging promoting increasing the demand because

202
00:24:15,480 --> 00:24:22,520
they actually demand it when a person takes whatever food they're given or you know and this

203
00:24:22,520 --> 00:24:27,400
doesn't just apply to monks so when we go to someone's house and they offer us food you know

204
00:24:27,400 --> 00:24:35,640
in the opera's meat this is considered you know we are a passive we are we are a passive

205
00:24:38,840 --> 00:24:44,200
recipient of the meat we're not actively seeking it out and we have no greed for it and we eat

206
00:24:44,200 --> 00:24:57,080
it for our nourishment I would say that this is this this is on the word this is ethically neutral

207
00:24:57,080 --> 00:25:03,960
an ethically neutral act and so for this reason I would say that no it's not necessarily to be

208
00:25:03,960 --> 00:25:09,080
vegetarian I would say that there is an argument that we should not actively seek out meat

209
00:25:09,080 --> 00:25:18,440
but if a person is if a person is is looking to practice meditation and follow the Buddhist

210
00:25:18,440 --> 00:25:24,600
teaching I would say that even even could be that buying meat is not a real problem if your

211
00:25:24,600 --> 00:25:30,040
intention is not to specifically buy meat but simply to buy food that is going to sustain you

212
00:25:30,040 --> 00:25:40,200
with the with the only object of for the practice of meditation because as I said the point of

213
00:25:40,200 --> 00:25:46,600
the whole argument the point of the Buddhist teaching is the simplification of our lives

214
00:25:46,600 --> 00:25:52,840
that our lives should be simply about our experience when we see it's only seeing when we hear

215
00:25:52,840 --> 00:25:57,400
it's only hearing the Buddha said this is the way to enlighten the way we should train ourselves

216
00:25:57,400 --> 00:26:02,920
this that seeing becomes only seeing hearing becomes only hearing smelling tasting feeling and

217
00:26:02,920 --> 00:26:16,120
thinking so meat is something that only that that only has significance in an intellectual

218
00:26:16,120 --> 00:26:23,880
sense on an experiential level it's something that has been abandoned there is no mind involved

219
00:26:23,880 --> 00:26:34,200
so there is no empathy and no sympathy and no relation to ourselves no connection to ourselves

220
00:26:34,200 --> 00:26:41,320
that needs to arise in the mind it's a physical that has been abandoned by the being to whom it

221
00:26:41,320 --> 00:26:51,800
once belonged so that is my understanding of vegetarianism that it's it's not quite as simple as one

222
00:26:51,800 --> 00:27:02,280
might think and it does show that the that Buddhist ethics are actually you know quite logical

223
00:27:02,280 --> 00:27:08,440
scientific and you know quite simple easy to understand I should think especially if you're

224
00:27:08,440 --> 00:27:13,720
practicing meditation the only time it would be unethical is in the same way that anything

225
00:27:14,360 --> 00:27:21,240
that has to do with involves greed is unethical anytime we have greed we can see why the

226
00:27:21,240 --> 00:27:27,240
universe why the world is being changed in so many ways is totally about greed our greed for oil

227
00:27:27,240 --> 00:27:34,360
our greed for land our greed for gold money our greed for computers and electronics this is a good

228
00:27:34,360 --> 00:27:40,760
example for instance the the the electronics industry and all of the metals and so on is actually

229
00:27:40,760 --> 00:27:48,280
creating great conflict in the world so there are people who will say that using the the electronic

230
00:27:48,280 --> 00:27:53,640
devices that we have with the computer chips and so on with all of their metals that have to

231
00:27:53,640 --> 00:28:00,920
be mined and therefore have to have to be fought over that so the people are using these are

232
00:28:00,920 --> 00:28:07,320
actually contributing to the conflicts in places like many African nations where they are doing

233
00:28:07,320 --> 00:28:12,120
mining and and there's you know there's not one group there's everyone's fighting over them and

234
00:28:12,120 --> 00:28:18,440
there's there's a lot of suffering that goes on my point is that that it's not direct because

235
00:28:18,440 --> 00:28:24,440
a person obviously who uses the electronic devices is has no thought in their mind that they're

236
00:28:24,440 --> 00:28:29,160
doing that the point is those people who are fighting over the these things are you know if they

237
00:28:29,160 --> 00:28:35,560
were to give up then there would be there would obviously be no problem when when we let go and

238
00:28:35,560 --> 00:28:42,520
we and the people who are encouraging the fighting who are pushing to you know to to get these

239
00:28:42,520 --> 00:28:50,200
minerals and so on but yet we can see how our greed affects that how our need for for these

240
00:28:50,200 --> 00:28:59,720
you know high tech gadgets and so on so the the unethical this is an unethical part of our being

241
00:28:59,720 --> 00:29:05,400
that we should give up if we can use things like technology that that are that exists without

242
00:29:05,400 --> 00:29:12,520
greed without attachment without need for them and without need for really everything then

243
00:29:12,520 --> 00:29:18,920
there would be never be any need to go to war for anything we'd all be able to use what we have

244
00:29:19,720 --> 00:29:23,160
but you can see it's totally based on greed that we're fighting over

245
00:29:23,160 --> 00:29:34,440
fighting over rocks as an example and so many other things of this is a different aspect of the

246
00:29:34,440 --> 00:29:46,360
issue of vegetarianism as it is an aspect of all of our actions all of our our part of our

247
00:29:46,360 --> 00:29:52,280
lives that has to do with our desires and our need for this or that so I hope this has been

248
00:29:52,280 --> 00:29:59,000
interesting and helped to put somehow in perspective the issue of vegetarianism I think it's sad that

249
00:29:59,000 --> 00:30:06,280
people might feel guilty when they eat meat you know unreasonably thinking that somehow they

250
00:30:06,280 --> 00:30:11,320
are responsible for some sort of killing and I know there are many people who feel that way and

251
00:30:11,320 --> 00:30:17,720
actually probably you know quite quite disagree with what I'm saying here but I would ask those

252
00:30:17,720 --> 00:30:22,360
people to look in their own minds and to see the state of mind even as they're listening to this

253
00:30:22,360 --> 00:30:27,080
video because it's the state of our mind that is the most important it's the only thing that's

254
00:30:27,080 --> 00:30:33,480
going to determine our future if I eat meat I'm not worried about it because it means nothing

255
00:30:33,480 --> 00:30:42,040
to me I know that the being has gone but when I see living beings even farm animals or so on

256
00:30:42,040 --> 00:30:49,320
knowing that they have to be slaughtered it's it's really a totally different state of affairs I

257
00:30:49,320 --> 00:30:57,320
could never do some of the things that I did before because I understand the suffering involved

258
00:30:57,320 --> 00:31:02,760
it is quite clear I think if you meditate to see the difference that you would never wish

259
00:31:02,760 --> 00:31:09,800
suffering on another being if you if you understand the nature of suffering and the nature of the

260
00:31:09,800 --> 00:31:16,520
defilements that lead lead us to cause suffering for others so I think that's enough for this

261
00:31:16,520 --> 00:31:40,280
question thanks for tuning in all the best

