1
00:00:00,000 --> 00:00:03,440
Hello and welcome back to us for a month.

2
00:00:03,440 --> 00:00:14,480
Today I will be answering the question as to what should we do when confronted by situations

3
00:00:14,480 --> 00:00:24,960
where we feel necessary to perform violent acts specifically in terms of killing what

4
00:00:24,960 --> 00:00:35,000
we deem to be pests, those sentient beings that are causing suffering for ourselves

5
00:00:35,000 --> 00:00:47,040
or those around us or are confronting us with a situation where we were put in some great

6
00:00:47,040 --> 00:00:49,520
physical difficulty.

7
00:00:49,520 --> 00:00:58,560
So the example that was given on the ask.ceremunglo.org was about rats and in this case

8
00:00:58,560 --> 00:01:09,880
it is not even a physical difficulty that they are giving, it is a problem with the landlord.

9
00:01:09,880 --> 00:01:17,200
So the landlord has given an ultimatum that is not an ultimatum the requirement that

10
00:01:17,200 --> 00:01:21,960
the rats be taken care of.

11
00:01:21,960 --> 00:01:26,760
And so the question is what to do at this point.

12
00:01:26,760 --> 00:01:40,040
So first of all this question is really the core issue here, not in regards to specific

13
00:01:40,040 --> 00:01:53,560
situations but the core theory that we have to get through or come to understand is

14
00:01:53,560 --> 00:01:59,920
the difference between physical well-being and mental well-being.

15
00:01:59,920 --> 00:02:13,720
And so the problem is that we quite often think more of our physical well-being than of

16
00:02:13,720 --> 00:02:19,600
our mental well-being and will often place our physical well-being ahead of our mental

17
00:02:19,600 --> 00:02:26,320
well-being, not realizing that this is actually the choice we are making and that by performing

18
00:02:26,320 --> 00:02:35,800
violent acts towards other beings that we are actually hurting our mental health, our

19
00:02:35,800 --> 00:02:37,680
mental well-being.

20
00:02:37,680 --> 00:02:46,200
And so we will commit egregious acts of violence against other living beings seeking out

21
00:02:46,200 --> 00:02:51,640
some state of physical well-being for ourselves or for others.

22
00:02:51,640 --> 00:02:58,920
And so this goes for a great number of situations and this could even be extended into acts

23
00:02:58,920 --> 00:03:06,120
of war and acts of murder and assassination and so on.

24
00:03:06,120 --> 00:03:12,720
And so the question of whether it would have been right or wrong to murder someone like

25
00:03:12,720 --> 00:03:18,160
Adolf Hitler or Osama bin Laden if you had the chance.

26
00:03:18,160 --> 00:03:24,880
And so this isn't exactly what we are dealing with here but the underlying issue is our

27
00:03:24,880 --> 00:03:28,280
physical well-being versus our mental well-being.

28
00:03:28,280 --> 00:03:37,200
So in cases of pests, in cases of dangerous animals, even snakes and scorpions and so on,

29
00:03:37,200 --> 00:03:48,240
we often react unmindfully not realizing that we are actually working towards our own detriment

30
00:03:48,240 --> 00:03:56,000
and even though by acting in such a way we might further our physical well-being for some

31
00:03:56,000 --> 00:04:07,120
time, we are actually causing a great amount of deterioration in our mental well-being.

32
00:04:07,120 --> 00:04:14,800
So we can ask ourselves which is more important, whether it is better that we live a healthy

33
00:04:14,800 --> 00:04:24,960
and a strong physical life with a corrupt and evil and unwholesome mind or whether we

34
00:04:24,960 --> 00:04:34,280
die with a pure mind and for most people because of our inability to see beyond this human

35
00:04:34,280 --> 00:04:44,680
state beyond this one life, what we call this life, this birth, this existence.

36
00:04:44,680 --> 00:04:49,520
Because this seems to be all that there is and because we are so entrenched in this idea

37
00:04:49,520 --> 00:04:56,120
of the human state as being the ultimate that we see nothing wrong with committing egregious

38
00:04:56,120 --> 00:05:06,240
acts and even with sullying our minds for the benefit for immediate pleasure, not realizing

39
00:05:06,240 --> 00:05:11,920
that we are accumulating these tendencies in our minds and the mind doesn't go away

40
00:05:11,920 --> 00:05:19,640
that it continues on and there is no end that we might call death or so on, as long as

41
00:05:19,640 --> 00:05:27,880
we have these tendencies they will increase and they will ever again lead us to conflict

42
00:05:27,880 --> 00:05:33,800
and suffering and they are actually setting us up for greater and greater suffering which

43
00:05:33,800 --> 00:05:38,200
is quite easy to see if one is practicing the meditation that one will see that these

44
00:05:38,200 --> 00:05:45,160
acts are unwholesome unpleasant, not leading to positive circumstances.

45
00:05:45,160 --> 00:05:51,440
There was a question that was asked some time ago on asked at seremungal.org about whether

46
00:05:51,440 --> 00:05:58,680
it would be worth it to go to hell yourself for people who believe in that the mind can

47
00:05:58,680 --> 00:06:06,240
become so solid that it goes to hell in order to save other beings in order to bring

48
00:06:06,240 --> 00:06:10,240
up to prevent great violence or so on.

49
00:06:10,240 --> 00:06:14,960
So if you were to kill Osama bin Laden they'd all Hitler someone like this and therefore

50
00:06:14,960 --> 00:06:22,840
we are able to prevent great suffering as the idea was to save the world would you do

51
00:06:22,840 --> 00:06:32,640
it and so the point here is that by creating more violence, we are in a state where there

52
00:06:32,640 --> 00:06:38,680
is a great amount of violence in the world and there are beings in positions to inflict

53
00:06:38,680 --> 00:06:46,480
great violence on other beings and this is a result of our accumulated tendencies to

54
00:06:46,480 --> 00:06:49,840
perform such violent acts on each other.

55
00:06:49,840 --> 00:06:56,440
Now by increasing those, you are not in any way helping things, the people who die,

56
00:06:56,440 --> 00:07:01,360
the people who kill, they all have these tendencies in them, what you do by in this case

57
00:07:01,360 --> 00:07:07,960
going to hell or in any case creating unwholesome states in the mind is just increasing

58
00:07:07,960 --> 00:07:12,240
the amount of unwholesome in the universe since the beings that die, they continue on

59
00:07:12,240 --> 00:07:19,160
the beings that kill, they continue on and you yourself by adding to the pot, you only

60
00:07:19,160 --> 00:07:20,560
increase the tendencies.

61
00:07:20,560 --> 00:07:29,080
So the important solution, the solution is to decrease the amount of killing and to teach

62
00:07:29,080 --> 00:07:39,800
people to be patient when confronted with revenge, when other beings would inflict violence

63
00:07:39,800 --> 00:07:47,760
on us to be forbearing and to put an end to it, if this means my death, then let that

64
00:07:47,760 --> 00:07:56,880
be the end to not react, to not reply and create the cycle of revenge which actually

65
00:07:56,880 --> 00:08:04,640
spans lifetimes, a life is nothing in the face of our minds and our minds continue on

66
00:08:04,640 --> 00:08:07,280
and carry these states with them.

67
00:08:07,280 --> 00:08:14,000
So this is sort of the backdrop, the theory behind what I'm going to talk about here and

68
00:08:14,000 --> 00:08:22,560
the basic answer that know it's not right and it could never be a good thing to get rid

69
00:08:22,560 --> 00:08:29,160
of rats, you know, to kill rats that are infesting your house or even to kill parasites

70
00:08:29,160 --> 00:08:36,760
or to kill those dangerous animals, a snake or someone that's an animal or an intruder

71
00:08:36,760 --> 00:08:41,320
who is threatening your family and so on.

72
00:08:41,320 --> 00:08:46,600
And this is because of the difference between physical health and mental health and if

73
00:08:46,600 --> 00:08:51,720
it means that we have to as a result suffer physically and not just our bodies but our

74
00:08:51,720 --> 00:09:06,840
physical surrounding might be imperfect, suboptimal but that our minds should remain healthy

75
00:09:06,840 --> 00:09:13,920
is far more important and if our minds can stay healthy and can stay pure then it really

76
00:09:13,920 --> 00:09:21,160
doesn't matter where we are and so this is the first thing that we have to get through

77
00:09:21,160 --> 00:09:23,480
and we have to understand.

78
00:09:23,480 --> 00:09:32,160
So then the question is well then what do you do if you are, if it is not in your best

79
00:09:32,160 --> 00:09:37,200
interest to perform acts of violence, well then what do you do when confronted with this

80
00:09:37,200 --> 00:09:38,200
situation?

81
00:09:38,200 --> 00:09:44,800
You're here, it's not just our well-being, we have a conflict with our landlord for example

82
00:09:44,800 --> 00:09:52,560
and so I've gotten several questions in regards to this, even people asking about if

83
00:09:52,560 --> 00:09:56,720
you're confronted by a violent person, what would you do?

84
00:09:56,720 --> 00:10:01,440
So there are I think three methods that are in line with the boot decision, there's actually

85
00:10:01,440 --> 00:10:08,320
you could break it up any number of ways but briefly I think there are three suitably

86
00:10:08,320 --> 00:10:17,200
Buddhist responses to this sort of situation, whether it be pests, whether it be criminals

87
00:10:17,200 --> 00:10:21,840
or murderers or however.

88
00:10:21,840 --> 00:10:29,120
The first is to avoid the situation, now the Buddha did condone avoiding those situations

89
00:10:29,120 --> 00:10:34,680
that would obviously get in the way of your meditation practice and your mental development

90
00:10:34,680 --> 00:10:45,320
and the examples he used were of dangerous, you know, a charging elephant, an elephant's

91
00:10:45,320 --> 00:10:50,560
charging at you, you avoid it, you don't stand there and seeing, you don't have to,

92
00:10:50,560 --> 00:10:54,040
you can move to the side or so on.

93
00:10:54,040 --> 00:11:00,160
But I think this extends to a lot of things, for instance avoiding situations, avoiding, in

94
00:11:00,160 --> 00:11:07,200
the case of criminals or murderers, avoiding those areas that where you're likely to be

95
00:11:07,200 --> 00:11:11,360
confronted with those sorts of people.

96
00:11:11,360 --> 00:11:20,760
Now in the case of rat infestations, this is probably not a useful solution but it is one

97
00:11:20,760 --> 00:11:26,000
solution that we should all keep in mind is to avoid those situations where you have a

98
00:11:26,000 --> 00:11:32,000
landlord or be, you're living in a house that is susceptible to these sorts of things.

99
00:11:32,000 --> 00:11:38,000
So I mean an ideal form of this would be to leave the home and live under a tree or

100
00:11:38,000 --> 00:11:44,520
live in a cave, live in the forest where you don't have to deal with these situations

101
00:11:44,520 --> 00:11:51,000
because obviously living in the household life, it's much more complicated and the situation

102
00:11:51,000 --> 00:11:56,320
with rats is one that comes up common. Another one that people talk about is lice.

103
00:11:56,320 --> 00:12:00,000
Well if you had lice, what would you do? Would you not want to kill them?

104
00:12:00,000 --> 00:12:06,280
And so one means of overcoming this is to shave your head as I've heard that actually,

105
00:12:06,280 --> 00:12:09,480
I'm not sure if this is true or not but I've heard that that will actually prevent the

106
00:12:09,480 --> 00:12:10,600
lice from reading.

107
00:12:10,600 --> 00:12:18,400
If you shave your head, they won't be able to stay there, they stay at the roots of the

108
00:12:18,400 --> 00:12:20,400
hair and so on.

109
00:12:20,400 --> 00:12:22,960
So these are just wild examples.

110
00:12:22,960 --> 00:12:29,280
I mean you could think of many different examples but with in the case of rats, in the

111
00:12:29,280 --> 00:12:36,360
case of violence and so on, we should be careful to avoid those kinds of situations that

112
00:12:36,360 --> 00:12:39,040
would only give rise to an awesomeness.

113
00:12:39,040 --> 00:12:48,280
If you're a, if you're not a strong person, a young attractive woman, for example, you

114
00:12:48,280 --> 00:12:52,000
might want to avoid dark streets at night or so on.

115
00:12:52,000 --> 00:12:57,280
I mean not even a woman if you're a person who doesn't look fierce or doesn't like

116
00:12:57,280 --> 00:13:03,280
me or so on, you might want to avoid those situations where you might get into a conflict.

117
00:13:03,280 --> 00:13:12,440
As a monk, I sometimes try to avoid those areas where I might be confronted with prejudice,

118
00:13:12,440 --> 00:13:13,440
bigotry and so on.

119
00:13:13,440 --> 00:13:19,400
I was, I was arrested and put in jail a couple of years ago, simply because I look

120
00:13:19,400 --> 00:13:29,000
different than, and some people do because of their fear, they either made some assumptions

121
00:13:29,000 --> 00:13:33,760
or else they were specifically trying to get rid of me, I don't know, but I was arrested

122
00:13:33,760 --> 00:13:36,560
and put in jail and I was a big deal.

123
00:13:36,560 --> 00:13:41,320
So would have probably been in my best interest to just avoid the whole issue and the

124
00:13:41,320 --> 00:13:49,240
whole situation and stay in a place that was more, more accepting.

125
00:13:49,240 --> 00:13:54,320
So this is the, the first answer, I think, and it will work in a variety of situations

126
00:13:54,320 --> 00:13:56,400
and it can be employed in a variety of ways.

127
00:13:56,400 --> 00:14:01,600
Just avoid the problem, find some way so that you, you don't have to be confronted with

128
00:14:01,600 --> 00:14:02,600
a situation.

129
00:14:02,600 --> 00:14:07,080
You don't have to be confronted with these difficult issues or, you know, try to restructure

130
00:14:07,080 --> 00:14:12,360
your life so in a way so that you don't meet with this situation.

131
00:14:12,360 --> 00:14:21,040
The second way is to find an alternative, an alternative to violence and there are many

132
00:14:21,040 --> 00:14:25,840
alternatives and I think that this is a point that I always try to raise with people is

133
00:14:25,840 --> 00:14:34,520
that killing and violence are not the only way out of a situation, whether it be with

134
00:14:34,520 --> 00:14:40,000
pests, whether it be with murderers or criminals or so on, often, you know, if someone

135
00:14:40,000 --> 00:14:44,360
wants your wallet, maybe you just give them your wallet and that's a way of dealing with

136
00:14:44,360 --> 00:14:51,720
the situation, mindfully, you know, giving up, letting go in this sense, dealing with

137
00:14:51,720 --> 00:14:57,160
the situation may be talking to the person, sometimes that can work with criminals and

138
00:14:57,160 --> 00:15:03,840
murderers, it's probably not likely to, with pests, this is a, this is really something

139
00:15:03,840 --> 00:15:07,680
that we, we spend far too little time on.

140
00:15:07,680 --> 00:15:14,680
I was trying to learn about how to get rid of, of termites and being, you know, the idea

141
00:15:14,680 --> 00:15:17,200
came up, well, when there are termites, you have to kill them.

142
00:15:17,200 --> 00:15:22,800
So I researched it and I was trying to find some information on ways of getting rid of termites

143
00:15:22,800 --> 00:15:28,760
that doesn't, that doesn't, that doesn't require you to kill them and no research has

144
00:15:28,760 --> 00:15:32,840
been done on this as far as I'm or maybe, but there's nothing, nothing on the internet

145
00:15:32,840 --> 00:15:38,160
anyway, and, and I'm betting that there's very little research been done on this because

146
00:15:38,160 --> 00:15:42,440
people don't think they think, well, with, when you have termites, you just kill them,

147
00:15:42,440 --> 00:15:49,520
you find some way and there are many ways, they have many need and, and ingenious ways

148
00:15:49,520 --> 00:15:54,920
of killing termites, but there's no, no one has put any of their ingenuity into finding

149
00:15:54,920 --> 00:15:59,600
other solutions because no one's ever thought that the importance of it, you know, when

150
00:15:59,600 --> 00:16:05,480
you can kill them, why would you find another solution? I think this is really tragic because

151
00:16:05,480 --> 00:16:09,400
in many cases, the solution does exist and it's not very difficult.

152
00:16:09,400 --> 00:16:13,920
The solution might be, with termites, might be simply finding a compound that they don't

153
00:16:13,920 --> 00:16:18,440
like. Now, who would have thought to try to find a compound that termites don't like when

154
00:16:18,440 --> 00:16:23,480
you can find a compound that kills them? Some chemical compound that drives them away.

155
00:16:23,480 --> 00:16:30,280
I don't know of any, but I haven't had that much experience with termites. I have had

156
00:16:30,280 --> 00:16:34,560
experience with other animals and with ants, for example, people will put out poison to

157
00:16:34,560 --> 00:16:43,600
kill the ants. Now, baby powder or talcum powder works, works not as well as poison, obviously,

158
00:16:43,600 --> 00:16:47,680
but not from Buddhist point of view, works much better than poison. You sweep the ants

159
00:16:47,680 --> 00:16:53,520
away, and then you put talcum powder down in their path, and I'm not sure if it's the

160
00:16:53,520 --> 00:16:59,240
scented talcum powder or if normal talcum powder will work as well, but somehow it stops

161
00:16:59,240 --> 00:17:03,560
them. The small ants aren't able to cross it, even the big ants don't like it because

162
00:17:03,560 --> 00:17:08,960
it removes their scent trails, and so they don't go across it, though, if you rub it

163
00:17:08,960 --> 00:17:13,880
into a plate, rub it across their trail, it'll remove their trail and they won't be able

164
00:17:13,880 --> 00:17:19,320
to find their way, and they won't come back in that direction. And I use this a lot with ants,

165
00:17:19,320 --> 00:17:26,960
but to a great success. If ants are coming along telephone wires or clothes lines or so

166
00:17:26,960 --> 00:17:31,220
on, you can put butter on the clothes line on the telephone wire and it won't cross

167
00:17:31,220 --> 00:17:36,960
butter. Not all ants, anyway. I do believe there are some varieties of ants that eat

168
00:17:36,960 --> 00:17:41,840
the butter, but as an example, there are ways around this with rats.

169
00:17:41,840 --> 00:17:49,440
The example I gave on the forum was to use humane rat traps, humane mice traps. These exist.

170
00:17:49,440 --> 00:17:55,200
It's a box, and you can buy them, and there's bait inside. The rat goes in the door

171
00:17:55,200 --> 00:17:59,200
closes, or the door is made in such a way that they can't get out again, and then you

172
00:17:59,200 --> 00:18:05,200
take it away to the forest, find some place far, far away, and release the rat end of

173
00:18:05,200 --> 00:18:12,720
the story. I do this with mosquitoes as well, and you have mosquitoes in your home,

174
00:18:12,720 --> 00:18:19,160
in your tent, and wherever you are, you take a cup, you put the mosquito in the cup,

175
00:18:19,160 --> 00:18:22,960
you take it outside, and you do this again and again, if you have a closed-off space where

176
00:18:22,960 --> 00:18:31,280
the mosquitoes don't come. So finding intelligent ways to deal with pests, I think, is

177
00:18:31,280 --> 00:18:35,440
incredibly important. In fact, part of me would like to set up a wiki page. Probably

178
00:18:35,440 --> 00:18:41,360
I will end up doing this a wiki page for just this sort of thing where people can post their

179
00:18:41,360 --> 00:18:47,920
good ideas for how to deal with difficult situations in a Buddhist way, so not just pests,

180
00:18:47,920 --> 00:18:54,800
but also how to deal with questions that you get, or how to deal with this sort of person,

181
00:18:54,800 --> 00:19:00,160
or that sort of person, how to deal with this situation, how to deal with so many of the issues

182
00:19:00,160 --> 00:19:04,800
that were confronted with that all people, and all religions are confronted with, and have to

183
00:19:05,600 --> 00:19:11,360
find some way to make it shy with their understanding of ethics and practice.

184
00:19:12,880 --> 00:19:20,320
So this is the second method, is to deal with it, find a way, an alternative means of dealing

185
00:19:20,320 --> 00:19:27,840
with the situation that doesn't require violence. And one note I'd make on that is that sometimes

186
00:19:27,840 --> 00:19:35,200
in self-defense, it is even according to the Buddha proper to resort to limited amounts of

187
00:19:35,200 --> 00:19:41,440
violence. So if someone is attacking you, to push them out of the way, or to hit them enough so

188
00:19:41,440 --> 00:19:46,080
that you can run away, even monks are allowed to do this. We're not allowed to hit someone,

189
00:19:46,080 --> 00:19:52,640
but we're allowed to hit someone in self-defense in order to get away. So if it means that you

190
00:19:52,640 --> 00:20:01,360
have to perform some limited act of violence in order to escape, or in order to wake up the

191
00:20:01,360 --> 00:20:07,360
attacker or so on, or to find a way to change the situation, even putting the attacker into

192
00:20:09,120 --> 00:20:15,680
an armlock or whatever, if you know karate or kung fu or some martial arts, to be able to

193
00:20:15,680 --> 00:20:24,560
change the situation and avoid avoiding the greater act of violence, then to a limited extent,

194
00:20:24,560 --> 00:20:34,800
because it's not something that is designed to, it's not something that is designed to harm,

195
00:20:34,800 --> 00:20:39,600
it's in self-defense and it's designed to allow you to escape the situation.

196
00:20:39,600 --> 00:20:49,440
And that is permitted that it doesn't inflict fatal harm on the other person or on the other

197
00:20:49,440 --> 00:20:59,120
being. So there might be a case where limited amounts of violence done in not in the intention

198
00:20:59,120 --> 00:21:05,920
of hurting or killing or seriously harming the other person, but simply in the interests of self-defense

199
00:21:05,920 --> 00:21:13,600
and for the purposes of escaping might be or are considered to be allowed. This is a way of dealing

200
00:21:13,600 --> 00:21:20,160
with the issue. The third answer, which I think we should also keep in mind, and this goes back to

201
00:21:20,160 --> 00:21:25,920
what I was saying in the beginning, is to accept and to let go of the situation. And I think the

202
00:21:25,920 --> 00:21:35,520
issue of rats in the landlord is an interesting example of this, because sometimes we have to

203
00:21:35,520 --> 00:21:41,200
think outside the box and we have to look outside of our situation and not get confined to

204
00:21:42,320 --> 00:21:47,600
A or B mentality, where if I don't do this, that is going to happen, because

205
00:21:48,960 --> 00:21:54,320
often when we let things go, when we're mindful, when we're aware of the situation,

206
00:21:54,320 --> 00:22:00,320
there's a C alternative arises, almost magically, and A and B disappear completely.

207
00:22:00,320 --> 00:22:09,360
So it may be in the case where we're confronted by an assault, someone assaulting us,

208
00:22:11,200 --> 00:22:17,840
sometimes when it can happen, that when we're mindful, when we're aware and when we're

209
00:22:17,840 --> 00:22:24,400
meditating on the situation, when we're taking it as a Buddhist practice, people were saying,

210
00:22:24,400 --> 00:22:31,280
what would you just say, pain, pain when someone's hitting you? I think yes, that's a good, a perfectly

211
00:22:31,280 --> 00:22:40,000
reasonable response for the situation, and can often have magical results, where people have

212
00:22:40,000 --> 00:22:47,520
found that suddenly this situation changed and they weren't, they were no longer the victim.

213
00:22:47,520 --> 00:22:53,520
Now they were in control and the other person was forced really due to the power of presence,

214
00:22:53,520 --> 00:22:59,120
because the mind is such a powerful thing, much more powerful than the body, and simply the presence

215
00:22:59,120 --> 00:23:06,400
of someone who is mindful is really the greatest weapon there is, and it's something that

216
00:23:06,400 --> 00:23:14,000
can truly overcome these situations. Sorry, in the case of the rats in the landlord, it might be

217
00:23:14,000 --> 00:23:21,280
that simply by being mindful and watching the situation unfold and allowing the consequences,

218
00:23:21,280 --> 00:23:26,800
if the landlords are going to throw you out, you simply say to the landlord, I'm Buddhist and I

219
00:23:26,800 --> 00:23:33,040
don't kill and so do what you will, and if it means that we have to do that, we have to come to

220
00:23:33,040 --> 00:23:40,560
some sort of conflict and so be it. Letting things go, holding on to what is really and truly

221
00:23:40,560 --> 00:23:46,160
important, which is your mental health and mental well-being, and the truth, because it's being

222
00:23:46,160 --> 00:23:51,440
untrue to yourself, to perform violence on other beings when you yourself don't want to

223
00:23:53,040 --> 00:23:59,280
feel such, but when you yourself don't or wish for that not to happen to you, so if someone's

224
00:23:59,280 --> 00:24:05,120
going to kill you, well the only reason you'd kill them first is because you yourself don't want

225
00:24:05,120 --> 00:24:10,320
to die, and so you're being just as hypocritical as the other person. You're inflicting something

226
00:24:10,320 --> 00:24:17,280
on other beings that you yourself would not wish for, and so it's something that is against harmony

227
00:24:17,280 --> 00:24:23,040
and is against the truth, it's against reality, and it's going to create, as I said, corruption

228
00:24:23,040 --> 00:24:29,920
in the mind, something that we can do much better without in this, and we can do with much less

229
00:24:29,920 --> 00:24:36,800
than there is already in the world, so something that we should strive to do away with rather than

230
00:24:36,800 --> 00:24:42,320
increase, so simply by being mindful, by being aware of the situation, whether it be a violent

231
00:24:42,320 --> 00:24:51,280
situation, whether it be a difficult situation, whether it be some life or something, even just

232
00:24:51,280 --> 00:24:58,800
sticking with the itching, and the pain of having life should be a part, not all of the answer,

233
00:24:58,800 --> 00:25:04,320
but it should be at least a part of the answer. Of course, we can find other solutions and

234
00:25:04,320 --> 00:25:15,680
something that stops the lies from or repels the lies, and so it repels the assailant, but doesn't

235
00:25:15,680 --> 00:25:25,280
cause more suffering than is warranted, or isn't of the purpose of causing death, or fatal, or

236
00:25:27,040 --> 00:25:33,440
permanent physical damage, or suffering to the other being. So I hope this has been helpful.

237
00:25:33,440 --> 00:25:37,360
I think this is an important subject, and I'm glad to talk about it. I've been meaning to make a

238
00:25:37,360 --> 00:26:07,200
video on this subject for a while, so there you have it. Thanks for tuning in, and all the best.

