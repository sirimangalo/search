I focus on the nostril, instead of the rising and falling of the valley, since I've started meditation.
Any other any downsides?
I've never used the nostrils, so I'm going, either by hearsay or by, once the word, conjecture.
By hearsay, we see Demaga says that the nostrils or mindfulness of breathing becomes more and more refined as you go.
But the only reason, categorical reason, reason for a decisive reason that was given for using the stomach, is that in Burma,
the in Burma mindfulness of breathing is understood to take place at the nose.
And mindfulness of breathing is considered a summit of meditation.
It's considered a meditation that starts off leading you through the janas, and later on is used to, can be used to bring about Vipassana.
But technically, it's the Jai-do-Vimuthi, the person who practices summit to first, and then Vipassana.
In our technique, we practice starting with Vipassana, so it was simply in order to avoid the accusation.
These are the words of Mahasi Sayadu.
In order to avoid the accusation that he was teaching samata, he switched to, and it may not have been him, it may have been his teacher,
but they switched to, he says, that they switched to the stomach.
The reason for it, the reason for needing to use, or for deciding to use the stomach, is that the Visudimaga says that a person who practices Vipassana should start by focusing on the four elements,
which are the building blocks that make up the physical aspect of experience.
So the earth element is the feeling of hardness and softness. The air element is the feeling of pressure or fluidity.
The fire element is the feeling of heat and cold, and the water element is the cohesion that keeps things together, or that makes things stick together.
In this case, this is the air element.
That's regards to the texts and theory, and as I said here say, so a textual argument.
Conjecture, it seems to me that because this one becomes more and more refined, and based on, I guess also here say what I've heard from meditators,
watching the nostril can be quite soothing, and as a result of it getting more and more refined, there's the tendency, or there appears to be the tendency to lead towards samata.
A person who practices mindfulness at the nostrils, I would conjecture is more likely, and from what I've seen and discussed with meditators who insist on using it, more likely to lead to samata meditation.
It's not wrong, it just tends to be more round about, and then you have to eventually get the meditator to focus it on it as ultimate reality.
But it would generally be after they've already entered into the janas, which takes time.
The stomach, on the other hand, being a course and rather large object is subject to, well, is an obvious, or leads to the clear awareness of ultimate reality.
And there's no opportunity for the arising of calm watching the stomach, and this is a problem that people have with this meditation.
They think it's wrong, they think it's bad, they think it's a problem, because they don't feel calm, because they don't feel peaceful.
But watching the stomach, you're going to see impermanence suffering and non-self quite quickly.
You're going to get quickly into issues of having to let go or suffer. It's a question of leading directly to vipassana.
So, it's not that there's disadvantages, it's just a tendency to lead to samata first, lead to tranquility first.
Whereas, the practice that we do is more like insight first and subsequent tranquility.
