1
00:00:00,000 --> 00:00:07,000
What would be your view on an ideal Buddhist society?

2
00:00:07,000 --> 00:00:14,000
Are there any examples of Asian countries that have come close to such an ideal?

3
00:00:14,000 --> 00:00:16,000
Who?

4
00:00:16,000 --> 00:00:19,000
That is difficult to answer.

5
00:00:19,000 --> 00:00:25,000
The last part of the question, especially...

6
00:00:25,000 --> 00:00:35,000
There are probably many views about an ideal Buddhist society.

7
00:00:35,000 --> 00:00:43,000
I think that if everybody, not only monks and nuns,

8
00:00:43,000 --> 00:00:52,000
would keep the precepts, then we wouldn't have any problems in the world,

9
00:00:52,000 --> 00:01:01,000
or in a society, and keeping the precepts at least the five, if not more.

10
00:01:01,000 --> 00:01:08,000
If everybody would keep the monks and nuns,

11
00:01:08,000 --> 00:01:11,000
precepts, then they wouldn't know.

12
00:01:11,000 --> 00:01:17,000
Be no problem at all in the world or in society, I would say.

13
00:01:17,000 --> 00:01:19,000
But I don't think that's livable.

14
00:01:19,000 --> 00:01:24,000
There would be no society because everyone says if we all became monks,

15
00:01:24,000 --> 00:01:28,000
what would we do then?

16
00:01:30,000 --> 00:01:33,000
Are there any countries?

17
00:01:33,000 --> 00:01:36,000
I don't really know.

18
00:01:36,000 --> 00:01:38,000
I don't think so.

19
00:01:38,000 --> 00:01:48,000
I think countries have their traditions and their cultures,

20
00:01:48,000 --> 00:01:58,000
and these cultures and traditions are so much inter-evolving and mixed with Buddhism

21
00:01:58,000 --> 00:02:05,000
that you can't really speak of a pure Buddhist country.

22
00:02:05,000 --> 00:02:14,000
There might be pure and ideal Buddhist movements or places,

23
00:02:14,000 --> 00:02:18,000
but countries, I wouldn't say so.

24
00:02:22,000 --> 00:02:27,000
I mean, it just makes me think the only answer I would have is Buddhist monastery.

25
00:02:27,000 --> 00:02:31,000
Buddhist monastery has the potential to be a perfect society.

26
00:02:31,000 --> 00:02:37,000
So rather than trying, it seems to me that these sorts of questions are asking,

27
00:02:37,000 --> 00:02:46,000
what should we do to make the world or even our country a perfect society or how should we work?

28
00:02:46,000 --> 00:02:53,000
At least thinking about this, trying to approach the idea of how to help society out

29
00:02:53,000 --> 00:02:57,000
because then when you hear the answer is to become a monk,

30
00:02:57,000 --> 00:03:04,000
the answer to the war and so on and to strive is for example to become a monk.

31
00:03:04,000 --> 00:03:11,000
Of course, it's not necessary. You can be a meditator and keep ordinary precepts and so on.

32
00:03:11,000 --> 00:03:18,000
Are we here about this? We say, well then how would society know how would humans,

33
00:03:18,000 --> 00:03:22,000
there would be no humans because everyone is celibate and who would feed the monks

34
00:03:22,000 --> 00:03:31,000
because monks go around and take whatever food is being given to holy people or to religious people

35
00:03:31,000 --> 00:03:35,000
and that's how they survive?

36
00:03:35,000 --> 00:03:39,000
So what would they do? But the point of, I think rather than,

37
00:03:39,000 --> 00:03:51,000
the point I think is rather than to try to create a perfect country or even a perfect

38
00:03:51,000 --> 00:04:03,000
country transforming the world into a perfect society is to enter into a community

39
00:04:03,000 --> 00:04:10,000
that exposes these ideals, which I would say is a Buddhist monastery

40
00:04:10,000 --> 00:04:16,000
because if there were more Buddhist monasteries and more people following

41
00:04:16,000 --> 00:04:21,000
purely and dedicating their lives to the practice and study and teaching

42
00:04:21,000 --> 00:04:29,000
of Buddhism or the Buddhist teaching, then you'd find that the whole world around them

43
00:04:29,000 --> 00:04:34,000
would begin to change because obviously they would be the role models and the leaders,

44
00:04:34,000 --> 00:04:43,000
not even cutting grass or not even cutting down trees, let alone taking life.

45
00:04:43,000 --> 00:04:50,000
The living with the bare necessities of life and living simply

46
00:04:50,000 --> 00:04:56,000
and dedicating oneself to self exploration and understanding

47
00:04:56,000 --> 00:05:03,000
of reality would have such an impact on the whole society

48
00:05:03,000 --> 00:05:07,000
and it does have such an impact on the world

49
00:05:07,000 --> 00:05:17,000
that that's really all that it takes and that's really the answer is to develop monastic institutions

50
00:05:17,000 --> 00:05:27,000
and of course there are many alternatives or alternative approaches to this idea

51
00:05:27,000 --> 00:05:32,000
people say no monasticism is not necessary and so they start a meditation center as laypeople

52
00:05:32,000 --> 00:05:46,000
and so well-powered to them but you know the Buddha had his own role, his own own lifestyle

53
00:05:46,000 --> 00:05:50,000
that he established for someone who was going to take his teaching seriously

54
00:05:50,000 --> 00:05:57,000
so to say that it's not necessary or so on is fine but it certainly is or seems to be

55
00:05:57,000 --> 00:06:03,000
the Buddha prescribed for someone who is going to be living the ideal contemplative life

56
00:06:03,000 --> 00:06:08,000
so I think the only way you can answer the question of what is an ideal society

57
00:06:08,000 --> 00:06:16,000
is by pointing to rather than what a society in the world would look like

58
00:06:16,000 --> 00:06:22,000
to go to the ideal community and just let it extrapolate from there

59
00:06:22,000 --> 00:06:29,000
and once you have an ideal monastic community which in and of itself is quite difficult to attain, to obtain

60
00:06:29,000 --> 00:06:35,000
then you have laypeople coming to the monastery and learning more about the precepts

61
00:06:35,000 --> 00:06:44,000
and taking it out and influencing the world around because ultimately we're not dealing with a stable system

62
00:06:44,000 --> 00:06:49,000
or dealing with a system that is in constant conflict, good and evil

63
00:06:49,000 --> 00:06:56,000
and unless you plan to do away with all of the evil in the world or even if you did

64
00:06:56,000 --> 00:07:11,000
I wouldn't suggest trying to arrange people in some sort of perfect and stable world-worldly society

65
00:07:11,000 --> 00:07:17,000
where people had jobs and so on, the best thing is to start at the core, start with yourself

66
00:07:17,000 --> 00:07:21,000
and let it work out outward naturally because if everyone did that

67
00:07:21,000 --> 00:07:48,000
and the more people that do that the better the whole world would be

