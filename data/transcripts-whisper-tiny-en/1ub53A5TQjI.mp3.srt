1
00:00:00,000 --> 00:00:07,000
What sort of benefit is there when one writes up the suit to copying them down by hand?

2
00:00:07,000 --> 00:00:28,000
Well it's a good way to remember them. I suppose maybe you're asking because you're searching for a way to get closer to the suit does and remember them and so on.

3
00:00:28,000 --> 00:00:36,000
The best way to remember the suit does is or the best way to approach the suit does is to read them.

4
00:00:36,000 --> 00:00:43,000
No. To practice meditation then read them then go back and practice meditation.

5
00:00:43,000 --> 00:00:49,000
You should practice meditation first because it will help you to understand the suit does.

6
00:00:49,000 --> 00:00:56,000
So you should at least start to practice before you get going to in depth study of them.

7
00:00:56,000 --> 00:01:03,000
And once you've undergone in depth or taken some study of them then you should go back to meditate.

8
00:01:03,000 --> 00:01:09,000
Because meditation improves your memory and improves your interest in the suit does.

9
00:01:09,000 --> 00:01:18,000
Which also improves your memory of the suit does because it's interest that leads us to be able to keep things in the mind.

10
00:01:18,000 --> 00:01:27,000
We aren't easily able to remember just facts and stories but if something is interesting to us we're much more able to remember it.

11
00:01:27,000 --> 00:01:38,000
So simply writing the suit does down is not a very good way to get closer to the suit does or to gain benefit from the suit does.

12
00:01:38,000 --> 00:01:52,000
There's a story of a monk who went through the 152 suit does of the Majima Nikaya and he was trying to memorize them and it was very, very difficult.

13
00:01:52,000 --> 00:01:58,000
And so eventually he gave up and went and practiced meditation for 20 years and became an Arahad.

14
00:01:58,000 --> 00:02:06,000
And then someone came to ask him about the Majima Nikaya and he was able to explain it without fail.

15
00:02:06,000 --> 00:02:11,000
Even though he said in 20 years I haven't looked at the Majima Nikaya.

16
00:02:11,000 --> 00:02:14,000
And he said, but try.

17
00:02:14,000 --> 00:02:23,000
Go ahead and start reciting and I'll see if I can pick it up and he was able to pick it up without failing a single word or so on.

18
00:02:23,000 --> 00:02:25,000
This is when it says in the VCD manga.

19
00:02:25,000 --> 00:02:29,000
So this is how we should approach.

20
00:02:29,000 --> 00:02:46,000
Or this is a good example of how we should approach the suit does that it's much more important and beneficial to meditate based on them than it is to really do any kind of in-depth study or of them.

21
00:02:46,000 --> 00:03:01,000
If your impetus for asking is in regards to the sort of general merit that one gains from it.

22
00:03:01,000 --> 00:03:07,000
Well, one feels happy in the mind and that peace in mind for doing a good deed.

23
00:03:07,000 --> 00:03:31,000
And the commentaries say writing one letter of Dhamma is equivalent to, you know, if it's one letter, each letter that makes up a Dhamma a suit that you write out or print up is the equivalent of building one Buddha statue.

24
00:03:31,000 --> 00:03:38,000
It's a monument of the Dhamma, monument of Buddhist monument that you're creating.

25
00:03:38,000 --> 00:03:40,000
It's helping to spread Buddhism.

26
00:03:40,000 --> 00:03:59,000
It's helping to spread the meditation practice to keep Buddhism alive and to give guidance to other people.

27
00:03:59,000 --> 00:04:05,000
You know, printing up Dhamma books, sharing Dhamma books is always a good thing.

28
00:04:05,000 --> 00:04:10,000
It's something which benefits your own mind makes you feel peaceful and happy inside.

29
00:04:10,000 --> 00:04:13,000
And so there's great benefit.

30
00:04:13,000 --> 00:04:23,000
If you make the determination at the time to become, you know, you can do it as a practice to encourage your mind to get closer to the suit.

31
00:04:23,000 --> 00:04:36,000
And to gain wisdom and understanding and to be able to meet with the Buddha or to be able to become a well-versed in the Buddha's teaching in the future, in maybe in the future life.

32
00:04:36,000 --> 00:04:45,000
Then it can be a great symbol or object for you to cultivate this determination for.

33
00:04:45,000 --> 00:04:53,000
And as a result, it can support your determination, your aditana.

34
00:04:53,000 --> 00:04:59,000
When you make a determination that in a future existence, may I become this or become that, it can have that benefit as well.

35
00:04:59,000 --> 00:05:07,000
If you use it just as a, didn't make a determination, may I, this support my meditation practice, then you can use that as well.

36
00:05:07,000 --> 00:05:15,000
It's all good deeds, it has the benefits of fortifying this one's mind's name.

