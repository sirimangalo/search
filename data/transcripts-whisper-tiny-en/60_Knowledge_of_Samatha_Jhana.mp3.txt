knowledge of Samata Jana's question.
Would it be helpful to experience the Jana's so that you can help a
meditator to move beyond blocks?
Answer, I think the underlying source of questions like this
is that people really want to learn the Jana's.
Just the thought of deep states of bliss and peace
sound exciting, so there arises a sort of desire for such states.
When you more clearly see the nature of ultimate reality, such
experiences become less exciting, as they are subject to the characteristics
of impermanence, suffering, and non-sal.
What you really need is an understanding of how reality works,
the Jana's are just another part of reality.
Jana's are not anything mysterious or extraordinary,
they are a state where the mind is secluded from the hindrances.
They are just mind states, they can last for extended periods of time,
you can cultivate them in various ways, and you can use them to develop
any number of magical powers, like remembering past lives and so on.
But the best way to understand them is to understand the reality
that underlies all experiences.
This is done through practicing the simple meditation of the four
foundations of mindfulness, to understand the building blocks of
experience because once you understand what makes up
experience, how experience works in terms of cause and effect,
then there is nothing mysterious or hard to understand
or really at all exciting about the Jana's.
So to directly answer your question, I do not think it would be at all
helpful. Direct experience of how the mind works
is far more useful than cultivating the samata Jana's in order to help someone
who experienced them and was having a block with them.
Attachment to peaceful and tranquil states of mind is common
and that is quite difficult to train someone out of.
It can be difficult to win a person who has practiced samata meditation
intensively off of it, especially if they are coming
from a different religious tradition. For example,
if they have practiced meditation in the Hindu tradition,
which has different goals and ideas, or even Buddhist meditation traditions
that emphasize the practice of tranquility and the states of
calm that it produces, which can be sometimes difficult to let go of
winning someone off of their attachment to pleasant states of mind
has nothing to do with knowledge of the Jana's.
It has to do with knowledge of attachments and views,
views about what is true happiness, views about what is self
and views about control and letting go. It has to do with views about
impermanence and as the Buddha said, the Jana's and any kind of
pleasant or even neutral feeling, they bring is not permanent.
Understanding these things does not require you to have a
attain the Jana's yourself.
