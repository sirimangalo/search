1
00:00:00,000 --> 00:00:20,300
Okay good evening everyone hopefully we have fixed the problem of the mono, the one channel

2
00:00:20,300 --> 00:00:21,300
audio.

3
00:00:21,300 --> 00:00:28,700
Can someone on a new tube confirm that we now have audio from both speakers?

4
00:00:28,700 --> 00:00:56,300
Testing, testing one, two, confirm, thank you.

5
00:00:56,300 --> 00:01:14,600
So we finished with the 8 fold noble path last night and that tonight we might focus more

6
00:01:14,600 --> 00:01:22,100
on the preliminary path, the pubanga manga and go over it in a way that's quite similar

7
00:01:22,100 --> 00:01:29,100
to the 8 fold noble path but it is more of a linear progression.

8
00:01:29,100 --> 00:01:39,100
When we talk about the 8 fold noble path it's a bit confusing as to which comes first.

9
00:01:39,100 --> 00:01:48,300
It's not quite clear, view comes first or that they all come together, they all happen

10
00:01:48,300 --> 00:02:00,460
at the same time and it's complicated and so we tend to simplify it and give a linear

11
00:02:00,460 --> 00:02:10,900
progression which is only a simplification because as many of you are most likely aware

12
00:02:10,900 --> 00:02:22,600
the path is complicated, it isn't a linear progression, there's a lot of bends and twists

13
00:02:22,600 --> 00:02:29,100
and turns and going backwards and forwards.

14
00:02:29,100 --> 00:02:36,820
It's not as straightforward as we might sometimes wish to make it but then the last having

15
00:02:36,820 --> 00:02:46,380
a sense of progression is quite useful so that as we wander about under in our search

16
00:02:46,380 --> 00:02:54,740
for enlightenment we always have a sense of which way is north, we have a sort of a compass

17
00:02:54,740 --> 00:03:05,020
to keep us clear on which way we have to go to get where we're heading.

18
00:03:05,020 --> 00:03:09,140
So the simplest way of understanding the 8 fold noble path in terms of a progression

19
00:03:09,140 --> 00:03:15,660
is morality, concentration and wisdom, sila samadhi panya.

20
00:03:15,660 --> 00:03:19,860
These the Buddha called the three trainings, it was a means of simplifying the 8 fold

21
00:03:19,860 --> 00:03:29,700
noble path and just make it clear to people what are we training in?

22
00:03:29,700 --> 00:03:40,660
And this appears to have evolved into the seven purifications, the sata risudhi, there's

23
00:03:40,660 --> 00:03:51,540
a discourse in the Majimani kaya between Sariputa and Poonamanta Niputa, these two esteemed

24
00:03:51,540 --> 00:03:56,420
leaders of the Buddhist religion in the time of the Buddha and they had a discussion

25
00:03:56,420 --> 00:04:03,500
about the seven purifications that one leads to the other, you're not practicing for any

26
00:04:03,500 --> 00:04:10,540
one of these but you practice one and then the other and the other and they go in progression

27
00:04:10,540 --> 00:04:17,340
just like relay chariots, the king wants to get from one city to another, he gets in one

28
00:04:17,340 --> 00:04:24,740
chariot and goes until it, he wears it out and then when he stops wherever it stops he

29
00:04:24,740 --> 00:04:29,260
gets in the second chariot, there's a second chariot waiting for him, they have spaced

30
00:04:29,260 --> 00:04:37,740
out, so each of the risudis is considered like these chariots, so today we'll deal with

31
00:04:37,740 --> 00:04:45,220
the first risudhi, risudhi means purification, it's what the risudhi manga, the book that

32
00:04:45,220 --> 00:04:49,860
we've been studying is based on, so this is maybe a good recap for those of you who have

33
00:04:49,860 --> 00:04:57,220
been involved with our study group, now that we're almost finished, we remember back to

34
00:04:57,220 --> 00:05:00,980
the beginning of the book we talked about sila risudhi and this is what we'll go over

35
00:05:00,980 --> 00:05:10,060
tonight, the word sila, we often translate as morality or ethics, that sort of thing,

36
00:05:10,060 --> 00:05:20,420
but etymologically it means something like behavior, or it's used in that sense, I think

37
00:05:20,420 --> 00:05:33,580
literally it just means normal or ordinary, usual in the sense of someone's sila is what

38
00:05:33,580 --> 00:05:39,780
they usually do, it's what they normally do, it's how they normally behave, it's what

39
00:05:39,780 --> 00:05:51,740
is normal for them, so it's used because what's normal for someone is their behavior

40
00:05:51,740 --> 00:06:02,980
and what we're talking about when we talk about sila is how someone behaves, it's

41
00:06:02,980 --> 00:06:07,620
the defilements, we split up into defilements being the things that we want to get rid

42
00:06:07,620 --> 00:06:12,140
of, that we say hey these are what's causing us suffering, these are what's in our mind

43
00:06:12,140 --> 00:06:20,540
that's a problem, they're a little bit into three parts and morality deals with defilements

44
00:06:20,540 --> 00:06:26,500
that are expressed, so expressed by body and by speech, as when we went over right action

45
00:06:26,500 --> 00:06:37,500
and right speech, we sort of talked about this, and so it's often meant to refer simply

46
00:06:37,500 --> 00:06:44,540
to not killing, not stealing, not doing bad deeds or lying and gossiping and that kind

47
00:06:44,540 --> 00:06:54,180
of thing, not having wrong speech, but it's a little more complicated, as I kind of hinted

48
00:06:54,180 --> 00:06:59,260
at, it's a little more complicated and so there's a whole chapter on this and it breaks

49
00:06:59,260 --> 00:07:06,100
it down into different parts, the easiest way to explain morality is to talk about the

50
00:07:06,100 --> 00:07:13,380
four-fold purification of morality, the Chateau Parisundi Sila, meaning that there are

51
00:07:13,380 --> 00:07:18,860
four aspects of morality, and if you cultivate them, if you purify them, you can consider

52
00:07:18,860 --> 00:07:28,700
your morality as pure, just keeping rules isn't enough, but keeping rules is the first

53
00:07:28,700 --> 00:07:38,020
part of the four-fold morality, so the first one is keeping moral precepts, and moral

54
00:07:38,020 --> 00:07:41,180
precepts are funny, it's not the kind of thing I would have ever thought myself to get

55
00:07:41,180 --> 00:07:45,660
into before I was interested in Buddhism, it never occurred to me that one should have

56
00:07:45,660 --> 00:07:51,820
to be particularly moral, it seemed like the sort of thing that people who didn't because

57
00:07:51,820 --> 00:07:57,420
they believed in God, who told them thou shalt not do this, thou shalt not do that, which

58
00:07:57,420 --> 00:08:05,020
if you think about it is a pretty silly reason not to do something, so is it wrong because

59
00:08:05,020 --> 00:08:10,700
God tells me to do it or does God tell me to do it because it's wrong, either way, it's

60
00:08:10,700 --> 00:08:15,660
not a very good reason, a better reason is to do it because it's wrong and then you think

61
00:08:15,660 --> 00:08:21,020
well, it's wrong with this, what's wrong with that, how can we say one thing is more

62
00:08:21,020 --> 00:08:30,460
wrong than another? What does it mean to say that something is wrong? Once you begin to practice

63
00:08:30,460 --> 00:08:36,140
meditation, you kind of jump on the moral bandwagon because you realize that it does have

64
00:08:36,140 --> 00:08:45,660
an impact on your life, on your mind, on who you are, what you do, what you say is your worst

65
00:08:45,660 --> 00:08:57,420
enemy, right? No enemy can do to you what you do yourself, what you do to yourself by what

66
00:08:57,420 --> 00:09:06,620
you do to others, for example, by how you treat others. And so keeping rules seems very much

67
00:09:06,620 --> 00:09:13,420
like a no brainer once you begin to practice meditation, it seems like it's the least I could

68
00:09:13,420 --> 00:09:21,340
do is to stop these things that I know are clearly wrong. And moreover it's easy, you know,

69
00:09:21,340 --> 00:09:30,860
keeping rules is as easy as long as you can remember them, it's encouraging because yeah, I may

70
00:09:30,860 --> 00:09:36,700
not be the nicest person, maybe I'm kind of mean and I agree sometimes, but I don't kill,

71
00:09:37,500 --> 00:09:43,180
so that's a start, I mean really, that's a good start, killing is a lot worse than the things

72
00:09:43,180 --> 00:09:50,300
that I do, so you know, clearly it's not the be all end all of being a moral and good person,

73
00:09:51,340 --> 00:10:01,420
but keeping rules is a very good start. And so as monastics, those when we take on meditation

74
00:10:01,420 --> 00:10:07,580
practice as monastics or when we take on monasticism as meditators, keeping hundreds of rules

75
00:10:07,580 --> 00:10:15,180
and all these minor rules is really more of a challenge than a burden. There's no burden to keeping

76
00:10:15,180 --> 00:10:21,100
the many, many rules of the monastic life when you're dedicated to the meditation practice.

77
00:10:24,860 --> 00:10:29,260
Permeditators not to eat in the morning seems like I'm not to eat in the afternoon to only in

78
00:10:29,260 --> 00:10:36,300
the morning seems like a burden in the beginning, but then as you practice and you see the benefit

79
00:10:36,300 --> 00:10:47,820
and you see how focused it keeps you, how it pushes you to keep your attention on the meditation

80
00:10:47,820 --> 00:10:53,580
and to stay efficient, you welcome it and you say, hey this is a good thing for me,

81
00:10:54,700 --> 00:11:04,060
moral ethical precepts are a good thing, they help me, they lead me to focus my mind which is the

82
00:11:04,060 --> 00:11:14,220
purpose of morality. The second aspect of morality is or behavior we might call it is

83
00:11:17,900 --> 00:11:26,540
the use of requisites and I talked about this before how we use the things that we own

84
00:11:26,540 --> 00:11:34,780
and the things that we come in contact with. Our ordinary way of using material possessions

85
00:11:34,780 --> 00:11:41,900
is for pleasure. We wear clothes because we like these clothes, they make us feel good. We eat food

86
00:11:41,900 --> 00:11:51,580
because it's delicious, it also makes us feel good. We have a home that pleases us, that makes us feel

87
00:11:51,580 --> 00:12:01,340
it makes us feel safe, makes us feel comfortable, makes us feel happy about ourselves,

88
00:12:01,340 --> 00:12:09,820
good about ourselves, proud of ourselves. And we use medicine to make us feel good.

89
00:12:12,620 --> 00:12:19,420
Use medicine sometimes to cultivate positive states, right? And medicinal marijuana has become

90
00:12:19,420 --> 00:12:24,860
this big thing and we want to joke really. For the most part, I understand if you're an epileptic

91
00:12:24,860 --> 00:12:30,620
or so on, there's apparently really good. I can see that. But medicinal marijuana because you're

92
00:12:30,620 --> 00:12:35,980
depressed is, oh I don't want to get into it, they make a lot of enemies off of that comment,

93
00:12:35,980 --> 00:12:42,060
but it's true. And I'm not a puritan, I've done lots, I smoke lots of marijuana, has shes,

94
00:12:42,060 --> 00:12:48,700
hash oil, mushrooms, we're going into the hard stuff, but all that, all the stuff surrounding the

95
00:12:48,700 --> 00:12:55,820
marijuana culture, I was into it for, you know, not as deep as some and not as deep as people

96
00:12:55,820 --> 00:13:07,500
are now, but I hear that nowadays it's much more potent, but again I'm going to people are

97
00:13:07,500 --> 00:13:15,980
going to say I'm smoking and critical of others, but it's kind of funny, I mean, think of it as

98
00:13:15,980 --> 00:13:24,460
medicinal. It's a good example, using medicine just to make you feel good, and to avoid unpleasant

99
00:13:24,460 --> 00:13:35,100
feelings, to avoid unpleasantness, it's a problem. And this is all a problem, all of all of these

100
00:13:35,100 --> 00:13:41,740
things that we use, when we use them for a pleasure, when we use them to avoid suffering.

101
00:13:43,500 --> 00:13:49,420
I mean to an extent it's necessary, if you don't have shelter, we're very difficult to meditate,

102
00:13:50,140 --> 00:13:57,020
if you don't have clothes, well, being bitten by mosquitoes and having flies land all over your

103
00:13:57,020 --> 00:14:05,260
body and just having to sit naked in the forest, it's not really a comfortable way to live,

104
00:14:07,260 --> 00:14:10,380
not to mention how distracting it is to have everyone walk around naked.

105
00:14:14,700 --> 00:14:19,900
So to some extent you have to use, and this is a bit of recognize, I think is quite practical

106
00:14:19,900 --> 00:14:25,020
and reasonable. He didn't say everyone go around naked and stop eating and so on, there are

107
00:14:25,020 --> 00:14:29,900
religious people who do that sort of thing. He found that that's not really important. I mean,

108
00:14:29,900 --> 00:14:39,180
morality is not, let's not say, your behavior is not that important, right? So meaning, whether

109
00:14:39,180 --> 00:14:44,220
you become a monk or whether you live in the world, someone who is living in the world can become

110
00:14:44,220 --> 00:14:50,220
enlightened, someone who is decked out in jewels and finery, can become enlightened,

111
00:14:50,220 --> 00:14:56,460
the queen, the king, these kind of people can, in certain instances, become enlightened.

112
00:14:59,020 --> 00:15:03,740
But that being said, it's important to understand how we use these things.

113
00:15:07,100 --> 00:15:10,780
So we use them, so we try to always remind ourselves why we're using them,

114
00:15:11,580 --> 00:15:15,420
ensure you can be wearing jewels and become enlightened, but it's much more likely that you're

115
00:15:15,420 --> 00:15:20,700
attached to your jewelry and proud about them and feeling proud about how you look and that kind

116
00:15:20,700 --> 00:15:27,420
of thing. Sure, you can say that medicinal marijuana helps you cope, but much more likely helps

117
00:15:27,420 --> 00:15:33,660
you avoid and ignore the problems rather than dealing with them. I mean, things like depression

118
00:15:33,660 --> 00:15:40,620
are really manageable and dealable if we have the right tools. And the end, it's much more

119
00:15:40,620 --> 00:15:46,940
about seeing that these things aren't problems and in all ways, seeing that it's not a problem.

120
00:15:46,940 --> 00:15:55,260
I remember speaking of shelter, I remember one, I tell this story a lot, I've told it before,

121
00:15:55,980 --> 00:16:01,420
how I didn't have a place to stay and it was, what am I going to do? You know, I got to go to

122
00:16:01,420 --> 00:16:07,260
this place or that place, I was in California in North Hollywood and I was trying to find a place

123
00:16:07,260 --> 00:16:15,660
to stay and you know, I wasn't, there were issues, it was complicated. And in the end, I just

124
00:16:15,660 --> 00:16:23,020
looked at, looked at the situation, I turned around and walked out the door and I went and slept

125
00:16:23,020 --> 00:16:29,500
under a park bench. In the middle of winter, it's quite cold and I slept on the concrete,

126
00:16:29,500 --> 00:16:35,580
I just rolled my robes up and lay down on the concrete and fell asleep and it was wonderful.

127
00:16:35,580 --> 00:16:40,780
It was, you know, just this freedom and realization that, you know, it's not a problem.

128
00:16:43,260 --> 00:16:47,260
It's not such a wonderful thing that I did, but it's just something that made me realize

129
00:16:48,380 --> 00:16:53,580
as many things have, it was one time, other bin times where I had no food and

130
00:16:54,300 --> 00:16:59,340
just the stress of worrying about what am I going to do if I had no food and realizing,

131
00:16:59,340 --> 00:17:06,300
I won't eat, that's all. It's quite simple actually.

132
00:17:11,100 --> 00:17:17,260
So realizing why we use these things, we use them, we use food because it's necessary to stay alive

133
00:17:18,860 --> 00:17:24,860
and realizing that we don't need a lot of food. So thinking about it this way helps us be practical.

134
00:17:24,860 --> 00:17:33,260
Why are we wearing clothes? It's important because clearly these things don't satisfy us,

135
00:17:34,780 --> 00:17:41,340
not clearly, but when you meditate you can start to see clearly that the cause of many of our

136
00:17:41,340 --> 00:17:48,060
problems is just our attachment to our possessions, our ego and how they feed our personality.

137
00:17:48,060 --> 00:17:57,820
I have this house and these clothes that make me look attractive or handsome or powerful.

138
00:18:01,580 --> 00:18:07,100
And we see how that leads to ego and attachment, how it leads to stress and suffering ultimately.

139
00:18:09,420 --> 00:18:14,140
So is the second using our requisites and using everything, being mindful of the things that we use

140
00:18:14,140 --> 00:18:19,260
and why we're using them and not letting them become a source for attachment,

141
00:18:19,260 --> 00:18:25,900
defilement, suffering, not letting them hurt us, create addiction and bad habits,

142
00:18:26,700 --> 00:18:30,940
make us into bad people so that we cause suffering for ourselves and others.

143
00:18:35,500 --> 00:18:40,940
The third one is right livelihood. So purification of livelihood.

144
00:18:40,940 --> 00:18:46,300
And we talked about this before, it's not much to say about it. If you're acting or speaking in a

145
00:18:46,300 --> 00:18:52,060
way that is unethical and you're doing that to make a living, well that's wrong livelihood.

146
00:18:52,060 --> 00:19:02,540
It's quite simple, but there is the added aspect of improper livelihood in the sense of livelihood

147
00:19:02,540 --> 00:19:08,380
that gets in the way of your practice. It's not necessarily wrong, but it's a distraction.

148
00:19:08,380 --> 00:19:12,540
And you think, well, most livelihood is a distraction, right?

149
00:19:13,580 --> 00:19:17,660
And the Buddha had to wrestle with this and in the time of the Buddha it was a question.

150
00:19:18,540 --> 00:19:24,860
If you want to focus yourself on enlightenment, spirituality in general,

151
00:19:25,980 --> 00:19:32,060
how do you live? What are you going to do? How can you make a living?

152
00:19:33,660 --> 00:19:37,100
If you've got all this work to do, how can you focus your attention on spirituality?

153
00:19:37,100 --> 00:19:43,180
And so it was a question I think asked in general and thought about, and it has been thought

154
00:19:43,180 --> 00:19:50,620
about in most societies. So I think I would argue that India was more spiritual than most cultures

155
00:19:52,700 --> 00:19:59,420
in ancient times. And so there was a real question about how do you be spiritual?

156
00:20:00,860 --> 00:20:06,700
How do you really practice spirituality? And so they worked on this system.

157
00:20:06,700 --> 00:20:12,140
Maybe not a system, but it was kind of an understanding. There were people who supported

158
00:20:13,500 --> 00:20:19,980
spiritual people to go off into the forest and do their rituals and meditation practices

159
00:20:19,980 --> 00:20:24,620
in seek out enlightenment. And it was considered to be a good thing, first of all,

160
00:20:25,580 --> 00:20:30,300
because it's a good thing, you know, to think of people in practicing meditation becoming

161
00:20:30,300 --> 00:20:36,940
enlightened is great. But also I think more practically is, well, then those people can teach us,

162
00:20:36,940 --> 00:20:43,260
right? Maybe they can be a refuge for me to become enlightened or maybe they can teach me

163
00:20:43,260 --> 00:20:50,620
at least how to be a good person. For those people who weren't so keen on spirituality,

164
00:20:50,620 --> 00:20:56,940
but wanted to learn how to prosper and succeed and be happy and be good, be good people.

165
00:20:56,940 --> 00:21:09,020
So it sort of goes like this idea of simplifying your life. If you're ambitious

166
00:21:10,220 --> 00:21:14,380
and keen on rising up in the world and making lots of money,

167
00:21:16,700 --> 00:21:25,580
we always hear about how money and spirituality don't really mix and how rich people have a hard

168
00:21:25,580 --> 00:21:32,460
time with spirituality. It's not really the point here. The point is not the money, the point is

169
00:21:32,460 --> 00:21:38,940
how ambitious your arm or your attention is. So it's not really about being rich, but it's about

170
00:21:38,940 --> 00:21:46,780
how you live your life. You know, rich person conceivably could be quite spiritual, because conceivably

171
00:21:46,780 --> 00:21:52,060
they have a lot of time on their hands and don't have to work. But a person who's very much

172
00:21:52,060 --> 00:21:58,300
dedicated to their work and a self-made millionaire or billionaire and someone who constantly can't

173
00:21:58,300 --> 00:22:04,460
help themselves and it's very keen on business and prosperity and a material sense, obviously

174
00:22:04,460 --> 00:22:13,180
it's going to have a hard time. So trying to find a way that it's important that we try to find

175
00:22:13,180 --> 00:22:20,380
a way to be spiritual and not let our livelihood get in the way. I mean, ultimately this is why

176
00:22:20,380 --> 00:22:28,700
the monastic order was was created to provide a way for people to live without having the

177
00:22:28,700 --> 00:22:36,380
requirements. So we eat only enough to survive, which means we can really get by on charity

178
00:22:37,260 --> 00:22:44,220
by those people who, for whatever reason, are keen to feed us. Sometimes because we teach

179
00:22:44,220 --> 00:22:47,740
them, sometimes just because they think, hey, that's a good thing, kind of like a scholarship.

180
00:22:47,740 --> 00:22:53,740
They think, wow, that's, I really appreciate those people trying to become better people trying

181
00:22:53,740 --> 00:23:02,140
to purify their minds. That kind of thing. For whatever reason, there is always this aspect

182
00:23:02,140 --> 00:23:11,420
of society where people support religious people and so it is possible. And so it's a question

183
00:23:11,420 --> 00:23:15,100
to ask. I mean, for most people, it's not going to be possible to become monastic. So they think

184
00:23:15,100 --> 00:23:26,140
about how to simplify their lives, find a way to make money and live without having to put too much

185
00:23:26,140 --> 00:23:32,940
energy into it, without having to get too caught up in it. So I'm doing good things. And like the

186
00:23:32,940 --> 00:23:40,860
story of this potter that we always come back to in Katakara who made pots. And what he did is

187
00:23:40,860 --> 00:23:46,700
he went and found clay by the side of the river and he went and found wood in the forest and he

188
00:23:47,340 --> 00:23:53,340
fired up these pots, handmade pots, just simple pots, but you know, made them very mindfully. He was

189
00:23:53,340 --> 00:24:00,620
an anangami. And he put them down by the side of the road and when people came and asked him how

190
00:24:00,620 --> 00:24:06,220
much those pots were, he would say, you know, give what you want. If you think it's worth so many

191
00:24:06,220 --> 00:24:10,860
beans, just give me some beans, if you think it's worth rice, give me some rice, whatever you think

192
00:24:10,860 --> 00:24:18,060
it's worth. And that's how he made his living. Probably didn't do very well, right? But how simple

193
00:24:18,860 --> 00:24:24,220
and he was able to survive. And of course, there are going to be people who appreciate that and

194
00:24:24,220 --> 00:24:31,580
you think, wow, what a noble individual and so really support him and offer him maybe more than

195
00:24:31,580 --> 00:24:38,940
the pots or worth. I mean, you can see it might seem incredibly naive, but I think people who

196
00:24:38,940 --> 00:24:45,500
think such things are naive are overly cynical or are overly reactionary because for sure he would

197
00:24:45,500 --> 00:24:52,140
have had to deal with people cheating him. But then he would have also found people who are

198
00:24:52,140 --> 00:25:00,700
willing to protect him and chastise people who cheated him, right? You can't escape your karma,

199
00:25:00,700 --> 00:25:06,780
a good karma and bad karma. If you have good karma, it's hard not to make a good living.

200
00:25:08,380 --> 00:25:12,460
And so we don't have to worry so much about it. What we should worry about is our goodness.

201
00:25:13,980 --> 00:25:17,500
And if you're a good person, it's not hard to live.

202
00:25:21,420 --> 00:25:26,780
It's number three. And number four is really most important for us as meditators.

203
00:25:26,780 --> 00:25:32,780
So all of this we've talked about is sort of more preliminary stuff, but the fourth one is

204
00:25:32,780 --> 00:25:38,780
guarding the senses. Guarding the senses is the most important behavior for a meditator.

205
00:25:42,060 --> 00:25:45,900
We talk about behavior, right? We're talking about our actions and our speech. Well,

206
00:25:47,340 --> 00:25:54,540
keeping your mind focused on your behavior. So everything that you see, what does the body do?

207
00:25:54,540 --> 00:26:01,100
Well, it seems. It doesn't seem, but it looks or it provides the basis for seeing.

208
00:26:03,340 --> 00:26:05,820
So how does the mind interact with the body through seeing?

209
00:26:08,940 --> 00:26:13,100
And the problem is, of course, when we see this is where all our defilements rise. We like

210
00:26:13,100 --> 00:26:18,540
certain things. We dislike certain things. Whatever we see, it affects us and we react to it.

211
00:26:18,540 --> 00:26:27,580
When we hear sounds, we like sounds, we dislike sounds. Our senses are unguarded. Of course,

212
00:26:27,580 --> 00:26:32,140
they are. No one ever taught us to guard our senses. It seems like a strange thing to have to do.

213
00:26:33,900 --> 00:26:39,100
Until you realize that this is where all the problems arise. This is where we are subjective,

214
00:26:39,100 --> 00:26:44,380
where we are reactionary, where we can give rise to liking, disliking.

215
00:26:44,380 --> 00:26:52,460
All of our senses. So when we talk about things like depression, anxiety, fear,

216
00:26:53,020 --> 00:26:57,180
we think of these as conditions of the mind, right? Like something that's stuck to us,

217
00:26:57,180 --> 00:27:01,660
something that's weighing down, or maybe even something that's growing from inside. It's a part

218
00:27:01,660 --> 00:27:08,300
of who we are, and we think, kill it, kill it. When in fact, these things are arising every moment,

219
00:27:08,300 --> 00:27:15,900
when you see that's when depression arises. That's when anxiety arises. That's when fear arises.

220
00:27:16,780 --> 00:27:22,940
When you hear, you hear a sad song, and it makes you depressed, or you hear someone's voice,

221
00:27:23,740 --> 00:27:30,460
who you loved, but maybe they dumped you, makes you feel depressed, or you hear people saying things

222
00:27:30,460 --> 00:27:38,860
that remind you of better times, or you smell, maybe you smell, good smells, bad smells,

223
00:27:38,860 --> 00:27:47,020
you smell delicious food that you can't afford, and you feel depressed, anxious, afraid.

224
00:27:47,020 --> 00:27:57,420
These arise from the senses, tastes, feelings, and of course, thoughts.

225
00:27:57,420 --> 00:28:04,140
All of these, including thoughts, you know, this is where our problems arise.

226
00:28:04,940 --> 00:28:09,820
They aren't problems in themselves, but at every moment of seeing, hearing, smelling,

227
00:28:09,820 --> 00:28:15,020
tasting, feeling, thinking, when our mind is unguarded, there's the opportunity for the

228
00:28:15,020 --> 00:28:30,300
filaments to come in. They would have said, just like a house that is poorly fattched.

229
00:28:30,300 --> 00:28:44,780
Just as a roof that is poorly fattched, the rain is able to penetrate. It leaks.

230
00:28:44,780 --> 00:29:14,060
It's really remarkable that we have this practice this way of being invincible,

231
00:29:14,060 --> 00:29:19,820
completely invincible, such that nothing can cause us suffering. I mean, that is incredible.

232
00:29:20,620 --> 00:29:27,900
It shouldn't appear incredible, I think. Incredible even people disbelieve it, but it's quite simple

233
00:29:28,620 --> 00:29:32,300
when you understand where the defilements come from. They come from our senses,

234
00:29:33,820 --> 00:29:40,940
and when seeing is just seeing, seeing can never hurt you. When feeling is just feeling

235
00:29:40,940 --> 00:29:46,220
can never hurt you. When thinking is just thinking, thoughts can never hurt you.

236
00:29:50,700 --> 00:29:57,100
So that one is, obviously, that one clearly gets more into the mind, and we suit you

237
00:29:57,100 --> 00:30:09,180
among the book. The text says, I mean, this one can also be categorized under the later purifications,

238
00:30:09,180 --> 00:30:14,940
but it is the culmination of morality. If you want to think, what is ultimate morality,

239
00:30:14,940 --> 00:30:21,580
it's well guarding the senses. Two ways of guarding, one way is to just not look at things

240
00:30:21,580 --> 00:30:27,820
when monks walk. They're supposed to look down and not look around at the world around them.

241
00:30:29,020 --> 00:30:35,100
For obvious reasons, it's easy to get distracted and caught up in things otherwise.

242
00:30:35,100 --> 00:30:40,780
But as meditators, it's just about being mindful. When we say to ourselves seeing,

243
00:30:40,780 --> 00:30:47,580
seeing, we're cultivating right morality, it focuses your mind because it's the right thing to do.

244
00:30:49,580 --> 00:30:55,660
It's the right way to interact with the physical world. Seeing is just seeing, seeing, seeing,

245
00:30:55,660 --> 00:31:05,500
hearing, hearing, hearing, hearing, that's all. So altogether, that's morality or behavior. We call it

246
00:31:05,500 --> 00:31:12,300
morality because we're thinking of this, well, it's not just any behavior. It doesn't matter whether

247
00:31:12,300 --> 00:31:22,300
you eat with your right hand or left hand, it's not about behavior, but what you do, whether you go

248
00:31:22,300 --> 00:31:30,060
for a walk or whether you drive a car, it's not that kind of behavior. It's about the behavior

249
00:31:30,060 --> 00:31:39,420
that actually is meaningful and ethical moral. And so ultimately, morality and the Buddhist

250
00:31:39,420 --> 00:31:45,820
sense is being mindful. It comes from guarding your senses and keeping them objective.

251
00:31:45,820 --> 00:31:53,180
So there you go. That's the dhamma for tonight. Quite a long one. Thank you all for

252
00:31:56,060 --> 00:31:58,860
keeping with me with it and watching it.

253
00:31:58,860 --> 00:32:18,700
We'll look at the questions. Three questions. Is it recommended in Buddhism that we should

254
00:32:18,700 --> 00:32:25,500
gain control of our dreams so we can meditate in our dreams? No, not really. You'll find that

255
00:32:25,500 --> 00:32:30,780
as you practice mindfulness more, you'll dream less. In the beginning, there might be dreams.

256
00:32:31,980 --> 00:32:35,900
In the beginning, your dreams might be more vivid because you're stirring your mind up, but

257
00:32:37,340 --> 00:32:45,260
you'll dream less and eventually not even dream at all. Your sleep will be a time for rest.

258
00:32:50,380 --> 00:32:54,460
Does right view in the eight full noble path mean right resolution in regards to conflicts and

259
00:32:54,460 --> 00:33:00,780
problems? No, that's right. That's the second one. Some must uncover. Right view is what leads to

260
00:33:00,780 --> 00:33:15,020
your resolutions. So it leads to your intentions. Right resolution, you mean resolving conflicts.

261
00:33:15,980 --> 00:33:22,380
It doesn't mean that, but it notes view. So let's say you have a conflict. If your view is that

262
00:33:22,380 --> 00:33:28,220
conflict is a good thing or that if your view is, hey, this person deserves me to yell at them,

263
00:33:29,020 --> 00:33:35,660
then the resolution is going to, and the sunk up is going to be sunk up a meaning of how you

264
00:33:35,660 --> 00:33:43,820
act or how you intend to act. So you think, yeah, this person deserves it, then you get really

265
00:33:43,820 --> 00:33:49,020
angry at them because you're very angry, then you act and speak in bad ways and so on.

266
00:33:49,020 --> 00:33:56,300
But right view is how you see things, how you, what your view is on things, what your opinion is

267
00:33:56,300 --> 00:34:06,220
of them. Ultimately comes down to seeing things as seeing the four noble truths, understanding

268
00:34:06,220 --> 00:34:08,780
the nature of reality as being based on the form of the truth.

269
00:34:13,820 --> 00:34:16,140
And my circle of friends, we often use swear words.

270
00:34:16,140 --> 00:34:21,340
Is it encouraging Duke of because it violates right speech? It doesn't violate right speech.

271
00:34:22,620 --> 00:34:30,380
You can swear all you want. There's nothing, even the word, should I dare I say

272
00:34:30,380 --> 00:34:35,820
some of these words? I don't know. Maybe I won't. I don't mind saying them myself, but

273
00:34:38,220 --> 00:34:43,340
just because you know this is a live broadcast, doesn't violate right speech.

274
00:34:43,340 --> 00:34:49,500
Someone sent me a video once by Osho, and I'm not a follower of Osho, and I think it's probably

275
00:34:49,500 --> 00:34:56,060
a lot of problems associated with this guy, but he said one of, some of you probably know this

276
00:34:56,060 --> 00:35:00,540
video. It's on YouTube about the four letter word starting with F.

277
00:35:02,540 --> 00:35:07,260
FUCK, I mean, it's not really even a bad word. Where it comes from is four unlawful

278
00:35:07,260 --> 00:35:13,020
carnal knowledge apparently. It's just an acronym. So it's amazing how it's become such an important

279
00:35:13,020 --> 00:35:16,700
word, and it's really funny. He goes through all the ways that we use the word,

280
00:35:19,020 --> 00:35:22,700
the many ways that it can be used, and it's a weird video in fact, but

281
00:35:25,180 --> 00:35:31,580
there's nothing wrong with any word. It's really wrong speech has to do very much with your intentions.

282
00:35:31,580 --> 00:35:40,700
So right speech is really in the mind. It's kind of weird that it is, but

283
00:35:42,060 --> 00:35:45,260
it's much less to do with your speech and much more to do with your intentions.

284
00:35:47,340 --> 00:35:52,380
But the point being that intentions are one thing, but when you act or speak based on your

285
00:35:52,380 --> 00:35:57,500
intentions, it's another thing entirely because it has much more far-reaching consequences.

286
00:35:57,500 --> 00:36:05,820
That's all. Okay, so is it only attitude? Yeah, so if I insult someone, that's right.

287
00:36:05,820 --> 00:36:12,300
Yeah, so if insult someone and say FU or you're an SHIT head and that kind of thing,

288
00:36:12,300 --> 00:36:17,900
you know, that's bad, obviously. But you can call someone a buffalo. That's a bad thing as well.

289
00:36:17,900 --> 00:36:28,780
That's bad speech. I call someone, what are the words, your dumb, that kind of thing.

290
00:36:28,780 --> 00:36:32,540
I mean, there's a list of them in this, or does I'm just trying to think of it, but

291
00:36:33,740 --> 00:36:39,820
insult any kind of insult. Even if it's, it's meant as an insult, but it isn't an insult.

292
00:36:39,820 --> 00:36:42,780
The fact that it's meant as an insult makes it bad speech.

293
00:36:42,780 --> 00:36:52,700
Okay, so that's everything. I couldn't get the audio feed working, hopefully it works,

294
00:36:52,700 --> 00:36:59,580
because I did something changed, messed with the audio, so it rejected it. I'll have to look into

295
00:36:59,580 --> 00:37:04,700
that. If any of you were relying on the audio stream, my apologies, but you can't hear this

296
00:37:04,700 --> 00:37:15,340
anyway. Anyway, thank you all for tuning in. Have a good night.

297
00:37:34,700 --> 00:37:36,700
you

