1
00:00:00,000 --> 00:00:28,820
Okay.

2
00:00:28,820 --> 00:00:32,820
Okay, hello and welcome back to our study of the Dhamabanda.

3
00:00:32,820 --> 00:00:35,820
Today we continue on with the JITDA wagat.

4
00:00:35,820 --> 00:00:40,820
The JITDA wagat wagana chapter number three

5
00:00:40,820 --> 00:00:44,820
means we finished chapters one and chapters two already.

6
00:00:44,820 --> 00:00:49,820
And this starts with verses number 33 and 34,

7
00:00:49,820 --> 00:00:53,820
which are based on a story about a monk called Meghia.

8
00:00:53,820 --> 00:00:55,820
So the verses go as follows.

9
00:00:55,820 --> 00:00:59,820
Pandanam chapter long titam,

10
00:00:59,820 --> 00:01:04,820
Dura kandunivarana, Duneivariyan,

11
00:01:04,820 --> 00:01:08,820
Ujum karuti, Mehtavi,

12
00:01:08,820 --> 00:01:14,820
Usu karova, Tejana.

13
00:01:14,820 --> 00:01:23,820
Vadhi, Jovat, Telekipto,

14
00:01:23,820 --> 00:01:30,820
Ukamo, Ukamo kata, Ubato.

15
00:01:30,820 --> 00:01:35,820
Pari, Panda, D, Dangitam,

16
00:01:35,820 --> 00:01:41,820
Mahra, Dhyang, Pahat, Tewai.

17
00:01:41,820 --> 00:01:44,820
These are the two verses for today.

18
00:01:44,820 --> 00:01:48,820
So the verse number, the translations verse number 33 is

19
00:01:48,820 --> 00:01:54,820
Pandanam titam, the mind that is fickle and unsteady.

20
00:01:54,820 --> 00:01:59,820
Dura kang, difficult to guard Duneivarayan,

21
00:01:59,820 --> 00:02:08,820
difficult to forbid or to stop to keep in check.

22
00:02:08,820 --> 00:02:12,820
Ujum karuti, Mehtavi, a wise person,

23
00:02:12,820 --> 00:02:14,820
makes strength.

24
00:02:14,820 --> 00:02:18,820
Just as Usu karuti, just as a Fletcher,

25
00:02:18,820 --> 00:02:22,820
makes an arrow strength.

26
00:02:22,820 --> 00:02:29,820
And verse 34, Vadhi, Jovat, just as a water-borne creature

27
00:02:29,820 --> 00:02:36,820
or a fish, Telekipto is thrown upon the cast upon the bare ground.

28
00:02:36,820 --> 00:02:41,820
Taken out from the water, Ukamo kata, Ubato.

29
00:02:41,820 --> 00:02:46,820
So cast upon the ground of having been taken out from the water.

30
00:02:46,820 --> 00:02:50,820
Pari, Panda, D, Dangitam,

31
00:02:50,820 --> 00:02:58,820
so to the, does this mind waver or flail about?

32
00:02:58,820 --> 00:03:02,820
Maradee, young, it is the realm of Mara.

33
00:03:02,820 --> 00:03:06,820
Pahatadee, that, and it should be discarded.

34
00:03:06,820 --> 00:03:09,820
Or the realm of Mara should be discarded.

35
00:03:09,820 --> 00:03:14,820
The mind that is fickle and the mind that is wavering.

36
00:03:14,820 --> 00:03:19,820
So two very important verses from a practical point of view.

37
00:03:19,820 --> 00:03:21,820
And we have a fairly brief story.

38
00:03:21,820 --> 00:03:23,820
Because the story is actually told elsewhere.

39
00:03:23,820 --> 00:03:26,820
So in the Dhamapadhi here, we have it in very, very short form,

40
00:03:26,820 --> 00:03:29,820
just mentioning what the story was about.

41
00:03:29,820 --> 00:03:32,820
So the story is about a monthly magia,

42
00:03:32,820 --> 00:03:35,820
who in the time, at some time in the Buddhist life,

43
00:03:35,820 --> 00:03:40,820
was the Buddha's, uh, attendant.

44
00:03:40,820 --> 00:03:43,820
And so he would carry the Buddha's bowl when they went on pin to pad,

45
00:03:43,820 --> 00:03:45,820
when they went on arms round.

46
00:03:45,820 --> 00:03:47,820
And one time when they were going on arms round,

47
00:03:47,820 --> 00:03:56,820
or traveling, magia had suddenly this urge to go and practice meditation

48
00:03:56,820 --> 00:04:01,820
in this forest growth that he had seen by the side of the, by the side of the road.

49
00:04:01,820 --> 00:04:05,820
He couldn't explain it, but he felt like he had to,

50
00:04:05,820 --> 00:04:08,820
he had to go and practice meditation that there was something

51
00:04:08,820 --> 00:04:11,820
that just wonderful about this little, this forest growth.

52
00:04:11,820 --> 00:04:14,820
So he asked the Buddha to take, to take his bowl,

53
00:04:14,820 --> 00:04:17,820
and he said, look, I'm without an attendant.

54
00:04:17,820 --> 00:04:21,820
You have to, you know, I have to carry my bowl.

55
00:04:21,820 --> 00:04:23,820
You have to act as my attendant.

56
00:04:23,820 --> 00:04:25,820
And he said, no, no, please, please, take your bowl.

57
00:04:25,820 --> 00:04:28,820
Because he couldn't understand why the Buddha was, uh,

58
00:04:28,820 --> 00:04:31,820
so, so badment that he should have to carry the Buddha's bowl.

59
00:04:31,820 --> 00:04:34,820
He said, well, why can't the Buddha carry his own bowl?

60
00:04:34,820 --> 00:04:39,820
He said, no, no, because this would be the duty of an attendant month to carry the bowl.

61
00:04:39,820 --> 00:04:41,820
But it seemed like a really silly thing.

62
00:04:41,820 --> 00:04:45,820
It made you try to explain that he wanted to go and practice meditation,

63
00:04:45,820 --> 00:04:48,820
and normally the Buddha would say, fine, fine, go and practice,

64
00:04:48,820 --> 00:04:50,820
and take his bowl back.

65
00:04:50,820 --> 00:04:53,820
But at this time, the Buddha refused.

66
00:04:53,820 --> 00:04:57,820
And for the third time, and finally, the Buddha consented in silence,

67
00:04:57,820 --> 00:05:00,820
took his bowl back and continued on his way.

68
00:05:00,820 --> 00:05:02,820
Meghia in the meantime went into this, into this,

69
00:05:02,820 --> 00:05:05,820
uh, growth to practice meditation.

70
00:05:05,820 --> 00:05:09,820
And as soon as he sat down, he felt his, his mind full of lust,

71
00:05:09,820 --> 00:05:14,820
and just this vivid images and memories of the past came up,

72
00:05:14,820 --> 00:05:19,820
and he was filled with all sorts of unwholesome stains.

73
00:05:19,820 --> 00:05:24,820
And so he, he quickly got up and went back to, to meet the Buddha,

74
00:05:24,820 --> 00:05:28,820
and he was sorry that he had left, and the Buddha said, I told you not to go.

75
00:05:28,820 --> 00:05:30,820
He said, and he told them actually a past life story,

76
00:05:30,820 --> 00:05:33,820
somehow it goes, then he said, you know, in a past life,

77
00:05:33,820 --> 00:05:36,820
that, that grow of the reason why it's so attractive to you,

78
00:05:36,820 --> 00:05:41,820
it's because it's a growth that you used to, uh, meet up with women in this one,

79
00:05:41,820 --> 00:05:44,820
something like that.

80
00:05:44,820 --> 00:05:49,820
And so the Buddha used this as an example to, to, to admonish the monks,

81
00:05:49,820 --> 00:05:51,820
that the mind is quite fickle, and you shouldn't,

82
00:05:51,820 --> 00:05:53,820
you shouldn't follow after it.

83
00:05:53,820 --> 00:05:56,820
That the mind can lead you in so many different ways.

84
00:05:56,820 --> 00:06:00,820
In one moment, the mind can change and, and lead us off into,

85
00:06:00,820 --> 00:06:04,820
uh, defilement and leave us off, lead us off into the wrong direction.

86
00:06:04,820 --> 00:06:06,820
And we might be sitting in meditation or,

87
00:06:06,820 --> 00:06:10,820
we might be living our lives in a peaceful way in, in a whole some way.

88
00:06:10,820 --> 00:06:14,820
And suddenly something comes up and sets us off.

89
00:06:14,820 --> 00:06:17,820
And, and, and, and carries us away.

90
00:06:17,820 --> 00:06:21,820
So the Buddha, the Buddha gave us explanation that this is the nature of the mind.

91
00:06:21,820 --> 00:06:26,820
The mind is fickle, the mind is unsteady, the mind is crooked.

92
00:06:26,820 --> 00:06:30,820
And he uses this, this image of a, of a Fletcher straightening an arrow.

93
00:06:30,820 --> 00:06:35,820
So what he's saying is that the, the mind in its ordinary state is not straight.

94
00:06:35,820 --> 00:06:36,820
It's crooked.

95
00:06:36,820 --> 00:06:41,820
And going here and going there, the mind, the mind is clinging.

96
00:06:41,820 --> 00:06:44,820
Like if you look at the hand when the hand clings to something,

97
00:06:44,820 --> 00:06:45,820
it's because it's not straight.

98
00:06:45,820 --> 00:06:48,820
If you're straight in your hand, you can't cling to anything.

99
00:06:48,820 --> 00:06:50,820
Or a hook, for example.

100
00:06:50,820 --> 00:06:53,820
Because in the next verse he's talking about fish and Mara,

101
00:06:53,820 --> 00:06:55,820
the bait of Mara, no.

102
00:06:55,820 --> 00:06:58,820
The only reason Mara can catch us is because our mind is,

103
00:06:58,820 --> 00:07:00,820
is, is crooked.

104
00:07:00,820 --> 00:07:02,820
The mind has something that you can hook into.

105
00:07:02,820 --> 00:07:08,820
But when the mind is straight, then there's no entry for Mara.

106
00:07:08,820 --> 00:07:13,820
And there's no way for us to, to get caught.

107
00:07:13,820 --> 00:07:18,820
So from, you know, the, the story is just one example of how this occurs.

108
00:07:18,820 --> 00:07:23,820
And it's a really good example for us to, to see how in meditation our mind can,

109
00:07:23,820 --> 00:07:26,820
can, can, can, can carry us away with it.

110
00:07:26,820 --> 00:07:31,820
That we can get carried away by the, the fickleness of the mind.

111
00:07:31,820 --> 00:07:33,820
The mind is difficult to guard.

112
00:07:33,820 --> 00:07:35,820
It's something that can fall into greed.

113
00:07:35,820 --> 00:07:36,820
It can fall into anger.

114
00:07:36,820 --> 00:07:39,820
It can fall into delusion very easily.

115
00:07:39,820 --> 00:07:40,820
It's difficult to guard.

116
00:07:40,820 --> 00:07:43,820
And it's difficult to, to forbid.

117
00:07:43,820 --> 00:07:46,820
Even to just stop the mind from, from, from,

118
00:07:46,820 --> 00:07:49,820
from, from thinking this or thinking that from, from getting angry,

119
00:07:49,820 --> 00:07:50,820
from getting upset.

120
00:07:50,820 --> 00:07:54,820
And recently one, one meditative was explaining how she,

121
00:07:54,820 --> 00:07:57,820
but very angry and, at, at work.

122
00:07:57,820 --> 00:07:59,820
Um, some, some one said something to her told,

123
00:07:59,820 --> 00:08:01,820
it was disappointed in her work.

124
00:08:01,820 --> 00:08:03,820
And she was so upset.

125
00:08:03,820 --> 00:08:05,820
But the bad thing wasn't the upset.

126
00:08:05,820 --> 00:08:09,820
She said the bad thing was, uh, was, was, was this anger

127
00:08:09,820 --> 00:08:11,820
that she had for herself?

128
00:08:11,820 --> 00:08:14,820
Um, because of how upset she was.

129
00:08:14,820 --> 00:08:18,820
So she, she, she was, the, the problem wasn't what they had said to her.

130
00:08:18,820 --> 00:08:21,820
She, she, she wasn't so, uh, upset about what they had said,

131
00:08:21,820 --> 00:08:24,820
but she was upset that she had got so upset.

132
00:08:24,820 --> 00:08:26,820
And this was a, became a huge problem.

133
00:08:26,820 --> 00:08:30,820
And so she was berating herself and, and, um,

134
00:08:30,820 --> 00:08:35,820
uh, it was very angry at herself for allowing her to self

135
00:08:35,820 --> 00:08:39,820
to be, herself to become so, um, out of,

136
00:08:39,820 --> 00:08:44,820
or to, to become so unstable in the mind.

137
00:08:44,820 --> 00:08:48,820
So the Buddha in, in another sense, the Buddha is just giving us a,

138
00:08:48,820 --> 00:08:53,820
um, a teaching here to, to allow us to see this clearly,

139
00:08:53,820 --> 00:08:55,820
to not become upset when this occurs.

140
00:08:55,820 --> 00:08:58,820
Because actually the truth is the problem isn't the upset.

141
00:08:58,820 --> 00:09:00,820
The problem is that you get upset about it.

142
00:09:00,820 --> 00:09:04,820
When, when you get angry, then you think there's some problem.

143
00:09:04,820 --> 00:09:05,820
Or when you have lust arise.

144
00:09:05,820 --> 00:09:07,820
So this, this monkey had some feeling arise.

145
00:09:07,820 --> 00:09:09,820
And he didn't know what it was.

146
00:09:09,820 --> 00:09:11,820
But he thought, of course, that he should follow it.

147
00:09:11,820 --> 00:09:16,820
And this is really the problem is, is the reactions to our reactions.

148
00:09:16,820 --> 00:09:18,820
The reactions to our emotions.

149
00:09:18,820 --> 00:09:21,820
When you want something, one thing isn't a real problem.

150
00:09:21,820 --> 00:09:23,820
And still you, until you chase after it.

151
00:09:23,820 --> 00:09:26,820
Until you try to achieve your desires.

152
00:09:26,820 --> 00:09:28,820
Anger isn't a real problem.

153
00:09:28,820 --> 00:09:30,820
Until you say, I can't take it anymore.

154
00:09:30,820 --> 00:09:32,820
I have to go and, and act upon it.

155
00:09:32,820 --> 00:09:35,820
I have to hurt someone or I have to cause suffering.

156
00:09:35,820 --> 00:09:39,820
And so the Buddha's, on the, on the, on the one hand here,

157
00:09:39,820 --> 00:09:42,820
he's, he's just explaining that this is the way it is.

158
00:09:42,820 --> 00:09:47,820
And your job is not to, to, to, to change this.

159
00:09:47,820 --> 00:09:49,820
You can't, you want, do new iron on the Buddha's.

160
00:09:49,820 --> 00:09:51,820
It's very difficult to prevent it.

161
00:09:51,820 --> 00:09:54,820
The, the, the, the duty is to make them, make, create a clear mind.

162
00:09:54,820 --> 00:09:56,820
Create a straight mind.

163
00:09:56,820 --> 00:09:58,820
To change this habit of the mind.

164
00:09:58,820 --> 00:10:02,820
The, the mind is going here and going there is,

165
00:10:02,820 --> 00:10:04,820
taking us away.

166
00:10:04,820 --> 00:10:06,820
It's crooked, you know.

167
00:10:06,820 --> 00:10:10,820
The mind is, is following all sorts of crooked paths.

168
00:10:10,820 --> 00:10:13,820
The goal of our practice is to straighten the mind.

169
00:10:13,820 --> 00:10:21,820
The, the, the, the, the, the crookedness here is,

170
00:10:21,820 --> 00:10:25,820
it's quite interesting because even in, even in meditation practice

171
00:10:25,820 --> 00:10:29,820
we can give rise to this sort of crookedness or, or even as monks

172
00:10:29,820 --> 00:10:30,820
even in a monastic setting.

173
00:10:30,820 --> 00:10:33,820
So for example, we, we know that we have to give up greed.

174
00:10:33,820 --> 00:10:35,820
We know that we have to give up anger.

175
00:10:35,820 --> 00:10:37,820
We know that we have to give up delusion.

176
00:10:37,820 --> 00:10:42,820
We'll always find little ways, devious ways of, of justifying our

177
00:10:42,820 --> 00:10:43,820
defilements.

178
00:10:43,820 --> 00:10:47,820
So the, the, the defilements will not really be the problem,

179
00:10:47,820 --> 00:10:51,820
but the problem is this crooked mind that is, is, is quite

180
00:10:51,820 --> 00:10:52,820
devious.

181
00:10:52,820 --> 00:10:56,820
So the example here is Meghia, who, who perhaps even knew that

182
00:10:56,820 --> 00:10:59,820
he had lost arising, but, you know, or, or knew that he was,

183
00:10:59,820 --> 00:11:02,820
he was being impartial, but he tried to justify it and say,

184
00:11:02,820 --> 00:11:05,820
oh, but, but I want to go and practice meditation.

185
00:11:05,820 --> 00:11:08,820
And the truth was he was just somehow attracted to this and he had

186
00:11:08,820 --> 00:11:12,820
some incredible desire to go into that, the, this grove, which

187
00:11:12,820 --> 00:11:15,820
had carried over apparently from his past lives.

188
00:11:15,820 --> 00:11:19,820
But we see this with, with so many things in our lives.

189
00:11:19,820 --> 00:11:24,820
We, we're talking about food, for example, and when you,

190
00:11:24,820 --> 00:11:27,820
you want this or want that food and then you make excuses.

191
00:11:27,820 --> 00:11:30,820
Well, like it's good for this or it's good for that.

192
00:11:30,820 --> 00:11:33,820
Or when you want to practice yoga, for example, having an argument

193
00:11:33,820 --> 00:11:36,820
about yoga, you want to practice yoga because you say it,

194
00:11:36,820 --> 00:11:38,820
it's good for your health or it's good for this or it's good

195
00:11:38,820 --> 00:11:39,820
for that.

196
00:11:39,820 --> 00:11:44,820
But the, you know, this is the mind will find many, many,

197
00:11:44,820 --> 00:11:49,820
many excuses to allow us to, to, to, to follow the things that

198
00:11:49,820 --> 00:11:52,820
we enjoy, to follow the things that, that bring us pleasure.

199
00:11:52,820 --> 00:11:57,820
We want, we're, we're talking about even just getting this,

200
00:11:57,820 --> 00:11:59,820
we want to get this drink in the morning.

201
00:11:59,820 --> 00:12:02,820
And, and you can make ulcers of excuses about how good it is

202
00:12:02,820 --> 00:12:05,820
and so we're arguing or we're talking, we're saying,

203
00:12:05,820 --> 00:12:08,820
but it's so good for you.

204
00:12:08,820 --> 00:12:11,820
Well, yes, it's good for us, but it's not good for us to want it.

205
00:12:11,820 --> 00:12:13,820
If you get it, that's great.

206
00:12:13,820 --> 00:12:15,820
It'd be good for your body and so on.

207
00:12:15,820 --> 00:12:18,820
And, and it would, it would even be good for your,

208
00:12:18,820 --> 00:12:21,820
your meditation practice to allow you to, to continue.

209
00:12:21,820 --> 00:12:24,820
But the greed for it, the wanting for it, of course,

210
00:12:24,820 --> 00:12:26,820
that doesn't mean you're going to get it and in fact,

211
00:12:26,820 --> 00:12:30,820
if you do get it, it's going to, it's come about in an

212
00:12:30,820 --> 00:12:35,820
whole some way and you'll, you, that will be a, a far worse

213
00:12:35,820 --> 00:12:37,820
problem in your meditation.

214
00:12:37,820 --> 00:12:40,820
This, this greed for it.

215
00:12:40,820 --> 00:12:44,820
So we, we, we, we have to be quite patient with our

216
00:12:44,820 --> 00:12:47,820
defilements and we shouldn't let our defilements define

217
00:12:47,820 --> 00:12:51,820
either how good our practice is or how bad our practices.

218
00:12:51,820 --> 00:12:54,820
So we shouldn't, we shouldn't take the defilements as being

219
00:12:54,820 --> 00:12:57,820
assigned that we're not practicing well or, or, or

220
00:12:57,820 --> 00:13:00,820
assign that we're, we're a failure in the practice.

221
00:13:00,820 --> 00:13:03,820
But we also shouldn't take the defilements to, you know,

222
00:13:03,820 --> 00:13:07,820
as, as a justify or try to justify them and try to pretend

223
00:13:07,820 --> 00:13:08,820
that they're okay.

224
00:13:08,820 --> 00:13:10,820
So we can get what we want and so on.

225
00:13:10,820 --> 00:13:15,820
We, we do this with our, our robes, you know, we think,

226
00:13:15,820 --> 00:13:17,820
well, I need good robes for this or for that.

227
00:13:17,820 --> 00:13:19,820
Or we do it with our, our lodging.

228
00:13:19,820 --> 00:13:21,820
We do it with all of our, all of our requisites.

229
00:13:21,820 --> 00:13:25,820
It's very easy to find excuses for having good things.

230
00:13:25,820 --> 00:13:28,820
So we use it for our meditation practice.

231
00:13:28,820 --> 00:13:31,820
So for example, yoga, I mean, there's nothing intrinsically

232
00:13:31,820 --> 00:13:35,820
wrong with yoga, yoga can be a good source of concentration

233
00:13:35,820 --> 00:13:39,820
and it can create states of mind that are, are quite stable

234
00:13:39,820 --> 00:13:43,820
and quite, quite powerful.

235
00:13:43,820 --> 00:13:47,820
But the, the justification for it and the idea that somehow it

236
00:13:47,820 --> 00:13:50,820
has this benefit or that benefit and, and, and therefore

237
00:13:50,820 --> 00:13:51,820
the clinging to it.

238
00:13:51,820 --> 00:13:54,820
This is the problem when, when we take these states as being

239
00:13:54,820 --> 00:13:56,820
somehow intrinsically meaningful.

240
00:13:56,820 --> 00:13:59,820
We find some justification and develop a view based on them.

241
00:13:59,820 --> 00:14:02,820
Because of course from a Buddhist point of view, yoga has

242
00:14:02,820 --> 00:14:06,820
associated views and the idea that yoga is of some intrinsic

243
00:14:06,820 --> 00:14:10,820
benefit and, and, and the samadhi that comes from it is,

244
00:14:10,820 --> 00:14:14,820
is actually enlightenment or so on, which is, which is a philosophy

245
00:14:14,820 --> 00:14:16,820
and the Hindu religion.

246
00:14:16,820 --> 00:14:18,820
The same even goes for wisdom.

247
00:14:18,820 --> 00:14:20,820
We can, we can, you know,

248
00:14:20,820 --> 00:14:24,820
when, when we look at the spectrum of Buddhist thought and Buddhist

249
00:14:24,820 --> 00:14:27,820
theory, there are many different theories and opinions and people

250
00:14:27,820 --> 00:14:30,820
even come to the opinion that an arahan, for example, can still

251
00:14:30,820 --> 00:14:33,820
have greed, can still have anger, can still have delusion.

252
00:14:33,820 --> 00:14:36,820
So it's a way of just defying our own defilements because

253
00:14:36,820 --> 00:14:39,820
we think, well, I'm an arahan, but I still have greed, anger

254
00:14:39,820 --> 00:14:42,820
and delusion. Well, then that means an arahan can have greed, anger

255
00:14:42,820 --> 00:14:46,820
and delusion. So we, we give rise to a very, very wrong view

256
00:14:46,820 --> 00:14:49,820
based on our misunderstanding and this is what we call

257
00:14:49,820 --> 00:14:54,820
the crookedness of the mind, the panda nang tapalang, the mind that is

258
00:14:54,820 --> 00:14:57,820
not stable, not, not straight.

259
00:14:57,820 --> 00:15:01,820
And so we have this very powerful teaching that the Buddha gave on

260
00:15:01,820 --> 00:15:05,820
making the mind straight. This, this explanation of, of the Buddha's teaching as

261
00:15:05,820 --> 00:15:11,820
being the ujukaro, ujukara, jitudjukamba,

262
00:15:11,820 --> 00:15:14,820
in the act of making the mind straight.

263
00:15:14,820 --> 00:15:20,820
And this is, this is a, a really good explanation of why we practice, for example,

264
00:15:20,820 --> 00:15:24,820
to, to note things. So when you're walking, to note yourself,

265
00:15:24,820 --> 00:15:27,820
walking as the Buddha said, good tanto wagatami, to your tongue.

266
00:15:27,820 --> 00:15:31,820
Well, why should you say, I am walking? You know that you're walking already?

267
00:15:31,820 --> 00:15:34,820
When you have pain, why should you remind yourself that you have pain?

268
00:15:34,820 --> 00:15:38,820
You know that there's pain, it's no good. It's a cause for suffering.

269
00:15:38,820 --> 00:15:42,820
What we're trying to do is straighten the mind out in regards to the pain.

270
00:15:42,820 --> 00:15:45,820
So instead of thinking, this is my pain, this is bad pain.

271
00:15:45,820 --> 00:15:48,820
Oh, I should move now and justifying yourself.

272
00:15:48,820 --> 00:15:52,820
Saying, wow, if I don't move, maybe I'll get an injury or so on.

273
00:15:52,820 --> 00:15:56,820
Instead of trying to find that justification, which is considered the crookedness of the mind,

274
00:15:56,820 --> 00:16:00,820
we straighten the mind. And the mind becomes straight in regards to the pain.

275
00:16:00,820 --> 00:16:04,820
And it's only aware of this is pain. When there's pain, the mind knows,

276
00:16:04,820 --> 00:16:07,820
there's pain. When, when, when, when, when,

277
00:16:07,820 --> 00:16:12,820
when things, then most of their thoughts, when one has emotions, when knows this emotion,

278
00:16:12,820 --> 00:16:17,820
there's anger, there's greed, there's delusion, liking, disliking, drowsiness and so on.

279
00:16:17,820 --> 00:16:20,820
Whatever exists in the mind, we know that that exists.

280
00:16:20,820 --> 00:16:28,820
This is making the mind straight because clearly that when the mind knows that this is seeing or hearing or pain or so on,

281
00:16:28,820 --> 00:16:34,820
when it knows this is this, this is this, that's the straightest the mind can possibly be.

282
00:16:34,820 --> 00:16:38,820
If the mind says, this is me, this is mine, this is good, this is bad.

283
00:16:38,820 --> 00:16:45,820
It's already subjective. It becomes a crookedness, an entangled, a wavering of the mind.

284
00:16:45,820 --> 00:16:50,820
The mind is no longer fixed on the object itself. So you say, when walking,

285
00:16:50,820 --> 00:16:53,820
I know I'm walking already, well, do you know you're walking?

286
00:16:53,820 --> 00:16:57,820
And how long do you know that you're walking? Maybe for one instant, you know you're walking,

287
00:16:57,820 --> 00:17:02,820
but then you think, well, it's me that's walking and this is good walking or this is bad walking.

288
00:17:02,820 --> 00:17:06,820
I don't want to walk anymore, I'm bored of walking, I'm tired of walking and so on.

289
00:17:06,820 --> 00:17:11,820
It may give rise to all of the partiality, all of the defilements.

290
00:17:11,820 --> 00:17:15,820
So the straightness of the mind is even in regards to the defilements,

291
00:17:15,820 --> 00:17:21,820
when you want something, when you don't like something, when you believe something.

292
00:17:21,820 --> 00:17:27,820
You believe that this is the truth. When you say, I believe this is the truth, that's the truth.

293
00:17:27,820 --> 00:17:32,820
But the truth is that you believe it, but it doesn't mean that what you believe is true.

294
00:17:32,820 --> 00:17:38,820
So even when we believe something, I just say thinking, thinking, or believing, believing if you like.

295
00:17:38,820 --> 00:17:46,820
And this is what makes the mind straight. Not the belief, not the idea, not the theory, not the logic, or so on.

296
00:17:46,820 --> 00:17:53,820
Because all of that can be crooked. You can justify just about anything based on the crookedness of the mind.

297
00:17:53,820 --> 00:17:58,820
So this is the first part, very important, and it's a very clear explanation of what we're trying to do.

298
00:17:58,820 --> 00:18:06,820
Make the mind straight, when you know that you're seeing, when you know that you're hearing,

299
00:18:06,820 --> 00:18:11,820
that hearing is hearing and smelling and tasting, when you know that pain is just pain.

300
00:18:11,820 --> 00:18:15,820
Whatever arises, when you know it for what it is, this is making the mind straight.

301
00:18:15,820 --> 00:18:20,820
And eventually the mind naturally inclines towards this sort of experience.

302
00:18:20,820 --> 00:18:25,820
Pain is just pain, thinking is just thinking, and so on.

303
00:18:25,820 --> 00:18:28,820
So that's verse number 33.

304
00:18:28,820 --> 00:18:35,820
Verse number 34, the Buddha also taught for the same story as a continuation of his explanation.

305
00:18:35,820 --> 00:18:44,820
He uses now, instead of the arrow, he uses the example of a fish, a fish out of water, which is quite interesting.

306
00:18:44,820 --> 00:18:49,820
Because you think, well, in the natural state, the mind is fickle and agitated.

307
00:18:49,820 --> 00:18:52,820
But it's actually not the natural state.

308
00:18:52,820 --> 00:18:55,820
The natural state of the mind would be a mind at rest.

309
00:18:55,820 --> 00:18:59,820
It has to have some cause for it to become agitated.

310
00:18:59,820 --> 00:19:00,820
And this is what happens.

311
00:19:00,820 --> 00:19:02,820
This is what we do throughout our whole lives.

312
00:19:02,820 --> 00:19:07,820
Because if you look at a child's mind, relatively speaking, even though it's already very agitated,

313
00:19:07,820 --> 00:19:10,820
relatively speaking, it's quite simple and quite direct.

314
00:19:10,820 --> 00:19:12,820
If it wants something, it takes it.

315
00:19:12,820 --> 00:19:15,820
If it doesn't want something in cries and so on.

316
00:19:15,820 --> 00:19:21,820
But humans, we are very devious, and we have these stresses and these worries and these concerns,

317
00:19:21,820 --> 00:19:27,820
and these anger and hatred and grudges that children could never have.

318
00:19:27,820 --> 00:19:29,820
And so we've cultivated this.

319
00:19:29,820 --> 00:19:33,820
We've taken our mind out of its natural state.

320
00:19:33,820 --> 00:19:39,820
The Buddha said, pabasarangidhan bhikavaititan, this mind amongst is beautiful.

321
00:19:39,820 --> 00:19:41,820
It's radiant.

322
00:19:41,820 --> 00:19:47,820
But it becomes defiled, based on the defilement, based on the greed and anger and delusion

323
00:19:47,820 --> 00:19:53,820
that we cultivate, that we build up in the mind, that we develop in the mind.

324
00:19:53,820 --> 00:20:00,820
Causing us to not be stable, to not be straight in the mind.

325
00:20:00,820 --> 00:20:05,820
So actually this idea of the fishes is quite apt.

326
00:20:05,820 --> 00:20:11,820
In normal, the way things actually go for most of us is when we come to meditate,

327
00:20:11,820 --> 00:20:15,820
we feel like the meditation is taking us out of the water.

328
00:20:15,820 --> 00:20:18,820
But actually it's quite the opposite.

329
00:20:18,820 --> 00:20:24,820
The meditation is to help bring us back and calm us down and find the water again.

330
00:20:24,820 --> 00:20:31,820
But we become so used to this agitated state that we think when we come to meditate,

331
00:20:31,820 --> 00:20:37,820
we say, let me out here, get me back to my ordinary life because we think that's where the stability is.

332
00:20:37,820 --> 00:20:40,820
But the truth is that the mind is like this and at all times.

333
00:20:40,820 --> 00:20:43,820
The mind is fickle and wavering and all over.

334
00:20:43,820 --> 00:20:46,820
And it's only when we come to meditate that we can see this.

335
00:20:46,820 --> 00:20:51,820
So we think, oh, meditation and Buddhism that takes us out of the water.

336
00:20:51,820 --> 00:20:57,820
But all it does is show us how to the water we are,

337
00:20:57,820 --> 00:21:06,820
how disturbed the mind actually is just as a fish becomes disturbed out of water.

338
00:21:06,820 --> 00:21:11,820
And so through the practice, we're coming to let go of these things.

339
00:21:11,820 --> 00:21:13,820
We're coming to give up the hooks.

340
00:21:13,820 --> 00:21:18,820
It's like a fish that has been hooked by the fishermen and wriggles back and forth.

341
00:21:18,820 --> 00:21:23,820
So we take out the hooks, everything that we desire, everything that we cling to,

342
00:21:23,820 --> 00:21:29,820
everything that we crave, everything that we hate and avoid and so on.

343
00:21:29,820 --> 00:21:31,820
We take this out.

344
00:21:31,820 --> 00:21:35,820
We see the desire, we see the aversion for what it is and we straighten the mind.

345
00:21:35,820 --> 00:21:38,820
And when the mind is straight, it can no longer cling.

346
00:21:38,820 --> 00:21:46,820
And when it doesn't cling anymore, all the hooks are removed and we put back into the water in the sand.

347
00:21:46,820 --> 00:21:56,820
And the Buddha said, he explained how this is, or he remarked how this is the realm of Mara,

348
00:21:56,820 --> 00:21:58,820
or in one sense the bait.

349
00:21:58,820 --> 00:22:01,820
So these hooks are the hooks of Mara.

350
00:22:01,820 --> 00:22:08,820
You know, by Mara we mean, of course, evil, which is all of the greed and anger and delusion.

351
00:22:08,820 --> 00:22:10,820
So he says, this is what we have to let go of.

352
00:22:10,820 --> 00:22:15,820
And in fact, in one way of reading this is that he's telling us to let go of the mind.

353
00:22:15,820 --> 00:22:19,820
And if you like, it means letting go of the mind that is fickle.

354
00:22:19,820 --> 00:22:21,820
Letting go of these mind states.

355
00:22:21,820 --> 00:22:24,820
When the mind wants something, you just see that it wants it.

356
00:22:24,820 --> 00:22:27,820
The hook is your attachment to it.

357
00:22:27,820 --> 00:22:32,820
If you want something and then you think, yes, I'm going to take it, then your hook.

358
00:22:32,820 --> 00:22:36,820
Even if you say no, I'm not going to take it, you get angry and upset at it.

359
00:22:36,820 --> 00:22:38,820
No, no, no.

360
00:22:38,820 --> 00:22:40,820
How bad mind is something.

361
00:22:40,820 --> 00:22:43,820
It's still a clinging, there's still a hook.

362
00:22:43,820 --> 00:22:44,820
You're like, no, no, no.

363
00:22:44,820 --> 00:22:46,820
And you stop, stop, stop.

364
00:22:46,820 --> 00:22:48,820
You're pushing it down.

365
00:22:48,820 --> 00:22:50,820
And as a result, you're stuck with it.

366
00:22:50,820 --> 00:22:54,820
You're stuck to it, even with anger, even with disliking it.

367
00:22:54,820 --> 00:23:00,820
So we never want people to feel guilty or upset when they have addictions or when they have a version,

368
00:23:00,820 --> 00:23:02,820
just to see them clearly.

369
00:23:02,820 --> 00:23:08,820
Because with any kind of liking or disliking, this is the hook, the clinging.

370
00:23:08,820 --> 00:23:12,820
Once you make your mind straight and you just know that is that, that is that.

371
00:23:12,820 --> 00:23:16,820
It feels exactly as though your mind is strained.

372
00:23:16,820 --> 00:23:18,820
And you're no longer clinging to it.

373
00:23:18,820 --> 00:23:19,820
The mind no longer clinging.

374
00:23:19,820 --> 00:23:21,820
Anyway, there's no way it can cling.

375
00:23:21,820 --> 00:23:22,820
And the mind is strained.

376
00:23:22,820 --> 00:23:25,820
So you remove the hook.

377
00:23:25,820 --> 00:23:27,820
This is the hook of Mara.

378
00:23:27,820 --> 00:23:30,820
And this is the pahat, the way where we, the Buddha,

379
00:23:30,820 --> 00:23:34,820
it joins us to give it up and to let go of it.

380
00:23:34,820 --> 00:23:37,820
So that's the teaching for today, just a short story.

381
00:23:37,820 --> 00:23:42,820
But two very important verses, one on the straightening of the mind

382
00:23:42,820 --> 00:23:54,820
and another one on the bringing the mind back to a state of natural purity.

383
00:23:54,820 --> 00:24:01,820
Just as a fish in water, the mind that is free from suffering lives in the world smooth and

384
00:24:01,820 --> 00:24:04,820
goes just like a fish in water.

385
00:24:04,820 --> 00:24:07,820
Most of us, we feel like a fish in water out of water.

386
00:24:07,820 --> 00:24:11,820
We've gotten so comfortable with our, with our state of suffering.

387
00:24:11,820 --> 00:24:14,820
And we think meditation is actually causing suffering.

388
00:24:14,820 --> 00:24:18,820
But it's because only when we come back to meditate, we see what we're doing to ourselves.

389
00:24:18,820 --> 00:24:23,820
And the truth is once you meditate, the more you meditate, the more you realize

390
00:24:23,820 --> 00:24:25,820
what you've been doing to yourself.

391
00:24:25,820 --> 00:24:29,820
And you see, wow, all that suffering I had in my life, all those terrible things

392
00:24:29,820 --> 00:24:32,820
I was doing to other people, all of the problems I was causing,

393
00:24:32,820 --> 00:24:36,820
all of the conflicts that I had, indeed the Buddha was correct.

394
00:24:36,820 --> 00:24:42,820
The mind is fickle, even in our ordinary lives.

395
00:24:42,820 --> 00:24:46,820
But that meditation, the mind will run around.

396
00:24:46,820 --> 00:24:48,820
But because you think this is right, this is good.

397
00:24:48,820 --> 00:24:51,820
You follow after it. You don't ever notice it.

398
00:24:51,820 --> 00:24:56,820
Only when you come to meditate and you come to try to bring the mind back to a natural state.

399
00:24:56,820 --> 00:25:00,820
Do you notice it and you see how crazy the mind is.

400
00:25:00,820 --> 00:25:05,820
So for new meditators, this is a good teaching for us

401
00:25:05,820 --> 00:25:13,820
to realize that this is not a sign that the meditation is wrong or is bad.

402
00:25:13,820 --> 00:25:18,820
And it shouldn't be something for us to become discouraged about.

403
00:25:18,820 --> 00:25:24,820
In the beginning, we're dealing with a mind that is unstable.

404
00:25:24,820 --> 00:25:30,820
And it takes quite a bit of time in things. You have many, many hooks.

405
00:25:30,820 --> 00:25:33,820
You have to remove all the hooks, slowly, slowly, slowly,

406
00:25:33,820 --> 00:25:37,820
before you're going to be stable in a piece.

407
00:25:37,820 --> 00:25:42,820
So reminding us that this is the nature of the mind and not to feel guilty

408
00:25:42,820 --> 00:25:45,820
or not to feel angry or not to hate ourselves for it.

409
00:25:45,820 --> 00:25:49,820
But just to slowly, slowly see our defilements as they are,

410
00:25:49,820 --> 00:25:53,820
see the suffering as it is, see everything just as it is.

411
00:25:53,820 --> 00:25:57,820
And let go of any desire or aversion towards it.

412
00:25:57,820 --> 00:26:24,820
Okay, so thank you for tuning in and that's all for today.

