1
00:00:00,000 --> 00:00:05,640
Hello and welcome back to our study of the Dampanda. Today we continue on with

2
00:00:05,640 --> 00:00:19,640
first number 126, which reads as follows.

3
00:00:19,640 --> 00:00:32,640
Sangang sugatino yante barinim bante anasua, which means some arise in a womb, the evil

4
00:00:32,640 --> 00:00:41,640
doers, apakami, apakama, the evil doers near young, go to hell.

5
00:00:41,640 --> 00:00:49,640
Sangang sugatino yante, the people with good destinations, people who have gone to good,

6
00:00:49,640 --> 00:00:59,640
go to heaven, sanga. Barinim bante anasua, those without any taints are released, go to full release

7
00:00:59,640 --> 00:01:08,640
barinim bante, so four different destinations for four different types of people.

8
00:01:08,640 --> 00:01:17,640
The story behind this is apropos, it's one of the more poignant stories in the Dampanda.

9
00:01:17,640 --> 00:01:26,640
Not long, but it has some quite interesting ramifications.

10
00:01:26,640 --> 00:01:32,640
So the Buddha was dwelling in Jitawanah, and there was a monk Disa, which is the name of a lot of monks,

11
00:01:32,640 --> 00:01:36,640
so it seems to have been a common name at the time.

12
00:01:36,640 --> 00:01:55,640
But he was an arahant, it seems, and for twelve years he had taken his meals in the house of a certain jeweler.

13
00:01:55,640 --> 00:02:01,640
And they had treated him well, like Mother and Father the text says.

14
00:02:01,640 --> 00:02:09,640
They were good people, they weren't enlightened people, but they were good people who were very kind and very devoted to him.

15
00:02:09,640 --> 00:02:21,640
But it so happened that one day that the king, King Paseenadeen, had sent a jewel worth a lot of money

16
00:02:21,640 --> 00:02:36,640
to the jeweler telling him it should be cut and sat in somehow to do something with it, whatever they do with jewels, with precious stones.

17
00:02:36,640 --> 00:02:46,640
And when the messenger brought it, the jeweler was busy cutting up meat, and so his hands were covered in blood.

18
00:02:46,640 --> 00:02:58,640
But even though he picked up the jewel, and he put it in a jewel box in this box, sitting on his desk,

19
00:02:58,640 --> 00:03:03,640
with his hands covered in blood, so it had this sort of blood on it.

20
00:03:03,640 --> 00:03:08,640
Or at least it had the smell of blood on it.

21
00:03:08,640 --> 00:03:19,640
And so then the elder came in for alms, and the jeweler went into the back to prepare food for the monk.

22
00:03:19,640 --> 00:03:27,640
And it so happened that there was a heron, the jeweler had a pet bird, a heron that lived in the house.

23
00:03:27,640 --> 00:03:39,640
And so happens that the heron wandered over to the desk, smelt the blood, and saw this jewel, which was maybe a ruby or something, and mistook it for a piece of meat.

24
00:03:39,640 --> 00:03:43,640
And right in front of the elder ate the jewel.

25
00:03:43,640 --> 00:03:56,640
And the elder watched as this bird popped this jewel into its mouth, swallowed it whole, and then wandered away.

26
00:03:56,640 --> 00:04:06,640
Now the jeweler came back, and this is where it gets interesting, because the jeweler came back and found that his jewel was missing.

27
00:04:06,640 --> 00:04:15,640
And started freaking out, you know, looked at the elder, but didn't want to believe, of course, that the elder took it.

28
00:04:15,640 --> 00:04:25,640
And so he went to his wife, and to his son, and asked them, did you take this jewel? What happened to him?

29
00:04:25,640 --> 00:04:28,640
No, they didn't take it, and so of course he trusts him.

30
00:04:28,640 --> 00:04:33,640
But then he says to his wife, it must have been the monk.

31
00:04:33,640 --> 00:04:38,640
The monk must have seen it, and I can't believe it, but they must have been him.

32
00:04:38,640 --> 00:04:42,640
And his wife scolded him and said, how could you don't say that?

33
00:04:42,640 --> 00:04:58,640
All these years he's come to see us, and he's never exhibited a single flaw, let alone the evil desire that would cause him to steal someone else's belonging.

34
00:04:58,640 --> 00:05:03,640
And so the jeweler accepted this, and went to ask the monk, and said to the monk, did you take the jewel?

35
00:05:03,640 --> 00:05:13,640
So you might wonder, well, why didn't he just say that the heron took it? Why wouldn't the elder just solve the problem for him?

36
00:05:13,640 --> 00:05:22,640
Because you see, the heron has swallowed the jewel, and the only way to get it out, the most reasonable way to get it out would be to kill the heron.

37
00:05:22,640 --> 00:05:32,640
And so in order to protect the heron, he didn't tell the, that was his reason for just saying no, I didn't take it, and keeping quiet.

38
00:05:32,640 --> 00:05:40,640
He wanted to get involved, because he knew the consequences of getting involved for the heron.

39
00:05:40,640 --> 00:05:47,640
And so he says to him, look, there's no one else here, my wife and sons, they say they haven't taken, so it must have been you, there's no one else.

40
00:05:47,640 --> 00:05:50,640
And the other just stayed quiet.

41
00:05:50,640 --> 00:05:58,640
And so he went to his wife, and he said, you know, it's gotta have been the elder, I'm going to torture him, torture him and get him to tell me, because he's freaking out really.

42
00:05:58,640 --> 00:06:04,640
It's this whole deal with kings, you know, the king is not to be trifled with.

43
00:06:04,640 --> 00:06:08,640
And of course, he's not gonna buy that he lost it, that doesn't really work.

44
00:06:08,640 --> 00:06:17,640
So he knows that it's gonna be on his head, if the king finds out that he has lost the jewel.

45
00:06:17,640 --> 00:06:19,640
And he says, so I'm gonna torture him.

46
00:06:19,640 --> 00:06:23,640
And the wife forbids him and says, don't ruin us.

47
00:06:23,640 --> 00:06:29,640
He says, far better for us to become slaves than delay such a charge at the door of the elder.

48
00:06:29,640 --> 00:06:38,640
And the jeweler says, where all of us to become slaves would still be, we would still not be worth the value of the jewel.

49
00:06:38,640 --> 00:06:40,640
The jewel is worth more than our slavery.

50
00:06:40,640 --> 00:06:51,640
You know, the other thing, speaking of slaves, that jewel, you know, we're not just gonna become slaves, we'll probably be executed.

51
00:06:51,640 --> 00:06:54,640
And so he does something curious, I don't quite understand.

52
00:06:54,640 --> 00:07:04,640
He puts a rope around the elder's head, binds it really tight, and then starts hitting him on the head with a stick or something.

53
00:07:04,640 --> 00:07:11,640
Blood streamed from the elder's head in years and nostrils, and his eyes looked as though they would pop out of their sockets.

54
00:07:11,640 --> 00:07:13,640
He really got a beating.

55
00:07:13,640 --> 00:07:21,640
And he never once said that the hair and ate the darn thing.

56
00:07:21,640 --> 00:07:29,640
And so he overwhelmed with pain and blood coming out of his oracles as he fell down on the ground as he was being beaten.

57
00:07:29,640 --> 00:07:36,640
And the hair and smelling the blood came over and started to drink this elder's blood.

58
00:07:36,640 --> 00:07:45,640
And the jeweler saw this, and he was overwhelmed with anger and just consumed with his horrible evil mind state.

59
00:07:45,640 --> 00:07:52,640
The elder, the hair, and then hit it with the stick and killed it.

60
00:07:52,640 --> 00:08:02,640
And the elder saw this through his bloody haze and asked the livers and said, is the hair and dead?

61
00:08:02,640 --> 00:08:06,640
And then the livers and said, you're next, you're going to be dead.

62
00:08:06,640 --> 00:08:10,640
If you don't tell me where that jewel is, you're going to be dead just like it.

63
00:08:10,640 --> 00:08:18,640
So taking that as a sign, as an answer that this hair and dead, the other said immediately, said it was the hair and the swallowed the jewel.

64
00:08:18,640 --> 00:08:24,640
I said, however, had the hair and not died, I would sooner have died myself.

65
00:08:24,640 --> 00:08:28,640
Then I told you it became the jewel.

66
00:08:28,640 --> 00:08:39,640
I had disbelieving that he pulled apart the hair and opened up its innards, and sure enough found the jewel.

67
00:08:39,640 --> 00:08:44,640
And yeah, you can imagine how he felt at that point.

68
00:08:44,640 --> 00:08:52,640
He kneeled down, prostrate at the elder's feet, asked his forgiveness, and please forgive me.

69
00:08:52,640 --> 00:08:59,640
That was just out of ignorance.

70
00:08:59,640 --> 00:09:03,640
And the elder, in the manner of enlightened beings, said it wasn't your fault.

71
00:09:03,640 --> 00:09:04,640
It wasn't my fault.

72
00:09:04,640 --> 00:09:07,640
It's the fault of the round of Zamsara.

73
00:09:07,640 --> 00:09:11,640
So it's a part of existence to suffer like this.

74
00:09:11,640 --> 00:09:13,640
This is a part of the game.

75
00:09:13,640 --> 00:09:19,640
It's just how it goes totally absorbing the guy, right?

76
00:09:19,640 --> 00:09:30,640
And so still mortified, the jeweler says, well, then please continue to come here, really don't.

77
00:09:30,640 --> 00:09:36,640
Please don't stop coming here just because I beat you almost to death.

78
00:09:36,640 --> 00:09:43,640
And the elder gave a really sort of moving, moving speech.

79
00:09:43,640 --> 00:09:49,640
He said, it was because I came into someone's, because I'm going into people's houses.

80
00:09:49,640 --> 00:09:50,640
This is actually a thing.

81
00:09:50,640 --> 00:09:52,640
Some monks will not go into people's houses.

82
00:09:52,640 --> 00:09:56,640
They'll only take what's given at the door because this sort of thing happens.

83
00:09:56,640 --> 00:10:00,640
You get in trouble when you go into people's houses.

84
00:10:00,640 --> 00:10:02,640
The rules are really interesting.

85
00:10:02,640 --> 00:10:09,640
And you really start to get a feel for wanting to keep them for reasons like not this,

86
00:10:09,640 --> 00:10:13,640
but this sort of thing that happens, you start to.

87
00:10:13,640 --> 00:10:15,640
And being a monk is kind of like that.

88
00:10:15,640 --> 00:10:22,640
So anyway, he says, henceforth, I shall never receive arms under anyone's house.

89
00:10:22,640 --> 00:10:27,640
But the interesting thing is whether he was trying to just spare the man's feelings,

90
00:10:27,640 --> 00:10:40,640
but he knew, it seems like he may have known that he was dying because actually he died as a result of the injury.

91
00:10:40,640 --> 00:10:47,640
So what he said is he actually spoke a stand that isn't part of the Dhamapada, but it's in the commentary.

92
00:10:47,640 --> 00:10:53,640
He said, food is cooked for the sage, a little hair and a little there in one house after another.

93
00:10:53,640 --> 00:10:58,640
I will journey about on my round for arms, a good stout leg is mine.

94
00:10:58,640 --> 00:11:00,640
Those were the last words, maybe on the other.

95
00:11:00,640 --> 00:11:07,640
Not long after he spoke them, he passed into Nipapada.

96
00:11:07,640 --> 00:11:14,640
The hair and was born, if you're interested, as the story goes, was born in the mother's womb,

97
00:11:14,640 --> 00:11:20,640
the wife, as her son or daughter.

98
00:11:20,640 --> 00:11:25,640
The husband, when he passed away, was born in hell.

99
00:11:25,640 --> 00:11:32,640
And the wife was born in heaven, as a result of never doubting the elder

100
00:11:32,640 --> 00:11:37,640
and trying to stop her husband from being such a jerk.

101
00:11:37,640 --> 00:11:49,640
So the monks asked the Buddha about these four beings, the hair and the wife, the husband and the monk.

102
00:11:49,640 --> 00:11:52,640
Or they maybe just asked about the monk.

103
00:11:52,640 --> 00:11:55,640
So the Buddha said, monks, living beings here in this world,

104
00:11:55,640 --> 00:11:59,640
some reenter the womb, others who are evil, doers go to hell.

105
00:11:59,640 --> 00:12:04,640
Others who have done good deeds go to the world of the gods, the world of the angels.

106
00:12:04,640 --> 00:12:08,640
Well, they that have written themselves of the tanes passed to Nipapada.

107
00:12:08,640 --> 00:12:11,640
And so he taught this verse.

108
00:12:11,640 --> 00:12:18,640
So the most interesting part is the part of the story where the elder doesn't give up the hair in.

109
00:12:18,640 --> 00:12:28,640
It doesn't save himself by condemning the hair into death, which is typical of enlightened beings.

110
00:12:28,640 --> 00:12:30,640
I mean, it's really an understatement.

111
00:12:30,640 --> 00:12:35,640
It's one of the most profound beauties of an enlightened being.

112
00:12:35,640 --> 00:12:39,640
There's a purity that surprises.

113
00:12:39,640 --> 00:12:41,640
I mean, you think, okay, they're pure.

114
00:12:41,640 --> 00:12:43,640
But surely they're not going to give up themselves.

115
00:12:43,640 --> 00:12:46,640
They're not going to allow themselves to be tortured.

116
00:12:46,640 --> 00:12:48,640
But yes, actually, they're pure.

117
00:12:48,640 --> 00:12:52,640
Their minds are so pure that being beaten to death doesn't bother them.

118
00:12:52,640 --> 00:12:54,640
It's that much strength.

119
00:12:54,640 --> 00:12:56,640
It's really is an invincibility.

120
00:12:56,640 --> 00:12:59,640
It's an example for all of us.

121
00:12:59,640 --> 00:13:04,640
Ask yourself, if I was being beaten to death, how would I feel?

122
00:13:04,640 --> 00:13:07,640
And then curious is what he said.

123
00:13:07,640 --> 00:13:17,640
I mean, the corollary is that because to be upset, even when being beaten to death,

124
00:13:17,640 --> 00:13:19,640
it's to misunderstand.

125
00:13:19,640 --> 00:13:22,640
I mean, being beaten to death is just part of reality.

126
00:13:22,640 --> 00:13:24,640
There's no person being beaten.

127
00:13:24,640 --> 00:13:25,640
It's just an experience.

128
00:13:25,640 --> 00:13:28,640
The body is being altered.

129
00:13:28,640 --> 00:13:31,640
And there's experiences of pain or rising.

130
00:13:31,640 --> 00:13:33,640
But there's still just experiences.

131
00:13:33,640 --> 00:13:36,640
There's nothing good or bad about them.

132
00:13:36,640 --> 00:13:39,640
And so he had no problem forgiving this guy

133
00:13:39,640 --> 00:13:41,640
or even telling him that there was nothing to forgive

134
00:13:41,640 --> 00:13:43,640
because in the eyes of an enlightened being,

135
00:13:43,640 --> 00:13:45,640
there really is nothing to forgive.

136
00:13:45,640 --> 00:13:47,640
It's just part of Sam Zara.

137
00:13:47,640 --> 00:13:52,640
There's mind states arise and the mind states lead to body states.

138
00:13:52,640 --> 00:13:56,640
The body states arise, lead to mind states.

139
00:13:56,640 --> 00:13:57,640
And so on.

140
00:13:57,640 --> 00:14:00,640
So body is a cause for mind, mind is a cause for body.

141
00:14:00,640 --> 00:14:03,640
It's just how things go.

142
00:14:03,640 --> 00:14:07,640
There's no good and no bad.

143
00:14:07,640 --> 00:14:09,640
In the end.

144
00:14:09,640 --> 00:14:13,640
But for those of us living in Sam Zara and concerned,

145
00:14:13,640 --> 00:14:19,640
it's worth noting that not every path leads to the same destination.

146
00:14:19,640 --> 00:14:21,640
I mean, they all still lead to Sam Zara.

147
00:14:21,640 --> 00:14:24,640
Actually going to heaven is part of Sam Zara going to hell.

148
00:14:24,640 --> 00:14:29,640
It's also part of Sam Zara being born in a womb, also part of Sam Zara.

149
00:14:29,640 --> 00:14:36,640
The only thing that's not is how enlightened beings go to need bad.

150
00:14:36,640 --> 00:14:43,640
And as sawa, those without as sawa, those without teams.

151
00:14:43,640 --> 00:14:45,640
So how this relates to our practice,

152
00:14:45,640 --> 00:14:49,640
I think the most important point is this example that the Arahan sets

153
00:14:49,640 --> 00:14:53,640
for us, this purity.

154
00:14:53,640 --> 00:14:56,640
Not being concerned for one's own welfare.

155
00:14:56,640 --> 00:14:58,640
Not even taking that into account.

156
00:14:58,640 --> 00:15:06,640
Now, you could argue that in most cases an Arahan and it seems

157
00:15:06,640 --> 00:15:12,640
they wouldn't be concerned in the sense of concern for the other person's welfare.

158
00:15:12,640 --> 00:15:15,640
And obviously, it's a tragedy.

159
00:15:15,640 --> 00:15:18,640
The tragedy is not that the Arahan was beaten.

160
00:15:18,640 --> 00:15:21,640
Although it's a tragedy that he had to die,

161
00:15:21,640 --> 00:15:25,640
because Arahan is the greatest of beings.

162
00:15:25,640 --> 00:15:35,640
But the great tragedy or the great tragedy is for the jeweler.

163
00:15:35,640 --> 00:15:39,640
He's the one who you should feel sad for, feel sorry for.

164
00:15:39,640 --> 00:15:48,640
And there's a sense that Arahans are perfectly fine with that with death.

165
00:15:48,640 --> 00:15:50,640
It leads to freedom.

166
00:15:50,640 --> 00:15:54,640
For an Arahan death is freedom.

167
00:15:54,640 --> 00:15:59,640
So there's no problem there, but for this jeweler,

168
00:15:59,640 --> 00:16:00,640
there's a big problem.

169
00:16:00,640 --> 00:16:03,640
I mean, it's the concept of how suffering doesn't lead to suffering.

170
00:16:03,640 --> 00:16:05,640
Happiness doesn't lead to happiness.

171
00:16:05,640 --> 00:16:08,640
It's not the case that suffering is bad.

172
00:16:08,640 --> 00:16:11,640
Suffering is not bad, because it doesn't lead to suffering.

173
00:16:11,640 --> 00:16:16,640
Which is an interesting thing, but evil is bad.

174
00:16:16,640 --> 00:16:18,640
Evil leads to suffering.

175
00:16:18,640 --> 00:16:20,640
I mean, this is still conventional,

176
00:16:20,640 --> 00:16:24,640
because again, good and evil are just part of some sorrow.

177
00:16:24,640 --> 00:16:27,640
In the end, true goodness is rising above them,

178
00:16:27,640 --> 00:16:30,640
freeing yourself from attachment to good deeds.

179
00:16:30,640 --> 00:16:32,640
In the sense of, oh, I must do more good deeds.

180
00:16:32,640 --> 00:16:34,640
Or wanting to do more good deeds, right?

181
00:16:34,640 --> 00:16:37,640
Even attachment to that will lead you to be reborn.

182
00:16:37,640 --> 00:16:42,640
It happens in nice places, in places that are conducive to meditation.

183
00:16:42,640 --> 00:16:46,640
But in the end, it can't be a goal in itself.

184
00:16:46,640 --> 00:16:51,640
But still, for your information, good and evil,

185
00:16:51,640 --> 00:16:52,640
two different things.

186
00:16:52,640 --> 00:16:56,640
Nohim dhammo a dhammo japubu santamipakana.

187
00:16:56,640 --> 00:16:59,640
Dhamma, the graciousness and unrighteousness,

188
00:16:59,640 --> 00:17:02,640
don't have the same result.

189
00:17:02,640 --> 00:17:06,640
So some are born in heaven, some are born in hell.

190
00:17:06,640 --> 00:17:08,640
For those of us who practice meditation,

191
00:17:08,640 --> 00:17:11,640
it's the example of purifying our minds,

192
00:17:11,640 --> 00:17:14,640
seeing things clearly and observing,

193
00:17:14,640 --> 00:17:17,640
and really taking this into account.

194
00:17:17,640 --> 00:17:19,640
If this are a hunt, we could do that,

195
00:17:19,640 --> 00:17:21,640
but we ask ourselves how far we are.

196
00:17:21,640 --> 00:17:24,640
How much pain, how much suffering are we able to tolerate?

197
00:17:24,640 --> 00:17:26,640
When do we start complaining?

198
00:17:26,640 --> 00:17:28,640
Do we start getting upset?

199
00:17:28,640 --> 00:17:30,640
How patient can we be?

200
00:17:30,640 --> 00:17:34,640
And what are we able to be patient with in our practice?

201
00:17:34,640 --> 00:17:37,640
So this kind of example of the dhammo pad,

202
00:17:37,640 --> 00:17:39,640
there's full of these kind of things that give us good examples

203
00:17:39,640 --> 00:17:43,640
in our practice, and so it's a good lesson for that reason.

204
00:17:43,640 --> 00:17:46,640
It's also a curious thing,

205
00:17:46,640 --> 00:17:54,640
because I think many people wouldn't have that sense of the sort

206
00:17:54,640 --> 00:17:58,640
of the conscientiousness of an oven in light and being

207
00:17:58,640 --> 00:18:00,640
to not betray someone else,

208
00:18:00,640 --> 00:18:04,640
to not save themselves at the expense of another.

209
00:18:04,640 --> 00:18:07,640
So, okay, that's the dhammo pad over tonight.

210
00:18:07,640 --> 00:18:09,640
Thank you all for tuning in.

211
00:18:09,640 --> 00:18:13,640
Keep practicing, give up.

