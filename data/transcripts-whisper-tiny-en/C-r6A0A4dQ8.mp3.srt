1
00:00:00,000 --> 00:00:08,000
Hi, today I'd like to talk to you all about a project that we've begun here in Ontario, Canada.

2
00:00:08,000 --> 00:00:20,000
As many of you may know, I've returned to Canada with the intention of continuing to spread the dhamma here in my own country.

3
00:00:20,000 --> 00:00:32,000
The reasons for that include the fact that it's easier and more convenient for me in a place where first of all I went past part.

4
00:00:32,000 --> 00:00:44,000
I don't have to worry about visas and also where the culture is familiar and where I'm able to integrate with the society.

5
00:00:44,000 --> 00:00:55,000
So that was the idea and I've been back in Canada for a while now and we've decided I've got a few people here who have gotten into helping.

6
00:00:55,000 --> 00:00:59,000
So we've decided to take it to the next level.

7
00:00:59,000 --> 00:01:15,000
Our goal is to move into a house of our own nearby to the university here in Hamilton, Ontario.

8
00:01:15,000 --> 00:01:26,000
In September I'll be going back to university and part of the reason for that is to be closer to the sort of people who'd be interested in the things that we do.

9
00:01:26,000 --> 00:01:38,000
Because one of the things that I found difficult is actually getting involved or getting into back into the society here.

10
00:01:38,000 --> 00:01:51,000
Back in with fellow Buddhists because of our location right now I'm in an area where there are not so many people and certainly not so many Buddhists.

11
00:01:51,000 --> 00:01:58,000
And so by going back to university the idea is to get back into the swing of things.

12
00:01:58,000 --> 00:02:07,000
I'm going to be involved with the Religious Studies Department there and we've applied to start a Buddhism Society.

13
00:02:07,000 --> 00:02:15,000
The idea will be that there will be more of a local community here interested in the meditation practice.

14
00:02:15,000 --> 00:02:30,000
I know I've got a lot of people following on the internet but the difficulty with that is arranging things like food for meditators and the lodging and sort of local coordination.

15
00:02:30,000 --> 00:02:33,000
Even just things like recording videos.

16
00:02:33,000 --> 00:02:42,000
Most they have to do by myself because there's no one here who is of a mind to help.

17
00:02:42,000 --> 00:02:50,000
So the goal is that in January I'll be halfway done the year at McMaster University.

18
00:02:50,000 --> 00:02:56,000
By January 1st we're going to try to rent a house near the university.

19
00:02:56,000 --> 00:03:04,000
There's a lot of student housing available and so we'll find a house that's not too expensive and begin by renting.

20
00:03:04,000 --> 00:03:19,000
And we will establish that as our meditation center and monastery and it will be a place that we can run and use to set up a studio.

21
00:03:19,000 --> 00:03:33,000
Right now I mean a single room here that's what I've got but we would be able to set up a studio and have someone from our new Buddhism Society at McMaster help out with recording videos.

22
00:03:33,000 --> 00:03:38,000
Maybe even have someone move in to take care of the place.

23
00:03:38,000 --> 00:03:51,000
But so the reason for making this video is that to let you know about the project and how it's being run by some very kind and devoted people who are interested in making this happen.

24
00:03:51,000 --> 00:04:02,000
So there's information on one of the crowdsourcing websites where this video is going to be placed and they're looking for support.

25
00:04:02,000 --> 00:04:13,000
If you would like to support this project please get in contact with the organizers and how about help us to make this possible.

26
00:04:13,000 --> 00:04:21,000
It's most likely going to happen. What we're looking for now is the support to make it happen in January.

27
00:04:21,000 --> 00:04:35,000
To be able to move into a place and take over a house for a year and get a year's lease on a building where we can hold meditation courses.

28
00:04:35,000 --> 00:04:51,000
That will be close enough to people close enough to the university that will be able to hold daily sessions, give daily talks, live to a live audience and teach meditation on a daily basis.

29
00:04:51,000 --> 00:05:10,000
And to just sort of begin to create a presence where we can eventually turn into a real and lasting resource, both locally and by extension internationally.

30
00:05:10,000 --> 00:05:22,000
So there you are. I don't want to say too much. I obviously can't get too much involved in the workings of it, but thank you all for your support and wishing you all the best.

31
00:05:22,000 --> 00:05:44,000
We'll keep track.

