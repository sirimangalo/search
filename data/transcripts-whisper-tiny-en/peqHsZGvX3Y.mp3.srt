1
00:00:00,000 --> 00:00:09,100
Hi, so this is an answer to a question on whether women can enter the monk's life.

2
00:00:09,100 --> 00:00:14,900
And the short answer is, of course, yes.

3
00:00:14,900 --> 00:00:19,100
And without getting into too much detail about it, just to point out that it's still

4
00:00:19,100 --> 00:00:20,420
quite difficult.

5
00:00:20,420 --> 00:00:29,980
And I think the biggest difficulty is just that there are so few female monks in existence.

6
00:00:29,980 --> 00:00:36,540
So not only is it difficult to ordain, but I think even more so it's difficult to find a place

7
00:00:36,540 --> 00:00:43,780
to stay, because a lot of the female monks are being supported by a small group of people

8
00:00:43,780 --> 00:00:50,860
and don't have the room or the facilities to accommodate more.

9
00:00:50,860 --> 00:00:59,060
That being said, if you're really interested, there are ways to get there, and you should

10
00:00:59,060 --> 00:01:05,860
understand that it's a long process, becoming a monk for anyone should be a slow and gradual

11
00:01:05,860 --> 00:01:09,620
process to be sure that you're really ready and really, really know what you're getting

12
00:01:09,620 --> 00:01:11,220
yourself into.

13
00:01:11,220 --> 00:01:16,740
If you're really interested in it, the best and really the only thing that I can suggest

14
00:01:16,740 --> 00:01:27,700
is that you can get in touch with me or with my group, and there is a possibility that once

15
00:01:27,700 --> 00:01:32,420
we have a place established, I mean even in the next year or so, that I'll be having

16
00:01:32,420 --> 00:01:39,220
females ordain as nuns as monks.

17
00:01:39,220 --> 00:01:48,180
So I hope that it clears a little bit up, just to encourage you in this idea, the thought

18
00:01:48,180 --> 00:01:54,340
of getting away from the sensual world and entering into a lifestyle that is more conducive

19
00:01:54,340 --> 00:01:56,740
towards meditation and peace.

20
00:01:56,740 --> 00:01:58,500
So thanks for the question and getting coming.

