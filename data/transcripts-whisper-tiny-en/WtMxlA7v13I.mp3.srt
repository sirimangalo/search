1
00:00:00,000 --> 00:00:12,760
Good evening everyone. Welcome to our live podcast. We're back to the old way of broadcasting.

2
00:00:12,760 --> 00:00:22,560
Seems like the new way is somewhat broken still. Quality apparently wasn't all that good.

3
00:00:22,560 --> 00:00:31,360
We're looking at the end good-turning guy again. We're going to skip ahead into chapter 5.

4
00:00:31,360 --> 00:00:40,160
Chapter 4 is similar to chapter 3.

5
00:00:40,160 --> 00:00:47,360
We're chapters 2, 3, and 4 are quite similar.

6
00:00:47,360 --> 00:01:04,560
The number 5 gives us at least some interesting similes and teachings in regards to the nature of the mind and the development of the mind.

7
00:01:04,560 --> 00:01:27,960
And the Buddha gives us the simile of a shift. It says a spike, but I don't know if it's called a spike, but a piece of a rice plant or a barley plant.

8
00:01:27,960 --> 00:01:37,160
If you've ever been a barley farmer, wheat farmer, rice farmer, you know that these things are quite sharp.

9
00:01:37,160 --> 00:01:47,560
And they can pierce your hand or your foot if they come in contact in just the right way.

10
00:01:47,560 --> 00:02:13,760
If it comes in contact in the wrong way, it's not going to pierce your hand or your foot.

11
00:02:13,760 --> 00:02:25,960
In most cases, the rice or barley plants won't do that as if they're misdirected.

12
00:02:25,960 --> 00:02:32,360
And he says in the same way, this is a simile, so this is like the mind.

13
00:02:32,360 --> 00:02:40,760
If the mind is misdirected, mostly the mind can't see things clearly.

14
00:02:40,760 --> 00:02:48,560
An ordinary mind doesn't fall into wisdom, you can't just suddenly become wise.

15
00:02:48,560 --> 00:02:54,860
To be able to pierce ignorance around true knowledge and realize nibana.

16
00:02:54,860 --> 00:03:01,460
Mostly we don't, right? We don't give rice to wisdom.

17
00:03:01,460 --> 00:03:05,260
The ordinary mind is unable to penetrate.

18
00:03:05,260 --> 00:03:16,460
In fact, even if you're seeking out wisdom, asking for the soft vocal questions, it's still very difficult to come across actual wisdom.

19
00:03:16,460 --> 00:03:20,860
For the most part, the mind is unable to pierce ignorance.

20
00:03:20,860 --> 00:03:22,460
Mind is clouded by ignorance.

21
00:03:22,460 --> 00:03:31,160
No matter where you look, how far you go, how deep you investigate, unless you can direct the mind properly,

22
00:03:31,160 --> 00:03:37,760
cultivate the qualities of mind that can pierce the ignorance.

23
00:03:37,760 --> 00:03:52,960
So the words of the Buddha pierce the ignorance, looking at the translation, actually.

24
00:03:52,960 --> 00:04:22,760
Now we young, majorty, majorty, the cut, actually not pierces it, completely, to destroy, to split, to sander, ignorance, breakthrough the ignorance.

25
00:04:22,760 --> 00:04:35,760
But a well-directed spike of rice or barley plant will be able to pierce your hand to your foot, cause a lot of pain.

26
00:04:35,760 --> 00:04:48,760
But it pierces, and this is how the well-directed mind is able to pierce ignorance around true knowledge and realize nibana.

27
00:04:48,760 --> 00:04:59,760
I think if it goes without saying, if you're familiar with our technique, it's easy to see the application here.

28
00:04:59,760 --> 00:05:19,760
You know, it's out of not paying much attention to words of the Buddha in this vein, and it becomes so much suspicious of the sort of piercing technique involved in our tradition.

29
00:05:19,760 --> 00:05:32,760
Ordinary meditation is quite comfortable, it's about watching the breath or looking at the body, examining things, but it's not piercing.

30
00:05:32,760 --> 00:05:48,760
Ancient meditation was very much about using a word to pierce through the ordinary state of the mind and to cut to that object, whether it be a samata object, the concept.

31
00:05:48,760 --> 00:05:53,760
Or that we pass an object, the reality.

32
00:05:53,760 --> 00:05:58,760
So when we say to ourselves, pain, pain, we're piercing through, this is a piercing.

33
00:05:58,760 --> 00:06:02,760
It's a penetrating sort of technique.

34
00:06:02,760 --> 00:06:04,760
You can see that right away.

35
00:06:04,760 --> 00:06:14,760
Some people don't like it for that reason, it's too strong or too hard.

36
00:06:14,760 --> 00:06:21,760
It's not soft or comfortable enough, it's quite uncomfortable to jab right at the problem, right?

37
00:06:21,760 --> 00:06:25,760
Because then you have to deal with it and it doesn't make it go away.

38
00:06:25,760 --> 00:06:31,760
You feel pain, you say pain, but it doesn't make the pain go away.

39
00:06:31,760 --> 00:06:36,760
It's a challenge to the whole way we look at things.

40
00:06:36,760 --> 00:06:40,760
It's penetrating, piercing.

41
00:06:40,760 --> 00:06:49,760
It cuts through all the bull, all the rubbish, all the ignorance in the mind.

42
00:06:49,760 --> 00:06:51,760
It cuts right through it.

43
00:06:51,760 --> 00:06:54,760
There's no doubting about this practice, how you're doing it.

44
00:06:54,760 --> 00:06:55,760
Am I doing it right?

45
00:06:55,760 --> 00:06:59,760
It's just quite simple to do, you see.

46
00:06:59,760 --> 00:07:09,760
It's not like it's complicated or convoluted or difficult to understand technique.

47
00:07:09,760 --> 00:07:11,760
So it cuts through all the...

48
00:07:11,760 --> 00:07:14,760
There's no wiggle room, right?

49
00:07:14,760 --> 00:07:18,760
It cuts through all that.

50
00:07:18,760 --> 00:07:20,760
And it will chaff.

51
00:07:20,760 --> 00:07:25,760
It gets right to the heart of the matter.

52
00:07:25,760 --> 00:07:27,760
Those are the first two cities.

53
00:07:27,760 --> 00:07:38,760
The third and the fourth one is the Buddha talking about the result of a good mind, a corrupted mind.

54
00:07:38,760 --> 00:07:40,760
Plastic.

55
00:07:40,760 --> 00:07:42,760
Plastic is probably not favored.

56
00:07:42,760 --> 00:07:44,760
The Buddha tells it down.

57
00:07:44,760 --> 00:07:47,760
The Sun, the Buddha tells it down.

58
00:07:47,760 --> 00:07:55,760
The Sun means it's a good mind.

59
00:07:55,760 --> 00:07:58,760
It's a very direct translation.

60
00:07:58,760 --> 00:07:59,760
It's bright.

61
00:07:59,760 --> 00:08:02,760
It can't just mean it's bright or pure mind.

62
00:08:02,760 --> 00:08:04,760
That's it, pure is the best.

63
00:08:04,760 --> 00:08:09,760
The Buddha means corrupted.

64
00:08:09,760 --> 00:08:21,760
The Buddha says, here, I have a certain type of person.

65
00:08:21,760 --> 00:08:28,760
I have encompassed his mind with mine or their mind with my own mind.

66
00:08:28,760 --> 00:08:33,760
It's a power of the Buddha, power of meditative, people of practice.

67
00:08:33,760 --> 00:08:38,760
Certain types of meditation, they're able to penetrate the minds of others

68
00:08:38,760 --> 00:08:44,760
and envelop their minds and see what their state of their mind is.

69
00:08:44,760 --> 00:08:48,760
And in doing so, to someone whose mind was corrupt,

70
00:08:48,760 --> 00:08:53,760
I was able to see Imam Hiji, Ayyang, Sammaji,

71
00:08:53,760 --> 00:08:56,760
Pungulok, Alang, got a yatab,

72
00:08:56,760 --> 00:09:06,760
but Niki, to wave on Niki.

73
00:09:06,760 --> 00:09:11,760
If at this time,

74
00:09:11,760 --> 00:09:17,760
if in this time Imam Hiji, Ayyang, Sammaji,

75
00:09:17,760 --> 00:09:27,760
no, Ayyang, there, Niki, Pungulok, this person,

76
00:09:27,760 --> 00:09:33,760
and he'd make an end to his time or to die.

77
00:09:33,760 --> 00:09:40,760
Just as if placed there would fall into hell.

78
00:09:40,760 --> 00:09:51,760
As the picked up and dropped, thus he wouldn't go to hell.

79
00:09:51,760 --> 00:09:55,760
Those people who think the Buddha didn't talk about heaven and hell.

80
00:09:55,760 --> 00:09:57,760
There's so many passages like this.

81
00:09:57,760 --> 00:10:04,760
In fact, this exact passage occurs in many, many places,

82
00:10:04,760 --> 00:10:11,760
when the Buddha talks about, at the breakup of the body.

83
00:10:11,760 --> 00:10:15,760
Ayyang, Niki, Kati, Satakayya, Sammaji,

84
00:10:15,760 --> 00:10:17,760
the breakup of the body.

85
00:10:17,760 --> 00:10:20,760
Parang, Marana, after death,

86
00:10:20,760 --> 00:10:26,760
aayang, Niki, Niki, Pang, Niki, Niki.

87
00:10:26,760 --> 00:10:36,760
He rises in hell and going to a scone to a fallen into a suffering.

88
00:10:36,760 --> 00:10:38,760
We'll do good thing.

89
00:10:38,760 --> 00:10:40,760
Bad destination.

90
00:10:40,760 --> 00:10:42,760
Me and Niki brought down the bad place.

91
00:10:42,760 --> 00:10:50,760
Niki young hell rises.

92
00:10:50,760 --> 00:10:53,760
You go to hell because of your state of mind.

93
00:10:53,760 --> 00:10:55,760
It's not actually because of the deeds that we do.

94
00:10:55,760 --> 00:10:58,760
This leading people think karma leads to suffering karma,

95
00:10:58,760 --> 00:11:00,760
in terms of the deeds doesn't.

96
00:11:00,760 --> 00:11:03,760
It's the state of mind.

97
00:11:03,760 --> 00:11:07,760
In fact, there's not even the deeds that we've done during our life exactly.

98
00:11:07,760 --> 00:11:11,760
At the last moment, what is your state of mind?

99
00:11:11,760 --> 00:11:15,760
What is it that's leading you on at that time?

100
00:11:15,760 --> 00:11:18,760
When you die, what is the mind?

101
00:11:18,760 --> 00:11:24,760
Is it a mind of desire, a mind of aversion, a mind of fear, a mind of anger?

102
00:11:24,760 --> 00:11:33,760
A mind of worry or confusion?

103
00:11:33,760 --> 00:11:42,760
Where is it a clear mind?

104
00:11:42,760 --> 00:11:47,760
Now it goes without saying that the state of your mind when you die is very much dependent on

105
00:11:47,760 --> 00:11:53,760
the states of mind during your life because they're habitual.

106
00:11:53,760 --> 00:12:05,760
If you cultivate a certain type of mind during your life, the chances of that being the dominant mind when you die are much greater.

107
00:12:05,760 --> 00:12:10,760
And so we see another important benefit of meditation practice that it cultivates

108
00:12:10,760 --> 00:12:19,760
mind states that we know to be positive, that we know to be healthy, pure, clear,

109
00:12:19,760 --> 00:12:22,760
conducive to happiness.

110
00:12:22,760 --> 00:12:28,760
Cultivation of the moment of death is such an important moment.

111
00:12:28,760 --> 00:12:33,760
You shouldn't discard or marginalize this sort of teaching.

112
00:12:33,760 --> 00:12:35,760
The world of death is very important.

113
00:12:35,760 --> 00:12:39,760
If you haven't become enlightened, you're going to be reborn again.

114
00:12:39,760 --> 00:12:44,760
And your rebirth depends very much on your state of mind when you die.

115
00:12:44,760 --> 00:12:52,760
Which in turn depends very much on the habit that you have cultivated during your life.

116
00:12:52,760 --> 00:13:01,760
Be they wholesome or not wholesome.

117
00:13:01,760 --> 00:13:02,760
There we have it.

118
00:13:02,760 --> 00:13:04,760
No, that's three and four or five and six.

119
00:13:04,760 --> 00:13:06,760
We have more similarly.

120
00:13:06,760 --> 00:13:13,760
We're going to talk about a pool of water that is cloudy, turbid and muddy.

121
00:13:13,760 --> 00:13:19,760
Imagine you have a pool, you know those tide pools by the ocean when I was in California.

122
00:13:19,760 --> 00:13:24,760
Many years ago, I was 13 years old and we went and saw the tide pools.

123
00:13:24,760 --> 00:13:30,760
And there were little hermit crabs and all sorts of interesting creatures.

124
00:13:30,760 --> 00:13:34,760
Because the pools were clear, you could see them.

125
00:13:34,760 --> 00:13:40,760
But if the pool of water is cloudy, turbid and muddy, you won't be able to see seashells,

126
00:13:40,760 --> 00:13:44,760
and pebbles and shoals of fish swimming about and resting.

127
00:13:44,760 --> 00:13:49,760
For what reason? Because the water is cloudy.

128
00:13:49,760 --> 00:13:52,760
Notice I'd have to do without our teaching well.

129
00:13:52,760 --> 00:13:57,760
If the mind is cloudy, it's impossible for one to know.

130
00:13:57,760 --> 00:14:02,760
One's own good, the good of others, the good of both.

131
00:14:02,760 --> 00:14:07,760
Or to realize superhuman distinction and knowledge and vision worthy of the noble ones.

132
00:14:07,760 --> 00:14:13,760
For what reason? Because his mind is cloudy.

133
00:14:13,760 --> 00:14:18,760
The commentary says this means enveloped by the five hindrance.

134
00:14:18,760 --> 00:14:24,760
So the five of the Buddha gives teaching cells for about the five hindrance is

135
00:14:24,760 --> 00:14:37,760
how much under desire is like dye or paint. If you pour paint in the water, you can't see a thing.

136
00:14:37,760 --> 00:14:41,760
Anger is like boiling water.

137
00:14:41,760 --> 00:14:46,760
So water is boiling, you also can't see anything.

138
00:14:46,760 --> 00:14:54,760
The tea in the mid days.

139
00:14:54,760 --> 00:14:56,760
Money water maybe does.

140
00:14:56,760 --> 00:14:58,760
No, money water is about the fifth one.

141
00:14:58,760 --> 00:15:01,760
Remember what the third one is?

142
00:15:01,760 --> 00:15:06,760
The various ways in which the water can be cloudy.

143
00:15:06,760 --> 00:15:15,760
Same in the various ways in which the mind can be cloudy.

144
00:15:15,760 --> 00:15:29,760
So this is why a simple examination of the mind doesn't work.

145
00:15:29,760 --> 00:15:34,760
Your mind has to be clear in order for you to see anything.

146
00:15:34,760 --> 00:15:40,760
In fact, that's all you have to do is clear the mind, the seeing occurs naturally.

147
00:15:40,760 --> 00:15:46,760
You're going to go about filtering water, you're looking at the water, you're going to see everything that's in it.

148
00:15:46,760 --> 00:15:51,760
You go about filtering the mind and cleansing the mind.

149
00:15:51,760 --> 00:15:55,760
You're also going to see everything that's in it as you clean it.

150
00:15:55,760 --> 00:15:59,760
So that's what happens. You'll practice, you'll start to see so many things.

151
00:15:59,760 --> 00:16:04,760
You don't have to worry about wisdom is it going to come and am I learning anything?

152
00:16:04,760 --> 00:16:11,760
It's a very bad attitude because as you start to worry, you stop practicing and so of course you stop learning.

153
00:16:11,760 --> 00:16:14,760
The way is to focus on the practice.

154
00:16:14,760 --> 00:16:16,760
Focus on practicing properly.

155
00:16:16,760 --> 00:16:20,760
Don't worry about results.

156
00:16:20,760 --> 00:16:25,760
They come by themselves.

157
00:16:25,760 --> 00:16:35,760
That's five and six clear and muddy water. The mind is clear, you can see into it.

158
00:16:35,760 --> 00:16:41,760
47 is a single one.

159
00:16:41,760 --> 00:16:46,760
It's about sandalwood. I've never seen sandalwood in the raw.

160
00:16:46,760 --> 00:16:49,760
I've only seen it carved into different things.

161
00:16:49,760 --> 00:16:55,760
It has a smell to it. The wood is actually quite pleasant to smell.

162
00:16:55,760 --> 00:16:59,760
It's very expensive sandalwood.

163
00:16:59,760 --> 00:17:03,760
The wood is also malleable and wheeled.

164
00:17:03,760 --> 00:17:10,760
It's easy to carve, I guess.

165
00:17:10,760 --> 00:17:20,760
So it's just like sandalwood, the mind that is well developed.

166
00:17:20,760 --> 00:17:23,760
Number eight, nine and ten are quite interesting.

167
00:17:23,760 --> 00:17:26,760
So we're going to go into the next one as well.

168
00:17:26,760 --> 00:17:30,760
Eight, nine, ten and then the first two of the next chapter.

169
00:17:30,760 --> 00:17:37,760
48 is because I do not see even one other thing that changes so quickly as the mind.

170
00:17:37,760 --> 00:17:42,760
It's not easy to give a simile for how quickly the mind changes.

171
00:17:42,760 --> 00:17:44,760
The mind is quick to change.

172
00:17:44,760 --> 00:17:46,760
This is the important point.

173
00:17:46,760 --> 00:17:49,760
Namika Bodhi gives some distinction here.

174
00:17:49,760 --> 00:17:55,760
He quotes the commentary as saying it's about the rising and ceasing of the mind.

175
00:17:55,760 --> 00:17:58,760
But he says, well, he also points to the wind.

176
00:17:58,760 --> 00:18:01,760
He has meaning something different in terms of the change.

177
00:18:01,760 --> 00:18:03,760
How quickly the mind is to change.

178
00:18:03,760 --> 00:18:05,760
Your mind will be okay right now.

179
00:18:05,760 --> 00:18:08,760
If you're not careful, it might completely change.

180
00:18:08,760 --> 00:18:10,760
But actually they're the same thing.

181
00:18:10,760 --> 00:18:12,760
The commentary is making the same point.

182
00:18:12,760 --> 00:18:17,760
A rising and ceasing is important because you can say, well, now I'm calm.

183
00:18:17,760 --> 00:18:20,760
But that mind only is a moment.

184
00:18:20,760 --> 00:18:22,760
It doesn't continue.

185
00:18:22,760 --> 00:18:26,760
And so the next very next moment might be a completely different mind.

186
00:18:26,760 --> 00:18:29,760
It's actually saying the same thing.

187
00:18:29,760 --> 00:18:33,760
It's an important aspect of reality for us to understand.

188
00:18:33,760 --> 00:18:36,760
And we're not constant, we're not continuous.

189
00:18:36,760 --> 00:18:41,760
Each moment is a new birth.

190
00:18:41,760 --> 00:18:47,760
You can't rely on any of one state because it's impermanent.

191
00:18:47,760 --> 00:18:53,760
Whatever power it has will have its effect.

192
00:18:53,760 --> 00:18:57,760
But once it's gone, it's gone.

193
00:18:57,760 --> 00:19:03,760
Mind is quick. You can't depend on your mind.

194
00:19:03,760 --> 00:19:09,760
If your mind is not well trained quickly, how quickly it can change.

195
00:19:09,760 --> 00:19:12,760
It usually takes you up.

196
00:19:12,760 --> 00:19:17,760
Once you're off your guard, this really hits home.

197
00:19:17,760 --> 00:19:19,760
Because it happens with meditators.

198
00:19:19,760 --> 00:19:26,760
They think they're so enlightened that they'll be they'll lose their guard.

199
00:19:26,760 --> 00:19:28,760
They'll let their guard down.

200
00:19:28,760 --> 00:19:35,760
Then something comes, anything in spam, they react.

201
00:19:35,760 --> 00:19:41,760
Is there they become overconfident?

202
00:19:41,760 --> 00:19:48,760
You have to be, this is why guarding the mind is an important quality of a Buddhist practitioner

203
00:19:48,760 --> 00:19:53,760
in guarding the mind throughout our lives, throughout our daily life.

204
00:19:53,760 --> 00:19:56,760
That's 48, 49, and 50 are familiar.

205
00:19:56,760 --> 00:19:59,760
It should be familiar.

206
00:19:59,760 --> 00:20:07,760
This is a fairly well known quote.

207
00:20:07,760 --> 00:20:09,760
We've actually been through it.

208
00:20:09,760 --> 00:20:12,760
It was in our quotes the past year.

209
00:20:12,760 --> 00:20:17,760
The mind is radiant.

210
00:20:17,760 --> 00:20:21,760
The mind, the true mind, the mind is radiant.

211
00:20:21,760 --> 00:20:30,760
The defilements that come are like guests, visitors, interlopers.

212
00:20:30,760 --> 00:20:40,760
This quote has been very badly misinterpreted by many Buddhists, teravada Buddhists as well.

213
00:20:40,760 --> 00:20:44,760
For my point of view, I'm from the Orthodox point of view.

214
00:20:44,760 --> 00:20:47,760
It's been quite criticized these ideas.

215
00:20:47,760 --> 00:21:00,760
What this means is that there's a mind that is a mind that is permanent, stable, happy, satisfied.

216
00:21:00,760 --> 00:21:06,760
They extrapolate this to mean that the mind and also this idea that there's a light involved.

217
00:21:06,760 --> 00:21:08,760
Because Babasir actually means it's light.

218
00:21:08,760 --> 00:21:15,760
They focus on this light and say, that's the pure mind, the true mind.

219
00:21:15,760 --> 00:21:20,760
Which is a total misunderstanding of what Baba does saying.

220
00:21:20,760 --> 00:21:24,760
He's just saying that the mind itself is not the blame.

221
00:21:24,760 --> 00:21:28,760
The mind is just mind.

222
00:21:28,760 --> 00:21:33,760
If you think something, if you are aware of something, the awareness isn't the problem.

223
00:21:33,760 --> 00:21:35,760
It's only the foundance.

224
00:21:35,760 --> 00:21:41,760
If you can get rid of them, everything clears up.

225
00:21:41,760 --> 00:21:46,760
Everything, the letting go, happens by itself.

226
00:21:46,760 --> 00:21:51,760
Everything becomes peaceful.

227
00:21:51,760 --> 00:21:53,760
It's just a figure of speech.

228
00:21:53,760 --> 00:21:57,760
Doesn't mean that there's actually light in the mind.

229
00:21:57,760 --> 00:21:59,760
51 and 52 are actually the same.

230
00:21:59,760 --> 00:22:06,760
They're not going to go over the details of them, but...

231
00:22:06,760 --> 00:22:08,760
Yeah, let me go stop there.

232
00:22:08,760 --> 00:22:12,760
So it's an interesting tidbits about the mind, I think.

233
00:22:12,760 --> 00:22:21,760
Let's move on to some questions.

234
00:22:21,760 --> 00:22:26,760
Looks like we have some questions that I missed.

235
00:22:26,760 --> 00:22:30,760
Is the address that Amazon wishlist has been changed to the new one?

236
00:22:30,760 --> 00:22:33,760
Yes, it has.

237
00:22:33,760 --> 00:22:38,760
A mind it has anyway. If it's my wishlist, then yes.

238
00:22:38,760 --> 00:22:40,760
I think there might be another wishlist.

239
00:22:40,760 --> 00:22:44,760
If there's another wishlist, you might have to wait and ask Robert.

240
00:22:44,760 --> 00:22:50,760
Would you say that meditation is like cleaning your room to avoid your mom's complaints?

241
00:22:50,760 --> 00:22:54,760
No, I wouldn't.

242
00:22:54,760 --> 00:23:00,760
Meditating is like cleaning your home so that you have a beautiful, clean place.

243
00:23:00,760 --> 00:23:04,760
What can you do with your mom?

244
00:23:12,760 --> 00:23:16,760
Whenever mindfulness becomes easier for me or whenever things in general

245
00:23:16,760 --> 00:23:19,760
are doing well, I tend to become careless.

246
00:23:19,760 --> 00:23:21,760
I was just talking about this.

247
00:23:21,760 --> 00:23:23,760
How can I make sure that I'm always on track?

248
00:23:23,760 --> 00:23:26,760
And I don't get caught up in temporary relaxation.

249
00:23:26,760 --> 00:23:29,760
Well, I don't know if you're practicing our technique,

250
00:23:29,760 --> 00:23:33,760
but I would recommend you start by reading my booklet and practicing our technique.

251
00:23:33,760 --> 00:23:39,760
If you've already done that, then you will understand how our technique works

252
00:23:39,760 --> 00:23:42,760
and it's something you can apply in daily life.

253
00:23:42,760 --> 00:23:49,760
The more caught and the more frequently you can apply it,

254
00:23:49,760 --> 00:23:53,760
the more vigilant you will be.

255
00:23:53,760 --> 00:23:55,760
It's a choice you have to make.

256
00:23:55,760 --> 00:23:59,760
It's a commitment you have to make to spiritual development.

257
00:23:59,760 --> 00:24:05,760
Because yeah, the only alternative is negligence.

258
00:24:05,760 --> 00:24:11,760
I'm getting caught off guard.

259
00:24:11,760 --> 00:24:14,760
I'm highly analytical and have strong tendencies towards thinking,

260
00:24:14,760 --> 00:24:17,760
I, I, I, I, I, I, I is the problem.

261
00:24:17,760 --> 00:24:20,760
However, when I'm thinking too much, it tends to lead to unease.

262
00:24:20,760 --> 00:24:22,760
What is a skillful way of handling?

263
00:24:22,760 --> 00:24:25,760
Get rid of the eye first.

264
00:24:25,760 --> 00:24:27,760
The idea that I am this, I am that.

265
00:24:27,760 --> 00:24:33,760
Because then you'll identify with these things and kind of get attached to them.

266
00:24:33,760 --> 00:24:38,760
If you, you know, the meditator and better way to explain that as a meditator is

267
00:24:38,760 --> 00:24:42,760
when I meditate, thoughts come frequently.

268
00:24:42,760 --> 00:24:44,760
Because then you're talking about reality.

269
00:24:44,760 --> 00:24:47,760
Reality is there are thoughts and they come frequently.

270
00:24:47,760 --> 00:24:51,760
Sometimes they're chaotic and sometimes they're

271
00:24:51,760 --> 00:24:56,760
deep and, you know,

272
00:24:56,760 --> 00:25:00,760
convoluted or involved.

273
00:25:00,760 --> 00:25:02,760
But they're still just thoughts.

274
00:25:02,760 --> 00:25:09,760
So if you learn to let go of the idea of eye being analytical,

275
00:25:09,760 --> 00:25:11,760
there's no eye was analytical.

276
00:25:11,760 --> 00:25:14,760
It just brings lots of thoughts because that's a habit.

277
00:25:14,760 --> 00:25:18,760
If that habit is causing suffering, which it most likely is.

278
00:25:18,760 --> 00:25:20,760
Better off getting rid of it.

279
00:25:20,760 --> 00:25:21,760
You're bombing your mind.

280
00:25:21,760 --> 00:25:26,760
I'm not calming your mind, but letting your mind become more peaceful.

281
00:25:26,760 --> 00:25:34,760
Stop obsessing over thoughts.

282
00:25:34,760 --> 00:25:36,760
Well, it's contemplating the body.

283
00:25:36,760 --> 00:25:39,760
I found great humor in the fact that we idolize people's beauty in regards to

284
00:25:39,760 --> 00:25:44,760
facial feature and structure, even though it is part of the head,

285
00:25:44,760 --> 00:25:48,760
which has seven of the nine oozing holes.

286
00:25:48,760 --> 00:25:50,760
Is humor in this sense a normal part of the path?

287
00:25:50,760 --> 00:25:53,760
Could you come in on the real humor plays along the path?

288
00:25:53,760 --> 00:25:58,760
Having humor is, of course, something you have to be careful about.

289
00:25:58,760 --> 00:26:00,760
It's fairly harmless.

290
00:26:00,760 --> 00:26:02,760
Just don't get caught up in it.

291
00:26:02,760 --> 00:26:05,760
Thinking that it's a mal-part of the path.

292
00:26:05,760 --> 00:26:06,760
It's really not.

293
00:26:06,760 --> 00:26:10,760
I mean, you could argue that it's a result of letting go.

294
00:26:10,760 --> 00:26:14,760
Because you see humor, as far as I can see,

295
00:26:14,760 --> 00:26:19,760
it's evolved as a means of discriminating between

296
00:26:19,760 --> 00:26:22,760
that which we should take seriously and develop,

297
00:26:22,760 --> 00:26:24,760
and that which we should discard.

298
00:26:24,760 --> 00:26:26,760
When you find something humorous,

299
00:26:26,760 --> 00:26:29,760
it makes it clear to your mind that you shouldn't pursue it.

300
00:26:29,760 --> 00:26:32,760
It shouldn't take it seriously.

301
00:26:32,760 --> 00:26:36,760
If someone talks about something absurd that happens and you laugh at it,

302
00:26:36,760 --> 00:26:38,760
the laughing is to make it clear.

303
00:26:38,760 --> 00:26:42,760
It makes it clear in your mind that that is something absurd.

304
00:26:42,760 --> 00:26:48,760
That is not something that should be cultivated as a rational individual.

305
00:26:48,760 --> 00:26:53,760
So for that to arise,

306
00:26:53,760 --> 00:26:58,760
I think based on seeing something that you took seriously

307
00:26:58,760 --> 00:27:01,760
as being somewhat ridiculous.

308
00:27:01,760 --> 00:27:04,760
I think it's quite common and reasonable.

309
00:27:04,760 --> 00:27:08,760
And humor is a big part of a meditator's life, I think.

310
00:27:08,760 --> 00:27:13,760
I mean, it does arise.

311
00:27:13,760 --> 00:27:14,760
I mean, it does arise.

312
00:27:14,760 --> 00:27:17,760
You realize how silly you've been because it's,

313
00:27:17,760 --> 00:27:20,760
I guess, humorous is the word.

314
00:27:20,760 --> 00:27:24,760
It's something that makes you smile.

315
00:27:24,760 --> 00:27:29,760
When you see how ridiculous it is.

316
00:27:29,760 --> 00:27:32,760
So seeing things that you took seriously as ridiculous,

317
00:27:32,760 --> 00:27:34,760
I would say is a side of program.

318
00:27:34,760 --> 00:27:36,760
Whereas before you thought it was important,

319
00:27:36,760 --> 00:27:42,760
I would say a side of program.

320
00:27:42,760 --> 00:27:45,760
Can I call myself Buddhist if I relate to the principles

321
00:27:45,760 --> 00:27:47,760
and fundamentals of the Buddhist teachings,

322
00:27:47,760 --> 00:27:49,760
but could never give up things like music

323
00:27:49,760 --> 00:27:52,760
and other forms of entertainment, greens from the Netherlands.

324
00:27:52,760 --> 00:27:54,760
See, I'm a Buddhist monk,

325
00:27:54,760 --> 00:27:56,760
and we have lots and lots of rules,

326
00:27:56,760 --> 00:28:01,760
but Buddhists listen to music and watch entertainment.

327
00:28:01,760 --> 00:28:04,760
I mean, this is a part of normal life.

328
00:28:04,760 --> 00:28:07,760
These aren't anti-Buddhist.

329
00:28:07,760 --> 00:28:10,760
I mean, yes, it's true that the Buddhists end there,

330
00:28:10,760 --> 00:28:12,760
cause for hindrance,

331
00:28:12,760 --> 00:28:15,760
and they will slow you down in your practice.

332
00:28:15,760 --> 00:28:18,760
But the only thing that really is anti-Buddhist

333
00:28:18,760 --> 00:28:22,760
or will prevent you from being considered Buddhist

334
00:28:22,760 --> 00:28:24,760
is breaking the five percent.

335
00:28:24,760 --> 00:28:28,760
If you kill, steal, cheat, lie, or take drugs and alcohol,

336
00:28:28,760 --> 00:28:31,760
you can't really honestly be considered a Buddhist

337
00:28:31,760 --> 00:28:36,760
and aren't in early Buddhist tradition.

338
00:28:36,760 --> 00:28:41,760
But if you listen to music and engage in harmless entertainment,

339
00:28:41,760 --> 00:28:43,760
it's not against Buddhists.

340
00:28:43,760 --> 00:28:45,760
It doesn't make you not Buddhist.

341
00:28:45,760 --> 00:28:49,760
So it makes you not a very good meditator, perhaps.

342
00:28:49,760 --> 00:28:53,760
In terms of doing an intensive meditation practice,

343
00:28:53,760 --> 00:28:56,760
but you can still meditate and do all that.

344
00:28:56,760 --> 00:29:01,760
We'll just progress slower.

345
00:29:09,760 --> 00:29:10,760
Sign it.

346
00:29:10,760 --> 00:29:13,760
So now the question is tonight.

347
00:29:13,760 --> 00:29:16,760
Robin drove home.

348
00:29:16,760 --> 00:29:18,760
There's a horse.

349
00:29:18,760 --> 00:29:23,760
It's horrific to listen to her talk about her driving experiences.

350
00:29:23,760 --> 00:29:28,760
She got lost on the way from here back to the monastery in Stony Creek,

351
00:29:28,760 --> 00:29:30,760
and then on the way back here.

352
00:29:30,760 --> 00:29:34,760
And so she ended up leaving around 3 p.m. yesterday.

353
00:29:34,760 --> 00:29:37,760
And she drove all night because she couldn't find a place to stay

354
00:29:37,760 --> 00:29:39,760
so she didn't sleep last night.

355
00:29:39,760 --> 00:29:42,760
And then she came to the Risudhi manga at 2 o'clock.

356
00:29:42,760 --> 00:29:45,760
I didn't know she was dead tired,

357
00:29:45,760 --> 00:29:49,760
but I just called her to come on the broadcast tonight

358
00:29:49,760 --> 00:29:53,760
and she said, she can't do it. She's going to have to go to sleep.

359
00:29:53,760 --> 00:29:56,760
She's got work in the morning tomorrow as well.

360
00:29:56,760 --> 00:29:58,760
It's quite impressive.

361
00:29:58,760 --> 00:30:02,760
Many people are just impressive like that.

362
00:30:02,760 --> 00:30:13,760
I'm not sure if Jitter refers to the long edge.

363
00:30:13,760 --> 00:30:19,760
I mean, that's insane.

364
00:30:19,760 --> 00:30:22,760
I disagree.

365
00:30:22,760 --> 00:30:24,760
Long edge there just rises.

366
00:30:24,760 --> 00:30:29,760
It's nothing.

367
00:30:29,760 --> 00:30:32,760
This means the mind doesn't seem to blame.

368
00:30:32,760 --> 00:30:35,760
Awareness, meaning isn't to blame, meaning isn't the problem.

369
00:30:35,760 --> 00:30:38,760
I mean, you could argue that meaning isn't as Duke,

370
00:30:38,760 --> 00:30:40,760
but it's just academic.

371
00:30:40,760 --> 00:30:41,760
The problem is not William.

372
00:30:41,760 --> 00:30:42,760
That's the point.

373
00:30:42,760 --> 00:30:49,760
The problem is the song card, certainly in the song card.

374
00:30:49,760 --> 00:30:55,760
It's the point.

375
00:30:55,760 --> 00:30:59,760
Curious what type of music you played when you played music,

376
00:30:59,760 --> 00:31:01,760
when you were younger.

377
00:31:01,760 --> 00:31:04,760
Classical music, but not mostly.

378
00:31:04,760 --> 00:31:09,760
I got into classical music, but it was more into,

379
00:31:09,760 --> 00:31:12,760
maybe I'm just just in the fun, you know.

380
00:31:12,760 --> 00:31:13,760
We were kids.

381
00:31:13,760 --> 00:31:14,760
We didn't know what we were doing.

382
00:31:14,760 --> 00:31:16,760
We weren't professional.

383
00:31:16,760 --> 00:31:21,760
We did a song about a vocal art.

384
00:31:21,760 --> 00:31:28,760
A vocal art is a mythical beast that my friend invented.

385
00:31:28,760 --> 00:31:36,760
Just fun.

386
00:31:36,760 --> 00:31:40,760
He went on to play in a different band.

387
00:31:40,760 --> 00:31:42,760
It was more professional.

388
00:31:42,760 --> 00:31:46,760
My older brother was our drummer, and he's still at 40 years old,

389
00:31:46,760 --> 00:31:50,760
still a drummer in the band in Taiwan.

390
00:31:50,760 --> 00:31:52,760
He came to music.

391
00:31:52,760 --> 00:31:57,760
When it was him, he and me and our friend,

392
00:31:57,760 --> 00:32:02,760
the bassist, and then another friend,

393
00:32:02,760 --> 00:32:05,760
the Undertaker's son, Big Guy,

394
00:32:05,760 --> 00:32:08,760
who was our lead vocalist.

395
00:32:08,760 --> 00:32:14,760
None of us were all that good.

396
00:32:14,760 --> 00:32:21,760
And punk, punk music for a while.

397
00:32:21,760 --> 00:32:26,760
Yeah.

398
00:32:26,760 --> 00:32:27,760
That's the thing.

399
00:32:27,760 --> 00:32:28,760
You do these things.

400
00:32:28,760 --> 00:32:29,760
You do these things.

401
00:32:29,760 --> 00:32:30,760
You do them.

402
00:32:30,760 --> 00:32:31,760
You do them.

403
00:32:31,760 --> 00:32:32,760
You do them.

404
00:32:32,760 --> 00:32:34,760
Doesn't make you a happier person.

405
00:32:34,760 --> 00:32:37,760
Your life isn't happier because of them.

406
00:32:37,760 --> 00:32:38,760
It's a myth.

407
00:32:38,760 --> 00:32:42,760
It's a myth.

408
00:32:42,760 --> 00:32:46,760
It affects your mind.

409
00:32:46,760 --> 00:32:50,760
We're addicted.

410
00:32:50,760 --> 00:32:55,760
That's all.

411
00:32:55,760 --> 00:32:56,760
All right.

412
00:32:56,760 --> 00:32:58,760
I'm going to go and call that a night.

413
00:32:58,760 --> 00:33:01,760
And if you have more questions,

414
00:33:01,760 --> 00:33:04,760
save him for tomorrow.

415
00:33:04,760 --> 00:33:14,760
Good night, everyone.

