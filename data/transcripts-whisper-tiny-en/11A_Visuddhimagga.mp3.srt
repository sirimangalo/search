1
00:00:00,000 --> 00:00:14,480
Today, we come to 2nd Jana and then 3rd Jana and the 4th Jana, and the 3rd Jana, and the

2
00:00:14,480 --> 00:00:18,880
translation given on the sheath.

3
00:00:18,880 --> 00:00:26,880
I think, uh, easier to understand, or clearer.

4
00:00:26,880 --> 00:00:32,720
So please, look, look at these translations also.

5
00:00:32,720 --> 00:00:48,400
Now, after getting the 1st Jana, when it wasn't 1st to get the 2nd Jana, the 1st theme,

6
00:00:48,400 --> 00:00:56,840
we should do before, finds fault with the 1st Jana.

7
00:00:56,840 --> 00:01:01,680
So after getting the 1st Jana, you thought it was not good enough.

8
00:01:01,680 --> 00:01:09,120
And with the 1st Jana, there are 5 factors, 5 Jana factors.

9
00:01:09,120 --> 00:01:18,840
And this is a publication, sustained application, P, P, and super heavyness, and 1-pointed

10
00:01:18,840 --> 00:01:20,800
ness of mine.

11
00:01:20,800 --> 00:01:34,000
Now, this person, after getting the 1st Jana, sees danger and being close to the hindrances.

12
00:01:34,000 --> 00:01:39,760
So by overcoming hindrances, he was able to get the 1st Jana.

13
00:01:39,760 --> 00:01:47,400
So once he gets the 1st Jana, it is free from hindrances, but it is with initial application,

14
00:01:47,400 --> 00:01:49,360
sustained application, and so on.

15
00:01:49,360 --> 00:01:55,000
Now, he finds fault with sustained initial application and sustained application, or in

16
00:01:55,000 --> 00:02:05,880
Pali, Vitaka, and Vitaka.

17
00:02:05,880 --> 00:02:11,560
And so he tries to be dispassionate towards these 2 factors and try to eliminate them.

18
00:02:11,560 --> 00:02:18,480
So when he is able to eliminate them, and he will get the 2nd Jana.

19
00:02:18,480 --> 00:02:40,960
So after the 1st Jana, after getting the 1st Jana, he tries to get mastery in dealing with

20
00:02:40,960 --> 00:02:48,280
the 1st Jana, particularly entering into age-quickening, emerging from it and so on.

21
00:02:48,280 --> 00:02:58,680
So when it is on page 161, for a group 138, so when he has emerged from the 1st Jana,

22
00:02:58,680 --> 00:03:04,960
the plaidam sustained toward a peer grows to him, applied thought means initial application

23
00:03:04,960 --> 00:03:09,840
and sustained thought means sustained application, a peer grows to him as he reviews the

24
00:03:09,840 --> 00:03:13,160
Jana factors with mindfulness and full awareness.

25
00:03:13,160 --> 00:03:19,480
So he enters into the 1st Jana, in much else from the Jana, and reviews the factors in

26
00:03:19,480 --> 00:03:20,480
the Jana.

27
00:03:20,480 --> 00:03:22,360
There are 5 factors in the 1st Jana.

28
00:03:22,360 --> 00:03:30,320
So when he reviews the first 2 factors, he applies thought and sustained toward a peer

29
00:03:30,320 --> 00:03:39,240
grows to him, while happiness and bliss, happiness is pity and bali and bliss is sukha.

30
00:03:39,240 --> 00:03:44,600
So happiness, bliss and unification of mind is he kakada, 1 point in the self-mind, a peer

31
00:03:44,600 --> 00:03:51,320
peaceful, then as he brings that same sign to mind as again and again.

32
00:03:51,320 --> 00:04:01,880
So after reviewing the Jana factors, he dwells upon the counterpart sign, he called

33
00:04:01,880 --> 00:04:04,840
during the preliminary stages.

34
00:04:04,840 --> 00:04:12,360
So he takes that counterpart sign as the objective meditation and concentrates on it saying

35
00:04:12,360 --> 00:04:17,640
that the other game and again, with the purpose of abandoning the grows factors and obtaining

36
00:04:17,640 --> 00:04:21,560
the peaceful factors.

37
00:04:21,560 --> 00:04:30,280
Knowing now the 2nd Jana will arise now, we'll have to correct this, when 2nd Jana

38
00:04:30,280 --> 00:04:38,400
is about to arise, not knowing, he doesn't know when it will arise.

39
00:04:38,400 --> 00:04:42,120
So this is a bali idiom, I've told you about this before.

40
00:04:42,120 --> 00:04:48,120
So when the 2nd Jana is about to arise, there arises in him mind or adverting with that

41
00:04:48,120 --> 00:04:53,480
same advocacy level object, interrupting the life continuum.

42
00:04:53,480 --> 00:05:03,360
If you look at the 1st Jana chart, 1st Jana sheet, at the bottom, it is given the thought

43
00:05:03,360 --> 00:05:04,360
process.

44
00:05:04,360 --> 00:05:12,880
So the same thought process, so cutting of the balanga or life continuum, there arises

45
00:05:12,880 --> 00:05:23,560
M, that is mind or adverting, and then there are 4 moments of sense, fear, impossible

46
00:05:23,560 --> 00:05:24,560
Jana.

47
00:05:24,560 --> 00:05:30,200
And then the 5th is Jana.

48
00:05:30,200 --> 00:05:39,520
For a person who is, who has quick intelligence, a quick realization, then there will

49
00:05:39,520 --> 00:05:46,800
be only 3 sense, fear, Jana, and then the 4th is Jana.

50
00:05:46,800 --> 00:05:51,560
So there arises in him mind or adverting with that same advocacy that is subject, interrupting

51
00:05:51,560 --> 00:05:52,560
the life continuum.

52
00:05:52,560 --> 00:06:00,520
After that, either 4 or 5 impressions in care on that same object, depending on whether

53
00:06:00,520 --> 00:06:05,720
the person is of quick intelligence or slow intelligence.

54
00:06:05,720 --> 00:06:09,160
The last one of which is an impression of fine material sphere.

55
00:06:09,160 --> 00:06:17,240
So the last one means the 5th or the 4th, and at that belongs to fine material sphere

56
00:06:17,240 --> 00:06:20,400
that is Rubal Wajara.

57
00:06:20,400 --> 00:06:25,520
Here is the 2nd Jana.

58
00:06:25,520 --> 00:06:29,880
The rest are of the sense sphere of the kind already stated.

59
00:06:29,880 --> 00:06:37,200
So the rest means that the person who wants full or three.

60
00:06:37,200 --> 00:06:41,920
And now comes the description of the 2nd Jana, the translation of the description of

61
00:06:41,920 --> 00:06:42,920
2nd Jana.

62
00:06:42,920 --> 00:06:49,080
So I have made a new translation.

63
00:06:49,080 --> 00:06:55,360
It cannot be used for all people, but I think for the class it is better.

64
00:06:55,360 --> 00:07:02,680
So with the stealing, that means overcoming of Whitaka and Wijara.

65
00:07:02,680 --> 00:07:13,200
The end is upon and dwells in the 2nd Jana, which arises internally which has of which

66
00:07:13,200 --> 00:07:21,560
is confidence and arouses singleness of mind or which clarifies the mind and arouses

67
00:07:21,560 --> 00:07:29,000
singleness, which is without Midagana, Whitaka, which is born of concentration and with

68
00:07:29,000 --> 00:07:33,880
PD and Sukhara, which is with PD and Sukhara born of concentration.

69
00:07:33,880 --> 00:07:37,080
So this is the description of the 2nd Jana.

70
00:07:37,080 --> 00:07:46,080
Whenever there is the description of 2nd Jana, the same sentence, it instruct sentences

71
00:07:46,080 --> 00:07:52,400
are repeated many times in the suit as an embita.

72
00:07:52,400 --> 00:08:01,480
The first is with the stealing or it means overcoming of Whitaka and Wijara.

73
00:08:01,480 --> 00:08:09,360
So when it wasn't once you get to the 2nd Jana to attain 2nd Jana, then he has to overcome

74
00:08:09,360 --> 00:08:12,400
or to abandon Whitaka and Wijara.

75
00:08:12,400 --> 00:08:19,880
So with the stealing overcoming of Whitaka and Wijara really means because of the stealing

76
00:08:19,880 --> 00:08:23,000
or overcoming of Whitaka and Wijara.

77
00:08:23,000 --> 00:08:29,000
He enters the born and dwells in the 2nd Jana, which arises internally.

78
00:08:29,000 --> 00:08:35,280
Jana is once, arises in one's mind and so it does not arise in other persons or outside

79
00:08:35,280 --> 00:08:36,280
teams.

80
00:08:36,280 --> 00:08:45,960
So it is internal and which is confidence and which arouses singleness of mind.

81
00:08:45,960 --> 00:09:08,320
Now, if you read the thought of unification, say you find this, this is page 162 and

82
00:09:08,320 --> 00:09:19,280
paragraph 142, now in far the word used is sampasadana, which means just confidence or

83
00:09:19,280 --> 00:09:20,280
sata.

84
00:09:20,280 --> 00:09:21,280
You remember sata?

85
00:09:21,280 --> 00:09:26,960
They are among the 52th JTC, its confidence of faith.

86
00:09:26,960 --> 00:09:37,360
So here the Jana itself is called confidence of faith, it is figuratively speaking.

87
00:09:37,360 --> 00:09:46,120
But the meaning is which is accompanied by confidence or which is accompanied by faith.

88
00:09:46,120 --> 00:09:53,640
So for this class, we would say which is confidence, but for Jana Baba's and we should

89
00:09:53,640 --> 00:10:02,120
say which has confidence, that is which arises together with what is called sada, confidence

90
00:10:02,120 --> 00:10:11,000
of faith and the function of confidence of faith is to clear the mind, to clear the mind of

91
00:10:11,000 --> 00:10:20,320
doubting others and which arises singleness of mind, singleness of mind really means concentration.

92
00:10:20,320 --> 00:10:28,200
And there is another explanation and that is which clarifies the mind and arises

93
00:10:28,200 --> 00:10:32,720
singleness.

94
00:10:32,720 --> 00:10:41,880
Now, the Pali, the Pali words are given in the footnote and if you look at it, footnote

95
00:10:41,880 --> 00:10:54,560
number 41, in the Pali, sampasadana, jita sō, ikori bawan, jita sō means of mind and that comes

96
00:10:54,560 --> 00:10:58,880
between two words sampasadana and ikori bawan.

97
00:10:58,880 --> 00:11:06,400
So the word jita sō can be connected with sampasadana or ikori bawan.

98
00:11:06,400 --> 00:11:13,480
So the commentary or the path of purification is explaining this.

99
00:11:13,480 --> 00:11:24,520
When it is connected with the first one, then that means clarifying the mind which

100
00:11:24,520 --> 00:11:34,280
clarifies the mind and when it is connected with the other word ikori bawan, it means

101
00:11:34,280 --> 00:11:40,000
arises singleness of mind because ikori bawan means singleness and jita sō means of

102
00:11:40,000 --> 00:11:41,000
mind.

103
00:11:41,000 --> 00:11:49,320
So the word jita sō can be connected with the previous word or the following word.

104
00:11:49,320 --> 00:12:04,640
So that is what is explained in the paragraph 142, 43, yeah.

105
00:12:04,640 --> 00:12:11,120
And which is without vita ka and vijara and which is born of concentration.

106
00:12:11,120 --> 00:12:18,600
That means after getting the first jara, he practices meditation again and gets second jara.

107
00:12:18,600 --> 00:12:29,280
So the second jara is said to be caused by a concentration in the first jara or it is accompanied

108
00:12:29,280 --> 00:12:34,320
by concentration when it arises to the second jara.

109
00:12:34,320 --> 00:12:47,680
So here the concentration can be taken as that which is with first jara or with second jara.

110
00:12:47,680 --> 00:12:51,120
It can be taken or either can be taken.

111
00:12:51,120 --> 00:13:00,160
So which is born of concentration and which is accompanied by pithi and suka.

112
00:13:00,160 --> 00:13:06,480
There is another explanation that is which is with pithi and suka born of concentration.

113
00:13:06,480 --> 00:13:11,200
So pithi and suka here are conditioned by concentration and so they are said to be born

114
00:13:11,200 --> 00:13:13,120
of concentration.

115
00:13:13,120 --> 00:13:15,480
So this is the explanation of the second jara.

116
00:13:15,480 --> 00:13:21,720
So the second jara, it was in guess is again jara after removing of the abandoning vita

117
00:13:21,720 --> 00:13:23,400
ka and vijara.

118
00:13:23,400 --> 00:13:29,720
And that jara is internal and that jara clarifies the mind and allows the singleness of

119
00:13:29,720 --> 00:13:34,360
mind and that jara is without vita ka and vijara.

120
00:13:34,360 --> 00:13:40,080
And the jara is born of concentration and accompanied by pithi and suka or it is accompanied

121
00:13:40,080 --> 00:13:44,480
by vidi and suka born of concentration.

122
00:13:44,480 --> 00:13:53,840
And then there are some problems and so they are all explained here.

123
00:13:53,840 --> 00:14:03,520
Now here it is said that the second jara has confidence that means second jara is accompanied

124
00:14:03,520 --> 00:14:17,520
by the shithi sika sadha confidence but this sadha accompanies first jara also.

125
00:14:17,520 --> 00:14:28,360
So paragraph 144 does not this fit exist in the first jara too and also this concentration

126
00:14:28,360 --> 00:14:35,360
with the name of the single theme because in the first jara with first jara there are

127
00:14:35,360 --> 00:14:46,040
the arise in number of chiti siggas and among them there is sadha of it or confidence

128
00:14:46,040 --> 00:14:51,640
and also unification of mind or one point in the face of mind.

129
00:14:51,640 --> 00:15:00,440
So why do you see that the second jara is with confidence and is second jara allows

130
00:15:00,440 --> 00:15:04,760
the singleness of mind because they are in the first jara also.

131
00:15:04,760 --> 00:15:13,720
So that problem be the comment data or the path of purification.

132
00:15:13,720 --> 00:15:24,520
So here then why is only this second jara set to have confidence and synchronous of

133
00:15:24,520 --> 00:15:29,160
mind it may be the blood as follows it is because that first jara is not fully confident

134
00:15:29,160 --> 00:15:33,000
owing to the disturbance created by applied and sustained.

135
00:15:33,000 --> 00:15:41,480
When there is the vita guide vityara they really are like taking mind to the object

136
00:15:41,480 --> 00:15:49,320
and pushing the mind to this object and then object and so they tend to distract the mind.

137
00:15:49,320 --> 00:16:01,040
So when there is vita guide vityara with the jara then sadha of confidence is not good

138
00:16:01,040 --> 00:16:08,680
enough it is not fully confident it is not fully clarifying like water ruffles by ripples

139
00:16:08,680 --> 00:16:16,520
and wavelengths that is why although it does exist in it it is not called it means the

140
00:16:16,520 --> 00:16:25,400
first jara is not called having confidence and there are two concentration is not fully

141
00:16:25,400 --> 00:16:32,680
evident because of the lack of full confidence and when the sadha is not strong then concentration

142
00:16:32,680 --> 00:16:39,960
is also not strong there that is why it is not called arousing concentration or arousing

143
00:16:39,960 --> 00:16:55,800
singleness but in this second jana fade is strong having got a footing in the absence of impediments

144
00:16:55,800 --> 00:17:02,920
of applied and sustained thought but in the second jana there are no vita guide vityara to disturb

145
00:17:02,920 --> 00:17:12,360
so the fade is strong in this jana and when fade is strong concentration also becomes strong

146
00:17:12,360 --> 00:17:20,120
that is why only this jana is described as having confidence and arousing concentration

147
00:17:20,120 --> 00:17:27,320
now is that the highest or the concentration? no not yet this is just a second the second

148
00:17:27,320 --> 00:17:35,080
second stage second level of concentration so we will have third food and then not in this

149
00:17:35,080 --> 00:17:42,040
chapter but in the later chapters we will go to the formless jana also so this is only the

150
00:17:42,040 --> 00:17:55,080
second stage of concentration that may be understood as the reason why only this jana is

151
00:17:55,080 --> 00:18:07,160
just described in this way but this much is actually stated in the vipanga too with the words

152
00:18:07,160 --> 00:18:16,440
confidence is hidden so on so although confidence and concentration are present with first jana

153
00:18:17,240 --> 00:18:22,600
the first jana is not described as having confidence and concentration because they are not strong

154
00:18:22,600 --> 00:18:28,360
enough there why are they why are they not strong enough there because there is the disturbance

155
00:18:28,360 --> 00:18:33,240
of vita guide vityara but in this second jana there are no vita guide vityara to disturb

156
00:18:33,240 --> 00:18:41,800
that so they become strong and that is why this jana only this jana is described as having

157
00:18:41,800 --> 00:18:48,200
confidence and arousing singleness of mind so it says that you don't need vita can be

158
00:18:48,200 --> 00:18:58,680
chosen to create that concentration actually vita guide vityara not really hindrance but they are

159
00:18:58,680 --> 00:19:09,960
something like an not really obstacle by some something there some some kind of disturbance

160
00:19:09,960 --> 00:19:19,480
as a to the strong concentration let us say because the first jana is also strong concentration

161
00:19:19,480 --> 00:19:30,440
but compared with second jana it is not so strong yeah then after that you you eliminate

162
00:19:30,440 --> 00:19:43,160
that you get rid of that you get second jana you know first the fine concentration of mind is not strong

163
00:19:43,160 --> 00:19:53,480
enough you need vita guide to support you to support the mind to be on the object after getting

164
00:19:53,480 --> 00:20:00,680
their jana you no longer need vita guide to take you to the area to take you to the mind doesn't

165
00:20:00,680 --> 00:20:06,760
need vita guide to take it to the object so it eliminates or gets rid of vita time with jana

166
00:20:08,360 --> 00:20:14,200
now that is one problem and another problem is

167
00:20:14,200 --> 00:20:29,240
the second jana is described as without vita kind vityara now the first sentence is the first

168
00:20:29,240 --> 00:20:36,680
sentence also means that it is it is without vita kind vityara so why is there a position

169
00:20:36,680 --> 00:20:45,240
with the stealing or overcoming of vita guide vityara and then here the second jana is without

170
00:20:45,240 --> 00:20:54,120
vita guide vityara so it seems to be repetition and we commonly the explains away this

171
00:20:54,120 --> 00:21:08,840
by giving two or three explanations the first one is paragraph 146

172
00:21:08,840 --> 00:21:26,920
that means the first one the first explanation is in order to get higher janas you have to eliminate

173
00:21:26,920 --> 00:21:37,080
the grosser element I mean grosser factor so in order to to let you understand this this phrase

174
00:21:37,080 --> 00:21:43,560
is repeated here that with the stealing of vita guide vityara and then it is repeated

175
00:21:43,560 --> 00:21:51,320
it is without vita guide vityara so that is one one reason it is repeated to show that

176
00:21:53,080 --> 00:21:59,640
only by eliminating the grosser factors in the lower janas do you

177
00:21:59,640 --> 00:22:07,080
say higher janas that is one explanation now another explanation is

178
00:22:12,200 --> 00:22:25,880
the the the phrase with the stealing of vita kind vityara is the cause of it is this jana

179
00:22:25,880 --> 00:22:32,040
having confidence and arousing singleness of minds

180
00:22:34,840 --> 00:22:42,040
it shows that it does not show the mere absence of vita kind vityara the first phrase

181
00:22:42,040 --> 00:22:51,880
but it shows that because of stealing a vita guide vityara this jana comes to be called confidence

182
00:22:51,880 --> 00:22:59,080
and this jana arouses singleness of mind so to show this relationship as cause and effect

183
00:23:00,200 --> 00:23:02,040
two phrases are given here

184
00:23:03,640 --> 00:23:10,840
now is this voluntary or is this it sounds like you're saying that one voluntarily drops vita

185
00:23:10,840 --> 00:23:16,120
kind vityara in order to achieve a higher state of course but does that just naturally arise

186
00:23:16,120 --> 00:23:23,640
after being in the first year yeah when when it arise it it arise naturally but before

187
00:23:23,640 --> 00:23:32,600
before the jana arise you have to meditate so while you meditate you have the intention to get rid

188
00:23:32,600 --> 00:23:41,400
of vita kind vityara because if you cannot eliminate vita kind vityara you will not get second jana

189
00:23:41,400 --> 00:23:48,200
and you eliminate them through more concentration that's right yeah first you you

190
00:23:48,200 --> 00:24:00,200
arrive dispersion to us vita van vityara it is just by thinking and then you review after you

191
00:24:01,480 --> 00:24:08,520
emerge from the jana you review or you concentrate on the factors this factors and so when you

192
00:24:08,520 --> 00:24:14,280
concentrate on these factors since since you you have fine fault found fault with vita van vityara

193
00:24:14,280 --> 00:24:23,160
the appear to be groves and other others appear to be subtle so these two groves factors

194
00:24:24,440 --> 00:24:26,680
you eliminate when you get the second jana

195
00:24:26,680 --> 00:24:37,800
so the elimination or the overcoming of vita van vityara is the cause and the second jana being

196
00:24:39,080 --> 00:24:46,200
with confidence and arousing singleness of mind is the effect so to show this

197
00:24:46,200 --> 00:25:01,400
constant effect relationship the first phrases state given here then there's another

198
00:25:02,600 --> 00:25:14,280
reason given and that is the second phase with it is without vita kind vityara means just

199
00:25:14,280 --> 00:25:27,000
the the absence of vita kind vityara nothing more but the first phrase means not only without

200
00:25:27,000 --> 00:25:36,600
vita kind vityara but also it is without vita kind vityara because they have been overcome

201
00:25:36,600 --> 00:25:45,720
so in this case the first phrase is the cause of the second phrase because vita and vityara are

202
00:25:45,720 --> 00:25:55,400
still because they are eliminated there are no vita and vityara in the second jana so in order

203
00:25:55,400 --> 00:26:11,320
to show that these two phrases are given or stated in the description of the second jana

204
00:26:11,320 --> 00:26:24,120
and then it is born of concentration and pd and suga the same due to be understood

205
00:26:26,520 --> 00:26:29,080
similarly as in the first jana

206
00:26:29,080 --> 00:26:44,200
now born of concentration as here also there is a problem because the first jana is also born

207
00:26:44,200 --> 00:26:51,880
of concentration because if you have no concentration you cannot you cannot get jana or you cannot

208
00:26:51,880 --> 00:27:00,200
get the access concentration that is preceding jana so if you have no concentration you cannot

209
00:27:00,200 --> 00:27:08,680
get any of these jana and the stage of access but why is why is this second jana just

210
00:27:08,680 --> 00:27:18,840
cried to be born of concentration again here concentration in the first jana is not so strong

211
00:27:18,840 --> 00:27:29,000
as concentration in the second jana for the same reason that it is disturbed by vita kind vityara

212
00:27:29,720 --> 00:27:37,160
so when there is vita kind vityara concentration is not so strong because there is a disturbance

213
00:27:37,160 --> 00:27:46,440
of vita kind vityara when vita kind vityara are eliminated they do not arise then concentration

214
00:27:46,440 --> 00:27:53,560
becomes very strong and very and the mind becomes very clear and concentration becomes very strong

215
00:27:53,560 --> 00:28:06,360
so in order to to praise the second jana it is just called as born of concentration but we

216
00:28:06,360 --> 00:28:20,840
we are to take that not only the second jana is born of concentration but also the first jana

217
00:28:20,840 --> 00:28:43,000
on which 164 paragraph 148 but in middle of the paragraph still it is only this concentration

218
00:28:43,000 --> 00:28:47,800
that is quite worthy to be called concentration because of its complete confidence

219
00:28:47,800 --> 00:29:01,560
confidence I mean complete clarity clarifying and extreme immobility what what do you understand by immobility

220
00:29:01,560 --> 00:29:24,360
still yeah not moving it's true I think stillness may be better here stillness because

221
00:29:24,360 --> 00:29:34,200
but the the body what is ajala so ajala means not moving the the translator I think wanted to be

222
00:29:35,400 --> 00:29:42,120
literal so he used immobility but it may mean some other thing than the stillness of mind

223
00:29:42,120 --> 00:29:50,840
so extreme stillness still do have sense of disturbance by have light and sustained and so on

224
00:29:51,640 --> 00:30:07,560
oh up there on paragraph 147 besides this confidence come about vity act of stealing

225
00:30:07,560 --> 00:30:16,920
not the darkness of the fireman actually the word does not mean darkness no it is the word means

226
00:30:18,040 --> 00:30:26,360
you know tub it tub itness maddiness cloudiness of the mind not not darkness

227
00:30:26,360 --> 00:30:37,800
it's always a quick clear clear so tub itness of the fireman

228
00:30:37,800 --> 00:30:52,920
and in this journal how many how many factors are there in the second journal

229
00:30:52,920 --> 00:31:10,600
three factors pd suka and unification of mind a kagata three factors and it

230
00:31:10,600 --> 00:31:23,880
a bad answer two factors we take a and we jump and we go to the third journal so you find for

231
00:31:23,880 --> 00:31:34,520
with pd now one by one you're going to eliminate so once this has been obtained in this way

232
00:31:34,520 --> 00:31:41,160
paragraph 151 and he has must read and the five ways already described them on emerging from

233
00:31:41,160 --> 00:31:48,600
the now familiar second journal he can regard the flaws in it does this attainment is threatened

234
00:31:48,600 --> 00:31:54,200
by the nearness of applied thought and sustained although with the banwijara not in the second

235
00:31:54,200 --> 00:32:01,240
journal it is close to the first journal which has with the banwijara so this second journal this

236
00:32:01,240 --> 00:32:06,200
attainment mean this again journal is close to applied thought and sustained thought whatever

237
00:32:06,200 --> 00:32:13,080
there is in it of heaviness of metal excitement through claims it's grossness that means

238
00:32:17,800 --> 00:32:25,160
its factors are weak because of grossness of pd which is described does in the soda

239
00:32:25,160 --> 00:32:33,320
this second journal is declared to be gross with that very pd because it is a metal excitement

240
00:32:34,200 --> 00:32:42,440
which is the one you are when you are feeling pd and you are something like floating on the

241
00:32:42,440 --> 00:32:51,480
surface so pd pd has the the function of exciting the mind now this person wants its mind to be

242
00:32:51,480 --> 00:32:59,160
very calm and serene so he finds fault with pd now and it's like this a weekend but the grossness

243
00:32:59,160 --> 00:33:04,440
of the happiness means pd so express he can bring the third journal to minus quieter and so

244
00:33:05,000 --> 00:33:09,880
and it's a testament to the second journal and said about doing what is needed for attending the

245
00:33:09,880 --> 00:33:16,520
third when he has emerged from the third journal pd appears close to him as he reduced the

246
00:33:16,520 --> 00:33:22,440
journal factors with mindfulness and full awareness while suka and decagular unification

247
00:33:22,440 --> 00:33:27,480
may be a peaceful then as he brings that same sign to mind the other again and again with the

248
00:33:27,480 --> 00:33:33,560
purpose of abandoning the gross factor or obtaining the peaceful factors and when the third

249
00:33:33,560 --> 00:33:39,880
journal is about to arise not knowing when the third journal is about to arise there arises in

250
00:33:39,880 --> 00:33:47,000
the mind or efforting and so on we are the same so now we come to the third journal and the third

251
00:33:47,000 --> 00:34:01,320
journal is described in set phrases or set sentences and we have the translation on the sheet

252
00:34:01,320 --> 00:34:13,480
that journal so with the fading away of pd as well he does in equanimity and is mindful and fully

253
00:34:13,480 --> 00:34:27,720
aware and so on now the words as well here are said to set to have the meaning of

254
00:34:27,720 --> 00:34:38,440
conjunction that means meaning some other thing also that's why it is used as it is said here as

255
00:34:38,440 --> 00:34:48,920
well so with the fading away of pd as well so by as well we can conjoint some others so they are

256
00:34:48,920 --> 00:34:56,520
described in the brackets with the distaste for that is fading away with the fading away and stealing

257
00:34:56,520 --> 00:35:10,280
of pd so here in the first case as well conjoint the stealing so here with the fading away

258
00:35:10,280 --> 00:35:19,720
of pd as well means within fading away and stealing of pd stealing is mentioned in the second

259
00:35:19,720 --> 00:35:33,000
journal so that is something like not understood but that is something like taken over to the third

260
00:35:33,000 --> 00:35:44,360
journal or to the taken over to the passage explaining the third journal so here with the fading

261
00:35:44,360 --> 00:35:54,280
away of pd as well means with the fading away and stealing of pd or it it conjoint some other thing

262
00:35:54,280 --> 00:36:03,480
so in that case with the surrounding that is fading away of pd and stealing of with that kind with

263
00:36:03,480 --> 00:36:11,640
that so it can mean one of these two things or both of these two things so with the fading away

264
00:36:11,640 --> 00:36:20,040
of pd as well simply means with the distaste for and stealing of pd or with the surrounding of pd

265
00:36:20,040 --> 00:36:28,360
and stealing of vita kind with jal he does an equanimity and is mindful and fully aware and

266
00:36:28,360 --> 00:36:35,960
experiences suka here suka means both bodily and mental with his mental body and enters upon

267
00:36:35,960 --> 00:36:41,320
and draws in the third journal on a count of which the noble ones announced with regard to your

268
00:36:41,320 --> 00:36:46,840
person who has attained the third journal he has equanimity is mindful and dwells in suka

269
00:36:48,840 --> 00:36:50,040
now this is a third journal

270
00:36:51,720 --> 00:36:56,840
and he experiences suka both bodily and mental with his mental body

271
00:36:56,840 --> 00:37:15,000
yeah I'll explain data now with that and vita are stills or they do not arise with second journal

272
00:37:15,640 --> 00:37:24,840
with that and vita but here also with the surrounding with the stealing of vita that vita

273
00:37:24,840 --> 00:37:34,920
simply is set to to praise the third journal so in praise of third journal sometimes some

274
00:37:34,920 --> 00:37:41,160
descriptions of the other journals are repeated here and this is what the comment data is explaining

275
00:37:42,680 --> 00:37:48,680
so actually there are no vita and vita are even in second journal much less in third journal

276
00:37:48,680 --> 00:38:02,520
but in order to to show the way to get higher journals and in order to to praise the third

277
00:38:02,520 --> 00:38:13,320
journal and it is set here also because the stealing of vita and vita and vita are

278
00:38:13,320 --> 00:38:19,640
is the way or condition for gaining the third journal

279
00:38:26,040 --> 00:38:32,680
and he draws in equanimity with regard in connection with the the word equanimity

280
00:38:32,680 --> 00:38:44,760
the commented give us nine kinds of let me use a pali word nine kinds of upika and it is very important

281
00:38:45,480 --> 00:38:53,080
whenever you see the word upika in in pali is pali soldiers you must understand what is meant

282
00:38:53,080 --> 00:39:09,160
at least upika means filling or equanimity, equanimity is not filling here equanimity is

283
00:39:10,520 --> 00:39:18,600
impartiality and this is one of the changes it has and filling is also one of the changes it has

284
00:39:18,600 --> 00:39:24,600
so they are different uh filling upika is different from equanimity upika

285
00:39:25,240 --> 00:39:30,760
did this much you have to understand so whenever you find the word upika you have to see whether

286
00:39:30,760 --> 00:39:40,840
it it meant uh filling upika or equanimity upika and here nine kinds of upika mentioned

287
00:39:40,840 --> 00:39:46,040
and yeah

288
00:39:47,800 --> 00:39:50,440
uh please upika

289
00:39:52,280 --> 00:39:54,680
as you said we always pronounce rupaka

290
00:39:57,720 --> 00:40:07,240
upika now upika comes on upika and ika so there that is explained in paragraph 156

291
00:40:07,240 --> 00:40:17,640
it watches things as they arise upika but it is equanimity does it is equanimity or upika that is

292
00:40:17,640 --> 00:40:26,120
on hooking it sees fairies sees without partiality upika but it is the main

293
00:40:26,120 --> 00:40:39,000
so there are nine kinds of I mean ten kinds of upika the first is called six-factor equanimity

294
00:40:39,000 --> 00:40:47,960
it is the equanimity found in the minds of other hands because here it because who's can

295
00:40:47,960 --> 00:41:02,920
cause a just fry that means an air hand is not a glad no said on seeing a visible object with the tie he

296
00:41:02,920 --> 00:41:13,720
is a good object of that object does not feel glad or said with regard to that object he is

297
00:41:13,720 --> 00:41:24,520
impartial to these objects so he can keep that impartiality in the face of objects and desirable

298
00:41:24,520 --> 00:41:32,440
or undesirable and that is one quality of the air hands because air hands have eradicated

299
00:41:32,440 --> 00:41:39,240
all mental defilements and so they are not they are not glad with attachment to things

300
00:41:39,240 --> 00:41:47,960
are they are not refast by things which are undesirable and the next one is equanimity as it

301
00:41:47,960 --> 00:41:56,520
divine abiding that is one of the four divine abiding he does intend upon one quarter with his heart

302
00:41:56,520 --> 00:42:09,400
and huge with equanimity that also is not hating and not loving so looking on with with impartiality

303
00:42:09,400 --> 00:42:16,920
the next one is equanimity as an enlightenment factor there are seven factors of enlightenment

304
00:42:16,920 --> 00:42:23,320
and equanimity is one of them he developed the equanimity enlightenment factor depending on

305
00:42:23,320 --> 00:42:34,360
relevant question so it is neutrality in connection states described as actually neutrality among

306
00:42:34,360 --> 00:42:41,560
co-niece who needs and states neutrality is also a co-niece and state so among the co-niece

307
00:42:41,560 --> 00:42:47,320
and states arising within with the consciousness this is neutrality is a mode of neutrality

308
00:42:47,320 --> 00:42:57,480
and this is one of the the factors of enlightenment and the next one is equanimity of energy

309
00:42:57,480 --> 00:43:04,760
actually energy is called equanimity here because energy should not be too much or too little

310
00:43:04,760 --> 00:43:11,640
so it must be in the middle so it is called equanimity here actually it is energy or

311
00:43:11,640 --> 00:43:21,480
video so neither of us strenuous nor overlex energy the next one is equanimity about formations

312
00:43:22,120 --> 00:43:28,760
and that that arises during vipassana meditation when a person practice a ripassana magic

313
00:43:28,760 --> 00:43:37,080
meditation and reaches behind higher stages of vipassana knowledge then he gains this

314
00:43:37,080 --> 00:43:46,920
equanimity about formations so neutrality about apprehending reflection and

315
00:43:46,920 --> 00:43:55,240
confusion regarding the hindrances etc. described as that means he doesn't have to make effort

316
00:43:55,240 --> 00:44:08,440
to keep his mind on the object so effort less observation at that moment and he is not attached

317
00:44:08,440 --> 00:44:17,880
to no ripass by what he sees and then how many kinds of equanimity about formations

318
00:44:17,880 --> 00:44:22,600
arise to concentration how many kinds of equanimity about formations arise to inside

319
00:44:22,600 --> 00:44:27,800
eight kinds of equanimity about formations arise through concentration

320
00:44:27,800 --> 00:44:33,640
in vipassana meditation these are the eight genres 10 kinds of equanimity about formations

321
00:44:33,640 --> 00:44:39,080
arise through inside so when you practice vipassana then you go through these 10 kinds of equanimity

322
00:44:39,080 --> 00:44:46,360
and those 10 are given down and in footnotes the eight are also given the eight kinds of those

323
00:44:46,360 --> 00:44:52,840
connected with the eight genre attainments the 10 kinds of those connected with the four paths the

324
00:44:52,840 --> 00:44:59,880
four frusions and the void liberation and sinless liberation actually the word liberation is not

325
00:44:59,880 --> 00:45:10,360
not used in the in the in the commentary it is not void liberation but dwelling in liberation

326
00:45:10,360 --> 00:45:18,680
dwelling in sinlessness dwelling in liberation means I mean dwelling in

327
00:45:19,960 --> 00:45:29,720
voidness means contemplating on the voidness of formations it is he is still in the stage of vipassana

328
00:45:29,720 --> 00:45:44,920
not not at the stage of enlightenment or path and frusions so here dwelling in whiteness means

329
00:45:45,640 --> 00:45:52,760
dwelling on whiteness and dwelling in sinlessness means dwelling on sinlessness of

330
00:45:52,760 --> 00:46:00,760
formations so he when we practice a vipassana meditation he takes the formations as object

331
00:46:00,760 --> 00:46:09,960
and these formations he he contemplates on as void it is that means void of permanency

332
00:46:10,760 --> 00:46:20,280
void of satisfactoryness and void of soul or self and sinlessness means the same thing

333
00:46:20,280 --> 00:46:27,800
so these are the 10 so when it was in reaches that stage he gains his equanimity about formation

334
00:46:27,800 --> 00:46:57,640
and then next is equanimity

