Bante, you mentioned the old monk will be exiting because he falls ill, where will the old monks go when they become ill, which is the usual monastic practice.
The usual monastic practice is to incinerate them.
Burn the bodies, do a funeral ceremony.
Exit means die, he is very old and will take care of him until he dies.
What I meant by exiting was just this euphemistic way of saying he is dying.
Of course we are all dying.
As long as people are still alive we take care of them.
But when they die we call it quits.
Burn them.
And do away with the ashes and say our last goodbyes.
