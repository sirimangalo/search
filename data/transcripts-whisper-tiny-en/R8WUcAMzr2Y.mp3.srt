1
00:00:00,000 --> 00:00:04,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:04,000 --> 00:00:14,000
Today we continue on with verses 2.44 and 2.45, which read us follows.

3
00:00:34,000 --> 00:01:03,000
This is the life of one who is shameless, brazen like a crow.

4
00:01:03,000 --> 00:01:16,000
An impudent, bold, reckless, living a corrupted life.

5
00:01:16,000 --> 00:01:27,000
And hard is the life of the conscientious ever pure in their search.

6
00:01:27,000 --> 00:01:46,000
Vigilant, dedicated, not boastful or bold or impudent or brazen, living a pure life with vision.

7
00:01:46,000 --> 00:01:54,000
So this set of verses was taught in regards to a very short story.

8
00:01:54,000 --> 00:02:03,000
The occasion of a certain monk who traded medical services for food.

9
00:02:03,000 --> 00:02:09,000
And so he had been a doctor perhaps before he became a monk and once he became a monk,

10
00:02:09,000 --> 00:02:25,000
he realized that by continuing to offer medical services he could amass or he could acquire luxurious requisites.

11
00:02:25,000 --> 00:02:38,000
So he would spend his time tending to rich lay people and in return getting the choices to foods and roams and medicines and so on.

12
00:02:38,000 --> 00:02:49,000
And one day when he was coming back from getting food, he saw Sariputta, the Buddha's chief disciple accompanied by a group of monks, I guess.

13
00:02:49,000 --> 00:02:55,000
And he saw Sariputta and he said, venerable sir, please, I've received all this food. Come partake in it.

14
00:02:55,000 --> 00:03:05,000
And he explained to me that I offer my medical services to the people and such a great idea because they give me all this good food.

15
00:03:05,000 --> 00:03:14,000
And Sariputta looked at him and listened to what he had to say and turned around and walked away.

16
00:03:14,000 --> 00:03:21,000
Now the monks hearing this went to see the Buddha and told this to the Buddha.

17
00:03:21,000 --> 00:03:28,000
And the Buddha simply said, indeed, that monk is living an easy life.

18
00:03:28,000 --> 00:03:36,000
He is corrupt ways, providing him with a pure life and it's hard to be someone who is pure.

19
00:03:36,000 --> 00:03:39,000
Their life will be hard.

20
00:03:39,000 --> 00:03:43,000
And then he taught these verses.

21
00:03:43,000 --> 00:03:53,000
It's a deceptively profound teaching and I think hard to understand if you investigate it.

22
00:03:53,000 --> 00:04:07,000
It's a hard one to appreciate and to really get behind because it appears to be saying that what we're doing, all of what we're doing is the wrong path.

23
00:04:07,000 --> 00:04:12,000
We are purposefully making life harder for ourselves. That's what it sounds like.

24
00:04:12,000 --> 00:04:23,000
Because the options are being corrupt or at the very least seeking out pleasure, seeking out the sources of pleasure.

25
00:04:23,000 --> 00:04:28,000
That's the right way. It leads to well pleasure.

26
00:04:28,000 --> 00:04:41,000
It seems to go very much against the law of karma, which you understand to mean that being a pure of a pure mind or having a pure character,

27
00:04:41,000 --> 00:04:45,000
living an ethical life would lead to good things.

28
00:04:45,000 --> 00:04:53,000
Why does the Buddha appear to be saying the opposite? That it's corruption that leads to good things, an easy life.

29
00:04:53,000 --> 00:05:09,000
Well, we know this to be true if you look at the world, this is the case, it's a very simple fact that all other things being equal.

30
00:05:09,000 --> 00:05:18,000
Chasing after the pleasure that is available is going to lead to more pleasure than not chasing after the pleasure.

31
00:05:18,000 --> 00:05:22,000
Most obviously there's no question here.

32
00:05:22,000 --> 00:05:34,000
When you have a set of potentials in front of you, seizing the potential, sees the day, drink and be merry, do it because the opportunity is there.

33
00:05:34,000 --> 00:05:45,000
Taking that opportunity makes life more pleasurable, generally makes life easier.

34
00:05:45,000 --> 00:05:53,000
We have to appreciate this. We have to understand this because it's the essence of how we think of happiness.

35
00:05:53,000 --> 00:06:05,000
We see this, this is an obvious truth, and so our thinking of how to attain happiness is always along these lines.

36
00:06:05,000 --> 00:06:14,000
It's the way in conventional world, and it's the way even in meditation. This bleeds over into our meditation practice,

37
00:06:14,000 --> 00:06:24,000
where it's often quite discouraging when your practice on a Buddhist path is unpleasant, is hard.

38
00:06:24,000 --> 00:06:33,000
And it seems to be moving away from pleasure in many ways.

39
00:06:33,000 --> 00:06:46,000
The Buddha even acknowledged this and reminded us to acknowledge the gratification of sensuality that there is the gratification, and it is there.

40
00:06:46,000 --> 00:06:51,000
But it doesn't take much to see the potential danger.

41
00:06:51,000 --> 00:07:10,000
With the example of this monk, of course, we're talking about sort of the very purest sort of livelihood. It doesn't seem dangerous for someone to exchange medical services for food, for example.

42
00:07:10,000 --> 00:07:13,000
No matter how luxurious it is.

43
00:07:13,000 --> 00:07:31,000
But it just speaks to how high a standard monks are held to, becoming a monk is a complete acceptance of a complete rejection of this whole system of pleasure-seeking.

44
00:07:31,000 --> 00:07:37,000
But we can see this happening in the world, and it's much more easy to see if we talk about it on a gross level.

45
00:07:37,000 --> 00:07:42,000
We see how the addiction to drugs, for example.

46
00:07:42,000 --> 00:07:54,000
Certainly, engages this on a very extreme level, this idea that the opportunity for pleasure exists, therefore seeking it is the way to, of course, find pleasure.

47
00:07:54,000 --> 00:07:58,000
But implicitly, to find happiness.

48
00:07:58,000 --> 00:08:02,000
Because we equate pleasure with this word, happiness.

49
00:08:02,000 --> 00:08:04,000
We equate them.

50
00:08:04,000 --> 00:08:08,000
We think of them, what pleasure means happiness, happiness means pleasure.

51
00:08:08,000 --> 00:08:10,000
They are one and the same.

52
00:08:10,000 --> 00:08:18,000
And yet a person who engages in drugs, for example, ends up being more unhappy, obviously.

53
00:08:18,000 --> 00:08:23,000
Their life becomes more unhappy.

54
00:08:23,000 --> 00:08:26,000
It involves the...

55
00:08:26,000 --> 00:08:38,000
It's because there's what you might consider a catch-22 to invoke that literary title.

56
00:08:38,000 --> 00:08:45,000
That, in order to experience pleasure, you have to chase after it.

57
00:08:45,000 --> 00:08:52,000
But a person who chases after pleasure, or the act of chasing after pleasure, reduces.

58
00:08:52,000 --> 00:08:59,000
It reduces the amount of pleasure that you gain, reduces the potentiality to obtain pleasure.

59
00:08:59,000 --> 00:09:08,000
If you think about a drug addict, it reduces the brain's capacity to gain pleasure from the same experiences.

60
00:09:08,000 --> 00:09:16,000
But if you think of it in a worldly sense, an external sense as well, a drug addict loses respect.

61
00:09:16,000 --> 00:09:18,000
They lose friends.

62
00:09:18,000 --> 00:09:21,000
They get in trouble with the law, but through stealing.

63
00:09:21,000 --> 00:09:26,000
Because the means of obtaining become more and more extreme.

64
00:09:26,000 --> 00:09:33,000
As you follow this to its logical conclusion, if seeking pleasure is always the right answer,

65
00:09:33,000 --> 00:09:45,000
then the consequences or the means of obtaining it, becoming consequential, the details of how you obtain it.

66
00:09:45,000 --> 00:09:46,000
You ignore them.

67
00:09:46,000 --> 00:09:50,000
You go beyond what is appropriate.

68
00:09:50,000 --> 00:10:03,000
It is very little, and we dedicate ourselves to no pursuit of central pleasure.

69
00:10:03,000 --> 00:10:08,000
But for a layperson, we can see it in things like drug addiction.

70
00:10:08,000 --> 00:10:11,000
We can also see it in business.

71
00:10:11,000 --> 00:10:13,000
We see it in the stock market.

72
00:10:13,000 --> 00:10:19,000
I mentioned the stock market because, coincidentally, there's a very big story in the news.

73
00:10:19,000 --> 00:10:32,000
That I noticed and started reading about, apparently, there's this in the stock market, a little bit of a tangent.

74
00:10:32,000 --> 00:10:35,000
But very much related.

75
00:10:35,000 --> 00:10:43,000
They take money and say, I'll owe you a certain number of stocks.

76
00:10:43,000 --> 00:10:50,000
They say, stocks are worth $10. Give me a hundred, and I'll owe you ten stocks.

77
00:10:50,000 --> 00:10:51,000
And they do this.

78
00:10:51,000 --> 00:10:53,000
So they take the money.

79
00:10:53,000 --> 00:10:56,000
But they do it with stocks that are going down in value.

80
00:10:56,000 --> 00:11:02,000
So as the value goes down, they can buy the stocks and give them back, and they made money on it.

81
00:11:02,000 --> 00:11:04,000
Because they buy later when it's lower.

82
00:11:04,000 --> 00:11:06,000
They're betting on the fact that it's going to go lower.

83
00:11:06,000 --> 00:11:20,000
So, apparently, the entire stock market or the rich people bought a hundred borrowed 140% of the stocks of this company, which there's only 100%.

84
00:11:20,000 --> 00:11:22,000
They bought too many.

85
00:11:22,000 --> 00:11:24,000
They borrowed too many.

86
00:11:24,000 --> 00:11:30,000
Another group of amateur investors found out about this.

87
00:11:30,000 --> 00:11:43,000
And now the whole stock market's in a tizzy, because they can't pay it back, and billionaires are losing millions of dollars, and amateur investors are making millions of dollars, and so on.

88
00:11:43,000 --> 00:11:59,000
I bring it up because just reading about it, and looking at this, this is the epitome of this seeking out of gain and the greed involved.

89
00:11:59,000 --> 00:12:16,000
And ultimately, it leads in the stock market, especially, to an increase and increase of greed until you manipulate the stock market and engage in illegal practices and so on.

90
00:12:16,000 --> 00:12:19,000
And you can lose your entire wealth and income.

91
00:12:19,000 --> 00:12:21,000
And there's no end to it.

92
00:12:21,000 --> 00:12:38,000
So, the whole system of pleasure seeking is caught up in a catch-22, that the more you seek out, the more trouble you get into.

93
00:12:38,000 --> 00:12:42,000
When you stop seeking out, of course, you don't get the pleasure.

94
00:12:42,000 --> 00:12:53,000
And so, on a fundamental level, pleasure cannot be equated with happiness. It can't make you happy, because you have two choices and neither one works.

95
00:12:53,000 --> 00:13:01,000
You seek out the pleasure, and you need more pleasure. You stop seeking out the pleasure, and you don't get any pleasure.

96
00:13:01,000 --> 00:13:06,000
It has a system, it just doesn't work.

97
00:13:06,000 --> 00:13:15,000
So, when we talk about in this verse the idea of living an easier life, we're really talking about on the short term.

98
00:13:15,000 --> 00:13:35,000
And we're talking about on the external level, a person who engages in betting on the stock market, engages in drug addiction, even a monk who engages in medical practices, obsessed with the idea that they're going to gain food is

99
00:13:35,000 --> 00:13:40,000
inevitably going to come to greater suffering.

100
00:13:40,000 --> 00:13:48,000
Their life becomes easier as a result, but their mind becomes corrupt.

101
00:13:48,000 --> 00:13:57,000
And so, when we think of the law of karma, we have to understand it on a mental level first.

102
00:13:57,000 --> 00:14:09,000
Karma works on its very base on a mental level. We can't look at what happens in the external world, because, of course, it's far too complicated.

103
00:14:09,000 --> 00:14:23,000
And because it involves past consequences, being born as a human being, we have so much potential for obtaining pleasure, and our corruption that the corruption that comes from seeking the pleasure isn't going to catch up with us.

104
00:14:23,000 --> 00:14:31,000
Not quickly, because we're protected by all the many factors that are set, the physical factors.

105
00:14:31,000 --> 00:14:47,000
But the mental factors are immediate. A person who engages in the acquisition of central pleasure is immediately caught up in this web of craving, increased desire.

106
00:14:47,000 --> 00:14:55,000
This is where meditation comes in, and this is what you see much more clearly on the meditative level, the experiential level.

107
00:14:55,000 --> 00:15:07,000
It's very difficult to see karma working in the world around us when we see rich people engaging in these manipulative practices or even drug addicts.

108
00:15:07,000 --> 00:15:14,000
If you look at them externally, it's hard to see what's really happening, where the danger is.

109
00:15:14,000 --> 00:15:20,000
For this monk, it's hard to see what the danger is. He appeared to be living quite well, and the Buddha remarked on it.

110
00:15:20,000 --> 00:15:42,000
But the danger is very real and very fundamental, much more fundamental than the externalities, because we have rich billionaires now quite upset because they're losing money, and they're certainly not going to be living on the streets as a result of this upheaval, but they're certainly upset about it.

111
00:15:42,000 --> 00:15:51,000
And the people who are angry at them are upset for breaking the law and so on.

112
00:15:51,000 --> 00:16:00,000
You can be surrounded by great pleasure, and in fact, the more pleasure you surround yourself with, the more unhappy you become.

113
00:16:00,000 --> 00:16:11,000
The more dissatisfied you become, because the act of acquiring the pleasure increases your desire for it, it's a habit.

114
00:16:11,000 --> 00:16:29,000
Everything in the mind works in terms of habits. Whatever you cultivate, whatever you engage in becomes your habit, desiring something, going after it, increases the impulse to go after it.

115
00:16:29,000 --> 00:16:48,000
So it doesn't lead to contentment. It's not, if I get this, I'll be content, because you're engaging this impulse to obtain, and that increases the habit of needing to obtain. It's impossible to satisfy.

116
00:16:48,000 --> 00:17:10,000
So in the Buddha said that a person who lives corrupt lives an easy life, he was only talking about the externality. He was pointing out what we have to understand about Buddhism and about meditation practice specifically, that it's not going to bring us more pleasure.

117
00:17:10,000 --> 00:17:18,000
It's not going to make life easier, in many, many cases, most cases we might say, it's going to make life harder.

118
00:17:18,000 --> 00:17:26,000
That's not intentional, and that's certainly not across the board, and it's certainly not a long-term problem.

119
00:17:26,000 --> 00:17:36,000
Because of course, when you engage in purity, people respect you more, your mind becomes more focused, many good things start to come to you.

120
00:17:36,000 --> 00:17:48,000
But no matter how many good things come to you, you refrain from seeking them out. You refrain from indulging your desire for them.

121
00:17:48,000 --> 00:18:02,000
We have to understand this, this will always be the case, and it's intentionally the case, because our intention is to move in another direction.

122
00:18:02,000 --> 00:18:18,000
Our happiness, our search for happiness is in another direction. Our happiness is outside of externalities, our happiness is independent of our experiences. That's the goal.

123
00:18:18,000 --> 00:18:33,000
So when we practice meditation, this is where I think it hits most viscerally and really most importantly for us.

124
00:18:33,000 --> 00:18:40,000
Meditation can be, and quite often is, and perhaps you could say should be difficult.

125
00:18:40,000 --> 00:18:45,000
Meditation is for the purpose of overcoming, craving, freeing ourselves from craving.

126
00:18:45,000 --> 00:18:54,000
You're free from craving, you don't have to practice meditation, but in so far as we have craving, it's going to be difficult, because it's going to challenge us.

127
00:18:54,000 --> 00:19:05,000
It's going to change our attitude, it's going to change our interaction with those things that we crave.

128
00:19:05,000 --> 00:19:11,000
It's going to help us or force us to see them objectively.

129
00:19:11,000 --> 00:19:15,000
It's not so much about denying yourself pleasure.

130
00:19:15,000 --> 00:19:22,000
It doesn't work in the conventional sense of abstaining.

131
00:19:22,000 --> 00:19:25,000
Mindfulness doesn't work that way.

132
00:19:25,000 --> 00:19:28,000
It works in the way of seeing objectively.

133
00:19:28,000 --> 00:19:35,000
So a thing that you might desire or crave after, rather than saying, no, no, I refuse.

134
00:19:35,000 --> 00:19:44,000
You can see, as the middle wave, absolutely, the middle wave seeing that object as it is.

135
00:19:44,000 --> 00:19:52,000
Not, oh, this is nice, I want this, but also not, this is dangerous, I must stay away.

136
00:19:52,000 --> 00:19:54,000
This is this.

137
00:19:54,000 --> 00:19:59,000
It avoids the pitfalls of both courses of action.

138
00:19:59,000 --> 00:20:05,000
If you are, of course, as we said, chasing after it, this is good.

139
00:20:05,000 --> 00:20:13,000
You cultivate that, but by avoiding it, you don't gain any special knowledge about pleasure and desire.

140
00:20:13,000 --> 00:20:23,000
You create, instead, an impulse of aversion, and you can see this, you'll start to become bitter and unhappy.

141
00:20:23,000 --> 00:20:31,000
And in the beginning for many monks and meditators, it can be quite an unhappy life because you find yourself doing that.

142
00:20:31,000 --> 00:20:34,000
You remind yourself, oh, this way is not right.

143
00:20:34,000 --> 00:20:38,000
And this is what the Buddha found him doing for, himself doing for six years.

144
00:20:38,000 --> 00:20:45,000
Repressing, repressing the desires, pushing away the desire, rejecting it.

145
00:20:45,000 --> 00:20:51,000
Until he found this, what he called the middle wave, and he saw, pleasure isn't the problem.

146
00:20:51,000 --> 00:20:53,000
Pleasure is just pleasure.

147
00:20:53,000 --> 00:20:59,000
The problem is that I want the pleasure, the problem is just, is that, there's a distinction there.

148
00:20:59,000 --> 00:21:02,000
I don't have to reject pleasure, he said.

149
00:21:02,000 --> 00:21:09,000
I just have to see it clearly and free myself from the desire for it.

150
00:21:09,000 --> 00:21:23,000
So, what we see quite often with new meditators is they become discouraged.

151
00:21:23,000 --> 00:21:27,000
They become discouraged because the results are not as they would expect.

152
00:21:27,000 --> 00:21:37,000
What we expect ordinarily from meditation is pleasure because that's how we understand happiness.

153
00:21:37,000 --> 00:21:41,000
Oh, I'm unhappy, meditation will make me happy.

154
00:21:41,000 --> 00:21:43,000
Therefore, it must bring me pleasure.

155
00:21:43,000 --> 00:21:44,000
And it doesn't.

156
00:21:44,000 --> 00:21:46,000
Mindfulness, meditation doesn't.

157
00:21:46,000 --> 00:21:50,000
And there are many experiences in meditation that can bring that pleasure.

158
00:21:50,000 --> 00:21:54,000
Many types of meditation that can bring pleasure.

159
00:21:54,000 --> 00:22:00,000
And so we seek out those and we become discouraged when practicing mindfulness because it doesn't seem to be doing that.

160
00:22:00,000 --> 00:22:05,000
It doesn't seem to be bringing us happiness at all, not in the beginning.

161
00:22:05,000 --> 00:22:13,000
Until we make that paradigm shift, until we come to see beyond pleasure.

162
00:22:13,000 --> 00:22:19,000
Seeing pleasure for pleasure, pain for pain, experience just as experience.

163
00:22:19,000 --> 00:22:22,000
Then you start to understand what true happiness is.

164
00:22:22,000 --> 00:22:32,000
True happiness is inextricable from freedom, from independence.

165
00:22:32,000 --> 00:22:40,000
It requires that you free yourself from need, from desire, from addiction, from aversion.

166
00:22:40,000 --> 00:22:43,000
It requires objectivity and clarity.

167
00:22:43,000 --> 00:22:49,000
And so the last word of these two verses that the Buddha used is perhaps the most important.

168
00:22:49,000 --> 00:22:52,000
It seems someone unconnected to the rest.

169
00:22:52,000 --> 00:22:57,000
It's not an equivalent of, you see, the verses are parallels.

170
00:22:57,000 --> 00:23:02,000
One talking about being corrupt, the other talking about being pure.

171
00:23:02,000 --> 00:23:03,000
But then he adds the last word, buseta.

172
00:23:03,000 --> 00:23:08,000
Buseta means seeing, or one who sees.

173
00:23:08,000 --> 00:23:11,000
Because that's ultimately what purity means.

174
00:23:11,000 --> 00:23:13,000
It doesn't mean rejecting pleasure.

175
00:23:13,000 --> 00:23:18,000
It means seeing pleasure and pain equally and clearly as they are.

176
00:23:18,000 --> 00:23:20,000
So that's what we try to do in meditation.

177
00:23:20,000 --> 00:23:26,000
And I think, as you can hopefully see, it's a very important teaching and important verse.

178
00:23:26,000 --> 00:23:30,000
That challenges us and helps us to see the depth of the teaching.

179
00:23:30,000 --> 00:23:33,000
That's not just a better way to find pleasure.

180
00:23:33,000 --> 00:23:36,000
But a different way to understand pleasure.

181
00:23:36,000 --> 00:23:39,000
And a better way to understand happiness.

182
00:23:39,000 --> 00:23:42,000
So that's the Dhammapad of her tonight.

183
00:23:42,000 --> 00:23:43,000
Thank you for listening.

184
00:23:43,000 --> 00:23:47,000
I wish you all peace, happiness, and freedom from suffering.

185
00:23:47,000 --> 00:23:57,000
Thank you.

