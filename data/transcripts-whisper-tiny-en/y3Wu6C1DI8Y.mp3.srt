1
00:00:00,000 --> 00:00:14,000
So rice or rice asks, whenever I hear or learn a new teaching of dharma, I find a deep feeling of peace and tranquility.

2
00:00:14,000 --> 00:00:21,000
Sometimes this reigns though, is a small moment. How can I learn to maintain this for longer?

3
00:00:21,000 --> 00:00:28,000
The reason I wanted to answer this one first is because this is exactly what got me on the spiritual path.

4
00:00:28,000 --> 00:00:32,000
This exact experience, so it must be incredibly common.

5
00:00:32,000 --> 00:00:40,000
When I was 16, I was probably born in the story and I was born a bit better.

6
00:00:40,000 --> 00:00:46,000
I was in humanity, challenge and change class.

7
00:00:46,000 --> 00:00:51,000
Well, it's really has nothing to do with the story, but it was a senior class.

8
00:00:51,000 --> 00:00:55,000
So I was in it early. It was a class that senior high school students would take.

9
00:00:55,000 --> 00:00:59,000
For seniorized students, that junior high school students would also take,

10
00:00:59,000 --> 00:01:03,000
so there were a couple of juniors in this class.

11
00:01:03,000 --> 00:01:07,000
And I was one of them. And I decided that I was sitting in the middle of the class

12
00:01:07,000 --> 00:01:12,000
and these two senior students, one of them passed a book to throw me to another one.

13
00:01:12,000 --> 00:01:15,000
And on the way back, I looked at it.

14
00:01:15,000 --> 00:01:24,000
And it was the day of 18, a interpretation by Stephen Mitchell.

15
00:01:24,000 --> 00:01:26,000
And I opened it.

16
00:01:26,000 --> 00:01:31,000
And the first, just some random page, and the first thing I read from it,

17
00:01:31,000 --> 00:01:33,000
just totally blew me away.

18
00:01:33,000 --> 00:01:38,000
It would just change my life because I had been looking.

19
00:01:38,000 --> 00:01:42,000
You know, when you're young, you're wondering why and what is the purpose of life?

20
00:01:42,000 --> 00:01:47,000
And you're starting to think of the spiritual things and thinking about

21
00:01:47,000 --> 00:01:52,000
the ways of bettering yourself, the ways of finding the truth.

22
00:01:52,000 --> 00:01:55,000
So I have to have kind of high spiritual goals.

23
00:01:55,000 --> 00:01:58,000
They usually get trashed in high school.

24
00:01:58,000 --> 00:02:05,000
I love to know about the romance itself.

25
00:02:05,000 --> 00:02:10,000
But this really was like, I found it. I found what I'm looking for.

26
00:02:10,000 --> 00:02:16,000
It brought me this incredible sense of peace and tranquility.

27
00:02:16,000 --> 00:02:19,000
And so I became Taoist at that point.

28
00:02:19,000 --> 00:02:24,000
And that's what led me on the spiritual path.

29
00:02:24,000 --> 00:02:29,000
For the next five years or four years,

30
00:02:29,000 --> 00:02:32,000
I would keep this book with me.

31
00:02:32,000 --> 00:02:35,000
I'd been able to even bought my own copy.

32
00:02:35,000 --> 00:02:37,000
I would keep this book with me.

33
00:02:37,000 --> 00:02:40,000
And I remember sitting in university,

34
00:02:40,000 --> 00:02:43,000
there was 19, three years later.

35
00:02:43,000 --> 00:02:45,000
Sitting on a bench, reading this book,

36
00:02:45,000 --> 00:02:49,000
and just finding this wonderful sense of peace and tranquility,

37
00:02:49,000 --> 00:02:51,000
and then having it disappear.

38
00:02:51,000 --> 00:02:53,000
Having it weighed, as you said.

39
00:02:53,000 --> 00:02:55,000
You're sitting there, read it,

40
00:02:55,000 --> 00:02:58,000
or you'll learn the new teaching.

41
00:02:58,000 --> 00:03:00,000
How it would bring to such peace and happiness,

42
00:03:00,000 --> 00:03:02,000
and then it disappeared.

43
00:03:02,000 --> 00:03:04,000
And then you think, how this is the question,

44
00:03:04,000 --> 00:03:08,000
and how can I learn to maintain this for long?

45
00:03:08,000 --> 00:03:13,000
So this is just back on how I can relate to this question.

46
00:03:13,000 --> 00:03:18,000
The answer to this question is that it's not the fact.

47
00:03:18,000 --> 00:03:21,000
And it's not...

48
00:03:21,000 --> 00:03:24,000
It relates back to what I was just saying about happiness.

49
00:03:24,000 --> 00:03:29,000
And the last really is that this is what makes us take on the spiritual path.

50
00:03:29,000 --> 00:03:31,000
This desire for happiness.

51
00:03:31,000 --> 00:03:35,000
This is what makes us take on any path.

52
00:03:35,000 --> 00:03:40,000
The reason why someone tries to find success in the world

53
00:03:40,000 --> 00:03:43,000
becomes a business person or becomes a doctor,

54
00:03:43,000 --> 00:03:45,000
becomes a teacher, becomes a teacher.

55
00:03:45,000 --> 00:03:48,000
It's in some way because they want to find happiness.

56
00:03:48,000 --> 00:03:50,000
Because they find happiness from helping other people,

57
00:03:50,000 --> 00:03:52,000
because they find happiness in money,

58
00:03:52,000 --> 00:03:54,000
or anything this is going to make them happy.

59
00:03:54,000 --> 00:03:56,000
So the spiritual path is the same.

60
00:03:56,000 --> 00:03:58,000
We take on the spiritual path,

61
00:03:58,000 --> 00:04:00,000
thinking it's going to bring us happy.

62
00:04:00,000 --> 00:04:03,000
The secret is that it really doesn't.

63
00:04:03,000 --> 00:04:08,000
It helps us give up our desire to find this idea of happiness.

64
00:04:08,000 --> 00:04:10,000
It's really interesting.

65
00:04:10,000 --> 00:04:12,000
It's really hard for people to accept it.

66
00:04:12,000 --> 00:04:15,000
You never want to think that you have to give up happiness.

67
00:04:15,000 --> 00:04:18,000
Because I mean, we do everything for happiness.

68
00:04:18,000 --> 00:04:21,000
But this is really the secret of the spiritual life,

69
00:04:21,000 --> 00:04:26,000
is giving up this thing that we can have in this.

70
00:04:26,000 --> 00:04:29,000
Giving up this distress that comes from trying to find something

71
00:04:29,000 --> 00:04:30,000
that doesn't exist.

72
00:04:30,000 --> 00:04:33,000
Something that is not possible to exist,

73
00:04:33,000 --> 00:04:40,000
that is an idea of something that is made up of completely different things.

74
00:04:40,000 --> 00:04:42,000
The idea of this being happiness,

75
00:04:42,000 --> 00:04:46,000
that is made up of things that are not unhappiness,

76
00:04:46,000 --> 00:04:48,000
but are not satisfying.

77
00:04:48,000 --> 00:04:50,000
You're not bringing any,

78
00:04:50,000 --> 00:04:52,000
you know, it cannot be spoken of the arise in this sense.

79
00:04:52,000 --> 00:04:53,000
And they're all mixed up,

80
00:04:53,000 --> 00:04:55,000
and they're all, you know, in sequence,

81
00:04:55,000 --> 00:04:57,000
you see something that brings a moment of pleasure

82
00:04:57,000 --> 00:04:59,000
and the moment of pleasure,

83
00:04:59,000 --> 00:05:01,000
the moment of pleasure brings desire for it,

84
00:05:01,000 --> 00:05:03,000
the desire brings thinking about it,

85
00:05:03,000 --> 00:05:04,000
how to get it and so on.

86
00:05:04,000 --> 00:05:06,000
And totally lost.

87
00:05:06,000 --> 00:05:10,000
And striving for things and work that we have to do and so on,

88
00:05:10,000 --> 00:05:12,000
to get the things we want.

89
00:05:16,000 --> 00:05:18,000
That in the end,

90
00:05:18,000 --> 00:05:22,000
it's this desire to increase this peace and tranquility

91
00:05:22,000 --> 00:05:24,000
that is going to eventually help you realize

92
00:05:24,000 --> 00:05:26,000
that even the greatest,

93
00:05:26,000 --> 00:05:29,000
even the greatest feeling that you can possibly have,

94
00:05:29,000 --> 00:05:31,000
which is the Jonathan's,

95
00:05:31,000 --> 00:05:33,000
these tranquility meditations,

96
00:05:33,000 --> 00:05:37,000
even these are impermanent.

97
00:05:37,000 --> 00:05:41,000
The good thing about them is that they bring you,

98
00:05:41,000 --> 00:05:44,000
the states of peace and tranquility

99
00:05:44,000 --> 00:05:47,000
and the practices to attain them,

100
00:05:47,000 --> 00:05:50,000
the practice of some of the meditation.

101
00:05:50,000 --> 00:05:53,000
So the danger of the problem is that they're impermanent,

102
00:05:53,000 --> 00:05:54,000
but they don't know.

103
00:05:54,000 --> 00:05:57,000
But actually the truth to them, the ultimate reality,

104
00:05:57,000 --> 00:06:01,000
is that they're impermanent, they're unsatisfying,

105
00:06:01,000 --> 00:06:02,000
and they're uncontrollable.

106
00:06:02,000 --> 00:06:04,000
Or you say they're impermanent,

107
00:06:04,000 --> 00:06:05,000
and they're uncontrollable.

108
00:06:05,000 --> 00:06:07,000
Therefore, they're unsatisfying.

109
00:06:07,000 --> 00:06:11,000
You can't control them.

110
00:06:11,000 --> 00:06:13,000
You can learn to maintain them for longer,

111
00:06:13,000 --> 00:06:15,000
but that's only going to increase your delusion.

112
00:06:15,000 --> 00:06:20,000
It's when they only increase the idea that they are yours,

113
00:06:20,000 --> 00:06:21,000
that they are controllable,

114
00:06:21,000 --> 00:06:25,000
that they're dependable, that they are somehow happiness,

115
00:06:25,000 --> 00:06:28,000
that they are somehow making you more happy.

116
00:06:28,000 --> 00:06:32,000
The pleasure that you get from is real,

117
00:06:32,000 --> 00:06:34,000
but it is meaningless.

118
00:06:34,000 --> 00:06:36,000
It's not true happiness.

119
00:06:36,000 --> 00:06:38,000
When you finish, you're not happier.

120
00:06:38,000 --> 00:06:40,000
You're not a more peaceful, more content person.

121
00:06:40,000 --> 00:06:42,000
This was the other thing I learned on the course

122
00:06:42,000 --> 00:06:44,000
that I was related to that, is contentment.

123
00:06:44,000 --> 00:06:46,000
How discontent,

124
00:06:46,000 --> 00:06:51,000
even on a spiritual path,

125
00:06:51,000 --> 00:06:52,000
you can become.

126
00:06:52,000 --> 00:06:53,000
How do you want more?

127
00:06:53,000 --> 00:06:55,000
How do you want to do that?

128
00:06:55,000 --> 00:06:56,000
How do you want to teach people?

129
00:06:56,000 --> 00:06:59,000
How do you want to build a meditation center?

130
00:06:59,000 --> 00:07:02,000
Generally, you were talking to Rakita,

131
00:07:02,000 --> 00:07:04,000
who said they're not just joined us,

132
00:07:04,000 --> 00:07:07,000
and Rakita was saying he wants to go back to England

133
00:07:07,000 --> 00:07:10,000
to teach, and I tend to do it without wanting.

134
00:07:10,000 --> 00:07:13,000
We teach people just because

135
00:07:13,000 --> 00:07:21,000
to pay back the Buddhist or to respect the Buddhist intentions

136
00:07:21,000 --> 00:07:25,000
for people to learn the teachings or to respect people's intentions.

137
00:07:25,000 --> 00:07:29,000
We should do it in a way that makes us

138
00:07:29,000 --> 00:07:32,000
that is comfortable for us.

139
00:07:32,000 --> 00:07:35,000
We shouldn't make ourselves suffer

140
00:07:35,000 --> 00:07:43,000
to go out of a way to suffer in order to spread the dhamma

141
00:07:43,000 --> 00:07:44,000
or spread Buddhism or something.

142
00:07:44,000 --> 00:07:47,000
At least you do things for the purpose of making this.

143
00:07:47,000 --> 00:07:49,000
The purpose of our own happiness.

144
00:07:49,000 --> 00:07:51,000
Because this is what everyone wants.

145
00:07:51,000 --> 00:07:53,000
We want to be happy with it.

146
00:07:53,000 --> 00:07:56,000
And you didn't say the purpose of our happiness.

147
00:07:56,000 --> 00:07:58,000
But this is what got me thinking about.

148
00:07:58,000 --> 00:08:00,000
Why are we doing things?

149
00:08:00,000 --> 00:08:09,000
Why are we chasing after worldly things?

150
00:08:09,000 --> 00:08:12,000
Or why are we even building a meditation center?

151
00:08:12,000 --> 00:08:14,000
Why are we teaching people?

152
00:08:14,000 --> 00:08:17,000
Why do we live our lives?

153
00:08:17,000 --> 00:08:18,000
And this is the point.

154
00:08:18,000 --> 00:08:21,000
We live our lives because we want to play happiness.

155
00:08:21,000 --> 00:08:26,000
And the realization was that really this teaching

156
00:08:26,000 --> 00:08:33,000
and contentment is so very important to remind ourselves

157
00:08:33,000 --> 00:08:40,000
that this is not the way to find happiness.

158
00:08:40,000 --> 00:08:43,000
This discontent is always wanting more and needing more

159
00:08:43,000 --> 00:08:47,000
and not just being content with what is here in there.

160
00:08:47,000 --> 00:08:50,000
Because here and now we are all sitting in this room.

161
00:08:50,000 --> 00:08:54,000
But our minds are creating things.

162
00:08:54,000 --> 00:08:58,000
We want to explain to you what I'm saying.

163
00:08:58,000 --> 00:09:02,000
And this is what you understand what I'm saying.

164
00:09:02,000 --> 00:09:07,000
And so I'm no longer content here now.

165
00:09:07,000 --> 00:09:11,000
And this contentment is the really,

166
00:09:11,000 --> 00:09:15,000
the thought of it as part of the key to finding

167
00:09:15,000 --> 00:09:17,000
what you might contribute to some happiness.

168
00:09:17,000 --> 00:09:20,000
It's contentment with things as they are.

169
00:09:20,000 --> 00:09:25,000
And it's going to be the piece of letting go of any search

170
00:09:25,000 --> 00:09:26,000
for happiness.

171
00:09:26,000 --> 00:09:29,000
So the short answer to your question

172
00:09:29,000 --> 00:09:32,000
will help others from something that's still on something

173
00:09:32,000 --> 00:09:37,000
that you say is that this isn't the point

174
00:09:37,000 --> 00:09:40,000
to learn to maintain it for longer.

175
00:09:40,000 --> 00:09:43,000
It's normal that this happens and it's natural

176
00:09:43,000 --> 00:09:45,000
that happens because this is the nature of things.

177
00:09:45,000 --> 00:09:48,000
And the answer is not to try to maintain it for longer.

178
00:09:48,000 --> 00:09:54,000
It's just a lot of being to become content with things as they are.

179
00:09:54,000 --> 00:10:01,000
I remember being in mind briefly not this one.

180
00:10:01,000 --> 00:10:12,000
So much being in a lot of more is being in a lot of meditation,

181
00:10:12,000 --> 00:10:20,000
retreat, unfriendly environment, and meditating so much

182
00:10:20,000 --> 00:10:25,000
that I, even in this environment,

183
00:10:25,000 --> 00:10:28,000
could find place in full quality.

184
00:10:28,000 --> 00:10:41,000
And I found this is like being in the eye of the hurricane.

185
00:10:41,000 --> 00:10:47,000
And this became something that I understood

186
00:10:47,000 --> 00:10:50,000
or how I can explain.

187
00:10:57,000 --> 00:10:59,000
Peace.

188
00:10:59,000 --> 00:11:06,000
You won't find peace because it's not existing in this world.

189
00:11:06,000 --> 00:11:10,000
Peace is an idea that we have

190
00:11:10,000 --> 00:11:15,000
or an experience that rises sometimes.

191
00:11:15,000 --> 00:11:19,000
But mostly I think it's a wanting

192
00:11:19,000 --> 00:11:24,000
and the idea of feeling.

193
00:11:24,000 --> 00:11:32,000
And when you, as Bhanta said, learn to let go of the wanting

194
00:11:32,000 --> 00:11:38,000
and when you learn that it doesn't matter when it rains

195
00:11:38,000 --> 00:11:45,000
that it doesn't matter if it's not there at all that peace within you,

196
00:11:45,000 --> 00:11:52,000
then you can feel that kind of in the eye of the hurricane.

197
00:11:52,000 --> 00:11:56,000
Everything moves around fast or slow,

198
00:11:56,000 --> 00:12:02,000
however it moves or however it goes.

199
00:12:02,000 --> 00:12:10,000
But the peace is just there in that moment within you.

200
00:12:10,000 --> 00:12:19,000
And this only comes when you let go of the wanting of it.

201
00:12:19,000 --> 00:12:27,000
And this comes from the acceptance of what is going around you

202
00:12:27,000 --> 00:12:32,000
and not to go against what is happening around you.

203
00:12:32,000 --> 00:12:43,000
So the wish to keep it is the one thing that prevents

204
00:12:43,000 --> 00:12:47,000
the peace to last for me.

205
00:12:47,000 --> 00:12:52,000
But I think that it's going to be necessary for me.

206
00:12:52,000 --> 00:13:01,000
What comes, of course, to my mind,

207
00:13:01,000 --> 00:13:09,000
talking of peace and this is what would be the only thing

208
00:13:09,000 --> 00:13:18,000
that, along as it lasts, is peaceful, really truly peaceful.

209
00:13:18,000 --> 00:13:23,000
Thank you so much.

210
00:13:23,000 --> 00:13:26,000
Thank you.

211
00:13:26,000 --> 00:13:31,000
I guess she wants to show me what she looks like.

212
00:13:31,000 --> 00:13:34,000
I think I'm going to go.

213
00:13:34,000 --> 00:13:36,000
I think I'm going to go.

214
00:13:36,000 --> 00:13:39,000
I think I'm going to go.

215
00:13:39,000 --> 00:13:42,000
I think I'm going to go.

216
00:13:42,000 --> 00:13:49,000
I'm going to go.

217
00:13:49,000 --> 00:13:58,000
I'm going to go.

218
00:13:58,000 --> 00:14:04,000
I think just kind of saying the same thing.

219
00:14:04,000 --> 00:14:14,000
I think both of you are standing in another way.

220
00:14:14,000 --> 00:14:19,000
Especially on course, as I guess it becomes really apparent

221
00:14:19,000 --> 00:14:25,000
that even really for supply and states of peace that arise,

222
00:14:25,000 --> 00:14:37,000
it's not a stable platform to sit on.

223
00:14:37,000 --> 00:14:38,000
Trust.

224
00:14:38,000 --> 00:14:45,000
I can't trust any real state of mind that I'm aware is arising.

225
00:14:45,000 --> 00:14:46,000
It's going to last.

226
00:14:46,000 --> 00:14:56,000
I think I'm very distrusting of feeling, this is safe,

227
00:14:56,000 --> 00:15:05,000
this is good, this joy, this peace, this calm.

228
00:15:05,000 --> 00:15:11,000
It has the two sides like the mask, the happy side mask,

229
00:15:11,000 --> 00:15:18,000
the dark and the light inside the moon sometimes.

230
00:15:18,000 --> 00:15:21,000
Being on the light side of the moon forget that there's a dark side,

231
00:15:21,000 --> 00:15:25,000
even though it's there and it just keeps.

232
00:15:25,000 --> 00:15:31,000
It's almost like more peace that has come from just trusting

233
00:15:31,000 --> 00:15:38,000
that I can't trust that this peace is worth holding onto is something

234
00:15:38,000 --> 00:15:43,000
that I can take refuge in.

235
00:15:43,000 --> 00:15:48,000
Just letting go and taking more of a huge and noticing

236
00:15:48,000 --> 00:15:53,000
that okay this tomb is not going to last.

237
00:15:53,000 --> 00:15:58,000
Just being mindful that as pleasant or unpleasant,

238
00:15:58,000 --> 00:16:01,000
it's actually depressing if I go all this happiness,

239
00:16:01,000 --> 00:16:04,000
this peace isn't going to last, but when you're depressed,

240
00:16:04,000 --> 00:16:09,000
or in a total state of despair that it seems completely unshakable,

241
00:16:09,000 --> 00:16:12,000
it's actually kind of uplifting to remember, oh yeah,

242
00:16:12,000 --> 00:16:15,000
this isn't going to last either.

243
00:16:15,000 --> 00:16:18,000
So, I guess that's all I have to add.

244
00:16:18,000 --> 00:16:22,000
I just wanted to add something small that that kind of more relates

245
00:16:22,000 --> 00:16:25,000
to your actual question than what you've been saying is

246
00:16:25,000 --> 00:16:29,000
in regards to where this this happiness comes from

247
00:16:29,000 --> 00:16:32,000
and where it's placed is because on a more mundane level

248
00:16:32,000 --> 00:16:35,000
to explain what the problem here is,

249
00:16:35,000 --> 00:16:38,000
that this happiness in peace is not,

250
00:16:38,000 --> 00:16:45,000
it's just based on intellectual acceptance

251
00:16:45,000 --> 00:16:49,000
of reflective acceptance of the truth or of the teachings

252
00:16:49,000 --> 00:16:51,000
of something that is wildest.

253
00:16:51,000 --> 00:16:55,000
And it's on a very superficial level.

254
00:16:55,000 --> 00:17:03,000
The same sort of peace and tranquility is found

255
00:17:03,000 --> 00:17:08,000
on a far more sustainable level

256
00:17:08,000 --> 00:17:11,000
through actual meditation, whether they've been said

257
00:17:11,000 --> 00:17:18,000
when you come to see for yourself the teachings,

258
00:17:18,000 --> 00:17:21,000
when you have to realize the teachings of a visceral level

259
00:17:21,000 --> 00:17:24,000
and when it really becomes a part of your core,

260
00:17:24,000 --> 00:17:27,000
when you're describing what semitas is describing

261
00:17:27,000 --> 00:17:29,000
basically nibita in a way.

262
00:17:29,000 --> 00:17:31,000
It's not just, you know,

263
00:17:31,000 --> 00:17:33,000
the one way of explaining was not trusting

264
00:17:33,000 --> 00:17:38,000
but the way it's more explained in the meditation tradition

265
00:17:38,000 --> 00:17:44,000
is losing passion for it or losing your interest

266
00:17:44,000 --> 00:17:49,000
and realizing that this is not not as exciting

267
00:17:49,000 --> 00:17:51,000
or wonderful as you thought it was,

268
00:17:51,000 --> 00:17:55,000
not that desire for an attachment to it.

269
00:17:55,000 --> 00:17:58,000
There was knowledge of dispassionate

270
00:18:04,000 --> 00:18:07,000
and that this comes from,

271
00:18:09,000 --> 00:18:11,000
from actually seeing things as that,

272
00:18:11,000 --> 00:18:14,000
this is what brings true peace in the country.

273
00:18:14,000 --> 00:18:16,000
It's when it brings commitment

274
00:18:16,000 --> 00:18:20,000
and brings patience, which brings peace

275
00:18:20,000 --> 00:18:23,000
and the mind will enter into us

276
00:18:23,000 --> 00:18:25,000
quite a similar feeling of peace and tranquility

277
00:18:25,000 --> 00:18:27,000
but it'll be much more sustainable

278
00:18:27,000 --> 00:18:30,000
because suddenly it doesn't matter where it rises

279
00:18:30,000 --> 00:18:35,000
and eventually this becomes more and more the case

280
00:18:35,000 --> 00:18:36,000
and more and more things

281
00:18:36,000 --> 00:18:38,000
and as the mind starts to understand

282
00:18:38,000 --> 00:18:40,000
the more comprehensive level,

283
00:18:40,000 --> 00:18:43,000
how the whole world is like this.

284
00:18:43,000 --> 00:18:44,000
It starts to give up more and more

285
00:18:44,000 --> 00:18:47,000
and more and more and more

286
00:18:47,000 --> 00:18:52,000
able to be able to be with the reality of the experience

287
00:18:52,000 --> 00:18:57,000
in content without extrapolating

288
00:18:57,000 --> 00:19:02,000
or proliferating or projecting experience.

289
00:19:02,000 --> 00:19:07,000
This is what brings true and lasting peace in country.

290
00:19:07,000 --> 00:19:10,000
So there is something that we said about true peace

291
00:19:10,000 --> 00:19:13,000
and tranquility that is deeper than what your experience

292
00:19:13,000 --> 00:19:15,000
and this was the problem of me

293
00:19:15,000 --> 00:19:17,000
and trying to get it from this book

294
00:19:17,000 --> 00:19:19,000
is really superficial

295
00:19:19,000 --> 00:19:21,000
and it's like, yeah, yeah.

296
00:19:21,000 --> 00:19:24,000
But you're only getting it on a very superficial level

297
00:19:24,000 --> 00:19:27,000
and it's not really sunk into your bones

298
00:19:27,000 --> 00:19:30,000
based on intensive practice

299
00:19:30,000 --> 00:19:32,000
and realization to the point where the mind

300
00:19:32,000 --> 00:19:34,000
really undergoes change.

301
00:19:34,000 --> 00:19:35,000
I mean, reading the teachings

302
00:19:35,000 --> 00:19:37,000
and learning the teachings is a great thing

303
00:19:37,000 --> 00:19:38,000
but you have to understand

304
00:19:38,000 --> 00:19:40,000
that as a very superficial part

305
00:19:40,000 --> 00:19:41,000
of what we're doing.

306
00:19:41,000 --> 00:19:43,000
Many people misunderstand that

307
00:19:43,000 --> 00:19:46,000
I'm reading and listening to so many teachings

308
00:19:46,000 --> 00:19:48,000
and never actually practicing

309
00:19:48,000 --> 00:19:49,000
and so as a result,

310
00:19:49,000 --> 00:19:51,000
good feeling times disappears.

311
00:19:51,000 --> 00:19:53,000
It's very superficial

312
00:19:53,000 --> 00:19:55,000
and it's very unstable.

313
00:19:55,000 --> 00:19:57,000
It's like a drop in the ocean

314
00:19:57,000 --> 00:19:58,000
so you get that piece

315
00:19:58,000 --> 00:20:00,000
within the rest of your mind.

316
00:20:00,000 --> 00:20:02,000
It's totally like I've been contrary to it.

317
00:20:02,000 --> 00:20:05,000
Once you see it intensively,

318
00:20:05,000 --> 00:20:07,000
again and again and again, eventually,

319
00:20:07,000 --> 00:20:09,000
it really permeates your whole being

320
00:20:09,000 --> 00:20:11,000
and then your mind becomes much better.

321
00:20:11,000 --> 00:20:13,000
I mean, it also has to do with experience

322
00:20:13,000 --> 00:20:15,000
of new violence.

323
00:20:15,000 --> 00:20:20,000
I mean, that's basically an important thing.

324
00:20:20,000 --> 00:20:22,000
That's good to do with this.

325
00:20:22,000 --> 00:20:25,000
I don't think I'm talking about this one question,

326
00:20:25,000 --> 00:20:29,000
but I mean, it has to do with expectations too.

327
00:20:29,000 --> 00:20:31,000
It's like we took a picture at the end of this course

328
00:20:31,000 --> 00:20:33,000
that we just went through.

329
00:20:33,000 --> 00:20:35,000
It was quite intensive for all of us

330
00:20:35,000 --> 00:20:36,000
and then you're saying,

331
00:20:36,000 --> 00:20:39,000
okay, now we came to the end of a course

332
00:20:39,000 --> 00:20:41,000
and we should be all looking very happy now

333
00:20:41,000 --> 00:20:43,000
and look at the pictures afterwards,

334
00:20:43,000 --> 00:20:45,000
and you think, right, who is this?

335
00:20:45,000 --> 00:20:47,000
For people who look like they've gone through hell

336
00:20:47,000 --> 00:20:51,000
and that's wrong expectation, whatever that is.

337
00:20:51,000 --> 00:20:54,000
I mean, meditation is also like measuring

338
00:20:54,000 --> 00:20:56,000
and saying, I'm happy now

339
00:20:56,000 --> 00:20:58,000
when am I unhappy?

340
00:20:58,000 --> 00:21:00,000
It's the same with me.

341
00:21:00,000 --> 00:21:03,000
Having these robes, I feel always hard, I always say,

342
00:21:03,000 --> 00:21:04,000
I'm always sweating.

343
00:21:04,000 --> 00:21:06,000
You see, Dante here is always sitting there

344
00:21:06,000 --> 00:21:09,000
who never seems sweat and you've been running

345
00:21:09,000 --> 00:21:13,000
today through a chunk of my life in the morning.

346
00:21:13,000 --> 00:21:16,000
And so, I mean, it's the same temperature

347
00:21:16,000 --> 00:21:18,000
which just feels different than I do

348
00:21:18,000 --> 00:21:22,000
is that with the heat and it's the same with happiness.

349
00:21:22,000 --> 00:21:24,000
It's like, you know, I'm just saying,

350
00:21:24,000 --> 00:21:26,000
no, I'm unhappy.

351
00:21:26,000 --> 00:21:27,000
When am I happy?

352
00:21:27,000 --> 00:21:30,000
So this is for me, the question sometimes

353
00:21:30,000 --> 00:21:32,000
and the thing that they expectation

354
00:21:32,000 --> 00:21:34,000
and having seen these pictures

355
00:21:34,000 --> 00:21:36,000
is like another shock he's like, you know,

356
00:21:36,000 --> 00:21:37,000
expect to see you.

357
00:21:37,000 --> 00:21:38,000
Oh, they're so happy.

358
00:21:38,000 --> 00:21:39,000
It's great, isn't it?

359
00:21:39,000 --> 00:21:43,000
When I imagine Tom was looking like, you know, I'm happy.

360
00:21:43,000 --> 00:21:46,000
But, you know, it's just me interpreting it.

361
00:21:46,000 --> 00:21:49,000
That's why interest is true.

362
00:21:49,000 --> 00:21:51,000
That's something important to talk about

363
00:21:51,000 --> 00:21:53,000
when I was looking at the pictures and thinking,

364
00:21:53,000 --> 00:21:55,000
oh, put these up and people are thinking,

365
00:21:55,000 --> 00:21:56,000
what did you do for Dante?

366
00:21:56,000 --> 00:21:59,000
It's like, you know, in anything.

367
00:21:59,000 --> 00:22:03,000
But it's so deceiving because we were really happy

368
00:22:03,000 --> 00:22:08,000
and that, you know, this has been such a uplifting experience.

369
00:22:08,000 --> 00:22:10,000
But I've had this before.

370
00:22:10,000 --> 00:22:12,000
People come to our meditation center and say,

371
00:22:12,000 --> 00:22:14,000
you just don't look happy.

372
00:22:14,000 --> 00:22:16,000
You're talking about that.

373
00:22:16,000 --> 00:22:18,000
You know, how this should make you happy.

374
00:22:18,000 --> 00:22:20,000
You don't look happy.

375
00:22:20,000 --> 00:22:23,000
This is why it's a different understanding.

376
00:22:23,000 --> 00:22:25,000
There's so much, you know,

377
00:22:25,000 --> 00:22:28,000
and people have doubts as a result of a meditation practice,

378
00:22:28,000 --> 00:22:31,000
but you can never convince a meditation that they're wrong.

379
00:22:31,000 --> 00:22:33,000
Don't get you, you're not smiling.

380
00:22:33,000 --> 00:22:34,000
Don't you see that something is wrong?

381
00:22:34,000 --> 00:22:37,000
You're like, my parents tried to do it when I went back home.

382
00:22:37,000 --> 00:22:38,000
Look at you.

383
00:22:38,000 --> 00:22:39,000
You're like a zombie.

384
00:22:39,000 --> 00:22:40,000
You don't like guitar anymore.

385
00:22:40,000 --> 00:22:42,000
You don't tell jokes anymore.

386
00:22:42,000 --> 00:22:43,000
You're not fun.

387
00:22:43,000 --> 00:22:45,000
Don't you see?

388
00:22:45,000 --> 00:22:47,000
You've been brainwashed.

389
00:22:47,000 --> 00:22:51,000
And, you know, I was so upset by what they were saying,

390
00:22:51,000 --> 00:22:57,000
but in my mind I'm making their wrong.

391
00:22:57,000 --> 00:22:58,000
These people are wrong.

392
00:22:58,000 --> 00:23:00,000
They have no idea.

393
00:23:00,000 --> 00:23:01,000
It's not intellectual.

394
00:23:01,000 --> 00:23:02,000
It's like you can't.

395
00:23:02,000 --> 00:23:06,000
There's nothing inside anybody you can use to convince me

396
00:23:06,000 --> 00:23:08,000
that you're right.

397
00:23:08,000 --> 00:23:15,000
There's no doubt that maybe I should go back and do all of this thing.

398
00:23:15,000 --> 00:23:17,000
Because you found true peace,

399
00:23:17,000 --> 00:23:19,000
and you've understood the difference between peace

400
00:23:19,000 --> 00:23:26,000
and some state of happiness and some state of your experience.

401
00:23:26,000 --> 00:23:31,000
It's explaining that there arises talking about something that is freedom.

402
00:23:31,000 --> 00:23:36,000
What we gain through the meditation is called freedom.

403
00:23:36,000 --> 00:24:01,000
And freedom is the highest.

