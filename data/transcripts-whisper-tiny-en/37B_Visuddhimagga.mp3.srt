1
00:00:00,000 --> 00:00:08,600
So it is difficult to know what type of consciousness is referred to because when we study

2
00:00:08,600 --> 00:00:14,600
Abirama we study the manual manual of Abirama and so we are familiar with terms given

3
00:00:14,600 --> 00:00:25,600
in that book but in the commentary the names, the names are different than we are familiar

4
00:00:25,600 --> 00:00:32,600
with. So sometimes we are at a loss to know what type of consciousness is meant, my

5
00:00:32,600 --> 00:00:44,800
priest was, they were something like profitable result and mine element something like that.

6
00:00:44,800 --> 00:00:52,400
But here the numbers are given so you can easily go to the chart and find out and then

7
00:00:52,400 --> 00:00:58,720
the next to the profitable result in mine element, the root cost less, mine consciousness

8
00:00:58,720 --> 00:01:06,080
element accompanied by joy. Here root cost less are it you have, mine consciousness

9
00:01:06,080 --> 00:01:16,760
mano vinyana, element dado. And what does that mean? That means santirana investigation

10
00:01:16,760 --> 00:01:25,120
and so on. Now in the middle of the paragraph there is a saying, I think I have not told

11
00:01:25,120 --> 00:01:34,960
you about this even at my Abirama classes that registration or retention to daramana,

12
00:01:34,960 --> 00:01:43,280
how many times do they arise to daramana? Two or never, right? So they arise two times

13
00:01:43,280 --> 00:01:59,640
or they don't rise at all. Now what do you find here? By occurring either once or twice

14
00:01:59,640 --> 00:02:08,000
as registration. So according to some teachers, retention, I mean registration can arise

15
00:02:08,000 --> 00:02:18,320
once. But the common opinion is registration does not arise once, but when it arises

16
00:02:18,320 --> 00:02:27,400
it arises twice. So here also, magical commentary does not mean the commentary we now have.

17
00:02:27,400 --> 00:02:34,480
It is of all single is commentary, now lost. But in the Abirama commentary two tons of

18
00:02:34,480 --> 00:02:39,640
consciousness have been handed down with respect to it, registration. That means in the

19
00:02:39,640 --> 00:02:48,600
Abirama commentary it is that the registration occurs or arises two times. This consciousness

20
00:02:48,600 --> 00:02:55,840
has two names, registration to daramana literally having the object that the preceding

21
00:02:55,840 --> 00:03:05,840
impressions had and after my life continued, pretty bawanga. Pretty means back. So back

22
00:03:05,840 --> 00:03:24,920
bawanga. Okay. So the other paragraphs are like that explaining the consciousness

23
00:03:24,920 --> 00:03:41,640
with reverence to the functions. So that is the cause of an existence of pawati and now

24
00:03:41,640 --> 00:03:51,040
at rebirth linking. But what was said above namely s to the remaining 19, there is none

25
00:03:51,040 --> 00:03:56,040
that does not occur as a rebut linking appropriate to it. There are 19 types of consciousness

26
00:03:56,040 --> 00:04:03,120
which has a function of rebirth. And this is hard to understand since it is too brief and

27
00:04:03,120 --> 00:04:09,120
in order to show the details it may be asked. So there are given details so many things

28
00:04:09,120 --> 00:04:19,960
here. Now let me tell you one big about this chat. If you look at the chat, say,

29
00:04:19,960 --> 00:04:32,920
the second link, conditions of marriage, then chitana and kama vajirakusala 8, conditioning.

30
00:04:32,920 --> 00:04:40,960
And condition is in kama sugati, maha vibhaka, aikusala vibhaka, udukasandana and then

31
00:04:40,960 --> 00:04:52,560
you find pd, right? So I do not explain this in the sheets. So please note that pd means

32
00:04:52,560 --> 00:05:05,120
at rebirth, at rebirth, at that rebut, at that reb linking and pv means during the cause

33
00:05:05,120 --> 00:05:31,920
of life. That means after after rebut. Okay, now just above paragraph 135, for briefly

34
00:05:31,920 --> 00:05:37,480
rebut, leaning consciousness has three kinds of objects, namely first present and not

35
00:05:37,480 --> 00:05:55,040
so classifiable. That means concepts. Panjabi. Panjabi has said to be without time, out of time.

36
00:05:55,040 --> 00:06:02,240
So they are not classifiable as past or present or future. So not so classifiable means

37
00:06:02,240 --> 00:06:15,880
Panjati. Non-pacipian rebut linking has no object. Non-pacipian rebut, rebut linking.

38
00:06:15,880 --> 00:06:28,320
What does that mean? Asani. The rebut of those born without mind. You know, there are

39
00:06:28,320 --> 00:06:37,480
beings who are only physical, physical bodies. No mind. They are Brahma. So they practice

40
00:06:37,480 --> 00:06:46,640
Jana's and they are those who hate mind. Because they think that because they are

41
00:06:46,640 --> 00:06:53,080
this mind, we suffer. We know this is suffering, this is playing and something like that.

42
00:06:53,080 --> 00:07:01,480
So if there were no mind, we would be very happy or blissful. So they practice Jana and

43
00:07:01,480 --> 00:07:09,360
as the result of that Jana, they are reborn as Brahma's without mind or mental activity.

44
00:07:09,360 --> 00:07:17,920
So they are like statues and they still like stages for 500 years and then they come back.

45
00:07:17,920 --> 00:07:25,200
So for their relinking, there is no object. Since there is no relinking consciousness.

46
00:07:25,200 --> 00:07:35,880
So at the moment of their relinking, there is only material properties. And so whenever

47
00:07:35,880 --> 00:07:45,480
you see, not so classifiable, take it to me, Panjati concepts. And then paragraph

48
00:07:45,480 --> 00:07:53,280
one another is six. The beginning for example. I think for example is not correct. For

49
00:07:53,280 --> 00:08:01,720
example means you use take only a part of, say, a part of the list or something, right?

50
00:08:01,720 --> 00:08:08,720
Not all. But here the body word sea dita means, means what are they or something like that.

51
00:08:08,720 --> 00:08:18,200
That means I'm going to give you all details. So it should be something like namely.

52
00:08:18,200 --> 00:08:38,960
That is something like that. I think these are the red slowly. They are going through as

53
00:08:38,960 --> 00:08:50,400
far as we do now because they are very important part of Abirama explaining the process

54
00:08:50,400 --> 00:09:06,680
of dead and rebut. So from happy to unhappy destiny, that means how a person is reborn from

55
00:09:06,680 --> 00:09:15,360
a happy, happy rep to unhappy rep. That means how a person, for example, how a person dies

56
00:09:15,360 --> 00:09:23,960
as a human being and reborn in hell or reborn as an animal and so on. So they are described

57
00:09:23,960 --> 00:09:36,640
here from happy to unhappy destiny. And then next page, I mean 633 from unhappy to happy destiny.

58
00:09:36,640 --> 00:09:49,720
And then from happy to happy destiny. And in the middle of that paragraph, close to number

59
00:09:49,720 --> 00:09:58,880
550, 550 square brackets. But in the case of one who has told Abkama of the exalted

60
00:09:58,880 --> 00:10:06,600
spheres, exalted spheres means Mahagata. So Mahagata means Rupa, Vichara and Arubhana.

61
00:10:06,600 --> 00:10:36,080
The kind of Kama comes into focus and so on. Then, so here the three kinds of objects

62
00:10:36,080 --> 00:10:48,120
of, I mean, relinking, bawanga, and Julia given. A sign of happy destiny in other words,

63
00:10:48,120 --> 00:10:54,040
the appearance of the mother's womb in the case of the human wall and so on. So sometimes

64
00:10:54,040 --> 00:11:01,560
Kama appears to be the mind of a person who is about to die, sometimes sign of Kama and

65
00:11:01,560 --> 00:11:10,560
sometimes sign of destiny. Sign of destiny is taken to be always present. But sign of Kama

66
00:11:10,560 --> 00:11:23,760
may be present or past. But Kama is always past. So next page, his rebutling in consciousness

67
00:11:23,760 --> 00:11:29,480
arises next to the dead consciousness. And the order shown for the sign of an unhappy destiny.

68
00:11:29,480 --> 00:11:42,920
That means that Rupa has to barograph 137. And then, in another case, relatives present

69
00:11:42,920 --> 00:11:47,760
objects to him at the five cents door, such as a visible datum as object, perhaps flowers,

70
00:11:47,760 --> 00:11:52,760
garlands, flex planets, etc. This is being offered with a blessed one for you sake. Dear,

71
00:11:52,760 --> 00:12:06,560
set your mind at rest and so on. Or is found as object and so on. So it is helping a man

72
00:12:06,560 --> 00:12:13,520
who is about to die. So helping him to get a cusola consciousness, cusola Kama, so that

73
00:12:13,520 --> 00:12:22,720
he can be reborn in a better existence. And so in that case, the object of the dead consciousness

74
00:12:22,720 --> 00:12:41,140
will be present object. Then from unhappy to unhappy destiny. So now, how Kama is a

75
00:12:41,140 --> 00:12:50,380
condition created 605 up to this point there has been shown the occurrence of the 19

76
00:12:50,380 --> 00:12:56,120
full consciousness as prepared thingy. Also, all this is for the classifier form. While

77
00:12:56,120 --> 00:13:02,720
it occurs in linking this, it is double class design through Kama and as mixed and not

78
00:13:02,720 --> 00:13:14,320
mixed and is still for the classifier and so on. So, paragraph 146 and 147 shows how Kama

79
00:13:14,320 --> 00:13:22,440
is a condition. And then paragraph 148 and following show the classification actually, classification

80
00:13:22,440 --> 00:13:37,640
of Kama that is a double class, mixed or not mixed and other classifications. It should

81
00:13:37,640 --> 00:13:44,160
be understood that when it occurs that it is double class, etc. is mixed or not. And it is still

82
00:13:44,160 --> 00:13:48,880
for the classifier. For example, to this type of consciousness occurs in one way only as

83
00:13:48,880 --> 00:13:55,160
we were thinking still, it is too full as divided into mixed and mixed with materiality.

84
00:13:55,160 --> 00:14:03,280
So rebutting consciousness arises and not mixed with materiality when it is a albāmajara

85
00:14:03,280 --> 00:14:10,400
but it is 3 full as divided according to sense, desire, find material and material becoming

86
00:14:10,400 --> 00:14:16,800
and so on. And it is full full as egg born, womb born, good sense, moisture born and of

87
00:14:16,800 --> 00:14:23,880
operational generation. You have to remember this, you have to imagine again and again.

88
00:14:23,880 --> 00:14:33,880
So there are four kinds of, what you call there, four kinds of participants, something like

89
00:14:33,880 --> 00:14:43,320
egg born, womb born, moisture born and operational generation. So what you see is generation

90
00:14:43,320 --> 00:14:56,440
or generation really means birth, something like that, not like this generation, next generation.

91
00:14:56,440 --> 00:15:03,160
And then it is 5 full according to destiny. So there are five kinds of destiny. They are

92
00:15:03,160 --> 00:15:08,560
to be found in Majemannigal. It is 7 full according to these stations of consciousness

93
00:15:08,560 --> 00:15:14,320
and it is 8 full according to the abodes of beings. So this is not of karma but of

94
00:15:14,320 --> 00:15:31,520
pretty something, I mean, relinking. So relinking can be only 1, it can be 3, it can be 4 and then

95
00:15:31,520 --> 00:15:43,160
it can be 7 and it can be 8. Now it is a 5 full destiny, at least 5 full just on the

96
00:15:43,160 --> 00:16:02,680
YouTube understand this. And 5 full destiny is, it is given later. So next phase, the

97
00:16:02,680 --> 00:16:15,080
mix is double, sex and not. So double means sex and not sex. So some are born with masculinity

98
00:16:15,080 --> 00:16:22,600
or femininity but there are the beings who are born without masculinity or femininity.

99
00:16:22,600 --> 00:16:35,560
So they are something like a, what do you call them, uniks, right? No, no sex.

100
00:16:35,560 --> 00:17:01,640
And then the least, decades, the first has got respectively are there or two and so on.

101
00:17:01,640 --> 00:17:12,600
So here the material properties rising with the, with the rebut linking mentioned

102
00:17:12,600 --> 00:17:20,040
that two decades and so on. But we have to understand the sixth chapter of the manual

103
00:17:20,040 --> 00:17:28,680
of Abirama. And then with 153, I mean, paragraph 153 here and how the different kinds

104
00:17:28,680 --> 00:17:34,360
of generations come about maybe understood according to the kind of destiny for every

105
00:17:34,360 --> 00:17:41,880
cutsties. No first three generations are in hell. With the deities, there should be also

106
00:17:41,880 --> 00:17:50,760
there, with the deities also. Save two of the, also fall in the three other destinies.

107
00:17:50,760 --> 00:18:01,880
And then here in, we should, we should insert some words here. Here in, by the word also,

108
00:18:01,880 --> 00:18:13,800
in the words with deities also. Now in, in probably the word, the word cha, I mean the

109
00:18:13,800 --> 00:18:23,640
particle cha is used. And sometimes it means some, some, something more. Like like also in English,

110
00:18:23,640 --> 00:18:31,320
say, deities also means deities and others. So by also, we should understand something,

111
00:18:31,320 --> 00:18:39,160
something else than deities. So there should be also in the verse and also by the also in the words

112
00:18:39,160 --> 00:18:44,360
with just the, with deities also, it should be understood that as in heaven, among deities,

113
00:18:44,360 --> 00:18:50,840
excepting up deities. So also among the ghosts consumed with thus. So we, we should take ghosts

114
00:18:50,840 --> 00:18:59,320
consumed with thirst, represented by the word also. So the first three kinds of generations

115
00:18:59,320 --> 00:19:04,920
are not far for their operational only. But in the remaining three kinds of destiny, in other words,

116
00:19:04,920 --> 00:19:09,640
among animals, ghosts, and human beings, and among the, up deities accepted above,

117
00:19:09,640 --> 00:19:14,680
there are all full kinds of generations. Now, there are five kinds of destinies, right?

118
00:19:17,080 --> 00:19:25,640
And how many do you find here? Hell is one, one destiny.

119
00:19:25,640 --> 00:19:39,960
Now, deities, another destiny. And then, like ghosts, I mean, ghosts consumed with thirst,

120
00:19:39,960 --> 00:19:52,040
something like that. Oh, yeah. And then what else? Human beings, one destiny.

121
00:19:52,040 --> 00:20:07,160
And deities, we have to, we have to differentiate two deities, deities accepting up deities.

122
00:20:07,160 --> 00:20:26,360
And then deities. And for the, for the hell, there is only one generation. I mean, only one,

123
00:20:26,360 --> 00:20:40,040
one, but that is operational, but something like that. So for the first, the first three kinds

124
00:20:40,040 --> 00:20:50,040
of generations are not far with regard to hell and deities, accepting up deities and ghosts consumed

125
00:20:50,040 --> 00:21:00,920
with thirst. But in the remaining three kinds of destiny, that is, another kind of hungry ghost,

126
00:21:03,880 --> 00:21:14,680
human beings, and animals. Yeah. I forgot about animals. Oh, animals are there.

127
00:21:14,680 --> 00:21:21,480
The remaining three kinds of destiny, in other words, among animals, ghosts, and human beings,

128
00:21:21,480 --> 00:21:27,800
and among the up deities, accepted upon, there are all full kinds of generations. So there can be

129
00:21:29,720 --> 00:21:38,840
egg born, womb born, moisture born, and operational born. There are four kinds of generations

130
00:21:38,840 --> 00:21:45,880
can form, can be formed there. And then the explanation, now the fine material codes of 39 and so on.

131
00:21:45,880 --> 00:21:54,840
So how many kinds of material properties arise with the fine material codes and so on?

132
00:21:59,000 --> 00:22:05,640
So among the fine material, Brahma's operational generation, they arise together with

133
00:22:05,640 --> 00:22:12,360
reporting in consciousness 30 and also nine material instances with the four groups, namely

134
00:22:12,360 --> 00:22:18,840
the decades of eye, ear, and physical basis, physical business, main heartless, and the

135
00:22:18,840 --> 00:22:31,880
end of life, you know, the end of life, and need, how do you say that? A group of nine, four

136
00:22:31,880 --> 00:22:46,600
inseparables and divita, and then about seven or eight lines down. Now the group of material states

137
00:22:46,600 --> 00:22:52,280
comprising the 10 material instances, namely, Kava, Olu, Flee, one nutritive essence and the

138
00:22:52,280 --> 00:23:00,200
four primary elements, with eye sensitivity and life, is, not end, is called the eye dagger.

139
00:23:01,800 --> 00:23:07,960
Eye dagger, ear dagger, and so on. So a group of material states is called the eye dagger.

140
00:23:08,520 --> 00:23:15,160
And they are Kava, Olu, Flee, one nutritive essence, four, and the four primary elements, eight,

141
00:23:15,160 --> 00:23:21,240
with eye sensitivity, nine, and life, 10. So they are called eye dagger. There are many groups

142
00:23:21,240 --> 00:23:25,880
of material states should be understood in the same way, that is, ear dagger, loose dagger,

143
00:23:25,880 --> 00:23:35,960
and so on. And then in the verse in paragraph 158, second line of the verse, under aggregates,

144
00:23:35,960 --> 00:23:44,680
objects cause, we should, we should correct it to root, not cause, or you can see root cause,

145
00:23:45,480 --> 00:23:53,480
not just cause.

146
00:23:53,480 --> 00:24:07,720
And then next page, paragraph 161, they are good. In mere state, let us call its conditions

147
00:24:07,720 --> 00:24:13,960
fascist in the ensuing existence. While it does not migrate from the first, with no cause in the

148
00:24:13,960 --> 00:24:24,440
first, it is not. So nothing from this life migrates to the other life, but it is reborn there,

149
00:24:24,440 --> 00:24:33,000
not without the cause here. So this is how we understand the rebirth. So if Buddhist accept another,

150
00:24:33,000 --> 00:24:42,360
right? There is no other, there is no permanent entity. And then they accept rebirth. So sometimes

151
00:24:42,360 --> 00:24:53,880
it seems incompatible. So you do not believe in anything, which is permanent and any soul or whatever.

152
00:24:53,880 --> 00:24:59,400
And then you are saying, he is reborn in another state and another state and so on. So the answer

153
00:24:59,400 --> 00:25:07,480
to this is this. In mere state, that is God's conditions, ashes in the ensuing existence. So it arises

154
00:25:07,480 --> 00:25:18,520
there, conditions by something here or something in the past. So, and when it does not migrate from

155
00:25:18,520 --> 00:25:25,000
the past, it does not go from the past to the present or to the present to the future, with no cause

156
00:25:25,000 --> 00:25:32,760
in the past, it is not. So that is cause of it. So it is a mere material and immaterial state arising

157
00:25:32,760 --> 00:25:38,360
when it is obtained is conditioned, that is spoken of, saying that it comes into the next becoming,

158
00:25:38,360 --> 00:25:43,080
it is not a lasting being, not a soul. And it is neither transmitted from the past becoming,

159
00:25:43,640 --> 00:25:50,200
nor yet is it manifested here without cause from there. And then we shall explain this by the

160
00:25:50,200 --> 00:25:56,840
normal process. That means obvious process of human death and rebirth linking and so on.

161
00:25:56,840 --> 00:26:06,040
And on the next page, the illustrations are given an echo or it is like supplies and so on.

162
00:26:06,040 --> 00:26:11,640
So the an echo, if light is still impression and a looking glass image. So these are these,

163
00:26:12,520 --> 00:26:17,400
the similes given when we explain rebirth. So an echo,

164
00:26:19,640 --> 00:26:25,720
a light is still impression and a looking glass. For the fact of it is not coming here from the

165
00:26:25,720 --> 00:26:31,400
previous becoming and for the fact that it arises going to causes that I included in past

166
00:26:31,400 --> 00:26:37,240
2000. So for just as an echo, a light is still impression and a share will have respectively

167
00:26:37,240 --> 00:26:42,600
sound, etc. as they are called and coming to being without going elsewhere. So also this

168
00:26:42,600 --> 00:26:59,480
is consciousness. And 167 and with the stream of continuity, there is neither identity nor otherness.

169
00:27:03,320 --> 00:27:10,600
Neither identity nor otherness. And if there were an absolute identity in the stream of continuity,

170
00:27:10,600 --> 00:27:17,400
there would be no foaming of card from milk. And yet if there were absolute otherness, the card

171
00:27:17,400 --> 00:27:22,520
would not be derived from the milk. And so to get all causally arisen things.

172
00:27:25,080 --> 00:27:30,440
And if there was so there would be an end to all world usage, which is hardly desirable. So neither

173
00:27:30,440 --> 00:27:38,360
absolute identity, no absolute otherness should be assumed here. Then if no transcribed,

174
00:27:38,360 --> 00:27:43,160
and France migration is manifested, then after the physician of the aggregates in this human

175
00:27:43,160 --> 00:27:48,840
person, that fruit could be another person or due to other karma. Since the karma, that is a

176
00:27:48,840 --> 00:27:55,080
condition for the fruit does not pass on there to where the fruit is. And whose is the fruit

177
00:27:55,080 --> 00:27:59,800
since there is no experience? Therefore, this formulation seems to be unsatisfactory.

178
00:27:59,800 --> 00:28:11,080
So one when just the karma and the other being gets to resolve something. So the answer is to

179
00:28:11,080 --> 00:28:20,120
continuity, like seeds and so on. And then what we call an experience is just the arising of

180
00:28:21,560 --> 00:28:26,840
just the arising of results. And so actually there is no experience I had all and so on.

181
00:28:26,840 --> 00:28:42,120
Then on page 174, I mean paragraph 174, about the middle of the paragraph. And therefore just

182
00:28:42,120 --> 00:28:48,600
as in the world when someone becomes an agent with the aim of completing some business or other,

183
00:28:48,600 --> 00:28:56,360
and he buys, could say, or obtain a law, it is simply the fact that he is performing the transaction,

184
00:28:56,360 --> 00:29:03,560
that is the condition for completing that business and so on. Now, it is not simply an agent.

185
00:29:04,600 --> 00:29:15,640
It is someone who promises to pay debt. That's something like a granda.

186
00:29:15,640 --> 00:29:27,480
You couldn't do something. And also, completing some business is very vague.

187
00:29:28,840 --> 00:29:37,800
The party word juicier is Neena Tana. And also, Pali language is a religious language.

188
00:29:37,800 --> 00:29:46,440
There are some words I think taken from Sanskrit, say, which we just to do with

189
00:29:47,320 --> 00:29:54,680
worldly things. And so I can say that it is a business party word. Neena Tana is a business

190
00:29:54,680 --> 00:30:00,120
party, but Neena Tana means praying back and not transacting a business, but praying back.

191
00:30:00,120 --> 00:30:11,640
So, the aim of praying what has been promised, or the aim of discharging the application, something like that.

192
00:30:11,640 --> 00:30:29,240
And then paragraph 175, and so on, explains the relationship according to Pali.

193
00:30:30,520 --> 00:30:37,240
So, you have to break this with reference to the chart. So, with the chart, you will understand

194
00:30:37,240 --> 00:30:46,040
that it can be easy. Amazing, something like that.

195
00:30:46,040 --> 00:31:09,480
Now, there's one thing worthy of note, and that is on page 643 at the top of the page.

196
00:31:09,480 --> 00:31:16,680
And then it is a condition for Brahma seeing undesirable visible data and hearing undesirable sounds

197
00:31:16,680 --> 00:31:22,840
that are in the sense fear. There are no undesirable visible data, etc. in the Brahma wall itself.

198
00:31:23,480 --> 00:31:30,920
So, in the Brahma walls, there are no undesirable sites and desirable sound and so on.

199
00:31:31,720 --> 00:31:36,120
But they can see undesirable sites, and they can hear answer to the answer,

200
00:31:36,120 --> 00:31:41,720
and desirable sounds from the sense fear. When they look down to the sense fear,

201
00:31:41,720 --> 00:31:49,560
they will see undesirable objects there. But in the Brahma wall itself, there is no undesirable

202
00:31:49,560 --> 00:31:57,800
visible sites, I mean, undesirable sites or undesirable sounds. And likewise, in the divine world of

203
00:31:57,800 --> 00:32:06,280
the sense fear. So, in the Deva Loka also, there are no undesirable objects, no Anita Aramana, because

204
00:32:08,360 --> 00:32:15,320
everything there is beautiful. But when they look down to the human wall,

205
00:32:15,320 --> 00:32:21,000
then they may see a lot of undesirable objects.

206
00:32:21,000 --> 00:32:32,600
Okay, I think that is the end of the second link, the second link between formations and consciousness.

207
00:32:32,600 --> 00:32:40,120
This is the longest of the links. I mean, the explanation, the longest explanation given by

208
00:32:40,120 --> 00:32:47,480
the Visadipma and the path of beautification. And there are a lot of information about

209
00:32:47,480 --> 00:32:58,200
Bhadisanti and death and rebirth, and also Kama and its results. So, as you read it slowly,

210
00:33:01,000 --> 00:33:09,560
and maybe you really help of manual Vabirama or a reading of manual Vabirama can help you here.

211
00:33:09,560 --> 00:33:22,600
So, it goes both ways. And those who have studied a Virama may know that here are the explanations

212
00:33:22,600 --> 00:33:25,880
for the manual of a Virama.

213
00:33:25,880 --> 00:33:39,800
For Kama and the results, the fifth chapter, and also the dead and the blood, the fifth chapter.

214
00:33:42,600 --> 00:33:49,400
So, you're the wrong retreat. We're going to have a retreat here. So, the last meeting for

215
00:33:49,400 --> 00:33:58,280
will be on December 14th. Yes. December 14th. So, it's one more meeting. This is 14th. So,

216
00:33:59,080 --> 00:34:01,640
which is four weeks from today.

217
00:34:01,640 --> 00:34:23,480
It's only from today, yes. December 14th.

218
00:34:23,480 --> 00:34:34,760
For the last class. With the help of the Visadipa chapter.

219
00:34:34,760 --> 00:35:00,920
What the notes are going on.

