1
00:00:00,000 --> 00:00:11,520
Hello everyone, here is a brief video announcement for the upcoming tour that I'm making

2
00:00:11,520 --> 00:00:16,520
down the east coast of North America.

3
00:00:16,520 --> 00:00:27,360
So for those of you who haven't heard about it, this next month, June 2014, I will be

4
00:00:27,360 --> 00:00:39,000
traveling mostly by bus and mostly with a companion from city to city, a tonal of six stops

5
00:00:39,000 --> 00:00:48,480
and I will be teaching and talking and meeting with people who have been following me

6
00:00:48,480 --> 00:00:51,160
on the internet for the most part.

7
00:00:51,160 --> 00:00:58,400
So the idea came up partly because my mother lives in Florida and that's where I'll be ending

8
00:00:58,400 --> 00:01:13,240
up, but mostly because of the number of people in North America who have contacted me

9
00:01:13,240 --> 00:01:22,040
trying to make some connection with a teacher and with some meditation practice and of course

10
00:01:22,040 --> 00:01:29,000
living in Canada or in Asia as I have for a lot of the time I've been to monk, it's hard

11
00:01:29,000 --> 00:01:36,280
to reach everyone and it's hard to provide a place where people can, or an opportunity

12
00:01:36,280 --> 00:01:40,360
for people to come to meet with us, meet with the teacher.

13
00:01:40,360 --> 00:01:47,080
So I thought why not take a trip and see as many people as I can now, it's obviously

14
00:01:47,080 --> 00:01:55,320
not going to help the majority of people, but I think a small step in the right direction

15
00:01:55,320 --> 00:01:58,400
and this year it's sort of an experiment.

16
00:01:58,400 --> 00:02:06,280
So I'd like to take this opportunity to go through the places that I'll be staying and

17
00:02:06,280 --> 00:02:12,240
just give a few words about each so you can have an idea of what will be going on there.

18
00:02:12,240 --> 00:02:19,040
I'm not entirely in the know about how things stand for each of these spots.

19
00:02:19,040 --> 00:02:25,680
I've left it very much up to the coordinators, but here is what I do know.

20
00:02:25,680 --> 00:02:33,240
I know that I will be flying into Boston, Massachusetts from Toronto, Ontario on the 9th

21
00:02:33,240 --> 00:02:40,920
of June, arriving in Boston at just afternoon, just after 12 noon, and then traveling

22
00:02:40,920 --> 00:02:52,160
to Worcesters, I don't know how you say it, W-O-O-R-C-E-S-T-E-R Massachusetts, and staying at

23
00:02:52,160 --> 00:03:00,760
Assumption College with some Catholic monks, I was invited there.

24
00:03:00,760 --> 00:03:06,240
I think when the first invitations I got when this idea came up was from a Catholic priest

25
00:03:06,240 --> 00:03:10,720
or no Catholic monk, I'm not sure now.

26
00:03:10,720 --> 00:03:21,360
I think he's a monk actually, yes, a Catholic monk, and with the thought that his

27
00:03:21,360 --> 00:03:26,360
brethren wouldn't be interested in this sort of thing, so it would be an interesting

28
00:03:26,360 --> 00:03:32,640
stay on me staying there for three nights, and he said they have many acres of land and

29
00:03:32,640 --> 00:03:37,400
lots of room for us to pitch tents, which we may do, or we may stay in rooms there, I'm

30
00:03:37,400 --> 00:03:38,920
not sure.

31
00:03:38,920 --> 00:03:45,640
Staying there for three nights until the 12th, and then on the 12th we take a bus to Long

32
00:03:45,640 --> 00:03:52,840
Island, New York, and we'll be staying at the Sri Lankan monastery there, which is called

33
00:03:52,840 --> 00:03:57,400
the Long Island Buddhist Meditation Center. I think there's another name for it as well,

34
00:03:57,400 --> 00:04:03,880
like the Long Island, Wehara, or something like that, but part of it is the Buddhist

35
00:04:03,880 --> 00:04:06,240
Meditation Center with Venerable Nanda Tera.

36
00:04:06,240 --> 00:04:13,400
Nanda Tera is a monk I know from Winnipeg, I met him at least once, I think twice, and

37
00:04:13,400 --> 00:04:19,400
I gave him one of my old video cameras, so we're acquaintances for friends, we don't

38
00:04:19,400 --> 00:04:24,360
know each other very well, but they were happy to have me, so I'll be staying in a Sri Lankan

39
00:04:24,360 --> 00:04:29,160
monastery in Long Island. It's in Port Jefferson.

40
00:04:29,160 --> 00:04:36,200
Now, one thing I should note before you all start clamoring for details is that the details

41
00:04:36,200 --> 00:04:43,000
of this entire trip are actually on the website at tarika.seriamungalod.org, and I'll

42
00:04:43,000 --> 00:04:52,520
put a link in the description to this video, so no need to worry about that. But the Long

43
00:04:52,520 --> 00:05:03,600
Island Buddhist Meditation Center, where I'll be staying in Port Jefferson, and then

44
00:05:03,600 --> 00:05:08,360
staying there three nights again until the 15th, and then on the 15th, traveling to Washington,

45
00:05:08,360 --> 00:05:15,720
D.C. to stay at a time monastery. It's a time monastery, I think, no, I think I haven't

46
00:05:15,720 --> 00:05:23,680
been to Washington, D.C. We took a road trip there many years ago. We drove down and

47
00:05:23,680 --> 00:05:29,360
got in a car accident. I was the navigator in the passenger seat, and we got in a car accident

48
00:05:29,360 --> 00:05:34,880
right in front of the White House, which is quite an experience. This hopefully will be

49
00:05:34,880 --> 00:05:43,720
more subdued trip, more meditative. Be staying at what? Tama Pratib, which is the time

50
00:05:43,720 --> 00:05:48,640
monastery. Again, the link should be on the website. If not, I'll put it there before

51
00:05:48,640 --> 00:05:55,720
I upload this video. Make sure all this is on there. Again, three nights there, and then

52
00:05:55,720 --> 00:06:03,920
on the 19th, traveling to South Carolina, and I believe it's an overnight bus trip, so

53
00:06:03,920 --> 00:06:09,960
leaving in the afternoon on the 18th, arriving in Greenville early in the morning on the

54
00:06:09,960 --> 00:06:19,040
19th of June. Staying there until the 21st. Now, that might change because the 21st is

55
00:06:19,040 --> 00:06:30,000
a holiday, and no, probably I'll stay, probably I'll have to leave on the 21st. Yes, well,

56
00:06:30,000 --> 00:06:37,000
we'll see anyway. It's the 21st, 19th to 21st, staying at Carolina Buddhist Viharo, which

57
00:06:37,000 --> 00:06:42,720
has a bikuni, a female monk from Sri Lanka, Venromosudina, who I've never met, but we've

58
00:06:42,720 --> 00:06:50,960
now corresponded over the internet, and sounds like they are quite welcoming, so that's

59
00:06:50,960 --> 00:06:58,200
great, much appreciated. Probably pitching tents again there, two nights, and then on the

60
00:06:58,200 --> 00:07:04,760
21st, heading to Atlanta, Georgia, actually, John's Creek, which I understand is just

61
00:07:04,760 --> 00:07:12,000
north of Atlanta, and we'll be staying in Atlanta for, again, for two nights at a private

62
00:07:12,000 --> 00:07:23,720
residence, and then on the 23rd, heading to Tampa, Florida, and again, I think it's an

63
00:07:23,720 --> 00:07:31,040
overnight trip, and then I'll be staying in the needan with my mother and teaching in

64
00:07:31,040 --> 00:07:40,000
Tampa for six nights, and on the 30th of June, I fly back to Canada. So I'll be teaching,

65
00:07:40,000 --> 00:07:48,320
I think, at University of South Florida, USF, and the Florida Buddhist Viharo, which

66
00:07:48,320 --> 00:07:55,320
I think it wasn't confirmed last I heard, but I'm thinking that's where I'll be teaching.

67
00:07:55,320 --> 00:07:59,720
Anyway, the whole time throughout this, I'll be available to meet with people. That's

68
00:07:59,720 --> 00:08:05,280
probably something I'm going to look forward to the most. There'll probably be formal

69
00:08:05,280 --> 00:08:11,160
talks, but at the very least, I will be meeting with people, and I'll have time to talk

70
00:08:11,160 --> 00:08:16,800
about meditation, give advice, pass on information, and knowledge that I've gained, and

71
00:08:16,800 --> 00:08:22,400
received myself. And so hopefully this will be a start of something, and maybe next year,

72
00:08:22,400 --> 00:08:27,960
I'll go down the west coast, or take longer, maybe I'll take two months next year to travel

73
00:08:27,960 --> 00:08:34,320
around North America, we'll see. Anyway, so this is an announcement, thought I'd let everyone

74
00:08:34,320 --> 00:08:40,200
know, and I hope that's useful for some people that they're able to connect with us. Again,

75
00:08:40,200 --> 00:08:48,280
go to the website, it's in the link, it's in the description, and maybe I'll just post

76
00:08:48,280 --> 00:08:58,400
the whole itinerary in the description. But either way, it's there somewhere, so hope to

77
00:08:58,400 --> 00:09:06,400
see you all there, and I'll be trying to stay online as much as possible, as it permits

78
00:09:06,400 --> 00:09:12,160
as we travel, because I think there's Wi-Fi on the buses, and so at the very least,

79
00:09:12,160 --> 00:09:18,480
we can post pictures and blogs, or whatever, try to record as much as I can, and probably

80
00:09:18,480 --> 00:09:22,920
won't bring a camera, but maybe have a phone camera, or at least recording audio anyway,

81
00:09:22,920 --> 00:09:30,640
we'll see. Probably some things will get recorded. Anyway, that's all, so lots of good

82
00:09:30,640 --> 00:09:37,640
stuff, wishing you all the best, and be well.

