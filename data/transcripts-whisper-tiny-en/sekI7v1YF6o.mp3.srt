1
00:00:00,000 --> 00:00:05,000
.

2
00:00:05,000 --> 00:00:08,000
.

3
00:00:08,000 --> 00:00:13,000
.

4
00:00:13,000 --> 00:00:16,000
.

5
00:00:16,000 --> 00:00:21,000
.

6
00:00:21,000 --> 00:00:28,000
.

7
00:00:28,000 --> 00:00:36,240
Okay, good evening, everyone.

8
00:00:36,240 --> 00:00:40,640
Welcome to our Saturday dumma.

9
00:00:40,640 --> 00:00:57,920
Today we are going to look at something called the Gannakam Mughalana sit down.

10
00:00:57,920 --> 00:01:05,360
There was a person called the Gannakam Mughalana, so that's the suit to God's name.

11
00:01:05,360 --> 00:01:13,280
It's typical of Buddhist scriptures and Buddhist culture, really to not, to not put too much

12
00:01:13,280 --> 00:01:22,600
emphasis on naming things, suitors don't have to have flowery or inspiring names, inspiration

13
00:01:22,600 --> 00:01:31,320
is can be useful, but it can also be misleading, sometimes you become attached to names.

14
00:01:31,320 --> 00:01:39,880
It's always interesting when you name a new monk to the actions.

15
00:01:39,880 --> 00:01:44,600
Sometimes the monk reacts and becomes very proud or maybe doesn't like their new name.

16
00:01:44,600 --> 00:01:54,760
It's had the case where the late community reacted quite violently to the naming of a monk,

17
00:01:54,760 --> 00:02:06,040
named a monk after a very famous Arahon Mugha, Mugha Raja, very famous in the time of the

18
00:02:06,040 --> 00:02:16,760
Buddha anyway, caused quite a kerfafo because people didn't understand where the name came from.

19
00:02:16,760 --> 00:02:21,200
So this is called Gannakam Mughalana, but we're not going to worry too much about the

20
00:02:21,200 --> 00:02:36,680
Buddha, but what we're most interested in is the Nambain.

21
00:02:36,680 --> 00:02:45,120
Gannakam Mughalana gets mentioned because he asks a good question, Gannakam Mughalana

22
00:02:45,120 --> 00:02:54,240
was a Brahman, which means he would have been a priest of some sort or someone involved

23
00:02:54,240 --> 00:03:01,080
with the practice of performing important rituals, rituals that were thought to be important

24
00:03:01,080 --> 00:03:07,200
and having spiritual learning and understanding of the order of society and so on as

25
00:03:07,200 --> 00:03:14,800
of course with the Brahmanic religion.

26
00:03:14,800 --> 00:03:21,600
But it wasn't really a religion, you could say that they had codes, but it wasn't a

27
00:03:21,600 --> 00:03:28,640
religion in the sense that they tried to separate themselves from all other teachers.

28
00:03:28,640 --> 00:03:35,400
It was just religious in a way, they were religious people because they would come to people

29
00:03:35,400 --> 00:03:40,720
like Buddha and ask them questions, it seems, they may be not all of them, but he asked

30
00:03:40,720 --> 00:04:06,880
it was interested in the Buddha's teaching and he considered, I think they would consider

31
00:04:06,880 --> 00:04:34,800
the

32
00:04:34,800 --> 00:04:56,420
Hello, hello, sorry about that, we're still alive, okay, and you'll hear enough about

33
00:04:56,420 --> 00:05:08,160
Raman. He asks a question of the Buddha. He says, if you look at this building, so the

34
00:05:08,160 --> 00:05:13,200
Buddha was staying in a place called Bhubharama. Bhubharama was Bhubharama means in the

35
00:05:13,200 --> 00:05:29,200
east, or it means the, I guess it means east, it was in the east of Sawati, and it was built

36
00:05:29,200 --> 00:05:38,740
paid for by Wizaka, who was a very famous land as I pulled the Buddha. Anyway, that's

37
00:05:38,740 --> 00:05:43,700
where they were staying in. It was so she made this big building for the Buddha to live in,

38
00:05:43,700 --> 00:05:48,780
and so Ganakomogalana points to the building that they're sitting in. He says, look at this

39
00:05:48,780 --> 00:05:57,780
building and you can see a gradual process involved. Actually, he says about the stairs,

40
00:05:57,780 --> 00:06:05,900
I'm not quite sure, but it seems there seems to be a bit of interpreting here, but he looks

41
00:06:05,900 --> 00:06:09,540
at the staircase and he says, you look down to the last step of the staircase into the

42
00:06:09,540 --> 00:06:17,980
progression there. So he starts the conversation, I'm talking about progressions. Now,

43
00:06:17,980 --> 00:06:23,020
the commentary says it actually refers to the building of the building. As you build it,

44
00:06:23,020 --> 00:06:27,900
you have to first lay down the foundation and then this, so you can look at a building

45
00:06:27,900 --> 00:06:37,620
and you can say, wow, there were steps involved with building this building. And then

46
00:06:37,620 --> 00:06:45,100
he says, among Brahmins, like me, we have a gradual process of becoming a brahmana or

47
00:06:45,100 --> 00:06:53,060
of progressing as a brahmana. Study, first you study the Vedas and you study the commentaries

48
00:06:53,060 --> 00:06:59,740
on Vedas and they have all these texts that you would have to study and then have to learn

49
00:06:59,740 --> 00:07:08,380
how to debate and how to explain and so on. And he says, or archers, you see, look at archers

50
00:07:08,380 --> 00:07:14,220
and they have to train first. They put the target close and then farther and farther

51
00:07:14,220 --> 00:07:23,260
and then they have moving targets and so on. He says, of accountants, there's also training,

52
00:07:23,260 --> 00:07:31,620
you know, many years of training to become an accountant. Make an account, take the

53
00:07:31,620 --> 00:07:42,940
apprentice and make him count one, one, two, two, three, three, four, four, five, five. And he says, now, is it possible, it's

54
00:07:42,940 --> 00:08:03,420
possible to describe in the Kottamas, Kottamas, the name of the Buddha. And Kottamas, sasana, is

55
00:08:03,420 --> 00:08:13,220
it? This udex, I think, is quite useful. It's not unique. There are many examples of very similar

56
00:08:13,220 --> 00:08:16,380
treatments and there shouldn't be a lot to surprise. Someone who's read a lot of the Buddha's

57
00:08:16,380 --> 00:08:27,180
teaching, if they read this text. But it's exemplary of this description of the paths.

58
00:08:27,180 --> 00:08:31,580
And a lot of these you'll notice are about path, a description of the path. And I think

59
00:08:31,580 --> 00:08:39,740
one last week was about the path, but in a different way. So here we're talking about the gradual

60
00:08:39,740 --> 00:08:47,980
process of becoming, really becoming a meditator and of becoming efficient in meditation. From the

61
00:08:47,980 --> 00:08:55,100
very beginning, when someone comes to you, that's the gradual process of becoming, you might say, in

62
00:08:55,100 --> 00:09:01,100
some ways, becoming Buddhist. In the sense of being someone who actually follows the Buddhist

63
00:09:01,100 --> 00:09:11,020
teaching, instead of just says, I'm bliss and I believe in Buddhism as well. And so it's a simple

64
00:09:11,020 --> 00:09:15,660
formulation, but I think it's useful to know. And so we won't skip this until we'll talk on this.

65
00:09:16,780 --> 00:09:23,820
And so the Buddha describes steps. I mean, describes steps in the dhamma and they're particularly

66
00:09:23,820 --> 00:09:31,500
appropriate for monks. They're directed towards a monastic community. But you can easily apply

67
00:09:31,500 --> 00:09:37,340
them if you adapt them. And what I'd like to do here is talk about ways that they can be generalized

68
00:09:37,340 --> 00:09:44,300
for really anyone who's interested in Buddhism. I think another thing this description does

69
00:09:44,300 --> 00:09:59,660
is it helps to provide a picture, sort of the sense of the bigger picture and the process involved

70
00:09:59,660 --> 00:10:08,140
in Buddhism for a newcomer, because there are teachings in different traditions and paths and

71
00:10:08,140 --> 00:10:18,300
religions that often talk about saints to be realized, but may not have a gradual path to lead

72
00:10:18,300 --> 00:10:29,260
their step by step teachings. I mean, even in Buddhism, there are some teachers who describe

73
00:10:29,260 --> 00:10:36,540
saints, but aren't able to describe a gradual process by which to get there. Maybe they themselves

74
00:10:36,540 --> 00:10:41,500
ought somewhere, but aren't quite clear how to get there. It's important to have this gradual

75
00:10:45,420 --> 00:10:50,540
on the other hand, they're teachings in different religions, even in Buddhism that are rather

76
00:10:50,540 --> 00:10:58,060
than having gradual process. They describe a first step. They give preliminaries where they give

77
00:10:58,060 --> 00:11:04,780
basic teachings. There's a lot of Buddhists who are caught up in practicing mate, which is great,

78
00:11:04,780 --> 00:11:09,020
but when that becomes your primary practice, you're not really going to get far, very far.

79
00:11:11,740 --> 00:11:17,260
Well, you're not going to get to nibana through that. You're not going to become free from suffering,

80
00:11:17,260 --> 00:11:23,660
or even more a lot of Buddhists and teacher Buddhist teachers will just teach people to take the

81
00:11:23,660 --> 00:11:31,100
refuge in the Buddha, to practice good deeds, charity, and so on. They keep the five precepts,

82
00:11:31,100 --> 00:11:36,940
and that's it, and they teach that again and again and again and again and it doesn't need anywhere,

83
00:11:36,940 --> 00:11:44,300
because that's just the first step. So here we have steps, and it's important to keep to remember them,

84
00:11:44,300 --> 00:11:52,140
to pay close attention and to actually put them into practice, so that step I step we can each little.

85
00:11:52,140 --> 00:12:00,860
So the first step that the Buddha describes, of course, is precepts, keeping rules.

86
00:12:01,980 --> 00:12:09,580
It's a living and disciplined life, having guidelines for your life. Having rules, I think,

87
00:12:09,580 --> 00:12:16,140
is probably a good way to describe it. Having rules is interesting. We had, we were talking about

88
00:12:16,140 --> 00:12:25,100
evil in glass this week, in my religious studies class, and it was an interesting debate about

89
00:12:25,660 --> 00:12:33,580
but in evil. The evil is a license, studying some own way, if you have anyone else who are probably

90
00:12:33,580 --> 00:12:41,020
not a bit French philosopher, and she says evil is a license. It was interesting because evil is

91
00:12:41,020 --> 00:12:49,180
therefore limitless. Evil doesn't put any limits on your behavior. Good is rather

92
00:12:50,060 --> 00:12:56,940
in some sense confined. Good is in a way restrictive, and that's why we've applied keen on it,

93
00:12:58,460 --> 00:13:02,780
because if you don't have rules, you can do anything in anywhere, and that's how the world is,

94
00:13:02,780 --> 00:13:12,860
right? For most part, our rules are vague and unclear, and so you easily have people become

95
00:13:13,500 --> 00:13:23,020
murderers and thieves and so on. Excited about evil in many cases, because they don't have a

96
00:13:23,020 --> 00:13:34,780
sense of the usefulness of rules. You could say they provide a direction, if we're talking about

97
00:13:34,780 --> 00:13:42,140
the path and we're going somewhere, and while you need a road, let's say, they don't go off the

98
00:13:42,140 --> 00:13:56,140
road because if you go off the road, you'll get lost, think it's stuck, might even start going the wrong way.

99
00:13:56,140 --> 00:14:04,620
For monks, there are many, many rules, but for lay people, the idea is just to have a sense of,

100
00:14:04,620 --> 00:14:11,100
this is right, this, I will do this, I will not do. I won't kill, I won't steal, I won't lie, I won't

101
00:14:11,100 --> 00:14:18,220
cheat. These are my rules, I won't take drugs or alcohol. That one last one's a really important,

102
00:14:18,220 --> 00:14:25,340
especially in modern times, when it's become quite accepted and often expected that people

103
00:14:25,340 --> 00:14:35,740
drink alcohol or take drugs in certain circles in certain society. It can be quite a very powerful

104
00:14:39,740 --> 00:14:46,140
powerful determination to say to yourself, I won't no longer, as a rule, I will not take

105
00:14:46,140 --> 00:14:53,340
drugs or alcohol. These are things that keep us pointed in the right direction, because if you

106
00:14:53,340 --> 00:14:57,740
engage in all of these, of course, you get pointed in the wrong direction quite easily.

107
00:14:58,780 --> 00:15:03,500
You don't have a rule against killing, anytime you engage in killing, it's leading you in the

108
00:15:03,500 --> 00:15:14,540
wrong direction, it's leading your mind towards corruption and deterioration and so on.

109
00:15:17,820 --> 00:15:21,900
So it's always the first step, before you're even going to talk about meditation, you really

110
00:15:21,900 --> 00:15:29,260
should be clear about your direction and therefore the rules that you're going to live by,

111
00:15:30,700 --> 00:15:36,540
at least the five precepts, or someone is really serious, you would want to keep the eight precepts.

112
00:15:38,380 --> 00:15:47,980
So not eating too much, not engaging in entertainment and only sleeping six hours or less,

113
00:15:47,980 --> 00:15:55,500
sleeping on the floor, that kind of thing. So that's the first one. The second one,

114
00:15:55,500 --> 00:16:01,980
he says, so when they've accomplished that, once person is more unethical, then we

115
00:16:01,980 --> 00:16:10,700
teach them to guard their senses. Second step, once you're settled in what's right and what's wrong,

116
00:16:10,700 --> 00:16:20,860
this is do this, don't do that, then you start to release and it's the next step in terms of action,

117
00:16:20,860 --> 00:16:26,140
in terms of our relationship with the world, this is the senses because once you've stopped

118
00:16:26,940 --> 00:16:35,180
doing any doing outward bad deeds, how killing, stealing, lying, cheating, even engaging in

119
00:16:35,180 --> 00:16:45,980
slight, wrong deeds, you guard the senses to keep from the arising of the emotions and the

120
00:16:46,700 --> 00:16:53,180
desires and versions that would again lead you to do or say things that you'd regret,

121
00:16:53,180 --> 00:16:55,660
that would cause you suffering or cause other people suffering.

122
00:16:55,660 --> 00:17:05,660
And so guarding the senses really is very central to our practice as Buddhists and especially

123
00:17:05,660 --> 00:17:14,140
as Buddhist meditators, the big part of the first steps in the meditation course where we teach

124
00:17:14,140 --> 00:17:20,300
the meditator to be mindful when you see anything say to yourself, see when you hear say hearing,

125
00:17:20,300 --> 00:17:26,060
this is guarding the senses, guarding your mind for getting caught up in what you see,

126
00:17:26,060 --> 00:17:33,340
don't just look at it and process all these things and get caught up in emotions like

127
00:17:33,340 --> 00:17:40,220
and dislikes, seeing when you see, remind yourself that's just seeing, when you hear that's hearing

128
00:17:40,220 --> 00:17:48,540
when you smell, smelling, taste, tasting, feel, feeling, we're going to put a lot of emphasis

129
00:17:48,540 --> 00:17:56,060
on the senses, decay, deep, the madam, we see that seeing, we see that would have seen,

130
00:17:56,060 --> 00:18:03,020
just be, would have seen nothing more, not this, not that, not me, not mine, not good, not bad,

131
00:18:04,700 --> 00:18:12,860
just seeing. Of course, there are other aspects of guarding your senses when you walk, not

132
00:18:12,860 --> 00:18:18,300
looking around everywhere and trying to see everything when you walk, keeping your mind on your

133
00:18:18,300 --> 00:18:25,260
feet when you walk and so on, that's guarding the eyes, guarding the ears means well, maybe

134
00:18:25,260 --> 00:18:34,060
don't run on music or go to places where there's sounds that can distract you,

135
00:18:34,060 --> 00:18:44,140
but most important and much more important, much more to be emphasized, is being mindful,

136
00:18:44,140 --> 00:18:50,380
guarding with mindfulness, because you can't always control sounds, you can't of course control

137
00:18:50,380 --> 00:18:57,580
sight, you can't close these doors, even though you might run away from all the good and bad

138
00:18:57,580 --> 00:19:03,900
things that you might see in here and so on. You can't run away from them, so ultimately you have

139
00:19:03,900 --> 00:19:14,940
to be mindful of whatever you experience hearing, hearing, hearing, hearing. So that's the second step.

140
00:19:17,100 --> 00:19:23,340
The third step he says is being moderate and eating, which I always was surprised at,

141
00:19:23,340 --> 00:19:30,380
because it seems like the single-out eating, why is that so important, the single-out,

142
00:19:30,380 --> 00:19:35,580
there's many other things. We have many other activities, but when you boil it down really,

143
00:19:35,580 --> 00:19:46,460
we don't live our lives, eating is really quite significant. It's the only thing that if you don't

144
00:19:46,460 --> 00:19:53,580
do hearing, it's one of the few things, if you don't, if you're going to die, eating, drinking,

145
00:19:53,580 --> 00:19:58,460
and breathing, I think, there's not much else. But eating is particular because

146
00:20:00,460 --> 00:20:12,300
we have a strong and long relationship with food that has quite caught up in likes and dislikes,

147
00:20:12,300 --> 00:20:21,580
and not even just the likes and dislikes of the flavor, but the fear of not getting enough food,

148
00:20:22,780 --> 00:20:27,660
not if you've ever been hungry and just felt the horror of the fact that today I'm not

149
00:20:28,380 --> 00:20:32,220
going to be able to eat because I don't have any money for food or that sort of thing,

150
00:20:32,940 --> 00:20:37,260
because my Buddhist monk and I didn't get homes for today, that sort of thing.

151
00:20:37,260 --> 00:20:43,340
You've ever been through that, you realize how attached we are to food, but also what a relief it is

152
00:20:43,340 --> 00:20:51,180
to let go. There was this book on George Orwell, I think, down and out in Paris and London. He got

153
00:20:51,180 --> 00:20:59,180
very poor, he managed to accidentally become impoverished, and he realized how liberating it is

154
00:20:59,180 --> 00:21:06,380
to be poor, because you're no longer, you're no longer have to worry about falling. There's nothing

155
00:21:06,380 --> 00:21:12,860
for you to guard, and you've reached the bottom, you know, you can't go any lower he's in.

156
00:21:17,500 --> 00:21:22,860
So, moderation and food is important to help us deal with this very basic

157
00:21:24,700 --> 00:21:31,180
addiction and obsession with food. I mean, there are other addictions and obsessions that are also

158
00:21:31,180 --> 00:21:37,900
useful, but useful to look at, of course, but food is the only one that we can't do without.

159
00:21:37,900 --> 00:21:46,780
You can avoid any other addictions, music, sex, those kinds of things, but food you still have

160
00:21:46,780 --> 00:21:53,340
to live with. So, definitely bears important mention. The other thing is if you eat too much,

161
00:21:53,340 --> 00:22:00,780
it's very hard to meditate here. You become, you get lazy amongst only in the morning,

162
00:22:01,980 --> 00:22:08,380
meditators as well, are meditators here only eating in the morning. It keeps you from doing

163
00:22:08,380 --> 00:22:16,140
a lot of physical labor, and that wouldn't really be possible, but it also keeps you from getting

164
00:22:16,140 --> 00:22:23,660
lazy and drowsy, and it keeps you sharp, and it keeps you on your toes. Sometimes you get hungry,

165
00:22:23,660 --> 00:22:35,740
that can happen. That's a morning part of the training to be aware of. We can expand this, of

166
00:22:35,740 --> 00:22:41,020
course, be aware of our food, be aware of the things we use. What it really means is the things

167
00:22:41,020 --> 00:22:49,580
that are necessities of life, food, clothes. Why do we wear the clothes we do? Why do we wear

168
00:22:49,580 --> 00:22:59,500
makeup? Why do we put makeup on? Why do we wear a hat or this hat? I'm buying this shirt or this

169
00:22:59,500 --> 00:23:06,300
pants. Why am I buying these ones? Is because they're cheap, or because they're sturdy,

170
00:23:06,300 --> 00:23:17,660
or is it because they think we look good? Because I like the color. Medicine, are we using medicine

171
00:23:17,660 --> 00:23:23,020
when it's important to use it, or we're just using it all the time whenever we have a small

172
00:23:23,020 --> 00:23:36,540
pain, whenever we have a small amount of this or that, and a shelter. Why do we live where we live,

173
00:23:36,540 --> 00:23:42,460
our bed? Why do we have a bed in the first place? I think there is something, but if you're higher,

174
00:23:42,460 --> 00:23:51,100
there's less dust that might be useful, but why do we have so soft beds? Why can't we sleep on a

175
00:23:51,100 --> 00:24:00,060
simple bed? Sleep on the carpet, and the taters will sleep on us. A thin mat, I sleep on the

176
00:24:00,060 --> 00:24:09,660
carpet, and then for a long time, it helps you wake up in the morning. Very important. So those

177
00:24:09,660 --> 00:24:15,580
kinds of things, the basics of our life, once we're cleared away all the bad stuff, how are we

178
00:24:15,580 --> 00:24:21,660
living our lives? Are they in the life that conducive to mindfulness and awareness, or is it conducive

179
00:24:21,660 --> 00:24:29,580
to laziness and addiction and clinging? Looking at the things that we use and be quite useful.

180
00:24:32,220 --> 00:24:42,380
So that's number three. Number four, number four is the schedule. Having a schedule, having a plan.

181
00:24:42,380 --> 00:24:52,780
So for an intensive meditate, what it says, I've talked about this before, he gives a schedule,

182
00:24:54,220 --> 00:25:05,260
he says you sleep four hours, and boiling it down. He describes it more in more detail, but basically,

183
00:25:05,260 --> 00:25:13,180
he says you sleep four hours, and the rest of the time is all just walking and sitting,

184
00:25:15,740 --> 00:25:21,580
and a very simple schedule. It says in the morning, in the afternoon, in the evening,

185
00:25:21,580 --> 00:25:31,100
walking and sitting, purifying your mind of instructive saints, purifying your mind of bad habits,

186
00:25:31,100 --> 00:25:40,540
really, changing our thought process. The way we live our lives ordinary, the course of a day,

187
00:25:41,420 --> 00:25:47,020
where does your mind go of a course of a day? It's an interesting question that we've probably

188
00:25:47,020 --> 00:25:52,700
never thought of in this way. If you were to enumerate the different mind states and the

189
00:25:52,700 --> 00:26:01,820
different emotions that went through, what does one day look like in the life of your mind? A lot of

190
00:26:01,820 --> 00:26:07,820
bad habits find through the meditation we tend to find our bad habits, and so we purify them. We

191
00:26:07,820 --> 00:26:17,420
live our lives in this way, walking, sitting, and being mindful and repurify it, because we see,

192
00:26:17,420 --> 00:26:23,660
oh, this is, this mind state that coming up now, that a mindful can see that it's

193
00:26:24,700 --> 00:26:30,380
unhelpful, so cause or stressfully, your mindful and your pay attention to it repeatedly,

194
00:26:31,500 --> 00:26:37,580
and you're able to change that, just through seeing it, just through seeing that it causes you suffering.

195
00:26:37,580 --> 00:26:49,340
That's the schedule of walking, sitting another interesting thing about this, is it points out

196
00:26:49,340 --> 00:26:56,300
walking, which is a question of many people I've met last. Did the Buddha really teach walking,

197
00:26:56,300 --> 00:27:01,660
or why can't we just sit all the time? The Buddha certainly didn't teach walking, and these

198
00:27:01,660 --> 00:27:07,980
this passage is repeated in many different places, that we should walk and sit in alternation.

199
00:27:09,340 --> 00:27:13,420
Because seeing all the time is not healthy for the body, it's not natural,

200
00:27:15,020 --> 00:27:25,660
it's not a way of a human being, it's not, it's not a way of life, walking and sitting is much more

201
00:27:25,660 --> 00:27:33,740
walking, some sitting, it's much more natural, and it's much better for you, because it balances,

202
00:27:33,740 --> 00:27:41,260
sitting can often make you tired, or focused, or concentrated, or as walking gives you energy,

203
00:27:41,260 --> 00:27:42,460
gives you strength.

204
00:27:48,540 --> 00:27:53,340
And when you do lie down to sleep, he says, this is ideally, I'm not going to suggest that

205
00:27:53,340 --> 00:27:58,220
everyone has to do this, he says lie down on your side, put one leg on top of the other,

206
00:28:00,460 --> 00:28:07,100
and note in your mind when you're going to wake up, say to yourself, okay, in four hours,

207
00:28:07,100 --> 00:28:14,620
or at this time, I'm going to wake up, meaning that what you should be focused on,

208
00:28:14,620 --> 00:28:19,100
when you go to sleep is when you're going to wake up, and there's a great power in that,

209
00:28:19,100 --> 00:28:23,900
because if you do that, and if you get good at it, you can find that you wake up quite close to

210
00:28:24,540 --> 00:28:31,260
when you determine to wake up, it's quite impressive how the mind's able to, to time things like that.

211
00:28:33,740 --> 00:28:36,780
It keeps you from oversleeping, it's a good practice.

212
00:28:42,140 --> 00:28:45,900
And then he says, he describes the practice in detail,

213
00:28:45,900 --> 00:28:52,860
so he says, be possessed of mindfulness and self-awareness, and he gives this list of

214
00:28:55,020 --> 00:28:59,660
comprehensive list of activities, meaning in all of our activities, and he says,

215
00:29:03,500 --> 00:29:10,780
when walking forward, walking back, when looking this way, turning your head looking,

216
00:29:10,780 --> 00:29:21,500
when flexing and extending your arms, and carrying your robes and your bull,

217
00:29:22,700 --> 00:29:26,860
or whatever you carry, when eating, when drinking,

218
00:29:28,860 --> 00:29:33,660
when tasting, when swallowing, when you're innating and defecating,

219
00:29:34,620 --> 00:29:39,740
something that doesn't get mentioned, I think I mentioned it quite often, or relatively often,

220
00:29:39,740 --> 00:29:45,900
otherwise you don't hear it that often, you're innating and defecating, or to be mindful and

221
00:29:45,900 --> 00:29:55,660
self-aware, meaning that mindfulness, how far in everything we do, even you're innating and defecating.

222
00:30:03,180 --> 00:30:07,900
So this means when you bend, say bending, when you flex, flex, when you reach, reaching,

223
00:30:07,900 --> 00:30:13,820
when you lift, lifting, when you brush your teeth, brushing, brushing, when you chew your food,

224
00:30:14,780 --> 00:30:17,340
doing, chewing, when you swallow, swallowing,

225
00:30:20,540 --> 00:30:24,300
when you're year-old, well, I'll be with you, feeling there's going to be a lot of feelings in

226
00:30:24,300 --> 00:30:33,660
within this, walking, standing, sitting, lying, falling asleep, waking up,

227
00:30:33,660 --> 00:30:40,300
walking and keeping silent, and everything you do, even in talking, when talking, you can be

228
00:30:40,300 --> 00:30:45,100
mindful of the movements of your lips. We think that when we're talking, our mind is

229
00:30:45,100 --> 00:30:49,500
preoccupied, but it's really not, the mind is involved there, but we're so good at it,

230
00:30:50,460 --> 00:30:55,420
that we launch the words out of the mouth, and meanwhile our mind can be focused on our lips.

231
00:30:55,420 --> 00:31:03,500
It's interesting, but it's because you see how, oh, it's not really me talking, it's my body,

232
00:31:04,540 --> 00:31:06,540
it's in the body, which is also not mine.

233
00:31:12,300 --> 00:31:16,780
So how many do we have now? We've got a bunch of steps about the count, we've got the next step.

234
00:31:18,460 --> 00:31:24,300
The next step is having been one possessed of mindfulness, I mean this is really the core of our

235
00:31:24,300 --> 00:31:31,180
practice, and particularly when you're not just entertaining, but the core is to be mindful,

236
00:31:31,180 --> 00:31:37,100
I'm dictating, not meditating, to be mindful, in whatever you do, watching the stomach that's

237
00:31:37,100 --> 00:31:44,860
being mindful, as you're rising, when you say falling, so on. But the next step he says is

238
00:31:44,860 --> 00:31:54,540
seclusion, going to a secluded place, so for months, well, if I go to the forest, a tree,

239
00:31:54,540 --> 00:32:02,060
root of a tree, some big trees, he can sit at the root, a mountain, a ravine, a cave, a

240
00:32:02,060 --> 00:32:05,980
charnold round, a charnold round is where they would just throw dead bodies,

241
00:32:05,980 --> 00:32:17,180
could place to develop, dispassion, to develop a sense of urgency, seeing all the dead people,

242
00:32:17,180 --> 00:32:19,340
and realizing this body is going to be next.

243
00:32:23,180 --> 00:32:28,220
A jungle thicket, an open space, or a heap of straw, anywhere that's occluded, your room,

244
00:32:28,220 --> 00:32:33,420
if you've got a bedroom to get the bed out, to make it a meditation room and sleep on the floor,

245
00:32:33,420 --> 00:32:40,940
then you've got a great space for meditation. You don't need a heap of straw, or if it will do fine.

246
00:32:45,740 --> 00:32:49,500
I mean, this goes without saying, that's occlusion is very important because of the

247
00:32:50,460 --> 00:33:02,700
difficulty in being mindful among society in an urban setting, or a society with people,

248
00:33:02,700 --> 00:33:06,700
but also society of objects, just being surrounded by such,

249
00:33:10,780 --> 00:33:13,740
all of the sights and sounds and smells and so on.

250
00:33:14,780 --> 00:33:20,060
Go into occlusion because your mind's much clearer, everything is much simpler,

251
00:33:20,940 --> 00:33:23,500
and so learning how the mind works is going to be much easier.

252
00:33:23,500 --> 00:33:29,900
But that's occlusion. After occlusion,

253
00:33:31,580 --> 00:33:35,900
there's a keeping in mind where all the other steps are still in effect. You haven't stopped.

254
00:33:35,900 --> 00:33:39,900
One thing I should point that out is that you can't say, okay, I've kept

255
00:33:40,780 --> 00:33:44,940
moral precepts. Now I'm going to do step two and forget about step one. It's not like that.

256
00:33:44,940 --> 00:33:50,380
These are cumulative. So all the other steps, they're keeping the precepts, the arting the senses,

257
00:33:50,380 --> 00:33:59,180
the being mindful, moderation and eating, being mindful, all of that.

258
00:34:00,620 --> 00:34:06,700
These are all still in effect and becomes occluded. And as you're doing this, once you're

259
00:34:06,700 --> 00:34:11,980
secluded, then you can really focus your attention on the next step, which is overcome the hindrance.

260
00:34:11,980 --> 00:34:22,460
Liking, disliking, drowsiness, distraction, doubt. Start working on the mind. Start looking at

261
00:34:22,460 --> 00:34:29,500
the negative habits, the things that cause us problems. Keep us from finding these. Keep us from

262
00:34:29,500 --> 00:34:42,700
seeing clearly. Keep us from being free from suffering. The process of meditation will do this,

263
00:34:42,700 --> 00:34:47,820
but it's important also to address them directly. If you like something, say,

264
00:34:49,260 --> 00:34:55,420
say to yourself, liking, liking. If you dislike something, dislike English, liking, either drowsies,

265
00:34:55,420 --> 00:35:01,180
eating drowsies, eating drowsies, you're distracted, thinking a lot distracted, distracted.

266
00:35:01,980 --> 00:35:06,540
If you have doubt, you're uncertainty, doubting, doubting. All of these take away from the

267
00:35:06,540 --> 00:35:12,300
strength of the mind, they take away from our presence, and so that it's important to be

268
00:35:12,940 --> 00:35:19,580
to deal with them, to overcome them. Once you overcome them, you find that your mind settles down,

269
00:35:19,580 --> 00:35:25,100
and the Buddha says you enter into things called jhana, which can be interpreted in different ways,

270
00:35:25,100 --> 00:35:31,420
and certainly are by every different teacher who talks about them, but it means stages of depth,

271
00:35:31,420 --> 00:35:39,180
of practice. So just say your mind becomes more clear, and become more mindful. You have in the end,

272
00:35:39,180 --> 00:35:45,100
mindfulness, due to equanimity, and Buddha says here. So that is the really the pinnacle. When you get

273
00:35:45,100 --> 00:35:51,660
to the point when you have true mindfulness, due to equanimity, you've seen, you've looked through

274
00:35:51,660 --> 00:35:59,260
everything, gone through it all, to the point where there's nothing better than anything else.

275
00:35:59,820 --> 00:36:07,020
It's not an intellectual realization, it's really a visceral realization that you've just done

276
00:36:07,020 --> 00:36:17,020
so much of observing through being mindful, that you see no other, nothing of any use besides

277
00:36:17,020 --> 00:36:24,220
being present mindful. And so that's the highest thing. And he says, this is my instruction,

278
00:36:24,220 --> 00:36:31,180
because when you get to that point, the next step is enlightenment. Once your equanimity is

279
00:36:31,180 --> 00:36:38,780
and mindful, and really observing everything, just rising and ceasing as it is, that when the

280
00:36:38,780 --> 00:36:46,460
mind becomes receptive to letting go. The next step, of course, is enlightenment. So he says,

281
00:36:46,460 --> 00:36:50,940
this is where we get to. This is what I teach people. This is gradual training.

282
00:36:50,940 --> 00:37:14,860
So when you teach this way, when you give them this right, this gradual practice, they all become

283
00:37:14,860 --> 00:37:20,860
enlightened, or to someone, to someone, to the alatini bhana, freedom from suffering, or to some

284
00:37:20,860 --> 00:37:27,740
not attain it. And the Buddha says, what do you think he says? He says, when they are thus advised

285
00:37:27,740 --> 00:37:37,900
and instructed by me, sematini bhana, the ultimate goal, and some do not. He says, well,

286
00:37:37,900 --> 00:37:46,300
you say this path leads to nibhana. You are here guiding people to nibhana. Why is it that

287
00:37:46,300 --> 00:37:54,060
some of them don't make it? And the Buddha says, I mean, it's not a very difficult question,

288
00:37:54,060 --> 00:38:02,220
I don't think if you consider it carefully. He says, here we are in Sawati, which is really in

289
00:38:02,220 --> 00:38:12,220
the north of India, the center north. And suppose someone wanted to go Rajaka, which is a little

290
00:38:12,220 --> 00:38:21,500
further east, and someone came to you and said, hey, how do you get to Rajaka? I need to

291
00:38:21,500 --> 00:38:27,820
told him, well, this road goes to Rajaka, follow it while you get to a village, go to the

292
00:38:27,820 --> 00:38:33,580
further, you'll see this town, go this way, you'll see Rajaka. Finally, you'll get to Rajaka,

293
00:38:34,220 --> 00:38:40,700
with its lovely park, scrubs, meadows and ponds. Rajaka is a lovely city, still is lovely,

294
00:38:42,060 --> 00:38:44,780
a little bit different in the time of the Buddha now.

295
00:38:44,780 --> 00:39:00,380
And then you told them all this, sent them on his way and went the wrong way. He went

296
00:39:00,380 --> 00:39:08,060
west instead, and given the directions, but he didn't listen to him, went the wrong way.

297
00:39:08,060 --> 00:39:15,340
And then a second person came and you told them the same thing, and then they did follow the road

298
00:39:15,340 --> 00:39:23,820
and became to Rajaka. Now, would you blame the guy or would you wonder what is the reason?

299
00:39:23,820 --> 00:39:29,980
I said, no, there's no reason. How could I say about that? I'm just the guy.

300
00:39:29,980 --> 00:39:39,100
And the Buddha said, yes, same is for practice. Mānā exists in the past, and Mānā exists,

301
00:39:39,100 --> 00:39:45,820
and I am present as the guy, yet someone become enlightened and someone do not.

302
00:39:46,940 --> 00:39:56,540
And then he says, Mānākā yi tātā, I am one who explains the path. That tātātā is the one

303
00:39:56,540 --> 00:40:03,980
who explains, describes, or details, what is the path.

304
00:40:10,060 --> 00:40:15,820
This is important. You can't rely upon, it's a simple teaching, you can't rely upon a Buddha,

305
00:40:15,820 --> 00:40:23,340
you can't lie upon Buddhism to save you. You can't rely on your meditation even, you can't,

306
00:40:23,340 --> 00:40:29,820
okay, here I am meditating, it's going to work. It's not just about meditating, it's about you now,

307
00:40:30,940 --> 00:40:36,620
your state of mind at every moment, what you are doing now, because that now is when we do work,

308
00:40:38,140 --> 00:40:48,380
when we act, and so it is you who have to do the work, so you who have to follow the path.

309
00:40:48,380 --> 00:40:56,780
And then kind of Mogulana praises the Buddha and his disciples, and that's really the end of the

310
00:40:56,780 --> 00:41:02,860
Buddha. So that's the metaphor tonight, another description to the path, I think that's quite useful,

311
00:41:03,580 --> 00:41:07,660
something to keep in mind. Some of these things should be familiar, but

312
00:41:10,140 --> 00:41:14,220
certain always good to go over the things that are important. It gives you a quick picture,

313
00:41:14,220 --> 00:41:19,660
really in a way of what is Buddhism, if you ask somebody to ask you what is Buddhism,

314
00:41:20,300 --> 00:41:26,700
that fairly simple description there of the gradual practice is as good a description as any,

315
00:41:27,660 --> 00:41:33,820
it doesn't have everything in it, but it's very practical. So that's the number for tonight,

316
00:41:33,820 --> 00:41:49,740
thank you all for tuning in, wish all the best.

