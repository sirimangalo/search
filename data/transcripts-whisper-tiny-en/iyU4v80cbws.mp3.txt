Going through the stages of knowledge, progressing in the course, there is really less that I need to say.
Today, I'm going to ask to talk about Marl, giving a short talk of this.
So the question we should ask is that in this world, it's so much beauty,
and so much happiness and joy.
So much that is pleasant and positive.
Take, for example, this place that we are even.
I've been here many days now.
It's hard to find fault from the trees and sun,
but the cold maybe for some.
But without this goodness, it's such a peaceful, pleasant world.
Why is there suffering?
Why is it a challenge for us to be in a place like this?
If in just to be here, it's a bit of a challenge.
Why is it a challenge to walk back and forth?
Why is it a challenge to sit still?
Everybody has seen, you're not envisioned, the meditator of the milk, the spiritual practitioner of the mountain.
It just seems like such a peaceful life.
Why is it a peaceful?
Because really there's nothing wrong with the world most of the time.
Barring natural disasters, catastrophes, accidents, it seems that we tend to think that life should be quite peaceful.
Why is it so hard sometimes?
We could ask more generally why is there suffering in the world at all?
Why do we suffer from anything?
Why can't we live our life in peace?
Why is the world in such a such a world that we have to experience something?
Sometimes great something, anguish, lamentation, sometimes despair, depression, anxiety, crippling panic attacks, crippling illness.
Why?
Why is the world like this?
It's probably, I think, one of the best answers to St. Mara.
Mara is one of the best answers to the lion.
It's such a nice, wonderful world.
There is still suffering.
Because it sort of encompasses all that's bad in the world.
Mara is a translation translated as evil, or malevolence, something that inflicts suffering.
It's a kind of a scapegoat like in Christianity that talk about safety.
See, it's kind of a scapegoat.
If you, I don't know if Christians actually do this, but I think it's sort of stereotypical
that if you do something evil, naughty, sinful, say anything different.
When you could look at it, at the same way Buddhism, Mara is pretty equivalent to saying.
I think the distinction is what is the most focused on the solution, right?
We're not complacent in the ways that some other religions might think we're not putting
our faith in an external source that will save us in spite of our Mara.
In some ways Mara is our adversity, our answer, our adversary.
It's in some ways a war, a battle, a war, I'd say, because it's made up of any smaller
battles.
It's our war with Mara.
The imagery of the Buddha on the seat of enlightenment, it was Mara who we had to fight
with all the armies of Mara.
So it might seem a little bit simplistic to blame it all.
One guy, Satan or Mara.
But Buddhism has five morals.
Buddhism doesn't stop at just the guy.
In Buddhism there are five kinds of morals that just encompass everything that's bad, really.
Everything that reflects suffering.
So I'll talk about those today.
The first is Khanda Mara.
This is the five aggregates.
The five aggregates are evil.
Basically what we're seeing, I mean, a more nuanced way of saying is that they're unable
to satisfy or probably banners, a clause for suffering if you click into them.
When calling to these five things are the truth of suffering.
These five things are suffering.
It's stressful.
I mean, in general, they're pretty unreliable.
Khanda will depend upon their five aggregates.
Not in a ways that we think we can.
So Rupa and I, it's Mara because it's not dependable.
It can depend on the body to work the way you want.
The body is full of all kinds of illness.
From the moment we're born, if some babies can sink or are born with debilitating illnesses.
But we all face the challenge of dealing with Rupa dealing with the physical.
Coronavirus is a very good example of this, though.
Here, but some people who's long, have something described by it.
I don't know.
Some people might think it's almost a big conspiracy, and that's not actually true.
I don't know.
I don't know anybody.
And you can't tell me that illness doesn't exist cancer.
You've ever seen someone who's just been crippled by cancer.
I met one of my students from ten, twelve years ago in California.
I met her again.
I didn't even recognize her.
She was withered in a wheelchair.
Apparently, it wasn't cancer.
Some muscular dystrophy are not actually sure what sickness was.
And she came to see me and I didn't recognize her, but I gave her a blessing.
It was so long ago.
And many students, I didn't remember her.
And she remembered me.
She couldn't talk, held her hands up and respect.
I saw her in two titles, and then she passed away while I was there.
There were 15 days I just happened to be there when she was under death, and she was able to.
Come to meet me before she passed away, which was this.
That's just kandamara.
One of our teachers in it, and she has conditioned to talk about this a lot.
We all face sickness on age, or eyes don't work, or ears don't work.
We get ulcers, heart disease, diarrhea, constipation, kandamara.
The upshot of it is simply, as I said, you can either try to fix all of this, and avoid all the unpleasantness.
Which is sort of what we're taught to try to do.
Or you can learn a way to face it, and to rise above it, and to be independent of the condoms.
It's a problem if you decide that I like root, but I like bottom.
I'm just going to do what I can to make it good.
That's a problem.
The problem is that you like that.
You become dependent on that.
And when it changes, the meaning of root bound, they use the word root bound, because
root bound means to change, to alter, to be subject to alteration.
The root bound changes.
And to be very, very upset when it changes.
We do not understand that we are very much attached to happiness, pleasure, even calm states.
But we do not know if the word stop, we do not know if the word all our pain is painful feelings.
If you want pleasure, both feelings are calm feelings, and be very upset when painful feelings come up.
When restlessness comes, when you're not calm,
if you're calm, you can't handle not being calm, if you're like happy,
happiness or pleasure, you can't stand when you have displeasure or pain.
And it becomes worse and worse when you come addicted to pleasure.
And let the drug happen.
So we have two choices.
We can continue to cling to the pleasant side and try to eat out as much as we can.
We are saying that we are one of those people who has to meet with great crippling pain in our lives.
I don't know if any of such people really exist.
Or we can find a better way to become independent, independent of feelings.
So that we still feel happy, we still have a pain.
But our peace and our happiness doesn't depend on the pleasure and pain in the common way of feeling.
Some of you have a third one recognition or memory.
This is sort of an indirect problem though.
We have problems both with not being able to remember and problems with having to remember.
It is a great source of stress and torture to have traumatic memories of the past come up again and again.
Recognition if something can trigger a memory, a sound, a sight,
or even just simple one seems to be a spiner, freaks you out, someone who is afraid of spiders.
I remember in Sri Lanka telling this big guy, taking him to his room.
He said, okay, this is your room, there's your bed and there's a spiner about this big on the parcel.
There's a spider on the floor, you might want to get that out and then I showed him and I said, okay, I'll see you later.
I went in the room but let alone that it came knocking on my throat.
Can you get this spiner?
Freaking it in front of a spider.
The sun is the recognition or the perception of something as something.
If you see a spider, it's actually to see that once you recognize it's a spider, it triggers a fear.
The much more horrible ones are past memories or the future being reminded again and again of things that you have to do in the future.
Exciting, having to give it down the top, for example.
I had to give down the tops in front of hundreds of people and when I was a young monk,
I'd be quite a thing to have to remember all I had to give them.
Talk in front of the most city tonight or something.
I wanted to hear the fore and long speak time.
Sunya, Sunya, I think.
It's one of the really important lessons that can be so valuable because it's pretty quick to see fruit.
It's the ability to separate the Sunya from the trigger reaction when you are able to remember something and have it just be a thought.
When you are able to see or hear something and have it only just be seen from here or smelling or tasting, feeling.
When past memories don't talk to you because they're just memories.
Sunya, Sunya, Sunya, so many different things, the fourth one.
Sunya is where the truth of it is.
But here I think it's simply just referring to Sunya, let's say it's habits.
Some power can be judgment, it can be things like racism, bigotry, xenophobia.
I say this because sometimes they can be ingrained where you don't want to be that, but you are something.
It's things like they call male privilege.
You are realizing how you oppress women just by having society or men who are considered superior.
Those kinds of things.
White privilege and so on.
Not realizing that things are...
Not realizing that you have ingrained prejudices that oppress others and so on.
It's just sometimes our perceptions of things can be skewed and are skewed over a lifetime of culturalization.
Sunya can be evil because Sunya means formation.
So you develop these formations and as many of you are seeing they're ingrained, you can't just turn them off.
You can't stop getting angry, you can't stop liking or wanting things.
The answer is not to fix these things, it's to see through them.
And again, the understanding that prevents them from arising in the first place.
Their habits takes work.
So Sunya is evil and he says, oh, this is where you see how evil you are.
Sometimes it's a meditative thing, I'm so evil.
I see all the more I've decided.
Don't let it discourage you.
This is actually a good thing to see in.
The first step of becoming a good person is realizing that you have things that make you an outcome person.
But it's said to that extent, the person is wise.
But the person who thinks they're good with things they're better than they are.
That's where the real is scary.
Scaryness comes from things that their evil is good.
And when Yayana, Vinyana, Kanda, this is the hard one, I think.
It's hard for us to think of Vinyana as being evil.
This is scary.
This is what keeps people from wanting to experience Nipana.
So I think it's hard to talk about how Vinyana might be evil.
And we just say that the main sort of theoretical, the true problem with Vinyana is that your force to experience all these things.
You can't control them and say, hey, Vinyana will go to Vinyana.
Vinyana will see.
Let my Vinyana be the best that my Vinyana cannot be best.
So in other words, I only want to experience good things.
I don't want to experience bad things.
Vinyana is consciousness of the awareness.
The fact that we're even aware is a problem because we have to be aware.
Things we like and things we don't like equally.
They can't control them.
But ultimately, there is something deeper truth there that is even a cessation of Vinyana.
It's a very peaceful experience.
Someone who has experienced that is really hard to talk about because the Vinyana sees writers not even an awareness at that moment.
But there is true peace.
And I don't want to try to convince you of that.
I made it for a few experiences and you can say for yourself whether it was a good thing to experience.
But these are the truth of the evils involved with the kindness that there is evil involved with them.
And really, ultimately, it's their own evil if you claim to them.
If you don't mind the fact that you experience pain and so on,
then if you've come to the point where you're independent of it, you're not reactionary towards it,
then they have no power over you.
And you see told Joviyarati, dwelling independent.
So that's the first one for more to go.
The second one is Kile Samara, or this one's easy.
But it's something that I haven't made me out on to everyone.
So just to remind you, there are only three Kile Samara defendants that were served with.
And if you really want to know whether you're progressing,
it's not actually precisely about how calm you feel or how quiet your mind is.
It's about the change in greed, anger, and delusion.
If you're seeing over time, and it can be hard during the course,
but over the years that you practice,
the progress that you see is that you have less anger and less delusion.
That's really the only correct answer.
If you have less greed, less anger, less delusion,
then you're progressing with them.
So greed includes any kind of like king or wanting,
anything that makes you more dependent on something.
This is considered a development because of how it pulls you,
and keeps you dependent and creates triggers for stress and suffering
when you don't get when you were.
And the course is in boredom, sadness, fear, depression,
frustration, disliking, aversion.
All of these are directly painful.
It's not pleasant to experience them.
It's not a cause for happiness, for yourself,
for others. It causes you to lash out and hurt others.
It creates all sorts of problems in life.
And delusion, the living is the worst.
I say about greed that greed isn't highly blamed in the world.
People don't blame you so much for being greedy.
Not generally speaking, it's not easy to see the fault of it.
Why? Because it's pleasant.
Greed can be very pleasant, so it seems like you like this.
Good, I like this too.
Like anything is what we do.
But it's slow to change.
It's a hard one to change.
Credit even greed is slow to change.
You can't easily free yourself from your desire for things
so that you can stop hard easily and have a hard time
freeing yourself from it.
And so it spreads and it's very sticky.
Anger is highly blamed.
It's a cause for a lot of strife.
When you get angry at someone, they tend to get angry back
and it causes trouble.
No one likes someone who's angry,
or if they knew it just makes them angry as well,
they can go and fight together or against each other.
But it's quick to change.
Anger is painful.
It's not hard to become aware of how bad it is.
And so it's quicker to change than greed.
So these two have their wonders,
their better and worse.
Greed is worse because it's slow to change.
Anger is worse because it's, I think,
they need to cause lots of problems.
They both cause lots of problems, but delusion is highly blamed
and slow to change.
So it's the worst.
Furthermore, delusion is what gives rise to greed.
Anger, you can't have greed or anger without delusion.
Delusion includes ignorance.
Misunderstanding, conceit, wrong views.
Even just adherence to views
when you hold on to something as a view
instead of being open to experience reality.
It can be hard to experience reality
if you have right view, if you're clinging to the view.
And I already know this.
If you already know everything,
it's hard to learn anything.
It's hard to have an open mind.
Delusion, conceit, arrogance, stubbornness,
and so even worry, anxiety is a kind of delusion.
Anything that is a distortion of the reality,
twisting of the nature of things.
Something that obscures reality in some way,
through ignorance or through darkness,
the darkness of delusion.
So these are more, this is the most precise
and the mean that we're faced with.
Because honestly, the aggregates are not our enemy.
Again, it's only because we cling to them
and the clinging has to do with greed, anger and delusion.
So our real enemy is really just these three.
You know, the ones that we're vigilant to try to see,
focus our attention on.
That helps, it helps to know that, I think.
It will help you to focus your attention
and your efforts.
And to be encouraged as you see these decrees
to know what's wrong,
to be aware that everything else is not really something
to be concerned with.
The third mara is called a visan kara mara.
A visan kara mara is visan kara refers to karma,
actions.
Both the mental formation and the actual actions
that make performance seem sort of the outcome of our
defilements because of greed, anger and delusion.
We heard ourselves, we heard other people,
we do things that cause trauma.
The turmoil in the world is all because of people's
a visan kara, which in turn is because of the defilements.
But so much of the evil in the world is the actions.
If you're frustrated, you punch a wall up.
You haven't solved your problem.
You've created more problems.
But when you punch someone else,
you've created a real problem.
When you yell at someone,
when you manipulate others,
stealing from them, lying, cheating,
even taking drugs in alcohol,
the confusion that comes in the mind,
you've really created a problem for yourself.
If they're addictive drugs,
or if they're delusional,
you have drugs that mess up your mind.
You create problems for yourself or others.
People who drive a drone, for example.
But to me, I'll be some kara.
There's a lot of this in the world.
This is another mara.
The fourth is much more death.
I'm going to single that as a mara.
Much needs death.
Death is a mara in a way that anything else isn't
because it puts limit on everything.
Any argument you might make for another way
of solving your problems well.
I'm going to just cure illness,
solve moral peace.
The world eventually couldn't live
in peace and harmony,
but it'll still die.
Death is a certainty.
In regard to certainty or not,
it's certainly evil.
It's evil in that it puts in
what's a stop of all of our efforts,
any one of us may die tomorrow.
That would be the end when it was where we'd go.
If we're lucky, maybe we'd continue now.
I've been practised mindfulness.
There's something very fundamental about the practice
of mindfulness as a purity of mind
and the clarity of moments.
That even if you forget all of us,
even if you do lose track of Buddhism and meditation,
there's still some quality of mind that it makes it very easy
for you to pick it up to future lives.
It's why some people in this life are able to practice quite easily
and they're not able to practice very easily at all.
So it's a great encouragement
that by doing something so fundamental to the quality of mind,
we are bettering ourselves not only in this life,
but in the next.
For someone who hasn't practiced mindfulness
it's such a scary thing.
It's hard to know where you're going to go
when you're constantly deaf.
It's a very traumatic experience for many people.
It's a sobering reality
that it's something to put a recommended we reflect upon often
because it reminds us that all these things we might be doing
in the world,
they all mean nothing in the face of death.
Everyone in this world will be dead,
probably most likely within a hundred years.
It's all of the all of our missions in the world.
It's a very useful lesson to think of.
I hope so too, remind you that we can't take anything with us
when we die.
But we carry with us our qualities of mind.
Go to the fourth or more.
The fifth Mara is deal of the love.
And this is the guide.
Or the people,
it's apparently a classification of angels that
delight in the creations of others.
There are demons who apparently encourage humans
or want to encourage humans with other beings
to make a mess of things, to create.
They probably delight in war.
They delight in capitalism.
They probably delight in technology.
They delight in wildfire.
Things that delight in creation.
So some of them might not even be all that evil,
but they are unique adversary in Buddhism
in a way that they wouldn't be for maybe other religions
or certainly not for secular people.
When they encourage a mission,
they encourage the late life,
marriage, kids,
a mission of all sorts, creation of all sorts.
And so the Buddha posed a unique problem
and a unique source of anxiety for these angels,
because he taught people to let go,
to give up, to be content.
Contentment is antithetical to the source of angels.
The people as well,
many people are horrified to think that Buddhists
give up world-new pleasures.
My parents were horrified that I stopped playing guitar
of all things.
And this is such a sad day to hear that
there was no moment when it played guitar.
This is, and people are even upset to know that
no longer going to create,
and no longer going to cultivate pleasure.
And so the Buddha called these people
these beings more.
It's annoying who you are, who you are, who you are.
When in fact, it seems that they were just like everybody else.
Mountain pleasure, delight in creation.
It points out the unique quality of Buddhism
as not just about being a good person,
because these angels were okay.
They weren't trying to make you suffer.
They were just keen that you should make things.
And so they said to the Buddha,
stay as a lay person,
or the body said to stay as a lay person.
You can do all some things as a lay person.
You can do lots of good as a lay person.
And the body said, I know you are.
And many monks as well.
And Mark came to equinee once and said,
what would a woman know about Muslim?
To be true against,
maybe he was just a misogynistic Eleanor,
but he said, what would a woman know with some people?
She said, what a woman is there in a Muslim?
What does a woman, what does a woman have to do with a Muslim?
There's no gender and truth.
I think Mark would try to,
very much trying these angels,
very much trying to stop people like us.
So you can sometimes get a sentence that Mark might be playing games with us.
I wouldn't have put too much weight on this.
It could make you very paranoid.
But it is sometimes useful to think of the strange things
that have been to your life as more.
And the best way to do that is to just remember there are five.
But if during our meditation course something terrible happens in the center,
it could be any of the five.
Well, it doesn't necessarily mean that there are angels up to get us.
Luckily, we have many dealers who appreciate what we do.
The many Buddhists up in heaven and here on earth,
many dealers who look after Buddhists,
they themselves have listened to the Buddhist teaching.
So we do have some protection.
But we don't have to look to the end.
The devil wants to know that the world is full of Mark.
And Mark can take the shape of people, damas,
or just the circumstances of our universe.
Sometimes the results of our past karma,
showing up in the form of illness or catastrophe or difficulty.
It's an important lesson to not try to not be afraid of things like illness,
like COVID, for example,
or afraid of accidents or afraid of tragedy.
It's important to understand that you're familiar with the reality of these things
that we're not caught up with.
That we're not even caught up with.
But the same meditation is one of the one thing it is,
is the preparation for death.
Not that we spent a lot of time thinking about it,
but that may become someone who can say we are prepared for death
in a way that most people are not.
That death is not something that scares us.
Because we become independent of our experiences.
We have understood that the nature of reality
that we understand that death can be no different
than what we experience here.
It's more so here you're smelling taste of it than thinking.
The ordinary person is dependent on experiences being a certain way
because they haven't come to terms with what might be.
And that's what you're going through in meditation.
You're experiencing things that you normally are not comfortable with
or not able to face.
And meditation is about expanding the realm of what you can face.
You see, so you're in a laboratory here.
There's no danger.
You have all the time that you need to investigate
and to look into these things that you are normally so afraid to face
and to gain this ability to face them.
To the point that nothing scares you.
The reactions are not a part of your experience.
And do well independent and at least happy without an object
without needing something to trigger the happiness.
And then you can enjoy the beautiful world.
There's really nothing wrong with it as long as you don't cling to it.
So that's my teaching of Mara.
That's the talk for today.
Have a good day, everyone.
Thank you.
