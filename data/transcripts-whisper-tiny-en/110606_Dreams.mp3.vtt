WEBVTT

00:00.000 --> 00:03.200
Hello, welcome back to Ask a Monk.

00:03.200 --> 00:05.540
Today I will be answering the question as to whether

00:05.540 --> 00:12.280
greens have karmic potency, so whether they have the ability

00:12.280 --> 00:18.480
to give rise to ethical states, do they have ethical

00:18.480 --> 00:22.080
implications, in the sense that can they affect our lives

00:22.080 --> 00:24.160
and can they affect the world around us?

00:24.160 --> 00:27.960
Is it possible to perform Buddhist karma,

00:27.960 --> 00:31.480
to make bad karma while you're asleep?

00:31.480 --> 00:37.080
And the answer I think is quite clearly yes.

00:37.080 --> 00:40.920
And it's important that we understand what we mean by karma.

00:40.920 --> 00:44.160
And this sort of helps us to understand what we mean by

00:44.160 --> 00:46.520
karma in Buddhism.

00:46.520 --> 00:49.480
Karma isn't an action that you do with the body or speech.

00:49.480 --> 00:51.720
It's something that arises in the mind.

00:51.720 --> 00:55.000
When you intend to do something that's karma, so the only

00:55.000 --> 00:57.840
reason to say killing is wrong or stealing is wrong or

00:57.840 --> 01:01.120
it's become bad karma is because of the effect that such

01:01.120 --> 01:05.560
things have on our mind and the mental aspect of the act

01:05.560 --> 01:10.760
that your mind is building up the tendency to be angry

01:10.760 --> 01:14.360
or greedy or corrupt.

01:14.360 --> 01:23.880
So in that sense, because dreams are tend to be ethically charged.

01:23.880 --> 01:27.400
So we give rise to the same states in our mind of greed

01:27.400 --> 01:29.320
and anger and delusion.

01:29.320 --> 01:32.920
They can all arise in our mind quite clearly there are ethical

01:32.920 --> 01:33.720
implications.

01:33.720 --> 01:36.520
Now, I want to talk about a few things here.

01:36.520 --> 01:41.240
So I'm going to try to keep them in a clear order.

01:41.240 --> 01:46.120
The first thing I should talk about is what we understand dreams

01:46.120 --> 01:47.600
to be in Buddhism.

01:47.600 --> 01:48.680
So what are we talking about?

01:52.040 --> 01:57.000
We have a description of the different kinds of dreams in one

01:57.000 --> 02:00.120
of the old Buddhist texts, the questions of King Melinda, the

02:00.120 --> 02:02.400
Melinda Panha.

02:02.400 --> 02:06.600
And that text is mentioned that there are six types of dreams.

02:06.600 --> 02:09.880
And so this will help us to understand what exactly

02:09.880 --> 02:14.960
mean by dreams and exactly how relevant they are to our

02:14.960 --> 02:18.120
practice and so on.

02:18.120 --> 02:20.720
Because I also want to talk about how we should approach dreams

02:20.720 --> 02:24.000
and what it means in terms of our practice.

02:24.000 --> 02:27.000
So there are six kinds.

02:27.000 --> 02:29.640
The first three are physical, wind, bile,

02:29.640 --> 02:30.760
flam.

02:30.760 --> 02:35.680
And this is just based on the medical terminology that they

02:35.680 --> 02:39.400
would use in the time of the Buddha or in ancient India.

02:39.400 --> 02:42.320
So basically, it means a disruption in the body.

02:42.320 --> 02:47.600
There's something maybe you eat bad food or maybe your body

02:47.600 --> 02:49.400
is out of order, you have sickness or so.

02:49.400 --> 02:53.880
And that can easily give rise to dreams.

02:53.880 --> 03:00.120
And the other three are some external influence, which

03:00.120 --> 03:01.680
could be a person around you.

03:01.680 --> 03:03.000
It could be spirits.

03:03.000 --> 03:05.680
It could be angels and I mean, I think what they're

03:05.680 --> 03:06.680
talking about is angels.

03:06.680 --> 03:09.640
But actually, if you think about it, we don't even have to

03:09.640 --> 03:10.960
go so far.

03:10.960 --> 03:13.200
If someone starts whispering in your ear where you're sleeping

03:13.200 --> 03:16.360
or if you hear noises, if you've ever had something

03:16.360 --> 03:19.960
going on around you and you start dreaming about something

03:19.960 --> 03:23.320
based on the sound that you hear or if it gets hot,

03:23.320 --> 03:27.280
that can affect your state of mind and so on,

03:27.280 --> 03:29.320
even cause the degree.

03:29.320 --> 03:33.840
The fifth is something from the past and the sixth is

03:33.840 --> 03:36.280
something from the future.

03:36.280 --> 03:38.080
This is where it gets interesting.

03:38.080 --> 03:44.320
So some people have visions of things in their dreams

03:44.320 --> 03:47.240
can be from the past in this life.

03:47.240 --> 03:49.400
Sometimes people are suspicious that it's something

03:49.400 --> 03:51.600
from their past lives.

03:51.600 --> 03:54.480
But let's just say something in the past in this life,

03:54.480 --> 03:57.320
because that's really obvious memories that we've had

03:57.320 --> 04:01.480
things that we keep in our minds can cause us to dream.

04:01.480 --> 04:07.040
But the sixth one is considered to be important.

04:07.040 --> 04:11.520
And it's worth noting that only the sixth is

04:11.520 --> 04:15.040
considered to be important in terms of actually paying

04:15.040 --> 04:18.120
some kind of attention to it.

04:18.120 --> 04:21.320
The sixth kind is something from the future.

04:21.320 --> 04:23.600
And some people may not even believe this,

04:23.600 --> 04:27.040
but other people will be emphatic that this sort of thing

04:27.040 --> 04:29.440
happens to them, where they'll dream something

04:29.440 --> 04:31.480
and then it happens.

04:31.480 --> 04:34.360
Suddenly, it comes in their life.

04:34.360 --> 04:37.040
People can even have this in meditation.

04:37.040 --> 04:40.280
But it's possible that something from the future

04:40.280 --> 04:43.560
can cause a dream, something that's about to happen.

04:43.560 --> 04:45.880
And you can understand this in different ways.

04:45.880 --> 04:49.360
It could be simply because of the power of what's

04:49.360 --> 04:53.000
going to happen, because we consider

04:53.000 --> 04:57.320
that the universe is much more than we can see

04:57.320 --> 04:59.560
and experience normally.

04:59.560 --> 05:03.080
So there are a lot of forces that are at work, potentially.

05:03.080 --> 05:08.880
And those forces can be experienced if you know about how

05:08.880 --> 05:10.760
animals are able to predict the weather

05:10.760 --> 05:13.320
and are able to predict disasters.

05:13.320 --> 05:16.720
And like before the tsunami in Asia,

05:16.720 --> 05:20.680
they say that the elephants all ran in land,

05:20.680 --> 05:26.240
like they knew it was going to come, and so on.

05:26.240 --> 05:29.000
Just the forces of nature that we're not

05:29.000 --> 05:33.600
able to sense in our normal state.

05:33.600 --> 05:36.520
Because actually, when you're sleeping,

05:36.520 --> 05:39.680
let's talk a little bit about what exactly dreams are.

05:39.680 --> 05:41.720
So these are the six sources of dreams.

05:41.720 --> 05:43.320
But what's happening in the dream state

05:43.320 --> 05:46.280
is your mind is not asleep and is not awake.

05:46.280 --> 05:49.160
This is what we know in science as well.

05:49.160 --> 05:50.880
But from a meditative point of view,

05:50.880 --> 05:55.400
there's still a great amount of concentration,

05:55.400 --> 05:56.920
but there's no mindfulness.

05:56.920 --> 06:00.200
So you're not really in control, obviously,

06:00.200 --> 06:03.320
as for most of us, unless you train yourself,

06:03.320 --> 06:05.480
because there are actually people who train themselves

06:05.480 --> 06:06.440
in dreams.

06:06.440 --> 06:07.840
And I'm not going to teach you to do this.

06:07.840 --> 06:09.240
This isn't what I teach.

06:09.240 --> 06:12.760
I just want to help people to understand this

06:12.760 --> 06:15.840
so we can move on and continue with our meditation.

06:15.840 --> 06:17.360
Because this will interrupt your life

06:17.360 --> 06:19.400
when you're wondering, well, people do wonderful.

06:19.400 --> 06:23.200
What is the significance of dreams?

06:23.200 --> 06:25.000
Most dreams are not significant.

06:25.000 --> 06:26.520
Because as I said, you're in this state,

06:26.520 --> 06:28.600
and you can be easily influenced by things

06:28.600 --> 06:30.640
because you don't have the ability to judge,

06:30.640 --> 06:34.640
to discriminate, and to catch yourself.

06:34.640 --> 06:39.920
And so you easily slip into illusion and fantasy.

06:39.920 --> 06:43.360
But there are certain dreams that are affected by the future.

06:43.360 --> 06:45.920
And this is what leads people, I think,

06:45.920 --> 06:48.840
to this end the dreams about the past,

06:48.840 --> 06:51.080
which is what leads people to put significance in dreams.

06:51.080 --> 06:54.680
So they might experience a dream that they feel

06:54.680 --> 06:57.480
has some significance and that should be explored

06:57.480 --> 07:01.360
and has some meaning and importance based on the past

07:01.360 --> 07:03.920
and something they might be repressing and so on.

07:03.920 --> 07:07.600
Or they might feel that they see they have some dream

07:07.600 --> 07:11.640
and then it sort of comes true in the future.

07:11.640 --> 07:13.240
And so as a result, they feel that dreams

07:13.240 --> 07:16.880
have great significance and psychotherapists

07:16.880 --> 07:22.280
and Freud and Jung put great emphasis on dreams

07:22.280 --> 07:24.640
and the importance of them.

07:24.640 --> 07:26.800
I want to say that we shouldn't put great importance

07:26.800 --> 07:27.880
on our dreams.

07:27.880 --> 07:29.840
And this comes back to the idea of whether you

07:29.840 --> 07:30.800
can create karma.

07:30.800 --> 07:34.040
As I said, there are karmic states that arise.

07:34.040 --> 07:42.040
And so you are potentially creating more karma.

07:42.040 --> 07:44.880
But the point is that you can't really control that.

07:44.880 --> 07:47.400
You can't say, I'm not going to create karma.

07:47.400 --> 07:48.760
Even you can't say stop.

07:48.760 --> 07:52.560
You can't stop yourself from giving rise to fear or anger

07:52.560 --> 07:57.480
for nightmares or anger or attachment and lust and so on.

07:57.480 --> 08:02.040
So it's much more useful.

08:02.040 --> 08:04.520
What I think dreams are really useful for

08:04.520 --> 08:07.760
is for understanding our mind state.

08:07.760 --> 08:12.600
And we should never take the whole of the dream

08:12.600 --> 08:15.640
as somehow significant, even if it means something

08:15.640 --> 08:19.480
for the future or it has a potential to be a prediction

08:19.480 --> 08:26.280
or when you call them, for telling the future.

08:26.280 --> 08:29.960
Or even if it comes from some repressed state

08:29.960 --> 08:31.600
that we have in the past, the point

08:31.600 --> 08:35.040
is that you're in a state where the mind is not connected.

08:35.040 --> 08:38.840
It's not functioning in a logical manner.

08:38.840 --> 08:41.560
So it's going to make connections between things

08:41.560 --> 08:42.880
that otherwise don't have connections.

08:42.880 --> 08:46.040
So maybe part of it comes from the fact

08:46.040 --> 08:48.880
that you had some traumatic experience in the past.

08:48.880 --> 08:53.160
But part of it just comes from the chaos in the mind

08:53.160 --> 08:58.320
and the nature of the mind to dream and to imagine.

08:58.320 --> 09:00.720
Likewise, you might think of something in the future

09:00.720 --> 09:03.560
or you might have the ability to see what's

09:03.560 --> 09:06.760
going to happen in the future, but it becomes distorted.

09:06.760 --> 09:08.160
It is distorted by the mind.

09:08.160 --> 09:11.760
It's the same as when you go in a hallucinogenic drug

09:11.760 --> 09:14.800
and you might have visions of angels, deities,

09:14.800 --> 09:18.040
and all sorts of spiritual things.

09:18.040 --> 09:20.760
And so you think that this is the truth

09:20.760 --> 09:22.240
and something that you're seeing something

09:22.240 --> 09:23.240
that we can't normally see.

09:23.240 --> 09:29.120
But it's also the brain that is creating,

09:29.120 --> 09:31.200
is imagining, is hallucinating.

09:31.200 --> 09:33.520
So whether part of it is real and whether

09:33.520 --> 09:36.520
it is sending the mind off into a dimension that

09:36.520 --> 09:38.480
can see things that we otherwise couldn't see,

09:38.480 --> 09:40.720
it's also the brain.

09:40.720 --> 09:43.920
And it's also doing all sorts of crazy things.

09:43.920 --> 09:45.640
I think if you look carefully, you'll

09:45.640 --> 09:47.800
see that this is true with dreams and that they shouldn't

09:47.800 --> 09:50.240
be trusted.

09:50.240 --> 09:53.040
And also, I think in the sense of whether you can create

09:53.040 --> 09:55.920
karma or the karma that you're creating.

09:55.920 --> 10:00.560
Because for one thing, the karma is not

10:00.560 --> 10:02.680
going to be strong because you don't really

10:02.680 --> 10:05.280
intend to do this or that in your dreams.

10:05.280 --> 10:08.080
You just kind of get angry or worried or afraid

10:08.080 --> 10:12.640
in the case of nightmares or have lustful dreams.

10:12.640 --> 10:14.880
But you don't really have the strong intention

10:14.880 --> 10:17.120
because your mind is not clear.

10:17.120 --> 10:21.440
Your mind doesn't have the will or the awareness

10:21.440 --> 10:28.200
or the mindfulness, which is able to make decisions clearly.

10:28.200 --> 10:31.960
So it's going to be weak karma for one thing.

10:31.960 --> 10:38.680
And also, for another thing, there's many states

10:38.680 --> 10:39.640
that are mixed up.

10:39.640 --> 10:43.480
And as I said, connections can be made in an illogical manner.

10:43.480 --> 10:47.080
And therefore, you have all these crazy dreams.

10:47.080 --> 10:50.400
So what we should use dreams for is to be able to judge

10:50.400 --> 10:52.080
our mind state, because what's good about them

10:52.080 --> 10:53.520
is you don't have this control.

10:53.520 --> 10:55.200
You aren't covering things up.

10:55.200 --> 10:57.080
Because there's no control, you're

10:57.080 --> 11:00.560
able to see many things about yourself

11:00.560 --> 11:01.840
that you wouldn't otherwise see.

11:01.840 --> 11:04.880
And this isn't the content of the dream,

11:04.880 --> 11:06.680
but it's the quality of the dream.

11:06.680 --> 11:08.880
So if you have a dream that's based on anger,

11:08.880 --> 11:11.440
if you have a dream that's based on fear, nightmares,

11:11.440 --> 11:14.040
you have a dream that's based on lust, or so on,

11:14.040 --> 11:15.440
this shows you what's in your mind.

11:15.440 --> 11:17.560
And often, in a way that you otherwise

11:17.560 --> 11:18.440
wouldn't be able to see.

11:18.440 --> 11:21.240
So people who are otherwise calm and controlled

11:21.240 --> 11:23.200
might find themselves suddenly having nightmares.

11:23.200 --> 11:25.120
And so they think something is wrong.

11:25.120 --> 11:27.120
Well, the truth is, this is you're seeing something deep

11:27.120 --> 11:30.400
down that you're repressing, because we're only

11:30.400 --> 11:32.320
able to repress things in our daily life

11:32.320 --> 11:36.240
by force of will, by actually some sort of mindfulness,

11:36.240 --> 11:40.960
and the ability to choose not to follow,

11:40.960 --> 11:43.600
which we don't have when we're dreaming, when we're asleep.

11:43.600 --> 11:46.880
So in that sense, I would say that really the only thing

11:46.880 --> 11:49.080
that we should do with dreams is use them

11:49.080 --> 11:51.360
to see where we have work to do, and to help us

11:51.360 --> 11:53.400
to understand how our mind works.

11:53.400 --> 11:56.000
Don't take the content of the dream as important.

11:56.000 --> 11:59.560
This is a big mistake, because you don't know

11:59.560 --> 12:00.440
where it's coming from.

12:00.440 --> 12:03.080
It could be coming simply because you had bad food.

12:03.080 --> 12:05.520
It could be coming from the way your body, the way

12:05.520 --> 12:07.440
your brain is functioning, the chemicals,

12:07.440 --> 12:10.560
and whatever substances you've been taking,

12:10.560 --> 12:14.480
the different foods that don't react well and so on.

12:14.480 --> 12:16.680
It could be coming partly from the past.

12:16.680 --> 12:17.960
It could be coming from the future.

12:17.960 --> 12:19.960
It could become external influences,

12:19.960 --> 12:22.880
sounds that you hear, or even angels.

12:22.880 --> 12:23.760
This is what they say.

12:23.760 --> 12:26.000
There could be spirits around them,

12:26.000 --> 12:29.520
because you have strong concentration.

12:29.520 --> 12:31.440
You're put into a state where you might be

12:31.440 --> 12:34.880
able to similar to meditation, where

12:34.880 --> 12:37.280
people practice meditation, go into great states

12:37.280 --> 12:39.600
of concentration, and are able to see, and hear,

12:39.600 --> 12:42.480
and experience things that we can't analyze.

12:42.480 --> 12:44.960
So it can come from all sorts of different sources,

12:44.960 --> 12:49.440
and it's not nearly as reliable as a meditation, for example,

12:49.440 --> 12:50.080
in any way.

12:50.080 --> 12:53.440
If you want to predict the future, go and practice meditation.

12:53.440 --> 12:55.640
If you want to understand about your past

12:55.640 --> 12:57.560
and what sort of things you're repressing,

12:57.560 --> 13:00.640
or practice meditation, if you want to see angels,

13:00.640 --> 13:02.720
and spirits, and so on, practice meditation,

13:02.720 --> 13:05.520
there are different meditations for this.

13:05.520 --> 13:10.040
But as I said, when you're dreaming,

13:10.040 --> 13:12.200
your dreams will show you something about yourself,

13:12.200 --> 13:13.280
about how your mind works.

13:13.280 --> 13:17.400
Because as I'm told, a perfectly enlightened person,

13:17.400 --> 13:21.520
a person who's done away with all of their mental defilements

13:21.520 --> 13:25.240
won't dream when they sleep, they sleep soundly,

13:25.240 --> 13:28.320
and they sleep mindfully.

13:28.320 --> 13:32.360
So actually, there's some sort of clarity of mind

13:32.360 --> 13:33.880
during the sleep, and when they wake up,

13:33.880 --> 13:36.360
they're fully refreshed and rested.

13:36.360 --> 13:40.760
But in regard, it's just important that, like anything,

13:40.760 --> 13:44.800
we don't follow them, and we don't project on them.

13:44.800 --> 13:46.000
A dream is what it is.

13:46.000 --> 13:47.680
You had that experience in your dream.

13:47.680 --> 13:50.480
It doesn't mean that it's going to happen in the future,

13:50.480 --> 13:53.520
or that somehow it has some special significance,

13:53.520 --> 13:56.840
or someone's trying to tell you something, or so on.

13:56.840 --> 13:58.760
Even if it's an angel or a spirit trying

13:58.760 --> 14:00.600
to tell you something, or even your mind trying

14:00.600 --> 14:03.640
to tell you something, that doesn't mean you should follow it.

14:03.640 --> 14:12.480
Because the source of the dream may still be full of delusion

14:12.480 --> 14:14.880
and misunderstanding is well and lead you on the wrong path.

14:14.880 --> 14:19.280
So dreams can be karmic, and that's

14:19.280 --> 14:21.160
been the last thing that I would say is that this

14:21.160 --> 14:24.040
should be a caution for us, that we can't always control

14:24.040 --> 14:25.840
our minds in the states like dream.

14:25.840 --> 14:29.560
But even in our waking states, dreams

14:29.560 --> 14:33.360
are just another reason for us to work on our minds,

14:33.360 --> 14:37.000
because otherwise, we're going to, when we fall asleep,

14:37.000 --> 14:37.680
we're going to dream.

14:37.680 --> 14:41.120
And what we mean by creating karma, even when we dream,

14:41.120 --> 14:44.280
is that we're building up the tendency to act in that way.

14:44.280 --> 14:47.200
So the dreams are reinforcing these emotions,

14:47.200 --> 14:49.600
reinforcing our fear, reinforcing our stress,

14:49.600 --> 14:52.360
reinforcing our anger, reinforcing our lust,

14:52.360 --> 14:55.360
reinforcing the states that we're trying to do away with.

14:55.360 --> 14:59.920
So as they can anyway, you can also have positive dreams

14:59.920 --> 15:03.560
where you have love and kindness towards other beings

15:03.560 --> 15:04.680
as possible.

15:04.680 --> 15:05.880
But generally, you'll see the things you're

15:05.880 --> 15:08.160
clinging to when you dream.

15:08.160 --> 15:11.080
And so it's an example of what we have to get rid of.

15:11.080 --> 15:15.040
And it's also a reason for us to, in a sense,

15:15.040 --> 15:18.240
be concerned and take our meditation seriously,

15:18.240 --> 15:21.760
because otherwise, this is the opposite of meditating

15:21.760 --> 15:23.720
where you're not mindful.

15:23.720 --> 15:25.720
And you will develop these states,

15:25.720 --> 15:29.160
and then we'll become more pronounced.

15:29.160 --> 15:31.560
So I hope that helps and answers the question.

15:31.560 --> 15:33.000
Thank you for tuning in once again.

15:33.000 --> 15:36.560
This has been another episode of Ask A Month.

15:36.560 --> 15:38.600
This is the first time using this new microphone.

15:38.600 --> 15:40.920
So I've got a microphone I hope it works.

15:40.920 --> 15:46.360
I can sit further away from the camera and let you see more

15:46.360 --> 15:47.600
than just my face.

15:47.600 --> 15:49.600
So thanks for tuning in, all the best.

