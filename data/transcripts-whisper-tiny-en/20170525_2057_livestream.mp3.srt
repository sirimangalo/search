1
00:00:00,000 --> 00:00:20,000
Okay, good evening everyone, welcome to our evening dumbest session.

2
00:00:20,000 --> 00:00:48,000
One very well-known description of the Buddha's teaching is as the middle way,

3
00:00:48,000 --> 00:01:00,000
and so all of us are on the middle way, trying to find the middle way.

4
00:01:00,000 --> 00:01:07,000
Sometimes it's described in that way.

5
00:01:07,000 --> 00:01:19,000
Most famously in terms of not torturing yourself, but not becoming indulgent.

6
00:01:19,000 --> 00:01:22,000
I think that applies very well to the meditation practice.

7
00:01:22,000 --> 00:01:28,000
It's something that is an important lesson for us during the meditation course.

8
00:01:28,000 --> 00:01:33,000
Don't torture yourself, don't force it.

9
00:01:33,000 --> 00:01:38,000
Don't even force yourself, not to force it.

10
00:01:38,000 --> 00:01:44,000
You realize that forcing isn't even under your control.

11
00:01:44,000 --> 00:01:51,000
It says it's something that you can just steamroll over.

12
00:01:51,000 --> 00:01:54,000
Don't torture yourself.

13
00:01:54,000 --> 00:01:57,000
But at the same time, don't become complacent.

14
00:01:57,000 --> 00:02:02,000
It seems like an intervention becomes a razor's edge.

15
00:02:02,000 --> 00:02:05,000
When there's only really one way, there's no leeway.

16
00:02:05,000 --> 00:02:10,000
If you want to really be in the middle way,

17
00:02:10,000 --> 00:02:17,000
a nupagama, you have to not go at all in either direction.

18
00:02:17,000 --> 00:02:26,000
So what we're looking for is this state of balance and the sense on a razor's edge.

19
00:02:26,000 --> 00:02:34,000
And balance is a really good way to understand the middle way as well.

20
00:02:34,000 --> 00:02:42,000
Now balance doesn't mean a little bit of indulgence and a little bit of torture.

21
00:02:42,000 --> 00:02:47,000
Balance means to find a state that is free from such things,

22
00:02:47,000 --> 00:02:57,000
like the state that is neither one direction.

