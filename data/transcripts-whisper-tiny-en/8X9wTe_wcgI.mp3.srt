1
00:00:00,000 --> 00:00:07,000
Okay, good evening, everyone. Welcome to our daily Dhamma.

2
00:00:16,000 --> 00:00:21,000
Today's topic is, what is it like to be enlightened?

3
00:00:21,000 --> 00:00:28,000
It's a good question, isn't it?

4
00:00:32,000 --> 00:00:39,000
It is what we're striving for, ultimately, right?

5
00:00:39,000 --> 00:00:49,000
Why is it good? Why is it a good question? Why is it good to know?

6
00:00:50,000 --> 00:00:58,000
I think obviously it's something we all are curious about and want to know.

7
00:00:59,000 --> 00:01:06,000
First of all, so that we know whether it's something that might be worth attaining ourselves.

8
00:01:06,000 --> 00:01:12,000
Also to know how far we are from it.

9
00:01:13,000 --> 00:01:17,000
But another, I think, interesting point is,

10
00:01:18,000 --> 00:01:25,000
it allows us to direct ourselves in the right direction,

11
00:01:25,000 --> 00:01:43,000
even though we're not enlightened ourselves, we can mimic and enlighten me.

12
00:01:43,000 --> 00:01:52,000
Maybe it's not the right word, we can emulate the word.

13
00:01:53,000 --> 00:02:00,000
Emulate is like when we keep the five precepts, when we don't kill, while enlightened beings don't kill naturally

14
00:02:01,000 --> 00:02:10,000
for the end enlightened, something you have to work at, so you emulate enlightened behavior.

15
00:02:10,000 --> 00:02:16,000
Some cynic might say, while you fake, you know, saying, fake it till you make it.

16
00:02:17,000 --> 00:02:24,000
There's something interesting there, because the Tibetans believe that by pretending to be a Buddha,

17
00:02:25,000 --> 00:02:32,000
or not pretending by really believing that you are a Buddha, you actually become closer to being a Buddha.

18
00:02:32,000 --> 00:02:40,000
Well, I don't think that that's, I don't agree that that's adequate practice.

19
00:02:41,000 --> 00:02:46,000
I don't know that the Tibetans really do either, although I don't know too much about their tradition.

20
00:02:47,000 --> 00:02:56,000
But there's something to it. I mean, it gives you a roadmap,

21
00:02:56,000 --> 00:03:06,000
it gives you some markers to live by, to know when you're acting like an enlightened being, and to know when you're not,

22
00:03:07,000 --> 00:03:09,000
so to know what you have to change.

23
00:03:20,000 --> 00:03:23,000
So this is the question, what is it like to be enlightened?

24
00:03:23,000 --> 00:03:31,000
There's a story that I think is interesting. I've got two lists that I can think of,

25
00:03:32,000 --> 00:03:37,000
where the Buddha gave a list of characteristics of what it might be like to be enlightened.

26
00:03:38,000 --> 00:03:40,000
The first one is, I think, my favorite.

27
00:03:42,000 --> 00:03:47,000
Sorry, Buddha asks, sorry, Buddha says something.

28
00:03:47,000 --> 00:03:56,000
I'm about the Buddha and the Buddha says, oh, so you have faith in me that I'm enlightened.

29
00:03:57,000 --> 00:04:01,000
And sorry, Buddha said, no, I don't have faith in you that you're enlightened.

30
00:04:06,000 --> 00:04:08,000
And the Buddha went away.

31
00:04:09,000 --> 00:04:11,000
And sorry, Buddha was the Buddha's chief disciple, right?

32
00:04:12,000 --> 00:04:15,000
I mean, chief disciple saying, hey, I don't have faith in you.

33
00:04:15,000 --> 00:04:26,000
And so the monks were all disturbed by this, the unenlightened monks were disturbed by it, because they misunderstood what he meant.

34
00:04:27,000 --> 00:04:33,000
Of course, he didn't mean that, sorry, but it didn't mean he didn't think the Buddha was enlightened, but he knew that the Buddha was enlightened.

35
00:04:34,000 --> 00:04:36,000
It wasn't out of faith, right?

36
00:04:36,000 --> 00:04:44,000
So they started talking about this, oh, sorry, Buddha has no faith in the Buddha.

37
00:04:45,000 --> 00:04:49,000
And the Buddha hears them talking, and he says, what are you guys talking about?

38
00:04:50,000 --> 00:04:57,000
They say, they tell them, and the Buddha said, yes, that's right, he's faithless.

39
00:04:57,000 --> 00:05:07,000
And he said, and he gave this word, I said, oh, I got the new son, he told you, no, no, no.

40
00:05:08,000 --> 00:05:12,000
And these words, you really have to know poly to really get the joke.

41
00:05:12,000 --> 00:05:16,000
I mean, it's probably one of the closest examples the Buddha comes to humor.

42
00:05:19,000 --> 00:05:22,000
I said, oh, means faithless.

43
00:05:22,000 --> 00:05:28,000
I cut the new means, it's a word that means ungrateful.

44
00:05:29,000 --> 00:05:31,000
Ah, it means not.

45
00:05:32,000 --> 00:05:37,000
And then cut that means what is done, and unnew means one who knows.

46
00:05:38,000 --> 00:05:44,000
So it means one who doesn't know, means in the sense of doesn't keep in mind the things that were done for them.

47
00:05:45,000 --> 00:05:49,000
Someone does something for you, you just forget about it, not interested.

48
00:05:49,000 --> 00:05:52,000
Not grateful, is what it means, I cut the new.

49
00:05:55,000 --> 00:06:02,000
Sundi Keto means one who breaks chains, breaks locks, lockpick.

50
00:06:03,000 --> 00:06:08,000
So when you have, you know, they would lock up, lock their gates in India,

51
00:06:10,000 --> 00:06:12,000
to keep the robbers out.

52
00:06:12,000 --> 00:06:19,000
So a Sunday chain, there was a robber, someone who picks locks, a house breaker, kind of thing.

53
00:06:22,000 --> 00:06:27,000
Sundi means a chain, and a chain, a Sunday chain, a chain that means one who breaks,

54
00:06:27,000 --> 00:06:31,000
it cuts with whatever they had to cut the chain.

55
00:06:31,000 --> 00:06:40,000
Hatha Wakasou, someone who has destroyed all opportunity.

56
00:06:43,000 --> 00:06:57,000
So someone who has destroyed all opportunities, for someone who is without any future.

57
00:06:57,000 --> 00:07:02,000
Like this is what you'd say about a bum, someone who had dropped out of school and had no interest,

58
00:07:03,000 --> 00:07:06,000
maybe just did drugs all day or something, drank alcohol all day.

59
00:07:07,000 --> 00:07:17,000
So someone with no opportunity, Hatha Wakasou, one Tasso means someone who is hopeless.

60
00:07:17,000 --> 00:07:26,000
I mean someone who you have no hope in, you look at them and you say, and you cannot hope good for that person.

61
00:07:26,000 --> 00:07:29,000
There is no hope for them, all hope is lost.

62
00:07:32,000 --> 00:07:36,000
Then the Buddha says, Hatha Wakasou, one to someone, then he says,

63
00:07:36,000 --> 00:07:47,000
subway, or demapouriso, or demapouriso. This is the height of humanity, he says. He's talking about, sorry, put that.

64
00:07:54,000 --> 00:07:57,000
And you have to know the pile, you didn't know how this all makes sense.

65
00:07:57,000 --> 00:08:00,000
And the first one is easy to understand this from the story.

66
00:08:00,000 --> 00:08:08,000
Hatha Wakasou means someone who doesn't have to believe anyone, not in regards to what's important.

67
00:08:09,000 --> 00:08:16,000
They have no faith in the Buddha because they know it's knowledge, it's no longer belief.

68
00:08:16,000 --> 00:08:28,000
I said to all these, I got an argument with the Buddhist, when Sri Lankan Buddhist went, he said, oh, faith is so important. I said, oh, yeah.

69
00:08:28,000 --> 00:08:32,000
Listen to the Buddha, he said, faith less is the best.

70
00:08:34,000 --> 00:08:38,000
It was kind of joking, faith is important, faith is useful.

71
00:08:38,000 --> 00:08:47,000
And there's a quality of mind, it's the same thing. In fact, the faith is much stronger when you act in the mind, when you actually know it.

72
00:08:47,000 --> 00:08:53,000
So it still is faith, but in a conventional sense, we wouldn't call it faith because you actually know.

73
00:08:53,000 --> 00:09:00,000
If you know something to be true, you don't have to believe it, but technically you still do believe it.

74
00:09:00,000 --> 00:09:08,000
I cut a new, I cut a new is an interesting compound because it actually can mean two different things.

75
00:09:08,000 --> 00:09:16,000
So I cut a new, cut a new means one who knows what is done, I cut a new one who doesn't know.

76
00:09:16,000 --> 00:09:25,000
But you can also split it, I cut that in a new one who knows what is, I cut that.

77
00:09:25,000 --> 00:09:30,000
And I cut that is that which is not made, that which is not produced.

78
00:09:30,000 --> 00:09:35,000
And there are only two things that are not, there's only one thing I guess that's not made, not produced.

79
00:09:35,000 --> 00:09:44,000
And that is nibhana. Nibhana is called the akata dhamma. It's not made, it's not produced.

80
00:09:44,000 --> 00:09:47,000
It's not caused.

81
00:09:48,000 --> 00:09:52,000
So sorry put to someone who knows nibhana is what it means.

82
00:09:52,000 --> 00:09:57,000
Sanitya da Sanitya das fairly easy because Sanitya is the chain.

83
00:09:57,000 --> 00:10:01,000
And it's the chain of the chain of causation, you know.

84
00:10:01,000 --> 00:10:06,000
A vidya pachea sankara sankara pachea vinya nuk.

85
00:10:06,000 --> 00:10:13,000
Because of ignorance there is karma because of karma there is vinya nuk birth.

86
00:10:13,000 --> 00:10:23,000
If you cut that chain, if you cut out the ignorance then there is no karma because there is no karma, there is no rebirth.

87
00:10:23,000 --> 00:10:26,000
Because there is no rebirth and there is no suffering and so on.

88
00:10:26,000 --> 00:10:32,000
There is no craving because there is no craving, there is no clinging, no clinging means no becoming and so on.

89
00:10:32,000 --> 00:10:37,000
You cut this chain.

90
00:10:37,000 --> 00:10:45,000
It means someone who has no opportunity.

91
00:10:45,000 --> 00:10:51,000
Opportunity here means opportunity for more arising, especially of defilements.

92
00:10:51,000 --> 00:10:59,000
In this person there is no opportunity for more karma, for more becoming everything they do is just functional.

93
00:10:59,000 --> 00:11:01,000
It's what we think we are.

94
00:11:01,000 --> 00:11:04,000
It sounds like a zombie but it's not.

95
00:11:04,000 --> 00:11:07,000
It's what we think we are.

96
00:11:07,000 --> 00:11:10,000
We think when I eat I'm just eating, right?

97
00:11:10,000 --> 00:11:14,000
When you come to meditate and you realize it's not actually the case.

98
00:11:14,000 --> 00:11:20,000
When we eat we're lost, we're often not even anywhere near the food.

99
00:11:20,000 --> 00:11:26,000
Our minds are off doing something else or if they're near the food they're obsessing over it.

100
00:11:26,000 --> 00:11:33,000
It's good, it's bad, it's too sweet, too salty, too plain, too hot, too cold.

101
00:11:33,000 --> 00:11:40,000
Or it's just right, it's perfect, it's delicious.

102
00:11:40,000 --> 00:11:48,000
And in light and being there's no opportunity for any of that.

103
00:11:48,000 --> 00:11:52,000
There's no opportunity for defilements to get in.

104
00:11:52,000 --> 00:11:55,000
No opportunity for mara, for evil.

105
00:11:55,000 --> 00:12:01,000
You can't hurt such a person, you can't trigger them.

106
00:12:01,000 --> 00:12:10,000
You can't instigate them.

107
00:12:10,000 --> 00:12:13,000
One hanta wakasu.

108
00:12:13,000 --> 00:12:15,000
One tasu, someone who is hopeless.

109
00:12:15,000 --> 00:12:17,000
I mean this is a great word.

110
00:12:17,000 --> 00:12:21,000
Someone who is hopeless means someone who doesn't hope.

111
00:12:21,000 --> 00:12:26,000
Someone who hopes means they still want things.

112
00:12:26,000 --> 00:12:32,000
Meditators hope that tomorrow will be that the pain will go away tomorrow.

113
00:12:32,000 --> 00:12:36,000
Had a bad day today, I hope that tomorrow is a better day.

114
00:12:36,000 --> 00:12:38,000
Or I had a really good day.

115
00:12:38,000 --> 00:12:43,000
I hope tomorrow is just like today or even better.

116
00:12:43,000 --> 00:12:45,000
So here's a reminder for you.

117
00:12:45,000 --> 00:12:47,000
Abandon all hope.

118
00:12:47,000 --> 00:12:52,000
I hope I had to know, hope you enter here.

119
00:12:52,000 --> 00:12:57,000
Without hope, it's like without wanting, if you have no hope,

120
00:12:57,000 --> 00:13:02,000
you're already perfect.

121
00:13:02,000 --> 00:13:04,000
It's really that simple.

122
00:13:04,000 --> 00:13:09,000
If you want to be happy, the only way to be happy is to stop wanting.

123
00:13:09,000 --> 00:13:12,000
And so therefore, just stop hoping.

124
00:13:12,000 --> 00:13:16,000
Hope will always be a vulnerability.

125
00:13:16,000 --> 00:13:21,000
Never be an asset.

126
00:13:21,000 --> 00:13:23,000
It's not an asset to become enlightened.

127
00:13:23,000 --> 00:13:25,000
You can't hope you're going to become enlightened.

128
00:13:25,000 --> 00:13:27,000
It doesn't work that way.

129
00:13:27,000 --> 00:13:31,000
Hope is a detriment because it means your discontent.

130
00:13:31,000 --> 00:13:38,000
It means you're not fully present here, objective with reality.

131
00:13:38,000 --> 00:13:45,000
Your biased partial.

132
00:13:45,000 --> 00:13:53,000
And therefore, disappointed much of the time.

133
00:13:53,000 --> 00:13:55,000
Some way with them are poorly solved.

134
00:13:55,000 --> 00:13:58,000
This is the height of what it means to be a human.

135
00:13:58,000 --> 00:14:01,000
The height of humanity.

136
00:14:01,000 --> 00:14:03,000
I think I've talked about this before.

137
00:14:03,000 --> 00:14:05,000
That's a really good list.

138
00:14:05,000 --> 00:14:12,000
There's another one that's actually in the Patisambita manga.

139
00:14:12,000 --> 00:14:15,000
That actually, I don't know if it comes anywhere else,

140
00:14:15,000 --> 00:14:19,000
but it's one that's familiar to me.

141
00:14:19,000 --> 00:14:23,000
I can only find it in the Patisambita manga,

142
00:14:23,000 --> 00:14:29,000
which is sort of a lesser.

143
00:14:29,000 --> 00:14:35,000
I mean, it's one of the more technical parts of the Topitika,

144
00:14:35,000 --> 00:14:39,000
probably not actually the words of the Buddha.

145
00:14:39,000 --> 00:14:46,000
It's supposed to be the words of sorry, Buddha, but maybe.

146
00:14:46,000 --> 00:14:47,000
So when it says,

147
00:14:47,000 --> 00:14:50,000
a kodana, a kodana, a kodana, a nupana.

148
00:14:50,000 --> 00:14:54,000
We see those through the tangato.

149
00:14:54,000 --> 00:14:55,000
There's no no.

150
00:14:55,000 --> 00:14:57,000
We see those.

151
00:14:57,000 --> 00:14:58,000
Let's see.

152
00:14:58,000 --> 00:14:59,000
A maki, maybe first.

153
00:14:59,000 --> 00:15:03,000
A kodana, a nupana, a maki.

154
00:15:03,000 --> 00:15:05,000
We see those through the tangato.

155
00:15:05,000 --> 00:15:13,000
So sampana, ditimidawi, dangana, a ryoiti.

156
00:15:13,000 --> 00:15:15,000
This appears to have come from an earlier text,

157
00:15:15,000 --> 00:15:19,000
but I don't know where that is.

158
00:15:19,000 --> 00:15:22,000
So it gives a list of qualities of an aria.

159
00:15:22,000 --> 00:15:26,000
I mean, this is the list of what it means to be an enlightened being.

160
00:15:26,000 --> 00:15:29,000
So it's another really good list to think of.

161
00:15:29,000 --> 00:15:31,000
It's simpler than the other one.

162
00:15:31,000 --> 00:15:32,000
A kodana, a nupana, a nupana,

163
00:15:32,000 --> 00:15:35,000
I mean, these two words often go together.

164
00:15:35,000 --> 00:15:40,000
They're on that translation of,

165
00:15:40,000 --> 00:15:42,000
for the non-attranslation,

166
00:15:42,000 --> 00:15:45,000
they relate to the verse that we have hanging on the wall.

167
00:15:45,000 --> 00:15:49,000
A kodana means one who doesn't get angry.

168
00:15:49,000 --> 00:15:51,000
Sorry, Buddha was famous for this.

169
00:15:51,000 --> 00:15:54,000
There was a monk who accused him of all sorts of things.

170
00:15:54,000 --> 00:15:56,000
There was another monk.

171
00:15:56,000 --> 00:15:59,000
There was a good story of a monk who heard that,

172
00:15:59,000 --> 00:16:03,000
sorry, Buddha didn't get angry.

173
00:16:03,000 --> 00:16:05,000
And everyone's praising, sorry, Buddha,

174
00:16:05,000 --> 00:16:07,000
and so he thought, I'm going to test this.

175
00:16:07,000 --> 00:16:09,000
So he took a stick.

176
00:16:09,000 --> 00:16:10,000
And when sorry, Buddha was walking,

177
00:16:10,000 --> 00:16:11,000
he actually came up behind him

178
00:16:11,000 --> 00:16:17,000
and just whacked him across the back with a stick.

179
00:16:17,000 --> 00:16:19,000
Yeah, it's ever happened to me,

180
00:16:19,000 --> 00:16:25,000
but I've certainly been tested by my share of people.

181
00:16:25,000 --> 00:16:27,000
I don't have to compare myself to sorry,

182
00:16:27,000 --> 00:16:30,000
I put them, but as a monk,

183
00:16:30,000 --> 00:16:33,000
it sounds crazy to think,

184
00:16:33,000 --> 00:16:37,000
but there are people who just,

185
00:16:37,000 --> 00:16:41,000
the first order of business for them

186
00:16:41,000 --> 00:16:47,000
is to test the people who they're going to look up to.

187
00:16:47,000 --> 00:16:50,000
So, slap them across the back

188
00:16:50,000 --> 00:16:54,000
and sorry, Buddha just turns around and looks at him

189
00:16:54,000 --> 00:17:01,000
and then keeps walking.

190
00:17:01,000 --> 00:17:04,000
I called her, no, he didn't get angry.

191
00:17:04,000 --> 00:17:09,000
Anupanahi is what relates to that verse on the wall.

192
00:17:09,000 --> 00:17:13,000
Anupanahi means to get angry back

193
00:17:13,000 --> 00:17:18,000
or it means to hold on to the anger.

194
00:17:18,000 --> 00:17:22,000
Holding on to the anger is

195
00:17:22,000 --> 00:17:27,000
the worst evil, right?

196
00:17:27,000 --> 00:17:31,000
I mean, getting angry is something we all work

197
00:17:31,000 --> 00:17:35,000
and work with, work at, work to overcome

198
00:17:35,000 --> 00:17:39,000
as meditators,

199
00:17:39,000 --> 00:17:42,000
being meditation centers,

200
00:17:42,000 --> 00:17:44,000
meditators will often get angry

201
00:17:44,000 --> 00:17:46,000
and people who work in the meditation center

202
00:17:46,000 --> 00:17:49,000
certainly will get angry at each other.

203
00:17:49,000 --> 00:17:51,000
And that's something that,

204
00:17:51,000 --> 00:17:53,000
to some extent, we have to allow,

205
00:17:53,000 --> 00:17:55,000
we have to make allowance for each other.

206
00:17:55,000 --> 00:17:59,000
Yes, this person is angry at me.

207
00:17:59,000 --> 00:18:03,000
I mean, I think often there's,

208
00:18:03,000 --> 00:18:04,000
it's dangerous.

209
00:18:04,000 --> 00:18:07,000
We fall into this,

210
00:18:11,000 --> 00:18:14,000
this word,

211
00:18:14,000 --> 00:18:19,000
where we don't allow it.

212
00:18:19,000 --> 00:18:26,000
Maybe lack of English words.

213
00:18:26,000 --> 00:18:31,000
We aren't able to accept

214
00:18:31,000 --> 00:18:33,000
someone else's angry,

215
00:18:33,000 --> 00:18:35,000
intolerance, that's sort.

216
00:18:35,000 --> 00:18:38,000
We're intolerant of other people's anger

217
00:18:38,000 --> 00:18:39,000
and we think, hey, these are meditators,

218
00:18:39,000 --> 00:18:41,000
what are they doing getting angry?

219
00:18:41,000 --> 00:18:44,000
I had one monk, he was threatening to throw me

220
00:18:44,000 --> 00:18:47,000
to smash me against a wall

221
00:18:47,000 --> 00:18:51,000
really big British guy once.

222
00:18:51,000 --> 00:18:54,000
Another monk chased me through the forest

223
00:18:54,000 --> 00:18:56,000
once I had a monk take a broomstick

224
00:18:56,000 --> 00:18:58,000
to my head once.

225
00:18:58,000 --> 00:18:59,000
He didn't actually hit me,

226
00:18:59,000 --> 00:19:00,000
but he would have.

227
00:19:00,000 --> 00:19:01,000
He really would have.

228
00:19:01,000 --> 00:19:03,000
He was ready too.

229
00:19:03,000 --> 00:19:04,000
I was being a little,

230
00:19:04,000 --> 00:19:07,000
I was testing him because I knew it

231
00:19:07,000 --> 00:19:12,000
long story, but yeah.

232
00:19:12,000 --> 00:19:15,000
But you have to allow for it to some extent.

233
00:19:15,000 --> 00:19:16,000
I mean, I think the broomstick

234
00:19:16,000 --> 00:19:20,000
was a bit much, but the problem

235
00:19:20,000 --> 00:19:24,000
is when we hold on to it.

236
00:19:24,000 --> 00:19:26,000
If you understand these things,

237
00:19:26,000 --> 00:19:30,000
you understand that these conditions come up

238
00:19:30,000 --> 00:19:32,000
and you deal with them.

239
00:19:32,000 --> 00:19:35,000
I remember I angry people coming to my teacher

240
00:19:35,000 --> 00:19:37,000
and leaving without the anger

241
00:19:37,000 --> 00:19:40,000
because he didn't partake in it.

242
00:19:40,000 --> 00:19:42,000
I'd sit and watch and people come

243
00:19:42,000 --> 00:19:44,000
with all sorts and it's like a sponge

244
00:19:44,000 --> 00:19:46,000
just taking it in,

245
00:19:46,000 --> 00:19:48,000
cleaning it out in with a bad,

246
00:19:48,000 --> 00:19:52,000
out with a good.

247
00:19:52,000 --> 00:19:54,000
Well, that's kind of how we should be

248
00:19:54,000 --> 00:19:56,000
as like filters.

249
00:19:56,000 --> 00:19:58,000
People bring us all their garbage.

250
00:19:58,000 --> 00:20:03,000
We take it in.

251
00:20:03,000 --> 00:20:07,000
And really it means we don't take it in.

252
00:20:07,000 --> 00:20:08,000
But it's kind of like a filter.

253
00:20:08,000 --> 00:20:10,000
You act like a filter because

254
00:20:10,000 --> 00:20:12,000
normally not taking in means to reject it.

255
00:20:12,000 --> 00:20:14,000
You know, you're angry at me.

256
00:20:14,000 --> 00:20:15,000
I reject that.

257
00:20:15,000 --> 00:20:17,000
No, you're not allowed to be angry at me.

258
00:20:17,000 --> 00:20:18,000
Go away.

259
00:20:18,000 --> 00:20:19,000
Get out of my face.

260
00:20:19,000 --> 00:20:20,000
I won't.

261
00:20:20,000 --> 00:20:21,000
I can't handle this.

262
00:20:21,000 --> 00:20:23,000
Go in your room, lock the door.

263
00:20:23,000 --> 00:20:26,000
That's how we normally reject people's anger.

264
00:20:26,000 --> 00:20:28,000
But that's this.

265
00:20:28,000 --> 00:20:29,000
That's holding on to it.

266
00:20:29,000 --> 00:20:31,000
That's reacting to it.

267
00:20:31,000 --> 00:20:34,000
And it's really the Buddha said it's the worst evil.

268
00:20:34,000 --> 00:20:36,000
To say what they're in a papi or yolk would

269
00:20:36,000 --> 00:20:38,000
dung particularly.

270
00:20:38,000 --> 00:20:40,000
One who is angry back at someone who gets angry.

271
00:20:40,000 --> 00:20:43,000
It's the worst evil.

272
00:20:43,000 --> 00:20:49,000
Because that's what creates the conflict.

273
00:20:49,000 --> 00:20:56,000
To be a real filter and to have true purity.

274
00:20:56,000 --> 00:20:58,000
You bring you take it in.

275
00:20:58,000 --> 00:20:59,000
You accept it.

276
00:20:59,000 --> 00:21:03,000
Someone's angry at you.

277
00:21:03,000 --> 00:21:06,000
I mean, the anger doesn't come to you.

278
00:21:06,000 --> 00:21:09,000
We think of it like the anger is some kind of vibe

279
00:21:09,000 --> 00:21:12,000
that makes us angry, but it's not.

280
00:21:12,000 --> 00:21:14,000
All that comes to us is seeing hearing,

281
00:21:14,000 --> 00:21:16,000
smelling, tasting, feeling, thinking.

282
00:21:16,000 --> 00:21:19,000
Our anger is totally unrelated to their anger.

283
00:21:19,000 --> 00:21:22,000
That's what we don't realize.

284
00:21:22,000 --> 00:21:24,000
Your anger is not because they are angry.

285
00:21:24,000 --> 00:21:29,000
It's because you get tricked into reacting the way they react.

286
00:21:29,000 --> 00:21:32,000
Where we mimic someone's angry.

287
00:21:32,000 --> 00:21:33,000
We get angry.

288
00:21:33,000 --> 00:21:36,000
Someone's stressed.

289
00:21:36,000 --> 00:21:38,000
We get stressed.

290
00:21:38,000 --> 00:21:41,000
And we think, oh, they rubbed off on me.

291
00:21:41,000 --> 00:21:42,000
It didn't rub off.

292
00:21:42,000 --> 00:21:44,000
You're mimicking them.

293
00:21:44,000 --> 00:21:49,000
You're letting it trigger you.

294
00:21:49,000 --> 00:21:51,000
So, according to no one up and I,

295
00:21:51,000 --> 00:21:56,000
not to get angry, not to hold on to anger.

296
00:21:56,000 --> 00:22:01,000
I'm a key means to not be crooked.

297
00:22:01,000 --> 00:22:07,000
I'm a key is to not look down on people.

298
00:22:07,000 --> 00:22:14,000
To not be condescending or arrogant, conceited.

299
00:22:14,000 --> 00:22:20,000
This one monk accused Sariput of hitting him.

300
00:22:20,000 --> 00:22:25,000
And what had happened is Sariput had walked by and brushed his robe.

301
00:22:25,000 --> 00:22:26,000
There was a part of his robe saying,

302
00:22:26,000 --> 00:22:28,000
Sariput had brushed him like that.

303
00:22:28,000 --> 00:22:31,000
And so he goes around saying that you really had a bone to pick.

304
00:22:31,000 --> 00:22:33,000
It was something about something else.

305
00:22:33,000 --> 00:22:36,000
He felt like Sariput was partial against him.

306
00:22:36,000 --> 00:22:40,000
So, he wanted to create trouble.

307
00:22:40,000 --> 00:22:41,000
And so he went around saying,

308
00:22:41,000 --> 00:22:43,000
sorry, I put that, hit him.

309
00:22:43,000 --> 00:22:45,000
And Sariput came before the Buddha and the Buddha said,

310
00:22:45,000 --> 00:22:48,000
hey, so they're saying that you hit this monk.

311
00:22:48,000 --> 00:22:49,000
Is it true?

312
00:22:49,000 --> 00:22:52,000
Or he didn't even ask whether it's true because he knows it's not true.

313
00:22:52,000 --> 00:22:54,000
But he said, this is what they're saying.

314
00:22:54,000 --> 00:22:57,000
And Sariput said, you know,

315
00:22:57,000 --> 00:23:00,000
for someone who valued this body,

316
00:23:00,000 --> 00:23:03,000
someone who valued the physical body,

317
00:23:03,000 --> 00:23:09,000
and I might such a person might hit another person.

318
00:23:09,000 --> 00:23:14,000
But this body is like a corpse to me that I have to carry around.

319
00:23:14,000 --> 00:23:18,000
I have no attachment to physical realm whatsoever.

320
00:23:18,000 --> 00:23:26,000
I would have no reason to hit anyone.

321
00:23:26,000 --> 00:23:35,000
The teaching is that an enlightened being doesn't have any,

322
00:23:35,000 --> 00:23:39,000
you know, attachment to themselves.

323
00:23:39,000 --> 00:23:45,000
No reason to harm others because they don't cling to themselves.

324
00:23:45,000 --> 00:23:50,000
So there's no holding yourself as hired as someone else.

325
00:23:50,000 --> 00:24:00,000
The idea that you might hit someone else is just ridiculous.

326
00:24:00,000 --> 00:24:03,000
And then we have, we sudho sudhatangato.

327
00:24:03,000 --> 00:24:06,000
We sudho means someone who is pure.

328
00:24:06,000 --> 00:24:08,000
So I mean, this is really the key.

329
00:24:08,000 --> 00:24:10,000
And this is something that we're all, I think, fairly well aware of.

330
00:24:10,000 --> 00:24:15,000
And enlightened being doesn't have anger, greed, delusion.

331
00:24:15,000 --> 00:24:19,000
Their minds are pure.

332
00:24:19,000 --> 00:24:26,000
So dhatangato means going to that which is pure, which is nibana again.

333
00:24:26,000 --> 00:24:35,000
They enter into states of purity where they become free from suffering.

334
00:24:35,000 --> 00:24:43,000
Some panadityi made how they have right view and their wives.

335
00:24:43,000 --> 00:24:49,000
Dhan dhan ya aryo, such a person you know as an area.

336
00:24:49,000 --> 00:24:53,000
So some panadityi means right view.

337
00:24:53,000 --> 00:24:56,000
And there's much, there's many aspects to this.

338
00:24:56,000 --> 00:25:01,000
I mean, right view to some extent is just knowing that what the Buddha top was right.

339
00:25:01,000 --> 00:25:07,000
But most importantly, it relates to the four noble truths.

340
00:25:07,000 --> 00:25:12,000
To know that nothing is worth clinging to, basically,

341
00:25:12,000 --> 00:25:15,000
what you're learning now may not realize it.

342
00:25:15,000 --> 00:25:21,000
But if you think about it, and if you reflect upon what you are learning,

343
00:25:21,000 --> 00:25:23,000
not even maybe what you expected to learn,

344
00:25:23,000 --> 00:25:28,000
but what you're learning is that nothing's worth clinging to.

345
00:25:28,000 --> 00:25:36,000
You're looking at, you're realizing that you're clinging to a lot of stuff that really isn't worth clinging to.

346
00:25:36,000 --> 00:25:42,000
Our suffering comes from clinging to things.

347
00:25:42,000 --> 00:25:44,000
That's the four noble truths.

348
00:25:44,000 --> 00:25:48,000
When you really realize that, that's called right view.

349
00:25:48,000 --> 00:25:52,000
Noble right view.

350
00:25:52,000 --> 00:25:56,000
And that's what leads to nibana, that's what leads to freedom.

351
00:25:56,000 --> 00:26:01,000
Made how he is wisdom, or one who is wise.

352
00:26:01,000 --> 00:26:04,000
That relates a lot to right view.

353
00:26:04,000 --> 00:26:09,000
But there's more, I think, right view does for you.

354
00:26:09,000 --> 00:26:14,000
It allows you wisdom of so many, you know, many different kinds.

355
00:26:14,000 --> 00:26:20,000
You find that amazingly you're able to solve all your worldly problems much better.

356
00:26:20,000 --> 00:26:28,000
There's still challenges, but the challenges are reduced by an order of magnitude, at least.

357
00:26:28,000 --> 00:26:37,000
They're not difficult in the same way, because you realize the true problem is never with your experiences or your situation.

358
00:26:37,000 --> 00:26:41,000
It's with your reactions.

359
00:26:41,000 --> 00:26:48,000
You're able to look at a situation objectively, because you're not hating it or loving it.

360
00:26:48,000 --> 00:26:50,000
You're observing it.

361
00:26:50,000 --> 00:26:54,000
And so, therefore, you can see what most other people overlook.

362
00:26:54,000 --> 00:27:04,000
And they tie themselves in knots to get what they want, and to get away from what they don't want.

363
00:27:04,000 --> 00:27:06,000
So that's sort of wisdom.

364
00:27:06,000 --> 00:27:20,000
It's the wisdom of an enlightened being, to be able to solve, and to fix, and to organize, and to build great things.

365
00:27:20,000 --> 00:27:28,000
And that's the demo for tonight, a little bit of insight into what it's like to be enlightened.

366
00:27:28,000 --> 00:27:36,000
And just some idea, some of the qualities of mind that enlightened being possessed is useful for us.

367
00:27:36,000 --> 00:27:46,000
This is what we strive for, and remind us of some of these things, and hopefully give us something to aim for, that's something that we'd like to be.

368
00:27:46,000 --> 00:27:58,000
Hopefully, some of this seems desirable, like something that you would like to strive towards.

369
00:27:58,000 --> 00:28:11,000
Because just feeling that way, just aligning yourself with these qualities, and just that is a great step towards becoming that.

370
00:28:11,000 --> 00:28:17,000
Because of your intention, your intention and your inclination will drive you.

371
00:28:17,000 --> 00:28:19,000
It's what will lead you.

372
00:28:19,000 --> 00:28:27,000
Having that framework puts you in the right direction, it focuses your practice.

373
00:28:27,000 --> 00:28:31,000
So, there you go. That's the demo for tonight.

374
00:28:31,000 --> 00:28:35,000
Thank you all for tuning in.

375
00:28:35,000 --> 00:28:38,000
You all can go practice.

376
00:28:38,000 --> 00:28:45,000
I'm going to answer a few questions.

377
00:28:45,000 --> 00:28:46,000
See how many questions?

378
00:28:46,000 --> 00:28:49,000
Okay, we've got four questions.

379
00:28:49,000 --> 00:28:55,000
Since killing is against the precepts, is spraying a house against bugs, cockroaches, killing, considered killing.

380
00:28:55,000 --> 00:28:59,000
I mean, unless it's repellent, those things usually kill the insects.

381
00:28:59,000 --> 00:29:02,000
So, yes, that would be killing.

382
00:29:02,000 --> 00:29:04,000
As far as bacteria, I don't know.

383
00:29:04,000 --> 00:29:05,000
I don't really have an answer for that.

384
00:29:05,000 --> 00:29:10,000
I'm not sure whether bacteria has a mind or not.

385
00:29:10,000 --> 00:29:19,000
Someone was saying, no, I asked among this once, and he said, no, because you don't even know that the bacteria are there.

386
00:29:19,000 --> 00:29:22,000
It's kind of a weird answer. I'm not so convinced.

387
00:29:22,000 --> 00:29:34,000
It was more complicated than that, but it's not something I would worry about personally.

388
00:29:34,000 --> 00:29:38,000
I mean, I don't sympathize so much with bacteria.

389
00:29:38,000 --> 00:29:41,000
I empathize with insects.

390
00:29:41,000 --> 00:29:45,000
So, therefore, I feel an important not to kill insects.

391
00:29:45,000 --> 00:29:52,000
Don't empathize with bacteria, really.

392
00:29:52,000 --> 00:30:00,000
Okay, I'm being propelled less propelled to engage in social interactions.

393
00:30:00,000 --> 00:30:04,000
Okay, I'm also doing summit and repass.

394
00:30:04,000 --> 00:30:08,000
Well, I mean, this is a long question, but my only answer to you is do one or the other.

395
00:30:08,000 --> 00:30:11,000
I mean, I don't teach summit that I think you know.

396
00:30:11,000 --> 00:30:19,000
So, if you want my advice, you really have to, your best bet is to just practice as I teach.

397
00:30:19,000 --> 00:30:26,000
Now, if you're practicing something else, really you should find a teacher who will help you with that.

398
00:30:26,000 --> 00:30:32,000
What are naga's in Buddhism?

399
00:30:32,000 --> 00:30:35,000
I don't know for anything to the naga's.

400
00:30:35,000 --> 00:30:44,000
Naga's are dragons, really, or it's a serpent race that apparently lives under the earth, under the water, I don't know.

401
00:30:44,000 --> 00:30:52,000
In their own realm, actually, it's like another dimension or something.

402
00:30:52,000 --> 00:31:00,000
How important is kindness and or giving to the practice? I should want to interact with people not directly involved with one's life.

403
00:31:00,000 --> 00:31:03,000
I think it's important.

404
00:31:03,000 --> 00:31:06,000
I mean, I think it's a quality of enlightened being.

405
00:31:06,000 --> 00:31:10,000
So, doing it is in way of emulating and enlightened being.

406
00:31:10,000 --> 00:31:13,000
Because enlightened being has no attachment to things.

407
00:31:13,000 --> 00:31:17,000
So, if someone asks you for something, it doesn't mean you should always give.

408
00:31:17,000 --> 00:31:23,000
But it is an interesting practice. It's a useful practice because it teaches you to let go.

409
00:31:23,000 --> 00:31:28,000
When you don't give, when someone asks you, when you don't help, when someone needs help,

410
00:31:28,000 --> 00:31:34,000
when you're stingy, you're greedy, you're holding on.

411
00:31:34,000 --> 00:31:40,000
Maybe you're even arrogant.

412
00:31:40,000 --> 00:31:44,000
Okay, and that's the question.

413
00:31:44,000 --> 00:31:48,000
So, thank you all for coming out. Have a good night.

