1
00:00:00,000 --> 00:00:09,580
Okay, and we'll come back to our city study today. We will be finishing up. Hopefully the

2
00:00:09,580 --> 00:00:18,920
Jula Hatipado Jula Hatipado from the city, which is kind of 27, picking up what we left

3
00:00:18,920 --> 00:00:27,400
off on Thursday, last week. So as usual, we will be, once we get started, we'll be reading

4
00:00:27,400 --> 00:00:36,360
the chanting the poly, and then reading and discussing the English. It's not much left in

5
00:00:36,360 --> 00:00:41,320
the soup, actually. It might be a short one. The discussion might be short because it's

6
00:00:41,320 --> 00:00:48,000
just going to go over the four Janas. There's a couple of paragraphs that are interesting.

7
00:00:48,000 --> 00:01:02,640
So yeah, I guess we can just start.

8
00:01:02,640 --> 00:01:30,660
So

9
00:03:00,660 --> 00:03:13,540
I'm not the son of a bi-number, taini tainu, kambi, bia, pada, puedo, sa, chitit, tambari, sordi, taina,

10
00:03:13,540 --> 00:03:25,540
mita, baha, ya, ya, taina, mita, vita, rati, alo, kasani, sato, sambajana, taina,

11
00:03:25,540 --> 00:03:39,980
mita, jit, tambari, sordi, taina, jakoku, chambari, ya, bai, anu, dato, ya, taina, tainu,

12
00:03:39,980 --> 00:03:47,660
pasanta, jit, tau, dajaku, ku, ku, chajit, tambari, sordi, taini,

13
00:03:47,660 --> 00:04:00,620
vita, ki, chambaha, ya, taina, vita, jit, javi, hada, tain, kati, kusaly, sudamis, vita, kit, jai,

14
00:04:00,620 --> 00:04:08,980
ya, tain, palli, sordi, taini, sordi, taini, sordi, sordi, taini, pangani, varani, baha, ya,

15
00:04:08,980 --> 00:04:21,660
jita, sordi, kilai, sappanya, ya, du, vali, karani, vita, kami, vita, kusaly,

16
00:04:21,660 --> 00:04:35,340
yita, kansawijarang, vita, jambi, tisan dang, pata, manjana, mupasanta, javi, lati,

17
00:04:35,340 --> 00:04:46,260
yita, mupasanta, tambi, tata, kata, nishi, vita, niti, pata,

18
00:04:46,260 --> 00:04:52,460
tata, kata, nang, jita, niti, pita, nati, vita, vita,

19
00:04:52,460 --> 00:05:04,380
vita, vita, vita, vita, kali, taini, sambu, wagawa, zwakato, bagawata,

20
00:05:04,380 --> 00:05:15,900
mus, vata, vita, numbagawato, sawa, kasambu, ti, puna, chaparambra, mana, biku, vita, kavi,

21
00:05:15,900 --> 00:05:26,420
tara, nang, vu, pasama, hanja, tamsa, pasa, dang, tita, sui, koda, bawa,

22
00:05:26,420 --> 00:05:35,060
vita, kama, vita, ramsa, madi, jambi, tisan, kam, tute, yanja, nang,

23
00:05:35,060 --> 00:05:44,500
pasa, mpa, javi, lati, yita, mupi, wagati, brahmana, vita, numbagawata,

24
00:05:44,500 --> 00:05:53,500
sawa, kasang, hoti, una, chaparambra, mana, biku, vita, javi, raga,

25
00:05:53,500 --> 00:05:59,260
opé, kakouja, vita, vita, sato, jasampa, jano,

26
00:05:59,260 --> 00:06:08,500
sukang, jakayin, a party, sambu, vita, ti, yang, jang, nariya, chichi khan, ti, vio,

27
00:06:08,500 --> 00:06:17,780
vita, kakou, sati, masu, gavi, hanja, diti, tati, nang, nupasampa, javi,

28
00:06:17,780 --> 00:06:23,420
hati, yita, vio, chiti, brahmana, vi, sukati,

29
00:06:23,420 --> 00:06:30,220
pano, vaga, vato, sawa, kasang, ku, vu, nachaparam, brahmana,

30
00:06:30,220 --> 00:06:36,460
viku, sukas, sitchapahana, dukas, sitchapahana,

31
00:06:36,460 --> 00:06:42,740
vu, vi, uasomana, sado, manasana, hangama,

32
00:06:42,740 --> 00:06:47,780
adu, kamasu, khan, vu, vi, kasapipah, diti,

33
00:06:47,780 --> 00:06:54,380
jaiin, chateau, pang, jan, nupasampa, javi, hadati,

34
00:06:54,380 --> 00:07:01,420
yita, vu, chati, brahmana, takau, gatau, niti, diti, takau,

35
00:07:01,420 --> 00:07:08,900
gatau, niti, pita, takau, gatau, niti, pita, niti,

36
00:07:08,900 --> 00:07:15,100
nati, vato, aaliya, sawa, ku, niti, tang, gatati,

37
00:07:15,100 --> 00:07:21,380
samasampa, dobagawa, sawa, kato, vaga, vato,

38
00:07:21,380 --> 00:07:27,860
dama, sukati, pano, vaga, vato, sawa, pisan, goti,

39
00:07:27,860 --> 00:07:32,780
sawa, hangama, hiti, jiti, pari, sukati,

40
00:07:32,780 --> 00:07:38,020
pari, ua, dati, anang, aali, vigatau, paki,

41
00:07:38,020 --> 00:07:42,380
lei, simundu, vu, tai, kama, niti,

42
00:07:42,380 --> 00:07:46,860
titi, anang, japa, tai, vu, vai, niti,

43
00:07:46,860 --> 00:07:52,660
was anu, sa, tini, na, na, ya, titi, tama, bini,

44
00:07:52,660 --> 00:07:56,940
na, niti, sawa, niti, kavi,

45
00:07:56,940 --> 00:08:01,540
lam, pum, vini, wasam, anu, sara, ti,

46
00:08:01,540 --> 00:08:07,100
sa, yati, aang, gi, kum, pi, jati, vi, pi, jati,

47
00:08:07,100 --> 00:08:12,860
o, vi, iti, sa, karans, aud, sam, na, niti,

48
00:08:12,860 --> 00:08:18,740
kavi, tam, pum, vini, wasam, anu, sara, ti,

49
00:08:18,740 --> 00:08:24,740
yi, dam, vi, ua, jati, ram, anu, niti, pi,

50
00:08:24,740 --> 00:08:28,540
tatag, tani, sa, vi, tam, niti, pi,

51
00:08:28,540 --> 00:08:33,740
tatag, tai, rang, jitam, yi, pi, na, vai,

52
00:08:33,740 --> 00:08:38,740
vatag, rai, sawa, kol, niti, kum, gatati,

53
00:08:38,740 --> 00:08:42,700
sam, sambu, do, vag, wah,

54
00:08:42,700 --> 00:08:46,660
sawa, kato, vag, watag, dam, mosu,

55
00:08:46,660 --> 00:08:51,140
bartipan, do, vag, wados, au, wakasam, go.

56
00:08:51,140 --> 00:08:55,300
So, au, hai, vang, sa, mahi, tai, jiti, pai, ri,

57
00:08:55,300 --> 00:08:59,780
sunde, pasi, au, dati, anang, niti,

58
00:08:59,780 --> 00:09:04,260
wakat upakil, au, sambu, do, vu, tai, kam,

59
00:09:04,260 --> 00:09:06,940
niti, au, niti, aa, niti, au, niti, au,

60
00:09:06,940 --> 00:09:14,460
niti, au, niti, au, niti, au, niti,

61
00:09:14,460 --> 00:09:19,740
jiti, tam, vini, niti, zu, dim,

62
00:09:49,740 --> 00:10:04,060
tanga chatee, samasamadha bhagawa, sahatah bhagawatadha mosupatipana, bhagawatha, sahakasangati.

63
00:10:04,060 --> 00:10:17,900
So, I want samahi teh chatee parisudha paryodha teh, anangarneh wigatupa kilese mundu vu teh kamaneh,

64
00:10:17,900 --> 00:10:26,860
tehanin jakHat Aayyah ca narayan nahi calls

65
00:10:26,860 --> 00:10:27,160
qitha blocked tea ma vi�valsh

66
00:10:27,160 --> 00:10:30,140
kaij sangamabe nahi yes

67
00:10:30,140 --> 00:10:35,860
courseva indalfta vu tanpajana tea

68
00:10:35,860 --> 00:10:27,180
kaijandhu ca samaleott

69
00:10:27,540 --> 00:10:40,160
haha

70
00:10:40,180 --> 00:10:46,500
samphajana tea

71
00:11:16,500 --> 00:11:24,780
I am also when you rule the garmini, party, party, party at the Buddha, pajana, tea.

72
00:11:24,780 --> 00:11:32,420
I don't be with chatigram or not, at the Buddha, but I'm a DP, that I get down, you see,

73
00:11:32,420 --> 00:11:42,140
with a new DP, tada, dada, rangitam, you'd be, not way, but I worry as I walk only,

74
00:11:42,140 --> 00:11:56,100
tangato, hooti, a bicho, nitang, a chati, samasamudobagawa, zwakato, bagawata, dama, zwapartipan,

75
00:11:56,100 --> 00:12:08,780
bagawatosa, wakasangoti, tasa, yuang dana, toi, yuang basa, toka, masa, pichitang,

76
00:12:08,780 --> 00:12:28,580
wimodjati, basa, wapititang, wimodjati, wimodjati, wimodjami, tinya, namodi, kinajati, wimodjati,

77
00:12:28,580 --> 00:12:40,540
kinajati, kinajati, kinajati, bajana, tinya, kinajati, yuang, bichati, ramodjati,

78
00:12:40,540 --> 00:12:50,060
tada, nititipan, tada, kinajati, you'd be, tada, you'd be, tada, you'd be, tada, you'd be,

79
00:12:50,060 --> 00:13:05,980
tada, yuang, kinajati, samasamudobagawa, zwakato, bagawata, dama, zwapartipan,

80
00:13:05,980 --> 00:13:21,900
namodjati, zwapartipan, kinajati, kinajati, kinajati, kinajati, kinajati, kinajati, kinajati,

81
00:14:21,900 --> 00:14:41,900
So now we're in the stomach, should we do it quite quickly?

82
00:14:41,900 --> 00:14:44,900
The first bit is kind of interesting, so go ahead.

83
00:14:44,900 --> 00:14:50,900
So again, first of all, again we're continuing on from after morality,

84
00:14:50,900 --> 00:14:58,900
what is starting to give some background on the practice that leads one to be capable of experiencing

85
00:14:58,900 --> 00:15:04,900
and determining whether to know that this teaching is really the true teaching.

86
00:15:04,900 --> 00:15:13,900
So just like whether you can judge when and how you can judge whether the elephant that you're tracking is really a bull elephant,

87
00:15:13,900 --> 00:15:16,900
really the head that's heard.

88
00:15:16,900 --> 00:15:22,900
Possessing this aggregate of noble virtue and this noble restraint of the faculties

89
00:15:22,900 --> 00:15:26,900
and possessing this noble mindfulness and full awareness,

90
00:15:26,900 --> 00:15:32,900
he resorts to a scheduled resting place to a secluded resting place,

91
00:15:32,900 --> 00:15:39,900
the forest, the root of a tree, a mountain, a ravine, a hillside cave,

92
00:15:39,900 --> 00:15:45,900
a charnel ground, a jungle thicket, an open space, a heap of straw.

93
00:15:45,900 --> 00:15:52,900
It doesn't often use this long form of...

94
00:15:52,900 --> 00:16:01,900
It must be elsewhere, but it's not the standard phrase to use for giving all these options.

95
00:16:01,900 --> 00:16:09,900
Going to the forest, root of a tree, a mountain ravine, doesn't even mention a cookie.

96
00:16:09,900 --> 00:16:16,900
So the idea is here that the best thing would be to just go up and live in nature.

97
00:16:16,900 --> 00:16:19,900
Or preferably whether it's not too many mosquitoes.

98
00:16:19,900 --> 00:16:22,900
Mother's people who give you food.

99
00:16:22,900 --> 00:16:26,900
There's people who give you food, but that's not so difficult to find.

100
00:16:26,900 --> 00:16:32,900
You don't need that much food, especially when you're just by the dating.

101
00:16:32,900 --> 00:16:40,900
So the way of thinking of this is the time during the day when the person is meditating.

102
00:16:40,900 --> 00:16:47,900
So it's not necessarily referring to the dwelling place.

103
00:16:47,900 --> 00:16:51,900
Once one has lived in the monastery and undertaken morality,

104
00:16:51,900 --> 00:16:54,900
then one should leave the monastery and go off into the forest

105
00:16:54,900 --> 00:16:58,900
or go off into a place suitable outside of the monastery,

106
00:16:58,900 --> 00:17:03,900
but nearby, so one may actually be staying in a good day somewhere.

107
00:17:03,900 --> 00:17:08,900
But quite often they would just live in maybe in robe tents or something.

108
00:17:08,900 --> 00:17:09,900
Turn their tents into robes.

109
00:17:09,900 --> 00:17:10,900
It was a common thing.

110
00:17:10,900 --> 00:17:12,900
Apparently Sarah put to the dead one.

111
00:17:12,900 --> 00:17:14,900
Turn their robes into tents.

112
00:17:14,900 --> 00:17:17,900
Okay, put one of the robes up like on a cord or something.

113
00:17:17,900 --> 00:17:19,900
He said turn their tents into robes.

114
00:17:19,900 --> 00:17:22,900
Turn their robes into tents.

115
00:17:22,900 --> 00:17:24,900
I'm returning from his alms round.

116
00:17:24,900 --> 00:17:28,900
After a smell, he sits down, folding his legs crosswise,

117
00:17:28,900 --> 00:17:32,900
setting his body erect and establishing mindfulness before him.

118
00:17:32,900 --> 00:17:35,900
Abandoning covetousness for the world,

119
00:17:35,900 --> 00:17:38,900
he abides with the mind free from covetousness.

120
00:17:38,900 --> 00:17:41,900
He purifies his mind from covetousness.

121
00:17:41,900 --> 00:17:43,900
Abandoning ill will and hatred,

122
00:17:43,900 --> 00:17:46,900
he abides with the mind free from ill will,

123
00:17:46,900 --> 00:17:49,900
compassionate for the welfare of all living beings.

124
00:17:49,900 --> 00:17:52,900
He purifies his mind from ill will and hatred.

125
00:17:52,900 --> 00:17:54,900
Abandoning sloth and torpor,

126
00:17:54,900 --> 00:17:57,900
he abides free from sloth and torpor.

127
00:17:57,900 --> 00:17:59,900
Percipients of light,

128
00:17:59,900 --> 00:18:00,900
mindful and slowly aware,

129
00:18:00,900 --> 00:18:03,900
he purifies his mind from sloth and torpor.

130
00:18:03,900 --> 00:18:06,900
Abandoning restlessness and remorse,

131
00:18:06,900 --> 00:18:09,900
he abides unagitated with a mind and really peaceful.

132
00:18:09,900 --> 00:18:12,900
He purifies his mind from restlessness and remorse.

133
00:18:12,900 --> 00:18:14,900
Abandoning doubt,

134
00:18:14,900 --> 00:18:16,900
he abides having gone beyond doubt.

135
00:18:16,900 --> 00:18:18,900
I'm perplexed about wholesome states.

136
00:18:18,900 --> 00:18:20,900
He purifies his mind from doubt.

137
00:18:20,900 --> 00:18:23,900
Okay, some two parts.

138
00:18:23,900 --> 00:18:27,900
The first part is discussing how the person should sit

139
00:18:27,900 --> 00:18:30,900
during meditation, which is common.

140
00:18:30,900 --> 00:18:34,900
So, first thing first is you have to eat first, right?

141
00:18:34,900 --> 00:18:36,900
It's not true.

142
00:18:36,900 --> 00:18:39,900
It's not doesn't mean you can't meditate before you've had your one meal

143
00:18:39,900 --> 00:18:41,900
because monks are beating one meal a day.

144
00:18:41,900 --> 00:18:43,900
It doesn't mean you can't meditate before that,

145
00:18:43,900 --> 00:18:46,900
but meditation always goes better after the food

146
00:18:46,900 --> 00:18:49,900
because it's the one food that we'd have during the day.

147
00:18:49,900 --> 00:18:53,900
And after that, then you have the energy

148
00:18:53,900 --> 00:18:55,900
to continue in the meditation.

149
00:18:55,900 --> 00:18:57,900
So, folding a legs crosswise

150
00:18:57,900 --> 00:19:00,900
would be ideally sitting in the full lotus position.

151
00:19:00,900 --> 00:19:04,900
That's what that's referring to with your feet actually up on your hips.

152
00:19:04,900 --> 00:19:08,900
Setting your body erect means actually getting the spine aligned

153
00:19:08,900 --> 00:19:12,900
one bone on top of the other.

154
00:19:12,900 --> 00:19:17,900
Establishing mindfulness before him.

155
00:19:17,900 --> 00:19:22,900
Now, this is not, we often kind of trivialize this

156
00:19:22,900 --> 00:19:25,900
because we're not expecting the meditators

157
00:19:25,900 --> 00:19:28,900
to get powerful states of concentration.

158
00:19:28,900 --> 00:19:30,900
So, we'd suggest that you sit comfortably

159
00:19:30,900 --> 00:19:31,900
if that means slouching a little bit.

160
00:19:31,900 --> 00:19:33,900
It's not really a problem.

161
00:19:33,900 --> 00:19:39,900
So, obviously, these are not in any way related to understanding reality

162
00:19:39,900 --> 00:19:44,900
as it is, but they are related to the acquisition of concentration

163
00:19:44,900 --> 00:19:47,900
of tranquility.

164
00:19:47,900 --> 00:19:50,900
So, if you're practicing trying to gain tranquility first,

165
00:19:50,900 --> 00:19:53,900
there's heavy states of these strong states of calm,

166
00:19:53,900 --> 00:19:55,900
which is going to talk about here,

167
00:19:55,900 --> 00:20:01,900
then you really do need to take care of such things as food and posture.

168
00:20:01,900 --> 00:20:04,900
There's neither even food isn't necessary for,

169
00:20:04,900 --> 00:20:08,900
or isn't such a big deal for the attainment of insight

170
00:20:08,900 --> 00:20:11,900
but for tranquility at some point, it's quite important.

171
00:20:11,900 --> 00:20:13,900
So, then he talks about what he's talking about here

172
00:20:13,900 --> 00:20:15,900
with five hindrances from here on a new band.

173
00:20:15,900 --> 00:20:18,900
And the five hindrances,

174
00:20:18,900 --> 00:20:21,900
abide free from the five hindrances

175
00:20:21,900 --> 00:20:25,900
which appear fine in mind from the five hindrances.

176
00:20:25,900 --> 00:20:32,900
They give each one as its own little unique phrase.

177
00:20:32,900 --> 00:20:37,900
And the co-dessness means desires for the worldly things.

178
00:20:37,900 --> 00:20:41,900
Ill will, as to with hatred,

179
00:20:41,900 --> 00:20:43,900
no has to do with compassion,

180
00:20:43,900 --> 00:20:45,900
so when it becomes compassionate,

181
00:20:45,900 --> 00:20:48,900
it's not in torpor, when it becomes percipient of light.

182
00:20:48,900 --> 00:20:50,900
Percipient of light is,

183
00:20:50,900 --> 00:20:55,900
it's something that the Buddha talks about.

184
00:20:55,900 --> 00:20:56,900
It's a curious statement.

185
00:20:56,900 --> 00:20:59,900
It has to do with having a bright mind.

186
00:20:59,900 --> 00:21:08,900
So, there's a place for the Buddha talks about when it's night

187
00:21:08,900 --> 00:21:12,900
having the perception that it's like daytime,

188
00:21:12,900 --> 00:21:18,900
somehow giving rise to energy.

189
00:21:18,900 --> 00:21:22,900
Which is why often we encourage to keep our lights on.

190
00:21:22,900 --> 00:21:25,900
Turn your lights on when you're doing meditation.

191
00:21:25,900 --> 00:21:27,900
It stops you from falling asleep.

192
00:21:27,900 --> 00:21:29,900
Have a way of cheating.

193
00:21:29,900 --> 00:21:37,900
I can't help it at night.

194
00:21:37,900 --> 00:21:41,900
Some darkness triggers something in the mind.

195
00:21:41,900 --> 00:21:43,900
For us to assist in a remorse,

196
00:21:43,900 --> 00:21:47,900
one becomes unagitated and peaceful and working.

197
00:21:47,900 --> 00:21:49,900
Abandoning doubt,

198
00:21:49,900 --> 00:21:51,900
one becomes un perplexed about wholesome state.

199
00:21:51,900 --> 00:21:53,900
Someone knows what's good and what's bad

200
00:21:53,900 --> 00:21:57,900
and doesn't have any doubt about them.

201
00:21:57,900 --> 00:22:02,900
So, this would have to do with repeated application of the meditation practice,

202
00:22:02,900 --> 00:22:04,900
whether it's inside or tranquility.

203
00:22:04,900 --> 00:22:05,900
It doesn't really matter.

204
00:22:05,900 --> 00:22:08,900
The point is that you continuously apply the mind

205
00:22:08,900 --> 00:22:13,900
until the mind straightens out and the mind begins to get into the habit of

206
00:22:13,900 --> 00:22:15,900
having freedom from these five things.

207
00:22:15,900 --> 00:22:17,900
And if you're practicing insight, then it leans to insight.

208
00:22:17,900 --> 00:22:21,900
You're practicing tranquility, then it leans as talks about here.

209
00:22:21,900 --> 00:22:24,900
It leads first to all the states of tranquility.

210
00:22:24,900 --> 00:22:26,900
So, you want to.

211
00:22:26,900 --> 00:22:30,900
Having vessel ban in these five hinders is imperfections of the mind

212
00:22:30,900 --> 00:22:32,900
that we can wisdom.

213
00:22:32,900 --> 00:22:36,900
It's included for potential pleasures, secluded from an wholesome state.

214
00:22:36,900 --> 00:22:39,900
He enters upon and abides in the first genre,

215
00:22:39,900 --> 00:22:42,900
which is accompanied by applied and sustained thoughts,

216
00:22:42,900 --> 00:22:45,900
with rapture and pleasure-abornist occlusion.

217
00:22:45,900 --> 00:22:49,900
This, Brahman, is called a footprint of the tatagata,

218
00:22:49,900 --> 00:22:51,900
something scraped by the tatagata,

219
00:22:51,900 --> 00:22:53,900
something marked by the tatagata,

220
00:22:53,900 --> 00:22:57,900
but a noble disciple does not yet come to the conclusion.

221
00:22:57,900 --> 00:22:59,900
The blessed one is fully enlightened.

222
00:22:59,900 --> 00:23:01,900
The dhamma is well proclaimed by the blessed one.

223
00:23:01,900 --> 00:23:04,900
The sangha is practicing the good way.

224
00:23:04,900 --> 00:23:07,900
So, if you remember what the original simile was,

225
00:23:07,900 --> 00:23:09,900
that when you see the big footprint,

226
00:23:09,900 --> 00:23:14,900
you should come to the conclusion that that's a bull elephant.

227
00:23:14,900 --> 00:23:15,900
And the moon is like,

228
00:23:15,900 --> 00:23:16,900
well, actually, even with bull elephants,

229
00:23:16,900 --> 00:23:17,900
that's not always the case.

230
00:23:17,900 --> 00:23:21,900
Because it could still be a she-elephant, a very big she-elephant.

231
00:23:21,900 --> 00:23:25,900
And even when you see the markings high up of the tusks,

232
00:23:25,900 --> 00:23:27,900
you don't think that only a bull elephant can do that,

233
00:23:27,900 --> 00:23:31,900
because young elephants can also be very big,

234
00:23:31,900 --> 00:23:34,900
and stand up on their hind legs and so on.

235
00:23:34,900 --> 00:23:40,900
So, the same goes with the jhana as one doesn't come to the conclusion

236
00:23:40,900 --> 00:23:43,900
that these are assigned,

237
00:23:43,900 --> 00:23:46,900
that the teacher of these things is enlightened,

238
00:23:46,900 --> 00:23:48,900
or that this practice is the practice to enlighten them.

239
00:23:48,900 --> 00:23:53,900
Because people outside of the Buddhist teaching can also cultivate these things.

240
00:23:53,900 --> 00:23:55,900
It's not unique to Buddhism by any means.

241
00:23:55,900 --> 00:24:00,900
It's a characteristic of tranquility meditation,

242
00:24:00,900 --> 00:24:05,900
which is practiced in many different religious and spiritual traditions.

243
00:24:05,900 --> 00:24:07,900
We've talked about the jhana.

244
00:24:07,900 --> 00:24:10,900
So then the second jhana is the same.

245
00:24:10,900 --> 00:24:11,900
The third jhana is the same.

246
00:24:11,900 --> 00:24:13,900
The fourth jhana is the same.

247
00:24:13,900 --> 00:24:16,900
We've talked about these before.

248
00:24:16,900 --> 00:24:18,900
We've read through these before anyway.

249
00:24:18,900 --> 00:24:19,900
This is none of these.

250
00:24:19,900 --> 00:24:21,900
None of these are a reason to come to a conclusion

251
00:24:21,900 --> 00:24:22,900
that the Buddha is enlightened.

252
00:24:22,900 --> 00:24:24,900
But the final one?

253
00:24:24,900 --> 00:24:26,900
Now, then we have the births.

254
00:24:26,900 --> 00:24:28,900
We've done these as well, right?

255
00:24:28,900 --> 00:24:30,900
We've talked about reflecting minimal.

256
00:24:30,900 --> 00:24:33,900
Even when you remember past lives, we don't consider it then.

257
00:24:33,900 --> 00:24:37,900
Knowledge is passing away in the career appearing of beings.

258
00:24:37,900 --> 00:24:42,900
Understands of being past the way we've read on this before another sentence.

259
00:24:42,900 --> 00:24:45,900
So, remembering past lives,

260
00:24:45,900 --> 00:24:48,900
and seeing beings arise and pass away,

261
00:24:48,900 --> 00:24:51,900
die from one birth and one born in another birth.

262
00:24:51,900 --> 00:24:54,900
Also, not a sign of enlightenment.

263
00:24:54,900 --> 00:24:56,900
A characteristic of enlightenment,

264
00:24:56,900 --> 00:24:58,900
but the last one is we have it.

265
00:24:58,900 --> 00:25:00,900
Yeah, this one here.

266
00:25:00,900 --> 00:25:03,900
When his concentrated mind is thus purified,

267
00:25:03,900 --> 00:25:06,900
right, unblemished, rid of imperfection,

268
00:25:06,900 --> 00:25:09,900
malleable, willy, steady,

269
00:25:09,900 --> 00:25:15,900
and attained to imperturbability and perturbability,

270
00:25:15,900 --> 00:25:19,900
he directs it to knowledge of the destruction of the taints.

271
00:25:19,900 --> 00:25:22,900
He understands, as it actually is,

272
00:25:22,900 --> 00:25:25,900
this is suffering, this is the origin of suffering,

273
00:25:25,900 --> 00:25:28,900
this is the cessation of suffering,

274
00:25:28,900 --> 00:25:30,900
this is the way leading to the cessation of suffering,

275
00:25:30,900 --> 00:25:32,900
these are the taints,

276
00:25:32,900 --> 00:25:34,900
this is the origin of the taints,

277
00:25:34,900 --> 00:25:36,900
this is the cessation of the taints,

278
00:25:36,900 --> 00:25:39,900
this is the way leading to the cessation of the taints.

279
00:25:39,900 --> 00:25:42,900
Just notice that the user is the same formula for suffering,

280
00:25:42,900 --> 00:25:44,900
as he does for the taints,

281
00:25:44,900 --> 00:25:48,660
and in the some Deer Soupt remains

282
00:25:48,660 --> 00:25:54,420
several of them, 12, I think different things. So you see that the formula is the important

283
00:25:54,420 --> 00:26:01,340
thing. What is the problem? And what is the cause of the problem? What is the solution

284
00:26:01,340 --> 00:26:09,260
or the healthy state, the good state? And what is the path of least the good state?

285
00:26:09,260 --> 00:26:15,060
So this bad thing, cause of bad thing, good thing, cause of good thing, same here, bad

286
00:26:15,060 --> 00:26:21,980
thing, the teams, cause of bad thing, good thing, cause of good thing. The good thing

287
00:26:21,980 --> 00:26:29,420
is also the absence of bad thing. So you could say bad thing, cause of bad thing, cessation

288
00:26:29,420 --> 00:26:38,500
of bad thing and path leading to cessation of bad thing. So this one, now he's going to talk

289
00:26:38,500 --> 00:26:47,260
down here and what I think we didn't separate it to paragraphs. Okay, here's the point

290
00:26:47,260 --> 00:26:52,340
is that with the practice of some of the meditation, the mind becomes quite clear and

291
00:26:52,340 --> 00:26:58,140
bright. So it's a good thing to practice first. Practice it and your mind becomes imperturbable.

292
00:26:58,140 --> 00:27:00,700
Then you can direct it to the knowledge and destruction of the taste. You don't need to

293
00:27:00,700 --> 00:27:06,380
exactly. As long as you get to a state of imperturbability, it doesn't have to be based

294
00:27:06,380 --> 00:27:10,100
on all this stuff above. You don't have to remember your passage. It helps to all that

295
00:27:10,100 --> 00:27:16,620
stuff. Cause your mind becomes so strong. That's very quick to become enlightened. If you

296
00:27:16,620 --> 00:27:23,340
go straight to the goal, you just get enough to see things as they are. You don't have

297
00:27:23,340 --> 00:27:28,620
to go through all the loops and who they're amongst, who claim, who said that they didn't

298
00:27:28,620 --> 00:27:35,100
have these things and they still became our hearts. All you have to do is this one

299
00:27:35,100 --> 00:27:40,620
to understand suffering and the cause of suffering and the cessation of suffering. The

300
00:27:40,620 --> 00:27:47,780
way they need the cessation of suffering. Same with the taints. The taints are also, the

301
00:27:47,780 --> 00:27:53,140
taints are all the, all the defilements, all the attachments that we have, all the things that

302
00:27:53,140 --> 00:27:58,820
corrupt our mind and they all come from, stuff I'm done, from some kind of craving for

303
00:27:58,820 --> 00:28:08,660
things to be this way or that or to not be this way or that. Okay, and this one, this

304
00:28:08,660 --> 00:28:14,540
two Brahman is called a footprint of the taugata. Wait, wait, wait. It's, we're not there yet.

305
00:28:14,540 --> 00:28:19,780
So sorry, he doesn't yet. Wait, and go ahead. This one's different. Not different, but

306
00:28:19,780 --> 00:28:25,140
something marked by the taugata, but a noble disciple still has not yet come to the conclusion.

307
00:28:25,140 --> 00:28:29,380
The blessed one is fully enlightened. The dhamma is well proclaimed by a blessed one. The

308
00:28:29,380 --> 00:28:34,780
sangha is practicing the good way. Rather, he is in the process of coming to this conclusion.

309
00:28:34,780 --> 00:28:39,940
Okay, so we can put it, I'll let Bika Bodhi explain why this one's different. It's special,

310
00:28:39,940 --> 00:28:49,140
but it's not the most special yet. So this is the moment of the path coming to the conclusion.

311
00:28:49,140 --> 00:28:57,300
This is because there's one moment. The moment one one experiences, realizes the

312
00:28:57,300 --> 00:29:04,620
four noble truths, one hasn't become free. There's no, during that moment of realizing the

313
00:29:04,620 --> 00:29:11,020
truth, there's no knowledge of freedom. So it has to come to the next moment. That's what

314
00:29:11,020 --> 00:29:16,820
the commentary says about that. There's other ways, I think, to explain that, but that

315
00:29:16,820 --> 00:29:24,380
would be the canonical, the orthodox interpretation. Anyway, point is, once he knows and sees

316
00:29:24,380 --> 00:29:29,820
that, just reading the next one here. When he knows and sees us, his mind is liberated

317
00:29:29,820 --> 00:29:35,460
from the taint's essential desire, from the taint of being, and from the taint of ignorance.

318
00:29:35,460 --> 00:29:40,260
When it is liberated, there comes to knowledge. It is liberated. He understands, birth is

319
00:29:40,260 --> 00:29:45,220
destroyed. The holy life has been lived. What had to be done has been done. There is no more

320
00:29:45,220 --> 00:29:51,660
coming to any state of being. Let me go for the next point. Here is not the other. This

321
00:29:51,660 --> 00:29:57,100
two, Brahmin, is called a footprint of the tittagata, suddenly scraped by the tittagata, something

322
00:29:57,100 --> 00:30:02,940
marked by the tittagata. It is at this point that a noble disciple has come to the conclusion

323
00:30:02,940 --> 00:30:07,020
that blessed one is fully enlightened. The dhamma is what proclaimed by the blessed one.

324
00:30:07,020 --> 00:30:11,500
The sankaya is practicing the good way. And it is at this point, Brahmin, that to

325
00:30:11,500 --> 00:30:16,900
simulate the elephant's footprint has been completed in detail. At that moment of

326
00:30:16,900 --> 00:30:22,940
realization, that is when one is coming to the conclusion that this is the path to enlightenment.

327
00:30:22,940 --> 00:30:29,260
The next moment from that point on, one has already come to the conclusion that this

328
00:30:29,260 --> 00:30:36,140
is the path to enlightenment. This is the bull elephant of religion, of spiritual practice.

329
00:30:36,140 --> 00:30:46,420
This one has realized full and sound. I would like to say that. All of these are stop passages.

330
00:30:46,420 --> 00:30:50,140
And we got something a little bit new here. I don't think we read this one before.

331
00:30:50,140 --> 00:30:55,100
When this one said, well, this was said, the rama and jhanu sony said, the blessed one,

332
00:30:55,100 --> 00:31:00,620
magnificent master Godhama, magnificent master Godhama. Master Godhama has made the dhamma

333
00:31:00,620 --> 00:31:05,460
clear in many ways, as though he returning upright what had been overthrown, revealing

334
00:31:05,460 --> 00:31:10,780
what was hidden, showing the way to one who was lost, or holding up a lamp in the dark,

335
00:31:10,780 --> 00:31:17,020
for those with eyesight to see forms. I go to master Godhama for refuge and do the dhamma

336
00:31:17,020 --> 00:31:22,500
and to the sankaya of bhikkhu's. From today, let master Godhama remember me as a lay follower

337
00:31:22,500 --> 00:31:25,020
who has gone to him for refuge for life.

338
00:31:25,020 --> 00:31:32,500
This is a stock passage and it will change here. Everyone seems to say the same thing,

339
00:31:32,500 --> 00:31:39,220
exactly, but this changes. Some of them asked to go forth. Sometimes they will ask to

340
00:31:39,220 --> 00:31:45,460
go forth as a monk. And in this case, he just decided he just asked to become a lay follower,

341
00:31:45,460 --> 00:31:51,780
which is also common. Depends on who the person was. This is a stock passage. It's quite poetic

342
00:31:51,780 --> 00:31:57,500
and it's quite apt really because you feel that way when you practice that it feels like

343
00:31:57,500 --> 00:32:03,100
someone had just turned upright, what had been overthrown, flipped upside down, revealing

344
00:32:03,100 --> 00:32:08,260
what was hidden, suddenly showing the lighting up a lamp in the dark for those with eyesight

345
00:32:08,260 --> 00:32:09,260
to see.

346
00:32:09,260 --> 00:32:13,060
I notice he still calls him Godhama.

347
00:32:13,060 --> 00:32:31,860
That says the climbing. So that's the end of the julahatipadopamasut. So, you can

348
00:32:31,860 --> 00:32:44,540
hear him there. Hey, we weren't singing, except we are singing, chanting. Anyway, thank

349
00:32:44,540 --> 00:33:09,660
you all for tuning in and see you all next time.

