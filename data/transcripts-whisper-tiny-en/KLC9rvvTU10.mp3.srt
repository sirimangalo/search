1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study on the Damopada.

2
00:00:05,000 --> 00:00:11,000
Today we continue on with verse number 135, which means as follows.

3
00:00:11,000 --> 00:00:38,000
Just as a cow herd with a stick drives the cows to the cow pasture,

4
00:00:38,000 --> 00:00:43,000
or the cow cattle.

5
00:00:43,000 --> 00:00:51,000
So to A1, old age and death, Jara Majugja,

6
00:00:51,000 --> 00:00:59,000
old age and death drive the life of beings.

7
00:00:59,000 --> 00:01:01,000
Drive the age

8
00:01:01,000 --> 00:01:06,120
Drive our age on to on to old age and death. Drive us on

9
00:01:08,800 --> 00:01:10,800
The direct us

10
00:01:11,440 --> 00:01:13,440
They control us

11
00:01:14,680 --> 00:01:17,800
They are the master the cow heard they heard us

12
00:01:18,560 --> 00:01:21,040
We have nowhere to go but to the past years

13
00:01:21,040 --> 00:01:28,160
Dictated by old age and death. I can't avoid them like the cows can't avoid the cow heard

14
00:01:30,440 --> 00:01:31,800
so

15
00:01:31,800 --> 00:01:36,400
The story behind this another short story is about Risakha

16
00:01:37,320 --> 00:01:39,680
Buddhist chief female a disciple

17
00:01:42,040 --> 00:01:44,600
In the time it's about the

18
00:01:44,600 --> 00:01:50,960
The apostle satah, which would be the holy day and so in time of the Buddha in India

19
00:01:51,400 --> 00:01:54,920
It was tradition in Indian society to

20
00:01:55,720 --> 00:01:58,680
Consider the full moon as a holy day where you would

21
00:01:59,360 --> 00:02:01,360
undertake religious practices so for

22
00:02:02,480 --> 00:02:10,280
Brahmans or Hindus we know them. They would take it on themselves to worship whatever God they followed

23
00:02:11,280 --> 00:02:13,280
so in Buddhism

24
00:02:13,280 --> 00:02:18,920
And this was modified to suit Buddhist religious principles of

25
00:02:20,120 --> 00:02:24,320
Ethics morality so it'd be a time where people would take on advanced or

26
00:02:25,880 --> 00:02:34,120
More strict and serious ethical and moral practices of abstaining from things like entertainment romance

27
00:02:35,880 --> 00:02:37,880
beautification

28
00:02:37,880 --> 00:02:43,600
Sleep that kind of thing so they wouldn't dedicate themselves to the Buddha's teaching

29
00:02:45,120 --> 00:02:46,880
so

30
00:02:46,880 --> 00:02:49,880
We saw Kha would of course keep these

31
00:02:51,080 --> 00:02:56,680
precepts as a she was so tough on that so she was at already seen in Banga for the first time

32
00:02:57,200 --> 00:02:59,200
She was on her way to enlightenment

33
00:02:59,440 --> 00:03:02,880
So she was keeping them for the right reason, but she saw these other women

34
00:03:02,880 --> 00:03:04,880
And

35
00:03:06,240 --> 00:03:11,640
These women her women followers actually so they they've kept it with her and

36
00:03:12,480 --> 00:03:16,120
some of them were girls young girls some of them were

37
00:03:17,520 --> 00:03:22,520
newly wedged you know young women in the prime of life some of them were middle-aged

38
00:03:24,160 --> 00:03:26,160
Some of them were old

39
00:03:26,160 --> 00:03:31,760
I've seen yours who are already showing signs of old age

40
00:03:33,000 --> 00:03:35,000
Close to death even

41
00:03:35,000 --> 00:03:38,120
So I'll all range of ages and so she wondered

42
00:03:38,720 --> 00:03:42,280
Why these women were keeping she was somebody's supposed

43
00:03:43,360 --> 00:03:48,800
Suspicious you can imagine are they doing it for the right reasons? I bet she was asking in her mind

44
00:03:50,400 --> 00:03:54,200
And so she came to them and she asked the old women first

45
00:03:54,200 --> 00:04:00,360
So why are you keeping the oposita? Why are you keeping these precepts? Why do you come and

46
00:04:01,120 --> 00:04:06,520
Listen to the Buddha's teaching and so why do you take this holiday? What does this holiday mean to you?

47
00:04:08,360 --> 00:04:13,120
And they said oh well, we're old and the pleasures of life have faded and

48
00:04:13,720 --> 00:04:16,400
So we're looking for the pleasures of heaven

49
00:04:17,000 --> 00:04:23,080
We're hoping that through this practice. We've heard that it's great wholesomeness and that's what leads you to heaven

50
00:04:23,080 --> 00:04:25,400
So we figured that this is the way to go

51
00:04:27,640 --> 00:04:31,120
And then she went and asked the middle-aged women

52
00:04:32,520 --> 00:04:36,520
Why are you coming here to practice and keep the holy holiday?

53
00:04:37,640 --> 00:04:43,440
Keep the precepts and so on and they said oh, we're here to escape the

54
00:04:43,760 --> 00:04:45,760
the control of our husbands

55
00:04:45,760 --> 00:04:53,880
They're supposed it's a temporary escape being able to go leave the house to go to the monastery because of course women in

56
00:04:53,880 --> 00:04:58,280
India at the time and to some extent even today in traditional societies are

57
00:05:00,120 --> 00:05:02,120
While we're often little more than

58
00:05:03,440 --> 00:05:05,840
Servants or possessions of their husband

59
00:05:06,320 --> 00:05:11,800
So they're very much under their power, but when they came to the monastery, of course, they were it's an excuse to leave and

60
00:05:13,160 --> 00:05:15,160
potentially there there was a

61
00:05:15,160 --> 00:05:19,320
path out so if they practice spiritual teachings they could

62
00:05:19,840 --> 00:05:24,920
leave the household life and and become nuns or female nuns

63
00:05:26,120 --> 00:05:30,280
And so that was there that was why they were doing it completely just to get away from their husbands

64
00:05:30,840 --> 00:05:35,280
Or in many cases abusive or at least domineering

65
00:05:36,120 --> 00:05:38,120
controlling

66
00:05:38,120 --> 00:05:46,720
And then so she went on it. That's interesting. She went on and asked various young young women who were

67
00:05:47,120 --> 00:05:49,120
You know in the prime of their life

68
00:05:49,320 --> 00:05:51,320
married but

69
00:05:51,320 --> 00:05:52,600
still young

70
00:05:52,600 --> 00:05:55,760
So she wondered what they would say and they said oh, we're keeping it and

71
00:05:57,560 --> 00:06:00,200
Perhaps not entirely but by and large

72
00:06:00,200 --> 00:06:07,560
She found that they were keeping it for things like and it gives the example of having babies

73
00:06:08,280 --> 00:06:11,640
So it was a big deal among many of them that

74
00:06:13,160 --> 00:06:19,000
Religious practice was and to some extent still is in traditional Buddhist societies

75
00:06:19,560 --> 00:06:21,400
understood to

76
00:06:21,400 --> 00:06:26,040
facilitate childbirth like there's something special about the karma you could

77
00:06:26,040 --> 00:06:35,160
You could explain it. Now it's I think a lot of superstition obviously that's what it sounds like but

78
00:06:36,840 --> 00:06:40,760
The idea that it leads to to to child birth

79
00:06:41,800 --> 00:06:43,800
because of how

80
00:06:43,800 --> 00:06:45,800
Birth comes about it's not

81
00:06:46,440 --> 00:06:51,000
it's not just chance or or random acts

82
00:06:51,000 --> 00:06:59,080
Blind luck that a woman is able to conceive now it requires of course the male in the female

83
00:07:00,360 --> 00:07:03,880
But it also requires of mine that also requires a being that is

84
00:07:04,520 --> 00:07:06,520
coming to be reborn and

85
00:07:06,520 --> 00:07:09,560
So for a being to reborn be reborn as a human being

86
00:07:10,040 --> 00:07:16,280
They have to have a whole some qualities it takes something special to be born as a human being and so

87
00:07:16,280 --> 00:07:21,960
The idea is that it also requires something special something attractive

88
00:07:24,680 --> 00:07:30,600
To such a being so there's the idea that religious practice is somehow attractive and

89
00:07:31,960 --> 00:07:34,760
Therefore puts one in a in a state

90
00:07:35,640 --> 00:07:39,320
Where one is a receptive is receptive or is attractive

91
00:07:39,960 --> 00:07:41,960
to such a being

92
00:07:41,960 --> 00:07:47,720
Pleasant and peaceful you could also say that the mind that comes from spiritual practices

93
00:07:48,360 --> 00:07:50,360
both physically more

94
00:07:50,520 --> 00:07:53,080
Receptive to childbirth but also mentally so

95
00:07:53,800 --> 00:07:56,920
Any being that is interested in being born as a human being

96
00:07:57,560 --> 00:08:01,880
Will gravitate towards the minds that are at peace the minds that are wholesome

97
00:08:02,360 --> 00:08:07,240
So there is an argument. I think to be made that it probably increases your chances of

98
00:08:08,280 --> 00:08:11,080
not only of giving birth

99
00:08:11,080 --> 00:08:13,080
But of giving birth to a

100
00:08:14,040 --> 00:08:19,400
Child who has wholesome qualities on this right if you're an unwholesome person

101
00:08:19,880 --> 00:08:24,200
More likely that your children are good people that are the beings that are going to be attracted to your womb

102
00:08:25,000 --> 00:08:27,000
They're going to be equally unwholesome

103
00:08:27,720 --> 00:08:29,720
some food for thought

104
00:08:29,720 --> 00:08:32,760
But anyway, they had this belief and it's not a Buddhist

105
00:08:33,400 --> 00:08:37,400
These are not none of these are really the reason for the proper reason

106
00:08:37,400 --> 00:08:43,000
So she's at this point starting to shake her head and wondering if anybody's really doing it for the right reason

107
00:08:43,000 --> 00:08:45,000
These are not proper reasons to practice

108
00:08:46,520 --> 00:08:48,520
They're not bad or unwholesome

109
00:08:49,160 --> 00:08:51,160
But they're quite limited

110
00:08:51,160 --> 00:08:54,680
And so that's what the Buddha's going to talk about when she finally gets to him

111
00:08:55,080 --> 00:08:59,000
So she goes to the maidens the young girls and asks them, you know

112
00:08:59,000 --> 00:09:03,880
Why are you doing all these other ones have these two your motives? What's your reason? Maybe it's pure

113
00:09:03,880 --> 00:09:07,480
She's found by and large the unmarried women

114
00:09:08,440 --> 00:09:15,880
Why were they practicing because they they felt that spiritual practice would help them to obtain a husband?

115
00:09:16,680 --> 00:09:22,280
Well, they were still young which is kind of funny because the older women are trying to get away from their husbands and the younger women are

116
00:09:23,080 --> 00:09:27,560
wishing to get husbands now. It's not entirely contradictory or

117
00:09:31,320 --> 00:09:32,440
absurd

118
00:09:32,440 --> 00:09:34,440
because

119
00:09:34,440 --> 00:09:37,880
There's the idea there was the sense that it was actually worse

120
00:09:40,200 --> 00:09:42,200
Then whatever you might

121
00:09:42,520 --> 00:09:45,400
Face at the hands of a husband to be unmarried

122
00:09:45,800 --> 00:09:52,040
If you're unmarried then you're a burden on your family who would treat you as little better than a servant an unmarried woman

123
00:09:52,680 --> 00:09:54,760
in this society was not very well

124
00:09:54,760 --> 00:10:04,680
It was was the lowest of the low and disrespected in favor of women who were fruitful and didn't marry

125
00:10:05,480 --> 00:10:09,720
Or gave honor and wealth and status to their families

126
00:10:10,440 --> 00:10:16,600
So not getting married was it was probably a fate worse than getting married and neither one is all that pleasant

127
00:10:17,960 --> 00:10:20,520
This was the state of women in India at the time so

128
00:10:20,520 --> 00:10:28,520
So if I used to say they had their reasons now the these are not goals that are are unwholesome

129
00:10:29,560 --> 00:10:36,200
But they're limited and so shaking her head did not really understanding she went to the Buddha and said what's going on with these people

130
00:10:36,760 --> 00:10:41,400
Why don't they see you know, why don't they see the true benefit of spirituality?

131
00:10:41,400 --> 00:10:44,760
Why can't they see that and the Buddha told this taught this verse and he said

132
00:10:44,760 --> 00:10:48,760
you know

133
00:10:50,440 --> 00:10:56,680
Birth so here. This is the quote birth a old age sickness and death are like cowards with staves in their hands

134
00:10:57,400 --> 00:11:03,320
Birth sends them to old age old age sends them to sickness and sickness sends them to death

135
00:11:04,360 --> 00:11:07,640
These things cut life short as though they cut with an axe

136
00:11:08,840 --> 00:11:12,760
And he says but despite this there are none the desire absence of rebirth

137
00:11:12,760 --> 00:11:14,760
This was the most interesting part of the story

138
00:11:15,480 --> 00:11:17,480
It's an important point that he makes

139
00:11:18,840 --> 00:11:21,880
None none none none but we're saying

140
00:11:23,240 --> 00:11:24,200
No

141
00:11:24,200 --> 00:11:26,360
basically none more or less

142
00:11:27,880 --> 00:11:31,800
Nobody in the world desires absence of rebirth very few

143
00:11:32,360 --> 00:11:34,920
It's the idea rebirth is all they desire

144
00:11:34,920 --> 00:11:41,480
And then he tells this says this verse so not only rebirth

145
00:11:43,080 --> 00:11:45,080
and

146
00:11:45,080 --> 00:11:47,080
the word is

147
00:11:50,200 --> 00:11:52,680
Hmm, it's actually not rebirth what done

148
00:11:54,120 --> 00:11:56,120
but they

149
00:11:56,600 --> 00:11:59,080
What done doesn't mean rebirth it means

150
00:12:00,600 --> 00:12:02,600
existence

151
00:12:02,600 --> 00:12:04,120
and

152
00:12:04,120 --> 00:12:06,120
becoming I guess

153
00:12:09,240 --> 00:12:11,240
So the

154
00:12:13,720 --> 00:12:15,720
The idea is that

155
00:12:17,720 --> 00:12:19,720
Just a second

156
00:12:24,840 --> 00:12:27,480
Hmm, okay. Now what the here means the round

157
00:12:27,480 --> 00:12:32,600
So it's the cycle and desire this cycle nobody that's right

158
00:12:33,080 --> 00:12:36,760
so the cycle is a birth old age sickness and death and

159
00:12:38,440 --> 00:12:41,240
Nobody desires and end to it. They all desire this

160
00:12:42,600 --> 00:12:45,160
And it's kind of what you're seeing here. It's kind of a cycle the

161
00:12:45,960 --> 00:12:51,960
Everybody wants the next step. So an unmarried woman wants a husband and in fact, it's not only

162
00:12:51,960 --> 00:12:56,840
To avoid suffering. It's actually of course there's attachment and

163
00:12:58,040 --> 00:13:04,200
Young women desire husbands. It's a it's a thrill to for them the idea of having a husband

164
00:13:04,760 --> 00:13:09,640
It's a you know sort of culturally bred into them and young men desire a wife

165
00:13:10,680 --> 00:13:11,960
equally

166
00:13:11,960 --> 00:13:16,120
And then when you're married, what's the next step? Well, you desire a child and wouldn't that be wonderful?

167
00:13:16,680 --> 00:13:21,080
Of course there are reasons to desire a child beyond just the pleasure of it because

168
00:13:21,080 --> 00:13:28,760
A woman who is unfruitful is a shame and is barren

169
00:13:29,720 --> 00:13:35,320
Considered to be useless because that's all they were good for in some people's eyes

170
00:13:36,200 --> 00:13:38,200
but also on the other hand

171
00:13:38,360 --> 00:13:43,400
Women and then both desire children it's something that they look forward to so

172
00:13:43,400 --> 00:13:50,280
Yeah, so it's a continuation the next step and then the next step and then the next step and finally once you've you've

173
00:13:51,080 --> 00:13:55,640
Squeezed all the pleasure out of this life that you can get you want to do it all over again

174
00:13:57,080 --> 00:14:01,800
Where like Mahasi Saya knows as we're like ducks and chickens

175
00:14:02,600 --> 00:14:05,080
That squawk and fight and play

176
00:14:05,960 --> 00:14:08,520
Thinking that they have this long life ahead of them

177
00:14:09,240 --> 00:14:11,640
And they don't know in fact that they're going to be slaughtered

178
00:14:11,640 --> 00:14:14,840
At the end of their their growth

179
00:14:15,400 --> 00:14:19,480
They're just going to have their heads cut off and be cooked for someone's dinner

180
00:14:19,960 --> 00:14:21,960
But but they don't see it

181
00:14:21,960 --> 00:14:27,000
Assumens were like that. It's a similar situation. We don't pay attention

182
00:14:27,720 --> 00:14:29,720
to the fact that this is

183
00:14:29,960 --> 00:14:36,760
So their pleasure and our happiness is circumscribed and we're doing nothing to prepare ourselves for the inevitability of suffering

184
00:14:36,760 --> 00:14:43,400
You know if it's not as we get old and it's when when we're die we die and it was that when we die

185
00:14:43,400 --> 00:14:46,040
Then it's when we're reborn again, and we forget

186
00:14:47,240 --> 00:14:52,200
We forget not only of our past deaths, but we forget about in this life all the suffering we've gone through

187
00:14:52,840 --> 00:14:55,240
From childhood so people want to be born again

188
00:14:57,080 --> 00:15:02,280
Most people in modern society don't think about being reborn, but when they hear about it

189
00:15:02,280 --> 00:15:06,920
It's exciting to them. Oh, I can do it all over again. Wouldn't it be great?

190
00:15:07,800 --> 00:15:11,000
Instead of thinking oh, that's horrific. I have to do this all over again

191
00:15:11,880 --> 00:15:16,840
Most if or many if not most people will think oh, that's great. I can get to do this all over again

192
00:15:17,400 --> 00:15:19,480
Which is funny because we've gone through so much

193
00:15:20,200 --> 00:15:26,040
Stress and suffering for the most part in our lives, but for the most part we've forgotten all about it

194
00:15:26,840 --> 00:15:28,120
and so

195
00:15:28,280 --> 00:15:30,280
We've got this idea

196
00:15:30,280 --> 00:15:36,600
This this rosy idea of life that it's all fun and games that it's all pleasure and happiness

197
00:15:37,160 --> 00:15:42,120
Because we're not willing no one wants to dwell on the unpleasantness

198
00:15:44,280 --> 00:15:46,280
We

199
00:15:48,040 --> 00:15:52,200
We're unable to see the suffering so this is the general philosophy

200
00:15:52,520 --> 00:15:55,160
Now how it has to do with our meditation specifically

201
00:15:55,160 --> 00:16:00,120
The meditation helps you to see through this. It helps you to see the good and the bad

202
00:16:00,120 --> 00:16:05,480
It helps you see the pleasant and the unpleasant and it helps you to see these things objectively

203
00:16:05,480 --> 00:16:08,360
It helps you to see that the things that you cling to are

204
00:16:10,120 --> 00:16:14,760
Unstable unsatisfying and uncontrollable, but they're not what you think

205
00:16:15,480 --> 00:16:19,480
That the that happiness is not the default state

206
00:16:20,040 --> 00:16:22,600
Happiness is something that in fact you have to work for

207
00:16:22,600 --> 00:16:26,200
And if you don't work for it, you can eat it up and eat it up and it

208
00:16:27,080 --> 00:16:33,240
It eventually is consumed and all you're left with is the craving for more and you're always going to be subject to this

209
00:16:33,800 --> 00:16:41,240
Disappointment is restlessness this agitation of not getting that what you want that it impels you

210
00:16:42,360 --> 00:16:46,760
To seek it out further whether in this life or in being reborn somewhere else

211
00:16:48,200 --> 00:16:50,200
It's something we have to work

212
00:16:50,200 --> 00:16:56,520
It's something we have to strive for it someday that takes a lot of stress and and suffering just again

213
00:16:57,720 --> 00:17:03,240
And so this is what we don't see we forget. I mean another important

214
00:17:05,480 --> 00:17:07,480
Point is that

215
00:17:07,960 --> 00:17:12,600
Our attachment to things that we desire is not rational

216
00:17:13,160 --> 00:17:15,160
You know, it's not

217
00:17:15,640 --> 00:17:17,640
There's no

218
00:17:17,640 --> 00:17:20,600
And if you look at it clearly

219
00:17:21,320 --> 00:17:22,680
There's no

220
00:17:22,680 --> 00:17:24,280
Explanation

221
00:17:24,280 --> 00:17:28,840
For why we cling to these things. I mean a drug addict know that knows this clearly

222
00:17:28,840 --> 00:17:30,840
They're able to see because it's so extreme

223
00:17:31,240 --> 00:17:33,880
But it's not rational that they desire these drugs

224
00:17:34,680 --> 00:17:38,120
But they still desire them now a person who's addicted to ordinary things

225
00:17:38,120 --> 00:17:40,120
our addicted to music or food or

226
00:17:41,000 --> 00:17:43,000
Sexuality or so on

227
00:17:43,000 --> 00:17:50,440
It is less clear, but still upon examination sees that it all falls apart and

228
00:17:51,560 --> 00:17:54,520
Depending on how strongly they're diluting themselves

229
00:17:55,080 --> 00:17:56,680
They can actually

230
00:17:56,680 --> 00:18:00,520
Ignore it at least temporarily that there's no rational reason for

231
00:18:01,320 --> 00:18:08,680
obtaining again and again and again these these activities, you know like what is sexuality you're bumping to

232
00:18:08,680 --> 00:18:12,680
bags of fluid to you know bags of

233
00:18:14,680 --> 00:18:16,680
Disgusting fluids and

234
00:18:18,680 --> 00:18:21,960
Matting matter together fat and pus and blood and

235
00:18:22,760 --> 00:18:24,200
and

236
00:18:24,200 --> 00:18:29,960
Send news and tendons and so on and flapping them together because it stimulates the chemicals

237
00:18:30,200 --> 00:18:36,600
I mean if you take it apart. It's actually kind of silly white, you know delicious food. We're stimulate you're just stimulating

238
00:18:36,600 --> 00:18:38,600
these chemical

239
00:18:38,920 --> 00:18:41,960
reaction so you look at the food and it's beautiful and so

240
00:18:44,760 --> 00:18:48,600
Music and what is music? It's just this rhythmic

241
00:18:53,640 --> 00:18:59,640
Pressing on the eardrum right that creates a sort of a trance in our minds that is pleasant

242
00:18:59,640 --> 00:19:03,800
I mean, it's all stimulation and the end it all comes down to that and

243
00:19:03,800 --> 00:19:07,880
And and in the end all it brings is a

244
00:19:10,920 --> 00:19:16,600
Cultivation or a stimulation of the addiction cycle in the brain

245
00:19:17,160 --> 00:19:22,600
Which means that using the word addiction for all these things is proper because that's what they are

246
00:19:22,920 --> 00:19:31,080
They all come down to some sort of chemical stimulation or some kind of stimulation for the brain and hence the mind and

247
00:19:31,080 --> 00:19:33,080
and

248
00:19:33,320 --> 00:19:35,320
thereby stimulate you know the

249
00:19:35,320 --> 00:19:37,320
Increase the need and the desire

250
00:19:37,960 --> 00:19:42,600
For these things now them being impermanent and unpredictable

251
00:19:43,240 --> 00:19:48,440
They therefore can satisfy they therefore lead to dissatisfaction at times

252
00:19:50,280 --> 00:19:57,080
And so this is kind of what we suck is seeing because she's practiced meditation and she practices and she sees

253
00:19:57,080 --> 00:20:03,800
That these things are not there's no rational reason. These are not true happiness. These are not the true goal

254
00:20:05,080 --> 00:20:09,160
True happiness and true peace has to come from letting go it has to come from

255
00:20:09,800 --> 00:20:15,400
Freeing yourself from any kind of need so that you're you have no wants you have no needs you're

256
00:20:16,200 --> 00:20:23,560
Satisfied the only way to be satisfied is to let go. It's not to always get what you want because that's of course not possible

257
00:20:23,560 --> 00:20:31,080
So that's the Dhamapada for this evening. Thank you all for tuning in wishing you all the best

