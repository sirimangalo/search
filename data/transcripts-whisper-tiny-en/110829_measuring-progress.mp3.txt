So this is what I would like to talk about today, the ways we can measure our progress.
And there are different ways, lots of different ways that you can measure your progress.
I think it's a good question, I'm sure it's something that comes up in everyone's mind,
that's something that's always been an important question for me, how am I progressing
and to be able to find markers and ways of understanding and measuring my progress.
And first I think there's one qualifier that needs to be made and that's that we shouldn't
get too caught up in the idea of progress.
There are many senses of the word or many ways that the word or the concept of progress
can be misinterpreted and can be actually harmful to one state of mind because one begins
to develop expectations and develops self-loathing or guilt feelings for not living up
to one standard.
There was an interesting, I had an interesting thought in this regard yesterday or today
or sometime recently, because I was recently, I was given an article on Asperger's syndrome.
And so I did a little bit of reading about it and I think everyone who starts reading
about specific labels that we put on specific conditions immediately start to look at
yourself and to compare the symptoms with your own experience and in a sort of a hypochondriac
sort of way you begin to think, wow, I've got that symptom and I've got this symptom.
And at the same, so I was reading about this and it struck me that hey, maybe this explains
some of the problems that I've had in my life, this is a form of autism that's marked
by higher in the normal intelligence but difficulty interacting with other people, it's difficult
to be in society, anti-social behavior, inability to engage in social, ordinary social behavior,
difficulty with empathy and so on, among other things, impaired motor activity and so on.
But the point is not in regards to the condition actually.
What happened then is that in the past week or so, I've been called, I was called anti-social
but in a very negative sort of way and a sociopath who's actually called the sociopath
or knows it wasn't a direct insult but it was like I don't want to talk with the sociopath.
And I was thinking about these words and the funny thing is of course that as Buddhists
are especially as monks, we're not all that keen on being social, it's not really such
a great quality, if someone says, wow, you're very social, it's not really a great compliment
for a Buddhist meditator, kind of something that you think, yeah, I'm a little too social
here, I should go back and look inside, spend less time looking outward, more time watching
my own feet, less time watching other people's feet.
But you know, when a monk, specifically in this case a monk calls you anti-social, a fellow
Buddhist monk, you start to think, you know, maybe there's something wrong.
And then I thought back about this thing I had read on the internet about this label, the
condition, the term syndrome, it's called, or whatever, it's labeled, it's given.
And the funny thing is it made me realize how much of a, how much of a relief it is,
and what a crutch it is, it can be to be able to have a name for your condition.
And I thought, yeah, hey, maybe it's just that I'm autistic.
And I thought immediately, hey, wow, you know, now people won't have to hate me because
I'm anti-social, I can just tell them I'm autistic and they'll be like, oh, that's
okay.
In that case, it's no problem.
Well, it's an interesting dilemma then, no, the problem with this, of course, is that
it leads us to be, it leads us to be negligent as a result of identifying with the condition
but anyway, it was something struck me as relevant to sort of our progress on the path
that we pick up these labels or these ideas of where we're going and who we can become
and we often come to identify with a specific condition, in some cases it can be a condition
that we have and in other cases it can be a condition that we don't have.
And in the cases where it's a condition that we do have, we very quickly begin to identify
with it and if we have a name for it, even more so, it becomes a part of who we are.
And as I was thinking about this, you know, this idea of how it might be, it might be
very easy to identify with the condition, you know, when a person has cancer, it becomes
a part of who they are and it's always in their mind this idea that they have cancer
but, you know, there's so much more than the cancer, if a person say has cancer on their
liver, for example, you know, suddenly they've become nothing more than this spot on their
liver and that's suddenly this one little part of them is more important, far more important
than any other part and of course there are reasons there but the idea is that we can often
take things out of proportion and when we begin to identify with the condition or a state
of being even more so.
So when someone calls you antisocial or in this example, you know, it's very easy to then
feel guilty and start to identify with this and think that that somehow you have to change
yourself and you have to become something, either you have to become a different person when
we have a specific condition, you know, a person who is this or that, anyway, it can be
a cause for us to actually give rise to negative mind states.
And on the other hand, when we think of who we want to be, what we want to become, we
want our mind to be calm, we want our mind to be quiet, we want to be this or that, you
know, we begin to identify with something that we're not and feel guilty about the fact
that we don't have a particular trait.
So this is all in regards to how we can do, we can judge our progress just that we shouldn't
try to judge ourselves.
And the idea of progress should really be in terms of mind states, of course, everything
in Buddhism, our whole practice should be in terms of ultimate realities for a moment
to moment.
So we shouldn't think in terms of, I am this, I am that, I'm not this, I'm not that.
And take it as some kind of being that exists.
We should never think of ourselves in terms of being.
We should always look at the experience, it becomes, you know, much more dynamic.
And our practice becomes much more visceral, much more real than this idea that who I
want to become, you know, people will often have the idea that they want to become a
soot upon an extreme enter when they start to read about these states of being.
And so that's a wonderful thing, I mean, there's certainly, it's a great aspiration in
a sense, but it can become an obsession where you identify with this desire or this state,
the idea that I am not this.
And you'll never reach it because there's a dichotomy, there's the eye that is not this,
and then there's this, the state, and they can never meld because they're separate entities
in our mind, the idea of who I am, the idea of an enlightened being.
When you begin to, you know, when you break that down and simply look at reality, you
know, look at your defilements and so on, and then of course it's a gradual and it's
an inevitable process that the mind inevitably releases and let's go without any desire
or intention.
So just a warning in regards to the idea of how we progress, that our progress should
be measured in terms of mind states, and I'm going to try to go through it here.
So the first way we can measure our progress is in terms, it's all in terms of those things
that I talked about in the beginning.
First, in regards to our meritorious acts, so the goodness in our hearts, the first
one is are we generous, are we kind with other people, are we helpful with other people,
to other people, are family members, you know, when they need something, when they want
something, when they need help or assistance or even just kindness, are we able to give
it to them, are we helpful, helpful individual?
Do we do things in our daily life, is our way of life kind and helpful in generous?
The people that we do interact with, are we selfish towards them, are we kind and helpful
in caring and compassionate?
This is the first meritorious quality of mind, generosity is the positive goodness of
an individual, so you can look and see, you know, when you interact with other people,
what sort of mind states come up, do you think to do good things and do you speak in
ways that are beneficial, are the words that you use kind and caring and helpful and
compassionate and so on, or can you be a bit of a jerk sometimes, so can you do sometimes
say things that hurt other people, then cause suffering and stress for others, do you
act in ways that are beneficial towards others, when you spend your time working in ways
that help only yourself or when other people need help, do you help them, so this positive
side of goodness, is it there in your life, or do the acts you do generally tend to,
tend towards your own material wealth and well-being and benefit and so on, or are you
just lazy and not doing anything good for anyone?
Number two, are you moral, the negative side of goodness, are you able to abstain from
those things that are a cause for suffering, a cause for suffering for others, and a cause
for your own mental distress and guilt and so on, when the opportunity comes up to hurt
or to kill animals or other beings, do you refrain, in regards to other people's property,
do you take and leave them without sometimes, taking the last cookie from the jar, no in this
case it means taking other people's belongings, when it's theirs, taking it from them so
that they are without, that they are left negative, in regards to friendships and relationships,
are you moral, do you avoid, do you refrain from splitting other people up, refrain from
romantic engagement with people who are already engaged?
So when you meet a couple who are happy together, do you think about how you can break
them up and take over the relationship, or are you able to let go of this and be content
with relationship with you that you have, or even more, be content in yourself, be content
to be one and to be whole in and of yourself, to be complete, to be a complete person
and a refuge for all beings in partiality, so to be celibate in this sense, and where
is your level of morality, and do you lie and cheat others or are you honest, and these
can all be measured relatively speaking, so we can look at the past, are we better in this
sense, are we more virtuous, more generous, and the third one is our meditation, are we
meditating more, are we more meditative, when we eat, are we meditative, being aware of
the tastes and the sensations and the motions of the mouth, the throat, when we take
a shower, are we mindful of the showering of the washing, we brush our teeth, are we mindful
when we walk, are we mindful, and relatively speaking, are we able to engage in mindfulness
more than before, or perhaps less even, maybe it's still the same, do we practice more
meditation, when we do our walking and our sitting, are we actually aware of the movement,
are we aware of the movement of the foot, or is our mind up somewhere else, are we aware
this is lifting, this is placing, this is, so are we aware of the stomach moving, when
the stomach moves, are we aware of it, this is rising, this is falling, are we aware
of feelings, are we aware of our thoughts, are we able to see things clearly, more clearly
than before, these three are considered to be merit, or goodness, let's say goodness is
a better word, so there are three aspects, basic aspects of goodness, and you can look
at them in a relative sense, so are you more endowed with more goodness than before, or
less, or the same amount, and hopefully you can see that through your practice that
actually you have more of these things, and it's something that we should at this point
in the rainy season, and also at this point in our lives look at and try to adjust, and
develop more of these qualities, generosity, morality, and meditation, or mental development,
this is one way, another way of measuring our progress is in terms of our training of the
mind, it's quite similar actually, but it's a different layout, not talking in terms
of goodness now, but talking in terms of training, and in Buddhism we recognize three
trainings, the training in morality, the training in concentration in the training in
wisdom, so where are we in this respect, more morality we've already talked about, but
on a moment to moment level, in terms of the meditation are we able to refrain from
the bad, no intentions of the mind, when the idea, when the intention to do bad things
or just say bad things, or even when the intention to become distracted arises, are we
able to keep our minds focused, pull our minds back, when our mind does get distracted,
when we have greed and anger and delusion, do we find ourselves acting on these, you know,
maybe when we have pain, do we find ourselves immediately adjusting, because we don't
like it, or when we want something, do we find ourselves immediately getting up and quitting
our meditation in order to obtain it, this is in regards to morality and are we able
to pull ourselves back, when the mind starts to wander, when the, when the fireman
arises, when we have anger, arise or disliking, when we have wanting arise, are we able
to catch ourselves and pull ourselves back to a clear awareness of the experience, so when
we have pain and we get upset by it, we're able to say to ourselves upset, upset, disliking
disliking, until it becomes just pain again, and we're able to go back to saying pain,
being aware of it just as pain, or do we find that we instead get carried away, and when
the disliking comes and it becomes an upset and then we're no longer able to sit still and
we have to get up and quit, this is in regards to morality.
In regards to concentration, are we able to see things more clearly than before so that
we don't get as angry or as greedy or as deluded as before, so that these things don't
even arise, concentration, or let's say focus, and I've talked about this before, because
we don't have to have this strong concentration where our mind is compressed into one
little state, concentration can be, or focus can, Samadhi can simply mean a levelness
of mind, where the mind is at the right level, it's tuned, so simply means that you see
things clearly without getting upset about them, are you able to experience reality without
getting upset by it, so when you have pain, are you able to see it simply as pain, whereas
before immediately you would get upset and have to move, when you hear sounds, so we're
able to simply hear them as sounds, when you see lights and colors and pictures, when
you have any sort of experience, are you able to experience it just as it is, this is
the focus, is your focus right, or are you out of focus and therefore still giving rise
to liking and disliking, because this is really the fine tuning of our practice, our meditation
practice progresses, based on our ability to simply see things as they are, simply
experience, when a thought arises and we want this or want that, to be able to see the
thought simply for what it is and not give rise to the wanting, when our old habits
crop up, and we're able to see them for what they are, and not give rise to the emotions
of liking and disliking, so that we're able to be happy and at peace with the reality
of the experience, that there's no upset in the mind, there's this concentration, if we're
doing well, we can see relatively speaking, are we better in this, are we less angry
and less upset than before, are we less greedy and less addicted than before, if so
then it means we have better concentrate, better focus, better surminding, and the third
one in this category is, as I said, wisdom, so do we find ourselves able to understand
things better, really comes directly from being in focus, because once you're in focus,
then you do see things clearer, so not just that our mind is concentrated, but actually
that we see things as they are, and we're able to see things arising and see things,
so whereas before when something arose, when say the roof caves in, right away you get
upset, you start to think about all the, what to do, the roof cave in though I have to
get a contractor to come and it's going to cost me all this money, even just thinking
about the roof caving in, can be a real catastrophe, just the thought can be a horrible
thing, if I tell you, be careful that if I ask you what would you do, if the roof
caved in, just my asking you, can in many cases bring up all sorts of stress and suffering,
so you think about money that you have to spend, do you think about the rain and, speaking
of rain, some things in my bathroom, you think of all of the suffering, the damage that
it might be caused and so on, let alone if it actually happened, so when it does happen
or when catastrophes happen, if you get in a car accident, I told this story a long time
ago about a woman who actually got in a car accident, and this is an excellent indicator
of the arising of wisdom, she was on a bus and the bus, the bus had a moose and this
is in Canada, we have moose, and she, she was thrown out of the bus, I don't know other
people, maybe people died, I don't know, but she was thrown out of the bus and landed
flat on her back, on a rock, paralyzed I think, or in great pain, and from what I was
told, she found herself just lying there, saying to herself, just watching her stomach
until the ambulance arrived, and the paramedics or the ambulance workers, they were so impressed
and shocked, you know, how could she, she seemed so calm, and she wasn't upset at all,
I had a, I tell this story often about this woman in the airplane that I met who, simply
by saying to herself, afraid, afraid, she was able to experience the landing of the plane
without any kind of fear or upset, as the power of seeing things as they are is, it's
quite simple and it's, it's so, it's too simple in fact that people just, you know, fail
to appreciate it, there was the case with this woman on the airplane, because she was asking
me, she said, you know, what does that do, what good is that, you know, is that really,
what does, what's the point of saying to yourself, afraid, afraid, afraid, it's like, it's
like stupid, really, it sounds like something silly, as I said, no, well, you know, we
can argue, I can explain it to you, but the best thing is for you to just try it, it's
the great thing about the Buddha's teaching is that it's truth, it's true, it really
is reality, what the Buddha taught, it's, it's, it's very easy to sell, if you understand
it, if you understand the Buddha's, it's very easy to, to sell to other people, well,
you know, people are people and they're very slippery, they're very good at finding reasons
not to see things clearly, but, you know, you don't have to, you don't have to convince
them, you can say, if you want to understand, try for yourself, they say, I don't want
to try them, that's up to them, this is a, probably the, the most powerful thing about
the Buddha's teaching is that if you try, you'll see, so I get in all these arguments
with people and you just think, well, you know, it's, it's really quite pointless, you
know, we can argue and argue, but if you try, you'll see for yourself, if you look, you
see, so, so this is in regards to wisdom, we can ask ourselves, are we able to experience
things with more wisdom, whereas before if we got thrown out of a bus, now we get thrown
out of the bus and land on our back and we're able to be less upset about it, you know,
I'm broken bones and so on, when you get bitten by a snake and you find yourself less, less
worried about dying than before, I was in the, I was up on a mountain, you know, this far
away mountain and I got bitten by a snake and, no, it was, it wasn't exactly, it was
like, ah, I'm bitten by a snake, but then it was kind of funny actually, it's like, you
know, wow, what a, what an odd occurrence, right, it's not something, not an everyday occurrence
to be bitten by a snake and having your foot starts to swell up and so on.
So I found it kind of humorous actually and the funniest part was the doctors and the
worst part was the doctors is that they wanted me to go to the hospital and they wanted
me to wear this, anyway that's, I've told that story before I think, but the point being
that, wow, you know, there's something good there, obviously something good about the
meditation because I assumed before I would have been a lot more freaked out about it,
well, maybe not, but you know, you can see that this is one way, this is really the, the
highest, the best way that meditation helps us, the ability to see things as they are
and to experience the reality around us, just for what it is, okay, so that's set number
two, set number three is specifically in regards to the defilements.
So these are really, it's all overlapping, it's all the same thing, but it's just different
ways of looking at our, our, our progress.
So now there's the simplest way of looking at our progress, it's just looking directly
at our level of defilement, the things in our mind that are causing us suffering, do I
have addiction, do I have more addiction than before or do I have less addiction than
before and to see whether you're progressing, you can ask yourself this whether you have
less, you're less addicted to the things that you used to be addicted to, maybe before
you were passionate about this or passionate about that and now when you look at it, you're
able to see it more objectively, you know, that whereas before it was a cause of great
tension, this desire, the tension brought on by desire and now you're looking at it and
you're able to, to actually experience it and to say, okay, now I'm, you know, now I'm
seeing something, you know, I'm, I'm even wanting something and to just look at the
wanting to look at the seeing, to look at the thinking, when you think about something
that you want, to feel the pleasure of that comes often with wanting and as you look
at it clearer, now you can see that it's actually, the tension is reduced and the great
stress that's involved with the desire has been, been reduced to some extent and as a result
you have less desire overall.
So you find that rather than wanting things, you feel, you know, kind of content and
happy with the way things are, the way things are right now, you find, you know, maybe
you want something, but then you look at it and you say, well, yeah, so I want something
and then it disappears and you're happy again and so slowly you start to give this up.
You find yourself less angry than before, you know, in the same way really, when something
unpleasant comes up and you look at the unpleasant feeling, you look at the pain and the
headache or the unpleasantness.
You look at the experience itself and you feel the tension that's involved in your ordinary
way of experiencing it and so slowly you give it up and you start to see that actually
it's just an experience, so do you have less anger now than before?
And number three, do you have less delusion?
No, delusion is a difficult one though.
It's often people often ask, well, I understand greed, I understand anger, but what about
delusion?
And really that, you know, the funny answer is that that's actually part of the point is
that delusion is not understanding, the confusion.
And you have to ask, well, what is delusion like to sign that you have some delusion because
there's the confusion?
That's one sense, but there are fairly clear signs of delusion that we can recognize.
Much of delusion is the non-recognition, so at the moment when you're mindful there's
no delusion, but any other time when you're not mindful there's the non-recognition that's
a kind of delusion, but a more active and positive form of delusion, positive in a sense
of actually being recognizable, actually being an experience, or in terms of conceit, our
desire to oppress others, desire, but the oppressive intentions.
Bring yourself above others, self-righteousness, you know, when you're angry and then you feel
like you deserve to be angry that another person deserves to suffer, you know, it's good
to make people suffer and so on, this delusion, the intention to oppress others, or oppress
yourself.
As I said in the beginning, I was trying to explain about our clinging to ideas of who
we are when we get into a situation, these feelings of guilt are not being good enough or
feelings of depression or our views.
Views are a very important part of delusion and this is the interesting thing I was looking
at today because it seems to me and could be wrong, but I was having an argument with someone
about, I think I'm still I am, and I think they're probably still going to fight back, I
say something, they say something, it's not exactly a fight, it's an argument.
And I said to them, you know, this argument is really, you know, what's the point of this
argument and back and forth, but I said, you know, really we should just go and practice
meditation, but the, the, the, the interesting thing about the argument is that I say when
I practice meditation, I see something, you know, it becomes clear, something becomes clear
to me, particularly, it becomes clear to me that rebirth, the concept of rebirth, of being
born again or of the mind continuing on, to spell it out exactly though the, the nature
of the mind to continue on after the moment of physical death seems to me clearly evident.
Now for this other person, it wasn't clearly evident, they practice meditation and in fact
they said they see the same sorts of things and experience the same sorts of things that
I claim to experience.
And yet they said they, they, they still feel like rebirth is just a theory.
And to me, from my point of view, this seems to be the, the case that a view is getting
in their way.
And why I explain this, why I bring this example up is, is to sort of show how views work
because it's possible that you can see something.
It may not be the case with this person, they may be right and I may be wrong.
So it's, I don't want to hold myself up above them, not in this case.
But it can be the case that someone can look and see and still hold on to view.
So a person could practice meditation, see all sorts of interesting and, and new things
and still not really get it, it'll be blocked by what we call views.
So for example, if a person has a view of God, that God is our Savior and it is through
the grace of God, that one goes to eternal heaven or eternal hell.
This view is pernicious, I don't know, pernicious is the right word.
But given this, the, a, someone holding onto this view, if it's a pernicious view, it's
something that blocks one from, from seeing the truth, so that actually, even, even should
one practice, even intensive meditation is quite likely that they won't get to the, the
result or, or obtain the fruits of the practice.
This is also the case with people who desire to become a Buddha, incidentally, that there's
a state of mind involved with the desire to not become enlightened, that actually prevents
one from seeing things clearly as they are.
You will stop one from, from obtaining the results of the practice, which is sort of related
to views, but here specifically dealing with, with views unrelated to reality.
If you have the view of self, it's a pernicious one, if people have the view that there
is a self, then, well, this is absolutely contrary to what we expect people to see in the
meditation.
So even though they will see things arising and ceasing, they'll still cling to the
idea that there is a self below this.
If they cling to that idea, then, then it'll never hit them, that, you know, everything
is impermanent, it will never get through their mind because they always have in their
idea, in their mind the idea, and this gets in our way, it stops the process of the
mind from getting it and from giving up.
So this is, so this is in regards to delusion, and this is the third group of, of indicators.
You know, do we have less greed, less anger, less delusion, or do we in fact have the same
amount as before, or do we have more?
So we have, of measuring our progress, and I think these three are, are three very good indicators
of our progress.
It's important that we understand, as I said in the beginning, that progress is something
that should be understood based on mind states, based on, on individual mind states.
So we should never get the idea that we are something, or that we're not something.
Another thing that often happens is people get overconfident, they have experience to
state a meditation or even a period of meditation, where they don't give rise to a certain
mind state, that they get rise to before, so before they would get very angry, and now
they find a very peaceful and calm, before maybe they were addicted to something, and
now they find that they're not addicted to it, and as a result they think that that
experience has disappeared, but a part of, of taking things mind state after mind state
is the realization that the progress in the meditation is very, very gradual, and that
in fact what you'll experience is that after a while these states will come back.
So maybe before you've got very angry about something, now you're not angry, and then
suddenly boom you get angry again, and it's, it got to be the case that people think,
you know, that I'm really gaining nothing out of this practice, the same old habits,
mind states are arising, that I thought I was, I was through with.
This is why I caution people against the understanding that concentration is of greatest
important.
A, a concentrated state of mind is actually, can be quite deceiving, deceptive, it's possible
for someone to enter into a very focused, concentrated state of mind, and still not learn
anything about reality, and so as a result give rise to the same old mind states again,
and this is often what happens in meditation.
Part of the results are actually not results, they're just temporary suppression.
You're not actually getting anything out of the meditation, it's just a temporary relief,
as we tend to repress and suppress rather than experience and understand and let go of.
So we shouldn't, on the one end we shouldn't get over, over confident and we shouldn't
mistake the suppression of defilements with actually around actual eradication, but on
the other hand we should look at the relative nature of the mind state, so yes we get
angry about the same things that we got angry about before, but is it really on the same
level as before, to the same extent as before, because generally what you'll find is yes
the same mind states come back, but no, they're not as strong as they used to be, and
I'm actually less attached this time around.
It's the same old mind state, you know I thought I realized that this was useless and yet
here I am going over it again, but it's less, it's lessened.
This is the key, this is the reality of our practice, it's not magic, it's not like
poof, gone, oh that was, that was just a nice trick, it's not a trick, it's a slow gradual
process of changing your understanding, changing your habits based on the understanding,
based on a new understanding of reality that doesn't involve partiality, it doesn't involve
compartmentalizing reality into good and bad, acceptable and unacceptable and so on.
So that was the talk to give tonight and on probably way over time, I think I've lost a few
people here, but we still have seven viewers, so I think the next part of our program tonight,
which has been a bit of a patch together program, but the upside is now you get to see
me, great and you get to see my wonderful green room, so next part of the program will
be the meditation part, so you're welcome to log out, go home, go back to your room and
meditate, you're welcome to go on and do other things if you don't want to meditate with
us, but I think now, as a group, for those who are interested, we will do some group
meditation, so let's offer the talk.
