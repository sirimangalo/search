1
00:00:00,000 --> 00:00:07,000
Is Epsomite in this the opposite of mindfulness?

2
00:00:07,000 --> 00:00:11,200
If someone forgets to buy groceries on the way home, despite having planned, does it necessarily

3
00:00:11,200 --> 00:00:13,000
mean he is very less mindful?

4
00:00:13,000 --> 00:00:19,000
Do they somehow affect each other?

5
00:00:19,000 --> 00:00:23,000
I would say they affect each other, but this is the problem with the word septi.

6
00:00:23,000 --> 00:00:29,000
Again, you have to throw out the word mindfulness because it's not a translation of septi,

7
00:00:29,000 --> 00:00:39,000
but septi actually does mean one of the meanings of septi is to remember or to be able

8
00:00:39,000 --> 00:00:45,000
to remember the ability to, not just the ability, the remembering.

9
00:00:45,000 --> 00:00:50,000
So, yeah, more like the ability dealing with the ability to remember.

10
00:00:50,000 --> 00:00:58,000
In one sense, yes, this is exactly the definition of septi is the ability to remember

11
00:00:58,000 --> 00:01:04,000
such things, but that's not the meaning of septi from the point of view of the septi patana.

12
00:01:04,000 --> 00:01:12,000
septi patana means to remember the essential quality of the experience, to not get caught up

13
00:01:12,000 --> 00:01:17,000
in the proliferation, to not get caught up in the concepts.

14
00:01:17,000 --> 00:01:20,000
So, you see this, right?

15
00:01:20,000 --> 00:01:29,000
So, not get caught up in hand or white or pale or ugly or five.

16
00:01:29,000 --> 00:01:32,000
Those are all concepts.

17
00:01:32,000 --> 00:01:37,000
Septi means to experience the seeing.

18
00:01:37,000 --> 00:01:41,000
This is seeing.

19
00:01:41,000 --> 00:01:43,000
That's all it is.

20
00:01:43,000 --> 00:01:46,000
When I flex my hand, there's a feeling of flexing.

21
00:01:46,000 --> 00:01:50,000
So, thinking of it as a hand is the concept.

22
00:01:50,000 --> 00:01:54,000
So, septi, in that sense, the meaning of the word septi is very specific.

23
00:01:54,000 --> 00:02:03,000
It means the ability to remember the actual experience, not get lost, not let the mind go tangentially

24
00:02:03,000 --> 00:02:08,000
into the back of your head where you start thinking of it as a hand, as ugly hand,

25
00:02:08,000 --> 00:02:16,000
and why is my hand so white, and this white light may be, you know, that kind of thing.

26
00:02:16,000 --> 00:02:20,000
So, to just keep it as an experience.

27
00:02:20,000 --> 00:02:23,000
So, it's just a different usage of the word.

28
00:02:23,000 --> 00:02:25,000
So, no, it is too different.

29
00:02:25,000 --> 00:02:31,000
If you're talking in terms of meditation, septi, from the meditation, is very different from

30
00:02:31,000 --> 00:02:35,000
septi and the ability to remember things far away.

31
00:02:35,000 --> 00:02:46,000
So, there seems to be a recognition in by the commentaries that, or I think even by the Buddha,

32
00:02:46,000 --> 00:02:50,000
by the Buddha himself, that there's a relationship.

33
00:02:50,000 --> 00:03:00,000
That the ability to remember things is a part of the same mind.

34
00:03:00,000 --> 00:03:09,000
So, it supports the ability to recognize or to remember things as they are.

35
00:03:09,000 --> 00:03:11,000
It's sort of the same function.

36
00:03:11,000 --> 00:03:13,000
So, they do affect each other.

37
00:03:13,000 --> 00:03:18,000
People who are mindful tend to be able to remember things better.

38
00:03:18,000 --> 00:03:23,000
But just simply not the same thing, certainly.

