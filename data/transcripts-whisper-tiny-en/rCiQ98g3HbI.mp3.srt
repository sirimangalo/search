1
00:00:00,000 --> 00:00:06,000
Are the monks precepts binding for life?

2
00:00:10,000 --> 00:00:16,000
Yes, there are when the monks stay in rope for life.

3
00:00:18,000 --> 00:00:22,000
As long as one is in ropes, they are binding.

4
00:00:22,000 --> 00:00:28,000
As soon as the ropes are off there, of course, not binding anymore.

5
00:00:31,000 --> 00:00:37,000
The other thing to note is that there's no god that's going to come down and punish you if you break them.

6
00:00:37,000 --> 00:00:40,000
In fact, some of them are not even immoral.

7
00:00:40,000 --> 00:00:46,000
So in Iran, an enlightened being can break certain rules.

8
00:00:46,000 --> 00:00:51,000
There's, I believe, there's stories.

9
00:00:51,000 --> 00:00:58,000
There are stories of Arahans who were the ones who were the first person to break the rule.

10
00:00:58,000 --> 00:01:00,000
There was some.

11
00:01:00,000 --> 00:01:05,000
And Arahans who spent the night with a woman alone in the room because she tricked him.

12
00:01:05,000 --> 00:01:09,000
And so he stayed there and he taught at a dumb mind. She became so dependent.

13
00:01:09,000 --> 00:01:15,000
And the Buddha said, don't stay alone with the woman because people will say bad things.

14
00:01:15,000 --> 00:01:20,000
But as far as Arahans actually breaking a rule that was already established,

15
00:01:20,000 --> 00:01:25,000
I actually can't think of any instances, but theoretically it's possible.

16
00:01:25,000 --> 00:01:31,000
One could imagine an example of an Arahan breaking a rule.

17
00:01:31,000 --> 00:01:36,000
Their community guidelines, I guess you could say, and they are binding.

18
00:01:36,000 --> 00:01:39,000
But if you break one, you just confess it.

19
00:01:39,000 --> 00:01:42,000
Or if it's a serious rule, you undergo pendants.

20
00:01:42,000 --> 00:01:46,000
Or if it's a major rule, you are no longer a monk.

21
00:01:46,000 --> 00:01:50,000
And people have said that if you break one of the major rules,

22
00:01:50,000 --> 00:01:55,000
one of the severe, the serious defeat rules,

23
00:01:55,000 --> 00:01:57,000
then in that life you can never become enlightened.

24
00:01:57,000 --> 00:01:59,000
But it's actually not.

25
00:01:59,000 --> 00:02:05,000
It has no basis in the Buddha's, in the canon, in the Buddha's scriptures.

26
00:02:05,000 --> 00:02:12,000
Mahasim Sayyada talks about this, he says.

27
00:02:12,000 --> 00:02:21,000
Then I can't remember what it was, but he was talking about something else,

28
00:02:21,000 --> 00:02:25,000
but then he mentions about the Parajika, the defeat offenses,

29
00:02:25,000 --> 00:02:32,000
that there's a story in the commentaries about this group of monks who did something.

30
00:02:32,000 --> 00:02:37,000
I can't remember what they did. They committed some kind of Parajika offense, maybe stealing.

31
00:02:37,000 --> 00:02:40,000
And they disrobed and became Samanera, became novices,

32
00:02:40,000 --> 00:02:43,000
because they could never ordain again. That's one of the rules.

33
00:02:43,000 --> 00:02:47,000
If you've committed one of these offenses, you're not allowed to ordain again.

34
00:02:47,000 --> 00:02:53,000
If I was a monk, you stole something worth more than a certain amount than you can't ordain again.

35
00:02:53,000 --> 00:02:58,000
But they ordain those novices and became so deprived in that life there.

36
00:02:58,000 --> 00:03:08,000
So they're not like, they're binding in the sense that you're expected to keep them.

37
00:03:08,000 --> 00:03:16,000
But many of them are not even immoral to break them, like eating in the afternoon, for example.

38
00:03:16,000 --> 00:03:22,000
If a person is sitting down and thinks it's 11 o'clock and then they eat some food

39
00:03:22,000 --> 00:03:25,000
and then they find out, oh, it's actually 12 o'clock.

40
00:03:25,000 --> 00:03:33,000
And they've technically broken the rule, but they may not be at all guilty of any irreverence,

41
00:03:33,000 --> 00:03:36,000
because they thought they were keeping the rule.

42
00:03:36,000 --> 00:03:39,000
That's what is an example.

43
00:03:39,000 --> 00:03:47,000
Men's, I mean, of course the Parajika, and then the heavy rules, the Sangha desasis,

44
00:03:47,000 --> 00:03:57,000
are better to keep because it's very difficult to deal with it when you break them.

45
00:03:57,000 --> 00:04:04,000
But all the other rules are kind of helping in the holy life.

46
00:04:04,000 --> 00:04:15,000
And I think they come out of compassion and made out of compassion.

47
00:04:15,000 --> 00:04:19,000
So he's asking now, in what sense are they're binding?

48
00:04:19,000 --> 00:04:22,000
If someone breaks them, who enforces that?

49
00:04:22,000 --> 00:04:27,000
Who holds the responsibilities at the Sangha?

50
00:04:27,000 --> 00:04:37,000
I'll answer to start that one holds one, ultimately holds one self-accountable.

51
00:04:37,000 --> 00:04:41,000
This monkey was teaching us about the precepts, all the new monks.

52
00:04:41,000 --> 00:04:46,000
I'm not sure why I was sitting in there. The new monks were studying basic dhamma,

53
00:04:46,000 --> 00:04:54,000
and I was trying to get the basic dhamma exams because in Thailand, in order to become the head monk of a monastery,

54
00:04:54,000 --> 00:04:57,000
you have to pass these exams. It was a total joke.

55
00:04:57,000 --> 00:05:04,000
They did a little bit of studying, and then on the exam day, their teacher came in and gave them all the answers.

56
00:05:04,000 --> 00:05:06,000
So we're sitting there.

57
00:05:06,000 --> 00:05:11,000
This is no relation to what we're talking about, but it's a funny story, so I'll tell it.

58
00:05:11,000 --> 00:05:15,000
We're sitting there, and they're all asking, what is jhana?

59
00:05:15,000 --> 00:05:22,000
Or no, what are the five sorts of uttoremanusadhamma?

60
00:05:22,000 --> 00:05:27,000
And then the guy is saying, what is this? What is that?

61
00:05:27,000 --> 00:05:31,000
Sorry, at the beginning of the exam, the head monk can come in and he said,

62
00:05:31,000 --> 00:05:37,000
okay, no ducharita, which means don't do any, no cheating, basically.

63
00:05:37,000 --> 00:05:44,000
The ducharita means no cheating. It means no evil acts, but in this context, it means no cheating.

64
00:05:44,000 --> 00:05:48,000
No cheating now, and then he no ducharita goes, and then he goes out.

65
00:05:48,000 --> 00:05:50,000
And so they're all asking, what's this, what's that?

66
00:05:50,000 --> 00:05:52,000
They're all giving each other the answers.

67
00:05:52,000 --> 00:05:56,000
And suddenly I pipe up, I say, what is ducharita?

68
00:05:56,000 --> 00:06:02,000
Because obviously they, and the one this one monk in front of me, he turns around and he's going to give me the answer.

69
00:06:02,000 --> 00:06:06,000
And I said, I was just joking.

70
00:06:06,000 --> 00:06:16,000
But the point actually came a lot earlier when we're in this class, and the teachers he's talking about,

71
00:06:16,000 --> 00:06:19,000
the precepts, he's talking about breaking them.

72
00:06:19,000 --> 00:06:22,000
And he says, you know, here's an example.

73
00:06:22,000 --> 00:06:29,000
Suppose I was in my room, and he looks at me, and I don't know why he looks at me.

74
00:06:29,000 --> 00:06:32,000
But he starts saying to me, and what do you think?

75
00:06:32,000 --> 00:06:39,000
No, I suppose I'm, and I say, suppose no, I'm not, I'm not, you think I'm actually do something like this.

76
00:06:39,000 --> 00:06:46,000
Suppose I go to my fridge and I pull out a salad at nine o'clock at night, and I eat the salad totally clean it out,

77
00:06:46,000 --> 00:06:56,000
and then they clean the styrofoam off, put it in a plastic bag tied up, and put it at the bottom of my garbage bin.

78
00:06:56,000 --> 00:07:01,000
And he said, no one knows that I broke the rule.

79
00:07:01,000 --> 00:07:03,000
And he says, or do they?

80
00:07:03,000 --> 00:07:06,000
He said, who knows about it?

81
00:07:06,000 --> 00:07:10,000
Who knows that I broke the rule?

82
00:07:10,000 --> 00:07:16,000
Hmm, that's the answer. Some people are saying, David, David, it's true, they may know.

83
00:07:16,000 --> 00:07:18,000
There are David's who couldn't know.

84
00:07:18,000 --> 00:07:22,000
There are also people with magical powers who couldn't know, but in the end, you know.

85
00:07:22,000 --> 00:07:26,000
And if you're breaking the rules, you're the one who has to live with it.

86
00:07:26,000 --> 00:07:30,000
This is where we were talking about yesterday in the study course that

87
00:07:30,000 --> 00:07:34,000
our good and bad deeds are the things that follow us around.

88
00:07:34,000 --> 00:07:37,000
And when we're alone, we have to think about them.

89
00:07:37,000 --> 00:07:42,000
And that's ultimately the best teacher. That's what helps you to keep the rules.

90
00:07:42,000 --> 00:07:46,000
When I was a young monk, in Thailand, many monks will not keep the rules.

91
00:07:46,000 --> 00:07:48,000
And so they will do things like that.

92
00:07:48,000 --> 00:07:55,000
I would go up to the school, and the novices are up there at nine evening frying up fish or something.

93
00:07:55,000 --> 00:08:01,000
A flagrant violation of monastic rules, but it became quite common.

94
00:08:01,000 --> 00:08:06,000
And so I broke some of them, not the eating one, but there were many drinking

95
00:08:06,000 --> 00:08:08,000
soil milk in the evening, for example.

96
00:08:08,000 --> 00:08:11,000
But eventually, it weighs down on you.

97
00:08:11,000 --> 00:08:14,000
And you start to read the Buddha's teaching.

98
00:08:14,000 --> 00:08:18,000
And you can see that this is not something that is of any benefit.

99
00:08:18,000 --> 00:08:27,000
And it's actually a means of degrading the Buddha's teaching, degrading the purity of the Buddha's ass.

100
00:08:27,000 --> 00:08:31,000
And the Buddha said, or the commentary, one of the commentaries says,

101
00:08:31,000 --> 00:08:35,000
that the Vinaya is the lifeblood of the sassana.

102
00:08:35,000 --> 00:08:39,000
Vinaya is what keeps the dhamma alive.

103
00:08:39,000 --> 00:08:48,000
Vinaya is the precepts, the more the ethics is the protection for the Buddha's teaching.

104
00:08:48,000 --> 00:08:52,000
And you can see this most clearly, you can see this in terms of money,

105
00:08:52,000 --> 00:08:57,000
because those monks who begin to touch money, and even though it may come as a surprise,

106
00:08:57,000 --> 00:09:02,000
but the majority of monastics out there would do carry money around with them.

107
00:09:02,000 --> 00:09:05,000
And for some of them, it's not a problem.

108
00:09:05,000 --> 00:09:10,000
For some of them, they will be able to comport themselves and deport themselves in a way,

109
00:09:10,000 --> 00:09:14,000
and use the money in such a way that it doesn't defy their mind,

110
00:09:14,000 --> 00:09:16,000
and they're doing great things with it.

111
00:09:16,000 --> 00:09:20,000
And you've got to admire the fact that they're doing good things.

112
00:09:20,000 --> 00:09:24,000
That being the case, even those pure and great monks,

113
00:09:24,000 --> 00:09:28,000
if you look at the monastery, is that they live in surrounded by monks who also touch money,

114
00:09:28,000 --> 00:09:30,000
who are not so pure.

115
00:09:30,000 --> 00:09:38,000
It's easy to see how this rule, even though it's only a concept, is incredibly important,

116
00:09:38,000 --> 00:09:42,000
because those certain monks may not be attached to the money.

117
00:09:42,000 --> 00:09:45,000
They begin to attract monks who are attached to the money,

118
00:09:45,000 --> 00:09:49,000
and of course, new monks won't have that ability to let go.

119
00:09:49,000 --> 00:09:55,000
And those monks who have some corruption, and their mind will be encouraged to develop that corruption.

120
00:09:55,000 --> 00:10:00,000
And eventually, the monastery of the teacher, especially, is very pure and becomes very famous.

121
00:10:00,000 --> 00:10:04,000
The monastery will become full of monks who are opportunists,

122
00:10:04,000 --> 00:10:14,000
so the corruption becomes rife, and it becomes a very difficult place to practice the Buddhist teaching,

123
00:10:14,000 --> 00:10:16,000
practice meditation.

124
00:10:16,000 --> 00:10:21,000
You learn a lot from it about human defilements and so on,

125
00:10:21,000 --> 00:10:26,000
but it's quite sad, on the other hand, to see the breakdown of the monastic community,

126
00:10:26,000 --> 00:10:29,000
as a result of not keeping the rules.

127
00:10:29,000 --> 00:10:31,000
I wish I could remember.

128
00:10:31,000 --> 00:10:32,000
There's like 10 reasons.

129
00:10:32,000 --> 00:10:34,000
The Buddha gave 10 reasons for the Vinayana.

130
00:10:34,000 --> 00:10:36,000
I'm not well versed.

131
00:10:36,000 --> 00:10:40,000
It's not something that I memorized read it several times,

132
00:10:40,000 --> 00:10:47,000
but it's this sort of thing.

133
00:10:47,000 --> 00:10:53,000
Good monks are able to live in comfort and in ease.

134
00:10:53,000 --> 00:10:58,000
It suppresses the defilements of the individual monks.

135
00:10:58,000 --> 00:11:00,000
It gives faith to the lay people.

136
00:11:00,000 --> 00:11:06,000
It gives faith to people who don't have faith when they see these people practicing in great discipline.

137
00:11:06,000 --> 00:11:11,000
Those people who have faith are their faith is preserved because when you break the rules,

138
00:11:11,000 --> 00:11:18,000
it's easy for people to lose faith and lose interest in the Buddhist teaching when they see people all correct,

139
00:11:18,000 --> 00:11:19,000
and so on and so on.

140
00:11:19,000 --> 00:11:23,000
And I think the last one is that it's a protection for the Dhamma.

141
00:11:23,000 --> 00:11:28,000
So many reasons for keeping them and those are really, I think,

142
00:11:28,000 --> 00:11:35,000
what holds you accountable is that you can see the results of keeping them

143
00:11:35,000 --> 00:11:40,000
and the results of breaking them.

