1
00:00:00,000 --> 00:00:07,720
I have spent some time in a Vashnava monastery, better known as the Hare Krsna's, and their

2
00:00:07,720 --> 00:00:12,520
scriptures say that the Buddha is an incarnation of God or Krsna.

3
00:00:12,520 --> 00:00:19,200
Do you think that this was a tactic to reduce people converting from Hinduism to Buddhism?

4
00:00:19,200 --> 00:00:35,520
Hmm, I don't think so.

5
00:00:35,520 --> 00:00:44,080
Because actually this is more a Hindu belief than a Buddhist belief.

6
00:00:44,080 --> 00:00:56,440
So how could they have gone people to become Buddhists or Hindus to become Buddhists?

7
00:00:56,440 --> 00:01:03,480
No, it's just to reduce people, to stop people from converting.

8
00:01:03,480 --> 00:01:08,840
Oh, well, yeah, then I do think.

9
00:01:08,840 --> 00:01:20,840
Yeah, yeah, probably that was the case, then I understood it wrong, so I don't know.

10
00:01:20,840 --> 00:01:28,440
I was going to say in Islam, Jesus Christ is one of their prophets.

11
00:01:28,440 --> 00:01:36,840
So I don't think it's unusual in cultures that grew up together to have to share figures.

12
00:01:36,840 --> 00:01:43,360
Well, in a religion that, and most religions claim to be universal truths, Christianity

13
00:01:43,360 --> 00:01:46,360
claims to be the whole truth.

14
00:01:46,360 --> 00:01:49,320
Islam claims to be the whole truth.

15
00:01:49,320 --> 00:01:55,280
Hinduism also claims to be a philosophy that encompasses the whole of reality.

16
00:01:55,280 --> 00:02:04,760
So to allow for something outside of that doctrine is not possible.

17
00:02:04,760 --> 00:02:11,520
So regardless of whether it's a jealousy thing or a worry about people leaving it, it's

18
00:02:11,520 --> 00:02:20,720
a way of blocking out the truth, really, that their doings is that their truths or other

19
00:02:20,720 --> 00:02:29,760
religious paths, because it's a threat to your belief to think that there could be something

20
00:02:29,760 --> 00:02:33,720
else, like an enlightenment that isn't your enlightenment.

21
00:02:33,720 --> 00:02:40,920
So they therefore find an explanation, so people will come to the teach, well, what about

22
00:02:40,920 --> 00:02:42,960
the Buddha?

23
00:02:42,960 --> 00:02:49,960
Well, Buddha was just another, actually, Vishnu or Krishna has many incarnations, and Buddha

24
00:02:49,960 --> 00:02:52,160
is just another.

25
00:02:52,160 --> 00:02:56,960
So this is how these theories come up, and it can become so ingrained in people that I've

26
00:02:56,960 --> 00:03:03,480
talked to these people, the very Christians, and they'll say it matter of factly, because

27
00:03:03,480 --> 00:03:09,760
it's their accepted belief, and they really do believe that that's the case.

28
00:03:09,760 --> 00:03:14,680
They believe that we're deluded to think that Buddhism teaches something other than the

29
00:03:14,680 --> 00:03:20,480
worship of Krishna or the attainment of Godhood.

30
00:03:20,480 --> 00:03:31,360
I think there's a real problem with this, because this theory makes Buddha a God, which

31
00:03:31,360 --> 00:03:44,640
is not the case, which was not the case, and they are really tense, everything, and

32
00:03:44,640 --> 00:03:50,400
puts it in the wrong direction, and it makes all the entire teaching, and the entire

33
00:03:50,400 --> 00:04:00,400
story of the Buddha kind of silly.

34
00:04:00,400 --> 00:04:04,960
And there's one other thing that I thought of as when I was talking about how these religious

35
00:04:04,960 --> 00:04:08,600
traditions are claimed to have the universe of the whole truth.

36
00:04:08,600 --> 00:04:13,480
I think you'd almost have room to say that Buddhism doesn't claim such a thing, that

37
00:04:13,480 --> 00:04:19,960
we might even boast of ourselves that we aren't falling into that folly, because in

38
00:04:19,960 --> 00:04:27,680
one sense, anyway, because Buddhism can accept many of the doctrines of these religions.

39
00:04:27,680 --> 00:04:35,880
You could even say, well, yeah, maybe Jesus Christ was the Son of God, but who's this

40
00:04:35,880 --> 00:04:38,080
God guy anyway?

41
00:04:38,080 --> 00:04:46,040
What's so special about him, or in Hinduism, we would accept, you know, Krishna may

42
00:04:46,040 --> 00:04:51,240
be Krishna exists. I mean, I think it's a little bit silly, because Krishna is actually

43
00:04:51,240 --> 00:04:55,640
a part of a Hindu, I guess you shouldn't talk about.

44
00:04:55,640 --> 00:04:59,800
These are stories that they composed much later on.

45
00:04:59,800 --> 00:05:09,160
The God Krishna doesn't even come into Hindu religion until after the Buddha, the Upanishads

46
00:05:09,160 --> 00:05:12,920
don't talk about Krishna, I don't think.

47
00:05:12,920 --> 00:05:18,120
One of the early gods of Indra, the Vedic God is Indra, and then you have Pajapati and

48
00:05:18,120 --> 00:05:22,760
I think even Vishnu, I can't remember, but then it was much, much later when Krishna

49
00:05:22,760 --> 00:05:28,480
came in the Mahabharata and Ramayana, which were really just folk tales like the Odyssey

50
00:05:28,480 --> 00:05:36,840
or the Iliad, very similar, long oral stories that people told, very much like the stories

51
00:05:36,840 --> 00:05:44,160
of Homer, but then the problem is that they became religious texts and they incorporated

52
00:05:44,160 --> 00:05:48,400
religious teachings of the Upanishads into them, and they took this religious tradition

53
00:05:48,400 --> 00:05:52,520
and put them in the context of these stories, and so you have the Bhagavad Gita, which

54
00:05:52,520 --> 00:06:00,960
is very much a religious teaching, but it's placed in the mouths of the mouth of this new

55
00:06:00,960 --> 00:06:09,200
God, it was really a new God, like Shiva, for example, who came up later as well.

56
00:06:09,200 --> 00:06:17,760
But we could accept that there are such gods, we might not accept Krishna because we might

57
00:06:17,760 --> 00:06:22,880
say, well, it looks a little bit more like he was made up as they went along, but we

58
00:06:22,880 --> 00:06:27,400
might accept that Indra exists, and in fact in Buddhism we do accept Indra because at the

59
00:06:27,400 --> 00:06:31,160
time of the Buddha it was well-known in people who were talking about him, but the Buddha said,

60
00:06:31,160 --> 00:06:38,280
well, you know, actually his name is not Indra, his name is Saka, so this kind of thing,

61
00:06:38,280 --> 00:06:44,000
but we can accept that these people have this path and it can lead you to become one with

62
00:06:44,000 --> 00:06:45,000
God.

63
00:06:45,000 --> 00:06:50,680
We could even go so far as to say, you can become one with God, always says it's not enough.

64
00:06:50,680 --> 00:06:56,440
So I think that is a valid point that it's simply because these religions are trying

65
00:06:56,440 --> 00:07:06,920
to fit the whole universe, the complex and manifold infinitely diverse universe with all

66
00:07:06,920 --> 00:07:14,480
of its infinite paths and potentials into a very, very, very small framework of reality

67
00:07:14,480 --> 00:07:18,760
that their own minds are able to encompass.

68
00:07:18,760 --> 00:07:24,320
So I think in one sense we are free from that in the Buddha's teaching that we don't

69
00:07:24,320 --> 00:07:31,680
really try to figure out who is God and what does He want and what does He want us to do.

70
00:07:31,680 --> 00:07:34,360
We're not really interested actually.

71
00:07:34,360 --> 00:07:41,960
We find a universal truth in another sense and that is that even God, even heaven,

72
00:07:41,960 --> 00:07:49,360
even God who and ourselves are meditation practice, all of these things are made up of building

73
00:07:49,360 --> 00:07:56,640
blocks, just as a physicist will tell you, the mind is also an experience in general

74
00:07:56,640 --> 00:08:04,880
is made up entirely of building blocks, of physical and mental components that are impermanence

75
00:08:04,880 --> 00:08:07,040
suffering and on self.

76
00:08:07,040 --> 00:08:10,400
The meaning is that there is no God, no matter whether you see God and He comes and talks

77
00:08:10,400 --> 00:08:12,160
to you, there is no God.

78
00:08:12,160 --> 00:08:14,160
When I'm sitting here talking to you, there is no me.

79
00:08:14,160 --> 00:08:19,040
And none of us really exists, this place, as I was telling you, I said, this monastery

80
00:08:19,040 --> 00:08:25,120
has no people in it and you have, if you want the best way to get along in the monasteries

81
00:08:25,120 --> 00:08:28,600
to remind yourself that there's no people here, because if there's no people working

82
00:08:28,600 --> 00:08:35,200
the problem come from and there is no people, there are only physical and mental realities

83
00:08:35,200 --> 00:08:40,240
that arise and cease.

84
00:08:40,240 --> 00:08:44,480
Thank you.

