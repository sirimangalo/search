Hello everyone, welcome to our Saturday demo broadcast.
Today I'll be talking a little bit about experience and then of course we'll have our usual
question and answer session.
I'd like to remind my students about the focus of an experience in Buddhism.
So we have describing the framework that would have used to describe reality.
If you look at all of the dumbas, the Buddha taught in relation to meditation and the
nature reality, you can see a pattern that they all relate back to and experiential outlook.
As opposed to a conceptual outlook, a conceptual outlook is where you create reality in your
mind, an idea of what reality is.
Even science does this if you're not to be disparaging but just to point out that when
you take a framework like the atomic structure of matter, you're ultimately conceptualizing
that in your mind.
It's not that the things that science explains are true or not true.
It's that the activity related to that outlook is conceptual.
You don't experience those things, you see.
So it's not a question of what's true, what's not true, what's right, what's not right.
It's a question of what is involved with the engaging with that framework.
When you engage with a atomic structure of matter as a framework for reality, you're conceptualizing
and when you look at something, you don't see the atomic structure but you can see
even your mind, there's mental activity involved, there's mental conceptual activity, conceptualization.
And all of that is on top of the more fundamental activity which is experiential.
So anything but an experiential outlook, an experiential framework is going to involve additional
conceptualization and it's going to largely ignore or overlook the actual experience because
you're not focused on experience when you're thinking about concepts.
When putting aside like physics or chemistry in our ordinary everyday life, when you open
the door to your bedroom, go into the kitchen, engage with your family, you're largely going
to be conceptualizing, thinking about them, who they are, thinking about your activities
for the day, maybe you go to get some food to eat and all of this is conceptualizing.
You see something and on top of the experience of seeing, there's a concept of it being
this or that and all of your narrative about that object.
So there's a still additional conceptualization.
This is what makes an experiential framework special, different and useful because if you're
focused on experience, then it's the one outlook that doesn't add anything on top of
what's actually happening.
When you direct your mind towards experience, there's nothing additional, there's nothing
added, there's nothing superfluous or nothing extraneous.
It has that advantage and that advantage allows, well two things, first it allows you
to live it live peacefully, there's a simplicity to it that is quite peaceful if you become
skilled at actually doing that.
In second, it allows you to see all of the things you might be doing wrong, all of
the harmful activities that you engage in on this level, on this fundamental experiential
level, like all of your engagement with concepts is going to be fraught with biases,
with conceivings, with views, beliefs, opinions, that might be right, might be wrong.
You'll start to see that many of them are wrong, so experiences, I can't stress how important
it is to focus on experience and practice of Buddhism and just as a means of bettering
ourselves on a fundamental level.
Sorry, I just adjusting my audio, maybe that's better, let me know, if there's any problems.
So, I just wanted to go through
sort of a survey of our relationship with experience in the practice of the Bhamma.
So the first thing to talk about related to experience is the means by which
we develop this perception of experience, of reality as experiences, how we're actually
experiencing the world.
And that's where the four foundations of mindfulness fit in.
Our foundations of mindfulness are a tool used to, they're a conceptual tool, there's
nothing, there's nothing magical about them, it sounds kind of like a magical special
tool.
It is special, but special because we know it comes from someone who really did understand
reality and more that he was an expert at teaching it, they really understood things
as they were, so we can appreciate, we can assume that it's going to be something special
in that way, but there's nothing magical about it, it's just a tool that allows us to
some divide reality into categories that are easily processed and easily engaged in.
And when we apply ourselves to these foundations or these applications where we establish
our mind, fix our mind on the realities when we grasp things as they are, the body
as body, feelings as feelings, mind as mind, dumb as it's dumb as.
Our mind starts to acclimatize, sort of the shift towards seeing things just as they
as experiences.
We start to notice experiences in the, on a level that we don't normally, I don't normally
experience, I don't normally see the, the second step is where we start to as a result
of focusing on experience, we start to, well, see experience, we start to see the makeup
of experience, the makeup means the things that make up experience.
It's not a, it doesn't sound very profound, it's not a very profound thing, but it's
a very important thing, it, it defines the shift, how you know that you're actually focused
on experience when you're seeing the makeup of experience, when you're seeing reality as
these aspects of experience and, and it's quite simple, the simplest way of understanding
it is physical and mental components.
When the stomach rises, you don't experience it as, oh, my stomach is rising, you experience
it as emotion, as attention in the, in the stomach, the tensing and then a release, tensing
when the stomach rises, releasing when the stomach falls, there's a feeling, it's not
a stomach, it's an experience, and you'll notice this with the foot, with the foot there
are feelings involved, sensations involved, and you start to notice this with all of the
many things you experience seeing, hearing there's a physical component, there's the object,
there's the organ that receives the phenomenon, and you also start to see things about
the mind, you start to see the mental aspect of experience, with the first of all, the
mental aspect that actually experiences things, you'll see that you don't always notice
that the stomach is rising only when the mind is there as well, only when there is this
mental component, and then when the mental component is somewhere else, there's no more
experience with the rising or falling, no more experience of the foot, and you start
to notice the contents of the mind, the reactions to things, you start to notice pain and
pleasure, and our reactions to pain and pleasure and calm, and not only notice but see
more clearly, but seeing clearly is a big part of this process, to get from step to step
to the end, there's an increase in clarity as you go.
The third step is understanding how these aspects of experience work together, and this
is where the ideas of karma and cause and effect come in, so as you focus on experience,
you focus on the six senses, you focus on the mental and physical aspects, you start
to see how they go in sequence and they create chains, they say you'll start to see your
reactions and you'll see how that works when you have a pain, then there'll be a reaction
to the pain, there's a reaction to the pain, then there's a desire to free yourself from
the pain and there's activity to relieve the pain and so on.
It's important because a lot of our reactions, of course, cause increased suffering,
they don't actually better our situation, when we are stressed about something anxious, afraid,
angry, frustrated, sad, depressed, or craving, wanting, yearning, I start to see the
cause and effect relationship between things, so we start to see how we develop habits
of causal consequence, the changing of our experiences together, and this repeated
changing that creates habits and the augmenting of the habits through repeated engagement,
because where mental problems come from real mental illness comes from this repeated
reaffirmation of bad habits, once we start to see this cause and effect, we start to
work it out, we start to engage with the experience, not actively changing it, but paying
better attention and it starts to work itself out based on this attention, based on this
better point of view, it's like we've got a better angle or a better place to view the
situation from getting a better angle when you have a photo shoot or something, we can
get a better perspective, you're able to see more clearly what's going on, when you're
watching a show or something, get a better angle on it, this better angle is our clarity
of perception, like focusing a camera, we're able to see more clearly, allows us to change
our habits, it reduces our engagement, it reduces our encouraging bad habits, when you see
something as unpleasant, you stop encouraging it and a lot of the problems we start to see
is we just didn't notice how bad it was for us and so we blindly encouraged, incited
and strengthened bad habits and that all starts to reduce, starts to break down, this is
where the three characteristics come in, because the way of explaining what's going on
is you start to see that the things that we're worth clinging to, because they were
stable, satisfying, controllable, are not stable, satisfying, or controllable and so not
worth clinging to not worth getting involved and not worth trying to fix or control or
keep or chase away, that that activity is not a solution, it's not helpful, it's not
viable because the things we thought were amenable to change, to control, to keeping, to
enjoying, are not amenable to those at all, they're unsatisfying, uncontrollable, unpredictable,
chaotic, and this really encompasses the majority of our meditation practice, once we've
passed through those first stages of understanding reality, you know, to make up of reality,
how reality works together, there's just a deepening of our understanding, our clarity,
our perspective, our familiarity with good experiences, bad experience with all experience,
that we become familiar, until we're able to see reality just as it is, without any reaction,
without any clinging, without any need or want, and then finally comes to the point where
we let go of experience, let go means we stop clinging, rather than getting stuck on anything
where we become independent, where we're no longer in need of this or that or anything,
where we're no longer dependent on circumstance, and this is where Nibana comes in, or
the mind, let's go, and there's freedom, and experience of release, and all of this
takes place in this framework of experiential reality, so if this all sounds very technical,
I hope it doesn't, but just to bring it down to earth right now at this moment, sitting
in your chair on your cushion or on your bed, listening to the sound of my voice, the temperature
in the room around, do the pressure on the various parts of your body, the thoughts
in the mind, the emotions, the sights, the sounds, the smells, the feelings, all of these
are ever present, and they're the doorway, they provide this path that is always accessible,
and by simply focusing on them and engaging with them, using the four foundations of mindfulness,
it leads in this very clear and straightforward direction, this path towards freedom from
clinging, freedom from suffering, so just some words on experience, that's the talk for today,
if we have questions, go ahead with that, all right, we have questions, let's begin.
If a view or opinion spontaneously arises in the mind all at once, how should I note it?
It doesn't feel like I'm actively thinking something through, so maybe knowing instead of thinking,
either one is fine, knowing is a little bit problematic because the view or opinion might not be,
you might not know the view or opinion, you believe it, so if you want to say believing that might
be just to be clear in your mind that you don't actually know whether it's true,
just be careful about that, that you don't, when you think you know something,
just because you think you know it doesn't mean you actually do know it,
it's still thinking and I would still probably go with thinking.
When I acknowledge and see that I can't know what will happen, the very next moment,
there is a big letting go, is that the correct understanding of what non-self means,
or is there something else? So this kind of question comes up
often and I think it comes from a concern that there's a concern that
we have a concern that we do actually, we will actually experience the three characteristics and
so we're constantly or often worried about it or concerned. I better make sure I see these three
things and so you want the affirmation that what you saw was the three characteristics, but
to be clear when you actually do fully see the three characteristics, any one of them,
the next experience is going to be nibanda, it's going to be a cessation of,
you know there's a bit, it's super mundane. So the three characteristics are
characteristics of all things that arise, they're they're not self and so kind of the answer is
yes, you through realizing impermanence that it's unpredictable, that helps you see non-self,
it helps you appreciate the non-self characteristic, but don't worry about it. Don't be obsessed
with what's that non-self, is that non-self? It's a good question in a way because it's good for us
to talk about these things and understand them, but it's not so healthy to ask this question
when you're practicing, so be wondering or be concerned as to whether something is the
one of the three characteristics, because it leads to clinging, clinging both when you don't
experience it and clinging when you do experience it, when you do have this kind of experience,
you then think yeah, I'm doing good and it can lead to complacency, it can lead to distraction.
So on the one hand, I would say yeah, kind of be reassured that that's the way things are,
just don't cling to it and just understand that as long as you're practicing, you're going to be
seeing the three characteristics more and more clearly, it's just it's not anything I'm
extruder or esoteric, it really is the nature of things, so it would be a shock if you didn't
see them when you're looking at reality, if you didn't see them it would mean you really aren't
practicing and most of seeing them, we never even realized that that was the three characteristics,
it doesn't even register, and there will be these moments of clarity where it just seems so
obvious and you do let go of something because you saw something very clear about the three characteristics,
but you're just always going to be seeing that, don't focus too much on it, focus just on seeing,
and be reassured by experiences like these, or just be reassured in general that that's just
the way things are, intellectually it's pretty easy to see or to appreciate that that's the nature
of things. The real problem is that even though intellectually we can appreciate, we still have
this misapprehension of things as being stable, satisfying, controllable, just because we're not
clear because we haven't really gained the familiarity, we haven't spent enough time focusing
on reality. I just mean to say don't get too concerned about it.
When focusing on the rising and falling of the stomach and specifically when waiting for the
rising, why is it recommended to note sitting? Doesn't that distract from the upcoming rising?
I'm not quite sure what you're referring to, but we often tell meditators to
add a third object, and during the course we'll tell them at some point to add a third object.
But the premise of your question is problematic because distracting from an object
is nothing to do with insight with we pass in a meditation. That would be something you'd be
concerned with in some time where you want to focus on a single object. Reality is always going to
be different objects, new objects. A big problem that we're trying to address is the fixation
on objects, on specific objects, and pushing you to switch objects is helpful in terms of
weaning you off this need for for rhythm, this need for for stability. So adding and sitting
is like throwing a monkey wrench into things like adding a third ball to a juggler.
We make it more challenging for you. Give you a new dynamic to try and train you and
to to to to expose the propensity or the inclination of the mind to cling
and inclination of the mind to rely on stability.
What is your advice for practicing walking meditation while experiencing things such as nausea?
I find it very difficult and tend to switch to lying down to meditate.
Well I'd start with with standing meditation before lying try and note it while you're standing
and just stand until the nausea goes away and if it doesn't of course you can sit down or even lie
down that's fine. Just be mindful as you do and mindful when you switch.
Can someone with deep rooted trauma practice meditation or will it make things worse?
So it's a bit of a you know the question is a bit too broad.
It depends on both the circumstances of the individual and the circumstances of the practice.
Practice meditation is there's a lot that goes into doing something
to make it actually practicing meditation and I'm talking about both the technique that the
practitioner uses and the conditions in which they practice. Do they have a teacher? Are they
meeting with that teacher? Are they living with that teacher? Etc. How much are they practicing?
So on the one hand the the nature of the actual practice is going to change the answer
and also the nature of the trauma, the nature of the individual. If someone has trauma and that's
all that's wrong that's not so bad that's actually pretty amenable to change through through
incitement through mindfulness meditation but if someone has mental issues as well like maybe they
are suicidal or have other mental and stability that can have already cropped up through the trauma
then that's going to exacerbate the situation make it more of a delicate situation.
So I would generally say yes but for such a person I would encourage them to try to find a
well two things I guess practicing on their own. If practicing on their own I would suggest for
them to just be careful and vigilant and clear in their mind that they're following the rules
and I would encourage them to follow a technique to the teeth and always remind themselves that
they have all of the technique and to try and remind themselves by reading about the technique
and rereading about technique. That's the wrong way accordingly because someone with real mental
issues real emotional issues or whatever is more likely to go off track. Just be your
trainer always keeping yourself on track and don't push yourself too hard don't
think that you're going to be able to do intensive practice on your own because some people do
fall into that trap and can actually make things worse by pushing themselves so hard
that they stop being mindful and it's just about pushing and it becomes obsessive and by the time
they realize they aren't actually meditating they've just gone off the deep end so not to scare
anyone it's not meditation isn't scary but you're dealing with the mind and if you stop actually
practicing mindfulness and think your practicing mindfulness without actually following the
technique it can get dangerous you just get lost in the forest meditation mindfulness isn't
dangerous just make sure you're actually being mindful but the second thing is if you if you do
want to do say a course that I would encourage such a person to actually
have a good relationship with a teacher and make sure that they're actually able to
depend on that teacher I think ideally to be in a center with a meditation center with a teacher
is walking a more samatha-like meditation than sitting since you are trying to maintain focus on
the feet and stopping only for bigger distractions samatha meditation focuses on a concept
walking meditation focuses on ultimate reality so no it's nothing like samatha
is it discouraged to do shorter meditation sessions throughout the day if necessary or practical
surely not but I can't get this notion out of my mind should we not just do as much as we can
so there are disadvantages to trying to do fewer long sessions just as there are
disadvantages to doing more short sessions I think I don't have a way to quantify which is
or where the optimum is but there is an optimal balance between length of session and number of
sessions if you do lots and lots of very short sessions you're going to you're going to do
it's going to be to your detriment and likewise if you try to do all of your meditation
in fewer sessions like one session per day one long session and trying to just keep increasing that
one session it's diminishing returns you won't get nearly as much out of both approach
shorter sessions are too short if they're too short they're just going to be too easy
and it's a cop out and longer sessions are going to be not only too hard but so in frequent
that the rest of the time what are you doing the rest of the time that you're not mindful you've
wasted the opportunity to to re-center yourself to refocus yourself I mean formal meditation is a
great support for the rest of your day so if you're only doing it once a day you've got 24 hours
between sessions so find a balance you really have to find a balance where it's challenging
but also continuous
I do elaborate more on experiencing dhamma as dhamma I have difficulty in understanding this
term especially when it refers to other things other than teachings so it does refer to
teachings but it's important to understand that by teachings it's a specific progression of
teachings and it's it's also realities so it's not just any realities the dhammas are a
teaching-based set of realities but what dhamma is dhamma the Buddha says that for all four of
the Satipatana and it just means being able to distinguish things as they are and and it's not
really dhammas as dhamma it's seeing in regards to these things that they are these things
right if you read the pali or if you get a sense of what the Buddha is talking about it it really is
just saying seeing things as they are for all of the things that you experience seeing them in them
kaya kaya nupasiri ati kaya in the kaya in the body in regards to the body one sees body
and it's just a elaborate way of saying you see it as it is you don't see body or something else
you don't see anything as something else or more than it is you see it just as it is just what it is
how does one know that one is making progress in meditation
well I'll say two things and if you've heard if anyone's heard me give answers before they
know probably what I'm going to say but first of all the the way you know that you're making
progress is you have less greed less anger and less delusion that's really the best answer I
can give but at the same time I would say don't focus too much on progress try and get a clear
appreciation of of the benefits or of the beneficial nature of mindfulness and just focus on trying
to be mindful trying to appreciate the clarity that comes from being mindful and not worry about
something about progress it's misleading you're not going anywhere what do you mean by progress
we when we think like this we generally have a wrong understanding of what we're trying to get
out of the practice and that leads to discouragement because it doesn't bring the things we think it's
going to bring and you miss the things as it actually is bringing it's bringing clarity and you
can see that every moment that you practice but then we think well what what good is clarity right
we go beyond which is ridiculous because clarity is so good it's so perfect are you saying you can't
see the difference between clarity and delusion right be content with that and be content with
being and it's like it's like being content with being a good person why do you do good deeds what
good is it for you is a ridiculous question I know being a good person is good right regardless of
what happens to me I don't know if that even that may to some extent not make sense because you
think we need we live in this this frame of mind that somehow we need a conceptual idea of what
the go with the reward is going to be right but that's a problem our think of the Pavlov
experiment the conditioning right reward we're conditioned for reward and that's the problem that's
not what this is this is not a pavlovian conditioning practice where you get some reward and it
makes you want to do it more because then you're going to get the reward absolutely not you're
not going to ever feel that way about meditation it's never going to feel like something you crave
because of the reward it gives you right so this this thinking about what is the reward
is Pavlovian it's classic or classic I can't remember it's conditioning anyway it's Pavlovian
conditioning and you're not going to get that for meditation it's not going to make you want it more
so it's like being a good person you're content with being a good person you're reassured and in
fact it is being a good person because the best sort of person is one with a clear mind and you
know putting aside the idea of a person just being the best or the right the rightest the most
right way to be is mindful and you can see that if you're mindful there's you know what you
will start to appreciate is how good it is to be mindful it just is good you don't have to worry
about what the consequences are the results are going to be I mean to some extent I do I'm going
to get in trouble because you do have to appreciate the goal and where we're headed it's just
that should never be your focus when you're meditating try and be mindful and and create the
clarity of mind and just focus on increasing clarity you can have an intellectual sense that
you know where your head at or what the reasons are and and what the problems what the causes
what made you want to do this and all of the Buddhist theory about the path and the
abundance and it's all that's all good just shouldn't be your focus is it ever advisable to lie
to avoid potential harm no again this is a sort of relates to what I was just saying is that
you know that lying is is unwholesome is is twisted and so you don't care about the results
because lying is just twisted perverse
in the Satipatana Sutta the Buddha talks about the third one as seeing the different qualities
of mind but in our practice it is seeing thoughts clearly why the difference
so it's not that's not exactly true the third one is about the mind and the Buddha talks
about the various types of mind but it's also about whatever thoughts are whether there is
a greed or not greed so knowing the content of the thought is the third one as well but
on a practical level you can't focus on those qualities like greed anger delusion
as separate from the five hindrances right so if you want to nitpick then you have to say that
while these these same qualities they are in both the third Satipatana and the fourth Satipatana
so practically the way to make them the separating the putting the ignoring of those aspects
in the third Satipatana is for the purpose of creating a clarity of division when you explain it
to meditators so when you give when we when we give our technique which is a way of practicing
it's just one way but it is a very concrete practical way of approaching and practicing the
fourth Satipatana relating them simply as the hindrances makes more sense
so if you read the third Satipatana it has not just the hindrances but it has
qualities of mind or state types of mind or states of mind like the mind is
exalted or inferior is distracted or is concentrated or so on
and all of that relates to the process of thinking the process of remembering and planning
and so on and all that relates to this foundation of thinking so to separate that out from those
qualities we put the hindrances in their own category that's all I mean it's nothing special I mean
there's no there's no one way of doing this or of categorizing them the point is to approach
the reality is talked about in the Satipatana Sutta so we do that through this technique I mean
there's no question that the way of practicing is focused or based on the fourth Satipatana it's
just a specific way of approaching them but that is you know totally exactly how the Satipatana
Sutta does it we just ignore those parts because we're already talking about them in its different
section and we don't exactly ignore them it's just we acknowledge that whenever when whatever
thoughts there might be they're going to have this baggage and so whether you put them in the
third Satipatana or the fourth Satipatana you're still going to note them the same
if you if you're thinking about something there's a there's a quality and the mind has a quality of
liking it well then you'd say liking right another thing is that when you note the thinking you'll
see all the different qualities that the mind might have you'll see the different
different quantities of the mind this mind is like this this mind is like that but you'll see it
all when you say thinking it's a good question it's worth no it's worth reading the Satipatana
Sutta but I mean there's no question that when you practice when you say thinking you'll see all
these things and of course when you notice them particularly you note them as the hindrance is
already so there's no need to repeat it i've been practicing vipassana meditation on sensation
and on upon that but nowadays it's difficult to practice regularly i've been going through this
new job transition and i couldn't sit for four days any advice i think you might be practicing
a different tradition so i can't give you any specific advice that recommend if you're interested
in our tradition you can read our booklet sign up for that home course if anyone else is interested
or as everyone is welcome to join it's all free no cost but read the booklet if you're interested
can sign up for a meditation course specific advice for that question though
um I mean in our tradition we have you be mindful during the day so you can be mindful of
things during daily life as well you can read about that in the booklet it talks about some of that
so it may be consider being mindful and applying the same principles
it throughout your day which some traditions might not talk about in my country it is very taboo
to be too generous do you know a way to train for a generous mind without involving other people
you don't need a generous mind you just need a mind that let's go
the great thing about generosity is it really helps you let go giving is such a thing you know
i mean as a monk i rely on people's generosity so we see all sorts of it and giving giving
comes in many shapes and forms and one thing you notice that is really important
is that a lot of giving isn't actually giving up
we'll give things sometimes that we don't need we'll give things because we want something back
from it we'll often give things with conditions expecting we give things um well maintaining control
of them we give things without actually giving up and to actually give up
is such a powerful practice and it's so supportive and the Buddha recommended it because it
ties in very nicely with the process of mindfulness meditation which is all about giving up
so when you give up and let go you don't actually have to give anything to anyone
but you're able to you're able to give anything to anyone because you lose all of your attachments
to things i wouldn't focus too much about being generous i mean
simply letting go of expectations is the whole goal in the first place
it's what makes you generous like truly generous and not exactly i mean generous is
even misleading it's almost like you don't care oh you want this okay sure you can have it
it's wrong thinking to be obsessed with generosity and thinking that you have to go out of your
way to give you really shouldn't go out of your way to give you should just always make it a part
of your life that when someone needs something and it's appropriate to give you have no qualms
about giving and giving up in the sense that i don't expect you to give me anything back for
this i don't expect to have any control over this this is no longer mine right that's the sort
of thing that supports our practice and supports giving up this idea of mine in the first place
me right it's a deeper sort of giving so i don't know if that helps too much i mean it's shame
that you're in a country where people don't like to give things maybe you can start a new trend
but most importantly don't be too obsessed with it it's just a part of life it's an important
thing to remember because it's going to challenge us it's one of those things that helps us see
our clinging when we consider giving something and we see how attached we are to it how sad we
are to lose something that's valuable to us giving things that are hard to give it's a good
challenge for us and and maybe maybe in your example giving things even when it makes people upset
you know it makes people um not like you or something like that just chat the challenge there
maybe you'll get you'll get people criticizing you so the ability to put up with criticism in the
face of wrong view really anybody says too generous you should read the way santa rajatika that's
a good one everyone should read the way santa rajatika he challenged he made a determination to
give up everything so no matter what he would give up everything and a part of it was the realization
that it not nothing was his in the first place the realization more that it did
not matter what he gave up because it's all just some sara even if you give up your life you
haven't lost anything you're just going to be born again he give up your eyes once when the
body santa was a king he gave up his eyes did an eye surgery and gave his eyes to a blind man
as it's like you know what is the meaning what is the what what good is they're in keeping
what good is they're holding on and he was even asked for his children and he washed his hands
of them because again the realization that it's all what is it to my children what what ultimately
is it to them they gave up his wife he gave up his children and and the the power of the goodness
the I mean it may not even seem like goodness to give up your children but the power of the the
pure non attachment you know it realigned everything it kept everything in a good way and of
course he was reunited with his children and his wife because that's the most that's the
defining factor of some sara is the goodness in our hearts the rightness the purity that's what
leads to good things and so he reminded his children that this is what's important to keep your
heart pure pretty radical do you have any advice or experience with removing drain gnats
or maybe their fruit flies humanely I have some in my shower and sink in my bathroom
I don't I mean I'm not special in this that I have some special insight on it but it's good
that you're thinking about that just don't kill them you know you seek a seek advice people who
know things about such things I mean I guess my first question would be why do you have to get rid
of them they're short-lived anyway they come and they go they don't carry diseases do they
sounds like fruit flies more than that I'm surprised that nats would be living in your drain but
I don't know insects are interesting we should really know more about insects as Buddhists
because we can get a sense of the sorts of things that attract and repel them
if you can find something that repels them you can you can free yourself from it
what precautions should be taken by people with mental health issues i.e. past trauma when
learning meditation
well I think I already went over this don't push yourself too hard if you're doing it alone
and have a good relationship with a teacher if you're doing it with a teacher
and make sure to ask questions and provide information and and and in both cases
to absolutely again follow the teaching to the letter make sure you're following the teacher
in teaching I mean don't be paranoid about it am I following I'm not sure just be clear I mean
it's not it's not rocket science it's not like you have to read between the lines or anything it's
just the difference between going on your own and assuming that you can do it your way or adapt
things versus actually saying no the teaching says to do it this way so I'll do it this way
no I don't know adapting no improvising I mean a little intro improvising but only improvising
where it's not explicitly stated otherwise just make sure you're clear about the teachings
and you're practicing it's not hard it's not hard it's it's not like something you have to be
paranoid about it's just don't improvise don't modify because that's where people go wrong when
they stop practicing and they think they're practicing or they they don't bother to check whether
they're practicing according to the teaching don't kid yourself that you can do it better than
the teacher or something like that that's all there's not it's not I don't mean to scare you either
I want to be clear it's not dangerous mindfulness is helpful so the only the only rare case where
someone goes wrong is when they just ignore the teachings stop doing the teachings start doing their
own thing or push themselves too hard when they're alone or even with a teacher you know the
teacher gives you advice you go off and you just push yourself push and push and push until you
drive yourself crazy don't do that it's not rocket sight me it's not it's not dangerous you're not
likely to fall into these it's just be aware of no extremes thank you Bante we've come to the
hour and have no further to ones good good questions good group thank you all and thank you all
