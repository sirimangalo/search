1
00:00:00,000 --> 00:00:07,380
So in this video, I will be talking about a third technique of meditation, which is generally

2
00:00:07,380 --> 00:00:14,420
used as a warm-up practice before doing the walking and the sitting meditation.

3
00:00:14,420 --> 00:00:18,060
This technique is called mindful prostration.

4
00:00:18,060 --> 00:00:23,060
Now prostration is something which people of various religious traditions are familiar with

5
00:00:23,060 --> 00:00:25,220
around the world.

6
00:00:25,220 --> 00:00:30,820
For instance, in Thailand, it is used as a means to pay respect to one's parents or one's

7
00:00:30,820 --> 00:00:37,060
teachers or figures of religious reverence.

8
00:00:37,060 --> 00:00:42,860
In other religious traditions, it might be used as a form of worship for a divinity, a

9
00:00:42,860 --> 00:00:49,500
god or an angel or an object of worship.

10
00:00:49,500 --> 00:00:56,340
In the meditation practice, we're using the prostration as a means of paying respect to

11
00:00:56,340 --> 00:00:59,740
the actual meditation practice itself.

12
00:00:59,740 --> 00:01:05,500
So what we mean by mindful prostration is, besides simply being a warm-up, it's also a way

13
00:01:05,500 --> 00:01:15,900
of creating a humble and a sincere attitude towards the meditation practice so that we have

14
00:01:15,900 --> 00:01:21,500
a feeling that what we're doing is not something as a hobby or just for fun, it's something

15
00:01:21,500 --> 00:01:28,860
that we seriously hope to incorporate into our lives and to make a part of who we are.

16
00:01:28,860 --> 00:01:33,980
What we're practicing here is something of great significance, of great importance and

17
00:01:33,980 --> 00:01:37,220
something worthy of our respect.

18
00:01:37,220 --> 00:01:44,020
So this doesn't mean that we are worshiping or we are bowing down to an individual

19
00:01:44,020 --> 00:01:46,340
or an entity of any sort.

20
00:01:46,340 --> 00:01:53,540
It's a way of paying respect to the practice which we're about to undertake.

21
00:01:53,540 --> 00:01:56,860
It also can be thought of as simply a warm-up exercise.

22
00:01:56,860 --> 00:02:01,300
So instead of doing a simple prostration, we're going to actually be focusing on the movements

23
00:02:01,300 --> 00:02:04,340
of the body as we do the prostration.

24
00:02:04,340 --> 00:02:11,260
But in order to get a sense of what is meant by prostration, I'm going to give as an example

25
00:02:11,260 --> 00:02:17,060
the method of prostration used by the Thai people when they would normally pay respect

26
00:02:17,060 --> 00:02:21,140
to someone higher than themselves.

27
00:02:21,140 --> 00:02:29,020
This is because that particular prostration method will then be used as a model on which

28
00:02:29,020 --> 00:02:32,820
we can then do our mindful prostration.

29
00:02:32,820 --> 00:02:36,820
So in order that you get an understanding of what the prostration should look like, I'll

30
00:02:36,820 --> 00:02:44,380
first do, perform for you to see a traditional Thai prostration.

31
00:02:44,380 --> 00:02:50,380
And so the prostration is typically three, three prostrations, three boughs of respect.

32
00:02:50,380 --> 00:02:58,460
It's performed on one's knees.

33
00:02:58,460 --> 00:03:04,540
Traditionally with the person sitting on their toes, but if this is uncomfortable, you

34
00:03:04,540 --> 00:03:14,020
can also sit down on the top of your feet.

35
00:03:14,020 --> 00:03:18,500
You can do as most comfortable for you.

36
00:03:18,500 --> 00:03:22,700
The hands are, to start, the hands are placed on the thighs.

37
00:03:22,700 --> 00:03:23,700
The back is straight.

38
00:03:23,700 --> 00:03:26,500
The eyes are open.

39
00:03:26,500 --> 00:03:37,140
And when begins by bringing the hands up to the chest, then touching at the eyebrows,

40
00:03:37,140 --> 00:03:47,540
and then out in a W shape with the thumbs touching, down to touch the floor in front.

41
00:03:47,540 --> 00:03:54,020
The elbows will touch in front of the knees, and the head goes down to touch the thumbs

42
00:03:54,020 --> 00:04:02,780
between the eyebrows.

43
00:04:02,780 --> 00:04:11,100
Bring in the head up again, and then the hands up to touch together in front of the chest.

44
00:04:11,100 --> 00:04:19,540
This is one prostration, then one goes on to do the second prostration up to the forehead,

45
00:04:19,540 --> 00:04:39,180
and out again, and back up, and a third time.

46
00:04:39,180 --> 00:04:44,700
And after the third prostration, we then bring the hands up to the forehead, but instead

47
00:04:44,700 --> 00:04:50,500
of out again, we bring the hands simply back down to the chest, and this is a traditional

48
00:04:50,500 --> 00:04:53,140
tie prostration.

49
00:04:53,140 --> 00:04:56,740
This is going to be the model for what we call the mindful prostration, so it's a means

50
00:04:56,740 --> 00:05:02,420
of using this bodily movement as a meditation technique, or as I said, a means of paying

51
00:05:02,420 --> 00:05:09,620
respect, and building up states of humility and of respect, of sincerity in regards to

52
00:05:09,620 --> 00:05:12,700
the meditation practice.

53
00:05:12,700 --> 00:05:16,900
So we begin in the same way by putting the hands on the thighs, and we start with the right

54
00:05:16,900 --> 00:05:20,700
hand, one hand at a time, because here we're going to be watching the movements of the

55
00:05:20,700 --> 00:05:26,860
hand, again, we're focusing on the body as our primary meditation object.

56
00:05:26,860 --> 00:05:34,260
As the hand turns, we say to ourselves, turning, and we say it three times, turning, turning,

57
00:05:34,260 --> 00:05:41,740
turning, again, keeping the mind, keeping the noting, the mantra, the clear thought in the

58
00:05:41,740 --> 00:05:42,740
present moment.

59
00:05:42,740 --> 00:05:45,820
So as the hand begins to turn, we say turning.

60
00:05:45,820 --> 00:05:49,660
When it's in the middle, we say turning, again, and when it finishes turning.

61
00:05:49,660 --> 00:05:56,260
We say it three times, saying to ourselves, again, turning, turning, turning, turning.

62
00:05:56,260 --> 00:06:01,500
When the hand raises, we say to ourselves, raising, raising, raising, and when it touches

63
00:06:01,500 --> 00:06:06,140
the chest, we say touching, touching, touching.

64
00:06:06,140 --> 00:06:12,100
And again, in the same with the left hand, turning, turning, turning, raising, raising,

65
00:06:12,100 --> 00:06:20,900
raising, touching, touching, touching, then up to the forehead, raising, raising, raising,

66
00:06:20,900 --> 00:06:27,940
touching, touching, touching, and back down to the chest, lowering, lowering, lowering,

67
00:06:27,940 --> 00:06:31,580
touching, touching, touching.

68
00:06:31,580 --> 00:06:34,980
So instead of going out, we've brought the hands back down to the chest, again, then

69
00:06:34,980 --> 00:06:41,580
And then we do the actual frustration, first we bend the back, bending, bending, bending.

70
00:06:41,580 --> 00:06:43,380
And then again, bring the hands out.

71
00:06:43,380 --> 00:06:50,100
But this time, one at a time, lowering, lowering, lowering, touching, touching, touching,

72
00:06:50,100 --> 00:07:01,260
lowering, lowering, covering, covering, covering, lowering, lowering, lowering, touching,

73
00:07:01,260 --> 00:07:17,820
touching, touching, touching, touching, raising, raising, raising, turning, turning,

74
00:07:17,820 --> 00:07:20,540
turning, turning, and here it starts over again.

75
00:07:20,540 --> 00:07:24,660
So again, we start with the turning three times this time from the floor, and then the

76
00:07:24,660 --> 00:07:29,560
hand out, raising, raising, raising, and so on.

77
00:07:29,560 --> 00:07:36,040
touching, touching, turning, turning, turning, raising, raising, raising, touching, touching,

78
00:07:36,040 --> 00:07:42,780
touching, touching, touching, raising, raising, raising, raising, touching, touching, touching,

79
00:07:42,780 --> 00:07:51,500
touching, lowering, lowering, lowering, touching, touching, touching, bending, bending,

80
00:07:51,500 --> 00:08:21,440
bending, lowering, lowering, lowering, touching, touching, touching, covering, covering,

81
00:08:21,440 --> 00:08:31,440
And again, H1 step at a time, bending, and so on.

82
00:08:31,440 --> 00:08:40,440
And after the third frustration, we come up raising, raising, raising, touching, touching,

83
00:08:40,440 --> 00:08:45,440
touching, and up again to the, after the third time, up again to the forehead, raising,

84
00:08:45,440 --> 00:08:53,440
touching, raising, touching, touching, touching, lowering, lowering, touching, touching, touching, touching,

85
00:08:53,440 --> 00:08:56,440
touching, touching, touching, touching, touching, matching, touching, touching,

86
00:08:56,440 --> 00:09:14,440
LoP engineering, touching, touching, touching, touching, touching, touching,

87
00:09:14,440 --> 00:09:19,680
To finish that, we continue on with the walking meditation and then with the sitting meditation.

88
00:09:19,680 --> 00:09:23,480
So it's important that once you finish this, you don't just get up to do the walking meditation.

89
00:09:23,480 --> 00:09:26,000
You continue on with the mindfulness.

90
00:09:26,000 --> 00:09:30,880
As you go to stand up, first you say to yourself, sit dinging, sitting, sitting, and then

91
00:09:30,880 --> 00:09:36,000
standing, standing and you stand up and then slowly go to do the walking meditation, making

92
00:09:36,000 --> 00:09:41,000
sure that your mindfulness, your awareness, your clear awareness of the present moment,

93
00:09:41,000 --> 00:09:43,000
is continuous.

94
00:09:43,000 --> 00:09:44,000
So that's all for today.

95
00:09:44,000 --> 00:09:48,040
This is the practice of what we call mindful frustration.

96
00:09:48,040 --> 00:09:53,520
So I hope again that this practice brings you both peace, happiness, and clarity of mind.

97
00:09:53,520 --> 00:10:06,600
Thank you.

