1
00:00:00,000 --> 00:00:03,600
Hello and welcome back to our study of the Dhammapanda.

2
00:00:03,600 --> 00:00:10,600
Today, we continue on with verse number 35, which goes as follows.

3
00:00:10,600 --> 00:00:39,600
The Dhammapanda is very difficult to restrain flighty, landing upon or lighting upon,

4
00:00:39,600 --> 00:00:43,600
whatever it desires.

5
00:00:43,600 --> 00:00:47,600
The taming of the mind is good.

6
00:00:47,600 --> 00:00:53,600
The tamed mind brings happiness.

7
00:00:53,600 --> 00:01:04,600
So another really pointed teaching from a meditative point of view.

8
00:01:04,600 --> 00:01:07,600
And this one has very much to do with meditation.

9
00:01:07,600 --> 00:01:12,600
It's actually quite a story, well known among Buddhist circles.

10
00:01:12,600 --> 00:01:17,600
The story goes that there was a village called Matika.

11
00:01:17,600 --> 00:01:27,600
And there was a group of thirteen bikhus who went to practice meditation there.

12
00:01:27,600 --> 00:01:33,600
And the headmen of the village, his mother had great faith.

13
00:01:33,600 --> 00:01:40,600
And when she saw these Buddhist monks coming, she thought she was quite excited to see them.

14
00:01:40,600 --> 00:01:42,600
And that's the way they were going.

15
00:01:42,600 --> 00:01:46,600
They said, well, they're just looking for a place to spend the rains.

16
00:01:46,600 --> 00:01:49,600
And so she immediately jumped upon it and said,

17
00:01:49,600 --> 00:01:54,600
look, if you stay here, then I'll be able to take the precepts

18
00:01:54,600 --> 00:01:56,600
and follow your teeth.

19
00:01:56,600 --> 00:02:01,600
Listen to your teachings and learn more about the Buddhist teaching and have a chance.

20
00:02:01,600 --> 00:02:05,600
A very good chance to study and practice the Buddhist teaching.

21
00:02:05,600 --> 00:02:08,600
So she said, please stay here and we'll build you a monastery.

22
00:02:08,600 --> 00:02:12,600
And so a proof, just like that, they built off in the forest.

23
00:02:12,600 --> 00:02:17,600
A monastery for these monks with the hopes or under the promise

24
00:02:17,600 --> 00:02:20,600
that they would stay there for the rains.

25
00:02:20,600 --> 00:02:24,600
And they promised that they would bring them food every day

26
00:02:24,600 --> 00:02:28,600
and whatever they would bring for these monks.

27
00:02:28,600 --> 00:02:33,600
And so the monks were quite happy about this because it wouldn't mean that for the rains

28
00:02:33,600 --> 00:02:37,600
they would have a chance to practice meditation strenuously

29
00:02:37,600 --> 00:02:40,600
and put all their effort into the practice.

30
00:02:40,600 --> 00:02:45,600
And so once the monastery was finished, they all went in and they had a meeting amongst themselves

31
00:02:45,600 --> 00:02:49,600
as to how they were going to conduct themselves or the rains.

32
00:02:49,600 --> 00:02:52,600
And what they said, just incidentally,

33
00:02:52,600 --> 00:02:54,600
the things that they said were kind of interesting.

34
00:02:54,600 --> 00:03:02,600
One of them is quite pertinent. It sort of gives you an idea of the intentions that they had going into this.

35
00:03:02,600 --> 00:03:25,600
They say, the Pali is,

36
00:03:25,600 --> 00:03:54,600
they say, it would not be good for us to be pamanda, pamanda, pamanda, pamanda, taram, taritum,

37
00:03:54,600 --> 00:03:57,600
pamanda… not what the deep.

38
00:03:57,600 --> 00:04:05,600
It is not proper… it will not be that we… it is not the case that we should be negligent

39
00:04:05,600 --> 00:04:10,600
because the eight hells are wide open meaning they still had greed, anger and delusion.

40
00:04:10,600 --> 00:04:15,600
And so, if they were negligent, then these things would lead them into one of the eight great hells

41
00:04:15,600 --> 00:04:19,600
or… or could possibly, very least it would lead them to suffering.

42
00:04:19,600 --> 00:04:33,840
So for that reason, they made a vow amongst themselves out of this great anxiety or fear

43
00:04:33,840 --> 00:04:40,500
that they all had of the possibility of going to a bad place and out of a desire to become

44
00:04:40,500 --> 00:04:50,840
free from it. They made a very extreme sort of agreement, a very hard agreement with

45
00:04:50,840 --> 00:04:56,400
them, some strict agreement with themselves, that during the rains retreat, they would as

46
00:04:56,400 --> 00:05:01,240
best as possible not talk to each other. Now what it says that they did is in the morning

47
00:05:01,240 --> 00:05:05,160
they would meet for alms, so then they would maybe even discuss where they're going to go

48
00:05:05,160 --> 00:05:10,880
on alms, who's going to go on which route and who's going to go in which direction.

49
00:05:10,880 --> 00:05:18,680
And in the evening, they would meet together to pay respect to the elder monks and to do

50
00:05:18,680 --> 00:05:25,520
the duties towards whatever monk is elder and so on. But apart from that, they would not,

51
00:05:25,520 --> 00:05:31,360
they would say we will not meditate two monks in the same place. We will all, we will not

52
00:05:31,360 --> 00:05:36,640
go on alms, round two monks in the same road, we will not meet eye to eye and out of fear

53
00:05:36,640 --> 00:05:45,080
that we might begin to converse and therefore keep rise to defound it.

54
00:05:45,080 --> 00:05:51,320
And so with this promise, they undertook to themselves to spend all their time in their

55
00:05:51,320 --> 00:05:56,440
cooking. Now the one thing they did say, and if a monk is sick, the only exception is

56
00:05:56,440 --> 00:06:01,480
if a monk gets sick, then that monk should go out and ring the bell in the middle of the

57
00:06:01,480 --> 00:06:06,320
monastery and then all of us must come to attend on the sick monk. So that was the only

58
00:06:06,320 --> 00:06:10,120
one exception. Apart from that, they spent all their time, they decided they would spend

59
00:06:10,120 --> 00:06:16,080
all their time in their cooking so immediately they all went to their own heights and undertook

60
00:06:16,080 --> 00:06:21,680
the practice of meditation. Now the woman who was the mother of Matika, the mother of

61
00:06:21,680 --> 00:06:26,200
the village headmen, who was looking after them, was quite excited and wanted to come

62
00:06:26,200 --> 00:06:30,000
and make sure that everything was all right for the monks. So she decided that she would

63
00:06:30,000 --> 00:06:35,320
come to the monastery and see how her sons were doing as she put it. But when she got

64
00:06:35,320 --> 00:06:40,000
there, it was not as she expected and she expected that her sons would be happily enjoying

65
00:06:40,000 --> 00:06:46,760
their monastery and discussing the Buddhist teaching and so on and so on. And so when

66
00:06:46,760 --> 00:06:52,640
she saw these monks, she went to the monastery and she said, well, where are all the monks?

67
00:06:52,640 --> 00:06:56,720
And the man who was looking after the monastery said, well, they're often their goodies

68
00:06:56,720 --> 00:07:01,800
one by one. They're all in different places and she saw that this was the case that they

69
00:07:01,800 --> 00:07:09,240
were all avoiding each other. So she said, what can I do if I want to talk to them? And

70
00:07:09,240 --> 00:07:12,880
he said, well, ring the bell and so she went up to the bell and she rang it. And she

71
00:07:12,880 --> 00:07:17,280
watched and they all came from different places and she said, sure enough, they're all avoiding

72
00:07:17,280 --> 00:07:22,160
each other. And she thought, this was the most peculiar behavior. So they said, they said,

73
00:07:22,160 --> 00:07:27,560
what's wrong? Without someone was maybe sick. And she said, no, I'm wondering why it is

74
00:07:27,560 --> 00:07:32,920
that you're not talking to each other. Why are you avoiding each other? And she said,

75
00:07:32,920 --> 00:07:37,080
you must be angry at each other. There must be some problem with your harmony. And here

76
00:07:37,080 --> 00:07:40,760
I was hoping to gain lots of merit having you here. But now I'm afraid that there may

77
00:07:40,760 --> 00:07:44,840
be some problem. And they said, no, no, it's not the case for practicing meditation. And

78
00:07:44,840 --> 00:07:51,920
when you practice meditation, you have to be in solitude. And she said, what does this mean

79
00:07:51,920 --> 00:07:56,040
practice meditation? She had no idea what this was. And she said, well, practice meditation

80
00:07:56,040 --> 00:08:01,200
for in our case, we're thinking of this the parts of the body. We say to ourselves,

81
00:08:01,200 --> 00:08:08,280
hair, hair, and skin, skin, and so on, focusing on the 32 parts of the body. And as a result,

82
00:08:08,280 --> 00:08:12,240
developing common tranquility and then developing insight based on that, this is what we've

83
00:08:12,240 --> 00:08:18,720
been taught to do. And she said, oh, that sounds, she said, oh, that's that's quite interesting

84
00:08:18,720 --> 00:08:22,640
actually. And she said, is it only monks who can practice this teaching? And he said,

85
00:08:22,640 --> 00:08:26,840
no, no, they said, no, no, anyone can practice this teaching. And she said, well, then

86
00:08:26,840 --> 00:08:32,880
can you teach it to me? And so they taught her these 32 parts of the body. And to just get

87
00:08:32,880 --> 00:08:38,960
to the end of the story, she practiced diligently and actually gained the third stage of

88
00:08:38,960 --> 00:08:45,160
enlightenment on the gami. She had no more sensual desire, no more anger. But she would

89
00:08:45,160 --> 00:08:51,200
still have these subtle desires to become and or to not become desire to get rid of things

90
00:08:51,200 --> 00:08:54,960
and then desire to achieve things to be this and to not be that. And she still would have

91
00:08:54,960 --> 00:09:00,880
conceited and so on. Some of the subtle defines, but she was totally freed from the lower

92
00:09:00,880 --> 00:09:08,640
defilements of lust and craving and so on. And anger and aversion. And she said, wow,

93
00:09:08,640 --> 00:09:13,920
this is wonderful. She said, well, I wonder, when did these monks attain this state? And

94
00:09:13,920 --> 00:09:19,720
she used her, these powers that she gained from as a result of her enlightenment. She

95
00:09:19,720 --> 00:09:24,840
to send her mind out and to examine these monks. And she said, these monks are still full

96
00:09:24,840 --> 00:09:28,000
of greed, anger and delusion. They haven't gained anything. She said, what's wrong? They've

97
00:09:28,000 --> 00:09:32,840
been here for some time practicing meditation. I wonder what can be wrong. And she looked

98
00:09:32,840 --> 00:09:38,080
and she said, do they have suitable lodging? Yes, they have suitable lodging. Do they have

99
00:09:38,080 --> 00:09:43,680
suitable company? Are there any problems in between them? No, there's no problems between

100
00:09:43,680 --> 00:09:49,040
them. Do they have suitable food? And she said, aha, they don't have suitable food.

101
00:09:49,040 --> 00:09:52,840
And so she thought to herself, you know, she sent her mind to all the monks and she thought

102
00:09:52,840 --> 00:09:58,560
which monk needs what sort of food. And she realized what sort of food that the monks

103
00:09:58,560 --> 00:10:03,960
wanted and what sort of food would make them, would bring them comfort in their meditation.

104
00:10:03,960 --> 00:10:08,360
And so she prepared these foods for them and apart from them prepared everything for them.

105
00:10:08,360 --> 00:10:12,720
Whatever they needed, whatever they were lacking or whatever she knew for herself would

106
00:10:12,720 --> 00:10:17,920
help them. She brought it to them. And as a result, the monks were all able to attain

107
00:10:17,920 --> 00:10:23,840
our hardship. As a result of attaining our hardship, they said to themselves, wow, at the

108
00:10:23,840 --> 00:10:27,280
end of the reigns, they said to themselves, the time has come for us to go and to see the

109
00:10:27,280 --> 00:10:31,960
Buddha. And so they went back to see the Buddha and the Buddha said, oh, you all look bright

110
00:10:31,960 --> 00:10:37,160
and radiant. You must have had suitable lodging and food and so on. And they said, oh, did

111
00:10:37,160 --> 00:10:43,720
we ever? This laywoman, she was, she attained Anagami and she was able to read our minds

112
00:10:43,720 --> 00:10:49,240
and she got us whatever we want and the Buddha said, oh, that's great, good for you and so

113
00:10:49,240 --> 00:10:54,120
on. There was something profound, of course. They had become Arahan. Now, the story goes

114
00:10:54,120 --> 00:11:01,280
that there was one monk listening to this and overheard the Buddha talking about, talking

115
00:11:01,280 --> 00:11:05,080
to these monks and overheard these monks exstalling the virtues of this laywoman who was

116
00:11:05,080 --> 00:11:13,360
so, so perfect or such a clear mind that she was actually able to attain enlightenment

117
00:11:13,360 --> 00:11:18,800
before these monks. But what he's got stuck up on is he said, this, he heard that this

118
00:11:18,800 --> 00:11:24,560
woman was bringing these monks whatever they want. And he was so single-handedly taking

119
00:11:24,560 --> 00:11:30,680
care of 30 monks and as a result allowed them to attain our hardship and kind of about

120
00:11:30,680 --> 00:11:35,400
agreed, he thought, wow, if I go there, I'll get whatever I want. If I want water, I'll

121
00:11:35,400 --> 00:11:39,960
get water, if I want food, I'll get food, whatever kind of food I want, I'll get it. And

122
00:11:39,960 --> 00:11:43,440
so he asked permission to go and say, there are, I'm not even sure if he asked permission,

123
00:11:43,440 --> 00:11:49,120
he just immediately went to this monastery and he said, okay, if this woman can read my mind,

124
00:11:49,120 --> 00:11:55,360
may she send someone to clean up this monastery for me because it would have been all

125
00:11:55,360 --> 00:12:01,600
covered over and leaves and so on. And immediately this man shows up and or not immediately

126
00:12:01,600 --> 00:12:07,760
but after some time this man shows up and starts sweeping the monastery. Wow, that's quite

127
00:12:07,760 --> 00:12:13,040
incredible. And so he thinks, there's no water here, may she send some sweet water and

128
00:12:13,040 --> 00:12:18,320
sudden and after some time someone comes with some sweet water and then she thinks he thinks

129
00:12:18,320 --> 00:12:23,920
oh, and in the morning, may she bring me this kind of good food and, and, and, yeah,

130
00:12:23,920 --> 00:12:31,680
go or, or congee or so on with sugar and this and that and dainties and stuff like that.

131
00:12:31,680 --> 00:12:35,920
May she bring me exactly what I want and she brings exactly what he thinks or he thinks

132
00:12:35,920 --> 00:12:40,080
of and then for lunch the same thing and then he says, wow, this is incredible. I mean,

133
00:12:40,080 --> 00:12:46,160
really like to meet this woman. And so she and and and within a no long time she comes

134
00:12:46,160 --> 00:12:52,200
to see him and he asks her, he says, wow, is it true that you can read people's minds

135
00:12:52,200 --> 00:12:56,920
in this way? And she said, why do you ask? And he said, well, you know, people send

136
00:12:56,920 --> 00:13:02,200
this and she said, oh, and she she did her best. She avoided the question completely

137
00:13:02,200 --> 00:13:06,160
and he thought to himself, he thought for sure she can read my mind because she isn't

138
00:13:06,160 --> 00:13:12,240
saying no, but because of her purity, she doesn't want to say yes. And he said, what

139
00:13:12,240 --> 00:13:17,480
have I done? What am I doing here? Suddenly it just the the gravity of the situation occurred

140
00:13:17,480 --> 00:13:23,440
to him. He said, if I have one thought of lust, imagine this woman is going to know right

141
00:13:23,440 --> 00:13:27,680
away. And so she'll probably grab me by my robe and throw me out of the monster if she finds

142
00:13:27,680 --> 00:13:33,880
out all the all the defilements that I have in my mind. And so the rest of the day you

143
00:13:33,880 --> 00:13:38,840
just was was walking around paranoid or thinking, oh, I can't stay here. I can't stay

144
00:13:38,840 --> 00:13:45,080
ran back to the the monastery where the Buddha was. The Buddha saw him coming and said,

145
00:13:45,080 --> 00:13:49,640
you know, where why are you staying at that monastery? And he said, I can't stay there.

146
00:13:49,640 --> 00:13:56,200
If I have one impure my thought, this woman's going to know it right away. And then

147
00:13:56,200 --> 00:13:59,760
the Buddha and then the Buddha gives this verse. This is the teaching that he gives

148
00:13:59,760 --> 00:14:04,080
to this monk. He says, look, you don't have to guard your this thought or that thought.

149
00:14:04,080 --> 00:14:08,160
Can you can you do one thing? And he said, what is the one thing? And he said, you just

150
00:14:08,160 --> 00:14:13,640
guard your mind. Watch your mind. Stay with the mind and train your name, your mind.

151
00:14:13,640 --> 00:14:22,040
The word is to tame the mind. Just just go, you know, go back there. Train your mind and overcome

152
00:14:22,040 --> 00:14:29,320
these, these thoughts work on them. Because really the truth is, you know, this woman

153
00:14:29,320 --> 00:14:32,560
wouldn't have thrown out the monastery. As an amnigami, she would have known very, very

154
00:14:32,560 --> 00:14:36,680
well the defilements that were in his mind. And she would never complain at all. And she

155
00:14:36,680 --> 00:14:43,400
would always work in a way as we all do to help people to overcome these. We never get

156
00:14:43,400 --> 00:14:47,440
angry or upset at students when they come and tell us they have lust. In fact, because

157
00:14:47,440 --> 00:14:50,840
we can't read their students minds, we need our students to tell us, you know, that

158
00:14:50,840 --> 00:14:56,160
lust arose or anger arose or so on. Mahasi Saiyatta was saying, like this, you're saying,

159
00:14:56,160 --> 00:15:02,160
well, it would be great if you could have a teacher like that who could read your minds.

160
00:15:02,160 --> 00:15:06,720
But if you don't have one, all you have to do is explain your situation or it becomes

161
00:15:06,720 --> 00:15:13,440
your duty to explain your situation. That is the answer. The answer is for you just to tell

162
00:15:13,440 --> 00:15:19,480
what is going on in your practice. Because the problem as I've always said is not this

163
00:15:19,480 --> 00:15:25,280
greed and not the anger, it's the delusion. It's our ignorance, our inability to understand

164
00:15:25,280 --> 00:15:30,560
the situation. The fact that we don't see that we have these or we don't see the problem

165
00:15:30,560 --> 00:15:35,440
with these things. The fact that we don't understand these things as they are. You know,

166
00:15:35,440 --> 00:15:39,240
if we actually look at them, accept that they're there and take a look at our last, take

167
00:15:39,240 --> 00:15:43,640
a look at our desire, take a look at our anger, take a look at our aversion or frustration

168
00:15:43,640 --> 00:15:50,280
or boredom or depression, look at it. And simply see it for what it is. Then we'll be able

169
00:15:50,280 --> 00:15:54,720
to become only then. We'll be able to become free from it. So rather than worrying about

170
00:15:54,720 --> 00:15:59,720
these things or what other people think of us or anything, let us just, he said, all

171
00:15:59,720 --> 00:16:04,080
you have to do is train your mind, straighten your mind as we were talking about before.

172
00:16:04,080 --> 00:16:09,480
But here he uses the word to tame the mind or to train the mind. And then he said this

173
00:16:09,480 --> 00:16:16,240
verse, he said, the mind is difficult to restrain and flighting, lighting upon whatever

174
00:16:16,240 --> 00:16:23,240
it desires. The taming of the mind is good. The tamed mind brings happiness. That's

175
00:16:23,240 --> 00:16:29,240
the verse. And the story goes that as a result, he didn't go back and became enlightened

176
00:16:29,240 --> 00:16:37,160
as a result. So for our practice, I think it's quite obvious and it's quite obvious in this,

177
00:16:37,160 --> 00:16:43,640
in this example, as to the importance or the meaning of this verse. But yes, this is

178
00:16:43,640 --> 00:16:49,640
the case. The mind will run after. It's desires. It will run after what it wants if it

179
00:16:49,640 --> 00:16:56,080
wants or if it wants something. And it will chase after it. And the commentary here has

180
00:16:56,080 --> 00:17:03,160
something interesting to say. It says, so much so that it won't worry about its reputation.

181
00:17:03,160 --> 00:17:07,200
And it won't worry about what is right or what is wrong. It won't worry about things

182
00:17:07,200 --> 00:17:12,720
like, in the case of lust, someone else's wife, someone else's husband, it won't worry

183
00:17:12,720 --> 00:17:17,400
about things like someone else's possession. It won't worry about suffering for oneself,

184
00:17:17,400 --> 00:17:22,720
for oneself, it won't worry about suffering for others, if one is angry. Even though one

185
00:17:22,720 --> 00:17:26,680
knows that one is hurting oneself, one will kick things and punch things, punching the

186
00:17:26,680 --> 00:17:31,400
wall and causing great suffering to oneself. This is the meaning of the word,

187
00:17:31,400 --> 00:17:37,360
yatakama, nipati, you know, whatever it wants, no matter what. Whatever the defilements lead

188
00:17:37,360 --> 00:17:42,720
one to, it will go there no matter what. In complete disregard for what is right or

189
00:17:42,720 --> 00:17:50,080
wrong. So this is why we see infidelity and this is why we have theft, this is why we have

190
00:17:50,080 --> 00:17:57,800
murder, this is why we have suicide, all of these things, because of our desires and

191
00:17:57,800 --> 00:18:03,880
our versions and our inability to control them. So rather than control them, the Buddha

192
00:18:03,880 --> 00:18:09,080
says to train the mind, to train the mind, to see them clearly, to see them as they are.

193
00:18:09,080 --> 00:18:13,280
This is the word vipasana, just to see them from what they are, because you can't,

194
00:18:13,280 --> 00:18:17,920
the Buddha says you can't restrain the mind, you can't tie it down and stop it from going

195
00:18:17,920 --> 00:18:24,680
these things. It's lahu, the word is lahu, which means light or flighty, like a bird.

196
00:18:24,680 --> 00:18:28,440
These words are actually what you would use to talk about a bird, the last time we had

197
00:18:28,440 --> 00:18:33,320
a fish now we have a bird. It's like a bird because it lights on, on whatever, whatever

198
00:18:33,320 --> 00:18:36,920
it wishes. If it doesn't like it here, it will go there, it won't go here. As soon

199
00:18:36,920 --> 00:18:42,480
as it wants, as soon as something scares it, it will be gone. This is the mind, you can't,

200
00:18:42,480 --> 00:18:47,560
you can't catch it. It's not a physical thing that you can put in the cage. If it were

201
00:18:47,560 --> 00:18:52,120
the body, you can tie the body up, no, you can cover your mouth, you can plug your ears,

202
00:18:52,120 --> 00:18:57,480
you can close your eyes, you can stop up your nose, you can stop, you can, the body you

203
00:18:57,480 --> 00:19:02,720
can stop, but the mind you can't stop, no matter if you do all of these things, the mind

204
00:19:02,720 --> 00:19:07,480
will still escape to cause you endless endless grief and suffering. In fact, if you do such

205
00:19:07,480 --> 00:19:13,920
a thing, you'll find for most people that just go crazy as a result. This is the funny

206
00:19:13,920 --> 00:19:17,120
thing, because you think, you know, you can control it and the more you try to control

207
00:19:17,120 --> 00:19:21,760
your mind, maybe you try to take medicine, some people when they have headaches or when

208
00:19:21,760 --> 00:19:26,000
they're thinking too much, they'll take pills or drugs and the more you take them, the

209
00:19:26,000 --> 00:19:32,240
worse it gets and the more wound up the mind becomes. So the more you try to tie the mind

210
00:19:32,240 --> 00:19:37,960
down, the more, the more of a problem your mind becomes. So this is the Buddha taught us

211
00:19:37,960 --> 00:19:41,960
to train the mind to see clearly. This is in the last one we're talking about straight

212
00:19:41,960 --> 00:19:53,240
in the mind, but the word tame here or dhamma or danta, this means the, making the mind

213
00:19:53,240 --> 00:20:02,280
like a trained animal, like a horse or a buffalo or something, something that is trained

214
00:20:02,280 --> 00:20:08,960
and understands what is the important thing to do. So teaching the mind to understand

215
00:20:08,960 --> 00:20:13,840
what is of its benefit, what is of its own benefit and what is to its detriment, understanding

216
00:20:13,840 --> 00:20:20,920
the anger, understanding the greed, seeing it for what it is. So this is very much a reminder

217
00:20:20,920 --> 00:20:27,440
to us that the untrained mind is the cause for all of our suffering and it's simply by

218
00:20:27,440 --> 00:20:31,120
training the mind that it leads to happiness. The first kind of happiness, of course,

219
00:20:31,120 --> 00:20:35,840
that it leads to is that it overcomes, helps us to overcome suffering. So as these monks

220
00:20:35,840 --> 00:20:40,640
said, it was the untrained mind that had agreed, that had anger, that had delusion, that

221
00:20:40,640 --> 00:20:45,680
was, that had opened up the hells in front of them, that they could even see in front

222
00:20:45,680 --> 00:20:51,160
of them, that if they were to just follow their desires, follow their versions, it's open

223
00:20:51,160 --> 00:20:55,680
like the doors, the doors of their own hells with a big welcome that in front. This is what

224
00:20:55,680 --> 00:21:00,400
they've said. So the training of the mind, the taming of the mind stops that, it stops the

225
00:21:00,400 --> 00:21:06,560
mind from exploding, it stops the mind from going insane, it stops the mind from following

226
00:21:06,560 --> 00:21:11,840
after desires and the versions and causing so much suffering. We can see it, even when people

227
00:21:11,840 --> 00:21:16,480
get angry, they kick things and they punch things as they said or they hurt other people.

228
00:21:16,480 --> 00:21:21,800
How we destroy our lives, even in this world you can understand how training the mind taming

229
00:21:21,800 --> 00:21:27,560
the mind brings happiness. If you think of how the world, mostly people think that happiness

230
00:21:27,560 --> 00:21:33,960
comes from war, from conflict and from victory. So what is the result of that? We see

231
00:21:33,960 --> 00:21:39,600
how the world has gone to war and when the world is at war, so little peace in the world.

232
00:21:39,600 --> 00:21:50,040
The peace vanishes like a mirage and what you're left with is conflict and the victor

233
00:21:50,040 --> 00:22:01,120
gets the ashes of the spoils. What you get is a world, a rubble and fire and suffering.

234
00:22:01,120 --> 00:22:06,000
The suffering that comes from war is something that we have all been told about and read

235
00:22:06,000 --> 00:22:16,320
about and heard the records of even in our society. When we look at capitalism or materialism

236
00:22:16,320 --> 00:22:22,640
or when we look at this competitive society, what it has brought to us? How much suffering,

237
00:22:22,640 --> 00:22:32,120
how much poverty and how much lack of trust this has brought to the world. How we can't

238
00:22:32,120 --> 00:22:38,560
even trust our neighbors, we can't trust our community, we can't trust the people at

239
00:22:38,560 --> 00:22:43,360
the stores, we can't trust the banks, we can't trust anybody. We can no longer trust each

240
00:22:43,360 --> 00:22:49,040
other because our minds are not trained, because our minds are full of these things.

241
00:22:49,040 --> 00:22:58,280
Even in the family, the untrained mind amongst family, friends at work, if you talk to anyone

242
00:22:58,280 --> 00:23:02,840
about their job situation and how it's going almost anyone in the world will be able to

243
00:23:02,840 --> 00:23:07,040
tell you about the fighting and the politics that goes on at work and if you ask people

244
00:23:07,040 --> 00:23:12,640
about their home life, most people nowadays because of the nature of our minds and our desires

245
00:23:12,640 --> 00:23:21,280
and everyone's conflicting desires, the anger and the fighting and the harsh words, even

246
00:23:21,280 --> 00:23:26,240
the abuse that goes on as a result of our untrained mind, how people even know they want

247
00:23:26,240 --> 00:23:31,760
to find happiness and they want others to be happy as well. They end up bringing suffering,

248
00:23:31,760 --> 00:23:39,120
suffering to both themselves and to others. This is all from the untrained mind. If we begin

249
00:23:39,120 --> 00:23:47,280
to practice meditation and then we consider our own lives, because of our stability of

250
00:23:47,280 --> 00:23:52,560
mind that comes in the ability to reflect clearly on what we've come from, we'll be able

251
00:23:52,560 --> 00:24:00,320
to see the suffering that we've been causing in our lives. It become quite clear to us

252
00:24:01,120 --> 00:24:05,280
where all of our suffering comes from and it'll become clear to us that we didn't

253
00:24:05,280 --> 00:24:11,840
read that we were in so much suffering that we didn't even realize. We thought we were living in

254
00:24:11,840 --> 00:24:17,280
great peace and more or less lying to ourselves. Once you begin to practice meditation, you start

255
00:24:17,280 --> 00:24:21,520
just, all this comes up in much guilt about all of the things that you've done to her

256
00:24:21,520 --> 00:24:26,960
others when you were just avoiding it the whole time and pretending it didn't exist and now

257
00:24:26,960 --> 00:24:29,920
as your mind starts to calm down, you get to see everything that's in there.

258
00:24:29,920 --> 00:24:36,240
So, for someone who's practiced meditation, this is a clear teaching that it is the trained mind,

259
00:24:37,200 --> 00:24:43,360
the tamed mind, the straight in mind that brings happiness. So, it's a good teaching for us

260
00:24:43,360 --> 00:24:50,160
to always remember, jit dang dang tang sukha wa hang, it's one that is often taught in Buddhist circles,

261
00:24:50,160 --> 00:24:58,000
that the trained or the tamed mind brings happiness. So, just one more verse in our series on

262
00:24:58,000 --> 00:25:13,920
the dambapada and thank you all for tuning in and we'll see you in the next time.

