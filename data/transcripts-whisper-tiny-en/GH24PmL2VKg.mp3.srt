1
00:00:00,000 --> 00:00:07,000
How do I meditate when I don't want to meditate?

2
00:00:07,000 --> 00:00:08,000
Basically.

3
00:00:08,000 --> 00:00:13,000
I know it sounds like a frivolous question, but it's a serious issue for me.

4
00:00:13,000 --> 00:00:19,000
When I sit down, I can do it for like a short time, but eventually it's like, you know,

5
00:00:19,000 --> 00:00:20,000
I get frustrated.

6
00:00:20,000 --> 00:00:21,000
My muscles cramp up.

7
00:00:21,000 --> 00:00:23,000
It feels like my head is shaking.

8
00:00:23,000 --> 00:00:25,000
It becomes just really difficult.

9
00:00:25,000 --> 00:00:29,000
So, I end up just like, you know, getting out, you know, whatever it is I'm making excuse,

10
00:00:29,000 --> 00:00:31,000
or it really gets very difficult.

11
00:00:31,000 --> 00:00:34,000
So, I guess my ego is like against it or something.

12
00:00:34,000 --> 00:00:36,000
Yeah.

13
00:00:36,000 --> 00:00:37,000
I guess, I don't know.

14
00:00:37,000 --> 00:00:39,000
I hope it's not a frivolous question.

15
00:00:39,000 --> 00:00:40,000
No, no, it's true.

16
00:00:40,000 --> 00:00:42,000
But I'm just, you know, sitting through it.

17
00:00:42,000 --> 00:00:46,000
But, you know, either I don't have the willpower, or I'm just lazy.

18
00:00:46,000 --> 00:00:48,000
So, is there any like practical advice for that?

19
00:00:48,000 --> 00:00:53,000
I think there's something we miss and that's the forcing ourselves to meditate,

20
00:00:53,000 --> 00:00:58,000
which can be, you know, it's not always the best idea,

21
00:00:58,000 --> 00:01:03,000
because you will cultivate a version to meditation.

22
00:01:03,000 --> 00:01:05,000
And that's no good.

23
00:01:05,000 --> 00:01:09,000
That being said, in the beginning, you don't know what's good for you.

24
00:01:09,000 --> 00:01:16,000
You can think of it as the learning process of a adolescent,

25
00:01:16,000 --> 00:01:20,000
you know, as a child, you have to force them to learn.

26
00:01:20,000 --> 00:01:27,000
You have to push them to learn, not force, but you have to at least direct them.

27
00:01:27,000 --> 00:01:32,000
Even still, forcing doesn't generally have the desired effect.

28
00:01:32,000 --> 00:01:37,000
And as a result, parents who push their children too hard find them break and,

29
00:01:37,000 --> 00:01:45,000
you know, rebel or worse can, you know, destroy their lives out of just the intense stress.

30
00:01:45,000 --> 00:01:47,000
But you should look at it that way.

31
00:01:47,000 --> 00:01:51,000
So, you know, it's a kind for all intents and purposes where we're children.

32
00:01:51,000 --> 00:01:53,000
You know, we never really grow up.

33
00:01:53,000 --> 00:01:55,000
Many of us never really grow up.

34
00:01:55,000 --> 00:01:57,000
And many parts of us never really grow up.

35
00:01:57,000 --> 00:02:02,000
The spiritual part for most of us is quite young.

36
00:02:02,000 --> 00:02:06,000
Especially if you acknowledge that you don't like to meditate,

37
00:02:06,000 --> 00:02:09,000
then you have to treat yourself as a child.

38
00:02:09,000 --> 00:02:13,000
You know, you have a child's mind, which is for most of us the case.

39
00:02:13,000 --> 00:02:18,000
And so in the beginning, you have to be a little bit insistent.

40
00:02:18,000 --> 00:02:21,000
And if you think of it as a child, how would you teach a child?

41
00:02:21,000 --> 00:02:27,000
You obviously, hopefully you wouldn't beat the child or force the child to,

42
00:02:27,000 --> 00:02:34,000
you know, into the extent that it caused the child's stress and repression.

43
00:02:34,000 --> 00:02:38,000
But on the other hand, you wouldn't just let the child sit around eating candy all day.

44
00:02:38,000 --> 00:02:44,000
Or watching cartoons all day, you would try to direct them in wholesome direction.

45
00:02:44,000 --> 00:02:47,000
So, there's no easy answer in the beginning.

46
00:02:47,000 --> 00:02:52,000
Except to answer, no, don't simply force yourself to meditate.

47
00:02:52,000 --> 00:02:55,000
You have to understand that you're still a child,

48
00:02:55,000 --> 00:02:58,000
and you're not going to want to meditate.

49
00:02:58,000 --> 00:03:01,000
On the other hand, don't say, well, then I just won't meditate.

50
00:03:01,000 --> 00:03:03,000
Obviously, I'm sure you know that that's not the case.

51
00:03:03,000 --> 00:03:05,000
Otherwise, you wouldn't be asking this question.

52
00:03:05,000 --> 00:03:06,000
You want to meditate.

53
00:03:06,000 --> 00:03:09,000
You just don't want to meditate.

54
00:03:09,000 --> 00:03:12,000
You see the problem, you know, it's good for you.

55
00:03:12,000 --> 00:03:14,000
You're all grown up intellectually.

56
00:03:14,000 --> 00:03:19,000
But part of you is still, you know, a child.

57
00:03:19,000 --> 00:03:25,000
A big part of it is our disconnect with the actual benefits of meditation.

58
00:03:25,000 --> 00:03:32,000
We're not machines where you can input the benefits of meditation and have that stick.

59
00:03:32,000 --> 00:03:37,000
You can tell yourself, you can listen to a talk on how great meditation isn't just say,

60
00:03:37,000 --> 00:03:39,000
yeah, meditation is awesome.

61
00:03:39,000 --> 00:03:40,000
I want to meditate.

62
00:03:40,000 --> 00:03:44,000
Ten minutes later, you'll forgotten it, and it just won't be in your psyche.

63
00:03:44,000 --> 00:03:45,000
It won't be an input.

64
00:03:45,000 --> 00:03:48,000
So, you'll be saying, why am I doing this again?

65
00:03:48,000 --> 00:03:51,000
You know, you just don't have any desire for it,

66
00:03:51,000 --> 00:03:54,000
which is kind of absurd because just the moment, you know, ten minutes ago,

67
00:03:54,000 --> 00:03:56,000
you were clear, meditation is awesome.

68
00:03:56,000 --> 00:03:58,000
I'm clear that it's a good thing.

69
00:03:58,000 --> 00:03:59,000
It's really going to help me.

70
00:03:59,000 --> 00:04:02,000
That can go on in the meditation course.

71
00:04:02,000 --> 00:04:03,000
You'll be practicing.

72
00:04:03,000 --> 00:04:05,000
You'll see, wow, this is so wonderful.

73
00:04:05,000 --> 00:04:07,000
The meditation just helps me so much.

74
00:04:07,000 --> 00:04:09,000
And then the next day, you're like, I want to leave.

75
00:04:09,000 --> 00:04:10,000
This is useless.

76
00:04:10,000 --> 00:04:12,000
This is what am I doing?

77
00:04:12,000 --> 00:04:14,000
Because we're organic.

78
00:04:14,000 --> 00:04:17,000
This is because the mind is biological.

79
00:04:17,000 --> 00:04:20,000
It's certainly not biologically.

80
00:04:20,000 --> 00:04:24,000
But basically, the mind is a part of the organism.

81
00:04:24,000 --> 00:04:29,000
And it's not like a computer.

82
00:04:29,000 --> 00:04:33,000
It's going to act irrationally.

83
00:04:33,000 --> 00:04:40,000
And so, a part of it, you know, part of it is going to be stepping back

84
00:04:40,000 --> 00:04:42,000
and saying, okay, yes, I don't want to meditate.

85
00:04:42,000 --> 00:04:47,000
I acknowledge that and not ignoring that and pushing on.

86
00:04:47,000 --> 00:04:51,000
I guess the meaning is, except that part, except,

87
00:04:51,000 --> 00:04:56,000
or not except, but observe and acknowledge is the word,

88
00:04:56,000 --> 00:04:59,000
acknowledge that part that I don't want to meditate.

89
00:04:59,000 --> 00:05:02,000
And you'll find, if you can truly acknowledge that,

90
00:05:02,000 --> 00:05:05,000
that you're able to actually meditate on it.

91
00:05:05,000 --> 00:05:09,000
You'll meditate on the frustration, meditate on the disliking.

92
00:05:09,000 --> 00:05:11,000
And you find it evaporates.

93
00:05:11,000 --> 00:05:15,000
At the same time, as you do that, as you accept or not accept

94
00:05:15,000 --> 00:05:18,000
acknowledge, you know, and focus more on the fact

95
00:05:18,000 --> 00:05:22,000
that you don't want to meditate than on actually meditating through it.

96
00:05:22,000 --> 00:05:27,000
At the same time, you know, re-ask yourself the question,

97
00:05:27,000 --> 00:05:30,000
honestly and openly, is meditation good for me?

98
00:05:30,000 --> 00:05:34,000
Bring yourself back to that question that you've probably answered in the past,

99
00:05:34,000 --> 00:05:37,000
but answer it again for yourself.

100
00:05:37,000 --> 00:05:38,000
Why am I doing this?

101
00:05:38,000 --> 00:05:39,000
You know, is it really good for me?

102
00:05:39,000 --> 00:05:42,000
And don't just accept some answer that you heard on the internet,

103
00:05:42,000 --> 00:05:44,000
but really ask yourself, you know, what do I want in life?

104
00:05:44,000 --> 00:05:46,000
Is this really going to benefit me?

105
00:05:46,000 --> 00:05:49,000
And sometimes that takes a little bit of soul searching,

106
00:05:49,000 --> 00:05:52,000
soul searching, bit of introspection, you know,

107
00:05:52,000 --> 00:05:57,000
saying, you know, no, I really just want to play sports

108
00:05:57,000 --> 00:06:01,000
and have sex and eat food all day.

109
00:06:01,000 --> 00:06:07,000
But then you say, okay, yes, I acknowledge I want all that,

110
00:06:07,000 --> 00:06:10,000
but really, and then you say to yourself,

111
00:06:10,000 --> 00:06:13,000
then if you're honest with yourself, you say,

112
00:06:13,000 --> 00:06:16,000
but no, that doesn't actually satisfy me.

113
00:06:16,000 --> 00:06:18,000
That isn't actually benefiting me.

114
00:06:18,000 --> 00:06:23,000
And so eventually coming to the conclusion of meditation

115
00:06:23,000 --> 00:06:25,000
is something I want to do.

116
00:06:25,000 --> 00:06:32,000
So there's room for having this sort of introspection and reflection.

117
00:06:32,000 --> 00:06:36,000
Sort of we monks, we monks, it means adjusting,

118
00:06:36,000 --> 00:06:40,000
you know, reflecting on your activity

119
00:06:40,000 --> 00:06:45,000
and ensuring that you're directing yourself in the right way.

120
00:06:45,000 --> 00:06:49,000
So I guess two parts, you know, try to be very, very aware

121
00:06:49,000 --> 00:06:53,000
of the aversion to the meditation,

122
00:06:53,000 --> 00:06:57,000
understanding that after some time you really,

123
00:06:57,000 --> 00:07:01,000
if you do grow up, and again,

124
00:07:01,000 --> 00:07:03,000
this isn't an insult.

125
00:07:03,000 --> 00:07:05,000
Most of us are in this situation.

126
00:07:05,000 --> 00:07:09,000
As you grow up, you'll want to meditate more.

127
00:07:09,000 --> 00:07:12,000
But understand that in the beginning,

128
00:07:12,000 --> 00:07:15,000
you're going to have to finesse.

129
00:07:15,000 --> 00:07:17,000
You're going to have to play games,

130
00:07:17,000 --> 00:07:20,000
to help the child grow up.

131
00:07:20,000 --> 00:07:28,000
And on the other side also have these kind of philosophical

132
00:07:28,000 --> 00:07:32,000
conversations with yourself about,

133
00:07:32,000 --> 00:07:35,000
I don't know if it's so much a conversation,

134
00:07:35,000 --> 00:07:37,000
but it's still just an acknowledgement.

135
00:07:37,000 --> 00:07:41,000
You have to accept the argument, like a judge.

136
00:07:41,000 --> 00:07:43,000
You can't just ignore one side,

137
00:07:43,000 --> 00:07:47,000
because you know this side is guilty.

138
00:07:47,000 --> 00:07:49,000
You can't just ignore them.

139
00:07:49,000 --> 00:07:52,000
I'm only going to listen to the prosecution.

140
00:07:52,000 --> 00:07:55,000
You'll be a mistrial, you see.

141
00:07:55,000 --> 00:07:59,000
It's not the outcome isn't no one is sure.

142
00:07:59,000 --> 00:08:00,000
No one is going to come out and say,

143
00:08:00,000 --> 00:08:03,000
yes, it was proven that this person was guilty.

144
00:08:03,000 --> 00:08:06,000
You didn't even give them a chance to present their caves.

145
00:08:06,000 --> 00:08:07,000
The mind is like that.

146
00:08:07,000 --> 00:08:09,000
If you don't give the defilements,

147
00:08:09,000 --> 00:08:12,000
they're a chance to speak.

148
00:08:12,000 --> 00:08:15,000
Maybe not quite so much,

149
00:08:15,000 --> 00:08:18,000
but in a sense,

150
00:08:18,000 --> 00:08:22,000
once the aversion, for example,

151
00:08:22,000 --> 00:08:25,000
to meditation arises,

152
00:08:25,000 --> 00:08:27,000
it's too late.

153
00:08:27,000 --> 00:08:28,000
It's already unwholesome.

154
00:08:28,000 --> 00:08:30,000
So pushing it away isn't going to help.

155
00:08:30,000 --> 00:08:31,000
This is why the Buddha said,

156
00:08:31,000 --> 00:08:34,000
when there's aversion arises in the mind,

157
00:08:34,000 --> 00:08:37,000
the key is to see that aversion has arisen in the mind,

158
00:08:37,000 --> 00:08:39,000
not to judge it at all.

159
00:08:39,000 --> 00:08:40,000
This is how a judge works.

160
00:08:40,000 --> 00:08:41,000
They don't judge.

161
00:08:41,000 --> 00:08:42,000
They observe.

162
00:08:42,000 --> 00:08:45,000
And they come to a decision,

163
00:08:45,000 --> 00:08:46,000
based on the facts,

164
00:08:46,000 --> 00:08:49,000
not based on any kind of judgment.

165
00:08:49,000 --> 00:08:54,000
So that's really what you have to do.

166
00:08:54,000 --> 00:08:56,000
And you'll feel this.

167
00:08:56,000 --> 00:09:00,000
You'll feel that the difference between forcing yourself

168
00:09:00,000 --> 00:09:01,000
to do something,

169
00:09:01,000 --> 00:09:06,000
and it's like how religious people feel

170
00:09:06,000 --> 00:09:09,000
when they're kind of believing in God

171
00:09:09,000 --> 00:09:11,000
just because they're afraid to go to hell

172
00:09:11,000 --> 00:09:13,000
or because their parents have told them

173
00:09:13,000 --> 00:09:17,000
or intellectually they believe in God or whatever,

174
00:09:17,000 --> 00:09:22,000
as opposed to actually truly accepting something

175
00:09:22,000 --> 00:09:25,000
or understanding something.

176
00:09:25,000 --> 00:09:27,000
You'll feel this.

177
00:09:27,000 --> 00:09:30,000
You'll feel the difference between accepting meditation

178
00:09:30,000 --> 00:09:33,000
because someone told you or because intellectually believe it's right.

179
00:09:33,000 --> 00:09:37,000
And actually getting a sense of how good meditation is for you.

180
00:09:37,000 --> 00:09:39,000
It's much more important.

181
00:09:39,000 --> 00:09:41,000
So it's a very good question.

182
00:09:41,000 --> 00:09:42,000
Thank you.

183
00:09:42,000 --> 00:10:04,000
Well, thank you so much.

