1
00:00:00,000 --> 00:00:19,720
Good evening, everyone, broadcasting live in Newberry sometime in January 15th, 2016.

2
00:00:19,720 --> 00:00:38,480
So it's been a event full week here, it's been Tuesday and Thursday, teaching more or less

3
00:00:38,480 --> 00:00:50,240
one-on-one, five-minute meditation lessons, sometimes two people, sometimes three, sometimes four.

4
00:00:50,240 --> 00:01:07,920
And it was just amazing to me, the diversity, and I go such a beneficial, such a fruitful activity.

5
00:01:07,920 --> 00:01:24,840
I taught two Sikh men, one point, two Muslim women, I taught, I taught a woman who had cerebral

6
00:01:24,840 --> 00:01:34,560
policy, children of wheelchair, I taught women who sat down and I taught one woman who sat

7
00:01:34,560 --> 00:01:42,800
down and just started crying.

8
00:01:42,800 --> 00:01:55,680
But mostly, mostly, you know, 20-some years old kids, a couple of older people, I taught

9
00:01:55,680 --> 00:02:02,240
one woman who sat down and told me she had psychic powers.

10
00:02:02,240 --> 00:02:14,880
And she got visions, if there were spirits came to her and showed her visions, and she

11
00:02:14,880 --> 00:02:17,840
was impressed by the practice, surprised.

12
00:02:17,840 --> 00:02:26,000
Most people were surprised by it, I think, with people expected from meditation, but pleasantly

13
00:02:26,000 --> 00:02:39,280
surprised, and some people really got it.

14
00:02:39,280 --> 00:02:45,200
And we're still working on exactly what I say, but it's more or less just telling people

15
00:02:45,200 --> 00:02:55,360
that the meditation is based on being objective, the key principle is that it's not

16
00:02:55,360 --> 00:03:01,280
our experiences that cause our suffering, it's our reactions to them, so if we can just

17
00:03:01,280 --> 00:03:05,760
experience things as they are, we won't suffer, for example, and then we start talking

18
00:03:05,760 --> 00:03:14,640
about the things, if someone's shouting at you, something sounded, caused you suffering,

19
00:03:14,640 --> 00:03:20,600
it's the reaction you have to the words, and you get angry and upset, and that cripples

20
00:03:20,600 --> 00:03:38,120
you in place, it locks you into this negative attitude that then leads to a human conflict.

21
00:03:38,120 --> 00:03:43,080
If you have thoughts, memories about the past, worries about the future, things you have

22
00:03:43,080 --> 00:03:46,760
to do in the future, things that are coming in the future.

23
00:03:46,760 --> 00:03:53,240
Things with the thoughts themselves aren't a problem, it's only a new one, let them get

24
00:03:53,240 --> 00:04:02,440
to you, but they become a problem.

25
00:04:02,440 --> 00:04:07,000
Even physical pain, physical pain is just an experience, you could just experience it

26
00:04:07,000 --> 00:04:13,200
as pain, but it too won't be a problem, it's just pain.

27
00:04:13,200 --> 00:04:18,640
So this is the theory that would be behind the practice, I tell them the theory, so it's

28
00:04:18,640 --> 00:04:22,360
up right into three parts, there's the theory, then there's the technique, and then

29
00:04:22,360 --> 00:04:29,120
I start explaining them about what you do, there's a hearing, hearing, thinking, thinking,

30
00:04:29,120 --> 00:04:36,280
pain, and then the third part is explaining why we have to practice it and practice meditation,

31
00:04:36,280 --> 00:04:43,960
it's a means of training ourselves in this practice of reminding ourselves.

32
00:04:43,960 --> 00:04:50,160
So we want to be objective, the means by which we, the technique by which we become objective

33
00:04:50,160 --> 00:04:54,160
is to remind ourselves, we want to see things as they are, so we remind ourselves of

34
00:04:54,160 --> 00:05:00,320
that, that's the word and then explain them that, and then I say so meditation is an

35
00:05:00,320 --> 00:05:09,360
exercise or a training practice to train you to, in this technique of reminding yourself

36
00:05:09,360 --> 00:05:13,160
so that you can understand things as they are.

37
00:05:13,160 --> 00:05:21,280
And so I say we start by picking an object that we're not likely to react to anyway,

38
00:05:21,280 --> 00:05:22,280
why?

39
00:05:22,280 --> 00:05:27,760
Because that's the easiest, it makes it easy for a beginner, if it was something that

40
00:05:27,760 --> 00:05:34,520
we'd normally react to, then very difficult for us to begin to cultivate objectivity,

41
00:05:34,520 --> 00:05:39,560
so that's why we use the breath, explain to them about stomach, and then I have them

42
00:05:39,560 --> 00:05:42,440
close their eyes and put their hand in their stomach.

43
00:05:42,440 --> 00:05:45,600
Most people thought that was a little bit goofy, I think everyone started smiling

44
00:05:45,600 --> 00:05:48,840
when they told them to put their hand in their stomach, because we're in a hallway and

45
00:05:48,840 --> 00:05:51,880
people are walking back and forth.

46
00:05:51,880 --> 00:05:59,760
It was incredible publicity, just about reminding people about meditation, making people

47
00:05:59,760 --> 00:06:02,200
think about meditation.

48
00:06:02,200 --> 00:06:08,160
Even today someone came up to me and asked me if I was doing it again, many, many, many

49
00:06:08,160 --> 00:06:10,560
people now know who I am on campus.

50
00:06:10,560 --> 00:06:17,800
They've been spending months wondering who I was and many of them know who I am.

51
00:06:17,800 --> 00:06:22,920
It's quite interesting, a lot of publicity people talking about it.

52
00:06:22,920 --> 00:06:32,120
And so then today we had our regularly scheduled Friday afternoon meditation, and four

53
00:06:32,120 --> 00:06:37,080
of them showed up, four people showed up who I'd given a five-minute meditation lesson

54
00:06:37,080 --> 00:06:40,400
to just sat down and went to do this.

55
00:06:40,400 --> 00:06:52,840
So we had a group of six of us, which is a record for a Friday, on Monday we'll do another

56
00:06:52,840 --> 00:06:56,480
meditation, just a half an hour of meditation.

57
00:06:56,480 --> 00:07:02,720
And then Wednesday I'm doing my first one into the teaching, so we'll see if anyone

58
00:07:02,720 --> 00:07:04,680
comes to that.

59
00:07:04,680 --> 00:07:12,680
And I'm still going to work on my TED talk, maybe this weekend off, put more effort into

60
00:07:12,680 --> 00:07:18,080
that, then school.

61
00:07:18,080 --> 00:07:19,080
School is interesting.

62
00:07:19,080 --> 00:07:26,760
I mean, first of all, school is great that this is the reason for staying in school

63
00:07:26,760 --> 00:07:32,920
for going back to school is to be at the university, get involved with the community.

64
00:07:32,920 --> 00:07:41,400
So I wouldn't have been able to do such great work, as I did this past week without

65
00:07:41,400 --> 00:07:47,760
being at the university, not being a student, I think.

66
00:07:47,760 --> 00:07:53,000
So that's reassuring, and the studies are interesting.

67
00:07:53,000 --> 00:08:01,440
Peace studies is really awesome, but it's so much of studying the ideas that I'm interested

68
00:08:01,440 --> 00:08:03,440
in anyway.

69
00:08:03,440 --> 00:08:04,440
What is Buddhism?

70
00:08:04,440 --> 00:08:05,440
What is Buddhism about?

71
00:08:05,440 --> 00:08:07,440
It's more peace studies than religious studies.

72
00:08:07,440 --> 00:08:16,080
It's more of a piece of literature, of course, but in religious studies sometimes in eastern

73
00:08:16,080 --> 00:08:17,080
Buddhism.

74
00:08:17,080 --> 00:08:22,200
And let me tell you, the Mahayana is messed up.

75
00:08:22,200 --> 00:08:28,720
Don't tell anyone, don't go on the internet, isn't there anything you want to broadcast

76
00:08:28,720 --> 00:08:29,720
on the internet?

77
00:08:29,720 --> 00:08:30,720
Right.

78
00:08:30,720 --> 00:08:38,720
But tell you, I'm sitting in this Buddhism class, this would be only one of the only

79
00:08:38,720 --> 00:08:44,800
practicing Buddhists, and I just think, oh no, please don't take this as representative

80
00:08:44,800 --> 00:08:48,480
of Buddhism.

81
00:08:48,480 --> 00:08:53,240
You know what they say, and this is the lotus sutra, which is really one of the base

82
00:08:53,240 --> 00:09:00,240
techniques, what they say is that the Buddha is basically lying, about there being three

83
00:09:00,240 --> 00:09:01,240
paths.

84
00:09:01,240 --> 00:09:05,440
The essence, the key that starts off to sutra, I haven't of course heard it all, but

85
00:09:05,440 --> 00:09:12,360
we just started, is that the three paths, you can choose to become a Buddha, or you can

86
00:09:12,360 --> 00:09:16,320
become a Pachaykambuddha, a private Buddha, or you can just become an Arahan following

87
00:09:16,320 --> 00:09:17,320
after the Buddha.

88
00:09:17,320 --> 00:09:20,120
That was a lie.

89
00:09:20,120 --> 00:09:24,120
This is out that we all have to become Buddhists, there's only one path.

90
00:09:24,120 --> 00:09:25,120
I think it's the Buddha.

91
00:09:25,120 --> 00:09:27,120
Yeah, it is the Buddha path.

92
00:09:27,120 --> 00:09:32,760
And so this is what the Mahayana is based on, that the Buddha was in the sutra has just

93
00:09:32,760 --> 00:09:33,760
so much different.

94
00:09:33,760 --> 00:09:41,280
They have lotus sutras, if you've read the polysutras, sutras, there's no comparison.

95
00:09:41,280 --> 00:09:48,760
Just the flavor of it is missing, to put these words in the Buddhist mouth, not impressed.

96
00:09:48,760 --> 00:10:01,720
But it's worth studying because Mahayana Buddhism is something that we have to deal with.

97
00:10:01,720 --> 00:10:12,320
And there's parts of the argument that just fall apart, but most especially the idea

98
00:10:12,320 --> 00:10:22,280
that the Buddha, the idea that these earlier teachings were just because nobody would understand

99
00:10:22,280 --> 00:10:26,000
if you actually told the truth, or they would be turned off.

100
00:10:26,000 --> 00:10:31,880
So instead of that, he had to trick them, give us tricking people, all of the teachings

101
00:10:31,880 --> 00:10:37,520
that we follow as to Arahan Buddhism, mostly just to trick people.

102
00:10:37,520 --> 00:10:38,520
Not exactly.

103
00:10:38,520 --> 00:10:39,520
But to encourage people in good things.

104
00:10:39,520 --> 00:10:47,400
It's still all of a good thing, so it's not evil, but it's kind of evil, to put these

105
00:10:47,400 --> 00:10:54,040
words in the Buddhist mouth, so now we don't make a religion of basically nullifying

106
00:10:54,040 --> 00:11:02,240
all the things that Buddha ever taught, the core of it ever.

107
00:11:02,240 --> 00:11:09,440
So that's, oh, and today a couple of people came to visit in the morning.

108
00:11:09,440 --> 00:11:20,200
I'm in with her two months old kid, daughter of a hundred minutes, she lives just down

109
00:11:20,200 --> 00:11:21,200
the road.

110
00:11:21,200 --> 00:11:27,680
It's just nice to meet our neighbors.

111
00:11:27,680 --> 00:11:43,600
Today we have a quote about helping oneself by not harming others, which is interesting.

112
00:11:43,600 --> 00:11:49,120
I think there's a thing that's only a partial quote, it can be interesting to read the

113
00:11:49,120 --> 00:11:56,760
whole sort of, so I don't have it here on the phone, but that's what leads to great good

114
00:11:56,760 --> 00:12:01,360
influence, so it talks about not harming others.

115
00:12:01,360 --> 00:12:11,120
So that is a fair point, of course, because that's the whole thing about, I mean, the

116
00:12:11,120 --> 00:12:17,200
supda has a bit of a problem for people because they might think, well, harming others

117
00:12:17,200 --> 00:12:20,600
doesn't necessarily bad for you, right?

118
00:12:20,600 --> 00:12:25,880
So you say, I shouldn't harm others because I wouldn't like it, and so how can I do

119
00:12:25,880 --> 00:12:26,880
it to them?

120
00:12:26,880 --> 00:12:32,880
Well, they think, well, harming them doesn't harm me, right?

121
00:12:32,880 --> 00:12:37,320
I steal from someone I don't get stolen from, right?

122
00:12:37,320 --> 00:12:46,600
I actually gain, I've gained from that, so that's good for me, right?

123
00:12:46,600 --> 00:12:55,800
But underlining this is something much more important than that, that harming others,

124
00:12:55,800 --> 00:13:01,680
that sets you in that realm of harming, of harm, you know, the people who you associate

125
00:13:01,680 --> 00:13:08,720
with start to change, your mind starts to become more corrupt.

126
00:13:08,720 --> 00:13:12,160
That's why it would go around comes around this because your whole universe changes.

127
00:13:12,160 --> 00:13:18,640
If you're a good person, you surround yourself and you'll be interested to be fond of good

128
00:13:18,640 --> 00:13:26,760
people, you cultivate unwholesome, you'll associate yourself and surround yourself with

129
00:13:26,760 --> 00:13:33,200
unwholesome people, and so much more, I mean, everything will change around you, your mind

130
00:13:33,200 --> 00:13:40,880
will change, your health will change, and so your work will change, your productivity

131
00:13:40,880 --> 00:13:57,440
will change, your relationships will change, everything will change, harm leads to harm.

132
00:13:57,440 --> 00:14:16,040
Anyway, not too much to say besides that, I think that's enough to talk with an aid.

133
00:14:16,040 --> 00:14:19,520
Thank you all for tuning in.

134
00:14:19,520 --> 00:14:26,320
Sorry about yesterday as I was a little bit, I just didn't feel up to doing a video

135
00:14:26,320 --> 00:14:34,520
and then broadcasting with tomorrow, I'll try to do tomorrow Q&A, okay, so Saturday's

136
00:14:34,520 --> 00:14:43,640
Q&A, we'll try that under the assumption that Saturday night is when everyone will be sitting

137
00:14:43,640 --> 00:14:52,480
at home, because no one does anything on Saturday night, right, and Saturday night we can

138
00:14:52,480 --> 00:15:04,240
meet together and do Q&A, and then next Thursday we'll do this one-on-one, okay, thanks

139
00:15:04,240 --> 00:15:30,240
guys, thanks for tuning in, look at that.

