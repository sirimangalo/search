I'm going to make sure so you will hear and get this as dark ice.
It's getting from the knowledge put to the knowledge put, so it's okay.
Good evening everyone, broadcasting live at a January 2nd 2016
with me today is Robin from Connecticut and Anthony from Toronto.
Hello. Anthony is on our board of directors, aren't you?
Yeah, who's our board of directors?
Me, Robin, I'm trying to think of anybody else.
You're open, it's on our, so the three of us are standing?
Yes, well, as the three of us, all the directors are here.
I didn't know that.
Jeff, Jeff helps a lot, but so far he has escaped being.
He was talking about getting back on the board, but I don't know where that stands right now.
Jeff is in Winnipeg, so it makes it a little difficult.
But okay, so we have our board of directors here, Anthony is our treasurer.
Robin is secretary. Secretary, is that what we need?
And I'm the monk.
The president monk.
I'm the president.
So I thought Anthony came today, so I thought I would invite him up here.
We were just talking about punctuality.
All right, Wes, how did this come up?
My mother was asked me this question about whether
what I thought about being punctual and whether it's
whether I thought it was important for people to be untied.
And I said, no, that's an interesting question.
I used to think, I mean, I used to really appreciate people who had this sense of
punctuality.
Like the head monk at Dehisuthep, when I was there,
remember going, we were going for this some kind of lunch.
And so I was invited and a bunch of other monks were invited.
And he said, we'll meet at the, it's at 11.
So we've got to be, be there.
We've got to meet at the elevator at 11 or the escalator.
There's a big escalator down the mountain.
And I got there 10 minutes before 11.
And the head monk got there about five minutes before 11.
And then at 11 o'clock, the other monks came in and he scolded them.
And he said, look, when we say we have to meet at 11,
you have to be here 10 minutes before 11.
At the time, I thought, oh, that's really, I appreciate that.
But I think I've changed the way I look at it.
I mean, Adjantong was really hard to have around time,
but that's, that goes without saying because he's so busy.
And, but looking at that gives you an example of another way to live.
You can't really control and not trying to control.
I mean, not trying to control just doing your best to fit as much in as you can.
It doesn't, it wouldn't work in modern society, I suppose.
And I was so my argument with my mother was basically,
you know, it's just the way our society is set up, the way we live our lives by the clock.
And I think there's an argument for us not really needing to
that our society could have been set up even in the complex, to support the complex systems that it does
without this need for nine to five jobs and meetings and so on.
I mean, think of what email has done, right?
Email, instant messaging, like look at our slack,
we are communications.
We don't need to have meetings for that.
We don't have to call people even.
We leave a message and then whenever the person gets the message, then they answer it.
It may be that we're even moving away from this punctuality idea because
we don't really, I don't know, I guess I don't live in the world.
Because it's maybe a few years people will start seeing it as a necessary.
Yeah, never enough.
I just, I remember being in Sri Lanka and it's just incredible that how
people who live in, in the forest, in huts
and we're talking about village villagers who really lived in very very simple conditions.
Some of them had dirt floors.
And they'd say like when you ask them when you come in, they say tomorrow
and they wouldn't come tomorrow.
And this happened again and again and again and again and people would say, okay, I'll come there
tomorrow and I started joking that I don't know what this word means.
The word in Singapore is hate that means tomorrow.
What does this word mean?
And the workers said it means or someone said it means tomorrow.
I said no, it doesn't because these workers have been saying this to me every day.
Every time they come, they never come tomorrow.
But it would be the same thing like I'll see even an hour and then three hours later they arrive
really incredible.
So it really changes your sense.
You've become kind of surreal.
This sense of, no, there'll be some nebulous time in the future.
You could never really play on or wait for anyone.
You just go about your life and hope that that serendipity and the synchronicity of it sort of works out.
My mother said, no, what if you have a doctor's appointment?
I said, well, doctors are notoriously late.
That was like, well, what if you have something to do afterwards?
Like if I have somewhere to be and I say to meet me half an hour before that and if you don't
meet me on time, then I'm going to be late for me other meeting and she said, sometimes it's
just, you need to be on time and I said, and yet the world continues to turn and she kind of
laughed.
But maybe my point being really, and we may make far big, we tend to make far bigger deals
of these things than they actually are.
Our petty lives, I mean, I don't live in the world, so I can say and I understand that the
systems we've set up in many ways require punctuality, but it is a philosophical, philosophically
interesting, I don't know, it's an interesting question.
I think we could all do to be more laid back about it time.
And certainly what you can pinpoint is someone who gets angry or irritated or upset when someone
is late, they're wrong, that is wrong.
Now you can scold someone, you can do it mindfully and you could argue that that's important,
they punctuality is important, you have to let it be known that this won't be tolerated,
but when people actually stand there and look at their watch and fumes on, that's where we have
to, that's where the work has to be done, that has to be fixed.
If you've gotten to send some of these people over to Sri Lanka.
Nice to get really irritated when doctors were late, as they always are, you're sitting there for
so long, but now they meditate and I don't mind, they can be as late as they want to be.
It's okay.
Questions?
Yes.
Sorry, there's a lot of talking, just to find the first question.
I have a private talk tomorrow, what website do I go to to video chat?
I think we've already talked.
Just for anyone else, you just click on the Meet button and click on the actual box that has
your name, right? Is that how a meeting is launched?
Sorry, I didn't quite catch that. For people who are signing up for the meetings for the first time,
how do they meet with you? They just click on the Meet button and then click on the box that has
their name on it. Is that how it works? No, there'll be a different button will pop up that says
start hanging out or something like that, call maybe, and then you have to invite me.
So far, it's working pretty well. Oh, Christopher outlined it.
Hey, someone's swearing here. Someone's having trouble with Mara.
Some people had their time zone set wrong. That's funny. It would show the time wrong.
And then you'll never get a button. Mara.
This doesn't have a question icon, but it seems like a question. I just read a quotation from the
Buddha. It said, doubt everything, find your own life. So would it be wrong to doubt the Buddhist
religion? Yeah, that's not a Buddha quote. I call BS on that one.
Yeah, check it out. Let's check it out. You want to call, you want to look it up? Take a
Buddha quote. Take Buddha quotes. If anyone's interested, go to fake
Buddha quotes.com. But it's here. Yeah, outside. Don't everything find your own life,
a yes, that is a fake Buddha quote. It's in several books, mostly from the 21st century,
21st century. Like that is a very new books. Also founded in a 1991 book,
Armageddon Musical, attributed to the Buddha. Unfortunately, I don't know where rank and picked up
the idea that this was for the Buddha. Find your own life.
I missed the site. I should come back and check it out again. What's he up to?
Is he still doing this? Yeah, December 11th was the slightest one. He's still at it. And he gets
such black from people. People get so upset and say, why are you being so critical?
Well, it's funny. If you see enough memes, you'll see the same quote. It's a Buddha quote. It's
you know, a Gandhi quote. It's an Abraham Lincoln quote. It's kind of funny. Yeah, Gandhi gets
risk quoted a lot as well. But not nearly as much as the Buddha in Einstein. But no one
as much as the Buddha, I don't think. Sure. I saw another one recently that I'm not sure about,
but it was posted by a monk or a beakuni. So I left it. I was going to say, give me a,
but it's pretty close to what I remember. What was it? I can't.
Something very close and it may just be a different version.
So would it be wrong to doubt the Buddha's religion? Well, doubt is always problematic.
Doubt is not a good thing. Sometimes you need, no, not need, but sometimes
I suppose there's a difference between doubting and questioning.
So questioning can include acknowledging that you don't know, right?
Acknowledging that something is a belief, rather than a knowledge with stability, the distinguish between
that. So that is useful.
Skepticism, right? Skepticism isn't about doubting everything, about requiring proof. Skepticism
is starting ahead. This might be true and it might be false, starting at a neutral position,
not taking sides, and then assessing based on evidence.
So when people go around saying prove it, prove it, prove it. That's only useful when
the evidence, when there's no evidence to support a claim.
So you have to start at, might be true, it might be false, and then you look at the evidence
and see what the evidence supports.
Anyway, the point is, I wouldn't use the word doubt. Doubt everything is really ridiculous.
I mean, the Buddha would never use that word doubt. Anything that translates as doubt is bad.
But to question, and maybe the word question isn't even quite right, but to suspend
judgment and to be able to see the difference between a belief and a knowledge is very important.
And belief is considered good in the sense that it's powerful when you believe something.
So I guess in that sense, doubt isn't always bad, but it's always bad for the person.
So faith is good in a mundane sense, because it makes you strong. When you believe something,
it allows you to do it. But if you believe in doing evil deeds, that's actually quite bad.
Even though it's good, the faith is good in the sense that it gives you the power to do all those
evil deeds. But it's quite bad, actually. And so doubt would be the same if you're told
that you must do lots of evil deeds. And then you doubt that. Well, that's a good thing.
Even though the doubt cripples you, it's good that it cripples you. So
the problem there is that if you're just dealing with faith and doubt,
you sort of have a sense of what should I have faith and what should I doubt? And that's where
knowledge comes in. Wisdom comes in or experience comes in. If you don't have knowledge in wisdom,
then your faith and your doubt are just going to be random or based on your preference or totally
arbitrary. But once you cultivate wisdom, then you doubt or you disbelieve things that have no
basis like the idea of God or the soul or whatever, a teapot orbiting the sun.
And you have faith in those things that are rooted in experience.
Like evil leads to suffering, good leads to happiness and so on.
What is the mind state of Mara the devil? How is he existing in the high realms while being
such an immoral character? And is there any hope for his enlightenment?
Maybe skip down a lot because I can't see that question, but I didn't see it before.
It's so cute before that. Five hours ago, yes.
Oh, did he skip the other? Oh, they're okay. So yeah, I saw it right. Mara isn't
necessarily immoral, although I suppose that is one way of looking at it. The angels are a weird
bunch and Buddhist cosmology. They apparently go to war with the Asuras, you know, they're really
killing each other or something. I don't know, at least fighting and they end up kicking the Asuras
out. It's all, this is all vengeance. I don't know whether it's true or not, whether it really
happens. I never can't remember ever being in heaven. So I can't say what it's like.
Well, I suppose it's just as weird, probably a lot weirder than the human world.
So yeah, apparently there is a group of Mara's. See, the thing about Amara is if you notice in this,
in the discourses, Mara was often encouraging the Bodhisatta and other monks and other meditators
to do good deeds, but to not practice meditation, so to give charity and so on.
Because this is the kind of thing that would lead them to heaven. And Mara likes that. Mara is
a part of the Bodharnimita, a very nimita, sawati. So what do you think? I think he's part of
those, which means people who delight or who are happy about the creation's feathers. So they
want other people to do things, to cultivate, they like all sorts of things, to build skyscrapers,
and to go to war even perhaps. They just like becoming, they like watching other people,
they like they encourage other people. So they encourage people to follow their dreams and
sawati. Mara is the one that encourages you to follow your dreams and think big and sawati.
We ambitious, they like ambition. So what are called him evil? What are called him or the
you know, this Mara that was chasing him, evil. But in a Monday sense, he generally doesn't seem
to be evil, he seems to just not want people to leave samsara. It's kind of sad for him,
some reason when people leave because then those people are not. I don't know anyway, they like
to encourage people to stay in samsara and to do, to create things.
Apparently we are for their amusement.
I had skipped some questions prior for me. My question is how do you lose Sakaya Diddy without
also having shed mana, for example, aren't both states interdependent in a way?
And a little bit more is, I mean, how do you become a sothapana without having conceit?
It just doesn't make sense to me.
Sothapana has no view of self because they've seen
they've seen the break. They've seen how everything and they've come perfectly clear about
how everything arises in seasons. But it doesn't really change that much else, like
they still get angry, they still get greedy, and all of that is based on
delusion. They still have a lot of delusion.
And you can see this just a part of that, conceit is a judgment, judging yourself is better,
worse than other people. It's just like a judge other things, things outside of yourself. They can
still get arrogant. Well, not so much. They're generally humble, they're generally not conceited.
What you might note that I think the orthodoxy is that there are nine types of conceit,
a person might esteem themselves as better than someone else,
equal to someone else or worse than someone else. And they might, in fact, be better than
someone else. They might, in fact, be equal to someone else. And they might, in fact, be worse than
someone else. So three times three makes nine types of conceit. For each of the three
beliefs or conceptions and the three realities, if you have the permutations make nine,
a sort of panic can only give rise to three of them, meaning if a sort of panic is
better than someone in some way, then they esteem themselves as better. If they're equal
to someone than they esteem themselves as being equal, if they're inferior than they esteem
themselves as being inferior, don't quote me on that. It's kind of in a while. And obviously,
details are not really me forte. But we see demagogues into detail about that. I should really
look kind of. Maybe that's my understanding of it. So the sort of panic does get rid of the wrong
sort of conceit, I think. But they can still feel good about being better than someone,
the arrogant potentially, though not that much. But it's kind of like the same way, how can they
get greedy and angry if they don't have a view of self, right? But it's mostly considered to be
just leftover. It's slowly working its way out. It's like the power that is given rise to these
things. The power is still there. It takes time for that to work itself.
Did that also answer about losing Sakaya Ditti without also having shadmana?
I thought it did. Oh, yeah, Sakaya Ditti is view of self. Manai is conceit.
So view is different from conceit. Conceit is just a steaming something. View is actually
there is a self. I believe there is a self. This is self.
Wrong conception can still arrive.
I have one question regarding sitting meditation. If you are thinking rising, falling as you
focus on your abdomen and respiration, you are indeed thinking, but should not meditation be a
thoughtless state of mind? No, there's no reason to think that
thoughts are a part of meditation. It's the third sepipatama or it's a part of it anyway.
It's also in the fourth sepipatama. So thoughts are very much a part of reality.
It's just absolutely a very big misconception about meditation that you should, but there are
many meditations that tell you to do just that, but it's a misconception that all meditation,
especially Buddhist meditation, should be less. Of course, many Buddhist meditations are,
but mindfulness meditation, or meditation to understand reality, which is the core and really
the end of the goal of the practice that you have to take requires thought,
both the examination of thought and the response. Some mindfulness is about creating a clear
thought because thought isn't a problem, but judgmental thought is a problem when you like
something or dislike something that's a problem. So we're just trying to replace that with a clear
thought.
If you find your mind restless at night, what are some good ways to help in relaxation
and clearing the mind and the body for the best rest?
Well, maybe read my booklet on how to meditate.
I guess one thing I would say is don't worry so much about relaxing. If your mind is awake,
you're restless and just meditate. Say yourself restless, restless and be okay with it,
because the more upset you get about it, the more restless and anxious you become.
Last month, you talked about different styles of chanting and how this Sri Lankan style resonated
with you. I was watching a seminar on past life memory that was given at a Buddhist conference,
and there was a fellow named Dhamma Wuran, who at the age of two or three could spontaneously
chant in palli at two o'clock in the morning. His foster father recorded some of these chants
and brought a monk to verify that it was palli. Not only did they verify it, they also identified
the sutas, and the monk said his style of chanting is no longer around these days.
Goes on and on a little more, but the question is, have you heard these chants? And if so,
does his style bring out the meaning of the sutta more than any other present chant?
I've never heard this. I've heard of this boy, but I've never heard this.
Never heard his chanting. The chanting is just, I mean, the best is something fairly simple.
I don't know. I mean, there's nothing magical about it. It's just words.
I suppose it's quite remarkable that someone at two or three could recite, you know,
in palli, wouldn't it be?
That's remarkable.
Yeah, I saw a video on YouTube. I'm not sure if it was that particular
two or three-year-old, or it was just someone who maybe had learned it, but it was a very small
child and he was chanting in palli, it was kind of cool. It wasn't very long, though.
I believe that's all the questions.
All right.
Anthony, questions from me? Anything to say?
I'll talk about any announcements. How's our organization do it?
Our organization has been on vacation for two weeks. We haven't had meetings for a couple
weeks, but we'll be meeting again tomorrow to see what the needs of yourself, the monastery and
meditation center are. Did the banner arrive for the five-minute meditation?
Yes, but the things that they're supposed to be clips didn't come with it.
So they're in a different container or something and they're going to be here by the seventh.
Well, there were two options. There was one option for those plastic tags. I forgot what they're
called. The plastic loops that you put up or a rope. So I ordered the rope because I thought
the plastic, I forgot what they're called, but you put them up and that's how you put up a banner
permanently. And I figure this is a banner you probably have to take down in between.
The ordered ropes, whatever ropes. A rope. An actual rope.
But how do you connect the rope to it?
It's just a piece of vinyl.
Yes, it has, I think it has holes in it and you put the rope really.
Anyway, worst case will be stuck there.
I've got a frame. We've got a good frame to put it on.
So hopefully that'll work.
When you do put it up, will it be permanently placed there? Do you have to take it down every
day or every week?
Yeah, so the plastic loop things won't work. I forgot what they're called, but they
they hold things up, but you have to cut them down to take them off.
I can't think what it's called.
Oh, the ties.
Yes.
Table ties.
That's not the real name of it.
I don't think so.
Zip ties, maybe that's what they're called.
But I guess they're for cables.
I worked with this one place to be called from cable ties.
Yeah, I know them by another name, but I think we're all talking about the same thing.
Yeah, it's those white plastic things.
Yeah, you have to cut through them when you're done using them.
So I got like what what the text of vinyl or whatever this thing is that we have.
There's there was something but a decent option.
The sticks to them with that has a little loop coming out that you hook into.
And there was a grommet option, I think grommets from like more expensive, but they're better for outdoors.
I didn't get anything.
So like I think there's another box coming to help there's.
Okay, yeah, hopefully that will explain because as I said, there were there were two add-on items either the rope or the the ties.
I don't really need rope.
We got rope.
Okay, I need something to connect it to the thing.
Okay, so meeting tomorrow at 12 and one.
I'm meeting for volunteers at one and for study group that too.
The admin is at 12.
You're not invited.
They're rocking this with internet.
Although we although we would love to have more volunteers, you know, active in that.
What is the admitting meeting?
Well, at least in the volunteer, the general volunteer meeting and then for people that are
really interested, it would be nice to have more people on the admin team.
That's the team that does all the accounting and kind of the the business end of.
Maybe people who are more experienced with that would help.
Yes, basically we do what we do.
So that month they doesn't have to be overly involved in financial matters.
So we try to take on all the the financial bookkeeping.
By laws, different business aspects of.
I mean, you guys do a lot of work that I would have that I couldn't do, but I can't do it because I'm too busy.
And also some of it, you know, is really against the windy, right?
Yeah, that's not how you used to.
So we're going on just great.
Yes.
Oh, thank you, Ryan.
So zip tie, zip straps, zip ties, I think.
Something like that.
And for Joshua asking how to join the study group, there's a Facebook page study group with
you to download be cool. And that gives the information. Basically, it's on mumble, an audio chat program.
And I'll link you all the information.
There's a web page on the website as well on the serimungalow.org website with all the information.
But we meet on mumble audio chat and we take turns reading the visudimaga.
It's a nice group. We've been together nearly a year now. I believe it started in February of
last year, making our way through the visudimaga. I think we're on chapter 10 now.
And there's a question, what is skeptical doubt in the context of the five hindrances?
Okay. And doubt is when you're not sure of something.
And that that's a hindrance because it prevents you from moving forward with whatever
this you're doing. Now, the commentary, I think, is a little bit less forgiving about it.
And you know, that's general. And it's more like doubt about the Buddha and doubt about the
dharma, doubt about the sun. And I would say it can be any kind of doubt, which is it's just that
doubt about the Buddha, the dharma, the sun goes actually technically a problem.
So we're saying you can doubt about bad things. And that's kind of a good thing.
Or you can doubt about whether Paris is the capital of France. It's not really an important
doubt. But if you doubt about the meditation practice, well, that's a hindrance, a real hindrance,
because it's going to prevent you from progressing in the practice.
Okay. That's all. Have a good night, everyone. Thanks for having me for joining me and
thank you, Anthony, for improving the camera. Thank you, Bantin Anthony. Good night, everyone.
Good night.
you
you
you
you
you
you
you
you
you
you
