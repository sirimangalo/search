1
00:00:00,000 --> 00:00:10,000
Would it be helpful to experience the Janas so that you can help a meditator to move beyond blocks?

2
00:00:10,000 --> 00:00:20,000
It would probably be helpful in order to teach, but not necessary.

3
00:00:20,000 --> 00:00:35,000
You can actually, when there is no teacher around who can teach you the Janas or better even Vipassana meditation, you can take everything as the teacher.

4
00:00:35,000 --> 00:00:56,000
And the Buddha, as far as I remember, mentioned once that teachers can be more experienced than the pupil can be equally experienced or can even be less experienced and still the student can learn from them.

5
00:00:56,000 --> 00:01:12,000
And we could, I learned so much from Anz for example, and they might not have attained any Janas, so it's not necessary.

6
00:01:12,000 --> 00:01:27,000
There's a lot of questions like this, and I think the underlying source of the questions is that people really want to learn the Janas, because they think there's something great to them and how mystical and magical it might be.

7
00:01:27,000 --> 00:01:38,000
And yeah, they are great and they are mystical and magical, but what I think you find when you practice Vipassana.

8
00:01:38,000 --> 00:01:47,000
The more you practice meditation and the more clearly you see reality, really the less enticing such things, any such things become.

9
00:01:47,000 --> 00:01:59,000
And the idea of whether you need to experience them to explain them, I think it's pretty invalid.

10
00:01:59,000 --> 00:02:07,000
What you need to explain things is an understanding of how reality works, and the Janas are just another part of reality.

11
00:02:07,000 --> 00:02:13,000
Janas are not something, they're not really something mysterious or incredible.

12
00:02:13,000 --> 00:02:25,000
It's just a state where the mind is focused, where the mind is fixed, where the mind is secluded from the hindrances.

13
00:02:25,000 --> 00:02:37,000
And that's just a mind state, it can last for some time, you can develop it, you can go on to develop any number of crazy things, you can have magical powers, remember your past lives and so on.

14
00:02:37,000 --> 00:02:49,000
But the best way to understand all of these things and to be able to talk and to teach about them is to understand reality, to practice the simple meditation, to understand the building blocks.

15
00:02:49,000 --> 00:03:05,000
Because that's really the key, once you understand what makes up experience and how experience works and how cause and effects works, then none of those things are mysterious or hard to understand or really at all enticing.

16
00:03:05,000 --> 00:03:27,000
Directly answer your question, I don't think it's necessary, I could help anyone who had practiced the Janas without having experience, I shouldn't talk exactly about myself, but I've had quite a bit of experience with meditators and with people and learning how the mind works and so on.

17
00:03:27,000 --> 00:03:38,000
I don't feel like I would ever need to train myself in the Janas in order to help someone who experienced them and was having a block with them or someone.

18
00:03:38,000 --> 00:04:02,000
What you do often get is a strong attachment to peace and to tranquil states of mind and that's quite difficult to train someone out of a person who has practiced some of the meditation intensively can be, can be quite difficult to wean off of it, especially if it was in a different religious tradition.

19
00:04:02,000 --> 00:04:30,000
So if they had practiced meditation and Hindu tradition, for example, which has different goals and ideas, or even Buddhist meditation where people practice tranquility and they like the state of calm that they attain, can be quite difficult to wean them off of it, but that is not, you know, weaning them off of it has nothing to do with knowledge of the Janas in order to do with knowledge of people's attachments and views, views about what is true happiness,

20
00:04:30,000 --> 00:04:49,000
but what is self about trying to control and letting go and so on about impermanence that these things, these states are not permanent, as the Buddha said, that the Janas and any kind of pleasant or even neutral feeling is not permanent.

21
00:04:49,000 --> 00:05:02,000
And none of those things require you to have attained the Janas yourself.

