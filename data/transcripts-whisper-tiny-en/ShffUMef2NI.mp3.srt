1
00:00:00,000 --> 00:00:07,000
So tell me a bit a little bit about what made you want to come and meditate with it.

2
00:00:07,000 --> 00:00:17,000
To meditate, first of all, I had a lot of fears that I was afraid, a lot of things, like darkness,

3
00:00:17,000 --> 00:00:25,000
but often I was afraid to stay alone in darkness, stay alone at home,

4
00:00:25,000 --> 00:00:37,000
or to be around a lot of people who I don't know, or also have a lot of attachments,

5
00:00:37,000 --> 00:00:44,000
attachments to my family, to close friends, and I used to have actually.

6
00:00:44,000 --> 00:00:49,000
And how did that affect your practice? Did you find the fears coming up stronger in practice?

7
00:00:49,000 --> 00:00:53,000
Were they interfering with it?

8
00:00:53,000 --> 00:00:56,000
How was it practicing with the fear?

9
00:00:56,000 --> 00:01:03,000
Practicing with the fear in the beginning, it was very hard, like because everything, what is inside, is coming out.

10
00:01:03,000 --> 00:01:09,000
And you have to acknowledge it, you have to notice it, actually, these are.

11
00:01:09,000 --> 00:01:23,000
And once you see it, you actually, it's getting better.

12
00:01:23,000 --> 00:01:32,000
Yeah, it's getting better and better every day, and also fear is just going out, and it's not bothering me anymore.

13
00:01:32,000 --> 00:01:40,000
How was it at the end? You really had to face really strong fear near the end?

14
00:01:40,000 --> 00:01:44,000
Actually, one night was very hard.

15
00:01:44,000 --> 00:01:50,000
Well, when I was supposed to stay during the night in meditate, it was very hard,

16
00:01:50,000 --> 00:02:00,000
because everything was bad inside, came outside in a short period of time, in a couple hours,

17
00:02:00,000 --> 00:02:08,000
and it was really difficult to fight it, but it is possible, everything is possible if you really wanted.

18
00:02:08,000 --> 00:02:13,000
Is that fear still there?

19
00:02:13,000 --> 00:02:20,000
I would say it is slightly present sometimes, but when I acknowledge it, I'm telling myself,

20
00:02:20,000 --> 00:02:25,000
I'm trying to meditate on it right away, and it's just going away.

21
00:02:25,000 --> 00:02:29,000
So you found a tool that allows you to deal with it, is that correct?

22
00:02:29,000 --> 00:02:34,000
This is exactly how do you feel now?

23
00:02:34,000 --> 00:02:42,000
I feel much better, I feel very calm, very relaxed, very peaceful, I would say.

24
00:02:42,000 --> 00:02:47,000
I'm glad to be around the people, glad to see them, to talk to them,

25
00:02:47,000 --> 00:02:50,000
and it doesn't matter what about it.

26
00:02:50,000 --> 00:02:53,000
It's just, I feel happy.

27
00:02:53,000 --> 00:02:56,000
This course made me happy.

28
00:02:56,000 --> 00:03:00,000
So what are the main benefits to practicing meditation?

29
00:03:00,000 --> 00:03:04,000
What do you think meditation does for you?

30
00:03:04,000 --> 00:03:14,000
It helps me to see actually what is inside me, good and bad things,

31
00:03:14,000 --> 00:03:27,000
and also accept those good and bad things, and also learn during course,

32
00:03:27,000 --> 00:03:35,000
and learn tools which help me to deal with the same sense.

33
00:03:35,000 --> 00:03:43,000
What would you say is the main difference for you between now and before you start learning?

34
00:03:43,000 --> 00:03:46,000
Before I started practicing?

35
00:03:46,000 --> 00:03:53,000
Before I started practicing, it was the first day when I came in meditation centre.

36
00:03:53,000 --> 00:03:58,000
It was really hard, lots of thoughts, I was running around in my head.

37
00:03:58,000 --> 00:04:04,000
I had the headache, and I couldn't focus on anything.

38
00:04:04,000 --> 00:04:08,000
It caused me a pain, everything was causing me a pain.

39
00:04:08,000 --> 00:04:14,000
I remember first day when I came here, I just basically go downstairs in basement,

40
00:04:14,000 --> 00:04:18,000
and spend two days, I spent in basement.

41
00:04:18,000 --> 00:04:23,000
I didn't want to come out, but slowly, slowly, it's got better and better.

42
00:04:23,000 --> 00:04:27,000
After three days, I would say, or four days.

43
00:04:27,000 --> 00:04:33,000
I noticed that I feel more comfortable outside, and I started to practice outside,

44
00:04:33,000 --> 00:04:38,000
and look back here, and didn't want to go down again.

45
00:04:38,000 --> 00:04:40,000
And now?

46
00:04:40,000 --> 00:04:50,000
And now, now it's changed, this feeling of peacefulness and happiness is present.

47
00:04:50,000 --> 00:05:04,000
Also, I noticed that I don't need so much food, sleep, and what else?

48
00:05:04,000 --> 00:05:12,000
Food sleep, and coffee, tea, and something, but I saw that this is very important in my life,

49
00:05:12,000 --> 00:05:18,000
that I really don't need, as if we can sleep a couple hours per night, and feel pretty good.

50
00:05:18,000 --> 00:05:21,000
Like, feel perfectly, basically.

51
00:05:21,000 --> 00:05:24,000
Like, everything we have is just our addictions.

52
00:05:24,000 --> 00:05:28,000
It's our idea that we need it, but we don't really need it.

53
00:05:28,000 --> 00:05:31,000
What would you say to people who want to come to practice?

54
00:05:31,000 --> 00:05:34,000
I think you need to come to practice.

55
00:05:34,000 --> 00:05:38,000
I would say that this is the best and most important thing,

56
00:05:38,000 --> 00:05:40,000
that I have done.

57
00:05:40,000 --> 00:05:45,000
And I'm really glad that I did it, and I would like to continue it.

58
00:05:45,000 --> 00:05:54,000
And I really recommend other people to take it for us, because it's pretty hard to do it at home.

59
00:05:54,000 --> 00:05:59,000
I could not imagine that I would have done something like this at home.

60
00:05:59,000 --> 00:06:01,000
You really need some support.

61
00:06:01,000 --> 00:06:08,000
You really need some teacher who will help you to done such things like meditation,

62
00:06:08,000 --> 00:06:16,000
correct your movement in necessary situation when it's needed.

63
00:06:16,000 --> 00:06:21,000
Any advice for someone thinking to do it, for preparing for the course?

64
00:06:21,000 --> 00:06:25,000
Preparing to the course?

65
00:06:25,000 --> 00:06:30,000
Maybe some kind of expectation they might have to help you want to,

66
00:06:30,000 --> 00:06:34,000
something they should know before going into the course?

67
00:06:34,000 --> 00:06:44,000
I think it would be easier to have some little practice might be at home before.

68
00:06:44,000 --> 00:06:47,000
I don't know, it's everything is individual.

69
00:06:47,000 --> 00:06:55,000
It's all depend from person, from what each he or she has inside.

70
00:06:55,000 --> 00:07:00,000
But the harder it works, the more benefits you get.

71
00:07:00,000 --> 00:07:04,000
This is how it is, yeah.

72
00:07:04,000 --> 00:07:05,000
It's very hard.

73
00:07:05,000 --> 00:07:14,000
I don't think that I would organize myself at home to practice a whole day and sleep six hours.

74
00:07:14,000 --> 00:07:16,000
It wouldn't be possible.

75
00:07:16,000 --> 00:07:17,000
I don't think so.

76
00:07:17,000 --> 00:07:24,000
But meditation center is very easy, because you are not distracted by other things.

77
00:07:24,000 --> 00:07:32,000
Just coming here, you are receiving a food, you sleep six hours, and this is enough.

78
00:07:32,000 --> 00:07:40,000
And all the time you have to try to make the practice and you are not bothered by other people.

79
00:07:40,000 --> 00:07:59,000
Thank you.

