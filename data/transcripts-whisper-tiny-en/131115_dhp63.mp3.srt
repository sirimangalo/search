1
00:00:00,000 --> 00:00:04,480
Hi, and welcome back to our study of the Dhamapana.

2
00:00:04,480 --> 00:00:32,920
Tonight, we continue on with verse number 63, which reads as follows.

3
00:00:34,480 --> 00:00:42,880
Yo balo manyati balyang, the fool who knows that they are a fool.

4
00:00:42,880 --> 00:00:52,560
Panditova, Pitinas, so for that reason, or to that extent, they can be called wise.

5
00:00:52,560 --> 00:01:03,600
Pandi, Balocha, Panditamani, but the fool who thinks of themselves as wise,

6
00:01:03,600 --> 00:01:12,360
that way balo di bajhati, that person indeed is called a fool.

7
00:01:12,360 --> 00:01:22,080
So it's one of the more important Dhamapana verses, I don't know, more memorable I suppose.

8
00:01:22,080 --> 00:01:27,960
It's a powerful one, especially for those of us beginning the path and feeling discouraged

9
00:01:27,960 --> 00:01:35,680
sometimes. Anyway, we'll go through the story and then we'll talk about the verse.

10
00:01:35,680 --> 00:01:37,320
Story goes, it's a very short story, actually.

11
00:01:37,320 --> 00:01:41,240
We're going to find several of the Dhamapana stories are quite short, so there's not

12
00:01:41,240 --> 00:01:44,320
much to say about them.

13
00:01:44,320 --> 00:01:51,760
When the Buddha was dwelling in Sabhati, there were these two thieves who decided that

14
00:01:51,760 --> 00:01:59,920
they would go off to the monastery and try to rob the people who came to listen to the

15
00:01:59,920 --> 00:02:03,120
Buddha's teaching.

16
00:02:03,120 --> 00:02:07,400
So here in Sabhati, there was a big city, walled city.

17
00:02:07,400 --> 00:02:11,160
You can even still see the walls if you go now.

18
00:02:11,160 --> 00:02:15,840
To visit, that's all that's left is a big field with walls.

19
00:02:15,840 --> 00:02:20,440
Ruins, the ruins are just covered, mounds of, who knows what, the whole sequence, if you

20
00:02:20,440 --> 00:02:26,280
go up on these pagodas, you can see the walls all around the city.

21
00:02:26,280 --> 00:02:32,800
So there would have been rich people and high-class people who would go to see the Buddha

22
00:02:32,800 --> 00:02:38,600
and hear the Buddha talk, hear the Buddha teach, and because there would be so intent

23
00:02:38,600 --> 00:02:42,960
upon what the Buddha was saying, they would often just lose track of their personal belongings,

24
00:02:42,960 --> 00:02:49,880
lose track, lose their care for their possessions, it'd actually be easy picking.

25
00:02:49,880 --> 00:02:54,720
This happens in Thailand actually, or it has happened in Thailand.

26
00:02:54,720 --> 00:02:59,720
So they say when you go to, when you go to hear the monks talk, don't keep your belongings

27
00:02:59,720 --> 00:03:00,720
close.

28
00:03:00,720 --> 00:03:06,760
If you're going to meditate with your person, your hands or something like that.

29
00:03:06,760 --> 00:03:18,280
Because unfortunately, by product of letting go is not worrying too much about your belongings.

30
00:03:18,280 --> 00:03:27,200
So for the best advice is to leave your valuables at home when you go to the monastery.

31
00:03:27,200 --> 00:03:32,720
In large crowds, you know, you're sitting around meditating.

32
00:03:32,720 --> 00:03:36,880
So they got there, these two thieves went and knew the Buddha was giving a talk and so

33
00:03:36,880 --> 00:03:45,840
they went and they saw the crowd and they split up to go and pickpocket the belongings

34
00:03:45,840 --> 00:03:49,080
of the audience.

35
00:03:49,080 --> 00:03:54,560
But when you heard the Buddha speak, he stopped for a second, kind of looking and scoping

36
00:03:54,560 --> 00:04:00,680
out his target and then he stopped and listened to what the Buddha had to say.

37
00:04:00,680 --> 00:04:04,280
And he became so enchanted by what the Buddha said that he just forgot all about stealing

38
00:04:04,280 --> 00:04:09,000
and he really listened to what the Buddha had to say and got the deeper meaning and realized

39
00:04:09,000 --> 00:04:19,360
there was a higher purpose to life and that theft of a few coins was meaningless in the

40
00:04:19,360 --> 00:04:22,200
whole scheme of grand scheme of things.

41
00:04:22,200 --> 00:04:25,960
And so he sat down and he started meditating and based on the Buddha's teaching, he was

42
00:04:25,960 --> 00:04:34,640
able to become a soda panda, just sitting there listening.

43
00:04:34,640 --> 00:04:38,880
And so at the end of the night, he just went back home without stealing anything, the other

44
00:04:38,880 --> 00:04:46,400
guy went, found his target to store some, I think it was five gold coins, pocketed them,

45
00:04:46,400 --> 00:04:55,480
went back home and had his wife cook him up some rich food and he had gotten the dough,

46
00:04:55,480 --> 00:05:06,680
got in the spoils and so he had his wife prepare a meal with buying groceries or whoever

47
00:05:06,680 --> 00:05:07,680
and preparing a meal for him.

48
00:05:07,680 --> 00:05:12,160
The other guy went home and knowing he had no money and he had nothing to show for it went

49
00:05:12,160 --> 00:05:15,520
home and ate nothing.

50
00:05:15,520 --> 00:05:23,440
And then the thief who actually stole, found out about this, they bragging to each other,

51
00:05:23,440 --> 00:05:27,520
they went up and asked him what he had gotten and he said, he didn't get anything and he

52
00:05:27,520 --> 00:05:34,080
looked at them and he said, oh, he said, what I got from this was wisdom.

53
00:05:34,080 --> 00:05:38,480
And the first thief says, oh, really wise, you're so wise, you can't even feed your

54
00:05:38,480 --> 00:05:39,480
family.

55
00:05:39,480 --> 00:05:44,520
Like at me, what I've got, you call yourself wise, who's the one who's able to feed,

56
00:05:44,520 --> 00:05:49,200
who's able to put a meal on the table.

57
00:05:49,200 --> 00:05:55,800
And the other thief, ex thief, looked at his ex friend and shook his head and thought

58
00:05:55,800 --> 00:06:02,280
to himself, look at this guy, this fool who thinks he's a wise man, not realizing how

59
00:06:02,280 --> 00:06:03,280
foolish he is.

60
00:06:03,280 --> 00:06:07,520
And so he went to the Buddha, he went back to Jita Wanda where the Buddha was staying

61
00:06:07,520 --> 00:06:14,760
in the monastery and related this to the Buddha and said, it's amazing how foolish people

62
00:06:14,760 --> 00:06:21,520
they think passes for wisdom, and that's the story the Buddha then told us, he said very

63
00:06:21,520 --> 00:06:27,280
much, that's very much the case, the true fool thinks they're wise, but a person if they're

64
00:06:27,280 --> 00:06:34,160
able to know that they're foolish, to that extent you can call them wise.

65
00:06:34,160 --> 00:06:39,720
This is an important verse because it highlights the importance of wisdom, the importance

66
00:06:39,720 --> 00:06:48,200
of truth, of understanding the truth, where true wisdom, true wisdom means understanding

67
00:06:48,200 --> 00:06:58,640
the truth, doesn't mean being able to lie and cheat and can I or plunder and so on.

68
00:06:58,640 --> 00:07:05,480
There's a difference between worldly intelligence and wisdom.

69
00:07:05,480 --> 00:07:12,800
And the key here that does have some interesting implications is that knowledge is always

70
00:07:12,800 --> 00:07:19,240
better.

71
00:07:19,240 --> 00:07:25,920
Many people are shocked when they hear that in Buddhism, we say it's better to know that

72
00:07:25,920 --> 00:07:28,920
you're doing, that what you're doing is wrong.

73
00:07:28,920 --> 00:07:37,600
So this intuitive idea that if you commit a bad deed, we talked about this on Sunday,

74
00:07:37,600 --> 00:07:46,920
the Dharma at the MBBCA, if we have this intuitive notion that if someone does a bad, performs

75
00:07:46,920 --> 00:07:52,640
it on wholesome deed, not knowing that it's on wholesome, that there's somehow less

76
00:07:52,640 --> 00:07:56,880
responsible, less culpable for their behavior.

77
00:07:56,880 --> 00:08:00,440
Whereas a person who does something knowing it's wrong, we always say, you should know

78
00:08:00,440 --> 00:08:03,240
better, you know better than that.

79
00:08:03,240 --> 00:08:08,920
So if the younger sibling steals something, they say he doesn't know any better.

80
00:08:08,920 --> 00:08:10,440
And so they let him offer that.

81
00:08:10,440 --> 00:08:16,920
But the older sibling who knows better, this is how it goes in accordance as well, right?

82
00:08:16,920 --> 00:08:22,360
The kid who's 12 to 18 doesn't know any better.

83
00:08:22,360 --> 00:08:26,160
Under 18 doesn't know any better, but once you're 18, you know better.

84
00:08:26,160 --> 00:08:27,160
And so you're punished.

85
00:08:27,160 --> 00:08:31,880
So we get this idea that somehow that wouldn't be the case for karma.

86
00:08:31,880 --> 00:08:38,480
It would be less karmically potent to do something not knowing that it's wrong.

87
00:08:38,480 --> 00:08:47,760
And that's really what this verse is talking about, that if all you know is that you're

88
00:08:47,760 --> 00:08:51,920
a fool, that's better than not knowing you're a fool and going about doing things thinking

89
00:08:51,920 --> 00:08:53,520
your wise.

90
00:08:53,520 --> 00:08:59,920
This story aptly illustrates that point, that the person who knows what they're doing

91
00:08:59,920 --> 00:09:06,520
is wrong, even if they're engaging in it, the idea is that they will engage in it hesitantly.

92
00:09:06,520 --> 00:09:10,400
But a person who thinks they're wise in doing well, not only perform evil needs but

93
00:09:10,400 --> 00:09:15,080
will boast and brag about them.

94
00:09:15,080 --> 00:09:22,760
An act of theft, whether you know it's wrong or not, is a corrupting act, it's a violation

95
00:09:22,760 --> 00:09:31,680
of other people's property, people's very being.

96
00:09:31,680 --> 00:09:44,240
If you can't see that, the intent to do it, the zest and the zeal in performing the act,

97
00:09:44,240 --> 00:09:48,640
it's much higher than if you can see that you're actually violating the person's being.

98
00:09:48,640 --> 00:09:54,920
If you know that what you're doing is reprehensible, there's that force against you, the

99
00:09:54,920 --> 00:09:57,120
internal force acting against you.

100
00:09:57,120 --> 00:10:06,200
The opposite holds for doing good deeds, what's an interesting truth, that if a person

101
00:10:06,200 --> 00:10:12,400
perform good deeds, they're more powerful when you know that they're good deeds.

102
00:10:12,400 --> 00:10:16,800
If you know something that is good for you, it's beneficial.

103
00:10:16,800 --> 00:10:24,320
You're more inclined to perform a more confidence in performing it, you're more content

104
00:10:24,320 --> 00:10:28,560
and interested in the zealous about it.

105
00:10:28,560 --> 00:10:32,760
Whereas if you don't know that something's good, your parents drag you off to the monastery

106
00:10:32,760 --> 00:10:38,280
to listen to the Buddhist teaching or you drag you off to give offerings to give food

107
00:10:38,280 --> 00:10:45,440
to the monks or if someone's nagging you to give charity or you're keeping the precepts

108
00:10:45,440 --> 00:10:56,840
out of keeping religious precepts out of some kind of traditional compulsion or so on,

109
00:10:56,840 --> 00:11:03,880
this is substandard to actually meaning it, actually, performing it from the heart.

110
00:11:03,880 --> 00:11:09,720
The point in both cases is knowledge is greatest importance.

111
00:11:09,720 --> 00:11:14,960
It's a catchphrase that you can use for people and if someone boasts about bad deeds you

112
00:11:14,960 --> 00:11:19,600
say, well, I mean, I'll be perfect, but at least I know my fault.

113
00:11:19,600 --> 00:11:25,040
This is really one of the most dangerous flaws that there is, along with like wrong view

114
00:11:25,040 --> 00:11:29,560
or belief in wrong view, it is a form of wrong view.

115
00:11:29,560 --> 00:11:35,680
If you believe that what you're doing is, if you believe that bad deeds are a good thing

116
00:11:35,680 --> 00:11:40,600
and that you're somehow wise in your foolishness, that's the most dangerous, that's more

117
00:11:40,600 --> 00:11:46,280
dangerous than the deeps themselves because that's what's going to compel you to perform

118
00:11:46,280 --> 00:11:52,880
the deeds with enthusiasm and perpetually.

119
00:11:52,880 --> 00:11:53,880
What does it mean to be a fool?

120
00:11:53,880 --> 00:12:02,560
A fool is someone who acts, speaks, and thinks unholesomely, so they perform unholesome

121
00:12:02,560 --> 00:12:09,200
deeds with the body, they kill, they steal, they cheat, they perform unholesome deeds

122
00:12:09,200 --> 00:12:19,920
with speech, they lie, they gossip, they scold and they prattle or chat, chatter and they

123
00:12:19,920 --> 00:12:25,040
perform unholesome deeds with mind, it means their minds are sort of greed and anger and

124
00:12:25,040 --> 00:12:33,240
delusion, ill will and conceit and so on.

125
00:12:33,240 --> 00:12:35,560
We all have these tendencies, right?

126
00:12:35,560 --> 00:12:41,440
We may not all kill and steal and lie and so on, but we all have the anger, the greed

127
00:12:41,440 --> 00:12:48,600
and the delusion until you become an Arhan, these things exist in the mind.

128
00:12:48,600 --> 00:12:59,280
That's not the worst, it's not the most important or the most horrific fact of the most

129
00:12:59,280 --> 00:13:03,560
horrific evil that there is, worst than actually, it's not the real problem.

130
00:13:03,560 --> 00:13:10,120
It's not the most compelling problem, the most compelling problem is our knowledge of what

131
00:13:10,120 --> 00:13:11,200
these are doing to ourselves.

132
00:13:11,200 --> 00:13:16,520
A person who is angry, if they know that they're angry, there's a potential to work

133
00:13:16,520 --> 00:13:17,520
it out.

134
00:13:17,520 --> 00:13:24,600
If they know that it's a bad thing to be angry, if a person is self-righteously angry,

135
00:13:24,600 --> 00:13:28,200
these are the people you have to watch out for, are the people who feel that there's

136
00:13:28,200 --> 00:13:33,440
benefit to being angry, people who think that there's benefit to killing, there's

137
00:13:33,440 --> 00:13:35,720
benefit to stealing and so on.

138
00:13:35,720 --> 00:13:36,720
This is the problem.

139
00:13:36,720 --> 00:13:40,800
A person who steals knowing it's wrong and feeling guilty about it, but steals a loaf

140
00:13:40,800 --> 00:13:49,200
of bread to feed their family, for example, this is an unwholesome act, but nothing compared

141
00:13:49,200 --> 00:13:58,400
to, nothing compared to stealing, not realizing there's anything wrong with it, stealing

142
00:13:58,400 --> 00:14:08,240
things with stealing from poor people, for example.

143
00:14:08,240 --> 00:14:11,360
So how does this relate to our meditation?

144
00:14:11,360 --> 00:14:16,200
It relates directly to insight meditation because insight meditation is what allows you

145
00:14:16,200 --> 00:14:18,400
to see this.

146
00:14:18,400 --> 00:14:24,640
The first step in insight meditation is not removing of greed, anger, and delusion.

147
00:14:24,640 --> 00:14:29,600
It focuses on one aspect of delusion and that's the wrong view aspect.

148
00:14:29,600 --> 00:14:37,040
The first step towards becoming wise is realizing that you're full, that's really it.

149
00:14:37,040 --> 00:14:41,040
This verse is very important for us at the beginning because this is our first goal, our

150
00:14:41,040 --> 00:14:42,040
first step.

151
00:14:42,040 --> 00:14:47,280
Our first step is to see what we're doing wrong, to realize that we're not doing the right

152
00:14:47,280 --> 00:14:52,720
things, that what we're doing is leading us to suffer.

153
00:14:52,720 --> 00:14:58,000
In fact, deep down, that's really the final step as well.

154
00:14:58,000 --> 00:15:04,320
Beginning it's intellectual realization that killing is wrong, eventually it's just

155
00:15:04,320 --> 00:15:11,960
realizes realization that all greed, all anger, and all delusion is causing us suffering,

156
00:15:11,960 --> 00:15:16,080
is harmful to our minds.

157
00:15:16,080 --> 00:15:30,360
So this constant and systematic observation, introspection, and analysis, that comes from

158
00:15:30,360 --> 00:15:46,160
just reminding ourselves and staying objective about our experience.

159
00:15:46,160 --> 00:15:53,480
This allows us to see what we're doing wrong and allows us to change our behavior.

160
00:15:53,480 --> 00:15:58,760
So in the beginning to know that we're foolish and then to do something about it.

161
00:15:58,760 --> 00:16:05,200
In fact, it really is deep down, it's the only step that needs to be accomplished.

162
00:16:05,200 --> 00:16:07,840
Every moment that you see that you're doing something foolish, that what you're doing

163
00:16:07,840 --> 00:16:13,760
is truly foolish at that moment, when you see that this reaction of yours, the things,

164
00:16:13,760 --> 00:16:19,920
when you react, when you overreact, a simple real-life examples, when you get anxious,

165
00:16:19,920 --> 00:16:23,880
if I get anxious that I have to talk in front of you, so I'm giving this talk and I feel

166
00:16:23,880 --> 00:16:24,880
anxious.

167
00:16:24,880 --> 00:16:31,240
Very ridiculous, how it's not helpful, it's not reasonable, it's not a proper response,

168
00:16:31,240 --> 00:16:36,680
it's really a silly thing to do, it's foolish.

169
00:16:36,680 --> 00:16:44,120
Seeing that wisdom, that is what changes my behavior.

170
00:16:44,120 --> 00:16:51,680
Next time I have no reason to act in that way, no reason to set my mind in that direction.

171
00:16:51,680 --> 00:16:55,720
When you truly realize that it's wrong, your mind doesn't go in that direction, this

172
00:16:55,720 --> 00:17:00,680
is the key to Buddhism, the key to the Buddha's teaching, this truth.

173
00:17:00,680 --> 00:17:09,320
That knowledge is really true knowledge, true wisdom, true understanding of the mistakes

174
00:17:09,320 --> 00:17:26,440
that we're making, knowledge of the problems with our reactions, this is the way out

175
00:17:26,440 --> 00:17:32,560
of suffering, this knowledge, knowledge of suffering, knowledge that what we're doing is

176
00:17:32,560 --> 00:17:34,480
causing us suffering.

177
00:17:34,480 --> 00:17:40,000
So very important verse, maybe even more important than it appears, but it's a very catchy

178
00:17:40,000 --> 00:17:45,960
one, and it's one that we should always think of, and you can flaunt it to people who

179
00:17:45,960 --> 00:17:56,560
would say such things, when you're good at issues, and who brag and boast about their bad

180
00:17:56,560 --> 00:18:00,720
deeds, their unholismness, their treachery, their trickery and so on.

181
00:18:00,720 --> 00:18:07,720
They shouldn't boast, but it's a reminder for people, it's a good thing to remind our

182
00:18:07,720 --> 00:18:15,080
friends, to remind ourselves that it's not, we don't have to be perfect, not at beginning.

183
00:18:15,080 --> 00:18:20,720
The path to perfection is to realize your imperfections, the path to become wise is to

184
00:18:20,720 --> 00:18:24,480
realize your foolishness, that's it, that's the path.

185
00:18:24,480 --> 00:18:32,480
So very important, and I think I've about beaten that horse dead, so there's the verse

186
00:18:32,480 --> 00:18:33,480
for tonight.

187
00:18:33,480 --> 00:18:37,720
Thank you all for listening, and we'll do some meditation, wishing you all peace, happiness

188
00:18:37,720 --> 00:18:59,800
and freedom from suffering, have a good one.

