1
00:00:00,000 --> 00:00:04,000
Look, it'll help me out, no?

2
00:00:04,000 --> 00:00:06,000
Many meditation practices.

3
00:00:06,000 --> 00:00:07,000
Okay, let me ask.

4
00:00:07,000 --> 00:00:11,000
Many meditation practices focus on peace and happiness,

5
00:00:11,000 --> 00:00:16,000
but I am interested in it to understand my existence if there is any.

6
00:00:16,000 --> 00:00:21,000
Enlightenment the rest is a byproduct in my view.

7
00:00:21,000 --> 00:00:26,000
What do you think suggests?

8
00:00:26,000 --> 00:00:28,000
Who wants to start?

9
00:00:28,000 --> 00:00:37,000
Do I understand this question?

10
00:00:37,000 --> 00:00:39,000
Okay, exactly.

11
00:00:39,000 --> 00:00:41,000
Let me start.

12
00:00:41,000 --> 00:00:44,000
You look at it from what I'll start.

13
00:00:44,000 --> 00:00:52,000
This is something I think Clemente brought up yesterday about something quite similar, actually,

14
00:00:52,000 --> 00:01:02,000
that the most important in his point of view was knowledge,

15
00:01:02,000 --> 00:01:04,000
and that's basically what he's saying.

16
00:01:04,000 --> 00:01:08,000
I'm interested to understand my existence.

17
00:01:08,000 --> 00:01:10,000
So I would go two ways here.

18
00:01:10,000 --> 00:01:11,000
I would say I can go two ways here.

19
00:01:11,000 --> 00:01:15,000
On the one hand, what you're saying could be very much

20
00:01:15,000 --> 00:01:18,000
the attainment of peace, happiness and freedom from suffering,

21
00:01:18,000 --> 00:01:23,000
because it gives you a certain contentment.

22
00:01:23,000 --> 00:01:31,000
I think contentment is a very good word to understand your existence,

23
00:01:31,000 --> 00:01:36,000
because if you actually do understand reality,

24
00:01:36,000 --> 00:01:41,000
that brings and is brought about, I would say,

25
00:01:41,000 --> 00:01:54,000
I would argue by a desire for peace, for happiness, for freedom from suffering.

26
00:01:54,000 --> 00:01:58,000
You can't want something without thinking that it's going to bring you

27
00:01:58,000 --> 00:02:02,000
some form of what we call happiness or something positive.

28
00:02:02,000 --> 00:02:07,000
If you think of knowledge or understanding as positive,

29
00:02:07,000 --> 00:02:12,000
then you're already defining it as bringing something

30
00:02:12,000 --> 00:02:17,000
that must be of the sort of being peace, happiness and freedom from suffering.

31
00:02:17,000 --> 00:02:22,000
If understanding brought you suffering,

32
00:02:22,000 --> 00:02:30,000
or brought you what was undesirable, would you go for it?

33
00:02:30,000 --> 00:02:32,000
I mean, intrinsically.

34
00:02:32,000 --> 00:02:34,000
I don't mean that, well, it maybe hurts for a while,

35
00:02:34,000 --> 00:02:36,000
but in the end, it's all for the best.

36
00:02:36,000 --> 00:02:38,000
If it's all for the best, then it brings happiness.

37
00:02:38,000 --> 00:02:40,000
And that's what's great about understanding,

38
00:02:40,000 --> 00:02:45,000
is that intrinsically it can't help, but bring you peace, happiness and freedom from suffering.

39
00:02:45,000 --> 00:02:47,000
I don't think that there's much different.

40
00:02:47,000 --> 00:02:51,000
The other thing I wanted to say before I'll just say everything before,

41
00:02:51,000 --> 00:02:56,000
anyone else gets a chance, is that you have to be careful,

42
00:02:56,000 --> 00:02:59,000
because what do you mean by understanding your existence?

43
00:02:59,000 --> 00:03:04,000
Existence could be infinite, and it can be very easy in that sense

44
00:03:04,000 --> 00:03:09,000
to get caught up and get hung up on knowledge and understanding.

45
00:03:09,000 --> 00:03:12,000
How much understanding do you want?

46
00:03:12,000 --> 00:03:14,000
And what sort of understanding do you want?

47
00:03:14,000 --> 00:03:18,000
If it's beyond what actually brings you peace, happiness and freedom from suffering,

48
00:03:18,000 --> 00:03:20,000
what good is it?

49
00:03:20,000 --> 00:03:26,000
It can be addictive, and it can lead you to want more.

50
00:03:26,000 --> 00:03:29,000
Anyway, anyone else go for it?

51
00:03:29,000 --> 00:03:35,000
Yeah, I just want to really cook it.

52
00:03:35,000 --> 00:03:37,000
Some persons seem to be asking himself a question.

53
00:03:37,000 --> 00:03:40,000
Who am I from my existence?

54
00:03:40,000 --> 00:03:44,000
Well, I'm doing here what's my destiny?

55
00:03:44,000 --> 00:03:50,000
Well, who I supposed to be like, so on, so on.

56
00:03:50,000 --> 00:03:55,000
It says, like, many meditation practices focus on peace and happiness.

57
00:03:55,000 --> 00:03:59,000
How about the Department of Meditation,

58
00:03:59,000 --> 00:04:06,000
when you just observe rising and falling of all phenomena?

59
00:04:06,000 --> 00:04:09,000
There is no...

60
00:04:09,000 --> 00:04:12,000
Do I understand my existence?

61
00:04:12,000 --> 00:04:14,000
He says, it's very...

62
00:04:14,000 --> 00:04:16,000
There is no such a...

63
00:04:16,000 --> 00:04:17,000
You can't have it.

64
00:04:17,000 --> 00:04:21,000
From my experience,

65
00:04:21,000 --> 00:04:26,000
I don't understand myself as a person anymore.

66
00:04:26,000 --> 00:04:30,000
The person is not...

67
00:04:30,000 --> 00:04:32,000
There is no such a thing as to you, really,

68
00:04:32,000 --> 00:04:37,000
because you've done your personality.

69
00:04:37,000 --> 00:04:40,000
Your parents gave you your personality.

70
00:04:40,000 --> 00:04:44,000
You were born in Poland.

71
00:04:44,000 --> 00:04:48,000
You will be probably Roman Catholic.

72
00:04:48,000 --> 00:04:51,000
You will be still where I'm born.

73
00:05:18,000 --> 00:05:23,000
Right, Peter Rose.

74
00:05:23,000 --> 00:05:46,000
Okay.

75
00:05:46,000 --> 00:05:54,760
For instance, let's take knowledge. Knowledge is not wisdom. For instance, you

76
00:05:54,760 --> 00:06:00,160
have dirty dishes and a sink. You know they're dirty. Doesn't mean they're going

77
00:06:00,160 --> 00:06:07,520
to get clean. Wisdom is not even thinking about the dishes. It's just cleaning

78
00:06:07,520 --> 00:06:18,640
them. And right view is understanding the tension between the fact that you

79
00:06:18,640 --> 00:06:25,040
have dirty dishes and the fact that not having them is a source of happiness. So

80
00:06:25,040 --> 00:06:34,560
these three distinct terms people can often, you know, confuse us as if they're

81
00:06:34,560 --> 00:06:40,600
all the same thing. But knowledge is not wisdom. You know, you can have knowledge on

82
00:06:40,600 --> 00:06:49,480
how to emit mass murder. But does that mean it's wisdom? And we can have great

83
00:06:49,480 --> 00:06:57,120
wisdom and not utilizes. Does that make you knowledgeable? You know, you

84
00:06:57,120 --> 00:07:03,080
could have a lot of knowledge but not apply the wisdom to it. And again, you

85
00:07:03,080 --> 00:07:09,560
know, you can be living in the world. It may be not have a big resource of the

86
00:07:09,560 --> 00:07:16,520
academic knowledge, but have a very, very clear view of the way nature really is

87
00:07:16,520 --> 00:07:23,360
and the cause and the fact. And we really need to make the distinction between

88
00:07:23,360 --> 00:07:31,100
the three. That's interesting that you quit. It seems like understanding of the

89
00:07:31,100 --> 00:07:36,160
nature of the end. We need an expert to describe.

90
00:08:01,700 --> 00:08:07,380
Yeah, the person goes on to say the ask who goes on to say based on what we've

91
00:08:07,380 --> 00:08:13,340
said, this is why I say a byproduct and is good. However, I might be unhappy

92
00:08:13,340 --> 00:08:18,460
after knowing insignificance of humans. But I would still like to know even if

93
00:08:18,460 --> 00:08:23,700
it makes me unhappy, though I hope it doesn't. I would still like to know, but I

94
00:08:23,700 --> 00:08:28,460
hope even if I see it. So the knowledge is more important than whether it

95
00:08:28,460 --> 00:08:32,860
makes you happy. But I'd say there's still two sides to that. You may be wrong.

96
00:08:32,860 --> 00:08:38,300
You may, you may be wrong in thinking that because well, people like heroin,

97
00:08:38,300 --> 00:08:41,780
even though they, you know, even though it makes them unhappy, that doesn't

98
00:08:41,780 --> 00:08:46,700
make it beneficial to them to take heroin. The point of the Buddha's teaching is

99
00:08:46,700 --> 00:08:52,700
that you can have desires that are to your detriment, that you can strive for

100
00:08:52,700 --> 00:08:58,700
things that are actually causing you, you harm, that are actually not giving the

101
00:08:58,700 --> 00:09:04,140
result that you desire. So even desiring more knowledge, just because you

102
00:09:04,140 --> 00:09:07,860
desire, it doesn't make it right. Just because you want something, doesn't make

103
00:09:07,860 --> 00:09:11,660
it beneficial to you. It doesn't mean that by striving after striving after it,

104
00:09:11,660 --> 00:09:14,540
you're going to actually feel better and you're going to actually think to

105
00:09:14,540 --> 00:09:18,700
yourself, wow, it benefited from this. You know, with so many things in our

106
00:09:18,700 --> 00:09:22,020
lives, we think this will benefit me and we do it again and again and again and

107
00:09:22,020 --> 00:09:27,380
10 years later, we're still, we're maybe even worse, in a worse position than we

108
00:09:27,380 --> 00:09:34,020
were. But that's the point that he doesn't want to get a benefit. He, he or she

109
00:09:34,020 --> 00:09:41,300
wants to know the truth simply, not for their own benefit, just because that's

110
00:09:41,300 --> 00:09:48,900
what they strive for. Right, but by doing that, it could be the truth that by

111
00:09:48,900 --> 00:09:53,300
doing that, you, you, you've heard yourself, who's to say that it will not.

112
00:09:53,300 --> 00:09:59,220
You can destroy yourself, but at least you have, you will know the truth.

113
00:09:59,220 --> 00:10:04,900
Well, okay. What is, what is it? What is this all about?

114
00:10:04,900 --> 00:10:09,780
What is the question? Right. I mean, I'm, I'm all for it, but, but I, I'm for it

115
00:10:09,780 --> 00:10:13,940
because it brings peace. It brings happiness. I wouldn't be for it if it didn't.

116
00:10:13,940 --> 00:10:17,620
And I'm not for those sorts of knowledge that bring more suffering, more stress

117
00:10:17,620 --> 00:10:21,940
and, and, you know, as you say, destroy yourself. To me, that's ridiculous.

118
00:10:21,940 --> 00:10:25,940
Great. You've destroyed yourself. Well, we're going to go. You get a medal.

119
00:10:25,940 --> 00:10:32,580
Maybe we're, we're different. I'm, I'm all, all, all for, all to make, all to make the knowledge.

120
00:10:32,580 --> 00:10:38,660
Sure. Well, go for it. I mean, a Buddha has, has, has, has ultimate knowledge.

121
00:10:38,660 --> 00:10:42,500
But he also knows the things that can't be known.

122
00:10:42,500 --> 00:10:48,260
The things that shouldn't be thought about and so on. And I think, um, that includes

123
00:10:48,260 --> 00:10:53,460
some things like, is the universe infinite, is the universe finite and so on.

124
00:10:54,660 --> 00:11:01,060
Which is it possibly answer with that? Right. So I mean, the, the, the point is that

125
00:11:01,060 --> 00:11:11,060
there are many kinds of knowledge that are useless. Um, you know, I mean, pie to the ex digits,

126
00:11:11,060 --> 00:11:17,220
right? Okay. Well, that's knowledge. Yeah. But we're not striving for complete knowledge,

127
00:11:17,220 --> 00:11:24,420
but for, uh, we ought to make choose. Okay. Well, yes, that's fine. Um, but I, you know, I,

128
00:11:25,300 --> 00:11:27,620
I don't know. Um, to me, it's that's, that's a,

129
00:11:30,020 --> 00:11:36,820
even, that's an illogical, I would say reason for, you know, or it's in, uh, meaningless

130
00:11:36,820 --> 00:11:41,300
statement. I mean, I want knowledge for this sake of knowledge. If it's not for the purpose of

131
00:11:41,300 --> 00:11:47,220
some benefit, and so that's the thing, it's utilitarian. There was, absolutely.

132
00:11:47,220 --> 00:11:52,580
Yeah, I guess ultimately you will be happy. You have, uh, access to your ultimate truth,

133
00:11:52,580 --> 00:11:58,660
even if it had you because it doesn't fit with your preconceptions or whatever. So you might

134
00:11:58,660 --> 00:12:04,100
be, but ultimately you're happy because you've reached, uh, your goal, which is, uh,

135
00:12:04,100 --> 00:12:08,900
well, and the wonderful thing is that it doesn't make you unhappy. Ultimately, truth makes

136
00:12:08,900 --> 00:12:10,820
you a perfectly happy.

137
00:12:14,420 --> 00:12:20,180
From into your study thing. Yeah, you know, what's it up? What's real interesting,

138
00:12:20,180 --> 00:12:27,540
you do a read and have to call my goal is to interrupt where the comma literally lays down the

139
00:12:27,540 --> 00:12:34,900
62 views of the distance that most only human being is going to have one of them, even to the

140
00:12:34,900 --> 00:12:43,300
stage about if the universe infinity is not really born, do you not? You know, is there a creator,

141
00:12:43,300 --> 00:12:52,340
is there not that ultimately knowledge in all this roots and gives useless. It, it, it doesn't

142
00:12:52,340 --> 00:12:58,100
mean to enlighten what's the level. And, uh, there's comma at least states over and over again

143
00:12:58,740 --> 00:13:05,300
that, you know, intellectual knowledge and intellectual critique, you know, uh, we have to be

144
00:13:05,300 --> 00:13:11,460
happy dawn and it goes on and on and on, but, you know, known ads and diads and fly ads,

145
00:13:11,460 --> 00:13:21,220
but really knowledge does not equate your enlightenment. And, uh, sorry, can I, can I interrupt you

146
00:13:21,220 --> 00:13:27,700
just for a second? I've got the comma, please, please, can I interrupt you just for a second?

147
00:13:27,700 --> 00:13:32,500
Really, really knowledge that the benefit of that would alleviate suffering?

148
00:13:32,500 --> 00:13:37,380
Oh, no, no. Uh, Clemente, you have to turn something wrong with your mic, you're

149
00:13:37,380 --> 00:13:45,460
stidicking, you got heavy static there. I don't know what's wrong, but you're giving us

150
00:13:45,460 --> 00:13:53,060
great static. I think I might be you. Um, I think me. No, I can't pronounce your name. Um,

151
00:13:53,060 --> 00:13:59,540
my fellow Canadian. Okay, sorry, Luke, continue. It's better now, whatever it was.

152
00:13:59,540 --> 00:14:06,980
Oh, okay, you can hear me now. Yep. Oh, I was saying that, uh, for instance,

153
00:14:06,980 --> 00:14:14,420
in the bromagile of sutra, there's a comma that he's down, he's 62 views of existence.

154
00:14:15,620 --> 00:14:23,540
Uh, you know, the point that I was trying to make was that the comma laid down every possible

155
00:14:23,540 --> 00:14:29,620
view that people can have about relative and ultimate existence about the nature of the universe

156
00:14:29,620 --> 00:14:38,660
and all of it. And his point is that knowledge does not lead to happiness. No, it doesn't

157
00:14:38,660 --> 00:14:44,980
heat it. It doesn't even lead to the truth. Right. Knowledge does not lead to the truth and that

158
00:14:45,700 --> 00:14:52,020
all of that, all of your inquiries to the nature of the universe. Okay, it's nice as an intellectual

159
00:14:52,020 --> 00:15:02,420
pursuit, but it does not alleviate suffering, which is his teaching. And, you know, this, this is

160
00:15:02,420 --> 00:15:08,420
really important in the Buddha Dharma is that, you know, we're not, I mean, if the point of the

161
00:15:08,420 --> 00:15:15,060
Buddha Dharma was to be intellectual, dilettante, well, then we don't need the Buddha Dharma. All we

162
00:15:15,060 --> 00:15:22,340
need is a very good university system. We all we need to rely on is the worldly system as we

163
00:15:22,340 --> 00:15:32,500
already have it. Okay, but it's kind of a, a sophistic question of whether ultimate truth,

164
00:15:33,300 --> 00:15:39,700
ultimate understanding would be worthwhile, even if it didn't bring happiness. Here's maybe

165
00:15:39,700 --> 00:15:44,820
how the question should be framed, which, you know, it's a bit sufficed because self is

166
00:15:44,820 --> 00:15:50,980
sufficed. So he asked, I'm thinking of selfism. I don't know how you say sufficed. I think

167
00:15:50,980 --> 00:15:54,660
sufficed. I know the joke related to that, but I don't know how to pronounce the word. Okay.

168
00:15:55,460 --> 00:16:00,260
Well, the point being, you know, kind of a useless question, because it's moot point, I guess

169
00:16:00,260 --> 00:16:08,900
you could say it, because that would be such a messed up universe where I'll, understanding of,

170
00:16:08,900 --> 00:16:15,060
of things as they are, didn't lead to happiness or didn't lead to peace. That's, you know, it's so

171
00:16:16,180 --> 00:16:22,820
intrinsic that they almost mean one and the same, one and the same thing. Ultimate realization,

172
00:16:23,540 --> 00:16:32,900
you know, of the wisdom of things as they are, is, is by definition, happiness.

173
00:16:32,900 --> 00:16:40,340
Well, let me correct myself if I, I didn't mean to imply that. What I meant was that

174
00:16:41,460 --> 00:16:51,620
taking a stand that you have to write to you, because you can elucidate facts and sort of prove

175
00:16:51,620 --> 00:16:59,540
your theories, that is the distinction from having genuine right you. No, I got you. I just wanted

176
00:16:59,540 --> 00:17:06,020
to make that distinction. Yeah. No, for sure. That's, I mean, and that's really where the

177
00:17:06,020 --> 00:17:11,700
problem comes. How do you know? It's easy to say I want ultimate truth. I want ultimate truth. But

178
00:17:12,900 --> 00:17:17,460
do you know what you're talking about, right? And until you realize it, and so the problem is that

179
00:17:17,460 --> 00:17:25,460
we can get caught up in speculation, which has not, which is not to do with ultimate truth. And

180
00:17:25,460 --> 00:17:33,140
we can get caught up in speculation about things thinking this is a, this is an aspect of

181
00:17:33,140 --> 00:17:42,180
ultimate truth when in fact it's not, you know, that ideas of, you know, the universe being finite

182
00:17:42,180 --> 00:17:47,780
and infinite. Doesn't our hunt exist after death or does, do they not exist after death?

183
00:17:47,780 --> 00:17:56,180
And there's these 10 questions that are considered to be fairly canonical questions that you just

184
00:17:56,180 --> 00:18:07,220
don't ask. Because you can't know? Because the more you investigate, the more naughty and complicated

185
00:18:07,220 --> 00:18:11,860
they become. Like look at, look at physics, for example, it's kind of a joke. First we had the

186
00:18:11,860 --> 00:18:16,740
Adam, and then we had subatomic particles. Now we don't even know what we've got, strings, I guess.

187
00:18:16,740 --> 00:18:22,180
There's some interesting space between the strings and something else.

188
00:18:22,180 --> 00:18:29,780
Yeah, and it's like fractals. It's just, it's like watching a, watching a clown show because it

189
00:18:29,780 --> 00:18:36,900
just gets more and more absurd. 11 dimensions now we need. And that's not enough. Now we need

190
00:18:36,900 --> 00:18:43,380
the multiverse. Anyway, I mean, not to joke because there's some serious things going on there,

191
00:18:43,380 --> 00:18:49,860
but it's potentially one question that that is unanswerable. You know, it may just keep going on

192
00:18:49,860 --> 00:18:56,980
for infinity. And we've come to the limit of human understanding. We can't go very much further,

193
00:18:56,980 --> 00:19:02,420
but reality could go a million times further. You know, if all we can get to is strings and

194
00:19:02,420 --> 00:19:10,100
11 dimensions, well, what if there's billions of iterations or infinite iterations? Well,

195
00:19:10,100 --> 00:19:16,420
let's never get there. I mean, it may be that these sorts of things are unknowable by human kind.

196
00:19:18,100 --> 00:19:23,060
And, and I guess the point is that even if they were knowable, they have nothing to do with

197
00:19:23,060 --> 00:19:35,460
the, the core reality. So, and I guess the, another point that we could make is how much of

198
00:19:35,460 --> 00:19:41,220
ultimate reality. Do you need all of ultimate reality? Do you need to know the ultimate reality of

199
00:19:41,220 --> 00:19:46,500
every being? You know, do you need to know, you know, for example, do you need to know

200
00:19:48,100 --> 00:19:54,260
the, the, the course of events with human being? I mean, every, every being has their own course,

201
00:19:54,260 --> 00:19:58,260
right? Do you need to know whether they're, they're going to attain nibana? Is this person going

202
00:19:58,260 --> 00:20:01,940
to? Is that person going to where are they going in their next life? Because all of that has to

203
00:20:01,940 --> 00:20:07,380
do with ultimate reality? Do you need to know all of that? No, but I think you're talking about knowledge

204
00:20:07,380 --> 00:20:12,340
there. And maybe the question was more about wisdom. Well, that's a good point. We don't,

205
00:20:12,340 --> 00:20:18,420
let's try to know everything, but have wisdom to make sure it's about reality.

206
00:20:18,420 --> 00:20:23,300
Okay. Very good point. And in that case, yeah, I would just go back to saying that they're equal

207
00:20:23,300 --> 00:20:28,020
in my mind there and it's intrinsically peace happiness and freedom from suffering.

208
00:20:28,020 --> 00:20:37,460
And I don't know. I think that's the best I can do. Anybody else? Welcome, Owen.

209
00:20:37,460 --> 00:20:59,460
Okay.

