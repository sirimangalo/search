1
00:00:00,000 --> 00:00:06,000
Hey, good evening.

2
00:00:06,000 --> 00:00:14,000
Hi, Robin.

3
00:00:14,000 --> 00:00:20,000
Can you hear me?

4
00:00:20,000 --> 00:00:23,000
Don't hear me.

5
00:00:23,000 --> 00:00:28,000
Okay, we're ready to start.

6
00:00:28,000 --> 00:00:31,000
So good evening, everyone.

7
00:00:31,000 --> 00:00:39,000
Broadcasting live from Stony Creek, Ontario, August 9th.

8
00:00:39,000 --> 00:00:42,000
I heard you there.

9
00:00:42,000 --> 00:00:46,000
I hear you.

10
00:00:46,000 --> 00:00:53,000
You hear me?

11
00:00:53,000 --> 00:00:55,000
I heard you.

12
00:00:55,000 --> 00:01:01,000
I heard something.

13
00:01:01,000 --> 00:01:03,000
We haven't joined by Robin tonight.

14
00:01:03,000 --> 00:01:06,000
If you'd like to join the Hangout, well, you have to get special

15
00:01:06,000 --> 00:01:08,000
permission.

16
00:01:08,000 --> 00:01:12,000
But it's not hard to get just asking.

17
00:01:12,000 --> 00:01:15,000
Well, actually, we probably wouldn't.

18
00:01:15,000 --> 00:01:19,000
This isn't something I'm going to open up to everyone.

19
00:01:19,000 --> 00:01:27,000
The idea here is to talk about this verse and answer people's questions.

20
00:01:27,000 --> 00:01:31,000
But tonight, we have something special as well that I'd like to bring up.

21
00:01:31,000 --> 00:01:35,000
And I was hoping Robin could maybe talk about it, but she's muted.

22
00:01:35,000 --> 00:01:37,000
I don't hear her.

23
00:01:37,000 --> 00:01:39,000
No.

24
00:01:39,000 --> 00:01:41,000
I see your lips are moving.

25
00:01:41,000 --> 00:01:45,000
No sound is coming up.

26
00:01:45,000 --> 00:01:49,000
Was for a second. I heard something. I think it might be a cable or something.

27
00:01:49,000 --> 00:01:51,000
How about that? Can you hear me now?

28
00:01:51,000 --> 00:01:54,000
Now I can hear you, but this sounds pretty awful.

29
00:01:54,000 --> 00:01:55,000
But that might be in my end.

30
00:01:55,000 --> 00:01:58,000
That might be on my end, because that was happening before just a second.

31
00:01:58,000 --> 00:02:02,000
That was when you were on Firefox.

32
00:02:02,000 --> 00:02:03,000
Okay.

33
00:02:03,000 --> 00:02:04,000
Go again.

34
00:02:04,000 --> 00:02:05,000
How is it now?

35
00:02:05,000 --> 00:02:10,000
Nope.

36
00:02:10,000 --> 00:02:19,000
Yeah, it's at my end.

37
00:02:19,000 --> 00:02:24,000
Are you able to go on Google at all and Google Chrome for this?

38
00:02:24,000 --> 00:02:30,000
Because I think last time the problem was when you were on Firefox.

39
00:02:30,000 --> 00:02:41,000
Now I've got a problem.

40
00:02:41,000 --> 00:03:08,000
I'm uploading it.

41
00:03:08,000 --> 00:03:25,000
I'm uploading it.

42
00:03:25,000 --> 00:03:27,000
Okay, how about now?

43
00:03:27,000 --> 00:03:29,000
Oh, how is that?

44
00:03:29,000 --> 00:03:30,000
No, you're still there.

45
00:03:30,000 --> 00:03:36,000
But that's just at my end. I don't think anyone else is bothered by it.

46
00:03:36,000 --> 00:04:01,000
I just got a deal with it.

47
00:04:01,000 --> 00:04:07,000
Anyway, enough of this. Good evening, as I said.

48
00:04:07,000 --> 00:04:09,000
Welcome.

49
00:04:09,000 --> 00:04:18,000
Today we're looking at a quote by Buddha.

50
00:04:18,000 --> 00:04:22,000
Actually a conversation between him and a Brahman.

51
00:04:22,000 --> 00:04:33,000
The Brahman says again with these views.

52
00:04:33,000 --> 00:04:35,000
Brahman has his view.

53
00:04:35,000 --> 00:04:41,000
So again we have this clear distinction between the Buddha and others.

54
00:04:41,000 --> 00:04:43,000
But it's not interested in views.

55
00:04:43,000 --> 00:04:47,000
So again we have someone coming to the Buddha saying,

56
00:04:47,000 --> 00:04:53,000
all right, here's what I believe.

57
00:04:53,000 --> 00:04:58,000
Keeping his views all bottled up is the thing about views.

58
00:04:58,000 --> 00:05:00,000
Because they're hard to hold on to.

59
00:05:00,000 --> 00:05:02,000
It takes energy and effort.

60
00:05:02,000 --> 00:05:05,000
So you have to exercise.

61
00:05:05,000 --> 00:05:11,000
Because you're constantly a swaghed by your own doubt.

62
00:05:11,000 --> 00:05:17,000
So it's a view. It's not based on empirical evidence.

63
00:05:17,000 --> 00:05:21,000
So in order to maintain it, you have to push.

64
00:05:21,000 --> 00:05:26,000
And the push is in trying to get other people to believe the same things as you.

65
00:05:26,000 --> 00:05:28,000
This is why people proselytize.

66
00:05:28,000 --> 00:05:30,000
They may say it's for whatever reason.

67
00:05:30,000 --> 00:05:32,000
But it's actually to reaffirm their own belief.

68
00:05:32,000 --> 00:05:35,000
Because belief is difficult to take, difficult to keep.

69
00:05:35,000 --> 00:05:37,000
So this Brahman he can't keep it.

70
00:05:37,000 --> 00:05:40,000
He's holding onto this view and he's like,

71
00:05:40,000 --> 00:05:41,000
believes it's so much.

72
00:05:41,000 --> 00:05:45,000
But in order to maintain that belief, whenever it starts to slip,

73
00:05:45,000 --> 00:05:47,000
he would have to go in.

74
00:05:47,000 --> 00:05:50,000
I mean, just assuming he would have to go and tell others.

75
00:05:50,000 --> 00:05:51,000
That's why he comes to the Buddha.

76
00:05:51,000 --> 00:05:53,000
That's why people would come to the Buddha.

77
00:05:53,000 --> 00:05:57,000
It would become to me and with these sort of things, all their views.

78
00:05:57,000 --> 00:06:01,000
It's nice to have the example of the Buddha to say,

79
00:06:01,000 --> 00:06:04,000
yeah, well, that's your view.

80
00:06:04,000 --> 00:06:07,000
There was an article someone posted recently

81
00:06:07,000 --> 00:06:12,000
and I was discussing it with them about this fact.

82
00:06:12,000 --> 00:06:17,000
And the idea that people think that their view,

83
00:06:17,000 --> 00:06:21,000
the idea of an opinion where people will come to you and say,

84
00:06:21,000 --> 00:06:25,000
well, my opinion, in my opinion, X.

85
00:06:25,000 --> 00:06:29,000
And it would be something like, in my opinion,

86
00:06:29,000 --> 00:06:32,000
vaccines give you Alzheimer's.

87
00:06:32,000 --> 00:06:34,000
You know, something like that.

88
00:06:34,000 --> 00:06:36,000
I mean, I don't know about vaccines giving you Alzheimer's,

89
00:06:36,000 --> 00:06:39,000
but the trust of the article is that,

90
00:06:39,000 --> 00:06:42,000
well, in many cases, it's not even really an opinion.

91
00:06:42,000 --> 00:06:44,000
It's just a mistake.

92
00:06:44,000 --> 00:06:46,000
It's an erroneous statement.

93
00:06:46,000 --> 00:06:48,000
It's just wrong.

94
00:06:48,000 --> 00:06:57,000
An opinion is I like onions or I like lavender or something.

95
00:06:57,000 --> 00:07:01,000
But there's this idea that you could hide behind something

96
00:07:01,000 --> 00:07:05,000
because it's your opinion.

97
00:07:05,000 --> 00:07:07,000
And also, if something is just my opinion,

98
00:07:07,000 --> 00:07:11,000
like everyone's entitled to their opinions.

99
00:07:11,000 --> 00:07:15,000
I mean, well, yeah, but it doesn't mean anything

100
00:07:15,000 --> 00:07:16,000
that you have an opinion.

101
00:07:16,000 --> 00:07:20,000
And so we were talking about whether there's

102
00:07:20,000 --> 00:07:23,000
any meaning to having an opinion.

103
00:07:23,000 --> 00:07:29,000
And besides how it affects your own mind,

104
00:07:29,000 --> 00:07:30,000
there's really nothing.

105
00:07:30,000 --> 00:07:31,000
It means nothing.

106
00:07:31,000 --> 00:07:34,000
When you tell me, I believe X, it's really meaningless.

107
00:07:34,000 --> 00:07:38,000
You know, if 90% of humans believed something,

108
00:07:38,000 --> 00:07:40,000
it would still be meaningless.

109
00:07:40,000 --> 00:07:43,000
You would still have no bearing on the truth.

110
00:07:43,000 --> 00:07:45,000
If 99% of humans believed something

111
00:07:45,000 --> 00:07:48,000
would still have no bearing on the truth.

112
00:07:48,000 --> 00:07:53,000
So belief in and of itself is of no import.

113
00:07:53,000 --> 00:07:56,000
Anyway, people seem to think it is important,

114
00:07:56,000 --> 00:07:58,000
but it's important that they tell you.

115
00:07:58,000 --> 00:08:01,000
So he came to go to the Buddha and said,

116
00:08:01,000 --> 00:08:04,000
OK, this is my opinion.

117
00:08:04,000 --> 00:08:07,000
There's no harm in telling people the truth.

118
00:08:07,000 --> 00:08:10,000
It's an interesting statement.

119
00:08:10,000 --> 00:08:17,000
It reminds one of the question of whether telling the truth

120
00:08:17,000 --> 00:08:20,000
means not lying means always telling the truth.

121
00:08:20,000 --> 00:08:22,000
Does not lying always mean telling the truth?

122
00:08:22,000 --> 00:08:24,000
And from a Buddhist point of view,

123
00:08:24,000 --> 00:08:28,000
obviously, especially here, the answer is no.

124
00:08:28,000 --> 00:08:32,000
This isn't what we mean by not telling a lie.

125
00:08:32,000 --> 00:08:34,000
Not telling a lie is actually lying.

126
00:08:34,000 --> 00:08:37,000
If you omit the truth, and as a result,

127
00:08:37,000 --> 00:08:39,000
someone has a mistaken understanding,

128
00:08:39,000 --> 00:08:41,000
that's not the same as lying.

129
00:08:41,000 --> 00:08:44,000
If you do anything to intentionally mislead someone,

130
00:08:44,000 --> 00:08:46,000
well, that could be considered lying.

131
00:08:46,000 --> 00:08:49,000
But withholding the truth is not considered lying,

132
00:08:49,000 --> 00:08:52,000
because withholding the truth can often be quite beneficial.

133
00:08:52,000 --> 00:08:55,000
The problem with lying is that it's a distortion.

134
00:08:55,000 --> 00:08:58,000
It's a perversion of reality.

135
00:08:58,000 --> 00:09:01,000
What you're doing is you're intentionally taking someone away

136
00:09:01,000 --> 00:09:02,000
from the truth.

137
00:09:02,000 --> 00:09:05,000
And so inherently, it's antithetical

138
00:09:05,000 --> 00:09:12,000
to the practice of Buddhism and attainment of the truth.

139
00:09:12,000 --> 00:09:14,000
But the other thing about this statement

140
00:09:14,000 --> 00:09:17,000
that the Buddha gives race is, I don't say that.

141
00:09:17,000 --> 00:09:20,000
I don't hold on to such a view.

142
00:09:20,000 --> 00:09:22,000
Instead, this is the Buddha looks at things

143
00:09:22,000 --> 00:09:25,000
from a completely different point of view,

144
00:09:25,000 --> 00:09:27,000
and it's in one sense utilitarian,

145
00:09:27,000 --> 00:09:29,000
if it's beneficial.

146
00:09:29,000 --> 00:09:32,000
If it helps, if saying something helps,

147
00:09:32,000 --> 00:09:34,000
then he will say it.

148
00:09:34,000 --> 00:09:37,000
If saying something doesn't help, then he won't say it.

149
00:09:37,000 --> 00:09:39,000
Now, the question then comes,

150
00:09:39,000 --> 00:09:43,000
what could you lie if it was beneficial?

151
00:09:43,000 --> 00:09:45,000
Or could you kill if it was beneficial?

152
00:09:45,000 --> 00:09:48,000
And the point is that, of course, that's not possible.

153
00:09:48,000 --> 00:09:55,000
If it's a lie is problematic, is inherently harmful.

154
00:09:55,000 --> 00:09:57,000
And so, no.

155
00:09:57,000 --> 00:10:02,000
Doing something that is harmful is not in the capability

156
00:10:02,000 --> 00:10:08,000
actually, have been enlightened being capable of intentional harm

157
00:10:08,000 --> 00:10:11,000
to themselves or others.

158
00:10:11,000 --> 00:10:14,000
So, intentionally telling a lie is not possible.

159
00:10:14,000 --> 00:10:18,000
Intentionally stealing, intentionally killing,

160
00:10:18,000 --> 00:10:22,000
intentionally doing something that harms others.

161
00:10:22,000 --> 00:10:27,000
They're intended for the intention to harm.

162
00:10:27,000 --> 00:10:28,000
Or oneself.

163
00:10:28,000 --> 00:10:33,000
I mean, intent having the unwholesome mind state

164
00:10:33,000 --> 00:10:40,000
that would give rise to killing or stealing is not possible.

165
00:10:40,000 --> 00:10:42,000
But the point here is in the difference

166
00:10:42,000 --> 00:10:49,000
in the Buddha's approach to reality and it's in regards to

167
00:10:49,000 --> 00:10:52,000
not whether it's this way or that way,

168
00:10:52,000 --> 00:10:54,000
but whether it's helpful.

169
00:10:54,000 --> 00:10:57,000
So, the Buddha is quite malleable and quite flexible

170
00:10:57,000 --> 00:11:02,000
in regards to a lot of the views that arose.

171
00:11:02,000 --> 00:11:05,000
So, as far as telling truth,

172
00:11:05,000 --> 00:11:07,000
that's quite a simple teaching.

173
00:11:07,000 --> 00:11:10,000
I mean, the actual teaching here is just that

174
00:11:10,000 --> 00:11:13,000
it's not always useful to tell the truth,

175
00:11:13,000 --> 00:11:17,000
but the premise behind it is more interesting.

176
00:11:17,000 --> 00:11:19,000
And it's that in regards to views,

177
00:11:19,000 --> 00:11:22,000
they're not really of any important opinions

178
00:11:22,000 --> 00:11:23,000
are not useful.

179
00:11:23,000 --> 00:11:25,000
The Buddha doesn't hold on to them,

180
00:11:25,000 --> 00:11:27,000
doesn't give them any weight.

181
00:11:27,000 --> 00:11:34,000
What he gave way to is truth, value, benefit.

182
00:11:34,000 --> 00:11:38,000
So, that's the demo for tonight.

183
00:11:38,000 --> 00:11:46,000
Does anybody have any questions?

184
00:11:46,000 --> 00:11:49,000
What if somebody can handle the truth?

185
00:11:49,000 --> 00:12:07,000
I don't understand.

186
00:12:07,000 --> 00:12:09,000
No, such as the non-existence.

187
00:12:09,000 --> 00:12:14,000
I mean, cannot handle the truth, is that what you're asking?

188
00:12:14,000 --> 00:12:18,000
I think we've got some grammar issues.

189
00:12:18,000 --> 00:12:20,000
If someone can't handle the truth,

190
00:12:20,000 --> 00:12:22,000
like they believe that God exists,

191
00:12:22,000 --> 00:12:24,000
so you shouldn't tell them that he doesn't exist.

192
00:12:24,000 --> 00:12:30,000
That God doesn't exist.

193
00:12:30,000 --> 00:12:33,000
I mean, this is another, you don't have to.

194
00:12:33,000 --> 00:12:35,000
It's important to always be right.

195
00:12:35,000 --> 00:12:37,000
It's not always important.

196
00:12:37,000 --> 00:12:40,000
It's not always wise to try to convince other people

197
00:12:40,000 --> 00:12:45,000
that you're right.

198
00:12:45,000 --> 00:12:47,000
And there's a big difference.

199
00:12:47,000 --> 00:12:55,000
It's not our duty to fix other people.

200
00:12:55,000 --> 00:13:01,000
It's not our role, our goal doesn't help to try.

201
00:13:01,000 --> 00:13:06,000
It's our role and our duty to be clear in the mind,

202
00:13:06,000 --> 00:13:11,000
to be mindful, to be beneficial,

203
00:13:11,000 --> 00:13:16,000
to be helpful, to be kind,

204
00:13:16,000 --> 00:13:21,000
to be pure.

205
00:13:21,000 --> 00:13:25,000
I have, so we have something that we wanted to talk about.

206
00:13:25,000 --> 00:13:32,000
If Robin is amenable, maybe I could ask her to say something about our project.

207
00:13:32,000 --> 00:13:33,000
Sure.

208
00:13:33,000 --> 00:13:34,000
Can you still hear me?

209
00:13:34,000 --> 00:13:35,000
Yes, I can.

210
00:13:35,000 --> 00:13:36,000
And about tomorrow.

211
00:13:36,000 --> 00:13:39,000
So if there's anybody who wants to join us tomorrow,

212
00:13:39,000 --> 00:13:41,000
they can, we're at seven o'clock.

213
00:13:41,000 --> 00:13:43,000
Just let them know.

214
00:13:43,000 --> 00:13:47,000
They can contact and I'm allowed them to hang up.

215
00:13:47,000 --> 00:13:48,000
Sure.

216
00:13:48,000 --> 00:13:51,000
So what we're meeting on tomorrow at seven

217
00:13:51,000 --> 00:13:54,000
for anyone who's interested is we're putting together

218
00:13:54,000 --> 00:13:56,000
an online campaign.

219
00:13:56,000 --> 00:14:02,000
Bunte is interested in establishing a monastery up in Canada

220
00:14:02,000 --> 00:14:05,000
in the area of McMaster University,

221
00:14:05,000 --> 00:14:08,000
which is a really exciting project.

222
00:14:08,000 --> 00:14:11,000
It's going to be great for the students at McMaster

223
00:14:11,000 --> 00:14:14,000
to have a Buddhist monastery,

224
00:14:14,000 --> 00:14:16,000
a place to go and learn meditation.

225
00:14:16,000 --> 00:14:18,000
But it's also going to be a benefit to people

226
00:14:18,000 --> 00:14:20,000
who may never make it to Canada.

227
00:14:20,000 --> 00:14:25,000
Although it's not that far, but even for people that are far away,

228
00:14:25,000 --> 00:14:28,000
it's a really interesting concept.

229
00:14:28,000 --> 00:14:33,000
What Bunte does now with broadcasting things that are going on on the internet.

230
00:14:33,000 --> 00:14:39,000
It's just a wonderful idea of having local meditation courses

231
00:14:39,000 --> 00:14:44,000
and local daily Dhamma and broadcasting it as well to us.

232
00:14:44,000 --> 00:14:47,000
So it's really, really exciting and definitely want to support him in that.

233
00:14:47,000 --> 00:14:50,000
So we have a campaign like the type that you see.

234
00:14:50,000 --> 00:14:52,000
It's through a website called YouCaring

235
00:14:52,000 --> 00:14:55,000
and we should have it ready tomorrow to roll out.

236
00:14:55,000 --> 00:14:59,000
And hopefully people will be interested in supporting it

237
00:14:59,000 --> 00:15:03,000
and sharing it with other people who might be interested in supporting it.

238
00:15:03,000 --> 00:15:05,000
Awesome. Thank you.

239
00:15:05,000 --> 00:15:07,000
Yeah. I mean, part of it is having a support group here

240
00:15:07,000 --> 00:15:09,000
to do these sorts of things.

241
00:15:09,000 --> 00:15:11,000
I mean, right now I'm doing everything on my own.

242
00:15:11,000 --> 00:15:14,000
If you see, I just recorded the video for the project here.

243
00:15:14,000 --> 00:15:17,000
There's the camera, right?

244
00:15:17,000 --> 00:15:21,000
And then over there there's a place to...

245
00:15:21,000 --> 00:15:25,000
Anyway, I'm not going to show you my room. It's all missing.

246
00:15:25,000 --> 00:15:28,000
There's a station where that's currently

247
00:15:28,000 --> 00:15:31,000
or just finished transcoding the video.

248
00:15:31,000 --> 00:15:33,000
So it goes from this station to the end.

249
00:15:33,000 --> 00:15:36,000
But I'm doing it all here in this small, small room.

250
00:15:36,000 --> 00:15:41,000
And so the idea is that that probably should change.

251
00:15:41,000 --> 00:15:45,000
So we're going to be starting at the place,

252
00:15:45,000 --> 00:15:54,000
but we need help in organizing and supporting making it happen.

253
00:15:54,000 --> 00:15:56,000
So tomorrow we're going to have a meeting.

254
00:15:56,000 --> 00:15:59,000
And if anyone would like to join the meeting,

255
00:15:59,000 --> 00:16:02,000
it's going to be online.

256
00:16:02,000 --> 00:16:07,000
Just let us know and we'll add you to the Hangout at 7 p.m. Eastern.

257
00:16:07,000 --> 00:16:12,000
And other than that, just wait for the YouTube video to come out.

258
00:16:12,000 --> 00:16:14,000
Shouldn't have it. I mean, it's ready now.

259
00:16:14,000 --> 00:16:16,000
I just have to upload it.

260
00:16:16,000 --> 00:16:19,000
And then there's going to be links and everything there.

261
00:16:19,000 --> 00:16:22,000
So I'm going to get involved.

262
00:16:26,000 --> 00:16:29,000
Actually, maybe we can kind of premiere it here with this group

263
00:16:29,000 --> 00:16:35,000
tomorrow before or at what point will it go live on YouTube?

264
00:16:35,000 --> 00:16:37,000
Tonight. Yeah.

265
00:16:37,000 --> 00:16:40,000
I mean, I can leave it private tonight, right?

266
00:16:40,000 --> 00:16:43,000
Yeah, maybe premiere it here tomorrow with the with the script here.

267
00:16:43,000 --> 00:16:44,000
That'd be nice.

268
00:16:44,000 --> 00:16:45,000
Sure.

269
00:16:59,000 --> 00:17:02,000
Anyway, back to some questions.

270
00:17:02,000 --> 00:17:06,000
When a viewer opinion arises, I find myself verbalizing in the head instead,

271
00:17:06,000 --> 00:17:10,000
along with my noting, if they're a way to not do so.

272
00:17:10,000 --> 00:17:16,000
Well, you can't control. You see, this is the thing that you're learning is that we're not in control exactly.

273
00:17:16,000 --> 00:17:22,000
We can change. We can steer ourselves in a different direction,

274
00:17:22,000 --> 00:17:26,000
but it's got to be moment to moment.

275
00:17:26,000 --> 00:17:30,000
And so for something to just not happen, it's like turning a switch.

276
00:17:30,000 --> 00:17:33,000
It's not, it doesn't work that way.

277
00:17:33,000 --> 00:17:35,000
Right now you're going heading in a,

278
00:17:35,000 --> 00:17:43,000
or you're in a certain bend or a certain direction that includes that sort of verbal

279
00:17:43,000 --> 00:17:46,000
verbalization, mentalization.

280
00:17:46,000 --> 00:17:51,000
But you can steer away from that if you start to cultivate different habits.

281
00:17:51,000 --> 00:17:57,000
You have to identify which habits or which activities are promoting the habit.

282
00:17:57,000 --> 00:18:01,000
And you start to change as you see that those are un,

283
00:18:01,000 --> 00:18:06,000
unbeneficial and they're leading to harmful habits, et cetera, et cetera.

284
00:18:18,000 --> 00:18:24,000
So if a father wants their children to be Catholic and they don't want.

285
00:18:24,000 --> 00:18:27,000
Yeah, I mean, sometimes you might just go along with something, right?

286
00:18:27,000 --> 00:18:31,000
But you don't go along to it to the extent that it means you're lying.

287
00:18:31,000 --> 00:18:34,000
But you just might not say, Father, I'm not a Catholic.

288
00:18:34,000 --> 00:18:37,000
You know, you don't have to tell them that you're not a Catholic.

289
00:18:37,000 --> 00:18:41,000
It doesn't really help him, not likely.

290
00:18:41,000 --> 00:18:44,000
I mean, if you think it could, this is the thing.

291
00:18:44,000 --> 00:18:49,000
But I said, if it's beneficial, the problem is it's hard for us to tell what is truly beneficial.

292
00:18:49,000 --> 00:18:52,000
So that's where uncertainty comes in.

293
00:18:52,000 --> 00:18:55,000
There are no certain answers, but there are certain answers.

294
00:18:55,000 --> 00:18:57,000
We just don't have them.

295
00:18:57,000 --> 00:19:03,000
So it's more about us trying in our muddled way to discern what is right.

296
00:19:03,000 --> 00:19:05,000
It's not just, yes, always.

297
00:19:05,000 --> 00:19:10,000
It'll be much easier if the answer was always tell people the truth.

298
00:19:10,000 --> 00:19:14,000
When the opportunity arises.

299
00:19:14,000 --> 00:19:21,000
But clearly that's not the right way.

300
00:19:21,000 --> 00:19:26,000
So normally normal that labeling things seems more distracting than just noting them,

301
00:19:26,000 --> 00:19:28,000
noticing them, but not picking word.

302
00:19:28,000 --> 00:19:29,000
Yeah.

303
00:19:29,000 --> 00:19:33,000
It's not distracting, but it's disturbing and it's meant to be.

304
00:19:33,000 --> 00:19:37,000
It's meant to disturb your habitual stream.

305
00:19:37,000 --> 00:19:40,000
You know, their habitual reactions.

306
00:19:40,000 --> 00:19:43,000
So we have this, we're comfortable in the way that we deal with things.

307
00:19:43,000 --> 00:19:48,000
But that being comfortable, that comfortable involves defilements.

308
00:19:48,000 --> 00:19:51,000
And so we're trying to disrupt it.

309
00:19:51,000 --> 00:19:56,000
The other thing is it's showing you how chaotic things really are.

310
00:19:56,000 --> 00:20:03,000
So we have a way of surfing over the waves, the disturbances.

311
00:20:03,000 --> 00:20:09,000
Sort of just superficial investigation, keeping it superficial enough so that it doesn't bother us.

312
00:20:09,000 --> 00:20:13,000
This meditation is bringing you right in there.

313
00:20:13,000 --> 00:20:15,000
And that's not easy.

314
00:20:15,000 --> 00:20:19,000
And so as a result, you're seeing how difficult it is and how chaotic it is.

315
00:20:19,000 --> 00:20:28,000
The third is that it's just a part of the beginner stages in the practice.

316
00:20:28,000 --> 00:20:37,000
For an advanced meditator, they're much better able to have a greater than superficial involvement with reality.

317
00:20:37,000 --> 00:20:40,000
And so as a result, it's not so disturbing.

318
00:20:40,000 --> 00:20:49,000
But in the beginning, you're doing it a lot wrong or it's clumsy, it's unskillful.

319
00:20:49,000 --> 00:20:55,000
And so yeah, as a result, it's going to be chaotic.

320
00:20:55,000 --> 00:20:57,000
But no, it's not actually distracting.

321
00:20:57,000 --> 00:21:04,000
That maybe how it appears because you have this idea that it's a better meditation somehow to just go with it.

322
00:21:04,000 --> 00:21:06,000
But it's actually not a disturbing.

323
00:21:06,000 --> 00:21:07,000
It's not distracting.

324
00:21:07,000 --> 00:21:09,000
It's just disturbing.

325
00:21:09,000 --> 00:21:11,000
And it's disturbing for these reasons.

326
00:21:11,000 --> 00:21:13,000
First of all, reality is kind of disturbing.

327
00:21:13,000 --> 00:21:18,000
Second of all, you're not in the beginning very good at approaching reality.

328
00:21:18,000 --> 00:21:23,000
And third, it's a part of the actual practice to be disturbed.

329
00:21:23,000 --> 00:21:29,000
Because the superficial ability to surf over the experiences,

330
00:21:29,000 --> 00:21:35,000
belies the fact that it's actually not a way to find happiness.

331
00:21:35,000 --> 00:21:41,000
That this is not sustainable, it's not conducive to true and lasting happiness.

332
00:21:41,000 --> 00:21:48,000
In truth, once you see reality clearly, you reject it entirely and you leave it behind.

333
00:21:48,000 --> 00:21:56,000
And in favor of freedom, you leave behind everything step by step in your life.

334
00:21:56,000 --> 00:22:00,000
You start letting go of the more complicated aspects of your life,

335
00:22:00,000 --> 00:22:10,000
identifying until eventually you just let go of everything and have no interest in it being involved at all.

336
00:22:10,000 --> 00:22:16,000
So some questions about this project that we kind of vaguely mentioned.

337
00:22:16,000 --> 00:22:20,000
I mean, I have to be a little vague because I can't say, but we need support.

338
00:22:20,000 --> 00:22:22,000
To make this happen, we need people to help.

339
00:22:22,000 --> 00:22:25,000
So if you want to help organize, there's a meeting.

340
00:22:25,000 --> 00:22:29,000
But we're going to set up something on a crowdsourcing site.

341
00:22:29,000 --> 00:22:31,000
So how many of you want to explain?

342
00:22:31,000 --> 00:22:32,000
Sure.

343
00:22:32,000 --> 00:22:34,000
It's a nice site called U-Caring.

344
00:22:34,000 --> 00:22:39,000
And there's a ton of crowdsourcing, crowdsourced funding sites.

345
00:22:39,000 --> 00:22:45,000
But we're able to find one that doesn't charge a percentage other than the PayPal percentage,

346
00:22:45,000 --> 00:22:48,000
which everyone has to charge. That's a PayPal policy.

347
00:22:48,000 --> 00:22:57,000
So in other words, Bontay's organization will receive the maximum amount of donations that are given to support this project.

348
00:22:57,000 --> 00:23:07,000
So we have the amount that we are hoping to raise, which is initially $6,000 to pay for some very basic expenses.

349
00:23:07,000 --> 00:23:12,000
The first and last months are into utilities and just real basic things like that.

350
00:23:12,000 --> 00:23:15,000
So that will be available tomorrow.

351
00:23:15,000 --> 00:23:17,000
And it will be on YouTube.

352
00:23:17,000 --> 00:23:24,000
And, you know, the basic kind of thing that you can share on your Facebook if you so choose or if you know particular people that you know might be interested.

353
00:23:24,000 --> 00:23:32,000
You know, please share it with them. They can be shared by email, many different methods of just getting the word out and be a great way to support.

354
00:23:32,000 --> 00:23:38,000
And as far as the seven o'clock meeting tomorrow, Bontay, a couple of people are asking where to go.

355
00:23:38,000 --> 00:23:40,000
That's actually on Google Hangouts, right?

356
00:23:40,000 --> 00:23:41,000
Yeah, you have to get in light.

357
00:23:41,000 --> 00:23:51,000
So you have to just send us an email or a message and send one of us a message and that's an organizational meeting.

358
00:23:51,000 --> 00:23:55,000
It's only for if you want to be involved in organizing.

359
00:23:55,000 --> 00:24:09,000
And we don't need, I suppose, we're going to need a few people involved in spreading the word and coming brainstorming and working out logistics and so on.

360
00:24:09,000 --> 00:24:15,000
We have to find a property and we've been looking at rental properties and have to find the right one.

361
00:24:15,000 --> 00:24:17,000
We have to go take a look at it.

362
00:24:17,000 --> 00:24:25,000
Hopefully when I go to McMaster, we're going to find a community there who some people might be able to help out with these sorts of things.

363
00:24:25,000 --> 00:24:34,000
I just put my email in the chat window if anyone is interested in helping out and being a volunteer or, you know, even just showing up just to kind of give some feedback.

364
00:24:34,000 --> 00:24:40,000
We'll be looking at the video and everything for the first time together and just making sure we didn't overlook anything.

365
00:24:40,000 --> 00:24:51,000
So if anyone's interested in attending or helping out, just send me an email and I'll make sure Bante has that information.

366
00:24:51,000 --> 00:24:53,000
Okay, another question.

367
00:24:53,000 --> 00:24:59,000
Is Buddhism or religion that focuses on worship towards Buddha rather than your own well-being?

368
00:24:59,000 --> 00:25:02,000
I would say Jacob, no.

369
00:25:02,000 --> 00:25:05,000
Simon says Jacob, no.

370
00:25:05,000 --> 00:25:13,000
Absolutely. When pretty much the opposite, right, it's much more about your own well-being and very little about any sort of worship of any kind.

371
00:25:13,000 --> 00:25:25,000
I mean, we do kind of worship the Buddha in the sense that we think he is someone worthy of our reverence, but it's more like reverence than worship.

372
00:25:25,000 --> 00:25:42,000
The common word worship, I think, has become too much of sort of devoted towards the simple ritual as a God or something, but it wasn't certainly a God.

373
00:25:42,000 --> 00:25:54,000
What would be your specific qualifications for our donation? Rule number one, don't ask me about our donation. Qualification number one, stop asking about our donation.

374
00:25:54,000 --> 00:26:07,000
And that's not really each tongue tongue in cheek because, I mean, it is, but it isn't because anyone who comes to me looking to ordain at this point, it's just a red flag.

375
00:26:07,000 --> 00:26:17,000
I'm sorry, and that sounds probably pretty bad. And it's probably in your case, not true, which is why I feel comfortable saying it because focus on meditation.

376
00:26:17,000 --> 00:26:24,000
And I'm not chastising you or anything. I'm saying, you want it to happen. You're that serious.

377
00:26:24,000 --> 00:26:32,000
Then it has to be, I'm so serious about meditation. I want to put on some rectangle of cloth and keep doing it. It has to be that.

378
00:26:32,000 --> 00:26:42,000
And it can't be about, I want to become a monk because I think it's cool to wear a toga.

379
00:26:42,000 --> 00:26:53,000
Right. So think of it like that, that 80s Kung Fu movie that I always referenced, where he sat in the courtyard.

380
00:26:53,000 --> 00:27:05,000
And they wouldn't let him in. They said, no, no, a fight club as well. One of the last movies I saw before I came a monk fight club where they make you stand outside.

381
00:27:05,000 --> 00:27:17,000
But there was an earlier movie this Shaolin movie where they made he sat outside and they said, no, no, we don't accept foreigners and they just ignored him and sat in front of the monastery until they let him in.

382
00:27:17,000 --> 00:27:24,000
And of course he ended up causing a ruckus and big upset in the monastery.

383
00:27:24,000 --> 00:27:38,000
So yeah, there's that. But let's say you have to approach it. You want, you come and do meditation course and then we'll talk about whether you can do a second meditation course.

384
00:27:38,000 --> 00:27:48,000
If you've done that, then we can talk about, well, maybe a third meditation course. Let me kind of get the point and get the picture.

385
00:27:48,000 --> 00:28:15,000
Because, you know, for that you stay, then you can stay.

386
00:28:18,000 --> 00:28:38,000
I think you've answered all the questions, Dante.

387
00:28:38,000 --> 00:28:51,000
That's what I was thinking. Yeah, there were like 200 questions piled up on Google moderator. So this is nice.

388
00:28:51,000 --> 00:28:54,000
All caught up.

389
00:28:54,000 --> 00:28:59,000
Awesome. Thank you, Robin, for being there to help. Oh, thank you.

390
00:28:59,000 --> 00:29:03,000
All right, let's call it a night. See you all tomorrow.

391
00:29:03,000 --> 00:29:08,000
Thank you, Dante.

