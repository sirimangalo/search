1
00:00:00,000 --> 00:00:17,760
Good evening everyone, welcome to our live broadcast and evening down the talk.

2
00:00:17,760 --> 00:00:29,000
Today we're looking at training and we're going to go through the entire book of three

3
00:00:29,000 --> 00:00:40,720
is set to 81 and onward.

4
00:00:40,720 --> 00:00:41,720
Another list of three.

5
00:00:41,720 --> 00:00:56,520
This is the list of three things that we have to do, three tasks to be accomplished.

6
00:00:56,520 --> 00:01:20,680
Three things by a shaman, a reckless, that's what we all are.

7
00:01:20,680 --> 00:01:30,520
We are wizards like shamans who have left the world to seek out higher things.

8
00:01:30,520 --> 00:01:36,920
The things that we accomplish in meditation most people would think of is wizardry if

9
00:01:36,920 --> 00:01:38,920
they knew what we were capable of.

10
00:01:38,920 --> 00:01:44,680
I'm not talking about magical powers although there are incredible magical powers that come

11
00:01:44,680 --> 00:01:51,280
from meditation, but the simple ability to bear with things that most people aren't able

12
00:01:51,280 --> 00:01:52,600
to bear with.

13
00:01:52,600 --> 00:02:00,320
It's not simple things that we do, it's not easy things in there, it's not mundane

14
00:02:00,320 --> 00:02:01,320
things.

15
00:02:01,320 --> 00:02:11,440
The powerful spiritual achievement through the practice, the ability to let go, the

16
00:02:11,440 --> 00:02:20,800
ability to tame the mind, to discard all the garbage in the mind and have a pure and clear

17
00:02:20,800 --> 00:02:21,800
mind.

18
00:02:21,800 --> 00:02:33,200
It's very profound thing, so we are all samana, we are all shamans here.

19
00:02:33,200 --> 00:02:38,160
What are the three things that a shaman, a shaman should accomplish?

20
00:02:38,160 --> 00:02:45,000
Well, in Buddhism, all that magical stuff, all the things that we normally associate

21
00:02:45,000 --> 00:02:51,120
with a shaman in modern times, or even in ancient times.

22
00:02:51,120 --> 00:03:03,400
Most of it's not important in Buddhism, but it's some sticks to the core, separates

23
00:03:03,400 --> 00:03:13,160
the wheat from the chaff, so much as just chaff, you might think it looks good and might

24
00:03:13,160 --> 00:03:23,200
think it's useful, but when you investigate, you find that there's not much to it, not

25
00:03:23,200 --> 00:03:43,880
much benefit to be had from it, and the end doesn't give you anything special.

26
00:03:43,880 --> 00:03:48,120
So what are the three things that we have to accomplish, samana, karini, anything, sing

27
00:03:48,120 --> 00:03:54,640
three things that must be done by a shaman?

28
00:03:54,640 --> 00:04:08,960
It's quite simple, many of you must know that these are adi cila, sika, samana, samana.

29
00:04:08,960 --> 00:04:21,960
Samana means taking upon yourself to take upon yourself the practice of higher

30
00:04:21,960 --> 00:04:31,680
sila, higher morality, higher ethical behavior.

31
00:04:31,680 --> 00:04:40,280
Number two, adi chita, sika, sika, samana, taking upon yourself the training and higher

32
00:04:40,280 --> 00:04:51,120
mind, higher mind states, concentration, seriously translated, explained that.

33
00:04:51,120 --> 00:04:57,880
And number three, adipanya, sika, samana, the training, taking upon yourself the training

34
00:04:57,880 --> 00:05:13,640
and the higher wisdom, morality, concentration, and wisdom, so it's usually described.

35
00:05:13,640 --> 00:05:16,280
How do we understand these things?

36
00:05:16,280 --> 00:05:23,200
Well, it's important to be able to distinguish between morality, concentration, and wisdom

37
00:05:23,200 --> 00:05:33,480
and higher morality, higher concentration, and higher wisdom, because it's of two kinds,

38
00:05:33,480 --> 00:05:40,280
there's ordinary morality and then there's higher morality, ordinary ethics, ordinary ethics

39
00:05:40,280 --> 00:05:49,440
is keeping rules, they all love rules, no, some of us do.

40
00:05:49,440 --> 00:05:57,000
So easy, easy to keep, I think it's hard to keep rules, relatively easy, monks can keep

41
00:05:57,000 --> 00:06:06,680
lots of rules, they keep rules, some people think because they keep rules, that means they're

42
00:06:06,680 --> 00:06:15,520
practicing Buddhism, they're practicing mundane Buddhism, sure, but it's actually not enough

43
00:06:15,520 --> 00:06:21,160
unfortunately, to just keep rules, rules are like fence posts, they can't keep the cows

44
00:06:21,160 --> 00:06:29,080
in, you need a fence for that, you need something real, fence posts are useful, they

45
00:06:29,080 --> 00:06:34,960
keep the fence from falling down, rules are useful, they keep your practice from falling

46
00:06:34,960 --> 00:06:42,160
down, but you need the fence, they're no less, and it's adisela, adisela is not just

47
00:06:42,160 --> 00:06:48,880
keeping rules, adisela is cultivating a mind that is free from the inclination to break

48
00:06:48,880 --> 00:06:57,920
rules, to do anything or more, it's guarding the mind, bringing your mind back, and meditation

49
00:06:57,920 --> 00:07:04,240
the simplest way to understand morality or ethics, it's not letting your mind wander,

50
00:07:04,240 --> 00:07:10,440
not stopping it, but the act of bringing your mind back, when you realize you've been

51
00:07:10,440 --> 00:07:20,280
thinking to remind yourself, thinking, thinking, the simple reminder, the initiate initiative

52
00:07:20,280 --> 00:07:25,880
to say to yourself, ah, I've been unmindful, and to bring yourself back, to cultivate

53
00:07:25,880 --> 00:07:34,040
a habit of focusing the mind, because these lead one to the other, what truly leads

54
00:07:34,040 --> 00:07:44,360
to higher mind states, it's clear mind states, is this act of guarding your mind, catching

55
00:07:44,360 --> 00:07:51,920
your mind, bringing your mind back to focus on the present moment, focus on reality,

56
00:07:51,920 --> 00:07:59,320
when you caught up in the conceptualization of an experience, suppose you have a thought

57
00:07:59,320 --> 00:08:07,640
and it makes you get lost in the world of the past, or the future, or imagination, to

58
00:08:07,640 --> 00:08:12,280
come back down to reality and to remind yourself, hey, that's a thought, that's images

59
00:08:12,280 --> 00:08:19,320
in my mind, it's just seeing, or if someone's yelling at you and you start to plan how

60
00:08:19,320 --> 00:08:24,840
you're going to get back to them, and you remind yourself that that's just sound, and suddenly

61
00:08:24,840 --> 00:08:31,640
you're back in reality, that act of bringing yourself back, that's the purest, simplest

62
00:08:31,640 --> 00:08:38,800
way of understanding ethics, not letting your mind wander, not freeing your mind to let

63
00:08:38,800 --> 00:08:46,040
it follow after its inclinations and habits, but instead training the mind to see that

64
00:08:46,040 --> 00:08:54,400
the facet of the experience that is real, the kernel of truth, that is in every experience,

65
00:08:54,400 --> 00:09:02,240
what's really happening, did I get lost in what's not really happening?

66
00:09:02,240 --> 00:09:12,120
Adijita Sika, again, two kinds, an ordinary, ordinary jita, ordinary concentration, an

67
00:09:12,120 --> 00:09:17,320
ordinary focusing of mind states, this is how we normally think of as, think of concentration

68
00:09:17,320 --> 00:09:28,840
as we can fix your mind in some way, where your mind is just shielded from the world somehow,

69
00:09:28,840 --> 00:09:38,400
shielded from unpleasantness, where your mind is fixed and focused on one thing, in one

70
00:09:38,400 --> 00:09:48,240
way, and that can't help you either, that too is not really the way to enlightenment, there

71
00:09:48,240 --> 00:09:53,520
are people in other religions who fix and focus their minds on things, and don't ever reach

72
00:09:53,520 --> 00:10:03,040
enlightenment, how many lifetimes the body is at the practice, focusing his mind, calming

73
00:10:03,040 --> 00:10:12,800
his mind and never became enlightened, until he was finally ready and he realized, by

74
00:10:12,800 --> 00:10:24,640
watching, by looking at reality instead, and seeing cause and effect, to see cause and effect,

75
00:10:24,640 --> 00:10:29,400
you have to focus on what's real, you have to focus on things that change, you have to

76
00:10:29,400 --> 00:10:37,200
focus on the kernel of truth, this kernel of experience that is real, because that's where

77
00:10:37,200 --> 00:10:45,640
cause and effect lives, that's where the only way you can truly see, good causes, good

78
00:10:45,640 --> 00:10:55,360
and bad causes, bad, clinging causes, suffering, ignorance causes clinging, you can give

79
00:10:55,360 --> 00:11:03,760
up ignorance, because you can see this clearly and no longer clean, so this comes in meditation

80
00:11:03,760 --> 00:11:09,640
when you watch reality, it's not as comfortable of course, but there's a focus to it,

81
00:11:09,640 --> 00:11:16,720
there's a clarity to it, to being able to move from one object to the other, stay with

82
00:11:16,720 --> 00:11:24,640
each object as it changes, as the object changes to keep the mind the same, this is what

83
00:11:24,640 --> 00:11:29,760
we're training ourselves for, to slowly be able to experience everything with the same

84
00:11:29,760 --> 00:11:36,600
clarity of mind, to slowly be able to see everything as it is, rather than judging it reacting

85
00:11:36,600 --> 00:11:46,040
to it, we slowly give up our reactions, our judgments, our attachments, our versions, that's

86
00:11:46,040 --> 00:11:56,160
a deep idea, a deep idea that then leads to this clarity of mind where you see things

87
00:11:56,160 --> 00:12:00,600
as they are, you see things clearly what allows you to give rise to wisdom, again they're

88
00:12:00,600 --> 00:12:07,320
two kinds of wisdom, so ordinary wisdom is where you listen to a talk like this and you

89
00:12:07,320 --> 00:12:13,120
learn something, so you learn what are the three training, where you learn what are the

90
00:12:13,120 --> 00:12:18,960
two levels of the three training, you can have ordinary training, you can have higher

91
00:12:18,960 --> 00:12:27,440
training, but we often think that that's enough, that kind of wisdom is enough, our teaching

92
00:12:27,440 --> 00:12:32,520
has to include a reminder that true wisdom doesn't come from listening, it doesn't

93
00:12:32,520 --> 00:12:41,200
come from thinking, come from intellectual thought or rationalization or logic or whatever,

94
00:12:41,200 --> 00:12:47,520
the wisdom comes from seeing things as they are, as you watch things arising and ceasing

95
00:12:47,520 --> 00:12:53,040
this is where you see cause and effect, more importantly you see the nature of things,

96
00:12:53,040 --> 00:12:56,800
but everything that arises ceases, you can teach them what they are done among some

97
00:12:56,800 --> 00:13:01,680
but don't need all the dumb ones, it's really the most important thing, it sounds silly,

98
00:13:01,680 --> 00:13:07,680
it sounds too overly simplistic, but when you finally get that truth, not intellectually

99
00:13:07,680 --> 00:13:12,760
but only because you've seen it again and again, and you finally realize that the truth

100
00:13:12,760 --> 00:13:21,720
of reality is nothing more than everything arises in ceases, everything that arises ceases,

101
00:13:21,720 --> 00:13:26,000
because that allows you to let go of it, there's no clinging to something that arises

102
00:13:26,000 --> 00:13:30,200
in ceases, there's only clinging to things that you think are stable, that you're

103
00:13:30,200 --> 00:13:43,160
saving correctly as being consistent, predictable, stable and secure, and thereby satisfying

104
00:13:43,160 --> 00:13:50,600
and controllable, but if things arise in cease moment after moment incessantly, they

105
00:13:50,600 --> 00:13:55,640
can't be satisfying, they can't be predictable, they can't be controllable, they come

106
00:13:55,640 --> 00:14:04,960
and go on their own, we see so much more as the problem, but near really is a limiting

107
00:14:04,960 --> 00:14:10,600
of what we see in things, normally when we see something we judge it immediately, we see

108
00:14:10,600 --> 00:14:17,840
so much in things too much, so we react to them, this is good, this is bad, this is

109
00:14:17,840 --> 00:14:25,720
mean, this is mine, this is right, this is wrong, we fail to see that simple reality, it's

110
00:14:25,720 --> 00:14:31,240
none of those things, it's just arising in ceasing, but when that's all you see about

111
00:14:31,240 --> 00:14:39,440
things, then you let go, or when you let go, that's what you'll see about things, you

112
00:14:39,440 --> 00:14:45,320
start to see that all this is wrong, all this judging and clinging and reacting, so much

113
00:14:45,320 --> 00:14:51,080
suffering, and you see it again and again and again until you finally had enough, and your

114
00:14:51,080 --> 00:15:06,200
mind will start to let go, loosen up, free itself from the clinging and suffering, so

115
00:15:06,200 --> 00:15:10,000
there's some more suit that's about these things, but I don't think that that's important,

116
00:15:10,000 --> 00:15:15,320
but one about the donkey, if you're interested in it, and recommend reading the donkey

117
00:15:15,320 --> 00:15:26,440
one, but it's not really important as a minute here, the feeling is an interesting

118
00:15:26,440 --> 00:15:36,080
one, interesting similes, a farmer, a farmer who is planting, planting a seed needs to do

119
00:15:36,080 --> 00:15:41,440
three things, first they have to plow and narrow the field, then they have to sow their

120
00:15:41,440 --> 00:15:51,400
seeds at the proper time, then they have to irrigate and drain the field, so what does

121
00:15:51,400 --> 00:15:57,320
a monk have to do, what does a meditator have to do, well that do the same, but what does

122
00:15:57,320 --> 00:16:03,960
it mean to plow in the field when that's morality, I think, because you can't focus

123
00:16:03,960 --> 00:16:09,840
the mind unless you're ethical first, and ultimately this means practicing, walking

124
00:16:09,840 --> 00:16:16,640
and sitting, and then you sow your seeds by being mindful during the time when you're

125
00:16:16,640 --> 00:16:24,800
walking and sitting, guarding your mind, and then the irrigation is the wisdom.

126
00:16:24,800 --> 00:16:35,760
Anyway, most important we remember, there's only these three trainings, we guard the

127
00:16:35,760 --> 00:16:44,760
mind and then focus the mind on reality and start to see things just as they are, don't

128
00:16:44,760 --> 00:16:50,360
think too much about things, don't see too much in them, see things clearly and fully

129
00:16:50,360 --> 00:17:00,840
as they are, see the kernel tree, see the Samandipanya, this is the higher training of the Buddha,

130
00:17:00,840 --> 00:17:14,240
quite simple, and quite powerful, so it's a demo for tonight, let's go to questions Robin

131
00:17:14,240 --> 00:17:32,240
I don't think it's here unless she's snuck in, she had something with her daughter, okay,

132
00:17:32,240 --> 00:17:41,200
questions, would you expand on the meaning of delusion?

133
00:17:41,200 --> 00:17:49,440
It means to be confused, in a sense, muddled, deluded is a really good, delusion is a good

134
00:17:49,440 --> 00:17:59,480
translation, but moha, it's like a cloud, it's a word that moh means to be muddled, confused,

135
00:17:59,480 --> 00:18:05,760
so it encompasses a lot of different things like confusion, anything that's based simply

136
00:18:05,760 --> 00:18:16,840
on delusion, right, misunderstanding of the world, a misapprehension of things, grasping

137
00:18:16,840 --> 00:18:22,160
things wrongly, so any kind of arrogance, anything to do with self, with ego is all

138
00:18:22,160 --> 00:18:33,080
delusion, arrogance can see, but also confusion and worry, these are all delusion, but these

139
00:18:33,080 --> 00:18:37,120
are pure delusion states, the other thing you have to understand about delusion is that

140
00:18:37,120 --> 00:18:42,240
it's also present in greed and anger, because greed and anger are based on delusion as well,

141
00:18:42,240 --> 00:18:47,320
they're based on a misapprehension of reality, so that's what delusion means, delusion

142
00:18:47,320 --> 00:18:52,000
is really the root, it's not, it's the opposite of wisdom, the opposite of seeing things

143
00:18:52,000 --> 00:18:57,840
clearly as they are, but the pure delusion states are those that are free from greed or anger,

144
00:18:57,840 --> 00:19:05,240
those moments when you're arrogant or conceited or worried or even distracted or confused

145
00:19:05,240 --> 00:19:18,680
doubt, doubt is a big delusion, he has a person who sees clearly has no doubt, will I ever

146
00:19:18,680 --> 00:19:24,800
do a TED talk, I was on my way to do a TED talk, I was in the semi, I was a semi-finalist

147
00:19:24,800 --> 00:19:31,000
to do a TED talk, TED talk, TED talk you have to be famous for that, but this TED

148
00:19:31,000 --> 00:19:36,040
talk and I backed out because it didn't feel right, it was not really about meditation

149
00:19:36,040 --> 00:19:40,800
and I just thought, here I was reading something I had written and it just felt kind

150
00:19:40,800 --> 00:19:49,280
of wrong, I like this better, teaching meditators, same hair is a good source for learning

151
00:19:49,280 --> 00:19:59,520
meditation, yeah I don't know about that, I wouldn't learn meditation personally from him

152
00:19:59,520 --> 00:20:04,800
but that's just me, and again it's sort of outside of my realm, I don't like talking

153
00:20:04,800 --> 00:20:13,280
about other teachers, other groups, other peoples, our lucid dreams, more wholesome than

154
00:20:13,280 --> 00:20:19,760
non lucid dreams, seems to me that you can manage to be mindful if you know you're dreaming

155
00:20:19,760 --> 00:20:25,480
potentially, I would say there's probably certain qualities missing to your lucidity

156
00:20:25,480 --> 00:20:29,680
just because you know something doesn't mean you can be mindful of it, because the word

157
00:20:29,680 --> 00:20:35,440
mindful is misleading, it's really shouldn't be translated as mindful, but when we talk

158
00:20:35,440 --> 00:20:39,160
about set date and in terms of the setty patan that we're talking about something very

159
00:20:39,160 --> 00:20:45,680
specific, we're able to remind yourself this is that seeing is just seeing, I'm not

160
00:20:45,680 --> 00:20:51,040
sure if that's possible during lucid dreaming, I mean it's not possible most of the time

161
00:20:51,040 --> 00:21:02,040
when we're awake right, but no but during dreaming there's potentially something missing

162
00:21:02,040 --> 00:21:09,880
because you don't really have control over the mind in the same way, not exactly control

163
00:21:09,880 --> 00:21:18,520
but sort of the ability to remind yourself, I don't know, I mean it's all very speculative,

164
00:21:18,520 --> 00:21:23,720
I would stick to worrying about meditating when you're awake and Jantong said I'm enlightened

165
00:21:23,720 --> 00:21:30,800
person doesn't dream, so you can go by that and Arhan doesn't dream, of course the Buddha

166
00:21:30,800 --> 00:21:36,560
had dreams, but those are I think special dreams for the bodhisattva, the Buddha and the

167
00:21:36,560 --> 00:21:46,360
bodhisattva, remember, maybe the Buddha didn't have dreams, is there a way to change the

168
00:21:46,360 --> 00:21:52,160
email address that is linked to our account, I think they're supposed to be, I'm not

169
00:21:52,160 --> 00:21:57,200
sure if there is yet, but you might want to look at the issues on GitHub and go in some

170
00:21:57,200 --> 00:22:06,000
minute issue if there is someone, cockroaches and insects, when I'm awake, when I flick off

171
00:22:06,000 --> 00:22:11,400
the light, I'm trying to sleep, I can feel them crawling all over me, I know how that is,

172
00:22:11,400 --> 00:22:16,640
I remember waking up in Thailand once, when there was a cockroach on my lips and it was

173
00:22:16,640 --> 00:22:27,320
a very hardy those things, they don't worry about hurting them, makes sleeping difficult,

174
00:22:27,320 --> 00:22:33,880
we'll get a net, the best way is to get one of those mosquito nets, I would say, sleeping

175
00:22:33,880 --> 00:22:46,320
in the mosquito tent, otherwise just live with it, good reason to sleep off the floor,

176
00:22:46,320 --> 00:22:51,440
I suppose, there's nothing wrong with sleeping off the floor, if you sleep on a bed,

177
00:22:51,440 --> 00:22:56,280
just make sure it's, if you get one of those wooden beds, it's just wood, you know, and

178
00:22:56,280 --> 00:23:01,360
then just let the man on top of that, it's not really about the height of it, it's more

179
00:23:01,360 --> 00:23:07,040
about the softness and the luxury of it, but there is something sleeping on the floor

180
00:23:07,040 --> 00:23:21,280
that just makes you feel kind of humble, I suppose, or simple, I'm just considering

181
00:23:21,280 --> 00:23:28,560
becoming a monk and vice, should I further my education, would you suggest before continuing

182
00:23:28,560 --> 00:23:33,360
on to going to Sri Lanka? No, if you want to ordain it's a monk, become a monk, but

183
00:23:33,360 --> 00:23:37,920
you got to be ready, you got to really want, you say I'm considering becoming a monk,

184
00:23:37,920 --> 00:23:43,880
I'd say forget about it, if that's how you phrase it, forget about it, if you're not

185
00:23:43,880 --> 00:23:50,480
like jumping off your seat, chomp, chomping at the bit, to become a monk, your chances

186
00:23:50,480 --> 00:23:57,960
of surviving as a monk in modern times, very slim, you got to want it, and you got to be

187
00:23:57,960 --> 00:24:04,680
sure that it's for you, I was sure, don't mean to brag, but it's not, it's not really bragging

188
00:24:04,680 --> 00:24:10,760
because it's, you see there's this class of monks who ordain and, you know, they're just

189
00:24:10,760 --> 00:24:14,960
sure that's what they want, doesn't mean they're good monks, but the means they can stay

190
00:24:14,960 --> 00:24:22,240
monks, and then you see this other class are kind of like, well, you know, maybe, and

191
00:24:22,240 --> 00:24:27,920
then they become a monk and they don't last, they just, they don't have it, it's not

192
00:24:27,920 --> 00:24:32,040
good, becoming a monk is not like a decision you make, it's something you have to, it has

193
00:24:32,040 --> 00:24:36,920
to be a part of you, really, it's a very special thing, becoming a Buddhist monk is not

194
00:24:36,920 --> 00:24:43,600
something to look lightly upon, if you don't, you're not 100% ready, don't do it, well,

195
00:24:43,600 --> 00:24:48,880
you're not 100% ready, chances are if you do do it, you won't last, that's all, it doesn't

196
00:24:48,880 --> 00:24:57,880
mean don't do it, you can go for it, but your chances of lasting are still, okay,

197
00:24:57,880 --> 00:25:04,160
what suit does the donkey one, yeah, well you got to read it for yourself, I didn't want

198
00:25:04,160 --> 00:25:08,360
to bother my meditations with too much, the donkey is more about monks, you know, there

199
00:25:08,360 --> 00:25:13,440
are monks who, who pretend to be monks, there are donkeys who pretend to be horses, so

200
00:25:13,440 --> 00:25:19,880
suppose there was a donkey following after the horses saying, I'm a horse, I'm a horse,

201
00:25:19,880 --> 00:25:30,600
but his body doesn't look like a horse, his brain doesn't sound like a horse, and his

202
00:25:30,600 --> 00:25:35,400
hoofs don't look like, his footprints don't look like horses, and the same goes with

203
00:25:35,400 --> 00:25:40,160
a monk, a beaucoup, a beaucoup doesn't have silas, samadipanya, they're just a donkey

204
00:25:40,160 --> 00:25:51,400
following after horses, because silas your physical appearance, samadhi is your sound, your

205
00:25:51,400 --> 00:26:04,200
voice, and it's just an analogy, and wisdom is your footprint, are you happy, yeah, it's

206
00:26:04,200 --> 00:26:08,160
a funny question really, because I don't exist, happiness is something that arises in

207
00:26:08,160 --> 00:26:19,480
CSS, but I don't answer questions about myself, sorry, we have rules as monks, someone

208
00:26:19,480 --> 00:26:25,120
can't be happy, you know, you can't be a happy person in general, you're asking me at that

209
00:26:25,120 --> 00:26:30,000
moment was I happy, you know what, that's something I suppose I could answer in this moment

210
00:26:30,000 --> 00:26:39,040
am I happy, but moments of rise and see is happiness, you know honestly the only true happiness

211
00:26:39,040 --> 00:26:47,720
and that's really valuable is nibanda, so you can only really say a happy person, someone

212
00:26:47,720 --> 00:26:57,080
who had attained nibanda, either they're in nibanda, there's the cessation of suffering,

213
00:26:57,080 --> 00:27:05,080
or else they've attained it and they're there for enlightenment, or an aran, or a buddha, happy

214
00:27:05,080 --> 00:27:19,240
we are among the unhappy buddha said, it's an aran, and the buddha said the head dreams

215
00:27:19,240 --> 00:27:38,680
thanks and care, that's all the questions for tonight, 25 people on our site, we're getting

216
00:27:38,680 --> 00:27:47,200
up there, 42 viewers on YouTube, that's average, it was great to have people watching, I was

217
00:27:47,200 --> 00:27:55,680
just looking at an article about virtual reality, conferencing, they're getting there,

218
00:27:55,680 --> 00:27:59,120
they're getting where you can actually get in a room with each other and like wave your

219
00:27:59,120 --> 00:28:05,840
arms I think, I didn't, I don't think I brought guests in this, but when I was in Thailand

220
00:28:05,840 --> 00:28:11,520
I should have taken a picture, my uncle's, my uncle's a big, or not no one's as a partner

221
00:28:11,520 --> 00:28:21,240
in one of the bigger film companies in Asia, in Thailand anyway, they do a lot of Hollywood

222
00:28:21,240 --> 00:28:31,520
films that happen in Asia, and they got virtual reality, they're working on this idea of

223
00:28:31,520 --> 00:28:36,880
virtual reality as the future, so I got to try it out, and I was meditating up on a cliff

224
00:28:36,880 --> 00:28:42,840
face, it's really neat, because I was thinking, you know, couldn't you do some kind of

225
00:28:42,840 --> 00:28:47,440
meditation teaching where you go up on the cliff and there's the monk, I am, but there's

226
00:28:47,440 --> 00:28:52,560
the teacher sitting on the cliff face and they start to teach you meditation, I mean for

227
00:28:52,560 --> 00:28:58,000
a beginner meditator, how I think I'm attractive that would be, if you've ever used virtual

228
00:28:58,000 --> 00:29:05,720
reality, that was my first virtual reality experience, it really is kind of immersive, it was

229
00:29:05,720 --> 00:29:10,800
funny because the air conditioning was on in the room and it felt like there was a breeze

230
00:29:10,800 --> 00:29:19,680
blowing, but you know, it'd be interesting if we could have, no, I don't know, this is

231
00:29:19,680 --> 00:29:23,920
good enough, let's not get too complicated, it's kind of weird with that, you got this big

232
00:29:23,920 --> 00:29:30,840
thing on your head, but it was nice setting up on a cliff, I got to sit up on this cliff,

233
00:29:30,840 --> 00:29:42,320
close my eyes and meditate, okay, anyway, this is good enough, we're all here, supporting

234
00:29:42,320 --> 00:29:45,160
each other in our meditation, the funny thing is, you know, when you meditate, you're

235
00:29:45,160 --> 00:29:51,080
all alone anyway, you don't meditate actually with others, and you shouldn't, meditation

236
00:29:51,080 --> 00:30:01,360
should be very much about you, I got one more question popped up, I'll answer one more

237
00:30:01,360 --> 00:30:06,800
and then we're done, during meditation is there a problem in the pace of noting it's

238
00:30:06,800 --> 00:30:14,760
inconsistent, mostly too fast, but sometimes very slow, if it appears too fast, it's something

239
00:30:14,760 --> 00:30:20,640
you should don't knowing, knowing, knowing maybe better, because it shouldn't appear too

240
00:30:20,640 --> 00:30:27,080
fast, it shouldn't appear too slow, it shouldn't, it shouldn't seem fast, it shouldn't

241
00:30:27,080 --> 00:30:32,760
seem slow, if it does, you've got to note that, and you should bring it back to something

242
00:30:32,760 --> 00:30:37,120
that doesn't feel like anything at all, it doesn't give you that feeling that you're

243
00:30:37,120 --> 00:30:47,960
going fast or you're going slow, it's not a magic trick where you can just note faster

244
00:30:47,960 --> 00:30:53,600
and you get that quicker, at John Taughing he said, I think he was talking, I think it

245
00:30:53,600 --> 00:30:58,240
was, I was translating for him, I think, and he's talking about us Westerners, he said,

246
00:30:58,240 --> 00:31:05,040
you know, I know you Westerners, maybe it was just modern, you know, saying in modern society,

247
00:31:05,040 --> 00:31:12,920
but I think he was talking to Westerners, he said modern society, you think that, you know,

248
00:31:12,920 --> 00:31:18,760
you have to work hard and you have to work quickly and you have to get things done quickly

249
00:31:18,760 --> 00:31:24,560
and you'll get a great reward from that, so meditation is not like that, meditation has

250
00:31:24,560 --> 00:31:31,320
to be slow, you practice slowly and you get a reward, but it doesn't mean really slow,

251
00:31:31,320 --> 00:31:39,120
it just means you have to, you can't speed it up, you have to be patient, you have to

252
00:31:39,120 --> 00:31:57,160
go at the speed of it, an ordinary speed, all right, I'm gonna call it a night there, thanks

253
00:31:57,160 --> 00:32:13,760
everyone for tuning in, see you all tomorrow.

