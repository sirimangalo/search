1
00:00:00,000 --> 00:00:14,280
I try and do things that you say to do, but because I'm still at school, I need to listen

2
00:00:14,280 --> 00:00:19,080
and do other stuff, so it causes me to not relax if you get me.

3
00:00:19,080 --> 00:00:23,760
What would you say to do?

4
00:00:23,760 --> 00:00:29,400
And I've recently chose this one because I feel like it applies to everybody because

5
00:00:29,400 --> 00:00:36,760
at some point in the day you feel like you're working and so you're doing something

6
00:00:36,760 --> 00:00:46,080
purely intellectual and it's hard to be mindful at that moment when you're trying to do

7
00:00:46,080 --> 00:00:56,120
something it's just intellectual.

8
00:00:56,120 --> 00:01:05,720
The way I see it is, there are going to be times when it's easy to be mindful and

9
00:01:05,720 --> 00:01:13,520
times when it's going to be hard and while you're trying to complete a task, it may be

10
00:01:13,520 --> 00:01:23,720
difficult or impossible, at least in some capacity, so I think you can be mindful as

11
00:01:23,720 --> 00:01:34,200
much as it's possible, but if you're doing something that's just an intellectual exercise

12
00:01:34,200 --> 00:01:46,780
like adding numbers or anything that you do at work or school, I don't know, don't be guilty

13
00:01:46,780 --> 00:01:52,480
because you can't be mindful of every single moment, you just be mindful of the moments

14
00:01:52,480 --> 00:02:05,480
that you can, so that's where I would start, for sure that's very good advice and don't

15
00:02:05,480 --> 00:02:10,320
be turned off when things are difficult.

16
00:02:10,320 --> 00:02:21,920
Another thing is that difficult meditation, I mean it's not really the problem of the

17
00:02:21,920 --> 00:02:30,160
meditation at all, it strikes me that maybe part of your problem is not the work that

18
00:02:30,160 --> 00:02:39,520
you're doing, but your understanding of what the meditation is because you see something

19
00:02:39,520 --> 00:02:46,400
about your need to work causes you not to relax.

20
00:02:46,400 --> 00:02:54,160
Well, unfortunately, the meditation unfortunately, actually, the meditation is not for the

21
00:02:54,160 --> 00:02:59,840
purpose of causing you to relax, I mean if you're in a relaxing environment, it may very

22
00:02:59,840 --> 00:03:04,640
well be that you are relaxed, if you're in a stressful environment, it's probably the

23
00:03:04,640 --> 00:03:10,400
case that you're going to be stressed, if you have to work, nothing can stop you from

24
00:03:10,400 --> 00:03:15,080
being stressed during your work, you can try to force yourself not to be stressed, this

25
00:03:15,080 --> 00:03:22,920
tends to cause even more stress and in the long run will only have negative effect.

26
00:03:22,920 --> 00:03:29,400
So the point is that the stress and the practice are two different things, there are

27
00:03:29,400 --> 00:03:35,400
times for sure when you won't be able to be mindful, as Desmond said, you try to be mindful

28
00:03:35,400 --> 00:03:42,160
when you cannot, when you're not mindful, you accept it, you can't be mindful.

29
00:03:42,160 --> 00:03:47,800
When you can be mindful, often the feeling comes up that it's useless, right?

30
00:03:47,800 --> 00:03:54,000
Because you're mindful, but you're still not relaxed, so the point is it, you know,

31
00:03:54,000 --> 00:04:00,120
would obviously meditation doesn't work in this situation, right?

32
00:04:00,120 --> 00:04:06,400
Well wrong, meditation isn't for the purpose, as I said, of making you relax, relaxed,

33
00:04:06,400 --> 00:04:11,120
it's for the purpose of helping you to deal with situations that are not relaxing, so

34
00:04:11,120 --> 00:04:18,360
as I said, with the anxiety question, it's learning to experience the physical results

35
00:04:18,360 --> 00:04:26,160
of stress for what they are without reacting to them, without needing them to change.

36
00:04:26,160 --> 00:04:30,360
When you're tense, when your body is stressed and you have a headache, meditate on the

37
00:04:30,360 --> 00:04:34,880
stress and the headache, meditate on the tension, meditate on what's there, don't expect

38
00:04:34,880 --> 00:04:40,320
it to go away, don't expect to be relaxed, but come to see the difference between the

39
00:04:40,320 --> 00:04:41,320
body and the mind.

40
00:04:41,320 --> 00:04:47,320
I mean, when I do these sessions and I have to set up the computer, I feel all headache

41
00:04:47,320 --> 00:04:52,080
and so on, but I look at my neck and I say, well that's just a headache, it's just

42
00:04:52,080 --> 00:04:58,760
a feeling, you meditate on the feeling, you don't expect it to go away, you don't think

43
00:04:58,760 --> 00:05:03,560
of it as something wrong, you don't think of it as a sign that you're destroying yourself

44
00:05:03,560 --> 00:05:06,640
or so on, just see it as a headache.

45
00:05:06,640 --> 00:05:11,000
When you have tension in the body, sometimes my neck from sitting here like this, and

46
00:05:11,000 --> 00:05:16,720
then she says, meet your guys, it's because we eat too much coconut oil, but I think

47
00:05:16,720 --> 00:05:23,000
it's from the tension of sitting in front of the computer as well, so sometimes when I'm

48
00:05:23,000 --> 00:05:25,560
sitting here for a while, then my neck stirs different.

49
00:05:25,560 --> 00:05:31,960
So sometimes you think, wow, it's a sign that I'm really destroying myself.

50
00:05:31,960 --> 00:05:36,640
It was this man who was here recently, he tried to explain to me how he had this exact

51
00:05:36,640 --> 00:05:44,920
thing that happened to him, he had really, he had a problem that was caused by the computer.

52
00:05:44,920 --> 00:05:48,600
This is basically how he put it, something almost exactly there, I don't know if he's listening

53
00:05:48,600 --> 00:05:55,000
there or not, but and so I explained to him, he said, you know, really, that's just the

54
00:05:55,000 --> 00:05:59,560
idea that's in your head, the truth of it is there's a feeling that arises here and

55
00:05:59,560 --> 00:06:05,320
now it may have some causes and conditions that arose in the past, but those are gone

56
00:06:05,320 --> 00:06:06,320
now.

57
00:06:06,320 --> 00:06:10,680
What's here and now is a feeling, it arises and he says, and it's really meaningless,

58
00:06:10,680 --> 00:06:16,160
it can't hurt you, it can't cause you to suffer unless you let it.

59
00:06:16,160 --> 00:06:21,080
This is what the meditation is all about, it's about experiencing things objectively

60
00:06:21,080 --> 00:06:32,640
as they are, try to be patient and learn or come to understand the possibility of meditation

61
00:06:32,640 --> 00:06:44,280
without relaxation, happiness without happiness without pleasure.

62
00:06:44,280 --> 00:06:49,520
So everything can be going horribly and yet inside you still have happiness.

63
00:06:49,520 --> 00:06:55,440
This is very difficult to see, you can have a headache and be happy, difficult.

64
00:06:55,440 --> 00:07:00,160
People can be yelling at you and you can be happy, you can be arguing with people and

65
00:07:00,160 --> 00:07:08,360
still be totally removed from the argument, maybe not entirely, but it's very difficult

66
00:07:08,360 --> 00:07:12,800
when you're arguing, talking and talking and talking, but it actually is possible.

67
00:07:12,800 --> 00:07:17,560
You can, like as I'm talking now, I can be mindful of it, I can be mindful of my lips

68
00:07:17,560 --> 00:07:23,720
moving, I can recognize this is a feeling that arises in the throat and the lips and in

69
00:07:23,720 --> 00:07:30,520
the head and recognize the thoughts and the emotions and the happiness and the unhappiness

70
00:07:30,520 --> 00:07:39,960
and worries and stresses that arise and actually can meditate while you're talking.

71
00:07:39,960 --> 00:07:46,040
Because the intention to talk arises and then already it begins, I think I've talked

72
00:07:46,040 --> 00:07:50,880
about this before and then the bell starts to work and that's more or less automatic

73
00:07:50,880 --> 00:07:57,440
and without even thinking about it you can let the mouth continue to talk and at the same

74
00:07:57,440 --> 00:08:04,640
time say to yourself feeling, feeling, but for sure at every moment you can do it when

75
00:08:04,640 --> 00:08:09,360
you feel happy about something, you focus on the happiness, feel unhappy.

76
00:08:09,360 --> 00:08:14,400
At first it won't seem to have any effect because as you say it's not relaxing, but

77
00:08:14,400 --> 00:08:23,720
if you compare doing that with not doing that, it's just engaging and reacting to everything.

78
00:08:23,720 --> 00:08:29,760
The experience afterwards is incredibly different and the result is incredibly different.

79
00:08:29,760 --> 00:08:39,920
The result of being mindful in this way is totally different in a good way than reacting

80
00:08:39,920 --> 00:08:42,640
blindly to everything that arises.

81
00:08:42,640 --> 00:08:48,000
So if at least from time to time you can get yourself back on track of the emotions and

82
00:08:48,000 --> 00:08:52,760
the reactions and so on, acknowledging at least sometimes you'll find that you're much

83
00:08:52,760 --> 00:08:56,840
better off at the end of the day, you might as much clearer and you've actually filed

84
00:08:56,840 --> 00:09:05,120
things in their appropriate slots so that you can sleep without dreaming and clinging

85
00:09:05,120 --> 00:09:14,480
into anybody else want to add something there? How does everyone else deal with work

86
00:09:14,480 --> 00:09:18,960
that they have to do, mindfully?

87
00:09:18,960 --> 00:09:36,960
And how's school going? Are you mindful at school? You know what? Maybe we can even talk

88
00:09:36,960 --> 00:09:37,960
about it.

89
00:09:37,960 --> 00:09:43,960
Okay, okay. All right, well yeah, I think you're good at it.

90
00:09:43,960 --> 00:09:50,560
If you go with the expectation that being mindful is going to make you relaxed all the time,

91
00:09:50,560 --> 00:09:56,960
then that's not a realistic, that's not even the coolest, not a realistic expectation either.

92
00:09:56,960 --> 00:09:58,960
It's just a big effort.

93
00:09:58,960 --> 00:10:03,520
Yeah, I mean, you're running an opposite direction if you sit down and you think I'm going

94
00:10:03,520 --> 00:10:05,960
to be relaxed now.

95
00:10:05,960 --> 00:10:20,960
That's fine.

