1
00:00:00,000 --> 00:00:12,920
So, tonight we will be going over some fundamentals of meditation practice, giving basic

2
00:00:12,920 --> 00:00:18,360
instruction in meditation practice by request.

3
00:00:18,360 --> 00:00:38,320
So, let's all get in a meditative mode, let's restart, once we start talking about meditation

4
00:00:38,320 --> 00:00:48,040
it's clearly not a theoretical subject, this isn't a talk that you should listen to intellectually

5
00:00:48,040 --> 00:00:59,280
it's a teaching that you should put into practice.

6
00:00:59,280 --> 00:01:15,700
The practice of meditation really follows this sequence of trainings as laid down by the

7
00:01:15,700 --> 00:01:28,460
Buddha, so the Buddha said there are three trainings that we should train ourselves in,

8
00:01:28,460 --> 00:01:40,760
and probably we call these sea lassamadi and panya, in English we might say morality, concentration

9
00:01:40,760 --> 00:01:53,900
and wisdom, but these are misleading or it's difficult to understand these without explaining

10
00:01:53,900 --> 00:02:02,220
them, explaining these terms in detail because misunderstanding arises and based on the

11
00:02:02,220 --> 00:02:09,060
words themselves, morality for example, so with morality we think of certain deeds that

12
00:02:09,060 --> 00:02:20,300
we shouldn't perform and concentration we think of focusing the mind on a single object

13
00:02:20,300 --> 00:02:33,180
or single point and keeping it in one place and wisdom we think of as intellectual thought

14
00:02:33,180 --> 00:02:47,820
or ideas, concepts, theories and so on, these are mundane morality, mundane concentration

15
00:02:47,820 --> 00:03:02,260
and mundane wisdom, in meditation it's on a more ultimate or a more experiential level,

16
00:03:02,260 --> 00:03:09,900
that has to do with our experience, morality plays a part in our existence right here

17
00:03:09,900 --> 00:03:18,460
and now even when we don't have an opportunity to do or say bad things, true morality

18
00:03:18,460 --> 00:03:40,660
is the mind that avoids or refrains from what we might call evil or unwholesumness and

19
00:03:40,660 --> 00:03:46,780
real concentration is, we might let's put it phrase in terms of reality, we'll say morality

20
00:03:46,780 --> 00:04:00,900
is the refraining from or the limiting one's attention to ultimate reality, to experience,

21
00:04:00,900 --> 00:04:07,940
so when the mind wanders off into concepts we bring it back to experience, to reality,

22
00:04:07,940 --> 00:04:13,900
to what's really being experienced right, so when we're thinking it's all just thinking

23
00:04:13,900 --> 00:04:20,180
but instead of seeing it as thinking we see it as this reality or that concept as people

24
00:04:20,180 --> 00:04:28,060
and places and things that we're thinking about, we don't realize that we're thinking.

25
00:04:28,060 --> 00:04:33,020
Concentration is the focusing in the Buddha's teaching, the ultimate or the highest concentration

26
00:04:33,020 --> 00:04:38,860
is the focusing on ultimate reality, to not focusing on one single thing but focusing

27
00:04:38,860 --> 00:04:47,540
on your own experience, having a focused mind, the mind that is in focus, so seeing things

28
00:04:47,540 --> 00:04:53,820
as they are, not how we want them to be or how we don't want them to be, just seeing them

29
00:04:53,820 --> 00:05:06,540
as they are.

30
00:05:06,540 --> 00:05:13,500
And wisdom is rather than any kind of theory about reality or the universe or the past or

31
00:05:13,500 --> 00:05:20,980
the future or our life or life in general, its understanding of experience, or understanding

32
00:05:20,980 --> 00:05:23,300
of reality.

33
00:05:23,300 --> 00:05:27,900
Which is really quite simple actually, it means seeing reality, knowing reality for what

34
00:05:27,900 --> 00:05:36,420
it is, it's something that we already think we have but we don't, we don't really understand

35
00:05:36,420 --> 00:05:42,940
reality, we don't understand our own experiences, if we did why would our experiences

36
00:05:42,940 --> 00:05:51,460
cause trouble for us, why would we do and say and think things that were too our detriment,

37
00:05:51,460 --> 00:05:57,860
it wouldn't if we really understood what we were doing, this is the idea, the idea

38
00:05:57,860 --> 00:06:11,020
behind wisdom, behind the training or training ourselves to go into or to enter into every

39
00:06:11,020 --> 00:06:16,980
experience with wisdom that prevents us from making bad choices.

40
00:06:16,980 --> 00:06:23,620
So this is the base of the teaching, first we have to bring the mind back or keep the

41
00:06:23,620 --> 00:06:33,540
mind from leaving the present moment or leaving the reality in front of us and we have

42
00:06:33,540 --> 00:06:38,860
to focus on the reality to see it as it is and then we have to see and to know it and

43
00:06:38,860 --> 00:06:41,460
to understand it as it is.

44
00:06:41,460 --> 00:06:46,660
This is the progression of our practice.

45
00:06:46,660 --> 00:06:50,980
The actual practice, the technique of the practice is what I was asked to talk about today

46
00:06:50,980 --> 00:06:58,100
is based on the four foundations of mindfulness which is, it's actually a subjective teaching

47
00:06:58,100 --> 00:07:04,140
of the Buddha, there's nothing in reality that you can point to and say these four foundations

48
00:07:04,140 --> 00:07:10,180
of mindfulness, it's just a, it's actually kind of a strange sounding title.

49
00:07:10,180 --> 00:07:18,140
That means the four categories of experience that you can establish mindfulness on, it's

50
00:07:18,140 --> 00:07:23,420
important to have categories or it's important to be able to name out what is ultimate

51
00:07:23,420 --> 00:07:30,980
reality so that you know what is within limits and what is outside of limits for beginning

52
00:07:30,980 --> 00:07:34,100
meditative is important.

53
00:07:34,100 --> 00:07:40,460
So we take these four to be limits, anything outside of these is not an objective entity,

54
00:07:40,460 --> 00:07:49,340
it's not something we should focus on and by separating them into four they, it's easy to

55
00:07:49,340 --> 00:07:57,260
tell what's what, or it's easier to recognize things as they are rather than just lumping

56
00:07:57,260 --> 00:08:03,180
them all together into one category of experience so we break experience up like this.

57
00:08:03,180 --> 00:08:09,300
It's quite useful especially for a beginner meditator you give them four things and they

58
00:08:09,300 --> 00:08:11,500
have something to work with.

59
00:08:11,500 --> 00:08:17,060
So these four are actually the most important thing for a beginner meditator to learn

60
00:08:17,060 --> 00:08:23,100
and so we often have people even memorize them and I'll go through them so it should

61
00:08:23,100 --> 00:08:31,940
be fairly easy to remember, these are the body, the feelings, the mind and the dumbness

62
00:08:31,940 --> 00:08:39,420
or the reality and it's a little bit of a, have to explain the force one a little bit.

63
00:08:39,420 --> 00:08:44,900
But the first three are fairly self-explanatory, the first one is the body and it doesn't

64
00:08:44,900 --> 00:08:51,980
or not exactly the body, it means body or bodily experiences because the body is still

65
00:08:51,980 --> 00:09:00,100
just a concept but in regards to the body or bodily experiences, in regards to body there

66
00:09:00,100 --> 00:09:06,700
are bodily experiences, there is the experience of sitting for example, what is the experience

67
00:09:06,700 --> 00:09:14,380
of sitting while there's pressure in the back and there's a softness on the seat that

68
00:09:14,380 --> 00:09:19,940
we're sitting on, there's attention in the legs, there are various aspects to the sitting

69
00:09:19,940 --> 00:09:27,740
there's even a heat in the body that comes from the contact with the seat and is generated

70
00:09:27,740 --> 00:09:33,180
from the body itself or there's cold from the air around us, there's a physical experience

71
00:09:33,180 --> 00:09:39,260
and we sum that all up by sitting sitting but actually sitting doesn't really exist, it's

72
00:09:39,260 --> 00:09:47,860
just a name for a certain experience and they're standing, they're walking, they're lying

73
00:09:47,860 --> 00:09:57,460
down, these are the basic aspects of physical experience, bending, stretching, turning, reaching,

74
00:09:57,460 --> 00:10:05,460
grasping, eating, chewing, swallowing, brushing your teeth, washing your hair, using the

75
00:10:05,460 --> 00:10:10,700
toilet, all of this is physical experience, you can even meditate on the toilet, the Buddha

76
00:10:10,700 --> 00:10:24,220
himself said it, if you're clearly aware of the physical experience, this is body.

77
00:10:24,220 --> 00:10:29,140
One of the things that we focus on is the stomach, it's a good example because it's a part

78
00:10:29,140 --> 00:10:35,740
of the body that's moving or that's a bodily experience and it's always present or most

79
00:10:35,740 --> 00:10:43,740
of the time is clearly perceptible, so when the stomach rises we just recognize it as

80
00:10:43,740 --> 00:10:54,700
rising and when it falls, as falling this is a basic meditation exercise using the body.

81
00:10:54,700 --> 00:11:04,980
The second one by feelings we don't mean mental judgments or so and we mean simple sensations,

82
00:11:04,980 --> 00:11:11,260
painful sensations, pleasant sensations and neutral sensations, so it could be pain in the body,

83
00:11:11,260 --> 00:11:17,980
this is an obvious one, if you recognize the pain as pain, you remind yourself it's pain,

84
00:11:17,980 --> 00:11:25,380
this is mindfulness of the feelings, if you feel happy and you say you remind yourself

85
00:11:25,380 --> 00:11:33,420
that it is happy and keep your mind focused on it as it's there or pleased or pleasure

86
00:11:33,420 --> 00:11:41,140
or even just feeling, or if you feel calm and you remind yourself calm, this is

87
00:11:41,140 --> 00:11:50,620
mindfulness of the feelings.

88
00:11:50,620 --> 00:11:57,220
When you're thinking about the past or the future and you have thoughts, this is the mind,

89
00:11:57,220 --> 00:12:14,220
when you're thinking dreaming or planning or fantasizing or reminiscing any kind of

90
00:12:14,220 --> 00:12:18,780
discursive thought, any kind of thought at all, the meditation isn't trying to get rid

91
00:12:18,780 --> 00:12:24,220
of the thought, it's not trying to get rid of anything, so this is, as I said before

92
00:12:24,220 --> 00:12:29,260
this kind of like a study, remember what we're trying to gain here, we're trying to gain

93
00:12:29,260 --> 00:12:38,220
first morality, so when thinking arises, well that's still reality, but when you get lost

94
00:12:38,220 --> 00:12:45,140
in it, when you start to perceive it as being an issue or a problem or a story or a fantasy,

95
00:12:45,140 --> 00:12:50,380
then you're outside of reality, so we have to catch the thoughts and see them just as thinking,

96
00:12:50,380 --> 00:12:55,540
when you remind ourselves thinking and trying to train the mind to only see and is thinking,

97
00:12:55,540 --> 00:13:00,820
this is bringing the mind back to reality, this is morality, bringing the mind back to reality,

98
00:13:00,820 --> 00:13:07,300
and then once you see the reality, see the thoughts arising and see, this is your focus,

99
00:13:07,300 --> 00:13:13,380
your focus on them as they arise and see, and the wisdom is seeing that thoughts arise

100
00:13:13,380 --> 00:13:17,380
and see some coming to understand that thoughts arise and see if they're not me, they're

101
00:13:17,380 --> 00:13:21,620
not mind, they're not under my control, they're not worth pursuing, they're not worth

102
00:13:21,620 --> 00:13:31,660
clinging to, they're just reality, they come in the going, and it's this realization

103
00:13:31,660 --> 00:13:44,980
of the pure state of awareness in the present moment, not judgmental and not distracted

104
00:13:44,980 --> 00:13:53,940
or lost or confused or muddled by thoughts and by concepts.

105
00:13:53,940 --> 00:13:58,660
So these three are pretty self-explanatory, you can go back and forth with them, the fourth

106
00:13:58,660 --> 00:14:06,260
one isn't mysterious or anything, it just means it's kind of like et cetera or extras

107
00:14:06,260 --> 00:14:20,060
or sets of the Buddha's teaching you might even say, that progress one further towards

108
00:14:20,060 --> 00:14:22,100
enlightenment.

109
00:14:22,100 --> 00:14:31,260
So the first set is the five hindrances, they're emotive states that will arise and get

110
00:14:31,260 --> 00:14:38,140
in your way and keep you from seeing clearly, so they're kind of extra from the other

111
00:14:38,140 --> 00:14:39,140
three.

112
00:14:39,140 --> 00:14:44,340
So it means in the mind there will be liking and disliking, you'll be focusing on the

113
00:14:44,340 --> 00:14:48,860
body, watching the stomach rise and fall or the feelings or so, and then there will be

114
00:14:48,860 --> 00:14:54,700
a liking or a disliking, maybe you'll feel bored or maybe you'll feel scared or maybe

115
00:14:54,700 --> 00:15:02,420
you'll feel frustrated or even a great anger can come up if it's stuck inside.

116
00:15:02,420 --> 00:15:05,460
On the other hand, you might be liking, you might like the meditation or there might

117
00:15:05,460 --> 00:15:13,020
be wanting, wanting food or wanting pleasure, wanting this or that or liking some experience

118
00:15:13,020 --> 00:15:18,220
that arise, it's maybe some thoughts or fantasies arise and you like them or some pleasant

119
00:15:18,220 --> 00:15:24,660
feelings arise in the body and you like them or you hear something and you like that.

120
00:15:24,660 --> 00:15:32,540
Now this liking and disliking are things that take us out of the present moment, take us

121
00:15:32,540 --> 00:15:39,180
out of reality, you're no longer studying, you're no judging and you're cultivating habits

122
00:15:39,180 --> 00:15:41,220
of judgment.

123
00:15:41,220 --> 00:15:47,940
This liking doesn't lead just to getting, it leads to more liking, it's habitual, disliking

124
00:15:47,940 --> 00:15:54,540
is the same, it leads to anger, it's liking, you have more you give rise and give

125
00:15:54,540 --> 00:16:04,740
and more you cling to the disliking, the more this liking there will be, the more angry

126
00:16:04,740 --> 00:16:06,540
a person you become.

127
00:16:06,540 --> 00:16:14,020
These are hindrances, they stop us from seeing things clearly, they cloud the mind, they

128
00:16:14,020 --> 00:16:22,180
lead the mind to, lead the mind out of reality and into a muddled state and they confuse

129
00:16:22,180 --> 00:16:30,580
the mind, they tire the mind, they tax the mind's strength and they prevent one, the most

130
00:16:30,580 --> 00:16:34,980
important is they prevent you from seeing clearly so again we're not judging them but we're

131
00:16:34,980 --> 00:16:39,740
learning about them, we're studying, we're learning how they come and how they go, what

132
00:16:39,740 --> 00:16:44,020
makes them arise, what makes them cease, we're coming to understand everything about them,

133
00:16:44,020 --> 00:16:45,420
that's all we need.

134
00:16:45,420 --> 00:16:53,020
As I said, the theory is that once you, and the observation, the observation is that

135
00:16:53,020 --> 00:17:00,300
once you understand something, once you understand something to be to your detriment, you

136
00:17:00,300 --> 00:17:11,020
abandon it without thought, without even having to consider, without effort, simply

137
00:17:11,020 --> 00:17:18,020
through understanding the mind, when understanding arises in the mind, understanding

138
00:17:18,020 --> 00:17:28,940
of the problems with negativity or with clinging in any case, then there will arise

139
00:17:28,940 --> 00:17:37,500
no clinging, the mind will change, the mind changes based on the habit.

140
00:17:37,500 --> 00:17:42,820
The liking, disliking in other words, drowsiness or laziness, so this is a hindrance because

141
00:17:42,820 --> 00:17:51,180
it keeps you from seeing things, keeps you from jumping, from going out to the object,

142
00:17:51,180 --> 00:17:59,460
from perceiving the object clearly, distraction when the mind is not focused, so laziness

143
00:17:59,460 --> 00:18:04,860
or this data feeling laziness too much focus or too much concentration, distraction

144
00:18:04,860 --> 00:18:09,820
is when you don't have enough concentration, we have to balance, let's find the balance

145
00:18:09,820 --> 00:18:17,300
where we're not too focused, laziness actually isn't a lack of energy, it's a excess

146
00:18:17,300 --> 00:18:22,140
of concentration, so you can actually pull yourself back and you can cultivate the energy

147
00:18:22,140 --> 00:18:31,380
that's missing by seeing it, by noting to yourself drowsy and drowsy, distraction

148
00:18:31,380 --> 00:18:36,940
or noting to yourself distracted or worried if you're worried and your mind is not able

149
00:18:36,940 --> 00:18:45,700
to stay firm, it's a worried worried, if you have doubt or confusion, say to yourself

150
00:18:45,700 --> 00:18:52,620
doubting, doubting, the underlying technique here is just this noting, this reminding

151
00:18:52,620 --> 00:18:58,260
yourself, the best we don't understand is reminding yourself because we're trying to use

152
00:18:58,260 --> 00:19:04,180
something that's broken to fix itself, our mind is, in a sense, broken, by broken means

153
00:19:04,180 --> 00:19:10,500
we do things that cause our suffering, we give rise to unwholesome thoughts, thoughts that

154
00:19:10,500 --> 00:19:16,100
are not to our benefit or to our detriment, if you're not such a person then we're

155
00:19:16,100 --> 00:19:19,700
considered even lightened and you don't have to practice, but this is the purpose of

156
00:19:19,700 --> 00:19:27,100
meditating to change or to remove or to understand at least what it is that we're doing

157
00:19:27,100 --> 00:19:36,540
to ourselves, but to do this we have to use the mind, the mind which is subjective,

158
00:19:36,540 --> 00:19:40,780
which is judgmental, which is partial, so how do we break out of that?

159
00:19:40,780 --> 00:19:47,460
We need some means of coming outside of our own habits, our own mode of behavior and

160
00:19:47,460 --> 00:19:54,060
this is with the reminding us, we have this objective tool that never changes, you can't

161
00:19:54,060 --> 00:19:58,700
trick yourself into using some other word and when you feel pain saying happy, happy

162
00:19:58,700 --> 00:20:03,500
or something like that, if you understand the technique, you have to say to yourself pain,

163
00:20:03,500 --> 00:20:09,860
you have to remind yourself and you have to confirm in yourself a firm pain, the firm

164
00:20:09,860 --> 00:20:15,460
the reality of it, it's similar to this positive affirmation stuff, it's totally different,

165
00:20:15,460 --> 00:20:22,500
but it's a similar concept and it's kind of counterintuitive, we think that what you should

166
00:20:22,500 --> 00:20:28,860
be saying is happy, happy or no pain, no pain, when you feel angry you should be saying

167
00:20:28,860 --> 00:20:37,420
love, love, to try to counter it, this is if you were trying to create some state, some

168
00:20:37,420 --> 00:20:44,340
opposite state, we're not trying to do that, we're trying to understand or to focus on

169
00:20:44,340 --> 00:20:52,220
the bad stuff, and it seems counterintuitive, you think why would you focus on the bad stuff,

170
00:20:52,220 --> 00:20:59,620
well why would a doctor focus on the sickness, same sort of thing, we have a problem,

171
00:20:59,620 --> 00:21:09,020
we are, our mind is not tweaked correctly, it needs to be adjusted, so we have to look

172
00:21:09,020 --> 00:21:13,420
and see where it needs to be adjusted, but we have to do it objectively and that's what

173
00:21:13,420 --> 00:21:19,940
this word does, so with the body we can just focus on the sitting and say to ourselves

174
00:21:19,940 --> 00:21:26,860
sitting, or we can focus on the stomach, the basic exercise we give to meditators who

175
00:21:26,860 --> 00:21:30,780
come for an intensive course who's watching the stomach, rising and falling and just

176
00:21:30,780 --> 00:21:39,940
remind themselves, rising, falling, of course you don't say these words out loud, you're

177
00:21:39,940 --> 00:21:49,060
just in your mind reminding yourself, so as to cut off or preclude any judgment, any thoughts

178
00:21:49,060 --> 00:21:56,740
of liking or disliking the experience or identifying with the experience or any discursive

179
00:21:56,740 --> 00:22:06,380
or extraneous thought, so when you feel pain and you say pain, this is learning about

180
00:22:06,380 --> 00:22:12,900
not just the pain, but also learning about our reactions to it, and once you see that

181
00:22:12,900 --> 00:22:16,980
your reactions are the problem and it's actually, you're disliking of the pain that's

182
00:22:16,980 --> 00:22:21,940
the problem, you come to see that pain is really just pain and as you say to yourself pain,

183
00:22:21,940 --> 00:22:27,340
pain, you're teaching yourself something, whereas without looking you would be upset about

184
00:22:27,340 --> 00:22:32,900
the pain, once you look at it and understand it as pain you realize for yourself it is

185
00:22:32,900 --> 00:22:39,340
just pain, there's nothing good or bad about it, and the same with happy feelings as

186
00:22:39,340 --> 00:22:44,980
well instead of clinging to them and wishing for them and hoping for them and feeling depressed

187
00:22:44,980 --> 00:22:50,940
and longing for them when they're gone, we see them just as happy feelings and we realize

188
00:22:50,940 --> 00:22:56,780
there's no benefit that comes from clinging to them, they might be good but as soon as

189
00:22:56,780 --> 00:22:59,820
you say that they're good you start clinging and when you cling then you suffer when

190
00:22:59,820 --> 00:23:06,540
they're gone, you have this partiality where your happiness depends on them, so we stop that,

191
00:23:06,540 --> 00:23:11,380
we know them just as happiness and you're thinking and then you say to yourself thinking

192
00:23:11,380 --> 00:23:16,420
thinking and thoughts as well have no power over you, good thoughts, bad thoughts, good

193
00:23:16,420 --> 00:23:25,060
thoughts don't take you away bad thoughts, don't upset you and even with the hindrances

194
00:23:25,060 --> 00:23:33,260
once we say to ourselves liking, liking or disliking, disliking, disliking, this is the means

195
00:23:33,260 --> 00:23:38,100
of understanding these things, sometimes it feels like they get worse when you acknowledge

196
00:23:38,100 --> 00:23:42,140
them and you say to yourself angry, angry, sometimes it feels like you get more angry

197
00:23:42,140 --> 00:23:47,780
and this is how it should be in the beginning because we have anger inside of us and our

198
00:23:47,780 --> 00:23:52,700
reaction to anger is to get angry, we have to see this, we have to see what we're doing

199
00:23:52,700 --> 00:23:57,060
to ourselves, it's actually the best thing for us once we watch the anger and we come

200
00:23:57,060 --> 00:24:03,100
upset by it, once we realize how horrific this anger is and how it can build and so on,

201
00:24:03,100 --> 00:24:07,620
this is exactly what we need to see, it shouldn't build, it should get out of hand so that

202
00:24:07,620 --> 00:24:13,660
we can see how out of hand gets and really want to change, deep down the mind will, this

203
00:24:13,660 --> 00:24:19,340
will affect the mind when it sees how strong this anger is, the problem with anger is

204
00:24:19,340 --> 00:24:26,620
and then it comes up, the problem is that we don't understand it when it arises and so

205
00:24:26,620 --> 00:24:34,060
we follow it, we make use of it, we use it as a premise by which to act, I'm angry

206
00:24:34,060 --> 00:24:38,660
therefore I should hit you or therefore I should yell at you, there's before I should

207
00:24:38,660 --> 00:24:45,300
do something nasty for you towards you, we don't ever understand the anger, let it come,

208
00:24:45,300 --> 00:24:50,780
this is important, it's important to teach your mind lesson, it's going to get angry, well

209
00:24:50,780 --> 00:25:01,820
let it see what anger is, look at the anger, let it eat its own dog food, so they say.

210
00:25:01,820 --> 00:25:07,340
So these are the four foundations, in brief this is all you need to, no depractist meditation,

211
00:25:07,340 --> 00:25:12,100
it's really almost enough, there's only one other thing that I'd add and that's the

212
00:25:12,100 --> 00:25:19,420
next set in the dumbest is the senses, so other things that might come up are seeing,

213
00:25:19,420 --> 00:25:24,380
hearing, smelling, tasting, feeling and thinking, thinking we already have, but the other

214
00:25:24,380 --> 00:25:32,020
five senses we have to note as well, seeing and hearing, all of these senses can give rise

215
00:25:32,020 --> 00:25:40,100
to partiality or conceptual thought, we see something we begin to think about it, when

216
00:25:40,100 --> 00:25:46,140
we hear something we begin to think about it, so we get lost from the meditation, we lose

217
00:25:46,140 --> 00:25:51,460
our morality in a sense, because we lose our morality, we have no concentration, no focus,

218
00:25:51,460 --> 00:25:56,780
we have no focus, we lose our wisdom, we're in the realm of delusion, where it's very

219
00:25:56,780 --> 00:26:03,100
easy to give rise to judgment and partiality, so we bring ourselves back when we see,

220
00:26:03,100 --> 00:26:08,580
we catch it just at the door and say to ourselves seeing, seeing, when we hear something

221
00:26:08,580 --> 00:26:16,900
hearing, when we smell or taste or feel, is it smelling, smelling, tasting, feeling and

222
00:26:16,900 --> 00:26:23,540
when we think, of course we know thinking, and in this way we've kind of covered all of

223
00:26:23,540 --> 00:26:29,500
our basements, there's nothing that could arise as long as we stay, the Buddha was emphatic

224
00:26:29,500 --> 00:26:36,100
about this, in fact he said, if you stay within this boundary of the four foundations

225
00:26:36,100 --> 00:26:42,900
of mindfulness, nothing can get you, Mara can't get you, evil cannot reach you, no trouble

226
00:26:42,900 --> 00:26:50,180
can come to you, it's a very powerful statement and it seems hard to believe, until you understand

227
00:26:50,180 --> 00:26:55,740
what is reality and you realize that all of your problems are outside of this, that there's

228
00:26:55,740 --> 00:27:01,380
no problems in any of these things, there's no issues in any of these things, the problem

229
00:27:01,380 --> 00:27:09,620
is in identifying with them, making more of them than they are projecting, extrapolating

230
00:27:09,620 --> 00:27:21,220
upon them, once they are what they are and that's all they are, there is no potential

231
00:27:21,220 --> 00:27:31,940
for suffering, there's no potential for clinging, there's no potential for expectation.

232
00:27:31,940 --> 00:27:38,620
So this is all the practice really, it's a training and a studying and a learning process.

233
00:27:38,620 --> 00:27:43,460
You want to be learning as much as you can, it's not something you should be, meditation

234
00:27:43,460 --> 00:27:47,180
isn't something you should be using to escape, it should be a laboratory where you study

235
00:27:47,180 --> 00:27:55,260
the things you don't dare study anywhere else, people who have anxiety or phobias or depression

236
00:27:55,260 --> 00:28:00,380
or anger, they don't dare to look at these things, anything to get away from them, how can

237
00:28:00,380 --> 00:28:06,300
I cure them, this is why people start to take pills, take medication, it's apparently

238
00:28:06,300 --> 00:28:10,860
a very common thing, I guess we don't realize how common it is but probably a lot of the people

239
00:28:10,860 --> 00:28:19,580
we know are on some sort of medication for one or another mental illness, we often think

240
00:28:19,580 --> 00:28:31,900
maybe it's only me but I'm not taking them but obviously but it's quite a common thing

241
00:28:31,900 --> 00:28:38,860
and we do this trying to get away from them, so meditation is a way to stop that, it's

242
00:28:38,860 --> 00:28:47,380
a controlled means of approaching these issues, these problems, you see we are sitting on

243
00:28:47,380 --> 00:28:58,980
a mat, we can let them arise without much to fear, if we have anger issues then we can

244
00:28:58,980 --> 00:29:05,260
let anger arise because there's no one around to get angry at, if we have fear or worry

245
00:29:05,260 --> 00:29:11,740
and now we can be afraid and worried because it's really inconsequential, we can look at

246
00:29:11,740 --> 00:29:17,340
the worry, we don't have to act out on it and we don't have to take some kind of medication

247
00:29:17,340 --> 00:29:22,900
to get over it, it's like we want these things to come up that we've never allowed ourselves

248
00:29:22,900 --> 00:29:31,220
to look at, so it's quite an exploration, it's something very unique and not very well

249
00:29:31,220 --> 00:29:40,580
understood, we usually think of meditation as some kind of retreat or an escape that certainly

250
00:29:40,580 --> 00:29:50,740
isn't, it's an advance, we are taking a leap into the unknown where we've never really

251
00:29:50,740 --> 00:29:58,140
gone before and this unknown isn't something mysterious, it's who we are, it's our everyday

252
00:29:58,140 --> 00:30:06,700
life that we never really look at, it's like tuning the machine or our automobile going

253
00:30:06,700 --> 00:30:14,260
under the hood, we drive the car every day and we often don't spend enough time tuning

254
00:30:14,260 --> 00:30:19,980
it or cleaning it, if you go around in your car and you never change the oil eventually

255
00:30:19,980 --> 00:30:29,620
it burns up, you know, I'm an engine trouble, it's exactly how the mind is, we don't

256
00:30:29,620 --> 00:30:35,340
spend the time, we spend all the time using our mind, we use our mind for so many things

257
00:30:35,340 --> 00:30:42,900
but we don't spend enough time servicing the mind, so this is kind of the servicing,

258
00:30:42,900 --> 00:30:48,660
removing all the bad stuff and cultivating all the good stuff.

259
00:30:48,660 --> 00:30:52,140
So that's basically how to meditate, this is sitting meditation, there's also, you can do

260
00:30:52,140 --> 00:30:57,580
it walking, so when you walk, I've given instructions in my booklet, you can read that,

261
00:30:57,580 --> 00:31:01,500
when you walk, you know the right foot is moving, step being right, it's just a technique

262
00:31:01,500 --> 00:31:09,700
but the, the, the, or it's a structure, the technique is the same, the noting is, reminding

263
00:31:09,700 --> 00:31:14,700
yourself is the same, if you use this technique of reminding yourself, you can even use

264
00:31:14,700 --> 00:31:18,860
it in daily life when you're sitting in a car, when you're, you're sitting on the bus,

265
00:31:18,860 --> 00:31:23,820
so when you're in at work and you want to take a break, you can do a five minute meditation

266
00:31:23,820 --> 00:31:29,500
break, you can, when you're eating, you can do eating meditation, chewing, chewing, swallowing,

267
00:31:29,500 --> 00:31:37,180
tasting, tasting, anything can become meditation, there's the formal aspect of it isn't

268
00:31:37,180 --> 00:31:47,100
so important, it's just a sort of, in the sense of retreat from, from the whole spectrum

269
00:31:47,100 --> 00:31:52,180
of experience to, to keep, keeping it in just a couple of objects, so walking back and

270
00:31:52,180 --> 00:31:58,660
forth is quite simple, gives you a much, much more controlled environment in which

271
00:31:58,660 --> 00:32:05,460
to study the experience, otherwise the technique is simply this, reminding yourself

272
00:32:05,460 --> 00:32:11,180
and based on these four foundations of mindfulness that Buddha taught us for, for aspects

273
00:32:11,180 --> 00:32:20,260
of existence, aspects of experience, okay, so let's take that as a basic understanding

274
00:32:20,260 --> 00:32:25,220
of meditation and we can try to, you know, do our, we'll do our daily meditation and

275
00:32:25,220 --> 00:32:40,220
try to put this into practice.

