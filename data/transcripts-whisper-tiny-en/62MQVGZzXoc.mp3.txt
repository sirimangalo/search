Hello, welcome back to Ask a Monk.
Today I thought I would try to answer at least one of the questions that are waiting for me.
And the question I'm thinking of now is on euthanasia.
It was asked, what is the Buddhist stance on killing someone who wants to die
or who would perhaps be freed from suffering through their own death?
Now, I hope it's quite clear that I don't think it should be clear from what I've said before that I don't think they would be freed from suffering through their own death.
I think that's something that I don't have to get into.
Because my views on that are pretty clear, I mean, only through giving up, clinging, are you free from suffering?
And that's really a key here that the view and the belief that killing someone could somehow be better for them is delusional
because it's something that they are clinging to and it's an aversion that they have towards the suffering and to indulge that is by no means of any benefit to them.
Anytime we look at a being who's suffering greatly and we think that by putting them out of their misery we're somehow doing them a favor is quite mistaken.
When in fact, some of the greatest insights and wisdom and realizations of the wake-up enlightenment that comes from suffering, that comes from bearing with your suffering and learning to overcome the clinging and the aversion that we have towards it.
That's where the real benefit is.
So countless stories, if you talk to people who work with dying patients in hospice care or so on, they will tell you about the moment of clarity that comes to a person who's suffering greatly, that right before they die, not everyone, but many people will have this moment where suddenly they're free.
And it's so clear that they've gone through something and they've worked through something and they've worked it out.
Finally, at peace with themselves and there's clarity of mind and they're able to move on.
When you kill, when you cut that off, first of all, especially if it's their wish that they should die, then you're helping them escape from the lesson.
You're helping them to run away from this temporarily, from this lesson that they have to help them to overcome the clinging.
Helping them to encourage their clinging to their aversion to pain and suffering.
That's the one side, but I think more important is to talk about the effect of killing on one's own mind, because that's what makes, as I've said before, an action immoral or moral.
It's the effect that it has on your own mind.
It's not really the suffering that it brings to the other person.
The most horrible thing about killing someone else is the disruptive nature of killing.
When you kill something that wants to live, it's of course a lot more disruptive than when you kill someone that wants to die.
But nonetheless, it's an incredibly disruptive act.
It's a very powerful and important act.
For people who have never killed before, for you to never kill before, it's very hard to understand this.
I think they will generally have a natural aversion to it because of the weight of the act.
So many people have never killed even insects and so on.
And really feel it abhorrent to do such a thing just by nature.
And that's because of the strength of the act.
But it may be difficult for them to understand because they've never experienced.
On the other hand, for someone who kills often a person who murders animals or slaughters animals or insects and so on,
it's equally difficult for them to see because they've become desensitized to it.
I've told the story before, but when I was younger, I would do hunting.
I thought to do hunting with my father, to hunt with my father and hunt deer.
And I killed one deer and it was really a very difficult thing to do.
It surprised me because I had no qualms about it.
I didn't think there was anything wrong with killing at the time.
I thought it's a good way to get food.
But when it came time to actually kill the deer, my whole body was shaking.
And it surprised me that I didn't understand what was going on, what was happening.
But of course, later on when I practiced meditation and really woke up and had this great wake-up cause to what I was doing to my mind.
And so many things that I had to go through and have to work through and all of these emotions that came up that I had to sort out and realize and give up.
And just changing my whole outlook until finally there was this great transformation.
When you go through meditation and I was in a pretty bad state, there's a lot of cleaning that goes on, even just in the first few days.
And so, made me realize that, how many of you realize that, oh, that's what was going on now.
There really is a great weight to killing.
And I think that's quite clear when you find tune your mind.
When you start to practice meditation, your mind becomes very quiet, relatively speaking.
You're able to see that any little thing that comes up, you're able to see it much better than before.
Whereas when your mind is very active, how in your full of defilement and greed and anger and so on, you can't really make head or tail of anything.
So it's really difficult for most people to come up with any solid theory of reality or solid understanding of reality when you talk about these things.
It kind of sounds interesting, but it's really hard for them to understand because they've never taken the time to quiet their mind
to receive things, clearly, one by one by one, and to be able to break things apart.
But it's like you have a very fine tuned instrument and you're able to see these things that people aren't able to see.
And so, killing becomes very abhorrent and you're able to see how even the slightest anger has a great power to let alone the destructive power.
When you say something to someone and it changes their life and it disrupts their course, there's great power in it.
But to kill has a far greater power. It's on a whole other level.
Even when you kill someone who wants to die, you're changing their karma.
It's like trying to help someone who's running from the law.
For instance, I'm going to rob the bank, you have to hide them. It's a lot of work.
And that's really what's going on here when you kill someone who wants to die.
It's no less, but it's still on the level of killing someone who doesn't want to die.
Because it's a great and weighty act to kill and it's something that weighs on your mind.
It's a very, there's repercussions because of the power and the intensity of the act of the disruption.
If you think of the world, the universe is being waves, energy waves, and even physical, of course, this energy.
If you think of it as all energy, the explosion of energy that you've created, mental energy is tremendous by killing.
If there's someone who's never killed before, it might be difficult to see.
This is why people think, well, euthanasia could be a good thing.
Now, there's one exception that I've talked about before, and I think it is an exception. I'm not 100% sure, but to me, you could at least argue for it.
And that is letting someone die.
I think clearly you can, you can and should let people die rather than giving them medication.
That's only going to prolong their misery or so on.
I think that's reasonable, but I think also you could stop life support.
If it was considered that they had no chance or maybe even at the mind had left or so on, there was the case.
Actually, apparently that's what happened with Mahasi Sayad, that they said, well, the mind is no longer working.
The brain is no longer functioning, so they could take them off life support because he was no longer there.
But on the other hand, this is where you could be excused.
But on the other hand, my teacher in Thailand gave an interesting perspective on this that you can take as you like.
But he said, well, you never really know.
And we can say that the person is not going to come back, but it's never really clear.
There was the case, and he told this story about someone who they thought was going to die, but they kept them on life support for some time.
And eventually, they were covered and came back and lived another 10 years or something.
And we're able to do all sorts of goodies.
Now, his point was the person has the opportunity and it's life to do good deeds.
You shouldn't be so quick to cut them off because you don't know where they're going.
This is why we say always that this human life is very precious.
Now, the second story I gave about Mathakundaly, the boy who was on his deathbed and had a chance to see the Buddha.
It's just an example of how we can possibly create wholesome mind states before the moment of death.
And you never really know if the person has a chance now as a human being, if their mind is impure, then letting them die or helping them to die.
He's only going to lead them or destined to an unpleasant result, an unpleasant life in the future.
So, if you can help them to stay and to at least help them to practice meditation or listen to the dhamma or so on,
or at least be with their family and learn about compassion and love and so on, and cultivate good thoughts.
And even cultivating patience with the pain, which is an excellent learning tool, learning experience.
It's something that we should not underestimate and undervalue.
There's going to be a great thing, and it's something that we should actually encourage.
Something that is far better than to dismiss the person and send them on their way, not knowing where they're going to go, and not just send them on their way,
but distract their life.
So, even in the case of not killing, but letting a person die, it can be that helping them to stay on is a good thing.
If you have to judge for yourself, it really depends on the individual.
I think I wouldn't want to stay on, and if it came down to taking medication or so on, I would just let myself go.
But you really have to judge for yourself, and for a person who's not meditating for a person who hasn't really begun to practice,
it's always dangerous to let them go because you don't know where they're going.
Once you can see, I guess the answer would be if once you can see that clearly they're on the right path and they're ready to go,
then you can help them to give up medication or to get off life so far and so on.
And say, go on your way and be reassured that they're going in the right direction.
Just be careful that they wouldn't be better off sticking around to do more good deeds and practice more meditation,
because you don't really know where they're going in the future.
So, I hope that that gave some insight into the Buddhist take on euthanasia.
Thanks for tuning in all the best.
