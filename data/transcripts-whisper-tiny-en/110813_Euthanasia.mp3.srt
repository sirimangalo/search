1
00:00:00,000 --> 00:00:03,000
Hello, welcome back to Ask a Monk.

2
00:00:03,000 --> 00:00:09,000
Today I thought I would try to answer at least one of the questions that are waiting for me.

3
00:00:09,000 --> 00:00:14,000
And the question I'm thinking of now is on euthanasia.

4
00:00:14,000 --> 00:00:23,000
It was asked, what is the Buddhist stance on killing someone who wants to die

5
00:00:23,000 --> 00:00:33,000
or who would perhaps be freed from suffering through their own death?

6
00:00:33,000 --> 00:00:41,000
Now I hope it's quite clear that I don't think it should be clear from what I've said before that I don't think they would be freed from suffering through their own death.

7
00:00:41,000 --> 00:00:46,000
I think that's something that I don't have to get into.

8
00:00:46,000 --> 00:00:53,000
Because my views on that are pretty clear, I mean only through giving up, clinging, are you free from suffering?

9
00:00:53,000 --> 00:01:09,000
And that's really a key here that the view and the belief that killing someone could somehow be better for them is delusional

10
00:01:09,000 --> 00:01:21,000
because it's something that they are clinging to and it's an aversion that they have towards the suffering and to indulge that is by no means of any benefit to them.

11
00:01:21,000 --> 00:01:33,000
Anytime we look at a being who's suffering greatly and we think that by putting them out of their misery we're somehow doing them a favor is quite mistaken.

12
00:01:33,000 --> 00:01:47,000
When in fact some of the greatest insights and wisdom and realizations of the wake-up enlightenment that comes from suffering, that comes from bearing with your suffering

13
00:01:47,000 --> 00:01:54,000
and learning to overcome the clinging and the aversion that we have towards it.

14
00:01:54,000 --> 00:01:56,000
That's where the real benefit is.

15
00:01:56,000 --> 00:02:15,000
The countless stories, if you talk to people who work with dying patients in hospice care or so on, they will tell you about the moment of clarity that comes to a person who's suffering greatly, that right before they die, not everyone but many people will have this moment where suddenly they're free.

16
00:02:15,000 --> 00:02:24,000
And it's so clear that they've gone through something and they've worked through something and they've worked it out and they're finally at peace with themselves.

17
00:02:24,000 --> 00:02:30,000
And there's clarity of mind and they're able to move on when you kill, when you cut that off.

18
00:02:30,000 --> 00:02:56,000
Well, first of all, especially if it's their wish that they should die, then you're helping them escape from the lesson, you're helping them to run away from this temporarily, from this lesson that they have to help them to overcome the clinging, helping them to encourage their clinging to their aversion to pain and suffering.

19
00:02:56,000 --> 00:03:07,000
That's the one side, but I think more important is to talk about the effect of killing on one's own mind because that's what makes, as I've said before, an action immoral or moral.

20
00:03:07,000 --> 00:03:12,000
It's the effect that it has on your own mind. It's not really the suffering that it brings to the other person.

21
00:03:12,000 --> 00:03:26,000
The most horrible thing about killing someone else is the disruptive nature of killing when you kill something that wants to live. It's of course a lot more destructive than when you kill someone that wants to die.

22
00:03:26,000 --> 00:03:35,000
But nonetheless, it's an incredibly disruptive act. It's a very powerful and important act for people who have never killed before.

23
00:03:35,000 --> 00:03:44,000
For you to never kill before, it's very hard to understand this. I think they will generally have a natural aversion to it because of the weight of the act.

24
00:03:44,000 --> 00:03:52,000
So many people have never killed even insects and so on, and really feel it abhorrent to do such a thing just by nature.

25
00:03:52,000 --> 00:04:01,000
And that's because of the strength of the act. But it may be difficult for them to understand because they never experienced.

26
00:04:01,000 --> 00:04:18,000
On the other hand, for someone who kills often a person who murders animals or slaughters animals or insects and so on, it's equally difficult for them to see because they've become desensitized to it.

27
00:04:18,000 --> 00:04:30,000
I've told a story before, but when I was younger, I would do hunting. I thought to do hunting with my father, to hunt with my father and hunt deer. And I killed one deer.

28
00:04:30,000 --> 00:04:43,000
And it was really a very difficult thing to do. It surprised me because I had no qualms about it. I didn't think there was anything wrong with killing at the time. I thought it's a good way to get food.

29
00:04:43,000 --> 00:04:54,000
But when it came time to actually kill the deer, my whole body was shaking. And it surprised me that I didn't understand what was going on, what was happening.

30
00:04:54,000 --> 00:05:07,000
But of course, later on when I practiced meditation and really woke up and had this great wake-up cause to what I was doing to my mind.

31
00:05:07,000 --> 00:05:16,000
So many things that I had to go through and to work through and all of these emotions that came up that I had to sort out and realize and give up.

32
00:05:16,000 --> 00:05:22,000
And just changing my whole outlook until finally there was this great transformation.

33
00:05:22,000 --> 00:05:35,000
Sorry, another person talked about great things that happened. When you go through meditation and I was in a pretty bad state, there's a lot of cleaning that goes on, even just in the first few days.

34
00:05:35,000 --> 00:05:44,000
And so made me realize that, how many realized that, oh, that's what was going on there.

35
00:05:44,000 --> 00:06:02,000
There really is a great weight to killing. And I think that's quite clear when you find tune your mind when you start to practice meditation, your mind becomes very quiet, relatively speaking.

36
00:06:02,000 --> 00:06:07,000
You're able to see any little thing that comes up, you're able to see it much better than before.

37
00:06:07,000 --> 00:06:16,000
Whereas when your mind is very active, how in your full of defilement and greed and anger and so on, you can't really make head or tail of anything.

38
00:06:16,000 --> 00:06:24,000
So it's really difficult for most people to come up with any solid theory of reality or solid understanding of reality when you talk about these things.

39
00:06:24,000 --> 00:06:38,000
It kind of sounds interesting, but it's really hard for them to understand because they've never taken the time to quiet their mind and to receive things, clearly one by one by one, and to be able to break things apart.

40
00:06:38,000 --> 00:06:44,000
But it's like you have a very fine tuned instrument and you're able to see these things that people aren't able to see.

41
00:06:44,000 --> 00:06:55,000
So killing becomes very abhorrent and you're able to see how even the slightest anger has a great power to let alone the destructive power.

42
00:06:55,000 --> 00:07:04,000
When you say something to someone and it changes their life and it disrupts their course, there's great power in it.

43
00:07:04,000 --> 00:07:10,000
But to kill has a far greater power. It's on a whole other level.

44
00:07:10,000 --> 00:07:15,000
Even when you kill someone who wants to die, you're changing their karma.

45
00:07:15,000 --> 00:07:24,000
It's like trying to help someone who's running from the law, for instance, I'm going to rob the bank, you have to hide them.

46
00:07:24,000 --> 00:07:25,000
It's a lot of work.

47
00:07:25,000 --> 00:07:28,000
And that's really what's going on here.

48
00:07:28,000 --> 00:07:35,000
When you kill someone who wants to die, it's no less, but it's still on the level of killing someone who doesn't want to die.

49
00:07:35,000 --> 00:07:40,000
Because it's a great and way to act to kill and it's something that weighs on your mind.

50
00:07:40,000 --> 00:07:50,000
It's a very, there's repercussions because of the power and the intensity of the act of the disruption.

51
00:07:50,000 --> 00:07:57,000
If you think of the world, the universe is being the waves, energy waves and even physical, of course, energy.

52
00:07:57,000 --> 00:08:07,000
If it is all energy, the explosion of energy you've created, mental energy is tremendous by killing.

53
00:08:07,000 --> 00:08:10,000
If there's someone who's never killed before, it may be difficult to see.

54
00:08:10,000 --> 00:08:14,000
This is why people think, well, euthanasia could be a good thing.

55
00:08:14,000 --> 00:08:19,000
Now, there's one exception that I've talked about before and I think it is an exception.

56
00:08:19,000 --> 00:08:23,000
I'm not 100% sure, but to me, you could at least argue for it.

57
00:08:23,000 --> 00:08:25,000
And that is letting someone die.

58
00:08:25,000 --> 00:08:33,000
I think clearly you can, you can and should let people die rather than giving them medication.

59
00:08:33,000 --> 00:08:37,000
That's only going to prolong their misery or so on.

60
00:08:37,000 --> 00:08:40,000
I think that's reasonable.

61
00:08:40,000 --> 00:08:53,000
But I think also you could stop life support if it was considered that they had no chance or maybe even at the mind had left or so on.

62
00:08:53,000 --> 00:08:54,000
That's the case.

63
00:08:54,000 --> 00:08:56,000
Actually, apparently that's what happened with Mahasi Sayad.

64
00:08:56,000 --> 00:09:00,000
They said, well, the mind is no longer working, the brain is no longer functioning.

65
00:09:00,000 --> 00:09:05,000
So they could take them off life support because he was no longer there.

66
00:09:05,000 --> 00:09:09,000
But on the other hand, this is where you could be excused.

67
00:09:09,000 --> 00:09:17,000
But on the other hand, my teacher in Thailand gave an interesting perspective on this that you can take as you like.

68
00:09:17,000 --> 00:09:26,000
But he said, well, you never really know and we can say that the person is not going to come back, but it's never really clear.

69
00:09:26,000 --> 00:09:30,000
There was the case and he told this story about someone who they thought was going to die,

70
00:09:30,000 --> 00:09:37,000
but they kept them on life support for some time and eventually they recovered and came back and lived another ten years or something.

71
00:09:37,000 --> 00:09:39,000
And we're able to do all sorts of goodies.

72
00:09:39,000 --> 00:09:43,000
Now his point was the person has the opportunity and it's life to do good deeds.

73
00:09:43,000 --> 00:09:48,000
You shouldn't be so quick to cut them off because you don't know where they're going.

74
00:09:48,000 --> 00:09:53,000
This is why we say always that this human life is very precious.

75
00:09:53,000 --> 00:10:00,000
Now, the second story I gave about Mathakundalee, the boy who was on his deathbed and had a chance to see the Buddha.

76
00:10:00,000 --> 00:10:06,000
That's just an example of how we can possibly create wholesome mind states before the moment of death.

77
00:10:06,000 --> 00:10:12,000
And you never really know if the person has a chance now as a human being.

78
00:10:12,000 --> 00:10:18,000
If their mind is impure, then letting them die or helping them to die.

79
00:10:18,000 --> 00:10:29,000
He's only going to lead them or destined to an unpleasant result, unpleasant life in the future.

80
00:10:29,000 --> 00:10:37,000
So if you can help them to stay and to at least help them to practice meditation or listen to the dhamma or so on,

81
00:10:37,000 --> 00:10:44,000
or at least be with their family and learn about compassion and love and so on, and cultivate good thoughts.

82
00:10:44,000 --> 00:10:51,000
And even cultivating patience with the pain, which is an excellent learning tool, learning experience.

83
00:10:51,000 --> 00:10:56,000
It's something that we should not underestimate and undervalue.

84
00:10:56,000 --> 00:11:01,000
This can be a great thing and it's something that we should actually encourage.

85
00:11:01,000 --> 00:11:11,000
And it's far better than to dismiss the person and send them on their way, not knowing where they're going to go, and not just send them on their way, but distract their life.

86
00:11:11,000 --> 00:11:22,000
So even in the case of not killing, but letting a person die, it can be that helping them to stay on is a good thing.

87
00:11:22,000 --> 00:11:24,000
Now you have to judge for yourself.

88
00:11:24,000 --> 00:11:32,000
It really depends on the individual. I think I wouldn't want to stay on and if it came down to taking medication or so on, I would just let myself go.

89
00:11:32,000 --> 00:11:34,000
But you really have to judge for yourself.

90
00:11:34,000 --> 00:11:46,000
And for a person who's not meditating for a person who hasn't really begun to practice, it's always dangerous to let them go because you don't know where they're going.

91
00:11:46,000 --> 00:11:53,000
If once you can see, I guess the answer would be if once you can see that clearly they're on the right path and they're ready to go,

92
00:11:53,000 --> 00:12:05,000
then you can help them to give up medication or to get off life support or so on and say, go on your way and be reassured that they're going in the right direction.

93
00:12:05,000 --> 00:12:16,000
Just be careful that they wouldn't be better off sticking around to do more good deeds and practice more meditation because you don't really know where they're going in the future.

94
00:12:16,000 --> 00:12:24,000
So I hope that that gave some insight into the Buddhist take on euthanasia and thanks for tuning in all the best.

