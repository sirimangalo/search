1
00:00:00,000 --> 00:00:20,000
Good evening everyone, welcome to our evening dumber, tonight we are looking at purification

2
00:00:20,000 --> 00:00:26,120
through knowledge and vision of what is and what is not the path, it's a bit of a

3
00:00:26,120 --> 00:00:54,080
mouthful. So we begin the practice of mindfulness. We set out on a path to free

4
00:00:54,080 --> 00:01:00,200
ourselves from suffering. You may not phrase it like that but that's generally

5
00:01:00,200 --> 00:01:14,000
the idea. We have problems or challenges or best

6
00:01:14,000 --> 00:01:21,560
say inefficiencies, things about ourselves that are, for whatever reason,

7
00:01:21,560 --> 00:01:28,600
however we look at them, are inefficient. We might present themselves as

8
00:01:28,600 --> 00:01:44,360
stressful or bad habits. We might feel guilty about part of who we are.

9
00:01:44,360 --> 00:01:52,360
We might just have the idea of gaining something extra. Maybe we don't feel bad

10
00:01:52,360 --> 00:01:59,080
about ourselves. It's not supposed to feel bad about yourself. We feel good

11
00:01:59,080 --> 00:02:05,280
about ourselves but we think, hey, that could be even better than this. We hear

12
00:02:05,280 --> 00:02:16,960
about spirituality. And you think, well that's something that I would

13
00:02:16,960 --> 00:02:25,360
really, it could really come in handy, really be useful for me to be more

14
00:02:25,360 --> 00:02:30,400
spiritual person. These qualities of being mindful you think, well that's a

15
00:02:30,400 --> 00:02:37,520
useful skill. That's really how we should look at it. In the Buddhist time, the

16
00:02:37,520 --> 00:02:43,920
best meditators weren't bad people but they were good people who saw that they

17
00:02:43,920 --> 00:02:52,920
could be better people. It's a bit of an important point. Meditation isn't

18
00:02:52,920 --> 00:03:04,200
just to make you an ordinary person. I talk about medication. Medication is

19
00:03:04,200 --> 00:03:07,240
for the purpose of making you a functional member of society. That's not

20
00:03:07,240 --> 00:03:12,480
really what meditation is all about. In fact, you find the more you meditate

21
00:03:12,480 --> 00:03:17,360
the harder it is to function in society. Does that make sense? Because you

22
00:03:17,360 --> 00:03:31,720
start to see the problems with society. But there's a general idea of going

23
00:03:31,720 --> 00:03:38,480
somewhere or getting something, right? Becoming something. A path. For

24
00:03:38,480 --> 00:03:44,320
probably the best way of putting it, it's a path. So that's what we're describing.

25
00:03:44,320 --> 00:03:52,200
We've talked about these purifications as though they are paths. And so it's

26
00:03:52,200 --> 00:04:02,320
this point where the meditator begins to find the path. Up until this point,

27
00:04:02,320 --> 00:04:11,680
they've really just done a lot of the tool. As I said, they're working out how

28
00:04:11,680 --> 00:04:20,920
to use their tools and begin to finally traverse the path in the last

29
00:04:20,920 --> 00:04:27,280
purification. When they overcome doubt in the last purification, the beginning

30
00:04:27,280 --> 00:04:35,080
meditator is wrestling about what is the right way to go. And then the

31
00:04:35,080 --> 00:04:40,720
meditator realizes or starts to see what is the right way to go. And they

32
00:04:40,720 --> 00:04:46,480
start to go in that direction. So in this purification is about seeing what

33
00:04:46,480 --> 00:04:52,040
leads us along the path, meaning what really frees us from suffering, what

34
00:04:52,040 --> 00:05:03,360
leads us to greater clarity of mind, peace of mind, freedom of mind. And the

35
00:05:03,360 --> 00:05:14,040
meditator begins to discern that which does and that which doesn't. And at the

36
00:05:14,040 --> 00:05:22,320
same time, this is when, well, for whatever reason, this is the time when all of

37
00:05:22,320 --> 00:05:29,040
the fake paths come out. It's basically because the meditator begins to

38
00:05:29,040 --> 00:05:34,920
practice correctly. And through practicing correctly, the meditator becomes

39
00:05:34,920 --> 00:05:43,280
concentrated, focused. There is without all the doubt to tear one apart.

40
00:05:43,280 --> 00:05:56,160
There is a sort of a sort of composure of mind. This is the state that we all

41
00:05:56,160 --> 00:06:01,360
are looking for. Anything of him. Good meditator is composed. Their mind is

42
00:06:01,360 --> 00:06:09,400
strong. So at this point, the meditators mind becomes strong. And as a result, a lot

43
00:06:09,400 --> 00:06:15,880
of new experiences come up. Mostly good ones. The texts talk mainly about the

44
00:06:15,880 --> 00:06:19,600
good ones. And with good reason, because through good practice, good things

45
00:06:19,600 --> 00:06:24,040
come up, it's worth mentioning that for some people, bad things will come up.

46
00:06:24,040 --> 00:06:29,680
It's not as common, but it's worth noting that if, and by bad things here, we

47
00:06:29,680 --> 00:06:34,040
don't mean ordinary, bad things, we mean deep, dark, bad things. You know,

48
00:06:34,040 --> 00:06:39,000
the meditators who have early childhood trauma or that kind of thing. This is

49
00:06:39,000 --> 00:06:43,160
where it might come up, because your mind is finally strong enough to

50
00:06:43,160 --> 00:06:52,080
penetrate deeply into your past. Your mind is focused enough to see, to weed

51
00:06:52,080 --> 00:07:00,560
out all the chaff, all the noise, ordinary bad stuff, ordinary stresses. And then

52
00:07:00,560 --> 00:07:05,120
the deep stresses come up. It's not something to be afraid of, but that's really

53
00:07:05,120 --> 00:07:09,480
the point is that it's something that distracts the meditator. Mostly good

54
00:07:09,480 --> 00:07:18,200
things. Sometimes bad things. But they're usually very new and new in terms of

55
00:07:18,200 --> 00:07:21,600
the meditation practice. They're not something one is dealt with in the

56
00:07:21,600 --> 00:07:27,960
beginning. At this point, new stuff comes up. Very good thing. Sometimes the

57
00:07:27,960 --> 00:07:33,280
meditator feels bliss. If every meditator is different, if you're not feeling

58
00:07:33,280 --> 00:07:36,160
any, if you're not experiencing any of these things, well, it's actually a

59
00:07:36,160 --> 00:07:41,600
bonus because you don't have any distractions. But meditators are distracted by

60
00:07:41,600 --> 00:07:47,360
bliss. They're distracted by calm. They're distracted by lights. Some meditators

61
00:07:47,360 --> 00:07:52,360
see bright lights or they just feel light. Their eyes are closed, but it feels

62
00:07:52,360 --> 00:08:00,800
like the room is very bright. And all these things come from some sort of state

63
00:08:00,800 --> 00:08:05,040
of mind that is evoked through strong concentration. Meditator's

64
00:08:05,040 --> 00:08:11,080
concentration improves. And a lot of much more to do with the nature of their

65
00:08:11,080 --> 00:08:19,000
own mind. A lot of concentrated states come up. For some people, it's bright

66
00:08:19,000 --> 00:08:24,880
lights. For some people, it's bliss. For some people, it's calm, quiet. A lot of

67
00:08:24,880 --> 00:08:30,560
people have states of quiet where there's really nothing. The mind is suddenly

68
00:08:30,560 --> 00:08:37,440
still. For some, it's knowledge. They begin to understand things about their

69
00:08:37,440 --> 00:08:42,400
lives. I've gone through this as a talk. I've given many times some of you

70
00:08:42,400 --> 00:08:49,600
find this stuff familiar, but it's always good to go over it. Knowledge means

71
00:08:49,600 --> 00:08:56,320
they, well, lots of different kinds of knowledge. Confidence. This is the point

72
00:08:56,320 --> 00:08:59,640
where the meditator finally has confidence. The doubt is gone and there's this

73
00:08:59,640 --> 00:09:05,440
upsurge of faith and confidence. Faith in the teacher. This teacher is such a

74
00:09:05,440 --> 00:09:10,280
great teacher. Faith in the teaching. So wow, this teaching is so perfect and

75
00:09:10,280 --> 00:09:18,040
wonderful and great. Faith in the Buddha. Faith in Buddhism. Faith in oneself. I am

76
00:09:18,040 --> 00:09:25,640
such a good meditator. I'm doing it. I'm successful. But the funny thing is

77
00:09:25,640 --> 00:09:31,480
when you start saying that, when you start focusing on that, you're no longer

78
00:09:31,480 --> 00:09:36,160
meditating. You're now a very poor meditator because you're no longer being

79
00:09:36,160 --> 00:09:42,440
mindful. I mean, it's a good sign. These are a sign that, wow, the mind is

80
00:09:42,440 --> 00:09:53,440
really progressing. But this is when the, so because the progress, because of

81
00:09:53,440 --> 00:09:59,400
the progress, good things come, but the real trick at this point and the key

82
00:09:59,400 --> 00:10:04,400
at this point is to be able to say, what is it that's causing the progress?

83
00:10:04,400 --> 00:10:11,720
What is it that is allowing me to progress? And what takes us is an asking of

84
00:10:11,720 --> 00:10:17,960
that question to make one realize, well, no, it's not these positive states

85
00:10:17,960 --> 00:10:22,760
that have suddenly come up. If now I fix and focus on these, I've stopped what I

86
00:10:22,760 --> 00:10:29,240
was doing. I've left the path. So these are good signs, but these are not the

87
00:10:29,240 --> 00:10:40,840
path. They're called byproducts of the practice. And they are problems simply

88
00:10:40,840 --> 00:10:46,960
because they distract. So it's a warning that, yes, good things are going to come.

89
00:10:46,960 --> 00:10:50,360
Many good things that may be wished for. Maybe you thought this would be great

90
00:10:50,360 --> 00:10:56,800
if they could come in my meditation. But happiness is not the cause of

91
00:10:56,800 --> 00:11:03,560
happiness. You don't get more happy just because you wallow in the happiness.

92
00:11:03,560 --> 00:11:09,400
If you want happiness, you need goodness. You need this purity of mind,

93
00:11:09,400 --> 00:11:20,440
mindfulness. So at this stage, the meditator begins to see things as they are.

94
00:11:20,440 --> 00:11:26,280
The meditator begins to practice skillfully for the first time. Those are the

95
00:11:26,280 --> 00:11:37,040
meditators really began to practice as and it's the door to insight.

96
00:11:37,040 --> 00:11:40,920
Right? One is beginning. Okay, practicing correctly, but then practicing

97
00:11:40,920 --> 00:11:45,040
incorrectly. And so it's this stage where the meditator begins to discern

98
00:11:45,040 --> 00:11:49,600
right practice from wrong practice. This is the path. This leads me further.

99
00:11:49,600 --> 00:11:54,960
This is not the path. This is just a distraction. It's important for the teacher

100
00:11:54,960 --> 00:12:00,280
to help the meditator here because it's very easy to get on the wrong path

101
00:12:00,280 --> 00:12:05,080
for quite some time. The meditator might get spend days even on the wrong path.

102
00:12:05,080 --> 00:12:09,560
For some meditators without teachers, if without a teacher they get to this

103
00:12:09,560 --> 00:12:15,440
point. It's quite common for a meditator to spend months or even years on the

104
00:12:15,440 --> 00:12:21,720
wrong path. You know, they'll practice mindfulness, but then they'll switch to

105
00:12:21,720 --> 00:12:27,680
focusing on the effects, the results of mindfulness. And they want to understand

106
00:12:27,680 --> 00:12:34,160
what the next step is. Not confident in the fact that mindfulness is the

107
00:12:34,160 --> 00:12:39,200
next step. Mindfulness of these states. So when you're calm, you should say to

108
00:12:39,200 --> 00:12:46,480
yourself, calm, calm, happy. So it's happy. If you see bright lights, of course,

109
00:12:46,480 --> 00:12:52,080
it's seeing seeing. Even if you feel very quiet or calm, quiet, you would just

110
00:12:52,080 --> 00:13:07,600
say quiet, quiet. Many things, many things, one can become attached to. So it's

111
00:13:07,600 --> 00:13:11,880
at this point where the meditator really has to assess. You need to be

112
00:13:11,880 --> 00:13:16,960
monks to step back and to assess your practice and say, hey, I'm not really being

113
00:13:16,960 --> 00:13:23,800
mindful of that. Here's something I'm not being mindful of. And refine your

114
00:13:23,800 --> 00:13:28,840
practice. This isn't really refining at this point. It's more just deciding.

115
00:13:28,840 --> 00:13:35,080
This is right. This is wrong. This is the way this is not the way. Sitting around,

116
00:13:35,080 --> 00:13:39,960
feeling happy all the time. Unfortunately, isn't the way to attain freedom from

117
00:13:39,960 --> 00:13:47,400
suffering? Because it's a product that's not a cause. It's an important point,

118
00:13:47,400 --> 00:13:53,920
right? It's a problem with societies, we think, don't worry, be happy. As though

119
00:13:53,920 --> 00:13:59,600
happiness could solve your problems, it unfortunately can. Because happiness

120
00:13:59,600 --> 00:14:05,680
doesn't lead to happiness. Happiness isn't the cause of happiness. Of course,

121
00:14:05,680 --> 00:14:12,400
not worrying is a good cause of happiness, but it's different. So if you don't

122
00:14:12,400 --> 00:14:18,720
worry, then you will be happy. Of course, by worry, we include all and all

123
00:14:18,720 --> 00:14:30,080
some states. If you stop worrying, if you stop getting angry or being afraid,

124
00:14:30,080 --> 00:14:36,080
if you stop wanting, if you stop being addicted to things, then yes, you'll be

125
00:14:36,080 --> 00:14:41,280
happy. But it's not about just being happy or just focusing on good states. And

126
00:14:41,280 --> 00:14:47,400
the same with bad states. Obsessing over bad states, delving into them, bringing

127
00:14:47,400 --> 00:14:53,040
them up again and again is also not the way. So this is the dark side. It can

128
00:14:53,040 --> 00:14:57,240
happen that at this point, bad things come up, things that are meditators held

129
00:14:57,240 --> 00:15:02,480
inside. They're not a problem. They're only a problem if we think that they're

130
00:15:02,480 --> 00:15:06,560
a problem. If we get upset about them, if we get worried about them, distracted by

131
00:15:06,560 --> 00:15:12,480
them, frustrated by them, attached to them. They're just phenomena. They're

132
00:15:12,480 --> 00:15:19,160
thoughts. They're emotions. They just come up. All of the emotions that exist in

133
00:15:19,160 --> 00:15:26,600
the mind are just that emotions. They can only hurt us if we give them the

134
00:15:26,600 --> 00:15:39,040
power to. Nothing is a problem in the practice. Nothing can block us from

135
00:15:39,040 --> 00:15:48,720
practicing because everything can be an object of mindfulness. So there you go,

136
00:15:48,720 --> 00:15:58,240
briefly talking about this latest, the next stage in the path of

137
00:15:58,240 --> 00:16:10,320
purification. It's the demo for tonight to thank you all for coming up. We're

138
00:16:10,320 --> 00:16:19,680
going to get the question page open. Pressure on the IT team. Questions aren't

139
00:16:19,680 --> 00:16:28,720
being answered because I can't access the questions page. They're also busy.

140
00:16:28,720 --> 00:16:35,240
Anybody wants to join the IT team? We can always use more volunteers.

141
00:16:35,240 --> 00:16:45,240
Figure out why this, why I'm all I'm seeing is the wheel. Turning and turning and turning.

142
00:16:45,240 --> 00:17:07,320
It's not right. We live in an age of instant gratification or my questions. Let's go

143
00:17:07,320 --> 00:17:13,600
see what's happening on YouTube. Hello, YouTube. We've got people saying thank you.

144
00:17:13,600 --> 00:17:24,240
That's nice. Oh, I've got to talk to Robin. I keep forgetting. Robin, I need to

145
00:17:24,240 --> 00:17:31,920
talk to you after this. Can we do a Google Hangout? I've got business to take

146
00:17:31,920 --> 00:17:41,720
care with my board of directors. So I'm going to use that as an excuse to not

147
00:17:41,720 --> 00:17:48,520
wait for this thing to start to finish loading and it's not loading. So good night

148
00:17:48,520 --> 00:18:18,360
everyone. Thank you all for tuning in.

