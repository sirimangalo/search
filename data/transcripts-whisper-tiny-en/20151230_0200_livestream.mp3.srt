1
00:00:00,000 --> 00:00:27,960
Good evening everyone, we're broadcasting live December 29th, 2015, we had a little

2
00:00:27,960 --> 00:00:45,240
over two days left in the year. I don't worry, there's another year to come. I had an

3
00:00:45,240 --> 00:01:01,480
argument with my youngest brother about rebirth. Stubborn. He said...

4
00:01:15,240 --> 00:01:33,960
I can't remember. We'll see who's right. We'll see who's right. It's going to be a

5
00:01:33,960 --> 00:01:43,080
shame for him if there are consequences to his actions and he can't escape

6
00:01:43,080 --> 00:01:54,720
them by dying. And he said, well that'll be... Yeah, and if I'm right, you'll have to

7
00:01:54,720 --> 00:02:04,640
deal with the consequences if I'm right. If you're right, there are no consequences.

8
00:02:04,640 --> 00:02:08,840
If you're right, it'd be great. It means we can do whatever we want, but it's quite

9
00:02:08,840 --> 00:02:20,600
a dangerous, quite a dangerous belief. The belief in death is a dangerous belief

10
00:02:20,600 --> 00:02:28,920
because of the consequences of it. Even in this life, the consequences are people are less

11
00:02:28,920 --> 00:02:45,800
likely to do good deeds, more likely to do evil deeds. Even if it was true, it would be better

12
00:02:45,800 --> 00:02:53,640
for us to believe that there were consequences. It would be better for the world. That's a

13
00:02:53,640 --> 00:03:00,200
funny situation. It's actually if it were true that when we died, there was nothing. It would

14
00:03:00,200 --> 00:03:08,320
still be better for all of us and for the world and for society, for us to believe that

15
00:03:08,320 --> 00:03:22,680
there were consequences. And so we argued and then right before I left this morning,

16
00:03:22,680 --> 00:03:29,680
he said, so we'll have a safe travels. And I said, thank you. And I said, so I'll see you.

17
00:03:29,680 --> 00:03:48,000
And I said, if not in this life, then I'm in the next. And we laughed. So I had one

18
00:03:48,000 --> 00:03:55,800
announcement today that seems a lot of people didn't know about the appointment schedule

19
00:03:55,800 --> 00:04:03,200
that we have. So if you go to meditation.ceremungalow.org, there's a meat page and it's got

20
00:04:03,200 --> 00:04:10,160
slots where you can sign up for a meditation course. And we use Google Hangouts. So you

21
00:04:10,160 --> 00:04:19,440
have to make sure you've got Google Hangouts installed and working. And then if you show

22
00:04:19,440 --> 00:04:32,560
up at the right time, you can meet with me. But you must be practicing at least an hour

23
00:04:32,560 --> 00:04:42,760
of meditation per day. Let me try to get you up to two hours. And you have to keep

24
00:04:42,760 --> 00:04:46,800
Buddhist precepts and stuff. But it's for people who are serious about the meditation

25
00:04:46,800 --> 00:04:56,240
practice and want to go the next, take the next step. Right now we have nine thoughts

26
00:04:56,240 --> 00:05:17,720
available. So still some more. I just got back from Florida today. So I'm not planning

27
00:05:17,720 --> 00:05:25,520
on sticking around too long this evening. Make a short night of it. Do you have any questions?

28
00:05:25,520 --> 00:05:33,840
We have questions. We have lots of questions on there. Let's skip to the good ones. Okay.

29
00:05:33,840 --> 00:05:37,840
Regarding meetings, is it required to have a camera and a microphone to make an individual

30
00:05:37,840 --> 00:05:44,280
meeting? I have social anxiety. So it's very difficult on me. And my English is sad too.

31
00:05:44,280 --> 00:05:50,520
Thanks. Yeah, we can do it just. You need a microphone absolutely. But I'd like to have

32
00:05:50,520 --> 00:06:00,480
a camera. I think it's important because normally you would come into the room and meet

33
00:06:00,480 --> 00:06:06,400
me. And we're trying to make it as close to that as possible. So I really would like

34
00:06:06,400 --> 00:06:18,000
for you to have a camera. Every time I do sitting meditation, I seem to keep dropping

35
00:06:18,000 --> 00:06:23,040
off to sleep. This can be any time of day apart from walking meditation. What can I do

36
00:06:23,040 --> 00:06:28,840
to help with my sitting meditation? Well, walking meditation is good. What's wrong with

37
00:06:28,840 --> 00:06:40,200
that? I think they're the mindful tired, tired helps, but probably has a lot to do with

38
00:06:40,200 --> 00:06:47,640
your lifestyle. You have to put up with falling asleep because of the state your mind

39
00:06:47,640 --> 00:06:56,360
is in.

40
00:06:56,360 --> 00:07:01,080
During sitting meditation, I find it hard to knit thoughts that arise in space quickly while

41
00:07:01,080 --> 00:07:06,440
I'm noting primary objectives. However, I am aware that this happens and it is interrupting

42
00:07:06,440 --> 00:07:11,080
my concentration. How it happens is that while I'm waiting for the rising of our old

43
00:07:11,080 --> 00:07:17,240
manifest and cease, and I will go on noting the rising, my question is, should I in this case

44
00:07:17,240 --> 00:07:23,360
be noting thinking, thinking, thinking after the fact or should I know distracted? If so,

45
00:07:23,360 --> 00:07:27,400
how many times should I know before returning to my objectives?

46
00:07:27,400 --> 00:07:36,400
You can. Just once. Where you can ignore it's knowing a couple of times. There's no,

47
00:07:36,400 --> 00:07:43,760
it's not magic or something. Just do it as it seems appropriate. Knowing would be knowing

48
00:07:43,760 --> 00:07:50,480
that you were thinking.

49
00:07:50,480 --> 00:07:54,560
There's some time of day in which meditation is best to be practiced in in the morning

50
00:07:54,560 --> 00:08:06,880
of Christence. How would you convince a non-bloodus that reincarnation is real? Is there

51
00:08:06,880 --> 00:08:12,200
a way one can know who one who one was in a past life? And thank you so much for taking

52
00:08:12,200 --> 00:08:14,240
the time to answer.

53
00:08:14,240 --> 00:08:16,680
Yeah, I'm going to skip that one.

54
00:08:16,680 --> 00:08:21,880
Okay. Sounds like you've already had that. I just wanted to say about consequences. You don't

55
00:08:21,880 --> 00:08:26,720
even have to wait until the next life for the consequences. You feel it right now when you

56
00:08:26,720 --> 00:08:27,720
try to do anything.

57
00:08:27,720 --> 00:08:32,560
Yeah, I was arguing that with him and he said, I get that. He said, my younger brother said,

58
00:08:32,560 --> 00:08:39,920
he knows that, but he said, but and then I said, but do you think that there are no

59
00:08:39,920 --> 00:08:58,560
consequences in the end when you die? You'll get that scot-free and you said, yeah.

60
00:08:58,560 --> 00:09:03,440
When I'm sitting I'm not sitting sitting, but how far do I need to go? Do I need to be

61
00:09:03,440 --> 00:09:08,880
mindful of the pressure below my butt or just today or just know that I'm sitting? Is

62
00:09:08,880 --> 00:09:14,560
sitting a concept? When my abdomen is rising, I'm not rising, rising, but no, do I need

63
00:09:14,560 --> 00:09:19,280
to follow the rising of the abdomen at every position? Thanks.

64
00:09:19,280 --> 00:09:24,120
Yeah, you should follow the rising from beginning to end. You should not just say, and

65
00:09:24,120 --> 00:09:33,240
it's not just words in your head. Sitting as well, sitting is an expression of the sensations.

66
00:09:33,240 --> 00:09:37,880
So yeah, you should be aware, but you don't have to focus on it. The only reason you know

67
00:09:37,880 --> 00:09:51,160
that you're sitting is because of the sensations in your back and in your bottom.

68
00:09:51,160 --> 00:09:59,080
Is there a difference between mindfulness achieved or meditation and mindfulness stirring

69
00:09:59,080 --> 00:10:05,160
every day activities? Is mindfulness achieved in formal meditation practice more beneficial?

70
00:10:05,160 --> 00:10:13,080
Thank you, Monteh. Mindfulness is mindfulness.

71
00:10:13,080 --> 00:10:21,560
Hello, Monteh. I'd like to start off by saying thank you for all your teeth and dedication.

72
00:10:21,560 --> 00:10:26,080
They truly have helped me put things in better perspective. I'll be taking a couple of college

73
00:10:26,080 --> 00:10:31,960
classes in a city called Sioux Falls, South Dakota, starting February 2016, and thinking

74
00:10:31,960 --> 00:10:37,000
of forming a small meditation group on campus with interested individuals. If you don't

75
00:10:37,000 --> 00:10:41,360
be willing to meet with us for that hour or less once a week via Skype or another form

76
00:10:41,360 --> 00:10:48,640
of video communication, I think that was a question. I'm not certain that the number

77
00:10:48,640 --> 00:10:52,160
of people who will be interested in such a group, but if any, it would be wonderful

78
00:10:52,160 --> 00:11:02,120
to have you guide us along this path to freedom from suffering. So I think the question

79
00:11:02,120 --> 00:11:13,840
was would you be potentially able to do such a thing? I don't know. Probably not.

80
00:11:13,840 --> 00:11:24,360
I'm doing a lot already. You just go, you know, go according to the booklet and people

81
00:11:24,360 --> 00:11:32,200
want to learn more. They can contact me. They can do a course.

82
00:11:32,200 --> 00:11:39,600
I mean, it's great that you're doing it. I want to encourage you in it, but I don't think

83
00:11:39,600 --> 00:11:45,200
it'd be. I'm already doing too much.

84
00:11:45,200 --> 00:11:52,320
I thought they had the job interview on one week. I started a lot going to talk and have

85
00:11:52,320 --> 00:11:57,280
difficulty to agree. Do you have any advice besides noting? And is it a good thing to

86
00:11:57,280 --> 00:12:02,360
imagine the interview beforehand and observe my reaction? Thanks.

87
00:12:02,360 --> 00:12:10,320
No, I mean, I teach meditation, so you know what I've got to offer. If it helps, take

88
00:12:10,320 --> 00:12:15,640
it. If it doesn't help, leave it.

89
00:12:15,640 --> 00:12:35,360
I guess this is a question. Do you know that you're just the best? That was a compliment.

90
00:12:35,360 --> 00:12:48,480
Is it better to meditate in a group rather than alone? If so, why? Thank you, Monte.

91
00:12:48,480 --> 00:12:55,320
You're always alone. You're never in a group. But much of meditation is an artifice.

92
00:12:55,320 --> 00:13:03,080
It's artificial. So there's lots of artificial support for meditation. And one of them is

93
00:13:03,080 --> 00:13:11,320
having a teacher, being in a group. These are artificial support. So yeah, I think they can

94
00:13:11,320 --> 00:13:17,440
support your meditation, provide encouragement and so on. Lots of things that can support

95
00:13:17,440 --> 00:13:25,000
your meditation.

96
00:13:25,000 --> 00:13:29,200
And sitting every day and making progress. Is there any benefit in doing your one-on-one

97
00:13:29,200 --> 00:13:33,840
course? Or is it just for those who need advice? Thank you.

98
00:13:33,840 --> 00:13:41,440
Well, if you're just practicing according to my booklet, that's only the first step.

99
00:13:41,440 --> 00:13:48,880
So yeah, we take it through the steps and we guide you through the course. I would say

100
00:13:48,880 --> 00:14:01,040
there's a lot of benefit in taking an online course. What's in the book that is just

101
00:14:01,040 --> 00:14:20,800
the very beginning? And with that, we've made it through all the questions.

102
00:14:20,800 --> 00:14:33,240
Tomorrow, we might do Dhamapada, probably. Should be okay. And on Thursday, we have the

103
00:14:33,240 --> 00:14:45,160
last day of the year. I'll be here. We'll see who shows up.

104
00:14:45,160 --> 00:14:48,440
Thanks for your muffins, Monte. What?

105
00:14:48,440 --> 00:14:57,120
Thanks for your muffins, Monte. Are you giving out muffins? No. Okay.

106
00:14:57,120 --> 00:15:07,040
Muffins. So I want to thank you for your muffins. I don't get it. You don't get

107
00:15:07,040 --> 00:15:22,600
that. I think it's a joke. Anyway, good night everyone. Good night, Monte.

108
00:15:22,600 --> 00:15:51,680
It's got tomorrow. Thank you. Thank you, Robby.

