1
00:00:00,000 --> 00:00:08,680
Hello guys, when meditating I sometimes focus on feelings of static or blood flow in the

2
00:00:08,680 --> 00:00:16,880
head, skin and eyes along with breath. Is this an accepted practice?

3
00:00:16,880 --> 00:00:31,640
It's not what do you focus on, it's how you focus on it and that much more determines

4
00:00:31,640 --> 00:00:42,120
how accepted it is. So we only accept objective awareness of objects, so focus isn't enough

5
00:00:42,120 --> 00:00:48,120
because you can focus on something and still be subjective about it. It's still be attached

6
00:00:48,120 --> 00:00:57,880
to it. I couldn't see even regards to it proliferate or extrapolate upon it, project,

7
00:00:57,880 --> 00:01:09,440
give rise to projections about it. So all of those things are perfectly valid objects of

8
00:01:09,440 --> 00:01:15,800
meditation, but are you actually meditating? Because simply focusing is not enough. The

9
00:01:15,800 --> 00:01:21,280
key is what we call set the, so it's how you understand the word set the. If you want,

10
00:01:21,280 --> 00:01:29,800
I talked in a few places in a book, I have a book now, isn't it funny? Very diculous. I wrote

11
00:01:29,800 --> 00:01:37,840
a book for people who helped me write a book. Practical, what is it called? Lessons

12
00:01:37,840 --> 00:01:47,080
in practical Buddhism? No, not my tablet. That's crazy. I think I should take it down.

13
00:01:47,080 --> 00:01:55,880
I'm so embarrassing to have a book. People putting my book on a tablet, that's almost

14
00:01:55,880 --> 00:02:03,280
scary. But yeah, I think there's some, you know, I am happy about some of the things

15
00:02:03,280 --> 00:02:07,640
that are in there, not all of it, but it's going to take more work. But some of the

16
00:02:07,640 --> 00:02:11,800
things that are in there are what I wanted to say. So some of the things in there, but

17
00:02:11,800 --> 00:02:17,760
set the could help you. I think it could really help people understand this from my

18
00:02:17,760 --> 00:02:24,720
point of view from our tradition. What do we mean and why we mean? What do we mean about

19
00:02:24,720 --> 00:02:28,720
the learned set date about mindfulness?

20
00:02:28,720 --> 00:02:43,080
Yeah, the breath is a good example. I think the breath, you have to be careful because

21
00:02:43,080 --> 00:02:46,960
well, not exactly careful, but you have to distinguish between the conceptual breath and

22
00:02:46,960 --> 00:02:52,680
the reality, because the breath doesn't exist. It's not a reality. The reality is the

23
00:02:52,680 --> 00:03:03,000
experience of heat, cold, pressure, the acidity in the body. All of these experiences arise

24
00:03:03,000 --> 00:03:07,080
in seaset, a single location. So the breath doesn't come into the body. That's not

25
00:03:07,080 --> 00:03:36,920
an ultimate experience. Ultimate reality is a momentary experience at some point in the body.

