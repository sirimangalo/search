1
00:00:00,000 --> 00:00:01,800
That way this goes.

2
00:00:01,800 --> 00:00:03,800
They feel like flattering you.

3
00:00:03,800 --> 00:00:06,880
By flattering you, you.

4
00:00:06,880 --> 00:00:10,680
Well, even just flattering you, that makes them a good friend.

5
00:00:14,320 --> 00:00:17,160
So they'll praise your good deeds, they'll praise your evil.

6
00:00:17,160 --> 00:00:19,320
It's the good things you do, they'll praise.

7
00:00:19,320 --> 00:00:21,760
This is very dangerous.

8
00:00:21,760 --> 00:00:26,280
It's clearly a sign of some mental imbalance or some problem.

9
00:00:26,280 --> 00:00:30,360
They might even recognize this in yourselves. We do this sometimes.

10
00:00:30,360 --> 00:00:35,720
Flattering others, feeling that it makes us a better friend.

11
00:00:35,720 --> 00:00:39,960
Feeling that somehow that'll make them like us more.

12
00:00:39,960 --> 00:00:41,880
It doesn't mean you shouldn't compliment people.

13
00:00:41,880 --> 00:00:48,080
In fact, it is a great thing to appreciate other people's good deeds.

14
00:00:48,080 --> 00:00:50,720
But it should only be their good deeds, right?

15
00:00:50,720 --> 00:00:54,080
You know you're a flatter or you know someone's a flatter when they

16
00:00:54,080 --> 00:00:57,080
praise the bad things you do.

