1
00:00:00,000 --> 00:00:29,000
Okay, good evening everyone, broadcasting live August 19th.

2
00:00:29,000 --> 00:00:48,000
We got a quote about the concept of triage, the idea that when you can't help everyone

3
00:00:48,000 --> 00:00:53,000
you have to prioritize.

4
00:00:53,000 --> 00:00:59,000
And you prioritize by making three groups. Robin, would you like to read the?

5
00:00:59,000 --> 00:01:02,000
Sure. Sure.

6
00:01:02,000 --> 00:01:07,000
There are three types of sick people to be found in the world. What three?

7
00:01:07,000 --> 00:01:13,000
There is the sick person who, whether or not he obtains the proper diet, medicines and nursing,

8
00:01:13,000 --> 00:01:19,000
will not recover from his illness. Then there is the sick person who, whether or not he obtains the proper diet,

9
00:01:19,000 --> 00:01:25,000
medicine and nursing will recover from his sickness anyway. And lastly, there is the sick person

10
00:01:25,000 --> 00:01:31,000
who will recover from his illness only if he gets the proper diet, medicines and nursing.

11
00:01:31,000 --> 00:01:39,000
It is for this last type that proper diet, medicine and nursing should be prescribed, but the other should be looked after also.

12
00:01:39,000 --> 00:01:45,000
Now, there are three types of persons in the world who can be compared to the three types of sick persons,

13
00:01:45,000 --> 00:01:51,000
which three. There is the person who, whether or not he gets the chance of seeing that tagta,

14
00:01:51,000 --> 00:01:56,000
and learning the dhammah in discipline will not enter the perfection of things that are skillful.

15
00:01:56,000 --> 00:02:03,000
Again, there is the person who, whether or not he gets the chance of seeing that tagta and learning the dhamma in discipline,

16
00:02:03,000 --> 00:02:10,000
will enter the perfection of things that are skillful and again, there's the person who will enter the perfection of things that are skillful,

17
00:02:10,000 --> 00:02:14,360
only if he gets the chance of seeing the tatakata and learning the dhamma

18
00:02:14,360 --> 00:02:18,240
and discipline. It is an account of this last person that the dhamma is

19
00:02:18,240 --> 00:02:23,200
proclaimed but it should be taught to others also.

20
00:02:23,800 --> 00:02:27,000
Thank you.

21
00:02:28,000 --> 00:02:34,920
So this is actually a well-known concept as I understand in more

22
00:02:34,920 --> 00:02:40,920
for wartime medics when there's a catastrophe or so on.

23
00:02:41,920 --> 00:02:45,920
It actually is a thing where you have to do what is called triage, triage,

24
00:02:45,920 --> 00:02:51,920
tri meaning three, you separate into three groups.

25
00:02:51,920 --> 00:03:01,920
Those who you can help and those who don't need your help and those who you can't help.

26
00:03:01,920 --> 00:03:10,920
Or those for whom the help is life is critical to their ability to live.

27
00:03:10,920 --> 00:03:13,920
That's the group that you're targeting.

28
00:03:13,920 --> 00:03:19,920
You don't target those who have injuries but won't die from them.

29
00:03:19,920 --> 00:03:24,920
You have to go for those who are life-threatening and those that are going to die

30
00:03:24,920 --> 00:03:27,920
even if you help them, you don't help them.

31
00:03:27,920 --> 00:03:37,920
Or you prioritize accordingly and I think that's the point here is priority.

32
00:03:37,920 --> 00:03:44,920
So yes, you teach all but there is a sense that priority should be put on those who both need it

33
00:03:44,920 --> 00:03:46,920
and will benefit from it.

34
00:03:46,920 --> 00:03:59,920
It's brought this up before in relation to teaching disabled people,

35
00:03:59,920 --> 00:04:12,920
mentally disabled children or people teaching those people who are not easily able to understand

36
00:04:12,920 --> 00:04:17,920
the dumber, even those people who don't want to hear the dumber.

37
00:04:17,920 --> 00:04:31,920
And it's a matter of, they call cost benefit ratio because theoretically you want to just help.

38
00:04:31,920 --> 00:04:35,920
You do as much as you can to help.

39
00:04:35,920 --> 00:04:47,920
But the question is whether you're actually the benefit that's actually going to come from the work that you do.

40
00:04:47,920 --> 00:04:55,920
And part of helping is assessing that benefit, where that benefit comes into play.

41
00:04:55,920 --> 00:05:01,920
In all aspects of the cultivation of goodness that would encourage what we call,

42
00:05:01,920 --> 00:05:07,920
we monks, which means discrimination, not just doing, doing, doing.

43
00:05:07,920 --> 00:05:10,920
You have to adjust accordingly.

44
00:05:10,920 --> 00:05:14,920
And at the Pindika came to the Buddha and said that he discriminated.

45
00:05:14,920 --> 00:05:21,920
And when he gave gifts, he was careful to consider who deserved the gift

46
00:05:21,920 --> 00:05:23,920
and not just give indiscriminately.

47
00:05:23,920 --> 00:05:24,920
And the Buddha praised him.

48
00:05:24,920 --> 00:05:27,920
He said, yes, that's the way in the past.

49
00:05:27,920 --> 00:05:37,920
Why is men did the same? And this is the proper way to cultivate goodness with discrimination.

50
00:05:37,920 --> 00:05:50,920
So this is one reason why I usually have to answer that you're better off helping those people who are best able to understand the teachings.

51
00:05:50,920 --> 00:05:57,920
You can avoid those who are already on the right path, to some extent.

52
00:05:57,920 --> 00:06:06,920
But those people, the most important, the highest priority should be put on those people who maybe haven't even heard the teachings.

53
00:06:06,920 --> 00:06:17,920
But regardless are those who are capable of progress, who have not yet progressed, but are readily capable.

54
00:06:17,920 --> 00:06:20,920
Because you do so, you put forth such little effort.

55
00:06:20,920 --> 00:06:33,920
And you get the most benefit, because those people in turn, those beings who have gained their by, become the army, the next generation, they spread the teaching.

56
00:06:33,920 --> 00:06:45,920
Whereas if you spend your whole life helping people who are not going to be able to turn around and then help others, you have had a very little impact on the world.

57
00:06:45,920 --> 00:07:02,920
And part of this is the idea that having helped those who are easily benefited from the teachings, those people then turn around and benefit others, who are maybe less ready.

58
00:07:02,920 --> 00:07:19,920
And so it's also a ripple effect, like a trickle down effect, more like a ripple effect, helping those who are best deserving of it, are not deserving but easiest to teach.

59
00:07:19,920 --> 00:07:34,920
So I mean, a big part of spreading Buddhism in the modern age over the Internet, an exciting aspect of it is how easy it is to get the word out.

60
00:07:34,920 --> 00:08:03,920
Because you start to realize that a great many people out there are simply lacking the knowledge of what is the right path, how to practice properly to see things clearly.

61
00:08:03,920 --> 00:08:10,920
And just the basic knowledge and understanding of what the Buddha taught.

62
00:08:10,920 --> 00:08:16,920
And if they had that, they would be able to progress quickly.

63
00:08:16,920 --> 00:08:31,920
And so just by putting the information out there, you often get feedback to the tune of people changing their lives, just from hearing things that they had never heard, just from learning.

64
00:08:31,920 --> 00:08:40,920
Simple things that they had never learned.

65
00:08:40,920 --> 00:08:47,920
So that's all this is a useful quote.

66
00:08:47,920 --> 00:09:03,920
And one couple of points is the first is that this quote makes reminds us that not everybody who hears the Buddha's teaching is going to understand it is going to benefit from it.

67
00:09:03,920 --> 00:09:16,920
Our benefit or not is going not everyone who listens to the Buddha's teaching. Here's the Buddha's teaching, learns the Buddha's teaching is going to realize the truth of it is going to become enlightened from it.

68
00:09:16,920 --> 00:09:23,920
The second point is that not everybody who is going to become enlightened has to follow the teachings of a Buddha.

69
00:09:23,920 --> 00:09:28,920
They can become what we would call a Buddha for themselves by themselves.

70
00:09:28,920 --> 00:09:40,920
So the idea that Buddhism is Buddhist, it doesn't hold, that you have to be a Buddhist to become enlightened or so on.

71
00:09:40,920 --> 00:09:46,920
And on the other hand, just being a Buddhist, the idea that that's enough following the Buddha's teaching.

72
00:09:46,920 --> 00:09:51,920
It's not enough. Some people will never put forth the effort and don't want to be such a person.

73
00:09:51,920 --> 00:10:01,920
The Buddha said it's like a spoon, such people like a spoon that never tastes the soup, even though the spoon can be in the soup.

74
00:10:01,920 --> 00:10:12,920
For a thousand years, for a thousand years, no matter how long the spoon is in the soup, it will never taste the flavor of the soup.

75
00:10:12,920 --> 00:10:19,920
So that's the quote tonight.

76
00:10:19,920 --> 00:10:28,920
Anybody have any questions?

77
00:10:28,920 --> 00:10:36,920
Looks like our viewers are chatting using the chat box to chat.

78
00:10:36,920 --> 00:10:40,920
There's a simple question.

79
00:10:40,920 --> 00:10:42,920
You have a question?

80
00:10:42,920 --> 00:10:52,920
What is the difference between the preceptor and the teacher?

81
00:10:52,920 --> 00:10:55,920
Right. Okay.

82
00:10:55,920 --> 00:10:58,920
So let me break it down.

83
00:10:58,920 --> 00:11:04,920
A monk who ordains, and this is going for the male monks, for females, I think it's a little bit different.

84
00:11:04,920 --> 00:11:11,920
The numbers might be a little bit different, but I think it's the same, close to the same.

85
00:11:11,920 --> 00:11:18,920
Now, there's a little bit of technical differences, but anyway, going by the male monks.

86
00:11:18,920 --> 00:11:26,920
For the first five years, you're what's called a nawakat, which means you have to stay with what we call a teacher.

87
00:11:26,920 --> 00:11:32,920
After five years, you are allowed to live without a teacher.

88
00:11:32,920 --> 00:11:41,920
After 10 years, you can become qualified to be either a teacher or a preceptor.

89
00:11:41,920 --> 00:11:48,920
Now, preceptor is the person who oversaw the ordination.

90
00:11:48,920 --> 00:11:52,920
So it's someone who is qualified to oversee the ordination.

91
00:11:52,920 --> 00:11:57,920
They are also considered to be that monk's default teacher.

92
00:11:57,920 --> 00:12:04,920
So any time they are in the same monastery with that monk, that monk is also their teacher.

93
00:12:04,920 --> 00:12:19,920
Now, this teacher, the only meaning there is that they fulfill the obligation.

94
00:12:19,920 --> 00:12:27,920
I don't know exactly how you'd say this. They feel that they're a requirement of the new monk.

95
00:12:27,920 --> 00:12:33,920
So a monk who's under five years as a monk has to stay in the same monastery with such a monk.

96
00:12:33,920 --> 00:12:38,920
Now, the preceptor automatically qualifies and automatically becomes that.

97
00:12:38,920 --> 00:12:43,920
So if I'm a new monk, then I go to the monastery where my teacher is.

98
00:12:43,920 --> 00:12:48,920
I don't have to do anything immediately. They are my teacher.

99
00:12:48,920 --> 00:12:56,920
If I leave, if and when I leave my teacher's monastery or my teacher leaves the monastery,

100
00:12:56,920 --> 00:13:03,920
I have to find, if I'm still under five years as a monk, I have to find a monk who can act as what we call the teacher.

101
00:13:03,920 --> 00:13:07,920
It doesn't mean that they actually have to do any teaching, though that's assumed.

102
00:13:07,920 --> 00:13:19,920
The meaning is that they are the, what do you call the person who, the officer in charge and the person who oversees that monk.

103
00:13:19,920 --> 00:13:26,920
So you have to go and find such a monk who's at least 10 years a monk and fulfills other basic qualifications of being a good monk.

104
00:13:26,920 --> 00:13:35,920
And it requests that it request to take dependence, what we call dependence on the teacher.

105
00:13:35,920 --> 00:13:40,920
So if it's not your preceptor, you actually have to go to them and request it and they have to accept.

106
00:13:40,920 --> 00:13:44,920
And then you consider that they're your teacher.

107
00:13:44,920 --> 00:13:52,920
So it doesn't technically have anything to do with teaching, though that's the role that you put them in.

108
00:13:52,920 --> 00:13:58,920
It's the role of being the supervisor to store.

109
00:13:58,920 --> 00:14:11,920
And so they stay your teacher until you leave them or until your preceptor comes back, in which case immediately the preceptor becomes fulfills that quality requirement.

110
00:14:11,920 --> 00:14:24,920
Okay, so the preceptor is the first teacher or are there some preceptors who just ordain people and then the ordained person immediately finds a different teacher.

111
00:14:24,920 --> 00:14:33,920
So then you never really call the preceptor a teacher, they're called the preceptor, the Upajaya, but they fulfill the requirement.

112
00:14:33,920 --> 00:14:37,920
And they fulfill it, they override any other teacher.

113
00:14:37,920 --> 00:14:42,920
So yes, right away they are, they fulfill that requirement.

114
00:14:42,920 --> 00:14:46,920
And you can't take and teach her as long as you're with your preceptor.

115
00:14:46,920 --> 00:14:54,920
You can denounce your preceptor, you can leave him and say, you no longer want that person to be your supervisor.

116
00:14:54,920 --> 00:14:58,920
And your preceptor can reject you.

117
00:14:58,920 --> 00:15:07,920
There are grounds for this, if you're misbehaving, if your preceptor is a loser, you can reject each other.

118
00:15:07,920 --> 00:15:21,920
In which case, I'm not sure there's ways in which it works, and then you can get them back afterwards, but yeah, that's a whole other complex issue.

119
00:15:21,920 --> 00:15:38,920
Okay, thank you, Bhante.

120
00:15:38,920 --> 00:15:40,920
We have a question.

121
00:15:40,920 --> 00:15:56,920
What is the difference between Chita and Dhamma?

122
00:15:56,920 --> 00:16:01,920
Chita refers to the mental activity specifically.

123
00:16:01,920 --> 00:16:15,920
Dhamma is a unique category among the four, and it's hard to pin down, but the best I've done is to explain it as the teachings of the Buddha.

124
00:16:15,920 --> 00:16:31,920
So, you have these three main aspects of experience, and then you have sort of a directed, so those ones are objective and they're neutral.

125
00:16:31,920 --> 00:16:40,920
But then you have the steering of the mind towards enlightenment, and that involves the Dhamma.

126
00:16:40,920 --> 00:16:52,920
The Dhamma is that come into play in the practice. The first one is the hindrances, and you've got, and I can never remember the order, the senses, and the anger gets.

127
00:16:52,920 --> 00:16:56,920
I think that's that order. The sixth sense is the five aggregates.

128
00:16:56,920 --> 00:17:15,920
You've got the boat-jungers, four-numbled truths, a full-numbled path, I can never remember exactly the enumeration, but it's the, you could say, it's the teachings, the Dhamma of the Buddha.

129
00:17:15,920 --> 00:17:44,920
Because all four of them are Dhamma, the body is a Dhamma, feelings are a Dhamma, the mind is a Dhamma, but this specifically number four here refers to the, the aspects of the path.

130
00:17:44,920 --> 00:17:57,920
In relation to the Buddha, the path of the Buddha.

131
00:17:57,920 --> 00:18:00,920
Ready for another question?

132
00:18:00,920 --> 00:18:10,920
Did you suspect Venerable Ajahn Tang was on our hunt when you first met him, or did you take, or did it take time, and if so, how much time?

133
00:18:10,920 --> 00:18:17,920
I hope said I suspect that Ajahn Tang is on our hunt.

134
00:18:17,920 --> 00:18:19,920
Did I say that?

135
00:18:19,920 --> 00:18:25,920
I don't think so. You always speak very, very glowingly of him though.

136
00:18:25,920 --> 00:18:28,920
He's obviously a very special person.

137
00:18:28,920 --> 00:18:36,920
He is a very special person. I will go on record at saying that.

138
00:18:36,920 --> 00:18:46,920
Now mind you, a lot of people do say things like he's in our hunt, but I always wonder how they would possibly know.

139
00:18:46,920 --> 00:18:56,920
Is it considered not proper to say something like that?

140
00:18:56,920 --> 00:19:07,920
Yeah, I mean moreover we're not allowed to.

141
00:19:07,920 --> 00:19:12,920
I often feel like out of sync with the things that arise in my mind.

142
00:19:12,920 --> 00:19:24,920
As soon as I notice I'm not rising, falling, I've seen or heard three things in the past, or my stepping left, or rising is occurring.

143
00:19:24,920 --> 00:19:33,920
Slightly before the action. How can I delimit the present and be mindful now, not focused in the past or the future?

144
00:19:33,920 --> 00:19:36,920
What does it mean to say I've seen or heard three things?

145
00:19:36,920 --> 00:19:51,920
I think he means by the time he realizes that he's not mindful of the rising and falling he's had a bunch of other thoughts going on.

146
00:19:51,920 --> 00:19:55,920
Yeah, well it's practice. It's not something you can control.

147
00:19:55,920 --> 00:19:58,920
What you're seeing is the non-self nature of the mind.

148
00:19:58,920 --> 00:20:07,920
And our practice has to accord with that. Our practice can't circumvent the non-self nature that we're trying to see from the practice.

149
00:20:07,920 --> 00:20:16,920
Our practice itself is non-self. The only control, you could say, is in choices.

150
00:20:16,920 --> 00:20:24,920
Every moment is a choice, and if you make the right choices every moment, you will become more mindful.

151
00:20:24,920 --> 00:20:30,920
There will be less distraction. But your mind is it's not under your control. There is no fix.

152
00:20:30,920 --> 00:20:35,920
Looking for a quick fix, some way to change the nature of the mind.

153
00:20:35,920 --> 00:20:44,920
The only way is steady, constant, repeated cultivation of this positive habit.

154
00:20:44,920 --> 00:20:49,920
Seeing things clearly.

155
00:20:49,920 --> 00:20:55,920
Would you say that you've been noticing that you're not noticing is a step in the right direction now?

156
00:20:55,920 --> 00:20:59,920
Yes, absolutely.

157
00:20:59,920 --> 00:21:02,920
I don't know, it's an outcome of the practice.

158
00:21:02,920 --> 00:21:06,920
Another step in the right direction would be to remind yourself knowing knowing.

159
00:21:06,920 --> 00:21:13,920
The noticing is the outcome. When you notice that you're distracted, that's the outcome of being mindful.

160
00:21:13,920 --> 00:21:27,920
But the step in the right direction is to get back on track and then apply mindfulness to even that.

161
00:21:27,920 --> 00:21:33,920
Would like to know whether you will continue with the Dhamapada teachings. They are very helpful indeed.

162
00:21:33,920 --> 00:21:41,920
Are they really? I question that. I know they're enjoyable. I question how helpful they are.

163
00:21:41,920 --> 00:21:48,920
I think they are because the background stories that you explain have a lot of the Buddhist teaching in them.

164
00:21:48,920 --> 00:21:56,920
It's not just entertaining, but it's not just entertainment. There's always a good,

165
00:21:56,920 --> 00:22:03,920
you know, the morality tales of other traditions.

166
00:22:03,920 --> 00:22:11,920
Yeah, I don't know. I think they're helpful. I think they have good messages.

167
00:22:11,920 --> 00:22:17,920
Yeah, but you just need someone to help you produce them.

168
00:22:17,920 --> 00:22:23,920
Yeah, if I had someone to produce and maybe an audience, you know,

169
00:22:23,920 --> 00:22:28,920
so I could do it. Yeah, I guess it's more someone to produce.

170
00:22:28,920 --> 00:22:34,920
If I had someone who was willing to do all the production, that would make it more reasonable.

171
00:22:34,920 --> 00:22:37,920
Because right now, you know, I've got the camera and all the things here,

172
00:22:37,920 --> 00:22:39,920
but then I have to go and set it up in the living room.

173
00:22:39,920 --> 00:22:42,920
And it's a lot of little things to set up in batteries.

174
00:22:42,920 --> 00:22:47,920
And I can do it all, but then I just kind of feel like, yeah, I don't know.

175
00:22:47,920 --> 00:22:52,920
It's a bit much for a monk to be doing.

176
00:22:52,920 --> 00:23:02,920
Hopefully, you know, with your attendance at university, maybe that will somehow work its way into a project or an assignment or something.

177
00:23:02,920 --> 00:23:08,920
That would be cool.

178
00:23:08,920 --> 00:23:20,920
What if someone born with this knowledge is made to forget and lives based on instinct instead of the teachings?

179
00:23:20,920 --> 00:23:23,920
I don't understand.

180
00:23:23,920 --> 00:23:33,920
So if someone had knowledge of the Buddha's teachings and forgot and lived based on instinct instead?

181
00:23:33,920 --> 00:23:39,920
Skip it brain hurts. Sorry.

182
00:23:39,920 --> 00:23:42,920
I'm going to skip that one.

183
00:23:42,920 --> 00:23:45,920
Okay. You're making my brain hurt.

184
00:23:45,920 --> 00:23:50,920
Well, maybe explain it a little bit better. What exactly do you want to know?

185
00:23:50,920 --> 00:23:57,920
I mean, it sounds like a weird question and I'm not really getting it.

186
00:23:57,920 --> 00:24:00,920
Maybe I'll work in some clarification on that one.

187
00:24:00,920 --> 00:24:08,920
In the Santi Patana, what is the difference between contemplating the body internally and contemplating the body externally?

188
00:24:08,920 --> 00:24:18,920
Well, you can theoretically contemplate someone else's body. You can contemplate a dead body, for example.

189
00:24:18,920 --> 00:24:21,920
It's a form of guy and a person, as I said, a bit Hannah.

190
00:24:21,920 --> 00:24:27,920
But it's not, it's not, it's not can't be used for Vipassana.

191
00:24:27,920 --> 00:24:31,920
Mindfulness of the external, the body externally.

192
00:24:31,920 --> 00:24:36,920
They repeat that for each phrase, but really only some of them relate to the external body.

193
00:24:36,920 --> 00:24:42,920
Like the Sampajana is nothing to do with the external body.

194
00:24:42,920 --> 00:24:49,920
The areaapatize nothing to do with the external body, but the cemetery contemplations.

195
00:24:49,920 --> 00:24:51,920
That's an external body.

196
00:24:51,920 --> 00:24:54,920
So some of them are external.

197
00:24:54,920 --> 00:24:57,920
The ones for Vipassana are internal.

198
00:24:57,920 --> 00:25:05,920
Another way I think of understanding external, maybe in regards to the external basis.

199
00:25:05,920 --> 00:25:11,920
So seeing, they're seeing, hearing, smelling, tasting, feeling and thinking.

200
00:25:11,920 --> 00:25:21,920
The external are sight, are light, sound, smells, tastes, feelings, and you can say thoughts.

201
00:25:21,920 --> 00:25:22,920
Those are external.

202
00:25:22,920 --> 00:25:28,920
Internal are the eye, the ear, the nose, the tongue, the body, and the mind.

203
00:25:28,920 --> 00:25:34,920
So if you want to think of it that way, then you've got external and internal.

204
00:25:34,920 --> 00:25:53,920
The external is still a part of the practice.

205
00:25:53,920 --> 00:26:00,920
So the question that wasn't clear before the person is asking about using instinct rather than knowledge.

206
00:26:00,920 --> 00:26:06,920
What do you want to know, Robin, what do they want to know?

207
00:26:06,920 --> 00:26:08,920
I don't know.

208
00:26:08,920 --> 00:26:16,920
I take a guess that, you know, is it possible that someone who had knowledge of the Buddhist teachings,

209
00:26:16,920 --> 00:26:22,920
maybe they were born into a Buddhist family and knew a lot and then just went about their life and forgot it,

210
00:26:22,920 --> 00:26:28,920
but lived by instinct without really thinking about it, but that might not be right.

211
00:26:28,920 --> 00:26:38,920
Well, if you have a question, it should be a question about your situation, something that is...

212
00:26:38,920 --> 00:26:40,920
Okay, now we're getting closer.

213
00:26:40,920 --> 00:26:42,920
I have not accepted something internally.

214
00:26:42,920 --> 00:26:44,920
So tell me about that.

215
00:26:44,920 --> 00:26:46,920
What is it that you want to know?

216
00:26:46,920 --> 00:26:51,920
What can I do to help you?

217
00:26:51,920 --> 00:27:00,920
You have lost something like you used to understand something or...

218
00:27:00,920 --> 00:27:19,920
I need a clear question about your practice.

219
00:27:19,920 --> 00:27:21,920
So Robin, how was your trip?

220
00:27:21,920 --> 00:27:23,920
My trip was very good.

221
00:27:23,920 --> 00:27:26,920
I went camping for a couple of days, survived.

222
00:27:26,920 --> 00:27:28,920
I hit a skunk.

223
00:27:28,920 --> 00:27:30,920
I hit a skunk come up to me yesterday.

224
00:27:30,920 --> 00:27:35,920
I was sitting at the campfire and there was just a skunk right at my feet.

225
00:27:35,920 --> 00:27:43,920
This is where it comes in handy to meditate regularly because I think a year ago I would scream.

226
00:27:43,920 --> 00:27:50,920
I didn't react and I didn't get sprayed.

227
00:27:50,920 --> 00:27:56,920
It's a nice little side benefit of being a little more calm, not jumpy.

228
00:27:56,920 --> 00:28:00,920
It's very nice trip.

229
00:28:00,920 --> 00:28:06,920
Animals seem to respond quicker to calm, to mindfulness.

230
00:28:06,920 --> 00:28:12,920
They can really feel the vibes quicker than humans.

231
00:28:12,920 --> 00:28:24,920
So we have a little more detail on the question.

232
00:28:24,920 --> 00:28:27,920
Adults were afraid of my knowledge, dreams as a boy.

233
00:28:27,920 --> 00:28:37,920
I was giving an induced amnesia along with implants.

234
00:28:37,920 --> 00:28:48,920
Some stories are a little difficult to tell, a little bit involved.

235
00:28:48,920 --> 00:28:58,920
If you need some help, you can contact me.

236
00:28:58,920 --> 00:29:01,920
Okay, have you read my booklet on how to meditate?

237
00:29:01,920 --> 00:29:05,920
Maybe that'll help.

238
00:29:05,920 --> 00:29:09,920
It's a good place to start.

239
00:29:09,920 --> 00:29:12,920
It's a good book to read over and over too.

240
00:29:12,920 --> 00:29:18,920
I found somebody gave me really good advice when I started studying Buddhism that when you first read a book.

241
00:29:18,920 --> 00:29:20,920
You don't really absorb too much of it.

242
00:29:20,920 --> 00:29:23,920
If you go back and read it a few months later or a year later.

243
00:29:23,920 --> 00:29:25,920
I found that with your book definitely.

244
00:29:25,920 --> 00:29:32,920
Each time I read it, I just find little things that I sometimes wonder if you updated it and added more to it.

245
00:29:32,920 --> 00:29:35,920
I think that's probably not the case.

246
00:29:35,920 --> 00:29:40,920
I probably just missed things the first several times.

247
00:29:40,920 --> 00:29:41,920
A lot actually.

248
00:29:41,920 --> 00:29:43,920
People don't want to say it to me.

249
00:29:43,920 --> 00:29:44,920
Don't want to tell me.

250
00:29:44,920 --> 00:29:52,920
But you can tell what they want to say is I only realized how useful it was the second time I read it.

251
00:29:52,920 --> 00:29:57,920
The first time wasn't as impressive to them as the second time.

252
00:29:57,920 --> 00:30:02,920
But what they say is that, as you say, they got a lot out of it the second time.

253
00:30:02,920 --> 00:30:06,920
But it seems to me that the first time reading it was not...

254
00:30:06,920 --> 00:30:12,920
Well, some people actually said that they had to read it a couple of times to really get what was being said.

255
00:30:12,920 --> 00:30:14,920
It is fairly...

256
00:30:14,920 --> 00:30:17,920
I mean, maybe it's just not entirely clear.

257
00:30:17,920 --> 00:30:21,920
But it's quite likely that it's just a fairly radical teaching.

258
00:30:21,920 --> 00:30:27,920
And without actually the problem with the book is that it's not actually telling you to do it.

259
00:30:27,920 --> 00:30:30,920
I mean, it's not forcing you to do it.

260
00:30:30,920 --> 00:30:34,920
It's not waiting, sitting there waiting for you to respond.

261
00:30:34,920 --> 00:30:35,920
It's passive.

262
00:30:35,920 --> 00:30:40,920
Whereas if I teach you these things, I'm telling you right then and there to do it.

263
00:30:40,920 --> 00:30:42,920
And you're right then and they're doing it.

264
00:30:42,920 --> 00:30:44,920
Whereas someone might read the book.

265
00:30:44,920 --> 00:30:48,920
And their mindset is to just how you read a book.

266
00:30:48,920 --> 00:30:49,920
I want some information.

267
00:30:49,920 --> 00:30:51,920
And that's not what this book is about.

268
00:30:51,920 --> 00:30:52,920
It's not an information book.

269
00:30:52,920 --> 00:30:55,920
It's a do this right now, kind of book.

270
00:30:55,920 --> 00:31:03,920
And I think until you actually try and until you realize flip that switch,

271
00:31:03,920 --> 00:31:09,920
where you say, oh, wait, it's telling me actually to really say, seeing, seeing...

272
00:31:09,920 --> 00:31:12,920
That even though that's exactly what it's saying the first time,

273
00:31:12,920 --> 00:31:14,920
I think some people don't get it.

274
00:31:14,920 --> 00:31:18,920
Definitely after reading it and doing it and practicing and then going back

275
00:31:18,920 --> 00:31:23,920
and there's definitely a lot more there than I just missed the whole first time.

276
00:31:33,920 --> 00:31:37,920
Well, I'd recommend reading my book clip.

277
00:31:37,920 --> 00:31:39,920
I mean, there are other resources.

278
00:31:39,920 --> 00:31:43,920
It doesn't have to be my booklet, but so there's a reason I wrote it.

279
00:31:43,920 --> 00:31:49,920
It's not my teachings. It's teachings that were given to me and given to all of us.

280
00:31:49,920 --> 00:31:54,920
And I heard them so many times that I thought, well, gee, why don't we have a book?

281
00:31:54,920 --> 00:31:59,920
I mean, there are books in time.

282
00:31:59,920 --> 00:32:03,920
Mindful while reading or memorizing.

283
00:32:03,920 --> 00:32:05,920
No, not very well.

284
00:32:05,920 --> 00:32:07,920
I mean, you can go back and forth, right?

285
00:32:07,920 --> 00:32:10,920
When you take a break or when you find yourself wondering,

286
00:32:10,920 --> 00:32:12,920
then you can come back and be mindful.

287
00:32:12,920 --> 00:32:23,920
But during the time when you're memorizing, it's a different activity.

288
00:32:23,920 --> 00:32:26,920
Not everything is meditation.

289
00:32:26,920 --> 00:32:29,920
Study is not really meditation, though.

290
00:32:29,920 --> 00:32:32,920
You can do them in tandem.

291
00:32:32,920 --> 00:32:35,920
Study is study.

292
00:32:35,920 --> 00:32:41,920
And that's why it's something you have to moderate because

293
00:32:41,920 --> 00:32:45,920
I study too much can detract from your meditation.

294
00:32:45,920 --> 00:32:49,920
We had one meditator who, one of the meditators who went crazy,

295
00:32:49,920 --> 00:32:53,920
not my meditator, someone else's meditator back when I was just,

296
00:32:53,920 --> 00:32:57,920
and I was not yet teaching.

297
00:32:57,920 --> 00:33:04,920
And we mentioned to Adjan Tang that he was reading a lot of books.

298
00:33:04,920 --> 00:33:06,920
And I don't know that anyway.

299
00:33:06,920 --> 00:33:08,920
He was reading a lot of books.

300
00:33:08,920 --> 00:33:11,920
And Adjan Tang said, that's fine. Let him read the books because

301
00:33:11,920 --> 00:33:14,920
it'll disrupt his concentration because he was going crazy.

302
00:33:14,920 --> 00:33:16,920
He was really warped.

303
00:33:16,920 --> 00:33:17,920
So he said, yeah, it's OK.

304
00:33:17,920 --> 00:33:20,920
Let him read because it'll disrupt his concentration.

305
00:33:20,920 --> 00:33:24,920
I'm sure that was interesting.

306
00:33:24,920 --> 00:33:37,920
So a bit of a slow day.

307
00:33:37,920 --> 00:33:40,920
Not too many questions.

308
00:33:40,920 --> 00:33:44,920
Thank you for tuning in everyone.

309
00:33:44,920 --> 00:33:47,920
See, that's ended there then.

310
00:33:47,920 --> 00:33:49,920
Come back again tomorrow.

311
00:33:49,920 --> 00:33:50,920
Thank you, Bhante.

312
00:33:50,920 --> 00:33:52,920
Nice to see you again, Robin.

313
00:33:52,920 --> 00:33:53,920
Thank you.

314
00:33:53,920 --> 00:33:55,920
Thanks, everyone.

315
00:33:55,920 --> 00:34:24,920
Thank you.

