1
00:00:00,000 --> 00:00:10,000
Okay, good evening everyone, welcome to our evening dumber.

2
00:00:10,000 --> 00:00:28,000
Tonight we're looking at Samawitaka, which is usually translated as right thought.

3
00:00:28,000 --> 00:00:34,000
Recently I heard someone say, oh no, right thought is probably not a good translation.

4
00:00:34,000 --> 00:00:43,000
And I almost changed it, I almost put something different, but I've changed it back.

5
00:00:43,000 --> 00:00:46,000
Because it's simple, right thought is easy to remember.

6
00:00:46,000 --> 00:00:49,000
It's not a mouthful like anything else.

7
00:00:49,000 --> 00:00:59,000
And thought is an interesting word, it's a good word because of how many uses we have for it.

8
00:00:59,000 --> 00:01:06,000
When we say right thought, immediately we think of the thought, right?

9
00:01:06,000 --> 00:01:14,000
I like turtles, that's a thought example.

10
00:01:14,000 --> 00:01:20,000
We have a thought arise and we think, well that's what we mean by thought.

11
00:01:20,000 --> 00:01:30,000
But when you think of something, what you think about something, we also use that for thought.

12
00:01:30,000 --> 00:01:36,000
I think Buddhism is good.

13
00:01:36,000 --> 00:01:43,000
I think meditation is great.

14
00:01:43,000 --> 00:01:48,000
That's a judgment we would say.

15
00:01:48,000 --> 00:01:58,000
Or I'm thinking about suicide, I'm thinking of killing myself.

16
00:01:58,000 --> 00:02:03,000
That's not just a thought, it's an inclination.

17
00:02:03,000 --> 00:02:11,000
And it's the inclinations I think that we're most concerned with here.

18
00:02:11,000 --> 00:02:20,000
This monk was right, that right thought is a dangerous translation, it's a problematic translation.

19
00:02:20,000 --> 00:02:22,000
Because we're certainly not talking about thought.

20
00:02:22,000 --> 00:02:27,000
And it gives the idea that a thought itself can be wrong.

21
00:02:27,000 --> 00:02:33,000
I know if you have thought, if a thought arises in your mind,

22
00:02:33,000 --> 00:02:40,000
I wonder what it would be like to kill myself or maybe the answer to life's problems is to kill myself.

23
00:02:40,000 --> 00:02:42,000
A thought like that can arise.

24
00:02:42,000 --> 00:02:48,000
Or you imagine doing something terrible and you think about killing someone and just the thought arises.

25
00:02:48,000 --> 00:02:51,000
It can arise without any inclination on your part to do it.

26
00:02:51,000 --> 00:03:00,000
You can be horrified by the thought or just find it ridiculous that you would think such a thing.

27
00:03:00,000 --> 00:03:04,000
And that's important because the thoughts themselves are not a problem.

28
00:03:04,000 --> 00:03:10,000
It's important to get across meditators often feel very guilty about their thoughts.

29
00:03:10,000 --> 00:03:15,000
You think of something and you think, oh, that's so wrong to think that.

30
00:03:15,000 --> 00:03:18,000
I shouldn't think that as a meditator.

31
00:03:18,000 --> 00:03:21,000
That's not the problem.

32
00:03:21,000 --> 00:03:24,000
What we're really concerned with here is your inclinations.

33
00:03:24,000 --> 00:03:29,000
Are you inclined to kill yourself? Are you inclined to kill someone else?

34
00:03:29,000 --> 00:03:33,000
If so, there's a problem.

35
00:03:33,000 --> 00:03:36,000
Are you inclined to think of these things as problems?

36
00:03:36,000 --> 00:03:40,000
Well, that's a problem as well.

37
00:03:40,000 --> 00:03:44,000
It's our judgments and our reactions, really.

38
00:03:44,000 --> 00:03:46,000
So this is what we talk about.

39
00:03:46,000 --> 00:03:52,000
Some mawitaka is being free from having the opposite kinds,

40
00:03:52,000 --> 00:03:55,000
having good kinds of inclinations,

41
00:03:55,000 --> 00:04:05,000
inclining to met towards meditation, inclining towards being kind to others.

42
00:04:05,000 --> 00:04:07,000
Good inclinations.

43
00:04:07,000 --> 00:04:15,000
Good thought is the other member of the wisdom faculty,

44
00:04:15,000 --> 00:04:21,000
the wisdom group of the eight full noble paths.

45
00:04:21,000 --> 00:04:26,000
So we have right view and then right thought.

46
00:04:26,000 --> 00:04:29,000
That's important to understand the distinction.

47
00:04:29,000 --> 00:04:33,000
Right view is what you believe.

48
00:04:33,000 --> 00:04:35,000
I believe this is right.

49
00:04:35,000 --> 00:04:38,000
I believe this is wrong.

50
00:04:38,000 --> 00:04:41,000
I should kill myself.

51
00:04:41,000 --> 00:04:43,000
I should kill that person.

52
00:04:43,000 --> 00:04:45,000
It's right to do.

53
00:04:45,000 --> 00:04:46,000
Killing is good.

54
00:04:46,000 --> 00:04:48,000
Stealing is good.

55
00:04:48,000 --> 00:04:51,000
Getting angry, that's a good thing.

56
00:04:51,000 --> 00:04:54,000
This is all wrong with you.

57
00:04:54,000 --> 00:05:00,000
Right view is when you know anger that's not useful.

58
00:05:00,000 --> 00:05:03,000
It's cause for suffering.

59
00:05:03,000 --> 00:05:08,000
Desire that's also not useful.

60
00:05:08,000 --> 00:05:15,000
That's your view when you know these things to be true.

61
00:05:15,000 --> 00:05:18,000
It's actually a lot easier.

62
00:05:18,000 --> 00:05:21,000
It's very difficult even to come to that view.

63
00:05:21,000 --> 00:05:26,000
That's something we should all be quite happy about and confident

64
00:05:26,000 --> 00:05:31,000
about the fact that we're in a state where we think meditation is good.

65
00:05:31,000 --> 00:05:32,000
Right.

66
00:05:32,000 --> 00:05:42,000
We have this view that meditation is beneficial and that getting caught up in the world is problematic.

67
00:05:42,000 --> 00:05:51,000
It's much more difficult and it's a whole other thing to give up the inclination for those things.

68
00:05:51,000 --> 00:05:52,000
Right.

69
00:05:52,000 --> 00:06:02,000
You can know that a drug addict knows that the drugs are really, really a bad idea.

70
00:06:02,000 --> 00:06:06,000
It doesn't mean they don't do drugs.

71
00:06:06,000 --> 00:06:12,000
They're all the time doing them, all the time they're doing them, they know that this is wrong, this is a problem.

72
00:06:12,000 --> 00:06:17,000
They still do the drugs because they don't have right thought yet.

73
00:06:17,000 --> 00:06:21,000
They have right view, but they don't have the right inclination.

74
00:06:21,000 --> 00:06:24,000
That's what we work on in meditation really.

75
00:06:24,000 --> 00:06:30,000
It's where our meditation practice takes place.

76
00:06:30,000 --> 00:06:36,000
Along the way, right view comes up and right view cuts off the inclination.

77
00:06:36,000 --> 00:06:39,000
So in your first meditating, you don't have either.

78
00:06:39,000 --> 00:06:41,000
But you work on the right inclination.

79
00:06:41,000 --> 00:06:46,000
You incline yourself to understand things.

80
00:06:46,000 --> 00:06:48,000
You incline yourself towards meditation.

81
00:06:48,000 --> 00:06:51,000
You say, okay, well, I'll try this. Maybe it's a good thing.

82
00:06:51,000 --> 00:06:54,000
The right inclination, and so you work at it.

83
00:06:54,000 --> 00:06:59,000
You work at purifying your mind, stopping your reactions.

84
00:06:59,000 --> 00:07:01,000
You work at being objective towards things.

85
00:07:01,000 --> 00:07:06,000
And after you do that, eventually right view arises with the realization of Nibana.

86
00:07:06,000 --> 00:07:08,000
Right view arises, you say, yes.

87
00:07:08,000 --> 00:07:13,000
Yes, these things are good, and yes, those things are bad.

88
00:07:13,000 --> 00:07:19,000
And that right view supports your right intention.

89
00:07:19,000 --> 00:07:24,000
But then you have to work to work on the right intention again.

90
00:07:24,000 --> 00:07:27,000
Only this time now you know that it's wrong.

91
00:07:27,000 --> 00:07:33,000
But even someone who knows, someone who's seen Nibana still gets greedy,

92
00:07:33,000 --> 00:07:39,000
still gets angry, still has wrong inclination, potentially,

93
00:07:39,000 --> 00:07:44,000
until they work it out completely.

94
00:07:44,000 --> 00:07:49,000
The Buddha likened it to someone who is wearing white,

95
00:07:49,000 --> 00:07:59,000
white, clean, white clothes, and perfumed, and really concerned about cleanliness.

96
00:07:59,000 --> 00:08:03,000
But then they're walking down this path,

97
00:08:03,000 --> 00:08:09,000
and suddenly they say, bull elephant charging at them.

98
00:08:09,000 --> 00:08:14,000
And they look off to the side of the path, and there's a cesspool.

99
00:08:14,000 --> 00:08:23,000
The cesspools are, you know, there's feces, urine, and lots of lots of gross things.

100
00:08:23,000 --> 00:08:29,000
And they jump into the cesspool to avoid the charging elephant.

101
00:08:29,000 --> 00:08:35,000
The Buddha likened this sort of thing, a soda pan, and knows that it's wrong,

102
00:08:35,000 --> 00:08:37,000
knows that these things are wrong.

103
00:08:37,000 --> 00:08:42,000
But in order to get away from the Kilesa, the anger is overwhelming,

104
00:08:42,000 --> 00:08:47,000
and the fears overwhelming, the greed is overwhelming, the desire and lust.

105
00:08:47,000 --> 00:08:50,000
They do it.

106
00:08:50,000 --> 00:08:53,000
But once they do it, they feel awful.

107
00:08:53,000 --> 00:08:57,000
They feel dirty, they're disgusted.

108
00:08:57,000 --> 00:09:01,000
They'll never say to themselves, that was a good idea.

109
00:09:01,000 --> 00:09:06,000
Well, it breaks down there, because the person who jumped in the cesspool

110
00:09:06,000 --> 00:09:11,000
probably it wasn't a good idea to avoid the elephant.

111
00:09:11,000 --> 00:09:16,000
But I suppose you could go further and say it's like they knew

112
00:09:16,000 --> 00:09:21,000
that the right thing to do would be to somehow tame the elephant.

113
00:09:21,000 --> 00:09:25,000
But they weren't strong enough to tame that elephant.

114
00:09:25,000 --> 00:09:29,000
So they jumped into the cesspool, and they know it wasn't a good thing.

115
00:09:29,000 --> 00:09:31,000
It wasn't pleasant.

116
00:09:31,000 --> 00:09:39,000
They're not happy about having jumped in the cesspool.

117
00:09:39,000 --> 00:09:42,000
That's how it becomes as you practice.

118
00:09:42,000 --> 00:09:45,000
It's important to understand the orderliness of this.

119
00:09:45,000 --> 00:09:48,000
The first thing to go away is wrong view.

120
00:09:48,000 --> 00:09:50,000
In the beginning, it's wrong.

121
00:09:50,000 --> 00:09:53,000
You go away because you have faith.

122
00:09:53,000 --> 00:09:56,000
I believe in what the Buddha taught.

123
00:09:56,000 --> 00:10:01,000
And that gives you enough of confidence to cultivate right thought,

124
00:10:01,000 --> 00:10:04,000
right intention.

125
00:10:04,000 --> 00:10:09,000
But once you hit the Ariyamanga in the noble path,

126
00:10:09,000 --> 00:10:11,000
wrong views cut on.

127
00:10:11,000 --> 00:10:15,000
So never again will you think you're never doubt.

128
00:10:15,000 --> 00:10:18,000
Is it right to get attached to things?

129
00:10:18,000 --> 00:10:23,000
Is it right to desire after anything in the world?

130
00:10:23,000 --> 00:10:25,000
You won't have that thought.

131
00:10:25,000 --> 00:10:27,000
You won't have that view.

132
00:10:27,000 --> 00:10:29,000
But you'll still desire those things.

133
00:10:29,000 --> 00:10:31,000
You'll still get angry.

134
00:10:31,000 --> 00:10:34,000
And these things will still come up.

135
00:10:34,000 --> 00:10:36,000
You'll feel awful afterwards.

136
00:10:36,000 --> 00:10:39,000
You'll see you.

137
00:10:39,000 --> 00:10:45,000
The Buddha mentions three types of Samhavitaka.

138
00:10:45,000 --> 00:10:46,000
I'm sorry.

139
00:10:46,000 --> 00:10:49,000
Samhavitaka, I'm using the wrong transition.

140
00:10:49,000 --> 00:10:52,000
Samhavitaka.

141
00:10:52,000 --> 00:10:55,000
I'm mixing up my translations here.

142
00:10:55,000 --> 00:10:57,000
The Pali word is not Vitaka.

143
00:10:57,000 --> 00:11:02,000
It's the same thing, but Samhavitaka.

144
00:11:02,000 --> 00:11:05,000
Samhavitit, Samhavitaka.

145
00:11:05,000 --> 00:11:08,000
See what it teaches you.

146
00:11:08,000 --> 00:11:12,000
Samhavitaka means the same thing, right thought.

147
00:11:12,000 --> 00:11:16,000
So the three kinds of rights are

148
00:11:16,000 --> 00:11:19,000
Nikama, Samhavitaka.

149
00:11:19,000 --> 00:11:22,000
They're also called Vidaka, Samhaviras.

150
00:11:22,000 --> 00:11:30,000
Nikama, Samhavitaka, Samhavitaka, Samhavitaka.

151
00:11:30,000 --> 00:11:35,000
Nikama is the inclination to

152
00:11:35,000 --> 00:11:42,000
pronounce, to give up the inclination to let go.

153
00:11:42,000 --> 00:11:54,000
Abhayapada, Sankapa, is the inclination to not get angry.

154
00:11:54,000 --> 00:12:00,000
In connection not to hate people or hate things.

155
00:12:00,000 --> 00:12:11,000
And our wings are Sankapa is the inclination not to be cruel.

156
00:12:11,000 --> 00:12:18,000
And I'm still not clear exactly the difference between Abhayapada and Abhayingsa,

157
00:12:18,000 --> 00:12:24,000
but it's clear by Abhayapada and Mihingsa.

158
00:12:24,000 --> 00:12:30,000
But Abhayapada means meta.

159
00:12:30,000 --> 00:12:34,000
And the commentary says that not hating people,

160
00:12:34,000 --> 00:12:38,000
not being angry towards people, is having love.

161
00:12:38,000 --> 00:12:41,000
You're opposite of hate.

162
00:12:41,000 --> 00:12:47,000
And non-cruelty is cut on its compassion.

163
00:12:47,000 --> 00:12:51,000
So we're starting to think how that differs.

164
00:12:51,000 --> 00:12:53,000
How it would differ to be angry at someone

165
00:12:53,000 --> 00:12:55,000
and to be cruel towards someone.

166
00:12:55,000 --> 00:12:57,000
And I think there are cases where it's different.

167
00:12:57,000 --> 00:13:01,000
Sometimes you just have disdain or lack of compassion for someone.

168
00:13:01,000 --> 00:13:03,000
You don't hate them.

169
00:13:03,000 --> 00:13:06,000
But you don't care that they're suffering, right?

170
00:13:06,000 --> 00:13:09,000
It doesn't bother you, it doesn't concern you.

171
00:13:09,000 --> 00:13:13,000
It doesn't move you to stop what you're doing.

172
00:13:13,000 --> 00:13:17,000
So that's Mihingsa.

173
00:13:17,000 --> 00:13:24,000
You do something that is cruel, which is distorted, right?

174
00:13:24,000 --> 00:13:29,000
Because we have this inclination not to suffer.

175
00:13:29,000 --> 00:13:33,000
We wouldn't ever want someone to, it sounds cliche,

176
00:13:33,000 --> 00:13:36,000
we wouldn't ever want someone to do that to us.

177
00:13:36,000 --> 00:13:39,000
And it sounds like, well, that's just a nice philosophy.

178
00:13:39,000 --> 00:13:41,000
But it actually, it makes sense.

179
00:13:41,000 --> 00:13:45,000
You're not inclined towards these states for yourself.

180
00:13:45,000 --> 00:13:47,000
It's not something that you think of,

181
00:13:47,000 --> 00:13:48,000
hey, that's a good idea.

182
00:13:48,000 --> 00:13:49,000
Let's hurt.

183
00:13:49,000 --> 00:13:50,000
Let's be hurt.

184
00:13:50,000 --> 00:13:52,000
Let's suffer.

185
00:13:52,000 --> 00:13:55,000
The idea of it is repulsive towards you.

186
00:13:55,000 --> 00:13:59,000
So to have that on the one hand and on the other hand,

187
00:13:59,000 --> 00:14:03,000
to be perfectly fine with it happening to someone else,

188
00:14:03,000 --> 00:14:05,000
there's a disconnect there.

189
00:14:05,000 --> 00:14:06,000
It doesn't, it may not seem like it.

190
00:14:06,000 --> 00:14:08,000
It seems like a natural thing, of course.

191
00:14:08,000 --> 00:14:10,000
Dog eat dog, everyone for themselves.

192
00:14:10,000 --> 00:14:13,000
But the interesting thing is, is when you meditate

193
00:14:13,000 --> 00:14:15,000
and meditation isn't on these things.

194
00:14:15,000 --> 00:14:18,000
Meditate means when you're mindful.

195
00:14:18,000 --> 00:14:23,000
When you learn how to see just ordinary experience as it is,

196
00:14:23,000 --> 00:14:25,000
a remarkable thing happens.

197
00:14:25,000 --> 00:14:29,000
You are inclined, it balances out.

198
00:14:29,000 --> 00:14:31,000
It comes into focus.

199
00:14:31,000 --> 00:14:35,000
And you lose this selfishness,

200
00:14:35,000 --> 00:14:37,000
which is, it's not real.

201
00:14:37,000 --> 00:14:39,000
It's not a natural state.

202
00:14:39,000 --> 00:14:42,000
It's not the ultimate balanced state

203
00:14:42,000 --> 00:14:45,000
to be only concerned about yourself.

204
00:14:45,000 --> 00:14:49,000
A person who is objective has no cruelty.

205
00:14:49,000 --> 00:14:52,000
They see their own suffering and someone else's suffering

206
00:14:52,000 --> 00:14:55,000
really has the same thing.

207
00:14:55,000 --> 00:14:57,000
They're not inclined towards either,

208
00:14:57,000 --> 00:15:06,000
and they're inclined to be compassionate.

209
00:15:06,000 --> 00:15:08,000
And the same with loving.

210
00:15:08,000 --> 00:15:10,000
They're inclined to be loving.

211
00:15:10,000 --> 00:15:14,000
They're not inclined to be angry towards others.

212
00:15:14,000 --> 00:15:16,000
They don't hate others.

213
00:15:16,000 --> 00:15:20,000
They don't hate themselves.

214
00:15:20,000 --> 00:15:22,000
So it's not a lot to say.

215
00:15:22,000 --> 00:15:23,000
It's an important teaching.

216
00:15:23,000 --> 00:15:24,000
It's quite simple.

217
00:15:24,000 --> 00:15:27,000
It's important to understand this distinction.

218
00:15:27,000 --> 00:15:29,000
It's thought and right view.

219
00:15:29,000 --> 00:15:34,000
Some meditate, some, some kappa.

220
00:15:34,000 --> 00:15:36,000
A right view is our beliefs.

221
00:15:36,000 --> 00:15:39,000
A right thought is actually our in practice.

222
00:15:39,000 --> 00:15:43,000
What we inclined towards.

223
00:15:43,000 --> 00:15:45,000
But they're related, of course.

224
00:15:45,000 --> 00:15:48,000
As I said, our right inclination leads to right view.

225
00:15:48,000 --> 00:15:51,000
Right view leads, of course, to, to right inclination.

226
00:15:51,000 --> 00:15:53,000
They work together.

227
00:15:53,000 --> 00:15:55,000
So this together is the wisdom.

228
00:15:55,000 --> 00:15:57,000
This is a person who has these two.

229
00:15:57,000 --> 00:15:59,000
It's considered to be wise.

230
00:15:59,000 --> 00:16:01,000
This is the wisdom, quite simple.

231
00:16:01,000 --> 00:16:02,000
No.

232
00:16:02,000 --> 00:16:08,000
The wisdom, the portion, the full noble path.

233
00:16:08,000 --> 00:16:13,000
So questions.

234
00:16:13,000 --> 00:16:18,000
Practicing formal meditation is one of the first things

235
00:16:18,000 --> 00:16:19,000
you do in the morning.

236
00:16:19,000 --> 00:16:22,000
Be better than waiting until after breakfast or other walking

237
00:16:22,000 --> 00:16:23,000
activities.

238
00:16:23,000 --> 00:16:29,000
But it's the kind of thing you should be doing during your

239
00:16:29,000 --> 00:16:31,000
waking activities.

240
00:16:31,000 --> 00:16:32,000
Ideally.

241
00:16:32,000 --> 00:16:37,000
I can see where you're going with that.

242
00:16:37,000 --> 00:16:40,000
I don't like to answer such questions because life is not

243
00:16:40,000 --> 00:16:43,000
something you can organize.

244
00:16:43,000 --> 00:16:44,000
It shouldn't be.

245
00:16:44,000 --> 00:16:51,000
It should be something that is, well, you have to learn about

246
00:16:51,000 --> 00:16:53,000
all aspects of life.

247
00:16:53,000 --> 00:16:56,000
And so if I said, this is good.

248
00:16:56,000 --> 00:16:58,000
You wouldn't want to neglect that.

249
00:16:58,000 --> 00:17:02,000
So meditate when you can.

250
00:17:02,000 --> 00:17:06,000
If it's, if it's not, seems natural to meditate before you

251
00:17:06,000 --> 00:17:10,000
eat or before you eat, is that what you're saying?

252
00:17:10,000 --> 00:17:13,000
Before you do things, anything in the morning, then

253
00:17:13,000 --> 00:17:14,000
grade.

254
00:17:14,000 --> 00:17:16,000
It feels natural to do those things.

255
00:17:16,000 --> 00:17:19,000
One thing I could say is the commentary does make mention of

256
00:17:19,000 --> 00:17:23,000
the fact that for monks, most especially, or probably

257
00:17:23,000 --> 00:17:28,000
mainly for monks, it's better to eat before you meditate

258
00:17:28,000 --> 00:17:33,000
because they'd have some rice grow in the morning

259
00:17:33,000 --> 00:17:38,000
because it calms your body down and it gives you some strength

260
00:17:38,000 --> 00:17:44,000
and it actually makes it easier to meditate.

261
00:17:44,000 --> 00:17:47,000
I don't, I don't think that's something to rely upon the

262
00:17:47,000 --> 00:17:51,000
commentary often says these things.

263
00:17:51,000 --> 00:17:55,000
It's easy to understand, but it's a good example.

264
00:17:55,000 --> 00:17:59,000
It can be easier after you eat and that's a good thing.

265
00:17:59,000 --> 00:18:01,000
But what about before you eat and when it's difficult?

266
00:18:01,000 --> 00:18:04,000
Are you going to ignore the difficult state?

267
00:18:04,000 --> 00:18:08,000
Are you going to only be inclined towards the good one?

268
00:18:08,000 --> 00:18:12,000
So that's what I mean by, if I answer one way, then you neglect

269
00:18:12,000 --> 00:18:13,000
the other way.

270
00:18:13,000 --> 00:18:19,000
So it's best to be natural about it and to be in tune with

271
00:18:19,000 --> 00:18:22,000
your rhythm and to see where you have problems that need to be

272
00:18:22,000 --> 00:18:25,000
worked out and slowly adjust them.

273
00:18:25,000 --> 00:18:29,000
Don't just work towards that, which is easier convenient.

274
00:18:29,000 --> 00:18:34,000
I mean, so the point of better and worse, not really that

275
00:18:34,000 --> 00:18:39,000
valid that you want to eventually be mindful all the time.

276
00:18:39,000 --> 00:18:41,000
That's cool.

277
00:18:41,000 --> 00:18:45,000
Okay, no name is asking lots of questions, but it appears that

278
00:18:45,000 --> 00:18:50,000
this person is practicing a different school of meditation.

279
00:18:50,000 --> 00:18:54,000
So I'd stop you there and suggest that you read my book on how

280
00:18:54,000 --> 00:19:04,000
to meditate and I think I'm only inclined to answer meditation

281
00:19:04,000 --> 00:19:09,000
questions about our tradition because, and you're asking

282
00:19:09,000 --> 00:19:12,000
questions whether I've experienced such things and I'm not really

283
00:19:12,000 --> 00:19:16,000
interested in answering questions about my own practice.

284
00:19:16,000 --> 00:19:23,000
But if you ask, how should one deal with something or what is the

285
00:19:23,000 --> 00:19:24,000
advice about fear?

286
00:19:24,000 --> 00:19:27,000
I didn't invite you to read my booklet first because I'm going

287
00:19:27,000 --> 00:19:30,000
to, if I'm going to answer that, I'm going to base it on what's in

288
00:19:30,000 --> 00:19:34,000
the booklet, base it on our tradition and the answers are in there,

289
00:19:34,000 --> 00:19:39,000
but I can elaborate on it if you like.

290
00:19:39,000 --> 00:19:43,000
So yeah, the whole problem with fear, I mean, I totally sympathize,

291
00:19:43,000 --> 00:19:49,000
but my advice to you because you're coming to me for advice is to

292
00:19:49,000 --> 00:19:53,000
read my booklet.

293
00:19:53,000 --> 00:19:58,000
It sounds self-serving and self-promoting, but I wrote it for a

294
00:19:58,000 --> 00:20:01,000
reason and I didn't take it from my own teachings.

295
00:20:01,000 --> 00:20:04,000
I copied everything from what I had learned.

296
00:20:04,000 --> 00:20:07,000
So it's not really my book.

297
00:20:07,000 --> 00:20:16,000
That's what I believe is the answer to things like fear.

298
00:20:16,000 --> 00:20:19,000
In mindful of the five aggregates, I find experience leads to

299
00:20:19,000 --> 00:20:21,000
volition and thought, feeling and body.

300
00:20:21,000 --> 00:20:24,000
Would it be better to start with mindfulness of the body instead of

301
00:20:24,000 --> 00:20:27,000
experience in order to cut craving attachment?

302
00:20:27,000 --> 00:20:30,000
Well, it's better to start with a body probably for a different

303
00:20:30,000 --> 00:20:31,000
reason.

304
00:20:31,000 --> 00:20:35,000
I think you may be complicating things a little too much.

305
00:20:35,000 --> 00:20:38,000
Well, the body is just gross in a sense.

306
00:20:38,000 --> 00:20:39,000
It's coarse.

307
00:20:39,000 --> 00:20:44,000
It's easy to find.

308
00:20:44,000 --> 00:20:46,000
It's not easy to find.

309
00:20:46,000 --> 00:20:48,000
It's like a dog chasing its tail sometimes.

310
00:20:48,000 --> 00:20:51,000
But the body is fairly easy to find.

311
00:20:51,000 --> 00:20:56,000
And the old text, say, to what extent the body becomes clear

312
00:20:56,000 --> 00:21:00,000
and you become clearly aware of the body, to that extent the mind also

313
00:21:00,000 --> 00:21:02,000
becomes clear.

314
00:21:02,000 --> 00:21:07,000
So simply focusing on the body, you already start to, you know,

315
00:21:07,000 --> 00:21:14,000
you will as a result begin to observe the mind.

316
00:21:14,000 --> 00:21:18,000
And then you jump off from there, focus on the mind.

317
00:21:18,000 --> 00:21:21,000
If you start to think about something, if you start to judge

318
00:21:21,000 --> 00:21:29,000
something, you start to feel something.

319
00:21:29,000 --> 00:21:32,000
What kind of meditation is best to do in the morning?

320
00:21:32,000 --> 00:21:33,000
Mindfulness meditation.

321
00:21:33,000 --> 00:21:37,000
It's the best meditation to do any time.

322
00:21:37,000 --> 00:21:41,000
But you can also look up the talk I did on the four

323
00:21:41,000 --> 00:21:45,000
protective meditations, I think it's called.

324
00:21:45,000 --> 00:21:48,000
Other types of meditation or something.

325
00:21:48,000 --> 00:21:50,000
We've given some talks on that.

326
00:21:50,000 --> 00:21:53,000
There are four types of meditation that are supportive of

327
00:21:53,000 --> 00:21:55,000
insight meditation.

328
00:21:55,000 --> 00:21:59,000
Maybe someday I'll do a talk on each one of those.

329
00:21:59,000 --> 00:22:02,000
I think people would be very interested in those.

330
00:22:02,000 --> 00:22:07,000
Okay, so that's all the questions for tonight.

331
00:22:07,000 --> 00:22:10,000
Thank you all for tuning in.

332
00:22:10,000 --> 00:22:39,000
Thank you.

