1
00:00:00,000 --> 00:00:04,240
When I meditate, I usually do it laying down.

2
00:00:04,240 --> 00:00:08,440
I do it this way because I like lots of concentration.

3
00:00:08,440 --> 00:00:13,320
Sometimes I do change positions about of a need, out of need.

4
00:00:13,320 --> 00:00:15,720
Is it a bad thing I do it this way?

5
00:00:15,720 --> 00:00:18,240
I think it's got a problem.

6
00:00:18,240 --> 00:00:23,880
The first key is the part where you say because I like lots of concentration.

7
00:00:23,880 --> 00:00:28,240
This kind of person I would be kind of mean and I would say stop lying down because

8
00:00:28,240 --> 00:00:30,800
that's what you like to do.

9
00:00:30,800 --> 00:00:35,720
We want to see what happens when you do something that you don't like to do.

10
00:00:35,720 --> 00:00:40,680
We want to show you what you've been building up based on your attachments because

11
00:00:40,680 --> 00:00:44,120
an enlightened being can be happy anywhere.

12
00:00:44,120 --> 00:00:49,560
If you have preference for something, let's see what happens when you don't get it.

13
00:00:49,560 --> 00:00:55,760
That's kind of being I suppose, but it's pertinent.

14
00:00:55,760 --> 00:01:03,280
Being down, there's no problem with doing lying meditation theoretically, but in practice

15
00:01:03,280 --> 00:01:10,280
it's quite dangerous because it can lead to falling asleep, it can lead to daydreaming.

16
00:01:10,280 --> 00:01:18,400
It's the position where the mind is most accustomed to freedom, not having any kind of

17
00:01:18,400 --> 00:01:20,600
troubles or difficulties.

18
00:01:20,600 --> 00:01:27,400
So there's very little potential to develop wisdom or to develop renegiation to develop

19
00:01:27,400 --> 00:01:34,480
patience to develop all sorts of good qualities and to let go because there's nothing that

20
00:01:34,480 --> 00:01:37,720
needs to be let go of at that time.

21
00:01:37,720 --> 00:01:42,440
What you can do is focus on the liking when you lie down, it can be quite pleasant and

22
00:01:42,440 --> 00:01:47,960
so it can be useful to focus on that experience and to try to free yourself from the

23
00:01:47,960 --> 00:01:50,880
clinging to it.

24
00:01:50,880 --> 00:02:04,600
Lying is best used for people who have stressful jobs or lifestyle, who need concentration,

25
00:02:04,600 --> 00:02:07,840
or their mind is working.

26
00:02:07,840 --> 00:02:12,800
People have, for example, ADHD might benefit a lot from doing lying meditation because

27
00:02:12,800 --> 00:02:16,720
they need the concentration, they need to balance it.

28
00:02:16,720 --> 00:02:21,520
So if your mind is racing, you can try doing lying meditation, but if you're tired or if

29
00:02:21,520 --> 00:02:29,520
you're kind of relaxed, I would work on the energy side, so do walking meditation, walking

30
00:02:29,520 --> 00:02:35,880
meditation is probably not as peaceful or comfortable for you, but that's really the point.

31
00:02:35,880 --> 00:02:40,840
The point is to see what your mind does when the body is not comfortable or not it's

32
00:02:40,840 --> 00:02:49,680
not not comfortable when the body has to face difficulties.

33
00:02:49,680 --> 00:02:53,240
You can sort of see how it's kind of spoiling you because the other part is where you

34
00:02:53,240 --> 00:02:58,080
talk about changing positions.

35
00:02:58,080 --> 00:03:03,400
What you're experiencing is the fact that even though it's pleasant to lie down, it's

36
00:03:03,400 --> 00:03:12,880
really not satisfying, and if you were to lie down for, say, six hours without a sleep,

37
00:03:12,880 --> 00:03:17,280
if you were to lie there and meditate for, say, six hours, 12 hours, some people do it

38
00:03:17,280 --> 00:03:23,280
for 24 hours, I've heard of someone doing lying meditation for 24 hours.

39
00:03:23,280 --> 00:03:27,280
It becomes quite difficult to stay in one posture.

40
00:03:27,280 --> 00:03:29,760
The meaning is that it can't satisfy you.

41
00:03:29,760 --> 00:03:35,600
It's not really pleasant, you still have to always change your posture and find a new posture

42
00:03:35,600 --> 00:03:36,600
that's more comfortable.

43
00:03:36,600 --> 00:03:43,560
I'm going to get a nicer pillow and switch from an ordinary mattress to a water band

44
00:03:43,560 --> 00:03:48,480
or so on and so on, then it's too hot, then it's too cold, then you're thirsty, then

45
00:03:48,480 --> 00:03:53,480
you're hungry, then you have to go to the washroom, then you're bored and you want to

46
00:03:53,480 --> 00:04:01,000
get up and check Facebook or YouTube.

47
00:04:01,000 --> 00:04:07,040
So nothing wrong with lying meditation, but you should understand how it's used.

48
00:04:07,040 --> 00:04:11,440
The fact that you like concentration is a bit of a problem because it becomes an attachment.

49
00:04:11,440 --> 00:04:19,240
You should try to let go of that, it would be probably better to do some walking meditation.

50
00:04:19,240 --> 00:04:23,160
You can get to see things about your mind that you couldn't see before, then you'll be able

51
00:04:23,160 --> 00:04:50,240
to see your attachments to the concentration, for example.

