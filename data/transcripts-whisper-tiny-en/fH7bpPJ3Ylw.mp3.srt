1
00:00:00,000 --> 00:00:16,800
Okay, good evening everyone broadcasting live hopefully November 12, 2015 trying out something new here

2
00:00:16,800 --> 00:00:31,280
So just a word

3
00:00:34,480 --> 00:00:43,440
Tomorrow we have a special event with the Buddhism Association so I won't be broadcasting tomorrow

4
00:00:43,440 --> 00:00:49,040
Not this canceled and then Saturday is a big day as well

5
00:00:50,080 --> 00:00:54,160
But Saturday broadcast should do ahead days plan. I mean just an ordinary broadcast

6
00:00:55,120 --> 00:00:57,120
We're skipping and I'm a part that

7
00:00:57,520 --> 00:00:59,520
So the next I'm a part of will be Monday

8
00:00:59,520 --> 00:01:12,080
I think we'll just skip it

9
00:01:15,120 --> 00:01:21,120
Which means we have to wait for this extra special next one next one's extra special

10
00:01:21,120 --> 00:01:30,880
I don't know how this new mic is working out hopefully it's okay

11
00:01:35,440 --> 00:01:38,000
So tonight we will take questions

12
00:01:39,760 --> 00:01:41,200
Anyone has any questions?

13
00:01:41,200 --> 00:01:49,280
It might sound fine, but they're just it's just picking up a little bit of noise from

14
00:01:50,000 --> 00:01:56,320
You can just rustling around a little bit, but it sounds good. Okay. I won't wrestle

15
00:01:58,000 --> 00:02:00,000
This is very quiet there

16
00:02:02,160 --> 00:02:08,480
So I have sort of a question a friend of mine and I think she's watching if she is Hi Sally

17
00:02:08,480 --> 00:02:15,600
A friend of mine had asked if her daughter could attend a Buddhist service with me and while that would have been great

18
00:02:15,600 --> 00:02:17,600
I would have really enjoyed bringing her

19
00:02:18,480 --> 00:02:24,240
There is only one there was only one and she couldn't she didn't have the availability on that day

20
00:02:24,640 --> 00:02:28,560
It's for homework assignment. So I was wondering if you could talk in general about

21
00:02:31,120 --> 00:02:37,120
The idea of her homework assignment was to attend a service that was something outside of her own religion

22
00:02:37,120 --> 00:02:42,240
It's for a religion education class. So I was wondering if you could talk a little bit about what

23
00:02:43,040 --> 00:02:45,040
Buddhist services involve what

24
00:02:45,840 --> 00:02:50,640
What the requirements are the expectations for followers of Buddhism. It's

25
00:02:51,360 --> 00:02:57,280
Considerably different from say Christian perspective with a real requirement to be there weekly and so forth

26
00:02:59,440 --> 00:03:01,440
Maybe talk

27
00:03:01,440 --> 00:03:06,400
I'm not sure exactly

28
00:03:09,760 --> 00:03:11,760
We don't have any requirements

29
00:03:12,560 --> 00:03:14,560
We teach the

30
00:03:14,560 --> 00:03:17,280
Cause and effect if you do this this will happen

31
00:03:18,320 --> 00:03:20,880
So we have theories and claims about

32
00:03:22,560 --> 00:03:26,160
Reality, but we don't have many obligations on people

33
00:03:26,160 --> 00:03:35,280
We have some shoulds, but they're more or less conditional. Okay, if you want this then you should do this

34
00:03:37,120 --> 00:03:39,520
If you want to be happy then you should be a good person

35
00:03:43,120 --> 00:03:45,120
But

36
00:03:45,120 --> 00:03:47,120
There's never any sense of a

37
00:03:47,600 --> 00:03:49,600
institution in Buddhism

38
00:03:49,600 --> 00:03:55,040
Except for the monastic institution, which is a whole other thing, but that's actually still quite

39
00:04:00,720 --> 00:04:02,720
Practical

40
00:04:04,240 --> 00:04:06,240
So I'm not sure quite what I can talk about

41
00:04:08,560 --> 00:04:13,600
That's that's a big part of it that there is no actual requirement. You know to to attend

42
00:04:13,600 --> 00:04:18,960
On a regular basis, but but people still do go to monasteries and temples

43
00:04:21,360 --> 00:04:24,640
You know when when they want to I guess

44
00:04:27,600 --> 00:04:30,240
Yeah, they go to well, they go to find teachers

45
00:04:32,480 --> 00:04:34,480
Go to the monastery to find the monk

46
00:04:35,280 --> 00:04:37,280
Because if you get that a monk is a

47
00:04:38,320 --> 00:04:40,320
Good choice for a teacher

48
00:04:40,320 --> 00:04:42,320
You

49
00:04:42,560 --> 00:04:44,560
Most likely has something good to teach

50
00:04:45,360 --> 00:04:49,120
And then you build up a rapid build up a relationship and so then you have

51
00:04:52,800 --> 00:04:56,320
You know that they're whether they're good to teach. I have something good to teach

52
00:04:57,440 --> 00:04:59,440
And so you go back and maybe you

53
00:05:01,120 --> 00:05:05,520
Ask them questions and get advice and clarify

54
00:05:05,520 --> 00:05:09,440
Teachings and they're kind of thing

55
00:05:11,360 --> 00:05:13,360
But there's also the opacity

56
00:05:19,840 --> 00:05:21,840
Also what the opacity

57
00:05:25,280 --> 00:05:27,280
Right

58
00:05:27,280 --> 00:05:29,280
Yeah, so there is something there's this

59
00:05:29,280 --> 00:05:34,560
People ask the Buddha how they should keep the holy days

60
00:05:36,240 --> 00:05:39,200
Because in India there was a tradition of

61
00:05:40,720 --> 00:05:42,720
Keeping the lunar cycle

62
00:05:43,680 --> 00:05:45,680
So holy days, so they would take the

63
00:05:46,560 --> 00:05:48,560
8 day and the 15th day

64
00:05:49,680 --> 00:05:53,440
Because if you know the moon goes in a cycle of 30 days approximately

65
00:05:53,440 --> 00:05:59,200
And so every 15 days you get at a noon every 15 days you get a lunar cycle

66
00:06:00,800 --> 00:06:04,560
After 15 days you get the noon moon and then another 15 days you get the phone and

67
00:06:05,680 --> 00:06:08,800
Then halfway in between and you get the half morning

68
00:06:09,600 --> 00:06:12,400
So they had four holy days

69
00:06:14,000 --> 00:06:19,840
People kind of whatever teaching they follow that was kind of a day that they consider the people

70
00:06:19,840 --> 00:06:21,840
Like

71
00:06:21,840 --> 00:06:23,840
Special day

72
00:06:23,840 --> 00:06:26,160
And so the Buddha said that people should keep them

73
00:06:26,960 --> 00:06:27,760
like

74
00:06:27,760 --> 00:06:29,760
Taking the the eight precepts

75
00:06:30,800 --> 00:06:33,520
Because that's the kind of thing an hour hunt lived by

76
00:06:34,640 --> 00:06:36,640
And so it was a way of

77
00:06:36,800 --> 00:06:39,120
Me making or emulating the hour hunts

78
00:06:40,160 --> 00:06:42,160
on those days

79
00:06:43,360 --> 00:06:46,480
By keeping the precepts, which is not killing not stealing

80
00:06:46,480 --> 00:06:50,080
Not having any sexual or romantic activity

81
00:06:50,960 --> 00:06:52,960
Not lying or

82
00:06:52,960 --> 00:06:54,320
Not lying

83
00:06:54,320 --> 00:06:56,320
Not taking drugs or alcohol

84
00:06:57,200 --> 00:06:59,200
Not sleeping on

85
00:06:59,840 --> 00:07:01,840
Luxurious beds

86
00:07:02,400 --> 00:07:04,400
Sorry, not eating a bit tired

87
00:07:04,960 --> 00:07:06,960
Not eating outside of the wrong time

88
00:07:08,240 --> 00:07:09,920
not

89
00:07:11,920 --> 00:07:13,920
Wearing beautification or

90
00:07:13,920 --> 00:07:15,920
And engaging in entertainment

91
00:07:17,280 --> 00:07:19,280
And not sleeping on Luxurious

92
00:07:26,880 --> 00:07:29,360
And they may not have something to do as an opportunity to

93
00:07:30,560 --> 00:07:32,560
Practice participation

94
00:07:32,560 --> 00:07:42,560
More so than the rest of the week

95
00:07:44,560 --> 00:07:49,840
So rather than being you know weekly on a Sunday, it would it would vary based on the moon cycles and

96
00:07:51,440 --> 00:07:53,920
Yeah, I mean they didn't have us all the week back

97
00:07:54,640 --> 00:07:57,840
So they're weak, but it didn't have about seven day week

98
00:07:58,560 --> 00:07:59,760
We have it

99
00:07:59,760 --> 00:08:05,840
Lunar week or would it be solar week? I don't know what we call ours, but they didn't have what we have

100
00:08:06,640 --> 00:08:08,640
They had a lunar week

101
00:08:09,920 --> 00:08:14,080
So a fortnight would be based on the lunar cycle

102
00:08:14,080 --> 00:08:25,840
Thank you, Bante

103
00:08:30,880 --> 00:08:34,560
Another question. What does an animal have to do to be born as a human?

104
00:08:36,560 --> 00:08:38,560
Not to keep the five percent

105
00:08:38,560 --> 00:08:40,560
I

106
00:08:41,760 --> 00:08:43,760
Mean, I mean there's it's

107
00:08:44,160 --> 00:08:50,160
How something is born as something else is if you're not anything about karma. It's there's no science. I mean

108
00:08:50,960 --> 00:08:52,960
There's no easy way to pinpoint it

109
00:08:54,000 --> 00:08:56,000
It's like asking what happens

110
00:08:57,120 --> 00:08:59,120
What do you have to do to create a earthquake?

111
00:08:59,600 --> 00:09:01,600
What do you have to do to create a tornado?

112
00:09:02,160 --> 00:09:06,320
It's a highly complex. Nobody knows how to predict tornadoes or

113
00:09:06,320 --> 00:09:08,320
even earthquakes

114
00:09:09,120 --> 00:09:11,120
Because it's very complex

115
00:09:12,240 --> 00:09:16,400
But how to predict where someone's going to be born is far more complex

116
00:09:18,160 --> 00:09:20,160
so

117
00:09:21,200 --> 00:09:28,000
A lot to I mean it comes down to the last moment what their mind is like in the last moment, but if they're not keeping five presets

118
00:09:28,720 --> 00:09:31,040
It's very difficult for them to be born as human

119
00:09:31,040 --> 00:09:33,040
And

120
00:09:34,400 --> 00:09:36,400
It comes down to that last moment

121
00:09:38,240 --> 00:09:41,920
That would be extremely difficult for an animal that was a carnivore. I would think

122
00:09:42,880 --> 00:09:44,880
Yeah

123
00:09:50,240 --> 00:09:53,600
It's not a matter being difficult because they wouldn't have any idea, but either

124
00:09:54,160 --> 00:09:57,600
They wouldn't have any idea. Oh, let me be born as a human being most for the most part

125
00:09:57,600 --> 00:10:01,920
They would be very happy living as they were and breeding with other

126
00:10:03,680 --> 00:10:05,680
Meeting with other animals with their species

127
00:10:07,120 --> 00:10:09,680
Being born again and again to save type of animal

128
00:10:10,480 --> 00:10:12,000
Very rare

129
00:10:12,160 --> 00:10:14,160
Not a very common thing or

130
00:10:14,160 --> 00:10:27,280
There would not be a common to have the cause to be born as a human

131
00:10:28,240 --> 00:10:32,960
Kind of a quote, right? We have a quote. Is there a quote any any of any interest?

132
00:10:32,960 --> 00:10:44,240
It is just as if a man traveling in a forest should come across an ancient road an ancient path

133
00:10:44,480 --> 00:10:46,480
Traversed by people in former times

134
00:10:47,200 --> 00:10:54,960
Proceeding along it. He comes to an ancient city an old royal citadel lived in by people in former times with parks and graves

135
00:10:55,760 --> 00:10:59,920
Oh, groves water tanks and walls a truly delightful place

136
00:10:59,920 --> 00:11:05,040
I'm supposed that this man should tell of his discovery to the King moral minister saying

137
00:11:05,600 --> 00:11:09,920
Sorry, you should know that I've discovered an ancient city. You should restore that place

138
00:11:10,480 --> 00:11:15,440
Then suppose that ancient city was restored so that it became prosperous flourishing

139
00:11:15,920 --> 00:11:19,280
Opulus and was filled with folk and it grew and expanded

140
00:11:19,920 --> 00:11:23,440
In the same way. I have seen an ancient road an ancient path

141
00:11:23,440 --> 00:11:30,480
Traversed by the fully enlightened with us of former times and what is that path? It is the noble equal to path

142
00:11:35,920 --> 00:11:37,920
And as a result he cleared the path

143
00:11:39,440 --> 00:11:41,600
And led people down the path

144
00:11:42,720 --> 00:11:44,720
to the great city

145
00:11:44,720 --> 00:11:54,560
And many people followed him and entered into the great cities into the third

146
00:11:55,280 --> 00:11:57,280
It's a nice way for me

147
00:11:57,280 --> 00:11:57,680
It is

148
00:11:57,680 --> 00:12:01,840
The idea of the practice being a path known as very important

149
00:12:09,360 --> 00:12:11,360
The idea that we are

150
00:12:11,360 --> 00:12:16,640
We are going somewhere to a destination. We've never been there

151
00:12:18,080 --> 00:12:20,720
And so the idea of needing a guide is

152
00:12:22,480 --> 00:12:24,480
Important to be clear on

153
00:12:26,160 --> 00:12:30,560
People who think they can just find the way themselves have to think

154
00:12:32,160 --> 00:12:35,120
The person has never been down the path and never been to the

155
00:12:35,120 --> 00:12:41,760
Destination. How would they know when they're getting closer? How would they know when they're on the right path?

156
00:12:42,400 --> 00:12:45,600
Most likely wouldn't and they'd very easily get off the path

157
00:12:46,400 --> 00:12:48,400
All onto the wrong path

158
00:12:48,880 --> 00:12:50,880
Having a guide is important

159
00:12:52,400 --> 00:12:54,880
And bring this up to people sometimes when you talk about

160
00:12:56,080 --> 00:12:58,400
Just trying to figure things out themselves

161
00:13:00,240 --> 00:13:02,240
Find their own way

162
00:13:02,240 --> 00:13:06,240
There's no reason to think you should be able to find your own way

163
00:13:07,360 --> 00:13:08,880
Quite

164
00:13:08,880 --> 00:13:10,880
Gimbal

165
00:13:17,600 --> 00:13:21,520
And like a path it's something that you have to travel you have to do

166
00:13:22,640 --> 00:13:25,280
And there's no it's not just going to come to you

167
00:13:25,280 --> 00:13:32,320
You're not just going to sit still for a while and enlighten me just going to hit you in the back of the head

168
00:13:33,200 --> 00:13:35,200
We have to walk the path

169
00:13:37,360 --> 00:13:39,600
And don't walk the path so never reach the goal

170
00:13:43,520 --> 00:13:47,520
Ponte would you just explain briefly what the eightfold path is?

171
00:13:48,480 --> 00:13:51,280
I don't know that my friends understands what that is

172
00:13:51,280 --> 00:13:54,960
You tell me

173
00:13:56,320 --> 00:13:58,320
Well, it's um

174
00:13:58,880 --> 00:14:01,600
You know correct you okay, that sounds good

175
00:14:03,200 --> 00:14:07,600
It's what would us try to do living through their daily life

176
00:14:08,320 --> 00:14:11,920
eight different areas where you you try to be very mindful

177
00:14:13,200 --> 00:14:15,200
and to

178
00:14:15,760 --> 00:14:17,600
Work towards

179
00:14:17,600 --> 00:14:23,680
Just being better in each area the first being um to have the right intentions

180
00:14:25,280 --> 00:14:27,280
Right to you is the first

181
00:14:27,600 --> 00:14:35,680
I'm sorry. What was that right to view is the first one. Oh, I'm sorry. Okay, so right view. So understanding what what the teachings are and

182
00:14:36,400 --> 00:14:38,400
right intentions

183
00:14:38,400 --> 00:14:40,640
Not having greed or anger or

184
00:14:40,640 --> 00:14:47,920
A delusion as your motivation having the right motivation um right action

185
00:14:48,800 --> 00:14:50,160
speech

186
00:14:50,160 --> 00:14:53,840
Oh, I'm sorry. I always get them in the wrong order. Right speech

187
00:14:55,040 --> 00:14:57,920
You know not being harsh with people not

188
00:14:59,920 --> 00:15:06,000
It's lying or spreading rumors or saying the wrong thing saying mean things

189
00:15:06,000 --> 00:15:10,080
um right action not killing people or

190
00:15:11,280 --> 00:15:13,280
Sexual misconduct. That's a big one

191
00:15:16,240 --> 00:15:18,240
Not stealing things like that

192
00:15:19,280 --> 00:15:21,280
um

193
00:15:22,160 --> 00:15:25,360
Right livelihood. There are certain jobs that Buddhists shouldn't have

194
00:15:26,000 --> 00:15:29,920
I'm not involved killing or selling poisons and

195
00:15:29,920 --> 00:15:37,360
um jobs that involve harm harm to being so anything that would involve killing like

196
00:15:38,000 --> 00:15:40,400
being a butcher or a fisherman or

197
00:15:41,840 --> 00:15:43,840
selling poisons and

198
00:15:45,680 --> 00:15:52,000
I think there are some other ones anything that would involve you know human trafficking or or slavery or anything like that

199
00:15:55,520 --> 00:15:57,840
And after right livelihood

200
00:15:57,840 --> 00:15:59,280
uh

201
00:15:59,280 --> 00:16:01,280
right concentration

202
00:16:02,960 --> 00:16:04,960
Oh, I'm sorry right effort

203
00:16:06,560 --> 00:16:08,560
Right concentration, which is

204
00:16:10,080 --> 00:16:12,560
Oh, I'm sorry. I'm really making a mess of this

205
00:16:12,560 --> 00:16:14,560
Concentration stuff

206
00:16:14,560 --> 00:16:16,560
Okay, so right effort

207
00:16:17,280 --> 00:16:20,480
I'm just putting a proper wholesome effort into

208
00:16:20,480 --> 00:16:31,680
Learning and in practicing all of this. I'm sorry. I forgot the one after effort again

209
00:16:31,680 --> 00:16:40,320
Right effort and right mindfulness then right mindfulness being mindful of what you're doing right concentration, which is

210
00:16:41,440 --> 00:16:43,440
involved with meditation

211
00:16:45,440 --> 00:16:47,440
And

212
00:16:47,440 --> 00:16:52,000
I'm sorry just just because I

213
00:16:53,040 --> 00:16:57,280
I'm on the spot. I'm totally forgetting everything. What's after right concentration?

214
00:16:58,160 --> 00:17:02,320
Well, it's it's the one dealing with inside but right mindfulness

215
00:17:03,440 --> 00:17:07,040
No, like that was a trick question right concentration number eight

216
00:17:07,840 --> 00:17:09,040
Okay

217
00:17:09,040 --> 00:17:11,040
So you got eight that was eight

218
00:17:11,280 --> 00:17:14,320
But actually after after right concentration comes

219
00:17:14,320 --> 00:17:17,600
Some on yana, which is number nine

220
00:17:19,440 --> 00:17:21,440
Which is right knowledge

221
00:17:22,720 --> 00:17:28,080
Which is just to get it to see the truth and to understand

222
00:17:29,520 --> 00:17:31,520
That which allows you to let go

223
00:17:34,480 --> 00:17:36,480
To see no truth

224
00:17:37,120 --> 00:17:39,120
But nothing is referring

225
00:17:39,120 --> 00:17:42,880
I'm just having this a picnic not really in a picnic just

226
00:17:44,240 --> 00:17:46,240
Conclusion that comes from the observation

227
00:17:47,680 --> 00:17:49,680
reality

228
00:17:50,320 --> 00:17:52,480
And after some on yana and some of the

229
00:17:53,280 --> 00:17:55,280
That's right freedom

230
00:17:55,280 --> 00:17:57,280
right liberation

231
00:17:59,280 --> 00:18:02,320
So having seen the truth you have freed from

232
00:18:03,520 --> 00:18:05,360
the

233
00:18:05,360 --> 00:18:06,880
shackles of

234
00:18:06,880 --> 00:18:08,880
Delusion

235
00:18:08,880 --> 00:18:10,880
And you don't cling to things

236
00:18:11,520 --> 00:18:14,000
I don't do things that got some harm

237
00:18:15,200 --> 00:18:17,520
Don't things that don't do things that create separate

238
00:18:26,000 --> 00:18:28,000
Thank you, Bumpy

239
00:18:28,000 --> 00:18:31,520
Thank you. I think I need to practice up on my eightfold path

240
00:18:33,680 --> 00:18:34,880
Good

241
00:18:34,880 --> 00:18:36,880
I have read about unconscious beings

242
00:18:38,080 --> 00:18:39,120
Hasan

243
00:18:39,120 --> 00:18:41,920
Hasan asata from one of the realms of existence

244
00:18:42,560 --> 00:18:44,560
Do we know much about these beings?

245
00:18:46,640 --> 00:18:48,640
I don't know much about those beings

246
00:18:49,040 --> 00:18:51,040
I think some people do

247
00:18:52,000 --> 00:18:54,880
They are a bit of a mystery like my teacher said

248
00:18:56,240 --> 00:18:58,880
There has to be some some

249
00:18:58,880 --> 00:19:03,840
Not some awareness or something

250
00:19:03,840 --> 00:19:05,840
He figures that has to be something

251
00:19:07,360 --> 00:19:10,800
Hasan is that that means they have no they don't have any

252
00:19:12,320 --> 00:19:14,800
Any mind, but he said there has to be mind

253
00:19:16,080 --> 00:19:20,000
I mean, I'm gonna be fuddled as well if there's no mind how it can continue on

254
00:19:22,000 --> 00:19:25,040
I mean, I've been done with scholars will tell you it's because of this or that but

255
00:19:25,040 --> 00:19:27,840
But please speaking it doesn't make much sense

256
00:19:38,160 --> 00:19:44,000
Would that be different kind of person who's unconscious in any human realm just for example being in a coma or something?

257
00:19:45,840 --> 00:19:47,840
Yeah, I don't know what a coma is like

258
00:19:49,680 --> 00:19:52,960
I think it's supposed to be different. It's being born without a mind

259
00:19:52,960 --> 00:19:54,960
It's a good idea

260
00:19:58,160 --> 00:20:03,280
But it doesn't say a son means non-recipient or not aware of anything

261
00:20:03,920 --> 00:20:05,920
So maybe it's being born without senses

262
00:20:07,360 --> 00:20:10,800
It's a Brahma realm. I think consider to be one of the Brahma realm

263
00:20:10,800 --> 00:20:18,800
How does right view differ from right knowledge?

264
00:20:23,040 --> 00:20:25,520
Robin how does right view differ from right knowledge?

265
00:20:28,000 --> 00:20:30,000
Oh right knowledge um

266
00:20:30,000 --> 00:20:40,000
Right view

267
00:20:46,400 --> 00:20:52,480
Well, if you get technical all of the eight all the eight full path factors

268
00:20:52,480 --> 00:20:59,440
Together when they are perfect that is right knowledge

269
00:21:01,040 --> 00:21:03,040
so

270
00:21:04,000 --> 00:21:11,520
The ninth one is just a way of saying talking about that moment when they come together and when your view becomes knowledge

271
00:21:14,480 --> 00:21:18,080
So up into that point it's kind of becoming clearer to you

272
00:21:18,080 --> 00:21:26,000
But that's the case, but at the moment that it becomes knowledge that someone I mean

273
00:21:27,200 --> 00:21:31,440
It's the Buddha's teaching right? This is many of the Buddha's teachings are teachings

274
00:21:31,440 --> 00:21:35,680
They're meant to teach you so the person listening is supposed to learn something

275
00:21:35,680 --> 00:21:42,000
It's not meant to be you know putting things in in putting pegs in holes or

276
00:21:42,000 --> 00:21:48,480
It's not meant to be theoretical. So he's there's a license being taken here

277
00:21:48,480 --> 00:21:52,240
But that's what it means when the full path factors become

278
00:21:52,960 --> 00:21:58,480
When you practice a full path factors when they become mature and there's the most of them. Yeah

279
00:21:59,360 --> 00:22:02,560
And with right knowledge then there's right freedom

280
00:22:05,520 --> 00:22:07,520
So it's not actually different

281
00:22:07,520 --> 00:22:14,560
Except that when you could say when right view has the other seven path factors together and they're all purified

282
00:22:15,600 --> 00:22:17,600
They're all perfect and there is

283
00:22:18,640 --> 00:22:20,640
someone

284
00:22:24,160 --> 00:22:28,880
One thing may you speak on the stillness in the practice of meditation. Thank you

285
00:22:31,840 --> 00:22:34,720
It's called passive tea it's one of the potential

286
00:22:34,720 --> 00:22:37,440
defilements of insight because

287
00:22:38,160 --> 00:22:40,160
People who become still

288
00:22:40,160 --> 00:22:45,120
Get attached to the stillness and think that they've attained something special and they follow it

289
00:22:45,280 --> 00:22:47,280
They think that's the path

290
00:22:47,520 --> 00:22:49,920
And since it's not the path they get off the path

291
00:22:51,280 --> 00:22:55,440
So when you're still you should acknowledge there's still still required quite

292
00:23:00,800 --> 00:23:02,800
It's not a bad thing

293
00:23:02,800 --> 00:23:07,920
But it's something that can get in the way of seeing clearly if you're not mindful

294
00:23:17,840 --> 00:23:20,320
So tomorrow is the campfire at McMaster

295
00:23:22,000 --> 00:23:26,640
Yeah, that sounds nice. We'll just campfire here. I think we will have someone take pictures

296
00:23:27,600 --> 00:23:29,600
We should

297
00:23:29,600 --> 00:23:35,360
Someone should take pictures if you can take pictures in the dark

298
00:23:37,520 --> 00:23:39,520
We have a good camera you can

299
00:23:50,960 --> 00:23:54,720
So is that it for our questions? We are all cut up on questions

300
00:23:54,720 --> 00:23:56,720
Thank you

301
00:23:58,960 --> 00:24:03,280
Thank you everyone. I'm coming out. Thanks Robin. You're your help

302
00:24:04,320 --> 00:24:06,320
Thank you your teachings

303
00:24:06,320 --> 00:24:08,960
Thank you, Mum. Thank you for straightening me out

304
00:24:08,960 --> 00:24:24,720
We're goodnight of you. Goodnight

