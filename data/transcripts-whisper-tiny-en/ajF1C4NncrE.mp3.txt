How could I be mindful when I come across some sangas who do not present or follow the
ways and rules of the Buddha Dhamma, because the sanga are what we learn from and look
up to them in this Buddha sasana time?
How could I be mindful when I come across them?
Well, the first important thing is to be clear that such sangas exist.
Too often we get shocking letters from people saying, I can't believe it.
I met a monk who does X, Y, or Z. How could it be possible?
It's important to understand that these kind of hypocrites, I suppose you would say,
are not to be too harsh, but people who are unable to follow the teachings that they have
taken on a bit enough more than they could chew maybe as a kind of a nice way of putting
it.
That they exist, and that will help with your mindfulness if you are prepared for it.
But on the other hand, not to be too skeptical.
The other thing that we often get is we hear lay people criticizing monks for doing silly
things, like lay people who criticize monks for eating meat.
Those monks eat meat.
How could they possibly, I mean, as far as we know, the Buddha Himself ate meat, we're
not, there's no clear case, but from the teachings that we have in our tradition, it's
pretty clear that the Buddha was not upset at all if a monk ate meat unless the Buddha
wasn't upset, but the Buddha wasn't against monks eating meat, provided that it had
been not killed for them or provided that they didn't have any idea that had been killed
for their purpose.
For example, and you hear this in many different levels, monks who are really doing good
things and may just not be totally dedicated to meditation, maybe they practice some meditation
or maybe they get too busy with other things to practice enough meditation and still people
will give them a hard time.
His man who came to visit me recently, he said, it's really a shame to hear people criticize
because he said, you know, you have to ask them, what could you do it?
Could you put on those wrongs?
It's easy for you to say, oh, this monk is bad, and that monk is bad.
Well, why aren't you among showing them how to be a good monk, right?
I mean, this is one thing that you have to be careful about as well, is over criticism
because when you say that they are not following the ways and rules, well, to what extent,
I mean, for the most part, even the really, not the really bad ones, but even some of
the monks who I wouldn't find impressive or that I might criticize or say things about
in private, when you look at it, they're actually doing good things.
They actually have many good qualities to them.
So I think that's important, and the Buddha was quite clear about this as well.
When you come upon someone who's verbal and physical acts are not very pure, but they
have mental good acts, so their mind is in the right place, so they have good intentions,
they just get everything wrong, then you think, well, you know, at least they're good
and mental in the mind, or if they have good speech but not good actually, you think,
well, at least their speech is good, or if their action is good, or if some of their actions
are good, then you can look at that as well and try to be impartial about it, because
that's a very important part of what mindfulness is, being impartial.
So the other thing is understanding that beings go according to their karma, so if these
beings are acting in a way that is actually unwholesome, especially using the Buddha's
teaching as a kind of a cover for that unwholesome behavior, then it's quite sad, actually.
It's quite something that is quite shocking when you think about how they are going to
have to suffer, so you can certainly get rid of your aversion or your anger or your frustration
at them or your criticism towards them, when you think of how pitiful they are and how
pitiable they are, how worthy of your pity they are, because, you know, like when you
would have said, like, for example, when you see a man walking towards a pit full of
burning hot embers, you think, wow, if that man continues on that path, he's going to fall
into that pit of embers and suffer horribly.
So what would you think, would you be angry at that man or would you think, would you
start criticizing and look at that, why is that dumb, you know, that terrible person?
What are they doing walking towards that pit?
That's really the case here, if people are really immoral and doing moral deeds, then
you should really look upon them in this way, and this is something else that helps.
Ultimately, it just, the only real answer to your question is to be mindful, learn how
to be mindful, learn what it means to be mindful, learn to see that the sun guy is not
real, the sun guy is a concept, a monk is a concept, person is a concept, all of these
things are not real, what is real is your experience moment to moment to moment, and as
with people dying, as with everyone, that'll be the best interaction that you have with
these people.
You can't change them and you can't force them to change, but you can purify your own
interactions with them, because the purer you are, the more closer you'll be to other
pure beings, and the more able you will be to supporting good things and to supporting
the sun guy and to supporting beneficial organizations and so on.
The better how you will be in this world, this is why we're out here encouraging people
to be good, because we know it depends on the people's minds, you have to develop yourself.
We have to develop ourselves before we can help other people.
Is it possible to say something or don't they hear?
The sun guy is made of individuals and all these individuals do probably the best they
can to follow the rules, but you can't do it perfect for everybody, no matter what you
do, you will always find somebody who finds fault and look at me, for example, today
I'm keeping the vineyard, but many people will find fault in it, because I'm not wearing
my robes as I should wear my robes as a bikuni, but many people who see the videos now
out in the world will say, how can she sit there without her shirt on, but in fact in the
Buddha's time the bikinis weren't wearing blouses, so in the villa, it is not stated that the
bikuni should wear blouses, we should wear the same kind of robes as a monk with two additional
parts, which of which none is a blouse, so I'm doing good in the villa sense, but for
some I might not do good, because I might hurt their feelings or something, so you can't
do it right for everybody.
