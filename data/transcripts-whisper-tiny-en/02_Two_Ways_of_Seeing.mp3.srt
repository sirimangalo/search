1
00:00:00,000 --> 00:00:08,640
two ways of seeing, our practice is called vipassana havana, or vipassana meditation and

2
00:00:08,640 --> 00:00:15,440
the goal is to attain vipassana, passana means seeing and we implies that it is in a

3
00:00:15,440 --> 00:00:22,400
spatial or exceptional way. The word vipassana implies that there are two ways of seeing

4
00:00:22,400 --> 00:00:29,040
and that one of the ways superior to the other. Vipassana means seeing the three characteristics

5
00:00:29,040 --> 00:00:38,560
impermanence, suffering and non-self, panikka, dukha, and anata. The other way of seeing is the

6
00:00:38,560 --> 00:00:46,880
probability. It is the ordinary way of seeing, nikka, stability or permanency, sukha, happiness

7
00:00:46,880 --> 00:00:56,000
or satisfaction. And ata, self, our ordinary way of seeing things without vipassana gives

8
00:00:56,000 --> 00:01:02,320
primacy to things. We could say both and both concepts, but we are more familiar with things

9
00:01:02,320 --> 00:01:10,160
because we do not realize that things are just concepts. People places, things, positions, entities.

10
00:01:10,720 --> 00:01:17,200
The way we look at reality gives primacy to these concepts as impersonal, unshakable,

11
00:01:17,200 --> 00:01:23,120
unchanging things, taking our attention away from the truth, the reality of experience.

12
00:01:23,120 --> 00:01:28,960
We do not trust our senses because the impression we get from them might be wrong.

13
00:01:29,600 --> 00:01:35,360
Rather than not trust our senses themselves. However, we should simply not trust the impressions

14
00:01:35,360 --> 00:01:40,640
or conclusions we throw from our senses. This is an important distinction. We normally

15
00:01:40,640 --> 00:01:46,960
mistrust our senses, a childly. Letting science tell us what is reality. Science has wonderful

16
00:01:46,960 --> 00:01:53,840
ways of explaining the reality of entity is to us, molecules, atoms, subatomic particles, etc.

17
00:01:54,480 --> 00:01:59,680
We think that things exist and more practically we think that things that we can see,

18
00:02:00,400 --> 00:02:09,520
people, places, objects and positions exist. Direct experience is relegated to a kind of epiphenomenon,

19
00:02:10,160 --> 00:02:15,360
like a byproduct of things. It is no wonder that we have this false sense of stability,

20
00:02:15,360 --> 00:02:21,680
satisfaction and control, a false sense of existence. There being things for ourselves,

21
00:02:22,320 --> 00:02:30,480
me, mine, you, and her, things. The ordinary way of looking at reality is problematic,

22
00:02:30,480 --> 00:02:37,920
because whether or not we trust our senses, they take precedence over things, atoms or subatomic

23
00:02:37,920 --> 00:02:44,480
particles as things have no bearing on our stress or happiness or everything that is truly important.

24
00:02:45,200 --> 00:02:51,120
It is not important, which paradigm is good and good, correct. It is important that our

25
00:02:51,120 --> 00:02:58,640
ordinary way of seeing is not helpful, harmful even as it relegates our experience to an inferior

26
00:02:58,640 --> 00:03:04,720
position that we disregard because of how valuable our impressions of experience can be.

27
00:03:04,720 --> 00:03:13,200
In doing so, we ignore what is most basic to our existence. The other way of looking at reality

28
00:03:13,200 --> 00:03:18,960
is to see beyond the conceptual realm of things, but reminding ourselves that things do not exist.

29
00:03:19,600 --> 00:03:25,520
Take the body, for example, does the body exist? We know from science that the body is made

30
00:03:25,520 --> 00:03:32,080
up of cells. Does the body exist in the same way as cells do? No matter how big or how small things

31
00:03:32,080 --> 00:03:38,800
are or how finally they are divided into parts, their existence never reaches the level of reality

32
00:03:38,800 --> 00:03:46,880
of experiences. Everything that we know relating to things is based on experience. Without experience,

33
00:03:46,880 --> 00:03:53,760
nothing else is truly meaningful. Experience is always primary. Things are secondary and depend on

34
00:03:53,760 --> 00:04:00,320
our experience. There is subjectivity with things because two people can look at the same thing

35
00:04:00,320 --> 00:04:07,520
and have very different ideas about it. Whether it is good, bad, beautiful, ugly, etc.,

36
00:04:08,080 --> 00:04:14,160
experiences are objective as they maintain their nature no matter what people see them to be.

37
00:04:15,280 --> 00:04:22,000
Experiential reality is also more useful because all of the problems in life like depression,

38
00:04:22,000 --> 00:04:29,520
anxiety, addiction, etc., are not a part of things. They are part of experiences. Putting

39
00:04:29,520 --> 00:04:35,920
experiences first, changes are entire outlook on life. That is what the Barcelona means.

40
00:04:36,720 --> 00:04:41,520
gaining a new perspective by changing what we put as primary in our outlook.

41
00:04:42,320 --> 00:04:48,160
We still acknowledge the conceptual existence of people, places and things as they are quite

42
00:04:48,160 --> 00:04:55,600
necessary for practical life. At the same time, we becoming touched with the reality of our experiences

43
00:04:55,600 --> 00:05:02,320
to the extent that we are no longer distracted by conceptual thought. As a result, we are

44
00:05:02,320 --> 00:05:09,040
able to see through our conceptual misunderstandings about reality, especially regarding the three

45
00:05:09,040 --> 00:05:15,040
characteristics. The three characteristics are quite difficult to understand when we think in terms of

46
00:05:15,040 --> 00:05:22,160
things. What does it mean to say that something is unstable? Why the nature of things is that

47
00:05:22,160 --> 00:05:28,880
they are stable, able to satisfy and subject to our control. Unfortunately, because things are

48
00:05:28,880 --> 00:05:34,960
conceptual and experiences are real, we are never actually able to find stability, satisfaction

49
00:05:34,960 --> 00:05:42,560
and control. Experiences are not stable, they arise and see it quite rapidly. As a result of this,

50
00:05:42,560 --> 00:05:47,600
they are unable to be a source of satisfaction. In the way we perceive things to be,

51
00:05:47,600 --> 00:05:54,880
likewise, we cannot see, they have selfness or substance to them. No can they be taken as positions

52
00:05:54,880 --> 00:06:02,960
or part of another self. They are simply experiences. When we see reality as experiences,

53
00:06:02,960 --> 00:06:08,800
we lack of addiction because we no longer conceive of things that might satisfy us.

54
00:06:09,520 --> 00:06:15,760
We stop seeing in terms of things which would have the potential in our minds to satisfy us.

55
00:06:15,760 --> 00:06:21,280
If it were only possible to experience them instead of momentary experiences, things are not what

56
00:06:21,280 --> 00:06:28,960
we experience. We experience experience. We experience seeing, pairing, smelling, tasting, feeling,

57
00:06:28,960 --> 00:06:35,440
thinking because experiences are required for enjoying things and experiences are unstable.

58
00:06:35,440 --> 00:06:42,960
There is really nothing that can truly satisfy us. Words, experiences are not under our control.

59
00:06:42,960 --> 00:06:50,560
They do not belong to us, thus clinging to anything becomes futile and of course for great disappointment

60
00:06:50,560 --> 00:06:56,400
and dissatisfaction. This is the simple truth about the nature of reality. All questions about

61
00:06:56,400 --> 00:07:02,240
self and non-self, does the self exist, etc., are totally of-based questions.

62
00:07:03,360 --> 00:07:06,080
Because the questions are based on the perception of things,

63
00:07:06,080 --> 00:07:13,680
non-self simply means that experiences do not belong to you. They are not you. They are not

64
00:07:13,680 --> 00:07:21,440
a you. They are not a thing, an entity. They are experiences that arise and sees.

65
00:07:22,160 --> 00:07:26,800
This understanding is not about realizing that the things you thought were permanent are

66
00:07:26,800 --> 00:07:32,560
impermanent. It is true that this is one way of explaining impermanence in a very superficial way.

67
00:07:32,560 --> 00:07:37,120
But true understanding is much deeper than that or at least very different from that.

68
00:07:37,840 --> 00:07:43,600
The three characteristics mean that you change the way you look at the reality from things

69
00:07:43,600 --> 00:07:50,480
that are stable, satisfying and controllable, which people places and things are to seeing

70
00:07:50,480 --> 00:07:56,720
in terms of experiences, which are impermanent and satisfying and controllable.

71
00:07:56,720 --> 00:08:03,200
This may sound like a bad deal. You may ask why you would want to trade in a perception of

72
00:08:03,200 --> 00:08:08,560
reality based on stability, satisfaction and control for one based on the opposite.

73
00:08:09,280 --> 00:08:14,400
Indeed, perceiving things is good in a worldly sense. As a result of our conventional framework

74
00:08:14,400 --> 00:08:20,400
of reality, we can build nuclear reactors, space, stations and computers. However, this has no

75
00:08:20,400 --> 00:08:27,280
bearing on our happiness or suffering, because again, in terms of what is good for us, experience

76
00:08:27,280 --> 00:08:34,880
is timely. If you live in the world of concepts or things, you are going to get bored, because

77
00:08:34,880 --> 00:08:39,200
you are ignoring the underlying reality of impermanence, suffering and non-self.

78
00:08:40,480 --> 00:08:47,200
If instead you see through the lens of experience, you are in one rebel to disappointment or upset

79
00:08:47,200 --> 00:08:52,400
as you are truly living in reality, keeping up with reality instead of fighting or living in fear

80
00:08:52,400 --> 00:09:00,480
of it. Suddenly, you fit with the war, with reality. You become natural, you become in tune with

81
00:09:00,480 --> 00:09:07,200
nature. There is no dissonance between you and reality. No worry about the future or sadness

82
00:09:07,200 --> 00:09:15,520
about the past. Both are all caught up in concepts, things and self. Experience is always

83
00:09:15,520 --> 00:09:23,280
only present here and now being born and dying every moment. The practice of mindfulness

84
00:09:23,280 --> 00:09:29,760
based on the four foundations of mindfulness is meant to help us see reality from the point of view

85
00:09:29,760 --> 00:09:38,560
of experience as follows. Number one, physical experience. When you walk, stepping right,

86
00:09:38,560 --> 00:09:45,040
there is an experience there. There are many experiences in walking. The right foot is one

87
00:09:45,040 --> 00:09:52,640
experience and the left foot is another experience. When you sit and say rising, falling,

88
00:09:52,640 --> 00:10:01,440
those are each physical experiences, two feelings. When you feel pain and you focus on the pain

89
00:10:01,440 --> 00:10:09,120
and say pain pain, for as long as it lasts, you see it as experience. Many moments of experience,

90
00:10:09,120 --> 00:10:20,160
three thoughts, thinking about past or future events. You see thoughts not as my past or my future,

91
00:10:20,160 --> 00:10:26,480
but rather as experiences of thinking. Likewise, good thoughts and bad thoughts become merely

92
00:10:26,480 --> 00:10:33,120
thoughts as you let go of any concealing about the thoughts. Four, mind states. When you like

93
00:10:33,120 --> 00:10:40,000
something, dislike something or experience, other mind states, likerowsiness, destruction,

94
00:10:40,000 --> 00:10:47,760
worry, anxiety, doubt, etc. Instead of being tortured or controlled by them and having to take drugs

95
00:10:47,760 --> 00:10:55,200
or medication to elevate them, you see them just as they are as experiences. We take drugs and

96
00:10:55,200 --> 00:11:01,280
medication because they perceive a mind state as problems. Whether mind states are extreme,

97
00:11:01,280 --> 00:11:08,240
we take medications as means of fixing the perceived problem. If you see your mind states

98
00:11:08,240 --> 00:11:13,280
just as part of your experience, it doesn't matter how intense or overwhelming they are,

99
00:11:13,280 --> 00:11:19,920
they just are. They are experiences. We are so accustomed to reacting and thinking of emotions

100
00:11:19,920 --> 00:11:27,440
as my emotions, as me experiencing emotions and even as the emotions being things are a part of

101
00:11:27,440 --> 00:11:34,960
ourselves. Like people say, I have depression, I am depressed, or I am clinically depressed,

102
00:11:34,960 --> 00:11:41,600
depression is an emotion that arises for a moment and then is gone because it comes and goes

103
00:11:41,600 --> 00:11:49,360
repeatedly, we perceive it to be everlasting entity, a thing. It is only when we lose sight of the

104
00:11:49,360 --> 00:11:55,280
moment reality of experiences that we suffer from things like depression. The same goes for

105
00:11:55,280 --> 00:12:05,360
most mental illnesses. The senses, seeing, hearing, smelling, testing, feeling, thinking all

106
00:12:05,360 --> 00:12:11,120
have great power to torture us, which is kind of silly because seeing is really just seeing.

107
00:12:11,760 --> 00:12:18,160
How is it that when you see a spider, you fake out or when you see something ugly, you become

108
00:12:18,160 --> 00:12:24,400
nauseous. Whatever you see, it is merely an experience of seeing. We are conditioned to react to

109
00:12:24,400 --> 00:12:30,000
our experiences or whether we condition ourselves and our encouraged by external influences to

110
00:12:30,000 --> 00:12:36,400
become conditioned. Mindfulness is about becoming unconditioned, objective, seeing things as they are

111
00:12:36,400 --> 00:12:43,040
and becoming in tune with reality. Seeing clearly, we pass now and mindfulness, sati are the

112
00:12:43,040 --> 00:12:50,560
two most important parts of our practice. We practice mindfulness to see clearly when we see reality

113
00:12:50,560 --> 00:12:56,560
clearly as it is, we fear ourselves from all suffering of our ignorance and delusion about the nature

114
00:12:56,560 --> 00:13:24,560
of our minds and the experiential word around us.

