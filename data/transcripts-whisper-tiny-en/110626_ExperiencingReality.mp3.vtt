WEBVTT

00:00.000 --> 00:05.000
Hello, welcome back to Askamunk.

00:05.000 --> 00:11.000
Some time ago, I did a video on what meditation is not,

00:11.000 --> 00:16.000
or specifically those experiences that are not insight meditation,

00:16.000 --> 00:22.000
or not meditation into the nature of reality.

00:22.000 --> 00:27.000
So these are sort of experiences and meditation,

00:27.000 --> 00:31.000
which on the surface appear to be positive beneficial,

00:31.000 --> 00:35.000
and are generally regarded to be good experiences,

00:35.000 --> 00:38.000
but when the meditator follows after them,

00:38.000 --> 00:43.000
and clings to them and becomes attached to them,

00:43.000 --> 00:46.000
they take one away from the purpose,

00:46.000 --> 00:48.000
which is to see things as they are.

00:48.000 --> 00:51.000
So these are experiences like states of happiness,

00:51.000 --> 00:53.000
states of peace, states of calm,

00:53.000 --> 00:59.000
states of supernatural knowledge of things far away,

00:59.000 --> 01:02.000
or knowledge of the past, knowledge of the future,

01:02.000 --> 01:04.000
and so on.

01:04.000 --> 01:08.000
Many supposedly good experiences,

01:08.000 --> 01:11.000
and in many types of meditation,

01:11.000 --> 01:13.000
these experiences are considered to be a goal,

01:13.000 --> 01:16.000
or a purpose for practicing meditation.

01:16.000 --> 01:18.000
Now in insight meditation,

01:18.000 --> 01:22.000
our intention is to see reality and understand reality,

01:22.000 --> 01:26.000
which is our experience of the world for what it is,

01:26.000 --> 01:28.000
and to do away with our misconceptions

01:28.000 --> 01:31.000
and to do away with our misunderstanding.

01:31.000 --> 01:36.000
So it's important to understand the truth of these experiences

01:36.000 --> 01:38.000
and to see them for what they are,

01:38.000 --> 01:43.000
as simply states of emotion or feeling or knowledge

01:43.000 --> 01:46.000
or experience that come and go,

01:46.000 --> 01:51.000
and there's nothing intrinsically special about them,

01:51.000 --> 01:54.000
and if one can take them as simply an experience

01:54.000 --> 01:58.000
and let them go and move on, then there'll be no problem.

01:58.000 --> 02:02.000
Now in this video, I wanted to talk about those experiences,

02:02.000 --> 02:05.000
which are insight meditation,

02:05.000 --> 02:07.000
or are a part of insight meditation,

02:07.000 --> 02:11.000
but are conversely often understood by the meditator

02:11.000 --> 02:13.000
to be wrong practice,

02:13.000 --> 02:16.000
or to be a sign that things are going wrong,

02:16.000 --> 02:18.000
or that one is practicing incorrectly.

02:18.000 --> 02:21.000
So we have both sides.

02:21.000 --> 02:23.000
The other video talked about those things,

02:23.000 --> 02:28.000
which the meditator misconceived to be beneficial,

02:28.000 --> 02:32.000
misconceived to be good practice, proper practice,

02:32.000 --> 02:34.000
and here I'd like to talk about those things

02:34.000 --> 02:37.000
that the meditator will often understand,

02:37.000 --> 02:41.000
misunderstand to be unbeneficial or improper practice,

02:41.000 --> 02:43.000
or a sign that practice is going wrong.

02:43.000 --> 02:47.000
When in fact, these things are a sign that one is practicing correctly.

02:47.000 --> 02:51.000
Now to understand this, it's important for us to understand

02:51.000 --> 02:56.000
what we mean by meditation, specifically insight meditation.

02:56.000 --> 03:02.000
The teaching of the Buddha is based on the fact that inside of all of us,

03:02.000 --> 03:06.000
we have three things which we would be better off without,

03:06.000 --> 03:12.000
which exist in our mind and our cause for all of our suffering.

03:12.000 --> 03:18.000
These we, in English, we translate them as greed, anger,

03:18.000 --> 03:19.000
and delusion.

03:19.000 --> 03:24.000
These are sort of approximate names for these states.

03:24.000 --> 03:28.000
Greed as any partiality for something,

03:28.000 --> 03:34.000
anger is a name for any partiality against something.

03:34.000 --> 03:39.000
And delusion is the misunderstanding that leads us

03:39.000 --> 03:44.000
to be partial towards or against something.

03:44.000 --> 03:51.000
So these three things are the problem that we are trying to address

03:51.000 --> 03:55.000
in the Buddhist teaching or in the insight meditation tradition.

03:55.000 --> 03:57.000
Because obviously, if you misunderstand something,

03:57.000 --> 03:59.000
that's not a good thing, but the point is,

03:59.000 --> 04:04.000
once you misunderstand something, it's going to give rise to attachment

04:04.000 --> 04:09.000
to things that are to certain things,

04:09.000 --> 04:12.000
and repulsion from certain other things.

04:12.000 --> 04:15.000
And this is how we often will live our lives,

04:15.000 --> 04:19.000
being attached to certain experiences and repulsed by other experiences,

04:19.000 --> 04:23.000
and so we're constantly in a state of chasing after some things

04:23.000 --> 04:26.000
and running away from certain other things.

04:26.000 --> 04:32.000
Now this is a problem because as the Buddha pointed out,

04:32.000 --> 04:37.000
the nature of reality is that nothing is going to satisfy us.

04:37.000 --> 04:41.000
There is nothing which we are going to chase after

04:41.000 --> 04:45.000
and it's going to bring us peace, happiness, and freedom from suffering

04:45.000 --> 04:47.000
once we get it, once we attain it.

04:47.000 --> 04:50.000
The reason for this is because we have another three things,

04:50.000 --> 04:54.000
and this ties into the experiences that are going to come up in meditation.

04:54.000 --> 04:57.000
When we practice meditation correctly,

04:57.000 --> 04:59.000
we're going to see reality for what it is.

04:59.000 --> 05:03.000
Now, the reason we get attached to certain things

05:03.000 --> 05:05.000
and repulsed by other certain things,

05:05.000 --> 05:08.000
because we don't see the nature of reality,

05:08.000 --> 05:12.000
we become attracted to certain things based on three characteristics.

05:12.000 --> 05:16.000
We perceive certain things in the world to be permanent,

05:16.000 --> 05:19.000
we perceive certain things to be satisfying,

05:19.000 --> 05:23.000
and we perceive certain things to be under our control.

05:23.000 --> 05:25.000
We expect certain things to be this way.

05:25.000 --> 05:27.000
We hope that certain things will be this way,

05:27.000 --> 05:31.000
and we hope to attain states that are this way,

05:31.000 --> 05:33.000
that are permanent, that are satisfying,

05:33.000 --> 05:35.000
and that are under our control.

05:35.000 --> 05:38.000
Now, the reason why nothing is going to satisfy

05:38.000 --> 05:41.000
is going to ever bring us peace, happiness, and freedom from suffering

05:41.000 --> 05:43.000
is because there's nothing in the world,

05:43.000 --> 05:45.000
which is any of these three things.

05:45.000 --> 05:48.000
And this is where our misunderstanding lies.

05:48.000 --> 05:50.000
When we talk about the delusion part,

05:50.000 --> 05:53.000
the misunderstanding part, it's the misunderstanding

05:53.000 --> 05:56.000
that we're going to somehow find something that is permanent.

05:56.000 --> 05:59.000
We're going to somehow find something that is satisfying.

05:59.000 --> 06:03.000
We're going to somehow find something that is controllable,

06:03.000 --> 06:08.000
that we can say belongs to us, or is ours.

06:08.000 --> 06:11.000
So this is why we give rise to greed.

06:11.000 --> 06:14.000
When we see certain things,

06:14.000 --> 06:17.000
when we are confronted by certain experiences,

06:17.000 --> 06:22.000
we conceive of that object to be a source of stability,

06:22.000 --> 06:26.000
permanence, reliability.

06:26.000 --> 06:30.000
We conceive it to be therefore satisfying, and pleasant,

06:30.000 --> 06:37.000
and cause for us to be truly happy, and truly at peace with ourselves.

06:37.000 --> 06:39.000
And something that we can control,

06:39.000 --> 06:42.000
something that we can predict,

06:42.000 --> 06:47.000
that will listen and obey our wishes when we want it to be this way.

06:47.000 --> 06:50.000
It will be this way when we want it to be that way.

06:50.000 --> 06:54.000
When we want it to be it will be when we want it to not be, it will not be.

06:54.000 --> 06:58.000
Now, the reason, this is the reason why we become attached to things,

06:58.000 --> 07:01.000
and we chase after things, because we conceive them to be this way.

07:01.000 --> 07:04.000
We gain certain states of pleasure,

07:04.000 --> 07:08.000
and because we're not looking closely at it,

07:08.000 --> 07:11.000
we think, oh, this pleasure is going to last a long time,

07:11.000 --> 07:14.000
or this pleasure, if I work,

07:14.000 --> 07:18.000
it will last longer and longer, and eventually it will last forever.

07:18.000 --> 07:24.000
And so we work and work and work, and eventually we are dissatisfied.

07:24.000 --> 07:26.000
We give rise to the other side.

07:26.000 --> 07:31.000
So the reason we give rise to greed is because we conceive things to be,

07:31.000 --> 07:35.000
to be permanent, satisfying, and controllable.

07:35.000 --> 07:40.000
But when they are not, when, on, in fact,

07:40.000 --> 07:44.000
we realize the truth, we're confronted with the truth.

07:44.000 --> 07:49.000
We're confronted with what we would expect to be a source of wisdom and understanding.

07:49.000 --> 07:52.000
So that is, we see things change.

07:52.000 --> 07:57.000
We see things disappear and fall apart.

07:57.000 --> 08:04.000
We see things cause stress and be uncomfortably experienced discomfort.

08:04.000 --> 08:07.000
We experience suffering and pain and so on.

08:07.000 --> 08:10.000
And when we aren't able to control things,

08:10.000 --> 08:12.000
when things are out of our control,

08:12.000 --> 08:16.000
when experiences, when the reality, when our life goes out of our control,

08:16.000 --> 08:18.000
the things around us, even our own bodies,

08:18.000 --> 08:23.000
they disobey us, and we lose control of our faculties, our mind,

08:23.000 --> 08:26.000
our memory, and so on.

08:26.000 --> 08:30.000
Just about everything, it gives rise to suffering.

08:30.000 --> 08:33.000
It gives rise to anger, which is the other side.

08:33.000 --> 08:38.000
So the reason why we get angry is because of our experience,

08:38.000 --> 08:39.000
our disappointment.

08:39.000 --> 08:42.000
We thought things would be permanent and so on.

08:42.000 --> 08:45.000
And when they're not, we become angry and upset,

08:45.000 --> 08:53.000
also based on our expectation that we should be able to find our expectation of finding

08:53.000 --> 09:01.000
permanent satisfaction and control or mastery or control over our lives,

09:01.000 --> 09:04.000
over our reality.

09:04.000 --> 09:11.000
So these two states are caused by these expectations.

09:11.000 --> 09:13.000
Now the expectation is the delusion.

09:13.000 --> 09:15.000
So these are how the three things work.

09:15.000 --> 09:19.000
If we can remove the delusion, the misunderstanding,

09:19.000 --> 09:23.000
if we can see things and understand the truth,

09:23.000 --> 09:26.000
that all of reality is impermanent,

09:26.000 --> 09:29.000
is unsatisfying, is uncontrollable.

09:29.000 --> 09:31.000
If we're able to see this, then there would be no problem.

09:31.000 --> 09:36.000
It's possible to live in this world in peace, happiness, and freedom from suffering.

09:36.000 --> 09:40.000
The point is not to expect things to be other than what they are,

09:40.000 --> 09:44.000
because in fact life is like a dance, or it has a rhythm to it,

09:44.000 --> 09:45.000
and everything is changing.

09:45.000 --> 09:50.000
And if you can accept that rhythm, and if you can accept reality for what it is,

09:50.000 --> 09:54.000
whenever or whatever it is at any given time,

09:54.000 --> 09:58.000
if you can give up these expectations that things should be this way,

09:58.000 --> 10:01.000
or that way that there should somehow be some stability,

10:01.000 --> 10:07.000
that there should somehow be some satisfaction in an experience,

10:07.000 --> 10:11.000
and that somehow experience should be under your control.

10:11.000 --> 10:16.000
If you can give this up, then you can live in peace, happiness, and freedom from suffering.

10:16.000 --> 10:20.000
It means you have to give up any expectation,

10:20.000 --> 10:25.000
and any attachment to this experience or that experience.

10:25.000 --> 10:29.000
So this is what we're trying to achieve in inside meditation,

10:29.000 --> 10:32.000
and so we're therefore trying to come to understand,

10:32.000 --> 10:36.000
simply to see reality for what it is, to strengthen our minds,

10:36.000 --> 10:41.000
and to create this understanding and this wisdom,

10:41.000 --> 10:47.000
which allows us to anticipate change, and to expect change,

10:47.000 --> 10:51.000
so that when change occurs, to be ready for it.

10:51.000 --> 10:58.000
So once we've seen an experience change and come to realize that this is the core of reality,

10:58.000 --> 11:01.000
is that whatever arises ceases,

11:01.000 --> 11:04.000
everything that comes goes, everything is constantly changing,

11:04.000 --> 11:07.000
and therefore no experience can satisfy us,

11:07.000 --> 11:11.000
and you can't expect something to really bring you happiness,

11:11.000 --> 11:14.000
and you can't expect to find control,

11:14.000 --> 11:23.000
or to be able to control and manipulate and dominate, and be in charge of even your own life.

11:23.000 --> 11:26.000
If we can let go of all this, then we'll be truly peaceful and happy.

11:26.000 --> 11:30.000
This is what we're trying to gain, this understanding.

11:30.000 --> 11:35.000
So the problem is, when we come to practice any type of meditation,

11:35.000 --> 11:40.000
we bring all of these ideas, these misunderstandings with us.

11:40.000 --> 11:48.000
So involuntarily or unconsciously, we expect the meditation to be permanent or stable,

11:48.000 --> 11:54.000
to be pleasant and satisfying, and to be controllable.

11:54.000 --> 11:59.000
So we expect that our object of meditation should be all of these things.

11:59.000 --> 12:04.000
When we focus on the stomach, we expect that with some work and with some practice,

12:04.000 --> 12:09.000
it's going to be constant, so it will be rising and falling,

12:09.000 --> 12:14.000
constantly and smoothly, and therefore it will be pleasant,

12:14.000 --> 12:19.000
it will be satisfying, and the meditation will bring us a great amount of pleasure and happiness.

12:19.000 --> 12:21.000
And we'll be able to control it.

12:21.000 --> 12:26.000
We'll be the one saying, now rise, now fall, and our minds will never think,

12:26.000 --> 12:31.000
and will never wander, and will never give rise to any unpleasant situation.

12:31.000 --> 12:35.000
Eventually the pain will leave, and we'll be able to sit comfortably, and so on.

12:35.000 --> 12:39.000
And so on, this is how we look at it, this is what we expect.

12:39.000 --> 12:44.000
But we have to understand that this is not the purpose of meditation,

12:44.000 --> 12:53.000
this is not the proper goal, the true way to find peace, happiness, and freedom from suffering.

12:53.000 --> 12:56.000
It's just the baggage and the misunderstandings that we're bringing with us.

12:56.000 --> 12:59.000
What we're actually trying to do is do away with these expectations,

12:59.000 --> 13:02.000
the need for some kind of stability,

13:02.000 --> 13:07.000
and the need for some kind of pleasure, and the need to be able to control.

13:07.000 --> 13:11.000
What we're trying to do is be able to experience and interact,

13:11.000 --> 13:14.000
and be with reality whatever it is.

13:14.000 --> 13:23.000
And so what we're going to try to do is to give up these expectations

13:23.000 --> 13:26.000
to come to see that reality is not what we think it is.

13:26.000 --> 13:31.000
And this is what's going to happen when, for instance, as an example, you watch the stomach.

13:31.000 --> 13:35.000
If you say to yourself, rising, falling, and just watch the stomach rising,

13:35.000 --> 13:39.000
when it rises rising, when it falls falling,

13:39.000 --> 13:41.000
you're going to see all three of these things.

13:41.000 --> 13:43.000
You're going to see impermanence.

13:43.000 --> 13:47.000
So sometimes it will be deep, sometimes it will be shallow,

13:47.000 --> 13:52.000
sometimes it will be smooth, sometimes it will be in pieces, and so on.

13:52.000 --> 13:59.000
And you're going to see, basically, that it's changing.

13:59.000 --> 14:02.000
You're also going to see sometimes that it's uncomfortable,

14:02.000 --> 14:05.000
that there's a sense of physical discomfort,

14:05.000 --> 14:09.000
that the rising and the falling, because of our clinging to it,

14:09.000 --> 14:16.000
because we like certain states, and because we see it as being ours,

14:16.000 --> 14:19.000
it leads us to a state of discomfort.

14:19.000 --> 14:23.000
There's the stress in the stomach and the tension,

14:23.000 --> 14:27.000
and sometimes breath is not smooth and not comfortable.

14:27.000 --> 14:30.000
Because it's not according to our wishes.

14:30.000 --> 14:34.000
And we're going to also see that it's not according to our,

14:34.000 --> 14:37.000
it's not under our control.

14:37.000 --> 14:44.000
So sometimes it will go by itself, eventually,

14:44.000 --> 14:48.000
in higher stages, when the meditator gets more clear and inside,

14:48.000 --> 14:51.000
the rising and falling will appear to go by itself.

14:51.000 --> 14:54.000
But in the beginning, this is experienced by actually the feeling

14:54.000 --> 14:58.000
that we are controlling our breath, the feeling that we are making it,

14:58.000 --> 15:02.000
this misunderstanding that somehow we are controlling it,

15:02.000 --> 15:06.000
and that idea of control or the habit of trying to control things

15:06.000 --> 15:09.000
is going to create a great amount of suffering.

15:09.000 --> 15:12.000
This is how we see that it's not under our control.

15:12.000 --> 15:17.000
Because when we do exert our control, trying to make it rise and fall smoothly,

15:17.000 --> 15:21.000
I'm saying, okay, now rise, now fall, now rise, now rise.

15:21.000 --> 15:23.000
It's going to create stress and discomfort.

15:23.000 --> 15:27.000
You'll find that when you force it, you'll conceive,

15:27.000 --> 15:29.000
it feels like you're forcing it.

15:29.000 --> 15:32.000
You'll see that this is a great amount of stress and suffering.

15:32.000 --> 15:35.000
So this is seeing that actually it's not under your control

15:35.000 --> 15:38.000
and trying to control things is the real,

15:38.000 --> 15:43.000
a real source of stress and suffering.

15:43.000 --> 15:45.000
Now the problem is when these things arise,

15:45.000 --> 15:47.000
the first thing the meditator thinks,

15:47.000 --> 15:50.000
most meditators think is something's going wrong.

15:50.000 --> 15:52.000
They think that either A, this meditation is no good

15:52.000 --> 15:55.000
or B, they're not practicing it correctly.

15:55.000 --> 16:00.000
Now it's important to understand that actually a person who realizes these things

16:00.000 --> 16:05.000
who sees the changing, who sees the discomfort,

16:05.000 --> 16:09.000
who sees the uncontrollability of the experience.

16:09.000 --> 16:12.000
This is a person who is starting to understand reality,

16:12.000 --> 16:14.000
to see that things the way they are,

16:14.000 --> 16:17.000
and to change their misperceptions about reality,

16:17.000 --> 16:21.000
to open up and to free themselves of these expectations

16:21.000 --> 16:24.000
of the dependency on a specific type of experience

16:24.000 --> 16:29.000
and the inability to bear with other types of experience.

16:29.000 --> 16:32.000
Once we can experience this,

16:32.000 --> 16:34.000
which is actually quite a difficult thing to do,

16:34.000 --> 16:37.000
simply watch the stomach and all of its changes

16:37.000 --> 16:41.000
and all of its variations without becoming upset

16:41.000 --> 16:47.000
without becoming frustrated and angry and fallen to suffering.

16:47.000 --> 16:50.000
If once we can do this, then we can experience anything.

16:50.000 --> 16:54.000
We'll be able to find that simply watching the stomach

16:54.000 --> 16:58.000
for some time you'll be able to deal with all sorts of difficulty

16:58.000 --> 17:01.000
that appear in your life much better.

17:01.000 --> 17:04.000
You'll find much greater peace, happiness and freedom from suffering

17:04.000 --> 17:07.000
in your daily life, no matter what arises,

17:07.000 --> 17:09.000
no matter what problems you have in your life.

17:09.000 --> 17:13.000
So this is the true reason why we're practicing.

17:13.000 --> 17:18.000
Now this talk I'm giving, I wanted to give it for a while,

17:18.000 --> 17:21.000
but this is specifically in response to someone

17:21.000 --> 17:27.000
who had a specific experience in after meditation.

17:27.000 --> 17:30.000
And this deals with these,

17:30.000 --> 17:35.000
or it has to do with this idea of certain experiences arising,

17:35.000 --> 17:40.000
because what's going to happen once you begin to see

17:40.000 --> 17:45.000
this impermanence, this suffering nature of things

17:45.000 --> 17:48.000
and the non-self, the inability to control things.

17:48.000 --> 17:51.000
Once you start to see this, you're going to start to let go,

17:51.000 --> 17:54.000
and you're going to start to let go of any concept of things.

17:54.000 --> 17:59.000
So instead of seeing people as a person,

17:59.000 --> 18:04.000
you're going to experience them as a set of experiences,

18:04.000 --> 18:06.000
a set of phenomenon that arise.

18:06.000 --> 18:09.000
So you hear their voice and you'll take that as hearing, as a sound.

18:09.000 --> 18:12.000
When you see them, you'll take that as seeing.

18:12.000 --> 18:14.000
So the idea of a specific person falls apart

18:14.000 --> 18:17.000
and you wind up taking people as they are.

18:17.000 --> 18:20.000
And instead of holding grudges or having expectations

18:20.000 --> 18:23.000
where this person has to behave in this way,

18:23.000 --> 18:25.000
or this person is a bad person, and so on,

18:25.000 --> 18:27.000
you'll take them moment by moment.

18:27.000 --> 18:29.000
And so whatever they are in that moment,

18:29.000 --> 18:31.000
you'll respond to that.

18:31.000 --> 18:37.000
And you'll be able to deal with them in a wise and impartial manner.

18:37.000 --> 18:42.000
You'll find yourself free from all of the baggage

18:42.000 --> 18:45.000
that we carry around with us.

18:45.000 --> 18:52.000
These cycles of vengeance and the feuding and so on,

18:52.000 --> 18:56.000
all of the problems that we have with people will disappear.

18:56.000 --> 18:58.000
All the problems that we have with everything,

18:58.000 --> 19:00.000
when you see something that, before, would scare you,

19:00.000 --> 19:02.000
when you experience something that, before,

19:02.000 --> 19:06.000
would make you afraid or stressful or stressed or worried.

19:06.000 --> 19:08.000
You'll find that you don't take it in that way anymore.

19:08.000 --> 19:09.000
You take it for what it is.

19:09.000 --> 19:11.000
When you're, for instance, in the airplane,

19:11.000 --> 19:14.000
when people are afraid of flying and so on,

19:14.000 --> 19:17.000
it's because they can see it to be much more than it is.

19:17.000 --> 19:19.000
Once you start to see things,

19:19.000 --> 19:23.000
simply for what they are, you give up any attachment to them.

19:23.000 --> 19:27.000
You see that they're impermanent, unsatisfying and uncontrollable.

19:27.000 --> 19:29.000
Because you're not clinging to them and saying,

19:29.000 --> 19:31.000
oh, this is going to make me happy,

19:31.000 --> 19:34.000
or how can I fix this to make it better,

19:34.000 --> 19:37.000
you're going to see it simply for what it is.

19:37.000 --> 19:40.000
And you're going to see the, for instance, the experience of flying

19:40.000 --> 19:43.000
as a series of experiences, a series of phenomenon,

19:43.000 --> 19:46.000
a phenomenon that you experience as seeing or hearing,

19:46.000 --> 19:48.000
or so on, and sitting in a plane

19:48.000 --> 19:50.000
is just going to be exactly what it is.

19:50.000 --> 19:52.000
There's going to be no baggage attached to it,

19:52.000 --> 19:57.000
and people are able to overcome fear of flying as an example.

19:57.000 --> 20:00.000
Arguments that we have when someone's yelling at you

20:00.000 --> 20:02.000
and you say to yourself, hearing, hearing,

20:02.000 --> 20:05.000
and you're simply aware of it as sound.

20:05.000 --> 20:09.000
You don't process the sound as good or bad.

20:09.000 --> 20:13.000
You simply know what's being said, and you're aware of the conversation,

20:13.000 --> 20:15.000
aware of the things that are being said,

20:15.000 --> 20:17.000
and you take it for what it is.

20:17.000 --> 20:21.000
Once you process it, you process it based on the meaning of the words,

20:21.000 --> 20:24.000
and you're able to see it impartially.

20:24.000 --> 20:26.000
Because, again, you don't have any idea

20:26.000 --> 20:28.000
that you're going to be able to fix this and make it better

20:28.000 --> 20:30.000
when bad things occur.

20:30.000 --> 20:32.000
You're just going to take it for it.

20:32.000 --> 20:34.000
You don't have any conception that it's bad.

20:34.000 --> 20:36.000
You simply see it as it is.

20:36.000 --> 20:39.000
But the side effect here, that was brought up,

20:39.000 --> 20:42.000
is that this person said they would look in the mirror

20:42.000 --> 20:45.000
and they didn't recognize themselves.

20:45.000 --> 20:50.000
And I think this is a good indicator

20:50.000 --> 20:53.000
that this person is actually practicing properly.

20:53.000 --> 20:55.000
They have good concentrations.

20:55.000 --> 20:58.000
So what they're seeing in the mirror is just light,

20:58.000 --> 20:59.000
and it takes a little bit of time,

20:59.000 --> 21:02.000
or it may not even occur sometimes.

21:02.000 --> 21:05.000
That this is mean that this is a person.

21:05.000 --> 21:08.000
The recognition disappears, or not exactly the recognition,

21:08.000 --> 21:11.000
but the seeing occurs in a different manner.

21:11.000 --> 21:14.000
Whereas for most of us, when we see something,

21:14.000 --> 21:16.000
there's so much baggage that occurs immediately.

21:16.000 --> 21:17.000
We like it.

21:17.000 --> 21:19.000
When we first recognize it, then we like it,

21:19.000 --> 21:22.000
or we dislike it, our minds move so quickly.

21:22.000 --> 21:24.000
And in a chaotic state most of the time,

21:24.000 --> 21:27.000
so we don't even have time to stop and realize

21:27.000 --> 21:29.000
that what we're seeing is light,

21:29.000 --> 21:31.000
and that it's an experience,

21:31.000 --> 21:34.000
and to catch what's going on.

21:34.000 --> 21:37.000
Once we practice meditation and we develop concentration

21:37.000 --> 21:39.000
and we're able to see things as they are,

21:39.000 --> 21:43.000
we're simply going to experience it for light touching my eye

21:43.000 --> 21:47.000
and a seeing experience that arises.

21:47.000 --> 21:49.000
It's not something out there.

21:49.000 --> 21:52.000
When you look in the mirror, you project and think there's a person.

21:52.000 --> 21:54.000
But actually, it's not.

21:54.000 --> 22:02.000
It's light touching the mirror and coming back and touching your face.

22:02.000 --> 22:05.000
You're eye, coming from the face to the mirror,

22:05.000 --> 22:08.000
then back to your eye, or however.

22:08.000 --> 22:13.000
And so it's important in this case,

22:13.000 --> 22:16.000
I think this explanation is important to help us

22:16.000 --> 22:22.000
to avoid this concern that somehow something's wrong.

22:22.000 --> 22:25.000
People will go home to their after meditation courses,

22:25.000 --> 22:29.000
to their families, and their families will become quite upset

22:29.000 --> 22:32.000
because their children are no longer clinging

22:32.000 --> 22:34.000
or no longer attaching to them,

22:34.000 --> 22:37.000
so as a result, there's not as much joy.

22:37.000 --> 22:41.000
It seems not as much affection and so on.

22:41.000 --> 22:45.000
But this is simply because these people don't realize

22:45.000 --> 22:48.000
that this form of joy is associated with clinging,

22:48.000 --> 22:51.000
and it only leads to expectations,

22:51.000 --> 22:53.000
and when those expectations are not met,

22:53.000 --> 22:56.000
it leads to anger and fighting and frustration,

22:56.000 --> 22:59.000
and eventually leads to sadness and despair

22:59.000 --> 23:02.000
when there's a breakup.

23:02.000 --> 23:05.000
So this is natural.

23:05.000 --> 23:07.000
The person doesn't make a decision.

23:07.000 --> 23:10.000
I'm not going to attach to you anymore because that's bad.

23:10.000 --> 23:13.000
They realize for themselves that there's no reason,

23:13.000 --> 23:16.000
there's no good that comes from attachment from clinging,

23:16.000 --> 23:19.000
and that in fact taking things as they are

23:19.000 --> 23:23.000
is a much more peaceful state of experience,

23:23.000 --> 23:24.000
a state of reality.

23:24.000 --> 23:26.000
And so they change their way of behaving

23:26.000 --> 23:29.000
and their way of approaching their lives

23:29.000 --> 23:31.000
and the world around them.

23:31.000 --> 23:34.000
So just to point out,

23:34.000 --> 23:36.000
just to point out, in this video,

23:36.000 --> 23:39.000
the idea that I like to point out is,

23:39.000 --> 23:43.000
there are these certain experiences that we will come across,

23:43.000 --> 23:45.000
and we should be open to them,

23:45.000 --> 23:48.000
and we should learn to be able to open up

23:48.000 --> 23:50.000
to unpleasant situations.

23:50.000 --> 23:53.000
It doesn't mean that our meditation will always be unpleasant.

23:53.000 --> 23:55.000
People can practice in sight meditation

23:55.000 --> 23:58.000
and have great pleasure and happiness.

23:58.000 --> 24:01.000
You can even have great states of calm and so on.

24:01.000 --> 24:03.000
But as I said in the other video,

24:03.000 --> 24:05.000
and I'll say it again here,

24:05.000 --> 24:08.000
we shouldn't cling to the good ones.

24:08.000 --> 24:10.000
We should also not cling to the bad ones,

24:10.000 --> 24:13.000
and we should not pay any attention or put any weight

24:13.000 --> 24:17.000
on the fact that we might have unpleasant experiences.

24:17.000 --> 24:21.000
And that in fact what we're going to realize is that there's no benefit

24:21.000 --> 24:26.000
or there's no true happiness in any experience

24:26.000 --> 24:29.000
that no experience can bring you happiness.

24:29.000 --> 24:32.000
If you're not already happy, if you're not already at peace with yourself,

24:32.000 --> 24:35.000
there's nothing that you will experience

24:35.000 --> 24:37.000
that is going to bring that to you.

24:37.000 --> 24:40.000
It's only your own freedom of mind,

24:40.000 --> 24:45.000
your own ability to accept ability to live with,

24:45.000 --> 24:52.000
to live with change, to live with discomfort,

24:52.000 --> 24:56.000
to live with the uncontrollability of the universe,

24:56.000 --> 24:59.000
in peace and happiness and freedom from suffering.

24:59.000 --> 25:02.000
Once you come to see these things

25:02.000 --> 25:04.000
and once you come to understand them

25:04.000 --> 25:08.000
and appreciate and accept and realize that this is the nature of reality,

25:08.000 --> 25:12.000
then you will find truth is happiness and freedom from suffering.

25:12.000 --> 25:14.000
When these things come up in your meditation,

25:14.000 --> 25:18.000
the important thing is to not be discouraged and not be frustrated,

25:18.000 --> 25:23.000
to appreciate it and to learn to acknowledge and accept it.

25:23.000 --> 25:25.000
So when you have pain and pain,

25:25.000 --> 25:27.000
when the stomach is uncomfortable,

25:27.000 --> 25:33.000
say to uncomfortable or discomfort or disliking or angry or upset and so on,

25:33.000 --> 25:37.000
when things are not the way you are, when it gives rise to frustration,

25:37.000 --> 25:41.000
to be aware of the frustration and to be aware of this situation.

25:41.000 --> 25:44.000
And if you do this and continue on in this way,

25:44.000 --> 25:47.000
eventually the frustration will disappear.

25:47.000 --> 25:50.000
Your mind will be forced to accept.

25:50.000 --> 25:56.000
Your mind will be forced to give up its misunderstandings.

25:56.000 --> 26:01.000
Once you see reality, you see that it's like this again and again

26:01.000 --> 26:05.000
and again, eventually the mind is forced to give up this delusion,

26:05.000 --> 26:08.000
this idea that somehow it's not like that.

26:08.000 --> 26:10.000
The idea that it can be different

26:10.000 --> 26:14.000
once you try and try again, your mind tries to find happiness,

26:14.000 --> 26:19.000
cling things and realizes that it's only leading again and again to suffering,

26:19.000 --> 26:23.000
then the mind will give up and the mind will stop looking for happiness,

26:23.000 --> 26:29.000
in external objects and things that have no ability to bring true happiness.

26:29.000 --> 26:34.000
So this is a video on basically what insight meditation is,

26:34.000 --> 26:39.000
if you practice in this way, it is the path which leads us to see clearly

26:39.000 --> 26:45.000
and become free from all suffering and stress.

26:45.000 --> 26:50.000
And so I'd like to wish everyone all the best and good luck on your path

26:50.000 --> 26:56.000
and hope that you two are able to find true insight into the nature of reality

26:56.000 --> 27:01.000
and thereby true peace, happiness and freedom from suffering.

27:01.000 --> 27:11.000
Thanks for tuning in and all the best.

