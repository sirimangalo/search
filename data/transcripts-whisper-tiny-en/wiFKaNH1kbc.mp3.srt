1
00:00:00,000 --> 00:00:02,840
So, welcome back to Ask a Monk.

2
00:00:02,840 --> 00:00:14,240
Next question comes from Birka Circus who asks any advice about fearing to look at reality?

3
00:00:14,240 --> 00:00:20,440
Fearing enlightenment, I find myself over and over again in the fog of fear and it whispers

4
00:00:20,440 --> 00:00:27,600
in my ear to the effect that it's not safe out there.

5
00:00:27,600 --> 00:00:36,560
Fearing to look at reality, it's difficult not knowing you and why it is that you're afraid

6
00:00:36,560 --> 00:00:40,640
to look at reality or what exactly you mean by that.

7
00:00:40,640 --> 00:00:51,160
But I think generally speaking it's important to understand that reality is not out there.

8
00:00:51,160 --> 00:01:03,480
He is all of our illusions, all of our illusions are formed off of reality, so we can become

9
00:01:03,480 --> 00:01:10,960
comfortable in illusion and think that giving up these illusions might be somehow scary

10
00:01:10,960 --> 00:01:19,720
that if we, the concept and the idea of not living our life the way we do, not having

11
00:01:19,720 --> 00:01:24,640
these illusions not being who we are and so on.

12
00:01:24,640 --> 00:01:33,440
And this relates I think with the idea of fearing to give up, having to give up things

13
00:01:33,440 --> 00:01:38,800
that you fight you hold dear and this is a big concern that people have, is having to

14
00:01:38,800 --> 00:01:43,400
give up those things that you don't want to give up and I've addressed that.

15
00:01:43,400 --> 00:01:48,280
I think that's part of the fear is this fear that we're going to have to give up things

16
00:01:48,280 --> 00:01:52,280
that we like, things that we want to hold on to.

17
00:01:52,280 --> 00:01:58,040
But the point is that you don't have to and so you don't have to go outside of yourself.

18
00:01:58,040 --> 00:02:02,520
You don't have to leave behind who you are and what you are.

19
00:02:02,520 --> 00:02:03,880
You just have to look at that.

20
00:02:03,880 --> 00:02:12,520
You have to start seeing it clearly and it's not really, I would bet that it's not really

21
00:02:12,520 --> 00:02:20,480
the way you say it, you say that it's scary out there and somehow that you're comfortable

22
00:02:20,480 --> 00:02:32,960
in the illusions or that you're afraid of going outside of who you are, fearing enlightenment

23
00:02:32,960 --> 00:02:34,560
and so on.

24
00:02:34,560 --> 00:02:38,520
But I would say that ask yourself are you suffering?

25
00:02:38,520 --> 00:02:43,520
Do you have pain?

26
00:02:43,520 --> 00:02:49,560
Because obviously your mind is not saying, oh this is good, let's hold on to this.

27
00:02:49,560 --> 00:02:58,000
Your mind is saying, oh this is unpleasant and there is stress and suffering in your

28
00:02:58,000 --> 00:02:59,000
life.

29
00:02:59,000 --> 00:03:03,760
So looking at that, that's really the door to enlightenment, the door to freedom, looking

30
00:03:03,760 --> 00:03:11,400
at why we suffer, looking at why we are not at peace with ourselves, why we have

31
00:03:11,400 --> 00:03:20,160
stress, why we have disappointment, why we want and need things that we don't have.

32
00:03:20,160 --> 00:03:26,640
Rather than focusing on the idea of enlightenment or the idea of some external thing that

33
00:03:26,640 --> 00:03:29,600
we call reality.

34
00:03:29,600 --> 00:03:34,640
It is every moment, it's you typing on the keyboard, it's you looking at the computer screen,

35
00:03:34,640 --> 00:03:38,520
it's you clicking the mouse, it's you sitting in the chair, it's the heat in the room

36
00:03:38,520 --> 00:03:43,960
and the cold in the room, it's the people in the next room shouting and so on.

37
00:03:43,960 --> 00:03:48,320
It's all of these things that are occurring right here and now it's the emotions in your

38
00:03:48,320 --> 00:03:58,360
mind and the tensions and the pains and the aches and the sensations in the body.

39
00:03:58,360 --> 00:04:05,920
It's your experience and so looking at this and seeing it clearly, there's nothing scary

40
00:04:05,920 --> 00:04:09,800
about that, it's just a matter of understanding that that's what we're talking about.

41
00:04:09,800 --> 00:04:17,840
We're not talking about some external reality or external truth or so on.

42
00:04:17,840 --> 00:04:26,480
Truth is here and now and it's within yourself and it's within your illusions and it's

43
00:04:26,480 --> 00:04:33,920
simply seeing your reality for what it is, seeing who you are for what it is.

44
00:04:33,920 --> 00:04:42,440
So there's some advice I think in a general sense on how to overcome this fear of enlightenment.

45
00:04:42,440 --> 00:04:51,600
Don't think about enlightenment, don't think about reality, just start to live your life

46
00:04:51,600 --> 00:04:58,360
clearly aware, live your life with a clear mind, let's put it that way.

47
00:04:58,360 --> 00:05:07,640
You're living your life going back and forth between liking and disliking up and down,

48
00:05:07,640 --> 00:05:14,720
like a yoyo and with a lot of stress and tension, try to live your life in such a way

49
00:05:14,720 --> 00:05:19,240
that it's free from where you see things clearly, where you experience things for what

50
00:05:19,240 --> 00:05:29,680
they are and stop judging and compartmentalizing things into good and bad in me and mine and

51
00:05:29,680 --> 00:05:31,480
so on.

52
00:05:31,480 --> 00:05:56,600
I hope that helps and all the best.

