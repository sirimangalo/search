1
00:00:00,000 --> 00:00:13,560
Okay, so we have five questions tonight.

2
00:00:13,560 --> 00:00:16,320
How can we cultivate hearing an OTABA?

3
00:00:16,320 --> 00:00:20,160
They come as a result from meditation practice or from intellectual thinking.

4
00:00:20,160 --> 00:00:24,240
If I restrain from looking at pornographic materials because of afraid it will hinder my meditation

5
00:00:24,240 --> 00:00:27,200
practice, is that OTABA.

6
00:00:27,200 --> 00:00:30,880
Some of the sort, I mean it comes from OTABA.

7
00:00:30,880 --> 00:00:38,720
Hearing an OTABA are tricky, I mean it's a good question.

8
00:00:38,720 --> 00:00:48,560
But ultimately I think the book answer is that they really come at Sotapana.

9
00:00:48,560 --> 00:00:51,160
Sotapana has hearing an OTABA.

10
00:00:51,160 --> 00:00:58,760
That's not really, that's a pure and the perfect type of hearing OTABA.

11
00:00:58,760 --> 00:01:03,960
Hearing an OTABA are a part of the world, you know our sense of shame that that leads us

12
00:01:03,960 --> 00:01:10,040
not to want to kill or steal or lie or cheat.

13
00:01:10,040 --> 00:01:12,560
I would say that's a stronger form of OTABA.

14
00:01:12,560 --> 00:01:25,840
Now take pornographic material, I think let's not look at pornographic material, let's

15
00:01:25,840 --> 00:01:29,960
say, well it is a good example because often it's just guilt.

16
00:01:29,960 --> 00:01:37,880
It's a sense of self-hatred and feeling bad about what you're doing.

17
00:01:37,880 --> 00:01:47,680
Or it can be real fear, like OTABA isn't fear in the sense of being worried or being

18
00:01:47,680 --> 00:01:51,640
afraid, which is why they're kind of tricky.

19
00:01:51,640 --> 00:02:01,720
OTABA, I don't know, I mean I guess in a worldly sense it's very much related to a sort

20
00:02:01,720 --> 00:02:04,760
of a fear but it's not the fear itself.

21
00:02:04,760 --> 00:02:10,320
It's that sense that this is going to be bad for you and so yeah, if you restrain yourself

22
00:02:10,320 --> 00:02:14,880
from looking at pornographic materials out of a sense that it is going to hinder your

23
00:02:14,880 --> 00:02:24,240
practice, out of a sense that the results will be bad, that's OTABA.

24
00:02:24,240 --> 00:02:35,720
If you do it out of a sense that it's, if you focus on the act itself, that's a problematic

25
00:02:35,720 --> 00:02:46,200
act, that act is involved with a lot of desire and clinging, then that's hearing.

26
00:02:46,200 --> 00:02:52,000
But so why it's a bit complicated is because for a sort upon it's quite different.

27
00:02:52,000 --> 00:03:01,640
For the sort upon that they know that it's bad, they understand why things are bad, why

28
00:03:01,640 --> 00:03:11,920
killing is bad, why stealing is bad, they know intrinsically implicitly why looking at pornography

29
00:03:11,920 --> 00:03:23,080
is problematic, but specifically why the desire relating to looking at pornography is bad.

30
00:03:23,080 --> 00:03:26,680
They know for themselves, I mean they've seen how it's a cause, and they understand

31
00:03:26,680 --> 00:03:32,360
why it's a cause of suffering.

32
00:03:32,360 --> 00:03:40,360
So how can we cultivate them, so there are different ways than if you cultivate them by studying

33
00:03:40,360 --> 00:03:47,040
Buddhism, then that's a way of cultivating a sort of hero DIPA, but it's much stronger when

34
00:03:47,040 --> 00:03:54,800
you practice insight meditation and see the things that are problematic.

35
00:03:54,800 --> 00:03:58,240
Is it useful to look for work?

36
00:03:58,240 --> 00:04:10,320
I mean it's useful to live and survive, so whatever can keep you alive, keep your

37
00:04:10,320 --> 00:04:11,320
meditating.

38
00:04:11,320 --> 00:04:18,080
It's hard for me to answer that.

39
00:04:18,080 --> 00:04:22,560
Dear Adjan you to demo, please don't call me Adjan you to demo unless you're Thai.

40
00:04:22,560 --> 00:04:27,400
I object, if you're not Thai I don't want to go by Adjan and I don't know why all these

41
00:04:27,400 --> 00:04:34,760
monks want to go by Adjan, it's a Thai word, you can call me Bante or Venerable or just

42
00:04:34,760 --> 00:04:41,360
you to demo is fine, I don't mind, but I'm not Adjan because I'm not Thai, Thai people

43
00:04:41,360 --> 00:04:49,760
call me Adjan, that's fine, Adjan is fine because he was Thai, Adjan, I recommend it

44
00:04:49,760 --> 00:04:55,920
is because 80% practice and 20% Thomas study is a healthy way, would you agree or do you

45
00:04:55,920 --> 00:05:10,560
consider the study of the text as important as the cultivation of my 80-20, yeah, I would

46
00:05:10,560 --> 00:05:16,640
say studying the text is not really necessary at all, provided you have a meditation

47
00:05:16,640 --> 00:05:23,760
teacher, right, it's a bit more complicated, when you're doing meditation practice, forget

48
00:05:23,760 --> 00:05:30,280
about study, focus on your practice and rely upon your meditation teacher, ask them the

49
00:05:30,280 --> 00:05:37,680
questions, now if you look at the sutta's, one sutta was enough for one monk or groups

50
00:05:37,680 --> 00:05:43,920
of monks, right, they didn't necessarily get a lot of different teachings, sometimes

51
00:05:43,920 --> 00:05:49,280
it was just enough to teach one thing and you just look at a lot of the sutta's, it's

52
00:05:49,280 --> 00:05:59,360
a lot of repetition, sometimes it's about drilling the same thing in and again and again.

53
00:05:59,360 --> 00:06:03,920
The problem comes of course that we're away from our teacher, so we use study as a substitute

54
00:06:03,920 --> 00:06:11,200
for a teacher, but because it's not directed to our specific situation and it's not practical

55
00:06:11,200 --> 00:06:16,880
in terms of hey do this now, you've got to read a lot more and study a lot more, Mahasya

56
00:06:16,880 --> 00:06:23,040
and I said if you've got no teacher, you're going to have to study the whole tepidica and

57
00:06:23,040 --> 00:06:31,520
the study the whole thing and I think that was maybe a bit of a hyperbole but it's a good

58
00:06:31,520 --> 00:06:38,560
sentiment, the less connection you have with a teacher, the more important it's going to

59
00:06:38,560 --> 00:06:45,000
be the study and have a comprehensive knowledge of the tepidica because who knows what

60
00:06:45,000 --> 00:06:54,120
part of the tepidica is going to be of use to you, so you should study it all, I mean it's not as good

61
00:06:54,120 --> 00:07:00,280
as having a teacher, it's just unfortunately you've got to do a lot of study in order to have all

62
00:07:00,280 --> 00:07:06,760
the answers to your problems, so a good teacher should have a wide knowledge of the dhamma,

63
00:07:06,760 --> 00:07:12,920
shouldn't have studied a lot, either that or be a Buddha, I think there's a problem when you have

64
00:07:12,920 --> 00:07:20,680
even an arahant but more likely a sotepana or something who just teaches without study,

65
00:07:20,680 --> 00:07:27,320
it can be a real problem because they're enlightened, they're understanding is not necessarily

66
00:07:27,320 --> 00:07:34,120
enough for everybody else, only the Buddha had answers to everything and a sotepana can still

67
00:07:34,120 --> 00:07:40,520
get things wrong and arahant can still get things wrong, so using the tepidica is invaluable

68
00:07:40,520 --> 00:07:48,360
for a meditation teacher, but that's that's more how it goes, it's a little more complicated than

69
00:07:48,360 --> 00:08:00,920
just to me anyway, 80, 20, 50, 50 it's not quite like that, what's the best way to improve

70
00:08:00,920 --> 00:08:10,440
concentration and mindfulness in meditation, how about in real life, well read my booklet that

71
00:08:10,440 --> 00:08:21,960
's my opinion, where are you headed in the future, don't answer questions like that, sorry,

72
00:08:23,560 --> 00:08:28,200
is there a meaning of different colored robes, I've noticed you wear red orange and brown,

73
00:08:28,200 --> 00:08:35,480
no there's no meaning to different colored robes, some sara is absurd and disorienting to me,

74
00:08:35,480 --> 00:08:40,360
even just looking around in a quiet room, around a quiet room is disturbing and strange,

75
00:08:42,520 --> 00:08:48,600
questions like is this normal, you see how often I get these questions, that's not really the

76
00:08:48,600 --> 00:08:58,200
question, the important question here, it's interesting that that becomes our concern,

77
00:08:58,200 --> 00:09:01,720
I'm not quite sure why it is that we always ask, I mean it's understandable, it's probably

78
00:09:01,720 --> 00:09:08,680
the how I would ask it, but let's forget about that, is it normal, not a useful question,

79
00:09:09,320 --> 00:09:15,880
so being disturbing, I guess the question we want to ask by that, am I doing okay, am I on the

80
00:09:15,880 --> 00:09:23,800
path or is something wrong, right, and it's much more interesting to look at our asking of that

81
00:09:23,800 --> 00:09:31,560
question, the fact that you're disturbed by it, the fact that you're worried about it,

82
00:09:32,120 --> 00:09:36,120
that in and of itself is interesting and that's something that you should be mindful of,

83
00:09:36,840 --> 00:09:41,240
because that's what we're interested in, how we react to things, so first of all this

84
00:09:41,240 --> 00:09:46,360
situation of being disturbed and so on, you're worried about that, it makes you doubt the practice,

85
00:09:46,360 --> 00:09:53,160
it makes you doubt your practice, that's interesting because that's a reaction to an experience,

86
00:09:53,160 --> 00:09:59,800
and that's an improper reaction, it's a stressful, it's a cause of stress and suffering,

87
00:10:00,760 --> 00:10:05,880
so what we're trying to do is learn to see experiences as they are, and so part of this question,

88
00:10:05,880 --> 00:10:11,800
what is something normal, why we ask that is because we're intent on judging, we're at good,

89
00:10:11,800 --> 00:10:16,360
bad, right, wrong, and we're trying to get to a sense that experience is experienced and to

90
00:10:16,360 --> 00:10:24,360
realize that it doesn't really matter what I experience, it's all arising and ceasing, and so the

91
00:10:24,360 --> 00:10:29,960
practice that I would recommend in this situation is to be mindful of that, of course,

92
00:10:29,960 --> 00:10:35,080
to worry the doubt and all that, but once you get over that, be mindful of the disorientation,

93
00:10:35,080 --> 00:10:41,400
be mindful of the feeling of absurdity, some sorrow is not absurd, you are judging it as absurd,

94
00:10:41,400 --> 00:10:46,680
and that's a judgment, so look at that judgment, do you like it, do you dislike it, do you feel

95
00:10:46,680 --> 00:10:53,560
an egotistical because you feel proud of yourself because you are not caught up in it,

96
00:10:53,560 --> 00:10:57,560
do you feel better than other people who are caught up in it, that kind of thing, and there's

97
00:10:57,560 --> 00:11:05,640
a lot of different states potentially there that you should be mindful of, can noting be done during

98
00:11:05,640 --> 00:11:16,440
the day, of course, yes it can, what does Buddhism stay about Kundalina, Kundalini, I get this

99
00:11:16,440 --> 00:11:21,720
question a lot, Buddhism doesn't say anything about Kundalini, it's not a Buddhist practice,

100
00:11:24,120 --> 00:11:28,280
a lot of people interested in Kundalini is that I have something to do with the yoga

101
00:11:28,280 --> 00:11:35,000
craze, everyone's into yoga now, we're not laughing at yoga, it's fine, it's just, it's a different,

102
00:11:37,000 --> 00:11:41,320
I can't talk about such things because it's a whole other language, the language they use is

103
00:11:41,320 --> 00:11:47,720
totally different, their view, their outlook, it's not the same outlook or the same view,

104
00:11:48,440 --> 00:11:55,720
it's steep in Hinduism, which, you know, comes from Brahmanism and their banishads and there's a

105
00:11:55,720 --> 00:12:02,120
whole different way of looking at the world there, so for me to even comment on something like

106
00:12:02,120 --> 00:12:06,600
Kundalini, it's just a different paradigm, you know,

107
00:12:12,600 --> 00:12:18,440
so I guess what I could say is that we're talking about some feeling that arises and some concepts

108
00:12:18,440 --> 00:12:23,160
surrounding that feeling, so the feeling is a feeling, the concepts are thoughts and those are

109
00:12:23,160 --> 00:12:28,200
experiences and you should be mindful and let them go, see that they're impermanent suffering and

110
00:12:28,200 --> 00:12:35,880
non-self, don't cling to them, it's all the questions for tonight, thank you all for coming and

111
00:12:35,880 --> 00:12:57,400
coming out.

