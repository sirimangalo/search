1
00:00:00,000 --> 00:00:03,720
Do I have any free will?

2
00:00:03,720 --> 00:00:09,280
I have had the experience after meditating a lot that I am only observing myself an action

3
00:00:09,280 --> 00:00:19,120
but have no control, only over my emotions through meditation, thank you.

4
00:00:19,120 --> 00:00:32,120
I think we do have free will, but only in the sense of being free to incline the

5
00:00:32,120 --> 00:00:40,200
mind in a certain direction in the present moment.

6
00:00:40,200 --> 00:00:54,280
And the word free will and having free will is maybe just an inexact or an inapplicable

7
00:00:54,280 --> 00:01:01,560
description of what is really going on, but in some sense, if we had a perfect language

8
00:01:01,560 --> 00:01:06,720
that was actually based on reality, then in some sense there is free will in the sense

9
00:01:06,720 --> 00:01:12,880
of being able to do to make a decision in the present moment.

10
00:01:12,880 --> 00:01:20,840
The nonself comes with the fact that you can't go beyond that, beyond inclining the mind.

11
00:01:20,840 --> 00:01:25,360
You can't force your, even if you want to just lift your hand, you can't force the hand

12
00:01:25,360 --> 00:01:26,360
to lift.

13
00:01:26,360 --> 00:01:34,160
All you can do is incline your mind towards the manifestation of the lifting of the hand

14
00:01:34,160 --> 00:01:42,840
and if your intention is strong enough and if the situation that you're in is favorable,

15
00:01:42,840 --> 00:01:46,920
the hand will raise.

16
00:01:46,920 --> 00:01:54,840
But what you realize, what you see in meditation is that that's not always the case, that

17
00:01:54,840 --> 00:02:00,320
sometimes the intention is there and the result doesn't come about.

18
00:02:00,320 --> 00:02:13,920
And you realize that this is where suffering is caught by not getting what you want.

19
00:02:13,920 --> 00:02:20,800
Another thing you realize is that the more you intend, the more you decide to do this or

20
00:02:20,800 --> 00:02:28,480
decide to do that, the more stress and potentially suffering that you cause.

21
00:02:28,480 --> 00:02:34,960
The more expectations you have and desires you have and stronger your intentions, the more

22
00:02:34,960 --> 00:02:43,960
you build up and the more you set yourself up for disappointment when you overstep, because

23
00:02:43,960 --> 00:03:02,880
you overstep based on your desires, you overstep what's possible.

