1
00:00:00,000 --> 00:00:02,000
Go ahead.

2
00:00:06,000 --> 00:00:08,000
Sorry, you were saying?

3
00:00:08,000 --> 00:00:12,000
You said the other one of your videos that we

4
00:00:12,000 --> 00:00:18,000
should practice 30-partana and vipasana

5
00:00:18,000 --> 00:00:28,000
but which technique should be used

6
00:00:28,000 --> 00:00:32,000
I mean, can we do some 30-partana at the beginning of meditation

7
00:00:32,000 --> 00:00:36,000
and then vipasana or we should separate it?

8
00:00:36,000 --> 00:00:38,000
Just to be clearly,

9
00:00:38,000 --> 00:00:40,000
Satipatana is the practice.

10
00:00:40,000 --> 00:00:43,000
Vipasana is the result.

11
00:00:43,000 --> 00:00:47,000
So you can't practice vipasana without Satipatana.

12
00:00:47,000 --> 00:00:50,000
You can't obtain vipasana without Satipatana.

13
00:00:50,000 --> 00:00:52,000
That's the point.

14
00:00:52,000 --> 00:00:56,000
So you can't practice vipasana.

15
00:00:56,000 --> 00:01:00,000
I don't want to be categorical about it because

16
00:01:00,000 --> 00:01:04,000
there are certain teachings that appear to be saying that

17
00:01:04,000 --> 00:01:08,000
you do contemplate impermanence suffering and non-self

18
00:01:08,000 --> 00:01:12,000
but I would say it's just a

19
00:01:14,000 --> 00:01:16,000
simplifying what's really going on

20
00:01:16,000 --> 00:01:19,000
and that is paying attention to the five aggregates.

21
00:01:19,000 --> 00:01:23,000
The Satipatana practice is paying attention to the five aggregates form.

22
00:01:23,000 --> 00:01:28,000
We have this physical sensation, physical experiences.

23
00:01:28,000 --> 00:01:33,000
We don't know which is our feelings, pain and happiness

24
00:01:33,000 --> 00:01:35,000
and calm and so on.

25
00:01:35,000 --> 00:01:39,000
Sunya, our memories and perceptions.

26
00:01:39,000 --> 00:01:42,000
Sunkara are thoughts.

27
00:01:42,000 --> 00:01:49,000
We know our judgments, our projections and formations in vigna.

28
00:01:49,000 --> 00:01:54,000
Which boil down to the same thing as the four Satipatanas.

29
00:01:54,000 --> 00:01:58,000
This is the truth of suffering.

30
00:01:58,000 --> 00:02:01,000
The four Satipatanas are the five aggregates

31
00:02:01,000 --> 00:02:06,000
and that is the object of meditation to realize vipasana.

32
00:02:06,000 --> 00:02:09,000
Once you look at these things, you'll see the first noble truth.

33
00:02:09,000 --> 00:02:14,000
The first noble truth is another way of saying the three characteristics.

34
00:02:14,000 --> 00:02:17,000
And sorry, I haven't made it clear that

35
00:02:17,000 --> 00:02:20,000
what I'm saying is that what we mean by vipasana

36
00:02:20,000 --> 00:02:23,000
is the realization of the three characteristics.

37
00:02:23,000 --> 00:02:28,000
The word vipasana is a synonym

38
00:02:28,000 --> 00:02:31,000
for realization of the three characteristics.

39
00:02:31,000 --> 00:02:34,000
That's what a definition of vipasana is.

40
00:02:34,000 --> 00:02:37,000
So that doesn't come about

41
00:02:37,000 --> 00:02:41,000
unless you look at things that are impermanence suffering and unsung.

42
00:02:41,000 --> 00:02:46,000
And in a way that allows you to see them as impermanence suffering and unsung.

43
00:02:46,000 --> 00:02:51,000
The considering in your mind that this is impermanent,

44
00:02:51,000 --> 00:02:55,000
that that is suffering, this is non-self, is just intellectual.

45
00:02:55,000 --> 00:02:59,000
Mahasim Sayada was explaining really the easiest way

46
00:02:59,000 --> 00:03:01,000
to explain the problem with that

47
00:03:01,000 --> 00:03:03,000
is that you don't have enough concentration.

48
00:03:03,000 --> 00:03:06,000
You're not really focused on the object.

49
00:03:06,000 --> 00:03:11,000
You need very strong concentration to see things as they are.

50
00:03:11,000 --> 00:03:14,000
It has to suppress the hindrances

51
00:03:14,000 --> 00:03:19,000
and allow your mind to be clear when you observe something.

52
00:03:19,000 --> 00:03:21,000
So simply thinking about something

53
00:03:21,000 --> 00:03:24,000
is not going to give you that kind of concentration.

54
00:03:24,000 --> 00:03:29,000
It's in fact a distracted state of mind.

55
00:03:29,000 --> 00:03:40,000
So you have to focus on the objects of experience.

56
00:03:40,000 --> 00:03:45,000
For a moment to moment in order to get that kind of concentration

57
00:03:45,000 --> 00:03:47,000
in order to really see things as they are.

58
00:03:47,000 --> 00:03:49,000
That's how we pass in the rice.

59
00:03:49,000 --> 00:03:51,000
So you don't practice both Saty bitana and vipasana.

60
00:03:51,000 --> 00:03:53,000
You practice Saty bitana

61
00:03:53,000 --> 00:03:56,000
and it lets you see things clearly which is vipasana.

62
00:03:56,000 --> 00:04:01,000
Does that make sense?

63
00:04:01,000 --> 00:04:02,000
Yes.

64
00:04:02,000 --> 00:04:03,000
Thank you.

65
00:04:03,000 --> 00:04:10,000
Thank you.

