1
00:00:00,000 --> 00:00:08,960
Mindfulness and noting. Question. What does it mean to note something? What is the

2
00:00:08,960 --> 00:00:13,640
thing that you are noting? What is the difference between noting and being

3
00:00:13,640 --> 00:00:20,040
mindful if there is any or the relationship between them? Answer. The word

4
00:00:20,040 --> 00:00:25,560
mindfulness is an imperfect translation of the word sati. Sati is the state of

5
00:00:25,560 --> 00:00:29,960
mind that rightly grasps the object in such a way that it can be seen clearly

6
00:00:29,960 --> 00:00:35,400
sati can also be used in a conventional sense as the ability to remember things

7
00:00:35,400 --> 00:00:41,640
that happened a long time ago. In meditation it means the ability to remember the

8
00:00:41,640 --> 00:00:48,680
present moment. It means not being distracted by or dwelling in abstract thought.

9
00:00:48,680 --> 00:00:55,640
Abstract thought is not evil or bad but it is not mindful. In our everyday

10
00:00:55,640 --> 00:01:01,120
activities we have to rely on abstract concepts. Many of our ordinary activities

11
00:01:01,120 --> 00:01:05,680
will be difficult to perform mindfully because being mindful means only being

12
00:01:05,680 --> 00:01:10,160
aware of the moments of experience. It is hard to maintain a stream of

13
00:01:10,160 --> 00:01:15,800
conceptual thought when you are mindful. Likewise, when we dwell in abstract

14
00:01:15,800 --> 00:01:20,920
thought we are generally unmindful of reality. For example, you are probably

15
00:01:20,920 --> 00:01:25,080
sitting while reading this but you likely have no awareness of that fact while

16
00:01:25,080 --> 00:01:30,280
reading. When you are caught up in abstract thought you quickly forget that you

17
00:01:30,280 --> 00:01:35,240
are sitting but when you read did you know that you are sitting? Suddenly you are

18
00:01:35,240 --> 00:01:39,600
reminded that you are sitting. I think this is why the Buddha chose the word

19
00:01:39,600 --> 00:01:45,120
sati to describe meditation not just as a state but as an activity, remembering.

20
00:01:45,120 --> 00:01:50,800
Sati is what leads to seeing clearly and the act of cultivating sati is this

21
00:01:50,800 --> 00:01:57,760
activity of reminding yourself of your experiential reality. Abstract thought is

22
00:01:57,760 --> 00:02:03,680
useful for cultivating right view. Abstraction is the basis of language so for

23
00:02:03,680 --> 00:02:07,600
someone else to remind us of something they need to use words that describe

24
00:02:07,600 --> 00:02:14,200
abstract concepts. Words themselves are also concepts. The word sati does not mean

25
00:02:14,200 --> 00:02:19,520
anything. It is just a sound. Its meaning is only conceptual based on how our

26
00:02:19,520 --> 00:02:24,920
minds interpret the sound. We know what sati refers to and based on our

27
00:02:24,920 --> 00:02:29,440
faculties of memory and recognition we associate that word with a certain

28
00:02:29,440 --> 00:02:36,720
mind state. The mind state described by sati is real. Mindfulness is real but the

29
00:02:36,720 --> 00:02:42,280
word mindfulness is just a concept. When I say mindfulness it evokes a

30
00:02:42,280 --> 00:02:48,200
perception of certain real mind states that are useful and not abstract.

31
00:02:48,200 --> 00:02:53,400
You may find this teaching useful. Teaching is a useful abstraction that can

32
00:02:53,400 --> 00:02:57,400
change the way you look at things and allow you to see reality in a different way.

33
00:02:57,400 --> 00:03:02,000
But it will never be a replacement for the experience of reality for yourself.

34
00:03:02,000 --> 00:03:06,840
It will never be enough. You could listen to all of my talks and it would

35
00:03:06,840 --> 00:03:11,560
never be enough unless you transform the teachings into actual practice that

36
00:03:11,560 --> 00:03:17,040
allows you to cultivate the realities described by the teachings. This is where

37
00:03:17,040 --> 00:03:22,280
noting comes in. Noting is a sort of gateway between the teaching and the mind

38
00:03:22,280 --> 00:03:27,120
states. Noting simply means saying a word to yourself that has a connection to

39
00:03:27,120 --> 00:03:32,440
the reality you are experiencing at that moment. When you engage in this

40
00:03:32,440 --> 00:03:37,000
practice of saying something to yourself this is what we call mantra meditation.

41
00:03:37,000 --> 00:03:42,840
Mantra meditation is very old. It was around before the Buddha himself and his

42
00:03:42,840 --> 00:03:48,600
widespread and broadly recognized today by many religious, spiritual, and even

43
00:03:48,600 --> 00:03:52,520
secular practices as an effective tool because it is the simplest means of

44
00:03:52,520 --> 00:03:58,600
focusing the mind in a specific way on a specific object. It is similar to how we

45
00:03:58,600 --> 00:04:06,000
use words to express emotion. If I yell at someone and say you hurt me it focuses

46
00:04:06,000 --> 00:04:11,240
my mind on the anger and self righteous indignation I feel in that moment, augmenting

47
00:04:11,240 --> 00:04:16,880
those mind states. Words are quite powerful. They have the effect of augmenting

48
00:04:16,880 --> 00:04:23,520
our mental states and emotions often to our detriment. We speak words in

49
00:04:23,520 --> 00:04:28,760
response to many of our experiences in ordinary life. When you like something

50
00:04:28,760 --> 00:04:33,400
you may say to yourself this is great which reinforces your attachment to that

51
00:04:33,400 --> 00:04:38,440
thing. Mantras make use of this process by which words evoke specific states of

52
00:04:38,440 --> 00:04:43,800
mind. Self help gurus often suggest mantras that help their followers create

53
00:04:43,800 --> 00:04:49,160
specific states of mind, stimulating the mind in a particular way. It is not

54
00:04:49,160 --> 00:04:52,560
common for someone to intentionally practice with a mantra that stimulates

55
00:04:52,560 --> 00:04:58,320
unwholesome qualities like greed or anger. But we often use words that evoke

56
00:04:58,320 --> 00:05:03,960
unwholesome mind states unknowingly. This is why meditation can be dangerous.

57
00:05:03,960 --> 00:05:08,880
Quite conceivably you could create a meditation practice that would make you more

58
00:05:08,880 --> 00:05:15,200
angry, more greedy, or more deluded. Mantras that stimulate delusion may be the

59
00:05:15,200 --> 00:05:18,920
most common as religious teachings that have wrong views often involve

60
00:05:18,920 --> 00:05:25,360
teachings, sayings, mottos and even mantras that support delusion. A good

61
00:05:25,360 --> 00:05:30,120
meditation may be used to evoke positive emotions. One example that should come

62
00:05:30,120 --> 00:05:35,080
to mind for many people is meta or friendliness meditation. Where we use the

63
00:05:35,080 --> 00:05:41,240
mantra may I be happy, may all beings be happy. The words themselves are not

64
00:05:41,240 --> 00:05:45,000
wholesome but they have the potential to evoke wholesome emotive states like

65
00:05:45,000 --> 00:05:50,200
friendliness, compassion and kindness. Other wholesome meditations are

66
00:05:50,200 --> 00:05:54,560
designed to cultivate not emotions but certain other qualities of mind like

67
00:05:54,560 --> 00:06:01,280
focus or clarity. Cassina meditation is a good example. Cassina means totality.

68
00:06:01,280 --> 00:06:06,320
A Cassina is an object that you use to create a sense of totality where your whole

69
00:06:06,320 --> 00:06:11,680
world is just one thing like a color for example. You may start by focusing on a

70
00:06:11,680 --> 00:06:18,080
circle of white and you would just repeat to yourself, white, white, white. The

71
00:06:18,080 --> 00:06:22,600
mantra helps evoke a single pointed attention on the color. White is not

72
00:06:22,600 --> 00:06:27,720
anything special but because it is a simple concept and very singular, focusing

73
00:06:27,720 --> 00:06:33,280
on it allows for the cultivation of focused attention. Repeating white is not

74
00:06:33,280 --> 00:06:37,520
likely to make you angry, greedy or deluded. It is not going to evoke bad

75
00:06:37,520 --> 00:06:42,440
things. Such practice has the potential to help you create very powerful and

76
00:06:42,440 --> 00:06:47,720
pure states of mind. Eventually if you practice such meditation all you will

77
00:06:47,720 --> 00:06:53,360
see even when you close your eyes is white. The benefit is this very strong

78
00:06:53,360 --> 00:06:59,560
concentrated awareness. With mindfulness meditation the purpose of the mantra is

79
00:06:59,560 --> 00:07:04,400
to help our minds focus on reality rather than on a concept. We use

80
00:07:04,400 --> 00:07:10,680
abstraction, noting, to bring the mind back to reality. It is a unique form of

81
00:07:10,680 --> 00:07:16,320
meditation. It may share technical similarities to other types of meditation but

82
00:07:16,320 --> 00:07:19,960
it is unique in application because rather than focusing your attention on

83
00:07:19,960 --> 00:07:25,440
another abstraction, the object of the mantra is real or experiential. The

84
00:07:25,440 --> 00:07:29,560
word real can mean different things but experiential means it is a part of

85
00:07:29,560 --> 00:07:35,600
experience which is the essence of ultimate reality and Buddhism. When you say

86
00:07:35,600 --> 00:07:41,280
to yourself, seeing, seeing, it helps focus your mind on the experience so

87
00:07:41,280 --> 00:07:45,120
you remember simply that you are seeing at that moment. There is this

88
00:07:45,120 --> 00:07:49,480
awareness and presence that is objective unlike our ordinary experience of

89
00:07:49,480 --> 00:07:54,920
things which is steeped in judgment and subjectivity. Ordinarily we

90
00:07:54,920 --> 00:07:58,800
recognize that an object has brought us pleasure in the past so we like and

91
00:07:58,800 --> 00:08:03,440
conceive of it as positive or we associated with a past suffering and

92
00:08:03,440 --> 00:08:08,480
dislike it or we associated with some view, conceit or arrogance and

93
00:08:08,480 --> 00:08:14,160
delusion arises. We might look in the mirror and see ourselves and think, I am

94
00:08:14,160 --> 00:08:22,080
so handsome or I am so ugly and conceit arises or some view of self like, this is

95
00:08:22,080 --> 00:08:29,040
me, this is mine, this is what I am. This practice of reminding yourself of the

96
00:08:29,040 --> 00:08:32,640
simple nature of each experience has the potential to remove all of those

97
00:08:32,640 --> 00:08:39,760
habits of greed, anger and delusion. It evokes objective states in our minds.

98
00:08:39,760 --> 00:08:45,480
Without mindfulness, we respond to experiences with thoughts of, this is bad, this is

99
00:08:45,480 --> 00:08:52,040
good, this is me, this is mine. But with the mindfulness practice you say, this is

100
00:08:52,040 --> 00:08:59,680
this, i.e., seeing, seeing, hearing, is hearing, thinking, is thinking. This is

101
00:08:59,680 --> 00:09:04,920
explicitly what the Buddha taught. What you can read about him teaching time and

102
00:09:04,920 --> 00:09:10,240
again, we have shells of the Buddha's teaching but this very basic teaching is so

103
00:09:10,240 --> 00:09:16,680
simple and so ordinary that it often gets overlooked. What is Buddhism? Buddhism is

104
00:09:16,680 --> 00:09:24,640
about seeing, hearing, smelling, tasting, feeling, thinking. We are made up of the

105
00:09:24,640 --> 00:09:28,800
five aggregates and they arise at the moment of experience of any of the six

106
00:09:28,800 --> 00:09:33,960
senses. Mindfulness is simply the practice of purifying our perception of these

107
00:09:33,960 --> 00:09:38,520
moments of experience, so our reactions are in line with reality rather than

108
00:09:38,520 --> 00:09:45,440
partiality or delusion. It is important not to confuse the act of noting with

109
00:09:45,440 --> 00:09:50,480
the states of mind that it evokes. Mindfulness is a tool. It is an artificial tool

110
00:09:50,480 --> 00:09:54,960
used to bring the mind closer to reality and become more objective about the

111
00:09:54,960 --> 00:10:01,800
reality or the experience. Mindfulness or noting is not the objectivity or

112
00:10:01,800 --> 00:10:07,000
awareness. Noting is the tool that you must use to evoke certain mind states. If

113
00:10:07,000 --> 00:10:11,440
you are anxious and you know that anxiety is composed of different physical and

114
00:10:11,440 --> 00:10:18,680
mental states, you can still just say anxious, anxious. The idea is to help you

115
00:10:18,680 --> 00:10:23,640
experience reality with a clear mind. The mantra is not some kind of pill that

116
00:10:23,640 --> 00:10:29,080
you swallow or a magical wand that you wave to change reality. You do not have

117
00:10:29,080 --> 00:10:33,520
to apply the technique to every little thing you experience. With anxiety, for

118
00:10:33,520 --> 00:10:38,600
example, the whole experience is anxiety, but some aspects of it are physical and

119
00:10:38,600 --> 00:10:43,360
some are mental. The butterflies that you feel in your stomach, the heart

120
00:10:43,360 --> 00:10:47,040
beating quickly, and the tension in your shoulders can all be noted

121
00:10:47,040 --> 00:10:50,760
individually, which will help you experience them without reaction or

122
00:10:50,760 --> 00:10:56,040
judgment. By simply saying to yourself when you are tense in the shoulders, tense,

123
00:10:56,040 --> 00:11:00,640
or when you feel there are butterflies in your stomach because you are

124
00:11:00,640 --> 00:11:07,080
anxious, saying, feeling, feeling. The experience does not necessarily go away,

125
00:11:07,080 --> 00:11:12,300
but it also does not lead to more anxiety. You will see that the practice of

126
00:11:12,300 --> 00:11:15,840
noting evokes the states of neutrality and objectivity that are not

127
00:11:15,840 --> 00:11:22,440
reactive. Some states, like anxiety, grow with anxiety, you start with a thought.

128
00:11:22,440 --> 00:11:28,360
For example, I might think there are fifty people now listening to me talk here,

129
00:11:28,360 --> 00:11:33,360
and that thought may evoke certain states of anxiety in the mind, which in turn

130
00:11:33,360 --> 00:11:38,840
create feelings in the body. Those feelings in the body may then make me more

131
00:11:38,840 --> 00:11:45,160
anxious. Then I may think, oh, I am anxious, and the feelings, and the mind

132
00:11:45,160 --> 00:11:50,280
states bounce back and forth like this. The physical states lead to more anxiety,

133
00:11:50,280 --> 00:11:55,560
more anxiety leads to stronger physical reactions, creating a feedback loop,

134
00:11:55,560 --> 00:12:00,960
wherein anxiety becomes stronger and stronger until it can even lead to a full

135
00:12:00,960 --> 00:12:06,480
blown panic attack. In each moment of anxiety, there is a process of

136
00:12:06,480 --> 00:12:11,680
purring. First, there is an experience. Then there is conceiving about that

137
00:12:11,680 --> 00:12:15,480
experience, and finally, there is the reaction based on one's

138
00:12:15,480 --> 00:12:20,880
conceptions of the experience. This process can be broken by simply changing

139
00:12:20,880 --> 00:12:25,520
one's conception of the experience. Rather than conceiving of the experience as

140
00:12:25,520 --> 00:12:31,120
I am anxious, this is a problem. You change the conception to just this is

141
00:12:31,120 --> 00:12:37,160
anxiety, or the physical, this is feeling, by using mantras like anxious or

142
00:12:37,160 --> 00:12:42,360
feeling to remind yourself of the objective nature of the experience. An act

143
00:12:42,360 --> 00:12:47,160
which evokes very different states of mind from ordinary conception. The

144
00:12:47,160 --> 00:12:52,160
mantra is not magic. It is not in and of itself mindfulness, but if used

145
00:12:52,160 --> 00:12:56,880
consistently and with proper attention, it can evoke states of objectivity

146
00:12:56,880 --> 00:13:00,720
and mindfulness that break away from the conceptual process that leads to

147
00:13:00,720 --> 00:13:06,760
building up of things like anxiety. Conceptualization can be useful, and the

148
00:13:06,760 --> 00:13:10,720
mantra is a useful conceptualization that helps us direct our attention to the

149
00:13:10,720 --> 00:13:15,920
object of our experience. In other types of meditation, a mantra is used to

150
00:13:15,920 --> 00:13:21,280
direct one's attention to concepts like white. In mindfulness, the object is

151
00:13:21,280 --> 00:13:26,720
not concepts, but experiential reality. This is an important difference because

152
00:13:26,720 --> 00:13:30,960
reality has certain qualities that are not present in concepts. Firstly,

153
00:13:30,960 --> 00:13:36,080
impermanence. Whereas we perceive conceptual objects to be stable in our

154
00:13:36,080 --> 00:13:42,080
imagination, experiences arise and cease from moment to moment. Because

155
00:13:42,080 --> 00:13:46,760
concepts rely on experiences to arise, our lack of familiarity with the

156
00:13:46,760 --> 00:13:50,800
impermanence of reality leads to disappointment when the conceptual things we

157
00:13:50,800 --> 00:13:56,720
come to expect cannot be produced on a whim. Through observing experiences

158
00:13:56,720 --> 00:14:01,600
directly, we become familiar with the chaotic nature of reality, and the whole

159
00:14:01,600 --> 00:14:06,440
idea of expectation becomes absurd. We cannot know what the future will

160
00:14:06,440 --> 00:14:11,760
bring, and forming expectations in our minds becomes a habit that can only lead

161
00:14:11,760 --> 00:14:17,160
to eventual disappointment. There is no benefit to expecting. What does it

162
00:14:17,160 --> 00:14:22,640
mean to say, I want things to be this way? All it means is you are cultivating a

163
00:14:22,640 --> 00:14:28,560
habit of desire. It does not have any bearing on how things are going to be.

164
00:14:28,560 --> 00:14:34,920
Secondly, mindfulness of experiences provides understanding of suffering. When you

165
00:14:34,920 --> 00:14:39,720
try to fix or control your conceptual reality, you become stressed and are

166
00:14:39,720 --> 00:14:44,640
susceptible to disappointment. Even if you are able occasionally to get whatever you

167
00:14:44,640 --> 00:14:50,360
want, you then just want it more. By keeping the mind present and engaged with

168
00:14:50,360 --> 00:14:54,280
your actual experiences, you see this stress and attachment for what it is,

169
00:14:54,280 --> 00:14:59,400
realizing that reality admits of no satisfaction for those who cling to

170
00:14:59,400 --> 00:15:04,560
concepts or experiences as they are unpredictable and uncontrollable.

171
00:15:04,560 --> 00:15:11,840
Thirdly, mindfulness shows you non-self that the conceptual entities we evoke in

172
00:15:11,840 --> 00:15:16,240
our minds are merely illusions formed of perception, recognition, and memory.

173
00:15:16,240 --> 00:15:20,840
Through mindful observation of experiences, you see that momentary

174
00:15:20,840 --> 00:15:25,760
experiences underlie all things, they have no substance beyond our imagination

175
00:15:25,760 --> 00:15:31,360
and the experiences that trigger it. As a result of this clarity of vision, we

176
00:15:31,360 --> 00:15:36,320
no longer obsess about controlling the world around us or cling to things as me

177
00:15:36,320 --> 00:15:42,720
or mine. We see that this clinging to illusions leads only to suffering. Our lack

178
00:15:42,720 --> 00:15:46,400
of familiarity with these fundamental aspects of nature leads us to

179
00:15:46,400 --> 00:15:51,440
conceive of experiences as entities and those entities as bringing us pleasure or

180
00:15:51,440 --> 00:15:57,160
pain, which causes us to react positively or negatively towards them.

181
00:15:57,160 --> 00:16:02,720
Through mindfulness, we start to see experiences just as experiences, noting is simply

182
00:16:02,720 --> 00:16:09,200
a tool to help evoke a state of mindfulness. When practicing noting meditation, it is

183
00:16:09,200 --> 00:16:13,440
important not to worry too much about the details, like whether you are noting

184
00:16:13,440 --> 00:16:19,120
everything or noting the correct thing or noting something correctly. You do not

185
00:16:19,120 --> 00:16:23,400
have to note everything. You do not have to worry about being perfectly correct.

186
00:16:23,400 --> 00:16:29,280
We note sitting, sitting, for example, even though it is not the concept of

187
00:16:29,280 --> 00:16:33,840
sitting we are focused on. The noting of sitting focuses your attention on the

188
00:16:33,840 --> 00:16:38,440
experience of sitting, the pressure on your bottom, the tension in your back and in

189
00:16:38,440 --> 00:16:44,240
your legs and so on, which is absolutely real. Thing to yourself, sitting, sitting,

190
00:16:44,240 --> 00:16:49,480
the vokes this object of awareness of the experience, saying, sitting, is not

191
00:16:49,480 --> 00:16:55,640
saying this is good or this is bad. It is an objective thing to say so, even though

192
00:16:55,640 --> 00:16:59,960
it is conceptual and the sitting itself is conceptual. The mantra helps you

193
00:16:59,960 --> 00:17:04,480
focus on reality. Anyone who is critical of this should read what the Buddha

194
00:17:04,480 --> 00:17:10,720
himself said, when sitting, no clearly to yourself, I am sitting. Most

195
00:17:10,720 --> 00:17:15,440
important is that mantra meditation or noting meditation or whatever you

196
00:17:15,440 --> 00:17:20,560
call it, is a concrete, easily accessible practice that almost anyone can

197
00:17:20,560 --> 00:17:25,920
undertake. Right here and now you can use it to become objectively aware of

198
00:17:25,920 --> 00:17:31,440
your own experiences as they truly are. Noting is a very useful tool, but do not

199
00:17:31,440 --> 00:17:36,040
let the tool become the object. Do not focus on the noting. Thinking, is it

200
00:17:36,040 --> 00:17:41,880
working? Why is it not working? Why do I say pain, pain and the pain does not go

201
00:17:41,880 --> 00:17:47,760
away? I expect results. Why am I not getting them? You are not getting good

202
00:17:47,760 --> 00:17:51,960
results because of expectation. The mantra is just a tool. The

203
00:17:51,960 --> 00:18:03,000
consequent mindfulness is what does the work to free us from suffering.

