1
00:00:00,000 --> 00:00:18,480
I have to fix this so can you say something yeah but they won't hear you because I have to do

2
00:00:30,000 --> 00:00:59,840
this test test test can I use talk?

3
00:00:59,840 --> 00:01:12,120
Yes can you okay? Now it should get both of us I think so we have to broadcast this is

4
00:01:12,120 --> 00:01:28,400
all right okay hello everyone broadcasting live December 23rd

5
00:01:28,400 --> 00:01:35,520
2015 no dumb of part of this week I think it's a bit too much to get set up here

6
00:01:35,520 --> 00:01:42,480
I did bring the camera but you know it's Christmas and my brother's coming

7
00:01:42,480 --> 00:01:50,400
I think I'll wait till I get back to the back to the back cave

8
00:01:50,400 --> 00:02:07,840
so I'll just do a regularly scheduled broadcast so I'm in Florida it's night

9
00:02:07,840 --> 00:02:24,480
here I spent the better part of a year in Thailand once living inside a mosquito

10
00:02:24,480 --> 00:02:35,720
screen like this it's made of teak wood it wasn't nearly as good quality but you know teak

11
00:02:35,720 --> 00:02:42,000
wood it was actually a lot more awesome and it had more purpose because there

12
00:02:42,000 --> 00:02:50,240
were thick mosquitoes where we were staying so I could be out in the forest my

13
00:02:50,240 --> 00:02:55,560
cookie was basically a mosquito screen they'd built it for adjunct on I think

14
00:02:55,560 --> 00:03:01,720
or at some point became adjunct on scooty and I don't know that he ever

15
00:03:01,720 --> 00:03:06,760
stayed in it or he maybe what in the past he'd stayed in it but then they

16
00:03:06,760 --> 00:03:19,040
built him this fancy luxury sort of house to stay in so when I went to this

17
00:03:19,040 --> 00:03:24,800
place the first time to do a ten-day course I got to stay in his cookie it was

18
00:03:24,800 --> 00:03:29,680
the only cookie left so there were two of us who were his one of his

19
00:03:29,680 --> 00:03:34,320
attendants and me who was learning how to teach so I was following along to

20
00:03:34,320 --> 00:03:39,640
listen in and to learn how to be a teacher so the two of us were going to stay

21
00:03:39,640 --> 00:03:46,080
in this cookie and most of it was this big mosquito screen and then there was a

22
00:03:46,080 --> 00:03:52,920
very small room that was actually wood and closed up and his attendant said

23
00:03:52,920 --> 00:03:57,320
to me no you sleep you can sleep out here I'll sleep in there and this was a

24
00:03:57,320 --> 00:04:05,240
Bangkok monk he was among from Bangkok a really nice guy but quite Bruce

25
00:04:05,240 --> 00:04:11,440
I think it's the word so he just you stay out here I'll stay in there and he

26
00:04:11,440 --> 00:04:15,120
went into the room and he banged around for a bit and I was just looking around

27
00:04:15,120 --> 00:04:18,520
thinking okay well this was nice I'll just sleep out here and then he comes

28
00:04:18,520 --> 00:04:22,880
storming out and he says I'm going to stay I'm going to stay with the abbot you

29
00:04:22,880 --> 00:04:27,920
can stay you can stay here and he just left and I thought it was a hard that

30
00:04:27,920 --> 00:04:35,720
you know he just suddenly went away so whatever and so I opened up the small

31
00:04:35,720 --> 00:04:38,560
room thinking well maybe you know maybe I can sleep in there I don't know and I

32
00:04:38,560 --> 00:04:45,560
looked in and there was a dead gecko right in the middle of the fire so this

33
00:04:45,560 --> 00:04:50,840
dead gecko had scared him away I'm sorry we picked up the dead gecko

34
00:04:50,840 --> 00:04:59,600
through it outside he ended up leaving the next day the next morning we went

35
00:04:59,600 --> 00:05:05,440
for breakfast and so we went for arms round through the village they drove us

36
00:05:05,440 --> 00:05:11,120
to the village and we went walking through the village on arms came back I'm

37
00:05:11,120 --> 00:05:16,760
so we're sitting down to eat there's sticky rice and there's this hot you know

38
00:05:16,760 --> 00:05:20,640
they have this paste that they make that's very very spicy and vegetables that

39
00:05:20,640 --> 00:05:24,440
you dip in it and pork grines and all this stuff I'm sitting there eating and

40
00:05:24,440 --> 00:05:29,320
we're eating whatever food we got and he this guy who ditched me the night

41
00:05:29,320 --> 00:05:35,560
before he looks over at me when he looks back at the food and he says I can't

42
00:05:35,560 --> 00:05:40,280
even meet this food and I'm tired what is it that you can even and he ended

43
00:05:40,280 --> 00:05:46,680
up leaving him leaving that day really nice guy but you know a city monk couldn't

44
00:05:46,680 --> 00:05:51,920
hack it in the countryside so I was there for 10 days and that was it was quite

45
00:05:51,920 --> 00:05:55,120
nice but I ended up going back there and staying the better part of a year and

46
00:05:55,120 --> 00:05:59,920
then then I really had to learn to deal with geckos because they were

47
00:05:59,920 --> 00:06:03,240
living in my bathroom and they were living in the in the roof so I go into the

48
00:06:03,240 --> 00:06:08,520
bathroom and there'd be a big gecko on the wall staring at me

49
00:06:09,240 --> 00:06:12,960
and then crap all over the place so I tried to get them out actually I'd use a

50
00:06:12,960 --> 00:06:19,720
Tupperware and kept them in the Tupperware and then put them on site and there

51
00:06:19,720 --> 00:06:23,720
were snakes I remember coming into the Kootian sitting down once and it was

52
00:06:23,720 --> 00:06:27,040
you know I was wearing glasses I didn't know my glasses on and I was sitting

53
00:06:27,040 --> 00:06:30,080
down in the middle of the Kootian and I turned and it looked like there was

54
00:06:30,080 --> 00:06:34,080
something had had defecated on the floor there was a brown pile and I turned

55
00:06:34,080 --> 00:06:38,560
and so I put my glasses on and it was a snake about this big one of these

56
00:06:38,560 --> 00:06:42,560
highly poisonous teeny tiny snakes sitting right beside my butt it was about an

57
00:06:42,560 --> 00:06:49,360
inch away from where I had sat down they did somehow gotten in through the floor

58
00:06:49,360 --> 00:06:53,560
when we had big snakes like the really long ones and then we had these monitor

59
00:06:53,560 --> 00:06:57,000
lizards that I never saw but we're apparently there and they're about when

60
00:06:57,000 --> 00:07:01,720
you can't see how big they have it they were like feet long

61
00:07:01,720 --> 00:07:06,720
lots of those in Sri Lanka but I never saw one in Thailand apparently they had

62
00:07:06,720 --> 00:07:11,320
them it's just that the Thai people in the forest they eat pretty much

63
00:07:11,320 --> 00:07:19,640
everything so all the monitor lizards were probably being hunted and eaten but

64
00:07:19,640 --> 00:07:26,720
none of that here unfortunately the most dangerous animal here is their cat

65
00:07:26,720 --> 00:07:35,880
like kill stuff and this cat winter geckos in Florida Pompey I know there are

66
00:07:35,880 --> 00:07:42,640
little lizards a little time on salamanders I think well that's salamanders

67
00:07:42,640 --> 00:07:48,720
some kind of those really really fast little lizards run

68
00:07:48,720 --> 00:07:57,320
big geckos I haven't seen any problem to a lot like Thailand here

69
00:07:57,320 --> 00:08:11,960
so it's my story of the day any questions we do have questions we have someone

70
00:08:11,960 --> 00:08:18,160
who last night had stayed up in in his country until like 4 a.m. because he

71
00:08:18,160 --> 00:08:21,800
wanted to ask you questions that was mummeled so I'm gonna ask mummeled

72
00:08:21,800 --> 00:08:26,440
questions tonight hopefully I'll get to see the the video which is the

73
00:08:26,440 --> 00:08:31,400
meditation helps meditators beat procrastination

74
00:08:35,680 --> 00:08:40,840
it depends I mean generally yeah I mean meditation helps with a lot of

75
00:08:40,840 --> 00:08:45,200
things it helps you sort of a lot of your issues helps you clear your mind

76
00:08:45,200 --> 00:08:55,240
but I think it it can in certain cases lead to procrastinating or reassessing

77
00:08:55,240 --> 00:09:02,280
priorities especially in the short term it can lead you to have difficulty

78
00:09:02,280 --> 00:09:10,840
performing duties that seem meaningless so in fact can lead to procrastination I

79
00:09:10,840 --> 00:09:16,920
think in the long term you get your priorities straightened out and those

80
00:09:16,920 --> 00:09:21,640
things that are duties you see them just as duties and you do them without

81
00:09:21,640 --> 00:09:28,480
letting your not letting any kind of aversion arise because you see it's

82
00:09:28,480 --> 00:09:35,360
still a version to be upset about doing useless things a meditator should

83
00:09:35,360 --> 00:09:41,040
be able to do something that's useless without being upset about it but you

84
00:09:41,040 --> 00:09:47,000
have this you don't you've taken out a desire to do it so when there's no

85
00:09:47,000 --> 00:09:51,920
desire I think well why am I doing this and so eventually you come to the I

86
00:09:51,920 --> 00:09:55,960
the concept of a duty of doing something just for doing it and doing it as

87
00:09:55,960 --> 00:09:59,280
a meditation so doing it as mindfully as you can and it doesn't matter

88
00:09:59,280 --> 00:10:03,920
whether you're doing walking meditation or cooking meditation or

89
00:10:03,920 --> 00:10:09,000
construction meditation you know whatever it is it can be a meditation

90
00:10:09,000 --> 00:10:15,480
typing meditation speaking meditation can all be meditation but I think in the

91
00:10:15,480 --> 00:10:19,120
beginning that's difficult and as a result leads it makes it difficult

92
00:10:19,120 --> 00:10:25,520
sometimes to do certain things I think procrastination is there and I think you

93
00:10:25,520 --> 00:10:31,280
could argue that in the long term it makes it really difficult to increasingly

94
00:10:31,280 --> 00:10:38,360
difficult to do the sorts of things that you're accustomed to doing and this

95
00:10:38,360 --> 00:10:42,240
may lead to falling behind in your work and eventually just saying I'm sorry I

96
00:10:42,240 --> 00:10:47,640
can't do this I can't bring myself to do but but that's sort of the

97
00:10:47,640 --> 00:10:51,960
outlier I'd say in general if I'm still going to help you because why do we

98
00:10:51,960 --> 00:10:57,600
procrastinate have we procrastinated of laziness we procrastinate out of

99
00:10:57,600 --> 00:11:06,160
our our addictions to to you know entertainment and so on and we procrastinate

100
00:11:06,160 --> 00:11:17,880
out of a version to hard work a version to pain

101
00:11:17,880 --> 00:11:30,520
come see everybody mom I was I was the internet I was broadcasting in there

102
00:11:30,520 --> 00:11:33,080
that forgot to turn them off just moved out here to see if it would work

103
00:11:33,080 --> 00:11:37,280
can you all right how let you go it's my mom hi Ruben

104
00:11:37,280 --> 00:11:43,400
I'm gonna see the internet comes the hello hi this is my younger brother

105
00:11:43,400 --> 00:11:50,280
Ruben hi how are you good how are you you're the internet there's more

106
00:11:50,280 --> 00:11:57,320
internet it's a small part of the internet so yeah my mom just went to pick up

107
00:11:57,320 --> 00:12:04,600
my younger brother at your port and they just got back so yeah yesterday we

108
00:12:04,600 --> 00:12:09,080
went to the children's home and that's actually what it's called the children's

109
00:12:09,080 --> 00:12:15,880
home incorporated and met with a guy named Adam and he showed us around we

110
00:12:15,880 --> 00:12:21,480
even got to go into one of the one of the residences and and kind of see the

111
00:12:21,480 --> 00:12:26,480
kids I I was kind of I didn't want to get I was kind of shy about it thinking

112
00:12:26,480 --> 00:12:30,040
I don't want these people to be on display it feels kind of weird you know

113
00:12:30,040 --> 00:12:34,720
like having a tour like a zoo or something you know I didn't I was like no I

114
00:12:34,720 --> 00:12:39,240
don't need to see them it was happy that we got to see them but kind of the

115
00:12:39,240 --> 00:12:46,640
wrong circumstances like as donors she's just giving go but I don't know it

116
00:12:46,640 --> 00:12:51,720
was really nice to see them and it was nice to just to get a feel of the

117
00:12:51,720 --> 00:12:58,400
good that we're doing so there's that for sure and Adam was he said he

118
00:12:58,400 --> 00:13:03,520
actually looked up my videos on YouTube and he watched a couple of them and he

119
00:13:03,520 --> 00:13:07,000
said he was interested in he was gonna and maybe he's watching right now I

120
00:13:07,000 --> 00:13:12,280
don't know but really they were really happy and

121
00:13:12,280 --> 00:13:19,320
Sunda Mullie and their group brought some baby stuff because they actually

122
00:13:19,320 --> 00:13:26,440
take care of pregnant mothers they have five now I think pregnant teenagers I

123
00:13:26,440 --> 00:13:33,480
think or mothers and mothers anyway and so there's they brought some

124
00:13:33,480 --> 00:13:39,600
baby stuff baby wipes and stuff and other things all the knowledge was

125
00:13:39,600 --> 00:13:45,680
really great so congratulations everyone thank you everyone for taking part

126
00:13:45,680 --> 00:13:55,480
it was a really neat thing absolutely meditation meditation makes you do

127
00:13:55,480 --> 00:14:01,440
things interesting about procrastination is my stepfather who's house this

128
00:14:01,440 --> 00:14:07,400
is with my mother he came now in the beginning I didn't tell them what it was

129
00:14:07,400 --> 00:14:12,560
I was talking to my mom I said so I've got a crisp we're giving this on

130
00:14:12,560 --> 00:14:17,760
on Tuesday we have to go somewhere for a Christmas present it's my

131
00:14:17,760 --> 00:14:21,760
Christmas or it was like I just was very vague about who was giving you

132
00:14:21,760 --> 00:14:26,160
it was it's just a Christmas present she said oh is it something Alan's going

133
00:14:26,160 --> 00:14:33,120
to like because you know I mean he's not on my trip she was like is it

134
00:14:33,120 --> 00:14:36,200
gonna be something Buddhist I said oh it's I'm actually I'm sure he will and

135
00:14:36,200 --> 00:14:40,040
she was kind of well I'll just tell you so I told her what it was and I talked

136
00:14:40,040 --> 00:14:44,600
with him afterwards and he said he's been trying to do this kind of thing for

137
00:14:44,600 --> 00:14:49,800
years with his family like every Christmas they his family and you know

138
00:14:49,800 --> 00:14:54,000
American family get together and give gifts and it's like we've all got good

139
00:14:54,000 --> 00:14:58,760
jobs and none of us need this need these things we don't need more stuff so

140
00:14:58,760 --> 00:15:02,960
getting gifts for each other's kind of ridiculous so they've tried many times

141
00:15:02,960 --> 00:15:08,840
with my mother as well to organize some kind of something like this and so he

142
00:15:08,840 --> 00:15:14,760
was really happy about it but but like it's that kind of thing like like we

143
00:15:14,760 --> 00:15:19,520
want to do good deeds but do we ever get out and do them and it's exciting

144
00:15:19,520 --> 00:15:26,560
being among us to have the to be in a position to to initiate this sort of

145
00:15:26,560 --> 00:15:33,720
thing and to gather people together like this I mean this is the idea of

146
00:15:33,720 --> 00:15:40,680
community and working together as an organization it's really awesome it's

147
00:15:40,680 --> 00:15:48,040
the kind of thing we couldn't do alone so we often do feel powerless you know

148
00:15:48,040 --> 00:15:54,400
what am I going to be able to do or how am I going to be able to succeed

149
00:15:54,400 --> 00:16:02,160
spiritually and this is why organized religion is of course so enticing and

150
00:16:02,160 --> 00:16:07,320
dangerous actually you know because people go to religion not because of

151
00:16:07,320 --> 00:16:12,960
the views it espouses but because it's organized and because it's powerful

152
00:16:12,960 --> 00:16:17,720
and so often they end up with whatever views the religious organization

153
00:16:17,720 --> 00:16:22,600
espouses no of course we're lucky because our organization has right and

154
00:16:22,600 --> 00:16:29,920
perfect views but that's just a coincidence that may not exactly it's not

155
00:16:29,920 --> 00:16:34,800
quite fair but it's it can be dangerous there is that hypnotic sort of

156
00:16:34,800 --> 00:16:41,160
pole of religion but but really in a good way I mean it's power of power

157
00:16:41,160 --> 00:16:51,400
is is the good in a sense of good for the person if you have whatever ambitions

158
00:16:51,400 --> 00:16:56,360
it's good for those ambitions so you can accomplish more in a group in

159
00:16:56,360 --> 00:17:03,320
institution so it's great to be in that kind of position now that's not

160
00:17:03,320 --> 00:17:06,160
exactly talking about meditation but meditation is a big part of that

161
00:17:06,160 --> 00:17:14,600
because meditation clears your mind and it frees you from the inertia

162
00:17:14,600 --> 00:17:21,680
sometimes of just a fear or powerlessness or depression so that you're

163
00:17:21,680 --> 00:17:26,480
actually able to accomplish things you actually do get off your butt and sit

164
00:17:26,480 --> 00:17:28,880
on your butt

165
00:17:31,880 --> 00:17:36,520
do more questions yes

166
00:17:36,520 --> 00:17:48,960
sorry I'm so involved with listening I forgot to look for the next one

167
00:17:55,600 --> 00:18:00,000
memorable you to demo I have a question if you meditate and therefore have a

168
00:18:00,000 --> 00:18:04,880
clear awareness of your feelings and see them as they are you constantly do

169
00:18:04,880 --> 00:18:09,520
that for a long time would you get to a point that you have enough experience

170
00:18:09,520 --> 00:18:14,440
to sort of outsmart your emotions and therefore not feel them another you

171
00:18:14,440 --> 00:18:19,520
get to a point where emotions are merely a part of your past

172
00:18:20,360 --> 00:18:24,960
it sounds right off that you're trying to intellectual is trying to figure out

173
00:18:24,960 --> 00:18:29,320
the meditation how does it work what is this thing that there where is this

174
00:18:29,320 --> 00:18:34,960
leading it sounds like you might be over analyzing things it's a little bit

175
00:18:34,960 --> 00:18:40,280
more simple and simple than that you I think I mean I'm not quite clear on

176
00:18:40,280 --> 00:18:45,520
what exactly you're asking but it sounds like just a little bit over

177
00:18:45,520 --> 00:18:51,880
intellectual I mean you you'll see when you meditate what is going on in your

178
00:18:51,880 --> 00:18:58,200
mind you'll see how your emotions arise you'll see what they lead to and you'll

179
00:18:58,200 --> 00:19:04,720
see the nature you'll see the nature of the things that you desire and the

180
00:19:04,720 --> 00:19:15,680
things that you're averse to and you'll see them as not being worthy of your

181
00:19:15,680 --> 00:19:22,800
reactions to them that they're simply phenomena that arise and seeds so it's not

182
00:19:22,800 --> 00:19:27,880
about outwitting your emotions in fact that's a dangerous the idea that you

183
00:19:27,880 --> 00:19:34,520
can have have an emotion and be intent upon a certain emotion and that you

184
00:19:34,520 --> 00:19:39,400
can somehow outwit that or outsmart that or arise about it that's not the

185
00:19:39,400 --> 00:19:44,800
case that emotion has to change I mean if you if you like something or want

186
00:19:44,800 --> 00:19:49,200
something or if you're averse to something or behave something then that's the

187
00:19:49,200 --> 00:19:55,520
issue the issue is that that is is your desire you can't avoid that you can't

188
00:19:55,520 --> 00:20:02,480
or round it or somehow circumvent it you have to deal with that you have to

189
00:20:02,480 --> 00:20:10,720
change that it's not about forcing or controlling or switching off it's

190
00:20:10,720 --> 00:20:15,600
about changing your view so that you don't have that emotion right so

191
00:20:15,600 --> 00:20:20,440
something arises and you like it now there's a reason why you like it you like

192
00:20:20,440 --> 00:20:23,280
it because you think it's going to bring you happiness you think there's

193
00:20:23,280 --> 00:20:28,160
something intrinsically good about that you have a view or an idea or a

194
00:20:28,160 --> 00:20:35,160
conception a notion that's artificial about that and I mean really that's the

195
00:20:35,160 --> 00:20:40,960
keys it's artificial because as you look closer you start to see about

196
00:20:40,960 --> 00:20:45,800
everything in the world that there's no inherent

197
00:20:45,800 --> 00:20:49,880
desirability in them there's nothing about the object that is desirable that

198
00:20:49,880 --> 00:20:58,080
makes it desire we make this up we it's it's arbitrary it's subjective it's not

199
00:20:58,080 --> 00:21:03,080
related to the nature of the object and we do this with things that we don't

200
00:21:03,080 --> 00:21:06,680
like something arises you don't like it there's a reason you don't like it

201
00:21:06,680 --> 00:21:09,800
you don't like it because you believe that it's going to cause you suffer

202
00:21:09,800 --> 00:21:15,680
you believe that it's bad that it's it's going to have a negative effect on

203
00:21:15,680 --> 00:21:21,400
as you are mindful you start to see that no that's actually not the case this

204
00:21:21,400 --> 00:21:25,880
thing has no power to hurt you whatsoever pain that arises also has no power to

205
00:21:25,880 --> 00:21:31,920
cause you suffering it's just pain and as a result you don't give rise to those

206
00:21:31,920 --> 00:21:37,160
emotions you see that those emotions are useless are unwarranted are harmful

207
00:21:37,160 --> 00:21:44,200
are the true source of suffering and stress and you just give them up you

208
00:21:44,200 --> 00:21:49,960
change your views and your views therefore don't give rise or don't lean to

209
00:21:49,960 --> 00:21:57,160
those emotions it's not about tricking it's not about there's no trick there's

210
00:21:57,160 --> 00:22:03,120
no way to there's no shortcut you learn you change and you just don't want

211
00:22:03,120 --> 00:22:07,640
the things that right now you want you just don't want them in that's the way

212
00:22:07,640 --> 00:22:19,400
there's only one way and it's a hard way it's the direct and the honest

213
00:22:19,400 --> 00:22:25,040
systematic methodical way thank you

214
00:22:25,040 --> 00:22:27,840
you want to be

215
00:22:27,840 --> 00:22:33,160
if one's free from suffering and unattached to anything wouldn't that make

216
00:22:33,160 --> 00:22:40,160
broadly pursue its pointless what makes them pointful pointed

217
00:22:40,160 --> 00:22:47,080
what's the opposite of pointless purposeful appointed is pointed or

218
00:22:47,080 --> 00:22:52,520
no pointless yes it is something else it means blunt

219
00:22:52,520 --> 00:22:59,520
I suppose pointful is a word now it doesn't

220
00:22:59,520 --> 00:23:06,720
doesn't quite having a point yeah

221
00:23:06,720 --> 00:23:11,760
you know the they're pointless anyway regardless of whether you meditate or

222
00:23:11,760 --> 00:23:18,920
not worldly pursuits have no point any point that they lead to is is just one

223
00:23:18,920 --> 00:23:24,680
point in time and and life goes on after that so you haven't accomplished

224
00:23:24,680 --> 00:23:29,840
anything permanent anything you've accomplished will eventually fade away

225
00:23:29,840 --> 00:23:34,400
you'll wind up back where you started we do that's why we're here today we

226
00:23:34,400 --> 00:23:42,840
wind up again back where we started again again and again and so meditation

227
00:23:42,840 --> 00:23:51,560
just helps you realize that helps you give up your ambitions as you see

228
00:23:51,560 --> 00:23:58,560
that really it's just the same old it's like a a rat a mouse and on the wheel

229
00:23:58,560 --> 00:24:06,880
you know that's what our that's what our endeavors amount to

230
00:24:06,880 --> 00:24:11,000
this world even helping people you know in the big secret is you can help

231
00:24:11,000 --> 00:24:14,280
people until you're blue in the face and there's still more people to

232
00:24:14,280 --> 00:24:23,280
help so helping people becomes not and not a means to and you know not

233
00:24:23,280 --> 00:24:28,280
helping people is in the end itself it's more of a means helping people is

234
00:24:28,280 --> 00:24:33,360
about doing the right thing and spiritual development and being on the right

235
00:24:33,360 --> 00:24:39,120
path yourself it's the right way to live that's about the best thing you can

236
00:24:39,120 --> 00:24:42,800
say of it not that it's going to change the world I'm going to change the world

237
00:24:42,800 --> 00:24:50,000
what does that mean there's more always going to be more beings to help even

238
00:24:50,000 --> 00:24:56,600
after the universe explodes or collapses it's going to start all over again

239
00:24:56,600 --> 00:25:01,480
eventually it will start all over again there'll be more beings coming

240
00:25:01,480 --> 00:25:10,400
it's all pointless in the end it's like the book of kings in the Bible which is

241
00:25:10,400 --> 00:25:13,120
kind of a funny book because he says everything's pointless everything's

242
00:25:13,120 --> 00:25:18,240
pointless therefore therefore worship God so it's a great book it's just the

243
00:25:18,240 --> 00:25:23,440
conclusion is a bit weird because if it's all pointless well would you want to

244
00:25:23,440 --> 00:25:27,440
like blame God and say stupid God what did you what are you doing making this

245
00:25:27,440 --> 00:25:35,680
universe so pointless but no the world is pointless life is pointless therefore

246
00:25:35,680 --> 00:25:42,600
spend your time praising God I don't know I'm not very impressed

247
00:25:46,880 --> 00:25:50,920
this question is probably a result of my poor understanding of this

248
00:25:50,920 --> 00:25:55,320
subject because when I think about it logically this is from the same person

249
00:25:55,320 --> 00:25:59,720
when I think about it logically that's what I feel comes down to but even if

250
00:25:59,720 --> 00:26:04,360
I were completely unattached to anything I imagine I'd still have the desire to

251
00:26:04,360 --> 00:26:09,160
travel to new places reading a book and learn new stuff

252
00:26:09,720 --> 00:26:15,840
that was a really more of a comment no you wouldn't when your desire is an

253
00:26:15,840 --> 00:26:19,560
attachment it it's just semantics whether you're talking about desire

254
00:26:19,560 --> 00:26:26,920
attachment there's something something you're stuck on that's leading you on

255
00:26:26,920 --> 00:26:31,800
that's going to lead to further becoming further becoming means something is

256
00:26:31,800 --> 00:26:36,520
going to come of it so if you didn't have the desire to travel there wouldn't

257
00:26:36,520 --> 00:26:40,960
be travel you know so travel is going to come of it there wouldn't be all the

258
00:26:40,960 --> 00:26:49,000
issues surrounding travel like visas and expenses and well meeting new people

259
00:26:49,000 --> 00:26:53,160
I mean travel isn't bad I'm not suggesting that but your desire to travel

260
00:26:53,160 --> 00:27:00,840
creates something for you you know more becoming and it also creates the desire

261
00:27:00,840 --> 00:27:06,200
to travel it becomes habitual it's not an end to itself when you desire to

262
00:27:06,200 --> 00:27:11,640
travel it creates a more liking of the traveling experience and you become what

263
00:27:11,640 --> 00:27:17,080
we'd say intoxicated by it and you wanted more and then when you can't travel

264
00:27:17,080 --> 00:27:20,400
or when people say traveling socks you shouldn't travel why don't you stay

265
00:27:20,400 --> 00:27:24,720
at home why don't you settle down you get upset why do you get upset because

266
00:27:24,720 --> 00:27:29,640
you're attached right it's all very subtle it's not a big deal it's not like

267
00:27:29,640 --> 00:27:35,120
you're going to hell for it it's not evil evil but it's part of what leads to

268
00:27:35,120 --> 00:27:42,200
more rebirth and it's just in the end the same old same old same old where

269
00:27:42,200 --> 00:27:49,480
excited by new things then they become old and we need newer things and then

270
00:27:49,480 --> 00:27:53,480
we die and forget it all and come back and do it again and in the meantime we

271
00:27:53,480 --> 00:27:58,640
suffer a lot eventually start to say is it really worth all the effort

272
00:27:58,640 --> 00:28:03,360
and then so we start to meditate and we say no it's not really worth the effort

273
00:28:03,360 --> 00:28:06,480
better I just let go

274
00:28:17,120 --> 00:28:22,600
and it catches up all the questions for tonight that's it I've been away for over a

275
00:28:22,600 --> 00:28:36,560
week I did three exams and I think I got a 90 a 95 and a 99 very nice yeah so

276
00:28:36,560 --> 00:28:43,040
made me think that I did learn stuff especially in philosophy which was

277
00:28:43,040 --> 00:28:46,720
surprising because I thought oh this is rubbish this Western philosophy but

278
00:28:46,720 --> 00:28:50,280
they really frame their arguments in an interesting way and they're talking

279
00:28:50,280 --> 00:28:54,840
about a lot of the same things that they were talking about in India they are

280
00:28:54,840 --> 00:29:00,880
talking about important topics even from a meditative from a Buddhist point

281
00:29:00,880 --> 00:29:05,440
of view most of them have it all wrong in fact all of them have most of it all

282
00:29:05,440 --> 00:29:11,800
wrong but it's this is the kind of important argument that we have to be

283
00:29:11,800 --> 00:29:27,720
able to address so like the idea of free will of desire what exists I guess

284
00:29:27,720 --> 00:29:34,920
it's not entirely relevant but much of it is and learning languages even

285
00:29:34,920 --> 00:29:41,040
learning Latin was I think a great thing and I realized that Latin is actually

286
00:29:41,040 --> 00:29:44,400
the language it is the linga Franca for the world so it is something that you

287
00:29:44,400 --> 00:29:49,400
can actually use to talk to people it's funny I've always wanted to use

288
00:29:49,400 --> 00:29:55,760
poly in that way but there's very few people who speak poly anymore even

289
00:29:55,760 --> 00:30:03,280
monks at least you know what the chanting is all about

290
00:30:03,280 --> 00:30:08,400
chanting well by by knowing poly you know what the chanting is you know

291
00:30:08,400 --> 00:30:15,440
I can read the depicted chanting isn't a big deal for me but being able to

292
00:30:15,440 --> 00:30:20,200
read the tags that's awesome absolutely there's nothing wrong with learning

293
00:30:20,200 --> 00:30:23,120
but that's why I'd like to be able to speak it with people and say it would

294
00:30:23,120 --> 00:30:32,080
keep me sharp and help me train train it more but no one who speaks it

295
00:30:32,080 --> 00:30:40,600
well then that was half an hour so I guess that's all we get tonight but if you

296
00:30:40,600 --> 00:30:46,200
have more questions I'll try to be back every night tomorrow is Christmas

297
00:30:46,200 --> 00:30:51,240
Eve tomorrow might not be able to I think tomorrow it's tomorrow's autumn also

298
00:30:51,240 --> 00:30:59,600
my father's birthday so we'll probably be doing a family hangout with him

299
00:30:59,600 --> 00:31:09,560
and then the next day is Christmas which of course is meaningless so my family

300
00:31:09,560 --> 00:31:14,920
will probably be picking out on Turkey but at nine I should be able to broadcast

301
00:31:14,920 --> 00:31:23,600
Christmas day Robin will you be available Christmas day and yes let's plan to

302
00:31:23,600 --> 00:31:31,040
meet here probably not tomorrow but probably that sounds good and that

303
00:31:31,040 --> 00:31:34,400
a question but Lee Lee had a comment that it looks like you're auditioning

304
00:31:34,400 --> 00:31:37,880
for Star Wars with the black background I thought the same thing that there's

305
00:31:37,880 --> 00:31:48,600
a new Star Wars out now yeah yeah it has a bit of a Star Wars look your

306
00:31:48,600 --> 00:31:57,240
background yeah a little bit a little bit and there is one more question if

307
00:31:57,240 --> 00:32:04,920
you have a couple more minutes Bombay okay I constantly feel very stressed

308
00:32:04,920 --> 00:32:09,320
when I meditate and this makes it difficult to feel any movement at the abdomen

309
00:32:09,320 --> 00:32:17,240
what should I do feels stressed constantly yes focus on the stress I would try

310
00:32:17,240 --> 00:32:23,000
lying meditation when you really stress lying meditation can be great but

311
00:32:23,000 --> 00:32:29,200
focus on the stress and say stress stress or anxious or frustrated however

312
00:32:29,200 --> 00:32:33,240
there's different flavors of stress right so whatever the flavors if you're

313
00:32:33,240 --> 00:32:40,520
worried or if your anxious or if you're angry if you're depressed or just

314
00:32:40,520 --> 00:32:47,200
stressed stress it's overwhelmed if you're overwhelmed but if you can't feel

315
00:32:47,200 --> 00:32:51,720
the abdomen at all try lying down and eventually it'll work itself out if

316
00:32:51,720 --> 00:32:56,440
you meditate regularly you find yourself getting less stressed and it'll work

317
00:32:56,440 --> 00:33:01,200
it'll eventually be easier to sit up and meditate so in the meantime you've

318
00:33:01,200 --> 00:33:10,240
got to you know approach it from the side do lying meditation do walking

319
00:33:10,240 --> 00:33:18,280
meditation break it up a bit it'll it'll it'll get better you know sounds

320
00:33:18,280 --> 00:33:22,960
like probably or a beginner and beginners at everything are not very

321
00:33:22,960 --> 00:33:30,040
proficient so take your time you'll get there if you want looks like we got lots

322
00:33:30,040 --> 00:33:36,320
of slots free so you can sign up to meet with me once a week you can go

323
00:33:36,320 --> 00:33:40,840
through a course if you're able to do at least an hour a day preferably two

324
00:33:40,840 --> 00:33:47,340
or eventually up to two hours a day you can sign up there

325
00:33:48,240 --> 00:33:51,960
that's right I was supposed to ask you about they are you conducting the

326
00:33:51,960 --> 00:33:55,760
individual meetings while you're away or is everything canceled I missed I

327
00:33:55,760 --> 00:34:01,040
think I thought I missed actually I didn't miss because there weren't any

328
00:34:01,040 --> 00:34:05,200
there's a lot of free free ones but on Monday I would have missed but I didn't

329
00:34:05,200 --> 00:34:16,120
know one came I'm doing them today I did one two I guess I did one in the

330
00:34:16,120 --> 00:34:23,840
morning as well no Adam wasn't here he's away for Christmas but tomorrow I

331
00:34:23,840 --> 00:34:30,880
have three if all goes is planned one on Friday and then I've added two

332
00:34:30,880 --> 00:34:37,920
extras on Saturday and there's four people on Saturday four people on

333
00:34:37,920 --> 00:34:47,800
Sunday but yeah lots of free spots maybe I'll make a push maybe I'll do a

334
00:34:47,800 --> 00:34:53,300
YouTube video about that some point let people know they can sign up for

335
00:34:53,300 --> 00:35:01,880
courses be good okay thanks Robin for joining me thanks for tuning in

336
00:35:01,880 --> 00:35:04,920
thank you thank you

337
00:35:04,920 --> 00:35:07,160
yeah

338
00:35:07,160 --> 00:35:25,720
happy Christmas take care

