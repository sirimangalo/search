Okay, so this next installment is me writing a letter to the more park acorn, the local
newspaper. There's the more park acorn and they have a letter to the editor
section. So I figured after being stopped by two police officers in the past
week that I should put everyone's try to help put everyone's minds at rest
by going over explaining in what I am and so I've done that here and I'll
have you check this out. Dear Citizens of More Park, I am writing to introduce
myself as a new resident of your city. I'm a Buddhist monk and I teach
insight meditation to students from around the world. I am also currently
doing volunteer work as a chaplain at the Federal Detention Center in
Los Angeles and I teach over the internet via YouTube at channel UTA Dhammo
as well. Every morning I walked to the local Thai restaurants whose owners
kindly give me food from my daily meal. I don't beg and I don't touch money
at all. I don't practice martial arts or any sorts of religious ritual. I wear
simple course robes that often draw attention to my person and I wish to make
it clear that I'm not here to convert people or interfere with their lives in
any way. Though I'm happy to teach meditation to those who ask. Otherwise I
mostly keep to myself and attend to my student. Recently I was taught by a
local police officer who politely asked who I was telling me that they had
received a call by a concerned citizen wondering who I was and what I carry
under my robes when I walk. As I explained to him and later to the sergeant at the
police department whom I took it upon myself to visit I am a simple monk and I
carry a simple arms ball under my robes that is used to carry the food I
receive. I don't wear shoes when on arms round in order to maintain humility
when receiving food otherwise I wear shoes as normal people do. I hope my
presence in more park will be seen as an addition to the community rather than
a threat the latter of which I guarantee that I am not. I am residing at the
Wood Creek apartments on spring road and I'm in the process of setting up a
small residence in the countryside nearby. This letter is only meant to ease the
minds of people who see the first Buddhist monk ever to come to live in more
park that I should present no danger or inconvenience to anyone and hope to
be on friendly terms with the residents of this fine community. Thank you for
reading this and thanks to the acorn for publishing it sincerely brother Noah
Yuta Damo. That's my letter and that's what I'm going to send to you right now
I'm going to email that to the more park acorn and see if that can help to
ease people's minds when they see me walking down the road. Apparently it's a
big sight I never see it but everyone tells me how when I walk down the street
everyone is like their heads just go boom maybe something strange and I think a
lot of people actually see it as kind of void exhibitionism where you know
trying to it's a stunt like trying to gain attention which is surprising to me
because or you know I didn't realize that that's how it would be seen and a
lot of people are jeering and you get a lot of kids you know taking taking the
opportunity to poke fun or kind of like play back you know interact with me
as though it were some kind of stunt that I was pulling or playing which is a
shame really but I'm thinking that it's going to get better and I think this is
a lot better than the community that I was in before where I was actually
arrested because someone claimed that I was doing things that I've never done.
You're hoping that this community will be at least a little bit more open-minded
and I won't be subject to such difficulties here so just another part of my
life never never a dull moment thanks for tuning in this is the latest
installment of a life in a day for a month.
