1
00:00:00,000 --> 00:00:05,480
Is it necessary to have an enlightened teacher or anyone who has reached a certain level can be a teacher?

2
00:00:06,920 --> 00:00:12,080
You don't even have to have reached a level. I mean if you can parrot what your teacher's teach

3
00:00:12,080 --> 00:00:14,840
I talked about this last week a little bit

4
00:00:18,680 --> 00:00:21,680
If you take the V2D Maga as as as a

5
00:00:22,440 --> 00:00:25,000
Guide it says, you know anyone can teach

6
00:00:25,000 --> 00:00:30,240
The the more knowledge they have of the Buddha's teaching the better they'll be able to teach and

7
00:00:31,320 --> 00:00:35,120
Even better than having knowledge of the Buddha's teaching is having the realizations

8
00:00:35,120 --> 00:00:39,240
So the best is to go to the Buddha then the bed then go to an Arahad then go to

9
00:00:39,800 --> 00:00:43,080
Anagami then a Sakitikami then a Sota Pana

10
00:00:43,080 --> 00:00:47,880
If you can't even find a Sota Pana then go to someone who has memorized the whole the Pitika

11
00:00:48,320 --> 00:00:53,120
If you can't find such a person go to someone who has memorized two to Pitika's one Pitika

12
00:00:53,120 --> 00:00:55,920
as a good knowledge of the Pitika's and so on

13
00:00:57,400 --> 00:01:00,320
But there's a story that we give of this monk who

14
00:01:02,440 --> 00:01:05,200
Taught all of his students and they all became enlightened and

15
00:01:06,000 --> 00:01:10,240
He himself was an ordinary world thing actually the V2D Maga has a few such monks

16
00:01:11,440 --> 00:01:13,200
But this one

17
00:01:13,200 --> 00:01:18,960
He thought he was a great teacher and he was totally oblivious to the fact that he hadn't gained anything until one day one of his

18
00:01:18,960 --> 00:01:25,680
Anagami students came in and said to him. Hey, how about a lesson today and he said oh, I'm too busy today

19
00:01:25,680 --> 00:01:30,840
I'm a my schedule is full and he said no, no, I didn't I didn't mean you giving me a lesson

20
00:01:30,840 --> 00:01:36,840
But here's a lesson for you and he said he sat cross-legged and floated up into the air and said

21
00:01:36,960 --> 00:01:38,840
You don't even have time for yourself

22
00:01:38,840 --> 00:01:44,760
How can you give lessons to other people and he flew out the window? He said I don't want a lesson from you and

23
00:01:44,760 --> 00:01:55,160
And you know this really you should perturb the the world-ling monk teacher and he went off to practice meditation and eventually became an arhope

24
00:01:58,120 --> 00:02:04,160
So there's no no need to be now the story goes on actually I should finish the story

25
00:02:04,160 --> 00:02:07,340
For those of you who haven't heard it. He went off into the forest

26
00:02:08,240 --> 00:02:10,800
Where there was nobody he said he said in that case

27
00:02:10,800 --> 00:02:15,560
This monk is correct and or he was just horrified by the fact that

28
00:02:15,880 --> 00:02:20,880
Jeez here here my students are totally enlightened and they can read my minds and know that I'm not

29
00:02:21,640 --> 00:02:28,840
How can I possibly stay here? He was ashamed and so he went off into the forest where there was no one and he just practiced on his own

30
00:02:28,840 --> 00:02:31,800
And he did such strenuous walking meditation

31
00:02:33,160 --> 00:02:39,200
Pushing himself really hard and this is what happens when people learn too much because they have they feel these expectations on them

32
00:02:39,200 --> 00:02:42,320
And they have this desire to become a light these expectations and

33
00:02:43,280 --> 00:02:47,920
Pushing themselves to become a light which is hopeless. It's it's at that. No, it's itself

34
00:02:48,800 --> 00:02:50,800
meditation has to be

35
00:02:52,320 --> 00:02:58,840
Natural it's about coming back to nature. So the walking and the knowledge of the walking has to come naturally slowly slowly

36
00:02:59,080 --> 00:03:05,480
You know not pushing but probing and and nudging the mind and and easing the mind into place

37
00:03:05,480 --> 00:03:09,680
You can't you know just force the square pegs to fit into the round holes

38
00:03:12,080 --> 00:03:17,720
And so he got nowhere and he was totally he wound up his clock eventually he it sprung and he got

39
00:03:19,280 --> 00:03:24,040
We got fed up and he sat down and totally overwhelmed and he started crying

40
00:03:25,680 --> 00:03:29,080
And as he started crying suddenly this angel appeared beside him

41
00:03:29,720 --> 00:03:32,640
Sat down beside him and started crying as well and he looks over

42
00:03:32,640 --> 00:03:38,200
And there's here's this angel because of course it's a forest with nobody in it. This is angel sitting crying beside him

43
00:03:38,200 --> 00:03:39,000
And he's like

44
00:03:39,000 --> 00:03:40,840
Who are you?

45
00:03:40,840 --> 00:03:47,080
The stop's crying and he says I'm an angel that lives in this forest. What are you doing here? Well, I knew you were a great

46
00:03:47,240 --> 00:03:49,760
Meditation teacher so I wanted to come and practice according to you

47
00:03:50,840 --> 00:03:52,800
According to your teachings. Well, why are you crying?

48
00:03:53,560 --> 00:03:54,840
Why are you crying?

49
00:03:54,840 --> 00:03:58,900
So I figured about that if he's crying. He's a great teacher must be the way to become a

50
00:03:58,900 --> 00:04:05,180
point and so the guy's like whoa, I think you know again totally ashamed of himself and

51
00:04:05,820 --> 00:04:12,580
But crying like that can be useful sometimes not the crying itself but the giving up the feeling how hopeless it is because that's

52
00:04:12,580 --> 00:04:20,140
The realization of not so the realization that you can't control and that's what allows you to finally give up and really start

53
00:04:20,140 --> 00:04:25,980
practicing some people have to reach that again and again and again like banging your head against the wall until you realize how

54
00:04:25,980 --> 00:04:27,980
futile it is and

55
00:04:27,980 --> 00:04:31,980
And so this is what he did and after that then he became enlightened

56
00:04:33,220 --> 00:04:36,780
So this story is a really good example of how this works

57
00:04:36,780 --> 00:04:43,260
You don't need to be enlightened to teach, but that's not saying anything. That's not it's not saying great

58
00:04:43,460 --> 00:04:45,460
So I mean I've all become a great teacher

59
00:04:45,860 --> 00:04:52,900
And saying don't be as I said last week don't be too attached to the idea of being a good teacher or this person being a good teacher

60
00:04:53,100 --> 00:04:55,700
It doesn't mean nearly as much it doesn't mean

61
00:04:55,700 --> 00:04:59,260
Quite as much anyway as we think

62
00:04:59,740 --> 00:05:05,340
You know there are great teachers outside of Buddhism who are able to convince their students of their views

63
00:05:07,300 --> 00:05:09,300
Now of course meditation

64
00:05:09,580 --> 00:05:13,100
Having its beneficial side effects allows you to be a better teacher

65
00:05:13,820 --> 00:05:16,300
But it's not required to be a good teacher

66
00:05:16,300 --> 00:05:23,820
The the I guess the only proviso is

67
00:05:24,260 --> 00:05:30,100
Whichever was probably going through everyone's mind is that without the meditation practice and without meditative

68
00:05:30,300 --> 00:05:33,460
Realizations, it's incredibly dangerous, isn't it? No

69
00:05:33,860 --> 00:05:36,780
It's not dangerous if the person goes by the Buddhist teaching

70
00:05:36,780 --> 00:05:41,900
But if the person still is a putujana a person with full green anger and delusion

71
00:05:41,900 --> 00:05:46,820
How can we be sure they're going to go according to the tepitika? How can we be sure they're going to go?

72
00:05:46,820 --> 00:05:51,420
So what generally happens people throw out the commentaries throw out different so it does throw out

73
00:05:52,140 --> 00:05:54,140
various and you know

74
00:05:55,100 --> 00:05:57,100
Conflicting ideas take a part of the tepitika

75
00:05:57,580 --> 00:06:01,300
Interpreting in their own way and teach some sort of lopsided dhamma

76
00:06:03,820 --> 00:06:05,820
Instead of going by the accepted

77
00:06:06,860 --> 00:06:11,580
Interpretation this is this is much more common people are much more inclined to do this

78
00:06:11,580 --> 00:06:15,340
based on their own views and opinions and and ideas

79
00:06:16,620 --> 00:06:19,500
This is a sign of the noble people from what I've seen

80
00:06:20,460 --> 00:06:22,460
if you're asking me and

81
00:06:23,100 --> 00:06:25,420
from from and you are asking me

82
00:06:28,020 --> 00:06:35,340
The most noble thing that I've seen and the most noble people that I've seen from my point of view as being noble

83
00:06:35,820 --> 00:06:37,820
you know from my

84
00:06:37,820 --> 00:06:43,500
Observation the people that I have seen and feel that I feel are the most noble and enlightened

85
00:06:43,820 --> 00:06:48,140
Turn out to be the people who have the least opinions will give their their

86
00:06:49,340 --> 00:06:53,900
Observations when they say well, this is kind of like Mahasi Sayad of for example

87
00:06:53,900 --> 00:06:58,060
There were at times where he would point out and say well, you know, this is kind of conflicting

88
00:06:58,060 --> 00:07:04,300
So we're really not sure here and it may be that there is a mistake here. They would say that but

89
00:07:04,300 --> 00:07:06,540
otherwise

90
00:07:06,540 --> 00:07:08,780
Almost always he would go according to the

91
00:07:09,100 --> 00:07:13,100
Topitika the commentaries of these two demaga all of this without interpretation

92
00:07:13,420 --> 00:07:17,340
Without trying to change it he would try his best to fit his meditation

93
00:07:17,980 --> 00:07:23,420
Into that you know as best he couldn't well still you know going according to the observation

94
00:07:23,420 --> 00:07:27,180
Then from my point of view from what I've seen that seems to work so

95
00:07:28,060 --> 00:07:30,620
It takes a certain amount of enlightenment

96
00:07:30,620 --> 00:07:37,420
I think to do that to give up your own views and opinions and to just go according to the standard

97
00:07:37,740 --> 00:07:42,140
Interpretation which you know from my point from what I've seen turns out to be

98
00:07:43,820 --> 00:07:45,820
You know perfect but

99
00:07:46,780 --> 00:07:52,060
Good enough shall to fit the perfect what would what might be the perfect meditation into it

100
00:07:53,020 --> 00:07:57,820
So there may be times where you have to kind of finesse it and kind of say well, you know

101
00:07:57,820 --> 00:08:04,220
Don't take that too seriously or that's just one interpretation which oftentimes especially the commentaries are

102
00:08:07,820 --> 00:08:09,820
But why I

103
00:08:10,460 --> 00:08:18,940
Go you know go to this this depth to explain is is because the danger of not having enlightenment is that you will misinterpret the teachings

104
00:08:20,220 --> 00:08:24,220
One two that you will miss you use the teachings so

105
00:08:24,220 --> 00:08:32,060
many many examples of this where a teacher becomes gets a guru complex and begins to

106
00:08:34,220 --> 00:08:36,220
Encourage this

107
00:08:37,100 --> 00:08:39,580
Attachment to their person and

108
00:08:41,420 --> 00:08:43,820
Taking them as refuge or something like that

109
00:08:46,540 --> 00:08:50,460
Even to the point of you know abusing it as far as

110
00:08:50,460 --> 00:08:54,060
Taking advantage of one students

111
00:08:55,180 --> 00:09:00,620
We take advantage people we these sorts of teachers who I I claim I'm not one

112
00:09:02,540 --> 00:09:07,500
Will not try to get money from their students for example a very common thing to do

113
00:09:08,060 --> 00:09:11,980
But less common, but that does still happen is they will

114
00:09:13,420 --> 00:09:17,580
And sometimes it seems you know they're just unable to control themselves, but they'll actually have

115
00:09:17,580 --> 00:09:23,740
Relations with their with their students and they'll start to have sexual or romantic relations with their students

116
00:09:24,220 --> 00:09:27,180
Still trying to be a meditation teacher and at the same time

117
00:09:28,460 --> 00:09:31,420
Filling around with their students because they just can't control themselves

118
00:09:32,220 --> 00:09:36,220
So these are the disadvantages best thing is to find in a light and being but

119
00:09:38,140 --> 00:09:42,620
Doesn't mean that we should denigrate a person who is not enlightened but is able to teach and

120
00:09:42,620 --> 00:09:46,540
More importantly doesn't it or it means that we should not

121
00:09:49,660 --> 00:09:55,660
We should not consider just because someone is a good teacher means that they're enlightened just because they talk like an enlightened being

122
00:09:56,700 --> 00:10:01,420
Because when I first started teaching it it was kind of embarrassing, but it's also you know

123
00:10:01,420 --> 00:10:03,420
It's kind of encouraging

124
00:10:03,420 --> 00:10:05,020
Encourage us

125
00:10:05,020 --> 00:10:09,580
As new teachers that I was able to sound like a jantong so when I my teacher

126
00:10:09,580 --> 00:10:15,740
So when I talked and when I taught in Thai, I would speak and and have the same nuances that he had

127
00:10:18,060 --> 00:10:25,820
So and it worked it did make me a good teacher fairly quickly. It doesn't make me a good person. It doesn't make me an enlightened person

128
00:10:27,500 --> 00:10:29,500
But

129
00:10:29,500 --> 00:10:31,500
You know it it really

130
00:10:31,500 --> 00:10:33,820
You could save and it fooled my students and you

131
00:10:34,620 --> 00:10:38,380
You could say that probably some of my students were fooled into thinking that I was more

132
00:10:38,380 --> 00:10:40,380
More

133
00:10:40,380 --> 00:10:43,660
More than I was thinking maybe he's in our hot there was actually one man who

134
00:10:44,780 --> 00:10:50,140
That's maybe a different story, but it was one man who thought I had told him that I was in our hot time

135
00:10:50,140 --> 00:10:52,140
He misunderstood what I had said

136
00:10:52,140 --> 00:10:54,140
I think it's a different story

137
00:10:54,220 --> 00:10:55,900
but

138
00:10:55,900 --> 00:10:57,900
You know at the very least I would say

139
00:10:59,020 --> 00:11:01,020
Try your best to get a

140
00:11:02,140 --> 00:11:03,820
An enlightened teacher

141
00:11:03,820 --> 00:11:05,820
But don't be too concerned about it

142
00:11:05,820 --> 00:11:09,100
Be concerned that they're going according to the

143
00:11:09,740 --> 00:11:14,300
Tradition both from the Buddha and the commentaries, you know people are going to

144
00:11:15,100 --> 00:11:21,180
Argue with that. There's a lot of people who will immediately be turned off by the idea of going with any sort of commentaries

145
00:11:21,580 --> 00:11:23,580
as they prefer to make their own commentaries

146
00:11:24,380 --> 00:11:25,980
um

147
00:11:26,220 --> 00:11:28,220
But you know that's my suggestion

148
00:11:29,660 --> 00:11:31,660
and

149
00:11:31,660 --> 00:11:37,980
Don't be and but on the other hand, once you find a good teacher, don't be too sure that they're going to always be in line with

150
00:11:38,540 --> 00:11:40,540
The suit does in the Buddha's teaching

151
00:11:40,540 --> 00:12:01,500
um because they may not be enlightened even though they might be able to teach

