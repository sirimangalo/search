1
00:00:00,000 --> 00:00:14,240
Good evening, everyone.

2
00:00:14,240 --> 00:00:26,080
Welcome to our evening dhamma.

3
00:00:26,080 --> 00:00:54,760
I wanted to take tonight to express this sort of appreciation for everyone, for all of our

4
00:00:54,760 --> 00:01:01,640
meditation community.

5
00:01:01,640 --> 00:01:10,520
For all of you people out there interested in Buddhism, who have taken up study and practice

6
00:01:10,520 --> 00:01:39,720
of this incredible, wonderful, marvelous, miraculous set of teachings and practices.

7
00:01:39,720 --> 00:01:53,600
And just to remark upon how special this moment is.

8
00:01:53,600 --> 00:02:05,880
For all of us out to remind us one and all, what it's taken for us to get here and how

9
00:02:05,880 --> 00:02:11,640
special is this moment.

10
00:02:11,640 --> 00:02:18,840
It's like one of those, this is like one of those grand events, like the inauguration

11
00:02:18,840 --> 00:02:45,120
of a great monument or some once in a lifetime experience or event.

12
00:02:45,120 --> 00:02:55,520
But it's said, don't let the moment pass you by.

13
00:02:55,520 --> 00:03:03,720
That's because this moment is special and it's a moment in time that there's a great significance

14
00:03:03,720 --> 00:03:05,360
to all of us.

15
00:03:05,360 --> 00:03:12,560
It's great meaning and great potential if we take advantage of it.

16
00:03:12,560 --> 00:03:21,720
If we take this opportunity, if we don't squander our time, if we don't allow ourselves

17
00:03:21,720 --> 00:03:32,440
to become distracted and diverted and caught up in the blinding power of defilements,

18
00:03:32,440 --> 00:03:40,520
we have great potential in this moment.

19
00:03:40,520 --> 00:03:51,680
And conventionally, before you even talk about the power of now, what it took for us

20
00:03:51,680 --> 00:03:58,280
to get here today is quite impressive.

21
00:03:58,280 --> 00:04:09,520
The first thing that's special about this moment is that it's a time of the Buddha.

22
00:04:09,520 --> 00:04:18,880
This is a time when we have the teachings of a fully enlightened Buddha, which for those

23
00:04:18,880 --> 00:04:24,000
of you new to the study of Buddhism, it may not seem all that significant, but for those

24
00:04:24,000 --> 00:04:32,040
of us who have studied deeply or practiced deeply, the teachings of the Buddha, it has

25
00:04:32,040 --> 00:04:37,680
profound significance.

26
00:04:37,680 --> 00:04:49,200
This isn't just some ordinary teaching, Nagama do mo nika masada mo, this is not the teaching

27
00:04:49,200 --> 00:05:03,680
of one village or city or one civilization, this is the dam of the whole world, this is

28
00:05:03,680 --> 00:05:13,560
the universal teachings of truth and they're not always here, they're not always available.

29
00:05:13,560 --> 00:05:21,920
In fact, the vast majority of time spent in some sara's in situations where there is

30
00:05:21,920 --> 00:05:37,680
no Buddha, there are world periods without the arising of a Buddha.

31
00:05:37,680 --> 00:05:43,600
As there are places where no one has ever heard of the Buddha, even when the Buddha was

32
00:05:43,600 --> 00:05:49,400
alive, if you were born at that time but you were born in Canada, if you weren't anywhere

33
00:05:49,400 --> 00:06:01,680
but in India chances are you'd never hear of the lone meat of the Buddha, but we're

34
00:06:01,680 --> 00:06:10,560
born in a time when even Canada has heard of the Buddha's teaching, it has spread and

35
00:06:10,560 --> 00:06:15,200
so in a sense we're lucky here as Canadians to be born today then to have been born

36
00:06:15,200 --> 00:06:21,600
2,500 years ago, 2,500 years ago in Canada wouldn't have been a good place to learn

37
00:06:21,600 --> 00:06:29,920
Buddhism, not that there weren't civilizations here, it's just they weren't Buddhist.

38
00:06:29,920 --> 00:06:41,280
It takes a Buddha, it's not something that comes up in a few days and it's not something

39
00:06:41,280 --> 00:06:47,400
you get every year, every year there's a new Buddha coming out there, every generation

40
00:06:47,400 --> 00:06:51,880
has its own Buddha, it's not like that.

41
00:06:51,880 --> 00:07:03,040
Our Buddha took four Asangaya, four uncountable eons it took him, plus another hundred thousand

42
00:07:03,040 --> 00:07:08,760
countable eons, great eons.

43
00:07:08,760 --> 00:07:14,720
In Asangaya is something you can't count, it's this measure that ironically you can count

44
00:07:14,720 --> 00:07:19,800
how many of them are, somehow you can count that there are four Asangaya, but you can't

45
00:07:19,800 --> 00:07:25,720
count how long in Asangaya is, so it must be marked by something at the end of an Asangaya

46
00:07:25,720 --> 00:07:33,000
something changes, I'm not up on all this, I don't quite know, but it's a huge, unfathomable

47
00:07:33,000 --> 00:07:39,720
amount of time.

48
00:07:39,720 --> 00:07:53,320
And we had to do all sorts of powerful acts and sacrifices giving up his eyes, giving up

49
00:07:53,320 --> 00:08:02,920
his life, renouncing the world, renouncing his family, being killed and tortured.

50
00:08:02,920 --> 00:08:16,680
Walking through fire to become a Buddha.

51
00:08:16,680 --> 00:08:22,760
And so here we have in the time of a very special individual who's teachings have come

52
00:08:22,760 --> 00:08:37,360
through the veil of darkness that has kept us kind of fused and unseen, the very rare

53
00:08:37,360 --> 00:08:38,360
opportunity.

54
00:08:38,360 --> 00:08:50,400
That's the first aspect, the first reason why this is a special moment.

55
00:08:50,400 --> 00:08:54,120
The second reason is that well, not only are we born in the time of the Buddha, but we're

56
00:08:54,120 --> 00:09:04,560
also born as humans, so there are lots of beings around that will never understand the

57
00:09:04,560 --> 00:09:05,560
Buddhist teachings.

58
00:09:05,560 --> 00:09:17,840
We could have been born quite easily as a deer or cat, a dog, a mosquito, a worm.

59
00:09:17,840 --> 00:09:21,680
We could have been born as many different things.

60
00:09:21,680 --> 00:09:25,800
We could have been born as a brahma and never met with a Buddha because we were lost

61
00:09:25,800 --> 00:09:34,400
in the brahma realms or never had the opportunity to become enlightened.

62
00:09:34,400 --> 00:09:41,040
Being born as a human or as an angel is a very rare thing to just rise up out of the

63
00:09:41,040 --> 00:09:44,400
ordinary animal realm is a very rare thing.

64
00:09:44,400 --> 00:09:50,440
And now we think it's quite common because there are seven or eight billion of us, and

65
00:09:50,440 --> 00:09:55,240
we could argue that that's due to the goodness of teachings like the Buddha that's allowed

66
00:09:55,240 --> 00:10:02,640
us to prosper, but even seven or eight billion isn't all that much.

67
00:10:02,640 --> 00:10:13,480
You can compare it to the number of animals, the number of ants and mosquitoes and

68
00:10:13,480 --> 00:10:22,760
the number of other beings, the beings in hell that are probably countless, the ability

69
00:10:22,760 --> 00:10:26,520
to be born as a human beings very rare.

70
00:10:26,520 --> 00:10:28,320
We've achieved it.

71
00:10:28,320 --> 00:10:35,360
We've made it out of the depths of despair and suffering of the animal realms, the lower

72
00:10:35,360 --> 00:10:39,400
realm.

73
00:10:39,400 --> 00:10:55,600
Third, we are in a position to practice the Buddha's teaching, so many people who are caught

74
00:10:55,600 --> 00:10:56,600
up.

75
00:10:56,600 --> 00:11:01,680
There's a lot of people who even want to practice Buddhism, but don't have the opportunity.

76
00:11:01,680 --> 00:11:08,920
They're caught up in their lives, perhaps they're sick or in debt, maybe they're

77
00:11:08,920 --> 00:11:16,440
married and have children and are in a position with young children where they can't

78
00:11:16,440 --> 00:11:28,120
come to practice meditation, maybe they have a job that doesn't allow them to get away.

79
00:11:28,120 --> 00:11:34,840
The life of human beings is a difficult life.

80
00:11:34,840 --> 00:11:40,440
If people have talked to people who are in a place where there is no Buddhism and there's

81
00:11:40,440 --> 00:11:47,200
no chance to go to a Buddhist monastery or to learn meditation, it's great now that we have

82
00:11:47,200 --> 00:11:57,160
these online courses, we have people coming and doing at least a non-intensive practice.

83
00:11:57,160 --> 00:12:03,280
Enough to give them courage and give them hope and tide them over while they cultivate

84
00:12:03,280 --> 00:12:06,080
the necessary conditions to actually come and do a course.

85
00:12:06,080 --> 00:12:10,080
We've had several people who have done that and eventually found a way to come and

86
00:12:10,080 --> 00:12:20,160
finish the course here.

87
00:12:20,160 --> 00:12:24,760
The third reason why this is special is we've all come together, most special for our

88
00:12:24,760 --> 00:12:32,040
resident meditators who have done this incredible thing to take time out of an ordinarily

89
00:12:32,040 --> 00:12:47,600
busy life and dedicate 20, 30 days straight, just training the mind, the pure unadulterated

90
00:12:47,600 --> 00:13:04,160
goodness.

91
00:13:04,160 --> 00:13:09,160
The third reason is that we've got the opportunity and the fourth reason is that we've

92
00:13:09,160 --> 00:13:12,560
actually taken the opportunity.

93
00:13:12,560 --> 00:13:16,680
This is what it means by not letting the moment pass you by.

94
00:13:16,680 --> 00:13:24,800
To say there are many people, there are a few people in this world who even think to do

95
00:13:24,800 --> 00:13:30,920
things like meditation, who are moved by things that they should be moved by.

96
00:13:30,920 --> 00:13:35,000
Most people aren't even moved when they think of death, when they think of sickness, they

97
00:13:35,000 --> 00:13:41,280
can't understand why we don't just engage in indulgent, in sensual pleasures, they can't

98
00:13:41,280 --> 00:13:45,560
understand why one would come here and do a meditation course would take time out of their

99
00:13:45,560 --> 00:13:51,640
lives or try to change their lives, strive to free themselves from craving, when there's

100
00:13:51,640 --> 00:13:56,680
so much pleasure to be had out there and they hear about things like old age sickness,

101
00:13:56,680 --> 00:14:03,400
death, they look at the anger and the greed and delusion that they're building inside

102
00:14:03,400 --> 00:14:08,560
and they aren't threatened by it, they don't feel disturbed by it, there are people

103
00:14:08,560 --> 00:14:17,240
like that, if they feel disturbed, they ignore it, they don't allow them to be themselves

104
00:14:17,240 --> 00:14:26,680
to become moved, very few are the people who actually discern this precarious state for

105
00:14:26,680 --> 00:14:31,680
what it is that at any moment things could change and we could be dumped, had long into

106
00:14:31,680 --> 00:14:39,440
the suffering for a variety of reasons and in fact will be as life changes, very few

107
00:14:39,440 --> 00:14:45,440
people actually see the suffering for what it is, see how much stress is involved in chasing

108
00:14:45,440 --> 00:14:54,920
after the objects of our desires, very few people see this, but among those people who

109
00:14:54,920 --> 00:15:04,000
see these problematic aspects of ordinary life, very few are those who actually do something

110
00:15:04,000 --> 00:15:14,120
about it, so it's worth feeling good about taking time to reflect how far we've come

111
00:15:14,120 --> 00:15:20,480
and be encouraged by how special this is and how powerful it is, so the real point

112
00:15:20,480 --> 00:15:30,480
is that we've come to a position where we can take advantage of all these supportive

113
00:15:30,480 --> 00:15:46,000
good luck that we've had, all these good conditions, favorable conditions and make this

114
00:15:46,000 --> 00:15:57,800
moment into something special and all these supportive conditions come together to allow

115
00:15:57,800 --> 00:16:03,920
us, all of them are required just for this one simple act of seeing things clearly,

116
00:16:03,920 --> 00:16:12,800
robustness, all these things that are there for everyone else, but that they never see

117
00:16:12,800 --> 00:16:18,240
right under our noses, right here in front of us, but that we never see clearly, they're

118
00:16:18,240 --> 00:16:24,440
all there, but we can't see them, we don't have the supportive conditions, well now we've

119
00:16:24,440 --> 00:16:32,840
come together and we have the supportive condition, we have the opportunity to see body

120
00:16:32,840 --> 00:16:45,600
as body, feelings as feelings, thoughts as thoughts, emotions as emotions, senses as senses,

121
00:16:45,600 --> 00:16:50,760
to see impermanence, to see suffering, to see nonself, to free ourselves from craving and clinging

122
00:16:50,760 --> 00:16:59,080
to things that can't possibly satisfy us, to strive for liberation and a state of mind

123
00:16:59,080 --> 00:17:11,640
that is unsullied by the ocean or the defilement of samsara, so it's quite a special opportunity

124
00:17:11,640 --> 00:17:20,560
that we have is what I'm trying to say, so I like to take this tonight to do a sort

125
00:17:20,560 --> 00:17:25,680
of appreciation and to offer this as a sort of dhamma, that the most important thing is

126
00:17:25,680 --> 00:17:32,480
the moment, to let the moment pass you by, there's a lot that's gone into this moment,

127
00:17:32,480 --> 00:17:46,600
this moment in time, so care for it, care for it, I think in the Missudhi Maga it talks

128
00:17:46,600 --> 00:17:53,280
about a person who is rocking a baby and then the olden times they would have a baby

129
00:17:53,280 --> 00:17:58,480
cradle on a string, maybe even nowadays they do this as well, so you have to pull the

130
00:17:58,480 --> 00:18:07,400
string and you have to be very careful to keep the string going, or the baby will wake up,

131
00:18:07,400 --> 00:18:17,360
our practice has to be cared for, carefully tended to, like a sleeping baby to keep it

132
00:18:17,360 --> 00:18:28,000
on kilter, to keep it online and in line and progressing smoothly, to constantly be cautious

133
00:18:28,000 --> 00:18:38,080
and careful, like a sick person looking after their pain in their body, not that we have

134
00:18:38,080 --> 00:18:43,320
to feel sick or it should be sickening or anything, but we should think of ourselves

135
00:18:43,320 --> 00:18:47,840
like someone who has to take care of something, because the potential for suffering, so

136
00:18:47,840 --> 00:18:52,000
a sick person, if they're not careful with how they move their body, they'll feel

137
00:18:52,000 --> 00:19:00,960
great pain, likewise a meditator in moving their body and speaking in everything they

138
00:19:00,960 --> 00:19:06,440
do and even in their thinking, they have to be careful to not go off track or they'll

139
00:19:06,440 --> 00:19:17,400
lose, they'll lose their equilibrium, they'll lose their direction, treat this moment

140
00:19:17,400 --> 00:19:27,200
with care, it's very valuable, priceless, so there you go, there's the bit of dhamma for

141
00:19:27,200 --> 00:19:42,960
tonight, thank you all for coming out, I'm happy to take questions if there are any.

142
00:19:42,960 --> 00:20:00,880
I hope you enjoyed this video, thanks.

143
00:20:30,880 --> 00:20:49,440
The reason one cannot be enlightened as a Brahma in the Brahma realms, that because

144
00:20:49,440 --> 00:20:54,680
there one cannot take the force at Deepa Tana, where are you planning on going to the Brahma

145
00:20:54,680 --> 00:20:55,680
realms?

146
00:20:55,680 --> 00:21:05,040
As I understand the s some of the Brahma realms, one cannot become enlightened, I'm not quite

147
00:21:05,040 --> 00:21:10,200
clear why, but I think Mahasisaya does says it's something about how they aren't able to see

148
00:21:10,200 --> 00:21:15,240
impermanence, because it's too stable.

149
00:21:15,240 --> 00:21:29,400
Now you have to note, curious, curious, wondering, wondering, that wasn't the point of my

150
00:21:29,400 --> 00:21:49,840
talk, such a terrible teacher, no, one answer your questions, I did kind of.

151
00:21:49,840 --> 00:22:19,800
Yeah, right, it might just be the Arupa Brahma realms that you can't practice.

152
00:22:19,800 --> 00:22:33,960
Because there are certainly Brahma realms where you can practice the Anagami realms.

153
00:22:33,960 --> 00:22:41,520
The Arupa Brahma realms are like real God states, where you're totally out of it, totally

154
00:22:41,520 --> 00:22:55,440
entranced with no form, only mind, I think.

155
00:22:55,440 --> 00:23:20,560
Alright, if there are no more questions, we'll call it a night, thank you all for coming

156
00:23:20,560 --> 00:23:25,400
up, wishing you all great practice, and great results.

