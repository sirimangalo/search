1
00:00:00,000 --> 00:00:17,440
So everyone broadcasting live January 7, 2016 with me tonight is Robin, we're broadcasting

2
00:00:17,440 --> 00:00:18,440
live.

3
00:00:18,440 --> 00:00:23,880
So if you have questions, you come on over to meditation.ceremungalow.org and you can ask

4
00:00:23,880 --> 00:00:24,880
them there.

5
00:00:24,880 --> 00:00:28,680
No, we're not answering questions on YouTube.

6
00:00:28,680 --> 00:00:38,840
Today we had a meeting of the McMaster Buddhism Association executive.

7
00:00:38,840 --> 00:00:55,960
So we are, our plan is to, we're going to have meetings weekly and there's the second

8
00:00:55,960 --> 00:01:11,560
I have to turn this stuff off, I forgot.

9
00:01:11,560 --> 00:01:16,680
It's great.

10
00:01:16,680 --> 00:01:24,560
We're good.

11
00:01:24,560 --> 00:01:33,400
So we're going to have meetings to just to get to know people, to bring people together.

12
00:01:33,400 --> 00:01:39,440
Maybe we'll give some teaching on Buddhism during the meeting.

13
00:01:39,440 --> 00:01:46,280
And we talked about these five minute meditation lessons in the table, have to put the

14
00:01:46,280 --> 00:01:55,480
banner of Robin, I'm sorry, but something that, that order went all wrong because I've

15
00:01:55,480 --> 00:01:59,640
got rope and I've got a banner and there's no way to connect the two together.

16
00:01:59,640 --> 00:02:02,160
There's no grommets or anything.

17
00:02:02,160 --> 00:02:06,200
Like when I did it, when I did a mock order and went through it, it asked me if I wanted

18
00:02:06,200 --> 00:02:09,360
grommets or sticky things.

19
00:02:09,360 --> 00:02:11,440
You didn't get that.

20
00:02:11,440 --> 00:02:13,480
So did you pick the sticky things then?

21
00:02:13,480 --> 00:02:16,480
I mean, I didn't order it.

22
00:02:16,480 --> 00:02:17,480
Okay.

23
00:02:17,480 --> 00:02:20,920
I mean, I just, I didn't design the order.

24
00:02:20,920 --> 00:02:22,960
I just submitted it.

25
00:02:22,960 --> 00:02:23,960
No.

26
00:02:23,960 --> 00:02:28,200
When you submitted it, didn't ask if you wanted grommets?

27
00:02:28,200 --> 00:02:32,680
No, it just asked if we wanted rope or zip ties.

28
00:02:32,680 --> 00:02:42,720
So maybe I missed something.

29
00:02:42,720 --> 00:02:46,720
Can you just cut little holes in it or would it rip?

30
00:02:46,720 --> 00:02:47,720
Is it kind of thin?

31
00:02:47,720 --> 00:02:50,040
No, it doesn't really work that way.

32
00:02:50,040 --> 00:02:59,320
The best thing is to punch holes and make grommets and get grommets.

33
00:02:59,320 --> 00:03:01,320
Maybe we should do that.

34
00:03:01,320 --> 00:03:09,520
Probably what I was going to do is just use duct tape to pass them it to the poles.

35
00:03:09,520 --> 00:03:24,760
We can check on the website and see if it can be sent back and fixed to have the grommets

36
00:03:24,760 --> 00:03:28,360
put into it.

37
00:03:28,360 --> 00:03:46,960
Before the sticky thing, see that mine for next Tuesday?

38
00:03:46,960 --> 00:03:48,960
No.

39
00:03:48,960 --> 00:03:53,960
Maybe we can go to the hardware store and get some grommets.

40
00:03:53,960 --> 00:04:07,440
Is it going to be impossible or like the Velcro sticky things on the back?

41
00:04:07,440 --> 00:04:15,960
But you don't have Velcro on the walls at the university, so.

42
00:04:15,960 --> 00:04:25,960
It's going to be in front of a window, actually.

43
00:04:25,960 --> 00:04:34,560
Maybe a big dowel, a long dowel.

44
00:04:34,560 --> 00:04:42,480
If you had a dowel, I don't know how long the banner is, but if you had a dowel, you could

45
00:04:42,480 --> 00:04:43,480
sew it onto it.

46
00:04:43,480 --> 00:04:48,480
And it would be like a stick, you put a string on it, and you could hang it.

47
00:04:48,480 --> 00:04:50,480
Sew it.

48
00:04:50,480 --> 00:04:57,480
There you go.

49
00:04:57,480 --> 00:05:01,480
Yeah, you've got to use duct tape.

50
00:05:01,480 --> 00:05:15,680
That tape sounds good.

51
00:05:15,680 --> 00:05:18,680
We have questions.

52
00:05:18,680 --> 00:05:23,760
This question about music, your opinion about music in relation to Buddhism, but one of

53
00:05:23,760 --> 00:05:29,200
the other meditators pointed the questioner to the video wiki.

54
00:05:29,200 --> 00:05:33,200
So did you want to just leave it at that?

55
00:05:33,200 --> 00:05:34,200
I don't know.

56
00:05:34,200 --> 00:05:35,200
Done videos on music?

57
00:05:35,200 --> 00:05:36,200
Yes.

58
00:05:36,200 --> 00:05:37,200
Okay.

59
00:05:37,200 --> 00:05:42,360
We must leave it at that.

60
00:05:42,360 --> 00:05:48,400
Is it possible that someone in meditation who has eliminated all desire could be so absolutely

61
00:05:48,400 --> 00:05:53,240
immersed in mindfulness of what things are that he dies dehydrated for not drinking

62
00:05:53,240 --> 00:05:54,240
water?

63
00:05:54,240 --> 00:05:58,240
Do you know of any case in which this has happened?

64
00:05:58,240 --> 00:06:04,240
Is this a person who is meditating with it?

65
00:06:04,240 --> 00:06:05,240
Yes.

66
00:06:05,240 --> 00:06:09,240
Quite a bit.

67
00:06:09,240 --> 00:06:16,240
I mean, it sounds like a bit of a misunderstanding of what mindfulness is, but maybe not.

68
00:06:16,240 --> 00:06:17,240
Okay.

69
00:06:17,240 --> 00:06:26,240
It's not really mindfulness that you're talking about, it's mindfulness can involve all of

70
00:06:26,240 --> 00:06:44,240
this awareness is more like focus, which can come from mindfulness practice.

71
00:06:44,240 --> 00:06:52,240
I don't think so.

72
00:06:52,240 --> 00:06:56,240
My influence brings you into the present moment, I mean, just off the cuff.

73
00:06:56,240 --> 00:07:01,240
I think technically I get what you're saying, but is it practically speaking?

74
00:07:01,240 --> 00:07:03,240
It's not the way it goes.

75
00:07:03,240 --> 00:07:12,240
I don't have the technical aspect of why it is so, but the meditators are more aware of

76
00:07:12,240 --> 00:07:17,240
the wants in music and body.

77
00:07:17,240 --> 00:07:25,240
It might consciously choose to ignore them, but it wouldn't suddenly say, oh, I'm dehydrated

78
00:07:25,240 --> 00:07:30,240
and I didn't know it because I was just experiencing it as sensations.

79
00:07:30,240 --> 00:07:42,240
That would require intense concentration that I don't think is really a part of the insight.

80
00:07:42,240 --> 00:07:48,240
So mindfulness really helps you, you're super conscious of the fact that there's the dehydration

81
00:07:48,240 --> 00:07:54,240
going on, all the aspects of knowing that it's being that it's being dehydrated are all still likely

82
00:07:54,240 --> 00:08:02,240
to be there, but it's just a knowledge of the concepts in mind.

83
00:08:02,240 --> 00:08:08,240
And those concepts could be absent, but I don't think they're more likely to be absent just because

84
00:08:08,240 --> 00:08:15,240
you know, I don't think it goes to that extent in most cases.

85
00:08:15,240 --> 00:08:19,240
I don't think I can bring it as possible.

86
00:08:19,240 --> 00:08:29,240
The other hand, why the heck are you asking such a question?

87
00:08:29,240 --> 00:08:33,240
I have been wanting to go down the path of the Buddhist among three years now.

88
00:08:33,240 --> 00:08:38,240
I have even planned to go to Nepal to travel and learn the best I could before becoming

89
00:08:38,240 --> 00:08:39,240
a monk.

90
00:08:39,240 --> 00:08:44,240
There is one thing troubling me though, is a relationship with another person and having children.

91
00:08:44,240 --> 00:08:51,240
I'm not with anybody, I'm just scared of getting it to a relationship and then later in life

92
00:08:51,240 --> 00:08:57,240
hurting that person because I would like to leave and do as I wished before I'd become a monk.

93
00:08:57,240 --> 00:08:59,240
I don't know if I'm overthinking this or not.

94
00:08:59,240 --> 00:09:06,240
This has been on my mind and I'm having troubles except accepting it in meditation.

95
00:09:06,240 --> 00:09:18,240
Quite understand, but I think it's like, you know, afraid of getting into a relationship because it might become a monk.

96
00:09:18,240 --> 00:09:29,240
It's not really, I don't know what I have to say to you.

97
00:09:29,240 --> 00:09:36,240
Don't get into a relationship.

98
00:09:36,240 --> 00:09:40,240
What can I do for you?

99
00:09:40,240 --> 00:09:44,240
It can mean like having trouble because you want to get into a relationship.

100
00:09:44,240 --> 00:09:50,240
You also want to become a monk, but that probably you won't become a monk.

101
00:09:50,240 --> 00:09:52,240
It's easy to want to become a monk.

102
00:09:52,240 --> 00:09:58,240
It's easy to actually do that.

103
00:09:58,240 --> 00:10:11,240
This is a question I actually asked to me from Ryan.

104
00:10:11,240 --> 00:10:17,240
I looked on Expedia today and round trip tickets to Toronto to Bangkok started around $1,000.

105
00:10:17,240 --> 00:10:23,240
So with that being said, what do we need to do to get the ball rolling to buy Bonte ticket to see Ajahn Tang?

106
00:10:23,240 --> 00:10:26,240
Is it just as simple as setting up another you-caring campaign?

107
00:10:26,240 --> 00:10:30,240
Let me know whatever I can do to help.

108
00:10:30,240 --> 00:10:43,240
It is and it isn't simple, only in the timing of it.

109
00:10:43,240 --> 00:10:52,240
We did a fundraising drive which provided funds for about nine months of operation at the monastery, which was wonderful.

110
00:10:52,240 --> 00:10:56,240
But it's not enough to keep the monastery going perpetually.

111
00:10:56,240 --> 00:11:08,240
So we actually had planned for February to do another fundraising campaign to judge continued support of the monastery and meditation center to see if it can keep going perpetually.

112
00:11:08,240 --> 00:11:12,240
And that's really crucial because it was wonderful, the initial support.

113
00:11:12,240 --> 00:11:16,240
But what's crucial is whether there's enough support to keep it going.

114
00:11:16,240 --> 00:11:24,240
So that's what the organization was gearing up to do to do another fundraising drive for the monastery and meditation center.

115
00:11:24,240 --> 00:11:31,240
So, you know, while the trip sounds wonderful, just the timing of it is a little bit premature.

116
00:11:31,240 --> 00:11:51,240
After we do the second fundraising campaign and see if there is continued support to keep the monastery and meditation center going, then that would be a great time to talk about extra things like a trip to Asia, which would obviously be a wonderful thing for Bonte to go see his teacher and receive the blessing and everything.

117
00:11:51,240 --> 00:11:56,240
It's just a little bit premature at this point, I think.

118
00:11:56,240 --> 00:11:59,240
But, you know, it's certainly not up to me.

119
00:11:59,240 --> 00:12:08,240
It's a joint decision with the directors of Sermon Glow International, which includes Bonte, so you can certainly chime in on that too.

120
00:12:08,240 --> 00:12:12,240
Yeah, I don't want to talk about those things.

121
00:12:12,240 --> 00:12:14,240
Thank you.

122
00:12:14,240 --> 00:12:17,240
All things in good time.

123
00:12:17,240 --> 00:12:26,240
We just need maybe just a little bit of patience right now to see, you know, what sort of support is available as continued support for the monastery and meditation center.

124
00:12:26,240 --> 00:12:28,240
We'll be rolling that out in a couple of weeks.

125
00:12:28,240 --> 00:12:37,240
We just, we had planned it as a fall campaign and a spring campaign, the spring campaign beginning in February and a couple of weeks.

126
00:12:37,240 --> 00:12:52,240
I guess I'd just say that it's hard to rationalize it by flinging yourself across the from one end of the earth to the other.

127
00:12:52,240 --> 00:12:58,240
And the purpose isn't, you know, it's not like my teacher doesn't know about this monastery doesn't know what I'm doing.

128
00:12:58,240 --> 00:13:01,240
I'm sure he'd like me to come.

129
00:13:01,240 --> 00:13:08,240
I would like to go, but doesn't really seem worth the.

130
00:13:08,240 --> 00:13:13,240
If you're called worth the trouble especially when we have.

131
00:13:13,240 --> 00:13:19,240
Other troubles here or challenges that we're facing.

132
00:13:19,240 --> 00:13:26,240
We actually don't know if we have troubles or challenges that are, you know, your supporters, your followers are very generous.

133
00:13:26,240 --> 00:13:31,240
We simply haven't, we simply haven't asked what the setup for our fundraising campaign.

134
00:13:31,240 --> 00:13:33,240
It was a limited online campaign.

135
00:13:33,240 --> 00:13:42,240
There's always a support page at the website surmunglo.org, but that doesn't tend to be something that people think of very often.

136
00:13:42,240 --> 00:13:50,240
So while there's always an opportunity to give support, not many people actually go to the support page at the surmunglo.org website.

137
00:13:50,240 --> 00:13:56,240
So based on that, we're just a little concerned because there hasn't been much support in the past couple of months.

138
00:13:56,240 --> 00:14:01,240
But we didn't have an active fundraising campaign that was kind of out there and being promoted to ask people.

139
00:14:01,240 --> 00:14:03,240
So we don't know that there's a problem.

140
00:14:03,240 --> 00:14:04,240
There could be no problem.

141
00:14:04,240 --> 00:14:09,240
You know, a month from now, the monastery could be funded for another six months and all as well.

142
00:14:09,240 --> 00:14:15,240
And I'm sure the organization would really like to send you to Asia to see our John Tom.

143
00:14:15,240 --> 00:14:20,240
It's just a little bit, you know, timing.

144
00:14:20,240 --> 00:14:23,240
Yeah, I mean, I wasn't expecting to go.

145
00:14:23,240 --> 00:14:30,240
I'm sad that I wasn't planning to go to Asia in the near future.

146
00:14:30,240 --> 00:14:32,240
This kind of came up.

147
00:14:32,240 --> 00:14:38,240
Also, there's might be half art or partly paid.

148
00:14:38,240 --> 00:14:44,240
That would change things quite a bit because that would make it, you know,

149
00:14:44,240 --> 00:14:50,240
a very affordable way to go over and see your teacher, which would be wonderful.

150
00:14:50,240 --> 00:14:51,240
Right.

151
00:14:51,240 --> 00:14:53,240
So that's a good reason.

152
00:14:53,240 --> 00:14:56,240
You know, if there's opportunity where half of it's going to be paid for, well,

153
00:14:56,240 --> 00:14:59,240
better than waiting until we have to pay for all.

154
00:14:59,240 --> 00:15:00,240
Yeah.

155
00:15:00,240 --> 00:15:01,240
Yeah.

156
00:15:01,240 --> 00:15:02,240
Okay.

157
00:15:02,240 --> 00:15:07,240
Anyway, I'm going to talk about money stuff too much.

158
00:15:07,240 --> 00:15:14,240
It's a thing where that's what our focus is about.

159
00:15:14,240 --> 00:15:15,240
It looks like that's it.

160
00:15:15,240 --> 00:15:16,240
Right.

161
00:15:16,240 --> 00:15:17,240
Yes.

162
00:15:17,240 --> 00:15:19,240
Because we didn't know about it tonight.

163
00:15:19,240 --> 00:15:23,240
So that's going to go up.

164
00:15:23,240 --> 00:15:30,240
Otherwise, I think the Saturday I'll try to do the Buddhism 101 videos.

165
00:15:30,240 --> 00:15:35,240
So I have to look and see where I left off many years ago.

166
00:15:35,240 --> 00:15:38,240
Oh, I'm going to try and get through.

167
00:15:38,240 --> 00:15:43,240
I kind of left off because I was having trouble figuring out how to address it

168
00:15:43,240 --> 00:15:45,240
from a modern perspective.

169
00:15:45,240 --> 00:15:48,240
I think it's the next one is to talk about heaven.

170
00:15:48,240 --> 00:15:49,240
What?

171
00:15:49,240 --> 00:15:54,240
You have to study it a little bit and try and get a grasp of exactly what concept

172
00:15:54,240 --> 00:16:03,240
of it is trying to pass on and how to translate from modern years such that.

173
00:16:03,240 --> 00:16:05,240
Now, we just want to talk about it so much about heaven.

174
00:16:05,240 --> 00:16:07,240
So how far do we want to take it?

175
00:16:07,240 --> 00:16:10,240
Because we actually need heaven as part of Buddhism?

176
00:16:10,240 --> 00:16:11,240
I think so.

177
00:16:11,240 --> 00:16:17,240
So I want to bring in the bigger context.

178
00:16:17,240 --> 00:16:20,240
And figure context.

179
00:16:20,240 --> 00:16:25,240
I think karma, I think the next one, is going to be the current question,

180
00:16:25,240 --> 00:16:28,240
but the results of our good deeds.

181
00:16:28,240 --> 00:16:39,240
Okay, thank everyone, thanks for having me for your help.

182
00:16:39,240 --> 00:16:41,240
Thank you, Dante.

183
00:16:41,240 --> 00:16:44,240
Good night.

