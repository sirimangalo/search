1
00:00:00,000 --> 00:00:03,440
Hello, welcome back to Askamunk.

2
00:00:03,440 --> 00:00:07,840
Now I have two questions that we're asked together.

3
00:00:07,840 --> 00:00:18,040
The first question is whether after realizing Nirvana, nibana, which means freedom or

4
00:00:18,040 --> 00:00:25,120
release or emancipation or however you want to translate it, is there anything left to

5
00:00:25,120 --> 00:00:31,280
do or is nibana it? I mean, is that all you have to do? That's the first

6
00:00:31,280 --> 00:00:40,360
question. So, I answer this question first. At the answer it's easy, nibana

7
00:00:40,360 --> 00:00:46,880
nirvana is the last thing that we ever have to do. Once you realize nirvana

8
00:00:46,880 --> 00:00:50,560
there's nothing left, there's nothing left that from a Buddhist point of view

9
00:00:50,560 --> 00:00:58,240
you need to do. The point is at that point you're free from suffering. So, what

10
00:00:58,240 --> 00:01:04,840
does this mean? Nirvana is the cessation of suffering. This is what the

11
00:01:04,840 --> 00:01:12,000
definition of it is. It's possible for a person to not not easy but it's

12
00:01:12,000 --> 00:01:18,080
possible for a human being to realize freedom from suffering, to come to be

13
00:01:18,080 --> 00:01:26,840
even temporarily, to be free from the arising of stress, the arising of any

14
00:01:26,840 --> 00:01:34,520
impermanent phenomenon that at that point the mind is perfectly free. It's

15
00:01:34,520 --> 00:01:41,080
possible to enter or to touch that state, to experience or to see or to

16
00:01:41,080 --> 00:01:50,840
realize that state, without being free from suffering, without being that

17
00:01:50,840 --> 00:01:55,160
way forever. This is the first thing to point out. This person we have names

18
00:01:55,160 --> 00:01:59,320
for them in the sota panda, someone who has entered the stream, because though

19
00:01:59,320 --> 00:02:08,060
they still have to suffer and they still have more to do, they've already

20
00:02:08,060 --> 00:02:13,400
cracked the shell, so to speak, and it's only a matter of time before they

21
00:02:13,400 --> 00:02:18,400
become totally free from suffering. So, this is one understanding of Nirvana.

22
00:02:18,400 --> 00:02:24,240
It's an experience that someone has and at that point one's life starts to go

23
00:02:24,240 --> 00:02:30,200
in the direction towards freedom from suffering. But what we mean by Nirvana or

24
00:02:30,200 --> 00:02:37,320
Parini Vanna or so on is the complete freedom from suffering, so though a person might

25
00:02:37,320 --> 00:02:43,360
still live, they have no more greed, no more anger, no more delusion. These

26
00:02:43,360 --> 00:02:49,560
things can't arise. There's perfect understanding of reality of the experiences

27
00:02:49,560 --> 00:02:55,160
around oneself. When you see and hear smell, taste, feel, think there's only the

28
00:02:55,160 --> 00:03:00,840
awareness of the object. There's no attachment to it. So we say this is Nirvana. This comes

29
00:03:00,840 --> 00:03:05,080
from realizing this state of ultimate freedom. When you realize it again and

30
00:03:05,080 --> 00:03:09,040
again, you start to see that there's nothing that lasts, that all of the

31
00:03:09,040 --> 00:03:14,800
experiences that we have simply arise and see. There's nothing lasting,

32
00:03:14,800 --> 00:03:18,240
there's nothing permanent, there's nothing stable. And so one loses all of

33
00:03:18,240 --> 00:03:26,160
one's attachment. At that point one isn't subject to future rebirth. There's this

34
00:03:26,160 --> 00:03:33,840
physical reality that we somehow built up that has to last out its lifetime.

35
00:03:33,840 --> 00:03:41,640
But at the end of that cycle, that revolution, at death there's nothing. So at death

36
00:03:41,640 --> 00:03:48,640
there's no more arising. And at that point there's freedom from suffering,

37
00:03:48,640 --> 00:03:57,720
from impermanence, from birth, from having to come back again. So this, I think

38
00:03:57,720 --> 00:04:01,120
this is clear that the answer from a Buddhist point, of a doctrinal point of view,

39
00:04:01,120 --> 00:04:05,520
is quite clear that after that there's nothing left to do. The Buddhist had

40
00:04:05,520 --> 00:04:12,520
katankaraniyam, done as what needs to be done. There's nothing left to do. There's nothing more here.

41
00:04:12,520 --> 00:04:20,640
But I think an important point to make that is often missed is that you don't

42
00:04:20,640 --> 00:04:27,600
somehow fall into this state. This is the ultimate, the sambonam, the final

43
00:04:27,600 --> 00:04:33,280
emancipation. It comes to someone who has had enough, who doesn't see any benefit

44
00:04:33,280 --> 00:04:38,160
in coming back. It doesn't see any benefit in recreating this human state or

45
00:04:38,160 --> 00:04:43,840
any state again and again and again. Most of us are not there. Most of us think

46
00:04:43,840 --> 00:04:47,520
that there's still something to be done. We want this, we want that, we've been

47
00:04:47,520 --> 00:04:52,800
taught that it's good to have a partner, to have a family, to have a job, to

48
00:04:52,800 --> 00:04:58,080
help people and so on. And we think that there's somehow some benefit in this.

49
00:04:58,080 --> 00:05:03,200
On the other hand, we see that in ourselves there are certain attachments that we have.

50
00:05:03,200 --> 00:05:07,600
That are causing us suffering. So this is where we're at. We're at the point where

51
00:05:07,600 --> 00:05:12,480
we want to hold on to certain things, but we want to let go of other certain things.

52
00:05:12,480 --> 00:05:17,920
So this just shows that we're somewhere along the path. We're somewhere between

53
00:05:20,560 --> 00:05:26,320
a useless person and an enlightened person. It's up to us which way we're going to go.

54
00:05:26,960 --> 00:05:32,480
If we take the side of letting go, we don't have to let go of those things we don't want to let go

55
00:05:32,480 --> 00:05:36,640
of. We let go of those things that we want to let go of. And this is how the path works.

56
00:05:37,200 --> 00:05:43,040
People who are afraid of things like Nirvana, afraid of letting go and afraid of practicing

57
00:05:43,040 --> 00:05:47,200
meditation because I think if I practice meditation, I'm going to let go of all these things

58
00:05:47,200 --> 00:05:51,520
I love and I love them and I want those things. I don't want to let go of them.

59
00:05:52,160 --> 00:05:55,760
And the truth is if you want it, if it's something you're holding on to, you're never

60
00:05:55,760 --> 00:06:02,400
going to let it go. That's the point. But the what you have to realize is that the more you

61
00:06:02,400 --> 00:06:08,320
understand, the more mature you become, the more you realize that you're not benefiting anyone

62
00:06:08,320 --> 00:06:13,920
or anything by yourself or others, by holding on to anything, by clinging to others,

63
00:06:13,920 --> 00:06:21,920
by wanting things to be a certain way. It's not of a benefit to you and it's not of a benefit

64
00:06:21,920 --> 00:06:27,040
to other people. That true happiness doesn't come from these things. You know, people when they

65
00:06:27,040 --> 00:06:30,880
hear about not coming back, they think, well, the great thing about Buddhism is that there is

66
00:06:30,880 --> 00:06:38,000
coming back. I remember talking with some, with a Catholic woman and she said, oh, it's so great

67
00:06:38,000 --> 00:06:42,880
to hear about how in Buddhism you've got a chance to come back because of course in the religion

68
00:06:42,880 --> 00:06:47,520
she had been brought up with. That's it. When you die, it's either having her health forever,

69
00:06:47,520 --> 00:06:50,800
but she was thinking, wow, you're not to be able to come back and have another chance.

70
00:06:50,800 --> 00:06:55,520
So for Westerners, I think this is common. We think, oh, it's great. I'll be able to come back

71
00:06:55,520 --> 00:07:00,400
and try again like this, that movie Groundhog Day, you know, until you get it right.

72
00:07:01,440 --> 00:07:04,880
And that's really the point is until you get it right and you're going to keep coming back.

73
00:07:05,840 --> 00:07:13,760
But whether this should be looked at, seen as a good thing or not, is debatable. I mean, ask yourself,

74
00:07:14,800 --> 00:07:17,920
don't ask yourself, how are you feeling now? Because most people think, well, I'm okay,

75
00:07:17,920 --> 00:07:22,800
and I've got plans, you know, if I can just this and this and this, there's a chance that I'll

76
00:07:22,800 --> 00:07:30,080
be stable and happy and living a good life. But then, you know, don't ask yourself, don't

77
00:07:30,080 --> 00:07:33,920
look at that. Ask yourself what it's taken to get even to the point that you're at now.

78
00:07:34,720 --> 00:07:39,680
And think of all the lessons you've had to learn. And if it's true that you come back again and

79
00:07:39,680 --> 00:07:44,480
again and again, then you're going to have to learn all of those. If you don't learn anything now

80
00:07:44,480 --> 00:07:50,400
and change anything about this state, you don't somehow increase your level of maturity in a way

81
00:07:50,400 --> 00:07:57,040
that you've never done before in all your lives, then you're just going to have to learn these

82
00:07:57,040 --> 00:08:02,400
lessons again and go through all of the suffering that we had to go through as children, as teens,

83
00:08:02,400 --> 00:08:09,120
as young adults, as adults, and so on. And wait until you get old sick and die, I mean, see how

84
00:08:09,120 --> 00:08:14,000
that's going to be. And then don't stop there, look at the people around you and in the world

85
00:08:14,000 --> 00:08:19,680
around us, who's to say, next life, you won't be one of the people who suffer terribly in this life.

86
00:08:21,440 --> 00:08:28,080
So it's not as simple a thing as some people might think, they think, oh great, I'll come back,

87
00:08:28,080 --> 00:08:32,480
I'll get to do this again and I'll do this differently and I'll learn more and so no, it's not like

88
00:08:32,480 --> 00:08:38,560
that. If you're not careful, you know, some lifetimes down the road, it's very, very possible that

89
00:08:38,560 --> 00:08:44,320
you'll end up in a terrible situation, in a situation of intense suffering, why do people suffer

90
00:08:44,960 --> 00:08:48,960
in such horrible ways that they do and who's to say you won't end up like them?

91
00:08:50,880 --> 00:08:59,680
So there is some, we can see some benefit in at least bringing ourselves somewhere,

92
00:08:59,680 --> 00:09:06,080
somewhat out of this state, to a state where we don't lose all of our memories every 50 to 100 years,

93
00:09:06,080 --> 00:09:13,680
where we're able to keep them and to continue developing, that's at the very least a good thing to

94
00:09:13,680 --> 00:09:21,040
do. And so, you know, the development of the mind shouldn't scare people, the practice of Buddhism

95
00:09:21,040 --> 00:09:26,560
shouldn't be a scary thing that you're going to let go of things you hold on to. Buddhism is

96
00:09:26,560 --> 00:09:31,360
about learning, it's about coming to grow, it's growing up and coming to learn things and

97
00:09:31,360 --> 00:09:39,280
understand more about reality, it's not to brainwash us or to force some kind of detached state,

98
00:09:39,280 --> 00:09:46,640
it's to help us to learn what's causing us suffering because all of us can tell if we're not

99
00:09:46,640 --> 00:09:54,480
terribly blind, we can verify that there is a lot of suffering in this world and the truth of

100
00:09:54,480 --> 00:10:00,240
the Buddha's teaching is that the suffering is unnecessary, we don't have to suffer in this way.

101
00:10:00,240 --> 00:10:05,040
The reason we do is because we don't understand and because we don't understand, we therefore do

102
00:10:05,040 --> 00:10:11,360
things that cause us suffering and that cause other people suffering. So slowly we learn how to overcome

103
00:10:11,360 --> 00:10:17,840
those things, how to be free from them and eventually we learn how to be free from all suffering,

104
00:10:17,840 --> 00:10:24,400
how to never cause suffering for others or for ourselves, how to act in such a way that is not

105
00:10:24,400 --> 00:10:30,160
contradictory to our wishes, where we wish to be happy, we wish to live in peace and yet we find

106
00:10:30,160 --> 00:10:36,400
ourselves acting and speaking and thinking in ways that cause us suffering and cause disharmony

107
00:10:36,400 --> 00:10:45,040
in the world around us. So this is the I think sort of a detailed answer of why freedom would be

108
00:10:45,040 --> 00:10:52,720
the last thing because once you have no clinging, once you have no attachment and no delusion,

109
00:10:52,720 --> 00:11:04,160
you will not cause any suffering for yourself and as a result, you will not have to come back and

110
00:11:06,000 --> 00:11:16,480
there is no more creating, no more forming, no more building, you realize that there is nothing

111
00:11:16,480 --> 00:11:22,480
worth clinging to, that true happiness doesn't come from the objects of the sense or any of the objects

112
00:11:22,480 --> 00:11:29,280
either inside of us or in the world around us, true happiness comes from freedom and you come to

113
00:11:29,280 --> 00:11:35,840
see that this freedom is the ultimate happiness. So let's answer to that question. The second question

114
00:11:36,880 --> 00:11:39,840
I'm going to have to deal with it here so this video is going to be a little bit longer.

115
00:11:40,880 --> 00:11:50,640
The second question is whether karma has physical results only or affects the mind as well.

116
00:11:50,640 --> 00:11:58,400
And these questions are actually fairly easy because they are just simple,

117
00:11:58,400 --> 00:12:04,320
doctrinal questions but they are interesting as well. Karma, first we have to understand that

118
00:12:04,320 --> 00:12:11,280
karma is not a thing, it doesn't exist in the world, you can't show me karma. Karma means

119
00:12:11,280 --> 00:12:17,040
action, so technically speaking you can show me an action when you show me the karma of killing,

120
00:12:17,040 --> 00:12:23,040
so you kill something, there is karma for you. But the problem is that we come to take karma

121
00:12:23,040 --> 00:12:28,080
as this substance that exists, like something you are carrying around like a weight over your

122
00:12:28,080 --> 00:12:33,840
shoulder or that is somehow tattooed on your skin or something like that, that you carry around

123
00:12:33,840 --> 00:12:40,640
and you are just waiting to drop on your head or something. And that's not the case, karma is

124
00:12:40,640 --> 00:12:54,640
a law, it's a law that says that there is an effect to our actions and not just our actions

125
00:12:54,640 --> 00:13:02,960
actually to our intentions, to our state of mind that the mind is able to affect the world around

126
00:13:02,960 --> 00:13:08,720
us. It's not a doctrine of determinism, if you were this substance or this thing that you could

127
00:13:08,720 --> 00:13:13,680
measure, then there might be some determinism, but it doesn't say that karma just says that

128
00:13:14,800 --> 00:13:21,200
it doesn't say either way that there is a free will or determinism, that's not the point of

129
00:13:21,200 --> 00:13:28,800
karma, karma says that when there is action, when you do kill or steal or lie or when in your mind

130
00:13:28,800 --> 00:13:37,920
you develop these unwholesome tendencies to these ideas for the creation of disharmony and

131
00:13:37,920 --> 00:13:46,000
suffering and stress, then there isn't the effect, the effect is that stress is created

132
00:13:47,280 --> 00:13:53,200
when you have the intention to help people as well. There's another sort of stress created,

133
00:13:54,560 --> 00:14:00,400
not an unpleasant stress, but it's a stress of sorts that creates pleasure, it's a vibration,

134
00:14:00,400 --> 00:14:06,960
when you, it's like a vibration, when you want to help people, when you have the intention to do

135
00:14:06,960 --> 00:14:18,320
good things, to give, to help, to support, to teach, to even to just be kind and when you study,

136
00:14:18,320 --> 00:14:25,120
when you learn, when you practice meditation, all of these things have an effect, they change who we

137
00:14:25,120 --> 00:14:37,520
are and so that's, it's important to understand that aspect of karma. But the short answer

138
00:14:37,520 --> 00:14:44,320
here question then is that karma, our actions will affect both the body and the mind.

139
00:14:45,760 --> 00:14:54,960
Technically speaking karma gives rise to experiences, it gives rise to seeing, hearing, smelling,

140
00:14:54,960 --> 00:15:00,800
tasting, feeling, thinking, this is the technical explanation, that it's going to give rise to

141
00:15:02,080 --> 00:15:07,200
sensual experience in the future. If you, if you do something and you intend to do something,

142
00:15:07,200 --> 00:15:11,600
that's going to change the experiences that you have. You're not going to see the same things

143
00:15:11,600 --> 00:15:16,400
as if you hadn't done that, right? If you kill someone, you're going to see the inside of a jail and

144
00:15:16,400 --> 00:15:21,920
so on. Hearing, smelling, tasting, feeling, thinking, but from a technical point of view,

145
00:15:21,920 --> 00:15:26,720
all that's what happens is there are different experiences that you have and those experiences

146
00:15:26,720 --> 00:15:36,880
are both physical and mental. So all it means is that our, we are affecting our destiny, whether this

147
00:15:36,880 --> 00:15:44,160
is based on free will or determinism isn't really the question. The point is that when we do do

148
00:15:44,160 --> 00:15:54,960
something, there is a result and so rather than worrying about such questions, whether it's free

149
00:15:54,960 --> 00:16:01,120
will or determinism or what does karma affect? Is it a physical or a mental thing? We should

150
00:16:01,120 --> 00:16:08,560
stop, we should train ourselves to, to give up the unful of some tendencies that cause

151
00:16:08,560 --> 00:16:13,840
us suffering. In the end, the interesting thing is that you give up both kinds of karma because you

152
00:16:13,840 --> 00:16:19,920
have no intention to create happiness either, to create happiness in a sense of happy experiences.

153
00:16:19,920 --> 00:16:25,520
You come to see that even pleasurable experiences are temporary. Even if you create harmony in the

154
00:16:25,520 --> 00:16:31,200
world, even if I were able to with my great karma, create peace and harmony in the world,

155
00:16:31,200 --> 00:16:38,400
and with some great act of minor series of acts, it's temporary. And unless there's wisdom

156
00:16:38,400 --> 00:16:43,360
and understanding that allows people to let go, they're just going to ruin it when I'm gone.

157
00:16:44,960 --> 00:16:50,240
For instance, the Buddha's teaching, when the Buddha was around, there was a lot of good in India

158
00:16:51,760 --> 00:16:58,000
and it lasted for some time and then India went back and now it's an ordinary country again.

159
00:16:58,000 --> 00:17:04,800
But for some time in India it was a special place and it was very Buddhist and there was very little

160
00:17:04,800 --> 00:17:11,520
killing and so on, one might, from what we understand. They're just an example, but the point is

161
00:17:11,520 --> 00:17:16,400
that it's impermanent and no matter what good things you do, even the Buddha's teaching is impermanent,

162
00:17:16,400 --> 00:17:21,520
it's not going to last forever and this is the teaching of a perfectly enlightened Buddha as we

163
00:17:21,520 --> 00:17:29,760
understand. So, you know, most important is to become free and to become free from karma as well,

164
00:17:29,760 --> 00:17:37,280
to not have any attachment to that thing that should turn out in a certain way, not to be expecting

165
00:17:37,280 --> 00:17:47,440
or have expectations about the future, to simply be content and in tune with reality, to see

166
00:17:47,440 --> 00:17:54,080
reality for what it is and to be at peace with that and to not cling and to not want and to not

167
00:17:54,720 --> 00:18:01,280
hope and care and worry and so on. But to live one's life in a dynamic way where you

168
00:18:03,040 --> 00:18:10,960
can accept and react and respond appropriately to every situation doesn't mean that you

169
00:18:10,960 --> 00:18:17,440
live in one place and do nothing, sit around and do nothing, it means that you're able to

170
00:18:17,440 --> 00:18:22,240
live dynamically, you're not a stick in the mud and when it's time to move can't move,

171
00:18:22,880 --> 00:18:30,960
your person, your mind is able to respond appropriately to all experiences and react

172
00:18:32,480 --> 00:18:40,320
without attachment. So, you know, fairly detailed answers to your questions, I hope they

173
00:18:40,320 --> 00:18:52,880
did hit the mark to at least to some extent. So, thanks for the questions, all the best.

