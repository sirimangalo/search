1
00:00:00,000 --> 00:00:08,000
Okay, so welcome everyone to this week's episode of long radio time for announcements.

2
00:00:11,000 --> 00:00:14,000
I've got, guess a couple of announcements.

3
00:00:14,000 --> 00:00:25,000
One that we, I hit 2 million views on YouTube, which is an interesting thing to think about.

4
00:00:25,000 --> 00:00:29,000
Because it really doesn't mean it really isn't that big of a number.

5
00:00:29,000 --> 00:00:35,000
I think there are videos on YouTube with like 10 million views, one video.

6
00:00:35,000 --> 00:00:41,000
So it really doesn't have that much significance.

7
00:00:41,000 --> 00:00:48,000
But on the other hand, if you think of 2 million, I don't know, 2 million video views.

8
00:00:48,000 --> 00:00:50,000
I mean it, it says something.

9
00:00:50,000 --> 00:00:55,000
It says that people are interested in Buddhism, interested in meditation.

10
00:00:55,000 --> 00:01:04,000
I'm interested in pornography and masturbation because that's one of my most famous videos.

11
00:01:04,000 --> 00:01:07,000
It's kind of sad and depressing in a way.

12
00:01:07,000 --> 00:01:10,000
But the number 1 video is about meditation.

13
00:01:10,000 --> 00:01:12,000
It's not the number 1 video.

14
00:01:12,000 --> 00:01:15,000
It's the number 2 video.

15
00:01:15,000 --> 00:01:21,000
So yeah, well, it's good to laugh about such things.

16
00:01:21,000 --> 00:01:28,000
We take them too seriously and that causes a lot of the problems that they are blamed for.

17
00:01:28,000 --> 00:01:33,000
It's our stress and our feelings of guilt that cause most of the suffering.

18
00:01:33,000 --> 00:01:39,000
You may overcome that, the problems themselves are not that insurmountable.

19
00:01:39,000 --> 00:01:41,000
I suppose they are difficult.

20
00:01:41,000 --> 00:01:50,000
I mean Buddha said sensuality is the greatest bind that there is ropes and thongs and chains.

21
00:01:50,000 --> 00:01:57,000
Weak and comparison to the bond of sensuality.

22
00:01:57,000 --> 00:02:01,000
So it's not something that you should take lightly for sure.

23
00:02:01,000 --> 00:02:08,000
But it's not something you should feel guilty and hate yourself.

24
00:02:08,000 --> 00:02:13,000
So okay, anyway, on track that was 2 million views.

25
00:02:13,000 --> 00:02:16,000
So here's to another 2 million.

26
00:02:16,000 --> 00:02:28,000
And hopefully with everyone's help here we'll have some good videos to start as hard as it should be this week.

27
00:02:28,000 --> 00:02:38,000
Another announcement we have mentioned it yesterday, I think, that we have candid it for ordination.

28
00:02:38,000 --> 00:02:47,000
And the big announcement today, a different from yesterday, is that he's going to be flying back to Finland.

29
00:02:47,000 --> 00:02:56,000
Probably flying back to Finland in the very near future, in order to ordain quicker than, quicker than was thought.

30
00:02:56,000 --> 00:03:03,000
So possibly ordain with me here in Sri Lanka before I leave for Thailand.

31
00:03:03,000 --> 00:03:08,000
And then go with me to Thailand, which would be great to have a companion.

32
00:03:08,000 --> 00:03:16,000
I think there's a Sri Lankan monk joining us as well who wants to go and meditate in the forests of Thailand, which would be great.

33
00:03:16,000 --> 00:03:27,000
I'm wondering why he doesn't meditate in the forest here, but I think it's hard to meditate in your own country sometimes because too many relatives and friends.

34
00:03:27,000 --> 00:03:34,000
So when you go to a foreign country you have to take it more seriously.

35
00:03:34,000 --> 00:03:43,000
And the other man who's staying here, George, is leaving soon.

36
00:03:43,000 --> 00:03:52,000
His grandmother died and he has to go back for, well, he was only going to stay till Friday anyway, but he could be going back early.

37
00:03:52,000 --> 00:04:00,000
But the reason I mentioned him is because he's an interesting case. He was, you know, everyone's running away, having very, very much difficulty staying in the forest.

38
00:04:00,000 --> 00:04:04,000
And he was one of those people, men mainly actually.

39
00:04:04,000 --> 00:04:07,000
The women don't have such troubles.

40
00:04:07,000 --> 00:04:12,000
So last week he was quite ready to leave and then he suddenly had a breakthrough.

41
00:04:12,000 --> 00:04:14,000
And now he doesn't want to leave.

42
00:04:14,000 --> 00:04:19,000
So he was asking me just now before I started, you know, what's he going to do?

43
00:04:19,000 --> 00:04:23,000
He's not going to be able to finish the course as you can to continue.

44
00:04:23,000 --> 00:04:30,000
And I said, let's just come back and we can talk over Skype or run the internet somehow and get practicing.

45
00:04:30,000 --> 00:04:32,000
But I said, you know, now you've made it.

46
00:04:32,000 --> 00:04:37,000
You made it over the biggest hurdle, which is the one thing to leave hurdle.

47
00:04:37,000 --> 00:04:44,000
Once you get over that and really appreciate the meditation for what it is, then nothing will stop you.

48
00:04:44,000 --> 00:04:50,000
Nothing can stand in your way. You'll just have to, as long as you can continue practicing.

49
00:04:50,000 --> 00:04:55,000
You have faced the worst of it.

50
00:04:55,000 --> 00:05:01,000
So those are two announcements or a few announcements.

51
00:05:01,000 --> 00:05:07,000
Apart from that, everything is continuing.

52
00:05:07,000 --> 00:05:16,000
Some sara is still here. Anybody have anything they want to say? Any announcements that they have that they'd like to share?

53
00:05:16,000 --> 00:05:20,000
Anybody on our panel?

54
00:05:20,000 --> 00:05:24,000
Paulina is still planning to join us, I think, in October.

55
00:05:24,000 --> 00:05:28,000
And she may be chasing her mind. So there's an announcement. We might have a new...

56
00:05:28,000 --> 00:05:29,000
Yeah.

57
00:05:29,000 --> 00:05:31,000
Wonderful. I'm going to...

58
00:05:31,000 --> 00:05:36,000
Ah, yes, that's right. She's already made... Sorry, I'm back livery.

59
00:05:36,000 --> 00:05:40,000
She's already dedicating herself to. She's great.

60
00:05:40,000 --> 00:05:45,000
Mary and also, there's another person who was here yesterday. She's on a retreat today.

61
00:05:45,000 --> 00:05:49,000
That's why she's not joining us and her plan is to come as well.

62
00:05:49,000 --> 00:05:53,000
So lots of good things happening. There's a couple of men who...

63
00:05:53,000 --> 00:05:56,000
Well, we have Lori, who's...

64
00:05:56,000 --> 00:06:01,000
I said, I mentioned already. We've got other people who have expressed interest in ordaining

65
00:06:01,000 --> 00:06:04,000
and people who just want to come to meditate in Thailand.

66
00:06:04,000 --> 00:06:08,000
So lots of good things happening. Meichi Jaya is here, the Burmese nun.

67
00:06:08,000 --> 00:06:13,000
And she said to me, she really likes this place.

68
00:06:13,000 --> 00:06:22,000
She agrees with me and many of the aspects of how great a country is to live in as a monastic.

69
00:06:22,000 --> 00:06:25,000
And so I think her plan is to come back and bring some friends.

70
00:06:25,000 --> 00:06:30,000
She has some supporters and she wants to bring as well.

71
00:06:30,000 --> 00:06:35,000
So lots of good stuff happening.

72
00:06:35,000 --> 00:06:40,000
Just continuing on with our work and practice.

73
00:06:40,000 --> 00:07:04,000
So that's announcements for this week.

