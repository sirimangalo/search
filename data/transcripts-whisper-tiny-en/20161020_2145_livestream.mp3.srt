1
00:00:00,000 --> 00:00:03,200
Verify these claims themselves. In other words, rebur it.

2
00:00:03,200 --> 00:00:06,000
Look at the answer, those types of questions. Sorry.

3
00:00:08,400 --> 00:00:11,280
It's, I don't, you know, this whole long question thing. I mean,

4
00:00:12,640 --> 00:00:19,040
I'm not confident in that, that sort of direction.

5
00:00:19,040 --> 00:00:34,480
The blood has spoken that the antidote of aversion is meta, and so if one is experiencing a

6
00:00:34,480 --> 00:00:40,640
aversion to their meditation, would the meta cultivated from the practice may I be happy?

7
00:00:40,640 --> 00:00:46,240
May I be well? May I be safe? May I be people with the aforementioned aversion?

8
00:00:46,240 --> 00:00:50,560
Yeah. That's not meta. You don't send meta to yourself.

9
00:00:52,640 --> 00:00:57,760
That's actually, the VCD manga is quite clear on, you don't send meta to yourself. You do it as an

10
00:00:57,760 --> 00:01:03,520
example. You wish, you think, well, I want to be happy, and that's your example. And just like I

11
00:01:03,520 --> 00:01:11,760
want to be happy, may those people be happy? It's pretty, pretty standard not to focus on yourself

12
00:01:11,760 --> 00:01:19,040
beyond just an original making. I think we do too much in the west of this self-love.

13
00:01:19,040 --> 00:01:25,360
We've got to love yourself. Maybe, and we do, we are hard on ourselves, but it's kind of a

14
00:01:25,360 --> 00:01:33,920
different issue. It's kind of a different issue. That one is the conceit, which is a bit different.

15
00:01:33,920 --> 00:01:40,960
It's useful, but with meta, it's about may they be happy. May other people be happy. So you sit

16
00:01:40,960 --> 00:01:45,840
and you start with may I be happy, but then you can start to expand it to everyone else, and

17
00:01:46,640 --> 00:01:51,040
you're trying to, the same attitude of wishing for yourself to be able to kind of apply it to

18
00:01:51,040 --> 00:01:58,640
others. That's meta. It's applying it to others. But yeah, I mean, in general, meta is the

19
00:01:58,640 --> 00:02:08,000
antidote. So it's good. It's not an antidote, per se, because it's temporary, as long as the underlying

20
00:02:08,000 --> 00:02:14,400
inclinations are still not uprooted. May there's never going to be enough, but it's a certain

21
00:02:14,400 --> 00:02:28,160
thing that's important. Bante did Bahia only practice Dominopasana to attain Arashith. According to the

22
00:02:28,160 --> 00:02:33,840
Bahia Suta, it would seem that the Buddha only explained about noting seeing, is seeing, hearing,

23
00:02:33,840 --> 00:02:37,040
is hearing, etc., during that short sermon in the street.

24
00:02:37,040 --> 00:02:46,640
I think you're probably overthinking it. I mean, yeah, it's because one way of looking at

25
00:02:46,640 --> 00:02:50,560
reality, everything is seeing, hearing, smelling, tasting, feeling. He also said, feeling,

26
00:02:50,560 --> 00:02:57,360
and how do you know, the body guy in the person is, is through feeling. So that feeling just

27
00:02:57,360 --> 00:03:07,520
be feeling, well, that's also a guy in the person. It could be a vinyade, vinyade, vinyade, the

28
00:03:07,520 --> 00:03:31,440
vinyade. Huithinan, rit Anupasana. Yeah, I wouldn't overthink that. So very sweet.

