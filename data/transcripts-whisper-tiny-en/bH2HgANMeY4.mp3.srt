1
00:00:00,000 --> 00:00:21,000
Good evening everyone, broadcasting live, February 10th,

2
00:00:21,000 --> 00:00:30,000
2016.

3
00:00:30,000 --> 00:00:41,000
Tonight's quote is kind of interesting in it.

4
00:00:41,000 --> 00:00:54,000
It demonstrates or it appoints to a quality of the Buddhist teaching,

5
00:00:54,000 --> 00:00:58,000
an important quality of the Buddhist teaching,

6
00:00:58,000 --> 00:01:06,000
an important quality of proper meditation practice.

7
00:01:06,000 --> 00:01:13,000
And that is what we call, we mungs,

8
00:01:13,000 --> 00:01:19,000
we mungs at the ability to discern

9
00:01:19,000 --> 00:01:27,000
and thereby adapt one's practice.

10
00:01:27,000 --> 00:01:34,000
So not just to understand the practicing correctly,

11
00:01:34,000 --> 00:01:43,000
doesn't work simply by learning the right way to practice

12
00:01:43,000 --> 00:01:47,000
and then applying it steadily.

13
00:01:47,000 --> 00:01:52,000
That's not really how it works.

14
00:01:52,000 --> 00:01:54,000
If it were that simple,

15
00:01:54,000 --> 00:02:00,000
that's not logically consistent, it's just not that simple.

16
00:02:00,000 --> 00:02:07,000
It's not easy to stay on the right path

17
00:02:07,000 --> 00:02:11,000
because the object is constantly changing

18
00:02:11,000 --> 00:02:20,000
and because the subject, our minds are constantly changing.

19
00:02:20,000 --> 00:02:27,000
So to think that you can learn the right method and then practice correctly just by pushing

20
00:02:27,000 --> 00:02:36,000
just through sheer constant applied effort is mistaken.

21
00:02:36,000 --> 00:02:39,000
It's like thinking you can go into battle and just charge the enemy

22
00:02:39,000 --> 00:02:43,000
and win, it's much more complicated than that.

23
00:02:43,000 --> 00:02:45,000
There are many more steps.

24
00:02:45,000 --> 00:02:51,000
There are strategy. That's what this is, is strategy.

25
00:02:51,000 --> 00:03:04,000
How to stay on course when the battle ground changes.

26
00:03:04,000 --> 00:03:09,000
Because the technique doesn't change the right way to practice doesn't change

27
00:03:09,000 --> 00:03:15,000
but the object and the subject change.

28
00:03:15,000 --> 00:03:22,000
So it's about seeing the relationship, the present relationship

29
00:03:22,000 --> 00:03:30,000
at every moment between the subject and the object, the mind and the meditation object

30
00:03:30,000 --> 00:03:34,000
and being able to tell when you've gotten off track.

31
00:03:34,000 --> 00:03:40,000
So there's talks about when you attend to a Nimita, the word that's used in the polys,

32
00:03:40,000 --> 00:03:44,000
Nimita, another thing, a Nimita means a sign.

33
00:03:44,000 --> 00:03:49,000
So Nimita is the word that the Buddha used.

34
00:03:49,000 --> 00:03:54,000
It must have been a common sort of usage of the word

35
00:03:54,000 --> 00:03:59,000
but it refers to the characteristic or something.

36
00:03:59,000 --> 00:04:05,000
He used the word Nimita because in meditation that's all you have, you don't have things.

37
00:04:05,000 --> 00:04:15,000
Things drop away and all you have are our experiences with certain characteristics.

38
00:04:15,000 --> 00:04:21,000
So in Samhita, some of the meditation you'll focus on the characteristic of blue or blueness.

39
00:04:21,000 --> 00:04:25,000
And blueness is a Nimita.

40
00:04:25,000 --> 00:04:31,000
It's sort of like that. It's a little more complicated.

41
00:04:31,000 --> 00:04:37,000
But basically, if like a Nimita of a man or a woman, when you see a person,

42
00:04:37,000 --> 00:04:41,000
maybe at first you don't know whether they're a man or a woman, but then you realize,

43
00:04:41,000 --> 00:04:46,000
oh that's a man. So they have a Nimita, the sign of a man.

44
00:04:46,000 --> 00:04:52,000
I'm using their woman. Maybe sometimes you're not sure if they're a man or a woman in certain cases.

45
00:04:52,000 --> 00:04:57,000
And you can see how that waiver, you waiver because we're very obsessed about gender.

46
00:04:57,000 --> 00:05:03,000
Knowing whether someone is male or female or when we can't, when the sign doesn't present itself.

47
00:05:03,000 --> 00:05:07,000
It's a clear example of what is meant by Nimita.

48
00:05:07,000 --> 00:05:15,000
It's not the person, it's not the thing, it's the how it presents itself to the mind.

49
00:05:15,000 --> 00:05:22,000
And so, Nimita is something that you actually experience. It's a description of what you experience.

50
00:05:22,000 --> 00:05:28,000
And therefore, it can be used as a meditation object either for some other verb.

51
00:05:28,000 --> 00:05:39,000
So when the stomach rises, there's the sign of rising. When the stomach falls, there's the sign of all that kind of thing.

52
00:05:39,000 --> 00:05:48,000
But sometimes when you meditate, it leads to anger or it leads to greed or it leads to delusion.

53
00:05:48,000 --> 00:05:54,000
It leads to frustration or it leads to laziness, it leads to problems.

54
00:05:54,000 --> 00:06:03,000
So your mind rather than becoming focused and concentrated becomes upset.

55
00:06:03,000 --> 00:06:10,000
And the most common reaction to that is just to push harder, to force yourself.

56
00:06:10,000 --> 00:06:17,000
And that's exactly what we're talking about is being insufficient.

57
00:06:17,000 --> 00:06:23,000
Without a way, without adjusting your practice, without discriminating and being able to adjust your practice,

58
00:06:23,000 --> 00:06:26,000
pushing harder isn't going to cut it.

59
00:06:26,000 --> 00:06:33,000
So the first way you do it is by changing your object.

60
00:06:33,000 --> 00:06:38,000
And this can take many forms of your change, the parameters.

61
00:06:38,000 --> 00:06:43,000
Look at it differently, approach it differently.

62
00:06:43,000 --> 00:06:47,000
For example, if you're angry about something or frustrated about something,

63
00:06:47,000 --> 00:06:55,000
we'll change the focus on the frustration or focus on the thoughts surrounding the object.

64
00:06:55,000 --> 00:06:59,000
If you have pain and you just keep saying pain, pain, pain.

65
00:06:59,000 --> 00:07:03,000
There's a frustration that you're not dealing with.

66
00:07:03,000 --> 00:07:09,000
Now in some of the meditation, you can just change the object if you're focusing on loving kindness

67
00:07:09,000 --> 00:07:12,000
and you start to feel lust for your object.

68
00:07:12,000 --> 00:07:20,000
For example, if you're focusing on the dead body and you start to feel repulsion or depression or something,

69
00:07:20,000 --> 00:07:24,000
then you should switch to a different meditation practice.

70
00:07:24,000 --> 00:07:29,000
In repulsion and meditation, you can also do that if you're focusing too much on the mind

71
00:07:29,000 --> 00:07:33,000
and if I'm yourself getting lost, then you can focus on the body

72
00:07:33,000 --> 00:07:37,000
where you can focus on feelings or you can focus on the emotions.

73
00:07:37,000 --> 00:07:41,000
Different people are better to focus on one and the other.

74
00:07:41,000 --> 00:07:43,000
There's some extent.

75
00:07:43,000 --> 00:07:46,000
This isn't a trick.

76
00:07:46,000 --> 00:07:50,000
You can't look for... look at this as being some trick.

77
00:07:50,000 --> 00:07:55,000
You have to be clever, but you can't run away from the problem.

78
00:07:55,000 --> 00:07:58,000
So in the end, you do have to face from the issues.

79
00:07:58,000 --> 00:08:01,000
But it's about facing it in a different way.

80
00:08:01,000 --> 00:08:04,000
Facing a different aspect of the experience

81
00:08:04,000 --> 00:08:07,000
and constantly being ready to be flexible.

82
00:08:07,000 --> 00:08:14,000
That's the best way to deal with these things is to be flexible and look at it in a new way.

83
00:08:14,000 --> 00:08:16,000
You approach the problem in a different way.

84
00:08:16,000 --> 00:08:21,000
We had one monk once, like I told this story before.

85
00:08:21,000 --> 00:08:26,000
He was really stubborn, but it was this kind of stubbornness

86
00:08:26,000 --> 00:08:32,000
where he felt really a low self-esteem, I think.

87
00:08:32,000 --> 00:08:38,000
He felt like he had problems because he had practice in my end of Buddhism

88
00:08:38,000 --> 00:08:41,000
and he had made a determination to become a Buddha

89
00:08:41,000 --> 00:08:46,000
or a Bodhisattva or whatever that means.

90
00:08:46,000 --> 00:08:51,000
But then his teacher, in Tibet, refused to teach him

91
00:08:51,000 --> 00:08:54,000
and told him to come and learn Vipassana with us first.

92
00:08:54,000 --> 00:08:59,000
But he couldn't practice Vipassana because we practiced to become free from suffering.

93
00:08:59,000 --> 00:09:02,000
And the Bodhisattva is the opposite.

94
00:09:02,000 --> 00:09:06,000
Or to some extent they make a vow to stay in the samsara,

95
00:09:06,000 --> 00:09:08,000
and not be free from suffering.

96
00:09:08,000 --> 00:09:14,000
He was stuck in any way, the point of it.

97
00:09:14,000 --> 00:09:18,000
He just got to the point where he felt that he couldn't practice at all.

98
00:09:18,000 --> 00:09:24,000
And he wasn't blaming anyone or he wasn't really resisting.

99
00:09:24,000 --> 00:09:27,000
He just said, I just can't be mindful. I can't practice.

100
00:09:27,000 --> 00:09:30,000
He said, I do sitting and I just,

101
00:09:30,000 --> 00:09:36,000
you say that he just gets something physical or something mental.

102
00:09:36,000 --> 00:09:38,000
I can't remember some reason it just wouldn't.

103
00:09:38,000 --> 00:09:40,000
He couldn't do sitting meditation.

104
00:09:40,000 --> 00:09:45,000
And my teachers said, well, then you're walking meditation.

105
00:09:45,000 --> 00:09:49,000
But it had gone on and on and he got into this point where

106
00:09:49,000 --> 00:09:50,000
they just couldn't sit.

107
00:09:50,000 --> 00:09:51,000
Well, then do walking.

108
00:09:51,000 --> 00:09:52,000
He said, yeah, yeah.

109
00:09:52,000 --> 00:09:54,000
I've tried walking meditation.

110
00:09:54,000 --> 00:09:56,000
Can't do walking does work either.

111
00:09:56,000 --> 00:09:58,000
He said, okay, well, I'm walking.

112
00:09:58,000 --> 00:09:59,000
Then just stand there.

113
00:09:59,000 --> 00:10:01,000
You know, standing doesn't work.

114
00:10:01,000 --> 00:10:03,000
I've tried that.

115
00:10:03,000 --> 00:10:06,000
I stood still for a while, but then it was like his body

116
00:10:06,000 --> 00:10:08,000
starts to contort or something.

117
00:10:08,000 --> 00:10:11,000
And he said, well, then do lying meditation.

118
00:10:11,000 --> 00:10:13,000
And he was like that.

119
00:10:13,000 --> 00:10:14,000
Oh, yeah.

120
00:10:14,000 --> 00:10:16,000
Okay, well, lying meditation.

121
00:10:16,000 --> 00:10:19,000
I can do that.

122
00:10:19,000 --> 00:10:21,000
It didn't ever really work for me.

123
00:10:21,000 --> 00:10:24,000
I ended up lighting himself on fire.

124
00:10:24,000 --> 00:10:27,000
Slitting his wrists.

125
00:10:27,000 --> 00:10:31,000
And eventually just rubbing that.

126
00:10:31,000 --> 00:10:36,000
It was an interesting sort of the extreme where

127
00:10:36,000 --> 00:10:39,000
yeah, there were other issues with him.

128
00:10:39,000 --> 00:10:42,000
But where nothing was working.

129
00:10:42,000 --> 00:10:45,000
The idea of adapting your practice.

130
00:10:45,000 --> 00:10:49,000
So changing postures is a big one.

131
00:10:49,000 --> 00:10:50,000
Changing your food.

132
00:10:50,000 --> 00:10:52,000
Just lots of interesting things.

133
00:10:52,000 --> 00:10:55,000
Like little tricks that can help when things

134
00:10:55,000 --> 00:10:57,000
start to go really bad.

135
00:10:57,000 --> 00:10:59,000
But you should never look for tricks really.

136
00:10:59,000 --> 00:11:02,000
And these things can help.

137
00:11:02,000 --> 00:11:06,000
But you have to be willing to admit or to face.

138
00:11:06,000 --> 00:11:10,000
You have to be willing to face your problems still.

139
00:11:10,000 --> 00:11:13,000
That doesn't mean that you shouldn't look at these things.

140
00:11:13,000 --> 00:11:17,000
You know, if food is a problem, you should you have to let us know.

141
00:11:17,000 --> 00:11:20,000
If your posture is a problem, sometimes you have to change your

142
00:11:20,000 --> 00:11:21,000
posture.

143
00:11:21,000 --> 00:11:25,000
Don't look at these to be fixes, but they can help.

144
00:11:25,000 --> 00:11:28,000
Don't ignore them.

145
00:11:28,000 --> 00:11:31,000
Change locations sometimes.

146
00:11:31,000 --> 00:11:37,000
Some locations are just bad for meditation.

147
00:11:37,000 --> 00:11:41,000
These things sometimes you have to be careful about.

148
00:11:41,000 --> 00:11:44,000
If that doesn't work.

149
00:11:44,000 --> 00:11:47,000
But it has another option.

150
00:11:47,000 --> 00:11:53,000
He says, look at the bad thoughts that come up.

151
00:11:53,000 --> 00:11:54,000
Look at the anger.

152
00:11:54,000 --> 00:12:00,000
Look at the greed, look at the delusion.

153
00:12:00,000 --> 00:12:04,000
And remind yourself the problem with these things.

154
00:12:04,000 --> 00:12:07,000
Look at them particularly.

155
00:12:07,000 --> 00:12:09,000
I mean, you could translate this into the past.

156
00:12:09,000 --> 00:12:11,000
And the meditation is actually saying to yourself, angry,

157
00:12:11,000 --> 00:12:14,000
angry, wanting, wanting.

158
00:12:14,000 --> 00:12:17,000
Or with delusion it's more difficult.

159
00:12:17,000 --> 00:12:19,000
Because you can't really be mindful.

160
00:12:19,000 --> 00:12:22,000
But conceited, conceited.

161
00:12:22,000 --> 00:12:29,000
It's more like knowing, knowing, or thinking, thinking.

162
00:12:29,000 --> 00:12:34,000
But at least reminding yourself the true nature of this thing.

163
00:12:34,000 --> 00:12:38,000
So he says you actually say to yourself,

164
00:12:38,000 --> 00:12:41,000
now you just ponder the disadvantages.

165
00:12:41,000 --> 00:12:44,000
So thinking about them in that way is sometimes you're giving yourself

166
00:12:44,000 --> 00:12:47,000
a pep top can be useful.

167
00:12:47,000 --> 00:12:51,000
Because you get so caught up in meditation that it's easy to get

168
00:12:51,000 --> 00:12:53,000
to lose track of why you're here.

169
00:12:53,000 --> 00:12:55,000
And then you step back and say, look, you know,

170
00:12:55,000 --> 00:13:02,000
I didn't come here to crave food or I didn't come here to flip out

171
00:13:02,000 --> 00:13:03,000
and get angry.

172
00:13:03,000 --> 00:13:06,000
Or you know, I didn't come here because I feel

173
00:13:06,000 --> 00:13:09,000
I can't afford to be conceited or arrogant.

174
00:13:09,000 --> 00:13:12,000
So this is not useful to me.

175
00:13:12,000 --> 00:13:14,000
There's a waste of my time.

176
00:13:14,000 --> 00:13:15,000
That kind of thing.

177
00:13:15,000 --> 00:13:17,000
And that can help.

178
00:13:17,000 --> 00:13:22,000
The sort of step back, there's more we monks reminding yourself

179
00:13:22,000 --> 00:13:26,000
and adjusting your mindset.

180
00:13:26,000 --> 00:13:30,000
So if that doesn't work,

181
00:13:30,000 --> 00:13:34,000
then you should ignore them.

182
00:13:34,000 --> 00:13:37,000
Ignore the sort of frustration and turn away.

183
00:13:37,000 --> 00:13:40,000
So this is where if something stays and it just doesn't go away,

184
00:13:40,000 --> 00:13:43,000
you can just ignore it.

185
00:13:43,000 --> 00:13:47,000
Try to become an equanimist about it.

186
00:13:47,000 --> 00:13:50,000
So if you do feel anger, then just ignore the anger.

187
00:13:50,000 --> 00:13:52,000
If you feel greed, then just ignore the greed.

188
00:13:52,000 --> 00:13:54,000
Do feel delusion.

189
00:13:54,000 --> 00:13:58,000
You can actually try to ignore it and it might go away.

190
00:13:58,000 --> 00:14:00,000
It's not the best way, but he's going down the list.

191
00:14:00,000 --> 00:14:03,000
So at this point, you know, maybe it'll go away.

192
00:14:03,000 --> 00:14:04,000
Maybe it'll subdue.

193
00:14:04,000 --> 00:14:05,000
Be subdued.

194
00:14:05,000 --> 00:14:08,000
You stop giving power to it really.

195
00:14:08,000 --> 00:14:12,000
It is a valid and this is the Buddha talking as far as we know.

196
00:14:12,000 --> 00:14:17,000
When you stop giving power to your emotions,

197
00:14:17,000 --> 00:14:20,000
it is an important aspect.

198
00:14:20,000 --> 00:14:22,000
Because anxiety leads to anxiety,

199
00:14:22,000 --> 00:14:25,000
fear leads to fear, anger, you get angry about the fact.

200
00:14:25,000 --> 00:14:28,000
So you're sitting there, you want something

201
00:14:28,000 --> 00:14:30,000
and then you're like, yeah, yeah, I want that.

202
00:14:30,000 --> 00:14:33,000
Or you feel anger frustrated and then that makes you frustrated.

203
00:14:33,000 --> 00:14:35,000
Like, why am I frustrated?

204
00:14:35,000 --> 00:14:37,000
Wasting my time.

205
00:14:37,000 --> 00:14:40,000
I'm useless at this or whatever.

206
00:14:40,000 --> 00:14:45,000
Sometimes you have to stop giving power to it.

207
00:14:45,000 --> 00:14:50,000
Stop entertaining those thoughts.

208
00:14:50,000 --> 00:14:59,000
There's a fourth one.

209
00:14:59,000 --> 00:15:04,000
I'm just going to look at the poly because we talk.

210
00:15:04,000 --> 00:15:11,000
We talk about the same time.

211
00:15:11,000 --> 00:15:16,000
We talk about the same time.

212
00:15:16,000 --> 00:15:18,000
I don't know.

213
00:15:18,000 --> 00:15:25,000
I can.

214
00:15:25,000 --> 00:15:26,000
Yeah, it's not.

215
00:15:26,000 --> 00:15:27,000
Here's a better transition.

216
00:15:27,000 --> 00:15:31,000
You need to give attention to stealing the thought formation of those thoughts.

217
00:15:31,000 --> 00:15:38,000
Stopping the cause of the thought.

218
00:15:38,000 --> 00:15:43,000
So this is accomplished by inquiring when an unwholesome thought has arisen.

219
00:15:43,000 --> 00:15:50,000
What is the cause? What is the cause of its cause?

220
00:15:50,000 --> 00:15:52,000
So stopping it from arising.

221
00:15:52,000 --> 00:15:57,000
Giving the focus on the cause of the thoughts.

222
00:15:57,000 --> 00:16:02,000
Focus on the root.

223
00:16:02,000 --> 00:16:07,000
And still that.

224
00:16:07,000 --> 00:16:12,000
In some cases, it's just an experience.

225
00:16:12,000 --> 00:16:14,000
You can focus on the experience.

226
00:16:14,000 --> 00:16:18,000
But I think in general, not totally convinced that it's just the cause,

227
00:16:18,000 --> 00:16:21,000
but that's what the commentary said.

228
00:16:21,000 --> 00:16:28,000
It's more about stealing, calming, like calming a small child.

229
00:16:28,000 --> 00:16:33,000
Hush, hush.

230
00:16:33,000 --> 00:16:37,000
Focus on calming the mind.

231
00:16:37,000 --> 00:16:40,000
And if you focus on calming the mind, if you focus on...

232
00:16:40,000 --> 00:16:42,000
I mean, there are other goals of good.

233
00:16:42,000 --> 00:16:45,000
If you can find the cause and you can do it with it,

234
00:16:45,000 --> 00:16:52,000
it's absolutely the actual defilement won't come up.

235
00:16:52,000 --> 00:16:55,000
But in any way, focusing on calming the mind.

236
00:16:55,000 --> 00:16:59,000
So sometimes lying down is going to net me.

237
00:16:59,000 --> 00:17:02,000
Sometimes getting up and walking.

238
00:17:02,000 --> 00:17:05,000
Sometimes moving your limbs if there's great pains.

239
00:17:05,000 --> 00:17:12,000
Sometimes you have to stretch.

240
00:17:12,000 --> 00:17:17,000
Sometimes there are thoughts that are a cause of anger,

241
00:17:17,000 --> 00:17:20,000
or greed, or delusion, and you have to address those thoughts

242
00:17:20,000 --> 00:17:28,000
and calm down those thoughts.

243
00:17:28,000 --> 00:17:34,000
Anyway, the idea that stealing is somehow...

244
00:17:34,000 --> 00:17:42,000
I mean, stealing the mind when the mind is calm, then those thoughts will subside.

245
00:17:42,000 --> 00:17:48,000
But if that doesn't even work, and here's the last one that is often remembered,

246
00:17:48,000 --> 00:17:57,000
then with his teeth clenched and his tongue pressed against the roof of his mouth,

247
00:17:57,000 --> 00:18:04,000
he should beat down, constrain, and crush mind with mind.

248
00:18:04,000 --> 00:18:07,000
Crush the thoughts.

249
00:18:07,000 --> 00:18:09,000
It was a last resort.

250
00:18:09,000 --> 00:18:14,000
But this Abura actually does say this.

251
00:18:14,000 --> 00:18:17,000
There are certain states that are so pernicious,

252
00:18:17,000 --> 00:18:19,000
that no matter what you do, they don't go away,

253
00:18:19,000 --> 00:18:21,000
and then you just have to say it and look.

254
00:18:21,000 --> 00:18:23,000
I've had enough of you.

255
00:18:23,000 --> 00:18:26,000
Go away.

256
00:18:26,000 --> 00:18:30,000
Not recommended as a first, or as a fallback option,

257
00:18:30,000 --> 00:18:35,000
or as something to rely upon, but from time to time,

258
00:18:35,000 --> 00:18:40,000
you can temporarily fix things by telling yourself, no.

259
00:18:40,000 --> 00:18:44,000
This works when Pete theorizes,

260
00:18:44,000 --> 00:18:48,000
when you find yourself shaking back and rocking back and forth.

261
00:18:48,000 --> 00:18:52,000
As it's hard to stop, it feels good.

262
00:18:52,000 --> 00:18:55,000
This happens in meditators, so those of you who don't know,

263
00:18:55,000 --> 00:18:57,000
haven't ever done a meditation course.

264
00:18:57,000 --> 00:19:00,000
This often happens when you start to rock back and forth,

265
00:19:00,000 --> 00:19:03,000
and it feels good because the pain in your back stops,

266
00:19:03,000 --> 00:19:05,000
and you've got a sort of a pleasant feeling.

267
00:19:05,000 --> 00:19:11,000
It's kind of nice and a sort of a faster.

268
00:19:11,000 --> 00:19:13,000
Sometimes it doesn't stop until you tell it to stop.

269
00:19:13,000 --> 00:19:15,000
It was a stop.

270
00:19:15,000 --> 00:19:17,000
It actually forced it to stop.

271
00:19:17,000 --> 00:19:22,000
Sometimes forcing is a stop cat measure.

272
00:19:22,000 --> 00:19:26,000
The whole point of this, in general,

273
00:19:26,000 --> 00:19:31,000
is being more clever than the actual,

274
00:19:31,000 --> 00:19:34,000
than the problem, getting smarter than it,

275
00:19:34,000 --> 00:19:37,000
outsmarting the problem.

276
00:19:37,000 --> 00:19:40,000
For adjusting your mind,

277
00:19:40,000 --> 00:19:43,000
adjusting the way you look at things.

278
00:19:43,000 --> 00:19:47,000
It's very important in meditation practice,

279
00:19:47,000 --> 00:19:49,000
because it's easy to get into a rat,

280
00:19:49,000 --> 00:19:53,000
but it's not about just forcing your way through it.

281
00:19:53,000 --> 00:19:55,000
You need to be clever,

282
00:19:55,000 --> 00:19:59,000
because these are some of the ways the Buddha taught to be clever.

283
00:19:59,000 --> 00:20:01,000
That's it.

284
00:20:01,000 --> 00:20:03,000
That's the devil's opinion.

285
00:20:03,000 --> 00:20:05,000
That's the devil's opinion.

286
00:20:05,000 --> 00:20:07,000
That's the devil's opinion.

287
00:20:07,000 --> 00:20:09,000
That's the devil's opinion.

288
00:20:09,000 --> 00:20:11,000
That's the devil's opinion, guys.

289
00:20:11,000 --> 00:20:13,000
Repeat.

290
00:20:13,000 --> 00:20:15,000
That's the word.

291
00:20:15,000 --> 00:20:19,000
This is the devil's opinion.

292
00:20:19,000 --> 00:20:21,000
Panelists.

293
00:20:21,000 --> 00:20:23,000
Now we have a panel.

294
00:20:23,000 --> 00:20:24,000
Buddhist panel.

295
00:20:24,000 --> 00:20:26,000
Oh, and you know what?

296
00:20:26,000 --> 00:20:28,000
I can't hear you because.

297
00:20:28,000 --> 00:20:30,000
Anyway, you guys can know.

298
00:20:30,000 --> 00:20:31,000
Thank you.

299
00:20:31,000 --> 00:20:34,000
Make a name.

300
00:20:34,000 --> 00:20:36,000
Not you, Ben.

301
00:20:36,000 --> 00:20:41,000
The local people can go many times.

302
00:20:41,000 --> 00:20:55,680
I have to figure out the sound doesn't have anything to do with the same thing, I don't

303
00:20:55,680 --> 00:20:56,680
understand.

304
00:20:56,680 --> 00:21:01,840
Okay, I think we have some more.

305
00:21:01,840 --> 00:21:09,840
Are you all just, none of you have questions, I don't think.

306
00:21:09,840 --> 00:21:11,840
Do any of you have questions?

307
00:21:11,840 --> 00:21:16,840
It's the usual suspects.

308
00:21:16,840 --> 00:21:23,680
You're just here to say hi.

309
00:21:23,680 --> 00:21:24,680
Can I hear you?

310
00:21:24,680 --> 00:21:26,080
Can you hear me?

311
00:21:26,080 --> 00:21:30,840
I can hear you fine, and I can hear you, but it's just good.

312
00:21:30,840 --> 00:21:35,520
I turned in real light, I just got out, I apologize.

313
00:21:35,520 --> 00:21:39,480
Sorry, I apologize for what?

314
00:21:39,480 --> 00:21:43,480
I turned in light, I just got keyed in.

315
00:21:43,480 --> 00:21:46,480
Okay, that's okay.

316
00:21:46,480 --> 00:21:59,760
Well, if there are no questions, oh, I have a question, sir, why are we all so different?

317
00:21:59,760 --> 00:22:06,280
Some people have lots of cars, some people have big houses, some people have nothing.

318
00:22:06,280 --> 00:22:11,320
Why are we all so different other than karma?

319
00:22:11,320 --> 00:22:15,080
Why do you expect us all to be the same, would that make sense?

320
00:22:15,080 --> 00:22:21,320
Like why are some people rich in some people poor, some people sick, some people healthy?

321
00:22:21,320 --> 00:22:26,760
What if we think that we would make more sense if we were all rich, are all poor?

322
00:22:26,760 --> 00:22:28,960
At least there would be equality of the masses.

323
00:22:28,960 --> 00:22:32,400
Yeah, but do you think that's more reasonable, more likely?

324
00:22:32,400 --> 00:22:34,960
I'm just talking universally, is that more likely to you?

325
00:22:34,960 --> 00:22:36,960
It would be nicer.

326
00:22:36,960 --> 00:22:41,760
Yeah, but that doesn't make it more likely.

327
00:22:41,760 --> 00:22:47,360
And nobody would be in wants, you wouldn't want, if everyone had the same, if everything,

328
00:22:47,360 --> 00:22:49,760
you wouldn't want what the other person has.

329
00:22:49,760 --> 00:22:55,160
Sure, so it's a good goal to work at, but if that would ever happen, I'd be like, wow,

330
00:22:55,160 --> 00:22:56,520
how did we get here?

331
00:22:56,520 --> 00:23:02,160
That would be more surprising for me than the way things are now, so I'm not quite clear

332
00:23:02,160 --> 00:23:03,960
on the question.

333
00:23:03,960 --> 00:23:05,960
Why are we all so different?

334
00:23:05,960 --> 00:23:16,080
Yeah, it's the way of the world, it just seems natural to me.

335
00:23:16,080 --> 00:23:21,400
Some people go in good directions, some people go in bad directions, some sort of karmic

336
00:23:21,400 --> 00:23:26,080
in the end.

337
00:23:26,080 --> 00:23:31,480
Sometimes we go in good directions, sometimes we go in bad directions.

338
00:23:31,480 --> 00:23:35,720
Some people are always happy, some people are always sad, some people have 10 karts, some

339
00:23:35,720 --> 00:23:37,680
people have one kart.

340
00:23:37,680 --> 00:23:44,880
Yeah, I don't know, some people are more happy, but always happy, not sure about that one.

341
00:23:44,880 --> 00:23:53,480
Maybe a Buddha or an enlightening, you could say, but even then, some people are never sad,

342
00:23:53,480 --> 00:24:02,880
so it's enlightening to people.

343
00:24:02,880 --> 00:24:05,400
I just don't get why we're all so different.

344
00:24:05,400 --> 00:24:09,720
I guess why would we all be the same?

345
00:24:09,720 --> 00:24:10,720
Why would we all be the same?

346
00:24:10,720 --> 00:24:12,240
We were more socialistic.

347
00:24:12,240 --> 00:24:13,240
Sure.

348
00:24:13,240 --> 00:24:17,920
Yeah, I mean, it's a good goal to work for it, absolutely.

349
00:24:17,920 --> 00:24:21,440
That you're thinking about it, I applaud that.

350
00:24:21,440 --> 00:24:31,000
Working towards equality, working towards helping people who are suffering, it's praise

351
00:24:31,000 --> 00:24:43,080
worthy, but very difficult goal to attain, and even more difficult to sustain.

352
00:24:43,080 --> 00:24:50,520
So if your goal is that, not really convinced because things are always changing.

353
00:24:50,520 --> 00:24:59,680
I didn't say that was my goal, I want more money, I want to be rich, but it's a funny thing

354
00:24:59,680 --> 00:25:01,720
to do, but you're rich.

355
00:25:01,720 --> 00:25:05,800
I want to be rich, but it would be nicer if everyone was like, you know the same.

356
00:25:05,800 --> 00:25:14,760
The funny thing is what we call rich in modern society, what we call rich is mostly on

357
00:25:14,760 --> 00:25:17,560
the backs of poor people.

358
00:25:17,560 --> 00:25:20,080
It wouldn't work for all people.

359
00:25:20,080 --> 00:25:29,120
We have computers, mobile phones, because, or there's an requirement for people to be in

360
00:25:29,120 --> 00:25:34,720
slave labor, you know, in lives that are really poor conditions, just so we can have the

361
00:25:34,720 --> 00:25:38,760
things that we want, there's the reality of it.

362
00:25:38,760 --> 00:25:48,320
So the system itself is problematic, so I think part of the answer to your question.

363
00:25:48,320 --> 00:25:52,640
You want to be rich, while there's your answer, everybody wanting to be rich is why some

364
00:25:52,640 --> 00:25:54,640
people have to be poor.

365
00:25:54,640 --> 00:25:57,360
Good, but I can tribute.

366
00:25:57,360 --> 00:25:58,360
Sorry?

367
00:25:58,360 --> 00:25:59,360
Could I contribute?

368
00:25:59,360 --> 00:26:01,360
Oh, absolutely.

369
00:26:01,360 --> 00:26:17,160
We're in a natural world, nature, you know, in all over the earth, we're just one part

370
00:26:17,160 --> 00:26:24,160
of the biosphere, so to speak, with animals and plants and earthquakes and tsunamis and

371
00:26:24,160 --> 00:26:31,960
natural disasters and some places in the world, you know, turning to desertification and

372
00:26:31,960 --> 00:26:41,880
some other places in the world maybe just remain lush and lots of good for all the critters

373
00:26:41,880 --> 00:26:52,600
and plants and such as that, it's almost like how could we expect people's human beings'

374
00:26:52,600 --> 00:27:01,680
circumstances to be far separated or different from all of the nature that we're

375
00:27:01,680 --> 00:27:12,400
amongst.

376
00:27:12,400 --> 00:27:18,600
I know there's, we don't appreciate that enough, the fact that all of this that we think

377
00:27:18,600 --> 00:27:23,920
of as human as not natural is just a part of nature.

378
00:27:23,920 --> 00:27:25,720
Yeah.

379
00:27:25,720 --> 00:27:31,800
I've talked about this before and I got in real trouble, it was a long, long controversy

380
00:27:31,800 --> 00:27:38,680
when I said the body is unnatural, I think a lot of dislikes of that, downvotes of that

381
00:27:38,680 --> 00:27:47,360
video, because on an ultimate level, the body is natural, or sorry, an ultimate level

382
00:27:47,360 --> 00:27:52,760
everything is natural, but if that were the case, then we wouldn't have this word unnatural.

383
00:27:52,760 --> 00:27:56,880
But what does it mean to say that something is unnatural?

384
00:27:56,880 --> 00:28:00,760
In order for that word to have meaning, we have to place a definition on the word.

385
00:28:00,760 --> 00:28:04,360
So what is the category of things that we would consider unnatural?

386
00:28:04,360 --> 00:28:08,680
It's either nothing or it's something, if it's nothing, well then everything is that,

387
00:28:08,680 --> 00:28:11,520
which is really true.

388
00:28:11,520 --> 00:28:18,920
But in a conventional sense, the word unnatural, I mean I defined it as something that

389
00:28:18,920 --> 00:28:25,480
is human-made, something that is made by this one specific group, or we could expand it

390
00:28:25,480 --> 00:28:28,720
that which is made by sentient being.

391
00:28:28,720 --> 00:28:36,440
So any sentient being creates something that's unnatural, you know, artificial is another

392
00:28:36,440 --> 00:28:42,720
word for it, and the body is artificial, the body is an artist, it's just making a point

393
00:28:42,720 --> 00:28:47,360
that we don't appreciate enough, we think that nature created the body, but actually

394
00:28:47,360 --> 00:28:53,240
we create the body, maybe this is something that we have formed in fashion, according

395
00:28:53,240 --> 00:28:59,080
to Buddhist thought, of course, many people don't believe that.

396
00:28:59,080 --> 00:29:07,000
Is that evolution that you're now not really, I mean evolution is kind of a part because

397
00:29:07,000 --> 00:29:14,240
it's trial and error, but the mind plays a part in it, and the mind we would say is

398
00:29:14,240 --> 00:29:21,640
always plays a primary part in it, in terms of actually actual trial and error, or the

399
00:29:21,640 --> 00:29:31,160
mind adjusts, sort of helps along what we would scientists see as being a completely

400
00:29:31,160 --> 00:29:36,120
physical process, but the mind is actually actively involved in deciding where evolution

401
00:29:36,120 --> 00:29:40,280
goes.

402
00:29:40,280 --> 00:29:53,080
Well, let me see, notionally, I guess theoretically, if civilization proceeds, continues

403
00:29:53,080 --> 00:30:05,720
to evolve, then supposedly brain features, crani, our skull features, brain would tend to

404
00:30:05,720 --> 00:30:14,160
change over time, maybe our limbs would start to atrophy, could that be part of what you're

405
00:30:14,160 --> 00:30:15,160
talking about?

406
00:30:15,160 --> 00:30:22,440
Yeah, but it comes from our mind, comes from the way that you act our mind, not just natural

407
00:30:22,440 --> 00:30:23,440
selection.

408
00:30:23,440 --> 00:30:29,640
Here's the new guy.

409
00:30:29,640 --> 00:30:31,720
Hi, I'm Fernando.

410
00:30:31,720 --> 00:30:32,720
Hi, Fernando.

411
00:30:32,720 --> 00:30:33,720
Nice to meet you, Juan.

412
00:30:33,720 --> 00:30:34,720
Nice to meet you.

413
00:30:34,720 --> 00:30:35,720
Where are you?

414
00:30:35,720 --> 00:30:44,720
Well, I live in Colombia, Missouri, but I'm from Mexico.

415
00:30:44,720 --> 00:30:47,360
Where are the rest of you from?

416
00:30:47,360 --> 00:30:48,360
From Darwin?

417
00:30:48,360 --> 00:30:49,360
Toronto.

418
00:30:49,360 --> 00:30:52,960
Oh, you live in Toronto?

419
00:30:52,960 --> 00:30:57,840
You're not here at Mac with me, are you?

420
00:30:57,840 --> 00:31:04,600
You live in Toronto?

421
00:31:04,600 --> 00:31:06,240
Larry, you're an American, right?

422
00:31:06,240 --> 00:31:15,240
Yes, I'm Mississippi, Southeast USA, and Simon, I think he's from Europe, right?

423
00:31:15,240 --> 00:31:16,240
Yep.

424
00:31:16,240 --> 00:31:17,640
I'm from Denmark.

425
00:31:17,640 --> 00:31:18,640
Denmark.

426
00:31:18,640 --> 00:31:22,480
It's a small country, right?

427
00:31:22,480 --> 00:31:26,680
Not a very small.

428
00:31:26,680 --> 00:31:29,680
What size of Mississippi?

429
00:31:29,680 --> 00:31:39,680
Oh, I actually think it's smaller, I mean, I couldn't imagine a state or even a big city

430
00:31:39,680 --> 00:31:43,080
being like the size of Denmark, but I couldn't be wrong.

431
00:31:43,080 --> 00:31:45,480
Well, if that's smaller.

432
00:31:45,480 --> 00:31:46,480
Yep.

433
00:31:46,480 --> 00:31:51,680
It's like I could just load up, or just have enough on time with comparing it the other

434
00:31:51,680 --> 00:31:52,680
day.

435
00:31:52,680 --> 00:31:59,200
It was like, I think it takes us as, as I can't remember what we did there, but it was

436
00:31:59,200 --> 00:32:05,560
like a huge, huge state in the US, like with 3 million people, and there was like 100 times

437
00:32:05,560 --> 00:32:31,920
bigger than it's country almost, like at least 10 times bigger than it's, et cetera,

438
00:32:31,920 --> 00:32:49,640
nine square kilometers, I mean, for 2000, sorry, alright, life to go study, because I have

439
00:32:49,640 --> 00:32:56,920
a Latin midterm in two days, have a great study spending, and thank you.

440
00:32:56,920 --> 00:32:57,920
Thank you.

441
00:32:57,920 --> 00:32:58,920
Thank you.

442
00:32:58,920 --> 00:32:59,920
Thank you.

443
00:32:59,920 --> 00:33:01,920
Good night, everyone.

444
00:33:01,920 --> 00:33:02,920
Good night.

445
00:33:02,920 --> 00:33:30,920
Good night.

