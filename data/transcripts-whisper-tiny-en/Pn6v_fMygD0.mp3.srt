1
00:00:00,000 --> 00:00:06,360
So Nelson asked, what is a good way to approach the parrota?

2
00:00:06,360 --> 00:00:11,760
Much of what I have read speaks in terms of them bringing good luck, etc.

3
00:00:11,760 --> 00:00:15,480
I'm respectfully this seems to be superstitious to me.

4
00:00:15,480 --> 00:00:20,000
Does their benefit lie only in the inspiration they can give one to live better?

5
00:00:20,000 --> 00:00:25,040
No, I think they have power and transit in the words have power.

6
00:00:25,040 --> 00:00:34,080
Apparently, it's possible with enough power to, for example, the Buddha made a vow for his

7
00:00:34,080 --> 00:00:37,800
bolt, float up stream, and apparently it did.

8
00:00:37,800 --> 00:00:44,880
I don't know, I mean, I'm willing to be open to the fact that there's inherent power in

9
00:00:44,880 --> 00:00:45,880
words.

10
00:00:45,880 --> 00:00:50,360
Even physically you could say the vibrations of the words have some power, but I think

11
00:00:50,360 --> 00:00:52,800
mentally there's a lot more power.

12
00:00:52,800 --> 00:00:59,320
If you think of how our minds can affect the physical in many ways, I mean, there's, of

13
00:00:59,320 --> 00:01:06,440
course, controversial, but you're talking about his monk, so this is our take on it.

14
00:01:06,440 --> 00:01:11,640
The mind is incredibly powerful, and the mind can make the body sick, and the mind can

15
00:01:11,640 --> 00:01:22,280
heal the body, the mind can... I've even been experienced some forms of black magic

16
00:01:22,280 --> 00:01:31,600
when I was in Thailand, that crazy lady, what tambourd tambourd tambourd tambourd, she was

17
00:01:31,600 --> 00:01:35,840
really angry at me one day, and she had told me several times how it was really interesting

18
00:01:35,840 --> 00:01:41,040
how when people did bad things to her, suddenly horrible things happen to them, like someone,

19
00:01:41,040 --> 00:01:45,080
you know, I can only assume what she's not telling me is that she's going around using

20
00:01:45,080 --> 00:01:48,800
the power of her mind against these people, and it was interesting because one time she

21
00:01:48,800 --> 00:01:54,760
got really angry at me, and suddenly I had this incredible pain in my neck that I couldn't

22
00:01:54,760 --> 00:02:01,400
even straighten my neck, I've never had that before, or I had it since, and for the

23
00:02:01,400 --> 00:02:09,480
whole day, for an entire day I could barely straighten my head, we were in the car, and

24
00:02:09,480 --> 00:02:17,680
every bump was just, it was incredible, I could barely move my head, and another example

25
00:02:17,680 --> 00:02:23,200
which kind of corroborated this was there was a monk who was pretty, you know, he had

26
00:02:23,200 --> 00:02:32,680
a gun and so on, this is an interesting monk to be sure, and he had, one of his arms

27
00:02:32,680 --> 00:02:42,040
was fairly paralyzed, and one day he got incredibly angry at me, or upset, or you

28
00:02:42,040 --> 00:02:47,080
know, there was a conflict between us, it wasn't just that he was angry, we were in conflict,

29
00:02:47,080 --> 00:02:51,880
and suddenly I couldn't move my arm, the other arm, the opposite arm, exactly as I couldn't

30
00:02:51,880 --> 00:03:01,360
even lift my arm up to pay respect, when he came in I had to hold my arm up as though

31
00:03:01,360 --> 00:03:06,160
I was a cripple, that's never happened, I mean that doesn't just happen, and it happened

32
00:03:06,160 --> 00:03:12,880
exactly when you got angry at me, so I'm willing to give the benefit of the doubt that

33
00:03:12,880 --> 00:03:18,560
the mind can do crazy things, in the end the mind can, you know, people have out of

34
00:03:18,560 --> 00:03:23,480
body experiences, I talked about something similar that I had when I was young, I believe

35
00:03:23,480 --> 00:03:30,120
all of, you know, I guarantee that these sorts of things exist with, you know, with

36
00:03:30,120 --> 00:03:38,040
some reasonable degree of certainty, and so the mind can radically alter reality, and

37
00:03:38,040 --> 00:03:44,640
so this as well has an important factor to play here, that if the paritas are done with

38
00:03:44,640 --> 00:03:51,120
a pure, a chandel, first of all, what is the parita, a parita is it a protection chanting,

39
00:03:51,120 --> 00:03:57,080
chanting the Buddha's teaching, or some praise of the Buddha, or other things as well,

40
00:03:57,080 --> 00:04:01,680
and thinking that it has some sort of power, so as I said the power can be physical, but

41
00:04:01,680 --> 00:04:07,640
it can also be mental, now the factors, there's a really good talk somewhere on the internet,

42
00:04:07,640 --> 00:04:13,080
and I have to try and find it where this, I think it's a Burmese monk, I think, actually

43
00:04:13,080 --> 00:04:18,960
we'll see the Nanda, but I can't remember who talks about what is required for a parita

44
00:04:18,960 --> 00:04:24,680
to be effective, the person who chants it has to understand it, this is the commentary

45
00:04:24,680 --> 00:04:31,240
that explains, the person who chants it has to understand it, they have to chant it correctly,

46
00:04:31,240 --> 00:04:37,400
the person who listens has to understand it, and I think there's many factors that contribute,

47
00:04:37,400 --> 00:04:41,520
I wouldn't say that it's necessary for all of them to be in place for it to have any power,

48
00:04:41,520 --> 00:04:47,960
but the point is that there are many factors that can cause it to have power, obviously,

49
00:04:47,960 --> 00:04:51,520
if you turn on the tape recording, this is what they do here in Sri Lanka, there was

50
00:04:51,520 --> 00:04:55,680
a man here who was really, really critical of this, he said, listen to these people, they

51
00:04:55,680 --> 00:05:00,960
just turn on this chanting and expect that they're, they're, they don't even listen

52
00:05:00,960 --> 00:05:03,720
to it, they turn on their chanting and then they go about their business and he said,

53
00:05:03,720 --> 00:05:09,960
everywhere you hear these people with this chanting on, but they don't actually listen

54
00:05:09,960 --> 00:05:15,080
to it and they don't actually, they're not actually being mindful when they do it,

55
00:05:15,080 --> 00:05:19,960
but the idea is that the sound is somehow going to have some kind of power, now if you

56
00:05:19,960 --> 00:05:27,000
have that kind of idea, I think still there can be a physical power to the, the vibration

57
00:05:27,000 --> 00:05:32,240
of the voice and so on, but very limited compared to the power of the mind, if you take

58
00:05:32,240 --> 00:05:39,520
one minute to listen to it and consider it and give rise to faith in your mind, the power

59
00:05:39,520 --> 00:05:45,360
of that can be exceptional, can change your life, it could even make you bring you enlightenment

60
00:05:45,360 --> 00:05:51,280
if used in the right way, and the, and the same goes for the person chanting, if it's

61
00:05:51,280 --> 00:05:57,280
actually, actually a person chanting and they make a determination, then a lot of great

62
00:05:57,280 --> 00:06:01,640
things can happen, there are even, and I would go even further to say it's not just the

63
00:06:01,640 --> 00:06:05,640
Buddha's teaching, the reason why we use the Buddha's teaching is because it's such powerful

64
00:06:05,640 --> 00:06:11,560
words, it's, it's enunciated very powerfully because of course the Buddha was, was perfectly

65
00:06:11,560 --> 00:06:17,960
enlightened, so his, his manner of speech was perfect, and then the teachings, the meaning

66
00:06:17,960 --> 00:06:25,080
of it, this is what we call the Ata and the, the Ata and Biangina, Biangina is the words

67
00:06:25,080 --> 00:06:33,160
and Ata is the, is the meaning, so the words were wonderful and the meaning is also wonderful

68
00:06:33,160 --> 00:06:40,720
and these, these two things make the Buddha's teaching incredibly powerful, but even just

69
00:06:40,720 --> 00:06:46,560
telling the truth, there's stories in, in the Jatakas about, there's a story of this

70
00:06:46,560 --> 00:06:58,080
kid who, it's this boy who was bitten by a snake and he was lying unconscious and so

71
00:06:58,080 --> 00:07:02,000
they, they also, what do we do? And they said there were three, there was the mother,

72
00:07:02,000 --> 00:07:06,720
the father and the hermit, this hermit, it was I think the Bodhisatta I can't remember

73
00:07:06,720 --> 00:07:11,840
and the Bodhisatta suggests that they all tell the truth, tell, say something truthful

74
00:07:11,840 --> 00:07:18,720
and so the wife says, truth is we've lived together for 50 years and I never love to, or

75
00:07:18,720 --> 00:07:25,280
something like that, and suddenly the kid rolls over and they're like, oh it's working and

76
00:07:25,280 --> 00:07:30,080
then the husband says, well the truth is I've been unfaithful to you this whole time and

77
00:07:30,080 --> 00:07:36,080
cheating on you with another woman and the kid sits up all of a sudden and the Bodhisatta

78
00:07:36,080 --> 00:07:40,160
or the hermit says, well the truth is I've been living as a monk and I'm totally unsatisfied

79
00:07:40,160 --> 00:07:44,320
and I'm always thinking that I just wanted this rope and go back to the home life but because

80
00:07:44,320 --> 00:07:50,320
I don't have any skills in the world I don't do it and suddenly the kid opens his eyes and

81
00:07:50,320 --> 00:07:58,480
is totally cured. So it's a funny story, it's worth reading but that concept I think has some

82
00:07:58,480 --> 00:08:03,120
merit, the idea of just telling the truth because the power of the truth, this is why the

83
00:08:03,120 --> 00:08:10,800
Buddha's teaching is so powerful, it has a power, a mental, a psychic power that can straighten

84
00:08:10,800 --> 00:08:17,120
out, I don't know that it can cure snakebite but certainly has some sort of power.

85
00:08:18,000 --> 00:08:28,720
I remember that we went a couple of months ago to hospital where the father of one of

86
00:08:28,720 --> 00:08:39,840
Buntas friends was sick quite severely and he was sleeping in under medication and

87
00:08:41,520 --> 00:08:50,640
kind of not very conscious at that time and the daughter, the friend invited us to do chanting

88
00:08:50,640 --> 00:08:57,760
there to do some paritas and we went there and before that I really thought kind of like

89
00:08:57,760 --> 00:09:06,960
Nelson so this sounds very superstitious, it's kind of nice and I kind of believe that there is

90
00:09:06,960 --> 00:09:18,560
something in but I had no proof of any real benefit of a power that I never saw it working.

91
00:09:18,560 --> 00:09:27,200
I never saw what it can do and when we were sitting next to the man who was

92
00:09:27,200 --> 00:09:39,520
in pain and agony there in this bed and he was on life support machines and there was a heart rate

93
00:09:41,760 --> 00:09:50,800
meter and his heart rate was very high, I don't understand about it but I read something

94
00:09:50,800 --> 00:10:03,040
about 32 in the beginning and then we started chanting the first parita and within the first

95
00:10:03,040 --> 00:10:15,840
parita the heart rate started to slow down and the man really stopped winding in his bed and

96
00:10:15,840 --> 00:10:27,840
the breath slowed down and I saw then maybe in the second parita I saw that the number on

97
00:10:27,840 --> 00:10:36,240
the heart rate count went down to 18 whatever that means I saw the number 18 and he was really

98
00:10:36,240 --> 00:10:46,480
tranquil at that time and then in the end when we were about to leave when we stopped it went up

99
00:10:46,480 --> 00:10:57,200
a little bit to 20 the last number I saw when we left was 22 so for me that was really

100
00:10:57,200 --> 00:11:16,640
a proof that the words the chanting does work and I think that it comes from the intentions

101
00:11:16,640 --> 00:11:28,080
more than from understanding the words probably from the intentions I mean I could say

102
00:11:29,760 --> 00:11:37,680
something that sounds weird or even evil to some people but if I say it

103
00:11:37,680 --> 00:11:55,280
with a loving heart if you for example but if you say you say to a dog something

104
00:11:57,440 --> 00:12:07,440
a bad name for example you tell the dog bad names but do it with a heartful of love

105
00:12:07,440 --> 00:12:16,480
for this dog it's not important what the words of for this dog the word is important and what

106
00:12:16,480 --> 00:12:23,760
what gets transported is the love that you have for the dog so the dog won't mind if you call it

107
00:12:23,760 --> 00:12:36,000
an idiot if you say it really loving yes but but one one one but there is the meaning because you

108
00:12:36,000 --> 00:12:41,920
will know and for a dog of course they love you and that was the sound of your voice and you can

109
00:12:41,920 --> 00:12:50,880
be loving but no I just think that there is another level beyond the because this is the same

110
00:12:50,880 --> 00:12:57,520
level as the faith that what I thought at first was this man was this is only because this man

111
00:12:57,520 --> 00:13:03,040
was Buddhist for example or it's only because he had he had this thought arise oh they're

112
00:13:03,040 --> 00:13:08,960
chanting for me right and so as a result he became reassured but we found out later that he was on

113
00:13:08,960 --> 00:13:14,560
medication he was drunk so it's like oh and then I thought wow that was useless and because he

114
00:13:14,560 --> 00:13:18,400
didn't and he didn't they didn't have a clue what we were saying and then Paul and he said this

115
00:13:18,400 --> 00:13:28,080
you noticed in the meter so the you know really the vibrations and so on but the truth of what

116
00:13:28,080 --> 00:13:33,680
you're saying I really think that there is some psychic powers I was trying to give these examples

117
00:13:33,680 --> 00:13:42,560
that there is another level that can't be gained from that that couldn't you I would say

118
00:13:42,560 --> 00:13:49,040
couldn't even be gained from from other religious teachings like praising God and so on they say

119
00:13:49,040 --> 00:13:56,720
you can heal heal by by praising God or by calling invoking Jesus you can heal other people's

120
00:13:56,720 --> 00:14:02,320
wounds and so on but I think the Buddha's teaching has has a greater power because it's actually

121
00:14:02,320 --> 00:14:11,040
the truth hey as opposed to at the religion well it's opposed to things like God which we

122
00:14:11,040 --> 00:14:16,480
understand to be not true

