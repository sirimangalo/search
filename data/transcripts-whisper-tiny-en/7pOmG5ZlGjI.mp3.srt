1
00:00:00,000 --> 00:00:11,000
During meditation, I often notice myself thinking about memories or events that I have happened or things I have to do or want to do.

2
00:00:12,000 --> 00:00:15,000
Is it best to pursue the thoughts while acknowledging them?

3
00:00:15,000 --> 00:00:30,000
You don't gain so much, even if it's something that you have to do, you really don't gain much from thinking about it.

4
00:00:33,000 --> 00:00:40,000
It's best not to pursue the thoughts. It's best to let the thoughts come and then be mindful of them. That's the best you can do.

5
00:00:40,000 --> 00:00:57,000
It will change your life, it will change your course, it will change the course of your work and it will help you in many ways.

6
00:00:57,000 --> 00:01:12,000
Certainly, the things that have happened before, there's not much benefit that comes from course, there's very little benefit and much potential detriment to thinking too much about them.

7
00:01:13,000 --> 00:01:20,000
So it's certainly best not to pursue the thoughts and not much benefit ever comes from pursuing thoughts.

8
00:01:20,000 --> 00:01:35,000
Thoughts are best left alone, left to come up, acknowledged and let them go without allowing and craving and desire to continue.

9
00:01:35,000 --> 00:01:59,000
Even in a work sense, I don't see much benefit that comes from thinking and planning and so on. It may seem that sometimes you do have to plan, but the best work doesn't come from planning, it comes from mindful processing, so letting the thoughts come up and then acting on them or writing them down and working them out.

10
00:01:59,000 --> 00:02:06,000
And they should just come by themselves without any pursuit or any worrying or any cultivated.

