1
00:00:00,000 --> 00:00:19,180
So, in paragraph 40, what is originated by temperature and it is 54 and in the manual of

2
00:00:19,180 --> 00:00:29,000
a bit of a only 13 are mentioned without growth and continuity. And then what is originated

3
00:00:29,000 --> 00:00:34,740
by a neutron man that is temperature as its condition, with temperature originated in

4
00:00:34,740 --> 00:00:39,860
a neutrative essence that has reached three nations, originates a further octet with

5
00:00:39,860 --> 00:00:51,620
a neutrative essence as state and so on. Now, did you wonder why the words that has reached

6
00:00:51,620 --> 00:01:02,000
a presence that has reached presence are mentioned again and again? Now, when consciousness

7
00:01:02,000 --> 00:01:09,320
of mind produces matter, it produces at the arising moment because it is said to be strong

8
00:01:09,320 --> 00:01:18,540
at the arising moment. But, matter is strong only at the presence moment. And that

9
00:01:18,540 --> 00:01:25,300
is why it is repeatedly said that has reached presence, that has reached presence and

10
00:01:25,300 --> 00:01:37,180
so on. So, the matter can produce matter only when it reaches the stage of presence,

11
00:01:37,180 --> 00:01:47,500
not at the arising moment or not at the solution moment, but only when it is presence.

12
00:01:47,500 --> 00:02:08,740
Now, comprehension of the immaterial. So, after comprehending of the remaining of the ruba,

13
00:02:08,740 --> 00:02:19,380
so just as one who is comprehending the material should see the generation of the material,

14
00:02:19,380 --> 00:02:23,980
so do one who is comprehending the immaterial should see the generation of the immaterial.

15
00:02:23,980 --> 00:02:29,300
That is through the 81 mundane arising of consciousness. That is to say, it is by camera

16
00:02:29,300 --> 00:02:39,020
accumulated in a previous becoming that this material mentality is generated and so on. So,

17
00:02:39,020 --> 00:02:52,020
when the arising of mentality is observed, then it should be observed by we of 81 mundane

18
00:02:52,020 --> 00:02:58,380
types of consciousness. There are 89 types of consciousness and 8 belong to Superman

19
00:02:58,380 --> 00:03:04,820
day. And Supermaning consciousness are not the object of Yipasana meditation. So, the only

20
00:03:04,820 --> 00:03:14,180
the mundane consciousness is given here. The most in which it is generated should be understood

21
00:03:14,180 --> 00:03:18,420
according to the matter given in the description of the dependent origin nation. So, in that

22
00:03:18,420 --> 00:03:28,460
chapter, how the main arises is given. That is saying, 19 full arising of consciousness

23
00:03:28,460 --> 00:03:34,620
is generated as like continuum as well and so on. Now, among you, that is 89 types of

24
00:03:34,620 --> 00:03:44,380
consciousness. There are 19 types of consciousness, which arise as linking life

25
00:03:44,380 --> 00:03:57,020
continuum and dead consciousness. So, these 9ingra mentioned here. In the course of an

26
00:03:57,020 --> 00:04:02,300
existence, eye consciousness together with its associated states supported by light and

27
00:04:02,300 --> 00:04:08,060
cost by attention is generated because the eye is contact and intact and because visible

28
00:04:08,060 --> 00:04:17,380
data has come into focus. Now, paragraph 44, right? So, here, we get the 4 conditions

29
00:04:17,380 --> 00:04:24,420
for the seeing consciousness to arise. Now, in the course of an existence, that is after

30
00:04:24,420 --> 00:04:30,260
birth, I mean after relating. Eye consciousness together with its associated states that

31
00:04:30,260 --> 00:04:37,460
means together with the mental factors supported by light. So, there must be light

32
00:04:37,460 --> 00:04:42,780
and cost by attention, there must be attention. It is generated because the eye is

33
00:04:42,780 --> 00:04:48,260
contact, there must be the eye and the eye is intact in between, there must be the eye

34
00:04:48,260 --> 00:04:54,420
and because visible data has come into focus. So, there must be visible data. So, only

35
00:04:54,420 --> 00:05:00,060
when these 4 conditions are fulfilled, you can see in consciousness arise. So, we must

36
00:05:00,060 --> 00:05:05,900
have the eye, there must be something to see, there must be lives and there must be attention.

37
00:05:05,900 --> 00:05:18,180
And then, there is the explanation of something like a thought process. Next to arises

38
00:05:18,180 --> 00:05:22,300
is a functional management with that same object accomplishing the function of advancing

39
00:05:22,300 --> 00:05:37,900
a so on. So, this is the explanation of the thought process. The bottom of the page,

40
00:05:37,900 --> 00:05:46,860
we have some correction to make. Kind of consciousness belonging to the sense fear, either

41
00:05:46,860 --> 00:05:56,460
as consciousness accompanied by a conformity and without root cause or as 5 or 7 incursions.

42
00:05:56,460 --> 00:06:03,180
Even even the text in the visual demother is misleading, a little misleading, it is difficult

43
00:06:03,180 --> 00:06:15,020
to understand. Now, what is meant here is, say, our function next, it is generated either

44
00:06:15,020 --> 00:06:20,700
as one from among the profitable and profitable or functional kinds of consciousness belonging

45
00:06:20,700 --> 00:06:32,380
to the sense fear. S, 5 or 7 incursions, we should put there. S, 5 or 7 incursions. So,

46
00:06:32,380 --> 00:06:46,300
after the word sphere, we should say, S, 5 or 7 incursions. And then, the word either

47
00:06:46,300 --> 00:06:57,580
it should be deleted. Yes, instead of it, we should say all. So, all, S consciousness accompanied

48
00:06:57,580 --> 00:07:05,500
by an acronym indeed and without root cause. Just there. And the other words are to be deleted

49
00:07:05,500 --> 00:07:19,980
or S, 5 or 7 incursions. You know, 5 or 7 incursions, right? Normally, 7 joulers arise. But

50
00:07:19,980 --> 00:07:26,140
in some cases, when a person has painted or something, there are only 5 moments of joulin.

51
00:07:26,140 --> 00:07:34,620
So, sometimes, 5, sometimes, most of the time, 7. So, 5 or 7 incursions. But sometimes,

52
00:07:34,620 --> 00:07:48,060
the object is so weak that it cannot be a condition for jouliner to arise. But that is also

53
00:07:48,060 --> 00:07:57,740
called a thought process. So, there are thought processes without the arising of jouliners. And

54
00:07:57,740 --> 00:08:04,620
while it says consciousness accompanied by anonymity without root cause, the other means there.

55
00:08:04,620 --> 00:08:11,820
That is why in the footnote, it is said this refers to determining. Now, any thought process,

56
00:08:11,820 --> 00:08:23,660
there is the accepting, receiving, investigating, determining and jouliners, right? Now, some,

57
00:08:23,660 --> 00:08:31,580
some thought process that can stop just after determining. They do not contain any jouliner

58
00:08:31,580 --> 00:08:41,820
moments. So, the other is referring to that, when it says the consciousness accompanied by

59
00:08:41,820 --> 00:08:46,700
equanimity and without root cause. That means, I am not or I am watching that or what happened.

60
00:08:46,700 --> 00:09:04,460
So, not all or not every thought process contains jouliners. They can be thought processes

61
00:09:04,460 --> 00:09:13,740
without jouliners. Now, material said that. Now, this is how one accomplishes the development

62
00:09:13,740 --> 00:09:19,020
of understanding progressing gradually by comprehending at one time the material and at another time

63
00:09:19,020 --> 00:09:25,740
the in material by attributing the three characteristics to them. So, he should always attribute

64
00:09:25,740 --> 00:09:34,220
three characteristics to what he or the impermanence painful and not self. Another comprehends

65
00:09:34,220 --> 00:09:38,940
formations by attributing the three characteristics to them through the medium of material

66
00:09:38,940 --> 00:09:48,940
sector and the in material sector. So, viewing, viewing matter in seven different ways and viewing

67
00:09:50,620 --> 00:09:57,100
mind in seven different ways. Now, the first is viewing matter in seven different ways

68
00:09:58,140 --> 00:10:05,340
and this is given in paragraph 46, as taking up and putting down as disappearance of what

69
00:10:05,340 --> 00:10:10,540
grows cold in each stage, as arising from mutromance, as arising from temperature, as

70
00:10:10,540 --> 00:10:16,540
comparable, as consciousness already needed and as natural materiality, hence the ancient

71
00:10:16,540 --> 00:10:23,020
set from one, taking up is rebutulating and putting down his death. So, as taking up and putting

72
00:10:23,020 --> 00:10:37,340
down means beginning and the end of one's life, taking one life as one unit, something like that.

73
00:10:37,340 --> 00:10:43,820
So, when he comprehends, according to this matter, he comprehends all formations between

74
00:10:43,820 --> 00:11:04,780
all formations, all nama and ruba begin, relinking and death. So, he, he contemplates on the nama

75
00:11:04,780 --> 00:11:13,420
and ruba from the moment of relinking to death as impermanent and why? Because of occurrence of

76
00:11:13,420 --> 00:11:18,620
price and fall, because of change, because of temporaryness, and because of the inclusion of

77
00:11:18,620 --> 00:11:27,900
permanence. Now, please note these explanations. These are the reasons for taking something

78
00:11:27,900 --> 00:11:34,780
as impermanence. Now, these renama and ruba, he and ruba, these ruba are impermanence because

79
00:11:36,380 --> 00:11:42,220
the occurrence of price and fall, because they have rising and disappearing.

80
00:11:42,220 --> 00:11:48,380
Because of change, because they change, because of temporaryness, they are just temporary,

81
00:11:48,380 --> 00:11:54,700
they don't last long. And because of preclusion of permanence, that means because of rejection

82
00:11:54,700 --> 00:12:02,220
of permanence, they are not permanence. But since a reason for missions of arrived, arrived at

83
00:12:02,220 --> 00:12:08,460
presence and when present, afflicted by aging and arriving and aging, a bond which is solved,

84
00:12:08,460 --> 00:12:14,380
they are therefore painful because of continual oppression, because of being hard to bear,

85
00:12:14,940 --> 00:12:20,620
because of being the basis of suffering and because of precluding pleasure. Now, this is the

86
00:12:20,620 --> 00:12:28,860
four reasons why something is called suffering or pain or dukha. So, what is the first one?

87
00:12:30,860 --> 00:12:37,740
Because of continual oppression, oppression by rising and disappearing, because of being hard to bear,

88
00:12:37,740 --> 00:12:44,540
something painful is hard to bear, because of being the basis of suffering, sometimes it is

89
00:12:44,540 --> 00:12:51,100
the basis for another suffering. And because of precluding pleasure, that means rejection of

90
00:12:52,460 --> 00:12:57,260
sukha, rejecting, saying that it is not sukha, it is sukha, something like that.

91
00:12:57,260 --> 00:13:08,940
And since no one has any power over and risen formations in the three instances, let them not

92
00:13:08,940 --> 00:13:13,580
reach presence, let those that have reached presence not age, and let those that have reached

93
00:13:13,580 --> 00:13:19,900
aging not to solve, and they are wide of the possibility of any power being exercised over them,

94
00:13:19,900 --> 00:13:25,980
they are therefore not self, because of wild, because of onalized, because onalized, because

95
00:13:25,980 --> 00:13:32,300
and susceptible to the wielding of power, and because of precluding itself, this is the

96
00:13:32,300 --> 00:13:43,100
four reasons for being another. Now, the first one is quite white, because it is quite,

97
00:13:43,100 --> 00:13:52,060
because onalized, onalized really means a lot less, no, no, no, overlord. The what the

98
00:13:52,060 --> 00:14:01,900
Pali-Watsamika has two meanings, an owner of a property or a lord. So, I think it is better to say

99
00:14:01,900 --> 00:14:12,300
lord here than owner. So, there is no overlord, and because unacceptable to the wielding of power,

100
00:14:12,300 --> 00:14:18,300
now this is also one of the explanations given with regard to the what another.

101
00:14:18,300 --> 00:14:28,620
So, we met one explanation before what? No, no core. So, that is what explanation. Another

102
00:14:28,620 --> 00:14:39,820
explanation is, we cannot exercise any power over it. It is not susceptible to wielding power

103
00:14:39,820 --> 00:14:47,260
over it, so that is another meaning of another. The example as the ligand, we have no

104
00:14:47,260 --> 00:14:55,020
no power over there, that is a meaning. And because of precluding itself, that means rejecting

105
00:14:55,020 --> 00:15:16,220
self. And in the footnote, let me see, on the previous page, I mean page 720, last line,

106
00:15:16,220 --> 00:15:25,100
and since no one has any power over, formations not arisen, just formations in the three instances,

107
00:15:26,300 --> 00:15:36,380
let them not reach presence. Actually, it is, let the formations that have arisen not reach presence,

108
00:15:36,380 --> 00:15:46,700
something like that. Let them reach presence, not age, and let those that have reached

109
00:15:46,700 --> 00:15:55,900
aging, not dissolving, so on. Something like, I want to be young or always young. So, I don't want

110
00:15:55,900 --> 00:16:04,060
to be 60 years of age, 70 years of age, but I have no power over myself. I will go on aging,

111
00:16:04,060 --> 00:16:15,180
they might deal. So, there is no possibility of exercising power over it. So, in the footnote,

112
00:16:15,980 --> 00:16:23,500
no one, not even the blessed one, has such mastery. For it is impossible for anyone to

113
00:16:23,500 --> 00:16:34,620
order the three characteristics. So, please, please, rub up three, just the characteristics, not three.

114
00:16:35,740 --> 00:16:41,580
The presence of human normal power is simply the alteration of a state. Now, what it means is,

115
00:16:43,820 --> 00:16:47,500
even Buddha cannot change the characteristic of something.

116
00:16:47,500 --> 00:16:56,620
Now, mind has the characteristic of bending towards object, right?

117
00:16:57,500 --> 00:17:03,900
Even Buddha cannot change the characteristic of mind. If it is mind, it will always have this

118
00:17:03,900 --> 00:17:13,500
characteristic. Then, what about when some somebody performs super normal power and do something

119
00:17:13,500 --> 00:17:21,580
which is not usual or real. Then, it is the alteration of a state and not the alteration of a

120
00:17:21,580 --> 00:17:33,740
characteristic. That means, he may make himself say, many persons or he may create

121
00:17:35,100 --> 00:17:41,580
likenesses of himself or something like that. So, the order is that the change of a state

122
00:17:41,580 --> 00:17:51,900
can be done by super normal power, but the change of characteristic cannot be done by any power

123
00:17:51,900 --> 00:18:00,220
or any person at all. So, not even the blessed one has such mastery. That means, it has a characteristic

124
00:18:00,220 --> 00:18:08,940
of aging. Then, Buddha cannot make this aging not age, not not not not not get old,

125
00:18:08,940 --> 00:18:14,300
do something like that. And then, because of precluding itself means, because of precluding

126
00:18:14,300 --> 00:18:21,740
itself conceived by those outside the dispensation, by those other than the Buddhist.

127
00:18:21,740 --> 00:18:29,020
For the non-existence in damas of any self, as conceived by outsiders is stated by the words,

128
00:18:29,020 --> 00:18:38,700
because of white, etcetera. Please, please, et cetera, there. Because, white, etcetera. So, there are,

129
00:18:38,700 --> 00:18:48,620
say, white, ownerless, susceptible to the reading of power. But, by this expression, that means,

130
00:18:48,620 --> 00:18:56,620
the last one, precluding itself. By this expression, it is stated that they are not self,

131
00:18:56,620 --> 00:19:01,900
not not there is no self. They are not self, because they have no such nature.

132
00:19:05,820 --> 00:19:15,420
So, the word another, here, has two meanings. There is no other in there, that is one meaning,

133
00:19:15,420 --> 00:19:22,940
right? Let's say, what is it? Rupa is another. That means, there is no other in Rupa,

134
00:19:22,940 --> 00:19:33,500
that is one meaning. And the other meaning is, Rupa is not another. There is no other in it,

135
00:19:34,140 --> 00:19:43,020
that is one meaning. The other meaning is, it is not other. So, the translation should be,

136
00:19:43,020 --> 00:19:59,500
but, by this expression, it is stated that they are not self, because they have no such nature.

137
00:19:59,500 --> 00:20:10,860
Okay, then, the other ones. Having attributed to three characteristics to materiality,

138
00:20:10,860 --> 00:20:17,740
allotted 100 years for the taking up and putting down dust, he next attributes the according to

139
00:20:17,740 --> 00:20:23,980
disappear and so forth, close poems in each stage. Here in disappear and so forth, close

140
00:20:23,980 --> 00:20:29,420
all in each stage, it is a name for the disability and something materiality that has grown all during

141
00:20:29,420 --> 00:20:34,860
a stage of life. The meaning is that he attributes the three characteristics and means of them.

142
00:20:34,860 --> 00:20:42,700
So, he tries to see all of them as impermanence, as painful and as no self. Now, the first one is

143
00:20:43,980 --> 00:20:51,180
dividing 100 years or less into three stages. So, the first is 33 years, the second 34 years,

144
00:20:51,180 --> 00:21:03,980
and the third 33 years. So, the second 34, not 33. And then, the yogi tries to see the impermanence

145
00:21:03,980 --> 00:21:13,180
and others of materiality in these three stages. The materiality occurring in the first stage

146
00:21:13,180 --> 00:21:19,740
ceased there without reaching the middle stage. Therefore, it is impermanence. What is impermanence

147
00:21:19,740 --> 00:21:24,540
painful? What is painful is not self. Also, the materiality occurring in the middle

148
00:21:24,540 --> 00:21:29,980
edge ceased there without reaching the last stage. Therefore, it too is impermanent.

149
00:21:31,660 --> 00:21:37,980
Not it is impermanent too. It too is impermanent, painful and not self.

150
00:21:39,180 --> 00:21:45,180
Also, there is no materiality occurring in the 33 years of the last stage that is capable of

151
00:21:45,180 --> 00:21:55,180
outlasting death. Therefore, that too is impermanent, painful and not self. So, he comprehends in

152
00:21:55,180 --> 00:22:05,180
that way. So, dividing, you know, the lifespan of human beings are said to be taken to be 100 years.

153
00:22:05,180 --> 00:22:12,460
That is why 100 years are divided into three conclusions. And then, a little more subtle

154
00:22:12,460 --> 00:22:29,340
by dividing 100 years into 10 stages, a paragraph 50. If the 10 stages are one, the tender

155
00:22:29,340 --> 00:22:35,500
daggies, the sport daggies, the beauty daggies, the strength daggies, the understanding

156
00:22:35,500 --> 00:22:44,220
daggies, the decline daggies, the studio daggies, the pain daggies, the total daggies and the prone daggies.

157
00:22:44,220 --> 00:23:00,940
You can find out where you are in. So, there is the word prone. What is the meaning of the word prone?

158
00:23:00,940 --> 00:23:08,700
Line down. Line down, face down one or face up one or just line down. Just line down.

159
00:23:08,700 --> 00:23:17,260
Just line down. Line it's all right. But when you get older, when you are more than 90 years old,

160
00:23:18,220 --> 00:23:22,620
you want to be in bed all the time. So, it's called prone daggie.

161
00:23:22,620 --> 00:23:31,980
So, the material, the tea from one decade does not go to another decade and so on. That is how

162
00:23:33,660 --> 00:23:45,100
the TV will be comprehended. And then, what is dosage? Becoming like a child?

163
00:23:45,100 --> 00:23:56,540
Yeah. Yeah, it means it may include that, but the body body is mobile, that means

164
00:23:59,660 --> 00:24:08,140
deluded state or something. He focused on his tongue and so on. The footage is sort of your

165
00:24:08,140 --> 00:24:15,820
mental function becomes weaker and weaker. So, you know, people would usually sleep and fall asleep.

166
00:24:15,820 --> 00:24:18,220
And then, that's all right.

167
00:24:23,660 --> 00:24:28,940
And then, next, about 53. Now, dividing into five,

168
00:24:28,940 --> 00:24:45,820
right, the same 100 years into 20 parts of five years. So, the reality in the first five years

169
00:24:45,820 --> 00:24:52,780
sees there and do not reach to the second five years and so on. So, this

170
00:24:52,780 --> 00:25:00,220
is a group of five years. And then, next, I mean, about a graph of 55, how many years?

171
00:25:04,780 --> 00:25:15,420
Four years. So, 25 parts, dividing 100 into 20 parts, 25 parts, and then, 33 parts, 3 years each,

172
00:25:15,420 --> 00:25:24,460
and then 50 parts, 2 years each, and then taking 1 year each. So, it goes smaller and smaller.

173
00:25:26,060 --> 00:25:37,580
And then, you reviewed, according to the three, three seasons, right? The materiality occurring

174
00:25:37,580 --> 00:25:42,940
at the full month of the rain, seasons, without reaching the winter and so on. Now,

175
00:25:42,940 --> 00:25:51,660
in India, there are said to be three, three seasons, also in other Asian countries,

176
00:25:53,980 --> 00:26:01,100
rain season, winter and summer. So, we have three seasons and they're not full like in the West.

177
00:26:02,700 --> 00:26:08,300
All there are six seasons. So, biography seven, just like these six seasons,

178
00:26:08,300 --> 00:26:20,940
rains, autumn, winter, cool, spring and summer. That means two months of each.

179
00:26:22,780 --> 00:26:33,100
So, materiality from at one season sees there and just that reach to the next season and so on.

180
00:26:33,100 --> 00:26:40,860
And then next, dividing a month into two parts, right? Bright house and dark house,

181
00:26:42,380 --> 00:26:51,340
for a graph of 58. And then next, for a graph of 59, dividing the day into night and day,

182
00:26:51,340 --> 00:27:03,100
they move 24 hours day into night and day. And then 60, dividing a day into six parts. What's that?

183
00:27:06,060 --> 00:27:13,100
Morning, noon, evening, first watch of the night, middle watch of the night and last watch of the night.

184
00:27:13,100 --> 00:27:24,620
And then, actually, during the three characteristics to that same materiality by means of moving forward

185
00:27:24,620 --> 00:27:30,540
and moving backward and looking forward and looking away, bending and stretching, thus.

186
00:27:32,300 --> 00:27:41,340
These, these watch are mentioned in the has a deeper than a soda in the section on clear

187
00:27:41,340 --> 00:27:48,460
comprehension. So, the materiality occurring and then moving forward sees it there without reaching

188
00:27:48,460 --> 00:27:59,420
the moving backward and so on. And then, the next paragraph, 62, deals with six stages in one step.

189
00:28:00,060 --> 00:28:07,100
Now, I, I, I didn't ask you to, to be mental of six stages in one step people,

190
00:28:07,100 --> 00:28:14,060
it's too, too, too many. But here at six stages, I mentioned, so lifting up,

191
00:28:14,700 --> 00:28:21,740
shifting forward, shifting sideways, lowering down, placing down and fixing down. So,

192
00:28:21,740 --> 00:28:25,260
six stages. So, lifting up is raising the foot from the ground,

193
00:28:25,980 --> 00:28:31,340
shifting forward is shifting it to the front. That means moving it to the front.

194
00:28:31,340 --> 00:28:37,340
Shifting sideways is moving the foot to one side or the other on seeing a thong,

195
00:28:37,340 --> 00:28:43,900
stance, neck and so on. It may not have been at every step moving.

196
00:28:45,580 --> 00:28:50,700
Then lowering down is letting the foot down. Placing down is putting the foot on the ground.

197
00:28:51,580 --> 00:28:56,540
Fixing down is pressing the foot on the ground while the other foot is being lifted up.

198
00:28:56,540 --> 00:29:04,300
So, when you want to lift the other foot, then you, you press the, the, the, the,

199
00:29:05,100 --> 00:29:11,820
one foot more summary on the ground. So, that is called fixing down. So, it will be six stages.

200
00:29:11,820 --> 00:29:19,020
So, the, here on the lifting upward, in the lifting up two elements, the other element and the water

201
00:29:19,020 --> 00:29:27,340
element as a body nate and slightly is while the other two are predominant and strong. Now, among

202
00:29:27,340 --> 00:29:32,460
the elements, water element and earth element are said to be heavy.

203
00:29:37,180 --> 00:29:44,940
And fire element and wind element are said to be light. So, when you, when you move upward,

204
00:29:44,940 --> 00:29:55,180
then the, the, the up element is sluggish, not, not predominant at that time, but the wind element

205
00:29:55,180 --> 00:30:01,740
and fire element are predominant. So, when you put down, then the other element is predominant,

206
00:30:02,780 --> 00:30:10,140
but not the higher element like that. So, likewise in the shifting forward and shifting sideways,

207
00:30:10,140 --> 00:30:14,860
in the lowering down two elements, the fire element and the air element as a body nate and slightly

208
00:30:14,860 --> 00:30:19,420
is mildly other two are predominant and strong. Likewise, the placing down and fixing down.

209
00:30:19,980 --> 00:30:25,420
He attributes a three characteristics to materiality according to disappearance of one

210
00:30:25,420 --> 00:30:31,180
close hole in each stage by means of the six parts and the quicks he just divided it. So, while

211
00:30:32,220 --> 00:30:38,860
while materiality in one stage sees the, sees it there and it does not reach to the second stage

212
00:30:38,860 --> 00:30:48,860
and so on. So, he tries to see, or here, he attribute the three characteristics to all these

213
00:30:49,420 --> 00:30:57,340
material properties. So, when you see formation stage by stage with inside that his comprehension

214
00:30:57,340 --> 00:31:04,700
of materiality has become subtle. Here is a similarly for its subtlety and then it's similarly

215
00:31:04,700 --> 00:31:10,220
is given and the application of the similarly to the, to the actual experience is explained

216
00:31:10,220 --> 00:31:24,220
in paragraph 67. And then, materiality arising from mu treatment,

217
00:31:26,460 --> 00:31:32,300
arising from temperature and camera board and consciousness already needed are explained.

218
00:31:32,300 --> 00:31:39,980
So, in whatever way, a yogi contemplates, he contemplates on the

219
00:31:41,660 --> 00:31:50,460
impermanence suffering and no self nature of these things. So, in fact, you see them as impermanent,

220
00:31:50,460 --> 00:32:01,180
as suffering and as no self. So, in many different ways, a yogi may see these during the practice

221
00:32:01,180 --> 00:32:08,220
of meditation, but actually, these are not directly, these are all influential people.

222
00:32:10,140 --> 00:32:21,420
And maybe a little, what do you call, a little thinking or something like that. That is why

223
00:32:21,420 --> 00:32:34,700
Mahazizya did not encourage this kind of practice. Just do it for a while. Don't spend much time

224
00:32:34,700 --> 00:32:42,940
with this type of observing or viewing different different major and physical phenomena

225
00:32:42,940 --> 00:32:48,860
and attributing the three characteristics to them. Because this is something like a

226
00:32:48,860 --> 00:32:58,380
distraction. Because when you are trying to see the impermanence and so on, you are not really

227
00:33:00,300 --> 00:33:05,980
practicing mindfulness. You are going to do something like speculating or thinking.

228
00:33:07,180 --> 00:33:12,460
So, it will take your mind away from the real object of meditation and so

229
00:33:12,460 --> 00:33:24,940
it is not encouraged. And also, a yogi cannot go through all these mentioned in all these methods,

230
00:33:24,940 --> 00:33:31,580
mentioned in this book. Because a yogi must have a good knowledge of a bit of an order to go through

231
00:33:31,580 --> 00:33:37,740
all these. And even though he has knowledge of a bit of a, he will not see all of them during his

232
00:33:37,740 --> 00:33:45,580
practice of meditation. Because some can be seen only by the Buddha and persons are very high

233
00:33:46,220 --> 00:33:54,220
intellectual nature. So, if you do not, during practice of re-person, if you do not experience all

234
00:33:54,220 --> 00:34:03,180
these, don't go there. But it is the book and so it is to explain everything

235
00:34:03,180 --> 00:34:09,260
possible during the practice and detail.

236
00:34:13,580 --> 00:34:24,140
So, in brief, it is just applying the three characteristics to everything we observe.

237
00:34:25,100 --> 00:34:28,540
Everything that we take as an object of meditation.

238
00:34:28,540 --> 00:34:36,700
So, when he discerns consciousness, originated material, and introduced the three characteristics to

239
00:34:36,700 --> 00:34:46,380
it in this way, this meaning becomes evident to him and verse on page 727. Life, person, pressure,

240
00:34:46,380 --> 00:34:53,340
pain, just this alone, join in one conscious moment that flicks by the heavy,

241
00:34:53,340 --> 00:35:02,940
here really means they are associated with one consciousness. But join in one consciousness

242
00:35:02,940 --> 00:35:12,140
means they are associated with only one moment of consciousness. And that moment is very brief.

243
00:35:13,820 --> 00:35:20,540
God though they live for 4 and 80,000 years and not the same for two such moments. Here also,

244
00:35:20,540 --> 00:35:30,380
the original poly means these gods do not exist, they are associated with two consciousness at

245
00:35:30,380 --> 00:35:38,460
a time. That means however long they live, they live for one moment, say one moment at a time,

246
00:35:39,180 --> 00:35:43,020
they cannot live for two moments at a time. That is what is meant here.

247
00:35:43,020 --> 00:35:51,900
Not that they are not the same for two such moments, but they do not exist with two moments of

248
00:35:51,900 --> 00:35:58,460
consciousness, so only one moment. These aggregates of those dead or alive are all alike,

249
00:35:58,460 --> 00:36:04,460
can never to return. And those that break up meanwhile and future have traits no different from

250
00:36:04,460 --> 00:36:11,260
those seeds before, so they have the same characteristics, the same nature of environmental and so on.

251
00:36:11,260 --> 00:36:23,260
No world is born if consciousness is not reduced. Actually no world is born with consciousness which

252
00:36:23,260 --> 00:36:29,260
is not reduced. When there is no consciousness there is no no world living or something like that.

253
00:36:30,460 --> 00:36:35,980
When that is present then it lives, so the world lives with the present consciousness.

254
00:36:35,980 --> 00:36:44,140
When consciousness that dissolves, the world is dead, the higher sense this concept will allow.

255
00:36:44,140 --> 00:36:50,220
Now when consciousness that dissolves, the world is dead means we are dying at every moment,

256
00:36:50,220 --> 00:36:57,820
there is dead at every moment because our consciousness is a very brief, the moment of consciousness

257
00:36:57,820 --> 00:37:06,300
is a very brief, and so at every dissolution of a moment of consciousness we die, so we are always dying,

258
00:37:07,420 --> 00:37:17,580
although we are living. He lives, he lives, a woman lives, I live, you live, but in fact we are always dying

259
00:37:19,420 --> 00:37:27,100
with the dissolution of its brief moment of consciousness. So when consciousness that dissolves,

260
00:37:27,100 --> 00:37:34,060
the world is dead, the higher sense this concept will allow. You know, you know the meaning of that.

261
00:37:38,380 --> 00:37:53,420
As he said in the food note, the PM means the subcometary, subcometary and the single

262
00:37:53,420 --> 00:37:58,860
ist translation has been taken as kind in rendering this rather difficult verse.

263
00:38:01,180 --> 00:38:05,740
Yeah, it is difficult. So here the higher sense this concept will allow means

264
00:38:09,180 --> 00:38:18,140
just when we say he lives, I live and so on, it is just for convenience in speaking that we say

265
00:38:18,140 --> 00:38:28,220
I live, you live, he lives and so on. So the real ultimate truth is that we are dying every moment,

266
00:38:28,220 --> 00:38:39,500
but when we say I live, he lives, we are taking death as though it is ultimate truth, but

267
00:38:39,500 --> 00:38:59,340
equally it is not. So although it is not ultimate truth, it is like ultimate truth. So that is what

268
00:38:59,340 --> 00:39:09,340
the higher sense means that the ultimate truth. So when we say when we use the expression

269
00:39:10,300 --> 00:39:19,660
he lives or I live or you live, that is we are talking on a conventional level, but we take

270
00:39:19,660 --> 00:39:29,180
that conventional level to be a real truth, actually it is not. No stew of broken states, no

271
00:39:29,180 --> 00:39:37,340
future stock. So there are no states in the first stone and no future stock, those born

272
00:39:37,340 --> 00:39:43,340
balance like seeds on needle points. So we see it put on a needle point immediately forms,

273
00:39:44,380 --> 00:39:50,380
break up of stated food blooms at their birth, those present decays and mingles with those past.

274
00:39:51,100 --> 00:39:57,100
They come from nowhere, break up, nowhere go, flesh in and out, as lightning in the sky.

275
00:39:57,100 --> 00:40:05,660
Everything just arises and disappears. So if you do not last long, having attributed

276
00:40:05,660 --> 00:40:10,060
to Zika logistics to death arising from neutral medicine for a year again, attributed to

277
00:40:10,060 --> 00:40:17,420
Zika logistics to natural materiality and natural materiality here means what? Outside things.

278
00:40:17,420 --> 00:40:25,980
Materiality, that is not bound up with faculties and that is not bound up with faculties

279
00:40:25,980 --> 00:40:37,740
mean that do not belong to living beings and arises along with the end of world expansion.

280
00:40:37,740 --> 00:40:52,780
You know that the end of world expansion. Do you remember the beginning of the world and the

281
00:40:52,780 --> 00:40:59,900
dissolution of the world cycle? So there are four periods in the world. So the end of world

282
00:40:59,900 --> 00:41:11,580
expansion means the beginning, the formation stage of the world. So the world

283
00:41:15,180 --> 00:41:23,100
just integrates and then stays in that stage for another period and then there is the

284
00:41:23,100 --> 00:41:33,820
rising period and so that period is called here, the end of world expansion. That means from

285
00:41:33,820 --> 00:41:46,940
let's say from the beginning of the world and they are what? Or you can go back to 13th chapter

286
00:41:46,940 --> 00:41:50,780
for a graph 28.

287
00:41:55,420 --> 00:42:08,460
When you see them, therefore not in this group, that is in the first, the first volume.

288
00:42:08,460 --> 00:42:17,980
So there are outside things like iron, copper, tin, lead, gold, silver, pearl gem and so on.

289
00:42:20,700 --> 00:42:26,460
That becomes evident in by means of an also country shoot. For that we begin with this

290
00:42:26,460 --> 00:42:32,940
clear thing and then in two or three days it becomes 10th red and so on. So from one stage to another,

291
00:42:32,940 --> 00:42:41,740
nothing is carried over or nothing reaches to another stage. So at the very stage of becoming

292
00:42:42,460 --> 00:42:48,300
it is all. So after it has become the color of that green leaves as it follows all the successive

293
00:42:48,300 --> 00:42:54,140
stages of such material continuity, it eventually becomes without foliage and at the end of the year

294
00:42:54,140 --> 00:43:00,860
it breaks due from its stem and force all. Descending that it gives a three characteristics to

295
00:43:00,860 --> 00:43:06,380
it thus. The materiality occurring when it is clear thing ceases there without reaching the time

296
00:43:06,380 --> 00:43:14,940
when it is dense red and so on. So this is how the yogi comprehends the formations

297
00:43:16,700 --> 00:43:24,300
by attributing the three characteristics by means of the material symptoms. So in seven different ways

298
00:43:24,300 --> 00:43:31,740
when he viewed the matter of material poverty as in permanence and so on.

299
00:43:34,620 --> 00:43:47,420
And in the party the word usual attributing is putting on. So to see the the individual characteristics

300
00:43:47,420 --> 00:43:57,180
is the real thing. And to see the the environment and so on is something we put on the

301
00:43:57,180 --> 00:44:05,820
characteristics. I mean we put on the damas. So that is why putting on where it set in the books.

302
00:44:05,820 --> 00:44:21,660
So here it is translated as executed. On page 719 and there is one foot note. When the generation

303
00:44:21,660 --> 00:44:31,340
of lativity is seen its dissolution also is seen. And so he said one who loses the generation

304
00:44:31,340 --> 00:44:37,020
of materiality does is set to comprehend the materiality at one time because of the

305
00:44:37,020 --> 00:44:45,900
brevity of states or currents. Now that phrase should go after a scene after the at the end of the

306
00:44:45,900 --> 00:44:52,300
first line. Well the generation of materiality is seen its dissolution is also seen because of the

307
00:44:52,300 --> 00:44:59,740
brevity of states or currents. Because the the horizon disappears so rapidly when you see the

308
00:44:59,740 --> 00:45:05,660
arriving you will not feel to see the solution also because they are so brief.

309
00:45:07,820 --> 00:45:13,660
For it is not the scene of mere generation that is called comprehension but there must be

310
00:45:13,660 --> 00:45:19,580
seeing of the horizon fall besides. So to the every having of generation and other instances.

311
00:45:19,580 --> 00:45:30,620
So seeing of mere generation mere arising is not called comprehension mere. So in order to be

312
00:45:30,620 --> 00:45:54,860
called comprehension is it must be put arising and disappear in one voice and form.

