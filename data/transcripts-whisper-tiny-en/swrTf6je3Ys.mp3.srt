1
00:00:00,000 --> 00:00:18,160
Okay. Good evening, everyone. We are broadcasting live, and the sound is going out.

2
00:00:18,160 --> 00:00:34,920
I can almost guarantee it. Tonight's quote, I'm curious, because the source doesn't have this

3
00:00:34,920 --> 00:00:40,920
part where it says they are of great help to one who has become human. So I'm somewhat inclined

4
00:00:40,920 --> 00:00:49,880
to assume that the author or the translator has added that passage in completely, which would

5
00:00:49,880 --> 00:00:56,040
be kind of shameful if he just goes about adding stuff in. Maybe that it exists somewhere

6
00:00:56,040 --> 00:01:03,720
else, because it wasn't the author, the translator, who found these quotes. So the person who

7
00:01:03,720 --> 00:01:09,640
found these quotes is a friend of mine. He's a Buddhist monk, spent a lot of time finding

8
00:01:09,640 --> 00:01:16,640
painstakingly each of these quotes. So he may have found it in the wrong place, anyway.

9
00:01:16,640 --> 00:01:27,080
Let's see if it is the right quote. Yep. It's at least the four-right dhamma. So the

10
00:01:27,080 --> 00:01:36,520
Buddhist has chata rumi bhikavidhamma. There are these four dhamma's monks. The Buddha was,

11
00:01:36,520 --> 00:01:45,280
this was a common way for him to teach. If you think this isn't an actual thing that the

12
00:01:45,280 --> 00:01:51,680
Buddha probably didn't teach this way, who would teach this way? A lot of monks do teach

13
00:01:51,680 --> 00:01:57,400
according to this tradition. It works really well. My teacher always does this. He basically

14
00:01:57,400 --> 00:02:05,560
says, we'll talk for five minutes about four things, one in the second. Pretty much exactly

15
00:02:05,560 --> 00:02:13,240
how the Buddha does it here. But then, as the Buddha would do in other places, he says,

16
00:02:13,240 --> 00:02:20,840
where is this? He talks about them in detail, one by one by one. Which is a way I go through

17
00:02:20,840 --> 00:02:29,400
these myths way as well. So there are these four dhammas. What are these dhammas? Four

18
00:02:29,400 --> 00:02:39,880
panya vudya sambhatanti. They exist or they lead to the increase in wisdom, growth in wisdom.

19
00:02:40,840 --> 00:02:49,720
They cause someone to grow in wisdom. And that's all it says, katami tataro, which four? Excuse me.

20
00:02:49,720 --> 00:03:03,880
Sapurisa sambhurisa sambhurisa sambhurisa sambhur, association with sapurisa, good people, good fellows.

21
00:03:06,120 --> 00:03:11,160
Sandam sambhana listening to the dhamma, listening to the good dhamma.

22
00:03:11,160 --> 00:03:29,320
You only sow in a sikarum, wise attention or wise mental activity, cultivating wisdom in the mind

23
00:03:29,320 --> 00:03:35,960
or paying wise attention. Dhamma and number four, dhamma nudhamma patipati,

24
00:03:35,960 --> 00:03:43,000
practicing the dhamma in order to realize the dhamma.

25
00:03:48,440 --> 00:03:55,320
It's an interesting word, dhamma nudhamma patipati. First of all, patipati, according to the tradition,

26
00:03:57,320 --> 00:04:04,280
means, and there's this one teacher in Bangkok, he's passed away, but he was very smart and wise

27
00:04:04,280 --> 00:04:10,840
and I'm pretty awesome meditation teacher too. And he said, he always translated this word,

28
00:04:10,840 --> 00:04:18,840
patipati. Patipati means specifically, and the first patipati means specifically,

29
00:04:18,840 --> 00:04:26,440
patipati, the second part means to attain. So patipati means an activity by which

30
00:04:26,440 --> 00:04:38,760
you are specifically going for a goal. So your intention is focus on a goal. That's what patipati

31
00:04:38,760 --> 00:04:44,600
patipati means, practice, which translates, but it has a bit of a deeper meaning in terms of

32
00:04:45,960 --> 00:04:53,080
something that you're doing for a reason. So what are we doing? We're practicing the dhamma,

33
00:04:53,080 --> 00:05:05,720
dhamma nudhamma, which means practicing the dhamma, specific focus on dhamma. So just focus on the

34
00:05:05,720 --> 00:05:12,120
five aggregates, focus on the six senses, focus on the hindrances, focus on the faculty,

35
00:05:12,120 --> 00:05:16,200
focus on the noble truths and the full noble path and all these good dhammas.

36
00:05:17,160 --> 00:05:20,920
Why are we doing it? What are we hoping to attain? What is this patipati?

37
00:05:20,920 --> 00:05:26,920
What are we doing? Patipati, in order to patipati, in order to attain,

38
00:05:28,360 --> 00:05:33,480
patipati means we're specifically practicing the dhamma. Patipati means to attain the dhamma.

39
00:05:34,440 --> 00:05:43,320
It's a very interesting word, self-referential, but the first dhamma, the second in terms of the

40
00:05:43,320 --> 00:05:51,960
explanation, but the first in the word refers to the truth. It's the higher dhamma, the result.

41
00:05:51,960 --> 00:05:59,000
So dhamma is in two parts. There's the pubangamanga, or there's the

42
00:05:59,000 --> 00:06:11,720
the practice, the kusala dhamma, and then there is the vipaka dhamma, or there is more than vipaka,

43
00:06:11,720 --> 00:06:21,640
even there is the the super mundane result. I guess which is vipaka in the sense.

44
00:06:21,640 --> 00:06:28,920
There's the super mundane vipaka, I think. It's called vipaka. Anyway, there's the attainment

45
00:06:28,920 --> 00:06:34,600
of nibanda, which is the the the patipati, which we're what we're trying to gain.

46
00:06:37,560 --> 00:06:43,560
So these are the four imiko bikouet at dhamma, dhamma, bhanya, vudya, samatanti. These four dhammas

47
00:06:44,200 --> 00:06:50,520
lead to a growth in wisdom. Pretty simple. What's neat about these kind of lists is you can just

48
00:06:50,520 --> 00:06:55,720
remember these four things, and it gives you a framework. You've got a framework by which to

49
00:06:55,720 --> 00:07:02,360
practice. So it's the first one associated with good people. Don't listen to people who are

50
00:07:03,480 --> 00:07:10,120
saying bad things, saying harmful things that lead you to to your detriment.

51
00:07:12,360 --> 00:07:18,920
It's same Patrick's day coming up soon. And I know this because at university there is a

52
00:07:18,920 --> 00:07:24,200
table set up by the wellness center. This wellness center at McMaster is just awesome.

53
00:07:25,000 --> 00:07:29,080
They should go visit them again. They're coming to our piece of symposium, but they're just neat

54
00:07:29,080 --> 00:07:34,440
people. So they're having they had this table set up and I stopped to ask what's this all about.

55
00:07:35,000 --> 00:07:40,520
It was all alcohol, paraphernalia and stuff. And the person manning it said to me

56
00:07:40,520 --> 00:07:51,000
we're trying to to talk to people about drinking alcohol. And she said

57
00:07:52,920 --> 00:07:56,280
I get first she was sort of beating around, but she said we're you know we're trying to

58
00:07:56,280 --> 00:08:01,080
teach them how to consume alcohol and moderation. And she said kind of apologetically

59
00:08:01,080 --> 00:08:04,840
because we can't you know we can't tell them not to drink alcohol. And I wanted to ask her

60
00:08:04,840 --> 00:08:10,840
do you drink alcohol? Because from the sounds of it she probably didn't. There are lots of people

61
00:08:10,840 --> 00:08:17,960
out there who just don't drink alcohol for the cultural or religious or or just reasons

62
00:08:19,080 --> 00:08:22,600
from their upbringing just pretty awesome. I wasn't one of those people.

63
00:08:24,120 --> 00:08:29,480
I believe. So I was on the other side. I was trying to convince my roommate and all my friends

64
00:08:29,480 --> 00:08:35,480
to drink with me and to drugs and all that stuff. Very bad karma.

65
00:08:40,680 --> 00:08:48,440
So yeah don't hang out with people like me if kind of person I was. I think I'm sufficiently

66
00:08:48,440 --> 00:08:54,280
or substantially changed. I think some people would still not consider that.

67
00:08:54,280 --> 00:09:01,320
Consider me a while there's takes all kinds right. But hang out with people and listen to

68
00:09:01,320 --> 00:09:06,840
people and talk with people and practice with people. You consider to be good fellas.

69
00:09:07,560 --> 00:09:16,840
Some police in people who are on a good path. People who when who appreciate and encourage

70
00:09:16,840 --> 00:09:28,440
and work towards good things. Hang out with those people. So see this leads to an increase

71
00:09:28,440 --> 00:09:33,240
in wisdom because you're going to hear wise things and because you're going to practice good

72
00:09:33,240 --> 00:09:39,880
things and that's going to make you wiser and make you lead you to understand better understanding

73
00:09:39,880 --> 00:09:48,120
better wisdom. Number one, number two, Saddam Hussein. So don't just hang out with such people

74
00:09:48,120 --> 00:09:53,240
listen to what they have to say. Listen to the Buddha's teaching or read the Buddha's teaching.

75
00:09:54,600 --> 00:10:03,240
Listen to talks on the dhamma and so on. Of course this leads to wisdom. Not only does it lead

76
00:10:03,240 --> 00:10:11,800
to information. It also leads you to thinking different ways. So it leads to gintamaya. You start to

77
00:10:11,800 --> 00:10:17,880
think about things you never thought about before. Oh yeah. It's like that. Never thought of that

78
00:10:17,880 --> 00:10:25,480
before. And then finally of course it leads you to change the way you see the world which is

79
00:10:25,480 --> 00:10:33,240
bhava namaya. It leads you to practice meditation and just in general leads you to see things

80
00:10:33,240 --> 00:10:40,600
differently. It leads to a different outlook. It leads to a real visceral change in the way you

81
00:10:40,600 --> 00:10:49,000
understand the world. That's bhava namaya. So Saddam Hussein listening to the dhamma. That's a good

82
00:10:49,000 --> 00:11:01,480
way to cultivate wisdom. Number two, number three, I don't think I'm happy with this translation.

83
00:11:02,360 --> 00:11:08,360
Oh no. Why is attention that's good? Yeah. So why is attention I've talked about this for

84
00:11:08,360 --> 00:11:19,240
yoni so means to the womb. Yoni is womb like a woman's womb where the baby is born. But what it

85
00:11:19,240 --> 00:11:26,280
means is getting back to the root, get back to the source. Yoni can also just mean source. So

86
00:11:26,280 --> 00:11:34,280
yoni so means to the source. And it's just an idiomatic way of saying with wisdom. But in the context

87
00:11:34,280 --> 00:11:40,280
of meditation it's quite interesting. So yoni so means to the source. Manasim means in the mind

88
00:11:40,280 --> 00:11:50,520
gara means to make or to hold, to make, to form your kind of kind of kind of just karma just

89
00:11:50,520 --> 00:12:00,440
means action. So to act in such a way or to make something be in the mind to the source which is

90
00:12:00,440 --> 00:12:05,560
very very interesting from the point of view of insight meditation where we where we do this right

91
00:12:05,560 --> 00:12:11,000
where when you when you focus on pain and you say pain, pain, you're keeping that thing in mind

92
00:12:11,000 --> 00:12:19,720
or you're you're you're you're establishing it in your mind at the source. So only the source

93
00:12:20,600 --> 00:12:27,640
you're not worried about is it good pain, bad pain, problem pain, my pain. Did you cause this pain?

94
00:12:27,640 --> 00:12:33,400
That kind of thing. All we know is the source. We get to the source and we see it just as pain

95
00:12:35,000 --> 00:12:41,960
which is of course is described elsewhere. But this word itself is very interesting. So

96
00:12:41,960 --> 00:12:47,160
proper attention. It can also refer to just proper attention when you're listening to the

97
00:12:47,160 --> 00:12:52,600
dhamma. It has worldly definitions as well. Like when you're listening to the dhamma you should

98
00:12:52,600 --> 00:12:57,560
pay attention. Keep it in mind with wisdom or reflect wisely on what's being said. A lot of people

99
00:12:57,560 --> 00:13:02,760
interpret it that way. But if you look at the texts, it's really not that's not enough. You only

100
00:13:02,760 --> 00:13:10,680
so many sikara has to be a meditative state. But you can apply it to thinking wisely listening wisely.

101
00:13:11,240 --> 00:13:18,200
Paying attention can be thought of just as that. So it refer it it does relate back to the other

102
00:13:18,200 --> 00:13:24,840
one. But more importantly, you should pay attention to the dhamma, the teachings, meaning

103
00:13:25,720 --> 00:13:30,920
the realities that are being taught about. You should pay attention to those realities.

104
00:13:31,880 --> 00:13:41,480
Put the put the teaching to work. But of course, you could also say, well that's fine with

105
00:13:41,480 --> 00:13:46,680
you. I need someone I say guys, just pay attention when you're listening to the dhamma. But number

106
00:13:46,680 --> 00:13:51,400
four is where you actually practice. We've already talked about what this one means, dhamma, dhamma,

107
00:13:51,960 --> 00:14:01,960
party, party. It means practicing specifically the dhamma in order to attain specifically the dhamma.

108
00:14:03,960 --> 00:14:09,560
So practicing for foundations of mindfulness and so on in order to attain

109
00:14:09,560 --> 00:14:20,520
nibhana, super mundane dhamma. This is a teaching. These four lead to wisdom. Wisdom has to come

110
00:14:20,520 --> 00:14:26,040
from practice. You can't have just the first three unless you count the third one as meditation,

111
00:14:26,040 --> 00:14:30,520
but you can't just associate with good people and listen to good things. If you're not

112
00:14:30,520 --> 00:14:36,760
paying attention and practicing the dhamma, wisdom motorized. But these four altogether,

113
00:14:36,760 --> 00:14:41,480
the other hand, if you just practice without listening, it's very easy to get on the wrong path.

114
00:14:41,480 --> 00:14:48,040
And many people, you know, part, a huge part of what I do is just correcting people's practice.

115
00:14:48,040 --> 00:14:54,280
Most people, well, all people who practice until you become enlightened, you're doing it wrong.

116
00:14:55,480 --> 00:15:01,160
You're not doing it perfectly. And that's an important point. It's not just, if I do this

117
00:15:01,160 --> 00:15:05,880
moral, become enlightened, that's not the case. That's not how enlightenment is based on wisdom.

118
00:15:05,880 --> 00:15:12,920
It's not based on work. It's not an effort. Of course, it takes effort to change your view,

119
00:15:12,920 --> 00:15:18,200
but it's not just effort. You have to change your view. You have to look at things differently.

120
00:15:18,200 --> 00:15:26,840
This is where, actually, where you only sow Manasika comes in and the wisdom actually has to

121
00:15:26,840 --> 00:15:32,200
take over in the practice. You have to be clever and wise in this world. We monks that I talk

122
00:15:32,200 --> 00:15:38,280
about often. It's not enough to just keep your attention on something. You have to be discriminating

123
00:15:38,280 --> 00:15:42,520
and discerning and saying, oh, I'm doing this wrong. I'm not paying attention to that.

124
00:15:42,520 --> 00:15:50,040
Oh, I'm not really being mindful here. And meditation is a constant adapting, refining.

125
00:15:50,760 --> 00:15:55,720
That's what it's really all about. And that's what leads to success, not just plowing through it

126
00:15:55,720 --> 00:16:01,160
and saying, I did so many hours a day. You can do lots and lots of hours of walking,

127
00:16:01,160 --> 00:16:07,080
sitting and getting nothing other than. That's possible. If you're not doing it right,

128
00:16:07,960 --> 00:16:10,520
so you have to constantly, and I can't tell you how to do it. You have to,

129
00:16:11,080 --> 00:16:14,760
the teaching is very simple, but you have to look and you have to say, am I doing it the way it

130
00:16:14,760 --> 00:16:18,440
said? And you have to go back to the teaching and say, oh, wait, I'm not actually doing that.

131
00:16:19,640 --> 00:16:23,320
So there's, and you need to teach her, well, it's good to have a teacher who can tell you,

132
00:16:23,320 --> 00:16:30,680
and you have to adjust that. So you have to, you need people, good people, and you need

133
00:16:31,320 --> 00:16:36,440
listen to listen to the dumb mind. You have to keep it in mind. You have to practice it.

134
00:16:37,800 --> 00:16:42,760
It's basically what you think inside here. So that's the dumb enough for tonight.

135
00:16:42,760 --> 00:16:54,120
I'm going to copy paste. Hang out if anybody wants to come on and ask questions.

136
00:16:57,320 --> 00:17:03,160
Do that for a few minutes to see if anybody has questions.

137
00:17:03,160 --> 00:17:19,080
If there are, we'll stay on if we're aren't. We'll consider that our dumb enough for tonight.

138
00:17:33,160 --> 00:17:49,160
All right, it looks like there are no, no questioners. So just kind of say good night then.

139
00:17:49,160 --> 00:18:05,080
See y'all tomorrow.

