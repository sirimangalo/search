1
00:00:00,000 --> 00:00:10,000
In many suitors, we read that we should not indulge in any sensual, ill-will, harming

2
00:00:10,000 --> 00:00:14,000
fault and should remove it completely.

3
00:00:14,000 --> 00:00:22,000
But is it not said that it's not said how in our tradition we can say, just note it,

4
00:00:22,000 --> 00:00:26,000
and move on, I guess that's what that meant.

5
00:00:26,000 --> 00:00:36,000
Yeah, not indulging in three types of micha, sangha, wrong thought,

6
00:00:36,000 --> 00:00:40,000
is actually a little bit more than just note it.

7
00:00:40,000 --> 00:00:46,000
Because there's an aspect of the teaching here that the Buddha is telling us to remind ourselves

8
00:00:46,000 --> 00:00:50,000
not to the opposite, which is not to follow the thoughts.

9
00:00:50,000 --> 00:00:53,000
And in meditation you have to do that.

10
00:00:53,000 --> 00:00:58,000
You can't note something until you get it into your head that you should be noting.

11
00:00:58,000 --> 00:01:01,000
So the teaching here is actually on two levels.

12
00:01:01,000 --> 00:01:06,000
The preliminary aspect is where you remind yourself,

13
00:01:06,000 --> 00:01:13,000
hey, I should stop daydreaming or hey, I should stop plotting revenge and realize that that's not

14
00:01:13,000 --> 00:01:18,000
the proper way to behave and then get into noting.

15
00:01:18,000 --> 00:01:23,000
So when the Buddha says removing it completely and not,

16
00:01:23,000 --> 00:01:28,000
I think how he puts it, but yeah, basically not indulging in it,

17
00:01:28,000 --> 00:01:40,000
not harboring it, not indulging in it, not allowing it to cultivate.

18
00:01:40,000 --> 00:01:48,000
There's two aspects to that, I think, practically, as I said,

19
00:01:48,000 --> 00:01:57,000
the act of giving it up in the act of then neutralizing it,

20
00:01:57,000 --> 00:02:01,000
by just saying, thinking, thinking, or liking, liking, wanting, wanting,

21
00:02:01,000 --> 00:02:04,000
or angry, angry, disliking, and so on.

22
00:02:04,000 --> 00:02:12,000
But thoughts are something that you have to deal with intellectually first and

23
00:02:12,000 --> 00:02:17,000
reprimand yourself and say, this is not, or not reprimand necessarily,

24
00:02:17,000 --> 00:02:26,000
but question yourself and investigate and see for yourself that that's not helping you.

25
00:02:26,000 --> 00:02:28,000
Ideally, this can just work by noting.

26
00:02:28,000 --> 00:02:32,000
And as you note, you start to realize that the thoughts are useless and not beneficial,

27
00:02:32,000 --> 00:02:38,000
but there is a sense of intellectual activity where you tell yourself,

28
00:02:38,000 --> 00:02:45,000
look, you know where this is leading and catching yourself and saying, oh, I should be meditating.

29
00:02:45,000 --> 00:02:49,000
That person had another post a couple of follow-up questions.

30
00:02:49,000 --> 00:02:54,000
As I said, are some contemplative practice maybe?

31
00:02:54,000 --> 00:03:00,000
The least probable would be to just repress it inside us.

32
00:03:00,000 --> 00:03:03,000
Question one.

33
00:03:03,000 --> 00:03:05,000
Yeah, repressing is an interesting idea.

34
00:03:05,000 --> 00:03:07,000
I think I've talked about this before.

35
00:03:07,000 --> 00:03:11,000
It's not really clear that you can actually repress something.

36
00:03:11,000 --> 00:03:15,000
What you're doing when you say repressing is actually reacting negatively to it

37
00:03:15,000 --> 00:03:17,000
and building up a habit of negative reaction.

38
00:03:17,000 --> 00:03:19,000
So it's not really good.

39
00:03:19,000 --> 00:03:22,000
But there are cases where the Buddha says you should do just that.

40
00:03:22,000 --> 00:03:27,000
When there's no other choice and it's just bugging you.

41
00:03:27,000 --> 00:03:32,000
And where you just can't get it out of your mind,

42
00:03:32,000 --> 00:03:37,000
you have to then be strong with yourself, be firm with yourself, and say no.

43
00:03:37,000 --> 00:03:43,000
And that comes up in different ways in meditation where you have a persistent state of mind.

44
00:03:43,000 --> 00:03:50,000
And instead of noting it, you have to reprimand yourself and say, no, this is wrong.

45
00:03:50,000 --> 00:03:56,000
And so we actually will encourage meditators sometimes to just say to themselves, stop.

46
00:03:56,000 --> 00:03:59,000
And their minds just make an mental note, stop.

47
00:03:59,000 --> 00:04:02,000
And then it'll stop.

48
00:04:02,000 --> 00:04:04,000
Yeah, it can stop.

49
00:04:04,000 --> 00:04:05,000
It's not totally under control.

50
00:04:05,000 --> 00:04:11,000
But this is a stop gap measure that you use while you're finding your feet

51
00:04:11,000 --> 00:04:16,000
and learning how to note that it can be useful at times.

52
00:04:16,000 --> 00:04:17,000
The best is just to note.

53
00:04:17,000 --> 00:04:20,000
If you can remember to note, just note the thoughts,

54
00:04:20,000 --> 00:04:26,000
the emotions, pick it apart, see it for what it is, and realize that, you know,

55
00:04:26,000 --> 00:04:32,000
teach yourself that these thoughts are causing you headaches and suffering and stress

56
00:04:32,000 --> 00:04:36,000
and getting you lost and leading you nowhere, most importantly,

57
00:04:36,000 --> 00:04:42,000
taking you away from reality because it's easy to get lost in illusion and fantasy

58
00:04:42,000 --> 00:04:47,000
even for a long time because it's much preferable to reality.

59
00:04:47,000 --> 00:04:52,000
In every aspect, illusion is preferable to reality and every aspect

60
00:04:52,000 --> 00:04:55,000
except that it's not real.

61
00:04:55,000 --> 00:04:57,000
So it messes with you, it messes with reality.

62
00:04:57,000 --> 00:05:01,000
This is why reality is changing in some bad ways now.

63
00:05:01,000 --> 00:05:06,000
If you look at our reality, all of us sitting in front of this computer,

64
00:05:06,000 --> 00:05:10,000
it's not really a pleasant, it's not really a pleasant.

65
00:05:10,000 --> 00:05:14,000
I mean, from my point of view, as pleasant as maybe sitting on a rock in the forest.

66
00:05:14,000 --> 00:05:23,000
But we've changed a lot and a lot of it has to do with our addiction to entertainment.

67
00:05:23,000 --> 00:05:27,000
I used to give talks in second life,

68
00:05:27,000 --> 00:05:29,000
and I was just talking about this last night.

69
00:05:29,000 --> 00:05:33,000
I was showing my old videos from second life to Lori before he left.

70
00:05:33,000 --> 00:05:42,000
And the second life is really a good example of this,

71
00:05:42,000 --> 00:05:47,000
where we go to this place that's so beautiful and in the forest and people make houses there

72
00:05:47,000 --> 00:05:52,000
and have beautiful bodies and beautiful houses and beautiful,

73
00:05:52,000 --> 00:05:54,000
you know, everything wonderful.

74
00:05:54,000 --> 00:05:58,000
But the truth is, they're just sitting in front of a black box,

75
00:05:58,000 --> 00:06:01,000
you know, at a desk somewhere.

76
00:06:01,000 --> 00:06:08,000
And that's kind of the depressing thing is that the further we get engaged in illusion.

77
00:06:08,000 --> 00:06:12,000
And this has a lot to do with these three kinds of thoughts.

78
00:06:12,000 --> 00:06:14,000
The worse off, we become in reality.

79
00:06:14,000 --> 00:06:18,000
Our reality becomes less and less agreeable, less and less.

80
00:06:18,000 --> 00:06:24,000
On two levels, mentally we become less appreciative of our reality

81
00:06:24,000 --> 00:06:31,000
and physically our reality changes, becomes more coarse and chaotic.

82
00:06:31,000 --> 00:06:34,000
Because we're not cultivating peace and harmony,

83
00:06:34,000 --> 00:06:42,000
I mean, much better than we should stay in reality and cultivate our

84
00:06:42,000 --> 00:06:47,000
bring our dreams in reality, make our life a dream, not a dream,

85
00:06:47,000 --> 00:06:53,000
but bring our dreams to fruition rather than dreaming.

86
00:06:53,000 --> 00:06:56,000
On the subject of meditation,

87
00:06:56,000 --> 00:07:01,000
that one listening writes a high bungee, I'm getting irregular heartbeat.

88
00:07:01,000 --> 00:07:04,000
And we better stop and wait, that's a different question. Let me stop this.

