1
00:00:00,000 --> 00:00:07,000
Hey, good evening everyone. Welcome to our live broadcast. Evening them.

2
00:00:10,000 --> 00:00:15,000
Today we're looking at the third normal truth.

3
00:00:15,000 --> 00:00:22,000
In the cessation of suffering.

4
00:00:33,000 --> 00:00:38,000
As a general concept, of course, the

5
00:00:38,000 --> 00:00:44,000
cessation of suffering is what we all desire, right?

6
00:00:44,000 --> 00:00:52,000
You may not have even thought about it, but sounds good. Sounds about right.

7
00:00:52,000 --> 00:01:00,000
Yes, that's why I came to learn about Buddhism. That's why I'm interested in spiritual ideas.

8
00:01:00,000 --> 00:01:10,000
Whatever way, whatever way things are wrong, I want to free myself from that.

9
00:01:10,000 --> 00:01:14,000
I want to write the wrong.

10
00:01:14,000 --> 00:01:20,000
Escape the pain, the stress, the suffering.

11
00:01:20,000 --> 00:01:22,000
So this is the goal.

12
00:01:22,000 --> 00:01:30,000
In the general sense, it's what we all desire.

13
00:01:30,000 --> 00:01:34,000
It's really the most important and framing it negatively like this,

14
00:01:34,000 --> 00:01:42,000
as I think we're really important. I mean, some people try to talk about Buddhism.

15
00:01:42,000 --> 00:01:47,000
I try to say, well, you know, it can go either way. Buddhism is also the way to find true happiness,

16
00:01:47,000 --> 00:01:51,000
but framing it as freedom from suffering is important,

17
00:01:51,000 --> 00:01:59,000
because once the suffering is gone, you don't have to speak about happiness.

18
00:01:59,000 --> 00:02:04,000
Once the suffering is gone, all that's left is happiness.

19
00:02:04,000 --> 00:02:12,000
On the other hand, if you speak about happiness, it's problematic because

20
00:02:12,000 --> 00:02:19,000
the feeling happy, happy feelings are not permanent.

21
00:02:19,000 --> 00:02:27,000
They're not constant. It's not possible to have a happy feeling all the time.

22
00:02:27,000 --> 00:02:32,000
So when you talk about it as the cessation of suffering, it makes more sense.

23
00:02:32,000 --> 00:02:36,000
And then you're left with times where you're not happy,

24
00:02:36,000 --> 00:02:39,000
but you're never left with times where you're unhappy.

25
00:02:39,000 --> 00:02:42,000
It's fine to be not happy sometimes, right?

26
00:02:42,000 --> 00:02:45,000
We don't have to be no one, no one.

27
00:02:45,000 --> 00:02:51,000
I think really strives to be happy all the time.

28
00:02:51,000 --> 00:02:58,000
We just, it would be enough if we weren't unhappy any time, right?

29
00:02:58,000 --> 00:03:03,000
Sometimes you just content peaceful.

30
00:03:03,000 --> 00:03:05,000
So I think it's easier to understand.

31
00:03:05,000 --> 00:03:13,000
I think you get the wrong idea if you think of finding permanent happiness.

32
00:03:13,000 --> 00:03:16,000
Even though it's phrased in that way, and it can be phrased,

33
00:03:16,000 --> 00:03:22,000
it's just a question of what you mean by happiness.

34
00:03:22,000 --> 00:03:25,000
But the Buddha talked about the cessation of suffering.

35
00:03:25,000 --> 00:03:27,000
And so that's what we teach.

36
00:03:27,000 --> 00:03:29,000
That's what we look for.

37
00:03:29,000 --> 00:03:34,000
So we strive for.

38
00:03:34,000 --> 00:03:38,000
But I say in a general sense, because when it gets to the specifics

39
00:03:38,000 --> 00:03:42,000
and the actual definition of what it means,

40
00:03:42,000 --> 00:03:49,000
I think it's a little bit scary.

41
00:03:49,000 --> 00:03:53,000
The first thing to understand about the cessation of suffering

42
00:03:53,000 --> 00:03:57,000
let's go right to what it's.

43
00:03:57,000 --> 00:04:01,000
What the Kijay is, what the duty is, the thing to be done,

44
00:04:01,000 --> 00:04:04,000
which to be done in regards to the cessation of suffering

45
00:04:04,000 --> 00:04:10,000
is to see it for yourself, such a gutter nut,

46
00:04:10,000 --> 00:04:16,000
to make it clear to yourself.

47
00:04:16,000 --> 00:04:22,000
So it's not about believing, obviously.

48
00:04:22,000 --> 00:04:25,000
It's not even really about striving for.

49
00:04:25,000 --> 00:04:27,000
We already know what our striving is.

50
00:04:27,000 --> 00:04:34,000
Our striving is to understand.

51
00:04:34,000 --> 00:04:41,000
When you understand suffering, the cause of suffering

52
00:04:41,000 --> 00:04:45,000
is removed and there comes about the cessation of suffering.

53
00:04:45,000 --> 00:04:49,000
So the Buddha described the third noble truth

54
00:04:49,000 --> 00:05:00,000
as with the complete cessation

55
00:05:00,000 --> 00:05:04,000
and disenchantments.

56
00:05:04,000 --> 00:05:08,000
I say, so we raga niroda.

57
00:05:08,000 --> 00:05:19,000
The cessation without remainder.

58
00:05:19,000 --> 00:05:24,000
Dispassion it, cessation without remainder.

59
00:05:24,000 --> 00:05:27,000
Without a new remainder of craving,

60
00:05:27,000 --> 00:05:35,000
we have that craving that we talked about yesterday.

61
00:05:35,000 --> 00:05:42,000
There is the cessation of suffering.

62
00:05:42,000 --> 00:05:44,000
But when we talk about the specifics of it,

63
00:05:44,000 --> 00:05:48,000
what we mean is not simply, I mean that's in a general sense

64
00:05:48,000 --> 00:05:50,000
that's the theory.

65
00:05:50,000 --> 00:05:53,000
If we stopped clinging to things, well,

66
00:05:53,000 --> 00:05:58,000
there'd be less disappointment.

67
00:05:58,000 --> 00:06:00,000
Craving and disappointment can go hand in hand

68
00:06:00,000 --> 00:06:02,000
as we talked about last night.

69
00:06:02,000 --> 00:06:05,000
You want, and the wanting increases until you don't get it.

70
00:06:05,000 --> 00:06:08,000
And then there's a period of disappointment

71
00:06:08,000 --> 00:06:10,000
that sort of stabilizes the wanting.

72
00:06:10,000 --> 00:06:13,000
And then when the disappointment ends,

73
00:06:13,000 --> 00:06:16,000
you strive for it again.

74
00:06:16,000 --> 00:06:20,000
Go, go, go, crash.

75
00:06:20,000 --> 00:06:25,000
And again, and again, we crash like this.

76
00:06:25,000 --> 00:06:29,000
And we strive through it our lives successfully,

77
00:06:29,000 --> 00:06:36,000
unsuccessfully, to avoid the crashes

78
00:06:36,000 --> 00:06:40,000
and so our craving builds and builds.

79
00:06:40,000 --> 00:06:42,000
Unless we find ways to temper it.

80
00:06:42,000 --> 00:06:48,000
There are people who find ways to be kind

81
00:06:48,000 --> 00:06:53,000
and they start to realize higher things.

82
00:06:53,000 --> 00:06:56,000
And they start to give up craving.

83
00:06:56,000 --> 00:06:58,000
Another thing is experience.

84
00:06:58,000 --> 00:07:00,000
Without even practicing Buddhism,

85
00:07:00,000 --> 00:07:02,000
simply through experiencing this truth.

86
00:07:02,000 --> 00:07:05,000
Yeah, craving builds and builds and builds.

87
00:07:05,000 --> 00:07:09,000
It's not just Buddhists, but all people ask they get older.

88
00:07:09,000 --> 00:07:14,000
In many cases, moderate and even reduce their craving.

89
00:07:14,000 --> 00:07:15,000
Why?

90
00:07:15,000 --> 00:07:21,000
Because they've seen that it causes suffering.

91
00:07:21,000 --> 00:07:24,000
But that's not actually the goal in Buddhism.

92
00:07:24,000 --> 00:07:28,000
It's not just to be, oh, well, now I'm living my life without suffering.

93
00:07:28,000 --> 00:07:33,000
The cessation of suffering is nibana.

94
00:07:33,000 --> 00:07:37,000
It's the complete cessation of dukha.

95
00:07:37,000 --> 00:07:41,000
And so here we get into, we have to get a technical,

96
00:07:41,000 --> 00:07:44,000
and we have to be a little more specific about what we mean by dukha.

97
00:07:44,000 --> 00:07:52,000
I said it's not always meant to mean suffering.

98
00:07:52,000 --> 00:07:58,000
But I think in an ultimate analysis,

99
00:07:58,000 --> 00:08:03,000
what you have to say about Sankara, about our experiences,

100
00:08:03,000 --> 00:08:06,000
it's not only that they're not satisfying,

101
00:08:06,000 --> 00:08:16,000
but that they are inferior.

102
00:08:16,000 --> 00:08:20,000
No, it's not a good word.

103
00:08:20,000 --> 00:08:22,000
They are unpeaceful.

104
00:08:22,000 --> 00:08:26,000
It's the right word.

105
00:08:26,000 --> 00:08:30,000
Stressful, even.

106
00:08:30,000 --> 00:08:33,000
They are stress.

107
00:08:33,000 --> 00:08:37,000
A risen experience is inferior,

108
00:08:37,000 --> 00:08:41,000
but it's used that sort of terminology

109
00:08:41,000 --> 00:08:46,000
to the cessation, to the non-arising.

110
00:08:46,000 --> 00:08:49,000
Non-arising is better than arising.

111
00:08:49,000 --> 00:08:51,000
This is where it starts to get scary,

112
00:08:51,000 --> 00:09:00,000
because this is the sort of thing that's hard to appreciate.

113
00:09:00,000 --> 00:09:05,000
It's hard to swallow.

114
00:09:05,000 --> 00:09:10,000
The idea that nibana is the unarising.

115
00:09:10,000 --> 00:09:14,000
It's unarising.

116
00:09:14,000 --> 00:09:17,000
So with the cessation of all that arises,

117
00:09:17,000 --> 00:09:20,000
with the cessation of craving,

118
00:09:20,000 --> 00:09:28,000
how it works is our mind keeps us constantly seeking out experience.

119
00:09:28,000 --> 00:09:34,000
Our experience is fueled by this craving.

120
00:09:34,000 --> 00:09:40,000
It's propelled on by this craving.

121
00:09:40,000 --> 00:09:42,000
If we're able to do away with it,

122
00:09:42,000 --> 00:09:45,000
we're able to create these pockets.

123
00:09:45,000 --> 00:09:52,000
If we're able to moderate and do away with temporarily this craving,

124
00:09:52,000 --> 00:09:59,000
then we're able to create a pocket of non-arising.

125
00:09:59,000 --> 00:10:04,000
That's why I said it's important to look at the kitchen,

126
00:10:04,000 --> 00:10:09,000
the task at hand,

127
00:10:09,000 --> 00:10:12,000
because you have to experience it for yourself

128
00:10:12,000 --> 00:10:14,000
to appreciate nibana.

129
00:10:14,000 --> 00:10:16,000
It's not something you can think about.

130
00:10:16,000 --> 00:10:21,000
Our whole being revolts against it,

131
00:10:21,000 --> 00:10:45,000
because where our whole being is built around craving.

132
00:10:45,000 --> 00:10:48,000
So it's a matter of seeing it for yourself.

133
00:10:48,000 --> 00:10:51,000
The great thing about the Buddha's teaching in general

134
00:10:51,000 --> 00:10:54,000
is that you can disagree with it all you want,

135
00:10:54,000 --> 00:10:56,000
but it's true.

136
00:10:56,000 --> 00:11:00,000
Anything that's true stands up to the test of investigation.

137
00:11:00,000 --> 00:11:02,000
All of the four noble truths,

138
00:11:02,000 --> 00:11:04,000
they're hard to stomach.

139
00:11:04,000 --> 00:11:06,000
They're hard to understand, they're hard to appreciate,

140
00:11:06,000 --> 00:11:08,000
but they're true.

141
00:11:08,000 --> 00:11:11,000
They don't sound like anything we've ever heard.

142
00:11:11,000 --> 00:11:15,000
They don't sound intuitively right.

143
00:11:15,000 --> 00:11:18,000
Of course they don't, because our whole being,

144
00:11:18,000 --> 00:11:20,000
even our intuition, is based around craving.

145
00:11:20,000 --> 00:11:22,000
It's built around it.

146
00:11:22,000 --> 00:11:26,000
We're born because of craving.

147
00:11:26,000 --> 00:11:31,000
Our whole paradigm of looking at the world is craving.

148
00:11:31,000 --> 00:11:33,000
So we look, what do you like?

149
00:11:33,000 --> 00:11:34,000
What do you dislike?

150
00:11:34,000 --> 00:11:37,000
That's what it's all about.

151
00:11:37,000 --> 00:11:43,000
For us.

152
00:11:43,000 --> 00:11:47,000
When you begin to look at things as they actually are,

153
00:11:47,000 --> 00:11:51,000
instead of how you want them to be or don't want them to be,

154
00:11:51,000 --> 00:11:57,000
all of these truths become evident.

155
00:11:57,000 --> 00:12:00,000
And eventually the third truth becomes evident,

156
00:12:00,000 --> 00:12:04,000
and you see you experience the cessation of Sankara.

157
00:12:04,000 --> 00:12:09,000
Because all a risen phenomena are dukkha.

158
00:12:09,000 --> 00:12:11,000
They're not just unsatisfying,

159
00:12:11,000 --> 00:12:15,000
and they're not just suffering when you cling to them.

160
00:12:15,000 --> 00:12:19,000
They're suffering when they arrive.

161
00:12:19,000 --> 00:12:23,000
So the question then is, does that mean an arahant suffers?

162
00:12:23,000 --> 00:12:27,000
And so this is the thing, how do you translate this?

163
00:12:27,000 --> 00:12:32,000
An arahant, one way of saying it is an arahant,

164
00:12:32,000 --> 00:12:34,000
of course, doesn't suffer.

165
00:12:34,000 --> 00:12:38,000
It's called saupadhi saiimbana.

166
00:12:38,000 --> 00:12:42,000
There's still remaining Sankara,

167
00:12:42,000 --> 00:12:44,000
but there's no craving for them,

168
00:12:44,000 --> 00:12:47,000
and someone doesn't cling to them.

169
00:12:47,000 --> 00:12:49,000
So that is nibana.

170
00:12:49,000 --> 00:12:52,000
That is a way of looking at it.

171
00:12:52,000 --> 00:12:55,000
But the experience of nibana,

172
00:12:55,000 --> 00:12:57,000
Anupadhi saiimbana,

173
00:12:57,000 --> 00:13:03,000
is when all Sankara sees new road.

174
00:13:03,000 --> 00:13:06,000
So technically in another way,

175
00:13:06,000 --> 00:13:09,000
you could say an arahant does suffer.

176
00:13:09,000 --> 00:13:11,000
They suffer not mentally,

177
00:13:11,000 --> 00:13:17,000
but they suffer in the sense of having to put up with that,

178
00:13:17,000 --> 00:13:20,000
which is stressful.

179
00:13:20,000 --> 00:13:26,000
And that which is unpeaceful.

180
00:13:26,000 --> 00:13:28,000
And because an arahant knows these things are unpeaceful,

181
00:13:28,000 --> 00:13:31,000
it doesn't see any use for them or benefit to them.

182
00:13:31,000 --> 00:13:36,000
Why an arahant is not reborn,

183
00:13:36,000 --> 00:13:41,000
is because an arahant sees that all arisen phenomena are worthless.

184
00:13:45,000 --> 00:13:49,000
If you want, you can think of it as a graduation.

185
00:13:49,000 --> 00:13:54,000
So most of us will be around here for a long time,

186
00:13:54,000 --> 00:13:57,000
because we have much more to learn.

187
00:13:57,000 --> 00:14:02,000
Nibana is not something you have to accept on faith, believe in,

188
00:14:02,000 --> 00:14:07,000
aspire to.

189
00:14:07,000 --> 00:14:09,000
But once you understand reality,

190
00:14:09,000 --> 00:14:10,000
this is the claim.

191
00:14:10,000 --> 00:14:12,000
Once you understand reality,

192
00:14:12,000 --> 00:14:17,000
that's where the mind inclines.

193
00:14:17,000 --> 00:14:20,000
It's like the final, the goal.

194
00:14:20,000 --> 00:14:22,000
It's actually a real goal.

195
00:14:22,000 --> 00:14:25,000
That's really the neat thing.

196
00:14:25,000 --> 00:14:34,000
Or the distinction in Buddhism is that any other goal isn't really a goal.

197
00:14:34,000 --> 00:14:39,000
No goal that we could strive for can truly be seen as a goal.

198
00:14:39,000 --> 00:14:43,000
Upon investigation, it's clear that there's still something more.

199
00:14:43,000 --> 00:14:49,000
Once you attain that goal, you just move on, right?

200
00:14:49,000 --> 00:14:56,000
So the idea of a goal seems actually non-existent.

201
00:14:56,000 --> 00:14:58,000
And in a sense, the goal is non-existent.

202
00:14:58,000 --> 00:15:03,000
It exists, but it's non-existent in the sense of non-arising.

203
00:15:03,000 --> 00:15:05,000
It depends on your defined existence.

204
00:15:05,000 --> 00:15:07,000
If you define it by that which arises,

205
00:15:07,000 --> 00:15:10,000
then Nibana is non-existent.

206
00:15:10,000 --> 00:15:16,000
But it exists in the ultimate sense as something that is real.

207
00:15:16,000 --> 00:15:18,000
It is real.

208
00:15:18,000 --> 00:15:23,000
It's real non-arising.

209
00:15:23,000 --> 00:15:25,000
So not too much to say about this.

210
00:15:25,000 --> 00:15:26,000
I think that's enough.

211
00:15:26,000 --> 00:15:32,000
I've said quite a bit already, probably more than most people would.

212
00:15:32,000 --> 00:15:36,000
But that's a little bit of information and to try to understand Nibana.

213
00:15:36,000 --> 00:15:41,000
A caution not to focus too much on it as something to obsess over,

214
00:15:41,000 --> 00:15:42,000
or worry about,

215
00:15:42,000 --> 00:15:45,000
do I really want to experience it?

216
00:15:45,000 --> 00:15:47,000
Do I really want that as my goal?

217
00:15:47,000 --> 00:15:49,000
Of course we do.

218
00:15:49,000 --> 00:15:50,000
We all do.

219
00:15:50,000 --> 00:15:51,000
We want to be free from suffering.

220
00:15:51,000 --> 00:15:53,000
And if that's enough,

221
00:15:53,000 --> 00:15:58,000
if that's true and that's enough,

222
00:15:58,000 --> 00:16:00,000
a question of what that means,

223
00:16:00,000 --> 00:16:03,000
it's not something you should worry about.

224
00:16:03,000 --> 00:16:07,000
Because you'll only realize that when you really want to,

225
00:16:07,000 --> 00:16:09,000
when you're really ready for it,

226
00:16:09,000 --> 00:16:13,000
when it's really clear to you,

227
00:16:13,000 --> 00:16:16,000
that this is happiness, this is peace.

228
00:16:16,000 --> 00:16:20,000
It doesn't sound like a tongue beneath them.

229
00:16:20,000 --> 00:16:26,000
This is peaceful, this is subtle, refined.

230
00:16:26,000 --> 00:16:30,000
So there's the demo for tonight.

231
00:16:30,000 --> 00:16:33,000
Thank you all for tuning in.

232
00:16:33,000 --> 00:16:35,000
And now go to the question section.

233
00:16:35,000 --> 00:16:38,000
We have two questions tonight.

234
00:16:38,000 --> 00:16:42,000
My question is on the second normal truth.

235
00:16:42,000 --> 00:16:45,000
If craving or wanting causes suffering,

236
00:16:45,000 --> 00:16:48,000
then how could we achieve anything in life?

237
00:16:48,000 --> 00:16:51,000
If a strong desire is required to receive anything,

238
00:16:51,000 --> 00:16:53,000
either you want to attain Nirvana

239
00:16:53,000 --> 00:16:55,000
or to become rich or anything,

240
00:16:55,000 --> 00:16:59,000
please throw some more light on it.

241
00:16:59,000 --> 00:17:04,000
Well, a person who wants to attain Nirvana

242
00:17:04,000 --> 00:17:06,000
still has craving, right?

243
00:17:06,000 --> 00:17:09,000
Person who wants to meditate still has craving.

244
00:17:09,000 --> 00:17:11,000
But when you meditate, the craving goes away

245
00:17:11,000 --> 00:17:13,000
when the craving is gone,

246
00:17:13,000 --> 00:17:15,000
you won't achieve anything.

247
00:17:15,000 --> 00:17:17,000
In some cases, it just wouldn't eat.

248
00:17:17,000 --> 00:17:19,000
There are cases of our hands

249
00:17:19,000 --> 00:17:21,000
who just let their bodies perish.

250
00:17:21,000 --> 00:17:25,000
It's not common, but it has happened.

251
00:17:25,000 --> 00:17:28,000
So you're looking at the wrong way.

252
00:17:28,000 --> 00:17:30,000
It's not like we say,

253
00:17:30,000 --> 00:17:31,000
okay, from this day forth,

254
00:17:31,000 --> 00:17:33,000
I'm not going to want anything.

255
00:17:33,000 --> 00:17:36,000
We say, okay, I'm going to try because I want

256
00:17:36,000 --> 00:17:38,000
to be free from wanting.

257
00:17:38,000 --> 00:17:40,000
And as you do that, you start to,

258
00:17:40,000 --> 00:17:42,000
it starts to unravel.

259
00:17:42,000 --> 00:17:45,000
You slowly stop wanting anything.

260
00:17:45,000 --> 00:17:47,000
Because meditation isn't one thing.

261
00:17:47,000 --> 00:17:49,000
You don't sit there and want to be enlightened.

262
00:17:49,000 --> 00:17:50,000
It's not saying they're

263
00:17:50,000 --> 00:17:51,000
when am I going to become,

264
00:17:51,000 --> 00:17:53,000
that there's nothing for you.

265
00:17:53,000 --> 00:17:55,000
Suppose you want to meditate.

266
00:17:55,000 --> 00:17:56,000
Well, then you unmeditate.

267
00:17:56,000 --> 00:17:58,000
And when you meditate, there's no wanting.

268
00:17:58,000 --> 00:18:01,000
Or there's the gradual,

269
00:18:01,000 --> 00:18:04,000
free realization that wanting is the cause of suffering.

270
00:18:04,000 --> 00:18:07,000
Once you realize that, the one thing goes away.

271
00:18:07,000 --> 00:18:09,000
I mean, this is a question people ask a lot

272
00:18:09,000 --> 00:18:10,000
and it's really intellectual.

273
00:18:10,000 --> 00:18:17,000
It isn't at all how it works in practice.

274
00:18:17,000 --> 00:18:19,000
I'm not to shoot you down or anything.

275
00:18:19,000 --> 00:18:20,000
It's a common question.

276
00:18:20,000 --> 00:18:23,000
It's just, I don't think that's a question you have

277
00:18:23,000 --> 00:18:25,000
to worry about because it's intellectual

278
00:18:25,000 --> 00:18:28,000
and it's not really how it goes down.

279
00:18:28,000 --> 00:18:31,000
The difference between the detachment of an

280
00:18:31,000 --> 00:18:33,000
arrow hunt to that of a person who's

281
00:18:33,000 --> 00:18:35,000
in a state of numbness caused by depression

282
00:18:35,000 --> 00:18:38,000
or other traumatic experience

283
00:18:38,000 --> 00:18:42,000
can be engaged in his desperate.

284
00:18:42,000 --> 00:18:44,000
Therefore, giving up, give up clinging

285
00:18:44,000 --> 00:18:47,000
to anything joyful or they cling to that state.

286
00:18:47,000 --> 00:18:50,000
They cling to their depression.

287
00:18:50,000 --> 00:18:52,000
They wish to be free from it.

288
00:18:52,000 --> 00:18:54,000
They cling to the fear.

289
00:18:54,000 --> 00:18:56,000
They cling to all sorts of things.

290
00:18:56,000 --> 00:19:00,000
They cling to disliking.

291
00:19:00,000 --> 00:19:03,000
State of numbness.

292
00:19:03,000 --> 00:19:05,000
There are many states that are concentrated

293
00:19:05,000 --> 00:19:08,000
and focused, but it's not really numb.

294
00:19:08,000 --> 00:19:10,000
If such a person were to begin to be mindful

295
00:19:10,000 --> 00:19:13,000
there would be a lot of things come up.

296
00:19:13,000 --> 00:19:14,000
It's completely different.

297
00:19:14,000 --> 00:19:16,000
An arrow hunt has looked through themselves

298
00:19:16,000 --> 00:19:18,000
and has no attachment.

299
00:19:18,000 --> 00:19:22,000
As understanding, they understand themselves thoroughly.

300
00:19:22,000 --> 00:19:25,000
It's not that they don't have any attachment.

301
00:19:25,000 --> 00:19:28,000
It's that they're unable to give rise to attachment.

302
00:19:28,000 --> 00:19:30,000
That person that you're talking about

303
00:19:30,000 --> 00:19:32,000
if they were to begin to be mindful

304
00:19:32,000 --> 00:19:34,000
may see a ton of attachment come up.

305
00:19:34,000 --> 00:19:39,000
They tend to find ways to get in a rut.

306
00:19:39,000 --> 00:19:43,000
It's not happiness, but it's bearable.

307
00:19:43,000 --> 00:19:46,000
It's pretty awful really once you become mindful of it.

308
00:19:46,000 --> 00:19:51,000
You see how stressful and unpleasant it was.

309
00:19:51,000 --> 00:19:59,000
I mean, I think mindfulness will answer that question.

310
00:19:59,000 --> 00:20:02,000
A mindfulness will shed light on

311
00:20:02,000 --> 00:20:04,000
with the differences that that person

312
00:20:04,000 --> 00:20:06,000
or that person is full of defilements,

313
00:20:06,000 --> 00:20:08,000
most likely.

314
00:20:16,000 --> 00:20:18,000
Okay, let's offer tonight.

315
00:20:18,000 --> 00:20:21,000
Thank you all for tuning in.

316
00:20:21,000 --> 00:20:37,000
Have a good night.

