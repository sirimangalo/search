WEBVTT

00:00.000 --> 00:18.960
Good evening everyone, welcome to our evening broadcast.

00:18.960 --> 00:29.120
Today is a dumb, today I wanted to look at, I'm going to talk about the idea of what

00:29.120 --> 00:37.280
this we need to know, what we need to learn from the practice, right?

00:37.280 --> 00:45.180
We talk about centipatanas for the purpose of attaining vipasana, the purpose of gaining

00:45.180 --> 00:57.560
insight, it's for the purpose of gaining yana, yana uddhapadi, vija uddhapadi, panya uddhapadi

00:57.560 --> 01:14.720
it's for gaining wisdom.

01:14.720 --> 01:21.680
I think a bit of a misunderstanding or a misconception of that is that some other many things

01:21.680 --> 01:28.160
that we have to learn, it's quite complex or a sense that we're not learning what we have

01:28.160 --> 01:29.160
to learn.

01:29.160 --> 01:31.040
You hear about wisdom, right?

01:31.040 --> 01:39.240
And you think I'm practicing for a long time, I don't see any wisdom yet, because

01:39.240 --> 01:46.560
you have this idea that wisdom is something lofty, wisdom is something perhaps intellectual

01:46.560 --> 01:57.480
or complicated, it's actually quite simple, it's somewhat sometimes a deceivingly simple

01:57.480 --> 02:14.160
anyway, my teacher, Adjantong said, when you say rise, when you watch the stomach rise

02:14.160 --> 02:22.680
and you say rise, when you know that the stomach is rising, that's wisdom, anything, that's

02:22.680 --> 02:33.760
it, why am I here, I can do this at home, it's a very important statement, when you know

02:33.760 --> 02:42.800
that the stomach is rising, that's wisdom, there's a problem is that we forget for, you

02:42.800 --> 02:51.880
never remember, we aren't wise, we're unwise, we have Ayoni Solmanasika, instead of just

02:51.880 --> 02:58.680
knowing that the stomach is rising or knowing that seeing is seeing, we know a lot more,

02:58.680 --> 03:08.040
we know too much, too much and not enough, everything and nothing, we know all the details

03:08.040 --> 03:18.880
about so many things, whereas as a human species seem bent on learning everything, collecting

03:18.880 --> 03:28.120
as much useless information as we can, mostly useless, and by useless, I mean by Buddhist

03:28.120 --> 03:34.080
standards, useless in the sense that they don't actually make us happy, the things we

03:34.080 --> 03:41.900
have, they don't actually bring peace, how much learning we need just to do some job that

03:41.900 --> 03:55.440
ends up fulfilling some function that does very little to promote well-being or learning

03:55.440 --> 04:01.600
skills that in the end are such a round about way of promoting any sort of goodness,

04:01.600 --> 04:11.760
life at all, and in fact often, promoting wholesomeness, how many skills, how much learning,

04:11.760 --> 04:32.040
how much knowledge is there out there that ends up being totally useless, or worse, harmful.

04:32.040 --> 04:38.360
When I was young, we used to play these computer games back before they had online computer

04:38.360 --> 04:50.040
games used to play, start craft, work craft, and you could spend hours when we play these

04:50.040 --> 04:55.640
battle simulations, we could spend hours, we would spend all night, we would stay awake

04:55.640 --> 05:03.240
all night and play, I'm sure to some of you, this all sounds like yeah, I mean, this is

05:03.240 --> 05:12.560
what we do now, I think this is what people do now, but you learn so much and you get

05:12.560 --> 05:21.520
these skills, wonderful skills and you get really good at these games or sports, or acting

05:21.520 --> 05:37.360
or mathematics, physics, all these wonderful things were able to build computers and

05:37.360 --> 05:48.120
spacecraft, so much learning, even languages, how much time we have to spend learning

05:48.120 --> 05:56.880
languages just to talk to each other, and then we die and forget at all, lose the languages

05:56.880 --> 06:04.840
enough to gain them all again, so no knowledge that we hope to gain from meditation

06:04.840 --> 06:11.960
is quite different, we hope to come to know that, oh right, yes, when the stomach rises,

06:11.960 --> 06:23.600
that's rising, it's not good, it's not bad, it's not me, it's not mine, but to be a little

06:23.600 --> 06:31.040
more precise if you like to get some clear up the doubt, it's easy to find doubt in regards

06:31.040 --> 06:37.120
to this, what is the wisdom, what am I trying to learn, one time I'm John Tong said to

06:37.120 --> 06:46.520
me that there are four things, there's only four things you have to learn, the first one

06:46.520 --> 07:03.720
is called Nama Rupa, number one, number two, Tila Kana, number three, maga, number four

07:03.720 --> 07:18.600
pala, under the pali word, Nama Rupa, this means Nama means the immaterial, Rupa means

07:18.600 --> 07:26.720
material, there are only two aspects to reality, the first thing you have to come to understand,

07:26.720 --> 07:33.680
if you want to practice meditation, there's only two parts to reality, there's the immaterial

07:33.680 --> 07:41.520
and there's the material, the material is the physical aspect of experience, when you walk

07:41.520 --> 07:46.800
you feel the tension or the hardness or the softness and the heat and the cold, and when

07:46.800 --> 08:00.160
you sit you feel the tension and so on, movements of the body, sensations in the body,

08:00.160 --> 08:06.520
these are material, when you touch something, the touching, the feeling of hardness or

08:06.520 --> 08:14.880
softness, that's material, but the immaterial is the knowing of it, the knowing of it and

08:14.880 --> 08:25.280
all the concomitant qualities of the knowing, when you like and dislike and all that, that's

08:25.280 --> 08:34.920
all material, immaterial, that's reality, that's what's real, the first thing you have

08:34.920 --> 08:41.920
to learn is what's real, the first step in meditation, you can progress until you're clear,

08:41.920 --> 08:50.400
when you move the right foot, there's an experience of the stuff of the pressure and

08:50.400 --> 09:02.080
the cold or movement of the wind and so on, and that arises and that's physical and there's

09:02.080 --> 09:09.240
the knowing of it as well, when the stomach rises, your mind knows, there's the rising

09:09.240 --> 09:17.200
and there's the rising movement and there's the mind, but there's no stomach, stomach

09:17.200 --> 09:22.400
is all produced in the brain, in the mind, without the brain in the mind, the brain also

09:22.400 --> 09:30.400
doesn't exist, these are concepts we give rise to, if you think of the brain or the brain

09:30.400 --> 09:36.880
doesn't exist, what do you mean the brain doesn't exist, besides as a concept there's no

09:36.880 --> 09:43.240
existence, if the brain is actually connected, it's just a part of the body, it extends

09:43.240 --> 09:50.080
into the central nervous system in order, so such thing is the brain, it's just a concept

09:50.080 --> 10:04.280
that we apply to certain mental and physical aspects of experience, there's no body, there's

10:04.280 --> 10:10.280
no room, we're not sitting in a room, there's not even any space, you know, what space

10:10.280 --> 10:16.560
is interesting because space is only a part of matter, it doesn't actually exist, it's

10:16.560 --> 10:22.360
only, it only comes to being because of matter, it's a part, a derived quality of matter,

10:22.360 --> 10:33.800
it does exist, it only exists only in regards to matter, the mind doesn't take up space,

10:33.800 --> 10:39.160
but not to get too complicated, very simply the only thing you have to know is that body

10:39.160 --> 10:49.720
in mind, material in material, reality is only made up of these two things, reality is

10:49.720 --> 10:55.240
made up of experiences, when you see something there's the physical light and there's

10:55.240 --> 11:02.560
the eye, then there's the knowing, sometimes with your eyes open you don't actually see

11:02.560 --> 11:07.360
something in front of you, your mind is somewhere else, even though your eyes are open,

11:07.360 --> 11:15.240
your mind isn't there, sound, hearing requires sound, the physical, the ear also requires

11:15.240 --> 11:19.880
the mind, of course sometimes you're absorbed in something and someone calls your name

11:19.880 --> 11:27.600
and you don't hear it, mind, that's the immaterial, two things required for experience,

11:27.600 --> 11:33.120
for seeing, hearing, smelling, tasting, feeling, thinking, you need a material immaterial,

11:33.120 --> 11:42.600
this is the first thing you have to, the second thing we have to learn is the three characteristics,

11:42.600 --> 11:49.880
tila kana, tit means three, lakana means characteristics, you're also called sama nyalakana,

11:49.880 --> 12:02.960
sama nyam means universal or universal basically, common to all, you can see the three characteristics

12:02.960 --> 12:15.200
of just about everything, the three characteristics of course are impermanence, instability, uncertainty,

12:15.200 --> 12:27.520
unreliability, unpredictability, all that, suffering, stress, dissatisfaction, inability

12:27.520 --> 12:50.160
to satisfy unhappiness, non-happiness, basically not being happiness and non-self, uncontrollability

12:50.160 --> 12:57.880
and substantiality, this is the second thing you have to learn, this is really what you

12:57.880 --> 13:07.440
start to see through the practice, you start to readjust your understanding about things,

13:07.440 --> 13:14.960
you see for the first time how much suffering we're causing ourselves and you start to see,

13:14.960 --> 13:21.960
the mistakes you're making, you're making a mistake when you cling, when you want, when you

13:21.960 --> 13:35.680
expect because reality is in constant, unpredictable, unsatisfying, cancerous, and uncontrollable,

13:35.680 --> 13:54.560
insubstantial, not self, it has no entity, no substantiality of its own, again these three

13:54.560 --> 13:58.520
things are not some mystery, everyone reads about these and they think, I don't see those

13:58.520 --> 14:05.440
things, I've given this talk several times about how meditators will come and say I can't,

14:05.440 --> 14:13.160
I'm not progressing, I just sit here and my mind is in chaos and it's unpleasant and

14:13.160 --> 14:20.920
I can't control that, how can I progress it in this right, this is progress, seeing

14:20.920 --> 14:30.000
that your mind is unpredictable, chaotic, it's a cause of great stress and all the clinging

14:30.000 --> 14:36.160
this causes for great stress that we're clinging to things that can't possibly satisfy

14:36.160 --> 14:43.320
as that's why we're stressed, it's out of control, you can't control, you can't predict

14:43.320 --> 14:48.960
expectations, have no bearing on reality, we act as though our expectations are going

14:48.960 --> 14:54.920
to somehow dictate reality, it's quite silly isn't it, somehow because we want things

14:54.920 --> 15:01.520
to be a certain way that they're going to be that way, such an odd idea, an odd concept,

15:01.520 --> 15:12.240
that's how we act, so we just start to change, just start to see that one's expectations

15:12.240 --> 15:21.440
and it's all highly problematic, it's the main cause of suffering, they can't predict,

15:21.440 --> 15:29.480
they can't expect, it doesn't, we really turn out the way we want, it's unreliable,

15:29.480 --> 15:34.000
this is the main insight, this is what starts to loosen up the mind and free us from our

15:34.000 --> 15:42.240
bonding, from this obsession that we have with pleasure and displeasure, attaining pleasure

15:42.240 --> 15:59.040
and removing or destroying, avoiding displeasure, until eventually we start to, we start to

15:59.040 --> 16:09.760
start to see things with equanimity, so the second is these characteristics, this is the

16:09.760 --> 16:15.600
beginning of the path, the three characteristics are really the beginning of insight, soon

16:15.600 --> 16:24.640
as you start, once you start to see these, you can say you started on the path, or you've

16:24.640 --> 16:30.120
opened the door, let's say, seeing these is like the door, right before, when you open

16:30.120 --> 16:35.280
the door and you say that's where I want to go, so you open the door, the next one is

16:35.280 --> 16:40.240
manga, it's the path, and that's what you walk, that's what you, that's what all the

16:40.240 --> 16:47.560
meditators here are on, they're cultivating the path, the first few days are just spent

16:47.560 --> 16:52.560
aligning yourself with the past, in fact some of you are still on this stage, but as

16:52.560 --> 16:57.640
you go along, you start to see the three characteristics, it'll come, these are not things

16:57.640 --> 17:08.120
you have to look for, all of these insights are like the stripes on a tiger, you don't

17:08.120 --> 17:13.760
have to look for them, once you see the tigers and here's the tiger, yep, I see it stripes,

17:13.760 --> 17:16.600
they're right there with the tiger, you don't have to look for them, they're not hard

17:16.600 --> 17:25.640
to fight, these are not complex or difficult things to understand, they're just, they're

17:25.640 --> 17:34.520
hidden to us because we're blind, because we don't look, once we look we'll see, so manga

17:34.520 --> 17:39.000
is as you start to progress and you start to see deeper the three characteristics and

17:39.000 --> 17:47.400
I've talked about this before, in detail the path, they won't go into detail here, but

17:47.400 --> 17:58.240
basically you start to see that reality is in constant arises and ceases, it's not actually

17:58.240 --> 18:02.960
suffering itself, but only suffering because we cling to it, because we have expectations

18:02.960 --> 18:08.760
about it, and if we stop that, we start to turn our way, we start to desire and then

18:08.760 --> 18:16.640
climb towards peace and freedom, we start to lose our desire for any sort of a risen

18:16.640 --> 18:26.760
experience and the mind starts to quiet, and it starts to become quite certain and sure

18:26.760 --> 18:35.000
of nature of reality, it's easy, so clearly and so consistently that there's clearly

18:35.000 --> 18:38.960
nothing worth clinging to, there's no benefit that comes from holding on, I mean this

18:38.960 --> 18:52.400
is what you gain the culmination of the path, so this is the way you need to go through,

18:52.400 --> 18:58.520
for those of you practicing at home, it's much longer and slower and more difficult

18:58.520 --> 19:05.120
path, coming here might seem more difficult, those meditators here must certainly feel

19:05.120 --> 19:11.240
how difficult it is, but it's so much easier in comparison, there's not years and years

19:11.240 --> 19:17.680
or lifetimes of struggling just to gain basic insight into reality, there's so much

19:17.680 --> 19:24.720
insight that comes from being here, so much purification and cleansing that goes on in the

19:24.720 --> 19:36.720
mind, freeing yourself in such a short time it's a great blessing, and so all you need

19:36.720 --> 19:43.360
is patience and you have to walk the path, it's the main portion of our undertaking is

19:43.360 --> 19:49.960
to follow the path and to see clearly and to cultivate and accumulate this understanding

19:49.960 --> 20:00.680
of the three characteristics in nature, reality is not worth clinging to, learning to let

20:00.680 --> 20:09.440
go, and so the fourth is Pala, this is the fruit, when the mind finally lets go, at the

20:09.440 --> 20:20.640
end of the path the mind sees so clearly and it releases, let's go, no more seeking, no more

20:20.640 --> 20:26.040
racing out to see, what's that, I want to see it, what's that, I want to hear it, I

20:26.040 --> 20:34.080
mind no longer, oh no, what's going on over here, no longer, the mind gets fed up and

20:34.080 --> 20:44.800
says, enough, enough with you all, and it drops, and it quiets and becomes perfectly

20:44.800 --> 20:54.920
and completely silent, there's cessation even, cessation of a risen experience, this

20:54.920 --> 21:08.920
is Nimanda, and then Nimanda is something that is under risen, and so the mind enters

21:08.920 --> 21:18.520
into an under risen state, which is pure peace, this is the final wisdom, this is,

21:18.520 --> 21:24.120
might not sound like wisdom actually, but it is the most powerful wisdom, and maybe the word

21:24.120 --> 21:30.040
wisdom is in English, we use, we have too much baggage surrounding that term, but Bunya,

21:30.040 --> 21:40.180
but means perfectly, rightly, strongly, na knowledge, no, so when you know Nimanda,

21:40.180 --> 21:47.480
Nirvana, it's the highest knowledge, to know Nimanda, there's nothing greater, there's nothing

21:47.480 --> 21:53.800
that even compares, there's nothing that's, anything close to the experience of Nimanda,

21:53.800 --> 21:59.640
in terms of changing who you are, changing your life, changing your direction, changing your

21:59.640 --> 22:07.440
mind, freeing you from stress and suffering, but still quite simple, there's nothing

22:07.440 --> 22:12.680
hard to understand, it's a bit scary, I suppose, ooh, cessation, but I don't want to

22:12.680 --> 22:27.080
cease, I want to go, what will happen to me, right, if there were a mean, but there's

22:27.080 --> 22:34.960
nothing hard to understand about these things, they're quite simple, maybe that's not true,

22:34.960 --> 22:38.440
I think they are hard to understand, but the problem is that we make more on something

22:38.440 --> 22:45.040
that they actually are, they're hard for us to understand because we act and we function

22:45.040 --> 22:50.800
on such a more complex level, and enlightened being is so much simpler, if you read the

22:50.800 --> 22:56.400
Buddhist text, they seem somewhat aggravatingly simple, the Buddha will give a talk just

22:56.400 --> 23:00.400
about seeing, hearing, smelling, tasting, feeling, thinking, and if you're not a Buddhist

23:00.400 --> 23:07.280
meditative, you think, this is dumb, you know, this is too, you might think how simplistic

23:07.280 --> 23:12.200
this is or meaningless, you just can't make head or tail on it, it's like, why is he talking

23:12.200 --> 23:19.920
about seeing, remember hearing about Buddhism and I was in a guide book when I was in Thailand

23:19.920 --> 23:26.240
and it said, the Buddha taught that when you walk, just walk, when you stand, just stand

23:26.240 --> 23:30.320
or something like that, and I thought, well that's okay, okay, I was trying to get

23:30.320 --> 23:35.360
it, but inside I'm thinking, you know, that's him, what's even talking about, what does

23:35.360 --> 23:43.600
that mean, doesn't look like wisdom, this doesn't seem anything to do with wisdom.

23:43.600 --> 23:48.320
I went to Thailand looking for wisdom when I went to meditation center, I asked, they asked

23:48.320 --> 23:53.040
me, curiously, they asked me, why are you here, what do you hope to get from it and I said,

23:53.040 --> 23:57.360
I would like to gain wisdom, right, first thing out of my mouth was what I was looking

23:57.360 --> 24:06.680
for, but boy, the change that went to my understanding of the change that came in my understanding

24:06.680 --> 24:15.880
of what wisdom was, I realized how foolish, how ignorant I was of what wisdom really means,

24:15.880 --> 24:24.080
wisdom has nothing to do with knowledge, with thinking, concept, wisdom is when the stomach

24:24.080 --> 24:36.640
rises, you know that it's rising, that's wisdom, very profound, very simple, so there

24:36.640 --> 25:02.720
you go, that's the demo for tonight, thank you all for tuning in.

