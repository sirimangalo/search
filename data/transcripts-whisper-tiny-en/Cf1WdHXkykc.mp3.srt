1
00:00:00,000 --> 00:00:09,400
Hello. Good evening. Welcome to our live broadcast. Again, we're starting late because

2
00:00:09,400 --> 00:00:13,280
I've just done a dumb-apada video. So if you want to listen live to that, you can listen

3
00:00:13,280 --> 00:00:22,400
live to it, but I don't want to record a video for it twice. So we're starting late. If

4
00:00:22,400 --> 00:00:27,520
you want it to list, get from the beginning, you should go to meditation.serimungalow.org.

5
00:00:27,520 --> 00:00:34,160
And that's where we have our live group. That's where you can ask questions as well.

6
00:00:34,160 --> 00:00:37,920
And you'll have an audio feed through the whole of the broadcast, including the receiving

7
00:00:37,920 --> 00:00:50,160
and including the dumb-apada teacher. So a couple of announcements, right? At least one.

8
00:00:50,160 --> 00:00:57,320
That today we had 10 people come to our weekly meditation group, which is more than

9
00:00:57,320 --> 00:01:11,160
usual. Some old, we had five returners, four returnings, four for old, six new. And we're

10
00:01:11,160 --> 00:01:15,920
planning on putting up posters. So we'll put up a whole bunch more, just black and white

11
00:01:15,920 --> 00:01:21,120
posters, saying, what are you waiting for? Because the exams are coming up, so people

12
00:01:21,120 --> 00:01:26,920
should learn to meditate. And this Friday, we're going to have, we're going to try again with

13
00:01:26,920 --> 00:01:37,640
a meditation mom thing. Just have a small group of dedicated people in the main student

14
00:01:37,640 --> 00:01:48,440
center group. I think I had another announcement. Robin, do you have any more announcements?

15
00:01:48,440 --> 00:01:55,720
No, not until we get a little more information maybe on the children's home in Florida.

16
00:01:55,720 --> 00:02:11,000
We have a meditator coming to stay. Vanessa, who is an old, old meditator friend, who

17
00:02:11,000 --> 00:02:17,880
first, well, old, fairly old from Sri Lanka. She came to Sri Lanka. She's from Europe.

18
00:02:17,880 --> 00:02:23,640
Don't ask me which country. I want to say Austria, but I can't remember. She'll be here

19
00:02:23,640 --> 00:02:29,320
Saturday. She'll just email me or message me or something, asking if she needed to bring

20
00:02:29,320 --> 00:02:36,680
white clothes. No, we're not requiring white clothes. Maybe we should, but we should talk

21
00:02:36,680 --> 00:02:43,240
about that. I think it's technically it's in the rules, but we can just adapt the rules

22
00:02:43,240 --> 00:02:49,040
to say optional if you want. Well, that's right, but that was because that was a Cambodian

23
00:02:49,040 --> 00:02:55,600
monastery. I'm not pushing about that. I mean, what's the deal with white clothes? It's a nice

24
00:02:55,600 --> 00:03:02,000
tradition. Maybe it is something we should consider. It is a nice tradition. It's just hard. I mean,

25
00:03:02,000 --> 00:03:05,520
then you're forcing people to go out and buy clothes just so they can practice meditation because

26
00:03:05,520 --> 00:03:10,640
no one has white clothes. It was more of a thing in India because that was accepted and finding

27
00:03:10,640 --> 00:03:16,000
white clothes was not difficult, I think. It was just they were uncolored. And there is a point

28
00:03:16,000 --> 00:03:20,240
to that because people come with bright. In India, it was a big thing to have brightly colored

29
00:03:20,240 --> 00:03:28,960
clothes like a peacock. Even now, they have these bright saris and so that's kind of a no-no,

30
00:03:28,960 --> 00:03:38,160
but if you're just using, nobody can say neutrals. And it has to be, the thing is it has to be

31
00:03:38,160 --> 00:03:49,040
what's the word? Conservative? No. It's a modest. I think we had one in Jom-Tong. When I was at Jom-Tong,

32
00:03:49,680 --> 00:03:56,320
the kind of people that would come, we had one woman come with a skirt that had a slit all the way up

33
00:03:56,320 --> 00:04:02,800
to her or her, I don't know what you call it, but you could almost see her underfoot.

34
00:04:02,800 --> 00:04:09,760
Well, quite high, slit on the side and she's just trying to walk around like that.

35
00:04:09,760 --> 00:04:15,600
Young works fairly attractive women, I think. And the old nuns were not happy about that.

36
00:04:16,320 --> 00:04:23,920
Wait, another man wearing some kind of Middle Eastern. It's like a big night shirt.

37
00:04:23,920 --> 00:04:28,160
You know, those those night gown and night gown kind of thing. It was that kind of thing,

38
00:04:28,160 --> 00:04:33,760
but I think it's from the Middle East, just a white one piece. And that was all he was wearing.

39
00:04:33,760 --> 00:04:38,960
I don't even think he had underwear on her jeans. This is the kind of thing you hear.

40
00:04:39,520 --> 00:04:45,760
I think that's also a no-no. Like women wearing skin paint, shirts, that kind of thing,

41
00:04:45,760 --> 00:04:47,520
not really appropriate.

42
00:04:47,520 --> 00:04:59,520
I had, but when I was in Manitoba, it was hot, you know, really hot in the summer.

43
00:04:59,520 --> 00:05:06,320
And what I had one meditator staying with me and she wore shorts that were just above the knees.

44
00:05:06,320 --> 00:05:11,680
You know, they were actually quite long shorts. And people got really upset about that.

45
00:05:11,680 --> 00:05:17,760
And I was kind of thinking, you know, it would be nice if she was wearing pants, but it was so hot

46
00:05:17,760 --> 00:05:22,800
that I never said anything. It actually became a bit of an issue for not happy about it.

47
00:05:22,800 --> 00:05:33,120
Traditional places were not happy. But you know, in Canada, who thinks twice about cargo shorts, right?

48
00:05:33,120 --> 00:05:41,440
It's considered modest here and who is considered modest here. Very different things.

49
00:05:43,120 --> 00:05:47,680
Not that it's, there's not a point there because showing your legs off, it's showing your legs

50
00:05:47,680 --> 00:05:56,000
off, it's potential. Just because cleavage is acceptable, it doesn't mean it's not going to

51
00:05:56,000 --> 00:06:04,080
do arouse emotions that are problematic in the meditations and that kind of thing. So there are

52
00:06:04,080 --> 00:06:13,920
issues to talk about and none. It's regarding those. But yeah, so we'll have Vanessa would be here.

53
00:06:15,200 --> 00:06:22,640
And then we have a man who came in, pulling. There was another man John who was supposed to come,

54
00:06:22,640 --> 00:06:30,160
but I think he's got problems with him. Well, we had our first graduate, I forgot to mention,

55
00:06:30,160 --> 00:06:36,080
history day. Not quite graduate, but the first person that goes through the entire online course

56
00:06:37,040 --> 00:06:41,280
that it means he's not finished. He still has to come here and finish the course, but that's the

57
00:06:41,280 --> 00:06:54,160
thing he's now has to come here and finish. So that's my work. I know more than that. Yeah, so his slot

58
00:06:54,160 --> 00:07:02,000
is available. If you notice, I think he's taking himself off, hasn't it? I'm not sure. When was

59
00:07:02,000 --> 00:07:05,760
that? That was yesterday, right? Yes. So yesterday, there's a Tuesday slot available now.

60
00:07:05,760 --> 00:07:13,040
Yes. But he's already planning to come up here to finish the course next January, maybe in January.

61
00:07:15,360 --> 00:07:20,320
She looks like there are three available now. So if anyone is looking, there's three.

62
00:07:21,920 --> 00:07:28,560
If somebody wants to start this online course, we'll have to get some feedback as to how useful it is.

63
00:07:28,560 --> 00:07:35,200
People seem to be having getting benefit from it. No, it's not the same as doing

64
00:07:36,640 --> 00:07:44,480
intensive course or not the same as being in the presence of meditators and being with your teacher

65
00:07:44,480 --> 00:07:54,560
that kind of thing. But what we got seems to be useful. Just having that, you know, just having

66
00:07:54,560 --> 00:08:00,160
someone to prod you and to remind you and hanging over your head. Oh, I've got to meet with this

67
00:08:00,160 --> 00:08:04,320
with my teacher. So I better meditate. Quite useful.

68
00:08:07,600 --> 00:08:11,840
Maybe good preparation for going to do the last part in person.

69
00:08:13,680 --> 00:08:17,840
Yeah, I'm skeptical. I don't I mean, it's not the best preparation. I'm not sure how long it's

70
00:08:17,840 --> 00:08:22,400
going to take people to finish it. But what we have seen is people who have done that

71
00:08:22,400 --> 00:08:29,840
take about a week and can do the course in a week. Week is cutting it short. So preferably

72
00:08:29,840 --> 00:08:35,520
it would be like 10 days. But if you had 10 days, nine days, right, that's what we'd prefer

73
00:08:35,520 --> 00:08:43,360
is two weekends. So if you had nine days, really that weeks and weeks of training is a good

74
00:08:45,920 --> 00:08:51,280
preparation for the actual. But you'd need about nine days. We just don't don't want to throw

75
00:08:51,280 --> 00:08:55,040
you right into hardcore intensive practice. So we have to build it up because

76
00:08:55,760 --> 00:09:00,720
even with all that practice, you've still never done a full day of meditation, theoretically.

77
00:09:04,000 --> 00:09:09,600
We have another two questions. One is whether this online course we have to get people to sign a

78
00:09:09,600 --> 00:09:20,800
waiver. So we have to discuss that. We do, right? And two, I'd like to get everyone's commitment

79
00:09:20,800 --> 00:09:26,640
to keep rules. So at least five precepts, right? Shouldn't be doing this course if you're still

80
00:09:26,640 --> 00:09:32,160
drinking alcohol. But the five precepts, you should be keeping it in this course. But I think I have

81
00:09:32,160 --> 00:09:37,760
to, I haven't talked to people about that. That's my, that's my bad. I should have.

82
00:09:41,040 --> 00:09:45,440
Maybe from now on, I'll ask everyone, I'll remind them, okay. I'm starting something new.

83
00:09:45,440 --> 00:09:52,160
But what about the waiver, Robin? Well, we were discussing that. And currently we put on the

84
00:09:52,160 --> 00:09:59,360
website, the center rules for when you're there in person. And also some frequently asked

85
00:09:59,360 --> 00:10:05,280
questions. And, you know, more geared, more so geared toward the residential part of it.

86
00:10:05,280 --> 00:10:10,480
But there's also a meditator application in waiver, again geared toward the residential. But

87
00:10:10,480 --> 00:10:15,120
there was discussion about maybe it would be good to have the online meditator

88
00:10:15,120 --> 00:10:18,960
sign that as well. The last my father would he thinks he's a lawyer.

89
00:10:21,520 --> 00:10:25,280
Yeah. And he was very concerned with those sorts of things. I remember we were trying to play paint

90
00:10:25,280 --> 00:10:31,520
ball on our, on his land and he wouldn't let us, very almost wouldn't let us because your liability.

91
00:10:33,360 --> 00:10:35,040
Big, a big thing liability.

92
00:10:39,280 --> 00:10:42,640
Anyway, that's a bunch of boring stuff. Does anyone have any questions?

93
00:10:42,640 --> 00:10:43,840
Yes.

94
00:10:44,880 --> 00:10:48,800
Month, they will you be doing anything in Florida, which part will you be staying in?

95
00:10:49,760 --> 00:10:54,720
I'll be in Tampa area. Dunedin's what I'll be staying. But yeah, we'll be doing at least a one day

96
00:10:54,720 --> 00:11:01,280
course in at the Florida Buddhist we hire rights on their website. So if you go to the Florida

97
00:11:01,280 --> 00:11:07,200
Buddhist we hire a website, I'm pretty sure they've got an event. And don't ask me which day

98
00:11:07,200 --> 00:11:13,440
to start. We can link that post.

99
00:11:13,440 --> 00:11:29,120
Actually, this one is dated June of 2014. So it seems to be the outdated one.

100
00:11:37,600 --> 00:11:40,560
Oh, here's the one December 26 2015.

101
00:11:40,560 --> 00:11:44,480
26 the boxing day.

102
00:12:11,200 --> 00:12:31,520
I'm trying to send a link there and it seems to be frozen up, but I'll try that again in a moment.

103
00:12:32,080 --> 00:12:36,400
Hello, Bante. Although we are encouraged to ask questions and talk about our problems,

104
00:12:36,400 --> 00:12:41,600
and of course, we all experience various problems or difficulties. But I'm interested to know,

105
00:12:41,600 --> 00:12:46,080
as you are a very experienced practitioner, are there any parts of the practice that you

106
00:12:46,080 --> 00:12:50,480
still find difficult? Brilliant, I'm a professor.

107
00:12:50,480 --> 00:12:54,720
I'm really talking about my own practice. Sorry.

108
00:12:54,720 --> 00:13:10,640
There's just too many problems associated with it.

109
00:13:25,040 --> 00:13:35,120
You seem to be having a little trouble with my meditation of websites here. It's frozen up.

110
00:13:35,120 --> 00:13:42,400
I'm just going to grab my phone.

111
00:13:42,400 --> 00:13:55,680
Okay, that was it for the questions, Bante.

112
00:13:58,240 --> 00:14:03,680
All right, well, we had the dumbapada, so that's my number for tonight.

113
00:14:05,040 --> 00:14:09,360
Let's just stop there then. Thank you, Bante.

114
00:14:09,360 --> 00:14:14,800
Thank you, Rubble. Good night, everyone. Good night.

