1
00:00:00,000 --> 00:00:21,680
Good evening, everyone broadcasting live from Stony Creek, Ontario and back this evening.

2
00:00:21,680 --> 00:00:36,680
Today we have a quote, a quote in the top of our meditation page.

3
00:00:36,680 --> 00:00:41,400
Maybe we should start by having someone read the quote off in the hangout.

4
00:00:41,400 --> 00:00:42,400
No?

5
00:00:42,400 --> 00:00:46,960
It's too bad, Robin's gone away.

6
00:00:46,960 --> 00:00:50,680
Is there anybody else out there who wants to join the hangout?

7
00:00:50,680 --> 00:01:09,080
If you can invite you in, if you're on Google+, give me your name, I guess for today

8
00:01:09,080 --> 00:01:15,360
I'll read it myself, that's a long one though.

9
00:01:15,360 --> 00:01:20,800
I tell you this, let an intelligent person who is sincere, honest, and straightforward

10
00:01:20,800 --> 00:01:26,960
come to me, and I will instruct him, I will teach him dhamma.

11
00:01:26,960 --> 00:01:33,000
If he practices as he is taught, then in seven years he will attain in this very life,

12
00:01:33,000 --> 00:01:37,840
by his own knowledge and vision, that goal for the sake of which young men go forth from

13
00:01:37,840 --> 00:01:43,160
home into homelessness, and he will abide in it.

14
00:01:43,160 --> 00:01:48,360
Never mind seven years, he will be able to do it in seven days.

15
00:01:48,360 --> 00:01:53,080
Now you may think, the monk Gautama only says this in order to get disciples, but this

16
00:01:53,080 --> 00:01:58,920
is not so, that he is your teacher, be your teacher still.

17
00:01:58,920 --> 00:02:04,800
You may think he wants us to give up our commandments, but this is not so, continue to live

18
00:02:04,800 --> 00:02:12,280
by your commandments, or you may think he wants us to give up our way of life, but this

19
00:02:12,280 --> 00:02:17,520
is not so, continue to live your way of life.

20
00:02:17,520 --> 00:02:22,840
Or perhaps you will think he wants us to practice things that are wrong or not practice

21
00:02:22,840 --> 00:02:26,680
things that are right according to our teaching.

22
00:02:26,680 --> 00:02:33,400
But this is not so, continue to avoid the things your teaching considers wrong and practice

23
00:02:33,400 --> 00:02:38,040
the things that your teaching considers right.

24
00:02:38,040 --> 00:02:44,640
But there are unskillful things not yet given up, things tainted, leading to rebirth,

25
00:02:44,640 --> 00:02:54,000
fearful, with painful result in the future, things associated with birth, decay, and death.

26
00:02:54,000 --> 00:02:57,720
And it is for the giving up of these things that I teach the number.

27
00:02:57,720 --> 00:03:03,360
However, if you practice correctly, these tainted things will be given up, and the things

28
00:03:03,360 --> 00:03:07,800
that lead to the purification will grow and develop.

29
00:03:07,800 --> 00:03:12,840
You will attain the fullness of perfected wisdom by your own knowledge and vision, and

30
00:03:12,840 --> 00:03:19,280
abide in it in this very life.

31
00:03:19,280 --> 00:03:41,360
This is a quote that is quite dear to me, and I suppose it is simply a quote that is useful

32
00:03:41,360 --> 00:03:51,840
to refer to when, especially when talking to people who aren't Buddhist, to explain to

33
00:03:51,840 --> 00:03:58,920
them our stance and the requirements we have, or the requirements which in fact we don't

34
00:03:58,920 --> 00:04:07,160
have for people wishing to practice the Buddhist teaching.

35
00:04:07,160 --> 00:04:15,600
Today I came back from British Columbia, got to the airport in Vancouver, and I came out

36
00:04:15,600 --> 00:04:24,200
of the washroom, and just as they were making an announcement, anyone on Air Canada,

37
00:04:24,200 --> 00:04:29,040
who would like to take an earlier flight, please come to the counter.

38
00:04:29,040 --> 00:04:33,040
I was walking to the counter anyway, I walked up to the counter, I said do you need people

39
00:04:33,040 --> 00:04:39,560
to take an earlier flight, they said yes, they got me on the earlier flight with the worst

40
00:04:39,560 --> 00:04:46,520
seat, the worst seat on the plane, very, very back in the middle seat between two people.

41
00:04:46,520 --> 00:04:55,840
But as chance added, the flight I ended up on, the man sitting next to me, and three

42
00:04:55,840 --> 00:05:07,240
of the stewardesses ended up, sorry, flight attendants ended up asking me for advice on

43
00:05:07,240 --> 00:05:08,240
meditation.

44
00:05:08,240 --> 00:05:16,480
The man sitting next to me had been doing 10-day Vipassana meditation courses, and

45
00:05:16,480 --> 00:05:24,880
so we talked about Vipassana, and actually we talked quite a bit about philosophy, and he

46
00:05:24,880 --> 00:05:31,560
said so who am I, we're talking, I mentioned something that made him ask who am I, let's

47
00:05:31,560 --> 00:05:39,160
we talked about that for a while, he asked what was the purpose of life, why are we here

48
00:05:39,160 --> 00:05:47,160
or something like that, and I said he's free from Spain, so his English wasn't so good,

49
00:05:47,160 --> 00:05:53,080
and he said you know the game monopoly, and in fact he did know the game monopoly, and

50
00:05:53,080 --> 00:05:59,160
he said well you look at monopoly, it's a sort of a game that has a clear goal, there's

51
00:05:59,160 --> 00:06:04,040
a purpose, and we don't, I can't remember what the exact goal of monopoly is, but there

52
00:06:04,040 --> 00:06:10,200
is one, he's bankrupt everybody else, and then you win the game, that's a game with

53
00:06:10,200 --> 00:06:15,600
a purpose, but then I said you know Lego, and yes he knew Lego, and I said what's the

54
00:06:15,600 --> 00:06:26,360
purpose of Lego, and you know I said by the point that there's not a set goal, so the

55
00:06:26,360 --> 00:06:32,400
problem is that we look at life from a paradigm, usually a Judeo-Christian paradigm, or

56
00:06:32,400 --> 00:06:41,680
biblical theological, theistic paradigm, where which is a lot like monopoly, but the world

57
00:06:41,680 --> 00:06:47,920
isn't like that, the world is more like Lego, you can build it, and so I made this analogy

58
00:06:47,920 --> 00:06:54,160
for him, and he thought about it, and he came back and he said yeah, but even with monopoly

59
00:06:54,160 --> 00:07:04,400
with Lego the real point is to enjoy yourself, and I said yeah, but you know after a while

60
00:07:04,400 --> 00:07:15,480
you get bored of the Lego, this is Buddhism, eventually you get bored of the Lego, and then

61
00:07:15,480 --> 00:07:22,440
we talked more about the idea of the flaw in the idea of how enjoyment, being the goal

62
00:07:22,440 --> 00:07:30,240
of life, you know the reason for living is to enjoy, because I said happiness doesn't

63
00:07:30,240 --> 00:07:36,960
need the happiness, this you can quote me on, I think I'm pretty unique in having said

64
00:07:36,960 --> 00:07:41,560
that, I don't know, anyway it's very Buddhist, I think it's a very Buddhist thing to say,

65
00:07:41,560 --> 00:07:45,840
I can't think of where exactly the Buddha may have said it, but happiness doesn't

66
00:07:45,840 --> 00:08:00,320
need to happiness necessarily, goodness leads to happiness necessarily, so I said if you take

67
00:08:00,320 --> 00:08:07,880
these two goals, one person has the goal of enjoying life, this is their goal, another person

68
00:08:07,880 --> 00:08:15,720
has the goal of being good or cultivating goodness, the funny thing is the one cultivating

69
00:08:15,720 --> 00:08:22,440
goodness who has made me no interest and eventually no interest in cultivating enjoyment,

70
00:08:22,440 --> 00:08:30,200
he actually turns out to be the happier one, and so we talked about that, and then one

71
00:08:30,200 --> 00:08:36,680
of the flight attendants was from Vietnam, which is Canadian, but she was born in Vietnam,

72
00:08:36,680 --> 00:08:44,200
so she asked me, she asked me to bless her, her mala, but we talked and she was interested

73
00:08:44,200 --> 00:08:50,040
in meditation and she wants to come and visit me here, and two other of the flight attendants

74
00:08:50,040 --> 00:08:55,000
as well, and I had a couple of extra, a bunch of extra books, so I gave up my booklet

75
00:08:55,000 --> 00:08:59,760
and we talked, and the guy on this side who was sitting in the aisle seat, I don't

76
00:08:59,760 --> 00:09:05,640
know, he wasn't in the same at all interested, so we were talking over him, I don't know

77
00:09:05,640 --> 00:09:13,560
how he felt about it all, but this quote makes me think back of that on that, and remarked

78
00:09:13,560 --> 00:09:23,920
that really this is what it's all about, sharing cultivating goodness by giving, by sharing

79
00:09:23,920 --> 00:09:38,440
the teachings, meaning teaching is open without any strings attached, that's my point,

80
00:09:38,440 --> 00:09:45,440
and is sharing, giving without any strings attached, not even requiring people to become

81
00:09:45,440 --> 00:09:47,280
Buddhist, right?

82
00:09:47,280 --> 00:09:52,280
My favorite part here is the monk Gotama only says this in order to get disciples, but

83
00:09:52,280 --> 00:09:59,000
this is not so, that whom who is your teacher, be your teacher still, that's powerful,

84
00:09:59,000 --> 00:10:03,120
and it's a powerful reminder that we're not looking for students, not looking for people

85
00:10:03,120 --> 00:10:15,480
to say, this is my teacher, I am his, her student, apologize also for the masculine pronouns

86
00:10:15,480 --> 00:10:17,480
and this, it's all masculine, right?

87
00:10:17,480 --> 00:10:26,760
It doesn't really have to be, I will instruct them, we could change all of these, anyway.

88
00:10:26,760 --> 00:10:34,440
It's an awesome quote, it's a direct quote, one of the most authentic, if you believe

89
00:10:34,440 --> 00:10:40,840
any of them to be authentic, this is from a set that is among the most likely to be authentic,

90
00:10:40,840 --> 00:10:48,280
so this is what the Buddha actually said, as far as we know, and it basically says that,

91
00:10:48,280 --> 00:11:00,000
salayans were really, it's exemplifying the complete purity of the Buddha in intention

92
00:11:00,000 --> 00:11:04,160
in terms of just helping, you practice these things, they will help you, this is the

93
00:11:04,160 --> 00:11:11,120
lions where we have, I don't need you as my disciple, we're not looking from converts,

94
00:11:11,120 --> 00:11:20,160
we're not asking you to give up the practices in your own religion, but, and the point

95
00:11:20,160 --> 00:11:28,760
is, with all of the things that you're doing now, our claim to those people who are

96
00:11:28,760 --> 00:11:34,040
not practicing meditation is that you're missing something, they're, meaning with all

97
00:11:34,040 --> 00:11:39,240
of that, there are still unskillful things not yet given up, that's the implication you're

98
00:11:39,240 --> 00:11:45,680
meaning with all of the things that you are doing, and this speech was actually given

99
00:11:45,680 --> 00:11:52,120
to a bunch of ascetics, the Buddha went to see them, if you read the sutta, it's, let's

100
00:11:52,120 --> 00:12:01,600
say, it's the Gennikaya number 25, ullum bar, ullum barikas, sihanada sutta, it's the lions

101
00:12:01,600 --> 00:12:14,160
roar to the ullum barikas, so the ullum barikans were, I don't know, a bunch of naked ascetics

102
00:12:14,160 --> 00:12:20,120
maybe, the Buddha said this to them, and this is the lions roar that he gave, this is one

103
00:12:20,120 --> 00:12:27,360
of the great lions roars means, it's a bold statement, something that roars and just anyone

104
00:12:27,360 --> 00:12:33,920
who listens to it, you would think would be moved by it, as soon as he says this, the

105
00:12:33,920 --> 00:12:39,240
next paragraph reads, at these words, the wanderers sat silent and upset, their shoulders

106
00:12:39,240 --> 00:12:44,560
drooped, they hung their heads and sat their downcast and bewildered, so possessed

107
00:12:44,560 --> 00:12:50,720
were their minds by evil, then the Lord said, the Buddha said, every one of these foolish

108
00:12:50,720 --> 00:12:56,000
men is possessed by the evil one, so that not a single one of them thinks, let us now follow

109
00:12:56,000 --> 00:13:00,800
the holy life proclaim, but he said it go to man, that we may learn it, for what to seven

110
00:13:00,800 --> 00:13:06,160
days matter, and the point is, you know, why not try it, if his claim is that we can get

111
00:13:06,160 --> 00:13:16,280
this in seven days, you know, what's the problem, let's try it for seven days, but not

112
00:13:16,280 --> 00:13:36,160
one of them thought that because they were so fully possessed by evil, evil thoughts,

113
00:13:36,160 --> 00:13:45,200
so that's our quote for tonight, let's go for some questions, I'm Brooklyn's mentions

114
00:13:45,200 --> 00:13:51,400
the arrow on shine in Bangkok, should maybe give a moment of silence for that, send our

115
00:13:51,400 --> 00:13:54,920
good thoughts, everyone should send their good thoughts, let's have a moment of silence

116
00:13:54,920 --> 00:14:03,960
here, online, live, where we all close our eyes and send good thoughts to Thailand, to

117
00:14:03,960 --> 00:14:09,240
the people who passed away that their spirits might find peace, to the people who are left

118
00:14:09,240 --> 00:14:14,280
afraid, worried about their country, really I'll find peace.

119
00:14:39,240 --> 00:15:06,280
How to make sense or live with such reckless hate in the destruction it brings, would love

120
00:15:06,280 --> 00:15:21,560
to hear anyone's ideas on reconciling us, you know, I don't know, I mean, that's part

121
00:15:21,560 --> 00:15:26,480
of Samsara, if anything it wakes should wake us up to the fact that we're none of us

122
00:15:26,480 --> 00:15:44,120
really safe, suffering death comes to all, suffering comes to all, we should not be negligent,

123
00:15:44,120 --> 00:15:55,040
the problem is we think we have all this time, we think everything's safe and stable,

124
00:15:55,040 --> 00:16:03,960
we're like chickens, ducks, Mahasizaya, that talks about these ducks or chickens that walk

125
00:16:03,960 --> 00:16:09,360
around pecking the ground as though they had so long to live and yet they're all going

126
00:16:09,360 --> 00:16:19,760
to be slaughtered, if you look at animals you can see this very clearly when they bite and

127
00:16:19,760 --> 00:16:27,120
hurt each other and then the next moment they pretend that everything's fine, human beings

128
00:16:27,120 --> 00:16:36,920
are, we have very much the same behavior, we're negligent even when we have the writing

129
00:16:36,920 --> 00:16:46,040
on the wall, the hints all around us, we still act as though it will never happen to us,

130
00:16:46,040 --> 00:16:58,080
it actually happens to all of us in some form or another, is Vipassana meditation comprised

131
00:16:58,080 --> 00:17:03,440
of entirely noting sensations and thoughts or is there a point where we are supposed

132
00:17:03,440 --> 00:17:10,560
to move past the rising and falling of phenomena and do something else, it sounds like

133
00:17:10,560 --> 00:17:15,160
you might be talking about a different tradition because in our tradition Vipassana

134
00:17:15,160 --> 00:17:21,040
meditation is comprised of the four Vipassana, the body, the feelings, the mind and the

135
00:17:21,040 --> 00:17:29,680
dhammas and it's completely entirely based on, you see, noting, so it's based on our

136
00:17:29,680 --> 00:17:34,840
noting when we say rising, falling, that's the body or when you say sifting, when you walk

137
00:17:34,840 --> 00:17:44,320
and say walking, walking, that's body, when you feel pain, pain, thinking, thinking,

138
00:17:44,320 --> 00:17:51,640
thinking, disliking, drowsiness, distraction doubt, seeing, hearing, smelling, tasting,

139
00:17:51,640 --> 00:17:57,920
feeling, thinking, but yes, Vipassana meditation is just that, the four Satipatana, same

140
00:17:57,920 --> 00:18:06,080
practice, you don't ever do anything else, once you practice that to fulfilment, there's

141
00:18:06,080 --> 00:18:12,080
the attainment of cessation, which is the realization of nibana, which is freedom, the

142
00:18:12,080 --> 00:18:29,440
mind, let's go.

143
00:18:29,440 --> 00:18:35,280
Some at the meditation could be a bit distracting, well, some at the meditation may be not

144
00:18:35,280 --> 00:18:37,440
distracting, no?

145
00:18:37,440 --> 00:18:41,520
Maybe it could, I don't know that, that's exactly what I said, maybe it was, sometimes

146
00:18:41,520 --> 00:18:44,640
I say things that are not exactly accurate.

147
00:18:44,640 --> 00:18:57,680
Some at the meditation can be lead to complacency, so not exactly distracting, but it can

148
00:18:57,680 --> 00:19:15,000
go you down, potentially, it doesn't have to, but it does, in practice, have this effect.

149
00:19:15,000 --> 00:19:24,800
Where it keeps you from progressing, but it can also be very helpful for Vipassana meditation,

150
00:19:24,800 --> 00:19:36,320
giving you good concentration, as long as you then know to use it for the practice of Vipassana.

151
00:19:36,320 --> 00:19:42,680
The guy waiting to get into the monastery, no, that's not kung fu series, the kung fu

152
00:19:42,680 --> 00:19:57,480
series, I don't even have a microphone here, there's anyone listening to the microphone,

153
00:19:57,480 --> 00:20:06,880
if you were listening to the live screen, I apologize for the very, very low volume, that

154
00:20:06,880 --> 00:20:16,600
was certainly, yeah, it should be louder now, kung fu series, how did that go, something

155
00:20:16,600 --> 00:20:17,600
different?

156
00:20:17,600 --> 00:20:23,040
He got, he was forced to go stay in the monastery, now the king of American Shaolin or

157
00:20:23,040 --> 00:20:28,280
something, that's the movie I'm always referring to, I think it's something like King

158
00:20:28,280 --> 00:20:42,760
of American Shaolin, I think it's the whole movie's on YouTube, go look for it, should

159
00:20:42,760 --> 00:20:50,680
we accept inequality, inequity in the world as part of the human evolution, I don't

160
00:20:50,680 --> 00:21:00,800
know about the word human evolution, I think you have to be careful using that word, that's

161
00:21:00,800 --> 00:21:09,840
part of the human state inequality is part of the nature of karma we would say, a lot

162
00:21:09,840 --> 00:21:16,320
of it just has to do with karma, rich people who are stingy become poor people, poor

163
00:21:16,320 --> 00:21:25,000
people who are kind and generous when they pass away, they become rich people.

164
00:21:25,000 --> 00:21:28,840
So unless everyone's going to have the same karma and be the same, have the same level

165
00:21:28,840 --> 00:21:34,800
of kindness and generosity, not everyone is going to be born into a situation where they

166
00:21:34,800 --> 00:21:48,960
have the same capabilities, that being said, I think the more kind and generous people are,

167
00:21:48,960 --> 00:21:51,800
the more equality there is.

168
00:21:51,800 --> 00:21:58,480
So if the world where more people became more kind, with people always giving to those

169
00:21:58,480 --> 00:22:08,800
who needed, right, there would be more equality, rich people would give to the poor people

170
00:22:08,800 --> 00:22:16,760
so it would level out, whereas the more stingy and unkind people are, the more inequality

171
00:22:16,760 --> 00:22:17,760
there is.

172
00:22:17,760 --> 00:22:22,600
Now that doesn't quite make sense because if everyone were stingy then everyone would

173
00:22:22,600 --> 00:22:28,440
start to be poor, which is probably why you see nowadays most people are poor because

174
00:22:28,440 --> 00:22:33,480
we're very stingy and we just, on a whim we decided to be very generous and then, no,

175
00:22:33,480 --> 00:22:37,320
we're suddenly very, very rich for one life and then back down to be very poor because

176
00:22:37,320 --> 00:22:42,440
when we were rich, we were greedy and that stingy and evil.

177
00:22:42,440 --> 00:22:46,720
I mean, that's a karmic perspective.

178
00:22:46,720 --> 00:22:53,480
It certainly doesn't mean that we should accept and say, oh, well, let the rich people

179
00:22:53,480 --> 00:22:59,960
keep their wealth, no, if you're rich you have a duty, you want to stay rich, you want

180
00:22:59,960 --> 00:23:06,200
to be a rich person really to have them get the best out of their money, they should

181
00:23:06,200 --> 00:23:14,200
give most of it or at least half of it away, I would say, maybe a quarter of it away,

182
00:23:14,200 --> 00:23:25,440
at least everyone did that.

183
00:23:25,440 --> 00:23:29,520
How do you judge a good meditator as opposed to a less skilled meditator?

184
00:23:29,520 --> 00:23:42,880
Well, you don't judge meditators against each other, you shouldn't, well, it's pretty

185
00:23:42,880 --> 00:23:47,840
easy, really, I mean, when you give exercises to the meditator, do they come back and say

186
00:23:47,840 --> 00:23:53,240
they could do it or do they say they couldn't do it, when the meditator tells you about

187
00:23:53,240 --> 00:23:58,800
their practice, are they able to talk about their experiences, do they tell you what they

188
00:23:58,800 --> 00:24:04,080
did and relate how they were mindful or do they come and ask you all sorts of questions

189
00:24:04,080 --> 00:24:12,160
about the self and argue with you about Buddhist philosophies, though they don't even believe,

190
00:24:12,160 --> 00:24:17,280
or they don't even accept it, you know, then you think, oh, this person has some problems

191
00:24:17,280 --> 00:24:22,920
because sometimes they just come and argue with you or more so they come and complain,

192
00:24:22,920 --> 00:24:27,400
which is fine, you know, complain is a problem, some people really have difficulty

193
00:24:27,400 --> 00:24:31,600
with the practice and that's fine, but that person you would say, okay, is going to have

194
00:24:31,600 --> 00:24:36,720
a harder time with it, that's fine, it's not to be discouraged, but some people practice

195
00:24:36,720 --> 00:24:43,360
and practice very easily, I wasn't one such person, so I see no shame in being, I can't

196
00:24:43,360 --> 00:24:51,360
look down on someone for being a poor meditator, but I was a fairly poor meditator, I've

197
00:24:51,360 --> 00:25:02,080
been, it's not difficult to recognize good and poor meditators, it's easy to be tricked,

198
00:25:02,080 --> 00:25:12,160
but I suppose by a meditator if they tell you all the right things, so if a meditator

199
00:25:12,160 --> 00:25:16,000
comes to you and their practice is too good, like if they say, oh, everything's fine

200
00:25:16,000 --> 00:25:20,400
and I'm doing really well and I'm really enjoying the practice, then sometimes you can

201
00:25:20,400 --> 00:25:25,720
be suspicious that they're not really practicing, but many times, I mean, it is possible

202
00:25:25,720 --> 00:25:34,080
to have a pleasant practice, but it's also quite possible to not really practice mindfulness

203
00:25:34,080 --> 00:25:42,400
and therefore have a pleasant but useless practice.

204
00:25:42,400 --> 00:25:46,560
What is the difference between the right hand path and the middle way?

205
00:25:46,560 --> 00:25:52,280
I don't know the right hand path, I've never heard of that, we have the right path and

206
00:25:52,280 --> 00:25:56,640
we have the middle way, and actually they are the one and the same thing, but I don't

207
00:25:56,640 --> 00:26:01,440
know right hand, if you mean the right path, that is the middle way.

208
00:26:01,440 --> 00:26:07,640
The right path of the Buddha is the eightfold number path, which is not an extreme path,

209
00:26:07,640 --> 00:26:22,480
so it's not torturous and it's not indulgent, it's balanced.

210
00:26:22,480 --> 00:26:25,400
What types of things should be unlocked while meditating?

211
00:26:25,400 --> 00:26:36,360
I'm sorry, I don't understand the question, not familiar with that terminology, or maybe

212
00:26:36,360 --> 00:26:42,680
I'm just too tired, it's been a long day, weekend, take your pick.

213
00:27:36,360 --> 00:27:45,200
Yep, I made it back in time, so I'm here to do the stream, it's nice to see people

214
00:27:45,200 --> 00:27:54,920
coming out.

215
00:27:54,920 --> 00:28:08,080
Maybe we'll stop it there though, if you have any more questions, you can save them for

216
00:28:08,080 --> 00:28:25,280
tomorrow, I'll say good night there, thank you all for tuning in, you will.

