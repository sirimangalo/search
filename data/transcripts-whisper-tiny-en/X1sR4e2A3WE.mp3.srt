1
00:00:00,000 --> 00:00:05,600
Is it okay to bring specific issues up during meditation as opposed to just

2
00:00:05,600 --> 00:00:10,760
allowing an experience to rise naturally? Like if I get angry at work I'd like

3
00:00:10,760 --> 00:00:15,240
to examine the source of it during my meditation time but it may not come up

4
00:00:15,240 --> 00:00:21,640
naturally. I've answered this one before I'm not sure if I've answered it on

5
00:00:21,640 --> 00:00:27,600
the internet but it's a common question and I'll just rather than answer it

6
00:00:27,600 --> 00:00:32,640
directly I'll point out the problem with it. No, I mean the answer is no but why?

7
00:00:32,640 --> 00:00:37,200
Well because there's good in it. You can see the potential good in in doing this

8
00:00:37,200 --> 00:00:41,640
the things that you really have to deal with come up and you can deal with them.

9
00:00:41,640 --> 00:00:49,440
The problem with that is this bringing up you know that is a karma that is

10
00:00:49,440 --> 00:00:55,240
there's something behind that there's the delusion of self there's greed there's

11
00:00:55,240 --> 00:01:00,440
maybe anger there's there's defilements involved with that. Most likely defilements

12
00:01:00,440 --> 00:01:06,000
involved with that subtle delusion and probably greed and anger. There's

13
00:01:06,000 --> 00:01:11,560
unwholesumness involved and you're cultivating that. Every time you bring

14
00:01:11,560 --> 00:01:15,400
something up you're cultivating the idea of self the idea that you have the

15
00:01:15,400 --> 00:01:22,600
power to bring this up and and that eventually becomes a view and it

16
00:01:22,600 --> 00:01:28,800
corrupts. So it's one of those those practices that it would say is black and

17
00:01:28,800 --> 00:01:33,600
white karma because it's doing something good but it has a bad result as well and

18
00:01:33,600 --> 00:01:39,160
so no we don't do that ideally and and sometimes you can make exceptions.

19
00:01:39,160 --> 00:01:42,520
There are exceptions you could see if you don't have enough time if you're

20
00:01:42,520 --> 00:01:50,280
you're you know there could be a reason for accepting the bad the need or it

21
00:01:50,280 --> 00:01:54,040
not being bad karma because there's something some extenuating circumstances

22
00:01:54,040 --> 00:01:59,520
that require it. Often the teacher will bring things up for students will

23
00:01:59,520 --> 00:02:06,560
will prod the student into experiencing something that they're avoiding for

24
00:02:06,560 --> 00:02:12,800
example. But for the most part we try to be patient and because we're trying to

25
00:02:12,800 --> 00:02:17,640
cultivate objectivity we have to be objective about the states that arise that's

26
00:02:17,640 --> 00:02:25,760
important and so as a result pushing something to come up is is problematic.

