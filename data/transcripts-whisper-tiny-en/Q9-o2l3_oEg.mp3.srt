1
00:00:00,000 --> 00:00:09,000
Okay, good evening, everyone. Welcome. Tonight's done, let's talk.

2
00:00:11,000 --> 00:00:16,800
I'm not going to be that long tonight, we'll see, but I have something short, but

3
00:00:16,800 --> 00:00:25,820
important to bring us back to make us think of. It's what I'd like to talk about

4
00:00:25,820 --> 00:00:40,000
tonight is the Badekarata Sutta, which often translated as the single

5
00:00:40,000 --> 00:00:52,600
excellent night. It's the Sutta on how to have one good day, actually, and in

6
00:00:52,600 --> 00:00:59,680
polity, they use the word ratty, which means night to signify a day. They

7
00:00:59,680 --> 00:01:08,180
come by the nights. So in English, we count by days, so we say how many days. And so

8
00:01:08,180 --> 00:01:18,360
we'd say one good day. Why it's interesting to talk about this is because the

9
00:01:18,360 --> 00:01:24,300
Buddha found it seems to have found it fairly outstandingly important,

10
00:01:24,300 --> 00:01:28,660
something that he taught repeatedly. We have and the people who put

11
00:01:28,660 --> 00:01:37,020
together the compilation and the Buddha's teaching that we have gave us four

12
00:01:37,020 --> 00:02:01,020
different Sutta's on Badekarata in the Maji Minikaya 31 to 34. One good day,

13
00:02:01,020 --> 00:02:08,620
something my teacher would often, quite often, bring up. It sums up, it's one of

14
00:02:08,620 --> 00:02:15,540
those things that suit us, that sums up mindfulness quite well, and it's quite

15
00:02:15,540 --> 00:02:29,820
useful as advice for meditators. I mean, just in and of itself, it's a teaching,

16
00:02:29,820 --> 00:02:41,220
how to have a good day. Because what it says is it says, first of all, how to be

17
00:02:41,220 --> 00:02:51,100
perfect, but also how to live now. We're talking about having a good day. This is

18
00:02:51,100 --> 00:02:57,280
the answer to our questions. How do we turn our hopes and our desires and our

19
00:02:57,280 --> 00:03:04,140
wants and our wishes? How do we find what we're looking for here and now? How do

20
00:03:04,140 --> 00:03:15,340
we make it real? So we can have that day. And also, according to Buddhism, what

21
00:03:15,340 --> 00:03:20,620
does it mean to have a good day? Right? Because for most of us, most people in

22
00:03:20,620 --> 00:03:24,500
the world, a good day is something quite different from what the Buddha had in

23
00:03:24,500 --> 00:03:33,580
mind. And that kind of good day is not, it's not terribly meaningful. I guess when

24
00:03:33,580 --> 00:03:37,540
we're talking about having a good day, it's something that, well, that's just

25
00:03:37,540 --> 00:03:43,620
single day, right? I know the Buddha meant something quite different. How do you

26
00:03:43,620 --> 00:03:56,860
get to the point where your day is good in a transformative way? So it's not just

27
00:03:56,860 --> 00:04:04,300
you have your good day and then it's over. You get to the point where you achieve, you

28
00:04:04,300 --> 00:04:17,820
accomplish, you become something worthwhile in that one day. So it starts with a

29
00:04:17,820 --> 00:04:25,840
very famous, I think, well-known teaching of the Buddha. Adhita Nanwakami and

30
00:04:25,840 --> 00:04:35,700
Apatigankayana Agatam, that will not go back to the past nor worry about the future. You

31
00:04:35,700 --> 00:04:38,940
may not have heard those words exactly before, but you know that this is what the Buddha

32
00:04:38,940 --> 00:04:47,460
taught. Don't go back to the past. Don't bring up the past. Don't worry about the future.

33
00:04:47,460 --> 00:04:57,460
Adhita Nanwakami Adhita Nanwakami Agatam, what's in the past is gone already, what's in

34
00:04:57,460 --> 00:05:06,380
the future has not yet come. And this is quite, it's obvious, right? This is not a

35
00:05:06,380 --> 00:05:12,100
teaching that that we have to be taught, you think. I don't suppose there's anyone

36
00:05:12,100 --> 00:05:17,400
here who didn't know that the past has already gone or that the future hasn't come yet.

37
00:05:17,400 --> 00:05:25,480
But it's important to repeat.

38
00:05:25,480 --> 00:05:34,240
We act as though the future is here and that the future is a certain thing.

39
00:05:34,240 --> 00:05:37,400
We act as though the past is here as well.

40
00:05:37,400 --> 00:05:41,240
When we think about the past, it makes us suffer.

41
00:05:41,240 --> 00:05:51,000
The bad things make us suffer just as they did when they actually happened.

42
00:05:51,000 --> 00:05:56,760
When we think about good things, we pine away after them.

43
00:05:56,760 --> 00:06:02,400
We never really… well, we often don't live in the present.

44
00:06:02,400 --> 00:06:09,000
We caught up in the past in the future, pushed and pulled.

45
00:06:09,000 --> 00:06:12,280
And in fact, the only thing that's real and the only thing that ever has been or ever will

46
00:06:12,280 --> 00:06:18,560
be real is now, is the present.

47
00:06:18,560 --> 00:06:23,360
Don't go back to the past, don't go ahead to the future.

48
00:06:23,360 --> 00:06:36,240
You know, these people who make plans, make plans for the future, only to have them ruined

49
00:06:36,240 --> 00:06:42,400
by reality, by the uncertainty of life.

50
00:06:42,400 --> 00:06:48,600
In fact, you might say that all of our disappointments in life come from making plans.

51
00:06:48,600 --> 00:06:54,360
If we didn't have expectations, if we didn't live in the future and think, maybe tomorrow,

52
00:06:54,360 --> 00:06:58,240
I'll get this or that, maybe tomorrow I'll have what I want.

53
00:06:58,240 --> 00:07:03,680
If we weren't looking for something, hey, even just the next moment, if I open the fridge,

54
00:07:03,680 --> 00:07:11,080
there will be some delicious food there, we didn't have any of that, we wouldn't be disappointed

55
00:07:11,080 --> 00:07:14,280
when things turned out differently.

56
00:07:14,280 --> 00:07:22,200
When we talk about the past, when we have this story of Patatra who lost her two sons

57
00:07:22,200 --> 00:07:29,120
and her whole family and her husband in the same day, and when crazy, he was totally lost

58
00:07:29,120 --> 00:07:37,040
her mind, went out of her mind because we're undermined with grief, losing her husband

59
00:07:37,040 --> 00:07:41,720
and both of her sons and then her whole family.

60
00:07:41,720 --> 00:07:49,640
And the Buddha said to her, yes, it's true that you've lost quite a bit, and that this

61
00:07:49,640 --> 00:07:56,160
is something that is making you very sad, but all the tears that you've cried and the

62
00:07:56,160 --> 00:08:10,200
rounds of Samsara are greater than the waters and all the ocean, meaning that the past

63
00:08:10,200 --> 00:08:33,960
has gone, and the past is, past is past, it's quite insignificant what happened in the past

64
00:08:33,960 --> 00:08:44,880
at this time or that time, all we have now is in the present, so Adita and Anwakami

65
00:08:44,880 --> 00:08:50,640
and Napati Kankayana and Gattam, okay, so then what should we do, if we shouldn't go back

66
00:08:50,640 --> 00:08:55,280
to the past or we shouldn't bring up the past to worry about the future, but Jupyana

67
00:08:55,280 --> 00:09:02,400
and Tiyoda among the tatatami, and this I talk about a lot, if many of you have probably

68
00:09:02,400 --> 00:09:15,560
heard me mention this before, but Jupyana is the present moment, Yodamang, Yodam, Yodamang,

69
00:09:15,560 --> 00:09:27,560
whatever Dhamma is a thing or an experience in this case, arises in the present, tatatatatat

70
00:09:27,560 --> 00:09:32,880
or Vipasati, Vipasati, you hear this word, Vipasati is where we get the word, Vipasana

71
00:09:32,880 --> 00:09:42,240
from, Vipasana means insight or it means seeing clearly, Vipasati means he or she, she's

72
00:09:42,240 --> 00:09:50,760
clearly, it's the verb form, so when someone doesn't go back to the past or the future,

73
00:09:50,760 --> 00:09:58,440
everyone sees what's in the present, clearly, whatever arises in the present, right, that's

74
00:09:58,440 --> 00:10:04,840
the essence of our practice, that should be cleared to everyone, but I think it bears

75
00:10:04,840 --> 00:10:12,000
mentioning and it bears reaffirming how powerful that is, right, and the far-reaching

76
00:10:12,000 --> 00:10:26,720
implications of it, that actually every problem that we have, right, becomes an experience,

77
00:10:26,720 --> 00:10:34,400
it really is applicable everywhere, someone's beating you with a stick, you really can

78
00:10:34,400 --> 00:10:39,600
be mindful, if you're present it's not really a problem, but if you're worrying about

79
00:10:39,600 --> 00:10:45,920
when they're going to hit you next or if you're sad or, or angry or upset about when

80
00:10:45,920 --> 00:10:51,160
they just hit you, meaning if you're upset by this experience, that's what causes

81
00:10:51,160 --> 00:10:59,640
suffering, if you caught up in the past in the future, when you're really in the present

82
00:10:59,640 --> 00:11:08,160
moment, there's only dumb, there's only experience, if you lose your job, if you get kicked

83
00:11:08,160 --> 00:11:16,840
out of your apartment, if you're living on the street, if you're starving to death, so

84
00:11:16,840 --> 00:11:22,840
wonderful thing about Buddhism and about this idea of the present moment is that it's

85
00:11:22,840 --> 00:11:29,840
never going to end, you know, there's no failure in that sense, it's not like you can

86
00:11:29,840 --> 00:11:38,120
do anything wrong, or something you can do anything irrevocably wrong.

87
00:11:38,120 --> 00:11:44,440
If you mess up this life really bad and just come back in the next life, I remember talking

88
00:11:44,440 --> 00:11:50,000
I mentioned this, I think talking to a friend of mine who had taken a Buddhism course

89
00:11:50,000 --> 00:12:01,120
with me in university and many years ago before I was among, I think, and I took the Buddhism

90
00:12:01,120 --> 00:12:07,680
course and she said she was Catholic and she said it's incredible, this idea of rebirth,

91
00:12:07,680 --> 00:12:16,160
the idea that you could have another chance, because in Christianity and Catholicism, this

92
00:12:16,160 --> 00:12:25,840
is it, one chance to do it right or go to hell for eternity, and Buddhism hell isn't

93
00:12:25,840 --> 00:12:32,120
eternal, it's always a second chance.

94
00:12:32,120 --> 00:12:44,680
And so this power of not fearing the future, not fearing consequences, learning to be what

95
00:12:44,680 --> 00:13:01,560
we say it when he says, next, this is unshakable, this is unshakable, then we know that

96
00:13:01,560 --> 00:13:05,880
and be sure of it invincibly unshakably.

97
00:13:05,880 --> 00:13:14,600
Present moment is invincible, it's unshakable, there is no experience, no situation, no conflict,

98
00:13:14,600 --> 00:13:26,600
that can be solved really, vanquished, defeated, conquered by just being mindful, it's

99
00:13:26,600 --> 00:13:31,200
the difference between trying to change the world around you and learning to dance with

100
00:13:31,200 --> 00:13:40,840
it, to be flexible with it, to roll with it, to stop reacting to it really, so that's

101
00:13:40,840 --> 00:13:47,760
the first half, we're halfway there, halfway to having a good day.

102
00:13:47,760 --> 00:13:53,480
The second half, a jiwa kitamata pungo janya marinang sui, well this isn't exactly advice,

103
00:13:53,480 --> 00:14:04,400
this is an admonishment, a reminder, most of the second half is more of a reminder, today

104
00:14:04,400 --> 00:14:10,560
and this is something we remind ourselves of a jiwa kitamata pung today is when we should

105
00:14:10,560 --> 00:14:17,680
do the work, so I just said you always have a second chance, but eventually it's going

106
00:14:17,680 --> 00:14:27,120
to have to be done today, and in fact, if it's not done today, this today, today, today,

107
00:14:27,120 --> 00:14:33,160
who knows when you'll have another chance, you might die tomorrow, kojanya marinang sui,

108
00:14:33,160 --> 00:14:53,000
who knows whether death might be even tomorrow, there is no bargaining with him, no bargain

109
00:14:53,000 --> 00:15:02,840
with death, together with his great armies or death's great armies, I don't know what

110
00:15:02,840 --> 00:15:11,400
the great army, the great army of death is, I'm not sure of that mythology, but saying

111
00:15:11,400 --> 00:15:18,400
they end up to look up in the commentaries, but death in his hordes, death in his armies,

112
00:15:18,400 --> 00:15:28,240
his army, there's no bargaining with death, it's just imagery, maybe the hordes of death

113
00:15:28,240 --> 00:15:41,480
are diseased, illness, old age, injury, war, murder, those are the maybe the armies

114
00:15:41,480 --> 00:15:54,160
of death, you can die crossing the street, you can die from food, you can die because some

115
00:15:54,160 --> 00:16:17,080
psychopaths decides to stab you, or blow you up, shoot you, we do it now, if you don't

116
00:16:17,080 --> 00:16:24,120
know when you'll get another chance, and that's not even the most important reason,

117
00:16:24,120 --> 00:16:30,480
you do it now because it's the best thing you could possibly do.

118
00:16:30,480 --> 00:16:35,080
Being in the present moment, there's no reason to do anything else to be anything else,

119
00:16:35,080 --> 00:16:39,880
the past doesn't make you happy, the future doesn't make you happy, clinging to things

120
00:16:39,880 --> 00:16:47,720
certainly doesn't make you happy, wishing or wanting for things, fighting or all the things

121
00:16:47,720 --> 00:16:51,320
that we waste our time with, how much time do we waste fighting with each other, holding

122
00:16:51,320 --> 00:16:57,480
grudges, pickering, you wouldn't believe some of the things that go on in Buddhist monasteries

123
00:16:57,480 --> 00:17:05,720
and even meditation centers, how we waste our time and energy fighting, as I've said,

124
00:17:05,720 --> 00:17:14,040
it's understandable, everyone has the filement, so there's people who are meditating,

125
00:17:14,040 --> 00:17:23,000
get upset, but we should remind ourselves not to turn it into conflict, not to waste our

126
00:17:23,000 --> 00:17:28,440
time with these things, we don't know when we're going to die, here we have this great

127
00:17:28,440 --> 00:17:41,240
opportunity, even when we're hiding at the being, dwelling thus ardently, all the

128
00:17:41,240 --> 00:17:46,760
ardor you're putting out here, that's the right word, all the energy and effort the meditators

129
00:17:46,760 --> 00:18:01,000
are putting out, it's just a greatness, a horror-tum-matanditang, both day and night relentlessly,

130
00:18:01,000 --> 00:18:09,560
so some of you have been practicing late into the night, maybe even not sleeping at night,

131
00:18:11,480 --> 00:18:19,720
doing great work, and those who have put this effort out if you've listened to some of the

132
00:18:19,720 --> 00:18:27,240
people who've gone through these courses, how incredible the transformation can be in some cases,

133
00:18:27,240 --> 00:18:35,000
where great benefit comes, and how such people can affirm, yes, they had a good day,

134
00:18:42,040 --> 00:18:45,800
tang wai badee kara tote santo a jikate money,

135
00:18:45,800 --> 00:18:54,440
and the peaceful sage has called this one who has had a good night or a good day.

136
00:18:58,520 --> 00:19:04,440
So it's not a very complex teaching, it happens to be one of the most important ones

137
00:19:04,440 --> 00:19:09,640
to stay in the present moment, to don't go back, not go back to the past or head to the future,

138
00:19:09,640 --> 00:19:17,000
to do your work today to remember that now is when the work must be done.

139
00:19:17,880 --> 00:19:25,240
Right now listening to me, are you mindful? Are you here or are you in the past in the future?

140
00:19:26,520 --> 00:19:33,720
Are you caught up in concepts and illusions? Or are you clearly aware of reality as it's

141
00:19:33,720 --> 00:19:45,160
happening every moment? There's only one way, there's only one thing to do, having a good day,

142
00:19:46,760 --> 00:19:56,680
it's not that hard to do, or it's not that complicated, it's actually something very challenging,

143
00:19:56,680 --> 00:20:03,400
but it's the work that is worth doing, it's the task that is worth accomplishing.

144
00:20:05,720 --> 00:20:13,400
So much appreciation to our meditators here who are working diligently everywhere,

145
00:20:13,400 --> 00:20:20,120
some in the main hall, some in the room, some outside, but so many people wanting to come,

146
00:20:20,120 --> 00:20:30,840
Jayvin's going to have to move into a tent soon, it's not a joke, he actually has a jealous

147
00:20:30,840 --> 00:20:51,000
kind of ice to live in the tent. Anyway, that's the dhamma for tonight, thank you all for coming up.

148
00:20:51,000 --> 00:21:11,400
We've got the questions up now. You clarify your statement yesterday, you can go you all

149
00:21:11,400 --> 00:21:17,720
don't have to stick around. Can you clarify your statement yesterday that the second way to have

150
00:21:17,720 --> 00:21:24,280
mindfulness go wrong? Here's to have mindfulness of a concept in the fact that we have traditional

151
00:21:24,280 --> 00:21:32,360
meditations on concepts such as mindfulness of the Buddha. So I thought I made it clear and I thought

152
00:21:32,360 --> 00:21:40,360
we talked about this afterwards. In fact, it seems to me this question came up and it's not wrong

153
00:21:40,360 --> 00:21:52,520
in a general sense, it's wrong, it's wrong for cultivating insight, it's like we're going down,

154
00:21:52,520 --> 00:21:58,760
if you want to go to Bangkok, you go this way, well it's just Toronto's better, if you want to

155
00:21:58,760 --> 00:22:05,400
go to Toronto you've got to take the 403 and don't get on the QEW to the aggregates the wrong way.

156
00:22:05,400 --> 00:22:12,120
Doesn't mean there's anything wrong with the way to Niagara, way to Niagara is a perfectly fine

157
00:22:12,120 --> 00:22:15,800
highway but it won't get you to Toronto, that sort of means by wrong.

158
00:22:19,640 --> 00:22:24,920
Your practice in insight concepts are wrong because it won't get you where you're trying to go.

159
00:22:27,480 --> 00:22:31,160
What are your thoughts on meditating, on loving kindness and compassion?

160
00:22:31,160 --> 00:22:36,600
I think they're good, I wouldn't take it as my primary meditation though you could,

161
00:22:38,360 --> 00:22:43,000
but I think I've talked about this actually before, they're supportive meditations, they're

162
00:22:43,000 --> 00:22:48,280
useful to support insight meditation. So I think yes, it's good to practice some every day.

163
00:22:48,280 --> 00:23:00,920
So way to change your username, yeah I have to talk to our IT people, you can send them an email.

164
00:23:04,360 --> 00:23:14,040
Luckily I'm blessed with a really awesome IT team now, we've got a real and just a great team in

165
00:23:14,040 --> 00:23:19,800
general, a shout out to our volunteer community who's put together this lovely website and

166
00:23:20,520 --> 00:23:26,760
so much more and just keeps this place running. At this point I couldn't do it alone anymore,

167
00:23:26,760 --> 00:23:35,240
it's a it's a group effort of real impressive proportions and just growing so keep it up.

168
00:23:35,240 --> 00:23:44,360
Thank you everyone.

169
00:23:46,280 --> 00:23:49,480
Do do do do do is this normal or am I doing something wrong?

170
00:23:50,920 --> 00:23:55,960
Okay I should have a we should have a note at the top, please do not ask if this is nor if X is

171
00:23:55,960 --> 00:24:03,240
normal. It's it's not useful to ask whether something is normal. You have an experience rather

172
00:24:03,240 --> 00:24:09,080
than asking me whether it's normal, try and figure out what to do about it, how you should relate to it.

173
00:24:10,360 --> 00:24:16,280
And as I said, you know your real question should be, is it when you say am I doing something wrong?

174
00:24:17,240 --> 00:24:24,120
So your real question should be, well there you go, am I doing something wrong? Is this wrong?

175
00:24:24,120 --> 00:24:31,960
Is this a bad state? And I can answer that for you, but it's not really where we go

176
00:24:31,960 --> 00:24:39,480
to tell you whether something is wrong or right and in fact in the end, it's not it's not even a valid

177
00:24:39,480 --> 00:24:46,360
question. The only thing that is right is mindfulness and if you're mindful about the state that

178
00:24:46,360 --> 00:24:53,000
you're in, right, how you see things differently, then you let it go because you're

179
00:24:53,000 --> 00:24:59,320
you're potentially conceited about it. You get conceited thinking hey look at me I'm progressing,

180
00:24:59,320 --> 00:25:07,240
feeling good about yourself confident, which can be misleading and misguided.

181
00:25:12,840 --> 00:25:15,160
And so you like this new perspective and so on.

182
00:25:17,800 --> 00:25:21,480
So I'm not going to tell you whether it's right or wrong. I want you to be mindful of it and

183
00:25:21,480 --> 00:25:35,240
learn to let it go. Even let go of the good things. Again we know what we have.

184
00:25:36,600 --> 00:25:40,120
Someone wanting me to give their thoughts. You want me to tell you whether this is normal?

185
00:25:41,240 --> 00:25:46,200
So yeah same advice. I'm not going to tell you one way or the other.

186
00:25:46,200 --> 00:25:51,720
Look at it be mindful of it and you'll see what leads to suffering and you'll see what leads to

187
00:25:51,720 --> 00:26:00,680
happiness. One is fully mindful are there any emotions? Yes there can be. There are always,

188
00:26:01,240 --> 00:26:06,280
but there can be. I mean it really depends what you mean by emotion because that's a Western word.

189
00:26:06,280 --> 00:26:13,320
We don't use that word in Buddhism. Don't have such a word really. I mean maybe I could think of

190
00:26:13,320 --> 00:26:22,520
some, but not really. So there are various states of mind.

191
00:26:23,960 --> 00:26:31,080
Regardless to wrong meditation, in mock meditation I have a tendency to keep the mantra on autopilot

192
00:26:31,080 --> 00:26:40,200
and my mind is drifting elsewhere. Device a new scheme, schemes are bad, schemes are bad

193
00:26:40,200 --> 00:26:47,000
from the outright because you're trying to fix and that's not our goal. So your mind drifts

194
00:26:47,000 --> 00:26:51,800
elsewhere. Mind drifting elsewhere is teaching you something. It's teaching a non-self

195
00:26:52,440 --> 00:26:58,760
that you're not in control. It's frustrating and that frustration and the exploration. There's

196
00:26:58,760 --> 00:27:06,840
no quick way. You have to study that distraction. You have to be meticulous about it and catch

197
00:27:06,840 --> 00:27:13,000
it every time again and again and stop trying to fix it. Stop trying to stop it from happening.

198
00:27:14,920 --> 00:27:22,200
How it stops happening is when you let go of that which is causing it. So some kind of

199
00:27:23,880 --> 00:27:28,520
potentially some kind of attachment or curiosity about whatever it is that causing you to drift.

200
00:27:29,960 --> 00:27:36,680
Sometimes it's just laziness, desire to just stop working so hard. So you just go on

201
00:27:36,680 --> 00:27:41,800
autopilot. It's more comfortable. That kind of thing. So look at that. Look at those states.

202
00:27:43,000 --> 00:27:47,720
Learn about them and just hear them clearly. You'll let them go.

203
00:27:51,080 --> 00:27:55,640
How much solo practice do you recommend before coming to your meditation course or the most

204
00:27:55,640 --> 00:28:01,000
benefit? Is there anything else that should be done before? And the best would be to do an online course

205
00:28:01,000 --> 00:28:08,040
if you have time to spend some time, we meet once a week and we have a schedule on this site.

206
00:28:08,040 --> 00:28:13,480
If you look and find it under the menu, you can reserve a spot and then just call me up using

207
00:28:13,480 --> 00:28:23,000
Google Hangouts and we can talk every week. Practicing solo, it's only the first exercise. What I

208
00:28:23,000 --> 00:28:29,560
give in the booklet is only the first step in this practice. So it's more fruitful generally to

209
00:28:29,560 --> 00:28:35,160
actually do a course and we can do that online. But other than that, there's no

210
00:28:37,720 --> 00:28:43,880
other than that come into a course as soon as you can because the course is the best way to gain

211
00:28:43,880 --> 00:28:50,840
a good foundation. So I go into a meditation center, we're having severe stiffness and tension in

212
00:28:50,840 --> 00:29:04,760
my back and neck. Yes, find a meditation center that can accommodate that. That gives you a chair

213
00:29:04,760 --> 00:29:09,480
or allows you to sit in a chair or even lie down or sit against the wall or that kind of thing.

214
00:29:10,280 --> 00:29:14,280
The body position is not that important. It's much more important what's going on in the mind.

215
00:29:14,280 --> 00:29:23,240
We have a woman here right now who has muscular dystrophy and she uses a walker to her walking.

216
00:29:25,000 --> 00:29:29,160
I said, I'm going to use her as an example. My teacher would always drag us out the foreigners

217
00:29:29,160 --> 00:29:33,960
and say, see if they can do it. These people from other countries come here and do it.

218
00:29:33,960 --> 00:29:40,040
All you could, you tie people can do it as well. So I'll use her as an example. I'd like to

219
00:29:40,040 --> 00:29:45,080
have her come here and talk about her practice, but she's done great. Finish the first course onto

220
00:29:45,080 --> 00:29:55,960
the second course. She can do it, you can. In this talk, you mentioned a psychopath shoots you

221
00:29:55,960 --> 00:30:01,880
a psychopathy, a result of past karmic actions, result of foreign fortune and injury. I'm not sure.

222
00:30:01,880 --> 00:30:07,800
I don't really know. I mean, psychopathy is just a word, right? They say a person who has no feelings

223
00:30:07,800 --> 00:30:13,560
and there's been studies that show I did a little research show that they actually do have feelings.

224
00:30:13,560 --> 00:30:18,440
They're just repressed or the brain is in such a way that it's very weak and that kind of thing.

225
00:30:22,360 --> 00:30:26,440
What is psychopathy? I don't know. I'd imagine there are various things that are

226
00:30:26,440 --> 00:30:31,400
labeled by psychiatrists or psychotherapists as psychopathy.

227
00:30:31,400 --> 00:30:42,040
They differentiate from psychosis and neurosis. Psychosis is thought to be

228
00:30:42,040 --> 00:30:47,560
that which is biological. It's a problem with the brain. Neurosis is something mental,

229
00:30:47,560 --> 00:30:52,760
something that's learned, so they try to differentiate. I would say it's a combination, right?

230
00:30:52,760 --> 00:30:59,960
Anyway, it's a fairly speculative question, so I wouldn't worry about it too much.

231
00:31:02,680 --> 00:31:06,760
Okay, and so talking to your partner to study group on Fridays is today Friday.

232
00:31:06,760 --> 00:31:09,880
Oh, are we supposed to have a meeting today? Are study group today?

233
00:31:12,040 --> 00:31:13,240
Wait, what's our schedule?

234
00:31:16,280 --> 00:31:19,400
Did I miss study group today? I thought Sunday. We're going to do one more Sunday

235
00:31:19,400 --> 00:31:25,720
and I didn't even know it was Friday. There you go, living in the present.

236
00:31:29,080 --> 00:31:32,120
It's a problem with a problem for keeping appointments, I suppose.

237
00:31:34,120 --> 00:31:37,080
Well, you guys better let me know what our schedule for that is. Otherwise,

238
00:31:37,720 --> 00:31:39,560
I guess from now on, no more Fridays.

239
00:31:39,560 --> 00:31:51,800
Oh, they're supposed to be with CD manga, so all right, so Sunday,

240
00:31:52,920 --> 00:31:56,760
Sunday was our last one and then it was switching to Friday. I'm sorry.

241
00:32:00,200 --> 00:32:03,720
No, let's do next Friday then. Take another break. Sorry.

242
00:32:03,720 --> 00:32:12,040
How to deal with some perceptible sensations. It feels like I have a lot

243
00:32:12,040 --> 00:32:13,960
crammed down from poor habits in the past.

244
00:32:17,800 --> 00:32:25,480
I don't understand. Patients wouldn't worry about it. Whatever arises, be mindful of that,

245
00:32:25,480 --> 00:32:29,720
but you put that into your demo, that that's the type of message we pass into.

246
00:32:29,720 --> 00:32:36,760
I'm really sorry about the VCD manga. I'm clearly not very good at appointments,

247
00:32:41,480 --> 00:32:48,680
but yeah, I think that's enough for tonight, so try again next week.

248
00:32:48,680 --> 00:33:04,600
Apologies. Have a good night, everyone.

