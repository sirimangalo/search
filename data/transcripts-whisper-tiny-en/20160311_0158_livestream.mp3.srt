1
00:00:00,000 --> 00:00:29,840
Good evening, everyone broadcasting live March 10th.

2
00:00:29,840 --> 00:00:58,480
Today's quote is one of these quotes about the right time.

3
00:00:58,480 --> 00:01:17,920
How do you find the right time, how do you find the good time, how do you find the happy time.

4
00:01:17,920 --> 00:01:26,720
We look for happy times, we look forward to them.

5
00:01:26,720 --> 00:01:29,480
We work towards them.

6
00:01:29,480 --> 00:01:44,480
Even meditation, we have the idea that it's going to lead us to happy, happier times in the future.

7
00:01:44,480 --> 00:01:52,320
Meditation should lead us to happiness, right?

8
00:01:52,320 --> 00:01:57,480
But if you know anything about Buddhism, the Buddha's teaching, you know, the sort of ideas

9
00:01:57,480 --> 00:02:03,280
that he presents in regards to time.

10
00:02:03,280 --> 00:02:12,520
That looking for a happy time, thinking of a specific time or play, so situation is

11
00:02:12,520 --> 00:02:24,480
being happy is the wrong way to go about it, the wrong way to go about finding happiness.

12
00:02:24,480 --> 00:02:52,640
The Buddha says, whenever monks, or monks, or bikhus,

13
00:02:52,640 --> 00:03:10,680
whatever being performs, whenever being performs bodily good conduct with the body, good

14
00:03:10,680 --> 00:03:24,760
speech, and good conduct of the mind in the morning, when one does that in the morning,

15
00:03:24,760 --> 00:03:42,160
to put one whole bikha-reh-tasana, it's a good morning monks, it's a good morning for those

16
00:03:42,160 --> 00:03:57,120
bikh-reh-tasana, and whatever beings in the middle of the day behave well with body, behave

17
00:03:57,120 --> 00:04:06,320
well with speech, behave well with mind, so much honey co-bikh-reh-tasana, that's a good

18
00:04:06,320 --> 00:04:23,240
midday for those beings, in the same evening, it's very important to you, it's very

19
00:04:23,240 --> 00:04:39,080
similar, simple, I know, but important still, important, if you want to tell us about

20
00:04:39,080 --> 00:04:48,040
Buddhist approach to happiness in a good time, that a good moment,

21
00:04:48,040 --> 00:05:10,640
it's a good season, it's a good blessing, it's a good moment,

22
00:05:10,640 --> 00:05:36,060
it's a good moment, so can you tell from my child, when there's the holy life, well,

23
00:05:36,060 --> 00:05:42,020
for a moment. That is a good moment. Actually, I'm not going to stop quite with it. It says

24
00:05:42,020 --> 00:06:02,300
it. Good behavior. A good state. Happiness is not to be found in time or place or not

25
00:06:02,300 --> 00:06:10,260
particular to time or place, not because of the specific time or place. Happiness is to be

26
00:06:10,260 --> 00:06:25,300
found in every time, in every place or not or found in no place, dependent only on one's

27
00:06:25,300 --> 00:06:36,300
state. If one behaves badly with body, badly in speech, badly in mind, that's not a good time, no

28
00:06:36,300 --> 00:06:53,940
matter when it is. But, the right moment, a happy time, happiness, can be found anywhere

29
00:06:53,940 --> 00:07:08,300
at any time. Peace. When the mind is at peace, when there is. When the body is properly

30
00:07:08,300 --> 00:07:22,060
behaved, when killing or stealing or cheating. When the speech is proper, not lying or

31
00:07:22,060 --> 00:07:32,060
hurting others with speech or using useless speech. And when the mind is well directed, mindful,

32
00:07:32,060 --> 00:07:48,060
pure, clear. When your mind is directed in the right way, aware of the body, aware of speech,

33
00:07:48,060 --> 00:07:55,060
aware of the thoughts. Clear in the thoughts with no anger, with no greed, with no delusion.

34
00:07:55,060 --> 00:08:03,580
If I have a moment, that's where happiness is. That's where business is. That's the right

35
00:08:03,580 --> 00:08:11,300
moment. A moment comes anywhere. You find it anywhere. You don't need to go far. All this ambition

36
00:08:11,300 --> 00:08:19,460
and the striving that we have for finding happiness. It's all misdirected, misguided. Find

37
00:08:19,460 --> 00:08:34,140
happiness here and now. We're ever here and now. They are at the landhouse, we keep

38
00:08:34,140 --> 00:08:55,940
in the landhouse. They have gained something useful. And they are happy. So it's actually

39
00:08:55,940 --> 00:09:01,260
a blessing, I think. It's a blessing that we do and I can't remember exactly how to

40
00:09:01,260 --> 00:09:08,960
answer. We don't know what to do. You think that they're free? Well, then let me

41
00:09:08,960 --> 00:09:15,460
Q the institutional broji's praise here. You keep going? Yes. It's wonderful.

42
00:09:15,460 --> 00:09:25,980
It's the

43
00:09:25,980 --> 00:09:28,660
Security.

44
00:09:28,660 --> 00:09:29,660
Bhatakini.

45
00:09:29,660 --> 00:09:30,660
Bhatakini.

46
00:09:30,660 --> 00:09:31,660
Bhatakini.

47
00:09:31,660 --> 00:09:32,660
Bhatakini.

48
00:09:32,660 --> 00:09:33,660
Bhatakini.

49
00:09:33,660 --> 00:09:34,660
Bhatakini.

50
00:09:34,660 --> 00:09:35,660
Bhatakini.

51
00:09:35,660 --> 00:09:36,660
Bhatakini.

52
00:09:36,660 --> 00:09:37,660
This is Bhubhana.

53
00:09:37,660 --> 00:09:38,660
Bhubhana.

54
00:09:38,660 --> 00:09:39,660
Bhubhana.

55
00:09:39,660 --> 00:09:43,660
It's so important because of what it says at the top.

56
00:09:43,660 --> 00:09:44,660
Good morning.

57
00:09:44,660 --> 00:09:45,660
Good afternoon.

58
00:09:45,660 --> 00:09:46,660
Really evening.

59
00:09:46,660 --> 00:09:47,660
Doesn't matter when.

60
00:09:47,660 --> 00:09:48,660
Doesn't matter where.

61
00:09:48,660 --> 00:09:49,660
Happiness.

62
00:09:49,660 --> 00:09:50,660
Goodness.

63
00:09:50,660 --> 00:09:51,660
Happy day.

64
00:09:51,660 --> 00:10:01,660
It's quite simple.

65
00:10:01,660 --> 00:10:06,660
It comes from the body, comes from speech, and from the mind.

66
00:10:06,660 --> 00:10:08,660
So that's the demo for tonight.

67
00:10:08,660 --> 00:10:10,660
Thank you all for tuning in.

68
00:10:10,660 --> 00:10:14,660
Guess we can go if anybody has questions.

69
00:10:14,660 --> 00:10:17,660
I'm going to post the hangout.

70
00:10:17,660 --> 00:10:24,660
So you can click on the link.

71
00:10:24,660 --> 00:10:31,660
If you've got questions, join the hangout.

72
00:10:31,660 --> 00:10:36,660
I think that there are no questions.

73
00:10:36,660 --> 00:10:43,660
And that's all for tonight.

74
00:10:43,660 --> 00:10:50,660
I'll see you next time.

75
00:10:50,660 --> 00:10:53,660
Bye bye.

76
00:10:53,660 --> 00:11:02,660
Bye.

77
00:11:02,660 --> 00:11:05,660
Bye, dear good evening.

78
00:11:05,660 --> 00:11:08,660
Bye, Larry.

79
00:11:08,660 --> 00:11:15,660
How are you?

80
00:11:15,660 --> 00:11:18,660
I'm good.

81
00:11:18,660 --> 00:11:20,660
Thank you.

82
00:11:20,660 --> 00:11:27,660
We're having some pretty heavy weather down here in Mississippi today and tonight.

83
00:11:27,660 --> 00:11:32,660
And I don't expect to keep internet.

84
00:11:32,660 --> 00:11:39,660
But I'm not going to get a good.

85
00:11:39,660 --> 00:11:44,660
You have some kind of hurricane.

86
00:11:44,660 --> 00:11:55,660
Just a big front system moving across the southern, western part of the south deep south.

87
00:11:55,660 --> 00:12:04,660
It seems to be kind of regular here.

88
00:12:04,660 --> 00:12:09,660
You know, when we end this in off and out rain and flash flood warnings and tornado watches and that sort of thing.

89
00:12:09,660 --> 00:12:14,660
I can get the power to change.

90
00:12:14,660 --> 00:12:18,660
But good to see you getting it.

91
00:12:18,660 --> 00:12:19,660
Have a good day.

92
00:12:19,660 --> 00:12:26,660
Yeah, you too.

93
00:12:26,660 --> 00:12:31,660
No question, I guess.

94
00:12:31,660 --> 00:12:36,660
Yeah, looks like the one else can come on.

95
00:12:36,660 --> 00:12:40,660
I'm curious, walking meditation, and doing half a half walk and half sitting.

96
00:12:40,660 --> 00:12:48,660
So if I want to sit for 45 minutes or an hour, then I make it walk for 45 minutes or an hour.

97
00:12:48,660 --> 00:12:55,660
And I wonder what the teravada forest tradition view is

98
00:12:55,660 --> 00:13:04,660
walking meditate using one of these labyrinths

99
00:13:04,660 --> 00:13:09,660
that you see in a walking pattern.

100
00:13:09,660 --> 00:13:15,660
It's kind of, you know, typically out of doors, a large walking pattern that you do

101
00:13:15,660 --> 00:13:25,660
kind of weave from the outside, follow this path intricately to get to the metal and then you turn around and walk back out.

102
00:13:25,660 --> 00:13:28,660
And it's not one that you get lost in.

103
00:13:28,660 --> 00:13:42,660
It's one that, you know, you kind of meditatively walk slowly along rather than as an option to just walking in a straight line,

104
00:13:42,660 --> 00:13:48,660
say 10 or 15 feet walking, turn around, come back, go back and forth.

105
00:13:48,660 --> 00:13:53,660
And it's just a curiosity question.

106
00:13:53,660 --> 00:13:59,660
Why would you walk in a different way?

107
00:13:59,660 --> 00:14:08,660
Well, it's a way of having, it appears.

108
00:14:08,660 --> 00:14:20,660
I really don't know that much about them, but I say an area that's maybe 20, 30, 40 feet across the conference.

109
00:14:20,660 --> 00:14:34,660
And you start at a point on the outside and it's like a little defined path that goes around, come back around on itself.

110
00:14:34,660 --> 00:14:47,660
And you eventually work to the metal of the pattern that would point you turn around and follow that same path, a little trail back out.

111
00:14:47,660 --> 00:14:56,660
Just kind of a novel looking thing, and I didn't know if it was something that's contrary.

112
00:14:56,660 --> 00:15:10,660
It evidently, a number of faiths or religions make use of it around the world.

113
00:15:10,660 --> 00:15:20,660
Presumably, some Buddhist and some Christian, you know, like Orthodox Christian churches and such as that.

114
00:15:20,660 --> 00:15:26,660
So it's really just a curiosity thing.

115
00:15:26,660 --> 00:15:28,660
Never of it.

116
00:15:28,660 --> 00:15:30,660
Okay.

117
00:15:30,660 --> 00:15:36,660
I don't have a, I don't have a picture of one handy.

118
00:15:36,660 --> 00:15:50,660
But, and I do want to ask about the walking meditation instruction.

119
00:15:50,660 --> 00:16:02,660
You instructed me to pick my foot up for the next step, pick it straight up, take a step and place it so that I'm lifting,

120
00:16:02,660 --> 00:16:06,660
placing, lifting, placing, lifting, placing.

121
00:16:06,660 --> 00:16:22,660
And to lift my foot, flat footed straight up and then go forward with it and place it straight down is causing me to take some pretty small steps.

122
00:16:22,660 --> 00:16:35,660
And it doesn't bother me, but it's almost, it makes the walking process much more tedious than it's requiring a lot.

123
00:16:35,660 --> 00:16:43,660
Well, it's requiring a little more attention to the mechanics of the walking process.

124
00:16:43,660 --> 00:16:52,660
And I'm just wondering if there's a, if that's what's intended.

125
00:16:52,660 --> 00:16:55,660
And I think I'm doing it right.

126
00:16:55,660 --> 00:17:00,660
Well, watch the video you can see how I do it in the video.

127
00:17:00,660 --> 00:17:01,660
Okay.

128
00:17:01,660 --> 00:17:03,660
I kind of lost the video.

129
00:17:03,660 --> 00:17:10,660
I was able to watch it that one time about six of the eight seconds.

130
00:17:10,660 --> 00:17:15,660
And then it, it didn't continue and I don't have it.

131
00:17:15,660 --> 00:17:18,660
It'll be in the hangout.

132
00:17:18,660 --> 00:17:19,660
Okay.

133
00:17:19,660 --> 00:17:21,660
In our hangout list.

134
00:17:21,660 --> 00:17:24,660
It'll be a Lincoln hangout.

135
00:17:24,660 --> 00:17:25,660
Yeah.

136
00:17:25,660 --> 00:17:26,660
Okay.

137
00:17:26,660 --> 00:17:29,660
Super.

138
00:17:29,660 --> 00:17:36,660
And also, go ahead.

139
00:17:36,660 --> 00:17:43,660
And then one other question, I, you're saying during the, um,

140
00:17:43,660 --> 00:17:51,660
I'm a talk in the evenings that some are watching it on YouTube.

141
00:17:51,660 --> 00:18:00,660
Um, so is the live stream live broadcast that's available on the meditation plus site.

142
00:18:00,660 --> 00:18:05,660
Is it also available as a YouTube visual?

143
00:18:05,660 --> 00:18:07,660
This one is.

144
00:18:07,660 --> 00:18:08,660
Okay.

145
00:18:08,660 --> 00:18:10,660
There at all.

146
00:18:10,660 --> 00:18:12,660
Okay.

147
00:18:12,660 --> 00:18:16,660
And how do, how would I reach that?

148
00:18:16,660 --> 00:18:21,660
Go to the YouTube, YouTube, my YouTube channel.

149
00:18:21,660 --> 00:18:23,660
Okay.

150
00:18:23,660 --> 00:18:24,660
Okay.

151
00:18:24,660 --> 00:18:29,660
Well, I, I evidently didn't do something right because like, I, I went, I went to it.

152
00:18:29,660 --> 00:18:37,660
But all I saw available were previous, uh, previous episodes.

153
00:18:37,660 --> 00:18:38,660
Yeah.

154
00:18:38,660 --> 00:18:40,660
I don't, I'm not that familiar with that actually.

155
00:18:40,660 --> 00:18:42,660
I just pretty sure that it works.

156
00:18:42,660 --> 00:18:45,660
It's not all, there's not always a video like.

157
00:18:45,660 --> 00:18:46,660
Okay.

158
00:18:46,660 --> 00:18:49,660
And I don't always record video.

159
00:18:49,660 --> 00:18:50,660
Okay.

160
00:18:50,660 --> 00:18:51,660
No big deal.

161
00:18:51,660 --> 00:18:55,660
I was just curious about that too.

162
00:18:55,660 --> 00:18:57,660
Okay.

163
00:18:57,660 --> 00:18:58,660
Well, I know you're busy.

164
00:18:58,660 --> 00:19:01,660
So I look like I'm the only one here.

165
00:19:01,660 --> 00:19:03,660
So I won't hold you up.

166
00:19:03,660 --> 00:19:04,660
All right.

167
00:19:04,660 --> 00:19:05,660
Take care.

168
00:19:05,660 --> 00:19:06,660
Take care.

169
00:19:06,660 --> 00:19:07,660
Practice out.

170
00:19:07,660 --> 00:19:08,660
Thank you very much.

171
00:19:08,660 --> 00:19:10,660
Thank you.

172
00:19:10,660 --> 00:19:33,660
Thank you.

