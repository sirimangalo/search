1
00:00:00,000 --> 00:00:07,000
Good evening everyone, welcome to our live broadcast.

2
00:00:07,000 --> 00:00:24,240
Today we are looking at just three short animal form of the five short, so it is from the

3
00:00:24,240 --> 00:00:38,240
Angupterani Gaya, number 53 to 57.

4
00:00:38,240 --> 00:00:50,240
The first three are from the most interesting, while we'll deal with the first three anyway to start with.

5
00:00:50,240 --> 00:01:03,240
But it says, if just for the time of the snapping of fingers, just that long.

6
00:01:03,240 --> 00:01:18,240
And because someone who has seen the danger of attachment is to pursue a mind of loving kindness,

7
00:01:18,240 --> 00:01:28,240
he is called, here she is called the Miku, who is not devoid of Jana, who acts upon the teaching of the teacher,

8
00:01:28,240 --> 00:01:35,240
who responds to his advice and doesn't, who does not eat the country's arms food in vain.

9
00:01:35,240 --> 00:01:43,240
How much more than those who cultivate it?

10
00:01:43,240 --> 00:01:52,240
That's it, the first one, the other two are almost the exact same wording, just one word is different.

11
00:01:52,240 --> 00:01:59,240
Pursues, develops, attends to three words.

12
00:01:59,240 --> 00:02:12,240
Seems that perhaps the Buddha gave the same discourse three times, or maybe he repeated it in itself three times a different morning.

13
00:02:12,240 --> 00:02:19,240
Or maybe he just used the three words in conjunction, and they were split up into different discourses.

14
00:02:19,240 --> 00:02:28,240
Anyway, the point is, and it's a really good point, a quite important point.

15
00:02:28,240 --> 00:02:37,240
And goodness, good actions are done in a moment.

16
00:02:37,240 --> 00:02:42,240
All the good that we do is momentary. All the evil that we do.

17
00:02:42,240 --> 00:02:48,240
Everything that we do, karma of any sort, good or bad, is momentary.

18
00:02:48,240 --> 00:02:55,240
You accomplish a good deed in a moment, you accomplish a bad deed in a moment.

19
00:02:55,240 --> 00:03:10,240
Good and evil are accomplished in a moment.

20
00:03:10,240 --> 00:03:21,240
Because it seems kind of insignificant to practice loving kindness for a moment, right?

21
00:03:21,240 --> 00:03:25,240
Just for a moment you wish for all beings to be happy.

22
00:03:25,240 --> 00:03:31,240
Seems like just a moment wouldn't do much, but...

23
00:03:31,240 --> 00:03:46,240
I think the point he's trying to make is as far as I can bear it out the meaning of these teachings in the wisdom of the Buddha.

24
00:03:46,240 --> 00:03:52,240
Is that a power of goodness? It doesn't get stronger over time.

25
00:03:52,240 --> 00:03:58,240
If you have one moment of love, there's an incredible power in just that one moment.

26
00:03:58,240 --> 00:04:00,240
One moment of friendliness.

27
00:04:00,240 --> 00:04:05,240
And this goes with that all good mind-states.

28
00:04:05,240 --> 00:04:08,240
They occur in a moment.

29
00:04:08,240 --> 00:04:18,240
So if you're looking for the benefit of your good deeds, the benefit of your meditation practice,

30
00:04:18,240 --> 00:04:25,240
the benefit of anything,

31
00:04:25,240 --> 00:04:28,240
it has to be in the quality of that mind.

32
00:04:28,240 --> 00:04:35,240
The benefit is inherent in the quality of a moment.

33
00:04:35,240 --> 00:04:41,240
So you can't say, I've been practicing meditation for an hour now, and I don't feel like I've gotten anything.

34
00:04:41,240 --> 00:04:44,240
Or a week, or a month, or a year.

35
00:04:44,240 --> 00:04:48,240
And I don't feel like it's helping me at all, or I'm not sure.

36
00:04:48,240 --> 00:04:51,240
I'm wondering whether this year is helping me.

37
00:04:51,240 --> 00:04:58,240
You can walk and sit all day long, day in and day out, and get nothing from it.

38
00:04:58,240 --> 00:05:07,240
But you can become enlightened in just a moment.

39
00:05:07,240 --> 00:05:12,240
It depends on the quality of the moment.

40
00:05:12,240 --> 00:05:15,240
It's the mind.

41
00:05:15,240 --> 00:05:20,240
If the mind is to file, it leads to a bad destination.

42
00:05:20,240 --> 00:05:22,240
We've been through this, right?

43
00:05:22,240 --> 00:05:29,240
And if you look at the 56 or 57,

44
00:05:29,240 --> 00:05:31,240
it says the same thing.

45
00:05:31,240 --> 00:05:35,240
Whatever qualities are on wholesome, partake of the unwholesome and pertain to the unwholesome,

46
00:05:35,240 --> 00:05:39,240
all have the mind as their forerunner.

47
00:05:39,240 --> 00:05:43,240
This is the number part of verse 1 all over again.

48
00:05:43,240 --> 00:05:49,240
Mind arises first, followed by the unwholesome quality.

49
00:05:49,240 --> 00:05:53,240
Bad stuff comes from the mind.

50
00:05:53,240 --> 00:05:55,240
And mind is only momentary.

51
00:05:55,240 --> 00:06:00,240
You have to put these two together to understand.

52
00:06:00,240 --> 00:06:11,240
The mind isn't a continuous thing, an entity that exists.

53
00:06:11,240 --> 00:06:15,240
A mind arises, depends on the mind.

54
00:06:15,240 --> 00:06:23,240
It depends on an individual mind state.

55
00:06:23,240 --> 00:06:26,240
So the cultivation of good deeds.

56
00:06:26,240 --> 00:06:30,240
But it says, how much more than those who cultivate it?

57
00:06:30,240 --> 00:06:32,240
That's not one moment being great.

58
00:06:32,240 --> 00:06:36,240
Wow, how much more great is it if someone actually cultivates it?

59
00:06:36,240 --> 00:06:43,240
So the cultivation consists merely of the accumulation of moments of goodness.

60
00:06:43,240 --> 00:06:47,240
Cultivate goodness in one moment, that's one good thing.

61
00:06:47,240 --> 00:06:53,240
And Gentong would often bring this up in the simile of the raindrops.

62
00:06:53,240 --> 00:06:57,240
If you look at a single raindrop, it doesn't seem to have that much power to it.

63
00:06:57,240 --> 00:07:01,240
It's not that much water in a single raindrop.

64
00:07:01,240 --> 00:07:10,240
But if you get lots of raindrops together, it can do great things, great damage, great work.

65
00:07:10,240 --> 00:07:15,240
Last night, I left our tent out. We have this tent foot.

66
00:07:15,240 --> 00:07:17,240
A tent that we were using.

67
00:07:17,240 --> 00:07:22,240
We're going to use to teach meditation when we send it out places.

68
00:07:22,240 --> 00:07:25,240
I send it up in the backyard thinking, well, it'd be nice.

69
00:07:25,240 --> 00:07:30,240
Maybe big meditators want to go and meditate in the backyard.

70
00:07:30,240 --> 00:07:38,240
A accumulation of raindrops and then a probe can say it, a collected rain on top.

71
00:07:38,240 --> 00:07:44,240
When this morning, I heard snapping sounds and it broke into pieces.

72
00:07:44,240 --> 00:07:49,240
It's the power of lots of raindrops.

73
00:07:49,240 --> 00:07:54,240
Because I didn't think, you know, they're just a little bit of rain.

74
00:07:54,240 --> 00:07:57,240
What could it do?

75
00:07:57,240 --> 00:08:04,240
And the same thing goes with these moments made that it, and a mind of friendliness.

76
00:08:04,240 --> 00:08:12,240
You accumulate that, it becomes a habit, and those habits can be incredibly powerful.

77
00:08:12,240 --> 00:08:17,240
You want to become a loving person. You want to become a kind person.

78
00:08:17,240 --> 00:08:22,240
You want to become a mindful person. You want to become a wise person.

79
00:08:22,240 --> 00:08:32,240
The person is created by the moment, and if you have moments of goodness,

80
00:08:32,240 --> 00:08:36,240
build that, accumulate it.

81
00:08:36,240 --> 00:08:49,240
If your goodness is consistent, that becomes who you are.

82
00:08:49,240 --> 00:08:54,240
But I think there's a great encouragement to be had here.

83
00:08:54,240 --> 00:08:59,240
And you feel discouraged about whether you're actually doing any good.

84
00:08:59,240 --> 00:09:03,240
Don't think about the long-term, think about the moments.

85
00:09:03,240 --> 00:09:07,240
Feel encouraged that if you have one moment of goodness,

86
00:09:07,240 --> 00:09:11,240
then what I said you're doing, you're doing the good work.

87
00:09:11,240 --> 00:09:15,240
If you think like that, it's very encouraging.

88
00:09:15,240 --> 00:09:20,240
And it's a great support for a practice to think in terms of moments.

89
00:09:20,240 --> 00:09:23,240
Forget about the past. Forget about the future.

90
00:09:23,240 --> 00:09:30,240
I've been practicing for an hour, a day, a week, a month.

91
00:09:30,240 --> 00:09:32,240
Focus on the moment.

92
00:09:32,240 --> 00:09:35,240
What am I doing in this moment?

93
00:09:35,240 --> 00:09:40,240
When you're mindful in this moment, you cultivate goodness in this moment.

94
00:09:40,240 --> 00:09:50,240
That's where goodness comes from.

95
00:09:50,240 --> 00:09:54,240
It comes from bad mind.

96
00:09:54,240 --> 00:10:00,240
So, that's a little bit of encouragement.

97
00:10:00,240 --> 00:10:02,240
It doesn't have a lot more to say.

98
00:10:02,240 --> 00:10:04,240
I think we'll just stop there.

99
00:10:04,240 --> 00:10:11,240
If you have any questions, happy to take questions.

100
00:10:11,240 --> 00:10:15,240
Hi, Robin.

101
00:10:15,240 --> 00:10:20,240
Hello, Bhante.

102
00:10:20,240 --> 00:10:22,240
You're back with us.

103
00:10:22,240 --> 00:10:24,240
I am.

104
00:10:24,240 --> 00:10:26,240
You went to work today, you know?

105
00:10:26,240 --> 00:10:28,240
Yes. How was that?

106
00:10:28,240 --> 00:10:34,240
I had 292 emails waiting for me.

107
00:10:34,240 --> 00:10:37,240
You worked so hard when you take the time off.

108
00:10:37,240 --> 00:10:40,240
I think it all equals out in the end.

109
00:10:40,240 --> 00:10:45,240
Make up for the time to cheer away.

110
00:10:45,240 --> 00:10:48,240
I think it bears repeating on the air.

111
00:10:48,240 --> 00:10:54,240
How fortunate we are to have your support and help and how much you did this week.

112
00:10:54,240 --> 00:10:55,240
Thank you.

113
00:10:55,240 --> 00:10:57,240
I sent her all set up.

114
00:10:57,240 --> 00:10:58,240
Thank you.

115
00:10:58,240 --> 00:11:01,240
I always enjoyed being at the center.

116
00:11:01,240 --> 00:11:07,240
Looking forward to going back soon.

117
00:11:07,240 --> 00:11:10,240
So, where did you leave off yesterday?

118
00:11:10,240 --> 00:11:13,240
Well, I think we're just down at the bottom.

119
00:11:13,240 --> 00:11:17,240
Okay.

120
00:11:17,240 --> 00:11:19,240
Next got a couple of questions.

121
00:11:19,240 --> 00:11:22,240
How about 23 hours ago?

122
00:11:22,240 --> 00:11:24,240
No, I don't think so.

123
00:11:24,240 --> 00:11:27,240
I'm highly analytical and have a strong tendency toward thinking.

124
00:11:27,240 --> 00:11:36,240
Was that on last night's broadcast?

125
00:11:36,240 --> 00:11:38,240
Or were done on those?

126
00:11:38,240 --> 00:11:39,240
Okay.

127
00:11:49,240 --> 00:11:52,240
What does the Angeli symbol with one, two, three, et cetera,

128
00:11:52,240 --> 00:11:54,240
under heading one mean?

129
00:11:54,240 --> 00:11:57,240
I think that's a question people ask a lot.

130
00:11:57,240 --> 00:12:03,240
It's just like a like on Facebook, a Facebook like.

131
00:12:03,240 --> 00:12:07,240
Yeah, it's just some silly thing to waste your time, actually.

132
00:12:07,240 --> 00:12:08,240
Yeah.

133
00:12:08,240 --> 00:12:12,240
Support digital support.

134
00:12:12,240 --> 00:12:16,240
Yeah, it is.

135
00:12:16,240 --> 00:12:19,240
Yeah, I'm going to click on it.

136
00:12:19,240 --> 00:12:22,240
Thank you very much for all you have taught us.

137
00:12:22,240 --> 00:12:24,240
I have a question regarding discipline.

138
00:12:24,240 --> 00:12:27,240
I have a serious problem with that aspect of my life.

139
00:12:27,240 --> 00:12:31,240
I began meditating in January, but I have not been very constant about meditating.

140
00:12:31,240 --> 00:12:34,240
Do you have any recommendations or exercises to train discipline?

141
00:12:34,240 --> 00:12:39,240
Thank you.

142
00:12:39,240 --> 00:12:43,240
I mean, you have to break it apart into what's really going on.

143
00:12:43,240 --> 00:12:46,240
Why are you not disciplined?

144
00:12:46,240 --> 00:12:50,240
The only way to change it is to actually look at who you are

145
00:12:50,240 --> 00:12:54,240
and what's going on in your daily life.

146
00:12:54,240 --> 00:12:57,240
What are the things that are keeping you from meditating?

147
00:12:57,240 --> 00:12:58,240
Do you dislike meditating?

148
00:12:58,240 --> 00:13:01,240
Do you feel aversion towards it?

149
00:13:01,240 --> 00:13:02,240
Like it's a chore?

150
00:13:02,240 --> 00:13:04,240
Well, that aversion might be a problem.

151
00:13:04,240 --> 00:13:08,240
Or are you addicted to certain other activity?

152
00:13:08,240 --> 00:13:12,240
Then you have to look at that addiction.

153
00:13:12,240 --> 00:13:13,240
It's not easy.

154
00:13:13,240 --> 00:13:17,240
Meditation is not something that comes easy to most people,

155
00:13:17,240 --> 00:13:19,240
especially in this day and age.

156
00:13:19,240 --> 00:13:23,240
Now everyone's out playing this new game.

157
00:13:23,240 --> 00:13:25,240
There's a game called Pokemon Go.

158
00:13:25,240 --> 00:13:27,240
I don't really know what Pokemon is.

159
00:13:27,240 --> 00:13:31,240
I think it's, I understand that it stands for pocket monster.

160
00:13:31,240 --> 00:13:33,240
That's about all I know about it.

161
00:13:33,240 --> 00:13:36,240
You do something like training these things or something.

162
00:13:36,240 --> 00:13:37,240
You catch them.

163
00:13:37,240 --> 00:13:39,240
Apparently everybody catches them.

164
00:13:39,240 --> 00:13:42,240
There's something about throwing things at them or something.

165
00:13:42,240 --> 00:13:45,240
Well, it's, you know, where you walk around now

166
00:13:45,240 --> 00:13:47,240
and you catch them on your phone, I guess.

167
00:13:47,240 --> 00:13:49,240
Currently everyone's out catching these things.

168
00:13:49,240 --> 00:13:52,240
They're in cemeteries and churches and all different kinds of places.

169
00:13:52,240 --> 00:13:56,240
It's kind of real controversial because there are people coming out with studies

170
00:13:56,240 --> 00:13:59,240
that it's great for people because it gets them outside.

171
00:13:59,240 --> 00:14:02,240
It gets them to interact with other people and it's wonderful.

172
00:14:02,240 --> 00:14:05,240
And other people are saying there are people falling off cliffs

173
00:14:05,240 --> 00:14:08,240
because they're so intent on catching the Pokemon.

174
00:14:08,240 --> 00:14:10,240
They don't notice what's actually going on around them.

175
00:14:10,240 --> 00:14:12,240
I guess some people have gotten hurt.

176
00:14:12,240 --> 00:14:13,240
So two sides.

177
00:14:13,240 --> 00:14:15,240
When I heard about it first immediately,

178
00:14:15,240 --> 00:14:19,240
I thought of Fahrenheit 451.

179
00:14:19,240 --> 00:14:21,240
Actually, what did I know?

180
00:14:21,240 --> 00:14:23,240
I think I didn't.

181
00:14:23,240 --> 00:14:28,240
Anyway, I've been thinking about Fahrenheit 451.

182
00:14:28,240 --> 00:14:30,240
And it's actually someone unrelated.

183
00:14:30,240 --> 00:14:33,240
So maybe it was something else I was thinking of.

184
00:14:33,240 --> 00:14:40,240
But it's the same sort of idea where we've supplanted reality

185
00:14:40,240 --> 00:14:43,240
with some sort of entertainment.

186
00:14:43,240 --> 00:14:46,240
They're not going outside to be outside.

187
00:14:46,240 --> 00:14:51,240
They're going outside because this computer is now everywhere.

188
00:14:51,240 --> 00:14:56,240
It doesn't really have that much to do with Fahrenheit 451,

189
00:14:56,240 --> 00:14:59,240
except if you read the book,

190
00:14:59,240 --> 00:15:01,240
then you know about the family where they have this room

191
00:15:01,240 --> 00:15:03,240
where you actually interact with it.

192
00:15:03,240 --> 00:15:07,240
But the point being that no one's really paying attention

193
00:15:07,240 --> 00:15:10,240
to reality anymore.

194
00:15:10,240 --> 00:15:14,240
I mean, you might be able to argue that

195
00:15:14,240 --> 00:15:20,240
Pokemon goes somehow making people become more aware

196
00:15:20,240 --> 00:15:26,240
that there was something about them actually learning about

197
00:15:26,240 --> 00:15:29,240
the world around them because they were going outside.

198
00:15:29,240 --> 00:15:31,240
But that's not reality.

199
00:15:31,240 --> 00:15:33,240
I guess I bring it up.

200
00:15:33,240 --> 00:15:39,240
I mean, I want to mention it because our minds are not interested

201
00:15:39,240 --> 00:15:41,240
in meditation.

202
00:15:41,240 --> 00:15:46,240
Our minds are not inclined towards meditation.

203
00:15:46,240 --> 00:15:49,240
That's a global society now.

204
00:15:49,240 --> 00:15:51,240
We're inclined in other ways.

205
00:15:51,240 --> 00:15:55,240
But it's not easy.

206
00:15:55,240 --> 00:16:00,240
We should have, we should have a meditation go out for something

207
00:16:00,240 --> 00:16:07,240
where you somehow are able to record

208
00:16:07,240 --> 00:16:10,240
walking mindfully.

209
00:16:10,240 --> 00:16:13,240
Like if you walk somewhere mindfully

210
00:16:13,240 --> 00:16:16,240
walking walking, at the end of it you push a button

211
00:16:16,240 --> 00:16:19,240
to record that you did that.

212
00:16:19,240 --> 00:16:23,240
And you get rewards or something.

213
00:16:23,240 --> 00:16:25,240
Point.

214
00:16:25,240 --> 00:16:29,240
There was an interesting article about Oculus Rift in Buddhism

215
00:16:29,240 --> 00:16:31,240
a while back.

216
00:16:31,240 --> 00:16:35,240
And it was, well, the article that I was reading,

217
00:16:35,240 --> 00:16:39,240
it was suggesting that it could be a potential

218
00:16:39,240 --> 00:16:43,240
where a person who has a meditative experience

219
00:16:43,240 --> 00:16:46,240
can somehow, through the Oculus Rift, you know,

220
00:16:46,240 --> 00:16:49,240
record that and transmit it so that someone else

221
00:16:49,240 --> 00:16:51,240
can experience what they experienced.

222
00:16:51,240 --> 00:16:54,240
Who knows how far that would go.

223
00:16:54,240 --> 00:17:00,240
I used and do visited me when I was in Thailand.

224
00:17:00,240 --> 00:17:05,240
And I used the HTC Vive, I think it's called,

225
00:17:05,240 --> 00:17:06,240
or is it Sony?

226
00:17:06,240 --> 00:17:07,240
I don't know.

227
00:17:07,240 --> 00:17:10,240
HTC, I think, the Vive, which is like a competitor

228
00:17:10,240 --> 00:17:14,240
to the Oculus Rift, I think.

229
00:17:14,240 --> 00:17:17,240
My uncle has it, his company, his production company

230
00:17:17,240 --> 00:17:21,240
has the Viva, and they're really keen to make movies

231
00:17:21,240 --> 00:17:24,240
that are three-dimensional or whatever,

232
00:17:24,240 --> 00:17:26,240
immersive.

233
00:17:26,240 --> 00:17:29,240
And so I went up, I was great.

234
00:17:29,240 --> 00:17:34,240
I went up on this cliff and meditated, sat down on the cliff

235
00:17:34,240 --> 00:17:36,240
and meditated.

236
00:17:36,240 --> 00:17:40,240
And it was in an air-conditioned room,

237
00:17:40,240 --> 00:17:42,240
so it felt like I was up on the cliff.

238
00:17:42,240 --> 00:17:43,240
It was nice.

239
00:17:43,240 --> 00:17:45,240
I thought, well, this is interesting.

240
00:17:45,240 --> 00:17:53,240
I mean, obviously, we're not fixated on pleasant surroundings,

241
00:17:53,240 --> 00:17:57,240
but for a beginner meditator, to take them out of it

242
00:17:57,240 --> 00:18:00,240
and, honestly, to sit there up on the cliff

243
00:18:00,240 --> 00:18:05,240
was really refreshing, to be able to sit there

244
00:18:05,240 --> 00:18:08,240
and you look around, and then there's this little dog

245
00:18:08,240 --> 00:18:12,240
and you throw a stick, and the dog catches your stick.

246
00:18:12,240 --> 00:18:16,240
That's really a bizarre experience, but it was nice

247
00:18:16,240 --> 00:18:19,240
to sit and meditate, and then it was under the sea one

248
00:18:19,240 --> 00:18:26,240
where it's not really all that appropriate

249
00:18:26,240 --> 00:18:30,240
in insight meditation, but for a beginner meditator,

250
00:18:30,240 --> 00:18:35,240
you could argue that it's somehow helpful.

251
00:18:35,240 --> 00:18:39,240
And they also were talking about, like,

252
00:18:39,240 --> 00:18:41,240
Dhamma meetings, where I mean,

253
00:18:41,240 --> 00:18:43,240
you've done something similar in second life,

254
00:18:43,240 --> 00:18:45,240
but this would be taking it to the next level.

255
00:18:45,240 --> 00:18:50,240
Well, the other thing I was thinking is if,

256
00:18:50,240 --> 00:18:53,240
I mean, meetings, I think,

257
00:18:53,240 --> 00:18:57,240
we were talking about that with one of the partners

258
00:18:57,240 --> 00:19:04,240
of this living films company, or the main guy.

259
00:19:04,240 --> 00:19:06,240
And there is stuff about that,

260
00:19:06,240 --> 00:19:09,240
but easier might be, or more simple,

261
00:19:09,240 --> 00:19:14,240
would be the idea of, you know,

262
00:19:14,240 --> 00:19:16,240
three-dimensional people might be different,

263
00:19:16,240 --> 00:19:18,240
but if you're sitting there up on the cliff

264
00:19:18,240 --> 00:19:21,240
and suddenly, among a teacher walks over

265
00:19:21,240 --> 00:19:25,240
and you explain meditation to you,

266
00:19:25,240 --> 00:19:28,240
and, you know, to get the sort of

267
00:19:28,240 --> 00:19:33,240
that kind of atmosphere, it might be interesting.

268
00:19:33,240 --> 00:19:36,240
It's a pretty awesome world way, living.

269
00:19:36,240 --> 00:19:40,240
Yeah, I don't know.

270
00:19:40,240 --> 00:19:46,240
What advice do you have for extending our meditations?

271
00:19:46,240 --> 00:19:50,240
Sort of the same answer as the last one.

272
00:19:50,240 --> 00:19:53,240
What is it that's keeping you from extending your meditations?

273
00:19:53,240 --> 00:20:00,240
Or from that, I mean, longer meditations require several things.

274
00:20:00,240 --> 00:20:05,240
And they require the cultivation of sustained mindfulness

275
00:20:05,240 --> 00:20:09,240
because otherwise you go crazy if you sit too long.

276
00:20:09,240 --> 00:20:11,240
You require bodily flexibilities.

277
00:20:11,240 --> 00:20:13,240
You've got your body has to loosen up

278
00:20:13,240 --> 00:20:17,240
and it'll do that naturally as the mind of things up,

279
00:20:17,240 --> 00:20:21,240
but it's sort of a progression.

280
00:20:21,240 --> 00:20:24,240
You have to really get into meditation before you can

281
00:20:24,240 --> 00:20:33,240
start to extend your sessions.

282
00:20:33,240 --> 00:20:37,240
I have been trying to perceive goodness or eyes in myself and others.

283
00:20:37,240 --> 00:20:40,240
It is enjoyable and it makes it easier to have good will to people

284
00:20:40,240 --> 00:20:41,240
I don't normally like.

285
00:20:41,240 --> 00:20:46,240
Is this part of the moodita development?

286
00:20:46,240 --> 00:20:51,240
Yeah, we're not so much yourself, but perceive the goodness

287
00:20:51,240 --> 00:20:52,240
and others.

288
00:20:52,240 --> 00:20:53,240
Yeah.

289
00:20:53,240 --> 00:21:00,240
That could be moodita.

290
00:21:00,240 --> 00:21:11,240
No, there are more simple ways to then perhaps

291
00:21:11,240 --> 00:21:18,240
investigating good people's good qualities.

292
00:21:18,240 --> 00:21:22,240
And the other thing is that you're trying to have appreciation

293
00:21:22,240 --> 00:21:30,240
of even people who you wouldn't normally appreciate.

294
00:21:30,240 --> 00:21:36,240
So yeah, no, I mean, that's sort of what you're saying.

295
00:21:36,240 --> 00:21:43,240
And I just you don't have to be all that specific about it.

296
00:21:43,240 --> 00:21:46,240
There are simple ways of practicing moodita.

297
00:21:46,240 --> 00:21:48,240
And if you're really practicing moodita,

298
00:21:48,240 --> 00:21:51,240
the best thing is to try and be systematic about it

299
00:21:51,240 --> 00:21:56,240
and just do it as a meditation practice.

300
00:21:56,240 --> 00:21:59,240
But no, I mean, appreciating people's good qualities

301
00:21:59,240 --> 00:22:02,240
is an important practice.

302
00:22:02,240 --> 00:22:04,240
Power to you, go for it.

303
00:22:04,240 --> 00:22:10,240
How far are the Brahma Vihars or are good things to cultivate

304
00:22:10,240 --> 00:22:16,240
as a person to be Buddhist to cultivate the four Brahma Vihars?

305
00:22:16,240 --> 00:22:23,240
So yeah, thinking about them being conscious of our intolerance,

306
00:22:23,240 --> 00:22:28,240
to be more tolerant and our cruelty to be more kind,

307
00:22:28,240 --> 00:22:41,240
our jealousy to be more appreciative.

308
00:22:41,240 --> 00:22:44,240
Games that don't teach too much entertainment,

309
00:22:44,240 --> 00:22:48,240
constant entertainment, is this good?

310
00:22:48,240 --> 00:22:50,240
No, it's not so good.

311
00:22:50,240 --> 00:22:51,240
It's not a killer.

312
00:22:51,240 --> 00:23:03,240
It's not a showstopper, but it'll hold you back, drag you down.

313
00:23:03,240 --> 00:23:06,240
Is it appropriate to try to cultivate a certain amount of enjoyment

314
00:23:06,240 --> 00:23:09,240
for the meditation process itself?

315
00:23:09,240 --> 00:23:12,240
It's probably the wrong word you have to be careful with enjoyment

316
00:23:12,240 --> 00:23:22,240
because if you're seeking out enjoyment, you've got a problem.

317
00:23:22,240 --> 00:23:28,240
Appreciation, sort of contentment with it,

318
00:23:28,240 --> 00:23:33,240
a state of pleasure isn't bad, but enjoyment,

319
00:23:33,240 --> 00:23:37,240
enjoyment and pleasure, like happiness.

320
00:23:37,240 --> 00:23:39,240
It can be a happy practice.

321
00:23:39,240 --> 00:23:42,240
But if you enjoy the happiness, it gets to be problematic,

322
00:23:42,240 --> 00:23:48,240
because there's a liking and it becomes an addiction, a partiality.

323
00:23:48,240 --> 00:23:51,240
I wouldn't worry about it trying to like things.

324
00:23:51,240 --> 00:23:54,240
We're trying to enjoy things.

325
00:23:54,240 --> 00:23:58,240
But if it's unenjoyable, if it's unpleasant,

326
00:23:58,240 --> 00:24:00,240
then you've got a problem.

327
00:24:00,240 --> 00:24:04,240
Just get rid of the unpleasantness is really enough.

328
00:24:04,240 --> 00:24:06,240
Once you're at peace with the meditation,

329
00:24:06,240 --> 00:24:09,240
then it's smooth sailing.

330
00:24:09,240 --> 00:24:16,240
But if you get into enjoying the memory of doing it's unenjoyable, right?

331
00:24:16,240 --> 00:24:22,240
When the enjoyment fades, it's not necessarily wrong.

332
00:24:22,240 --> 00:24:28,240
It's just a difficult word, and it's easy to fall into desire

333
00:24:28,240 --> 00:24:40,240
and clinging, like I mean, if you think of it that way.

334
00:24:40,240 --> 00:24:42,240
Can tabs come into this?

335
00:24:42,240 --> 00:24:45,240
I find it would be difficult, but compassion easy.

336
00:24:45,240 --> 00:24:49,240
So I'm not sure if that's a question.

337
00:24:49,240 --> 00:24:50,240
Just a comment.

338
00:24:50,240 --> 00:24:54,240
Let's say you can look up the sort of systematic ways of practicing.

339
00:24:54,240 --> 00:24:59,240
If you're interested, it's worth going through the Risudimaga's

340
00:24:59,240 --> 00:25:03,240
description of moodita, of the four Brahmui Haras,

341
00:25:03,240 --> 00:25:11,240
and practicing it systematically, rather than just trying to do it in your daily life.

342
00:25:11,240 --> 00:25:15,240
Because if you get systematic about it, then it's much easier.

343
00:25:15,240 --> 00:25:20,240
You're much stronger as far as cultivating it in your daily life.

344
00:25:20,240 --> 00:25:29,240
It's going to spend some time cultivating these four.

345
00:25:29,240 --> 00:25:32,240
Would it be fair to say the one that's hardest to cultivate

346
00:25:32,240 --> 00:25:35,240
is the one that you probably need to cultivate the most?

347
00:25:35,240 --> 00:25:39,240
Perhaps.

348
00:25:39,240 --> 00:25:42,240
Yes, most likely.

349
00:25:42,240 --> 00:25:45,240
But I wouldn't pay too much attention to these,

350
00:25:45,240 --> 00:25:50,240
because what we're aiming for is purity of mind, and that comes from

351
00:25:50,240 --> 00:25:53,240
the deepest level, it comes from insight meditation.

352
00:25:53,240 --> 00:25:56,240
And if you've become accomplished in insight meditation,

353
00:25:56,240 --> 00:26:01,240
then all four of the Brahmui Haras will come in.

354
00:26:01,240 --> 00:26:08,240
The more you practice, the more you stay, they become.

355
00:26:08,240 --> 00:26:16,240
Too many questions tonight, it's a Monday night.

356
00:26:16,240 --> 00:26:19,240
We don't have so many viewers either.

357
00:26:19,240 --> 00:26:26,240
Still a good number, but we had 50 last night.

358
00:26:26,240 --> 00:26:31,240
Anyway, happy to come out here every night.

359
00:26:31,240 --> 00:26:33,240
Thank you all for tuning in.

360
00:26:33,240 --> 00:26:35,240
I guess we'll end it there.

361
00:26:35,240 --> 00:26:38,240
If you have more questions, say it's unfair tomorrow.

362
00:26:38,240 --> 00:26:40,240
Thanks for having me for coming on.

363
00:26:40,240 --> 00:26:42,240
Thank you, Bhante.

364
00:26:42,240 --> 00:27:06,240
Good night.

