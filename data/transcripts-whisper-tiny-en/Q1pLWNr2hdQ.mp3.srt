1
00:00:00,000 --> 00:00:13,920
Okay, good evening everyone. Welcome to our evening dhamma and take a moment here

2
00:00:13,920 --> 00:00:26,000
to ask everyone how the audio is. I've got audio in second life. We've got audio on YouTube.

3
00:00:26,000 --> 00:00:38,000
We've got local audio. Can you hear me in the back there? I'm nodding, okay. Locally I'm okay.

4
00:00:38,000 --> 00:00:45,000
I've got to get some speakers in this room. I can set up a microphone.

5
00:00:45,000 --> 00:00:54,000
See YouTube? How can YouTube hear me? I'm not going to get any death threats from YouTube for not.

6
00:00:54,000 --> 00:00:58,000
Having sufficient audio? Okay, good. YouTube is good.

7
00:00:58,000 --> 00:01:04,000
A lot in the last night.

8
00:01:04,000 --> 00:01:15,000
All right, well, we'll see if there's any comments by the end. We'll adjust it again.

9
00:01:15,000 --> 00:01:34,000
So today we're talking about purification, purification by overcoming doubt.

10
00:01:34,000 --> 00:01:44,000
What if you understand it is the next logical step from after purification of you?

11
00:01:44,000 --> 00:01:56,000
Again, purification of you, what we talked about last night is changing the way we look at the world.

12
00:01:56,000 --> 00:02:07,000
So moving from a concept or entity-based three-dimensional, impersonal view of reality to an experiential base.

13
00:02:07,000 --> 00:02:16,000
We begin to look at reality from a point of view of here now and first person.

14
00:02:16,000 --> 00:02:21,000
What am I experiencing? What is the experience here and now?

15
00:02:21,000 --> 00:02:27,000
That's the basis for the reality.

16
00:02:27,000 --> 00:02:32,000
So purification by overcoming doubt isn't really about doubt about that.

17
00:02:32,000 --> 00:02:37,000
Tell about what reality is made up of.

18
00:02:37,000 --> 00:02:58,000
It's more in regards to the way we're going to approach reality or the consequences of the reality.

19
00:02:58,000 --> 00:03:08,000
See what it means, the implications.

20
00:03:08,000 --> 00:03:18,000
A quality of mind that we live with ordinarily.

21
00:03:18,000 --> 00:03:30,000
We live with a doubt constantly about what should I do with my life? What is the right way to go?

22
00:03:30,000 --> 00:03:47,000
We have doubts about the nature of our lives, about what our situations, we have doubts about what we should do.

23
00:03:47,000 --> 00:03:54,000
We have doubts about the nature of the world, how does the world work?

24
00:03:54,000 --> 00:04:09,000
A lot of science was created to help answer these questions, help us find a better way to live, help us understand the universe.

25
00:04:09,000 --> 00:04:20,000
So that we might live a better way. It's why religion was created as well, religion and science, as we understand these words.

26
00:04:20,000 --> 00:04:25,000
They both tried to provide us with answers.

27
00:04:25,000 --> 00:04:41,000
Though what we call religion has often been more about believing without much basis in evidence.

28
00:04:41,000 --> 00:04:45,000
And science has been much more about evidence. They both tried to give us answers to help us overcome doubt.

29
00:04:45,000 --> 00:04:54,000
And there's quite a bit of overlap, in fact, if you think about it from a phenomenological point of view to believe something or to know it.

30
00:04:54,000 --> 00:05:12,000
The point is it provides you with an answer.

31
00:05:12,000 --> 00:05:19,000
Science provides us with answers based on evidence, and that seems that that's very good at overcoming doubt.

32
00:05:19,000 --> 00:05:25,000
Conventionally, right?

33
00:05:25,000 --> 00:05:31,000
If you rely on science, if you believe in science, if you put your faith in science,

34
00:05:31,000 --> 00:05:35,000
it's very good at overcoming our doubt rationally.

35
00:05:35,000 --> 00:05:48,000
Very, very good at helping us to find ways of living our lives, and there's so much we take for granted about the world that science has discovered to be true.

36
00:05:48,000 --> 00:06:05,000
So science begins to assuage our doubts and help us free ourselves from doubt, but it doesn't go the full distance.

37
00:06:05,000 --> 00:06:09,000
I mean, for two reasons, first, because science doesn't know everything.

38
00:06:09,000 --> 00:06:19,000
Scientists haven't figured out all the answers to all the questions, but also because the answers are not our answers.

39
00:06:19,000 --> 00:06:28,000
Where science breaks down is it can't help you come to know the truth of your reality.

40
00:06:28,000 --> 00:06:35,000
So we talked about things like depression and science can help you understand the physical aspects of depression,

41
00:06:35,000 --> 00:06:40,000
but it can't help you overcome depression, because it can't help you understand.

42
00:06:40,000 --> 00:06:47,000
I mean, it can help, but it can't bring you to understand your own depression,

43
00:06:47,000 --> 00:06:56,000
and understand your depression to the extent that you free yourself from it, or anxiety or fear.

44
00:06:56,000 --> 00:07:00,000
Science can't tell you how to live your life, can't tell you how to react to situations,

45
00:07:00,000 --> 00:07:05,000
it can't teach you wisdom. It's the difference again between wisdom and intelligence.

46
00:07:05,000 --> 00:07:17,000
So religion tries to provide this wisdom. It tries to offer things like ethics.

47
00:07:17,000 --> 00:07:22,000
And so the biggest doubts are usually in regards to religion.

48
00:07:22,000 --> 00:07:33,000
In regards to our own what we call spirituality, but what really turns out to be much more how we interact in a very mundane way with the universe.

49
00:07:33,000 --> 00:07:42,000
We talk about belief in God as though it's some sort of divine or abstract thing, but it's really just a means for us to cope.

50
00:07:42,000 --> 00:07:47,000
God is a coping mechanism more or less.

51
00:07:47,000 --> 00:07:53,000
How do I mean that? We use God to reassure ourselves. God has a plan for you.

52
00:07:53,000 --> 00:07:58,000
No matter how hard things get, just remember God loves you.

53
00:07:58,000 --> 00:08:02,000
Just remember God is in heaven waiting for you.

54
00:08:02,000 --> 00:08:13,000
So from a psychological point of view, which really is a much better founded in reality than religion.

55
00:08:13,000 --> 00:08:30,000
God and things like God are just tools that we use to help us help us help us help us solve our problems, help us cope with our difficulties.

56
00:08:30,000 --> 00:08:34,000
Most of religion is like that ritual, for example.

57
00:08:34,000 --> 00:08:45,000
You see Buddhist do a lot of rituals, chanting, ringing bells, bowing, psychologically, these are useful things.

58
00:08:45,000 --> 00:08:50,000
They help promote positive mind-states.

59
00:08:50,000 --> 00:08:53,000
But none of these things is really enough.

60
00:08:53,000 --> 00:09:01,000
Enough for what? Enough to help us to allow us to completely overcome doubt.

61
00:09:01,000 --> 00:09:13,000
So this next stage is where the meditator begins to actually understand not only the nature of reality,

62
00:09:13,000 --> 00:09:25,000
but not only the building blocks of reality, but how it works, how the machine works, not just what it's made up of, how it works.

63
00:09:25,000 --> 00:09:31,000
And so the first stage is the meditator now begins to not only look at body and mind.

64
00:09:31,000 --> 00:09:38,000
Remember what we talked about last night, but begins to understand how body and mind work together.

65
00:09:38,000 --> 00:09:51,000
So the overcoming of doubt here has to do very much with how reality really works, how our problems come to be.

66
00:09:51,000 --> 00:09:56,000
I mean, the big part of our problem is we don't even understand our problems.

67
00:09:56,000 --> 00:10:01,000
I'm anxious, where does it come from? What does it mean? I'm depressed.

68
00:10:01,000 --> 00:10:09,000
What is it? When you go to the doctor and they tell you, oh, it's because your brain is this and this and this.

69
00:10:09,000 --> 00:10:20,000
Which again is useful to a point. It leads us on the wrong path, they think in some sense, because it's too much of a reliance on that as the answer.

70
00:10:20,000 --> 00:10:33,000
It's not wrong, the brain is doing certain things, but it's wrong in the sense that that answer isn't going to free us from depression.

71
00:10:33,000 --> 00:10:39,000
It's the wrong answer because it's the wrong type of answer.

72
00:10:39,000 --> 00:10:44,000
The right type of answer is one that we see for ourselves. Oh, yes, look at me creating my depression.

73
00:10:44,000 --> 00:10:48,000
Look at the depression caused by this and this and this.

74
00:10:48,000 --> 00:10:52,000
The meditator begins to see cause and effect.

75
00:10:52,000 --> 00:10:58,000
It's called Bachea Parigahayana. It's the second of this 16 stages of knowledge.

76
00:10:58,000 --> 00:11:02,000
It's hard to see cause and effect.

77
00:11:02,000 --> 00:11:05,000
Again, these knowledges are not something intellectual.

78
00:11:05,000 --> 00:11:08,000
I wouldn't even really recommend researching them too much.

79
00:11:08,000 --> 00:11:13,000
Anyone who gets caught up in these ends up just hurting themselves in terms of the practice.

80
00:11:13,000 --> 00:11:19,000
If you get too caught up in learning about the knowledges and thinking about the knowledges.

81
00:11:19,000 --> 00:11:25,000
The kind of thing teachers use to assess their students, but they're not the most important thing.

82
00:11:25,000 --> 00:11:29,000
They can even be detrimental if you know too much.

83
00:11:29,000 --> 00:11:32,000
Again, at this point, we're very much into results of the practice.

84
00:11:32,000 --> 00:11:35,000
This is useful for those of you who are meditating.

85
00:11:35,000 --> 00:11:41,000
It's useful for those of you who haven't, in a sense, to give you a sense of where you're going.

86
00:11:41,000 --> 00:11:49,000
It helps build confidence and direction, but it's certainly not a replacement for actual practice.

87
00:11:49,000 --> 00:11:58,000
It's most useful for those people who are already experiencing these things so they can confirm and focus their energies.

88
00:11:58,000 --> 00:12:05,000
Based on what is and what is not the path.

89
00:12:05,000 --> 00:12:11,000
In summary, this is an understanding of karma.

90
00:12:11,000 --> 00:12:22,000
We talk about karma in Buddhism. What we really mean by karma in Buddhism, we mean our intentions or our bent, our volition in the mind.

91
00:12:22,000 --> 00:12:26,000
At this point, the meditator starts to understand what volition means.

92
00:12:26,000 --> 00:12:31,000
When you have a desire for something, well, that leads to a result.

93
00:12:31,000 --> 00:12:35,000
When you learn what that result is, you don't have to believe anyone.

94
00:12:35,000 --> 00:12:37,000
When you have this sort of mindset, it leads to this result.

95
00:12:37,000 --> 00:12:43,000
The meditator begins to reflect upon all of their past volitions as well.

96
00:12:43,000 --> 00:12:48,000
They start to see how this led to this and that led to that.

97
00:12:48,000 --> 00:12:55,000
Most importantly, they see here and now this leads to this, that leads to that.

98
00:12:55,000 --> 00:13:01,000
It's very simple. The meditator begins to see the mechanics of reality.

99
00:13:01,000 --> 00:13:09,000
It's quite obvious. The meditator, once they've passed the first stage, they'll enter into this stage.

100
00:13:09,000 --> 00:13:17,000
The next thing that has to do with that overcoming doubt has to do with is what we do in regards to that.

101
00:13:17,000 --> 00:13:27,000
Hey, so this leads to this, that leads to that. What does it all mean?

102
00:13:27,000 --> 00:13:30,000
What do I do about this? What's the answer?

103
00:13:30,000 --> 00:13:38,000
Does that make sense? Of course, the answer is just to be mindful, but there's a deeper understanding that happens here.

104
00:13:38,000 --> 00:13:51,000
There's a wrestling with oneself as one wrestles with the implications of the nature of reality.

105
00:13:51,000 --> 00:13:58,000
One sees how craving is a cause for suffering when begins to see this at this point.

106
00:13:58,000 --> 00:14:07,000
And one wrestles with that in the sense that the meditation is showing one some fairly uncomfortable truth.

107
00:14:07,000 --> 00:14:18,000
And so the wrestling has to do with understanding that this isn't a product of the meditation.

108
00:14:18,000 --> 00:14:24,000
The meditator's first instinct is something wrong with the meditation because craving doesn't lead to suffering.

109
00:14:24,000 --> 00:14:27,000
When I want things, I get them.

110
00:14:27,000 --> 00:14:31,000
The problem here is I'm not getting what I want.

111
00:14:31,000 --> 00:14:38,000
The meditator begins to see a lot of suffering stress. It's quite stressful at this point.

112
00:14:38,000 --> 00:14:50,000
It's the one of the stages of the meditation that can be quite stressful is when it's forced to choose between following one's desires

113
00:14:50,000 --> 00:14:53,000
or letting them go.

114
00:14:53,000 --> 00:15:00,000
I mean, both seem to be potential ways to free oneself from suffering.

115
00:15:00,000 --> 00:15:02,000
You want something.

116
00:15:02,000 --> 00:15:07,000
Your two options are get what you want and give up the wanting.

117
00:15:07,000 --> 00:15:12,000
Either one frees you from the wanting, it frees you from the suffering.

118
00:15:12,000 --> 00:15:20,000
And so the meditator begins to see, begins to, I mean, they have this doubt

119
00:15:20,000 --> 00:15:26,000
and it actually can cause some meditators early on in the course to leave or to consider leaving.

120
00:15:26,000 --> 00:15:30,000
It's the most difficult, one of the most difficult parts of the course.

121
00:15:30,000 --> 00:15:36,000
Just psychologically because the meditator is wrestling with this newfound knowledge

122
00:15:36,000 --> 00:15:42,000
that the things that we cling to are not satisfying. They're not stable or predictable.

123
00:15:42,000 --> 00:15:44,000
And they're not controllable.

124
00:15:44,000 --> 00:15:51,000
So one begins to see the three characteristics.

125
00:15:51,000 --> 00:15:59,000
The one's first taste of the three characteristics of imperman and suffering on self are a challenge.

126
00:15:59,000 --> 00:16:03,000
And so there's a lot of doubt in this stage as a result.

127
00:16:03,000 --> 00:16:09,000
So it's called purification by overcoming doubt because the meditator, who successfully navigates this,

128
00:16:09,000 --> 00:16:19,000
begins to gain a real understanding and reassurance of how nature really works, how reality really works.

129
00:16:19,000 --> 00:16:23,000
Starts to realize, oh, this is the problem, not that I'm not getting what I want,

130
00:16:23,000 --> 00:16:26,000
but that I want in the first place.

131
00:16:26,000 --> 00:16:32,000
If I were content, if I were a piece, if I didn't react to things,

132
00:16:32,000 --> 00:16:40,000
that wouldn't suffer. Why? Because those things that I'm reacting to are imperman and suffering in oneself.

133
00:16:40,000 --> 00:16:47,000
They can't possibly satisfy. They're impermanent in that they're uncertain, unstable.

134
00:16:47,000 --> 00:16:54,000
They're suffering, and since they're unsatisfying, and if you cling to them, you're just going to suffer because, of course, they're impermanent.

135
00:16:54,000 --> 00:17:02,000
And they're out of our control, they're not me, they're not my, and they're not self, myself, they're not any self.

136
00:17:02,000 --> 00:17:14,000
They're experiences that arise and cease, and that are based on causes and conditions.

137
00:17:14,000 --> 00:17:19,000
So the meditator who starts to accept this, it really is a liberating moment.

138
00:17:19,000 --> 00:17:32,000
The next stage is what we'll talk about a little bit more next time, are much more peaceful and pleasant reassuring to the meditator.

139
00:17:32,000 --> 00:17:39,000
Once they make this shift, it's really another shift from doubting and uncertainty and wavering.

140
00:17:39,000 --> 00:17:51,000
A lot of wrong ideas about the nature of reality to a certainty to seeing how reality really works and understanding why we were doubting in the first place,

141
00:17:51,000 --> 00:17:58,000
why we were unsure about our lives and what to do where to go. We had it all wrong.

142
00:17:58,000 --> 00:18:05,000
We started to see what's really going on, how reality really works.

143
00:18:05,000 --> 00:18:14,000
Freezing from doubt, when you're free from doubt, there's a certainty, there's a composure of mind.

144
00:18:14,000 --> 00:18:24,000
And that's what propels one into the more pleasant states, the more composed and energetic states.

145
00:18:24,000 --> 00:18:29,000
So it's an encouragement in the beginning stages of the practice, and it's difficult.

146
00:18:29,000 --> 00:18:36,000
You're probably going through this where you start to see the three characteristics for the first time, and it's not pleasant, and you think,

147
00:18:36,000 --> 00:18:41,000
well, this meditation is changing reality.

148
00:18:41,000 --> 00:18:49,000
And reality isn't like this. It must be a meditation that makes things suddenly impermanent and unsatisfying and controllable.

149
00:18:49,000 --> 00:18:56,000
Better go back to my other reality where things are pleasant, satisfying, and controllable,

150
00:18:56,000 --> 00:19:02,000
until you realize, oh no, that's nothing to do with the meditation at all.

151
00:19:02,000 --> 00:19:09,000
This is the nature of reality, whether I'm here or not. You can't escape it by going home.

152
00:19:09,000 --> 00:19:17,000
And at that point one is really in a good way. At that point they begin to practice with pastana.

153
00:19:17,000 --> 00:19:20,000
Here we're not yet in Vipasana.

154
00:19:20,000 --> 00:19:28,000
You're not yet in the stages of insight. Insight isn't said to have arisen until the next purification.

155
00:19:28,000 --> 00:19:38,000
Even though there is insight, it's not really considered Vipasana because when it's still struggling to learn the nature of reality,

156
00:19:38,000 --> 00:19:41,000
it's like at this point we're just learning how the tools work.

157
00:19:41,000 --> 00:19:45,000
It's like you're beginning your carpenter.

158
00:19:45,000 --> 00:19:51,000
And so before you go and carve up some wood or build a cabin in or something,

159
00:19:51,000 --> 00:19:58,000
you have to learn about your tools. This is a chisel, this is a hammer, this is that.

160
00:19:58,000 --> 00:20:04,000
That's what we're doing now. Learning how reality works, learning what it's all about.

161
00:20:04,000 --> 00:20:08,000
Once you do that and the doubt disappears, it means you get this composure of mind.

162
00:20:08,000 --> 00:20:20,000
That's when your tools, you're able to use your mind as your tool and the mind has become a useful tool that you can apply

163
00:20:20,000 --> 00:20:31,000
and begin to address and overcome suffering and stress and difficulty.

164
00:20:31,000 --> 00:20:39,000
So there we are, purification number four already.

165
00:20:39,000 --> 00:21:02,000
That's the demo for tonight. Thank you all for coming up.

166
00:21:02,000 --> 00:21:25,000
The site again is not loading. There was some problem with it last night and again tonight it's not loading.

167
00:21:25,000 --> 00:21:44,000
Performing a TLS handshake to meditation.ceremungalow.org.

168
00:21:44,000 --> 00:21:55,000
It's good to know my browser at least has good manners.

169
00:21:55,000 --> 00:22:21,000
The server is not accepting the handshake though. Here we go.

170
00:22:21,000 --> 00:22:46,000
Everything should be working. We also have an audio stream. I don't know if anyone is actually listening to it, but hopefully it's also working well.

171
00:22:46,000 --> 00:23:13,000
There must be some bottleneck. Everyone trying to access the site at once is causing it to not work, which I really think we know our IT people are overworked and underpaid,

172
00:23:13,000 --> 00:23:39,000
but I really think we should assess whether there's something fundamentally wrong with our setup because if it keeps stopping and we have to reset it, it's clearly only when it seems to be only when we do this session.

173
00:23:39,000 --> 00:23:52,000
It seems to be clear the case of too many people trying to access at once, which means we need a better service.

174
00:23:52,000 --> 00:24:04,000
Something is there's some bottleneck that has to be addressed.

175
00:24:04,000 --> 00:24:13,000
Another excuse for me not to answer questions. Thank you all for tuning in. Have a good night.

