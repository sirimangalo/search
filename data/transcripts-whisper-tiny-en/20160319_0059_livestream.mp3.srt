1
00:00:00,000 --> 00:00:26,000
And we're live leading everyone, March 18th, 9 p.m.

2
00:00:26,000 --> 00:00:32,000
As usual.

3
00:00:32,000 --> 00:00:38,000
Start by looking at the quote.

4
00:00:38,000 --> 00:00:39,000
This is half.

5
00:00:39,000 --> 00:00:41,000
This quote is half of a suit.

6
00:00:41,000 --> 00:00:46,000
It's the second half.

7
00:00:46,000 --> 00:01:04,000
So it starts actually with five Asamaya, Asamaya, Badhana, five

8
00:01:04,000 --> 00:01:13,000
and non-times for striving.

9
00:01:13,000 --> 00:01:18,000
So you've got Asamaya and you've got Asamaya.

10
00:01:18,000 --> 00:01:30,000
There are times for striving and there are times where you should not strive.

11
00:01:30,000 --> 00:01:43,000
So I'm looking at the commentaries, trying to get some clarification here because I think you have to qualify this to some extent.

12
00:01:43,000 --> 00:01:51,000
But there's a valuable lesson to be learned here.

13
00:01:51,000 --> 00:02:09,000
I mean, I suppose the first lesson to consider is other ways of deciding what's the right time to meditate and to strive are often misguided.

14
00:02:09,000 --> 00:02:17,000
So we have to think, well, I'd like to meditate, but I just can't find the time.

15
00:02:17,000 --> 00:02:23,000
I just can't find the right time to do it.

16
00:02:23,000 --> 00:02:31,000
I don't have enough time or it's just not the time.

17
00:02:31,000 --> 00:02:39,000
But as countries, they often say things like, well, when I get old, I'll meditate.

18
00:02:39,000 --> 00:02:42,000
When I'm young, I've got work to do.

19
00:02:42,000 --> 00:02:51,000
But when I get old, I'll take the time to meditate.

20
00:02:51,000 --> 00:02:55,000
So we put it aside.

21
00:02:55,000 --> 00:03:08,000
I think to some extent, this suit, as I said, has to be qualified because we've also said that mindfulness is always useful.

22
00:03:08,000 --> 00:03:21,000
And that's, I think, important that striving is not the only way of meditating, not the only way of progressing or practicing.

23
00:03:21,000 --> 00:03:31,000
Anyway, let's go through these, because it's important because there is a certain amount of striving that you shouldn't do at certain times.

24
00:03:31,000 --> 00:03:34,000
Let's start with the Asamia.

25
00:03:34,000 --> 00:03:55,000
So how do you interpret this?

26
00:03:55,000 --> 00:04:09,000
Is he saying that old people shouldn't meditate, shouldn't practice, shouldn't strive spiritually?

27
00:04:09,000 --> 00:04:15,000
No, in fact, I think what is being said here is this is a bad time.

28
00:04:15,000 --> 00:04:16,000
It's harder.

29
00:04:16,000 --> 00:04:18,000
It's a harder time.

30
00:04:18,000 --> 00:04:21,000
And the important lesson there is don't wait till you're old.

31
00:04:21,000 --> 00:04:26,000
Because old people will tell you, and there's, there's difficulties.

32
00:04:26,000 --> 00:04:34,000
I mean, interestingly enough, as I said, it's when people, mostly there's the excuse them to young.

33
00:04:34,000 --> 00:04:36,000
I'll do it when I'm older.

34
00:04:36,000 --> 00:04:41,000
So interestingly enough, a lot of old people do take up meditation and get good results.

35
00:04:41,000 --> 00:04:48,000
But if you accumulate so much baggage that there's a lot more work to be done.

36
00:04:48,000 --> 00:04:55,000
And you have the Buddhist words here and another place is that this is not the right way to look at things.

37
00:04:55,000 --> 00:05:01,000
As if we skip down to the first one on the positive side.

38
00:05:01,000 --> 00:05:19,000
In the Bhikkhu, Bhikkhu, Daha, Roho, T.

39
00:05:19,000 --> 00:05:25,000
One is a chai, a yu, a yu, a yu.

40
00:05:25,000 --> 00:05:32,000
It turns great.

41
00:05:32,000 --> 00:05:44,000
Don't know, Bhikkhu, Banyina, you know, Banyina, use in a height of their use, something like that.

42
00:05:44,000 --> 00:06:03,000
Samanagato is possessed of the height of use, the best part of use, the strength of use, the greatness of use.

43
00:06:03,000 --> 00:06:13,000
Bhata me no wa yasa, in the first, the first waya, the first part of life, the first angel life.

44
00:06:13,000 --> 00:06:24,000
I am Bhikkhu, but the most amayo, Badana. This is the first occasion, the right time for striving.

45
00:06:24,000 --> 00:06:28,000
Do a wa yu young.

46
00:06:28,000 --> 00:06:36,000
Because every moment that we're unmindful is accumulating karma, it's accumulating habits.

47
00:06:36,000 --> 00:06:41,000
And so he can say that old people can benefit from meditation absolutely.

48
00:06:41,000 --> 00:06:46,000
But they also have a lot of habits built up.

49
00:06:46,000 --> 00:06:54,000
And so as far as how far they can get, you know, it depends on the person, of course,

50
00:06:54,000 --> 00:06:59,000
but people have greater potential, greater strength.

51
00:06:59,000 --> 00:07:05,000
If you've seen some of these young monks meditating day and night,

52
00:07:05,000 --> 00:07:15,000
they're able to walk very well, they will sit very still, their minds are sharp.

53
00:07:15,000 --> 00:07:22,000
I mean it goes both ways because old people have a lot of wisdom that's required to understand why we're meditating

54
00:07:22,000 --> 00:07:32,000
and people meditate for a little while and then get bored and give it up.

55
00:07:32,000 --> 00:07:36,000
Nonetheless, something that you should start young, get ahead, start.

56
00:07:36,000 --> 00:07:40,000
And the old meditator will tell you, don't wait until you're old.

57
00:07:40,000 --> 00:07:43,000
It's not much harder.

58
00:07:43,000 --> 00:07:49,000
Number two, when you're sick, when you're sick, don't put out effort.

59
00:07:49,000 --> 00:07:53,000
That's just interesting because you should meditate. You should be very mindful, as I said.

60
00:07:53,000 --> 00:08:02,000
And with another place, as I said, that sickness is a great catalyst for insight.

61
00:08:02,000 --> 00:08:09,000
But you shouldn't work hard because you can aggravate the sickness and prolong the sickness.

62
00:08:09,000 --> 00:08:13,000
You should take care of your body, you know, rest a lot, lie down.

63
00:08:13,000 --> 00:08:17,000
Don't do lots of walking and don't push yourself.

64
00:08:17,000 --> 00:08:22,000
Don't push your body anyway.

65
00:08:22,000 --> 00:08:32,000
When the opposite, if you're, when you're healthy, that's when you're should meditating because when you're sick, it gets harder.

66
00:08:32,000 --> 00:08:36,000
You know, it may be a great time to be mindful, but it's much harder to be mindful.

67
00:08:36,000 --> 00:08:38,000
And of course, you can't do all the exercises.

68
00:08:38,000 --> 00:08:42,000
So the point is, when you're healthy, take that time to meditate.

69
00:08:42,000 --> 00:08:45,000
Don't be negligent.

70
00:08:45,000 --> 00:08:48,000
A lot of talk about the negligence of health.

71
00:08:48,000 --> 00:08:52,000
Oh, I'm healthy now. I don't need to worry about suffering.

72
00:08:52,000 --> 00:08:55,000
I don't need to worry about my defilements.

73
00:08:55,000 --> 00:08:58,000
I'm healthy. I can get what I want. I can do what I want.

74
00:08:58,000 --> 00:09:03,000
If I have problems, I just chase them away or I run away from them.

75
00:09:03,000 --> 00:09:12,000
And then when we're sick, we don't know what to do. We're unprepared.

76
00:09:12,000 --> 00:09:18,000
So it's a good lesson, rather than thinking, oh, I'm happy, so why should I worry?

77
00:09:18,000 --> 00:09:21,000
Should I work? Why should I train myself?

78
00:09:21,000 --> 00:09:28,000
I should think that when you're happy, when you're well, you should prepare yourself for being unwell.

79
00:09:28,000 --> 00:09:35,000
You should prepare yourself. It's a good time to train yourself and to make yourself stronger,

80
00:09:35,000 --> 00:09:39,000
because making yourself stronger once you're already unwell is much more difficult.

81
00:09:39,000 --> 00:09:45,000
The person's never practiced meditation and never practiced mindfulness, and then they get sick.

82
00:09:45,000 --> 00:09:50,000
It's very hard to teach the meditation at that point, because they won't hear it.

83
00:09:50,000 --> 00:09:53,000
Their mind is too reactionary.

84
00:09:53,000 --> 00:09:56,000
Better to start with when it's easy.

85
00:09:56,000 --> 00:10:04,000
Just start with when it's hard, much harder to start.

86
00:10:04,000 --> 00:10:10,000
It doesn't mean you shouldn't be mindful and you shouldn't try your best to be mindful when you're sick.

87
00:10:10,000 --> 00:10:16,000
When you're sick, it's a great time to learn and to understand, to learn to let go,

88
00:10:16,000 --> 00:10:24,000
because you have suffering and suffering is something that we react to very strongly.

89
00:10:24,000 --> 00:10:35,000
Number three, do we come, do we come, holding one at a time when there is not much food?

90
00:10:35,000 --> 00:10:41,000
Do sassan? Do sassan is sassan?

91
00:10:41,000 --> 00:10:51,000
Crops, right? When there are poor crops, do love up in them when it's hard to get arms.

92
00:10:51,000 --> 00:11:08,000
No soup, karang, unchain, it's not easy to gather, to something, something asks,

93
00:11:08,000 --> 00:11:23,000
all right? Begging, you're happy to eat. Why does it say that you're happy to eat?

94
00:11:23,000 --> 00:11:33,000
Anyway, not easy to keep on's life going on sassanins or something like that.

95
00:11:33,000 --> 00:11:38,000
When it's hard to get food, food is scarce.

96
00:11:38,000 --> 00:11:45,000
This is an issue for monks more than laypeople, I suppose, but it can happen when you're poor,

97
00:11:45,000 --> 00:11:50,000
that food is scarce. Well, don't push your body at that point.

98
00:11:50,000 --> 00:11:54,000
Remember, we had one person who came to meditate and wanted to do a fast,

99
00:11:54,000 --> 00:11:59,000
so they were just going to have fruit for like three weeks.

100
00:11:59,000 --> 00:12:08,000
So after two weeks, he came to my hut and said he had like pulled a ligament in his leg.

101
00:12:08,000 --> 00:12:14,000
Because it's hard, you know, this isn't like some yoga retreat where you just lie on your mat

102
00:12:14,000 --> 00:12:21,000
and stretch all and do simple stretches. You're actually doing hours and hours of walking, you know?

103
00:12:21,000 --> 00:12:34,000
And it's insight, so you're dealing with ordinary states, you're not entering into very high and special states.

104
00:12:34,000 --> 00:12:39,000
You're dealing with a lot of difficulties because we're trying to learn and understand how your mind works.

105
00:12:39,000 --> 00:12:45,000
So digging right in there with the problems, it's very strenuous.

106
00:12:45,000 --> 00:12:56,000
No, it's very taxing on your system.

107
00:12:56,000 --> 00:13:09,000
So when you have to nourish your body, so you're not able to find nourishment, that's the time you have to be careful.

108
00:13:09,000 --> 00:13:22,000
And then Bayanghoti, Atavisankopo.

109
00:13:22,000 --> 00:13:27,000
When there is danger.

110
00:13:27,000 --> 00:13:49,000
There is peril, turbulence in the wilderness, and the people of the countryside mounted on their vehicles flee on all sides.

111
00:13:49,000 --> 00:13:57,000
Think you'd be good, buddy. Yeah, there's danger.

112
00:13:57,000 --> 00:14:02,000
When there's danger, you shouldn't be careful, I shouldn't go out and do walking.

113
00:14:02,000 --> 00:14:08,000
If there's a war in your area, you have to take care of your body.

114
00:14:08,000 --> 00:14:18,000
It's not a good time for striving because you'll be worried about dangers and you might even be attacked and harmed.

115
00:14:18,000 --> 00:14:27,000
And so, I mean, part of the lesson here is that when you don't have danger, when life is good,

116
00:14:27,000 --> 00:14:32,000
this is when take advantage of the moment, take advantage of the opportunity.

117
00:14:32,000 --> 00:15:01,000
So, when there's harmony in society, manosa, samangas, samodhamana, avivandhamana, and people are dwelling in harmony.

118
00:15:01,000 --> 00:15:16,000
When they're not fighting, key road, keyboard, having become like milk and water, mixing like milk and water without any turbulent people can mix.

119
00:15:16,000 --> 00:15:40,000
And when there's, when people are looking at each other, fondly, pia, chu, he, sampasanta, we had a day, doing dwell, harmony, eyeing each other favorably, kindly, dearly.

120
00:15:40,000 --> 00:15:49,000
Number five is when your community, when the sun guy is dwelling, when the sun guy is split up.

121
00:15:49,000 --> 00:15:58,000
When your meditation community, when your community, when your family, that's a time where it's doing it's difficult to strive.

122
00:15:58,000 --> 00:16:09,000
So, when your family is in harmony, you should take that opportunity to strive.

123
00:16:09,000 --> 00:16:19,000
And so, I don't think this really means that you shouldn't try and be mindful, you should just give up and throw in the towel when things are difficult.

124
00:16:19,000 --> 00:16:24,000
It's more of a lesson of how difficult it is with those things.

125
00:16:24,000 --> 00:16:26,000
And so, to take advantage of the time.

126
00:16:26,000 --> 00:16:35,000
But it's also a warning that you shouldn't push too hard when there are things that will be affected by striving.

127
00:16:35,000 --> 00:16:41,000
So, when your body might be injured, because you're striving, because you're not getting enough nourishment.

128
00:16:41,000 --> 00:16:48,000
When you might be killed, because there's, and there's, you know, war going on, and that kind of thing.

129
00:16:48,000 --> 00:16:58,000
Nor are you might be the subject to all sorts of problems, because people are fighting, and your family, or your community is fighting.

130
00:16:58,000 --> 00:17:04,000
You should resolve some of these problems.

131
00:17:04,000 --> 00:17:06,000
Anyway.

132
00:17:06,000 --> 00:17:10,000
Interesting. So, giving the five.

133
00:17:10,000 --> 00:17:20,000
Good times to strive, and the five times when striving becomes difficult.

134
00:17:20,000 --> 00:17:26,000
So, the next step.

135
00:17:26,000 --> 00:17:32,000
I'm going to post the Hangout link on meditation.serimongolow.org.

136
00:17:32,000 --> 00:17:37,000
If you want to join the Hangout, you're welcome to come and ask questions.

137
00:17:37,000 --> 00:17:47,000
This is where I take live questions for anyone brave enough to join me with a video webcam and a microphone.

138
00:17:47,000 --> 00:17:54,000
Give me a couple of minutes if nobody shows up, then we say goodnight.

139
00:17:54,000 --> 00:18:07,000
Tomorrow, I'm giving a talk in Second Life as far as I know. That's at 12 p.m., 12 noon, Second Life Time, in the Buddha Center.

140
00:18:07,000 --> 00:18:21,000
So, if you know what that means, come on out. If you don't know what that means, don't worry about it.

141
00:18:21,000 --> 00:18:25,000
Oh, and it looks like I'm going to New York City.

142
00:18:25,000 --> 00:18:36,000
If you're in the New York City area, it looks like I might be there from the 16th of April to the 23rd of April.

143
00:18:36,000 --> 00:18:39,000
It looks sometime around that time.

144
00:18:39,000 --> 00:18:47,000
It's not confirmed yet, but it's decided upon. Agreed upon.

145
00:18:47,000 --> 00:18:55,000
It looks like I'll be doing some meditation teachings in New York City.

146
00:18:55,000 --> 00:18:58,000
Some special memories of New York City.

147
00:18:58,000 --> 00:19:12,000
I was flying to Thailand as a monk, and I brought along a novice, a Cambodian novice.

148
00:19:12,000 --> 00:19:18,000
This was, of course, back when I was with monks who were all using money, but I decided,

149
00:19:18,000 --> 00:19:24,000
this was the first time I decided that I was going to stop using money.

150
00:19:24,000 --> 00:19:27,000
I said, we're not bringing any money on this trip.

151
00:19:27,000 --> 00:19:34,000
We got to New York City, and I had left my passport on the plane in the seat in front of me,

152
00:19:34,000 --> 00:19:36,000
and it went back to Toronto.

153
00:19:36,000 --> 00:19:55,000
We missed our plane from New York City to Korea, Canada, Korea, Taiwan, probably Taiwan.

154
00:19:55,000 --> 00:20:01,000
And so we spent two nights, I think, in, you know, just maybe one night.

155
00:20:01,000 --> 00:20:09,000
We spent about 20, almost 24 hours in over 24 hours in New York City.

156
00:20:09,000 --> 00:20:13,000
And we wouldn't have survived if the novice hadn't secretly.

157
00:20:13,000 --> 00:20:15,000
He pulled ice and I don't know what we're going to do.

158
00:20:15,000 --> 00:20:17,000
So he pulls at like $100.

159
00:20:17,000 --> 00:20:21,000
He had stuck some money.

160
00:20:21,000 --> 00:20:27,000
So he had money to get a taxi and then get a train back to the airport.

161
00:20:27,000 --> 00:20:38,000
So we got a taxi to the Cambodian monastery, and then we slept some time in the JFK airport.

162
00:20:38,000 --> 00:20:44,000
But I was there a couple of years ago as well, just passed through,

163
00:20:44,000 --> 00:20:49,000
ended up staying a couple of nights in Long Island.

164
00:20:49,000 --> 00:20:56,000
Maybe I'll go to Long Island again this time to the Sri Lankan meditation center there.

165
00:20:56,000 --> 00:21:02,000
That's a really nice, good friend of mine, Nanda.

166
00:21:02,000 --> 00:21:07,000
Hello, hello Stephen.

167
00:21:07,000 --> 00:21:11,000
Or Stephen.

168
00:21:11,000 --> 00:21:14,000
Hello.

169
00:21:14,000 --> 00:21:21,000
Your mic was tired, I can hear you.

170
00:21:21,000 --> 00:21:27,000
I can hear you, but I don't know what you're saying.

171
00:21:27,000 --> 00:21:32,000
Do you have a question?

172
00:21:32,000 --> 00:21:40,000
Hello.

173
00:21:40,000 --> 00:21:48,000
I don't know, man.

174
00:21:48,000 --> 00:21:50,000
Interesting.

175
00:21:50,000 --> 00:21:51,000
All right.

176
00:21:51,000 --> 00:21:57,000
I'm assuming no questions.

177
00:21:57,000 --> 00:22:05,000
So where will I be teaching in New City?

178
00:22:05,000 --> 00:22:07,000
I don't know yet.

179
00:22:07,000 --> 00:22:08,000
Nothing's confirmed.

180
00:22:08,000 --> 00:22:11,000
It's all very preliminary.

181
00:22:11,000 --> 00:22:15,000
I'll let you know.

182
00:22:15,000 --> 00:22:18,000
Have a good night everyone.

183
00:22:18,000 --> 00:22:25,000
See some of you in second life tomorrow.

184
00:22:25,000 --> 00:22:42,000
Good night.

185
00:22:42,000 --> 00:22:49,000
Thank you.

