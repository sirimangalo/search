We are getting settled in as best you can hope the combinations are comfortable, suitable
for practice. If I thought today I'd begin to move forward to talk a little bit about the
path of progress. It's quite interested in progress. I tell you not to be too fixated
on progress, because it often relates to the future. It leads to expectations, and
we should agree and worry, fear that you might not progress. What I'm going to talk about
today is a lot of the progress that you've already made. What I want to do is sort of
solidify that foundation. Before we talk about the rest of the path, it's like if you want to
build a palace, you better have a pretty strong foundation. It's not been too quick to rush
to build the walls and the roof of our house. This is a common problem for meditators,
you get to the point where you realize you've been kind of rushing it, rushing ahead towards the
results without working hard enough on the foundation. Then you have to go back and fill it in.
Let's deal with the foundation for some of the basics again. We have a book. It's an ancient
commentary called the Visudimaga, which means the path of purification, and the Buddha's teaching
the Buddha himself called it the path of purification. It contains his path.
He is someone who is so dear, this is the path of purification. When writing this book
that was supposed to be the outline of the Buddha's teaching and the Buddha's path,
he started off by reminding us of a verse the Buddha said when he was asked by an angel,
I would ask you all quote, I say, this branch of Buddha, and my name is Bh Eata toxicity
And this one, the intertango and the outer tatango, and the whole of
The people, the people are entangled by a tangle inside, tangled up inside, tangled up with the world around them.
This is an angel looking down on the earth 25 hundred years ago. Imagine what they're looking at now.
Oh, why? The tangles have just gotten worse.
Either tangle is the other tangle.
The pamgo, the makujabi, we ask go to mam.
Who will be able to untangle the tangle?
What sort of person can untangle the tangle?
These are the sort of questions that angels day was asked for Buddha.
If you read the day was something that's very different from a lot of the other teachings.
Very poetic. It feels very angelic.
Sometimes it's hard to understand their questions. Their questions are often very sort of nebulous.
The allegorical or metaphorical and that sort of metaphorical tangle.
And the Buddha's response was what Buddha Kosa used to, as the basis of his book, the vise timaka.
The vise kos, sea lee, patitaya, na rosa panyu, chita, panyu, jalpaya, jalpaya, jalpaya, jalpaya, jalpaya, jalpaya, jalpaya, jalpaya, jalpaya, jalpaya.
See lee, patitaya, found it on, having found it themselves, or establish themselves on sea lee, or james, behaviour, or ethics.
A wise person, na rosa panyu, a person with wisdom.
Based on, established on sea lee, they develop jita and panyu, jita nipanyu, jalpaya, jalpaya, jita nipanyu, jalpaya, jita nipanyu, jita nipanyu, jita nipanyu, jita nipanyu.
It means the mind, but they focus purifying their mind, and with that pure mind, gives rise to wisdom.
A person with wisdom, he's praising it by saying, that's a person that's a big group, a true big group. It's a true recloser, true monk.
Not just listening to me when I tell you to walk and sit that they're actually seeing clearly.
And so, he used that as his basis, reminding us, it's nothing new. He reminds us that a Buddha ordered his teaching, sea lee, and samadhi, and then panyu.
And so, he outlined three parts to his book. The first part is sea lee, the second part of samadhi, the third part of samadhi.
But I bring that up specifically because sea lee is our foundation, we have to have a strong foundation, and sea lee is the basis.
So, before you even come to practice, we have rules. If you're going to come to practice here, you need to keep certain rules, and rules are an important part of seaman.
It's a very long time today. But rules are all of seaman. The rules are the beginning of seaman. There's called the fourfold sea lee, which is basically ethics, or it's the basic foundation relating to your activities or your behavior.
So, sea lee is, in some way, sea lee is always with us. It isn't just when you didn't break the precepts, or when you promised to keep the precepts.
Sea lee is the way you live, the way you approach your life, your day, your experience.
So, it's a very important, it's a constant part of the practice.
In four ways, the first one is the precepts, so you don't break the precepts. Don't kill you, don't steal, you don't die, don't cheat.
Sexual activity, no drugs are alcohol, no overeating, no entertainment, no over sleeping, that sort of thing.
Those are rules that are useful. They're helpful. If we do that, that will be a good strong foundation for us to develop some idea.
But the second one is the represents, and we went over this yesterday, so I won't go into detail.
But the part of our sea lee is our use of things, so that we don't misuse things or overuse things.
Don't overeat. Don't deck yourself out in beautiful clothes. Don't oversleep or get attached to your dwelling or your bed.
Don't abuse medicine, that sort of thing.
The third one is our livelihood, and again that really doesn't apply.
But one of the great things of being here is someone mentioned today how useful it is to come here,
because you don't have to worry about your livelihood.
Lightly it is now very simple for you. How do you make a living?
Go to the kitchen, pick up your food, go back to your place and sit down and eat there.
You can live for the day.
Everything is made very simple for you. That's why meditation centers present.
But the ideal environment, everything is quite a luxury in some ways.
Of course the place doesn't seem luxurious, but the luxury is that you are free from the burdens of having to make a living.
For example, having to go to work.
It's an environment that is very suitable for meditation.
So having a right-line leave it from...
See it applies to monks, or monks, or don't go around taking.
So for you guys things like don't go looking for special food or going out of your way to take the last of whatever is really good or something like that.
Don't be greedy, try to be content of whatever there is, and there's some for everybody else.
And that comes not just with food, but for everything, just be content.
But it's at Subhara, which is not burdensome in how we live our lives.
But the most important I think in relation to meditation is the fourth part of Siva.
And that's the Garden of the Senses.
And you can see that in how Kudagosa describes it.
He reminds us of how a Garden of the Senses is really very much associated with mindfulness.
So I've talked about that as well.
But in Garden of the Senses it allows us to see how Siva, first of all how it's very much associated with the mind,
which is something we don't always notice.
We always think of Siva as the physical thing.
We just don't kill them.
Well that's a physical activity, but it really isn't.
The problem isn't the killing.
The problem is the mind that in ten is to kill.
If you step on an end and it didn't know it was there,
I'm not looking for the end, but it's not an wholesome in your mind.
You didn't have any on wholesome intentions.
So it's not a breach we see that.
But when you think of it as Garden of the Senses,
and it's really every experience becomes ethically, potentially ethically charged.
When you walk, walking can be ethical or unethical.
Sometimes when you're angry you walk very loud, no?
They say you can tell the difference between someone who is greedy,
someone who is angry and someone who is deluded by the measure of their steps
now you're all going to be checking.
A person who is angry, boom, boom, boom, they step with their heels, mostly.
A person who is greedy steps with their toes.
Don't go in there.
They're keen here.
An eager.
And the person who is deluded has no pattern to their steps.
Sometimes with their toes, sometimes with their heels,
sometimes with wide, sometimes short.
This is the sort of person who doesn't have any real pattern
because that might their own mixed up.
But they also say, and this is to keep your new toes,
they also say that it's not a very good indication
because when people know about it, then they can alter their steps
and they're all going to be altering their steps.
It's a good reminder to us we can say,
oh yeah, this is what we're doing.
Why I'm stop things because I'm angry.
But if you walk angry, that's an ethical act.
If you speak angrily, that's an ethical course, right?
Even if you wish someone well, but you're angry about that,
it's still an ethical.
And then it goes to simple experience.
There's a story of a monk who stopped to smell a flower.
And there was his angel living in the flower
or in the forest or whatever, and it said,
feed, feed.
I don't know.
What is this?
Stealing the smell, he's stealing the scent
because he's a monk in his community.
It's not an ethical to know in your ordinary life
you're not going to be that concerned about these things.
But here, it's not seeing you.
We're not really all that vindictive or hateful
or we're not demanding.
These are commandments.
The point is that this is helpful.
If you keep your mind pure and clear and just object it,
you'll find you have a greater peace, right?
You've been here for a while.
There's moments of peace that maybe weren't there before.
You start to get that as you start to free yourself
from your judgment, from your personalities.
So try and work with that when you see or hear or smell.
Remember, in everything you do,
think of it as ethically charged every moment
and we can keep ourselves pure.
And when we're mindful, we fear ourselves
from the stress and the energy that it takes
to be partial towards it.
So that's ethics.
This is called, this is the first purification.
So this is not from the commentaries.
This is in the Suptans in Machimani by 24.
There's a numeration of the seven purification.
It's the seven recentings.
And that's another part of the Risudimami.
He separates it up based on these seven pages
of the Risudimami.
Right?
And then it's a vitica.
So see, the Risudimami is the first purification.
It's the first part of the path.
But that doesn't mean that once you've done it
and then you can put it aside, it's really just the foundation.
And in some ways, therefore, we call SEMA the actual practice.
When you're mindful, when you're walking mindfully
when you're sitting and you're experiencing
the stomach rising and falling, just as an example,
it is pure.
And your SEMA is pure.
Your behavior.
Your meditation is like an act.
It's a behavior.
So as you perform in the meditation,
and so it's the most ethical act you can do
when you walk mindfully when you sit like this.
The second purification is called JITWISUDI.
JITWISUDI is the whole of the SEMAD section.
It's an idea of one single thing.
It's the development of the mind.
It's the purification of the mind.
It's called JITWISUDI, which that's literally
being purification of the mind.
And it's a little bit surprising, I think,
when you refer to your decision to think
that this is only the second of the purification.
So that's not actually the goal.
Because we think, well, the purification of mind
is the goal, isn't it?
But the problem is the mind is not a single entity.
And if your mind is pure right now,
you probably notice that that doesn't actually
ensure that your mind is going to be pure the next moment.
Mind is just a moment that each mind has
a moment of existence and arises in its seasons.
And so some of our minds are pure and some of them are not.
And they come and they go.
So we're trying to do something actually
a little bit beyond just purifying the mind.
And basically purification of purification
of our understanding that through experience,
which is the whole rest of the chapters,
our mind is unable to not be pure.
We cultivate purity through our practice in our mind
and that purity leads to vision.
And it's that vision that keeps our mind pure.
It keeps our mind from getting impure.
But first, before we can see clearly,
our mind has to be pure.
We do need these moments of purity.
And there's different ways to go about that.
You can undergo a very strenuous process of cultivating
just pure mind states where your mind is perfectly pure.
You're not interested in wisdom yet.
You're just focused on something that
calms the mind and purifies the mind.
And you have this very strong and no hindrances,
no liking, disliking, drowsiness, destruction,
and just an alert pure mind state.
And if you do that for a while,
the power of that can be very useful in cultivating wisdom.
But it's also possible that that in and of itself
doesn't lead to wisdom.
No matter how you do it,
eventually you have to come to the point
where you apply the quality of your mind.
The purity of the mind.
You apply it to your experience.
So that's what we're doing here.
What we're doing is kind of a condensed course
because there's so many things we could teach you
and it could take years to really cultivate these intense
states of calm and then maybe even gain magical powers.
Remember your past life.
Some centers they actually do that.
But ultimately it's not necessary to go to that length.
It's just more thorough and more powerful, let's see.
But here we have 14 days of don't discount
the greatness that you can get by focusing on
what's actually important.
And that's this momentary concentration focused
on momentary experiences.
And it's different.
It's extra in that.
It's focused on things that can actually help you understand
how your mind works.
For example, there's meditations
where you focus on the Buddha.
Focusing on the Buddha will never teach you directly
how your mind works because you become so focused
on the Buddha as an idea or may all things be happy.
It's very false.
I've been very good.
But as long as you're focused on the means,
the ideas of people or all beings or one being,
that doesn't matter.
You're not going to be focused on the actual experience
on a momentary level.
So what we're doing here is taking our level of concentration
and applying it to our experience.
Because you haven't spent all that time developing your concentration,
it's a little harder at the beginning.
There's no question.
But I hope you can see that after a few days,
instead of taking months or years to develop
very high states of concentration,
even after just a few years, you're able to do it at the same time.
So this is called samata and we pass them together,
technically speaking.
You can do some at the first and then later practice.
We pass them down.
What we're doing is both of them together.
So you're at the same time you're developing concentration,
you're also developing wisdom or clarity.
Because the focus of our attention is on our actual experiences,
which are able to give us wisdom.
So that's chitwis routine.
That's what you're struggling with now.
You find your mind is sometimes pure,
you're sometimes impure.
And as you practice, you'll start to see more moments
in state security,
where you're actually able to experience things clean.
The next stages,
and this is sort of where your guys are now,
where you've just come through,
that just going to go over again,
so that we can sort of solidify our foundation.
Then another day I'll talk about some of the more later stages.
But once you've cultivated samadhi,
and you start to cultivate this clarity of mind,
this purity of mind,
based on your actual experience,
you start to learn things about reality.
You start to understand reality more clearly.
On the first day,
this would have been the first,
or while the second reporting session
or third reporting session at home course,
I asked you a whole bunch of questions
that some of you might vaguely remember
about when you step,
step being right, step being left,
the right and the left are the one thing or something.
When you sit in the same rising or falling,
are the rising and falling,
one thing you're separately.
I had to get the right answer,
and I couldn't give you the next step.
This is relating to the third purification.
This is called purification of you.
You don't have to remember all these,
just kind of get a sense of what I'm talking about.
This is where you start to be able to see things as they are.
It's a different sort of perspective than we're used to.
We're used to looking at reality as things,
cups and phones and rugs and people and places and so on.
Concepts, these are concepts.
This isn't what we actually experience.
So underneath all of that is the seeing of the cup
or the seeing that gives rise to the idea of cup in the mind.
The hearing that gives rise to the idea of the meaning behind the words
or the hearing of a bird or the dog
gives you the idea of the bird of the dog.
But that the basis is the hearing of the experience
when you walk, step being right, step being left.
Ordinarily, we think of this as I'm walking.
These are my legs moving.
But as you practice meditation, you see underlying that,
the only way I have an idea of I'm moving or my legs are moving
is because of each individual experience that actually arises in season.
At the very base of this reality is what gives me the information
that tells me that I'm walking and that's the experiences.
And those are momentary.
So step being right and rises in seasons, step being left
or rise in seasons.
This is like opening the door to a new reality
or a new perspective on reality.
When you sit and say rise,
falling, the rise has a beginning and an end.
The falling has a beginning and an end.
Everything, every experience has a beginning and end.
And then I ask you whether when you watch,
rise, is there only the rising or is there also the mind that knows the rising?
So the other thing you learn here is that there is two parts
to this experience thing.
There's the objects and there's the subject, the mind,
the knowing of it.
If your mind is off somewhere else, you don't know that the stomach's rising.
It's only when the mind is there as well that there is the knowledge of the rising.
So this purification is crucial and very important,
but by this point, it probably seems kind of benign.
But just think if you haven't ever looked,
if you haven't ever taken this perspective of seeing experiential,
you couldn't ever stop to break apart your reactions,
how you react to things from your reactions lead to stress or suffering.
So it's the very first step towards wisdom.
It's the very first stage of wisdom.
This is an apnea section.
Purification of you.
It means seeing things in the right way.
I'll stay truly hard.
The fourth stage of the fourth purification is called Kalun-Kawi,
Dr. Nouri-Santi.
And this relates to right and wrong.
And it has a lot to do with why you're here.
If I tell you greed is bad,
the question that is a person, an ordinary person,
is it really anger is bad?
Sometimes it's good to get angry.
But when you get to the second stage of knowledge,
it's hard to avoid the truth that some things are just not good.
On an experiential level,
and you start to see that,
you see this leads to this.
That leads to that cause and effect.
So it's after seeing what exists,
you start to see how things relate to each other.
What experiences, what reality is cause one?
We have questions for that.
It's sort of testing you to see if you get there.
Once you get there, then we can rest easy
because we know you're starting to get on the right path
because you start to appreciate why you're here.
What is the benefit of mindfulness?
What is the importance of it?
Because you start to see that you have inside some bad causes,
some things that cause suffering.
You start to see how it's not other people that are causing me suffering.
So it's us, our habits, our reactions to things.
The fourth purification is called
Magha Magha Nyanadasana-wisutiti.
In this one, I'm going to spend a little bit of time on.
Magha Magha Nyanadasana-wisutiti.
So I said in the second one,
you already get an idea of right and wrong.
But you still have to...
It's more like in Gantawitara-namisutiti,
you start to see cause and effect.
In Magha Magha Nyanadasana-wisutiti,
you start to put that to use and pick out
which is right and which is wrong.
And in the beginning, that is just seeing what causes you suffering.
But it actually goes deeper than that,
and that's what I want to spend some time on is that
you can actually lose your way and get onto the wrong path
by following something positive, something good.
This is an important part of our explanation of inside meditation.
That you're all sort of on a good ground with,
but let's just firm up the ground and make it very clear
that this is an important thing for you to keep in mind.
What do I mean by good things can lead you astray?
Many of you have already kind of taken to taskfulness
or just reminded you because there are so many good things that come
once you start to deal with the bad things.
Once you get a hang of anger or greed,
it still comes up, but you're less scared of it
to be less overwhelmed by it,
to be less under its power.
You do start to calm down.
You should all start to,
don't worry if you don't really,
but you should all start to feel some calm, some peace.
And most of you will start to think that's the path
that you dwell in it.
And we like it.
Some people will see lights and colors
and just follow after them, dwell in them.
So I've said to many of you to be vigilant.
It's not a bad thing.
Happiness isn't a bad thing.
Calm is a good thing.
It's a good sign.
But as soon as you take them to be a good thing,
you're in trouble because you're going to get a sad try to write it.
So I can't tell you they're a good thing.
Don't tell anybody.
They're not actually a good thing.
These are good things that we call bite products.
They're good as fruit.
They're good as a fruit of the practice,
like a reward.
Like if I give you a ribbon because you put a sticker
on your paper because you have a perfect assignment or something.
It's a sign that you're doing good.
But you don't stop studying or stop going to school
because I've got this start.
The start is a sign that your practice was good.
So there are many good things that come up
and they can side track us.
And they're not a problem until they
have to prevent us from doing what it was
that allowed us to start to experience
more positive experiences.
And that's be mindful.
Happiness doesn't lead to happiness.
Goodness leads to happiness.
So we always practice goodness.
We never practice happiness.
Don't just dwell in the calm and think,
oh, now my practice is going to just so calm.
No, the result of your practice is good.
Your practice is calm.
To waste because you stop practicing.
There's 10 of these 10 things.
And it could be more of an enumerate 10 things
that are called the ribustinopathy Lisa.
They're the defilements of insight
or defilements of seeing clearly.
And above means they're kind of not
exactly defilements.
They're not evil or bad things, but they're imperfections.
They can cause problems, not because of what they are,
but because of how we were linked to them.
So you might see things, as I said,
see lines, colors, pictures, all sorts of crazy things.
Maybe you feel very bright in the mind.
Nothing wrong with that.
But it is important and easy to forget
that you should not see, see,
just stay with it until it goes away.
Another one is called nyana.
Your own order here, but let's say nyana is next.
The other is knowledge.
And this is what I don't think I've talked about.
You can gain a lot of insight through meditation,
through this meditation.
You may have noticed that some problems in your life
you get solutions to them.
Or even just about the meditation, you just start to understand.
And beginning maybe you didn't understand
about mindfulness and that.
I can't understand it.
Why is this a bad thing?
It's not a bad thing, but again, it's the results.
It's not the practice.
And if you cling to it, you stop practicing.
Clinging in the sense of thinking about it,
going over it, extrapolating it, revisiting it.
Sometimes you think, oh,
what is the next one going to come?
What more could I learn in the future?
So it takes you away from being mightful.
Anything you, any learning you gain,
any insight you gain, any epiphanies you have,
all of these have to be taken as a good sign,
but a potential problem if you get sidetracked by it.
If you forget that they're the fruit and not the practice.
So even when you know or think you have to see thinking
or knowing, if you like it, say lightning.
Third, we have ED.
BT, I talked about, but here at BT is a little bit different meaning.
The meaning here is some kind of very visceral sort of ratcher.
Like we've ever seen these religious people who go like this.
I saw a documentary of people actually, you know,
felt like that was God or something.
I'm like, hey, practice meditation if you think
it's going to get the same thing.
They don't need God.
It's not really God.
But it feels like it, it feels often very, very external
like you're not doing it.
You're not causing yourself to shake.
So you can also get all sorts of ideas
about what it might be.
But it's BT, it's ratcher.
The mind gets into a consecrated state.
You are subtly, the mind is subtly encouraging it.
And so there you have to say shake, shake your swing.
And one of the teachers that is listening to you
is that sometimes it won't stop and you have to tell it to stop.
You have to say stop.
He said, because the defilements are not in the sense.
He said, give me what do you think is not in the trickie
or defilements of mind and trick ourselves.
Oh, I'm not doing this.
This is great.
Stop, tell it to stop.
Sometimes the note begins another.
You have a little bit of a trick.
Because there's nothing wrong with it.
It's not a bad feeling.
When I first started, I was like, oh, no more pain.
This is great.
No, you're not learning anything.
You're not going to gain anything from that.
So it's not bad.
That's just a wasted time.
That's all.
So these are the things just to keep in mind that you're not
putting your time to best fuse.
To be mindful not angry or upset.
It's not a bad thing.
Just mindful and let it go.
Other kinds of rats really might feel goosebumps.
You might feel energy.
Some people feel it's exhilarating energy going through their body.
Some people start crying.
And this is raptured.
But they're not unhappy.
They're just crying for hours.
Some people laughing can be yawning apparently light.
Light in the sense of feeling light.
You can feel very light.
Some people apparently actually levitate.
But I'd say feeling like you're levitating is probably
more common.
You probably heard these myths about people levitating
apparently it's possible.
So believe it or not.
I don't expect you to believe in all of you.
But apparently you can actually levitate.
There was one story that I have said, you know,
I was actually levitating.
And it teaches that I am going to take this pencil.
And when you feel yourself levitating,
make a mark on the ceiling with the pen and the pencil.
And he did it, apparently.
This is a story I've heard.
He actually did make a mark on the ceiling.
But believe it if you will.
But you'll feel light often.
Just say feeling, feeling, or light.
Again, it's not bad.
It's just an experience.
Don't get sidetracked.
Don't get distracted by it.
That's not a problem.
Just take it as an object of light on this.
And we have then happiness.
So we've talked to many of you about this happiness.
It's a good sign.
But not a part of the practice.
It's not practice.
Just be happy.
But to say to yourself, happy.
Otherwise the horizon is liking.
And then you're upset when it's gone.
Trying to get it back.
And only satisfied when it's there.
Calm is another one.
Calm, not calm, but quiet.
Quiet is a common one.
Well, for some people, they'll feel very quiet.
And it's a common problem that we often hear from people
who are practicing for a long time in another tradition.
They'll say, well, I'm feeling very calm.
And there's nothing to note.
I'm sorry, I'm feeling very quiet.
It's so quiet.
There's nothing.
And so then you would note it's positive.
It's an actual state of mind.
And you would not quiet.
Quiet, quiet, calm.
And just get quiet.
You can also say nothing.
And I say, you don't have the quietest of it.
So it's easy to get a side-tracked by that.
Because you feel like there's nothing to note.
It's just quiet.
And you should not quiet by ever even knowing that it's quiet.
It's just a state that arises and ceases.
It'll be a kind.
You might feel very equanimous.
Some people feel just completely neutral about everything.
You'll start to feel neutral about things that you'd normally
be reacting to.
Meditators often describe before they would be very reactionary
to pain or to thoughts or to sound.
Maybe the person next to you, they're a lot of me big.
And you're no longer angry about it.
And you can get excited about that.
You can get attached to that as well.
If you feel calm, just note that as well.
Make sure you don't sit there slowly.
I'm such a good meditator in the community.
Not reacting.
And then you're not a good meditator anymore.
Just something to keep in mind.
It doesn't make you bad or even.
It's just something you have to remind yourself of.
And then we have, from many weeks,
to at six already.
And then the next ones relate to qualities that we're trying to develop.
Confidence.
Confidence is a very common one.
At certain points, at a certain point in the meditation,
probably around where many of you are now or have just come from,
you're going to feel very confident in the practice.
There comes a time where you feel like, wow,
this is such a good thing.
Practice.
I don't know.
Maybe some of you are doubting as well.
So don't be discouraged if you don't have this.
But it can happen.
Sometimes meditators will feel like they want more day.
I'll be sitting there and think, I just have to dedicate my life
to this.
And I'll start to think about, I'm going to go home
and I'm going to ordain and just give up the world.
Another common one is, you'll think about family.
I have to get my, someone said the day,
I want to get my parents to practice this.
And these are good thoughts, but you get sidetracked
as you get so confident and so appreciative of it
that you just start to think about.
I'm going to go home and I'm going to teach all my friends
and relatives out of practice and tell them all about it
and they'll all love it.
And those of you that experienced with courses before it
isn't always quite so easy.
But regardless, it's a real distraction, confidence.
If you become confident in the practice,
it's a good sign.
It's a good sign that you're confident in things
that are beneficial to you, but just make sure
you're not taking so hard on the field.
Another one is energy.
You might feel very energetic.
As you practice, you should sort of throw off this burden
of fatigue and drowsiness.
And over time, you start to feel a grain of other energy.
And that can be a distraction simply because you start
to appreciate it and you feel like, oh, so much energy
and get distracted by it.
So just make sure you're noting even these good things.
And the right one is mindfulness.
So even mindfulness can do this to you.
It's a common thing for a meditator who finally
feels themselves able to be mindful,
even effortlessly mindful.
Maybe suddenly quite happy about that.
Excited about that.
Boy, I cannot everything.
You know, I'm just so mindful.
Some people even think they're enlightened
when they get to this point and they think, oh, I'm there.
And I figure it out.
Look at me doing this.
I'm going to be doing this.
So mindfulness, even mindfulness can get you on track.
Anything good because it starts to get better.
Basically.
And as it starts to get better, you start to get complacent
and finally something I can just relax with.
It's not going to go away.
It's not going to be a danger if you keep being mindful.
It's actually going to continue to get better.
That's the whole point is it'll stop getting better if you stop
being mindful.
It'll stop becoming more clear.
You're mind more stop.
Purify.
Purification doesn't have it.
And finally, the last one is kind of a bit different.
It's called minkanti.
Minkanti is sort of translated as desire.
And I guess included here because it relates to all the other ones.
So it's kind of distinct from them.
You couldn't say you can become a light.
You can like other things like senses.
This is a common thing.
You'll see nature around you.
You just like it.
Or you hear the sound of nature.
You just like it.
Or you taste the food that you just saw this food.
I never appreciated food so much before.
And hungry all night.
It's pretty so good.
So minkanti can arise there.
But it also says that minkanti is really
related to the other mind.
Minkanti is what causes a problem with everything else.
All of these good things arise.
And it's because you get excited.
Because you're attracted to them.
Because you like them.
That you get sidetracked.
So it's important that you notice while liking.
Even liking of the good things.
Because that's what initiates.
It's kind of a subtle liking that initiates the attachment
and the diversion.
And then it's minkanti on a dozen of you.
Knowledge and vision, purification by knowledge and vision
of what is and is not the path.
But so it goes deeper than just not knowing.
And then just knowing that bad things are not the path.
It's also a realization that even happy and pleasant
experiences are not the path.
It's an understanding that actually out of everything
that I've experienced mindfulness is the path.
That's what I'm hoping that you can realize here.
I'm hoping that you can get from this.
I believe it truly.
And I see that some of you are starting to see.
Hopefully all of you are starting to see this truth as well
that mindfulness is truly useful all the time.
And that's when the path really begins.
At that point, you enter into the path of practice.
Up until that point, you're kind of on the path, off the path.
And you're just learning when is the path
and many ways very hesitant.
And so at this point, your foundation is considered to be secure.
And there's not so much guidance that in the beginning,
we need a lot of guidance.
Sometimes turning you very easily on the other way.
After this point, it's just a matter of adjusting,
making sure you don't get off track.
And then you slide away.
So that said in a description of the foundation.
And I'm cutting it in half there.
So it's in the foundation.
Next time we talk, I'll talk to you about something else
that I'll see.
But I'll talk eventually about the rest of the path.
In reef, I don't want to go into too much detail
because otherwise you're going to head
to yourself and start expecting, thinking about the future.
But I'm going to try to lay on a map.
We will try to talk about where we're going, right?
Why are we doing this?
Is this really what I want to do with my life?
Let's give you an idea of hopefully,
providing some sort of calm and reassurance
that this is a beneficial and awesome and helpful.
Just more and more things that help to provide encouragement,
I think, all of you are doing quite well in your practice.
It's great to see so much vigilance and sort of dedication
to the practice.
So I appreciate all of you very much.
Thank you for coming.
I hope that you do gain truth and peace and happiness.
I'm pretty sorry.
I've been talking a lot today, and I'm going to stop there.
I wish for you all to have a good day.
And we'll talk again tomorrow.
Thank you.
Thank you so much.
