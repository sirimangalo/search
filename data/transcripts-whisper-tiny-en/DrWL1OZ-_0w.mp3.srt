1
00:00:00,000 --> 00:00:07,760
Does the might have a gender or is it only a property of the body?

2
00:00:07,760 --> 00:00:09,720
Gender is only a property of the body.

3
00:00:09,720 --> 00:00:10,720
Good question, really.

4
00:00:10,720 --> 00:00:18,480
I mean, people don't realize that, but yeah, gender actually has a...in Buddhism, it's

5
00:00:18,480 --> 00:00:27,360
a physical, it's called itindria and puri-syndria, I think, puri-syndria, I think is the

6
00:00:27,360 --> 00:00:28,840
other one.

7
00:00:28,840 --> 00:00:38,120
So, females have physical, female, faculty, males have the male, and I suppose you could

8
00:00:38,120 --> 00:00:45,200
conjecture that some people have both, or neither, they don't really know how it works.

9
00:00:45,200 --> 00:00:52,920
But the important point is that, yes, it's only physical.

10
00:00:52,920 --> 00:00:56,120
There's no mental aspect to gender.

11
00:00:56,120 --> 00:01:03,120
Except as to, first of all, A, how, first of all, how the physical affects the mental,

12
00:01:03,120 --> 00:01:12,000
because the female physical property, physical faculty, produces certain chemicals, and

13
00:01:12,000 --> 00:01:18,160
the male faculty produces different chemicals, and those chemicals affect the mind differently.

14
00:01:18,160 --> 00:01:27,640
And to some extent, I think, I don't really, I'm a biologist, but they affect the mind

15
00:01:27,640 --> 00:01:33,160
in different ways, and so, condition the person's mind in different ways.

16
00:01:33,160 --> 00:01:39,640
This is why you would expect that females exhibit certain characteristics, whereas males

17
00:01:39,640 --> 00:01:47,640
exhibit certain different characteristics in general, because of the influence of the different

18
00:01:47,640 --> 00:01:49,960
chemical, I would say.

19
00:01:49,960 --> 00:01:53,800
Among other things, of course, the physical characteristics, differences between the female

20
00:01:53,800 --> 00:02:01,000
body and the male body, will also affect one's behavior indirectly through conditioning.

21
00:02:01,000 --> 00:02:12,480
And then, on top of it, you have culture and society that highly specializes in gender

22
00:02:12,480 --> 00:02:23,960
roles and gender expectations and so on, gender the way we look at gender.

23
00:02:23,960 --> 00:02:49,800
And all of that, of course, has a profound impact on the mind.

