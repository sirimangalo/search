1
00:00:00,000 --> 00:00:03,080
Well, and welcome back to Ask a Monk.

2
00:00:03,080 --> 00:00:08,880
Now I'd like to answer a question about pain in meditation.

3
00:00:08,880 --> 00:00:17,240
When you feel a great amount of pain during your meditation, is it necessary to sit still

4
00:00:17,240 --> 00:00:20,960
and ensure that you never move if a person moves?

5
00:00:20,960 --> 00:00:22,680
Is there something wrong with that?

6
00:00:22,680 --> 00:00:27,920
Or is the point of meditation to be able to not move?

7
00:00:27,920 --> 00:00:31,200
Is it wrong practice if you move your body?

8
00:00:31,200 --> 00:00:35,320
And this is a very simple question with a very simple answer, but it actually has some profound

9
00:00:35,320 --> 00:00:36,320
implications.

10
00:00:36,320 --> 00:00:41,360
So I'm going to take a little bit of time to answer it in some detail.

11
00:00:41,360 --> 00:00:48,800
The simple answer first is, is yes indeed, you can move, there's nothing intrinsically wrong

12
00:00:48,800 --> 00:00:49,800
with moving your body.

13
00:00:49,800 --> 00:00:51,640
You know, we move it all the time.

14
00:00:51,640 --> 00:00:57,480
The problem here, of course, that we have to understand is that moving based on pain creates

15
00:00:57,480 --> 00:00:59,640
a version towards the pain.

16
00:00:59,640 --> 00:01:03,360
Now that's very all very well in theory.

17
00:01:03,360 --> 00:01:09,080
You know, you shouldn't move because if you move, you're moving based on a version.

18
00:01:09,080 --> 00:01:15,120
But it can actually go the other way, where you crush your version to the pain and you

19
00:01:15,120 --> 00:01:17,080
force yourself to sit.

20
00:01:17,080 --> 00:01:25,280
In fact, creating more attention and more stress because there's the forcing, you know,

21
00:01:25,280 --> 00:01:33,960
the not complying with nature and in fact, sometimes nature dictates that it's necessary

22
00:01:33,960 --> 00:01:35,840
to move.

23
00:01:35,840 --> 00:01:39,720
The appropriate reaction, the physical reaction, is to move.

24
00:01:39,720 --> 00:01:43,160
Once you get on in the meditation, this will change.

25
00:01:43,160 --> 00:01:45,240
First of all, there will be less pain.

26
00:01:45,240 --> 00:01:50,720
Second of all, it will bring less stress and tension and less of a desire to move.

27
00:01:50,720 --> 00:01:55,440
The pain will become just another sensation and the mind won't say something's wrong or feel

28
00:01:55,440 --> 00:01:56,440
that something's wrong.

29
00:01:56,440 --> 00:02:00,960
It will just see it as a sensation and be content with it and not upset by it.

30
00:02:00,960 --> 00:02:02,960
So you won't need to move.

31
00:02:02,960 --> 00:02:05,440
Now in the beginning, this isn't the case.

32
00:02:05,440 --> 00:02:10,560
In the beginning, it brings more stress and tension and really great problems.

33
00:02:10,560 --> 00:02:18,880
So what we should do is be mindful of it and when it first comes, we should certainly

34
00:02:18,880 --> 00:02:25,160
try to be as mindful of it as possible and not move without immediately moving our bodies

35
00:02:25,160 --> 00:02:29,600
because this is a great teacher for us and this is what I'd like to get into.

36
00:02:29,600 --> 00:02:38,280
But the basic technique is to say to ourselves, pain, pain, just quietly, not out loud

37
00:02:38,280 --> 00:02:43,240
but in the mind, just focusing on the pain and reminding ourselves, hey, this is just pain.

38
00:02:43,240 --> 00:02:44,800
There's nothing wrong with it.

39
00:02:44,800 --> 00:02:50,760
It's not a negative experience, there's nothing intrinsically bad about pain.

40
00:02:50,760 --> 00:02:54,920
In terms of the universe, really doesn't care whether you're in pain or not and so it's

41
00:02:54,920 --> 00:02:58,880
not something that is intrinsically negative.

42
00:02:58,880 --> 00:03:03,720
What is negative is your negative reactions to it, your anger, your frustration, your thinking

43
00:03:03,720 --> 00:03:07,920
that it's bad, that it's a problem that you have to do something about it.

44
00:03:07,920 --> 00:03:11,560
So when you see it simply as pain, all of that goes away.

45
00:03:11,560 --> 00:03:14,080
This is really the truth, it's something that you should try.

46
00:03:14,080 --> 00:03:17,760
You know, these videos are not meant to just be, oh, that sounds nice, I agree with

47
00:03:17,760 --> 00:03:21,800
that and then you go home or go back to Facebook or whatever.

48
00:03:21,800 --> 00:03:23,600
These are something you should put into practice.

49
00:03:23,600 --> 00:03:29,880
So when I say this, try it and find out for yourself because it really has wonderful

50
00:03:29,880 --> 00:03:31,440
consequences.

51
00:03:31,440 --> 00:03:36,200
Any kind of pain that you come up with in your life suddenly, it's no longer a great problem

52
00:03:36,200 --> 00:03:38,480
or a great difficulty.

53
00:03:38,480 --> 00:03:43,640
It's something that you can deal with with mindfulness and clarity and wisdom and not

54
00:03:43,640 --> 00:03:46,240
have to suffer from.

55
00:03:46,240 --> 00:03:50,280
Once that becomes too much, in the beginning that's very difficult to do and you're not

56
00:03:50,280 --> 00:03:54,680
really mindful, you're mostly at pain, pain and you're really angry about it anyway.

57
00:03:54,680 --> 00:04:00,400
So saying pain isn't really acknowledging, it's reinforcing the anger and the hate.

58
00:04:00,400 --> 00:04:05,200
So when it gets to that point where it's overwhelming and you feel like you have to move,

59
00:04:05,200 --> 00:04:06,280
then just move mindfully.

60
00:04:06,280 --> 00:04:11,240
You can lift your leg when moving, you're in, moving, say to yourself, moving, placing

61
00:04:11,240 --> 00:04:17,480
or grasping, lifting, moving, placing, or you can just move your leg without, you know,

62
00:04:17,480 --> 00:04:23,880
if you're sitting in this position, just lifting, moving, placing, just do it mindfully

63
00:04:23,880 --> 00:04:28,280
and it becomes a meditation because eventually our whole life should become meditation.

64
00:04:28,280 --> 00:04:32,240
We should try to be as mindful as we can in our daily lives.

65
00:04:32,240 --> 00:04:35,240
When we eat, we're mindful, when we drink, we're mindful.

66
00:04:35,240 --> 00:04:36,800
Everything we do, we know what we're doing.

67
00:04:36,800 --> 00:04:41,680
When we move our hands, this should be moving, moving or shaking.

68
00:04:41,680 --> 00:04:44,880
When we talk, we should know that we're talking, what is this feeling of my lips moving?

69
00:04:44,880 --> 00:04:48,400
And even to that extent, you can be mindful.

70
00:04:48,400 --> 00:04:52,520
When you're listening, you can say hearing, hearing and acknowledging the sound and you'll

71
00:04:52,520 --> 00:04:56,680
find that you really understand the meaning much better and you get less caught up in your

72
00:04:56,680 --> 00:04:59,720
own emotions and judgments and so on.

73
00:04:59,720 --> 00:05:02,920
So this is the basic technique.

74
00:05:02,920 --> 00:05:09,720
Now the theory behind pain and how it's such a good teacher is that eventually pain

75
00:05:09,720 --> 00:05:17,720
is one of the most, the best teachers that we have because pain is something that we don't

76
00:05:17,720 --> 00:05:18,720
like.

77
00:05:18,720 --> 00:05:25,560
It's our aversion, it's our problem, you know, it's if it weren't for pain, if it weren't

78
00:05:25,560 --> 00:05:31,360
for things like pain, then we would never need to practice meditation, we would never

79
00:05:31,360 --> 00:05:33,680
think to come and practice meditation.

80
00:05:33,680 --> 00:05:34,680
Why wouldn't we?

81
00:05:34,680 --> 00:05:38,680
It's the point of wasting all this time when we can be out enjoying life.

82
00:05:38,680 --> 00:05:42,720
But the problem is that in our life, it's not all fun and games and in fact our clinging

83
00:05:42,720 --> 00:05:47,720
leads us to have suffering because we don't like certain things where we're not content,

84
00:05:47,720 --> 00:05:52,280
we're thinking about the good things and it's a bad thing, it makes us angry because

85
00:05:52,280 --> 00:05:54,680
it's not what we want and so on.

86
00:05:54,680 --> 00:06:03,600
So what we're basically doing here is learning how to live with it and how to see it for

87
00:06:03,600 --> 00:06:07,960
what it is and see it simply as another experience, not as a bad thing and not seeing other

88
00:06:07,960 --> 00:06:10,200
things as good things.

89
00:06:10,200 --> 00:06:16,480
And so the theory here is that most people understand, it has to do with how people understand

90
00:06:16,480 --> 00:06:18,840
suffering.

91
00:06:18,840 --> 00:06:26,240
An ordinary person understands suffering to be, and suffering here is of course the big elephant

92
00:06:26,240 --> 00:06:32,560
in the room, it's what we as Buddhists deal with, it's what we talk about, what we teach

93
00:06:32,560 --> 00:06:36,760
and yet always have a very difficult, a great amount of difficulty talking about.

94
00:06:36,760 --> 00:06:41,760
But this is because most people understand suffering to be the pain, suffering to be

95
00:06:41,760 --> 00:06:48,240
a feeling, this is what is a dukkawedana, dukkawedana means dukkawedana is the feeling.

96
00:06:48,240 --> 00:06:52,600
So we understand dukkawedana to be a feeling that you get, you get this feeling that's

97
00:06:52,600 --> 00:06:56,800
dukkawedana suffering, you get that feeling that's suffering, you get that's another feeling

98
00:06:56,800 --> 00:06:58,080
and it's not suffering.

99
00:06:58,080 --> 00:07:06,360
So we categorize our experiences, this one is suffering, this one is not suffering and

100
00:07:06,360 --> 00:07:16,200
as a result our means of overcoming suffering, of being free from suffering is to find a

101
00:07:16,200 --> 00:07:21,240
way to only have these experiences and to never have these experiences, this doesn't

102
00:07:21,240 --> 00:07:25,680
that sound normal, that sounds yeah that's how we get rid of suffering, that's because

103
00:07:25,680 --> 00:07:29,440
that's what we're told, that's what we're taught and it's totally false, it's totally

104
00:07:29,440 --> 00:07:35,120
wrong, it's the wrong way to deal with suffering, but this is how we do, why is it wrong?

105
00:07:35,120 --> 00:07:40,680
Okay so you have pain in the leg, you move your leg and then you have pain in the back

106
00:07:40,680 --> 00:07:44,400
and so then you have to stretch your back and then you have pain in your head so you

107
00:07:44,400 --> 00:07:49,360
have to take a pill for the pain and then you have pain here pain there and you have to

108
00:07:49,360 --> 00:07:54,400
take another pill or you have to get an operation or so on and so on and all this time

109
00:07:54,400 --> 00:07:58,680
you're developing more and more aversion to the pain until eventually it becomes totally

110
00:07:58,680 --> 00:08:04,000
overwhelming, when a person takes a pill for a headache really what they're doing is

111
00:08:04,000 --> 00:08:09,000
reaffirming their aversion towards the pain and as a result it's going to be worse next time

112
00:08:09,000 --> 00:08:14,760
it's going to, yeah it's going to be worse and worse and worse, not because the pain changes

113
00:08:14,760 --> 00:08:19,640
but because our attitude towards it is reaffirmed as being this is bad, this is bad, this

114
00:08:19,640 --> 00:08:25,360
is bad bad bad, until it becomes totally unbearable, these things are not static, craving

115
00:08:25,360 --> 00:08:32,960
is not static, it builds, it turns into a habit, it changes our whole vibration in that

116
00:08:32,960 --> 00:08:39,600
direction and it changes the world around us as well, anger does as well, aversion does

117
00:08:39,600 --> 00:08:46,920
as well, we become totally averse to these things and unable to bear them.

118
00:08:46,920 --> 00:08:54,120
So what would we come to see and the reason why we come to practice meditation is the

119
00:08:54,120 --> 00:09:00,320
second type of suffering, the Buddha said it dukasabawa, somehow means a reality of suffering

120
00:09:00,320 --> 00:09:07,440
that suffering is a fact, you can't escape it, it's reality, it's a part of reality and

121
00:09:07,440 --> 00:09:12,000
this is like getting old is a part of reality, getting sick is a part of it, dying is a

122
00:09:12,000 --> 00:09:25,240
part of reality, so these methods of overcoming suffering or these ways of avoiding the unpleasant

123
00:09:25,240 --> 00:09:33,640
part of reality are temporary, are ineffective because it's there, it's a part of life,

124
00:09:33,640 --> 00:09:37,840
it's not going to go away, you're not going to be to remove it, you know, you're not

125
00:09:37,840 --> 00:09:40,600
going to get rid of sickness, you're not going to get rid of old age, you're not going

126
00:09:40,600 --> 00:09:46,680
to get rid of death, these things are going to come to you and it's this realization

127
00:09:46,680 --> 00:09:50,920
when we have a situation that we can't overcome, people who have migraine headaches,

128
00:09:50,920 --> 00:09:56,080
they've taken every pill and nothing works, people who are mourning a person who passed

129
00:09:56,080 --> 00:10:01,720
away, they can drink or whatever, but nothing works and they're still sad, they're still

130
00:10:01,720 --> 00:10:04,520
thinking about the person, nothing works.

131
00:10:04,520 --> 00:10:09,360
When a person gets to this point, this is most often when they begin to practice meditation,

132
00:10:09,360 --> 00:10:13,000
when they get to the point where they realize they're totally on the wrong path, when

133
00:10:13,000 --> 00:10:20,760
they realize that this reaffirmation of greed, anger, delusion is totally dragging them down

134
00:10:20,760 --> 00:10:25,640
in the wrong path and they're not helping, they're reactions towards suffering, their

135
00:10:25,640 --> 00:10:29,000
ways of dealing with suffering are ineffective.

136
00:10:29,000 --> 00:10:33,520
This is the second type of suffering, it's a very important realization because that's

137
00:10:33,520 --> 00:10:37,280
what leads us, that's what leads to the conclusion that we have to do something, people

138
00:10:37,280 --> 00:10:40,520
say why do you meditate, what's the point wasting your time?

139
00:10:40,520 --> 00:10:44,680
Well, those people have not yet realized, this is what they haven't yet realized,

140
00:10:44,680 --> 00:10:48,760
for a person who has realized that you can't really escape it, you know, you can say

141
00:10:48,760 --> 00:10:55,560
I go out and party and happy, I've tried it and I have no, I'm not able to escape suffering

142
00:10:55,560 --> 00:11:00,800
in the way you pretend or you think you're able to, my experience is that suffering is

143
00:11:00,800 --> 00:11:08,240
a part of life, it's something that we either learn to deal with or we suffer more and

144
00:11:08,240 --> 00:11:12,480
more and more, it gets worse and worse and worse, so we find a way, so this is when we

145
00:11:12,480 --> 00:11:15,160
come to practice meditation.

146
00:11:15,160 --> 00:11:20,280
When we come to practice meditation, we realize the third truth, the third type of suffering,

147
00:11:20,280 --> 00:11:27,280
the third way of understanding this word suffering and this is that suffering is inherent

148
00:11:27,280 --> 00:11:35,480
in all things, just as heat is inherent in fire, there's no fire that is not hot, fire

149
00:11:35,480 --> 00:11:43,960
is hot, that's, well, in a conventional sense, fire is hot, by the same token, all things

150
00:11:43,960 --> 00:11:50,480
in the world, anything that arises is suffering and what we mean by this is just as fire

151
00:11:50,480 --> 00:11:57,560
is hot, if you hold on to it and burns you, when something arises and you cling to it,

152
00:11:57,560 --> 00:12:01,680
it makes you suffer, why this happens basically what I've been saying, when it's something

153
00:12:01,680 --> 00:12:06,720
good, you cling to it as good and then it disappears, when it disappears, you're unhappy,

154
00:12:06,720 --> 00:12:10,840
you're looking for it, because there's still the wanting, there's still the clinging in

155
00:12:10,840 --> 00:12:15,640
your mind and in fact you cultivate it and develop it and it comes up again and again,

156
00:12:15,640 --> 00:12:21,320
I want this, I want this, I like this, I like this, when it's gone, the craving doesn't

157
00:12:21,320 --> 00:12:26,920
go away, the experience is gone and who knows when it'll come back, if it's an unpleasant

158
00:12:26,920 --> 00:12:32,120
experience, the craving, a version go away and pushing it away with this method of that

159
00:12:32,120 --> 00:12:34,920
method makes it worse and so on.

160
00:12:34,920 --> 00:12:40,440
When we practice meditation, we come to realize that no, these things are not really going

161
00:12:40,440 --> 00:12:43,680
to make me happy, clinging to good things, it's not going to make me happy, why?

162
00:12:43,680 --> 00:12:48,840
Because they come and they go, they arise and they cease, my happiness depends on that, how

163
00:12:48,840 --> 00:12:54,440
can I ever hope to be happy, if say my happiness depends on this person, you come to realize

164
00:12:54,440 --> 00:12:58,640
that that person is only experiences, you hear something and that's that person, you see

165
00:12:58,640 --> 00:13:03,440
something and that person, you think of something and that's that person, but all of those

166
00:13:03,440 --> 00:13:08,360
experiences are impermanent and they're uncertain, you don't know whether they're going

167
00:13:08,360 --> 00:13:12,640
to say something good to you, maybe someone you love says something bad to you and you suffer

168
00:13:12,640 --> 00:13:18,120
from it and the realization that it's not sure, there's no person there that's wonderful,

169
00:13:18,120 --> 00:13:22,160
there's a bunch of experiences waiting to happen and those experiences will not all be

170
00:13:22,160 --> 00:13:26,880
pleasant when the person dies, if they're a loved one, then that will be a very unpleasant

171
00:13:26,880 --> 00:13:28,080
thing.

172
00:13:28,080 --> 00:13:32,640
This realization we come to through our meditation practice, even just watching our own

173
00:13:32,640 --> 00:13:37,080
body, watching the rising and falling and the stomach, coming to see that every single

174
00:13:37,080 --> 00:13:44,400
thing that arises has to pass away, it comes and it goes, coming to see that we do it even

175
00:13:44,400 --> 00:13:49,480
with our stomach, rising and falling, oh now it's smooth and then we like it and then suddenly

176
00:13:49,480 --> 00:13:54,040
it changes and it's rough and uncomfortable and we can't really find it and we're angry

177
00:13:54,040 --> 00:13:59,520
and upset, well even this very stupid simple object of the stomach can teach us everything

178
00:13:59,520 --> 00:14:06,200
we need to know about reality because it shows us our mind, it shows us the way we project

179
00:14:06,200 --> 00:14:13,000
and we relate to things and make more out of things than they actually are, through the

180
00:14:13,000 --> 00:14:18,920
meditation practice we'll come to break down people, break down things and experiences

181
00:14:18,920 --> 00:14:26,400
into individual phenomenon that arise and sees and come and go and so as a result we see

182
00:14:26,400 --> 00:14:33,120
that none of them are pleasant or none of them are satisfying and this is what it means

183
00:14:33,120 --> 00:14:38,080
by suffering that our happiness should never depend on these things, it depends on a person

184
00:14:38,080 --> 00:14:44,600
that person doesn't exist, it's just an experiences coming to see that our relationship

185
00:14:44,600 --> 00:14:48,760
with the person is just through experiences which are never going to satisfy us, they're

186
00:14:48,760 --> 00:14:54,480
only going to, they can possibly do is bring more clinging and craving and dissatisfaction

187
00:14:54,480 --> 00:15:00,280
and the need for more and so on and we will never truly be at peace with ourselves, people

188
00:15:00,280 --> 00:15:04,400
think that they will but you can look at those people and see that they're not really

189
00:15:04,400 --> 00:15:09,520
truly at peace with themselves and through the meditation we come to see this and so we

190
00:15:09,520 --> 00:15:14,960
come to gain truth peace for ourselves, as you see these things, you know you'll see things

191
00:15:14,960 --> 00:15:19,400
arising even just the stomach coming and going and you'll see the clinging and how useless

192
00:15:19,400 --> 00:15:23,800
it is to cling to it to be this way or that way because it's changing, you see it coming

193
00:15:23,800 --> 00:15:27,920
and going and that it's not under your control, you see impermanence suffering on self these

194
00:15:27,920 --> 00:15:34,560
three characteristics, as this goes on the fourth type of suffering or the fourth way of

195
00:15:34,560 --> 00:15:38,600
understanding suffering comes to you and that is as the truth and this is what we call

196
00:15:38,600 --> 00:15:43,080
you know you hear about the noble truth, the noble truth of suffering and this is the

197
00:15:43,080 --> 00:15:48,960
realization that we're hoping for, this realization it's not, it's not a good, it's

198
00:15:48,960 --> 00:15:55,640
not satisfying, these are not going to make me happy, this realization is the fourth

199
00:15:55,640 --> 00:16:00,880
type of way of understanding suffering is really the consummation of the Buddha's teaching,

200
00:16:00,880 --> 00:16:04,880
it's just you know starting to see things, things coming and going on this isn't satisfying

201
00:16:04,880 --> 00:16:09,440
that is and you just realize at some point this isn't the way to find happiness, it's

202
00:16:09,440 --> 00:16:12,760
not intellectual of course, I'm just you know putting words to it but it's suddenly

203
00:16:12,760 --> 00:16:19,360
a boom, the mind just says no and the mind gives up, the mind lets go and at that point

204
00:16:19,360 --> 00:16:27,720
there's freedom, the mind is released and you enter into this state of of total freedom

205
00:16:27,720 --> 00:16:31,200
which really there's no experience at all, there's no seeing, hearing, smelling, tasting

206
00:16:31,200 --> 00:16:36,000
feeling or even thinking, they don't even aware, it's like kind of like falling asleep

207
00:16:36,000 --> 00:16:40,960
but it's total peace and when you come back you know that you really wow I just totally

208
00:16:40,960 --> 00:16:49,200
let go, you know there was no arising of anything at that point and this is you know

209
00:16:49,200 --> 00:16:53,880
we call Nirvana, there's realization and a person starts to realize this and looking

210
00:16:53,880 --> 00:16:58,600
around and seeing that their happiness can't come from anything outside of themselves

211
00:16:58,600 --> 00:17:02,560
and so they cultivate this more and more and more and start to see the truth more and

212
00:17:02,560 --> 00:17:08,320
more and more and eventually are able to become totally free from suffering.

213
00:17:08,320 --> 00:17:13,880
So but the point being that this realization, when you talk about Nirvana and so on and

214
00:17:13,880 --> 00:17:20,080
see for yourself, Buddha's teaching is to see for yourself but the point which I think

215
00:17:20,080 --> 00:17:24,240
everyone can understand is that we have to see this reality, we have to really grasp

216
00:17:24,240 --> 00:17:31,200
it that we're not going to find happiness in the things outside of ourselves and that

217
00:17:31,200 --> 00:17:39,520
these phenomena that arise, there's no good that can come from clinging even in terms

218
00:17:39,520 --> 00:17:45,960
of aversion, there's no good that comes from wanting to escape it or wanting to even force

219
00:17:45,960 --> 00:17:48,680
yourself to sit still in this case.

220
00:17:48,680 --> 00:17:53,040
So it's a very simple question with a very simple answer but here the profound implications

221
00:17:53,040 --> 00:17:57,200
and to do is suffering that help us to understand why it is that we might want to sit

222
00:17:57,200 --> 00:18:02,320
through it sometimes and if we can get that through our head then you know sometimes

223
00:18:02,320 --> 00:18:06,200
we can sit through it we say you know yeah I want to move but it's not really a big deal

224
00:18:06,200 --> 00:18:10,440
it's not going to kill me and I'm going to learn something here and as you do that you'll

225
00:18:10,440 --> 00:18:16,240
see how your mind works, you'll see the craving and the aversion and you see how the phenomenon

226
00:18:16,240 --> 00:18:20,680
works and how it arises and ceases and see that there's no good that comes from being

227
00:18:20,680 --> 00:18:24,920
angry about it or upset about it you don't feel happier it doesn't solve the problem

228
00:18:24,920 --> 00:18:29,280
it creates more immersion moving your foot doesn't solve the problem it's a temporary

229
00:18:29,280 --> 00:18:34,240
solution to kind of ease the pressure when it's too much for you but eventually it's

230
00:18:34,240 --> 00:18:38,560
you're just going to you know say why would I move my foot again I've moved my foot ten

231
00:18:38,560 --> 00:18:42,640
times already it didn't solve the problem and eventually you give that up and you say

232
00:18:42,640 --> 00:18:46,440
well I'll just sit through it that's what happens in meditation when you go to an intensive

233
00:18:46,440 --> 00:18:50,000
course in the beginning the pain here you say okay I can fix that and you put a pillow

234
00:18:50,000 --> 00:18:52,960
under here and then this one then you put a pillow under here and then here you put

235
00:18:52,960 --> 00:18:58,400
a pillow and here you put a pillow until eventually you just say it's not working it's

236
00:18:58,400 --> 00:19:03,800
not solving the problem and you give up and that's the realization that we're striving

237
00:19:03,800 --> 00:19:07,680
for you realize that more and more and more eventually you'll realize that that's the

238
00:19:07,680 --> 00:19:16,560
truth that that's the truth of reality and you let go and not cling so this is my discussion

239
00:19:16,560 --> 00:19:20,800
of the truth of suffering in the Buddha's teaching thanks for tuning in and I wish you

240
00:19:20,800 --> 00:19:26,000
all the best that you're all able to put this into practice and and try your best to

241
00:19:26,000 --> 00:19:32,200
become patient and be able to overcome your attachments and emotions and suffering and find

242
00:19:32,200 --> 00:19:36,200
true peace happiness and freedom of suffering so all the best

