1
00:00:00,000 --> 00:00:07,120
Hello, I'm Max, a volunteer for Siramongalo International.

2
00:00:07,120 --> 00:00:11,520
Currently, a group of volunteers are working to transcribe Bonti to Dhamma's videos into

3
00:00:11,520 --> 00:00:14,440
booklets in order to support meditators.

4
00:00:14,440 --> 00:00:19,000
To be suitable for reading, the transcripts must be edited from their raw form.

5
00:00:19,000 --> 00:00:23,080
We invite anyone with strong English language writing or copy editing skills to join

6
00:00:23,080 --> 00:00:25,760
us in editing these transcripts.

7
00:00:25,760 --> 00:00:31,240
If you're interested, you may email Volunteer Coordinator at Siramongalo.org or join

8
00:00:31,240 --> 00:00:34,000
our Discord server and leave a message there.

9
00:00:34,000 --> 00:00:36,160
The link is below.

10
00:00:36,160 --> 00:00:40,160
There are several other volunteer projects happening on Discord that you may also be interested

11
00:00:40,160 --> 00:00:41,800
in contributing to.

12
00:00:41,800 --> 00:00:43,280
Thank you.

13
00:00:43,280 --> 00:01:13,120
May we all find peace and happiness.

