WEBVTT

00:00.000 --> 00:07.000
Good evening, we're continuing on with our study of the Dhamapada.

00:07.000 --> 00:16.000
Today we look at verse 204, which reads as follows.

00:16.000 --> 00:23.000
Arogya paramalabha sent to Thi paramandhanam.

00:23.000 --> 00:37.000
We sasa paramaya nyati nibanam paramansukha, which means

00:37.000 --> 00:48.000
Arogya, freedom from sickness, paramalabha is the greatest gain.

00:48.000 --> 01:01.000
Santuti contentment is the highest wealth.

01:01.000 --> 01:09.000
We sasa paramaya nyati, which sasa means something like trust or confidence,

01:09.000 --> 01:15.000
is the greatest relative.

01:15.000 --> 01:22.000
And nibana is the highest happiness.

01:22.000 --> 01:32.000
So this was taught in regards to King Paseenidhi, the course of the kingdom.

01:32.000 --> 01:38.000
The story goes that he was overweight.

01:38.000 --> 01:49.000
Overweight is perhaps a problematic term because it requires a judgment to say that something is underweight or overweight.

01:49.000 --> 01:56.000
But in a conventional sense one might say he was and he said he was because he complained about it.

01:56.000 --> 02:04.000
But more than that he ate too much, regardless of his weight, he ate too much.

02:04.000 --> 02:08.000
And why can we say it was too much without saying it's a judgment?

02:08.000 --> 02:13.000
Because he complained about it being too much.

02:13.000 --> 02:20.000
Well, he didn't say actually, he came to the Buddha, he ate, they say he would eat.

02:20.000 --> 02:22.000
What is it?

02:22.000 --> 02:24.000
A bushel, a bushel of rice.

02:24.000 --> 02:33.000
I don't know how big bushels were with the actual measurement ones, but he had a lot of rice and curries and so on in proportion.

02:33.000 --> 02:36.000
Too much, he ate too much.

02:36.000 --> 02:38.000
And why it was too much?

02:38.000 --> 02:42.000
Because after he was done eating he would feel very tired.

02:42.000 --> 02:48.000
And so one day he ate and then went to the monastery and he couldn't even sit down because he knew if he sat down he'd fall asleep.

02:48.000 --> 02:53.000
So the Buddha was teaching and he started walking back and forth.

02:53.000 --> 02:58.000
And then what he really wanted to do was lie down and fall asleep, but he knew he couldn't.

02:58.000 --> 03:02.000
So he laid out, he sat down and tried his best to stay awake.

03:02.000 --> 03:08.000
And the Buddha looked at him and said, great king, are you not well?

03:08.000 --> 03:12.000
And he said, no, it's not that, I hate too much.

03:12.000 --> 03:17.000
I hate so much, I'm tired now.

03:17.000 --> 03:21.000
I can't stay awake and he said, I don't know what to do.

03:21.000 --> 03:27.000
And the Buddha said, oh, and he gave him a verse to recite.

03:27.000 --> 03:30.000
And it's actually a Dhammapada verse, 324.

03:30.000 --> 03:33.000
A way is a way, we'll get to it eventually.

03:33.000 --> 03:44.000
But it basically said, it was talking about the disadvantages of indolence, of laziness, of overeating.

03:44.000 --> 03:47.000
How such a person goes to suffering.

03:47.000 --> 03:53.000
And he had him, remember this verse, but Pazainadi was actually quite...

03:53.000 --> 03:59.000
Well, he was tired at the time, but we think perhaps he was a little bit dull as well.

03:59.000 --> 04:02.000
And he said he couldn't remember it.

04:02.000 --> 04:06.000
Probably it was more because he was tired, but anyway, the Buddha said to his nephew.

04:06.000 --> 04:09.000
He called his nephew, the King Pazainadi's nephew.

04:09.000 --> 04:15.000
He taught him the verse and he said, when Pazainadi is eating, just before he finished his eating,

04:15.000 --> 04:18.000
you recite him this verse and he'll stop eating.

04:18.000 --> 04:26.000
And you can keep cutting away at his food that way.

04:26.000 --> 04:28.000
And sure enough, it's a short story.

04:28.000 --> 04:39.000
Sure enough, they went away and his nephew Sudasana would recite this verse to Pazainadi

04:39.000 --> 04:42.000
and Pazainadi came back to the Buddha and he remarked upon this.

04:42.000 --> 04:44.000
He said, it's incredible, Venerable, sir.

04:44.000 --> 04:46.000
I'm a new man.

04:46.000 --> 04:54.000
I'm no longer drowsy or tired, but I'm also no longer cranky because that verse has sort of taught me

04:54.000 --> 04:59.000
any contentment in the mind.

04:59.000 --> 05:04.000
And so he talked about all the good things that it brought that he was kinder.

05:04.000 --> 05:12.000
And when he heard this verse, it made him want to give away rather than take for himself.

05:12.000 --> 05:15.000
And so he gave away lots of charity.

05:15.000 --> 05:18.000
They say, when he heard this verse, I guess he would give to Sudasana.

05:18.000 --> 05:24.000
I don't know. It says he gave out thousands of gold coins.

05:24.000 --> 05:30.000
And he talked about how he was no longer fighting with his relatives.

05:30.000 --> 05:35.000
His nephew, he was now very much indebted and grateful to his nephew.

05:35.000 --> 05:37.000
He just talked about generally.

05:37.000 --> 05:40.000
And most importantly, not eating so much, he was thinner.

05:40.000 --> 05:44.000
And he could go on hunts again with his head.

05:44.000 --> 05:48.000
It's a little bit eye opening to hear him tell the Buddha that he could hunt again.

05:48.000 --> 05:53.000
But I don't know exactly what that means because it was something about chasing horses.

05:53.000 --> 05:58.000
I don't think it was just about killing.

05:58.000 --> 06:03.000
And the Buddha said, yes, great king, it's true that these are great things that you've gained.

06:03.000 --> 06:13.000
And then he said, this not having a healthy body, he said, it's a great gain.

06:13.000 --> 06:15.000
Great gain.

06:15.000 --> 06:20.000
And being content with less is a great wealth.

06:20.000 --> 06:30.000
And having trustworthy relatives is a great thing, great relatives to have having this nephew, for example.

06:30.000 --> 06:33.000
And then he said, bat nibana is the highest happiness.

06:33.000 --> 06:38.000
And then he taught this verse basically.

06:38.000 --> 06:46.000
So there's a mixture here of conventional and ultimate sort of shallow and deep teachings.

06:46.000 --> 06:49.000
And it's not to be little shallow teachings.

06:49.000 --> 06:56.000
We should practically, conventionally, we should have what we might refer to as shallow teachings.

06:56.000 --> 07:01.000
Meaning they just meaning that they don't, in and of themselves, bring about enlightenment.

07:01.000 --> 07:07.000
They're not on the level of insight that's going to lead us to be free from suffering.

07:07.000 --> 07:16.000
But they are practically applicable and helpful, supportive of our practice.

07:16.000 --> 07:21.000
And the first one that comes from, of course, the story, this idea of overeating.

07:21.000 --> 07:23.000
Because it's something that you find in meditation.

07:23.000 --> 07:30.000
It's a concern of people who want to come and meditate that they're told they're only going to be eating in the morning.

07:30.000 --> 07:35.000
And that's scary, worrisome.

07:35.000 --> 07:40.000
But then you come and practice and you find that it's actually the easiest role to keep.

07:40.000 --> 07:47.000
But in fact, overeating becomes a bit of an issue because you find after you've eaten your drowsy.

07:47.000 --> 07:52.000
And it's an issue without such strict rules.

07:52.000 --> 07:55.000
It's an issue because I've been in places where they didn't have such strict rules.

07:55.000 --> 08:00.000
Where in the evening they would allow milk or even yogurt or whatever.

08:00.000 --> 08:10.000
And you'll find that by drinking or eating, really having solid, something solid, even a solid is milk.

08:10.000 --> 08:18.000
You find that it takes energy because your body is using energy to process it.

08:18.000 --> 08:20.000
Of course, it gives you some kind of energy.

08:20.000 --> 08:29.000
But if you're not using it, it just stores the energy as fat or whatever.

08:29.000 --> 08:33.000
And so it is a concern for meditators.

08:33.000 --> 08:38.000
I think less when you're staying here, but still something you have to be mindful of.

08:38.000 --> 08:42.000
Of course, more importantly, I think is the mindfulness of eating.

08:42.000 --> 08:46.000
You know, being taught as a verse every being reminded of this verse every day.

08:46.000 --> 08:50.000
It was not just about remembering.

08:50.000 --> 08:56.000
It was not just about realizing the disadvantages of eating too much.

08:56.000 --> 09:00.000
It's about being mindful and realizing when you've eaten enough.

09:00.000 --> 09:06.000
It's an important part of, it's a practical part of meditation that it teaches you when it's enough.

09:06.000 --> 09:08.000
Really in all things.

09:08.000 --> 09:11.000
But food is a really obvious example.

09:11.000 --> 09:22.000
You find it's very hard to overeat when you're mindful because you're aware, you're present and objectively observing.

09:22.000 --> 09:25.000
And you can feel and know when you've had enough.

09:25.000 --> 09:36.000
So deep and more than that becomes more difficult.

09:36.000 --> 09:44.000
As far as the verse goes, though, we can each of the four parts has an important lesson for us.

09:44.000 --> 09:46.000
And so we'll take them apart.

09:46.000 --> 09:50.000
That's really where the core of the lesson comes from for our purposes.

09:50.000 --> 09:59.000
The first part, Rogia, Rogia meaning sickness, illness, disease.

09:59.000 --> 10:01.000
In Buddhism, there are four types of disease.

10:01.000 --> 10:08.000
And so we have disease in a conventional sense and disease in an ultimate sense.

10:08.000 --> 10:11.000
But the categorization is that there are four.

10:11.000 --> 10:16.000
The first type of sickness comes from the environment.

10:16.000 --> 10:26.000
You could say things like the other weather, but airborne bacteria and so on.

10:26.000 --> 10:32.000
Viruses, heat and cold can affect us.

10:32.000 --> 10:37.000
Radiation, even the sun.

10:37.000 --> 10:49.000
And the second is from food, where you ingest food and drink and make you sick.

10:49.000 --> 10:59.000
The third is kamma, which means deeds, but here it is specifically referring to bad things you've done in the past.

10:59.000 --> 11:01.000
Past lives can make you sick.

11:01.000 --> 11:12.000
Even probably this is more referring to past life karma that has made you born with an illness or disability or so on.

11:12.000 --> 11:17.000
And finally sicknesses that come from the mind.

11:17.000 --> 11:19.000
The first two are physical.

11:19.000 --> 11:25.000
The first two are things where they don't have so much control over.

11:25.000 --> 11:28.000
But practically speaking, we can find the lesson in all of these.

11:28.000 --> 11:34.000
It is practically speaking important to remember that our environment impacts us.

11:34.000 --> 11:40.000
And being mindful of what we engage in, it's important to be mindful about food.

11:40.000 --> 11:42.000
It is important to have a healthy body.

11:42.000 --> 11:47.000
Of course, it's important equally not to obsess over the body.

11:47.000 --> 11:51.000
But you can put some weight on these things and say,

11:51.000 --> 11:56.000
yes, I want to be healthy, of course, because a healthy body is useful for you.

11:56.000 --> 12:02.000
A much more important from a Buddhist perspective is the deeper sort of ultimate level of sickness,

12:02.000 --> 12:05.000
which comes from the mind.

12:05.000 --> 12:09.000
So karma, of course, comes from the mind that's in past lives.

12:09.000 --> 12:10.000
We've done bad things.

12:10.000 --> 12:12.000
It's a good lesson to us.

12:12.000 --> 12:17.000
And a helpful reminder to us of the dangers of doing bad deeds.

12:17.000 --> 12:23.000
That it corrupts your mind, yes, but it also can have disastrous consequences on your body

12:23.000 --> 12:27.000
and your future life and future existence.

12:27.000 --> 12:35.000
People who are born ugly with disfigured features or said to mean an evil in past lives.

12:35.000 --> 12:39.000
Who have short lives, who are sick, even though they might live a long time

12:39.000 --> 12:41.000
or are sick their whole lives.

12:41.000 --> 12:43.000
It's not shameful.

12:43.000 --> 12:49.000
It's not something that you would say, oh, that's a terrible person because of these sicknesses or so on.

12:49.000 --> 12:54.000
It's something as a lesson for us that we can take as a lesson.

12:54.000 --> 13:01.000
It helps us to understand and have a more just understanding of the world.

13:01.000 --> 13:06.000
This isn't because God decided that I should be sick or it isn't chance.

13:06.000 --> 13:08.000
It isn't just bad luck on my part.

13:08.000 --> 13:11.000
That there's some order to the universe.

13:11.000 --> 13:18.000
And most important, it should scare the heck out of us in regards to doing bad deeds in the future.

13:18.000 --> 13:27.000
I think for our purposes, though, the fourth one is where we find real wisdom and deep insight

13:27.000 --> 13:33.000
and real application in our practice, the sickness that comes from the mind.

13:33.000 --> 13:38.000
And so practically on a conventional shallow sort of level you could talk about

13:38.000 --> 13:44.000
the ways that the body suffers from the mind, and this is certainly true.

13:44.000 --> 13:51.000
Even just being angry, just generally being angry can have an effect on the body.

13:51.000 --> 13:55.000
Greedy lustful states have an effect on the body.

13:55.000 --> 13:58.000
Delusion has an effect on the body as well.

13:58.000 --> 14:04.000
The man who's arrogant or all puffed up can have great effect on the body.

14:04.000 --> 14:12.000
There's even some, and I'm not sure how credible it is, but there's some reference to the idea that the fluids in the body relate to

14:12.000 --> 14:15.000
the three defilements.

14:15.000 --> 14:17.000
So blood is related to anger.

14:17.000 --> 14:21.000
I think flam is related to delusion.

14:21.000 --> 14:26.000
And they can't remember which is related to what the third one would be.

14:26.000 --> 14:29.000
Pushed maybe, I don't know.

14:29.000 --> 14:35.000
So there are stories of people who got so angry that they vomited blood.

14:35.000 --> 14:44.000
They've adopted, I think, as one, and stories of people getting nosebleeds because of anger and so on.

14:44.000 --> 14:49.000
And certainly when you meditate you off, some people find that their nose is running a lot,

14:49.000 --> 14:53.000
or they have a lot of flam coming up, and there can be some relation there.

14:53.000 --> 14:55.000
None of this, I think, is very deep.

14:55.000 --> 15:02.000
It's a bit interesting as Buddhists, but much more interesting is not talking about our bodies too much.

15:02.000 --> 15:08.000
Not being too concerned, worried, or obsessed with the health of the body.

15:08.000 --> 15:11.000
Because, of course, the health of the mind is much more important.

15:11.000 --> 15:20.000
There's a much more powerful impact on our lives, on our happiness.

15:20.000 --> 15:28.000
And so the ultimate level of sickness, which is related to gain.

15:28.000 --> 15:32.000
Because if you say it's great, gain to be physically well, that's true.

15:32.000 --> 15:37.000
It's the best physical gain, I think, to be physically well.

15:37.000 --> 15:43.000
And you could say, even your rich and powerful, but if you're sick, it doesn't mean anything.

15:43.000 --> 15:45.000
It's a very important teaching.

15:45.000 --> 15:53.000
But it's not so deep as talking about mental health, because you can be physically well,

15:53.000 --> 15:59.000
as physically healthy as well. But if your mind is corrupt, you'll never be happy either.

15:59.000 --> 16:08.000
You can have everything. You can be healthy and strong and fit in good shape, as they say.

16:08.000 --> 16:14.000
And still be very unhappy because of your mind.

16:14.000 --> 16:18.000
And so really that's what we're about here.

16:18.000 --> 16:28.000
When they talk in modern parlay, in modern talk, they talk about mental illness.

16:28.000 --> 16:37.000
And I think it's important to point out how Buddhism says things a little bit differently.

16:37.000 --> 16:44.000
Because making something categorically a mental illness is very dangerous.

16:44.000 --> 16:49.000
Even if doctors don't mean to, when you call something an illness, you say,

16:49.000 --> 16:53.000
yes, this person has this illness and this person doesn't.

16:53.000 --> 16:57.000
You both trivialize the problems that this person might have.

16:57.000 --> 17:00.000
And you reify the problems that this person has.

17:00.000 --> 17:04.000
And by reify, I mean, you turn it into a thing.

17:04.000 --> 17:06.000
And then what are you going to do with this thing?

17:06.000 --> 17:09.000
I've talked about this before. It's not something you can deal with.

17:09.000 --> 17:11.000
What do you do with depression?

17:11.000 --> 17:14.000
It wants it to thing. What do you do with it?

17:14.000 --> 17:19.000
In Buddhism, we talk about experiences, habits.

17:19.000 --> 17:22.000
And if you work in terms of that, you can change.

17:22.000 --> 17:24.000
You can alter these things, but you can't.

17:24.000 --> 17:34.000
If someone has clinical depression, it's not like a possession that you have to carry on your back or in your pocket.

17:34.000 --> 17:39.000
Everyone has greed, anger, and delusion to varying degrees.

17:39.000 --> 17:50.000
For some people, it's become so intense that they get in a loop where they have no clue about how to deal with it, how to overcome it.

17:50.000 --> 17:58.000
But it doesn't make it any different categorically or functionally different from anybody else's mentalist.

17:58.000 --> 18:01.000
Now, not to trivialize that, of course.

18:01.000 --> 18:07.000
For some people, it's so radically different from an ordinary person.

18:07.000 --> 18:13.000
But even the word ordinary is reifying some kind of category.

18:13.000 --> 18:18.000
It's not a cat, it's not categorical.

18:18.000 --> 18:24.000
All of us have mental illness, and people who have been recognized as clinically having mental illnesses.

18:24.000 --> 18:36.000
Have the same problems and the same resources and the same path to follow in terms of ameliorating their situation.

18:36.000 --> 18:43.000
Bettering themselves.

18:43.000 --> 18:48.000
So this is what our meditation, it's a really good way of describing what our meditation is about.

18:48.000 --> 18:52.000
The insight meditation is about learning to see things clearly.

18:52.000 --> 18:55.000
And it's not so much, and I've said this before.

18:55.000 --> 18:58.000
It's not so much about what you see when we talk about seeing clearly.

18:58.000 --> 19:00.000
But what is it I'm going to see clearly?

19:00.000 --> 19:01.000
Yes, we can talk about that.

19:01.000 --> 19:07.000
But that's not so much important. The important is the state of seeing clearly.

19:07.000 --> 19:10.000
So we talk about understanding and wisdom, and people say,

19:10.000 --> 19:13.000
what is it the thing that we're going to learn?

19:13.000 --> 19:16.000
What are we going to understand expecting some kind of theory?

19:16.000 --> 19:20.000
And that's really not the point that there's something there.

19:20.000 --> 19:27.000
But much more important and much more true to Buddhism is the state of understanding.

19:27.000 --> 19:36.000
The perspective where you're no longer trying to control and fix and claim your understanding.

19:36.000 --> 19:42.000
It's like if someone comes to you with a problem or someone has done something,

19:42.000 --> 19:45.000
and you have choices, you can condemn them.

19:45.000 --> 19:50.000
You can side with them or you can understand. You can be understanding.

19:50.000 --> 19:53.000
When we deal with people, we talk about being understanding.

19:53.000 --> 19:58.000
And that's I think an important aspect of how we look at wisdom in Buddhism.

19:58.000 --> 20:02.000
That's why mindfulness is so important and so much our practice.

20:02.000 --> 20:07.000
I would say, don't think about wisdom as something that you're going to get

20:07.000 --> 20:12.000
or suddenly you'll realize some theory or profound idea.

20:12.000 --> 20:18.000
Wisdom, my teacher would say, when you know that the stomach is rising, that's wisdom.

20:18.000 --> 20:25.000
It very much is. If we were in that state where we knew the stomach was rising,

20:25.000 --> 20:33.000
that was the epitome, that was how we looked at everything.

20:33.000 --> 20:36.000
We would be enlightened.

20:36.000 --> 20:38.000
The problem is that we don't.

20:38.000 --> 20:43.000
We have so much delusion and ignorance that we react to things.

20:43.000 --> 20:48.000
We judge things and we're ill, we're sick.

20:48.000 --> 20:52.000
So when we free ourselves from that, that's the greatest game.

20:52.000 --> 20:55.000
That's the first one, we'll move along.

20:55.000 --> 21:02.000
The second one is another very good way of talking about the path.

21:02.000 --> 21:07.000
So there's this sort of shallow level, sent to deep and among Tannan.

21:07.000 --> 21:13.000
On a conventional level, it's very important, a very Buddhist to talk about contentment.

21:13.000 --> 21:18.000
Contentment is the greatest wealth.

21:18.000 --> 21:26.000
We talk a lot about consumerism and greed and how you'll never always have what you want

21:26.000 --> 21:31.000
and seeking out things and even experiences.

21:31.000 --> 21:35.000
It's just going to lead to more discontent.

21:35.000 --> 21:41.000
You'll get what you want and you'll want it more and you'll set yourself up for disappointment.

21:41.000 --> 21:50.000
Greater and greater disappointment when you can't get what you want.

21:50.000 --> 21:57.000
So of all the wealth that we could ever have, contentment is the highest.

21:57.000 --> 22:06.000
And we have to differentiate even here, I think, because the word contentment is used and abused.

22:06.000 --> 22:13.000
Many people will call themselves content and why are they content because they've got everything they want.

22:13.000 --> 22:16.000
And that's possible for some time, right?

22:16.000 --> 22:26.000
It's possible to be very content when you're rich and have a stable job or social life or family or...

22:26.000 --> 22:33.000
It's very, very possible when things are on the up.

22:33.000 --> 22:37.000
But that's not considered contentment in Buddhism.

22:37.000 --> 22:42.000
There's a deeper level of contentment, not the deepest level, I think.

22:42.000 --> 22:47.000
But, well, there's a deeper level.

22:47.000 --> 22:52.000
And the deeper level, of course, is to be content with anything.

22:52.000 --> 22:58.000
Sun to tea. It's a good word. Two tea means...

22:58.000 --> 23:03.000
It could mean something like joy or excitement or so on.

23:03.000 --> 23:06.000
It really means something like happiness. Let's say joy.

23:06.000 --> 23:13.000
And Sun means here it implies the idea of what you already have.

23:13.000 --> 23:25.000
Sun means what's there, kind of. Sun to tea means happy with one's own possession.

23:25.000 --> 23:31.000
But it doesn't mean happy with, as I said, certain things.

23:31.000 --> 23:35.000
It means happiness with things as they are.

23:35.000 --> 23:41.000
And the only way to really have that is when you're happy with everything or with anything.

23:41.000 --> 23:52.000
Sun to tea, meaning unconnected to conditions, unconnected to environment.

23:52.000 --> 23:55.000
And that's much more difficult, of course.

23:55.000 --> 24:00.000
It's easy to be easier to be content when you have things.

24:00.000 --> 24:04.000
It's not that easy. Some people are distal discontent.

24:04.000 --> 24:05.000
They can have everything and be...

24:05.000 --> 24:07.000
In fact, most people are.

24:07.000 --> 24:13.000
In fact, I would say it's quite fake to think of contentment as...

24:13.000 --> 24:17.000
I have all these things and I'm content. Why should I do anything?

24:17.000 --> 24:19.000
You could argue well, you might lose everything.

24:19.000 --> 24:24.000
But even when you have everything you could possibly want, there's always craving for more.

24:24.000 --> 24:29.000
And the only way is to jump from one thing to another because that's how the brain works.

24:29.000 --> 24:33.000
It means new stimuli. It means greater stimuli.

24:33.000 --> 24:38.000
And so you have to vary your experience.

24:38.000 --> 24:45.000
If you try to always be happy with things, like get the things that make you happy, you find it doesn't work.

24:45.000 --> 24:51.000
As you grow older, this is why people are tempering their desires because they realize

24:51.000 --> 24:56.000
or if they don't realize they become addicts and go that way and can get terrible.

24:56.000 --> 24:59.000
But if they're temperate, otherwise it doesn't work.

24:59.000 --> 25:08.000
That's the brain won't make you happy. It's only capable of a certain amount of happiness before it needs more stimuli.

25:08.000 --> 25:19.000
How the brain chemicals work.

25:19.000 --> 25:22.000
But much more difficult.

25:22.000 --> 25:29.000
But much more difficult, but much more sustainable and attainable is the true contentment.

25:29.000 --> 25:37.000
A place where you could really be content is when you're content with anything or nothing.

25:37.000 --> 25:45.000
Content when you get what you want or need, content when you don't get what you want or even need.

25:45.000 --> 25:49.000
And it's really not all or nothing, I suppose, but it's a direction.

25:49.000 --> 25:58.000
And the direction we take in meditation, we don't come here and say, okay, day one, I'm going to let go of everything.

25:58.000 --> 26:03.000
But as you practice, you let go of more and more, and you become more and more content.

26:03.000 --> 26:08.000
You need less and less to give you happiness.

26:08.000 --> 26:12.000
Your happiness becomes less and less dependent on experience.

26:12.000 --> 26:13.000
You see impermanence.

26:13.000 --> 26:19.000
Everything is changing, so you become less expecting that things should be this way or that way.

26:19.000 --> 26:25.000
You see suffering, so you become less clingy, because you see when you cling, you just dress yourself up.

26:25.000 --> 26:28.000
You become less controlling, you see things are not self.

26:28.000 --> 26:34.000
You control them less, because you see controlling leads to suffering.

26:34.000 --> 26:45.000
And as a result, you become better able to be at peace, touched by the vicissitudes of life.

26:45.000 --> 26:52.000
Your mind is unshaken, the Buddha said.

26:52.000 --> 26:53.000
That's unto tea.

26:53.000 --> 26:59.000
Santo tea contentment is a very good way of describing Buddhism, if described correctly.

26:59.000 --> 27:04.000
I understood in this way.

27:04.000 --> 27:08.000
The third one, we sassaparamaniati.

27:08.000 --> 27:14.000
We sassa is a bit difficult to translate, I think, trust confidence,

27:14.000 --> 27:18.000
but confidence in the specific sense of taking someone in confidence,

27:18.000 --> 27:22.000
having confidence in someone or having their confidence.

27:22.000 --> 27:25.000
It's a specific use of the word in English.

27:25.000 --> 27:31.000
Trust, in Thai, they actually say something more like familiarity,

27:31.000 --> 27:39.000
but it all points to a connection with someone that is based on trust,

27:39.000 --> 27:43.000
based on familiarity.

27:43.000 --> 27:46.000
And the Buddha said this is the greatest relative,

27:46.000 --> 27:51.000
and someone you can trust, someone you can depend upon,

27:51.000 --> 27:54.000
someone who you have this kind of relationship with.

27:54.000 --> 27:57.000
And the commentary makes it clear that even if your mother,

27:57.000 --> 28:00.000
even your mother and father, if they don't have this quality,

28:00.000 --> 28:03.000
you can't trust them, depend upon them.

28:03.000 --> 28:10.000
If you don't have this kind of close relationship where you depend on each other,

28:10.000 --> 28:13.000
then even your mother and father aren't really relatives.

28:13.000 --> 28:16.000
They're not very good relatives.

28:16.000 --> 28:19.000
And he said, but even if someone is not related to you,

28:19.000 --> 28:22.000
the commentary says, even if they're not related to you,

28:22.000 --> 28:26.000
they have these qualities, you can consider them a true relative.

28:26.000 --> 28:29.000
Now, this of all of them, I think, is a fairly,

28:29.000 --> 28:31.000
what we might call shallow teaching.

28:31.000 --> 28:33.000
It's useful, it's practical.

28:33.000 --> 28:36.000
It's good to remember, but the deeper teaching,

28:36.000 --> 28:41.000
there is a deeper teaching here, and that's related to this idea

28:41.000 --> 28:44.000
that someone could be a relative without being a relative,

28:44.000 --> 28:47.000
which in itself isn't a deep teaching,

28:47.000 --> 28:58.000
but it relates and it points to the difference between convention and reality.

28:58.000 --> 29:01.000
Because relatives, of course, are a convention.

29:01.000 --> 29:07.000
If you think about it, we're all probably related by blood generation after generation.

29:07.000 --> 29:10.000
There weren't two original human beings.

29:10.000 --> 29:13.000
However, it happened.

29:13.000 --> 29:18.000
But more importantly, what does it mean to be related at all?

29:18.000 --> 29:23.000
If you had a mother and father who you never met somehow,

29:23.000 --> 29:27.000
they died when you were born, or even before you were born,

29:27.000 --> 29:34.000
and your parents died somehow as you were being born kind of thing?

29:34.000 --> 29:39.000
There's one of the monks was like this,

29:39.000 --> 29:43.000
his name, I can't remember his name, it'll come to me,

29:43.000 --> 29:49.000
but it was one monk who his mother died while he was still in her stomach,

29:49.000 --> 29:52.000
and they were burning the body.

29:52.000 --> 29:55.000
Because he had such good karma, he didn't die.

29:55.000 --> 30:01.000
They burned the body, and the guy was using his stick to turn the body.

30:01.000 --> 30:05.000
They have to turn it because it doesn't burn unless you keep poking it.

30:05.000 --> 30:11.000
He scraped the eyebrow of this baby,

30:11.000 --> 30:14.000
and he got a name based on that.

30:14.000 --> 30:15.000
There's an old story.

30:15.000 --> 30:20.000
And he found it here, this baby crying, and he pulled it out.

30:20.000 --> 30:24.000
And I think the king raised him or something.

30:24.000 --> 30:28.000
I think I might have been Kumarakas, actually.

30:28.000 --> 30:31.000
Kumaram, because he was a prince.

30:31.000 --> 30:35.000
In this case, means prince, because of the king raised him.

30:35.000 --> 30:38.000
No, that wasn't Kumarakas, but that's another story.

30:38.000 --> 30:43.000
Someone knows, I'm sure.

30:43.000 --> 30:45.000
Is that Bakul? I know.

30:45.000 --> 30:49.000
One of the monks, one of the famous monks.

30:49.000 --> 30:54.000
But it can happen if you have no parents,

30:54.000 --> 31:01.000
or if you were separated from your parents at birth, I guess,

31:01.000 --> 31:07.000
is a better story than what would it mean to say you were related to them?

31:07.000 --> 31:10.000
You could argue there's karma involved,

31:10.000 --> 31:17.000
but more important and more useful for us is people who

31:17.000 --> 31:22.000
can depend upon people who are close to.

31:22.000 --> 31:25.000
And Buddhism we talk about, Buddhist cultures,

31:25.000 --> 31:29.000
they'll talk about something called

31:29.000 --> 31:31.000
Dhammanyati.

31:31.000 --> 31:35.000
Dhammanyati means a relative in the Dhamma.

31:35.000 --> 31:41.000
Entirely, transport is a nyati, Dhamma.

31:41.000 --> 31:45.000
But the deeper teaching there is that this relates to everything,

31:45.000 --> 31:51.000
that we have many conventions about things we think are good and right,

31:51.000 --> 31:55.000
and about our duties and relationships.

31:55.000 --> 31:58.000
Relationships, not just with people, but with places and things,

31:58.000 --> 32:00.000
like being Canadian, for example.

32:00.000 --> 32:03.000
Even me being a monk is just a convention.

32:03.000 --> 32:06.000
Being a man, being a woman.

32:06.000 --> 32:10.000
Many times Mara would come to the biquinis and tell them,

32:10.000 --> 32:13.000
what do you think you're doing?

32:13.000 --> 32:17.000
You're just a woman, and how can you practice?

32:17.000 --> 32:20.000
Because of course, women were not seen very highly at that time.

32:20.000 --> 32:23.000
Time, I think.

32:23.000 --> 32:26.000
And they would say, there's no woman here.

32:26.000 --> 32:29.000
There's no woman, there's no man.

32:29.000 --> 32:35.000
What room is there for any of that?

32:35.000 --> 32:38.000
And so this is a good example.

32:38.000 --> 32:40.000
It's not exactly I think with the Buddha was teaching,

32:40.000 --> 32:45.000
but it does make us think a deeper sense about sort of pulling ourselves

32:45.000 --> 32:50.000
out of the conventional, even human, being human and saying,

32:50.000 --> 32:54.000
10 fingers, 10 toes and two eyes and so on.

32:54.000 --> 32:57.000
What it means to be human, it's all just convention.

32:57.000 --> 33:00.000
It's just circumstance.

33:00.000 --> 33:05.000
Our life, the birth, all the age, death, not to mention things like getting a job

33:05.000 --> 33:09.000
or going to school, all of these conventions,

33:09.000 --> 33:12.000
having a relative, a mother and a father.

33:12.000 --> 33:15.000
I mean, there are not things we should discard out of hand,

33:15.000 --> 33:18.000
but they have to be put in their place.

33:18.000 --> 33:21.000
And we have to understand that on a deeper level.

33:21.000 --> 33:26.000
There's this old, this new age saying, we are stardust.

33:26.000 --> 33:29.000
And there is some truth, of course, to that.

33:29.000 --> 33:34.000
It puts things in perspective to realize that we are stardust.

33:34.000 --> 33:44.000
All of our atoms originally came from stars.

33:44.000 --> 33:49.000
Anyway, I think that that's a good interesting part of that teaching.

33:49.000 --> 33:51.000
Useful for us to remember.

33:51.000 --> 33:57.000
Much of insight meditation, mindfulness is about taking us out of our conventional

33:57.000 --> 34:03.000
ideas that are all contrived so that we can see more clearly what really is.

34:03.000 --> 34:13.000
And the fourth teaching, if you read the verse, it sounds like he's just listing off for things.

34:13.000 --> 34:18.000
But if you read the story, the story does point something out important that these others

34:18.000 --> 34:20.000
three may be the case.

34:20.000 --> 34:23.000
It may be true that all of these are true.

34:23.000 --> 34:27.000
It's true that these are all the case.

34:27.000 --> 34:31.000
But none of them are the highest happiness, being free from illness,

34:31.000 --> 34:36.000
unless you take it on a deeper level, having contentment again,

34:36.000 --> 34:38.000
unless you take it on a very deep level.

34:38.000 --> 34:45.000
Even if you do, these states are still mundane.

34:45.000 --> 34:49.000
And that the highest happiness is still nibana.

34:49.000 --> 34:52.000
The ultimate happiness.

34:52.000 --> 34:54.000
And this again gives us direction.

34:54.000 --> 34:58.000
It's not that we have to discard everything and only think of nibana.

34:58.000 --> 35:01.000
Of course, we really shouldn't think much about nibana at all.

35:01.000 --> 35:04.000
But it does give us a direction.

35:04.000 --> 35:06.000
Because nibana means cessation.

35:06.000 --> 35:08.000
It means freedom from suffering.

35:08.000 --> 35:09.000
It means letting go.

35:09.000 --> 35:15.000
It has all these connotations and characteristics that give us direction

35:15.000 --> 35:17.000
and remind us of where we're headed.

35:17.000 --> 35:22.000
That this isn't just something of if I practice meditation or be free from sickness

35:22.000 --> 35:24.000
because my body will feel healthy or so on.

35:24.000 --> 35:28.000
I'll be more content and I'll be able to live a more content life.

35:28.000 --> 35:30.000
It's much more profound in that.

35:30.000 --> 35:34.000
And much more direct and simple.

35:34.000 --> 35:35.000
It's just about peace.

35:35.000 --> 35:37.000
It's about happiness.

35:37.000 --> 35:40.000
It really is about happiness.

35:40.000 --> 35:44.000
It's about the fact that happiness can't come from clinging to things.

35:44.000 --> 35:45.000
Happiness comes from letting go.

35:45.000 --> 35:49.000
It can't come from things that we get, things that come to us.

35:49.000 --> 35:54.000
It can't come from things.

35:54.000 --> 35:59.000
Happiness comes from the wisdom to see nothing is worth clinging to.

35:59.000 --> 36:01.000
Happiness comes from letting go.

36:01.000 --> 36:04.000
The letting go that comes from wisdom.

36:04.000 --> 36:12.000
I've talked about nibana before, but in this verse there's a particular reminder.

36:12.000 --> 36:15.000
Not just putting nibana with everything else.

36:15.000 --> 36:20.000
All these other things may be good.

36:20.000 --> 36:24.000
None of them get to the real point.

36:24.000 --> 36:26.000
That nibana is the highest happiness.

36:26.000 --> 36:28.000
These may be the highest other things.

36:28.000 --> 36:31.000
And they're good and important.

36:31.000 --> 36:35.000
But ultimately nothing is better.

36:35.000 --> 36:38.000
Nothing is more direct.

36:38.000 --> 36:42.000
Nothing can be said to be greater happiness.

36:42.000 --> 36:45.000
Then nibana or letting go.

36:45.000 --> 36:48.000
Being free from suffering.

36:48.000 --> 36:52.000
So that's the Dhammapana for tonight.

36:52.000 --> 37:19.000
Thank you all for listening.

