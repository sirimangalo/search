1
00:00:00,000 --> 00:00:22,400
Okay, so today we had a fire in the building where we were holding meditation group

2
00:00:22,400 --> 00:00:34,800
meditation. So we are broadcasting here from my apartment instead temporarily. Tomorrow we

3
00:00:34,800 --> 00:00:41,800
should be back to the correct place. So if you just bear with me here, just going to get

4
00:00:41,800 --> 00:01:09,800
it's set up, turn on some lights, and give it a short talk.

5
00:01:11,800 --> 00:01:38,080
Let me get comfortable, and just meditators are going to come late, so I'm just going to

6
00:01:38,080 --> 00:01:48,880
be the talk in English over the internet, and then at 7.30, they'll come up and do good meditation again.

7
00:01:48,880 --> 00:02:12,600
So meditation practice is something that has the potential to change our lives, and

8
00:02:12,600 --> 00:02:25,840
has the potential to bring to us peace and happiness and freedom from suffering that we would

9
00:02:25,840 --> 00:02:45,360
have thought impossible before. It takes us out of the set of experiences and reactions

10
00:02:45,360 --> 00:03:05,640
to them that we're used to. It changes a great amount of what we call who we are.

11
00:03:05,640 --> 00:03:21,720
And it does this because meditation is the practice of changing how we look at things,

12
00:03:21,720 --> 00:03:42,960
changing our basic set of reactions to the reality around us.

13
00:03:42,960 --> 00:03:59,920
And for meditation to be effective, thus we can see that we will need to take it seriously.

14
00:03:59,920 --> 00:04:10,640
Meditation is not something that you can do on the side or do a certification or practice

15
00:04:10,640 --> 00:04:15,880
once a day or so and expect to get any real results.

16
00:04:15,880 --> 00:04:22,920
Meditation is something that it has to become a new way of looking at the world, a new

17
00:04:22,920 --> 00:04:37,680
way of living our lives. Because suffering and mental turmoil doesn't come only when

18
00:04:37,680 --> 00:04:46,600
we sit down, we meditate, we can come at any time, we can come when we're driving a car,

19
00:04:46,600 --> 00:04:51,400
we can come when we're sitting at home and we can come when we're using the computer

20
00:04:51,400 --> 00:05:00,240
or watching television or talking to our friends and our family, we can come when we're at

21
00:05:00,240 --> 00:05:14,360
work. They say Mara is the evil one we're sitting in Buddhism, they say Mara doesn't

22
00:05:14,360 --> 00:05:24,240
take a day off. So there's no break from danger and by danger here's something in danger

23
00:05:24,240 --> 00:05:31,360
from suffering or danger from doing the wrong thing or saying the wrong thing or thinking

24
00:05:31,360 --> 00:05:38,440
the wrong thing, being caught up in a cycle of suffering, it can come at any moment,

25
00:05:38,440 --> 00:05:42,960
you know if you sit in comfortably and suddenly something happens and makes you very angry

26
00:05:42,960 --> 00:05:50,200
or so you're sitting very comfortably and suddenly something you want something, you need

27
00:05:50,200 --> 00:05:59,720
something. The meditation isn't something that we can say well we'll do it once a day

28
00:05:59,720 --> 00:06:04,600
or once a week or once a month or once a year. The meditation has to be something that

29
00:06:04,600 --> 00:06:10,400
becomes who we are and becomes our part of our lives and the more better able we are

30
00:06:10,400 --> 00:06:22,200
we are integrating it with our lives, the more benefit it brings to us. Meditation, if we

31
00:06:22,200 --> 00:06:27,840
just look at this word it does say a lot and it's too bad that it's lost some of its impact

32
00:06:27,840 --> 00:06:35,400
we don't really look at the word anymore. The word meditation means to contemplate so it

33
00:06:35,400 --> 00:06:39,200
doesn't have to be just sitting or it doesn't have to be a time in your day or your

34
00:06:39,200 --> 00:06:45,760
week or month or year when you take off to meditate it can be contemplating anything

35
00:06:45,760 --> 00:06:53,040
it's a process of learning. Really Buddhist meditation if you practice it for any length

36
00:06:53,040 --> 00:06:59,360
of time you realize that it's the process of turning the whole of our lives and the whole

37
00:06:59,360 --> 00:07:05,440
world into a learning experience. You know we talk about how we learn things in our lives

38
00:07:05,440 --> 00:07:16,840
and we certainly learn a lot. We miss a heck of a lot more. We miss say 90% of the learning

39
00:07:16,840 --> 00:07:24,360
opportunity and we make mistakes and we don't learn from these mistakes and when we're

40
00:07:24,360 --> 00:07:31,560
meditating we learn from everything. We learn from every moment, we can learn from walking

41
00:07:31,560 --> 00:07:36,640
down the street, we can learn from driving our car, we can learn from talking to someone,

42
00:07:36,640 --> 00:07:44,680
we can learn from eating, we can learn from going to the washroom, we can learn from showering,

43
00:07:44,680 --> 00:07:50,960
we can learn from doing laundry and brushing your teeth, anything, washing the dishes.

44
00:07:50,960 --> 00:07:59,480
We can learn from the whole of our lives and becomes a school. Our lives become one big

45
00:07:59,480 --> 00:08:03,320
learning experience and this is really the wonder of meditation which is what is so exciting

46
00:08:03,320 --> 00:08:19,480
about it. So, exhilarating this idea that we found this practice that we've been given

47
00:08:19,480 --> 00:08:25,720
and it's been passed on to us this practice whereby suddenly we can make the most of our

48
00:08:25,720 --> 00:08:38,000
lives, really make the most and make our lives entirely meaningful. So, all of this

49
00:08:38,000 --> 00:08:44,280
is to say that meditation shouldn't be something where we just do a meditation course

50
00:08:44,280 --> 00:08:51,400
or do a meditation round and not to put down these and be little this experience of

51
00:08:51,400 --> 00:08:56,520
doing intense with meditation but it's something that we have to understand eventually

52
00:08:56,520 --> 00:09:07,720
has to become all of our life, our life is meditation, we live our lives, meditate, meditatively,

53
00:09:07,720 --> 00:09:16,520
we live our lives contemplating and considering everything carefully, being clearly aware

54
00:09:16,520 --> 00:09:28,040
of things. So, normally as I said before, when we're angry, it's quite a while before

55
00:09:28,040 --> 00:09:33,520
we realized that we're angry and we've already done them, said things that we probably

56
00:09:33,520 --> 00:09:39,440
wish we hadn't and when we want something, it takes quite a while before we realized

57
00:09:39,440 --> 00:09:47,760
that we wanted it. We may have already chased after it gone for it and feel kind of depressed

58
00:09:47,760 --> 00:09:59,960
after it, so addicted to this or that. The meditation changes that meditation allows us

59
00:09:59,960 --> 00:10:09,120
to keep up with experience, keep up with the rhythm of life and break things down to

60
00:10:09,120 --> 00:10:14,560
their constituent part. So, we see when we want something, well there's the one thing,

61
00:10:14,560 --> 00:10:21,440
there's the happy feeling associated with the one thing, there's the object and there's

62
00:10:21,440 --> 00:10:26,600
our thoughts about it, there's all these different parts and when we see it like this, we break

63
00:10:26,600 --> 00:10:31,840
it apart and there's nothing in any one part of that that's really attractive, really

64
00:10:31,840 --> 00:10:41,840
of any value. We see it, it has no core, it has no essence, there's no essential value

65
00:10:41,840 --> 00:10:48,840
to the object's ever desired. And with anger the same, as we start to acknowledge,

66
00:10:48,840 --> 00:11:01,040
the universe of angry, angry, disliking or pain or so on. We realize as well that there's no

67
00:11:01,040 --> 00:11:06,480
core to any of it, there's no core to the thing that's making us suffer, so it's not

68
00:11:06,480 --> 00:11:17,920
really painful or it's not really unpleasant and there's no benefit from being angry.

69
00:11:17,920 --> 00:11:24,320
Not only is there no benefit from being angry, but anger isn't us, it isn't I am angry.

70
00:11:24,320 --> 00:11:30,400
It's only something that arises, something that comes up. The most pure and simple way

71
00:11:30,400 --> 00:11:40,880
you can look at all of the states that arise in our mind is that they are, they simply

72
00:11:40,880 --> 00:11:49,720
arise and they don't belong to us, they don't have any essence or value, they simply

73
00:11:49,720 --> 00:11:58,160
are what they are. This is the clearest you can come to experiencing things as they

74
00:11:58,160 --> 00:12:10,720
are. The most clearest estimation of experience and the benefits of this are immeasurable

75
00:12:10,720 --> 00:12:17,000
and it not only makes us happier and more peaceful, but it also makes us a better person.

76
00:12:17,000 --> 00:12:21,880
And this is something that is often missed, I think, talking with people who are not

77
00:12:21,880 --> 00:12:28,800
meditators in the West, who have maybe no interest in meditation, but somehow we get talking.

78
00:12:28,800 --> 00:12:35,400
They feel like they're really happy, a lot of people, they get the idea that they have

79
00:12:35,400 --> 00:12:40,040
a great amount of happiness, perhaps greater than most people because they feel they hit

80
00:12:40,040 --> 00:12:46,400
upon the right way to live their lives. But they'll also admit that they get angry and

81
00:12:46,400 --> 00:12:53,040
that they have greed and that they have conceived and that they have opinions and so on.

82
00:12:53,040 --> 00:13:06,040
They have many things that when pressed, they will admit that our unpleasant, our unwholesome.

83
00:13:06,040 --> 00:13:10,160
Or maybe we won't even admit that I don't know people who don't admit this, who believe

84
00:13:10,160 --> 00:13:15,080
in the broad range of experience, that anger is human, greed is human, without these

85
00:13:15,080 --> 00:13:25,080
we would be a robot, we would be a heartless.

86
00:13:25,080 --> 00:13:31,800
And you really can't break this view or this idea until you start to meditate. I'm still

87
00:13:31,800 --> 00:13:35,600
really starting to consider carefully what it is that you're talking about because it's

88
00:13:35,600 --> 00:13:45,720
easy to have a view and it takes no effort to have a opinion about something. An idiot,

89
00:13:45,720 --> 00:13:52,280
someone who knows nothing about a subject can have an opinion on it. And so we can easily

90
00:13:52,280 --> 00:13:58,040
say whatever we want about reality. We can say this is good, this is bad, this is right,

91
00:13:58,040 --> 00:14:12,000
this is wrong. And with impunity, because there's no, there's no difference between

92
00:14:12,000 --> 00:14:20,160
one opinion or the other, their opinions. The problem of course comes when we understand

93
00:14:20,160 --> 00:14:26,120
that some opinions are false. When we come to analyze it and ask ourselves, what does

94
00:14:26,120 --> 00:14:33,880
this mean that every opinion is true? So if you believe that when you throw something

95
00:14:33,880 --> 00:14:41,240
up, it doesn't come back down for instance. When you push something, it doesn't move

96
00:14:41,240 --> 00:14:49,520
or so and then of course there's lots of physics. It can be tested and it can be found

97
00:14:49,520 --> 00:14:59,120
to be false for the majority of cases. And really the same goes with meditation, that's

98
00:14:59,120 --> 00:15:05,440
exactly how meditation works, that sure you can say that greed is good, anger is good.

99
00:15:05,440 --> 00:15:14,520
But what do you mean by good? What you mean is that it brings you some benefit. And the

100
00:15:14,520 --> 00:15:19,480
question is have you analyzed this, have you really studied this to see if it's

101
00:15:19,480 --> 00:15:25,800
true? You can have this view that this broad range of experiences, the best way to live

102
00:15:25,800 --> 00:15:30,720
your life, and you should never cut off any part of your experience. And on the one hand,

103
00:15:30,720 --> 00:15:41,800
I would agree with that. I think too much of our problems in the world come from repressing,

104
00:15:41,800 --> 00:15:49,360
negative emotions, repressing negative mindsets, those things that we don't.

105
00:15:49,360 --> 00:15:55,800
We can't bear with those things that we feel bad about the guilt, feelings, much of

106
00:15:55,800 --> 00:16:01,760
our problems in the world come from repressing, people hating themselves, and so on,

107
00:16:01,760 --> 00:16:13,480
feeling inadequate or imperfect. And there's a lot of this in the world. Quite a lot.

108
00:16:13,480 --> 00:16:21,520
But that doesn't mean, and of course it seems to me for many people, that you should

109
00:16:21,520 --> 00:16:25,480
conversely chase after these things. I mean there's a reason why people feel guilty about

110
00:16:25,480 --> 00:16:28,920
them, it's because they've analyzed them and they can see that they're wrong. And the

111
00:16:28,920 --> 00:16:33,920
only thing they can think to do is to repress them, which of course doesn't have the greatest

112
00:16:33,920 --> 00:16:42,920
effect, the consequences are that you get sick, you can become even perverted in the mind,

113
00:16:42,920 --> 00:16:55,200
it's like trying to repress a gas or oil spill or trying to keep it down and then suddenly

114
00:16:55,200 --> 00:17:03,080
the sea bed starts cracking and it comes up in other ways and you get an even bigger problem.

115
00:17:03,080 --> 00:17:11,560
We have this in the mind. The mind has to repress things. We find that the same defilements

116
00:17:11,560 --> 00:17:20,440
come out and it's seep up to the cracks in the floor of the mind that we've been using

117
00:17:20,440 --> 00:17:32,000
to push them down. But that doesn't mean that these things are good. I mean the reason

118
00:17:32,000 --> 00:17:34,920
we're pushing them down is because they're bad, the problem is pushing them down doesn't

119
00:17:34,920 --> 00:17:35,920
work.

120
00:17:35,920 --> 00:17:43,200
When we talk about chasing after or not chasing after but accepting and appreciating things

121
00:17:43,200 --> 00:17:52,160
like anger and greed and delusion and conceit and so on, we enter into the idea that everything

122
00:17:52,160 --> 00:17:57,720
is good, we enter into the view that everything is positive, that there's no negative

123
00:17:57,720 --> 00:18:07,080
emotion, we enter into this extreme idea that everything goes, which of course I think

124
00:18:07,080 --> 00:18:22,120
most of us would at least profess to disagree with. We would agree that there are many

125
00:18:22,120 --> 00:18:28,040
things in the world which are not right, beating people up is not right, hurting other

126
00:18:28,040 --> 00:18:38,920
people is not right, stealing in many cases anyway, stealing to the extent that it hurts

127
00:18:38,920 --> 00:18:43,240
someone is not right. Many people believe that stealing is okay as long as you're robbing

128
00:18:43,240 --> 00:18:51,600
from the rich or whatever. Today we went to try to register this trailer that we bought

129
00:18:51,600 --> 00:18:59,800
and we bought it and there was a space to fill in how much you paid for it. The idea of

130
00:18:59,800 --> 00:19:03,720
course didn't do the buying but it was under my name so I had to write down and I wrote

131
00:19:03,720 --> 00:19:15,280
down the price. Today we went into the Department of Motor Vehicles and when the man

132
00:19:15,280 --> 00:19:20,440
who was with me saw, he said why did you put down that price? Or maybe I pointed out to

133
00:19:20,440 --> 00:19:27,440
him, he said look the last owner, he only put $500 and we put down almost 3,000 and I said

134
00:19:27,440 --> 00:19:35,240
look at him and he said yeah you have to do that, you should have put 500. So he was trying

135
00:19:35,240 --> 00:19:39,320
to explain to me how it's not wrong and everybody does it and he said it's not cheating.

136
00:19:39,320 --> 00:19:47,600
And I said in what way is it not cheating? Directly cheating but the point here is that

137
00:19:47,600 --> 00:19:55,680
many people don't agree with the idea that all stealing and all lying is wrong. I'm not

138
00:19:55,680 --> 00:20:01,340
advocating for any lying. Of course I was adamant that I would rather pay the exorbitant

139
00:20:01,340 --> 00:20:11,120
taxes for the sale than to cheat and to save money. Of course it wasn't my money anyways.

140
00:20:11,120 --> 00:20:22,840
I really didn't have them figure it out themselves. But at any rate we can agree that

141
00:20:22,840 --> 00:20:30,800
there are many, there's a limit and there are many things which are disagreeable to the

142
00:20:30,800 --> 00:20:37,600
vast majority of us. We find people who cross these lines disagreeable and we believe

143
00:20:37,600 --> 00:20:45,240
that they're wrong. But then we can ask ourselves where do we draw these limits and what

144
00:20:45,240 --> 00:20:52,920
is it those things? What are the things that causes to cross these limits? Because if

145
00:20:52,920 --> 00:20:57,280
we look carefully and the problem is that we don't but when we look carefully we see that

146
00:20:57,280 --> 00:21:01,640
it's these very things that causes to cross those limits. And that in fact there are

147
00:21:01,640 --> 00:21:07,760
no barrier where you can say I stole this much and that was okay because it didn't

148
00:21:07,760 --> 00:21:14,040
hurt anyone and then I stole if I stole a little bit more suddenly I'm hurting someone.

149
00:21:14,040 --> 00:21:18,840
You can see that you're hurting both yourself and other people when you live like this.

150
00:21:18,840 --> 00:21:24,960
When you're hurting society this is why society runs the way it is with so many checks

151
00:21:24,960 --> 00:21:36,080
and balances, so many watchdogs and so much policing regulations. We wouldn't need

152
00:21:36,080 --> 00:21:42,320
policing, we wouldn't need regulations if it weren't for for executives that most of

153
00:21:42,320 --> 00:21:54,280
us cheat most of us lie and you know have many many of my students have a real hard time

154
00:21:54,280 --> 00:21:58,760
in meditation not many but some of my students have a hard time in meditation because

155
00:21:58,760 --> 00:22:05,200
they're unable to grasp it. You know many people cheat on their taxes and it's simply

156
00:22:05,200 --> 00:22:13,160
not proper to do so it's simply not a proper way to live your life. It's a sign of

157
00:22:13,160 --> 00:22:24,920
mental crookedness and I'm sorry to say but what we can see when we practice is that this

158
00:22:24,920 --> 00:22:32,480
is really having an impact on our mind that when we lie when we steal and we cheat when

159
00:22:32,480 --> 00:22:48,200
we kill or hurt other people it really has an effect on our mind. There are certain states

160
00:22:48,200 --> 00:22:52,960
of mind which we simply have to do with that we simply have to change the better.

161
00:22:52,960 --> 00:23:03,560
This is one thing that we can use to sort of reaffirm our incentive or interest in the

162
00:23:03,560 --> 00:23:10,280
practice. Is this realization that meditation not only makes us more peaceful but it also

163
00:23:10,280 --> 00:23:16,200
makes us a better person and this is how we can we can sort of rationalize when our

164
00:23:16,200 --> 00:23:24,000
practice is unpleasant because it often will be and it's pretty easy to dismiss the doubt

165
00:23:24,000 --> 00:23:32,800
that comes from unpleasant practice as you as you continue on once you practice intensively

166
00:23:32,800 --> 00:23:39,200
because you have to admit to yourself that you've got problems. If your meditation is

167
00:23:39,200 --> 00:23:45,360
not pleasant it's not peaceful. There's no reason for you to blame the meditation if you're

168
00:23:45,360 --> 00:23:51,680
still blaming the meditation practice it's a sign that you're really not getting it.

169
00:23:51,680 --> 00:23:58,480
Well I mean it's a sign that you're still not quite frightened not really practicing.

170
00:23:58,480 --> 00:24:05,960
Once you practice intensively for some time you change the way you look at things you

171
00:24:05,960 --> 00:24:12,680
start to see it's not the experience of whatever I'm doing it's my reaction to it.

172
00:24:12,680 --> 00:24:24,200
It's the problem. It's my mind. It's how I look at things. It's how I respond to the

173
00:24:24,200 --> 00:24:45,920
things around you. And so meditation is a re-adjusting of our perceptions of things. It's

174
00:24:45,920 --> 00:24:57,040
a cleaning out of a lot of the garbage that we keep in our minds. It's a way of understanding

175
00:24:57,040 --> 00:25:03,440
coming to understand things. It's a way of examining things like under a microscope without

176
00:25:03,440 --> 00:25:12,640
a microscope we could have never found germs or cells or even down to atoms. We would

177
00:25:12,640 --> 00:25:20,760
never really understood the nature of matter. But even with matter no matter how far

178
00:25:20,760 --> 00:25:29,280
we go we can never really understand the reality of the mind without looking at the

179
00:25:29,280 --> 00:25:40,000
mind under a microscope. I'm reading this interesting study or survey and I guess what

180
00:25:40,000 --> 00:25:45,680
do you call it? It's an overview of many of the studies and research that's gone on in

181
00:25:45,680 --> 00:25:52,000
regards to the mind and the connection between the mind and the brain. And something that

182
00:25:52,000 --> 00:26:05,760
was interesting that I really still haven't quite grasped is the idea of the part that

183
00:26:05,760 --> 00:26:15,120
the mind has. The role that the mind plays in physics in say quantum theory. Because quantum

184
00:26:15,120 --> 00:26:26,080
theory says or part of quantum theory or some quantum theorists have the idea and again

185
00:26:26,080 --> 00:26:33,880
I'm not a physicist. I might not be getting this entirely correct that before a particle

186
00:26:33,880 --> 00:26:47,480
before a particle is observed. It is theoretically in every state possible. So this example

187
00:26:47,480 --> 00:26:58,040
of Schrodinger's cat. He's got a cat in a box and there's a gun aimed at the cat that's

188
00:26:58,040 --> 00:27:08,600
hooked up to a particle, a certain particle somehow so that if the particle moves in one direction

189
00:27:08,600 --> 00:27:16,360
the gunfire and kills the cat. It moves in another direction the gun doesn't fire and doesn't

190
00:27:16,360 --> 00:27:23,480
kill the cat. And according to quantum theory both things have happened. Both things happen

191
00:27:23,480 --> 00:27:31,920
when you do this. So until you, but the interesting thing is that when you open the box only

192
00:27:31,920 --> 00:27:38,560
one thing has happened. And this is incredibly difficult to understand. But there's something

193
00:27:38,560 --> 00:27:44,320
there. What makes it easy to understand I guess is turning this over on its head and saying

194
00:27:44,320 --> 00:27:49,800
wait a minute. You're looking at this from a physical, you're looking at this from a certain

195
00:27:49,800 --> 00:27:56,480
point of view and that's the materialist point of view. So our core principles are making

196
00:27:56,480 --> 00:28:03,680
it complicated. If you understand reality from a mental point of view it makes perfect sense

197
00:28:03,680 --> 00:28:11,160
that you can never really have a complete theory of physics because the mind is creating

198
00:28:11,160 --> 00:28:22,040
the universe as it goes along. The mind is being seen in quantum physics, is having an

199
00:28:22,040 --> 00:28:27,960
effect on all particles. In the mind they say, what they say is you can even have an effect

200
00:28:27,960 --> 00:28:34,320
on particles that are billions of years old or billions of years ago or so. You know,

201
00:28:34,320 --> 00:28:43,480
rays of light that are coming from distant stars and so on. Meaning that the mind is actually

202
00:28:43,480 --> 00:28:48,080
creating, one theory could be that the mind is creating the universe. And this is a theory

203
00:28:48,080 --> 00:28:57,680
that is not discarded by many physicists, of course not all physicists. But the idea that

204
00:28:57,680 --> 00:29:06,920
physics is just a construct that has come about by this huge conglomeration of minds

205
00:29:06,920 --> 00:29:16,320
or of mind of substance. I thought that was quite interesting and really puts into focus

206
00:29:16,320 --> 00:29:24,960
this idea of training the mind that instead of thinking, well, this is my life here

207
00:29:24,960 --> 00:29:32,240
other than this building with all of these people. And this is my reality. Looking at

208
00:29:32,240 --> 00:29:36,800
it from point of view of a mind, the mind and coming to realize that actually reality

209
00:29:36,800 --> 00:29:44,680
is experiencing certain things from seeing certain things, hearing certain things. And

210
00:29:44,680 --> 00:30:01,680
this experience is being created by my actions and reactions to the stimulus. And this

211
00:30:01,680 --> 00:30:08,920
latter way of looking at things is infinitely more beneficial, infinitely more useful than

212
00:30:08,920 --> 00:30:13,960
a materialist point of view. Because the materialist point of view easily falls into so

213
00:30:13,960 --> 00:30:20,960
many problems and creates so many problems. It's created this environmental degradation

214
00:30:20,960 --> 00:30:32,040
and not the theoretical materialism. But materialism as a theory has permeated our

215
00:30:32,040 --> 00:30:38,160
entire culture to the point where materialism is the most important, getting money, getting

216
00:30:38,160 --> 00:30:48,640
possessions, getting physical pleasures are considered to be the highest. And it turns

217
00:30:48,640 --> 00:30:56,000
us into very coarse beings to look at people who have talked with and watched certain

218
00:30:56,000 --> 00:31:02,200
people who profess materialism and profound materialism. And they tend to be very coarse

219
00:31:02,200 --> 00:31:11,320
individual. Simply on that basis alone, the personality type of people who have not taken

220
00:31:11,320 --> 00:31:16,480
any interest in the mind of no interest in training the minds who believe that we are

221
00:31:16,480 --> 00:31:23,440
who we are because of our genes and sort of this random interaction or not in our random

222
00:31:23,440 --> 00:31:35,640
but sort of chance or happenstance interaction between particles and cells and organisms.

223
00:31:35,640 --> 00:31:44,400
And so we just live our lives as we live it. Whatever we want to do, we do it. And this

224
00:31:44,400 --> 00:31:48,520
way of living, we can see that brings about a certain type of personality. And on that

225
00:31:48,520 --> 00:31:54,000
basis alone, I'd say I wouldn't want to embrace that sort of theory. It is not the sort

226
00:31:54,000 --> 00:31:59,080
of person that I'd want to be. But on a deeper level, when you start to look at things

227
00:31:59,080 --> 00:32:06,640
from whatever perspective, it's very easy. If you're open minded, it's very easy to see

228
00:32:06,640 --> 00:32:12,280
that the mind is really the most important thing. And it's quite profound to read

229
00:32:12,280 --> 00:32:19,480
this about the quantum theory that it says that the mind is the one that creates the decision

230
00:32:19,480 --> 00:32:24,600
that makes the decision whether the cat has died or the cat is still alive. The particle

231
00:32:24,600 --> 00:32:30,680
has moved and has not moved. Simply by observing it. Because that's the first thing

232
00:32:30,680 --> 00:32:35,520
that Buddha said or the first verse that we have, the Buddha's teaching in the Dhamma

233
00:32:35,520 --> 00:32:41,840
panda, which is Manopupankama. It really gives that verse a new meaning. It means mind

234
00:32:41,840 --> 00:32:48,360
precedes all things. And from a meditative perspective, we understand this from Buddha's

235
00:32:48,360 --> 00:32:55,840
perspective. We understand the theory. But to hear this from quantum businesses, really

236
00:32:55,840 --> 00:33:03,840
drives it home. These particles are created by the mind. The mind comes first. The mind

237
00:33:03,840 --> 00:33:18,240
is the first thing. And the second thing is the particle. The reality. So that's a little

238
00:33:18,240 --> 00:33:23,720
bit wondering, I suppose, but here are just some general thoughts on meditation practice

239
00:33:23,720 --> 00:33:30,440
and coming back to earth, coming back to what is really useful for us, what this all

240
00:33:30,440 --> 00:33:35,360
means is that we should incorporate our practice into our daily life, that our meditation

241
00:33:35,360 --> 00:33:40,080
should become something that we do when we walk, something that we do when we sit, when

242
00:33:40,080 --> 00:33:43,880
we walk within walking and you're sitting at your computer here instead of fiddling with

243
00:33:43,880 --> 00:33:49,120
the mouse and looking at this sort of thing. That take the time to really appreciate this

244
00:33:49,120 --> 00:33:55,320
time that we have together and sit in meditation. Or if you're looking at something or

245
00:33:55,320 --> 00:34:01,000
whatever, try to figure out what it is that's causing your mind to wander, what is it that's

246
00:34:01,000 --> 00:34:05,560
causing your mind to chase after things. You can learn so many things when you want to do

247
00:34:05,560 --> 00:34:09,280
something. First, there's the wanting and there's the doing. So if you understand the

248
00:34:09,280 --> 00:34:14,880
wanting, you don't need to do it. You don't need to chase after the object. And we do

249
00:34:14,880 --> 00:34:20,600
this again simply by contemplating it as it is when you want something wanting, when

250
00:34:20,600 --> 00:34:29,080
you don't like something just liking, just typing and so on. I'm trying to be mindful

251
00:34:29,080 --> 00:34:33,840
of just about everything that comes mindful of the body. When you're eating food, you

252
00:34:33,840 --> 00:34:40,720
can scoop things, or putting your mouth opening, chewing, swallowing. When you wash your hair

253
00:34:40,720 --> 00:34:46,080
washing, scrubbing, scrubbing, when you brush your teeth, brushing, brushing. When you

254
00:34:46,080 --> 00:34:53,800
lie down to sleep lying, lying, rising, falling. Mindfulness of pains and aches and soreness

255
00:34:53,800 --> 00:34:59,600
when they come up instead of reaching for the tylenol and aspirin or painkillers and pain.

256
00:34:59,600 --> 00:35:10,240
Instead, just remind yourself and analyze it for what you can see, for what it is, pain, pain

257
00:35:10,240 --> 00:35:15,000
until it goes away. And you won't need to take a pain during, you won't even feel upset

258
00:35:15,000 --> 00:35:22,680
by it. You'll feel so much relieved, so much more relieved. Buy your ability to accept

259
00:35:22,680 --> 00:35:31,840
things as they are. When you're thinking, say to yourself thinking, emotions that come

260
00:35:31,840 --> 00:35:43,400
up, liking, liking, disliking, drowsing, distracted, never negative mind-stain to rise, picking

261
00:35:43,400 --> 00:35:49,560
up them. These are very important. You like something like stopping yourself and chasing

262
00:35:49,560 --> 00:35:57,040
after things or running away from things. You feel drowsy and lazy. That stops you from

263
00:35:57,040 --> 00:36:01,760
getting things done. You feel distracted. It stops you from focusing. You have doubt

264
00:36:01,760 --> 00:36:10,440
stops you from doing anything. And when you do this, this is really the tools that we

265
00:36:10,440 --> 00:36:18,200
use to examine reality, to learn about ourselves, to come to understand the experience,

266
00:36:18,200 --> 00:36:28,040
understand things as they are. So that's the talk to which they, and I think maybe just

267
00:36:28,040 --> 00:36:32,880
in a few minutes we'll have a group up here meditating. It looks like there's another

268
00:36:32,880 --> 00:36:41,640
fire or smoke or something, so just bear with us and we'll be practicing here until 9pm.

269
00:36:41,640 --> 00:36:48,640
Just might take us a few minutes to get organized. Okay. Thanks for coming anyway. That's

270
00:36:48,640 --> 00:37:07,240
all for the talk today. Next up is meditation.

