1
00:00:00,000 --> 00:00:03,200
Hello, welcome back to Ask a Monk.

2
00:00:03,200 --> 00:00:05,540
Today I will be answering the question as to whether

3
00:00:05,540 --> 00:00:12,280
greens have karmic potency, so whether they have the ability

4
00:00:12,280 --> 00:00:18,480
to give rise to ethical states, do they have ethical

5
00:00:18,480 --> 00:00:22,080
implications, in the sense that can they affect our lives

6
00:00:22,080 --> 00:00:24,160
and can they affect the world around us?

7
00:00:24,160 --> 00:00:27,960
Is it possible to perform Buddhist karma,

8
00:00:27,960 --> 00:00:31,480
to make bad karma while you're asleep?

9
00:00:31,480 --> 00:00:37,080
And the answer I think is quite clearly yes.

10
00:00:37,080 --> 00:00:40,920
And it's important that we understand what we mean by karma.

11
00:00:40,920 --> 00:00:44,160
And this sort of helps us to understand what we mean by

12
00:00:44,160 --> 00:00:46,520
karma in Buddhism.

13
00:00:46,520 --> 00:00:49,480
Karma isn't an action that you do with the body or speech.

14
00:00:49,480 --> 00:00:51,720
It's something that arises in the mind.

15
00:00:51,720 --> 00:00:55,000
When you intend to do something that's karma, so the only

16
00:00:55,000 --> 00:00:57,840
reason to say killing is wrong or stealing is wrong or

17
00:00:57,840 --> 00:01:01,120
it's become bad karma is because of the effect that such

18
00:01:01,120 --> 00:01:05,560
things have on our mind and the mental aspect of the act

19
00:01:05,560 --> 00:01:10,760
that your mind is building up the tendency to be angry

20
00:01:10,760 --> 00:01:14,360
or greedy or corrupt.

21
00:01:14,360 --> 00:01:23,880
So in that sense, because dreams are tend to be ethically charged.

22
00:01:23,880 --> 00:01:27,400
So we give rise to the same states in our mind of greed

23
00:01:27,400 --> 00:01:29,320
and anger and delusion.

24
00:01:29,320 --> 00:01:32,920
They can all arise in our mind quite clearly there are ethical

25
00:01:32,920 --> 00:01:33,720
implications.

26
00:01:33,720 --> 00:01:36,520
Now, I want to talk about a few things here.

27
00:01:36,520 --> 00:01:41,240
So I'm going to try to keep them in a clear order.

28
00:01:41,240 --> 00:01:46,120
The first thing I should talk about is what we understand dreams

29
00:01:46,120 --> 00:01:47,600
to be in Buddhism.

30
00:01:47,600 --> 00:01:48,680
So what are we talking about?

31
00:01:52,040 --> 00:01:57,000
We have a description of the different kinds of dreams in one

32
00:01:57,000 --> 00:02:00,120
of the old Buddhist texts, the questions of King Melinda, the

33
00:02:00,120 --> 00:02:02,400
Melinda Panha.

34
00:02:02,400 --> 00:02:06,600
And that text is mentioned that there are six types of dreams.

35
00:02:06,600 --> 00:02:09,880
And so this will help us to understand what exactly

36
00:02:09,880 --> 00:02:14,960
mean by dreams and exactly how relevant they are to our

37
00:02:14,960 --> 00:02:18,120
practice and so on.

38
00:02:18,120 --> 00:02:20,720
Because I also want to talk about how we should approach dreams

39
00:02:20,720 --> 00:02:24,000
and what it means in terms of our practice.

40
00:02:24,000 --> 00:02:27,000
So there are six kinds.

41
00:02:27,000 --> 00:02:29,640
The first three are physical, wind, bile,

42
00:02:29,640 --> 00:02:30,760
flam.

43
00:02:30,760 --> 00:02:35,680
And this is just based on the medical terminology that they

44
00:02:35,680 --> 00:02:39,400
would use in the time of the Buddha or in ancient India.

45
00:02:39,400 --> 00:02:42,320
So basically, it means a disruption in the body.

46
00:02:42,320 --> 00:02:47,600
There's something maybe you eat bad food or maybe your body

47
00:02:47,600 --> 00:02:49,400
is out of order, you have sickness or so.

48
00:02:49,400 --> 00:02:53,880
And that can easily give rise to dreams.

49
00:02:53,880 --> 00:03:00,120
And the other three are some external influence, which

50
00:03:00,120 --> 00:03:01,680
could be a person around you.

51
00:03:01,680 --> 00:03:03,000
It could be spirits.

52
00:03:03,000 --> 00:03:05,680
It could be angels and I mean, I think what they're

53
00:03:05,680 --> 00:03:06,680
talking about is angels.

54
00:03:06,680 --> 00:03:09,640
But actually, if you think about it, we don't even have to

55
00:03:09,640 --> 00:03:10,960
go so far.

56
00:03:10,960 --> 00:03:13,200
If someone starts whispering in your ear where you're sleeping

57
00:03:13,200 --> 00:03:16,360
or if you hear noises, if you've ever had something

58
00:03:16,360 --> 00:03:19,960
going on around you and you start dreaming about something

59
00:03:19,960 --> 00:03:23,320
based on the sound that you hear or if it gets hot,

60
00:03:23,320 --> 00:03:27,280
that can affect your state of mind and so on,

61
00:03:27,280 --> 00:03:29,320
even cause the degree.

62
00:03:29,320 --> 00:03:33,840
The fifth is something from the past and the sixth is

63
00:03:33,840 --> 00:03:36,280
something from the future.

64
00:03:36,280 --> 00:03:38,080
This is where it gets interesting.

65
00:03:38,080 --> 00:03:44,320
So some people have visions of things in their dreams

66
00:03:44,320 --> 00:03:47,240
can be from the past in this life.

67
00:03:47,240 --> 00:03:49,400
Sometimes people are suspicious that it's something

68
00:03:49,400 --> 00:03:51,600
from their past lives.

69
00:03:51,600 --> 00:03:54,480
But let's just say something in the past in this life,

70
00:03:54,480 --> 00:03:57,320
because that's really obvious memories that we've had

71
00:03:57,320 --> 00:04:01,480
things that we keep in our minds can cause us to dream.

72
00:04:01,480 --> 00:04:07,040
But the sixth one is considered to be important.

73
00:04:07,040 --> 00:04:11,520
And it's worth noting that only the sixth is

74
00:04:11,520 --> 00:04:15,040
considered to be important in terms of actually paying

75
00:04:15,040 --> 00:04:18,120
some kind of attention to it.

76
00:04:18,120 --> 00:04:21,320
The sixth kind is something from the future.

77
00:04:21,320 --> 00:04:23,600
And some people may not even believe this,

78
00:04:23,600 --> 00:04:27,040
but other people will be emphatic that this sort of thing

79
00:04:27,040 --> 00:04:29,440
happens to them, where they'll dream something

80
00:04:29,440 --> 00:04:31,480
and then it happens.

81
00:04:31,480 --> 00:04:34,360
Suddenly, it comes in their life.

82
00:04:34,360 --> 00:04:37,040
People can even have this in meditation.

83
00:04:37,040 --> 00:04:40,280
But it's possible that something from the future

84
00:04:40,280 --> 00:04:43,560
can cause a dream, something that's about to happen.

85
00:04:43,560 --> 00:04:45,880
And you can understand this in different ways.

86
00:04:45,880 --> 00:04:49,360
It could be simply because of the power of what's

87
00:04:49,360 --> 00:04:53,000
going to happen, because we consider

88
00:04:53,000 --> 00:04:57,320
that the universe is much more than we can see

89
00:04:57,320 --> 00:04:59,560
and experience normally.

90
00:04:59,560 --> 00:05:03,080
So there are a lot of forces that are at work, potentially.

91
00:05:03,080 --> 00:05:08,880
And those forces can be experienced if you know about how

92
00:05:08,880 --> 00:05:10,760
animals are able to predict the weather

93
00:05:10,760 --> 00:05:13,320
and are able to predict disasters.

94
00:05:13,320 --> 00:05:16,720
And like before the tsunami in Asia,

95
00:05:16,720 --> 00:05:20,680
they say that the elephants all ran in land,

96
00:05:20,680 --> 00:05:26,240
like they knew it was going to come, and so on.

97
00:05:26,240 --> 00:05:29,000
Just the forces of nature that we're not

98
00:05:29,000 --> 00:05:33,600
able to sense in our normal state.

99
00:05:33,600 --> 00:05:36,520
Because actually, when you're sleeping,

100
00:05:36,520 --> 00:05:39,680
let's talk a little bit about what exactly dreams are.

101
00:05:39,680 --> 00:05:41,720
So these are the six sources of dreams.

102
00:05:41,720 --> 00:05:43,320
But what's happening in the dream state

103
00:05:43,320 --> 00:05:46,280
is your mind is not asleep and is not awake.

104
00:05:46,280 --> 00:05:49,160
This is what we know in science as well.

105
00:05:49,160 --> 00:05:50,880
But from a meditative point of view,

106
00:05:50,880 --> 00:05:55,400
there's still a great amount of concentration,

107
00:05:55,400 --> 00:05:56,920
but there's no mindfulness.

108
00:05:56,920 --> 00:06:00,200
So you're not really in control, obviously,

109
00:06:00,200 --> 00:06:03,320
as for most of us, unless you train yourself,

110
00:06:03,320 --> 00:06:05,480
because there are actually people who train themselves

111
00:06:05,480 --> 00:06:06,440
in dreams.

112
00:06:06,440 --> 00:06:07,840
And I'm not going to teach you to do this.

113
00:06:07,840 --> 00:06:09,240
This isn't what I teach.

114
00:06:09,240 --> 00:06:12,760
I just want to help people to understand this

115
00:06:12,760 --> 00:06:15,840
so we can move on and continue with our meditation.

116
00:06:15,840 --> 00:06:17,360
Because this will interrupt your life

117
00:06:17,360 --> 00:06:19,400
when you're wondering, well, people do wonderful.

118
00:06:19,400 --> 00:06:23,200
What is the significance of dreams?

119
00:06:23,200 --> 00:06:25,000
Most dreams are not significant.

120
00:06:25,000 --> 00:06:26,520
Because as I said, you're in this state,

121
00:06:26,520 --> 00:06:28,600
and you can be easily influenced by things

122
00:06:28,600 --> 00:06:30,640
because you don't have the ability to judge,

123
00:06:30,640 --> 00:06:34,640
to discriminate, and to catch yourself.

124
00:06:34,640 --> 00:06:39,920
And so you easily slip into illusion and fantasy.

125
00:06:39,920 --> 00:06:43,360
But there are certain dreams that are affected by the future.

126
00:06:43,360 --> 00:06:45,920
And this is what leads people, I think,

127
00:06:45,920 --> 00:06:48,840
to this end the dreams about the past,

128
00:06:48,840 --> 00:06:51,080
which is what leads people to put significance in dreams.

129
00:06:51,080 --> 00:06:54,680
So they might experience a dream that they feel

130
00:06:54,680 --> 00:06:57,480
has some significance and that should be explored

131
00:06:57,480 --> 00:07:01,360
and has some meaning and importance based on the past

132
00:07:01,360 --> 00:07:03,920
and something they might be repressing and so on.

133
00:07:03,920 --> 00:07:07,600
Or they might feel that they see they have some dream

134
00:07:07,600 --> 00:07:11,640
and then it sort of comes true in the future.

135
00:07:11,640 --> 00:07:13,240
And so as a result, they feel that dreams

136
00:07:13,240 --> 00:07:16,880
have great significance and psychotherapists

137
00:07:16,880 --> 00:07:22,280
and Freud and Jung put great emphasis on dreams

138
00:07:22,280 --> 00:07:24,640
and the importance of them.

139
00:07:24,640 --> 00:07:26,800
I want to say that we shouldn't put great importance

140
00:07:26,800 --> 00:07:27,880
on our dreams.

141
00:07:27,880 --> 00:07:29,840
And this comes back to the idea of whether you

142
00:07:29,840 --> 00:07:30,800
can create karma.

143
00:07:30,800 --> 00:07:34,040
As I said, there are karmic states that arise.

144
00:07:34,040 --> 00:07:42,040
And so you are potentially creating more karma.

145
00:07:42,040 --> 00:07:44,880
But the point is that you can't really control that.

146
00:07:44,880 --> 00:07:47,400
You can't say, I'm not going to create karma.

147
00:07:47,400 --> 00:07:48,760
Even you can't say stop.

148
00:07:48,760 --> 00:07:52,560
You can't stop yourself from giving rise to fear or anger

149
00:07:52,560 --> 00:07:57,480
for nightmares or anger or attachment and lust and so on.

150
00:07:57,480 --> 00:08:02,040
So it's much more useful.

151
00:08:02,040 --> 00:08:04,520
What I think dreams are really useful for

152
00:08:04,520 --> 00:08:07,760
is for understanding our mind state.

153
00:08:07,760 --> 00:08:12,600
And we should never take the whole of the dream

154
00:08:12,600 --> 00:08:15,640
as somehow significant, even if it means something

155
00:08:15,640 --> 00:08:19,480
for the future or it has a potential to be a prediction

156
00:08:19,480 --> 00:08:26,280
or when you call them, for telling the future.

157
00:08:26,280 --> 00:08:29,960
Or even if it comes from some repressed state

158
00:08:29,960 --> 00:08:31,600
that we have in the past, the point

159
00:08:31,600 --> 00:08:35,040
is that you're in a state where the mind is not connected.

160
00:08:35,040 --> 00:08:38,840
It's not functioning in a logical manner.

161
00:08:38,840 --> 00:08:41,560
So it's going to make connections between things

162
00:08:41,560 --> 00:08:42,880
that otherwise don't have connections.

163
00:08:42,880 --> 00:08:46,040
So maybe part of it comes from the fact

164
00:08:46,040 --> 00:08:48,880
that you had some traumatic experience in the past.

165
00:08:48,880 --> 00:08:53,160
But part of it just comes from the chaos in the mind

166
00:08:53,160 --> 00:08:58,320
and the nature of the mind to dream and to imagine.

167
00:08:58,320 --> 00:09:00,720
Likewise, you might think of something in the future

168
00:09:00,720 --> 00:09:03,560
or you might have the ability to see what's

169
00:09:03,560 --> 00:09:06,760
going to happen in the future, but it becomes distorted.

170
00:09:06,760 --> 00:09:08,160
It is distorted by the mind.

171
00:09:08,160 --> 00:09:11,760
It's the same as when you go in a hallucinogenic drug

172
00:09:11,760 --> 00:09:14,800
and you might have visions of angels, deities,

173
00:09:14,800 --> 00:09:18,040
and all sorts of spiritual things.

174
00:09:18,040 --> 00:09:20,760
And so you think that this is the truth

175
00:09:20,760 --> 00:09:22,240
and something that you're seeing something

176
00:09:22,240 --> 00:09:23,240
that we can't normally see.

177
00:09:23,240 --> 00:09:29,120
But it's also the brain that is creating,

178
00:09:29,120 --> 00:09:31,200
is imagining, is hallucinating.

179
00:09:31,200 --> 00:09:33,520
So whether part of it is real and whether

180
00:09:33,520 --> 00:09:36,520
it is sending the mind off into a dimension that

181
00:09:36,520 --> 00:09:38,480
can see things that we otherwise couldn't see,

182
00:09:38,480 --> 00:09:40,720
it's also the brain.

183
00:09:40,720 --> 00:09:43,920
And it's also doing all sorts of crazy things.

184
00:09:43,920 --> 00:09:45,640
I think if you look carefully, you'll

185
00:09:45,640 --> 00:09:47,800
see that this is true with dreams and that they shouldn't

186
00:09:47,800 --> 00:09:50,240
be trusted.

187
00:09:50,240 --> 00:09:53,040
And also, I think in the sense of whether you can create

188
00:09:53,040 --> 00:09:55,920
karma or the karma that you're creating.

189
00:09:55,920 --> 00:10:00,560
Because for one thing, the karma is not

190
00:10:00,560 --> 00:10:02,680
going to be strong because you don't really

191
00:10:02,680 --> 00:10:05,280
intend to do this or that in your dreams.

192
00:10:05,280 --> 00:10:08,080
You just kind of get angry or worried or afraid

193
00:10:08,080 --> 00:10:12,640
in the case of nightmares or have lustful dreams.

194
00:10:12,640 --> 00:10:14,880
But you don't really have the strong intention

195
00:10:14,880 --> 00:10:17,120
because your mind is not clear.

196
00:10:17,120 --> 00:10:21,440
Your mind doesn't have the will or the awareness

197
00:10:21,440 --> 00:10:28,200
or the mindfulness, which is able to make decisions clearly.

198
00:10:28,200 --> 00:10:31,960
So it's going to be weak karma for one thing.

199
00:10:31,960 --> 00:10:38,680
And also, for another thing, there's many states

200
00:10:38,680 --> 00:10:39,640
that are mixed up.

201
00:10:39,640 --> 00:10:43,480
And as I said, connections can be made in an illogical manner.

202
00:10:43,480 --> 00:10:47,080
And therefore, you have all these crazy dreams.

203
00:10:47,080 --> 00:10:50,400
So what we should use dreams for is to be able to judge

204
00:10:50,400 --> 00:10:52,080
our mind state, because what's good about them

205
00:10:52,080 --> 00:10:53,520
is you don't have this control.

206
00:10:53,520 --> 00:10:55,200
You aren't covering things up.

207
00:10:55,200 --> 00:10:57,080
Because there's no control, you're

208
00:10:57,080 --> 00:11:00,560
able to see many things about yourself

209
00:11:00,560 --> 00:11:01,840
that you wouldn't otherwise see.

210
00:11:01,840 --> 00:11:04,880
And this isn't the content of the dream,

211
00:11:04,880 --> 00:11:06,680
but it's the quality of the dream.

212
00:11:06,680 --> 00:11:08,880
So if you have a dream that's based on anger,

213
00:11:08,880 --> 00:11:11,440
if you have a dream that's based on fear, nightmares,

214
00:11:11,440 --> 00:11:14,040
you have a dream that's based on lust, or so on,

215
00:11:14,040 --> 00:11:15,440
this shows you what's in your mind.

216
00:11:15,440 --> 00:11:17,560
And often, in a way that you otherwise

217
00:11:17,560 --> 00:11:18,440
wouldn't be able to see.

218
00:11:18,440 --> 00:11:21,240
So people who are otherwise calm and controlled

219
00:11:21,240 --> 00:11:23,200
might find themselves suddenly having nightmares.

220
00:11:23,200 --> 00:11:25,120
And so they think something is wrong.

221
00:11:25,120 --> 00:11:27,120
Well, the truth is, this is you're seeing something deep

222
00:11:27,120 --> 00:11:30,400
down that you're repressing, because we're only

223
00:11:30,400 --> 00:11:32,320
able to repress things in our daily life

224
00:11:32,320 --> 00:11:36,240
by force of will, by actually some sort of mindfulness,

225
00:11:36,240 --> 00:11:40,960
and the ability to choose not to follow,

226
00:11:40,960 --> 00:11:43,600
which we don't have when we're dreaming, when we're asleep.

227
00:11:43,600 --> 00:11:46,880
So in that sense, I would say that really the only thing

228
00:11:46,880 --> 00:11:49,080
that we should do with dreams is use them

229
00:11:49,080 --> 00:11:51,360
to see where we have work to do, and to help us

230
00:11:51,360 --> 00:11:53,400
to understand how our mind works.

231
00:11:53,400 --> 00:11:56,000
Don't take the content of the dream as important.

232
00:11:56,000 --> 00:11:59,560
This is a big mistake, because you don't know

233
00:11:59,560 --> 00:12:00,440
where it's coming from.

234
00:12:00,440 --> 00:12:03,080
It could be coming simply because you had bad food.

235
00:12:03,080 --> 00:12:05,520
It could be coming from the way your body, the way

236
00:12:05,520 --> 00:12:07,440
your brain is functioning, the chemicals,

237
00:12:07,440 --> 00:12:10,560
and whatever substances you've been taking,

238
00:12:10,560 --> 00:12:14,480
the different foods that don't react well and so on.

239
00:12:14,480 --> 00:12:16,680
It could be coming partly from the past.

240
00:12:16,680 --> 00:12:17,960
It could be coming from the future.

241
00:12:17,960 --> 00:12:19,960
It could become external influences,

242
00:12:19,960 --> 00:12:22,880
sounds that you hear, or even angels.

243
00:12:22,880 --> 00:12:23,760
This is what they say.

244
00:12:23,760 --> 00:12:26,000
There could be spirits around them,

245
00:12:26,000 --> 00:12:29,520
because you have strong concentration.

246
00:12:29,520 --> 00:12:31,440
You're put into a state where you might be

247
00:12:31,440 --> 00:12:34,880
able to similar to meditation, where

248
00:12:34,880 --> 00:12:37,280
people practice meditation, go into great states

249
00:12:37,280 --> 00:12:39,600
of concentration, and are able to see, and hear,

250
00:12:39,600 --> 00:12:42,480
and experience things that we can't analyze.

251
00:12:42,480 --> 00:12:44,960
So it can come from all sorts of different sources,

252
00:12:44,960 --> 00:12:49,440
and it's not nearly as reliable as a meditation, for example,

253
00:12:49,440 --> 00:12:50,080
in any way.

254
00:12:50,080 --> 00:12:53,440
If you want to predict the future, go and practice meditation.

255
00:12:53,440 --> 00:12:55,640
If you want to understand about your past

256
00:12:55,640 --> 00:12:57,560
and what sort of things you're repressing,

257
00:12:57,560 --> 00:13:00,640
or practice meditation, if you want to see angels,

258
00:13:00,640 --> 00:13:02,720
and spirits, and so on, practice meditation,

259
00:13:02,720 --> 00:13:05,520
there are different meditations for this.

260
00:13:05,520 --> 00:13:10,040
But as I said, when you're dreaming,

261
00:13:10,040 --> 00:13:12,200
your dreams will show you something about yourself,

262
00:13:12,200 --> 00:13:13,280
about how your mind works.

263
00:13:13,280 --> 00:13:17,400
Because as I'm told, a perfectly enlightened person,

264
00:13:17,400 --> 00:13:21,520
a person who's done away with all of their mental defilements

265
00:13:21,520 --> 00:13:25,240
won't dream when they sleep, they sleep soundly,

266
00:13:25,240 --> 00:13:28,320
and they sleep mindfully.

267
00:13:28,320 --> 00:13:32,360
So actually, there's some sort of clarity of mind

268
00:13:32,360 --> 00:13:33,880
during the sleep, and when they wake up,

269
00:13:33,880 --> 00:13:36,360
they're fully refreshed and rested.

270
00:13:36,360 --> 00:13:40,760
But in regard, it's just important that, like anything,

271
00:13:40,760 --> 00:13:44,800
we don't follow them, and we don't project on them.

272
00:13:44,800 --> 00:13:46,000
A dream is what it is.

273
00:13:46,000 --> 00:13:47,680
You had that experience in your dream.

274
00:13:47,680 --> 00:13:50,480
It doesn't mean that it's going to happen in the future,

275
00:13:50,480 --> 00:13:53,520
or that somehow it has some special significance,

276
00:13:53,520 --> 00:13:56,840
or someone's trying to tell you something, or so on.

277
00:13:56,840 --> 00:13:58,760
Even if it's an angel or a spirit trying

278
00:13:58,760 --> 00:14:00,600
to tell you something, or even your mind trying

279
00:14:00,600 --> 00:14:03,640
to tell you something, that doesn't mean you should follow it.

280
00:14:03,640 --> 00:14:12,480
Because the source of the dream may still be full of delusion

281
00:14:12,480 --> 00:14:14,880
and misunderstanding is well and lead you on the wrong path.

282
00:14:14,880 --> 00:14:19,280
So dreams can be karmic, and that's

283
00:14:19,280 --> 00:14:21,160
been the last thing that I would say is that this

284
00:14:21,160 --> 00:14:24,040
should be a caution for us, that we can't always control

285
00:14:24,040 --> 00:14:25,840
our minds in the states like dream.

286
00:14:25,840 --> 00:14:29,560
But even in our waking states, dreams

287
00:14:29,560 --> 00:14:33,360
are just another reason for us to work on our minds,

288
00:14:33,360 --> 00:14:37,000
because otherwise, we're going to, when we fall asleep,

289
00:14:37,000 --> 00:14:37,680
we're going to dream.

290
00:14:37,680 --> 00:14:41,120
And what we mean by creating karma, even when we dream,

291
00:14:41,120 --> 00:14:44,280
is that we're building up the tendency to act in that way.

292
00:14:44,280 --> 00:14:47,200
So the dreams are reinforcing these emotions,

293
00:14:47,200 --> 00:14:49,600
reinforcing our fear, reinforcing our stress,

294
00:14:49,600 --> 00:14:52,360
reinforcing our anger, reinforcing our lust,

295
00:14:52,360 --> 00:14:55,360
reinforcing the states that we're trying to do away with.

296
00:14:55,360 --> 00:14:59,920
So as they can anyway, you can also have positive dreams

297
00:14:59,920 --> 00:15:03,560
where you have love and kindness towards other beings

298
00:15:03,560 --> 00:15:04,680
as possible.

299
00:15:04,680 --> 00:15:05,880
But generally, you'll see the things you're

300
00:15:05,880 --> 00:15:08,160
clinging to when you dream.

301
00:15:08,160 --> 00:15:11,080
And so it's an example of what we have to get rid of.

302
00:15:11,080 --> 00:15:15,040
And it's also a reason for us to, in a sense,

303
00:15:15,040 --> 00:15:18,240
be concerned and take our meditation seriously,

304
00:15:18,240 --> 00:15:21,760
because otherwise, this is the opposite of meditating

305
00:15:21,760 --> 00:15:23,720
where you're not mindful.

306
00:15:23,720 --> 00:15:25,720
And you will develop these states,

307
00:15:25,720 --> 00:15:29,160
and then we'll become more pronounced.

308
00:15:29,160 --> 00:15:31,560
So I hope that helps and answers the question.

309
00:15:31,560 --> 00:15:33,000
Thank you for tuning in once again.

310
00:15:33,000 --> 00:15:36,560
This has been another episode of Ask A Month.

311
00:15:36,560 --> 00:15:38,600
This is the first time using this new microphone.

312
00:15:38,600 --> 00:15:40,920
So I've got a microphone I hope it works.

313
00:15:40,920 --> 00:15:46,360
I can sit further away from the camera and let you see more

314
00:15:46,360 --> 00:15:47,600
than just my face.

315
00:15:47,600 --> 00:15:49,600
So thanks for tuning in, all the best.

