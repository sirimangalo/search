1
00:00:00,000 --> 00:00:09,280
Okay, so I've been sent some info by someone to like or sign various petitions against pages that show animal cruelty

2
00:00:10,320 --> 00:00:15,720
Against military action against the monetary system, etc. We have social website

3
00:00:16,560 --> 00:00:24,320
Please could you advise on how one should respond but for now I have offered back a link to the how to meditate series of video

4
00:00:24,320 --> 00:00:28,600
I'm starting to get the feeling that

5
00:00:32,080 --> 00:00:34,080
No, it's not the right way to start this

6
00:00:40,240 --> 00:00:48,160
I don't know that I can answer everyone's questions. I don't know if I can really answer all questions. Which should I do in this?

7
00:00:50,240 --> 00:00:52,240
situation

8
00:00:52,240 --> 00:00:56,560
For I think a couple of reasons at least one that that a

9
00:00:57,520 --> 00:00:59,520
Lot of them have context

10
00:00:59,520 --> 00:01:02,320
You know it how you respond to someone

11
00:01:03,280 --> 00:01:06,960
Depends a lot on who they are what they're like

12
00:01:10,240 --> 00:01:12,240
and

13
00:01:13,960 --> 00:01:19,440
Know what your duty is and what you're you know if you work for Facebook if you work for a social network for example

14
00:01:19,440 --> 00:01:22,320
You might have a different duty or if you

15
00:01:23,680 --> 00:01:26,880
You know depending on your situation as well

16
00:01:28,800 --> 00:01:30,800
and the other one is that

17
00:01:32,560 --> 00:01:35,240
These questions are very I mean the answer is

18
00:01:36,240 --> 00:01:38,240
answer a lot of these is

19
00:01:38,800 --> 00:01:44,800
Quit which you quit your job quit school drop out become a monk live in the forest and a star

20
00:01:44,800 --> 00:01:50,520
Then you can then then when people send you things you can say well, I'm a monk so it doesn't concern me

21
00:01:52,080 --> 00:01:54,240
So what I mean to say by that is that

22
00:01:55,680 --> 00:02:01,840
You know the answers that I would give to all two questions like this are not really going to be the

23
00:02:02,560 --> 00:02:03,760
being

24
00:02:03,760 --> 00:02:05,760
the final answer

25
00:02:05,760 --> 00:02:10,160
You know and what I mean is that there's just going to be an endless stream of questions like this

26
00:02:10,160 --> 00:02:14,800
What do I do in this situation would because you're living in the complex complicated

27
00:02:16,560 --> 00:02:18,560
Environment surround you know

28
00:02:18,800 --> 00:02:23,520
Constantly bombarded by things that don't really make sense and don't have easy answers

29
00:02:24,080 --> 00:02:26,080
That's just a general comment on

30
00:02:26,080 --> 00:02:29,760
Questions of the nature. What should I do in this situation?

31
00:02:30,480 --> 00:02:37,280
I mean if if you're asking me what should you do as a monk in the what should you do when you're meditating in the forest in a tiger

32
00:02:37,280 --> 00:02:43,440
Starts growling at you. For example, then this is something that I think well. This is this is really what we have to deal with

33
00:02:45,280 --> 00:02:52,480
But I'm gonna I'm gonna throw a caution out there against questions of the nature of which should I do in such a

34
00:02:53,360 --> 00:02:55,600
situation because I get a lot of them and they don't really

35
00:02:57,040 --> 00:02:58,240
feel

36
00:02:58,240 --> 00:03:00,240
Totally comfortable

37
00:03:00,240 --> 00:03:08,400
I'm answering but I can answer them, but I don't feel like the answers are really all that satisfying because they're not really the final answer

38
00:03:08,400 --> 00:03:09,920
final answers

39
00:03:09,920 --> 00:03:13,280
Quit leave get out get a run away

40
00:03:14,240 --> 00:03:20,240
No, find a way to to simplify your life so that you don't have to deal with such complex situations

41
00:03:22,080 --> 00:03:24,080
Um

42
00:03:24,080 --> 00:03:29,840
But being sent stuff. I mean, I just don't reply mostly. I don't reply to so much of the things people send me

43
00:03:30,960 --> 00:03:34,560
Which um, you know often feels a little bit a little bit

44
00:03:36,000 --> 00:03:40,800
Unpleasant, you know people often send me questions and they're valid questions

45
00:03:41,440 --> 00:03:43,840
Send me in about questions in the email

46
00:03:44,560 --> 00:03:50,640
But I've developed some kind of natural filtration system in my mind that I just most of them

47
00:03:50,640 --> 00:03:55,360
I just don't answer and then people send me a second one asking me why I didn't answer. I don't answer that

48
00:03:56,240 --> 00:04:00,960
It says send me a third one and really start complaining then they start thinking okay, okay time to tell them

49
00:04:01,520 --> 00:04:04,080
Explain to them that I just don't answer most of my emails

50
00:04:06,320 --> 00:04:09,200
Because I'm among so I've got an excuse

51
00:04:09,200 --> 00:04:18,160
Um, when you're living in society, you do kind of have a responsibility, you know

52
00:04:19,200 --> 00:04:24,000
To I mean we all have kind of a responsibility to our fellow humans

53
00:04:24,640 --> 00:04:29,120
Even I consider that in some sense. I have to help people who are looking for help

54
00:04:33,840 --> 00:04:35,840
But um

55
00:04:35,840 --> 00:04:41,920
I don't know. I guess if you're asking about what are my thoughts on things that are against things against

56
00:04:42,720 --> 00:04:47,200
Animal cruelty against military action against the monetary system

57
00:04:47,840 --> 00:04:53,200
I believe in in a lot of the principles. I just don't really believe in protesting. I believe in in

58
00:04:54,000 --> 00:04:56,000
discussion

59
00:04:56,000 --> 00:04:59,440
Um, but I think that includes discussion with people who

60
00:04:59,440 --> 00:05:05,440
Always people who who support such things. You know if people who support the monetary system

61
00:05:05,440 --> 00:05:07,440
I think debating with them is very

62
00:05:07,760 --> 00:05:09,760
educational I'm

63
00:05:10,320 --> 00:05:16,320
If I had the time I'd be you know interested in these sorts of debates of course you need to study and you need to that's the problem

64
00:05:16,320 --> 00:05:18,320
Really is there so many issues

65
00:05:18,560 --> 00:05:20,800
How do you know that they're all wrong right?

66
00:05:22,240 --> 00:05:24,240
So these three are pretty

67
00:05:24,640 --> 00:05:27,440
Well, the first two are pretty clear cut animal cruelty most

68
00:05:27,440 --> 00:05:31,600
Developed people you know can especially Buddhists can

69
00:05:32,960 --> 00:05:34,960
readily agree that it's wrong

70
00:05:34,960 --> 00:05:37,440
Military action. We readily agree that it's wrong, but

71
00:05:38,400 --> 00:05:44,400
You still have to study in order to realize that right so to know that animal cruelty is wrong you have to have done some

72
00:05:45,120 --> 00:05:51,760
At least some study and analysis better yet have done some meditation to help you see clearly that it's wrong

73
00:05:51,760 --> 00:05:58,640
Military action a lot of people think can in certain circumstances is necessary is ethical

74
00:05:59,280 --> 00:06:01,280
ethically required

75
00:06:01,440 --> 00:06:05,200
That non intervention is unethical for example

76
00:06:07,040 --> 00:06:11,040
So you have to study up on that and you have to be clear study either through

77
00:06:11,040 --> 00:06:18,240
Through study book study or through meditation practice to get your mind clear in regards to to that one

78
00:06:18,240 --> 00:06:22,560
The monetary system is very complicated, you know, I don't even know

79
00:06:23,440 --> 00:06:27,040
I mean, I have my thoughts on it and I'm I'm not really pro money, but

80
00:06:29,040 --> 00:06:31,040
You know it's such a complicated issue

81
00:06:31,760 --> 00:06:36,480
What does it mean to say you're against the monetary system that you want tomorrow to stop using money?

82
00:06:36,480 --> 00:06:41,760
Of course there'd be chaos right does it mean that you want to move in this direction or is it some

83
00:06:42,560 --> 00:06:45,120
some website that actually has a plan to

84
00:06:45,120 --> 00:06:50,960
Encourage sharing and and and cooperation as opposed to competition and

85
00:06:55,280 --> 00:06:57,280
Capitalism

86
00:06:59,280 --> 00:07:06,720
Or is it just someone is saying down with this down with this and and just getting angry about things right pointing out how awful people are right

87
00:07:07,520 --> 00:07:11,760
Just all these corrupt, you know, expose the corrupt business people

88
00:07:11,760 --> 00:07:14,880
It doesn't really do that much you become part of the problem

89
00:07:15,520 --> 00:07:21,040
When you fight against something you become part of I guess that's a good answer to this is that I would from my point of view

90
00:07:21,040 --> 00:07:23,040
When you fight against something you become part of the problem

91
00:07:26,000 --> 00:07:28,000
You can't win by

92
00:07:28,800 --> 00:07:32,240
You can't really fight fire with fire you have to fight with patience

93
00:07:32,560 --> 00:07:36,800
You have to fight with truth. You have to stand your ground. You have to say what's wrong

94
00:07:37,840 --> 00:07:39,840
but

95
00:07:39,840 --> 00:07:41,840
We should never get angry and you should never

96
00:07:42,800 --> 00:07:44,800
become

97
00:07:44,800 --> 00:07:47,600
Militant about it us and them and on

98
00:07:51,360 --> 00:07:53,360
So how should you respond?

99
00:07:53,680 --> 00:07:59,040
Kindly respond with love. I think it's the same as the proselytizing answer give them a muffin

100
00:08:00,240 --> 00:08:02,240
It's gonna be my answer to everything

101
00:08:02,240 --> 00:08:12,240
So send them a muffin

