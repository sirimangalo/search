Good evening, everyone broadcasting live December 29th, 2015, we have a little over two
days left in the year. I don't worry, there's another year to come. I had an argument with
my youngest brother about rebirth, stubborn. He said,
I can't remember. We'll see who's right when we die. We'll see who's right. It's going to be a shame for him
if there are consequences to his actions. He can't escape them by dying. He said,
well, yeah, if I'm right, you'll have to deal with the consequences. If I'm right,
and I still don't know, actually, if you're right, there are no consequences. If you're right, it'd be
great. It means we can do whatever we want, but it's quite a dangerous, quite a dangerous belief,
belief that belief in death is a dangerous belief because of the consequences of it. Even in
this life, the consequences are less likely to do good deeds, more likely to do evil deeds. Even if
it was true, it would be better for us to believe that there were consequences. It would be
better for the world. That's a funny situation. If it were true that when we died, there was nothing.
It would still be better for all of us and for the world and for society, for us to believe that there
were consequences. Before I left this morning, I said, so we'll have a safe travels, and I said,
thank you. I said, so I'll see you. If not in this life, then I'm in next. We laughed. So I had one
announcement today that seems a lot of people didn't know about the appointment schedule that we
have. If you go to meditation.ceremungalow.org, there's a meet page, and it's got slots where you can sign
up for a meditation course. We use Google Hangouts, so you have to make sure you've got Google Hangouts
installed and working, and then if you show up at the right time, you can meet with me, but you must be
practicing at least an hour of meditation per day. We try to get you up to two hours, and you have to
keep with this preset and stuff, but it's for people who are serious about the meditation practice and want
to go the next take the next step. Right now we have nine slots available. Still, I just got
back from Florida today. I'm not planning on sticking around too long this evening. Make a short
night of it. Do you have any questions? We have questions. We have lots of questions, Bante.
Let's skip to the good ones. Okay. Regarding meetings, is it required to have a camera and a
microphone to make an individual meeting? I have social anxiety, so it's very difficult for me,
and my English is bad too. Thanks. Yeah, we can do it just. You need a microphone absolutely,
but I'd like to have a camera. I think it's important because normally you would come into the
room and meet me, and we're trying to make it as close to that as possible. I really would
like for you to have a camera. Every time I do sitting meditation, I seem to keep dropping off
to sleep. This can be any time of day apart from walking meditation. What can I do to help with
my sitting meditation? Well, walking meditation is good. What's wrong with that? You're mindful
tired, tired helps, but probably has a lot to do with your lifestyle. You have to put up with
falling asleep because of the state of your mind is in. During sitting meditation, I find it
hard to knit thoughts that arise and sees quickly while I'm noting primary objectives. However,
I am aware that this happens and it is interrupting my concentration. How it happens is that while I'm
waiting for the rising, I thought I'll manifest and see and I will go on noting the rising.
My question is, should I in this case be noting thinking, thinking after the fact,
or should I note distracted? If so, how many times should I note before returning to my objectives?
You can, just once, where you can ignore it's knowing a couple of times.
There's no, it's not magic or something. Just do it as it seems appropriate.
Knowing would be knowing that you were thinking.
Is there a certain time of day in which meditation is best to be practiced in the morning,
for instance? How would you convince a non-butters that reincarnation is real?
Is there a way one can know who one was in a past life and thank you so much for taking the
time to answer? Yeah, I'm going to skip that one. Okay, it sounds like you've already had that.
I just wanted to say about consequences. You don't even have to wait until the next life
for the consequences. I mean, you feel them right now when you try to do anything.
Yeah, I was arguing that with the many said, I get that. He said, my younger brother said,
he knows that, but he said, but then I said, but you think that there are no
content in the end when you die, you get that Scott friends. When I'm sitting, I know sitting,
sitting, but how far do I need to go? Do I need to be mindful of the pressure below my butt
or just today or just know that I'm sitting? Is sitting a concept? When my abdomen is rising,
I note rising, rising, but do I need to follow the rising of the abdomen at every position?
Thanks. Yeah, you should follow the rising from beginning to end. You're not just saying,
and it's not just words in your head. Sitting as well, sitting is an expression of the
sensations. So yeah, you should be aware, but you don't have to focus on it. The only reason
you know that you're sitting is because of the sensations in your back and in your bottom.
Is there a difference between mindfulness achieved during meditation and mindfulness during
everyday activities? Is mindfulness achieved in formal meditation practice more beneficial?
Thank you, Montay.
Mindfulness is mindfulness.
Hello, Montay. I'd like to start off by saying thank you for all your teachings and dedication.
They truly have helped me put things in better perspective. I'll be taking a couple of college
classes in a city called Sioux Falls, South Dakota, starting February 2016. And I'm thinking of
forming a small meditation group on campus with interested individuals. If you'll be willing to meet
with us for about an hour or less once a week via Skype or another form of video communication,
I think that was a question. I'm not certain of the number of people who will be interested
in such a group, but if any, it would be wonderful to have you guide us along this path to freedom
from suffering.
So I think the question was would you be potentially able to do such a thing?
I don't know. Probably not.
I'm doing a lot already.
You just go according to the booklet and people want to learn more. They can contact me,
they can do a course.
They can join a room here as well.
Great that you're doing and I think I want to encourage you in it, but I don't think I'd be
I'm already doing too much.
Hi, Bunte. I have a job interview in one week. I study a lot when I need to talk and have difficulty
to breathe. Do you have any advice besides noting? And is it a good thing to imagine the interview
beforehand and observe my reaction? Thanks.
No, I mean, I teach meditation, so you know what I've got to offer. If it helps, take
it. If it doesn't help, leave it.
I guess this is a question. Do you know that you're just the best?
I think that was a compliment, but I had a question mark.
We're all worthless.
Is it better to meditate in a group rather than alone? If so, why? Thank you, Bunte.
You're always alone. You've never been a group.
But more much of meditation is an artifice. It's artificial.
So there's lots of artificial supports for meditation and one of them is having a teacher,
being in a group. These are artificial supports. So yeah, I think they can support your meditation.
Providing encouragement and so on. Lots of things that can support your meditation.
I'm sitting every day in making progress. Is there any benefit in doing your one-on-one course?
Or is it just for those who need advice? Thank you.
Why, if you're just practicing according to my booklet, that's only the first step.
And so yeah, we take it through the steps and we guide you through the course.
So I would say there's a lot of benefit in taking an online course.
It's just the very beginning.
And with that, you've made it through all the questions.
So we have tomorrow. We might do Dhamma Padha probably.
Should be okay.
And then on Thursday, we have the last day of the year.
I'll be here. We'll see who shows up.
Thanks for your muffins, Bhante.
What?
Thanks for your muffins, Bhante. Are you giving out muffins?
No.
Okay.
Okay.
Muffins.
Someone thanked you for your muffins.
I don't get it.
You don't get it.
I think it's a joke.
Maybe.
And anyway, good night, everyone. Good night, Bhante.
See you tomorrow.
Thank you.
Yeah. Thank you.
