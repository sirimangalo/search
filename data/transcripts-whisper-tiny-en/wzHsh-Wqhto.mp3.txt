You
You
You
You
You
You
You
You
You
Good evening everyone and welcome to our live broadcast so many buttons the first I can remember to push them all
You
Tonight we're looking at a good through any guy in book of four is suit of 37
Another one of my teacher's favorites
It's nice to go through these and find the ones that he used to teach or perhaps still even teaches the known as frequently
And the uppery honey and dumb I put a honey
They're dumb ones actually I'm not sure if this was one of them he used to teach
Yeah
Yeah, no, I think this is actually before a pretty honey and I'm
Parihani a parihan Parihan Parihan
Parihana
means to decline
I
Mean
Very hard
Pariha
Means decrease
So these are dumb ones that do not cause one to decline or
Prevent one from declining
This is of course an important aspect of spiritual practice
And that we continue to grow that we continue to progress
And we don't stagnate
And that we
And don't lose the path
That we don't fade away that we don't
Fall away from the path
It's very easy to become discouraged and to lose our way
Through doubt or
distraction
Through attachment to worldly pleasures and so on
So many things that cause us to
Lose our way
And hence decline
So these are four dumb ones that
Prevent that
That keep us on the path
These will dumb ones to know
And again another set that it's good for us to know
If you're not writing these sets down at home, I'll get better start because
These are good things all good things to know
But I guess you've got them on YouTube if you ever want to go back over them
So what are the four?
And not only that the Buddha said nibhana Santi K
nibhana Santi K
nibhana seva Santi K
And not only that one is barely in the presence of nibhana or in the neighborhood or close
close to freedom
In the one actually accomplishes these
Well, it does well not fade away, but one is on the doorstep of
True freedom for suffering
So what it is for got the maids up to me
Number one see the sampano
One is
Accomplished in ethics or ethical behavior morality
Complished in their abstention from unwholesome activity
Number two
Inrya Sugutadwara Ohoti
They are
They are people who they are ones who guard the doors of the senses
Those doors of the senses are guarded
Number three bhodja nibhata nibhata nibhata nibhata nibhodi
They are those who know
moderation in
regards to food
And number four jagariyang Anuyutohu
And those who are
devoted to
wakefulness
vigilance
alertness
Is for are the for a parihanyanam you accomplish these
It's another way of saying another way of looking at the path
and another set that
describes the
factors that protect you
So it doesn't mean to be virtuous. It means to basically to
Generally means to keep rules
To refrain from those actions and speech that are immoral
In the world they sense on the most coarse level. It means not killing stealing cheating like
Taking drugs and alcohol that kind of thing
On a more refined level it means any not doing anything with greed or anger or
Or decision and so for a meditator this even means not
Not walking angrily not speaking angrily or greedily
Not letting our actions or our speech become unguarded
So that everything we do is mindful and doing everything unconfused
When you eat to eat with a clear mind when you speak to speak with a mindful mind
When you lie down to sleep to lie down to sleep with a
Mindful mind
In mind that it's clear and alert and remembers itself in mind that it's in the present moment
How does one guard the sentence faculty is it doesn't mean the guard your doors of the senses?
Again, we've been over this one many times, but
It means when that's seeing we see doesn't have anything to do with
Preventing yourself from seeing certain things because the things that you see are not the problem
Whether you see something beautiful or you see something ugly
It's the mind that causes the attachment or immersion
And so you don't get caught up in the particulars seeing is just seeing
Hearing is just hearing sensing is all just sensing all the senses
Even thinking is just thinking
Because without that that's where the mind gets into trouble
If we leave the mind unguarded the Buddha says bad unwholesome states of longing and dejection
went very well invade the mind
and take it over and
And so we guard we guard not against seeing or hearing or smelling we guard against the particular
Against judging
In essence we're guarding against our own minds
We're not guarding the doors to stop the senses from coming in
They're stopping regarding the doors to stop our own minds from getting out
It's a big difference. There's normally we want to stop things from getting in. We want to stop seeing or hearing
We think the answer is to just avoid certain experiences
It's not both a Buddha meant by guarding the doors. It's more like a prison
Guard your mind keep it in check keep the animal tame
Keep the beast from getting out. I'm getting loose
Let's seeing just be seeing teach yourself teach yourself how to see things just as they are
Number three moderation and eating again here. We see how important food is in the Buddha's estimation
The practice of eating is a very big part of our life. It's the one thing that we
require to live that it's our
It's our illness hunger is our illness and it's our
Strongest attachment the one thing we require
And so it easily becomes a
Object of abuse. We abuse food. We abuse our bodies with food. We eat too much. We don't eat enough
We eat poorly
Unhealthily
We don't eat for the right purpose. We eat for amusement intoxication
physical strength or beauty
When in fact food is something very simple. It's a medicine that keeps us alive
We don't eat it. We die or at least very good get very sick
So that's all it's for when you start to think like that. Do you realize how little food and
How simple food you need?
Simply need food to keep your life. You don't you'll die. Okay
So I'll eat a little bit of food and that keeps me alive and now I can get on with my work
This is from the Buddha. This is one of the important practices
You mean you could extrapolate this to all sorts of things. There's so many more things that we cling to but food is really
Near the top if not the top of those things that we cling to
Or it's those state one of those things that is so much a part of our lives that we must
We can't remove it from our lives
You can remove all the other things you're attached to when you go to do a meditation course, but food is one of the few things that you
can't escape
So of all the things that we can't escape. It's probably the most
significant in terms of cultivating attachment in the version
And so once we get into the practice
We have to look at food as a very dangerous proposition eating
eating is like a
confrontation with the armies of Mara
When you go into the kitchen to make your food you have to think like you're going to battle
You're going to war
Going to war against your defilements
So you have to prepare yourself with the arm yourself
And you have to defend yourself
The food war food
The battle for food
Let food just be food. No, that not be mental food
Let it not feed your ego or feed your desires or feed your
Parcealities and aversions
Let it just feed your body in the thing more
Or let it feed your practice in the sense of being simply for the purpose of allowing you to continue doing the right thing
Number three
Number four is being alert
Being devoted to wakefulness being awake and so the Buddha talks about here's one of the
Examples of where the Buddha talks about a regimen for meditation for those of you who are here doing a course and
Want to have some confirmation that what you're doing is
Very much in line with it the Buddha taught Buddha said during the day you walk back and forth and sitting
and
Purifying your minds
I'm destructive of obstructive qualities. That's what the Buddha said. So what he is supposed to be doing walking back and forth and sitting
This is what you're doing
And how do you describe the practice of what you do is?
I want me he done me he did done but he's so deep he purify your mind
I'm obstructing
qualities
All the things that are causing you stress and conflict and friction and trouble all of those you
cleanse from your mind
And then in as far as the night goes you split the night up into three parts and
In the first part you do the same you walk back and forth and sit
So there's no variation here
In the second watch of the night you lie down. So that's about four hours
Well, we allow six hours here maximum, but if you can get down to four that's great eventually we'd like you to get down to four of it
all in good time
And when you do lie down you lie down in a mindful posture and
Rather than thinking about going to sleep you actually think about
Awaking awakening
So you keep in mind the time when you're going to wake up and that's all you think about as though
making a
A concession that the lying down is simply a
an admittance of your
Love the need to sleep and that's all there's no desire no attachment your
Desire is to get up as soon as possible and go and practice
So you make a note okay in four hours at this time. I'm going to wake up for six hours whenever you're sleeping
And then you get up and you don't try to go to sleep you lie down and you try to meditate and be as mindful as you can
And just the when you do fall asleep
To find that you're actually quite mindful at that moment and
You wake up alert
refreshed and ready to
Continue on your path
Striving for freedom
And then in the first in the last watch of the night again you walk back and forth and so
So it means staying awake
Being devoted to wakefulness because sleep is I suppose the other one food and sleep
These are the two big ones that you have to contend with as a meditator
Food is a bit of an escape sleep certainly as an escape from all the trouble that comes from meditation practice on the difficulty on the challenge
Sleep is not very challenging
Especially when you've been meditating on the all you want to do is lie down to sleep
But it's over time as through the days you start to see that all sleep doesn't really help
It doesn't really make me feel better. I didn't don't feel more at peace with myself because of my sleep
It's actually very much an addiction
And that's all it's not really a cause for satisfaction
And so eventually the meditator begins to be inclined to devote themselves to wakefulness
They don't wish to sleep
They wish to be awake because they see all the fruit and the benefit that comes from being awake
The good that comes from
So these are the four aprehani Adam is for
render a big coup
incapable of decline abubbo abubbo
parehana
Un incapable of decline nibana save us and decay and in the neighborhood of nibana
The medist wonderful verse that I'll just read out
Sile patitator bikhu indriyasu chasamu to bow genami to matanyu jagariyang anu yunjati
So this is repeating them
When it was abikhu who is established in morality or ethics
And has the faculties well guarded or restrained
and who knows moderation in regards to food
and is dedicated to wakefulness
a one-way haariyatapi thus one dwells with effort exerting themselves
a horam horatamatandi to both by day and night
active
wakeful or energetic both by day and by night
bawayang kusalang dhamma nyoga kimasapatya
These are no whatsoever
bawayang
okay developing wholesome qualities to attain security of bondage
bikha bodhisatt
right to attain
batiya to attain freedom from bondage
freedom from slavery
abba madaratobikhu abikhu delights in heedfulness
in non-intoxica not being intoxicated
and being heedful and vigilant
bamade by a da ciwa
or shimabhiwa would visit
by a da sāmi
by a da sāmi
bamade by a da sāmi
one who sees fear, fearfulness
sees the danger
in negligence
the danger in letting your mind wander
letting your mind get caught up in
base thoughts, course thoughts, distracted thoughts
get caught up in the world
abba bawayang a sacha wand is incapable of declining bāmi sāmi sāmi tiki
and in the neighborhood of nyvāga
so
more dhamma to think on
to reflect upon and to
use as a mirror for ourselves
the great things about these dhamma, the greatest
thing about these dhammas they provide a mirror
when we hear the dhamma we think about ourselves and we compare them to ourselves
do i have these and it reminds us where we're
where we still need to work
it reminds us to continue working
and it gives us direction on how we should continue working
so that's the dhamma for this evening
now we move on to questions
i've been meditating for seven months now because i'm not breathing properly using my stomach
i focus on my stomach rising and falling, letting it be as it is without controlling the in and
out breaths noting the tension for what it is after my 30 minutes i feel great my stomach is
relaxed there's no tension no forcing of breath but as the day continues i feel the tension
come back and i start hurting again it's really confusing me do i always have to be mindful for
this habit of forcing to disappear how do i break this habit of forcing my breath at every
time of the day every moment it's not exactly forcing that's what it feels like and everyone talks
complaints about this it's the stress that your mind's inability to and it's a lot of things
there's so much in there that's all compacted into this feeling of control but it's
it's really just a feeling of stress and tension you can't as it turns out we can't watch
something without obsessing over it without having to having to force or or not control but
to regulate it and to make it stable so the unpredictability of it or the unpredictability of
unpredictability of the mind causes a stress and tension as we're sorely unequipped to deal with
the true nature of reality and so it causes this tension and stress now absolutely you're not
you know this this isn't something to be avoided this isn't something to be
fixed per se except that it is the problem and the answer or the solution is simply to see
that it's a problem and to teach yourself to just to see more clearly what exactly is it that
you're doing to watch your behavior and to slowly unlearn your behavior as a result i mean
you unlearn it because you start to see that it's causing you suffering that's what you're
starting to see but this is very much a part of who you are it's it's it's all based on your
habits of behavior in your life so when given a simple exercise like watching your breath
you get to see all about what you do wrong when you when you're given such an exercise or such
an object of focus the breath is a great is there before the stomach is there for a great
teacher to us because it allows us to see what we do to ourselves when you know whenever we
apply or mind to something if you can apply your mind and the breath is just going naturally as
it is without any stress or suffering then you can see that your mind is well able and your mind
is in a good state because when you put your mind to something it applies it peacefully
but that's not what we see most of the time that's clearly the the issue so all we're trying to do is
see our faults is to see the see what we're doing wrong see the faults in our behavior
so eventually it's going to you're going to see deeper than that if you're just seeing that it's
you trying to force it then you're not yet seeing the whole picture you're going to look
carefully and see what is actually happening because you're not just trying to force in fact that's
really vague what's really happening is perhaps you want it there's wanting there's a desire
and there's an aversion to the way it is happening there's worry there's fear there's doubt
there's all sorts of hindrances all mixed up and causing a lot of stress
so absolutely this is what we want to see this is the way the mind works this is the way the mind
learns is it sees what it's doing wrong and it begins to change all we're trying to do is give it
the opportunity to see but you don't break habits you just unlearn them if you could break
habits it would be more control and force and the fact that you're asking that is a good indication
that's something I can point out to you that you're the kind of person as most of us are
who wants to try and fix things control things be in charge and that's probably what's causing
you stressed with the stomach as you're trying to fix and control it so until you can give up the
need to try and fix your your fixing you're always going to be trying to fixing everything
until you stop trying to control your controlling you're ever going to be an uncontrolled
you're never going to be in person who doesn't control doesn't try to control thing
I remember you once said that the fold path is realized in three moments but I've also heard
that Dharma talks regarding the fold path in terms of a systematic process in other words under
right effort one is to cultivate positive emotions and to remove the five entrances it seems to me
it seems to imply to that when a negative emotion arises one counter-acts it with a positive
emotion or in the future regarding thoughts the Buddha said to replace annoying thoughts
with Dharma thoughts this approach contradicts the mindful noting process as to what's happening
in the here and now can you please say something on this the Buddha taught many different things
at many different times on many different levels so he taught things like mitt if you're angry you
should practice mitt doesn't mean that that's going to enlighten you it means it's a useful
tool it's one of the things that protects your practice actually but
the idea of doing different practices at different times in different situations is
on that level of being fairly rudimentary basic and protective rather than then core the core practice
is the practice of the Satipatana when you practice that you're cultivating the factors of the
fold path now that's one part of the answer is that you don't really have to go around like
okay today I'm going to practice effort or today I'm going to practice concentration or today
I'm going to do cultivate right view or that kind of thing doesn't really work that way
the fold path really only comes through insight meditation
in parts of it come from summit but they can't be considered to be the fold path
now as far as whether it's instantaneous or whether it's a process that's
that the point is there are two paths there's what called the pubangamanga which is the preliminary
path and what that means is you're cultivating the qualities of the fold noble path but aren't
there yet the noble path the arising in three moments means that you have all of those eight
meaning you've they're all you're on a path and this is more with the hateful
noble path is about it's describing all the qualities all the important qualities of the path
but it's one path it's not eight different parts eight different paths that you practice
first I'll go out this path then I'll go out that path it's one path that's what you have to
remember so you're not going to practice one and then the other you practice the path and
it that path has all of those eight factors so at that moment there's one moment where you
enter into the path and why it's called the path well why it's called the noble path is because
it is perfect you've you've perfected all four of those all eight of those
but why it's called the path at all it's because it's the one moment it is the one state
the only state in in samsara in this in the nature of the mind all the many different times
types of mind states that might arise it's the only one that is able to destroy the
defamments because the next moment those defamments are gone in fact the next moment has
the same qualities it's not called the path though but the eightfold noble path is that one
moment and this may seem very technical but it's important it's an important technical
distinction it's the one moment that destroys the defalment and we know this because the next
moment they're gone without that one moment they would never disappear but that next moment has
the same qualities why it's not called the path is because those defalments are already destroyed
they were destroyed in the last moment so that's what the eightfold noble path is now
may not seem all that useful for us and people like useful things and that's why we think of
the eightfold noble path in terms of something you practice gradually but what that is not the
eightfold noble path that is technically the pubangamanga the preliminary path which is perfectly
valid and that's what we're all practicing we're practicing the preliminary path once you get
to that one moment where you see things clearly you enter into the full noble path and that one
moment is enough to destroy the defalments so the preliminary path is like weaker versions of
all eight you're just getting there you know you don't have right view yet you don't have right
thought you're cultivating so I don't know if I answered everything in your question but
perhaps you can perhaps you can elucidate if you need more elucidation
but do those three moments have names it's not three moments it's one moment
thank you the magajita in fact there's four of them because there's four magas there's four pads
so it's a bit more complicated than that the eightfold noble path they're actually four of them
there's the sotapana and the sakadagami and the gami and our hand so there are four distinct magajitas
thank you magda I think we actually had this question the other night what object can we
meditate on to stop ourselves from falling into despair of another's terminal illness
we did it yeah you know it's I mean that's one of those questions where I would normally just
say you know meditation but I didn't I didn't go into a bit of detail didn't you did and I
believe that was last night's show which is still recorded the other two i'm gonna work
summers and recorded my interpretation of the anapatas anapanasati suta is that at least
some sufficient level of samadhi should be attained if not full absorption into the janas before
moving on to insect practice my interpretation of your methodology is that fixed concentration
samadhi and absorption should be avoided in order to stay focused on insight
by these interpretations correct and if so why not first learn to establish piti and suka
before moving into insight as the anapanasati suta suggests
you should never you should never
I think this really goes without you should go without saying that you should never
define Buddhism by what one suta says so you're saying based on the interpretation of the
anapanasati is that you should do a or you should do x well okay maybe first of all we
could say maybe according to the anapanasati suta but that's not the only thing the buddha
it's not the only way that the buddha time the anapanasati is just one suta and it's one way of
looking at things the anapanasati suta describes the practice of someone who will cultivate
samadhi first and then we pass it in it's probably the most common way the buddha time
but it's not the only way he taught and many times he would just teach insight meditation
but the point is that eventually you have to come out of jhana no matter what you have to come
out of your absorption and look at reality as impermanent unsatisfying and uncontrollable there's no
question if you know even a moderate amount of the teachings of the buddha that one has to see
impermanent suffering in non-self you can't see that practicing samadhi the samadha jhana is
don't allow for it they can't they're absorbed on a concept that concept is fixed and stable if
it weren't it it wouldn't allow for for that deep constant absorbed concentration how can you
enter into absorption when things are changing all the time when things are when your mind is
flitting from one thing to another for example but on the other hand how can you see impermanent
suffering in non-self when your mind is fixed and focused on the single object so the anapana said
t-sudhana goes into samadha first and then it starts to apply that concentration to be
passive no i think you understand that already but it's not the way i teach and the way we teach
is another way which is still very much in line with the buddha's teaching because the buddha
never really said would you know he never he never required the entering into the samadha jhana's
and you know that's a complex point to argue but it's it's quite backed up quite well by
both the sutas and the commentaries and we always turn modus few sutas that we turn to for that
but whatever that's not be dogmatic either way and so the way we teach or yeah the way we teach is
to cultivate concentration sure but with a focus on mindfulness we follow the sutatipatana sutat
which also has anapana sati in there sure but as much more focused on things like walking you know
and pain and mind states and emotions and senses it's got everything in there focus on reality as
the object of your meditation so fixed constant i do not say fixed concentration samadha
and absorption should be avoided but i do say that they have to eventually be left behind
so someday they should be avoided but it's just a shifting focus so rather than trying to absorb
the mind you try to see things clearly and that's everything rising and ceasing so as to your
actual question if you want to establish beat the ensuka by all means go for it i have no qualms or
no no problem with you doing that i'll be here if you ever want to come and practice with sutatana
if there is no consciousness in nibhana how can the mind know that it has happened
by the results
when there's a where it's called it's the 16th stage of no it's called patisankanya
you know it's a god
but you wake in the end but you wake in the end but you wake in the means you reflect
it's the time of reflection after it after arising from nibhana returning you could say
there's a reflection well i feel different i'm free from something happened something quite
profound happened and i'm free from certain defilements so you know five things you become aware
of the path that you followed the fruit the and you become aware of you become you reflect
and you reflect on five things you reflect on the path what happened there what happened to
cause it what was it that caused that experience the fruit there was an experience the cessation
the third is you reflect on nibhana there was nothing no more rising whatsoever
which is you know like reflecting on some on a non-experience because there was no
no a right arisen experience but something definitely happened
it seems in the intellect so it seems like nothing but you certainly are aware that something
happened even though there's no memory or of that and four years aware of that you reflect upon
the defilements that have been cut off and five you reflect upon the defilements that are
remaining now that fifth one of course doesn't occur for an arghund but for all the others
for so upon a second again again there's that kind of reflection
and that's what that's what you know what will happen after so reflect
hey how are you very curious on what your average day is like any interesting experiences
I'm fine thank you well like today was on a was was out of the ordinary today I had a visitor
but he comes once a month or so a monk from Toronto we sat around and we ate lunch together
we ate fruit I ate food it's in the evening I think so and he had a big breakfast
so he only on fruit we talked for quite a while and then we went to the food bank
because he was delivering food to the kids food bank nearby but you know regular things that I do
I teach early in the morning I teach at seven so before that people bring food and I teach in the
evening and I teach at night and I answer emails some not most I'm organizing a peace club
and a Buddhism club at the university I'll be going to school soon so that'll change my schedule
but other than that I do a meditation take a shower I eat I do laundry once in a while sometimes I
shave it's what I do interesting experiences I don't know every experiences to some extent interesting
when sleeping how do we balance focusing on the moment of waking up and noting the rising and falling
of the abdomen yeah I don't think you need to be obsessively repeating it in your mind
you just before you when you do lie down you make a determination that you're going to wake up
at a certain time I'm not convinced that you should actually lie there thinking about when you're
waking up because that's thinking about the future right it's not really useful
which I do that and then meditate to sleep it's of course seems it's much much more
doable oops I do do the wrong one did you see that one that I just deleted or I don't think so
okay we got an answered tab okay oh no hey they're not an order the answered ones are not
an order yeah they seem to um they're going according to how many we should see if we can get
the answered ones to go in order of when they were asked are they going in order of up votes
like they do on the question panel okay I don't even know if there was one that I missed
I think I deleted the wrong one okay well hopefully the person's still there and if your
question disappeared maybe you can resubmit it should we contemplate on the four protections
daily that is Buddha himself appreciating the nine chief qualities of the Buddha loving kindness
a little loathsome aspects of the body and death sure yeah
I mean that's a lot of what evening and morning chanting is about
so it becomes very ritualistic so you might want to make it a little more meaningful
yeah those are all good things
how do you deal with sleep in us during the midday
you note it under walking meditation
but try and note it you know when you feel sleepy try and focus on the sleepiness
and say tire and tire you find that you can overcome it sometimes
can one abandon habits by making a determination of not giving into the desire and observing the
attachment
well you only abandon habits when you start to see that they're useless
so determinations can be good helpful but they don't do the trick they don't help you abandon
and they don't cause you to abandon habits the only thing that causes you to abandon habits is
clear sight seeing things clearly as they are because that helps you see that those habits are used
sounds like you had a great day I have a weird question to ask you
I have a lot of anxiety naturally and I noticed that I get extremely attached to meditation
in an unhealthy way I get stressed extremely easily especially for someone young
it seems like meditation helps but there's some sort of phenomena preventing me from being calm
making it so I meditate all the time and going without it for a few days starts making me messed up
should I consider some sort of a psychiatrist
oh no just keep meditating no I mean meditation should become a part of your life
meaning when you're not meditating because you say you get stressed I don't know if you're even
practicing according to our tradition perhaps you're not if you're not then my first advice
because you're coming to me for advice is to read my booklet on how to meditate because the way
we practice here and therefore all of my answers and advice are going to be based on the teaching
in that book now if and when you do that or if you've already done that I'd recommend
and practice in that way but then also applying it to your daily life so when you're worried
or stressed you would say worried, worried, stressed, stressed and you're trying to be mindful
during your daily life when you're walking anywhere trying to say to yourself walking walking
when you eat, say chewing, chewing, swallowing but at the very least it would be aware of
your emotions during the time you're not meditating especially if you're doing meditation so well
that it's a refuge for you the other thing is perhaps not be so obsessed with being calm and
you have to learn to meditate on the calm by saying to yourself calm so that you don't get attached
to it because once you get attached to it then you can't deal when it's not present you become
dependent on it as you are relating so big part of it is to be able to note and let go of the
calm sometimes when I meditate I start to get a really strong terror I start to think that
nibana is actually complete suffering and that it should stop practicing how should I deal with
this it's quite overwhelming we all have irrational fears just learn to note them
I think you're all caught up on questions monthly
now you're not what is your greatest struggle as a meditator
yes sorry I'm not gonna answer questions about myself you've got one in there about my day
I'll go that far and tell you what I do during the day but nope sorry rule number three
no questions about me what we're rules number one in two bante I'm just guessing one of the
rules is no questions asking about things I don't know anything about
right like what do you think of all right of Krishna Murti and that kind of thing
I don't remember the third one I think there was a third rule though
well no non meditative questions right yeah maybe speculative questions
do you think there are aliens on another planet
sorry you're asking the wrong dude
well when you used to have the what was what was the older format with the
Askamunk it was Google Moderator and people would vote on questions and there was this one
question about how many minds are there in the universe and I thought it was such a great question
and I voted on it and I waited and waited and waited until it finally got to the point where
it was the top question and you were going to answer and your answer was I'm not answering this
question hey it waited so long
so for the person who just asked again are you enlightened that actually monks don't talk about
that there's there's actually rules for monks not to speak about their practice attainments
and so forth so that's why your question wasn't answered the first time round
one last question and then we'll go okay do you have any suggestions on places to stay on a meditative
retreat do you offer this service absolutely we have a meditation center but I would read my booklet
that I'm assuming you probably haven't read my booklet on how to meditate that's where I would start
you can find it in the menu here to this website you'll see a book a link in the side that says
guide in the side menu you should read that guide it's actually a book on how to meditate it's not
very long and if you like that then you're welcome to come and meditate with us if you want to
meditate with us you should go to our main site which is just SiriMungalow.org and check us out we
have right now four meditators staying here which is our capacity but you can book a time book
some dates and come and stay with us and that's right on the same website with how to meditate
book and different tabs up top just look for the one that says I think the schedule is visible
to people so you can check and see when there's room and then there's another tab that says
apply to meditate okay well that's all for tonight thank you all for tuning in thanks for
a good day everyone thank you Bantay good night
