1
00:00:00,000 --> 00:00:13,360
Good evening.

2
00:00:13,360 --> 00:00:34,880
So I was thinking about Chanda. Chanda means desire or interest.

3
00:00:34,880 --> 00:00:43,200
I mean something like contentment or being pleased with something, being happy with something,

4
00:00:43,200 --> 00:00:48,040
being comfortable with something, perhaps.

5
00:00:48,040 --> 00:00:53,640
And it's the first of the four Hindi pad that if you want to succeed in something you

6
00:00:53,640 --> 00:00:57,640
need Chanda, you need to want to do it.

7
00:00:57,640 --> 00:01:02,480
In some sense of the word, not that you crave it, I don't know if you crave something, you're

8
00:01:02,480 --> 00:01:04,840
much more likely to do it.

9
00:01:04,840 --> 00:01:07,440
You can't really crave meditation, unfortunately.

10
00:01:07,440 --> 00:01:21,080
I can't crave to be mindful it would be somewhat counter-productive.

11
00:01:21,080 --> 00:01:29,080
But you can be interested in it, it can be intent upon it, it's possible for you to want

12
00:01:29,080 --> 00:01:34,960
to do it, be happy doing it, be content doing it.

13
00:01:34,960 --> 00:01:43,240
Of course, you probably realize that it's also possible to be very discontent and unsure

14
00:01:43,240 --> 00:01:45,200
about practice.

15
00:01:45,200 --> 00:01:56,280
Often more discontent about the concept of meditating, the fact that I have to go and

16
00:01:56,280 --> 00:02:03,800
sit for an hour or walk for an hour, that's not a fact, it's an idea that's all in your

17
00:02:03,800 --> 00:02:04,800
mind.

18
00:02:04,800 --> 00:02:05,800
Right?

19
00:02:05,800 --> 00:02:07,320
But that's how we live.

20
00:02:07,320 --> 00:02:15,560
We put ideas in our mind, we build pictures, and that becomes our, it feels like that's

21
00:02:15,560 --> 00:02:25,920
our reality or we mistake that for a reality and makes it hard to, hard to do it.

22
00:02:25,920 --> 00:02:37,680
That needs to be done because the picture easily becomes an object of discontent, displeasure,

23
00:02:37,680 --> 00:02:42,320
or version even.

24
00:02:42,320 --> 00:02:55,720
But what I wanted to talk about tonight was some of the ways we can put a positive picture

25
00:02:55,720 --> 00:03:06,800
in your mind, to practically speak in we can fight, we can fight against or work against

26
00:03:06,800 --> 00:03:13,760
the negative image we have of having to do an hour of walking meditation or sitting

27
00:03:13,760 --> 00:03:22,920
meditation or having to stay here for another day or week or months or however long.

28
00:03:22,920 --> 00:03:35,440
Having to, having to live a simple life, having to be deprived of many things, we'd

29
00:03:35,440 --> 00:03:46,160
to preview of food, we'd to preview of entertainment, sex, we'd to preview of sleep even.

30
00:03:46,160 --> 00:03:53,920
And to live like that, how do you paint a positive picture when you're so deprived?

31
00:03:53,920 --> 00:03:59,920
I think it should be clear that it's possible.

32
00:03:59,920 --> 00:04:05,600
I mean a lot of what religion tries to do is evoke these religious feelings.

33
00:04:05,600 --> 00:04:08,800
It's maybe what the word religion means.

34
00:04:08,800 --> 00:04:10,840
Religion is something you're dedicated to.

35
00:04:10,840 --> 00:04:16,920
If it's your religion, it means it's the thing you're religious about, whatever that

36
00:04:16,920 --> 00:04:19,640
may be, it could be anything.

37
00:04:19,640 --> 00:04:26,040
So if we could be religious about mindfulness, it would mean we were quite attached to it,

38
00:04:26,040 --> 00:04:37,880
quite intent upon it, hopefully quite comfortable and keen on it.

39
00:04:37,880 --> 00:04:49,520
So the picture that we try to paint is of the importance of mindfulness, the importance

40
00:04:49,520 --> 00:04:58,080
of meditation, the importance of goodness, why it's important.

41
00:04:58,080 --> 00:05:10,640
And there are two ways I guess I'm doing that, one is talking about the benefits of it.

42
00:05:10,640 --> 00:05:16,320
Another is scaring the heck out of you about the detriments of not doing it.

43
00:05:16,320 --> 00:05:20,760
I realize that as I think about it that today is in the latter category, I don't know

44
00:05:20,760 --> 00:05:24,800
anything positive to tell you about mindfulness today.

45
00:05:24,800 --> 00:05:31,440
I'm just going to tell you all the negative stuff about being not mindful, how awful

46
00:05:31,440 --> 00:05:32,440
it is.

47
00:05:32,440 --> 00:05:33,440
I'll scare you.

48
00:05:33,440 --> 00:05:40,720
I've got the carrot and the stick, so today is the stick, which is unfortunate because

49
00:05:40,720 --> 00:05:45,400
I find more often talking about negatives, so let's address this, let's talk about

50
00:05:45,400 --> 00:05:46,400
time first.

51
00:05:46,400 --> 00:05:49,200
Why is Buddhism so negative?

52
00:05:49,200 --> 00:05:53,040
Why is it always talking about bad or anything else?

53
00:05:53,040 --> 00:05:58,760
I think there's too, I mean there's many ways of talking about that, but two things I could

54
00:05:58,760 --> 00:06:00,080
say.

55
00:06:00,080 --> 00:06:10,320
The first is that there's not much you can say about good, there's not much because goodness

56
00:06:10,320 --> 00:06:19,600
as it turns out is not pleasure, the good, what is good, what is valuable is not a pleasant

57
00:06:19,600 --> 00:06:20,600
feeling.

58
00:06:20,600 --> 00:06:26,960
It's something beyond that, so you can talk about it, there are things you can say like

59
00:06:26,960 --> 00:06:32,240
talking about peace, even happiness, because it is happiness, it's just you have to be careful

60
00:06:32,240 --> 00:06:46,120
not to mistake the happiness for a pleasant feeling, the coolness, the calm, the quiet,

61
00:06:46,120 --> 00:07:01,480
the clarity and the wisdom, many good things you can say, but it's often in relation

62
00:07:01,480 --> 00:07:11,400
to the negative, we talk about freedom from suffering, true happiness is the absence of suffering,

63
00:07:11,400 --> 00:07:19,800
the absence of defilement, the absence of those mind states that cause suffering, so when

64
00:07:19,800 --> 00:07:26,640
you get rid of all the bad stuff, that's when you find peace, so that's the other side

65
00:07:26,640 --> 00:07:32,440
of the coin, it's really the bad stuff that we have to focus on, why Buddhism focuses

66
00:07:32,440 --> 00:07:42,200
on negative is very similar to why a doctor focuses on sickness, on illness, so it's not

67
00:07:42,200 --> 00:07:48,800
meant to press your bum, your bum, your bum, yeah, it's not meant to make you feel bad,

68
00:07:48,800 --> 00:07:52,720
it's meant to focus our attention on what's important, we shouldn't mistake that, Buddhism

69
00:07:52,720 --> 00:08:00,560
is not trying to be miserable and unpleasant by thinking about bad stuff, it's about an

70
00:08:00,560 --> 00:08:09,200
honest look at what's wrong, an honest affirmation that something is wrong in the first

71
00:08:09,200 --> 00:08:16,080
place, right, as opposed to living your life, thinking everything's fine, and so what I wanted

72
00:08:16,080 --> 00:08:25,840
to talk tonight, talk about tonight was five reflections that I think may help to helpfully

73
00:08:25,840 --> 00:08:38,800
help to stir you up, stir up a feeling of time to make you want to be mindful, give

74
00:08:38,800 --> 00:08:45,960
your reason to be mindful, why am I here, why am I doing this, to answer any questions,

75
00:08:45,960 --> 00:08:53,880
so these are five reflections that are sort of thing anyone can and shouldn't reflect upon

76
00:08:53,880 --> 00:09:03,360
daily, and they give us a little bit of reminder of sorts of reasons why we should strive

77
00:09:03,360 --> 00:09:12,400
our best to become mindful, the white simple, so this talk may not be a long one, but there

78
00:09:12,400 --> 00:09:17,000
are five things that the Buddha said we should remember, so these are the five things,

79
00:09:17,000 --> 00:09:20,920
monks have ten things actually, it's another list that's probably good for meditators,

80
00:09:20,920 --> 00:09:25,760
but start with five.

81
00:09:25,760 --> 00:09:39,720
The first one, Jara Omhi, Jara Damumhi, Jara Anakitu, I am of a nature to get old, it is

82
00:09:39,720 --> 00:09:56,760
my nature to get old, to age, I cannot escape aging, so let's go through the moon, I won't,

83
00:09:56,760 --> 00:10:06,800
I won't go through the moon, so the first one is aging, that aging is a part of life,

84
00:10:06,800 --> 00:10:13,760
so the idea here is that what we're doing is something much bigger than a hobby or past

85
00:10:13,760 --> 00:10:30,000
time or something that you have to try once, it involves a bit of a philosophy, I guess,

86
00:10:30,000 --> 00:10:37,280
the reasons why we practice have more to do, have to do with more than just feeling good

87
00:10:37,280 --> 00:10:48,480
for a little while, it's about helping us find the right way to live our lives, to come

88
00:10:48,480 --> 00:11:02,520
to terms with samsara and to terms with the world, and so the first thing that we talk

89
00:11:02,520 --> 00:11:14,240
about is the facts of life, the reasons why we can't just go through our lives, doing

90
00:11:14,240 --> 00:11:22,880
whatever we want, enjoying central pleasure or ambition or whatever, when we talk about the facts

91
00:11:22,880 --> 00:11:32,640
of life, when non-meditators talk about the facts of life, I think they often talk about

92
00:11:32,640 --> 00:11:47,280
things like money and jobs and relationships, the things that are important, and so we get

93
00:11:47,280 --> 00:11:54,920
caught up in ambitions, we get caught up in narratives and stories about who we are and

94
00:11:54,920 --> 00:12:04,840
who are going to be, and we lose sight of what's actually going on behind the scenes

95
00:12:04,840 --> 00:12:13,800
while we're busy doing other things, busy making other plans, busy with our lives, what's

96
00:12:13,800 --> 00:12:20,480
happening with life, the main thing that's happening is we're getting older, it's the

97
00:12:20,480 --> 00:12:27,240
function of life really, what is life all about, what we say, what is the purpose of life,

98
00:12:27,240 --> 00:12:31,200
it's sort of the wrong question, life doesn't have a purpose, but what does it mean to

99
00:12:31,200 --> 00:12:39,480
live, to live is to age, to change, to grow, right?

100
00:12:39,480 --> 00:12:44,400
The first shock comes when we're kids, well I mean the first shock is when you have to

101
00:12:44,400 --> 00:12:48,480
leave the womb and you're like, what the hell, it was warm in there, it was come to

102
00:12:48,480 --> 00:12:57,120
your morning, have to freak out because you've grown too big for that, but then once you

103
00:12:57,120 --> 00:13:05,800
get to 15 years or so and while you grow again and all the changes of puberty, but more

104
00:13:05,800 --> 00:13:18,920
importantly, you realize that there are some facts of life that you didn't have to deal

105
00:13:18,920 --> 00:13:29,640
with, so you have to get a job and you realize that we have to deal with more than just

106
00:13:29,640 --> 00:13:34,880
playing with our toys and our friends and even just going to school.

107
00:13:34,880 --> 00:13:44,920
We have to grow up, but it doesn't stop, aging is the function of life where we're not

108
00:13:44,920 --> 00:13:49,720
static, we can't find stability.

109
00:13:49,720 --> 00:13:57,520
Anything we build up is a little more than a sandcastlely, it just gets knocked down again,

110
00:13:57,520 --> 00:14:11,880
we build up stability in our lives and we lose it, we get old, so you have to start

111
00:14:11,880 --> 00:14:19,080
thinking of meditation as something bigger than just this days that you're here, it's

112
00:14:19,080 --> 00:14:29,920
about changing the way we look at life and familiarizing ourselves with the reality of

113
00:14:29,920 --> 00:14:43,640
change and we're going to get old, we're going to lose our faculties.

114
00:14:43,640 --> 00:14:49,760
The big thing with all of these reflections is how we deal with life, are we going to

115
00:14:49,760 --> 00:15:00,160
be able to deal with getting old, losing our physical radiance and the beauty, the grain

116
00:15:00,160 --> 00:15:11,600
of hair and the yellowing of teeth, wrinkles and the aches and pains of old age, loss

117
00:15:11,600 --> 00:15:21,320
of memory, loss of sight, loss of hearing, well there's everything that, well that's

118
00:15:21,320 --> 00:15:29,480
another one, but aging takes away a lot of the things we cling to.

119
00:15:29,480 --> 00:15:37,120
And I think more importantly it's just the reality of life, life is this, it's a process

120
00:15:37,120 --> 00:15:44,840
of getting old, the process of growing, it's not static, I think mindfulness really helps

121
00:15:44,840 --> 00:15:56,040
us to, it should be clear that mindfulness helps us to be more at ease with change, with

122
00:15:56,040 --> 00:16:05,200
difficulty, with reality to be less attached to ourself, even our physical body, in the

123
00:16:05,200 --> 00:16:09,480
mind as well.

124
00:16:09,480 --> 00:16:16,120
Remember that we're getting old, it's a scary thing, can be quite scary if you're not prepared

125
00:16:16,120 --> 00:16:27,120
for it, you are clinging to being young, to being fit and beautiful and smart and clever

126
00:16:27,120 --> 00:16:40,680
and athletic and whatever, if you're clinging to it you're going to get left in the

127
00:16:40,680 --> 00:16:49,480
alert when it starts to change, when you get old, so that's one, I mean, these are all

128
00:16:49,480 --> 00:16:53,480
these first three I related, the next one is that we're going to get sick, we have a

129
00:16:53,480 --> 00:17:01,760
nature to get sick, we have the adi dhammum, he'd be adding an adi doh, we can just keep

130
00:17:01,760 --> 00:17:14,560
sickness, so we, I think, it's hard to go through life, hard to find someone who hasn't

131
00:17:14,560 --> 00:17:20,920
been sick with a cold or a flu or even maybe worse, but sickness goes deeper than that,

132
00:17:20,920 --> 00:17:24,600
of course, there's a sickness coming that's going to kill us, even if it's just the

133
00:17:24,600 --> 00:17:37,000
sickness of old age, most of us will die from something else, cancer or heart disease,

134
00:17:37,000 --> 00:17:46,520
diabetes, lots of ways to die, lots of sicknesses.

135
00:17:46,520 --> 00:17:54,480
Imagine having to live through cancer, it's not unlikely that some of the people listening

136
00:17:54,480 --> 00:18:01,480
to this who feel very healthy are suddenly going to find that they have been diagnosed

137
00:18:01,480 --> 00:18:08,840
with cancer and have to live through that, heart disease, diabetes, many different

138
00:18:08,840 --> 00:18:27,000
sicknesses might come, again it's about preparing ourselves for the reality of life, life

139
00:18:27,000 --> 00:18:34,600
is not, sickness is not actually a problem, sickness is not scary, but it scares the heck

140
00:18:34,600 --> 00:18:48,520
out of us, the reality of life that all of our plans and ambitions, all of our possessions,

141
00:18:48,520 --> 00:19:06,560
all of the things we, all of our wealth and power can't stop us from suffering, and reminding

142
00:19:06,560 --> 00:19:17,080
ourselves I am of a nature to get sick, I can't escape sickness, it's a good reminder

143
00:19:17,080 --> 00:19:30,040
of the potential for us to find ourselves in a situation where mindfulness and clarity

144
00:19:30,040 --> 00:19:36,600
is going to be very important, because our relatives can't save us and, well, sure can't

145
00:19:36,600 --> 00:19:46,440
save us, sickness can be a terrible thing if you've ever been very sick, the pain that

146
00:19:46,440 --> 00:19:53,880
the body is capable of is quite impressive, so when we talk about pain and we are concerned

147
00:19:53,880 --> 00:19:59,960
with pain in meditation you have to remember, the benefits of learning about pain and

148
00:19:59,960 --> 00:20:06,680
coming to deal with pain coming to terms with it are useful and beneficial it is for

149
00:20:06,680 --> 00:20:21,280
all the sickness, and of course the third one death, the modern adaamum, imaranaanatito,

150
00:20:21,280 --> 00:20:32,360
we are going to die, death I think is one of the scariest things, how scary it is to

151
00:20:32,360 --> 00:20:41,800
think about death, I can die tomorrow any one of us, we all will die, I don't know where

152
00:20:41,800 --> 00:20:48,200
we don't know when, we don't know how, we don't know what's going to happen to our body,

153
00:20:48,200 --> 00:20:54,120
we don't know where our mind is going to go, death is the scariest one if you really get into

154
00:20:54,120 --> 00:21:06,840
it because it involves life, it involves birth, death is the leading cause of birth,

155
00:21:06,840 --> 00:21:13,720
just as birth is the leading cause of death, you know, it's the cause that they lead to one another,

156
00:21:16,040 --> 00:21:23,160
and if you're not prepared for death, if you don't have a good death, it's like an exam,

157
00:21:23,160 --> 00:21:29,960
if you fail the exam, you haven't prepared for it, you're not ready for the final test,

158
00:21:31,800 --> 00:21:41,320
your mind will be in a very bad state when you die, you'll be afraid, you'll be clinging,

159
00:21:41,320 --> 00:21:58,440
maybe angry or sad, they say your whole life flashes before your eyes when you die, or there's

160
00:21:58,440 --> 00:22:04,840
a potential, I don't think it's true that your whole life but many things flash before your eyes,

161
00:22:04,840 --> 00:22:09,080
you know, when they say you're near, I've never had it happen, but they say that when you're

162
00:22:09,080 --> 00:22:16,040
when you realize you're going to die, when you're in danger, they say your whole life flashes

163
00:22:16,040 --> 00:22:22,520
before your eyes, why that happens, I don't know, I assume it, I can guess it has something to do

164
00:22:22,520 --> 00:22:29,880
with the clarity at that moment because there's such a strength of mind, the adrenaline, the faculties

165
00:22:29,880 --> 00:22:37,640
are so sharp that everything opens up and all the powerful emotional mental states are brought to

166
00:22:37,640 --> 00:22:43,880
the front, why? Because when you're practicing meditation, you should feel the same,

167
00:22:44,600 --> 00:22:48,440
you should realize that this is how the mind

168
00:22:51,640 --> 00:22:57,960
is that the nature of the mind is that it keeps all these states and when your mind is still,

169
00:22:57,960 --> 00:23:03,640
when your mind is clear, it brings them up, you see them again. Every day you should be seeing

170
00:23:03,640 --> 00:23:10,760
things you thought you'd forgotten about, things you thought you'd dealt with and they come up

171
00:23:10,760 --> 00:23:23,880
and give you a challenge. I think if you're an opportunity to address them, to come to terms with

172
00:23:23,880 --> 00:23:35,400
them, to make peace with them, which if you think about it, the philosophy of it is quite beneficial

173
00:23:36,280 --> 00:23:43,560
because whenever I can deal with now and whatever capacity to deal with these things I can

174
00:23:43,560 --> 00:23:53,320
cultivate now will prepare me that much better for when I don't have a choice, when they have to

175
00:23:53,320 --> 00:24:02,520
face them, when they're all I have because my body is failing and left only with my memories.

176
00:24:08,200 --> 00:24:14,440
Thinking about death, I think, is of the three the most powerful when you think about you might

177
00:24:14,440 --> 00:24:20,920
die tomorrow, you will die and you don't know when it's going to happen and realizing how

178
00:24:20,920 --> 00:24:31,960
incomplete our journey is, I mean just in terms of our capacity to deal with the facts of life,

179
00:24:31,960 --> 00:24:38,040
to deal with old age and sickness and suffering, challenges and change.

180
00:24:38,040 --> 00:24:55,400
How unequipped we are gives you a good impetus, a good push and a good push towards practicing

181
00:24:56,600 --> 00:25:05,720
mindfulness and goodness in general just being a good person, you know, that old book,

182
00:25:05,720 --> 00:25:12,360
the Christmas Carol or this guy sees these ghosts and realizes

183
00:25:14,760 --> 00:25:24,680
how rotten he is and how doomed he is to misery because of his evil deeds, his evil stinginess,

184
00:25:24,680 --> 00:25:41,000
it changes him. So there's a greatness in meditation, there's a greatness in its capacity to

185
00:25:42,440 --> 00:25:48,440
address these very fundamental facts of life, old age sickness and death.

186
00:25:48,440 --> 00:25:56,600
The fourth reflection that we should make every day

187
00:25:56,600 --> 00:26:21,880
is, and Pauli, it's, everything I hold dear will leave me, will disappear.

188
00:26:27,160 --> 00:26:39,160
All of our possessions, this one is addressing me, the fact of life relating to our possessions,

189
00:26:40,280 --> 00:26:46,200
this aspect of human existence or existence that is possessiveness or possession.

190
00:26:46,200 --> 00:27:02,280
The people and things that we think of as ours are belongings, our relatives, our loved ones,

191
00:27:02,280 --> 00:27:13,800
friends and lovers and so on, all the things that we think are ours and that you hear about

192
00:27:13,800 --> 00:27:22,360
people losing, right, losing possession. We've ever had something stolen from you, you know,

193
00:27:22,360 --> 00:27:32,600
what a violation it feels like, how awful. When you lose a friend or a relative, someone dies,

194
00:27:32,600 --> 00:27:46,920
or you lose a romantic interest, they lose interest or unfaithful. How much suffering in the world,

195
00:27:47,560 --> 00:27:52,360
you can measure how much suffering there is. If we had some kind of an instrument that

196
00:27:52,360 --> 00:27:59,480
allowed us to measure all the suffering that comes from people losing things. How much suffering

197
00:27:59,480 --> 00:28:04,520
that comes from divorce, we ever lived through a divorce, my parents got divorced and it was terrible,

198
00:28:05,480 --> 00:28:11,400
the amount of suffering for everybody. It's kind of funny when you think about it because

199
00:28:12,600 --> 00:28:15,560
why couldn't we, you know, why weren't we prepared to deal with that?

200
00:28:18,600 --> 00:28:19,960
Why did we go through that?

201
00:28:19,960 --> 00:28:33,000
As of course relates to craving, it relates to attachment to things. Why are we not able to deal

202
00:28:33,000 --> 00:28:37,960
with it because we're addicted to the things that bring us pleasure. People bring us pleasure

203
00:28:37,960 --> 00:28:47,960
when we lose them. My cousin died and it was just horrible for his parents. It just took something

204
00:28:47,960 --> 00:29:01,000
from them, something very precious. They weren't ready to deal with that. It's not surprising

205
00:29:01,720 --> 00:29:08,040
as we are, we love each other. We talk about loving each other but it's two parts. Love on the

206
00:29:08,040 --> 00:29:17,480
one hand is a capacity to wish well for others. But love on the other hand, what we call love

207
00:29:17,480 --> 00:29:25,560
is attachment. It's the need for certain things from that person, even for them to be happy,

208
00:29:25,560 --> 00:29:30,440
right? When someone's unhappy and then we feel depressed and unhappy because they are, oh,

209
00:29:31,320 --> 00:29:36,680
my friend, my relative is suffering and you feel bad because you feel like they're yours.

210
00:29:38,360 --> 00:29:41,640
Much different from when you feel it's strangers suffering me.

211
00:29:41,640 --> 00:29:56,120
So much suffering comes from, there was someone in the Buddha's time who was, I can't remember,

212
00:29:56,120 --> 00:30:11,480
I think it was just some householder who was saying, what was it? B-at-e-jai-at-e-su-kong,

213
00:30:13,880 --> 00:30:19,800
that which is dear. That which is dear brings happiness

214
00:30:19,800 --> 00:30:32,760
from that which is dear comes happiness, so-called, and the Buddha said,

215
00:30:32,760 --> 00:30:50,520
from that which is dear comes, so-called, so-called, so-called means at this, the opposite of so-called.

216
00:30:50,520 --> 00:31:03,480
Yeah, when you own something dear, think of how much happiness it brings us.

217
00:31:06,520 --> 00:31:10,120
I think you get much more happiness, I think, and we can make a claim.

218
00:31:10,120 --> 00:31:20,120
Much more happiness comes from letting go, comes from an open appreciation of

219
00:31:24,520 --> 00:31:34,760
the universe as a whole. What's real, rather than liking the things and wanting the things

220
00:31:34,760 --> 00:31:41,080
that you think of as a mind or you think of as what I'm partial to,

221
00:31:44,920 --> 00:31:53,240
to have a capacity to be at peace with what you have, to have a bunch more open and healthy and

222
00:31:55,080 --> 00:32:01,640
equanimist relationship with things. Between the two, a person who needs certain things to be

223
00:32:01,640 --> 00:32:10,680
happy and can't be happy in great many circumstances and a person who's happy no matter what happens,

224
00:32:12,040 --> 00:32:19,000
if someone dies or they lose someone or something for their rich or poor, healthy, unhealthy.

225
00:32:22,600 --> 00:32:28,280
There's a greatness in our capacity to be open to change and loss.

226
00:32:28,280 --> 00:32:36,040
So reminding ourselves of why that is, the fact that we're going to lose everything,

227
00:32:36,040 --> 00:32:39,560
that everything we hold dear, we're going to lose, it's going to leave us.

228
00:32:41,800 --> 00:32:45,560
It's a great thing to reflect on it. These sort of things will test you

229
00:32:46,520 --> 00:32:49,800
because when you remind yourself of them, you realize that you're not ready for that.

230
00:32:51,240 --> 00:32:54,040
Not ready for all the sickness, death, loss.

231
00:32:54,040 --> 00:33:04,120
The fifth reflection is, it's a longer one, kamasakomi.

232
00:33:06,040 --> 00:33:15,000
I am the owner of my kamma, my deans, kamadaya do. I am the heir to my deans,

233
00:33:15,000 --> 00:33:28,040
I mean, I receive their inheritance, kamayoni, I am born of my deans, kamabandhu,

234
00:33:28,040 --> 00:33:37,240
I am bound to my deans, kamapati sartano, I am dependent on my deans,

235
00:33:37,240 --> 00:33:48,200
thank you. Young kamamakati sami, kamayana, kamapakana, whatever deans I perform,

236
00:33:49,000 --> 00:33:56,360
beautiful or evil, kalayana, this means good. It literally means beautiful. It's nice to say it,

237
00:33:56,360 --> 00:34:02,920
but it just means good deans. Beautiful deans are known as beautiful deans.

238
00:34:02,920 --> 00:34:12,040
kalayana, namapapakama, tasadaya dobhui sami, I will become an heir to those dead.

239
00:34:18,760 --> 00:34:23,320
This one, I think, is straight to the point. It's different from the others and then it relates

240
00:34:23,320 --> 00:34:34,440
to not just experience in our reaction to them, but our actual reactions, it deals with the

241
00:34:38,280 --> 00:34:45,160
the things we do to try and avoid all the hidden sickness, death, loss, etc.

242
00:34:45,160 --> 00:34:54,520
Take hold on to our youth, our health, our life, our pleasures, happiness.

243
00:34:58,760 --> 00:35:04,280
All the things we do, we become heir to them. If you leave here today,

244
00:35:04,280 --> 00:35:09,960
you get to take everything with you. All the things we've done good and bad.

245
00:35:09,960 --> 00:35:24,600
Someone recently come to me quite upset and saying they had to confess that when they were young,

246
00:35:26,280 --> 00:35:34,680
they did a lot of bad things, torturing animals, that kind of thing. We talked to

247
00:35:34,680 --> 00:35:42,040
everybody, they were worried that they were hopeless, never going to be able to progress and

248
00:35:42,040 --> 00:35:47,880
put his teaching and meditation and so on, because those are bad deeds. What's going to happen,

249
00:35:47,880 --> 00:35:56,280
I said, well, you might be reborn ugly or sickly. They laugh, oh, I can handle that.

250
00:35:56,280 --> 00:36:06,280
It's a good reflection because we have to come to terms with this as well.

251
00:36:07,080 --> 00:36:13,480
Meditation isn't going to smooth out everything. It's not going to make it so that you never

252
00:36:13,480 --> 00:36:21,640
get sick. Goodness is not going to erase everything we've done.

253
00:36:21,640 --> 00:36:33,160
It's important because part of the way we approach the practice fairly bare-bones streamline

254
00:36:33,160 --> 00:36:43,160
approach, it's much more about learning to be at peace with suffering than to try and avoid

255
00:36:43,160 --> 00:36:50,200
their escape suffering because it's very different from

256
00:36:54,120 --> 00:36:59,240
the happiness of avoiding suffering is very different from

257
00:36:59,240 --> 00:37:12,920
the true state of peace and freedom.

258
00:37:12,920 --> 00:37:34,760
When you come here to meditate you, you're faced with who you are, you're faced with

259
00:37:34,760 --> 00:37:43,560
your mental qualities, you're also faced with your memories, the things you've done.

260
00:37:47,000 --> 00:37:53,640
When you start to feel, you start to see as you practice mindfulness, you start to see how

261
00:37:54,440 --> 00:38:02,520
every moment has the capacity to be life-changing in the very small way,

262
00:38:02,520 --> 00:38:09,000
a better capacity to change who I am, how I relate to people, how I interact with people.

263
00:38:10,760 --> 00:38:15,240
You start to see how all the things you've done through your life have changed who you are.

264
00:38:18,440 --> 00:38:24,200
And so some of them have just immediate consequences. It upsets you or

265
00:38:24,200 --> 00:38:31,960
it affects your personality, if you're cruel, then you become more angry and that sort of thing.

266
00:38:32,760 --> 00:38:36,440
Some of them are the sorts of things that you have to remember now years later when you've done

267
00:38:36,440 --> 00:38:42,520
them and you feel bad about them. It feels sad because of the things you've done.

268
00:38:47,560 --> 00:38:51,320
And some of them are going to make you ugly in your next life.

269
00:38:51,320 --> 00:38:59,240
And of course the opposite is true. This is not all doom and gloom. This is one of the

270
00:38:59,240 --> 00:39:07,320
fire that we can actually look at the good side. And especially in terms of meditation,

271
00:39:07,320 --> 00:39:14,280
because there are many good things we can do. We can be charitable, we can be ethical,

272
00:39:14,280 --> 00:39:27,960
we can be kind to others, loving, compassionate. But mindfulness, mindfulness we hold above all of

273
00:39:27,960 --> 00:39:35,160
those. All of those things are good and important to you. But the best, the best, the best,

274
00:39:35,160 --> 00:39:42,120
the best, the best wholesomeness is the practice of mindfulness, the practice of seeing things

275
00:39:42,120 --> 00:39:54,520
as they are without judgment, without partiality, without stress or suffering.

276
00:39:58,120 --> 00:40:03,800
And so the great wholesomeness that you're cultivating, even here, just walking and sitting in your

277
00:40:03,800 --> 00:40:17,480
room, that the idea is there's a great cleansing quality to it. So much, you should see so much

278
00:40:19,320 --> 00:40:25,480
rubbish in the mind, not just garbage, but some bad habits.

279
00:40:25,480 --> 00:40:34,600
It's like we're, it's not only that we're unclean, it's that we're committed to dirty and

280
00:40:34,600 --> 00:40:42,120
solving ourselves further. It's like a child, you can give it a bath and then it jumps into the

281
00:40:42,120 --> 00:40:50,520
mud hole again. Okay, I'll clean up, clean up your children as soon as they've done in the

282
00:40:50,520 --> 00:40:56,040
bath, they will jump in the mud again.

283
00:40:57,400 --> 00:41:16,120
Great advice that we're not only not only do we have bad tendencies, but we're inclined to do things

284
00:41:16,120 --> 00:41:27,400
that reinforce them. So you, so you come to address as you come to recognize this

285
00:41:28,920 --> 00:41:37,800
and start to deal with it. Being mindful is changing your habits, it's about changing who we are,

286
00:41:37,800 --> 00:41:44,920
changing what we do. And the point is that this is the one thing that we take with us, we don't

287
00:41:44,920 --> 00:41:49,720
get to take our possessions, we don't get to take our relatives, we don't even get to take our

288
00:41:49,720 --> 00:41:57,720
lives, our body, our selves, really. Even our minds, if you want to go there, we don't get to take

289
00:41:58,760 --> 00:42:05,960
all of our memories and thoughts and mental capacities. But what we do take is our habits and

290
00:42:05,960 --> 00:42:14,760
our personalities as they evolve, right? We don't get to be who we were when we were five years old.

291
00:42:16,040 --> 00:42:23,720
We've evolved from there, we've moved on from there. And our deeds, deeds and Buddhism is not

292
00:42:23,720 --> 00:42:30,360
physical, it's your mental inclination, volition when you decide to do something when you

293
00:42:30,360 --> 00:42:41,400
reinforce a certain habit. That's the real deeds. When you do that, as we do that in life,

294
00:42:41,400 --> 00:42:46,680
that builds who we are, that builds our character for good or for evil. And that's what we take with

295
00:42:46,680 --> 00:42:56,280
us. We own them, we can't pass them off on someone else. We are their heir, we inherit

296
00:42:56,280 --> 00:43:03,560
the results, if you do, if you harm others, hurt others or mean to them. Bad things are going to

297
00:43:03,560 --> 00:43:10,280
come your way and they won't like you. You won't like yourself, probably, and be afraid of

298
00:43:10,280 --> 00:43:16,040
retribution, etc, etc. Bad things, right? On the other hand, you help people if you're kind and

299
00:43:16,040 --> 00:43:24,360
generous and charitable, even if you're just ethical, the appreciation and comfort that people

300
00:43:24,360 --> 00:43:32,600
have with you, how trustworthy, how trusting people will become of you. It's all the good that

301
00:43:32,600 --> 00:43:42,680
comes, how good you feel about yourself for doing something so pure, good. All that changes you.

302
00:43:43,640 --> 00:43:50,040
Makes you physically healthy because you're not stressed or worried or guilty feeling.

303
00:43:50,040 --> 00:43:57,880
Makes you mentally healthy. You can be at peace with yourself knowing that you're a good person

304
00:43:58,440 --> 00:44:02,200
and that you've done good. When you sit here in meditation, you should see both.

305
00:44:03,320 --> 00:44:09,480
If you haven't done a lot of good deeds, well, coming to turn, remember, it's not that you've

306
00:44:09,480 --> 00:44:15,320
done good or bad deeds. It's coming to terms with them. Just be prepared for suffering. That's

307
00:44:15,320 --> 00:44:27,160
all. Maybe you'll be ugly or sick or whatever. Realizing that this is who we are. We are not who

308
00:44:27,160 --> 00:44:33,880
what we own. We are not our friends. We are not who we associate with. Who we are is our minds,

309
00:44:33,880 --> 00:44:40,840
our state of mind, our habits, our personality, how much ultimately comes down to our

310
00:44:40,840 --> 00:44:48,520
karma, our deeds, the things we do, the decisions we make and that's important. It's probably

311
00:44:48,520 --> 00:44:57,080
the scariest because every moment we have potential to be in good, cultivate good things or

312
00:44:58,840 --> 00:45:07,400
reinforce the negative ones. So five reflections. Hopefully there's something that gives you

313
00:45:07,400 --> 00:45:15,000
some encouragement and says, yeah, this is an important thing to do. It's more than just

314
00:45:17,000 --> 00:45:32,280
an idle interest that you do once and then forget about. It's about waking up

315
00:45:32,280 --> 00:45:42,280
to the facts of life and finding a way to relate to the truth. So these five reflections

316
00:45:42,280 --> 00:45:49,960
sort of provide that framework of one of the important facts of life and Buddhism things to

317
00:45:49,960 --> 00:46:00,360
reflect on to remind you of what's important and what we should do but do about it. So that's the

318
00:46:00,360 --> 00:46:22,040
demo for tonight. Thank you for listening.

