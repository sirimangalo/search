1
00:00:00,000 --> 00:00:05,000
So someone says, I am ordaining in the forest tradition as a Samanera soon.

2
00:00:05,000 --> 00:00:13,000
I will stay as a novice for about four to six months, then I will get married to another novice.

3
00:00:13,000 --> 00:00:17,000
Like at the monster, I have a strong desire to become a monk.

4
00:00:17,000 --> 00:00:21,000
Hence, I put it off for long, wrong.

5
00:00:21,000 --> 00:00:26,000
I assume you'll just robe before you get married.

6
00:00:26,000 --> 00:00:36,000
It's not like after four to six months in that you get married to silly jokes, sorry.

7
00:00:36,000 --> 00:00:42,000
I mean the reason for joking is it's such a difficult question.

8
00:00:42,000 --> 00:00:53,000
It's not really according to the tradition to leave the home life with the expectation of

9
00:00:53,000 --> 00:00:58,000
going, you know, getting more caught up in the home life afterwards.

10
00:00:58,000 --> 00:01:07,000
Yeah, I don't think it's a good idea to really decide to be a monk and then make plans of leaving later.

11
00:01:07,000 --> 00:01:09,000
Yeah.

12
00:01:09,000 --> 00:01:13,000
So if you want to get married, well, get married.

13
00:01:13,000 --> 00:01:22,000
I mean, the reason why it's a difficult question is because really people are doing it every day and mostly in Thailand.

14
00:01:22,000 --> 00:01:24,000
Maybe Burma, I'm not sure.

15
00:01:24,000 --> 00:01:29,000
But for sure in Thailand, probably Cambodia, Laos.

16
00:01:29,000 --> 00:01:31,000
It's a big thing.

17
00:01:31,000 --> 00:01:35,000
Or day in for a week, or day in for a month, or day in for three months.

18
00:01:35,000 --> 00:01:42,000
Four to six months is pretty impressive by today's standard.

19
00:01:42,000 --> 00:01:49,000
But I mean, it's not just going on a retreat.

20
00:01:49,000 --> 00:01:53,000
Becoming a monk is taking on a lifestyle.

21
00:01:53,000 --> 00:01:56,000
It's like taking on a profession.

22
00:01:56,000 --> 00:02:05,000
And I've gone on above why I'm personally not inclined to support temporary ordinations because I've seen what they do.

23
00:02:05,000 --> 00:02:11,000
My monastery nowadays has every other day has an ordination.

24
00:02:11,000 --> 00:02:28,000
The first problem is that it inclines people to think of the monk would as a temporary thing.

25
00:02:28,000 --> 00:02:38,000
And so, rather than taking it seriously and saying you ordained to dedicate your life to it, they say, oh yeah, ordination, that's what you do before you get married.

26
00:02:38,000 --> 00:02:43,000
And that's really how many people look at ordination.

27
00:02:43,000 --> 00:02:52,000
They are very happy that their children are ordained, but after a month, if they decide they want to stay on, oh, there's health to pay.

28
00:02:52,000 --> 00:02:57,000
Ordained for more than a month isn't a month enough, that kind of thing.

29
00:02:57,000 --> 00:03:02,000
And this is really sad that people actually develop these ideas.

30
00:03:02,000 --> 00:03:21,000
The second reason is that the monks who do stay on wind up spending all of their time organizing ordinations, the shaving, the robes, the bull, checking everything, teaching the applicants how to do the chanting, how to do the ceremonies.

31
00:03:21,000 --> 00:03:33,000
And then teaching the new monks, who, you know, sometimes people, there was recently or a year or two ago I heard, but there was an ordination.

32
00:03:33,000 --> 00:03:39,000
And that night, after they'd ordained, one of the monks got drunk.

33
00:03:39,000 --> 00:03:47,000
So, you're dealing with people who have no clue about the Vinaya and about the rules, and you have to teach them from scratch.

34
00:03:47,000 --> 00:03:53,000
And that's, if done properly, that's a difficult task. Now, what happens is, it's usually not done properly.

35
00:03:53,000 --> 00:04:03,000
And so, the monks sit around smoking and chatting and they have money and they buy food and they buy, you know, they eat in the evening and maybe they, you know.

36
00:04:03,000 --> 00:04:20,000
I mean, not maybe. I mean, I've been in, I've seen how it goes. They, they, how can they, what's the euphemism? They engage in sexual, they, they pleasure themselves.

37
00:04:20,000 --> 00:04:25,000
I don't know how do you say that. Let's, let's say, so not words that I use for you.

38
00:04:25,000 --> 00:04:31,000
You know, so they, they break a lot of actually sometimes serious rules.

39
00:04:31,000 --> 00:04:49,000
So, that's incredibly tiring and, and for the senior monks to have to deal with and, and it takes them away from practice, study and teaching that they could be be, they could be better used elsewhere.

40
00:04:49,000 --> 00:04:58,000
You know, if someone comes for a month and they say, I want to come and meditate and then they, but I also want to ordain, you know, what a waste of time when they could be spending that month meditating.

41
00:04:58,000 --> 00:05:03,000
To have to teach them how to put a robe on correctly and in Thailand, that's a big deal.

42
00:05:03,000 --> 00:05:10,000
How to put the robe on correctly, how to walk on arms around, how to, you know, this and that and the rules, how many rules you have to keep.

43
00:05:10,000 --> 00:05:14,000
This isn't something that's designed for short term.

44
00:05:14,000 --> 00:05:27,000
It takes you five years to become a good monk and if you don't have five years to do it, then you're, you're just going to spend all that time training for nothing.

45
00:05:27,000 --> 00:05:32,000
You know, well, maybe it's useful to deal with married life.

46
00:05:32,000 --> 00:05:42,000
See how discipline could be useful for dealing with, having a family, and that's the argument that they use.

47
00:05:42,000 --> 00:05:56,000
Anyway, so you have that, then you have the fact that it brings money and it brings,

48
00:05:56,000 --> 00:06:04,000
well, yeah, it brings money and it brings fame and it brings affluence to the monastery.

49
00:06:04,000 --> 00:06:20,000
And so the monks wind up not only wasting their time, but getting caught up in and focusing their energies on the ordinations because they get paid for it or they get some kind of, you know, it looks good to have many, many monks

50
00:06:20,000 --> 00:06:26,000
who are dating and they get prestige, they get power, they get fame and so on.

51
00:06:26,000 --> 00:06:33,000
And so they focus on it, kind of like a business, this happens.

52
00:06:33,000 --> 00:06:42,000
And finally it destroys a monastic harmony when you have these monks who are misbehaving.

53
00:06:42,000 --> 00:06:51,000
And when the monastery is, you know, at least half the monks will be new monks and that's a constant state.

54
00:06:51,000 --> 00:06:58,000
So those, you know, half the monks will always be causing trouble.

55
00:06:58,000 --> 00:07:04,000
And so by the time you teach them how to, how to fit in, then they leave and then you've got a new badge.

56
00:07:04,000 --> 00:07:13,000
And so the monastery never really feels like a monastery and there's a lot of stress and tension and so on.

57
00:07:13,000 --> 00:07:19,000
So the results that it has from what I've seen are not positive.

58
00:07:19,000 --> 00:07:21,000
Is it against the rules?

59
00:07:21,000 --> 00:07:23,000
Technically it's not against the rules.

60
00:07:23,000 --> 00:07:26,000
And if that's all you've got, then that's all you've got.

61
00:07:26,000 --> 00:07:35,000
I think the biggest point is the misconception of it as being the proper thing to do.

62
00:07:35,000 --> 00:07:41,000
Which leads, as I said, people to become complacent and not an undervalue of the monastic life.

63
00:07:41,000 --> 00:07:45,000
That's really the start of it because if someone says, yeah, I want to ordain temporarily,

64
00:07:45,000 --> 00:07:47,000
well, you don't have to forbid them.

65
00:07:47,000 --> 00:07:50,000
You don't have to say, no, we don't do temporary nations.

66
00:07:50,000 --> 00:07:58,000
I mean, I might even do that. I'm not really interested in allowing, well, in some cases,

67
00:07:58,000 --> 00:08:03,000
I would allow people to ordain temporarily, usually with thought that they want to ordain temporarily

68
00:08:03,000 --> 00:08:05,000
because they have something to do later.

69
00:08:05,000 --> 00:08:09,000
So okay, they can just robe and go take care of that and then come back and ordain long term.

70
00:08:09,000 --> 00:08:12,000
That kind of thing makes sense to me.

71
00:08:12,000 --> 00:08:17,000
But the idea of ordaining before marriage is personally something I wouldn't do it.

72
00:08:17,000 --> 00:08:26,000
But the point more than that is, it should be understood that this isn't what monastic life is about.

73
00:08:26,000 --> 00:08:29,000
Anyone have any thoughts?

74
00:08:29,000 --> 00:08:35,000
It's kind of something you have to have experienced firsthand to have much of a feeling about.

75
00:08:35,000 --> 00:08:43,000
I did see last year there was a bit of a thing going around where you could be a monk for a month.

76
00:08:43,000 --> 00:08:47,000
Yeah, I commented on that and it wasn't really well received.

77
00:08:47,000 --> 00:08:53,000
Well, I think they were not enough, but we had a little bit of a difference of opinion.

78
00:08:53,000 --> 00:09:02,000
I would imagine it takes probably just a couple of years to really overcome attachment.

79
00:09:02,000 --> 00:09:05,000
Or lifetimes, yeah.

80
00:09:05,000 --> 00:09:13,000
Long time just to do that.

81
00:09:13,000 --> 00:09:15,000
Don't talk about giving up attachments.

82
00:09:15,000 --> 00:09:19,000
It takes some months to learn how to put your robe on.

83
00:09:19,000 --> 00:09:29,000
It takes the way it generally goes is after a monk you're so proud of yourself for being able to put your robe on.

84
00:09:29,000 --> 00:09:35,000
That you start to feel like you're really the most awesome monk in existence.

85
00:09:35,000 --> 00:09:43,000
And then when you get to, when you act like a jerk for a year, for 11 months and when you get to a year,

86
00:09:43,000 --> 00:09:45,000
I say, wow, I've been a monk for a year.

87
00:09:45,000 --> 00:09:52,000
I mean a lot of mistakes, but now I'm an excellent monk.

88
00:09:52,000 --> 00:09:58,000
After a year you have a real, you don't have this insane ego,

89
00:09:58,000 --> 00:10:03,000
but you have a chip on your shoulder thinking, I'm so much better than the rest of these monks.

90
00:10:03,000 --> 00:10:07,000
Look at these monks who have been months for 10 years, 20 years, and they're doing nothing.

91
00:10:07,000 --> 00:10:13,000
I really, you're really proud of yourself.

92
00:10:13,000 --> 00:10:18,000
And so you spend the next four years acting like a little bit of a jerk.

93
00:10:18,000 --> 00:10:27,000
And when you hit five years, then you say, well, that was five years for tough, but now I'm a good monk.

94
00:10:27,000 --> 00:10:29,000
Now I'm now I'm free.

95
00:10:29,000 --> 00:10:34,000
I don't have to stay with my teacher, so I am qualified to go out and teach,

96
00:10:34,000 --> 00:10:37,000
and I don't have to live under the dependence of anyone.

97
00:10:37,000 --> 00:10:40,000
It's the rule that at five years then you can go on your own,

98
00:10:40,000 --> 00:10:48,000
and you spend the next five years not acting like a jerk anymore, but with a chip on your shoulder.

99
00:10:48,000 --> 00:10:52,000
Proud of yourself and your accomplishment.

100
00:10:52,000 --> 00:11:01,000
And then after 10 years, well, that's where I am, so I have, you know, now I have to learn how the next 10 years are going to be,

101
00:11:01,000 --> 00:11:04,000
but it feels a lot more like you start to say, well, now I'm just a monk.

102
00:11:04,000 --> 00:11:11,000
I don't think it's quite that, but I would say probably when you get to 20 years, that's where it seems.

103
00:11:11,000 --> 00:11:14,000
So if you look at monks who have been 20 years, whether they're good monks or bad monks,

104
00:11:14,000 --> 00:11:16,000
they really stop thinking about being a monk.

105
00:11:16,000 --> 00:11:22,000
And they just are, they are who they are, and there's no more of this.

106
00:11:22,000 --> 00:11:23,000
There's no more counting, right?

107
00:11:23,000 --> 00:11:27,000
Because you see, you're always counting how long until you get to a year,

108
00:11:27,000 --> 00:11:31,000
and then how long you get to five years, and there's all this stuff about seniorities.

109
00:11:31,000 --> 00:11:34,000
So you're always thinking, oh, this guy's more senior than me.

110
00:11:34,000 --> 00:11:40,000
And then the new monks come and you're proud because they're less senior to you and you sit in front of them and chanting.

111
00:11:40,000 --> 00:11:46,000
There's all this kind of rivalry when a monk comes to visit, and he sits in front of you,

112
00:11:46,000 --> 00:11:49,000
and then suddenly there's someone senior to you and stuff.

113
00:11:49,000 --> 00:11:53,000
So you're always counting the years, and you're saying, now I'm five years.

114
00:11:53,000 --> 00:11:57,000
And of course, after five years, you're counting it because after five years, you can go on your own.

115
00:11:57,000 --> 00:12:02,000
And after 10 years, you can actually, sorry, five years, you're not a teacher at 10 years,

116
00:12:02,000 --> 00:12:09,000
you can actually be a teacher, and a teacher of monks, and you can actually theoretically ordain

117
00:12:09,000 --> 00:12:12,000
at their monks.

118
00:12:12,000 --> 00:12:18,000
So I was counting that because that's been a big part of my work to not have to rely on others,

119
00:12:18,000 --> 00:12:20,000
to create a monastic community.

120
00:12:20,000 --> 00:12:27,000
So I think after 10 years, I'm not really counting anymore.

121
00:12:27,000 --> 00:12:31,000
I'm not thinking, oh boy, how many years left until I get to 20?

122
00:12:31,000 --> 00:12:35,000
It's not really, it doesn't mean as much anymore.

123
00:12:35,000 --> 00:12:40,000
So I don't know, that wasn't so specifically directed towards what you're saying,

124
00:12:40,000 --> 00:12:47,000
but definitely definitely it takes many, many years to settle down as a monk.

125
00:12:47,000 --> 00:12:49,000
I would say really 10.

126
00:12:49,000 --> 00:12:54,000
If you're not planning to be a monk for 10 years, you're not really going to become a monk.

127
00:12:54,000 --> 00:12:59,000
Now the thing is he's saying about becoming a novice, which is a little bit interesting because

128
00:12:59,000 --> 00:13:05,000
there's even less to discuss, and actually the, or to learn,

129
00:13:05,000 --> 00:13:09,000
actually the monk for a month turns out they're not becoming monks,

130
00:13:09,000 --> 00:13:13,000
they're becoming saminaras as well, which only have 10, who only have 10 precepts.

131
00:13:13,000 --> 00:13:18,000
So it's debatable, you could say, well, that's an interesting halfway.

132
00:13:18,000 --> 00:13:26,000
And certainly the ordination is a lot less, puts a lot less stress on the monastery.

133
00:13:26,000 --> 00:13:37,000
So it's a lot less of an issue to become a novice than it is to become a temporary monk.

134
00:13:37,000 --> 00:13:42,000
But you're still, you're taking on the roads, you're ordaining.

135
00:13:42,000 --> 00:13:47,000
If you want to leave home for a while, you'll be taking eight precepts,

136
00:13:47,000 --> 00:13:49,000
and that's established.

137
00:13:49,000 --> 00:13:53,000
If you want to go to stay in the monastery, even staying in the monastery with eight precepts,

138
00:13:53,000 --> 00:13:56,000
that's really a cool thing to do.

139
00:13:56,000 --> 00:14:01,000
But if you're coming with a fiance to ordain a novice,

140
00:14:01,000 --> 00:14:04,000
it's kind of disrespectful, I think.

141
00:14:04,000 --> 00:14:13,000
I don't want to deduce you or anything, but it's not you that I'm talking about this person has.

142
00:14:13,000 --> 00:14:17,000
It's the general tendency, which is prevalent.

143
00:14:17,000 --> 00:14:18,000
I mean, my teacher does it.

144
00:14:18,000 --> 00:14:20,000
They ordain temporary monks all the time.

145
00:14:20,000 --> 00:14:23,000
That's the culture and violent.

146
00:14:23,000 --> 00:14:38,000
But it's in general, I think, something that it's a shame that it's so prevalent in my mind.

147
00:14:38,000 --> 00:14:44,000
Anyway, that's why maybe it's done with the hope that they'll stay.

148
00:14:44,000 --> 00:14:48,000
Honestly, I agree with what you're saying completely.

149
00:14:48,000 --> 00:14:51,000
I think that's one of the reasons why my teacher does it.

150
00:14:51,000 --> 00:14:53,000
And I don't think.

151
00:14:53,000 --> 00:14:57,000
I mean, it's easy to see because after one month and he's pushing them to stay two months.

152
00:14:57,000 --> 00:15:00,000
And that's really how it's got to work in Thailand.

153
00:15:00,000 --> 00:15:07,000
If you want monks to stay long term, you just ordain 20 of them and hope that one of them sticks.

154
00:15:07,000 --> 00:15:08,000
I was ordained.

155
00:15:08,000 --> 00:15:10,000
There were 18 of us when I ordained.

156
00:15:10,000 --> 00:15:12,000
I'm the only one left.

157
00:15:12,000 --> 00:15:26,000
But of course, that was the case, all of the other 17 or 15 of them were clear that they were ordaining temporarily in honor of the king from the king's birthday.

158
00:15:26,000 --> 00:15:28,000
And there were three foreigners.

159
00:15:28,000 --> 00:15:37,000
And I think all three of us had at least some idea to stay, but the other two disrobed.

160
00:15:37,000 --> 00:15:42,000
Sounds pretty strict. The whole thing about the robe that you're talking about.

161
00:15:42,000 --> 00:15:45,000
That sounds like a ritual in itself.

162
00:15:45,000 --> 00:15:48,000
What do you mean wearing the robe?

163
00:15:48,000 --> 00:15:51,000
Yeah, we're talking about it takes you just a certain amount of time.

164
00:15:51,000 --> 00:15:53,000
Just learn how to put your robe on.

165
00:15:53,000 --> 00:15:54,000
Yeah.

166
00:15:54,000 --> 00:15:56,000
Yeah, I mean, not in Sri Lanka so much.

167
00:15:56,000 --> 00:15:58,000
See, we weren't quite simply.

168
00:15:58,000 --> 00:16:05,000
But if you look at my how to meditate videos when I was wearing a robe like a time monk, that takes work.

169
00:16:05,000 --> 00:16:08,000
Take some time.

170
00:16:08,000 --> 00:16:14,000
And even we have another way to wear it when you go outside where you put it over both shoulders and roll it all up.

171
00:16:14,000 --> 00:16:18,000
That takes about a month to get to get skilled at them.

172
00:16:18,000 --> 00:16:24,000
After a couple of days, even I can teach a person it in a day so that they are able to go out the next day.

173
00:16:24,000 --> 00:16:30,000
But for them to keep it on and to be comfortable in it for that takes at least a month.

174
00:16:30,000 --> 00:16:32,000
I'm doing it every day.

175
00:16:32,000 --> 00:16:39,000
I saw an interesting question here.

176
00:16:39,000 --> 00:16:43,000
Wait, someone's this person is actually continuing this if we can talk.

177
00:16:43,000 --> 00:16:44,000
It's the same person.

178
00:16:44,000 --> 00:16:46,000
What if it is marriage out of compassion?

179
00:16:46,000 --> 00:16:48,000
I don't really want to get married.

180
00:16:48,000 --> 00:16:56,000
But I see how me becoming this person's being in this person's life will help them more than if I become a monk for a longer time.

181
00:16:56,000 --> 00:17:02,000
So, I'm in a smaller life.

182
00:17:02,000 --> 00:17:11,000
Marriage out of compassion.

183
00:17:11,000 --> 00:17:17,000
I'm sorry, that sounds like a bad reason to get married.

184
00:17:17,000 --> 00:17:27,000
I can see it, but I don't have any real good comments.

185
00:17:27,000 --> 00:17:32,000
Yeah, I have comments.

186
00:17:32,000 --> 00:17:36,000
You know, helping people, what is the best way to help a person?

187
00:17:36,000 --> 00:17:38,000
Who can help a person?

188
00:17:38,000 --> 00:17:41,000
I guess I have comments because I've dealt with this.

189
00:17:41,000 --> 00:17:56,000
I've dealt with women who have asked me to not ask directly, but who have made it quite clear what their intention was and wanted an answer.

190
00:17:56,000 --> 00:18:02,000
And even recently, there was a case with...

191
00:18:02,000 --> 00:18:10,000
Well, I had a case where I was being asked questions about marriage that were quite pointed.

192
00:18:10,000 --> 00:18:18,000
And I had to go through this, and so I took this person through a long...

193
00:18:18,000 --> 00:18:20,000
Kind of like we went on a trip together.

194
00:18:20,000 --> 00:18:27,000
I mean, we sat there and we talked and I led her through this issue that she was obviously having.

195
00:18:27,000 --> 00:18:39,000
And the main point of it was how much more benefit comes from having a spiritual relationship.

196
00:18:39,000 --> 00:18:48,000
Then from having a corporeal, one corporeal physical relationship.

197
00:18:48,000 --> 00:19:02,000
So, when you become a monk, you from a Buddhist point, if you take on the greatest role you can ever have in relating to other people,

198
00:19:02,000 --> 00:19:09,000
you are the representative of the Buddha.

199
00:19:09,000 --> 00:19:21,000
You have direct and dedicated access, continuous access to the Buddha's teaching.

200
00:19:21,000 --> 00:19:28,000
You are living, breathing, you are immersed in Buddhism.

201
00:19:28,000 --> 00:19:46,000
So, theoretically, you are your role and the relationship that you have to all of the people who are close to you is optimum.

202
00:19:46,000 --> 00:19:50,000
You can relate to them in a way that is completely pure.

203
00:19:50,000 --> 00:19:52,000
You can love them.

204
00:19:52,000 --> 00:19:58,000
I mean, a big part of our conversation was about love with this person.

205
00:19:58,000 --> 00:20:02,000
Because you can very much love people.

206
00:20:02,000 --> 00:20:04,000
There is nothing wrong with love.

207
00:20:04,000 --> 00:20:12,000
In fact, it is a great thing and it leads people to find happiness and to be at peace in their mind.

208
00:20:12,000 --> 00:20:19,000
To love.

209
00:20:19,000 --> 00:20:33,000
So, the only benefit of marriage or the only thing missing from a monastic relationship that marriage has is based on attachment.

210
00:20:33,000 --> 00:20:38,000
And so, the question you have to ask is whether attachment is beneficial to anyone.

211
00:20:38,000 --> 00:20:42,000
Of course, there are people who believe that, but certainly as Buddhist we don't believe it.

212
00:20:42,000 --> 00:20:46,000
If you think it will help this person to be in their life,

213
00:20:46,000 --> 00:20:49,000
then what we have two parts to that.

214
00:20:49,000 --> 00:20:54,000
The one part is your attachment to helping this person.

215
00:20:54,000 --> 00:21:00,000
Why is this person so important to be helped when the world is full of people to be helped?

216
00:21:00,000 --> 00:21:06,000
So, it may be because it may be as a monk that you are not able to directly help this person, right?

217
00:21:06,000 --> 00:21:10,000
But certainly you will be able to help whoever it is that you are around.

218
00:21:10,000 --> 00:21:14,000
If this person decides when you are a monk they don't want to have anything to do with you.

219
00:21:14,000 --> 00:21:17,000
Then it may be that yes, you can't help that person.

220
00:21:17,000 --> 00:21:20,000
But what is it that's making you want to help this person?

221
00:21:20,000 --> 00:21:22,000
It's an attachment.

222
00:21:22,000 --> 00:21:27,000
The other thing is if you do stay with them and if they do stay with you,

223
00:21:27,000 --> 00:21:31,000
you become a monk and they are still very interested in you.

224
00:21:31,000 --> 00:21:33,000
I know married couple.

225
00:21:33,000 --> 00:21:37,000
I know a married couple where he's married and he goes to visit them.

226
00:21:37,000 --> 00:21:42,000
He goes to visit his wife, he goes to visit his daughter, his granddaughter.

227
00:21:42,000 --> 00:21:45,000
And he acts like a very good monk.

228
00:21:45,000 --> 00:21:46,000
He is a very good monk.

229
00:21:46,000 --> 00:21:47,000
I'm quite impressed by him.

230
00:21:47,000 --> 00:21:51,000
His wife is a very good lay Buddhist and very much interested in meditation practice.

231
00:21:51,000 --> 00:21:52,000
And they get along.

232
00:21:52,000 --> 00:21:59,000
I mean, you kind of get the feeling they have a bit of a content, a bit of a difficult relationship.

233
00:21:59,000 --> 00:22:02,000
Or they had a bit of a difficult relationship even before he remained.

234
00:22:02,000 --> 00:22:05,000
But I went to, when I go to Bangkok, I will visit.

235
00:22:05,000 --> 00:22:11,000
Probably I'll see them, see his daughter again and probably his ex or his wife.

236
00:22:11,000 --> 00:22:16,000
He might even see him if he comes down from Chengway.

237
00:22:16,000 --> 00:22:27,000
And so I think he's done them a world to provide them with someone who's in the monastic circle.

238
00:22:27,000 --> 00:22:30,000
So that whenever they want, they can go and practice.

239
00:22:30,000 --> 00:22:34,000
And they're that much closer to Buddhism as a result.

240
00:22:34,000 --> 00:22:37,000
At the very least, they have a reminder.

241
00:22:37,000 --> 00:22:43,000
So I certainly can't see how getting married with someone could be better than becoming a monk.

242
00:22:43,000 --> 00:22:48,000
Unless, as you say, as I say, you're probably attached to this person.

243
00:22:48,000 --> 00:22:52,000
Then the help doesn't have anything to do with being married to them.

244
00:22:52,000 --> 00:22:55,000
The help comes in spite of getting married.

245
00:22:55,000 --> 00:22:58,000
I would say, if you want to get married, that's the attachment.

246
00:22:58,000 --> 00:23:00,000
That's not out of the time of wanting to help them.

247
00:23:00,000 --> 00:23:02,000
Doesn't mean that you can't help the married.

248
00:23:02,000 --> 00:23:07,000
But the marriage part, I don't think.

249
00:23:07,000 --> 00:23:09,000
Because it's based on attachment.

250
00:23:09,000 --> 00:23:17,000
And this is why the whole thing about this is why we have this crazy situation

251
00:23:17,000 --> 00:23:20,000
where Christians get married to Buddhists.

252
00:23:20,000 --> 00:23:22,000
We're talking about this yesterday.

253
00:23:22,000 --> 00:23:26,000
Lama Kunga said, what did he say?

254
00:23:26,000 --> 00:23:32,000
Isn't that absurd or something? Don't understand that at all, I think he said.

255
00:23:32,000 --> 00:23:39,000
Can't hear something very clever to say about it.

256
00:23:39,000 --> 00:23:42,000
And the point is that you don't...

257
00:23:42,000 --> 00:23:47,000
How that can happen is because you don't marry someone or have a relationship with someone

258
00:23:47,000 --> 00:23:52,000
because it's not a spiritual choice.

259
00:23:52,000 --> 00:23:55,000
You make the choice based on attachment.

260
00:23:55,000 --> 00:23:58,000
People get... I mean, that's generalizing.

261
00:23:58,000 --> 00:24:01,000
And you say you have your reasons for getting married.

262
00:24:01,000 --> 00:24:08,000
But I would question them or I would ask yourself

263
00:24:08,000 --> 00:24:18,000
if you're sure that that's something that actually helps or conversely puts you

264
00:24:18,000 --> 00:24:24,000
in a situation where you're no longer able to objectively help this person

265
00:24:24,000 --> 00:24:31,000
or other people.

266
00:24:31,000 --> 00:24:36,000
Anyone else?

267
00:24:36,000 --> 00:24:51,000
Well, that never been married, so I don't feel like I should bestow advice on marriage.

268
00:24:51,000 --> 00:24:55,000
Oh, come on.

269
00:24:55,000 --> 00:24:59,000
I've never been married and I can bestow advice on that.

270
00:24:59,000 --> 00:25:05,000
Well, sometimes the worst people to bestow advice are the people who are in the thick of it.

271
00:25:05,000 --> 00:25:08,000
If you're married and you're doing it all wrong,

272
00:25:08,000 --> 00:25:14,000
well, I suppose you could still help people what's wrong.

273
00:25:14,000 --> 00:25:22,000
I don't know. I mean, when I think about the value of marriage,

274
00:25:22,000 --> 00:25:27,000
I think it basically is just two people sharing their experience together.

275
00:25:27,000 --> 00:25:30,000
And in and of itself, I don't think there's anything wrong with that.

276
00:25:30,000 --> 00:25:35,000
That could be a good thing, a great thing.

277
00:25:35,000 --> 00:25:45,000
But I see a lot of insecure people looking to the other person to complete them.

278
00:25:45,000 --> 00:25:48,000
You know, the old, you complete me.

279
00:25:48,000 --> 00:25:54,000
And I'm not so sure if that's such a good way to look at it

280
00:25:54,000 --> 00:25:58,000
as one person completing the other.

281
00:25:58,000 --> 00:26:03,000
I don't think there's anything wrong with sharing your experience with another person.

282
00:26:03,000 --> 00:26:13,000
But I don't quite get on board with the whole idea that someone else completes you.

283
00:26:13,000 --> 00:26:37,000
And I don't think that's a good reason to get married because you're looking for someone to fill the void.

