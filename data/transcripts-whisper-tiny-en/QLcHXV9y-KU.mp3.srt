1
00:00:00,000 --> 00:00:12,000
Hey everyone, just another video, quickly to announce that we have created a video archive

2
00:00:12,000 --> 00:00:21,000
for the Ask a Monk, Monk Radio, Question and Answer videos that I've made over the years.

3
00:00:21,000 --> 00:00:33,000
So, there's hundreds of them, and I'm not sure I don't go looking for them myself, except when I need a particular one that I know what I'm looking for.

4
00:00:33,000 --> 00:00:43,000
So, I'm just assuming that it's difficult to find the ones that you're looking for.

5
00:00:43,000 --> 00:00:48,000
I think especially because I see the same questions being asked again and again.

6
00:00:48,000 --> 00:01:04,000
So, it seemed to be quite worthwhile to somehow categorize and offer the videos up in a more organized fashion than YouTube allows easily.

7
00:01:04,000 --> 00:01:13,000
So, with that in mind, I created a page on our website that has a list of videos.

8
00:01:13,000 --> 00:01:22,000
And it's been put together by volunteers who have been following my web blog and Facebook.

9
00:01:22,000 --> 00:01:28,000
And it's been adding videos over the past week or so.

10
00:01:28,000 --> 00:01:32,000
And it now has, I think, 700 videos or so on it.

11
00:01:32,000 --> 00:01:37,000
And there may be duplicates because some videos fit in more than one category.

12
00:01:37,000 --> 00:01:43,000
But the point is that those videos are up now, and they're listed.

13
00:01:43,000 --> 00:01:45,000
They're all still hosted on YouTube.

14
00:01:45,000 --> 00:01:47,000
It's just that we've organized them.

15
00:01:47,000 --> 00:02:02,000
Hopefully in the manner that allows it, allows easier finding of the videos that you're looking for the subject that you have a question about.

16
00:02:02,000 --> 00:02:10,000
So, go over there if you're looking for something on a particular topic or even if you're not just going browse through them.

17
00:02:10,000 --> 00:02:16,000
And I think a lot of people will be surprised to see just how many videos there are.

18
00:02:16,000 --> 00:02:24,000
And probably find videos that they had never watched before on topics that they were interested in.

19
00:02:24,000 --> 00:02:26,000
So, hopefully that's a good resource.

20
00:02:26,000 --> 00:02:32,000
I think it's something that's highly overdue and something that we'll keep adding to in the future.

21
00:02:32,000 --> 00:02:39,000
If you'd like to help, there's a special admin page that many people have been using to actually add videos there.

22
00:02:39,000 --> 00:02:48,000
But I'm not going to make that too open because I figure if there's too many people adding to it, then it just gets chaotic.

23
00:02:48,000 --> 00:02:58,000
So, if you'd like to help, and you know, a video that should be on there, or you see something wrong with the page, just let me know and I'll send you the link.

24
00:02:58,000 --> 00:03:03,000
And you can help out, otherwise, head on over there.

25
00:03:03,000 --> 00:03:14,000
The URL is in the description to this video, but it is simply video.ceremungalow.org.

26
00:03:14,000 --> 00:03:21,000
So, if you head on over there, you can access the list and see what you think. Let me know.

27
00:03:21,000 --> 00:03:26,000
I hope that's useful. Thank you all. Keep meditating and keep practicing.

28
00:03:26,000 --> 00:03:48,000
And be well, everyone.

