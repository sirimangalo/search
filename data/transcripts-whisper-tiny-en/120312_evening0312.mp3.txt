One of the greatest things that we find when we first learn about the Buddhist teaching,
I think, is the sense of empowerment that it brings.
The sense of being in control, in sense, is to talk about how everything is unattised and
controllable, but unattised, really, if it's on an experience, it's has to do with our experience
of things.
On a conventional level, Buddhism puts us very much in control.
We're just saying you're only live once, we're saying that you only live once is a license
of course to do whatever you want and to not take life seriously, to just make it a good
time, but it's actually also quite scary, I think you've only got one chance to live.
Buddhism is quite empowering, or the idea of rebirth, for example, quite empowering,
because it says you have another chance, there is no, you never hit the bottom, you can
just keep falling and falling until you find your wings, there's no dead end, there's no
full stop at the end, it's quite encouraging for people to hear this sort of thing, especially
if they've been taught to believe that you only have one chance, and it's empowering
in another way to teach in on karma, of course, very empowering, because it says you're
in charge of your destiny, you'll always think of karma as relating to why I have to suffer
so much in this life, so instead of feeling sorry for yourself, you can feel guilty, but karma
is a diet, I come with love, karma is teaching to help us to see how in power, how much power
we have, how much power our actions have, and in fact not to feel guilty or sorry for
ourselves, but to take it, to be invigorated by it, why is it they're wallowing in self-pity
when you can do something about it, that you'll never help, these two teachings combined
are incredibly empowered, the fact that you have the time to do something about it, and
you have the ability to do something, to change your life, to change who you are, we should
never feel useless or hopeless, we never have reason to feel hopeless, if the path is going
to take us a million lifetimes to reach the goal, we have millions and millions and millions
of lifetimes to ahead of us, we have uncountable lifetimes ahead of us, so a million
doesn't seem so bad anymore, it's impossible to be hopeless, because you always have more
lifetimes, more time to learn, more time to develop, for someone who hasn't begun
to develop themselves or hasn't set themselves on the idea of developing themselves and
making themselves a better person, then of course it's possible to spend an eternity
just running around with circles, but rather than feeling discouraged by slow progress
or minimal results, pitifully small, better than realizations and seemingly a poor
practice, we should be invigorated by how possible it is for us, how doable, we have to
think of the long term, maybe it will take a long time for us to become amazed by putting
one foot in front of the other, by working continuously we can make it there, this is something
that should be invigorated, it should give us encouragement and keep us moving on the path,
it's only when we feel like it's hopeless that would stop, but if you know that you can't
go wrong, you can't run out of time, you can't fail, the only way you can fail in meditation
is by stopping, if you stop meditating, if you stop developing yourself if you stop
straightening your mind, stop learning about yourself, learning about experience, then
you go back, running around in circles, or staying foot, if you never move, you'll never
get to the goal, but here we have the time, we have the power to reach the goal, everyone
does.
So with this power and with this potential, our first task as human beings is to reflect
on how we best spend our time, and you find this is a large part of the question that
we have as human beings, what is the meaning of life, what should I do with my life,
what should I make of my life, here I've been, I've found myself in this state of being
a human, because of that of my life.
I think what's exciting or encouraging heartwarming is how we found a way to live our
life, that somehow manage to come to this way of life, that is quite meaningful and special,
and is taking the bull by the horns, taking life to task, taking life seriously, and here
we have this time, we have this life, we have this strength and this energy inside,
then putting it to good use.
This was the Buddha's understanding of the Buddha's knowledge of what it means to live
a blessed life, or to have your life be a blessing, and so one way of understanding
our practice and our goal, our way of finding meaning in life, is that we should make our
life a blessing, that our life should be a blessing both to ourselves and to others, it should
be something special.
Once you see that you have the power, the time and the power and the ability to affect
your life, then it seems obvious that you should make your life a blessing, but I think
this is important to say is because without this feeling of empowerment and this feeling
of the potential, this is what leads people to lose their dreams and to lose the give
up the idea that they might be special, to feel like they're all their capable of, there's
an ordinary life.
How many people just live an ordinary life because they feel that's all they're capable
of, because they don't see that they too are capable of a life, a blessing, a special
life, a life that is blessing to the world, a life that is a blessing to themselves,
to other people.
We have that potential, this is the exciting thing, we all have the potential, but if we
don't know it, if we don't realize it, if we don't have the encouragement and the confidence
in ourselves, we'll never realize this potential.
We have the potential at all times and that how many people never realize their potential.
Here we've come together out of a some knowledge, we've been not perfect knowledge, but
understanding that we have this potential, that we have the power to shape our own destiny,
that this is possible, that it's possible to become free from suffering, it's possible
to become enlightened, it's possible to reach the goal of existence or the pinnacle of
existence.
This is possible for all beings, it's just a matter of time, how long it will take, and
whether they actually begin, and they actually strive and utilize or see their potential
to fruition, so it's quite encouraging and something that I'd again like to
express my appreciation to see what we're doing, what is happening in here and in this world
and remember the circles that people are trying the way that most people never try to
become special, to become blessed, to make something of their life beyond just money and
possessions and sensual pleasures and beyond all the age sickness and death beyond just
living your life to have a good death, not living your life to become eternal, to become
immortal, giving your life to see through death, to see through all the age, to see through
life, to see through life life lives, to see through birth, to see through birth, to
never feel discouraged or unworthy or useless or poor meditators, you should never feel
like we're incapable or substandard, should never be afraid that we're not as good as other
meditators or not as quick or not as, after capable or so, I should feel confident
that we should feel proud of ourselves, encouraged, from my own point of view it's quite
exciting, it's quite encouraging, from myself to see something feel great joy or
rising inside, rejoicing and seeing people really working to make themselves a better,
a kinder, wiser people, something we shouldn't be afraid to be in a sense proud of or
confident, because this is what we're aiming for, we're not aiming to lead to, to
head ourselves, we're aiming to, to angry or to deny ourselves, we're aiming to be, in
light, to be noble, something special and all, we all have this potential, we have to
encourage it in ourselves and another.
So, the Buddha had much to say on what it means to live a blessed life, what it means
to live a special life, he gave this talk with his very famous discourse on the blessings,
and if you look at it, you can see really how to live your life in a blessed life, it's
the perfect outline for how to live your life, how to live your life of blessing, live your
life as a blessing, live a special life with a noble life, so it's important for us to
remember these things, first of all, for guidance, of course, first and foremost, to guide
us away from improper or deleterious practices, to make us become based in pure and to encourage
us, to guide us towards those practices that are beneficial both to ourselves and others
and really improve both our own lives in the world around us, but yet if the other useful
thing is to give us encouragement, when we see that our lives are special because of
this, that our lives do have blessings in this is called counting your blessings, so if
we talk about counting your blessings in Buddhism, we have 37 blessings that we can
count in the Mahamanga, which is a very popular suit that most Buddhists who were raised
Buddhist, were heard and often times memorized at some point in their lives, so in that
we chant quite often at ceremonies and blessings and so on, because it is a story about
blessing, so in the time of the Buddha, everyone was asking, what is a blessing, what are
blessing, so some people said when you hear this bird or that bird, it's a blessing,
some people said when you see this or see that, it's a blessing, these omens, some people
said the horseshoe is a blessing or a rabbit's for this blessing, lucky, these are things
that are able to have some magical power to them, everyone had a different idea about
what was lucky, what was the ability to ward off evil, because the word is manga means
evil, that means to describe, kill, to vanquish the evil, so for example when you say
something unlucky, knock on wood, and that's a manga, because it's able to describe
people say it's able to get rid of the current of the jinx, they say when you put a horse
or horseshoe over your door, it's able to ward off evil when you wear a cross or something
around your neck, it's able to ward off evil, vampires and so on, and so everyone was asking
this, this was a philosophical question that got the angels and the angels took it up,
that was the topic of conversation, they went all the way to the king of the angels to
stack up, it came to the angels who were living about in the namapada, and then he told
them to go to see the buddha, and the buddha is the one who knows, and so one angel went
to see the buddha and asked him, send that everyone's wondering what is buddha the blessing,
and buddha told buddha the 37 blessings in mind, and so it became quite a famous, well remember
in this course, and I can start working go through some of these here maybe tonight we'll
go through some and then the next day is we can go through some and try to finish them all,
just to give us an idea of how our practice should progress, talk about wholesome qualities,
some of the greatest assistance for the development and the cultivation of mind, of course,
is the discussion or the talk, listening to talk or even discussing wholesome and unwholesome
qualities, profitable talk, talking about good things, encouraging each other and good things.
So the first blessings are a savannah kebala and not associating with wholes, with unwise
people, with foolish people, but in the development of savannah associating with wise people,
the buddha's are poisoning and associating, paying homage to those worthy of homage.
This is the first stance I can see.
So thinking about these ones first off, that should really give us confidence in what
we're doing and how lucky we are to have found the community where we don't want to
brag, or it's not to say that we're special, but that the people here are not fools.
If you look around this monastery, you don't see fools, don't see foolish people, and
you might say, what the Hindu can be foolish at times, but we know, from a Buddhist point
of view, we know what it means to be fools, it doesn't mean to joke or to laugh or to
be simple or to be unintelligent and unlearned and math or science or so, the fool means
someone who does, who's engaged in evil deeds, drinking and killing and stealing and lying
and cheating or drugs and alcohol gambling, engaged in the buttery and partying and all
of these things, we're able to avoid so many unwholesome relationships just by being
here in this world, we depend on the people around us, we rely on them, we rely on
community, we rely on society, and so often the society, the community itself becomes
a danger to us, we feel so it's cheap, it's like flyers and like burgers, rapists and
all kinds of evils in the world, and look what we found, we found a small group of people
who share a like interest and that interest happens to be the development of purity,
the development of goodness, where will you find a group of people who are actively working
day in and day out to remove the anger from their mind, remove the greed from their mind,
remove conceit and reviews and so you might have all of these things, but what are they
doing?
They're activity day in and day out, so they become better people to make their life
a blessing for themselves and for others, we have that, we have the community, not to feel
complacent or brag about it as well, but to feel good about ourselves, so to feel confident
that we can do this, we have blessings already, pull down at the ports and being ambushed
those worthy of homage, this in Buddhism in the world, I mean this is in religious, in
a religious sense, this is something that occurs in all religions, being respect to God
or playing respect to your religious leader, if it's a cult or so on, you pay respect
to the cult leader, and even in Buddhism we talk a lot about being respect to the Buddha,
how great it is to bow down to the Buddha and to recite the Buddha's qualities and so on,
and this is an ordinary way of paying homage that you see in all of it, so it's worth
discussing and respect in general is worth discussing, because very common thing nowadays
to find that people don't want to respect their teachers or don't want to respect their
elders, don't want to respect anyone in terms of putting anyone higher than themselves,
the Buddha really means putting something higher than themselves, and on an alternate reality
level, there's a good point there, but the problem is people are unable to separate
alternate reality from convention, and we can't live just on the alternate reality level,
we can't function, it's lying to yourself, we can't even talk without using conventions,
so when you try to talk in terms of alternate reality, like we're all equal because everything
is equal, you're complaining to two levels of reality, you're talking about beings,
and yet you're talking about them as if they were alternate reality, so you're just lying
to yourself, the only way to exist on a conventional level is to have respect for those
people who have given you support and who have been a guide for you, and so on, so this
is one important reason why this is a blessing in all of it, since in all, not even just
the religion, in all facts, facets of life, all aspects of life, this is why paying respect
to your parents is a blessing, because on a conventional level that is true, they are your
own God, they are the God for the children, they are the ones who provide for the ones
who created us, it's all conventional, but we don't discard the conventional way, all of
our lives is convention, even the lives of our hands, it's all convention, even though
they see alternate reality, they don't go around saying, I'm not your husband, I'm not,
that's not my column by their name, they say, I'm not that name, I'm just root by
an animal, they don't say that, and someone calls them by their name, they only accept
that that's their name, when they talk about people, they talk about them as people,
saying armages on a conventional level, it's how we have to live our lives, if you want
harmony, if you want peace, if you want to really and truly develop and cultivate this
blessed life, you have to be respectful and humble, how it creates humility to put someone
above you, and in the Buddha's time there's a good example of this, the Buddha's relatives
all decided to go forth, it was Anand and they were that time Anurunda and Madhya and
Upali, there were a couple of others in the University of Pali, Upali wasn't one of its relatives,
Upali was their barber, and they asked Upali to go with them, to escort them, to be their
bodyguard, sort of, as they left, not exactly bodyguard, but their messenger, because
what they did is then they took up all of their ornaments and so on and gave them to Upali
and said, take these back and tell everyone that we've gone forth, and you'd be the messenger
to let them know that we've gone forth, and Upali, as they were walking away, Upali looked
at the ornaments and said, if I go back with these, people are going to think I killed
them, or something, if I were going to think that maybe I did something bad happened to
them, said I'm not going to go back, I can't go back, and he said I want to go and be a monk
as well, so he decided that he would join them and he asked permission of, they gave
him permission to join them, but then they had a problem because they said amongst themselves
that if they went forth and Upali went forth as well, they'd still always be thinking
about him as a servant, as their barber, and he said this would be something that gave
them consent and would be difficult for their practice, so what they did is they went
to the Buddha, and they asked the Buddha permission to allow Upali to ordain before them,
and the Buddha gave this permission, and Upali ordained first, and this means that Upali
was senior to all of them, and they all had to bow down to Upali, and this was their idea,
this was their plan, to help them to become free from conceit, you see how important
and how serious they may took this one, some people are unable to respect others, unable
to put people and there's about them, their teachers, I know how it is, in high school
they always thought that our teachers were just people as well, what need should we have
to respect them, we think they're getting paid and they're doing it because they would
get the money or so, just such a horrible way to think, certainly not the truth, but
we treat them poorly and with no respect and they have to use rules and discipline to keep
us in line, it's quite horrible really, so this is one way of understanding this one,
but of course in a Buddhist sense nothing is, nothing can ever be left in the conventional
level, so the Buddha himself didn't rest at that, of course he didn't ever deny it and
he was very clear that on a conventional level this is important, this seniority and paying
respect to those who have helped him before in parents and teachers and if you've been
just anyone who's supported you, sorry put the for example, sorry put this was the best
example I think of of gratitude, because his first teacher asked him, every day he would
bow down, if you knew that asked him to use a misdirection or that down he would bow down
in that direction, out of respect for us, but it wasn't only his teachers, one time there was
this one man who nobody would ordain him, I think probably because he was old, even older than
Paul and so they didn't want to ordain him, no he must have been quite older, and I think
there were other reasons maybe he had some kind of conceit or so on, so they didn't want
to ordain him, and I think there was a rather reason like he was serving them, so they were
happy that he was a layman, that's what it was, he was so useful to them, and they needed a
steward, so they didn't want to ordain him, let's see if I didn't want to stir, it's not even
nowadays, you have the steward who wants to ordain what they think, well if you are
ordain then we've got no steward, so they push them not to ordain, wasn't that he was old,
and so that he went to the Buddha and he asked, you know, when am I going to ordain, remember
they said, well there's no one here who will ordain you and everyone was quiet, and he says
there's anyone here who can think of a reason to ordain him, and so he put this spoke up,
he said, one time he said, all ordain him, and this Brahman one time he gave me a spoonful of rice,
and this is a story that we tell even nowadays that over a spoonful of rice, sorry put
those willing to do what nobody else would do in ordain this guy, this is the reason why
sir he put it aside, spoke up and said he would ordain him, because he said this Brahman has
given us a single spoonful of rice, probably not even not enough to eat, not enough for a meal,
just that, and he had this kind of respect, so anyone who's helped us, anyone who's given us
things, people who've supported the monastery or supported the meditation center,
these in a sense, there should be a certain sense of pooja there as well, where we have
homage, we respect and pay homage to these people, and when they come to the monastery we always
think of how helpful they've been to us, and as a result we always make a wish in our hearts
that somehow we can give something to them and teaching them and so on, at least being hospitable.
So this is on a conventional level but on an alternate level, the pooja that we're
exhorted to undertake by the Buddha, the Paramaya, Parama pooja, the highest pooja, the highest
form of homage, it's called pati pati pooja, it's always here but it's like that saying
imitation is the most sincere form of flattery, it's along the same lines really, imitation is the
most sincere form of homage, practicing someone's teachings, practicing the teacher's teachings,
following the teacher's advice is the most sincere form of homage, when we practice the Buddha's
teaching, when we follow the Buddha's teaching, this is what it means to pay homage to the Buddha,
so in that sense on an alternate level it's not even the matter of thinking of the Buddha as
higher than us but it's acceptance and agreement in our minds, bringing our minds in line with
what the Buddha taught, that is in and of itself an homage or obviously a respect on the highest
respect is making or emulating the life of the Buddha, emulating the mind of the Buddha to our best
ability.
So this is the Buddha's end, these three the Buddha's end, these are three of life's greatest
blessings and here of course you can see quite clearly we have all three of them, this is something
that when we think of this tonight, that here we have a, we have set ourselves, we've made
this wish and this intention and we have set ourselves in a place and in the community.
We have made a determination in our minds that we're not going to live amongst the foolish
people who spend their lives drinking and gambling and carrying on like there's no consequences
of action, people who don't understand karma, don't see that life is not static, the mind is not
fixed and all of our actions affect our mind, these people who corrupt their minds and corrupt the
people around them and create stress and addiction and friction and conflict in the society
and the community around them, we've decided we want to leave that behind. The Buddha gives
this story about that's supposed to happen sometime in the future when the majority of society
will decide we'll get so based that they will lose all sensuality and they will
copulate like animals and treat each other like animals raping and
taking and stealing and taking advantage of each other like animals and killing each other like
animals hunting each other like animals. There will be constant state of war but he says there
will be a certain faction of society of the world that decides this is not for them,
decides that they don't want to take life, they don't want to engage in such a horrific act
and goes off and lives in the forest and it's these people he says that will begin the new
society will begin to change the world. It's always something to think about that we're a part of
that society. We're a part of that tradition that will be carried on and who knows if none of us
then maybe the people we, the people we inspire will be those people. The reason why those people
will exist is because we're carrying on this tradition now people like us are carrying on this
tradition where we have broken through broken free from the feelings of helplessness from the
addictions of the hopelessness that people come to thinking that the best they can ever hope for
is a life of sensuality and the life free from physical discomfort.
We have nothing higher in their mind than these base desires and base addiction.
Don't ever think to practice meditation, don't ever think to make themselves better,
don't ever think to respect the truth. When we talk about respecting for the Buddha we talk about
respecting is we have to think of respecting the truth. This word respect has another meaning
like when you respect the law or when you respect the laws of physics. Everything has to respect
the laws of physics that can't break them. If we try to break them we find we only meet with
failure, try to break the laws of physics disrespect the laws of physics and it only comes back
to bring frustration. The same goes with the extended extension which is the laws of the mind
if you try to break these laws if you don't respect the truth which is what we mean by
by paying homage really on an ultimate level what we mean by paying respect to those
or those things where they respect such as the being the truth. The truth is immutable
and it's unbreakable so it's true that when we have impurity in our mind it will lead to
impurity it will lead to suffering in the future. When we develop purity in our minds it will lead to happiness
so this is on an ultimate level what we mean by the respect, by paying respect. This is why the
Buddha said it is practiced because our minds should be set on becoming our harmonious
and in harmony with reality on an ultimate level that's the highest form of respect that exists
is the respect for the laws of nature, respect for the laws of the mind and the laws of
our mind and action and the laws of existence and so it actually really just means wisdom
to understand reality because when we understand reality we act according to a person who
knows the truth will never act in a way to harm them so if we know that something is going to
cause a suffering will never do it. The only reason why we cause ourselves suffering
is because we don't see clearly that it's causing us suffering. It can't be that if we knew
it was the cause of suffering that we would perform it. If we knew that it was something bad we can
never logically remember. So our practice is what it's meant here. This is these are great blessings
to have the, as came up today, the idea of having a teacher. That's not even just having a
teacher. It's been associated with good people to have these reminders, even just to have a group
to practice meditation. But of course to hear the dhamma, to hear anyone speaking the dhamma,
it's really a much easier thing than to actually practice the dhamma. So encouraging people to
become teachers I think is a very important thing. People always think that I'm not qualified to teach
or I'm not a very good practitioner. How could I possibly teach? Teaching is much easier
than practicing. All meditators should engage them. Not because they want to be teachers.
Not because of the importance of place and living in blessed life. We're going to surround ourselves
with good people. The only way we can do this is to be a good person ourselves. If we want to
be surrounded by people who are kind and generous and helpful and of course kind and generous
and helpful of course on an alternate level means in terms of dhamma. People who are kind enough
to support our practice, people who are helpful enough to be able to guide our practice.
We want to be surrounded by such people. We have to be such a person ourselves.
We can't just take and expect never to have to get it. It's part of the law of karma,
part of reality. You must be like the people you wish to be around. So if you want to be
around people who are good teachers, you have to be a good teacher yourself because I think
one of the great karmas that must come from teaching people is that you'll never be without
a teacher yourself. You'll never have to meet. You'll never have to want for a teacher. I always
feel how blessed I was to have such a wonderful teacher who I consider to be such a wonderful
teacher. So I'm very happy to be teaching other people because I know that if all it means is that
I get to meet people like him in the future and that's enough for me to make it part of my life.
In the future I will always have those people who can guide me.
So this is a three of life's greatest blessings. We kind of overlook them sometimes and think
how simple they are. But the Buddha was very clear that this is the whole of the holy life.
The whole of the spiritual life is associating with people who are spiritual.
And this associating for people who are unspiritual because we guide each other. We help each other.
We can't see our own eyes. Without a mirror we can't see our own face.
And without wisdom we can't see our own mind.
So for us to develop wisdom is something that requires assistance, requires guidance,
requires support. That's what we have here. So these are the first three blessings.
We can just try to get through the soup to eventually at 34 left to go. Isn't that exciting?
Here we have three of life's greatest blessings and that's not it. There's 34 more
that we can be happy about and feel good about and feel confident about that.
Here we are developing all of these wonderful things. And if we're not different lacking in some of them,
now we'll know what they are. Now we'll now realize and adjust ourselves appropriately.
So I think that's a good start on the manga to sit there. We can finish this recall
day by day. So now on with our practice, we listen to the dummies like reading the road map.
We have to walk the path where we have to actually follow the path.
So listening to the dummies, useful listening to the dummies out for
but we have to put it into practice and our practice shouldn't be intellectual and shouldn't be.
It shouldn't be just based on suit does or what we've planned or what we've heard.
So we have to be careful sometimes that it can be just an intellectual thing.
But I don't think we have that really that problem. It's just a standard warning that I think
is worth giving, worth us keeping in mind that we're here to find the wisdom within ourselves,
to find the truth inside of ourselves.
But I know that this is what we're doing and this is what is so inspiring about being
in this type of community and to something that gives one encouragement in working to maintain
and working to support this kind of community, working to cultivate and to protect this kind
of a place to at the very least to express appreciation and find. We should be clearing our minds
this is a blessing that we have here, something that's not easy to find and to make the best of it
and to use our time you're wisely, not let the moment pass us by.
Can all mama up at the ground, don't let the moment pass you by.
So here we have the time that's made the most of it.
So there's a group we want to practice with in mind for frustration and then walking in the
room today.
