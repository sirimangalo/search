1
00:00:00,000 --> 00:00:07,000
Do you think it's worth coming to your monastery to meditate for a shorter time than the foundation course?

2
00:00:07,000 --> 00:00:13,000
I can't leave my job currently and the break I'm given is too short.

3
00:00:13,000 --> 00:00:15,000
I think that's Peter from Canada.

4
00:00:15,000 --> 00:00:23,000
Someone else has this recently and the only problem is that you're looking at at least a week of jet lag.

5
00:00:23,000 --> 00:00:29,000
Some people seem to claim that they can get over jet lag quicker, but

6
00:00:29,000 --> 00:00:37,000
if you only come for like a week then especially as you get older it's difficult to

7
00:00:37,000 --> 00:00:45,000
really focus on the meditation and get a clear practice for that first week.

8
00:00:45,000 --> 00:00:51,000
If on the other hand this is Peter from Sri Lanka then for sure we've got three Sri Lankans

9
00:00:51,000 --> 00:00:57,000
here now a mother and her son and daughter who are here for a week and they're meditating

10
00:00:57,000 --> 00:01:04,000
and then we have Malanta who's coming on the 17th for just five days or seven days or something.

