1
00:00:00,000 --> 00:00:22,920
Good evening everyone, we brought casting live to lie 10th, 2016, the day is quote, it seems

2
00:00:22,920 --> 00:00:30,200
like we've had this quote, I mean we've actually had this quote, maybe it's been a year

3
00:00:30,200 --> 00:00:41,880
already, but had a quote like this, problem with this quote is it's only part of a quote,

4
00:00:41,880 --> 00:00:48,080
and it's a bit misleading at that.

5
00:00:48,080 --> 00:01:00,960
So the quote that we have is that one can have a sadhan, sealhan, suithan, sadhan, one can have faith

6
00:01:00,960 --> 00:01:09,720
in the Buddha, one can have sealhan, one can have morality, and one can be learned and heard

7
00:01:09,720 --> 00:01:19,760
much of the Buddha's teaching, but one's still lacking in one regard, one respect, one

8
00:01:19,760 --> 00:01:27,000
is not a teacher of the dhamma.

9
00:01:27,000 --> 00:01:36,560
So it sounds according to this that there are these four aspects.

10
00:01:36,560 --> 00:01:47,120
And one has all of these, then one is complete with us, it's not what it says, it says

11
00:01:47,120 --> 00:02:05,440
it means it shouldn't be because what it means is in regards to that, through that

12
00:02:05,440 --> 00:02:15,520
one is more fuller or one is full in regards to that, that factor, see the way it has

13
00:02:15,520 --> 00:02:26,240
to be translated as this latest factor makes one more complete, because there's lots more

14
00:02:26,240 --> 00:02:28,240
factors.

15
00:02:28,240 --> 00:02:35,240
This is actually, and with their Nicki book of 10, there's actually 10 factors.

16
00:02:35,240 --> 00:02:44,440
So this quote is misleading, not that the only thing missing from a good monk and a good

17
00:02:44,440 --> 00:02:50,600
list is that they're not teaching, no, there's lots of things missing, so we have seed

18
00:02:50,600 --> 00:02:59,880
of one might, it starts off by saying one might be moral, one might have confidence

19
00:02:59,880 --> 00:03:14,360
in the moral, one may be confident, but not moral.

20
00:03:14,360 --> 00:03:22,040
My faith in the Buddha but not be moral, so here's a person who calls themselves Buddhist

21
00:03:22,040 --> 00:03:29,240
and there's real faith in Buddha having heard his story and his teachings and really having

22
00:03:29,240 --> 00:03:34,520
met the Buddha or having met Buddhist monks, their great faith in the Buddha, they don't

23
00:03:34,520 --> 00:03:39,520
actually do anything, so they're still killing and lying and cheating and stealing and

24
00:03:39,520 --> 00:03:45,480
all this.

25
00:03:45,480 --> 00:03:53,600
In this regard there's the lacking, but then even if they have, even if they're moral,

26
00:03:53,600 --> 00:04:02,520
they're complete in that regard, what if they're not well-learned by those subdomains

27
00:04:02,520 --> 00:04:06,760
of much learning, having heard much of the Buddha's teaching, they're going to learn

28
00:04:06,760 --> 00:04:11,440
much of the Buddha's teaching, well they're lacking in that regard, so there's many questions

29
00:04:11,440 --> 00:04:16,480
that they might have and others might have that they can't answer, they might be challenged

30
00:04:16,480 --> 00:04:23,720
as it's very hard to answer, the challenge put by others or even by your own mind when

31
00:04:23,720 --> 00:04:30,480
you have doubts, you haven't learned a lot, haven't learned all the different ins and

32
00:04:30,480 --> 00:04:37,400
outs of the teaching, so who are's to that when they're still not complete, but then

33
00:04:37,400 --> 00:04:46,120
one can be well-learned, no Jeddamu katipo, and this is where this is the part that of the

34
00:04:46,120 --> 00:04:51,240
quote that he's pulled out, but one still isn't teach the number, there's one that

35
00:04:51,240 --> 00:04:55,320
doesn't teach the Dhamma well and it's not really complete in that regard, in that regard

36
00:04:55,320 --> 00:05:01,360
it's still lacking, so it is true that the Buddha is saying here that maybe it's just

37
00:05:01,360 --> 00:05:06,480
for months or maybe we can all take a lesson from this and that, it's inferior, if you're

38
00:05:06,480 --> 00:05:12,680
not helping others, if you're not sharing the Dhamma, if you're not using it for the benefit

39
00:05:12,680 --> 00:05:18,680
of others, well it's somehow lacking, there's something you can't become enlightened

40
00:05:18,680 --> 00:05:28,720
for yourself, but I mean, there's something to this sort of natural inclination to teach,

41
00:05:28,720 --> 00:05:36,560
the natural inclination to help others, teaching a lot today, in this morning we went

42
00:05:36,560 --> 00:05:45,400
to Mississauga again, this ordination, today was the second half of the whole thing and

43
00:05:45,400 --> 00:05:52,520
I was able to give a short talk, I'm really happy about that, it was really good to

44
00:05:52,520 --> 00:06:01,160
be that, it's been spiring to offer this inspiration, that's what this suit does about

45
00:06:01,160 --> 00:06:08,960
it's about being an inspiration, inspiring confidence in all respects and who is

46
00:06:08,960 --> 00:06:16,080
completing all aspects, so we don't get to that until the end, it's not complete yet,

47
00:06:16,080 --> 00:06:27,920
so this quote is there for misleading, so because one might teach the Dhamma and then

48
00:06:27,920 --> 00:06:39,520
not be one who upholds the winnier, won't the winnier, I'm not sure how that differs from

49
00:06:39,520 --> 00:06:46,640
sea though, but there you have it, winnier and ah, winnier and ah, but there's something

50
00:06:46,640 --> 00:06:51,880
in the commentary about that, that's the difference, but one might be a winnier, winnier

51
00:06:51,880 --> 00:06:58,280
and ah, I guess someone who knows the ins and outs of the winnier, the monastic rule,

52
00:06:58,280 --> 00:07:03,960
it's not the same as sea though, I guess it's real true ethics, but winnier and ah, for

53
00:07:03,960 --> 00:07:09,160
a month, to know the ins and outs of all the artificial rules, like how to wear your

54
00:07:09,160 --> 00:07:17,280
robe or how not to wear your robe and how to carry your bowl, all the ins and outs of

55
00:07:17,280 --> 00:07:26,320
monastic life, that's important, it's important to maintain order and to create a framework

56
00:07:26,320 --> 00:07:38,200
for the training that we undertake as for this month, so this would actually is pretty

57
00:07:38,200 --> 00:07:46,040
specific geared towards months, but you can adapt it for it, everybody has rules of their

58
00:07:46,040 --> 00:07:56,520
community and norms that have to be maintained, meditation centers have rules, that kind

59
00:07:56,520 --> 00:08:06,320
of thing, so but even if one is well versed in community, ah, communal harmony and that

60
00:08:06,320 --> 00:08:17,720
sort of thing, ah, but one isn't an aranyika, one that's seen us and one, one doesn't live

61
00:08:17,720 --> 00:08:25,520
good dwell in the forest, it doesn't dwell in a secluded dwelling, ah, and then one that's

62
00:08:25,520 --> 00:08:32,960
incomplete in that regard, aranyiko chat on that, let's see in a scenario, so there you have

63
00:08:32,960 --> 00:08:37,320
it, we should be living in the forest, because our goal I was just talking to some people

64
00:08:37,320 --> 00:08:43,360
today in Mississauga and talking to the monks and that kind of thing, trying to get a

65
00:08:43,360 --> 00:08:51,120
sense of where we can find a place, ah, what can we do, ah, where can we go in Ontario

66
00:08:51,120 --> 00:09:01,200
and Canada or anywhere, we can build a forest or a monastery, more in nature, there's

67
00:09:01,200 --> 00:09:09,160
more room and less, ah, less of the constraints of being in the city, something for the

68
00:09:09,160 --> 00:09:16,200
future, but even if one dwells in seclusion which is very good, you know, being alone

69
00:09:16,200 --> 00:09:28,480
being in a secluded place, it allows you to enter into states of common, by it, if you

70
00:09:28,480 --> 00:09:34,800
go into the forest and don't enter into the forest, this is the next one, no chat, tapunang

71
00:09:34,800 --> 00:09:50,040
jana, jana, nam, abhite, tasikana, tita dhammasukhui hara nam, mikkam, malaam, when it's not

72
00:09:50,040 --> 00:10:00,520
able to obtain, when it's not become one who is able to obtain with ease this dwelling

73
00:10:00,520 --> 00:10:10,480
happily in the here and now, in reality and in the here and now, that is the forjana, the

74
00:10:10,480 --> 00:10:25,440
abhite, tasikana, higher dhamma, the higher, the higher mind that is the forjana, and

75
00:10:25,440 --> 00:10:31,240
still not, still missing, still lacking, if you can't actually calm the mind, focus the

76
00:10:31,240 --> 00:10:39,760
mind, there's something missing, but you can calm and focus the mind, and there's still

77
00:10:39,760 --> 00:10:50,520
something missing, even then, no chatas, sawa, nam, kaya, nasa, vang jita, vimu, tambanyu,

78
00:10:50,520 --> 00:10:57,880
tang jide, vadambi, zingang, abhinya, satchikattva, for some part, we had it.

79
00:10:57,880 --> 00:11:04,760
One can be calm and tranquil, living in the forest, keeping the morale, the monastic precepts

80
00:11:04,760 --> 00:11:12,880
and so on and so on, even teaching and even have very faith in all these things, but if

81
00:11:12,880 --> 00:11:21,760
you haven't cut off the defalments, the taints, you haven't destroyed the taints as sawa,

82
00:11:21,760 --> 00:11:32,760
nang, kaya, dwelling in the taintless freedom of mind and freedom of wisdom, and you're

83
00:11:32,760 --> 00:11:39,760
still missing something.

84
00:11:39,760 --> 00:11:49,320
And you will say, just a second, we should fulfill a little less being incomplete in regards

85
00:11:49,320 --> 00:11:56,440
to one fact, whatever factor, they should fulfill that factor, thinking, how can I be

86
00:11:56,440 --> 00:12:03,360
in doubt with faith, how can I be in doubt with this and this, then basically you should

87
00:12:03,360 --> 00:12:10,800
work to fulfill that which is missing.

88
00:12:10,800 --> 00:12:17,840
And you said, but a person who amongst who has all these ten things, who has confidence

89
00:12:17,840 --> 00:12:28,840
in the Buddha, who has morality, who has well-learned, who is a teacher of the dhamma, who

90
00:12:28,840 --> 00:12:38,840
is a holder of the discipline, who lives in a secluded forest dwelling, who can enter

91
00:12:38,840 --> 00:12:49,920
in and ease the four genres, and who has cut off the taints, destroying the taints on

92
00:12:49,920 --> 00:12:55,040
wholesome states of mind, that even though I tend to see it from wholesome states of

93
00:12:55,040 --> 00:13:02,280
mind, such a person is complete.

94
00:13:02,280 --> 00:13:12,940
If ItWrite's path, he will increase the intricacies of magic, and maybe he will be

95
00:13:12,940 --> 00:13:21,080
supported through that point, and it's complete in all aspects.

96
00:13:21,080 --> 00:13:26,960
That's the proper teaching here.

97
00:13:26,960 --> 00:13:29,160
Inhroll Culture

98
00:13:29,160 --> 00:13:37,680
This is, although he is making a fair point that it's interesting how the Buddha talks

99
00:13:37,680 --> 00:13:44,760
about teaching as being necessary for completion, especially for a Buddhist monk.

100
00:13:44,760 --> 00:13:51,240
I mean, he's not saying that any of this is, that all of this is necessarily necessary.

101
00:13:51,240 --> 00:13:59,880
It's just that it's not perfect. The best kind of monk, and by extension, the best kind of Buddhist

102
00:13:59,880 --> 00:14:05,280
meditator is one of the pills all 10 minutes. That doesn't mean that you have to be completed

103
00:14:05,280 --> 00:14:14,240
and all of it. You have to respect and appreciate all of these factors.

104
00:14:14,240 --> 00:14:26,720
So, anyway, that's the dhamma that we're looking at today. I missed yesterday, so let's see if

105
00:14:26,720 --> 00:14:36,480
we've got some questions. We have some questions from yesterday. We need a better interface

106
00:14:36,480 --> 00:14:47,560
setting. We should really have a separate interface from questions, probably. I can't

107
00:14:47,560 --> 00:14:55,280
see all the questions, because you're not within the cues. Here's one. I woke up this morning

108
00:14:55,280 --> 00:15:05,040
with a strong feeling of, or for, nothing is really worth it. I wonder if it is a way

109
00:15:05,040 --> 00:15:12,840
to narrow it up. I can't say I like it or don't like it, but I'm just not indifferent either.

110
00:15:12,840 --> 00:15:18,520
Hours after I thought it might count more as liking, because it is novel encounter. I felt

111
00:15:18,520 --> 00:15:24,480
fresh, but with the component feeling, and it's much to do like, now what? I guess my

112
00:15:24,480 --> 00:15:30,800
question is whether response is like this, like, different. I really exhaust it or suffice

113
00:15:30,800 --> 00:15:43,960
to categorize all ideas. It can be subtle, and it can be on mixed, like boredom. What

114
00:15:43,960 --> 00:15:51,760
is boredom? Well, it's mostly anger-based, and it's also got some delusion in there with laziness

115
00:15:51,760 --> 00:15:58,240
in that kind of thing. It can also be mixed up with greed, boredom can be very much associated

116
00:15:58,240 --> 00:16:07,520
with greed. It's just an example. It's not really important, which one it is. It's important

117
00:16:07,520 --> 00:16:14,320
exactly what it is. If you're thinking, or if you're feeling, you can just say thinking,

118
00:16:14,320 --> 00:16:22,760
or if you're feeling, or if you're feeling. There is strong feeling of nothing being

119
00:16:22,760 --> 00:16:33,840
worth it, of indifference in the sense or disinterest. You could say disinterested, or

120
00:16:33,840 --> 00:16:39,760
weary, maybe. But feeling, feeling works. As long as you're going to identify it objectively,

121
00:16:39,760 --> 00:16:46,760
this is this. It's not so important, but you can't.

122
00:17:09,760 --> 00:17:26,240
Someone who, becoming ordained is really out of the picture. A wife and 12 year old kid being

123
00:17:26,240 --> 00:17:34,280
ordained with some unbreak. It comes so far and learns so much. What is the closest I could

124
00:17:34,280 --> 00:17:47,080
become to ordaining? It's really out to you. It's an apt question, I suppose, because that's

125
00:17:47,080 --> 00:17:57,880
exactly it. You can get close to ordaining. Ordaining is not a yes or no question, ordain

126
00:17:57,880 --> 00:18:08,400
ordained. It's not a binary thing. You can undertake many of these ten things. Many aspects

127
00:18:08,400 --> 00:18:15,120
of the monastic life can be undertaken by laypeople. That's really important. As a layperson,

128
00:18:15,120 --> 00:18:20,960
I was doing this. When I started meditating, I didn't have the opportunity to ordain

129
00:18:20,960 --> 00:18:27,960
for around two years. During those two years, I was doing my best to try and be as monastic

130
00:18:27,960 --> 00:18:33,440
as possible as much of the time as possible. In fact, at one point, I thought I was going

131
00:18:33,440 --> 00:18:41,080
to ordain at least temporarily. Then I found that I really couldn't or just wasn't

132
00:18:41,080 --> 00:18:48,160
convenient. In protest, I went out and bought myself a stainless steel bowl and started

133
00:18:48,160 --> 00:18:56,880
eating food mixed up in a non-monastic but mixing bowl. The nuns in the monastery, I remember

134
00:18:56,880 --> 00:19:04,120
them looking at me. He's got a big bowl. They were also such busy bodies. It's a group

135
00:19:04,120 --> 00:19:11,360
of memories worried about everyone else. They were looking at my big bowl. I think thinking

136
00:19:11,360 --> 00:19:18,360
that I was being greedy when I wanted more food, so I needed a big bowl. It was more

137
00:19:18,360 --> 00:19:23,640
just to be like a monastic. There are many things you can do. Some people in Thailand

138
00:19:23,640 --> 00:19:31,680
want to take to wear white robes. Maybe that's beyond, but that's what it's closest

139
00:19:31,680 --> 00:19:39,080
they can get. There's many things you can do. Even just if you start giving up, your

140
00:19:39,080 --> 00:19:49,560
luxury is your extravagant sense. It starts sleeping on the floor. There's many things

141
00:19:49,560 --> 00:19:56,280
you can do. You don't have to do any or all or any of them. Whatever you can do to practice

142
00:19:56,280 --> 00:20:08,920
renunciation is great. When I get a bad mindset, I say to myself, wrong, wrong, wrong, wrong.

143
00:20:08,920 --> 00:20:15,240
Well, that's not really helpful. Wrong is too much of a judgment and it will be very

144
00:20:15,240 --> 00:20:22,000
negative in the mind. There are other ways to deal with toxic people other than avoiding

145
00:20:22,000 --> 00:20:30,520
them all together. Sidewalk can be a hostile place. I guess, I mean mindfulness of course

146
00:20:30,520 --> 00:20:35,720
is the answer. I'm going to be mindful of the hostile. Sidewalk's not really a hostile

147
00:20:35,720 --> 00:20:47,480
place. You have to remember that. Like Buddha said, you're talking about the use of the word

148
00:20:47,480 --> 00:20:55,880
toxic. The poison only hurts the hand that is wounded. If your hand is unmounded, you can

149
00:20:55,880 --> 00:21:04,800
handle poison in your hands. You can carry it in your hands and not die from it. So anything

150
00:21:04,800 --> 00:21:14,120
toxic to the mind only poisons your mind if you let it in. If your mind is strong, there's

151
00:21:14,120 --> 00:21:23,680
nothing that can poison you.

152
00:21:23,680 --> 00:21:34,600
The attachment causes us to be attracted to others that's fallen in love. The attachment

153
00:21:34,600 --> 00:21:39,280
is one cause of suffering. So how should someone live who is already in relationship

154
00:21:39,280 --> 00:21:45,680
knowing that someday it's feeling or desire for his heart or advantage? I don't know

155
00:21:45,680 --> 00:21:55,360
that that's how it works. I mean, it doesn't just vanish. I mean, you're kind of putting

156
00:21:55,360 --> 00:22:03,800
a cart before the horse. How should I act when I know that someday I might give up? I

157
00:22:03,800 --> 00:22:13,240
mean, why don't you cross that bridge when you come to it? Yeah, I mean, it would be kind

158
00:22:13,240 --> 00:22:20,040
of pessimistic. Not not exactly pessimistic, but it would be getting ahead of yourself

159
00:22:20,040 --> 00:22:29,000
very much. So for no reason, if and when you give it up, there'll be no need to prepare

160
00:22:29,000 --> 00:22:33,880
for it. It's not like I've given it up and I wasn't ready to give it up. You only give

161
00:22:33,880 --> 00:22:42,880
something up when you're very much ready to give it up. So in the meantime, if you want

162
00:22:42,880 --> 00:22:47,560
to be free from suffering, you want to cultivate true peace and happiness. You should look

163
00:22:47,560 --> 00:22:54,320
at the emotion, look at the love, the attachment, the desire. Don't judge it, just learn

164
00:22:54,320 --> 00:23:03,920
about it, study it. If you study it close enough, you'll start to give it up naturally.

165
00:23:03,920 --> 00:23:07,240
As far as thinking about impermanence, that's not really how it works. But what you're

166
00:23:07,240 --> 00:23:12,600
going to see is that it is impermanence. That seeing will come by itself. It'll come naturally.

167
00:23:12,600 --> 00:23:18,120
It won't be an intellectual thing. It's not thinking about impermanence. But as you see

168
00:23:18,120 --> 00:23:29,780
that it's impermanence, you'll start to give it up. I wish to study the Buddhist

169
00:23:29,780 --> 00:23:38,080
texts, but not sure where to begin. Is there a recommended starting point for a beginner?

170
00:23:38,080 --> 00:23:47,080
Well, a lot of the Buddhist teaching has been translated into English and recommend reading

171
00:23:47,080 --> 00:24:13,960
bicobodies translation. If you know, it's a good recommended for us to meditate before listening

172
00:24:13,960 --> 00:24:26,880
to Dhammata. Do you have any more advice how I want to listen to Dhammata? I should be mindful

173
00:24:26,880 --> 00:24:32,720
and meditate during the time. Read my booklet, how to meditate, and try to practice that

174
00:24:32,720 --> 00:24:37,960
while you're listening. It's a great way to meditate. A great way to listen to the Dhammata.

175
00:24:37,960 --> 00:24:41,040
First thing you don't have to listen to so much. Dhammatas, do you get it? You start to

176
00:24:41,040 --> 00:24:45,840
say, well, why do I need to listen to this so that I can just meditate without it?

177
00:24:45,840 --> 00:24:51,960
Well, listening to the Dhammatas actually in the long one, generally overrated. We've

178
00:24:51,960 --> 00:24:57,800
listened to too much and we don't practice enough. It's a fairly general good generalization

179
00:24:57,800 --> 00:25:04,160
of not everyone and not all the time. But in general, you tend to listen too much and

180
00:25:04,160 --> 00:25:11,160
practice too much.

181
00:25:34,160 --> 00:25:44,640
Alright, I'm sorry to have all that much to say tonight. It's been a bit of a day traveling

182
00:25:44,640 --> 00:25:53,640
to Mississauga, but it was nice. It's nice always to meet new people. This is a new

183
00:25:53,640 --> 00:26:01,920
group, Bangladeshi Buddhists, Bengali Buddhists, Bangladeshi. You can't keep it on straight

184
00:26:01,920 --> 00:26:11,360
from Bangladesh. Bangladesh is an incredible situation. Lots of violence there, violence

185
00:26:11,360 --> 00:26:18,840
against Buddhists. It's a fairly complicated situation, but something complicated about the

186
00:26:18,840 --> 00:26:31,400
terrible things that are happening. They are day in six of their young boys temporarily. Six

187
00:26:31,400 --> 00:26:36,000
Bengali Buddhists came to this really good monastery because the monk who sort of runs the

188
00:26:36,000 --> 00:26:41,440
place or is a catalyst for a lot of the things. It's from Bangladesh. It's actually from

189
00:26:41,440 --> 00:26:54,640
Bangladesh. It's a really good friend, a really nice guy. It was invited. Many of us were

190
00:26:54,640 --> 00:27:01,400
invited to take part. I was asked to give a talk, but the funny thing is there were two of

191
00:27:01,400 --> 00:27:06,760
us. The first monk was a Bengali Buddhist. He was asked to give five or ten minutes. He

192
00:27:06,760 --> 00:27:14,200
went on for, I think, 15 or 20 minutes. It came my turn. The head monk said, yeah, maybe

193
00:27:14,200 --> 00:27:21,480
five to eight minutes. We didn't have any time. It's not right. Five minutes. I get a five

194
00:27:21,480 --> 00:27:26,640
minute. I'm a talk, which may not sound like much. It probably went over a couple of minutes

195
00:27:26,640 --> 00:27:32,240
over, but it was actually quite... I was able to get quite a bit out. Many people came

196
00:27:32,240 --> 00:27:37,160
up and thanked me afterwards, just for such a short talk. It must have been more than five

197
00:27:37,160 --> 00:27:48,280
minutes, but not much more. I can't really take credit for it. I'm just saying the things

198
00:27:48,280 --> 00:27:57,560
that my teacher said to all of us. It's very pithy stuff. I'm talking about the four opportunities

199
00:27:57,560 --> 00:28:06,920
that we have. There's four opportunities. That's what I was teaching today. These six young

200
00:28:06,920 --> 00:28:11,440
men have taken the opportunity. They said, don't let the opportunity. Don't let the

201
00:28:11,440 --> 00:28:32,600
moment pass you by. Canno, mam. Just of my teaching. Is intuition real? Sometimes I can

202
00:28:32,600 --> 00:28:38,080
sense something bad happening. Yeah, potentially. We're just reading today. We're just in

203
00:28:38,080 --> 00:28:44,360
the VCD manga that we did today. We were reading about Anag at Tungsei, a knowledge of

204
00:28:44,360 --> 00:28:51,560
the future. Prognostication seems to be a thing that seemed to be possible. Premonitions

205
00:28:51,560 --> 00:29:00,160
do seem to be at times based on five. I don't know how it works. Except I always bring

206
00:29:00,160 --> 00:29:09,160
up this quantum slip experiment where you have these two entangled particles, such that

207
00:29:09,160 --> 00:29:15,280
they go two particles go up in separate directions. If you measure this particle, it affects

208
00:29:15,280 --> 00:29:22,840
this particle, which may not sound like much, but it actually implies the problem that

209
00:29:22,840 --> 00:29:28,920
they had with this, that it seems to imply faster than light travel because affecting

210
00:29:28,920 --> 00:29:34,440
something over here can't change something over here, can't affect something over here

211
00:29:34,440 --> 00:29:38,160
faster than the speed of light because information can't travel faster than the speed

212
00:29:38,160 --> 00:29:45,280
of light. This particle can't know that this particle was measured, but it does. As soon

213
00:29:45,280 --> 00:29:51,560
as this is measured, this one is changed. It shows the reality is not quite what we think

214
00:29:51,560 --> 00:29:57,520
it is, but that's not what's interesting. Interesting is, if you shorten the distance

215
00:29:57,520 --> 00:30:12,440
such that this particle is measured before this, if you lengthen this one, so the particle

216
00:30:12,440 --> 00:30:19,360
is measured after the particle is measured, which is the thing that affects this one, but

217
00:30:19,360 --> 00:30:28,680
it's measured after this particle touches its destination. So once it reaches its destination,

218
00:30:28,680 --> 00:30:34,840
it should be fixed, right? But no, if you measure this particle after this particle has

219
00:30:34,840 --> 00:30:42,040
reached its destination, the measuring of this particle still affects this particle. And

220
00:30:42,040 --> 00:30:46,200
being very general, you have to really study it, but this is actually, from what I've

221
00:30:46,200 --> 00:30:51,960
studied, this has actually happened. So this particle is actually affecting something that

222
00:30:51,960 --> 00:30:58,120
has already happened, which, I mean, I'm sure quantum physicists would, while some

223
00:30:58,120 --> 00:31:02,800
of them would criticize me for even suggesting it, but it does appear that that's a sort

224
00:31:02,800 --> 00:31:08,800
of a timing travel. The future is affecting the paths. It's not exactly, and it's not as

225
00:31:08,800 --> 00:31:17,920
simple as that, or maybe it's even more simple than that, but the point being that the

226
00:31:17,920 --> 00:31:24,280
universe is not quite as we think it is. This idea of classical physics, classical physics

227
00:31:24,280 --> 00:31:33,920
is false. This physics of billiard balls of linear time and so on is simplistic, and

228
00:31:33,920 --> 00:31:41,560
many aspects of it are simply incorrect. It's a bit of a tangent, it's not really important

229
00:31:41,560 --> 00:31:47,760
for our practice as good as it's except, kind of to understand that the universe is not

230
00:31:47,760 --> 00:31:54,600
as it appears. It's very much more based on experience than it is on billiard balls,

231
00:31:54,600 --> 00:32:03,840
atoms and that kind of thing, particles. And if I brought up the teaching, you're going

232
00:32:03,840 --> 00:32:14,320
to make me rehash it. Spooky action at a distance, I believe it was Einstein. He was the

233
00:32:14,320 --> 00:32:20,800
one who was against one for physics. He was really turned off by this new thing and said,

234
00:32:20,800 --> 00:32:28,320
it's just not, not, or something wrong here. It just doesn't sit well. He called it spooky

235
00:32:28,320 --> 00:32:38,600
action at a distance, I believe. The four opportunities, we have this opportunity that we are

236
00:32:38,600 --> 00:32:48,520
born in the time of kubudas teaching, this time period that there is a Buddha in here. We

237
00:32:48,520 --> 00:32:55,040
still have the Buddha. We have his dhammakai, dhammakai means the body of teachings. So all the

238
00:32:55,040 --> 00:32:58,880
teachings of the Buddha, the given this quote that we had today, that's part of the dhammakai.

239
00:33:00,720 --> 00:33:07,440
And that body of teachings is the body of the Buddha. But that's not always the case. Who's to

240
00:33:07,440 --> 00:33:13,760
say that there's going to be another Buddha when the next Buddha is going to come. There are many,

241
00:33:13,760 --> 00:33:19,840
many periods of time. Most of the vast majority of time in this universe is spent without

242
00:33:19,840 --> 00:33:28,080
someone who has seen through the net, seen through the veil of ignorance.

243
00:33:31,040 --> 00:33:37,120
So we have that opportunity. The second opportunity is that we're born as humans. It's not very

244
00:33:37,120 --> 00:33:43,760
useful if you're born as a cow. The Buddha isn't all that much benefit to most cows, except in

245
00:33:43,760 --> 00:33:50,080
terms of not getting them killed because fewer people are inclined to do killing them,

246
00:33:51,280 --> 00:33:57,840
to kill cows. But not really helpful if the Buddha teaches a profound discourse on insight

247
00:33:57,840 --> 00:34:04,800
meditation, to a cow. Cow is not going to be all that benefit. Being born a human is a great

248
00:34:05,600 --> 00:34:11,520
and difficult opportunity. Much more often, much more common is the trust of

249
00:34:11,520 --> 00:34:20,640
your born as animals or even in the hell around us. We'll be born as animals a thousand times

250
00:34:20,640 --> 00:34:26,240
before we get the opportunity just by luck. Just by some happen, it happens to us that we

251
00:34:27,120 --> 00:34:31,600
incline towards good things to such an extent that we're actually born as a human being,

252
00:34:31,600 --> 00:34:43,520
or we interact with human beings in that such that we're inclined to be born as a human being.

253
00:34:45,520 --> 00:34:48,800
So that's the second opportunity. The third opportunity is that we

254
00:34:49,840 --> 00:34:55,040
is the opportunity to practice the Buddha's teaching. Many human beings never have that

255
00:34:55,040 --> 00:35:02,320
opportunity, even the important human being. Physically, there are many obstacles. If someone is sick

256
00:35:02,320 --> 00:35:09,680
or someone is crippled, if someone is an illness is born with debilitating illness, they might

257
00:35:09,680 --> 00:35:16,000
die before they get the opportunity. There may be physical obstacles, such as distance,

258
00:35:16,000 --> 00:35:20,720
there may be born in a place where there is no Buddha's teaching, there's no meditation center.

259
00:35:20,720 --> 00:35:29,520
They might have debt or they might have commitments that get in their way.

260
00:35:31,760 --> 00:35:36,240
So many people don't even have the opportunity to practice the Buddha's teaching for whatever reason.

261
00:35:37,600 --> 00:35:45,600
And the fourth opportunity is the mental opportunity to have the right view,

262
00:35:45,600 --> 00:35:52,080
have the good intention, the intention to ordain the intention to practice meditation. Some people

263
00:35:52,080 --> 00:35:58,880
have the opportunity and they never take it. So this is two parts. The Buddha said it's rare

264
00:35:58,880 --> 00:36:06,480
to find a person who is stirred by truly stirring things, who is moved by that, which is truly moving.

265
00:36:09,360 --> 00:36:13,520
That's rare. But what's even rare, interesting part of the quote is,

266
00:36:13,520 --> 00:36:20,560
what's even more rare is that is among those people who are moved by things that are moving,

267
00:36:23,360 --> 00:36:29,280
who are stirred, who are shook, shaken up when they hear about things that are truly

268
00:36:31,600 --> 00:36:37,680
life-shaking or earth-shattering, how you say it. But even rare among those people,

269
00:36:37,680 --> 00:36:44,480
are people who actually do something about it. Even though it upsets them, many people do not do

270
00:36:44,480 --> 00:36:49,440
anything. So even many Buddhists who are inclined to meditate, never actually get around to doing it.

271
00:36:51,200 --> 00:36:57,600
This is important to actually have the will and the volition to just get up and do it

272
00:36:57,600 --> 00:37:07,600
as very rare, much more rare.

273
00:37:13,600 --> 00:37:19,920
So that's the fourth opportunity. The good intention, and then they have this good intention.

274
00:37:20,560 --> 00:37:26,480
So we should seize upon it. Let me guess the point is to have the desire to do good deeds.

275
00:37:26,480 --> 00:37:30,080
And that's an opportunity that we shouldn't let pass up. None of these opportunities.

276
00:37:30,880 --> 00:37:37,680
We should not let them pass us by. Don't let the moment pass you by actually become someone who

277
00:37:38,720 --> 00:37:43,280
who takes seizes the opportunity. Seize the day.

278
00:37:46,960 --> 00:37:51,440
So that's basically what I talked about. I think it was a little bit more eloquent earlier on in the day.

279
00:37:51,440 --> 00:37:59,440
But that's the gist of it.

280
00:38:02,400 --> 00:38:06,160
Do you think someone can consider themselves a Buddhist without believing in rebirth?

281
00:38:11,120 --> 00:38:17,040
We don't have to believe in anything to be a Buddhist. We have to practice the Buddhist teaching.

282
00:38:17,040 --> 00:38:28,000
We need four things. Don't let the days and nights go by. Don't just be lazy.

283
00:38:30,400 --> 00:38:39,360
Don't neglect your solitude. So it will be inclined and protect your solitude.

284
00:38:39,360 --> 00:38:50,240
We do well in solitude. Keep to yourself and cultivate tranquility and cultivate insight.

285
00:38:50,240 --> 00:38:54,560
Please form. This is how you become someone who lives by the dumb one.

286
00:39:00,640 --> 00:39:04,720
If you have a view that there is no rebirth, that's potentially problematic.

287
00:39:04,720 --> 00:39:10,000
Does that view that belief? You say, I don't believe in rebirth. What you mean is,

288
00:39:10,000 --> 00:39:15,360
you believe there is no rebirth. If you believe that there is no rebirth, that belief gets

289
00:39:15,360 --> 00:39:24,800
in the way because it's against reality. Truth is, unless you're free from, unless you're free

290
00:39:24,800 --> 00:39:32,880
from causes of rebirth, desire. There's going to be rebirth continuously. Even in this life,

291
00:39:32,880 --> 00:39:37,680
there will be the birth of many things in this life. But even beyond this life.

292
00:39:41,200 --> 00:39:46,080
Sangha, your question doesn't seem that important, that it's practical to me.

293
00:39:46,080 --> 00:39:52,080
Again, it's one of these technical questions. And I don't really have a clue about it anyway.

294
00:39:52,080 --> 00:40:04,480
And I don't see how it's practical. Even after, call you out on it.

295
00:40:11,600 --> 00:40:16,080
Okay, look, we're going to ask a question, put the cocuens under it. We're not doing it.

296
00:40:16,080 --> 00:40:24,480
We're not playing by the rules. See, here's a question. This is a question.

297
00:40:28,320 --> 00:40:32,800
See, it has the cube of mine. Can you get a little green? A question?

298
00:40:32,800 --> 00:40:48,720
Or yellow, if you haven't been medicated.

299
00:41:00,160 --> 00:41:01,760
So, you didn't really have to appear.

300
00:41:01,760 --> 00:41:09,440
Okay. And you're the lesser chance for things like PE, music, art, cooking,

301
00:41:10,080 --> 00:41:14,240
which are all required to do at my school. I really don't want to do them especially for PE,

302
00:41:14,240 --> 00:41:20,000
because I want to continue to eat two times a day. How should I deal with this?

303
00:41:23,120 --> 00:41:28,160
Drop out of school. I don't know, are you old enough to drop out the school? Go to an alternative

304
00:41:28,160 --> 00:41:32,240
school. There's lots of alternative schools you can do. Of course,

305
00:41:32,240 --> 00:41:36,400
respondents' education through the province, I think. There's different ways in Canada,

306
00:41:36,400 --> 00:41:41,040
you can anyway. So, the idea that I actually dropped out of school went to an alternative

307
00:41:41,040 --> 00:41:46,080
school and it was great. Just meet with my teacher once a week and did all the work by myself.

308
00:41:47,040 --> 00:41:51,920
Great for meditation. It's a really adult sort of thing. If you've grown up enough to consider

309
00:41:51,920 --> 00:41:58,400
meditation, I think you've grown up enough to do advanced or to do alternative education.

310
00:41:59,280 --> 00:42:05,520
That's what I would recommend. What can I say? Tell them you're not going to do it.

311
00:42:05,520 --> 00:42:08,160
Sit there in the bleachers. Fail it. I don't know what I mean.

312
00:42:10,800 --> 00:42:16,000
Do it mindfully, I suppose. That would be the best compromise, the easiest compromise.

313
00:42:16,000 --> 00:42:22,960
If you're going to run, run mindfully, if you're going to throw a ball, throw the ball

314
00:42:22,960 --> 00:42:30,800
mindfully. You may not end up, you have people throw the ball at you and you're seeing

315
00:42:30,800 --> 00:42:37,840
seeing what the ball hit, you might happen. You might not be the best basketball player, but

316
00:42:37,840 --> 00:42:50,400
you get straight. I don't imagine meditators have the best reflexes, seeing, seeing,

317
00:42:50,400 --> 00:43:15,360
intending the catch, and then there's things in that time you've already been hitting the face with the ball.

318
00:43:15,360 --> 00:43:24,080
All right, you know, thank you all for coming out tonight. Thank you.

319
00:43:24,080 --> 00:43:46,240
Good to have a regular audience. See you out tomorrow. Have a good night.

