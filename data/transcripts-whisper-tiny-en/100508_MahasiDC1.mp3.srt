1
00:00:00,000 --> 00:00:23,000
So today, to sort of switch things up, I thought I would, instead of giving a talk from something from my own brain, I thought I'd read something, which might be of use to meditators or Buddhists or people, people on the path.

2
00:00:23,000 --> 00:00:47,000
So what follows is an excerpt from the Dhamma-chakabawatanasut, the discourse on the turning of the wheel of the Dhamma, which is a series of talks given by a very famous teacher from Burma, and it's based on a discourse that the Buddha gave called the Dhamma-chakabawatanasut.

3
00:00:47,000 --> 00:00:58,000
This is a discourse on the discourse, a talk on the talk which the Buddha gave, sort of an explanation of the Buddha's talk.

4
00:00:58,000 --> 00:01:01,000
So he starts by quoting the Buddha in Bali.

5
00:01:01,000 --> 00:01:17,000
Bhikkhus, one who has gone forth from the worldly life, should not indulge in these two extreme parts, which will be presently explained.

6
00:01:17,000 --> 00:01:20,000
And why shouldn't he indulge in these?

7
00:01:20,000 --> 00:01:28,000
Because the main purpose of one who has gone forth from the worldly life is to rid himself of such defilements as lust and anger.

8
00:01:28,000 --> 00:01:37,000
This objective cannot be achieved by indulging in the two extreme things, for they will only attend to promote further accumulation of lust and anger.

9
00:01:37,000 --> 00:01:40,000
What are the two extreme things?

10
00:01:40,000 --> 00:01:49,000
Delighting in desirable sense objects, pursuing and enjoying sensuous pleasure constitute one extreme practice.

11
00:01:49,000 --> 00:02:02,000
This practice is low, vulgar, being the habit of village and town folks, indulged in by ordinary common-world things, not pursued by the noble ones, ignoble, unclean,

12
00:02:02,000 --> 00:02:08,000
not tending to the true interests one is seeking after.

13
00:02:08,000 --> 00:02:16,000
Such pursued after centuries sensuous pleasures is an extreme practice which should be avoided.

14
00:02:16,000 --> 00:02:26,000
There are five kinds of desirable sense objects, namely pleasurable sights, pleasurable sounds, pleasurable smells, tastes and touch.

15
00:02:26,000 --> 00:02:33,000
In brief, all the material objects animate or inanimate, enjoyed by people in the world.

16
00:02:33,000 --> 00:02:41,000
Delighting in a seemingly pleasurable sight and enjoying it constitute practice and pursuit of sensuality.

17
00:02:41,000 --> 00:02:48,000
Here the sense object of sight means not merely a source of light or color that comes into contact with the seeing eye,

18
00:02:48,000 --> 00:02:55,000
but the man or woman or the whole of the object that forms the source or origin of that sight.

19
00:02:55,000 --> 00:03:05,000
Similarly, all sources of sound, smell and touch, whether man, women, or instrumental objects constitute sensuous objects.

20
00:03:05,000 --> 00:03:17,000
As regards taste, not only the various foods, fruits, and delicacies, but also men, women, and people who prepare and serve them are classified as objects of taste.

21
00:03:17,000 --> 00:03:25,000
Listening to a pleasant sound, smelling a sweet fragment smell are as sensuous as enjoyment of good, delicious food,

22
00:03:25,000 --> 00:03:44,000
luxury, or a comfortable bed, or physical contact with the opposite sex. Delighting in sensuous pleasures and relishing them is to be regarded as a vulgar practice because such enjoyments lead to formation of base desires which are clinging and lustful.

23
00:03:44,000 --> 00:03:51,000
They tend to promote self-conced with the thought that no one else is in the same position to enjoy such pleasures.

24
00:03:51,000 --> 00:04:05,000
At the same time, when becomes oppressed with thoughts of avarice, not wishing to share the good fortune with others, or overcome by thoughts of jealousy, and be anxious, anxious to deny similar pleasures to others.

25
00:04:05,000 --> 00:04:10,000
It arouses ill will towards those who are thought to be opposed to oneself.

26
00:04:10,000 --> 00:04:19,000
Flushed with success and affluence, when becomes shameless and unscrupulous, bold and reckless in one's behavior, no longer afraid to do evils.

27
00:04:19,000 --> 00:04:40,000
When begins to deceive oneself with false impression of well-being and prosperity, the newly-informed worldling may also come to hold the wrong view of living soul or at that or self to entertain disbelief in the resultant effects of one's own actions, karma.

28
00:04:40,000 --> 00:04:47,000
Such being the outcome of delighting in and relishing of central pleasures, they are to be regarded as low and base.

29
00:04:47,000 --> 00:04:56,000
Furthermore, indulgence in central pleasures is the habitual practice of lower forms of creatures such as animals, ghosts, and so on.

30
00:04:56,000 --> 00:05:09,000
The monks and novices belonging to the higher stages of existence should not stoop too low to view with the lower forms of life in the vulgar practice of base sensuality.

31
00:05:09,000 --> 00:05:17,000
Pursuit after sensuous pleasures does not lie within the province of one who has gone forth from the worldly life.

32
00:05:17,000 --> 00:05:23,000
It is the concern of the town and village folks who regard central pleasures as the highest attributes of bliss.

33
00:05:23,000 --> 00:05:26,000
The greater the pleasures, the greater the happiness.

34
00:05:26,000 --> 00:05:32,000
In ancient times, rulers and rich people engage themselves in the pursuit of central pleasures.

35
00:05:32,000 --> 00:05:37,000
Wars were waged, and violent conquests made all for the gratification of sense desire.

36
00:05:37,000 --> 00:05:44,000
In modern times, too, similar conquests are still being made in some areas for the same objectives.

37
00:05:44,000 --> 00:05:52,000
But it is not only the rulers and the rich who seek central pleasures, the poor are also arduous in the pursuit of worldly goods and pleasures.

38
00:05:52,000 --> 00:06:01,000
As our matter of fact, as soon as adolescence is reached, the instinct for mating and sexual gratification makes itself felt.

39
00:06:01,000 --> 00:06:12,000
For the worldly household are veiled from the Buddha and the Dhamma, gratification of sense desires appears to be indeed the acne of happiness and bliss.

40
00:06:12,000 --> 00:06:19,000
Even before the time of the Buddha, there were people who held the belief that heavenly bliss can be enjoyed in this very life.

41
00:06:19,000 --> 00:06:23,000
According to them, central pleasures was indeed blissful.

42
00:06:23,000 --> 00:06:29,000
There was nothing to surpass it, and that pleasure was to be enjoyed in this very life.

43
00:06:29,000 --> 00:06:36,000
It would be foolish to let precious moments for enjoyment pass, waiting for the bliss in future life, which does not exist.

44
00:06:36,000 --> 00:06:41,000
The time for full gratification of central pleasures is now, this very life.

45
00:06:41,000 --> 00:06:47,000
Such is the heavenly bliss, which they believe in in this very life.

46
00:06:47,000 --> 00:06:54,000
This is one of the 62 wrong views, expounded by the Buddha in the Brahmajala Sutta of the Silakhanda of the Diknikaya.

47
00:06:54,000 --> 00:07:03,000
Thus enjoyment of central pleasures is the preoccupation of town and village people, not the concern of recklessness and monks.

48
00:07:03,000 --> 00:07:11,000
For them to go after sense desire would mean reverting back to the worldly life, which they have renounced.

49
00:07:11,000 --> 00:07:19,000
People show great reverence to them believing they are leading a holy life, undisturbed by worldly distraction or a lerman of the opposite sex.

50
00:07:19,000 --> 00:07:28,000
People making the best offer of food and clothing to the recklessness, denying these to themselves, often at the sacrifice of the needs of their dear ones and their family.

51
00:07:28,000 --> 00:07:37,000
It would be most improper for the monks to seek worldly pleasures, just like the householders, while living on the charity of the people.

52
00:07:37,000 --> 00:07:47,000
In addition, monks renounce the world with a vow to work for the release from sufferings, from the sufferings inherent in the rounds of rebirth and for the realization of nibana.

53
00:07:47,000 --> 00:07:55,000
It is obvious that these noble ideals cannot be attained by the monks if they go after central pleasures in the manner of householders.

54
00:07:55,000 --> 00:08:02,000
Thus one who has gone forth from the worldly life should not indulge in delightful, sensuous pleasures.

55
00:08:02,000 --> 00:08:09,000
The majority of people in the world are ordinary, common folk engaged in seeking the means of living and enjoying central pleasures.

56
00:08:09,000 --> 00:08:16,000
There are only a few who can rise above the common crowd, who can see the dharma and live a holy life.

57
00:08:16,000 --> 00:08:23,000
It is not for them to indulge in course worldly pleasures, which is the main concern of the lower class of beings.

58
00:08:23,000 --> 00:08:27,000
Enjoyment of worldly pleasures is not the practice of the noble ones.

59
00:08:27,000 --> 00:08:34,000
One may ask here why the noble ones like Wisaka, Anatapindika, and the Saka, the king of the angels,

60
00:08:34,000 --> 00:08:40,000
who had already reached the first stage of the noble life, engaged themselves in pursuit of central pleasures.

61
00:08:40,000 --> 00:08:46,000
In Sota Pandas or Streamenters, lust and passions are not yet overcome.

62
00:08:46,000 --> 00:08:52,000
There are still lingers in them the incipient perception of agreeableness of carnal pleasures.

63
00:08:52,000 --> 00:09:01,000
This point is illustrated in the Anguitara Nikaya by the example of a person who is prestigious in the habits of cleanliness,

64
00:09:01,000 --> 00:09:08,000
but seeks shelter in a filthy place filled with excrement to avoid attack by an elephant in must.

65
00:09:08,000 --> 00:09:15,000
This defiling course habit being ignoble and unclean should be avoided by recklessness and monks.

66
00:09:15,000 --> 00:09:19,000
This practice does not tend to one's own welfare or well-being.

67
00:09:19,000 --> 00:09:27,000
In the common popular view, making money and accumulating wealth, establishing a family life with retinues and a circle of friends,

68
00:09:27,000 --> 00:09:34,000
in short, striving for success and prosperity in this world, appears indeed to be working for one's own welfare.

69
00:09:34,000 --> 00:09:40,000
Actually, however, the such worldly success and prosperity do not amount to one's own well-being.

70
00:09:40,000 --> 00:09:46,000
One's true interest lies in seeking ways of overcoming old age, disease, and death,

71
00:09:46,000 --> 00:09:50,000
and attaining release from all forms of suffering.

72
00:09:50,000 --> 00:09:58,000
The only way to escape from all forms of suffering is through development of morality, concentration, and insight wisdom.

73
00:09:58,000 --> 00:10:06,000
Only these, namely, morality, concentration, and wisdom are to be sought for in the true interest of oneself.

74
00:10:06,000 --> 00:10:13,000
Pursuit of sensual pleasures cannot lead to the conquest of old age, disease, death, or all forms of suffering.

75
00:10:13,000 --> 00:10:21,000
It only tends to breach the code of morality, such as the commitment of illegal sexual conduct.

76
00:10:21,000 --> 00:10:27,000
Seeking worldly amenities through killing, theft, or deceit also amounts to violation of moral precepts.

77
00:10:27,000 --> 00:10:30,000
Not to speak of physical actions.

78
00:10:30,000 --> 00:10:34,000
Not to speak of physical actions, mere thought of enjoyment of sensual pleasures,

79
00:10:34,000 --> 00:10:37,000
prohibits development of mental concentration and wisdom,

80
00:10:37,000 --> 00:10:43,000
and thus forms a hindrance to the realization of nibana, the cessation of suffering.

81
00:10:43,000 --> 00:10:48,000
Failure to observe moral precepts is a sure step to the foreign other worlds of intense suffering.

82
00:10:48,000 --> 00:10:52,000
It is to be noted, however, that maintenance of moral character alone,

83
00:10:52,000 --> 00:10:59,000
without simultaneous development of wisdom, concentration, and wisdom, will not lead to enlightenment.

84
00:10:59,000 --> 00:11:03,000
It only encourages rebirth, repeatedly, and happier, and existences.

85
00:11:03,000 --> 00:11:10,000
Where, however, manifold suffering, such as old age, disease, and death, are still encountered again and again.

86
00:11:10,000 --> 00:11:16,000
Requises and monks having renounced the world, with the avowed purpose of achieving enlightenment,

87
00:11:16,000 --> 00:11:25,000
where all suffering cease, should have nothing to do with the pursuit of sensuous pleasures that only obstruct development of morality, concentration, and wisdom.

88
00:11:25,000 --> 00:11:37,000
To recapitulate, enjoyment of sensual pleasures is low, vulgar, being the preoccupation of common people of low intelligence, unclean, ignoble, not practiced by the noble ones.

89
00:11:37,000 --> 00:11:41,000
It is detrimental to the progress in morality, concentration, and wisdom.

90
00:11:41,000 --> 00:11:50,000
And thus works against the true interest of those intent on achievement of the unaged, undecist, the deathless, the nibana.

91
00:11:50,000 --> 00:11:57,000
The text only says that one who has gone forth from the worldly life should not indulge in sensual pleasures.

92
00:11:57,000 --> 00:12:08,000
The question, therefore, arises whether ordinary householders who remain amidst the worldly surroundings, should freely pursue sensuous pleasures without any restraint.

93
00:12:08,000 --> 00:12:16,000
Since the gratification of sense desires is the preoccupation of common people, it would be pointless to enjoy them from doing so.

94
00:12:16,000 --> 00:12:24,000
But the householder intent on practicing the noble dhamma should advisedly avoid these pleasures to the extent necessary for practice.

95
00:12:24,000 --> 00:12:29,000
Observance of the five precepts requires abstaining from commitment of sins of the flesh.

96
00:12:29,000 --> 00:12:36,000
Likewise, possession of worldly goods should not be sought through killing after deceit.

97
00:12:36,000 --> 00:12:46,000
In the Pasadikasuta of the Patikavagadikani Kaya, Buddha has had stated four kinds of indulgence in worldly enjoyment.

98
00:12:46,000 --> 00:12:56,000
Sunda, in this world there are some foolish ignorant people who promote their own enrichment by the slaughter of animals, cattle, pigs, chicken, fish.

99
00:12:56,000 --> 00:13:01,000
This practice constitutes the first form of indulgence in worldly enjoyment.

100
00:13:01,000 --> 00:13:12,000
Theft and robbery consist of the second form of indulgence in worldly enjoyment, while deceitful means of earning one's livelihood constitute the third.

101
00:13:12,000 --> 00:13:19,000
The fourth form of indulgence embraces other means besides these three by which worldly wealth is gained.

102
00:13:19,000 --> 00:13:25,000
The Sutta stated that Buddha's disciples monks were free from these indulgences.

103
00:13:25,000 --> 00:13:38,000
Laypeople in observing the aid precepts and ten precepts have to maintain chastity and abstain from partaking of food after midday, dancing and singing, all of these being forms of sensuous pleasure.

104
00:13:38,000 --> 00:13:51,000
When one is engaged in meditation practices, one has to forego all kinds of sensuous enjoyment, just like the monks who have gone forth from the worldly life, because they tend to hinder the development of morality, concentration and wisdom.

105
00:13:51,000 --> 00:13:58,000
A meditator, even if he is a lame person, must not therefore indulge in worldly enjoyment.

106
00:13:58,000 --> 00:14:05,000
This should suffice regarding one form of extreme practice, namely indulgence in worldly enjoyment.

107
00:14:05,000 --> 00:14:11,000
The second extreme practice of self-mortification results only in self's torture and suffering.

108
00:14:11,000 --> 00:14:15,000
It is not the practice of the noble ones.

109
00:14:15,000 --> 00:14:22,000
Hence ignoble, unclean and does not tend to one's own practical welfare and interest.

110
00:14:22,000 --> 00:14:26,000
This extreme practice should also be avoided.

111
00:14:26,000 --> 00:14:34,000
Self-mortification, which leads only to the suffering, only to suffering, was practiced by those who held the belief that luxurious living would cause attachment to sensuous pleasures,

112
00:14:34,000 --> 00:14:40,000
and that only austerity practices denying oneself,

113
00:14:40,000 --> 00:14:45,000
denying oneself's sense objects such as food and clothing would remove sense desires.

114
00:14:45,000 --> 00:14:51,000
Then only the eternal peace, the state of the unaged, undesesed, the deathless could be achieved.

115
00:14:51,000 --> 00:14:56,000
Such was the belief of those who practiced self-mortification.

116
00:14:56,000 --> 00:15:05,000
Good monks cover themselves with robes and clothing for decency and to shield themselves from heat and cold, from insects, flies and mosquitoes.

117
00:15:05,000 --> 00:15:11,000
But self-mortifiers go about without any clothing. When the weather is cold, they immerse themselves underwater.

118
00:15:11,000 --> 00:15:19,000
When hot, they expose themselves to the sun, standing amidst four fireplaces, thus subjecting themselves to heat from five directions.

119
00:15:19,000 --> 00:15:23,000
This is known as the five-fold penance by heat.

120
00:15:23,000 --> 00:15:27,000
They have no use for regular beds lying on the naked ground for resting.

121
00:15:27,000 --> 00:15:34,000
Some of them resort to lying on prickly thorns, covered only by a sheet of clothing.

122
00:15:34,000 --> 00:15:41,000
There are some who remain in a sitting posture for days, while others keep to standing only, either lying or sitting down.

123
00:15:41,000 --> 00:15:48,000
One form of self-infliction is to lie hanging down, suspended from a tree branch by two legs.

124
00:15:48,000 --> 00:15:53,000
To stand straight on one's head in a topsy-turvy posture is yet another.

125
00:15:53,000 --> 00:16:02,000
Whereas it is the normal habit of good monks to assage hunger by per taking of food, some self-tormenders completely cut off food and water.

126
00:16:02,000 --> 00:16:09,000
There are some who eat only on alternate days, while others eat once in two days, three days, etc.

127
00:16:09,000 --> 00:16:17,000
Some practitioners are staying from food for four days, five days, six days, seven days, some even for fifteen days on end.

128
00:16:17,000 --> 00:16:28,000
Some reduce their meal to just one handful of food while others live on nothing but green vegetables and grass or on cow excrement.

129
00:16:28,000 --> 00:16:33,000
All such self-imposed penances constitute self-mortification.

130
00:16:33,000 --> 00:16:39,000
These practices were followed by the niganta nataputa sect long before the time of the Buddha.

131
00:16:39,000 --> 00:16:44,000
The present day giants are the descendants of the niganta nataputa.

132
00:16:44,000 --> 00:16:50,000
Their practice of self-mortification was commonly acclaimed and well thought of by the multitude in those days.

133
00:16:50,000 --> 00:16:59,000
Hence, when the Bodhisattva gave up austere practices and resumed per taking of normal meals, his intimate colleagues, the group of five monks,

134
00:16:59,000 --> 00:17:08,000
forsook him misjudging that the Bodhisattva had given up the right practice, right exertion and that he would not attain enlightenment.

135
00:17:08,000 --> 00:17:15,000
According to the scriptures of the niganta, emancipation from the suffering of samsara is achieved by two means.

136
00:17:15,000 --> 00:17:25,000
Number one means of restraint. This consists of an restraining sense object, such as sight, sounds, smells, taste, touch, from entering their body,

137
00:17:25,000 --> 00:17:31,000
where it is their belief they will conjoin with the Atman to produce fresh karma.

138
00:17:31,000 --> 00:17:35,000
These fresh karmas are believed to form in turn new life.

139
00:17:35,000 --> 00:17:41,000
And number two, annihilation of results of past karma through torturous penance.

140
00:17:41,000 --> 00:17:52,000
Their belief is that their belief is that results of past misdeeds are expiated and redemption obtained by submitting oneself to self-mortification.

141
00:17:52,000 --> 00:17:57,000
The Buddha asked of the nikta sedex who were practicing self-mortification.

142
00:17:57,000 --> 00:18:04,000
You state that you go through physical sufferings to exhaust the results of unwholesome deeds of past existences.

143
00:18:04,000 --> 00:18:10,000
But you know for certain that you have indeed committed on virtuous acts in previous existences.

144
00:18:10,000 --> 00:18:13,000
Their reply was in the negative.

145
00:18:13,000 --> 00:18:19,000
The Buddha further questioned them whether they knew how much unwholesome karma they had done previously,

146
00:18:19,000 --> 00:18:24,000
how much of it they had expiated through self-mortification and how much of it remained.

147
00:18:24,000 --> 00:18:28,000
The replies were all in the negative, they did not know.

148
00:18:28,000 --> 00:18:35,000
Then the Buddha explained to them in order to give them the seed of intellectual advancement that it was fruitless to practice torturous penance.

149
00:18:35,000 --> 00:18:41,000
Not knowing if there were any past misdeeds nor how much of it they had expiated.

150
00:18:41,000 --> 00:18:47,000
The Buddha stated further that these who were trying to absolve themselves from the past misdeeds through self-porture

151
00:18:47,000 --> 00:18:57,000
may truly have committed large amounts of unwholesome deeds for which they were now paying through their wrong practice.

152
00:18:57,000 --> 00:19:04,000
The Buddha said, but previously adopted extreme measures of practice not with a view to expiate his past misdeeds, if any.

153
00:19:04,000 --> 00:19:06,000
But thinking that they would lead to higher knowledge.

154
00:19:06,000 --> 00:19:13,000
But after five years of strenuous efforts as stated above, realizing that extreme practices would not lead to knowledge or insight.

155
00:19:13,000 --> 00:19:21,000
And wondering whether there was another way that would lead to his cherished goal, he abandoned the practice of self-mortification.

156
00:19:21,000 --> 00:19:25,000
Practice of self-torture results only in physical suffering.

157
00:19:25,000 --> 00:19:30,000
But it was regarded that naked ascetics, it was regarded by naked ascetics as being holy.

158
00:19:30,000 --> 00:19:37,000
In order to spare their sensibilities as explained in the commentary, the Buddha did not denounce the practice as being lower base.

159
00:19:37,000 --> 00:19:47,000
Nor was it described as vulgar, not being practiced by ordinary village folk, nor as common as common because ordinary common people did not indulge in it.

160
00:19:47,000 --> 00:19:55,000
The Buddha described the method simply as painful and unclean and ignoble, not being followed by the noble persons.

161
00:19:55,000 --> 00:20:01,000
Practice of extreme torture also does not pertain to true interests, one is seeking after.

162
00:20:01,000 --> 00:20:13,000
Not only that it is not concerned with higher ideals of morality, concentration, and wisdom, it does not contribute to anything to mundane advancements, being a prophetless effort resulting only in physical suffering.

163
00:20:13,000 --> 00:20:21,000
The austere, austere practices may even prove fatal to the overzealous practitioner, it is utterly prophetless.

164
00:20:21,000 --> 00:20:34,000
Before the appearance of the supremely enlightened Buddha it was widely held through widely held through India, the middle country, that self-mortification was a noble holy practice, which truly led to liberation.

165
00:20:34,000 --> 00:20:39,000
The group of five monks also held that view.

166
00:20:39,000 --> 00:20:42,000
But the Buddha said that extreme practice produced only suffering.

167
00:20:42,000 --> 00:20:47,000
They were not indulged in by noble persons being unclean and ignoble.

168
00:20:47,000 --> 00:20:51,000
It did not pertain to the interests one was seeking after.

169
00:20:51,000 --> 00:20:57,000
The Buddha therefore clearly advised those who had gone forth from the world to avoid them.

170
00:20:57,000 --> 00:21:07,000
A definite pronouncement regarding unworthiness of extreme practice was necessary at that stage because not only was it universally held, that only self-mortification would lead to higher knowledge.

171
00:21:07,000 --> 00:21:11,000
The group of five monks also accepted this belief.

172
00:21:11,000 --> 00:21:18,000
As long as they held fast to this view, they would not be receptive to the doctrine of the noble eightfold path.

173
00:21:18,000 --> 00:21:25,000
Hence, the open denunciation by the Buddha, that self-mortification was prophetless, leading only to physical suffering.

174
00:21:25,000 --> 00:21:32,000
The first extreme portion gives practice, gives free reign to mind and body, and is therefore to be regarded as too lax or yielding.

175
00:21:32,000 --> 00:21:40,000
A free mind not controlled by meditation is liable to sink low in its pursuits of central pleasures.

176
00:21:40,000 --> 00:21:44,000
It is learned that some teachers are teaching the practice of relaxing the mind, giving it free reign.

177
00:21:44,000 --> 00:21:48,000
But the nature of mind is such that it requires constant guard over it.

178
00:21:48,000 --> 00:21:54,000
Even when constantly controlled by meditations, the mind wanders forth to objects of central pleasure.

179
00:21:54,000 --> 00:22:03,000
It is therefore obvious that left by itself, unguarded by meditation, the mind will surely engage itself in thoughts of central pleasures.

180
00:22:03,000 --> 00:22:10,000
The second extreme practice inflicts suffering on oneself through denial of normal requirements of food and clothing.

181
00:22:10,000 --> 00:22:19,000
It is too rigid, unbending, and depriving on self of an unordinary comfort. It is thus to be avoided too.

182
00:22:19,000 --> 00:22:27,000
A wrong interpretation as to what constitutes self-mortifications is being made by some teachers in contradiction to the teaching of the Buddha.

183
00:22:27,000 --> 00:22:35,000
According to them, earnest, tireless effort required for meditation amounts to self-mortification.

184
00:22:35,000 --> 00:22:46,000
This view is diametrically opposed to the exhortation of the Buddha who advised strenuous, unrelenting exertion, even at the sacrifice of life and limb to attain concentration and insight.

185
00:22:46,000 --> 00:22:57,000
Let only skin, sinew, and bone remain, let the flesh and blood dry up, I will exert incessantly, until I achieve the path and fruition I work for.

186
00:22:57,000 --> 00:23:06,000
Such must be that resolute firmness of determination, counseled by the Buddha, with which the goal was to be pursued.

187
00:23:06,000 --> 00:23:15,000
Thus, strenuous, relentless effort in meditation practices for achievement of concentration and insight should not be misconstrued as a form of self-torture.

188
00:23:15,000 --> 00:23:24,000
Leaving aside meditation practices, even keeping the precepts, which entails some physical discomfort, is not to be regarded as a practice of self-mortification.

189
00:23:24,000 --> 00:23:31,000
Young people and young novices suffer from pangs of hunger in the evenings while keeping the eight precepts.

190
00:23:31,000 --> 00:23:37,000
But as fasting is done in the fulfillment of the precepts, it does not amount to modification.

191
00:23:37,000 --> 00:23:42,000
For some people, the precept of abstaining from taking life is a sacrifice on their part.

192
00:23:42,000 --> 00:23:48,000
They suffer certain disadvantages as a consequence, but as it constitutes the good deed of keeping the precepts.

193
00:23:48,000 --> 00:23:51,000
It is not to be viewed as a form of self-mortification.

194
00:23:51,000 --> 00:24:03,000
In the Mahadhamma Samadhanasuta of mulapanasa, the Maji Minikaya, the Buddha explains that such acts of sacrifice at the present time, is bound to produce beneficial results in the future.

195
00:24:03,000 --> 00:24:10,000
The Buddha said, in this world, some people abstain from taking life, causing some physical and mental sufferings to themselves.

196
00:24:10,000 --> 00:24:16,000
They take up the right view of not killing, for which they have to suffer physically and mentally.

197
00:24:16,000 --> 00:24:21,000
These people thus voluntarily going through this suffering to keep the precepts at the present time.

198
00:24:21,000 --> 00:24:26,000
We'll, after passing away, attain the higher abodes in the as an age as angels.

199
00:24:26,000 --> 00:24:34,000
These ten meritorious deeds are known as good practices, which produce beneficial results in afterlife through suffering for the present.

200
00:24:34,000 --> 00:24:49,000
Thus, any practice which promotes morality concentration wisdom is not profitless, not self-mortification, and that which is not to be indulged in, but beneficial, and is in line with the middle path, which should certainly be followed.

201
00:24:49,000 --> 00:25:02,000
It should be definitely noted that only that practice which does not develop morality, concentration, and wisdom, but results merely in physical suffering constitute self-mortification.

202
00:25:02,000 --> 00:25:16,000
There are some who hold the view that contemplation on pleasant feelings constitutes indulgence and sensual pleasure, while contemplation on painful feelings constitute self-mortification the second extreme.

203
00:25:16,000 --> 00:25:23,000
Thus, they hold on one should avoid both of them and engage only in contemplation of equanimity.

204
00:25:23,000 --> 00:25:27,000
This is certainly an irrational misconception not supported by any textual authority.

205
00:25:27,000 --> 00:25:38,000
The Buddha had definitely stated in the Mahasati Patanasutra that pleasant feelings, painful feelings, as well as equanimity are all objects for contemplation.

206
00:25:38,000 --> 00:25:51,000
The same statement was repeated in many other discourses, thus it should definitely be noted that any object which falls into the category of the five groups of grasping is a legitimate object for meditational contemplation.

207
00:25:51,000 --> 00:26:04,000
A lay meditation teacher is reported to have stated, while engaged in practice of meditation, taking up any posture if one begins to feel tired, painful, or be numbed, hot, or unpleasant in the limbs.

208
00:26:04,000 --> 00:26:07,000
One should at once change the posture.

209
00:26:07,000 --> 00:26:16,000
If one persists in practice of mindfulness in spite of the unpleasant sensations or tiredness, one is actually engaged in self-toucher.

210
00:26:16,000 --> 00:26:21,000
This statement is made apparently taking into consideration the welfare of the meditator.

211
00:26:21,000 --> 00:26:25,000
Nonetheless, it must be said that it is unsound and ill-advised.

212
00:26:25,000 --> 00:26:37,000
In the practice of concentration or insight meditation, patience or self-control, plays an important role is an important factor for the successful practice of concentration or insight meditation.

213
00:26:37,000 --> 00:26:43,000
One pointiness of mind can only be achieved through patiently bearing some bodily discomfort.

214
00:26:43,000 --> 00:26:52,000
It is within the experience of anyone who has practiced meditation and earnest that continual changing of posture is not conducive to development of concentration.

215
00:26:52,000 --> 00:26:56,000
Therefore, unpleasant physical discomfort has to be born with patience.

216
00:26:56,000 --> 00:27:10,000
The self-controlled practice, thus, is not self-mortification in as much as the goal, being not mere suffering, but for the promotion of morality, concentration, and wisdom, in accordance with the wishes of the Buddha.

217
00:27:10,000 --> 00:27:21,000
The Blessed One desired if possible, and even more relentless effort to achieve the noblest fruit of enlightenment by one continuous sitting, uninterrupted by change of posture.

218
00:27:21,000 --> 00:27:31,000
In the Maha-gosa-gosenga-suta of the mula-pañasa, the Blessed One stated a bikhu on monk meditates after making a firm resolution.

219
00:27:31,000 --> 00:27:39,000
Unclinging, I will remain seated without changing the cross-legged position until the asa was the defilements have been removed.

220
00:27:39,000 --> 00:27:48,000
Such a monk is an adornment to the gosenga monastery in the forest of these trees, a valuable asset to the forest abode.

221
00:27:48,000 --> 00:27:59,000
Thus, to state that patient contemplation of painful feelings is a form of self-torture, is to denounce these meditators who are following the instructions of the Buddha.

222
00:27:59,000 --> 00:28:08,000
It also amounts to a rejection of the Buddha's words and discourage the effort of the meditators, who could achieve concentration and insight, only through patient bearing of pain,

223
00:28:08,000 --> 00:28:11,000
brought about by shiftless posture.

224
00:28:11,000 --> 00:28:13,000
Okay, and I'm going to stop there.

225
00:28:13,000 --> 00:28:15,000
Thanks for coming, everyone.

226
00:28:15,000 --> 00:28:17,000
That's the reading for tonight.

227
00:28:17,000 --> 00:28:26,000
Now, if there's any discussion, people would like to ask questions or make comments or discuss this.

228
00:28:26,000 --> 00:28:28,000
I'm happy to stay on, I think.

229
00:28:28,000 --> 00:28:37,000
This is sort of a good basis for discussion on meditation practice, talking about the very basics of the Buddha's teaching

230
00:28:37,000 --> 00:28:44,000
as to what constitutes right practice and what constitutes wrong practice.

