1
00:00:00,000 --> 00:00:02,800
Really, I have a terrible fear of death.

2
00:00:02,800 --> 00:00:06,040
I won't be able to take it off my mind.

3
00:00:06,040 --> 00:00:09,680
I'm scared I will no longer exist when I die.

4
00:00:09,680 --> 00:00:12,920
It keeps my heart raising all day.

5
00:00:12,920 --> 00:00:14,560
Makes me so anxious.

6
00:00:14,560 --> 00:00:17,840
Can you help me?

7
00:00:17,840 --> 00:00:20,280
I can help you help yourself.

8
00:00:22,240 --> 00:00:25,400
There's, um...

9
00:00:25,400 --> 00:00:32,920
In many ways I can do that, you could start by reading the book I wrote, a little book on

10
00:00:32,920 --> 00:00:38,480
how to meditate, and you can contact me by email.

11
00:00:38,480 --> 00:00:43,280
Best thing is you could come out here and start practicing.

12
00:00:43,280 --> 00:00:49,600
But the question is, help you do what?

13
00:00:49,600 --> 00:01:01,360
Help you remove the fear, or help you to rise above it.

14
00:01:01,360 --> 00:01:13,720
I mean, that's kind of a silly way to put it now.

15
00:01:13,720 --> 00:01:18,560
What I mean to say is, you're better off not trying, if your intention is to do away with

16
00:01:18,560 --> 00:01:22,920
the fear, then you're going to have a problem.

17
00:01:22,920 --> 00:01:29,480
So rather than recommending, rather than trying to help you get rid of the fear, I would

18
00:01:29,480 --> 00:01:40,320
help you to see the fear, and to live with the fear until you can understand the fear.

19
00:01:40,320 --> 00:01:41,320
And overcome the fear.

20
00:01:41,320 --> 00:01:54,680
As we cling to things like fear, we cling to all of our emotions, and use the clinging

21
00:01:54,680 --> 00:02:05,240
as substitute for actually doing something, as a solution, I get stuck on the fear, stuck

22
00:02:05,240 --> 00:02:07,720
on the emotions.

23
00:02:07,720 --> 00:02:09,760
So look at them.

24
00:02:09,760 --> 00:02:10,760
Stop clinging to them.

25
00:02:10,760 --> 00:02:13,520
You have to change the way you think about this.

26
00:02:13,520 --> 00:02:17,480
In every case, we have to change the way we think about it.

27
00:02:17,480 --> 00:02:19,120
Stop thinking of it as a problem.

28
00:02:19,120 --> 00:02:20,760
Stop thinking about the problem at all.

29
00:02:20,760 --> 00:02:23,800
Stop thinking about death.

30
00:02:23,800 --> 00:02:28,800
Start thinking about the fear, which is the last thing we look at when we never think to

31
00:02:28,800 --> 00:02:32,760
look at the reaction, and that's where the problem is.

32
00:02:32,760 --> 00:02:40,360
The problem is not in death, the problem is in your reaction to it.

33
00:02:40,360 --> 00:02:45,360
There's no problem with death, it's a problem with fear.

34
00:02:45,360 --> 00:02:50,760
So instead of thinking about death, or trying to figure out how to solve this problem

35
00:02:50,760 --> 00:02:56,520
of death, I figure out how to solve the problem of fear.

36
00:02:56,520 --> 00:02:58,760
That's really what you want them.

37
00:02:58,760 --> 00:03:04,720
No, I mean, the problem is that probably you want to not die, right?

38
00:03:04,720 --> 00:03:12,480
You can't have that, but what you should want is to not fear, die without fear.

39
00:03:12,480 --> 00:03:20,040
So try to learn about the fear, don't get rid of it, learn about it, understand it.

40
00:03:20,040 --> 00:03:24,440
Once you understand the fear, you problem would be solved.

41
00:03:24,440 --> 00:03:28,680
Let the fear be, let it be there, so you're afraid of death, deathly afraid.

42
00:03:28,680 --> 00:03:38,240
It's not really a problem, and when you look at it, you'll see it's just useless, it's

43
00:03:38,240 --> 00:03:39,240
meaningless, pointless.

44
00:03:39,240 --> 00:03:43,440
You'll see, you'll ask yourself, why am I afraid?

45
00:03:43,440 --> 00:03:52,520
What a silly thing, how pointless, how absurd, how irrational, and to keep it up.

46
00:03:52,520 --> 00:04:06,560
Okay.

