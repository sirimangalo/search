1
00:00:00,000 --> 00:00:07,280
Hello everyone, I know I've been away from the camera for a while, so I thought I'd just

2
00:00:07,280 --> 00:00:17,280
post a bit of a video blog and we'll notice maybe I can get back into regular video

3
00:00:17,280 --> 00:00:18,280
blogging.

4
00:00:18,280 --> 00:00:27,920
I'm going to be back in Canada on April 2nd or 4th or around the beginning of April,

5
00:00:27,920 --> 00:00:37,760
so I will, I should begin to do more regular video teachings from then on, back into the

6
00:00:37,760 --> 00:00:43,640
Dhammapada videos, I'd like to finish up the introduction to Buddhism series that we kind

7
00:00:43,640 --> 00:00:54,960
of left very much in the middle, so I haven't abandoned the internet or YouTube and I will

8
00:00:54,960 --> 00:01:00,760
be back for more teachings but this is just kind of a hello and a couple of announcements

9
00:01:00,760 --> 00:01:04,360
in the case people out there are interested.

10
00:01:04,360 --> 00:01:11,760
The first announcement is that we've been working on translations to my booklet on how

11
00:01:11,760 --> 00:01:13,520
to meditate.

12
00:01:13,520 --> 00:01:22,600
So we have eight translations that have been completed for a while now and I've gathered

13
00:01:22,600 --> 00:01:27,280
together a number of people to make translations into their language.

14
00:01:27,280 --> 00:01:33,120
We have Russian, which is now being edited and Thai, which is in the process of being

15
00:01:33,120 --> 00:01:34,120
edited.

16
00:01:34,120 --> 00:01:43,080
There's a Polish translation underway and there's a couple of others.

17
00:01:43,080 --> 00:01:46,920
So quite a few people have responded actually.

18
00:01:46,920 --> 00:01:52,680
If you have a language that you'd like to see the booklet translated in please, feel free

19
00:01:52,680 --> 00:01:57,760
to get in touch with me and by all means, if someone else hasn't already started that

20
00:01:57,760 --> 00:02:02,400
language, I'm going to be happy to have it translated.

21
00:02:02,400 --> 00:02:09,800
It's only, it's not so long, I think 20, 30 pages long.

22
00:02:09,800 --> 00:02:16,800
So if you have the time and the expertise and feel comfortable translating English into

23
00:02:16,800 --> 00:02:19,760
your own language, please check out the link in the description.

24
00:02:19,760 --> 00:02:24,600
If you haven't already read the booklet, if you have and would like to translate it,

25
00:02:24,600 --> 00:02:29,760
please just send me an email and let me know and I'll confirm that whether or not someone

26
00:02:29,760 --> 00:02:33,120
has already started that language or not.

27
00:02:33,120 --> 00:02:40,640
The second announcement is that I'm tentatively planning to make a trip.

28
00:02:40,640 --> 00:02:46,760
This will be only interesting for certain people down the east coast of the United States.

29
00:02:46,760 --> 00:02:48,440
In June.

30
00:02:48,440 --> 00:02:59,040
So June 2014, if you are somewhere between roughly between New York and Florida and would

31
00:02:59,040 --> 00:03:09,240
like to host me and a couple other people to give teachings or to just meet up and talk

32
00:03:09,240 --> 00:03:15,640
about meditation on the way, please let me know because we'll be taking a Greyhound

33
00:03:15,640 --> 00:03:19,240
bus down the coast.

34
00:03:19,240 --> 00:03:22,840
Someone's already talked about us going to Vermont, so we might head up there first and

35
00:03:22,840 --> 00:03:30,200
then head down to New York and New Jersey and wherever, anywhere along that area, we'll

36
00:03:30,200 --> 00:03:35,640
be setting up tents and people's backyard, me and at least one other person, maybe a

37
00:03:35,640 --> 00:03:42,560
couple more and hopefully doing a little bit of spreading of the dhamma at the time will

38
00:03:42,560 --> 00:03:48,080
try to keep some kind of an active internet presence during the trip so people know where

39
00:03:48,080 --> 00:04:00,440
we are and it can kind of be an interactive live sort of dhamma tour and there's a website

40
00:04:00,440 --> 00:04:06,440
for that, or I'm using an old website that we'd used for something similar at jarikandat

41
00:04:06,440 --> 00:04:12,200
seramongalod.org, I'll put the link as well in the description if I can and you can use,

42
00:04:12,200 --> 00:04:18,560
there's not much there yet because I'm not at a computer right now but when I get back to

43
00:04:18,560 --> 00:04:21,920
Canada, we'll try to put more information and kind of submit if I think, but if you're

44
00:04:21,920 --> 00:04:27,320
interested and if you're in that area, please let me know and there's a contact form there

45
00:04:27,320 --> 00:04:31,960
that you can use or if you have my email or have some other way of getting in touch with

46
00:04:31,960 --> 00:04:39,920
maybe a Facebook or Google or whatever and let us know because we'll try to quickly put

47
00:04:39,920 --> 00:04:46,240
out the announcement for people who are in the various areas if they want to join up and

48
00:04:46,240 --> 00:04:52,240
meet up and listen to the dhamma or talk about the dhamma and so on.

49
00:04:52,240 --> 00:05:00,280
So that's all, I have those two announcements to give and hopefully these things will come

50
00:05:00,280 --> 00:05:04,640
to fruition and that there will be more in the future.

51
00:05:04,640 --> 00:05:05,640
That's all.

52
00:05:05,640 --> 00:05:13,440
I'm well, I'm in here in Bangkok, just had my eyes fixed so I won't be wearing glasses

53
00:05:13,440 --> 00:05:20,840
anymore, I don't think, not until I go blind from old age and been doing a little bit

54
00:05:20,840 --> 00:05:27,120
of teaching here in Bangkok yesterday and the day before I was giving talks in the downtown

55
00:05:27,120 --> 00:05:33,080
area and today I'll give the third talk and next Sunday, this Sunday I'll be giving

56
00:05:33,080 --> 00:05:41,760
a one day course somewhere near the democracy monument.

57
00:05:41,760 --> 00:05:48,440
Otherwise just hanging out and getting ready, I'm also started doing the poly studies

58
00:05:48,440 --> 00:05:54,120
in Thai so that's going to be taking up a bunch of my time for the next two years when

59
00:05:54,120 --> 00:06:01,880
I take these two exams to try and get certified as a Thai poly scholar which shouldn't

60
00:06:01,880 --> 00:06:11,080
be helpful for recognition purposes, people won't be able to say that I don't know my

61
00:06:11,080 --> 00:06:12,240
poly.

62
00:06:12,240 --> 00:06:17,560
Lots of things, so lots of things going on have many projects on the go so you can keep

63
00:06:17,560 --> 00:06:23,000
in touch here on YouTube, keep up by subscribing to my channel, I'll try to as I said

64
00:06:23,000 --> 00:06:28,560
maybe post updates every so often along with my regular videos or follow my web blog

65
00:06:28,560 --> 00:06:36,000
or Facebook or Google Plus, I'm pretty active these days, at least posting if not

66
00:06:36,000 --> 00:06:41,240
checking responses, so thanks for keeping up, it's good to see we have over 4 million views

67
00:06:41,240 --> 00:06:47,040
I think on YouTube now, 25,000 subscribers is great, pretty cool, so thanks for all your

68
00:06:47,040 --> 00:06:54,040
interest and keep practicing, and all the best.

