import json
from glob import glob
from os import path
import sqlite3

if __name__ == "__main__":
    con = sqlite3.connect("data.db")
    cur = con.cursor()

    cur.execute("CREATE VIRTUAL TABLE videos USING fts5(id UNINDEXED, title, transcript, date UNINDEXED);")

    metadata = {}
    videos = []

    for fn in glob('./video-metadata/*.json'):
        data = json.loads(open(fn).read())
        metadata[data['id']] = data

    for fn in glob('subtitles/*.json'):
        try:
            items = json.loads(open(fn).read())

            idx = path.basename(fn).replace('.json', '')
            transcript_with_timestamps = ' '.join([
                item for sublist in 
                [('T_' + str(int(t['start'])), t['text']) for t in items] 
                for item in sublist
            ])

            videos.append((
                idx, 
                metadata[idx]['fulltitle'], 
                transcript_with_timestamps,
                metadata[idx]['upload_date']
            ))

        except Exception as e:
            print('failed', fn)
            print(e)
            
    cur.executemany("insert into videos values (?, ?, ?, ?)", videos)

    con.commit()
    con.close()
