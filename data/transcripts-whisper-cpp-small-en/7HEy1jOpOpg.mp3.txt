 Hello and welcome back to our study of the Dhammapada.
 Today we continue on with
 verses 104 and 105 which read as follows.
 Atahvajitang seyo yajayang itarapajah atadantasa posasa n
ityansanya tacharino
 nivadevo nagandambhu namaro sahabrahmana
 chitang apajitang gahira tatarupasa jantunu which means it
's actually quite
 similar to the last one. Conquest of the self is better
 than the
 conquest of others, of other people. Atadantasa posasa nity
ansanya tacharino
 for one who has conquered themselves and is constantly
 restrained, sanyat
 tacharino, one who fares restrained or controlled in a
 sense of not
 giving rise to uncontrolled behavior or activity that
 causes harm to others.
 Nivadevo nagandambhu nadan angel nagandambha which is also
 another
 type of angel I guess. Namaro sahabrahmana not Mara which
 is like the evil
 Satan nor Brahma's gods. Chitang apajitang gahira can turn
 their victory into
 defeat. Chitang apajitang apajita is defeat I believe. Chit
ang apajitang means
 turning victory into defeat. They will not be able to for
 such a person. They
 will not be able to turn the victory of such a person into
 defeat. Great verse
 shows the power of self-conquest. Simple story. The story
 goes there was a Brahmin
 who came to the Buddha and asked him a question. I guess
 maybe he was a follower
 of the Buddha or he understood how worthy the Buddha was or
 how wise the
 Buddha was. He thought he had this question. He wondered to
 himself whether the Buddha, whether Buddhas, whether those
 who are
 enlightened know only what is of benefit or they also know
 what leads to
 detriment. Do they only know about gain or do they know
 about loss as well?
 Ata, do they know ata me wajanati udahu anatampi puchisa?
 Anatampi. Do they know
 only what is ata? Ata means what is of benefit or what is
 of value or do they
 also know what is of detriment? Anata which is which is
 harmful or leads one
 away from success, leads one away from what is useful, that
 which is useless or
 harmful. So he went to ask the Buddhists. I guess he had
 some idea that the
 Buddha only taught people good things. He only taught so
 everyone was praising how
 the Buddhist teachings would lead people to good things and
 they were talking
 about how great the Buddhist teaching Buddha would teach
 you do this do that
 and so he hadn't heard the Buddha talk about don't do this
 don't do that and so
 he went to the Buddha and the Buddha gave him an
 interesting quote and I was
 just having a terrible time with it because the English is
 it's incredible
 how sloppy they were with it. They translate fierce fierc
eness, chandikang,
 as moonlight which is which is well I mean it's
 understandable because
 chanda would be the moon but chanda with the
 with the Helveopalasial ND anyway doesn't mean moon at all
 unless there's
 another version that confuses the two but it means ferocity
 so the Buddha was
 the Buddha says gives a list of six things and I think I've
 got them all
 down and he says so he goes and asked the Buddha do you
 know about loss as well
 and the Buddha says of course and he tells him these he
 says atanjahang
 brahmanajanami anatanjha I know both that which is of
 benefit and that which
 is to detriment and so he taught says please then tell me
 about what is to
 detriment and the Buddha says well then listen and he gives
 this verse
 which isn't the Dhammapandu verse but there's this is often
 the case with these
 stories there will be other verses that where they come
 from exactly we don't
 know where they've been recorded we don't know but it's
 supposed to have
 been said by the Buddha as well so he says usura seyang
 which means clear that
 one's clear it means sleeping beyond sunrise so sleeping
 during the day when
 it's time to get up and go to work one instead stays in bed
 and doesn't do
 one's work alas young laziness and one is just lazy and
 doesn't want to do good
 things chandikang means again ferocity so being mean or
 nasty or cruel being
 being you know when people are overbearing and just
 constantly irritable
 and then Ligasone de young is an interesting one but I
 think because
 Ligas means long but Sondia means means addiction to
 intoxication or addiction
 to drink usually refers to and maybe intoxication is better
 so I think it
 says something to do with in drinking intoxicating drinks
 and it may be a
 corruption but I probably just don't know what it means
 anyway but the
 English is terrible it doesn't have any of that what does
 it say long continued
 prosperity which is ridiculous Ligas is long but long
 continued prosperity is
 not a cause for ruin except it can make you negligent I
 suppose but that's
 certainly not what's being said here and then we've got I
 think it's addiction to
 strong drink why because these come from the or these are
 echoed in the
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 the D
 and it's curious, you know, because he doesn't say gambling
 and yet gambling is considered one of the things that leads
 to laws.
 I don't believe it's in here. If I've translated it
 correctly, it's not there.
 And in the Sigalawada Sutta, it's clearly there.
 And that's interesting because the Brahman is then
 impressed and says, "Very good, very good. That's awesome.
 "Well, well spoken. You're really, really a great teacher
 and you really know what is a benefit and what is a
 detriment."
 And then the Buddha asks him, "So Brahman, how do you make
 your livelihood?"
 And turns out the Brahman makes his, this Brahman makes his
 livelihood through gambling.
 That's how he makes a living.
 I guess by, he must be sort of one of these people who, a
 con artist, right?
 Or something like that. How to make his, I guess there are
 professional poker players nowadays.
 It's a big thing apparently to gamble online.
 I have people who tell me about that, how they do gambling
 online.
 And so it's interesting that the Buddha didn't include it,
 but that is common to be very careful not to attack a
 person directly.
 So if he had brought up gambling, it would have, there's a
 little bit, as a Buddhist, it's more curious than this Brah
man probably realized.
 He doesn't bring it up, but yet he wants to make it clear
 that gambling is in there as well without actually saying,
 "Oh, gambling is going to lead you to ruin."
 It's as though he purposefully left it out or it's, this
 story, part of the reason for telling this story is to,
 or part of the meaning behind the story is to show that he
 didn't attack him directly. It's quite curious.
 But can be a really important aspect of teaching. So anyway
, the point is he tells him,
 the Brahman says he makes his living by gambling.
 And this is the funny sort of inside joke that here we can
 kind of smile because we know what's kind of what's going
 through the Buddhist mind to some extent.
 That he knows that this Brahman is on a state of loss and
 is heading in a bad way.
 And so then the Buddha asks him, "So who wins when you
 gamble? Do you win or does your opponents win?"
 He said, "Oh, well, sometimes I win, sometimes my opponents
 win."
 And then the Buddha says, then the Buddha lays down the law
 and says, "Brahman, that's not a real victory."
 As you can see, that's the nature of victory. Sometimes you
 win, sometimes you lose.
 And we have this saying, "You win some, you lose some."
 That's not a really... I mean, on balance you come out with
 nothing, right? You never come out ahead.
 Or it's certainly not guaranteed that you're going to come
 out ahead. And there must be some other factors.
 Often people would say luck is all that allows you to come
 out ahead.
 And as we talked about, of course, it doesn't matter
 whether you come out ahead or not.
 When you come out ahead, what happens? Well, you beget enm
ity. Your opponents are upset because they've lost.
 When you win, someone has to lose. That kind of victory is
 not really...
 He says a trifling. That's how the English translate it.
 You can't really trust the English, no, can we?
 "Parajayo." He says that's a low sort of... I believe...
 Jayo? No, Jayopi?
 "Oti, parajayo ti." No, that's a question, sorry.
 "Brahman apamatakoya." So it's a trifle.
 He doesn't say that's a bad kind of victory or that kind of
 victory is actually leading you to ruin, which in fact it
 is.
 He says that's a trifling victory. So in a roundabout way,
 he's trying to get this Brahman to see the error of his
 ways.
 You know, provide him with some friendly advice. The Brah
man did ask, after all, what leads to loss.
 And so he's trying to somehow tell him that his life's down
 to that.
 But instead he says, so to be very gentle, he says that's a
 trifling victory. A real victory is self-victory.
 A real conquest? That's conquering yourself. Why? Because
 that kind of conquest, you'll never lose.
 And so it's poignant in relation to this story, the idea of
 being invincible.
 Because the win of a gambler is always subject to threat.
 The next roll of the dice, the next deal of the cards could
 change your fortune, could spell ruin.
 And much victory is like this. Victory in war, victory in
 business, victory in romance, victory of all kinds is
 unpredictable.
 And is unstable, is impermanent. It doesn't last forever.
 It's always subject to the potential of being overthrown.
 But that's really the claim that's being made. So this is
 how it relates to our practice.
 It's an understanding of the extent and the magnitude of
 the practice that we're undertaking.
 I mean, this isn't just stress relief. I was thinking today
 about advertising the benefits of meditation.
 We're probably going to put up posters. And it made me
 think of, you know, people really, really,
 it's really catchy to say mindfulness-based stress
 reduction because it's non-sectarian, it's non-religious.
 It's really something that people can, sort of, rolls off
 the tongue.
 But then I thought it's kind of, you know, petty to say
 stress reduction because that's not really what we're
 talking about, is it?
 In Buddhism we go quite, it's mindfulness-based or it's
 based on sati.
 But it goes farther than just reducing stress. It uproots
 anything that could possibly cause you stress in the future
.
 There's no potential for future stress. Meaning you're
 invincible.
 And it goes beyond heaven, it goes beyond God, it goes
 beyond the universe.
 Something that goes beyond loss, something that can never
 be taken from you.
 We're talking about invincibility. How does this come about
?
 Comes about by a person who is ata-danta, bhosa, nityang s
anyatacarino, who is constantly, constantly restrained.
 Meaning they never, when seeing a form with the eye, they
 never give rise to likes or dislikes.
 They see it as it is in their objective.
 They free themselves from the potential for, they free
 themselves from involvement in this game of gain and loss
 and happiness and suffering.
 It sounds actually quite fearsome for people who are
 addicted to pleasure and who are caught up in this game of
 gain and loss.
 So it's something that you do have to kind of finesse when
 you talk to people.
 And people often accuse, make the accusation that such a
 person would be someone like a zombie, right?
 When they are restrained, means they are just suppressing
 their urges, which isn't at all the case.
 Suppressing your urges wouldn't work. It's not something
 that is sustainable.
 It's not the true victory. True victory is invincible,
 imperturbable.
 I was thinking about this today, the whole idea of the
 zombie, right?
 So we have this idea that a person who doesn't like and
 dislike things is just like a zombie.
 Their life has no flavor, has no meaning.
 But then if you look at people who are caught up, if you
 think of the state of mind,
 the person who is caught up in chasing after things,
 objects of their desire, how much like a zombie they are,
 how unaware they are of their own mind, of their own state,
 how inflamed the mind becomes with passion and hate,
 and how the mind just in general becomes inflamed, and so
 is subject to greed, anger and delusion constantly.
 Sometimes with such intense greed that they will do
 anything to get what they want,
 even at the detriment to other people, even at the cost of
 friendship.
 Sometimes give rise to such states of delusion that they
 alienate all of their friends
 due to their oppressive, arrogant, conceited nature, the
 level of conceit that we're capable of,
 the level of arrogance we're capable of, and how awful it
 is, and how harmful it is.
 And anger, how we can be so caught up with rage and hatred,
 constantly depressed or angry, sad, afraid, all of these
 negative states.
 It's kind of like a zombie, like one of those zombie movies
 or zombie shows that everyone's watching.
 Evil zombies, how horrific they are. That's kind of what we
're talking about.
 Enlightened people are nothing like that.
 An enlightened one is the opposite of a zombie. They are
 awake, they are alive, they never die, they're invincible.
 And so this is what we're striving for. It's something
 quite profound.
 I teach meditation, now teaching at the university, and I
 teach to all sorts of people,
 and a lot of people are just in it for stress reduction,
 stress relief.
 And so it kind of pulls you in and you start to realize, oh
, it goes a little bit deeper,
 and you end up down the rabbit hole, so to speak, where you
 don't recognize the world around you anymore.
 Your whole world changes. I mean, it's not like it's a trap
 or something, it's just the sweater starts to unravel.
 The whole illusion that you built up around yourself, and
 who you thought you were starts to unravel,
 and you see how asleep you've been. You're like a sleep
walker, like a zombie.
 That's how it should be. That's how it really is in
 meditation.
 You start to wake up and see, oh, I've got these problems.
 My mind has greed, my mind has anger, my mind has delusion.
 Oh boy, I'm in trouble. And so you start to do the hard
 work of changing that.
 That's what we do in meditation.
 And that is the highest victory. It's a victory that no one
 can take from you.
 That's the point of this verse, and it's very well said, as
 are all the things the Buddha said.
 So something that we should appreciate and remember to give
 us confidence that what we're aiming for is not something
 simple.
 It's not just a hobby that you can pick up and put down.
 Something that really and truly changes your life,
 transforms you, wakes you up, and helps you rise above your
 troubles.
 So that even though the situation may not change, no matter
 what the situation is, you are above it, you are beyond it.
 So that's the teaching for tonight, verses 104 and 105.
 We're almost a quarter of the way through the Dhammapada. I
 think when we get to 108, that'll be one quarter of the way
.
 So thank you all for tuning in. I'm wishing you all the
 best on your path to invincibility. Be well.
