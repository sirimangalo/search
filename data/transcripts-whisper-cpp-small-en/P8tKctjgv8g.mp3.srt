1
00:00:00,000 --> 00:00:07,170
 It says, "Bonti, I have been thinking recently, I have

2
00:00:07,170 --> 00:00:07,760
 already made mention of how some of

3
00:00:07,760 --> 00:00:12,600
 the Christians hand out magazines to me at the bus stop.

4
00:00:12,600 --> 00:00:17,840
 Would it be wrong if I told them I'll take it if they

5
00:00:17,840 --> 00:00:21,160
 listen to what I have to say?

6
00:00:21,160 --> 00:00:30,540
 Not so much they are wrong, but wouldn't have any effect

7
00:00:30,540 --> 00:00:31,600
 upon others anyway."

8
00:00:31,600 --> 00:00:37,320
 What do you think, Larry?

9
00:00:37,320 --> 00:00:39,320
 Is that wrong?

10
00:00:39,320 --> 00:00:45,070
 "Yes, in a way I think it is wrong, because first of all

11
00:00:45,070 --> 00:00:48,440
 they're wasting other people's

12
00:00:48,440 --> 00:00:54,180
 time and as he stated, he or she, handing out one pamphlet

13
00:00:54,180 --> 00:00:57,480
 is not going to make a difference

14
00:00:57,480 --> 00:01:02,470
 in how someone feels about their inner beliefs in the first

15
00:01:02,470 --> 00:01:03,360
 place.

16
00:01:03,360 --> 00:01:07,600
 So really it is quite agitating.

17
00:01:07,600 --> 00:01:10,220
 We have some people here in the state that go door to door

18
00:01:10,220 --> 00:01:11,480
 from time to time handing

19
00:01:11,480 --> 00:01:13,680
 out pamphlets.

20
00:01:13,680 --> 00:01:17,200
 I usually just out of courtesy take them and say thank you

21
00:01:17,200 --> 00:01:18,840
 and I go chunk them as soon

22
00:01:18,840 --> 00:01:22,230
 as they leave the front door because I'm not going to read

23
00:01:22,230 --> 00:01:24,120
 them because I know basically

24
00:01:24,120 --> 00:01:27,880
 I've read enough to basically know some of their beliefs

25
00:01:27,880 --> 00:01:29,700
 and I just have no interest

26
00:01:29,700 --> 00:01:32,280
 if they want to believe that way fine.

27
00:01:32,280 --> 00:01:36,920
 But if not, I believe the way I would want to believe.

28
00:01:36,920 --> 00:01:48,880
 So I think to be polite to those individuals would be just

29
00:01:48,880 --> 00:01:54,280
 to say I'd rather not take your

30
00:01:54,280 --> 00:01:59,710
 your pamphlets because I really don't want your pamphlet

31
00:01:59,710 --> 00:02:01,120
 and leave it at that.

32
00:02:01,120 --> 00:02:05,620
 A confrontation particularly in a bus station or an airport

33
00:02:05,620 --> 00:02:07,640
 is futile to begin with.

34
00:02:07,640 --> 00:02:09,880
 No one is going to convince anyone else of anything.

35
00:02:09,880 --> 00:02:14,580
 So they believe that, they believe in apostasizing and that

36
00:02:14,580 --> 00:02:17,640
's their belief, that's fine but just

37
00:02:17,640 --> 00:02:24,080
 don't take it out on me.

38
00:02:24,080 --> 00:02:25,080
 Anybody else?

39
00:02:25,080 --> 00:02:26,080
 Not wrong.

40
00:02:26,080 --> 00:02:30,960
 I don't think it's an eye for an eye kind of thing.

41
00:02:30,960 --> 00:02:34,560
 You have to listen to me and I'll listen to you.

42
00:02:34,560 --> 00:02:36,400
 Fair as fair, right?

43
00:02:36,400 --> 00:02:42,240
 Yeah, I would just say thanks for calling my ones.

44
00:02:42,240 --> 00:02:47,620
 If it's wrong for them to be confrontational in forcing

45
00:02:47,620 --> 00:02:51,240
 their views upon you or if it seems

46
00:02:51,240 --> 00:02:55,940
 kind of rude and so on then adding rude to rude is not

47
00:02:55,940 --> 00:02:58,080
 probably the best.

48
00:02:58,080 --> 00:03:02,150
 What I find works much better is to inquire as to their

49
00:03:02,150 --> 00:03:03,920
 beliefs politely.

50
00:03:03,920 --> 00:03:10,810
 What I find has the best effect on people who engage in

51
00:03:10,810 --> 00:03:15,000
 such blind faith in things like

52
00:03:15,000 --> 00:03:23,340
 God and so on and other imaginary things is to inquire and

53
00:03:23,340 --> 00:03:27,680
 actually investigate with them

54
00:03:27,680 --> 00:03:29,640
 the way Socrates did.

55
00:03:29,640 --> 00:03:34,200
 Not exactly the way Socrates did but politely asking and

56
00:03:34,200 --> 00:03:36,920
 because they have so much stress,

57
00:03:36,920 --> 00:03:40,070
 I've talked about this before, these people have so much

58
00:03:40,070 --> 00:03:41,320
 stress and tension.

59
00:03:41,320 --> 00:03:44,680
 This is the person who asked the question before, I don't

60
00:03:44,680 --> 00:03:46,280
 remember it but I remember

61
00:03:46,280 --> 00:03:49,510
 answering this somehow that they have so much stress and

62
00:03:49,510 --> 00:03:51,320
 tension built up that really all

63
00:03:51,320 --> 00:03:54,360
 they want is to let go.

64
00:03:54,360 --> 00:03:57,960
 Not want but really all they need and really what would

65
00:03:57,960 --> 00:04:00,180
 make them happy, truly happy is

66
00:04:00,180 --> 00:04:05,350
 to let go and stop trying to justify something that's

67
00:04:05,350 --> 00:04:09,600
 totally unjustifiable and irrational,

68
00:04:09,600 --> 00:04:10,600
 trying to rationalize it.

69
00:04:10,600 --> 00:04:14,430
 Here in the United States so many times, one particular

70
00:04:14,430 --> 00:04:16,720
 denomination that I'm familiar

71
00:04:16,720 --> 00:04:20,680
 with, they send their children around door to door.

72
00:04:20,680 --> 00:04:24,970
 They sit in the car, I don't know if they came to come up

73
00:04:24,970 --> 00:04:27,400
 or afraid or what but they'll

74
00:04:27,400 --> 00:04:31,290
 send their children to the door with these pamphlets and

75
00:04:31,290 --> 00:04:33,400
 you can't confront those kids

76
00:04:33,400 --> 00:04:38,360
 because quite frankly I think they've been brainwashed.

77
00:04:38,360 --> 00:04:41,830
 It's just like the church out in Missouri that their

78
00:04:41,830 --> 00:04:44,200
 parents and their kids, I'm sure

79
00:04:44,200 --> 00:04:47,970
 all of you are familiar with this, at least the ones here

80
00:04:47,970 --> 00:04:49,920
 in the states where they're

81
00:04:49,920 --> 00:04:55,020
 protesting the military funerals of soldiers that have been

82
00:04:55,020 --> 00:04:57,240
 killed in action and these

83
00:04:57,240 --> 00:05:00,770
 church members and their children, they've all been indoctr

84
00:05:00,770 --> 00:05:01,440
inated.

85
00:05:01,440 --> 00:05:04,970
 The ones here locally are not affiliated with that at all

86
00:05:04,970 --> 00:05:06,920
 but I do Google them from time

87
00:05:06,920 --> 00:05:10,880
 to time and find out that it happens to be one of the, I

88
00:05:10,880 --> 00:05:13,200
 guess you could call it a sect,

89
00:05:13,200 --> 00:05:17,620
 they wouldn't call themselves a sect but they happen to be

90
00:05:17,620 --> 00:05:21,560
 a group that feel like they're

91
00:05:21,560 --> 00:05:24,920
 wrong and everyone, I mean they're right and everyone else

92
00:05:24,920 --> 00:05:26,640
 is wrong and the fact I've read

93
00:05:26,640 --> 00:05:32,760
 some articles written by previous members of that

94
00:05:32,760 --> 00:05:37,200
 particular religious sect that say

95
00:05:37,200 --> 00:05:40,200
 once you are...

96
00:05:40,200 --> 00:05:51,520
 We're losing you Larry.

97
00:05:51,520 --> 00:05:57,660
 I think his internet term that they use in that particular

98
00:05:57,660 --> 00:06:00,320
 sect but it is true too so

99
00:06:00,320 --> 00:06:04,400
 it's a very sticky question.

100
00:06:04,400 --> 00:06:08,210
 I just feel like personally I just really don't have the

101
00:06:08,210 --> 00:06:09,480
 time for them.

102
00:06:09,480 --> 00:06:14,090
 I try to get out of the situation as quickly as I can

103
00:06:14,090 --> 00:06:17,680
 without being confrontational with

104
00:06:17,680 --> 00:06:21,470
 them but they're just not going to convince me of their

105
00:06:21,470 --> 00:06:23,800
 ways regardless of what they say

106
00:06:23,800 --> 00:06:27,540
 or what they give me so to me it's just a waste of my time

107
00:06:27,540 --> 00:06:30,040
 there, let them go find somebody

108
00:06:30,040 --> 00:06:36,880
 that will believe what they say.

109
00:06:36,880 --> 00:06:38,520
 With children it's especially difficult.

110
00:06:38,520 --> 00:06:43,810
 I remember once these evangelicals came to the monastery

111
00:06:43,810 --> 00:06:46,560
 and once they had me targeted

112
00:06:46,560 --> 00:06:52,540
 this woman, there were some women there and then one of

113
00:06:52,540 --> 00:06:56,440
 them was the leader and she totally

114
00:06:56,440 --> 00:07:01,370
 cold and I said would you like this card and they said no,

115
00:07:01,370 --> 00:07:03,880
 no, she said for everyone no

116
00:07:03,880 --> 00:07:07,630
 we don't want your card, just in a really cold and hard way

117
00:07:07,630 --> 00:07:09,480
 and then suddenly the next

118
00:07:09,480 --> 00:07:14,630
 night she comes back with her husband and her two kids and

119
00:07:14,630 --> 00:07:16,880
 I think I started talking

120
00:07:16,880 --> 00:07:21,760
 to the kids and it was well depressing because they

121
00:07:21,760 --> 00:07:25,560
 obviously have no ability to think for

122
00:07:25,560 --> 00:07:33,400
 themselves and have never been taught how to use the reason

123
00:07:33,400 --> 00:07:36,640
 circuits in their brain

124
00:07:36,640 --> 00:07:43,620
 or in the mind, the mind's ability to investigate and they

125
00:07:43,620 --> 00:07:47,320
've been totally speaking, talking

126
00:07:47,320 --> 00:07:53,340
 of the brain as a filter and well their brain is like a

127
00:07:53,340 --> 00:07:58,040
 vice, vice grip, keeping them locked

128
00:07:58,040 --> 00:08:01,760
 in on certain predefined views.

129
00:08:01,760 --> 00:08:09,380
 So I asked them and the parents said go ahead Tommy, tell

130
00:08:09,380 --> 00:08:13,480
 him and then which is really sad

131
00:08:13,480 --> 00:08:17,480
 because yeah what can you do with kids?

132
00:08:17,480 --> 00:08:23,590
 They're so malleable that you can twist them and turn them

133
00:08:23,590 --> 00:08:25,920
 in any sort of way.

134
00:08:25,920 --> 00:08:32,790
 They're not strong enough to reject the things that their

135
00:08:32,790 --> 00:08:35,600
 parents teach them.

136
00:08:35,600 --> 00:08:42,150
 When they get older they may be able to but you still have

137
00:08:42,150 --> 00:08:48,640
 to feel for these people and

138
00:08:48,640 --> 00:08:53,240
 to some extent, not to a very big extent, but to some

139
00:08:53,240 --> 00:08:55,840
 extent see their suffering or

140
00:08:55,840 --> 00:08:58,080
 from my point of view because what happened then, I think I

141
00:08:58,080 --> 00:08:59,280
've told this story before,

142
00:08:59,280 --> 00:09:01,820
 the next day I said well if they're coming to visit me I

143
00:09:01,820 --> 00:09:03,320
 should go visit them and then

144
00:09:03,320 --> 00:09:07,160
 the next day I went to their church which was this huge

145
00:09:07,160 --> 00:09:09,440
 church just down the road from

146
00:09:09,440 --> 00:09:13,540
 what, Thai of Los Angeles and it was such an interesting

147
00:09:13,540 --> 00:09:14,760
 experience.

148
00:09:14,760 --> 00:09:19,650
 I talked with this teacher of preachers, he was a preacher

149
00:09:19,650 --> 00:09:23,760
 teacher, he taught preachers,

150
00:09:23,760 --> 00:09:30,700
 pastors, he taught pastors and he was suffering, it was so

151
00:09:30,700 --> 00:09:32,440
 easy to see.

152
00:09:32,440 --> 00:09:36,430
 I made him see, not to a great extent but he must be

153
00:09:36,430 --> 00:09:39,000
 dealing with this conflict inside

154
00:09:39,000 --> 00:09:43,610
 of himself every day, how difficult it is to keep this

155
00:09:43,610 --> 00:09:46,040
 faith that is so ungrounded in

156
00:09:46,040 --> 00:09:52,880
 any form of reality.

157
00:09:52,880 --> 00:10:00,500
 To the extent that you're able to gently show them their

158
00:10:00,500 --> 00:10:04,640
 own suffering, I think you do them

159
00:10:04,640 --> 00:10:11,040
 a great deal of benefit.

160
00:10:11,040 --> 00:10:18,020
 I don't mind if you talk to me, I don't mind if you give me

161
00:10:18,020 --> 00:10:20,400
 your brochure.

162
00:10:20,400 --> 00:10:26,480
 Why because I'm happy, I'm settled in who I am.

163
00:10:26,480 --> 00:10:30,340
 You want to give me your brochure, sure, it's just a piece

164
00:10:30,340 --> 00:10:32,400
 of paper but somehow bringing

165
00:10:32,400 --> 00:10:35,690
 across to them a sense of peace and happiness and helping

166
00:10:35,690 --> 00:10:37,880
 them to see that that's not possible

167
00:10:37,880 --> 00:10:43,410
 to gain with blind faith, that you are free from something

168
00:10:43,410 --> 00:10:48,640
 that they are not free from.

169
00:10:48,640 --> 00:10:52,000
 I think this doesn't come from imposing your views on them,

170
00:10:52,000 --> 00:10:53,800
 it comes from being accepting

171
00:10:53,800 --> 00:10:56,780
 and open and loving and caring towards them because really

172
00:10:56,780 --> 00:10:58,200
 that's generally why people

173
00:10:58,200 --> 00:11:06,050
 go to such cults you might say or sects, is because of the

174
00:11:06,050 --> 00:11:09,960
 love that they find, the need

175
00:11:09,960 --> 00:11:16,600
 that they have for being controlled or for being taken care

176
00:11:16,600 --> 00:11:19,960
 of, the need for in Judeo-Christian

177
00:11:19,960 --> 00:11:28,600
 religions the need for a father figure.

178
00:11:28,600 --> 00:11:31,170
 It's such a fatherly religion, if you actually read the

179
00:11:31,170 --> 00:11:32,840
 things that Jesus says it's very

180
00:11:32,840 --> 00:11:35,240
 much, very oppressive actually.

181
00:11:35,240 --> 00:11:40,600
 Some of it's nice but a lot of it's just your father, your

182
00:11:40,600 --> 00:11:47,880
 father and how you have to.

183
00:11:47,880 --> 00:11:52,820
 So if you help people to see the possibility of just loving

184
00:11:52,820 --> 00:11:56,120
 without needing, without needing,

185
00:11:56,120 --> 00:11:58,760
 then you do them a favour.

186
00:11:58,760 --> 00:12:01,110
 This is the thing, rather than seeing it as a conflict,

187
00:12:01,110 --> 00:12:02,880
 rather than seeing it as a confrontation

188
00:12:02,880 --> 00:12:10,240
 you should see it as a fellow human being who you might be

189
00:12:10,240 --> 00:12:12,360
 able to help.

190
00:12:12,360 --> 00:12:15,060
 If you think in terms of helping this person and not in a

191
00:12:15,060 --> 00:12:16,840
 condescending or self-righteous

192
00:12:16,840 --> 00:12:19,930
 way, but just in the way that you would help someone who

193
00:12:19,930 --> 00:12:21,720
 needs food and give them some

194
00:12:21,720 --> 00:12:24,930
 food, you're not judging them and you're not thinking to

195
00:12:24,930 --> 00:12:26,480
 look down on them or that you're

196
00:12:26,480 --> 00:12:29,900
 better than them, but just look and see if there's any way

197
00:12:29,900 --> 00:12:31,880
 you can help you with children.

198
00:12:31,880 --> 00:12:34,620
 If it's children bringing pamphlets to your door there's

199
00:12:34,620 --> 00:12:36,040
 not much you can do but you can

200
00:12:36,040 --> 00:12:37,040
 love them.

201
00:12:37,040 --> 00:12:39,170
 It makes you happy and these kids come to your door and

202
00:12:39,170 --> 00:12:40,740
 they want to give you some brochures

203
00:12:40,740 --> 00:12:45,800
 and say thank you, I hope you have a good day.

204
00:12:45,800 --> 00:12:48,430
 It's good for you, that's a good thing that you've done for

205
00:12:48,430 --> 00:12:48,880
 them.

206
00:12:48,880 --> 00:12:51,400
 It's not going to change them or make them doubt their

207
00:12:51,400 --> 00:12:53,160
 religion, not in any significant

208
00:12:53,160 --> 00:12:55,600
 way, but it's what you can do.

209
00:12:55,600 --> 00:13:00,930
 You gave them, it's like you gave them a muffin or

210
00:13:00,930 --> 00:13:02,520
 something.

211
00:13:02,520 --> 00:13:07,160
 You gave them something that was good for them.

212
00:13:07,160 --> 00:13:10,960
 It's not going to solve the world's problems, but we're not

213
00:13:10,960 --> 00:13:13,320
 here to solve the world's problems.

214
00:13:13,320 --> 00:13:16,480
 We're here to do good things, avoid evil things and purify

215
00:13:16,480 --> 00:13:17,440
 our own minds.

216
00:13:17,440 --> 00:13:22,840
 So whatever helps you to do that, that's what I would do.

217
00:13:22,840 --> 00:13:24,360
 Hopefully that's what I would do.

218
00:13:24,360 --> 00:13:27,210
 I might just get all self-righteous on them and start

219
00:13:27,210 --> 00:13:29,080
 telling them about Buddhism and

220
00:13:29,080 --> 00:13:33,320
 so on, but I should.

221
00:13:33,320 --> 00:13:36,480
 I'm not saying that I practice everything that I preach,

222
00:13:36,480 --> 00:13:38,080
 but do as I say, not as I maybe

223
00:13:38,080 --> 00:13:38,680
 do sometimes.

224
00:13:38,680 --> 00:13:39,680
 Thank you.

