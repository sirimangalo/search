1
00:00:00,000 --> 00:00:02,880
 How can I cultivate effort?

2
00:00:02,880 --> 00:00:05,920
 Which way can I cultivate effort?

3
00:00:05,920 --> 00:00:08,160
 It is hard for me to meditate.

4
00:00:08,160 --> 00:00:10,640
 How do I make it easier?

5
00:00:10,640 --> 00:00:12,640
 Meditation is hard.

6
00:00:12,640 --> 00:00:17,120
 Dissatisfaction with quality or amount of practice is a

7
00:00:17,120 --> 00:00:19,600
 recognition of imperfection.

8
00:00:19,600 --> 00:00:22,560
 You just have to work on becoming more perfect.

9
00:00:22,560 --> 00:00:26,570
 It may take years or lifetimes to become a perfect medit

10
00:00:26,570 --> 00:00:27,280
ator.

11
00:00:27,280 --> 00:00:30,960
 There is no quick fix and no pill that one can take

12
00:00:30,960 --> 00:00:35,130
 that suddenly gives one the qualities necessary to make it

13
00:00:35,130 --> 00:00:36,640
 feel effortless.

14
00:00:36,640 --> 00:00:39,600
 Effort is something one has to work at.

15
00:00:39,600 --> 00:00:43,990
 Asking for the ways to make it easier is probably not the

16
00:00:43,990 --> 00:00:46,400
 best way to cultivate effort.

17
00:00:46,400 --> 00:00:50,950
 Look at it as a chance to find ways to become stronger than

18
00:00:50,950 --> 00:00:53,120
 be experienced easier.

19
00:00:53,120 --> 00:00:57,870
 Association with good people can help. The Buddha

20
00:00:57,870 --> 00:01:00,320
 recommended us to associate with good people

21
00:01:00,320 --> 00:01:05,050
 as when one associates with good people, good qualities

22
00:01:05,050 --> 00:01:06,400
 increase.

