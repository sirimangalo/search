 Hello, everyone.
 Broadcasting live, getting ready to.
 So see, on days where I'm doing the Dhammapada,
 now I'm going to not broadcast that live,
 because that will go up as its own YouTube video shortly.
 So what we have now is just answering questions.
 It's not going to be that long, because it's getting late
 here.
 I had two quizzes due today.
 Tomorrow we have our day of mindfulness, if people come.
 I think there's at least one man coming.
 And another thing we're doing is Monday, Monday and Friday,
 we're going to try and do a med mob, a McMaster, which
 means we're just going to go and meditate in public
 in the student center, which is like basically a cafeteria
 area.
 But it's more than that.
 It's like the everything.
 There's a Starbucks and Tim Hortons and restaurants
 and the transportation hub where you buy your tickets
 for public transportation.
 But it's a big area.
 And there are empty spaces where we could be meditating,
 partly just because we don't have a space.
 It's very difficult to book space at McMaster,
 and partly to remind people to be public, to be present.
 And to encourage that kind of thing and others.
 So that's happening.
 Do you have any questions?
 We have questions.
 How wide or narrow is the scope of the various experiences
 we note?
 For example, during walking, are we
 noting at the foot sole, foot, leg, or whole body walking?
 Or for breathing, is it one part or the whole abdomen?
 Or maybe the skin rubbing against the clothes?
 I think you might be overthinking it.
 I wouldn't worry too much.
 Stay with the foot when you walk, with the stomach when
 you sit.
 That might change.
 Obsessing over it is probably a sign of the state of mind.
 And then you should note that as well.
 You shouldn't be worried about the specifics.
 But to specify, we are focusing on the foot
 when we walk and the abdomen when we sit.
 Now, that experience might change.
 It might sometimes just be the sole of the foot.
 Sometimes just be the tip of the abdomen.
 But you shouldn't.
 The idea of purposefully trying for it to be one thing
 or another is counter-productive.
 [END PLAYBACK]
 Where can the full commentary for the Dhammapada be found?
 Burlingame.
 You get it from Harvard.
 Harvard-- well, you're talking about the translation,
 I assume.
 It's over 100 years old, I think.
 But the only English translation--
 you can actually get it on our website.
 We have it in our PDF archive.
 Someone sent it to me a while ago.
 So if someone wants to post that link,
 if someone knows where our PDF archive is.
 I can post that after the broadcast, which might be soon
 because that was the last question.
 OK.
 All right, then.
 Thank you, everyone.
 Have a good night.
 See you all tomorrow.
 Thank you, Bante.
 Thanks, Robert.
 Good night.
 [END PLAYBACK]
