1
00:00:00,000 --> 00:00:04,000
 It's easy to understand experience may continue after death

2
00:00:04,000 --> 00:00:04,000
.

3
00:00:04,000 --> 00:00:09,000
 However, how can we know where experience continues?

4
00:00:09,000 --> 00:00:14,000
 Such as in heaven or hell, or the animal realm, etc.

5
00:00:14,000 --> 00:00:22,790
 Well, it seems it's most like, I appreciate that you've

6
00:00:22,790 --> 00:00:24,000
 said it in this way.

7
00:00:24,000 --> 00:00:26,000
 I mean, it's nice to hear that there are actually people

8
00:00:26,000 --> 00:00:28,000
 who are getting it.

9
00:00:28,000 --> 00:00:34,000
 Getting this point that it doesn't seem unreasonable,

10
00:00:34,000 --> 00:00:38,340
 whether you've experienced past memories of past lives or

11
00:00:38,340 --> 00:00:40,000
 so on or not.

12
00:00:40,000 --> 00:00:45,180
 It seems to be quite in line with experience and reality to

13
00:00:45,180 --> 00:00:48,000
 consider that the mind continues on.

14
00:00:48,000 --> 00:00:54,000
 Knowing as we do that, that reality is based on experience.

15
00:00:54,000 --> 00:01:01,990
 So, it seems also quite logical and reasonable to suggest

16
00:01:01,990 --> 00:01:06,000
 that it's going to continue wherever,

17
00:01:06,000 --> 00:01:11,010
 in whatever state that most, in whatever state most

18
00:01:11,010 --> 00:01:16,000
 describes the nature of the mind.

19
00:01:16,000 --> 00:01:21,480
 So if the mind is full of greed, then one would expect to

20
00:01:21,480 --> 00:01:23,000
 have some consequences to that.

21
00:01:23,000 --> 00:01:27,000
 If the mind is full of anger, full of delusion, etc.

22
00:01:27,000 --> 00:01:30,460
 I have given talks before on the Buddhist idea of where

23
00:01:30,460 --> 00:01:35,000
 people go based on their acts and their deeds.

24
00:01:35,000 --> 00:01:39,210
 If a person, and so I can go over this as well, if a person

25
00:01:39,210 --> 00:01:41,000
 is full of anger,

26
00:01:41,000 --> 00:01:43,900
 this is what leads them to be born in hell because there is

27
00:01:43,900 --> 00:01:46,000
 a great amount of suffering in the mind.

28
00:01:46,000 --> 00:01:51,830
 So the mind becomes on fire and in some sort of hellish

29
00:01:51,830 --> 00:01:53,000
 state.

30
00:01:53,000 --> 00:01:58,000
 If the mind is full of greed, one is in a wanting state.

31
00:01:58,000 --> 00:02:01,000
 One ends up in a state of ultimate wanting as a ghost.

32
00:02:01,000 --> 00:02:04,840
 This is where one is most likely to arrive, a state of want

33
00:02:04,840 --> 00:02:06,000
, a state of need,

34
00:02:06,000 --> 00:02:09,000
 these ghosts that wailing and clinging to things,

35
00:02:09,000 --> 00:02:13,150
 or haunting a house because they're very much attached to

36
00:02:13,150 --> 00:02:16,000
 the house and to the people and so on.

37
00:02:16,000 --> 00:02:23,130
 If the mind is very much full of delusion, then one is most

38
00:02:23,130 --> 00:02:25,000
 likely to be born as an animal

39
00:02:25,000 --> 00:02:31,200
 because the mind is clouded and deluded and in a state of

40
00:02:31,200 --> 00:02:35,000
 only able to appreciate the very simple aspects of life,

41
00:02:35,000 --> 00:02:39,730
 like eating, sleeping and sexual intercourse, fighting and

42
00:02:39,730 --> 00:02:41,000
 so on.

43
00:02:41,000 --> 00:02:45,660
 So as a result, one is most likely to be born as what we

44
00:02:45,660 --> 00:02:47,000
 call animals,

45
00:02:47,000 --> 00:02:52,160
 these beings that are only able to appreciate the bare

46
00:02:52,160 --> 00:02:56,000
 basic simplest experiences of life.

47
00:02:56,000 --> 00:03:01,310
 Now if a person is able to clear their mind to the extent

48
00:03:01,310 --> 00:03:06,000
 that they're not saying or doing things based on,

49
00:03:06,000 --> 00:03:08,980
 if they're able to be free from greed, anger and delusion

50
00:03:08,980 --> 00:03:10,000
 to a certain level,

51
00:03:10,000 --> 00:03:12,610
 to the extent that they're not saying or doing things based

52
00:03:12,610 --> 00:03:13,000
 on that,

53
00:03:13,000 --> 00:03:16,770
 so their mind is pure to that extent, that is what allows

54
00:03:16,770 --> 00:03:20,000
 one to be born as a human being based on morality.

55
00:03:20,000 --> 00:03:24,860
 If they're able to keep their minds clear to the extent

56
00:03:24,860 --> 00:03:29,990
 that they're able to control their physical and verbal acts

57
00:03:29,990 --> 00:03:30,000
,

58
00:03:30,000 --> 00:03:32,790
 so they don't kill, they don't steal, they don't lie, they

59
00:03:32,790 --> 00:03:34,000
 don't cheat and so on.

60
00:03:34,000 --> 00:03:36,000
 They're born as humans.

61
00:03:36,000 --> 00:03:40,000
 Now in the human realm, it's further subdivided.

62
00:03:40,000 --> 00:03:54,020
 One is born rich or poor based on one's charity, this

63
00:03:54,020 --> 00:03:59,000
 desire to see, gain,

64
00:03:59,000 --> 00:04:03,850
 to have people get things, impresses on the mind this idea

65
00:04:03,850 --> 00:04:07,000
 of getting, this idea of receiving.

66
00:04:07,000 --> 00:04:10,000
 And so as a result, one is born rich.

67
00:04:10,000 --> 00:04:13,740
 People who are born wise because they have gone out and

68
00:04:13,740 --> 00:04:22,000
 asked questions and seeked out, sought out wise people.

69
00:04:22,000 --> 00:04:25,000
 People who are born stupid because they don't.

70
00:04:25,000 --> 00:04:27,780
 People who are born poor because they didn't give and so on

71
00:04:27,780 --> 00:04:28,000
.

72
00:04:28,000 --> 00:04:32,490
 People who are given to killing have a short life because

73
00:04:32,490 --> 00:04:35,000
 they're fixated on the idea of ending life.

74
00:04:35,000 --> 00:04:39,000
 And so as a result, when they're born as a human later on,

75
00:04:39,000 --> 00:04:41,610
 they will be born, if they're able to still be born as a

76
00:04:41,610 --> 00:04:42,000
 human,

77
00:04:42,000 --> 00:04:46,000
 they will be born with a short life.

78
00:04:46,000 --> 00:04:48,250
 If they're given to torturing or hurting other beings, then

79
00:04:48,250 --> 00:04:51,000
 when they're born as a human, if they're born as a human,

80
00:04:51,000 --> 00:04:57,000
 they'll be born sick with poor health.

81
00:04:57,000 --> 00:04:59,000
 How are we doing?

82
00:04:59,000 --> 00:05:03,820
 If they're born in high society, it's because they were

83
00:05:03,820 --> 00:05:06,000
 humble and unconceited

84
00:05:06,000 --> 00:05:10,800
 and they were given to respecting other people, respecting

85
00:05:10,800 --> 00:05:14,000
 mother, respecting father, respecting teachers.

86
00:05:14,000 --> 00:05:17,390
 So as a result, they have this idea of respect in their

87
00:05:17,390 --> 00:05:18,000
 mind.

88
00:05:18,000 --> 00:05:21,000
 So they're born in a high society or in a high position.

89
00:05:21,000 --> 00:05:25,350
 People who are born in a low position is because they are

90
00:05:25,350 --> 00:05:26,000
 not.

91
00:05:26,000 --> 00:05:30,000
 People who are born famous...

92
00:05:30,000 --> 00:05:33,140
 I can't remember all of them. There's altogether eight. I

93
00:05:33,140 --> 00:05:34,000
 got through six, didn't I?

94
00:05:34,000 --> 00:05:38,000
 I think I got through six. I got through a bunch. Five.

95
00:05:38,000 --> 00:05:43,700
 Anyway, it's in the Chula Kamo Ibangasuta, I think, or Mah

96
00:05:43,700 --> 00:05:46,000
au Kamo Ibangasuta.

97
00:05:46,000 --> 00:05:50,520
 There's two of them that should be read and should be memor

98
00:05:50,520 --> 00:05:54,000
ized in a way that I haven't, obviously.

99
00:05:54,000 --> 00:05:56,690
 But those are ways that you're born. That's in the human

100
00:05:56,690 --> 00:05:57,000
 realm.

101
00:05:57,000 --> 00:06:01,000
 Then to be born as an angel is because of great goodness,

102
00:06:01,000 --> 00:06:01,000
 Mahakasala.

103
00:06:01,000 --> 00:06:06,210
 If a person is given to doing great good deeds, profound

104
00:06:06,210 --> 00:06:08,000
 and exalted deeds,

105
00:06:08,000 --> 00:06:11,750
 like giving up all of their possessions, devoting their

106
00:06:11,750 --> 00:06:14,000
 life to helping others,

107
00:06:14,000 --> 00:06:17,870
 and to dedicating themselves to goodness, they become angel

108
00:06:17,870 --> 00:06:18,000
ic.

109
00:06:18,000 --> 00:06:20,000
 So they're born as an angel.

110
00:06:20,000 --> 00:06:24,360
 If a being is given over to intensive tranquility or

111
00:06:24,360 --> 00:06:26,000
 transcendental meditation,

112
00:06:26,000 --> 00:06:31,000
 then they're born as a God because their mind is so exalted

113
00:06:31,000 --> 00:06:32,000
 and universal

114
00:06:32,000 --> 00:06:36,760
 that they're born in an exalted and all-encompassing state

115
00:06:36,760 --> 00:06:38,000
 of Godhood.

116
00:06:38,000 --> 00:06:40,420
 If one practices to be passed into meditation, then there

117
00:06:40,420 --> 00:06:41,000
 is no clinging,

118
00:06:41,000 --> 00:06:45,000
 and so when one passes away, there is no rebirth.

119
00:06:45,000 --> 00:06:49,000
 That's where the mind goes.

120
00:06:51,000 --> 00:06:53,000
 Any more?

