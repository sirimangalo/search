1
00:00:00,000 --> 00:00:06,060
 Emma asked, she had a talk with a friend who's Muslim and

2
00:00:06,060 --> 00:00:07,320
 the friend told her that

3
00:00:07,320 --> 00:00:11,710
 Nirvana and Heaven are just the same, meaning that people

4
00:00:11,710 --> 00:00:12,720
 who do good things

5
00:00:12,720 --> 00:00:17,180
 will be rewarded. I think she's wrong but I was not able to

6
00:00:17,180 --> 00:00:18,320
 find good arguments.

7
00:00:18,320 --> 00:00:21,080
 Can you help?

8
00:00:25,080 --> 00:00:30,350
 I think Bantay should answer this one because one of us

9
00:00:30,350 --> 00:00:31,840
 will possibly say something

10
00:00:31,840 --> 00:00:38,440
 inaccurate about this. Well it's a very technical question.

11
00:00:38,440 --> 00:00:41,440
 You want to be perfectly orthodox.

12
00:00:41,440 --> 00:00:48,930
 Well, there are many ways of answering this question. The

13
00:00:48,930 --> 00:00:50,460
 Buddha did use the word

14
00:00:50,460 --> 00:00:57,620
 sugati to describe both of, or saga even, saga which means

15
00:00:57,620 --> 00:01:01,920
 heaven, but to describe

16
00:01:01,920 --> 00:01:06,980
 Nibbana as well, or it seems anyway, that he would include

17
00:01:06,980 --> 00:01:07,880
 it in a good

18
00:01:07,880 --> 00:01:12,960
 destination. So kind of lumping them together, the idea of

19
00:01:12,960 --> 00:01:14,760
 heaven, the idea of

20
00:01:14,760 --> 00:01:22,470
 Nibbana. But, so you know, I mean if you want to avoid an

21
00:01:22,470 --> 00:01:23,360
 argument you could just

22
00:01:23,360 --> 00:01:26,400
 answer it in that way. You could say to the Muslim friend,

23
00:01:26,400 --> 00:01:27,400
 "Well yes, you know, I

24
00:01:27,400 --> 00:01:30,440
 mean that's the point really. You do good, you get good.

25
00:01:30,440 --> 00:01:32,240
 You do evil, you get evil.

26
00:01:32,240 --> 00:01:37,870
 You get bad come to you." And why? Because this Muslim

27
00:01:37,870 --> 00:01:39,400
 friend is probably not going

28
00:01:39,400 --> 00:01:43,840
 to be convinced by you that, "Oh yes, there is no self and

29
00:01:43,840 --> 00:01:44,520
 there is no God

30
00:01:44,520 --> 00:01:47,560
 and there is no entity whatsoever and everything that

31
00:01:47,560 --> 00:01:49,160
 arises ceases and it's

32
00:01:49,160 --> 00:01:51,720
 impermanent suffering and non-self and therefore we should

33
00:01:51,720 --> 00:01:53,160
 let go of it and see

34
00:01:53,160 --> 00:01:55,760
 things as just experiential in the present moment, give up

35
00:01:55,760 --> 00:01:56,360
 past, give up

36
00:01:56,360 --> 00:02:00,880
 future, etc., etc. Probably not going to get very far with

37
00:02:00,880 --> 00:02:05,240
 them. So rather than

38
00:02:05,240 --> 00:02:08,530
 then take them all the way to Nibbana, an understanding, a

39
00:02:08,530 --> 00:02:09,440
 deep understanding of

40
00:02:09,440 --> 00:02:11,810
 the difference between Nibbana and heaven, well encourage

41
00:02:11,810 --> 00:02:12,920
 them in that, "Yes, do good

42
00:02:12,920 --> 00:02:17,040
 deeds." And as a result, encourage them in doing meditation

43
00:02:17,040 --> 00:02:17,480
 because

44
00:02:17,480 --> 00:02:21,910
 there's actually Buddhism only adds one aspect to the

45
00:02:21,910 --> 00:02:23,320
 teaching of other

46
00:02:23,320 --> 00:02:26,300
 religions and that's the purification of the mind. So the

47
00:02:26,300 --> 00:02:27,160
 Buddha taught that there

48
00:02:27,160 --> 00:02:32,090
 are three aspects to Buddhism and Sabapapasa, a Karanam,

49
00:02:32,090 --> 00:02:33,320
 not doing any evil,

50
00:02:33,320 --> 00:02:38,000
 kustla, supa, sampada, the becoming full of good or the

51
00:02:38,000 --> 00:02:39,640
 fulfillment of goodness,

52
00:02:39,640 --> 00:02:43,740
 of wholesomeness, satchitaparyodapanang, and the pur

53
00:02:43,740 --> 00:02:46,760
ification of the mind. So it's

54
00:02:46,760 --> 00:02:49,790
 really this purification of the mind that enters into a

55
00:02:49,790 --> 00:02:50,880
 different route

56
00:02:50,880 --> 00:02:55,700
 because you might even say that Nibbana is beyond good and

57
00:02:55,700 --> 00:02:57,040
 evil and the Buddha

58
00:02:57,040 --> 00:03:00,960
 said in the Dhammapada, he said, "An arahant has gone

59
00:03:00,960 --> 00:03:03,000
 beyond and given up

60
00:03:03,000 --> 00:03:11,400
 good and evil." This is why they fly free like a bird. But

61
00:03:11,400 --> 00:03:13,240
 the point

62
00:03:13,240 --> 00:03:21,400
 that I would recommend for the Muslim is to encourage them

63
00:03:21,400 --> 00:03:22,040
 in the good deed of

64
00:03:22,040 --> 00:03:26,480
 meditation as well. So to help them to see that if they

65
00:03:26,480 --> 00:03:27,640
 want to do good deeds,

66
00:03:27,640 --> 00:03:31,200
 it really has to come from a good mind because even Islam

67
00:03:31,200 --> 00:03:32,120
 agrees with such

68
00:03:32,120 --> 00:03:36,560
 things that if your mind is full of anger, then it's a bad

69
00:03:36,560 --> 00:03:37,320
 deed. If your mind

70
00:03:37,320 --> 00:03:40,630
 is full of greed, then it's a bad deed and so on. And they

71
00:03:40,630 --> 00:03:41,320
 do have this

72
00:03:41,320 --> 00:03:44,870
 rudimentary understanding of the importance of the mind. So

73
00:03:44,870 --> 00:03:46,160
 for example,

74
00:03:46,160 --> 00:03:49,010
 I've heard a teaching, I don't know, I'm assuming it's

75
00:03:49,010 --> 00:03:52,040
 Orthodox Islam, that there

76
00:03:52,040 --> 00:03:55,600
 was a story of a Christian and a Muslim and they were

77
00:03:55,600 --> 00:03:58,400
 fighting and the Islam

78
00:03:58,400 --> 00:04:01,280
 was winning and he was about to kill the Christian and then

79
00:04:01,280 --> 00:04:02,080
 the Christian spit

80
00:04:02,080 --> 00:04:05,570
 on him. And when the Christian spit on him, he got angry

81
00:04:05,570 --> 00:04:06,760
 and so he put his sword

82
00:04:06,760 --> 00:04:10,190
 away and he said, "I can't kill you now because I have

83
00:04:10,190 --> 00:04:14,000
 anger in my mind." So yeah,

84
00:04:14,000 --> 00:04:17,380
 if you're ever getting killed by a Muslim soldier, just

85
00:04:17,380 --> 00:04:20,200
 make him angry. I mean, it

86
00:04:20,200 --> 00:04:22,840
 shows how rude, I mean, because obviously from Buddha's

87
00:04:22,840 --> 00:04:23,360
 point of view, we

88
00:04:23,360 --> 00:04:25,860
 don't agree with that. You can't, you could never kill

89
00:04:25,860 --> 00:04:27,080
 without anger, without

90
00:04:27,080 --> 00:04:34,870
 some cruelty in the mind. But that's really the point. And

91
00:04:34,870 --> 00:04:35,560
 so helping them to

92
00:04:35,560 --> 00:04:38,690
 say, "We're not asking you to become Buddhist, we're just

93
00:04:38,690 --> 00:04:39,240
 asking you to,

94
00:04:39,240 --> 00:04:42,940
 we're just encouraging you to become a good person." So

95
00:04:42,940 --> 00:04:44,240
 really, if you want to be

96
00:04:44,240 --> 00:04:53,410
 full of good deeds and be avoiding evil deeds, the

97
00:04:53,410 --> 00:04:54,280
 performance of these

98
00:04:54,280 --> 00:04:57,370
 deeds and the abstention from certain deeds isn't really

99
00:04:57,370 --> 00:04:58,600
 enough because the

100
00:04:58,600 --> 00:05:01,300
 only way to really perform good deeds and to abstain from

101
00:05:01,300 --> 00:05:02,000
 bad deeds is to

102
00:05:02,000 --> 00:05:08,000
 purify your mind, to have a good source of your deeds. And

103
00:05:08,000 --> 00:05:08,800
 there's a way to avoid

104
00:05:08,800 --> 00:05:12,130
 argument and a way to encourage people in meditation

105
00:05:12,130 --> 00:05:14,320
 because ultimately, the

106
00:05:14,320 --> 00:05:18,680
 only way to overcome people's views is through a meditation

107
00:05:18,680 --> 00:05:19,240
 and

108
00:05:19,240 --> 00:05:22,430
 ultimately that's all they are, is there's some belief,

109
00:05:22,430 --> 00:05:23,440
 some view that is

110
00:05:23,440 --> 00:05:28,490
 really this, it's part of this whole conceptual layer that

111
00:05:28,490 --> 00:05:29,680
 is such a thin

112
00:05:29,680 --> 00:05:34,630
 layer of our experience and of our minds. So you'll find

113
00:05:34,630 --> 00:05:36,000
 that Christians,

114
00:05:36,000 --> 00:05:40,260
 Muslims, Jews, people from any religion or any walk of life

115
00:05:40,260 --> 00:05:41,800
, within a week of

116
00:05:41,800 --> 00:05:48,960
 meditating, they can discard any number of views. It's in

117
00:05:48,960 --> 00:05:50,040
 fact much

118
00:05:50,040 --> 00:05:53,470
 less the views and more the addictions and defilements that

119
00:05:53,470 --> 00:05:55,320
 tend to persist. If

120
00:05:55,320 --> 00:06:00,840
 you just give them straight repass and insight

121
00:06:00,840 --> 00:06:05,090
 meditation in line with the four foundations of mindfulness

122
00:06:05,090 --> 00:06:05,640
, if they

123
00:06:05,640 --> 00:06:09,240
 practice it, in about a week they'll start to ease up on

124
00:06:09,240 --> 00:06:10,120
 this sort of thing

125
00:06:10,120 --> 00:06:13,490
 and not really care unless you make it a point of

126
00:06:13,490 --> 00:06:15,000
 controversy with them. You say,

127
00:06:15,000 --> 00:06:17,450
 "No, if you want to practice meditation you have to give up

128
00:06:17,450 --> 00:06:18,180
 God. If you want to

129
00:06:18,180 --> 00:06:20,320
 practice meditation you have to give up heaven." You have

130
00:06:20,320 --> 00:06:21,160
 to understand the

131
00:06:21,160 --> 00:06:24,410
 difference between nibbana. If you start requiring these

132
00:06:24,410 --> 00:06:25,640
 things and arguing with

133
00:06:25,640 --> 00:06:29,880
 them about them and making it clear to them that there's

134
00:06:29,880 --> 00:06:30,680
 something

135
00:06:30,680 --> 00:06:33,090
 important here that you're getting wrong, then they're

136
00:06:33,090 --> 00:06:34,320
 going to just cling harder

137
00:06:34,320 --> 00:06:38,730
 and harder to them and then it could be a lifetime or a

138
00:06:38,730 --> 00:06:39,960
 million, you know,

139
00:06:39,960 --> 00:06:44,000
 thousand, thousands of lifetimes and they'll never get it.

140
00:06:44,000 --> 00:06:44,840
 They'll never stop

141
00:06:44,840 --> 00:06:50,430
 clinging because there's the conflict. So that's the first

142
00:06:50,430 --> 00:06:51,200
 way I would answer

143
00:06:51,200 --> 00:06:55,480
 this question is to avoid that sort of conflict. There was

144
00:06:55,480 --> 00:06:55,680
 even, there was a

145
00:06:55,680 --> 00:07:01,520
 Iranian man, I don't know if he's here tonight, but he was

146
00:07:01,520 --> 00:07:02,360
 asking my

147
00:07:02,360 --> 00:07:05,590
 teacher about this because he had to go back to Iran and he

148
00:07:05,590 --> 00:07:08,240
 was afraid that it

149
00:07:08,240 --> 00:07:11,660
 would be very difficult to teach meditation there because

150
00:07:11,660 --> 00:07:12,560
 the Iranian

151
00:07:12,560 --> 00:07:18,800
 people are, or because the government will even put him in

152
00:07:18,800 --> 00:07:19,440
 jail or even

153
00:07:19,440 --> 00:07:22,870
 execute him for converting to another religion and for

154
00:07:22,870 --> 00:07:23,480
 trying to convert

155
00:07:23,480 --> 00:07:29,620
 other people to religion that are not believing in God. And

156
00:07:29,620 --> 00:07:30,680
 my teacher said to

157
00:07:30,680 --> 00:07:34,210
 him, "Well, let's just say it's the teaching of the Lord."

158
00:07:34,210 --> 00:07:35,040
 This is just the

159
00:07:35,040 --> 00:07:39,480
 teaching of the Lord. He's using Thai language to explain

160
00:07:39,480 --> 00:07:41,040
 it and so the word

161
00:07:41,040 --> 00:07:45,800
 basically Lord. And so he said because it's the law of

162
00:07:45,800 --> 00:07:46,680
 nature, it's

163
00:07:46,680 --> 00:07:51,300
 reality, it's the Lord. Or you could say it's because it's

164
00:07:51,300 --> 00:07:52,320
 the Lord Buddha.

165
00:07:52,320 --> 00:07:54,820
 But basically he was saying, "You know, it comes from the

166
00:07:54,820 --> 00:07:56,240
 universe, it comes from the

167
00:07:56,240 --> 00:08:01,720
 Creator." So the point being to just, you know, encourage

168
00:08:01,720 --> 00:08:02,720
 people in meditation.

169
00:08:02,720 --> 00:08:09,540
 Don't worry about who it belongs to or what are the words

170
00:08:09,540 --> 00:08:10,640
 that you use and so

171
00:08:10,640 --> 00:08:15,150
 on. But if you're interested in the difference between

172
00:08:15,150 --> 00:08:17,680
 heaven and nirvana,

173
00:08:17,680 --> 00:08:24,160
 it's that heaven is a myth, well basically heaven is a myth

174
00:08:24,160 --> 00:08:25,160
 and nirvana is

175
00:08:25,160 --> 00:08:30,240
 reality. Why? Because heaven, the concept of heaven is

176
00:08:30,240 --> 00:08:34,560
 intrinsically based on a

177
00:08:34,560 --> 00:08:37,840
 risen phenomenon. The idea of heaven is that there is

178
00:08:37,840 --> 00:08:38,480
 something that has a

179
00:08:38,480 --> 00:08:48,760
 risen. And so it's describing a series of experiences with

180
00:08:48,760 --> 00:08:49,680
 a term, with a

181
00:08:49,680 --> 00:08:54,880
 conceptual name, namapanyati. So you're in heaven and you

182
00:08:54,880 --> 00:08:55,960
're experiencing the

183
00:08:55,960 --> 00:08:59,410
 glory of God and so on. And you're praising God for

184
00:08:59,410 --> 00:09:00,640
 eternity and there's

185
00:09:00,640 --> 00:09:05,070
 all the virgins or whatever. I mean, I don't know, nymphs

186
00:09:05,070 --> 00:09:06,320
 or whatever the truth is.

187
00:09:06,320 --> 00:09:09,020
 Because there's some controversy about the use of the word

188
00:09:09,020 --> 00:09:10,000
 virgin, it might just

189
00:09:10,000 --> 00:09:16,440
 mean women, young women or something. But at any rate, up

190
00:09:16,440 --> 00:09:17,400
 in heaven the idea is

191
00:09:17,400 --> 00:09:20,580
 that there is an experience. And those experiences are

192
00:09:20,580 --> 00:09:21,760
 impermanent suffering

193
00:09:21,760 --> 00:09:24,850
 and non-self. That's reality and that's the reality that

194
00:09:24,850 --> 00:09:26,200
 ordinarily we can't see.

195
00:09:26,200 --> 00:09:29,430
 So calling it heaven, calling it whatever you like is

196
00:09:29,430 --> 00:09:30,580
 saying nothing. It's

197
00:09:30,580 --> 00:09:34,830
 still changing. Every moment is changing and therefore it

198
00:09:34,830 --> 00:09:35,680
 can't be stable. There

199
00:09:35,680 --> 00:09:43,880
 is no force that can keep it static because it's changing.

200
00:09:43,880 --> 00:09:47,480
 Nibana on the other hand is free from arising. There is

201
00:09:47,480 --> 00:09:48,520
 none of that experience

202
00:09:48,520 --> 00:09:51,820
 arising. There is no seeing, no hearing, no smelling, no

203
00:09:51,820 --> 00:09:52,760
 tasting, no feeling,

204
00:09:52,760 --> 00:09:57,020
 thinking. Everything that would arise has ceased and has

205
00:09:57,020 --> 00:09:57,680
 ceased with that

206
00:09:57,680 --> 00:10:01,320
 remainder. It's like the ultimate rest, the ultimate

207
00:10:01,320 --> 00:10:02,320
 cessation, the ultimate

208
00:10:02,320 --> 00:10:09,950
 freedom from suffering. It's the only possible alternative

209
00:10:09,950 --> 00:10:10,720
 to arisen

210
00:10:10,720 --> 00:10:14,310
 phenomena. It's not some special state of arisen phenomena

211
00:10:14,310 --> 00:10:15,880
 or arisen experience.

212
00:10:15,880 --> 00:10:19,690
 It's the only exception. Everything else that you could

213
00:10:19,690 --> 00:10:21,320
 possibly ever imagine

214
00:10:21,320 --> 00:10:26,120
 that could possibly arise is subject to cessation.

215
00:10:26,120 --> 00:10:31,770
 Yankinche samudayadam mang sabantang nirod damang. Whatever

216
00:10:31,770 --> 00:10:32,960
 reality, whatever

217
00:10:32,960 --> 00:10:38,580
 thing really and truly arises, that thing must really and

218
00:10:38,580 --> 00:10:40,360
 truly cease. And from

219
00:10:40,360 --> 00:10:44,760
 one moment to the next. So the only way out of that, the

220
00:10:44,760 --> 00:10:45,720
 only exception that could

221
00:10:45,720 --> 00:10:51,830
 ever exist in reality logically and experientially is

222
00:10:51,830 --> 00:10:53,880
 cessation, this state

223
00:10:53,880 --> 00:10:57,500
 of cessation. Now if you don't want to attain that, because

224
00:10:57,500 --> 00:10:58,240
 there was actually

225
00:10:58,240 --> 00:11:03,000
 this question was asked on our Q&A forum yesterday or

226
00:11:03,000 --> 00:11:03,600
 something and I was

227
00:11:03,600 --> 00:11:07,540
 just answering it. And the person said, "Well there's

228
00:11:07,540 --> 00:11:08,680
 someone who said they're

229
00:11:08,680 --> 00:11:11,430
 so afraid of this idea of cessation because what will

230
00:11:11,430 --> 00:11:12,400
 happen to their self?"

231
00:11:12,400 --> 00:11:19,760
 It's the cessation of self, it's annihilation of self. And

232
00:11:19,760 --> 00:11:20,880
 that's a

233
00:11:20,880 --> 00:11:23,880
 whole other issue about the non-existence of self and so on

234
00:11:23,880 --> 00:11:24,320
. If you

235
00:11:24,320 --> 00:11:28,320
 want to go and read that question it's quite interesting.

236
00:11:28,320 --> 00:11:30,400
 So it's one

237
00:11:30,400 --> 00:11:33,050
 thing to say that you don't want to attain it and of course

238
00:11:33,050 --> 00:11:33,520
 there's

239
00:11:33,520 --> 00:11:37,240
 various issues with that and how to help someone to see

240
00:11:37,240 --> 00:11:38,440
 that there's nothing

241
00:11:38,440 --> 00:11:40,720
 being annihilated, there was nothing existing in the first

242
00:11:40,720 --> 00:11:41,600
 place. It's just

243
00:11:41,600 --> 00:11:46,680
 those things that arise that have ceased. They don't arise

244
00:11:46,680 --> 00:11:47,160
 again and

245
00:11:47,160 --> 00:12:01,920
 it doesn't occur any arising. But just to see that this is

246
00:12:01,920 --> 00:12:02,200
 the only

247
00:12:02,200 --> 00:12:11,280
 alternative, this cessation of suffering. And so whether

248
00:12:11,280 --> 00:12:12,280
 you want to realize it

249
00:12:12,280 --> 00:12:15,440
 or not, it is the only alternative, it's the only thing

250
00:12:15,440 --> 00:12:16,760
 that could possibly exist

251
00:12:16,760 --> 00:12:21,440
 outside of that. And the great thing about it is that it

252
00:12:21,440 --> 00:12:22,480
 actually does lean

253
00:12:22,480 --> 00:12:26,410
 to peace, happiness and freedom from suffering. And it is

254
00:12:26,410 --> 00:12:28,120
 actually far

255
00:12:28,120 --> 00:12:31,690
 preferable to any other experience. It's in a whole other

256
00:12:31,690 --> 00:12:33,400
 level of peace and

257
00:12:33,400 --> 00:12:39,760
 happiness. And because of its actual nature of providing

258
00:12:39,760 --> 00:12:41,360
 freedom from this

259
00:12:41,360 --> 00:12:46,880
 incessant arising and seizing, it actually is true peace

260
00:12:46,880 --> 00:12:48,240
 and happiness. And a person

261
00:12:48,240 --> 00:12:52,630
 realizes it has no doubt. That's why a sotapana is free

262
00:12:52,630 --> 00:12:54,280
 from doubt because they

263
00:12:54,280 --> 00:12:56,640
 have none of this doubt about whether it's useful or not

264
00:12:56,640 --> 00:12:58,220
 useful. They've come

265
00:12:58,220 --> 00:13:02,000
 to realize it and so they understand what is of true

266
00:13:02,000 --> 00:13:03,240
 benefit and that's the

267
00:13:03,240 --> 00:13:08,050
 cessation of suffering. So help in some way that answers

268
00:13:08,050 --> 00:13:10,720
 your question.

