1
00:00:00,000 --> 00:00:03,000
 But here's one about meditation particularly.

2
00:00:03,000 --> 00:00:07,070
 I know the difference between vipassana and samatha, but

3
00:00:07,070 --> 00:00:09,000
 not in the following situation.

4
00:00:09,000 --> 00:00:12,460
 Sometimes in vipassana you can stay with your breath for a

5
00:00:12,460 --> 00:00:13,000
 while.

6
00:00:13,000 --> 00:00:16,060
 Are you practicing samatha then? Because you're staying

7
00:00:16,060 --> 00:00:17,000
 with one object then.

8
00:00:17,000 --> 00:00:19,000
 You're still aware of the rising and falling.

9
00:00:19,000 --> 00:00:23,280
 I understand that when you are too deep in breath and you

10
00:00:23,280 --> 00:00:24,000
're in absorption,

11
00:00:24,000 --> 00:00:27,000
 then you are practicing samatha.

12
00:00:27,000 --> 00:00:30,280
 No, this isn't the difference between what we call samatha

13
00:00:30,280 --> 00:00:32,000
 meditation and vipassana meditation

14
00:00:32,000 --> 00:00:35,000
 in the sense that they are two different meditations.

15
00:00:35,000 --> 00:00:38,730
 It's possible to say that at that moment you have samatha

16
00:00:38,730 --> 00:00:40,000
 because your mind is tranquil.

17
00:00:40,000 --> 00:00:43,030
 At that moment your mind is not distracted, so you can say,

18
00:00:43,030 --> 00:00:45,000
 "Well, that's samatha."

19
00:00:45,000 --> 00:00:48,000
 But based on the visuddhi maga, based on the commentaries,

20
00:00:48,000 --> 00:00:50,000
 based on even I think the patisambhita maga,

21
00:00:50,000 --> 00:00:53,000
 samatha is when you're focusing on a concept.

22
00:00:53,000 --> 00:00:55,520
 It doesn't matter how long you're with the object, it

23
00:00:55,520 --> 00:00:57,000
 matters what the object is.

24
00:00:57,000 --> 00:01:00,800
 If the object is a concept and you're creating mindfulness

25
00:01:00,800 --> 00:01:02,000
 about the object,

26
00:01:02,000 --> 00:01:05,300
 you're clearly seeing the object as being what it is, like

27
00:01:05,300 --> 00:01:11,000
 the Buddha or color or even the breath,

28
00:01:11,000 --> 00:01:14,000
 seeing the breath as a concept going in and going out

29
00:01:14,000 --> 00:01:17,000
 because we know that's not the actual experience of it,

30
00:01:17,000 --> 00:01:19,000
 then that's samatha.

31
00:01:19,000 --> 00:01:21,000
 So if you're still aware of the rising and the falling,

32
00:01:21,000 --> 00:01:24,500
 at the moment when you're aware of the rising or the

33
00:01:24,500 --> 00:01:25,000
 falling,

34
00:01:25,000 --> 00:01:28,000
 you have the potential to develop vipassana meditation.

35
00:01:28,000 --> 00:01:30,000
 It doesn't mean that you are practicing vipassana,

36
00:01:30,000 --> 00:01:34,080
 but if you see it clearly as rising or you see it clearly

37
00:01:34,080 --> 00:01:35,000
 as falling,

38
00:01:35,000 --> 00:01:38,780
 you're cultivating the four satipatanas, and the four satip

39
00:01:38,780 --> 00:01:41,000
atanas allow the arising of inside,

40
00:01:41,000 --> 00:01:44,300
 the arising of vipassana, so that's considered vipassana

41
00:01:44,300 --> 00:01:45,000
 meditation.

42
00:01:45,000 --> 00:01:48,440
 If you're focusing on something else, it's not outside of

43
00:01:48,440 --> 00:01:50,000
 the five aggregates,

44
00:01:50,000 --> 00:01:53,000
 so a concept, and it's samatha.

45
00:01:53,000 --> 00:02:00,000
 Are you deriving this from like the Vysuddhi Maga?

46
00:02:00,000 --> 00:02:03,000
 Yes, and the commentary.

47
00:02:03,000 --> 00:02:04,000
 Okay.

