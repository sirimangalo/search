1
00:00:00,000 --> 00:00:01,840
 Solitude

2
00:00:01,840 --> 00:00:03,240
 Question

3
00:00:03,240 --> 00:00:06,440
 Do some enlightened beings retreat into solitude?

4
00:00:06,440 --> 00:00:09,570
 The Buddha did not do that, but do you think you would

5
00:00:09,570 --> 00:00:11,880
 consider that decision respectable?

6
00:00:11,880 --> 00:00:15,160
 Answer yes, absolutely

7
00:00:15,160 --> 00:00:19,590
 The Buddha himself retreated into solitude more than once

8
00:00:19,590 --> 00:00:22,480
 if the Buddha hadn't been asked to teach

9
00:00:22,480 --> 00:00:25,100
 It is likely that he would have just retreated into

10
00:00:25,100 --> 00:00:27,520
 solitude for the remainder of his life

11
00:00:29,160 --> 00:00:31,160
 Solitude is completely respectable

12
00:00:31,160 --> 00:00:35,320
 There are many stories of arahants who lived in solitude

13
00:00:35,320 --> 00:00:38,970
 The Buddha was clear that there are many different ways to

14
00:00:38,970 --> 00:00:40,200
 live as a Buddhist

15
00:00:40,200 --> 00:00:44,160
 You do not have to live in solitude, but it is not wrong

16
00:00:44,160 --> 00:00:47,230
 The most important thing is that you practice mental

17
00:00:47,230 --> 00:00:50,880
 solitude keeping yourself secluded from mental impurity

