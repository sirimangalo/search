 Good evening everyone.
 Broadcasting Live, May 6th.
 Today's quote is somewhat curious.
 And not quite how, well, Bhikkhobodi's translation of
 course is preferred.
 But the word isn't quite false, it's 'wipati'.
 'Wipati' is failure, failings as Bhikkhobodi translates it.
 So 'wipati' and 'sampati'. 'Wipati' and 'sampati'.
 'Wipati' means lacking. 'Sampati' means attaining our
 failings and our attainment.
 And so the Buddha says, and it's kind of, I wouldn't put
 too much on this,
 because the context of the quote is that Devadatta just
 left.
 Devadatta of course was the Buddha's cousin who, well, he
 came to the Buddha and he said,
 'Oh, Venerable Surya, you're getting old, so why don't you
 pass the Sangha on to me,
 'Aali the Sangha?'
 And the Buddha said to him, 'I wouldn't even hand it over
 to Sariputta and Moggallana,
 let alone some insignificant little person like you.'
 He said something really mean to him actually, kind of.
 Because Devadatta is out of control.
 And so there's only one way it's going to end.
 In the end, the Buddha has to be firm.
 It's rare because he's not usually like this, but with Dev
adatta he had to be firm at all times.
 And so Devadatta got very upset and he tried to scheme up a
 way to take control of the Sangha.
 And so he came to the Buddha later and asked him to instate
 five rules for monks,
 that they should always live in the forest, that they
 should only eat vegetarian food,
 and things that actually were contentious and seemed
 reasonable.
 But the Buddha couldn't enforce and he stopped short of
 forcing monks,
 even though he praised living in the forest and even
 obviously eating meat is problematic
 and there are types of meat that absolutely shouldn't be
 eaten.
 So he refused and Devadatta used that as grounds to start
 his schism.
 He got monks on his side saying, 'Hey, the Buddha clearly
 doesn't know what he's doing.'
 But the monks he got on his side were actually new monks.
 They're monks who didn't know better.
 They hadn't studied their mind. They hadn't really learned
 the intricacies of monastic life.
 And so they thought Devadatta was more serious than the
 Buddha.
 The Buddha seemed to be actually kind of lax and indulgent.
 And so they went with Devadatta.
 But later the Buddha had Sariputta and Mughalana go to see
 them.
 And it's kind of funny how it turned out.
 This is after this sutta is actually before that.
 But later on Sariputta and Mughalana went to see Devadatta.
 And Devadatta said, 'Oh, look, here come the Buddha's two
 chief disciples.
 They're joining us as well.'
 And Sariputta and Mughalana didn't say anything.
 And so Devadatta said, 'Sariputta and Mughalana, you teach
 the monks. Now I'm tired.'
 And so he pretended to be like the Buddha where the Buddha
 would say, you know, 'You teach.'
 And the Buddha would lie down and listen mindfully.
 But instead Devadatta lay down and fell asleep.
 And so Sariputta and Mughalana taught the monks the truth
 and converted them all.
 And they all went back to see the Buddha.
 They all went back to be with the Buddha once they realized
 what was right and what was wrong.
 And Devadatta's sort of second in command kicked Devadatta
 in the chest to wake him up.
 And was very angry and said, 'Look, look at what's happened
. I told you to be careful of these two.
 And you didn't listen.'
 But he kicked him in the chest and it wounded him.
 And that ended up, I think, being fatal. That was what
 killed him in the end.
 He got sick after that, coughed up in blood.
 But so this quote is somewhere during this whole fiasco
 when Devadatta has just left.
 And so the Buddha kind of to introduce the topic, he says
 it's appropriate from time to time,
 'gaade na kalang,' to talk about one's own failings.
 Not talk about but to keep that to consider.
 It is good that they be considered, reflected upon.
 And it's good to reflect upon the failings of others.
 And you have to take this with a grain of salt.
 It's important not to get obsessed with the faults of
 others.
 Because the Buddha has said, of course, 'na praisang wilom
ani na praisang katagatango.'
 Don't worry about.
 One should not become obsessed with other people's faults.
 So here he's cautious. He says, 'gaade na kalang,' from
 time to time.
 It's useful, right?
 Because if you look at Devadatta as an example and if you
 talk about him and think about him,
 you can say, 'That's not how I want to be.
 That's something that leads to stress and suffering.'
 And he says it's good from time to time to think of your
 own attainments and attainments of others.
 But then he gets on to the meat of the sutta, which is
 actually more interesting.
 He says, well, he says, 'Devadatta's going to hell,' right?
 That's what he says. Wait, where is it?
 There are these eight by eight, let me read the English
 because I'm...
 'Because he was overcome and obsessed by eight bad
 conditions,
 Devadatta is bound for the plane of misery, bound for hell,
 and he will remain there for a neon.'
 So the point is to introduce... he's now going to talk
 about Devadatta's failings, what Devadatta did wrong.
 And it's interesting because these eight things are a list
 of things that we have to be not obsessed with.
 We cannot let overcome us.
 The first is gain.
 Some people are obsessed with gain, wanting money, wanting
 possessions, wanting to get things.
 And when you get obsessed with that, it overpowers you, it
 inflames the mind.
 Overcome by loss when you don't get what you want, when you
 lose what you like.
 Fame, number three, is fame.
 And people are obsessed with being famous.
 People who post YouTube videos, they can be obsessed with
 people's...
 how many subscribers they have or so.
 We go on Facebook, we're obsessed by how many likes we get.
 That kind of thing. By disrepute the opposite of fame.
 We're obsessed when people have a bad image of you or when
 you become infamous,
 or when no one knows who you are.
 By honor, I'm not sure what the context here of honor is.
 Look at this.
 Sakara, right?
 Yaśa, ayaśa, sakara.
 Sakara is similar to fame, but it's when people present you
 with things,
 like respect and gifts and praise and honor.
 Yaśa, that's where he gets the word honor.
 Sakara means doing rightly.
 And people are obsessed with other people,
 praising them and honoring them and...
 rewarding them, that kind of thing.
 Number seven, by evil desires.
 He was obsessed by evil desires.
 David Datta had the most evil desires.
 He wanted to kill the Buddha, he wanted to become...
 He ended up trying to kill the Buddha.
 He so wanted to be head of the monks, head of the Sangha,
 so badly,
 that he did evil things.
 He was jealous of the Buddha.
 He was covetous of the Buddha's position, that kind of
 thing.
 And number eight, bad friendship.
 So David Datta...
 David Datta was actually the bad friendship himself.
 He ended up making friends with King Bimbisara's son,
 who ended up killing his father.
 And Jatasattu was actually able to kill his father.
 But association with bad friends means, if you're on a bad
 path,
 it's not a good idea to find people who have your own
 faults, right?
 We say birds of a feather flock together, well that's not
 always wise.
 If you have faults, then maybe it's better to associate
 with people
 who don't have those faults.
 They can teach you something, they can remind you,
 they can challenge you, right?
 But we tend the other way, right?
 We tend to flock towards birds with the same faults.
 We tend to flock towards...
 If we are an angry sort of person,
 we tend to find solace in other people who are angry.
 We don't seem so out of place.
 If we're around people who are not angry, we feel guilty
 all the time,
 we feel bad.
 So it's actually easier to be around angry people,
 which is of course the most dangerous thing,
 because you're just going to become more of an angry person
.
 Greed, if you're a greedy person,
 shouldn't be around other addicts or other greedy people.
 If you're deluded, if you're full of arrogance and conceit
 and so on,
 you should be surrounded by humble people,
 you should go to seek out humble people to teach you
 humility.
 And then he says, "It is good for a bhikkhu to overcome
 these eight things
 whenever they arise."
 Overcome, see what the word he uses for overcome.
 "Ambi buya," and the commentary talks about.
 "Ambi buya."
 This is an interesting word.
 To...
 I guess it just means overcome, to be above.
 The commentary says, "Ambi bhavitva, maditva."
 To subjugate, to conquer.
 One should conquer these.
 Conquer these things. These are our enemies.
 Devadatta wasn't the enemy, the Buddha wasn't his enemy.
 His enemy was these eight things.
 It was his obsession for these eight things, I shouldn't
 say.
 Because of course, gain is not a problem,
 loss is not a problem.
 It's not these things that are the problem,
 except for evil wishes.
 But the real problem is our obsession.
 And this goes actually with even our emotions.
 If you're an angry person, that's not the biggest problem.
 The biggest problem is your obsession with it,
 when you let it consume you, when you have desire.
 That's not the biggest problem.
 The biggest problem is you don't...
 You fail to address it, you fail to rise above it, to
 overcome it.
 When you have drowsiness, when you have distraction,
 when you have doubt, you have doubt about the Buddha's
 teaching
 or the practice.
 Doubt isn't the biggest problem that you let it get to you.
 Even these things, they're bad.
 They really become bad when you follow them,
 when you chase them, when you make much of them.
 And so with these eight things, it's really the obsession
 with them.
 But Devadatta wasn't able to see them clearly.
 Dasmatiham...
 No, and then he says, "Why should you not let them consume
 you?"
 And he says, "Because there will be a 'Wigata Parilaha'
 which is a fever burning.
 Those taints, distressful and feverish that might arise
 in one who has not overcome gain and so on,
 do not occur in one who has overcome it."
 So your mind doesn't become inflamed, distressed,
 stressed, fatigued, overwhelmed.
 The Asa is outpouring the emotions and the defilement,
 the stresses that arise when you've overcome them.
 And you've come to see them for what they are
 and discard them as unproductive.
 For that reason you should train yourself.
 Dasmatiham tikure...
 Aivang sikidabang, it should be trained as
 we will dwell having overcome.
 Abhibu ya abhibu ya wihari saam.
 We will dwell having overcome gain and loss
 and pain and infamy and honor and dishonor
 and evil desires
 and evil friends.
 Pa peet chitang pa pam mitatang, evil friendship.
 So a good list, good list.
 It's almost the eight lokiyadamma
 that the last two would be happiness and suffering.
 Dukkha and sukha, instead of pa peet chitang pa pam,
 pa pam mitatang.
 Anyway, there's our dhamma for tonight.
 Don't be like Devadatta.
 Don't let these things overwhelm you.
 Any questions?
 Can you stay for a second? You can go.
 Okay, so we got a couple of questions first.
 I want to say here's...
 Have you met these people?
 Have I showed you?
 This is Michael.
 Michael's living here now.
 He's the steward here for a while.
 Just finished his advanced course,
 so he's done both courses.
 So we'll have to do an interview with Michael
 to see what he thinks of the practice.
 See how it helped him, if it helped him.
 And Sunday we're going to Mississauga together.
 And then...
 I have to go to New York.
 We're going to drive to New York.
 You're going to drive to New York?
 Would you be able to drive to New York?
 How is it about crossing the border back and forth?
 There's no problem, is there?
 What did they say when you came in?
 Did they ask you?
 They did make trouble for you?
 I wouldn't say trouble,
 but they had to wait for about an hour.
 An hour?
 I mean, you know, I mentioned your name,
 and then I was thinking about a story.
 Yeah, they have many questions about that.
 Like suspicious or just curious?
 A little about...
 Oh, we should talk about it.
 You probably should give people letters in the future.
 Letters of recommendation, like letters of invitation.
 How weird.
 Yeah, they did ask for that.
 Yeah.
 Yeah, I think we've had this issue before.
 I just sent a letter of invitation to someone in the
 Ukraine
 who wants to come and meditate, so hopefully that works for
 him.
 So there's this monk I've been talking about.
 He wants to go visit Bikubodi, so he wants me to go with
 him.
 And he was going to drive her, and I said, "Well, I've got
 a driver.
 We've got a Prius, right?
 It's a good mileage, so probably he'll pay for gas."
 But, yeah, so that's a week and a half away, the 17th.
 Sunday we're going to Mississauga for Wesaka.
 Okay.
 So questions.
 Larry says, "During sitting meditation,
 can more than one hindrance be recognized concurrently?"
 Yeah, you won't have them both at once.
 You can't have a virgin end craving concurrently.
 I mean, it feels like that because over time you say,
 "Wow, I was both angry and greedy,"
 but you can't have them both at the exact same moment.
 So concurrently in the broad sense, sure, but in the
 absolute sense, no.
 In the absolute sense, there's one at a time,
 so whichever one is clear in that moment, just focus on it.
 It doesn't really matter.
 I wouldn't worry about which is which, just whichever one
 is clear
 in whatever moment, focus on it.
 You don't have to catch them all.
 But again, you can't have all of them at once,
 except for the fact that aversion to something is kind of
 like desire
 for it not to be, right?
 So you could argue that it's actually in that same moment,
 it's the same thing.
 You're saying the same thing.
 You dislike something means you want it to be gone.
 It's not quite craving, or it is, some people call that
 "vibhavatan," desire for non-existence.
 "Sabhay sankhara dukkha," does that hold true only for unen
lightened beings?
 No.
 No, it does not.
 They are dukkha.
 Dukkha is their characteristic, means they are unsatisfying
.
 It means they are not good.
 It's the intrinsic characteristic.
 It doesn't mean that they are painful,
 or when they arise cause one mental suffering,
 or even physical suffering.
 That's not what dukkha means here.
 Dukkha means unsatisfying or unable to satisfy,
 unable to bring happiness, not happiness.
 Remember these three characteristics are in opposition
 of what we think things are.
 We think things are stable, we think things are satisfying
 or able to satisfy us or productive of true happiness,
 and we think things are ours or me or mine.
 We can control them.
 So this is just realizing that they are not that,
 that they are not nitya, that they are not sukha, that they
 are not ata.
 That's what these three things mean.
 They are actually anichas dukkha anata.
 Can you go back to using the red room?
 Wow, there's someone who actually likes that red.
 No, I can't go back to using the red room.
 There's some disliking, disliking.
 Sadhu means good.
 Sadhu just means good.
 Well, sadhu in Hinduism, sadhu is a, or in India,
 sadhu means a, it's a word also for a holy person,
 like a monk or a recluse, so they call us sadhus.
 They would call me a sadhu.
 But it literally means, at least in Pali, I don't know in
 Sanskrit,
 but in Pali it's just used to mean good.
 Sadhu, it's an exclamation, it means it is good.
 When something is good you say sadhu, that thing is sadhu.
 In Pali you would say, dang sadhu, it is good.
 You wouldn't say it like that, but then like sadhu,
 it was used in the sutta, right?
 Sadhu bhikkhu, bhikkhu, upanang, labang, abhibuya, abhibuya
, bhihareya.
 It is good, or it would be good if one were to dwell again.
 But isn't suffering only mental?
 No, there's two kinds of suffering, there's physical
 suffering and mental suffering,
 but that's again not what this means.
 I don't know, I think there's a lag, so I'm not sure if
 that was after my explanation or before it.
 But again, that's not what dukkha means here.
 Dukkha means not happiness, or not conducive to happiness,
 not productive of happiness.
 Where is Robin?
 Robin's here.
 I was doing video questions, I was forcing people, trying
 to get people to come on the hangout,
 so didn't have any need for someone to read.
 I think there's a way at some time, and then we just stop.
 Everything changes, this is what you have to realize.
 Don't get attached to things as they are.
 Try to change the format around, shake things up.
 Anyway, that's all for tonight.
 Thank you all for tuning in.
 Tomorrow, another thing is tomorrow at the university, it's
 called May at Mac.
 May at Mac, it's the yearly McMaster Open House,
 and the Peace Studies Department has a booth,
 and they've asked some of us to go and talk to new students
 about the Peace Studies Department.
 They take it as an opportunity to get people to sign up for
 the Peace Club as well.
 But it would be interesting, I'll go as a Buddhist monk to
 go and promote the Peace Studies Department.
 I think that'll be good.
 I do believe in it, I'm not sure how, obviously I don't,
 studies is not my, these kind of studies is not my priority
,
 but I want to encourage them and support them.
 Anyone who's talking about peace, studying peace is just
 all good in my book.
 So I'm happy to promote it.
 Anyway, have a good night everyone.
 [BLANK_AUDIO]
