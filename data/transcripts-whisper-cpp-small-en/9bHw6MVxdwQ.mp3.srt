1
00:00:00,000 --> 00:00:05,610
 Hi, so the next question comes from someone called "Kiddie

2
00:00:05,610 --> 00:00:07,560
 Boo Kiddie" and the question

3
00:00:07,560 --> 00:00:12,730
 is "What do monks eat and how do you view food with regards

4
00:00:12,730 --> 00:00:14,900
 to your existence?"

5
00:00:14,900 --> 00:00:21,490
 Monks eat whatever, almost whatever is put in their bowls

6
00:00:21,490 --> 00:00:24,540
 with the qualifier that we

7
00:00:24,540 --> 00:00:28,710
 do tend to separate the food out into that food that is

8
00:00:28,710 --> 00:00:31,040
 considered to be healthy and

9
00:00:31,040 --> 00:00:35,290
 beneficial to the body and that food which is unhealthy and

10
00:00:35,290 --> 00:00:36,640
 unbeneficial.

11
00:00:36,640 --> 00:00:40,740
 So a lot of fatty foods and sweet foods and so on, we're

12
00:00:40,740 --> 00:00:43,240
 allowed to separate them out.

13
00:00:43,240 --> 00:00:49,020
 We don't have to just eat blindly whatever we get but once

14
00:00:49,020 --> 00:00:54,440
 that hurdle has been passed

15
00:00:54,440 --> 00:00:58,230
 we don't generally express much preference in regards to

16
00:00:58,230 --> 00:01:00,000
 this food or that food.

17
00:01:00,000 --> 00:01:04,080
 Though we're not allowed to eat meat that has been killed

18
00:01:04,080 --> 00:01:05,960
 for our benefit and we're

19
00:01:05,960 --> 00:01:08,960
 not allowed to eat raw flesh and so on, we're not allowed

20
00:01:08,960 --> 00:01:10,640
 to eat certain types of meat like

21
00:01:10,640 --> 00:01:17,570
 snake and elephant and lion and a lot of maybe the more rep

22
00:01:17,570 --> 00:01:20,760
ulsive or exotic foods.

23
00:01:20,760 --> 00:01:27,000
 How do I view food with regards to my existence?

24
00:01:27,000 --> 00:01:29,730
 Well the Buddha taught that there are four kinds of food

25
00:01:29,730 --> 00:01:31,160
 and the first kind is the food

26
00:01:31,160 --> 00:01:32,600
 that we eat.

27
00:01:32,600 --> 00:01:37,890
 This is the rice and the curries and the hamburgers and the

28
00:01:37,890 --> 00:01:40,960
 sandwiches and so on or whatever you

29
00:01:40,960 --> 00:01:44,480
 get in whatever culture.

30
00:01:44,480 --> 00:01:49,100
 Anything that is of nutritious value to a person.

31
00:01:49,100 --> 00:01:52,980
 The second type of food is contact and the meaning of food

32
00:01:52,980 --> 00:01:55,120
 here is something that brings

33
00:01:55,120 --> 00:01:57,920
 fruit, something that brings a result.

34
00:01:57,920 --> 00:02:03,580
 So the result of the physical food is that it brings

35
00:02:03,580 --> 00:02:07,780
 strength and sustenance to the body.

36
00:02:07,780 --> 00:02:10,950
 But another type of food, something that feeds you or

37
00:02:10,950 --> 00:02:13,280
 brings some kind of fruit is contact.

38
00:02:13,280 --> 00:02:15,380
 So when you see, when you hear, when you smell, when you

39
00:02:15,380 --> 00:02:16,880
 taste, when you feel anything, this

40
00:02:16,880 --> 00:02:20,590
 is also a sort of a type of food that feeds the mind and as

41
00:02:20,590 --> 00:02:22,760
 a result there arise all sorts

42
00:02:22,760 --> 00:02:23,760
 of defilements.

43
00:02:23,760 --> 00:02:26,890
 You know when you see food for instance, it makes your

44
00:02:26,890 --> 00:02:28,840
 mouth water even though you haven't

45
00:02:28,840 --> 00:02:32,790
 even tasted it because there's the contact between the eye

46
00:02:32,790 --> 00:02:34,720
 and the food it brings about

47
00:02:34,720 --> 00:02:36,680
 a liking or disliking.

48
00:02:36,680 --> 00:02:39,900
 This is the cause of all of the good things and bad things

49
00:02:39,900 --> 00:02:41,400
 that exist in our mind.

50
00:02:41,400 --> 00:02:45,990
 Why we get attached to things, why we get repulsed by

51
00:02:45,990 --> 00:02:47,040
 things.

52
00:02:47,040 --> 00:02:50,820
 It's also how we become enlightened through connecting with

53
00:02:50,820 --> 00:02:52,440
 reality and coming to see

54
00:02:52,440 --> 00:02:55,040
 it clearly for what it is.

55
00:02:55,040 --> 00:03:00,620
 The third type of food is called volition or intention and

56
00:03:00,620 --> 00:03:02,280
 this is karma.

57
00:03:02,280 --> 00:03:06,740
 This is the good and the bad deeds that we decide to do.

58
00:03:06,740 --> 00:03:09,740
 When we decide to hurt someone, when we decide to help

59
00:03:09,740 --> 00:03:12,160
 someone, when we decide to say something

60
00:03:12,160 --> 00:03:17,280
 nasty or when we decide to say something nice and helpful.

61
00:03:17,280 --> 00:03:20,200
 It's our intention that is a food.

62
00:03:20,200 --> 00:03:22,400
 It's something that feeds the world around us.

63
00:03:22,400 --> 00:03:23,960
 It feeds our reality.

64
00:03:23,960 --> 00:03:29,400
 So when we intend to hurt someone, it's that which brings

65
00:03:29,400 --> 00:03:32,040
 about all of our actions that

66
00:03:32,040 --> 00:03:34,280
 hurt and do nasty things to others.

67
00:03:34,280 --> 00:03:37,060
 When we have the intention to help other people, then it

68
00:03:37,060 --> 00:03:38,880
 feeds all of our actions in the other

69
00:03:38,880 --> 00:03:40,680
 direction.

70
00:03:40,680 --> 00:03:47,600
 The fourth type of food is consciousness.

71
00:03:47,600 --> 00:03:53,290
 So it's our consciousness which feeds all of our experience

72
00:03:53,290 --> 00:03:55,580
, all of our reality.

73
00:03:55,580 --> 00:03:57,280
 Our reality is based on consciousness.

74
00:03:57,280 --> 00:04:00,100
 If there were no consciousness, then we wouldn't have any

75
00:04:00,100 --> 00:04:02,000
 awareness obviously of reality.

76
00:04:02,000 --> 00:04:04,520
 There would be no reality for us.

77
00:04:04,520 --> 00:04:05,520
 There would be no world.

78
00:04:05,520 --> 00:04:07,000
 There would be no existence.

79
00:04:07,000 --> 00:04:11,120
 So consciousness is considered the fourth type of food.

80
00:04:11,120 --> 00:04:14,380
 So that's probably a little bit more of a philosophical

81
00:04:14,380 --> 00:04:16,320
 answer than you were expecting

82
00:04:16,320 --> 00:04:18,120
 but that's how I regard food.

83
00:04:18,120 --> 00:04:20,120
 Food is something that brings a result.

84
00:04:20,120 --> 00:04:23,770
 Whatever feeds my experience or feeds my reality, I

85
00:04:23,770 --> 00:04:25,400
 consider that to be food.

86
00:04:25,400 --> 00:04:30,010
 As far as the physical lumps of rice and curry and so on,

87
00:04:30,010 --> 00:04:32,840
 that's really just a part of it.

88
00:04:32,840 --> 00:04:37,240
 It's something that generally feeds the negative side of

89
00:04:37,240 --> 00:04:40,320
 our reality, the addictive side where

90
00:04:40,320 --> 00:04:43,240
 we become addicted to certain types of food.

91
00:04:43,240 --> 00:04:45,940
 That's certainly something that we try not to feed and we

92
00:04:45,940 --> 00:04:46,840
 try to cut off.

93
00:04:46,840 --> 00:04:50,900
 So I only generally eat one meal a day and try to be very

94
00:04:50,900 --> 00:04:53,280
 mindful when I eat being aware

95
00:04:53,280 --> 00:04:56,340
 of the tastes and aware of the feeling of the food in the

96
00:04:56,340 --> 00:04:58,240
 mouth and going down the throat

97
00:04:58,240 --> 00:05:04,220
 and just being aware of it for what it is, seeing it as

98
00:05:04,220 --> 00:05:08,280
 something ordinary and something

99
00:05:08,280 --> 00:05:12,360
 in terms of the phenomenology of it, the reality of it.

100
00:05:12,360 --> 00:05:14,240
 It's a phenomenon that arises and ceases.

101
00:05:14,240 --> 00:05:18,440
 It's something that comes and goes and it has no substance

102
00:05:18,440 --> 00:05:19,960
 in and of itself.

103
00:05:19,960 --> 00:05:23,580
 The act of eating brings about a fruit for the body that

104
00:05:23,580 --> 00:05:25,520
 allows the body to continue

105
00:05:25,520 --> 00:05:28,000
 on for one more day.

106
00:05:28,000 --> 00:05:29,000
 So hope that's of interest.

107
00:05:29,000 --> 00:05:30,000
 Thanks for the question.

