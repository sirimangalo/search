1
00:00:00,000 --> 00:00:04,110
 Okay, one rather simple question about eating and

2
00:00:04,110 --> 00:00:06,280
 meditation. For example, I am doing something

3
00:00:06,280 --> 00:00:09,380
 which will take me half an hour or hour to finish, and

4
00:00:09,380 --> 00:00:11,200
 after that I am thinking to do

5
00:00:11,200 --> 00:00:15,360
 some meditation. But during this half hour I become hungry.

6
00:00:15,360 --> 00:00:17,020
 Should I eat and when I finish

7
00:00:17,020 --> 00:00:20,840
 doing what I'm doing meditate or not eat and keep working,

8
00:00:20,840 --> 00:00:22,880
 then do the meditation on hungry

9
00:00:22,880 --> 00:00:29,070
 stomach or maybe just have a snack. It's not exactly a

10
00:00:29,070 --> 00:00:32,840
 simple question. There's a lot in

11
00:00:32,840 --> 00:00:43,050
 there that's a bit, I'm not quite clear on the whole of the

12
00:00:43,050 --> 00:00:44,960
 question. But one thing I

13
00:00:44,960 --> 00:00:48,590
 would say which is kind of smarmy is that your work should

14
00:00:48,590 --> 00:00:50,600
 be meditation and your eating

15
00:00:50,600 --> 00:00:54,300
 should be meditation. So that's really where your answer,

16
00:00:54,300 --> 00:00:55,800
 that's the most important part

17
00:00:55,800 --> 00:01:00,810
 of the answer. It's not really smarmy at all. It's probably

18
00:01:00,810 --> 00:01:03,120
 not the answer that most people

19
00:01:03,120 --> 00:01:11,210
 would expect. The meditation starts now. It doesn't start

20
00:01:11,210 --> 00:01:12,400
 30 minutes from now when you're

21
00:01:12,400 --> 00:01:16,750
 going to meditate. Because then you're wasting 30 minutes

22
00:01:16,750 --> 00:01:18,920
 thinking it's okay. In 30 minutes

23
00:01:18,920 --> 00:01:22,320
 I'll meditate or in an hour I'll meditate or after this I

24
00:01:22,320 --> 00:01:24,480
'll meditate. And during all that

25
00:01:24,480 --> 00:01:29,130
 30 minutes or an hour you're wasting good meditation time.

26
00:01:29,130 --> 00:01:31,040
 So when you work you should

27
00:01:31,040 --> 00:01:35,120
 try to be mindful. When you eat you should try to be

28
00:01:35,120 --> 00:01:38,240
 mindful. You should work on that.

29
00:01:38,240 --> 00:01:42,600
 Why it's not such a smarmy answer after all is that that

30
00:01:42,600 --> 00:01:44,880
 will really help your formal

31
00:01:44,880 --> 00:01:47,920
 meditation. If a person just does formal meditation and

32
00:01:47,920 --> 00:01:50,000
 doesn't think at all about meditation

33
00:01:50,000 --> 00:01:54,000
 during their daily life, the formal meditation becomes

34
00:01:54,000 --> 00:01:56,560
 quite difficult and it's a constant

35
00:01:56,560 --> 00:02:01,280
 uphill struggle because it's out of tune with your ordinary

36
00:02:01,280 --> 00:02:02,880
 reality. So you should try to

37
00:02:02,880 --> 00:02:06,860
 constantly be bringing your mind back to attention. I mean

38
00:02:06,860 --> 00:02:09,160
 even right now here we are sitting

39
00:02:09,160 --> 00:02:13,650
 together we should try to make it a mindful experience.

40
00:02:13,650 --> 00:02:17,000
 Being aware at least of our emotions,

41
00:02:17,000 --> 00:02:22,920
 likes and dislikes, the distractions in the mind, the

42
00:02:22,920 --> 00:02:25,800
 feelings that we have. If you're

43
00:02:25,800 --> 00:02:28,820
 just listening here, because I'm talking I have to do more

44
00:02:28,820 --> 00:02:30,200
 action, but for some of you

45
00:02:30,200 --> 00:02:37,460
 just listening you can just revert back to your meditation

46
00:02:37,460 --> 00:02:41,600
 unless you have questions.

47
00:02:41,600 --> 00:02:44,120
 But as far as me making decisions as to when you should do

48
00:02:44,120 --> 00:02:45,560
 eating and when you should do

49
00:02:45,560 --> 00:02:50,730
 meditating, meditating on an empty stomach is interesting,

50
00:02:50,730 --> 00:02:52,800
 can be useful to help you

51
00:02:52,800 --> 00:03:00,030
 to learn about hunger. But I'd say generally you need the

52
00:03:00,030 --> 00:03:05,320
 food for the energy for the meditation,

53
00:03:05,320 --> 00:03:08,040
 so better to have a little bit of food before you meditate.

54
00:03:08,040 --> 00:03:10,920
 I don't know, meditate anytime.

55
00:03:10,920 --> 00:03:14,730
 Whenever you meditate, meditate, get some time for it and

56
00:03:14,730 --> 00:03:17,120
 do it. Just don't procrastinate.

57
00:03:17,120 --> 00:03:21,130
 It kind of sounds like you might be talking about procrast

58
00:03:21,130 --> 00:03:23,520
inating, which is dangerous.

59
00:03:23,520 --> 00:03:28,910
 So it goes, "Work for a half an hour and then I'll have a

60
00:03:28,910 --> 00:03:30,440
 snack." And then when I'm having

61
00:03:30,440 --> 00:03:33,850
 a snack I think about, "Oh then I have to go check Facebook

62
00:03:33,850 --> 00:03:35,480
 or email and check email."

63
00:03:35,480 --> 00:03:37,550
 And then something comes up in email and I have to call

64
00:03:37,550 --> 00:03:38,800
 this person or that person or

65
00:03:38,800 --> 00:03:43,090
 do this or I have to go to the store. And then you never

66
00:03:43,090 --> 00:03:45,360
 meditate after all. That kind of

67
00:03:45,360 --> 00:03:51,010
 thing is dangerous. So better to be clear with yourself

68
00:03:51,010 --> 00:03:53,840
 that if you really need food

69
00:03:53,840 --> 00:03:58,820
 and you're really hungry, then eat or eat a little bit. But

70
00:03:58,820 --> 00:04:03,160
 if you don't, sometimes

71
00:04:03,160 --> 00:04:08,510
 it's just procrastinating. Because meditation can be

72
00:04:08,510 --> 00:04:09,880
 difficult and the mind will actually

73
00:04:09,880 --> 00:04:15,800
 develop aversion towards it if you're not careful. And if

74
00:04:15,800 --> 00:04:18,320
 you're not aware of the aversion,

75
00:04:18,320 --> 00:04:21,440
 if you're not looking at it and contemplating it as well,

76
00:04:21,440 --> 00:04:23,920
 it can become a great hindrance.

77
00:04:23,920 --> 00:04:26,910
 And more than a hindrance, it can actually trick you into

78
00:04:26,910 --> 00:04:28,800
 every time you think of meditation

79
00:04:28,800 --> 00:04:31,580
 your mind says, "Oh, he's thinking of meditation again. Let

80
00:04:31,580 --> 00:04:33,200
's divert his attention. Hey, look

81
00:04:33,200 --> 00:04:38,540
 at that. That's nice. Hey, aren't you hungry? Hey, want to

82
00:04:38,540 --> 00:04:41,160
 go for burgers or something?"

83
00:04:41,160 --> 00:04:44,310
 Right? The mind will do this to you. Because it doesn't

84
00:04:44,310 --> 00:04:45,920
 want to meditate. It's like, "Oh

85
00:04:45,920 --> 00:04:53,810
 no, don't put me back there. That's torture." So be careful

86
00:04:53,810 --> 00:04:54,800
 of that one.

