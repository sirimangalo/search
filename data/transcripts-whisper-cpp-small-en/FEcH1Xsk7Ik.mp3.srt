1
00:00:00,000 --> 00:00:04,640
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,640 --> 00:00:06,580
 Today we continue on with

3
00:00:06,580 --> 00:00:12,360
 verse 159 which reads as follows.

4
00:00:12,360 --> 00:00:21,030
 atanan chaitathagayira yatanya manosasati sudanto vatadamet

5
00:00:21,030 --> 00:00:25,360
an atahikira dudamo

6
00:00:25,360 --> 00:00:39,890
 which means if one makes oneself if one does to oneself as

7
00:00:39,890 --> 00:00:44,520
 one instructs others

8
00:00:45,240 --> 00:00:54,240
 then indeed such a well trained one should train others for

9
00:00:54,240 --> 00:00:55,720
 indeed

10
00:00:55,720 --> 00:01:00,490
 difficult the training of this it is the self it is the

11
00:01:00,490 --> 00:01:02,400
 self indeed that is

12
00:01:02,400 --> 00:01:07,520
 difficult to train atahikira dudamo

13
00:01:07,520 --> 00:01:17,840
 Dhamma is training sudanta one who is well trained Dhammeta

14
00:01:17,840 --> 00:01:19,160
 makes other

15
00:01:19,160 --> 00:01:26,080
 people train dudamo difficult to train the self is

16
00:01:26,080 --> 00:01:27,920
 difficult to train

17
00:01:27,920 --> 00:01:35,150
 Dhammati it's an important verb in Buddhism the Buddha is

18
00:01:35,150 --> 00:01:36,200
 said to be a

19
00:01:36,200 --> 00:01:47,520
 puri sadaam sahrati a trainer he is a trainer of those who

20
00:01:47,520 --> 00:01:55,360
 are trainable so

21
00:01:55,360 --> 00:02:01,330
 this was told in relation to in regards to a story of a

22
00:02:01,330 --> 00:02:02,920
 monk called Badani

23
00:02:02,920 --> 00:02:09,360
 Padani kathisa Padana means effort so Padani ka is one who

24
00:02:09,360 --> 00:02:10,120
 has effort

25
00:02:10,120 --> 00:02:14,010
 this was his name but they called him Padani ka and it's an

26
00:02:14,010 --> 00:02:16,360
 ironic name it's

27
00:02:16,360 --> 00:02:19,630
 it's kind of a evidence that there might be a sense of

28
00:02:19,630 --> 00:02:22,360
 humor among me among me

29
00:02:22,360 --> 00:02:27,440
 compilers of the Pali again or a sense of humor at the time

30
00:02:27,440 --> 00:02:28,040
 in the time of the

31
00:02:28,040 --> 00:02:36,370
 Buddha at the very least it's a sign of it's a sense of

32
00:02:36,370 --> 00:02:39,160
 sense of shame I suppose

33
00:02:39,160 --> 00:02:42,620
 it's how this monk is remembered which is unfortunate

34
00:02:42,620 --> 00:02:43,880
 because the story is

35
00:02:43,880 --> 00:02:47,460
 actually quite unflattering to him in regards to effort

36
00:02:47,460 --> 00:02:48,680
 especially so the

37
00:02:48,680 --> 00:02:57,960
 story goes that he received a meditation object practice of

38
00:02:57,960 --> 00:02:59,880
 meditation from the

39
00:02:59,880 --> 00:03:03,870
 Buddha and together with a large group of monks went off

40
00:03:03,870 --> 00:03:05,200
 into the forest and

41
00:03:05,200 --> 00:03:08,720
 entered into the rains so right now we're in the rainy

42
00:03:08,720 --> 00:03:10,020
 season and this is a

43
00:03:10,020 --> 00:03:14,620
 three-month period where we don't go anywhere we can go but

44
00:03:14,620 --> 00:03:15,720
 we can't stay

45
00:03:15,720 --> 00:03:21,160
 overnight normally I try to stay put for three months and

46
00:03:21,160 --> 00:03:23,160
 focus on our practice

47
00:03:23,160 --> 00:03:29,500
 so that's what they did and when they when they were in the

48
00:03:29,500 --> 00:03:31,080
 forest they this

49
00:03:31,080 --> 00:03:40,300
 this Padani kathisa he says to the other monks we've we've

50
00:03:40,300 --> 00:03:44,160
 received a meditation

51
00:03:44,160 --> 00:03:50,660
 from the Buddha himself let us not let us be a pamata let

52
00:03:50,660 --> 00:03:52,200
 us not be heedless

53
00:03:52,200 --> 00:03:58,720
 someone and among groda do the duties of a summoner of a

54
00:03:58,720 --> 00:04:01,720
 shaman or reckless of an

55
00:04:01,720 --> 00:04:09,970
 ascetic and having said this he went off into his room and

56
00:04:09,970 --> 00:04:12,200
 lay down to sleep and

57
00:04:12,200 --> 00:04:16,580
 the other monks in the first watch of the night they they

58
00:04:16,580 --> 00:04:17,960
 did walking and

59
00:04:17,960 --> 00:04:23,060
 sitting meditation as normal to understand the watches we

60
00:04:23,060 --> 00:04:24,920
 have 12 out I

61
00:04:24,920 --> 00:04:29,730
 know right a 12-hour period right the day has 12 hours the

62
00:04:29,730 --> 00:04:31,000
 night has 12 hours

63
00:04:31,000 --> 00:04:36,680
 so from 6 a.m. to 6 p.m. is daytime from 6 p.m. to 6 a.m.

64
00:04:36,680 --> 00:04:38,600
 this night so the first

65
00:04:38,600 --> 00:04:44,590
 watch is 6 p.m. till 10 p.m. approximately and during that

66
00:04:44,590 --> 00:04:46,480
 time they did intensive

67
00:04:46,480 --> 00:04:50,500
 meditation practice really put out effort and then there's

68
00:04:50,500 --> 00:04:51,900
 the second watch

69
00:04:51,900 --> 00:04:56,670
 of the night from 10 a.m. 10 p.m. till 2 a.m. they went

70
00:04:56,670 --> 00:04:58,680
 back into the into the

71
00:04:58,680 --> 00:05:02,820
 residence the dwelling or their dwellings and lay down to

72
00:05:02,820 --> 00:05:05,080
 go to sleep

73
00:05:05,080 --> 00:05:10,660
 mindfully right as they were lying down or after they just

74
00:05:10,660 --> 00:05:11,400
 fallen asleep this

75
00:05:11,400 --> 00:05:15,790
 other monk having slept soundly wakes up goes into the

76
00:05:15,790 --> 00:05:17,040
 resident the other

77
00:05:17,040 --> 00:05:21,210
 residences and sees the monks lying down asleep and he bang

78
00:05:21,210 --> 00:05:23,560
s a drum or what does

79
00:05:23,560 --> 00:05:34,690
 he do he just went and said to them they said hey what did

80
00:05:34,690 --> 00:05:35,440
 you come here is

81
00:05:35,440 --> 00:05:39,530
 thinking to yourself we'll come here to sleep says get it

82
00:05:39,530 --> 00:05:40,320
 back out there and

83
00:05:40,320 --> 00:05:44,370
 devote yourself to the meditation and so they all wake up

84
00:05:44,370 --> 00:05:45,720
 and they're kind of

85
00:05:45,720 --> 00:05:48,730
 groggy and of course being woken up from sleep is worse

86
00:05:48,730 --> 00:05:50,040
 than not having slept at

87
00:05:50,040 --> 00:05:54,440
 all because your days didn't confused but they went out and

88
00:05:54,440 --> 00:05:55,720
 they tried their

89
00:05:55,720 --> 00:05:59,160
 best and stumbling around for the four hours when they'd

90
00:05:59,160 --> 00:06:01,440
 normally be asleep and

91
00:06:01,440 --> 00:06:04,890
 in the third watch of the night exhausted again they lay

92
00:06:04,890 --> 00:06:05,800
 down to sleep

93
00:06:05,800 --> 00:06:10,280
 and again this monk having told them what to do went back

94
00:06:10,280 --> 00:06:11,720
 to sleep and soon

95
00:06:11,720 --> 00:06:14,370
 as they lay down to sleep or and sometime in the third

96
00:06:14,370 --> 00:06:15,200
 watch the night he

97
00:06:15,200 --> 00:06:18,970
 gets up again and yells at them again and tells them to go

98
00:06:18,970 --> 00:06:20,200
 out there so they

99
00:06:20,200 --> 00:06:23,360
 didn't get really to sleep at all

100
00:06:23,360 --> 00:06:33,500
 and after of course this other month but Danny Katisa got a

101
00:06:33,500 --> 00:06:34,600
 lot of sleep he slept

102
00:06:34,600 --> 00:06:37,750
 through all three watches basically being interrupted

103
00:06:37,750 --> 00:06:38,880
 having to get up and

104
00:06:38,880 --> 00:06:45,750
 struck the other monks so and he did this repeatedly

105
00:06:45,750 --> 00:06:46,760
 basically every night it

106
00:06:46,760 --> 00:06:52,520
 sounds like and the monks after after a while you know they

107
00:06:52,520 --> 00:06:53,400
 weren't getting

108
00:06:53,400 --> 00:06:55,920
 anywhere with their meditation because they were constantly

109
00:06:55,920 --> 00:06:57,000
 being disturbed and

110
00:06:57,000 --> 00:07:05,950
 disoriented by lack of sleep but also this sort of constant

111
00:07:05,950 --> 00:07:07,400
 disruption of

112
00:07:07,400 --> 00:07:13,520
 their ordinary sleep cycle and they thought to themselves

113
00:07:13,520 --> 00:07:14,640
 wow are this is

114
00:07:14,640 --> 00:07:19,130
 really really a tough practice our teacher must be the

115
00:07:19,130 --> 00:07:20,600
 elder must be really

116
00:07:20,600 --> 00:07:25,400
 must be full of energy and quite energetic if he's able to

117
00:07:25,400 --> 00:07:27,040
 do this let's

118
00:07:27,040 --> 00:07:30,220
 go watch him and so they went and they in this first watch

119
00:07:30,220 --> 00:07:31,240
 the night they went

120
00:07:31,240 --> 00:07:35,150
 to look into his room and found that he was sleeping and

121
00:07:35,150 --> 00:07:36,760
 they watched him when

122
00:07:36,760 --> 00:07:39,630
 they realized that he was sleeping throughout the whole

123
00:07:39,630 --> 00:07:41,080
 night they felt

124
00:07:41,080 --> 00:07:46,320
 like they felt betrayed of course and they said our

125
00:07:46,320 --> 00:07:48,600
 teachers teaching somebody

126
00:07:48,600 --> 00:07:52,820
 that he himself doesn't even practice it's empty words they

127
00:07:52,820 --> 00:07:55,320
 said ducha what's

128
00:07:55,320 --> 00:07:57,680
 it called

129
00:08:02,480 --> 00:08:05,480
 to chara long empty

130
00:08:05,480 --> 00:08:15,560
 rava empty sounds is empty words

131
00:08:15,560 --> 00:08:23,410
 and so as a result of their result of that they felt lost

132
00:08:23,410 --> 00:08:24,720
 and they're

133
00:08:24,720 --> 00:08:27,190
 disoriented and they spent the rains retreat not really

134
00:08:27,190 --> 00:08:28,080
 gaining much out of

135
00:08:28,080 --> 00:08:31,130
 it at the end of the rains they left and went back to see

136
00:08:31,130 --> 00:08:31,840
 the Buddha and the

137
00:08:31,840 --> 00:08:36,520
 Buddha said hey how did it go did you were you heedful and

138
00:08:36,520 --> 00:08:37,360
 did you perform

139
00:08:37,360 --> 00:08:41,880
 did you meditate well and they told him the story and he

140
00:08:41,880 --> 00:08:42,960
 said oh this monk has

141
00:08:42,960 --> 00:08:45,810
 always been like this and he told a story of the from the j

142
00:08:45,810 --> 00:08:46,960
ataka this also

143
00:08:46,960 --> 00:08:53,030
 appears in the jataka where he says once this monk was a ro

144
00:08:53,030 --> 00:08:54,440
oster and he was a

145
00:08:54,440 --> 00:08:57,800
 rooster that had never been really taught when to crow and

146
00:08:57,800 --> 00:08:58,840
 so he would crowed

147
00:08:58,840 --> 00:09:03,440
 all day and all night because he had no sense

148
00:09:03,440 --> 00:09:10,440
 one of the jatakas and then he told then he told this verse

149
00:09:10,440 --> 00:09:12,440
 he said look this is

150
00:09:12,440 --> 00:09:16,800
 the case someone before teaching it's not really proper it

151
00:09:16,800 --> 00:09:17,640
's obviously not

152
00:09:17,640 --> 00:09:22,450
 proper if you haven't well developed yourself to go out and

153
00:09:22,450 --> 00:09:24,200
 teach people

154
00:09:24,200 --> 00:09:27,670
 because it's difficult what is really difficult is training

155
00:09:27,670 --> 00:09:28,720
 yourself training

156
00:09:28,720 --> 00:09:32,560
 someone else well obviously it's easy it's easy to tell

157
00:09:32,560 --> 00:09:33,360
 other people what to

158
00:09:33,360 --> 00:09:38,370
 do and so let's talk about that a little bit that's the

159
00:09:38,370 --> 00:09:39,600
 obvious lesson here when

160
00:09:39,600 --> 00:09:42,640
 we talk about how this relates to our practices regards to

161
00:09:42,640 --> 00:09:43,520
 the difference

162
00:09:43,520 --> 00:09:46,950
 between teaching and practicing but first let's first there

163
00:09:46,950 --> 00:09:47,840
's another point

164
00:09:47,840 --> 00:09:52,140
 that also relates to our practice it's in regards to hey

165
00:09:52,140 --> 00:09:52,960
 what is proper

166
00:09:52,960 --> 00:09:58,040
 practice sleeping waking being heedful dedicating to

167
00:09:58,040 --> 00:10:00,280
 yourself to your meditation

168
00:10:00,280 --> 00:10:05,190
 what's the deal with sleep here was this monk wrong in his

169
00:10:05,190 --> 00:10:08,720
 teachings anyway that

170
00:10:08,720 --> 00:10:15,070
 that relate to the second part but yes the question of what

171
00:10:15,070 --> 00:10:16,840
's the right amount

172
00:10:16,840 --> 00:10:21,240
 of effort so the Buddha taught generally speaking for

173
00:10:21,240 --> 00:10:22,840
 intensive meditator to try

174
00:10:22,840 --> 00:10:31,550
 and and get to the point where you can manage on four hours

175
00:10:31,550 --> 00:10:33,840
 of sleep manage only

176
00:10:33,840 --> 00:10:39,150
 to sleep during the middle watch of the night and that's

177
00:10:39,150 --> 00:10:41,080
 not really a theoretical

178
00:10:41,080 --> 00:10:43,350
 thing you can see how that plays out in practice for

179
00:10:43,350 --> 00:10:44,560
 someone who's undertaking

180
00:10:44,560 --> 00:10:48,980
 intensive meditation practice four hours of sleep is really

181
00:10:48,980 --> 00:10:50,520
 optimal in the

182
00:10:50,520 --> 00:10:54,140
 beginning it's difficult and and that's really the most

183
00:10:54,140 --> 00:10:55,560
 important point here is

184
00:10:55,560 --> 00:11:00,200
 not exactly that this monk was advocating not to sleep

185
00:11:00,200 --> 00:11:01,800
 because four

186
00:11:01,800 --> 00:11:04,870
 hours is optimal but for someone who's truly in the zone

187
00:11:04,870 --> 00:11:07,880
 like really in a on a

188
00:11:07,880 --> 00:11:12,220
 role and in a meditative state they don't really need to

189
00:11:12,220 --> 00:11:12,920
 sleep at all that

190
00:11:12,920 --> 00:11:16,140
 it's possible for them to go without sleep for long periods

191
00:11:16,140 --> 00:11:18,520
 of time and and

192
00:11:18,520 --> 00:11:21,470
 this is also something you see you hear stories about it

193
00:11:21,470 --> 00:11:22,440
 but you also have

194
00:11:22,440 --> 00:11:27,080
 examples of people who continue to practice day and night

195
00:11:27,080 --> 00:11:28,560
 but the point is

196
00:11:28,560 --> 00:11:33,080
 that they're really they've developed they've gotten and

197
00:11:33,080 --> 00:11:34,000
 they've cultivated

198
00:11:34,000 --> 00:11:38,500
 that right and so really from my point of view with the the

199
00:11:38,500 --> 00:11:40,080
 two problems first of

200
00:11:40,080 --> 00:11:47,170
 all that this monk was himself not a very energetic fellow

201
00:11:47,170 --> 00:11:48,920
 but also that that

202
00:11:48,920 --> 00:11:52,320
 there was no buildup to it right I mean these monks

203
00:11:52,320 --> 00:11:54,280
 seemingly coming fresh off

204
00:11:54,280 --> 00:11:56,760
 the boat they hadn't really done much meditation or

205
00:11:56,760 --> 00:11:58,080
 intensive meditation of

206
00:11:58,080 --> 00:12:02,420
 this sort and then to just dump them into a practice of not

207
00:12:02,420 --> 00:12:03,680
 sleeping at all

208
00:12:03,680 --> 00:12:07,340
 certainly not going to have positive effects obviously and

209
00:12:07,340 --> 00:12:08,360
 then worse than

210
00:12:08,360 --> 00:12:11,850
 that he was waking them up in the middle of the night in

211
00:12:11,850 --> 00:12:13,000
 the middle of their

212
00:12:13,000 --> 00:12:21,890
 sleep patterns which is you know it's disturbing and to

213
00:12:21,890 --> 00:12:23,920
 some extent and most

214
00:12:23,920 --> 00:12:28,100
 especially with a beginner meditator you want to create a

215
00:12:28,100 --> 00:12:31,000
 sense of comfort and

216
00:12:31,000 --> 00:12:35,150
 routine you know you want things to go smoothly and so it's

217
00:12:35,150 --> 00:12:36,920
 more about reducing

218
00:12:36,920 --> 00:12:40,350
 the amount that you need to sleep as you start to become

219
00:12:40,350 --> 00:12:41,800
 more energetic and as

220
00:12:41,800 --> 00:12:45,000
 you start to become more focused and really this whole

221
00:12:45,000 --> 00:12:46,200
 question about how

222
00:12:46,200 --> 00:12:52,670
 much you should sleep is is very much strongly dependent on

223
00:12:52,670 --> 00:12:53,400
 your state of mind

224
00:12:53,400 --> 00:12:56,610
 right and your state of body if you're exercising a lot

225
00:12:56,610 --> 00:12:57,840
 your body is just going

226
00:12:57,840 --> 00:13:01,390
 to shut down but more importantly if your mind is stressed

227
00:13:01,390 --> 00:13:02,320
 if your mind is

228
00:13:02,320 --> 00:13:06,470
 stressed you'll find it impossible to stay alert to stay

229
00:13:06,470 --> 00:13:07,800
 focused because

230
00:13:07,800 --> 00:13:14,580
 you're taxing your system so so it's just such an extreme

231
00:13:14,580 --> 00:13:15,560
 in our ordinary

232
00:13:15,560 --> 00:13:20,550
 lives living in a complicated society with all of our

233
00:13:20,550 --> 00:13:22,720
 machines and computers

234
00:13:22,720 --> 00:13:27,410
 and so on it's not easy to get into this state where you're

235
00:13:27,410 --> 00:13:29,600
 able to stay focused

236
00:13:29,600 --> 00:13:32,800
 stay awake stay alert

237
00:13:32,800 --> 00:13:40,730
 so it relates to our practice in terms of thinking about

238
00:13:40,730 --> 00:13:43,520
 how much sleep we need

239
00:13:43,520 --> 00:13:50,930
 and having a sense of the potential goodness involved in in

240
00:13:50,930 --> 00:13:52,920
 Leslie involved

241
00:13:52,920 --> 00:13:55,720
 in this practice of getting to the point where you need

242
00:13:55,720 --> 00:13:57,560
 Leslie certainly as

243
00:13:57,560 --> 00:14:00,840
 someone who's in the those really

244
00:14:00,840 --> 00:14:04,800
 progressed in the meditation practice this needs a lot less

245
00:14:04,800 --> 00:14:08,000
 thing as a result

246
00:14:08,000 --> 00:14:12,530
 and so it so in that sense it's not exactly wrong what this

247
00:14:12,530 --> 00:14:13,280
 monk was

248
00:14:13,280 --> 00:14:18,340
 teaching in the sense of advocating Leslie of course he

249
00:14:18,340 --> 00:14:19,200
 went about it all

250
00:14:19,200 --> 00:14:22,620
 wrong and that leads to the second part so the question

251
00:14:22,620 --> 00:14:23,520
 here there's some

252
00:14:23,520 --> 00:14:26,980
 questions involved first the first question is can someone

253
00:14:26,980 --> 00:14:27,720
 who's never

254
00:14:27,720 --> 00:14:35,750
 practiced actually instruct others and it's hard to believe

255
00:14:35,750 --> 00:14:38,960
 really that

256
00:14:38,960 --> 00:14:41,730
 someone that there's another story it's not this story but

257
00:14:41,730 --> 00:14:42,720
 there's a story of a

258
00:14:42,720 --> 00:14:46,560
 monk would never really practiced at all or maybe he had

259
00:14:46,560 --> 00:14:47,560
 but he hadn't gotten

260
00:14:47,560 --> 00:14:51,070
 good results and yet all of the students became enlightened

261
00:14:51,070 --> 00:14:52,760
 and that's hard to

262
00:14:52,760 --> 00:14:57,590
 believe and so there's more to this story than just someone

263
00:14:57,590 --> 00:14:58,640
 teaching but not

264
00:14:58,640 --> 00:15:02,880
 practicing there's the fact that someone who doesn't

265
00:15:02,880 --> 00:15:04,560
 practice or hasn't practiced

266
00:15:04,560 --> 00:15:08,250
 is going to teach his students all wrong right I mean this

267
00:15:08,250 --> 00:15:09,200
 guy went about it all

268
00:15:09,200 --> 00:15:13,610
 wrong he'd heard I guess that one has to put out effort and

269
00:15:13,610 --> 00:15:14,640
 be vigilant and he'd

270
00:15:14,640 --> 00:15:16,940
 maybe heard about monks who didn't sleep at all and so he

271
00:15:16,940 --> 00:15:18,480
 thought well that's

272
00:15:18,480 --> 00:15:21,790
 what my students should do and not having any experience in

273
00:15:21,790 --> 00:15:22,560
 the training of

274
00:15:22,560 --> 00:15:26,010
 the mind himself he didn't have any sense of the gradual

275
00:15:26,010 --> 00:15:27,560
 progression involved in

276
00:15:27,560 --> 00:15:32,630
 that practice so the idea that someone who hadn't practiced

277
00:15:32,630 --> 00:15:35,000
 could adequately

278
00:15:35,000 --> 00:15:39,160
 teach their students and the Buddha's last words about how

279
00:15:39,160 --> 00:15:40,080
 difficult it is to

280
00:15:40,080 --> 00:15:44,400
 teach one to teach others I think it actually points to

281
00:15:44,400 --> 00:15:45,480
 something else

282
00:15:45,480 --> 00:15:48,870
 because for this guy I think it was quite difficult to

283
00:15:48,870 --> 00:15:50,160
 teach others I think

284
00:15:50,160 --> 00:15:54,600
 it was near impossible for him to actually help other

285
00:15:54,600 --> 00:15:57,040
 people it's easy I

286
00:15:57,040 --> 00:16:00,840
 think in regards to this story it's easy to give advice it

287
00:16:00,840 --> 00:16:02,920
's much harder to give

288
00:16:02,920 --> 00:16:08,620
 advice that actually helps your students right anyone can

289
00:16:08,620 --> 00:16:10,600
 give advice

290
00:16:15,520 --> 00:16:21,080
 but the other question here is of

291
00:16:21,080 --> 00:16:36,840
 the question of

292
00:16:36,840 --> 00:16:40,910
 the difficulty in teaching one teaching yourself versus

293
00:16:40,910 --> 00:16:42,520
 teaching others and I

294
00:16:42,520 --> 00:16:46,330
 think this is more of a problem for people who are on the

295
00:16:46,330 --> 00:16:47,920
 path someone who

296
00:16:47,920 --> 00:16:51,570
 has practiced and has gained results it's easy to get

297
00:16:51,570 --> 00:16:53,000
 distracted teaching

298
00:16:53,000 --> 00:16:56,420
 others and I don't think that's really what this this story

299
00:16:56,420 --> 00:16:57,560
 is talking about

300
00:16:57,560 --> 00:17:00,340
 because this guy went about it all wrong and clearly didn't

301
00:17:00,340 --> 00:17:00,920
 have a good

302
00:17:00,920 --> 00:17:04,080
 understanding of how meditation practice works he ruined

303
00:17:04,080 --> 00:17:05,440
 these other monks

304
00:17:05,440 --> 00:17:09,790
 meditation but I would really agree with what the Buddha

305
00:17:09,790 --> 00:17:10,600
 says at the end about

306
00:17:10,600 --> 00:17:13,280
 how easy it is about how hard it was really hard is

307
00:17:13,280 --> 00:17:14,400
 teaching at teaching

308
00:17:14,400 --> 00:17:18,180
 yourself it's it's quite easy for someone who does have

309
00:17:18,180 --> 00:17:19,760
 positive results

310
00:17:19,760 --> 00:17:24,000
 and meaningful results in the practice to teach others it's

311
00:17:24,000 --> 00:17:25,240
 easy to give

312
00:17:25,240 --> 00:17:29,860
 advice and say hey do this do that and it's it's a quite a

313
00:17:29,860 --> 00:17:31,360
 distraction on the

314
00:17:31,360 --> 00:17:36,560
 path and so there's two cautions here in regards to how

315
00:17:36,560 --> 00:17:38,520
 this relates to our

316
00:17:38,520 --> 00:17:48,140
 practice as meditators the first one is it's a strong

317
00:17:48,140 --> 00:17:50,040
 encouragement to never

318
00:17:50,040 --> 00:17:55,480
 ever ever teach another person unless you yourself have

319
00:17:55,480 --> 00:17:56,440
 gained something from

320
00:17:56,440 --> 00:18:00,960
 the practice because the potential for you to teach it all

321
00:18:00,960 --> 00:18:03,080
 wrong is incredibly

322
00:18:03,080 --> 00:18:08,800
 great is is more likely I've seen teachers and heard about

323
00:18:08,800 --> 00:18:09,680
 teachers who

324
00:18:09,680 --> 00:18:15,160
 are of this sort whose students get on the wrong path

325
00:18:15,160 --> 00:18:16,240
 because their teacher has

326
00:18:16,240 --> 00:18:19,810
 never gained any understanding of how the practice works

327
00:18:19,810 --> 00:18:22,680
 for themselves but

328
00:18:22,680 --> 00:18:27,730
 the second caution is that once you do get results don't

329
00:18:27,730 --> 00:18:30,400
 stop and don't get

330
00:18:30,400 --> 00:18:35,150
 sidetracked into making your life all about teaching others

331
00:18:35,150 --> 00:18:36,960
 our lives as as

332
00:18:36,960 --> 00:18:42,370
 Buddhists should be very much about our own practice and

333
00:18:42,370 --> 00:18:44,080
 you can go on and on

334
00:18:44,080 --> 00:18:48,230
 and on teaching others but what you end up with is a lot of

335
00:18:48,230 --> 00:18:49,240
 people practicing on

336
00:18:49,240 --> 00:18:53,720
 a very shallow level because no one ever gets or will you

337
00:18:53,720 --> 00:18:55,840
 never get to to

338
00:18:55,840 --> 00:18:59,720
 understand the teachings on a deeper level you have to push

339
00:18:59,720 --> 00:19:00,960
 on yourself work

340
00:19:00,960 --> 00:19:07,760
 for your own salvation and so this is really a problem for

341
00:19:07,760 --> 00:19:08,560
 those who are on

342
00:19:08,560 --> 00:19:12,120
 the path of course for for someone who has finished the

343
00:19:12,120 --> 00:19:13,640
 path has become fully

344
00:19:13,640 --> 00:19:18,200
 enlightened they have nothing to do more for themselves and

345
00:19:18,200 --> 00:19:20,360
 often it can happen

346
00:19:20,360 --> 00:19:25,290
 that they do spend a lot of their time helping others not

347
00:19:25,290 --> 00:19:27,360
 always quite often

348
00:19:27,360 --> 00:19:31,420
 they're whether certainly most content not to help anyone

349
00:19:31,420 --> 00:19:32,880
 but they often find

350
00:19:32,880 --> 00:19:37,720
 themselves helping people much of their time because there

351
00:19:37,720 --> 00:19:39,240
's such a demand on

352
00:19:39,240 --> 00:19:45,460
 their they're in such high demand for their greatness for

353
00:19:45,460 --> 00:19:48,000
 their ability and

354
00:19:48,000 --> 00:19:54,250
 understanding wisdom and so on so two aspects first the

355
00:19:54,250 --> 00:19:56,360
 caution and the

356
00:19:56,360 --> 00:20:00,000
 importance of right understanding in regards to sleep in

357
00:20:00,000 --> 00:20:01,240
 regards to putting

358
00:20:01,240 --> 00:20:06,120
 out right effort let's be clear the Buddha was advocating

359
00:20:06,120 --> 00:20:07,560
 intensive practice

360
00:20:07,560 --> 00:20:15,760
 for 20 hours a day and sometimes more practicing throughout

361
00:20:15,760 --> 00:20:16,320
 the day and the

362
00:20:16,320 --> 00:20:21,300
 night at times without sleep but it has to be done right

363
00:20:21,300 --> 00:20:22,840
 and the second thing is

364
00:20:22,840 --> 00:20:28,550
 in regards to teaching which is really the primary lesson

365
00:20:28,550 --> 00:20:30,120
 of this don't get

366
00:20:30,120 --> 00:20:32,990
 caught up in teaching certainly don't think about teaching

367
00:20:32,990 --> 00:20:34,640
 others until you

368
00:20:34,640 --> 00:20:39,640
 yourself have settled yourself in what's right and even

369
00:20:39,640 --> 00:20:41,520
 then teaching is fine

370
00:20:41,520 --> 00:20:48,150
 it's good but it's not something that should take over your

371
00:20:48,150 --> 00:20:48,960
 life to the point

372
00:20:48,960 --> 00:20:55,520
 that you stop practicing yourself so another Dhammapada

373
00:20:55,520 --> 00:20:57,400
 verse thank you all

374
00:20:57,400 --> 00:21:02,560
 for tuning in wish you all the best

