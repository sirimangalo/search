1
00:00:00,000 --> 00:00:05,320
 Good evening.

2
00:00:05,320 --> 00:00:22,960
 Podcasting from Stony Creek Ontario.

3
00:00:22,960 --> 00:00:40,200
 Here to give a daily teaching on the Dhamma.

4
00:00:40,200 --> 00:00:53,160
 When we practice the Dhamma, we focus our attention on Duk

5
00:00:53,160 --> 00:00:54,560
ha.

6
00:00:54,560 --> 00:01:01,040
 This is where we should greatly focus our attention.

7
00:01:01,040 --> 00:01:06,200
 It is the object of study.

8
00:01:06,200 --> 00:01:11,480
 The Buddha said we must know thoroughly, suffering.

9
00:01:11,480 --> 00:01:15,440
 We must understand thoroughly.

10
00:01:15,440 --> 00:01:21,770
 We must understand that the things that are causing us

11
00:01:21,770 --> 00:01:24,640
 suffering are causing us suffering.

12
00:01:24,640 --> 00:01:29,640
 We think that we wrongly think are bringing us happiness.

13
00:01:29,640 --> 00:01:33,910
 It's important that we see the truth that they are not

14
00:01:33,910 --> 00:01:40,320
 actually bringing us happiness.

15
00:01:40,320 --> 00:01:45,080
 That we see the suffering caused by unwholesomeness.

16
00:01:45,080 --> 00:01:52,300
 We see the suffering inherent in the things that we cling

17
00:01:52,300 --> 00:01:53,120
 to.

18
00:01:53,120 --> 00:02:00,240
 This is the main task in Buddhism.

19
00:02:00,240 --> 00:02:06,720
 It's quite simple actually.

20
00:02:06,720 --> 00:02:15,000
 Then we have to ask ourselves what is meant by suffering.

21
00:02:15,000 --> 00:02:23,190
 We look to the Buddha's words and find out what the Buddha

22
00:02:23,190 --> 00:02:26,400
 said on suffering.

23
00:02:26,400 --> 00:02:34,630
 The words of the Buddha are Sankitena Panchupa Dhanakanta D

24
00:02:34,630 --> 00:02:35,400
ukha.

25
00:02:35,400 --> 00:02:39,280
 The five Bhadhanakanta.

26
00:02:39,280 --> 00:02:46,040
 This is suffering in brief, Sankitena.

27
00:02:46,040 --> 00:02:53,000
 In brief.

28
00:02:53,000 --> 00:02:57,560
 Five aggregates of suffering.

29
00:02:57,560 --> 00:02:59,680
 Five aggregates of clinging.

30
00:02:59,680 --> 00:03:11,740
 The five aggregates of things, the five groups of things

31
00:03:11,740 --> 00:03:19,200
 which are the object of clinging.

32
00:03:19,200 --> 00:03:23,720
 This is where our study must take place.

33
00:03:23,720 --> 00:03:36,880
 We must learn about these five things.

34
00:03:36,880 --> 00:03:40,640
 The first one, rupa, we have to learn about form.

35
00:03:40,640 --> 00:03:47,920
 We have to understand the physical.

36
00:03:47,920 --> 00:03:51,950
 Some physical form in Buddhism is still just a product of

37
00:03:51,950 --> 00:03:54,720
 experience or a part of experience.

38
00:03:54,720 --> 00:04:00,640
 What we're talking about here is the four elements of earth

39
00:04:00,640 --> 00:04:03,640
, air, water and fire which

40
00:04:03,640 --> 00:04:10,000
 are the four aspects of our experience of matter.

41
00:04:10,000 --> 00:04:18,240
 When we experience hardness or softness, this is earth.

42
00:04:18,240 --> 00:04:19,960
 When we experience heat or cold, this is fire.

43
00:04:19,960 --> 00:04:26,520
 When we experience pressure, this is air.

44
00:04:26,520 --> 00:04:33,120
 Water is the cohesion and the stickiness.

45
00:04:33,120 --> 00:04:39,120
 Traction draws entities together.

46
00:04:39,120 --> 00:04:43,240
 This is the physical.

47
00:04:43,240 --> 00:04:46,670
 It's the physical that underlines the whole of the

48
00:04:46,670 --> 00:04:50,320
 conceptual physical world around us.

49
00:04:50,320 --> 00:04:54,640
 Objects, people, places, things.

50
00:04:54,640 --> 00:05:02,480
 All of this is conceptual that we put together in our minds

51
00:05:02,480 --> 00:05:04,360
 based on the experiences that

52
00:05:04,360 --> 00:05:05,360
 we have.

53
00:05:05,360 --> 00:05:09,930
 We see certain things or hear or smell or taste or feel all

54
00:05:09,930 --> 00:05:12,040
 around the world around

55
00:05:12,040 --> 00:05:20,440
 us and we extrapolate and create entities out of it.

56
00:05:20,440 --> 00:05:27,820
 These entities give us the illusion of permanent stability,

57
00:05:27,820 --> 00:05:31,720
 of pleasure, satisfaction.

58
00:05:31,720 --> 00:05:39,560
 When we look underneath and see the volatile building

59
00:05:39,560 --> 00:05:43,960
 blocks that make up the experience,

60
00:05:43,960 --> 00:05:51,360
 make up the entities, these experiences that arise and

61
00:05:51,360 --> 00:05:55,640
 cease without warning out of our

62
00:05:55,640 --> 00:05:59,410
 control, then we see that this isn't a means of

63
00:05:59,410 --> 00:06:02,160
 satisfaction, this isn't where happiness

64
00:06:02,160 --> 00:06:03,160
 is found.

65
00:06:03,160 --> 00:06:06,000
 This is actually a source of great suffering.

66
00:06:06,000 --> 00:06:12,270
 When we cling to it, when we try to fix it, we try to hold

67
00:06:12,270 --> 00:06:15,280
 on to or push away various

68
00:06:15,280 --> 00:06:18,960
 aspects of the physical world.

69
00:06:18,960 --> 00:06:23,840
 Pushing away people, clinging to people, pushing away

70
00:06:23,840 --> 00:06:27,080
 places and things, clinging to places

71
00:06:27,080 --> 00:06:28,080
 and things, honest.

72
00:06:28,080 --> 00:06:34,280
 This is what we study inside meditation.

73
00:06:34,280 --> 00:06:36,520
 We study the second one, feelings.

74
00:06:36,520 --> 00:06:43,780
 We study our pleasure, we study our pain, we study even our

75
00:06:43,780 --> 00:06:46,920
 calm feelings, any type

76
00:06:46,920 --> 00:06:52,520
 of feeling we study it.

77
00:06:52,520 --> 00:06:59,090
 In our study we come to see that even happy feelings we can

78
00:06:59,090 --> 00:07:02,480
't find satisfaction in.

79
00:07:02,480 --> 00:07:08,070
 Even happy feelings have no benefit or use to us, no

80
00:07:08,070 --> 00:07:09,400
 purpose.

81
00:07:09,400 --> 00:07:15,920
 They come and they go and it's not happiness that makes you

82
00:07:15,920 --> 00:07:17,640
 more happy.

83
00:07:17,640 --> 00:07:20,440
 Holding onto happiness doesn't make you happy.

84
00:07:20,440 --> 00:07:23,360
 It's not the cause of happiness.

85
00:07:23,360 --> 00:07:27,840
 It's not happiness that causes happiness.

86
00:07:27,840 --> 00:07:30,560
 In other words, we can't hold on to happiness.

87
00:07:30,560 --> 00:07:35,400
 When you hold on to it, it doesn't make you happy.

88
00:07:35,400 --> 00:07:40,000
 It makes you want it more needed.

89
00:07:40,000 --> 00:07:44,760
 When this is upada, you come to need it.

90
00:07:44,760 --> 00:07:49,160
 You're dissatisfied when you can't have it, which is

91
00:07:49,160 --> 00:07:52,040
 inevitable because happiness doesn't

92
00:07:52,040 --> 00:07:56,040
 bring happiness.

93
00:07:56,040 --> 00:08:08,490
 We study the painful feelings, the neutral feelings, the

94
00:08:08,490 --> 00:08:12,160
 nature of feelings, things being

95
00:08:12,160 --> 00:08:13,160
 not a refuge.

96
00:08:13,160 --> 00:08:24,560
 We can't find refuge because they change.

97
00:08:24,560 --> 00:08:31,350
 We study our own memories and perceptions, the recognitions

98
00:08:31,350 --> 00:08:35,520
, the similarities, the perception

99
00:08:35,520 --> 00:08:38,800
 of the world around us, how we perceive things to be

100
00:08:38,800 --> 00:08:39,640
 objects.

101
00:08:39,640 --> 00:08:45,300
 Our actual perception of entities, people, places, things,

102
00:08:45,300 --> 00:08:47,800
 this actual perception.

103
00:08:47,800 --> 00:08:52,360
 We study that.

104
00:08:52,360 --> 00:08:55,360
 This isn't the mind now.

105
00:08:55,360 --> 00:08:56,360
 It's nothing to do with the world around us.

106
00:08:56,360 --> 00:08:59,150
 We see something and then your mind processes it and gets

107
00:08:59,150 --> 00:09:00,240
 the idea of an entity.

108
00:09:00,240 --> 00:09:03,000
 So when you close your eyes and you think of people, and

109
00:09:03,000 --> 00:09:04,440
 when you don't see them, you

110
00:09:04,440 --> 00:09:12,560
 can give rise to the idea of the person in your mind.

111
00:09:12,560 --> 00:09:20,130
 And like I said, we cling to these entities because they're

112
00:09:20,130 --> 00:09:24,000
 illusions, because they're

113
00:09:24,000 --> 00:09:26,000
 only in the mind.

114
00:09:26,000 --> 00:09:30,600
 They don't have any bearing on actual reality.

115
00:09:30,600 --> 00:09:38,340
 The entity that we hold onto changes because it's made up

116
00:09:38,340 --> 00:09:43,040
 of the impermanent experiences.

117
00:09:43,040 --> 00:09:46,200
 As these experiences change, the entity changes.

118
00:09:46,200 --> 00:09:49,530
 So we become disappointed in people when they don't act

119
00:09:49,530 --> 00:09:52,640
 according to our image of them,

120
00:09:52,640 --> 00:09:58,930
 disappointed in the objects of experience when they break

121
00:09:58,930 --> 00:10:01,640
 or when they fall apart, when

122
00:10:01,640 --> 00:10:06,640
 they disappear, when they can't have them.

123
00:10:06,640 --> 00:10:08,640
 Clinging to people, places, things.

124
00:10:08,640 --> 00:10:12,400
 Obviously this is a big one where we cry when we lose

125
00:10:12,400 --> 00:10:14,960
 someone we love, when we're upset

126
00:10:14,960 --> 00:10:18,480
 when we're near someone who we don't like, because they're

127
00:10:18,480 --> 00:10:19,960
 afraid of certain people

128
00:10:19,960 --> 00:10:23,800
 they're sorry.

129
00:10:23,800 --> 00:10:24,800
 All of this is suffering.

130
00:10:24,800 --> 00:10:26,800
 We all study this.

131
00:10:26,800 --> 00:10:29,800
 We study how these things make us suffer.

132
00:10:29,800 --> 00:10:34,570
 And in the end, there's no entity that can truly satisfy us

133
00:10:34,570 --> 00:10:34,960
.

134
00:10:34,960 --> 00:10:37,080
 It comes and it goes.

135
00:10:37,080 --> 00:10:41,360
 They all come and go.

136
00:10:41,360 --> 00:10:46,270
 We study our thoughts, our judgments, what we think of

137
00:10:46,270 --> 00:10:49,280
 things, our partialities, our

138
00:10:49,280 --> 00:10:53,360
 likes and dislikes, things that we would say make us most

139
00:10:53,360 --> 00:10:55,600
 who we are, make us individuals,

140
00:10:55,600 --> 00:11:02,960
 our preferences, our personalities, our emotions.

141
00:11:02,960 --> 00:11:08,050
 Study all of these and we see that the likes and our disl

142
00:11:08,050 --> 00:11:10,640
ikes are not helping us.

143
00:11:10,640 --> 00:11:15,100
 It doesn't make us, it doesn't bring happiness to like

144
00:11:15,100 --> 00:11:18,640
 something, to want something, doesn't

145
00:11:18,640 --> 00:11:19,640
 make you happy.

146
00:11:19,640 --> 00:11:24,640
 Sort of the other way around, right?

147
00:11:24,640 --> 00:11:30,920
 It's a vicious cycle anyway.

148
00:11:30,920 --> 00:11:34,270
 You want something and oh yeah, you get what you want and

149
00:11:34,270 --> 00:11:36,440
 there's this pleasure associated

150
00:11:36,440 --> 00:11:39,440
 with it.

151
00:11:39,440 --> 00:11:44,480
 But it's an endless quest that is ever and ever less

152
00:11:44,480 --> 00:11:46,080
 fulfilling.

153
00:11:46,080 --> 00:11:50,170
 Until it doesn't fulfill and you have to jump to something

154
00:11:50,170 --> 00:11:52,520
 else and try to find fulfillment

155
00:11:52,520 --> 00:11:57,850
 which of course isn't the last eye there and so you're

156
00:11:57,850 --> 00:12:00,640
 jumping and jumping from one pleasure

157
00:12:00,640 --> 00:12:08,840
 search to the next.

158
00:12:08,840 --> 00:12:13,130
 Of course there's the weighing when we like something, we

159
00:12:13,130 --> 00:12:15,560
 are disinclined to things that

160
00:12:15,560 --> 00:12:21,640
 are not like it and so it breeds aversion to other things.

161
00:12:21,640 --> 00:12:28,000
 It's impossible to like everything.

162
00:12:28,000 --> 00:12:30,840
 A lot of it has to do with brain chemistry it seems.

163
00:12:30,840 --> 00:12:35,600
 Some people are more inclined to like lots of different

164
00:12:35,600 --> 00:12:36,600
 things.

165
00:12:36,600 --> 00:12:42,360
 It's only a temporary thing.

166
00:12:42,360 --> 00:12:45,640
 Like the more like you have, the more dislike you build up,

167
00:12:45,640 --> 00:12:47,440
 the more intolerance you build

168
00:12:47,440 --> 00:12:50,440
 up to other things.

169
00:12:50,440 --> 00:13:03,720
 Or to not having what you want.

170
00:13:03,720 --> 00:13:05,720
 All that changes as well.

171
00:13:05,720 --> 00:13:11,360
 You get what you want for a while and then you lose it.

172
00:13:11,360 --> 00:13:16,040
 Sometimes for many years you can be in a perpetual chase.

173
00:13:16,040 --> 00:13:20,800
 Always getting what you want for a whole lifetime.

174
00:13:20,800 --> 00:13:25,830
 There are angels or humans who can have great luck with

175
00:13:25,830 --> 00:13:27,840
 their preferences.

176
00:13:27,840 --> 00:13:31,730
 And be these sorts of people who always seem to get what

177
00:13:31,730 --> 00:13:32,760
 they want.

178
00:13:32,760 --> 00:13:35,360
 Live a life of positive thinking.

179
00:13:35,360 --> 00:13:42,160
 Thinking that all you have to do is think positively.

180
00:13:42,160 --> 00:13:45,600
 You talk to self-made, so quote unquote self-made

181
00:13:45,600 --> 00:13:48,080
 millionaires and billionaires.

182
00:13:48,080 --> 00:13:51,300
 They develop these philosophies that all you have to do is

183
00:13:51,300 --> 00:13:51,760
 try.

184
00:13:51,760 --> 00:13:55,800
 All you have to do is do what I did because it works.

185
00:13:55,800 --> 00:13:57,200
 I know because I did it.

186
00:13:57,200 --> 00:14:01,040
 But they don't see what happened behind the scenes.

187
00:14:01,040 --> 00:14:04,320
 Much of it had to do with past lives.

188
00:14:04,320 --> 00:14:05,320
 It was certainly deserved.

189
00:14:05,320 --> 00:14:08,760
 But I also don't see exactly what caused it.

190
00:14:08,760 --> 00:14:13,430
 And I don't realize that they're not perpetuating the cause

191
00:14:13,430 --> 00:14:13,560
.

192
00:14:13,560 --> 00:14:15,560
 Wondering for a rude awakening.

193
00:14:15,560 --> 00:14:24,560
 When they lose what they've gained, they're not able to get

194
00:14:24,560 --> 00:14:26,240
 it back.

195
00:14:26,240 --> 00:14:27,240
 So we study these thoughts.

196
00:14:27,240 --> 00:14:28,240
 We study our own thoughts.

197
00:14:28,240 --> 00:14:38,600
 The reason they are mental, mental, mental ecosystem.

198
00:14:38,600 --> 00:14:45,560
 There are many different things that grow in our minds.

199
00:14:45,560 --> 00:14:50,520
 We study and we see how suffering is caused here.

200
00:14:50,520 --> 00:14:51,520
 Not always.

201
00:14:51,520 --> 00:14:55,760
 Many of these are actually, many of the Sankars are

202
00:14:55,760 --> 00:14:58,160
 actually useful and lead us to towards

203
00:14:58,160 --> 00:14:59,160
 freedom.

204
00:14:59,160 --> 00:15:05,160
 But we see how suffering arises.

205
00:15:05,160 --> 00:15:08,360
 See what we're doing wrong.

206
00:15:08,360 --> 00:15:09,360
 That's more important.

207
00:15:09,360 --> 00:15:12,360
 I suppose equally important.

208
00:15:12,360 --> 00:15:15,000
 We focus on the good ones.

209
00:15:15,000 --> 00:15:17,360
 We learn what's useful.

210
00:15:17,360 --> 00:15:23,960
 We learn what is harmful.

211
00:15:23,960 --> 00:15:27,850
 Just by learning, just by studying, we incline towards what

212
00:15:27,850 --> 00:15:29,680
 is helpful and away from what

213
00:15:29,680 --> 00:15:35,680
 is harmful.

214
00:15:35,680 --> 00:15:36,680
 And finally we study consciousness.

215
00:15:36,680 --> 00:15:37,680
 Consciousness is the big one.

216
00:15:37,680 --> 00:15:38,680
 That's us.

217
00:15:38,680 --> 00:15:39,680
 That's what makes us real.

218
00:15:39,680 --> 00:15:40,680
 Makes us alive.

219
00:15:40,680 --> 00:15:50,680
 Without consciousness we would be computers.

220
00:15:50,680 --> 00:15:55,570
 Computers, brains that were working, operating like a

221
00:15:55,570 --> 00:15:59,120
 computer artificial intelligence that

222
00:15:59,120 --> 00:16:05,400
 appears to be thinking and playing chess and so on.

223
00:16:05,400 --> 00:16:06,800
 That has no clue what it's doing.

224
00:16:06,800 --> 00:16:07,800
 There's no awareness.

225
00:16:07,800 --> 00:16:13,430
 It's that awareness, that quailia, qualea, whatever you

226
00:16:13,430 --> 00:16:14,480
 call it.

227
00:16:14,480 --> 00:16:21,420
 It really has neuroscientists stumped or understand still

228
00:16:21,420 --> 00:16:24,600
 because you can't, no matter how much

229
00:16:24,600 --> 00:16:29,450
 you look at the brain and show how the brain works, you can

230
00:16:29,450 --> 00:16:32,160
't find the mind in the brain.

231
00:16:32,160 --> 00:16:36,560
 You can't find the experience in the brain.

232
00:16:36,560 --> 00:16:39,200
 You can't translate or no one knows how to translate.

233
00:16:39,200 --> 00:16:45,320
 It doesn't seem possible to translate physical into mental.

234
00:16:45,320 --> 00:16:49,850
 And so there's belief that the brain causes the mind to

235
00:16:49,850 --> 00:16:52,480
 exist and gives rise to the mind

236
00:16:52,480 --> 00:17:01,800
 but you can't translate cells and atoms into experience.

237
00:17:01,800 --> 00:17:04,760
 What we don't have to actually, this is a corner that

238
00:17:04,760 --> 00:17:07,080
 materialist scientists have painted

239
00:17:07,080 --> 00:17:11,750
 themselves into, physicalists who believe the mind to be a

240
00:17:11,750 --> 00:17:13,720
 product of the brain.

241
00:17:13,720 --> 00:17:17,490
 It's very easy in meditation to just bypass that and say, "

242
00:17:17,490 --> 00:17:19,640
Well, consciousness exists."

243
00:17:19,640 --> 00:17:23,000
 So just study it.

244
00:17:23,000 --> 00:17:24,000
 Study consciousness.

245
00:17:24,000 --> 00:17:29,260
 Why fool around with cells and atoms that are all

246
00:17:29,260 --> 00:17:31,840
 conceptual anyway.

247
00:17:31,840 --> 00:17:33,480
 So we study experience.

248
00:17:33,480 --> 00:17:42,730
 We study our consciousness and it's sort of the overarching

249
00:17:42,730 --> 00:17:46,720
 phenomenon that gives rise

250
00:17:46,720 --> 00:17:51,040
 to all of the other aggregates.

251
00:17:51,040 --> 00:17:55,030
 They're all dependent on consciousness, the mind that

252
00:17:55,030 --> 00:17:56,960
 allows it all to happen.

253
00:17:56,960 --> 00:18:02,400
 So the awareness, consciousness, [inaudible]

254
00:18:02,400 --> 00:18:03,800
 So we study that.

255
00:18:03,800 --> 00:18:07,760
 We study our awareness.

256
00:18:07,760 --> 00:18:10,880
 At first we think it's self, we think it's I.

257
00:18:10,880 --> 00:18:11,880
 It's an entity.

258
00:18:11,880 --> 00:18:15,770
 We start to see that even this consciousness is not really

259
00:18:15,770 --> 00:18:16,920
 under control.

260
00:18:16,920 --> 00:18:20,580
 I can't be conscious only of the things that I want to be

261
00:18:20,580 --> 00:18:21,800
 conscious of.

262
00:18:21,800 --> 00:18:25,100
 I can't keep myself from being conscious of unwholesome

263
00:18:25,100 --> 00:18:27,080
 things or unpleasant things.

264
00:18:27,080 --> 00:18:31,610
 I can't shut out sounds that I don't want to hear, sights

265
00:18:31,610 --> 00:18:33,760
 that I don't want to see, thoughts

266
00:18:33,760 --> 00:18:36,760
 that I don't want to have.

267
00:18:36,760 --> 00:18:39,990
 If consciousness were ours, if it was us who was conscious,

268
00:18:39,990 --> 00:18:41,280
 there was an "I" in the sense

269
00:18:41,280 --> 00:18:42,280
 of the self.

270
00:18:42,280 --> 00:18:45,440
 Then we could control who we could say, "Okay, no, no, I'm

271
00:18:45,440 --> 00:18:47,280
 not going to think about that.

272
00:18:47,280 --> 00:18:48,280
 Of course we do.

273
00:18:48,280 --> 00:18:53,160
 I'm not going to hear that, but of course we hear it."

274
00:18:53,160 --> 00:18:54,160
 Or don't.

275
00:18:54,160 --> 00:18:57,160
 We want to hear something and don't.

276
00:18:57,160 --> 00:18:58,160
 We want to see something and don't.

277
00:18:58,160 --> 00:19:02,920
 We want to remember something and don't, all this.

278
00:19:02,920 --> 00:19:03,920
 So we study that.

279
00:19:03,920 --> 00:19:07,040
 We study how none of these things can satisfy us.

280
00:19:07,040 --> 00:19:10,920
 And we start to let go of them.

281
00:19:10,920 --> 00:19:19,030
 Meditation is about freedom, freeing ourselves from this

282
00:19:19,030 --> 00:19:23,440
 constant search, constant quest

283
00:19:23,440 --> 00:19:28,440
 for satisfaction in that which cannot satisfy us.

284
00:19:28,440 --> 00:19:31,720
 That's the core practice in Buddhism.

285
00:19:31,720 --> 00:19:34,920
 So that's what we do when we practice the Four Satipatanas.

286
00:19:34,920 --> 00:19:38,480
 The Four Satipatanas are basically the five aggregates in

287
00:19:38,480 --> 00:19:39,600
 another form.

288
00:19:39,600 --> 00:19:41,840
 It's another way of talking about them.

289
00:19:41,840 --> 00:19:43,840
 So we have "gaya" as "rupa".

290
00:19:43,840 --> 00:19:46,320
 We focus on the body.

291
00:19:46,320 --> 00:19:48,880
 We're focusing on "rupa".

292
00:19:48,880 --> 00:19:49,880
 Feelings is feelings.

293
00:19:49,880 --> 00:19:51,880
 It's the same word.

294
00:19:51,880 --> 00:19:56,440
 "Tjita" is probably best understood as the same as "vidyana

295
00:19:56,440 --> 00:19:56,880
".

296
00:19:56,880 --> 00:20:04,820
 "Dhamma" focuses mainly on "sanyan" and "sankara", the

297
00:20:04,820 --> 00:20:09,160
 memories and the thoughts.

298
00:20:09,160 --> 00:20:12,160
 Mainly it's not an exact overlap.

299
00:20:12,160 --> 00:20:14,160
 It's a full overlap.

300
00:20:14,160 --> 00:20:19,320
 It's just not, it's a bit messy, especially with the Four

301
00:20:19,320 --> 00:20:20,880
 Satipatanas.

302
00:20:20,880 --> 00:20:21,880
 The Satipatanas are practical.

303
00:20:21,880 --> 00:20:24,240
 They're a way of approaching the five aggregates

304
00:20:24,240 --> 00:20:24,880
 practically.

305
00:20:24,880 --> 00:20:29,240
 So when you read what the Buddha taught about the Satipat

306
00:20:29,240 --> 00:20:32,160
anas, it was how to practice.

307
00:20:32,160 --> 00:20:38,880
 By practicing them, you're able to understand the five k

308
00:20:38,880 --> 00:20:40,320
handhas.

309
00:20:40,320 --> 00:20:41,320
 So there you are.

310
00:20:41,320 --> 00:20:47,970
 It should be encouragement for the practice of Satipatana,

311
00:20:47,970 --> 00:20:51,520
 insight through mindfulness.

312
00:20:51,520 --> 00:20:54,560
 And your understanding, your noble truth of suffering,

313
00:20:54,560 --> 00:20:56,520
 which is really, really important.

314
00:20:56,520 --> 00:20:59,720
 It's what allows us to be free from suffering.

315
00:20:59,720 --> 00:21:04,280
 When we see that we're causing ourselves suffering, we're

316
00:21:04,280 --> 00:21:07,320
 thereby able to escape, to be free from

317
00:21:07,320 --> 00:21:08,320
 it.

318
00:21:08,320 --> 00:21:11,960
 It's always a good thing.

319
00:21:11,960 --> 00:21:13,960
 So that's the Dhamma for tonight.

320
00:21:13,960 --> 00:21:15,960
 Thank you all for tuning in.

321
00:21:15,960 --> 00:21:15,960
 Be well.

