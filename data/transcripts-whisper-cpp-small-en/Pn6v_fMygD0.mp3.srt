1
00:00:00,000 --> 00:00:06,190
 So Nelson asked, "What is a good way to approach the parrot

2
00:00:06,190 --> 00:00:06,360
?

3
00:00:06,360 --> 00:00:10,270
 Much of what I have read speaks in terms of them bringing

4
00:00:10,270 --> 00:00:12,040
 good luck, etc.

5
00:00:12,040 --> 00:00:15,440
 Respectfully this seems to be superstitious to me.

6
00:00:15,440 --> 00:00:18,240
 Does their benefit lie only in the inspiration they can

7
00:00:18,240 --> 00:00:19,720
 give one to live better?"

8
00:00:19,720 --> 00:00:24,320
 No, I think they have power intrinsic in them.

9
00:00:24,320 --> 00:00:27,160
 Words have power, apparently.

10
00:00:27,160 --> 00:00:30,160
 That's possible with enough power to...

11
00:00:30,160 --> 00:00:34,900
 For example, the Buddha made a vow for his bowl to float

12
00:00:34,900 --> 00:00:37,160
 upstream, and apparently it

13
00:00:37,160 --> 00:00:38,160
 did.

14
00:00:38,160 --> 00:00:40,160
 I don't know.

15
00:00:40,160 --> 00:00:44,390
 I'm willing to be open to the fact that there's inherent

16
00:00:44,390 --> 00:00:45,880
 power in words.

17
00:00:45,880 --> 00:00:49,000
 Even physically you could say the vibrations of the words

18
00:00:49,000 --> 00:00:50,320
 have some power, but I think

19
00:00:50,320 --> 00:00:52,200
 mentally there's a lot more power.

20
00:00:52,200 --> 00:00:56,890
 If you think of how our minds can affect the physical in

21
00:00:56,890 --> 00:00:59,520
 many ways, I mean, it's of course

22
00:00:59,520 --> 00:01:04,410
 controversial, but you're talking to Buddhist monks, so

23
00:01:04,410 --> 00:01:06,480
 this is our take on it.

24
00:01:06,480 --> 00:01:08,560
 The mind is incredibly powerful.

25
00:01:08,560 --> 00:01:12,840
 The mind can make the body sick, and the mind can heal the

26
00:01:12,840 --> 00:01:13,560
 body.

27
00:01:13,560 --> 00:01:23,720
 The mind can...I've even been experienced some forms of

28
00:01:23,720 --> 00:01:24,400
 black magic when I was in Thailand.

29
00:01:24,400 --> 00:01:28,120
 That crazy lady, what?

30
00:01:28,120 --> 00:01:31,440
 Tham bhut dang crazy lady?

31
00:01:31,440 --> 00:01:33,910
 She was really angry at me one day, and she had told me

32
00:01:33,910 --> 00:01:35,460
 several times how it was really

33
00:01:35,460 --> 00:01:37,740
 interesting how when people did bad things to her, suddenly

34
00:01:37,740 --> 00:01:38,800
 horrible things happened

35
00:01:38,800 --> 00:01:39,800
 to them.

36
00:01:39,800 --> 00:01:43,440
 I can only assume what she's not telling me is that she's

37
00:01:43,440 --> 00:01:45,400
 going around using the power

38
00:01:45,400 --> 00:01:46,880
 of her mind against these people.

39
00:01:46,880 --> 00:01:49,470
 It was interesting because one time she got really angry at

40
00:01:49,470 --> 00:01:50,760
 me, and suddenly I had this

41
00:01:50,760 --> 00:01:53,400
 incredible pain in my neck.

42
00:01:53,400 --> 00:01:57,120
 I couldn't even straighten my neck.

43
00:01:57,120 --> 00:02:01,320
 I've never had that before, or I had it since.

44
00:02:01,320 --> 00:02:06,040
 For the whole day, for an entire day, I could barely

45
00:02:06,040 --> 00:02:08,200
 straighten my head.

46
00:02:08,200 --> 00:02:11,920
 We were in the car, and every bump was just incredible.

47
00:02:11,920 --> 00:02:17,040
 I could barely move my head.

48
00:02:17,040 --> 00:02:22,610
 Another example which corroborated this was there was a

49
00:02:22,610 --> 00:02:25,520
 monk who had a gun and so on.

50
00:02:25,520 --> 00:02:32,080
 Interesting monk, to be sure.

51
00:02:32,080 --> 00:02:37,520
 One of his arms was fairly paralyzed.

52
00:02:37,520 --> 00:02:41,690
 One day he got incredibly angry at me, or upset, or there

53
00:02:41,690 --> 00:02:43,920
 was a conflict between us.

54
00:02:43,920 --> 00:02:45,240
 It wasn't just that he was angry.

55
00:02:45,240 --> 00:02:49,480
 We were in conflict, and suddenly I couldn't move my arm.

56
00:02:49,480 --> 00:02:51,600
 The other arm, the opposite arm.

57
00:02:51,600 --> 00:02:57,040
 I couldn't even lift my arm up to pay respect.

58
00:02:57,040 --> 00:03:01,630
 When he came in, I had to hold my arm up as though I was a

59
00:03:01,630 --> 00:03:02,760
 cripple.

60
00:03:02,760 --> 00:03:04,240
 That's never happened.

61
00:03:04,240 --> 00:03:05,880
 That doesn't just happen.

62
00:03:05,880 --> 00:03:11,160
 That happened exactly when he got angry at me.

63
00:03:11,160 --> 00:03:14,840
 I'm willing to give the benefit of the doubt that the mind

64
00:03:14,840 --> 00:03:16,360
 can do crazy things.

65
00:03:16,360 --> 00:03:19,050
 In the end, the mind can ... People have out of body

66
00:03:19,050 --> 00:03:19,960
 experiences.

67
00:03:19,960 --> 00:03:22,590
 I talked about something similar that I had when I was

68
00:03:22,590 --> 00:03:23,120
 young.

69
00:03:23,120 --> 00:03:26,000
 I believe all of it.

70
00:03:26,000 --> 00:03:30,340
 I guarantee that these sorts of things exist with some

71
00:03:30,340 --> 00:03:33,320
 reasonable degree of certainty.

72
00:03:33,320 --> 00:03:39,080
 The mind can radically alter reality.

73
00:03:39,080 --> 00:03:42,600
 This as well has an important factor to play here.

74
00:03:42,600 --> 00:03:46,840
 If the paritas are done with a pure ... First of all, what

75
00:03:46,840 --> 00:03:47,840
 is a paritta?

76
00:03:47,840 --> 00:03:51,280
 A paritta is a protection chanting.

77
00:03:51,280 --> 00:03:54,890
 Chanting the Buddha's teaching, or some praise of the

78
00:03:54,890 --> 00:03:57,360
 Buddha, or other things as well.

79
00:03:57,360 --> 00:03:59,400
 Thinking that it has some sort of power.

80
00:03:59,400 --> 00:04:03,170
 As I said, the power can be physical, but it can also be

81
00:04:03,170 --> 00:04:04,000
 mental.

82
00:04:04,000 --> 00:04:05,000
 The factors.

83
00:04:05,000 --> 00:04:07,280
 There's a really good talk somewhere on the internet.

84
00:04:07,280 --> 00:04:11,060
 I have to try and find it where this ... I think it's a Bur

85
00:04:11,060 --> 00:04:11,960
mese monk.

86
00:04:11,960 --> 00:04:15,910
 I think actually it was Silananda, but I can't remember,

87
00:04:15,910 --> 00:04:18,080
 who talks about what is required

88
00:04:18,080 --> 00:04:21,040
 for a paritta to be effective.

89
00:04:21,040 --> 00:04:23,800
 The person who chants it has to understand it.

90
00:04:23,800 --> 00:04:27,400
 This is the commentary that explains.

91
00:04:27,400 --> 00:04:29,320
 The person who chants it has to understand it.

92
00:04:29,320 --> 00:04:31,240
 They have to chant it correctly.

93
00:04:31,240 --> 00:04:34,120
 The person who listens has to understand it.

94
00:04:34,120 --> 00:04:37,400
 I think there's many factors that contribute.

95
00:04:37,400 --> 00:04:39,860
 I wouldn't say that it's necessary for all of them to be in

96
00:04:39,860 --> 00:04:41,000
 place for it to have any

97
00:04:41,000 --> 00:04:44,990
 power, but the point is that there are many factors that

98
00:04:44,990 --> 00:04:47,080
 can cause it to have power.

99
00:04:47,080 --> 00:04:49,930
 Obviously, if you turn on the tape recording, this is what

100
00:04:49,930 --> 00:04:51,360
 they do here in Sri Lanka.

101
00:04:51,360 --> 00:04:54,010
 There was a man here who was really, really critical of

102
00:04:54,010 --> 00:04:54,520
 this.

103
00:04:54,520 --> 00:04:55,640
 He said, "Listen to these people.

104
00:04:55,640 --> 00:04:59,750
 They just turn on this chanting and expect that they don't

105
00:04:59,750 --> 00:05:01,280
 even listen to it.

106
00:05:01,280 --> 00:05:02,910
 They turn on their chanting and then they go about their

107
00:05:02,910 --> 00:05:03,400
 business."

108
00:05:03,400 --> 00:05:07,000
 He said, "Everywhere you hear these people with this

109
00:05:07,000 --> 00:05:09,640
 chanting on, but they don't actually

110
00:05:09,640 --> 00:05:12,460
 listen to it and they don't actually ... They're not

111
00:05:12,460 --> 00:05:15,160
 actually being mindful when they do it."

112
00:05:15,160 --> 00:05:18,630
 The idea is that the sound is somehow going to have some

113
00:05:18,630 --> 00:05:19,800
 kind of power.

114
00:05:19,800 --> 00:05:24,280
 If you have that kind of idea, I think still there can be a

115
00:05:24,280 --> 00:05:27,000
 physical power to the vibration

116
00:05:27,000 --> 00:05:30,460
 of the voice and so on, but very limited compared to the

117
00:05:30,460 --> 00:05:31,800
 power of the mind.

118
00:05:31,800 --> 00:05:36,270
 If you take one minute to listen to it and consider it and

119
00:05:36,270 --> 00:05:38,480
 give rise to faith in your

120
00:05:38,480 --> 00:05:41,000
 mind, the power of that can be exceptional.

121
00:05:41,000 --> 00:05:42,360
 It can change your life.

122
00:05:42,360 --> 00:05:48,000
 It could even bring you enlightenment if used in the right

123
00:05:48,000 --> 00:05:48,800
 way.

124
00:05:48,800 --> 00:05:51,000
 The same goes for the person chanting.

125
00:05:51,000 --> 00:05:54,560
 If it's actually a person chanting and they make a

126
00:05:54,560 --> 00:05:57,280
 determination, then a lot of great

127
00:05:57,280 --> 00:05:59,800
 things can happen.

128
00:05:59,800 --> 00:06:01,760
 I would go even further to say it's not just the Buddha's

129
00:06:01,760 --> 00:06:02,340
 teaching.

130
00:06:02,340 --> 00:06:05,640
 The reason why we use the Buddha's teaching is because it's

131
00:06:05,640 --> 00:06:07,120
 such powerful words.

132
00:06:07,120 --> 00:06:10,150
 It's enunciated very powerfully because, of course, the

133
00:06:10,150 --> 00:06:12,160
 Buddha was perfectly enlightened,

134
00:06:12,160 --> 00:06:16,960
 so his manner of speech was perfect.

135
00:06:16,960 --> 00:06:21,100
 The teachings, the meaning of it, this is what we call the 

136
00:06:21,100 --> 00:06:23,440
ātā and the ātā and bhyānjana.

137
00:06:23,440 --> 00:06:28,000
 Bhyānjana is the words and ātā is the meaning.

138
00:06:28,000 --> 00:06:34,840
 The words were wonderful and the meaning is also wonderful.

139
00:06:34,840 --> 00:06:38,670
 These two things make the Buddha's teaching incredibly

140
00:06:38,670 --> 00:06:39,480
 powerful.

141
00:06:39,480 --> 00:06:43,980
 Even just telling the truth, there's stories in the jātak

142
00:06:43,980 --> 00:06:46,560
as about there's a story of this

143
00:06:46,560 --> 00:06:58,080
 kid who was bitten by a snake and he was lying unconscious.

144
00:06:58,080 --> 00:07:01,360
 They all said, "What do we do?"

145
00:07:01,360 --> 00:07:03,910
 There was the mother, the father, and the hermit, this her

146
00:07:03,910 --> 00:07:05,120
mit, who was, I think, the

147
00:07:05,120 --> 00:07:06,880
 bodhisattva, I can't remember.

148
00:07:06,880 --> 00:07:11,340
 The bodhisattva suggests that they all say something

149
00:07:11,340 --> 00:07:12,440
 truthful.

150
00:07:12,440 --> 00:07:16,920
 The wife says, "Truth is, we've lived together for 50 years

151
00:07:16,920 --> 00:07:19,080
 and I never loved you," or something

152
00:07:19,080 --> 00:07:21,080
 like that.

153
00:07:21,080 --> 00:07:25,060
 Suddenly the kid rolls over and they're like, "It's working

154
00:07:25,060 --> 00:07:25,400
."

155
00:07:25,400 --> 00:07:28,750
 The husband says, "Well, the truth is I've been unfaithful

156
00:07:28,750 --> 00:07:30,080
 to you this whole time and

157
00:07:30,080 --> 00:07:32,040
 cheating on you with another woman."

158
00:07:32,040 --> 00:07:35,360
 The kid sits up all of a sudden.

159
00:07:35,360 --> 00:07:37,630
 The bodhisattva or the hermit says, "Well, the truth is I

160
00:07:37,630 --> 00:07:38,920
've been living as a monk and

161
00:07:38,920 --> 00:07:41,470
 I'm totally unsatisfied and I'm always thinking that I just

162
00:07:41,470 --> 00:07:42,720
 want to disrobe and go back to

163
00:07:42,720 --> 00:07:45,750
 the home life, but because I don't have any skills in the

164
00:07:45,750 --> 00:07:47,240
 world, I don't do it."

165
00:07:47,240 --> 00:07:54,320
 Suddenly the kid opens his eyes and is totally cured.

166
00:07:54,320 --> 00:07:55,320
 It's a funny story.

167
00:07:55,320 --> 00:07:57,160
 It's worth reading.

168
00:07:57,160 --> 00:07:59,000
 That concept, I think, has some merit.

169
00:07:59,000 --> 00:08:01,910
 The idea of just telling the truth because the power of the

170
00:08:01,910 --> 00:08:03,480
 truth, this is why the Buddha's

171
00:08:03,480 --> 00:08:05,600
 teaching is so powerful.

172
00:08:05,600 --> 00:08:10,810
 It has a power, a mental, a psychic power that can straight

173
00:08:10,810 --> 00:08:11,720
en out.

174
00:08:11,720 --> 00:08:15,320
 I don't know that it can cure a snake bite, but it

175
00:08:15,320 --> 00:08:18,120
 certainly has some sort of power.

176
00:08:18,120 --> 00:08:24,300
 I remember that we went a couple of months ago to a

177
00:08:24,300 --> 00:08:28,600
 hospital where the father of one

178
00:08:28,600 --> 00:08:35,720
 of Banté's friends was sick quite severely.

179
00:08:35,720 --> 00:08:44,350
 He was sleeping and under medication and not very conscious

180
00:08:44,350 --> 00:08:46,640
 at that time.

181
00:08:46,640 --> 00:08:51,110
 The daughter, the friend, invited us to do a chanting there

182
00:08:51,110 --> 00:08:53,000
, to do some paritas.

183
00:08:53,000 --> 00:08:54,760
 We went there.

184
00:08:54,760 --> 00:09:00,040
 For that, I really thought, kind of like Nelson, "Oh, this

185
00:09:00,040 --> 00:09:02,800
 sounds very superstitious.

186
00:09:02,800 --> 00:09:04,440
 It's kind of nice."

187
00:09:04,440 --> 00:09:12,190
 I kind of believed that there is something in, but I had no

188
00:09:12,190 --> 00:09:15,680
 proof of any real benefit

189
00:09:15,680 --> 00:09:16,680
 of a parata.

190
00:09:16,680 --> 00:09:18,800
 I never saw it working.

191
00:09:18,800 --> 00:09:22,680
 I never saw what it can do.

192
00:09:22,680 --> 00:09:29,230
 When we were sitting next to the man who was in pain and in

193
00:09:29,230 --> 00:09:32,640
 agony there in his bed, and

194
00:09:32,640 --> 00:09:40,550
 he was on life support machines, and there was a heart rate

195
00:09:40,550 --> 00:09:44,200
 meter, and his heart rate

196
00:09:44,200 --> 00:09:47,240
 was very high.

197
00:09:47,240 --> 00:09:52,140
 I don't understand about it, but I read something about 32

198
00:09:52,140 --> 00:09:53,800
 in the beginning.

199
00:09:53,800 --> 00:10:02,360
 Then we started chanting the first parata.

200
00:10:02,360 --> 00:10:09,490
 Within the first parata, the heart rate started to slow

201
00:10:09,490 --> 00:10:10,040
 down.

202
00:10:10,040 --> 00:10:17,540
 The man really stopped winding in his bed and the breath

203
00:10:17,540 --> 00:10:19,720
 slowed down.

204
00:10:19,720 --> 00:10:25,570
 I saw then, maybe in the second parata, I saw that the

205
00:10:25,570 --> 00:10:29,200
 number on the heart rate counter

206
00:10:29,200 --> 00:10:33,040
 went down to 18, whatever that means.

207
00:10:33,040 --> 00:10:35,560
 I saw the number 18.

208
00:10:35,560 --> 00:10:40,440
 He was really tranquil at that time.

209
00:10:40,440 --> 00:10:45,060
 Then in the end, when we were about to leave, when we

210
00:10:45,060 --> 00:10:47,680
 stopped, it went up a little bit to

211
00:10:47,680 --> 00:10:48,680
 20.

212
00:10:48,680 --> 00:10:55,960
 The last number I saw when we left was 22.

213
00:10:55,960 --> 00:11:05,370
 For me, that was really a proof that the words, the

214
00:11:05,370 --> 00:11:10,040
 chanting, does work.

215
00:11:10,040 --> 00:11:17,500
 I think that it comes from the intentions more than from

216
00:11:17,500 --> 00:11:22,600
 understanding the words, probably

217
00:11:22,600 --> 00:11:27,160
 from the intentions.

218
00:11:27,160 --> 00:11:35,700
 I could say something that sounds weird or even evil to

219
00:11:35,700 --> 00:11:40,120
 some people, but if I say it

220
00:11:40,120 --> 00:11:50,200
 with a loving heart, if you, for example, maybe a stupid

221
00:11:50,200 --> 00:11:54,040
 example, but if you say to

222
00:11:54,040 --> 00:12:02,400
 a dog something, a bad name, for example, you tell the dog

223
00:12:02,400 --> 00:12:06,040
 bad names, but do it with

224
00:12:06,040 --> 00:12:08,480
 a heart full of love for this dog.

225
00:12:08,480 --> 00:12:14,880
 It's not important what the words are for this dog.

226
00:12:14,880 --> 00:12:19,730
 What is important and what gets transported is the love

227
00:12:19,730 --> 00:12:22,080
 that you have for the dog.

228
00:12:22,080 --> 00:12:26,980
 The dog won't mind if you call it an idiot, if you say it

229
00:12:26,980 --> 00:12:28,680
 really loving.

230
00:12:28,680 --> 00:12:37,620
 Yes, but one but there is the meaning because you will know

231
00:12:37,620 --> 00:12:37,960
.

232
00:12:37,960 --> 00:12:40,780
 For a dog, of course, they love you and it was the sound of

233
00:12:40,780 --> 00:12:42,040
 your voice and you can be

234
00:12:42,040 --> 00:12:44,040
 loving.

235
00:12:44,040 --> 00:12:50,320
 No, I just think that there is another level beyond the ...

236
00:12:50,320 --> 00:12:51,120
 Because this is the same level

237
00:12:51,120 --> 00:12:53,000
 as the faith.

238
00:12:53,000 --> 00:12:56,350
 What I thought at first was this man was, this is only

239
00:12:56,350 --> 00:12:58,280
 because this man was Buddhist,

240
00:12:58,280 --> 00:13:01,830
 for example, or it's only because he had this thought arise

241
00:13:01,830 --> 00:13:04,840
, "Oh, they're chanting for me."

242
00:13:04,840 --> 00:13:07,840
 As a result, he became reassured.

243
00:13:07,840 --> 00:13:10,720
 We found out later that he was on medication, he was drunk.

244
00:13:10,720 --> 00:13:13,990
 I was like, "Oh," and then I thought, "Wow, that was

245
00:13:13,990 --> 00:13:16,040
 useless," because he didn't have

246
00:13:16,040 --> 00:13:17,480
 a clue what we were saying.

247
00:13:17,480 --> 00:13:22,720
 Then Palanyani said this, she noticed the meter.

248
00:13:22,720 --> 00:13:27,340
 The ... Really the vibrations and so on, but the truth of

249
00:13:27,340 --> 00:13:29,640
 what you're saying, I really

250
00:13:29,640 --> 00:13:33,760
 think that there is some psychic power as I was trying to

251
00:13:33,760 --> 00:13:35,960
 give these examples that there

252
00:13:35,960 --> 00:13:41,100
 is another level that can't be gained from ... That couldn

253
00:13:41,100 --> 00:13:42,840
't ... I would say couldn't

254
00:13:42,840 --> 00:13:47,300
 even be gained from other religious teachings like praising

255
00:13:47,300 --> 00:13:48,480
 God and so on.

256
00:13:48,480 --> 00:13:53,230
 They say you can heal by praising God or by calling ... Inv

257
00:13:53,230 --> 00:13:55,640
oking Jesus, you can heal other

258
00:13:55,640 --> 00:13:57,880
 people's wounds and so on.

259
00:13:57,880 --> 00:14:03,180
 But I think the Buddhist teaching has a greater power

260
00:14:03,180 --> 00:14:06,880
 because it's actually the truth.

261
00:14:06,880 --> 00:14:10,060
 As opposed to other religions, as opposed to things like

262
00:14:10,060 --> 00:14:11,880
 God, which we understand to be

263
00:14:11,880 --> 00:14:13,440
 not true.

264
00:14:13,440 --> 00:14:14,440
 Thank you.

