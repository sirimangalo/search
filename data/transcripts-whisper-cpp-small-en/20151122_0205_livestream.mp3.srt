1
00:00:00,000 --> 00:00:04,000
 How's this one, Bante?

2
00:00:04,000 --> 00:00:08,000
 We're broadcasting live.

3
00:00:08,000 --> 00:00:12,000
 So, good evening, everyone.

4
00:00:12,000 --> 00:00:16,000
 Welcome to our live broadcast.

5
00:00:16,000 --> 00:00:20,000
 November 21, 2015.

6
00:00:22,000 --> 00:00:26,000
 November 21, 2015.

7
00:00:26,000 --> 00:00:34,000
 Apparently, last night's Dhammapada video was too long.

8
00:00:34,000 --> 00:00:39,110
 I'm told that no one has time to sit through a 47-minute

9
00:00:39,110 --> 00:00:40,000
 video.

10
00:00:40,000 --> 00:00:24,490
 And normally, comments like this will go unanswered and

11
00:00:24,490 --> 00:00:46,000
 discussed.

12
00:00:46,000 --> 00:00:49,570
 I'm not reading all comments, so please don't expect me to

13
00:00:49,570 --> 00:00:51,000
 reply to your comments.

14
00:00:51,000 --> 00:00:54,700
 But I do read the first couple on each video just to make

15
00:00:54,700 --> 00:00:56,000
 sure the video got uploaded correctly

16
00:00:56,000 --> 00:00:58,000
 and no one's having problems.

17
00:00:58,000 --> 00:01:01,020
 Sometimes the first couple of comments tell me that the

18
00:01:01,020 --> 00:01:02,000
 audio was no good

19
00:01:02,000 --> 00:01:05,000
 or was missing or something like this or that.

20
00:01:05,000 --> 00:01:08,000
 But no, it was too long.

21
00:01:08,000 --> 00:01:11,340
 I just thought it interesting to comment because a 47-

22
00:01:11,340 --> 00:01:12,000
minute Dhammatak

23
00:01:12,000 --> 00:01:15,000
 is usually considered to be too short.

24
00:01:15,000 --> 00:01:17,880
 So if people are having trouble, and I know this is a thing

25
00:01:17,880 --> 00:01:19,000
, really, it's true.

26
00:01:19,000 --> 00:01:23,090
 YouTube actually tells you no one's going to watch a 47-

27
00:01:23,090 --> 00:01:24,000
minute video.

28
00:01:24,000 --> 00:01:27,000
 You shouldn't upload them. You should upload short videos.

29
00:01:27,000 --> 00:01:32,730
 When you learn about what YouTube has to say, I mean, they

30
00:01:32,730 --> 00:01:34,000
 know from experience.

31
00:01:34,000 --> 00:01:37,510
 I think I've even got a tool that lets me see how many

32
00:01:37,510 --> 00:01:40,000
 minutes of my video people watch on average.

33
00:01:40,000 --> 00:01:44,360
 It says out of a 47-minute video, average number of minutes

34
00:01:44,360 --> 00:01:48,000
 watched, it's probably about three.

35
00:01:48,000 --> 00:01:52,000
 I don't know. I don't use the tool, but I can check.

36
00:01:52,000 --> 00:01:57,180
 But it's a bit of a shame, really, because, as I said, and

37
00:01:57,180 --> 00:01:58,000
 for good reason,

38
00:01:58,000 --> 00:02:03,000
 Dhammatak's 47 minutes, that's short.

39
00:02:03,000 --> 00:02:06,000
 I've given a couple of hour Dhammatak, two hour Dhammatak,

40
00:02:06,000 --> 00:02:09,000
 once, I think.

41
00:02:09,000 --> 00:02:13,000
 People, after two hours, people start to complain a bit.

42
00:02:13,000 --> 00:02:17,000
 But two or three hours is not unheard of.

43
00:02:17,000 --> 00:02:21,370
 One hour is a pretty good length for a talk. That's usually

44
00:02:21,370 --> 00:02:23,000
 what we aim for.

45
00:02:23,000 --> 00:02:26,280
 Now, I know the Dhammatak videos have been shorter, and

46
00:02:26,280 --> 00:02:28,000
 some of the videos don't require it,

47
00:02:28,000 --> 00:02:32,000
 and I'm probably going more in-depth and more off-track.

48
00:02:32,000 --> 00:02:36,000
 And so I appreciate that. I will try to stay on track,

49
00:02:36,000 --> 00:02:40,000
 maybe a little more on track than before.

50
00:02:40,000 --> 00:02:46,340
 But, that being said, it shouldn't be the length, it should

51
00:02:46,340 --> 00:02:47,000
 be the content.

52
00:02:47,000 --> 00:02:52,000
 If the content is objectionable, then that's a problem.

53
00:02:52,000 --> 00:03:01,000
 Anyway, not a big deal, I just thought it was interesting.

54
00:03:01,000 --> 00:03:04,710
 We have a quote from the Buddha's Last Moments, that's

55
00:03:04,710 --> 00:03:07,000
 actually a really good quote.

56
00:03:07,000 --> 00:03:16,000
 It's one that Ajahn Tong brings up a lot.

57
00:03:16,000 --> 00:03:34,000
 More part of it, anyway.

58
00:03:34,000 --> 00:03:54,010
 "Yokoh Ananda Bhikkhuva Bhikkhu Niva Bhikkhuva Bhikkhu Niva

59
00:03:54,010 --> 00:03:57,000
 Upasakauva Upasikava

60
00:03:57,000 --> 00:04:05,600
 Dhammanudham ati paanau viharati sami cheep ati paanau anud

61
00:04:05,600 --> 00:04:07,000
hamachari

62
00:04:07,000 --> 00:04:16,140
 soddar hagatang sakkaroate karungaroate manate manate puje

63
00:04:16,140 --> 00:04:17,000
ite

64
00:04:17,000 --> 00:04:28,000
 There's a word that's not normally there.

65
00:04:28,000 --> 00:04:32,000
 This word isn't in all of the versions.

66
00:04:32,000 --> 00:04:37,000
 Apatyati paramaayapujaaya

67
00:04:37,000 --> 00:04:47,000
 Any monk, female or male, or lay disciple, male or female,

68
00:04:47,000 --> 00:04:52,880
 who practices the Dhamma in line with the Dhamma or leading

69
00:04:52,880 --> 00:04:54,000
 to the Dhamma,

70
00:04:54,000 --> 00:05:00,550
 who dwells practicing the Dhamma in order to attain the D

71
00:05:00,550 --> 00:05:02,000
hamma,

72
00:05:02,000 --> 00:05:09,000
 who practices properly.

73
00:05:09,000 --> 00:05:14,000
 Anudhamachari, one who fares according to the Dhamma,

74
00:05:14,000 --> 00:05:18,430
 soddar hagatang sakkaroate, such a person does right by the

75
00:05:18,430 --> 00:05:22,000
 Buddha, sakkaroate.

76
00:05:22,000 --> 00:05:28,450
 Garungaroate does, makes reverence to the Buddha, makes

77
00:05:28,450 --> 00:05:29,000
 homage.

78
00:05:29,000 --> 00:05:36,420
 Manate holds up high, holds in their mind, honors, thinks

79
00:05:36,420 --> 00:05:37,000
 highly of.

80
00:05:37,000 --> 00:05:42,000
 Pujeite, reverences.

81
00:05:42,000 --> 00:05:47,000
 Apatyati, no idea what apatyati is.

82
00:05:47,000 --> 00:05:50,000
 Not in all the versions.

83
00:05:50,000 --> 00:05:56,000
 Paramaayapujaaya, with the highest form of reverence.

84
00:05:56,000 --> 00:06:01,000
 Tasmatihananda, Dhamma, Anudhamma, Patipanna,

85
00:06:01,000 --> 00:06:07,000
 viharisamma, sami ji patipanna, anudhamachari no.

86
00:06:07,000 --> 00:06:14,770
 Therefore, Ananda, you all should dwell practicing the Dham

87
00:06:14,770 --> 00:06:20,000
ma in order to attain the Dhamma.

88
00:06:20,000 --> 00:06:25,340
 You should practice properly, should fare according to the

89
00:06:25,340 --> 00:06:26,000
 Dhamma,

90
00:06:26,000 --> 00:06:33,000
 lest you should train yourselves.

91
00:06:33,000 --> 00:06:38,000
 Ah yes, you have that actually, in English.

92
00:06:38,000 --> 00:06:57,000
 [Silence]

93
00:06:57,000 --> 00:07:03,000
 So, do we have any questions for tonight?

94
00:07:03,000 --> 00:07:05,000
 We do.

95
00:07:05,000 --> 00:07:08,530
 "Thinking about self and non-self, or what is, or what is

96
00:07:08,530 --> 00:07:09,000
 not,

97
00:07:09,000 --> 00:07:13,000
 I came to the idea that there is just happening.

98
00:07:13,000 --> 00:07:16,430
 Is this idea of just happening in line with Buddhist

99
00:07:16,430 --> 00:07:18,000
 teaching moments of happening?

100
00:07:18,000 --> 00:07:22,000
 Actions, acting upon actions?"

101
00:07:22,000 --> 00:07:25,000
 Yeah.

102
00:07:25,000 --> 00:07:29,900
 Yeah, I mean, that sort of insight is kind of the object

103
00:07:29,900 --> 00:07:32,000
ivity poking through.

104
00:07:32,000 --> 00:07:35,130
 It means you're starting to become more objective. That

105
00:07:35,130 --> 00:07:36,000
 sounds what it sounds like.

106
00:07:36,000 --> 00:07:39,000
 Don't hold on to it. That's a raft.

107
00:07:39,000 --> 00:07:42,760
 That raft changed your mind, so it got you across something

108
00:07:42,760 --> 00:07:43,000
.

109
00:07:43,000 --> 00:07:46,000
 Now throw the raft away and keep going.

110
00:07:46,000 --> 00:07:51,000
 [Silence]

111
00:07:51,000 --> 00:07:54,000
 Hello, Bhante. Can you talk about body scanning?

112
00:07:54,000 --> 00:07:57,000
 It should always be categorized as sanata, right?

113
00:07:57,000 --> 00:08:00,680
 Many people seem to associate practicing mindfulness with

114
00:08:00,680 --> 00:08:02,000
 doing body scanning,

115
00:08:02,000 --> 00:08:06,180
 or even associate the term vipassana with body scanning,

116
00:08:06,180 --> 00:08:08,000
 maybe due to goenka.

117
00:08:08,000 --> 00:08:11,570
 The more I have practiced insight meditation, the less I

118
00:08:11,570 --> 00:08:13,000
 have done body scanning.

119
00:08:13,000 --> 00:08:15,000
 Should I stop it completely?

120
00:08:15,000 --> 00:08:19,000
 I've noticed it's easy to get attached to the tranquility.

121
00:08:19,000 --> 00:08:21,450
 How to think about this and how to talk about it to the

122
00:08:21,450 --> 00:08:25,000
 people that have the practicing mindfulness

123
00:08:25,000 --> 00:08:27,000
 equals body scanning view.

124
00:08:27,000 --> 00:08:31,350
 If one wants to practice vipassana intensively and advance

125
00:08:31,350 --> 00:08:34,000
 one's understanding of reality,

126
00:08:34,000 --> 00:08:37,000
 is it counterproductive to do body scanning once in a while

127
00:08:37,000 --> 00:08:39,000
, or can it be useful somehow?

128
00:08:39,000 --> 00:08:43,000
 Sorry for the poor articulation. I'm tired or something.

129
00:08:43,000 --> 00:08:44,000
 Thank you.

130
00:08:44,000 --> 00:08:48,000
 Right.

131
00:08:48,000 --> 00:08:50,920
 Yeah, well, the word "body scanning," I mean, words are

132
00:08:50,920 --> 00:08:52,000
 really problematic

133
00:08:52,000 --> 00:08:57,000
 when people have catchphrases or labels for things.

134
00:08:57,000 --> 00:09:01,110
 Like our, for example, our technique is often talked about

135
00:09:01,110 --> 00:09:04,000
 as labeling or noting.

136
00:09:04,000 --> 00:09:07,320
 Acknowledging was a big one. Acknowledging. But

137
00:09:07,320 --> 00:09:11,000
 acknowledging is a problematic term.

138
00:09:11,000 --> 00:09:15,370
 Anyway, a little bit off track. The point being, body

139
00:09:15,370 --> 00:09:17,000
 scanning,

140
00:09:17,000 --> 00:09:19,000
 in order to understand it, we really have to break it down

141
00:09:19,000 --> 00:09:21,000
 and see what you're doing,

142
00:09:21,000 --> 00:09:25,000
 because body scanning is not, when you say body scanning,

143
00:09:25,000 --> 00:09:28,000
 it does not immediately mean that it's samatha.

144
00:09:28,000 --> 00:09:31,000
 No, I don't think that's fair to say.

145
00:09:31,000 --> 00:09:36,810
 As to whether it is proper meditation and whether it is vip

146
00:09:36,810 --> 00:09:38,000
assana.

147
00:09:38,000 --> 00:09:41,000
 Right, the question would be whether it's proper meditation

148
00:09:41,000 --> 00:09:41,000
,

149
00:09:41,000 --> 00:09:43,000
 because vipassana depends on the object.

150
00:09:43,000 --> 00:09:45,000
 Now, body scanning.

151
00:09:45,000 --> 00:09:56,410
 Body scanning could be problematic if it deals with a body,

152
00:09:56,410 --> 00:10:02,000
 like moving through the body.

153
00:10:02,000 --> 00:10:05,360
 First of all, I would say there's no reason to suggest that

154
00:10:05,360 --> 00:10:08,000
 one should practice in this way.

155
00:10:08,000 --> 00:10:13,640
 I can't think of. I can't think of a single, even say the w

156
00:10:13,640 --> 00:10:19,000
isudimaga, passage which encourages body scanning.

157
00:10:19,000 --> 00:10:23,460
 There may be, but I can't think of anything that hints at

158
00:10:23,460 --> 00:10:24,000
 it.

159
00:10:24,000 --> 00:10:29,640
 I can't even, I don't even, now maybe Lady Sayadaw says

160
00:10:29,640 --> 00:10:31,000
 something about this.

161
00:10:31,000 --> 00:10:34,000
 And maybe it is in the commentary somewhere.

162
00:10:34,000 --> 00:10:38,000
 But I would imagine that even Lady Sayadaw didn't.

163
00:10:38,000 --> 00:10:41,220
 I mean, that would be a good question. Did Lady Sayadaw,

164
00:10:41,220 --> 00:10:45,000
 who was supposed to be the teacher of the teacher of Goenka

165
00:10:45,000 --> 00:10:45,000
,

166
00:10:45,000 --> 00:10:49,000
 and Lady Sayadaw is usually who they go back to.

167
00:10:49,000 --> 00:10:51,610
 So the question would be whether Lady Sayadaw or Weibu Say

168
00:10:51,610 --> 00:10:55,000
adaw, who I think is another one, a student of Lady Sayadaw,

169
00:10:55,000 --> 00:10:58,670
 whether these guys talked about body scanning and where

170
00:10:58,670 --> 00:11:00,000
 they got it from.

171
00:11:00,000 --> 00:11:05,520
 So, I mean, that is something. When there's no long-

172
00:11:05,520 --> 00:11:08,850
standing tradition, or you can't bring it back to the Buddha

173
00:11:08,850 --> 00:11:09,000
,

174
00:11:09,000 --> 00:11:11,880
 you can't bring it back to the sort of things that the

175
00:11:11,880 --> 00:11:13,000
 Buddha taught.

176
00:11:13,000 --> 00:11:17,540
 If you contrast that with what we do, as I've been pointing

177
00:11:17,540 --> 00:11:20,000
 out, especially in our study of the wisudimaga,

178
00:11:20,000 --> 00:11:24,000
 there is very strong precedent for this kind of practice.

179
00:11:24,000 --> 00:11:29,590
 The wisudimaga, it literally, or it directly explains to

180
00:11:29,590 --> 00:11:32,000
 practice in this way.

181
00:11:32,000 --> 00:11:34,000
 This is how you practice meditation.

182
00:11:34,000 --> 00:11:37,790
 In the satipatthana sutta, the Buddha himself appears to be

183
00:11:37,790 --> 00:11:41,000
, at least it's subject to interpretation,

184
00:11:41,000 --> 00:11:45,000
 but the grammar itself supports directly saying to yourself

185
00:11:45,000 --> 00:11:50,000
 things like walking, walking, or angry, angry, or pain,

186
00:11:50,000 --> 00:11:53,000
 pain, or so on.

187
00:11:53,000 --> 00:11:58,000
 It's pretty easy to read that into the...

188
00:11:58,000 --> 00:12:00,440
 I am saying some people deny this. Some people say, "No,

189
00:12:00,440 --> 00:12:01,000
 that's not what it says,"

190
00:12:01,000 --> 00:12:03,270
 because it depends what you mean by the grammar, but it

191
00:12:03,270 --> 00:12:04,000
 literally says,

192
00:12:04,000 --> 00:12:08,260
 "Gachantova gachamiti bhajanati." Gachamiti means "I am

193
00:12:08,260 --> 00:12:10,000
 walking," and "ti" is a quote.

194
00:12:10,000 --> 00:12:13,570
 So it is a quote. You know, but the word, the verb is bhaj

195
00:12:13,570 --> 00:12:16,000
anati, which means "nose clearly."

196
00:12:16,000 --> 00:12:21,000
 So one knows clearly, quote-unquote, "I am walking."

197
00:12:21,000 --> 00:12:26,030
 So that, coupled with the tradition of how meditation was

198
00:12:26,030 --> 00:12:27,000
 practiced,

199
00:12:27,000 --> 00:12:30,810
 is a clear indication that what we're doing is not

200
00:12:30,810 --> 00:12:34,000
 something new or off the wall or far-fetched.

201
00:12:34,000 --> 00:12:38,840
 Now, body scanning, just as a precursor, I want to say that

202
00:12:38,840 --> 00:12:43,000
, that I don't think it's well supported by the text.

203
00:12:43,000 --> 00:12:46,630
 It could be wrong, and it would be interesting to talk to

204
00:12:46,630 --> 00:12:48,000
 them about that.

205
00:12:48,000 --> 00:12:50,450
 But that having been said, that's not that we shouldn't be

206
00:12:50,450 --> 00:12:51,000
 dogmatic,

207
00:12:51,000 --> 00:12:55,000
 so dogmatic as to discard it as a result of that.

208
00:12:55,000 --> 00:12:58,540
 But as I said, we have a bit of a problem because it may

209
00:12:58,540 --> 00:13:01,000
 encourage the concept of a body.

210
00:13:01,000 --> 00:13:09,970
 The other thing it may encourage is self, in the sense of

211
00:13:09,970 --> 00:13:13,000
 actively seeking out.

212
00:13:13,000 --> 00:13:16,290
 Now, there's a little bit of that in what we do, but we're

213
00:13:16,290 --> 00:13:17,000
 careful to...

214
00:13:17,000 --> 00:13:20,010
 It's a sensitive subject. It's something that we have to be

215
00:13:20,010 --> 00:13:21,000
 sensitive to,

216
00:13:21,000 --> 00:13:25,000
 that we're not actually forcing or seeking.

217
00:13:25,000 --> 00:13:27,520
 And so we tell you to focus on the stomach rising and

218
00:13:27,520 --> 00:13:28,000
 falling,

219
00:13:28,000 --> 00:13:31,320
 and you could argue that that pushing yourself to stay with

220
00:13:31,320 --> 00:13:33,000
 the stomach could be,

221
00:13:33,000 --> 00:13:38,580
 and it can become, somewhat forceful and based on concepts

222
00:13:38,580 --> 00:13:40,000
 of control.

223
00:13:40,000 --> 00:13:44,000
 But we're fairly careful not to let it become that.

224
00:13:44,000 --> 00:13:47,120
 We say that's where we start, but we're completely open to

225
00:13:47,120 --> 00:13:48,000
 letting the mind go

226
00:13:48,000 --> 00:13:51,000
 into what is often called choiceless awareness.

227
00:13:51,000 --> 00:13:56,000
 It is pretty choiceless. We're not exactly choosing.

228
00:13:56,000 --> 00:14:00,010
 Even though we do choose the stomach, we do it as a base

229
00:14:00,010 --> 00:14:03,000
 more than anything.

230
00:14:03,000 --> 00:14:06,000
 There's still that potential criticism.

231
00:14:06,000 --> 00:14:08,000
 And so I think some people would say,

232
00:14:08,000 --> 00:14:13,450
 "Well, if you're going to be totally... you're going to be

233
00:14:13,450 --> 00:14:14,000
 sincere about this,

234
00:14:14,000 --> 00:14:16,000
 it should be completely choiceless."

235
00:14:16,000 --> 00:14:18,000
 Of course, that's a problem, and that doesn't really work.

236
00:14:18,000 --> 00:14:19,000
 You can't do that.

237
00:14:19,000 --> 00:14:22,000
 And so as a result, we do use some control.

238
00:14:22,000 --> 00:14:25,000
 And I think the biggest argument for either practice,

239
00:14:25,000 --> 00:14:27,500
 whether it's the body scan or whether it's staying with the

240
00:14:27,500 --> 00:14:30,000
 stomach rising and falling,

241
00:14:30,000 --> 00:14:36,000
 is that you need to start from a point of control

242
00:14:36,000 --> 00:14:39,640
 in order to just get your feet, in order to just get

243
00:14:39,640 --> 00:14:42,000
 balanced enough to start to see,

244
00:14:42,000 --> 00:14:45,000
 and to see how control breaks down.

245
00:14:45,000 --> 00:14:51,000
 So it's not exactly control, but there is the potential.

246
00:14:51,000 --> 00:14:55,710
 And I think a good thing about the body scan is that it's

247
00:14:55,710 --> 00:14:58,000
 not partial.

248
00:14:58,000 --> 00:15:02,000
 So the idea is if you scan from head to feet,

249
00:15:02,000 --> 00:15:05,000
 you're not choosing this part or that part based on your

250
00:15:05,000 --> 00:15:05,000
 preference.

251
00:15:05,000 --> 00:15:07,440
 You're not going to certain things and avoiding other

252
00:15:07,440 --> 00:15:08,000
 things.

253
00:15:08,000 --> 00:15:11,000
 You're forced to go through the entire body, I think.

254
00:15:11,000 --> 00:15:15,000
 I've never practiced it. I think they do from head to feet.

255
00:15:15,000 --> 00:15:19,600
 But that being said, apart from those minor thoughts about

256
00:15:19,600 --> 00:15:20,000
 it,

257
00:15:20,000 --> 00:15:22,000
 I don't think about it too much.

258
00:15:22,000 --> 00:15:24,560
 And probably this is... probably I shouldn't have said even

259
00:15:24,560 --> 00:15:25,000
 that much

260
00:15:25,000 --> 00:15:27,000
 because it's not our technique.

261
00:15:27,000 --> 00:15:31,640
 And I don't like to talk about things that don't concern us

262
00:15:31,640 --> 00:15:32,000
.

263
00:15:32,000 --> 00:15:35,000
 But it allowed me to talk about our technique.

264
00:15:35,000 --> 00:15:38,000
 And I think rather than attack other people's techniques,

265
00:15:38,000 --> 00:15:40,770
 I would say that about our technique, that it's well

266
00:15:40,770 --> 00:15:42,000
 represented in the texts.

267
00:15:42,000 --> 00:15:45,000
 And so that's why we practice this way.

268
00:15:45,000 --> 00:15:53,250
 Is that everything? There was a lot of question as to what

269
00:15:53,250 --> 00:15:55,000
 you should do.

270
00:15:55,000 --> 00:15:58,000
 Should you stop it completely?

271
00:15:58,000 --> 00:16:00,000
 You should practice one way or the other.

272
00:16:00,000 --> 00:16:02,000
 If you're practicing our technique, practice our technique.

273
00:16:02,000 --> 00:16:03,560
 If you're practicing that technique, practice that

274
00:16:03,560 --> 00:16:04,000
 technique.

275
00:16:04,000 --> 00:16:06,000
 You shouldn't mix.

276
00:16:06,000 --> 00:16:09,520
 You shouldn't... because that becomes based on your partial

277
00:16:09,520 --> 00:16:10,000
ity.

278
00:16:10,000 --> 00:16:13,110
 Mixing, apart from being confusing, it's also usually based

279
00:16:13,110 --> 00:16:14,000
 on preference.

280
00:16:14,000 --> 00:16:17,000
 I like to do this sometimes and that sometimes.

281
00:16:17,000 --> 00:16:19,000
 So when I feel like doing this, I'll do this.

282
00:16:19,000 --> 00:16:21,000
 When I feel like doing that, I'll do that.

283
00:16:21,000 --> 00:16:23,000
 And that's hugely problematic.

284
00:16:29,000 --> 00:16:32,730
 What about the idea of talking about this to people who

285
00:16:32,730 --> 00:16:35,000
 have the body scanning view?

286
00:16:35,000 --> 00:16:37,000
 We're not about changing people's views.

287
00:16:37,000 --> 00:16:40,000
 If they have that view, then power to them.

288
00:16:40,000 --> 00:16:43,000
 If they're looking to change, then let them change.

289
00:16:43,000 --> 00:16:46,000
 We're not trying to convince the world of our practice.

290
00:16:46,000 --> 00:16:49,000
 Usually if people even want us to convince them, we say,

291
00:16:49,000 --> 00:16:52,000
 "You know, it's too much trouble for me."

292
00:16:52,000 --> 00:16:56,520
 I mean, that's what sort of the example we get from the A

293
00:16:56,520 --> 00:16:58,000
rahants in the texts,

294
00:16:58,000 --> 00:17:00,000
 but for the most part, they would be...

295
00:17:00,000 --> 00:17:02,300
 You know, teaching you would be too much trouble is the

296
00:17:02,300 --> 00:17:04,000
 kind of thing we would say.

297
00:17:04,000 --> 00:17:07,000
 It's important. It's important not to pander, you know?

298
00:17:07,000 --> 00:17:10,000
 We're not... and not to...

299
00:17:10,000 --> 00:17:15,000
 not to obsess over changing other people.

300
00:17:15,000 --> 00:17:20,350
 So look at yourself. At that point, you should look at your

301
00:17:20,350 --> 00:17:21,000
 own thoughts.

302
00:17:21,000 --> 00:17:23,000
 Do you understand this? Then okay.

303
00:17:23,000 --> 00:17:25,000
 Do it the way you understand it.

304
00:17:25,000 --> 00:17:28,000
 And then you ask yourself, "Maybe I'm doing it wrong."

305
00:17:28,000 --> 00:17:30,000
 That's what you have to figure out.

306
00:17:30,000 --> 00:17:33,000
 Everyone should look at their own feet when they walk.

307
00:17:33,000 --> 00:17:35,990
 Not literally. When we do walking meditation, don't look at

308
00:17:35,990 --> 00:17:37,000
 your feet.

309
00:17:37,000 --> 00:17:39,000
 And as the Buddha said, we watch our own path.

310
00:17:39,000 --> 00:17:44,000
 We don't worry about the footsteps of others.

311
00:17:44,000 --> 00:17:52,000
 Question about the meditator list.

312
00:17:52,000 --> 00:17:55,680
 Excuse me. What does the plus one and the number shown

313
00:17:55,680 --> 00:17:57,000
 between the two hands indicate?

314
00:17:57,000 --> 00:18:00,290
 Mine has changed from five to two and fairly new to the

315
00:18:00,290 --> 00:18:01,000
 layout.

316
00:18:01,000 --> 00:18:03,000
 I don't think that's possible.

317
00:18:03,000 --> 00:18:06,000
 Unless you're using the Android app, it does weird things.

318
00:18:06,000 --> 00:18:08,000
 I don't know how to fix it.

319
00:18:08,000 --> 00:18:12,000
 Yeah, the Android app sometimes shows it in green,

320
00:18:12,000 --> 00:18:15,000
 meaning that you've already clicked on it when you have it.

321
00:18:15,000 --> 00:18:17,000
 So that's a little strange.

322
00:18:17,000 --> 00:18:19,540
 If you don't move the list, it fixes itself on the next

323
00:18:19,540 --> 00:18:20,000
 update,

324
00:18:20,000 --> 00:18:24,000
 but if you scroll up and down, something goes kind of funny

325
00:18:24,000 --> 00:18:24,000
.

326
00:18:24,000 --> 00:18:27,830
 Because there's something wrong with it. It's going to be

327
00:18:27,830 --> 00:18:29,000
 fixed.

328
00:18:29,000 --> 00:18:32,000
 You need someone who's smart.

329
00:18:32,000 --> 00:18:38,610
 The plus one means you've done something good in Hanamond

330
00:18:38,610 --> 00:18:39,000
ana.

331
00:18:39,000 --> 00:18:42,000
 It's a number of likes. It's likes.

332
00:18:42,000 --> 00:18:46,000
 It's likes. It's like a Facebook like.

333
00:18:47,000 --> 00:18:51,000
 So it should... I don't think they can go down, but...

334
00:18:51,000 --> 00:18:53,000
 Who knows, huh?

335
00:18:53,000 --> 00:18:57,000
 Yeah. Technical difficulties.

336
00:18:57,000 --> 00:19:00,000
 On weekends, I try to practice the egg precepts.

337
00:19:00,000 --> 00:19:03,300
 I've noticed strong craving, but there doesn't seem to be

338
00:19:03,300 --> 00:19:05,000
 any object for the craving.

339
00:19:05,000 --> 00:19:08,000
 It seems like the mind is just hungry.

340
00:19:08,000 --> 00:19:10,840
 I can watch this hungry mind for only so long, then I end

341
00:19:10,840 --> 00:19:12,000
 up feeding it.

342
00:19:12,000 --> 00:19:15,100
 Is the idea to feed it but in the most wholesome way

343
00:19:15,100 --> 00:19:16,000
 possible?

344
00:19:17,000 --> 00:19:22,570
 I think by feeding it, you're talking sort of a figurative

345
00:19:22,570 --> 00:19:24,000
 feeding.

346
00:19:24,000 --> 00:19:26,000
 I hope.

347
00:19:26,000 --> 00:19:29,000
 Talking about feeding the mind, right?

348
00:19:29,000 --> 00:19:35,890
 I mean, the best is if you can learn to overcome and let go

349
00:19:35,890 --> 00:19:37,000
 of it.

350
00:19:37,000 --> 00:19:40,840
 But yeah, if you're right, so you're trying to keep the

351
00:19:40,840 --> 00:19:42,000
 eight precepts

352
00:19:42,000 --> 00:19:46,110
 and then you end up breaking the eight precepts maybe by

353
00:19:46,110 --> 00:19:47,000
 listening to music or something

354
00:19:47,000 --> 00:19:50,000
 or watching a movie or something.

355
00:19:50,000 --> 00:19:52,460
 I mean, there are ways to do it without breaking the

356
00:19:52,460 --> 00:19:53,000
 precepts.

357
00:19:53,000 --> 00:19:56,430
 It would just sort of distract you, but it's kind of beside

358
00:19:56,430 --> 00:19:57,000
 the point.

359
00:19:57,000 --> 00:20:00,000
 In the end, you have to learn to overcome it.

360
00:20:00,000 --> 00:20:02,980
 If you're keeping the eight precepts, you should keep the

361
00:20:02,980 --> 00:20:04,000
 eight precepts.

362
00:20:04,000 --> 00:20:06,000
 It's only temporary.

363
00:20:06,000 --> 00:20:09,450
 So you learn... it's teaching you self-control if nothing

364
00:20:09,450 --> 00:20:10,000
 else.

365
00:20:10,000 --> 00:20:12,890
 But that's part of the reason for keeping the eight precept

366
00:20:12,890 --> 00:20:13,000
s

367
00:20:13,000 --> 00:20:16,000
 is to learn about your desires and to have a chance

368
00:20:16,000 --> 00:20:24,360
 because if you constantly have the ability to indulge, to

369
00:20:24,360 --> 00:20:27,000
 satisfy your desires,

370
00:20:27,000 --> 00:20:29,000
 you'll never get to see what this desire is like.

371
00:20:29,000 --> 00:20:33,000
 You'll never really get to understand what desire is.

372
00:20:33,000 --> 00:20:36,170
 You'll never have a chance to challenge this idea that

373
00:20:36,170 --> 00:20:37,000
 desire is worth having

374
00:20:37,000 --> 00:20:41,340
 and that desire is a sign that you should chase after what

375
00:20:41,340 --> 00:20:43,000
 you're looking for.

376
00:20:43,000 --> 00:20:47,000
 So as you watch the desire without indulging in it,

377
00:20:47,000 --> 00:20:50,520
 you're able to see a middle way, a way of just being with

378
00:20:50,520 --> 00:20:51,000
 desire

379
00:20:51,000 --> 00:20:54,000
 without acting on it or without repressing it.

380
00:20:54,000 --> 00:20:57,000
 That's what we're looking for.

381
00:20:57,000 --> 00:20:59,430
 So what you're talking about is not the idea. The idea is

382
00:20:59,430 --> 00:21:00,000
 not to feed it.

383
00:21:00,000 --> 00:21:04,000
 But if you feed it, then just be as mindful as you can.

384
00:21:04,000 --> 00:21:12,440
 Of course, if you're keeping eight precepts, you shouldn't

385
00:21:12,440 --> 00:21:14,000
 feed it.

386
00:21:14,000 --> 00:21:17,000
 Hello, Pante. Could you please tell whether it is true

387
00:21:17,000 --> 00:21:19,000
 that one has to cycle through the insight knowledges

388
00:21:19,000 --> 00:21:22,320
 many, many times after third path before actually arriving

389
00:21:22,320 --> 00:21:24,000
 at fourth path?

390
00:21:24,000 --> 00:21:27,000
 Or does one only have to get through one more progress of

391
00:21:27,000 --> 00:21:27,000
 insight cycle

392
00:21:27,000 --> 00:21:32,000
 after third path to attain fourth path? Thanks.

393
00:21:32,000 --> 00:21:37,000
 It only requires one cycle, but that's technically.

394
00:21:37,000 --> 00:21:41,000
 Realistically, it usually requires many.

395
00:21:41,000 --> 00:21:44,000
 But there's two different cycles.

396
00:21:44,000 --> 00:21:47,000
 One cycle is only going to be a review,

397
00:21:47,000 --> 00:21:53,000
 so it's not going to be all 16 stages of knowledge.

398
00:21:53,000 --> 00:21:57,730
 You can actually only go through the stages of knowledge

399
00:21:57,730 --> 00:21:59,000
 four times.

400
00:21:59,000 --> 00:22:02,000
 So you're not actually going through all the stages of

401
00:22:02,000 --> 00:22:02,000
 knowledge,

402
00:22:02,000 --> 00:22:06,060
 not technically, because it's missing one until you reach

403
00:22:06,060 --> 00:22:07,000
 the next path.

404
00:22:07,000 --> 00:22:11,000
 But I think that's really a technicality.

405
00:22:11,000 --> 00:22:14,680
 Every time you go through the knowledges, you reduce the

406
00:22:14,680 --> 00:22:15,000
 number of...

407
00:22:15,000 --> 00:22:17,000
 you reduce your defilements.

408
00:22:17,000 --> 00:22:20,000
 So they get cut off piece by piece by piece.

409
00:22:20,000 --> 00:22:23,000
 When you reach a certain point, that's called anagami.

410
00:22:23,000 --> 00:22:25,000
 And you reach another... when you reach the final point

411
00:22:25,000 --> 00:22:43,000
 where there's none left, that's arahant.

412
00:22:43,000 --> 00:22:46,000
 There is one more.

413
00:22:46,000 --> 00:22:48,420
 Could you please talk about how an anagami progresses to a

414
00:22:48,420 --> 00:22:50,000
rahantship?

415
00:22:50,000 --> 00:22:55,000
 I am... at high equanimity after third path.

416
00:22:55,000 --> 00:23:00,000
 Any advice? I got frustrated after practicing a long time.

417
00:23:00,000 --> 00:23:06,000
 Advice. Here's a person with yellow... orange...

418
00:23:06,000 --> 00:23:09,000
 who hasn't done any meditation with us.

419
00:23:09,000 --> 00:23:11,000
 Anagamis don't get frustrated.

420
00:23:11,000 --> 00:23:13,000
 I'm not going to answer your question.

421
00:23:13,000 --> 00:23:15,000
 If you start meditating with us,

422
00:23:15,000 --> 00:23:17,470
 I think I'm not going to answer these sorts of questions as

423
00:23:17,470 --> 00:23:18,000
 either.

424
00:23:18,000 --> 00:23:21,080
 If you want to meditate with us, you should start logging

425
00:23:21,080 --> 00:23:22,000
 your meditation.

426
00:23:22,000 --> 00:23:24,380
 Read my booklet. Start practicing according to our

427
00:23:24,380 --> 00:23:25,000
 technique.

428
00:23:25,000 --> 00:23:27,000
 Start logging your meditation.

429
00:23:27,000 --> 00:23:29,330
 And if you have questions about your meditation, I'm happy

430
00:23:29,330 --> 00:23:30,000
 to answer them.

431
00:23:30,000 --> 00:23:33,000
 But I'm not going to talk about these.

432
00:23:33,000 --> 00:23:36,400
 Because it's not something I can verify, and from the

433
00:23:36,400 --> 00:23:37,000
 sounds of it,

434
00:23:37,000 --> 00:23:46,000
 there's a misunderstanding of what an anagami is.

435
00:23:46,000 --> 00:23:50,000
 I'm sorry.

436
00:23:50,000 --> 00:23:52,000
 And with that, we are caught up.

437
00:23:52,000 --> 00:23:55,390
 Nobody clicks on people's hands anymore, do they? I used to

438
00:23:55,390 --> 00:23:56,000
.

439
00:23:56,000 --> 00:23:58,000
 I do sometimes.

440
00:23:58,000 --> 00:24:06,000
 There's a lot of them. Click, click, click.

441
00:24:06,000 --> 00:24:10,000
 What we don't do very often is, up on the top part,

442
00:24:10,000 --> 00:24:15,000
 click to like questions and comments and things.

443
00:24:15,000 --> 00:24:36,000
 And I forget to do that.

444
00:24:36,000 --> 00:24:39,580
 Okay, that's all then for tonight. Thank you all for tuning

445
00:24:39,580 --> 00:24:40,000
 in.

446
00:24:40,000 --> 00:24:41,000
 Have a good night.

447
00:24:41,000 --> 00:24:45,000
 Thank you. Thank you, Bhante. Good night.

448
00:24:46,000 --> 00:24:48,000
 Thank you.

449
00:24:49,000 --> 00:24:51,000
 Thank you.

450
00:24:52,000 --> 00:24:54,000
 Thank you.

451
00:24:55,000 --> 00:24:57,000
 Thank you.

452
00:24:58,000 --> 00:25:00,000
 Thank you.

453
00:25:01,000 --> 00:25:03,000
 Thank you.

454
00:25:03,000 --> 00:25:05,000
 Thank you.

455
00:25:05,000 --> 00:25:07,000
 Thank you.

456
00:25:08,000 --> 00:25:10,000
 Thank you.

457
00:25:11,000 --> 00:25:13,000
 Thank you.

458
00:25:13,000 --> 00:25:15,000
 Thank you.

459
00:25:15,000 --> 00:25:17,000
 Thank you.

460
00:25:19,000 --> 00:25:21,000
 Thank you.

461
00:25:22,000 --> 00:25:24,000
 Thank you.

462
00:25:24,000 --> 00:25:26,000
 Thank you.

