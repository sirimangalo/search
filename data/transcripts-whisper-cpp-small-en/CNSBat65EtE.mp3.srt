1
00:00:00,000 --> 00:00:04,740
 Okay, Ian asks, "Are you familiar with astral projection?

2
00:00:04,740 --> 00:00:06,400
 If so, what are your views on it?"

3
00:00:06,400 --> 00:00:14,090
 Well, I get a lot of questions like this and I, some of

4
00:00:14,090 --> 00:00:17,320
 them I don't answer, but astral projection in particular is

5
00:00:17,320 --> 00:00:20,040
 one that I

6
00:00:20,040 --> 00:00:23,480
 can comment on because when I was 13, I

7
00:00:24,440 --> 00:00:28,320
 did some sort of astral projection and this is part of why

8
00:00:28,320 --> 00:00:30,720
 it's really interesting for me to

9
00:00:30,720 --> 00:00:33,400
 think about doing a

10
00:00:33,400 --> 00:00:35,860
 video for children on meditation because a lot of

11
00:00:35,860 --> 00:00:37,980
 interesting things would come from it.

12
00:00:37,980 --> 00:00:43,760
 When I was 13, I was doing this meditation where I would

13
00:00:43,760 --> 00:00:45,120
 lie in bed and

14
00:00:45,120 --> 00:00:51,940
 I would rock the bed back and forth like a hammock in my

15
00:00:51,940 --> 00:00:52,520
 mind.

16
00:00:53,560 --> 00:00:56,120
 If any of you are familiar with astral projection, you

17
00:00:56,120 --> 00:00:59,120
 probably know where this is going to go and can understand

18
00:00:59,120 --> 00:01:00,800
 how it went the way it did

19
00:01:00,800 --> 00:01:05,810
 because they don't use this technique, but they use this

20
00:01:05,810 --> 00:01:06,760
 sort of technique.

21
00:01:06,760 --> 00:01:10,300
 So, and eventually I could get to the point where my

22
00:01:10,300 --> 00:01:14,690
 the whole bed was rocking like a hammock and it felt, it

23
00:01:14,690 --> 00:01:16,040
 really felt like it and

24
00:01:16,040 --> 00:01:18,620
 then I could get it to the point where the bed would do

25
00:01:18,620 --> 00:01:18,960
 loops.

26
00:01:19,920 --> 00:01:24,790
 I would be lying in bed and the whole bed would be doing

27
00:01:24,790 --> 00:01:26,000
 loops like a hammock.

28
00:01:26,000 --> 00:01:28,390
 That's why I was in a hammock, but doing loops and I go

29
00:01:28,390 --> 00:01:29,320
 back and forth.

30
00:01:29,320 --> 00:01:34,110
 I did this day after day. The reason I was doing it, I hadn

31
00:01:34,110 --> 00:01:37,280
't learned it from anywhere, but it felt good and it made me

32
00:01:37,280 --> 00:01:37,560
 feel

33
00:01:37,560 --> 00:01:40,080
 calm and

34
00:01:40,080 --> 00:01:44,200
 it was an antidote for all of the suffering that I felt.

35
00:01:46,800 --> 00:01:49,840
 Suffering at school and unstressed as well from school,

36
00:01:49,840 --> 00:01:53,480
 but just general depression

37
00:01:53,480 --> 00:01:57,120
 being a teenager.

38
00:01:57,120 --> 00:02:00,800
 Love said.

39
00:02:00,800 --> 00:02:07,930
 And I did this for quite some time, got good at it and then

40
00:02:07,930 --> 00:02:10,600
 at one point I started doing it from head to toe

41
00:02:10,600 --> 00:02:14,760
 where I would, it would be like a hammock, but going the

42
00:02:14,760 --> 00:02:15,280
 other way.

43
00:02:15,280 --> 00:02:19,910
 So going to up at my toes and then up at my head and

44
00:02:19,910 --> 00:02:21,280
 swinging in this direction.

45
00:02:21,280 --> 00:02:25,970
 And then one day it happened that my mind popped out of my

46
00:02:25,970 --> 00:02:26,360
 body,

47
00:02:26,360 --> 00:02:28,920
 popped out of my head, just whoop.

48
00:02:28,920 --> 00:02:33,730
 And you know, it's so long ago that it's, I'm just, because

49
00:02:33,730 --> 00:02:36,000
 I repeat this again and again, I'm able to remember it.

50
00:02:36,000 --> 00:02:38,320
 But I

51
00:02:38,320 --> 00:02:40,480
 kind of get the feeling back now of what it was like.

52
00:02:44,320 --> 00:02:48,150
 And I floated, I was upstairs, I floated downstairs and my

53
00:02:48,150 --> 00:02:51,760
 younger brother was pouring a bowl of cereal and

54
00:02:51,760 --> 00:02:55,040
 I

55
00:02:55,040 --> 00:02:58,470
 watched him pour the bowl of cereal, pour the milk into the

56
00:02:58,470 --> 00:03:01,880
 cereal and then he was turning to

57
00:03:01,880 --> 00:03:05,600
 put the cereal away or something like that,

58
00:03:05,600 --> 00:03:10,000
 put something away and his elbow or the milk or something

59
00:03:10,000 --> 00:03:11,400
 hit the bowl of cereal and

60
00:03:11,640 --> 00:03:14,400
 knocked it on the floor and smashed the bowl into pieces

61
00:03:14,400 --> 00:03:16,160
 with the cereal going everywhere.

62
00:03:16,160 --> 00:03:22,850
 And I saw this and after seeing it, I floated back upstairs

63
00:03:22,850 --> 00:03:23,960
 and back into my head,

64
00:03:23,960 --> 00:03:26,760
 back into my body and

65
00:03:26,760 --> 00:03:30,100
 then I got up and you know kind of like, wow, that was a

66
00:03:30,100 --> 00:03:32,520
 bit weird, went downstairs and

67
00:03:32,520 --> 00:03:35,720
 saw my brother pour a bowl of cereal,

68
00:03:36,840 --> 00:03:39,420
 turned to put the cereal away and knocked the bowl of

69
00:03:39,420 --> 00:03:41,560
 cereal on the floor smashing it into pieces.

70
00:03:41,560 --> 00:03:48,450
 So, so not only was it astral projection, but it was, I

71
00:03:48,450 --> 00:03:51,320
 think that's what the term astral projection is.

72
00:03:51,320 --> 00:03:53,790
 I might be totally off base actually, you might be talking

73
00:03:53,790 --> 00:03:54,880
 about something else.

74
00:03:54,880 --> 00:03:59,400
 We, we, I think nowadays they refer to it as out of body

75
00:03:59,400 --> 00:04:02,120
 experiences or OOBE.

76
00:04:02,120 --> 00:04:05,560
 There's a, this, there's this

77
00:04:06,080 --> 00:04:08,080
 group of researchers in

78
00:04:08,080 --> 00:04:12,760
 University of Georgia based on the work of Ian Stevenson

79
00:04:12,760 --> 00:04:14,800
 and they study this kind of thing.

80
00:04:14,800 --> 00:04:21,240
 Yeah, but not only was it an out of body experience, but it

81
00:04:21,240 --> 00:04:22,280
 was also a,

82
00:04:22,280 --> 00:04:26,560
 what do you call it,

83
00:04:26,560 --> 00:04:28,920
 seeing into the future.

84
00:04:28,920 --> 00:04:34,840
 It was such a banal experience, but or subject.

85
00:04:36,520 --> 00:04:39,460
 But that was my one experience of something like astral

86
00:04:39,460 --> 00:04:41,160
 projection or so on.

87
00:04:41,160 --> 00:04:44,790
 So I believe it's possible. It seems, it seems quite

88
00:04:44,790 --> 00:04:48,120
 logical and quite reasonable, quite simple.

89
00:04:48,120 --> 00:04:51,450
 And I could, if you want, you could train in it. If you're

90
00:04:51,450 --> 00:04:54,240
 interested, do the sort of thing that I did and

91
00:04:54,240 --> 00:04:58,360
 take weeks or months to train like that and

92
00:04:58,360 --> 00:05:01,720
 see what happens. There are easier ways. There are

93
00:05:01,720 --> 00:05:03,440
 documented ways where you,

94
00:05:03,960 --> 00:05:07,520
 you, I think they also recommend lying down. It's funny,

95
00:05:07,520 --> 00:05:09,670
 I must have done it in the past life or something because

96
00:05:09,670 --> 00:05:11,800
 it just came, I didn't read about it at all.

97
00:05:11,800 --> 00:05:14,240
 They talk about lying down and

98
00:05:14,240 --> 00:05:18,090
 something about moving your mind, getting your mind out of

99
00:05:18,090 --> 00:05:20,530
 your body. Anyway, they have, there are techniques, I think

100
00:05:20,530 --> 00:05:21,680
, on the internet even about it.

101
00:05:21,680 --> 00:05:25,920
 So I don't have many views on it, but, but the other thing

102
00:05:25,920 --> 00:05:27,920
 I wanted to say is, is what are my views on it?

103
00:05:27,920 --> 00:05:29,960
 Is that, these are obviously not

104
00:05:30,560 --> 00:05:34,080
 what I teach and not what I'm interested in and, and in

105
00:05:34,080 --> 00:05:36,040
 general, what, not what Buddhist

106
00:05:36,040 --> 00:05:39,670
 teachers and practitioners are interested in. That's why I

107
00:05:39,670 --> 00:05:42,660
 don't generally answer these sorts of questions when they

108
00:05:42,660 --> 00:05:43,160
 come up.

109
00:05:43,160 --> 00:05:45,920
 Questions about

110
00:05:45,920 --> 00:05:50,120
 the third eye and the chakras and so on because

111
00:05:50,120 --> 00:05:53,600
 they're really beside the point. The Buddha taught four

112
00:05:53,600 --> 00:05:54,000
 things.

113
00:05:54,000 --> 00:05:56,480
 He taught the cause of, he taught suffering, the cause of

114
00:05:56,480 --> 00:05:56,920
 suffering,

115
00:05:57,400 --> 00:06:00,450
 the cessation of suffering and the path which leads to the

116
00:06:00,450 --> 00:06:01,880
 cessation of suffering and

117
00:06:01,880 --> 00:06:06,920
 this astral projection is not in one of those. If anything,

118
00:06:06,920 --> 00:06:08,990
 it's the truth of suffering because it's not going to

119
00:06:08,990 --> 00:06:09,920
 satisfy you.

120
00:06:09,920 --> 00:06:12,970
 It's not going to bring true peace and happiness. It's not

121
00:06:12,970 --> 00:06:15,520
 going to bring wisdom and understanding.

122
00:06:15,520 --> 00:06:21,100
 All it does is make you feel good and, and give you

123
00:06:21,100 --> 00:06:23,440
 something to go, hmm, that's neat.

124
00:06:25,280 --> 00:06:27,280
 Okay, so thanks for the question.

