 Today I will
 talk about a discourse given by the Buddha. It is found in
 the collection of medium length
 discourses. This discourse is about the taking heritage.
 The name of this discourse is called
 Dhamma Daya Dha Sutta. You know, Buddha's teachings are
 divided into three main divisions.
 The first one is the rules and regulations for monks and
 nuns. And the third division
 is higher teachings of Buddhist psychology and philosophy.
 The second division, which
 is the largest, contains the discourses given by the Buddha
 to both lay people and monks.
 And they are the popular teachings of the Buddha. And what
 most people know about Buddhism
 comes from these discourses. And as you know, the majority
 of these discourses begin with
 the usual words, "Euam Meh Sutta" - "Thus have I heard".
 And in almost every Suddha,
 an information about that Suddha was given, where this Sudd
ha was taught, and on what
 occasion it was taught, and to whom it was taught. So most
 discourses contain these information.
 Take for example the discourse on the benefits of being a
 recluse. Now that Suddha was taught
 by the Buddha as a response to the question put to him by
 King Acha Dasadu. That is a
 fairly long discourse, and so it is included in the
 collection of long discourses. Most
 of you are familiar with the blessing discourse, Mangala S
uddha. So Mangala Suddha was given
 by the Buddha, delivered by the Buddha, because a deity
 came to him during the night and asked
 him to teach him the blessings. So it was at the request of
 that deity that Buddha taught
 the blessings. And there are other Suddhas where no
 occasion for delivering the discourse
 was given. So Buddha just talked to the monks, and this S
uddha belongs to that second category.
 So nobody asked the Buddha to deliver this discourse, and
 there was no occasion for delivering
 this discourse. But Buddha just came and then talked to the
 monks. The daily schedule of
 the Buddha, the first part of the night was reserved for
 monks. So during that time monks
 went to him, asked him questions or requested him to give a
 meditation subject, and so on.
 So that period was reserved for monks. And so it must be at
 such a period that Buddha
 delivered these discourse. And also during the time of the
 Buddha, monks assembled in
 the Dhamma Hall and maybe waited for the Buddha, and they
 always have a seat prepared for him.
 So he just went into the assembly and then called the monks
 and then taught what he wanted
 to teach. This is, Buddha first got the attention of the
 monks by calling them "O monks." And
 the monks always responded by the words "Bhadhanti." It is
 like the word "Bhante." So they responded
 as "Your reverence." And then Buddha taught. First thing
 Buddha said was, "Monks, be my
 heirs in Dhamma, not my heirs in material things." That
 means be the takers of Dhamma heritage
 and not the takers of material heritage. So by this
 sentence Buddha wanted his disciples
 to take the Dhamma heritage, not the material heritage from
 him. Buddha also gave reasons
 why he wanted his disciples to be heirs in Dhamma and not
 heirs in material things. And
 then he again taught or exalted them to be heirs in Dhamma
 with an example. Then after
 teaching with an example, Buddha got up and went back to
 his chamber. And when Buddha
 left, the Venerable Saribuddha continued the teaching. So
 this soda contains teachings
 of two teachers of the Buddha and of the Venerable Saribudd
ha. His disciples to be heirs in Dhamma.
 Now what is the Dhamma heritage? What does Dhamma mean here
? Now we meet the word Dhamma
 again. It tells us that there are two kinds of Dhamma here
 and two kinds of material things.
 Two kinds of Dhamma are the real Dhamma or the direct Dham
ma and indirect Dhamma or the
 real Dhamma and roundabout Dhamma. And the material things
 also are of two kinds, the
 direct material things and indirect material things or the
 real material things and roundabout
 material things. Since Buddha was very emphatic, very clear
, he does not say just be my heirs
 in Dhamma. He also said be not my heirs in material things.
 So he's very specific. So
 now we must understand the Dhamma, the direct Dhamma and
 the indirect Dhamma. These are
 for the realization of the four noble truths or for the
 eradication of mental defilements.
 And when a person gets enlightenment, then at that moment
 of enlightenment, a type of
 consciousness arises in his mind and that type of
 consciousness is called the path consciousness.
 And that consciousness has the power to destroy mental def
ilements at the same time taking
 Nibbana as object. And that path consciousness is
 immediately followed by fruit consciousness.
 It is said that there are two or three moments of fruit
 consciousness following one moment
 of path consciousness. And there are four types of path
 consciousness and four types
 of fruit consciousness. Take Nibbana as object. So four
 types of path consciousness, four
 types of fruit consciousness and the object Nibbana came to
 be called the nine supramandana
 states or nine supramandana Dhammas. So these are the herit
ages Buddha wanted to leave for
 his disciples. These are called heritages because when the
 disciples attain these types of consciousness
 or these stages of enlightenment, they get free from mental
 defilements and so they get
 free from suffering or they get free from rebirth in the s
amsara.
 When a person gets the first path and the first fruit
 consciousness, he is called a sottapana,
 a stream entrant. So a stream entrant eradicates some
 mental defilements altogether or once
 and for all. And since he has eradicated the mental defile
ments that would bring him down
 to four woeful states, he will not be reborn in these four
 woeful states. And also there
 will be only seven more lives for him to go until he attain
 final passing away. He is called
 a once returner. No, right? Sagadagami, yeah, he is called
 a once returner. That means he
 will return to this human world only once. That means he
 has only two more rebirths.
 One rebirth in the celestial realm and one back to the
 human world. So he has only two
 more rebirths before he attained nirvana. A returner does
 not eradicate any mental defilements
 but he makes the remaining mental defilements thinner or
 weaker. In the person reaches the
 third stage, the third path and fruit and consciousness, he
 is called a non returner.
 That means he will not return to this world of human beings
 and lower celestial beings.
 So he will be reborn in the world of Brahmās. So he will
 have rebirth as a Brahma and he
 may have only one rebirth as a Brahma or two, three, four,
 five rebirths as Brahma. And this
 one non returner eradicates sensual desire and ill will. In
 the person reaches the fourth
 stage he is called an Arahant. So an Arahant eradicates all
 the remaining mental defilements.
 Since he has eradicated all the mental defilements, there
 is no more rebirth for him. Be my heirs
 in Dhamma. He wanted us to attain one of these states. The
 four fruits and nirvana altogether
 are called Dharagdhamma here. The Inter-Dharma is the mer
itorious deeds done with the expectation
 of getting out of this round of rebirths. Now when we do
 meritorious deeds, sometimes
 we direct our minds to getting out of this round of rebirth
s and sometimes people may
 turn their minds to just getting the worldly benefits,
 worldly results. So the meritorious
 deeds done with the intention of getting out of this round
 of rebirths is called here Indirect
 Dhamma. Because these meritorious deeds we do with the
 intention of getting out of the
 samsara will ultimately or gradually take us to the
 realization of the direct Dhamma.
 So they are not the direct Dhamma but they can lead us to
 the direct Dhamma. So they
 are called indirect Dhamma. Be my heirs in Dhamma, he
 included this indirect Dhamma in
 the Dhamma. So by this exhortation Buddha wanted us to do
 meritorious deeds with the intention
 of getting out of samsara and ultimately to gain
 enlightenment. To meritorious deeds we
 must be careful that we make aspirations for getting out of
 this samsara or for realization
 of nirvana and not for worldly gains such as longevity or
 prosperity or beauty and others.
 So when meritorious deeds are done with the intention of
 getting out of this samsara,
 these meritorious deeds can gradually lead us to the
 realization of the true Dhamma.
 So when you make jhana, make offerings, when you keep
 reserves, when you offer flowers,
 perfumes and others to the Buddha and others, then you
 should direct your mind to nirvana
 or the realization of the Four Noble Truths. And also the j
hana's people attained through
 the practice of samatha meditation. Also when they are
 directed to the attainment of nirvana
 then they can lead those to that attainment. But if people
 practice and get jhana with
 the intention of worldly benefits then they will get
 worldly benefits only and not the
 ultimate benefit of getting out of this samsara. And so
 material things. First one is the direct
 material thing and second the indirect material thing. So
 here material things means the four
 requisites that monks receive. Now there are four requ
isites for monks and they are food,
 clothing, dwelling place and medicine. Because they are
 necessary, because they are conducive
 to the well-being of monks, they are called requisites.
 They are the ones that monks need
 to survive. So without food monks cannot survive, without
 clothing, without robes, without dwelling
 place or without medicine, they cannot survive. And so
 these four things are called the requisites
 for monks. But actually these four things are requisites
 not only for monks, for laypeople
 also. You also need these four, so four requisites. You
 need food, you need clothing, you need
 a house to live in and you need medicine. So these are the
 necessary things for survival
 of human beings. Now here the material things means these
 four requisites that monks get.
 These requisites to the monks, laypeople. But here these
 requisites are called Buddha's
 heritage. That means Buddha's things, Buddha's possessions.
 It is because Buddha allowed
 monks to accept the requisites offered by laypeople and
 Buddha allowed laypeople to offer
 requisites to monks. So although these requisites are
 actually given by laypeople, these are
 called Buddha's things, Buddha's heritage. The correct
 material things is again meritorious
 deeds done with the intention of getting just worldly
 benefits. So if you do a meritorious
 deed and wish for longevity or for beauty or for rebirth in
 human realm or in the realm
 of Devas and so on, then that meritorious deed is called
 indirect material things. Because
 although they are meritorious deeds, not things, they lead
 ultimately to getting these material
 things. They lead ultimately to getting wealth, possessions
 and beauty, longevity and so on.
 And so they are called here, indirect material things. Four
 kinds of heritages, two dharma
 heritage and two material heritage. And among these four,
 Buddha exhorted us to take the
 dharma heritage only and not the material heritage. And why
 did Buddha want his disciples
 to be heirs in dharma and not heirs in material things?
 Buddha said, "I have compassion for
 you thinking in what way my disciples will be heirs in dh
arma and not heirs in material
 things." The main thing is, Buddha said, "I have compassion
 for you." That means if you
 go after material things as heritage, then you will suffer.
 If you go for the material
 things only, then you will do anything. You will break
 rules and you will use means, whether
 just or unjust, or you will use means forbidden by the
 Buddha to get material things, especially
 for monks. And so when you get material things, then you
 will be attached to them. And so
 the attachment to the material things will take you down to
 the four woeful states. And
 even though you are not attached to these material things,
 because you use unlawful
 means to acquire these material things, you will go down to
 four woeful states. So the
 path of material gains is actually the path to suffering.
 And so Buddha had compassion
 for his monks and so he exalted them not to follow the path
 of material gains, but to
 follow the path of dharma. So this is one reason why Buddha
 wanted his disciples to
 be heirs in dharma, because he had compassion for them and
 he did not want them to go astray
 and to resort to means that are forbidden by him. Another
 reason why he exalted monks
 to be heirs in dharma, and that is if monks are heirs in
 material things and not in dharma,
 then they will be censured by people. They will be repro
ached, saying although these
 monks are the disciples of the Buddha, they do not take the
 advice of the Buddha and they
 go after material things and so on. So Buddha said you will
 be reproached if you are heirs
 not in dharma but in material things. And I will also be
 reproached because people will
 say this Buddha, he claimed to be an omniscient one, but he
 is unable to teach his disciples
 to be heirs in dharma. So both the monks and the Buddha
 will be reproached if the monks
 become the heirs in material things and not in dharma. He
 continued, if you are heirs
 in dharma and not in material things, then you will not be
 reproached and I will not
 be reproached either. So you become heirs in dharma and not
 in material things.
 Father taught with an example. The Buddha said, suppose I
 have eaten, I mean Buddha
 said, suppose I have eaten and I finished eating and there
 is some arms food left over
 and that is to be thrown away. And suppose two monks
 arrived who are hungry and weak,
 then I will tell them monks, I finished eating and there is
 some left over and that is to
 be thrown away. You may eat it if you like. If you don't
 eat it, then I will throw it
 away where there are no green plants and trees or I will
 drop it into water where there is
 no life. That means I will have to throw it away if you don
't eat. My thinking in this
 way. Now Buddha said that he had eaten and there is
 something left over and he said,
 if we do not eat it, then he will throw it away or float it
 on the water. So there is
 a food and I am weak and I am hungry. But Buddha had said
 that we are not to be heirs
 in material things but to be heirs in dharma. So following
 that advice, he would avoid eating
 that food and first the night and day, hungry and weak. So
 that is the first monk. So he
 will not take the food because taking the food means taking
 the material things, heritage.
 Now the second monk, he thought differently. Now there is a
 food Buddha had eaten and there
 is the food left over and if I do not eat it, Buddha will
 throw it away. So why not
 eat the food so that I can get rid of hunger and weakness
 and then I will spend the night
 and day in comfort. So he took the food. Then Buddha said,
 among these two monks, the one
 who did not eat the food but passed the day and night is
 more place worthy. Now Buddha
 did not blame the one who ate the food but he said that the
 monk who did not eat the
 food but avoid eating the food and spend the night with
 hunger and weakness. So that monk
 is more place worthy. So both monks are place worthy but
 the first monk is more place worthy
 than the others because when they passed the night and day,
 they did not pass idling. They
 passed the day and night in meditation. So both were good
 monks but Buddha said the first
 monk is more place worthy than the second monk. So why was
 the first monk more place
 worthy than the second one? Now Buddha said his practice of
 refusing to eat the food will
 help him in the future to practice fewness of wishes, to
 practice contentment, to practice
 scraping away mental defilements and to be easy to support
 and to arouse energy when
 he is discouraged or when he is low in spirits. That monk
 is tormented by thoughts of getting
 more and more. So when such thoughts arise in him, he could
 admonish himself saying,
 you even give up eating the food and you spend the night
 hungry and weary at that time, so
 why can you not resist this? I don't know how to call that.
 To having many wishes. So
 how can you not resist having many wishes? And also he may
 sometimes become not content
 with what he has. He may want more things for himself and
 then he can admonish himself
 like saying himself, you even relinquish eating food on
 that occasion and why not can you
 be content this time and so on. So he can use that practice
 or that incident as a means
 for admonishing him when he is attacked by such thoughts of
 desire and thoughts of discontentment
 or thoughts of mental defilements. So that is why this monk
 is said to be more praiseworthy
 than the other monk. He needs to have few wishes to be
 content, to scrape mental defilements
 away and to be easy to support and to be energetic. So when
 a monk wants to act against this training
 or against this advice, then he can admonish himself if he
 had avoided eating that food
 left over by the Buddha. That is why Buddha said he was
 more praiseworthy than the other
 monk. So monks are trained to be content and to be easy to
 support. That is very important
 quality in a monk. So if a monk wants this and that and he
 is not satisfied with just
 what he has, he causes suffering to himself as well as to
 those who support him. So monks
 should be those who are easy to support. That means who are
 content with what they are given
 and also they are to be content with what they already have
. So these are the two qualities,
 especially these two qualities monks have to develop. So
 Buddha exalted his disciples
 to be heirs in Dharma and not to be heirs in material
 things. And how many reasons did
 he give? Because he had compassion for monks because if
 they follow the path of material
 things then they will suffer in four vocal states. And so
 he had compassion for the monks
 and so he he exhorted them to be heirs in Dharma and not to
 be heirs in material things. And
 the second reason he gave was if you don't follow the path
 of Dharma, if you are not
 heirs in Dharma then you will be reproached and I will also
 be reproached. So that is
 the second reason. And the third is if you follow the the
 the the path of Dharma, if you are not
 an heir to material things then you can admonish yourself
 later when thoughts of greed,
 thoughts of craving and others come to torment your mind.
 So for these three reasons
 Buddha exhorted his disciples to be heirs in Dharma and not
 in material things.
 Okay, Buddha did not want us to be heirs in material things
. But does that mean that we
 must not accept any requisites at all? Without the requ
isites we cannot survive. So we have to accept,
 we have to have these requisites. So I think what Buddha
 meant here was not to accept requisites at
 all but to practice contentment, to practice fewness of
 wishes with regard to these requisites,
 and not to be attached to these requisites, and not to try
 to to get more and more requisites.
 Because when a person or a monk wants more and more then he
 will resort to any means,
 not only lawful means but unlawful means also he will
 resort to. And so I think here Buddha wanted
 us to practice restraint with regard to the requisites but
 not to get requisites at all.
 Then that is impossible. So what heritage we will take?
 There are four choices,
 two dharma and two material things. Which ones do what you
 choose?
 All four?
 So the first priority should be the first one, the dharma.
 And if not the first one, then
 the second one, the meritorious deeds with inclination to
 get out of this samsara.
 And the third one is not for you, it is for monks only. And
 what about the fourth one?
 Doing meritorious deeds with the intention of getting
 worldly results.
 Now the best thing is the first one, right? And the next
 best thing is the second one.
 And then the best thing after that is, I think the fourth
 one.
 Now, once my preceptor, not Mahasishya, he was another
 teacher, my preceptor,
 he was a very famous preacher. So in one of his preachings
 he said, even if you cannot
 do the meritorious deeds inclining towards getting out of s
amsara, you may even do the last one.
 Because it is better than doing no merit at all. And he
 gave us
 analogy. Suppose you are drowning and there is a carcass of
 an animal near you,
 what will you do? You will hold on to it or you will let
 yourself drown.
 So when you are drowning in this samsara, that may be good
 for you if there is no other thing.
 So although the merit with the intention of getting out of
 samsara is to be preferred,
 sometimes if we cannot get that type of merit, then we may
 get the other type of merit.
 Because this merit which leads us back into the samsara,
 but it will lead us to better
 existences, better lives. And so we can change ourselves
 when we get to the better lives and
 we can acquire meritorious deeds with the intention of
 getting out of the samsara.
 So since the fourth one is also a type of merit, it is
 preferable to not doing merit at all.
 During doing merit with the intention of getting worldly
 results,
 it's better than not doing merit at all. That is what I
 said.
 Okay, so now we are practicing vibhasana meditation. So
 what heritage are we taking?
 The first one? We are trying to take the first one, right?
 And the second one,
 I think vibhasana is also a meritorious deed. And when we
 practice vibhasana,
 we direct our minds to getting rid of samsara, right? To
 getting rid of mental defilements.
 So I think we are taking the second heritage also.
 So I think we are the beautiful followers, beautiful
 disciples of the Buddha.
 Buddha concluded his short discourse with the very words he
 said in the beginning,
 that is monks, be my heirs in dharma and not my heirs in
 material things.
 Because I have compassion for you, thinking how, in what
 way
 my disciples will be heirs in dharma and not my heirs in
 material things. So he concluded,
 I think it is to emphasize the taking of dharma heritage.
 So he concluded
 his short talk with the very words he said in the beginning
.
 The Buddha rose from his seat and went into his dwelling.
 So he caught up and left.
 So when the Buddha left, it was the responsibility of the
 chief disciple to continue his teachings.
 So Sariboda took up the teaching and gave a talk to the
 monks. Because
 of why the Buddha left, the reason why the Buddha left was
 not given.
 But in some discourses, the reason was given. Sometimes a
 Buddha got up and left, saying,
 I have an ache in my back, so I want to relax.
 And then he would leave. But here Buddha did not see
 anything,
 so he didn't suffer any back ache here.
 To give chance to the Venerable Sariboda to teach the monks
. So he left.
 And the Venerable Sariboda addressed the monks, saying,
 friend monks or friends monks.
 Then they replied friend. Now there is a difference between
 the Buddha and the disciples.
 When the Buddha addressed the monks, the Buddha just said,
 oh monks. And then they would say Venerable Sar.
 But when a disciple like Sariboda addressed the monks, he
 said, friends monks, not just monks.
 So he used the word friends. And then they responded with
 the word friends again, oh friend.
 So during the time of the Buddha, monks call each other,
 let's say friend, in Pali, Auso.
 So whether the monk is older than him or younger than him,
 they would address him as friend.
 But after the death of the Buddha, it was changed. And a
 younger monk never called an older monk friend.
 We call Banti or Venerable Sar. And the older monk or
 younger monk Auso, that means a friend.
 So during the time of the Buddha, this different form of
 address was not introduced yet.
 So only after the death of the Buddha, actually the Buddha
 gave instructions to monks that after
 my passing away, the younger monks should address older
 monks by the word Banti.
 And older monks should address the younger monks by the
 word Auso, that means a Venerable Sar and friend.
 Yeah, that's in Venerable Sar. So here the Venerable Sarib
oda said friends, monks, and then they said friends.
 That there may be those who are younger than the Venerable
 Sariboda in the audience, but they're just a friend.
 And so the Venerable Sariboda continued the Dhamma talk,
 teaching them about the three kinds of seclusion and then
 telling them the evil states that are to be abandoned and
 also telling them that there is
 a way, a middle way for the abandonment of these mental def
ilements.
 So we will hear Sariboda tomorrow. What is that?
 Sariboda.
 Sariboda.
 See.
