1
00:00:00,000 --> 00:00:05,380
 Is there anything we can do in this life to guarantee that

2
00:00:05,380 --> 00:00:08,000
 we will meet the Dhamma again in our next life?

3
00:00:08,000 --> 00:00:18,690
 Well simply being close to the Dhamma, our minds tend, it's

4
00:00:18,690 --> 00:00:23,000
 not entirely sufficient,

5
00:00:23,000 --> 00:00:30,520
 but our minds tend towards the sorts of things that we are

6
00:00:30,520 --> 00:00:32,000
 engaged in.

7
00:00:32,000 --> 00:00:39,050
 So there's just so many factors that you can never really

8
00:00:39,050 --> 00:00:40,000
 be sure.

9
00:00:40,000 --> 00:00:46,840
 But what you can be sure is if your mind is not in line

10
00:00:46,840 --> 00:00:49,000
 with the Dhamma,

11
00:00:49,000 --> 00:00:52,480
 then it doesn't matter whether you meet with the Dhamma or

12
00:00:52,480 --> 00:00:53,000
 not.

13
00:00:53,000 --> 00:00:57,000
 So in fact, it's important to meet with the Dhamma,

14
00:00:57,000 --> 00:01:01,000
 but it's far more important to be ready to meet with it.

15
00:01:01,000 --> 00:01:05,290
 And that's, I think, something that is much more under our

16
00:01:05,290 --> 00:01:08,000
 control than whether we actually meet with the Dhamma.

17
00:01:08,000 --> 00:01:11,000
 So you can't say what your next life is going to be like.

18
00:01:11,000 --> 00:01:16,510
 All of the factors are just so hard to predict and there's

19
00:01:16,510 --> 00:01:21,810
 so many things involved with the chance of where you're

20
00:01:21,810 --> 00:01:23,000
 going to be born.

21
00:01:23,000 --> 00:01:31,000
 But you can affect where your mind is at that time.

22
00:01:31,000 --> 00:01:36,880
 So in some cases, like in the case of Sumana the gardener,

23
00:01:36,880 --> 00:01:42,000
 who I talked about in the last Dhammapada video,

24
00:01:42,000 --> 00:01:45,190
 he met with a Buddha and he listened to the Buddha's

25
00:01:45,190 --> 00:01:46,000
 teaching, I think.

26
00:01:46,000 --> 00:01:50,540
 He paid great respect to the Buddha, but the Buddha's

27
00:01:50,540 --> 00:01:54,380
 prediction was that he would never become enlightened under

28
00:01:54,380 --> 00:01:55,000
 a Buddha,

29
00:01:55,000 --> 00:01:58,000
 that eventually he would become enlightened by himself.

30
00:01:58,000 --> 00:02:01,060
 He would become self-enlightened as a Pacheka Buddha, a

31
00:02:01,060 --> 00:02:04,000
 private Buddha, sometime in the future.

32
00:02:04,000 --> 00:02:10,210
 So it's a reminder that actually meeting with the Dhamma in

33
00:02:10,210 --> 00:02:18,510
 the sense of meeting with the passed-on teachings is not

34
00:02:18,510 --> 00:02:21,000
 the most important thing.

35
00:02:21,000 --> 00:02:23,490
 The most important thing is getting your mind in line with

36
00:02:23,490 --> 00:02:26,240
 the Dhamma, which most likely will cause you to meet with

37
00:02:26,240 --> 00:02:27,000
 the Dhamma.

38
00:02:27,000 --> 00:02:29,000
 It's just not certain.

39
00:02:29,000 --> 00:02:32,000
 But as I said, what is certain is if you're in a bad place,

40
00:02:32,000 --> 00:02:35,670
 if your mind is in a bad place, it doesn't matter if you

41
00:02:35,670 --> 00:02:38,000
 meet with the Dhamma,

42
00:02:38,000 --> 00:02:41,000
 because you'll never meet with the Dhamma, the truth.

43
00:02:41,000 --> 00:02:43,000
 The Dhamma is on different levels.

44
00:02:43,000 --> 00:02:46,920
 There's the teachings and then there's the truth, which the

45
00:02:46,920 --> 00:02:51,000
 teachings provide or help one realize.

46
00:02:51,000 --> 00:02:53,780
 So someone can realize the truth without having been taught

47
00:02:53,780 --> 00:02:54,000
.

48
00:02:54,000 --> 00:02:57,920
 It's much more difficult, of course, but the worst is if

49
00:02:57,920 --> 00:03:00,000
 your mind is not ready to understand,

50
00:03:00,000 --> 00:03:04,320
 and then it doesn't matter whether you receive the

51
00:03:04,320 --> 00:03:08,000
 teachings or not, you'll never become enlightened.

52
00:03:08,000 --> 00:03:13,250
 So the best we can do is to be ready for the teachings, to

53
00:03:13,250 --> 00:03:16,000
 purify our minds and have our minds in a good place.

54
00:03:16,000 --> 00:03:19,770
 Of course, the best we can do is now that we have met with

55
00:03:19,770 --> 00:03:24,000
 the teachings, to realize the Dhamma in this life.

56
00:03:24,000 --> 00:03:27,190
 Obviously, that's the best, and thinking about the next

57
00:03:27,190 --> 00:03:30,000
 life is a really bad place to focus our attention.

58
00:03:30,000 --> 00:03:34,660
 But a side product of that is, first of all, we'll be ready

59
00:03:34,660 --> 00:03:36,000
 for the teachings.

60
00:03:36,000 --> 00:03:40,530
 And second of all, provided our mind is involved enough

61
00:03:40,530 --> 00:03:44,540
 with the teachings and involved with people who are med

62
00:03:44,540 --> 00:03:45,000
itating

63
00:03:45,000 --> 00:03:48,990
 and so maybe helping out at a meditation center or teaching

64
00:03:48,990 --> 00:03:54,000
 meditation or helping other people learn the Dhamma.

65
00:03:54,000 --> 00:03:56,940
 For example, when you teach the Dhamma, you would think

66
00:03:56,940 --> 00:04:00,450
 that that puts you in a position to learn the Dhamma in the

67
00:04:00,450 --> 00:04:01,000
 future.

68
00:04:01,000 --> 00:04:05,280
 That if karma is really all that it's cracked up to be, the

69
00:04:05,280 --> 00:04:09,000
 best way to receive teachings is to give them.

70
00:04:09,000 --> 00:04:11,550
 Even in this life, you can see the best way to learn

71
00:04:11,550 --> 00:04:15,000
 something is to teach it. Many people will verify it.

72
00:04:15,000 --> 00:04:21,150
 So, obviously practicing the Dhamma is the best way, but

73
00:04:21,150 --> 00:04:24,490
 just remember that the most important thing is to be ready

74
00:04:24,490 --> 00:04:27,000
 for it rather than to simply meet with it.

75
00:04:27,000 --> 00:04:31,540
 Though, not to put down the chance of meeting with the Dham

76
00:04:31,540 --> 00:04:35,290
ma, it's a rare opportunity and it's a wonderful thing that

77
00:04:35,290 --> 00:04:36,000
 we have.

78
00:04:36,000 --> 00:04:39,500
 It's just that I would say it's not entirely certain, even

79
00:04:39,500 --> 00:04:43,140
 if you are a practitioner, because there's just so many

80
00:04:43,140 --> 00:04:45,000
 variables involved with karma.

81
00:04:45,000 --> 00:04:47,900
 It's something that we can't really understand and can't

82
00:04:47,900 --> 00:04:51,000
 really predict unless we're fully enlightened Buddhas.

