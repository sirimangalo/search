1
00:00:00,000 --> 00:00:02,260
 Okay so today we continue our sthanaibh

2
00:00:02,260 --> 00:00:09,740
 dhammapada with verse number 16 which

3
00:00:09,740 --> 00:00:12,200
 reads as follows.

4
00:00:12,200 --> 00:00:17,680
 "Indha maudati, pecha maudati, kata punyo

5
00:00:17,680 --> 00:00:24,240
 ubhayata maudati, so maudati, so pamodati,

6
00:00:24,240 --> 00:00:30,880
 dhisvakamat visudimmatano," which means

7
00:00:30,880 --> 00:00:36,960
 here he rejoices, hereafter he rejoices.

8
00:00:36,960 --> 00:00:41,880
 The kata punyo, the person, the doer of

9
00:00:41,880 --> 00:00:46,440
 good deeds rejoices in both places. He

10
00:00:46,440 --> 00:00:50,640
 rejoices, he thoroughly rejoices, having

11
00:00:50,640 --> 00:00:56,600
 seen the purity of his own actions.

12
00:00:56,600 --> 00:01:00,360
 This is the companion verse to our last,

13
00:01:00,360 --> 00:01:03,600
 to verse 15, but the story is different.

14
00:01:03,600 --> 00:01:07,200
 The story here is about a good doer.

15
00:01:07,200 --> 00:01:10,920
 It's a story of dhammika ubasika,

16
00:01:10,920 --> 00:01:15,840
 ubasika, who was, dhammika means one who

17
00:01:15,840 --> 00:01:19,080
 lives by the dhamma. Ubasika is a person

18
00:01:19,080 --> 00:01:22,440
 who follows the teaching or who

19
00:01:22,440 --> 00:01:26,960
 takes refuge. Dhammika ubasika was a

20
00:01:26,960 --> 00:01:30,120
 follower of the Buddha who was very much

21
00:01:30,120 --> 00:01:34,320
 involved with the Buddha's teaching. He

22
00:01:34,320 --> 00:01:36,480
 was very keen on it and interested in it.

23
00:01:36,480 --> 00:01:38,720
 So he would always want to listen to

24
00:01:38,720 --> 00:01:40,200
 the Buddha's teaching, listen to the

25
00:01:40,200 --> 00:01:43,320
 sermons again and again, invite monks to

26
00:01:43,320 --> 00:01:45,840
 his house to give preachings. He

27
00:01:45,840 --> 00:01:48,400
 would be always giving alms to the

28
00:01:48,400 --> 00:01:49,960
 monks, giving food to the monks when they

29
00:01:49,960 --> 00:01:53,240
 came. He was very much looking after

30
00:01:53,240 --> 00:01:55,880
 them. So always doing good deeds. He was

31
00:01:55,880 --> 00:01:59,040
 he was established in morality and I

32
00:01:59,040 --> 00:02:02,120
 believe he was also a meditator. He must

33
00:02:02,120 --> 00:02:05,360
 have very well been because he was very

34
00:02:05,360 --> 00:02:08,080
 much interested in the Buddha's teaching

35
00:02:08,080 --> 00:02:12,640
 on satipatthana as the story goes. So as

36
00:02:12,640 --> 00:02:13,960
 I understand he may have even been a

37
00:02:13,960 --> 00:02:16,080
 sotapana, but I'm not sure about that

38
00:02:16,080 --> 00:02:17,840
 I can't quite remember. Anyway we can

39
00:02:17,840 --> 00:02:20,960
 consider him to be a very good Buddhist

40
00:02:20,960 --> 00:02:28,560
 in all senses of the word. And so it

41
00:02:28,560 --> 00:02:31,080
 happened that he as well after many

42
00:02:31,080 --> 00:02:33,280
 years he became ill and was on his

43
00:02:33,280 --> 00:02:35,240
 deathbed just like Chunda the pork

44
00:02:35,240 --> 00:02:38,840
 butcher. But when he was on his deathbed

45
00:02:38,840 --> 00:02:42,840
 rather than becoming deranged or having

46
00:02:42,840 --> 00:02:46,400
 his deeds, his bad past deeds, overwhelm

47
00:02:46,400 --> 00:02:49,360
 him, his good deeds came to him. And right

48
00:02:49,360 --> 00:02:52,440
 away he thought to do more good deeds. So

49
00:02:52,440 --> 00:02:54,760
 when he knew that he was probably at

50
00:02:54,760 --> 00:02:58,080
 his last moments he called he sent a

51
00:02:58,080 --> 00:02:59,800
 message to the Buddha asking for monks

52
00:02:59,800 --> 00:03:01,800
 to come. If they sent some monks I would

53
00:03:01,800 --> 00:03:04,240
 like to hear the Dhamma. I figure the best

54
00:03:04,240 --> 00:03:05,840
 way to go out is listening to the Dhamma

55
00:03:05,840 --> 00:03:09,680
 and practicing according to the teaching.

56
00:03:09,680 --> 00:03:11,880
 So the Buddha right away sent monks and

57
00:03:11,880 --> 00:03:13,360
 they came and they sat around his bed

58
00:03:13,360 --> 00:03:17,280
 and they asked him, "Upaśika, what

59
00:03:17,280 --> 00:03:19,600
 teaching would you like to hear?" And he

60
00:03:19,600 --> 00:03:22,240
 said, "Please recite for me the Satipatāna

61
00:03:22,240 --> 00:03:24,280
 sūta because it's the teaching of

62
00:03:24,280 --> 00:03:27,400
 all the of all the Buddhas." It's clear

63
00:03:27,400 --> 00:03:29,480
 that he was very much clear and very much

64
00:03:29,480 --> 00:03:31,200
 interested in the meditation practice.

65
00:03:31,200 --> 00:03:34,440
 Satipatāna sūta is what our

66
00:03:34,440 --> 00:03:36,640
 meditation practice is based on. It talks

67
00:03:36,640 --> 00:03:39,200
 about the four foundations of mindfulness,

68
00:03:39,200 --> 00:03:42,160
 the body, the feelings, the mind and the

69
00:03:42,160 --> 00:03:45,680
 Dhamma. So if you haven't read it, I

70
00:03:45,680 --> 00:03:48,120
 think you probably all have read it, but

71
00:03:48,120 --> 00:03:50,760
 if you haven't read it, just to explain,

72
00:03:50,760 --> 00:03:54,360
 it talks about the body. So how when

73
00:03:54,360 --> 00:03:55,760
 you're walking, you know you're walking,

74
00:03:55,760 --> 00:03:56,960
 when the breath comes in, you know the

75
00:03:56,960 --> 00:03:59,440
 breath is coming in. For instance, rising

76
00:03:59,440 --> 00:04:01,760
 and falling, being aware of the physical

77
00:04:01,760 --> 00:04:04,080
 aspects of the body, the movements of the

78
00:04:04,080 --> 00:04:06,280
 body, when the stomach moves, when the

79
00:04:06,280 --> 00:04:10,240
 foot moves, when the hand moves, even when

80
00:04:10,240 --> 00:04:11,520
 you're going to the washroom, when you're

81
00:04:11,520 --> 00:04:13,800
 eating, when you're, you know, whatever you're

82
00:04:13,800 --> 00:04:16,040
 doing during the day, it talks about being

83
00:04:16,040 --> 00:04:17,600
 mindful of the body, knowing it for what

84
00:04:17,600 --> 00:04:20,440
 it is. When you have feelings, you know

85
00:04:20,440 --> 00:04:22,680
 the feelings for what it is, pain or aching

86
00:04:22,680 --> 00:04:25,720
 or soreness, when you have thoughts, so

87
00:04:25,720 --> 00:04:27,640
 that and when you have emotions, the

88
00:04:27,640 --> 00:04:30,080
 Dhamma, the liking, disliking and so on.

89
00:04:30,080 --> 00:04:32,040
 This is what it talks about. So they began

90
00:04:32,040 --> 00:04:34,520
 to recite this. They started with the

91
00:04:34,520 --> 00:04:37,000
 beginning that says, "Eka yanoa yang bhikkhu

92
00:04:37,000 --> 00:04:39,320
 mānggu." This is the only way

93
00:04:39,320 --> 00:04:41,520
 o monks, or this is the one way o monks,

94
00:04:41,520 --> 00:04:44,880
 for the purification of beings, for the

95
00:04:44,880 --> 00:04:47,480
 overcoming of sorrow, lamentation and

96
00:04:47,480 --> 00:04:50,600
 despair, for the destruction of bodily

97
00:04:50,600 --> 00:04:52,680
 and mental suffering, for attaining the

98
00:04:52,680 --> 00:04:55,520
 right path and for realizing freedom.

99
00:04:55,520 --> 00:04:58,840
 And so it's a very profound discourse.

100
00:04:58,840 --> 00:05:01,840
 It's one that even today they will

101
00:05:01,840 --> 00:05:04,320
 often recite for people who are dying or

102
00:05:04,320 --> 00:05:06,760
 recite just in general in meditation

103
00:05:06,760 --> 00:05:09,440
 centers to remind them of the teachings.

104
00:05:09,440 --> 00:05:11,720
 So at this time he would be listening to

105
00:05:11,720 --> 00:05:14,560
 and he would also be practicing. Now as he

106
00:05:14,560 --> 00:05:16,440
 was on his way out, and because he was

107
00:05:16,440 --> 00:05:20,440
 actually such a pure and wonderful

108
00:05:20,440 --> 00:05:26,080
 person, the story goes, and this is where

109
00:05:26,080 --> 00:05:28,040
 many people get turned off because some

110
00:05:28,040 --> 00:05:29,320
 of these stories are a little bit

111
00:05:29,320 --> 00:05:31,720
 fantastical. So whether they're true or

112
00:05:31,720 --> 00:05:33,080
 not, I'll leave it up to you to decide

113
00:05:33,080 --> 00:05:35,680
 with the point that the truth of

114
00:05:35,680 --> 00:05:37,640
 this fantastical parts doesn't really

115
00:05:37,640 --> 00:05:40,840
 affect the message at all. And it's our

116
00:05:40,840 --> 00:05:44,720
 meditation that verifies or denies the

117
00:05:44,720 --> 00:05:47,120
 message, which in this case is that a

118
00:05:47,120 --> 00:05:49,800
 good person has good things happen to them.

119
00:05:49,800 --> 00:05:52,400
 But it's said that he had visions, suddenly

120
00:05:52,400 --> 00:05:57,600
 had visions of angels coming from all of

121
00:05:57,600 --> 00:06:01,440
 the six levels of essential heaven when

122
00:06:01,440 --> 00:06:04,480
 they came in chariots. And they all began

123
00:06:04,480 --> 00:06:07,400
 to call to him, "Please be born in my realm,

124
00:06:07,400 --> 00:06:09,000
 be born in..." because they all wanted

125
00:06:09,000 --> 00:06:12,800
 this guy, it was kind of like a baseball

126
00:06:12,800 --> 00:06:15,600
 draft or something. They all wanted this

127
00:06:15,600 --> 00:06:18,920
 guy on their team. And so they were all

128
00:06:18,920 --> 00:06:23,000
 extolling the virtues of their

129
00:06:23,000 --> 00:06:25,680
 heaven, and that he should be born there.

130
00:06:25,680 --> 00:06:29,720
 And nobody else could hear or see them,

131
00:06:29,720 --> 00:06:31,400
 it was only a vision that he had. And so

132
00:06:31,400 --> 00:06:36,720
 he said to them, "Wait, wait." And his

133
00:06:36,720 --> 00:06:40,080
 children heard this, "Wait, wait," and this

134
00:06:40,080 --> 00:06:41,840
 kind of distracted look like he was

135
00:06:41,840 --> 00:06:43,920
 talking to himself, that he was saying,

136
00:06:43,920 --> 00:06:47,280
 "Wait, wait." And they thought, "He must be

137
00:06:47,280 --> 00:06:48,760
 telling the monks to stop. The monks

138
00:06:48,760 --> 00:06:50,000
 were there chanting, and suddenly he

139
00:06:50,000 --> 00:06:51,480
 interrupts them and says, "Wait, wait."

140
00:06:51,480 --> 00:06:53,840
 And so the monks suddenly stopped

141
00:06:53,840 --> 00:06:56,400
 chanting. And they listened to him, "Okay, he

142
00:06:56,400 --> 00:06:59,120
 wants us to wait, let's wait." Because they

143
00:06:59,120 --> 00:07:01,720
 respected this guy. And the children

144
00:07:01,720 --> 00:07:03,960
 started crying, and they were

145
00:07:03,960 --> 00:07:06,040
 mortified because they had been

146
00:07:06,040 --> 00:07:08,280
 taught by him the importance of

147
00:07:08,280 --> 00:07:13,360
 listening to the Dhamma. And so they

148
00:07:13,360 --> 00:07:15,720
 thought, "He must be totally deranged," or

149
00:07:15,720 --> 00:07:17,880
 "He must be experiencing something

150
00:07:17,880 --> 00:07:20,400
 terrible and be afraid," or so on, "He

151
00:07:20,400 --> 00:07:21,920
 doesn't want the monks to teach the

152
00:07:21,920 --> 00:07:24,440
 Dhamma." And so they began crying. They

153
00:07:24,440 --> 00:07:27,080
 said, "Truly, no one is immune to this.

154
00:07:27,080 --> 00:07:29,040
 Even our own father, who we thought

155
00:07:29,040 --> 00:07:32,280
 was so mindful." And so the monks saw them

156
00:07:32,280 --> 00:07:34,320
 crying, and the old men saying, "Wait, wait."

157
00:07:34,320 --> 00:07:38,120
 They said, "Nothing for us to do,"

158
00:07:38,120 --> 00:07:42,560
 so they all left. And after they left,

159
00:07:42,560 --> 00:07:44,720
 the old men said, "Where did the monks go?

160
00:07:44,720 --> 00:07:46,440
 Why did they stop chanting?" And they

161
00:07:46,440 --> 00:07:48,320
 said, "You told them to wait."

162
00:07:48,320 --> 00:07:53,000
 And we were all horrified that

163
00:07:53,000 --> 00:07:57,400
 we realized that you also

164
00:07:57,400 --> 00:07:59,080
 were becoming a little bit deranged here.

165
00:07:59,080 --> 00:08:01,840
 And he said, "That's not why I wasn't

166
00:08:01,840 --> 00:08:03,280
 telling the monks to wait. I was telling

167
00:08:03,280 --> 00:08:05,680
 these angels here to wait. Don't come

168
00:08:05,680 --> 00:08:07,080
 from me yet. I'm still listening to the

169
00:08:07,080 --> 00:08:10,200
 Dhamma." And they said, "What?

170
00:08:10,200 --> 00:08:12,440
 Chariots. We don't see them." And then the

171
00:08:12,440 --> 00:08:14,480
 story goes that he said he had them

172
00:08:14,480 --> 00:08:16,880
 take a wreath of flowers and throw it up

173
00:08:16,880 --> 00:08:19,160
 in the air, and it landed on one of the

174
00:08:19,160 --> 00:08:21,160
 chariots, and it just hung there in the

175
00:08:21,160 --> 00:08:23,160
 air. And he said, "That's the heaven that

176
00:08:23,160 --> 00:08:25,240
 I'm going to be born in, the Tousita

177
00:08:25,240 --> 00:08:27,640
 heaven." And then he passed away, and when

178
00:08:27,640 --> 00:08:29,020
 he passed away, he entered into the

179
00:08:29,020 --> 00:08:32,200
 chariot and went to heaven. This is how

180
00:08:32,200 --> 00:08:35,800
 the story goes. So you can imagine that it

181
00:08:35,800 --> 00:08:37,640
 may have been fancified. It may not have

182
00:08:37,640 --> 00:08:39,960
 been. It may be true that this kind of thing

183
00:08:39,960 --> 00:08:42,720
 happens. I'm open-minded about it, but

184
00:08:42,720 --> 00:08:44,760
 it's not really important. What we can't

185
00:08:44,760 --> 00:08:46,600
 understand, even based on the last one,

186
00:08:46,600 --> 00:08:50,680
 is on two levels, a person who

187
00:08:50,680 --> 00:08:53,280
 does good deeds, has good things

188
00:08:53,280 --> 00:08:55,000
 happen to them, and a person who does bad

189
00:08:55,000 --> 00:08:58,080
 things, has bad things happen, or

190
00:08:58,080 --> 00:09:01,120
 suffers. Sojati and modati. Sojati

191
00:09:01,120 --> 00:09:03,400
 means sorrows, and modati means rejoicers,

192
00:09:03,400 --> 00:09:09,160
 or feels great pleasure for. So when the

193
00:09:09,160 --> 00:09:10,960
 monks went back to the monastery, they

194
00:09:10,960 --> 00:09:12,360
 said to the Buddha, "You know, we tried,

195
00:09:12,360 --> 00:09:14,800
 but the sad thing is he got

196
00:09:14,800 --> 00:09:16,120
 deranged at the last moment, and he

197
00:09:16,120 --> 00:09:17,880
 actually told us to stop teaching the

198
00:09:17,880 --> 00:09:20,480
 Dhamma." And the Buddha said, "That's not

199
00:09:20,480 --> 00:09:22,520
 why he..." and the Buddha explained exactly

200
00:09:22,520 --> 00:09:26,600
 what happened. And then he said, "Wow,

201
00:09:26,600 --> 00:09:29,880
 he was so happy here on earth.

202
00:09:29,880 --> 00:09:31,520
 It's amazing. He was so happy here on

203
00:09:31,520 --> 00:09:33,840
 earth, and now he's even happier up in

204
00:09:33,840 --> 00:09:35,400
 heaven. How wonderful." And the Buddha said,

205
00:09:35,400 --> 00:09:37,640
 "This is the way it goes." A person who

206
00:09:37,640 --> 00:09:40,080
 does good deeds, and he recited the verse.

207
00:09:40,080 --> 00:09:46,480
 So this is another thing. It's also

208
00:09:46,480 --> 00:09:48,680
 something that is often invisible to us,

209
00:09:48,680 --> 00:09:51,560
 because we take it in a very shallow

210
00:09:51,560 --> 00:09:53,600
 sense. So you'll often see people who have

211
00:09:53,600 --> 00:09:55,720
 bad lives, who have bad things happening

212
00:09:55,720 --> 00:09:57,840
 to them because of the nature of their

213
00:09:57,840 --> 00:10:01,200
 lives, and they will undertake to do good

214
00:10:01,200 --> 00:10:02,440
 deeds thinking that it's going to get

215
00:10:02,440 --> 00:10:04,600
 rid of all of their suffering, right? And

216
00:10:04,600 --> 00:10:05,720
 then they're disappointed that it

217
00:10:05,720 --> 00:10:07,000
 doesn't, and they feel like they've been

218
00:10:07,000 --> 00:10:10,520
 betrayed, and so on. Because they have

219
00:10:10,520 --> 00:10:12,280
 this incredibly shallow understanding of

220
00:10:12,280 --> 00:10:14,760
 how good deeds work. But you do have

221
00:10:14,760 --> 00:10:16,840
 those people who understand how good

222
00:10:16,840 --> 00:10:19,440
 deeds work, and who really feel the

223
00:10:19,440 --> 00:10:22,880
 benefit of them, and understand how

224
00:10:22,880 --> 00:10:26,080
 wonderful it is to, for instance, give a

225
00:10:26,080 --> 00:10:29,120
 gift. I have this story of a woman I knew

226
00:10:29,120 --> 00:10:33,840
 when a girl, we were teenagers, and we

227
00:10:33,840 --> 00:10:35,040
 were walking down the street in the

228
00:10:35,040 --> 00:10:38,880
 middle of winter, and suddenly she, we

229
00:10:38,880 --> 00:10:40,400
 were walking aside and said, "Suddenly she

230
00:10:40,400 --> 00:10:42,880
 turned and went into a McDonald's," and

231
00:10:42,880 --> 00:10:44,560
 she said, "Wait here." And so I'm waiting

232
00:10:44,560 --> 00:10:47,600
 outside, and there's this beggar

233
00:10:47,600 --> 00:10:50,480
 right in front of this guy living on a

234
00:10:50,480 --> 00:10:52,000
 piece of cardboard outside the McDonald's.

235
00:10:52,000 --> 00:10:54,080
 She comes out with a hamburger, and I

236
00:10:54,080 --> 00:10:57,040
 know she doesn't eat meat, and she hands

237
00:10:57,040 --> 00:10:58,600
 it to this, she bends down and hands it

238
00:10:58,600 --> 00:11:00,320
 to this guy sitting on the

239
00:11:00,320 --> 00:11:03,040
 cardboard and says, "Here you go." And I'm

240
00:11:03,040 --> 00:11:04,400
 standing there kind of feeling ashamed

241
00:11:04,400 --> 00:11:05,800
 of myself. I didn't even see the guy

242
00:11:05,800 --> 00:11:08,720
 until she went in. I didn't think anything

243
00:11:08,720 --> 00:11:10,880
 of it, but right away, and then we started

244
00:11:10,880 --> 00:11:12,560
 walking again, and right away she said to

245
00:11:12,560 --> 00:11:17,080
 me, she said, "I don't give to people,

246
00:11:17,080 --> 00:11:20,560
 I don't give charity because I want

247
00:11:20,560 --> 00:11:22,920
 them to feel happy. I do it because I'm

248
00:11:22,920 --> 00:11:25,920
 selfish. I give gifts because it makes me

249
00:11:25,920 --> 00:11:28,360
 feel happy." This is a person who isn't

250
00:11:28,360 --> 00:11:30,520
 religious at all, and I don't know where

251
00:11:30,520 --> 00:11:32,240
 she is now, but she's just a person who

252
00:11:32,240 --> 00:11:35,040
 has really felt the goodness of giving,

253
00:11:35,040 --> 00:11:38,000
 the goodness of charity. And I think

254
00:11:38,000 --> 00:11:39,800
 people who are engaged in giving do feel

255
00:11:39,800 --> 00:11:42,160
 this, because it's a pleasure

256
00:11:42,160 --> 00:11:44,520
 that is unadulterated. It's a pleasure

257
00:11:44,520 --> 00:11:47,840
 that when you have this pleasure,

258
00:11:47,840 --> 00:11:50,400
 there's nothing tainting it or

259
00:11:50,400 --> 00:11:53,400
 contaminating it. And it's also something

260
00:11:53,400 --> 00:11:55,040
 that lasts with you forever. It's

261
00:11:55,040 --> 00:11:56,400
 something that's never going to be taken

262
00:11:56,400 --> 00:11:58,360
 away, whereas pleasure based on

263
00:11:58,360 --> 00:12:00,880
 sensuality is totally dependent on the

264
00:12:00,880 --> 00:12:03,680
 object of the sense. When a person

265
00:12:03,680 --> 00:12:06,240
 remembers about the good deeds that they

266
00:12:06,240 --> 00:12:08,240
 have done, they can repeatedly gain this

267
00:12:08,240 --> 00:12:14,000
 happiness and this peace. So it's

268
00:12:14,000 --> 00:12:15,640
 something that people, I think,

269
00:12:15,640 --> 00:12:17,040
 misunderstand, and it's a part of how

270
00:12:17,040 --> 00:12:19,000
 people misunderstand karma as being some

271
00:12:19,000 --> 00:12:23,000
 magical thing that you can just put

272
00:12:23,000 --> 00:12:25,600
 money in a box and magically become

273
00:12:25,600 --> 00:12:28,880
 rich or so on, or give this, or give

274
00:12:28,880 --> 00:12:31,640
 gifts, and suddenly you're happy. But

275
00:12:31,640 --> 00:12:33,760
 people who do it continuously and who

276
00:12:33,760 --> 00:12:36,200
 get engaged in it as a practice, giving

277
00:12:36,200 --> 00:12:38,960
 or morality, morality is another one.

278
00:12:38,960 --> 00:12:41,400
 People feel like morality is a waste of

279
00:12:41,400 --> 00:12:43,400
 time or it's just a lot of suffering to

280
00:12:43,400 --> 00:12:45,600
 not do things that you want to do. You

281
00:12:45,600 --> 00:12:48,720
 have a mosquito, why not just kill it?

282
00:12:48,720 --> 00:12:50,040
 The people who do it, and most

283
00:12:50,040 --> 00:12:51,400
 importantly, the people who engage in

284
00:12:51,400 --> 00:12:53,360
 meditation, are able to see the

285
00:12:53,360 --> 00:12:55,000
 difference. And so this is what you

286
00:12:55,000 --> 00:12:57,600
 should all be seeing right now, that the

287
00:12:57,600 --> 00:12:58,960
 good and the bad things in your mind

288
00:12:58,960 --> 00:13:01,080
 have really very much contributed to how

289
00:13:01,080 --> 00:13:03,760
 you now relate to the present moment, how

290
00:13:03,760 --> 00:13:07,400
 you react to things. And through the

291
00:13:07,400 --> 00:13:08,760
 meditation you begin to see the

292
00:13:08,760 --> 00:13:10,360
 importance of doing good deeds, the

293
00:13:10,360 --> 00:13:12,760
 importance of being generous, the

294
00:13:12,760 --> 00:13:15,240
 importance of being moral, and the

295
00:13:15,240 --> 00:13:17,360
 importance of practicing, of developing

296
00:13:17,360 --> 00:13:20,160
 your mind, of strengthening your mind so

297
00:13:20,160 --> 00:13:22,400
 that you can see things as they are. As

298
00:13:22,400 --> 00:13:24,080
 you can see, the weaknesses in our mind,

299
00:13:24,080 --> 00:13:27,200
 the greed, the anger, the delusion, are

300
00:13:27,200 --> 00:13:29,040
 what's causing us great suffering, our

301
00:13:29,040 --> 00:13:34,960
 stinginess, our jealousy, and so on.

302
00:13:34,960 --> 00:13:38,320
 So it's actually easier to see how

303
00:13:38,320 --> 00:13:41,720
 this works in the present life, even

304
00:13:41,720 --> 00:13:42,920
 though most people don't see it, but

305
00:13:42,920 --> 00:13:44,080
 through a little bit of meditation we

306
00:13:44,080 --> 00:13:46,280
 can see how it happens. Now we have to

307
00:13:46,280 --> 00:13:49,000
 kind of, people say, take a leap of faith

308
00:13:49,000 --> 00:13:50,400
 to think about how it's going to happen

309
00:13:50,400 --> 00:13:51,960
 in the next life, just like with the last

310
00:13:51,960 --> 00:13:53,880
 one. You know, is it really true that this

311
00:13:53,880 --> 00:13:55,480
 man went to heaven and that man went to

312
00:13:55,480 --> 00:13:59,000
 hell and so on? But it's

313
00:13:59,000 --> 00:14:01,360
 ameliorated, and it's actually really done

314
00:14:01,360 --> 00:14:04,360
 away with this idea of taking a leap of

315
00:14:04,360 --> 00:14:06,160
 faith in regards to what happens when we

316
00:14:06,160 --> 00:14:10,560
 die by understanding, by really

317
00:14:10,560 --> 00:14:12,360
 appreciating the experience that we're

318
00:14:12,360 --> 00:14:15,440
 having in meditation, and internalizing it,

319
00:14:15,440 --> 00:14:17,760
 and realizing that this is reality. When

320
00:14:17,760 --> 00:14:19,280
 we look around us and we see there's

321
00:14:19,280 --> 00:14:21,640
 this building, there's the trees, and so

322
00:14:21,640 --> 00:14:23,640
 on, we see other people, we're actually

323
00:14:23,640 --> 00:14:25,720
 just building these constructs up in our

324
00:14:25,720 --> 00:14:27,400
 mind. The truth is that there's

325
00:14:27,400 --> 00:14:29,520
 experience. Now you're hearing, now you're

326
00:14:29,520 --> 00:14:31,040
 seeing, now you're smelling, now you're

327
00:14:31,040 --> 00:14:33,120
 tasting, now you're feeling, and now you're

328
00:14:33,120 --> 00:14:36,000
 thinking. So at the moment of death, the

329
00:14:36,000 --> 00:14:38,000
 point is that this doesn't change. The

330
00:14:38,000 --> 00:14:41,320
 theory is, that we subscribe to, is that

331
00:14:41,320 --> 00:14:42,880
 this doesn't change, because there's no

332
00:14:42,880 --> 00:14:45,680
 reason for it to change. The physical

333
00:14:45,680 --> 00:14:48,360
 aspect of our reality is really just a

334
00:14:48,360 --> 00:14:50,760
 part of experience, moment to moment to

335
00:14:50,760 --> 00:14:53,600
 moment to moment experience that continues on.

336
00:14:53,600 --> 00:14:57,440
 It really helps to make sense of why

337
00:14:57,440 --> 00:14:59,400
 the universe is the way it is. It's very

338
00:14:59,400 --> 00:15:02,480
 much based on past causes and conditions,

339
00:15:02,480 --> 00:15:04,560
 and so the future as well will be based

340
00:15:04,560 --> 00:15:06,400
 on these same causes and conditions. If

341
00:15:06,400 --> 00:15:09,840
 our mind is impure, as the Buddha says,

342
00:15:09,840 --> 00:15:13,520
 suffering follows us. If our mind is pure,

343
00:15:13,520 --> 00:15:15,880
 then happiness follows us. So this is how

344
00:15:15,880 --> 00:15:18,360
 we understand that a person who does good

345
00:15:18,360 --> 00:15:21,680
 deeds benefits both in this life and in

346
00:15:21,680 --> 00:15:24,280
 the next, and it's very much a part of

347
00:15:24,280 --> 00:15:26,480
 our practice of meditation. In the practice

348
00:15:26,480 --> 00:15:28,560
 of meditation, we're purifying our minds,

349
00:15:28,560 --> 00:15:31,560
 overcoming greed, the desire to gain this

350
00:15:31,560 --> 00:15:33,280
 and that, because we're understanding that

351
00:15:33,280 --> 00:15:35,360
 it's a cause for suffering, and we're

352
00:15:35,360 --> 00:15:39,120
 purifying. The word he uses is kamma visudingatamo.

353
00:15:39,120 --> 00:15:42,040
 Visudmi means purity, so it's the purity

354
00:15:42,040 --> 00:15:45,520
 of our acts. An act is pure or is defiled,

355
00:15:45,520 --> 00:15:48,280
 as I think you all are aware, based on the

356
00:15:48,280 --> 00:15:51,120
 intentions of the mind. So even if

357
00:15:51,120 --> 00:15:53,480
 you walk, you can walk with an impure mind.

358
00:15:53,480 --> 00:15:55,680
 You can speak. When you speak, you can

359
00:15:55,680 --> 00:15:58,960
 speak with a purer and impure mind.

360
00:15:58,960 --> 00:16:00,680
 Obviously, the practice of meditation is

361
00:16:00,680 --> 00:16:02,960
 what's allowing us to see purity and

362
00:16:02,960 --> 00:16:06,200
 impurity and to refine our behavior so

363
00:16:06,200 --> 00:16:07,800
 that our reactions to things. This is

364
00:16:07,800 --> 00:16:09,480
 really what you're doing, because again

365
00:16:09,480 --> 00:16:11,520
 and again you're reacting to things and

366
00:16:11,520 --> 00:16:13,960
 you'll see your reaction to things and

367
00:16:13,960 --> 00:16:15,840
 you'll slowly refine those reactions

368
00:16:15,840 --> 00:16:17,600
 until it just becomes an interaction.

369
00:16:17,600 --> 00:16:20,120
 You're not liking things or disliking

370
00:16:20,120 --> 00:16:23,200
 things, you're just being with them.

371
00:16:23,200 --> 00:16:25,360
 "Now I'm seeing, now I'm hearing, now I have pain,

372
00:16:25,360 --> 00:16:28,640
 now I have pleasure," and so on, and you're

373
00:16:28,640 --> 00:16:30,640
 not clinging to anything. This is how the

374
00:16:30,640 --> 00:16:32,240
 Buddha said in the Satipatthana Suddha,

375
00:16:32,240 --> 00:16:34,720
 "Ani sito nju hi hatatthi rindwel," not

376
00:16:34,720 --> 00:16:38,240
 clinging to anything. So just another

377
00:16:38,240 --> 00:16:41,040
 brief teaching, something more for us to

378
00:16:41,040 --> 00:16:43,360
 keep in mind when we do our practice

379
00:16:43,360 --> 00:16:44,760
 that this is really what we're doing is

380
00:16:44,760 --> 00:16:48,160
 purifying our thoughts and our speech

381
00:16:48,160 --> 00:16:51,320
 and our deeds. So thanks for listening.

382
00:16:51,320 --> 00:16:55,240
 And back to meditation.

