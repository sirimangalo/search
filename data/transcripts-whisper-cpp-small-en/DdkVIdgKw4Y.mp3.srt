1
00:00:00,000 --> 00:00:05,720
 Here's a good one. Thank you for your erratic question.

2
00:00:05,720 --> 00:00:06,480
 Question is, it's not

3
00:00:06,480 --> 00:00:10,400
 better to suffer in future lives, it's not better to suffer

4
00:00:10,400 --> 00:00:11,220
 in future lives

5
00:00:11,220 --> 00:00:15,450
 instead of reaching enlightenment and not live anymore. I

6
00:00:15,450 --> 00:00:20,000
 hear you. It's quite

7
00:00:20,000 --> 00:00:26,490
 clear and I think this is on a lot of people's minds. There

8
00:00:26,490 --> 00:00:29,800
 was a queen once in

9
00:00:29,800 --> 00:00:35,490
 the Buddha's time and she became an Arahant and the Buddha

10
00:00:35,490 --> 00:00:36,480
 said to the king,

11
00:00:36,480 --> 00:00:42,880
 "Well either you let her become a bhikkhuni or she's going

12
00:00:42,880 --> 00:00:43,080
 to

13
00:00:43,080 --> 00:00:48,130
 pass away in within seven days because there's no way she

14
00:00:48,130 --> 00:00:49,120
 could survive,

15
00:00:49,120 --> 00:00:56,390
 there's no way she could continue. It's not really possible

16
00:00:56,390 --> 00:00:58,400
." So she would

17
00:00:58,400 --> 00:01:01,010
 pass away and he was like, "Oh no, then ordain her, ordain

18
00:01:01,010 --> 00:01:02,080
 her right away."

19
00:01:02,080 --> 00:01:05,440
 Because he couldn't bear to have her disappear, bear to

20
00:01:05,440 --> 00:01:08,040
 have her gone.

21
00:01:08,040 --> 00:01:17,600
 He said, "Enough of this talk of parinibhana." The question

22
00:01:17,600 --> 00:01:18,720
, I'm sorry,

23
00:01:18,720 --> 00:01:24,460
 it's, I'm sorry to find it funny, but it really answers

24
00:01:24,460 --> 00:01:27,160
 itself. What you're

25
00:01:27,160 --> 00:01:34,070
 saying is you want to suffer in future lives. So there's

26
00:01:34,070 --> 00:01:35,960
 nothing really to worry

27
00:01:35,960 --> 00:01:39,840
 about because you have no chance. With that mind state

28
00:01:39,840 --> 00:01:41,960
 remaining, you have no

29
00:01:41,960 --> 00:01:47,910
 chance of not living anymore. You will have to come back

30
00:01:47,910 --> 00:01:49,200
 because there is

31
00:01:49,200 --> 00:01:55,280
 still this cause for future rebirth. At the moment of death

32
00:01:55,280 --> 00:01:57,020
 you don't say, "Okay,

33
00:01:57,020 --> 00:02:00,720
 enough." You say, "More, more, more. What about, what about

34
00:02:00,720 --> 00:02:03,360
 this? What about that?"

35
00:02:03,360 --> 00:02:09,480
 So don't, you don't have to wrestle with this one. You don

36
00:02:09,480 --> 00:02:11,080
't have to think, "Hmm,

37
00:02:11,080 --> 00:02:14,440
 should I practice Buddhism because if I practice Buddhism

38
00:02:14,440 --> 00:02:15,280
 maybe I won't live

39
00:02:15,280 --> 00:02:19,260
 anymore?" It's not possible. The only way that you could

40
00:02:19,260 --> 00:02:20,760
 not come back and live

41
00:02:20,760 --> 00:02:25,690
 again to suffer more and more is if you decided for

42
00:02:25,690 --> 00:02:28,320
 yourself through what I

43
00:02:28,320 --> 00:02:31,500
 would understand to be an incredible realization of the

44
00:02:31,500 --> 00:02:33,280
 truth, but you know,

45
00:02:33,280 --> 00:02:38,750
 depends who you ask, that there were no reason to come back

46
00:02:38,750 --> 00:02:40,640
. You will come to

47
00:02:40,640 --> 00:02:46,600
 realize that there was no benefit and you come to realize

48
00:02:46,600 --> 00:02:48,120
 that in fact there

49
00:02:48,120 --> 00:02:51,130
 is no one living at all. There is only actually these

50
00:02:51,130 --> 00:02:52,800
 experiences, none of which

51
00:02:52,800 --> 00:02:58,030
 are of any benefit to you or of any benefit in general,

52
00:02:58,030 --> 00:02:59,460
 none of which have no

53
00:02:59,460 --> 00:03:04,800
 intrinsic value. And when you realize that you you're not

54
00:03:04,800 --> 00:03:05,840
 born anymore, it's

55
00:03:05,840 --> 00:03:12,440
 just, it just happens. So for yeah, for as long as you, you

56
00:03:12,440 --> 00:03:13,320
 still think that there's

57
00:03:13,320 --> 00:03:19,030
 some benefit to existence, don't worry about it. Here you

58
00:03:19,030 --> 00:03:20,160
 got lots of, lots of

59
00:03:20,160 --> 00:03:22,460
 time to come back again and again, which a lot of people

60
00:03:22,460 --> 00:03:23,760
 like about Buddhism. They

61
00:03:23,760 --> 00:03:26,080
 think, "Well great, I'll just come back again and again and

62
00:03:26,080 --> 00:03:27,560
 again and eventually

63
00:03:27,560 --> 00:03:32,740
 I'll learn everything and become, become enlightened, all

64
00:03:32,740 --> 00:03:34,320
 in good time, but first

65
00:03:34,320 --> 00:03:38,080
 let me explore this kind of this novel idea that I can

66
00:03:38,080 --> 00:03:39,800
 actually, you know, do

67
00:03:39,800 --> 00:03:43,300
 good deeds and go to heaven and so on, which is fine, you

68
00:03:43,300 --> 00:03:45,840
 know. It's a kind of

69
00:03:45,840 --> 00:03:49,090
 unshaky ground because you never know where you're going to

70
00:03:49,090 --> 00:03:50,360
 go and maybe this

71
00:03:50,360 --> 00:03:54,000
 life, next life you go to heaven, but once you forget all

72
00:03:54,000 --> 00:03:55,120
 of the stuff that

73
00:03:55,120 --> 00:04:00,220
 you've learned in regards to goodness and badness, you

74
00:04:00,220 --> 00:04:00,960
 might come back as one

75
00:04:00,960 --> 00:04:07,360
 of those evil people and go to hell as well. So yeah, but

76
00:04:07,360 --> 00:04:07,960
 really that's the

77
00:04:07,960 --> 00:04:12,970
 point is that, to answer your question directly, it's

78
00:04:12,970 --> 00:04:13,680
 because there's no,

79
00:04:13,680 --> 00:04:18,920
 there's nothing good about any piece of existence. It's

80
00:04:18,920 --> 00:04:19,280
 actually a

81
00:04:19,280 --> 00:04:23,880
 delusion that we have. It's not understanding reality as it

82
00:04:23,880 --> 00:04:26,200
 is, but for

83
00:04:26,200 --> 00:04:29,290
 as long as you understand reality to have some intrinsic

84
00:04:29,290 --> 00:04:30,400
 benefit or realities,

85
00:04:30,400 --> 00:04:39,200
 as long as you understand experience, seeing, hearing,

86
00:04:39,200 --> 00:04:40,680
 smelling, tasting, feeling,

87
00:04:40,680 --> 00:04:43,830
 thinking, to have some intrinsic value, you come back and

88
00:04:43,830 --> 00:04:44,880
 there's no need to

89
00:04:44,880 --> 00:04:48,760
 worry about that. The other thing I'd say is that it's kind

90
00:04:48,760 --> 00:04:50,000
 of like, it kind of

91
00:04:50,000 --> 00:04:55,680
 like works like pulling the thread of a sweater, because

92
00:04:55,680 --> 00:04:56,200
 you got this loose

93
00:04:56,200 --> 00:04:58,160
 thread and you say, "Well that's no good, I got to get rid

94
00:04:58,160 --> 00:04:59,960
 of that," but as you pull

95
00:04:59,960 --> 00:05:03,970
 it you realize that there's, it's attached to so much more

96
00:05:03,970 --> 00:05:04,760
 and then you

97
00:05:04,760 --> 00:05:06,800
 keep pulling and pulling and pulling and eventually you're

98
00:05:06,800 --> 00:05:09,360
 left without a sweater.

99
00:05:09,360 --> 00:05:12,950
 Suffering is really in that vein. We start, we come to

100
00:05:12,950 --> 00:05:14,280
 practice not because we

101
00:05:14,280 --> 00:05:16,250
 want to become enlightened, but because we've got a lot of

102
00:05:16,250 --> 00:05:18,280
 problems. So we figure,

103
00:05:18,280 --> 00:05:22,870
 get rid of those problems and life will be fine. Then I won

104
00:05:22,870 --> 00:05:23,640
't need to come and

105
00:05:23,640 --> 00:05:25,680
 meditate, then I won't have any interest in meditate, that

106
00:05:25,680 --> 00:05:27,280
's enough. Let me get that

107
00:05:27,280 --> 00:05:32,040
 far and then quit. Problem is, as you explore these

108
00:05:32,040 --> 00:05:33,520
 questions or these

109
00:05:33,520 --> 00:05:38,550
 problems, deeper and deeper and until you get to the root

110
00:05:38,550 --> 00:05:39,320
 of the problem, you find

111
00:05:39,320 --> 00:05:42,900
 that the root is actually the root of who you are. It's the

112
00:05:42,900 --> 00:05:43,600
 root of your

113
00:05:43,600 --> 00:05:49,620
 identification with, with existence. And once that's gone,

114
00:05:49,620 --> 00:05:50,320
 then you have no

115
00:05:50,320 --> 00:05:55,030
 interest either way. There's, there's no interest in, in

116
00:05:55,030 --> 00:05:56,040
 coming back as this or

117
00:05:56,040 --> 00:06:01,570
 that. There's no aversion to existing. And so there's just

118
00:06:01,570 --> 00:06:04,360
 peace. And then at the

119
00:06:04,360 --> 00:06:12,200
 end there's freedom. So I hope that answers your question.

