1
00:00:00,000 --> 00:00:04,880
 Okay, this is in regards to feeling angry towards my mom

2
00:00:04,880 --> 00:00:07,990
 Because of how she treated me when I was growing up as a

3
00:00:07,990 --> 00:00:08,420
 child

4
00:00:08,420 --> 00:00:11,960
 I'm thinking about having a conversation

5
00:00:11,960 --> 00:00:14,440
 telling her

6
00:00:14,440 --> 00:00:18,280
 What I needed as a child from her, but wasn't receiving

7
00:00:18,280 --> 00:00:21,660
 Which has now affected me as an adult. What do you think

8
00:00:21,660 --> 00:00:22,200
 about this?

9
00:00:22,200 --> 00:00:25,700
 Would this create even more tension and negativity or would

10
00:00:25,700 --> 00:00:28,360
 it be the start of a healing process for both of us?

11
00:00:29,080 --> 00:00:31,080
 Hmm

12
00:00:31,080 --> 00:00:36,810
 In the first place it would show you very clearly the the

13
00:00:36,810 --> 00:00:39,640
 defilements that you have

14
00:00:39,640 --> 00:00:46,570
 And it shows you the wanting that there is something that

15
00:00:46,570 --> 00:00:49,760
 you want from your mother

16
00:00:49,760 --> 00:00:55,200
 that you wanted then and that you still want now and

17
00:00:57,640 --> 00:00:59,640
 It is very very

18
00:00:59,640 --> 00:01:03,890
 Good that you see this that you that you have the chance

19
00:01:03,890 --> 00:01:05,120
 now to observe

20
00:01:05,120 --> 00:01:08,680
 Your your defilements as they arise

21
00:01:08,680 --> 00:01:16,380
 If you talk with her or not is really not so important

22
00:01:16,380 --> 00:01:19,640
 What is important is that you see?

23
00:01:19,640 --> 00:01:21,800
 that

24
00:01:21,800 --> 00:01:27,330
 What has happened when you was a child is the past it is no

25
00:01:27,330 --> 00:01:29,240
 more it has gone

26
00:01:29,240 --> 00:01:31,880
 It is past

27
00:01:31,880 --> 00:01:35,280
 so now you see I

28
00:01:35,280 --> 00:01:40,840
 Wanted this and that and I didn't get it and I'm angry

29
00:01:40,840 --> 00:01:45,500
 So noted and be happy be grateful that you have the chance

30
00:01:45,500 --> 00:01:46,840
 to learn this now

31
00:01:49,640 --> 00:01:55,720
 And don't don't get deeper into it just try to let go of it

32
00:01:55,720 --> 00:02:01,770
 Learn to let go of it. Your mother is not the one she was I

33
00:02:01,770 --> 00:02:04,320
 don't know what she did and it

34
00:02:04,320 --> 00:02:06,520
 Really doesn't matter

35
00:02:06,520 --> 00:02:13,480
 Because the point is that now is now past is past and

36
00:02:13,480 --> 00:02:17,440
 You create now

37
00:02:19,320 --> 00:02:24,080
 The karma that you will have to deal with in future

38
00:02:24,080 --> 00:02:28,840
 So when you don't stop to be angry with her now

39
00:02:28,840 --> 00:02:33,080
 you will be angry with her in future and

40
00:02:33,080 --> 00:02:37,680
 The only

41
00:02:37,680 --> 00:02:42,670
 chance to change it is to stop being angry with her now and

42
00:02:42,670 --> 00:02:43,920
 you can do so by

43
00:02:43,920 --> 00:02:47,520
 seeing very clearly what is going on in your mind and

44
00:02:48,200 --> 00:02:52,960
 To do what bante you tada mo said in regards to equanimity

45
00:02:52,960 --> 00:03:00,480
 Yeah, that's just to put it into a kind of a

46
00:03:00,480 --> 00:03:05,560
 More general set in general

47
00:03:05,560 --> 00:03:09,240
 Format in general

48
00:03:09,240 --> 00:03:13,380
 Going back to the past and worrying about what has happened

49
00:03:13,380 --> 00:03:17,480
 in the past is is not recommended

50
00:03:19,240 --> 00:03:21,760
 Going back to the past is connecting who you are now with

51
00:03:21,760 --> 00:03:25,320
 who you were in the past and as Palanjani says

52
00:03:25,320 --> 00:03:28,810
 The past is past and you were a different person then the

53
00:03:28,810 --> 00:03:31,000
 point is that it reinforces the ego

54
00:03:31,000 --> 00:03:33,560
 It reinforces the idea of I

55
00:03:33,560 --> 00:03:39,400
 and and you know the whole idea of needs as well, but

56
00:03:39,400 --> 00:03:42,040
 the idea of

57
00:03:42,040 --> 00:03:44,520
 Having problems in the present based on the past

58
00:03:44,520 --> 00:03:47,920
 I mean people who are victims of abuse will often fall into

59
00:03:47,920 --> 00:03:48,920
 that kind of a

60
00:03:48,920 --> 00:03:52,840
 Mentality and really a trap. I

61
00:03:52,840 --> 00:03:56,220
 Had one meditator that I always bring up as an example when

62
00:03:56,220 --> 00:03:57,440
 I was in doi suit have

63
00:03:57,440 --> 00:03:59,320
 who

64
00:03:59,320 --> 00:04:04,420
 Was abused as a child and she came to me and she was 40.

65
00:04:04,420 --> 00:04:06,080
 She came to our meditation center

66
00:04:06,080 --> 00:04:08,720
 She was 40 something, but she looked like a ghost

67
00:04:09,040 --> 00:04:12,760
 If you look at her her hair was in her face and just this

68
00:04:12,760 --> 00:04:16,000
 horrified expression that she was like as though she was

69
00:04:16,000 --> 00:04:16,160
 seeing

70
00:04:16,160 --> 00:04:17,120
 a ghost

71
00:04:17,120 --> 00:04:19,120
 She had been raped by her father

72
00:04:19,120 --> 00:04:23,800
 When when she was young and now at 40 years old she was

73
00:04:23,800 --> 00:04:25,080
 still dealing with it

74
00:04:25,080 --> 00:04:28,840
 I mean, I don't know how long ago it was but one assumes

75
00:04:28,840 --> 00:04:31,440
 and actually I think I didn't she did tell me later

76
00:04:31,440 --> 00:04:35,320
 But the point I want to make is that and it was clear that

77
00:04:35,320 --> 00:04:37,840
 something was wrong clear that something was going on

78
00:04:37,840 --> 00:04:42,120
 But I never asked her about it and I think this really

79
00:04:42,120 --> 00:04:44,120
 puzzled her

80
00:04:44,120 --> 00:04:47,280
 You know the the whole because her whole life the therapy

81
00:04:47,280 --> 00:04:49,120
 that she'd always gone through again and again

82
00:04:49,120 --> 00:04:52,800
 For this was to go into it and to become you know

83
00:04:52,800 --> 00:04:57,200
 To somehow delve into these problems and to connect what's

84
00:04:57,200 --> 00:04:59,960
 going on now with what was going on in the past

85
00:04:59,960 --> 00:05:02,680
 and

86
00:05:02,680 --> 00:05:05,470
 We don't do that and I didn't do it with her and and as a

87
00:05:05,470 --> 00:05:07,240
 result she she started to

88
00:05:07,840 --> 00:05:10,310
 At first it was like well, don't you want to know why I'm

89
00:05:10,310 --> 00:05:13,120
 crying don't you know, what's wrong with me?

90
00:05:13,120 --> 00:05:16,240
 until eventually she realized that you know based on

91
00:05:16,240 --> 00:05:19,430
 The simple teachings that had nothing to do with any any

92
00:05:19,430 --> 00:05:21,280
 problem in the past and you know

93
00:05:21,280 --> 00:05:23,240
 I said when you're crying just say to yourself crying

94
00:05:23,240 --> 00:05:24,040
 crying don't don't

95
00:05:24,040 --> 00:05:27,270
 Don't worry about it's not a problem that you're crying as

96
00:05:27,270 --> 00:05:30,590
 there's nothing to be concerned about just say to yourself

97
00:05:30,590 --> 00:05:31,640
 crying crying

98
00:05:31,640 --> 00:05:35,970
 And it totally changed her outlook on on the problems. She

99
00:05:35,970 --> 00:05:36,800
 was able to

100
00:05:38,760 --> 00:05:40,680
 Finally cope with what was the real problem and as what's

101
00:05:40,680 --> 00:05:43,120
 occurring right here and now that

102
00:05:43,120 --> 00:05:47,060
 Really should have nothing to do with with the past the

103
00:05:47,060 --> 00:05:50,280
 past has created where you are now for sure

104
00:05:50,280 --> 00:05:53,370
 but now that's meaningless the part what's in the past is

105
00:05:53,370 --> 00:05:53,880
 gone and

106
00:05:53,880 --> 00:05:58,120
 Trying to

107
00:05:58,120 --> 00:06:01,480
 Have people make amends for it or or to

108
00:06:02,480 --> 00:06:05,470
 Know to cling to the idea that someone else has hurt you.

109
00:06:05,470 --> 00:06:07,930
 This is in the Dhamma Padra the Buddha was quite clear

110
00:06:07,930 --> 00:06:08,320
 about it

111
00:06:08,320 --> 00:06:14,230
 He beat me. He robbed me. He a coach among the Hasimung a j

112
00:06:14,230 --> 00:06:14,800
inni mung

113
00:06:14,800 --> 00:06:17,920
 however, it goes

114
00:06:17,920 --> 00:06:21,660
 He scolded me he abused me and so on for a person who har

115
00:06:21,660 --> 00:06:22,640
bors these thoughts

116
00:06:22,640 --> 00:06:25,370
 Suffering never ceases now. I know you're aware of this and

117
00:06:25,370 --> 00:06:26,760
 you don't want to be angry at her

118
00:06:26,840 --> 00:06:29,980
 But just to say that going back to the past only reaffirms

119
00:06:29,980 --> 00:06:34,440
 the idea of self of I and and and what I need

120
00:06:34,440 --> 00:06:37,480
 And so we don't do it. It's not part of our therapy

121
00:06:37,480 --> 00:06:40,120
 Our therapy is to deal with the here and the now and to

122
00:06:40,120 --> 00:06:43,560
 learn to dissociate stop making associations

123
00:06:43,560 --> 00:06:44,960
 It is what it is

124
00:06:44,960 --> 00:06:48,050
 Hence the when you feel pain to say to yourself pain do not

125
00:06:48,050 --> 00:06:50,600
 think about why you feel pain or it's because of an

126
00:06:50,600 --> 00:06:51,080
 accident

127
00:06:51,080 --> 00:06:53,080
 or this or that

128
00:06:53,280 --> 00:06:56,070
 When you feel anger to not think about why you feel anger

129
00:06:56,070 --> 00:06:57,640
 or even who you feel angry at

130
00:06:57,640 --> 00:07:01,160
 You're just feeling angry and to focus on the anger to

131
00:07:01,160 --> 00:07:05,010
 focus on the experience as it is because the whole point is

132
00:07:05,010 --> 00:07:05,280
 to

133
00:07:05,280 --> 00:07:09,600
 Lose the object when you say to yourself angry angry angry

134
00:07:09,600 --> 00:07:12,660
 You're you're breaking the connection with the other with

135
00:07:12,660 --> 00:07:14,040
 with the object of your anger

136
00:07:14,040 --> 00:07:17,060
 This is this is incredibly important as long as there's the

137
00:07:17,060 --> 00:07:19,760
 connection with the object. There's going to be

138
00:07:20,760 --> 00:07:24,070
 The object arises thinking about the object arises anger

139
00:07:24,070 --> 00:07:27,520
 arises based on the thinking based on the anger. There's

140
00:07:27,520 --> 00:07:29,840
 There's a

141
00:07:29,840 --> 00:07:32,600
 Returning to the object thinking about what you can do to

142
00:07:32,600 --> 00:07:34,440
 hurt them thinking about what they've done to you and

143
00:07:34,440 --> 00:07:35,440
 getting angry again

144
00:07:35,440 --> 00:07:37,440
 And it's a feedback loop

145
00:07:37,440 --> 00:07:40,320
 As long as you have the connection with the object you're

146
00:07:40,320 --> 00:07:41,640
 going to be creating more and more angry

147
00:07:41,640 --> 00:07:44,740
 Anger every time you think about that object when you say

148
00:07:44,740 --> 00:07:46,400
 to yourself angry angry angry

149
00:07:46,400 --> 00:07:48,680
 you change the

150
00:07:48,680 --> 00:07:52,140
 Habit in the mind that when anger arises, there's just

151
00:07:52,140 --> 00:07:52,960
 anger anger

152
00:07:52,960 --> 00:07:56,240
 It doesn't go back to the object thinking about

153
00:07:56,240 --> 00:08:00,960
 You know creating difficulties or problems and so the anger

154
00:08:00,960 --> 00:08:02,960
 the point is the anger has no fuel

155
00:08:02,960 --> 00:08:06,400
 Once the anger has no fuel and it stays for a while

156
00:08:06,400 --> 00:08:09,340
 It might even stay quite strong based on the fuel that it

157
00:08:09,340 --> 00:08:11,640
 already has from your past or from your habits

158
00:08:11,640 --> 00:08:13,880
 But eventually it disappears

159
00:08:13,880 --> 00:08:17,060
 The key to dealing with these things is to see them for

160
00:08:17,060 --> 00:08:19,160
 what they are and and nothing more

161
00:08:19,160 --> 00:08:21,760
 you may have

162
00:08:21,760 --> 00:08:26,520
 Intense states of anger or intense states of greed and

163
00:08:26,520 --> 00:08:30,820
 That's not the biggest problem. The biggest it's not the

164
00:08:30,820 --> 00:08:31,600
 real problem

165
00:08:31,600 --> 00:08:34,140
 The real problem is when you follow after them if you're

166
00:08:34,140 --> 00:08:35,440
 able to be mindful

167
00:08:35,440 --> 00:08:39,380
 No wanting wanting or angry angry and see them for what

168
00:08:39,380 --> 00:08:40,080
 they are

169
00:08:42,360 --> 00:08:44,360
 You see that they're really harmless

170
00:08:44,360 --> 00:08:47,120
 They're the only thing that they're doing is bringing you

171
00:08:47,120 --> 00:08:50,210
 more stress in the mind. And so you quickly could do away

172
00:08:50,210 --> 00:08:50,760
 with them

173
00:08:50,760 --> 00:08:55,930
 That's really how we we deal with these things. So just

174
00:08:55,930 --> 00:08:56,480
 meant to

175
00:08:56,480 --> 00:08:59,440
 To sort of generalize what you'd already say

176
00:08:59,440 --> 00:09:01,440
 You

