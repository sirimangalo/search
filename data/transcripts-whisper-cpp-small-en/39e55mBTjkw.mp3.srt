1
00:00:00,000 --> 00:00:08,580
 Namarupa is interesting. It's a bit of a dilemma really and

2
00:00:08,580 --> 00:00:15,010
 it's the dilemma that science has faced and fought against

3
00:00:15,010 --> 00:00:17,000
 in many ways.

4
00:00:17,000 --> 00:00:32,090
 The popular view according to science is that, well

5
00:00:32,090 --> 00:00:32,790
 according to many scientists, is that there's only the

6
00:00:32,790 --> 00:00:33,000
 material.

7
00:00:33,000 --> 00:00:37,050
 And materialism has become quite popular or has always been

8
00:00:37,050 --> 00:00:42,150
 I guess quite popular among material scientists, people who

9
00:00:42,150 --> 00:00:45,000
 study the physical side of reality.

10
00:00:45,000 --> 00:00:47,950
 So they want to be able to say that the mental side of

11
00:00:47,950 --> 00:00:52,780
 reality is an epiphenomenon. It's something that arises

12
00:00:52,780 --> 00:00:57,260
 based on the physical and is totally dependent on the

13
00:00:57,260 --> 00:01:02,000
 physical, has no power to influence reality.

14
00:01:02,000 --> 00:01:07,230
 It's like harmony that arises or sound that arises when you

15
00:01:07,230 --> 00:01:13,000
 clap your hands. The sound is dependent on the hands.

16
00:01:13,000 --> 00:01:21,970
 Buddhism isn't that different. I mean much consciousness

17
00:01:21,970 --> 00:01:27,500
 does come from the physical and is conditioned very much by

18
00:01:27,500 --> 00:01:29,000
 the physical.

19
00:01:29,000 --> 00:01:36,150
 But it's really making an assumption to say that the

20
00:01:36,150 --> 00:01:41,390
 relationship is one way. When you could just as easily say

21
00:01:41,390 --> 00:01:44,020
 the relationship goes the other way, you could postulate

22
00:01:44,020 --> 00:01:47,800
 and run experiments in the same way as people do in the

23
00:01:47,800 --> 00:01:49,000
 material realm.

24
00:01:49,000 --> 00:01:52,340
 You could run these experiments in the mental realm and

25
00:01:52,340 --> 00:01:56,000
 come to the conclusion that the physical doesn't exist.

26
00:01:56,000 --> 00:01:59,180
 The physical is just an epiphenomenon that is conditioned

27
00:01:59,180 --> 00:02:05,210
 by the mind. In other words, all that exists is it's like a

28
00:02:05,210 --> 00:02:06,000
 dream.

29
00:02:06,000 --> 00:02:09,250
 And this is kind of I suppose similar to solipsism,

30
00:02:09,250 --> 00:02:15,410
 although it's not really solipsistic. It's just a purely

31
00:02:15,410 --> 00:02:23,000
 mental reality that all is just mind.

32
00:02:23,000 --> 00:02:27,100
 And mind is the only thing that exists. Now it's pretty

33
00:02:27,100 --> 00:02:31,020
 clear that the Buddha didn't ascribe to either of these

34
00:02:31,020 --> 00:02:32,000
 theories.

35
00:02:32,000 --> 00:02:35,400
 And he didn't really ascribe to theories at all. This is

36
00:02:35,400 --> 00:02:38,470
 why I don't particularly like to talk about such things

37
00:02:38,470 --> 00:02:43,230
 because or why I might be hesitant to talk about it because

38
00:02:43,230 --> 00:02:45,000
 what's to talk about?

39
00:02:45,000 --> 00:02:47,950
 Namarupa is something that you experience and it's

40
00:02:47,950 --> 00:02:51,010
 something that you come to in the very early stages of

41
00:02:51,010 --> 00:02:52,000
 meditation.

42
00:02:52,000 --> 00:02:55,310
 But why I think on the other hand it can be good to talk to

43
00:02:55,310 --> 00:02:58,920
 it about people who are meditating is because when you just

44
00:02:58,920 --> 00:03:02,140
 start out you really don't know what it is that you're

45
00:03:02,140 --> 00:03:03,000
 seeing.

46
00:03:03,000 --> 00:03:06,390
 You look at it and you wonder might it be this, might it be

47
00:03:06,390 --> 00:03:12,000
 that? And you get the idea of me and mind and self.

48
00:03:12,000 --> 00:03:15,310
 So the idea of nama rupa, the Buddha didn't say one way or

49
00:03:15,310 --> 00:03:18,810
 other whether nama comes from rupa, rupa comes from nama or

50
00:03:18,810 --> 00:03:22,000
 whether they both exist independently or so on.

51
00:03:22,000 --> 00:03:30,650
 He likened them to two men, two people. One of them, I

52
00:03:30,650 --> 00:03:33,270
 believe it was the Buddha or one of his disciples, I can't

53
00:03:33,270 --> 00:03:34,000
 remember.

54
00:03:34,000 --> 00:03:37,680
 Anyway, there's a simile of these two men, one who is

55
00:03:37,680 --> 00:03:42,380
 crippled and one who is blind. And so the body is like this

56
00:03:42,380 --> 00:03:47,000
 blind man who can't see but has good legs and can walk.

57
00:03:47,000 --> 00:03:52,500
 And the mind is like a crippled person who can see fine but

58
00:03:52,500 --> 00:03:56,000
 can't walk. And so they work together.

59
00:03:56,000 --> 00:03:59,960
 The cripple gets on the back of the blind man and they walk

60
00:03:59,960 --> 00:04:04,000
 around and the cripple says go this way, go that way.

61
00:04:04,000 --> 00:04:08,000
 And basically the idea here is that they're interdependent.

62
00:04:08,000 --> 00:04:10,420
 They rely on each other and they perform different

63
00:04:10,420 --> 00:04:11,000
 functions.

64
00:04:11,000 --> 00:04:15,140
 But it isn't really a philosophical teaching as to whether

65
00:04:15,140 --> 00:04:18,000
 one or the other is real or is not real.

66
00:04:18,000 --> 00:04:22,260
 And I think that's an important, that's the best way to

67
00:04:22,260 --> 00:04:26,320
 look at the Buddha's teaching. I don't think we should get

68
00:04:26,320 --> 00:04:31,970
 into the idea of existence or reality because there's such

69
00:04:31,970 --> 00:04:33,000
 arbitrary words.

70
00:04:33,000 --> 00:04:36,670
 How do you define whether something really exists? You know

71
00:04:36,670 --> 00:04:40,000
, besides just saying that it's experienced.

72
00:04:40,000 --> 00:04:43,640
 The importance of nama rupa, especially for meditators, is

73
00:04:43,640 --> 00:04:46,790
 not whether one or the other is real or whether they're

74
00:04:46,790 --> 00:04:48,000
 both real or so on.

75
00:04:48,000 --> 00:04:51,000
 It's that they are all that is real.

76
00:04:51,000 --> 00:04:56,620
 Nama rupa or the mental and physical experience, and we don

77
00:04:56,620 --> 00:05:02,030
't even have to separate them, but the experience that is

78
00:05:02,030 --> 00:05:07,620
 partly described as mental and partly described as physical

79
00:05:07,620 --> 00:05:10,000
 is all that exists.

80
00:05:10,000 --> 00:05:14,260
 And so we're not trying to define one in contrast with the

81
00:05:14,260 --> 00:05:18,440
 other. We're trying to define them in contrast with

82
00:05:18,440 --> 00:05:21,000
 illusion, which is the self.

83
00:05:21,000 --> 00:05:24,750
 The first thing you realize as a meditator is that it

84
00:05:24,750 --> 00:05:27,000
 appears that there are two things.

85
00:05:27,000 --> 00:05:30,320
 There's the physical, which is like the stomach rising and

86
00:05:30,320 --> 00:05:33,000
 falling, and then there's the mind that goes to know it.

87
00:05:33,000 --> 00:05:36,310
 They arise together. They cease together. But neither one

88
00:05:36,310 --> 00:05:41,600
 continues on after their allotted time. The rising starts

89
00:05:41,600 --> 00:05:43,000
 and it stops.

90
00:05:43,000 --> 00:05:48,760
 And the stopping is total cessation. That phenomenon is

91
00:05:48,760 --> 00:05:50,000
 gone.

92
00:05:50,000 --> 00:05:53,980
 The knowing of the rising is also gone. It's gone at the

93
00:05:53,980 --> 00:05:56,000
 moment that the rising ends.

94
00:05:56,000 --> 00:06:04,880
 So they arise and cease together. The foot moving, when you

95
00:06:04,880 --> 00:06:06,510
 say stepping right, stepping left, the foot moving is

96
00:06:06,510 --> 00:06:07,000
 physical.

97
00:06:07,000 --> 00:06:10,480
 The mind that knows the foot is moving, that's mental. Now,

98
00:06:10,480 --> 00:06:14,870
 whether you say they're one or two or so on, they are

99
00:06:14,870 --> 00:06:17,000
 clearly distinguishable.

100
00:06:17,000 --> 00:06:20,000
 If you want to explain what's going on, you have to say,

101
00:06:20,000 --> 00:06:23,030
 well, there's the foot moving and then there's the mind

102
00:06:23,030 --> 00:06:24,000
 that knows it.

103
00:06:24,000 --> 00:06:28,540
 But if you look carefully at it, you'll see that what there

104
00:06:28,540 --> 00:06:32,000
 isn't is some being involved in the process.

105
00:06:32,000 --> 00:06:35,730
 And look carefully. It just means if you practice it again

106
00:06:35,730 --> 00:06:39,000
 and again and again and look again and again eventually,

107
00:06:39,000 --> 00:06:45,620
 your mind will start to focus and to see clearly what's

108
00:06:45,620 --> 00:06:50,000
 going on, that this is a process,

109
00:06:50,000 --> 00:06:56,000
 a phenomena that arises and sees and arises incessantly.

110
00:06:56,000 --> 00:07:00,260
 This is called momentary death. And this is one of the

111
00:07:00,260 --> 00:07:02,000
 first things we realize.

112
00:07:02,000 --> 00:07:05,500
 If people don't understand it, my teacher would often give

113
00:07:05,500 --> 00:07:10,880
 a talk, just a short talk explaining this, the different

114
00:07:10,880 --> 00:07:12,000
 kinds of death.

115
00:07:12,000 --> 00:07:16,560
 Like normally death, we think of a person dying. And so he

116
00:07:16,560 --> 00:07:20,000
 says that that kind of death is just conceptual.

117
00:07:20,000 --> 00:07:23,940
 It's not real. It's not a part of reality. Reality is that

118
00:07:23,940 --> 00:07:26,000
 the mind continues on and on and on.

119
00:07:26,000 --> 00:07:29,000
 The physical actually continues on and on. It just changes.

120
00:07:29,000 --> 00:07:31,470
 So at the moment of death, well, it's just like a wave

121
00:07:31,470 --> 00:07:36,000
 crashing against the shore. The ocean remains the same.

122
00:07:36,000 --> 00:07:42,860
 So a wave has a beginning and an end. But the stuff that it

123
00:07:42,860 --> 00:07:45,000
's made of, in this case water,

124
00:07:45,000 --> 00:07:49,660
 or in our case physical and mental phenomena or experiences

125
00:07:49,660 --> 00:07:51,000
, continues.

126
00:07:51,000 --> 00:07:56,000
 And it's not really important whether it does or not.

127
00:07:56,000 --> 00:07:59,000
 Our experience right now is that it continues on and on.

128
00:07:59,000 --> 00:08:02,850
 People want to debate as to whether rebirth is important

129
00:08:02,850 --> 00:08:06,000
 considering that we can't experience it.

130
00:08:06,000 --> 00:08:09,280
 It's not really so important. If you wait long enough,

131
00:08:09,280 --> 00:08:11,000
 eventually you'll experience it.

132
00:08:11,000 --> 00:08:15,500
 But what we're experiencing now is what is real. The idea

133
00:08:15,500 --> 00:08:18,000
 that we might die is also in the future.

134
00:08:18,000 --> 00:08:20,660
 What is real is our death at every moment, our birth and

135
00:08:20,660 --> 00:08:23,000
 death, birth and death, birth and death.

136
00:08:23,000 --> 00:08:25,530
 And this cuts through the delusion of self, which is

137
00:08:25,530 --> 00:08:27,000
 incredibly important.

138
00:08:27,000 --> 00:08:31,000
 Many of you are aware of why it's so important.

139
00:08:31,000 --> 00:08:35,660
 But if you haven't heard much about Buddhism, the reason

140
00:08:35,660 --> 00:08:38,000
 why it's so important is

141
00:08:38,000 --> 00:08:42,000
 this is really the true cause of our suffering.

142
00:08:42,000 --> 00:08:45,000
 The fact that we attach to things, identify with them,

143
00:08:45,000 --> 00:08:48,000
 think of them as me and mine, as I,

144
00:08:48,000 --> 00:08:52,000
 and therefore begin to judge them.

145
00:08:52,000 --> 00:08:55,140
 The reason why we say something is good is we think it's

146
00:08:55,140 --> 00:08:56,000
 good for me.

147
00:08:56,000 --> 00:08:58,790
 And we want to get it for ourselves because we have the

148
00:08:58,790 --> 00:09:03,000
 idea of a self that experiences pleasure and pain.

149
00:09:03,000 --> 00:09:05,760
 When you start to see in this way, you realize that

150
00:09:05,760 --> 00:09:08,000
 pleasure and pain arise and cease on their own.

151
00:09:08,000 --> 00:09:11,190
 And there's really no experience or there's the mind, but

152
00:09:11,190 --> 00:09:15,160
 that mind arises and ceases with the pleasure and with the

153
00:09:15,160 --> 00:09:16,000
 pain.

154
00:09:16,000 --> 00:09:19,460
 And again, this, this, I think for many people, will just

155
00:09:19,460 --> 00:09:22,790
 sound like an intellectual theory and go way over their

156
00:09:22,790 --> 00:09:23,000
 head

157
00:09:23,000 --> 00:09:26,610
 and they're not meditating. But my hope is that many of you

158
00:09:26,610 --> 00:09:30,000
 are meditating and therefore this should actually make some

159
00:09:30,000 --> 00:09:30,000
 sense

160
00:09:30,000 --> 00:09:34,000
 and relate in some way to what you're practicing.

161
00:09:34,000 --> 00:09:38,000
 So that's a little bit about Nama and Rupa.

162
00:09:38,000 --> 00:09:41,410
 For those of you who don't know these words, Nama, the

163
00:09:41,410 --> 00:09:45,000
 definition of Nama is that which knows an object.

164
00:09:45,000 --> 00:09:48,360
 The definition of Rupa is something that doesn't know an

165
00:09:48,360 --> 00:09:51,000
 object, doesn't have awareness.

166
00:09:51,000 --> 00:09:54,250
 So the mind is the awareness. When you move the foot, there

167
00:09:54,250 --> 00:09:56,000
's the Rupa, which doesn't know anything.

168
00:09:56,000 --> 00:10:00,000
 It's just the movement. And then there's the knowing of it.

169
00:10:00,000 --> 00:10:05,600
 And they go together. The Rupa occurs and the Nama knows it

170
00:10:05,600 --> 00:10:06,000
.

171
00:10:06,000 --> 00:10:09,740
 So it just means body and mind, really. That's the language

172
00:10:09,740 --> 00:10:11,000
 that we use.

