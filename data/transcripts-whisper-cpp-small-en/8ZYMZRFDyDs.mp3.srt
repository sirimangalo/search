1
00:00:00,000 --> 00:00:01,000
 Go ahead.

2
00:00:01,000 --> 00:00:05,000
 I have a friend who suffers from a severe disease and only

3
00:00:05,000 --> 00:00:06,960
 has one month and a half

4
00:00:06,960 --> 00:00:08,880
 according to the doctors.

5
00:00:08,880 --> 00:00:13,160
 I'm able to deal with this much better than my friend's.

6
00:00:13,160 --> 00:00:16,300
 Problem is, I don't know how to help them through this

7
00:00:16,300 --> 00:00:17,000
 better.

8
00:00:17,000 --> 00:00:18,000
 [Pause]

9
00:00:18,000 --> 00:00:19,000
 Yeah.

10
00:00:19,000 --> 00:00:45,000
 So the question is not about your friend who has only one

11
00:00:45,000 --> 00:00:45,080
 month and a half, but he has

12
00:00:45,080 --> 00:00:48,470
 only one month and a half, but about the other friends

13
00:00:48,470 --> 00:00:49,880
 around both of you.

14
00:00:49,880 --> 00:00:52,040
 Do I understand that correctly?

15
00:00:52,040 --> 00:00:58,970
 How to help other people deal with a friend who's dying or

16
00:00:58,970 --> 00:01:01,760
 when a friend dies?

17
00:01:01,760 --> 00:01:07,860
 I think the first thing to do is to make them see, maybe

18
00:01:07,860 --> 00:01:11,520
 you find a careful way to do that,

19
00:01:11,520 --> 00:01:17,230
 to make them see that death is very much part of life and

20
00:01:17,230 --> 00:01:21,120
 that everybody has to face it

21
00:01:21,120 --> 00:01:27,560
 sooner or later and that from the beginning we are born, we

22
00:01:27,560 --> 00:01:30,920
 are dying, we are, of course,

23
00:01:30,920 --> 00:01:36,080
 some years are building up strength and power and so on.

24
00:01:36,080 --> 00:01:46,420
 And I think from age 18 or so, we start actually to age and

25
00:01:46,420 --> 00:01:48,760
 to get old.

26
00:01:48,760 --> 00:02:00,880
 So this is a fact and it is very important to accept this.

27
00:02:00,880 --> 00:02:05,580
 Although most people run away from seeing this fact and try

28
00:02:05,580 --> 00:02:07,760
 to pretend this will never

29
00:02:07,760 --> 00:02:12,900
 happen to me, so I think the most important is to stop them

30
00:02:12,900 --> 00:02:15,280
 from running away and make

31
00:02:15,280 --> 00:02:20,670
 them understand that it is going to happen and that it is

32
00:02:20,670 --> 00:02:23,840
 something very natural, that

33
00:02:23,840 --> 00:02:36,760
 it is nothing that we have to fear, although most people

34
00:02:36,760 --> 00:02:44,120
 fear it, but we should try to

35
00:02:44,120 --> 00:02:54,480
 see it as natural happening.

36
00:02:54,480 --> 00:03:03,810
 I don't know how I have something in mind, but I'm not sure

37
00:03:03,810 --> 00:03:06,840
 how to say that.

38
00:03:06,840 --> 00:03:12,930
 Please go ahead, you and maybe I come with my thought later

39
00:03:12,930 --> 00:03:13,360
.

40
00:03:13,360 --> 00:03:16,140
 You do have to mention what we were talking about in the

41
00:03:16,140 --> 00:03:23,880
 last question as well, that you

42
00:03:23,880 --> 00:03:27,710
 have to set yourself in what is right first before you can

43
00:03:27,710 --> 00:03:28,840
 help others.

44
00:03:28,840 --> 00:03:36,950
 So looking out for other people and trying to give them the

45
00:03:36,950 --> 00:03:41,080
 answers to life is not really

46
00:03:41,080 --> 00:03:44,200
 in the cards so much.

47
00:03:44,200 --> 00:03:48,440
 Even the Buddha was only able to show the way and he said,

48
00:03:48,440 --> 00:03:50,120
 "It's up to you to do the

49
00:03:50,120 --> 00:03:51,120
 work."

50
00:03:51,120 --> 00:03:56,880
 So, yeah, stating the obvious and letting them know that

51
00:03:56,880 --> 00:03:59,320
 death is a part of life is

52
00:03:59,320 --> 00:04:02,840
 important, it takes a lot of work and a lot of

53
00:04:02,840 --> 00:04:07,760
 understanding and you have to develop yourself.

54
00:04:07,760 --> 00:04:10,240
 You really have to be a meditation teacher to answer your

55
00:04:10,240 --> 00:04:11,480
 question because if you're

56
00:04:11,480 --> 00:04:14,580
 a meditation teacher then you do have something to give to

57
00:04:14,580 --> 00:04:15,080
 them.

58
00:04:15,080 --> 00:04:18,180
 If you've done the training and know how to teach people

59
00:04:18,180 --> 00:04:20,040
 meditation, that's all I could

60
00:04:20,040 --> 00:04:23,380
 answer is saying, "Find them a meditation teacher or train

61
00:04:23,380 --> 00:04:24,880
 yourself as a meditation

62
00:04:24,880 --> 00:04:27,760
 teacher because we will all meet with these people."

63
00:04:27,760 --> 00:04:30,510
 That's why I have everyone who comes here try to encourage

64
00:04:30,510 --> 00:04:32,000
 them to train as a meditation

65
00:04:32,000 --> 00:04:40,080
 teacher because that's what you can actually give to people

66
00:04:40,080 --> 00:04:40,400
.

67
00:04:40,400 --> 00:04:50,980
 Of course, you can only ever point them the way, you can't

68
00:04:50,980 --> 00:04:54,520
 make them drink.

69
00:04:54,520 --> 00:05:00,680
 I think my thought was going in that direction that I

70
00:05:00,680 --> 00:05:04,880
 wanted to find a shortcut in how you

71
00:05:04,880 --> 00:05:09,010
 could tell your friends that are actually in that situation

72
00:05:09,010 --> 00:05:10,760
 of losing a friend in one

73
00:05:10,760 --> 00:05:14,800
 and a half months.

74
00:05:14,800 --> 00:05:18,800
 That might be very scary for them.

75
00:05:18,800 --> 00:05:24,190
 It is probably very important for them to say everything

76
00:05:24,190 --> 00:05:27,880
 that they want to say to that person

77
00:05:27,880 --> 00:05:34,740
 and to bid farewell already so that in the end there's

78
00:05:34,740 --> 00:05:37,520
 nothing left to say.

79
00:05:37,520 --> 00:05:41,660
 They spend all the time that they need to spend with that

80
00:05:41,660 --> 00:05:43,680
 person, that they ask for

81
00:05:43,680 --> 00:05:49,210
 forgiveness for things that they have done wrong or ask

82
00:05:49,210 --> 00:05:52,760
 that person to ask for forgiveness

83
00:05:52,760 --> 00:05:57,640
 from them if there is anything unclear among them, this

84
00:05:57,640 --> 00:05:59,920
 should be cleared out before the

85
00:05:59,920 --> 00:06:00,920
 person dies.

86
00:06:00,920 --> 00:06:02,520
 That's a good point.

87
00:06:02,520 --> 00:06:05,760
 We ... Sorry to interrupt but ... No, no, go ahead.

88
00:06:05,760 --> 00:06:12,360
 Just totally to add to that.

89
00:06:12,360 --> 00:06:15,250
 Actually we don't know when we're going to die and I had a

90
00:06:15,250 --> 00:06:16,840
 friend in Los Angeles, he's

91
00:06:16,840 --> 00:06:20,400
 a Buddhist and his brother died.

92
00:06:20,400 --> 00:06:24,060
 Like fell down the stairs or got hit by something and went

93
00:06:24,060 --> 00:06:26,160
 into a coma and died within a few

94
00:06:26,160 --> 00:06:28,440
 days so they didn't have the chance.

95
00:06:28,440 --> 00:06:30,680
 You've got a month and a half with this guy, right?

96
00:06:30,680 --> 00:06:35,040
 That's a great point that you made is that you now have the

97
00:06:35,040 --> 00:06:37,120
 time to find closure with

98
00:06:37,120 --> 00:06:40,220
 this person and that's really how we should look at all of

99
00:06:40,220 --> 00:06:40,600
 us.

100
00:06:40,600 --> 00:06:42,840
 We should look around us at everyone.

101
00:06:42,840 --> 00:06:46,520
 This is how the Buddha would have us do with everyone.

102
00:06:46,520 --> 00:06:48,200
 Don't spend your time fighting.

103
00:06:48,200 --> 00:06:54,820
 Don't spend your time prancing around as though tomorrow

104
00:06:54,820 --> 00:06:57,680
 wasn't going to come.

105
00:06:57,680 --> 00:06:59,640
 You don't know when death is going to come to yourself or

106
00:06:59,640 --> 00:07:00,680
 to the people around you if

107
00:07:00,680 --> 00:07:05,290
 you waste your time arguing and fighting and don't get the

108
00:07:05,290 --> 00:07:07,560
 chance to find closure with

109
00:07:07,560 --> 00:07:08,560
 the person.

110
00:07:08,560 --> 00:07:12,740
 This is what leads people to suffer horribly, to be

111
00:07:12,740 --> 00:07:15,880
 unprepared for their own death and for

112
00:07:15,880 --> 00:07:17,440
 the death of other people.

113
00:07:17,440 --> 00:07:20,320
 It's a great point to take the time.

114
00:07:20,320 --> 00:07:26,790
 One other thing I wanted to say, if you have more please,

115
00:07:26,790 --> 00:07:29,880
 is that in line with our not

116
00:07:29,880 --> 00:07:32,360
 being able to help other people, we always do this.

117
00:07:32,360 --> 00:07:35,280
 We go to funerals and I've seen it several times already.

118
00:07:35,280 --> 00:07:38,880
 You go to the funeral and you explain all of this about how

119
00:07:38,880 --> 00:07:40,680
 we should understand death

120
00:07:40,680 --> 00:07:44,490
 and death is a part of life and that we shouldn't cry and

121
00:07:44,490 --> 00:07:46,360
 we shouldn't feel bad.

122
00:07:46,360 --> 00:07:47,360
 It doesn't help the person.

123
00:07:47,360 --> 00:07:51,080
 It doesn't help us.

124
00:07:51,080 --> 00:07:53,900
 What we should be doing is doing good deeds on behalf of

125
00:07:53,900 --> 00:07:54,840
 this person.

126
00:07:54,840 --> 00:07:57,320
 Let their name live on.

127
00:07:57,320 --> 00:08:02,980
 Do something to carry on our good relationship with them to

128
00:08:02,980 --> 00:08:04,920
 find the closure.

129
00:08:04,920 --> 00:08:08,880
 To say, "Well, I have this attachment to this person.

130
00:08:08,880 --> 00:08:11,310
 Every time I think about them, I'm going to use that as an

131
00:08:11,310 --> 00:08:15,480
 impetus to do more good deeds."

132
00:08:15,480 --> 00:08:20,410
 I just remember a girl here from the village very growing

133
00:08:20,410 --> 00:08:21,960
 on alms round.

134
00:08:21,960 --> 00:08:26,020
 She speaks good English and she came the other day to give

135
00:08:26,020 --> 00:08:28,760
 a donation, to put it in the donation

136
00:08:28,760 --> 00:08:31,800
 box when we were present.

137
00:08:31,800 --> 00:08:37,600
 Bante asked if there is a special reason for it.

138
00:08:37,600 --> 00:08:41,800
 She said, "Yes, for this and that person died."

139
00:08:41,800 --> 00:08:46,240
 We said, "Oh, condolences," and so on.

140
00:08:46,240 --> 00:08:50,400
 She said, "Oh, that's 18 years ago."

141
00:08:50,400 --> 00:08:51,400
 Still they do it.

142
00:08:51,400 --> 00:08:53,520
 They do it every month.

143
00:08:53,520 --> 00:08:58,280
 They do something for that person.

144
00:08:58,280 --> 00:09:01,900
 The conclusion that I wanted to bring up is just some sort

145
00:09:01,900 --> 00:09:03,360
 of food for thought.

146
00:09:03,360 --> 00:09:07,770
 We always give this kind of teaching, especially

147
00:09:07,770 --> 00:09:11,040
 emphasizing the "don't cry" part.

148
00:09:11,040 --> 00:09:14,000
 Our turn is over and the whole family comes up and says

149
00:09:14,000 --> 00:09:14,920
 therapies.

150
00:09:14,920 --> 00:09:18,920
 They all break down crying invariably.

151
00:09:18,920 --> 00:09:21,250
 You might give this profound "dhamma talk" about how death

152
00:09:21,250 --> 00:09:22,800
 is just a natural part of

153
00:09:22,800 --> 00:09:23,800
 life.

154
00:09:23,800 --> 00:09:30,680
 It's not something, but the attachment, this is the habits

155
00:09:30,680 --> 00:09:33,760
 that have been built up.

156
00:09:33,760 --> 00:09:34,760
 You can't break that.

157
00:09:34,760 --> 00:09:37,800
 You can't control it.

158
00:09:37,800 --> 00:09:43,400
 On the one hand, you might for that reason say they're

159
00:09:43,400 --> 00:09:46,640
 crying or their sadness is the

160
00:09:46,640 --> 00:09:52,540
 natural outcome of the years and years of clinging and

161
00:09:52,540 --> 00:09:55,760
 their inability to understand

162
00:09:55,760 --> 00:09:58,310
 the feelings that arise when sadness arises, the inability

163
00:09:58,310 --> 00:09:59,800
 to acknowledge it, the inability

164
00:09:59,800 --> 00:10:02,000
 to see it for what it is.

165
00:10:02,000 --> 00:10:04,400
 It leads to, "What am I going to do?"

166
00:10:04,400 --> 00:10:05,400
 "Oh, and then sad again."

167
00:10:05,400 --> 00:10:06,400
 "Oh, what am I going to do?"

168
00:10:06,400 --> 00:10:07,400
 "Is sad again."

169
00:10:07,400 --> 00:10:08,400
 Feeding it.

170
00:10:08,400 --> 00:10:13,560
 It creates a feedback loop.

171
00:10:13,560 --> 00:10:19,300
 You may not be able to help people to not cry and not be

172
00:10:19,300 --> 00:10:20,080
 sad.

173
00:10:20,080 --> 00:10:27,460
 On the other hand, you can see that as a guideline for how

174
00:10:27,460 --> 00:10:32,000
 to actually help these people.

175
00:10:32,000 --> 00:10:34,940
 You can't make them not feel sad, but you can help them to

176
00:10:34,940 --> 00:10:36,320
 deal with the sadness and

177
00:10:36,320 --> 00:10:38,080
 say it's natural to be sad.

178
00:10:38,080 --> 00:10:41,380
 Not say we shouldn't feel sad, but say when we feel sad, we

179
00:10:41,380 --> 00:10:42,920
 should be mindful of it and

180
00:10:42,920 --> 00:10:47,880
 we understand where the sadness comes, teaching people why

181
00:10:47,880 --> 00:10:49,400
 they are feeling sad.

182
00:10:49,400 --> 00:10:54,830
 Often you can talk to them after and say, "Wow, that was

183
00:10:54,830 --> 00:10:56,760
 really moving."

184
00:10:56,760 --> 00:11:00,120
 They'll say, "Yeah, I didn't think I was going to cry."

185
00:11:00,120 --> 00:11:02,470
 They didn't feel like they were going to cry because they

186
00:11:02,470 --> 00:11:03,760
're hiding it all inside.

187
00:11:03,760 --> 00:11:06,430
 As soon as they get up there and they start to talk about

188
00:11:06,430 --> 00:11:07,960
 the person, everyone breaks

189
00:11:07,960 --> 00:11:08,960
 down.

190
00:11:08,960 --> 00:11:09,960
 It's quite interesting.

191
00:11:09,960 --> 00:11:16,480
 It's a funeral phenomenon.

192
00:11:16,480 --> 00:11:19,930
 Help them when the sadness comes up to understand why it's

193
00:11:19,930 --> 00:11:22,840
 there, why it's coming out of nowhere

194
00:11:22,840 --> 00:11:31,400
 and how to deal with it, how to be mindful of it and so on.

195
00:11:31,400 --> 00:11:38,310
 One thing I wanted to add that is only far related to your

196
00:11:38,310 --> 00:11:41,880
 question, but it is a great

197
00:11:41,880 --> 00:11:45,090
 lesson maybe for yourself and for your friends when a

198
00:11:45,090 --> 00:11:47,200
 friend is dying and you can be with

199
00:11:47,200 --> 00:11:49,600
 a friend.

200
00:11:49,600 --> 00:11:52,760
 You can learn a lot from that.

201
00:11:52,760 --> 00:11:58,820
 Be grateful for that friend that this person shares this

202
00:11:58,820 --> 00:12:02,160
 experience of dying with you and

203
00:12:02,160 --> 00:12:06,040
 learn as much as you can from that.

204
00:12:06,040 --> 00:12:10,320
 Take the lesson very well.

205
00:12:10,320 --> 00:12:16,040
 The Pali word is marana-musati, the reflection on death

206
00:12:16,040 --> 00:12:19,520
 that helps maybe to understand some

207
00:12:19,520 --> 00:12:22,960
 of life.

