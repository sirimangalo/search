1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:12,000
 Today we continue with verse 251, which reads as follows.

3
00:00:12,000 --> 00:00:21,000
 Nati raga samo hagi, nati dosa samo gaho,

4
00:00:21,000 --> 00:00:30,000
 nati moha samang jalang, nati tanha samang samo nadi.

5
00:00:30,000 --> 00:00:39,000
 Which means, there is no fire like passion.

6
00:00:39,000 --> 00:00:49,000
 There is no grip as tight as anger.

7
00:00:49,000 --> 00:01:01,000
 There is no net equal to ignorant delusion.

8
00:01:01,000 --> 00:01:11,000
 There is no river like craving.

9
00:01:11,000 --> 00:01:22,000
 This verse was taught in response to a story of five men,

10
00:01:22,000 --> 00:01:29,000
 who the story says came to listen to the Buddha's teaching.

11
00:01:29,000 --> 00:01:34,250
 The story makes clear that the Buddha teaches out of

12
00:01:34,250 --> 00:01:39,000
 respect for the Dhamma.

13
00:01:39,000 --> 00:01:48,000
 And he doesn't always concern himself with the audience.

14
00:01:48,000 --> 00:01:52,280
 He teaches out of respect for the Dhamma and to lay down

15
00:01:52,280 --> 00:01:54,000
 the Dhamma,

16
00:01:54,000 --> 00:01:58,480
 even though sometimes the people listening might not

17
00:01:58,480 --> 00:02:00,000
 understand it.

18
00:02:00,000 --> 00:02:07,050
 So the story goes that these five men came to listen to the

19
00:02:07,050 --> 00:02:08,000
 Dhamma,

20
00:02:08,000 --> 00:02:12,980
 and of the five men, the first of them, while he was

21
00:02:12,980 --> 00:02:16,000
 sitting there listening to the Dhamma, fell asleep.

22
00:02:16,000 --> 00:02:24,000
 The guest fell asleep sitting up, right in the middle of

23
00:02:24,000 --> 00:02:24,000
 the Buddha's Dhamma talk.

24
00:02:24,000 --> 00:02:27,000
 Could you imagine?

25
00:02:27,000 --> 00:02:31,740
 The second one, while listening to the Dhamma, wasn't

26
00:02:31,740 --> 00:02:34,000
 really listening.

27
00:02:34,000 --> 00:02:38,000
 Instead he was poking at the ground.

28
00:02:38,000 --> 00:02:43,850
 He was making little doodles in the ground, squiggly lines

29
00:02:43,850 --> 00:02:47,000
 and etchings into the ground,

30
00:02:47,000 --> 00:02:52,000
 kind of doodling with his hand.

31
00:02:52,000 --> 00:02:56,000
 Never seen someone do that when they're sitting.

32
00:02:56,000 --> 00:03:00,000
 They're clearly not listening.

33
00:03:00,000 --> 00:03:03,480
 And the third man listening, I guess they were in the

34
00:03:03,480 --> 00:03:04,000
 forest,

35
00:03:04,000 --> 00:03:08,440
 I guess he got bored because he saw this little tree next

36
00:03:08,440 --> 00:03:10,000
 to him and he started shaking it.

37
00:03:10,000 --> 00:03:17,000
 He was shaking this tree.

38
00:03:17,000 --> 00:03:22,950
 The fourth one, while the Buddha was talking, if you watch,

39
00:03:22,950 --> 00:03:29,000
 he was staring up at the skies.

40
00:03:29,000 --> 00:03:34,460
 Don't know what he was doing, but spent the whole time just

41
00:03:34,460 --> 00:03:37,000
 looking up at the stars.

42
00:03:37,000 --> 00:03:42,050
 And the fifth one sat upright and listened attentively,

43
00:03:42,050 --> 00:03:45,000
 maybe even with his eyes open,

44
00:03:45,000 --> 00:03:50,340
 completely absorbed in memorizing and remembering what the

45
00:03:50,340 --> 00:03:52,000
 Buddha taught.

46
00:03:52,000 --> 00:03:55,000
 Ananda was fanning the Buddha.

47
00:03:55,000 --> 00:03:59,480
 Sometimes they would fan the Buddha for keep the flies off

48
00:03:59,480 --> 00:04:02,000
 of him or because it was hot maybe.

49
00:04:02,000 --> 00:04:06,000
 Maybe it was during the day, I don't know.

50
00:04:06,000 --> 00:04:10,240
 And he noticed these five men and he watched them and he

51
00:04:10,240 --> 00:04:11,000
 saw,

52
00:04:11,000 --> 00:04:15,810
 and he thought to himself, what the difference among

53
00:04:15,810 --> 00:04:23,000
 mortals, among humans, among beings,

54
00:04:23,000 --> 00:04:26,000
 among worldlings.

55
00:04:26,000 --> 00:04:29,000
 And he said to the Buddha, he said, it's amazing.

56
00:04:29,000 --> 00:04:35,260
 Here we have this monumentous occasion, the teaching, the

57
00:04:35,260 --> 00:04:37,000
 preaching of the Dhamma

58
00:04:37,000 --> 00:04:42,000
 by the Lord Buddha himself.

59
00:04:42,000 --> 00:04:45,300
 The Lord Buddha's teaching is like thunder, is like a lion

60
00:04:45,300 --> 00:04:46,000
's roar.

61
00:04:46,000 --> 00:04:51,680
 It's like an earthquake, such a profound and important

62
00:04:51,680 --> 00:04:52,000
 event.

63
00:04:52,000 --> 00:04:56,000
 And yet only one of these guys was really paying attention.

64
00:04:56,000 --> 00:05:02,060
 Why were the rest of them totally distracted by something

65
00:05:02,060 --> 00:05:03,000
 else?

66
00:05:03,000 --> 00:05:10,390
 The Buddha said, oh, it's because of people's character and

67
00:05:10,390 --> 00:05:13,000
 their inclinations.

68
00:05:13,000 --> 00:05:21,280
 And he said, that one that fell asleep for 500 lifetimes or

69
00:05:21,280 --> 00:05:25,000
 for countless, many, many, many lifetimes,

70
00:05:25,000 --> 00:05:32,000
 every lifetime, he was born as a snake.

71
00:05:32,000 --> 00:05:37,000
 And so that's what snakes do when it cools down, I guess,

72
00:05:37,000 --> 00:05:39,000
 when it heats up, I don't know.

73
00:05:39,000 --> 00:05:47,000
 He's curled up in a ball and fell asleep.

74
00:05:47,000 --> 00:05:50,000
 He used to be a snake, that's why.

75
00:05:50,000 --> 00:05:54,590
 The second one, when he was digging in the earth, well, for

76
00:05:54,590 --> 00:05:56,000
 lifetime after lifetime,

77
00:05:56,000 --> 00:06:01,000
 he was born as an earthworm.

78
00:06:01,000 --> 00:06:06,850
 And so he's just absorbed in the earth, digging in the

79
00:06:06,850 --> 00:06:09,000
 earth, I guess.

80
00:06:09,000 --> 00:06:13,000
 The third one, the one that was shaking the tree, well,

81
00:06:13,000 --> 00:06:14,000
 lifetime after lifetime,

82
00:06:14,000 --> 00:06:16,000
 he had been born a monkey.

83
00:06:16,000 --> 00:06:23,000
 And so his inclination was fixed on the trees.

84
00:06:23,000 --> 00:06:29,720
 He was just thinking about climbing the tree, maybe, I don

85
00:06:29,720 --> 00:06:31,000
't know.

86
00:06:31,000 --> 00:06:34,000
 The fourth one, the one that was looking up at the skies,

87
00:06:34,000 --> 00:06:36,000
 well, for lifetime after lifetime,

88
00:06:36,000 --> 00:06:41,810
 he had been born an astrologer, someone who finds patterns

89
00:06:41,810 --> 00:06:43,000
 in the stars,

90
00:06:43,000 --> 00:06:49,000
 and divides people's fortunes from the stars.

91
00:06:49,000 --> 00:06:53,000
 And so all he could think about was the stars. He was

92
00:06:53,000 --> 00:06:59,000
 fascinated and absorbed by them.

93
00:06:59,000 --> 00:07:02,440
 The fifth one, the fifth one, in many lifetimes, he had

94
00:07:02,440 --> 00:07:04,000
 been born a school of,

95
00:07:04,000 --> 00:07:10,000
 sorry, a student of the Vedas, a Brahmin student,

96
00:07:10,000 --> 00:07:14,000
 a student of philosophical texts and religious texts.

97
00:07:14,000 --> 00:07:21,000
 And so he was well inclined towards studying and memorizing

98
00:07:21,000 --> 00:07:22,000
.

99
00:07:22,000 --> 00:07:27,340
 And so he had this positive quality of attention that he

100
00:07:27,340 --> 00:07:28,000
 had developed,

101
00:07:28,000 --> 00:07:33,000
 lifetime after lifetime.

102
00:07:33,000 --> 00:07:37,610
 Alanda was amazed and he said, "Is it possible that they

103
00:07:37,610 --> 00:07:39,000
 were just born one thing?"

104
00:07:39,000 --> 00:07:42,000
 And the Buddha said, "Oh, well."

105
00:07:42,000 --> 00:07:47,130
 He said, "It's not possible to know what they were in every

106
00:07:47,130 --> 00:07:48,000
 lifetime,

107
00:07:48,000 --> 00:07:53,000
 but guaranteed this is where their minds were."

108
00:07:53,000 --> 00:08:04,000
 And then he said to Ananda, he said,

109
00:08:04,000 --> 00:08:09,000
 "This is the power of these four things."

110
00:08:09,000 --> 00:08:17,420
 He said, "They have the power to overcome and override any

111
00:08:17,420 --> 00:08:22,000
 inclination towards goodness.

112
00:08:22,000 --> 00:08:25,470
 They have the power to keep one from cultivating good

113
00:08:25,470 --> 00:08:26,000
 things.

114
00:08:26,000 --> 00:08:29,000
 Even when the Buddha is sitting right in front of them,

115
00:08:29,000 --> 00:08:32,780
 they can overpower the inclination to listen to his

116
00:08:32,780 --> 00:08:34,000
 teachings."

117
00:08:34,000 --> 00:08:39,000
 And he taught this verse.

118
00:08:39,000 --> 00:08:42,350
 So the first lesson, the lesson the story gives us is a

119
00:08:42,350 --> 00:08:46,000
 reminder

120
00:08:46,000 --> 00:08:56,000
 and an eye-opener as to how lucky and how rare it is

121
00:08:56,000 --> 00:09:01,000
 to be able to hear and appreciate the Buddha's teaching.

122
00:09:01,000 --> 00:09:05,000
 It's rare to even have the opportunity to hear the teaching

123
00:09:05,000 --> 00:09:05,000
 of the Buddha.

124
00:09:05,000 --> 00:09:08,000
 None of us have it, of course, in this life.

125
00:09:08,000 --> 00:09:13,120
 We have the rare opportunity to hear and study the Buddha's

126
00:09:13,120 --> 00:09:14,000
 teaching,

127
00:09:14,000 --> 00:09:19,750
 but we've missed the opportunity to hear it from the Buddha

128
00:09:19,750 --> 00:09:21,000
 himself.

129
00:09:21,000 --> 00:09:25,000
 But even someone who's able to hear the Buddha's teaching,

130
00:09:25,000 --> 00:09:31,000
 even someone who's able to study it,

131
00:09:31,000 --> 00:09:34,950
 gains no benefit from it if they're not inclined towards it

132
00:09:34,950 --> 00:09:35,000
,

133
00:09:35,000 --> 00:09:38,000
 if they're not able to appreciate it,

134
00:09:38,000 --> 00:09:45,780
 and if they're not in a state where they're able to pay

135
00:09:45,780 --> 00:09:48,000
 attention.

136
00:09:48,000 --> 00:09:50,490
 There are many people, Buddhists even, who appreciate the

137
00:09:50,490 --> 00:09:51,000
 Buddha's teaching

138
00:09:51,000 --> 00:09:54,000
 but are unable to pay attention to it.

139
00:09:54,000 --> 00:09:56,540
 You can see them doing these sorts of things during the D

140
00:09:56,540 --> 00:09:58,000
hamma talk.

141
00:09:58,000 --> 00:10:02,340
 Maybe distracted thinking about food or distracted thinking

142
00:10:02,340 --> 00:10:04,000
 about work,

143
00:10:04,000 --> 00:10:08,560
 distracted thinking about home, family, many, many

144
00:10:08,560 --> 00:10:11,000
 different things.

145
00:10:11,000 --> 00:10:17,000
 Without the lacking the mental capacity to appreciate it,

146
00:10:17,000 --> 00:10:22,690
 to understand it because of various unwholesome incl

147
00:10:22,690 --> 00:10:25,000
inations, habits,

148
00:10:25,000 --> 00:10:31,000
 because of development, cultivation in the wrong direction.

149
00:10:31,000 --> 00:10:32,000
 So this tells us two things.

150
00:10:32,000 --> 00:10:36,000
 One, to appreciate the fact that we can, first of all,

151
00:10:36,000 --> 00:10:41,000
 appreciate the Buddha's teaching,

152
00:10:41,000 --> 00:10:47,160
 that we can understand it and to appreciate the fact that

153
00:10:47,160 --> 00:10:48,000
 we have this opportunity

154
00:10:48,000 --> 00:10:51,000
 and to not waste it or squander it.

155
00:10:51,000 --> 00:10:53,000
 Because if we develop ourselves in the wrong way,

156
00:10:53,000 --> 00:10:55,610
 the Buddha's teaching might come around and we might just

157
00:10:55,610 --> 00:10:56,000
 ignore it.

158
00:10:56,000 --> 00:10:59,000
 You know, I just have no capacity to appreciate it.

159
00:10:59,000 --> 00:11:04,000
 If you can appreciate and understand the teaching now,

160
00:11:04,000 --> 00:11:08,000
 this is an important opportunity that you have.

161
00:11:08,000 --> 00:11:13,000
 It should be an eye-opener that we take for granted

162
00:11:13,000 --> 00:11:14,000
 sometimes

163
00:11:14,000 --> 00:11:21,000
 our mental capacity to understand things if we're able to.

164
00:11:21,000 --> 00:11:24,000
 Not realizing that some people will...

165
00:11:24,000 --> 00:11:26,000
 And it's not just, we're not saying that they're stupid.

166
00:11:26,000 --> 00:11:29,270
 I mean stupidity or lack of wisdom, you might say, is a

167
00:11:29,270 --> 00:11:30,000
 part of it.

168
00:11:30,000 --> 00:11:33,000
 But greed is a part of it, anger is a part of it.

169
00:11:33,000 --> 00:11:37,120
 There's so many things just being distracted by other

170
00:11:37,120 --> 00:11:38,000
 things.

171
00:11:38,000 --> 00:11:39,000
 Many different reasons.

172
00:11:39,000 --> 00:11:43,000
 If our mind is not, if our wholesome karma is not

173
00:11:43,000 --> 00:11:47,000
 cultivated in the right direction,

174
00:11:47,000 --> 00:11:50,220
 we might just come to the point where we just come to the

175
00:11:50,220 --> 00:11:51,000
 Buddha's teaching

176
00:11:51,000 --> 00:11:53,000
 and miss it entirely.

177
00:11:53,000 --> 00:11:55,000
 There are people for whom this happens,

178
00:11:55,000 --> 00:11:58,000
 and this could be you if you're not careful.

179
00:11:58,000 --> 00:12:01,440
 But the second part of it, I think, is that we shouldn't

180
00:12:01,440 --> 00:12:02,000
 take for granted

181
00:12:02,000 --> 00:12:06,000
 that we do understand the Buddha's teaching.

182
00:12:06,000 --> 00:12:15,360
 It's quite common for us to...for one to believe that

183
00:12:15,360 --> 00:12:16,000
 because they're listening,

184
00:12:16,000 --> 00:12:19,150
 because they're attending, because they're studying the

185
00:12:19,150 --> 00:12:20,000
 Buddha's teaching,

186
00:12:20,000 --> 00:12:21,000
 that they understand it.

187
00:12:21,000 --> 00:12:23,000
 And one of the big things you learn as a meditator

188
00:12:23,000 --> 00:12:25,500
 is that you don't really understand the Dhamma when you

189
00:12:25,500 --> 00:12:26,000
 start.

190
00:12:26,000 --> 00:12:27,730
 The understanding you thought you had was a very

191
00:12:27,730 --> 00:12:30,000
 superficial understanding.

192
00:12:30,000 --> 00:12:33,200
 And you gain a deeper and deeper understanding of the same

193
00:12:33,200 --> 00:12:35,000
 things through practice.

194
00:12:35,000 --> 00:12:38,050
 We shouldn't take for granted the intellectual learning

195
00:12:38,050 --> 00:12:40,000
 that we have.

196
00:12:40,000 --> 00:12:43,390
 Our capacity to understand things without mental

197
00:12:43,390 --> 00:12:44,000
 development,

198
00:12:44,000 --> 00:12:49,000
 intensive mental development is superficial.

199
00:12:49,000 --> 00:12:52,400
 It's easy to feel complacent and believe that you

200
00:12:52,400 --> 00:12:54,000
 understand the teachings

201
00:12:54,000 --> 00:12:58,000
 when in fact you've only scratched the surface.

202
00:12:58,000 --> 00:13:01,720
 Just a reminder that understanding the Dhamma doesn't just

203
00:13:01,720 --> 00:13:03,000
 mean listening to it

204
00:13:03,000 --> 00:13:06,000
 and applying yourself.

205
00:13:06,000 --> 00:13:10,000
 It doesn't just mean attending a Dhamma talk,

206
00:13:10,000 --> 00:13:14,680
 or some people you hear about, they'll turn on the Dhamma

207
00:13:14,680 --> 00:13:16,000
 talk while they're doing the dishes,

208
00:13:16,000 --> 00:13:22,620
 or while they're at work, or while they're doing something

209
00:13:22,620 --> 00:13:23,000
 else,

210
00:13:23,000 --> 00:13:29,000
 which I think is not really the best idea.

211
00:13:29,000 --> 00:13:32,180
 The Dhamma is first of all something that is sacred and

212
00:13:32,180 --> 00:13:34,000
 something that should be respected,

213
00:13:34,000 --> 00:13:38,670
 but that's not just a religious idea, it's not just out of

214
00:13:38,670 --> 00:13:39,000
 respect,

215
00:13:39,000 --> 00:13:44,000
 although that is a wholesome quality.

216
00:13:44,000 --> 00:13:47,000
 It's also because it's not easy to understand.

217
00:13:47,000 --> 00:13:49,380
 If you want to understand, you can't just let the words go

218
00:13:49,380 --> 00:13:51,000
 into your head and say,

219
00:13:51,000 --> 00:13:55,000
 "I understand the words and the syntax and the grammar."

220
00:13:55,000 --> 00:14:00,910
 Your mind has to be in the right frame, has to be in the

221
00:14:00,910 --> 00:14:02,000
 right state.

222
00:14:02,000 --> 00:14:06,890
 You have to have a clarity of mind and a proper perspective

223
00:14:06,890 --> 00:14:07,000
,

224
00:14:07,000 --> 00:14:16,000
 a mindful presence in order to understand the Dhamma.

225
00:14:16,000 --> 00:14:23,000
 The lesson of the verse is about what exactly the things

226
00:14:23,000 --> 00:14:25,000
 are that keep us from understanding,

227
00:14:25,000 --> 00:14:29,910
 not just understanding a Dhamma talk, but understanding the

228
00:14:29,910 --> 00:14:31,000
 truth,

229
00:14:31,000 --> 00:14:38,660
 keep us from realizing, appreciating, and understanding the

230
00:14:38,660 --> 00:14:42,000
 nature of reality,

231
00:14:42,000 --> 00:14:52,250
 and a reminder of how terrible and how great the power of

232
00:14:52,250 --> 00:14:54,000
 these things is.

233
00:14:54,000 --> 00:15:00,000
 The Buddha said there is no fire like passion.

234
00:15:00,000 --> 00:15:07,000
 Ordinarily fires burst up and they can consume everything.

235
00:15:07,000 --> 00:15:12,540
 They don't really consume everything. They leave behind

236
00:15:12,540 --> 00:15:13,000
 charred ashes,

237
00:15:13,000 --> 00:15:18,000
 and when they've taken up their fuel, they burn out.

238
00:15:18,000 --> 00:15:20,280
 They might rage on for a while, but they burn out

239
00:15:20,280 --> 00:15:21,000
 eventually,

240
00:15:21,000 --> 00:15:25,400
 but passion never burns out, never runs out of fuel, I

241
00:15:25,400 --> 00:15:26,000
 guess.

242
00:15:26,000 --> 00:15:31,980
 It doesn't just burn sometimes. It will flare up at any

243
00:15:31,980 --> 00:15:33,000
 moment.

244
00:15:33,000 --> 00:15:41,000
 You can be trying to sleep and your passion flares up,

245
00:15:41,000 --> 00:15:46,000
 trying to work, trying to focus.

246
00:15:46,000 --> 00:15:50,000
 Passion is something that consumes us, drives us on.

247
00:15:50,000 --> 00:15:54,820
 It's why people go into debt. It's why people, why we fight

248
00:15:54,820 --> 00:15:55,000
,

249
00:15:55,000 --> 00:15:59,300
 why we manipulate, why we compete. It leads to ambition. It

250
00:15:59,300 --> 00:16:02,000
 leads to violence.

251
00:16:02,000 --> 00:16:11,420
 It leads to cruelty. It leads to miserliness and stinginess

252
00:16:11,420 --> 00:16:12,000
.

253
00:16:12,000 --> 00:16:15,000
 It flames on and it inflames the mind.

254
00:16:15,000 --> 00:16:20,060
 Most importantly, it's like a fire that consumes our

255
00:16:20,060 --> 00:16:24,000
 ability to see clearly.

256
00:16:24,000 --> 00:16:27,180
 Until the fire of passion dies down, you can't really

257
00:16:27,180 --> 00:16:29,000
 understand the truth.

258
00:16:29,000 --> 00:16:32,680
 You can't really understand the truth of your own situation

259
00:16:32,680 --> 00:16:33,000
.

260
00:16:33,000 --> 00:16:35,000
 You'll be blinded by passion.

261
00:16:35,000 --> 00:16:39,000
 Passion blinds us to the suffering that we cause

262
00:16:39,000 --> 00:16:43,000
 by chasing after the things we're passionate about.

263
00:16:43,000 --> 00:16:45,510
 If you're passionate about something, you don't care who

264
00:16:45,510 --> 00:16:46,000
 you're hurting,

265
00:16:46,000 --> 00:16:48,000
 other people are hurting yourself.

266
00:16:48,000 --> 00:16:51,000
 You can make yourself sick because of your passion,

267
00:16:51,000 --> 00:16:56,000
 because of lust, because of desire.

268
00:16:56,000 --> 00:16:59,000
 You look at a drug addict, that's what happens to them,

269
00:16:59,000 --> 00:17:02,000
 but it's not limited only to drug addicts.

270
00:17:02,000 --> 00:17:08,000
 Meditation helps us see the danger.

271
00:17:08,000 --> 00:17:11,000
 It helps us see this inflamed state.

272
00:17:11,000 --> 00:17:20,000
 It sees the...

273
00:17:20,000 --> 00:17:31,000
 the obstruction that passion... that passion is for us.

274
00:17:31,000 --> 00:17:37,000
 It's like the greatest fire leaves nothing behind.

275
00:17:37,000 --> 00:17:42,000
 There is no grip like anger.

276
00:17:42,000 --> 00:17:46,000
 So the commentary compares the grips of a monster,

277
00:17:46,000 --> 00:17:53,000
 or the grip of a wild animal maybe.

278
00:17:53,000 --> 00:17:57,000
 Someone can grab you, can hold you, can pin you down,

279
00:17:57,000 --> 00:18:01,210
 keep you from doing the many things that you'd like to be

280
00:18:01,210 --> 00:18:02,000
 doing.

281
00:18:02,000 --> 00:18:06,290
 They can even drag you away in a direction you don't want

282
00:18:06,290 --> 00:18:07,000
 to go.

283
00:18:07,000 --> 00:18:11,000
 But none of those grips are like the grip of anger.

284
00:18:11,000 --> 00:18:19,000
 Anger seizes us, anger causes us to do and say things

285
00:18:19,000 --> 00:18:23,080
 that are completely against our own benefit and the benefit

286
00:18:23,080 --> 00:18:24,000
 of others.

287
00:18:24,000 --> 00:18:32,000
 It blinds us, it drives us like a slave driver.

288
00:18:32,000 --> 00:18:34,870
 People who have anger issues will tell you they didn't mean

289
00:18:34,870 --> 00:18:35,000
 to,

290
00:18:35,000 --> 00:18:37,000
 they didn't want to hurt others,

291
00:18:37,000 --> 00:18:44,000
 but the anger just blinds you, you just fly into a rage.

292
00:18:44,000 --> 00:18:52,000
 Anger makes you reckless, anger seizes you.

293
00:18:52,000 --> 00:18:56,850
 People who are habitually angry also become habitually

294
00:18:56,850 --> 00:18:58,000
 unhealthy.

295
00:18:58,000 --> 00:19:01,000
 They don't take care of their bodies,

296
00:19:01,000 --> 00:19:05,000
 and their bodies start to heat up,

297
00:19:05,000 --> 00:19:10,000
 even their physical form becomes unhealthy through anger.

298
00:19:10,000 --> 00:19:17,000
 Anger is like a great sickness,

299
00:19:17,000 --> 00:19:21,000
 causes you to hurt yourself and hurt others.

300
00:19:21,000 --> 00:19:24,000
 It's a very fearsome sort of thing,

301
00:19:24,000 --> 00:19:29,000
 something that we should be terrified of, our own anger.

302
00:19:29,000 --> 00:19:35,000
 Anger causes you to say things, causes you to do things.

303
00:19:35,000 --> 00:19:37,000
 You have the best of intentions, but when anger comes,

304
00:19:37,000 --> 00:19:42,210
 it's just you're like another person, like a chuckle and

305
00:19:42,210 --> 00:19:46,000
 hide kind of thing.

306
00:19:46,000 --> 00:19:54,800
 Number three, nati moha samang jalang, there is no net like

307
00:19:54,800 --> 00:19:57,000
 delusion.

308
00:19:57,000 --> 00:20:00,000
 This is a reference to the brahmanjala, I think,

309
00:20:00,000 --> 00:20:06,000
 but it shows the meaning of the word brahmanjala,

310
00:20:06,000 --> 00:20:10,680
 that the net of delusion extends through the whole universe

311
00:20:10,680 --> 00:20:11,000
.

312
00:20:11,000 --> 00:20:17,180
 Most nets, if you wriggle around, you might actually escape

313
00:20:17,180 --> 00:20:18,000
 from them.

314
00:20:18,000 --> 00:20:23,150
 And if you avoid them, you can find a way to just not get

315
00:20:23,150 --> 00:20:25,000
 caught up in the net.

316
00:20:25,000 --> 00:20:28,000
 The person hunting birds will throw a net,

317
00:20:28,000 --> 00:20:31,000
 some of the birds get caught, some of them don't.

318
00:20:31,000 --> 00:20:38,800
 But the brahmanjala, the net of samsara catches even brah

319
00:20:38,800 --> 00:20:40,000
man.

320
00:20:40,000 --> 00:20:43,000
 You can go anywhere in samsara, you won't escape delusion,

321
00:20:43,000 --> 00:20:50,000
 meaning there's no path you can take that will free you

322
00:20:50,000 --> 00:20:51,000
 because of delusion.

323
00:20:51,000 --> 00:20:54,000
 You can't become free by going to the brahman realm

324
00:20:54,000 --> 00:20:59,000
 because a Brahma still has delusion, greed, anger,

325
00:20:59,000 --> 00:21:02,840
 a Brahma doesn't have any of these, but a Brahma still has

326
00:21:02,840 --> 00:21:04,000
 delusion.

327
00:21:04,000 --> 00:21:10,670
 They still can have wrong view, they still can have conceit

328
00:21:10,670 --> 00:21:12,000
, ignorance,

329
00:21:12,000 --> 00:21:14,000
 they still have ignorance.

330
00:21:14,000 --> 00:21:19,230
 Many religious paths focus on, spiritual paths focus on

331
00:21:19,230 --> 00:21:20,000
 endeavor,

332
00:21:20,000 --> 00:21:25,000
 cultivating meditative practices, spiritual practices,

333
00:21:25,000 --> 00:21:30,000
 based on effort, based on concentration, focusing the mind,

334
00:21:30,000 --> 00:21:33,000
 calming the mind.

335
00:21:33,000 --> 00:21:35,000
 And they'll never escape that way.

336
00:21:35,000 --> 00:21:37,000
 The net still catches you.

337
00:21:37,000 --> 00:21:40,000
 The only way to escape the net is wisdom,

338
00:21:40,000 --> 00:21:45,000
 overcoming ignorance, straightening one's delusion,

339
00:21:45,000 --> 00:21:49,660
 correcting one's delusions, one's wrong views, one's conce

340
00:21:49,660 --> 00:21:50,000
its,

341
00:21:50,000 --> 00:21:53,000
 one's self-identity and so on.

342
00:21:53,000 --> 00:21:59,150
 Only by discarding all of these, can one cast off the net

343
00:21:59,150 --> 00:22:00,000
 of delusion.

344
00:22:00,000 --> 00:22:04,850
 Delusion is the net that covers greed and anger, rely on

345
00:22:04,850 --> 00:22:06,000
 delusion.

346
00:22:06,000 --> 00:22:09,000
 Without delusion there can be no greed, no anger.

347
00:22:09,000 --> 00:22:13,000
 Without ignorance there can be no suffering.

348
00:22:13,000 --> 00:22:17,000
 Awija pa chaya, that's the ignorance, it's the beginning.

349
00:22:17,000 --> 00:22:22,000
 It's the net that we're trying to escape.

350
00:22:22,000 --> 00:22:25,000
 So it's quite, meditation in that sense is quite simple.

351
00:22:25,000 --> 00:22:28,830
 It's not about fixing our greed and problems, our anger

352
00:22:28,830 --> 00:22:30,000
 problems.

353
00:22:30,000 --> 00:22:33,230
 It's about fixing our delusion problems, basically fixing

354
00:22:33,230 --> 00:22:34,000
 our ignorance problems,

355
00:22:34,000 --> 00:22:39,000
 about understanding, it's a very simple concept.

356
00:22:39,000 --> 00:22:43,000
 And finally, nathit tanha samana dhi.

357
00:22:43,000 --> 00:22:47,000
 Tanha, there is no river like craving.

358
00:22:47,000 --> 00:22:50,000
 So tanha is more broad, I think, than raga.

359
00:22:50,000 --> 00:22:55,370
 Raga is maybe a specific type, but raga has its specific

360
00:22:55,370 --> 00:22:57,000
 idea.

361
00:22:57,000 --> 00:23:04,360
 Tanha is any sort of clinging, any sort of flowing, really,

362
00:23:04,360 --> 00:23:08,000
 like the asava, like a river.

363
00:23:08,000 --> 00:23:15,490
 Any kind of inclination we have, desire for anything, any

364
00:23:15,490 --> 00:23:21,000
 ambition, any clinging to anything, really.

365
00:23:21,000 --> 00:23:28,000
 The analogy, or the imagery of a river is quite apt.

366
00:23:28,000 --> 00:23:36,000
 Samsara or life existence is like a river, flowing on.

367
00:23:36,000 --> 00:23:40,000
 But tanha is like the clinging to something in the river.

368
00:23:40,000 --> 00:23:44,580
 It just makes no sense in the end, because life continues

369
00:23:44,580 --> 00:23:45,000
 on.

370
00:23:45,000 --> 00:23:47,000
 Life goes on and on.

371
00:23:47,000 --> 00:23:50,000
 An enlightened being is so at peace, even alive,

372
00:23:50,000 --> 00:23:54,000
 even though they still suffer from physical maladies,

373
00:23:54,000 --> 00:24:03,030
 from being accosted by people and insects and the elements

374
00:24:03,030 --> 00:24:05,000
 and old age, sickness, death.

375
00:24:05,000 --> 00:24:09,000
 But they live in such peace because they go with the river.

376
00:24:09,000 --> 00:24:13,320
 But the river of craving here is something different from

377
00:24:13,320 --> 00:24:14,000
 that.

378
00:24:14,000 --> 00:24:20,000
 The river of craving, because craving never ends.

379
00:24:20,000 --> 00:24:25,000
 It never dries up, just like the fire of passion.

380
00:24:25,000 --> 00:24:28,290
 Most rivers, if you've ever been to one of the big rivers

381
00:24:28,290 --> 00:24:29,000
 in India,

382
00:24:29,000 --> 00:24:33,450
 the river where the Buddha cut off his hair and crossed the

383
00:24:33,450 --> 00:24:34,000
 river,

384
00:24:34,000 --> 00:24:40,000
 and went to the Bodhi tree, that river dries up.

385
00:24:40,000 --> 00:24:44,000
 Most of the year, much of the year, it's mostly dried up.

386
00:24:44,000 --> 00:24:47,000
 So sometimes it floods, sometimes it dries.

387
00:24:47,000 --> 00:24:50,000
 But the river of craving never dries up.

388
00:24:50,000 --> 00:24:55,000
 You can't just wait for your craving to dry up, thinking,

389
00:24:55,000 --> 00:24:59,000
 "It's okay if I cling to this or cling to that.

390
00:24:59,000 --> 00:25:01,000
 Eventually I'll have enough."

391
00:25:01,000 --> 00:25:06,000
 Once you've had enough, there's never enough for craving.

392
00:25:06,000 --> 00:25:09,000
 Craving never has enough.

393
00:25:09,000 --> 00:25:10,000
 It's habitual, in fact.

394
00:25:10,000 --> 00:25:16,400
 The more you incline towards something, the more you want

395
00:25:16,400 --> 00:25:18,000
 it.

396
00:25:18,000 --> 00:25:22,000
 So there is no river like craving.

397
00:25:22,000 --> 00:25:25,000
 The lesson for us as meditators is,

398
00:25:25,000 --> 00:25:28,410
 well, besides the lessons of remembering the great

399
00:25:28,410 --> 00:25:30,000
 opportunity we have

400
00:25:30,000 --> 00:25:35,000
 and appreciating the need for a depth of understanding

401
00:25:35,000 --> 00:25:38,000
 that can't be found just in texts,

402
00:25:38,000 --> 00:25:43,570
 I think it helps us to focus our attention on what is

403
00:25:43,570 --> 00:25:46,000
 really important,

404
00:25:46,000 --> 00:25:49,000
 reminding us that our progress in the practice

405
00:25:49,000 --> 00:25:53,590
 really comes down to our freeing ourselves from these

406
00:25:53,590 --> 00:25:54,000
 things,

407
00:25:54,000 --> 00:26:00,000
 from passion, from anger, from delusion.

408
00:26:00,000 --> 00:26:02,000
 From all the many forms that these take,

409
00:26:02,000 --> 00:26:10,190
 you know, we can be under the delusion that these things

410
00:26:10,190 --> 00:26:11,000
 are good.

411
00:26:11,000 --> 00:26:13,720
 We can be angry when people suggest that we give up the

412
00:26:13,720 --> 00:26:15,000
 things we crave

413
00:26:15,000 --> 00:26:18,000
 or cling to.

414
00:26:18,000 --> 00:26:22,000
 And we're completely ignorant about the problem and the

415
00:26:22,000 --> 00:26:23,000
 danger.

416
00:26:23,000 --> 00:26:26,860
 Even ignorant about the existence of these things in

417
00:26:26,860 --> 00:26:28,000
 ourselves.

418
00:26:28,000 --> 00:26:31,000
 Part of our meditation practice is a big part of it,

419
00:26:31,000 --> 00:26:34,000
 is just coming to see what's inside,

420
00:26:34,000 --> 00:26:37,000
 coming to see that we have these things,

421
00:26:37,000 --> 00:26:42,000
 see that we have them, see the nature of them.

422
00:26:42,000 --> 00:26:48,630
 Just really to see the chaos and the inconsistency in our

423
00:26:48,630 --> 00:26:49,000
 minds.

424
00:26:49,000 --> 00:26:52,000
 That there's no rhyme or reason to these things.

425
00:26:52,000 --> 00:26:54,000
 There's no reason to get angry.

426
00:26:54,000 --> 00:26:56,000
 There's no reason to cling.

427
00:26:56,000 --> 00:26:59,000
 There's no reason to believe.

428
00:26:59,000 --> 00:27:03,000
 There's no validity to our beliefs and so on.

429
00:27:03,000 --> 00:27:05,000
 So you don't have to actually fix any of these things.

430
00:27:05,000 --> 00:27:09,000
 You just see that they're meaningless and useless.

431
00:27:09,000 --> 00:27:13,000
 They're a waste of time, a waste of effort.

432
00:27:13,000 --> 00:27:15,000
 They're embarrassing, really.

433
00:27:15,000 --> 00:27:18,660
 You realize how embarrassing it is to get caught up in

434
00:27:18,660 --> 00:27:21,000
 things that are so useless.

435
00:27:21,000 --> 00:27:27,020
 You see that they are there without value and you let them

436
00:27:27,020 --> 00:27:28,000
 go naturally.

437
00:27:28,000 --> 00:27:35,000
 So a good verse, one of those important verses that boldly

438
00:27:35,000 --> 00:27:36,000
 claims

439
00:27:36,000 --> 00:27:42,000
 what is the essence of spiritual, religious life

440
00:27:42,000 --> 00:27:47,000
 and the essence of the goal of existence.

441
00:27:47,000 --> 00:27:50,000
 So that's the Dhammapada for today.

442
00:27:50,000 --> 00:27:52,000
 Thank you for listening.

