1
00:00:00,000 --> 00:00:02,720
 The Hindrances - Introduction

2
00:00:02,720 --> 00:00:05,810
 The first set of dhammas discussed in the Satipatthana Sut

3
00:00:05,810 --> 00:00:07,440
ta are the hindrances.

4
00:00:07,440 --> 00:00:11,380
 Before all other dhammas, for someone who is practicing Sat

5
00:00:11,380 --> 00:00:13,680
ipatthana, which is mindfulness,

6
00:00:13,680 --> 00:00:17,130
 the first meditation object outside of the body, feelings,

7
00:00:17,130 --> 00:00:19,840
 and the mind is the hindrances.

8
00:00:19,840 --> 00:00:22,690
 They are taught first because they are strongest and most

9
00:00:22,690 --> 00:00:24,320
 persistent in new meditators.

10
00:00:25,360 --> 00:00:28,280
 Our ordinary relationship with our state of mind is

11
00:00:28,280 --> 00:00:30,720
 problematic because it is backwards.

12
00:00:30,720 --> 00:00:34,080
 When we like or want something, we take it as an indication

13
00:00:34,080 --> 00:00:36,160
 that we need to get that something,

14
00:00:36,160 --> 00:00:38,560
 that we should obtain the object of our desire.

15
00:00:38,560 --> 00:00:43,760
 When we dislike or are angry at something, we take it as an

16
00:00:43,760 --> 00:00:45,440
 indication that we should do whatever

17
00:00:45,440 --> 00:00:48,920
 we can to get rid of the object of our aversion. When we

18
00:00:48,920 --> 00:00:51,840
 are tired, we take it as an indication

19
00:00:51,840 --> 00:00:55,020
 that we should sleep. When we are distracted or restless,

20
00:00:55,020 --> 00:00:56,640
 we take it as a sign that we should

21
00:00:56,640 --> 00:01:00,930
 get up and do something, anything. This is not intellectual

22
00:01:00,930 --> 00:01:02,960
, it is just how we react.

23
00:01:02,960 --> 00:01:06,300
 Our whole being is conditioned to react in this way. When

24
00:01:06,300 --> 00:01:08,640
 we are confused or when we have doubts

25
00:01:08,640 --> 00:01:11,640
 about something, we take it as an indication that we should

26
00:01:11,640 --> 00:01:13,920
 stop what we are doing and reject it.

27
00:01:13,920 --> 00:01:16,840
 We live our lives, trying to fit the world around our

28
00:01:16,840 --> 00:01:18,880
 expectations and personality.

29
00:01:19,520 --> 00:01:22,450
 How often do we make decisions based on our likes and disl

30
00:01:22,450 --> 00:01:24,480
ikes? When we think that it is

31
00:01:24,480 --> 00:01:28,200
 natural to do so, that if you do not like something, why

32
00:01:28,200 --> 00:01:30,240
 would you tolerate it? And if you like

33
00:01:30,240 --> 00:01:32,610
 something, we think of course that is where you should

34
00:01:32,610 --> 00:01:36,400
 focus your effort. Unfortunately,

35
00:01:36,400 --> 00:01:39,890
 the results of this are not as positive as we might think.

36
00:01:39,890 --> 00:01:41,520
 When you chase after what you want

37
00:01:41,520 --> 00:01:44,170
 and what you like, you find yourself bound tighter and

38
00:01:44,170 --> 00:01:46,640
 tighter to those things that you like and your

39
00:01:46,640 --> 00:01:50,220
 happiness becomes more and more dependent on them. This is

40
00:01:50,220 --> 00:01:52,240
 how the addiction cycle works.

41
00:01:52,240 --> 00:01:54,770
 When we are angry or averse to something, we become

42
00:01:54,770 --> 00:01:57,040
 vulnerable to it. We become more and more

43
00:01:57,040 --> 00:02:01,000
 averse, fragile or vulnerable to its arising. We also

44
00:02:01,000 --> 00:02:03,520
 become more and more susceptible to anger,

45
00:02:03,520 --> 00:02:06,310
 disappointment and displeasure when it arises or when it

46
00:02:06,310 --> 00:02:07,440
 cannot be removed.

47
00:02:07,440 --> 00:02:11,990
 If we just sleep whenever we are tired, contrary to popular

48
00:02:11,990 --> 00:02:14,240
 opinion, that does not actually give

49
00:02:14,240 --> 00:02:17,590
 us more energy. There is a real problem with this for medit

50
00:02:17,590 --> 00:02:19,920
ators who come to practice mindfulness,

51
00:02:19,920 --> 00:02:22,780
 because psychologically they have this idea that if you are

52
00:02:22,780 --> 00:02:24,240
 tired, you should sleep,

53
00:02:24,240 --> 00:02:27,000
 and that sleep will somehow make you less of a tired person

54
00:02:27,000 --> 00:02:29,760
. It is actually somewhat the opposite.

55
00:02:29,760 --> 00:02:33,710
 The more quickly you give in to sleep, the more habitually

56
00:02:33,710 --> 00:02:35,760
 prone to drowsiness you become.

57
00:02:35,760 --> 00:02:38,930
 Of course, your body does need sleep based on your daily

58
00:02:38,930 --> 00:02:41,120
 physical and mental activities.

59
00:02:41,120 --> 00:02:44,040
 But if you are very mindful, you need far less sleep and

60
00:02:44,040 --> 00:02:46,080
 are far less susceptible to drowsiness

61
00:02:46,080 --> 00:02:50,110
 than most people. If you are restless and you just get up

62
00:02:50,110 --> 00:02:52,720
 and find something to occupy yourself with,

63
00:02:52,720 --> 00:02:55,360
 you become prone to restlessness. You become more

64
00:02:55,360 --> 00:02:56,640
 distracted by nature.

65
00:02:56,640 --> 00:03:00,760
 As with the other hindrances, we react to our state of mind

66
00:03:00,760 --> 00:03:02,000
 and try to fit the world around

67
00:03:02,000 --> 00:03:04,810
 it to appease it, rather than learning to have our state of

68
00:03:04,810 --> 00:03:06,000
 mind fit the world.

69
00:03:06,880 --> 00:03:09,590
 We need to see how these hindrances are actually the

70
00:03:09,590 --> 00:03:12,000
 biggest reason for us not being able to fit

71
00:03:12,000 --> 00:03:14,940
 in with the world properly or live without stress and

72
00:03:14,940 --> 00:03:17,920
 suffering. If we are always doubting,

73
00:03:17,920 --> 00:03:20,940
 then we just become someone who doubts everything. We can

74
00:03:20,940 --> 00:03:22,960
 never accomplish anything of any great

75
00:03:22,960 --> 00:03:26,180
 significance because doubt arises habitually and we find

76
00:03:26,180 --> 00:03:28,560
 ourselves doubting even things that present

77
00:03:28,560 --> 00:03:32,280
 themselves right in front of us and are clearly observable.

78
00:03:32,280 --> 00:03:34,640
 This happens for meditators. They will

79
00:03:34,640 --> 00:03:37,740
 achieve positive results in their practice but later doubt

80
00:03:37,740 --> 00:03:39,760
 what they achieved. One day they will

81
00:03:39,760 --> 00:03:42,540
 have great results and be sure the practice is beneficial

82
00:03:42,540 --> 00:03:44,240
 and the next day they forget those

83
00:03:44,240 --> 00:03:46,760
 results and doubt whether the practice has any use

84
00:03:46,760 --> 00:03:50,480
 whatsoever. The states of mind discussed above

85
00:03:50,480 --> 00:03:54,220
 are hindrances. There are positive good mind states but

86
00:03:54,220 --> 00:03:56,880
 there are also bad harmful mind states.

87
00:03:56,880 --> 00:03:59,830
 In this practice, we don't consider our normal states of

88
00:03:59,830 --> 00:04:01,920
 mind as proper cause for action,

89
00:04:01,920 --> 00:04:05,550
 speech, or even thought. We instead focus on cultivating

90
00:04:05,550 --> 00:04:07,280
 those states of mind that lead

91
00:04:07,280 --> 00:04:11,120
 naturally to wholesome speech, deeds, and thought. This is

92
00:04:11,120 --> 00:04:13,840
 why we focus on our mind states as objects

93
00:04:13,840 --> 00:04:16,870
 of meditation. Rather than allowing our mind states to

94
00:04:16,870 --> 00:04:18,800
 guide us, we guide ourselves back

95
00:04:18,800 --> 00:04:21,360
 to the states of mind themselves to study them and

96
00:04:21,360 --> 00:04:23,200
 understand their true nature.

97
00:04:23,200 --> 00:04:27,280
 Another common way people deal with troublesome mind states

98
00:04:27,280 --> 00:04:29,200
 is to reject them, suppress them,

99
00:04:29,200 --> 00:04:33,450
 or try to avoid them. One might think, "Desire is bad? Okay

100
00:04:33,450 --> 00:04:35,440
, I will just get really upset every

101
00:04:35,440 --> 00:04:38,760
 time I want something. Anger is bad? Well, I will feel

102
00:04:38,760 --> 00:04:40,880
 really bad about that and then I will get

103
00:04:40,880 --> 00:04:43,970
 angry at the fact that I am angry. If I am tired, then I

104
00:04:43,970 --> 00:04:46,400
 will try to force myself to stay awake and

105
00:04:46,400 --> 00:04:49,820
 tire myself out by working really hard. If I am distracted,

106
00:04:49,820 --> 00:04:51,760
 then I will force and exert myself

107
00:04:51,760 --> 00:04:54,870
 with lots of effort. I will apply the effort to suppress

108
00:04:54,870 --> 00:04:57,120
 the effort, as if you can somehow stop

109
00:04:57,120 --> 00:05:01,270
 yourself from being distracted." One might think, "If I

110
00:05:01,270 --> 00:05:03,440
 have doubt, then I will just force myself to

111
00:05:03,440 --> 00:05:06,660
 believe," which of course is the best way to cultivate long

112
00:05:06,660 --> 00:05:08,560
-term doubt. It does not get rid

113
00:05:08,560 --> 00:05:11,760
 of the doubt at all. Instead, in the long term, it just

114
00:05:11,760 --> 00:05:13,840
 makes you realize how you never really

115
00:05:13,840 --> 00:05:17,410
 understood or had any good basis for confidence. It

116
00:05:17,410 --> 00:05:19,840
 prevents you from gaining true confidence.

117
00:05:19,840 --> 00:05:23,340
 What helps you to gain true confidence and overcome all

118
00:05:23,340 --> 00:05:25,520
 problematic mind states is honest

119
00:05:25,520 --> 00:05:28,960
 and objective observation of the states of mind themselves.

120
00:05:28,960 --> 00:05:32,240
 The Buddha said the same thing about

121
00:05:32,240 --> 00:05:35,220
 the hindrances as he did about all other aspects of our

122
00:05:35,220 --> 00:05:37,600
 experience in the Satipatthana Sutta,

123
00:05:37,600 --> 00:05:41,190
 that we should be mindful of them. We should see how they

124
00:05:41,190 --> 00:05:43,520
 arise and cease. We should watch them,

125
00:05:43,520 --> 00:05:47,070
 observe them, and study them. When the mind is full of

126
00:05:47,070 --> 00:05:49,200
 anger, we should know that it is a mind

127
00:05:49,200 --> 00:05:52,320
 full of anger. When the mind is full of lust or passion, we

128
00:05:52,320 --> 00:05:54,320
 should know that it is a mind full of

129
00:05:54,320 --> 00:05:58,300
 passion. If you see something beautiful or attractive that

130
00:05:58,300 --> 00:06:00,080
 will give rise to liking,

131
00:06:00,080 --> 00:06:04,350
 you note seeing, and the liking will not arise. If you note

132
00:06:04,350 --> 00:06:06,960
 this process, you are able to understand

133
00:06:06,960 --> 00:06:10,990
 cause and effect. You are able to see what leads to what.

134
00:06:10,990 --> 00:06:13,600
 You can see how desire clouds and colors

135
00:06:13,600 --> 00:06:17,230
 the mind. You can see how anger inflames the mind and drows

136
00:06:17,230 --> 00:06:18,880
iness stifles the mind.

137
00:06:20,080 --> 00:06:24,390
 The five hindrances in brief are liking, disliking, drows

138
00:06:24,390 --> 00:06:26,480
iness, distraction, and doubt.

139
00:06:26,480 --> 00:06:30,450
 These five states of mind are the most important aspects of

140
00:06:30,450 --> 00:06:32,800
 the Dhamma to focus on as a beginner

141
00:06:32,800 --> 00:06:35,350
 meditator because in the beginning you are easily

142
00:06:35,350 --> 00:06:38,640
 overwhelmed by them. They will arise throughout

143
00:06:38,640 --> 00:06:41,550
 your practice, but in the very beginning the hindrances

144
00:06:41,550 --> 00:06:43,600
 will truly obstruct your practice.

145
00:06:43,600 --> 00:06:47,410
 A new meditator has to be vigilant about the hindrances. If

146
00:06:47,410 --> 00:06:49,680
 your practice is not going well,

147
00:06:49,680 --> 00:06:53,860
 there are only five reasons. Liking or wanting, disliking,

148
00:06:53,860 --> 00:06:55,600
 which includes boredom, fear,

149
00:06:55,600 --> 00:06:59,690
 depression, and sadness, drowsiness or tiredness,

150
00:06:59,690 --> 00:07:02,480
 distraction or worry, or the inability to focus

151
00:07:02,480 --> 00:07:06,850
 the mind on the task at hand, and doubt or confusion. The

152
00:07:06,850 --> 00:07:08,640
 hindrances are the only reasons

153
00:07:08,640 --> 00:07:11,600
 why you really fail and why meditation practice can ever be

154
00:07:11,600 --> 00:07:14,400
 difficult. If the hindrances do not

155
00:07:14,400 --> 00:07:18,230
 arise, then you will be able to be mindful of anything no

156
00:07:18,230 --> 00:07:20,560
 matter what happens. Without the

157
00:07:20,560 --> 00:07:24,410
 hindrances the mind is flexible and when experiences change

158
00:07:24,410 --> 00:07:26,400
 one can be fully focused on the new

159
00:07:26,400 --> 00:07:30,280
 experiences. When the hindrances do not arise it is very

160
00:07:30,280 --> 00:07:32,880
 hard to accept change. It is very hard to

161
00:07:32,880 --> 00:07:36,050
 be open. It is very hard to be flexible in the face of

162
00:07:36,050 --> 00:07:38,800
 impermanence, suffering, or non-self,

163
00:07:38,800 --> 00:07:42,160
 in the face of unmet expectations.

164
00:07:42,160 --> 00:07:52,080
 -

