1
00:00:00,000 --> 00:00:08,000
 Good evening everyone.

2
00:00:08,000 --> 00:00:29,000
 Broadcasting live April 26.

3
00:00:29,000 --> 00:00:34,000
 Today is quote again about anger.

4
00:00:34,000 --> 00:00:42,000
 This time from the Visuddhi Maga.

5
00:00:42,000 --> 00:00:45,000
 It's under the teaching about metta.

6
00:00:45,000 --> 00:00:49,000
 So in order to cultivate metta you have to be skillful in

7
00:00:49,000 --> 00:00:53,000
 overcoming anger.

8
00:00:53,000 --> 00:01:03,000
 There's this set of verses that forms tonight's quote.

9
00:01:03,000 --> 00:01:12,000
 So the first stanza is if someone hurts you,

10
00:01:12,000 --> 00:01:19,300
 hurts you insofar as they can, you've done whatever they

11
00:01:19,300 --> 00:01:21,000
 can to hurt you.

12
00:01:21,000 --> 00:01:25,000
 That makes you hurt once.

13
00:01:25,000 --> 00:01:34,000
 You hurt because of the things that they've done.

14
00:01:34,000 --> 00:01:37,460
 And then the question is, the question you should ask

15
00:01:37,460 --> 00:01:38,000
 yourself.

16
00:01:38,000 --> 00:01:44,180
 So this quote is actually supposed to be directed to

17
00:01:44,180 --> 00:01:46,000
 yourself.

18
00:01:46,000 --> 00:01:50,000
 When you get angry about something someone has done to you,

19
00:01:50,000 --> 00:01:54,640
 you should ask yourself, why do I want to hurt myself again

20
00:01:54,640 --> 00:01:55,000
?

21
00:01:55,000 --> 00:02:00,000
 Why am I sitting here hurting myself?

22
00:02:00,000 --> 00:02:02,000
 Because someone has hurt me.

23
00:02:02,000 --> 00:02:04,000
 Why am I doing it again?

24
00:02:04,000 --> 00:02:07,000
 Why am I making it twice as bad?

25
00:02:07,000 --> 00:02:13,000
 Why am I helping my enemies?

26
00:02:13,000 --> 00:02:21,000
 For people who want to bring me suffering, why do I suffer?

27
00:02:21,000 --> 00:02:24,980
 You think that anger is a valid response, a proper response

28
00:02:24,980 --> 00:02:27,000
 to anything really.

29
00:02:27,000 --> 00:02:29,820
 Buddha said if you're in pain and then you get upset about

30
00:02:29,820 --> 00:02:30,000
 it,

31
00:02:30,000 --> 00:02:39,000
 it's like you have a thorn or a splinter in your skin

32
00:02:39,000 --> 00:02:43,000
 and you take another thorn and you try to gouge it out,

33
00:02:43,000 --> 00:02:51,000
 you make it twice as bad.

34
00:02:51,000 --> 00:02:56,000
 The second stanza is, in tears you left your family.

35
00:02:56,000 --> 00:02:59,000
 They had been kind and helpful too.

36
00:02:59,000 --> 00:03:05,120
 So why not leave your enemy the anger that brings harm to

37
00:03:05,120 --> 00:03:06,000
 you?

38
00:03:06,000 --> 00:03:09,600
 So we left behind all that was dear to us to come to med

39
00:03:09,600 --> 00:03:10,000
itate,

40
00:03:10,000 --> 00:03:13,000
 to cultivate spirituality.

41
00:03:13,000 --> 00:03:17,000
 We give up all the good things and all the good things

42
00:03:17,000 --> 00:03:25,000
 or pleasurable things in the world.

43
00:03:25,000 --> 00:03:29,000
 And yet we bring our enemies with us.

44
00:03:29,000 --> 00:03:33,000
 Now when we get angry, why are we holding onto bad things?

45
00:03:33,000 --> 00:03:39,860
 This is an admonishment, a self admonishment for someone

46
00:03:39,860 --> 00:03:45,000
 who is angry.

47
00:03:45,000 --> 00:03:50,000
 Actually it's generally easier to give up anger than it is

48
00:03:50,000 --> 00:03:55,000
 to give up greed or craving.

49
00:03:55,000 --> 00:03:58,240
 And so this kind of admonishment is actually quite, you

50
00:03:58,240 --> 00:04:00,000
 know, it's fairly simple.

51
00:04:00,000 --> 00:04:03,000
 You just remind yourself that this is hurting you.

52
00:04:03,000 --> 00:04:05,000
 This is harmful.

53
00:04:05,000 --> 00:04:16,000
 The anger, the reaction, the judging.

54
00:04:16,000 --> 00:04:20,360
 This anger that you entertain, the third stanza, this anger

55
00:04:20,360 --> 00:04:21,000
 that you entertain,

56
00:04:21,000 --> 00:04:25,000
 and again I'm reading actually Nyanamali's translation

57
00:04:25,000 --> 00:04:30,000
 so it's a bit different from the one on our website.

58
00:04:30,000 --> 00:04:33,000
 This anger that you entertain is gnawing at the very roots

59
00:04:33,000 --> 00:04:37,000
 of all the virtue that you guard.

60
00:04:37,000 --> 00:04:41,000
 Who is there such a fool as you?

61
00:04:41,000 --> 00:04:47,000
 Anger, greed, they gnaw away at our goodness.

62
00:04:47,000 --> 00:04:53,850
 No, they chew at it. They eat away at all the good that we

63
00:04:53,850 --> 00:04:55,000
've done.

64
00:04:55,000 --> 00:04:57,000
 It gets spoiled.

65
00:04:57,000 --> 00:05:03,000
 You can be helpful to people and charitable and kind,

66
00:05:03,000 --> 00:05:07,660
 but you can be, then you're angry one time and you lose

67
00:05:07,660 --> 00:05:11,000
 your reputation as a good person.

68
00:05:11,000 --> 00:05:16,000
 And it inflames your mind and it prevents you from enjoying

69
00:05:16,000 --> 00:05:24,000
 the peace and happiness that come from goodness.

70
00:05:24,000 --> 00:05:28,000
 Remind ourselves of how it's eating away at our virtue.

71
00:05:28,000 --> 00:05:32,000
 It's making us more coarse.

72
00:05:32,000 --> 00:05:40,460
 It helps us to give up the inclination to be angry, to get

73
00:05:40,460 --> 00:05:42,000
 angry.

74
00:05:42,000 --> 00:05:46,000
 The next stanza, another does ignoble deeds.

75
00:05:46,000 --> 00:05:49,000
 So you are angry. How is this?

76
00:05:49,000 --> 00:05:54,540
 Do you then want to copy to the sort of acts that he

77
00:05:54,540 --> 00:05:56,000
 commits?

78
00:05:56,000 --> 00:06:01,000
 So this person has done something evil and it is evil.

79
00:06:01,000 --> 00:06:06,500
 And so why in the world are you now, who believes that it

80
00:06:06,500 --> 00:06:13,000
 was wrong, why are you now doing the same thing?

81
00:06:13,000 --> 00:06:15,880
 That's a curious point because when you get angry at

82
00:06:15,880 --> 00:06:20,000
 someone who's evil, you're joining them in their evil.

83
00:06:20,000 --> 00:06:24,990
 In fact, the Buddha said, and it mentions in this up above

84
00:06:24,990 --> 00:06:28,000
 that it's worse to get angry back at someone.

85
00:06:28,000 --> 00:06:33,190
 It's a worse evil. Someone's angry at you and you return

86
00:06:33,190 --> 00:06:34,000
 the anger.

87
00:06:34,000 --> 00:06:37,000
 That's worse because we all get angry.

88
00:06:37,000 --> 00:06:44,080
 Anger comes and if we're, if we love each other, then we

89
00:06:44,080 --> 00:06:47,000
 let each other vent.

90
00:06:47,000 --> 00:06:50,410
 When someone gets angry at us, we appreciate what they're

91
00:06:50,410 --> 00:06:51,000
 saying.

92
00:06:51,000 --> 00:06:55,860
 We listen and we try to calm them down and appease them in

93
00:06:55,860 --> 00:06:57,000
 some way.

94
00:06:57,000 --> 00:07:01,430
 But when you get angry back, then you start the fight, you

95
00:07:01,430 --> 00:07:03,000
 start conflict.

96
00:07:03,000 --> 00:07:05,000
 That's when the real problem is.

97
00:07:05,000 --> 00:07:07,900
 So, and this is a good lesson, you know, we judge each

98
00:07:07,900 --> 00:07:09,000
 other very much.

99
00:07:09,000 --> 00:07:12,000
 And if someone gets angry, we get angry back.

100
00:07:12,000 --> 00:07:15,000
 If someone is greedy, we get upset at them.

101
00:07:15,000 --> 00:07:17,000
 Someone is arrogant and conceited.

102
00:07:17,000 --> 00:07:22,000
 We get angry and upset about the bad things in others.

103
00:07:22,000 --> 00:07:26,000
 And then that makes us just as bad.

104
00:07:26,000 --> 00:07:29,000
 Moreover, we're too critical.

105
00:07:29,000 --> 00:07:33,000
 Critical of ourselves and critical of each other.

106
00:07:33,000 --> 00:07:37,600
 In monasteries, it's quite common for new monks to come and

107
00:07:37,600 --> 00:07:39,000
 just criticize roundly,

108
00:07:39,000 --> 00:07:43,080
 criticize every other monk, all the old monks, all the

109
00:07:43,080 --> 00:07:47,130
 monks who have been there for a long time because they're

110
00:07:47,130 --> 00:07:49,000
 not perfect.

111
00:07:49,000 --> 00:07:52,380
 With this kind of conception that you're not perfect, then

112
00:07:52,380 --> 00:07:55,000
 you deserve to be criticized.

113
00:07:55,000 --> 00:07:59,170
 Really, if you're not perfect, you deserve to be helped, to

114
00:07:59,170 --> 00:08:04,620
 be supported and directed and guided and pushed and prodded

115
00:08:04,620 --> 00:08:07,000
 in the right direction, sure.

116
00:08:07,000 --> 00:08:11,560
 But to denounce someone because they're imperfect, that's

117
00:08:11,560 --> 00:08:13,000
 not the Buddhist way.

118
00:08:13,000 --> 00:08:18,750
 For more reasons than one, because it's useless, it's un

119
00:08:18,750 --> 00:08:25,000
helpful, and because it's harmful to yourself as well.

120
00:08:25,000 --> 00:08:28,000
 Next stanza.

121
00:08:28,000 --> 00:08:31,710
 Next stanza just says that when you get angry at something

122
00:08:31,710 --> 00:08:35,000
 someone has done, you do what they would have you do.

123
00:08:35,000 --> 00:08:39,000
 They want you to suffer and, well, bingo, you're suffering.

124
00:08:39,000 --> 00:08:43,100
 And that's key because really other people can't make us

125
00:08:43,100 --> 00:08:44,000
 suffer.

126
00:08:44,000 --> 00:08:47,810
 They can hit you, they can denounce you, they can scold you

127
00:08:47,810 --> 00:08:52,000
, they can insult you, but they can't make you suffer.

128
00:08:52,000 --> 00:08:55,000
 They can't make your mind suffer.

129
00:08:55,000 --> 00:09:00,000
 They can't make you get angry.

130
00:09:00,000 --> 00:09:05,660
 I mean, it depends how you look at it, but it's possible to

131
00:09:05,660 --> 00:09:11,000
 be free from anger no matter what someone else does.

132
00:09:11,000 --> 00:09:18,000
 But once you get angry, that's when the suffering comes.

133
00:09:18,000 --> 00:09:20,080
 There's no question you suffer when you're angry. You

134
00:09:20,080 --> 00:09:22,850
 suffer when you dislike something. That's where suffering

135
00:09:22,850 --> 00:09:24,000
 comes from.

136
00:09:24,000 --> 00:09:32,000
 Next stanza.

137
00:09:32,000 --> 00:09:36,640
 If you get angry, then maybe you make him suffer, maybe not

138
00:09:36,640 --> 00:09:37,000
.

139
00:09:37,000 --> 00:09:40,740
 The width of hurt that anger brings you certainly are

140
00:09:40,740 --> 00:09:42,000
 punished now.

141
00:09:42,000 --> 00:09:46,360
 So when you get angry back, usually it's because you are

142
00:09:46,360 --> 00:09:51,000
 thinking of ways to retaliate, to seek revenge.

143
00:09:51,000 --> 00:09:54,280
 And so there's like, well, maybe, maybe you can find

144
00:09:54,280 --> 00:09:55,000
 revenge.

145
00:09:55,000 --> 00:09:57,900
 Maybe you'll actually end up harming the other person and

146
00:09:57,900 --> 00:09:59,000
 getting revenge.

147
00:09:59,000 --> 00:10:01,000
 We don't know. It's possible.

148
00:10:01,000 --> 00:10:05,160
 But what's certain is that you're punishing yourself with

149
00:10:05,160 --> 00:10:09,850
 your anger, and you punish yourself more if you seek

150
00:10:09,850 --> 00:10:11,000
 revenge.

151
00:10:11,000 --> 00:10:16,100
 If anger blinded enemies set out to tread the path of woe,

152
00:10:16,100 --> 00:10:21,520
 do you, by getting angry, too, intend to follow heel to toe

153
00:10:21,520 --> 00:10:22,000
?

154
00:10:22,000 --> 00:10:28,000
 You intend to follow these anger blinded enemies.

155
00:10:28,000 --> 00:10:35,890
 If hurt is done, you buy a foe because of anger on your

156
00:10:35,890 --> 00:10:43,680
 part, then put your anger down for why should you be

157
00:10:43,680 --> 00:10:48,000
 harassed groundlessly?

158
00:10:48,000 --> 00:10:53,270
 Yes. Why? You weren't the one that did the evil. Why are

159
00:10:53,270 --> 00:10:54,000
 you suffering?

160
00:10:54,000 --> 00:10:58,640
 Someone hurts you or attacks you or criticizes you or so on

161
00:10:58,640 --> 00:11:02,000
. That's their problem.

162
00:11:02,000 --> 00:11:06,000
 Why are you suffering?

163
00:11:06,000 --> 00:11:09,840
 Since states last but a moment's time, those aggregates by

164
00:11:09,840 --> 00:11:13,000
 which was done, the odious act have ceased.

165
00:11:13,000 --> 00:11:16,000
 So now what is it you are angry with?

166
00:11:16,000 --> 00:11:21,680
 This is very much in meditation. The person who we're angry

167
00:11:21,680 --> 00:11:23,000
 at doesn't exist.

168
00:11:23,000 --> 00:11:27,710
 The states that arose that gave rise to the event that we

169
00:11:27,710 --> 00:11:31,000
're angry at, that those have ceased.

170
00:11:31,000 --> 00:11:34,120
 And we're angry at something or worried about something, or

171
00:11:34,120 --> 00:11:37,000
 when we're attached to something, we want something.

172
00:11:37,000 --> 00:11:41,460
 That thing is only a concept. The experiences arise and

173
00:11:41,460 --> 00:11:42,000
 cease.

174
00:11:42,000 --> 00:11:44,730
 And if it's a good thing, well, that good thing is not

175
00:11:44,730 --> 00:11:49,000
 really good in any way. It just arises and ceases.

176
00:11:49,000 --> 00:11:52,860
 Comes and goes and the same with bad things. There's no

177
00:11:52,860 --> 00:11:55,000
 person who is our enemy.

178
00:11:55,000 --> 00:11:59,000
 What are we angry at? What really are we angry at?

179
00:11:59,000 --> 00:12:03,350
 What we're angry at is a memory, a thought, a thought that

180
00:12:03,350 --> 00:12:08,000
 arises in our mind that is an echo of some past deed.

181
00:12:08,000 --> 00:12:14,000
 And we get angry at that thought.

182
00:12:14,000 --> 00:12:18,090
 Whom shall he hurt who seeks to hurt another in the other's

183
00:12:18,090 --> 00:12:21,000
 absence? Your presence is the cause of hurt.

184
00:12:21,000 --> 00:12:24,000
 Why are you angry then with him?

185
00:12:24,000 --> 00:12:27,260
 Yes, we're angry at ourselves, actually. It's our own mind

186
00:12:27,260 --> 00:12:30,000
 states, the thoughts that arise in our own mind.

187
00:12:30,000 --> 00:12:34,770
 The person who did this deed, if someone sits in this very

188
00:12:34,770 --> 00:12:37,000
 angry at another person,

189
00:12:37,000 --> 00:12:40,140
 it's nothing to do with the other person. It's you yourself

190
00:12:40,140 --> 00:12:42,000
 who are hurting yourself.

191
00:12:42,000 --> 00:12:46,030
 You're angry at thoughts that arise in your own mind.

192
00:12:46,030 --> 00:12:49,000
 Nothing to do with the other person.

193
00:12:49,000 --> 00:12:56,450
 So this has to do with anger. But more generally, it's the

194
00:12:56,450 --> 00:13:01,000
 idea that only we can hurt ourselves.

195
00:13:01,000 --> 00:13:07,560
 And happiness as well has to come from within. Happiness

196
00:13:07,560 --> 00:13:11,000
 can't be based on a thing.

197
00:13:11,000 --> 00:13:13,760
 And suffering doesn't come from things. Happiness and

198
00:13:13,760 --> 00:13:16,000
 suffering don't come from the outside.

199
00:13:16,000 --> 00:13:20,950
 They come from within. It's quite simple. Our state of mind

200
00:13:20,950 --> 00:13:25,000
 determines our state of happiness,

201
00:13:25,000 --> 00:13:30,890
 not the world around us. This is crucial. It's a specific

202
00:13:30,890 --> 00:13:35,870
 Buddhist doctrine that happiness and suffering come from

203
00:13:35,870 --> 00:13:37,000
 the mind.

204
00:13:37,000 --> 00:13:45,160
 This is clear. So a big part of our practice is learning to

205
00:13:45,160 --> 00:13:50,660
 be objective and to see the experiences that come from

206
00:13:50,660 --> 00:13:52,000
 external,

207
00:13:52,000 --> 00:14:04,320
 that come from the body and the mind, to see them with

208
00:14:04,320 --> 00:14:13,000
 wisdom, to cultivate a relationship with experience that is

209
00:14:13,000 --> 00:14:18,000
 free from likes and dislikes,

210
00:14:18,000 --> 00:14:23,200
 to be objective and to see things as they are. Because when

211
00:14:23,200 --> 00:14:30,970
 you do, there's not intrinsic likeability or dislikeability

212
00:14:30,970 --> 00:14:31,000
,

213
00:14:31,000 --> 00:14:44,550
 attraction, attractiveness or undetracktiveness. It's not a

214
00:14:44,550 --> 00:14:49,000
 quality of things.

215
00:14:49,000 --> 00:14:52,820
 And so when we look at things as they are, we find that

216
00:14:52,820 --> 00:14:57,280
 they're not attractive. We don't give rise to the

217
00:14:57,280 --> 00:14:59,000
 attraction.

218
00:14:59,000 --> 00:15:03,000
 In a version, they come from not seeing things as they are.

219
00:15:03,000 --> 00:15:06,000
 That's key as well. Because it could be otherwise.

220
00:15:06,000 --> 00:15:09,500
 Mostly we think it's otherwise. We think that some things

221
00:15:09,500 --> 00:15:12,000
 are intrinsically beautiful or desirable.

222
00:15:12,000 --> 00:15:20,110
 Some other things are not, are intrinsically dislikable, a

223
00:15:20,110 --> 00:15:24,000
 direct cause for disliking. That's not true.

224
00:15:24,000 --> 00:15:29,810
 When you see things as they are, it's only because of

225
00:15:29,810 --> 00:15:38,880
 delusion that we, because of misconception that we have

226
00:15:38,880 --> 00:15:43,000
 desire in a version.

227
00:15:43,000 --> 00:15:53,000
 So that's our dhamma for tonight.

228
00:15:53,000 --> 00:15:55,860
 If you're interested in the Risuddhi manga, we're actually

229
00:15:55,860 --> 00:16:02,100
 studying it on Sundays, but we're most of the way through

230
00:16:02,100 --> 00:16:04,000
 it already.

231
00:16:04,000 --> 00:16:14,340
 Risuddhi magas, I appreciate the Risuddhi manga. It's more

232
00:16:14,340 --> 00:16:20,000
 like a manual or a reference guide. It's good for teachers.

233
00:16:20,000 --> 00:16:23,400
 It's also good for meditators, experienced meditators, I

234
00:16:23,400 --> 00:16:28,420
 think. There's just so much in it, so many different, like

235
00:16:28,420 --> 00:16:32,000
 this set of verses is quite useful.

236
00:16:32,000 --> 00:16:38,000
 That's from the Risuddhi manga.

237
00:16:38,000 --> 00:16:50,670
 You too can go. You don't have to stay. I'm just going to

238
00:16:50,670 --> 00:16:58,000
 answer questions if people have.

239
00:16:58,000 --> 00:17:06,110
 I was in New York, the monk I was staying with in New York

240
00:17:06,110 --> 00:17:13,110
 doesn't like the Risuddhi manga, or the commentaries, or

241
00:17:13,110 --> 00:17:17,000
 the abhidharma, which is very common with Western monks,

242
00:17:17,000 --> 00:17:21,290
 especially in the Ajahn Chah group. Although he's not

243
00:17:21,290 --> 00:17:24,000
 exactly with the Ajahn Chah group.

244
00:17:24,000 --> 00:17:30,060
 They're so keen on the suttas, which is great. The suttas

245
00:17:30,060 --> 00:17:34,000
 are just the most pure.

246
00:17:34,000 --> 00:17:44,000
 But the suttas are not the only source of Buddhism.

247
00:17:44,000 --> 00:17:46,870
 The suttas are talks that were given, mostly that were

248
00:17:46,870 --> 00:17:51,150
 given to the monks, or given to certain people at a certain

249
00:17:51,150 --> 00:17:52,000
 time.

250
00:17:52,000 --> 00:17:55,940
 They have to be interpreted, especially because we're so

251
00:17:55,940 --> 00:17:59,000
 far removed from that time and that place.

252
00:17:59,000 --> 00:18:04,000
 So no matter what, you have to explain the suttas.

253
00:18:04,000 --> 00:18:06,840
 So either you follow the commentary explanation, the Risudd

254
00:18:06,840 --> 00:18:10,120
hi manga explanation, or you follow some modern commentator

255
00:18:10,120 --> 00:18:11,000
's explanation.

256
00:18:11,000 --> 00:18:14,730
 It turns out to be no better. It turns out to be actually

257
00:18:14,730 --> 00:18:17,000
 usually worse, I would argue.

258
00:18:17,000 --> 00:18:22,000
 Worse in the sense that whether you agree with it or not,

259
00:18:22,000 --> 00:18:27,690
 far less rigorous, far less objective, far less

260
00:18:27,690 --> 00:18:31,000
 comprehensive, far less in-depth.

261
00:18:31,000 --> 00:18:35,400
 The Risuddhi manga just gets incredible work. If you

262
00:18:35,400 --> 00:18:38,780
 disagree with it or disagree with it, it's amazing that

263
00:18:38,780 --> 00:18:40,000
 someone actually wrote it,

264
00:18:40,000 --> 00:18:46,020
 because it's huge and it's so in-depth and it's got so much

265
00:18:46,020 --> 00:18:47,000
 in it.

266
00:18:47,000 --> 00:18:50,610
 Remember I was giving a talk when I was in Sri Lanka last

267
00:18:50,610 --> 00:18:55,490
 time, and the man who was translating for me refused to

268
00:18:55,490 --> 00:18:56,000
 translate

269
00:18:56,000 --> 00:18:59,470
 because I was quoting the Risuddhi manga and he said, "Oh,

270
00:18:59,470 --> 00:19:03,000
 we don't follow the Risuddhi manga here."

271
00:19:03,000 --> 00:19:05,920
 That was the first for me. I was giving a talk and the

272
00:19:05,920 --> 00:19:08,000
 translator wouldn't translate.

273
00:19:08,000 --> 00:19:14,000
 I think I was saying something mean or evil.

274
00:19:14,000 --> 00:19:17,600
 So it's a contentious, the Risuddhi manga tradition is

275
00:19:17,600 --> 00:19:20,000
 fairly contentious to some people.

276
00:19:20,000 --> 00:19:27,030
 Most Theravada societies, for the most part, they follow

277
00:19:27,030 --> 00:19:30,000
 the Risuddhi manga.

278
00:19:30,000 --> 00:19:32,780
 In Thailand, it forms the latter part of one's Pali studies

279
00:19:32,780 --> 00:19:33,000
.

280
00:19:33,000 --> 00:19:36,790
 So any monk who studies higher-level Pali will have to

281
00:19:36,790 --> 00:19:41,690
 translate the Risuddhi manga into Thai and then translate

282
00:19:41,690 --> 00:19:43,000
 Thai passages,

283
00:19:43,000 --> 00:19:46,000
 Thai translations of the Risuddhi manga back into Pali.

284
00:19:46,000 --> 00:19:48,000
 So they have to really memorize.

285
00:19:48,000 --> 00:19:52,090
 Many of them basically, I think, will memorize the Risuddhi

286
00:19:52,090 --> 00:19:57,000
 manga in Pali and English, which is impressive in itself.

287
00:19:57,000 --> 00:20:08,000
 Maybe not memorize, but come close.

288
00:20:08,000 --> 00:20:14,640
 Any requirements for taking a meditation course at our

289
00:20:14,640 --> 00:20:16,000
 center?

290
00:20:16,000 --> 00:20:18,000
 Yeah, probably.

291
00:20:18,000 --> 00:20:24,910
 If you're coming alone and you're under 16, I think we'd

292
00:20:24,910 --> 00:20:27,000
 have to have a talk about that.

293
00:20:27,000 --> 00:20:31,630
 I don't know. I would imagine having people under 16 coming

294
00:20:31,630 --> 00:20:35,630
 and doing courses is problematic unless we talk to their

295
00:20:35,630 --> 00:20:36,000
 parents.

296
00:20:36,000 --> 00:20:39,000
 I think 16 is probably.

297
00:20:39,000 --> 00:20:44,400
 And as for upper age limit, it's more to do with your

298
00:20:44,400 --> 00:20:45,000
 health, you know.

299
00:20:45,000 --> 00:20:48,990
 If you're able to do walking and sitting meditation for

300
00:20:48,990 --> 00:20:51,000
 hours a day, it's fine.

301
00:20:51,000 --> 00:21:01,000
 You can sit on a chair if you have to.

302
00:21:01,000 --> 00:21:06,730
 I don't know. Robin, I haven't really thought about the

303
00:21:06,730 --> 00:21:08,000
 future.

304
00:21:08,000 --> 00:21:11,270
 I mean, there are some other things I'm looking at,

305
00:21:11,270 --> 00:21:15,380
 actually, that like actually the Anguttra Nikai is a good

306
00:21:15,380 --> 00:21:26,580
 volume and I've started maybe collecting some good suttas

307
00:21:26,580 --> 00:21:29,000
 from it.

308
00:21:29,000 --> 00:21:32,000
 A new set of robes for my trip to Asia.

309
00:21:32,000 --> 00:21:35,710
 The funny thing about these robes, they're fairly new,

310
00:21:35,710 --> 00:21:38,000
 right? But they came with holes in them.

311
00:21:38,000 --> 00:21:40,840
 Like I think they were mouse eaten or they were ripped or

312
00:21:40,840 --> 00:21:43,960
 something because in my back there's actually little holes

313
00:21:43,960 --> 00:21:49,000
 that probably shouldn't sew up.

314
00:21:49,000 --> 00:21:51,000
 Not that that's here or there.

315
00:21:51,000 --> 00:21:53,000
 But I know these are fine in Asia.

316
00:21:53,000 --> 00:21:55,000
 It'll be a little hot.

317
00:21:55,000 --> 00:21:59,020
 You know, this upper robe I don't wear a lot of the time.

318
00:21:59,020 --> 00:22:02,000
 When I'm in my room, I rarely wear this.

319
00:22:02,000 --> 00:22:06,020
 Sometimes I just go bare chest or sometimes I have a basic

320
00:22:06,020 --> 00:22:08,000
 what's called an angsa.

321
00:22:08,000 --> 00:22:22,000
 It's just a very thin shoulder robe, shoulder cloth.

322
00:22:22,000 --> 00:22:25,650
 But these aren't that hot. It's just cotton. So it's

323
00:22:25,650 --> 00:22:28,000
 actually not that hot.

324
00:22:28,000 --> 00:22:31,000
 But it's going to be hot in June.

325
00:22:31,000 --> 00:22:35,000
 So I'll try to go up and live on the mountain.

326
00:22:35,000 --> 00:22:40,000
 I'm going to try to go up on a mountain in Sri Lanka.

327
00:22:40,000 --> 00:22:44,120
 Arguing with Sangha about that, Sangha wants me to stay in

328
00:22:44,120 --> 00:22:46,000
 Colombo for two weeks.

329
00:22:46,000 --> 00:22:50,910
 I'm trying to push to take a week out. He says a week isn't

330
00:22:50,910 --> 00:22:52,000
 enough.

331
00:22:52,000 --> 00:22:55,700
 I know it isn't. I mean a month isn't enough. I could spend

332
00:22:55,700 --> 00:23:01,230
 months in Sri Lanka giving talks every day, but not really

333
00:23:01,230 --> 00:23:04,000
 keen on it.

334
00:23:04,000 --> 00:23:11,790
 And then in Thailand, maybe go back up on the mountain as

335
00:23:11,790 --> 00:23:13,000
 well.

336
00:23:13,000 --> 00:23:16,210
 Probably not staying Jumtong very long because it's

337
00:23:16,210 --> 00:23:18,000
 probably pretty hot in June.

338
00:23:18,000 --> 00:23:25,000
 Unless it started raining, it should be okay.

339
00:23:25,000 --> 00:23:29,710
 Do we only have a few months left? Have we really gone

340
00:23:29,710 --> 00:23:32,000
 through most of the year?

341
00:23:32,000 --> 00:23:37,000
 When did we start August or something?

342
00:23:37,000 --> 00:23:42,000
 July maybe?

343
00:23:42,000 --> 00:23:45,000
 Time flies.

344
00:23:45,000 --> 00:24:00,000
 Tempest forget. Time flees.

345
00:24:00,000 --> 00:24:06,000
 Okay. Anyway, good night everyone. See you all next time.

346
00:24:07,000 --> 00:24:10,000
 Thank you.

