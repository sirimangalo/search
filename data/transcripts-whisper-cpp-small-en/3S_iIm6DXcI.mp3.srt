1
00:00:00,000 --> 00:00:03,280
 Is there a Buddhist approach to get new friends?

2
00:00:03,280 --> 00:00:07,200
 I mean, I heard your recent talks on the Suddha study.

3
00:00:07,200 --> 00:00:11,040
 As I see it, the one way to get new friends arises from

4
00:00:11,040 --> 00:00:17,360
 purity and the eightfold noble path, right?

5
00:00:17,360 --> 00:00:27,280
 To get a new set of friends,

6
00:00:27,280 --> 00:00:30,980
 where you've got some friends who you realize are not

7
00:00:30,980 --> 00:00:33,440
 friends at all,

8
00:00:33,440 --> 00:00:37,760
 but are enemies in wolf and sheep's clothing.

9
00:00:37,760 --> 00:00:41,600
 So what do you do to change that?

10
00:00:41,600 --> 00:00:44,830
 Well, this kind of conflict that we're talking about is a

11
00:00:44,830 --> 00:00:45,760
 big part.

12
00:00:45,760 --> 00:00:49,520
 When I started practicing meditation, I lost all my friends

13
00:00:49,520 --> 00:00:50,320
.

14
00:00:50,320 --> 00:00:53,570
 And a lot of that was due to the conflict that I wasn't at

15
00:00:53,570 --> 00:00:55,520
 all interested in

16
00:00:55,520 --> 00:01:02,640
 compromising. I was very keen on changing my life.

17
00:01:02,640 --> 00:01:05,860
 So at first I thought I could do it, I could bring my

18
00:01:05,860 --> 00:01:06,880
 friends along.

19
00:01:06,880 --> 00:01:10,320
 And so I tried my best to encourage them in it and

20
00:01:10,320 --> 00:01:14,160
 absolutely none of them were interested.

21
00:01:14,160 --> 00:01:20,800
 There were a couple of old ex-girlfriends who I managed to

22
00:01:20,800 --> 00:01:22,080
 convince,

23
00:01:22,080 --> 00:01:26,400
 but I don't know how pure the intentions were there.

24
00:01:26,400 --> 00:01:30,000
 They weren't completely impure intentions, but

25
00:01:30,000 --> 00:01:33,280
 they certainly. And I got a couple of people involved

26
00:01:33,280 --> 00:01:36,790
 temporarily. One person actually went on to do a goyinka

27
00:01:36,790 --> 00:01:38,080
 course.

28
00:01:38,080 --> 00:01:42,400
 But yeah, so the conflict is a big part of it where you

29
00:01:42,400 --> 00:01:46,600
 don't compromise. When you stop compromising your friends'

30
00:01:46,600 --> 00:01:47,520
 change rate,

31
00:01:47,520 --> 00:01:53,040
 your social circle changes very quickly.

32
00:01:53,040 --> 00:01:59,160
 I don't think you should seek out new friends, particularly

33
00:01:59,160 --> 00:01:59,920
. You might want to

34
00:01:59,920 --> 00:02:05,200
 seek out a meditation teacher, which is considered

35
00:02:05,200 --> 00:02:08,400
 to be the best friend, someone who can guide you. So that

36
00:02:08,400 --> 00:02:08,640
 friend

37
00:02:08,640 --> 00:02:12,490
 that we talked about yesterday, the one who gives advice,

38
00:02:12,490 --> 00:02:14,480
 gives guidance,

39
00:02:14,480 --> 00:02:19,200
 I would seek out such a, that sort of friend.

40
00:02:19,200 --> 00:02:26,640
 But if you can, if you can find a Dhamma group,

41
00:02:26,640 --> 00:02:29,810
 if you can put together a Dhamma group even. I mean that

42
00:02:29,810 --> 00:02:30,080
 would be

43
00:02:30,080 --> 00:02:33,600
 something interesting to try. I actually thought, you know,

44
00:02:33,600 --> 00:02:33,840
 we could

45
00:02:33,840 --> 00:02:38,460
 put together some sort of package where, like the goyinka

46
00:02:38,460 --> 00:02:39,360
 tradition,

47
00:02:39,360 --> 00:02:42,960
 where we have this package and you pick up the package and

48
00:02:42,960 --> 00:02:47,600
 it's like a do-it-yourself Dhamma group. So ways and means

49
00:02:47,600 --> 00:02:48,400
 of putting together

50
00:02:48,400 --> 00:02:51,760
 your own meditation group.

51
00:02:51,760 --> 00:02:57,280
 We could do that even. That's something interesting that we

52
00:02:57,280 --> 00:03:00,330
 could even do on the Siri Mangalu site. Anybody who wants

53
00:03:00,330 --> 00:03:01,120
 to start a Dhamma

54
00:03:01,120 --> 00:03:04,080
 group, we could give them a subdomain or a page

55
00:03:04,080 --> 00:03:07,440
 of their own and it would have all sorts of tools on it or

56
00:03:07,440 --> 00:03:08,800
 something.

57
00:03:08,800 --> 00:03:15,110
 Setting up a group and, I don't know, whatever someone they

58
00:03:15,110 --> 00:03:15,120
,

59
00:03:15,120 --> 00:03:19,770
 would really need is someone who can figure out how to put

60
00:03:19,770 --> 00:03:20,640
 that together

61
00:03:20,640 --> 00:03:24,720
 to allow people to just set up their own group and

62
00:03:24,720 --> 00:03:29,360
 attract other like-minded people in their area.

63
00:03:29,360 --> 00:03:33,120
 That's maybe the next iteration of siri-mongolu.org.

64
00:03:33,120 --> 00:03:36,880
 But I need some help. I'm not really going to be able to

65
00:03:36,880 --> 00:03:42,960
 expand upon the website.

