1
00:00:00,000 --> 00:00:10,000
 [BLANK_AUDIO]

2
00:00:10,000 --> 00:00:20,000
 [BLANK_AUDIO]

3
00:00:20,000 --> 00:00:35,440
 Okay, good evening, everyone.

4
00:00:35,440 --> 00:00:38,080
 Welcome to our evening done session.

5
00:00:38,080 --> 00:00:40,080
 [BLANK_AUDIO]

6
00:00:40,080 --> 00:00:43,680
 Just supposed to be questions and answers tonight, but

7
00:00:43,680 --> 00:00:45,440
 [BLANK_AUDIO]

8
00:00:45,440 --> 00:00:47,960
 I think we exhausted all questions last week.

9
00:00:47,960 --> 00:00:49,520
 There's only five this week.

10
00:00:49,520 --> 00:00:52,480
 [BLANK_AUDIO]

11
00:00:52,480 --> 00:00:53,920
 So maybe we'll try and

12
00:00:53,920 --> 00:00:57,200
 [BLANK_AUDIO]

13
00:00:57,200 --> 00:01:02,400
 Elaborate on at least one of the questions.

14
00:01:02,400 --> 00:01:10,140
 Give a little bit of a longer talk in the form of an answer

15
00:01:10,140 --> 00:01:10,720
.

16
00:01:10,720 --> 00:01:13,400
 So Sunk asks a really good question.

17
00:01:13,400 --> 00:01:16,920
 [BLANK_AUDIO]

18
00:01:16,920 --> 00:01:21,080
 How would you explain ignorance playing second fiddle to

19
00:01:21,080 --> 00:01:25,480
 craving as the root cause of suffering according to the

20
00:01:25,480 --> 00:01:26,840
 second noble truth?

21
00:01:28,840 --> 00:01:32,150
 In other words, why is craving highlighted as the cause of

22
00:01:32,150 --> 00:01:33,200
 suffering instead of

23
00:01:33,200 --> 00:01:33,880
 ignorance?

24
00:01:33,880 --> 00:01:38,440
 [BLANK_AUDIO]

25
00:01:38,440 --> 00:01:44,890
 So to recap, the context of this question, there are four

26
00:01:44,890 --> 00:01:46,280
 noble truths.

27
00:01:46,280 --> 00:01:48,200
 [BLANK_AUDIO]

28
00:01:48,200 --> 00:01:53,080
 What it means is, [BLANK_AUDIO]

29
00:01:53,080 --> 00:01:56,970
 There's a difference between a noble truth and an ordinary

30
00:01:56,970 --> 00:01:57,480
 truth.

31
00:01:57,480 --> 00:02:02,520
 Truth is a very special thing.

32
00:02:02,520 --> 00:02:07,200
 But there are many truths that are special in the fact that

33
00:02:07,200 --> 00:02:09,000
 they're true.

34
00:02:09,000 --> 00:02:13,200
 Because there are many more things that are false.

35
00:02:13,200 --> 00:02:17,520
 There's only one capital of France and that's Paris.

36
00:02:17,520 --> 00:02:22,020
 There are a lot of other things that are not capital of

37
00:02:22,020 --> 00:02:23,000
 France.

38
00:02:23,000 --> 00:02:24,360
 That's not a noble truth.

39
00:02:24,360 --> 00:02:27,720
 [BLANK_AUDIO]

40
00:02:27,720 --> 00:02:35,110
 So the four Buddha explicitly delineated the number of

41
00:02:35,110 --> 00:02:38,880
 noble truths as four.

42
00:02:38,880 --> 00:02:39,960
 There's none more than that.

43
00:02:39,960 --> 00:02:43,080
 [BLANK_AUDIO]

44
00:02:43,080 --> 00:02:44,920
 Many other truths in Buddhism.

45
00:02:44,920 --> 00:02:47,680
 [BLANK_AUDIO]

46
00:02:47,680 --> 00:02:52,390
 But the reason why only these four are noble is because

47
00:02:52,390 --> 00:02:52,760
 they

48
00:02:52,760 --> 00:02:57,880
 involve the realization of nirvana.

49
00:02:57,880 --> 00:03:03,160
 They're the only ones that involve directly the realization

50
00:03:03,160 --> 00:03:03,800
 of nirvana.

51
00:03:03,800 --> 00:03:08,800
 [BLANK_AUDIO]

52
00:03:08,800 --> 00:03:14,360
 So in that sense, actually the truth of them is not really

53
00:03:14,360 --> 00:03:15,240
 noble.

54
00:03:15,240 --> 00:03:17,560
 [BLANK_AUDIO]

55
00:03:17,560 --> 00:03:19,960
 In the sense that if you know them intellectually,

56
00:03:19,960 --> 00:03:22,440
 [BLANK_AUDIO]

57
00:03:22,440 --> 00:03:27,080
 If you've read about the four noble truths and you believe

58
00:03:27,080 --> 00:03:27,520
 them,

59
00:03:27,520 --> 00:03:29,080
 [BLANK_AUDIO]

60
00:03:29,080 --> 00:03:30,320
 That's still not noble.

61
00:03:30,320 --> 00:03:32,760
 [BLANK_AUDIO]

62
00:03:32,760 --> 00:03:37,920
 The four noble truths are the experience of freedom.

63
00:03:37,920 --> 00:03:41,240
 [BLANK_AUDIO]

64
00:03:41,240 --> 00:03:44,000
 The truth of suffering.

65
00:03:44,000 --> 00:03:46,000
 The meditator realizes that.

66
00:03:46,000 --> 00:03:49,600
 [BLANK_AUDIO]

67
00:03:49,600 --> 00:03:52,520
 Experience is not something that you can cling to,

68
00:03:52,520 --> 00:03:54,480
 not something that can satisfy you.

69
00:03:54,480 --> 00:03:57,280
 [BLANK_AUDIO]

70
00:03:57,280 --> 00:04:02,000
 Getting more and more experiences, the right type of

71
00:04:02,000 --> 00:04:03,000
 experiences,

72
00:04:03,000 --> 00:04:06,920
 will never lead to happiness.

73
00:04:06,920 --> 00:04:12,480
 [BLANK_AUDIO]

74
00:04:12,480 --> 00:04:14,840
 That's the first noble truth, really that's the first noble

75
00:04:14,840 --> 00:04:15,200
 truth.

76
00:04:15,200 --> 00:04:19,880
 The second noble truth is what is the reason,

77
00:04:19,880 --> 00:04:26,520
 the reason why suffer, cause of suffering is while craving.

78
00:04:26,520 --> 00:04:29,760
 [BLANK_AUDIO]

79
00:04:29,760 --> 00:04:32,870
 And it's craving after those things that actually can't

80
00:04:32,870 --> 00:04:33,800
 satisfy you.

81
00:04:33,800 --> 00:04:38,680
 Braving after those things that cause you stress.

82
00:04:38,680 --> 00:04:40,320
 They cause you stress when you cling to them.

83
00:04:40,320 --> 00:04:42,760
 [BLANK_AUDIO]

84
00:04:42,760 --> 00:04:45,880
 So the cause of stress is craving or clinging or how real

85
00:04:45,880 --> 00:04:46,600
 to say it.

86
00:04:46,600 --> 00:04:56,600
 [BLANK_AUDIO]

87
00:04:56,600 --> 00:05:02,160
 It's a cause of suffering.

88
00:05:02,160 --> 00:05:04,760
 [BLANK_AUDIO]

89
00:05:04,760 --> 00:05:07,640
 It's a cause of suffering because of where it leads us.

90
00:05:07,640 --> 00:05:09,480
 I mean, you can extrapolate upon this.

91
00:05:09,480 --> 00:05:14,360
 The reason why we work so hard and

92
00:05:14,360 --> 00:05:19,840
 strive so hard in the world and fight with each other.

93
00:05:19,840 --> 00:05:21,320
 It's all because of our desire for

94
00:05:21,320 --> 00:05:24,600
 specific experiences, certain experiences.

95
00:05:24,600 --> 00:05:27,600
 Or our aversion towards other experiences.

96
00:05:27,600 --> 00:05:30,600
 [BLANK_AUDIO]

97
00:05:30,600 --> 00:05:34,440
 Fear of certain, what it comes down to, just experiences.

98
00:05:34,440 --> 00:05:44,440
 [BLANK_AUDIO]

99
00:05:44,440 --> 00:05:48,680
 The third noble truth is the cessation of suffering.

100
00:05:48,680 --> 00:05:54,040
 [BLANK_AUDIO]

101
00:05:54,040 --> 00:05:58,880
 So the noble truth, both in the fact that it means that the

102
00:05:58,880 --> 00:05:59,680
 cessation of

103
00:05:59,680 --> 00:06:03,840
 craving is the cessation of suffering, but also it's noble

104
00:06:03,840 --> 00:06:05,520
 in fact that that is

105
00:06:05,520 --> 00:06:10,120
 freedom, that is the cessation of suffering, that is nir

106
00:06:10,120 --> 00:06:10,480
vana.

107
00:06:10,480 --> 00:06:14,080
 [BLANK_AUDIO]

108
00:06:14,080 --> 00:06:15,160
 It is true happiness.

109
00:06:15,160 --> 00:06:17,800
 It's the opposite of the first noble truth.

110
00:06:17,800 --> 00:06:19,920
 [BLANK_AUDIO]

111
00:06:19,920 --> 00:06:24,430
 And the fourth noble truth is the path that leads to the

112
00:06:24,430 --> 00:06:25,560
 cessation of suffering.

113
00:06:25,560 --> 00:06:27,600
 So again, the cause of happiness.

114
00:06:27,600 --> 00:06:30,000
 [BLANK_AUDIO]

115
00:06:30,000 --> 00:06:32,490
 And the cause of happiness is really the opposite of

116
00:06:32,490 --> 00:06:32,960
 craving.

117
00:06:32,960 --> 00:06:36,080
 It's the opposite of reacting.

118
00:06:36,080 --> 00:06:38,960
 [BLANK_AUDIO]

119
00:06:38,960 --> 00:06:43,240
 So cravings are sort of a reaction to experience.

120
00:06:43,240 --> 00:06:47,520
 When a thought arises in the mind, there's a desire for it.

121
00:06:47,520 --> 00:06:52,940
 Like you think of malicious food, there arises a craving, a

122
00:06:52,940 --> 00:06:54,240
 desire.

123
00:06:54,240 --> 00:06:57,880
 And the desire of course leads you then to strive for it,

124
00:06:57,880 --> 00:07:03,190
 which is the beginning of stress and suffering and problems

125
00:07:03,190 --> 00:07:03,840
.

126
00:07:03,840 --> 00:07:10,280
 When you can't get what you want, when you reaffirm your

127
00:07:10,280 --> 00:07:16,160
 craving, your addiction, or when you get what you want and

128
00:07:16,160 --> 00:07:18,000
 it's a cause of,

129
00:07:18,000 --> 00:07:22,120
 suppose you want cheesecake and then you get cheesecake and

130
00:07:22,120 --> 00:07:24,390
 getting it is actually, there's a lot of suffering involved

131
00:07:24,390 --> 00:07:24,760
 because.

132
00:07:24,760 --> 00:07:28,040
 Well, maybe that's not quite right.

133
00:07:28,040 --> 00:07:30,790
 There's suffering involved because you have expectations

134
00:07:30,790 --> 00:07:31,000
 and.

135
00:07:31,000 --> 00:07:34,480
 [BLANK_AUDIO]

136
00:07:34,480 --> 00:07:37,080
 But there are consequences of getting what you want, right?

137
00:07:37,080 --> 00:07:40,520
 If you get lots and lots of cheesecake, you'll become sick,

138
00:07:40,520 --> 00:07:41,640
 you become unhealthy.

139
00:07:41,640 --> 00:07:47,640
 People who eat a lot of sweets or, and

140
00:07:47,640 --> 00:07:50,040
 people who do drugs is a very extreme example.

141
00:07:50,040 --> 00:07:53,760
 You get what you want, it's not, but it's not really fair

142
00:07:53,760 --> 00:07:54,920
 to say that that suffering,

143
00:07:54,920 --> 00:08:00,000
 suffering is really because of the, the attachment.

144
00:08:00,000 --> 00:08:06,360
 And maybe it's still fair because people with attachment or

145
00:08:06,360 --> 00:08:11,760
 with strong attachment are going to cultivate experiences

146
00:08:11,760 --> 00:08:13,960
 that are very stressful.

147
00:08:17,240 --> 00:08:20,200
 Really the core reason is because of their, their addiction

148
00:08:20,200 --> 00:08:21,080
, not because of the,

149
00:08:21,080 --> 00:08:22,800
 the results.

150
00:08:22,800 --> 00:08:25,840
 So getting sick isn't really suffering, it's suffering

151
00:08:25,840 --> 00:08:27,320
 because you don't want to be sick,

152
00:08:27,320 --> 00:08:30,320
 because we have desire for pleasant experiences.

153
00:08:30,320 --> 00:08:34,000
 But it is true that the more you strive after pleasant

154
00:08:34,000 --> 00:08:35,560
 experiences,

155
00:08:35,560 --> 00:08:39,060
 the more likely you are to fall into unpleasant experiences

156
00:08:39,060 --> 00:08:39,320
.

157
00:08:39,320 --> 00:08:45,250
 A person who lives contently, who lives off in the forest,

158
00:08:45,250 --> 00:08:45,520
 say.

159
00:08:47,480 --> 00:08:50,080
 They'll be free from, from certain types of suffering or

160
00:08:50,080 --> 00:08:52,360
 stressful experiences.

161
00:08:52,360 --> 00:09:00,920
 Simply by virtue of not needing to fight with people and

162
00:09:00,920 --> 00:09:05,920
 work labor, laborious jobs and so on.

163
00:09:05,920 --> 00:09:13,350
 Anyway, so the path, so that, that's cause of suffering is,

164
00:09:13,350 --> 00:09:14,400
 is our,

165
00:09:14,400 --> 00:09:15,400
 our craving.

166
00:09:15,400 --> 00:09:19,400
 The, the path, what leads out of that is the opposite when

167
00:09:19,400 --> 00:09:20,560
 you have experiences in

168
00:09:20,560 --> 00:09:21,080
 your mind.

169
00:09:21,080 --> 00:09:24,870
 It's the Eightfold Noble Path really, but it involves this

170
00:09:24,870 --> 00:09:25,920
 practice that we're doing

171
00:09:25,920 --> 00:09:29,400
 of learning how to have perfect experience.

172
00:09:29,400 --> 00:09:36,880
 An experience that's free from that sort of reaction.

173
00:09:36,880 --> 00:09:42,960
 So those are the Four Noble Truths.

174
00:09:42,960 --> 00:09:45,990
 And the second one is, says that craving is the cause of

175
00:09:45,990 --> 00:09:46,840
 suffering.

176
00:09:46,840 --> 00:09:51,880
 But we also, if you look at the Teachesamupada, it's clear

177
00:09:51,880 --> 00:09:52,160
 that,

178
00:09:52,160 --> 00:09:55,650
 independent origination, it's clear that ignorance is the

179
00:09:55,650 --> 00:09:58,120
 beginning, right?

180
00:09:58,120 --> 00:10:00,000
 Without ignorance, there would be no craving.

181
00:10:00,000 --> 00:10:02,920
 But why isn't ignorance the cause of suffering?

182
00:10:02,920 --> 00:10:08,840
 Because what we mean by ignorance is ignorance of the fact,

183
00:10:08,840 --> 00:10:13,130
 ignorance of the Four Noble Truths, that's what is, we mean

184
00:10:13,130 --> 00:10:14,040
 by ignorance.

185
00:10:14,040 --> 00:10:21,590
 So, I mean, suggesting that ignorance is the cause of

186
00:10:21,590 --> 00:10:23,080
 suffering is,

187
00:10:23,080 --> 00:10:27,600
 it's like saying, you have a thorn in, you have a, a,

188
00:10:27,600 --> 00:10:31,240
 a thorn in your side or a sliver in your hand or something.

189
00:10:31,240 --> 00:10:33,640
 Piece of glass in your foot.

190
00:10:36,840 --> 00:10:40,930
 And it's causing you suffering, but it's made worse by the

191
00:10:40,930 --> 00:10:41,280
 fact that

192
00:10:41,280 --> 00:10:43,520
 you don't even know that it's there, right?

193
00:10:43,520 --> 00:10:50,440
 If you don't know that the sliver is there, that makes,

194
00:10:50,440 --> 00:10:50,720
 that,

195
00:10:50,720 --> 00:10:53,840
 that's the reason why it continues to hurt you,

196
00:10:53,840 --> 00:10:55,240
 because you haven't gotten rid of it.

197
00:10:55,240 --> 00:11:02,640
 Or maybe more precisely, suppose you have,

198
00:11:02,640 --> 00:11:07,720
 suppose you eat unhealthy food, it's the unhealthy food

199
00:11:07,720 --> 00:11:09,320
 that's causing you sickness,

200
00:11:09,320 --> 00:11:12,150
 but it's the ignorance of the fact that the food is

201
00:11:12,150 --> 00:11:12,800
 unhealthy for

202
00:11:12,800 --> 00:11:16,560
 you that's causing you to eat the unhealthy food.

203
00:11:16,560 --> 00:11:19,180
 So it would be more fair to say that ignorance is the cause

204
00:11:19,180 --> 00:11:19,920
 of craving.

205
00:11:19,920 --> 00:11:25,560
 Without craving, there is no, without ignorance, there is

206
00:11:25,560 --> 00:11:26,120
 no craving.

207
00:11:30,760 --> 00:11:33,810
 And what that, all that means is if you knew that the

208
00:11:33,810 --> 00:11:35,040
 things that you were craving

209
00:11:35,040 --> 00:11:40,300
 were not satisfying, if you knew that craving was useless,

210
00:11:40,300 --> 00:11:41,320
 was meaningless,

211
00:11:41,320 --> 00:11:45,260
 or if you experience things as they are, there would be no

212
00:11:45,260 --> 00:11:45,720
 craving.

213
00:11:45,720 --> 00:11:51,880
 When we walk, stepping right, stepping left, in that moment

214
00:11:51,880 --> 00:11:51,880
,

215
00:11:51,880 --> 00:11:55,160
 there's no opportunity for the arising craving because our

216
00:11:55,160 --> 00:11:56,200
 interaction with our

217
00:11:56,200 --> 00:12:00,940
 experiences is, is equanimous, it's clear, it's neutral, it

218
00:12:00,940 --> 00:12:01,800
's pure.

219
00:12:01,800 --> 00:12:11,430
 So this is why ignorance is not, and the other thing is

220
00:12:11,430 --> 00:12:13,400
 that ignorance isn't really a thing, right?

221
00:12:13,400 --> 00:12:18,360
 You don't suddenly give rise to ignorance.

222
00:12:18,360 --> 00:12:20,680
 Ignorance just means the fact that you don't know yet.

223
00:12:23,080 --> 00:12:27,400
 It's not something really that arises, so it's not a thing.

224
00:12:27,400 --> 00:12:32,730
 Ignorance is a state, it's a description of a type of

225
00:12:32,730 --> 00:12:33,640
 person.

226
00:12:33,640 --> 00:12:38,460
 A person has ignorance just means that they've not yet

227
00:12:38,460 --> 00:12:39,320
 learned the truth.

228
00:12:39,320 --> 00:12:45,310
 And it really means they've not yet become enlightened

229
00:12:45,310 --> 00:12:47,160
 because it's the experience of

230
00:12:47,160 --> 00:12:50,920
 nirvana that does away with, with what we term ignorance,

231
00:12:50,920 --> 00:12:51,880
 changes a person.

232
00:12:53,040 --> 00:12:55,640
 So ignorance is a description of a worlding, an ordinary

233
00:12:55,640 --> 00:12:56,200
 person.

234
00:12:56,200 --> 00:13:00,110
 A person who's become enlightened is, is free from that

235
00:13:00,110 --> 00:13:00,920
 ignorance.

236
00:13:00,920 --> 00:13:05,720
 It's in the category of being free from ignorance.

237
00:13:05,720 --> 00:13:11,960
 But anyway, that may be enough to say about that.

238
00:13:11,960 --> 00:13:18,040
 Why are the cemetery contemplations part of the Satyavat

239
00:13:18,040 --> 00:13:19,400
anasuta?

240
00:13:19,400 --> 00:13:23,720
 It's a good question, it's not really relatable for this

241
00:13:23,720 --> 00:13:26,440
 group because we're not wondering whether

242
00:13:26,440 --> 00:13:27,400
 we should practice them.

243
00:13:27,400 --> 00:13:31,790
 So this maybe gives us a chance to talk about samatam vip

244
00:13:31,790 --> 00:13:32,760
assana.

245
00:13:32,760 --> 00:13:35,860
 There are certain types of meditation or ways of focusing

246
00:13:35,860 --> 00:13:38,440
 your attention that are, are,

247
00:13:38,440 --> 00:13:44,520
 that are just for the purposes of tranquilizing the mind.

248
00:13:44,520 --> 00:13:47,320
 And the cemetery contemplations are that sort of meditation

249
00:13:47,320 --> 00:13:49,080
, but they're also just generally useful.

250
00:13:50,600 --> 00:13:54,000
 I think they're included there just because it's the Satyav

251
00:13:54,000 --> 00:13:56,040
atanasuta seems to be a,

252
00:13:56,040 --> 00:14:00,680
 a way of describing a life, you know, a way of living your

253
00:14:00,680 --> 00:14:03,720
 life if you read through the various

254
00:14:03,720 --> 00:14:06,520
 sections of, especially kaya nipasana, body.

255
00:14:06,520 --> 00:14:09,080
 That's one.

256
00:14:09,080 --> 00:14:12,250
 If you contemplate dead bodies, it, it fortifies your

257
00:14:12,250 --> 00:14:13,880
 insight practice because

258
00:14:16,280 --> 00:14:20,430
 it creates sobriety about the body and it reduces your

259
00:14:20,430 --> 00:14:24,040
 delusions about the body being something

260
00:14:24,040 --> 00:14:25,800
 beautiful or special or wonderful.

261
00:14:25,800 --> 00:14:32,760
 When you see dead bodies rotting and pus coming out of them

262
00:14:32,760 --> 00:14:34,280
, breaking up

263
00:14:34,280 --> 00:14:38,290
 with just flesh hanging off the bones and eventually with

264
00:14:38,290 --> 00:14:39,080
 just bones.

265
00:14:39,080 --> 00:14:46,840
 Okay.

266
00:14:46,840 --> 00:14:49,720
 And then we have some questions that I don't really, well,

267
00:14:49,720 --> 00:14:51,080
 we'll go through them fairly quickly.

268
00:14:51,080 --> 00:14:55,490
 What's the difference between a dead body and a body that's

269
00:14:55,490 --> 00:14:55,960
 alive?

270
00:14:55,960 --> 00:15:05,160
 Well, body isn't something that exists.

271
00:15:05,160 --> 00:15:08,790
 I mean, I guess you'd want to say the mind isn't there in

272
00:15:08,790 --> 00:15:11,000
 the dead body, but

273
00:15:11,000 --> 00:15:14,120
 the body is just concept.

274
00:15:14,120 --> 00:15:22,480
 It's a temporary amalgamation or conglomeration of physical

275
00:15:22,480 --> 00:15:23,160
 particles,

276
00:15:23,160 --> 00:15:27,960
 but there's a history of why it's there.

277
00:15:27,960 --> 00:15:31,000
 So you're just really seeing an echo of some activity, some

278
00:15:31,000 --> 00:15:31,880
 mental activity.

279
00:15:32,600 --> 00:15:35,880
 Body, the dead body is like an echo of the mind.

280
00:15:35,880 --> 00:15:38,880
 Because if you think back to how it arose, it rose in the

281
00:15:38,880 --> 00:15:43,480
 womb of the mother and was created,

282
00:15:43,480 --> 00:15:47,810
 we would say, based on the activity, the active mind in the

283
00:15:47,810 --> 00:15:48,360
 womb.

284
00:15:48,360 --> 00:15:51,960
 And when you die, that, well, that ceases.

285
00:15:51,960 --> 00:15:55,590
 So all you've got left is sort of the echo of that activity

286
00:15:55,590 --> 00:15:55,880
.

287
00:15:55,880 --> 00:16:00,520
 What's the relation between the previous and next birth?

288
00:16:02,520 --> 00:16:08,740
 Well, that's maybe a long question, long answer, explaining

289
00:16:08,740 --> 00:16:10,360
 rebirth.

290
00:16:10,360 --> 00:16:14,920
 But basically, again, birth is just a concept.

291
00:16:14,920 --> 00:16:17,000
 The mind is born and dies every moment.

292
00:16:17,000 --> 00:16:21,640
 When we die, it's just the body.

293
00:16:21,640 --> 00:16:25,480
 It's more like discarding in this body and starting fresh

294
00:16:25,480 --> 00:16:27,560
 because the body wears out.

295
00:16:27,560 --> 00:16:34,360
 Is rebirth caused by the result of the mind clinging onto

296
00:16:34,360 --> 00:16:37,000
 more experiences?

297
00:16:37,000 --> 00:16:38,520
 Yeah, yes.

298
00:16:38,520 --> 00:16:41,800
 It's really just generally the fact that there's clinging

299
00:16:41,800 --> 00:16:43,080
 left because the mind

300
00:16:43,080 --> 00:16:47,000
 with clinging will seek out, will seek, you know.

301
00:16:47,000 --> 00:16:50,520
 So clinging onto more experiences, absolutely, that's it.

302
00:16:50,520 --> 00:16:56,840
 Is performing magic not practicing the precept on deceit?

303
00:16:58,440 --> 00:17:02,140
 Well, if you're talking about sleight of hand, like not

304
00:17:02,140 --> 00:17:04,920
 real magic, but performing

305
00:17:04,920 --> 00:17:08,950
 fake magic, like not really doing magical tricks, but

306
00:17:08,950 --> 00:17:12,120
 presenting them as being magical tricks,

307
00:17:12,120 --> 00:17:13,320
 well, that's one thing.

308
00:17:13,320 --> 00:17:16,140
 I'd say most magicians accept the fact that their audience

309
00:17:16,140 --> 00:17:18,200
 doesn't believe that they actually are

310
00:17:18,200 --> 00:17:23,070
 magical and knows that they're being trick, which makes it

311
00:17:23,070 --> 00:17:24,280
 not a trick.

312
00:17:25,400 --> 00:17:29,960
 And so magic just becomes the act of trying to figure out

313
00:17:29,960 --> 00:17:32,360
 how someone did what they did.

314
00:17:32,360 --> 00:17:38,880
 So it's a matter of, magic in that sense is just about

315
00:17:38,880 --> 00:17:41,880
 hiding, trying to hide your methods,

316
00:17:41,880 --> 00:17:48,210
 and the audience is left wondering about your methods, not

317
00:17:48,210 --> 00:17:50,280
 about pretending that you did

318
00:17:50,280 --> 00:17:52,440
 something one way and instead doing it another way.

319
00:17:54,120 --> 00:17:55,720
 Like people don't actually leave.

320
00:17:55,720 --> 00:17:56,440
 Wow.

321
00:17:56,440 --> 00:17:59,310
 Although it's interesting if you're talking about kids, you

322
00:17:59,310 --> 00:18:03,400
 may actually fool the children.

323
00:18:03,400 --> 00:18:08,520
 Like, for example, Santa Claus, let's say, right?

324
00:18:08,520 --> 00:18:11,900
 Lying to children that there exists this Santa Claus is

325
00:18:11,900 --> 00:18:13,160
 actually lying.

326
00:18:13,160 --> 00:18:14,360
 It's actually unethical.

327
00:18:14,360 --> 00:18:17,640
 Santa Claus is really bad, a really bad idea.

328
00:18:17,640 --> 00:18:22,260
 All around that, you know, there's nothing good about Santa

329
00:18:22,260 --> 00:18:22,600
 Claus.

330
00:18:22,600 --> 00:18:23,560
 Come to think about it.

331
00:18:24,680 --> 00:18:25,480
 I'm probably wrong.

332
00:18:25,480 --> 00:18:27,240
 They're probably if I thought about it long enough.

333
00:18:27,240 --> 00:18:30,330
 But a superficial look at Santa Claus makes him out to be a

334
00:18:30,330 --> 00:18:31,240
 real bad guy.

335
00:18:31,240 --> 00:18:34,120
 First of all, there's this guilt, right?

336
00:18:34,120 --> 00:18:35,400
 It's like this boogeyman.

337
00:18:35,400 --> 00:18:38,120
 He knows if you've been bad or good, right?

338
00:18:38,120 --> 00:18:40,360
 So it's a way of guilt-tripping kids.

339
00:18:40,360 --> 00:18:44,920
 It's a way of making ethics and morality all about rewards.

340
00:18:44,920 --> 00:18:47,160
 If you don't do this, you're not going to get some toy that

341
00:18:47,160 --> 00:18:47,800
 you want.

342
00:18:47,800 --> 00:18:50,680
 I mean, what does that say about morality?

343
00:18:50,680 --> 00:18:53,160
 And they say it's going to be good for goodness sake.

344
00:18:53,160 --> 00:18:55,320
 But you know, this song, right?

345
00:18:55,320 --> 00:18:58,880
 Santa Claus is the opposite of doing good for goodness sake

346
00:18:58,880 --> 00:18:59,160
.

347
00:18:59,160 --> 00:19:03,960
 Santa Claus is about doing good because your parents lied

348
00:19:03,960 --> 00:19:05,320
 to you about getting rewards.

349
00:19:05,320 --> 00:19:12,520
 You could say it's all for good fun, but I disagree.

350
00:19:12,520 --> 00:19:14,520
 It's tricking your children.

351
00:19:14,520 --> 00:19:16,620
 And then eventually they find that they've been lying to

352
00:19:16,620 --> 00:19:17,560
 them all these years.

353
00:19:17,560 --> 00:19:20,520
 And they learn that's the good way to behave.

354
00:19:20,520 --> 00:19:21,960
 You know, lie to people.

355
00:19:23,560 --> 00:19:24,760
 Yes, I know it's more.

356
00:19:24,760 --> 00:19:29,220
 It's all in good fun to some extent, but that's not my idea

357
00:19:29,220 --> 00:19:30,440
 of good fun.

358
00:19:30,440 --> 00:19:34,520
 I was thinking about this whole gift giving thing.

359
00:19:34,520 --> 00:19:38,870
 Gift giving around Christmas is also, I really am not so

360
00:19:38,870 --> 00:19:39,960
 keen on it.

361
00:19:39,960 --> 00:19:43,720
 Especially with kids because of all the greed involved.

362
00:19:43,720 --> 00:19:48,600
 But gifts should be much more natural, you know?

363
00:19:49,960 --> 00:19:52,680
 For Christmas there tends to be, I don't know, I mean,

364
00:19:52,680 --> 00:19:56,440
 people really enjoy it and have, it's great to give.

365
00:19:56,440 --> 00:20:01,260
 I'm just not convinced that it's the best idea to have a

366
00:20:01,260 --> 00:20:03,400
 holiday for giving.

367
00:20:03,400 --> 00:20:08,040
 If you want to make a tradition in your family, when

368
00:20:08,040 --> 00:20:09,880
 everyone gets together, we all give gifts.

369
00:20:09,880 --> 00:20:12,760
 But it still, I think, misses the point.

370
00:20:12,760 --> 00:20:16,840
 You know, this whole giving gifts to everyone.

371
00:20:18,600 --> 00:20:22,280
 It's very active sort of giving.

372
00:20:22,280 --> 00:20:27,640
 It's very much active in the sense of wanting to give

373
00:20:27,640 --> 00:20:31,400
 and creating this desire to give, which is actually not

374
00:20:31,400 --> 00:20:33,160
 really very Buddhist.

375
00:20:33,160 --> 00:20:35,640
 Giving should be natural.

376
00:20:35,640 --> 00:20:37,480
 Someone needs something, you give it to them.

377
00:20:37,480 --> 00:20:18,450
 Someone asks you for something or someone deserves

378
00:20:18,450 --> 00:20:45,640
 something,

379
00:20:45,640 --> 00:20:47,880
 like your parents, for example.

380
00:20:47,880 --> 00:20:52,760
 You think, "Oh, I want to express my gratitude."

381
00:20:52,760 --> 00:20:55,800
 Or, "Gratitude to a teacher," that sort of thing.

382
00:20:55,800 --> 00:20:58,040
 "Oh, sorry, I shouldn't. I'm thinking of my teacher.

383
00:20:58,040 --> 00:21:01,310
 I wasn't trying to suggest giving me anything. Don't go

384
00:21:01,310 --> 00:21:01,640
 there."

385
00:21:01,640 --> 00:21:10,120
 But yeah, this whole Christmas thing.

386
00:21:10,120 --> 00:21:14,840
 I think if Christmas is about getting together as a family,

387
00:21:14,840 --> 00:21:16,520
 I think that part is fairly wholesome.

388
00:21:16,520 --> 00:21:18,520
 But the gift-giving part,

389
00:21:18,520 --> 00:21:22,440
 we had this charity thing.

390
00:21:22,440 --> 00:21:24,840
 We all got together and gave money to charity.

391
00:21:24,840 --> 00:21:29,880
 All my students did and my family sort of got involved.

392
00:21:29,880 --> 00:21:32,680
 I think that's a good Christmas tradition.

393
00:21:32,680 --> 00:21:37,320
 I'm not the only one. Families do that, you know.

394
00:21:37,320 --> 00:21:38,360
 But we did it as well.

395
00:21:38,360 --> 00:21:41,090
 And we got this really nice letter back from them saying

396
00:21:41,090 --> 00:21:43,800
 how much we're helping children.

397
00:21:45,800 --> 00:21:48,280
 That's better than this whole Santa Claus croc.

398
00:21:48,280 --> 00:21:54,520
 I'm being purposely controversial, I think.

399
00:21:54,520 --> 00:21:58,680
 "Will you be broadcasting on Second Life in the future?"

400
00:21:58,680 --> 00:21:59,880
 I'm not sure, actually.

401
00:21:59,880 --> 00:22:03,730
 I've got an email sitting in my inbox that I have to answer

402
00:22:03,730 --> 00:22:04,760
 about that.

403
00:22:04,760 --> 00:22:09,080
 But I was thinking about Second Life and I don't really see

404
00:22:09,080 --> 00:22:10,120
 the point.

405
00:22:12,120 --> 00:22:15,560
 I'd like to support the Buddhist Center, that thing, but...

406
00:22:15,560 --> 00:22:21,960
 I'm not clear that it's actually of great...

407
00:22:21,960 --> 00:22:24,520
 You know what I was thinking about today?

408
00:22:24,520 --> 00:22:28,600
 I think it'd be a real gimmick if I did a virtual reality,

409
00:22:28,600 --> 00:22:31,800
 some kind of virtual reality thing.

410
00:22:31,800 --> 00:22:34,680
 I tried virtual reality when I was in Thailand.

411
00:22:34,680 --> 00:22:39,080
 And I was able to sit up on a cliff and meditate.

412
00:22:39,080 --> 00:22:40,680
 And I was sitting up on a cliff, you know.

413
00:22:40,680 --> 00:22:45,000
 And I look around.

414
00:22:45,000 --> 00:22:47,480
 I was thinking, you know, be a...

415
00:22:47,480 --> 00:22:51,320
 By "I mean gimmick," I mean it'd be something that might

416
00:22:51,320 --> 00:22:55,560
 get the attention of the press or something.

417
00:22:55,560 --> 00:22:59,000
 So it's kind of a silly thing to do, but it might be neat

418
00:22:59,000 --> 00:23:00,760
 to go that way and

419
00:23:00,760 --> 00:23:04,520
 make something that's not been done before.

420
00:23:04,520 --> 00:23:08,060
 It's that, you know, you walk, you sit, you go into this

421
00:23:08,060 --> 00:23:09,240
 virtual reality,

422
00:23:09,240 --> 00:23:12,440
 and suddenly I come walking towards you and explain to you

423
00:23:12,440 --> 00:23:13,240
 how to meditate.

424
00:23:13,240 --> 00:23:15,160
 And we go through the exercises together.

425
00:23:15,160 --> 00:23:19,080
 I don't know how difficult that would be to program, but...

426
00:23:19,080 --> 00:23:21,800
 If there's anybody out there who knows how to do that,

427
00:23:21,800 --> 00:23:24,600
 maybe we could think about that.

428
00:23:24,600 --> 00:23:28,840
 But, you know, mass media is much more...

429
00:23:28,840 --> 00:23:29,480
 There's two...

430
00:23:29,480 --> 00:23:33,320
 I think two useful activities.

431
00:23:33,320 --> 00:23:38,120
 One is to produce things that can be mass consumed.

432
00:23:39,000 --> 00:23:42,520
 And the other is to do intensive one-on-one training.

433
00:23:42,520 --> 00:23:49,330
 And I think that's sort of the two important aspects of,

434
00:23:49,330 --> 00:23:50,040
 from my point of view,

435
00:23:50,040 --> 00:23:51,240
 of what should be done.

436
00:23:51,240 --> 00:23:55,080
 But teaching small groups, giving a talk to small groups,

437
00:23:55,080 --> 00:23:57,640
 it doesn't really interest me.

438
00:23:57,640 --> 00:24:04,600
 Because if I'm going to teach 10 people or five people...

439
00:24:04,600 --> 00:24:12,920
 ...who, you know, who aren't doing a meditation course, or

440
00:24:12,920 --> 00:24:12,920
...

441
00:24:12,920 --> 00:24:15,240
 ...really...

442
00:24:15,240 --> 00:24:19,750
 It's not that I'm doing it, it's that going out of my way

443
00:24:19,750 --> 00:24:21,080
 to do it doesn't really seem...

444
00:24:21,080 --> 00:24:26,600
 All that beneficial, it's just too small of a...

445
00:24:26,600 --> 00:24:29,160
 I mean, the benefit is too small.

446
00:24:30,040 --> 00:24:34,910
 When, you know, giving a talk on YouTube is the same result

447
00:24:34,910 --> 00:24:36,440
, except it reaches a lot more people.

448
00:24:36,440 --> 00:24:41,840
 I guess what I really mean is it's a lot of work to boot up

449
00:24:41,840 --> 00:24:43,080
 Second Life and get in there.

450
00:24:43,080 --> 00:24:48,470
 It's not that I can't do it, it's just I have to ask myself

451
00:24:48,470 --> 00:24:49,400
 why I'm doing it.

452
00:24:49,400 --> 00:24:51,640
 And as a monk, that's an important question, because...

453
00:24:51,640 --> 00:24:56,950
 You know, I have to be careful that I'm not going outside

454
00:24:56,950 --> 00:24:59,240
 of what a Buddhist monk should do.

455
00:24:59,880 --> 00:25:02,530
 Not so much that I feel lazy or something like that, it's

456
00:25:02,530 --> 00:25:03,560
 not laziness.

457
00:25:03,560 --> 00:25:08,700
 It's a question of, you know, going beyond this realm of

458
00:25:08,700 --> 00:25:13,000
 teaching, because people ask you to teach.

459
00:25:13,000 --> 00:25:17,480
 I think there's a line there.

460
00:25:17,480 --> 00:25:22,480
 Someone says, "My microphone has broken, the voice is too

461
00:25:22,480 --> 00:25:23,240
 low."

462
00:25:26,680 --> 00:25:29,360
 There's a reason to stop all this entirely, it's really

463
00:25:29,360 --> 00:25:30,440
 just not working.

464
00:25:30,440 --> 00:25:40,680
 Hello, hello.

465
00:25:40,680 --> 00:25:47,560
 Test, test.

466
00:25:47,560 --> 00:25:53,960
 Hello, hello. Yeah, why is that so low?

467
00:25:53,960 --> 00:26:00,920
 I don't know, it still looks fine to me.

468
00:26:00,920 --> 00:26:07,880
 Why?

469
00:26:07,880 --> 00:26:28,120
 I don't think I changed any settings.

470
00:26:28,120 --> 00:26:43,240
 [ "I'll turn your volume up, maybe."

471
00:26:43,240 --> 00:26:51,320
 Anyway, that's all the questions, and I think that was some

472
00:26:51,320 --> 00:26:53,320
 good dumb.

473
00:26:53,320 --> 00:26:56,600
 So I'm gonna call it a meet there.

474
00:26:57,640 --> 00:27:00,920
 Maybe I'm just speaking quietly.

475
00:27:00,920 --> 00:27:06,440
 Anyway, have a good night. Thank you all for tuning in.

476
00:27:06,440 --> 00:27:16,280
 [ "I'll turn your volume up, maybe." ]

477
00:27:16,280 --> 00:27:24,120
 [ "I'll turn your volume up, maybe." ]

