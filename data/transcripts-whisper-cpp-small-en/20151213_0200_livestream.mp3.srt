1
00:00:00,000 --> 00:00:07,080
 Good evening everyone.

2
00:00:07,080 --> 00:00:11,640
 Broadcasting live December 12th, 2015.

3
00:00:11,640 --> 00:00:19,720
 Maybe a short broadcast tonight.

4
00:00:19,720 --> 00:00:22,270
 I'm realizing that I have quite a bit of studying left to

5
00:00:22,270 --> 00:00:23,800
 do for the next two exams.

6
00:00:23,800 --> 00:00:29,230
 I haven't taken these classes probably as seriously as I

7
00:00:29,230 --> 00:00:30,800
 should have.

8
00:00:30,800 --> 00:00:35,800
 I had to do a lot of reading.

9
00:00:35,800 --> 00:00:39,800
 Probably not going to do that well on either of these exams

10
00:00:39,800 --> 00:00:39,800
.

11
00:00:39,800 --> 00:00:41,800
 What subjects, Pante?

12
00:00:41,800 --> 00:00:49,800
 Linguistics and philosophy.

13
00:00:49,800 --> 00:00:52,930
 Monday we're having a study session, so two of my

14
00:00:52,930 --> 00:00:54,800
 classmates are coming over.

15
00:00:54,800 --> 00:00:58,800
 We're going to sit down for the linguistics exam.

16
00:00:58,800 --> 00:01:01,460
 I'm not really worried about it because that's a first year

17
00:01:01,460 --> 00:01:01,800
 class.

18
00:01:01,800 --> 00:01:05,120
 I don't think it really matters either way as long as I

19
00:01:05,120 --> 00:01:07,800
 pass it, which I will.

20
00:01:07,800 --> 00:01:14,800
 But somehow I was talking with another student about this

21
00:01:14,800 --> 00:01:15,800
 for the Latin exam.

22
00:01:15,800 --> 00:01:22,800
 I was talking to someone about how it feels kind of wrong

23
00:01:22,800 --> 00:01:30,800
 to not do your best at something.

24
00:01:30,800 --> 00:01:32,800
 It's strange.

25
00:01:32,800 --> 00:01:37,800
 There's still an intention to get 100% in everything.

26
00:01:37,800 --> 00:01:43,380
 I don't get 100% while a lesson has to be learned to try to

27
00:01:43,380 --> 00:01:46,800
 get 100% next time,

28
00:01:46,800 --> 00:01:52,800
 which is a difficult position to be in.

29
00:01:52,800 --> 00:01:54,800
 We'll see if I get 100% in Latin.

30
00:01:54,800 --> 00:01:57,800
 Wouldn't that be funny?

31
00:01:57,800 --> 00:01:59,800
 I had 100% going into the exam.

32
00:01:59,800 --> 00:02:04,800
 Probably got a 95.

33
00:02:04,800 --> 00:02:08,800
 So Latin was last week's?

34
00:02:08,800 --> 00:02:10,800
 There was a bonus question.

35
00:02:10,800 --> 00:02:13,290
 I asked if there was going to be a bonus section because he

36
00:02:13,290 --> 00:02:14,800
 puts bonus on all the quizzes,

37
00:02:14,800 --> 00:02:16,800
 and they're actually quite generous.

38
00:02:16,800 --> 00:02:22,360
 So I actually on each of my quizzes I got over 100% because

39
00:02:22,360 --> 00:02:23,800
 of the bonus.

40
00:02:23,800 --> 00:02:27,800
 I think average was like 103%.

41
00:02:27,800 --> 00:02:33,240
 So there was bonus on the exam, but the bonus was actually

42
00:02:33,240 --> 00:02:34,800
 quite cheap.

43
00:02:34,800 --> 00:02:37,800
 Out of 200 marks, it was a 3-mark bonus question.

44
00:02:37,800 --> 00:02:44,800
 But why I bring it up, and this is actually, it is related.

45
00:02:44,800 --> 00:02:51,230
 It was an interesting quote, interesting probably to many

46
00:02:51,230 --> 00:02:51,800
 of us,

47
00:02:51,800 --> 00:02:57,800
 because he brought up this quote that had come up recently

48
00:02:57,800 --> 00:03:00,800
 in the media, a Latin quote.

49
00:03:00,800 --> 00:03:07,040
 And in regards to one of the biggest stories of the past

50
00:03:07,040 --> 00:03:07,800
 month,

51
00:03:07,800 --> 00:03:14,340
 if anyone knows what that quote was, because it's actually

52
00:03:14,340 --> 00:03:16,800
 quite a famous quote.

53
00:03:16,800 --> 00:03:21,800
 I had, you know, not being a news person, I hadn't seen it,

54
00:03:21,800 --> 00:03:22,800
 but he brought up a picture,

55
00:03:22,800 --> 00:03:28,800
 and it's now on the Wikipedia page for that quote.

56
00:03:28,800 --> 00:03:37,800
 The quote is "Floc duat n'ec morge tour."

57
00:03:37,800 --> 00:03:41,760
 It doesn't ring any bells in it, unless you're really

58
00:03:41,760 --> 00:03:42,800
 keeping up with the news,

59
00:03:42,800 --> 00:03:46,840
 or unless you happen to be from France, because it's a very

60
00:03:46,840 --> 00:03:48,800
 famous quote in France.

61
00:03:48,800 --> 00:03:53,800
 "Floc duat" means "tossed about the fluctuate," right?

62
00:03:53,800 --> 00:04:02,800
 "Floc duat" means "to be shaken or disturbed."

63
00:04:02,800 --> 00:04:07,800
 It's using the imagery of a boat.

64
00:04:07,800 --> 00:04:13,400
 "Floc duat" means "shaken about," "tossed about" by the way

65
00:04:13,400 --> 00:04:13,800
.

66
00:04:13,800 --> 00:04:17,800
 "Nec" means "and not."

67
00:04:17,800 --> 00:04:21,800
 And "merge tour."

68
00:04:21,800 --> 00:04:27,800
 "Merge tour" means "merge tour" maybe.

69
00:04:27,800 --> 00:04:31,800
 "Merge" is not "merge," it's from "submerge."

70
00:04:31,800 --> 00:04:35,800
 So "merge tour" means "sank."

71
00:04:35,800 --> 00:04:39,800
 So a boat, a successful ship, is one that is tossed about

72
00:04:39,800 --> 00:04:47,800
 but not submerged, not sunk.

73
00:04:47,800 --> 00:04:51,320
 I just barely remembered him bringing up and forgot totally

74
00:04:51,320 --> 00:04:51,800
 what it was about,

75
00:04:51,800 --> 00:04:56,800
 but I kind of guessed, and actually I think I got it right.

76
00:04:56,800 --> 00:05:00,800
 But tossed about and not submerged.

77
00:05:00,800 --> 00:05:06,110
 I think it's more like tossed about and not sunk, but

78
00:05:06,110 --> 00:05:07,800
 basically that.

79
00:05:07,800 --> 00:05:14,040
 Anyway, it was used, it's been used for many occasions in

80
00:05:14,040 --> 00:05:18,800
 France to talk about how they are "resilient."

81
00:05:18,800 --> 00:05:21,890
 It's a quote about resiliency, but they also used it for

82
00:05:21,890 --> 00:05:22,800
 the Paris bombings

83
00:05:22,800 --> 00:05:29,210
 that show that they weren't going to be defeated, weren't

84
00:05:29,210 --> 00:05:32,800
 going to let this defeat them.

85
00:05:32,800 --> 00:05:35,580
 It's a quote about resiliency, which I think is a very good

86
00:05:35,580 --> 00:05:40,800
 quote from Buddhism, from Buddhist practice.

87
00:05:40,800 --> 00:05:47,360
 And it's a very good Buddhist response to things like

88
00:05:47,360 --> 00:05:49,800
 terrorist attacks

89
00:05:49,800 --> 00:05:52,800
 and the difficulties in life.

90
00:05:52,800 --> 00:06:02,800
 Shaken, yes, never sunk.

91
00:06:02,800 --> 00:06:06,820
 There's always another chance. You always get another

92
00:06:06,820 --> 00:06:07,800
 chance.

93
00:06:07,800 --> 00:06:11,530
 We have the Buddha as our example of that, the Buddha who

94
00:06:11,530 --> 00:06:13,800
 took four uncountable eons

95
00:06:13,800 --> 00:06:26,800
 and 100,000 countable eons, great eons, to become a Buddha.

96
00:06:26,800 --> 00:06:34,160
 All the time it took him, all the mistakes he had to make

97
00:06:34,160 --> 00:06:36,800
 to get there.

98
00:06:36,800 --> 00:06:46,520
 And I think that our one little life is not really worth

99
00:06:46,520 --> 00:06:51,800
 all the concern we give it.

100
00:06:51,800 --> 00:06:56,520
 Like there's that Zen book that my mother got and showed to

101
00:06:56,520 --> 00:06:56,800
 me,

102
00:06:56,800 --> 00:07:05,800
 "Don't sweat the small stuff, and it's all small stuff."

103
00:07:05,800 --> 00:07:14,800
 No questions tonight?

104
00:07:14,800 --> 00:07:20,800
 I don't think so.

105
00:07:20,800 --> 00:07:24,800
 We have here a quote about an acrobat, right?

106
00:07:24,800 --> 00:07:27,800
 Yes, I like that quote.

107
00:07:27,800 --> 00:07:30,800
 It says "protect", but protect is a poor translation.

108
00:07:30,800 --> 00:07:34,970
 It is a literal translation, but it means guard. Guard

109
00:07:34,970 --> 00:07:35,800
 would probably be better.

110
00:07:35,800 --> 00:07:45,800
 Watch out for raka, but raka, guard is better.

111
00:07:45,800 --> 00:08:02,800
 I will...

112
00:08:02,800 --> 00:08:06,000
 He does use guard. He watched and guarded by himself for

113
00:08:06,000 --> 00:08:06,800
 the first part,

114
00:08:06,800 --> 00:08:13,800
 but then in the second part he says, "No master."

115
00:08:13,800 --> 00:08:36,800
 But then he switches to protect, which is weird. Guard.

116
00:08:36,800 --> 00:08:43,800
 How do we guard ourselves?

117
00:08:43,800 --> 00:08:46,800
 We practice the four foundations of mindfulness,

118
00:08:46,800 --> 00:08:49,800
 and by protecting ourselves we also protect others.

119
00:08:49,800 --> 00:08:51,800
 This is another interesting part of this.

120
00:08:51,800 --> 00:08:56,800
 Not only is it about everyone doing their own duty,

121
00:08:56,800 --> 00:09:08,750
 and being good people, but it addresses the idea that when

122
00:09:08,750 --> 00:09:09,800
 you do that you also help others.

123
00:09:09,800 --> 00:09:15,570
 Why? Because of the leading to patience, forbearance, harm

124
00:09:15,570 --> 00:09:20,800
lessness, love and compassion.

125
00:09:20,800 --> 00:09:34,690
 By practicing mindfulness, by practicing for one's own

126
00:09:34,690 --> 00:09:41,800
 betterment, one protects others.

127
00:09:41,800 --> 00:09:47,740
 This is in contrast to those people who think the way to be

128
00:09:47,740 --> 00:09:48,800
 a good person,

129
00:09:48,800 --> 00:09:52,800
 the way to do the right thing is to help others.

130
00:09:52,800 --> 00:09:55,800
 It's not really.

131
00:09:55,800 --> 00:09:59,290
 There's a person who helps themselves, who's naturally

132
00:09:59,290 --> 00:10:01,800
 inclined to help others.

133
00:10:01,800 --> 00:10:05,800
 It's not what should be the focus, right?

134
00:10:05,800 --> 00:10:09,800
 If your focus is on helping others, you neglect the source.

135
00:10:09,800 --> 00:10:24,680
 If your focus is on helping yourself, then you radiate

136
00:10:24,680 --> 00:10:34,800
 goodness in all things that you do.

137
00:10:34,800 --> 00:10:40,140
 If we don't have any questions, we can just call it a night

138
00:10:40,140 --> 00:10:41,800
 and say hello.

139
00:10:41,800 --> 00:10:49,800
 Thank you all for meditating with us.

140
00:10:49,800 --> 00:10:57,800
 Tomorrow we have meetings and a mursudimaga.

141
00:10:57,800 --> 00:11:06,800
 I may skip some of the meetings.

142
00:11:06,800 --> 00:11:10,220
 Maybe I'll do my studying while you guys are having the

143
00:11:10,220 --> 00:11:15,800
 meeting, and I'll listen.

144
00:11:15,800 --> 00:11:18,800
 I can study and be there at the same time, I think.

145
00:11:18,800 --> 00:11:22,250
 Yeah, we can just kind of call out to you if we need your

146
00:11:22,250 --> 00:11:23,800
 input on something.

147
00:11:23,800 --> 00:11:27,800
 Sure.

148
00:11:27,800 --> 00:11:35,800
 That sounds good.

149
00:11:35,800 --> 00:11:39,400
 Okay, then. Good night, everyone. Thank you, Robin, for

150
00:11:39,400 --> 00:11:39,800
 joining me.

151
00:11:39,800 --> 00:11:41,800
 Thank you, Bante.

152
00:11:41,800 --> 00:11:42,800
 Good night.

153
00:11:43,800 --> 00:11:44,800
 Thank you.

