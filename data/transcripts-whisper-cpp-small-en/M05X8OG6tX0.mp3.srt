1
00:00:00,000 --> 00:00:04,460
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,460 --> 00:00:08,810
 Today we continue on with verse number 91 which reads as

3
00:00:08,810 --> 00:00:10,360
 follows.

4
00:00:10,360 --> 00:00:19,900
 Uyun janti satimanto nani-kei te ramanti te hang sahwa pal

5
00:00:19,900 --> 00:00:23,920
alang hitwa okamo gang jahanti

6
00:00:23,920 --> 00:00:26,920
 te.

7
00:00:26,920 --> 00:00:33,510
 Which means, uyun janti satimanto, the mindful exert

8
00:00:33,510 --> 00:00:38,680
 themselves or the mindful work hard.

9
00:00:38,680 --> 00:00:45,200
 Nani-kei te ramanti te, they don't delight in dwellings or

10
00:00:45,200 --> 00:00:49,720
 they don't delight in dwellings,

11
00:00:49,720 --> 00:00:50,720
 in homes.

12
00:00:50,720 --> 00:00:57,420
 Hang sahwa palalang hitwa, just as a, or like a swan,

13
00:00:57,420 --> 00:01:02,440
 having abandoned the lake, abandoned

14
00:01:02,440 --> 00:01:11,940
 the lake, uyun janti te, they abandon house after house,

15
00:01:11,940 --> 00:01:15,080
 home after home.

16
00:01:15,080 --> 00:01:19,090
 So this story, this verse was told in relation to a story

17
00:01:19,090 --> 00:01:21,480
 that's actually interesting in

18
00:01:21,480 --> 00:01:25,980
 relation to the verse because it actually implies something

19
00:01:25,980 --> 00:01:28,000
 of the opposite, what you

20
00:01:28,000 --> 00:01:29,000
 would think.

21
00:01:29,000 --> 00:01:32,060
 From the sounds of this verse you would think that the

22
00:01:32,060 --> 00:01:34,080
 Buddha was saying that the person

23
00:01:34,080 --> 00:01:41,080
 who is mindful exerts themselves such a person should, you

24
00:01:41,080 --> 00:01:45,040
 would most likely expect to avoid

25
00:01:45,040 --> 00:01:50,120
 having a home and wander.

26
00:01:50,120 --> 00:01:56,060
 So here we have this story, the Buddha was going on

27
00:01:56,060 --> 00:02:00,560
 pilgrimage, wandering Charika.

28
00:02:00,560 --> 00:02:06,290
 The Buddha would go around India to teach, so traveling

29
00:02:06,290 --> 00:02:08,760
 from place to place and living

30
00:02:08,760 --> 00:02:12,640
 and talking with people, debating sometimes with people

31
00:02:12,640 --> 00:02:14,920
 with other ascetics, sometimes

32
00:02:14,920 --> 00:02:18,940
 lay people would come and debate with him or ask him

33
00:02:18,940 --> 00:02:21,680
 difficult questions and he would

34
00:02:21,680 --> 00:02:28,880
 teach and answer and spread his teachings throughout India.

35
00:02:28,880 --> 00:02:31,400
 So he was getting ready to go and all the monks knew that

36
00:02:31,400 --> 00:02:32,840
 the Buddha was getting ready

37
00:02:32,840 --> 00:02:38,350
 to go and so they all prepared, packed up their things,

38
00:02:38,350 --> 00:02:41,400
 they scrubbed out their bowls

39
00:02:41,400 --> 00:02:46,510
 and gathered up their robes and then they saw Mahakasapa

40
00:02:46,510 --> 00:02:53,560
 who was washing his robe and

41
00:02:53,560 --> 00:02:58,270
 they started snickering to themselves or frowning, turning

42
00:02:58,270 --> 00:03:00,880
 up their noses at this because this

43
00:03:00,880 --> 00:03:03,960
 is a clear indication that Mahakasapa wasn't ready to go

44
00:03:03,960 --> 00:03:06,040
 and he wasn't going and they said,

45
00:03:06,040 --> 00:03:07,040
 "Oh I know."

46
00:03:07,040 --> 00:03:09,640
 One of them said, "I know what's going on here."

47
00:03:09,640 --> 00:03:12,790
 Here's a guy who is attached to houses, he's attached to

48
00:03:12,790 --> 00:03:14,880
 his dwelling, I mean who wouldn't

49
00:03:14,880 --> 00:03:16,280
 be?

50
00:03:16,280 --> 00:03:20,250
 He's famous and he's loved by all the lay people and they

51
00:03:20,250 --> 00:03:22,160
 give him the best choices

52
00:03:22,160 --> 00:03:28,760
 to food and robes and everything he wants, he gets here.

53
00:03:28,760 --> 00:03:30,680
 If I got that I wouldn't leave either.

54
00:03:30,680 --> 00:03:33,150
 So no doubt he's staying here because he's attached to it

55
00:03:33,150 --> 00:03:39,280
 and he's comfortable with it.

56
00:03:39,280 --> 00:03:45,890
 And so they gossiped and rather fairly mean-spirited it

57
00:03:45,890 --> 00:03:47,800
 sounds like.

58
00:03:47,800 --> 00:03:51,030
 Maybe not mean-spirited but they certainly misrepresented

59
00:03:51,030 --> 00:03:52,800
 his intention because the Buddha

60
00:03:52,800 --> 00:03:53,800
 heard about it.

61
00:03:53,800 --> 00:03:58,840
 Well what happened then, everyone laughed, all the monks

62
00:03:58,840 --> 00:04:00,520
 laughed and Mahakasapa went

63
00:04:00,520 --> 00:04:03,810
 with them but he got to a certain point and the Buddha

64
00:04:03,810 --> 00:04:05,960
 turned and said to Mahakasapa,

65
00:04:05,960 --> 00:04:10,000
 "Mahakasapa you stay behind."

66
00:04:10,000 --> 00:04:13,920
 And the monks nodded and said to themselves, said to each

67
00:04:13,920 --> 00:04:15,880
 other, "Now there you go, even

68
00:04:15,880 --> 00:04:19,080
 the Buddha knows that Mahakasapa can't leave this place, he

69
00:04:19,080 --> 00:04:20,520
's too attached to it."

70
00:04:20,520 --> 00:04:23,230
 And so when they were sitting down I guess in the evening

71
00:04:23,230 --> 00:04:24,680
 or they were talking amongst

72
00:04:24,680 --> 00:04:26,500
 themselves when they were walking, maybe the Buddha turned

73
00:04:26,500 --> 00:04:27,280
 and said, "What are you guys

74
00:04:27,280 --> 00:04:29,800
 talking about?

75
00:04:29,800 --> 00:04:36,320
 What are you bhikkhus engaged in discussion about?"

76
00:04:36,320 --> 00:04:41,100
 And they told him and he said, "That's ridiculous, that's

77
00:04:41,100 --> 00:04:42,360
 not at all."

78
00:04:42,360 --> 00:04:46,900
 He didn't use those words but he said, "That's not at all

79
00:04:46,900 --> 00:04:49,240
 why Mahakasapa was not ready to

80
00:04:49,240 --> 00:04:54,360
 go and that's not at all why I asked him to turn around."

81
00:04:54,360 --> 00:05:00,480
 Because Mahakasapa made a determination in his past lives

82
00:05:00,480 --> 00:05:03,560
 to be a support for lay people

83
00:05:03,560 --> 00:05:05,600
 and so he gives a long story about when he made this

84
00:05:05,600 --> 00:05:07,160
 determination but for our purposes

85
00:05:07,160 --> 00:05:08,760
 it's not really useful.

86
00:05:08,760 --> 00:05:11,860
 The point is he had this intention to stay and be of

87
00:05:11,860 --> 00:05:13,280
 benefit to people.

88
00:05:13,280 --> 00:05:17,880
 If all the monks left then who would stay to teach and who

89
00:05:17,880 --> 00:05:20,080
 would stay to care for and

90
00:05:20,080 --> 00:05:24,780
 encourage and really support the meditators and the

91
00:05:24,780 --> 00:05:27,480
 practitioners in the area.

92
00:05:27,480 --> 00:05:32,080
 So Mahakasapa knew that he and the Buddha were actually

93
00:05:32,080 --> 00:05:34,640
 very close and when the Buddha

94
00:05:34,640 --> 00:05:39,210
 passed away Mahakasapa was sort of looked upon as the

95
00:05:39,210 --> 00:05:41,640
 leader of sorts, though not to

96
00:05:41,640 --> 00:05:47,800
 take the place of the Buddha but to take over some of the

97
00:05:47,800 --> 00:05:51,880
 role of guidance and direction.

98
00:05:51,880 --> 00:05:57,260
 So when the Buddha left to go on his tour, Mahakasapa knew

99
00:05:57,260 --> 00:05:59,760
 it was best for him to stay

100
00:05:59,760 --> 00:06:04,360
 behind because otherwise there wouldn't be many things left

101
00:06:04,360 --> 00:06:06,480
 undone and many people looking

102
00:06:06,480 --> 00:06:10,420
 to learn about the Dhamma who wouldn't get to hear it and

103
00:06:10,420 --> 00:06:11,080
 so on.

104
00:06:11,080 --> 00:06:16,810
 Then the Buddha said, he told this long story about Mahakas

105
00:06:16,810 --> 00:06:19,520
apa making this wish to be of

106
00:06:19,520 --> 00:06:23,830
 benefit to people and to not go out of his way and then the

107
00:06:23,830 --> 00:06:25,900
 Buddha pointed out using

108
00:06:25,900 --> 00:06:31,570
 this verse, he actually pointed out that it's not that

109
00:06:31,570 --> 00:06:35,360
 people who are mindful, who are enlightened

110
00:06:35,360 --> 00:06:40,370
 and like an arahant, it's not that they are the ones, they

111
00:06:40,370 --> 00:06:42,680
 are not the ones who need to

112
00:06:42,680 --> 00:06:46,980
 leave houses, they need to leave their dwellings, need to

113
00:06:46,980 --> 00:06:51,160
 be careful because they have no attachment.

114
00:06:51,160 --> 00:06:53,890
 The person who has no attachment has no problem staying

115
00:06:53,890 --> 00:06:55,720
 behind, it's the exact person that

116
00:06:55,720 --> 00:07:01,830
 you want to have stay in one place and teach and to spread

117
00:07:01,830 --> 00:07:03,900
 the teachings.

118
00:07:03,900 --> 00:07:05,800
 So it kind of put the other monks in the place because the

119
00:07:05,800 --> 00:07:06,920
 implication is, well you guys

120
00:07:06,920 --> 00:07:11,500
 have to come with me so I can take care of you and so you

121
00:07:11,500 --> 00:07:13,880
 can not be attached to one

122
00:07:13,880 --> 00:07:15,640
 place for too long.

123
00:07:15,640 --> 00:07:19,980
 So in Thailand you get this a lot, monks will wander and

124
00:07:19,980 --> 00:07:22,520
 never spend more than one night

125
00:07:22,520 --> 00:07:24,960
 or never spend more than one week in one place.

126
00:07:24,960 --> 00:07:28,060
 I've talked to monks who spent the whole year outside of

127
00:07:28,060 --> 00:07:30,360
 the rains, they wouldn't spend

128
00:07:30,360 --> 00:07:35,160
 two nights in a row in the same spot, so they would

129
00:07:35,160 --> 00:07:37,760
 constantly be moving.

130
00:07:37,760 --> 00:07:40,600
 It's harder to do now, there are monks who do this.

131
00:07:40,600 --> 00:07:43,950
 I'm not convinced that it's the greatest thing to do but

132
00:07:43,950 --> 00:07:45,680
 there's a sense that when you're

133
00:07:45,680 --> 00:07:52,480
 still attached to dwellings that can be quite useful to

134
00:07:52,480 --> 00:07:55,520
 keep you on your toes.

135
00:07:55,520 --> 00:07:59,390
 So this is our story and the verse which is fairly clear

136
00:07:59,390 --> 00:08:01,640
 but let's just go through it.

137
00:08:01,640 --> 00:08:06,120
 So Sati Mantu Uyun Janti, that the mindful are not lazy is

138
00:08:06,120 --> 00:08:08,440
 basically what's being said

139
00:08:08,440 --> 00:08:11,520
 here because there was an implication that Maa Kasuba was

140
00:08:11,520 --> 00:08:13,440
 lazy, he just couldn't be bothered

141
00:08:13,440 --> 00:08:14,520
 to go on a tour.

142
00:08:14,520 --> 00:08:17,520
 Why bother when everything is so comfortable here?

143
00:08:17,520 --> 00:08:21,400
 The Buddha pointed out that even when mindful people stay

144
00:08:21,400 --> 00:08:23,600
 in one spot, you can't call them

145
00:08:23,600 --> 00:08:26,360
 lazy, they will never get lazy because they're mindful.

146
00:08:26,360 --> 00:08:29,310
 A person who stays put, even a person who does nothing but

147
00:08:29,310 --> 00:08:31,080
 walk and sit all day, shouldn't

148
00:08:31,080 --> 00:08:32,800
 be considered lazy.

149
00:08:32,800 --> 00:08:37,250
 Their mindfulness keeps them alert, keeps them energetic,

150
00:08:37,250 --> 00:08:39,120
 keeps them from being lazy.

151
00:08:39,120 --> 00:08:41,850
 So it's not necessarily the case that just because you stay

152
00:08:41,850 --> 00:08:43,120
 in one place, you're not

153
00:08:43,120 --> 00:08:45,360
 going to be energetic.

154
00:08:45,360 --> 00:08:50,500
 The mindful are always energetic, never lazy.

155
00:08:50,500 --> 00:08:53,520
 So those of you who feel like you're lazy out there, use

156
00:08:53,520 --> 00:08:54,120
 mindfulness.

157
00:08:54,120 --> 00:08:57,600
 You don't have to go running or you don't have to exert

158
00:08:57,600 --> 00:08:59,720
 yourself, just be mindful.

159
00:08:59,720 --> 00:09:02,760
 When you're mindful, you can be sitting still all day and

160
00:09:02,760 --> 00:09:04,320
 still be considered energetic.

161
00:09:04,320 --> 00:09:08,880
 Your mind will be alert and you won't fall into the trap of

162
00:09:08,880 --> 00:09:10,160
 being lazy.

163
00:09:10,160 --> 00:09:15,600
 Nanikeite Ramantite, they don't have love for dwellings.

164
00:09:15,600 --> 00:09:25,240
 So it's not that they leave them behind, that they have to

165
00:09:25,240 --> 00:09:26,960
 escape them, but that they have

166
00:09:26,960 --> 00:09:30,770
 no love for them and they will leave and they're happy to

167
00:09:30,770 --> 00:09:32,640
 go when it's time to go.

168
00:09:32,640 --> 00:09:36,120
 The second part says like a swan leaves behind a lake.

169
00:09:36,120 --> 00:09:37,720
 The swan doesn't worry about the lake.

170
00:09:37,720 --> 00:09:40,220
 The swan is not concerned with the lake or say, "Oh, that

171
00:09:40,220 --> 00:09:41,160
 was a nice place.

172
00:09:41,160 --> 00:09:42,560
 Maybe I should stay."

173
00:09:42,560 --> 00:09:44,760
 Swans when they leave, they leave.

174
00:09:44,760 --> 00:09:48,880
 I mean, swans are just dumb animals, but in the same vein,

175
00:09:48,880 --> 00:09:50,880
 the swan not worrying about

176
00:09:50,880 --> 00:09:54,520
 the lake or not loving the lake or falling in love with the

177
00:09:54,520 --> 00:09:56,120
 lake or being attached to

178
00:09:56,120 --> 00:09:58,960
 it, the swan goes and knows when it's time to go.

179
00:09:58,960 --> 00:10:04,390
 In the same way, wise people or those who are mindful, oka

180
00:10:04,390 --> 00:10:06,800
 mo gang jahan titae, they

181
00:10:06,800 --> 00:10:10,760
 leave behind or they abandon house after house.

182
00:10:10,760 --> 00:10:15,040
 So basically saying Mahakasapa has no problem leaving.

183
00:10:15,040 --> 00:10:16,760
 He certainly has not attached to it.

184
00:10:16,760 --> 00:10:20,220
 I mean, Mahakasapa was one of the greatest of the Buddhist

185
00:10:20,220 --> 00:10:21,400
 disciples.

186
00:10:21,400 --> 00:10:25,950
 There's some traditions that say he's still alive waiting

187
00:10:25,950 --> 00:10:28,080
 to give his robe to the next

188
00:10:28,080 --> 00:10:31,520
 Buddha.

189
00:10:31,520 --> 00:10:35,550
 He entombed himself in the mountain and went into a trance

190
00:10:35,550 --> 00:10:37,560
 and he's going to stay there

191
00:10:37,560 --> 00:10:42,360
 for whatever thousands or hundreds of thousands of years

192
00:10:42,360 --> 00:10:44,320
 before medea comes.

193
00:10:44,320 --> 00:10:46,300
 And when medea comes, he's going to come out and give him

194
00:10:46,300 --> 00:10:46,800
 his robe.

195
00:10:46,800 --> 00:10:50,050
 I think it's a Mahayana tradition that says, or it's maybe

196
00:10:50,050 --> 00:10:53,680
 just an Indian folklore or something,

197
00:10:53,680 --> 00:10:59,480
 but he's highly revered.

198
00:10:59,480 --> 00:11:03,120
 He was apparently the tradition is I think that he aspired

199
00:11:03,120 --> 00:11:04,840
 once to be a Buddha, but when

200
00:11:04,840 --> 00:11:08,710
 he met our Buddha, he realized how difficult it would be

201
00:11:08,710 --> 00:11:10,960
 and how much better off he would

202
00:11:10,960 --> 00:11:13,920
 be to learn from this Buddha.

203
00:11:13,920 --> 00:11:16,400
 So he put aside his intention.

204
00:11:16,400 --> 00:11:17,920
 That was Mahakachayan, I think.

205
00:11:17,920 --> 00:11:18,920
 That's a different monk.

206
00:11:18,920 --> 00:11:20,520
 I'm confusing them.

207
00:11:20,520 --> 00:11:24,040
 But no, Mahakasapa was the head of the Sangha after the

208
00:11:24,040 --> 00:11:26,260
 Buddha passed away or the father

209
00:11:26,260 --> 00:11:30,000
 of the Sangha is often called after the Buddha passed away.

210
00:11:30,000 --> 00:11:35,130
 It's not exactly accurate, but that's sort of how he's

211
00:11:35,130 --> 00:11:36,680
 looked upon.

212
00:11:36,680 --> 00:11:41,850
 So he was a great figure in the Sangha and certainly had no

213
00:11:41,850 --> 00:11:43,440
 attachments.

214
00:11:43,440 --> 00:11:45,320
 But there's a couple of lessons we can learn here.

215
00:11:45,320 --> 00:11:49,040
 The first is not to criticize people.

216
00:11:49,040 --> 00:11:51,560
 Really not at all.

217
00:11:51,560 --> 00:11:53,920
 Criticizing others rarely does any good.

218
00:11:53,920 --> 00:12:02,070
 It usually just feeds your ego and helps us stage any insec

219
00:12:02,070 --> 00:12:04,680
urities we have.

220
00:12:04,680 --> 00:12:06,920
 You feel better bringing other people down a notch.

221
00:12:06,920 --> 00:12:11,320
 Not good.

222
00:12:11,320 --> 00:12:13,760
 And it's often misguided.

223
00:12:13,760 --> 00:12:16,950
 People do things for reasons that if we thought about it,

224
00:12:16,950 --> 00:12:18,840
 we might be able to empathize and

225
00:12:18,840 --> 00:12:20,680
 understand why they're doing things.

226
00:12:20,680 --> 00:12:23,520
 The person who you're very angry at because of the evil

227
00:12:23,520 --> 00:12:25,120
 things they've done well, who

228
00:12:25,120 --> 00:12:26,480
 knows why they're doing them?

229
00:12:26,480 --> 00:12:29,390
 Probably they have evil inside of them, but they may be

230
00:12:29,390 --> 00:12:31,080
 doing it because they feel hurt

231
00:12:31,080 --> 00:12:34,660
 or because they're in a position where they're not able to

232
00:12:34,660 --> 00:12:36,640
 understand what's wrong with what

233
00:12:36,640 --> 00:12:39,840
 they're doing or this kind of thing.

234
00:12:39,840 --> 00:12:42,200
 And often they're doing it blamelessly.

235
00:12:42,200 --> 00:12:46,340
 We get to constantly, adjanthong, people would criticize

236
00:12:46,340 --> 00:12:50,640
 him about the silliest of things.

237
00:12:50,640 --> 00:12:53,860
 And there was one monk who came and saw that adjanthong had

238
00:12:53,860 --> 00:12:55,880
 all this wonderful food because

239
00:12:55,880 --> 00:12:58,000
 people adore him.

240
00:12:58,000 --> 00:13:02,000
 So the food that they would give him is extra special food.

241
00:13:02,000 --> 00:13:05,900
 And there was this one western guy who ordained as a monk

242
00:13:05,900 --> 00:13:07,360
 for a short time.

243
00:13:07,360 --> 00:13:09,080
 He just was disgusted by it.

244
00:13:09,080 --> 00:13:12,510
 He said, "What's he doing eating all that good food when we

245
00:13:12,510 --> 00:13:14,000
 hear we have to..."

246
00:13:14,000 --> 00:13:17,080
 I think it came from the fact that he wanted some of the

247
00:13:17,080 --> 00:13:19,040
 good food, but he just felt it

248
00:13:19,040 --> 00:13:23,600
 was wrong and unfair.

249
00:13:23,600 --> 00:13:29,710
 People criticize things like there's this story of this Zen

250
00:13:29,710 --> 00:13:31,840
 story of these western men,

251
00:13:31,840 --> 00:13:34,400
 I guess who became monks.

252
00:13:34,400 --> 00:13:38,040
 And they went to live with this Japanese monk, I think

253
00:13:38,040 --> 00:13:40,480
 Japanese monk, Chinese monk maybe,

254
00:13:40,480 --> 00:13:42,160
 and probably Chinese.

255
00:13:42,160 --> 00:13:45,280
 And they were in the monastery and every day the...

256
00:13:45,280 --> 00:13:48,700
 Maybe it was just one man, anyway, in the monastery, every

257
00:13:48,700 --> 00:13:50,560
 day the monk would bow down to this

258
00:13:50,560 --> 00:13:52,720
 Buddha statue.

259
00:13:52,720 --> 00:13:55,390
 And so the guy started doing it and then every day he saw,

260
00:13:55,390 --> 00:13:56,800
 every day we had to do bowing

261
00:13:56,800 --> 00:13:59,640
 and bowing and bowing to this Buddha statue.

262
00:13:59,640 --> 00:14:02,850
 And finally the guy got upset about it and he turns to him

263
00:14:02,850 --> 00:14:04,480
 and he said, "Look, this

264
00:14:04,480 --> 00:14:05,480
 is ridiculous.

265
00:14:05,480 --> 00:14:07,640
 I don't want to bow down to this dumb statue."

266
00:14:07,640 --> 00:14:13,300
 He said, "I'd rather spit on that image than bow down to it

267
00:14:13,300 --> 00:14:13,440
."

268
00:14:13,440 --> 00:14:20,280
 And the Chinese monk turns to him and says, "Okay, you spit

269
00:14:20,280 --> 00:14:22,160
, I'll bow."

270
00:14:22,160 --> 00:14:26,100
 We sometimes we hold on to things quite strongly and we

271
00:14:26,100 --> 00:14:28,760
 miss the fact that it's not wrong with

272
00:14:28,760 --> 00:14:30,720
 the other person, what other people are doing.

273
00:14:30,720 --> 00:14:33,510
 They have reasons for it and we have to ask ourselves, this

274
00:14:33,510 --> 00:14:34,720
 is what I learned a lot in

275
00:14:34,720 --> 00:14:38,080
 Thailand because I was that sort of way in Thailand

276
00:14:38,080 --> 00:14:40,920
 criticizing this, criticizing that.

277
00:14:40,920 --> 00:14:44,220
 But then I watched and I saw especially Ajahn Tong, how unc

278
00:14:44,220 --> 00:14:46,080
ritical he is and how accepting

279
00:14:46,080 --> 00:14:50,880
 and how he just doesn't even comment on so many things.

280
00:14:50,880 --> 00:14:54,840
 And then when he does comment, he finds the good in it.

281
00:14:54,840 --> 00:14:59,700
 He finds a way to focus people's attention on what's good

282
00:14:59,700 --> 00:15:02,360
 about it and to point out that

283
00:15:02,360 --> 00:15:04,700
 sure he'll say sometimes like, "You know, some people say

284
00:15:04,700 --> 00:15:05,840
 this is just a ritual, but

285
00:15:05,840 --> 00:15:07,880
 you know, it makes people feel good.

286
00:15:07,880 --> 00:15:08,880
 It makes people mindful.

287
00:15:08,880 --> 00:15:13,320
 It makes people think about the Buddha, etc., etc."

288
00:15:13,320 --> 00:15:15,720
 And that really opened my eyes to, "Yeah, that's it.

289
00:15:15,720 --> 00:15:18,120
 I mean, we're the ones with the unwholesome mind.

290
00:15:18,120 --> 00:15:21,350
 This guy who was obsessed with spitting, well, he was the

291
00:15:21,350 --> 00:15:23,200
 one obsessed with the bowing.

292
00:15:23,200 --> 00:15:29,920
 He was the one that got upset about it."

293
00:15:29,920 --> 00:15:32,890
 That's the first thing we can learn is to not criticize

294
00:15:32,890 --> 00:15:34,600
 others without knowing their

295
00:15:34,600 --> 00:15:35,600
 reasons.

296
00:15:35,600 --> 00:15:39,210
 Be careful. We should always worry more about our own

297
00:15:39,210 --> 00:15:39,280
 faults.

298
00:15:39,280 --> 00:15:43,280
 "Napare stang willo mani, napare stang katagatang."

299
00:15:43,280 --> 00:15:49,980
 We shouldn't worry about the faults of others or the things

300
00:15:49,980 --> 00:15:53,640
 they've done or left undone.

301
00:15:53,640 --> 00:15:57,030
 The second thing is this point that I've sort of already

302
00:15:57,030 --> 00:15:59,080
 made that, but it's a two-pronged

303
00:15:59,080 --> 00:16:00,080
 point.

304
00:16:00,080 --> 00:16:08,430
 The idea that people who are truly mindful don't need to do

305
00:16:08,430 --> 00:16:12,160
 any special practice.

306
00:16:12,160 --> 00:16:13,160
 And that's very much true.

307
00:16:13,160 --> 00:16:17,010
 You know, a mindful person, there's this Zen thing about

308
00:16:17,010 --> 00:16:19,080
 the Buddha riding in a Ferrari

309
00:16:19,080 --> 00:16:24,670
 and wearing gold rings and so on because he wasn't attached

310
00:16:24,670 --> 00:16:25,640
 to it.

311
00:16:25,640 --> 00:16:31,020
 And so this kind of thing, it's possible to go way too far

312
00:16:31,020 --> 00:16:33,640
 with this idea because you

313
00:16:33,640 --> 00:16:35,840
 say, "Well, okay, so a mindful person doesn't have to do

314
00:16:35,840 --> 00:16:36,880
 any special practice.

315
00:16:36,880 --> 00:16:38,600
 A mindful person doesn't attach to things."

316
00:16:38,600 --> 00:16:41,360
 Well, then they could have sex and they could drink alcohol

317
00:16:41,360 --> 00:16:42,640
 and they could do drugs and

318
00:16:42,640 --> 00:16:43,640
 all these sorts of things.

319
00:16:43,640 --> 00:16:46,320
 It wouldn't hurt them.

320
00:16:46,320 --> 00:16:49,700
 But the point is that a mindful person wouldn't do these

321
00:16:49,700 --> 00:16:50,440
 things.

322
00:16:50,440 --> 00:16:52,720
 I mean, it's not to say that it couldn't happen.

323
00:16:52,720 --> 00:16:56,260
 You can be raped or that kind of thing, and you can drink

324
00:16:56,260 --> 00:16:58,240
 something thinking it's not

325
00:16:58,240 --> 00:17:03,520
 alcohol and wind up being alcohol or drugs or so on.

326
00:17:03,520 --> 00:17:10,640
 I was once given these pills by this woman.

327
00:17:10,640 --> 00:17:13,400
 I don't know what her deal was, but I don't really think

328
00:17:13,400 --> 00:17:15,080
 she had bad intentions, but she

329
00:17:15,080 --> 00:17:17,400
 gave me these pills saying it was like jin singh.

330
00:17:17,400 --> 00:17:22,040
 I thought, "Well, jin singh, that's good for your mind."

331
00:17:22,040 --> 00:17:25,330
 But when I looked it up on the internet, I took the pill

332
00:17:25,330 --> 00:17:26,640
 and I swallowed it.

333
00:17:26,640 --> 00:17:29,320
 She gave me this bottle and she said, "That'd be good for

334
00:17:29,320 --> 00:17:30,280
 you as a monk."

335
00:17:30,280 --> 00:17:34,760
 So I took the pill, I swallowed it, and as it was settling

336
00:17:34,760 --> 00:17:36,920
 in my stomach, I looked on

337
00:17:36,920 --> 00:17:41,520
 the internet to see what its characteristics were, and it

338
00:17:41,520 --> 00:17:43,040
 was an aphrodisiac.

339
00:17:43,040 --> 00:17:47,040
 I'm like, "Oh, well, there it is.

340
00:17:47,040 --> 00:17:53,760
 I guess that's the only one I'll be eating of that."

341
00:17:53,760 --> 00:17:57,990
 It's true that if you don't have the desires, then it

342
00:17:57,990 --> 00:18:00,040
 shouldn't affect you.

343
00:18:00,040 --> 00:18:04,480
 It shouldn't be a problem.

344
00:18:04,480 --> 00:18:07,300
 But people use it as an excuse to do things that they want

345
00:18:07,300 --> 00:18:08,800
 to do and claim that they're

346
00:18:08,800 --> 00:18:09,800
 not attached.

347
00:18:09,800 --> 00:18:13,640
 "Yeah, I enjoy it, but I'm not attached to it."

348
00:18:13,640 --> 00:18:17,870
 It's a huge exercise in delusion, in self-delusion, that

349
00:18:17,870 --> 00:18:20,560
 somehow you can be angry and not cling

350
00:18:20,560 --> 00:18:26,760
 to it or be greedy and not cling to it.

351
00:18:26,760 --> 00:18:29,880
 It's certainly not the case.

352
00:18:29,880 --> 00:18:33,510
 Anything that's mental, any of your mental judgments, that

353
00:18:33,510 --> 00:18:35,200
's where the problem lies.

354
00:18:35,200 --> 00:18:40,560
 Anything that makes you afraid or worried or bored or sad.

355
00:18:40,560 --> 00:18:44,720
 They are greed or anger in its very base forms.

356
00:18:44,720 --> 00:18:51,000
 They are in and of themselves the problem.

357
00:18:51,000 --> 00:18:54,840
 You shouldn't use this as an excuse to stay put.

358
00:18:54,840 --> 00:18:59,870
 In fact, often Mahakasava may have stayed where he was, but

359
00:18:59,870 --> 00:19:02,060
 he in the end went and lived

360
00:19:02,060 --> 00:19:08,080
 up on the hill, on the mountain above Rajagaha.

361
00:19:08,080 --> 00:19:10,360
 I think after the Buddha passed away, that's where he was

362
00:19:10,360 --> 00:19:11,520
 staying, because that's where

363
00:19:11,520 --> 00:19:15,600
 they did the first council.

364
00:19:15,600 --> 00:19:17,000
 They invited him down.

365
00:19:17,000 --> 00:19:19,250
 They wanted him to come and stay in the city because he was

366
00:19:19,250 --> 00:19:20,480
 the big monk and he was getting

367
00:19:20,480 --> 00:19:25,440
 old and they said, "We need you down here to support us."

368
00:19:25,440 --> 00:19:26,440
 He refused.

369
00:19:26,440 --> 00:19:28,560
 Every day he would go back up, walk all the way up.

370
00:19:28,560 --> 00:19:30,600
 It's quite a walk.

371
00:19:30,600 --> 00:19:33,480
 Every day he would walk up and live up on the mountain.

372
00:19:33,480 --> 00:19:37,800
 Apparently, he never laid down either.

373
00:19:37,800 --> 00:19:42,180
 He would always be walking and sitting, never lying down

374
00:19:42,180 --> 00:19:44,720
 throughout his life or his later

375
00:19:44,720 --> 00:19:54,600
 years anyway, but I think throughout his monastic life.

376
00:19:54,600 --> 00:19:58,200
 It's not like he was just living in the lap of luxury and

377
00:19:58,200 --> 00:19:59,680
 not clinging to it.

378
00:19:59,680 --> 00:20:02,320
 They asked him why and he said it was out of compassion for

379
00:20:02,320 --> 00:20:03,520
 others so that they were

380
00:20:03,520 --> 00:20:07,990
 able to see the good example and because it's much more

381
00:20:07,990 --> 00:20:09,120
 peaceful.

382
00:20:09,120 --> 00:20:12,450
 It's just the right thing to do, living in the forest,

383
00:20:12,450 --> 00:20:14,680
 living alone, living at peace.

384
00:20:14,680 --> 00:20:17,970
 There's no sense of that, but one should not be attached to

385
00:20:17,970 --> 00:20:19,600
 these things nor criticize

386
00:20:19,600 --> 00:20:20,600
 someone.

387
00:20:20,600 --> 00:20:21,600
 I mean, a lot of criticism.

388
00:20:21,600 --> 00:20:24,120
 There was this big criticism that went around recently

389
00:20:24,120 --> 00:20:25,800
 about these monks who had their own

390
00:20:25,800 --> 00:20:30,760
 private jet and wore sunglasses.

391
00:20:30,760 --> 00:20:33,690
 At first I understood that was the criticism and it didn't

392
00:20:33,690 --> 00:20:35,000
 really faze me at all.

393
00:20:35,000 --> 00:20:38,740
 I'm like, "Okay, well, it's possible that the monastery

394
00:20:38,740 --> 00:20:40,400
 could have a jet for us.

395
00:20:40,400 --> 00:20:41,640
 It's a bit outrageous."

396
00:20:41,640 --> 00:20:45,330
 But my point being, those things in and of themselves don't

397
00:20:45,330 --> 00:20:47,400
 necessarily, because they're

398
00:20:47,400 --> 00:20:48,400
 tools.

399
00:20:48,400 --> 00:20:50,440
 Even sunglasses, lots of monks wear sunglasses.

400
00:20:50,440 --> 00:20:52,200
 Ajahn Tong wears sunglasses.

401
00:20:52,200 --> 00:20:53,200
 Why?

402
00:20:53,200 --> 00:20:58,720
 Because he had eye surgery and the light has been or was,

403
00:20:58,720 --> 00:21:01,520
 his eyes were very sensitive

404
00:21:01,520 --> 00:21:02,520
 to sunlight.

405
00:21:02,520 --> 00:21:08,960
 I know lots of monks in Thailand who wear sunglasses.

406
00:21:08,960 --> 00:21:12,980
 I don't really think it's that great of an idea, but it's

407
00:21:12,980 --> 00:21:14,880
 not in and of itself a sign

408
00:21:14,880 --> 00:21:15,880
 of huge corruption.

409
00:21:15,880 --> 00:21:19,320
 I mean, there are far worse things that can and probably do

410
00:21:19,320 --> 00:21:20,960
 go on that we're not really

411
00:21:20,960 --> 00:21:27,830
 aware of, but you'd be careful not to judge things by

412
00:21:27,830 --> 00:21:30,880
 appearances or etc.

413
00:21:30,880 --> 00:21:33,650
 I think it turns out that those monks were selling drugs or

414
00:21:33,650 --> 00:21:35,000
 doing drugs or this or that

415
00:21:35,000 --> 00:21:36,280
 or had women or this or that.

416
00:21:36,280 --> 00:21:43,000
 So yeah, that's something you can definitely criticize.

417
00:21:43,000 --> 00:21:46,360
 But it's an interesting quote.

418
00:21:46,360 --> 00:21:52,290
 I mean, it's on the face, it's a sign of the importance of

419
00:21:52,290 --> 00:21:55,240
 not clinging to our place,

420
00:21:55,240 --> 00:22:00,440
 our possessions, not clinging to be mindful.

421
00:22:00,440 --> 00:22:03,590
 But deeper down, and if you know the backstory, it has more

422
00:22:03,590 --> 00:22:05,360
 to do with the idea that you don't

423
00:22:05,360 --> 00:22:08,960
 need to be too concerned about going off into the forest.

424
00:22:08,960 --> 00:22:11,740
 "Oh, how can I follow the Buddha's teaching living in the

425
00:22:11,740 --> 00:22:12,280
 home life?"

426
00:22:12,280 --> 00:22:13,280
 Yeah, it's more difficult.

427
00:22:13,280 --> 00:22:17,490
 Yeah, you're going to be tested and tried, but it can be

428
00:22:17,490 --> 00:22:18,160
 done.

429
00:22:18,160 --> 00:22:20,080
 And we shouldn't be clear about two things.

430
00:22:20,080 --> 00:22:22,790
 One that it can be done, and two that unless we're really,

431
00:22:22,790 --> 00:22:24,520
 really mindful, it's much easier

432
00:22:24,520 --> 00:22:25,760
 to get attached.

433
00:22:25,760 --> 00:22:28,910
 So you have to be more on your regard, more vigilant when

434
00:22:28,910 --> 00:22:30,840
 you're living in lay society,

435
00:22:30,840 --> 00:22:34,240
 when you're living in a house.

436
00:22:34,240 --> 00:22:35,400
 Much easier to be mindful.

437
00:22:35,400 --> 00:22:37,160
 It's actually kind of cheating.

438
00:22:37,160 --> 00:22:38,160
 It's easier.

439
00:22:38,160 --> 00:22:41,470
 It's an easier way to go off in the forest where we think,

440
00:22:41,470 --> 00:22:43,240
 "Oh, going off in the forest

441
00:22:43,240 --> 00:22:44,240
 that's difficult."

442
00:22:44,240 --> 00:22:45,240
 It's not really difficult.

443
00:22:45,240 --> 00:22:47,480
 Living in the forest, there's nothing.

444
00:22:47,480 --> 00:22:50,840
 It's just simple and easy, and it's wonderful.

445
00:22:50,840 --> 00:22:55,320
 Anyway, so that's the Dhammapada for tonight.

446
00:22:55,320 --> 00:22:56,760
 Thank you all for tuning in.

447
00:22:56,760 --> 00:23:00,880
 Wishing you all good practice, and that you all become free

448
00:23:00,880 --> 00:23:02,240
 from suffering.

