1
00:00:00,000 --> 00:00:04,000
 Okay, good evening everyone.

2
00:00:04,000 --> 00:00:08,000
 Again, still trying to get the audio

3
00:00:08,000 --> 00:00:12,000
 the proper volume. Hopefully it's better tonight.

4
00:00:12,000 --> 00:00:16,000
 Let me know if it's not.

5
00:00:26,000 --> 00:00:30,000
 So tonight,

6
00:00:30,000 --> 00:00:38,000
 the first set of dhammas

7
00:00:38,000 --> 00:00:42,000
 in the Satipatthana Sutta,

8
00:00:42,000 --> 00:00:46,000
 when we get to the section on dhammas,

9
00:00:46,000 --> 00:00:50,000
 the first set is the hindrances.

10
00:00:54,000 --> 00:00:58,000
 And you've got to think there's a reason for that.

11
00:00:58,000 --> 00:01:03,360
 Before all of their dhammas, for someone who's practicing

12
00:01:03,360 --> 00:01:04,000
 Satipatthana,

13
00:01:04,000 --> 00:01:08,000
 practicing mindfulness,

14
00:01:08,000 --> 00:01:14,000
 the first thing outside of the body,

15
00:01:14,000 --> 00:01:18,000
 feelings, the mind, is the hindrances.

16
00:01:18,000 --> 00:01:22,000
 The things that will get in your way.

17
00:01:22,000 --> 00:01:26,000
 Certainly whatever the reason, the actual reason for that

18
00:01:26,000 --> 00:01:26,000
 is,

19
00:01:26,000 --> 00:01:30,000
 it certainly is useful to give them to meditators

20
00:01:30,000 --> 00:01:33,160
 in the beginning because that's when of course they're the

21
00:01:33,160 --> 00:01:34,000
 strongest.

22
00:01:34,000 --> 00:01:42,000
 Our ordinary relationship with our emotions

23
00:01:42,000 --> 00:01:46,000
 is problematic.

24
00:01:46,000 --> 00:01:50,000
 It's backwards really.

25
00:01:52,000 --> 00:01:56,000
 When we like something or we want something,

26
00:01:56,000 --> 00:02:00,000
 we take it as evidence

27
00:02:00,000 --> 00:02:04,000
 or an indication

28
00:02:04,000 --> 00:02:08,000
 that we need to get something, that we should

29
00:02:08,000 --> 00:02:12,000
 obtain the object of our desire.

30
00:02:16,000 --> 00:02:20,000
 When we dislike something

31
00:02:20,000 --> 00:02:24,000
 or are angry at something, we take it as an indication

32
00:02:24,000 --> 00:02:28,000
 that we should do whatever we can to get rid of it.

33
00:02:44,000 --> 00:02:48,000
 When we are tired, we take it as an indication that we

34
00:02:48,000 --> 00:02:48,000
 should sleep.

35
00:02:48,000 --> 00:02:56,000
 When we're distracted or restless, we take it as a sign

36
00:02:56,000 --> 00:02:56,000
 that we should get up and do something.

37
00:02:56,000 --> 00:03:02,000
 Not even intellectually,

38
00:03:02,000 --> 00:03:06,000
 we react, our whole being is conditioned

39
00:03:06,000 --> 00:03:10,000
 to react in a specific way.

40
00:03:10,000 --> 00:03:14,000
 React in this way.

41
00:03:14,000 --> 00:03:18,000
 When we are confused, when we have doubt about something

42
00:03:18,000 --> 00:03:22,000
 we have to take it as an indication that we should stop.

43
00:03:22,000 --> 00:03:26,000
 Stop what we're doing.

44
00:03:26,000 --> 00:03:30,000
 Reject it.

45
00:03:30,000 --> 00:03:34,000
 We live our lives trying to fit the world

46
00:03:34,000 --> 00:03:38,000
 around our expectations, around our personality.

47
00:03:38,000 --> 00:03:42,000
 How many times have we made decisions based on our

48
00:03:42,000 --> 00:03:42,000
 personality?

49
00:03:42,000 --> 00:03:46,000
 Based on our likes and dislikes?

50
00:03:46,000 --> 00:03:50,000
 We think that's natural, right?

51
00:03:50,000 --> 00:03:54,000
 If you don't like something, why would you seek it out?

52
00:03:54,000 --> 00:03:58,000
 If you like something, of course.

53
00:03:58,000 --> 00:04:02,000
 That's where you should focus your efforts.

54
00:04:02,000 --> 00:04:06,000
 Unfortunately,

55
00:04:06,000 --> 00:04:10,000
 the results of this are not as positive as we might think.

56
00:04:10,000 --> 00:04:14,000
 When you chase after what you want,

57
00:04:14,000 --> 00:04:18,000
 what you like,

58
00:04:18,000 --> 00:04:22,000
 you find yourself wound tighter and tighter,

59
00:04:22,000 --> 00:04:26,000
 bound tighter and tighter to that,

60
00:04:26,000 --> 00:04:30,000
 are those things that you like.

61
00:04:30,000 --> 00:04:34,000
 And your happiness becomes more and more dependent on them.

62
00:04:34,000 --> 00:04:38,000
 When we're angry, averse to things, we become vulnerable to

63
00:04:38,000 --> 00:04:38,000
 them.

64
00:04:38,000 --> 00:04:42,000
 More and more averse, more and more fragile,

65
00:04:42,000 --> 00:04:46,000
 or vulnerable

66
00:04:46,000 --> 00:04:50,000
 to their arising.

67
00:04:50,000 --> 00:04:54,000
 More and more susceptible to anger, disappointment,

68
00:04:54,000 --> 00:04:58,000
 displeasure when they arise, or when they can't be removed.

69
00:04:58,000 --> 00:05:02,000
 When we're

70
00:05:02,000 --> 00:05:06,000
 tired, if we just sleep whenever we're tired,

71
00:05:06,000 --> 00:05:09,160
 contrary to popular opinion, that doesn't actually give you

72
00:05:09,160 --> 00:05:10,000
 more energy.

73
00:05:10,000 --> 00:05:14,000
 A real problem with this for meditators who come to

74
00:05:14,000 --> 00:05:14,000
 practice,

75
00:05:14,000 --> 00:05:18,000
 because psychologically they have this

76
00:05:18,000 --> 00:05:22,000
 idea that if you're tired, you should sleep.

77
00:05:22,000 --> 00:05:26,000
 And that somehow makes you less of a tired person.

78
00:05:26,000 --> 00:05:30,000
 And in fact, it's somewhat the opposite.

79
00:05:30,000 --> 00:05:34,000
 The more quick you are to give in to sleep, the more tired

80
00:05:34,000 --> 00:05:34,000
 you

81
00:05:34,000 --> 00:05:38,000
 become as an individual.

82
00:05:38,000 --> 00:05:42,000
 To some extent, of course,

83
00:05:42,000 --> 00:05:46,000
 depending on your workload, your body does need sleep.

84
00:05:46,000 --> 00:05:50,000
 But not to get tired, and if you're very, very

85
00:05:50,000 --> 00:05:54,000
 mindful, and you're far less

86
00:05:54,000 --> 00:05:58,000
 susceptible to drowsiness

87
00:05:58,000 --> 00:06:02,000
 than most people.

88
00:06:02,000 --> 00:06:06,000
 If you're restless

89
00:06:06,000 --> 00:06:10,000
 and you just get up and go, you become restless, you become

90
00:06:10,000 --> 00:06:14,000
 distracted as an individual, reacting,

91
00:06:14,000 --> 00:06:18,000
 reacting to our emotions, trying to fit the world around

92
00:06:18,000 --> 00:06:18,000
 our emotions

93
00:06:18,000 --> 00:06:22,000
 rather than learn to have our emotions fit the world.

94
00:06:26,000 --> 00:06:30,000
 And to see how our emotions

95
00:06:30,000 --> 00:06:33,420
 are really the biggest reason for us not being able to fit

96
00:06:33,420 --> 00:06:34,000
 into the world properly

97
00:06:34,000 --> 00:06:38,000
 or without stress or suffering.

98
00:06:38,000 --> 00:06:42,000
 When we have doubt or confusion

99
00:06:42,000 --> 00:06:45,130
 for always doubting, then we just become someone who doubts

100
00:06:45,130 --> 00:06:46,000
 everything. We can never

101
00:06:46,000 --> 00:06:50,000
 accomplish anything of any great significance, because

102
00:06:50,000 --> 00:06:54,000
 the habit of doubt comes up. And unless we're able to

103
00:06:54,000 --> 00:06:58,000
 see quick results,

104
00:06:58,000 --> 00:07:02,000
 well, even if we are, in fact, we find ourselves

105
00:07:02,000 --> 00:07:06,000
 doubting even those things that are right in front of us

106
00:07:06,000 --> 00:07:06,000
 clearly

107
00:07:06,000 --> 00:07:10,000
 observable.

108
00:07:10,000 --> 00:07:13,380
 This happens for meditators. They'll come, they'll get good

109
00:07:13,380 --> 00:07:14,000
 results, and then they'll start doubting.

110
00:07:14,000 --> 00:07:17,340
 One day they'll have great results, and be sure of the

111
00:07:17,340 --> 00:07:18,000
 practice. Next day

112
00:07:18,000 --> 00:07:21,650
 they forget all about those results, and the doubt comes

113
00:07:21,650 --> 00:07:22,000
 back.

114
00:07:22,000 --> 00:07:26,000
 And the doubt overrides the good results.

115
00:07:26,000 --> 00:07:38,000
 So we look at these differently. Most of our emotions,

116
00:07:38,000 --> 00:07:42,000
 the ones mentioned anyway, are hindrances.

117
00:07:42,000 --> 00:07:48,000
 The point being, there are positive emotions, there are

118
00:07:48,000 --> 00:07:48,000
 good emotions.

119
00:07:48,000 --> 00:07:52,000
 But there are also bad emotions.

120
00:07:52,000 --> 00:07:56,000
 And we don't take it that our emotions

121
00:07:56,000 --> 00:08:00,000
 are, or should be,

122
00:08:00,000 --> 00:08:04,000
 or are the proper impetus for action,

123
00:08:04,000 --> 00:08:08,000
 for speech, even for thought.

124
00:08:08,000 --> 00:08:14,000
 We rather wish to cultivate emotions that are

125
00:08:14,000 --> 00:08:18,000
 conducive to

126
00:08:18,000 --> 00:08:22,000
 wholesome speech, and deeds, thought.

127
00:08:22,000 --> 00:08:28,000
 And so meditation, as with all other things,

128
00:08:28,000 --> 00:08:32,000
 it's the act of retrospection or

129
00:08:32,000 --> 00:08:36,000
 reflection, looking back on yourself.

130
00:08:36,000 --> 00:08:40,000
 Rather than allowing

131
00:08:40,000 --> 00:08:44,000
 the emotions to guide us, we guide ourselves back to the

132
00:08:44,000 --> 00:08:44,000
 emotions,

133
00:08:44,000 --> 00:08:48,000
 and we learn about them, we study them.

134
00:08:48,000 --> 00:08:52,000
 Because there's another way that people deal with emotions,

135
00:08:52,000 --> 00:08:52,000
 they reject them.

136
00:08:52,000 --> 00:08:56,000
 They suppress them, they try to avoid them.

137
00:08:56,000 --> 00:09:00,000
 Desire's bad, okay, I'll just get really upset

138
00:09:00,000 --> 00:09:04,000
 every time I want something. Anger's bad, well I'll feel

139
00:09:04,000 --> 00:09:04,000
 really

140
00:09:04,000 --> 00:09:07,540
 bad about that then. I'll get angry at the fact that I'm

141
00:09:07,540 --> 00:09:08,000
 angry.

142
00:09:08,000 --> 00:09:12,000
 If I'm tired, well then I'll try to force myself to stay

143
00:09:12,000 --> 00:09:12,000
 awake.

144
00:09:12,000 --> 00:09:16,000
 Tire myself out by

145
00:09:16,000 --> 00:09:20,000
 working really hard.

146
00:09:20,000 --> 00:09:24,000
 If I'm distracted, well then I'll force myself.

147
00:09:24,000 --> 00:09:28,000
 I'll exert myself with lots of effort.

148
00:09:28,000 --> 00:09:34,000
 I'll apply the effort to suppress the effort

149
00:09:34,000 --> 00:09:37,560
 so that might work. You can somehow stop yourself from

150
00:09:37,560 --> 00:09:38,000
 being distracted.

151
00:09:38,000 --> 00:09:42,000
 And if I have doubt,

152
00:09:42,000 --> 00:09:45,250
 well I'll just force myself to believe, which of course is

153
00:09:45,250 --> 00:09:46,000
 the best way to cultivate

154
00:09:46,000 --> 00:09:50,000
 long-term doubt.

155
00:09:50,000 --> 00:09:54,000
 It doesn't help the doubt at all.

156
00:09:54,000 --> 00:09:57,470
 In the long term it just makes you realize how you never

157
00:09:57,470 --> 00:09:58,000
 really understood

158
00:09:58,000 --> 00:10:02,000
 or had any good basis for confidence.

159
00:10:04,000 --> 00:10:06,170
 It prevents you from gaining true confidence. What helps

160
00:10:06,170 --> 00:10:08,000
 you gain true confidence

161
00:10:08,000 --> 00:10:11,440
 and helps overcome all of these really is observation of

162
00:10:11,440 --> 00:10:12,000
 them.

163
00:10:12,000 --> 00:10:15,690
 So as for the hindrances, the Buddha called them hindrances

164
00:10:15,690 --> 00:10:16,000
.

165
00:10:16,000 --> 00:10:20,000
 But like all the other things in the Satyavatthana Suddha,

166
00:10:20,000 --> 00:10:22,820
 all the other aspects of our experience, he said we should

167
00:10:22,820 --> 00:10:24,000
 be mindful of them.

168
00:10:24,000 --> 00:10:28,000
 See how they arise, see how they cease.

169
00:10:28,000 --> 00:10:32,000
 Observe them.

170
00:10:32,000 --> 00:10:36,000
 Study them.

171
00:10:36,000 --> 00:10:40,000
 The mind is full of anger. You know this is the mind full

172
00:10:40,000 --> 00:10:40,000
 of anger.

173
00:10:40,000 --> 00:10:44,000
 The mind is full of lust, passion.

174
00:10:44,000 --> 00:10:48,000
 This is the mind full of passion.

175
00:10:48,000 --> 00:10:52,000
 And you know the arising. So that means you know also the

176
00:10:52,000 --> 00:10:52,000
 things that give rise to them.

177
00:10:52,000 --> 00:10:56,000
 If you see something, that will give rise to liking,

178
00:10:56,000 --> 00:10:56,000
 something beautiful,

179
00:10:56,000 --> 00:11:00,000
 something attractive. So you're not seeing as well.

180
00:11:00,000 --> 00:11:04,000
 And if you note this process, you're able to understand

181
00:11:04,000 --> 00:11:08,000
 the cause and the effect. You see what it leads to.

182
00:11:08,000 --> 00:11:12,000
 You see the nature, the impression that you get from it.

183
00:11:12,000 --> 00:11:16,000
 How desire is something that clouds the mind.

184
00:11:16,000 --> 00:11:20,000
 It colors the mind.

185
00:11:20,000 --> 00:11:24,000
 Anger is something that inflames the mind. Drowsiness is

186
00:11:24,000 --> 00:11:28,000
 something that stifles the mind.

187
00:11:28,000 --> 00:11:32,000
 And so we note them just like everything else.

188
00:11:32,000 --> 00:11:36,000
 The five hindrances. The five hindrances in

189
00:11:36,000 --> 00:11:40,000
 brief, liking, disliking,

190
00:11:40,000 --> 00:11:44,000
 drowsiness, distraction, and doubt.

191
00:11:44,000 --> 00:11:48,000
 These five things, as I've been talking about,

192
00:11:48,000 --> 00:11:52,000
 these are really the most important advice to give to men.

193
00:11:52,000 --> 00:11:56,000
 They're the most important

194
00:11:56,000 --> 00:12:00,000
 aspect of the Dhamma to focus on as a beginner meditator.

195
00:12:00,000 --> 00:12:04,000
 Because in the beginning you're overwhelmed by these.

196
00:12:04,000 --> 00:12:08,000
 I mean throughout the course they'll come up, but in the

197
00:12:08,000 --> 00:12:08,000
 very beginning

198
00:12:08,000 --> 00:12:12,000
 these are what are going to get in your way. This is when a

199
00:12:12,000 --> 00:12:12,000
 new meditator really has to be

200
00:12:12,000 --> 00:12:16,000
 vigilant about. If your practice is not

201
00:12:16,000 --> 00:12:20,000
 going well, there's only five reasons really.

202
00:12:20,000 --> 00:12:24,000
 There's only these five.

203
00:12:24,000 --> 00:12:28,000
 What liking or wanting?

204
00:12:28,000 --> 00:12:32,000
 Disliking, which includes boredom,

205
00:12:32,000 --> 00:12:36,000
 fear, depression, sadness,

206
00:12:36,000 --> 00:12:40,000
 drowsiness, tiredness,

207
00:12:40,000 --> 00:12:44,000
 distraction. Distraction is also worry included in here.

208
00:12:44,000 --> 00:12:48,000
 These things prevent you from

209
00:12:48,000 --> 00:12:52,000
 focusing the mind on your work, on your task.

210
00:12:52,000 --> 00:12:56,000
 And doubt, doubt or confusion.

211
00:12:56,000 --> 00:13:00,000
 These are the only reasons why you really fail and why it's

212
00:13:00,000 --> 00:13:00,000
 hard

213
00:13:00,000 --> 00:13:04,000
 to practice meditation. It's important to look at these.

214
00:13:04,000 --> 00:13:08,000
 If these don't exist, because mindfulness can be

215
00:13:08,000 --> 00:13:12,000
 mindful of anything, whatever happens. You don't have to

216
00:13:12,000 --> 00:13:12,000
 stick to some formula.

217
00:13:12,000 --> 00:13:16,000
 And so without these

218
00:13:16,000 --> 00:13:20,000
 you can adapt, you can be flexible when

219
00:13:20,000 --> 00:13:24,000
 experiences change, you be mindful of the new experiences.

220
00:13:24,000 --> 00:13:28,000
 But with these it's very hard to accept change,

221
00:13:28,000 --> 00:13:32,000
 it's very hard to be open, it's very hard to

222
00:13:32,000 --> 00:13:36,000
 be flexible in the face of impermanence,

223
00:13:36,000 --> 00:13:40,000
 suffering non-self, in the face of

224
00:13:40,000 --> 00:13:44,000
 unmet expectations and so on.

225
00:13:46,000 --> 00:13:47,790
 So there you go, I knew this was just going to be a short

226
00:13:47,790 --> 00:13:50,000
 teaching, I've decided to,

227
00:13:50,000 --> 00:13:53,550
 because I have to think of something to talk about every

228
00:13:53,550 --> 00:13:54,000
 day,

229
00:13:54,000 --> 00:13:57,200
 I'll go with short talks every day, this is what my teacher

230
00:13:57,200 --> 00:13:58,000
 used to do.

231
00:13:58,000 --> 00:14:00,890
 Just give you a little bit of dhamma every day, something

232
00:14:00,890 --> 00:14:02,000
 for us to think about

233
00:14:02,000 --> 00:14:06,000
 in our practice.

234
00:14:06,000 --> 00:14:12,000
 So there you go, that's the dhamma for tonight.

235
00:14:12,000 --> 00:14:16,000
 I think there's one question on the site, but it wasn't

236
00:14:16,000 --> 00:14:20,000
 really about practice.

237
00:14:20,000 --> 00:14:24,000
 If you want to ask questions again, go to our meditation

238
00:14:24,000 --> 00:14:28,000
 site, there's two of them now.

239
00:14:28,000 --> 00:14:32,000
 Oh, three of them, okay. How do you address monks?

240
00:14:32,000 --> 00:14:36,000
 Oh, it's up to you.

241
00:14:36,000 --> 00:14:40,000
 People, my students call me bhante, which means like

242
00:14:40,000 --> 00:14:44,000
 venerable sir or something like that.

243
00:14:44,000 --> 00:15:06,000
 Can one become a Buddhist monk and still keep in touch with

244
00:15:06,000 --> 00:15:10,000
 family members? Yes, sure. Go to weddings,

245
00:15:10,000 --> 00:15:14,000
 funerals, and funerals maybe, weddings might be

246
00:15:14,000 --> 00:15:18,000
 problematic because being around alcohol is

247
00:15:18,000 --> 00:15:21,490
 I mean it's not like you're going to drink, but it's not

248
00:15:21,490 --> 00:15:22,000
 really a place for a Buddhist

249
00:15:22,000 --> 00:15:26,000
 monk, it's called agocera. There are places that are

250
00:15:26,000 --> 00:15:30,000
 not really proper for Buddhist monks, and places where they

251
00:15:30,000 --> 00:15:30,000
're

252
00:15:30,000 --> 00:15:34,000
 drinking alcohol is getting there. I mean there's no

253
00:15:34,000 --> 00:15:34,000
 specific

254
00:15:34,000 --> 00:15:38,000
 rule, but my rule of thumb is places where there's alcohol

255
00:15:38,000 --> 00:15:42,000
 use discretion.

256
00:15:42,000 --> 00:15:46,000
 But wedding ceremonies maybe. On the other hand

257
00:15:46,000 --> 00:15:50,000
 yeah, wedding ceremony maybe.

258
00:15:50,000 --> 00:15:54,000
 And there's lots, you know, being in touch with your family

259
00:15:54,000 --> 00:15:57,430
 is certainly not a problem. There's lots of rules that make

260
00:15:57,430 --> 00:15:58,000
 it clear that monks are

261
00:15:58,000 --> 00:16:02,000
 understood to keep in general relations with

262
00:16:02,000 --> 00:16:06,000
 their families because we have special exceptions for our

263
00:16:06,000 --> 00:16:10,000
 families, exceptions to many rules, allowances allowing us

264
00:16:10,000 --> 00:16:14,000
 to be involved and to care for our parents, to give food to

265
00:16:14,000 --> 00:16:14,000
 our

266
00:16:14,000 --> 00:16:18,000
 relatives, that kind of thing.

267
00:16:18,000 --> 00:16:26,000
 When craving arises, how should I go about cutting it out?

268
00:16:26,000 --> 00:16:30,000
 I should read my booklet on how to meditate. You can watch

269
00:16:30,000 --> 00:16:30,000
 my video

270
00:16:30,000 --> 00:16:34,000
 on pornography.

271
00:16:34,000 --> 00:16:38,000
 It's my most popular video, probably by the title.

272
00:16:38,000 --> 00:16:42,000
 But I don't know, it's a long, maybe a little bit rambling,

273
00:16:42,000 --> 00:16:42,000
 I don't

274
00:16:42,000 --> 00:16:46,000
 know, but I think there's some good stuff in there. It's

275
00:16:46,000 --> 00:16:46,000
 very popular

276
00:16:46,000 --> 00:16:50,000
 for whatever reason.

277
00:16:50,000 --> 00:16:53,520
 It was on pornography and masturbation based on a question

278
00:16:53,520 --> 00:16:54,000
 someone asked many years ago.

279
00:16:54,000 --> 00:16:58,000
 As a Buddhist, what should one

280
00:16:58,000 --> 00:17:02,000
 do if a precept is accidentally broken? Well, make a

281
00:17:02,000 --> 00:17:02,000
 determination

282
00:17:02,000 --> 00:17:06,000
 not to break it in the future.

283
00:17:06,000 --> 00:17:09,070
 Driving in a squirrel jumped in front of the road and

284
00:17:09,070 --> 00:17:10,000
 accidentally hit it. Well, that's

285
00:17:10,000 --> 00:17:14,000
 not breaking a precept. Killing is only killing if you

286
00:17:14,000 --> 00:17:14,000
 intend

287
00:17:14,000 --> 00:17:18,000
 to kill. The fact that you feel bad about it is the real

288
00:17:18,000 --> 00:17:18,000
 problem.

289
00:17:18,000 --> 00:17:22,000
 It's a problem because it's causing suffering.

290
00:17:22,000 --> 00:17:26,000
 You didn't intend for the squirrel to die. There's no

291
00:17:26,000 --> 00:17:26,000
 reason for you to be

292
00:17:26,000 --> 00:17:29,700
 upset. If it was a person who jumped out in front of your

293
00:17:29,700 --> 00:17:30,000
 car, nobody would

294
00:17:30,000 --> 00:17:33,270
 come after you. The law wouldn't come after you unless you

295
00:17:33,270 --> 00:17:34,000
 were speeding or reckless

296
00:17:34,000 --> 00:17:38,000
 or something like that.

297
00:17:38,000 --> 00:17:42,000
 Even then, I don't think you'd be caught for manslaughter

298
00:17:42,000 --> 00:17:46,000
 because a person jumped out in front of you on the road.

299
00:17:46,000 --> 00:17:50,000
 I don't know.

300
00:17:50,000 --> 00:17:54,000
 But yeah, that's certainly not a bad thing.

301
00:17:54,000 --> 00:17:58,000
 But the bad thing is the guilt and the feeling bad about it

302
00:17:58,000 --> 00:17:58,000
.

303
00:17:58,000 --> 00:18:02,000
 You should be mindful of that and learn to let it go and be

304
00:18:02,000 --> 00:18:02,000
 happier.

305
00:18:22,000 --> 00:18:26,000
 Okay, well that's all the questions on our site.

306
00:18:26,000 --> 00:18:30,000
 How are we doing in Second Life?

307
00:18:30,000 --> 00:18:34,000
 Everybody hear me?

308
00:18:34,000 --> 00:18:48,000
 Okay. Thank you all for coming out.

309
00:18:48,000 --> 00:18:52,000
 Have a good night.

