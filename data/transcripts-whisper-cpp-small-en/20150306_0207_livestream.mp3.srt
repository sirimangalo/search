1
00:00:00,000 --> 00:00:12,920
 Okay, so here we are today broadcasting from Stony Creek,

2
00:00:12,920 --> 00:00:23,000
 Ontario.

3
00:00:23,000 --> 00:00:43,200
 Religion is an interesting thing, something that can be

4
00:00:43,200 --> 00:00:47,000
 approached in lots of different

5
00:00:47,000 --> 00:00:59,600
 ways, can be used for many different things, many different

6
00:00:59,600 --> 00:01:04,800
 purposes.

7
00:01:04,800 --> 00:01:28,520
 Buddha likened Buddhism to a tree, a big majestic tree with

8
00:01:28,520 --> 00:01:33,800
 many different parts to it.

9
00:01:33,800 --> 00:01:51,440
 And so he pointed out that different people come to the

10
00:01:51,440 --> 00:01:54,600
 tree and take away different parts

11
00:01:54,600 --> 00:01:57,600
 of the tree.

12
00:01:57,600 --> 00:02:07,830
 And he said it's like a person going into the forest

13
00:02:07,830 --> 00:02:14,400
 looking for the heartwood or the

14
00:02:14,400 --> 00:02:34,120
 essence.

15
00:02:34,120 --> 00:02:40,080
 They come to Buddhism looking for the heart of Buddhism or

16
00:02:40,080 --> 00:02:44,120
 the heart of the Buddhist teaching.

17
00:02:44,120 --> 00:02:47,120
 They come looking for the highest goal.

18
00:02:47,120 --> 00:03:05,320
 But what we take away is it differs from person to person.

19
00:03:05,320 --> 00:03:11,280
 I get a lot of requests from people, for example, who would

20
00:03:11,280 --> 00:03:16,120
 like to become monks, like to ordain.

21
00:03:16,120 --> 00:03:21,730
 So they contact me asking how they can ordain, where they

22
00:03:21,730 --> 00:03:24,120
 can go to ordain.

23
00:03:24,120 --> 00:03:26,120
 It's not something I really answer.

24
00:03:26,120 --> 00:03:36,680
 It's a question I personally tend to avoid.

25
00:03:36,680 --> 00:03:39,890
 Because if a person is just looking to ordain and doesn't

26
00:03:39,890 --> 00:03:42,120
 mention an interest in meditation,

27
00:03:42,120 --> 00:03:46,310
 they've kind of, you could say they're barking up the wrong

28
00:03:46,310 --> 00:03:47,120
 tree.

29
00:03:47,120 --> 00:03:53,120
 They're coming in with the wrong idea.

30
00:03:53,120 --> 00:04:03,150
 Similarly, you have many Buddhists who grew up as Buddhists

31
00:04:03,150 --> 00:04:05,840
 culturally.

32
00:04:05,840 --> 00:04:13,890
 Some of them even ordain as monks without any idea about

33
00:04:13,890 --> 00:04:17,840
 the essence or the heart of

34
00:04:17,840 --> 00:04:23,000
 Buddhism with the real true teachings of the Buddha, true

35
00:04:23,000 --> 00:04:31,560
 intentions of the Buddha in teaching.

36
00:04:31,560 --> 00:04:42,420
 So the Buddha gave an example of a person who comes to

37
00:04:42,420 --> 00:04:47,280
 follow the Buddhist teaching only

38
00:04:47,280 --> 00:05:01,040
 for the material aspects.

39
00:05:01,040 --> 00:05:10,920
 So maybe they come to view the architecture or the art or

40
00:05:10,920 --> 00:05:15,720
 listen to the chanting or something

41
00:05:15,720 --> 00:05:22,960
 like that.

42
00:05:22,960 --> 00:05:30,630
 Or to the point, maybe they ordain or they stay in the

43
00:05:30,630 --> 00:05:33,840
 meditation center just to make

44
00:05:33,840 --> 00:05:41,010
 a living, to get fed and to be sheltered and to be

45
00:05:41,010 --> 00:05:43,200
 supported.

46
00:05:43,200 --> 00:05:47,050
 People who ordain for this reason, even in the time of the

47
00:05:47,050 --> 00:05:49,160
 Buddha, this was the case.

48
00:05:49,160 --> 00:05:56,390
 People tried to ordain just to get fed because they saw the

49
00:05:56,390 --> 00:06:00,120
 Buddhist monks who were very

50
00:06:00,120 --> 00:06:08,360
 well fed because the Buddha was so well respected.

51
00:06:08,360 --> 00:06:19,560
 The Buddha said this is like someone who goes into the

52
00:06:19,560 --> 00:06:26,760
 forest looking for wood and comes

53
00:06:26,760 --> 00:06:45,120
 out with leaves and twigs, leaves and branches.

54
00:06:45,120 --> 00:06:49,340
 This person thinks to themselves that they found the heart

55
00:06:49,340 --> 00:06:51,560
 wood and they're wrong.

56
00:06:51,560 --> 00:06:58,080
 They haven't gotten to the heart of the tree.

57
00:06:58,080 --> 00:07:01,400
 They found something that's actually quite useless in the

58
00:07:01,400 --> 00:07:03,160
 long term, something that is

59
00:07:03,160 --> 00:07:14,760
 not going to help them.

60
00:07:14,760 --> 00:07:28,520
 The second person, the first one is leaves and twigs.

61
00:07:28,520 --> 00:07:35,630
 The second person practices Buddhism to the extent of

62
00:07:35,630 --> 00:07:40,200
 morality as an ethical practice.

63
00:07:40,200 --> 00:07:46,230
 This is common with people who will keep the five precepts

64
00:07:46,230 --> 00:07:49,480
 or even the eight precepts.

65
00:07:49,480 --> 00:07:53,370
 Even monks who will keep all the monastic precepts, but

66
00:07:53,370 --> 00:07:55,560
 they might not go any further.

67
00:07:55,560 --> 00:08:06,880
 In fact, they can become proud of their morality, proud of

68
00:08:06,880 --> 00:08:12,200
 their ethical behavior.

69
00:08:12,200 --> 00:08:16,400
 You can even become proud of the gains that you get.

70
00:08:16,400 --> 00:08:23,210
 The material, talking about material, it's easy to become

71
00:08:23,210 --> 00:08:24,360
 proud.

72
00:08:24,360 --> 00:08:28,960
 Monks become proud of their status and they will become

73
00:08:28,960 --> 00:08:31,720
 proud of their cultural aspects

74
00:08:31,720 --> 00:08:37,240
 of their religion.

75
00:08:37,240 --> 00:08:40,960
 You can also become proud of your morality, proud of your

76
00:08:40,960 --> 00:08:43,280
 ethical behavior, your ability

77
00:08:43,280 --> 00:08:56,080
 to follow rules, even our ability to meditate.

78
00:08:56,080 --> 00:09:02,120
 Be proud of the time that we put into the meditation.

79
00:09:02,120 --> 00:09:10,260
 But if you don't go any further and actually cultivate the

80
00:09:10,260 --> 00:09:12,000
 mental aspects of meditation,

81
00:09:12,000 --> 00:09:22,160
 this is like taking the branches of the tree.

82
00:09:22,160 --> 00:09:24,520
 I don't think this is a big problem for meditators.

83
00:09:24,520 --> 00:09:27,920
 Usually they can go further than this.

84
00:09:27,920 --> 00:09:31,640
 It's where it starts to get interesting when we talk about

85
00:09:31,640 --> 00:09:34,480
 the different types of meditation.

86
00:09:34,480 --> 00:09:39,320
 The Buddha said there's another person who cultivates

87
00:09:39,320 --> 00:09:41,680
 morality but isn't satisfied

88
00:09:41,680 --> 00:09:50,430
 thereby and goes deeper and actually cultivates

89
00:09:50,430 --> 00:09:55,520
 concentration, tranquility.

90
00:09:55,520 --> 00:09:58,600
 They calm their mind.

91
00:09:58,600 --> 00:10:06,000
 They settle their minds into the moment, into the

92
00:10:06,000 --> 00:10:08,760
 meditation object.

93
00:10:08,760 --> 00:10:18,680
 And their minds become focused and calm.

94
00:10:18,680 --> 00:10:25,620
 Such a person can still become complacent, even egotistical

95
00:10:25,620 --> 00:10:29,720
 or proud of their accomplishments,

96
00:10:29,720 --> 00:10:38,400
 thinking they've accomplished something essential and

97
00:10:38,400 --> 00:10:42,360
 profound and meaningful.

98
00:10:42,360 --> 00:10:46,290
 The Buddha said this sort of person is like a person who

99
00:10:46,290 --> 00:10:48,840
 going in for the hardwood settles

100
00:10:48,840 --> 00:10:55,640
 for bark, settles for the bark of the tree.

101
00:10:55,640 --> 00:11:00,480
 From tranquility meditation, even all the jhanas and these

102
00:11:00,480 --> 00:11:02,280
 states of calm, profound

103
00:11:02,280 --> 00:11:07,320
 calm and absorption that can lead to magical powers and so

104
00:11:07,320 --> 00:11:07,920
 on.

105
00:11:07,920 --> 00:11:13,310
 All of this the Buddha said is just like bark, the bark of

106
00:11:13,310 --> 00:11:14,520
 the tree.

107
00:11:14,520 --> 00:11:20,650
 It's useful to protect the hardwood but it isn't the hard

108
00:11:20,650 --> 00:11:21,640
wood.

109
00:11:21,640 --> 00:11:27,200
 It isn't even any of the wood at all.

110
00:11:27,200 --> 00:11:32,360
 It's because it can't.

111
00:11:32,360 --> 00:11:35,880
 It can't deal with the nature of reality.

112
00:11:35,880 --> 00:11:38,360
 It doesn't really deal with wisdom.

113
00:11:38,360 --> 00:11:44,640
 It doesn't address the problem of ignorance and delusion.

114
00:11:44,640 --> 00:11:53,640
 It doesn't enlighten us to the nature of reality.

115
00:11:53,640 --> 00:11:58,400
 So we have to go further.

116
00:11:58,400 --> 00:12:01,750
 And so yet another person isn't content with the tranqu

117
00:12:01,750 --> 00:12:03,840
ility that comes from meditation

118
00:12:03,840 --> 00:12:07,200
 but they delve deeper and cultivate insight.

119
00:12:07,200 --> 00:12:14,960
 This is where we find ourselves here in this practice, in

120
00:12:14,960 --> 00:12:18,280
 this tradition, in this meditation

121
00:12:18,280 --> 00:12:19,280
 course.

122
00:12:19,280 --> 00:12:27,800
 We've become and delve into the realm of insight.

123
00:12:27,800 --> 00:12:34,850
 They start to see the nature of the body and mind, how the

124
00:12:34,850 --> 00:12:38,800
 body and mind work together.

125
00:12:38,800 --> 00:12:47,160
 And they start to understand deeper and deeper profound

126
00:12:47,160 --> 00:12:55,520
 knowledges in regards to this reality.

127
00:12:55,520 --> 00:13:01,000
 See everything arising and ceasing and unstable, uncontroll

128
00:13:01,000 --> 00:13:04,760
able, undesirable, not worth clinging

129
00:13:04,760 --> 00:13:05,760
 to.

130
00:13:05,760 --> 00:13:08,750
 They start to see clearer and clearer that there's nothing

131
00:13:08,750 --> 00:13:09,920
 worth clinging to.

132
00:13:09,920 --> 00:13:12,440
 That clinging only leads to suffering.

133
00:13:12,440 --> 00:13:18,430
 And so they slowly, slowly align themselves with this truth

134
00:13:18,430 --> 00:13:21,440
 and begin to free themselves

135
00:13:21,440 --> 00:13:25,480
 from their attachments.

136
00:13:25,480 --> 00:13:30,440
 Now you'd think this was the hardwood but it's not actually

137
00:13:30,440 --> 00:13:30,800
.

138
00:13:30,800 --> 00:13:39,000
 This is the softwood, the soft outer wood of the tree that

139
00:13:39,000 --> 00:13:42,880
 is still not ideal, still

140
00:13:42,880 --> 00:13:47,080
 not the goal, still not enough.

141
00:13:47,080 --> 00:13:57,480
 You can't really build a home with this wood.

142
00:13:57,480 --> 00:13:58,480
 It's too soft.

143
00:13:58,480 --> 00:14:02,600
 The point being that it isn't permanent.

144
00:14:02,600 --> 00:14:08,040
 It isn't decisive.

145
00:14:08,040 --> 00:14:10,660
 All the knowledge we gain and we pass into insight and we

146
00:14:10,660 --> 00:14:12,080
 pass into meditation can be

147
00:14:12,080 --> 00:14:17,000
 lost.

148
00:14:17,000 --> 00:14:21,120
 All the knowledge that comes and all the realizations that

149
00:14:21,120 --> 00:14:23,560
 come, as long as they're still worldly

150
00:14:23,560 --> 00:14:24,920
 they can all be lost.

151
00:14:24,920 --> 00:14:28,960
 It's only when we attain super mundane insight.

152
00:14:28,960 --> 00:14:33,380
 This is the final person, the person who goes in looking

153
00:14:33,380 --> 00:14:35,760
 for hardwood and comes out with

154
00:14:35,760 --> 00:14:36,760
 hardwood.

155
00:14:36,760 --> 00:14:42,010
 That's someone who has followed the path of insight, who's

156
00:14:42,010 --> 00:14:47,080
 cultivated morality and concentration

157
00:14:47,080 --> 00:14:49,930
 and cultivated wisdom to the extent that they free

158
00:14:49,930 --> 00:14:50,920
 themselves.

159
00:14:50,920 --> 00:14:54,640
 So this is what we're aiming for.

160
00:14:54,640 --> 00:14:58,640
 As you practice along the insights that come are great but

161
00:14:58,640 --> 00:15:00,800
 they're only stepping stones

162
00:15:00,800 --> 00:15:05,440
 towards the final goal of freedom.

163
00:15:05,440 --> 00:15:12,080
 And only when you finally fully and completely let go and

164
00:15:12,080 --> 00:15:16,160
 align yourself with the truth.

165
00:15:16,160 --> 00:15:22,320
 Only then can you say you've found the heart of the Buddha

166
00:15:22,320 --> 00:15:24,120
's teaching.

167
00:15:24,120 --> 00:15:28,740
 Something that is really useful because everything else is

168
00:15:28,740 --> 00:15:31,640
 impermanent and it really lasts.

169
00:15:31,640 --> 00:15:35,560
 All of the rest is just support.

170
00:15:35,560 --> 00:15:42,370
 It's not that the rest of these things are useless, they

171
00:15:42,370 --> 00:15:45,080
 support the tree.

172
00:15:45,080 --> 00:15:54,120
 But unless you can see that goal, that essence, the rest

173
00:15:54,120 --> 00:15:59,120
 becomes quite meaningless.

174
00:15:59,120 --> 00:16:10,520
 You lose the reason for all of it, purpose for any of it.

175
00:16:10,520 --> 00:16:12,960
 So we have to ask ourselves what we've gotten.

176
00:16:12,960 --> 00:16:29,770
 We can look and count our winnings, count our gains, see

177
00:16:29,770 --> 00:16:32,320
 what we've gained from the Buddha's

178
00:16:32,320 --> 00:16:36,240
 teaching.

179
00:16:36,240 --> 00:16:48,760
 And strive on until we find the essence, the heart.

180
00:16:48,760 --> 00:16:55,030
 The heart which is real freedom, true freedom, where we are

181
00:16:55,030 --> 00:16:55,440
.

182
00:16:55,440 --> 00:17:04,360
 It changes, something that changes who we are.

183
00:17:04,360 --> 00:17:07,720
 Not much I can say you have to experience it for yourself.

184
00:17:07,720 --> 00:17:14,600
 But rest assured, it's generally not any of the things that

185
00:17:14,600 --> 00:17:16,040
 we think of.

186
00:17:16,040 --> 00:17:20,240
 It's not any of the stepping stones on the path.

187
00:17:20,240 --> 00:17:22,520
 Even though all these stepping stones point towards

188
00:17:22,520 --> 00:17:24,560
 something, it's what they point towards.

189
00:17:24,560 --> 00:17:28,480
 It's what they lead us towards.

190
00:17:28,480 --> 00:17:31,890
 Rest assured there is a goal at the end, something on a

191
00:17:31,890 --> 00:17:34,320
 level more profound than any of this.

192
00:17:34,320 --> 00:17:39,220
 And once you realize it, then you can say for yourself that

193
00:17:39,220 --> 00:17:41,320
 this indeed is the goal,

194
00:17:41,320 --> 00:17:45,280
 this indeed is the heart of the Buddha's teaching.

195
00:17:45,280 --> 00:17:54,630
 So something worth thinking about, a little more dumb for

196
00:17:54,630 --> 00:17:57,000
 listening.

197
00:17:57,000 --> 00:17:57,680
 Keep practicing.

