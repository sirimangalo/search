1
00:00:00,000 --> 00:00:23,110
 Today we go on to finish the latest verse on the Mangala

2
00:00:23,110 --> 00:00:26,640
 Sutta.

3
00:00:27,640 --> 00:00:56,120
 This verse is called Kathanyutam.

4
00:00:56,120 --> 00:01:02,480
 Kattanyutta means gratitude.

5
00:01:02,480 --> 00:01:16,010
 Kralena Dhammasavanam means to hear the Dhamma at the right

6
00:01:16,010 --> 00:01:18,840
 time.

7
00:01:18,840 --> 00:01:24,700
 Kathanyutta is something very important in Buddhism which

8
00:01:24,700 --> 00:01:27,400
 is maybe not talked about quite

9
00:01:27,400 --> 00:01:43,000
 enough.

10
00:01:43,000 --> 00:01:45,610
 But it is certainly a part of the Lord Buddha's teaching

11
00:01:45,610 --> 00:01:47,400
 that we should be grateful for those

12
00:01:47,400 --> 00:01:50,760
 people who have helped us.

13
00:01:50,760 --> 00:01:56,200
 We should never forget the things, the good things that

14
00:01:56,200 --> 00:01:58,680
 they have done for us.

15
00:01:58,680 --> 00:02:03,140
 We should try our best to put aside the bad things which

16
00:02:03,140 --> 00:02:04,680
 they have done.

17
00:02:04,680 --> 00:02:06,880
 And whatever good things someone else has done for us,

18
00:02:06,880 --> 00:02:08,160
 always try to remember those

19
00:02:08,160 --> 00:02:09,160
 things.

20
00:02:09,160 --> 00:02:23,330
 It doesn't mean that we have to be with the people who have

21
00:02:23,330 --> 00:02:32,440
 done good things for us or

22
00:02:32,440 --> 00:02:39,560
 even necessarily associate with them.

23
00:02:39,560 --> 00:02:43,960
 Sometimes people have done good things for us but are also

24
00:02:43,960 --> 00:02:45,880
 the sort of people who we

25
00:02:45,880 --> 00:03:00,200
 shouldn't associate closely with.

26
00:03:00,200 --> 00:03:07,100
 For instance with monks it's always difficult to associate

27
00:03:07,100 --> 00:03:09,400
 with lay people.

28
00:03:09,400 --> 00:03:13,060
 Sometimes it seems like the monks are very distant from the

29
00:03:13,060 --> 00:03:13,720
 world.

30
00:03:13,720 --> 00:03:21,730
 And this is of course a behavior for monks who have removed

31
00:03:21,730 --> 00:03:26,000
 themselves from the world.

32
00:03:26,000 --> 00:03:37,290
 Which is quite difficult because we depend on lay people as

33
00:03:37,290 --> 00:03:40,360
 our support.

34
00:03:40,360 --> 00:03:50,790
 Yet it's prohibited for monks to repay the acts of kindness

35
00:03:50,790 --> 00:03:54,200
 shown by lay people with

36
00:03:54,200 --> 00:03:59,560
 similar acts of help.

37
00:03:59,560 --> 00:04:06,640
 It's considered wrong livelihood for monks to barter with

38
00:04:06,640 --> 00:04:08,440
 lay people.

39
00:04:08,440 --> 00:04:13,450
 The supporters do one thing and then the monks turn around

40
00:04:13,450 --> 00:04:17,040
 and support with labor or with

41
00:04:17,040 --> 00:04:21,800
 gifts or so on in the same way.

42
00:04:21,800 --> 00:04:30,650
 It's considered to be flattering or ingratiating oneself

43
00:04:30,650 --> 00:04:33,600
 with lay people.

44
00:04:33,600 --> 00:04:39,000
 As a monk we are content with whatever they get.

45
00:04:39,000 --> 00:04:49,140
 We are very happy to receive nothing and have to be content

46
00:04:49,140 --> 00:04:52,200
 with nothing.

47
00:04:52,200 --> 00:05:00,120
 So we are not allowed to ask for things or to return the

48
00:05:00,120 --> 00:05:04,160
 favor in the way of amissat

49
00:05:04,160 --> 00:05:09,200
 or objects, external objects.

50
00:05:09,200 --> 00:05:12,720
 Of course it is the job of the monks to turn around and

51
00:05:12,720 --> 00:05:15,120
 teach, to study intensively and

52
00:05:15,120 --> 00:05:18,440
 to memorize.

53
00:05:18,440 --> 00:05:20,950
 Sometimes for lay people the only duty is to come to

54
00:05:20,950 --> 00:05:21,640
 practice.

55
00:05:21,640 --> 00:05:25,690
 And for monks it's necessary to study and to memorize the

56
00:05:25,690 --> 00:05:27,520
 Buddhist teaching to pass

57
00:05:27,520 --> 00:05:30,680
 it on.

58
00:05:30,680 --> 00:05:37,550
 So they often are in a better position to teach as a result

59
00:05:37,550 --> 00:05:37,600
.

60
00:05:37,600 --> 00:05:43,860
 They can go ahead and teach as a way of returning the favor

61
00:05:43,860 --> 00:05:44,240
.

62
00:05:44,240 --> 00:05:50,420
 Sometimes we can't always show our gratitude due to the

63
00:05:50,420 --> 00:05:52,640
 circumstances.

64
00:05:52,640 --> 00:05:55,470
 We have to keep in our hearts a sense of gratitude for

65
00:05:55,470 --> 00:05:57,480
 those people who have helped us.

66
00:05:57,480 --> 00:06:01,960
 Most of all gratitude for our men and our father.

67
00:06:01,960 --> 00:06:08,410
 Even though sometimes we can't be with them sometimes, even

68
00:06:08,410 --> 00:06:09,200
 our mother and father might

69
00:06:09,200 --> 00:06:12,200
 have wrong view.

70
00:06:12,200 --> 00:06:17,900
 But it's still somehow our duty not to be with them or even

71
00:06:17,900 --> 00:06:20,680
 to necessarily to support

72
00:06:20,680 --> 00:06:21,680
 them.

73
00:06:21,680 --> 00:06:27,130
 It's necessary to try our best to show them the way if

74
00:06:27,130 --> 00:06:28,680
 possible.

75
00:06:28,680 --> 00:06:30,600
 Sometimes it's not possible.

76
00:06:30,600 --> 00:06:35,560
 Many times it doesn't come about.

77
00:06:35,560 --> 00:06:38,680
 But sometimes if we're lucky it is possible.

78
00:06:38,680 --> 00:06:45,440
 It's possible to bring about a change in our parents.

79
00:06:45,440 --> 00:06:50,840
 Sariputta's mother, even to the last moment before Sariput

80
00:06:50,840 --> 00:06:53,120
ta was going to pass away, she

81
00:06:53,120 --> 00:06:58,410
 still held firmly and was very adamant that Sariputta had

82
00:06:58,410 --> 00:07:01,720
 followed the wrong path by leaving

83
00:07:01,720 --> 00:07:07,560
 behind the religion of the Brahmins.

84
00:07:07,560 --> 00:07:11,680
 And before he passed away Sariputta was very sick and went

85
00:07:11,680 --> 00:07:13,200
 to see his mother.

86
00:07:13,200 --> 00:07:18,860
 We managed to show her that the path which he had followed

87
00:07:18,860 --> 00:07:21,720
 was truly the right path.

88
00:07:21,720 --> 00:07:28,320
 But in fact it was the path that even Brahma himself made

89
00:07:28,320 --> 00:07:31,720
 the greatest respect to.

90
00:07:31,720 --> 00:07:40,190
 Brahma himself came to pay respect to Sariputta before he

91
00:07:40,190 --> 00:07:42,560
 passed away.

92
00:07:42,560 --> 00:07:49,160
 Sariputta was a very good example of gratitude.

93
00:07:49,160 --> 00:07:51,980
 Not just in regards to his mother but in regards to anyone

94
00:07:51,980 --> 00:07:53,560
 who had done something good for

95
00:07:53,560 --> 00:07:54,560
 him.

96
00:07:54,560 --> 00:07:58,050
 And the way he showed it was very clearly in line with the

97
00:07:58,050 --> 00:07:59,560
 Buddha's teaching.

98
00:07:59,560 --> 00:08:03,440
 There was one man who came to be ordained.

99
00:08:03,440 --> 00:08:08,840
 He was the man who I believe had disrobed six times.

100
00:08:08,840 --> 00:08:11,470
 He had become a monk and disrobed and become a monk and dis

101
00:08:11,470 --> 00:08:12,000
robed.

102
00:08:12,000 --> 00:08:15,480
 In the seventh time no one wanted to ordain him.

103
00:08:15,480 --> 00:08:17,480
 Nobody would ordain him.

104
00:08:17,480 --> 00:08:21,360
 And he came and he asked permission to ordain again.

105
00:08:21,360 --> 00:08:30,120
 And the Lord Buddha knew that if he ordained the seventh

106
00:08:30,120 --> 00:08:35,160
 time he would become an arahant.

107
00:08:35,160 --> 00:08:40,720
 So he asked the monks, "Is there anyone who could think of

108
00:08:40,720 --> 00:08:43,440
 something that this old man

109
00:08:43,440 --> 00:08:49,280
 had done for them?"

110
00:08:49,280 --> 00:08:53,690
 And Sariputta spoke up and said, "Yes, one time when I was

111
00:08:53,690 --> 00:08:55,600
 going by his house he put

112
00:08:55,600 --> 00:08:57,560
 a spoonful of rice in my bowl."

113
00:08:57,560 --> 00:09:06,360
 I can remember something good that he did for me.

114
00:09:06,360 --> 00:09:07,360
 And the Lord Buddha said, "What, Sariputta?

115
00:09:07,360 --> 00:09:08,360
 Then you ordain him."

116
00:09:08,360 --> 00:09:09,360
 And he said, "Sadhu Sariputta."

117
00:09:09,360 --> 00:09:10,360
 Very good Sariputta.

118
00:09:10,360 --> 00:09:32,760
 Because this is a sign of a good person who remembers even

119
00:09:32,760 --> 00:09:37,360
 a small gift given to them.

120
00:09:37,360 --> 00:09:41,040
 Even a small good deed someone does for them they remember.

121
00:09:41,040 --> 00:09:44,420
 They don't ever forget the good deeds which one has done.

122
00:09:44,420 --> 00:09:50,680
 Even a small good deed.

123
00:09:50,680 --> 00:09:55,280
 And so he ended up ordaining and becoming an under Sariput

124
00:09:55,280 --> 00:09:57,460
ta and becoming as a result

125
00:09:57,460 --> 00:10:01,720
 of Sariputta's gratitude.

126
00:10:01,720 --> 00:10:06,780
 Just one spoonful of rice.

127
00:10:06,780 --> 00:10:10,640
 And gratitude isn't exactly what we're practicing here.

128
00:10:10,640 --> 00:10:15,690
 We're not directly practicing gratitude but we can explain

129
00:10:15,690 --> 00:10:18,440
 how does gratitude fit in with

130
00:10:18,440 --> 00:10:21,920
 the meditation practice.

131
00:10:21,920 --> 00:10:25,030
 It's important that every time we practice, when we finish

132
00:10:25,030 --> 00:10:26,680
 practicing, we spend a short

133
00:10:26,680 --> 00:10:32,320
 period of time dedicating the merit which comes from the

134
00:10:32,320 --> 00:10:35,120
 practice to our parents and

135
00:10:35,120 --> 00:10:37,680
 our teachers.

136
00:10:37,680 --> 00:10:42,090
 Teachers since we were young, to our relatives and our

137
00:10:42,090 --> 00:10:45,080
 friends and our enemies, and to all

138
00:10:45,080 --> 00:10:51,640
 beings and the all beings being free from suffering.

139
00:10:51,640 --> 00:10:56,080
 If we do like this we'll never meet with danger anywhere we

140
00:10:56,080 --> 00:10:56,640
 go.

141
00:10:56,640 --> 00:11:02,590
 We have to try to remember every time to distribute the

142
00:11:02,590 --> 00:11:06,520
 goodness which comes from the meditation

143
00:11:06,520 --> 00:11:07,520
 practice.

144
00:11:07,520 --> 00:11:12,520
 It's a way of developing gratitude in the heart.

145
00:11:12,520 --> 00:11:19,360
 Developing a sense of gratitude for those people who have

146
00:11:19,360 --> 00:11:22,280
 helped us in this way.

147
00:11:22,280 --> 00:11:27,680
 And the last thing to talk about is Kalayana Dhamma Savana.

148
00:11:27,680 --> 00:11:35,840
 Do you hear the Dhamma at the right time?

149
00:11:35,840 --> 00:11:41,800
 Hearing the Dhamma is something which is very difficult.

150
00:11:41,800 --> 00:11:45,960
 Those of us who come from other countries we can appreciate

151
00:11:45,960 --> 00:11:46,520
 this.

152
00:11:46,520 --> 00:11:48,880
 Sometimes the Thai people don't appreciate it so much.

153
00:11:48,880 --> 00:11:50,760
 How difficult it is.

154
00:11:50,760 --> 00:11:57,800
 Which is natural because it's not so difficult in Thailand.

155
00:11:57,800 --> 00:12:01,330
 But the difficulty doesn't just come from not living in a

156
00:12:01,330 --> 00:12:03,040
 place where we can hear the

157
00:12:03,040 --> 00:12:05,800
 Dhamma.

158
00:12:05,800 --> 00:12:11,450
 It also comes from the difficulty in finding a time to hear

159
00:12:11,450 --> 00:12:12,960
 the Dhamma.

160
00:12:12,960 --> 00:12:17,440
 The Buddha said Kalayana, which means at a time when there

161
00:12:17,440 --> 00:12:19,460
 is the Dhamma or at the time

162
00:12:19,460 --> 00:12:24,140
 which is suitable, can also mean the fact that it's hard to

163
00:12:24,140 --> 00:12:26,000
 find the time to hear the

164
00:12:26,000 --> 00:12:27,000
 Dhamma.

165
00:12:27,000 --> 00:12:30,330
 Even people who live in Thailand in the time when the

166
00:12:30,330 --> 00:12:32,680
 Buddha's Asana is very strong and

167
00:12:32,680 --> 00:12:35,200
 they don't take the time.

168
00:12:35,200 --> 00:12:40,480
 Even in the Buddha's time many people couldn't find the

169
00:12:40,480 --> 00:12:43,120
 time to hear the Dhamma.

170
00:12:43,120 --> 00:12:48,690
 Or it can also mean in the time when there is the Dhamma in

171
00:12:48,690 --> 00:12:50,120
 the world.

172
00:12:50,120 --> 00:12:54,670
 Not that there's going to be a Buddha arise in the world

173
00:12:54,670 --> 00:12:55,840
 every day.

174
00:12:55,840 --> 00:12:58,960
 It's obvious that it's something very rare to find someone

175
00:12:58,960 --> 00:13:00,240
 to arise in the world who

176
00:13:00,240 --> 00:13:03,400
 we could call a Buddha.

177
00:13:03,400 --> 00:13:09,400
 Who is capable of teaching.

178
00:13:09,400 --> 00:13:15,040
 It's very difficult, this Dhamma which is very difficult to

179
00:13:15,040 --> 00:13:15,800
 see.

180
00:13:15,800 --> 00:13:26,360
 Which is very profound and deep and hard to understand.

181
00:13:26,360 --> 00:13:31,190
 Take someone, they say four uncountable, at least at the

182
00:13:31,190 --> 00:13:33,920
 very minimum it would take four

183
00:13:33,920 --> 00:13:41,370
 uncountable eons and a hundred thousand great eons on top

184
00:13:41,370 --> 00:13:42,840
 of that.

185
00:13:42,840 --> 00:13:46,600
 Just to become a Buddha which I talked about before when I

186
00:13:46,600 --> 00:13:48,720
 talked about the Buddha, the

187
00:13:48,720 --> 00:13:52,120
 Dhamma and the Sangha.

188
00:13:52,120 --> 00:14:01,140
 And during that time, so other Buddhas arise and pass away

189
00:14:01,140 --> 00:14:05,080
 but so much of that time is

190
00:14:05,080 --> 00:14:13,040
 full of, is empty of the teaching of the Lord Buddha.

191
00:14:13,040 --> 00:14:16,950
 So in this life we've had a chance to come to see the Lord

192
00:14:16,950 --> 00:14:19,400
 Buddha, to see the teachings,

193
00:14:19,400 --> 00:14:23,150
 to come to hear the teachings of the Lord Buddha, we

194
00:14:23,150 --> 00:14:25,720
 shouldn't take it for granted.

195
00:14:25,720 --> 00:14:30,880
 It's not something that's easy to find.

196
00:14:30,880 --> 00:14:34,390
 Not just difficult to find in the world now but difficult

197
00:14:34,390 --> 00:14:36,200
 to find this birth as a human

198
00:14:36,200 --> 00:14:40,240
 being in the time of the Buddhist teaching.

199
00:14:40,240 --> 00:14:45,230
 Buddhist teaching is something which doesn't arise in the

200
00:14:45,230 --> 00:14:46,920
 world every day.

201
00:14:46,920 --> 00:14:49,760
 It shouldn't be negligent, we've missed time.

202
00:14:49,760 --> 00:14:54,780
 For ourselves we've wasted 2500 years not coming to see the

203
00:14:54,780 --> 00:14:55,720
 truth.

204
00:14:55,720 --> 00:14:59,050
 Even though the Buddha's teaching has been around for 2500

205
00:14:59,050 --> 00:15:00,840
 years we still haven't reached

206
00:15:00,840 --> 00:15:05,360
 the goal which means all of us have been quite negligent.

207
00:15:05,360 --> 00:15:14,450
 So now that we have found the chance we should not waste

208
00:15:14,450 --> 00:15:20,800
 our time being negligent anymore.

209
00:15:20,800 --> 00:15:32,760
 So what does it mean to hear the Dhamma?

210
00:15:32,760 --> 00:15:36,030
 The Dhamma of the Lord Buddha there are 84,000 teachings

211
00:15:36,030 --> 00:15:37,680
 according to Prananda who kept it

212
00:15:37,680 --> 00:15:45,800
 in his mind with everything the Lord Buddha taught.

213
00:15:45,800 --> 00:15:49,000
 All together it comes down to three things.

214
00:15:49,000 --> 00:15:54,110
 84,000 teachings come down to the teaching on morality

215
00:15:54,110 --> 00:15:56,520
 which means to refrain from bad

216
00:15:56,520 --> 00:16:03,710
 deeds, teaching on concentration which means to be full of

217
00:16:03,710 --> 00:16:07,520
 goodness in the heart, and the

218
00:16:07,520 --> 00:16:12,690
 teaching on wisdom which means to see clearly the truth of

219
00:16:12,690 --> 00:16:16,120
 suffering and the cause of suffering,

220
00:16:16,120 --> 00:16:23,520
 the cessation of suffering and the path which leads to the

221
00:16:23,520 --> 00:16:27,120
 cessation of suffering.

222
00:16:27,120 --> 00:16:33,120
 84,000 teachings it all comes down to only three things.

223
00:16:33,120 --> 00:16:43,120
 The Buddha's teaching all comes down to three things.

224
00:16:43,120 --> 00:16:47,980
 Morality, morality doesn't necessarily mean keeping precept

225
00:16:47,980 --> 00:16:50,000
s although the rules which

226
00:16:50,000 --> 00:16:53,080
 we keep are quite important.

227
00:16:53,080 --> 00:16:56,300
 For lay people it's important to keep five precepts at

228
00:16:56,300 --> 00:16:56,960
 least.

229
00:16:56,960 --> 00:17:02,280
 If you're practicing meditation eight precepts.

230
00:17:02,280 --> 00:17:10,320
 Novices have to keep 10, monks have to keep 227.

231
00:17:10,320 --> 00:17:12,320
 But these are simply worldly precepts.

232
00:17:12,320 --> 00:17:16,760
 These are things which create a communal harmony.

233
00:17:16,760 --> 00:17:20,600
 If we can keep these precepts they will create harmony and

234
00:17:20,600 --> 00:17:22,680
 the monastery harmony and the

235
00:17:22,680 --> 00:17:27,320
 country harmony in the world.

236
00:17:27,320 --> 00:17:34,840
 They create a sort of establishment.

237
00:17:34,840 --> 00:17:39,610
 But the morality which the Lord would have talked about was

238
00:17:39,610 --> 00:17:42,200
 a sort of morality of mind,

239
00:17:42,200 --> 00:17:53,190
 virtue of mind where the mind doesn't fall into evil states

240
00:17:53,190 --> 00:17:53,320
.

241
00:17:53,320 --> 00:17:58,170
 When we watch the foot right goes thus, left goes thus or

242
00:17:58,170 --> 00:18:01,840
 lifting, lifting, lifting, lifting,

243
00:18:01,840 --> 00:18:05,520
 lifting, lifting and so on.

244
00:18:05,520 --> 00:18:07,770
 When we guard the mind and don't let it stray from the

245
00:18:07,770 --> 00:18:09,400
 object and keep bringing it back

246
00:18:09,400 --> 00:18:15,280
 again and again to the object, this is morality.

247
00:18:15,280 --> 00:18:20,240
 It's called the morality makanga.

248
00:18:20,240 --> 00:18:24,800
 It's a factor of the path.

249
00:18:24,800 --> 00:18:28,050
 Morality is a factor of a part of the path which we have to

250
00:18:28,050 --> 00:18:28,720
 follow.

251
00:18:28,720 --> 00:18:32,670
 All together there are eight parts or we can say three

252
00:18:32,670 --> 00:18:34,240
 parts to the path.

253
00:18:34,240 --> 00:18:38,830
 This is the morality part of the path which we have to

254
00:18:38,830 --> 00:18:39,840
 follow.

255
00:18:39,840 --> 00:18:44,270
 When we guard our minds, keep it from wandering away from

256
00:18:44,270 --> 00:18:46,960
 the object, this is morality.

257
00:18:46,960 --> 00:18:50,740
 Once we guard our minds in this way, there arises

258
00:18:50,740 --> 00:18:52,600
 concentration.

259
00:18:52,600 --> 00:18:57,400
 Most people think of concentration as fixing on one thing.

260
00:18:57,400 --> 00:19:00,380
 This is a kind of concentration but it's a concentration in

261
00:19:00,380 --> 00:19:01,800
 the world when our minds are

262
00:19:01,800 --> 00:19:06,840
 fixed and focused on a single object.

263
00:19:06,840 --> 00:19:08,440
 But this is impossible.

264
00:19:08,440 --> 00:19:12,790
 It's impossible for this kind of concentration to lead to

265
00:19:12,790 --> 00:19:15,240
 wisdom and the wisdom which the

266
00:19:15,240 --> 00:19:20,120
 Lord Buddha needed us to see which is impermanent.

267
00:19:20,120 --> 00:19:23,780
 Until we can see impermanence, it's clear that we're not

268
00:19:23,780 --> 00:19:26,040
 meditating correctly according

269
00:19:26,040 --> 00:19:28,280
 to the Lord Buddha's teaching.

270
00:19:28,280 --> 00:19:31,290
 We're still only practicing with the Lord Buddha called sam

271
00:19:31,290 --> 00:19:32,760
atha, which is a good thing

272
00:19:32,760 --> 00:19:39,580
 but it's not correct as far as allowing us to see the truth

273
00:19:39,580 --> 00:19:42,680
 of suffering as well.

274
00:19:42,680 --> 00:19:46,540
 We have to come to have fixed concentration on everything

275
00:19:46,540 --> 00:19:48,440
 which arises and ceases, on

276
00:19:48,440 --> 00:19:54,250
 those things which are impermanent, those things which are

277
00:19:54,250 --> 00:19:56,840
 real inside of ourself.

278
00:19:56,840 --> 00:20:00,720
 We watch the foot moving or we watch the belly rising and

279
00:20:00,720 --> 00:20:03,000
 we see rise, being falling.

280
00:20:03,000 --> 00:20:05,040
 The mind fixes on that object.

281
00:20:05,040 --> 00:20:07,880
 Why do we use the word to acknowledge?

282
00:20:07,880 --> 00:20:12,980
 The mind fixes when we use the word as an aid, it fixes

283
00:20:12,980 --> 00:20:14,240
 someone.

284
00:20:14,240 --> 00:20:19,140
 It's like when you practice samatha you need to say bhudho

285
00:20:19,140 --> 00:20:19,840
 bhudho bhudho.

286
00:20:19,840 --> 00:20:24,040
 Tamo tamo tamo, sanko samo, and so on.

287
00:20:24,040 --> 00:20:27,360
 When you practice vipassana you also have to fix but you

288
00:20:27,360 --> 00:20:29,520
 have to fix on reality.

289
00:20:29,520 --> 00:20:38,640
 That's the only difference.

290
00:20:38,640 --> 00:20:40,160
 Concentration means you have to fix the mind.

291
00:20:40,160 --> 00:20:43,640
 You use this word like a mantra almost.

292
00:20:43,640 --> 00:20:47,130
 To fix on the ultimate reality, to keep the mind from

293
00:20:47,130 --> 00:20:48,120
 wandering.

294
00:20:48,120 --> 00:20:52,130
 Because normally the mind is watching the phenomena arise

295
00:20:52,130 --> 00:20:53,080
 and cease.

296
00:20:53,080 --> 00:21:05,330
 But it's watching with wrong view, with conceit, and with

297
00:21:05,330 --> 00:21:07,840
 craving.

298
00:21:07,840 --> 00:21:13,800
 And this is me, this is mine, this is under my control and

299
00:21:13,800 --> 00:21:14,720
 so on.

300
00:21:14,720 --> 00:21:19,680
 When we say to ourselves, rising, falling, the mind is

