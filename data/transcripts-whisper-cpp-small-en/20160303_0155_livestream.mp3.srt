1
00:00:00,000 --> 00:00:09,000
 Good evening everyone.

2
00:00:09,000 --> 00:00:24,000
 I'm casting live March 2nd, 2016.

3
00:00:24,000 --> 00:00:33,500
 So I've been sick for the past days, last night, last night

4
00:00:33,500 --> 00:00:36,000
 the power went out.

5
00:00:36,000 --> 00:00:40,000
 So no internet, no not the power, the internet went out.

6
00:00:40,000 --> 00:00:51,000
 The power was on, the internet went out.

7
00:00:51,000 --> 00:01:03,000
 But probably I was still too sick to broadcasting.

8
00:01:03,000 --> 00:01:18,000
 The Buddha said freedom from sickness is the greatest gain.

9
00:01:18,000 --> 00:01:21,000
 Which is interesting.

10
00:01:21,000 --> 00:01:36,330
 At face value it seems a little odd. Of course you can't

11
00:01:36,330 --> 00:01:37,370
 deny the fact that when you're sick it doesn't matter what

12
00:01:37,370 --> 00:01:42,000
 you get, what else you get, what else you've got.

13
00:01:42,000 --> 00:01:53,100
 You can be rich, you can be surrounded by friends, you can

14
00:01:53,100 --> 00:01:57,000
 have guards at every door.

15
00:01:57,000 --> 00:02:14,000
 But when you're sick it all means nothing. The most

16
00:02:14,000 --> 00:02:14,000
 comfortable bed can become a torture chamber.

17
00:02:14,000 --> 00:02:28,460
 The greatest food can become poison. You have warmth and it

18
00:02:28,460 --> 00:02:35,000
's too hot or you can be freezing cold in a hot room.

19
00:02:35,000 --> 00:02:45,130
 You can be boiling up in a cold room. Sickness will do that

20
00:02:45,130 --> 00:02:46,000
.

21
00:02:46,000 --> 00:02:50,250
 But there's more to that, more to this saying than just

22
00:02:50,250 --> 00:02:51,000
 that.

23
00:02:51,000 --> 00:03:03,050
 Freedom from sickness means more than just physical

24
00:03:03,050 --> 00:03:09,000
 sickness. We're all sick.

25
00:03:09,000 --> 00:03:22,060
 We're all sick, we have a body and mind. We've got to eat,

26
00:03:22,060 --> 00:03:32,580
 we've got to drink, we've got to breathe. These are all

27
00:03:32,580 --> 00:03:38,000
 sicknesses.

28
00:03:38,000 --> 00:03:43,020
 So, warning, tonight's talk might be a little depressing.

29
00:03:43,020 --> 00:04:00,000
 Or humorous depending on how Buddhist you are.

30
00:04:00,000 --> 00:04:07,000
 I don't even know if anyone's listening. Maybe you're all

31
00:04:07,000 --> 00:04:14,000
 gone. Been away for so many days.

32
00:04:14,000 --> 00:04:18,510
 Anyway this gets recorded so someone will hear it

33
00:04:18,510 --> 00:04:27,000
 eventually. Maybe after I'm dead.

34
00:04:27,000 --> 00:04:35,000
 We have to urinate and defecate. These are sicknesses.

35
00:04:35,000 --> 00:04:42,760
 Sickness. Try and stop yourself from defecating. Try and

36
00:04:42,760 --> 00:04:51,000
 stop yourself from urinating.

37
00:04:51,000 --> 00:04:54,420
 You know, but those are maybe pretty extreme. We don't

38
00:04:54,420 --> 00:04:58,070
 normally think of them as sicknesses. So put them aside,

39
00:04:58,070 --> 00:04:59,000
 that's fine.

40
00:04:59,000 --> 00:05:05,000
 Still have the sicknesses of getting cold and flu.

41
00:05:05,000 --> 00:05:11,200
 We have the Buddha in the Girimananda Sutta, he detailed

42
00:05:11,200 --> 00:05:18,070
 all these different kinds of sickness. Everyone should read

43
00:05:18,070 --> 00:05:21,000
 the Girimananda Sutta.

44
00:05:21,000 --> 00:05:24,000
 That's really a good time.

45
00:05:24,000 --> 00:05:34,450
 There's one part about various types of sicknesses. There's

46
00:05:34,450 --> 00:05:43,740
 eye sicknesses, ear sicknesses, nose sicknesses, body

47
00:05:43,740 --> 00:05:49,000
 sicknesses.

48
00:05:49,000 --> 00:06:03,000
 There's all kinds of diseases.

49
00:06:03,000 --> 00:06:11,390
 And really if you've got any of these, it doesn't really

50
00:06:11,390 --> 00:06:16,000
 matter what else you've got.

51
00:06:16,000 --> 00:06:20,470
 But even that isn't really freedom from sickness. Even

52
00:06:20,470 --> 00:06:25,000
 freedom from many diseases that we can get.

53
00:06:25,000 --> 00:06:29,510
 That we recognize as diseases isn't true freedom, right?

54
00:06:29,510 --> 00:06:33,000
 Because then we have mental sickness.

55
00:06:33,000 --> 00:06:37,470
 And most people think that most of us think that most of

56
00:06:37,470 --> 00:06:41,940
 the rest of the people around them are free from mental

57
00:06:41,940 --> 00:06:43,000
 illness.

58
00:06:43,000 --> 00:06:48,190
 But I bet if not most, many people will admit to themselves

59
00:06:48,190 --> 00:06:52,000
 that they have mental illness themselves.

60
00:06:52,000 --> 00:06:56,160
 Even if they won't admit it to others or ever think about

61
00:06:56,160 --> 00:06:58,000
 going to see a doctor.

62
00:06:58,000 --> 00:07:02,000
 But we look around at everybody else and we think, well,

63
00:07:02,000 --> 00:07:06,000
 they look normal, right?

64
00:07:06,000 --> 00:07:12,520
 It's funny, you can barely, if you're not looking carefully

65
00:07:12,520 --> 00:07:16,000
, you can't tell the difference.

66
00:07:16,000 --> 00:07:20,260
 In between people who, even people who are taking

67
00:07:20,260 --> 00:07:24,000
 medication for mental illness.

68
00:07:24,000 --> 00:07:33,500
 They're almost indistinguishable. Very hard to tell the

69
00:07:33,500 --> 00:07:36,000
 difference.

70
00:07:36,000 --> 00:07:39,000
 But even taking medication doesn't solve the problem.

71
00:07:39,000 --> 00:07:43,010
 And taking medication, a person on medication still knows

72
00:07:43,010 --> 00:07:44,000
 they've got a problem.

73
00:07:44,000 --> 00:07:48,000
 They're not fixing it. They're just covering it up.

74
00:07:48,000 --> 00:07:51,450
 Most anyway, they do know some people who think that

75
00:07:51,450 --> 00:07:54,000
 medication solves their problems.

76
00:07:54,000 --> 00:08:00,000
 Well, that's another argument.

77
00:08:00,000 --> 00:08:09,000
 But if I just go, just talk about the mental illness.

78
00:08:09,000 --> 00:08:17,000
 Well, first of all, I should back up and that's detailed.

79
00:08:17,000 --> 00:08:21,000
 Go over the standard enumeration for sickness.

80
00:08:21,000 --> 00:08:29,780
 We've enumerated four types. Four types of sickness based

81
00:08:29,780 --> 00:08:33,000
 on their origin.

82
00:08:33,000 --> 00:08:43,000
 We've got sickness from the environment.

83
00:08:43,000 --> 00:08:48,000
 We've got sickness that comes from food.

84
00:08:48,000 --> 00:08:52,000
 We've got sickness that comes from the mind.

85
00:08:52,000 --> 00:08:56,000
 And sickness that comes from karma.

86
00:08:56,000 --> 00:08:58,250
 Maybe not in that order. Actually, this may not have come

87
00:08:58,250 --> 00:09:00,000
 from the Buddha. I can't remember.

88
00:09:00,000 --> 00:09:05,160
 Maybe in the commentary. I'm pretty sure it's from the

89
00:09:05,160 --> 00:09:06,000
 Buddha.

90
00:09:06,000 --> 00:09:19,000
 Definitely Buddhist.

91
00:09:19,000 --> 00:09:24,000
 So the first two are more physical.

92
00:09:24,000 --> 00:09:28,000
 Sickness comes from the environment.

93
00:09:28,000 --> 00:09:32,000
 Normally we understand this is viruses,

94
00:09:32,000 --> 00:09:37,000
 bugs that you get when you touch the same doorknob that

95
00:09:37,000 --> 00:09:39,000
 someone else had to stick to the side of

96
00:09:39,000 --> 00:09:50,000
 and someone coughs into your face.

97
00:09:50,000 --> 00:09:53,000
 Any kind of communicable disease.

98
00:09:53,000 --> 00:09:58,000
 But it can also be radiation poisoning, lead or water

99
00:09:58,000 --> 00:09:58,000
 poisoning.

100
00:09:58,000 --> 00:10:02,000
 That's just maybe from food.

101
00:10:02,000 --> 00:10:06,000
 Anything from the environment.

102
00:10:06,000 --> 00:10:12,000
 Now let's say water poisoning is also from the environment.

103
00:10:12,000 --> 00:10:16,100
 These people in Michigan, they think about their water

104
00:10:16,100 --> 00:10:17,000
 poisoning.

105
00:10:17,000 --> 00:10:23,000
 Like the government didn't do anything about it.

106
00:10:23,000 --> 00:10:35,000
 Very bad karma.

107
00:10:35,000 --> 00:10:40,000
 All that kind of sickness comes from the environment.

108
00:10:40,000 --> 00:10:43,000
 Even I would say some kind of sickness that comes from food

109
00:10:43,000 --> 00:10:43,000
,

110
00:10:43,000 --> 00:10:47,000
 you could call environmental sickness instead.

111
00:10:47,000 --> 00:10:50,990
 Like a lot of cancer and stuff probably comes from

112
00:10:50,990 --> 00:10:52,000
 chemicals,

113
00:10:52,000 --> 00:10:59,000
 pesticides that gets into food, gets into plastic and...

114
00:10:59,000 --> 00:11:04,000
 (coughing)

115
00:11:04,000 --> 00:11:08,990
 Our meat is tainted in people who eat liver that has

116
00:11:08,990 --> 00:11:11,000
 mercury, I think.

117
00:11:11,000 --> 00:11:15,000
 Or fish that has all sorts of mercury and stuff.

118
00:11:15,000 --> 00:11:18,000
 That's environmental poisoning.

119
00:11:18,000 --> 00:11:20,000
 But food poisoning.

120
00:11:20,000 --> 00:11:24,000
 Poisoning from food or sickness that comes from food.

121
00:11:24,000 --> 00:11:29,460
 I suppose bacteria could be included there, but I would say

122
00:11:29,460 --> 00:11:32,000
 this mostly deals with...

123
00:11:32,000 --> 00:11:35,000
 (coughing)

124
00:11:35,000 --> 00:11:44,150
 Over-eating or eating fatty foods, you know, diabetes,

125
00:11:44,150 --> 00:11:47,000
 heart disease.

126
00:11:47,000 --> 00:11:52,000
 Obesity.

127
00:11:52,000 --> 00:11:56,000
 These aren't so important.

128
00:11:56,000 --> 00:12:00,000
 It's much more important than the third one, mind.

129
00:12:00,000 --> 00:12:03,000
 Sickness that comes from the mind.

130
00:12:03,000 --> 00:12:09,000
 Because your body can be sick, but your mind can be okay.

131
00:12:09,000 --> 00:12:13,010
 On the other hand, if your mind is sick, your body is not

132
00:12:13,010 --> 00:12:16,000
 likely to stay okay.

133
00:12:16,000 --> 00:12:20,000
 Mind can hurt the body.

134
00:12:20,000 --> 00:12:22,000
 Can the body hurt the mind?

135
00:12:22,000 --> 00:12:27,000
 I suppose, but not directly.

136
00:12:27,000 --> 00:12:29,000
 Depends on the mind.

137
00:12:29,000 --> 00:12:34,000
 The mind is like an open sore.

138
00:12:34,000 --> 00:12:41,000
 And then the body can harm the mind.

139
00:12:41,000 --> 00:12:43,000
 The mind has to be reactionary.

140
00:12:43,000 --> 00:12:46,000
 It has to be reactive.

141
00:12:46,000 --> 00:12:51,000
 The mind is strong or wise.

142
00:12:51,000 --> 00:12:58,000
 The mind is free from clinging.

143
00:12:58,000 --> 00:13:02,000
 Shh.

144
00:13:02,000 --> 00:13:09,000
 And the body has no effect.

145
00:13:09,000 --> 00:13:16,040
 Even if the body is sick and in pain, and even if you're

146
00:13:16,040 --> 00:13:27,000
 dying, it can be at peace.

147
00:13:27,000 --> 00:13:29,000
 This is what we aim to accomplish.

148
00:13:29,000 --> 00:13:33,130
 This is actually related to the fourth one, because

149
00:13:33,130 --> 00:13:35,360
 sickness that comes from karma actually comes from the mind

150
00:13:35,360 --> 00:13:36,000
 in the first place.

151
00:13:36,000 --> 00:13:41,000
 But it's indirect.

152
00:13:41,000 --> 00:13:44,000
 So the mind makes the body sick.

153
00:13:44,000 --> 00:13:47,000
 That's karma.

154
00:13:47,000 --> 00:13:51,000
 You become an evil and twisted person.

155
00:13:51,000 --> 00:13:54,000
 You get sick seemingly for no reason.

156
00:13:54,000 --> 00:13:59,460
 Some people have very strange sicknesses that doctors can't

157
00:13:59,460 --> 00:14:02,000
 figure out the cause for.

158
00:14:02,000 --> 00:14:04,000
 Hey, who knows?

159
00:14:04,000 --> 00:14:12,090
 We would argue that maybe, just maybe some of them are

160
00:14:12,090 --> 00:14:15,000
 caused by karma.

161
00:14:15,000 --> 00:14:20,000
 Maybe some cancer is caused by this as well.

162
00:14:20,000 --> 00:14:25,000
 Cancer is weird. Sickness just seems to pop up.

163
00:14:25,000 --> 00:14:27,000
 And sometimes it doesn't seem to go away.

164
00:14:27,000 --> 00:14:31,150
 Get rid of it and then just don't know where it pops up

165
00:14:31,150 --> 00:14:32,000
 again.

166
00:14:32,000 --> 00:14:36,000
 Mysterious.

167
00:14:36,000 --> 00:14:41,000
 It's almost as though you were born with some pre-

168
00:14:41,000 --> 00:14:46,000
 pre-

169
00:14:46,000 --> 00:15:00,000
 pre-inclination towards cancer.

170
00:15:00,000 --> 00:15:02,000
 So the mind is the most important.

171
00:15:02,000 --> 00:15:07,000
 When we talk about freedom from sickness, we really mean

172
00:15:07,000 --> 00:15:14,410
 the state of being free from the fires of greed and anger

173
00:15:14,410 --> 00:15:15,000
 and delusion.

174
00:15:15,000 --> 00:15:19,000
 The fevers.

175
00:15:19,000 --> 00:15:30,000
 The fevers of defilement that inflame the mind.

176
00:15:30,000 --> 00:15:34,000
 It's not easy to overcome.

177
00:15:34,000 --> 00:15:39,000
 I mean, physical sickness, what's it going to last for?

178
00:15:39,000 --> 00:15:42,000
 What's the worst physical sickness going to last for?

179
00:15:42,000 --> 00:15:48,590
 Well, it doesn't last more than a year, five years, the

180
00:15:48,590 --> 00:15:54,000
 outside, ten years, twenty years, fifty years.

181
00:15:54,000 --> 00:15:57,930
 There's no sickness that lasts a hundred years, not that I

182
00:15:57,930 --> 00:15:59,000
 know of.

183
00:15:59,000 --> 00:16:03,000
 Except for life, I guess life sometimes lasts.

184
00:16:03,000 --> 00:16:07,000
 We call that a sickness.

185
00:16:07,000 --> 00:16:14,000
 A sickness of hunger, a sickness of thirst, these last.

186
00:16:14,000 --> 00:16:16,000
 But beyond that, that's it.

187
00:16:16,000 --> 00:16:22,000
 A sickness in the mind is something we carry with us.

188
00:16:22,000 --> 00:16:27,000
 Carry with us to hell, carry with us to animal realms.

189
00:16:27,000 --> 00:16:31,000
 Carry with us from life to life.

190
00:16:31,000 --> 00:16:35,010
 I don't really complain about how hard it is to get rid of

191
00:16:35,010 --> 00:16:38,470
 these sickness, man. I come to meditate and oh, I've been

192
00:16:38,470 --> 00:16:40,000
 practicing for a week already.

193
00:16:40,000 --> 00:16:43,000
 Why am I still getting angry?

194
00:16:43,000 --> 00:16:45,950
 Why am I still getting, why am I still attached to all

195
00:16:45,950 --> 00:16:47,000
 these things?

196
00:16:47,000 --> 00:16:55,000
 I've been practicing for a year.

197
00:16:55,000 --> 00:17:01,000
 Well, I mean, not to say that you have to equal it out.

198
00:17:01,000 --> 00:17:05,250
 You have to somehow practice an equal number of years as

199
00:17:05,250 --> 00:17:10,000
 you've been greedy or angry or deluded.

200
00:17:10,000 --> 00:17:13,000
 But you have to keep things in proportion.

201
00:17:13,000 --> 00:17:16,000
 We're building new habits.

202
00:17:16,000 --> 00:17:27,000
 The new habits are stronger because they're more immediate.

203
00:17:27,000 --> 00:17:39,000
 But they're also weaker because they're only recent.

204
00:17:39,000 --> 00:17:49,020
 So it's important not to lose hope, to not, to look down

205
00:17:49,020 --> 00:17:53,000
 upon the fact that we've been working hard.

206
00:17:53,000 --> 00:17:55,000
 We shouldn't ignore that.

207
00:17:55,000 --> 00:17:59,000
 Yes, we work hard.

208
00:17:59,000 --> 00:18:05,510
 And that work isn't in vain. But we also worked hard at

209
00:18:05,510 --> 00:18:09,000
 being greedy and angry and deluded.

210
00:18:09,000 --> 00:18:13,440
 We've done a good job cultivating those. We've had a lot

211
00:18:13,440 --> 00:18:16,000
 more practice at that.

212
00:18:16,000 --> 00:18:23,560
 So that's all we need is practice, and practice and

213
00:18:23,560 --> 00:18:25,000
 practice.

214
00:18:25,000 --> 00:18:32,270
 Every moment, every moment of mindfulness that's feeding

215
00:18:32,270 --> 00:18:36,000
 into the habit of mindfulness.

216
00:18:36,000 --> 00:18:45,000
 We can always start. Never do it.

217
00:18:45,000 --> 00:18:52,440
 Anyway, so that sickness, hopefully by tomorrow the cough

218
00:18:52,440 --> 00:18:53,000
 will be gone.

219
00:18:53,000 --> 00:18:57,000
 We'll have a little more energy.

220
00:18:57,000 --> 00:19:02,000
 But I'm also very behind in school.

221
00:19:02,000 --> 00:19:11,000
 This crazy idea to go back to school and study things.

222
00:19:11,000 --> 00:19:14,000
 I'm not sure how that's working out now.

223
00:19:14,000 --> 00:19:20,470
 But there's a peace symposium at McMaster that I'm helping

224
00:19:20,470 --> 00:19:22,000
 organize.

225
00:19:22,000 --> 00:19:26,000
 I like peace studies. I like encouraging peace.

226
00:19:26,000 --> 00:19:30,000
 Let me see how that goes.

227
00:19:30,000 --> 00:19:35,000
 And studying the Lotus Sutra.

228
00:19:35,000 --> 00:19:42,680
 I'm going to try to do an essay on the polemic aspects of

229
00:19:42,680 --> 00:19:45,000
 the Lotus Sutra.

230
00:19:45,000 --> 00:19:49,000
 Because I like getting in trouble.

231
00:19:49,000 --> 00:19:53,000
 My professor actually agrees with me on a very good topic.

232
00:19:53,000 --> 00:19:56,640
 What's interesting is the Lotus Sutra never really got,

233
00:19:56,640 --> 00:20:02,000
 apparently never really gained traction in India.

234
00:20:02,000 --> 00:20:05,000
 But gained a lot of traction in East Asia.

235
00:20:05,000 --> 00:20:12,000
 And what's interesting about that is,

236
00:20:12,000 --> 00:20:17,520
 that it was very much directed at mainstream Indian

237
00:20:17,520 --> 00:20:19,000
 Buddhism.

238
00:20:19,000 --> 00:20:24,130
 So the question is, how is it that when it moved to China,

239
00:20:24,130 --> 00:20:28,000
 where there was no Buddhism, it became a hit?

240
00:20:28,000 --> 00:20:30,880
 And I guess maybe what I'm going to look at is whether it

241
00:20:30,880 --> 00:20:33,000
 became a hit for the wrong reasons.

242
00:20:33,000 --> 00:20:38,370
 Like how we reinterpret religious texts in ways that they

243
00:20:38,370 --> 00:20:42,000
 were never meant to be interpreted.

244
00:20:42,000 --> 00:20:45,530
 Because I'm told that I'm interpreting the Lotus Sutra

245
00:20:45,530 --> 00:20:47,000
 completely wrong.

246
00:20:47,000 --> 00:20:49,000
 Which is funny.

247
00:20:49,000 --> 00:20:51,490
 Because I wonder if the people who are telling me that are

248
00:20:51,490 --> 00:20:55,730
 actually, well, you know, we each interpret things our own

249
00:20:55,730 --> 00:20:56,000
 way.

250
00:20:56,000 --> 00:20:58,000
 And who's to say what is the right way.

251
00:20:58,000 --> 00:21:01,000
 But maybe that there is a clear.

252
00:21:01,000 --> 00:21:06,000
 Take for example the Old Testament.

253
00:21:06,000 --> 00:21:10,330
 And how the Old Testament was interpreted by the New

254
00:21:10,330 --> 00:21:13,000
 Testament authors.

255
00:21:13,000 --> 00:21:19,000
 To say that, to be, to prophesize Jesus Christ.

256
00:21:19,000 --> 00:21:22,990
 The Old Testament, according to Christians, the Christian

257
00:21:22,990 --> 00:21:25,000
 authors of the New Testament,

258
00:21:25,000 --> 00:21:28,000
 was supposed to prophesize the coming of Jesus.

259
00:21:28,000 --> 00:21:31,000
 There's a lot of passages that must have to do that.

260
00:21:31,000 --> 00:21:34,950
 But according to biblical scholars, that's almost

261
00:21:34,950 --> 00:21:36,000
 ridiculous.

262
00:21:36,000 --> 00:21:39,000
 Or it is a ridiculous sort of claim.

263
00:21:39,000 --> 00:21:45,750
 I mean, unless you are willfully ignorant in terms of

264
00:21:45,750 --> 00:21:50,300
 saying, you know, well, each interpretation is equally

265
00:21:50,300 --> 00:21:51,000
 valid.

266
00:21:51,000 --> 00:21:53,000
 It's just not true.

267
00:21:53,000 --> 00:21:57,000
 The Old Testament was clearly written.

268
00:21:57,000 --> 00:22:00,820
 And what we call the Old Testament was clearly written by a

269
00:22:00,820 --> 00:22:03,000
 diverse group of authors.

270
00:22:03,000 --> 00:22:11,130
 But it was also written for clearly political and specific

271
00:22:11,130 --> 00:22:12,000
 reasons.

272
00:22:12,000 --> 00:22:14,840
 I mean, the point being, there's no way that it had

273
00:22:14,840 --> 00:22:18,710
 anything to do with prophesizing the coming of Jesus Christ

274
00:22:18,710 --> 00:22:19,000
.

275
00:22:19,000 --> 00:22:24,000
 Just an example, believe it or not, that's what.

276
00:22:24,000 --> 00:22:29,920
 So the question is whether the Lotus Sutra is in a similar

277
00:22:29,920 --> 00:22:31,000
 state.

278
00:22:31,000 --> 00:22:39,010
 I'd just like to look at, I guess, how the Chinese teachers

279
00:22:39,010 --> 00:22:41,000
 did, and maybe even do today,

280
00:22:41,000 --> 00:22:44,900
 interpret those passages of the Lotus Sutra that to me as

281
00:22:44,900 --> 00:22:47,000
 an Indian Buddhist scholar,

282
00:22:47,000 --> 00:22:52,100
 clearly, clearly, clearly aimed at denouncing Indian

283
00:22:52,100 --> 00:22:58,000
 Buddhism, which was totally unbeknownst.

284
00:22:58,000 --> 00:23:02,760
 Totally unknown to the people who came in contact with the

285
00:23:02,760 --> 00:23:09,000
 Lotus Sutra, I think.

286
00:23:09,000 --> 00:23:13,610
 But at any rate, sorry, I don't mean that I'm confused, it

287
00:23:13,610 --> 00:23:18,120
's just that it's not that it was unknown, it's that it had

288
00:23:18,120 --> 00:23:19,000
 no place.

289
00:23:19,000 --> 00:23:23,050
 So when the Lotus Sutra and I guess the teachers like it

290
00:23:23,050 --> 00:23:28,000
 came to it, came to China, they weren't reacting to it.

291
00:23:28,000 --> 00:23:32,450
 They could be taken completely at face value, which I guess

292
00:23:32,450 --> 00:23:37,000
 my argument is they weren't meant to be taken at face value

293
00:23:37,000 --> 00:23:37,000
.

294
00:23:37,000 --> 00:23:41,000
 Or they weren't intended.

295
00:23:41,000 --> 00:23:45,000
 Or they weren't expected to be taken at face value.

296
00:23:45,000 --> 00:23:54,700
 They were expected to be, they were expected to create

297
00:23:54,700 --> 00:24:02,710
 controversy because they were stepping into a realm of

298
00:24:02,710 --> 00:24:06,000
 Buddhist Orthodoxy.

299
00:24:06,000 --> 00:24:12,270
 So I don't know where that came from, it must be the

300
00:24:12,270 --> 00:24:15,000
 sickness talking.

301
00:24:15,000 --> 00:24:18,000
 Just kidding.

302
00:24:18,000 --> 00:24:28,000
 Anyway, people studying Latin as well.

303
00:24:28,000 --> 00:24:33,080
 Studying Latin while you're sick is not the most fruitful

304
00:24:33,080 --> 00:24:34,000
 thing.

305
00:24:34,000 --> 00:24:36,000
 Anyway, I'm going to go.

306
00:24:36,000 --> 00:24:38,000
 That's all for tonight.

307
00:24:38,000 --> 00:24:40,290
 Thank you all for tuning in and wishing you all good

308
00:24:40,290 --> 00:24:41,000
 practice.

309
00:24:41,000 --> 00:24:43,000
 Good night.

