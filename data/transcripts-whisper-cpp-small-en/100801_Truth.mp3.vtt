WEBVTT

00:00:00.000 --> 00:00:11.100
 Today's talk, I thought I would talk about truth.

00:00:11.100 --> 00:00:19.580
 Because I think this is a fairly relevant topic for a place

00:00:19.580 --> 00:00:20.940
 like Second Life.

00:00:20.940 --> 00:00:27.350
 Because we're dealing with a lot of illusion and potential

00:00:27.350 --> 00:00:29.260
 falsehood.

00:00:29.260 --> 00:00:38.250
 So I think it's an important subject and something for us

00:00:38.250 --> 00:00:41.780
 all to think about.

00:00:41.780 --> 00:00:47.110
 We come into a place like Second Life and I think there are

00:00:47.110 --> 00:00:56.480
 two related but different

00:00:56.480 --> 00:01:02.860
 parts of this place and one is the illusion.

00:01:02.860 --> 00:01:07.240
 One is illusion and the other is falsehood.

00:01:07.240 --> 00:01:11.560
 So illusion can often be innocuous.

00:01:11.560 --> 00:01:21.040
 It doesn't have to be an immoral thing.

00:01:21.040 --> 00:01:26.640
 There's a lot of illusion here that you might say is in

00:01:26.640 --> 00:01:29.180
 many ways positive.

00:01:29.180 --> 00:01:31.520
 The illusion of being able to see each other for instance.

00:01:31.520 --> 00:01:37.320
 The illusion of us sitting here together can in many ways

00:01:37.320 --> 00:01:38.880
 have a benefit.

00:01:38.880 --> 00:01:44.360
 It can give us this sense of community and can really

00:01:44.360 --> 00:01:48.080
 direct our attention towards the

00:01:48.080 --> 00:01:50.440
 task at hand.

00:01:50.440 --> 00:01:56.340
 It allows us to envision things and to often create and

00:01:56.340 --> 00:01:59.760
 work out things in a way that might

00:01:59.760 --> 00:02:03.360
 otherwise not be possible.

00:02:03.360 --> 00:02:06.160
 Illusion is something which we have in our daily life.

00:02:06.160 --> 00:02:12.250
 The illusion of solidity is all around us and it's totally

00:02:12.250 --> 00:02:14.560
 necessary if we didn't perceive

00:02:14.560 --> 00:02:22.060
 things as an object as what they were, if we didn't put

00:02:22.060 --> 00:02:25.600
 things together as being this

00:02:25.600 --> 00:02:30.500
 entity or that entity in terms of food or in terms of

00:02:30.500 --> 00:02:33.480
 dangers then we wouldn't be able

00:02:33.480 --> 00:02:39.760
 to survive.

00:02:39.760 --> 00:02:44.670
 So Buddhism is very much about truth but doesn't deny the

00:02:44.670 --> 00:02:47.440
 importance of concepts, the importance

00:02:47.440 --> 00:02:56.960
 of illusion or the importance of the construct.

00:02:56.960 --> 00:03:00.880
 So in many ways second life is simply a construct.

00:03:00.880 --> 00:03:04.630
 The problem of course with second life comes when we get

00:03:04.630 --> 00:03:06.100
 into falsehoods.

00:03:06.100 --> 00:03:09.580
 When we start to become something that we're not, when we

00:03:09.580 --> 00:03:11.520
 hide things that we are, when

00:03:11.520 --> 00:03:16.000
 we start running away from reality and trying to portray

00:03:16.000 --> 00:03:18.840
 ourselves as something other than

00:03:18.840 --> 00:03:22.200
 what we are.

00:03:22.200 --> 00:03:29.200
 And I'd like to explore this topic a little bit.

00:03:29.200 --> 00:03:33.290
 I think it's open to some debate but I think there are some

00:03:33.290 --> 00:03:35.600
 moral issues here and by moral

00:03:35.600 --> 00:03:39.320
 I don't mean anything that's objectionable.

00:03:39.320 --> 00:03:41.610
 I'm never talking about something that's objectionable to

00:03:41.610 --> 00:03:42.320
 other people.

00:03:42.320 --> 00:03:47.520
 I'm talking about something which causes suffering and

00:03:47.520 --> 00:03:50.440
 mental upset for the individual who undertakes

00:03:50.440 --> 00:03:51.440
 it.

00:03:51.440 --> 00:03:55.050
 Things are immoral in Buddhism not because of what other

00:03:55.050 --> 00:03:57.040
 people think of you or how other

00:03:57.040 --> 00:03:59.090
 people judge you but they're immoral because of the

00:03:59.090 --> 00:04:00.800
 suffering they cause for you, because

00:04:00.800 --> 00:04:04.130
 of how they warp your mind, because of how they take you

00:04:04.130 --> 00:04:05.960
 away from reality, take you

00:04:05.960 --> 00:04:09.200
 away from the truth.

00:04:09.200 --> 00:04:11.660
 So a lot of it seems innocuous.

00:04:11.660 --> 00:04:16.840
 It seems to be something that's just a game, just for fun.

00:04:16.840 --> 00:04:20.860
 And it's an interesting thing about games that they are

00:04:20.860 --> 00:04:23.120
 generally not innocuous.

00:04:23.120 --> 00:04:27.300
 If you think back to when we used to play board games, when

00:04:27.300 --> 00:04:29.280
 we used to play Monopoly,

00:04:29.280 --> 00:04:33.400
 I don't know if anyone still plays Monopoly anymore.

00:04:33.400 --> 00:04:37.010
 Probably we now play Virtual Monopoly or something like

00:04:37.010 --> 00:04:37.600
 that.

00:04:37.600 --> 00:04:39.680
 3D Monopoly, I don't know.

00:04:39.680 --> 00:04:43.400
 No, I'm sure there are still people who play it.

00:04:43.400 --> 00:04:51.110
 Back when we used to play Monopoly we would get very

00:04:51.110 --> 00:04:52.640
 competitive.

00:04:52.640 --> 00:04:54.440
 It's certainly not innocuous.

00:04:54.440 --> 00:04:56.040
 Sports even more so.

00:04:56.040 --> 00:04:57.520
 It becomes a bravado thing.

00:04:57.520 --> 00:05:00.780
 I remember when we used to play sports and it was very mach

00:05:00.780 --> 00:05:02.520
o and the adrenaline gets

00:05:02.520 --> 00:05:04.520
 going and so on.

00:05:04.520 --> 00:05:07.620
 And you see, I'm from Canada so we have a big hockey

00:05:07.620 --> 00:05:09.720
 tradition up there and it's certainly

00:05:09.720 --> 00:05:12.840
 not just a game.

00:05:12.840 --> 00:05:18.400
 They beat each other and it's part of the game.

00:05:18.400 --> 00:05:20.830
 So these are good examples that we should then bring over

00:05:20.830 --> 00:05:22.080
 into something like Second

00:05:22.080 --> 00:05:23.660
 Life.

00:05:23.660 --> 00:05:26.800
 When we start looking at it as a game or when we use that

00:05:26.800 --> 00:05:27.800
 as an excuse.

00:05:27.800 --> 00:05:30.680
 Most of us, I think, don't see it simply as a game.

00:05:30.680 --> 00:05:34.080
 We see it as a life, as a part of our lives.

00:05:34.080 --> 00:05:48.370
 But I think in many ways we rationalize our falsehoods in

00:05:48.370 --> 00:05:51.480
 this way.

00:05:51.480 --> 00:05:57.240
 We use the game mentality or we evoke this.

00:05:57.240 --> 00:06:03.200
 Oh, it's just for pretend.

00:06:03.200 --> 00:06:07.400
 And again, the problem here is that it can often take us

00:06:07.400 --> 00:06:09.600
 away from what is real and it

00:06:09.600 --> 00:06:13.280
 makes things easy.

00:06:13.280 --> 00:06:16.360
 It allows us to escape many of our problems.

00:06:16.360 --> 00:06:18.040
 So we have problems in real life.

00:06:18.040 --> 00:06:22.340
 We have to look at it on Second Life when none of those

00:06:22.340 --> 00:06:24.000
 problems exist.

00:06:24.000 --> 00:06:32.480
 And I think this is indicative of, or not indicative, this

00:06:32.480 --> 00:06:34.760
 exists in all parts of our

00:06:34.760 --> 00:06:38.520
 discourse with our interaction with the computer.

00:06:38.520 --> 00:06:46.430
 The computer is a tool that has become a means of getting

00:06:46.430 --> 00:06:52.200
 very quick and very direct pleasure.

00:06:52.200 --> 00:06:57.440
 We have this very easy gratification system.

00:06:57.440 --> 00:07:02.040
 And I think it's exemplified by the iPhone or the iPad.

00:07:02.040 --> 00:07:04.340
 Now I've used them a little bit, but I'm not thinking of

00:07:04.340 --> 00:07:05.160
 when I use them.

00:07:05.160 --> 00:07:09.320
 I'm looking at how it's arisen and how the interface works.

00:07:09.320 --> 00:07:11.120
 You have these big buttons.

00:07:11.120 --> 00:07:12.540
 It's like a toy.

00:07:12.540 --> 00:07:15.040
 It's like a child's toy.

00:07:15.040 --> 00:07:18.320
 Way back when Windows was so cool because there were all

00:07:18.320 --> 00:07:20.240
 these intricate parts to it.

00:07:20.240 --> 00:07:25.280
 But now it's become push.

00:07:25.280 --> 00:07:29.340
 Someone showed me recently an iPhone application with a cat

00:07:29.340 --> 00:07:29.600
.

00:07:29.600 --> 00:07:32.560
 And I'm sure some of you have seen this app if you're into

00:07:32.560 --> 00:07:33.440
 the iPhone.

00:07:33.440 --> 00:07:34.760
 It's a cat.

00:07:34.760 --> 00:07:36.200
 What do you do?

00:07:36.200 --> 00:07:40.280
 You speak and it speaks it back only in cat voice.

00:07:40.280 --> 00:07:42.640
 And if you rub the cat, it laughs.

00:07:42.640 --> 00:07:53.440
 And if you push it, if you poke it, it groans.

00:07:53.440 --> 00:07:57.310
 Computers have become an easy way around many of the

00:07:57.310 --> 00:07:59.280
 difficulties in life.

00:07:59.280 --> 00:08:04.720
 An easy way to forget about some of the very real problems.

00:08:04.720 --> 00:08:10.950
 And what they do then is they create pleasure, stimulus and

00:08:10.950 --> 00:08:12.480
 addiction.

00:08:12.480 --> 00:08:20.360
 Because the real problem that comes with getting what you

00:08:20.360 --> 00:08:22.600
 want is that then you're cultivating

00:08:22.600 --> 00:08:23.600
 want.

00:08:23.600 --> 00:08:25.360
 You're cultivating desire.

00:08:25.360 --> 00:08:28.040
 Then whenever you don't get what you want or whenever it's

00:08:28.040 --> 00:08:29.320
 not present, you have to

00:08:29.320 --> 00:08:31.280
 seek it out.

00:08:31.280 --> 00:08:33.840
 And you wanted it more and more and more.

00:08:33.840 --> 00:08:41.080
 You're trying to get it again and again and again.

00:08:41.080 --> 00:08:46.440
 And I think this relates directly to our creating of

00:08:46.440 --> 00:08:50.660
 secondary personalities or becoming who

00:08:50.660 --> 00:08:54.240
 we really would like to be here.

00:08:54.240 --> 00:08:57.240
 And trying to be something that we're not.

00:08:57.240 --> 00:09:01.390
 And I'm not saying that this is indicative of everybody's

00:09:01.390 --> 00:09:03.120
 life in Second Life.

00:09:03.120 --> 00:09:06.410
 I think there's a lot of good that can be done here because

00:09:06.410 --> 00:09:07.760
 you are freed from many

00:09:07.760 --> 00:09:18.610
 of the unnecessary stigma and many of the problems that

00:09:18.610 --> 00:09:19.980
 exist in real life.

00:09:19.980 --> 00:09:24.340
 So you are able to get along a lot better and you are able

00:09:24.340 --> 00:09:26.200
 to get closer to people in

00:09:26.200 --> 00:09:29.160
 many ways.

00:09:29.160 --> 00:09:30.360
 I'm only bringing this up.

00:09:30.360 --> 00:09:33.580
 I think it's an important thing for us to look at because

00:09:33.580 --> 00:09:35.040
 there is an aspect of our

00:09:35.040 --> 00:09:48.870
 existence here that somehow leads us to dualism, leads us

00:09:48.870 --> 00:09:51.240
 to leading a dual life where we get

00:09:51.240 --> 00:09:55.760
 to the point where we have to make a choice.

00:09:55.760 --> 00:09:58.940
 Are we going to be the same person as we are in real life

00:09:58.940 --> 00:10:01.280
 or are we going to become a different

00:10:01.280 --> 00:10:02.880
 person?

00:10:02.880 --> 00:10:05.760
 And I would like to recommend that we don't separate these

00:10:05.760 --> 00:10:06.240
 two.

00:10:06.240 --> 00:10:09.340
 And why this really came to me is because looking around or

00:10:09.340 --> 00:10:10.820
 walking around, I always

00:10:10.820 --> 00:10:15.410
 get this sort of fear of talking to people or this hesit

00:10:15.410 --> 00:10:17.920
ancy to really get in and talk

00:10:17.920 --> 00:10:21.220
 to people because you don't really know who they are.

00:10:21.220 --> 00:10:25.030
 They could be a man in real life and in a woman's body in

00:10:25.030 --> 00:10:26.080
 this life.

00:10:26.080 --> 00:10:29.480
 They could be old and in a young person's body or so on.

00:10:29.480 --> 00:10:36.630
 And you run the risk of getting into some kind of fantasy

00:10:36.630 --> 00:10:39.060
 relationship.

00:10:39.060 --> 00:10:46.450
 And I found that I was really relieved and excited when I

00:10:46.450 --> 00:10:50.560
 would read people's profiles

00:10:50.560 --> 00:10:53.920
 telling me who they were in real life and saying this is

00:10:53.920 --> 00:10:55.440
 who they really were.

00:10:55.440 --> 00:10:59.080
 And not presenting themselves here as something other than

00:10:59.080 --> 00:11:00.880
 what they are in real life but

00:11:00.880 --> 00:11:04.720
 using second life as a means to extend their real life and

00:11:04.720 --> 00:11:07.120
 to meet people that they wouldn't

00:11:07.120 --> 00:11:11.800
 be able to meet in real life.

00:11:11.800 --> 00:11:16.400
 Truth is a very important thing in Buddhism.

00:11:16.400 --> 00:11:19.980
 And there's a reason for this because truth and wisdom,

00:11:19.980 --> 00:11:22.120
 truth and understanding is what

00:11:22.120 --> 00:11:26.080
 sets you free from suffering.

00:11:26.080 --> 00:11:32.720
 The suffering and the problems in our life are very much

00:11:32.720 --> 00:11:35.520
 like knots in a rope.

00:11:35.520 --> 00:11:37.670
 The only problem with the rope is that it's tied up in

00:11:37.670 --> 00:11:38.120
 knots.

00:11:38.120 --> 00:11:45.680
 There's nothing wrong with the quality or the material.

00:11:45.680 --> 00:11:48.680
 There's nothing wrong with the rope.

00:11:48.680 --> 00:11:52.700
 You can take the most wonderful rope and when you tie it up

00:11:52.700 --> 00:11:54.920
 into knots it becomes useless.

00:11:54.920 --> 00:11:56.040
 And our mind is the same.

00:11:56.040 --> 00:11:57.040
 Our life is the same.

00:11:57.040 --> 00:12:02.720
 We tie ourselves up in these knots and in these tangled

00:12:02.720 --> 00:12:05.080
 webs that we weave.

00:12:05.080 --> 00:12:08.120
 And this has an effect on our minds.

00:12:08.120 --> 00:12:13.710
 This takes away from our purity and takes away from our

00:12:13.710 --> 00:12:16.640
 peace and our happiness.

00:12:16.640 --> 00:12:21.140
 And personally I would much rather see the real thing that

00:12:21.140 --> 00:12:23.080
 we are who we are and that

00:12:23.080 --> 00:12:25.440
 we come to look at who we are.

00:12:25.440 --> 00:12:28.900
 And we'd be careful that we don't run away from these

00:12:28.900 --> 00:12:29.720
 things.

00:12:29.720 --> 00:12:33.800
 I think this is important here if we're going to use second

00:12:33.800 --> 00:12:35.760
 life as a tool to develop in

00:12:35.760 --> 00:12:41.560
 the Buddhist practice.

00:12:41.560 --> 00:12:48.520
 So truth is a lot like untying the knots or truth is like

00:12:48.520 --> 00:12:53.180
 keeping yourself untied or keeping

00:12:53.180 --> 00:12:56.440
 yourself from getting entangled.

00:12:56.440 --> 00:12:59.630
 When you see things as they are this is untying the knots

00:12:59.630 --> 00:13:00.640
 of existence.

00:13:00.640 --> 00:13:04.500
 Our knots, our ties are what keeps us attached to things,

00:13:04.500 --> 00:13:07.360
 what keeps us attached to our addictions,

00:13:07.360 --> 00:13:14.650
 what keeps us attached to our aversions, our depressions,

00:13:14.650 --> 00:13:18.200
 our fears and our worries.

00:13:18.200 --> 00:13:20.080
 All of these things they come from untruth.

00:13:20.080 --> 00:13:24.460
 They come from the lies that we tell ourselves and the lies

00:13:24.460 --> 00:13:26.560
 that we tell other people.

00:13:26.560 --> 00:13:31.680
 When we believe things to be other than what they are.

00:13:31.680 --> 00:13:39.010
 And so while there's nothing wrong with the illusion of

00:13:39.010 --> 00:13:41.000
 second life, take a second to

00:13:41.000 --> 00:13:42.000
 look for yourself.

00:13:42.000 --> 00:13:44.280
 What are you looking at here?

00:13:44.280 --> 00:13:45.280
 What are you looking at?

00:13:45.280 --> 00:13:50.200
 You're looking at a piece of glass or plastic and some

00:13:50.200 --> 00:13:53.260
 brightly colored lights.

00:13:53.260 --> 00:13:58.720
 These are just pixels that you're looking at.

00:13:58.720 --> 00:14:00.320
 That's the reality of it.

00:14:00.320 --> 00:14:02.880
 And in fact it's not even out there, it's at the eye.

00:14:02.880 --> 00:14:06.770
 What you're seeing is being processed on the skin of the

00:14:06.770 --> 00:14:08.760
 eye or inside the eye or even

00:14:08.760 --> 00:14:12.070
 in the middle of the brain because that's where it's being

00:14:12.070 --> 00:14:12.960
 processed.

00:14:12.960 --> 00:14:18.770
 We don't actually see at the eye, we see in the middle of

00:14:18.770 --> 00:14:21.420
 the brain somewhere.

00:14:21.420 --> 00:14:24.300
 And there's nothing wrong with using these illusions to

00:14:24.300 --> 00:14:26.080
 settle ourselves down and verify

00:14:26.080 --> 00:14:27.920
 for ourselves where are we.

00:14:27.920 --> 00:14:32.170
 Here we are sitting together listening to a meditation talk

00:14:32.170 --> 00:14:34.280
 around the campfire in front

00:14:34.280 --> 00:14:36.080
 of the Buddha in a deer park.

00:14:36.080 --> 00:14:37.440
 There's nothing wrong with that.

00:14:37.440 --> 00:14:39.160
 That's very good preliminary.

00:14:39.160 --> 00:14:42.370
 But if that becomes then your life where you say, "Boy, I

00:14:42.370 --> 00:14:44.240
 really enjoy being in this deer

00:14:44.240 --> 00:14:47.000
 park and this really just makes my day."

00:14:47.000 --> 00:14:48.320
 Well then you've got a problem.

00:14:48.320 --> 00:14:51.120
 You're living a falsehood.

00:14:51.120 --> 00:14:55.960
 We use the illusions for a purpose.

00:14:55.960 --> 00:15:00.250
 But in the end the most important for us is going to be to

00:15:00.250 --> 00:15:02.440
 see the truth and to look and

00:15:02.440 --> 00:15:06.660
 to see, "Well, why is it that I enjoy being in this deer

00:15:06.660 --> 00:15:07.480
 park?

00:15:07.480 --> 00:15:12.010
 Why is it that these pixels on this screen are able to

00:15:12.010 --> 00:15:15.260
 create this pleasure in my brain?

00:15:15.260 --> 00:15:18.200
 They're able to stimulate my brain and give rise to certain

00:15:18.200 --> 00:15:19.640
 chemicals that then make me

00:15:19.640 --> 00:15:22.600
 feel pleasure."

00:15:22.600 --> 00:15:24.320
 And then we start getting into the truth.

00:15:24.320 --> 00:15:29.280
 Then we start to penetrate the truth.

00:15:29.280 --> 00:15:31.790
 So we don't have to deny the illusion and say, "No, no, I'm

00:15:31.790 --> 00:15:34.280
 not sitting in a deer park."

00:15:34.280 --> 00:15:36.600
 But we have to go the next step and say, "Oh, what happens?

00:15:36.600 --> 00:15:37.760
 What's happening here?

00:15:37.760 --> 00:15:39.000
 What's really happening?"

00:15:39.000 --> 00:15:43.500
 And we have to look and we have to take it apart to be able

00:15:43.500 --> 00:15:45.840
 to say to ourselves, "This

00:15:45.840 --> 00:15:48.100
 is what's real.

00:15:48.100 --> 00:15:51.280
 This is the truth."

00:15:51.280 --> 00:15:54.240
 For this reason it's very important that we don't get

00:15:54.240 --> 00:15:55.880
 caught up in falsehood.

00:15:55.880 --> 00:16:04.880
 We don't get caught up in the illusion.

00:16:04.880 --> 00:16:08.730
 So the meditation practice is a tool that we use to see

00:16:08.730 --> 00:16:10.440
 things as they are.

00:16:10.440 --> 00:16:13.120
 It's a tool that we use to break away from the illusion and

00:16:13.120 --> 00:16:14.560
 to see the illusion for what

00:16:14.560 --> 00:16:15.960
 it is.

00:16:15.960 --> 00:16:16.960
 This is illusion.

00:16:16.960 --> 00:16:17.960
 This is real.

00:16:17.960 --> 00:16:19.560
 The reality is this.

00:16:19.560 --> 00:16:22.960
 Everything outside of that is illusion.

00:16:22.960 --> 00:16:27.640
 It has its place, but it only goes so far.

00:16:27.640 --> 00:16:32.710
 And the way we do this, as I've said again and again, is to

00:16:32.710 --> 00:16:34.700
 remind ourselves.

00:16:34.700 --> 00:16:37.240
 This word that we have, we say "mindfulness."

00:16:37.240 --> 00:16:40.640
 The meaning of mindfulness is to remind yourself.

00:16:40.640 --> 00:16:43.440
 You're reminding yourself of the reality of the situation,

00:16:43.440 --> 00:16:45.640
 the reality of the experience.

00:16:45.640 --> 00:16:46.800
 What's really happening here?

00:16:46.800 --> 00:16:48.040
 What do I think is happening?

00:16:48.040 --> 00:16:49.520
 What do I believe?

00:16:49.520 --> 00:16:50.520
 What is my view?

00:16:50.520 --> 00:16:52.180
 What is my religion?

00:16:52.180 --> 00:16:54.280
 What is my way of looking at things?

00:16:54.280 --> 00:16:56.280
 What's real?

00:16:56.280 --> 00:16:59.610
 And when we start to do that we'll see many things that we

00:16:59.610 --> 00:17:01.440
 wouldn't otherwise be able

00:17:01.440 --> 00:17:02.440
 to see.

00:17:02.440 --> 00:17:06.080
 We'll see many things going on in our minds.

00:17:06.080 --> 00:17:07.080
 Right here.

00:17:07.080 --> 00:17:09.080
 You see, you're not actually sitting at this campfire.

00:17:09.080 --> 00:17:12.440
 You're here in a room, all alone.

00:17:12.440 --> 00:17:14.740
 Maybe you're lonely.

00:17:14.740 --> 00:17:17.620
 Maybe you're depressed.

00:17:17.620 --> 00:17:22.360
 Maybe you don't like what I'm saying and you're upset.

00:17:22.360 --> 00:17:24.340
 Maybe you're sitting on a hard chair and it starts to get

00:17:24.340 --> 00:17:25.120
 uncomfortable.

00:17:25.120 --> 00:17:29.350
 Maybe you're sitting on a soft chair and your back starts

00:17:29.350 --> 00:17:30.240
 to hurt.

00:17:30.240 --> 00:17:31.240
 Maybe it's too hot.

00:17:31.240 --> 00:17:33.040
 Maybe it's too cold.

00:17:33.040 --> 00:17:34.040
 Maybe you're thirsty.

00:17:34.040 --> 00:17:35.880
 Maybe you're hungry.

00:17:35.880 --> 00:17:40.440
 Maybe you're sick.

00:17:40.440 --> 00:17:42.240
 Maybe you're at peace with yourself.

00:17:42.240 --> 00:17:47.660
 There could be many different things happening inside,

00:17:47.660 --> 00:17:50.880
 happening in your experience.

00:17:50.880 --> 00:17:52.840
 And so what we do in the meditation practice is we remind

00:17:52.840 --> 00:17:54.200
 ourselves of what's happening.

00:17:54.200 --> 00:17:59.590
 We remind ourselves of what these things are, of what's

00:17:59.590 --> 00:18:01.480
 really going on.

00:18:01.480 --> 00:18:02.480
 Suppose you like what I'm saying.

00:18:02.480 --> 00:18:05.410
 Then you say to yourself, "Liking, liking, liking," and you

00:18:05.410 --> 00:18:07.480
 just remind yourselves, "Oh,

00:18:07.480 --> 00:18:10.380
 now I'm liking something."

00:18:10.380 --> 00:18:17.280
 If you feel happy, you say, "Happy, happy."

00:18:17.280 --> 00:18:20.000
 If you don't like what I'm saying, you say, "Disliking,

00:18:20.000 --> 00:18:20.800
 disliking."

00:18:20.800 --> 00:18:22.800
 "Unhappy, unhappy."

00:18:22.800 --> 00:18:24.800
 Sad, sad.

00:18:24.800 --> 00:18:27.560
 Bored, bored.

00:18:27.560 --> 00:18:31.510
 See, because I can't make you bored and I can't make you

00:18:31.510 --> 00:18:32.280
 happy.

00:18:32.280 --> 00:18:34.840
 You can only do that for yourself.

00:18:34.840 --> 00:18:37.800
 You're not hearing me speak.

00:18:37.800 --> 00:18:40.560
 You can't even hear the words I'm saying.

00:18:40.560 --> 00:18:46.040
 What you hear is an electronic representation, or actually

00:18:46.040 --> 00:18:48.680
 it's an analog representation

00:18:48.680 --> 00:19:00.560
 of a digital representation of my analog transmission.

00:19:00.560 --> 00:19:03.690
 And where it's occurring is at your ear and where you're

00:19:03.690 --> 00:19:07.000
 processing it is in your mind.

00:19:07.000 --> 00:19:11.880
 So, how can I do anything to you?

00:19:11.880 --> 00:19:18.330
 I can make you hear noise if I make a noise, if I clap my

00:19:18.330 --> 00:19:18.840
 hands.

00:19:18.840 --> 00:19:21.360
 I can make sure that you all heard that.

00:19:21.360 --> 00:19:24.000
 If you were all paying attention, then most likely you all

00:19:24.000 --> 00:19:24.720
 heard that.

00:19:24.720 --> 00:19:27.480
 But I can't make you feel happy or unhappy about it.

00:19:27.480 --> 00:19:30.800
 I can't say, "I'm going to say something and everyone's

00:19:30.800 --> 00:19:32.560
 going to like what I say."

00:19:32.560 --> 00:19:35.760
 And I also can't say something to you and make you feel

00:19:35.760 --> 00:19:36.900
 angry about it.

00:19:36.900 --> 00:19:39.100
 I can't call you bad names and be sure that you're going to

00:19:39.100 --> 00:19:39.680
 be upset.

00:19:39.680 --> 00:19:46.130
 Though if you're an ordinary person, I could probably do a

00:19:46.130 --> 00:19:48.080
 good job at it.

00:19:48.080 --> 00:19:50.480
 It's not in my power.

00:19:50.480 --> 00:19:55.020
 The reason I can make you angry is because we're most of us

00:19:55.020 --> 00:19:56.580
 prone to anger.

00:19:56.580 --> 00:20:11.840
 Most of us don't like to be criticized and so on.

00:20:11.840 --> 00:20:16.370
 But in the end, the reality of it is you're making yourself

00:20:16.370 --> 00:20:18.080
 happy or unhappy.

00:20:18.080 --> 00:20:20.410
 You're hearing what I say, you're processing it and you're

00:20:20.410 --> 00:20:21.760
 saying, "Oh, that's nice."

00:20:21.760 --> 00:20:25.300
 Or, "Oh, that sucks."

00:20:25.300 --> 00:20:26.300
 You're doing that to yourself.

00:20:26.300 --> 00:20:28.560
 And we do this with everything.

00:20:28.560 --> 00:20:31.720
 There's nothing intrinsically wonderful about sitting here,

00:20:31.720 --> 00:20:33.080
 staring at some pixels that

00:20:33.080 --> 00:20:41.770
 happen to look like a deer and a tree and an ocean and a

00:20:41.770 --> 00:20:43.680
 Buddha.

00:20:43.680 --> 00:20:46.040
 There's nothing special about it.

00:20:46.040 --> 00:20:47.960
 But we make it special.

00:20:47.960 --> 00:20:50.000
 We say, "Oh, this is wonderful."

00:20:50.000 --> 00:20:54.560
 We feel very happy.

00:20:54.560 --> 00:20:58.760
 When someone yells at you and says nasty things, there's

00:20:58.760 --> 00:21:01.360
 nothing nasty about the sound.

00:21:01.360 --> 00:21:04.080
 The worst they can do is break your eardrums if it's really

00:21:04.080 --> 00:21:05.040
, really loud.

00:21:05.040 --> 00:21:09.480
 And even that, there's nothing wrong with that.

00:21:09.480 --> 00:21:11.430
 Until it gets in your mind, "Oh man, now I can't hear

00:21:11.430 --> 00:21:11.920
 anymore.

00:21:11.920 --> 00:21:13.960
 My eardrums are busted."

00:21:13.960 --> 00:21:18.860
 Or when you hear me say nasty things, you say, "Oh, what a

00:21:18.860 --> 00:21:19.600
 jerk.

00:21:19.600 --> 00:21:22.320
 What nasty things he says."

00:21:22.320 --> 00:21:32.280
 You do it to yourself.

00:21:32.280 --> 00:21:38.710
 So meditation is taking that power away, taking that reflex

00:21:38.710 --> 00:21:41.560
 away from the equation.

00:21:41.560 --> 00:21:46.760
 When we see something, instead of saying, "This is good,"

00:21:46.760 --> 00:21:49.200
 the one is this, two is three,

00:21:49.200 --> 00:21:52.640
 good, this is good.

00:21:52.640 --> 00:21:59.560
 We turn it back on ourselves and we say, "This is this."

00:21:59.560 --> 00:22:00.900
 Seeing is seeing.

00:22:00.900 --> 00:22:05.760
 Seeing this is seeing this.

00:22:05.760 --> 00:22:07.560
 We remind ourselves, "Read mind."

00:22:07.560 --> 00:22:09.440
 We bring the mind back.

00:22:09.440 --> 00:22:12.520
 We don't let the mind go forward.

00:22:12.520 --> 00:22:13.920
 Seeing is what?

00:22:13.920 --> 00:22:14.920
 Seeing.

00:22:14.920 --> 00:22:15.920
 Hearing is what?

00:22:15.920 --> 00:22:17.920
 Hearing.

00:22:17.920 --> 00:22:20.480
 So we say to ourselves, "Seeing, seeing."

00:22:20.480 --> 00:22:24.960
 When we hear something, hearing, here.

00:22:24.960 --> 00:22:28.320
 Pain is pain.

00:22:28.320 --> 00:22:30.040
 Anger is anger.

00:22:30.040 --> 00:22:31.040
 Greed is greed.

00:22:31.040 --> 00:22:33.500
 It is what it is.

00:22:33.500 --> 00:22:37.480
 It can't escape that.

00:22:37.480 --> 00:22:39.430
 The things in our mind, in our body, and in the world

00:22:39.430 --> 00:22:41.800
 around us, they can't escape their

00:22:41.800 --> 00:22:43.520
 real nature.

00:22:43.520 --> 00:22:51.840
 It's the one thing we can be sure of, is that they are what

00:22:51.840 --> 00:22:53.360
 they are.

00:22:53.360 --> 00:22:55.700
 It's the one thing we can be sure of, and this is

00:22:55.700 --> 00:22:57.520
 interesting in meditation because

00:22:57.520 --> 00:23:02.470
 in the many religious traditions, people claim to see this

00:23:02.470 --> 00:23:03.720
 or see that.

00:23:03.720 --> 00:23:07.160
 And scientists and atheists and skeptics in general are

00:23:07.160 --> 00:23:09.120
 always like, "Well, can you be

00:23:09.120 --> 00:23:13.160
 sure that that's what you really saw?

00:23:13.160 --> 00:23:16.200
 Can you be sure, you know, someone that I saw God?"

00:23:16.200 --> 00:23:22.120
 And you say, "Well, how do you know it was God?"

00:23:22.120 --> 00:23:25.800
 I felt, you know, like I was in heaven.

00:23:25.800 --> 00:23:29.210
 Or these people say, "I saw a bright light at the end of

00:23:29.210 --> 00:23:30.320
 the tunnel."

00:23:30.320 --> 00:23:33.240
 This guy says, "That's because you've got tunnel vision,

00:23:33.240 --> 00:23:34.040
 you moron."

00:23:34.040 --> 00:23:37.360
 Sorry, that's a quote.

00:23:37.360 --> 00:23:40.360
 Those weren't my words.

00:23:40.360 --> 00:23:45.980
 No, I actually do believe in this tunnel, seeing a tunnel

00:23:45.980 --> 00:23:48.320
 and a bright light at the

00:23:48.320 --> 00:23:51.690
 end because there's a lot more to it than just seeing a

00:23:51.690 --> 00:23:53.800
 tunnel, and there's some very

00:23:53.800 --> 00:23:58.360
 interesting clinical studies that have been done, that have

00:23:58.360 --> 00:24:00.360
 been documented, yes.

00:24:00.360 --> 00:24:05.130
 So, it's a good example in the sense that, you know, you're

00:24:05.130 --> 00:24:07.400
 interpreting that as being

00:24:07.400 --> 00:24:11.400
 the path, the stairway to heaven or something.

00:24:11.400 --> 00:24:13.000
 You don't really know that.

00:24:13.000 --> 00:24:16.670
 And interesting about that, but interesting about it is if

00:24:16.670 --> 00:24:18.800
 you look at, in say, the Tibetan

00:24:18.800 --> 00:24:22.390
 tradition, from what I understand, and I don't know a lot

00:24:22.390 --> 00:24:24.480
 about the Tibetan tradition, is

00:24:24.480 --> 00:24:28.560
 that these bright lights are negative.

00:24:28.560 --> 00:24:30.400
 They're a bad thing.

00:24:30.400 --> 00:24:32.120
 And that's very Buddhist.

00:24:32.120 --> 00:24:34.560
 It's a really good, important teaching.

00:24:34.560 --> 00:24:37.680
 When you die, you're going to see bright lights.

00:24:37.680 --> 00:24:39.040
 Don't chase after them.

00:24:39.040 --> 00:24:40.300
 Don't go near them.

00:24:40.300 --> 00:24:44.650
 Don't cling to them because those bright lights are your

00:24:44.650 --> 00:24:50.440
 mother's womb, are the next rebirth.

00:24:50.440 --> 00:24:51.440
 Don't go into the light.

00:24:51.440 --> 00:24:56.040
 So, you see, it's very easy to interpret things in a

00:24:56.040 --> 00:24:58.960
 certain way and say, "This is good.

00:24:58.960 --> 00:25:01.520
 This is bad."

00:25:01.520 --> 00:25:04.230
 The reason they say that is, you know, you're going to

00:25:04.230 --> 00:25:05.000
 cling to it.

00:25:05.000 --> 00:25:06.000
 And you don't know what that light is.

00:25:06.000 --> 00:25:07.360
 It could be a human womb.

00:25:07.360 --> 00:25:08.580
 It could be a dog womb.

00:25:08.580 --> 00:25:10.800
 It could be a deer womb.

00:25:10.800 --> 00:25:21.760
 I don't know.

00:25:21.760 --> 00:25:25.040
 It's a very good example in terms of interpreting things,

00:25:25.040 --> 00:25:27.640
 the things that we experience in meditation

00:25:27.640 --> 00:25:30.680
 or near-death experiences and so on.

00:25:30.680 --> 00:25:32.360
 They are what they are.

00:25:32.360 --> 00:25:34.480
 And this is how a Buddhist gets out of that.

00:25:34.480 --> 00:25:36.080
 What is different about Buddhism?

00:25:36.080 --> 00:25:37.480
 Buddhism has these experiences.

00:25:37.480 --> 00:25:40.840
 We have these experiences of bliss and seeing bright lights

00:25:40.840 --> 00:25:42.680
 and seeing angels and so on.

00:25:42.680 --> 00:25:48.610
 How do we get, how do we avoid this dilemma, the religious

00:25:48.610 --> 00:25:51.760
 dilemma of proving that we see

00:25:51.760 --> 00:25:54.960
 what we see?

00:25:54.960 --> 00:25:56.890
 And I think I've already answered this and it should be

00:25:56.890 --> 00:25:58.040
 clear that Buddhists don't go

00:25:58.040 --> 00:25:59.400
 beyond the seeing.

00:25:59.400 --> 00:26:03.400
 What you can confirm is that you're seeing something.

00:26:03.400 --> 00:26:05.240
 No one can take that away from you and say, "No, no, you

00:26:05.240 --> 00:26:06.120
 didn't see anything."

00:26:06.120 --> 00:26:12.680
 There was no experience of seeing there.

00:26:12.680 --> 00:26:15.960
 And this is important because in science they will say that

00:26:15.960 --> 00:26:16.200
.

00:26:16.200 --> 00:26:17.800
 They will say, "You didn't see anything."

00:26:17.800 --> 00:26:18.800
 Why?

00:26:18.800 --> 00:26:23.980
 Because your eyes aren't working or your brain wasn't

00:26:23.980 --> 00:26:25.240
 working.

00:26:25.240 --> 00:26:29.560
 There are cases of people who went into cardiac arrest for

00:26:29.560 --> 00:26:33.000
 minutes and had near-death experiences.

00:26:33.000 --> 00:26:36.380
 Now this is impossible from scientific point of view

00:26:36.380 --> 00:26:38.400
 because you go into brain death after

00:26:38.400 --> 00:26:41.000
 something like 20 seconds.

00:26:41.000 --> 00:26:43.960
 There's zero brain activity.

00:26:43.960 --> 00:26:46.680
 Zero.

00:26:46.680 --> 00:26:47.680
 Something like that.

00:26:47.680 --> 00:26:51.720
 I'm not a scientist and so I don't know.

00:26:51.720 --> 00:26:54.640
 I'm not in Western terms anyway.

00:26:54.640 --> 00:26:58.270
 But I'm reading the scientific studies and after about 20

00:26:58.270 --> 00:27:00.920
 seconds, after about 5 seconds,

00:27:00.920 --> 00:27:01.920
 your brain dead.

00:27:01.920 --> 00:27:05.440
 You aren't able to think according to science.

00:27:05.440 --> 00:27:06.600
 The brain isn't able to function.

00:27:06.600 --> 00:27:11.760
 But after 20 seconds, there's no activity whatsoever.

00:27:11.760 --> 00:27:14.470
 It's not only you're not able to think, but the brain is

00:27:14.470 --> 00:27:15.780
 just not functioning.

00:27:15.780 --> 00:27:18.640
 And yet people will have these experiences.

00:27:18.640 --> 00:27:21.780
 And what makes them interesting is because people will

00:27:21.780 --> 00:27:23.880
 recount afterwards when they come

00:27:23.880 --> 00:27:29.240
 back, listening to the doctor's talk, watching the doctors

00:27:29.240 --> 00:27:32.040
 do things, seeing the doctors

00:27:32.040 --> 00:27:34.200
 operate on them.

00:27:34.200 --> 00:27:39.940
 There's just one patient who passed away or was passing

00:27:39.940 --> 00:27:43.080
 away and came back and claimed

00:27:43.080 --> 00:27:48.770
 to see the surgeon flapping his arms like a chicken while

00:27:48.770 --> 00:27:50.440
 she was out.

00:27:50.440 --> 00:27:59.390
 Now this doctor had a peculiar habit of flapping his hands

00:27:59.390 --> 00:28:04.880
 like a chicken because he would disinfect

00:28:04.880 --> 00:28:09.490
 his hands before operating and then he'd place them on his

00:28:09.490 --> 00:28:12.240
 chest and give instructions with

00:28:12.240 --> 00:28:16.490
 his elbows because he was in order to make sure that he

00:28:16.490 --> 00:28:18.880
 wouldn't touch anything.

00:28:18.880 --> 00:28:20.140
 And so it looked like he was flapping himself like a

00:28:20.140 --> 00:28:20.480
 chicken.

00:28:20.480 --> 00:28:28.330
 So he was doing this well after she had gone into brain

00:28:28.330 --> 00:28:29.660
 dead.

00:28:29.660 --> 00:28:33.930
 So there's a very strong case for the idea that seeing is

00:28:33.930 --> 00:28:34.920
 seeing.

00:28:34.920 --> 00:28:37.240
 That we can be sure when you see something that should be

00:28:37.240 --> 00:28:38.320
 considered seeing.

00:28:38.320 --> 00:28:41.210
 Whatever science wants to say that no, it's just an

00:28:41.210 --> 00:28:42.040
 illusion.

00:28:42.040 --> 00:28:43.360
 Well an illusion of what?

00:28:43.360 --> 00:28:46.120
 Or an illusion in what form?

00:28:46.120 --> 00:28:47.980
 How can you say that it's not seeing when there's an

00:28:47.980 --> 00:28:49.000
 experience of seeing?

00:28:49.000 --> 00:28:52.690
 So this is sort of a roundabout, but it's talking, trying

00:28:52.690 --> 00:28:54.920
 to explain what I'm talking about here

00:28:54.920 --> 00:29:00.400
 in terms of reality, in terms of the truth.

00:29:00.400 --> 00:29:03.650
 Truth is very important in Buddhism and that's the lesson

00:29:03.650 --> 00:29:05.840
 that I would like to impart today.

00:29:05.840 --> 00:29:09.590
 I think I've gone for 30 minutes now so I'm just going to

00:29:09.590 --> 00:29:11.400
 stop there if anyone has any

00:29:11.400 --> 00:29:12.400
 questions.

00:29:12.400 --> 00:29:13.400
 It can take a little bit of time.

00:29:13.400 --> 00:29:18.220
 I'd also like to announce that I'm now taking questions

00:29:18.220 --> 00:29:20.200
 over the internet.

00:29:20.200 --> 00:29:26.440
 I have this new project going on on the internet on YouTube

00:29:26.440 --> 00:29:29.880
 called Ask a Monk, where if you

00:29:29.880 --> 00:29:36.630
 have any questions on Buddhism or meditation, you can log

00:29:36.630 --> 00:29:39.920
 on to my YouTube channel.

00:29:39.920 --> 00:29:48.980
 And if you leave questions, I'll answer them in the form of

00:29:48.980 --> 00:29:51.400
 videos so that way other people

00:29:51.400 --> 00:29:53.320
 can hear them as well.

00:29:53.320 --> 00:29:59.410
 If you want to go directly to the questions, they're at the

00:29:59.410 --> 00:30:01.080
 second link.

00:30:01.080 --> 00:30:04.160
 But you can ask questions directly on my YouTube channel so

00:30:04.160 --> 00:30:09.040
 that's probably the best app

00:30:09.040 --> 00:30:11.040
 avenue to go.

00:30:11.040 --> 00:30:23.480
 Okay, so the second link there is to, if you want

00:30:23.480 --> 00:30:26.170
 to go directly to the list of questions, but you can ask

00:30:26.170 --> 00:30:27.840
 questions on my YouTube channel

00:30:27.840 --> 00:30:31.800
 as well so that's probably the best link to go.

00:30:31.800 --> 00:30:34.200
 So that's all for the speech today.

00:30:34.200 --> 00:30:35.200
 Thank you.

00:30:35.200 --> 00:30:50.280
 [

