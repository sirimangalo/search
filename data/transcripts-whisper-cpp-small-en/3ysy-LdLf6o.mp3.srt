1
00:00:00,000 --> 00:00:03,550
 What is your recommendation for dealing with an angry

2
00:00:03,550 --> 00:00:05,000
 person lashing out at you?

3
00:00:05,000 --> 00:00:07,670
 Do you ignore them and go straight to paying attention to

4
00:00:07,670 --> 00:00:09,000
 the rising and falling?

5
00:00:09,000 --> 00:00:14,000
 Yeah, no, never do that.

6
00:00:14,000 --> 00:00:18,280
 Rising and falling, there's nothing special about the

7
00:00:18,280 --> 00:00:21,000
 rising and falling of the abdomen.

8
00:00:21,000 --> 00:00:26,990
 There's one thing special, it's quite easy and coarse and

9
00:00:26,990 --> 00:00:29,000
 obvious.

10
00:00:29,000 --> 00:00:32,000
 So it's child's plane.

11
00:00:32,000 --> 00:00:39,040
 I remember when I first started, and I did this exact thing

12
00:00:39,040 --> 00:00:42,000
 when people were yelling at me,

13
00:00:42,000 --> 00:00:43,000
 and I didn't know what to do.

14
00:00:43,000 --> 00:00:45,520
 I just closed my eyes and went back to the rising and

15
00:00:45,520 --> 00:00:46,000
 falling.

16
00:00:46,000 --> 00:00:49,370
 It doesn't really work because you've got this intense

17
00:00:49,370 --> 00:00:54,000
 headache and tension and stress,

18
00:00:54,000 --> 00:00:57,000
 because this person is making you all upset.

19
00:00:57,000 --> 00:01:04,290
 So you're not actually cultivating objectivity, you're not

20
00:01:04,290 --> 00:01:05,000
 actually meditating.

21
00:01:05,000 --> 00:01:10,000
 You should never ignore anything.

22
00:01:10,000 --> 00:01:14,000
 The point is, don't ignore the present moment.

23
00:01:14,000 --> 00:01:18,230
 The present moment at that moment is this person lashing

24
00:01:18,230 --> 00:01:19,000
 out at you.

25
00:01:19,000 --> 00:01:22,000
 So you can close your eyes, you can keep your eyes open,

26
00:01:22,000 --> 00:01:25,000
 but what you should be focusing on is where your mind is.

27
00:01:26,000 --> 00:01:29,260
 If your mind is not interested, if you're so highly

28
00:01:29,260 --> 00:01:31,000
 developed that you're not interested,

29
00:01:31,000 --> 00:01:33,280
 "Oh, this person's lashing out at me, okay, well, back to

30
00:01:33,280 --> 00:01:36,000
 my meditation," then absolutely that's fine.

31
00:01:36,000 --> 00:01:39,560
 If it doesn't bother you and your mind doesn't even go out

32
00:01:39,560 --> 00:01:40,000
 there,

33
00:01:40,000 --> 00:01:43,470
 you know the person's there, you say to yourself, "Hearing,

34
00:01:43,470 --> 00:01:44,000
 hearing,"

35
00:01:44,000 --> 00:01:46,560
 and then just ignore them and go back to your rising and

36
00:01:46,560 --> 00:01:47,000
 falling,

37
00:01:47,000 --> 00:01:49,000
 and it's just like birds chirping and fine.

38
00:01:49,000 --> 00:01:53,000
 But if it upsets you, or if you're at all interested in it,

39
00:01:53,000 --> 00:01:57,380
 then it's highly unlikely that you're able to really put

40
00:01:57,380 --> 00:01:58,000
 this out of your mind

41
00:01:58,000 --> 00:02:00,000
 unless you are highly developed.

42
00:02:00,000 --> 00:02:03,580
 So your mind, even if it's not upset, it's going to be

43
00:02:03,580 --> 00:02:07,000
 going back to focusing on what this person is saying,

44
00:02:07,000 --> 00:02:10,000
 all the nasty things they're saying about you,

45
00:02:10,000 --> 00:02:12,330
 in which case that's what you should be focusing on, saying

46
00:02:12,330 --> 00:02:15,000
 to yourself, "Hearing, hearing,"

47
00:02:15,000 --> 00:02:17,800
 and once your mind does let go of it, if at times your mind

48
00:02:17,800 --> 00:02:19,000
 is no longer interested in it

49
00:02:19,000 --> 00:02:21,820
 or the person stops yelling, you can just go back to your

50
00:02:21,820 --> 00:02:23,000
 rising and falling.

51
00:02:23,000 --> 00:02:26,370
 You should also be noting the emotions, if it does give

52
00:02:26,370 --> 00:02:28,000
 rise to emotional turmoil,

53
00:02:28,000 --> 00:02:31,620
 like you're upset or angry, or if you find it funny, or if

54
00:02:31,620 --> 00:02:34,000
 you feel somehow egotistical.

55
00:02:34,000 --> 00:02:37,270
 Often we feel proud of ourselves that we're able to not

56
00:02:37,270 --> 00:02:38,000
 argue back,

57
00:02:38,000 --> 00:02:41,210
 and you should watch that as well, not think you're better

58
00:02:41,210 --> 00:02:43,000
 than the person because they're lashing out,

59
00:02:43,000 --> 00:02:52,330
 and all this person is such a low, low spirituality, such a

60
00:02:52,330 --> 00:02:54,000
 low being.

61
00:02:54,000 --> 00:02:56,000
 I don't know what the word is.

62
00:02:56,000 --> 00:03:00,000
 They can't even control their temper, that kind of thing.

63
00:03:00,000 --> 00:03:01,000
 You should watch that as well.

64
00:03:01,000 --> 00:03:03,000
 You should watch all of these things and be mindful of them

65
00:03:03,000 --> 00:03:04,000
,

66
00:03:04,000 --> 00:03:11,000
 replace the judgments with objectivity and mindfulness.

