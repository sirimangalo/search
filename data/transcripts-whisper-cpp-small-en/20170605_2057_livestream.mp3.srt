1
00:00:00,000 --> 00:00:10,640
 Okay, good evening everyone.

2
00:00:10,640 --> 00:00:19,080
 Welcome to our evening Dharma session.

3
00:00:19,080 --> 00:00:26,370
 So the other talk that I'm set to give this month at this

4
00:00:26,370 --> 00:00:30,520
 conference is on wrong mindfulness.

5
00:00:30,520 --> 00:00:35,370
 Not sure if I've talked about this before but I certainly

6
00:00:35,370 --> 00:00:40,520
 haven't given this talk yet.

7
00:00:40,520 --> 00:00:43,960
 So save you all the cost of coming out to the conference.

8
00:00:43,960 --> 00:00:50,240
 I'm trying to practice it tonight. Not the talk but just

9
00:00:50,240 --> 00:00:53,480
 the sorts of things I'm going to talk about.

10
00:00:53,480 --> 00:01:02,040
 To get a feel for what's important to say.

11
00:01:02,040 --> 00:01:05,560
 So wrong mindfulness.

12
00:01:05,560 --> 00:01:13,150
 When we talk about wrong mindfulness, we're concerned about

13
00:01:13,150 --> 00:01:16,920
 how the practice goes wrong.

14
00:01:16,920 --> 00:01:27,720
 The practice can go wrong.

15
00:01:27,720 --> 00:01:33,880
 And so the first thing to do is to talk about what we mean

16
00:01:33,880 --> 00:01:36,920
 by this word mindfulness.

17
00:01:36,920 --> 00:01:40,240
 When we understand mindfulness then we can talk about how

18
00:01:40,240 --> 00:01:41,720
 mindfulness can go wrong.

19
00:01:41,720 --> 00:01:47,600
 I mean it's an interesting choice of a topic. It wasn't the

20
00:01:47,600 --> 00:01:49,320
 one I picked by me.

21
00:01:49,320 --> 00:01:52,630
 But when I asked about what they meant, why did they choose

22
00:01:52,630 --> 00:01:53,160
 this topic,

23
00:01:53,160 --> 00:01:56,680
 they really meant how the practice can go wrong. Because

24
00:01:56,680 --> 00:01:57,720
 this happens.

25
00:01:57,720 --> 00:02:03,460
 People go and do meditation courses here and there and

26
00:02:03,460 --> 00:02:05,960
 things can go wrong.

27
00:02:05,960 --> 00:02:09,300
 It's never really happened for me so I'm not trying to

28
00:02:09,300 --> 00:02:10,200
 scare anyone.

29
00:02:10,200 --> 00:02:13,960
 It's not the sort of thing that happens to our students.

30
00:02:13,960 --> 00:02:18,440
 But it's interesting to talk about.

31
00:02:18,440 --> 00:02:22,180
 I mean, not our meditators are not perfect and things do go

32
00:02:22,180 --> 00:02:24,760
 wrong but we're here to correct them.

33
00:02:24,760 --> 00:02:29,000
 This is not such a difficult thing to do.

34
00:02:29,000 --> 00:02:32,460
 So it is important to know these are the various ways it

35
00:02:32,460 --> 00:02:33,560
 can go wrong.

36
00:02:33,560 --> 00:02:36,300
 It doesn't really mean that mindfulness can never be wrong

37
00:02:36,300 --> 00:02:37,000
 in that sense.

38
00:02:37,000 --> 00:02:39,960
 But I'll talk about that at the end of the talk.

39
00:02:39,960 --> 00:02:43,720
 A way that it could be seen to be wrong mindfulness.

40
00:02:43,720 --> 00:02:48,340
 It's interesting. Well there are four ways we can think of

41
00:02:48,340 --> 00:02:48,600
 it.

42
00:02:48,600 --> 00:02:51,730
 But first thing to do is to talk about what we mean by

43
00:02:51,730 --> 00:02:52,840
 mindfulness.

44
00:02:52,840 --> 00:02:55,090
 If we're going to talk about wrong mindfulness we have to

45
00:02:55,090 --> 00:02:56,680
 talk about

46
00:02:56,680 --> 00:03:01,000
 mindfulness or even right mindfulness.

47
00:03:01,000 --> 00:03:09,880
 And of course, I mean this is important because

48
00:03:09,880 --> 00:03:13,800
 mindfulness is just a word. It's an imprecise translation

49
00:03:13,800 --> 00:03:15,640
 of the word sati.

50
00:03:15,640 --> 00:03:19,480
 It's not the most literal translation out there.

51
00:03:19,480 --> 00:03:23,720
 It's not a bad translation, not a terrible translation.

52
00:03:23,720 --> 00:03:26,920
 But it's not what is directly meant by the word sati. Sati

53
00:03:26,920 --> 00:03:26,920
 means,

54
00:03:26,920 --> 00:03:31,160
 sati comes from the root sar which means to remember.

55
00:03:31,160 --> 00:03:37,240
 So it was used in a non-buddhist context

56
00:03:37,240 --> 00:03:39,640
 to refer to being able to remember things that happened a

57
00:03:39,640 --> 00:03:40,440
 long time ago.

58
00:03:40,440 --> 00:03:45,400
 If you've got sati you're a person with a good memory.

59
00:03:45,400 --> 00:03:52,520
 You're a person who is sharp-minded in a sense.

60
00:03:52,520 --> 00:03:55,580
 Now when the Buddha came along the idea was that you

61
00:03:55,580 --> 00:03:56,200
 remember

62
00:03:56,200 --> 00:04:00,700
 or you reflect on something. Maybe reflect is a little

63
00:04:00,700 --> 00:04:01,400
 closer to how it's

64
00:04:01,400 --> 00:04:04,520
 used in a meditative sense.

65
00:04:05,640 --> 00:04:09,720
 But it doesn't have anything to do with exactly mindfulness

66
00:04:09,720 --> 00:04:09,800
.

67
00:04:09,800 --> 00:04:13,400
 It's the same sort of idea.

68
00:04:13,400 --> 00:04:17,740
 So but the best way to understand in Theravada Buddhism,

69
00:04:17,740 --> 00:04:17,880
 the best way

70
00:04:17,880 --> 00:04:21,290
 to understand something is to look up what we call lakkhan

71
00:04:21,290 --> 00:04:22,680
adi chatukkha.

72
00:04:22,680 --> 00:04:26,080
 I think I've actually gone over this or we've gone over it

73
00:04:26,080 --> 00:04:26,200
 in our

74
00:04:26,200 --> 00:04:29,240
 Rishuddhi Maga course.

75
00:04:29,480 --> 00:04:35,200
 Lakkhanadi chatukkha is the fourfold qualities of something

76
00:04:35,200 --> 00:04:37,080
. All

77
00:04:37,080 --> 00:04:45,560
 dhammas in Buddhism are given this set of four attributes.

78
00:04:45,560 --> 00:04:49,240
 We're starting with the lakkhanada, the characteristic.

79
00:04:49,240 --> 00:04:53,320
 So you have the characteristic of something. What's it like

80
00:04:53,320 --> 00:04:53,720
?

81
00:04:53,720 --> 00:05:00,360
 You have the rasa which is the function is what it does.

82
00:05:00,360 --> 00:05:06,360
 The pajupatana which is the

83
00:05:06,360 --> 00:05:10,920
 which is the manifestation which means how it presents

84
00:05:10,920 --> 00:05:13,240
 itself.

85
00:05:13,240 --> 00:05:15,960
 And then the

86
00:05:19,880 --> 00:05:24,280
 in the pajatana which is the approximate cause.

87
00:05:24,280 --> 00:05:28,920
 What is it that causes that dumb? So in the Rishuddhi Maga

88
00:05:28,920 --> 00:05:29,880
 it's got many many

89
00:05:29,880 --> 00:05:36,280
 lists of these and in one list we have sati mentioned

90
00:05:36,280 --> 00:05:41,240
 which is you know it's a really to pull out sati as the

91
00:05:41,240 --> 00:05:45,930
 then define it in this way is quite useful I think as you

92
00:05:45,930 --> 00:05:46,600
'll see.

93
00:05:46,600 --> 00:05:49,450
 So the characteristic of sati is something called upilapan

94
00:05:49,450 --> 00:05:50,440
avich means

95
00:05:50,440 --> 00:05:58,120
 non wavering or wobbling. The characteristic of

96
00:05:58,120 --> 00:06:03,480
 mindfulness and here we're thinking of a

97
00:06:03,480 --> 00:06:06,200
 not just reflecting on something but a state of mind that

98
00:06:06,200 --> 00:06:07,320
 is reflective or that

99
00:06:07,320 --> 00:06:11,640
 is remembering.

100
00:06:11,640 --> 00:06:17,400
 We don't have a word in English for that but is

101
00:06:17,400 --> 00:06:21,400
 is mindful for lack of a better word.

102
00:06:21,400 --> 00:06:26,440
 It doesn't waver from the object so the ordinary mind is

103
00:06:26,440 --> 00:06:33,800
 is unstable right it flits here and there

104
00:06:33,800 --> 00:06:37,080
 and sati is something that grasps an object.

105
00:06:37,080 --> 00:06:40,730
 Grasps in in a in an English sense right when you're able

106
00:06:40,730 --> 00:06:41,320
 to grasp a

107
00:06:41,320 --> 00:06:44,760
 concept or something so the grasping of an object is to

108
00:06:44,760 --> 00:06:49,960
 really experience it fully

109
00:06:49,960 --> 00:06:57,240
 and perhaps mindful is good in that sense because you fully

110
00:06:57,240 --> 00:07:00,680
 you get it fully in your mind.

111
00:07:00,680 --> 00:07:07,160
 Your mind is fully aware of it.

112
00:07:08,040 --> 00:07:14,440
 The function is asamosa. Asamosa means not forgetting

113
00:07:14,440 --> 00:07:17,720
 because once you grasp something when you grasp something

114
00:07:17,720 --> 00:07:24,680
 you remember it you you stay with it. So in an in an

115
00:07:24,680 --> 00:07:25,320
 ordinary

116
00:07:25,320 --> 00:07:29,800
 sense this would mean being able to remember clearly things

117
00:07:29,800 --> 00:07:32,760
 that happen in the past or even in the future but

118
00:07:32,760 --> 00:07:36,280
 from a meditative point of view it means remembering

119
00:07:36,280 --> 00:07:40,200
 the actual experience. So when you have pain

120
00:07:40,200 --> 00:07:44,760
 now we all even even not meditating when we have pain we're

121
00:07:44,760 --> 00:07:45,560
 aware that we have

122
00:07:45,560 --> 00:07:50,520
 pain right but that awareness that knowledge

123
00:07:50,520 --> 00:07:56,200
 is lost or that bare experience is gone in the next moment

124
00:07:56,200 --> 00:07:59,080
 because in the next moment we're judging it

125
00:07:59,080 --> 00:08:02,410
 we're disliking it we're trying to figure out how to get

126
00:08:02,410 --> 00:08:03,000
 rid of it

127
00:08:03,000 --> 00:08:05,880
 we're doing everything but experiencing it and so in that

128
00:08:05,880 --> 00:08:07,080
 sense we've forgotten

129
00:08:07,080 --> 00:08:10,600
 it. With mindfulness you don't forget the

130
00:08:10,600 --> 00:08:13,880
 object right and so when we say to ourselves

131
00:08:13,880 --> 00:08:17,240
 pain pain we're remembering it we're reminding

132
00:08:17,240 --> 00:08:21,880
 ourselves and the the the result the consequences that we

133
00:08:21,880 --> 00:08:24,520
 remember it.

134
00:08:31,240 --> 00:08:37,320
 The manifestation is

135
00:08:37,320 --> 00:08:44,760
 which means protection regarding it manifested as a guarded

136
00:08:44,760 --> 00:08:45,320
 state

137
00:08:45,320 --> 00:08:48,120
 a state that

138
00:08:48,120 --> 00:08:56,680
 is protected from defilement so the the mind that is is

139
00:08:56,680 --> 00:08:57,960
 aware of pain

140
00:08:57,960 --> 00:09:02,520
 the mind that is aware that it is pain is is invincible is

141
00:09:02,520 --> 00:09:04,040
 impervious

142
00:09:04,040 --> 00:09:07,160
 it's non-reactive

143
00:09:07,160 --> 00:09:14,640
 that's actually a somewhat specific manifestation i mean it

144
00:09:14,640 --> 00:09:14,840
's it's a

145
00:09:14,840 --> 00:09:17,640
 characteristic of the experience that is important for

146
00:09:17,640 --> 00:09:18,840
 Buddhists

147
00:09:18,840 --> 00:09:25,400
 but the other it says or the manifestation is as

148
00:09:25,400 --> 00:09:30,360
 the state of confronting the objective field so confronting

149
00:09:30,360 --> 00:09:33,080
 the object

150
00:09:33,080 --> 00:09:36,190
 which is a little bit more clear about what we mean by the

151
00:09:36,190 --> 00:09:36,680
 function of

152
00:09:36,680 --> 00:09:40,280
 mindfulness and it's interesting because when you

153
00:09:40,280 --> 00:09:42,920
 think about it

154
00:09:42,920 --> 00:09:46,900
 practically this is what we're trying to do is confront our

155
00:09:46,900 --> 00:09:47,640
 problems

156
00:09:47,640 --> 00:09:51,800
 normally when we have a problem we'll run away from it

157
00:09:51,800 --> 00:09:55,590
 or when we're confronted by something attractive we'll

158
00:09:55,590 --> 00:09:56,600
 chase after it

159
00:09:56,600 --> 00:10:00,680
 we're unable to confront it to stay with it

160
00:10:00,680 --> 00:10:06,440
 right when something good comes we're unable to

161
00:10:06,440 --> 00:10:12,200
 to rest without without obtaining it when something bad

162
00:10:12,200 --> 00:10:13,720
 comes unpleasant comes

163
00:10:13,720 --> 00:10:17,560
 we're unable to rest until it's gone

164
00:10:19,240 --> 00:10:23,560
 we are unable to face it so this is clearly what what we're

165
00:10:23,560 --> 00:10:23,960
 talking

166
00:10:23,960 --> 00:10:27,240
 about when we when we use this word mindfulness or

167
00:10:27,240 --> 00:10:32,200
 sati is not trying to change things i mean the

168
00:10:32,200 --> 00:10:35,930
 idea that meditators get mistakenly is that when you say

169
00:10:35,930 --> 00:10:37,400
 pain pain the pain is

170
00:10:37,400 --> 00:10:40,550
 supposed to go away or going to go away it's neither

171
00:10:40,550 --> 00:10:41,880
 supposed to go away and

172
00:10:41,880 --> 00:10:45,320
 are going to go away i mean sometimes it may

173
00:10:45,320 --> 00:10:47,790
 sometimes the pain is caused by stress in the mind so when

174
00:10:47,790 --> 00:10:49,160
 you're mindful

175
00:10:49,160 --> 00:10:54,040
 it does go away but not always

176
00:10:54,040 --> 00:10:59,410
 no the point is to confront the pain and to straighten out

177
00:10:59,410 --> 00:11:00,280
 our minds

178
00:11:00,280 --> 00:11:03,320
 to strengthen our mind so that we're able to experience

179
00:11:03,320 --> 00:11:03,800
 things

180
00:11:03,800 --> 00:11:12,680
 and not forget them not get lost in reactions judgments

181
00:11:12,680 --> 00:11:20,200
 so

182
00:11:20,200 --> 00:11:28,840
 so that's the the manifestation the last one is the

183
00:11:28,840 --> 00:11:32,520
 proximate cause and this one i mean this whole thing is

184
00:11:32,520 --> 00:11:33,160
 really

185
00:11:33,160 --> 00:11:36,440
 actually this you know this talk tonight the the

186
00:11:36,440 --> 00:11:40,810
 content here is very important it's very important for us

187
00:11:40,810 --> 00:11:41,640
 to understand because

188
00:11:41,640 --> 00:11:44,520
 we use this word and we say oh what are you practicing

189
00:11:44,520 --> 00:11:48,210
 here we teach mindfulness right i always say we're here to

190
00:11:48,210 --> 00:11:49,240
 practice mindfulness

191
00:11:49,240 --> 00:11:52,600
 meditation so here you now you have an

192
00:11:52,600 --> 00:11:57,780
 understanding of what that means so the the proximate cause

193
00:11:57,780 --> 00:11:58,680
 of mindfulness

194
00:11:58,680 --> 00:12:02,000
 and this is important because it it it answers the question

195
00:12:02,000 --> 00:12:02,760
 how do you be

196
00:12:02,760 --> 00:12:06,430
 mindful how do you come to be mindful proximate cause is

197
00:12:06,430 --> 00:12:07,000
 something called

198
00:12:07,000 --> 00:12:09,870
 tira sanya which i think many of you have heard me say

199
00:12:09,870 --> 00:12:12,200
 before

200
00:12:12,200 --> 00:12:16,600
 sanya is is um well it means different things but

201
00:12:16,600 --> 00:12:23,190
 um here it means the perception of an object how you

202
00:12:23,190 --> 00:12:25,000
 perceive it

203
00:12:25,000 --> 00:12:28,680
 so sanya can mean the the recognition of something you look

204
00:12:28,680 --> 00:12:29,400
 at

205
00:12:29,400 --> 00:12:33,000
 you see a woman you see a man you see a tree you hear

206
00:12:33,000 --> 00:12:34,920
 something and you recognize

207
00:12:34,920 --> 00:12:39,480
 sanya is what helps you recognize her or is the recognition

208
00:12:39,480 --> 00:12:44,770
 of the object um but it can also just mean the perception

209
00:12:44,770 --> 00:12:46,440
 of something

210
00:12:46,440 --> 00:12:49,640
 and so that's the bare perception when you see something

211
00:12:49,640 --> 00:12:53,800
 the perception of seeing that sanya

212
00:12:53,800 --> 00:12:59,530
 so tira sanya is always there sanya is in every experience

213
00:12:59,530 --> 00:13:01,240
 but tira sanya

214
00:13:01,240 --> 00:13:03,330
 it's a really interesting word that you don't hear

215
00:13:03,330 --> 00:13:04,280
 mentioned that much i

216
00:13:04,280 --> 00:13:07,560
 bring it up quite a lot because i'm interested in it

217
00:13:07,560 --> 00:13:14,520
 but tira sanya tira means strong or or firm

218
00:13:14,520 --> 00:13:17,680
 which is interesting because sanya is already there so all

219
00:13:17,680 --> 00:13:18,360
 we're doing

220
00:13:18,360 --> 00:13:22,840
 is strengthening the perception what does that mean

221
00:13:22,840 --> 00:13:27,320
 if you want to be if you want mindfulness to arise

222
00:13:27,320 --> 00:13:31,500
 it requires something it requires a strengthening of the

223
00:13:31,500 --> 00:13:32,440
 perception

224
00:13:32,440 --> 00:13:35,360
 in the context of the other aspects of this description it

225
00:13:35,360 --> 00:13:35,880
 should be

226
00:13:35,880 --> 00:13:40,120
 fairly clear you're strengthening your your experience of

227
00:13:40,120 --> 00:13:40,920
 you're strengthening

228
00:13:40,920 --> 00:13:47,160
 your your your mind the mind that experiences it and so

229
00:13:47,160 --> 00:13:50,760
 hence the reason why we repeat to ourselves pain

230
00:13:50,760 --> 00:13:55,480
 pain because we have the perception of pain all we're doing

231
00:13:55,480 --> 00:13:59,680
 is reaffirming affirming that right that's what a mantra

232
00:13:59,680 --> 00:14:00,120
 does

233
00:14:00,120 --> 00:14:03,690
 a mantra is an ancient meditation technique and that's its

234
00:14:03,690 --> 00:14:04,280
 purpose

235
00:14:04,280 --> 00:14:08,280
 is to strengthen the perception of the object

236
00:14:08,280 --> 00:14:13,080
 so whether it be on a concept or whether it be on reality

237
00:14:13,080 --> 00:14:21,240
 tira sanya is is the cause of of the grasping of the object

238
00:14:21,240 --> 00:14:27,080
 or in a in a practical more mundane sense it says or the

239
00:14:27,080 --> 00:14:28,120
 approximate cause is

240
00:14:28,120 --> 00:14:31,480
 just the foundations of mindfulness beginning with

241
00:14:31,480 --> 00:14:36,920
 the body so when we when we teach mindfulness we

242
00:14:36,920 --> 00:14:42,040
 we try to bring up you know i usually have people read my

243
00:14:42,040 --> 00:14:42,680
 booklet and the

244
00:14:42,680 --> 00:14:45,510
 first thing in the booklet is the four foundations of

245
00:14:45,510 --> 00:14:46,280
 mindfulness

246
00:14:46,280 --> 00:14:50,520
 and that's what it's saying here that um what's what's the

247
00:14:50,520 --> 00:14:51,240
 approximate cause of

248
00:14:51,240 --> 00:14:54,440
 mindfulness while practicing the four foundations of

249
00:14:54,440 --> 00:14:57,000
 mindfulness

250
00:14:58,440 --> 00:15:01,510
 and so these are the body being mindful of the body when

251
00:15:01,510 --> 00:15:01,880
 you

252
00:15:01,880 --> 00:15:05,330
 when the stomach rises and falls or when you move your

253
00:15:05,330 --> 00:15:06,280
 hands or so on

254
00:15:06,280 --> 00:15:10,520
 when you walk walking walking stepping right stepping

255
00:15:10,520 --> 00:15:15,410
 being mindful of the body that way is the cause of

256
00:15:15,410 --> 00:15:16,840
 mindfulness

257
00:15:16,840 --> 00:15:20,220
 we did that number two is we did not so being mindful of

258
00:15:20,220 --> 00:15:21,640
 pain if you feel pain

259
00:15:21,640 --> 00:15:27,720
 say pain pain if you feel happy say happy happy or calm

260
00:15:27,720 --> 00:15:28,120
 calm

261
00:15:28,120 --> 00:15:33,150
 the mind is thinking thinking about the past or future good

262
00:15:33,150 --> 00:15:35,720
 thoughts bad thoughts

263
00:15:35,720 --> 00:15:40,570
 being mindful of thoughts and four is dumb the dumb are

264
00:15:40,570 --> 00:15:41,480
 many different things

265
00:15:41,480 --> 00:15:47,240
 we have the emotions or the hindrances liking disliking

266
00:15:47,240 --> 00:15:52,990
 drowsiness distraction doubt and so on all mind states that

267
00:15:52,990 --> 00:15:53,640
 arise

268
00:15:53,640 --> 00:15:59,080
 judgments that arise or or well states of mind that are

269
00:15:59,080 --> 00:16:00,760
 that are not

270
00:16:00,760 --> 00:16:07,240
 not neutral all of those states that's your hindrances

271
00:16:07,240 --> 00:16:10,910
 we have the senses so seeing hearing smelling tasting

272
00:16:10,910 --> 00:16:12,280
 feeling thinking

273
00:16:12,280 --> 00:16:17,720
 being mindful of all that so the four satipatanas are just

274
00:16:17,720 --> 00:16:17,800
 a

275
00:16:17,800 --> 00:16:22,840
 description of of us and our experience and the practice of

276
00:16:22,840 --> 00:16:23,880
 mindfulness based on

277
00:16:23,880 --> 00:16:28,040
 them of course is what leads to mindfulness

278
00:16:28,040 --> 00:16:38,960
 so that's the lakana dichatukha that is a i think a fairly

279
00:16:38,960 --> 00:16:39,240
 good

280
00:16:39,240 --> 00:16:43,300
 all well-rounded explanation of of mindfulness the last

281
00:16:43,300 --> 00:16:43,960
 thing this text

282
00:16:43,960 --> 00:16:47,400
 says which is also quite interesting is well

283
00:16:47,400 --> 00:16:51,960
 it's neat gives it gives a description of how you should

284
00:16:51,960 --> 00:16:54,360
 how it should be seen

285
00:16:54,360 --> 00:16:56,840
 it says

286
00:17:05,480 --> 00:17:12,760
 it says but or or furthermore mindfulness should be

287
00:17:12,760 --> 00:17:16,360
 mindfulness is like a pillar

288
00:17:16,360 --> 00:17:28,280
 in that it is well established stuck firmly established

289
00:17:28,280 --> 00:17:33,400
 in the object so the ordinary mind is and the texts do go

290
00:17:33,400 --> 00:17:34,200
 into detail about

291
00:17:34,200 --> 00:17:39,650
 this the ordinary mind is like a gourd a pumpkin let's say

292
00:17:39,650 --> 00:17:40,760
 a pumpkin floating on

293
00:17:40,760 --> 00:17:43,240
 the water

294
00:17:43,240 --> 00:17:49,400
 so the ordinary mind is you know it's bounced about by the

295
00:17:49,400 --> 00:17:52,520
 waves of experience

296
00:17:52,520 --> 00:17:57,240
 going with the current the ordinary mind is is very much

297
00:17:57,240 --> 00:18:01,730
 susceptible to experience to loss to gain to praise to

298
00:18:01,730 --> 00:18:03,560
 blame

299
00:18:03,560 --> 00:18:06,120
 and so on

300
00:18:06,120 --> 00:18:13,640
 but the mindful mind is like a pillar that's stuck in the

301
00:18:13,640 --> 00:18:14,360
 bottom of

302
00:18:14,360 --> 00:18:20,600
 in the stuck in the ground and the water can't

303
00:18:20,600 --> 00:18:25,080
 can't buffet it and it's not moved by the currents or the

304
00:18:25,080 --> 00:18:27,400
 vicissitudes

305
00:18:27,400 --> 00:18:31,320
 the mind is not moved by the vicissitudes of life

306
00:18:31,320 --> 00:18:38,120
 whether it's blame or praise or fame or loss or

307
00:18:38,120 --> 00:18:43,240
 happiness suffering the good or the bad a good meditation

308
00:18:43,240 --> 00:18:43,960
 session a bad

309
00:18:43,960 --> 00:18:48,120
 meditation session the mindful mind is not buffeted by good

310
00:18:48,120 --> 00:18:54,520
 or bad experiences it's not caught up by experiences so

311
00:18:54,520 --> 00:18:57,850
 when seeing or hearing or smelling or tasting or feeling or

312
00:18:57,850 --> 00:18:59,480
 thinking that

313
00:18:59,480 --> 00:19:04,680
 there is no reaction and that's no suffering

314
00:19:04,680 --> 00:19:09,640
 this right here this is the this is really the core of bud

315
00:19:09,640 --> 00:19:10,440
dhism just

316
00:19:10,440 --> 00:19:13,480
 this little paragraph

317
00:19:13,480 --> 00:19:17,960
 that's wonderful because i get feedback from people who

318
00:19:17,960 --> 00:19:18,600
 tell me

319
00:19:18,600 --> 00:19:21,460
 i think last night right one of the questions was basically

320
00:19:21,460 --> 00:19:23,160
 saying how

321
00:19:23,160 --> 00:19:26,440
 how great how amazed they were how remarkable it was

322
00:19:26,440 --> 00:19:31,480
 how well this works i think so too it's a big reason why i

323
00:19:31,480 --> 00:19:32,920
'm here

324
00:19:32,920 --> 00:19:38,190
 repeating these things on the internet the last thing it

325
00:19:38,190 --> 00:19:40,120
 says is

326
00:19:40,120 --> 00:19:45,890
 it should also be seen as mindfulness should also be seen

327
00:19:45,890 --> 00:19:46,200
 as

328
00:19:46,200 --> 00:19:55,480
 like a dawarika dawarika via like a doorkeeper a gatekeeper

329
00:19:57,000 --> 00:20:08,200
 because because it protects or guards the

330
00:20:08,200 --> 00:20:13,400
 doors starting with the eye the eye door and so on

331
00:20:13,400 --> 00:20:16,020
 the mindfulness is often i think i mentioned the

332
00:20:16,020 --> 00:20:17,240
 mindfulness is often

333
00:20:17,240 --> 00:20:21,650
 likened to a doorkeeper because all of our experiences come

334
00:20:21,650 --> 00:20:22,600
 through the

335
00:20:22,600 --> 00:20:27,240
 senses the eye the ear the nose the tongue the body and the

336
00:20:27,240 --> 00:20:31,080
 mind and

337
00:20:31,080 --> 00:20:38,840
 in a sense these are called the doors and so in a sense

338
00:20:38,840 --> 00:20:43,000
 um this is how the defilements get to us this is how

339
00:20:43,000 --> 00:20:45,790
 defilements enter our hearts when you experience something

340
00:20:45,790 --> 00:20:46,600
 and you're not

341
00:20:46,600 --> 00:20:49,880
 mindful of it so you're not guarding that door

342
00:20:49,880 --> 00:20:53,800
 then the defilements come liking disliking

343
00:20:53,800 --> 00:20:59,480
 anger frustration addiction worry stress all causes of

344
00:20:59,480 --> 00:21:02,200
 suffering come

345
00:21:02,200 --> 00:21:18,440
 and so mindfulness is this guard when you're mindful you're

346
00:21:18,440 --> 00:21:19,160
 mindful of the

347
00:21:19,160 --> 00:21:25,160
 senses and seeing it's just seeing

348
00:21:25,160 --> 00:21:29,320
 when you say to yourself seeing you're reminding yourself

349
00:21:29,320 --> 00:21:34,120
 when you remind yourself there's there's no reaction

350
00:21:34,120 --> 00:21:37,590
 this is this it's not good it's not bad it's not me it's

351
00:21:37,590 --> 00:21:38,680
 not mine

352
00:21:38,680 --> 00:21:43,480
 it is what it is that's what mindfulness gives us

353
00:21:43,480 --> 00:21:48,440
 mindfulness is a quality of that we cultivate

354
00:21:48,440 --> 00:21:52,520
 and that we become comfortable with a familiar with

355
00:21:52,520 --> 00:21:58,280
 and that becomes part of our patterned behavior it becomes

356
00:21:58,280 --> 00:21:59,480
 a habit

357
00:21:59,480 --> 00:22:03,490
 and through the practice of mindfulness of course this is

358
00:22:03,490 --> 00:22:04,520
 what leads to insight

359
00:22:04,520 --> 00:22:08,840
 and inside of course is what leads to freedom

360
00:22:08,840 --> 00:22:12,220
 so this is the path this is what the buddha said is the ik

361
00:22:12,220 --> 00:22:15,080
ayana manga

362
00:22:16,200 --> 00:22:20,700
 i've talked for quite a bit and what i think i'm going to

363
00:22:20,700 --> 00:22:21,160
 do is

364
00:22:21,160 --> 00:22:24,450
 obviously i'm gonna have to shorten that when i actually

365
00:22:24,450 --> 00:22:25,160
 give my talk

366
00:22:25,160 --> 00:22:29,840
 good to know um but i'm gonna give the second half tomorrow

367
00:22:29,840 --> 00:22:31,400
 i think

368
00:22:31,400 --> 00:22:35,320
 and if i'm here tomorrow if i don't die in the meantime

369
00:22:35,320 --> 00:22:39,560
 i'll give the second half tomorrow

370
00:22:40,440 --> 00:22:45,890
 okay so thank you all for coming out that's the demo for

371
00:22:45,890 --> 00:22:46,840
 tonight

372
00:22:46,840 --> 00:22:54,470
 see if there are any questions if i can get to the

373
00:22:54,470 --> 00:22:56,280
 questions

374
00:22:56,280 --> 00:23:07,000
 oh it looks good okay a bunch of questions oh no a bunch of

375
00:23:07,000 --> 00:23:09,400
 okay

376
00:23:11,320 --> 00:23:14,120
 questions

377
00:23:14,120 --> 00:23:21,800
 you guys can go you better not to sit and listen to these

378
00:23:21,800 --> 00:23:26,810
 not really related to the practice not always related to

379
00:23:26,810 --> 00:23:28,040
 the practice

380
00:23:28,040 --> 00:23:30,950
 should i only be mindful of my own actions throughout the

381
00:23:30,950 --> 00:23:31,640
 day or try by

382
00:23:31,640 --> 00:23:34,760
 focusing on the external environment as well

383
00:23:34,760 --> 00:32:28,390
 such as the immediate surroundings should try to be mindful

384
00:32:28,390 --> 00:23:38,600
 of whatever you

385
00:23:38,600 --> 00:23:43,400
 can i mean if you're clear what is the um field

386
00:23:43,400 --> 00:23:47,720
 or the the um

387
00:23:47,720 --> 00:23:53,720
 what's the word well the the boundaries of what it was

388
00:23:53,720 --> 00:23:56,680
 mindfulness because you can't be mindful of concepts that's

389
00:23:56,680 --> 00:23:57,240
 not part of

390
00:23:57,240 --> 00:24:00,040
 our practice i'll talk a little bit more about that

391
00:24:00,040 --> 00:24:03,960
 tomorrow um but as long as it's reality as long as

392
00:24:03,960 --> 00:24:07,240
 it's an expression of reality that's what you

393
00:24:07,240 --> 00:24:10,120
 should be mindful of

394
00:24:10,120 --> 00:24:14,680
 so as far as the external environment well you're you're

395
00:24:14,680 --> 00:24:17,880
 you're aware of your experience of it so seeing

396
00:24:17,880 --> 00:24:20,520
 you'd say seeing if it's hearing you would say hearing

397
00:24:20,520 --> 00:24:21,640
 hearing

398
00:24:21,640 --> 00:24:24,280
 that kind of thing

399
00:24:24,280 --> 00:24:30,520
 in regards to letting go of control and yet still doing the

400
00:24:30,520 --> 00:24:32,120
 things you're

401
00:24:32,120 --> 00:24:35,240
 responsible for

402
00:24:35,960 --> 00:24:39,480
 so

403
00:24:39,480 --> 00:24:52,740
 you're not actually asking a question i think i know what

404
00:24:52,740 --> 00:24:57,880
 you're getting at but

405
00:24:57,880 --> 00:25:01,560
 it's a little too complicated for me i'd like you to maybe

406
00:25:01,560 --> 00:25:05,340
 um pinpoint exactly what you want to know you want to know

407
00:25:05,340 --> 00:25:06,600
 about letting go of

408
00:25:06,600 --> 00:25:11,390
 letting go and yet still doing what is responsible well i

409
00:25:11,390 --> 00:25:12,200
 mean i can talk to

410
00:25:12,200 --> 00:25:16,520
 that talk about that um

411
00:25:16,520 --> 00:25:21,960
 what are you responsible for it's just um it's just a

412
00:25:21,960 --> 00:25:26,600
 duty you know and it's it's part of what is most efficient

413
00:25:26,600 --> 00:25:29,750
 so that doesn't change i mean just because you're very

414
00:25:29,750 --> 00:25:30,680
 mindful doesn't mean

415
00:25:30,680 --> 00:25:33,880
 you stop doing good things for people because

416
00:25:33,880 --> 00:25:37,320
 doing good things is generally the most efficient thing to

417
00:25:37,320 --> 00:25:41,240
 do it prevents a lot of stress and suffering

418
00:25:41,240 --> 00:25:44,760
 and complications

419
00:25:44,760 --> 00:25:50,450
 but um the rest of that i don't really have much to speak

420
00:25:50,450 --> 00:25:51,160
 about

421
00:25:51,160 --> 00:25:55,690
 consumed by extreme guilt and self-hatred over a mistake i

422
00:25:55,690 --> 00:25:56,600
 made a long

423
00:25:56,600 --> 00:25:58,920
 time ago how do you overcome a horrible deed you

424
00:25:58,920 --> 00:26:02,920
 committed well you you be mindful of the

425
00:26:02,920 --> 00:26:07,080
 guilt and self-hatred i mean it's a bad habit so habits are

426
00:26:07,080 --> 00:26:12,280
 hard to hard to overcome but it's part of our

427
00:26:12,280 --> 00:26:14,920
 practice i mean none of it none of this is really easy it

428
00:26:14,920 --> 00:26:16,120
 just takes time and a

429
00:26:16,120 --> 00:26:18,680
 lot of work

430
00:26:18,680 --> 00:26:21,670
 but it's important to realize that it's not good to feel

431
00:26:21,670 --> 00:26:22,760
 guilt it's not good to

432
00:26:22,760 --> 00:26:28,360
 hate yourself so so you can stop that stop actively

433
00:26:28,360 --> 00:26:33,530
 encouraging it that that's that's as much evil i mean that

434
00:26:33,530 --> 00:26:35,240
's really evil

435
00:26:35,240 --> 00:26:40,280
 it's evil as well as the evil thing you did

436
00:26:40,280 --> 00:26:45,240
 is it wholesome to celebrate the wonders and beauty of life

437
00:26:45,240 --> 00:26:51,660
 now it's kind of unwholesome because life is wretched and

438
00:26:51,660 --> 00:26:52,760
 ugly

439
00:26:52,760 --> 00:26:56,280
 no i don't know that life is a tough one because life

440
00:26:56,280 --> 00:27:01,720
 life is it's just a concept right but um you know our

441
00:27:01,720 --> 00:27:02,840
 existence is pretty

442
00:27:02,840 --> 00:27:06,440
 wretched so if you're celebrating it there might

443
00:27:06,440 --> 00:27:09,320
 be a problem there

444
00:27:09,320 --> 00:27:12,260
 what do i mean by that i talked about wretched right a

445
00:27:12,260 --> 00:27:12,920
 couple of days ago or

446
00:27:12,920 --> 00:27:17,000
 yesterday no a couple of days ago i think yeah

447
00:27:17,000 --> 00:27:19,320
 um

448
00:27:19,320 --> 00:27:24,440
 yeah i know you get intoxicated by these things and and it

449
00:27:24,440 --> 00:27:26,200
 creates a pattern of

450
00:27:26,200 --> 00:27:29,880
 of attachment doesn't actually make you happier

451
00:27:29,880 --> 00:27:32,070
 and it's very contentious i'm sure there are people who

452
00:27:32,070 --> 00:27:33,240
 disagree very strongly

453
00:27:33,240 --> 00:27:39,000
 with what i'm saying which of course is fine

454
00:27:39,000 --> 00:27:42,680
 the buddha said

455
00:27:42,680 --> 00:27:49,720
 come look at this world which is just decked out just like

456
00:27:49,720 --> 00:27:50,520
 a royal

457
00:27:50,520 --> 00:27:53,080
 chariot

458
00:27:55,960 --> 00:27:59,000
 we see danti

459
00:27:59,000 --> 00:28:04,840
 nutty

460
00:28:04,840 --> 00:28:10,520
 i don't remember the poly um the fool gets an

461
00:28:10,520 --> 00:28:14,440
 enamored by it you see a beautiful king's chariot

462
00:28:14,440 --> 00:28:17,880
 and you think how wonderful how marvelous it is

463
00:28:17,880 --> 00:28:22,000
 the world is like that all decked out so many beautiful

464
00:28:22,000 --> 00:28:22,600
 things

465
00:28:22,600 --> 00:28:27,160
 wonderful things but the wise are not have have no

466
00:28:27,160 --> 00:28:30,040
 connection with it

467
00:28:30,040 --> 00:28:32,860
 if the world is such a beautiful place why is there so much

468
00:28:32,860 --> 00:28:33,720
 suffering i mean

469
00:28:33,720 --> 00:28:37,480
 this is really the question why is there so much evil

470
00:28:37,480 --> 00:28:41,160
 why is there so much wretchedness

471
00:28:41,160 --> 00:28:45,240
 why are we not truly happy

472
00:28:45,240 --> 00:28:49,400
 can the world make us happy i think the answer is no

473
00:28:49,400 --> 00:28:52,050
 that's really the point so our wretchedness really comes

474
00:28:52,050 --> 00:28:52,760
 from trying to

475
00:28:52,760 --> 00:28:56,040
 find happiness in the world

476
00:28:56,040 --> 00:29:07,110
 happiness is it's possibly happy in the world but you have

477
00:29:07,110 --> 00:29:08,120
 to be

478
00:29:08,120 --> 00:29:11,640
 you have to be above it in a sense you have to be

479
00:29:11,640 --> 00:29:17,880
 uh independent not susceptible to the changes of life

480
00:29:17,880 --> 00:29:22,040
 right i mean it's not that you don't experience the changes

481
00:29:22,040 --> 00:29:25,000
 but our problem is that when we experience them we're not

482
00:29:25,000 --> 00:29:30,440
 we're not in tune with them

483
00:29:30,440 --> 00:29:33,940
 so i think we often mix this this wonder and appreciation

484
00:29:33,940 --> 00:29:34,840
 with being in

485
00:29:34,840 --> 00:29:37,720
 tune with reality a person who's in tune with reality

486
00:29:37,720 --> 00:29:41,140
 it's like a part they're a part of the landscape right here

487
00:29:41,140 --> 00:29:41,800
's the difference

488
00:29:41,800 --> 00:29:44,760
 that's because it's romantic to think of someone

489
00:29:44,760 --> 00:29:48,760
 standing there appreciating the mountain right but the the

490
00:29:48,760 --> 00:29:50,920
 classical zen

491
00:29:50,920 --> 00:29:55,080
 story is here i see here we sit the mountain me until

492
00:29:55,080 --> 00:29:58,640
 only the mountain remains because eventually the the

493
00:29:58,640 --> 00:29:59,400
 enlightened being

494
00:29:59,400 --> 00:30:02,860
 becomes a part of the landscape so rather than the person

495
00:30:02,860 --> 00:30:04,920
 admiring it

496
00:30:04,920 --> 00:30:08,520
 they are a part of it that's quite different you see

497
00:30:08,520 --> 00:30:11,560
 you don't become part of the landscape by admiring it

498
00:30:11,560 --> 00:30:14,950
 in fact you become quite enamored and will often do your

499
00:30:14,950 --> 00:30:16,360
 best to try and fix it

500
00:30:16,360 --> 00:30:20,680
 and make it the way you want it to be control it

501
00:30:20,680 --> 00:30:26,280
 right we love nature so much that we've destroyed it

502
00:30:26,280 --> 00:30:31,880
 trying to make it perfect so we can have the perfect

503
00:30:31,880 --> 00:30:37,640
 climate we've destroyed our climate

504
00:30:40,840 --> 00:30:44,120
 does meditation help with improving oneself yes

505
00:30:44,120 --> 00:30:48,120
 absolutely that's what meditation is for

506
00:30:48,120 --> 00:30:52,260
 in in one sense i suppose i mean in a deeper level it's for

507
00:30:52,260 --> 00:30:52,840
 letting go of

508
00:30:52,840 --> 00:30:57,480
 yourself but uh but that's an improvement

509
00:30:57,480 --> 00:31:01,130
 what do you think of meditation as a means to reaching

510
00:31:01,130 --> 00:31:02,520
 mundane goals instead

511
00:31:02,520 --> 00:31:07,320
 of reaching enlightenment or spiritual goals

512
00:31:07,320 --> 00:31:12,040
 did the buddha speak of this um i mean it depends how

513
00:31:12,040 --> 00:31:13,880
 mundane right

514
00:31:13,880 --> 00:31:17,420
 it can't be used to get greedy it can't be used to become

515
00:31:17,420 --> 00:31:18,200
 ambitious

516
00:31:18,200 --> 00:31:23,560
 so being successful in in business is probably not

517
00:31:23,560 --> 00:31:28,600
 probably not possible

518
00:31:28,600 --> 00:31:32,520
 but no i mean meditation improves things really

519
00:31:32,520 --> 00:31:36,040
 the problem with what you're what you're referring to

520
00:31:36,040 --> 00:31:38,910
 is that those things don't actually have any purpose or

521
00:31:38,910 --> 00:31:39,400
 benefit

522
00:31:39,400 --> 00:31:43,110
 so because mindfulness leads to wisdom it's going to show

523
00:31:43,110 --> 00:31:43,560
 you that

524
00:31:43,560 --> 00:31:46,410
 you're going to see that those things are useless and

525
00:31:46,410 --> 00:31:47,480
 certainly won't help you

526
00:31:47,480 --> 00:31:53,400
 accomplish them the other thing is that's a good thing

527
00:31:53,400 --> 00:31:57,800
 because those things are not worth obtaining anyway

528
00:31:57,800 --> 00:32:03,660
 but i mean as far as helping with health and general well-

529
00:32:03,660 --> 00:32:04,360
being

530
00:32:04,360 --> 00:32:08,460
 living your life more peacefully i mean that's very much a

531
00:32:08,460 --> 00:32:09,240
 part of it

532
00:32:09,240 --> 00:32:16,050
 okay so that's uh our that's all for the questions that's

533
00:32:16,050 --> 00:32:17,400
 all for tonight

534
00:32:17,400 --> 00:32:29,240
 thank you all for coming out have a good night

