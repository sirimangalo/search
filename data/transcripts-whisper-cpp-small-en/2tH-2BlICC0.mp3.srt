1
00:00:00,000 --> 00:00:11,120
 Hello, good evening everyone.

2
00:00:11,120 --> 00:00:11,600
 Broadcasting live.

3
00:00:11,600 --> 00:00:40,280
 Sorry, broadcasting live.

4
00:00:40,280 --> 00:01:01,680
 May 15th.

5
00:01:01,680 --> 00:01:13,110
 Today's quote is from the commentary to the Kudaka Bata,

6
00:01:13,110 --> 00:01:14,160
 the first book of the Kudaka

7
00:01:14,160 --> 00:01:17,880
 Nikaya.

8
00:01:17,880 --> 00:01:20,880
 This is the commentary to the Saranagamanakata.

9
00:01:20,880 --> 00:01:23,120
 Is that what it's called?

10
00:01:23,120 --> 00:01:24,120
 Sarana, no.

11
00:01:24,120 --> 00:01:25,120
 Saranataaya.

12
00:01:25,120 --> 00:01:28,120
 It's called the Saranataaya.

13
00:01:28,120 --> 00:01:32,240
 What's it actually called?

14
00:01:32,240 --> 00:01:36,680
 Saranatayam.

15
00:01:36,680 --> 00:01:44,690
 The Kudaka Bata is, Kudaka means small, short, could mean

16
00:01:44,690 --> 00:01:49,800
 minor or kind of secondary, insignificant

17
00:01:49,800 --> 00:01:52,800
 in a sense.

18
00:01:52,800 --> 00:02:01,850
 It might mean miscellaneous, but short is appropriate

19
00:02:01,850 --> 00:02:06,200
 because the Saranatayam is where

20
00:02:06,200 --> 00:02:11,230
 we find Buddhangsarananga-chang, Dhammangsarananga-chang,

21
00:02:11,230 --> 00:02:13,640
 Sanghangsarananga-chang.

22
00:02:13,640 --> 00:02:21,100
 I go to the Buddha as my Sarana as a refuge or as a

23
00:02:21,100 --> 00:02:25,000
 reflection, a recollection.

24
00:02:25,000 --> 00:02:27,000
 Refuge may be better.

25
00:02:27,000 --> 00:02:31,670
 Dhammangsarananga-chang, I go to the Dhamma, I go to the

26
00:02:31,670 --> 00:02:33,600
 Sangha for refuge.

27
00:02:33,600 --> 00:02:36,560
 Duttiampi-buddhangsarananga-chang.

28
00:02:36,560 --> 00:02:41,680
 For a second time, I go to the Buddha as a refuge.

29
00:02:42,680 --> 00:02:46,200
 I go to the Sangha as a refuge.

30
00:02:46,200 --> 00:02:49,880
 Duttiampi-buddhangsarananga-chang, for a third time.

31
00:02:49,880 --> 00:02:51,880
 And that's it.

32
00:02:51,880 --> 00:02:55,880
 Nine lines.

33
00:02:55,880 --> 00:03:04,880
 33 words, right?

34
00:03:04,880 --> 00:03:08,880
 33 words long.

35
00:03:08,880 --> 00:03:12,240
 It's a small section.

36
00:03:12,240 --> 00:03:15,600
 That's it.

37
00:03:15,600 --> 00:03:25,320
 Commentary has some interesting things to say about this.

38
00:03:25,320 --> 00:03:30,080
 First it talks about the Buddha, but we'll skip that part.

39
00:03:30,080 --> 00:03:31,440
 Only because it's not in our quote.

40
00:03:31,440 --> 00:03:38,570
 Our quote is from the second part that talks about the

41
00:03:38,570 --> 00:03:42,360
 three refuges as a whole.

42
00:03:42,360 --> 00:03:45,580
 And so there's some other similes here that hopefully I can

43
00:03:45,580 --> 00:03:46,240
...

44
00:03:46,240 --> 00:03:52,240
 I don't think this is in it.

45
00:03:52,240 --> 00:03:53,240
 Uh-oh.

46
00:03:53,240 --> 00:03:56,240
 I find it.

47
00:03:56,240 --> 00:04:02,040
 Actually quite long.

48
00:04:02,040 --> 00:04:05,040
 Here we are.

49
00:04:05,040 --> 00:04:07,880
 Puna-chando-yabuddho.

50
00:04:07,880 --> 00:04:11,040
 The Buddha is like the full moon.

51
00:04:11,040 --> 00:04:14,040
 Puna-chando-yabuddho.

52
00:04:14,040 --> 00:04:22,040
 Chanda-kirana-nikaro-vyate-nadesito-dhammo.

53
00:04:22,040 --> 00:04:29,040
 The dhamma taught by him is like the rays of the moon.

54
00:04:29,040 --> 00:04:37,040
 The many rays of light that shine from the moon.

55
00:04:37,040 --> 00:04:49,640
 Puna-chanda-kirana-samu-vadita-pimito-loko-vyasango.

56
00:04:49,640 --> 00:04:59,160
 The world-loka that is delighted, let's see, pleased, that

57
00:04:59,160 --> 00:05:08,160
 is lit up maybe, by the arisen,

58
00:05:08,160 --> 00:05:13,080
 by the rays of the moon.

59
00:05:13,080 --> 00:05:14,840
 So the dhamma lights up the world.

60
00:05:14,840 --> 00:05:17,600
 Lights up is probably the best.

61
00:05:17,600 --> 00:05:24,360
 Is the Sangha.

62
00:05:24,360 --> 00:05:27,600
 So the Sangha are those of us that are lit up.

63
00:05:27,600 --> 00:05:34,200
 The light, light comes to you, alokudbadi.

64
00:05:34,200 --> 00:05:38,040
 It's like you were in a dark room before and suddenly there

65
00:05:38,040 --> 00:05:38,880
 is light.

66
00:05:38,880 --> 00:05:43,960
 The dhamma opens your eyes to things that were there

67
00:05:43,960 --> 00:05:45,160
 already.

68
00:05:45,160 --> 00:05:52,440
 It doesn't bring anything new, not chiefly.

69
00:05:52,440 --> 00:05:57,120
 It teaches you about what's already there.

70
00:05:57,120 --> 00:06:04,220
 That's the whole of it.

71
00:06:04,220 --> 00:06:07,760
 They joke those people who practice breath meditation, they

72
00:06:07,760 --> 00:06:09,400
 say, "It teaches you what's

73
00:06:09,400 --> 00:06:10,400
 under your nose."

74
00:06:10,400 --> 00:06:16,020
 Because of course it uses the breath, they focus on the

75
00:06:16,020 --> 00:06:16,960
 nose.

76
00:06:16,960 --> 00:06:20,950
 But really it's here, the Buddha said, in this six-foot

77
00:06:20,950 --> 00:06:23,040
 frame, this is where you find

78
00:06:23,040 --> 00:06:27,640
 the beginning and the end of the universe.

79
00:06:27,640 --> 00:06:28,640
 Everything is already here.

80
00:06:28,640 --> 00:06:31,160
 You don't have to go far.

81
00:06:31,160 --> 00:06:38,770
 There was this monk in Bangkok who he would give an example

82
00:06:38,770 --> 00:06:41,000
 of how we really don't know

83
00:06:41,000 --> 00:06:43,800
 ourselves very well.

84
00:06:43,800 --> 00:06:48,180
 He asked this woman, she was 95 years old, he said, "How

85
00:06:48,180 --> 00:06:50,200
 many knuckles do you have on

86
00:06:50,200 --> 00:06:51,200
 one hand?"

87
00:06:51,200 --> 00:06:54,160
 He said, "Don't count, but just tell me.

88
00:06:54,160 --> 00:06:55,160
 Do you know?"

89
00:06:55,160 --> 00:06:56,160
 He said, "Don't know."

90
00:06:56,160 --> 00:06:59,600
 95 years old, you still don't know.

91
00:06:59,600 --> 00:07:03,680
 If you never counted, you don't know.

92
00:07:03,680 --> 00:07:07,070
 So when you say you know something like the back of your

93
00:07:07,070 --> 00:07:08,360
 hand, not so sure.

94
00:07:08,360 --> 00:07:11,460
 You never look.

95
00:07:11,460 --> 00:07:13,870
 So you can look at your hand, that's not very difficult to

96
00:07:13,870 --> 00:07:15,200
 figure out how many knuckles

97
00:07:15,200 --> 00:07:16,200
 you have.

98
00:07:16,200 --> 00:07:19,430
 But to understand your mind, how many people actually spend

99
00:07:19,430 --> 00:07:21,480
 the time learning about themselves,

100
00:07:21,480 --> 00:07:26,480
 about their mind?

101
00:07:26,480 --> 00:07:36,160
 So, this is the light, like the light of the moon.

102
00:07:36,160 --> 00:07:39,520
 The Buddha is like the moon.

103
00:07:39,520 --> 00:07:40,520
 That's the first one.

104
00:07:40,520 --> 00:07:43,520
 The second one is bala.

105
00:07:43,520 --> 00:07:48,680
 Bala is not foolish.

106
00:07:48,680 --> 00:07:59,470
 Not a problem here because it shouldn't be bala, it shouldn

107
00:07:59,470 --> 00:08:01,120
't be.

108
00:08:01,120 --> 00:08:04,200
 Let me see.

109
00:08:04,200 --> 00:08:07,000
 Or bala suriya, the newly arisen sun, I get it.

110
00:08:07,000 --> 00:08:08,400
 Bala means young in this case.

111
00:08:08,400 --> 00:08:10,400
 It usually means foolish.

112
00:08:10,400 --> 00:08:19,200
 Bala suriya is the dawn.

113
00:08:19,200 --> 00:08:30,200
 The Buddha is like the dawn.

114
00:08:30,200 --> 00:08:36,400
 See if I can get this.

115
00:08:36,400 --> 00:08:45,760
 Rasmi, got that one, jala.

116
00:08:45,760 --> 00:08:52,730
 The glow, the rest of the glow and the, the Dhamma with its

117
00:08:52,730 --> 00:08:55,440
 many qualities is like the

118
00:08:55,440 --> 00:08:57,280
 glow of the sun, I don't know.

119
00:08:57,280 --> 00:09:00,240
 It's like the qualities of the risen sun.

120
00:09:00,240 --> 00:09:01,240
 Same thing.

121
00:09:01,240 --> 00:09:08,000
 But clear about that.

122
00:09:08,000 --> 00:09:20,640
 My Pali is not great.

123
00:09:20,640 --> 00:09:25,960
 The darkness, Andha Ka, Andha Kaara is, Andha means blind,

124
00:09:25,960 --> 00:09:29,560
 Kaara means that which makes,

125
00:09:29,560 --> 00:09:33,520
 that which makes you blind, Andha Kaara is darkness.

126
00:09:33,520 --> 00:09:41,480
 Vihata means destroyed or dispelled.

127
00:09:41,480 --> 00:09:47,450
 The world that, whose darkness is destroyed by the sun is

128
00:09:47,450 --> 00:09:49,520
 like the Sangha.

129
00:09:49,520 --> 00:09:54,420
 So again, this light where the Buddha is like the moon, he

130
00:09:54,420 --> 00:09:55,960
 is like the sun.

131
00:09:55,960 --> 00:10:01,040
 And it lights up the world.

132
00:10:01,040 --> 00:10:02,040
 Because that's it.

133
00:10:02,040 --> 00:10:04,450
 You know, Buddhism isn't about, I had an interesting

134
00:10:04,450 --> 00:10:05,600
 conversation today.

135
00:10:05,600 --> 00:10:07,320
 I went to see my family today.

136
00:10:07,320 --> 00:10:10,700
 We talked about music.

137
00:10:10,700 --> 00:10:13,710
 And it was a good debate and I had to admit some points

138
00:10:13,710 --> 00:10:16,180
 because there's definitely a potential

139
00:10:16,180 --> 00:10:18,700
 to see good in, in music.

140
00:10:18,700 --> 00:10:24,230
 In the sense, in the worldly sense that it can strengthen

141
00:10:24,230 --> 00:10:27,200
 you, the rhythm, the trance

142
00:10:27,200 --> 00:10:32,920
 that it puts you in can strengthen you.

143
00:10:32,920 --> 00:10:35,980
 So what I couldn't probably quite get across as well as I

144
00:10:35,980 --> 00:10:37,840
 would have liked, it was about

145
00:10:37,840 --> 00:10:39,200
 five or six on one.

146
00:10:39,200 --> 00:10:43,360
 So I was vastly outnumbered.

147
00:10:43,360 --> 00:10:47,760
 My family's into music.

148
00:10:47,760 --> 00:10:49,880
 Is that the sensual, is it the sensual attachment?

149
00:10:49,880 --> 00:10:54,280
 I mean, we didn't talk about it, but it's a tough crowd.

150
00:10:54,280 --> 00:10:56,640
 Not very convinced.

151
00:10:56,640 --> 00:11:04,280
 Is the attachment to it, you know, in the mind.

152
00:11:04,280 --> 00:11:06,560
 You can be attached, attached to the sound.

153
00:11:06,560 --> 00:11:07,560
 You like it.

154
00:11:07,560 --> 00:11:09,040
 And liking is an addiction.

155
00:11:09,040 --> 00:11:12,510
 And there's so many things that you have to, there's a

156
00:11:12,510 --> 00:11:14,600
 worldview that you have to.

157
00:11:14,600 --> 00:11:17,480
 I guess a change in worldview.

158
00:11:17,480 --> 00:11:21,000
 But the reason I bring it up is because it's not about

159
00:11:21,000 --> 00:11:21,760
 dogma.

160
00:11:21,760 --> 00:11:23,880
 We can't just say music is bad.

161
00:11:23,880 --> 00:11:25,600
 We have to look at reality.

162
00:11:25,600 --> 00:11:28,280
 We're not about, we can't be.

163
00:11:28,280 --> 00:11:34,360
 If we want to be followed in Buddha, we can't accept dogma.

164
00:11:34,360 --> 00:11:35,880
 We can't follow dogma.

165
00:11:35,880 --> 00:11:39,360
 We can't believe things.

166
00:11:39,360 --> 00:11:42,920
 We can't follow things just because it's a belief.

167
00:11:42,920 --> 00:11:45,280
 We follow things because they're true.

168
00:11:45,280 --> 00:11:49,440
 If they are not true, we have to stop following them.

169
00:11:49,440 --> 00:11:53,600
 There's a challenge there, you know.

170
00:11:53,600 --> 00:11:56,840
 What can we know to be true?

171
00:11:56,840 --> 00:12:05,360
 And so it's not, I mean, it's not about the whole truth.

172
00:12:05,360 --> 00:12:07,910
 It's about two things, the good truth, the truth that is

173
00:12:07,910 --> 00:12:09,480
 useful, but also the truth that

174
00:12:09,480 --> 00:12:13,680
 we can know.

175
00:12:13,680 --> 00:12:19,120
 So any truth that we are going to find has to be normal.

176
00:12:19,120 --> 00:12:21,200
 And we have to come to know it.

177
00:12:21,200 --> 00:12:24,680
 That's how we become a true follower of the Buddha.

178
00:12:24,680 --> 00:12:26,520
 You practice his teachings.

179
00:12:26,520 --> 00:12:27,640
 We don't believe it.

180
00:12:27,640 --> 00:12:28,640
 We don't think it.

181
00:12:28,640 --> 00:12:31,240
 We don't suspect it.

182
00:12:31,240 --> 00:12:32,240
 We realize it.

183
00:12:32,240 --> 00:12:33,240
 It's a very simple thing.

184
00:12:33,240 --> 00:12:35,360
 So we study music, for example.

185
00:12:35,360 --> 00:12:41,280
 And you can say music is good, music is bad, music makes me

186
00:12:41,280 --> 00:12:41,640
 happy.

187
00:12:41,640 --> 00:12:44,320
 My main argument was that happiness doesn't make you happy.

188
00:12:44,320 --> 00:12:48,160
 This is a very good Buddhist argument.

189
00:12:48,160 --> 00:12:51,280
 It didn't really work because, well, when you're so opposed

190
00:12:51,280 --> 00:12:52,640
 to something, when your

191
00:12:52,640 --> 00:12:56,870
 view is very much opposed to it, so we have different views

192
00:12:56,870 --> 00:12:57,080
.

193
00:12:57,080 --> 00:13:02,720
 But you can argue, you can say happiness makes you happy.

194
00:13:02,720 --> 00:13:04,040
 Happiness doesn't lead to happiness.

195
00:13:04,040 --> 00:13:06,320
 It's not the cause of happiness.

196
00:13:06,320 --> 00:13:07,320
 It's not the cause of itself.

197
00:13:07,320 --> 00:13:09,830
 If it was, I mean, actually, it's kind of silly because if

198
00:13:09,830 --> 00:13:11,080
 it was, it would be a feedback

199
00:13:11,080 --> 00:13:12,080
 loop.

200
00:13:12,080 --> 00:13:13,080
 We'd always be happy.

201
00:13:13,080 --> 00:13:15,840
 Be happy once, you're happy forever.

202
00:13:15,840 --> 00:13:17,000
 It's not the case.

203
00:13:17,000 --> 00:13:18,000
 Happiness doesn't lead to happiness.

204
00:13:18,000 --> 00:13:21,310
 I suppose it's a bit simplistic and people would argue that

205
00:13:21,310 --> 00:13:23,160
 it's more complicated than

206
00:13:23,160 --> 00:13:24,160
 that.

207
00:13:24,160 --> 00:13:25,680
 But happiness doesn't lead to happiness.

208
00:13:25,680 --> 00:13:28,560
 It doesn't have that power.

209
00:13:28,560 --> 00:13:29,640
 Something leads to happiness.

210
00:13:29,640 --> 00:13:33,250
 We'd argue that things, goodness in all its forms, things

211
00:13:33,250 --> 00:13:34,960
 that we, well, I mean, that's

212
00:13:34,960 --> 00:13:35,960
 a tautology.

213
00:13:35,960 --> 00:13:38,440
 We call it goodness because it leads to happiness.

214
00:13:38,440 --> 00:13:40,360
 It leads to happiness because it's goodness.

215
00:13:40,360 --> 00:13:41,600
 Not really.

216
00:13:41,600 --> 00:13:44,960
 There are certain things that lead to happiness.

217
00:13:44,960 --> 00:13:49,360
 Happiness is not one of them.

218
00:13:49,360 --> 00:13:53,120
 But that's a good example because that's a claim that we

219
00:13:53,120 --> 00:13:54,040
 can test.

220
00:13:54,040 --> 00:13:55,720
 Does happiness lead to happiness?

221
00:13:55,720 --> 00:13:57,480
 Most people will argue things like that.

222
00:13:57,480 --> 00:14:00,600
 Of course, happiness is good in and of itself.

223
00:14:00,600 --> 00:14:05,280
 They'd argue, no, but let's investigate.

224
00:14:05,280 --> 00:14:06,360
 It's a problem, really.

225
00:14:06,360 --> 00:14:09,360
 People have views without investigating or they have views

226
00:14:09,360 --> 00:14:11,640
 based on their own experience,

227
00:14:11,640 --> 00:14:18,170
 anecdotal experience, without being systematic in their

228
00:14:18,170 --> 00:14:22,720
 exploration, their investigation.

229
00:14:22,720 --> 00:14:24,200
 Buddhism is about bringing light.

230
00:14:24,200 --> 00:14:25,840
 It's not about changing anything.

231
00:14:25,840 --> 00:14:29,440
 It's about bringing the purest light possible that we can,

232
00:14:29,440 --> 00:14:32,760
 the purest light we're able to.

233
00:14:32,760 --> 00:14:36,900
 It's making ourselves as objective as we can to see things

234
00:14:36,900 --> 00:14:39,000
 as clearly as we can and just

235
00:14:39,000 --> 00:14:41,000
 accepting the truth.

236
00:14:41,000 --> 00:14:44,560
 In fact, it's not something you even have to accept once

237
00:14:44,560 --> 00:14:45,480
 you see it.

238
00:14:45,480 --> 00:14:48,360
 It changes you.

239
00:14:48,360 --> 00:14:50,700
 Changes your whole perspective.

240
00:14:50,700 --> 00:14:52,140
 You can't unsee it.

241
00:14:52,140 --> 00:14:55,140
 You can't unknow it.

242
00:14:55,140 --> 00:14:56,640
 Not that you would ever want to, right?

243
00:14:56,640 --> 00:14:58,720
 I mean, what's better in a dark room?

244
00:14:58,720 --> 00:15:00,560
 What's better in a room?

245
00:15:00,560 --> 00:15:03,390
 To know where everything is or to not know where everything

246
00:15:03,390 --> 00:15:03,760
 is?

247
00:15:03,760 --> 00:15:07,360
 You want to forget where the chairs are so you bump into

248
00:15:07,360 --> 00:15:08,360
 them again?

249
00:15:08,360 --> 00:15:14,120
 What's better that you know?

250
00:15:14,120 --> 00:15:15,120
 One and dahaka, purisoviyamudu.

251
00:15:15,120 --> 00:15:16,120
 One and dahaka.

252
00:15:16,120 --> 00:15:17,120
 I think dahaka.

253
00:15:17,120 --> 00:15:18,120
 Let's look at...

254
00:15:18,120 --> 00:15:22,880
 I don't know dahaka.

255
00:15:22,880 --> 00:15:25,280
 It must be like leader.

256
00:15:25,280 --> 00:15:34,040
 I don't think we have dahaka.

257
00:15:34,040 --> 00:15:37,280
 Someone who leads you out of a forest, I think.

258
00:15:37,280 --> 00:15:38,280
 Dahana.

259
00:15:38,280 --> 00:15:39,280
 Dahana.

260
00:15:39,280 --> 00:15:40,280
 Burning?

261
00:15:40,280 --> 00:15:41,280
 Go.

262
00:15:41,280 --> 00:15:48,960
 Buddha is like someone who burns down the forest.

263
00:15:48,960 --> 00:15:50,960
 I'm not sure.

264
00:15:50,960 --> 00:15:56,000
 Kilesa, one and dahana.

265
00:15:56,000 --> 00:15:57,560
 Buddha is one who burns down a forest.

266
00:15:57,560 --> 00:16:02,800
 He's a person who burns down a forest, I think.

267
00:16:02,800 --> 00:16:07,280
 The fire burning the forest is the dahana.

268
00:16:07,280 --> 00:16:14,280
 One and dahana gi viya, kilesa one and dahana udhamu.

269
00:16:14,280 --> 00:16:27,440
 The fire burning the forest, which is the...

270
00:16:27,440 --> 00:16:32,800
 Like the fire burning the forest is the dhamma that burns

271
00:16:32,800 --> 00:16:34,800
 the forest of kilesa, the forest

272
00:16:34,800 --> 00:16:36,800
 of defilements.

273
00:16:36,800 --> 00:16:44,400
 Dada one...

274
00:16:44,400 --> 00:16:48,960
 Keta Bhutu viya, Bhumi Bhago.

275
00:16:48,960 --> 00:16:53,040
 No, I'm not going to get this one.

276
00:16:53,040 --> 00:16:58,880
 Dada kilesata punya keta Bhutu sangho.

277
00:16:58,880 --> 00:17:01,920
 I think it means the field.

278
00:17:01,920 --> 00:17:08,040
 That's the state of the forest being...

279
00:17:08,040 --> 00:17:13,400
 No, the thing, no?

280
00:17:13,400 --> 00:17:15,720
 Bhumi Bhago.

281
00:17:15,720 --> 00:17:25,960
 That portion of earth that has become a field through the

282
00:17:25,960 --> 00:17:31,440
 burning of the forest.

283
00:17:31,440 --> 00:17:33,800
 Through the forest having been burned.

284
00:17:33,800 --> 00:17:38,880
 Once the forest is consumed by fire, you have a field.

285
00:17:38,880 --> 00:17:39,960
 It's not the best imagery.

286
00:17:39,960 --> 00:17:43,810
 We tend to like our forests and the Buddha liked forests as

287
00:17:43,810 --> 00:17:45,840
 well, but commentator here

288
00:17:45,840 --> 00:17:51,320
 has chosen some fairly strong imagery.

289
00:17:51,320 --> 00:17:52,320
 But it's clever.

290
00:17:52,320 --> 00:17:53,800
 It's clever because...

291
00:17:53,800 --> 00:17:54,800
 And this is the sangha.

292
00:17:54,800 --> 00:17:57,240
 The sangha is like that field.

293
00:17:57,240 --> 00:18:01,600
 Once you've burnt down the forest, you can plant stuff in

294
00:18:01,600 --> 00:18:02,120
 it.

295
00:18:02,120 --> 00:18:03,440
 And so you have the...

296
00:18:03,440 --> 00:18:08,780
 Once the defilements are burnt down, that person who has

297
00:18:08,780 --> 00:18:11,800
 become a field of merit, punya

298
00:18:11,800 --> 00:18:12,800
 keta.

299
00:18:12,800 --> 00:18:17,800
 They are an incomparable field of merit.

300
00:18:17,800 --> 00:18:20,320
 This is the sangha.

301
00:18:20,320 --> 00:18:25,890
 They are incredible people to associate with, enlightened

302
00:18:25,890 --> 00:18:26,960
 beings.

303
00:18:26,960 --> 00:18:29,560
 You can become a better person.

304
00:18:29,560 --> 00:18:34,520
 You can do good deeds around them like nothing else.

305
00:18:34,520 --> 00:18:40,880
 Even just giving them a gift.

306
00:18:40,880 --> 00:18:44,350
 Even just giving a gift to the Buddha or one of his chief

307
00:18:44,350 --> 00:18:46,980
 disciples or something or supporting

308
00:18:46,980 --> 00:18:49,800
 anyone who practices meditation is awesome.

309
00:18:49,800 --> 00:18:54,440
 They're a great field of merit.

310
00:18:54,440 --> 00:18:56,480
 So he's not saying burn down forests or burning down

311
00:18:56,480 --> 00:18:57,040
 forests.

312
00:18:57,040 --> 00:18:58,040
 It's a good thing.

313
00:18:58,040 --> 00:19:00,120
 But it's a curious...

314
00:19:00,120 --> 00:19:03,800
 It's a clever imagery because we always talk about the san

315
00:19:03,800 --> 00:19:05,920
gha as being a field of merit.

316
00:19:05,920 --> 00:19:11,400
 Mahamigoh via Buddha, salida utti uyadhamo.

317
00:19:11,400 --> 00:19:16,440
 So the Buddha is like a great rain cloud.

318
00:19:16,440 --> 00:19:20,600
 Salida utti, the rain...

319
00:19:20,600 --> 00:19:25,000
 The water of the rain is like the dhamma.

320
00:19:25,000 --> 00:19:27,080
 What good is water from rain?

321
00:19:27,080 --> 00:19:38,810
 Utini pa tupasamitareno viyajanapado upasamitakide sarino

322
00:19:38,810 --> 00:19:40,400
 sangho.

323
00:19:40,400 --> 00:19:43,400
 What is rain?

324
00:19:43,400 --> 00:19:50,120
 Dust, that removes the dust.

325
00:19:50,120 --> 00:19:56,720
 Upasamitare which removes basically or it actually appeases

326
00:19:56,720 --> 00:20:00,520
 but that's not right here.

327
00:20:00,520 --> 00:20:02,520
 Upasamitare.

328
00:20:02,520 --> 00:20:04,920
 The dust is appeased.

329
00:20:04,920 --> 00:20:07,640
 The dust is cleansed.

330
00:20:07,640 --> 00:20:12,480
 That's just loosely translated.

331
00:20:12,480 --> 00:20:19,360
 The dust that is removed through the falling of the rain on

332
00:20:19,360 --> 00:20:23,560
 the jandapado, on the countryside,

333
00:20:23,560 --> 00:20:25,480
 is like the sangha.

334
00:20:25,480 --> 00:20:27,480
 Yes.

335
00:20:27,480 --> 00:20:30,480
 Is like the sangha or the sangha is like that.

336
00:20:30,480 --> 00:20:39,800
 The sangha who have pacified the dust of defilements.

337
00:20:39,800 --> 00:20:43,320
 So they have cleansed the dust of defilements from their

338
00:20:43,320 --> 00:20:43,920
 mind.

339
00:20:43,920 --> 00:20:50,600
 They have wiped it away from their minds.

340
00:20:50,600 --> 00:20:55,770
 The dust is the things that make our mind dirty, make our

341
00:20:55,770 --> 00:20:58,360
 minds muddled, muddied in

342
00:20:58,360 --> 00:21:02,840
 the sense of not being able to see clearly.

343
00:21:02,840 --> 00:21:03,840
 Susara...

344
00:21:03,840 --> 00:21:04,840
 Oh no, that's the third one, right?

345
00:21:04,840 --> 00:21:05,840
 That's the sangha.

346
00:21:05,840 --> 00:21:10,770
 The sangha who have cleansed their defilements using the

347
00:21:10,770 --> 00:21:13,520
 rain of the dhamma taught by the

348
00:21:13,520 --> 00:21:18,680
 rain cloud of the Buddha.

349
00:21:18,680 --> 00:21:19,680
 So this is cleansing.

350
00:21:19,680 --> 00:21:22,800
 We haven't gotten to the doctor yet.

351
00:21:22,800 --> 00:21:25,120
 That's the quote, today's quote.

352
00:21:25,120 --> 00:21:29,120
 Susarathi vyabuddho.

353
00:21:29,120 --> 00:21:33,280
 Sarathi is a charioteer, isn't it?

354
00:21:33,280 --> 00:21:34,280
 Yes.

355
00:21:34,280 --> 00:21:38,040
 Buddha is like a skilled driver.

356
00:21:38,040 --> 00:21:39,040
 Shofar.

357
00:21:39,040 --> 00:21:42,520
 No, not a shofar.

358
00:21:42,520 --> 00:21:43,520
 Charioteer.

359
00:21:43,520 --> 00:21:47,520
 Asadjanya uyinayupayo vyadambho.

360
00:21:47,520 --> 00:21:59,680
 Asadjanya is a good horse that is trained and skillful.

361
00:21:59,680 --> 00:22:02,560
 Upaya is skillful.

362
00:22:02,560 --> 00:22:10,320
 No, the skill, aha, the skill in training horses of good

363
00:22:10,320 --> 00:22:14,560
 breeding is like the dhamma.

364
00:22:14,560 --> 00:22:18,000
 So a charioteer has to know how to train horses.

365
00:22:18,000 --> 00:22:23,400
 This is a horse chariot, right?

366
00:22:23,400 --> 00:22:27,620
 Nowadays it would be the skill in driving a car, race car

367
00:22:27,620 --> 00:22:28,960
 driver maybe.

368
00:22:28,960 --> 00:22:38,680
 Subhinita asadjanya samuho vyasangho.

369
00:22:38,680 --> 00:22:46,070
 The samuha is a multitude, the well-trained multitude of

370
00:22:46,070 --> 00:22:47,520
 horses.

371
00:22:47,520 --> 00:22:51,680
 It's like the sangha, the herd of horses, right?

372
00:22:51,680 --> 00:22:53,640
 It's like the sangha.

373
00:22:53,640 --> 00:22:57,080
 So Buddhism is a training, very important.

374
00:22:57,080 --> 00:22:59,850
 This meditation isn't about blissing out or enjoying

375
00:22:59,850 --> 00:23:00,600
 yourself.

376
00:23:00,600 --> 00:23:04,870
 It's about training your mind, training your mind in

377
00:23:04,870 --> 00:23:07,560
 clarity and seeing things as they

378
00:23:07,560 --> 00:23:13,080
 are and training your mind through clarity.

379
00:23:13,080 --> 00:23:18,880
 The clarity trains you, trains you to do and say and think

380
00:23:18,880 --> 00:23:21,680
 the right things because you

381
00:23:21,680 --> 00:23:26,070
 know what's right and what's wrong for yourself without any

382
00:23:26,070 --> 00:23:28,560
 kind of brainwashing or dogma.

383
00:23:28,560 --> 00:23:32,320
 I don't know if I should go on.

384
00:23:32,320 --> 00:23:34,800
 It looks like there's tons and tons and tons of these.

385
00:23:34,800 --> 00:23:38,760
 This would be a good commentary to translate.

386
00:23:38,760 --> 00:23:40,280
 I think I'll stop there though.

387
00:23:40,280 --> 00:23:41,520
 The doctor one comes up.

388
00:23:41,520 --> 00:23:45,480
 So the quote tonight is, "Can we skip ahead to the doctor?"

389
00:23:45,480 --> 00:23:50,960
 I don't know if I can even find it.

390
00:23:50,960 --> 00:23:55,400
 Maybe it's coming up.

391
00:23:55,400 --> 00:23:57,240
 No.

392
00:23:57,240 --> 00:24:00,840
 There's tons.

393
00:24:00,840 --> 00:24:07,840
 There's like 50 or 60 different, well, 40, 20, 30, 40.

394
00:24:07,840 --> 00:24:09,400
 There's lots of them.

395
00:24:09,400 --> 00:24:11,240
 I think we'll end it there.

396
00:24:11,240 --> 00:24:16,080
 So the quote was, "The Buddha is a skilled physician, like

397
00:24:16,080 --> 00:24:18,240
 a physician who is able to

398
00:24:18,240 --> 00:24:19,880
 heal the sickness."

399
00:24:19,880 --> 00:24:25,200
 So it was the sickness of defilements.

400
00:24:25,200 --> 00:24:28,160
 The Dhamma is like the medicine, rightly applied.

401
00:24:28,160 --> 00:24:32,470
 The Sangha with their defilements cured are like people

402
00:24:32,470 --> 00:24:35,000
 destroyed, restored to health

403
00:24:35,000 --> 00:24:44,400
 by that medicine.

404
00:24:44,400 --> 00:24:45,840
 Rightly applied, no.

405
00:24:45,840 --> 00:24:49,460
 Physician-like medicine, the Dhamma has to be rightly

406
00:24:49,460 --> 00:24:50,360
 applied.

407
00:24:50,360 --> 00:24:53,760
 You can't just read the Dhamma and decide to practice it.

408
00:24:53,760 --> 00:25:02,670
 It's important to have the direction, guidance, support

409
00:25:02,670 --> 00:25:05,960
 over these things.

410
00:25:05,960 --> 00:25:09,500
 So if you have any important questions, it's been a long

411
00:25:09,500 --> 00:25:11,400
 day, so I'm not going to stay

412
00:25:11,400 --> 00:25:12,400
 here too long.

413
00:25:12,400 --> 00:25:33,960
 If you got some important meditative questions.

414
00:25:33,960 --> 00:25:35,480
 Simon's trying to explain music.

415
00:25:35,480 --> 00:25:38,780
 No, the other thing I wanted to say about music is that in

416
00:25:38,780 --> 00:25:40,440
 a worldly sense, it could

417
00:25:40,440 --> 00:25:41,600
 be healthy.

418
00:25:41,600 --> 00:25:43,400
 They say, "Well, music is good for you.

419
00:25:43,400 --> 00:25:44,640
 It actually leads to health."

420
00:25:44,640 --> 00:25:47,790
 They were arguing that there are cases of people being

421
00:25:47,790 --> 00:25:49,280
 cured through music.

422
00:25:49,280 --> 00:25:53,400
 So in a worldly sense, I could see how the rhythm would be

423
00:25:53,400 --> 00:25:55,200
 healthy for the body.

424
00:25:55,200 --> 00:25:59,730
 And even for the mind, rhythm puts you in a trance, and so

425
00:25:59,730 --> 00:26:02,240
 you're in a blissful space.

426
00:26:02,240 --> 00:26:13,040
 Whether that's right or wrong, good or bad, it's a lot of

427
00:26:13,040 --> 00:26:15,040
 delusion involved with good

428
00:26:15,040 --> 00:26:17,560
 health of course, but that's not a big deal.

429
00:26:17,560 --> 00:26:22,000
 Worldly sense, okay, we agree that there are some states of

430
00:26:22,000 --> 00:26:24,200
 mind that are trance-like that

431
00:26:24,200 --> 00:26:27,120
 can be brought about through music.

432
00:26:27,120 --> 00:26:30,060
 The bigger your problem is the sensuality, just the

433
00:26:30,060 --> 00:26:32,120
 enjoyment of the sound that becomes

434
00:26:32,120 --> 00:26:35,800
 addictive and it's just a minor addiction.

435
00:26:35,800 --> 00:26:36,800
 Nothing huge.

436
00:26:36,800 --> 00:26:39,560
 It's not breaking the five precepts.

437
00:26:39,560 --> 00:26:45,550
 Yeah, I mean, and then there's bad music, like music that

438
00:26:45,550 --> 00:26:48,120
 teaches you how to be a bad

439
00:26:48,120 --> 00:26:55,200
 person, gangster rap, that kind of stuff of course.

440
00:26:55,200 --> 00:26:58,360
 But my brother's a musician, it was a difficult argument.

441
00:26:58,360 --> 00:27:02,720
 I don't want to get into it too much.

442
00:27:02,720 --> 00:27:05,520
 Okay, I see that there are no pertinent questions.

443
00:27:05,520 --> 00:27:11,210
 If anyone had pressing questions, if anyone had pressing

444
00:27:11,210 --> 00:27:13,360
 questions, I would, I think they

445
00:27:13,360 --> 00:27:16,910
 would have been posted by now, so I'm going to say good

446
00:27:16,910 --> 00:27:17,600
night.

447
00:27:17,600 --> 00:27:20,970
 Tomorrow morning I'm off to New York with two other monks

448
00:27:20,970 --> 00:27:22,800
 and Michael's our driver.

449
00:27:22,800 --> 00:27:28,500
 So probably nothing tomorrow, but actually maybe if I can

450
00:27:28,500 --> 00:27:31,480
 get Wi-Fi in this monastery,

451
00:27:31,480 --> 00:27:37,610
 Bikubodi's monastery, I'll try and broadcast something, we

452
00:27:37,610 --> 00:27:38,720
'll see.

453
00:27:38,720 --> 00:27:39,720
 Anyway, goodnight.

454
00:27:39,720 --> 00:27:40,720
 Thank you.

455
00:27:40,720 --> 00:27:41,720
 Thank you.

456
00:27:41,720 --> 00:27:41,720
 Thank you.

457
00:27:41,720 --> 00:27:42,720
 Thank you.

458
00:27:42,720 --> 00:27:43,720
 Thank you.

459
00:27:43,720 --> 00:27:44,720
 Thank you.

