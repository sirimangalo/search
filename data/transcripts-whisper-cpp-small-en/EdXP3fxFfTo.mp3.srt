1
00:00:00,000 --> 00:00:03,640
 OK, good evening, YouTube.

2
00:00:03,640 --> 00:00:07,080
 We are now broadcasting YouTube live.

3
00:00:07,080 --> 00:00:12,800
 And we can link the Hangout to our chat.

4
00:00:12,800 --> 00:00:19,880
 So if anybody wants to join the Hangout,

5
00:00:19,880 --> 00:00:21,200
 come on and show their face.

6
00:00:21,200 --> 00:00:24,080
 You're welcome to.

7
00:00:24,080 --> 00:00:30,320
 If you want to hide behind the text, you can do so.

8
00:00:30,320 --> 00:00:39,520
 So I have-- do you remember how I was talking about--

9
00:00:39,520 --> 00:00:47,400
 was I ranting on here about this Muslim group on campus?

10
00:00:47,400 --> 00:00:49,200
 Maybe not on here.

11
00:00:49,200 --> 00:00:56,520
 I told you guys about this interfaith cafe.

12
00:00:56,520 --> 00:00:57,800
 Can I tell you about that?

13
00:00:57,800 --> 00:00:59,280
 Yes.

14
00:00:59,280 --> 00:01:00,960
 And how everyone-- they didn't really

15
00:01:00,960 --> 00:01:02,280
 seem all that interested in peace.

16
00:01:02,280 --> 00:01:05,360
 It was more like our religion is great.

17
00:01:05,360 --> 00:01:11,440
 And then in follow up to that, after Paris,

18
00:01:11,440 --> 00:01:15,760
 the Muslim Association on campus has really aggressively

19
00:01:15,760 --> 00:01:30,920
 sought to appease people's or improve the people's opinion

20
00:01:30,920 --> 00:01:32,760
 of Islam.

21
00:01:32,760 --> 00:01:36,600
 So today, after class, I was in the student center

22
00:01:36,600 --> 00:01:41,840
 and I was meeting up with my friend and her friend.

23
00:01:41,840 --> 00:01:50,840
 And because she had had a breakdown,

24
00:01:50,840 --> 00:01:53,680
 I was there to talk to her.

25
00:01:53,680 --> 00:01:58,040
 But in the entryway, they've got all these tables

26
00:01:58,040 --> 00:01:58,800
 for all the clubs.

27
00:01:58,800 --> 00:02:04,400
 And I've seen in the big area, somehow they

28
00:02:04,400 --> 00:02:05,920
 were able to put up all these banners.

29
00:02:05,920 --> 00:02:09,240
 And one of the big banners is this big black banner

30
00:02:09,240 --> 00:02:12,160
 with red lettering.

31
00:02:12,160 --> 00:02:16,600
 A black banner with red lettering saying,

32
00:02:16,600 --> 00:02:21,400
 are Islam and the West incompatible

33
00:02:21,400 --> 00:02:22,960
 or something like that?

34
00:02:22,960 --> 00:02:29,080
 And then there is Islam is about justice, about inequality,

35
00:02:29,080 --> 00:02:31,240
 and that's it.

36
00:02:31,240 --> 00:02:33,800
 And so I've been complaining about it,

37
00:02:33,800 --> 00:02:37,520
 saying that why isn't it about peace?

38
00:02:37,520 --> 00:02:45,480
 And if they're trying to give a positive image,

39
00:02:45,480 --> 00:02:47,120
 why are they using black and red?

40
00:02:47,120 --> 00:02:52,200
 And why are they worried about Islam being compatible?

41
00:02:52,200 --> 00:02:55,440
 I mean, the wording of it is really strange.

42
00:02:55,440 --> 00:02:56,200
 I was my father.

43
00:02:56,200 --> 00:02:59,360
 I was telling my father about this.

44
00:02:59,360 --> 00:03:00,520
 They're not saying peace.

45
00:03:00,520 --> 00:03:03,800
 And I felt like they weren't at this interfaith cafe.

46
00:03:03,800 --> 00:03:05,720
 They also weren't.

47
00:03:05,720 --> 00:03:09,600
 I know, not at the end of it, there was a rally for Beirut

48
00:03:09,600 --> 00:03:12,680
 because there was a bombing and there was something in

49
00:03:12,680 --> 00:03:14,080
 Beirut

50
00:03:14,080 --> 00:03:17,000
 and in Paris.

51
00:03:17,000 --> 00:03:20,280
 And this guy stood up, who I guess was a Muslim,

52
00:03:20,280 --> 00:03:21,880
 and he was talking about Islam.

53
00:03:21,880 --> 00:03:23,440
 And he never really said peace.

54
00:03:23,440 --> 00:03:26,720
 It was like he was beating around the bush.

55
00:03:26,720 --> 00:03:28,960
 And so I get the feeling these people aren't interested

56
00:03:28,960 --> 00:03:30,680
 in peace exactly.

57
00:03:30,680 --> 00:03:33,120
 And so this guy in the entryway, this guy said,

58
00:03:33,120 --> 00:03:35,200
 can I take 10 seconds of your time?

59
00:03:35,200 --> 00:03:36,320
 And I had no idea what he was.

60
00:03:36,320 --> 00:03:38,480
 I didn't see his table and what he was holding.

61
00:03:38,480 --> 00:03:39,880
 And I looked at him and I said, one--

62
00:03:39,880 --> 00:03:44,040
 he was going to start counting.

63
00:03:44,040 --> 00:03:45,240
 And then he starts talking.

64
00:03:45,240 --> 00:03:46,800
 And then he pulls out this little thing,

65
00:03:46,800 --> 00:03:50,440
 which is a smaller version that says Islam in the West.

66
00:03:50,440 --> 00:03:51,840
 And I started ranting at him.

67
00:03:51,840 --> 00:03:54,680
 I said, look, you--

68
00:03:54,680 --> 00:03:55,680
 this is not helping.

69
00:03:55,680 --> 00:03:58,120
 And I said, why doesn't it say peace?

70
00:03:58,120 --> 00:04:00,160
 And he said, oh, well, Islam means peace.

71
00:04:00,160 --> 00:04:01,200
 Islam doesn't mean peace.

72
00:04:01,200 --> 00:04:02,400
 I'm pretty sure.

73
00:04:02,400 --> 00:04:04,440
 Islam means surrender, I think, right?

74
00:04:04,440 --> 00:04:05,160
 Is that the word?

75
00:04:05,160 --> 00:04:10,400
 And I said, well, why doesn't it say that?

76
00:04:10,400 --> 00:04:12,400
 Why are you concerned with Islam in the West?

77
00:04:12,400 --> 00:04:15,760
 If it said Islam in the peace, I'd go.

78
00:04:15,760 --> 00:04:16,920
 And he said, well, if you come, you'll

79
00:04:16,920 --> 00:04:18,240
 feel very peaceful vibes.

80
00:04:18,240 --> 00:04:19,360
 And I said, well, looking at that,

81
00:04:19,360 --> 00:04:20,640
 I don't feel peaceful vibes.

82
00:04:20,640 --> 00:04:21,720
 It's black and red.

83
00:04:21,720 --> 00:04:22,880
 If you were looking--

84
00:04:22,880 --> 00:04:24,000
 and he actually greeted me.

85
00:04:24,000 --> 00:04:27,040
 He said, yeah, well, that's true.

86
00:04:27,040 --> 00:04:28,680
 So I really gave him a piece of my mind.

87
00:04:28,680 --> 00:04:30,560
 And I think I really got through to him

88
00:04:30,560 --> 00:04:32,680
 whether it's going to make any influence or not.

89
00:04:32,680 --> 00:04:33,920
 But I'm not impressed.

90
00:04:33,920 --> 00:04:35,040
 And I told him that.

91
00:04:35,040 --> 00:04:38,360
 I said, you guys don't have your heads on straight.

92
00:04:38,360 --> 00:04:41,080
 And I think the Christians, as well, for the most part,

93
00:04:41,080 --> 00:04:42,120
 don't have their head on straight.

94
00:04:42,120 --> 00:04:44,320
 And people are not really interested in peace.

95
00:04:44,320 --> 00:04:49,400
 And that's shameful on both sides.

96
00:04:49,400 --> 00:04:52,960
 If you want to improve the image of your religion,

97
00:04:52,960 --> 00:04:54,720
 come out and say it.

98
00:04:54,720 --> 00:04:56,200
 Don't be afraid to say it.

99
00:04:56,200 --> 00:04:57,840
 My religion is about peace.

100
00:04:57,840 --> 00:04:59,360
 I believe in peace.

101
00:04:59,360 --> 00:05:01,680
 And that's it.

102
00:05:03,320 --> 00:05:08,360
 I denounce any kind of violence, I think.

103
00:05:08,360 --> 00:05:09,000
 But they don't.

104
00:05:09,000 --> 00:05:11,240
 I think, for the most part, they're happy about violence.

105
00:05:11,240 --> 00:05:13,040
 And they want violence.

106
00:05:13,040 --> 00:05:14,320
 Well, maybe that's going too far.

107
00:05:14,320 --> 00:05:19,200
 But if someone wrongs you, you hurt them.

108
00:05:19,200 --> 00:05:21,290
 That kind of thing, the eye for the eye and that kind of

109
00:05:21,290 --> 00:05:21,720
 thing.

110
00:05:21,720 --> 00:05:25,840
 So this thing says justice and equality.

111
00:05:25,840 --> 00:05:27,040
 And I was thinking about that.

112
00:05:27,040 --> 00:05:30,880
 Because justice doesn't do it for me.

113
00:05:30,880 --> 00:05:33,840
 Because everyone has their own idea of justice, right?

114
00:05:33,840 --> 00:05:36,310
 If your justice is like the Old Testament, an eye for an

115
00:05:36,310 --> 00:05:36,560
 eye,

116
00:05:36,560 --> 00:05:42,760
 well, that makes everybody blind in one eye.

117
00:05:42,760 --> 00:05:49,440
 And equality, as well.

118
00:05:49,440 --> 00:05:52,480
 I mean, if your religion teaches horrible things, which,

119
00:05:52,480 --> 00:05:56,600
 I don't know, I think many of these religions,

120
00:05:56,600 --> 00:05:59,800
 Christianity, Judaism, Islam, do,

121
00:05:59,800 --> 00:06:01,880
 then equality is meaningless.

122
00:06:01,880 --> 00:06:06,200
 If everyone has to follow horrible teachings equally.

123
00:06:06,200 --> 00:06:07,800
 I mean, I guess they're addressing the idea

124
00:06:07,800 --> 00:06:15,680
 that women are unequal in most of these religions.

125
00:06:15,680 --> 00:06:16,960
 I don't know about Christianity.

126
00:06:16,960 --> 00:06:17,800
 I can't remember.

127
00:06:17,800 --> 00:06:21,000
 But I'm pretty sure that in Judaism,

128
00:06:21,000 --> 00:06:23,920
 it's been pretty unequal in the texts.

129
00:06:23,920 --> 00:06:27,360
 And I'm pretty sure that in Islam, it's the same.

130
00:06:27,360 --> 00:06:28,480
 I don't know.

131
00:06:28,480 --> 00:06:31,880
 Either way, I mean, equality, that's good in a sense,

132
00:06:31,880 --> 00:06:34,200
 to not be prejudiced against people.

133
00:06:34,200 --> 00:06:36,600
 My point is, they're missing the whole point.

134
00:06:36,600 --> 00:06:40,520
 Here, people have been violent and awful

135
00:06:40,520 --> 00:06:42,320
 in the name of your religion.

136
00:06:42,320 --> 00:06:44,790
 And all you want to say is that it's compatible with the

137
00:06:44,790 --> 00:06:45,240
 West.

138
00:06:45,240 --> 00:06:47,640
 I mean, who cares?

139
00:06:47,640 --> 00:06:49,400
 Are you against these bombings?

140
00:06:49,400 --> 00:06:50,880
 Are you against these shootings?

141
00:06:50,880 --> 00:06:52,640
 What do they do?

142
00:06:52,640 --> 00:06:54,480
 Are you against these atrocities or not?

143
00:06:54,480 --> 00:06:55,600
 That's what we want to know.

144
00:06:55,600 --> 00:06:57,800
 Do you believe that hurting people is good?

145
00:06:57,800 --> 00:06:58,920
 And they're not addressing it.

146
00:06:58,920 --> 00:07:00,880
 And then they put up these black and red signs,

147
00:07:00,880 --> 00:07:02,800
 and you think, these are terrorists.

148
00:07:02,800 --> 00:07:04,440
 Who puts up a black and red sign?

149
00:07:04,440 --> 00:07:11,160
 Death metal signs are black and red.

150
00:07:11,160 --> 00:07:13,240
 It was the kind of colors that I would go for when

151
00:07:13,240 --> 00:07:17,880
 I was having metal listening to Ozzy Osbourne

152
00:07:17,880 --> 00:07:20,880
 and going to black Sabbath concerts.

153
00:07:20,880 --> 00:07:22,840
 It's not peace.

154
00:07:22,840 --> 00:07:24,680
 Oh, there's my rant for the day.

155
00:07:24,680 --> 00:07:27,000
 But I felt like I'd done something,

156
00:07:27,000 --> 00:07:29,870
 because I finally got a chance to talk to someone about

157
00:07:29,870 --> 00:07:30,200
 this.

158
00:07:30,200 --> 00:07:32,200
 I've just been shaking my head at these people.

159
00:07:32,200 --> 00:07:33,280
 It's no idea.

160
00:07:33,280 --> 00:07:36,040
 PR, they've got a bad PR person, that's for sure.

161
00:07:36,040 --> 00:07:41,720
 But I think it's more than that.

162
00:07:41,720 --> 00:07:42,440
 I don't know.

163
00:07:42,440 --> 00:07:46,120
 I'm really not impressed with Islam so far.

164
00:07:46,120 --> 00:07:48,280
 I'm not impressed with many religions, actually.

165
00:07:52,080 --> 00:07:58,160
 So then we had a discussion about religion with Katya,

166
00:07:58,160 --> 00:07:59,520
 is her name.

167
00:07:59,520 --> 00:08:02,920
 She said, would you say that all religions or religions

168
00:08:02,920 --> 00:08:09,560
 have been the cause of most conflict in the world?

169
00:08:09,560 --> 00:08:12,480
 We just got talking about it.

170
00:08:12,480 --> 00:08:16,440
 Anyway, that was my story for today.

171
00:08:20,840 --> 00:08:22,680
 Oh, and we meditated in the atrium.

172
00:08:22,680 --> 00:08:23,840
 That's the other thing.

173
00:08:23,840 --> 00:08:25,080
 Just a short meditation.

174
00:08:25,080 --> 00:08:27,480
 There was only three of us.

175
00:08:27,480 --> 00:08:30,040
 But it was nice.

176
00:08:30,040 --> 00:08:33,680
 Atrium's a nice place.

177
00:08:33,680 --> 00:08:40,760
 So we're going to do atrium on Monday, the gym on Wednesday

178
00:08:40,760 --> 00:08:40,760
,

179
00:08:40,760 --> 00:08:42,720
 and the student center on Friday.

180
00:08:42,720 --> 00:08:47,140
 So we'll have three meditations a week for the rest of the

181
00:08:47,140 --> 00:08:47,520
 year,

182
00:08:47,520 --> 00:08:49,920
 hopefully.

183
00:08:49,920 --> 00:08:50,420
 Thanks.

184
00:08:50,420 --> 00:09:03,000
 And did I mention the book thing?

185
00:09:03,000 --> 00:09:05,320
 Last night we talked about the books, right?

186
00:09:05,320 --> 00:09:06,120
 Yes.

187
00:09:06,120 --> 00:09:08,560
 Briefly, yes.

188
00:09:08,560 --> 00:09:09,520
 We're trying to figure it out.

189
00:09:09,520 --> 00:09:10,520
 Everyone's all confused.

190
00:09:10,520 --> 00:09:12,040
 I don't remember who did it last time.

191
00:09:12,040 --> 00:09:14,640
 But I just assumed it would be easy.

192
00:09:14,640 --> 00:09:15,960
 As the publisher certainly knows,

193
00:09:15,960 --> 00:09:18,360
 if we can somehow get through to the publisher,

194
00:09:18,360 --> 00:09:20,600
 he's ready to print them, I'm sure.

195
00:09:20,600 --> 00:09:25,680
 As long as he's not dead, everyone dies and will be dead.

196
00:09:25,680 --> 00:09:28,600
 But otherwise, it's just a matter of everyone's

197
00:09:28,600 --> 00:09:31,120
 confused wondering, oh, you want--

198
00:09:31,120 --> 00:09:32,400
 send us it and we'll print it out.

199
00:09:32,400 --> 00:09:33,640
 No, no.

200
00:09:33,640 --> 00:09:34,480
 It's already there.

201
00:09:34,480 --> 00:09:36,800
 It just has to be printed.

202
00:09:36,800 --> 00:09:37,520
 Is it color?

203
00:09:37,520 --> 00:09:38,480
 Is it this color?

204
00:09:38,480 --> 00:09:40,720
 No, you don't understand.

205
00:09:40,720 --> 00:09:41,760
 Ask me these questions.

206
00:09:41,760 --> 00:09:43,040
 Just contact the publisher.

207
00:09:43,040 --> 00:09:44,080
 He knows.

208
00:09:44,080 --> 00:09:45,720
 So it's a reprint, really.

209
00:09:45,720 --> 00:09:46,800
 Yeah, it's a reprint.

210
00:09:46,800 --> 00:09:48,440
 It's like the third or fourth reprint.

211
00:09:48,440 --> 00:09:54,080
 Just keep printing more.

212
00:09:54,080 --> 00:09:56,520
 That's a good thing.

213
00:09:56,520 --> 00:09:57,720
 Yeah, and he's a Buddhist.

214
00:09:57,720 --> 00:09:58,800
 He's a student of--

215
00:09:58,800 --> 00:10:04,120
 [NON-ENGLISH SPEECH]

216
00:10:04,120 --> 00:10:05,680
 [NON-ENGLISH SPEECH]

217
00:10:05,680 --> 00:10:12,680
 And [NON-ENGLISH SPEECH]

218
00:10:12,680 --> 00:10:14,040
 Well, [NON-ENGLISH SPEECH]

219
00:10:14,040 --> 00:10:15,760
 I don't remember where he's from.

220
00:10:15,760 --> 00:10:18,200
 But [NON-ENGLISH SPEECH]

221
00:10:18,200 --> 00:10:20,520
 He's two monks.

222
00:10:20,520 --> 00:10:22,640
 Really a neat guy.

223
00:10:22,640 --> 00:10:23,320
 Seems like it.

224
00:10:23,320 --> 00:10:28,640
 Montay, when you get the books back, if people want one,

225
00:10:28,640 --> 00:10:31,480
 can they send a self-addressed stamped envelope

226
00:10:31,480 --> 00:10:34,120
 or something to get one?

227
00:10:34,120 --> 00:10:37,040
 Yeah, some books for me.

228
00:10:37,040 --> 00:10:41,640
 If you send a self-addressed stamped envelope,

229
00:10:41,640 --> 00:10:45,080
 I can send it back.

230
00:10:45,080 --> 00:10:48,560
 It's not really necessary because you can download it,

231
00:10:48,560 --> 00:10:49,400
 right?

232
00:10:49,400 --> 00:10:51,240
 Yes.

233
00:10:51,240 --> 00:10:54,840
 If you want the nice, shiny copy,

234
00:10:54,840 --> 00:10:57,120
 I don't have one here.

235
00:10:57,120 --> 00:11:03,160
 You can also-- questions.

236
00:11:03,160 --> 00:11:04,640
 Do we have any questions today?

237
00:11:04,640 --> 00:11:06,200
 We have questions.

238
00:11:06,200 --> 00:11:07,360
 OK.

239
00:11:07,360 --> 00:11:10,160
 In the Western world and Western ancient studies,

240
00:11:10,160 --> 00:11:13,600
 there are a number of people that study and even fluently

241
00:11:13,600 --> 00:11:15,880
 speak the dead language Latin.

242
00:11:15,880 --> 00:11:18,590
 Are there any people today that still study Pali to the

243
00:11:18,590 --> 00:11:18,800
 point

244
00:11:18,800 --> 00:11:20,400
 that they can speak it?

245
00:11:20,400 --> 00:11:22,880
 If so, how would one do this?

246
00:11:22,880 --> 00:11:25,320
 Would you ever consider taking a large amount of time

247
00:11:25,320 --> 00:11:27,720
 to do this?

248
00:11:27,720 --> 00:11:29,160
 We talked about it.

249
00:11:29,160 --> 00:11:30,240
 In Burma, they do it.

250
00:11:30,240 --> 00:11:34,600
 I think Burma is probably the only place left.

251
00:11:34,600 --> 00:11:37,120
 Maybe Sri Lanka, but not really.

252
00:11:37,120 --> 00:11:38,840
 Not really.

253
00:11:38,840 --> 00:11:41,520
 But in Burma, they still do conversational Pali.

254
00:11:41,520 --> 00:11:43,640
 And in Thailand, they've started to try.

255
00:11:43,640 --> 00:11:47,440
 These monks who have studied in Burma have made textbooks.

256
00:11:47,440 --> 00:11:50,560
 I've got one of the textbooks that's also in English.

257
00:11:50,560 --> 00:11:53,520
 It's English, Thai, and Pali.

258
00:11:53,520 --> 00:11:55,760
 And it's conversational Pali.

259
00:11:55,760 --> 00:11:58,840
 It's really kind of neat, but it never got off the ground.

260
00:11:58,840 --> 00:12:01,200
 I'd like to get some people who know Pali together

261
00:12:01,200 --> 00:12:04,400
 and actually do this, learn it.

262
00:12:04,400 --> 00:12:09,120
 The only time I really got close was many years ago.

263
00:12:09,120 --> 00:12:13,760
 There was a Pali group, and I think it's still going.

264
00:12:13,760 --> 00:12:15,320
 I'm not really involved.

265
00:12:15,320 --> 00:12:23,760
 But what we did is we set up this kind of like a wiki.

266
00:12:23,760 --> 00:12:27,680
 It was just a page that we could all edit.

267
00:12:27,680 --> 00:12:29,960
 And we would take turns writing sentences.

268
00:12:29,960 --> 00:12:33,480
 So I would say something, and the next person would reply.

269
00:12:33,480 --> 00:12:35,360
 And we were speaking in Pali.

270
00:12:35,360 --> 00:12:37,320
 And we could correct each other's grammar,

271
00:12:37,320 --> 00:12:40,120
 and we could ask questions about grammar, and so on.

272
00:12:40,120 --> 00:12:43,690
 But it never took off because I wasn't really all that good

273
00:12:43,690 --> 00:12:43,800
.

274
00:12:43,800 --> 00:12:47,240
 But I was the only one who really had--

275
00:12:47,240 --> 00:12:48,900
 and there weren't many people interested.

276
00:12:48,900 --> 00:12:51,520
 But of the three or four people interested,

277
00:12:51,520 --> 00:12:53,320
 I was the only one who could even come close

278
00:12:53,320 --> 00:12:55,320
 to forming a sentence.

279
00:12:55,320 --> 00:13:00,440
 So you need people who really know the language.

280
00:13:00,440 --> 00:13:04,840
 But that would be something it was really neat to do,

281
00:13:04,840 --> 00:13:07,240
 to be able to type to each other in Pali,

282
00:13:07,240 --> 00:13:10,120
 to have a conversation.

283
00:13:10,120 --> 00:13:12,480
 It's easier when you type because you've got more time.

284
00:13:12,480 --> 00:13:14,480
 You actually have to speak it.

285
00:13:14,480 --> 00:13:16,760
 Ajahn Tong, whenever he meets Burmese people,

286
00:13:16,760 --> 00:13:19,600
 he says something in Pali to them.

287
00:13:19,600 --> 00:13:21,240
 And Sri Lankan as well, he's tried.

288
00:13:21,240 --> 00:13:23,880
 But Sri Lankan monk, I brought a Sri Lankan monk to him.

289
00:13:23,880 --> 00:13:29,240
 The Sri Lankan monk had no idea what he was saying.

290
00:13:34,720 --> 00:13:37,520
 Sri Lankan, they don't really do it anymore.

291
00:13:37,520 --> 00:13:39,080
 Some do, but it's not so common.

292
00:13:39,080 --> 00:13:49,200
 I continually have this happen where I cannot seem to focus

293
00:13:49,200 --> 00:13:51,240
 on one thing with noting.

294
00:13:51,240 --> 00:13:53,040
 I will note rising, and then my attention

295
00:13:53,040 --> 00:13:54,640
 will go to my feeling of sitting.

296
00:13:54,640 --> 00:13:56,360
 So I'll say sitting.

297
00:13:56,360 --> 00:13:58,720
 But then my attention will have gone elsewhere.

298
00:13:58,720 --> 00:14:01,560
 So what do I do to stop focusing on noting

299
00:14:01,560 --> 00:14:03,920
 and let myself just experience what happens

300
00:14:03,920 --> 00:14:06,200
 and note when possible?

301
00:14:06,200 --> 00:14:08,200
 Is this acceptable practice?

302
00:14:08,200 --> 00:14:10,200
 Do you understand what I'm trying to say?

303
00:14:10,200 --> 00:14:12,280
 I'll be on tomorrow to clarify if not.

304
00:14:12,280 --> 00:14:13,560
 Thank you, Bandhi.

305
00:14:13,560 --> 00:14:17,760
 I mean, if it's many things distracting you,

306
00:14:17,760 --> 00:14:20,320
 then you would say distracted or even overwhelmed.

307
00:14:20,320 --> 00:14:22,400
 It's a common-- when you feel overwhelmed,

308
00:14:22,400 --> 00:14:24,800
 you can say overwhelmed, overwhelmed.

309
00:14:24,800 --> 00:14:26,400
 There's nothing wrong with that per se.

310
00:14:26,400 --> 00:14:29,200
 But you do want to try to come back to your base,

311
00:14:29,200 --> 00:14:33,440
 back to the stomach, rather than jumping from object,

312
00:14:33,440 --> 00:14:34,960
 from one to another.

313
00:14:34,960 --> 00:14:37,120
 You know, you do one, and then if something drops,

314
00:14:37,120 --> 00:14:39,920
 you say that you acknowledge that.

315
00:14:39,920 --> 00:14:42,320
 But when there's nothing, try to come back to the stomach

316
00:14:42,320 --> 00:14:42,920
 to start over.

317
00:14:42,920 --> 00:14:47,080
 I wouldn't worry too much about that.

318
00:14:47,080 --> 00:14:49,280
 You're seeing impermanent suffering and non-self.

319
00:14:49,280 --> 00:14:51,280
 So that's good.

320
00:14:51,280 --> 00:14:52,240
 It helps you to let go.

321
00:15:01,840 --> 00:15:04,520
 How does one know when they are experiencing nirvana

322
00:15:04,520 --> 00:15:09,320
 and not just filled with aversion to life's activities?

323
00:15:09,320 --> 00:15:11,440
 You don't know when you're experiencing nirvana.

324
00:15:11,440 --> 00:15:14,640
 There's no knowing in nirvana.

325
00:15:14,640 --> 00:15:18,200
 Don't worry about that.

326
00:15:18,200 --> 00:15:19,600
 Don't worry about nirvana.

327
00:15:19,600 --> 00:15:23,800
 It's not something you can miss.

328
00:15:23,800 --> 00:15:26,200
 It's not something you can mistake for anything else.

329
00:15:26,200 --> 00:15:29,160
 [END PLAYBACK]

330
00:15:29,160 --> 00:15:41,480
 I think we're all caught up with questions.

331
00:15:41,480 --> 00:15:52,200
 Oh, I'm sorry.

332
00:15:52,200 --> 00:15:53,640
 I read it wrong.

333
00:15:53,640 --> 00:15:55,520
 I apologize.

334
00:15:55,520 --> 00:15:57,800
 He meant nirvida, not nirvana.

335
00:15:57,800 --> 00:16:01,480
 How does one know when they are experiencing nirvida

336
00:16:01,480 --> 00:16:04,440
 and not just filled with aversion to life's activities?

337
00:16:04,440 --> 00:16:06,760
 My apologies.

338
00:16:06,760 --> 00:16:08,160
 Well, nirvida is not aversion.

339
00:16:08,160 --> 00:16:11,240
 Nirvida is losing your desire.

340
00:16:11,240 --> 00:16:13,440
 So when you find that you don't have desire for things

341
00:16:13,440 --> 00:16:16,080
 that you used as desire, that's nirvida.

342
00:16:16,080 --> 00:16:18,080
 When you look at things that you normally desire,

343
00:16:18,080 --> 00:16:20,920
 and you say it's just not worth it, that's nirvida.

344
00:16:20,920 --> 00:16:27,040
 Nih means out or not.

345
00:16:27,040 --> 00:16:31,200
 Vidha comes from-- where does it come from?

346
00:16:31,200 --> 00:16:32,800
 I think from desire or something.

347
00:16:32,800 --> 00:16:35,720
 It has something to do with your excitement.

348
00:16:35,720 --> 00:16:39,640
 Nihvida means not excitement, not excited.

349
00:16:39,640 --> 00:16:41,600
 Vidha, I don't know.

350
00:16:41,600 --> 00:16:44,560
 [END PLAYBACK]

351
00:16:44,560 --> 00:17:02,960
 Any word on the project in Tampa, Bante?

352
00:17:02,960 --> 00:17:05,080
 No, no word yet.

353
00:17:05,080 --> 00:17:06,640
 But they said Monday or Tuesday.

354
00:17:06,640 --> 00:17:07,640
 Maybe tomorrow.

355
00:17:08,640 --> 00:17:09,140
 Yeah.

356
00:17:09,140 --> 00:17:34,360
 OK.

357
00:17:34,360 --> 00:17:36,200
 Good night then, everyone.

358
00:17:36,200 --> 00:17:37,320
 Thank you for tuning in.

359
00:17:37,320 --> 00:17:39,480
 Thanks, Robin, for your help.

360
00:17:39,480 --> 00:17:41,000
 Thank you, Bante.

361
00:17:41,000 --> 00:17:41,960
 You're welcome.

362
00:17:41,960 --> 00:17:42,960
 Good night.

363
00:17:42,960 --> 00:17:44,520
 Good night.

364
00:17:44,520 --> 00:17:46,960
 [END PLAYBACK]

365
00:17:46,960 --> 00:17:49,400
 [MUSIC PLAYING]

