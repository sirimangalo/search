1
00:00:00,000 --> 00:00:05,000
 Hi, next question comes from Jilliannepp.

2
00:00:05,000 --> 00:00:09,820
 "I am a beginner to Buddhism and am, in fact, still doing

3
00:00:09,820 --> 00:00:12,000
 my initial research on it.

4
00:00:12,000 --> 00:00:15,090
 I would like to get a balanced view of the negative aspects

5
00:00:15,090 --> 00:00:16,000
 of Buddhism.

6
00:00:16,000 --> 00:00:18,800
 What are some of the things that have derailed both laypers

7
00:00:18,800 --> 00:00:21,000
ons and monastics?"

8
00:00:21,000 --> 00:00:28,020
 It's not often I get asked what are the negative aspects of

9
00:00:28,020 --> 00:00:30,000
 my religion.

10
00:00:30,000 --> 00:00:33,000
 I don't think there are any, honestly.

11
00:00:33,000 --> 00:00:37,380
 Although I suppose I've gone over some of the apparent

12
00:00:37,380 --> 00:00:39,000
 negative aspects of Buddhism

13
00:00:39,000 --> 00:00:44,000
 in the sense of not being much fun at parties,

14
00:00:44,000 --> 00:00:49,990
 not being able to get along in society, having to segregate

15
00:00:49,990 --> 00:00:53,000
 yourself, having to be careful.

16
00:00:53,000 --> 00:00:58,170
 There's a lot of difficulty that comes from trying to be

17
00:00:58,170 --> 00:00:59,000
 pure.

18
00:00:59,000 --> 00:01:05,470
 It's not easy to purify your mind and to be solely a good

19
00:01:05,470 --> 00:01:06,000
 person

20
00:01:06,000 --> 00:01:09,730
 and to do away with all evil in the mind, which is really

21
00:01:09,730 --> 00:01:11,000
 what we're trying to do.

22
00:01:11,000 --> 00:01:15,000
 There are many people who would say that's impossible,

23
00:01:15,000 --> 00:01:20,000
 mostly because they've never practiced meditation.

24
00:01:20,000 --> 00:01:24,730
 So they would consider that to be a negative aspect of

25
00:01:24,730 --> 00:01:26,000
 Buddhism.

26
00:01:26,000 --> 00:01:30,340
 But I can't think of any, obviously I don't think there are

27
00:01:30,340 --> 00:01:33,000
 any negative aspects to Buddhism.

28
00:01:33,000 --> 00:01:35,000
 You're kind of asking two questions.

29
00:01:35,000 --> 00:01:41,000
 The other one is, or I don't know what you're aiming at,

30
00:01:41,000 --> 00:01:45,120
 but as far as what stops people from practicing Buddhism

31
00:01:45,120 --> 00:01:46,000
 correctly,

32
00:01:46,000 --> 00:01:48,000
 I guess that's the answer.

33
00:01:48,000 --> 00:01:52,220
 There's nothing wrong with Buddhism, but people just don't

34
00:01:52,220 --> 00:01:55,000
 understand it or don't practice it correctly.

35
00:01:55,000 --> 00:02:00,000
 What stops people from practicing correctly, obviously,

36
00:02:00,000 --> 00:02:05,000
 are preconceived notions of what is right and what is wrong

37
00:02:05,000 --> 00:02:05,000
,

38
00:02:05,000 --> 00:02:09,000
 which are not really based on truth and reality.

39
00:02:09,000 --> 00:02:15,000
 What stops people from making progress,

40
00:02:15,000 --> 00:02:22,260
 according to the Buddha, there are certain things that get

41
00:02:22,260 --> 00:02:23,000
 in the way

42
00:02:23,000 --> 00:02:26,430
 and prevent a person's progress as a lay person and as a

43
00:02:26,430 --> 00:02:27,000
 monk.

44
00:02:27,000 --> 00:02:30,000
 So maybe I'm not sure if this is exactly what you're asking

45
00:02:30,000 --> 00:02:30,000
,

46
00:02:30,000 --> 00:02:32,480
 but I'll take the opportunity to talk about some of these

47
00:02:32,480 --> 00:02:33,000
 things.

48
00:02:33,000 --> 00:02:39,200
 For lay people, the first one is there's a requirement for

49
00:02:39,200 --> 00:02:40,000
 faith.

50
00:02:40,000 --> 00:02:44,480
 If people don't have a faith and confidence in what they're

51
00:02:44,480 --> 00:02:45,000
 doing,

52
00:02:45,000 --> 00:02:48,000
 it makes it very difficult for them to continue,

53
00:02:48,000 --> 00:02:51,000
 because obviously for most or for many lay people,

54
00:02:51,000 --> 00:02:54,300
 there's not the opportunity to realize all of the Buddha's

55
00:02:54,300 --> 00:02:55,000
 teachings,

56
00:02:55,000 --> 00:02:57,000
 even to study all of the Buddha's teachings.

57
00:02:57,000 --> 00:03:03,320
 So there has to be some level of confidence in your leaders

58
00:03:03,320 --> 00:03:05,000
 in the religion.

59
00:03:05,000 --> 00:03:08,000
 If you don't believe in the principles of Buddhism,

60
00:03:08,000 --> 00:03:12,000
 it can be very difficult for you to progress.

61
00:03:12,000 --> 00:03:17,840
 I guess what that means really is if you have ideas that

62
00:03:17,840 --> 00:03:19,000
 are contrary,

63
00:03:19,000 --> 00:03:22,360
 then you won't even open your mind to the idea that the

64
00:03:22,360 --> 00:03:24,000
 Buddha's teaching might be right.

65
00:03:24,000 --> 00:03:27,000
 I recognize this in many so-called Buddhists.

66
00:03:27,000 --> 00:03:32,000
 They still believe in a delicious meal

67
00:03:32,000 --> 00:03:39,000
 and in preference for things in beauty, in sexuality and so

68
00:03:39,000 --> 00:03:39,000
 on.

69
00:03:39,000 --> 00:03:42,450
 Not only are they taking it, but they also believe that it

70
00:03:42,450 --> 00:03:43,000
's right

71
00:03:43,000 --> 00:03:45,000
 to be attached to these things.

72
00:03:45,000 --> 00:03:48,000
 These are people who consider themselves to be Buddhists

73
00:03:48,000 --> 00:03:50,000
 even from birth.

74
00:03:50,000 --> 00:03:54,000
 That can be a real hindrance in the practice.

75
00:03:54,000 --> 00:03:56,000
 The second one is morality.

76
00:03:56,000 --> 00:03:59,000
 Obviously, if you're an immoral person,

77
00:03:59,000 --> 00:04:02,000
 it's very difficult to practice the Buddha's teaching.

78
00:04:02,000 --> 00:04:07,000
 The third one is a belief in superstition.

79
00:04:07,000 --> 00:04:11,360
 This is common throughout the Buddhist world and the world

80
00:04:11,360 --> 00:04:13,000
 in its entirety.

81
00:04:13,000 --> 00:04:16,000
 Because the Buddha taught cause and effect,

82
00:04:16,000 --> 00:04:19,390
 when people have different ideas of what is cause and what

83
00:04:19,390 --> 00:04:20,000
 is effect

84
00:04:20,000 --> 00:04:23,000
 in terms of magical causes,

85
00:04:23,000 --> 00:04:25,570
 if you wear this around your neck, somehow it's going to

86
00:04:25,570 --> 00:04:26,000
 have a benefit.

87
00:04:26,000 --> 00:04:29,130
 If you say this or say that, somehow there's going to be a

88
00:04:29,130 --> 00:04:32,000
 negative effect and so on.

89
00:04:32,000 --> 00:04:38,300
 The belief in superstition, because it's contrary to the

90
00:04:38,300 --> 00:04:39,000
 idea of karma

91
00:04:39,000 --> 00:04:44,670
 and cause and effect is another hindrance to, especially to

92
00:04:44,670 --> 00:04:45,000
 lay people,

93
00:04:45,000 --> 00:04:48,000
 because they're not so close to the teachings.

94
00:04:48,000 --> 00:04:55,380
 They hear about this witch doctor or a fortune teller or so

95
00:04:55,380 --> 00:04:56,000
 on.

96
00:04:56,000 --> 00:04:59,120
 They can get mixed up in terms of what is the Buddha's

97
00:04:59,120 --> 00:05:00,000
 teaching.

98
00:05:00,000 --> 00:05:06,000
 The fourth one is getting involved in other religions.

99
00:05:06,000 --> 00:05:09,000
 It's related to the last one.

100
00:05:09,000 --> 00:05:15,260
 When people go and support other religions, other religious

101
00:05:15,260 --> 00:05:16,000
 doctrines

102
00:05:16,000 --> 00:05:20,000
 in terms of paying respect to these teachers,

103
00:05:20,000 --> 00:05:23,450
 listening to what they have to say and trying to get their

104
00:05:23,450 --> 00:05:24,000
 opinion,

105
00:05:24,000 --> 00:05:29,050
 because they have a whole other outlook on life and it conf

106
00:05:29,050 --> 00:05:32,000
uses people.

107
00:05:32,000 --> 00:05:35,510
 When people say things like, "All religions teach the same

108
00:05:35,510 --> 00:05:36,000
 thing,"

109
00:05:36,000 --> 00:05:39,540
 or so on, "All religions are the same. It doesn't matter

110
00:05:39,540 --> 00:05:41,000
 what religion you are."

111
00:05:41,000 --> 00:05:45,000
 They will respect the teachings of all religions.

112
00:05:45,000 --> 00:05:48,020
 It's not that we don't, in a sense, respect these teachings

113
00:05:48,020 --> 00:05:49,000
.

114
00:05:49,000 --> 00:05:51,750
 In terms of you want to practice that way, that's fine. You

115
00:05:51,750 --> 00:05:53,000
 believe this, that's fine.

116
00:05:53,000 --> 00:05:58,070
 But to accept that that belief is proper, that it actually

117
00:05:58,070 --> 00:05:59,000
 is beneficial.

118
00:05:59,000 --> 00:06:03,450
 When those beliefs go against the understanding of the

119
00:06:03,450 --> 00:06:04,000
 Buddha

120
00:06:04,000 --> 00:06:07,190
 as to what is right and what is wrong can be a hindrance to

121
00:06:07,190 --> 00:06:08,000
 one's practice,

122
00:06:08,000 --> 00:06:13,000
 because if there is and when there is a conflict.

123
00:06:13,000 --> 00:06:18,000
 The final one is to support the Buddha's teachings,

124
00:06:18,000 --> 00:06:22,500
 so to not spend time supporting teachings which are

125
00:06:22,500 --> 00:06:25,000
 contrary to the Buddha's teaching

126
00:06:25,000 --> 00:06:30,000
 and to actually engage in supporting the Buddhist religion

127
00:06:30,000 --> 00:06:36,000
 in terms of material support, in terms of spiritual support

128
00:06:36,000 --> 00:06:40,000
 and spreading the teachings and so on.

129
00:06:40,000 --> 00:06:45,870
 When we fail to do that and when we get involved in other

130
00:06:45,870 --> 00:06:47,000
 religions,

131
00:06:47,000 --> 00:06:51,000
 these are two things the Buddha said cause you to move away

132
00:06:51,000 --> 00:06:53,000
 from the Buddha's teaching.

133
00:06:53,000 --> 00:06:59,800
 Those things which are hindrance or cause monks to become

134
00:06:59,800 --> 00:07:01,000
 derailed, as you say,

135
00:07:01,000 --> 00:07:05,770
 the Buddha had four things that he said, especially for new

136
00:07:05,770 --> 00:07:06,000
 monks,

137
00:07:06,000 --> 00:07:09,000
 are going to be a real hindrance.

138
00:07:09,000 --> 00:07:12,130
 This is sort of an addition to what I was saying about how

139
00:07:12,130 --> 00:07:13,000
 to become a monk earlier.

140
00:07:13,000 --> 00:07:17,000
 I think it's useful to know about these as well.

141
00:07:17,000 --> 00:07:27,150
 The first one is not being able to stand the teachings or

142
00:07:27,150 --> 00:07:28,000
 instruction,

143
00:07:28,000 --> 00:07:31,590
 not being able to bear being instructed, being told what to

144
00:07:31,590 --> 00:07:32,000
 do.

145
00:07:32,000 --> 00:07:35,000
 This is very common for new monks.

146
00:07:35,000 --> 00:07:38,000
 When they're told they have to do this and have to do that,

147
00:07:38,000 --> 00:07:42,000
 it's very easy for them to become angry and frustrated

148
00:07:42,000 --> 00:07:47,620
 when they don't understand and don't agree, they aren't

149
00:07:47,620 --> 00:07:49,000
 able to follow along

150
00:07:49,000 --> 00:07:53,520
 because of their own ideas, their own views of what is

151
00:07:53,520 --> 00:07:55,000
 right and what is wrong.

152
00:07:55,000 --> 00:07:57,380
 It makes it very difficult for them to do things like

153
00:07:57,380 --> 00:07:59,000
 walking back and forth

154
00:07:59,000 --> 00:08:03,000
 or sitting for long periods of time.

155
00:08:03,000 --> 00:08:06,000
 When we tell meditators they have to sit through the pain

156
00:08:06,000 --> 00:08:09,000
 instead of trying to move around all the time.

157
00:08:09,000 --> 00:08:11,000
 This can be very difficult for some people. They don't

158
00:08:11,000 --> 00:08:11,000
 agree with it.

159
00:08:11,000 --> 00:08:14,000
 When we tell people they have to let go, they can't cling

160
00:08:14,000 --> 00:08:17,000
 and they can't chase after.

161
00:08:17,000 --> 00:08:21,250
 When we tell them not to make eye contact or look around or

162
00:08:21,250 --> 00:08:24,000
 wander or move quickly or so on,

163
00:08:24,000 --> 00:08:29,000
 it can be very difficult for people, especially for monks

164
00:08:29,000 --> 00:08:31,000
 and for meditators

165
00:08:31,000 --> 00:08:35,000
 who have come to the meditation center to stay.

166
00:08:35,000 --> 00:08:50,210
 The second one is being addicted to simple, being lazy, I

167
00:08:50,210 --> 00:08:53,000
 guess, is a good explanation of it.

168
00:08:53,000 --> 00:08:57,940
 Being addicted to just eating and sleeping and lazing

169
00:08:57,940 --> 00:08:59,000
 around.

170
00:08:59,000 --> 00:09:03,000
 Laziness, I guess, is a good summary of it.

171
00:09:03,000 --> 00:09:05,580
 Meditation in the Buddha's teaching does take a lot of

172
00:09:05,580 --> 00:09:07,000
 effort.

173
00:09:07,000 --> 00:09:09,680
 It's something that you have to work hard at because we're

174
00:09:09,680 --> 00:09:13,000
 trying to change the very core of our being,

175
00:09:13,000 --> 00:09:20,490
 the very core of how we look at the world, of how we see

176
00:09:20,490 --> 00:09:21,000
 things.

177
00:09:21,000 --> 00:09:24,690
 We're trying to change the way we look entirely, change the

178
00:09:24,690 --> 00:09:26,000
 way we look at things.

179
00:09:26,000 --> 00:09:30,230
 This takes a lot of effort because we're generally so off

180
00:09:30,230 --> 00:09:31,000
 track.

181
00:09:31,000 --> 00:09:37,280
 If we are lazy, if we think we can just sit around and eat

182
00:09:37,280 --> 00:09:41,000
 and sleep and socialize

183
00:09:41,000 --> 00:09:44,540
 and think that being a monk or being a meditator is somehow

184
00:09:44,540 --> 00:09:45,000
 enough.

185
00:09:45,000 --> 00:09:47,880
 If you come to stay at the monastery, somehow you're

186
00:09:47,880 --> 00:09:51,000
 gaining something without even practicing.

187
00:09:51,000 --> 00:09:54,920
 This can make it very difficult and it does in the end lead

188
00:09:54,920 --> 00:09:57,000
 one to feel like

189
00:09:57,000 --> 00:10:02,010
 being a monk or being a meditator is pointless, going home

190
00:10:02,010 --> 00:10:05,000
 and not gaining any benefit from it.

191
00:10:05,000 --> 00:10:09,340
 The third one is being attached to happiness, being

192
00:10:09,340 --> 00:10:11,000
 attached to sensual pleasures.

193
00:10:11,000 --> 00:10:19,380
 So needing good food, needing nice clothing, needing a soft

194
00:10:19,380 --> 00:10:20,000
 bed and so on,

195
00:10:20,000 --> 00:10:24,380
 needing to listen to music and watch television, all of

196
00:10:24,380 --> 00:10:26,000
 these things that monks are not allowed,

197
00:10:26,000 --> 00:10:29,000
 monks and meditators are not allowed to do.

198
00:10:29,000 --> 00:10:33,000
 When these addictions come up, if they're very strong,

199
00:10:33,000 --> 00:10:38,330
 they can very easily lead one to become derailed and lose

200
00:10:38,330 --> 00:10:40,000
 interest in the practice

201
00:10:40,000 --> 00:10:44,000
 because of one's addictions, one's attachments.

202
00:10:44,000 --> 00:10:48,220
 When these come up, it leads to boredom, it leads to dis

203
00:10:48,220 --> 00:10:51,000
interest in reality and meditation,

204
00:10:51,000 --> 00:10:55,680
 wanting something else, wanting more and eventually leads

205
00:10:55,680 --> 00:10:59,000
 one to leave the monk's life

206
00:10:59,000 --> 00:11:02,000
 or to leave the meditation center.

207
00:11:02,000 --> 00:11:08,530
 The fourth one is specifically for monks, they say is love

208
00:11:08,530 --> 00:11:11,000
 for a woman.

209
00:11:11,000 --> 00:11:16,270
 But I think it can easily obviously be flipped to talking

210
00:11:16,270 --> 00:11:20,000
 about a female or even just a meditator in general.

211
00:11:20,000 --> 00:11:25,680
 Sexuality obviously is a very strong attachment and it's

212
00:11:25,680 --> 00:11:29,000
 the prime reason why monks disrobe

213
00:11:29,000 --> 00:11:33,000
 when a woman comes to the monastery and they fall in love

214
00:11:33,000 --> 00:11:40,000
 and the monk decides that he's had enough and he's found a

215
00:11:40,000 --> 00:11:47,000
 more desirable or more pleasurable path to follow.

216
00:11:47,000 --> 00:11:52,370
 This is the fourth danger. These dangers are something that

217
00:11:52,370 --> 00:11:54,000
 one has to watch out for,

218
00:11:54,000 --> 00:11:56,760
 especially this last one, you have to be very careful about

219
00:11:56,760 --> 00:11:57,000
.

220
00:11:57,000 --> 00:12:01,240
 I think it all depends on your appreciation of the Buddha's

221
00:12:01,240 --> 00:12:02,000
 teaching

222
00:12:02,000 --> 00:12:09,200
 and your appreciation of the life that the Buddha laid out

223
00:12:09,200 --> 00:12:10,000
 for us.

224
00:12:10,000 --> 00:12:14,000
 If you really believe in what the Buddha taught

225
00:12:14,000 --> 00:12:17,000
 and if you really see the benefit in what the Buddha taught

226
00:12:17,000 --> 00:12:22,450
 and if it really does bring you benefit, then I think these

227
00:12:22,450 --> 00:12:26,000
 dangers are easily overcome

228
00:12:26,000 --> 00:12:30,000
 and the way you overcome them is by seeing the benefit,

229
00:12:30,000 --> 00:12:34,120
 by understanding that what we're doing is something great,

230
00:12:34,120 --> 00:12:37,000
 something powerful, something pure

231
00:12:37,000 --> 00:12:40,640
 and something higher than all of these other desires, these

232
00:12:40,640 --> 00:12:44,000
 other attachments, these other needs and wants,

233
00:12:44,000 --> 00:12:48,000
 higher than our views and our opinions.

234
00:12:48,000 --> 00:12:52,000
 That what the Buddha taught is very much real

235
00:12:52,000 --> 00:12:56,500
 and the more we appreciate, the more we study and the more

236
00:12:56,500 --> 00:12:58,000
 we practice the Buddha's teaching,

237
00:12:58,000 --> 00:13:01,410
 the less chance there is that we'll become derailed, either

238
00:13:01,410 --> 00:13:03,000
 as lay people or as monks,

239
00:13:03,000 --> 00:13:05,700
 or as lay people will feel more and more attracted to

240
00:13:05,700 --> 00:13:07,000
 helping and supporting learning,

241
00:13:07,000 --> 00:13:09,500
 studying and appreciating and following the Buddha's

242
00:13:09,500 --> 00:13:11,000
 teaching.

243
00:13:11,000 --> 00:13:14,840
 And with monks as well, we'll be able to overcome all

244
00:13:14,840 --> 00:13:16,000
 adversity

245
00:13:16,000 --> 00:13:22,490
 and all of our prior addictions and attachments and wants

246
00:13:22,490 --> 00:13:24,000
 and needs and so on.

247
00:13:24,000 --> 00:13:26,480
 So I hope that comes at least close to answering your

248
00:13:26,480 --> 00:13:27,000
 question.

249
00:13:27,000 --> 00:13:32,000
 Thanks for giving me a chance to talk about these subjects,

250
00:13:32,000 --> 00:13:35,000
 I think are very important for Buddhists.

251
00:13:35,000 --> 00:13:37,000
 Okay, that's all.

