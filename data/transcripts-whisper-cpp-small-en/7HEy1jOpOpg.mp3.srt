1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:05,160
 Today we continue on with

3
00:00:05,160 --> 00:00:12,440
 verses 104 and 105 which read as follows.

4
00:00:12,440 --> 00:00:21,780
 Atahvajitang seyo yajayang itarapajah atadantasa posasa n

5
00:00:21,780 --> 00:00:25,040
ityansanya tacharino

6
00:00:25,040 --> 00:00:32,040
 nivadevo nagandambhu namaro sahabrahmana

7
00:00:32,040 --> 00:00:43,370
 chitang apajitang gahira tatarupasa jantunu which means it

8
00:00:43,370 --> 00:00:44,600
's actually quite

9
00:00:44,600 --> 00:00:50,430
 similar to the last one. Conquest of the self is better

10
00:00:50,430 --> 00:00:51,760
 than the

11
00:00:51,760 --> 00:00:58,950
 conquest of others, of other people. Atadantasa posasa nity

12
00:00:58,950 --> 00:01:00,080
ansanya tacharino

13
00:01:00,080 --> 00:01:08,920
 for one who has conquered themselves and is constantly

14
00:01:08,920 --> 00:01:13,120
 restrained, sanyat

15
00:01:13,120 --> 00:01:22,160
 tacharino, one who fares restrained or controlled in a

16
00:01:22,160 --> 00:01:26,440
 sense of not

17
00:01:26,440 --> 00:01:31,690
 giving rise to uncontrolled behavior or activity that

18
00:01:31,690 --> 00:01:33,320
 causes harm to others.

19
00:01:33,320 --> 00:01:43,380
 Nivadevo nagandambhu nadan angel nagandambha which is also

20
00:01:43,380 --> 00:01:43,560
 another

21
00:01:43,560 --> 00:01:48,180
 type of angel I guess. Namaro sahabrahmana not Mara which

22
00:01:48,180 --> 00:01:49,280
 is like the evil

23
00:01:49,280 --> 00:02:00,640
 Satan nor Brahma's gods. Chitang apajitang gahira can turn

24
00:02:00,640 --> 00:02:03,600
 their victory into

25
00:02:03,600 --> 00:02:08,220
 defeat. Chitang apajitang apajita is defeat I believe. Chit

26
00:02:08,220 --> 00:02:09,400
ang apajitang means

27
00:02:09,400 --> 00:02:17,560
 turning victory into defeat. They will not be able to for

28
00:02:17,560 --> 00:02:19,160
 such a person. They

29
00:02:19,160 --> 00:02:22,600
 will not be able to turn the victory of such a person into

30
00:02:22,600 --> 00:02:27,280
 defeat. Great verse

31
00:02:27,280 --> 00:02:35,400
 shows the power of self-conquest. Simple story. The story

32
00:02:35,400 --> 00:02:37,200
 goes there was a Brahmin

33
00:02:37,200 --> 00:02:46,490
 who came to the Buddha and asked him a question. I guess

34
00:02:46,490 --> 00:02:47,640
 maybe he was a follower

35
00:02:47,640 --> 00:02:53,010
 of the Buddha or he understood how worthy the Buddha was or

36
00:02:53,010 --> 00:02:54,240
 how wise the

37
00:02:54,240 --> 00:03:00,320
 Buddha was. He thought he had this question. He wondered to

38
00:03:00,320 --> 00:03:05,470
 himself whether the Buddha, whether Buddhas, whether those

39
00:03:05,470 --> 00:03:05,760
 who are

40
00:03:05,760 --> 00:03:13,440
 enlightened know only what is of benefit or they also know

41
00:03:13,440 --> 00:03:14,720
 what leads to

42
00:03:14,720 --> 00:03:18,510
 detriment. Do they only know about gain or do they know

43
00:03:18,510 --> 00:03:21,360
 about loss as well?

44
00:03:21,360 --> 00:03:30,860
 Ata, do they know ata me wajanati udahu anatampi puchisa?

45
00:03:30,860 --> 00:03:32,640
 Anatampi. Do they know

46
00:03:32,640 --> 00:03:36,080
 only what is ata? Ata means what is of benefit or what is

47
00:03:36,080 --> 00:03:37,640
 of value or do they

48
00:03:37,640 --> 00:03:42,320
 also know what is of detriment? Anata which is which is

49
00:03:42,320 --> 00:03:44,040
 harmful or leads one

50
00:03:44,040 --> 00:03:48,840
 away from success, leads one away from what is useful, that

51
00:03:48,840 --> 00:03:51,120
 which is useless or

52
00:03:51,120 --> 00:03:57,560
 harmful. So he went to ask the Buddhists. I guess he had

53
00:03:57,560 --> 00:03:58,840
 some idea that the

54
00:03:58,840 --> 00:04:01,560
 Buddha only taught people good things. He only taught so

55
00:04:01,560 --> 00:04:02,600
 everyone was praising how

56
00:04:02,600 --> 00:04:06,830
 the Buddhist teachings would lead people to good things and

57
00:04:06,830 --> 00:04:07,880
 they were talking

58
00:04:07,880 --> 00:04:10,330
 about how great the Buddhist teaching Buddha would teach

59
00:04:10,330 --> 00:04:11,000
 you do this do that

60
00:04:11,000 --> 00:04:14,280
 and so he hadn't heard the Buddha talk about don't do this

61
00:04:14,280 --> 00:04:16,200
 don't do that and so

62
00:04:16,200 --> 00:04:19,130
 he went to the Buddha and the Buddha gave him an

63
00:04:19,130 --> 00:04:20,880
 interesting quote and I was

64
00:04:20,880 --> 00:04:24,270
 just having a terrible time with it because the English is

65
00:04:24,270 --> 00:04:25,760
 it's incredible

66
00:04:25,760 --> 00:04:30,910
 how sloppy they were with it. They translate fierce fierc

67
00:04:30,910 --> 00:04:32,440
eness, chandikang,

68
00:04:32,440 --> 00:04:37,270
 as moonlight which is which is well I mean it's

69
00:04:37,270 --> 00:04:39,360
 understandable because

70
00:04:39,360 --> 00:04:44,800
 chanda would be the moon but chanda with the

71
00:04:44,800 --> 00:04:50,970
 with the Helveopalasial ND anyway doesn't mean moon at all

72
00:04:50,970 --> 00:04:51,680
 unless there's

73
00:04:51,680 --> 00:04:56,120
 another version that confuses the two but it means ferocity

74
00:04:56,120 --> 00:04:56,920
 so the Buddha was

75
00:04:56,920 --> 00:05:01,980
 the Buddha says gives a list of six things and I think I've

76
00:05:01,980 --> 00:05:03,160
 got them all

77
00:05:03,160 --> 00:05:07,510
 down and he says so he goes and asked the Buddha do you

78
00:05:07,510 --> 00:05:09,320
 know about loss as well

79
00:05:09,320 --> 00:05:13,590
 and the Buddha says of course and he tells him these he

80
00:05:13,590 --> 00:05:14,840
 says atanjahang

81
00:05:14,840 --> 00:05:19,370
 brahmanajanami anatanjha I know both that which is of

82
00:05:19,370 --> 00:05:20,440
 benefit and that which

83
00:05:20,440 --> 00:05:25,800
 is to detriment and so he taught says please then tell me

84
00:05:25,800 --> 00:05:27,400
 about what is to

85
00:05:27,400 --> 00:05:32,030
 detriment and the Buddha says well then listen and he gives

86
00:05:32,030 --> 00:05:32,960
 this verse

87
00:05:32,960 --> 00:05:35,760
 which isn't the Dhammapandu verse but there's this is often

88
00:05:35,760 --> 00:05:36,600
 the case with these

89
00:05:36,600 --> 00:05:40,540
 stories there will be other verses that where they come

90
00:05:40,540 --> 00:05:41,880
 from exactly we don't

91
00:05:41,880 --> 00:05:44,030
 know where they've been recorded we don't know but it's

92
00:05:44,030 --> 00:05:44,760
 supposed to have

93
00:05:44,760 --> 00:05:49,200
 been said by the Buddha as well so he says usura seyang

94
00:05:49,200 --> 00:05:50,560
 which means clear that

95
00:05:50,560 --> 00:05:54,440
 one's clear it means sleeping beyond sunrise so sleeping

96
00:05:54,440 --> 00:05:55,760
 during the day when

97
00:05:55,760 --> 00:05:58,830
 it's time to get up and go to work one instead stays in bed

98
00:05:58,830 --> 00:05:59,720
 and doesn't do

99
00:05:59,720 --> 00:06:06,840
 one's work alas young laziness and one is just lazy and

100
00:06:06,840 --> 00:06:07,800
 doesn't want to do good

101
00:06:07,800 --> 00:06:12,920
 things chandikang means again ferocity so being mean or

102
00:06:12,920 --> 00:06:15,200
 nasty or cruel being

103
00:06:15,200 --> 00:06:18,460
 being you know when people are overbearing and just

104
00:06:18,460 --> 00:06:19,920
 constantly irritable

105
00:06:19,920 --> 00:06:24,640
 and then Ligasone de young is an interesting one but I

106
00:06:24,640 --> 00:06:26,040
 think because

107
00:06:26,040 --> 00:06:31,270
 Ligas means long but Sondia means means addiction to

108
00:06:31,270 --> 00:06:33,040
 intoxication or addiction

109
00:06:33,040 --> 00:06:37,910
 to drink usually refers to and maybe intoxication is better

110
00:06:37,910 --> 00:06:39,480
 so I think it

111
00:06:39,480 --> 00:06:42,710
 says something to do with in drinking intoxicating drinks

112
00:06:42,710 --> 00:06:43,480
 and it may be a

113
00:06:43,480 --> 00:06:46,150
 corruption but I probably just don't know what it means

114
00:06:46,150 --> 00:06:47,340
 anyway but the

115
00:06:47,340 --> 00:06:50,600
 English is terrible it doesn't have any of that what does

116
00:06:50,600 --> 00:06:52,240
 it say long continued

117
00:06:52,240 --> 00:06:58,580
 prosperity which is ridiculous Ligas is long but long

118
00:06:58,580 --> 00:06:59,800
 continued prosperity is

119
00:06:59,800 --> 00:07:02,630
 not a cause for ruin except it can make you negligent I

120
00:07:02,630 --> 00:07:03,560
 suppose but that's

121
00:07:03,560 --> 00:07:08,270
 certainly not what's being said here and then we've got I

122
00:07:08,270 --> 00:07:09,960
 think it's addiction to

123
00:07:09,960 --> 00:07:15,140
 strong drink why because these come from the or these are

124
00:07:15,140 --> 00:07:17,200
 echoed in the

125
00:07:17,200 --> 00:07:19,200
 the D

126
00:07:19,200 --> 00:07:20,200
 the D

127
00:07:20,200 --> 00:07:24,200
 the D

128
00:07:24,200 --> 00:07:25,200
 the D

129
00:07:25,200 --> 00:07:26,200
 the D

130
00:07:26,200 --> 00:07:27,200
 the D

131
00:07:27,200 --> 00:07:28,200
 the D

132
00:07:28,200 --> 00:07:29,200
 the D

133
00:07:29,200 --> 00:07:30,200
 the D

134
00:07:30,200 --> 00:07:31,200
 the D

135
00:07:31,200 --> 00:07:32,200
 the D

136
00:07:32,200 --> 00:07:33,200
 the D

137
00:07:33,200 --> 00:07:34,200
 the D

138
00:07:34,200 --> 00:07:35,200
 the D

139
00:07:35,200 --> 00:07:36,200
 the D

140
00:07:36,200 --> 00:07:37,200
 the D

141
00:07:37,200 --> 00:07:38,200
 the D

142
00:07:38,200 --> 00:07:39,200
 the D

143
00:07:39,200 --> 00:07:40,200
 the D

144
00:07:40,200 --> 00:07:41,200
 the D

145
00:07:41,200 --> 00:07:42,200
 the D

146
00:07:42,200 --> 00:07:43,200
 the D

147
00:07:43,200 --> 00:07:44,200
 the D

148
00:07:44,200 --> 00:07:45,200
 the D

149
00:07:45,200 --> 00:07:46,200
 the D

150
00:07:46,200 --> 00:07:47,200
 the D

151
00:07:47,200 --> 00:07:48,200
 the D

152
00:07:48,200 --> 00:07:49,200
 the D

153
00:07:49,200 --> 00:07:50,200
 the D

154
00:07:50,200 --> 00:07:51,200
 the D

155
00:07:51,200 --> 00:07:52,200
 the D

156
00:07:52,200 --> 00:07:53,200
 the D

157
00:07:53,200 --> 00:07:54,200
 the D

158
00:07:54,200 --> 00:07:55,200
 the D

159
00:07:55,200 --> 00:07:56,200
 the D

160
00:07:56,200 --> 00:07:57,200
 the D

161
00:07:57,200 --> 00:07:58,200
 the D

162
00:07:58,200 --> 00:07:59,200
 the D

163
00:07:59,200 --> 00:08:00,200
 the D

164
00:08:00,200 --> 00:08:01,200
 the D

165
00:08:01,200 --> 00:08:02,200
 the D

166
00:08:02,200 --> 00:08:03,200
 the D

167
00:08:03,200 --> 00:08:04,200
 the D

168
00:08:04,200 --> 00:08:05,200
 the D

169
00:08:05,200 --> 00:08:06,200
 the D

170
00:08:06,200 --> 00:08:07,200
 the D

171
00:08:07,200 --> 00:08:08,200
 the D

172
00:08:08,200 --> 00:08:09,200
 the D

173
00:08:09,200 --> 00:08:10,200
 the D

174
00:08:10,200 --> 00:08:11,200
 the D

175
00:08:11,200 --> 00:08:12,200
 the D

176
00:08:12,200 --> 00:08:13,200
 the D

177
00:08:13,200 --> 00:08:14,200
 the D

178
00:08:14,200 --> 00:08:15,200
 the D

179
00:08:15,200 --> 00:08:16,200
 the D

180
00:08:16,200 --> 00:08:17,200
 the D

181
00:08:17,200 --> 00:08:18,200
 the D

182
00:08:18,200 --> 00:08:19,200
 the D

183
00:08:19,200 --> 00:08:20,200
 the D

184
00:08:20,200 --> 00:08:21,200
 the D

185
00:08:21,200 --> 00:08:22,200
 the D

186
00:08:22,200 --> 00:08:23,200
 the D

187
00:08:23,200 --> 00:08:24,200
 the D

188
00:08:24,200 --> 00:08:25,200
 the D

189
00:08:25,200 --> 00:08:26,200
 the D

190
00:08:26,200 --> 00:08:27,200
 the D

191
00:08:27,200 --> 00:08:28,200
 the D

192
00:08:28,200 --> 00:08:29,200
 the D

193
00:08:29,200 --> 00:08:30,200
 the D

194
00:08:30,200 --> 00:08:31,200
 the D

195
00:08:31,200 --> 00:08:32,200
 the D

196
00:08:32,200 --> 00:08:33,200
 the D

197
00:08:33,200 --> 00:08:34,200
 the D

198
00:08:34,200 --> 00:08:35,200
 the D

199
00:08:35,200 --> 00:08:36,200
 the D

200
00:08:36,200 --> 00:08:37,200
 the D

201
00:08:37,200 --> 00:08:38,200
 the D

202
00:08:38,200 --> 00:08:39,200
 the D

203
00:08:39,200 --> 00:08:40,200
 the D

204
00:08:40,200 --> 00:08:41,200
 the D

205
00:08:41,200 --> 00:08:42,200
 the D

206
00:08:42,200 --> 00:08:43,200
 the D

207
00:08:43,200 --> 00:08:44,200
 the D

208
00:08:44,200 --> 00:08:50,330
 and it's curious, you know, because he doesn't say gambling

209
00:08:50,330 --> 00:08:53,240
 and yet gambling is considered one of the things that leads

210
00:08:53,240 --> 00:08:54,200
 to laws.

211
00:08:54,200 --> 00:08:57,400
 I don't believe it's in here. If I've translated it

212
00:08:57,400 --> 00:08:59,200
 correctly, it's not there.

213
00:08:59,200 --> 00:09:03,200
 And in the Sigalawada Sutta, it's clearly there.

214
00:09:03,200 --> 00:09:09,370
 And that's interesting because the Brahman is then

215
00:09:09,370 --> 00:09:13,200
 impressed and says, "Very good, very good. That's awesome.

216
00:09:13,200 --> 00:09:16,200
 "Well, well spoken. You're really, really a great teacher

217
00:09:16,200 --> 00:09:19,490
 and you really know what is a benefit and what is a

218
00:09:19,490 --> 00:09:21,200
 detriment."

219
00:09:21,200 --> 00:09:26,530
 And then the Buddha asks him, "So Brahman, how do you make

220
00:09:26,530 --> 00:09:28,200
 your livelihood?"

221
00:09:28,200 --> 00:09:32,550
 And turns out the Brahman makes his, this Brahman makes his

222
00:09:32,550 --> 00:09:34,200
 livelihood through gambling.

223
00:09:34,200 --> 00:09:36,200
 That's how he makes a living.

224
00:09:36,200 --> 00:09:45,040
 I guess by, he must be sort of one of these people who, a

225
00:09:45,040 --> 00:09:47,200
 con artist, right?

226
00:09:47,200 --> 00:09:51,360
 Or something like that. How to make his, I guess there are

227
00:09:51,360 --> 00:09:52,200
 professional poker players nowadays.

228
00:09:52,200 --> 00:09:55,200
 It's a big thing apparently to gamble online.

229
00:09:55,200 --> 00:10:02,740
 I have people who tell me about that, how they do gambling

230
00:10:02,740 --> 00:10:04,200
 online.

231
00:10:04,200 --> 00:10:10,230
 And so it's interesting that the Buddha didn't include it,

232
00:10:10,230 --> 00:10:15,070
 but that is common to be very careful not to attack a

233
00:10:15,070 --> 00:10:17,200
 person directly.

234
00:10:17,200 --> 00:10:21,610
 So if he had brought up gambling, it would have, there's a

235
00:10:21,610 --> 00:10:24,980
 little bit, as a Buddhist, it's more curious than this Brah

236
00:10:24,980 --> 00:10:27,200
man probably realized.

237
00:10:27,200 --> 00:10:31,540
 He doesn't bring it up, but yet he wants to make it clear

238
00:10:31,540 --> 00:10:35,340
 that gambling is in there as well without actually saying,

239
00:10:35,340 --> 00:10:38,200
 "Oh, gambling is going to lead you to ruin."

240
00:10:38,200 --> 00:10:41,820
 It's as though he purposefully left it out or it's, this

241
00:10:41,820 --> 00:10:50,200
 story, part of the reason for telling this story is to,

242
00:10:50,200 --> 00:10:54,300
 or part of the meaning behind the story is to show that he

243
00:10:54,300 --> 00:10:58,200
 didn't attack him directly. It's quite curious.

244
00:10:58,200 --> 00:11:03,990
 But can be a really important aspect of teaching. So anyway

245
00:11:03,990 --> 00:11:08,200
, the point is he tells him,

246
00:11:08,200 --> 00:11:11,200
 the Brahman says he makes his living by gambling.

247
00:11:11,200 --> 00:11:17,230
 And this is the funny sort of inside joke that here we can

248
00:11:17,230 --> 00:11:21,210
 kind of smile because we know what's kind of what's going

249
00:11:21,210 --> 00:11:23,200
 through the Buddhist mind to some extent.

250
00:11:23,200 --> 00:11:30,620
 That he knows that this Brahman is on a state of loss and

251
00:11:30,620 --> 00:11:34,200
 is heading in a bad way.

252
00:11:34,200 --> 00:11:38,710
 And so then the Buddha asks him, "So who wins when you

253
00:11:38,710 --> 00:11:43,200
 gamble? Do you win or does your opponents win?"

254
00:11:43,200 --> 00:11:46,660
 He said, "Oh, well, sometimes I win, sometimes my opponents

255
00:11:46,660 --> 00:11:47,200
 win."

256
00:11:47,200 --> 00:11:52,270
 And then the Buddha says, then the Buddha lays down the law

257
00:11:52,270 --> 00:11:55,200
 and says, "Brahman, that's not a real victory."

258
00:11:55,200 --> 00:12:00,190
 As you can see, that's the nature of victory. Sometimes you

259
00:12:00,190 --> 00:12:02,200
 win, sometimes you lose.

260
00:12:02,200 --> 00:12:05,200
 And we have this saying, "You win some, you lose some."

261
00:12:05,200 --> 00:12:08,700
 That's not a really... I mean, on balance you come out with

262
00:12:08,700 --> 00:12:11,200
 nothing, right? You never come out ahead.

263
00:12:11,200 --> 00:12:13,610
 Or it's certainly not guaranteed that you're going to come

264
00:12:13,610 --> 00:12:16,200
 out ahead. And there must be some other factors.

265
00:12:16,200 --> 00:12:20,190
 Often people would say luck is all that allows you to come

266
00:12:20,190 --> 00:12:21,200
 out ahead.

267
00:12:21,200 --> 00:12:23,640
 And as we talked about, of course, it doesn't matter

268
00:12:23,640 --> 00:12:25,200
 whether you come out ahead or not.

269
00:12:25,200 --> 00:12:29,590
 When you come out ahead, what happens? Well, you beget enm

270
00:12:29,590 --> 00:12:33,200
ity. Your opponents are upset because they've lost.

271
00:12:33,200 --> 00:12:36,720
 When you win, someone has to lose. That kind of victory is

272
00:12:36,720 --> 00:12:38,200
 not really...

273
00:12:38,200 --> 00:12:41,270
 He says a trifling. That's how the English translate it.

274
00:12:41,270 --> 00:12:43,200
 You can't really trust the English, no, can we?

275
00:12:43,200 --> 00:12:53,710
 "Parajayo." He says that's a low sort of... I believe...

276
00:12:53,710 --> 00:12:54,200
 Jayo? No, Jayopi?

277
00:12:54,200 --> 00:12:58,200
 "Oti, parajayo ti." No, that's a question, sorry.

278
00:12:58,200 --> 00:13:01,200
 "Brahman apamatakoya." So it's a trifle.

279
00:13:01,200 --> 00:13:04,080
 He doesn't say that's a bad kind of victory or that kind of

280
00:13:04,080 --> 00:13:07,680
 victory is actually leading you to ruin, which in fact it

281
00:13:07,680 --> 00:13:08,200
 is.

282
00:13:08,200 --> 00:13:14,030
 He says that's a trifling victory. So in a roundabout way,

283
00:13:14,030 --> 00:13:16,820
 he's trying to get this Brahman to see the error of his

284
00:13:16,820 --> 00:13:17,200
 ways.

285
00:13:17,200 --> 00:13:19,660
 You know, provide him with some friendly advice. The Brah

286
00:13:19,660 --> 00:13:22,200
man did ask, after all, what leads to loss.

287
00:13:22,200 --> 00:13:27,220
 And so he's trying to somehow tell him that his life's down

288
00:13:27,220 --> 00:13:28,200
 to that.

289
00:13:28,200 --> 00:13:30,990
 But instead he says, so to be very gentle, he says that's a

290
00:13:30,990 --> 00:13:34,200
 trifling victory. A real victory is self-victory.

291
00:13:34,200 --> 00:13:39,760
 A real conquest? That's conquering yourself. Why? Because

292
00:13:39,760 --> 00:13:43,200
 that kind of conquest, you'll never lose.

293
00:13:43,200 --> 00:13:47,690
 And so it's poignant in relation to this story, the idea of

294
00:13:47,690 --> 00:13:49,200
 being invincible.

295
00:13:49,200 --> 00:13:57,200
 Because the win of a gambler is always subject to threat.

296
00:13:57,200 --> 00:14:02,520
 The next roll of the dice, the next deal of the cards could

297
00:14:02,520 --> 00:14:06,200
 change your fortune, could spell ruin.

298
00:14:06,200 --> 00:14:11,880
 And much victory is like this. Victory in war, victory in

299
00:14:11,880 --> 00:14:18,600
 business, victory in romance, victory of all kinds is

300
00:14:18,600 --> 00:14:21,200
 unpredictable.

301
00:14:21,200 --> 00:14:27,200
 And is unstable, is impermanent. It doesn't last forever.

302
00:14:27,200 --> 00:14:35,200
 It's always subject to the potential of being overthrown.

303
00:14:35,200 --> 00:14:38,640
 But that's really the claim that's being made. So this is

304
00:14:38,640 --> 00:14:40,200
 how it relates to our practice.

305
00:14:40,200 --> 00:14:44,770
 It's an understanding of the extent and the magnitude of

306
00:14:44,770 --> 00:14:48,200
 the practice that we're undertaking.

307
00:14:48,200 --> 00:14:52,700
 I mean, this isn't just stress relief. I was thinking today

308
00:14:52,700 --> 00:14:56,200
 about advertising the benefits of meditation.

309
00:14:56,200 --> 00:15:02,180
 We're probably going to put up posters. And it made me

310
00:15:02,180 --> 00:15:06,200
 think of, you know, people really, really,

311
00:15:06,200 --> 00:15:09,490
 it's really catchy to say mindfulness-based stress

312
00:15:09,490 --> 00:15:13,200
 reduction because it's non-sectarian, it's non-religious.

313
00:15:13,200 --> 00:15:17,200
 It's really something that people can, sort of, rolls off

314
00:15:17,200 --> 00:15:18,200
 the tongue.

315
00:15:18,200 --> 00:15:21,890
 But then I thought it's kind of, you know, petty to say

316
00:15:21,890 --> 00:15:23,840
 stress reduction because that's not really what we're

317
00:15:23,840 --> 00:15:25,200
 talking about, is it?

318
00:15:25,200 --> 00:15:29,160
 In Buddhism we go quite, it's mindfulness-based or it's

319
00:15:29,160 --> 00:15:30,200
 based on sati.

320
00:15:30,200 --> 00:15:35,180
 But it goes farther than just reducing stress. It uproots

321
00:15:35,180 --> 00:15:38,910
 anything that could possibly cause you stress in the future

322
00:15:38,910 --> 00:15:39,200
.

323
00:15:39,200 --> 00:15:46,320
 There's no potential for future stress. Meaning you're

324
00:15:46,320 --> 00:15:47,200
 invincible.

325
00:15:47,200 --> 00:15:51,940
 And it goes beyond heaven, it goes beyond God, it goes

326
00:15:51,940 --> 00:15:54,200
 beyond the universe.

327
00:15:54,200 --> 00:16:00,850
 Something that goes beyond loss, something that can never

328
00:16:00,850 --> 00:16:03,200
 be taken from you.

329
00:16:03,200 --> 00:16:09,900
 We're talking about invincibility. How does this come about

330
00:16:09,900 --> 00:16:10,200
?

331
00:16:10,200 --> 00:16:19,380
 Comes about by a person who is ata-danta, bhosa, nityang s

332
00:16:19,380 --> 00:16:27,200
anyatacarino, who is constantly, constantly restrained.

333
00:16:27,200 --> 00:16:33,520
 Meaning they never, when seeing a form with the eye, they

334
00:16:33,520 --> 00:16:36,200
 never give rise to likes or dislikes.

335
00:16:36,200 --> 00:16:40,200
 They see it as it is in their objective.

336
00:16:40,200 --> 00:16:47,540
 They free themselves from the potential for, they free

337
00:16:47,540 --> 00:16:54,940
 themselves from involvement in this game of gain and loss

338
00:16:54,940 --> 00:16:59,200
 and happiness and suffering.

339
00:16:59,200 --> 00:17:04,630
 It sounds actually quite fearsome for people who are

340
00:17:04,630 --> 00:17:10,380
 addicted to pleasure and who are caught up in this game of

341
00:17:10,380 --> 00:17:12,200
 gain and loss.

342
00:17:12,200 --> 00:17:17,440
 So it's something that you do have to kind of finesse when

343
00:17:17,440 --> 00:17:19,200
 you talk to people.

344
00:17:19,200 --> 00:17:23,880
 And people often accuse, make the accusation that such a

345
00:17:23,880 --> 00:17:27,200
 person would be someone like a zombie, right?

346
00:17:27,200 --> 00:17:34,260
 When they are restrained, means they are just suppressing

347
00:17:34,260 --> 00:17:36,200
 their urges, which isn't at all the case.

348
00:17:36,200 --> 00:17:40,510
 Suppressing your urges wouldn't work. It's not something

349
00:17:40,510 --> 00:17:42,200
 that is sustainable.

350
00:17:42,200 --> 00:17:53,470
 It's not the true victory. True victory is invincible,

351
00:17:53,470 --> 00:17:57,200
 imperturbable.

352
00:17:57,200 --> 00:18:00,090
 I was thinking about this today, the whole idea of the

353
00:18:00,090 --> 00:18:01,200
 zombie, right?

354
00:18:01,200 --> 00:18:03,970
 So we have this idea that a person who doesn't like and

355
00:18:03,970 --> 00:18:06,200
 dislike things is just like a zombie.

356
00:18:06,200 --> 00:18:09,200
 Their life has no flavor, has no meaning.

357
00:18:09,200 --> 00:18:13,960
 But then if you look at people who are caught up, if you

358
00:18:13,960 --> 00:18:17,200
 think of the state of mind,

359
00:18:17,200 --> 00:18:20,610
 the person who is caught up in chasing after things,

360
00:18:20,610 --> 00:18:25,200
 objects of their desire, how much like a zombie they are,

361
00:18:25,200 --> 00:18:31,200
 how unaware they are of their own mind, of their own state,

362
00:18:31,200 --> 00:18:36,200
 how inflamed the mind becomes with passion and hate,

363
00:18:36,200 --> 00:18:40,830
 and how the mind just in general becomes inflamed, and so

364
00:18:40,830 --> 00:18:45,200
 is subject to greed, anger and delusion constantly.

365
00:18:45,200 --> 00:18:49,100
 Sometimes with such intense greed that they will do

366
00:18:49,100 --> 00:18:51,200
 anything to get what they want,

367
00:18:51,200 --> 00:18:56,650
 even at the detriment to other people, even at the cost of

368
00:18:56,650 --> 00:18:58,200
 friendship.

369
00:18:58,200 --> 00:19:05,340
 Sometimes give rise to such states of delusion that they

370
00:19:05,340 --> 00:19:09,200
 alienate all of their friends

371
00:19:09,200 --> 00:19:16,370
 due to their oppressive, arrogant, conceited nature, the

372
00:19:16,370 --> 00:19:18,200
 level of conceit that we're capable of,

373
00:19:18,200 --> 00:19:21,710
 the level of arrogance we're capable of, and how awful it

374
00:19:21,710 --> 00:19:24,200
 is, and how harmful it is.

375
00:19:26,200 --> 00:19:32,590
 And anger, how we can be so caught up with rage and hatred,

376
00:19:32,590 --> 00:19:41,880
 constantly depressed or angry, sad, afraid, all of these

377
00:19:41,880 --> 00:19:43,200
 negative states.

378
00:19:43,200 --> 00:19:49,280
 It's kind of like a zombie, like one of those zombie movies

379
00:19:49,280 --> 00:19:52,200
 or zombie shows that everyone's watching.

380
00:19:54,200 --> 00:19:58,190
 Evil zombies, how horrific they are. That's kind of what we

381
00:19:58,190 --> 00:19:59,200
're talking about.

382
00:19:59,200 --> 00:20:01,200
 Enlightened people are nothing like that.

383
00:20:01,200 --> 00:20:04,690
 An enlightened one is the opposite of a zombie. They are

384
00:20:04,690 --> 00:20:11,200
 awake, they are alive, they never die, they're invincible.

385
00:20:11,200 --> 00:20:14,630
 And so this is what we're striving for. It's something

386
00:20:14,630 --> 00:20:15,200
 quite profound.

387
00:20:15,200 --> 00:20:19,590
 I teach meditation, now teaching at the university, and I

388
00:20:19,590 --> 00:20:21,200
 teach to all sorts of people,

389
00:20:21,200 --> 00:20:24,160
 and a lot of people are just in it for stress reduction,

390
00:20:24,160 --> 00:20:25,200
 stress relief.

391
00:20:25,200 --> 00:20:29,350
 And so it kind of pulls you in and you start to realize, oh

392
00:20:29,350 --> 00:20:31,200
, it goes a little bit deeper,

393
00:20:31,200 --> 00:20:35,580
 and you end up down the rabbit hole, so to speak, where you

394
00:20:35,580 --> 00:20:39,200
 don't recognize the world around you anymore.

395
00:20:39,200 --> 00:20:42,180
 Your whole world changes. I mean, it's not like it's a trap

396
00:20:42,180 --> 00:20:45,200
 or something, it's just the sweater starts to unravel.

397
00:20:46,200 --> 00:20:54,200
 The whole illusion that you built up around yourself, and

398
00:20:54,200 --> 00:20:57,200
 who you thought you were starts to unravel,

399
00:20:57,200 --> 00:21:00,020
 and you see how asleep you've been. You're like a sleep

400
00:21:00,020 --> 00:21:01,200
walker, like a zombie.

401
00:21:01,200 --> 00:21:04,210
 That's how it should be. That's how it really is in

402
00:21:04,210 --> 00:21:05,200
 meditation.

403
00:21:05,200 --> 00:21:08,200
 You start to wake up and see, oh, I've got these problems.

404
00:21:08,200 --> 00:21:13,200
 My mind has greed, my mind has anger, my mind has delusion.

405
00:21:14,200 --> 00:21:18,280
 Oh boy, I'm in trouble. And so you start to do the hard

406
00:21:18,280 --> 00:21:20,200
 work of changing that.

407
00:21:20,200 --> 00:21:22,200
 That's what we do in meditation.

408
00:21:22,200 --> 00:21:27,840
 And that is the highest victory. It's a victory that no one

409
00:21:27,840 --> 00:21:29,200
 can take from you.

410
00:21:29,200 --> 00:21:32,380
 That's the point of this verse, and it's very well said, as

411
00:21:32,380 --> 00:21:35,200
 are all the things the Buddha said.

412
00:21:35,200 --> 00:21:39,530
 So something that we should appreciate and remember to give

413
00:21:39,530 --> 00:21:42,700
 us confidence that what we're aiming for is not something

414
00:21:42,700 --> 00:21:43,200
 simple.

415
00:21:43,200 --> 00:21:47,200
 It's not just a hobby that you can pick up and put down.

416
00:21:47,200 --> 00:21:50,980
 Something that really and truly changes your life,

417
00:21:50,980 --> 00:21:56,290
 transforms you, wakes you up, and helps you rise above your

418
00:21:56,290 --> 00:21:57,200
 troubles.

419
00:21:57,200 --> 00:22:00,710
 So that even though the situation may not change, no matter

420
00:22:00,710 --> 00:22:06,200
 what the situation is, you are above it, you are beyond it.

421
00:22:07,200 --> 00:22:13,200
 So that's the teaching for tonight, verses 104 and 105.

422
00:22:13,200 --> 00:22:17,300
 We're almost a quarter of the way through the Dhammapada. I

423
00:22:17,300 --> 00:22:21,970
 think when we get to 108, that'll be one quarter of the way

424
00:22:21,970 --> 00:22:22,200
.

425
00:22:22,200 --> 00:22:26,670
 So thank you all for tuning in. I'm wishing you all the

426
00:22:26,670 --> 00:22:31,200
 best on your path to invincibility. Be well.

