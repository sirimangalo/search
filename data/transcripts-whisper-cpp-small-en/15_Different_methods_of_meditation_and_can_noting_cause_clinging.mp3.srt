1
00:00:00,000 --> 00:00:04,420
 Different methods of meditation and can noting cause

2
00:00:04,420 --> 00:00:05,000
 clinging?

3
00:00:05,000 --> 00:00:10,310
 What is meditation? Is X, insert method here, a valid

4
00:00:10,310 --> 00:00:11,000
 meditation?

5
00:00:11,000 --> 00:00:14,000
 Can noting lead to clinging?

6
00:00:14,000 --> 00:00:18,000
 Meditation is any kind of mental practice.

7
00:00:18,000 --> 00:00:22,250
 It generally involves habits, cultivating or working with

8
00:00:22,250 --> 00:00:24,000
 mental habits in some way.

9
00:00:24,000 --> 00:00:27,000
 It can be about breaking down habits.

10
00:00:27,000 --> 00:00:30,290
 For every moment of experience, there are many ways you can

11
00:00:30,290 --> 00:00:31,000
 respond.

12
00:00:31,000 --> 00:00:35,000
 You can think about the experience. You can react to it.

13
00:00:35,000 --> 00:00:39,040
 You can try to suppress it. You can try and find a way to

14
00:00:39,040 --> 00:00:40,000
 avoid it.

15
00:00:40,000 --> 00:00:44,000
 You can go with it or engage with it.

16
00:00:44,000 --> 00:00:48,500
 You can encourage it to continue. You can enjoy it. You can

17
00:00:48,500 --> 00:00:49,000
 ignore it.

18
00:00:49,000 --> 00:00:53,910
 Some types of practice are only meditation in the western

19
00:00:53,910 --> 00:00:55,000
 sense of the word.

20
00:00:55,000 --> 00:00:58,000
 Mollying over or pondering something.

21
00:00:58,000 --> 00:01:01,720
 These are not valid forms of meditation in a Buddhist sense

22
00:01:01,720 --> 00:01:02,000
.

23
00:01:02,000 --> 00:01:06,190
 When you feel pain, if you contemplate it intellectually,

24
00:01:06,190 --> 00:01:08,000
 thinking to yourself,

25
00:01:08,000 --> 00:01:11,520
 "This isn't permanent. This is suffering. This is not self

26
00:01:11,520 --> 00:01:12,000
."

27
00:01:12,000 --> 00:01:15,000
 Then you are creating abstract thought about it.

28
00:01:15,000 --> 00:01:18,000
 You are not actually cultivating mindfulness.

29
00:01:18,000 --> 00:01:22,270
 Your mind is focused on the thoughts and you are not

30
00:01:22,270 --> 00:01:24,000
 actually paying attention

31
00:01:24,000 --> 00:01:28,320
 to the experience. The difference between abstraction and

32
00:01:28,320 --> 00:01:30,000
 saying to yourself,

33
00:01:30,000 --> 00:01:36,650
 "Pain, pain, or thinking, thinking," is that repeating the

34
00:01:36,650 --> 00:01:38,000
 name of the experience

35
00:01:38,000 --> 00:01:41,000
 puts your attention on the experience itself.

36
00:01:41,000 --> 00:01:45,080
 Evoking a consciousness that is free from any kind of

37
00:01:45,080 --> 00:01:47,000
 judgment, abstraction,

38
00:01:47,000 --> 00:01:50,420
 or diversification. Free from making more out of the

39
00:01:50,420 --> 00:01:53,000
 experience than it actually is.

40
00:01:53,000 --> 00:01:57,510
 Noting is a unique form of interaction designed to create

41
00:01:57,510 --> 00:01:59,000
 an objective state of mind

42
00:01:59,000 --> 00:02:03,000
 where you simply experience the object as it is.

43
00:02:03,000 --> 00:02:07,130
 Qualities like patience, equanimity, and even peace and

44
00:02:07,130 --> 00:02:08,000
 tranquility

45
00:02:08,000 --> 00:02:11,900
 are all associated with this non-reactive state of

46
00:02:11,900 --> 00:02:13,000
 awareness.

47
00:02:13,000 --> 00:02:18,200
 Our purpose is to cultivate a state where we do not create,

48
00:02:18,200 --> 00:02:19,000
 proliferate,

49
00:02:19,000 --> 00:02:23,000
 or make anything out of the objects of our experience.

50
00:02:23,000 --> 00:02:26,400
 In Buddhist theory, "bhava" is the link in the chain that

51
00:02:26,400 --> 00:02:28,000
 leads to suffering.

52
00:02:28,000 --> 00:02:32,000
 "Bhava" means making something out of something.

53
00:02:32,000 --> 00:02:35,890
 For example, if someone insults you and you get upset about

54
00:02:35,890 --> 00:02:36,000
 it,

55
00:02:36,000 --> 00:02:40,000
 make something of it, then you have engaged in "bhava."

56
00:02:40,000 --> 00:02:44,000
 "Bhava" means literally being or becoming.

57
00:02:44,000 --> 00:02:49,330
 Any time you make a big or small deal of something, you are

58
00:02:49,330 --> 00:02:50,000
 creating "bhava."

59
00:02:50,000 --> 00:02:54,320
 The use of the mantra or noting is special in that it is t

60
00:02:54,320 --> 00:02:56,000
autological.

61
00:02:56,000 --> 00:03:00,000
 It is not saying anything new about the experience.

62
00:03:00,000 --> 00:03:03,850
 It is just reminding you to experience the object simply

63
00:03:03,850 --> 00:03:05,000
 for what it is.

64
00:03:05,000 --> 00:03:09,240
 Noting is unique in that it results in an absence of

65
00:03:09,240 --> 00:03:10,000
 results.

66
00:03:10,000 --> 00:03:12,000
 And it should feel like that.

67
00:03:12,000 --> 00:03:15,000
 It should feel like you have gained a respite,

68
00:03:15,000 --> 00:03:19,060
 being given a moment that is free from any contemplating,

69
00:03:19,060 --> 00:03:21,000
 reacting, or diversifying.

70
00:03:21,000 --> 00:03:26,000
 On the other hand, the result of noting is not nothing.

71
00:03:26,000 --> 00:03:30,060
 When you remind yourself of the objective nature of any

72
00:03:30,060 --> 00:03:31,000
 experience,

73
00:03:31,000 --> 00:03:34,000
 you create a state of objectivity.

74
00:03:34,000 --> 00:03:37,730
 And more importantly, you foster a habit of being objective

75
00:03:37,730 --> 00:03:38,000
.

76
00:03:38,000 --> 00:03:41,360
 People who never practice meditation are constantly

77
00:03:41,360 --> 00:03:43,000
 engaging with experiences

78
00:03:43,000 --> 00:03:47,000
 based on specific habits of reactivity.

79
00:03:47,000 --> 00:03:52,000
 Liking, disliking, identifying, judging, etc.

80
00:03:52,000 --> 00:03:56,000
 These reactions are so immediate and so habitual

81
00:03:56,000 --> 00:03:59,000
 that they seem to be a part of the experience itself.

82
00:03:59,000 --> 00:04:04,000
 In a similar manner, meditation develops specific habits.

83
00:04:04,000 --> 00:04:08,080
 But the habits in meditation, especially mindfulness

84
00:04:08,080 --> 00:04:09,000
 meditation,

85
00:04:09,000 --> 00:04:11,000
 are much simpler.

86
00:04:11,000 --> 00:04:16,630
 By saying to ourselves, "Pain, pain, or thinking, thinking

87
00:04:16,630 --> 00:04:17,000
,"

88
00:04:17,000 --> 00:04:21,000
 we evoke states of experiencing things just as they are.

89
00:04:21,000 --> 00:04:24,330
 By practicing this way regularly, such states become

90
00:04:24,330 --> 00:04:26,000
 habitual,

91
00:04:26,000 --> 00:04:28,000
 flowing into our ordinary lives.

92
00:04:28,000 --> 00:04:32,000
 At work or with family, in situations,

93
00:04:32,000 --> 00:04:36,000
 we would ordinarily be stressed, reactive, or reactionary.

94
00:04:36,000 --> 00:04:39,000
 We will find ourselves far less reactionary

95
00:04:39,000 --> 00:04:42,240
 because of the habits that we are developing through

96
00:04:42,240 --> 00:04:43,000
 mindfulness practice.

97
00:04:43,000 --> 00:04:47,000
 Meditation is not magic, and it is not a quick fix

98
00:04:47,000 --> 00:04:51,270
 because we are dealing with old habits that are years and

99
00:04:51,270 --> 00:04:53,000
 perhaps lifetimes old.

100
00:04:53,000 --> 00:04:57,270
 But the change takes place quite clearly, and the evidence

101
00:04:57,270 --> 00:04:59,000
 is empirically observable.

102
00:04:59,000 --> 00:05:03,420
 It involves simple principles of cultivating habits and how

103
00:05:03,420 --> 00:05:05,000
 they affect one's mind.

104
00:05:05,000 --> 00:05:09,260
 Furthermore, becoming habitually objective allows you to

105
00:05:09,260 --> 00:05:11,000
 see your experiences

106
00:05:11,000 --> 00:05:13,000
 and reactions more clearly than before.

107
00:05:13,000 --> 00:05:17,000
 You will see clearly how bad habits are bad.

108
00:05:17,000 --> 00:05:22,050
 Anger, greed, conceit, arrogance, worry, restlessness,

109
00:05:22,050 --> 00:05:24,000
 distraction, and so on.

110
00:05:24,000 --> 00:05:27,960
 You will also see the experiences you react to more clearly

111
00:05:27,960 --> 00:05:29,000
 themselves.

112
00:05:29,000 --> 00:05:34,220
 You will see your thoughts, the rationalization, and extrap

113
00:05:34,220 --> 00:05:35,000
olation

114
00:05:35,000 --> 00:05:39,000
 that go on in the mind facilitating our reactions.

115
00:05:39,000 --> 00:05:42,000
 Most importantly, you will come to see the characteristics

116
00:05:42,000 --> 00:05:46,000
 of impermanence, suffering, and non-self

117
00:05:46,000 --> 00:05:48,000
 in all aspects of your experience.

118
00:05:48,000 --> 00:05:52,510
 You will see that the things that you used to cling to or

119
00:05:52,510 --> 00:05:55,000
 want are not worth wanting

120
00:05:55,000 --> 00:05:58,430
 because the stability you saw in them, the satisfaction you

121
00:05:58,430 --> 00:05:59,000
 saw in them,

122
00:05:59,000 --> 00:06:03,380
 and the control that you thought you had over them was an

123
00:06:03,380 --> 00:06:04,000
 illusion,

124
00:06:04,000 --> 00:06:07,000
 something ill-conceived out of ignorance.

125
00:06:07,000 --> 00:06:10,770
 When you see your experiences as they truly are, you see

126
00:06:10,770 --> 00:06:13,000
 that they are unpredictable,

127
00:06:13,000 --> 00:06:18,310
 inconstant, and impermanent, unsatisfying, and unable to

128
00:06:18,310 --> 00:06:20,000
 facilitate satisfaction

129
00:06:20,000 --> 00:06:24,770
 and uncontrollable, unmanageable, without any substance

130
00:06:24,770 --> 00:06:26,000
 whatsoever.

131
00:06:26,000 --> 00:06:30,240
 It is this clarity of vision that allows us to let go and

132
00:06:30,240 --> 00:06:33,000
 become truly independent in the world.

133
00:06:33,000 --> 00:06:36,350
 The question of whether noting can lead to clinging is a

134
00:06:36,350 --> 00:06:37,000
 common one.

135
00:06:37,000 --> 00:06:41,640
 When hearing about this practice of saying to yourself, "

136
00:06:41,640 --> 00:06:45,000
angry, angry, or liking, liking,"

137
00:06:45,000 --> 00:06:50,000
 people often ask, "Wouldn't that just make the anger worse?

138
00:06:50,000 --> 00:06:53,000
 Wouldn't that just make me like or want that thing more?"

139
00:06:53,000 --> 00:06:56,000
 It is reasonable to ask this.

140
00:06:56,000 --> 00:07:00,010
 If you actually practice noting, however, you will see that

141
00:07:00,010 --> 00:07:01,000
 it is not so.

142
00:07:01,000 --> 00:07:05,000
 When we experience something pleasant, why do we want it?

143
00:07:05,000 --> 00:07:08,420
 What is it that makes us angry about experiences we don't

144
00:07:08,420 --> 00:07:09,000
 like?

145
00:07:09,000 --> 00:07:14,000
 Clearly, it is a bias or prejudice that leads to each.

146
00:07:14,000 --> 00:07:18,410
 The purpose of reminding yourself of the nature of the

147
00:07:18,410 --> 00:07:21,000
 object is to prevent the biased perspective

148
00:07:21,000 --> 00:07:26,000
 that allows reactions like greed and anger to arise.

149
00:07:26,000 --> 00:07:29,530
 When states like greed and anger arise, there is a judgment

150
00:07:29,530 --> 00:07:31,000
 of the experience as

151
00:07:31,000 --> 00:07:35,000
 "good, bad, me, mine, etc."

152
00:07:35,000 --> 00:07:39,000
 We often reinforce these perceptions with thoughts like

153
00:07:39,000 --> 00:07:42,000
 "This is good" or "This is bad."

154
00:07:42,000 --> 00:07:45,270
 If you are angry at someone, you might think to yourself, "

155
00:07:45,270 --> 00:07:47,000
I hate that person."

156
00:07:47,000 --> 00:07:50,870
 Such a thought makes you more angry because your focus is

157
00:07:50,870 --> 00:07:53,000
 on the person, not the anger.

158
00:07:53,000 --> 00:07:56,010
 When you are anxious, you normally perceive the anxiety

159
00:07:56,010 --> 00:07:57,000
 with more anxiety,

160
00:07:57,000 --> 00:08:00,950
 perceiving it as a problem you possess rather than merely

161
00:08:00,950 --> 00:08:02,000
 an experience.

162
00:08:02,000 --> 00:08:07,990
 When you say to yourself, "Angry, angry," or "Anxious,

163
00:08:07,990 --> 00:08:09,000
 anxious,"

164
00:08:09,000 --> 00:08:13,020
 it does not escalate because you see it as an experience

165
00:08:13,020 --> 00:08:16,000
 rather than a problem or possession.

166
00:08:16,000 --> 00:08:20,310
 Noting is basically saying, "This is this," rather than

167
00:08:20,310 --> 00:08:22,000
 saying, "This is bad," or,

168
00:08:22,000 --> 00:08:27,000
 "This is good," or, "This is mine," etc.

