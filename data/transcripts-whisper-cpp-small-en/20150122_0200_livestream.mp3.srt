1
00:00:00,000 --> 00:00:07,910
 Okay, good morning. If all is gone as planned then I'm

2
00:00:07,910 --> 00:00:17,000
 broadcasting live here from Chiang Mai.

3
00:00:24,000 --> 00:00:32,000
 Bangkok, sorry. Yes, the live feed is going.

4
00:00:32,000 --> 00:00:38,250
 Okay, so this morning, or for a while now, I've been

5
00:00:38,250 --> 00:00:47,000
 interested in looking at something called the Mula Kasuta,

6
00:00:51,000 --> 00:00:56,160
 which is a, it's not really a sutta as we understand it in

7
00:00:56,160 --> 00:00:59,280
 the West. We hear about these sutta's or sutras and we

8
00:00:59,280 --> 00:01:01,000
 think of these long things,

9
00:01:01,000 --> 00:01:06,400
 but there are many, many, many little sutta's, some of

10
00:01:06,400 --> 00:01:14,000
 which are incredibly valuable, but terse.

11
00:01:15,000 --> 00:01:21,350
 They simply say what they have to say and that's it. And so

12
00:01:21,350 --> 00:01:27,000
 they're, they've been recorded in their bare essence,

13
00:01:27,000 --> 00:01:30,890
 giving a teaching, so it'll, they weren't, it wasn't even

14
00:01:30,890 --> 00:01:35,560
 recorded where it was given. Usually they are understood to

15
00:01:35,560 --> 00:01:39,000
 be given, have been given in Sawati.

16
00:01:40,000 --> 00:01:44,370
 So sometimes they start with just the word Sawati nidana

17
00:01:44,370 --> 00:01:47,000
 means it took place in Sawati.

18
00:01:47,000 --> 00:01:53,240
 But most of them don't even go that far. They just assume

19
00:01:53,240 --> 00:01:57,000
 that it's understood unless otherwise stated.

20
00:01:58,000 --> 00:02:09,060
 So this one starts with the Buddha saying, let's get the

21
00:02:09,060 --> 00:02:10,000
 Pali up here.

22
00:02:10,000 --> 00:02:16,070
 Sache bhikkhu vay anya titya paribhajaka hi wang bhuche y

23
00:02:16,070 --> 00:02:17,000
ung.

24
00:02:18,000 --> 00:02:28,360
 It's an interesting, interesting start. What's useful or

25
00:02:28,360 --> 00:02:30,990
 important about this sutta is the Buddha's telling them

26
00:02:30,990 --> 00:02:35,000
 that if an anya titya,

27
00:02:36,000 --> 00:02:45,000
 someone who holds another view, anya titya paribhajaka,

28
00:02:45,000 --> 00:02:48,000
 someone who has gone forth a recluse in another religion,

29
00:02:48,000 --> 00:02:52,140
 if so, if we can translate it generally in modern terms, if

30
00:02:52,140 --> 00:02:57,680
 someone from another religion or an ordained, someone who

31
00:02:57,680 --> 00:03:01,000
 is ordained in another religion asks you,

32
00:03:02,000 --> 00:03:06,000
 you know, it's sort of a test or a debate.

33
00:03:06,000 --> 00:03:11,000
 King moolaka avuso sabhay damma.

34
00:03:11,000 --> 00:03:20,000
 Friend having what as their root are all dhammas.

35
00:03:20,000 --> 00:03:25,350
 That's the Pali way of expressing things. I mean, what do

36
00:03:25,350 --> 00:03:29,370
 all dhammas have as their root or what is the root of all d

37
00:03:29,370 --> 00:03:30,000
hammas?

38
00:03:30,000 --> 00:03:37,000
 King moolaka, having what as their root?

39
00:03:37,000 --> 00:03:44,420
 So right away we have a important question and the answer

40
00:03:44,420 --> 00:03:49,300
 is going to be decisive. So this is a sutta you can always

41
00:03:49,300 --> 00:03:50,000
 come back to.

42
00:03:53,000 --> 00:03:55,840
 If you want to know what is the root of all dhammas

43
00:03:55,840 --> 00:03:59,190
 according to the Buddha, the word dhamma here means all

44
00:03:59,190 --> 00:04:01,000
 realities.

45
00:04:01,000 --> 00:04:10,320
 Or you could potentially say the understanding here is its

46
00:04:10,320 --> 00:04:13,000
 results or outcomes.

47
00:04:14,000 --> 00:04:22,000
 All situations or states, I would say, including...

48
00:04:22,000 --> 00:04:30,000
 Or yes, out of everything, which one is the root?

49
00:04:31,000 --> 00:04:37,180
 And here the root would be the root of happiness and

50
00:04:37,180 --> 00:04:43,990
 suffering, the root of good and evil, etc. Root of all

51
00:04:43,990 --> 00:04:45,000
 things.

52
00:04:45,000 --> 00:04:51,380
 And then he goes on, that's only the first one. There's

53
00:04:51,380 --> 00:04:57,000
 nine more. He asks, what if they ask you, king sambhawa?

54
00:04:57,000 --> 00:05:05,000
 King sambhawa, what is the...

55
00:05:23,000 --> 00:05:31,000
 What is that which causes them, brings them into being?

56
00:05:31,000 --> 00:05:41,000
 What is their arising?

57
00:05:46,000 --> 00:05:52,310
 King samudaya, what do they arise from? Or what is the

58
00:05:52,310 --> 00:05:54,000
 cause? Samudaya.

59
00:05:54,000 --> 00:06:14,380
 King samosarana, what do they have as their meeting place

60
00:06:14,380 --> 00:06:14,740
 or convergence? Where do they converge? Where do they come

61
00:06:14,740 --> 00:06:15,000
 together? Samosarana.

62
00:06:17,000 --> 00:06:27,200
 King bhamukha, what is their leader? King adipateya, what

63
00:06:27,200 --> 00:06:31,000
 is their master?

64
00:06:36,000 --> 00:06:44,000
 King utra, what is the pinnacle or the highest?

65
00:06:44,000 --> 00:06:52,000
 What is their lord?

66
00:06:52,000 --> 00:07:00,000
 What is higher than all dhammas? What is above them?

67
00:07:02,000 --> 00:07:13,560
 King saara, king saara sabedhamma, what is the essence or

68
00:07:13,560 --> 00:07:19,000
 the core? Of all dhammas, which one is core?

69
00:07:19,000 --> 00:07:30,000
 King ogada, ogada sabedhamma.

70
00:07:32,000 --> 00:07:36,000
 What is the...

71
00:07:36,000 --> 00:07:44,910
 That which they become immersed in? Or what is the complete

72
00:07:44,910 --> 00:07:49,000
 immersion? Or...

73
00:07:54,000 --> 00:07:59,640
 Sort of like the resting place, but the word here is that

74
00:07:59,640 --> 00:08:03,000
 which they become immersed in.

75
00:08:10,000 --> 00:08:19,800
 King paryosana, what is their ending, complete and final

76
00:08:19,800 --> 00:08:24,000
 ending of all dhammas?

77
00:08:27,000 --> 00:08:34,900
 So he asks ten very pertinent and potentially difficult

78
00:08:34,900 --> 00:08:40,040
 questions. And he says if they ask you these ten questions,

79
00:08:40,040 --> 00:08:45,000
 how are you going to answer? How would you answer?

80
00:08:47,000 --> 00:08:51,350
 The monks are... they play it safe as usual. This is the

81
00:08:51,350 --> 00:08:57,000
 standard response. They say, "Oh, Bhagavangmulaka novande d

82
00:08:57,000 --> 00:08:58,000
hamma."

83
00:08:59,000 --> 00:09:07,530
 The Blessed One is... the dhamma is rooted in the Blessed

84
00:09:07,530 --> 00:09:15,030
 One. It comes from the Blessed One. The Blessed One is our

85
00:09:15,030 --> 00:09:19,000
 guide, nithika. The Blessed One is our refuge.

86
00:09:20,000 --> 00:09:24,820
 "Satu vata bande bhagavantam nieva patibhatu." It would be

87
00:09:24,820 --> 00:09:29,240
 good if the Blessed One were to tell us. Don't make us

88
00:09:29,240 --> 00:09:31,000
 guess, please.

89
00:09:31,000 --> 00:09:36,990
 Tell us the meaning. "Eta sabhasi dasa ato." Tell us the

90
00:09:36,990 --> 00:09:41,000
 meaning of this bhassita, this saying.

91
00:09:42,000 --> 00:09:47,320
 "Bhagavatos utva bhikkhu dharasanti." Having heard the

92
00:09:47,320 --> 00:09:52,210
 Blessed One, the bhikkhus will carry it, will hold it. Hold

93
00:09:52,210 --> 00:09:58,190
 that to be true. "Dharasanti." This is... "Dharati" is... "

94
00:09:58,190 --> 00:10:02,700
Dharati" means to hold... it's actually from the same root

95
00:10:02,700 --> 00:10:04,000
 as the word "dharma."

96
00:10:05,000 --> 00:10:09,020
 That's what "dharma" means. They will hold it as "dharma,"

97
00:10:09,020 --> 00:10:13,760
 whatever the Buddha says. So they play it safe. They say, "

98
00:10:13,760 --> 00:10:16,000
Lord Buddha, please tell us."

99
00:10:17,000 --> 00:10:22,370
 "Tay nahi mikvei sunata sadukang manasi karoda." In that

100
00:10:22,370 --> 00:10:28,340
 case, bhikkhus, listen well. Keep it in mind. "I will speak

101
00:10:28,340 --> 00:10:30,000
 bhasis ami."

102
00:10:30,000 --> 00:10:37,060
 "Evang bande," they say. "Yes, Venerable Sir." Very good.

103
00:10:37,060 --> 00:10:40,000
 Let it be thus Venerable Sir.

104
00:10:42,000 --> 00:10:46,000
 "Bhagavahitadobhoccha." The Blessed One spoke thus.

105
00:10:48,000 --> 00:10:55,140
 "If sajebi kuvei nyatitya paribhajika, if recluses of other

106
00:10:55,140 --> 00:11:01,460
 religions or other views ask you these ten questions, you

107
00:11:01,460 --> 00:11:04,000
 should answer as follows."

108
00:11:05,000 --> 00:11:10,430
 "Chanda moolaka auso sabedam." "Chanda" is the root of all

109
00:11:10,430 --> 00:11:16,150
 dhams. "Chanda" means contentment or interest, desire you

110
00:11:16,150 --> 00:11:18,000
 could even say.

111
00:11:20,000 --> 00:11:27,090
 But interest is probably a safer word here. Or contentment

112
00:11:27,090 --> 00:11:34,000
 in the sense of inclining towards, inclination maybe.

113
00:11:34,000 --> 00:11:38,370
 Maybe we'll just go with desire even though it's a tough

114
00:11:38,370 --> 00:11:39,000
 one.

115
00:11:41,000 --> 00:11:51,000
 "Sambhava," what is the... "Sambhava," what is there coming

116
00:11:51,000 --> 00:11:51,000
 into being?

117
00:11:51,000 --> 00:11:55,230
 It's very tough. A couple of these I don't quite understand

118
00:11:55,230 --> 00:11:59,750
 and even the commentary is not clear on because the Pali is

119
00:11:59,750 --> 00:12:03,000
 not... I mean, it's quite terse.

120
00:12:04,000 --> 00:12:10,300
 "Sambhava" means coming into being, so "manasikara,"

121
00:12:10,300 --> 00:12:14,000
 keeping in mind or attention.

122
00:12:14,000 --> 00:12:20,180
 Attention is what allows, it's like the catalyst, I guess,

123
00:12:20,180 --> 00:12:23,000
 that allows things to arise.

124
00:12:24,000 --> 00:12:29,320
 "Pasa samudaya," "Pasa" is that which causes things to

125
00:12:29,320 --> 00:12:34,000
 arise. Contact, contact is the samudaya.

126
00:12:39,000 --> 00:12:44,980
 "Vedana samosarana," "Vedana," feelings are the feeling. "V

127
00:12:44,980 --> 00:12:50,450
edana" is a bit more than feeling, it's the actual sensation

128
00:12:50,450 --> 00:12:54,000
 or the tasting of the experience.

129
00:12:54,000 --> 00:12:57,670
 So it's where everything gathers, it's the meeting place,

130
00:12:57,670 --> 00:13:01,000
 the convergence. Everything converges on "Vedana."

131
00:13:02,000 --> 00:13:06,530
 All the other aspects of experience rely upon "Vedana" or

132
00:13:06,530 --> 00:13:09,000
 come together at "Vedana."

133
00:13:09,000 --> 00:13:17,040
 "Samadhi pamukha sabhi damma," concentration or focus is

134
00:13:17,040 --> 00:13:20,000
 the leader of all dhammas.

135
00:13:22,000 --> 00:13:27,420
 "Sattadhi pateya sabha," "Sattadhi pateya sabhi damma," all

136
00:13:27,420 --> 00:13:34,000
 dhammas have sati, mindfulness as their master.

137
00:13:34,000 --> 00:13:42,920
 "Pannutara sabhi damma," wisdom is the highest store, that

138
00:13:42,920 --> 00:13:51,000
 which is above, that which, the peak of all dhammas.

139
00:13:52,000 --> 00:13:57,150
 "Vimutri saara sabhi damma," freedom or release is the goal

140
00:13:57,150 --> 00:14:03,320
 or the essence, but the word essence here isn't correct, it

141
00:14:03,320 --> 00:14:08,790
's the, that which is essential or that which has true

142
00:14:08,790 --> 00:14:14,610
 meaning or benefit, that which is, the goal is maybe the

143
00:14:14,610 --> 00:14:19,000
 best non-literal translation here.

144
00:14:20,000 --> 00:14:25,640
 "Amatogata sabhi damma," amata, the deathless, is that

145
00:14:25,640 --> 00:14:29,000
 which everything plunges into.

146
00:14:29,000 --> 00:14:34,410
 "Nibbana paryosana sabhi damma," and "Nibbana" is the

147
00:14:34,410 --> 00:14:40,000
 ending, the complete and total ending of all dhammas.

148
00:14:45,000 --> 00:14:48,550
 So I think this is an interesting sutta worth a little bit

149
00:14:48,550 --> 00:14:50,000
 of investigation.

150
00:14:50,000 --> 00:14:54,950
 Definitely these are ten of the most important dhammas

151
00:14:54,950 --> 00:15:00,380
 there are, and here they are classified. It's understood

152
00:15:00,380 --> 00:15:03,000
 the importance of these ten.

153
00:15:04,000 --> 00:15:09,270
 So Chandamulaka is interesting, it's an important fact,

154
00:15:09,270 --> 00:15:14,100
 aspect of the Buddhist teaching that whatever you incline

155
00:15:14,100 --> 00:15:19,800
 your mind towards, whatever you're interested in, that's

156
00:15:19,800 --> 00:15:22,000
 what comes to me.

157
00:15:22,000 --> 00:15:25,690
 So if you incline your mind in a good way, good things come

158
00:15:25,690 --> 00:15:29,000
. You incline your mind in a bad way, bad things come.

159
00:15:30,000 --> 00:15:34,730
 Whatever you incline your mind towards, that's where your

160
00:15:34,730 --> 00:15:36,000
 future lies.

161
00:15:36,000 --> 00:15:40,340
 Whatever you're interested in, well that's what gives rise

162
00:15:40,340 --> 00:15:41,000
 to it.

163
00:15:41,000 --> 00:15:49,250
 You can't blame external, you can only blame external

164
00:15:49,250 --> 00:15:52,000
 factors so far.

165
00:15:53,000 --> 00:15:57,630
 In the end it comes down to our own reactions to them and

166
00:15:57,630 --> 00:16:01,810
 how we change and how we work with what we've got in the

167
00:16:01,810 --> 00:16:04,000
 present moment to change.

168
00:16:04,000 --> 00:16:08,000
 Because everything comes from the mind.

169
00:16:08,000 --> 00:16:13,310
 "Manobhabhankama dhamma," all dhammas come from the mind

170
00:16:13,310 --> 00:16:18,660
 specifically here from our interest, our desires, our incl

171
00:16:18,660 --> 00:16:20,000
inations.

172
00:16:22,000 --> 00:16:29,070
 "Manasikara" means attention, so whatever you give

173
00:16:29,070 --> 00:16:34,000
 attention to, that causes things to happen.

174
00:16:34,000 --> 00:16:40,130
 They're the instigator. That's what instigates the next one

175
00:16:40,130 --> 00:16:42,000
, which is "pasa."

176
00:16:43,000 --> 00:16:53,870
 So these two are interesting. It's a reminder of the

177
00:16:53,870 --> 00:16:56,700
 importance of, or it's a reminder of the paradigm in the

178
00:16:56,700 --> 00:17:02,000
 Buddha's teaching that it's experiential.

179
00:17:03,000 --> 00:17:05,260
 Nothing exists until it's experienced or except for

180
00:17:05,260 --> 00:17:13,000
 experience. Experience is, it arises and it ceases.

181
00:17:13,000 --> 00:17:23,910
 All dhammas come into being with contact. Contact means

182
00:17:23,910 --> 00:17:27,650
 contact, not physical contact, it means contact with the

183
00:17:27,650 --> 00:17:28,000
 mind.

184
00:17:29,000 --> 00:17:32,900
 It's a mind contact based on "manasikara," based on our

185
00:17:32,900 --> 00:17:34,000
 attention.

186
00:17:34,000 --> 00:17:38,900
 When we're interested in something, then we incline our

187
00:17:38,900 --> 00:17:43,000
 minds there and that inclination gives rise to attention,

188
00:17:43,000 --> 00:17:46,000
 which gives rise to contact.

189
00:17:46,000 --> 00:17:49,720
 Contact is at the eye, the ear, the nose, the tongue, the

190
00:17:49,720 --> 00:17:52,000
 body, and the heart or the mind.

191
00:17:53,000 --> 00:17:56,150
 In any one of these six states, then the five aggregates

192
00:17:56,150 --> 00:17:58,000
 arise and there's experience.

193
00:17:58,000 --> 00:18:03,000
 That experience arises and ceases. That's reality.

194
00:18:03,000 --> 00:18:07,500
 So this is where our meditation takes us. It helps us to

195
00:18:07,500 --> 00:18:13,070
 see these experiences one by one by one and see many

196
00:18:13,070 --> 00:18:17,000
 interesting things about them and to see how

197
00:18:19,000 --> 00:18:25,740
 we are relating to these experiences, whether for profit or

198
00:18:25,740 --> 00:18:30,000
 for ill. It helps us to adjust our habits.

199
00:18:30,000 --> 00:18:35,360
 This is because these are the building blocks of our lives,

200
00:18:35,360 --> 00:18:37,000
 of our reality.

201
00:18:37,000 --> 00:18:43,800
 So as we understand them, we can rearrange them based on

202
00:18:43,800 --> 00:18:46,000
 our understanding.

203
00:18:48,000 --> 00:18:53,000
 "King samos aruna," so "vaidana samos aruna," then.

204
00:18:53,000 --> 00:18:57,990
 "Vaidana" is the key. "Vaidana" is where "tanha" arises. So

205
00:18:57,990 --> 00:19:01,000
 based on "vaidana" comes "tanha."

206
00:19:01,000 --> 00:19:08,310
 So there comes real desire, so thirst or craving based on "

207
00:19:08,310 --> 00:19:10,000
vaidana."

208
00:19:11,000 --> 00:19:13,960
 "Vaidana" is the key. It's the crux. It's where everything

209
00:19:13,960 --> 00:19:16,700
 comes together. But it's also the meaning here. It's the

210
00:19:16,700 --> 00:19:19,000
 essence of experience.

211
00:19:19,000 --> 00:19:22,480
 That's what the word "vaidana" really means. "Vaidana"

212
00:19:22,480 --> 00:19:26,000
 means to experience. "Vaidana" means the experience of it.

213
00:19:26,000 --> 00:19:31,030
 So generally understood to refer to the pleasant or painful

214
00:19:31,030 --> 00:19:36,000
 or neutral aspect of the experience, how it's received.

215
00:19:36,000 --> 00:19:45,560
 "Vaidana" is understood to be like the king. When the king

216
00:19:45,560 --> 00:19:51,130
 is eating, so many people have to prepare the food and

217
00:19:51,130 --> 00:19:52,000
 bring it to the king.

218
00:19:52,000 --> 00:19:57,260
 Only the king himself gets to taste it. "Vaidana" is the

219
00:19:57,260 --> 00:20:00,000
 one that tastes the experience.

220
00:20:05,000 --> 00:20:12,030
 "Samadhi pamukha" focus is the leader. This is, I think,

221
00:20:12,030 --> 00:20:17,480
 the most useful part in my mind of the suttas, the

222
00:20:17,480 --> 00:20:20,410
 difference between "samadhi" and "sati," because I use this

223
00:20:20,410 --> 00:20:23,000
 aspect of these two together.

224
00:20:24,000 --> 00:20:29,750
 Often I'll remind people of these things. "Samadhi" is the

225
00:20:29,750 --> 00:20:35,000
 leader, but mindfulness is the master or the Lord.

226
00:20:38,000 --> 00:20:42,290
 What does this mean? It means that "samadhi" is at the

227
00:20:42,290 --> 00:20:49,000
 front. "Samadhi" leads you. But mindfulness is the guide.

228
00:20:49,000 --> 00:20:52,000
 Mindfulness is the controller.

229
00:20:55,000 --> 00:21:00,500
 The important point here is that if you have "samadhi" but

230
00:21:00,500 --> 00:21:05,880
 "no sati," then you go, but you don't know where you're

231
00:21:05,880 --> 00:21:07,000
 going.

232
00:21:07,000 --> 00:21:10,150
 It's like a boat without a rudder or a boat without a

233
00:21:10,150 --> 00:21:15,670
 captain. The boat just drifts, even though there's great

234
00:21:15,670 --> 00:21:18,000
 wind and it can be quite powerful.

235
00:21:19,000 --> 00:21:22,900
 It's like a car without a driver. Who knows where it's

236
00:21:22,900 --> 00:21:26,900
 going to go? So concentration is this way. If you practice

237
00:21:26,900 --> 00:21:30,000
 meditation without mindfulness, just focusing the mind.

238
00:21:30,000 --> 00:21:33,210
 Many people do this. They think meditation is just... the

239
00:21:33,210 --> 00:21:36,720
 goal is just focus your mind. You don't realize that there

240
00:21:36,720 --> 00:21:38,000
's a quality to it.

241
00:21:39,000 --> 00:21:41,500
 If that's a good quality, if your mind is in a good place,

242
00:21:41,500 --> 00:21:44,590
 then the concentration will lead you in a good way. But if

243
00:21:44,590 --> 00:21:49,560
 your mind comes to be in a bad way, the concentration will

244
00:21:49,560 --> 00:21:53,740
 lead you to... can drive you crazy even without mindfulness

245
00:21:53,740 --> 00:21:54,000
.

246
00:21:54,000 --> 00:21:58,150
 In meditation we focus more on mindfulness, cultivating

247
00:21:58,150 --> 00:22:02,690
 mindfulness and letting the concentration develop naturally

248
00:22:02,690 --> 00:22:03,000
.

249
00:22:04,000 --> 00:22:08,370
 You want the concentration to develop slowly. If it

250
00:22:08,370 --> 00:22:15,000
 develops quickly without mindfulness, then it's dangerous.

251
00:22:15,000 --> 00:22:18,550
 If you're developing mindfulness and you could say

252
00:22:18,550 --> 00:22:22,610
 concentration together, then you have the power and you

253
00:22:22,610 --> 00:22:24,000
 have the control.

254
00:22:25,000 --> 00:22:32,000
 Mindfulness is what keeps the mind from going off track.

255
00:22:47,000 --> 00:22:49,870
 But without samadhi, the other thing is if you have just

256
00:22:49,870 --> 00:22:52,840
 sati but no samadhi, you also can't... you don't go

257
00:22:52,840 --> 00:22:56,000
 anywhere at all. Your mind is too distracted.

258
00:22:56,000 --> 00:23:01,000
 So you need both samadhi and sati.

259
00:23:04,000 --> 00:23:13,260
 Panyutara samvitam. Wisdom is the highest or the pinnacle

260
00:23:13,260 --> 00:23:15,000
 or the peak. It's above all dhammas.

261
00:23:22,000 --> 00:23:29,110
 This is another very important aspect of the Buddhist. I

262
00:23:29,110 --> 00:23:34,270
 mean, this really outlines Buddhism in a very compact

263
00:23:34,270 --> 00:23:35,000
 manner.

264
00:23:35,000 --> 00:23:38,500
 By giving this list of ten things, it really sets out, this

265
00:23:38,500 --> 00:23:41,560
 is why he asks, he says, "If others ask you so they can be

266
00:23:41,560 --> 00:23:44,000
 clear, this is what Buddhism believes."

267
00:23:45,000 --> 00:23:48,440
 Understand that Buddhism, Buddhism wisdom is the highest.

268
00:23:48,440 --> 00:23:51,000
 This is another clear feature of Buddhism.

269
00:23:51,000 --> 00:23:56,020
 It's not compassion, it's not love, it's not mindfulness,

270
00:23:56,020 --> 00:24:01,000
 it's not concentration, not happiness, it's not peace.

271
00:24:01,000 --> 00:24:05,930
 It's wisdom that is the highest. It's wisdom that we put up

272
00:24:05,930 --> 00:24:07,000
 at the top.

273
00:24:08,000 --> 00:24:14,330
 It's what rises above all dhammas. Wisdom is like being at

274
00:24:14,330 --> 00:24:19,160
 the top of a mountain, looking down, and you can see

275
00:24:19,160 --> 00:24:20,000
 everything.

276
00:24:20,000 --> 00:24:23,000
 But you don't get caught up in everything.

277
00:24:23,000 --> 00:24:28,160
 So our goal in meditation is always wisdom, always

278
00:24:28,160 --> 00:24:32,560
 understanding, learning more about ourselves, understanding

279
00:24:32,560 --> 00:24:33,000
 how we work,

280
00:24:34,000 --> 00:24:37,170
 understanding our defilements, understanding our suffering,

281
00:24:37,170 --> 00:24:40,000
 understanding happiness, understanding goodness.

282
00:24:45,000 --> 00:24:50,610
 We mutti-sara, the essence or the goal of all dhammas, out

283
00:24:50,610 --> 00:24:56,260
 of all dhammas what is the goal? The goal is freedom. The

284
00:24:56,260 --> 00:25:01,990
 goal is to be free, to not be bound to anything, to not be

285
00:25:01,990 --> 00:25:04,000
 imprisoned by anything,

286
00:25:07,000 --> 00:25:10,370
 to not be imprisoned by our defilements, to not be

287
00:25:10,370 --> 00:25:14,760
 imprisoned by pretty things or attractive things, to not be

288
00:25:14,760 --> 00:25:18,000
 imprisoned by hatred or aversion.

289
00:25:18,000 --> 00:25:22,600
 These are all prisons. To not be imprisoned by ignorance

290
00:25:22,600 --> 00:25:27,810
 and delusion and confusion, doubt, worry, to not become a

291
00:25:27,810 --> 00:25:32,000
 slave to our minds, our emotions, and so on.

292
00:25:33,000 --> 00:25:44,360
 To be free, to not have to jump, every time we wander, when

293
00:25:44,360 --> 00:25:48,000
 we don't want.

294
00:25:51,000 --> 00:25:59,370
 Amatogada, samayidama. Amata is the that which things

295
00:25:59,370 --> 00:26:01,000
 plunge into.

296
00:26:01,000 --> 00:26:07,620
 The meaning here, if you unpack it, is that there's a sense

297
00:26:07,620 --> 00:26:13,990
 of plunging into the deathless. The Buddha uses this phrase

298
00:26:13,990 --> 00:26:17,000
, plunge into.

299
00:26:17,000 --> 00:26:27,000
 Deathless. Deathlessness.

300
00:26:27,000 --> 00:26:33,960
 Because all dhammas are death, most dhammas, all other dham

301
00:26:33,960 --> 00:26:41,000
mas besides nirvana are mortal. They arise and they cease.

302
00:26:42,000 --> 00:26:49,150
 But when they cease, that cessation is deathless. Or lack

303
00:26:49,150 --> 00:26:58,140
 of arising, or the cessation, the voidness that is beyond

304
00:26:58,140 --> 00:27:04,290
 arising, that is eternal, doesn't die because it doesn't

305
00:27:04,290 --> 00:27:05,000
 arise.

306
00:27:09,000 --> 00:27:21,820
 And so when someone attains magapala, nirvana, one plunges

307
00:27:21,820 --> 00:27:30,000
 into this state, plunges into deathlessness.

308
00:27:32,000 --> 00:27:37,070
 Because all the other, all the things that die have died.

309
00:27:37,070 --> 00:27:42,510
 There's no more birth, no more experiences arising. So what

310
00:27:42,510 --> 00:27:45,000
's left is called the deathless.

311
00:27:45,000 --> 00:27:51,020
 It's like the ultimate, you take a fire and you plunge it

312
00:27:51,020 --> 00:27:55,000
 into the water and the fire all goes out.

313
00:27:58,000 --> 00:28:02,240
 And finally, nirvana pariosana means, well finally, it's n

314
00:28:02,240 --> 00:28:07,000
irvana. Pariosana means the end, the total complete end.

315
00:28:07,000 --> 00:28:12,350
 That all dhammas, the only end is nirvana. Everything else

316
00:28:12,350 --> 00:28:14,000
 is just a cycle.

317
00:28:14,000 --> 00:28:18,090
 So we have this cycle of samsara that we go around and

318
00:28:18,090 --> 00:28:24,000
 around and around in, until we realize nirvana.

319
00:28:25,000 --> 00:28:31,000
 That's the pariosana, the ending. No more becoming.

320
00:28:31,000 --> 00:28:39,050
 So, just a little food for thought, quite a profound and

321
00:28:39,050 --> 00:28:42,000
 important sutta.

322
00:28:42,000 --> 00:28:47,560
 That's the dhamma for today. Thank you for tuning in. Hope

323
00:28:47,560 --> 00:28:50,000
 that got broadcast.

324
00:28:51,000 --> 00:28:55,210
 And this will all, if you missed part of it, it's going to

325
00:28:55,210 --> 00:29:00,000
 be recorded hopefully and it'll be on the website at...

326
00:29:00,000 --> 00:29:06,980
 Where is it? I don't know where it is. Go to meditation.s

327
00:29:06,980 --> 00:29:09,590
iri-mungalow.org and there's a folder there, there's a link

328
00:29:09,590 --> 00:29:10,000
 there.

329
00:29:10,000 --> 00:29:14,000
 Okay, thanks for tuning in. Be well.

