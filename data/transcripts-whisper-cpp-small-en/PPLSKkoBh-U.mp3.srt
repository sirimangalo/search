1
00:00:00,000 --> 00:00:07,000
 So here's a question. Is there a sort of Buddhist approach

2
00:00:07,000 --> 00:00:09,240
 when talking to people who accept

3
00:00:09,240 --> 00:00:13,050
 that we look at, expect that we look at them and see the

4
00:00:13,050 --> 00:00:15,040
 illusion they are creating as

5
00:00:15,040 --> 00:00:19,540
 being the essence? Maybe best to just avoid. It's funny,

6
00:00:19,540 --> 00:00:22,200
 this question came up today and

7
00:00:22,200 --> 00:00:29,120
 it's come up recently, something I've been thinking about.

8
00:00:29,120 --> 00:00:31,920
 Well, sort of to beat around

9
00:00:31,920 --> 00:00:39,680
 the bush, to flush it out a little bit. I think it's quite

10
00:00:39,680 --> 00:00:42,240
 interesting and worth examining

11
00:00:42,240 --> 00:00:48,610
 the fact that in general this is what we do. And this is

12
00:00:48,610 --> 00:00:52,040
 why people who have done meditation,

13
00:00:52,040 --> 00:00:57,700
 practice, find it very tiresome and often become very

14
00:00:57,700 --> 00:01:01,120
 agitated or distressed or upset

15
00:01:01,120 --> 00:01:06,900
 when they have to be around people again. And often people

16
00:01:06,900 --> 00:01:08,080
 get the impression that when

17
00:01:08,080 --> 00:01:10,730
 they've gone to practice meditation they come back and

18
00:01:10,730 --> 00:01:14,000
 there's something wrong with them.

19
00:01:14,000 --> 00:01:17,380
 They start to doubt their practice and everyone else really

20
00:01:17,380 --> 00:01:18,760
 certainly doubts their practice

21
00:01:18,760 --> 00:01:20,910
 and said, "What have you done? You've gone and brainwashed

22
00:01:20,910 --> 00:01:22,160
 yourself. You're not the same

23
00:01:22,160 --> 00:01:26,780
 person you were." And they can't be in social situations,

24
00:01:26,780 --> 00:01:29,240
 they feel awkward and agitated

25
00:01:29,240 --> 00:01:32,890
 and so on. But the interesting thing here, and I think the

26
00:01:32,890 --> 00:01:34,880
 answer that should clarify

27
00:01:34,880 --> 00:01:39,940
 it up quite a bit, is that that's because they're no longer

28
00:01:39,940 --> 00:01:42,880
, I want to use a swear word,

29
00:01:42,880 --> 00:01:48,100
 but they're no longer BSing themselves, they're no longer

30
00:01:48,100 --> 00:01:50,640
 lying to the other person or lying

31
00:01:50,640 --> 00:01:54,880
 to themselves, they're no longer pretending, they're no

32
00:01:54,880 --> 00:01:57,320
 longer buying into it, or at that

33
00:01:57,320 --> 00:02:01,400
 time they're not. Now if they go on and on, eventually they

34
00:02:01,400 --> 00:02:03,040
're going to A, either buy

35
00:02:03,040 --> 00:02:07,180
 back into it, which should be the case if they haven't done

36
00:02:07,180 --> 00:02:09,360
 really intensive meditation

37
00:02:09,360 --> 00:02:13,460
 or haven't really come to change the way their mind works,

38
00:02:13,460 --> 00:02:15,600
 or they're going to become more

39
00:02:15,600 --> 00:02:20,490
 skillful at avoiding problems. More skillful in dealing

40
00:02:20,490 --> 00:02:23,560
 with the situation. Because it's

41
00:02:23,560 --> 00:02:27,040
 not easy, it's not easy to be true to yourself and still

42
00:02:27,040 --> 00:02:29,000
 not have people think of you as

43
00:02:29,000 --> 00:02:37,820
 a freak. When I first started meditating, when I came back

44
00:02:37,820 --> 00:02:40,880
 home, I was sure that I had found

45
00:02:40,880 --> 00:02:44,470
 something great and that this was what I wanted to do, but

46
00:02:44,470 --> 00:02:47,160
 I couldn't figure out how to incorporate

47
00:02:47,160 --> 00:02:51,070
 it into my life. I mean it was difficult. Here I was

48
00:02:51,070 --> 00:02:53,160
 surrounded by people who, from

49
00:02:53,160 --> 00:02:59,700
 my point of view, were crazy, were all messed up. But when

50
00:02:59,700 --> 00:03:02,360
 they looked at me, I was the

51
00:03:02,360 --> 00:03:07,160
 one who was totally messed up to them. So when they would

52
00:03:07,160 --> 00:03:09,740
 yell and when they would scold

53
00:03:09,740 --> 00:03:13,480
 and complain about me, call me, say all these bad things,

54
00:03:13,480 --> 00:03:15,600
 you're like a zombie, why don't

55
00:03:15,600 --> 00:03:19,950
 you, can't you see that you've been brainwashed? I would

56
00:03:19,950 --> 00:03:22,560
 just close my eyes and go back to

57
00:03:22,560 --> 00:03:29,920
 meditating, rising, falling, and oh they would get so angry

58
00:03:29,920 --> 00:03:31,240
. But it was like that. It was

59
00:03:31,240 --> 00:03:35,550
 like, well I know what I'm doing is right, so I'll just go

60
00:03:35,550 --> 00:03:37,880
 back to doing it. And it created

61
00:03:37,880 --> 00:03:42,810
 all sorts of difficulties and problems. So that's not

62
00:03:42,810 --> 00:03:44,440
 answering your question, but hopefully

63
00:03:44,440 --> 00:03:49,140
 it's kind of setting the tone here for what we're talking

64
00:03:49,140 --> 00:03:51,840
 about. The approach, I think,

65
00:03:51,840 --> 00:03:55,300
 is just something you learn over time. And rather than talk

66
00:03:55,300 --> 00:03:56,720
 about what is the actual

67
00:03:56,720 --> 00:04:01,720
 approach, so maybe I can give some tips. I think it's more

68
00:04:01,720 --> 00:04:02,840
 important to just caution

69
00:04:02,840 --> 00:04:09,440
 that as you learn how to deal with people, that A, don't

70
00:04:09,440 --> 00:04:12,600
 buy into the idea that most

71
00:04:12,600 --> 00:04:18,640
 important is to fit in. Because this is where we lie to

72
00:04:18,640 --> 00:04:21,320
 ourselves and to other people. Most

73
00:04:21,320 --> 00:04:26,120
 of our interactions are just, oh, some people want us to

74
00:04:26,120 --> 00:04:29,080
 say certain things, they'll talk

75
00:04:29,080 --> 00:04:31,950
 about how great they are, and of course they want you to

76
00:04:31,950 --> 00:04:33,760
 say, oh yes, you're great. And

77
00:04:33,760 --> 00:04:35,770
 this is why people fight, because one person says, I'm

78
00:04:35,770 --> 00:04:37,240
 great, and the other person doesn't

79
00:04:37,240 --> 00:04:40,860
 say, oh you're great. The other person says, oh yeah, well,

80
00:04:40,860 --> 00:04:42,320
 I've done better than that.

81
00:04:42,320 --> 00:04:45,110
 And then they get angry and they fight. Because who wants

82
00:04:45,110 --> 00:04:46,840
 to sit there and compliment another

83
00:04:46,840 --> 00:04:50,630
 person all day, right? We have our own defilements and we

84
00:04:50,630 --> 00:04:53,720
 get bored at what they say. So the

85
00:04:53,720 --> 00:04:57,620
 only way to fit in with each other is to lie and to play up

86
00:04:57,620 --> 00:05:00,280
 to each other's defilements.

87
00:05:00,280 --> 00:05:03,220
 Otherwise it leads to fighting and argument. That's why

88
00:05:03,220 --> 00:05:04,960
 families end up, family members

89
00:05:04,960 --> 00:05:09,180
 end up fighting a lot, or can end up fighting, because they

90
00:05:09,180 --> 00:05:11,480
 stop playing up to each other's

91
00:05:11,480 --> 00:05:14,770
 defilements. And they go the other way and they get bored

92
00:05:14,770 --> 00:05:16,360
 of it and they don't feel the

93
00:05:16,360 --> 00:05:25,550
 need to coddle the other person or to pander. What is the

94
00:05:25,550 --> 00:05:28,800
 word? And so they get into argument.

95
00:05:28,800 --> 00:05:35,830
 So don't buy into it and watch yourself very carefully,

96
00:05:35,830 --> 00:05:39,320
 because it's a lot easier to just

97
00:05:39,320 --> 00:05:46,220
 lie and not exactly tell lies, but become fake again. And

98
00:05:46,220 --> 00:05:49,760
 to pretend and to convince

99
00:05:49,760 --> 00:05:54,050
 yourself that yeah, you do enjoy these stupid things that

100
00:05:54,050 --> 00:05:56,720
 people are talking about. And

101
00:05:56,720 --> 00:05:59,910
 go back to talking about useless things. And if you still

102
00:05:59,910 --> 00:06:02,000
 have the craving for those things,

103
00:06:02,000 --> 00:06:06,520
 then yeah, actually getting involved in talking about them.

104
00:06:06,520 --> 00:06:09,120
 So you have to learn a way to

105
00:06:09,120 --> 00:06:15,570
 not do that. And I think the easiest or the most important

106
00:06:15,570 --> 00:06:17,160
 part, and again it's easier

107
00:06:17,160 --> 00:06:23,000
 for me because I'm not in these situations. I'm able to

108
00:06:23,000 --> 00:06:25,280
 avoid them. I wear the robe and

109
00:06:25,280 --> 00:06:28,090
 people are like, "Oh, well, it's okay. He's a monk, right?

110
00:06:28,090 --> 00:06:29,440
 If he doesn't talk, if he doesn't

111
00:06:29,440 --> 00:06:32,420
 do this or that, then well, we understand because he's a

112
00:06:32,420 --> 00:06:34,800
 monk." And we may not agree,

113
00:06:34,800 --> 00:06:38,350
 but there's no expectation there. So they won't get angry

114
00:06:38,350 --> 00:06:39,760
 at me when I say, "I can't

115
00:06:39,760 --> 00:06:47,190
 go dancing," or so on. But I think an important part is to

116
00:06:47,190 --> 00:06:52,280
 keep quiet and to only talk when

117
00:06:52,280 --> 00:06:56,000
 people have questions for you and to keep your answers

118
00:06:56,000 --> 00:06:57,080
 simple. And to be more of an

119
00:06:57,080 --> 00:06:59,960
 example than a teacher, because every time you open your

120
00:06:59,960 --> 00:07:01,760
 mouth, you're going to say something

121
00:07:01,760 --> 00:07:04,630
 that makes you sound funny. You're going to say something

122
00:07:04,630 --> 00:07:05,960
 that is totally out of whack

123
00:07:05,960 --> 00:07:08,560
 with what they are. They'll be like, "Hey, don't you think

124
00:07:08,560 --> 00:07:09,880
 it's great? Don't you think

125
00:07:09,880 --> 00:07:13,090
 this movie is great? Or don't you think this..." They show

126
00:07:13,090 --> 00:07:15,240
 you something, "Isn't this beautiful?"

127
00:07:15,240 --> 00:07:19,440
 And you're like, "Well, what do I say? I mean, maybe I'm

128
00:07:19,440 --> 00:07:21,280
 attracted to it, but I know

129
00:07:21,280 --> 00:07:24,420
 that beauty is this illusion and it only is based on

130
00:07:24,420 --> 00:07:26,480
 attachment and I don't want to be

131
00:07:26,480 --> 00:07:31,710
 attached to things." So what do you say? Well, this was

132
00:07:31,710 --> 00:07:34,160
 always funny. People ask in Thailand,

133
00:07:34,160 --> 00:07:37,030
 they love to ask, "How's the food? They don't ask, 'How's

134
00:07:37,030 --> 00:07:40,560
 the food?' They say, 'Is it delicious?'"

135
00:07:40,560 --> 00:07:43,760
 And what do you say? I mean, if you say it's delicious, it

136
00:07:43,760 --> 00:07:46,280
's like encouraging or it's really

137
00:07:46,280 --> 00:07:48,660
 embarrassing to have to say that something's delicious. "

138
00:07:48,660 --> 00:07:50,000
Well, yeah, I like it, but that's

139
00:07:50,000 --> 00:07:53,900
 my own attachment." That's not a good thing. It's not a

140
00:07:53,900 --> 00:07:55,560
 compliment to say your food is

141
00:07:55,560 --> 00:08:07,850
 delicious. It's admitting my own faults. Yeah, it's

142
00:08:07,850 --> 00:08:13,040
 difficult. It's difficult. I would say,

143
00:08:13,040 --> 00:08:15,600
 the most important thing, because eventually you'll learn

144
00:08:15,600 --> 00:08:17,080
 how to do it. Eventually you'll

145
00:08:17,080 --> 00:08:21,300
 find a way to fit in. Meditation changes who you are, so it

146
00:08:21,300 --> 00:08:23,740
 upsets that routine or it upsets

147
00:08:23,740 --> 00:08:28,930
 your habit. It's scrambling the eggs, right? You can't make

148
00:08:28,930 --> 00:08:31,200
 an omelet without breaking

149
00:08:31,200 --> 00:08:34,230
 the eggs, right? So here we're breaking the eggs. The

150
00:08:34,230 --> 00:08:36,320
 breaking the eggs is the bad part,

151
00:08:36,320 --> 00:08:40,570
 but once you get used to this new way, you'll eventually

152
00:08:40,570 --> 00:08:43,160
 find a life that works with it.

153
00:08:43,160 --> 00:08:46,070
 For many people, eventually that'll be becoming a monk. For

154
00:08:46,070 --> 00:08:47,680
 sure it will be. Either that or

155
00:08:47,680 --> 00:08:50,960
 they'll just plod along. Some people will go back and give

156
00:08:50,960 --> 00:08:52,840
 up their meditation practice.

157
00:08:52,840 --> 00:09:00,170
 There are many ways. But to do it, eventually, even in your

158
00:09:00,170 --> 00:09:03,080
 own life, you will come up with

159
00:09:03,080 --> 00:09:06,710
 a way and some things will change. Maybe you'll lose a lot

160
00:09:06,710 --> 00:09:10,040
 of friends. I lost a lot of friends.

161
00:09:10,040 --> 00:09:17,120
 I don't have any of my old friends left. Maybe one or two.

162
00:09:17,120 --> 00:09:18,720
 But I have a lot of new friends,

163
00:09:18,720 --> 00:09:22,160
 and so your life will change. I guess that's another advice

164
00:09:22,160 --> 00:09:23,600
 I would give, is not to be

165
00:09:23,600 --> 00:09:33,100
 afraid of change. Never delude yourself into thinking that

166
00:09:33,100 --> 00:09:35,240
 you have some connection to

167
00:09:35,240 --> 00:09:41,190
 this or that person. When it's time to let them go, let

168
00:09:41,190 --> 00:09:44,840
 them go. Your environment will

169
00:09:44,840 --> 00:09:50,360
 change. The people you surround yourself with, the people

170
00:09:50,360 --> 00:09:53,280
 who come to connect with you will

171
00:09:53,280 --> 00:09:56,980
 change. Look at who my friends are. You're all my friends.

172
00:09:56,980 --> 00:09:58,360
 Some of you have never even

173
00:09:58,360 --> 00:10:05,490
 met. The people here, Manju is my friend. Manju is this guy

174
00:10:05,490 --> 00:10:06,920
 who helps in the monastery.

175
00:10:06,920 --> 00:10:11,760
 Who would have ever thought that he and I, we spend a lot

176
00:10:11,760 --> 00:10:14,000
 of time together and we're

177
00:10:14,000 --> 00:10:18,920
 from two totally different cultures. But that's life. Maybe

178
00:10:18,920 --> 00:10:20,960
 we have some connection in the

179
00:10:20,960 --> 00:10:25,350
 past. Maybe we don't. I don't know. What's important is

180
00:10:25,350 --> 00:10:27,400
 that we're here now. I'm not

181
00:10:27,400 --> 00:10:30,890
 going to try to change that. I would never look at Manju

182
00:10:30,890 --> 00:10:33,000
 and say, "Oh, he's just a worker

183
00:10:33,000 --> 00:10:36,540
 here. My real friends are back in Canada," or so on. I'd

184
00:10:36,540 --> 00:10:38,600
 say, "No, here I am with Manju,

185
00:10:38,600 --> 00:10:43,510
 and he's a person. He may not be meditating, but I will

186
00:10:43,510 --> 00:10:46,120
 give him my attention and try to

187
00:10:46,120 --> 00:10:52,290
 do my best in regards to his life, in regards to our

188
00:10:52,290 --> 00:10:56,880
 relationship, because it's here and

189
00:10:56,880 --> 00:11:06,110
 now." For sure, moving from one country to another really

190
00:11:06,110 --> 00:11:07,120
 opens you up to these kinds

191
00:11:07,120 --> 00:11:07,680
 of things.

192
00:11:07,680 --> 00:11:07,700
 [

193
00:11:07,700 --> 00:11:08,700
 [

