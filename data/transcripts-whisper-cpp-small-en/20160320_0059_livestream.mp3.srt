1
00:00:00,000 --> 00:00:02,000
 [silence]

2
00:00:02,000 --> 00:00:04,000
 [silence]

3
00:00:04,000 --> 00:00:06,000
 [silence]

4
00:00:32,000 --> 00:00:34,000
 Good evening everyone.

5
00:00:34,000 --> 00:00:36,000
 I'm broadcasting live.

6
00:00:36,000 --> 00:00:47,000
 Broadcasting live, March 19th.

7
00:00:47,000 --> 00:00:58,000
 I think everyone's doing everything just okay.

8
00:00:58,000 --> 00:01:00,000
 [silence]

9
00:01:02,000 --> 00:01:04,000
 [silence]

10
00:01:06,000 --> 00:01:08,000
 [silence]

11
00:01:10,000 --> 00:01:12,000
 [silence]

12
00:01:14,000 --> 00:01:16,000
 [silence]

13
00:01:18,000 --> 00:01:20,000
 [silence]

14
00:01:44,000 --> 00:01:46,000
 Sorry, I'm just reading through the comments.

15
00:01:46,000 --> 00:01:52,000
 Tonight's quote is actually a series of quotes.

16
00:01:52,000 --> 00:01:57,000
 Four verses from the Dhammapada.

17
00:01:57,000 --> 00:02:08,000
 One of which we've actually studied in our Dhammapada

18
00:02:08,000 --> 00:02:08,000
 verses.

19
00:02:08,000 --> 00:02:18,000
 [silence]

20
00:02:18,000 --> 00:02:26,000
 Not the defaults of others, not the faults of others.

21
00:02:26,000 --> 00:02:31,190
 Not what they have done, not what others have done and not

22
00:02:31,190 --> 00:02:32,000
 done.

23
00:02:33,000 --> 00:02:41,000
 Ata nova avikhe ha. One should look upon oneself.

24
00:02:41,000 --> 00:02:46,000
 Look upon those of oneself. Katani akata nita.

25
00:02:46,000 --> 00:02:50,000
 Done, things done and not done.

26
00:02:50,000 --> 00:02:59,000
 This is a theme that runs through paravada Buddhism.

27
00:03:01,000 --> 00:03:03,990
 It's our understanding that the Buddha actually taught us

28
00:03:03,990 --> 00:03:08,000
 to look after ourselves.

29
00:03:08,000 --> 00:03:12,760
 Actually on face value this one says something a little bit

30
00:03:12,760 --> 00:03:14,000
 different.

31
00:03:14,000 --> 00:03:19,000
 She says, "Don't go around blaming other people."

32
00:03:19,000 --> 00:03:25,000
 Looking at the faults of others, criticizing others.

33
00:03:26,000 --> 00:03:30,130
 But you could expand that to think about concern and

34
00:03:30,130 --> 00:03:32,000
 worrying about others.

35
00:03:32,000 --> 00:03:35,000
 Trying to fix everyone else's problems.

36
00:03:35,000 --> 00:03:45,740
 There's another Dhammapada verse. Actually it might be one

37
00:03:45,740 --> 00:03:47,000
 of these four.

38
00:03:55,000 --> 00:03:57,000
 I think it's missing here.

39
00:03:57,000 --> 00:04:00,000
 There's another one that's...

40
00:04:00,000 --> 00:04:05,000
 Ata namimabhata mampatirupayuniwesay.

41
00:04:05,000 --> 00:04:09,000
 One should set oneself in what is right first.

42
00:04:09,000 --> 00:04:13,000
 And only then should one teach others.

43
00:04:13,000 --> 00:04:17,000
 That's a wise one wouldn't defile themselves.

44
00:04:17,000 --> 00:04:22,000
 Which is really the best explanation of what's going on.

45
00:04:22,000 --> 00:04:31,000
 And some of these quotes is that in order to help others

46
00:04:31,000 --> 00:04:34,000
 you really have to help yourself.

47
00:04:34,000 --> 00:04:43,000
 And so there's this perception that it's selfish.

48
00:04:43,000 --> 00:04:48,000
 Self-centered, selfish.

49
00:04:49,000 --> 00:04:54,000
 To go off in your room and practice meditation.

50
00:04:54,000 --> 00:04:58,280
 To come to a meditation center and spend your time med

51
00:04:58,280 --> 00:05:00,000
itating apropos.

52
00:05:00,000 --> 00:05:06,850
 Just this morning something happened that I didn't really

53
00:05:06,850 --> 00:05:08,000
 expect to happen in Canada.

54
00:05:08,000 --> 00:05:11,310
 Though I suppose thinking about it, it's happened many

55
00:05:11,310 --> 00:05:12,000
 years ago.

56
00:05:12,000 --> 00:05:15,000
 But it hasn't happened in many years.

57
00:05:16,000 --> 00:05:20,200
 Someone in a van just down the street here was walking down

58
00:05:20,200 --> 00:05:23,000
 the street in a white van.

59
00:05:23,000 --> 00:05:31,000
 As it was driving by someone shouted out, "Get a job!"

60
00:05:31,000 --> 00:05:36,000
 I couldn't really believe it. It was a surprise.

61
00:05:36,000 --> 00:05:41,000
 Someone told me I should get a job.

62
00:05:41,000 --> 00:05:45,970
 Which I think is apropos because there's a sense that

63
00:05:45,970 --> 00:05:52,000
 spiritual practice is selfish, is lazy.

64
00:05:52,000 --> 00:06:06,350
 That shout, that experience this morning, actually there's

65
00:06:06,350 --> 00:06:07,000
 much more I could say on that.

66
00:06:07,000 --> 00:06:12,000
 But not that relevant. Let's not go too far afield.

67
00:06:12,000 --> 00:06:17,000
 But there is this sense, which is a shame.

68
00:06:17,000 --> 00:06:25,270
 It shows a misunderstanding of what is truly a benefit and

69
00:06:25,270 --> 00:06:28,000
 where one can be of most benefit.

70
00:06:28,000 --> 00:06:32,000
 How one can best benefit the world.

71
00:06:33,000 --> 00:06:37,800
 If everyone were to better themselves, then that's the

72
00:06:37,800 --> 00:06:39,000
 first thing you can say.

73
00:06:39,000 --> 00:06:42,400
 If everyone were to work on themselves, everyone in the

74
00:06:42,400 --> 00:06:46,000
 world, then who would need to help anyone else?

75
00:06:46,000 --> 00:06:51,510
 Second thing you could say is that someone who has worked

76
00:06:51,510 --> 00:06:58,000
 on themselves is in a far better position to help others.

77
00:06:58,000 --> 00:07:03,740
 If I just think about all the horrible things I did to

78
00:07:03,740 --> 00:07:08,000
 other people before I knew anything about meditation.

79
00:07:08,000 --> 00:07:14,900
 Mean things I've done, and unpleasantness I've created,

80
00:07:14,900 --> 00:07:17,000
 suffering I've caused.

81
00:07:17,000 --> 00:07:21,000
 And people who try to help do this as well.

82
00:07:21,000 --> 00:07:25,660
 You try to help and you just end up confusing the situation

83
00:07:25,660 --> 00:07:31,270
 because you yourself are full of biases and delusions and

84
00:07:31,270 --> 00:07:36,000
 opinions and views that are not really based on reality.

85
00:07:44,000 --> 00:07:51,500
 Meditation is actually really the highest duty that we have

86
00:07:51,500 --> 00:07:52,000
.

87
00:07:52,000 --> 00:07:59,710
 It's not an indulgence or laziness. Meditation is doing

88
00:07:59,710 --> 00:08:01,000
 your duty.

89
00:08:01,000 --> 00:08:05,930
 Just like you wouldn't walk into a crowded place if you

90
00:08:05,930 --> 00:08:11,400
 haven't showered, if you haven't cleaned your body, if you

91
00:08:11,400 --> 00:08:15,000
 haven't cleaned your clothes, washed your clothes.

92
00:08:15,000 --> 00:08:20,730
 You wouldn't go walking into a public place because there's

93
00:08:20,730 --> 00:08:26,000
 a sense that you have a duty to be clean and not smell.

94
00:08:28,000 --> 00:08:32,000
 It's unpleasant in this world.

95
00:08:32,000 --> 00:08:37,980
 And the same goes on a far more important scale with our

96
00:08:37,980 --> 00:08:39,000
 mind.

97
00:08:39,000 --> 00:08:44,330
 Though we don't do our duty, we have a duty to clean our

98
00:08:44,330 --> 00:08:45,000
 minds.

99
00:08:45,000 --> 00:08:50,000
 Because we don't do this, we fight and we argue and we

100
00:08:50,000 --> 00:08:55,000
 manipulate each other and we cling to each other.

101
00:08:57,000 --> 00:09:02,460
 We're not ready to face the world. We're not equipped to

102
00:09:02,460 --> 00:09:06,000
 live in peace and harmony.

103
00:09:06,000 --> 00:09:13,340
 We're defiled. If you want to put it in very harsh and sort

104
00:09:13,340 --> 00:09:16,000
 of negative ways.

105
00:09:16,000 --> 00:09:19,790
 In a very real sense we are defiled and until we clean

106
00:09:19,790 --> 00:09:23,000
 ourselves we shouldn't be allowed to go in public.

107
00:09:23,000 --> 00:09:27,000
 We shouldn't try to interact with other people.

108
00:09:27,000 --> 00:09:33,010
 This is why people become monks, go off in the forest

109
00:09:33,010 --> 00:09:35,000
 because they have a job to do.

110
00:09:35,000 --> 00:09:42,000
 Because they don't want to get a job. They have a job.

111
00:09:42,000 --> 00:09:45,050
 They're doing the most important job. This is why we

112
00:09:45,050 --> 00:09:49,000
 appreciate and welcome people who want to come and meditate

113
00:09:49,000 --> 00:09:49,000
.

114
00:09:49,000 --> 00:09:53,000
 Because we feel they're doing the world a service.

115
00:09:53,000 --> 00:09:56,770
 They're bettering the world just by learning about

116
00:09:56,770 --> 00:09:58,000
 themselves.

117
00:09:58,000 --> 00:10:02,150
 It's like how we say someone who keeps five precepts

118
00:10:02,150 --> 00:10:06,000
 actually is giving a great gift to the world.

119
00:10:06,000 --> 00:10:09,800
 They're not really doing something for themselves. They're

120
00:10:09,800 --> 00:10:12,000
 bestowing freedom.

121
00:10:12,000 --> 00:10:18,130
 Freedom from danger. That no being in the universe has

122
00:10:18,130 --> 00:10:22,000
 reason to fear this person.

123
00:10:22,000 --> 00:10:27,020
 When they stop killing and stealing and cheating and lying

124
00:10:27,020 --> 00:10:30,000
 they free so many beings from fear.

125
00:10:30,000 --> 00:10:35,840
 All beings actually. No being in the universe has reason to

126
00:10:35,840 --> 00:10:38,000
 fear that person.

127
00:10:38,000 --> 00:11:03,000
 So helping oneself is very important.

128
00:11:03,000 --> 00:11:07,800
 When one looks at another's faults, and as always let's try

129
00:11:07,800 --> 00:11:10,000
 and find this quote as well.

130
00:11:10,000 --> 00:11:39,000
 (Tibetan)

131
00:11:39,000 --> 00:11:45,620
 When a person looks at the faults of others, "Nithjang, Uj

132
00:11:45,620 --> 00:11:58,000
janasan" always perceiving fault. Always full of envy.

133
00:11:58,000 --> 00:12:04,060
 "Nithjang, Ujjanasan" is funny that envy is there. It's not

134
00:12:04,060 --> 00:12:10,000
 envy. "Ujjanasan" is picking on others really.

135
00:12:10,000 --> 00:12:16,000
 Seeing faults in others.

136
00:12:16,000 --> 00:12:20,000
 "Paravanjanupasisa"

137
00:12:20,000 --> 00:12:27,380
 For such a person, "Asavatasa vadhanti" such a person, "As

138
00:12:27,380 --> 00:12:31,000
avas" the taints grow, increase.

139
00:12:31,000 --> 00:12:42,140
 "Araso asavakaya" they're far away from the ending of the

140
00:12:42,140 --> 00:12:45,000
 defilements.

141
00:12:45,000 --> 00:12:56,000
 And then we skip to 159.

142
00:12:56,000 --> 00:13:13,000
 "Atanam jeta takahira yatanum manu sasati"

143
00:13:13,000 --> 00:13:18,410
 If only you would do, right? If you would do, if oneself

144
00:13:18,410 --> 00:13:23,000
 would do, what one teaches others.

145
00:13:23,000 --> 00:13:32,000
 "Sudantovattadam meta hatahikiradudamu"

146
00:13:32,000 --> 00:13:37,000
 What does that mean?

147
00:13:37,000 --> 00:13:51,000
 "Sudantovattadam meta"

148
00:13:51,000 --> 00:13:56,710
 Well trained oneself, one could then train, one should then

149
00:13:56,710 --> 00:13:58,000
 train others.

150
00:13:58,000 --> 00:14:07,000
 For it is difficult to train oneself. "Atanikiradudamu"

151
00:14:07,000 --> 00:14:11,000
 The self is difficult to tame.

152
00:14:11,000 --> 00:14:13,870
 It's easy to give advice to others, right? Actually

153
00:14:13,870 --> 00:14:18,000
 teaching is a lot easier than actually practicing.

154
00:14:18,000 --> 00:14:21,020
 So if people say thank you, to me I say no thank you, you

155
00:14:21,020 --> 00:14:23,000
're the one who's doing all the work.

156
00:14:23,000 --> 00:14:28,000
 I'm just teaching you, I'm just leading you on the path,

157
00:14:28,000 --> 00:14:34,000
 you're the one who has to do the work for yourself.

158
00:14:34,000 --> 00:14:48,000
 Even the Buddha just pointed the way.

159
00:14:48,000 --> 00:14:56,000
 And the last one which is I think probably the most.

160
00:14:56,000 --> 00:15:03,000
 359, 379, what's the second one? 379.

161
00:15:03,000 --> 00:15:07,000
 This is the most inspiring.

162
00:15:07,000 --> 00:15:17,000
 "Atanak jodaya tanang patimang seta atana"

163
00:15:17,000 --> 00:15:27,000
 "So atagutto satima sukhandikku ya isi"

164
00:15:27,000 --> 00:15:41,000
 "Jodaya tanang" That's not it.

165
00:15:41,000 --> 00:15:52,000
 "The self should guard oneself"

166
00:15:52,000 --> 00:16:07,000
 "And should examine oneself"

167
00:16:07,000 --> 00:16:15,180
 When one is self-guarded and mindful, "So atagutto satima

168
00:16:15,180 --> 00:16:19,000
 sukhandikku ya isi"

169
00:16:19,000 --> 00:16:21,000
 It's a good name for a monk.

170
00:16:21,000 --> 00:16:24,000
 "Atagutto"

171
00:16:24,000 --> 00:16:28,720
 One who guards oneself. If you want a good Pali name, there

172
00:16:28,720 --> 00:16:30,000
's a good one.

173
00:16:30,000 --> 00:16:37,300
 "Self-guarded" means to guard your senses. Guard the eye,

174
00:16:37,300 --> 00:16:41,000
 the ear, the nose, the tongue, the body, and the mind.

175
00:16:41,000 --> 00:16:45,380
 So the six doors, it's a good description of the meditation

176
00:16:45,380 --> 00:16:49,920
 practice because all of our experiences come through these

177
00:16:49,920 --> 00:16:53,000
 six doors.

178
00:16:53,000 --> 00:17:00,850
 And if you get caught up in any one of them, it leads to

179
00:17:00,850 --> 00:17:02,000
 bad habits.

180
00:17:02,000 --> 00:17:06,360
 To put it simply, because it becomes habitual and it gets

181
00:17:06,360 --> 00:17:10,000
 you caught up in it, gets you off track,

182
00:17:10,000 --> 00:17:14,160
 gets you caught up in some kind of cycle of addiction or

183
00:17:14,160 --> 00:17:21,000
 aversion or conceit or views or whatever.

184
00:17:21,000 --> 00:17:27,000
 And leads to suffering, leads to stress.

185
00:17:27,000 --> 00:17:32,000
 Because you don't see things clearly biased.

186
00:17:32,000 --> 00:17:38,190
 And your biases create embarrassment and create stress and

187
00:17:38,190 --> 00:17:50,000
 create conflict, create disappointment and expectations.

188
00:17:50,000 --> 00:17:55,240
 So the next verses are all about yourself because that's

189
00:17:55,240 --> 00:17:57,000
 where we focus.

190
00:17:57,000 --> 00:18:01,000
 A river is only as pure as its source.

191
00:18:01,000 --> 00:18:04,660
 All of our life and our interactions with the world around

192
00:18:04,660 --> 00:18:07,000
 us, it's all dependent on our minds.

193
00:18:07,000 --> 00:18:11,550
 Your state of mind is not pure, you can't expect to help

194
00:18:11,550 --> 00:18:16,630
 others or have a good influence on others or have a good

195
00:18:16,630 --> 00:18:20,000
 relationship with others.

196
00:18:20,000 --> 00:18:23,350
 Work on yourself, it's the best thing you can do and in

197
00:18:23,350 --> 00:18:26,000
 fact it's the only thing you need to do.

198
00:18:26,000 --> 00:18:28,000
 Everything else comes naturally.

199
00:18:28,000 --> 00:18:32,000
 Once your mind is pure, the more your mind is pure, the

200
00:18:32,000 --> 00:18:36,290
 better your relationship with others, your influence on

201
00:18:36,290 --> 00:18:42,000
 others, your impact on the world.

202
00:18:42,000 --> 00:18:45,680
 And that's why we meditate, that's why we work on ourselves

203
00:18:45,680 --> 00:18:48,000
, that's why we study ourselves.

204
00:18:48,000 --> 00:18:53,000
 That's why we try to free ourselves from suffering.

205
00:18:53,000 --> 00:18:58,290
 You can't help others if you're in pain and stressed about

206
00:18:58,290 --> 00:19:00,000
 your own problems.

207
00:19:00,000 --> 00:19:05,790
 But a person who's truly happy, they are a benefit to the

208
00:19:05,790 --> 00:19:11,000
 world and to everyone they come in contact with.

209
00:19:11,000 --> 00:19:19,000
 Alright, so that's the quote for tonight.

210
00:19:19,000 --> 00:19:22,850
 I'm going to post the hangout if anyone has questions, you

211
00:19:22,850 --> 00:19:28,000
're welcome to click on the link and come ask.

212
00:19:28,000 --> 00:19:42,080
 I'll hang around for a few minutes to see if anyone has

213
00:19:42,080 --> 00:19:46,000
 questions.

214
00:19:46,000 --> 00:20:00,890
 This is the second broadcast today, today was second life

215
00:20:00,890 --> 00:20:11,000
 day so I broadcasted in virtual reality.

216
00:20:11,000 --> 00:20:19,030
 Only 20 people can listen to a second life talk, it's

217
00:20:19,030 --> 00:20:25,000
 actually a limit placed by the company.

218
00:20:25,000 --> 00:20:29,950
 It's quite expensive, second life is quite expensive for

219
00:20:29,950 --> 00:20:34,000
 people who have land like the Buddha Center.

220
00:20:34,000 --> 00:20:41,000
 Steven you're back but you're still... hello?

221
00:20:41,000 --> 00:20:46,000
 Yep, I hear you.

222
00:20:46,000 --> 00:20:52,000
 Do you have a question for me?

223
00:20:52,000 --> 00:21:10,000
 No, I don't have a question.

224
00:21:10,000 --> 00:21:29,000
 I just want to say a comment if you think it's proper.

225
00:21:29,000 --> 00:21:36,830
 Just because you say something about how people feel about

226
00:21:36,830 --> 00:21:42,720
 meditation, that they think sometimes that we are kind of

227
00:21:42,720 --> 00:21:45,000
 wasting our time.

228
00:21:45,000 --> 00:21:51,170
 I used to have a mental illness, I had depression and

229
00:21:51,170 --> 00:21:57,000
 anxiety, severe depression and anxiety.

230
00:21:57,000 --> 00:22:05,640
 And naturally I went through a paranoia state for one and a

231
00:22:05,640 --> 00:22:08,000
 half months.

232
00:22:08,000 --> 00:22:15,920
 At that time all my people, all my family took out and

233
00:22:15,920 --> 00:22:18,000
 supported me.

234
00:22:18,000 --> 00:22:30,160
 But after the crisis passed, they think that it wasn't

235
00:22:30,160 --> 00:22:35,000
 necessary to do more things about it.

236
00:22:35,000 --> 00:22:50,000
 Even though I didn't want to leave taking pills.

237
00:22:50,000 --> 00:23:02,040
 So I decided not to take pills anymore and start meditating

238
00:23:02,040 --> 00:23:05,000
 more seriously.

239
00:23:05,000 --> 00:23:15,610
 And I think that sometimes we don't should pay attention on

240
00:23:15,610 --> 00:23:22,020
 what other people said about leaving our jobs and stop

241
00:23:22,020 --> 00:23:24,000
 taking meditamins.

242
00:23:24,000 --> 00:23:31,670
 And if we feel that meditating is going to help us, we

243
00:23:31,670 --> 00:23:35,000
 should just do that.

244
00:23:35,000 --> 00:23:44,000
 That's how I make a comment.

245
00:23:44,000 --> 00:23:48,000
 Well, thank you.

246
00:23:48,000 --> 00:23:52,000
 Hey, you're smoking? Who is this guy?

247
00:23:52,000 --> 00:23:54,000
 I'm not convinced.

248
00:23:54,000 --> 00:23:57,110
 Stephen, can you tell us a little bit about yourself? Why

249
00:23:57,110 --> 00:24:02,000
 are you here?

250
00:24:02,000 --> 00:24:05,870
 I don't know who this guy is, but I think smoking on our

251
00:24:05,870 --> 00:24:08,000
 broadcast is not allowed.

252
00:24:08,000 --> 00:24:17,000
 Apologies.

253
00:24:17,000 --> 00:24:23,000
 Should we have rules like that? Is that appropriate? Well,

254
00:24:23,000 --> 00:24:23,000
 yes. I mean, that's not very respectful.

255
00:24:23,000 --> 00:24:28,000
 You have to stop smoking if you want to join our broadcast.

256
00:24:28,000 --> 00:24:35,000
 No smoking, no eating, that kind of thing.

257
00:24:35,000 --> 00:24:44,000
 Do you have a question, sir?

258
00:24:44,000 --> 00:24:48,000
 The reason I stick around is in case people have questions.

259
00:24:48,000 --> 00:24:53,000
 If nobody has questions, I'm going to end the broadcast.

260
00:24:53,000 --> 00:24:56,000
 Thank you. Thank you all. Have a good night.

261
00:24:56,000 --> 00:25:17,000
 In there.

