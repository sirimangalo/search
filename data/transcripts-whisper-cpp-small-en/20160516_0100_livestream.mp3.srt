1
00:00:00,000 --> 00:00:10,640
 Hello, good evening everyone.

2
00:00:10,640 --> 00:00:23,640
 Broadcasting live.

3
00:00:23,640 --> 00:00:24,640
 May 15th.

4
00:00:24,640 --> 00:00:44,000
 Sorry, broadcasting live.

5
00:00:44,000 --> 00:01:12,830
 Today's quote is from the commentary to the Kudaka Pada,

6
00:01:12,830 --> 00:01:13,820
 the first book of the Kudaka

7
00:01:13,820 --> 00:01:17,520
 Nikaiah.

8
00:01:17,520 --> 00:01:20,920
 This is the commentary to the Saranagamanaka Ta.

9
00:01:20,920 --> 00:01:23,920
 Is that what it's called?

10
00:01:23,920 --> 00:01:24,920
 Saranataiah.

11
00:01:24,920 --> 00:01:27,920
 It's called the Saranataiah.

12
00:01:27,920 --> 00:01:31,280
 What's it actually called?

13
00:01:31,280 --> 00:01:32,360
 Saranatayam.

14
00:01:32,360 --> 00:01:42,920
 The Kudaka Pada is, Kudaka means small.

15
00:01:42,920 --> 00:01:51,330
 Short could mean minor or kind of secondary, insignificant

16
00:01:51,330 --> 00:01:53,200
 in a sense.

17
00:01:53,200 --> 00:01:56,920
 Might mean miscellaneous.

18
00:01:56,920 --> 00:02:05,560
 But short is appropriate because the Saranatayam, Saranatay

19
00:02:05,560 --> 00:02:08,440
am is where we find buddhang, Saranangacang,

20
00:02:08,440 --> 00:02:13,280
 dhammang, Saranangacang, sanghang, Saranangacang.

21
00:02:13,280 --> 00:02:19,900
 I go to the Buddha as my Saranat, as a refuge or as a

22
00:02:19,900 --> 00:02:24,600
 reflection, a recollection.

23
00:02:24,600 --> 00:02:26,600
 Refuge may be better.

24
00:02:26,600 --> 00:02:28,600
 Dhammang, Saranangacang.

25
00:02:28,600 --> 00:02:33,040
 I go to the Dhamma, I go to the Sangha for refuge.

26
00:02:33,040 --> 00:02:40,040
 Dhrtyampi, buddhang, Saranangacang. For a second time I go

27
00:02:40,040 --> 00:02:41,320
 to the Buddha as a refuge.

28
00:02:41,320 --> 00:02:43,320
 I go to the Dhamma as a refuge.

29
00:02:43,320 --> 00:02:45,320
 I go to the Sangha as a refuge.

30
00:02:45,320 --> 00:02:49,320
 Dhrtyampi for a third time.

31
00:02:49,320 --> 00:02:51,320
 And that's it.

32
00:02:51,320 --> 00:02:53,320
 Nine lines.

33
00:03:02,320 --> 00:03:04,320
 Thirty-three words.

34
00:03:04,320 --> 00:03:08,320
 Thirty-three words long.

35
00:03:08,320 --> 00:03:12,320
 It's a small section. That's it.

36
00:03:12,320 --> 00:03:20,320
 Commentary has some interesting things to say about this.

37
00:03:20,320 --> 00:03:28,320
 First it talks about the Buddha, but we'll skip that part.

38
00:03:28,320 --> 00:03:31,320
 Only because it's not in our quote.

39
00:03:31,320 --> 00:03:33,320
 It's from the second part.

40
00:03:33,320 --> 00:03:40,320
 It talks about the three refuges as a whole.

41
00:03:40,320 --> 00:03:46,320
 And so there's some other similes here that hopefully I can

42
00:03:46,320 --> 00:03:46,320
...

43
00:03:46,320 --> 00:03:49,320
 I don't think this is in it.

44
00:03:49,320 --> 00:03:53,320
 Uh-oh.

45
00:03:53,320 --> 00:03:55,320
 I find it.

46
00:03:55,320 --> 00:04:00,320
 Right. It's actually quite long. Here we are.

47
00:04:02,320 --> 00:04:09,320
 Puna-chan-do-viyabuddho. The Buddha is like the full moon.

48
00:04:09,320 --> 00:04:13,320
 Puna-chan-do-viyabuddho.

49
00:04:13,320 --> 00:04:20,320
 Chanda-kirana-nikaro-viyate-nadesito-dhammo.

50
00:04:20,320 --> 00:04:26,320
 The Dhamma taught by him is like the rays of the moon.

51
00:04:28,320 --> 00:04:35,320
 The many rays of light that shine from the moon.

52
00:04:35,320 --> 00:04:46,320
 Puna-chan-do-kirana-sammo-padita-pinnito-loko-viyasango.

53
00:04:46,320 --> 00:04:53,320
 The world loka that is delighted,

54
00:04:53,320 --> 00:04:59,320
 we'll see, pleased, that is lit up maybe,

55
00:04:59,320 --> 00:05:09,320
 by the arisen, by the rays of the moon.

56
00:05:09,320 --> 00:05:15,590
 So the Dhamma lights up the world. Lights up is probably

57
00:05:15,590 --> 00:05:16,320
 the best.

58
00:05:16,320 --> 00:05:19,320
 This is the Sangha.

59
00:05:19,320 --> 00:05:25,320
 So the Sangha are those of us that are lit up.

60
00:05:25,320 --> 00:05:32,320
 The light comes to you, a-loko-dvadi.

61
00:05:32,320 --> 00:05:37,000
 It's like you were in a dark room before and suddenly there

62
00:05:37,000 --> 00:05:37,320
 is light.

63
00:05:37,320 --> 00:05:43,580
 The Dhamma opens your eyes to things that were there

64
00:05:43,580 --> 00:05:44,320
 already.

65
00:05:45,320 --> 00:05:50,320
 It doesn't bring anything new, not chiefly.

66
00:05:50,320 --> 00:05:55,320
 Chiefly it teaches you about what's already there.

67
00:05:55,320 --> 00:05:59,320
 That's the whole of it.

68
00:05:59,320 --> 00:06:07,320
 They joke those people who practice breath meditation,

69
00:06:07,320 --> 00:06:11,320
 they say it teaches you what's under your nose.

70
00:06:11,320 --> 00:06:15,580
 Because of course it uses the breath, they focus on the

71
00:06:15,580 --> 00:06:16,320
 nose.

72
00:06:16,320 --> 00:06:20,780
 But really it's here, the Buddha said, in this six-foot

73
00:06:20,780 --> 00:06:21,320
 frame,

74
00:06:21,320 --> 00:06:25,280
 this is where you find the beginning and the end of the

75
00:06:25,280 --> 00:06:26,320
 universe.

76
00:06:26,320 --> 00:06:31,320
 Everything is already here, you don't have to go far.

77
00:06:31,320 --> 00:06:37,320
 There's this monk in Bangkok who,

78
00:06:38,320 --> 00:06:41,790
 he would give an example of how we really don't know

79
00:06:41,790 --> 00:06:43,320
 ourselves very well.

80
00:06:43,320 --> 00:06:48,320
 He asked this woman, she was 95 years old,

81
00:06:48,320 --> 00:06:51,320
 he said, "How many knuckles do you have on one hand?"

82
00:06:51,320 --> 00:06:54,320
 He said, "Don't count, just tell me. Do you know?"

83
00:06:54,320 --> 00:06:56,320
 He said, "I don't know."

84
00:06:56,320 --> 00:06:59,320
 "95 years old, you still don't know."

85
00:06:59,320 --> 00:07:03,320
 "If you never counted, you don't know."

86
00:07:03,320 --> 00:07:07,680
 So when you say you know something like the back of your

87
00:07:07,680 --> 00:07:08,320
 hand, not so sure.

88
00:07:08,320 --> 00:07:11,320
 But you never look.

89
00:07:11,320 --> 00:07:13,510
 So you can look at your hand, that's not very difficult to

90
00:07:13,510 --> 00:07:15,320
 figure out how many knuckles you have.

91
00:07:15,320 --> 00:07:18,730
 But to understand your mind, how many people actually spend

92
00:07:18,730 --> 00:07:23,320
 the time learning about themselves, about their mind?

93
00:07:29,320 --> 00:07:34,320
 So this is the light, like the light of the moon.

94
00:07:34,320 --> 00:07:38,320
 The Buddha is like the moon.

95
00:07:38,320 --> 00:07:42,320
 That's the first one. The second one is

96
00:07:42,320 --> 00:07:47,320
 bhala.

97
00:07:47,320 --> 00:07:50,320
 Bala is not foolish.

98
00:07:50,320 --> 00:07:56,710
 I've got a problem here because it shouldn't be bhala, it

99
00:07:56,710 --> 00:07:58,320
 should be,

100
00:07:58,320 --> 00:08:00,320
 let me see.

101
00:08:00,320 --> 00:08:06,320
 Oh, balasurya, the newly arisen sun, I get it.

102
00:08:06,320 --> 00:08:09,320
 Bala means young in this case. It usually means foolish.

103
00:08:09,320 --> 00:08:13,320
 Balasurya is the dawn.

104
00:08:13,320 --> 00:08:17,320
 The Buddha is like the dawn.

105
00:08:17,320 --> 00:08:24,320
 dasa rasmin jalami vata pakarodamu

106
00:08:24,320 --> 00:08:30,320
 See if I can get this.

107
00:08:30,320 --> 00:08:40,320
 rasmi, got that one, jala.

108
00:08:40,320 --> 00:08:48,320
 The glow, the rest, the glow and the

109
00:08:51,320 --> 00:08:56,410
 dhamma with its many qualities is like the glow of the sun,

110
00:08:56,410 --> 00:08:59,320
 I don't know, it's like the qualities of the risen sun.

111
00:08:59,320 --> 00:09:01,320
 Same thing.

112
00:09:01,320 --> 00:09:06,320
 vutapakaro, not clear about that.

113
00:09:06,320 --> 00:09:09,320
 dhamma, my body is not great.

114
00:09:09,320 --> 00:09:17,320
 Dhena vihatanda karo lokowiya sango

115
00:09:19,320 --> 00:09:26,500
 The darkness, anda kara is, anda means blind, kara means

116
00:09:26,500 --> 00:09:27,320
 that which makes,

117
00:09:27,320 --> 00:09:32,320
 that which makes you blind, anda kara is darkness.

118
00:09:32,320 --> 00:09:38,320
 vihata means destroyed or dispelled.

119
00:09:38,320 --> 00:09:47,110
 The world whose darkness is destroyed by the sun is like

120
00:09:47,110 --> 00:09:48,320
 the sangha.

121
00:09:48,320 --> 00:09:51,600
 So again, this light where the Buddha is like the moon, he

122
00:09:51,600 --> 00:09:53,320
 is like the sun.

123
00:09:53,320 --> 00:09:58,320
 When it lights up the world.

124
00:09:58,320 --> 00:10:02,670
 Because that's it, you know Buddhism isn't about, I had an

125
00:10:02,670 --> 00:10:06,010
 interesting conversation today, I went to see my family

126
00:10:06,010 --> 00:10:06,320
 today.

127
00:10:06,320 --> 00:10:10,900
 We talked about music, and it was a good debate, and I had

128
00:10:10,900 --> 00:10:13,320
 to admit some points because

129
00:10:13,320 --> 00:10:18,880
 there's definitely a potential to see good in music, in the

130
00:10:18,880 --> 00:10:21,320
 sense, in the worldly sense that

131
00:10:21,320 --> 00:10:27,640
 it can strengthen you, the rhythm, the trance that it puts

132
00:10:27,640 --> 00:10:30,320
 you in can strengthen you.

133
00:10:30,320 --> 00:10:34,870
 So what I couldn't probably quite get across as well as I

134
00:10:34,870 --> 00:10:36,320
 would have liked,

135
00:10:36,320 --> 00:10:40,720
 it was about five or six on one, so I was vastly

136
00:10:40,720 --> 00:10:44,320
 outnumbered, my family's into music.

137
00:10:44,320 --> 00:10:50,150
 Is that the sensual, the sensual attachment? I mean we didn

138
00:10:50,150 --> 00:10:51,320
't talk about it but,

139
00:10:51,320 --> 00:10:55,320
 it's a tough crowd, not very convinced.

140
00:10:55,320 --> 00:11:01,320
 Is the attachment to it, you know, in the mind?

141
00:11:03,320 --> 00:11:07,450
 It can be attached, attached to the sound, you like it, and

142
00:11:07,450 --> 00:11:08,320
 liking is an addiction.

143
00:11:08,320 --> 00:11:11,590
 And now there's so many things that you have, there's a

144
00:11:11,590 --> 00:11:13,320
 worldview that you have to,

145
00:11:13,320 --> 00:11:16,320
 I guess, a change in worldview.

146
00:11:16,320 --> 00:11:19,810
 But the reason I bring it up is because it's not about

147
00:11:19,810 --> 00:11:20,320
 dogma.

148
00:11:20,320 --> 00:11:24,320
 We can't just say music is bad, we have to look at reality.

149
00:11:24,320 --> 00:11:27,320
 We're not about, we can't be.

150
00:11:27,320 --> 00:11:33,320
 If we want to follow the Buddha, we can't accept dogma,

151
00:11:33,320 --> 00:11:38,320
 we can't follow dogma, we can't believe things.

152
00:11:38,320 --> 00:11:42,320
 We can't follow things just because it's a belief.

153
00:11:42,320 --> 00:11:44,320
 We follow things because they're true.

154
00:11:44,320 --> 00:11:48,320
 If they are not true, we have to stop following them.

155
00:11:48,320 --> 00:11:52,320
 There's a challenge there, you know.

156
00:11:52,320 --> 00:11:55,320
 What can we know to be true?

157
00:11:55,320 --> 00:12:01,320
 And so it's not, I mean, it's not about the whole truth.

158
00:12:01,320 --> 00:12:08,010
 It's about two things, the good truth, the truth that is

159
00:12:08,010 --> 00:12:08,320
 useful,

160
00:12:08,320 --> 00:12:12,320
 but also the truth that we can know.

161
00:12:12,320 --> 00:12:18,320
 So any truth that we are going to find has to be normal,

162
00:12:18,320 --> 00:12:20,320
 and we have to come to know it.

163
00:12:20,320 --> 00:12:24,320
 That's how we become a true follower of the Buddha.

164
00:12:24,320 --> 00:12:27,320
 We do practice his teachings, we don't believe it,

165
00:12:27,320 --> 00:12:30,320
 we don't think it, we don't suspect it.

166
00:12:30,320 --> 00:12:32,320
 We realize it. It's a very simple thing.

167
00:12:32,320 --> 00:12:34,320
 So we study music, for example.

168
00:12:34,320 --> 00:12:39,120
 And you can say music is good, music is bad, music makes me

169
00:12:39,120 --> 00:12:40,320
 happy.

170
00:12:40,320 --> 00:12:44,320
 My main argument was that happiness doesn't make you happy.

171
00:12:44,320 --> 00:12:47,320
 This is a very good Buddhist argument.

172
00:12:47,320 --> 00:12:51,320
 It didn't really work because, well, when you're so opposed

173
00:12:51,320 --> 00:12:52,320
 to something,

174
00:12:52,320 --> 00:12:55,320
 your view is very much opposed to...

175
00:12:55,320 --> 00:13:00,820
 So we have different views, but you can argue, you can say

176
00:13:00,820 --> 00:13:02,320
 happiness makes you happy.

177
00:13:02,320 --> 00:13:04,870
 Happiness doesn't lead to happiness, it's not the cause of

178
00:13:04,870 --> 00:13:05,320
 happiness.

179
00:13:05,320 --> 00:13:08,650
 It's not the cause of itself. If it was, I mean, actually

180
00:13:08,650 --> 00:13:09,320
 it's kind of silly,

181
00:13:09,320 --> 00:13:12,300
 because if it was, it would be a feedback loop, we'd always

182
00:13:12,300 --> 00:13:13,320
 be happy.

183
00:13:13,320 --> 00:13:16,320
 Be happy once, you're happy forever. It's not the case.

184
00:13:16,320 --> 00:13:18,320
 Happiness doesn't lead to happiness.

185
00:13:18,320 --> 00:13:21,010
 I suppose it's a bit simplistic, and people would argue

186
00:13:21,010 --> 00:13:23,320
 that it's more complicated than that,

187
00:13:23,320 --> 00:13:26,350
 but happiness doesn't lead to happiness. It doesn't have

188
00:13:26,350 --> 00:13:27,320
 that power.

189
00:13:27,320 --> 00:13:29,320
 Something leads to happiness.

190
00:13:29,320 --> 00:13:32,600
 We'd argue that things, goodness in all its forms, things

191
00:13:32,600 --> 00:13:33,320
 that we...

192
00:13:33,320 --> 00:13:35,320
 Well, I mean, that's a tautology.

193
00:13:35,320 --> 00:13:38,320
 We call it goodness because it leads to happiness.

194
00:13:38,320 --> 00:13:41,320
 It leads to happiness because it's goodness, not really.

195
00:13:41,320 --> 00:13:44,320
 There are certain things that lead to happiness.

196
00:13:44,320 --> 00:13:47,320
 Happiness is not one of them.

197
00:13:48,320 --> 00:13:52,160
 But that's a good example because that's a claim that we

198
00:13:52,160 --> 00:13:53,320
 can test.

199
00:13:53,320 --> 00:13:56,230
 Does happiness lead to happiness? Most people will argue

200
00:13:56,230 --> 00:13:57,320
 things like that.

201
00:13:57,320 --> 00:13:59,320
 Of course, happiness is good in and of itself.

202
00:13:59,320 --> 00:14:03,320
 They'd argue, no, but let's investigate.

203
00:14:03,320 --> 00:14:07,010
 It's a problem, really. People have views without

204
00:14:07,010 --> 00:14:08,320
 investigating,

205
00:14:08,320 --> 00:14:12,280
 or they have views based on their own experience, anecdotal

206
00:14:12,280 --> 00:14:13,320
 experience,

207
00:14:13,320 --> 00:14:17,000
 without being systematic in their exploration, their

208
00:14:17,000 --> 00:14:18,320
 investigation.

209
00:14:18,320 --> 00:14:24,940
 Buddhism is about bringing light. It's not about changing

210
00:14:24,940 --> 00:14:25,320
 anything.

211
00:14:25,320 --> 00:14:29,320
 It's about bringing the purest light possible that we can,

212
00:14:29,320 --> 00:14:32,320
 the purest light we're able to.

213
00:14:32,320 --> 00:14:36,120
 It's making ourselves as objective as we can, to see things

214
00:14:36,120 --> 00:14:38,320
 as clearly as we can,

215
00:14:38,320 --> 00:14:40,320
 and just accepting the truth.

216
00:14:40,320 --> 00:14:43,470
 In fact, it's not something you even have to accept once

217
00:14:43,470 --> 00:14:44,320
 you see it.

218
00:14:44,320 --> 00:14:49,320
 It changes you. It changes your whole perspective.

219
00:14:49,320 --> 00:14:53,320
 You can't unsee it. You can't unknow it.

220
00:14:53,320 --> 00:14:56,320
 Not that you would ever want to, right?

221
00:14:56,320 --> 00:14:59,090
 I mean, what's better in a dark room? What's better in a

222
00:14:59,090 --> 00:14:59,320
 room?

223
00:14:59,320 --> 00:15:02,870
 To know where everything is or to not know where everything

224
00:15:02,870 --> 00:15:03,320
 is.

225
00:15:03,320 --> 00:15:06,460
 You want to forget where the chairs are so you bump into

226
00:15:06,460 --> 00:15:07,320
 them again?

227
00:15:07,320 --> 00:15:10,320
 What's better that you know?

228
00:15:10,320 --> 00:15:16,320
 "Wana dahaka puri so viyam budo"

229
00:15:16,320 --> 00:15:20,320
 "Wana dahaka" I think "dahaka" is... look at...

230
00:15:20,320 --> 00:15:24,320
 I don't know "dahaka". It must be like leader.

231
00:15:24,320 --> 00:15:30,320
 I don't think we have dahaka.

232
00:15:33,320 --> 00:15:37,320
 Somebody leads you out of a forest, I think.

233
00:15:37,320 --> 00:15:39,320
 "Dahana"

234
00:15:39,320 --> 00:15:41,320
 "Dahana"

235
00:15:41,320 --> 00:15:43,320
 Burning? Oh.

236
00:15:43,320 --> 00:15:49,220
 Buddha is like someone who burns down the forest? I'm not

237
00:15:49,220 --> 00:15:50,320
 sure.

238
00:15:50,320 --> 00:15:55,320
 "Knei sa wana dahana" Okay.

239
00:15:55,320 --> 00:15:57,320
 Buddha is one who burns down a forest.

240
00:15:57,320 --> 00:16:01,320
 He's a person who burns down a forest, I think.

241
00:16:02,320 --> 00:16:06,320
 The fire burning the forest is the dhamma.

242
00:16:06,320 --> 00:16:13,320
 "Wana dahana gi viya kilesa wana dahana o dhamma"

243
00:16:13,320 --> 00:16:21,320
 The fire burning the forest, which is the...

244
00:16:26,320 --> 00:16:32,320
 Like the fire burning the forest is the dhamma that burns

245
00:16:32,320 --> 00:16:36,320
 the forest of kilesa, the forest of defilements.

246
00:16:36,320 --> 00:16:48,320
 "Dahana dahana keta budo viya bhumi bhago"

247
00:16:48,320 --> 00:16:50,320
 No, I'm not going to get this one.

248
00:16:50,320 --> 00:16:57,320
 "Dahana kilesa tah punya keta budo san go"

249
00:16:57,320 --> 00:17:00,320
 I think it means the field.

250
00:17:00,320 --> 00:17:10,320
 Once the state of the forest being the thing,

251
00:17:13,320 --> 00:17:23,230
 "Bhumi bhago" That portion of earth that has become a field

252
00:17:23,230 --> 00:17:23,320
,

253
00:17:23,320 --> 00:17:30,950
 through the burning of the forest, through the forest

254
00:17:30,950 --> 00:17:32,320
 having been burned.

255
00:17:32,320 --> 00:17:37,320
 Once the forest is consumed by fire, you have a field.

256
00:17:37,320 --> 00:17:43,320
 It's not the best imagery. We tend to like our forests. And

257
00:17:43,320 --> 00:17:43,320
 the Buddha liked forests as well.

258
00:17:43,320 --> 00:17:48,330
 But the commentator here has chosen some fairly strong

259
00:17:48,330 --> 00:17:49,320
 imagery.

260
00:17:49,320 --> 00:17:54,940
 But it's clever. This is the sangha. The sangha is like

261
00:17:54,940 --> 00:17:55,320
 that field.

262
00:17:55,320 --> 00:17:58,870
 Once you've burnt down the forest, you can plant stuff in

263
00:17:58,870 --> 00:17:59,320
 it.

264
00:18:01,320 --> 00:18:07,570
 And so you have the, once the defilements are burnt down,

265
00:18:07,570 --> 00:18:12,320
 that person who has become a field of merit, punya keta.

266
00:18:12,320 --> 00:18:18,130
 They are an incomparable field of merit. This is the sangha

267
00:18:18,130 --> 00:18:18,320
.

268
00:18:18,320 --> 00:18:24,230
 They are incredible people to associate with, enlightened

269
00:18:24,230 --> 00:18:25,320
 beings.

270
00:18:25,320 --> 00:18:29,460
 You can become a better person. You can do good deeds

271
00:18:29,460 --> 00:18:32,320
 around them, like nothing else.

272
00:18:32,320 --> 00:18:36,320
 Even just giving them a gift.

273
00:18:36,320 --> 00:18:41,810
 Even just giving a gift to the Buddha or one of his chief

274
00:18:41,810 --> 00:18:44,320
 disciples or something.

275
00:18:44,320 --> 00:18:49,320
 Supporting anyone who practices meditation is awesome.

276
00:18:49,320 --> 00:18:53,320
 They are a great field of merit.

277
00:18:53,320 --> 00:18:55,700
 So he's not saying burn down forests or burning down

278
00:18:55,700 --> 00:18:57,320
 forests. It's a good thing.

279
00:18:57,320 --> 00:19:01,100
 But it's a curious, it's a clever imagery because we always

280
00:19:01,100 --> 00:19:04,320
 talk about the sangha as being a field of merit.

281
00:19:04,320 --> 00:19:10,320
 Mahamigoh viyabuddho salilah utti uyadhamo.

282
00:19:10,320 --> 00:19:14,320
 So the Buddha is like a great rain cloud.

283
00:19:14,320 --> 00:19:21,590
 Salilah utti, the rain, the water of the rain is like the d

284
00:19:21,590 --> 00:19:22,320
hamma.

285
00:19:23,320 --> 00:19:26,320
 So what good is water from rain?

286
00:19:26,320 --> 00:19:36,460
 Utini patupasamitareno viyajanapado upasamitakide saraino

287
00:19:36,460 --> 00:19:38,320
 sangho.

288
00:19:38,320 --> 00:19:41,320
 So what is rain?

289
00:19:41,320 --> 00:19:45,320
 Dust.

290
00:19:46,320 --> 00:19:53,860
 That removes the dust. Upasamitareno, which removes

291
00:19:53,860 --> 00:19:58,760
 basically or it actually appeases, but that's not right

292
00:19:58,760 --> 00:19:59,320
 here.

293
00:19:59,320 --> 00:20:08,010
 Upasamitareno, the dust is cleansed. That's just loosely

294
00:20:08,010 --> 00:20:09,320
 translated.

295
00:20:10,320 --> 00:20:16,410
 The dust that is removed through the falling of the rain on

296
00:20:16,410 --> 00:20:24,320
 the jandapado, on the countryside, is like the sangha.

297
00:20:24,320 --> 00:20:29,320
 Yes, it's like the sangha or the sangha is like that.

298
00:20:29,320 --> 00:20:39,470
 The sangha who have pacified the dust of defilements, so

299
00:20:39,470 --> 00:20:43,320
 they have cleansed the dust of defilements from their mind.

300
00:20:43,320 --> 00:20:49,320
 They have wiped it away from their minds.

301
00:20:49,320 --> 00:20:54,620
 The dust is the things that make our mind dirty, make our

302
00:20:54,620 --> 00:20:59,290
 minds muddled, muddied, in the sense of not being able to

303
00:20:59,290 --> 00:21:00,320
 see clearly.

304
00:21:00,320 --> 00:21:05,320
 Susar, no that's the third one, right, that's the sangha.

305
00:21:05,320 --> 00:21:09,130
 So the sangha who have cleansed their defilements using the

306
00:21:09,130 --> 00:21:14,910
 rain of the dhamma, thought by the rain cloud of the Buddha

307
00:21:14,910 --> 00:21:15,320
.

308
00:21:17,320 --> 00:21:21,320
 So this is cleansing. We haven't gotten to the doctor yet.

309
00:21:21,320 --> 00:21:24,320
 That's the quote, today's quote.

310
00:21:24,320 --> 00:21:32,320
 Susarati viyabuddho, sarati is a chariot here, isn't it?

311
00:21:32,320 --> 00:21:39,240
 Buddha is like a skilled driver, chauffeur, no not a chau

312
00:21:39,240 --> 00:21:42,320
ffeur, chariot here.

313
00:21:42,320 --> 00:21:49,320
 Asadjaniya uinai upayu viyadambhu.

314
00:21:49,320 --> 00:22:00,320
 Asadjaniya is a good horse that is trained and skillful.

315
00:22:03,320 --> 00:22:11,730
 No, the skill in training horses of good breeding is like

316
00:22:11,730 --> 00:22:13,320
 the dhamma.

317
00:22:13,320 --> 00:22:18,030
 So a charioteer has to know how to train horses. This is a

318
00:22:18,030 --> 00:22:20,320
 horse chariot, right?

319
00:22:20,320 --> 00:22:26,430
 Nowadays it would be the skill in driving a car, race car

320
00:22:26,430 --> 00:22:28,320
 driver maybe.

321
00:22:29,320 --> 00:22:35,320
 Subhinitasadjaniya samuho viyasangho.

322
00:22:35,320 --> 00:22:45,350
 The samuha is a multitude, the well-trained multitude of

323
00:22:45,350 --> 00:22:50,320
 horses is like the sangha, the herd of horses, right?

324
00:22:52,320 --> 00:22:58,650
 It's like the sangha. So Buddhism is a training, very

325
00:22:58,650 --> 00:23:00,320
 important. Buddhist meditation isn't about blissing out or

326
00:23:00,320 --> 00:23:00,320
 enjoying yourself.

327
00:23:00,320 --> 00:23:04,770
 It's about training your mind, training your mind in

328
00:23:04,770 --> 00:23:07,320
 clarity and seeing things as they are.

329
00:23:07,320 --> 00:23:15,320
 Training your mind through clarity. The clarity trains you,

330
00:23:16,320 --> 00:23:19,880
 trains you to do and say and think the right things because

331
00:23:19,880 --> 00:23:24,310
 you know what's right and what's wrong for yourself without

332
00:23:24,310 --> 00:23:29,320
 any kind of brainwashing or dogma or anything.

333
00:23:29,320 --> 00:23:32,740
 I don't know if I should go on. It looks like there's tons

334
00:23:32,740 --> 00:23:35,600
 and tons and tons of these. This would be a good commentary

335
00:23:35,600 --> 00:23:36,320
 to translate.

336
00:23:36,320 --> 00:23:40,530
 I think I'll stop there though. The doctor one comes up. So

337
00:23:40,530 --> 00:23:42,320
 the quote tonight is,

338
00:23:42,320 --> 00:23:47,950
 "Can we skip ahead to the doctor?" I don't know if I can

339
00:23:47,950 --> 00:23:52,320
 even find it. Maybe it's coming up.

340
00:23:52,320 --> 00:23:57,320
 No.

341
00:23:57,320 --> 00:24:05,920
 There's tons. There's like 50 or 60 different, well 40, 20,

342
00:24:05,920 --> 00:24:10,320
 30, 40. There's lots of them. I think we'll end it there.

343
00:24:10,320 --> 00:24:14,790
 So the quote was, "The Buddha is a skilled physician, like

344
00:24:14,790 --> 00:24:20,040
 a physician who is able to heal the sickness, so he heals

345
00:24:20,040 --> 00:24:22,320
 the sickness of defilements."

346
00:24:22,320 --> 00:24:28,610
 The Dhamma is like the medicine, rightly applied. The San

347
00:24:28,610 --> 00:24:33,050
gha, with their defilements cured, are like people restored

348
00:24:33,050 --> 00:24:36,320
 to health by that medicine.

349
00:24:36,320 --> 00:24:46,770
 Rightly applied, no? Even like medicine, the Dhamma has to

350
00:24:46,770 --> 00:24:48,320
 be rightly applied.

351
00:24:48,320 --> 00:24:53,320
 You can't just read the Dhamma and decide to practice it.

352
00:24:53,320 --> 00:24:57,860
 It's important to have the direction, guidance, support of

353
00:24:57,860 --> 00:25:00,320
 these things.

354
00:25:05,320 --> 00:25:08,860
 So, if you have any important questions, it's been a long

355
00:25:08,860 --> 00:25:12,200
 day, so I'm not going to stay here too long, but if you've

356
00:25:12,200 --> 00:25:15,320
 got some important meditative questions.

357
00:25:33,320 --> 00:25:36,360
 I'm just trying to explain music. No, the only thing I

358
00:25:36,360 --> 00:25:39,180
 wanted to say about music is that in a worldly sense, it

359
00:25:39,180 --> 00:25:40,320
 could be healthy.

360
00:25:40,320 --> 00:25:43,490
 They say, "Well, music is good for you. It actually leads

361
00:25:43,490 --> 00:25:46,890
 to health." They were arguing that there are cases of

362
00:25:46,890 --> 00:25:49,320
 people being cured through music.

363
00:25:49,320 --> 00:25:52,800
 So, in a worldly sense, I could see how the rhythm would be

364
00:25:52,800 --> 00:25:56,180
 healthy for the body, and even for the mind, rhythm puts

365
00:25:56,180 --> 00:26:01,320
 you in a trance, and so you're in a blissful space.

366
00:26:08,320 --> 00:26:11,680
 Whether that's right or wrong, good or bad, it's a lot of

367
00:26:11,680 --> 00:26:15,250
 delusion involved with good health, of course, but that's

368
00:26:15,250 --> 00:26:16,320
 not a big deal.

369
00:26:16,320 --> 00:26:20,480
 Worldly sense, okay, we agree that there are some states of

370
00:26:20,480 --> 00:26:24,550
 mind that are trance-like that can be brought about through

371
00:26:24,550 --> 00:26:25,320
 music.

372
00:26:25,320 --> 00:26:29,650
 The bigger problem is the sensuality, just the enjoyment of

373
00:26:29,650 --> 00:26:33,450
 the sound that becomes addictive, and it's just a minor

374
00:26:33,450 --> 00:26:34,320
 addiction.

375
00:26:34,320 --> 00:26:38,320
 Nothing huge, it's not breaking the five precepts, really.

376
00:26:38,320 --> 00:26:44,320
 And then there's bad music, like music that teaches you how

377
00:26:44,320 --> 00:26:50,080
 to be a bad person, gangster rap, that kind of stuff, of

378
00:26:50,080 --> 00:26:51,320
 course.

379
00:26:51,320 --> 00:26:58,390
 But my brother's a musician. It was a difficult argument. I

380
00:26:58,390 --> 00:27:00,320
 want to get into it too much.

381
00:27:02,320 --> 00:27:05,890
 Okay, I see that there are no pertinent questions, if

382
00:27:05,890 --> 00:27:09,370
 anyone had pressing questions, if anyone had pressing

383
00:27:09,370 --> 00:27:11,320
 questions, I would...

384
00:27:11,320 --> 00:27:14,290
 I think they would have been posted by now, so I'm going to

385
00:27:14,290 --> 00:27:15,320
 say goodnight.

386
00:27:15,320 --> 00:27:18,630
 Tomorrow morning I'm off to New York with two other monks

387
00:27:18,630 --> 00:27:26,470
 and Michael's our driver, so probably nothing tomorrow, but

388
00:27:26,470 --> 00:27:27,320
 actually maybe.

389
00:27:28,320 --> 00:27:31,180
 If I can get WiFi in this monastery, in Bikubodi's

390
00:27:31,180 --> 00:27:37,320
 monastery, I'll try and broadcast something, we'll see.

391
00:27:37,320 --> 00:27:40,320
 Anyway, goodnight.

392
00:27:40,320 --> 00:27:51,320
 [Silence]

