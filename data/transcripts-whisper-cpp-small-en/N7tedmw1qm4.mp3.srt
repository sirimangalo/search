1
00:00:00,000 --> 00:00:04,890
 When you thought of being a monk, were you asking yourself

2
00:00:04,890 --> 00:00:06,520
 too much questions like us

3
00:00:06,520 --> 00:00:07,520
 here?

4
00:00:07,520 --> 00:00:13,540
 Do you think we are relying too much on the idealization

5
00:00:13,540 --> 00:00:16,640
 level of consciousness?

6
00:00:16,640 --> 00:00:21,320
 Yeah, too many questions.

7
00:00:21,320 --> 00:00:24,220
 Well some questions are a little bit much, but most of the

8
00:00:24,220 --> 00:00:27,880
 questions seem to be really

9
00:00:27,880 --> 00:00:29,920
 well thought out and important to people.

10
00:00:29,920 --> 00:00:32,750
 I think a lot of the questions here are actually important

11
00:00:32,750 --> 00:00:33,440
 to people.

12
00:00:33,440 --> 00:00:36,240
 You guys deal with a lot more than I do.

13
00:00:36,240 --> 00:00:39,390
 You have a lot more factors to deal with and it makes

14
00:00:39,390 --> 00:00:41,280
 things more complicated.

15
00:00:41,280 --> 00:00:45,870
 It's a lot easier for someone on the outside to help you

16
00:00:45,870 --> 00:00:47,360
 out with that.

17
00:00:47,360 --> 00:00:50,560
 A lot of the helping out is just to bring you back to a

18
00:00:50,560 --> 00:00:52,480
 more simple, pull you into a

19
00:00:52,480 --> 00:00:57,220
 more simple life, help you to rearrange things so that you

20
00:00:57,220 --> 00:00:59,520
 live a more simple life, so that

21
00:00:59,520 --> 00:01:03,770
 you think in a more simple way, that you live in a more

22
00:01:03,770 --> 00:01:06,320
 simple, you live your life more

23
00:01:06,320 --> 00:01:09,240
 simply.

24
00:01:09,240 --> 00:01:12,320
 But that's not the first question.

25
00:01:12,320 --> 00:01:14,360
 It's not either question really.

26
00:01:14,360 --> 00:01:20,030
 But asking too many questions, was I asking too many

27
00:01:20,030 --> 00:01:21,640
 questions?

28
00:01:21,640 --> 00:01:22,640
 Maybe I don't know.

29
00:01:22,640 --> 00:01:24,800
 I didn't have people to ask them.

30
00:01:24,800 --> 00:01:28,000
 I wasn't really asking a lot of questions, no.

31
00:01:28,000 --> 00:01:38,950
 But what I would say is that I was misled or I was totally

32
00:01:38,950 --> 00:01:41,560
 in the dark.

33
00:01:41,560 --> 00:01:42,880
 So I didn't know.

34
00:01:42,880 --> 00:01:45,790
 I think the problem here, and that's often a problem, is

35
00:01:45,790 --> 00:01:46,960
 people think they should ask

36
00:01:46,960 --> 00:01:47,960
 something.

37
00:01:47,960 --> 00:01:51,710
 So they come here and they think, "Well, okay, so this is

38
00:01:51,710 --> 00:01:52,640
 Buddhism.

39
00:01:52,640 --> 00:01:55,240
 You come and ask questions of the monk."

40
00:01:55,240 --> 00:01:57,600
 Which is a bit of a problem, no?

41
00:01:57,600 --> 00:01:59,350
 That's the negative side of this, is that people get the

42
00:01:59,350 --> 00:02:00,520
 idea that, "Well, this is their

43
00:02:00,520 --> 00:02:05,840
 Buddhist practice is to come and ask questions."

44
00:02:05,840 --> 00:02:09,380
 Because when before we've practiced meditation, we're in

45
00:02:09,380 --> 00:02:10,160
 the dark.

46
00:02:10,160 --> 00:02:12,200
 We don't really know what is the path.

47
00:02:12,200 --> 00:02:14,720
 I went to Thailand looking for wisdom.

48
00:02:14,720 --> 00:02:16,200
 And that's what I said with the meditation.

49
00:02:16,200 --> 00:02:18,800
 I said, "Why are you coming to meditate?"

50
00:02:18,800 --> 00:02:20,120
 I said, "I want to find wisdom."

51
00:02:20,120 --> 00:02:22,800
 And they said, "Oh, that's a good thing."

52
00:02:22,800 --> 00:02:25,360
 But it was absurd because I hadn't a clue what wisdom was.

53
00:02:25,360 --> 00:02:30,820
 And when I found or when I began to develop wisdom, it

54
00:02:30,820 --> 00:02:34,500
 turned out to be totally otherwise

55
00:02:34,500 --> 00:02:35,500
 than what I thought.

56
00:02:35,500 --> 00:02:40,470
 And it's a surprise when you develop, when you really gain

57
00:02:40,470 --> 00:02:41,440
 wisdom.

58
00:02:41,440 --> 00:02:42,680
 It totally shocks you.

59
00:02:42,680 --> 00:02:45,400
 It's totally other than what you think.

60
00:02:45,400 --> 00:02:50,400
 So asking questions here is incredibly limited.

61
00:02:50,400 --> 00:02:53,630
 It shouldn't ever be considered your practice or it shouldn

62
00:02:53,630 --> 00:02:55,280
't ever be considered to solve

63
00:02:55,280 --> 00:02:56,280
 things.

64
00:02:56,280 --> 00:03:01,750
 If anything, it should be, if I'm answering these questions

65
00:03:01,750 --> 00:03:04,560
 correctly, it should incline

66
00:03:04,560 --> 00:03:07,850
 you more towards meditation, incline you more to find the

67
00:03:07,850 --> 00:03:08,600
 answers.

68
00:03:08,600 --> 00:03:12,800
 I'm not answering, I'm not giving you the answers.

69
00:03:12,800 --> 00:03:19,330
 I'm trying and hopefully, I don't know if I'm succeeding,

70
00:03:19,330 --> 00:03:22,200
 but trying to encourage you

71
00:03:22,200 --> 00:03:26,920
 to find the answers, to look for the answers.

72
00:03:26,920 --> 00:03:29,420
 The best thing would be if people came away thinking, "I

73
00:03:29,420 --> 00:03:31,080
 want to go on a meditation course.

74
00:03:31,080 --> 00:03:34,800
 I want to go off and really dedicate myself to this."

75
00:03:34,800 --> 00:03:38,620
 This was the problem with meditation videos and even a

76
00:03:38,620 --> 00:03:40,840
 booklet on how to meditate.

77
00:03:40,840 --> 00:03:41,840
 Great.

78
00:03:41,840 --> 00:03:45,680
 It's a lot of people.

79
00:03:45,680 --> 00:03:49,460
 The one downside, and it's not a reason to stop, but the

80
00:03:49,460 --> 00:03:51,440
 problem is that people get the

81
00:03:51,440 --> 00:03:55,040
 idea that that's enough.

82
00:03:55,040 --> 00:03:56,760
 If they do say, "I'm going to go on a meditation course,"

83
00:03:56,760 --> 00:03:57,920
 they think it'll be just the same

84
00:03:57,920 --> 00:04:00,440
 as practicing meditation at home.

85
00:04:00,440 --> 00:04:05,710
 And then it just totally blows them away because it's on a

86
00:04:05,710 --> 00:04:08,480
 totally different level.

87
00:04:08,480 --> 00:04:14,190
 So yeah, you should never be content with this sort of an

88
00:04:14,190 --> 00:04:15,600
 activity.

89
00:04:15,600 --> 00:04:19,830
 This is just for people who are new, I would say, or people

90
00:04:19,830 --> 00:04:21,840
 who come back and have some

91
00:04:21,840 --> 00:04:25,130
 free time from their meditation and they want to hear the D

92
00:04:25,130 --> 00:04:26,840
hamma, listen to the Dhamma,

93
00:04:26,840 --> 00:04:31,080
 maybe even share the Dhamma themselves.

94
00:04:31,080 --> 00:04:35,580
 So I would say don't ask too many questions and don't rely

95
00:04:35,580 --> 00:04:37,840
 too much on the ideation level

96
00:04:37,840 --> 00:04:38,840
 of consciousness.

97
00:04:38,840 --> 00:04:41,710
 Whether people are or not, I get the feeling that people

98
00:04:41,710 --> 00:04:43,120
 are fairly sincere here.

99
00:04:43,120 --> 00:04:45,160
 So I hope that helps.

