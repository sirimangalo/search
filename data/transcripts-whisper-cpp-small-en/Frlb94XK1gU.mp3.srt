1
00:00:00,000 --> 00:00:02,000
 Guru.

2
00:00:02,000 --> 00:00:06,760
 Okay, so welcome everyone

3
00:00:06,760 --> 00:00:14,470
 to our live broadcast and real life meditation teachings

4
00:00:14,470 --> 00:00:15,960
 and practice.

5
00:00:15,960 --> 00:00:19,520
 Everybody get comfortable.

6
00:00:19,520 --> 00:00:23,160
 Remember we're taking this as an opportunity to learn about

7
00:00:23,160 --> 00:00:24,880
 ourselves.

8
00:00:26,360 --> 00:00:29,800
 The teaching of the Buddha is to learn something about

9
00:00:29,800 --> 00:00:33,880
 yourself. It's not philosophy or

10
00:00:33,880 --> 00:00:41,760
 theoretical science or so on.

11
00:00:41,760 --> 00:00:45,920
 It's practical. It's meant to help us.

12
00:00:45,920 --> 00:00:51,420
 So sit down, sit in meditation, close your eyes, don't look

13
00:00:51,420 --> 00:00:51,840
 at me.

14
00:00:54,800 --> 00:00:57,640
 I'm not going to help you. You have to help yourself.

15
00:00:57,640 --> 00:01:02,890
 Tonight I'd like to talk about the question, "What is

16
00:01:02,890 --> 00:01:04,400
 Buddhism?"

17
00:01:04,400 --> 00:01:16,680
 It's a question I don't get asked that often.

18
00:01:16,680 --> 00:01:21,520
 I think it's a rather useful question and it's one that we

19
00:01:21,520 --> 00:01:23,520
 seem

20
00:01:23,520 --> 00:01:31,310
 we're often perhaps confused about in the nature of

21
00:01:31,310 --> 00:01:32,200
 Buddhism.

22
00:01:32,200 --> 00:01:39,350
 And obviously there are going to be many answers to this

23
00:01:39,350 --> 00:01:39,960
 question.

24
00:01:39,960 --> 00:01:42,310
 I think that's probably why you don't get asked it and why

25
00:01:42,310 --> 00:01:43,840
 people don't think about it too much.

26
00:01:43,840 --> 00:01:47,360
 Because it's such a

27
00:01:47,360 --> 00:01:50,000
 broad topic. It's very hard to get your

28
00:01:51,000 --> 00:01:53,000
 head around Buddhism.

29
00:01:53,000 --> 00:01:59,140
 So the answer that I give is I guarantee it's only one of

30
00:01:59,140 --> 00:02:00,480
 many answers.

31
00:02:00,480 --> 00:02:05,200
 It's probably not the most all-inclusive answer out there.

32
00:02:05,200 --> 00:02:10,200
 It's probably not the most popular answer out there.

33
00:02:10,200 --> 00:02:16,560
 But Buddhism for me is something very particular.

34
00:02:18,240 --> 00:02:21,440
 And so I'm calling it as ASEAN.

35
00:02:21,440 --> 00:02:26,280
 Calling it as Buddhism means for me, what it means to me.

36
00:02:26,280 --> 00:02:28,240
 If you think Buddhism is something else,

37
00:02:28,240 --> 00:02:32,040
 then so be it. I have no problem with that.

38
00:02:32,040 --> 00:02:35,960
 But Buddhism

39
00:02:35,960 --> 00:02:39,240
 Buddhism as ASEAN, if someone asked me, "What is Buddhism?"

40
00:02:39,240 --> 00:02:43,150
 I'd say Buddhism is the teachings of the Buddha or the

41
00:02:43,150 --> 00:02:45,040
 teachings of a Buddha.

42
00:02:46,320 --> 00:02:48,320
 I think that's fair.

43
00:02:48,320 --> 00:02:50,680
 I don't think that's

44
00:02:50,680 --> 00:02:53,680
 completely...

45
00:02:53,680 --> 00:02:55,680
 I don't think that's entirely agreed upon.

46
00:02:55,680 --> 00:03:00,660
 But I think it's a simple definition. It's one that many

47
00:03:00,660 --> 00:03:03,360
 outsiders would easily understand.

48
00:03:03,360 --> 00:03:09,400
 The teachings of a Buddha and who is a Buddha, Buddha is

49
00:03:09,400 --> 00:03:11,440
 someone who knows.

50
00:03:13,880 --> 00:03:17,520
 And as I understand it, a Buddha is someone who knows and

51
00:03:17,520 --> 00:03:20,280
 understands the Four Noble Truths.

52
00:03:20,280 --> 00:03:26,480
 But as someone who understands those things,

53
00:03:26,480 --> 00:03:30,040
 that when understood,

54
00:03:30,040 --> 00:03:34,810
 bring one, transport one directly to complete freedom from

55
00:03:34,810 --> 00:03:35,720
 suffering.

56
00:03:35,720 --> 00:03:38,760
 That simply by knowing these things,

57
00:03:39,640 --> 00:03:43,640
 not simply, but the entire set of things that are required

58
00:03:43,640 --> 00:03:45,760
 to free one from suffering.

59
00:03:45,760 --> 00:03:50,700
 Upon the realization of these things, one no longer suffers

60
00:03:50,700 --> 00:03:54,400
. Basically the understanding of suffering and how it works.

61
00:03:54,400 --> 00:04:03,220
 Then the question that we often get asked is, "Is Buddhism

62
00:04:03,220 --> 00:04:05,640
 a religion?"

63
00:04:06,640 --> 00:04:10,390
 And there are many answers to this question. And these

64
00:04:10,390 --> 00:04:13,400
 answers are all out there. If you Google it, you'd probably

65
00:04:13,400 --> 00:04:13,920
 find right away,

66
00:04:13,920 --> 00:04:17,360
 "Buddhism religion. Is Buddhism a religion?"

67
00:04:17,360 --> 00:04:20,280
 You have people asking and answering.

68
00:04:20,280 --> 00:04:25,400
 My answer is that yes, Buddhism is a religion.

69
00:04:25,400 --> 00:04:31,290
 And I agree with some people who say Buddhism is not a

70
00:04:31,290 --> 00:04:32,480
 religion.

71
00:04:32,480 --> 00:04:35,240
 Because it totally depends on what you mean by the word "

72
00:04:35,240 --> 00:04:36,040
religion."

73
00:04:38,000 --> 00:04:41,990
 But the word "religion," it's generally understood to come

74
00:04:41,990 --> 00:04:45,360
 from being bound to something.

75
00:04:45,360 --> 00:04:49,000
 And I'm willing to bet that that's where the word actually

76
00:04:49,000 --> 00:04:49,920
 came from.

77
00:04:49,920 --> 00:04:55,280
 That it was being bound to God or being bound to a set of

78
00:04:55,280 --> 00:04:56,720
 teachings,

79
00:04:56,720 --> 00:04:59,720
 a set of principles,

80
00:04:59,720 --> 00:05:02,720
 a set of theories,

81
00:05:04,920 --> 00:05:06,920
 a set of beliefs.

82
00:05:06,920 --> 00:05:11,320
 While Buddhism isn't

83
00:05:11,320 --> 00:05:14,680
 fixated on the belief in God or the

84
00:05:14,680 --> 00:05:18,360
 idea of a supreme being,

85
00:05:18,360 --> 00:05:20,880
 Buddhism is,

86
00:05:20,880 --> 00:05:24,520
 as I see it, it's an attachment to certain beliefs and

87
00:05:24,520 --> 00:05:27,040
 ideas and practices and so on.

88
00:05:27,040 --> 00:05:31,690
 To me, religious Buddhism is taking Buddhism, taking the

89
00:05:31,690 --> 00:05:32,920
 teaching of the Buddha's

90
00:05:33,880 --> 00:05:35,680
 religiously.

91
00:05:35,680 --> 00:05:37,680
 The things that the Buddha taught

92
00:05:37,680 --> 00:05:44,760
 we hold to those.

93
00:05:44,760 --> 00:05:48,910
 We are restrained by those. We are bound to the teachings

94
00:05:48,910 --> 00:05:50,200
 of the Buddha.

95
00:05:50,200 --> 00:05:54,350
 If you consider yourself Buddhist, a part of the Buddhist

96
00:05:54,350 --> 00:05:54,920
 religion,

97
00:05:54,920 --> 00:05:58,320
 then the minimum requirement, and I'd say the only

98
00:05:58,320 --> 00:05:59,240
 requirement, is that you

99
00:05:59,240 --> 00:06:02,800
 stick to the teachings of the Buddha.

100
00:06:03,320 --> 00:06:05,320
 And to me that makes it religious.

101
00:06:05,320 --> 00:06:09,760
 That's qualification enough for it to be a religion.

102
00:06:09,760 --> 00:06:13,880
 It doesn't have to be faith. It doesn't have to be belief.

103
00:06:13,880 --> 00:06:17,390
 If you practice the Buddha's teachings out of faith, then

104
00:06:17,390 --> 00:06:18,720
 you're doing it religiously.

105
00:06:18,720 --> 00:06:21,280
 If you practice the teachings of the Buddha out of wisdom,

106
00:06:21,280 --> 00:06:23,160
 you're still doing it religiously.

107
00:06:23,160 --> 00:06:30,520
 As long as you stick to the teachings of the Buddha or a

108
00:06:30,520 --> 00:06:31,800
 Buddha.

109
00:06:31,800 --> 00:06:33,800
 [Silence]

110
00:06:33,800 --> 00:06:42,960
 So the teaching that I often do get asked by Thai people,

111
00:06:42,960 --> 00:06:46,940
 and it's an indirect question. It's one of these funny

112
00:06:46,940 --> 00:06:48,360
 things about cultural Buddhists,

113
00:06:48,360 --> 00:06:51,040
 people who grew up Buddhist, but

114
00:06:51,040 --> 00:06:54,420
 never really studied or practiced the Buddha's teachings in

115
00:06:54,420 --> 00:06:55,520
 any great depth.

116
00:06:56,920 --> 00:06:59,840
 They often ask me, "How would they explain Buddhism in a

117
00:06:59,840 --> 00:07:02,580
 nutshell to someone who doesn't know anything about

118
00:07:02,580 --> 00:07:03,480
 Buddhism?"

119
00:07:03,480 --> 00:07:12,560
 "How would you explain Buddhism?"

120
00:07:12,560 --> 00:07:15,880
 "What is the core of Buddhism?"

121
00:07:15,880 --> 00:07:19,720
 "To someone who doesn't know anything about Buddhism?"

122
00:07:19,720 --> 00:07:24,400
 And this one is, this question is answered

123
00:07:25,680 --> 00:07:27,800
 quite well, I think, and across the board.

124
00:07:27,800 --> 00:07:32,600
 And I think the best answer is given quite often is

125
00:07:32,600 --> 00:07:36,160
 that the teachings of the Buddha are summed up by what is

126
00:07:36,160 --> 00:07:38,400
 called the "Ovada Patimoka."

127
00:07:38,400 --> 00:07:44,000
 The "Ovada Patimoka." Patimoka means

128
00:07:44,000 --> 00:07:49,440
 the code,

129
00:07:51,200 --> 00:07:55,520
 it's translated as "code." In this case monastic code you

130
00:07:55,520 --> 00:07:56,240
 could say.

131
00:07:56,240 --> 00:08:01,790
 The code of the Buddha, the real core essence of the Dharma

132
00:08:01,790 --> 00:08:01,800
,

133
00:08:01,800 --> 00:08:05,840
 of the teachings of the Buddha.

134
00:08:05,840 --> 00:08:11,120
 And "Ovada" means the speech.

135
00:08:11,120 --> 00:08:15,240
 So this is the speech given by the Buddha that is the core,

136
00:08:15,240 --> 00:08:17,760
 or the code.

137
00:08:17,760 --> 00:08:21,880
 It was to be the binding code or the, you know, like the

138
00:08:21,880 --> 00:08:24,580
 ten commandments of Buddhism sort of, except it's not

139
00:08:24,580 --> 00:08:25,120
 commandments,

140
00:08:25,120 --> 00:08:29,520
 it's edicts or it's injunctions or so on.

141
00:08:29,520 --> 00:08:34,720
 It's laws, you could say.

142
00:08:34,720 --> 00:08:37,680
 The Buddha said, "This is this, this is this."

143
00:08:42,000 --> 00:08:48,080
 And how it goes is the Buddha first sums up his teachings.

144
00:08:48,080 --> 00:08:58,160
 That the teaching of all the Buddhas,

145
00:08:58,160 --> 00:09:07,270
 whether they be in the past or the present Buddha that has

146
00:09:07,270 --> 00:09:08,640
 just recently passed away,

147
00:09:09,600 --> 00:09:12,710
 or whether they be future Buddhas, their teaching is all

148
00:09:12,710 --> 00:09:13,920
 comes down to three things.

149
00:09:13,920 --> 00:09:22,830
 Not doing any evil deeds, number one. Number two, being

150
00:09:22,830 --> 00:09:27,280
 full of good deeds, and number three, purifying the mind.

151
00:09:27,280 --> 00:09:34,100
 These three things the Buddha said, "This is the teaching

152
00:09:34,100 --> 00:09:35,200
 of all the Buddhas."

153
00:09:35,200 --> 00:09:37,560
 Anyone who claims to be a Buddha has to teach these three

154
00:09:37,560 --> 00:09:38,080
 things.

155
00:09:38,720 --> 00:09:41,740
 Anyone who is a perfectly enlightened Buddha teaches these

156
00:09:41,740 --> 00:09:42,640
 three things.

157
00:09:42,640 --> 00:09:46,080
 It's really not a lot.

158
00:09:46,080 --> 00:09:51,080
 In fact, one of the wonderful things about the Buddha's

159
00:09:51,080 --> 00:09:53,920
 teaching is what it's not.

160
00:09:53,920 --> 00:09:58,330
 From my point of view, one of the great things about the

161
00:09:58,330 --> 00:10:01,040
 Buddha's teaching is what it leaves out, what it leaves

162
00:10:01,040 --> 00:10:01,600
 behind.

163
00:10:01,600 --> 00:10:06,100
 There's no excess baggage in Buddhism in the Buddha's

164
00:10:06,100 --> 00:10:06,960
 teaching.

165
00:10:08,560 --> 00:10:11,800
 We Buddhists have put a lot of extra baggage on it. We have

166
00:10:11,800 --> 00:10:15,360
 these ceremonies and practices.

167
00:10:15,360 --> 00:10:19,440
 But the core teachings of the Buddha are very simple.

168
00:10:19,440 --> 00:10:22,130
 And if you can follow these three things, you don't need

169
00:10:22,130 --> 00:10:23,120
 anything else.

170
00:10:23,120 --> 00:10:28,360
 If you can stop doing all evil deeds, fill yourself up with

171
00:10:28,360 --> 00:10:30,480
 goodness and purify your mind,

172
00:10:30,480 --> 00:10:33,920
 then you become enlightened.

173
00:10:35,440 --> 00:10:37,470
 According to the Buddha, according to the Buddha's teaching

174
00:10:37,470 --> 00:10:38,400
, you're enlightened.

175
00:10:38,400 --> 00:10:41,280
 There's nothing else you need to do.

176
00:10:41,280 --> 00:10:46,180
 So when someone asks, "What are the teachings of the Buddha

177
00:10:46,180 --> 00:10:46,400
?"

178
00:10:46,400 --> 00:10:52,400
 These three are the nutshell, Buddhism in a nutshell.

179
00:10:52,400 --> 00:10:56,640
 You can go into the Four Noble Truths and so on and so on,

180
00:10:56,640 --> 00:10:57,600
 but if you want people to

181
00:10:57,600 --> 00:11:00,240
 understand exactly what is Buddhism,

182
00:11:00,240 --> 00:11:03,360
 and they should understand that that's all it is,

183
00:11:03,760 --> 00:11:10,500
 that there's no requirements in terms of believing this or

184
00:11:10,500 --> 00:11:15,840
 believing that, worship or rights and rituals.

185
00:11:15,840 --> 00:11:20,160
 You don't even have to call yourself Buddhist.

186
00:11:20,160 --> 00:11:26,100
 If you do these three things, you're following the

187
00:11:26,100 --> 00:11:27,280
 teachings of the Buddha.

188
00:11:27,280 --> 00:11:47,280
 [silence]

189
00:11:47,280 --> 00:11:52,810
 The key of the three, the one that really sets the Buddha's

190
00:11:52,810 --> 00:11:55,520
 teaching apart from other religious teachings,

191
00:11:56,960 --> 00:12:03,380
 or according to us, it does, I think it's not 100% cut and

192
00:12:03,380 --> 00:12:06,720
 dry, but the one thing where the Buddha really shines,

193
00:12:06,720 --> 00:12:08,780
 where the Buddha's teaching really shines, is in the pur

194
00:12:08,780 --> 00:12:09,600
ification of the mind,

195
00:12:09,600 --> 00:12:12,880
 because it's not something you hear talked about a lot.

196
00:12:12,880 --> 00:12:17,440
 You might find it mentioned in other religious traditions,

197
00:12:17,440 --> 00:12:23,780
 but the purification of the mind becomes the central theme

198
00:12:23,780 --> 00:12:24,640
 of Buddhism,

199
00:12:25,440 --> 00:12:28,800
 and actually not doing bad deeds and doing good deeds

200
00:12:28,800 --> 00:12:34,400
 actually becomes secondary or becomes less central to the

201
00:12:34,400 --> 00:12:36,160
 teachings.

202
00:12:36,160 --> 00:12:40,060
 Yes, it's important to refrain from bad deeds, and yes, it

203
00:12:40,060 --> 00:12:41,280
's important to do good deeds,

204
00:12:41,280 --> 00:12:47,120
 but the most important, by far most important,

205
00:12:47,120 --> 00:12:50,810
 and in fact, the only important thing for you to do is to

206
00:12:50,810 --> 00:12:52,000
 purify your mind,

207
00:12:52,720 --> 00:12:57,360
 because once you've done that, you can never do bad deeds.

208
00:12:57,360 --> 00:13:01,680
 Everything you do is either a neutral deed or a good deed.

209
00:13:01,680 --> 00:13:04,960
 You can never perform an evil deed if your mind is pure.

210
00:13:04,960 --> 00:13:16,880
 And so, while it would never do for us to ignore the first

211
00:13:16,880 --> 00:13:17,440
 two,

212
00:13:18,000 --> 00:13:23,100
 you shouldn't say, "Ah, well, you do bad deeds. Don't think

213
00:13:23,100 --> 00:13:24,160
 about doing good deeds.

214
00:13:24,160 --> 00:13:27,520
 Don't worry about that. Just try to purify your mind."

215
00:13:27,520 --> 00:13:31,680
 You can't say that, because every time you do a bad deed,

216
00:13:31,680 --> 00:13:33,040
 your mind becomes more defiled,

217
00:13:33,040 --> 00:13:36,470
 and without performing good deeds, your mind will never

218
00:13:36,470 --> 00:13:39,520
 have the power and the strength

219
00:13:39,520 --> 00:13:43,520
 to become free from, to be pure.

220
00:13:45,600 --> 00:13:48,200
 Without undertaking the first two, your mind will never

221
00:13:48,200 --> 00:13:48,960
 become pure.

222
00:13:48,960 --> 00:13:56,800
 But when we want to understand what is the core and what is

223
00:13:56,800 --> 00:13:57,600
 the most important,

224
00:13:57,600 --> 00:14:00,600
 it's the purification of the mind. We should never be compl

225
00:14:00,600 --> 00:14:02,880
acent, content just to

226
00:14:02,880 --> 00:14:07,750
 be a good person and not do bad deeds, thinking that that's

227
00:14:07,750 --> 00:14:08,240
 enough,

228
00:14:08,240 --> 00:14:14,000
 because inevitably, when we are aroused, bad thoughts will

229
00:14:14,000 --> 00:14:14,880
 come into our minds,

230
00:14:14,880 --> 00:14:18,880
 whether it be thoughts of anger, hate theories, and extr

231
00:14:18,880 --> 00:14:21,040
aneous, superfluous

232
00:14:21,040 --> 00:14:24,720
 ideas about what the Buddha taught on the other side.

233
00:14:24,720 --> 00:14:29,540
 You get all sorts of things. People say that the Buddha was

234
00:14:29,540 --> 00:14:30,480
 a social activist.

235
00:14:30,480 --> 00:14:35,440
 The outcast in India said he was their champion.

236
00:14:35,440 --> 00:14:43,030
 In Buddhist countries, we see rituals and even worship and

237
00:14:43,030 --> 00:14:44,640
 prayer come up.

238
00:14:45,760 --> 00:14:51,520
 [Silence]

239
00:14:51,520 --> 00:14:56,880
 And Buddhism becomes a much fuller entity.

240
00:14:56,880 --> 00:15:01,350
 To the point, not that these things are wrong, and the

241
00:15:01,350 --> 00:15:04,720
 Buddha certainly was good for the lower

242
00:15:04,720 --> 00:15:10,310
 class in India, and prayer in a certain form, even worship

243
00:15:10,310 --> 00:15:13,200
 and ritual in a certain form,

244
00:15:13,200 --> 00:15:17,720
 can be wholesome. But it gets to the point where we lose

245
00:15:17,720 --> 00:15:18,560
 sight of the core,

246
00:15:18,560 --> 00:15:21,560
 this side of what is the Buddha's teaching, because the

247
00:15:21,560 --> 00:15:23,600
 Buddha didn't teach us to

248
00:15:23,600 --> 00:15:28,620
 rise up and raise our status in society. The Buddha didn't

249
00:15:28,620 --> 00:15:31,520
 teach us to worship him and

250
00:15:31,520 --> 00:15:38,290
 perform rites and rituals and to pray. The Buddha taught us

251
00:15:38,290 --> 00:15:39,760
 these three things.

252
00:15:40,800 --> 00:15:43,990
 And if we do these other associated things, then as long as

253
00:15:43,990 --> 00:15:45,600
 they have to do with the purification

254
00:15:45,600 --> 00:15:49,390
 of the mind, then they can fit in with the Buddha's

255
00:15:49,390 --> 00:15:53,360
 teaching. We often talked about culture.

256
00:15:53,360 --> 00:15:58,400
 Culture, when culture mixes with Buddhism, it's fine if

257
00:15:58,400 --> 00:16:00,160
 culture can adapt itself

258
00:16:00,160 --> 00:16:02,260
 to the Buddha's teaching, but you should never adapt the

259
00:16:02,260 --> 00:16:03,840
 Buddha's teaching to fit the culture.

260
00:16:06,240 --> 00:16:13,960
 You've got people drinking alcohol and dancing and beauty

261
00:16:13,960 --> 00:16:20,240
 pageants and so on in the monasteries.

262
00:16:20,240 --> 00:16:27,040
 It can only degrade the purity of the Buddha's teaching.

263
00:16:27,040 --> 00:16:34,120
 This is if we wish to be Buddhist. We wish to follow the

264
00:16:34,120 --> 00:16:35,600
 Buddha's teaching.

265
00:16:36,160 --> 00:16:37,780
 Then we have to understand what is the core. What are the

266
00:16:37,780 --> 00:16:39,520
 things that the Buddha actually taught?

267
00:16:39,520 --> 00:16:46,020
 So what else did the Buddha teach? He said this is the core

268
00:16:46,020 --> 00:16:46,480
.

269
00:16:46,480 --> 00:16:48,440
 Or he didn't say this is the core. He said this is the

270
00:16:48,440 --> 00:16:50,240
 teachings of all the Buddha. We understand

271
00:16:50,240 --> 00:16:56,450
 this to be the core. What else did he say? He said patience

272
00:16:56,450 --> 00:17:01,520
. Patience is the highest form of austerity.

273
00:17:03,920 --> 00:17:12,880
 I think no one understands this as well as meditators.

274
00:17:12,880 --> 00:17:19,220
 In that sense, it's spot on. It's the first thing you need

275
00:17:19,220 --> 00:17:20,000
 to tell a meditator.

276
00:17:20,000 --> 00:17:25,640
 Patience is the highest form of austerity. Patience is the

277
00:17:25,640 --> 00:17:30,240
 greatest form of torture.

278
00:17:31,600 --> 00:17:34,480
 In one way to translate this is the form of torture.

279
00:17:34,480 --> 00:17:38,400
 Because in India they would torture themselves.

280
00:17:38,400 --> 00:17:42,290
 Torturing themselves saying, "Okay, I'm going to inflict

281
00:17:42,290 --> 00:17:44,080
 this pain on that pain on myself."

282
00:17:44,080 --> 00:17:46,960
 Actually, I would say there's nothing as painful as

283
00:17:46,960 --> 00:17:47,760
 patience.

284
00:17:47,760 --> 00:17:53,040
 I would say that because when you inflict pain on yourself,

285
00:17:53,040 --> 00:17:56,080
 there's some kind of grim

286
00:18:00,320 --> 00:18:04,300
 pleasure that you gain from it. This kind of gritting your

287
00:18:04,300 --> 00:18:06,400
 teeth and so on.

288
00:18:06,400 --> 00:18:11,530
 Inflicting something. You're in charge. But patience is the

289
00:18:11,530 --> 00:18:12,400
 worst.

290
00:18:12,400 --> 00:18:17,160
 Patience is an incredible torture. Having to sit through my

291
00:18:17,160 --> 00:18:19,280
 talks

292
00:18:19,280 --> 00:18:23,920
 for minutes and minutes on end.

293
00:18:26,720 --> 00:18:30,880
 When I was in Thailand, we had to sit through talks an hour

294
00:18:30,880 --> 00:18:32,960
, sometimes an hour and a half.

295
00:18:32,960 --> 00:18:38,970
 You think that was bad? You think it's bad to listen to an

296
00:18:38,970 --> 00:18:40,080
 hour and a half talk?

297
00:18:40,080 --> 00:18:43,480
 They were all in Thai and we didn't understand the word of

298
00:18:43,480 --> 00:18:46,960
 it. That's the worst.

299
00:18:46,960 --> 00:18:53,010
 Here we are, these new meditators. Our teacher tells us to

300
00:18:53,010 --> 00:18:54,400
 sit for 10 minutes,

301
00:18:54,400 --> 00:18:56,880
 walk for 10 minutes, sit for 10 minutes and we can do that.

302
00:18:56,880 --> 00:19:02,010
 Maybe we're up to 15 minutes walking, 15 minutes sitting,

303
00:19:02,010 --> 00:19:03,760
 20 minutes walking, 20 minutes.

304
00:19:03,760 --> 00:19:06,170
 Then suddenly we have to sit still for an hour and a half,

305
00:19:06,170 --> 00:19:07,200
 two hours sometimes

306
00:19:07,200 --> 00:19:09,600
 with all the ceremonies and so on.

307
00:19:09,600 --> 00:19:18,090
 Patience is the worst. Simply having to bear with things

308
00:19:18,090 --> 00:19:19,520
 and to the point where people often

309
00:19:20,320 --> 00:19:23,970
 accuse us of torturing ourselves. Sitting through pain when

310
00:19:23,970 --> 00:19:25,840
 you sit still and then pain arises.

311
00:19:25,840 --> 00:19:29,700
 The normal thought is to shift, get yourself some more pill

312
00:19:29,700 --> 00:19:31,920
ows. One under this leg, one

313
00:19:31,920 --> 00:19:34,860
 under that leg, one behind your back and the point where

314
00:19:34,860 --> 00:19:36,720
 you might as well be lying down.

315
00:19:36,720 --> 00:19:42,680
 When we tell them not to, we say try your best to sit still

316
00:19:42,680 --> 00:19:43,200
.

317
00:19:45,120 --> 00:19:49,010
 If you have to prop yourself, your legs up in the beginning

318
00:19:49,010 --> 00:19:50,960
. I had one student who started

319
00:19:50,960 --> 00:19:52,940
 on the floor, he was doing really good and then the second

320
00:19:52,940 --> 00:19:53,680
 day he put a pillow,

321
00:19:53,680 --> 00:19:56,970
 then the third day two pillows under his legs and then the

322
00:19:56,970 --> 00:19:58,640
 fourth day another pillow.

323
00:19:58,640 --> 00:20:03,140
 And I said to him, "Look, the object really is to go down,

324
00:20:03,140 --> 00:20:06,480
 not to go up. The object is to

325
00:20:06,480 --> 00:20:12,560
 become more tolerant, not less tolerant." It's like you're

326
00:20:12,560 --> 00:20:14,720
 just trying to find the

327
00:20:14,720 --> 00:20:17,290
 perfect position and then when some pain comes up you think

328
00:20:17,290 --> 00:20:19,120
, "Okay, I gotta add another pillow,

329
00:20:19,120 --> 00:20:20,720
 then more pain and I gotta add more."

330
00:20:20,720 --> 00:20:29,670
 And people say, "Oh, you're bearing with the pain that's

331
00:20:29,670 --> 00:20:31,600
 torture.

332
00:20:31,600 --> 00:20:35,380
 That's wrong practice. That's against the Buddhist teaching

333
00:20:35,380 --> 00:20:38,000
." And we have to vehemently deny this.

334
00:20:38,000 --> 00:20:43,190
 Someone who teaches this way is not looking out for your

335
00:20:43,190 --> 00:20:46,000
 best interest and is not going

336
00:20:46,000 --> 00:20:47,910
 according to the Buddhist teaching. When they tell you, you

337
00:20:47,910 --> 00:20:49,200
 have to adjust, you have to move,

338
00:20:49,200 --> 00:20:52,920
 you have to avoid all pain. It's very clear from the

339
00:20:52,920 --> 00:20:55,440
 Buddhist teaching that you have to put up with

340
00:20:55,440 --> 00:20:58,810
 pain even to the point that it might kill you. If you

341
00:20:58,810 --> 00:21:01,600
 really want to become free from suffering,

342
00:21:01,600 --> 00:21:04,640
 you have to be willing to put up with even deadly pain

343
00:21:04,640 --> 00:21:08,240
 because that's what we're faced with. In the

344
00:21:08,240 --> 00:21:13,260
 end, deadly pain is on our horizon. That may be how we die

345
00:21:13,260 --> 00:21:17,680
 in deadly pain. And if we're not able to

346
00:21:17,680 --> 00:21:22,490
 bear with it now, then surely the pain when we die will be

347
00:21:22,490 --> 00:21:24,720
 much worse. And if we can't bear with

348
00:21:24,720 --> 00:21:28,220
 it, we'll have to take drugs and we die in a muddled state.

349
00:21:28,220 --> 00:21:29,840
 And who knows where we end up,

350
00:21:29,840 --> 00:21:33,200
 but for sure we end up attached and clinging because we

351
00:21:33,200 --> 00:21:34,400
 haven't let go.

352
00:21:34,400 --> 00:21:40,560
 Patience is so important because it helps you to let go.

353
00:21:40,560 --> 00:21:44,160
 Patience is the ultimate letting go.

354
00:21:44,160 --> 00:21:47,810
 It's saying to yourself, "I can bear with this. I can live

355
00:21:47,810 --> 00:21:48,720
 with this.

356
00:21:51,600 --> 00:21:56,420
 I can accept this reality." It's accepting reality for what

357
00:21:56,420 --> 00:21:57,120
 it is.

358
00:21:57,120 --> 00:22:03,440
 I don't need some other state of reality to make me happy.

359
00:22:03,440 --> 00:22:11,330
 It's contentment. I always think of contentment as this

360
00:22:11,330 --> 00:22:16,720
 wonderful, like the crown jewel of existence.

361
00:22:18,160 --> 00:22:22,040
 If you could only be content with whatever comes, the

362
00:22:22,040 --> 00:22:26,320
 Buddha said it's the ultimate treasure,

363
00:22:26,320 --> 00:22:33,360
 the ultimate wealth. The greatest wealth is contentment.

364
00:22:33,360 --> 00:22:36,480
 And I think that's so true.

365
00:22:36,480 --> 00:22:40,650
 There's nothing we can get that's ever going to satisfy us.

366
00:22:40,650 --> 00:22:42,480
 There's no object that we can get

367
00:22:42,480 --> 00:22:46,430
 that's ever going to satisfy us except contentment. And we

368
00:22:46,430 --> 00:22:48,960
 can be content with things as they are

369
00:22:48,960 --> 00:22:54,320
 because reality is singular. You only have one reality. It

370
00:22:54,320 --> 00:22:55,680
's this. It's where you are now.

371
00:22:55,680 --> 00:23:00,170
 And every moment you have two choices, accepting it or not

372
00:23:00,170 --> 00:23:02,080
 accepting it,

373
00:23:02,080 --> 00:23:06,960
 accepting it or needing something else. Clinging.

374
00:23:12,080 --> 00:23:14,760
 When you're finally content with things as they are and you

375
00:23:14,760 --> 00:23:17,920
 don't require any specific reality,

376
00:23:17,920 --> 00:23:28,210
 this is the ultimate wealth. And so patience is the highest

377
00:23:28,210 --> 00:23:28,960
 form of

378
00:23:28,960 --> 00:23:33,170
 torture and this is because it leads to contentment. It

379
00:23:33,170 --> 00:23:34,560
 leads you to accept.

380
00:23:37,360 --> 00:23:41,470
 The word actually the Buddha uses is tapa. And tapa means

381
00:23:41,470 --> 00:23:45,120
 to burn or it means heat.

382
00:23:45,120 --> 00:23:49,130
 But it was used in India to mean austerity and torturing

383
00:23:49,130 --> 00:23:50,800
 yourself and so on.

384
00:23:50,800 --> 00:23:54,780
 But if you think about it, what is it doing? Is it burning

385
00:23:54,780 --> 00:23:58,000
 up? It's burning up the evil inside.

386
00:23:58,000 --> 00:24:01,050
 It's burning up the unwholesomeness, the things that are

387
00:24:01,050 --> 00:24:02,560
 causing you suffering.

388
00:24:04,640 --> 00:24:06,740
 It's another thing when we talk about evil in Buddhism, we

389
00:24:06,740 --> 00:24:07,200
 don't mean

390
00:24:07,200 --> 00:24:11,120
 God says it's evil or I say it's evil or Buddha says evil.

391
00:24:11,120 --> 00:24:13,040
 It means it's something that hurts you,

392
00:24:13,040 --> 00:24:16,200
 something that causes suffering for you, something that

393
00:24:16,200 --> 00:24:18,000
 only leads to your detriment.

394
00:24:18,000 --> 00:24:22,200
 It's a pretty selfish sort of thing. There's nothing that's

395
00:24:22,200 --> 00:24:24,560
 evil except leads to your detriment.

396
00:24:29,440 --> 00:24:36,880
 And how we get to avoid the selfishness is because anything

397
00:24:36,880 --> 00:24:38,160
 that hurts anyone else also

398
00:24:38,160 --> 00:24:41,310
 leads to your detriment. It can't help you to hurt someone

399
00:24:41,310 --> 00:24:44,320
 else. So there's no need to worry

400
00:24:44,320 --> 00:24:47,790
 about being selfish in Buddhism. People say you're sitting

401
00:24:47,790 --> 00:24:49,520
 in meditation, what are you doing for

402
00:24:49,520 --> 00:24:54,260
 other people? What are you doing to help the world? It's

403
00:24:54,260 --> 00:24:56,640
 what you're not doing really.

404
00:24:59,120 --> 00:25:02,380
 In one sense, it's what you're not doing. That is of most

405
00:25:02,380 --> 00:25:04,560
 benefit. You're not going out there

406
00:25:04,560 --> 00:25:08,890
 and yelling at people and fighting with people and grabbing

407
00:25:08,890 --> 00:25:13,280
 for power. Think of what people

408
00:25:13,280 --> 00:25:16,650
 do when they get together. Think of what we do when we get

409
00:25:16,650 --> 00:25:19,200
 together. Half the time it's unwholesome,

410
00:25:19,200 --> 00:25:25,440
 more than half the time. Clinging and posturing.

411
00:25:25,440 --> 00:25:34,240
 [silence]

412
00:25:34,240 --> 00:25:36,560
 When we sit in meditation, what are we doing? We're pur

413
00:25:36,560 --> 00:25:37,440
ifying our minds.

414
00:25:37,440 --> 00:25:40,680
 So on the one hand, we're not hurting anyone and that's a

415
00:25:40,680 --> 00:25:41,360
 good thing.

416
00:25:41,360 --> 00:25:44,390
 But on the other hand, we're doing something equally

417
00:25:44,390 --> 00:25:45,280
 important.

418
00:25:49,440 --> 00:25:54,950
 We're preparing ourselves. We're changing the makeup of our

419
00:25:54,950 --> 00:25:55,840
 minds.

420
00:25:55,840 --> 00:26:01,980
 We're changing the way we look at things. We're changing

421
00:26:01,980 --> 00:26:03,920
 the way we react to things.

422
00:26:03,920 --> 00:26:13,010
 It's amazing to me that anyone could say that meditating is

423
00:26:13,010 --> 00:26:16,000
 being lazy. I think I understand

424
00:26:16,000 --> 00:26:18,920
 why it's because they don't understand what is meditation.

425
00:26:18,920 --> 00:26:21,760
 But if you understood what meditation

426
00:26:21,760 --> 00:26:26,530
 was, it would be incredible to think that somehow it was un

427
00:26:26,530 --> 00:26:29,120
beneficial, that it was selfish,

428
00:26:29,120 --> 00:26:31,800
 that it wasn't going to help anybody else for you to sit on

429
00:26:31,800 --> 00:26:32,880
 a pillow and meditate.

430
00:26:32,880 --> 00:26:37,310
 Because it's an incredible benefit to everyone you come in

431
00:26:37,310 --> 00:26:40,800
 contact with.

432
00:26:41,920 --> 00:26:42,480
 Excuse me.

433
00:26:42,480 --> 00:26:52,470
 You meditate. You practice. You learn. You understand. You

434
00:26:52,470 --> 00:26:54,800
're patient.

435
00:26:54,800 --> 00:26:59,920
 And all of those skills that you gain from the meditation,

436
00:26:59,920 --> 00:27:02,400
 you bring out into the world

437
00:27:02,400 --> 00:27:06,890
 around you. Meditation is the beginning of your life. It's

438
00:27:06,890 --> 00:27:08,320
 not the end of it.

439
00:27:10,080 --> 00:27:11,920
 Meditation is what leads to...

440
00:27:11,920 --> 00:27:16,040
 It should be the source of all of your actions. It should

441
00:27:16,040 --> 00:27:17,760
 be the meditative mind.

442
00:27:17,760 --> 00:27:26,250
 Just like the source of water, the spring flowing down into

443
00:27:26,250 --> 00:27:28,480
 the stream and the stream

444
00:27:28,480 --> 00:27:33,790
 flowing down into the lake. And if the source is pure, the

445
00:27:33,790 --> 00:27:36,800
 stream is pure and the lake is pure.

446
00:27:38,160 --> 00:27:46,860
 But if the source is defiled, is impure, then the river is

447
00:27:46,860 --> 00:27:49,440
 impure, the stream is impure and the lake

448
00:27:49,440 --> 00:27:53,600
 can't help but be impure. It means everything we do and say

449
00:27:53,600 --> 00:27:56,080
 and how we interact with the world

450
00:27:56,080 --> 00:28:00,350
 around us is based on the mind. This is the source. When

451
00:28:00,350 --> 00:28:02,480
 our mind is impure, everything

452
00:28:03,280 --> 00:28:06,250
 we do and say is going to be based on that impurity. We're

453
00:28:06,250 --> 00:28:07,920
 going to hurt other people.

454
00:28:07,920 --> 00:28:09,040
 We're going to hurt ourselves.

455
00:28:09,040 --> 00:28:16,200
 So when people say meditation is only for yourself or so on

456
00:28:16,200 --> 00:28:16,240
,

457
00:28:18,000 --> 00:28:32,730
 you know meditation is a benefit to everyone. Even our own

458
00:28:32,730 --> 00:28:35,760
 meditation.

459
00:28:35,760 --> 00:28:37,800
 I don't want to go in too far. I think I've gone on long

460
00:28:37,800 --> 00:28:38,320
 enough.

461
00:28:38,320 --> 00:28:42,080
 So I'm just going to wrap it up there. I didn't want to...

462
00:28:44,560 --> 00:28:46,780
 We're going to do too much detail about the Buddha's

463
00:28:46,780 --> 00:28:48,560
 teaching but just try to get an idea

464
00:28:48,560 --> 00:28:53,010
 of what is Buddhism. It's a religion where we bind

465
00:28:53,010 --> 00:28:56,000
 ourselves to these practices.

466
00:28:56,000 --> 00:29:01,760
 We make a resolution in ourselves that we're going to work

467
00:29:01,760 --> 00:29:05,440
 and we're going to base our lives

468
00:29:05,440 --> 00:29:09,830
 on these principles of not hurting other people, not doing

469
00:29:09,830 --> 00:29:13,600
 evil deeds, trying our best to do good

470
00:29:13,600 --> 00:29:17,400
 deeds to help other people and to help ourselves and to pur

471
00:29:17,400 --> 00:29:19,200
ification of our minds.

472
00:29:19,200 --> 00:29:23,350
 So this is why we're here tonight. We're here to practice

473
00:29:23,350 --> 00:29:24,880
 these teachings.

474
00:29:24,880 --> 00:29:29,330
 It becomes a religion for us when we say this is how we

475
00:29:29,330 --> 00:29:31,840
 want to live our lives.

476
00:29:31,840 --> 00:29:36,340
 There's no need to make it a religion if you want to just

477
00:29:36,340 --> 00:29:37,920
 try it and people want to

478
00:29:37,920 --> 00:29:41,350
 do it as a hobby or so on. There's no need to call yourself

479
00:29:41,350 --> 00:29:42,240
 Buddhist.

480
00:29:42,800 --> 00:29:44,720
 But there's no need to be afraid of calling yourself

481
00:29:44,720 --> 00:29:47,120
 Buddhist, at least in your own mind,

482
00:29:47,120 --> 00:29:50,860
 at least internally. You can say to yourself that you're

483
00:29:50,860 --> 00:29:51,520
 Buddhist

484
00:29:51,520 --> 00:29:56,160
 and you don't have to accept this or that tradition or this

485
00:29:56,160 --> 00:29:57,280
 or that culture.

486
00:29:57,280 --> 00:30:00,640
 You accept these three principles in your Buddhist.

487
00:30:00,640 --> 00:30:07,120
 So especially this one, the third one, this is what we're

488
00:30:07,120 --> 00:30:08,080
 focusing on here.

489
00:30:09,360 --> 00:30:12,470
 This is what we're here together to do. We're here to pur

490
00:30:12,470 --> 00:30:13,280
ify our minds,

491
00:30:13,280 --> 00:30:17,110
 to purify our understanding of ourselves and the world

492
00:30:17,110 --> 00:30:17,840
 around us.

493
00:30:17,840 --> 00:30:22,190
 And we start with our formal meditation practice, which we

494
00:30:22,190 --> 00:30:24,720
 can then take out into our daily lives.

495
00:30:24,720 --> 00:30:31,670
 In our daily lives, we can apply these principles of being

496
00:30:31,670 --> 00:30:33,200
 mindful of knowing what we're doing,

497
00:30:33,200 --> 00:30:37,350
 of seeing things for what they are, no more, no less. This

498
00:30:37,350 --> 00:30:40,080
 is what we're training ourselves here.

499
00:30:40,080 --> 00:30:45,600
 I thank everyone for coming and now we'll go right into the

500
00:30:45,600 --> 00:30:46,720
 practical

501
00:30:46,720 --> 00:30:53,040
 segment. First we'll do mindful frustration, then walking.

502
00:30:53,520 --> 00:30:54,880
 And then sitting.

503
00:30:54,880 --> 00:30:58,720
 So.

