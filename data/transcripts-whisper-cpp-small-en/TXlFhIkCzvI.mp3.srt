1
00:00:00,000 --> 00:00:24,680
 Good evening everyone.

2
00:00:24,680 --> 00:00:35,680
 I'm broadcasting live March 19th.

3
00:00:35,680 --> 00:00:50,680
 I think everyone's doing everything just okay.

4
00:00:50,680 --> 00:00:57,680
 Good evening.

5
00:00:57,680 --> 00:01:07,680
 Good evening.

6
00:01:07,680 --> 00:01:33,680
 Good evening.

7
00:01:33,680 --> 00:01:39,350
 I'm just reading through the comments. Tonight's quote is

8
00:01:39,350 --> 00:01:43,680
 actually a series of quotes.

9
00:01:43,680 --> 00:01:48,680
 Four verses from the Dhammapada.

10
00:01:48,680 --> 00:02:08,020
 One of which we've actually studied in our Dhammapada

11
00:02:08,020 --> 00:02:10,680
 verses.

12
00:02:10,680 --> 00:02:17,820
 Not to the faults of others, not what they have done, not

13
00:02:17,820 --> 00:02:22,680
 what others have done and not done.

14
00:02:22,680 --> 00:02:30,680
 Ata nova avai kaya. One should look upon oneself.

15
00:02:30,680 --> 00:02:39,060
 Look upon those of oneself. Katani, akata, nitya. Done.

16
00:02:39,060 --> 00:02:43,680
 Things done and not done.

17
00:02:43,680 --> 00:02:49,680
 This is a theme that runs through Theravada Buddhism.

18
00:02:49,680 --> 00:02:54,430
 It's our understanding that the Buddha actually taught us

19
00:02:54,430 --> 00:02:57,680
 to look after ourselves.

20
00:02:57,680 --> 00:03:01,770
 Let me actually on face value, this one says something a

21
00:03:01,770 --> 00:03:03,680
 little bit different.

22
00:03:03,680 --> 00:03:09,680
 He actually says, don't go around blaming other people.

23
00:03:09,680 --> 00:03:15,680
 Looking at the faults of others, criticizing others.

24
00:03:15,680 --> 00:03:19,620
 But you could expand that to think about concerning,

25
00:03:19,620 --> 00:03:21,680
 worrying about others.

26
00:03:21,680 --> 00:03:31,680
 Trying to fix everyone else's problems.

27
00:03:31,680 --> 00:03:39,540
 There's another Dhammapada verse. Actually it might be one

28
00:03:39,540 --> 00:03:43,680
 of these four.

29
00:03:43,680 --> 00:03:46,680
 I think it's missing here.

30
00:03:46,680 --> 00:03:53,940
 There's another one that's, ata namiva patamang patirupay

31
00:03:53,940 --> 00:03:54,680
 uni vesay.

32
00:03:54,680 --> 00:04:00,730
 One should set oneself in what is right first and only then

33
00:04:00,730 --> 00:04:02,680
 should one teach others.

34
00:04:02,680 --> 00:04:05,680
 That's why one wouldn't defile themselves.

35
00:04:05,680 --> 00:04:16,780
 Which is really the best explanation of what's going on in

36
00:04:16,780 --> 00:04:17,680
 some of these quotes.

37
00:04:17,680 --> 00:04:25,680
 In order to help others, you really have to help yourself.

38
00:04:25,680 --> 00:04:37,030
 There's this perception that it's selfish, self-centered,

39
00:04:37,030 --> 00:04:37,680
 selfish,

40
00:04:37,680 --> 00:04:42,680
 to go off in your room and practice meditation,

41
00:04:42,680 --> 00:04:47,680
 to come to a meditation center, spend your time meditating,

42
00:04:47,680 --> 00:04:51,680
 apropos.

43
00:04:51,680 --> 00:04:55,510
 Just this morning something happened that I didn't really

44
00:04:55,510 --> 00:04:56,680
 expect to happen in Canada.

45
00:04:56,680 --> 00:05:00,000
 Though I suppose thinking about it, it's happened many

46
00:05:00,000 --> 00:05:00,680
 years ago,

47
00:05:00,680 --> 00:05:04,680
 but it hasn't happened in many years.

48
00:05:04,680 --> 00:05:08,550
 Someone in a van just down the street here, was walking

49
00:05:08,550 --> 00:05:14,680
 down the street in a white van.

50
00:05:14,680 --> 00:05:21,680
 As it was driving by, someone shouted out, "Get a job!"

51
00:05:21,680 --> 00:05:26,680
 I couldn't really believe it. I was just surprised.

52
00:05:26,680 --> 00:05:30,680
 Someone told me I should get a job.

53
00:05:30,680 --> 00:05:35,680
 Which I think is apropos because there's a sense that

54
00:05:35,680 --> 00:05:38,680
 spiritual practice is selfish,

55
00:05:38,680 --> 00:05:45,680
 lazy, you know.

56
00:05:45,680 --> 00:05:55,300
 That shout, that experience this morning, actually there's

57
00:05:55,300 --> 00:05:56,680
 much more I could say on that,

58
00:05:56,680 --> 00:06:00,680
 but not that relevant. Let's not go too far afield.

59
00:06:00,680 --> 00:06:09,890
 But there is this sense, which is a shame. It shows a

60
00:06:09,890 --> 00:06:12,680
 misunderstanding of

61
00:06:12,680 --> 00:06:16,850
 what is truly a benefit and where one can be of most

62
00:06:16,850 --> 00:06:17,680
 benefit,

63
00:06:17,680 --> 00:06:21,680
 how one can best benefit the world.

64
00:06:21,680 --> 00:06:27,190
 If everyone were to better themselves, then it's the first

65
00:06:27,190 --> 00:06:27,680
 thing you can say.

66
00:06:27,680 --> 00:06:31,370
 If everyone were to work on themselves, everyone in the

67
00:06:31,370 --> 00:06:31,680
 world,

68
00:06:31,680 --> 00:06:37,680
 then who would need to help anyone else?

69
00:06:37,680 --> 00:06:41,590
 Second thing you could say is that someone who has worked

70
00:06:41,590 --> 00:06:42,680
 on themselves

71
00:06:42,680 --> 00:06:46,680
 is in a far better position to help others.

72
00:06:46,680 --> 00:06:51,980
 If I just think about all the horrible things I did to

73
00:06:51,980 --> 00:06:53,680
 other people

74
00:06:53,680 --> 00:06:58,440
 before I knew anything about meditation, mean things I've

75
00:06:58,440 --> 00:07:01,680
 done,

76
00:07:01,680 --> 00:07:07,680
 unpleasantness I've created, suffering I've caused,

77
00:07:07,680 --> 00:07:10,680
 and people who try to help do this as well.

78
00:07:10,680 --> 00:07:14,680
 You try to help and you just end up confusing the situation

79
00:07:14,680 --> 00:07:19,950
 because you yourself are full of biases and delusions and

80
00:07:19,950 --> 00:07:20,680
 opinions

81
00:07:20,680 --> 00:07:33,680
 and views that are not really based on reality.

82
00:07:33,680 --> 00:07:40,220
 Meditation is actually really the highest duty that we have

83
00:07:40,220 --> 00:07:40,680
.

84
00:07:40,680 --> 00:07:46,680
 It's not an indulgence or laziness.

85
00:07:46,680 --> 00:07:50,680
 Meditation is doing your duty.

86
00:07:50,680 --> 00:07:56,180
 Just like you wouldn't walk into a crowded place if you

87
00:07:56,180 --> 00:07:57,680
 haven't showered,

88
00:07:57,680 --> 00:08:01,090
 you haven't cleaned your body, if you haven't cleaned your

89
00:08:01,090 --> 00:08:04,680
 clothes, washed your clothes.

90
00:08:04,680 --> 00:08:08,680
 You wouldn't go walking into a public place because,

91
00:08:08,680 --> 00:08:15,080
 well, there's a sense that you have a duty to be clean and

92
00:08:15,080 --> 00:08:16,680
 not smell.

93
00:08:16,680 --> 00:08:21,680
 It's unpleasant in this world.

94
00:08:21,680 --> 00:08:28,680
 The same goes on a far more important scale with our mind.

95
00:08:28,680 --> 00:08:33,910
 Though we don't do our duty, we have a duty to clean our

96
00:08:33,910 --> 00:08:34,680
 minds.

97
00:08:34,680 --> 00:08:39,840
 Because we don't do this, we fight and we argue and we

98
00:08:39,840 --> 00:08:40,680
 manipulate each other

99
00:08:40,680 --> 00:08:46,680
 and we cling to each other.

100
00:08:46,680 --> 00:08:49,680
 We're not ready to face the world.

101
00:08:49,680 --> 00:08:57,680
 We're not equipped to live in peace and harmony.

102
00:08:57,680 --> 00:09:00,680
 We're defiled.

103
00:09:00,680 --> 00:09:04,680
 We want to put it in very harsh and sort of negative ways.

104
00:09:04,680 --> 00:09:07,680
 In the very real sense, we are defiled.

105
00:09:07,680 --> 00:09:11,870
 Until we clean ourselves, we shouldn't be allowed to go in

106
00:09:11,870 --> 00:09:12,680
 public.

107
00:09:12,680 --> 00:09:17,680
 We shouldn't try to interact with other people.

108
00:09:17,680 --> 00:09:21,680
 This is why people become monks, go off in the forest,

109
00:09:21,680 --> 00:09:24,680
 because they have a job to do.

110
00:09:24,680 --> 00:09:30,120
 Because they don't want to get a job, because they have a

111
00:09:30,120 --> 00:09:30,680
 job.

112
00:09:30,680 --> 00:09:32,680
 They're doing the most important job.

113
00:09:32,680 --> 00:09:36,360
 This is why we appreciate and welcome people who want to

114
00:09:36,360 --> 00:09:37,680
 come and meditate,

115
00:09:37,680 --> 00:09:41,680
 because we feel they're doing the world a service.

116
00:09:41,680 --> 00:09:45,370
 They're bettering the world just by learning about

117
00:09:45,370 --> 00:09:46,680
 themselves.

118
00:09:46,680 --> 00:09:50,680
 It's like how we say someone who keeps five precepts

119
00:09:50,680 --> 00:09:54,680
 actually is giving a great gift to the world.

120
00:09:54,680 --> 00:09:57,680
 They're not really doing something for themselves.

121
00:09:57,680 --> 00:10:03,680
 They're bestowing freedom, freedom from danger,

122
00:10:03,680 --> 00:10:09,530
 that no being in the universe has reason to fear this

123
00:10:09,530 --> 00:10:11,680
 person.

124
00:10:11,680 --> 00:10:15,680
 When they stop killing and stealing and cheating and lying,

125
00:10:15,680 --> 00:10:20,680
 they free so many beings from fear, all beings actually.

126
00:10:20,680 --> 00:10:43,680
 No being in the universe has reason to fear that person.

127
00:10:43,680 --> 00:10:51,680
 So helping oneself is very important.

128
00:10:51,680 --> 00:10:54,680
 When one looks at another's faults, and as always,

129
00:10:54,680 --> 00:10:58,680
 let's try and find this quote as well.

130
00:10:58,680 --> 00:11:09,680
 [Singing]

131
00:11:09,680 --> 00:11:11,680
 No, that's not the right one.

132
00:11:11,680 --> 00:11:13,680
 I hear it.

133
00:11:13,680 --> 00:11:29,680
 [Singing]

134
00:11:29,680 --> 00:11:33,680
 When a person looks at the faults of others,

135
00:11:33,680 --> 00:11:35,680
 [Singing]

136
00:11:35,680 --> 00:11:39,680
 always perceiving fault.

137
00:11:39,680 --> 00:11:42,680
 Oh, always full of envy?

138
00:11:42,680 --> 00:11:46,680
 No.

139
00:11:46,680 --> 00:11:48,680
 [Singing]

140
00:11:48,680 --> 00:11:53,680
 It's funny that envy is there. It's not envy.

141
00:11:53,680 --> 00:11:57,680
 [Singing]

142
00:11:57,680 --> 00:11:59,680
 Picking on others, really.

143
00:11:59,680 --> 00:12:05,680
 Seeing faults in others.

144
00:12:05,680 --> 00:12:11,680
 [Singing]

145
00:12:11,680 --> 00:12:14,680
 For such a person, asavatasavadhanti,

146
00:12:14,680 --> 00:12:20,680
 such a person, asavas, the taints grow, increase.

147
00:12:20,680 --> 00:12:22,680
 [Singing]

148
00:12:22,680 --> 00:12:30,680
 They're far away from the ending of the defilements.

149
00:12:31,680 --> 00:12:38,680
 [Singing]

150
00:12:38,680 --> 00:12:45,680
 And then we skip to 159.

151
00:12:45,680 --> 00:13:01,680
 [Singing]

152
00:13:01,680 --> 00:13:03,680
 If only you would do, right?

153
00:13:03,680 --> 00:13:08,680
 If you would do, if oneself would do,

154
00:13:08,680 --> 00:13:16,680
 would one teach others?

155
00:13:16,680 --> 00:13:25,680
 [Singing]

156
00:13:25,680 --> 00:13:29,680
 Ah, right.

157
00:13:29,680 --> 00:13:37,680
 [Singing]

158
00:13:37,680 --> 00:13:40,680
 [Singing]

159
00:13:40,680 --> 00:13:43,680
 Well trained oneself, one could then train,

160
00:13:43,680 --> 00:13:47,680
 one should then train others.

161
00:13:47,680 --> 00:13:50,680
 For it is difficult to train oneself.

162
00:13:50,680 --> 00:13:55,680
 [Singing]

163
00:13:55,680 --> 00:13:59,680
 The self is difficult to tame.

164
00:13:59,680 --> 00:14:01,680
 It's easy to give advice to others, right?

165
00:14:01,680 --> 00:14:06,680
 Actually teaching is a lot easier than actually practicing.

166
00:14:06,680 --> 00:14:09,680
 So if people say thank you, to me I say no thank you,

167
00:14:09,680 --> 00:14:12,680
 you're the one who's doing all the work.

168
00:14:12,680 --> 00:14:14,680
 I'm just teaching you.

169
00:14:14,680 --> 00:14:16,680
 I'm just leading you on the path.

170
00:14:16,680 --> 00:14:22,680
 You're the one who has to do the work for yourself.

171
00:14:22,680 --> 00:14:36,680
 Even the Buddha just pointed the way.

172
00:14:36,680 --> 00:14:45,680
 And the last one which is I think probably the most.

173
00:14:45,680 --> 00:14:55,680
 This is the most inspiring.

174
00:14:55,680 --> 00:15:17,680
 [Singing]

175
00:15:17,680 --> 00:15:29,680
 It's not it.

176
00:15:29,680 --> 00:15:40,680
 The self should guard oneself.

177
00:15:40,680 --> 00:15:55,680
 It should examine oneself.

178
00:15:55,680 --> 00:15:59,680
 When one is self guarded and mindful,

179
00:15:59,680 --> 00:16:07,680
 [Singing]

180
00:16:07,680 --> 00:16:09,680
 It's a good name for a monk.

181
00:16:09,680 --> 00:16:12,680
 [Singing]

182
00:16:12,680 --> 00:16:14,680
 The one who guards oneself.

183
00:16:14,680 --> 00:16:18,680
 If you want a good Pali name, there's a good one.

184
00:16:18,680 --> 00:16:23,680
 Self guarded means to guard your senses.

185
00:16:23,680 --> 00:16:27,680
 Guard the eye, the ear, the nose, the tongue, the body,

186
00:16:27,680 --> 00:16:29,680
 and the mind.

187
00:16:29,680 --> 00:16:33,680
 So the six doors, it's a good description of the meditation

188
00:16:33,680 --> 00:16:34,680
 practice

189
00:16:34,680 --> 00:16:41,680
 because all of our experiences come through the six doors.

190
00:16:41,680 --> 00:16:47,680
 And if you get caught up in any one of them,

191
00:16:47,680 --> 00:16:50,680
 it leads to bad habits.

192
00:16:50,680 --> 00:16:53,680
 To put it simply, because it becomes habitual

193
00:16:53,680 --> 00:16:58,680
 and it gets you caught up in it, gets you off track.

194
00:16:58,680 --> 00:17:02,080
 It's caught up in some kind of cycle of addiction or

195
00:17:02,080 --> 00:17:03,680
 aversion

196
00:17:03,680 --> 00:17:09,680
 or conceit or views or whatever.

197
00:17:09,680 --> 00:17:15,680
 And leads to suffering, leads to stress.

198
00:17:15,680 --> 00:17:20,680
 Because you don't see things clearly biased.

199
00:17:20,680 --> 00:17:25,680
 And your bias is create embarrassment and create stress

200
00:17:25,680 --> 00:17:28,680
 and create conflict.

201
00:17:28,680 --> 00:17:38,680
 Create disappointment and expectations.

202
00:17:38,680 --> 00:17:41,680
 So the ninth verses are all about yourself

203
00:17:41,680 --> 00:17:45,680
 because that's where we focus.

204
00:17:45,680 --> 00:17:49,680
 A river is only as pure as its source.

205
00:17:49,680 --> 00:17:53,560
 All of our life and our interactions with the world around

206
00:17:53,560 --> 00:17:53,680
 us,

207
00:17:53,680 --> 00:17:55,680
 it's all dependent on our minds.

208
00:17:55,680 --> 00:17:57,680
 Your state of mind is not pure.

209
00:17:57,680 --> 00:17:59,680
 You can't expect to help others

210
00:17:59,680 --> 00:18:03,680
 or be a good, have a good influence on others

211
00:18:03,680 --> 00:18:09,680
 or have a good relationship with others.

212
00:18:09,680 --> 00:18:10,680
 Work on yourself.

213
00:18:10,680 --> 00:18:11,680
 It's the best thing you can do.

214
00:18:11,680 --> 00:18:14,680
 And in fact, it's the only thing you need to do.

215
00:18:14,680 --> 00:18:16,680
 Everything else comes naturally.

216
00:18:16,680 --> 00:18:20,680
 Once your mind is pure, the more your mind is pure,

217
00:18:20,680 --> 00:18:22,680
 the better your relationship with others,

218
00:18:22,680 --> 00:18:30,680
 your influence on others, your impact on the world.

219
00:18:30,680 --> 00:18:32,680
 And that's why we meditate.

220
00:18:32,680 --> 00:18:34,680
 That's why we work on ourselves.

221
00:18:34,680 --> 00:18:36,680
 That's why we study ourselves.

222
00:18:36,680 --> 00:18:41,680
 That's why we try to free ourselves from suffering.

223
00:18:41,680 --> 00:18:47,680
 You can't help others if you're in pain and stressed

224
00:18:47,680 --> 00:18:49,680
 about your own problems.

225
00:18:49,680 --> 00:18:51,680
 But a person who's truly happy,

226
00:18:51,680 --> 00:18:54,680
 they're a benefit to the world

227
00:18:54,680 --> 00:18:59,680
 and to everyone they come in contact with.

228
00:18:59,680 --> 00:19:01,680
 All right.

229
00:19:01,680 --> 00:19:08,680
 That's the quote for tonight.

230
00:19:08,680 --> 00:19:11,680
 I'm going to post the Hangout if anyone has questions.

231
00:19:11,680 --> 00:19:16,680
 You're welcome to click on the link and come ask.

232
00:19:16,680 --> 00:19:18,680
 I'll hang around for a few minutes

233
00:19:18,680 --> 00:19:34,680
 to see if anyone has questions.

234
00:19:34,680 --> 00:19:36,680
 This is the second broadcast today.

235
00:19:36,680 --> 00:19:38,680
 Today was Second Life Day,

236
00:19:38,680 --> 00:19:59,680
 so I broadcasted in virtual reality.

237
00:19:59,680 --> 00:20:03,680
 Only 20 people can listen to a Second Life talk.

238
00:20:03,680 --> 00:20:09,680
 It's actually a limit placed on the--

239
00:20:09,680 --> 00:20:13,680
 placed by the company.

240
00:20:13,680 --> 00:20:14,680
 It's quite expensive.

241
00:20:14,680 --> 00:20:18,680
 Second Life is quite expensive for people who have land

242
00:20:18,680 --> 00:20:22,680
 like the Buddha Center.

243
00:20:22,680 --> 00:20:29,680
 Stephen, you're back, but you're still-- hello?

244
00:20:29,680 --> 00:20:34,680
 Yep, I hear you.

245
00:20:34,680 --> 00:20:40,680
 Do you have a question for me?

246
00:20:40,680 --> 00:20:45,680
 My name is not--

247
00:20:45,680 --> 00:20:48,680
 Hmm, sorry.

248
00:20:48,680 --> 00:20:50,680
 Are you there? Hello?

249
00:20:50,680 --> 00:20:57,680
 Hi.

250
00:20:57,680 --> 00:21:00,680
 Guadalupe, you're here.

251
00:21:00,680 --> 00:21:03,680
 Yeah, I just--

252
00:21:03,680 --> 00:21:05,680
 We just talked.

253
00:21:05,680 --> 00:21:07,680
 Yeah, I don't have a question.

254
00:21:07,680 --> 00:21:13,680
 I just want to say a comment if you've seen this proper.

255
00:21:13,680 --> 00:21:15,680
 I mean--

256
00:21:15,680 --> 00:21:17,680
 Sure.

257
00:21:17,680 --> 00:21:20,680
 Just because you say something that--

258
00:21:20,680 --> 00:21:25,680
 about how people feel about meditation,

259
00:21:25,680 --> 00:21:32,020
 that they think sometimes that we are kind of wasting our

260
00:21:32,020 --> 00:21:33,680
 time.

261
00:21:33,680 --> 00:21:38,680
 I used to have a mental illness.

262
00:21:38,680 --> 00:21:44,570
 I had depression and anxiety, severe depression and anxiety

263
00:21:44,570 --> 00:21:44,680
.

264
00:21:44,680 --> 00:21:54,190
 And naturally, I went through a paranoia state for one and

265
00:21:54,190 --> 00:21:56,680
 a half months.

266
00:21:56,680 --> 00:22:02,680
 At that time, all my people, all my family freaked out and

267
00:22:02,680 --> 00:22:06,680
 support me.

268
00:22:06,680 --> 00:22:19,520
 But after the crisis passed, they think that it wasn't

269
00:22:19,520 --> 00:22:23,680
 necessary to do more things about it.

270
00:22:23,680 --> 00:22:31,680
 Even though I didn't want to live taking pills.

271
00:22:31,680 --> 00:22:42,680
 And all the time, so I decided to look to--

272
00:22:42,680 --> 00:22:50,830
 not to take pills anymore and start meditating more

273
00:22:50,830 --> 00:22:53,680
 seriously.

274
00:22:53,680 --> 00:23:01,680
 And I think that sometimes we don't--

275
00:23:01,680 --> 00:23:06,680
 we don't should pay attention on what other people said

276
00:23:06,680 --> 00:23:12,680
 about leaving our jobs and stop taking a meditiment.

277
00:23:12,680 --> 00:23:20,200
 If we feel that meditating is going to help us, we should

278
00:23:20,200 --> 00:23:22,680
 just do that.

279
00:23:22,680 --> 00:23:31,680
 And that's how me come in.

280
00:23:31,680 --> 00:23:35,680
 Well, thank you.

281
00:23:35,680 --> 00:23:39,680
 Hey, you're smoking? Who is this guy?

282
00:23:39,680 --> 00:23:41,680
 I'm not convinced.

283
00:23:41,680 --> 00:23:45,610
 Stephen, can you tell us a little bit about yourself? Why

284
00:23:45,610 --> 00:23:49,680
 are you here?

285
00:23:49,680 --> 00:23:53,530
 I don't know who this guy is, but I think smoking on our

286
00:23:53,530 --> 00:23:55,680
 broadcast is not allowed.

287
00:23:55,680 --> 00:24:04,680
 Apologies.

288
00:24:04,680 --> 00:24:08,680
 Do we have rules like that? Is that appropriate?

289
00:24:08,680 --> 00:24:11,680
 Well, yes. I mean, that's not very respectful.

290
00:24:11,680 --> 00:24:20,680
 You have to stop smoking if you want to join our broadcast.

291
00:24:20,680 --> 00:24:22,680
 No smoking, no eating, and that kind of thing.

292
00:24:22,680 --> 00:24:33,680
 Do you have a question, sir?

293
00:24:33,680 --> 00:24:37,580
 The reason I stick around is in case people have questions.

294
00:24:37,580 --> 00:24:41,680
 If nobody has questions, we're going to end the broadcast.

295
00:24:41,680 --> 00:24:42,680
 Thank you.

296
00:24:42,680 --> 00:24:44,680
 Thank you all. Have a good night.

297
00:24:44,680 --> 00:24:45,680
 Good night.

298
00:24:45,680 --> 00:24:46,680
 Thank you.

299
00:24:47,680 --> 00:24:48,680
 Thank you.

