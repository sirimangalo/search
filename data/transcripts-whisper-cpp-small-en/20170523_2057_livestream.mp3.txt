 Okay, good evening everyone.
 Welcome to our evening Dhamma session.
 Another important way of describing the Buddha's teaching.
 We're talking about what is Buddhism.
 One that's not talked about perhaps enough and one that
 actually I think I talk about
 quite a bit.
 I think I've done several videos on it.
 The concept of Appamada.
 Appamada is a word that interestingly many Buddhists or
 quite a few Buddhists don't really
 understand very well.
 We end up talking about it with Buddhists giving talks on
 it and find that people who
 have been Buddhists all their life don't really have a good
 sense of what it means.
 It's a hard word to translate into English.
 We have some translations but none of them are quite right.
 So we have to... this is why it's useful to know the e
tymology of the word.
 Appamada comes from pamada.
 It's the opposite of pamada.
 So it's actually a negative.
 Pamada is something. Appamada is not being pamada.
 Pa is a prefix that means that strengthens or gives a
 special meaning to words.
 And we'll see that that's what it does in this case.
 Madha comes from mud.
 Mud is a root that means to be drunk, to be intoxicated.
 And in fact we find the word pamada used in just this way
 in the fifth precept.
 So Buddhists keep five precepts.
 We don't kill, we don't steal, we don't commit sexual
 misconduct, we don't lie and we don't
 take drugs or alcohol.
 That's what all Buddhist monks are not monks.
 Those are the basic commandments if you will.
 They're not commandments, they are precepts that we take on
 ourselves and say this is
 what we train as Buddhists.
 If you don't train in these things, you can't really very
 well be called a Buddhist.
 For meditators, those of you here, you have to give eight
 precepts.
 Not to kill, not to steal, not to have any romantic or
 sexual activity, not to lie and
 not to take drugs and alcohol, to only eat in the morning,
 to have no entertainment or
 beautification and to sleep on the floor or on a simple bed
.
 As novices, novices take an extra precept.
 They don't use money or valuables and Buddhist monks have
 many, many more.
 A little background, but the fifth precept for all these
 groups is not to take drugs
 or alcohol and the precept explains when you recite it, you
 give an explanation as to why
 because these are things they lead to, madja, they are
 intoxicating.
 Bhamadatana, Bhamadatana, they are a cause for Bhamada.
 So actually I think it has both of them, madja is the root
 mud which means to become drunk
 or intoxicated and that intoxication, the physical
 intoxication can lead to Bhamada
 which means a sort of mental intoxication or that's where
 it comes from.
 But the word Bhamada is used by the Buddha in many
 different places.
 It's something that I think isn't given nearly the
 importance that it deserves.
 It's used poetically, the Buddha said, "Yopu," he likened
 Bhamada to being covered over by
 a cloud like the light of the moon at night when it's
 covered over by the cloud.
 He said, "Yopu bei pama jitva pacha sona pama jiti,"
 whoever in the past was Bhamada and
 in the present becomes or afterwards later on becomes apam
ada or not pamaada.
 Such a person lights up the earth just as the moon coming
 out from behind a cloud.
 This is how it can feel sometimes, you feel like you were
 asleep and now you've woken
 up or in the dark and now there is light.
 Meditation is I think a real wake up call especially when
 you first start out.
 It's quite shocking to realize how misguided you've been,
 misguided we've been in our
 mind.
 You see it's kind of amazing how wrong we were in our
 thinking about what leads to happiness
 and what leads to contentment, how blind we were to those
 things that lead to stress and
 suffering.
 So this is what sobering up does, it's like a slap in the
 face or a splash of cold water
 or the moon coming out from behind the cloud.
 It was used in another story, even stronger language or
 much stronger language, this story
 of these two queens Samavati and Mangandia.
 One was a very very bitter and nasty sort of person and the
 other was a very pure and well
 cultivated or highly spiritual individual.
 The spiritual queen was a very devout follower of the
 Buddha, she saw the greatness of him
 and had her servant woman, I've talked about Samavati
 before in the Dhammapada briefly,
 but she'd have her servant woman go to learn from the
 Buddha and come back and teach them
 all.
 And Mangandia was very jealous of her and tried her best to
 destroy Samavati and in
 the end had her relatives lock her up in her room and burn
 her and all of her maidens,
 burn down the queen's quarters and they all died.
 When the king found out he killed Mangandia, had her
 tortured and all of her relatives
 tortured and killed.
 It wasn't a very nice king either.
 And so the monks were talking about this, talking about
 both of them dying and how they
 died and how it's interesting that one of them was so pure
 and one of them was so evil
 and yet they both suffered terribly.
 And the Buddha came in and asked them what are you guys
 talking about and they said oh
 we're talking about these two queens that died and the
 Buddha differentiated for them
 he said Samavati didn't die basically is what he said.
 Mangandia was already dead but Samavati didn't die and he
 said, "Apamadu amatapadang pamadu
 machunopadang."
 "Apamatana miyantiyay pamatayatamata."
 Which means pamada is the path to death.
 And apamada is the path of the deathless.
 Those who are whoever is, whoever is apamada never dies and
 those who are pamada are already
 dead.
 So little explanation, in Buddhism really we are born and
 die every moment.
 The only death that we know is the death of experiences.
 Our experiences die, die, die every moment.
 Just after they're born they die.
 That's a real death.
 The death that we know of this person died, that person
 died.
 It's really only conventional.
 And because our momentary experiences, it's not really a
 death.
 It really is but it's just one thing and then there's right
 away another one.
 So doesn't it all seem like a death?
 Because of that he said, "Really, in any meaningful sense,
 the wise never die."
 What about the unwise?
 So then for the unwise, what's up with them?
 Why are they different?
 Well, they're already dead.
 And again, this is how we feel before we gain this clarity
 of mind.
 Like we were dead.
 It was like our life had no meaning in many ways, in many
 cases.
 Maybe it could be very successful in the world.
 Maybe even be king of the world.
 I was just thinking of another quote the Buddha said with
 all these quotes in my head.
 "Pata bhi ai karajena sagasagam nenoa sabaloka di pachayena
 sota pati palangwaram."
 Better than king of the world, king of the earth, the one
 king of the earth or of heaven
 or of earthy cities.
 Better than being the ruler of the whole world is the
 attainment of sota pana, sota pati
 palangwaram, the fruition of sota pana.
 It's becoming enlightened, seeing nirvana, even if you're a
 poor farmer.
 It's of much greater wealth.
 Because dead people can't enjoy things.
 Dead people can't really be happy, right?
 So this comparison of people being dead, or like zombies,
 you know, you hear, we talk about
 this how we're like zombie workers.
 We go into work nine to five and we come home and nowadays
 we're on the computer like zombies.
 All it takes is a moment of mindfulness and you wake up,
 suddenly you're living.
 The most important place and the reason why we know that
 Appamada is so important, such
 an important concept in Buddhism and one that you should
 all remember, something that you
 may not have ever heard, unless you've been keeping up with
 me, one that we should all
 remember.
 Why we know this is because it was the last words of the
 Buddha.
 We have recorded what are supposed to be the very last
 words that the Buddha ever uttered.
 He'd been teaching, he'd left home at 29 and practiced for
 six years torturing himself
 and realized that was no good and went and sat under the
 Bodhi tree and found the middle
 way, spent 45 years tirelessly teaching, sleeping three
 hours a night or lying down mindfully
 whether he actually slept, I'm not sure.
 And for 45 years he taught 45 books.
 In the Thai version there are 45 books so they say one year
 a book, a book a year.
 That's what it works out to.
 But these are not small books, it's a huge number of
 teachings that we have recorded.
 And then when he was lying at his deathbed, I mean, could
 you imagine how important those
 last words must have been and how much preparation or not
 preparation, but how much significance
 went into choosing his last words, what he was going to
 leave his followers with.
 And here's what he said.
 He said, "Andatani bhikave amantayam yu."
 I tell you, oh monks.
 Here then monks I tell you, amantayami, I instruct you, I
 exhort you maybe.
 Vayadhammasankara, all sankaras, all formations fade away,
 are of a nature to fade away.
 Don't cling to anything, right?
 Nothing is worth clinging to.
 Vayadhammasankara, everything fades.
 Apamadina sampadita, these are the last words.
 This is what we always remember, apamadina sampadita.
 Strive on or work to fulfill, work to become full, become
 accomplished in apamada.
 That was it.
 The commentary says something really interesting, not here
 but elsewhere.
 It says, "The whole of the Buddhist teaching can be
 summarized as the path of apamada."
 So there you go.
 You want a good explanation of Buddhism, the practice of
 Buddhism.
 So we have to differentiate between Buddhism and the goal
 or the intention.
 People often focus very much on the goal of Buddhism.
 So what is Buddhism?
 It's about reaching nibbana we think, right?
 Or it's about gaining wisdom or something like that.
 But Buddhism is not that.
 Buddhism is the practice which leads to those things.
 So we talk about Buddhism and this is important because it
's really the most important thing.
 Focusing on enlightenment, nibbana, even insight or wisdom.
 We talk about this as we pass on the meditation.
 Focusing on those things is not useful at all.
 It's putting the cart before the horse.
 It's counting your chickens before they hatch maybe.
 It's looking at the product, the result.
 If you focus too much on the result, you don't actually do
 the work.
 So Buddhism is much more about apamada than it is about say
 wisdom or enlightenment.
 Buddhism is the practice which leads to enlightenment,
 which leads to wisdom.
 And that's important.
 As meditators, you shouldn't focus on wisdom.
 You shouldn't focus on nibbana or enlightenment.
 You shouldn't set it, maybe set it as a goal, but don't fix
ate on it.
 You should fixate on apamada.
 It's the only one thing.
 There are many other things that are useful and morality
 and so on.
 But apamada is the most important.
 It's the key.
 The commentary says and the Buddha appears to have said, it
's the whole of the Buddha's
 teaching.
 It's going to be the core.
 Sakalampi Hitte Pitakang, Budhu Vatchanang, Aharitva, the
 whole of the three pitakas.
 So if you look on that shelf, I think some of them are
 there.
 Or on this shelf, some of them are here as well.
 The teaching of the Buddha is, as I said, 45 volumes, but
 it's organized into three
 pitakas.
 The Vinaya Pitaka deals with morality.
 So the things that we try to avoid basically.
 The Suta Pitaka deals with concentration.
 So it's very much about meditation practice.
 It's about focusing your mind on many different things,
 ideally ultimately the present moment.
 And the third one is wisdom.
 The Abhidhamma deals with wisdom.
 So it talks about the nature of reality as it's experienced
 by meditators and various
 permutations and relationships between reality.
 So it's very much about the sort of wisdom that we gain
 from meditation practice.
 But the commentary says all of these, all three of these,
 and all 45 volumes or how
 many volumes, all the thousands of teachings of the Buddha
 can be summarized by the path
 of Apamada.
 So what the heck is it?
 I purposely not translated it because as I said it's
 difficult, but you get the idea
 that maybe some sort of sobriety, the concept of sobriety
 being sober, being unintoxicated
 is really what it means.
 Probably my favorite translation would be unintoxicated, un
intoxication, non-intoxication.
 Because again it's Apamada, it's the not something.
 And so we're talking not about physical intoxication, we're
 talking about mental intoxication.
 Like when you're addicted to something, you're not thinking
 clearly, right?
 You notice how when you want to eat something, you want to
 get something, sometimes you'll
 do crazy things for it.
 I mean drug addicts certainly will, but we as well, we lose
 our mindfulness.
 When you're angry of course, right?
 If you've ever been angry and you stub your toe on
 something and get really angry, you
 start kicking the thing that you stub your toe on, yelling
 at it, cursing it, kicking
 it, pushing it, and totally irrational.
 Or how we fight with each other, right?
 We hurt those that we love, we lash out, purposefully
 trying to harm other people, right?
 It's totally irrational, harmful, it destroys friendships.
 We're like drunk people wandering around and delusion,
 delusion is like blindness, delusion
 is like all these wrong views we have.
 We were talking today about people who have delusion, all
 sorts of other religions, I
 should probably be afraid to say it, but I'm not.
 All these other religions that teach such craziness, God,
 heaven, well heaven's not
 that crazy, but believe X and you'll go to heaven, right?
 Do X and you'll go to heaven like some magic, like God's
 watching and he says okay, that
 person.
 Or like there's a guy at the gate when you get to heaven
 and he says okay, check, you
 did X.
 You blew up a bunch of kids at a concert in the UK, you can
 get into heaven.
 There was another bomb for those of you who are here and
 don't know what's going on in
 the world.
 Killed a bunch of kids, really, really, really evil.
 People are taught or brainwashed into thinking that somehow
 that leads to heaven.
 Total blindness, worse than blindness, I mean it's just the
 worst evil, but based on blindness.
 Blindness will let you do anything.
 You think this is the exit, you run into a wall.
 You think it's solid ground, you fall into a pit.
 You think there's no one there, you run into someone.
 You hurt them, you hurt yourself.
 You've ever been in a dark room, right?
 A pitch black room?
 Just try navigating it without hurting yourself or someone
 else.
 That's what wrong view does.
 Wrong view is the worst.
 Wrong view is what leads us to do all sorts of wrong things
.
 The Buddha said there's no worse evil than fixed wrong view
.
 Nyatamichaditi, which means wrong view that is stable
 certain.
 Wrong view is okay if you're willing to challenge your
 views, but you have wrong view and you're
 stuck on it.
 It's nothing worse.
 So what does it mean?
 What is upamada?
 Well, I mean we have this understanding of it as being non-
intoxication.
 But what does that mean for us, right?
 None of you are taking drugs or alcohol, I hope.
 I had one meditator who lied actually on the application
 just so we would let him come.
 And halfway through the course he told me that he was on
 medication for a mental illness.
 I said, "Well, we can't really do the course in that case."
 Because it's clear, I mean I've been challenged by this and
 actually yelled at because of
 this, claiming that somehow it prevents you from
 progressing.
 But it does.
 I mean I've seen clearly this meditator the time he was
 here had no conditions.
 None of the struggles that you're going through.
 And if he doesn't have any of the struggles, how could he
 possibly gain the benefit?
 It has to be natural.
 You have to let your mind settle really into a natural
 state.
 If you're constantly putting, you know it's not just the
 drugs, it's the intention to
 take them.
 It's the intention to avoid.
 Anyway, none of you are taking drugs, I hope.
 If you are, you've lied and you should tell me because
 lying is a bad thing.
 But there's a deeper intoxication.
 And what does it mean to be non-intoxicated?
 Well, we've got clear advice on this.
 So in detail we've got four things.
 The Buddha gave a talk and he said, "Abhi apa no sadassato,
 ajatang su samahito, abhi
 ca vinayesu kang apa madho ji ujiti."
 Which means, "Abhi apa no, bi apa no" is ill will.
 So "Abhi apa no" means someone who is without anger.
 Give up all your vengeance.
 This is a part of being unintoxicated, right?
 Something we don't talk about maybe enough in this course
 and in this center is loving-kindness.
 It's useful sometimes.
 I wouldn't spend hours on it, but from time to time, after
 you meditate, before you meditate,
 to settle your mind and to remember those people who you
 maybe have problems with, who
 are you angry at, or who are angry at you, who have hurt
 you, or who you've hurt, and
 wish them well.
 May they be happy.
 May they be free from suffering.
 May they find peace.
 Even those who haven't, think about all the people you love
 or all the people in this
 building, right?
 Think about the other people, the person who slams the door
 next to you or uses the washroom
 for too long or so on, or just who's doing such a great job
 meditating out of appreciation
 to them, wish for them to be happy.
 You know, spend some time each day in that.
 I just read about this.
 This is, I was just reading today a monk, Sri Lankan monk,
 a very wonderful and famous
 monk talking about reminding monks, "This is a great thing
 for monks because we get,
 we receive food from the lay people and the question is,
 you know, how can we consider
 ourselves to be worthy of it?"
 And one very important way is to wish them well, to have
 loving kindness for them because
 it, not because it actually makes our wishes come true
 necessarily, but because it cultivates
 our goodness in ourselves and by cultivating love for them
 and kindness, we are cultivating
 goodness.
 And by cultivating goodness, we become worthy of the food.
 So all of you are being fed here.
 The food that you're eating is free.
 So you should wish good thoughts for those people who bring
 the food as well.
 Sadāsattva number two, the most important one, always
 mindful.
 The other explanation, in another place, I believe it was
 the Buddha who said, the Buddha
 or the commentaries, Satya-vipoaso-apamādoti-wucitti.
 To never be without mindfulness, this is what it means to
 be appamada.
 And my teacher makes this connection very, very explicit.
 He says, "Mindfulness is appamada."
 So honestly, if you want the proper translation of the word
 appamada, it's not a literal translation,
 but what it really means in Buddhism is mindfulness.
 And the commentary says the same thing.
 This is the orthodox interpretation of what appamada is.
 It's just another word for mindfulness.
 So in fact, even though we don't hear enough about appamada
, we hear more than enough about mindfulness,
 which gives us much more encouragement that mindfulness
 really is the core of the Buddha's teaching.
 Mindfulness is the practice of in that moment grasping the
 object as it is, or it's the
 act of reminding yourself so that you see an object as it
 is without judging or so on.
 And so what you're doing is really the core of the Buddha's
 teaching.
 All of the Buddha's teaching, you're doubting about or
 confused about what actually the Buddha
 taught and worried that maybe you don't know some important
 things about Buddhism, well,
 rest assured by all accounts, mindfulness is the core of
 what the Buddha taught.
 That's mindfulness.
 Appamada is being always mindful.
 So we're not there yet.
 Only an arahant is really appamada.
 What it means to be appamada is to finally and truly live
 mindfully.
 Ajatang su samahito, to have a, to be composed internally,
 this refers to concentration,
 to be focused.
 Mindfulness requires focus.
 This is why we do formal meditation.
 It's very possible to be mindful in daily life, but you
 need good focus.
 So whether it's in your daily life or whether it's sitting
 on the mat, watching your stomach
 rise and fall, mindfulness is, or appamada requires focus,
 requires you to work, requires
 you to be vigilant.
 And finally, abhijo vinayesikam.
 And my teacher is explaining this, Ajantang explaining this
.
 It's interesting because this means working or training to
 overcome or to root out or
 to get away from, to free yourself from greed or desire.
 And he points out the difference here with anger, you're
 just supposed to give it up,
 to just not be angry.
 With greed, it's a little bit different.
 Greed is something that you have to focus on.
 It's something you have to train.
 And this sort of goes along with a commentary that says
 anger is quick to change.
 Greed is slow to change.
 Craving is really what it's at, right?
 We get angry because we don't get what we want or because
 we get what we don't want.
 But what is it that drives us?
 It's not really anger, anger, not for the most part.
 It's desire.
 What is it at the root is we want to be happy, we want to
 be pleased, we want to be contented,
 we want to be satisfied.
 We want and we want many different things.
 But it's our wants that we have to train out of.
 They're things that are much more complicated or involved
 to get rid of craving.
 But anyway, ultimately it comes down to getting rid of
 greed and anger.
 Through mindfulness, and here the Buddha doesn't mention
 delusion because mindfulness is the
 opposite of delusion.
 This practice, Apamada, is the giving up of delusion.
 When you do that, you attack, you root out greed and anger,
 and you become focused and
 concentrated until your mind becomes so strong that you
 free yourself and become liberated.
 There you go.
 There's a half an hour talk on Apamada.
 I know I've given many of these talks, but I think there's
 some new stuff in there as
 well.
 So thank you all for tuning in.
 Thank you for coming out.
 And the meditator's here.
 You can go back to meditate.
 Okay, I've got the meditation side up, so if it stays up I
 can answer some of the questions
 on there.
 There's a whole bunch.
 How do Buddhist teachings overcome neuroscientific facts?
 Nothing relates to modulation and dopamine levels.
 Liking correlates to modulations in the subcortical
 structures of the brain.
 Well, we don't really.
 I mean, how do neuroscientists explain the mind?
 They don't really.
 They try to.
 Well, they don't really try to.
 Honestly, anyone who knows who's really actually a neuro
scientist, I think for the most part,
 doesn't claim to know very much about consciousness.
 Doesn't claim to have a handle on what consciousness is.
 They certainly haven't learned how the brain creates
 consciousness, which is what they
 think.
 But it's the hard problem.
 Consciousness is the hard problem.
 So I mean, definitely there is a connection between what we
 call the brain, which is physical
 systems, or patterns, we might say, that we can observe and
 that we can measure.
 The relationship between that and the mind.
 I mean, it's possible to alter the physical and have the
 mind be altered as well.
 And here's where it gets sketchy, that neuroscientists
 wouldn't certainly agree, for the most part,
 or many of them wouldn't agree, that it's also possible for
 the mind to leave the brain,
 in a sense.
 Not exactly, but for there to be a mental experience that
 is free from the confines
 and the control of the brain.
 Out of body experiences, near-death experiences, where the
 person who has them says it is completely
 unlike any ordinary experience.
 It's clear, it's perfect, it's more alive than they've ever
 been.
 I never had such an experience, except in my dreams.
 I think when I sleep sometimes, I leave my body.
 A lot of people talk about this.
 And I had this experience when I was young, where it was a
 little bit different, where
 I saw something that hadn't yet happened.
 Left my body and saw something that hadn't yet happened.
 But a lot of, many people talk about these experiences.
 A lot of body experiences may be not so strong, but when
 the brain actually dies, when the
 brain actually stops functioning, people have talked about
 having vertical experiences,
 where they're able to experience the world, even seeing,
 hearing, even smelling perhaps,
 but outside of having nothing to do with the brain.
 So when it comes down to Buddhism, we don't have a sense of
 being a soul.
 So the idea that an experience, a mental experience, could
 be created or at the very least conditioned
 by the physical is fine.
 But it's also the case that a mental experience can affect
 the physical.
 And it's not a one-to-one thing, it's not an equivalence.
 How it actually works is much more complicated.
 But Buddhism is very much open to all of this.
 The point I'd like to make, I suppose, is that liking is
 not subcortical structure,
 the modulations of the subcortical structure of the brain.
 Wanting isn't modulation in dopamine levels.
 Are they related?
 Yes, they're related.
 Are they the same thing?
 No, they're completely different things.
 One is physical, one is mental.
 If you can't get that straight, I think you've got a bit of
 a problem.
 Certainly a problem with the Buddhist.
 So hopefully that helps.
 I'm sure it doesn't satisfy everyone.
 People have their views.
 Okay, here's one about someone who can't deal with the loss
 of a cat, I think, a therapist
 pet, a kiddie.
 A person who was a soldier and returned from Iraq, I think
 a soldier.
 Any advice on meditation?
 Well, I don't think this is one of those questions where I
 would say, "You just have to read
 my booklet.
 And if you'd like, you're welcome to do a course with me,"
 assuming you're not taking
 medication for your stress and PTSD or whatever.
 You can do a online course.
 We have online courses.
 How do bikus deal with nocturnal emissions?
 It's actually an exception.
 It's not a breach of the monastic precepts.
 It's not something ... How you deal with it is be mindful.
 If you're mindful before your sleep, it's supposed to help.
 If you fall asleep on mindfully, dream lustful or whatever.
 Mindfulness before you sleep clears out a lot of that.
 But some of it's just physical and has to do with memories
 and past stuff.
 It's not something you have to be worried about.
 Something to work out.
 Sanjali palasa.
 Sanjali palasa qualifies as a kusala kamma.
 Sanjali palasa.
 No, Sanjali palasa is enough.
 That's a kusala.
 Jitta is when you think, when you have a thought.
 Like if I get angry, that's sanya.
 If I think about, if I then think, boy that person makes me
 mad or boy that person is
 a real jerk, that's jitta vipalaasa.
 That's such a bad person.
 Then if I get the view, then that's diti vipalaasa.
 Jitta vipalaasa would be that person deserves to die.
 That's just a thought.
 But then if you really think to yourself, yes, that's right
, that person, then that's diti vipalaasa.
 But sanya vipalaasa is just getting angry or greedy or so
 on.
 So that's already a kusala.
 If tanha appears in the mind, does it always lead to upad
ana?
 Can the meditator stop it at tanha by being mindful?
 I don't know.
 I can't remember.
 It has gotten a lot easier to abstain from any kinds of
 sensual music.
 Seems to be the hardest for me.
 Any techniques meant to help this?
 I mean meditation, vigilance.
 Eventually you see that music is just another attachment.
 It's really kind of silly.
 Here's a question about a different meditation practice.
 I'm just going to skip that one and you should read my
 booklet.
 Because these answers, questions are unfortunately really
 only about our tradition.
 Healing insects.
 Find it difficult to comprehend reason to salvage, for
 example, moth.
 It's not about salvaging, it's just about not killing.
 Advice or not cause suffering really advice but not as good
 as avoiding anyway.
 It's about not creating cruelty in your mind.
 If you kill an animal, it's cruel.
 That animal didn't want to die.
 So doing that is considered to be cruelty.
 There's nothing particularly special about killing.
 It's just a good fence, a good line in the sand.
 Of course torturing beings is really bad as well.
 Pulling the wings off of flies, that's pretty awful.
 But killing is just something easily identifiable as pretty
 awful.
 Is there a point where there is no progress but rather just
 constantly observing reality?
 If you mean constantly being mindful, then that's the end.
 There's no progress after that.
 [silence]
 Reality cannot be defined and separated from imagination.
 They're just words but concepts in reality are two very
 different things.
 One concept doesn't exist, like a cat doesn't exist, but
 the thought about the cat exists.
 The thought exists.
 The conception in the mind exists, but the cat itself doesn
't exist.
 Not from a point of view of experience.
 So apart from that I'm not quite sure what you're getting
 into.
 In which monastery you have studied and who is your teacher
 because I want to become Buddhist monk.
 You become a mentor to become monk.
 Thank you for your work.
 Yeah, unless you live in Canada or can get a long-term
 Canadian visa, there's not much I can do for you.
 But I was ordained in Thailand, Northern Thailand.
 My teacher's now very old and I wouldn't necessarily
 recommend going to his monastery to become a foreign monk
 because you wouldn't have much to do with him.
 And I can't vouch for the system set up there.
 There's no self, how can we have free will? Who said there
's no self?
 We have free will, but didn't go into much detail.
 Do we have free will? I don't know.
 Free will, free will.
 Yeah, I don't really know. I think it's outside of the
 realm of what's real.
 Reality is here now. It's very much conditioned by very
 many different things.
 In this moment we have, is there a function by which the
 future is completely on,
 completely set in stone, out of our control? I don't know.
 There seems to be a free won't, what I believe it was.
 Yeah, these guys did this experiment. I think it wasn't at
 Locke who said there's a free won't.
 Anyway, speculative, not really interested.
 No objective basis for morality or moral obligations.
 Usually they say the Hume's famous argument, one cannot
 drive an ought from an is.
 Yeah, well that's a very, that's this impersonal outlook.
 Buddhism challenges that.
 Suffering exists. There is an intrinsic, so I talked about
 this a few days ago or a couple of days ago.
 Buddhism seems to make the claim that suffering and liking
 and disliking and happiness,
 these are all a part of reality. And yet these are aughts
 and these are very much caught up in aught.
 In the sense that suffering by its very definition, by
 suffering I don't mean just pain,
 I mean a painful state or an unpleasant state.
 Well, it's not even so much that it exists, it's that it is
 perceived thus.
 And so the mind is in a state where it perceives certain
 things as suffering,
 perceives certain things as pleasant.
 And so it has, it's full of ought, in the sense of ought to
 be happy and ought not to be unhappy.
 And so really all the Buddha did was point out that if you
 want to be happy, if happiness is what you ought to be,
 then you should stop doing all these things that make you
 unhappy.
 And he pointed out that immorality makes you unhappy.
 Now I think it's true that once you become enlightened, you
 can say that there only is is, there is no more ought.
 The mind is no longer caught up with ought to be happy and
 ought to not be not happy.
 So the idea of morality doesn't really have much weight on
 that person.
 By the same token they're not able to be immoral because
 immorality would lead to unhappiness or it would be based
 on things that one has come to see, clearly see are useless
.
 One has no ability to get angry or greedy or so.
 So understand like eventually the meditator comes to this
 is, but because we're so full of ought, the Buddha was just
 pointing out.
 And he does it in a fairly laid back sort of way, like he's
 not really trying to say thou shalt not this or that.
 He's just pointing out, look, if you want to be happy, stop
, stop hurting yourself.
 That's what morality is for. That's what all of the
 Buddhist teams are really.
 And we're done. There you go. There's a bunch of questions.
 I'm not even going to look and see what's going on YouTube.
 How are we doing? Hello, everyone in Second Life. Looks
 like you're having a good chat here.
 Thank you all for coming out. Have a good night.
 Thank you.
 Thank you.
