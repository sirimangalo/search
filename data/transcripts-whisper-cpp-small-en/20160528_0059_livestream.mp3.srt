1
00:00:00,000 --> 00:00:16,640
 Good evening everyone.

2
00:00:16,640 --> 00:00:27,600
 We're broadcasting live May 27th.

3
00:00:27,600 --> 00:00:35,080
 Today's quote is about the Buddha, about how radiant and

4
00:00:35,080 --> 00:00:41,160
 clear is his complexion.

5
00:00:41,160 --> 00:00:47,600
 Just as a trinket of red gold shines and glitters, so too

6
00:00:47,600 --> 00:00:51,160
 the good Godama's senses are calmed,

7
00:00:51,160 --> 00:00:59,960
 and his complexion is clear and radiant.

8
00:00:59,960 --> 00:01:09,600
 So in and of itself it's a praise of the Buddha.

9
00:01:09,600 --> 00:01:14,740
 The most obvious benefit of this quote is to encourage

10
00:01:14,740 --> 00:01:17,720
 reverence for the Buddha, which

11
00:01:17,720 --> 00:01:24,320
 is good in and of itself, it's good to revere great people,

12
00:01:24,320 --> 00:01:25,600
 it's good to respect them, it's

13
00:01:25,600 --> 00:01:29,900
 good even to potentially bow down before the Buddha, which

14
00:01:29,900 --> 00:01:32,240
 is a common thing for Buddhists

15
00:01:32,240 --> 00:01:33,240
 to do.

16
00:01:33,240 --> 00:01:35,080
 These are good things.

17
00:01:35,080 --> 00:01:40,720
 They cultivate humility, respect, reverence, appreciation.

18
00:01:40,720 --> 00:01:47,870
 They help you take the practice, the teaching seriously,

19
00:01:47,870 --> 00:01:51,680
 when you think about the Buddha

20
00:01:51,680 --> 00:01:54,400
 and how wonderful he was.

21
00:01:54,400 --> 00:02:00,800
 It's a good thing, good for your practice.

22
00:02:00,800 --> 00:02:07,310
 I think what's more interesting is why, why is the Buddha

23
00:02:07,310 --> 00:02:09,000
 so radiant?

24
00:02:09,000 --> 00:02:13,490
 What is it about a Buddha as opposed to another religious

25
00:02:13,490 --> 00:02:16,080
 teacher that we would argue or say

26
00:02:16,080 --> 00:02:20,280
 or claim makes him more radiant?

27
00:02:20,280 --> 00:02:24,520
 Or what makes him special, what makes him radiant more than

28
00:02:24,520 --> 00:02:26,200
 an ordinary person?

29
00:02:26,200 --> 00:02:28,780
 And that we find in the Buddha's teaching, and that's more

30
00:02:28,780 --> 00:02:30,960
 interesting because then it

31
00:02:30,960 --> 00:02:34,200
 is something that we can actually emulate.

32
00:02:34,200 --> 00:02:43,730
 We emulate the practices and the behaviors and the mind

33
00:02:43,730 --> 00:02:50,260
 states that lead one to be radiant.

34
00:02:50,260 --> 00:02:54,580
 That's of deeper benefit than simply praising the Buddha,

35
00:02:54,580 --> 00:02:56,880
 because of course that's quite

36
00:02:56,880 --> 00:02:58,080
 limited.

37
00:02:58,080 --> 00:03:01,930
 Some people, that's all they do is rather than working to

38
00:03:01,930 --> 00:03:04,280
 better themselves, they praise

39
00:03:04,280 --> 00:03:07,680
 people who are better than themselves.

40
00:03:07,680 --> 00:03:11,160
 Not even in religion, this is a common thing in the world.

41
00:03:11,160 --> 00:03:14,660
 People use it as a defense mechanism when someone is great,

42
00:03:14,660 --> 00:03:16,400
 when someone is exceptional

43
00:03:16,400 --> 00:03:19,120
 at something.

44
00:03:19,120 --> 00:03:23,480
 Use it as a defense mechanism to praise them.

45
00:03:23,480 --> 00:03:28,380
 It is a way of avoiding actually emulating them or keeping

46
00:03:28,380 --> 00:03:29,800
 up with them.

47
00:03:29,800 --> 00:03:36,520
 "Oh you're such a good person, why?"

48
00:03:36,520 --> 00:03:40,830
 What that means is you're not going to become a good person

49
00:03:40,830 --> 00:03:41,120
.

50
00:03:41,120 --> 00:03:46,390
 That's actually the thing, "Oh you're so nice, so wonderful

51
00:03:46,390 --> 00:03:46,880
."

52
00:03:46,880 --> 00:03:53,250
 Actually saying, "I have no intention of becoming such a

53
00:03:53,250 --> 00:03:55,200
 nice person."

54
00:03:55,200 --> 00:03:57,560
 I find it remarkable.

55
00:03:57,560 --> 00:04:01,970
 Sometimes we maybe wish we could be like that person, but

56
00:04:01,970 --> 00:04:04,280
 the idea of praise is limited

57
00:04:04,280 --> 00:04:07,990
 and limiting and can be potentially limiting if we rely

58
00:04:07,990 --> 00:04:10,640
 upon praise of the Buddha, we worship

59
00:04:10,640 --> 00:04:12,840
 the Buddha, he's so great.

60
00:04:12,840 --> 00:04:15,990
 When people say things like, "You can't possibly see Nibb

61
00:04:15,990 --> 00:04:18,040
ana yourself, Nibbana is not something

62
00:04:18,040 --> 00:04:19,840
 a human being can..."

63
00:04:19,840 --> 00:04:25,800
 I had one man tell me, "Human beings can't attain Nibbana."

64
00:04:25,800 --> 00:04:36,850
 He said, "You Westerners, you look at the moon and you

65
00:04:36,850 --> 00:04:41,440
 think to go to the moon."

66
00:04:41,440 --> 00:04:48,440
 And I said, "Well we did actually go to the moon."

67
00:04:48,440 --> 00:05:01,280
 The idea is to emulate, not simply revere the Buddha,

68
00:05:01,280 --> 00:05:05,120
 emulate the enlightened ones.

69
00:05:05,120 --> 00:05:08,030
 And the Buddha said, "If you want to respect the Buddha,

70
00:05:08,030 --> 00:05:09,720
 what's the greatest respect is

71
00:05:09,720 --> 00:05:11,800
 to practice his teachings."

72
00:05:11,800 --> 00:05:16,530
 So what are these great teachings that lead one to be

73
00:05:16,530 --> 00:05:17,640
 radiant?

74
00:05:17,640 --> 00:05:21,880
 The one that's most directly related to radiance and to

75
00:05:21,880 --> 00:05:24,760
 this clear complexion is staying in

76
00:05:24,760 --> 00:05:25,760
 the present moment.

77
00:05:25,760 --> 00:05:30,590
 Of course that describes very much the core practice of

78
00:05:30,590 --> 00:05:34,560
 insight meditation, but this description

79
00:05:34,560 --> 00:05:40,560
 of being rooted in the present moment.

80
00:05:40,560 --> 00:05:48,540
 And he uses language relating to plants and how they're

81
00:05:48,540 --> 00:05:50,560
 rooted in the ground.

82
00:05:50,560 --> 00:05:52,880
 So he talks about grass.

83
00:05:52,880 --> 00:05:59,780
 When you cut grass, it gets cut off from its source of

84
00:05:59,780 --> 00:06:04,240
 energy and withers up and dies.

85
00:06:04,240 --> 00:06:08,420
 And likewise, this is, I'm very apt analogy because

86
00:06:08,420 --> 00:06:11,200
 likewise the mind that's caught up

87
00:06:11,200 --> 00:06:16,840
 in the past or the future gets cut off from reality.

88
00:06:16,840 --> 00:06:22,840
 It loses its energy and it withers up, dries up.

89
00:06:22,840 --> 00:06:32,830
 So people who are of poor complexion, a sense of aged and

90
00:06:32,830 --> 00:06:40,440
 wrinkled and so on, beyond normal.

91
00:06:40,440 --> 00:06:49,000
 People whose bodies become sick and chronically unhealthy.

92
00:06:49,000 --> 00:06:58,240
 It's for many causes, but one cause is not being present.

93
00:06:58,240 --> 00:07:01,880
 Buddha was asked, "Why, how is it that these monks who only

94
00:07:01,880 --> 00:07:03,600
 eat one meal a day, how can

95
00:07:03,600 --> 00:07:04,600
 they be radiant?

96
00:07:04,600 --> 00:07:09,840
 How can they look so healthy, so young if they're not

97
00:07:09,840 --> 00:07:13,120
 eating full meals, sometimes they

98
00:07:13,120 --> 00:07:15,120
 don't get much to eat.

99
00:07:15,120 --> 00:07:16,920
 How can they still look so radiant?"

100
00:07:16,920 --> 00:07:19,920
 That's what the Buddha said.

101
00:07:19,920 --> 00:07:23,720
 He said, "It's because they don't.

102
00:07:23,720 --> 00:07:27,840
 For the past they do not mourn nor for the future weep.

103
00:07:27,840 --> 00:07:30,590
 They take the present as it comes and thus their color keep

104
00:07:30,590 --> 00:07:30,880
."

105
00:07:30,880 --> 00:07:43,000
 That's from the Jataka.

106
00:07:43,000 --> 00:07:45,420
 So everything that we experience occurs in the present

107
00:07:45,420 --> 00:07:45,960
 moment.

108
00:07:45,960 --> 00:07:48,240
 You don't have to go looking for it.

109
00:07:48,240 --> 00:07:53,660
 You just have to remind yourself that you're here now.

110
00:07:53,660 --> 00:07:56,220
 When you think about the past, remind yourself it's just a

111
00:07:56,220 --> 00:07:57,560
 thought and it's present.

112
00:07:57,560 --> 00:08:00,490
 When you worry about the future, remind yourself this is

113
00:08:00,490 --> 00:08:02,040
 worry and this is thought.

114
00:08:02,040 --> 00:08:03,800
 This is planning.

115
00:08:03,800 --> 00:08:05,800
 This is remembering.

116
00:08:05,800 --> 00:08:08,730
 Even remembering and planning, they all happen here and now

117
00:08:08,730 --> 00:08:08,960
.

118
00:08:08,960 --> 00:08:12,190
 To remind yourself, otherwise you create the concept of

119
00:08:12,190 --> 00:08:15,320
 future and past and you're lost.

120
00:08:15,320 --> 00:08:17,000
 You're in the realm of concepts.

121
00:08:17,000 --> 00:08:19,760
 You're no longer in the realm of reality.

122
00:08:19,760 --> 00:08:22,080
 You create a new universe.

123
00:08:22,080 --> 00:08:23,760
 It's not here and now.

124
00:08:23,760 --> 00:08:30,720
 It's completely illusory.

125
00:08:30,720 --> 00:08:32,080
 And you can feel the difference.

126
00:08:32,080 --> 00:08:35,400
 You might argue, "Well, that's useful," and so on.

127
00:08:35,400 --> 00:08:38,720
 Argue as you may.

128
00:08:38,720 --> 00:08:42,420
 We suffer from living in the past and the future.

129
00:08:42,420 --> 00:08:45,300
 You can feel it viscerally.

130
00:08:45,300 --> 00:08:46,300
 You lose energy.

131
00:08:46,300 --> 00:08:47,300
 You're weighted down.

132
00:08:47,300 --> 00:08:48,300
 You're tired.

133
00:08:48,300 --> 00:08:52,000
 You get sick.

134
00:08:52,000 --> 00:08:57,340
 All of our desires and aversions, they come from this realm

135
00:08:57,340 --> 00:08:58,800
 of concepts.

136
00:08:58,800 --> 00:09:03,200
 Reality is not clingable.

137
00:09:03,200 --> 00:09:13,080
 If you're living in reality, there's no quality to it that

138
00:09:13,080 --> 00:09:15,120
 is appealing or...

139
00:09:15,120 --> 00:09:16,120
 Displeasing.

140
00:09:16,120 --> 00:09:24,920
 So, a little bit of...

141
00:09:24,920 --> 00:09:35,600
 So, we live in the present moment.

142
00:09:35,600 --> 00:09:37,600
 This is what we try to do when we meditate.

143
00:09:37,600 --> 00:09:41,440
 We try to just be here now, plain and simple.

144
00:09:41,440 --> 00:09:42,440
 Not judge it.

145
00:09:42,440 --> 00:09:43,440
 Not react to it.

146
00:09:43,440 --> 00:09:45,440
 It's not easy.

147
00:09:45,440 --> 00:09:48,320
 It's building a new habit.

148
00:09:48,320 --> 00:09:50,600
 So, it takes training.

149
00:09:50,600 --> 00:09:52,600
 It takes practice.

150
00:09:52,600 --> 00:09:55,160
 But that's what we do.

151
00:09:55,160 --> 00:09:57,800
 And then we shine as well.

152
00:09:57,800 --> 00:09:59,800
 We become more radiant.

153
00:09:59,800 --> 00:10:08,880
 So, a little bit of dhamma for today.

154
00:10:08,880 --> 00:10:10,880
 Everybody's waving at me.

155
00:10:10,880 --> 00:10:13,880
 All these waves.

156
00:10:13,880 --> 00:10:19,320
 Okay, we got a couple of questions here.

157
00:10:19,320 --> 00:10:22,140
 Do you have to see impermanence, suffering in non-self

158
00:10:22,140 --> 00:10:23,640
 moment to moment, or does simply

159
00:10:23,640 --> 00:10:26,800
 knowing and understanding impermanence, suffering in non-

160
00:10:26,800 --> 00:10:28,720
self moment to moment, satisfy the

161
00:10:28,720 --> 00:10:34,720
 practice?

162
00:10:34,720 --> 00:10:37,280
 Knowing and seeing are the same.

163
00:10:37,280 --> 00:10:42,120
 Sounds like you're maybe referring to intellectual

164
00:10:42,120 --> 00:10:45,920
 knowledge, theoretical knowledge.

165
00:10:45,920 --> 00:10:52,080
 Like, "Hey, I was happy yesterday and I'm not happy today.

166
00:10:52,080 --> 00:10:53,080
 That's impermanence."

167
00:10:53,080 --> 00:10:55,080
 Or something like that.

168
00:10:55,080 --> 00:10:56,480
 Knowing and seeing.

169
00:10:56,480 --> 00:10:57,960
 Knowing comes from seeing.

170
00:10:57,960 --> 00:11:01,000
 When you see, you know.

171
00:11:01,000 --> 00:11:05,200
 You know because you see.

172
00:11:05,200 --> 00:11:06,640
 And it's not something you have to do.

173
00:11:06,640 --> 00:11:10,440
 It's something that happens.

174
00:11:10,440 --> 00:11:14,000
 What you have to do is be present and be only present.

175
00:11:14,000 --> 00:11:16,200
 And you'll see things arising and ceasing.

176
00:11:16,200 --> 00:11:17,200
 They're impermanent.

177
00:11:17,200 --> 00:11:19,320
 It means they're not stable.

178
00:11:19,320 --> 00:11:24,240
 They're dukkha, which means they're not satisfying.

179
00:11:24,240 --> 00:11:28,160
 Because they're unstable, you can't claim that that's a

180
00:11:28,160 --> 00:11:29,800
 cause of happiness.

181
00:11:29,800 --> 00:11:32,960
 Like that's going to satisfy you.

182
00:11:32,960 --> 00:11:34,120
 And you can't control them.

183
00:11:34,120 --> 00:11:35,520
 They don't belong to you.

184
00:11:35,520 --> 00:11:38,320
 They don't have any entity of their own.

185
00:11:38,320 --> 00:11:39,320
 They're ephemeral.

186
00:11:39,320 --> 00:11:44,800
 They arise in these things.

187
00:11:44,800 --> 00:11:47,460
 Once you understand and know suffering, impermanence, and

188
00:11:47,460 --> 00:11:49,200
 non-self, what is the next part of the

189
00:11:49,200 --> 00:11:50,680
 practice of meditation?

190
00:11:50,680 --> 00:11:52,280
 It only takes a moment.

191
00:11:52,280 --> 00:11:56,820
 If you have one moment of pure understanding of imperman

192
00:11:56,820 --> 00:11:59,480
ence or suffering or non-self,

193
00:11:59,480 --> 00:12:01,640
 one or the other, you'll see it clearly.

194
00:12:01,640 --> 00:12:02,640
 So clearly.

195
00:12:02,640 --> 00:12:07,920
 The next moment is the cessation, nirvana.

196
00:12:07,920 --> 00:12:11,880
 So there isn't really a next step.

197
00:12:11,880 --> 00:12:17,720
 The next step happens by itself.

198
00:12:17,720 --> 00:12:21,830
 Is it natural for fear to arise at times one sees non-

199
00:12:21,830 --> 00:12:22,840
control?

200
00:12:22,840 --> 00:12:26,800
 Absolutely, yes.

201
00:12:26,800 --> 00:12:31,600
 It's called bhayanyana.

202
00:12:31,600 --> 00:12:32,600
 Not exactly.

203
00:12:32,600 --> 00:12:36,040
 Bhayanyana is you see the fierceness.

204
00:12:36,040 --> 00:12:38,080
 The fear that comes from it is problem.

205
00:12:38,080 --> 00:12:41,080
 Fear is based on anger, dosa.

206
00:12:41,080 --> 00:12:43,960
 So you have to acknowledge it.

207
00:12:43,960 --> 00:12:46,160
 Afraid, afraid.

208
00:12:46,160 --> 00:12:50,200
 But it's of course common once you see how terrifying it is

209
00:12:50,200 --> 00:12:52,200
, how unstable everything

210
00:12:52,200 --> 00:12:53,200
 is.

211
00:12:53,200 --> 00:12:55,200
 It's like having the rug pulled out from under you or

212
00:12:55,200 --> 00:13:00,080
 having your whole foundation shook.

213
00:13:00,080 --> 00:13:01,640
 Because we think of our lives as.

214
00:13:01,640 --> 00:13:05,520
 We can control this and then we realize, "Ah, I can't

215
00:13:05,520 --> 00:13:06,720
 control it."

216
00:13:06,720 --> 00:13:07,720
 That's scary.

217
00:13:07,720 --> 00:13:10,080
 But the knowledge isn't afraid.

218
00:13:10,080 --> 00:13:12,080
 It's not important to be afraid.

219
00:13:12,080 --> 00:13:13,080
 It's not good to be afraid.

220
00:13:13,080 --> 00:13:22,920
 It's just a reaction that we have to work on and overcome.

221
00:13:22,920 --> 00:13:25,570
 When an unenlightened person notes seeing as seeing,

222
00:13:25,570 --> 00:13:27,880
 hearing as hearing, is it still karma?

223
00:13:27,880 --> 00:13:29,880
 Yes, it's kusala karma.

224
00:13:29,880 --> 00:13:32,880
 It's jnana sampayuta as well.

225
00:13:32,880 --> 00:13:35,880
 So it's associated with knowledge.

226
00:13:35,880 --> 00:13:41,880
 So it's karma and the best of the best sort.

227
00:13:41,880 --> 00:13:51,780
 It's karma that gets you closer to seeing the truth and

228
00:13:51,780 --> 00:13:58,680
 letting go and realizing nirvana.

229
00:13:58,680 --> 00:14:01,400
 What is the role of prayer in spiritual practice?

230
00:14:01,400 --> 00:14:04,680
 If you're praying to another being, that's delusion.

231
00:14:04,680 --> 00:14:07,640
 That's what role it has.

232
00:14:07,640 --> 00:14:09,970
 But if you're making a determination for something, "May

233
00:14:09,970 --> 00:14:12,880
 this happen, may that happen," that's

234
00:14:12,880 --> 00:14:17,640
 a good way of setting yourself.

235
00:14:17,640 --> 00:14:19,280
 It actually changes the universe.

236
00:14:19,280 --> 00:14:20,400
 It changes your mind.

237
00:14:20,400 --> 00:14:22,800
 It changes your surroundings.

238
00:14:22,800 --> 00:14:27,060
 If you have a very strong mind, you can actually change

239
00:14:27,060 --> 00:14:29,920
 things with your determination.

240
00:14:29,920 --> 00:14:33,400
 May it be thus, may it be thus, may it not be this.

241
00:14:33,400 --> 00:14:36,800
 You can actually affect the universe.

242
00:14:36,800 --> 00:14:40,820
 This is how some meditators, some meditation practices,

243
00:14:40,820 --> 00:14:43,360
 they actually practice to manifest

244
00:14:43,360 --> 00:14:45,360
 things like, "May I be rich?

245
00:14:45,360 --> 00:14:46,360
 May I be this or that?"

246
00:14:46,360 --> 00:14:50,320
 Of course we don't do that, but they seem to have some

247
00:14:50,320 --> 00:14:52,520
 results in getting what they

248
00:14:52,520 --> 00:14:55,690
 want through the power of the mind, the power of

249
00:14:55,690 --> 00:14:57,000
 determination.

250
00:14:57,000 --> 00:14:58,760
 So making a wish, "May I be happy?

251
00:14:58,760 --> 00:14:59,760
 May other beings be happy?

252
00:14:59,760 --> 00:15:00,760
 May my parents be happy?"

253
00:15:00,760 --> 00:15:02,760
 It can really affect them.

254
00:15:02,760 --> 00:15:05,920
 It can change reality if you have a strong enough

255
00:15:05,920 --> 00:15:07,240
 determination.

256
00:15:07,240 --> 00:15:15,520
 We call it determination, atitana.

257
00:15:15,520 --> 00:15:18,090
 If you hear a voice clearly, is it important to note all

258
00:15:18,090 --> 00:15:19,760
 the change in the voice happening

259
00:15:19,760 --> 00:15:23,040
 in that particular moment?

260
00:15:23,040 --> 00:15:26,200
 No, just say "hearing, hearing."

261
00:15:26,200 --> 00:15:34,120
 You don't have to worry about the particulars.

262
00:15:34,120 --> 00:15:36,970
 You'll see all the particulars anyway, but what you have to

263
00:15:36,970 --> 00:15:38,360
 remind yourself is that that's

264
00:15:38,360 --> 00:15:39,360
 just hearing.

265
00:15:39,360 --> 00:15:40,360
 It's nothing else.

266
00:15:40,360 --> 00:15:41,360
 It's not good.

267
00:15:41,360 --> 00:15:42,360
 It's not bad.

268
00:15:42,360 --> 00:15:43,360
 It's not me.

269
00:15:43,360 --> 00:15:44,360
 It's not life.

270
00:15:44,360 --> 00:16:05,200
 There are a good number of people tonight.

271
00:16:05,200 --> 00:16:08,120
 Most of you are green, which is good.

272
00:16:08,120 --> 00:16:10,200
 Green means you've meditated recently.

273
00:16:10,200 --> 00:16:11,800
 Appreciate that.

274
00:16:11,800 --> 00:16:18,800
 You're welcome.

275
00:16:18,800 --> 00:16:44,200
 No other questions?

276
00:16:44,200 --> 00:16:46,360
 We will stop there.

277
00:16:46,360 --> 00:16:51,040
 Tomorrow, I don't think I'll be broadcasting tomorrow.

278
00:16:51,040 --> 00:16:54,250
 It's a long day tomorrow with this big thing in Mississauga

279
00:16:54,250 --> 00:16:54,520
.

280
00:16:54,520 --> 00:16:56,720
 I'll just take a break.

281
00:16:56,720 --> 00:16:57,720
 We'll see.

282
00:16:57,720 --> 00:16:58,720
 I don't think so.

283
00:16:58,720 --> 00:16:58,720
 Have a good night, everyone.

284
00:16:58,720 --> 00:17:16,720
 Sorry, I was thinking.

