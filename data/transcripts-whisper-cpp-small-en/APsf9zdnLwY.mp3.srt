1
00:00:00,000 --> 00:00:07,760
 Okay, on Kalyanamitata, I have a childhood friend whom I

2
00:00:07,760 --> 00:00:09,320
 care for a great deal, but he

3
00:00:09,320 --> 00:00:10,320
 is quite base.

4
00:00:10,320 --> 00:00:13,970
 I don't like being around him when he talks about drinking,

5
00:00:13,970 --> 00:00:15,160
 hunting, etc.

6
00:00:15,160 --> 00:00:18,260
 He does have some potential, but is it worth continuing

7
00:00:18,260 --> 00:00:19,600
 this relationship?

8
00:00:19,600 --> 00:00:25,080
 Okay, I'm going to take a fairly hardline Buddhist stance

9
00:00:25,080 --> 00:00:25,920
 here.

10
00:00:25,920 --> 00:00:28,440
 Probably not in the way you think.

11
00:00:28,440 --> 00:00:31,080
 But the hardline Buddhist stance is that it's not worth

12
00:00:31,080 --> 00:00:32,680
 keeping any relationship.

13
00:00:32,680 --> 00:00:36,920
 Okay, there's no worth in relationships.

14
00:00:36,920 --> 00:00:42,770
 The only relationship that is really promoted by the Buddha

15
00:00:42,770 --> 00:00:45,640
 is that the association with

16
00:00:45,640 --> 00:00:46,800
 good people.

17
00:00:46,800 --> 00:00:52,030
 So it's kind of radical or incorrect of me even to suggest

18
00:00:52,030 --> 00:00:55,800
 that no relationship is worthwhile.

19
00:00:55,800 --> 00:00:59,280
 So you would argue and you would say, "No, the Buddha said

20
00:00:59,280 --> 00:01:03,160
," and this is correct, "Good

21
00:01:03,160 --> 00:01:08,000
 relationships with good people, people who are your equal

22
00:01:08,000 --> 00:01:10,320
 in morality and concentration

23
00:01:10,320 --> 00:01:16,820
 and wisdom, in practice and in discipline, or better," so

24
00:01:16,820 --> 00:01:21,080
 better than you at those things.

25
00:01:21,080 --> 00:01:23,280
 This is good.

26
00:01:23,280 --> 00:01:30,800
 So the association with these people has potential.

27
00:01:30,800 --> 00:01:37,580
 The other type of relationship is useless, is harmful, is a

28
00:01:37,580 --> 00:01:39,280
 drag on you.

29
00:01:39,280 --> 00:01:45,440
 So the association with people who are less cultivated than

30
00:01:45,440 --> 00:01:46,240
 you.

31
00:01:46,240 --> 00:01:48,910
 Now we'll get back to that because even that sounds not so

32
00:01:48,910 --> 00:01:49,360
 nice.

33
00:01:49,360 --> 00:01:51,480
 But let's go even harder than that and say, "No

34
00:01:51,480 --> 00:01:53,360
 relationships are worth anything."

35
00:01:53,360 --> 00:01:55,890
 Because we can say that in an ultimate sense, even though

36
00:01:55,890 --> 00:01:57,280
 it's not how the Buddha would

37
00:01:57,280 --> 00:02:00,220
 teach, we can say it in an ultimate sense and so we can

38
00:02:00,220 --> 00:02:02,040
 really get a perspective here.

39
00:02:02,040 --> 00:02:06,340
 Because nothing is truly, nothing in this world, nothing

40
00:02:06,340 --> 00:02:08,600
 that arises has any intrinsic

41
00:02:08,600 --> 00:02:09,600
 worth.

42
00:02:09,600 --> 00:02:12,540
 In the end, even the good things in this world you have to

43
00:02:12,540 --> 00:02:13,280
 let go of.

44
00:02:13,280 --> 00:02:17,900
 If you cling to them as having some essential worth, you

45
00:02:17,900 --> 00:02:19,200
 will get stuck.

46
00:02:19,200 --> 00:02:21,120
 You will be stuck on them.

47
00:02:21,120 --> 00:02:23,520
 You will cling to anything, even the good things.

48
00:02:23,520 --> 00:02:25,480
 This is why the Buddha taught the simile of the raft.

49
00:02:25,480 --> 00:02:29,320
 A raft is something you use to get across a river.

50
00:02:29,320 --> 00:02:31,430
 It's not something once you get across the river that you

51
00:02:31,430 --> 00:02:32,400
 put on your back and carry

52
00:02:32,400 --> 00:02:34,240
 with you.

53
00:02:34,240 --> 00:02:37,460
 So for that reason, even good relationships, you should be

54
00:02:37,460 --> 00:02:38,960
 able to let go of them at the

55
00:02:38,960 --> 00:02:41,400
 drop of the hat.

56
00:02:41,400 --> 00:02:44,040
 So how does this relate to bad relationships?

57
00:02:44,040 --> 00:02:48,520
 It sounds like what the Buddha is saying is avoid these

58
00:02:48,520 --> 00:02:49,480
 people.

59
00:02:49,480 --> 00:02:53,020
 And I think yes, in some instances you should actively

60
00:02:53,020 --> 00:02:55,180
 avoid such people in cases where

61
00:02:55,180 --> 00:02:57,080
 they are dragging you down.

62
00:02:57,080 --> 00:03:04,180
 If such a person is dragging you down and you're getting

63
00:03:04,180 --> 00:03:07,520
 less pure and they're getting

64
00:03:07,520 --> 00:03:10,800
 less pure and they're dragging you down, they're not

65
00:03:10,800 --> 00:03:13,240
 becoming any better for a relationship,

66
00:03:13,240 --> 00:03:14,800
 then you should avoid them.

67
00:03:14,800 --> 00:03:22,490
 But I would sort of guess or suggest that in this case, the

68
00:03:22,490 --> 00:03:25,960
 difference here between

69
00:03:25,960 --> 00:03:28,510
 this relationship, a bad relationship and a good

70
00:03:28,510 --> 00:03:30,480
 relationship, the meaning is a good

71
00:03:30,480 --> 00:03:33,400
 relationship should be cultivated.

72
00:03:33,400 --> 00:03:35,320
 You should seek out such people.

73
00:03:35,320 --> 00:03:37,200
 You should visit them.

74
00:03:37,200 --> 00:03:43,920
 You should incline towards the participation in the things

75
00:03:43,920 --> 00:03:47,560
 they do, the things they say,

76
00:03:47,560 --> 00:03:50,360
 which means you should listen to what they say.

77
00:03:50,360 --> 00:03:52,820
 So there should be an active participation in the

78
00:03:52,820 --> 00:03:53,780
 relationship.

79
00:03:53,780 --> 00:03:59,500
 For bad relationships, the general rule of thumb that I

80
00:03:59,500 --> 00:04:02,880
 would suggest is to not actively

81
00:04:02,880 --> 00:04:04,280
 pursue them.

82
00:04:04,280 --> 00:04:09,490
 So the sense is they come to you and this is actually how a

83
00:04:09,490 --> 00:04:11,880
 teacher should behave in

84
00:04:11,880 --> 00:04:16,980
 Buddhism, not seeking out students, not chasing after them,

85
00:04:16,980 --> 00:04:19,520
 not pushing them to practice or

86
00:04:19,520 --> 00:04:23,200
 nagging them about the practice, but letting them come.

87
00:04:23,200 --> 00:04:27,800
 So when a student comes and asks questions and asks for

88
00:04:27,800 --> 00:04:30,960
 teachings, then the teacher teaches.

89
00:04:30,960 --> 00:04:33,720
 So after you get people who think the other way around,

90
00:04:33,720 --> 00:04:35,440
 they wait for the teacher to teach

91
00:04:35,440 --> 00:04:39,200
 them and the teacher is like, "Why would I bother?"

92
00:04:39,200 --> 00:04:43,600
 It's not up to me to chase after my students.

93
00:04:43,600 --> 00:04:46,760
 I often have to remind my students this, "I'm not going to

94
00:04:46,760 --> 00:04:47,920
 chase after you.

95
00:04:47,920 --> 00:04:50,960
 If you want to learn, you come and chase after me.

96
00:04:50,960 --> 00:04:51,960
 You can come."

97
00:04:51,960 --> 00:04:56,350
 And like in ancient times, like I've mentioned before, you

98
00:04:56,350 --> 00:04:58,600
 had to sit out in the courtyard

99
00:04:58,600 --> 00:04:59,960
 and they wouldn't let you into the monastery.

100
00:04:59,960 --> 00:05:00,960
 They'd say, "Go away.

101
00:05:00,960 --> 00:05:01,960
 We won't accept people."

102
00:05:01,960 --> 00:05:04,750
 In order to get into the monastery, you had to actually sit

103
00:05:04,750 --> 00:05:06,040
 in the courtyard for days

104
00:05:06,040 --> 00:05:09,560
 in the rain and the sun until they finally, like Fight Club

105
00:05:09,560 --> 00:05:11,120
, like that old movie Fight

106
00:05:11,120 --> 00:05:16,110
 Club, and to sit there and they'd tell you they're not

107
00:05:16,110 --> 00:05:19,760
 going to let you in until finally

108
00:05:19,760 --> 00:05:22,520
 they would see that you're very dedicated and let you in.

109
00:05:22,520 --> 00:05:25,440
 But at the very least, you have to make the effort.

110
00:05:25,440 --> 00:05:33,430
 Now, you shouldn't make this effort in regards to people

111
00:05:33,430 --> 00:05:36,720
 who are going to drag you down.

112
00:05:36,720 --> 00:05:38,000
 Let's put it that way.

113
00:05:38,000 --> 00:05:41,580
 We're not trying to be judgmental and say, "Lessers, better

114
00:05:41,580 --> 00:05:41,760
."

115
00:05:41,760 --> 00:05:46,600
 People who are not especially conducive towards your own

116
00:05:46,600 --> 00:05:47,760
 practice.

117
00:05:47,760 --> 00:05:51,440
 There's no reason to cultivate them because seeking them

118
00:05:51,440 --> 00:05:53,360
 out encourages them on their

119
00:05:53,360 --> 00:05:56,460
 practice. It's like saying to them, "Yes, what you're doing

120
00:05:56,460 --> 00:05:56,680
 is good. I want to be a

121
00:05:56,680 --> 00:05:57,680
 part of it."

122
00:05:57,680 --> 00:06:02,090
 Now, the only option would be, "Yes, or no, you're doing

123
00:06:02,090 --> 00:06:04,000
 bad. I want to help you with

124
00:06:04,000 --> 00:06:06,880
 that. I want to save you from that."

125
00:06:06,880 --> 00:06:10,400
 But it doesn't, in reality, work that way because you're

126
00:06:10,400 --> 00:06:11,680
 letting them get their hooks

127
00:06:11,680 --> 00:06:12,680
 in.

128
00:06:12,680 --> 00:06:17,030
 Anytime we apply ourselves to someone, it's creating a

129
00:06:17,030 --> 00:06:19,840
 karma, a connection, a hook with

130
00:06:19,840 --> 00:06:23,840
 that person. And if it's a person inclined towards bad

131
00:06:23,840 --> 00:06:26,080
 things, then they have a hook

132
00:06:26,080 --> 00:06:28,840
 in you and they can drag you in a bad way.

133
00:06:28,840 --> 00:06:33,670
 So it's something that should be looked upon with equanim

134
00:06:33,670 --> 00:06:34,360
ity.

135
00:06:34,360 --> 00:06:37,280
 So you talk about a childhood friend whom I care for a

136
00:06:37,280 --> 00:06:39,240
 great deal, which is really the

137
00:06:39,240 --> 00:06:44,280
 problem because in no case in Buddhism, hardline Buddhism,

138
00:06:44,280 --> 00:06:46,800
 should you care for anyone.

139
00:06:46,800 --> 00:06:51,860
 What that means is it shouldn't be about attachment. If you

140
00:06:51,860 --> 00:06:53,360
 care for someone, I've gotten in trouble

141
00:06:53,360 --> 00:06:56,240
 with this before, but I stand by it. I did a video on

142
00:06:56,240 --> 00:06:58,200
 caring. You should stop caring.

143
00:06:58,200 --> 00:07:02,240
 You should be uncaring. It's just words. It's semantics.

144
00:07:02,240 --> 00:07:07,420
 Now, if you care, that means when they suffer, when they're

145
00:07:07,420 --> 00:07:09,520
 happy, you're happy. When they

146
00:07:09,520 --> 00:07:13,330
 suffer, you're unhappy. And so your happiness depends on

147
00:07:13,330 --> 00:07:15,080
 that person. It doesn't help that

148
00:07:15,080 --> 00:07:18,770
 person in any way. You're caring for someone. It doesn't

149
00:07:18,770 --> 00:07:21,840
 help them. It makes them feel cared

150
00:07:21,840 --> 00:07:31,360
 for, which is good for people who have low self-esteem and

151
00:07:31,360 --> 00:07:35,360
 who need the support.

152
00:07:35,360 --> 00:07:38,720
 But it's not a wholesome thing for you to actually care for

153
00:07:38,720 --> 00:07:40,080
 the person. So you can say

154
00:07:40,080 --> 00:07:43,640
 to the person, "I'm here for you if you ever need me," and

155
00:07:43,640 --> 00:07:45,560
 that makes them feel good about

156
00:07:45,560 --> 00:07:51,300
 it. But when you actually care, it's a difference between

157
00:07:51,300 --> 00:07:58,400
 caring in the sense of an active,

158
00:07:58,400 --> 00:08:02,540
 an action. Like I care for you by cleaning your wounds and

159
00:08:02,540 --> 00:08:04,680
 changing your bandages, and

160
00:08:04,680 --> 00:08:07,920
 I care for you in terms of feeding you, and I care for you

161
00:08:07,920 --> 00:08:09,480
 to take care of someone. If

162
00:08:09,480 --> 00:08:13,340
 you mean it in terms of taking care of someone, then care

163
00:08:13,340 --> 00:08:15,560
 is okay. Care is great. Even just

164
00:08:15,560 --> 00:08:18,300
 to talk to the person and say, "I'm here for you if you

165
00:08:18,300 --> 00:08:20,040
 need anything," to care for them

166
00:08:20,040 --> 00:08:23,660
 by teaching them meditation and so on and helping them in

167
00:08:23,660 --> 00:08:25,280
 whatever way you can. But

168
00:08:25,280 --> 00:08:30,800
 to actually care, what happens to someone? It doesn't help

169
00:08:30,800 --> 00:08:33,240
 the other person. It means

170
00:08:33,240 --> 00:08:36,550
 that your happiness is dependent on something external. It

171
00:08:36,550 --> 00:08:38,600
 doesn't actually make you a better

172
00:08:38,600 --> 00:08:45,030
 caretaker, although it does make you more caught up in

173
00:08:45,030 --> 00:08:49,160
 their happiness and suffering.

174
00:08:49,160 --> 00:08:53,250
 For laypeople, that's tough because you have family and

175
00:08:53,250 --> 00:08:55,320
 friends who you very much care

176
00:08:55,320 --> 00:09:02,930
 for. We do try to encourage people to think just in terms

177
00:09:02,930 --> 00:09:05,080
 of duties. Your relationships

178
00:09:05,080 --> 00:09:08,700
 with people are your position in life, and so you do things

179
00:09:08,700 --> 00:09:10,480
 for such people because you

180
00:09:10,480 --> 00:09:14,240
 have duties to your parents, to your families. But in the

181
00:09:14,240 --> 00:09:15,440
 end, no matter what, there will

182
00:09:15,440 --> 00:09:21,040
 be caring. We care for each other. If someone suffers, we

183
00:09:21,040 --> 00:09:23,920
 suffer. If someone leaves us,

184
00:09:23,920 --> 00:09:29,100
 if someone dumps us, if someone dies, this causes us

185
00:09:29,100 --> 00:09:31,200
 suffering. The claim is this is

186
00:09:31,200 --> 00:09:35,090
 not a beneficial thing. It just makes us suffer a needless

187
00:09:35,090 --> 00:09:36,800
 thing. It doesn't make you any

188
00:09:36,800 --> 00:09:42,250
 better at helping a person out. But what that's doing here

189
00:09:42,250 --> 00:09:47,160
 is it's creating an arbitrary reason

190
00:09:47,160 --> 00:09:51,980
 to maintain a friendship with the person. It says nothing

191
00:09:51,980 --> 00:09:53,600
 about the quality of a friendship

192
00:09:53,600 --> 00:09:57,290
 that you care for a person. The fact that you care a great

193
00:09:57,290 --> 00:09:59,880
 deal about this person says nothing

194
00:09:59,880 --> 00:10:06,210
 about whether it's a good relationship. So if I care very

195
00:10:06,210 --> 00:10:11,920
 much for a vicious pit bull,

196
00:10:11,920 --> 00:10:15,840
 it says nothing about whether that's a good thing or not.

197
00:10:15,840 --> 00:10:17,640
 Or if I care for a poisonous

198
00:10:17,640 --> 00:10:21,350
 snake. Like in the jataka's there was this snake charmer

199
00:10:21,350 --> 00:10:23,200
 who was just in love with his

200
00:10:23,200 --> 00:10:26,950
 snake and he was a very poisonous snake and he'd play with

201
00:10:26,950 --> 00:10:28,880
 the snake all the time until

202
00:10:28,880 --> 00:10:31,750
 one day and they all told him, "You're crazy." And he said,

203
00:10:31,750 --> 00:10:33,240
 "Oh, I care for this snake very

204
00:10:33,240 --> 00:10:39,000
 much." And one day he stuck his hand in the snake's basket

205
00:10:39,000 --> 00:10:41,360
 and the snake was just in a

206
00:10:41,360 --> 00:10:47,400
 bad mood and it bit him and killed him. It really says it's

207
00:10:47,400 --> 00:10:50,120
 not meaningful to say I care

208
00:10:50,120 --> 00:10:53,400
 for the person. The only question is whether it's

209
00:10:53,400 --> 00:10:56,160
 beneficial, whether it's good for you,

210
00:10:56,160 --> 00:11:00,090
 whether it's a good thing, whether it's the proper way of

211
00:11:00,090 --> 00:11:02,200
 approaching the universe, whether

212
00:11:02,200 --> 00:11:09,200
 it's a part of the cosmic plan. But not the cosmic plan in

213
00:11:09,200 --> 00:11:13,240
 the sense of in line with reality

214
00:11:13,240 --> 00:11:15,490
 as far as bringing happiness. And you don't have to

215
00:11:15,490 --> 00:11:17,280
 distinguish between you and the other

216
00:11:17,280 --> 00:11:21,520
 person. Just in general, does it get closer to a state of

217
00:11:21,520 --> 00:11:23,920
 peace or does it incline towards

218
00:11:23,920 --> 00:11:31,680
 suffering and upset? This is how you have to look at. And

219
00:11:31,680 --> 00:11:33,280
 we're usually not able to do that

220
00:11:33,280 --> 00:11:38,190
 with relationships where we care for the person because our

221
00:11:38,190 --> 00:11:40,760
 reason for being friends with

222
00:11:40,760 --> 00:11:44,270
 them is arbitrary, to some extent arbitrary. Now, of course

223
00:11:44,270 --> 00:11:45,940
, reasons for caring people

224
00:11:45,940 --> 00:11:49,700
 usually have to do with the happiness that we bring each

225
00:11:49,700 --> 00:11:52,320
 other. So there's that. But

226
00:11:52,320 --> 00:11:59,510
 the caring in and of itself is actually a danger for you

227
00:11:59,510 --> 00:12:02,440
 because it means that your

228
00:12:02,440 --> 00:12:06,130
 happiness is dependent on something else. It seems nice and

229
00:12:06,130 --> 00:12:07,280
 it's what society teaches

230
00:12:07,280 --> 00:12:13,200
 us very strongly. If you don't care, you're a heartless ble

231
00:12:13,200 --> 00:12:19,800
ep, bleepity, bleep. But the

232
00:12:19,800 --> 00:12:24,130
 truth of it is that we have to be whole, right? We can't be

233
00:12:24,130 --> 00:12:26,200
 dependent on others if you really

234
00:12:26,200 --> 00:12:30,030
 want to be happy. If you really want to be truly happy, you

235
00:12:30,030 --> 00:12:31,360
 have to have some kind of

236
00:12:31,360 --> 00:12:35,420
 equanimous love for beings. So it has to be universal. Like

237
00:12:35,420 --> 00:12:37,680
 the Buddha's love was universal.

238
00:12:37,680 --> 00:12:41,920
 He was able to have love for all beings and compassion for

239
00:12:41,920 --> 00:12:44,080
 all beings without any kind

240
00:12:44,080 --> 00:12:49,960
 of partiality. So he had no caring. Buddha didn't care

241
00:12:49,960 --> 00:12:53,280
 about anyone in the sense of actually

242
00:12:53,280 --> 00:12:57,550
 being upset when a person, if a person didn't get his

243
00:12:57,550 --> 00:13:00,720
 teachings or didn't like him or so

244
00:13:00,720 --> 00:13:04,690
 on. There was no sense of betrayal when they were not to

245
00:13:04,690 --> 00:13:07,020
 betray him. There was no sense

246
00:13:07,020 --> 00:13:13,030
 of loss when his two chief disciples passed away because he

247
00:13:13,030 --> 00:13:15,880
 was free. He was at peace.

248
00:13:15,880 --> 00:13:21,750
 So things didn't bother him even though he worked very hard

249
00:13:21,750 --> 00:13:24,320
 to help the world. But he

250
00:13:24,320 --> 00:13:27,840
 did it without caring. That's awful. I know it's a problem

251
00:13:27,840 --> 00:13:29,680
 with the English language.

252
00:13:29,680 --> 00:13:35,490
 The word has become so steeped in goodness. Caring is good.

253
00:13:35,490 --> 00:13:38,320
 Sorry, that's not really.

254
00:13:38,320 --> 00:13:41,320
 It doesn't actually lead to happiness. Maybe that's a big

255
00:13:41,320 --> 00:13:43,000
 part of the problem is our culture

256
00:13:43,000 --> 00:13:48,780
 is so steeped in caring that we get upset very easily. Even

257
00:13:48,780 --> 00:13:50,400
 if I just tell you that,

258
00:13:50,400 --> 00:13:53,770
 we care very much about caring. So if I tell you that it's

259
00:13:53,770 --> 00:13:55,600
 not good to care, then you right

260
00:13:55,600 --> 00:14:00,820
 away get upset. I think many people will unsubscribe or put

261
00:14:00,820 --> 00:14:03,320
 nasty comments to my videos and when

262
00:14:03,320 --> 00:14:06,060
 they find out I'm telling people not to care, that caring

263
00:14:06,060 --> 00:14:07,800
 is bad. So this is the problem

264
00:14:07,800 --> 00:14:12,780
 with caring is it actually makes you angry. It leads to

265
00:14:12,780 --> 00:14:15,720
 lots of trouble. When you care

266
00:14:15,720 --> 00:14:21,250
 about something, it can lead to conflict. It does lead to

267
00:14:21,250 --> 00:14:24,360
 conflict when things go against

268
00:14:24,360 --> 00:14:27,880
 your expectations. So I hope that helps.

