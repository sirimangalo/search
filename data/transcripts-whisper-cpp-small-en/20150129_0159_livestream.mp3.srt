1
00:00:00,000 --> 00:00:14,600
 Okay, good morning everyone.

2
00:00:14,600 --> 00:00:23,540
 I'm going to be doing lives from Myeongsan, from the

3
00:00:23,540 --> 00:00:30,600
 mountain.

4
00:00:30,600 --> 00:00:41,520
 I've come here to undertake more intensive practice in the

5
00:00:41,520 --> 00:00:43,600
 forest.

6
00:00:43,600 --> 00:00:53,310
 One thing that I think is not well understood about

7
00:00:53,310 --> 00:01:03,200
 Buddhist meditation is that it's designed

8
00:01:03,200 --> 00:01:15,200
 to change the person in a very fundamental way.

9
00:01:15,200 --> 00:01:29,410
 It's not meant to merely return you to an ordinary state

10
00:01:29,410 --> 00:01:29,880
 where you

11
00:01:29,880 --> 00:01:35,880
 can go back and live your life as you would like.

12
00:01:35,880 --> 00:01:44,000
 In many ways will change the way you look at the world.

13
00:01:44,000 --> 00:01:54,160
 In other words, it's not designed to make you an ordinary

14
00:01:54,160 --> 00:01:55,760
 person, it's designed to make

15
00:01:55,760 --> 00:02:13,040
 you a somewhat special person.

16
00:02:13,040 --> 00:02:17,490
 And so it can be quite shocking to realize the sort of the

17
00:02:17,490 --> 00:02:20,480
 depth and the difficulty of

18
00:02:20,480 --> 00:02:31,000
 the practice, difficult not only in the resistance to the

19
00:02:31,000 --> 00:02:34,720
 practice, but difficult in the resistance

20
00:02:34,720 --> 00:02:53,740
 to change, a resistance to accept the magnitude of our

21
00:02:53,740 --> 00:03:00,320
 misunderstandings about the world.

22
00:03:00,320 --> 00:03:18,840
 The magnitude of the depth of our attachments, and how deep

23
00:03:18,840 --> 00:03:20,560
 we have to go to actually become

24
00:03:20,560 --> 00:03:23,560
 free from suffering.

25
00:03:23,560 --> 00:03:35,310
 But on the other hand, it's encouraging to realize how

26
00:03:35,310 --> 00:03:37,040
 profound the practice really is

27
00:03:37,040 --> 00:03:44,760
 and how powerful it is.

28
00:03:44,760 --> 00:03:49,950
 The Buddha taught that it makes you, he said that the

29
00:03:49,950 --> 00:03:53,440
 practice makes you a great being.

30
00:03:53,440 --> 00:04:01,840
 We have this word mahantata.

31
00:04:01,840 --> 00:04:12,160
 In Sanskrit it would be mahatma.

32
00:04:12,160 --> 00:04:19,600
 This word in India was used to describe Gandhi, but it was

33
00:04:19,600 --> 00:04:24,360
 used to describe any great being.

34
00:04:24,360 --> 00:04:36,680
 So if you think of it in those terms, there are very few

35
00:04:36,680 --> 00:04:43,320
 people who don't admire the image

36
00:04:43,320 --> 00:04:51,350
 of Gandhi, the ideal that he was supposed to represent, the

37
00:04:51,350 --> 00:05:05,360
 greatness of mind, of being,

38
00:05:05,360 --> 00:05:10,120
 mahatma.

39
00:05:10,120 --> 00:05:12,620
 I think many people come, most people don't come to

40
00:05:12,620 --> 00:05:14,400
 meditation thinking they're going

41
00:05:14,400 --> 00:05:21,090
 to become someone great or someone famous or powerful or so

42
00:05:21,090 --> 00:05:21,560
.

43
00:05:21,560 --> 00:05:26,530
 You just want to become, at best, want to become good

44
00:05:26,530 --> 00:05:27,600
 people.

45
00:05:27,600 --> 00:05:33,180
 But greatness doesn't have anything to do, of course, with

46
00:05:33,180 --> 00:05:35,640
 power or fame or renown.

47
00:05:35,640 --> 00:05:39,690
 You can be a great person living off in the forest, but it

48
00:05:39,690 --> 00:05:41,840
's important, I think, to think

49
00:05:41,840 --> 00:05:50,520
 in this way because we shouldn't settle for mediocrity.

50
00:05:50,520 --> 00:05:59,120
 Meditation won't allow you to.

51
00:05:59,120 --> 00:06:04,510
 We should see the benefit of being a great person, a great

52
00:06:04,510 --> 00:06:06,720
 being of greatness.

53
00:06:06,720 --> 00:06:12,590
 We should see the importance of true change and true

54
00:06:12,590 --> 00:06:16,400
 spiritual enlightenment as opposed

55
00:06:16,400 --> 00:06:23,930
 to just meditating to feel good, to avoid our problems or

56
00:06:23,930 --> 00:06:29,200
 to escape our problems temporarily.

57
00:06:29,200 --> 00:06:33,270
 If you want to succeed in meditation, you have to be open

58
00:06:33,270 --> 00:06:36,280
 to change and true change,

59
00:06:36,280 --> 00:06:40,240
 true transformation.

60
00:06:40,240 --> 00:06:43,450
 So the Buddha described what it takes to become a great

61
00:06:43,450 --> 00:06:45,600
 person and these are qualities that

62
00:06:45,600 --> 00:06:55,480
 as meditators we would do well to espouse and cultivate.

63
00:06:55,480 --> 00:06:59,450
 Anyone who has these qualities is sure to become a great

64
00:06:59,450 --> 00:07:00,280
 person.

65
00:07:00,280 --> 00:07:09,920
 "Mohantatang vai pula tang," vai pula means deep, profound.

66
00:07:09,920 --> 00:07:24,240
 You will become a great being, a profound being.

67
00:07:24,240 --> 00:07:27,560
 The first one is "Aloka bahulo."

68
00:07:27,560 --> 00:07:40,400
 Meditation has to be full of wisdom.

69
00:07:40,400 --> 00:07:44,000
 Number two, "Yoga bahulo," will be full of yoga.

70
00:07:44,000 --> 00:07:50,880
 Yoga here means energy or effort.

71
00:07:50,880 --> 00:07:53,160
 Number three, "Veda bahulo."

72
00:07:53,160 --> 00:08:03,800
 Veda means feeling, will be full of feeling of zeros, aster

73
00:08:03,800 --> 00:08:06,600
, interest.

74
00:08:06,600 --> 00:08:25,440
 Asantuti bahulo will be full of discontent.

75
00:08:25,440 --> 00:08:44,850
 You will be able to attain greater and greater wholesomen

76
00:08:44,850 --> 00:08:47,320
ess.

77
00:08:47,320 --> 00:08:58,870
 These six things are the dumbest that need one to become a

78
00:08:58,870 --> 00:09:02,120
 great being.

79
00:09:02,120 --> 00:09:09,160
 So briefly, "Aloka bahulo" means wisdom.

80
00:09:09,160 --> 00:09:14,640
 In the meditation practice we have to always be clear in

81
00:09:14,640 --> 00:09:16,000
 our minds.

82
00:09:16,000 --> 00:09:19,920
 We have to use the wisdom that we gain to continue.

83
00:09:19,920 --> 00:09:32,220
 We have to be wise and clever to be clear that the things

84
00:09:32,220 --> 00:09:34,440
 that you experience are impermanent,

85
00:09:34,440 --> 00:09:39,200
 unsatisfying, uncontrollable.

86
00:09:39,200 --> 00:09:44,320
 Because this is what you're going to see when you practice.

87
00:09:44,320 --> 00:09:48,570
 And if you can't recognize that, it becomes a source of

88
00:09:48,570 --> 00:09:51,520
 stress and suffering and difficulty

89
00:09:51,520 --> 00:09:54,920
 in the practice.

90
00:09:54,920 --> 00:09:58,160
 When pain arises, the inability to see it as impermanent,

91
00:09:58,160 --> 00:10:00,800
 unsatisfying, in the sense

92
00:10:00,800 --> 00:10:04,520
 that you can't fix it.

93
00:10:04,520 --> 00:10:09,440
 You can't make it better.

94
00:10:09,440 --> 00:10:14,040
 It doesn't belong to you.

95
00:10:14,040 --> 00:10:17,520
 You can't see this, then it leads you to suffer and doubt

96
00:10:17,520 --> 00:10:19,400
 about the practice and doubt about

97
00:10:19,400 --> 00:10:23,040
 your ability to practice.

98
00:10:23,040 --> 00:10:26,110
 The same goes with pleasant things if you experience

99
00:10:26,110 --> 00:10:30,440
 pleasant sensations, pleasant experiences.

100
00:10:30,440 --> 00:10:38,270
 If you can't see them as transient, as unsatisfying,

101
00:10:38,270 --> 00:10:42,720
 because they're transient.

102
00:10:42,720 --> 00:10:47,950
 Because when they end, they're gone and all you're left

103
00:10:47,950 --> 00:10:50,160
 with is your desire.

104
00:10:50,160 --> 00:10:52,600
 The desire builds, but the pleasure doesn't.

105
00:10:52,600 --> 00:10:56,560
 The pleasure fades.

106
00:10:56,560 --> 00:11:02,880
 You can't see this, then you become discontent.

107
00:11:02,880 --> 00:11:08,630
 More and more wanting for pleasant sensations, pleasant

108
00:11:08,630 --> 00:11:10,320
 experiences.

109
00:11:10,320 --> 00:11:19,370
 As you're less able to maintain and sustain them to a level

110
00:11:19,370 --> 00:11:23,040
 that satisfies you.

111
00:11:23,040 --> 00:11:26,160
 Aloka mahalo, you have to cultivate as much light.

112
00:11:26,160 --> 00:11:28,880
 Aloka means light, but it is light of wisdom.

113
00:11:28,880 --> 00:11:33,490
 Your mind has to be bright and clear to cultivate this

114
00:11:33,490 --> 00:11:35,240
 clarity of mind.

115
00:11:35,240 --> 00:11:40,250
 In the beginning when we practice our minds are very cloud

116
00:11:40,250 --> 00:11:41,680
ed and murky.

117
00:11:41,680 --> 00:11:44,200
 We don't see so clearly.

118
00:11:44,200 --> 00:11:46,840
 It's hard to tell what's right and what's wrong.

119
00:11:46,840 --> 00:11:52,120
 It's hard to tell what is the point of the practice.

120
00:11:52,120 --> 00:11:55,640
 But as you practice, your mind becomes clear.

121
00:11:55,640 --> 00:12:00,840
 As your mind clears, you're able to see the difference.

122
00:12:00,840 --> 00:12:13,350
 You're able to see the benefits of clarity and you're able

123
00:12:13,350 --> 00:12:16,280
 to see the dangers in attachment

124
00:12:16,280 --> 00:12:18,320
 and clinging and aversion.

125
00:12:18,320 --> 00:12:23,160
 You see the causes and effect.

126
00:12:23,160 --> 00:12:26,320
 What leads to what?

127
00:12:26,320 --> 00:12:35,260
 You start to see how your mind works and you can adjust

128
00:12:35,260 --> 00:12:38,160
 accordingly.

129
00:12:38,160 --> 00:12:43,560
 Yoga bahulo, you have to work hard.

130
00:12:43,560 --> 00:12:45,560
 Meditation isn't meant to be easy.

131
00:12:45,560 --> 00:12:51,200
 It's not something you can do when you feel like it.

132
00:12:51,200 --> 00:12:56,600
 You should try to be persistent in the practice.

133
00:12:56,600 --> 00:13:02,640
 You do in a sense have to push yourself to practice.

134
00:13:02,640 --> 00:13:08,600
 Otherwise laziness takes over and your mind falls back into

135
00:13:08,600 --> 00:13:11,520
 old comfortable patterns,

136
00:13:11,520 --> 00:13:16,200
 which is much more comfortable and pleasant.

137
00:13:16,200 --> 00:13:27,990
 But it doesn't stir up, it doesn't dig deep and uproot our

138
00:13:27,990 --> 00:13:31,000
 bad habits.

139
00:13:31,000 --> 00:13:44,210
 You overcome suffering through effort and you have to work

140
00:13:44,210 --> 00:13:46,440
 at it.

141
00:13:46,440 --> 00:13:50,120
 To work at it you have to overcome your aversion to work.

142
00:13:50,120 --> 00:13:56,160
 It makes us cranky when we have to work.

143
00:13:56,160 --> 00:14:02,600
 It's unpleasant, you have to overcome that, you have to

144
00:14:02,600 --> 00:14:04,680
 become strong.

145
00:14:04,680 --> 00:14:08,660
 The natural perfect state is actually quite energetic, but

146
00:14:08,660 --> 00:14:10,720
 our ordinary state is usually

147
00:14:10,720 --> 00:14:12,520
 much more lazy.

148
00:14:12,520 --> 00:14:20,360
 And so it feels somehow stressful to have to work because

149
00:14:20,360 --> 00:14:23,640
 we'd much rather just laze

150
00:14:23,640 --> 00:14:31,320
 around and enjoy ourselves.

151
00:14:31,320 --> 00:14:37,280
 But it's a trap because you become more and more obsessed

152
00:14:37,280 --> 00:14:40,360
 with the pleasantness of lying

153
00:14:40,360 --> 00:14:51,760
 down and sitting in comfortable chairs and seats.

154
00:14:51,760 --> 00:14:56,280
 A great person is able to work at an optimum level.

155
00:14:56,280 --> 00:15:12,960
 It's very little rest, very little comfort.

156
00:15:12,960 --> 00:15:14,280
 The point is to be objective.

157
00:15:14,280 --> 00:15:19,310
 If you're truly objective then you don't mind to be in a

158
00:15:19,310 --> 00:15:22,360
 state of comfort or discomfort.

159
00:15:22,360 --> 00:15:27,200
 It doesn't bother you either way.

160
00:15:27,200 --> 00:15:30,780
 But our ordinary state is only content with comfort and

161
00:15:30,780 --> 00:15:31,720
 pleasure.

162
00:15:31,720 --> 00:15:35,790
 And when we don't have it then we get upset and angry and

163
00:15:35,790 --> 00:15:37,080
 frustrated.

164
00:15:37,080 --> 00:15:40,040
 So clearly we're not objective.

165
00:15:40,040 --> 00:15:45,520
 To be truly objective you have to actually work hard, work

166
00:15:45,520 --> 00:15:48,440
 hard to change that, to bring

167
00:15:48,440 --> 00:15:53,770
 your mind to a truly neutral ground when you're able to be

168
00:15:53,770 --> 00:15:57,400
 happy no matter what the situation,

169
00:15:57,400 --> 00:16:03,040
 whether you have comfort or discomfort.

170
00:16:03,040 --> 00:16:04,040
 Wait about it.

171
00:16:04,040 --> 00:16:06,970
 You have to work hard to overcome unwholesomeness in the

172
00:16:06,970 --> 00:16:09,280
 mind, you have to work hard to cultivate

173
00:16:09,280 --> 00:16:12,280
 wholesomeness.

174
00:16:12,280 --> 00:16:17,600
 Yoga Bahulos, yoga Bahulos.

175
00:16:17,600 --> 00:16:24,800
 Veda Bahulos means feeling, it means putting your heart

176
00:16:24,800 --> 00:16:27,240
 into something.

177
00:16:27,240 --> 00:16:35,920
 You have to practice with your wholeheartedness.

178
00:16:35,920 --> 00:16:39,960
 It means you can't just force yourself to practice.

179
00:16:39,960 --> 00:16:45,360
 You can't just be here because someone told you that you

180
00:16:45,360 --> 00:16:48,360
 should or because you think it's

181
00:16:48,360 --> 00:16:53,920
 the right thing to do.

182
00:16:53,920 --> 00:16:59,400
 If you don't really want to do it, it's very difficult to

183
00:16:59,400 --> 00:17:00,800
 meditate.

184
00:17:00,800 --> 00:17:05,120
 To become great you have to be passionate about what you do

185
00:17:05,120 --> 00:17:05,400
.

186
00:17:05,400 --> 00:17:08,560
 To some extent passionate about meditation.

187
00:17:08,560 --> 00:17:15,180
 It just means you have to be clear in your mind about the

188
00:17:15,180 --> 00:17:18,720
 benefits of meditation.

189
00:17:18,720 --> 00:17:23,790
 And to be very careful because the mind will trick you into

190
00:17:23,790 --> 00:17:26,080
 thinking that you don't need

191
00:17:26,080 --> 00:17:36,050
 to meditate or that it's too much work or that it's too

192
00:17:36,050 --> 00:17:38,280
 stressful or so on.

193
00:17:38,280 --> 00:17:50,840
 It will trick you into thinking that meditation is not the

194
00:17:50,840 --> 00:17:54,840
 right way to go.

195
00:17:54,840 --> 00:17:57,920
 You have to be clear in your mind the benefits.

196
00:17:57,920 --> 00:18:00,240
 Meditation helps to clear the mind.

197
00:18:00,240 --> 00:18:07,300
 Meditation helps you to root out all mental upset, mental

198
00:18:07,300 --> 00:18:11,720
 illness, all mental disturbances.

199
00:18:11,720 --> 00:18:16,040
 It helps you overcome suffering.

200
00:18:16,040 --> 00:18:21,210
 Meditation clears up the attachments that lead us to

201
00:18:21,210 --> 00:18:24,880
 suffering and cause us to fall in distress

202
00:18:24,880 --> 00:18:29,200
 and suffering.

203
00:18:29,200 --> 00:18:31,160
 Meditation leads you to the right path.

204
00:18:31,160 --> 00:18:37,810
 It allows you to live your life however you like without

205
00:18:37,810 --> 00:18:41,920
 worry or stress or fear or anxiety

206
00:18:41,920 --> 00:18:42,920
 without doubt.

207
00:18:42,920 --> 00:18:47,000
 It gives you a clear mind.

208
00:18:47,000 --> 00:18:48,000
 Meditation leads you to freedom.

209
00:18:48,000 --> 00:18:52,000
 We have to put our hearts into it.

210
00:18:52,000 --> 00:18:54,000
 You can't do this half-hearted.

211
00:18:54,000 --> 00:18:59,410
 You have to be very careful to uproot doubt and uncertainty

212
00:18:59,410 --> 00:19:06,160
 and vigilance.

213
00:19:06,160 --> 00:19:12,350
 The uncaring mind, the mind that is uncommitted, you have

214
00:19:12,350 --> 00:19:16,880
 to be committed to the practice.

215
00:19:16,880 --> 00:19:20,680
 Veda Bahu.

216
00:19:20,680 --> 00:19:25,520
 Vahasantudi Bahu means you have to be full of discontent.

217
00:19:25,520 --> 00:19:28,080
 So discontent here, there's two kinds of contentment.

218
00:19:28,080 --> 00:19:31,130
 We have to of course cultivate contentment with our

219
00:19:31,130 --> 00:19:32,160
 experiences.

220
00:19:32,160 --> 00:19:36,750
 But the Buddha said discontent, this means in regards to

221
00:19:36,750 --> 00:19:37,960
 goodness.

222
00:19:37,960 --> 00:19:45,130
 You should never be content with what you have, with who

223
00:19:45,130 --> 00:19:46,600
 you are.

224
00:19:46,600 --> 00:19:50,560
 Be content to keep the unwholesomeness that exists in the

225
00:19:50,560 --> 00:19:52,600
 mind saying, "Oh, it's not

226
00:19:52,600 --> 00:19:53,600
 so bad."

227
00:19:53,600 --> 00:19:57,840
 Or the goodness saying, "Oh, it's good enough."

228
00:19:57,840 --> 00:20:01,430
 If you want to become a great person and really succeed in

229
00:20:01,430 --> 00:20:03,520
 the meditation, you have to be

230
00:20:03,520 --> 00:20:10,180
 willing to give up all unwholesomeness and be ready to

231
00:20:10,180 --> 00:20:15,040
 cultivate perfect wholesomeness.

232
00:20:15,040 --> 00:20:17,800
 The great thing is that this is possible.

233
00:20:17,800 --> 00:20:20,840
 You can become a great person.

234
00:20:20,840 --> 00:20:23,840
 It's there for us to do.

235
00:20:23,840 --> 00:20:32,640
 It's there for us to attain.

236
00:20:32,640 --> 00:20:37,490
 So characteristic of a great person is that they don't rest

237
00:20:37,490 --> 00:20:39,200
 on their laurels.

238
00:20:39,200 --> 00:20:42,520
 They continue to.

239
00:20:42,520 --> 00:20:55,360
 A great person is recognized by their ability to see the

240
00:20:55,360 --> 00:20:58,480
 smallest faults.

241
00:20:58,480 --> 00:21:02,770
 An ordinary person ignores their great faults and dismisses

242
00:21:02,770 --> 00:21:05,280
 them as being inconsequential.

243
00:21:05,280 --> 00:21:12,130
 But a great person is even disturbed by their smallest

244
00:21:12,130 --> 00:21:13,440
 fault.

245
00:21:13,440 --> 00:21:22,630
 There's a difference between an ordinary person and a great

246
00:21:22,630 --> 00:21:24,360
 person.

247
00:21:24,360 --> 00:21:31,390
 Anikita duro means not to ever put down the task, so to not

248
00:21:31,390 --> 00:21:33,440
 take a break.

249
00:21:33,440 --> 00:21:35,760
 In meditation, there really is no need to take a break.

250
00:21:35,760 --> 00:21:39,400
 There's no benefit in taking a break.

251
00:21:39,400 --> 00:21:43,650
 It doesn't mean that you have to be doing formal meditation

252
00:21:43,650 --> 00:21:45,560
, but it means there's no

253
00:21:45,560 --> 00:21:47,920
 reason to give up mindfulness.

254
00:21:47,920 --> 00:21:51,640
 Because even when you're not meditating, no matter what you

255
00:21:51,640 --> 00:21:53,360
're doing, you can always

256
00:21:53,360 --> 00:21:58,800
 work at cultivating clarity of mind.

257
00:21:58,800 --> 00:22:04,680
 When you're eating, you can work to have a clear mind.

258
00:22:04,680 --> 00:22:09,480
 Taking a shower or brushing your teeth.

259
00:22:09,480 --> 00:22:14,240
 On the toilet, you can be working to clear your mind.

260
00:22:14,240 --> 00:22:17,360
 Even when you lie down to sleep at night, you can be

261
00:22:17,360 --> 00:22:19,440
 working to cultivate clarity of

262
00:22:19,440 --> 00:22:20,440
 mind.

263
00:22:20,440 --> 00:22:25,250
 There's no excuse, there's no reason, there's no benefit to

264
00:22:25,250 --> 00:22:26,760
 stopping that.

265
00:22:26,760 --> 00:22:30,830
 So it's another quality of a great person that they don't

266
00:22:30,830 --> 00:22:32,680
 give up, and they don't

267
00:22:32,680 --> 00:22:33,680
 take breaks.

268
00:22:33,680 --> 00:22:41,080
 They're constantly working to improve themselves.

269
00:22:41,080 --> 00:22:45,700
 And the final one, kusalei su dhammiso utari cha patrati

270
00:22:45,700 --> 00:22:48,240
 means not only are they working,

271
00:22:48,240 --> 00:22:50,040
 it's not a linear progress.

272
00:22:50,040 --> 00:22:54,400
 It's just one important point to understand about

273
00:22:54,400 --> 00:22:57,800
 meditation, that it's not something

274
00:22:57,800 --> 00:23:07,080
 that you can just put your head down and plow on.

275
00:23:07,080 --> 00:23:16,060
 You have to be adaptive, to be able to adapt, adaptable, to

276
00:23:16,060 --> 00:23:20,000
 be ready to adapt to new, new

277
00:23:20,000 --> 00:23:21,000
 situations.

278
00:23:21,000 --> 00:23:28,770
 Utari cha patrati means working to further yourself, to

279
00:23:28,770 --> 00:23:31,560
 greater goodness.

280
00:23:31,560 --> 00:23:35,920
 Always looking for new ways to improve yourself.

281
00:23:35,920 --> 00:23:41,920
 And in meditation, this takes special importance because

282
00:23:41,920 --> 00:23:44,720
 your conditions change.

283
00:23:44,720 --> 00:23:48,200
 It's common for a meditator to become complacent thinking

284
00:23:48,200 --> 00:23:50,280
 they've figured it out, they've

285
00:23:50,280 --> 00:23:53,880
 come to understand how to meditate.

286
00:23:53,880 --> 00:23:58,590
 So they work out a problem in the mind, and then they try

287
00:23:58,590 --> 00:24:00,960
 to apply the same technique

288
00:24:00,960 --> 00:24:09,420
 to a new problem and find that the results are different,

289
00:24:09,420 --> 00:24:13,200
 or become complacent.

290
00:24:13,200 --> 00:24:21,440
 You'll find that meditation is like layers of an onion.

291
00:24:21,440 --> 00:24:23,610
 Every time you figure something out, you learn something

292
00:24:23,610 --> 00:24:24,520
 new about yourself.

293
00:24:24,520 --> 00:24:29,680
 There's something new and a little bit different to learn.

294
00:24:29,680 --> 00:24:39,040
 There's always new and greater understanding to be gained.

295
00:24:39,040 --> 00:24:47,600
 And so we have to approach every experience, every

296
00:24:47,600 --> 00:24:53,720
 meditation state with an open mind,

297
00:24:53,720 --> 00:25:00,610
 with a beginner's mind, they say, ready to learn something

298
00:25:00,610 --> 00:25:01,480
 new.

299
00:25:01,480 --> 00:25:04,750
 This quality of mind is important in meditation, the

300
00:25:04,750 --> 00:25:06,840
 ability to adapt to a new situation and

301
00:25:06,840 --> 00:25:09,400
 to not take it for granted.

302
00:25:09,400 --> 00:25:13,010
 Because it's very easy for a meditator to get into a rut

303
00:25:13,010 --> 00:25:15,000
 where they are meditating but

304
00:25:15,000 --> 00:25:21,170
 aren't progressing because they're ignoring new stimuli,

305
00:25:21,170 --> 00:25:23,240
 new information.

306
00:25:23,240 --> 00:25:28,000
 They'll be very confident in dealing with certain

307
00:25:28,000 --> 00:25:31,720
 situations, certain emotions, but

308
00:25:31,720 --> 00:25:36,420
 other emotions they've ignored and they've never really

309
00:25:36,420 --> 00:25:38,760
 learned how to deal with.

310
00:25:38,760 --> 00:25:45,930
 This takes the ability to discern and discriminate, not

311
00:25:45,930 --> 00:25:50,520
 just work hard, but to be able to adjust

312
00:25:50,520 --> 00:26:00,360
 your work according to the task at hand.

313
00:26:00,360 --> 00:26:04,200
 Working to reach a new level of understanding.

314
00:26:04,200 --> 00:26:10,960
 It's not just effort, but you need wisdom and discernment.

315
00:26:10,960 --> 00:26:15,900
 So some good dhammas, I think these are useful for all med

316
00:26:15,900 --> 00:26:18,680
itators, whether we're here in

317
00:26:18,680 --> 00:26:24,850
 the forest practicing intensively or for people in the

318
00:26:24,850 --> 00:26:29,080
 world to think about and to cultivate.

319
00:26:29,080 --> 00:26:35,780
 Because in the end that's the goal, that's the way we're

320
00:26:35,780 --> 00:26:38,760
 headed to greatness.

321
00:26:38,760 --> 00:26:46,880
 It's something that is well worth appreciation, greatly

322
00:26:46,880 --> 00:26:51,560
 appreciated to see so many people

323
00:26:51,560 --> 00:26:57,800
 practicing to become better people.

324
00:26:57,800 --> 00:27:00,640
 These are some of the dhammas that help us do it, help us

325
00:27:00,640 --> 00:27:02,200
 become not only good people

326
00:27:02,200 --> 00:27:09,080
 but great, deep, profound individuals.

