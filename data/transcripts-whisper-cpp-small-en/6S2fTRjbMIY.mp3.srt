1
00:00:00,000 --> 00:00:07,630
 Okay, so what should one do about those that try to convert

2
00:00:07,630 --> 00:00:08,360
 us?

3
00:00:08,360 --> 00:00:12,980
 This seems to happen to me when I'm waiting for the bus for

4
00:00:12,980 --> 00:00:13,160
 anything.

5
00:00:13,160 --> 00:00:18,080
 Some of them won't leave me alone till my bus is there.

6
00:00:18,080 --> 00:00:24,080
 What to do with the crazy, crazy zealots?

7
00:00:24,080 --> 00:00:25,080
 No.

8
00:00:25,080 --> 00:00:34,280
 I've of course come up against this myself.

9
00:00:34,280 --> 00:00:37,910
 The best way, if you can, the best way that I've found is

10
00:00:37,910 --> 00:00:41,880
 to ask them lots of questions.

11
00:00:41,880 --> 00:00:46,110
 Because it helps you to be open-minded and it helps to

12
00:00:46,110 --> 00:00:48,120
 change your, the angst that we

13
00:00:48,120 --> 00:00:50,200
 have, the aversion that we have.

14
00:00:50,200 --> 00:00:52,640
 This what we call patigah.

15
00:00:52,640 --> 00:00:55,800
 Patigah is this kind of annoyance that comes up.

16
00:00:55,800 --> 00:01:01,190
 It becomes annoying or it chafes on us to have to put up

17
00:01:01,190 --> 00:01:02,840
 with people.

18
00:01:02,840 --> 00:01:06,500
 But clearly these sort of people have some problem inside.

19
00:01:06,500 --> 00:01:10,480
 They have some very, very, very strong clinging.

20
00:01:10,480 --> 00:01:14,790
 It's clinging to views and it's clinging to their idea of

21
00:01:14,790 --> 00:01:17,280
 pleasure and happiness and what's

22
00:01:17,280 --> 00:01:19,400
 right.

23
00:01:19,400 --> 00:01:24,050
 They're clinging out of fear as well, clinging out of

24
00:01:24,050 --> 00:01:26,200
 ignorance as well.

25
00:01:26,200 --> 00:01:30,110
 And the best way that, so you consider them as someone with

26
00:01:30,110 --> 00:01:31,520
 a big knot inside.

27
00:01:31,520 --> 00:01:34,560
 They're all twisted up inside and you have to look at this

28
00:01:34,560 --> 00:01:35,960
 and you have to help them

29
00:01:35,960 --> 00:01:37,680
 to untie it.

30
00:01:37,680 --> 00:01:43,300
 And it takes skill, it takes clarity of mind.

31
00:01:43,300 --> 00:01:46,800
 So what I find is the best is to explore it with them.

32
00:01:46,800 --> 00:01:51,890
 They're telling you that X is right or X is the right path

33
00:01:51,890 --> 00:01:54,200
 or if you don't X then you're

34
00:01:54,200 --> 00:01:57,320
 going to Y or you're going to hell.

35
00:01:57,320 --> 00:02:00,240
 You're going to suffer.

36
00:02:00,240 --> 00:02:01,720
 So you can ask them about it.

37
00:02:01,720 --> 00:02:05,450
 That's what I found is the best thing to do because it

38
00:02:05,450 --> 00:02:07,920
 helps to ease their pressure and

39
00:02:07,920 --> 00:02:11,760
 you can sort of help carry their burden around for them.

40
00:02:11,760 --> 00:02:14,160
 Say, "Okay, well let's look at this burden."

41
00:02:14,160 --> 00:02:17,380
 And hopefully when you look at it you'll see that it's less

42
00:02:17,380 --> 00:02:19,080
 useful than you thought.

43
00:02:19,080 --> 00:02:25,240
 It helps to ease up the situation.

44
00:02:25,240 --> 00:02:27,440
 If I keep denying God.

45
00:02:27,440 --> 00:02:31,560
 Yeah, I guess you get that.

46
00:02:31,560 --> 00:02:36,320
 I don't think I've ever gotten it quite like that.

47
00:02:36,320 --> 00:02:40,280
 I've never had anyone tell me that I'm going to hell.

48
00:02:40,280 --> 00:02:43,740
 But that's what they're thinking, right?

49
00:02:43,740 --> 00:02:46,600
 Like if you talk to them about it and ask them ... No, I

50
00:02:46,600 --> 00:02:48,160
 was asking.

51
00:02:48,160 --> 00:02:49,160
 It was funny.

52
00:02:49,160 --> 00:02:51,560
 I have a funny story that I'll tell you and this is

53
00:02:51,560 --> 00:02:53,320
 hopefully a little bit of support

54
00:02:53,320 --> 00:02:59,080
 for the idea of asking questions because I went to this

55
00:02:59,080 --> 00:03:02,000
 evangelical church that was ... I've

56
00:03:02,000 --> 00:03:04,400
 told this story before but some of you may have heard it

57
00:03:04,400 --> 00:03:06,120
 but let's start at the beginning.

58
00:03:06,120 --> 00:03:09,940
 I was teaching meditation in Los Angeles at a Thai

59
00:03:09,940 --> 00:03:11,160
 monastery.

60
00:03:11,160 --> 00:03:15,190
 There were some English teachers that were teaching the

61
00:03:15,190 --> 00:03:17,520
 monks English in the monastery

62
00:03:17,520 --> 00:03:18,840
 and I said, "Well, bring them down.

63
00:03:18,840 --> 00:03:19,840
 Some Westerners.

64
00:03:19,840 --> 00:03:20,840
 Let's teach them meditation."

65
00:03:20,840 --> 00:03:23,440
 They're like, "I don't think they'll be interested."

66
00:03:23,440 --> 00:03:24,440
 "Well, why not?

67
00:03:24,440 --> 00:03:25,440
 Bring them down."

68
00:03:25,440 --> 00:03:28,520
 "Well, they're actually evangelical Christians."

69
00:03:28,520 --> 00:03:31,760
 I'm like, "Oh, well bring them down.

70
00:03:31,760 --> 00:03:32,760
 Christians can meditate.

71
00:03:32,760 --> 00:03:33,760
 Why not?"

72
00:03:33,760 --> 00:03:37,550
 Turns out by the looks of it their only reason for going

73
00:03:37,550 --> 00:03:40,320
 there was to convert everyone because

74
00:03:40,320 --> 00:03:46,640
 they were totally ... patigah is the word.

75
00:03:46,640 --> 00:03:53,680
 They were averse to me and cold.

76
00:03:53,680 --> 00:03:57,040
 A couple of them were nice but their leader, there was the

77
00:03:57,040 --> 00:03:59,240
 leader of them and I said, "Well,

78
00:03:59,240 --> 00:04:00,240
 why don't you take my card?"

79
00:04:00,240 --> 00:04:01,680
 She's like, "No thanks.

80
00:04:01,680 --> 00:04:02,680
 We don't want your card."

81
00:04:02,680 --> 00:04:05,040
 I'm like, "Whoa."

82
00:04:05,040 --> 00:04:07,280
 It was that severe.

83
00:04:07,280 --> 00:04:10,360
 I talked to them a little bit and tried to open them up and

84
00:04:10,360 --> 00:04:11,920
 just explain a little bit

85
00:04:11,920 --> 00:04:13,640
 about meditation.

86
00:04:13,640 --> 00:04:17,280
 I think in the end a couple of them took my card.

87
00:04:17,280 --> 00:04:21,280
 Next night the leader comes back with her husband who works

88
00:04:21,280 --> 00:04:23,140
 at the evangelical parish

89
00:04:23,140 --> 00:04:26,560
 or whatever you call it.

90
00:04:26,560 --> 00:04:29,480
 He had a Bible with him and their kids came as well.

91
00:04:29,480 --> 00:04:32,250
 Their kids were a real trip because they had trained their

92
00:04:32,250 --> 00:04:33,600
 kids to say all these crazy

93
00:04:33,600 --> 00:04:35,680
 things.

94
00:04:35,680 --> 00:04:38,540
 I was asking him and I was asking his kids and he said, "Go

95
00:04:38,540 --> 00:04:38,920
 ahead.

96
00:04:38,920 --> 00:04:39,920
 You can tell him."

97
00:04:39,920 --> 00:04:42,390
 The kid would say, "If you don't believe in God, you're

98
00:04:42,390 --> 00:04:43,280
 going to hell."

99
00:04:43,280 --> 00:04:49,120
 Whatever they were saying, all of these crazy things that

100
00:04:49,120 --> 00:04:50,240
 they say.

101
00:04:50,240 --> 00:04:51,240
 We really nailed them.

102
00:04:51,240 --> 00:04:55,900
 I was with a Jewish friend, a Jewish Hindu friend who's ...

103
00:04:55,900 --> 00:04:57,760
 She's really a trip and she

104
00:04:57,760 --> 00:05:01,110
 had some good arguments as well and we were just arguing

105
00:05:01,110 --> 00:05:02,000
 with them.

106
00:05:02,000 --> 00:05:05,440
 The argument didn't ... It didn't help them at all.

107
00:05:05,440 --> 00:05:10,170
 It created more aversion in their mind and it widened the

108
00:05:10,170 --> 00:05:12,360
 gap between us really.

109
00:05:12,360 --> 00:05:16,200
 It didn't end the night on ... We felt good because we had

110
00:05:16,200 --> 00:05:17,200
 won the argument.

111
00:05:17,200 --> 00:05:21,400
 What is that except conceit and attachment?

112
00:05:21,400 --> 00:05:23,110
 The next morning I was thinking about this and I said, "

113
00:05:23,110 --> 00:05:24,240
Well, if they're going to come

114
00:05:24,240 --> 00:05:28,480
 and visit me, then I have a right to go visit them."

115
00:05:28,480 --> 00:05:29,480
 I did.

116
00:05:29,480 --> 00:05:32,560
 I walked the next day after my alms round.

117
00:05:32,560 --> 00:05:34,740
 On the way back from alms or after I ate or I can't

118
00:05:34,740 --> 00:05:36,560
 remember, I don't know because I ate

119
00:05:36,560 --> 00:05:39,110
 on alms round and then when I came back I said, "Okay, I'm

120
00:05:39,110 --> 00:05:40,420
 going to go down to the parish

121
00:05:40,420 --> 00:05:44,880
 and walk down there."

122
00:05:44,880 --> 00:05:52,170
 And went in and there was a guy up on the dais or whatever

123
00:05:52,170 --> 00:05:55,520
 talking and the hall was

124
00:05:55,520 --> 00:05:58,480
 full of people.

125
00:05:58,480 --> 00:06:03,240
 I stood in the back and this couple of people were standing

126
00:06:03,240 --> 00:06:05,720
 in the back and I think I talked

127
00:06:05,720 --> 00:06:10,470
 with him or I can't remember how it came up but it turns

128
00:06:10,470 --> 00:06:14,040
 out it was a teaching of pastors.

129
00:06:14,040 --> 00:06:16,960
 This guy was a teacher of pastors.

130
00:06:16,960 --> 00:06:17,960
 I don't know what they call him.

131
00:06:17,960 --> 00:06:21,450
 He was teaching the teachers or he was a pastor to the

132
00:06:21,450 --> 00:06:23,920
 pastors or something like that.

133
00:06:23,920 --> 00:06:30,320
 Came down from San Francisco and he went on and on and I

134
00:06:30,320 --> 00:06:34,080
 was just kind of listening and

135
00:06:34,080 --> 00:06:36,900
 then this guy beside me hands me a Bible and he said, "You

136
00:06:36,900 --> 00:06:37,840
 can keep that."

137
00:06:37,840 --> 00:06:41,450
 It was a nice little Bible with gold trims, not a cheap

138
00:06:41,450 --> 00:06:42,100
 item.

139
00:06:42,100 --> 00:06:44,240
 It was his Bible that he was giving me.

140
00:06:44,240 --> 00:06:45,840
 So they're nice people.

141
00:06:45,840 --> 00:06:54,800
 They have an agenda but they have a big heart in many ways.

142
00:06:54,800 --> 00:06:56,670
 So I was reading through it and then I started reading

143
00:06:56,670 --> 00:06:58,020
 through other parts and then it kind

144
00:06:58,020 --> 00:07:00,700
 of wound down and I went outside and sat on a bench and

145
00:07:00,700 --> 00:07:02,540
 started reading and I was reading

146
00:07:02,540 --> 00:07:07,560
 some passages and looking up because I had heard that God

147
00:07:07,560 --> 00:07:09,880
 is love so I was looking it

148
00:07:09,880 --> 00:07:16,900
 up and I can't remember the whole thing but then the guy

149
00:07:16,900 --> 00:07:20,600
 comes out, the guy bursts out

150
00:07:20,600 --> 00:07:28,270
 the doors and bears down on me with both barrels loaded and

151
00:07:28,270 --> 00:07:33,120
 he said, "What an opportunity."

152
00:07:33,120 --> 00:07:36,000
 He said, "I saw you in the back."

153
00:07:36,000 --> 00:07:37,000
 And so on and so on.

154
00:07:37,000 --> 00:07:41,920
 The Bible that was this, you can see, was this big.

155
00:07:41,920 --> 00:07:42,920
 It was huge.

156
00:07:42,920 --> 00:07:48,880
 He had the Bible's Bible and he drops it on the bench

157
00:07:48,880 --> 00:07:52,400
 beside me and puts his foot up on

158
00:07:52,400 --> 00:07:55,260
 the bench or something like that and it was just like, "

159
00:07:55,260 --> 00:07:56,960
What an opportunity to convert

160
00:07:56,960 --> 00:07:59,080
 me or something."

161
00:07:59,080 --> 00:08:03,020
 And so this was the example of how to deal with these

162
00:08:03,020 --> 00:08:05,440
 people is I started asking him

163
00:08:05,440 --> 00:08:08,120
 questions because here I had a Bible and I was like, "Great

164
00:08:08,120 --> 00:08:09,400
, here I have the teacher

165
00:08:09,400 --> 00:08:12,510
 of the pastors right in front of me and I can ask him

166
00:08:12,510 --> 00:08:13,560
 questions."

167
00:08:13,560 --> 00:08:17,500
 So I asked him some fairly, no, not terribly pointed

168
00:08:17,500 --> 00:08:20,880
 because you don't want to sound threatening

169
00:08:20,880 --> 00:08:21,880
 to them.

170
00:08:21,880 --> 00:08:24,700
 You don't want to seem to be just asking questions for the

171
00:08:24,700 --> 00:08:26,160
 sake of asking questions.

172
00:08:26,160 --> 00:08:29,320
 I was really interested and he told me some really weird

173
00:08:29,320 --> 00:08:33,000
 things like how we die twice.

174
00:08:33,000 --> 00:08:35,830
 There was something about how God is love and I said, "If

175
00:08:35,830 --> 00:08:37,240
 God is love, then why does

176
00:08:37,240 --> 00:08:40,440
 he send you to hell?" or something like that.

177
00:08:40,440 --> 00:08:43,360
 And then he went on about how God is also truth or

178
00:08:43,360 --> 00:08:44,320
 something.

179
00:08:44,320 --> 00:08:47,600
 I don't know how God loves us and therefore he has to send

180
00:08:47,600 --> 00:08:49,360
 us to hell, something like

181
00:08:49,360 --> 00:08:50,360
 that.

182
00:08:50,360 --> 00:08:53,550
 I mean, what's going on here is you're starting to point

183
00:08:53,550 --> 00:08:55,880
 out to them or let them show themselves

184
00:08:55,880 --> 00:08:59,280
 the inconsistencies because in trying to explain such a

185
00:08:59,280 --> 00:09:01,960
 thing, you have to run so many somersaults

186
00:09:01,960 --> 00:09:07,340
 that it gets tiring and you go through the inconsistencies.

187
00:09:07,340 --> 00:09:09,080
 You're forced through them.

188
00:09:09,080 --> 00:09:11,920
 You're forced to feel the inconsistency of your argument

189
00:09:11,920 --> 00:09:13,900
 and that's a powerful thing.

190
00:09:13,900 --> 00:09:17,440
 So I did this with him a few times and in a few ways.

191
00:09:17,440 --> 00:09:21,080
 I asked him about children.

192
00:09:21,080 --> 00:09:22,400
 I said, "What about children?

193
00:09:22,400 --> 00:09:24,280
 Do they go to hell?"

194
00:09:24,280 --> 00:09:25,280
 No.

195
00:09:25,280 --> 00:09:27,280
 No, I didn't ask him.

196
00:09:27,280 --> 00:09:30,850
 I said, "Well, what about a person in a country where they

197
00:09:30,850 --> 00:09:32,760
've never heard of Jesus?

198
00:09:32,760 --> 00:09:36,220
 What if they die without having taken Jesus into their

199
00:09:36,220 --> 00:09:40,000
 heart or something about a child?"

200
00:09:40,000 --> 00:09:42,890
 And he said, "Well, yeah, according to the Bible, they go

201
00:09:42,890 --> 00:09:43,920
 to hell as well."

202
00:09:43,920 --> 00:09:47,120
 And I said, "Well, that doesn't sound really fair, does it

203
00:09:47,120 --> 00:09:47,200
?"

204
00:09:47,200 --> 00:09:51,080
 And he said something like, "No, no, it doesn't."

205
00:09:51,080 --> 00:09:53,480
 I can't remember exactly how it went, but it was so absurd.

206
00:09:53,480 --> 00:09:55,650
 It was like this is the guy who's supposed to be teaching

207
00:09:55,650 --> 00:09:56,960
 this stuff and he doesn't even

208
00:09:56,960 --> 00:09:59,480
 think it's fair.

209
00:09:59,480 --> 00:10:01,520
 And then we were talking about faith.

210
00:10:01,520 --> 00:10:06,320
 In the end, it finally came down to this where I really got

211
00:10:06,320 --> 00:10:07,000
 him.

212
00:10:07,000 --> 00:10:10,480
 So I was asking him about faith and he was saying, "The

213
00:10:10,480 --> 00:10:12,560
 point in Christianity or in his

214
00:10:12,560 --> 00:10:16,480
 Christianity is that it's faith.

215
00:10:16,480 --> 00:10:20,000
 Faith is what determines whether you're Christian or not

216
00:10:20,000 --> 00:10:22,240
 and it's because it's because of faith

217
00:10:22,240 --> 00:10:24,040
 that works, come," or so on.

218
00:10:24,040 --> 00:10:29,840
 So he has this argument of faith over works.

219
00:10:29,840 --> 00:10:34,740
 So he said, "When you really, really become Christian is

220
00:10:34,740 --> 00:10:37,280
 when you have this unwavering

221
00:10:37,280 --> 00:10:40,200
 faith or a person who has truly accepted Christ."

222
00:10:40,200 --> 00:10:42,550
 He said, "It's something like those people in Africa or in

223
00:10:42,550 --> 00:10:43,900
 countries where they've never

224
00:10:43,900 --> 00:10:46,720
 heard of Jesus Christ, they've already hardened their

225
00:10:46,720 --> 00:10:47,800
 hearts to Jesus."

226
00:10:47,800 --> 00:10:50,950
 And he said, "For a person who's truly accepted Jesus into

227
00:10:50,950 --> 00:10:53,040
 their hearts, they have unwavering

228
00:10:53,040 --> 00:10:55,200
 faith."

229
00:10:55,200 --> 00:10:58,200
 And so I asked him, I said, "So what about your faith?

230
00:10:58,200 --> 00:11:00,920
 Does your faith ever waver?"

231
00:11:00,920 --> 00:11:03,760
 And you could tell, I could see it in him.

232
00:11:03,760 --> 00:11:06,400
 He just went like this.

233
00:11:06,400 --> 00:11:10,190
 His mind started wavering in just a split second and then

234
00:11:10,190 --> 00:11:12,400
 he said, "Well, for myself,

235
00:11:12,400 --> 00:11:15,000
 no, I'd have to say it doesn't waver."

236
00:11:15,000 --> 00:11:19,080
 But he was like wavering all over the place.

237
00:11:19,080 --> 00:11:22,230
 And so I just left him at that and thinking to myself, "Wow

238
00:11:22,230 --> 00:11:25,280
, this is the teacher's teacher."

239
00:11:25,280 --> 00:11:27,560
 They don't really have that much.

240
00:11:27,560 --> 00:11:30,470
 And if you just go through it with them, it's kind of fun

241
00:11:30,470 --> 00:11:31,520
 actually.

242
00:11:31,520 --> 00:11:35,240
 I went through with some Mormon kids through this.

243
00:11:35,240 --> 00:11:37,880
 Later on I realized there was something to this.

244
00:11:37,880 --> 00:11:41,130
 And so I talked with the Mormons and actually it was really

245
00:11:41,130 --> 00:11:41,480
 interesting.

246
00:11:41,480 --> 00:11:44,950
 Mormons are like, they don't really have many beliefs at

247
00:11:44,950 --> 00:11:45,440
 all.

248
00:11:45,440 --> 00:11:48,320
 They were talking about how God went through all that we

249
00:11:48,320 --> 00:11:49,880
 have to go through and that's

250
00:11:49,880 --> 00:11:50,880
 how he became God.

251
00:11:50,880 --> 00:11:53,480
 And I was like, "Well, that's kind of interesting."

252
00:11:53,480 --> 00:11:57,840
 And a lot of what they were saying was actually compatible

253
00:11:57,840 --> 00:12:00,320
 with a never ending universe or

254
00:12:00,320 --> 00:12:04,520
 with some sara.

255
00:12:04,520 --> 00:12:06,800
 So I think, well, that's my answer to that question.

256
00:12:06,800 --> 00:12:08,480
 Hopefully that's helpful in some way.

