1
00:00:00,000 --> 00:00:07,620
 Welcome to our evening Dhamma. Tonight we're talking about

2
00:00:07,620 --> 00:00:13,920
 right concentration.

3
00:00:13,920 --> 00:00:18,410
 Right concentration. And there's been some talk of how the

4
00:00:18,410 --> 00:00:20,680
 word concentration isn't probably

5
00:00:20,680 --> 00:00:29,330
 the right translation. It isn't probably the best word to

6
00:00:29,330 --> 00:00:31,160
 use. I don't think it's that

7
00:00:31,160 --> 00:00:37,470
 bad. I think obviously when we talk about Samadhi, we're

8
00:00:37,470 --> 00:00:40,120
 talking about it in contrast

9
00:00:40,120 --> 00:00:45,300
 or in complement to effort. So if you have lots of effort

10
00:00:45,300 --> 00:00:47,540
 but no concentration, you get

11
00:00:47,540 --> 00:00:52,780
 distracted. If you have lots of concentration but not

12
00:00:52,780 --> 00:00:56,240
 enough effort, you fall asleep. I

13
00:00:56,240 --> 00:01:01,340
 don't think it's, I think it's pretty clear what we're

14
00:01:01,340 --> 00:01:05,880
 talking about. And to that extent,

15
00:01:05,880 --> 00:01:09,440
 concentration isn't one of those things that you can't get

16
00:01:09,440 --> 00:01:11,920
 enough of. I mean, only mindfulness

17
00:01:11,920 --> 00:01:19,390
 really works in that way that you want to constantly be

18
00:01:19,390 --> 00:01:23,320
 developing it. And so there's

19
00:01:23,320 --> 00:01:28,590
 quite a bit of debate over the role of concentration and

20
00:01:28,590 --> 00:01:33,380
 the nature of right concentration. Something

21
00:01:33,380 --> 00:01:36,660
 that I could go on about for quite a while and I have

22
00:01:36,660 --> 00:01:39,200
 talked about. And it's something

23
00:01:39,200 --> 00:01:46,810
 that is hotly debated. But the gist of it is that the

24
00:01:46,810 --> 00:01:50,160
 Buddha talked often about specific

25
00:01:50,160 --> 00:01:57,110
 types of concentration which he called jhanas. Jhana means

26
00:01:57,110 --> 00:02:00,760
 something like meditation. The

27
00:02:00,760 --> 00:02:07,900
 word itself is used in several different ways. It's used to

28
00:02:07,900 --> 00:02:11,600
 describe when the bodhisattva

29
00:02:11,600 --> 00:02:17,760
 stopped breathing. He practiced the jhana of not breathing.

30
00:02:17,760 --> 00:02:18,480
 So what it just means is

31
00:02:18,480 --> 00:02:21,920
 he did some sort of meditation probably isn't the right

32
00:02:21,920 --> 00:02:24,000
 word but we use it in that sense.

33
00:02:24,000 --> 00:02:34,060
 It was a development or a jhana. He entered into this fixed

34
00:02:34,060 --> 00:02:37,920
 state of, he fixed himself

35
00:02:37,920 --> 00:02:42,280
 on. The Buddha would just, when he wanted the monks to med

36
00:02:42,280 --> 00:02:50,680
itate he would say jhata bhikkhoy.

37
00:02:50,680 --> 00:03:00,870
 I think the same root as jhana. Jhana is the noun. Jayati

38
00:03:00,870 --> 00:03:06,720
 is the verb. Jayata, meditate.

39
00:03:06,720 --> 00:03:13,410
 Focus your attention. But the Buddha talked about four jhan

40
00:03:13,410 --> 00:03:15,480
as and there are eight or nine

41
00:03:15,480 --> 00:03:21,990
 jhanas in total but most often he talked about four of them

42
00:03:21,990 --> 00:03:24,200
. So there's a lot of debate over

43
00:03:24,200 --> 00:03:28,440
 what exactly these mean and I think with good reason

44
00:03:28,440 --> 00:03:31,320
 because it's not entirely clear or

45
00:03:31,320 --> 00:03:36,080
 entirely certain. I mean there's room clearly for

46
00:03:36,080 --> 00:03:40,160
 interpretation and there's the rub because

47
00:03:40,160 --> 00:03:47,960
 we'll interpret them differently. But briefly the jhanas

48
00:03:47,960 --> 00:03:52,560
 involve states of wholesome concentration

49
00:03:52,560 --> 00:03:58,070
 where the mind is removed from worldly affairs. There's

50
00:03:58,070 --> 00:04:01,360
 some sense that in certain types of

51
00:04:01,360 --> 00:04:05,390
 jhana, and I would personally argue that there are many

52
00:04:05,390 --> 00:04:07,920
 ways of talking about jhana, but

53
00:04:07,920 --> 00:04:11,430
 in certain types of jhana one doesn't even hear anything or

54
00:04:11,430 --> 00:04:13,320
 see anything. It's totally

55
00:04:13,320 --> 00:04:19,750
 oblivious to the world around. One is so focused on a

56
00:04:19,750 --> 00:04:24,560
 single object that the rest of the world

57
00:04:24,560 --> 00:04:30,730
 disappears. So that's really a trance state. We talk about

58
00:04:30,730 --> 00:04:33,760
 the jhanas, we often refer to

59
00:04:33,760 --> 00:04:39,650
 these trance-like states. And why there are four is because

60
00:04:39,650 --> 00:04:42,560
 they become increasingly subtle.

61
00:04:42,560 --> 00:04:52,320
 So the first jhana one is observing the object. One's mind

62
00:04:52,320 --> 00:04:57,280
 is intent upon perceiving the object

63
00:04:57,280 --> 00:05:02,900
 again and again and again and contemplating it. So there's

64
00:05:02,900 --> 00:05:05,520
 this sense of approaching the

65
00:05:05,520 --> 00:05:11,520
 object and kind of sticking with it. That's the first jhana

66
00:05:11,520 --> 00:05:12,960
. It's also accompanied by

67
00:05:12,960 --> 00:05:21,010
 rapture, so there's a sense of ecstasy involved, excitement

68
00:05:21,010 --> 00:05:25,760
 in a sense, this charge, this energy

69
00:05:25,760 --> 00:05:29,990
 if you will. There's happiness and there's single-pointed

70
00:05:29,990 --> 00:05:32,160
ness. So the mind is continuously

71
00:05:32,160 --> 00:05:37,540
 focused on a single object. That's the first jhana. In the

72
00:05:37,540 --> 00:05:40,000
 second jhana there's no more

73
00:05:40,000 --> 00:05:42,290
 thinking about the object in the sense of sending the mind

74
00:05:42,290 --> 00:05:43,560
 out to the object. The mind

75
00:05:43,560 --> 00:05:49,520
 is stuck on the object and it's so constant that there's no

76
00:05:49,520 --> 00:05:52,880
 need for application or contemplation.

77
00:05:52,880 --> 00:05:56,460
 The mind is simply with the object, one with the object if

78
00:05:56,460 --> 00:05:58,960
 you will. It's the second jhana.

79
00:05:58,960 --> 00:06:06,440
 So the first two factors disappear what we call vitaka and

80
00:06:06,440 --> 00:06:08,320
 vitara. But there's still

81
00:06:08,320 --> 00:06:10,720
 rapture, there's still happiness and there's still single-

82
00:06:10,720 --> 00:06:12,800
pointedness. In the third jhana

83
00:06:12,800 --> 00:06:16,140
 rapture disappears, so there's not this excitement. The

84
00:06:16,140 --> 00:06:19,400
 mind becomes more refined and it's just

85
00:06:19,400 --> 00:06:25,050
 fixed and focused on the object. That's the third jhana. In

86
00:06:25,050 --> 00:06:27,160
 the fourth jhana, in the third

87
00:06:27,160 --> 00:06:32,840
 jhana there's still happiness and single-pointedness. And

88
00:06:32,840 --> 00:06:36,680
 in the fifth jhana happiness disappears.

89
00:06:36,680 --> 00:06:39,900
 But it doesn't mean there's no feeling. Happiness is

90
00:06:39,900 --> 00:06:41,880
 replaced by equanimity. So there's still

91
00:06:41,880 --> 00:06:49,930
 these two factors of upeka and ekagata, equanimity and one-

92
00:06:49,930 --> 00:06:53,720
pointedness. So the idea is that through

93
00:06:53,720 --> 00:06:57,560
 practicing focused meditation, what we call samatha

94
00:06:57,560 --> 00:07:00,600
 meditation generally, you enter into

95
00:07:00,600 --> 00:07:03,910
 more and more refined states. And it's usually based on a

96
00:07:03,910 --> 00:07:05,960
 conceptual object and this is why

97
00:07:05,960 --> 00:07:10,720
 we kind of limit our interest in samatha meditation. Not

98
00:07:10,720 --> 00:07:13,720
 because it's not helpful, it's actually

99
00:07:13,720 --> 00:07:16,720
 quite powerful. And the Buddha talked about how this type

100
00:07:16,720 --> 00:07:18,440
 of meditation can lead to magical

101
00:07:18,440 --> 00:07:22,470
 powers because you're very much in control of the mind. But

102
00:07:22,470 --> 00:07:24,280
 the key there is control.

103
00:07:24,280 --> 00:07:28,450
 It ends up being a bit of a problem because you can get

104
00:07:28,450 --> 00:07:30,280
 stuck on it. You can get caught

105
00:07:30,280 --> 00:07:35,380
 up in these powers. As Ajahn Chah said, it's based on

106
00:07:35,380 --> 00:07:37,880
 clinging. I don't know if I

107
00:07:37,880 --> 00:07:41,150
 would use such strong language, but it's interesting that

108
00:07:41,150 --> 00:07:42,680
 he would say such a thing when

109
00:07:42,680 --> 00:07:47,720
 many of his followers are adamant that it being the path to

110
00:07:47,720 --> 00:07:49,480
 enlightenment.

111
00:07:49,480 --> 00:07:54,410
 So this is one way of practicing and it's often referred to

112
00:07:54,410 --> 00:07:56,520
 as right concentration. The Buddha

113
00:07:56,520 --> 00:08:00,440
 certainly, that's when he talked about right concentration,

114
00:08:00,440 --> 00:08:03,160
 he most often talked about these

115
00:08:03,160 --> 00:08:06,600
 four states of mind. We'll get back to that in a second.

116
00:08:06,600 --> 00:08:10,920
 When we enter into these states in

117
00:08:10,920 --> 00:08:13,700
 ordinary meditation, we have to remember again, when we

118
00:08:13,700 --> 00:08:15,560
 talk about the noble path, we're talking

119
00:08:15,560 --> 00:08:19,360
 about this moment where your mind is perfect. That moment

120
00:08:19,360 --> 00:08:22,200
 can't come through samatha meditation.

121
00:08:23,480 --> 00:08:26,440
 Samatha meditation isn't that moment. So to talk about

122
00:08:26,440 --> 00:08:29,720
 those states of ordinary meditation

123
00:08:29,720 --> 00:08:33,860
 as though they are right concentration is just clearly

124
00:08:33,860 --> 00:08:38,280
 wrong. Those states are a precursor

125
00:08:38,280 --> 00:08:43,430
 potentially, like a practice, if you will. They certainly

126
00:08:43,430 --> 00:08:46,920
 get the mind more focused and prepare

127
00:08:46,920 --> 00:08:51,240
 one for the ultimate focus on the four noble truths and nib

128
00:08:51,240 --> 00:08:53,640
hana, but they aren't and shouldn't be

129
00:08:53,640 --> 00:08:57,950
 mistaken for enlightenment. So this is one way of

130
00:08:57,950 --> 00:09:02,360
 approaching the path where you develop these first

131
00:09:02,360 --> 00:09:07,500
 and based on them, then insight comes after. When insight

132
00:09:07,500 --> 00:09:09,320
 and concentration are samatha and

133
00:09:09,320 --> 00:09:17,560
 nibhasana are both perfect, that's enlightenment. That's

134
00:09:17,560 --> 00:09:18,840
 one way. The way we do it, and really the

135
00:09:18,840 --> 00:09:21,570
 way a lot of people talk about it, we argue, we hear these

136
00:09:21,570 --> 00:09:23,480
 arguments, but in the end if you listen

137
00:09:23,480 --> 00:09:25,480
 to people, they're mostly talking about the same thing.

138
00:09:25,480 --> 00:09:26,920
 They're mostly not talking about that.

139
00:09:26,920 --> 00:09:32,390
 Most traditions, especially the ones that argue about this,

140
00:09:32,390 --> 00:09:36,280
 don't practice that type of meditation.

141
00:09:36,280 --> 00:09:39,670
 They practice meditation where you're developing insight

142
00:09:39,670 --> 00:09:42,200
 and tranquility in union. That Zhan

143
00:09:42,200 --> 00:09:45,290
 Chah was big on this, just to use him not because I follow

144
00:09:45,290 --> 00:09:46,840
 his teachings, but because

145
00:09:46,840 --> 00:09:49,470
 a lot of people on the other side of this argument do

146
00:09:49,470 --> 00:09:51,240
 follow his teachings. Imagine

147
00:09:51,240 --> 00:09:54,160
 Zhan Chah was clear that they should come together. He said

148
00:09:54,160 --> 00:09:58,200
 various things. I won't use him as a,

149
00:09:58,200 --> 00:10:03,400
 he's not my favorite authority, source of authority. There

150
00:10:03,400 --> 00:10:05,160
 were monks that were far more,

151
00:10:05,160 --> 00:10:09,150
 I think, versed in the actual texts if you want to find an

152
00:10:09,150 --> 00:10:11,320
 authority of what's in the text.

153
00:10:11,320 --> 00:10:15,720
 And I go a little bit more towards that.

154
00:10:15,720 --> 00:10:27,640
 If you practice in this way, then they come together. You

155
00:10:27,640 --> 00:10:29,080
 never really enter into these

156
00:10:29,080 --> 00:10:33,040
 trance states where you're blissful and peaceful. There may

157
00:10:33,040 --> 00:10:37,000
 come bliss and peace, but you're

158
00:10:37,000 --> 00:10:41,840
 cultivating a different type of Jhana, and the Jhana doesn

159
00:10:41,840 --> 00:10:44,040
't come. When people talk about the

160
00:10:44,040 --> 00:10:47,560
 Jhanas as being right concentration, being necessary,

161
00:10:47,560 --> 00:10:49,480
 totally agree. I think it's a

162
00:10:49,480 --> 00:10:53,240
 problem that we argue about this when in fact the orthodoxy

163
00:10:53,240 --> 00:10:56,280
 is quite clear. When you get to Nibana,

164
00:10:57,400 --> 00:10:59,640
 at that moment there's no question you're in the Jhana.

165
00:10:59,640 --> 00:11:02,040
 There's no question you're in one of the

166
00:11:02,040 --> 00:11:05,290
 four Jhanas. Usually, in our case, it would be the first Jh

167
00:11:05,290 --> 00:11:07,240
ana, or it would be considered the

168
00:11:07,240 --> 00:11:11,370
 first Jhana because you haven't cultivated the other Jhanas

169
00:11:11,370 --> 00:11:13,880
. But at that moment it's a Jhana

170
00:11:13,880 --> 00:11:22,230
 based on Nibana, and that's right concentration. So to be

171
00:11:22,230 --> 00:11:26,600
 clear, the Buddha seems most often to

172
00:11:26,600 --> 00:11:31,500
 have talked about is these mundane Jhana. Most often that's

173
00:11:31,500 --> 00:11:34,440
 understood to be a trance state

174
00:11:34,440 --> 00:11:37,920
 that involves a lot of magic and exalted states can even

175
00:11:37,920 --> 00:11:40,040
 lead you to the Brahma realms.

176
00:11:40,040 --> 00:11:44,400
 So the Buddha had this sort of comprehensive practice that

177
00:11:44,400 --> 00:11:46,120
 he got his monks to undertake.

178
00:11:46,120 --> 00:11:52,600
 It was quite powerful, but it was also completely mundane.

179
00:11:53,640 --> 00:11:56,800
 His magical powers certainly aren't enlightenment, and the

180
00:11:56,800 --> 00:12:00,680
 states of trance also aren't enlightenment.

181
00:12:00,680 --> 00:12:03,350
 But they're powerful and they're supportive, they're wholes

182
00:12:03,350 --> 00:12:03,720
ome.

183
00:12:03,720 --> 00:12:10,160
 But if you look at the wording of the various Jhanas, it

184
00:12:10,160 --> 00:12:11,640
 seems often as well

185
00:12:11,640 --> 00:12:14,440
 the Buddha was talking about something a little different.

186
00:12:14,440 --> 00:12:16,760
 He was talking about this practice of

187
00:12:16,760 --> 00:12:20,010
 samatha and vipassana together because you're mindful. So

188
00:12:20,010 --> 00:12:23,480
 it appears in some sense that

189
00:12:23,480 --> 00:12:28,010
 one can practice Jhana based on ultimate reality, which

190
00:12:28,010 --> 00:12:30,600
 makes sense because of course the word Jhana

191
00:12:30,600 --> 00:12:36,210
 just means meditation. So the commentary picks up on this

192
00:12:36,210 --> 00:12:39,480
 and talks about two types of Jhana.

193
00:12:39,480 --> 00:12:44,120
 Lakanupani Jhana, Arahmanupani Jhana, and Lakanupani Jhana,

194
00:12:44,120 --> 00:12:44,760
 these are the words.

195
00:12:44,760 --> 00:12:48,760
 And again and again it refers to these two types of Jhana.

196
00:12:48,760 --> 00:12:51,240
 So one is Arahmana means an object. So

197
00:12:51,800 --> 00:12:55,480
 this is samatha meditation where you're meditating and you

198
00:12:55,480 --> 00:12:58,440
're fixed and focused on a single object,

199
00:12:58,440 --> 00:13:01,610
 a concept. And because you're focused on that one object

200
00:13:01,610 --> 00:13:03,640
 you'll never see impermanent suffering

201
00:13:03,640 --> 00:13:06,560
 and non-self. As a result you'll never see, just by

202
00:13:06,560 --> 00:13:09,320
 focusing on that object, you'll never see nirvana.

203
00:13:09,320 --> 00:13:13,610
 The other one, Lakanupani Jhana, is where you're focused

204
00:13:13,610 --> 00:13:15,720
 and fixed on the three characteristics.

205
00:13:18,360 --> 00:13:23,320
 It's a focus on reality and the three characteristics,

206
00:13:23,320 --> 00:13:25,960
 seeing them, means it's a state of complete

207
00:13:25,960 --> 00:13:28,830
 tranquility as well. Because seeing the three

208
00:13:28,830 --> 00:13:31,640
 characteristics there is no reaction. You see

209
00:13:31,640 --> 00:13:34,210
 that the things that we would normally cling to are not

210
00:13:34,210 --> 00:13:36,120
 worth clinging to, they're not worth getting

211
00:13:36,120 --> 00:13:40,610
 upset about. So it's also right concentration and it should

212
00:13:40,610 --> 00:13:43,320
 also be considered Jhana in a sense. I

213
00:13:43,320 --> 00:13:47,160
 mean if you ever read what the various teachers say on the

214
00:13:47,160 --> 00:13:50,120
 word Jhana, nobody agrees anyway. So

215
00:13:50,120 --> 00:13:53,500
 I think this is one case where we can be, where we see

216
00:13:53,500 --> 00:13:56,280
 people getting often too caught up in their

217
00:13:56,280 --> 00:13:59,900
 own definition of a word. Sometimes it's true, a word has

218
00:13:59,900 --> 00:14:02,120
 to be defined in a specific way, but

219
00:14:02,120 --> 00:14:05,460
 it appears the word Jhana should not be. There are clearly

220
00:14:05,460 --> 00:14:07,880
 Jhanas that are unwholesome and Jhanas

221
00:14:07,880 --> 00:14:11,010
 that are wholesome, that's clear. But in regards to those

222
00:14:11,010 --> 00:14:13,240
 that are wholesome, there appears to be

223
00:14:13,240 --> 00:14:19,730
 a definite amount of leeway and those people who argue that

224
00:14:19,730 --> 00:14:22,440
 your way is wrong and so on and so on

225
00:14:22,440 --> 00:14:25,760
 are in this case I would say being overly dogmatic,

226
00:14:25,760 --> 00:14:27,560
 probably on both sides.

227
00:14:27,560 --> 00:14:31,430
 So I don't know, hopefully that's cleared up this whole

228
00:14:31,430 --> 00:14:34,200
 idea of right concentration. For those of

229
00:14:34,200 --> 00:14:36,910
 you who aren't aware of this whole debate, the point here

230
00:14:36,910 --> 00:14:38,760
 is that your mind has to be fixed and

231
00:14:38,760 --> 00:14:42,800
 focused. Your mind has to be in a wholesome state. The real

232
00:14:42,800 --> 00:14:45,320
 characteristic that I haven't mentioned

233
00:14:45,320 --> 00:14:49,140
 of concentration is that it frees you of the five hindr

234
00:14:49,140 --> 00:14:52,120
ances. And so just another point as to,

235
00:14:52,120 --> 00:14:55,750
 some people say you have to enter into the Jhanas before

236
00:14:55,750 --> 00:14:57,960
 you begin to practice mindfulness and I

237
00:14:57,960 --> 00:15:00,900
 think that's quite absurd because you have to be mindful of

238
00:15:00,900 --> 00:15:01,880
 the hindrances.

239
00:15:03,240 --> 00:15:06,270
 The Buddha was quite clear that when anger arises rather

240
00:15:06,270 --> 00:15:08,280
 than saying, "Okay, get rid of the anger,"

241
00:15:08,280 --> 00:15:13,320
 you should be mindful of it. So clearly there's some leeway

242
00:15:13,320 --> 00:15:15,080
 here. Yes, you can enter into states

243
00:15:15,080 --> 00:15:18,700
 where there is no greed, no anger, no deli... no greed and

244
00:15:18,700 --> 00:15:23,880
 no anger anyway. But it's clearly not

245
00:15:23,880 --> 00:15:27,670
 the only way. If there's anger in the mind, the way of

246
00:15:27,670 --> 00:15:30,840
 mindfulness is to be mindful of the anger.

247
00:15:32,520 --> 00:15:36,460
 And as a result of that, your concentration improves and

248
00:15:36,460 --> 00:15:38,680
 eventually you enter into a Jhana,

249
00:15:38,680 --> 00:15:41,780
 which means you enter into a state where there's no more

250
00:15:41,780 --> 00:15:43,720
 anger, where you're experiencing... you're

251
00:15:43,720 --> 00:15:46,880
 still experiencing things, seeing, hearing, but you have

252
00:15:46,880 --> 00:15:50,680
 equanimity about them. Mahasya Sayyara goes

253
00:15:50,680 --> 00:15:54,160
 into detail about... it's kind of a bit speculative about

254
00:15:54,160 --> 00:15:56,360
 the idea of there being vipassana jhana.

255
00:15:58,600 --> 00:16:02,110
 And I think all he's saying is that, "Hey, let's not be

256
00:16:02,110 --> 00:16:04,200
 quite so dogmatic when there's no reason

257
00:16:04,200 --> 00:16:07,220
 to and there's certainly no backing for it or basis for it

258
00:16:07,220 --> 00:16:08,920
 in the Buddha's teaching."

259
00:16:08,920 --> 00:16:12,620
 Jhana is something that just means the wholesome

260
00:16:12,620 --> 00:16:14,680
 concentration of the mind.

261
00:16:14,680 --> 00:16:17,700
 And at one point the Buddha says this. He says any

262
00:16:17,700 --> 00:16:20,840
 concentration that is accompanied by the other

263
00:16:20,840 --> 00:16:23,800
 seven path factors is right concentration, which is of

264
00:16:23,800 --> 00:16:27,080
 course. That's the key. You know, you're

265
00:16:27,080 --> 00:16:31,270
 focused, but you also have right mindfulness, right view,

266
00:16:31,270 --> 00:16:34,920
 right thought and so on.

267
00:16:34,920 --> 00:16:41,150
 And there's lots of ways to accomplish this, but ultimately

268
00:16:41,150 --> 00:16:42,920
 they must involve

269
00:16:42,920 --> 00:16:48,120
 mindfulness, means awareness of reality, because they must

270
00:16:48,120 --> 00:16:49,720
 involve the three characteristics and

271
00:16:50,440 --> 00:16:54,510
 must involve a clear understanding of suffering. Parinya,

272
00:16:54,510 --> 00:16:56,920
 right? When we talked about the first

273
00:16:56,920 --> 00:17:00,090
 noble truth, the path is not to avoid suffering and enter

274
00:17:00,090 --> 00:17:02,920
 into blissful states. It's about letting

275
00:17:02,920 --> 00:17:06,140
 go of suffering. It's about understanding suffering. And

276
00:17:06,140 --> 00:17:07,720
 when you see that it's suffering,

277
00:17:07,720 --> 00:17:15,420
 you let go of it. You stop clinging to it. So, hopefully I

278
00:17:15,420 --> 00:17:18,840
 haven't confused you all even more,

279
00:17:18,840 --> 00:17:21,640
 but concentration is something that comes through the

280
00:17:21,640 --> 00:17:23,720
 practice. It's something that

281
00:17:23,720 --> 00:17:28,330
 involves seeing things clearly. Another way of translating

282
00:17:28,330 --> 00:17:30,840
 it is right focus. When your mind is

283
00:17:30,840 --> 00:17:34,050
 in focus, it means you can see things clearly. And that's

284
00:17:34,050 --> 00:17:36,440
 important because concentration in

285
00:17:36,440 --> 00:17:40,160
 the threefold training is what leads to wisdom. We have sil

286
00:17:40,160 --> 00:17:42,040
a, morality or ethics leads to

287
00:17:42,040 --> 00:17:46,310
 concentration. Concentration, samadhi leads to wisdom. So

288
00:17:46,310 --> 00:17:49,480
 it involves focus. It means you focus

289
00:17:49,480 --> 00:17:53,950
 your attention, not too hard, just right so that you can

290
00:17:53,950 --> 00:17:56,600
 see things, just perfect focus,

291
00:17:56,600 --> 00:18:00,250
 so you can see things as they are. That's right,

292
00:18:00,250 --> 00:18:01,800
 concentration.

293
00:18:01,800 --> 00:18:08,280
 So thank you all for tuning in. That's our evening dhamma.

294
00:18:08,280 --> 00:18:14,280
 Now we'll take questions.

295
00:18:14,280 --> 00:18:25,160
 Two questions tonight. When doing walking meditation, my

296
00:18:25,160 --> 00:18:26,360
 back foot seems to always

297
00:18:26,360 --> 00:18:29,160
 start to come up at the same time I put my front toes down.

298
00:18:29,160 --> 00:18:32,840
 Should there be awareness of both actions at the same time?

299
00:18:35,400 --> 00:18:38,000
 Okay, well that shouldn't happen. Remember, when you're

300
00:18:38,000 --> 00:18:40,040
 doing walking meditation, you're not trying

301
00:18:40,040 --> 00:18:44,350
 to walk. You're trying to move one foot. And that movement

302
00:18:44,350 --> 00:18:46,920
 should be all that's in your mind.

303
00:18:46,920 --> 00:18:51,790
 So the technique is to only move one foot at a time. If you

304
00:18:51,790 --> 00:18:53,000
're moving both feet at once,

305
00:18:53,000 --> 00:18:58,960
 well, you're doing something. It's suboptimal, right?

306
00:18:58,960 --> 00:19:00,040
 Because if two things are moving at

307
00:19:00,040 --> 00:19:03,630
 once, very hard to focus only on one, as you can see. So do

308
00:19:03,630 --> 00:19:05,320
 take a little shorter step,

309
00:19:05,320 --> 00:19:07,830
 or just modify the way you think about it. It's not about

310
00:19:07,830 --> 00:19:10,440
 walking. I'm moving this foot,

311
00:19:10,440 --> 00:19:14,040
 right? That one's done. Then start moving the other foot.

312
00:19:14,040 --> 00:19:15,080
 You're not trying to get anywhere.

313
00:19:15,080 --> 00:19:18,560
 You're being aware of a movement. I've had kids do this

314
00:19:18,560 --> 00:19:21,240
 standing still. When I have a room full

315
00:19:21,240 --> 00:19:23,440
 of kids, they can't walk, and you wouldn't want them to

316
00:19:23,440 --> 00:19:25,320
 anyway. Just tell them to lift their feet

317
00:19:25,880 --> 00:19:30,900
 and put their foot down. Lifting, placing, lifting, placing

318
00:19:30,900 --> 00:19:32,600
. Because that's, we're just trying to

319
00:19:32,600 --> 00:19:37,920
 learn. We're trying to observe reality to see how our mind

320
00:19:37,920 --> 00:19:40,600
 reacts, and to learn about the interaction

321
00:19:40,600 --> 00:19:46,630
 between body and mind. So focus on one experience at a time

322
00:19:46,630 --> 00:19:50,840
. You can't possibly be aware of two

323
00:19:50,840 --> 00:19:53,830
 things at once anyway. It's not technically. If you think

324
00:19:53,830 --> 00:19:55,640
 about it, it would be very strange

325
00:19:55,640 --> 00:20:01,320
 to think of that as possible. An anagami experienced dom

326
00:20:01,320 --> 00:20:03,560
anasa is a version of prerequisite for

327
00:20:03,560 --> 00:20:13,170
 unhappiness. I would say no. An anagami cannot experience

328
00:20:13,170 --> 00:20:16,680
 domanasa because

329
00:20:16,680 --> 00:20:21,290
 domanasa has to do with patigah. Patigah is something that

330
00:20:21,290 --> 00:20:23,800
 an anagami has done away with.

331
00:20:23,800 --> 00:20:32,280
 I think, again, very technical questions. Let me know when

332
00:20:32,280 --> 00:20:33,960
 you become an anagami, and then we'll talk.

333
00:20:33,960 --> 00:20:39,720
 How is enlightenment permanent?

334
00:20:39,720 --> 00:20:48,380
 It's kind of like a dam crack. A crack in a dam. You can't

335
00:20:48,380 --> 00:20:51,160
 possibly get that water back in.

336
00:20:51,800 --> 00:20:54,860
 That's not a great analogy, but that's how I thought of

337
00:20:54,860 --> 00:20:55,400
 this. You know,

338
00:20:55,400 --> 00:20:58,320
 think of the one thing that is irreversible. When a dam

339
00:20:58,320 --> 00:21:01,640
 cracks, that's it. You can't fix it.

340
00:21:01,640 --> 00:21:04,810
 When the dam breaks, there's no getting that water back in

341
00:21:04,810 --> 00:21:07,080
 there. It's done.

342
00:21:07,080 --> 00:21:14,020
 Maybe it is a fairly good analogy. It's like pulling the

343
00:21:14,020 --> 00:21:15,720
 plug on a bathtub.

344
00:21:17,240 --> 00:21:20,190
 It's only a matter of time. It's not the greatest because

345
00:21:20,190 --> 00:21:21,880
 you can replug the bathtub, but

346
00:21:21,880 --> 00:21:24,890
 it's really like a crack. When you become a sotapan and you

347
00:21:24,890 --> 00:21:27,000
've seen nirban, it puts a crack in

348
00:21:27,000 --> 00:21:31,820
 in samsara. It starts making cracks in the universe, and

349
00:21:31,820 --> 00:21:33,480
 you see it again and again,

350
00:21:33,480 --> 00:21:36,240
 and the cracks get bigger and bigger. You can't fix those

351
00:21:36,240 --> 00:21:36,840
 cracks.

352
00:21:36,840 --> 00:21:43,310
 You know, I mean, how is it? Well, it's a part of reality.

353
00:21:43,310 --> 00:21:45,000
 It's a claim we make.

354
00:21:46,440 --> 00:21:49,800
 So maybe you disagree or are skeptical, which is fine.

355
00:21:49,800 --> 00:21:53,170
 Practice for yourself and see. If it turns out to be not

356
00:21:53,170 --> 00:21:55,880
 permanent, then you know.

357
00:21:55,880 --> 00:22:02,040
 Is working on improving life for all beings by using

358
00:22:02,040 --> 00:22:05,000
 science, compassion, and education,

359
00:22:05,880 --> 00:22:12,760
 science, compassion, and education wholesome? Yes, very

360
00:22:12,760 --> 00:22:17,960
 wholesome. Technically, it's not anything

361
00:22:17,960 --> 00:22:21,710
 to do with wholesomeness. Wholesomeness is your state of

362
00:22:21,710 --> 00:22:24,120
 mind, and that changes every moment.

363
00:22:24,120 --> 00:22:27,000
 Suppose you say, "I'm working to improve people's life

364
00:22:27,000 --> 00:22:30,040
 using science, compassion, and education,"

365
00:22:30,040 --> 00:22:33,500
 but you still get angry, and you get frustrated, and you

366
00:22:33,500 --> 00:22:35,800
 get burnt out, and you feel ego about

367
00:22:35,800 --> 00:22:38,590
 your good work, and so on. All of those moments when those

368
00:22:38,590 --> 00:22:39,880
 arise are unwholesome,

369
00:22:39,880 --> 00:22:43,610
 which is why the greatest wholesomeness is mindfulness. The

370
00:22:43,610 --> 00:22:44,680
 greatest wholesomeness is

371
00:22:44,680 --> 00:22:48,910
 meditation because you're clear. Every moment is wholesome,

372
00:22:48,910 --> 00:22:51,080
 and you're developing habits of

373
00:22:51,080 --> 00:22:55,870
 wholesomeness. So, no, work can never be wholesome. The

374
00:22:55,870 --> 00:22:58,680
 question is, what is your intention now?

375
00:22:59,480 --> 00:23:03,600
 When your work is to better improve the life of beings, it

376
00:23:03,600 --> 00:23:06,040
's, of course, far more likely

377
00:23:06,040 --> 00:23:09,850
 that wholesome states are going to arise. Anytime you help

378
00:23:09,850 --> 00:23:11,880
 someone, it can be very strong and

379
00:23:11,880 --> 00:23:15,470
 powerful wholesomeness. So, sure, of course. But be clear,

380
00:23:15,470 --> 00:23:18,440
 it's your state of mind. Someone says,

381
00:23:18,440 --> 00:23:20,520
 "Yes, I'm working to improve the lives of beings by

382
00:23:20,520 --> 00:23:27,640
 experimenting, using testing drugs on lab rats."

383
00:23:28,440 --> 00:23:32,520
 Not very wholesome, right? Because those poor lab rats are

384
00:23:32,520 --> 00:23:35,160
... I mean, it's probably a reason,

385
00:23:35,160 --> 00:23:38,360
 big reason why there's so much disease and sickness is

386
00:23:38,360 --> 00:23:40,200
 because of how we treat each other,

387
00:23:40,200 --> 00:23:45,500
 living beings. We get sick all the time because we're mean

388
00:23:45,500 --> 00:23:46,440
 and nasty people,

389
00:23:46,440 --> 00:23:49,880
 I mean, from lifetime to lifetime, of course.

390
00:23:52,840 --> 00:23:58,140
 So, yes, it's about moments rather than actions or

391
00:23:58,140 --> 00:24:01,560
 lifestyles or work or that kind of thing.

392
00:24:01,560 --> 00:24:07,780
 Okay, so that's the demo for tonight. Thank you all for

393
00:24:07,780 --> 00:24:09,240
 tuning in.

394
00:24:09,240 --> 00:24:36,040
 [

