1
00:00:00,000 --> 00:00:04,630
 Accepting that all views distract from the road to Nibbana,

2
00:00:04,630 --> 00:00:07,700
 how tolerant should Buddhists be to other views and

3
00:00:07,700 --> 00:00:11,000
 religions which may just be different perspectives?

4
00:00:11,000 --> 00:00:14,100
 I heard Buddhism is meant to be tolerant to other views,

5
00:00:14,100 --> 00:00:18,000
 but how tolerant is tolerant? Good question.

6
00:00:18,000 --> 00:00:22,180
 I don't know, Buddhism tolerant to other views, it depends

7
00:00:22,180 --> 00:00:24,000
 what you mean by tolerant.

8
00:00:24,000 --> 00:00:26,960
 And that's really what you're asking, no? What do we mean

9
00:00:26,960 --> 00:00:28,000
 by tolerant?

10
00:00:28,000 --> 00:00:31,130
 How should we be tolerant and what ways should we be

11
00:00:31,130 --> 00:00:32,000
 tolerant?

12
00:00:32,000 --> 00:00:36,220
 We should approach views and religions in the same way that

13
00:00:36,220 --> 00:00:38,000
 we approach everything.

14
00:00:38,000 --> 00:00:42,860
 No? Really everything. Everything should be approached in

15
00:00:42,860 --> 00:00:46,000
 the same way.

16
00:00:46,000 --> 00:00:51,520
 With mindfulness, with clarity, with knowledge of the

17
00:00:51,520 --> 00:00:56,630
 ultimate reality involved in the experience, involved in

18
00:00:56,630 --> 00:00:58,000
 the situation.

19
00:00:58,000 --> 00:01:02,000
 So religions don't exist, views...

20
00:01:02,000 --> 00:01:04,000
 May I add openness?

21
00:01:04,000 --> 00:01:06,000
 Openness.

22
00:01:06,000 --> 00:01:10,000
 Open with an open accepting mind?

23
00:01:10,000 --> 00:01:16,000
 Yeah. A level, balanced mind maybe.

24
00:01:16,000 --> 00:01:18,880
 Because that's the question we're dealing with here, is

25
00:01:18,880 --> 00:01:23,100
 whether you should be fully open minded to give the benefit

26
00:01:23,100 --> 00:01:24,000
 of the doubt.

27
00:01:24,000 --> 00:01:27,230
 We were talking about this earlier, there is a sense of

28
00:01:27,230 --> 00:01:31,000
 openness, even in bad things.

29
00:01:31,000 --> 00:01:34,330
 Because if it's bad, it's objectively bad. So yeah, open as

30
00:01:34,330 --> 00:01:35,000
 well.

31
00:01:35,000 --> 00:01:39,000
 You don't have to have prejudice even against bad things.

32
00:01:39,000 --> 00:01:42,520
 So if it's a wrong view or so on, you shouldn't have the

33
00:01:42,520 --> 00:01:44,000
 idea this is wrong.

34
00:01:44,000 --> 00:01:48,310
 You shouldn't have an aversion to it. There should be an

35
00:01:48,310 --> 00:01:50,000
 openness.

36
00:01:50,000 --> 00:01:57,000
 Yes, so I agree. Just had to think about it.

37
00:01:57,000 --> 00:02:00,000
 Because if it's wrong, it's going to be objectively wrong.

38
00:02:00,000 --> 00:02:05,270
 And by giving it a fair hearing, it's going to fail. It's

39
00:02:05,270 --> 00:02:07,000
 going to fail the test of usefulness.

40
00:02:07,000 --> 00:02:10,990
 And that's really the only way, the only litmus test to

41
00:02:10,990 --> 00:02:14,910
 tell you whether something is good or bad is objective

42
00:02:14,910 --> 00:02:16,000
 observation.

43
00:02:16,000 --> 00:02:21,120
 And non-judgmental, I guess, is really what is meant by

44
00:02:21,120 --> 00:02:22,000
 open.

45
00:02:22,000 --> 00:02:27,710
 To give it the fair trial and let it say its peace without

46
00:02:27,710 --> 00:02:31,420
 any partiality. Not being favorable towards it, not being

47
00:02:31,420 --> 00:02:32,000
 unfavorable.

48
00:02:32,000 --> 00:02:46,880
 So why I talk about this is because we don't aim to have,

49
00:02:46,880 --> 00:02:48,000
 because our intellectual,

50
00:02:48,000 --> 00:02:52,000
 I don't want you to think I want you to be intellectual,

51
00:02:52,000 --> 00:02:54,820
 but the activity that should go on in the mind is that this

52
00:02:54,820 --> 00:02:56,000
 is wrong view.

53
00:02:56,000 --> 00:02:59,870
 We come to see this through the practice. When you practice

54
00:02:59,870 --> 00:03:04,000
 vipassana meditation, you come to see how, as I said,

55
00:03:04,000 --> 00:03:06,560
 how body and mind works. You come to see what our body and

56
00:03:06,560 --> 00:03:10,010
 mind, what is reality, its body and mind, how they work

57
00:03:10,010 --> 00:03:11,000
 together.

58
00:03:11,000 --> 00:03:14,960
 They work together in terms of cause and effect. And so

59
00:03:14,960 --> 00:03:18,000
 doing bad deeds does lead to bad result. Doing good deeds

60
00:03:18,000 --> 00:03:18,000
 does lead to good result.

61
00:03:18,000 --> 00:03:22,190
 And things like God don't exist, the Creator God doesn't

62
00:03:22,190 --> 00:03:26,550
 exist. There are things that cannot exist in the system of

63
00:03:26,550 --> 00:03:27,000
 reality.

64
00:03:27,000 --> 00:03:31,400
 They're impossibilities in the system of reality. So if

65
00:03:31,400 --> 00:03:36,050
 someone tells you God created the universe in seven days,

66
00:03:36,050 --> 00:03:37,000
 has a plan for you,

67
00:03:37,000 --> 00:03:41,730
 and at some point in the future he's going to give you

68
00:03:41,730 --> 00:03:48,000
 eternal bliss in heaven if you believe in him.

69
00:03:48,000 --> 00:03:58,190
 This is impossible for some, for, this is impossible to,

70
00:03:58,190 --> 00:04:01,000
 this is impossible, no, it can't occur.

71
00:04:01,000 --> 00:04:05,340
 And it's completely 100% unbelievable to someone who has

72
00:04:05,340 --> 00:04:09,430
 understood reality. It can't be the case that this could be

73
00:04:09,430 --> 00:04:10,000
 true.

74
00:04:10,000 --> 00:04:14,190
 So one knows in one's mind, I would say intellectually, but

75
00:04:14,190 --> 00:04:17,230
 what I mean by that is that's the thought that goes through

76
00:04:17,230 --> 00:04:18,000
 one's mind.

77
00:04:18,000 --> 00:04:20,960
 That's all I mean by intellectually. One doesn't come to

78
00:04:20,960 --> 00:04:24,550
 that logically. But the mental activity that goes on in the

79
00:04:24,550 --> 00:04:26,000
 mind will be this is wrong view.

80
00:04:26,000 --> 00:04:30,230
 Whenever someone hears, whenever say an enlightened person

81
00:04:30,230 --> 00:04:34,270
 or even just a vipassana meditator hears these things, this

82
00:04:34,270 --> 00:04:36,000
 is correct for this to come in through the mind.

83
00:04:36,000 --> 00:04:40,580
 But that doesn't mean that one, that shouldn't have a great

84
00:04:40,580 --> 00:04:44,890
 deal of impact on how one interacts with people of other

85
00:04:44,890 --> 00:04:46,000
 religions.

86
00:04:46,000 --> 00:04:49,920
 So this is where the tolerance comes in. In the mind, in

87
00:04:49,920 --> 00:04:54,350
 one's intellectual mind, one has no tolerance for it. One

88
00:04:54,350 --> 00:04:56,000
 can't have tolerance for it.

89
00:04:56,000 --> 00:04:59,340
 One can't entertain this view and say, "Well, maybe they're

90
00:04:59,340 --> 00:05:03,430
 right." One can say that in one's mind, but one can never

91
00:05:03,430 --> 00:05:06,000
 have the feeling that this might be right at all.

92
00:05:06,000 --> 00:05:08,230
 So you can't say, "Okay, well let's look and see whether

93
00:05:08,230 --> 00:05:12,070
 this is right." But it's impossible that one could come to

94
00:05:12,070 --> 00:05:16,160
 any conclusion that this is right or even entertain it if

95
00:05:16,160 --> 00:05:18,000
 they have seen reality.

96
00:05:18,000 --> 00:05:21,090
 I mean that's really just the case. If I have something in

97
00:05:21,090 --> 00:05:24,160
 my hand, as long as you've never seen what's in my hand,

98
00:05:24,160 --> 00:05:27,340
 then you can believe me when I tell you there's a diamond

99
00:05:27,340 --> 00:05:31,770
 in my hand or there's a frog in my hand or whatever I tell

100
00:05:31,770 --> 00:05:34,000
 you, you can still entertain that.

101
00:05:34,000 --> 00:05:37,200
 But once you've seen what's in my hand, then you can't

102
00:05:37,200 --> 00:05:40,000
 believe me when I say there's this or that.

103
00:05:40,000 --> 00:05:43,120
 So if I open it and show you there's nothing, then if I say

104
00:05:43,120 --> 00:05:46,000
 there's a frog in my hand, you can't believe me.

105
00:05:46,000 --> 00:05:50,260
 Once you've seen it for yourself, this is the way with

106
00:05:50,260 --> 00:05:55,040
 anything and reality, of course, is the most absolute in

107
00:05:55,040 --> 00:05:56,000
 that way.

108
00:05:56,000 --> 00:06:00,260
 So you're not tolerant at all internally. Externally is

109
00:06:00,260 --> 00:06:04,110
 totally different. And I think that's how this should be

110
00:06:04,110 --> 00:06:09,050
 understood, that we don't entertain other religions in

111
00:06:09,050 --> 00:06:11,700
 saying, "Well, you know, they're all teaching people to be

112
00:06:11,700 --> 00:06:16,000
 good, so they're all equal or something."

113
00:06:16,000 --> 00:06:21,010
 But we can and we should pick out what is good in all

114
00:06:21,010 --> 00:06:25,000
 religions and say that that is good.

115
00:06:25,000 --> 00:06:27,690
 As the Buddha said, he said, "When a person says something

116
00:06:27,690 --> 00:06:30,690
 that's in line with the truth, I agree with them. When a

117
00:06:30,690 --> 00:06:33,130
 person that says something that's out of line with the

118
00:06:33,130 --> 00:06:35,000
 truth, I disagree with them."

119
00:06:35,000 --> 00:06:37,680
 So how you react to the person, how you react to other

120
00:06:37,680 --> 00:06:40,630
 people and other religions and how you talk about them and

121
00:06:40,630 --> 00:06:43,590
 speak about them is a totally different matter because if

122
00:06:43,590 --> 00:06:46,390
 you sit around saying, "Oh, these other religions are

123
00:06:46,390 --> 00:06:49,420
 horrible," then, well, that's not a terribly Buddhist way

124
00:06:49,420 --> 00:06:50,000
 to behave.

125
00:06:50,000 --> 00:06:54,260
 If you ridicule them or if you say mean and nasty things to

126
00:06:54,260 --> 00:06:58,480
 them or about them, then what it shows is that you're

127
00:06:58,480 --> 00:07:03,470
 lacking in Buddhist practice. A Buddhist, the person who is

128
00:07:03,470 --> 00:07:08,300
 Buddhist, will be, I think maybe the word isn't tolerant,

129
00:07:08,300 --> 00:07:11,000
 will be, you know, mindful is the best word.

130
00:07:11,000 --> 00:07:16,180
 You could say respectful or you could say cordial or so on,

131
00:07:16,180 --> 00:07:19,000
 but the real word is mindful.

132
00:07:19,000 --> 00:07:21,450
 And so because they act in a mindful way, it could be

133
00:07:21,450 --> 00:07:24,680
 different for some people. Some people, mindful might just

134
00:07:24,680 --> 00:07:28,280
 be to not say anything. For some people, mindful might be

135
00:07:28,280 --> 00:07:32,260
 to interact with the person and speak cordially to them and

136
00:07:32,260 --> 00:07:36,000
 agree with them when they talk about good things.

137
00:07:36,000 --> 00:07:38,800
 So when a Christian says, you know, "You reap what you sow

138
00:07:38,800 --> 00:07:41,000
," we say, "Yeah, yeah, that's so true."

139
00:07:41,000 --> 00:07:43,210
 Or when they say, "Blessed are the meek," you say, "Yes,

140
00:07:43,210 --> 00:07:44,000
 yes, that's true."

141
00:07:44,000 --> 00:07:47,140
 And you can even say, "You know, Jesus really had some good

142
00:07:47,140 --> 00:07:49,660
 things to say," for example. You can say that about all

143
00:07:49,660 --> 00:07:53,250
 religions. That's, I think, a good thing to do on a

144
00:07:53,250 --> 00:07:55,000
 practical level.

145
00:07:55,000 --> 00:07:58,460
 If you're just looking for tips on how to deal with people

146
00:07:58,460 --> 00:08:01,830
 from other religions, I think that's the best is to find

147
00:08:01,830 --> 00:08:05,020
 the good things in them and stick to that and try to do

148
00:08:05,020 --> 00:08:09,000
 your best to be silent and avoid the controversial issues.

149
00:08:09,000 --> 00:08:13,090
 But that has nothing to do with how tolerant you are, I

150
00:08:13,090 --> 00:08:14,000
 think.

151
00:08:14,000 --> 00:08:16,880
 Except, you know, see, it's a difficult word tolerant

152
00:08:16,880 --> 00:08:19,700
 because you certainly tolerate them. And if they want to

153
00:08:19,700 --> 00:08:21,000
 kill you, you tolerate that.

154
00:08:21,000 --> 00:08:24,360
 If they say, "Jihad, time," and they say, "Convert or die,"

155
00:08:24,360 --> 00:08:28,000
 you say, "Okay, go ahead, do your worst." That's tolerant.

156
00:08:28,000 --> 00:08:30,800
 So in a sense, you're completely tolerant. But you're not

157
00:08:30,800 --> 00:08:34,350
 tolerant in the sense that they say, "Believe in our

158
00:08:34,350 --> 00:08:38,320
 religion or we'll kill you," or, "You should believe in our

159
00:08:38,320 --> 00:08:39,000
 religion."

160
00:08:39,000 --> 00:08:43,450
 You say, "Okay, I'll go with that." Or, "Our religion is

161
00:08:43,450 --> 00:08:47,000
 equal. You will never agree with them on that."

162
00:08:47,000 --> 00:08:50,970
 But I guess that is still within tolerance, no? Tolerant in

163
00:08:50,970 --> 00:08:54,910
 the sense that you will never be angry about the other

164
00:08:54,910 --> 00:08:59,000
 religion. You'll never reject it as a negative thing.

165
00:08:59,000 --> 00:09:03,620
 But you will reject it 100% in the mind. And if they ask

166
00:09:03,620 --> 00:09:05,850
 you your honest opinion and you have to give it, you'll say

167
00:09:05,850 --> 00:09:07,000
, "That's wrong view."

168
00:09:07,000 --> 00:09:10,350
 You'll say, "That is not in line with reality." Or you

169
00:09:10,350 --> 00:09:13,000
 might try to find some better way to say it.

170
00:09:13,000 --> 00:09:16,540
 But it works on two levels. What you believe for yourself

171
00:09:16,540 --> 00:09:20,310
 and how you interact with other people, which I think are

172
00:09:20,310 --> 00:09:24,000
 two very, very different spheres of activity.

173
00:09:24,000 --> 00:09:29,000
 I have nothing to add to that.

