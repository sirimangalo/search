1
00:00:00,000 --> 00:00:05,560
 Welcome back to Ask a Monk. This question comes from Yong

2
00:00:05,560 --> 00:00:09,560
 Pei-Singh who asks, "Do we

3
00:00:09,560 --> 00:00:16,640
 need to give up all materialistic pleasure?" Giving up, on

4
00:00:16,640 --> 00:00:18,700
 giving up, giving

5
00:00:18,700 --> 00:00:22,000
 up pleasure. No, you don't have to give up any pleasure.

6
00:00:22,000 --> 00:00:24,080
 This is really the key.

7
00:00:24,080 --> 00:00:27,990
 You have to give up your attachment to the pleasure and

8
00:00:27,990 --> 00:00:30,880
 there's a difference. You

9
00:00:30,880 --> 00:00:34,240
 can experience pleasure without being attached to it. That

10
00:00:34,240 --> 00:00:36,440
's clear. The body has

11
00:00:36,440 --> 00:00:41,680
 many states, even the mind has many states, and these don't

12
00:00:41,680 --> 00:00:43,200
 all necessarily

13
00:00:43,200 --> 00:00:50,740
 stem from greed or coexist with attachment. But attachment

14
00:00:50,740 --> 00:00:51,680
 is a sort of

15
00:00:51,680 --> 00:00:55,010
 stress. It's something that brings the mind out of its

16
00:00:55,010 --> 00:00:56,040
 state of peace and

17
00:00:56,040 --> 00:00:59,920
 happiness. And if you engage in attachment, addiction,

18
00:00:59,920 --> 00:01:01,680
 wanting, desire, and so on,

19
00:01:01,680 --> 00:01:04,860
 you're only going to become more and more miserable when

20
00:01:04,860 --> 00:01:06,360
 you compartmentalize

21
00:01:06,360 --> 00:01:10,470
 reality into good and bad and therefore don't get always

22
00:01:10,470 --> 00:01:12,040
 what you want because

23
00:01:12,040 --> 00:01:16,240
 you have wants. If you're able to accept reality for what

24
00:01:16,240 --> 00:01:17,640
 it is then it's not a

25
00:01:17,640 --> 00:01:22,600
 question of whether it's pleasure or pain at all. You're

26
00:01:22,600 --> 00:01:24,240
 able to live in

27
00:01:24,240 --> 00:01:27,910
 harmony with the world around you and in harmony with

28
00:01:27,910 --> 00:01:30,080
 reality as it is. Accepting

29
00:01:30,080 --> 00:01:35,360
 change, accepting the good and the bad and not giving

30
00:01:35,360 --> 00:01:36,360
 labels to things as

31
00:01:36,360 --> 00:01:41,030
 good or bad. But I think more than that it's important to

32
00:01:41,030 --> 00:01:42,840
 understand that you

33
00:01:42,840 --> 00:01:47,090
 don't have to give up anything at all. Buddhism is not a

34
00:01:47,090 --> 00:01:49,640
 dogma. There's no theory

35
00:01:49,640 --> 00:01:54,240
 involved in terms of you have to let go of this or let go

36
00:01:54,240 --> 00:01:57,120
 of that. The point is

37
00:01:57,120 --> 00:02:03,150
 that you're suffering and that you may not realize that.

38
00:02:03,150 --> 00:02:04,720
 For many people they

39
00:02:04,720 --> 00:02:07,430
 don't realize that. They don't realize that they're unable

40
00:02:07,430 --> 00:02:08,600
 to see their

41
00:02:08,600 --> 00:02:13,960
 state of existence as having suffering in it. But you are

42
00:02:13,960 --> 00:02:14,760
 and we all are.

43
00:02:14,760 --> 00:02:22,840
 Once we realize that, that there is suffering, then we want

44
00:02:22,840 --> 00:02:23,680
 to find a way out

45
00:02:23,680 --> 00:02:26,790
 of it. So we start looking and once you start looking you

46
00:02:26,790 --> 00:02:28,440
 realize that the cause

47
00:02:28,440 --> 00:02:33,400
 of suffering is really your attachments. As I said, the

48
00:02:33,400 --> 00:02:35,120
 attachments to this and

49
00:02:35,120 --> 00:02:41,400
 to that, to pleasure, the attachments to being in a certain

50
00:02:41,400 --> 00:02:45,030
 existence, to being something. Our attachments to not being

51
00:02:45,030 --> 00:02:47,840
 something. These

52
00:02:47,840 --> 00:02:50,660
 are a cause of suffering because we're not able to accept

53
00:02:50,660 --> 00:02:51,640
 reality for what it

54
00:02:51,640 --> 00:02:56,410
 is. We're not able to accept the experience. And when you

55
00:02:56,410 --> 00:02:57,400
 realize this,

56
00:02:57,400 --> 00:03:02,040
 you let go by yourself. You realize the stress that you're

57
00:03:02,040 --> 00:03:03,440
 causing. When you

58
00:03:03,440 --> 00:03:07,400
 see something as causing you stress, if you see it clearly,

59
00:03:07,400 --> 00:03:08,360
 there's no

60
00:03:08,360 --> 00:03:13,390
 thought involved at all. It immediately changes who you are

61
00:03:13,390 --> 00:03:15,120
. Your way of

62
00:03:15,120 --> 00:03:19,880
 behaving changes so that you don't go that way anymore. You

63
00:03:19,880 --> 00:03:20,240
 would never

64
00:03:20,240 --> 00:03:24,950
 do something when you know that, when you know and you

65
00:03:24,950 --> 00:03:26,640
 verified for yourself that

66
00:03:26,640 --> 00:03:30,400
 that is causing you suffering. That's nature. That's the

67
00:03:30,400 --> 00:03:31,000
 nature of the

68
00:03:31,000 --> 00:03:34,740
 mind. We only do things because we think they're going to

69
00:03:34,740 --> 00:03:35,200
 cause us

70
00:03:35,200 --> 00:03:41,160
 pleasure and happiness and bring about some sort of benefit

71
00:03:41,160 --> 00:03:41,800
. When we see

72
00:03:41,800 --> 00:03:45,320
 that there is no benefit in something and see clearly means

73
00:03:45,320 --> 00:03:46,180
 in a meditative

74
00:03:46,180 --> 00:03:50,960
 sense, we won't engage in that. So you don't ever have to

75
00:03:50,960 --> 00:03:52,060
 worry about having to

76
00:03:52,060 --> 00:03:55,260
 give something up. Thinking, "Oh, I'm gonna have to give up

77
00:03:55,260 --> 00:03:56,500
 this or that." That's not

78
00:03:56,500 --> 00:04:02,430
 the point. The point is, look closer, look clearer. Give up

79
00:04:02,430 --> 00:04:03,560
 those things that are

80
00:04:03,560 --> 00:04:07,060
 stopping you from seeing clearly. Of course, give up

81
00:04:07,060 --> 00:04:08,560
 drinking, alcohol, and

82
00:04:08,560 --> 00:04:13,560
 drugs and so on because you can't see objectively with your

83
00:04:13,560 --> 00:04:14,320
 mind

84
00:04:14,320 --> 00:04:19,330
 intoxicated. For instance, give up entertainment to a

85
00:04:19,330 --> 00:04:20,240
 certain degree, to

86
00:04:20,240 --> 00:04:24,940
 the degree that it's getting in the way of your clear

87
00:04:24,940 --> 00:04:26,280
 investigation of

88
00:04:26,280 --> 00:04:29,580
 reality. But that's just a technical issue. If you don't

89
00:04:29,580 --> 00:04:30,640
 give it up, you won't

90
00:04:30,640 --> 00:04:35,880
 be able to undertake the scientific inquiry. But once you

91
00:04:35,880 --> 00:04:36,320
've gone that far,

92
00:04:36,320 --> 00:04:39,210
 you know, start to look and don't take my word for that.

93
00:04:39,210 --> 00:04:40,320
 You have to give up this

94
00:04:40,320 --> 00:04:44,130
 or that or everything. Just start looking and the better

95
00:04:44,130 --> 00:04:45,440
 you understand

96
00:04:45,440 --> 00:04:48,730
 reality, the more you'll give up until finally you give up

97
00:04:48,730 --> 00:04:49,680
 everything by

98
00:04:49,680 --> 00:04:54,960
 yourself. There's no forcing involved or pushing or pulling

99
00:04:54,960 --> 00:04:55,320
 at all.

100
00:04:55,320 --> 00:05:01,610
 It's wisdom. Once you see things as they are, you don't

101
00:05:01,610 --> 00:05:02,280
 want to cling

102
00:05:02,280 --> 00:05:06,120
 because it's not a belief, it's not a theory. There's no

103
00:05:06,120 --> 00:05:07,160
 happiness that comes

104
00:05:07,160 --> 00:05:12,520
 from clinging. So, all the best. Hope that helps.

