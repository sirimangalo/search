1
00:00:00,000 --> 00:00:06,980
 "How do I know I am being mindful enough and practicing

2
00:00:06,980 --> 00:00:08,000
 properly?"

3
00:00:08,000 --> 00:00:19,600
 It's a difficult question to answer because you really have

4
00:00:19,600 --> 00:00:21,240
 to understand where your question

5
00:00:21,240 --> 00:00:24,000
 is coming from.

6
00:00:24,000 --> 00:00:27,270
 I mean you, I mean when you ask this question the important

7
00:00:27,270 --> 00:00:28,780
 thing for the teacher to point

8
00:00:28,780 --> 00:00:35,800
 out is what's giving rise to this question.

9
00:00:35,800 --> 00:00:39,200
 And what's giving rise to it, maybe what's giving rise to

10
00:00:39,200 --> 00:00:40,920
 it, I would venture a guess

11
00:00:40,920 --> 00:00:45,800
 is some kind of doubt or anxiety about your practice, about

12
00:00:45,800 --> 00:00:47,960
 who you are and about your

13
00:00:47,960 --> 00:00:52,880
 status as a meditator and so on.

14
00:00:52,880 --> 00:00:57,430
 It could come from doubts or uncertainty or even can come

15
00:00:57,430 --> 00:00:59,640
 simply from curiosity.

16
00:00:59,640 --> 00:01:03,770
 But all of these are mind states and all of those are what

17
00:01:03,770 --> 00:01:06,280
 you should be focusing on rather

18
00:01:06,280 --> 00:01:08,680
 than asking such questions.

19
00:01:08,680 --> 00:01:11,520
 That's really avoiding the question, isn't it?

20
00:01:11,520 --> 00:01:13,930
 I don't think it's a very good question to ask and I'm not

21
00:01:13,930 --> 00:01:15,320
 criticizing you because it's

22
00:01:15,320 --> 00:01:22,950
 a question that I get a lot but the way to answer it is to

23
00:01:22,950 --> 00:01:28,120
 divert the focus back to where

24
00:01:28,120 --> 00:01:33,000
 it should be and that is on the states of mind when they

25
00:01:33,000 --> 00:01:35,000
 arise rather than worrying

26
00:01:35,000 --> 00:01:41,080
 about how your practice is doing or how mindful you are.

27
00:01:41,080 --> 00:01:47,390
 There's kind of a, there's a general confusion that occurs

28
00:01:47,390 --> 00:01:51,200
 in the meditator's mind because

29
00:01:51,200 --> 00:01:58,280
 they are indoctrinated culturally into the idea that

30
00:01:58,280 --> 00:02:03,000
 meditation is about concentration.

31
00:02:03,000 --> 00:02:06,910
 So they use the word mindfulness but their brain is still

32
00:02:06,910 --> 00:02:09,000
 thinking concentration and

33
00:02:09,000 --> 00:02:12,110
 so they ask questions and this is a very common question,

34
00:02:12,110 --> 00:02:13,760
 am I being mindful enough?

35
00:02:13,760 --> 00:02:23,120
 You can't possibly qualify, qualify?

36
00:02:23,120 --> 00:02:25,400
 There's no quality to mindfulness.

37
00:02:25,400 --> 00:02:29,490
 You either are mindfulness or you are not and you can only

38
00:02:29,490 --> 00:02:31,320
 be mindful one moment at

39
00:02:31,320 --> 00:02:34,620
 a time.

40
00:02:34,620 --> 00:02:37,560
 This is what is confirmed by our use of this word.

41
00:02:37,560 --> 00:02:41,540
 The word is the recognition but that recognition occurs,

42
00:02:41,540 --> 00:02:44,000
 the word mindfulness which really

43
00:02:44,000 --> 00:02:49,960
 means to recognize and to fully grasp the object as it is,

44
00:02:49,960 --> 00:02:52,520
 it occurs for one moment

45
00:02:52,520 --> 00:02:57,080
 and if you're not practicing the next moment then that goes

46
00:02:57,080 --> 00:02:57,800
 away.

47
00:02:57,800 --> 00:03:07,880
 If you're not quick and you're not catching the next one

48
00:03:07,880 --> 00:03:15,520
 then you miss the whole point.

49
00:03:15,520 --> 00:03:27,240
 So the answer here is to try and see it moment to moment.

50
00:03:27,240 --> 00:03:32,100
 Don't worry about your quality of practice.

51
00:03:32,100 --> 00:03:34,600
 Don't worry about your quality of mind.

52
00:03:34,600 --> 00:03:37,890
 People will always come to me, meditators will always come

53
00:03:37,890 --> 00:03:39,840
 to me and say I'm not concentrated

54
00:03:39,840 --> 00:03:49,140
 enough and I will remind them again and again and again

55
00:03:49,140 --> 00:03:51,760
 that it's not really what we're

56
00:03:51,760 --> 00:03:52,760
 looking for.

57
00:03:52,760 --> 00:03:57,010
 You know I didn't ask how concentrated are you, how's your

58
00:03:57,010 --> 00:03:58,840
 practice and then I was like

59
00:03:58,840 --> 00:04:00,200
 oh I'm not very concentrated.

60
00:04:00,200 --> 00:04:03,280
 Well I didn't ask that.

61
00:04:03,280 --> 00:04:07,680
 I mean basically the meaning is that's not how I would

62
00:04:07,680 --> 00:04:11,120
 judge your practice by how concentrated

63
00:04:11,120 --> 00:04:15,750
 you are but when you're not concentrated as the Buddha said

64
00:04:15,750 --> 00:04:17,960
 in the Satipatthana sutta

65
00:04:17,960 --> 00:04:22,590
 you should know when I'm unfocused he knows I'm unfocused,

66
00:04:22,590 --> 00:04:24,760
 when he's focused he knows

67
00:04:24,760 --> 00:04:32,720
 he's focused.

68
00:04:32,720 --> 00:04:38,260
 I think that will help you and I'm happy to not only

69
00:04:38,260 --> 00:04:42,520
 progress but to hopefully avoid what

70
00:04:42,520 --> 00:04:48,720
 may be and I'm assuming likely is doubts about yourself and

71
00:04:48,720 --> 00:04:51,400
 your practice and worrying about

72
00:04:51,400 --> 00:04:52,400
 the practice.

73
00:04:52,400 --> 00:04:54,320
 Okay, glad that helped.

