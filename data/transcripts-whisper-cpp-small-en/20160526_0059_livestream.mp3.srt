1
00:00:00,000 --> 00:00:05,000
 Good evening everyone.

2
00:00:05,000 --> 00:00:11,000
 Broadcasting Live, May 25th.

3
00:00:11,000 --> 00:00:25,000
 Today's quote is somewhat suspect.

4
00:00:28,000 --> 00:00:38,000
 Looks like he's loosely translated something problematic.

5
00:00:38,000 --> 00:00:42,000
 This is the word "love" here.

6
00:00:42,000 --> 00:00:48,570
 As far as I can see, the only instance of this quote doesn

7
00:00:48,570 --> 00:00:51,000
't use the word "love".

8
00:00:51,000 --> 00:00:56,940
 He uses the word "pati santara", which for some reason I

9
00:00:56,940 --> 00:01:01,000
 think we've already had as a quote.

10
00:01:01,000 --> 00:01:06,000
 I think at some point we had this list of six.

11
00:01:06,000 --> 00:01:10,000
 Maybe I'm wrong.

12
00:01:10,000 --> 00:01:16,000
 I'm pretty sure though, "garawa". We talked about "garawa".

13
00:01:16,000 --> 00:01:22,610
 This time it has the backstory of an angel coming to see

14
00:01:22,610 --> 00:01:24,000
 the Buddha,

15
00:01:24,000 --> 00:01:30,130
 telling him about these six things, and the Buddha aff

16
00:01:30,130 --> 00:01:32,000
irming that it's indeed true.

17
00:01:32,000 --> 00:01:40,000
 "garawata". Six kinds of respect.

18
00:01:40,000 --> 00:01:46,420
 "Garawata" comes from the root. The root would be "gar", I

19
00:01:46,420 --> 00:01:47,000
 guess.

20
00:01:47,000 --> 00:01:52,000
 "Guru". I don't know if "gar", I guess.

21
00:01:52,000 --> 00:01:55,000
 It's where the word "guru" comes from, "garu".

22
00:01:55,000 --> 00:02:04,000
 In Pali, "garu" means "heavy", or someone who is important.

23
00:02:04,000 --> 00:02:10,000
 A teacher, "garu" is a teacher, a guru.

24
00:02:10,000 --> 00:02:14,820
 But "garu" also means "heavy", so it means "taking

25
00:02:14,820 --> 00:02:16,000
 something seriously".

26
00:02:16,000 --> 00:02:20,000
 "Garawata" means "taking something seriously".

27
00:02:20,000 --> 00:02:24,030
 There are six things that, according to this angel and then

28
00:02:24,030 --> 00:02:27,000
 according to the Buddha,

29
00:02:27,000 --> 00:02:37,000
 are important for not falling away, for not declining.

30
00:02:37,000 --> 00:02:43,000
 We want to progress in this life. We want to succeed.

31
00:02:43,000 --> 00:02:46,760
 We're talking specifically about spirituality, about

32
00:02:46,760 --> 00:02:51,000
 Buddhism and the Buddha's teaching.

33
00:02:51,000 --> 00:02:54,890
 What is it that leads to success? What is it that leads to

34
00:02:54,890 --> 00:02:56,000
 progress?

35
00:02:56,000 --> 00:02:59,000
 There are these six things.

36
00:02:59,000 --> 00:03:02,000
 Number six, I don't think, is as translated.

37
00:03:02,000 --> 00:03:05,000
 Let's go through them. "Satu garawata".

38
00:03:05,000 --> 00:03:10,000
 "Satu" means "the teacher", so "the Buddha".

39
00:03:10,000 --> 00:03:18,000
 Having respect for and taking the Buddha seriously.

40
00:03:18,000 --> 00:03:21,680
 Traditionally, we'll take the Buddha so seriously that they

41
00:03:21,680 --> 00:03:24,000
 make images of the Buddha

42
00:03:24,000 --> 00:03:27,000
 and they bow down to them and they give offerings.

43
00:03:27,000 --> 00:03:29,000
 That's how seriously they take the Buddha.

44
00:03:29,000 --> 00:03:36,000
 Some might argue a little too seriously, but

45
00:03:36,000 --> 00:03:40,000
 the most important is appreciating the Buddha

46
00:03:40,000 --> 00:03:45,400
 and respecting his enlightenment, respecting his state, his

47
00:03:45,400 --> 00:03:50,000
 perfection, his greatness.

48
00:03:50,000 --> 00:03:55,420
 It's important because it keeps you in line with his

49
00:03:55,420 --> 00:03:57,000
 teachings.

50
00:03:57,000 --> 00:04:01,440
 It's easy for the mind to get lost, for the mind to lose

51
00:04:01,440 --> 00:04:02,000
 its way.

52
00:04:02,000 --> 00:04:06,300
 If you don't take the Buddha seriously, your mind is very

53
00:04:06,300 --> 00:04:07,000
 easily diverted

54
00:04:07,000 --> 00:04:11,000
 and caught up by other things.

55
00:04:11,000 --> 00:04:14,000
 Number two, of course, "dhamma garawata".

56
00:04:14,000 --> 00:04:16,000
 Number three, "sanga garawata".

57
00:04:16,000 --> 00:04:19,530
 The Buddha, the dhamma and the sangha, should take them

58
00:04:19,530 --> 00:04:20,000
 seriously.

59
00:04:20,000 --> 00:04:25,000
 The dhamma of the Buddha refers to his teachings.

60
00:04:25,000 --> 00:04:27,000
 Should take them seriously.

61
00:04:27,000 --> 00:04:32,500
 Should not look lightly upon the meditation practice and

62
00:04:32,500 --> 00:04:35,000
 the teachings.

63
00:04:35,000 --> 00:04:41,000
 We should appreciate them.

64
00:04:41,000 --> 00:04:46,230
 Some people would be so appreciative that they don't put

65
00:04:46,230 --> 00:04:51,000
 books about the Buddha's teaching on the floor.

66
00:04:51,000 --> 00:05:00,000
 They'll keep them up high and treat the books with respect.

67
00:05:00,000 --> 00:05:05,000
 Some might argue that that's not really necessary.

68
00:05:05,000 --> 00:05:08,000
 But it's all about the sense of respect.

69
00:05:08,000 --> 00:05:11,360
 So treating the Buddha and the dhamma in this way is useful

70
00:05:11,360 --> 00:05:14,000
 for cultivating wholesome mind states,

71
00:05:14,000 --> 00:05:20,470
 respectful mind states, mind states that are conducive to

72
00:05:20,470 --> 00:05:31,000
 practice, conducive to progress.

73
00:05:31,000 --> 00:05:33,000
 The most important is that we respect the teaching.

74
00:05:33,000 --> 00:05:36,000
 We appreciate how important it is.

75
00:05:36,000 --> 00:05:40,070
 Sometimes studying the Buddha's teaching is good because it

76
00:05:40,070 --> 00:05:42,000
's not just for the knowledge it gives you,

77
00:05:42,000 --> 00:05:45,920
 but because it gives you a deeper respect so that you don't

78
00:05:45,920 --> 00:05:48,000
 have to doubt what little you know.

79
00:05:48,000 --> 00:05:51,440
 If you know a lot more, it can help to solidify your

80
00:05:51,440 --> 00:05:55,000
 practice and make it clear the bigger picture.

81
00:05:55,000 --> 00:05:58,000
 Why are we doing this? Why is it important to let go?

82
00:05:58,000 --> 00:06:01,000
 Why is it wrong to cling?

83
00:06:01,000 --> 00:06:07,650
 Sometimes studying is useful, but we could also talk about

84
00:06:07,650 --> 00:06:10,000
 appreciation of meditation practice,

85
00:06:10,000 --> 00:06:12,000
 taking that seriously.

86
00:06:12,000 --> 00:06:17,000
 Here I think it just means the teaching is the doctrine.

87
00:06:17,000 --> 00:06:19,000
 Number three, the Sangha.

88
00:06:19,000 --> 00:06:22,080
 So appreciating teachers, appreciating enlightened beings,

89
00:06:22,080 --> 00:06:24,840
 even if they don't teach, just appreciating their

90
00:06:24,840 --> 00:06:26,000
 enlightenment.

91
00:06:26,000 --> 00:06:30,000
 Appreciating looking up to someone who you'd like to

92
00:06:30,000 --> 00:06:31,000
 emulate.

93
00:06:31,000 --> 00:06:35,310
 When you take them seriously, it becomes easy to emulate

94
00:06:35,310 --> 00:06:39,000
 them, to follow their lead,

95
00:06:39,000 --> 00:06:43,920
 to follow the way that they've gone, follow in their

96
00:06:43,920 --> 00:06:48,000
 footsteps as well.

97
00:06:48,000 --> 00:06:52,640
 Number four, Sikha Garawata, the training, taking the

98
00:06:52,640 --> 00:06:54,000
 training seriously.

99
00:06:54,000 --> 00:06:58,000
 Training in morality, taking the precepts seriously,

100
00:06:58,000 --> 00:07:01,000
 training in concentration,

101
00:07:01,000 --> 00:07:07,000
 so taking meditation seriously, and training in wisdom,

102
00:07:07,000 --> 00:07:11,620
 so taking wisdom seriously in the practice of insight

103
00:07:11,620 --> 00:07:13,000
 meditation.

104
00:07:13,000 --> 00:07:17,950
 It shouldn't be a hobby, this should be something that

105
00:07:17,950 --> 00:07:20,000
 changes your life,

106
00:07:20,000 --> 00:07:24,450
 it should be a part of your life, every day meditating,

107
00:07:24,450 --> 00:07:26,000
 every day being mindful, whatever you do.

108
00:07:26,000 --> 00:07:29,880
 When you eat, eat mindfully, when you brush your teeth,

109
00:07:29,880 --> 00:07:32,000
 brush your teeth mindfully.

110
00:07:32,000 --> 00:07:36,000
 When you use the toilet, use the toilet mindfully.

111
00:07:36,000 --> 00:07:40,000
 When you lie down to sleep, lie down to sleep mindfully.

112
00:07:40,000 --> 00:07:44,000
 Take it seriously.

113
00:07:44,000 --> 00:07:47,000
 Number five, Appa Madha Garawata.

114
00:07:47,000 --> 00:07:50,000
 So this is heedfulness.

115
00:07:50,000 --> 00:07:55,000
 Appa Madha is a trademark teaching of the Buddha,

116
00:07:55,000 --> 00:07:59,000
 it's very core teaching.

117
00:07:59,000 --> 00:08:04,350
 It means being mindful, being vigilant, being constantly

118
00:08:04,350 --> 00:08:08,000
 aware, consistent, continuously.

119
00:08:08,000 --> 00:08:11,950
 If you don't have continuity in your practice, it's like

120
00:08:11,950 --> 00:08:15,000
 two steps forward, one step back.

121
00:08:15,000 --> 00:08:18,790
 It slows you down, it can even take you backwards if you're

122
00:08:18,790 --> 00:08:21,000
 not consistent enough.

123
00:08:21,000 --> 00:08:26,070
 You should try to be vigilant, heedful, don't lose sight of

124
00:08:26,070 --> 00:08:32,200
 what's right, what's good, what's positive, don't get lost

125
00:08:32,200 --> 00:08:36,000
 and negative.

126
00:08:36,000 --> 00:08:39,000
 And number six, Pati Santara.

127
00:08:39,000 --> 00:08:43,680
 Pati Santara, this problematic word which doesn't mean love

128
00:08:43,680 --> 00:08:44,000
.

129
00:08:44,000 --> 00:08:51,000
 Pati San, pati means, specifically, sang means together,

130
00:08:51,000 --> 00:08:54,000
 they don't really have a meaning but

131
00:08:54,000 --> 00:09:01,000
 Tara, stir, comes from, it says literally spreading before.

132
00:09:01,000 --> 00:09:08,000
 But Pati Santara is understood to refer to hospitality.

133
00:09:08,000 --> 00:09:11,400
 There's two kinds of pati santara, amisa pati santara and d

134
00:09:11,400 --> 00:09:13,000
hamma pati santara,

135
00:09:13,000 --> 00:09:17,000
 so they don't have anything to do with love, not directly.

136
00:09:17,000 --> 00:09:22,140
 And goodwill, perhaps, friendliness, but hospitality is

137
00:09:22,140 --> 00:09:24,000
 probably the best.

138
00:09:24,000 --> 00:09:28,360
 So when you put things out for strangers, one is when

139
00:09:28,360 --> 00:09:29,000
 someone,

140
00:09:29,000 --> 00:09:33,000
 so when people come to visit you're being hospitable.

141
00:09:33,000 --> 00:09:39,740
 It's curious that the Buddha uses this, but it is what

142
00:09:39,740 --> 00:09:46,000
 keeps the religion alive, keeps Buddhism alive.

143
00:09:46,000 --> 00:09:53,000
 Good will, so goodwill to others, sharing with others,

144
00:09:53,000 --> 00:09:57,000
 receiving others hospitably.

145
00:09:57,000 --> 00:10:01,000
 Amisa pati santara is with gifts, with physical things,

146
00:10:01,000 --> 00:10:03,000
 with people that need food,

147
00:10:03,000 --> 00:10:06,520
 giving them food, when people need shelter, giving them

148
00:10:06,520 --> 00:10:11,000
 shelter, this kind of thing.

149
00:10:11,000 --> 00:10:15,410
 Dhamma pati santara means offering the dhamma, offering

150
00:10:15,410 --> 00:10:16,000
 teachings,

151
00:10:16,000 --> 00:10:18,530
 sharing the teachings with others, you don't have to be a

152
00:10:18,530 --> 00:10:21,000
 teacher to share teachings.

153
00:10:21,000 --> 00:10:27,750
 You can explain or demonstrate meditation practice just as

154
00:10:27,750 --> 00:10:40,000
 goodwill, as a gift, as a help for others.

155
00:10:40,000 --> 00:10:44,670
 So taking that seriously, all of these are said to be

156
00:10:44,670 --> 00:10:46,000
 important.

157
00:10:46,000 --> 00:10:52,510
 The Buddha says, abhavbho parihana ya nirbhaana seva sant

158
00:10:52,510 --> 00:10:53,000
ike.

159
00:10:53,000 --> 00:11:02,000
 They are not capable of fading away.

160
00:11:02,000 --> 00:11:09,670
 They are close there in the presence of nirbhaana, nirbha

161
00:11:09,670 --> 00:11:12,000
ana seva santike.

162
00:11:12,000 --> 00:11:17,000
 Things that lead you to nirbhaana.

163
00:11:17,000 --> 00:11:20,380
 So Buddhism sort of has a rap for being fairly dour and

164
00:11:20,380 --> 00:11:21,000
 serious.

165
00:11:21,000 --> 00:11:25,000
 And here's more about taking things seriously.

166
00:11:25,000 --> 00:11:28,340
 Just because you take something seriously doesn't mean you

167
00:11:28,340 --> 00:11:30,000
 have to be dour or serious.

168
00:11:30,000 --> 00:11:34,450
 It means you have to be conscientious, I think is a very

169
00:11:34,450 --> 00:11:36,000
 Buddhist word.

170
00:11:36,000 --> 00:11:42,790
 Conscientious, don't be lazy, don't be negligent, don't be

171
00:11:42,790 --> 00:11:46,000
 flippant or laxidasical.

172
00:11:46,000 --> 00:11:52,000
 Be conscientious, mindful.

173
00:11:52,000 --> 00:11:55,000
 These are good words.

174
00:11:55,000 --> 00:11:59,100
 So again, I think we've already had this quote somehow. It

175
00:11:59,100 --> 00:12:00,000
's very familiar.

176
00:12:00,000 --> 00:12:04,320
 I know this quote from my own studies, but it seems like we

177
00:12:04,320 --> 00:12:07,000
've gone through this recently.

178
00:12:07,000 --> 00:12:12,000
 So that's the Dhamma for this evening.

179
00:12:12,000 --> 00:12:15,000
 We have questions.

180
00:12:15,000 --> 00:12:18,000
 People have questions about our site.

181
00:12:18,000 --> 00:12:21,860
 The site's going to undergo a big change, so don't worry

182
00:12:21,860 --> 00:12:25,000
 too much about it yet.

183
00:12:25,000 --> 00:12:32,100
 Once we change over to the new format, we've got an IT team

184
00:12:32,100 --> 00:12:36,000
 diligently working on the site.

185
00:12:36,000 --> 00:12:39,000
 So hopefully that'll all come together this summer.

186
00:12:39,000 --> 00:12:57,000
 [

187
00:12:57,000 --> 00:12:59,000
 No questions today?

188
00:13:05,000 --> 00:13:10,000
 We don't have to have lots of questions every day.

189
00:13:10,000 --> 00:13:14,110
 There will be questions, there have been questions, there

190
00:13:14,110 --> 00:13:19,000
 will be more questions.

191
00:13:19,000 --> 00:13:22,530
 When the Lord said, "Kustalasa utta sampada," was he

192
00:13:22,530 --> 00:13:25,000
 referring to all good deeds,

193
00:13:25,000 --> 00:13:30,000
 or the good deeds associated with the Noble Eightfold Path?

194
00:13:30,000 --> 00:13:35,000
 [No questions today?

195
00:13:35,000 --> 00:13:39,010
 We don't have to have all good deeds associated with the

196
00:13:39,010 --> 00:13:41,000
 Noble Eightfold Path.]

197
00:13:41,000 --> 00:13:45,000
 I don't know the mind of the Buddha.

198
00:13:45,000 --> 00:13:49,000
 I would venture that he meant all wholesome deeds,

199
00:13:49,000 --> 00:13:53,990
 because Buddhas do teach that which is not directly related

200
00:13:53,990 --> 00:13:57,000
 to the Eightfold Noble Path, not directly.

201
00:13:57,000 --> 00:13:59,560
 Of course you could argue that all good deeds are

202
00:13:59,560 --> 00:14:02,000
 supportive of the Eightfold Noble Path.

203
00:14:02,000 --> 00:14:05,000
 I would say it's more in that case.

204
00:14:05,000 --> 00:14:10,340
 Buddhas teach things that are supportive, but not directly

205
00:14:10,340 --> 00:14:13,000
 a part of the Eightfold Noble Path.

206
00:14:13,000 --> 00:14:17,490
 I mean, charity is not in the Eightfold Noble Path, but it

207
00:14:17,490 --> 00:14:20,000
 is supportive of the Eightfold Noble Path.

208
00:14:20,000 --> 00:14:22,000
 And Buddhas do teach it.

209
00:14:22,000 --> 00:14:32,280
 On the other hand, the word "upasampada," you could argue,

210
00:14:32,280 --> 00:14:36,000
 means referring to the...

211
00:14:36,000 --> 00:14:39,700
 It's not just saying, perform wholesome deeds, it's saying

212
00:14:39,700 --> 00:14:41,000
 get to the pinnacle,

213
00:14:41,000 --> 00:14:49,000
 get to the highest, become complete, become full.

214
00:14:49,000 --> 00:14:53,880
 And so there it's actually specifically referring to the

215
00:14:53,880 --> 00:15:01,000
 Eightfold Noble Path, I would say.

216
00:15:01,000 --> 00:15:06,010
 There's difficult questions you give me. Good, keep me on

217
00:15:06,010 --> 00:15:08,000
 my toes.

218
00:15:08,000 --> 00:15:11,630
 And you talk about restraint. How much and in what way

219
00:15:11,630 --> 00:15:14,000
 should one restrain oneself?

220
00:15:14,000 --> 00:15:16,770
 There's a good quote of the Buddha that I can't remember

221
00:15:16,770 --> 00:15:20,000
 exactly where it is. Mahasya Sayadaw talks about it.

222
00:15:20,000 --> 00:15:25,000
 Restraint, sanyata is restraint, or it's one word.

223
00:15:25,000 --> 00:15:44,000
 Sanyama means restraint.

224
00:15:44,000 --> 00:15:49,920
 You restrain the hands, you restrain of the feet, you

225
00:15:49,920 --> 00:15:54,000
 restrain of the body, of the self.

226
00:15:54,000 --> 00:16:01,000
 All kinds of restraint are good.

227
00:16:01,000 --> 00:16:03,390
 So there are different kinds. He talks about the five

228
00:16:03,390 --> 00:16:07,000
 things, the five sorts of restraint.

229
00:16:07,000 --> 00:16:11,500
 Restraint through morality, restraint through effort,

230
00:16:11,500 --> 00:16:14,000
 restraint through knowledge,

231
00:16:14,000 --> 00:16:17,780
 restraint through mindfulness, and restraint through wisdom

232
00:16:17,780 --> 00:16:20,000
. I think those are the five.

233
00:16:20,000 --> 00:16:24,180
 So there are different kinds of restraint. Now, the word "

234
00:16:24,180 --> 00:16:27,000
restraint" kind of implies forcing,

235
00:16:27,000 --> 00:16:30,320
 but that's not really it. You think of these five, you

236
00:16:30,320 --> 00:16:33,000
 restrain yourself in different ways,

237
00:16:33,000 --> 00:16:37,000
 but with morality, and that's kind of forcing.

238
00:16:37,000 --> 00:16:42,040
 So that's kind of the coarsest way, just saying, "No, no, I

239
00:16:42,040 --> 00:16:46,000
'm not going to do that, I'm not going to do that."

240
00:16:46,000 --> 00:16:53,080
 But then, with effort, maybe somehow related, morality and

241
00:16:53,080 --> 00:16:54,000
 effort.

242
00:16:54,000 --> 00:16:58,000
 But effort can also refer to the effort in meditation.

243
00:16:58,000 --> 00:17:03,820
 If you work hard in Samatha meditation, I think it's effort

244
00:17:03,820 --> 00:17:07,000
. I'm actually right, remember.

245
00:17:07,000 --> 00:17:16,760
 Virya, with knowledge. No, it's not knowledge. Nyanasamwara

246
00:17:16,760 --> 00:17:17,000
.

247
00:17:17,000 --> 00:17:20,590
 So it's not Panya, it's not the fifth one. There's five of

248
00:17:20,590 --> 00:17:22,000
 them, I can't remember them all.

249
00:17:22,000 --> 00:17:28,950
 Should know them. With mindfulness is probably the most

250
00:17:28,950 --> 00:17:34,000
 important, so not exactly forcing yourself,

251
00:17:34,000 --> 00:17:40,040
 but straightening your mind. So when the mind experiences

252
00:17:40,040 --> 00:17:43,000
 something, see it clearly.

253
00:17:43,000 --> 00:17:47,000
 When you feel pain and you say to yourself, "Pain, pain."

254
00:17:47,000 --> 00:17:50,660
 You're restraining yourself in the sense of you're diver

255
00:17:50,660 --> 00:17:55,000
ting your attention not to the reactions,

256
00:17:55,000 --> 00:17:58,440
 what the mind thinks of it, but by fixing on the actual

257
00:17:58,440 --> 00:18:00,000
 experience itself.

258
00:18:00,000 --> 00:18:02,680
 So that restrains the mind in the sense that it keeps the

259
00:18:02,680 --> 00:18:06,000
 mind from getting upset about the pain,

260
00:18:06,000 --> 00:18:15,000
 or attached to things, enjoyable things.

261
00:18:15,000 --> 00:18:18,000
 Sabe, where is Sabe? Sabe.

262
00:18:18,000 --> 00:18:22,000
 Kusala, saba pa pasa karanam kusala saba.

263
00:18:22,000 --> 00:18:26,000
 Sabe, saba.

264
00:18:26,000 --> 00:18:31,000
 Saba is for the first line, I don't understand.

265
00:18:31,000 --> 00:18:40,000
 Saba pa pasa karanam.

266
00:18:40,000 --> 00:18:44,560
 Kusala, saba, I mean it's in contrast with papa. Papa means

267
00:18:44,560 --> 00:18:47,000
 evil. So kusala is any good.

268
00:18:47,000 --> 00:18:54,540
 Upa sampada, I could imply, is all, be full, do all kinds

269
00:18:54,540 --> 00:18:56,000
 of good.

270
00:18:56,000 --> 00:19:03,200
 Sabe, mabhikve, bahita, punya, nang, don't be afraid of pun

271
00:19:03,200 --> 00:19:06,000
ya, which is kusala.

272
00:19:06,000 --> 00:19:09,530
 During walking meditation sometimes we hear some joints

273
00:19:09,530 --> 00:19:12,000
 making popping sounds out of nowhere.

274
00:19:12,000 --> 00:19:18,000
 Why does that hearing arise?

275
00:19:18,000 --> 00:19:21,000
 I don't have any take on that, that's the body.

276
00:19:21,000 --> 00:19:24,110
 You have to talk to someone who knows stuff about bones. I

277
00:19:24,110 --> 00:19:28,000
 believe it's gas releasing the body.

278
00:19:28,000 --> 00:19:31,070
 You're thinking philosophically, what is the karma that

279
00:19:31,070 --> 00:19:32,000
 leads to that?

280
00:19:32,000 --> 00:19:36,000
 No, it's very complicated, it's the karma of being human.

281
00:19:36,000 --> 00:19:39,310
 We've put together this very complex being that has lots of

282
00:19:39,310 --> 00:19:46,000
 complex things happen to it, including our joints popping.

283
00:19:46,000 --> 00:19:50,000
 It's just part of being human.

284
00:19:50,000 --> 00:19:52,430
 Perhaps we're going to have to close that window when we

285
00:19:52,430 --> 00:19:55,000
 record. No, it's okay, next time.

286
00:19:55,000 --> 00:20:01,000
 It's unfortunate because it's so hot up here.

287
00:20:01,000 --> 00:20:07,000
 Why does the mind go to the ear?

288
00:20:07,000 --> 00:20:13,000
 The mind is not focused, the mind is the nature of the mind

289
00:20:13,000 --> 00:20:13,000
.

290
00:20:13,000 --> 00:20:20,000
 When there's an experience, it is drawn to the experience.

291
00:20:20,000 --> 00:20:25,590
 Technically in Vyabhidhamma it's all cause and effect, so

292
00:20:25,590 --> 00:20:28,000
 there's resultant.

293
00:20:28,000 --> 00:20:37,570
 Seeing, hearing, smelling, tasting, feeling, thinking they

294
00:20:37,570 --> 00:20:41,000
 are the results of karma.

295
00:20:41,000 --> 00:20:54,000
 They're related to things that happened in the past.

296
00:20:54,000 --> 00:20:59,150
 We're not so concerned about why it's happening, we're

297
00:20:59,150 --> 00:21:00,650
 concerned about what is happening, the nature of what is

298
00:21:00,650 --> 00:21:01,000
 happening.

299
00:21:01,000 --> 00:21:06,090
 Because it's not who cares why, if it is happening, that's

300
00:21:06,090 --> 00:21:07,000
 the issue.

301
00:21:07,000 --> 00:21:11,730
 And of course the popping isn't the problem, the issue is

302
00:21:11,730 --> 00:21:14,000
 how you react to it, that's what's really important.

303
00:21:14,000 --> 00:21:16,740
 It's important that we learn how we react to these things,

304
00:21:16,740 --> 00:21:18,000
 does it frustrate you?

305
00:21:18,000 --> 00:21:22,000
 Do you feel curious? Do you wonder why you're the popping?

306
00:21:22,000 --> 00:21:24,400
 Wondering, that's what we want to learn about. Is that good

307
00:21:24,400 --> 00:21:26,000
? Is that bad? Is that problematic?

308
00:21:26,000 --> 00:21:30,190
 Is that the best way to use my mental attention, my mental

309
00:21:30,190 --> 00:21:31,000
 power?

310
00:21:31,000 --> 00:21:35,000
 That's what we're focused on, that's what's important.

311
00:21:35,000 --> 00:21:38,000
 The why isn't such a big deal.

312
00:21:38,000 --> 00:21:56,000
 Okay, no more questions, then good night everyone.

313
00:21:56,000 --> 00:21:59,000
 See you next time.

314
00:21:59,000 --> 00:22:06,000
 [Music]

