 Okay, welcome everyone. Now we're starting. We're
 continuing our study of the Visuddhi
 manga. Robin, you want to give us an introduction? Where
 are we?
 Sure, we're on chapter seven on page 203, number 49. And
 for our new people here this
 week, what we do is we just go down the list alphabetically
 and read. If you don't have
 a mic or you're not able to read aloud this week for some
 reason, just mute yourself so
 we'll know. But otherwise we'll just go right down the list
. So if you could start us off
 for a row, that would be great. We're continuing on with
 Buddha and Nusati.
 So we're going through the qualities of the Buddha. And
 these are what someone who is
 practicing mindfulness of the Buddha would contemplate. And
 so the explanation, it's
 going to be like this with most of the rest of the
 meditation subjects. It's a detailed
 explanation for the purpose as a background knowledge for
 the practitioner to have this
 theory and this knowledge of what exactly it is we are
 contemplating so that when you
 actually put it into practice, these qualities, these
 concepts will arise in the mind or will
 be quick to come because of having it all in the back of
 your mind.
 So sometimes it seems a little detailed. Why is this all
 theoretical? Well, this is the
 preliminary understanding that you need in order to really
 clearly make a meditation,
 a creative contemplation with full knowledge and awareness
 of what it is that you're contemplating.
 So today we're on teachers of gods and men.
 Teacher of gods and men. He teaches by means of here and
 now, of the life to come and of
 the ultimate goal, according as befits the case, thus he's
 the teacher. And furthermore,
 this meaning should be on just according to the Nadesa,
 thus teacher, the blessed one
 is a caravan leader since he brings home caravans. Just as
 one who was a caravan home gets caravans
 across a wilderness, gets them across a robber and pestered
 wilderness, gets them across
 a wild beast and pestered wilderness, gets them across a
 foodless wilderness, gets them
 across a waterless wilderness, gets them right across, gets
 them quite across, gets them
 properly across, gets them to reach a land of safety. So
 too the blessed one is a caravan
 leader, one who brings home the caravans. He gets them
 across a wilderness, gets them
 across the wilderness of earth.
 Of gods and men. Deva Manusanam. Resolution of compound.
 This is said in order to denote
 those who are the best and also to note those persons
 capable of progress. For the blessed
 one as a teacher bestowed his teaching upon animals as well
. For when animals can, through
 listening to the blessed one's dhamma, acquire the benefit
 of a suitable rebirth as support
 for progress and with the benefit of that same support,
 they come in their second or
 third rebirth to partake of the path and its fruition.
 Thank you, Glenn. Jason, are you able to read? Do you have
 a mic hooked up?
 Maybe not. László, would you be able to read 51?
 Yes. Manka, the deary's son and others illustrate this.
 While the blessed one was teaching the
 dhamma to the inhabitants of the city of Champa on the
 banks of the Gagara lake, it seems
 a frog apprehended a sign in the blessed one's voice. A cow
herd who was standing leaning
 on a stick put his stick on the frog's head and crushed it.
 He died and was straight away
 reborn in a gilded, divine place, twelve leagues brought in
 the realm of the Thirty-Three. He
 found himself there, as if waking up from sleep amidst a
 host of celestial nymphs, and
 he exclaimed, "So I have actually been reborn here. What
 did I do?" When he sought for
 the reason, he found it was none other than his
 apprehension of the sign in the blessed
 one's voice. He went with it, his divine palace, at once to
 the blessed one, and paid homage
 at his feet. Though the blessed one knew about it, he asked
 him, "Who now pays homage at
 my feet, shining with glory of success, illuminating all
 around, with beauty so astounding?"
 In my last life I was a frog, the waters of a pound my home
. A cowherd's crook ended
 my life while listening to your dhamma. The blessed one
 themed the dhamma. Eighty-four
 thousand creatures gained penetration to the dhamma. As
 soon as the deity's son became
 established in the fruition of stream entry, he smiled and
 then vanished. Just a quick
 note, the word devaputa doesn't actually mean deity's son.
 Putta here means descendant
 of it. It literally means son, but devaputa is just a word
 used to mean angel, like one
 of the family of angels. So deity's son is too literal. It
 really just means belonging
 to the family of angels, so being an angel basically. It's
 an expression.
 I think even Mars and just like that in some places devaput
amara.
 That's right. The word putta is used, it literally means
 son, but it's used colloquially to mean
 descendant of. And even that is misleading because it's not
 like they actually give
 birth. It's just a general word like sakya putta. It doesn
't mean literally a son. It
 means belonging to the family. So that just means being an
 angel. Devaputa is another
 word for angel.
 Thank you. Naga, are you able to read 52? I think you just
 have to unmute yourself.
 He's enlightened with the, can you hear me?
 Yes, very well.
 He's enlightened with the knowledge that belongs to the
 fruit of liberation. Since everything
 that can be known has been discovered by him. Or
 alternatively, he discovered the four truths
 by himself and awakened others to them. Does and for others
, for other such reasons, he's
 enlightened. And in order to explain this meaning, the
 whole passage in the Nadesa beginning
 does he's the discoverer, the jihitar of the truth. Does he
's enlightened Buddha? He's
 awakened the jihitar of the generation. Does he's
 enlightened Buddha? Nade, 1557. Or the
 same passage from the
 Sanbhita should be quoted in detail.
 Blessed is a term signifying the respect and veneration,
 accorded to him as the highest
 of all beings and distinguished by his special qualities.
 Hence the ancient said, blessed
 is the best of words. Blessed is the finest word. Deserving
 awe and veneration, blessed
 is the name therefore.
 Or alternatively, names are of four kinds, denoting a
 period of life, describing a particular
 mark, signifying a particular
 requirement and fortuitously arisen, which last in the
 current usage of the word is called
 capricious. Herein names to note a period of life are those
 such as yearling calf, steered
 to be trained, yokaks, and the like. Names describing a
 particular mark are those such
 as staff bearer, umbrella bearer, topknot wearer, hand poss
essor, and the like. Names signifying
 a particular requirement are those such as possessor of the
 threefold clear vision, possessor
 of the six direct knowledges, and the like. Such names are
 seerawadaka, augmentor of luster,
 danawidaka, augmentor of wealth, etc., are fortuitously
 arisen names. They have no reference
 to the word meanings.
 This name blessed is one signifying a particular
 requirement. It is not made by Maha-Maya or
 by King Sudodana or by 80,000 kingsmen or by distinguished
 deities like Sakka, Santosita,
 and others. And this is said by the general of law, general
 of the law. Blessed, this
 is not a name made by a mother. This name Buddha which
 signifies final liberation is
 a realistic description of Buddha's enlightened ones. The
 blessed ones together with their
 obtainment of omniscient knowledge at the root of
 enlightenment, at the root of an enlightenment
 tree.
 Now, in order to extend also the special quality signified
 by this name, they cite the following
 stanza.
 Help.
 Let me say that then. Let's just for sake of completion.
 Bhagi bhaji bagi wibhata wa iti akasi bhaganti gauruti bh
agi wa bhoo hi nya ye hi su bhavi
 tatano bhavantago so bhagavati uchiti.
 The reverent one has blessings, is a prequenter, a partaker
, a possessor of what has been analyzed.
 He has caused abolishing. He is fallen. He has fully
 developed himself in many ways.
 He has gone to the end of becoming. Thus is called lust.
 The meaning of these words should be understood according
 to the method of explanation given
 in the nidasa.
 And there is this other way.
 Bhagya wa bhagya wa yutto bhagya hi ca wibhata wa bhata wa
 wanta gamano bhavi su bhagya wa
 tatoo.
 He is fortunate, bhagya wa, possessed of abolishment, bhag
ya wa, associated with blessings, yutto
 bhavi, and the possessor of what has been analyzed, uvim bh
ata wa.
 He has frequented bhata wa and he has rejected going in the
 kinds of becoming, wanta gamano
 bhavi su.
 Thus he has blessed bhagya wa.
 Herein by using the characteristic of language beginning
 with vowel augmentation of syllable,
 or by using the characteristic of insertion beginning with
 the example of isodhara, isodhaksa,
 etc.
 It may be known that he can also be called blessed when he
 can be called fortunate, owing
 to the fortunateness to have reached the further shore of
 the ocean of perfection of giving
 virtue, etc., which produce mundane and submundane bliss.
 Similarly he can also be called blessed when he can be
 called possessed of abolishment,
 owing to the following weaknesses having been abolished.
 For he has abolished all the hundred thousand kinds of
 trouble, anxiety and defilement,
 classes, greed, as hate, as illusion, and as misdirected
 attention, as tensionlessness
 and shamelessness, as anger and enmity, as contempt and dom
ineering, as envy and avarice,
 as deceit and fraud, as obduracy and presumption, as pride,
 as haughtiness, as vanity and negligence,
 as craving and ignorance.
 As the three roots of the unprofitable kinds of misconduct,
 defilement, stains, fictitious
 perceptions, applied thoughts and diversifications, as the
 four perversing senses, cankers, tights,
 plots, bonds, bad ways, cravings and clingings, as the five
 wilderness in the heart, shakles
 in the heart, hindrances in kinds of delight, as the six
 fruits of discord and grouse of
 craving, as the seven hindrances, as the eight wrongnesses,
 as the nine things rooted in
 craving, as the ten forces of unprofitable action, as the
 sixty-two kinds of false view,
 as the hundred and eight ways of behavior and craving.
 In brief, the five maras, that is to say,
 [
 And
 by his fortunateness is indicated the excellence of his
 material body which bears a hundred
 characteristics of merit, and by his having abolished
 defects is indicated the excellence
 of his dhamma body.
 Likewise, by his fortunateness is indicated the esteem of
 worldly people, and by his having
 abolished defects the esteem of those who resemble him, and
 by his fortunateness is indicated
 that he is fit to be relied on by laymen, and by his having
 abolished defects that he
 is fit to be relied on by those gone forth into
 homelessness.
 And when both have relied on him, they acquire relief from
 bodily and mental pain, as well
 as help with both material and dhamma gifts, and they are
 rendered capable of finding both
 mundane and supermundane bliss.
 He is also called blessed, since he is associated with
 blessings, such as those of the following
 kind, in the sense that he has those blessings.
 Now in the world the word "blessing" is used for six things
, namely lordship, dhamma, fame,
 glory, wish and endeavor.
 He has supreme lordship over his own mind, either the kind
 reckoned as mundane and consisting
 in minuteness, lightness, etc., or that complete in all
 aspects, and likewise the supramundane
 dhamma.
 And he has exceedingly pure fame, spread through the three
 worlds acquired through the special
 quality of veracity.
 And he has glory of all limbs, perfect in every aspect,
 which is capable of comforting
 the eyes of people, eager to see his material body.
 And he has his wish, in other words, the production of what
 is wanted, whatever is wanted, and
 needed by him as beneficial to himself or others, is then
 and there produced for him.
 And he has the endeavor, in other words, the right effort,
 which is the reason why the
 whole world venerates him.
 Thank you.
 Nanka, can you read 62?
 I
 read
 one
 of
 yours
 in
 the
 of
 read
 in
 the
 is
 the
 and
 the
 body
 he is
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
 the
