1
00:00:00,000 --> 00:00:03,960
 "Bhante, could you please tell us how you would meet

2
00:00:03,960 --> 00:00:05,700
 criticism regarding rebirth from

3
00:00:05,700 --> 00:00:08,120
 modern materialists such as Richard Dawkins?"

4
00:00:08,120 --> 00:00:12,520
 Okay, here's one that we can all sink our teeth into.

5
00:00:12,520 --> 00:00:13,520
 Richard Dawkins.

6
00:00:13,520 --> 00:00:18,320
 I'll go on the record saying I'm not really fond of Richard

7
00:00:18,320 --> 00:00:19,360
 Dawkins.

8
00:00:19,360 --> 00:00:22,970
 I like some of the things he says, but if you look at, I

9
00:00:22,970 --> 00:00:25,680
 don't know, this is an ad hominem,

10
00:00:25,680 --> 00:00:32,190
 isn't it? But there is something from a Buddhist

11
00:00:32,190 --> 00:00:33,000
 perspective to... it's not answering the question

12
00:00:33,000 --> 00:00:36,430
 at all really, it's hopefully kind of sitting with me, is

13
00:00:36,430 --> 00:00:38,280
 that it's sad really to see how

14
00:00:38,280 --> 00:00:43,820
 materialism destroys someone's spiritual qualities. It

15
00:00:43,820 --> 00:00:48,000
 makes them seem very, very coarse and unpleasant

16
00:00:48,000 --> 00:00:51,030
 in a sense. I mean, to me anyway, as refined as he sounds,

17
00:00:51,030 --> 00:00:52,440
 it's clear that he's not a very

18
00:00:52,440 --> 00:00:55,970
 refined individual, so I'm going to point you to it. But I

19
00:00:55,970 --> 00:00:57,320
'd like to open it up to other

20
00:00:57,320 --> 00:00:58,320
 people.

21
00:00:58,320 --> 00:01:04,580
 Well, it seems that Richard Dawkins had a very tough Anglic

22
00:01:04,580 --> 00:01:07,520
an childhood, you know?

23
00:01:07,520 --> 00:01:10,880
 Honestly.

24
00:01:10,880 --> 00:01:15,310
 The really difficult thing is to convince or to show

25
00:01:15,310 --> 00:01:18,520
 materialists that they're holding

26
00:01:18,520 --> 00:01:23,970
 a faith, that they believe in a material physical world, is

27
00:01:23,970 --> 00:01:28,400
 a belief that is not founded in actual

28
00:01:28,400 --> 00:01:29,400
 experience.

29
00:01:29,400 --> 00:01:30,400
 Or physics.

30
00:01:30,400 --> 00:01:35,070
 Well, physics is all based on assumptions and theories that

31
00:01:35,070 --> 00:01:36,520
 are not...

32
00:01:36,520 --> 00:01:40,230
 No, but I mean, modern physics shows that. Modern physics

33
00:01:40,230 --> 00:01:43,000
 seems to suggest, barring multiverse

34
00:01:43,000 --> 00:01:48,510
 or so on, or many worlds that are not perceived, seems to

35
00:01:48,510 --> 00:01:51,800
 suggest that it is the case, that

36
00:01:51,800 --> 00:01:55,370
 entities don't exist and it's all based on, to some extent,

37
00:01:55,370 --> 00:01:56,800
 based on experience.

38
00:01:56,800 --> 00:02:03,250
 Well, hell, if you really want to look at it, material form

39
00:02:03,250 --> 00:02:07,120
 doesn't even actually exist.

40
00:02:07,120 --> 00:02:12,690
 Color doesn't even actually exist. It's what we perceive as

41
00:02:12,690 --> 00:02:15,600
 color, but really in the outer

42
00:02:15,600 --> 00:02:21,040
 world, it's just vibrations being reflected off of surfaces

43
00:02:21,040 --> 00:02:23,880
. There is no color. We interpret

44
00:02:23,880 --> 00:02:29,280
 the world. We create this world from our interpretations of

45
00:02:29,280 --> 00:02:32,720
 things and give it solidity and color

46
00:02:32,720 --> 00:02:38,450
 and form and meaning where really, to another being with

47
00:02:38,450 --> 00:02:41,200
 different senses, it's a completely

48
00:02:41,200 --> 00:02:46,620
 different world. And that's the problem with materialist

49
00:02:46,620 --> 00:02:47,080
 views.

50
00:02:47,080 --> 00:02:51,630
 Not exactly, because you can do the same experiment a

51
00:02:51,630 --> 00:02:55,320
 million times and get the same physics experiment

52
00:02:55,320 --> 00:02:58,930
 a million times and get the same results. So obviously

53
00:02:58,930 --> 00:03:01,040
 there is, not proof, but a clear

54
00:03:01,040 --> 00:03:05,700
 indication that a physics would say that there is the

55
00:03:05,700 --> 00:03:07,360
 physical realm.

56
00:03:07,360 --> 00:03:11,390
 Oh, no, no, no. I'm saying that there is the physical realm

57
00:03:11,390 --> 00:03:13,240
. What I'm implying is that

58
00:03:13,240 --> 00:03:17,200
 our interpretation as to what is the reality of this

59
00:03:17,200 --> 00:03:19,280
 physical realm...

60
00:03:19,280 --> 00:03:22,460
 It's the perception. There's a lot of psychological

61
00:03:22,460 --> 00:03:23,440
 research.

62
00:03:23,440 --> 00:03:27,840
 It's me who's saying that there's no physical realm.

63
00:03:27,840 --> 00:03:34,040
 Okay, but you're actually giving fodder to their argument.

64
00:03:34,040 --> 00:03:35,040
 Because then they would say,

65
00:03:35,040 --> 00:03:38,870
 "Well, yes, that's why we can't trust experience, why we

66
00:03:38,870 --> 00:03:41,240
 can't trust meditation, because our

67
00:03:41,240 --> 00:03:45,380
 perceptions are skewed. We have to use..." This is why they

68
00:03:45,380 --> 00:03:46,720
 will say, "This is why they

69
00:03:46,720 --> 00:03:51,140
 fight against meditation as a 'special kind of knowing' or

70
00:03:51,140 --> 00:03:53,100
 any special kind of knowing.

71
00:03:53,100 --> 00:03:57,670
 You can only know something from a materialist point of

72
00:03:57,670 --> 00:04:01,800
 view through rigorous third, impersonal

73
00:04:01,800 --> 00:04:06,850
 experimentation, where you've taken a subject completely

74
00:04:06,850 --> 00:04:08,960
 out of the picture. So this idea

75
00:04:08,960 --> 00:04:12,450
 of subjectivity is actually a bad thing, and subjective

76
00:04:12,450 --> 00:04:14,880
 experience is just that, subjective

77
00:04:14,880 --> 00:04:16,280
 in a bad way.

78
00:04:16,280 --> 00:04:25,680
 Well, that's quite old piece of... I mean, not problem. In

79
00:04:25,680 --> 00:04:29,120
 every psychology book, you

80
00:04:29,120 --> 00:04:33,850
 can find some of the different perceptions, how the eye

81
00:04:33,850 --> 00:04:36,840
 works, and how we actually experience

82
00:04:36,840 --> 00:04:45,820
 reality through our senses. There's a lot of question marks

83
00:04:45,820 --> 00:04:50,600
. We don't know how it works.

84
00:04:50,600 --> 00:04:53,200
 We still don't know how it works.

85
00:04:53,200 --> 00:04:57,350
 Yeah, but we have... materialists have pretty good evidence

86
00:04:57,350 --> 00:04:58,920
 to support their view. Like

87
00:04:58,920 --> 00:05:06,160
 at death, there's nothing left of brain activity, for

88
00:05:06,160 --> 00:05:08,240
 example. So just if we can get back to

89
00:05:08,240 --> 00:05:12,260
 what is the question? The question is, how would you deal

90
00:05:12,260 --> 00:05:15,840
 with people? How do you refute?

91
00:05:15,840 --> 00:05:18,200
 How would you meet criticism regarding rebirth?

92
00:05:18,200 --> 00:05:21,840
 Anyone read Richard O'Dowkey's book, The God-Delusion?

93
00:05:21,840 --> 00:05:24,650
 Yeah, but that's not a book of rebirth. Let's stay on track

94
00:05:24,650 --> 00:05:24,920
.

95
00:05:24,920 --> 00:05:27,480
 Yeah, I'll just ask you. Anyone?

96
00:05:27,480 --> 00:05:31,480
 I listened to your book.

97
00:05:31,480 --> 00:05:38,520
 I'll tell you personally, I'm still... I don't hold any

98
00:05:38,520 --> 00:05:42,760
 belief on rebirth and reincarnation

99
00:05:42,760 --> 00:05:50,720
 at all. I'm still open on that. I can't say I have a belief

100
00:05:50,720 --> 00:05:52,360
 or a faith in rebirth.

101
00:05:52,360 --> 00:05:58,080
 I think the good thing...

102
00:05:58,080 --> 00:06:01,880
 Just wait long enough. We'll all know the answer. It's not

103
00:06:01,880 --> 00:06:03,160
 really that important, is

104
00:06:03,160 --> 00:06:08,220
 it? But there's something great we do, because you kind of

105
00:06:08,220 --> 00:06:10,960
 have a choice. If your outlook

106
00:06:10,960 --> 00:06:14,500
 on life is that... well, you can even avoid the subject

107
00:06:14,500 --> 00:06:16,480
 entirely and just say, "Well,

108
00:06:16,480 --> 00:06:19,520
 just in the present moment." But there are problems with

109
00:06:19,520 --> 00:06:20,840
 that as well. So if you have

110
00:06:20,840 --> 00:06:28,010
 the view, if your worldview is based on permanent death,

111
00:06:28,010 --> 00:06:33,520
 not the moment of death, then you...

112
00:06:33,520 --> 00:06:36,870
 The problems with that are what you see from the material

113
00:06:36,870 --> 00:06:39,200
ist, which you see from secularists

114
00:06:39,200 --> 00:06:47,020
 in general, they tend to get caught up in addiction and

115
00:06:47,020 --> 00:06:51,520
 following after their desires

116
00:06:51,520 --> 00:06:57,880
 and living a very materialistic lifestyle. And we can see

117
00:06:57,880 --> 00:06:58,800
 the result that it's having

118
00:06:58,800 --> 00:07:02,070
 on the world. We can see the result it has on people's

119
00:07:02,070 --> 00:07:04,240
 minds and on their lives. But

120
00:07:04,240 --> 00:07:06,960
 the response, of course, is that, "Well, it's okay. When

121
00:07:06,960 --> 00:07:08,640
 you die, you're scot-free." And

122
00:07:08,640 --> 00:07:12,240
 all these negative habits of addiction and aversion that

123
00:07:12,240 --> 00:07:14,360
 you've built up, partialities

124
00:07:14,360 --> 00:07:18,370
 and so on, you give them up when you die. It's not quite

125
00:07:18,370 --> 00:07:22,600
 that simple, because actually

126
00:07:22,600 --> 00:07:25,470
 you have to deal with them even in this life as well. If

127
00:07:25,470 --> 00:07:27,640
 you ever stop and look at yourself

128
00:07:27,640 --> 00:07:30,720
 and what you're doing to yourself, it's quite unpleasant.

129
00:07:30,720 --> 00:07:31,800
 If you take, on the other hand,

130
00:07:31,800 --> 00:07:42,870
 the view of continuation and the impotence of death in that

131
00:07:42,870 --> 00:07:49,320
 sense to deprive you of the

132
00:07:49,320 --> 00:07:52,040
 fruit of your habits, then you've got to be a lot more

133
00:07:52,040 --> 00:07:53,920
 careful about how you act. And

134
00:07:53,920 --> 00:07:56,710
 you really do have to take the mind and experience

135
00:07:56,710 --> 00:08:01,640
 seriously. You have to take the effects of

136
00:08:01,640 --> 00:08:06,100
 your actions seriously. That tends to, one would think,

137
00:08:06,100 --> 00:08:08,400
 lead to more peace and harmony

138
00:08:08,400 --> 00:08:13,950
 in the world, on the one hand. And it also, well, it leads

139
00:08:13,950 --> 00:08:16,800
 to a different way of behavior,

140
00:08:16,800 --> 00:08:20,720
 I think, quite radically. So it's not trivial.

141
00:08:20,720 --> 00:08:29,060
 I'd like to interrupt you and ask you a question, please.

142
00:08:29,060 --> 00:08:29,200
 And it has exactly to do with what

143
00:08:29,200 --> 00:08:35,380
 you're asking with the question. It's the ontological

144
00:08:35,380 --> 00:08:39,000
 dilemma of what would take rebirth

145
00:08:39,000 --> 00:08:44,420
 if we don't have a permanent "ata," a permanent self. What

146
00:08:44,420 --> 00:08:47,280
 of the five skandhas or all of

147
00:08:47,280 --> 00:08:52,690
 the five skandhas would take rebirth that we would call an

148
00:08:52,690 --> 00:08:54,480
 "I" or a "self"?

149
00:08:54,480 --> 00:08:58,980
 I'll answer that if you like. But I'm trying to point it

150
00:08:58,980 --> 00:08:59,640
 out.

151
00:08:59,640 --> 00:09:01,560
 That's why we're bringing that up right now, because it's a

152
00:09:01,560 --> 00:09:03,640
 perfect thing for you to really

153
00:09:03,640 --> 00:09:09,120
 elucidate, because that causes a lot of confusion.

154
00:09:09,120 --> 00:09:12,990
 But what I was trying to discuss is the difference between

155
00:09:12,990 --> 00:09:15,680
 the two views and the different effect

156
00:09:15,680 --> 00:09:18,420
 that it has on your mind, regardless of whether one is

157
00:09:18,420 --> 00:09:20,080
 right or how you explain one or the

158
00:09:20,080 --> 00:09:24,200
 other. So you have these two views, then you also have the

159
00:09:24,200 --> 00:09:26,680
 view that neither one is important

160
00:09:26,680 --> 00:09:29,400
 and you should just focus on the present moment. I think

161
00:09:29,400 --> 00:09:33,800
 there's potential there, and it certainly

162
00:09:33,800 --> 00:09:37,960
 seems to be very much what the Buddha taught many times, to

163
00:09:37,960 --> 00:09:40,540
 just focus on the present moment.

164
00:09:40,540 --> 00:09:45,950
 But if there is no thought of the future or the

165
00:09:45,950 --> 00:09:50,360
 repercussions of your actions, in other

166
00:09:50,360 --> 00:09:57,720
 words, the potential for this moment to create another

167
00:09:57,720 --> 00:10:02,280
 moment and another moment, and to

168
00:10:02,280 --> 00:10:07,400
 have no escape from the next moment, if you don't have that

169
00:10:07,400 --> 00:10:09,160
, and if you're just really

170
00:10:09,160 --> 00:10:13,980
 sticking and saying, "Well, Carpe diem sees the day, live

171
00:10:13,980 --> 00:10:16,080
 for the now," so on, without

172
00:10:16,080 --> 00:10:22,700
 some sort of philosophical understanding or doctrine of

173
00:10:22,700 --> 00:10:26,360
 continuity, or maybe not even

174
00:10:26,360 --> 00:10:29,460
 philosophical, but the idea of cause and effect. Let's put

175
00:10:29,460 --> 00:10:30,400
 it this way, without the idea of

176
00:10:30,400 --> 00:10:35,420
 cause and effect, even a theory of just staying in the here

177
00:10:35,420 --> 00:10:38,680
 and now does, in practical terms,

178
00:10:38,680 --> 00:10:43,200
 lead people to selfishness and to hedonism. Hedonism is, I

179
00:10:43,200 --> 00:10:45,400
 think, very much living in the

180
00:10:45,400 --> 00:10:51,360
 now. So I think that is really the key, and it's also the

181
00:10:51,360 --> 00:10:54,400
 key to rebirth, cause and effect.

182
00:10:54,400 --> 00:10:57,930
 Science works in terms of cause and effect. The laws of

183
00:10:57,930 --> 00:10:59,880
 science show very clearly the

184
00:10:59,880 --> 00:11:03,970
 idea of cause and effect. Quantum physics shows cause and

185
00:11:03,970 --> 00:11:06,800
 effect, classical mechanics,

186
00:11:06,800 --> 00:11:09,810
 classical physics shows cause and effect, but so does

187
00:11:09,810 --> 00:11:11,840
 quantum physics. It just changes the

188
00:11:11,840 --> 00:11:17,560
 realm in which we talk about cause and effect. Buddhist

189
00:11:17,560 --> 00:11:21,600
 meditation shows cause and effect.

190
00:11:21,600 --> 00:11:24,960
 It's quite clear from all aspects that cause and effect is

191
00:11:24,960 --> 00:11:26,180
 reality. That's what we mean

192
00:11:26,180 --> 00:11:29,790
 by karma. When people say karma is faith-based, this is a

193
00:11:29,790 --> 00:11:31,960
 total misunderstanding of the concept

194
00:11:31,960 --> 00:11:35,510
 of karma. When people say that rebirth is faith-based, from

195
00:11:35,510 --> 00:11:38,240
 a Buddhist perspective that's a total

196
00:11:38,240 --> 00:11:44,270
 misunderstanding of our approach towards rebirth. Rebirth

197
00:11:44,270 --> 00:11:45,920
 is just an extrapolation of cause

198
00:11:45,920 --> 00:11:52,830
 and effect. We don't accept the belief that physical death

199
00:11:52,830 --> 00:11:57,560
 changes anything in the causal

200
00:11:57,560 --> 00:12:00,670
 continuum of consciousness from one moment to the next,

201
00:12:00,670 --> 00:12:02,280
 which we clearly perceive to

202
00:12:02,280 --> 00:12:06,250
 be occurring. It goes back to just the framework within

203
00:12:06,250 --> 00:12:08,280
 which we're working, which is a mental

204
00:12:08,280 --> 00:12:13,840
 framework. We don't subscribe to the idea that subjective

205
00:12:13,840 --> 00:12:15,680
 reality is a negative thing,

206
00:12:15,680 --> 00:12:19,010
 or subjective experience is a negative thing. We

207
00:12:19,010 --> 00:12:22,000
 differentiate between the aspects of personal

208
00:12:22,000 --> 00:12:25,970
 experience and the subjective ones in a way that science

209
00:12:25,970 --> 00:12:28,480
 doesn't, or materialist science

210
00:12:28,480 --> 00:12:31,880
 does. Materialist science says all experience is subjective

211
00:12:31,880 --> 00:12:33,600
 and leaves it at that. That's

212
00:12:33,600 --> 00:12:38,960
 certainly not the case. It's possible to get into a state

213
00:12:38,960 --> 00:12:41,800
 of mind, aka what we know of

214
00:12:41,800 --> 00:12:47,940
 as mindfulness, and have the results and the observations

215
00:12:47,940 --> 00:12:51,700
 of that experience be reproducible

216
00:12:51,700 --> 00:12:55,900
 in all cases, for all people. You could have a thousand

217
00:12:55,900 --> 00:12:59,800
 people performing the same experiments,

218
00:12:59,800 --> 00:13:03,340
 provided they're truthful, and so on and so on, provided

219
00:13:03,340 --> 00:13:05,280
 they're sane, provided they are

220
00:13:05,280 --> 00:13:09,400
 ordinary human beings. They will be able to achieve the

221
00:13:09,400 --> 00:13:11,160
 same results. This is what Sam

222
00:13:11,160 --> 00:13:14,260
 Harris talks about in terms of everyone being able to see

223
00:13:14,260 --> 00:13:16,240
 that compassion, that it's possible

224
00:13:16,240 --> 00:13:19,960
 to enter into these states that he seems to think are truly

225
00:13:19,960 --> 00:13:21,800
 objectively positive, or so

226
00:13:21,800 --> 00:13:26,600
 on. He seems to get a little bit caught up in them, but he

227
00:13:26,600 --> 00:13:29,120
 makes a good point that there's

228
00:13:29,120 --> 00:13:34,380
 an objective mindset. It does truly occur, and you could do

229
00:13:34,380 --> 00:13:36,660
 materialist diagnostics or

230
00:13:36,660 --> 00:13:43,050
 studies of it to see whether it was having the same effect

231
00:13:43,050 --> 00:13:45,840
 on the physical side.

232
00:13:45,840 --> 00:13:53,080
 And we simply take that objective reality more on a mental

233
00:13:53,080 --> 00:13:56,320
 level, to be the basis of

234
00:13:56,320 --> 00:14:01,880
 reality. And so you would have to introduce the belief of

235
00:14:01,880 --> 00:14:04,680
 nihilism, of annihilation at

236
00:14:04,680 --> 00:14:14,810
 death, whether this actually phases a materialist is deb

237
00:14:14,810 --> 00:14:18,800
atable. I talked to the Atheist Association

238
00:14:18,800 --> 00:14:21,430
 of Austin. What are they called? Do you know these guys

239
00:14:21,430 --> 00:14:23,000
 that are on YouTube? The atheists

240
00:14:23,000 --> 00:14:24,000
 of...

241
00:14:24,000 --> 00:14:28,140
 Yeah, I know. I heard about him. They're a radio kind of

242
00:14:28,140 --> 00:14:28,820
 thing.

243
00:14:28,820 --> 00:14:32,040
 And so I sent them an email, and they actually responded to

244
00:14:32,040 --> 00:14:33,800
 me. This mad guy. Oh, he was

245
00:14:33,800 --> 00:14:37,140
 horrible. He was worse than Richard. Like, just the coars

246
00:14:37,140 --> 00:14:37,520
est.

247
00:14:37,520 --> 00:14:41,200
 No, no, no, no. I think these people are very smart.

248
00:14:41,200 --> 00:14:44,640
 That was a guy named Aaron Raw?

249
00:14:44,640 --> 00:14:49,140
 I remember Aaron, but Matt Dillahunty, or whatever his name

250
00:14:49,140 --> 00:14:51,040
 is, he's the host. And he's

251
00:14:51,040 --> 00:14:54,870
 great when he's attacking theists, but he just lumped

252
00:14:54,870 --> 00:14:56,600
 meditation in with all of that.

253
00:14:56,600 --> 00:14:59,900
 He said to me at one point, "You could make them the next

254
00:14:59,900 --> 00:15:01,800
 exposed movie, this ridiculous

255
00:15:01,800 --> 00:15:05,480
 movie about teaching evolution in school." He said, "Yeah,

256
00:15:05,480 --> 00:15:06,520
 you want to teach mindfulness?

257
00:15:06,520 --> 00:15:12,930
 Well, you could write the next exposed movie." I don't

258
00:15:12,930 --> 00:15:16,040
 remember. I didn't watch it, but no,

259
00:15:16,040 --> 00:15:22,400
 but this Ben Stiller guy. Sam Harris is an atheist who

260
00:15:22,400 --> 00:15:25,200
 makes the distinction there, and

261
00:15:25,200 --> 00:15:28,320
 who makes this distinction.

262
00:15:28,320 --> 00:15:32,040
 Not all--

263
00:15:32,040 --> 00:15:33,200
 You mean Ben Stein?

264
00:15:33,200 --> 00:15:39,440
 Right, Ben Stein, not Ben Stiller. Ben Stein.

265
00:15:39,440 --> 00:15:44,510
 But Sam Harris, as I'm saying, there is the objective

266
00:15:44,510 --> 00:15:47,920
 aspect of personal experience that

267
00:15:47,920 --> 00:15:57,010
 truly, whatever theory you have about it, it truly holds,

268
00:15:57,010 --> 00:16:00,160
 and practices based on it

269
00:16:00,160 --> 00:16:04,720
 do have an effect. This Happiest Man in the World article.

270
00:16:04,720 --> 00:16:07,600
 This guy really did get happy.

271
00:16:07,600 --> 00:16:19,800
 By the end of the day, the rebirth and karma and all these

272
00:16:19,800 --> 00:16:23,760
 things are beyond what's going

273
00:16:23,760 --> 00:16:29,040
 to happen. It's not that important, is it?

274
00:16:29,040 --> 00:16:33,000
 No, karma is incredibly important. Karma is the meaning.

275
00:16:33,000 --> 00:16:38,120
 I mean, no, no, no, no, no. Instead of karma, you can be as

276
00:16:38,120 --> 00:16:40,360
 deep as they are. You can see

277
00:16:40,360 --> 00:16:41,360
 suffering.

278
00:16:41,360 --> 00:16:44,400
 Yes, but you see, what is the problem with suffering? There

279
00:16:44,400 --> 00:16:45,960
 is the cause of suffering.

280
00:16:45,960 --> 00:16:49,240
 The cause of suffering is what? That's karma. The result.

281
00:16:49,240 --> 00:16:53,290
 If you do bad deeds, you feel horrible, right? You don't

282
00:16:53,290 --> 00:16:55,720
 feel bad about it. That's all you

283
00:16:55,720 --> 00:16:56,720
 need.

284
00:16:56,720 --> 00:17:02,940
 Oh, okay. But don't call it karma. You just call it

285
00:17:02,940 --> 00:17:04,920
 observation.

286
00:17:04,920 --> 00:17:05,920
 Oh, Jesus.

287
00:17:05,920 --> 00:17:07,240
 Just call it observation.

288
00:17:07,240 --> 00:17:10,850
 This is the problem. This is the problem. It's not you.

289
00:17:10,850 --> 00:17:12,240
 This is how people think when

290
00:17:12,240 --> 00:17:14,720
 they approach Buddhism. Well, that's all that's important.

291
00:17:14,720 --> 00:17:16,080
 This karma thing, throw that out.

292
00:17:16,080 --> 00:17:19,330
 But that's exactly what you think karma is. It's practical.

293
00:17:19,330 --> 00:17:20,680
 It's not a theory where you

294
00:17:20,680 --> 00:17:24,070
 think, "Wow, yeah, if I kill someone in this life, I'll be

295
00:17:24,070 --> 00:17:25,720
 killed in my next life." All

296
00:17:25,720 --> 00:17:29,200
 that is is an extrapolation. If it's true or not true, it

297
00:17:29,200 --> 00:17:30,720
 makes sense. If you do bad

298
00:17:30,720 --> 00:17:33,460
 things, it hurts you now. But it also has the potential to

299
00:17:33,460 --> 00:17:34,640
 ripple out and hurt you in

300
00:17:34,640 --> 00:17:36,920
 the future. It can hurt you in this life. Maybe the person

301
00:17:36,920 --> 00:17:37,720
 will come back and kill you

302
00:17:37,720 --> 00:17:39,720
 in this life. That makes sense.

303
00:17:39,720 --> 00:17:44,140
 Yes. If we replace things like karma with observation, we

304
00:17:44,140 --> 00:17:46,400
 replace meditating with mind

305
00:17:46,400 --> 00:17:47,400
 training.

306
00:17:47,400 --> 00:17:51,040
 No, that's not because observation is the way it is.

307
00:17:51,040 --> 00:17:53,490
 Karma is the way it is. Observation is seeing how it is.

308
00:17:53,490 --> 00:17:54,560
 Two different things.

309
00:17:54,560 --> 00:17:59,530
 Yeah, I mean, you don't have to know the concept of karma

310
00:17:59,530 --> 00:18:01,080
 just to see it.

311
00:18:01,080 --> 00:18:04,080
 You have to see it.

312
00:18:04,080 --> 00:18:09,310
 Yeah. You see it, but you can see it straight away

313
00:18:09,310 --> 00:18:10,680
 sometimes.

314
00:18:10,680 --> 00:18:11,680
 Exactly.

315
00:18:11,680 --> 00:18:15,560
 Sometimes not. Sometimes you can.

316
00:18:15,560 --> 00:18:19,240
 It's not necessary to see what's going to happen in your

317
00:18:19,240 --> 00:18:20,880
 next life based on what you

318
00:18:20,880 --> 00:18:25,570
 did in this life. But it's just part of the theory. You can

319
00:18:25,570 --> 00:18:27,120
't say exactly, but it makes

320
00:18:27,120 --> 00:18:31,360
 sense that it's the bad effects of doing that.

321
00:18:31,360 --> 00:18:35,800
 Exactly. It makes sense. I don't think that if we regard

322
00:18:35,800 --> 00:18:38,600
 Buddhism as might not be religion,

323
00:18:38,600 --> 00:18:42,460
 that atheists shouldn't be attacking Buddhism. Because

324
00:18:42,460 --> 00:18:44,680
 there's not even one thing that should

325
00:18:44,680 --> 00:18:47,400
 be attacked. Because it's basically reason.

326
00:18:47,400 --> 00:18:50,190
 But rebirth and karma is something they do attack. Many

327
00:18:50,190 --> 00:18:50,840
 people do attack.

328
00:18:50,840 --> 00:18:54,910
 For this reason. They take it from, well, ass backwards. I

329
00:18:54,910 --> 00:18:56,400
 think it's the technical

330
00:18:56,400 --> 00:19:01,450
 term. They start from next life and work their way back,

331
00:19:01,450 --> 00:19:05,000
 which is not how we just explain

332
00:19:05,000 --> 00:19:07,800
 it. It starts from this moment and ripples out.

333
00:19:07,800 --> 00:19:10,160
 What's going to happen in the next life? Well, it's

334
00:19:10,160 --> 00:19:12,480
 conjecture, but it makes sense that it's

335
00:19:12,480 --> 00:19:16,560
 based on, in some part, on what we do in this life.

336
00:19:16,560 --> 00:19:17,560
 Yeah.

337
00:19:17,560 --> 00:19:19,160
 It makes sense.

338
00:19:19,160 --> 00:19:21,280
 Basically.

339
00:19:21,280 --> 00:19:23,080
 I'm not going to give you proof, but it makes sense.

340
00:19:23,080 --> 00:19:23,800
 Please.

