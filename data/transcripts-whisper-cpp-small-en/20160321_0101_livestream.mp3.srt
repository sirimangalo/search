1
00:00:00,000 --> 00:00:18,000
 [silence]

2
00:00:18,000 --> 00:00:21,000
 Good evening, everyone.

3
00:00:21,000 --> 00:00:24,000
 I'm broadcasting live.

4
00:00:24,000 --> 00:00:30,000
 [door opens]

5
00:00:30,000 --> 00:00:35,000
 March 20th.

6
00:00:35,000 --> 00:00:39,000
 I'm going to cut my meditation short this evening.

7
00:00:39,000 --> 00:00:43,000
 It's been a long day, lots of different things.

8
00:00:43,000 --> 00:00:45,000
 I'm going to cut up in the...

9
00:00:45,000 --> 00:00:53,000
 [birds chirping]

10
00:00:53,000 --> 00:01:01,000
 [silence]

11
00:01:01,000 --> 00:01:07,000
 I was doing...

12
00:01:07,000 --> 00:01:14,000
 homework.

13
00:01:14,000 --> 00:01:26,000
 Anyway, here on time.

14
00:01:26,000 --> 00:01:31,000
 So today's quote is...

15
00:01:31,000 --> 00:01:35,000
 I think quite an interesting one.

16
00:01:35,000 --> 00:01:40,000
 Let's see if we can get...

17
00:01:40,000 --> 00:01:55,000
 61.

18
00:01:55,000 --> 00:01:58,000
 61.

19
00:01:58,000 --> 00:02:05,000
 71.

20
00:02:05,000 --> 00:02:25,720
 [

21
00:02:25,720 --> 00:02:31,720
 62.

22
00:02:31,720 --> 00:02:36,720
 63.

23
00:02:36,720 --> 00:02:41,720
 65.

24
00:02:41,720 --> 00:02:47,720
 62.

25
00:02:47,720 --> 00:02:52,720
 63.

26
00:02:52,720 --> 00:02:57,720
 64.

27
00:02:57,720 --> 00:03:03,720
 62.

28
00:03:03,720 --> 00:03:08,720
 63.

29
00:03:08,720 --> 00:03:13,720
 63.

30
00:03:13,720 --> 00:03:19,720
 64.

31
00:03:19,720 --> 00:03:25,720
 64.

32
00:03:25,720 --> 00:03:30,720
 65.

33
00:03:30,720 --> 00:03:35,720
 61.

34
00:03:35,720 --> 00:03:41,720
 62.

35
00:03:41,720 --> 00:03:47,720
 63.

36
00:03:47,720 --> 00:03:53,720
 63.

37
00:03:53,720 --> 00:03:59,720
 63.

38
00:03:59,720 --> 00:04:05,720
 63.

39
00:04:05,720 --> 00:04:11,720
 63.

40
00:04:11,720 --> 00:04:16,720
 63.

41
00:04:16,720 --> 00:04:21,720
 63.

42
00:04:21,720 --> 00:04:27,720
 63.

43
00:04:27,720 --> 00:04:33,720
 63.

44
00:04:33,720 --> 00:04:38,720
 63.

45
00:04:38,720 --> 00:04:43,720
 63.

46
00:04:43,720 --> 00:04:49,720
 63.

47
00:04:49,720 --> 00:04:55,720
 64.

48
00:04:55,720 --> 00:04:59,720
 You see, so science...

49
00:04:59,720 --> 00:05:05,720
 and say ordinary people...

50
00:05:05,720 --> 00:05:08,720
 have this idea of a substratum.

51
00:05:08,720 --> 00:05:12,720
 Most religions as well.

52
00:05:12,720 --> 00:05:16,720
 So science is quite convinced...

53
00:05:16,720 --> 00:05:20,720
 that there is a substratum of reality.

54
00:05:20,720 --> 00:05:24,520
 Religion is usually quite convinced that there's a substr

55
00:05:24,520 --> 00:05:24,720
atum of mentality.

56
00:05:24,720 --> 00:05:35,720
 The soul, the self, God.

57
00:05:35,720 --> 00:05:44,720
 And so there's a lot of postulation of various substrata.

58
00:05:44,720 --> 00:05:52,720
 And so science cultivates substrata that tend to...

59
00:05:52,720 --> 00:05:55,720
 tend to accord with reality.

60
00:05:55,720 --> 00:06:00,720
 Religion often not.

61
00:06:00,720 --> 00:06:03,720
 But religion also does a better job...

62
00:06:03,720 --> 00:06:11,720
 according with mental reality sometimes.

63
00:06:11,720 --> 00:06:14,720
 Than science.

64
00:06:14,720 --> 00:06:17,720
 Nonetheless, the tendency is to...

65
00:06:17,720 --> 00:06:20,720
 postulate substrata.

66
00:06:20,720 --> 00:06:24,720
 Substrata means like a framework.

67
00:06:24,720 --> 00:06:27,720
 Like a mind, a body.

68
00:06:27,720 --> 00:06:34,970
 A three-dimensional, four-dimensional reality that we live

69
00:06:34,970 --> 00:06:35,720
 in.

70
00:06:35,720 --> 00:06:41,720
 Buddhism doesn't do that.

71
00:06:41,720 --> 00:06:45,720
 Buddhism has a very simple take on the universe that...

72
00:06:45,720 --> 00:06:48,720
 this moment is real.

73
00:06:48,720 --> 00:06:51,720
 This moment is an experience.

74
00:06:51,720 --> 00:06:53,720
 And then it ceases.

75
00:06:53,720 --> 00:06:57,720
 And then this moment is real.

76
00:06:57,720 --> 00:06:59,720
 And that moment...

77
00:06:59,720 --> 00:07:03,720
 so when we talk about mind in Theravada Buddhism...

78
00:07:03,720 --> 00:07:06,720
 we mean that moment of mind.

79
00:07:06,720 --> 00:07:08,720
 One moment of mind.

80
00:07:08,720 --> 00:07:11,720
 One moment of experience.

81
00:07:11,720 --> 00:07:15,720
 One moment of awareness or...

82
00:07:15,720 --> 00:07:18,720
 contemplation of an object.

83
00:07:18,720 --> 00:07:21,720
 Observation of an object.

84
00:07:21,720 --> 00:07:27,720
 One moment of experience.

85
00:07:27,720 --> 00:07:31,720
 And so if you ask the question, who's right?

86
00:07:31,720 --> 00:07:35,720
 It's not really the appropriate question.

87
00:07:35,720 --> 00:07:39,520
 Scientists tend to scoff at Buddhism as being overly simple

88
00:07:39,520 --> 00:07:42,720
, simplistic.

89
00:07:42,720 --> 00:07:47,780
 They'll say, "Okay, well, maybe you're right, but you haven

90
00:07:47,780 --> 00:07:50,720
't said anything about reality."

91
00:07:50,720 --> 00:07:56,720
 "You haven't figured out subatomic particles."

92
00:07:56,720 --> 00:08:00,720
 "How to build an atom bomb."

93
00:08:00,720 --> 00:08:07,720
 "Or have you explored the vast reaches of the universe?"

94
00:08:07,720 --> 00:08:11,720
 "The solar system and the galaxy."

95
00:08:11,720 --> 00:08:13,720
 "The known universe."

96
00:08:13,720 --> 00:08:17,720
 "You haven't really explored reality."

97
00:08:17,720 --> 00:08:26,720
 We can say, "No, no, we haven't."

98
00:08:26,720 --> 00:08:30,720
 Or the question is, "To what end?"

99
00:08:30,720 --> 00:08:35,720
 The question of all of this is, "To what end?"

100
00:08:35,720 --> 00:08:40,720
 Religion makes all sorts of claims about reality.

101
00:08:40,720 --> 00:08:44,720
 We can ask, "What is the result?"

102
00:08:44,720 --> 00:08:52,720
 If it were useful and positive to delude yourself

103
00:08:52,720 --> 00:08:59,560
 and to make claims about reality that were not in accord

104
00:08:59,560 --> 00:09:00,720
 with reality,

105
00:09:00,720 --> 00:09:03,720
 that had no evidence, that were not based on evidence,

106
00:09:03,720 --> 00:09:11,890
 if that were somehow beneficial, then there wouldn't be

107
00:09:11,890 --> 00:09:12,720
 much of a problem.

108
00:09:12,720 --> 00:09:17,360
 Most religions would do fine, but because they tend to not

109
00:09:17,360 --> 00:09:21,720
 relate to actual experience in reality,

110
00:09:21,720 --> 00:09:26,720
 they tend to rely more on belief.

111
00:09:26,720 --> 00:09:35,720
 They tend to be quite artificial and stilted or forced.

112
00:09:35,720 --> 00:09:40,310
 If you step back, or say you step out of a meditation

113
00:09:40,310 --> 00:09:40,720
 course

114
00:09:40,720 --> 00:09:46,380
 where you've been studying your own mind and what's really

115
00:09:46,380 --> 00:09:47,720
 here and now,

116
00:09:47,720 --> 00:09:50,720
 and then you contemplate one of these religions,

117
00:09:50,720 --> 00:09:55,350
 any religion at random, and you contemplate what they're

118
00:09:55,350 --> 00:09:56,720
 actually saying.

119
00:09:56,720 --> 00:10:03,720
 It's shocking how absurd it all is.

120
00:10:03,720 --> 00:10:11,720
 What are you talking about? What is this garbage?

121
00:10:11,720 --> 00:10:21,720
 Really? So much belief. There was this guy, he said this,

122
00:10:21,720 --> 00:10:25,720
 he claimed this, he did this,

123
00:10:25,720 --> 00:10:29,720
 and he will save us.

124
00:10:29,720 --> 00:10:35,350
 Or there's this God up there, and all you have to do is

125
00:10:35,350 --> 00:10:36,720
 pray or repeat his name,

126
00:10:36,720 --> 00:10:44,720
 or chant this or chant that, and he will save you.

127
00:10:44,720 --> 00:10:50,720
 So they run into lots of problems, and I think wise people

128
00:10:50,720 --> 00:10:51,720
 tend to stay away

129
00:10:51,720 --> 00:10:57,370
 from most of the religions out there, because they tend to

130
00:10:57,370 --> 00:10:58,720
 be not very much,

131
00:10:58,720 --> 00:11:02,720
 not very closely aligned to reality.

132
00:11:02,720 --> 00:11:07,720
 But science, science does a good job focusing on reality,

133
00:11:07,720 --> 00:11:09,720
 aligning itself with experience.

134
00:11:09,720 --> 00:11:13,930
 They are able to run experiments that actually accord with

135
00:11:13,930 --> 00:11:14,720
 the way things are,

136
00:11:14,720 --> 00:11:18,720
 but they've got a different problem.

137
00:11:18,720 --> 00:11:24,720
 And we can argue about what is more real in the substratum

138
00:11:24,720 --> 00:11:26,720
 of physical,

139
00:11:26,720 --> 00:11:31,360
 and even maybe mental reality, physical reality, or

140
00:11:31,360 --> 00:11:32,720
 experience.

141
00:11:32,720 --> 00:11:36,290
 Which one is more real? We can argue about that, but it's

142
00:11:36,290 --> 00:11:37,720
 not an important argument again.

143
00:11:37,720 --> 00:11:46,210
 To what end? What has science given us? What has it brought

144
00:11:46,210 --> 00:11:46,720
 us?

145
00:11:46,720 --> 00:11:50,720
 It's brought us all sorts of magical, wonderful things,

146
00:11:50,720 --> 00:11:52,720
 like the internet.

147
00:11:52,720 --> 00:11:56,970
 The fact that I'm able to talk to people all around the

148
00:11:56,970 --> 00:11:59,720
 world, kind of magical.

149
00:11:59,720 --> 00:12:03,630
 It's brought us all this, very powerful, this kind of

150
00:12:03,630 --> 00:12:04,720
 knowledge.

151
00:12:04,720 --> 00:12:07,720
 But has it brought us happiness? Has it brought us peace?

152
00:12:07,720 --> 00:12:13,720
 Has it brought us understanding?

153
00:12:13,720 --> 00:12:16,660
 And I mean a specific, I'm thinking of a specific type of

154
00:12:16,660 --> 00:12:18,720
 understanding, really.

155
00:12:18,720 --> 00:12:25,720
 Has it brought us to understanding of ourselves? No.

156
00:12:25,720 --> 00:12:29,940
 No, the people who study science get angry, become addicted

157
00:12:29,940 --> 00:12:30,720
 to things,

158
00:12:30,720 --> 00:12:35,110
 they have a hard time dealing with ordinary experiences,

159
00:12:35,110 --> 00:12:38,720
 and so they suffer.

160
00:12:38,720 --> 00:12:46,050
 They stress, they kill themselves, they drown their, soak

161
00:12:46,050 --> 00:12:47,720
 their brains in alcohol,

162
00:12:47,720 --> 00:12:54,720
 or drugs, because they can't deal with ordinary reality.

163
00:12:54,720 --> 00:13:04,720
 They study reality so much, but can't deal with it.

164
00:13:04,720 --> 00:13:10,020
 And then you have this Buddhist, I'd say, a meditator who

165
00:13:10,020 --> 00:13:11,720
 focuses on the reality,

166
00:13:11,720 --> 00:13:18,720
 who looks at their experience.

167
00:13:18,720 --> 00:13:21,720
 When they have emotions, they see them as emotions, when

168
00:13:21,720 --> 00:13:21,720
 they have pain,

169
00:13:21,720 --> 00:13:27,470
 they see it as pain, and the experiences of seeing or

170
00:13:27,470 --> 00:13:28,720
 hearing,

171
00:13:28,720 --> 00:13:30,720
 they see them for what they are.

172
00:13:30,720 --> 00:13:35,720
 They put aside any theories or thoughts about a substratum,

173
00:13:35,720 --> 00:13:37,720
 about reality.

174
00:13:37,720 --> 00:13:42,310
 They go, "Here, I'm in this room." They even lose track of

175
00:13:42,310 --> 00:13:43,720
 being in a room.

176
00:13:43,720 --> 00:13:46,940
 They just see the mind arising, and they see seeing the

177
00:13:46,940 --> 00:13:47,720
 mind arises,

178
00:13:47,720 --> 00:13:51,780
 is aware of something, and sees this, and immediately there

179
00:13:51,780 --> 00:13:52,720
's another mind.

180
00:13:52,720 --> 00:13:56,720
 But it's totally different.

181
00:13:56,720 --> 00:14:01,720
 It's a whole new mind, a whole new experience.

182
00:14:01,720 --> 00:14:04,720
 And they see this, too.

183
00:14:04,720 --> 00:14:09,660
 They're able to see a reality, or a way of looking at

184
00:14:09,660 --> 00:14:15,720
 reality, for themselves.

185
00:14:15,720 --> 00:14:19,720
 And that leads to objectivity.

186
00:14:19,720 --> 00:14:24,400
 They're able to fully comprehend reality, I guess you could

187
00:14:24,400 --> 00:14:24,720
 say.

188
00:14:24,720 --> 00:14:27,690
 Because science, you can study it, but you can't fully

189
00:14:27,690 --> 00:14:28,720
 comprehend it.

190
00:14:28,720 --> 00:14:33,790
 You can't look at the wall and comprehend that that wall is

191
00:14:33,790 --> 00:14:36,720
 made up of mostly empty space.

192
00:14:36,720 --> 00:14:42,650
 To give an example, moreover, you're not able to comprehend

193
00:14:42,650 --> 00:14:44,720
 all those things.

194
00:14:44,720 --> 00:14:47,280
 I guess I would say it's not even worth it, it wouldn't

195
00:14:47,280 --> 00:14:48,720
 even be worth it if we could.

196
00:14:48,720 --> 00:14:52,980
 And in fact, our comprehension of things is part of the

197
00:14:52,980 --> 00:14:53,720
 problem.

198
00:14:53,720 --> 00:14:56,720
 Because when you think about concepts,

199
00:14:56,720 --> 00:14:59,010
 when you think about entities, you have to abstract

200
00:14:59,010 --> 00:14:59,720
 something.

201
00:14:59,720 --> 00:15:03,720
 You have this mouse.

202
00:15:03,720 --> 00:15:06,720
 What happens when I think of this mouse?

203
00:15:06,720 --> 00:15:11,720
 There's something going on in my mind.

204
00:15:11,720 --> 00:15:15,650
 There's an experience, and there's an experience which says

205
00:15:15,650 --> 00:15:17,720
 there is a mouse in your hand.

206
00:15:17,720 --> 00:15:22,350
 But it has nothing to do directly with the experience of

207
00:15:22,350 --> 00:15:23,720
 seeing the mouse.

208
00:15:23,720 --> 00:15:27,720
 The seeing and the feeling that created it.

209
00:15:27,720 --> 00:15:30,720
 It's all in the mind at that point.

210
00:15:30,720 --> 00:15:33,720
 There are minds arising and seething,

211
00:15:33,720 --> 00:15:39,720
 leading therefore, therefore, therefore, one to the other.

212
00:15:39,720 --> 00:15:41,720
 And I'm not aware of that.

213
00:15:41,720 --> 00:15:46,720
 So there's a reality that is being ignored.

214
00:15:46,720 --> 00:15:50,720
 Because instead I'm focused on the idea of the mouse.

215
00:15:50,720 --> 00:15:53,720
 There's something that I'm missing.

216
00:15:53,720 --> 00:15:56,720
 And as a result, when likes and dislikes come up,

217
00:15:56,720 --> 00:16:00,720
 I miss them as well. So I like the mouse, or I dislike it.

218
00:16:00,720 --> 00:16:06,110
 And that's what leads to frustration and addiction and so

219
00:16:06,110 --> 00:16:06,720
 on.

220
00:16:06,720 --> 00:16:11,950
 That's what leads us to attach to things and people and

221
00:16:11,950 --> 00:16:12,720
 experiences,

222
00:16:12,720 --> 00:16:18,940
 and leads us to get frustrated and upset and bored and

223
00:16:18,940 --> 00:16:19,720
 disappointed

224
00:16:19,720 --> 00:16:25,720
 when we don't get what we want.

225
00:16:25,720 --> 00:16:28,540
 So when we pay attention to that, as I'm picking up the

226
00:16:28,540 --> 00:16:28,720
 mouse,

227
00:16:28,720 --> 00:16:33,640
 I pay attention to the experience and the thinking and the

228
00:16:33,640 --> 00:16:34,720
 judging

229
00:16:34,720 --> 00:16:41,060
 that I am in a position to understand and to see the

230
00:16:41,060 --> 00:16:44,720
 experience clearly

231
00:16:44,720 --> 00:16:51,720
 and to not react, or to understand my reactions

232
00:16:51,720 --> 00:16:57,650
 and to see how they're hurting me and to slowly give them

233
00:16:57,650 --> 00:16:58,720
 up.

234
00:16:58,720 --> 00:17:07,370
 This is an important quote to help us understand where our

235
00:17:07,370 --> 00:17:08,720
 focus should be.

236
00:17:08,720 --> 00:17:12,720
 Our focus should be on experiential reality.

237
00:17:12,720 --> 00:17:16,720
 That's what the Buddha talked about.

238
00:17:16,720 --> 00:17:19,720
 So here what do we have? This whole sutta is interesting.

239
00:17:19,720 --> 00:17:25,720
 The uninstructed world thing, the putudjana, right?

240
00:17:25,720 --> 00:17:35,290
 The sutva, the sutva bhikkave putudjana, the uninstructed

241
00:17:35,290 --> 00:17:36,720
 putudjana,

242
00:17:36,720 --> 00:17:39,720
 might experience revulsion towards this body.

243
00:17:39,720 --> 00:17:44,720
 He might become dispassionate towards it and be liberated.

244
00:17:44,720 --> 00:17:48,840
 He might even be liberated because growth and decline is

245
00:17:48,840 --> 00:17:49,720
 seen in this body

246
00:17:49,720 --> 00:17:59,720
 and is seen and taken up.

247
00:17:59,720 --> 00:18:03,720
 Okay, he's saying he might be liberated from the body.

248
00:18:03,720 --> 00:18:07,910
 The body is revolting. It's easier to see it's revolting as

249
00:18:07,910 --> 00:18:08,720
 you get old

250
00:18:08,720 --> 00:18:10,720
 and as you get sick and so on.

251
00:18:10,720 --> 00:18:14,720
 It's possible for ordinary people to let go of it.

252
00:18:14,720 --> 00:18:19,720
 But the mind, the mind we are unable to let go of,

253
00:18:19,720 --> 00:18:23,750
 he says the ordinary uninstructed world thing is unable to

254
00:18:23,750 --> 00:18:24,720
 experience

255
00:18:24,720 --> 00:18:27,200
 revulsion towards it, unable to become dispassionate

256
00:18:27,200 --> 00:18:27,720
 towards it

257
00:18:27,720 --> 00:18:30,720
 and be liberated from it.

258
00:18:30,720 --> 00:18:33,720
 Because for a long time this has been held by him,

259
00:18:33,720 --> 00:18:39,470
 appropriate and grasped us. This is mine, this I am, this

260
00:18:39,470 --> 00:18:39,720
 is my self.

261
00:18:39,720 --> 00:18:48,870
 Ṭīgāratan heitang bhikkave sutva, sutva to putudjana sa

262
00:18:48,870 --> 00:18:53,720
 ajja o sitang mamāiitang parāmatam

263
00:18:53,720 --> 00:18:57,720
 eitang mamā eso hamasuni eso meyata.

264
00:18:57,720 --> 00:19:02,720
 This is my self, this is mine, this I am.

265
00:19:02,720 --> 00:19:05,470
 This is what we think of the mind, right? I mean, who doesn

266
00:19:05,470 --> 00:19:08,720
't think the mind is me and mine?

267
00:19:08,720 --> 00:19:11,720
 The body we cannot go.

268
00:19:11,720 --> 00:19:18,720
 When the mind is pleased or displaced, that's me, that's I.

269
00:19:25,720 --> 00:19:34,080
 Ṭāsmā tatra sutva putudjana nālāng nimbidhi tū nāl

270
00:19:34,080 --> 00:19:36,720
āng wirajitū nālāng mudhi tū

271
00:19:36,720 --> 00:19:42,720
 I was using interesting words.

272
00:19:42,720 --> 00:19:45,720
 I guess revulsion isn't really the right word.

273
00:19:45,720 --> 00:19:51,470
 Nimbidha is not revulsion, it becomes disenchanted, becomes

274
00:19:51,470 --> 00:19:55,720
 dispassionate, becomes liberated. That's probably there.

275
00:19:55,720 --> 00:20:02,940
 But it would be better to take, this is an interesting,

276
00:20:02,940 --> 00:20:05,720
 this is a famous one actually because

277
00:20:05,720 --> 00:20:09,280
 it would be better for the un-instructed worldling to take

278
00:20:09,280 --> 00:20:10,720
 the self as the body,

279
00:20:10,720 --> 00:20:17,850
 take the body as self because the body is seen standing for

280
00:20:17,850 --> 00:20:18,720
 one year, for two years,

281
00:20:18,720 --> 00:20:21,610
 for three, four, five, or ten years, for twenty, thirty,

282
00:20:21,610 --> 00:20:25,720
 forty, fifty years, for a hundred years, or even longer.

283
00:20:25,720 --> 00:20:30,200
 But the mind and mentality and consciousness arises as one

284
00:20:30,200 --> 00:20:33,720
 thing, and this is the quote, and ceases as another.

285
00:20:33,720 --> 00:20:40,720
 The mind is flighty, the mind arises and ceases quickly.

286
00:20:40,720 --> 00:20:48,350
 And this is really what I'm talking about, is the body

287
00:20:48,350 --> 00:20:49,720
 appears to be a thing, right?

288
00:20:49,720 --> 00:20:52,980
 If you think about the body, it's understandable that you

289
00:20:52,980 --> 00:20:56,720
 would cling to it, but how can you cling to the mind?

290
00:20:56,720 --> 00:21:00,350
 This is a different, the Buddha is exposing a different way

291
00:21:00,350 --> 00:21:03,720
 of looking at reality from the point of view of the mind.

292
00:21:03,720 --> 00:21:07,270
 Because from the point of view of the mind, even the body

293
00:21:07,270 --> 00:21:08,720
 arises and ceases.

294
00:21:08,720 --> 00:21:13,720
 And then he says,

295
00:21:13,720 --> 00:21:19,670
 "Tatrabhika vesutva, a learned Aryasa avakum, disciple of

296
00:21:19,670 --> 00:21:28,070
 the noble one, paticasumupadam ye wasadukanyon isumanasikar

297
00:21:28,070 --> 00:21:28,720
uti."

298
00:21:28,720 --> 00:21:41,830
 Well, with wisdom, considers paticasumupadam, the dependent

299
00:21:41,830 --> 00:21:42,720
 origination,

300
00:21:42,720 --> 00:21:47,720
 "ithi imasming sati idang hoti."

301
00:21:47,720 --> 00:21:58,140
 And thus, you see, when this exists, that comes to be, the

302
00:21:58,140 --> 00:22:01,720
 arising of this, that arises.

303
00:22:01,720 --> 00:22:09,720
 "Ithi imasming sati idang hoti imasupada idang upajatih."

304
00:22:09,720 --> 00:22:13,720
 Oh yeah, okay. When this arises, there is that.

305
00:22:13,720 --> 00:22:18,720
 With the arising of this, that arises.

306
00:22:18,720 --> 00:22:24,860
 "Imasming asati" was the non-existence of this, "Ithang nah

307
00:22:24,860 --> 00:22:26,720
oti" there is not that.

308
00:22:26,720 --> 00:22:32,200
 "Imasaniloda imang idang nirajati" with the cessation of

309
00:22:32,200 --> 00:22:34,720
 this, and that ceases.

310
00:22:37,720 --> 00:22:43,260
 "Yadidang" that is to say, "awijapatjya sankhara sankhara

311
00:22:43,260 --> 00:22:45,720
 patjiyanyanang" and so on.

312
00:22:45,720 --> 00:22:48,720
 This is a paticasumupada dependent origination.

313
00:22:48,720 --> 00:22:51,700
 And so what he's saying here is, this is how experiential

314
00:22:51,700 --> 00:22:52,720
 reality works.

315
00:22:52,720 --> 00:22:55,720
 You study in terms of cause and effect.

316
00:22:55,720 --> 00:22:59,920
 There is ignorance. Because we're ignorant, we give rise to

317
00:22:59,920 --> 00:23:01,720
 our partialities.

318
00:23:01,720 --> 00:23:06,440
 We like some things, we dislike, we chase after some things

319
00:23:06,440 --> 00:23:07,720
, we chase away some things.

320
00:23:07,720 --> 00:23:10,680
 Because we don't see things clearly. As a result, we're

321
00:23:10,680 --> 00:23:11,720
 born again and again.

322
00:23:11,720 --> 00:23:16,720
 And we evaluate leads to suffering.

323
00:23:16,720 --> 00:23:19,720
 I won't go into detail with that.

324
00:23:19,720 --> 00:23:23,210
 The important point is that we look at reality from the

325
00:23:23,210 --> 00:23:24,720
 point of view of our experiences.

326
00:23:24,720 --> 00:23:27,120
 If you do that, there's really nothing, you become

327
00:23:27,120 --> 00:23:27,720
 invincible.

328
00:23:27,720 --> 00:23:32,720
 There's nothing that can gain power over you.

329
00:23:32,720 --> 00:23:37,270
 Whether it be something pleasant that you want to, there's

330
00:23:37,270 --> 00:23:38,720
 no you, there's no thing.

331
00:23:38,720 --> 00:23:42,720
 If you see it as an experience, it arises.

332
00:23:42,720 --> 00:23:47,720
 If there's something that upsets you or frustrates you,

333
00:23:47,720 --> 00:23:50,720
 that thing is just an experience.

334
00:23:50,720 --> 00:23:53,720
 You see it arising, you see the experience.

335
00:23:54,720 --> 00:23:58,720
 What does it mean? It's just an experience.

336
00:23:58,720 --> 00:24:03,810
 You see the frustration, you see the upset arise with vin

337
00:24:03,810 --> 00:24:04,720
ceses.

338
00:24:04,720 --> 00:24:10,720
 You stop hiding, you stop running,

339
00:24:10,720 --> 00:24:16,600
 stop living in conceptual reality of things and people and

340
00:24:16,600 --> 00:24:17,720
 places.

341
00:24:17,720 --> 00:24:22,370
 Start living in reality, true reality, just your experience

342
00:24:22,370 --> 00:24:22,720
.

343
00:24:22,720 --> 00:24:30,710
 Start seeing what's going on underneath that chrome plating

344
00:24:30,710 --> 00:24:31,720
 of beautiful things

345
00:24:31,720 --> 00:24:37,720
 and desirable things and ugly things and scary things.

346
00:24:37,720 --> 00:24:42,720
 Underneath it's all just experience.

347
00:24:42,720 --> 00:24:48,720
 Anyway, there's the quote for this evening.

348
00:24:48,720 --> 00:24:52,720
 That's the dhamma for this evening.

349
00:24:52,720 --> 00:24:58,720
 Now, as usual, I'm going to put the link to the Hangout app

350
00:24:58,720 --> 00:24:58,720
.

351
00:24:58,720 --> 00:25:03,510
 If you would like to join and ask a question, you're

352
00:25:03,510 --> 00:25:06,720
 welcome to click the link.

353
00:25:06,720 --> 00:25:09,990
 Only if you have a question and only if you're in a

354
00:25:09,990 --> 00:25:11,720
 respectful, like no coming on naked

355
00:25:11,720 --> 00:25:18,720
 or smoking or drinking alcohol or eating food.

356
00:25:18,720 --> 00:25:24,720
 You should consider this to be a dhamma hangout.

357
00:25:24,720 --> 00:25:34,720
 Scare everybody away.

358
00:25:34,720 --> 00:25:46,720
 [silence]

359
00:25:46,720 --> 00:25:49,720
 You can go the left side too.

360
00:25:49,720 --> 00:26:07,720
 [silence]

361
00:26:07,720 --> 00:26:31,720
 [silence]

362
00:26:31,720 --> 00:26:41,720
 [silence]

363
00:26:41,720 --> 00:26:47,510
 Alright, nobody with burning questions is sleeping to join

364
00:26:47,510 --> 00:26:48,720
 the hangout.

365
00:26:48,720 --> 00:27:02,720
 And I think I'll say goodnight. See you all tomorrow.

366
00:27:04,720 --> 00:27:14,720
 [silence]

