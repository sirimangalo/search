1
00:00:00,000 --> 00:00:07,000
 Okay, so here I am broadcasting in Chiang Mai.

2
00:00:07,000 --> 00:00:14,000
 You'll notice that the audio quality won't be as good

3
00:00:14,000 --> 00:00:16,000
 because I don't have my external

4
00:00:16,000 --> 00:00:17,000
 mic.

5
00:00:17,000 --> 00:00:25,000
 But here I am sitting in Chiang Mai city visiting some

6
00:00:25,000 --> 00:00:29,000
 supporters who offer me food in the morning.

7
00:00:29,000 --> 00:00:33,000
 People who I've known for many years.

8
00:00:33,000 --> 00:00:46,330
 Back when I was teaching in Jantang, I learned Thai to be

9
00:00:46,330 --> 00:00:49,000
 able to understand my teacher,

10
00:00:49,000 --> 00:00:52,000
 Ajahn Tang.

11
00:00:52,000 --> 00:01:00,000
 I realized there was something missing, you know.

12
00:01:00,000 --> 00:01:02,440
 I had been taught how to practice meditation, but there

13
00:01:02,440 --> 00:01:05,000
 wasn't a lot of instruction to

14
00:01:05,000 --> 00:01:10,000
 the Buddha's teaching and to the Dhamma.

15
00:01:10,000 --> 00:01:14,130
 So I would go to chanting in the morning and I saw that Aj

16
00:01:14,130 --> 00:01:16,000
ahn Tang was giving talks every

17
00:01:16,000 --> 00:01:20,000
 morning, every morning and almost every evening.

18
00:01:20,000 --> 00:01:28,400
 And so it seemed, it felt like a real loss that I wasn't

19
00:01:28,400 --> 00:01:32,000
 able to understand.

20
00:01:32,000 --> 00:01:37,170
 And we'd go every week to these talks and the talks were in

21
00:01:37,170 --> 00:01:38,000
 Thai.

22
00:01:38,000 --> 00:01:44,240
 We didn't have a clue what they were saying by some scholar

23
00:01:44,240 --> 00:01:47,000
 monk in the monastery.

24
00:01:47,000 --> 00:01:53,340
 So I learned Thai slowly, arduously, on my own with diction

25
00:01:53,340 --> 00:01:55,000
aries and this really good

26
00:01:55,000 --> 00:01:56,000
 guide.

27
00:01:56,000 --> 00:02:04,710
 There's a really good learning, the standard how to speak

28
00:02:04,710 --> 00:02:08,000
 Thai or something.

29
00:02:08,000 --> 00:02:11,550
 So I learned Thai and as soon as Ajahn Tang realized I

30
00:02:11,550 --> 00:02:15,000
 could speak Thai, he started sending

31
00:02:15,000 --> 00:02:17,000
 meditators my way.

32
00:02:17,000 --> 00:02:20,490
 So I'd been sitting with him, listening to him teach and

33
00:02:20,490 --> 00:02:22,000
 hopping a little bit, but then

34
00:02:22,000 --> 00:02:25,360
 he realized I could speak and so he would send meditators

35
00:02:25,360 --> 00:02:26,000
 my way.

36
00:02:26,000 --> 00:02:29,000
 So I started teaching Thai meditators.

37
00:02:29,000 --> 00:02:36,960
 And one day a few people came and I got tasked with

38
00:02:36,960 --> 00:02:40,000
 teaching them.

39
00:02:40,000 --> 00:02:44,330
 And one of them was the owner of this quite large sort of,

40
00:02:44,330 --> 00:02:46,000
 I guess you'd almost call it

41
00:02:46,000 --> 00:02:49,780
 a mansion, it's a very big house and they own a shipping

42
00:02:49,780 --> 00:02:51,000
 company here.

43
00:02:51,000 --> 00:02:54,560
 And it was funny because she was thinking, "Oh, I like to

44
00:02:54,560 --> 00:02:56,000
 do good deeds but I've never

45
00:02:56,000 --> 00:02:58,000
 really been into meditation."

46
00:02:58,000 --> 00:03:00,000
 Meditation is just not my thing.

47
00:03:00,000 --> 00:03:06,000
 She turns out she'd gotten drug along by her relatives.

48
00:03:06,000 --> 00:03:09,410
 And it's true to this day she really still doesn't do much

49
00:03:09,410 --> 00:03:11,000
 in the way of meditation,

50
00:03:11,000 --> 00:03:17,160
 but she likes to do good deeds in terms of giving gifts and

51
00:03:17,160 --> 00:03:20,000
 helping supporting good causes

52
00:03:20,000 --> 00:03:24,000
 and so on.

53
00:03:24,000 --> 00:03:27,000
 So anyway, that's where I am right now.

54
00:03:27,000 --> 00:03:32,000
 I'm at their house.

55
00:03:32,000 --> 00:03:39,000
 And it's a bit loud.

56
00:03:39,000 --> 00:03:44,000
 So I was thinking this morning about our meditation group

57
00:03:44,000 --> 00:03:47,000
 and our meditation practice and just

58
00:03:47,000 --> 00:03:53,000
 something that is worth reiterating.

59
00:03:53,000 --> 00:03:59,440
 I got a, just in the past day, yesterday I think, got a

60
00:03:59,440 --> 00:04:04,000
 message from someone who was

61
00:04:04,000 --> 00:04:08,640
 listening to my, trying to follow the things that I teach

62
00:04:08,640 --> 00:04:12,000
 but felt it difficult to really

63
00:04:12,000 --> 00:04:14,000
 manage.

64
00:04:14,000 --> 00:04:17,200
 And you know the problem is a lot of when you give talks

65
00:04:17,200 --> 00:04:19,000
 like this, there's sort of an

66
00:04:19,000 --> 00:04:22,000
 expectation of basic knowledge.

67
00:04:22,000 --> 00:04:25,000
 I can't keep repeating the basics.

68
00:04:25,000 --> 00:04:27,380
 So there's an assumption that you already know a lot of

69
00:04:27,380 --> 00:04:29,000
 things that actually a lot of

70
00:04:29,000 --> 00:04:33,000
 people listening and watching my YouTube videos don't know.

71
00:04:33,000 --> 00:04:36,530
 So probably the majority of people, for example, the

72
00:04:36,530 --> 00:04:39,000
 majority of people who watch my YouTube

73
00:04:39,000 --> 00:04:43,000
 videos, I've never read my booklet on how to meditate.

74
00:04:43,000 --> 00:04:46,000
 And that's really the wrong order.

75
00:04:46,000 --> 00:04:49,000
 You should have read the booklet first.

76
00:04:49,000 --> 00:04:53,000
 But unfortunately it doesn't happen that way.

77
00:04:53,000 --> 00:04:55,730
 Some people have seen the videos on how to meditate and

78
00:04:55,730 --> 00:04:57,000
 that's almost as good as the

79
00:04:57,000 --> 00:04:59,000
 booklet.

80
00:04:59,000 --> 00:05:01,640
 In some ways I guess it's better because it's visual but

81
00:05:01,640 --> 00:05:04,000
 really the booklet is the best

82
00:05:04,000 --> 00:05:10,000
 for theory.

83
00:05:10,000 --> 00:05:12,950
 That's not the point I wanted to reiterate but that is also

84
00:05:12,950 --> 00:05:15,000
 worth reiterating that you

85
00:05:15,000 --> 00:05:17,000
 should start with the basics.

86
00:05:17,000 --> 00:05:22,040
 Something more is that there may be people who once they've

87
00:05:22,040 --> 00:05:24,000
 read the booklet then they

88
00:05:24,000 --> 00:05:26,000
 think that's all there is to the meditation technique.

89
00:05:26,000 --> 00:05:32,140
 And in fact what's written in that booklet is just the tip

90
00:05:32,140 --> 00:05:35,000
 of the iceberg really.

91
00:05:35,000 --> 00:05:37,440
 Even in terms of the technique, well not the tip of the

92
00:05:37,440 --> 00:05:39,000
 iceberg but it's the foundation.

93
00:05:39,000 --> 00:05:43,070
 It's like the cement foundation that we're then going to

94
00:05:43,070 --> 00:05:44,000
 build on.

95
00:05:44,000 --> 00:05:48,000
 But we've got a lot of building to do.

96
00:05:48,000 --> 00:05:55,000
 There's a lot more that has to be learned.

97
00:05:55,000 --> 00:06:01,550
 Just pouring a concrete foundation doesn't give you a house

98
00:06:01,550 --> 00:06:02,000
.

99
00:06:02,000 --> 00:06:05,370
 So in fact that's the teaching we give to meditators on the

100
00:06:05,370 --> 00:06:07,000
 first day that they arrive

101
00:06:07,000 --> 00:06:09,000
 at our center.

102
00:06:09,000 --> 00:06:12,920
 And the day they arrive we will teach them basically what's

103
00:06:12,920 --> 00:06:15,000
 in that book, give or take.

104
00:06:15,000 --> 00:06:19,000
 I suppose there's a bit in there that's more advanced.

105
00:06:19,000 --> 00:06:22,670
 Some of it's actually like lesson six is something we teach

106
00:06:22,670 --> 00:06:25,000
 on the second day for example.

107
00:06:25,000 --> 00:06:28,200
 But the point is it's the kind of thing that you can give

108
00:06:28,200 --> 00:06:35,000
 to everyone and assume that it's

109
00:06:35,000 --> 00:06:37,000
 appropriate across the board.

110
00:06:37,000 --> 00:06:40,000
 The first two days of a meditation course in our tradition

111
00:06:40,000 --> 00:06:46,000
 are pretty much the same for everyone.

112
00:06:46,000 --> 00:06:52,340
 But on the second day, after the second day, things start

113
00:06:52,340 --> 00:07:00,000
 to individualize, start to differentiate.

114
00:07:00,000 --> 00:07:06,000
 So this is why our courses are done one on one.

115
00:07:06,000 --> 00:07:09,000
 We don't do group courses.

116
00:07:09,000 --> 00:07:13,500
 And it's also why a group course will never match an

117
00:07:13,500 --> 00:07:16,000
 individualized course.

118
00:07:16,000 --> 00:07:19,960
 You know if you have these courses for whatever seven days

119
00:07:19,960 --> 00:07:27,000
 or even longer where you sit in

120
00:07:27,000 --> 00:07:31,770
 a group, you listen to talks, maybe you have a period to

121
00:07:31,770 --> 00:07:34,000
 ask questions, but you're all

122
00:07:34,000 --> 00:07:36,000
 given the same practice.

123
00:07:36,000 --> 00:07:43,470
 This can never equal an individualized course because

124
00:07:43,470 --> 00:07:48,000
 everyone learns at different speeds

125
00:07:48,000 --> 00:07:50,000
 and even in different ways.

126
00:07:50,000 --> 00:07:53,910
 So what you learn quite quickly as a teacher is you've got

127
00:07:53,910 --> 00:07:55,000
 to be flexible and you've got

128
00:07:55,000 --> 00:07:57,000
 to be quick on your feet.

129
00:07:57,000 --> 00:08:02,270
 You've got to be mindful, really, because you'll encounter

130
00:08:02,270 --> 00:08:07,000
 ten people with ten different character

131
00:08:07,000 --> 00:08:08,000
 types.

132
00:08:08,000 --> 00:08:10,000
 One person is skeptical.

133
00:08:10,000 --> 00:08:17,870
 Another person is lost in their intense faith in what they

134
00:08:17,870 --> 00:08:21,000
 already believe.

135
00:08:21,000 --> 00:08:25,000
 Another person is lustful and passionate.

136
00:08:25,000 --> 00:08:28,240
 Another person is lazy and not really practicing and so on

137
00:08:28,240 --> 00:08:29,000
 and so on.

138
00:08:29,000 --> 00:08:38,000
 People are complex and they're complex in different ways.

139
00:08:38,000 --> 00:08:44,300
 So you have to deal with their complexities and ground them

140
00:08:44,300 --> 00:08:48,000
 in the common building blocks.

141
00:08:48,000 --> 00:08:56,210
 Basically what I'm saying is you need more than just that

142
00:08:56,210 --> 00:08:58,000
 booklet.

143
00:08:58,000 --> 00:09:02,680
 And it's always worth reiterating that if people have the

144
00:09:02,680 --> 00:09:05,000
 chance of finding a teacher, finding

145
00:09:05,000 --> 00:09:18,250
 a meditation center, a place where you can do a course is

146
00:09:18,250 --> 00:09:22,000
 invaluable.

147
00:09:22,000 --> 00:09:33,510
 You can't compare daily practice without a teacher to

148
00:09:33,510 --> 00:09:40,000
 actually having a teacher.

149
00:09:40,000 --> 00:09:45,550
 So I was just going to say something, give some sort of

150
00:09:45,550 --> 00:09:48,000
 idea of what this means, why

151
00:09:48,000 --> 00:09:52,540
 this is or where it is you're going, where it is a teacher

152
00:09:52,540 --> 00:09:54,000
 will take you.

153
00:09:54,000 --> 00:09:56,830
 So the first thing we want to establish, and this is what

154
00:09:56,830 --> 00:10:00,000
 we get across with the booklet,

155
00:10:00,000 --> 00:10:05,000
 is that reality is made up of body and mind.

156
00:10:05,000 --> 00:10:09,390
 That's the base right view that we're trying to establish

157
00:10:09,390 --> 00:10:11,000
 in the meditator.

158
00:10:11,000 --> 00:10:14,760
 So we teach them how to meditate and this is where we start

159
00:10:14,760 --> 00:10:16,000
 to differentiate between

160
00:10:16,000 --> 00:10:20,670
 people because we have a sort of a test, a pop quiz for the

161
00:10:20,670 --> 00:10:22,000
 meditators.

162
00:10:22,000 --> 00:10:25,010
 And if they pass the quiz then we know that they understand

163
00:10:25,010 --> 00:10:26,000
 body and mind.

164
00:10:26,000 --> 00:10:28,970
 We're reasonably sure that they don't have wrong views

165
00:10:28,970 --> 00:10:30,000
 about reality.

166
00:10:30,000 --> 00:10:34,840
 But if they can't answer simple questions about the body

167
00:10:34,840 --> 00:10:37,000
 and the mind then we hold them

168
00:10:37,000 --> 00:10:42,280
 back and keep them practicing at the basic level until they

169
00:10:42,280 --> 00:10:44,000
 understand it.

170
00:10:44,000 --> 00:10:50,510
 Once they understand body and mind then we start watching

171
00:10:50,510 --> 00:10:51,000
 them, testing them,

172
00:10:51,000 --> 00:10:54,750
 and we try to straighten out their views so that they

173
00:10:54,750 --> 00:10:57,000
 understand cause and effect.

174
00:10:57,000 --> 00:11:01,000
 So they see how body and mind work together.

175
00:11:01,000 --> 00:11:06,000
 What does this lead to? What is the cause of this?

176
00:11:06,000 --> 00:11:10,000
 What does anger bring? What does greed bring?

177
00:11:10,000 --> 00:11:16,380
 Not on any deep level but just being aware enough, alert

178
00:11:16,380 --> 00:11:18,000
 enough, focused enough

179
00:11:18,000 --> 00:11:20,000
 to be able to see cause and effect.

180
00:11:20,000 --> 00:11:23,010
 It's not like you're going to have this profound

181
00:11:23,010 --> 00:11:25,000
 realization of dependent origination

182
00:11:25,000 --> 00:11:27,970
 but you're going to see, "Oh, this caused this or that

183
00:11:27,970 --> 00:11:29,000
 caused that."

184
00:11:29,000 --> 00:11:33,070
 And that's important because that's the beginning of the

185
00:11:33,070 --> 00:11:35,000
 cultivation of wisdom.

186
00:11:35,000 --> 00:11:38,780
 That's really all you need because if you have that

187
00:11:38,780 --> 00:11:40,000
 capacity to see,

188
00:11:40,000 --> 00:11:43,280
 "This causes this, that causes that," then that's going to

189
00:11:43,280 --> 00:11:44,000
 be the rest of your path.

190
00:11:44,000 --> 00:11:49,000
 Or that at least initiates you on the path to wisdom.

191
00:11:49,000 --> 00:11:52,620
 Because once you understand, "This leads to this," the next

192
00:11:52,620 --> 00:11:53,000
 step is to see,

193
00:11:53,000 --> 00:11:58,000
 "This is bad so let's not cause it."

194
00:11:58,000 --> 00:12:01,280
 And you start to change your habits based on seeing what

195
00:12:01,280 --> 00:12:02,000
 causes what

196
00:12:02,000 --> 00:12:04,000
 and what causes suffering.

197
00:12:04,000 --> 00:12:08,000
 This is why the Four Noble Truths are phrased the way they

198
00:12:08,000 --> 00:12:11,000
 are, suffering and its cause.

199
00:12:11,000 --> 00:12:14,890
 Once you see what causes suffering, then you're able to

200
00:12:14,890 --> 00:12:16,000
 change your habits.

201
00:12:16,000 --> 00:12:19,270
 It's actually through not clearly seeing what causes

202
00:12:19,270 --> 00:12:20,000
 suffering

203
00:12:20,000 --> 00:12:30,000
 that we actively cause suffering for ourselves.

204
00:12:30,000 --> 00:12:38,000
 After that you start to see in detail,

205
00:12:38,000 --> 00:12:41,000
 it's even beyond the idea of cause and effect,

206
00:12:41,000 --> 00:12:47,000
 the nature of all cause and effect.

207
00:12:47,000 --> 00:12:50,940
 All things that are subject to cause and effect, which

208
00:12:50,940 --> 00:12:58,000
 means everything that arises.

209
00:12:58,000 --> 00:13:02,600
 I think everything that arises, whatever arises based on a

210
00:13:02,600 --> 00:13:03,000
 cause,

211
00:13:03,000 --> 00:13:10,000
 is equally empty.

212
00:13:10,000 --> 00:13:15,160
 I guess empty is this catch phrase in Mahayana Buddhism,

213
00:13:15,160 --> 00:13:16,000
 but it is apt

214
00:13:16,000 --> 00:13:18,000
 to sort of sum up this concept.

215
00:13:18,000 --> 00:13:20,530
 It's the concept of the three characteristics of imper

216
00:13:20,530 --> 00:13:23,000
manent suffering and non-self.

217
00:13:23,000 --> 00:13:29,410
 Things that are impermanent are unsatisfying, so we call

218
00:13:29,410 --> 00:13:31,000
 them suffering,

219
00:13:31,000 --> 00:13:34,450
 but it just means that they may cause you pain or they may

220
00:13:34,450 --> 00:13:36,000
 just be unable to bring you happiness

221
00:13:36,000 --> 00:13:39,000
 because they're impermanent.

222
00:13:39,000 --> 00:13:42,000
 And ultimately they're empty.

223
00:13:42,000 --> 00:13:48,000
 They're like trash or rubbish.

224
00:13:48,000 --> 00:13:53,690
 Not exactly, but they're meaningless. Empty is the best

225
00:13:53,690 --> 00:13:55,000
 word because it's not negative,

226
00:13:55,000 --> 00:14:02,100
 it's not pejorative, but it nullifies any value or any

227
00:14:02,100 --> 00:14:05,000
 reason to cling.

228
00:14:05,000 --> 00:14:09,320
 Any idea that you might cling to it as a refuge or as a

229
00:14:09,320 --> 00:14:12,000
 support.

230
00:14:12,000 --> 00:14:20,570
 And that is about, if you want to sum it up in a short, in

231
00:14:20,570 --> 00:14:22,000
 brief,

232
00:14:22,000 --> 00:14:27,000
 that's the rest of the path, the rest of the worldly path,

233
00:14:27,000 --> 00:14:31,790
 until you realize Nibbana, until you realize the Four Noble

234
00:14:31,790 --> 00:14:32,000
 Truths.

235
00:14:32,000 --> 00:14:40,490
 And that is this process of observing again and again and

236
00:14:40,490 --> 00:14:46,000
 again until you see

237
00:14:46,000 --> 00:14:49,360
 that it's not just clinging to certain things that causes

238
00:14:49,360 --> 00:14:50,000
 suffering,

239
00:14:50,000 --> 00:14:55,000
 it's clinging to anything that causes suffering.

240
00:14:55,000 --> 00:14:58,280
 So in the beginning it seems like certain things cause

241
00:14:58,280 --> 00:14:59,000
 suffering,

242
00:14:59,000 --> 00:15:02,720
 and anger and delusion, you can see how your strong

243
00:15:02,720 --> 00:15:04,000
 emotions are causing you suffering,

244
00:15:04,000 --> 00:15:08,000
 but you see deeper than that as you progress on the path.

245
00:15:08,000 --> 00:15:12,000
 You see, it's not just that, it's everything.

246
00:15:12,000 --> 00:15:17,530
 Everything is, you can't cling to anything, you can't rely

247
00:15:17,530 --> 00:15:20,000
 on anything as a support.

248
00:15:20,000 --> 00:15:24,000
 Because anything that arises will cease.

249
00:15:24,000 --> 00:15:29,000
 And it just leaves you wanting more.

250
00:15:29,000 --> 00:15:32,000
 It leaves you with expectations.

251
00:15:32,000 --> 00:15:36,000
 Nothing in the world can be an end unto itself.

252
00:15:36,000 --> 00:15:38,570
 Because any goal that you might have, any end that you

253
00:15:38,570 --> 00:15:42,000
 might have is ephemeral.

254
00:15:42,000 --> 00:15:49,000
 You realize it and then it's gone.

255
00:15:49,000 --> 00:15:54,000
 "Kiranakindji samudayadamang sabantang nirodadamam"

256
00:15:54,000 --> 00:15:59,910
 All dhammas that are of a nature to arise, all such dhammas

257
00:15:59,910 --> 00:16:03,000
 are of a nature to cease.

258
00:16:03,000 --> 00:16:09,490
 Any dhammas that are of a nature to arise are also of a

259
00:16:09,490 --> 00:16:12,000
 nature to cease.

260
00:16:12,000 --> 00:16:19,370
 Obviously it seems like a... so obvious it doesn't need to

261
00:16:19,370 --> 00:16:25,000
 be even said, but...

262
00:16:25,000 --> 00:16:28,290
 But the profundity of it is realized in the meditation that

263
00:16:28,290 --> 00:16:31,000
 actually, yes, that's really it.

264
00:16:31,000 --> 00:16:35,000
 There's no argument, you can't argue with that.

265
00:16:35,000 --> 00:16:41,920
 That statement defeats any argument for clinging to

266
00:16:41,920 --> 00:16:48,000
 something arisen, because it will cease.

267
00:16:48,000 --> 00:16:51,000
 But it's a long way to get there.

268
00:16:51,000 --> 00:16:57,000
 This knowledge isn't really... isn't for most people enough

269
00:16:57,000 --> 00:16:57,000
.

270
00:16:57,000 --> 00:17:02,060
 And you don't want to go into detail, like in a booklet,

271
00:17:02,060 --> 00:17:06,000
 about the things that you need to get there.

272
00:17:06,000 --> 00:17:11,040
 Because the last thing you want is someone taking shortcuts

273
00:17:11,040 --> 00:17:21,000
 or using book learning or listening to talks

274
00:17:21,000 --> 00:17:29,720
 as a replacement for actual guidance, guidance to correct

275
00:17:29,720 --> 00:17:31,000
 your wrong views.

276
00:17:31,000 --> 00:17:35,990
 Until we reach nibbana, we can't trust ourselves. We really

277
00:17:35,990 --> 00:17:38,000
 can't.

278
00:17:38,000 --> 00:17:43,510
 There's a great difference between someone who has seen nib

279
00:17:43,510 --> 00:17:45,000
bana and someone who hasn't.

280
00:17:45,000 --> 00:17:49,060
 This is where the Buddha put great emphasis on a sotapana,

281
00:17:49,060 --> 00:17:51,720
 even though they still have defilements, they can still get

282
00:17:51,720 --> 00:17:54,000
 greedy and angry.

283
00:17:54,000 --> 00:17:57,890
 But there's a huge difference, because they can't have

284
00:17:57,890 --> 00:17:59,000
 wrong view.

285
00:17:59,000 --> 00:18:06,000
 And it's... it's... it's a categorical difference.

286
00:18:06,000 --> 00:18:08,990
 An ordinary person can have right view from time to time,

287
00:18:08,990 --> 00:18:14,000
 but they're just as likely to get lost in wrong view.

288
00:18:14,000 --> 00:18:18,000
 Nothing will stop that. Nothing can stop that.

289
00:18:18,000 --> 00:18:21,360
 Nothing can prevent wrong view from arising, except the

290
00:18:21,360 --> 00:18:29,000
 realization of nibbana, the realization of sotapana.

291
00:18:29,000 --> 00:18:35,140
 So, just a reminder that there's more, and if you have time

292
00:18:35,140 --> 00:18:37,000
, I know a lot of people...

293
00:18:37,000 --> 00:18:40,660
 The reason for doing all this internet work is to reach out

294
00:18:40,660 --> 00:18:43,000
 to people who don't have something.

295
00:18:43,000 --> 00:18:48,000
 So, not to despair, certainly this is great. A great thing,

296
00:18:48,000 --> 00:18:52,790
 those people who are starting out and practicing basic

297
00:18:52,790 --> 00:18:56,000
 meditation, it's a great thing.

298
00:18:56,000 --> 00:19:01,000
 But the next step is to find someone who can guide you.

299
00:19:01,000 --> 00:19:05,520
 Because that's really how Buddhism is spread, really

300
00:19:05,520 --> 00:19:07,000
 through guidance.

301
00:19:07,000 --> 00:19:12,240
 And, actually, these days when we're, I think, arguably

302
00:19:12,240 --> 00:19:17,000
 more lost and harder to reach than in the Buddhist time.

303
00:19:17,000 --> 00:19:20,030
 I think it's reasonable to say that we're harder to reach

304
00:19:20,030 --> 00:19:23,000
 these days than we were 2,500 years ago.

305
00:19:23,000 --> 00:19:29,000
 So, it takes time and it takes guidance.

306
00:19:29,000 --> 00:19:32,730
 And trying to myself, and we try, you know, it's a common

307
00:19:32,730 --> 00:19:36,180
 thing for teachers to travel around the world for this

308
00:19:36,180 --> 00:19:37,000
 reason.

309
00:19:37,000 --> 00:19:42,060
 Personally, I'll probably be going through parts of the US,

310
00:19:42,060 --> 00:19:46,620
 maybe even making it to the UK, and who knows, maybe

311
00:19:46,620 --> 00:19:49,000
 Australia someday.

312
00:19:49,000 --> 00:19:57,090
 But there's always the opportunity to travel somewhere. If

313
00:19:57,090 --> 00:19:59,110
 you can travel to where I am, travel to where you know

314
00:19:59,110 --> 00:20:00,000
 there's a teacher.

315
00:20:00,000 --> 00:20:04,260
 There are many opportunities, many teachers out there. Cour

316
00:20:04,260 --> 00:20:08,000
ses are now easier to come by. Not easy, but easier.

317
00:20:08,000 --> 00:20:14,120
 Just to say that it's worth it. Guidance is still the best

318
00:20:14,120 --> 00:20:15,000
 way.

319
00:20:15,000 --> 00:20:20,470
 Buddhism isn't spread through texts. Eventually the texts

320
00:20:20,470 --> 00:20:24,040
 are going to be all that's left and there will be no one to

321
00:20:24,040 --> 00:20:25,000
 explain them.

322
00:20:25,000 --> 00:20:29,950
 Because reading the texts with wrong view will...it's very

323
00:20:29,950 --> 00:20:33,000
 difficult for it to give you a right view.

324
00:20:33,000 --> 00:20:37,590
 Very difficult. Because the texts aren't enlightened. The

325
00:20:37,590 --> 00:20:42,000
 texts are just words on paper.

326
00:20:42,000 --> 00:20:47,180
 They're...I mean, they can. Some people can, but it's not

327
00:20:47,180 --> 00:20:48,000
 common.

328
00:20:48,000 --> 00:20:51,390
 You really need that human connection. Someone to remind

329
00:20:51,390 --> 00:20:54,000
 you, someone to give you an alternative.

330
00:20:54,000 --> 00:20:59,610
 Because we bounce off of each other, we react to each other

331
00:20:59,610 --> 00:21:00,000
.

332
00:21:00,000 --> 00:21:05,600
 I hold one view and one...I come from one point of view and

333
00:21:05,600 --> 00:21:09,000
 I expect you to resonate with that.

334
00:21:09,000 --> 00:21:14,330
 But if I've got wrong view and you're a sotapana, then I

335
00:21:14,330 --> 00:21:17,470
 come to you with my wrong view and you don't resonate with

336
00:21:17,470 --> 00:21:18,000
 that.

337
00:21:18,000 --> 00:21:22,850
 And this is what you need. You need someone who disturbs

338
00:21:22,850 --> 00:21:26,000
 your sense of...your inertia.

339
00:21:26,000 --> 00:21:31,850
 Who disturbs your equilibrium. Shakes you up. That's what

340
00:21:31,850 --> 00:21:34,000
 an enlightened being does.

341
00:21:34,000 --> 00:21:39,560
 Because they're not like you. They're not like an unenlight

342
00:21:39,560 --> 00:21:42,000
ened ordinary people.

343
00:21:42,000 --> 00:21:46,220
 There's something categorically different about them and

344
00:21:46,220 --> 00:21:51,000
 they will disturb the equilibrium. That's what's important.

345
00:21:51,000 --> 00:21:55,170
 Anyway, just more food for thought. Again, thanks for

346
00:21:55,170 --> 00:22:00,000
 tuning in. I hope this was useful, helpful.

347
00:22:00,000 --> 00:22:06,050
 I'm wishing you all good practice and the attainment of

348
00:22:06,050 --> 00:22:10,000
 peace, happiness and freedom from suffering. Be well.

