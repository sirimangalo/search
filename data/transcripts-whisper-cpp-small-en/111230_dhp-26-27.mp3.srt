1
00:00:00,000 --> 00:00:05,240
 Hello and welcome again to our study of the Dhammapada.

2
00:00:05,240 --> 00:00:07,000
 Today we go on to verses number

3
00:00:07,000 --> 00:00:13,000
 26 and 27 which read as follows.

4
00:00:13,000 --> 00:00:25,680
 Madam Anu Yunjanti, Baladu Medhinojana, Apama Dhancha Medha

5
00:00:25,680 --> 00:00:30,790
 we, Dhananse Dhanwaraakati, Mopama Dham Anu Yunjeta, Makama

6
00:00:30,790 --> 00:00:32,000
 Rati Sandhavang,

7
00:00:32,000 --> 00:00:42,560
 Apamatthohi Chayantthoh Papthohi Vipulangsukham. So two

8
00:00:42,560 --> 00:00:46,000
 verses again on the practice of meditation.

9
00:00:46,000 --> 00:00:49,110
 The Apamada Vaga is going to be mostly about the practice

10
00:00:49,110 --> 00:00:52,000
 of meditation because the word Apamada

11
00:00:52,000 --> 00:00:56,170
 we translate as heedfulness but it's really the core of

12
00:00:56,170 --> 00:00:58,710
 what is meant by meditation or meditation practice in

13
00:00:58,710 --> 00:00:59,000
 Buddhism.

14
00:00:59,000 --> 00:01:03,000
 It's translated directly by the Buddha as mindfulness.

15
00:01:03,000 --> 00:01:07,810
 It says the meaning of the word Apamada is to be mindful

16
00:01:07,810 --> 00:01:11,620
 all the time, to never be without mindfulness or without

17
00:01:11,620 --> 00:01:15,000
 sati, without remembrance or recollection.

18
00:01:15,000 --> 00:01:19,840
 So the direct meaning of these verses, "Pamada Manu Yunjant

19
00:01:19,840 --> 00:01:20,000
in"

20
00:01:20,000 --> 00:01:28,250
 they indulge or they partake of heedlessness or negligence,

21
00:01:28,250 --> 00:01:32,000
 the fools and the unwise people.

22
00:01:32,000 --> 00:01:45,210
 But those who are wise made the wise partake in Apamada,

23
00:01:45,210 --> 00:01:47,000
 partake in heedfulness

24
00:01:47,000 --> 00:01:53,830
 and guard it, Vlakati, Dhanang Satang like a high or a

25
00:01:53,830 --> 00:01:56,000
 special treasure.

26
00:01:56,000 --> 00:02:03,610
 So the wise guard mindfulness or heedfulness as a great

27
00:02:03,610 --> 00:02:05,000
 treasure.

28
00:02:05,000 --> 00:02:10,000
 And then the Buddha says, "Mopama Dham Anu Yunjeta"

29
00:02:10,000 --> 00:02:17,560
 Don't engage in heedlessness, in negligence. "Mopama Rati

30
00:02:17,560 --> 00:02:18,000
 Santavang"

31
00:02:18,000 --> 00:02:26,030
 Don't partake or indulge in or don't become intoxicated by

32
00:02:26,030 --> 00:02:28,000
 sensuality

33
00:02:28,000 --> 00:02:36,480
 because only those who are heedful and meditating attain

34
00:02:36,480 --> 00:02:42,000
 great happiness or attain a profound happiness.

35
00:02:42,000 --> 00:02:48,000
 "Papu Ti Wipulang Sukan" A profound happiness is attained.

36
00:02:48,000 --> 00:02:52,140
 So this is a very important teaching to understand and it's

37
00:02:52,140 --> 00:02:55,430
 something that really cuts right to the heart of our

38
00:02:55,430 --> 00:02:57,000
 attachments in the worldly sphere.

39
00:02:57,000 --> 00:03:01,800
 Really good story, very short story. But the story goes

40
00:03:01,800 --> 00:03:06,000
 that in Sawati there was a holiday.

41
00:03:06,000 --> 00:03:09,640
 And it kind of seems like a strange holiday but there are

42
00:03:09,640 --> 00:03:12,000
 parallels even in modern times.

43
00:03:12,000 --> 00:03:17,000
 It was a holiday where anything went, anything was allowed.

44
00:03:17,000 --> 00:03:23,190
 So no longer you had, there was no sense of respect for

45
00:03:23,190 --> 00:03:24,000
 anyone.

46
00:03:24,000 --> 00:03:27,820
 The people who partook in this holiday would abandon all

47
00:03:27,820 --> 00:03:33,000
 sense of respect for mother, father, for boss, for teacher

48
00:03:33,000 --> 00:03:33,000
 and so on.

49
00:03:33,000 --> 00:03:37,560
 And they would go from house to house, probably drunk, and

50
00:03:37,560 --> 00:03:40,000
 abuse and revile everyone.

51
00:03:40,000 --> 00:03:43,800
 You pig, you cow, you goat, calling them all sorts of

52
00:03:43,800 --> 00:03:45,000
 horrible names.

53
00:03:45,000 --> 00:03:47,960
 And the game was that if you wanted them to go away you had

54
00:03:47,960 --> 00:03:49,000
 to give them stuff.

55
00:03:49,000 --> 00:03:51,630
 So it was like trick or treat basically, like Halloween

56
00:03:51,630 --> 00:03:55,000
 except they really did it. They were really mean and nasty.

57
00:03:55,000 --> 00:03:59,320
 And they would stay there until you give them some money or

58
00:03:59,320 --> 00:04:04,000
 some wine or whatever, food, and then they would go away.

59
00:04:04,000 --> 00:04:07,740
 And so the thing was that in Sawati there was a great

60
00:04:07,740 --> 00:04:11,000
 proportion of people who were enlightened.

61
00:04:11,000 --> 00:04:14,010
 Or Sota, Pana, or Sakita, Gami, at some stage of

62
00:04:14,010 --> 00:04:15,000
 enlightenment.

63
00:04:15,000 --> 00:04:19,000
 So they obviously wouldn't partake in such a festival.

64
00:04:19,000 --> 00:04:22,000
 And so they had a horrible time for seven days.

65
00:04:22,000 --> 00:04:24,650
 And they asked the Buddha not to come to Sawati because if

66
00:04:24,650 --> 00:04:27,000
 the Buddha went there they would revile the Buddha.

67
00:04:27,000 --> 00:04:29,000
 And that would be very bad karma for them.

68
00:04:29,000 --> 00:04:31,420
 So they said to the Buddha, "Please stay in Jethavana and

69
00:04:31,420 --> 00:04:33,000
 we'll bring you food every day."

70
00:04:33,000 --> 00:04:35,000
 So they brought food for seven days.

71
00:04:35,000 --> 00:04:38,000
 And then after the seventh day they came to see the Buddha

72
00:04:38,000 --> 00:04:40,930
 and said, "Oh, Venerable Sir, we stayed so awful for these

73
00:04:40,930 --> 00:04:42,000
 seven days.

74
00:04:42,000 --> 00:04:46,140
 It was so tiring for us to have to put up with this sill

75
00:04:46,140 --> 00:04:47,000
iness.

76
00:04:47,000 --> 00:04:54,000
 Why are people like this? Why do people act in such a way?

77
00:04:54,000 --> 00:04:58,000
 Isn't it crazy that they do?" And the Buddha said, "Oh yes,

78
00:04:58,000 --> 00:05:02,000
 this is the difference between those who are enlightened

79
00:05:02,000 --> 00:05:05,230
 or those who have understood the truth and those who are

80
00:05:05,230 --> 00:05:07,000
 still mired in delusion."

81
00:05:07,000 --> 00:05:12,000
 And so the first verse here, and then he told these verses,

82
00:05:12,000 --> 00:05:15,000
 and the first verse explains the difference between the two

83
00:05:15,000 --> 00:05:15,000
.

84
00:05:15,000 --> 00:05:18,980
 That an ordinary person, a person who has no idea about

85
00:05:18,980 --> 00:05:21,000
 what is right and what is wrong

86
00:05:21,000 --> 00:05:24,260
 and has never contemplated these things or investigated

87
00:05:24,260 --> 00:05:26,000
 what might be the truth,

88
00:05:26,000 --> 00:05:33,450
 what the things that they value are the most base and

89
00:05:33,450 --> 00:05:36,000
 useless qualities.

90
00:05:36,000 --> 00:05:40,000
 For instance, negligence or drunkenness.

91
00:05:40,000 --> 00:05:44,000
 So an ordinary person and people in ordinary life will

92
00:05:44,000 --> 00:05:46,000
 actually,

93
00:05:46,000 --> 00:05:48,820
 the things that they will cherish are the times when they

94
00:05:48,820 --> 00:05:53,000
 were the least mindful and the least clear in their mind.

95
00:05:53,000 --> 00:05:56,830
 So when you think in lay life, would people, or not in lay

96
00:05:56,830 --> 00:05:57,000
 life,

97
00:05:57,000 --> 00:05:59,640
 for people who haven't practiced meditation, the kind of

98
00:05:59,640 --> 00:06:01,000
 things that they cherish,

99
00:06:01,000 --> 00:06:03,350
 they cherish those times when they were drunk or when they

100
00:06:03,350 --> 00:06:04,000
 did something silly

101
00:06:04,000 --> 00:06:07,210
 and they were having a great time and doing all sorts of

102
00:06:07,210 --> 00:06:08,000
 crazy things

103
00:06:08,000 --> 00:06:11,650
 when they were young and crazy and how wonderful it was and

104
00:06:11,650 --> 00:06:12,000
 so on.

105
00:06:12,000 --> 00:06:15,000
 They cherish these times when they do the craziest things.

106
00:06:15,000 --> 00:06:18,540
 And so people who, for instance, in this festival, they

107
00:06:18,540 --> 00:06:20,000
 would always think back to,

108
00:06:20,000 --> 00:06:22,500
 "Oh, wasn't it a wonderful time where we could say whatever

109
00:06:22,500 --> 00:06:25,000
 we want? Didn't we have a great time then?"

110
00:06:25,000 --> 00:06:28,000
 And they make it out to be this wonderful thing.

111
00:06:28,000 --> 00:06:33,200
 And a person who has actually taken the time to see the

112
00:06:33,200 --> 00:06:34,000
 consequences of our actions

113
00:06:34,000 --> 00:06:38,400
 and the consequences of every individual mind state is

114
00:06:38,400 --> 00:06:40,000
 totally the opposite

115
00:06:40,000 --> 00:06:43,630
 and actually cherishes as a treasure those times when they

116
00:06:43,630 --> 00:06:46,000
 are cogent, those times when they are aware.

117
00:06:46,000 --> 00:06:49,270
 And one of the big reasons for this, I think, is that a

118
00:06:49,270 --> 00:06:55,030
 person who has no idea or no skill in understanding

119
00:06:55,030 --> 00:06:56,000
 experience

120
00:06:56,000 --> 00:07:00,000
 will have a very difficult time just being with reality.

121
00:07:00,000 --> 00:07:02,300
 Reality is the worst thing for them, the last thing they

122
00:07:02,300 --> 00:07:05,000
 want to focus on, because it's unpleasant,

123
00:07:05,000 --> 00:07:07,900
 it's difficult to deal with. So pain comes up in their

124
00:07:07,900 --> 00:07:09,000
 tears in their eyes

125
00:07:09,000 --> 00:07:13,550
 and when someone's saying things to them that they don't

126
00:07:13,550 --> 00:07:15,000
 like and they get angry,

127
00:07:15,000 --> 00:07:18,000
 when they have to do work and then they become frustrated

128
00:07:18,000 --> 00:07:22,000
 or bored or depressed or so on,

129
00:07:22,000 --> 00:07:26,000
 the last thing they want to do is to focus on reality.

130
00:07:26,000 --> 00:07:34,580
 And so the whole goal or the point in Buddhism, in the

131
00:07:34,580 --> 00:07:37,000
 Buddhist teaching, is to come back to reality

132
00:07:37,000 --> 00:07:44,000
 and to find a way to be happy at all times in any situation

133
00:07:44,000 --> 00:07:44,000
,

134
00:07:44,000 --> 00:07:50,000
 not have our happiness depend on some specific circumstance

135
00:07:50,000 --> 00:07:56,000
 and not have it require some specific situation.

136
00:07:56,000 --> 00:07:59,180
 Because when your happiness depends on something, it

137
00:07:59,180 --> 00:08:00,000
 becomes an addiction

138
00:08:00,000 --> 00:08:03,760
 and it becomes a partiality where you aren't able to accept

139
00:08:03,760 --> 00:08:05,000
 the rest of reality

140
00:08:05,000 --> 00:08:10,100
 because you're thinking about or wanting or striving after

141
00:08:10,100 --> 00:08:12,000
 a certain experience.

142
00:08:12,000 --> 00:08:18,450
 And the experience has to become further and more and more

143
00:08:18,450 --> 00:08:22,000
 extravagant in order to satisfy the cravings

144
00:08:22,000 --> 00:08:29,000
 because our pleasure stimulus isn't a static system.

145
00:08:29,000 --> 00:08:32,000
 When you get what you want, you want more of it.

146
00:08:32,000 --> 00:08:34,740
 When you get more of it, you want even more and it builds

147
00:08:34,740 --> 00:08:36,000
 and builds and builds until you...

148
00:08:36,000 --> 00:08:39,290
 The only way you can be happy is to have some crazy

149
00:08:39,290 --> 00:08:40,000
 festival like this

150
00:08:40,000 --> 00:08:44,000
 where you go and do something because you're repressing

151
00:08:44,000 --> 00:08:44,000
 these desires,

152
00:08:44,000 --> 00:08:49,000
 you're repressing your greed, your anger and so on.

153
00:08:49,000 --> 00:08:55,000
 I heard about this one society, this one group of people

154
00:08:55,000 --> 00:09:02,000
 where they will engage in group sex.

155
00:09:02,000 --> 00:09:06,040
 They will get all together and have a big orgy just like

156
00:09:06,040 --> 00:09:07,000
 animals.

157
00:09:07,000 --> 00:09:11,000
 And for them, this may sound coarse and rude to say,

158
00:09:11,000 --> 00:09:14,710
 but it's actually a philosophy that they have that if this

159
00:09:14,710 --> 00:09:16,000
 is a way to find pleasure,

160
00:09:16,000 --> 00:09:21,340
 then you should strive to indulge in it to the most extreme

161
00:09:21,340 --> 00:09:23,000
 extent possible.

162
00:09:23,000 --> 00:09:26,000
 And so it's a philosophy that they have.

163
00:09:26,000 --> 00:09:29,140
 And the idea being that as long as you can get it, then

164
00:09:29,140 --> 00:09:30,000
 what's wrong with it?

165
00:09:30,000 --> 00:09:33,000
 Because you can be happy all the time. This is the idea.

166
00:09:33,000 --> 00:09:36,310
 So this is for people who don't see what they're doing to

167
00:09:36,310 --> 00:09:37,000
 their mind.

168
00:09:37,000 --> 00:09:39,660
 They think it's somehow static. Well, if I want it any time

169
00:09:39,660 --> 00:09:40,000
, I can get it.

170
00:09:40,000 --> 00:09:42,800
 And then if I don't want it, I can come back and not have

171
00:09:42,800 --> 00:09:43,000
 it.

172
00:09:43,000 --> 00:09:46,270
 Without seeing the effect that it's having on your mind and

173
00:09:46,270 --> 00:09:50,000
 on the pleasure stimulus systems in the brain and so on.

174
00:09:50,000 --> 00:09:53,510
 And how actually you're creating an addiction and you're

175
00:09:53,510 --> 00:09:55,000
 creating an intense partiality.

176
00:09:55,000 --> 00:09:57,000
 This is an example of an extreme state.

177
00:09:57,000 --> 00:09:59,650
 There are societies where they don't allow this sort of

178
00:09:59,650 --> 00:10:00,000
 thing

179
00:10:00,000 --> 00:10:08,310
 and where you have to be totally formal and polite and

180
00:10:08,310 --> 00:10:10,000
 proper at all times.

181
00:10:10,000 --> 00:10:14,000
 Conservative, ethical and so on.

182
00:10:14,000 --> 00:10:21,000
 And any kind of showing of emotion is considered bad form.

183
00:10:21,000 --> 00:10:24,760
 But then they still have these desires and they repress

184
00:10:24,760 --> 00:10:26,000
 them and repress them and repress them.

185
00:10:26,000 --> 00:10:28,000
 And then they have these festivals.

186
00:10:28,000 --> 00:10:32,250
 So I've heard places where everything has to be proper and

187
00:10:32,250 --> 00:10:36,000
 everyone has a role and hierarchy and so on.

188
00:10:36,000 --> 00:10:38,360
 But then they have these parties where they get drunk and

189
00:10:38,360 --> 00:10:39,000
 anything goes.

190
00:10:39,000 --> 00:10:43,280
 And like I heard about the boss getting together with

191
00:10:43,280 --> 00:10:45,000
 having crazy things.

192
00:10:45,000 --> 00:10:47,540
 So these are the things that I hear in the world. And these

193
00:10:47,540 --> 00:10:49,000
 things really do go on.

194
00:10:49,000 --> 00:10:52,970
 I mean, it's actually kind of embarrassing to talk about

195
00:10:52,970 --> 00:10:54,000
 these things.

196
00:10:54,000 --> 00:10:56,000
 But this is what's really happening in the world.

197
00:10:56,000 --> 00:11:01,530
 People are so lost that they actually think these things

198
00:11:01,530 --> 00:11:04,000
 are a cause for happiness.

199
00:11:04,000 --> 00:11:08,820
 And so there are scientists and researchers who have

200
00:11:08,820 --> 00:11:10,000
 studied this

201
00:11:10,000 --> 00:11:13,410
 and see that it really goes step by step by step until

202
00:11:13,410 --> 00:11:16,000
 finally it becomes such a perverse thing

203
00:11:16,000 --> 00:11:20,560
 where you're involving these groups or even they were

204
00:11:20,560 --> 00:11:25,000
 saying on this one article that I read.

205
00:11:25,000 --> 00:11:29,000
 It eventually gets to with animals and so on.

206
00:11:29,000 --> 00:11:32,000
 This is where you find your pleasure.

207
00:11:32,000 --> 00:11:34,600
 And so this is the kind of thing that we would have seen in

208
00:11:34,600 --> 00:11:35,000
 Sawati.

209
00:11:35,000 --> 00:11:38,000
 Of course, it just talks about reviling people.

210
00:11:38,000 --> 00:11:42,870
 But for sure that they had other festivals in regards to

211
00:11:42,870 --> 00:11:44,000
 sensuality.

212
00:11:44,000 --> 00:11:47,000
 And this is where the whole Gama Sutra came up and so on.

213
00:11:47,000 --> 00:11:49,890
 And this is how people in the world think that they can

214
00:11:49,890 --> 00:11:51,000
 find happiness.

215
00:11:51,000 --> 00:11:55,000
 So there are two ways that we understand this.

216
00:11:55,000 --> 00:11:57,630
 For someone who practices meditation, it's obvious that

217
00:11:57,630 --> 00:11:59,000
 these things don't lead to happiness.

218
00:11:59,000 --> 00:12:01,990
 You engage in them, you indulge in them, and you find that

219
00:12:01,990 --> 00:12:04,000
 you're just back where you started from.

220
00:12:04,000 --> 00:12:08,370
 You're still unhappy inside. You're still unsatisfied even

221
00:12:08,370 --> 00:12:10,000
 with these things.

222
00:12:10,000 --> 00:12:17,410
 But really the idea is that we're trying to find happiness

223
00:12:17,410 --> 00:12:19,000
 here and now.

224
00:12:19,000 --> 00:12:22,150
 Even if you can say that, "Well, I may be unhappy here, but

225
00:12:22,150 --> 00:12:24,000
 when I get what I want, at that time I'm happy.

226
00:12:24,000 --> 00:12:27,260
 So what's wrong with it? I'm getting a happiness. At the

227
00:12:27,260 --> 00:12:29,000
 time when I get it, there's pleasure."

228
00:12:29,000 --> 00:12:33,000
 But the point is that the whole fact that you...

229
00:12:33,000 --> 00:12:37,210
 The bare fact that you want that is a sign that you're not

230
00:12:37,210 --> 00:12:38,000
 happy.

231
00:12:38,000 --> 00:12:39,820
 If you are happy, you wouldn't need to strive for something

232
00:12:39,820 --> 00:12:40,000
.

233
00:12:40,000 --> 00:12:43,000
 You wouldn't need to go for something.

234
00:12:43,000 --> 00:12:44,000
 You wouldn't need to build something.

235
00:12:44,000 --> 00:12:45,000
 You wouldn't need to create something.

236
00:12:45,000 --> 00:12:49,000
 You wouldn't need to attain something or get something.

237
00:12:49,000 --> 00:12:55,200
 So the radical task that we have is to find happiness

238
00:12:55,200 --> 00:12:56,000
 without getting that,

239
00:12:56,000 --> 00:12:59,000
 without getting anything, to find happiness here and now.

240
00:12:59,000 --> 00:13:02,690
 And it's actually really a simple thing, and if you think

241
00:13:02,690 --> 00:13:04,000
 about it, it's how it should be,

242
00:13:04,000 --> 00:13:06,000
 that we should find happiness here and now.

243
00:13:06,000 --> 00:13:09,000
 Why should our happiness depend on something over there?

244
00:13:09,000 --> 00:13:12,000
 Why can't our happiness be here and now?

245
00:13:12,000 --> 00:13:16,000
 It's a very valid question, but it's one that we never ask

246
00:13:16,000 --> 00:13:18,000
 and we always overlook.

247
00:13:18,000 --> 00:13:20,440
 We think, obviously, if you want happiness, you have to

248
00:13:20,440 --> 00:13:23,000
 strive for it. You have to go for it.

249
00:13:23,000 --> 00:13:25,800
 Well, the truth is that even to find happiness here and now

250
00:13:25,800 --> 00:13:27,000
, you have to strive for it.

251
00:13:27,000 --> 00:13:32,470
 And so this is why actually often people will find that the

252
00:13:32,470 --> 00:13:34,000
 practice of meditation is something

253
00:13:34,000 --> 00:13:37,000
 that is uncomfortable and unpleasant, right?

254
00:13:37,000 --> 00:13:39,300
 The point being, because you always want to go out for

255
00:13:39,300 --> 00:13:40,000
 something.

256
00:13:40,000 --> 00:13:42,620
 You want to go after that and you're trying to train

257
00:13:42,620 --> 00:13:44,000
 yourself out of that.

258
00:13:44,000 --> 00:13:47,550
 So sometimes you stop yourself and you say no, and then you

259
00:13:47,550 --> 00:13:49,000
 feel unhappy.

260
00:13:49,000 --> 00:13:51,830
 Or sometimes you go after it and then you feel guilty or so

261
00:13:51,830 --> 00:13:52,000
 on,

262
00:13:52,000 --> 00:13:57,550
 until you finally are able to change your mind and straight

263
00:13:57,550 --> 00:13:58,000
en your mind

264
00:13:58,000 --> 00:14:05,600
 so that you're able to see the goal, to see the object of

265
00:14:05,600 --> 00:14:08,000
 your desire

266
00:14:08,000 --> 00:14:12,390
 and to simply see it for what it is and to be able to live

267
00:14:12,390 --> 00:14:15,000
 with the perception of something

268
00:14:15,000 --> 00:14:17,000
 without having to chase after it.

269
00:14:17,000 --> 00:14:20,000
 So when you think of something that you normally would like

270
00:14:20,000 --> 00:14:21,000
, you're just thinking of it.

271
00:14:21,000 --> 00:14:25,610
 When you hear a sound that you find pleasant, you just hear

272
00:14:25,610 --> 00:14:26,000
 the sound.

273
00:14:26,000 --> 00:14:29,000
 When you see something beautiful, you just see it.

274
00:14:29,000 --> 00:14:33,000
 And you're able to experience happiness in that moment

275
00:14:33,000 --> 00:14:35,000
 without having your happiness be only

276
00:14:35,000 --> 00:14:39,900
 when you get that object and you find yourself at peace

277
00:14:39,900 --> 00:14:41,000
 here and now.

278
00:14:41,000 --> 00:14:43,000
 But it's something that you have to strive for.

279
00:14:43,000 --> 00:14:45,000
 So we can say that there are these two strivings.

280
00:14:45,000 --> 00:14:48,300
 This is what the Buddha said, people who strive after

281
00:14:48,300 --> 00:14:49,000
 pleasure

282
00:14:49,000 --> 00:14:52,000
 and trying to find some object of happiness.

283
00:14:52,000 --> 00:14:54,480
 And people who strive to be free from that, to find

284
00:14:54,480 --> 00:14:56,000
 happiness here and now.

285
00:14:56,000 --> 00:14:58,000
 It's really two different paths.

286
00:14:58,000 --> 00:15:00,390
 It's not that we're striving to achieve something or get

287
00:15:00,390 --> 00:15:01,000
 something.

288
00:15:01,000 --> 00:15:05,570
 We're trying to stop striving, to stop achieving, to stop

289
00:15:05,570 --> 00:15:07,000
 chasing after.

290
00:15:07,000 --> 00:15:08,000
 It's also very difficult.

291
00:15:08,000 --> 00:15:10,480
 So these are the two paths and this is what the Buddha lays

292
00:15:10,480 --> 00:15:12,000
 out here.

293
00:15:12,000 --> 00:15:16,280
 That for ordinary people they will just let themselves go,

294
00:15:16,280 --> 00:15:19,000
 drink alcohol and take drugs

295
00:15:19,000 --> 00:15:23,090
 and engage in all sorts of foolishness, enjoying the

296
00:15:23,090 --> 00:15:24,000
 happiness.

297
00:15:24,000 --> 00:15:26,540
 But they don't see how it's changing their minds and they

298
00:15:26,540 --> 00:15:29,000
 don't see how it's building these mindsets.

299
00:15:29,000 --> 00:15:31,000
 We're not static beings.

300
00:15:31,000 --> 00:15:34,230
 Another thing is this shows how important the Buddha's

301
00:15:34,230 --> 00:15:36,000
 teaching of karma is.

302
00:15:36,000 --> 00:15:39,050
 And it goes a lot deeper than we actually think because I

303
00:15:39,050 --> 00:15:41,000
 don't have karma, you don't have karma.

304
00:15:41,000 --> 00:15:45,000
 Karma is the truth of reality. There only is karma.

305
00:15:45,000 --> 00:15:47,000
 We create ourselves out of karma.

306
00:15:47,000 --> 00:15:49,000
 The whole of our being is created out of karma.

307
00:15:49,000 --> 00:15:52,000
 The whole of our life is created out of karma.

308
00:15:52,000 --> 00:15:55,640
 There's no "I". There's only the actions, the things that

309
00:15:55,640 --> 00:15:56,000
 we do.

310
00:15:56,000 --> 00:16:00,000
 So everything that we do creates us.

311
00:16:00,000 --> 00:16:02,750
 The reason why we get angry is because we've built up anger

312
00:16:02,750 --> 00:16:03,000
.

313
00:16:03,000 --> 00:16:05,600
 The reason why we have greed and want things is because we

314
00:16:05,600 --> 00:16:07,000
 build up greed and want things.

315
00:16:07,000 --> 00:16:09,000
 Year after year, that's all we are.

316
00:16:09,000 --> 00:16:12,540
 Where these accumulation or these conglomeration,

317
00:16:12,540 --> 00:16:15,000
 aggregation of minds things,

318
00:16:15,000 --> 00:16:17,000
 both positive and negative.

319
00:16:17,000 --> 00:16:19,860
 Why we have love and kindness is because we've developed

320
00:16:19,860 --> 00:16:20,000
 this.

321
00:16:20,000 --> 00:16:22,000
 Because we've come to see the truth.

322
00:16:22,000 --> 00:16:25,000
 Because we've come to see how horrible it is to suffer.

323
00:16:25,000 --> 00:16:29,000
 We don't want to see other being suffer and so on.

324
00:16:29,000 --> 00:16:33,470
 We are what we do. We are according to the things that we

325
00:16:33,470 --> 00:16:36,000
 do and the qualities that we build up.

326
00:16:36,000 --> 00:16:39,000
 So it's important to understand the difference here.

327
00:16:39,000 --> 00:16:43,320
 Because I think all of us can see in our own lives and in

328
00:16:43,320 --> 00:16:46,000
 our own past this attachment to negligence,

329
00:16:46,000 --> 00:16:49,000
 this attachment to having fun and enjoying life and so on.

330
00:16:49,000 --> 00:16:53,580
 Without seeing how it's creating addiction, how it's

331
00:16:53,580 --> 00:16:55,000
 actually feeding our dissatisfaction.

332
00:16:55,000 --> 00:16:59,000
 So it's making it harder for us to just be here and now.

333
00:16:59,000 --> 00:17:01,720
 When we come back to be just here and now, we see how

334
00:17:01,720 --> 00:17:05,000
 radically opposed these two paths are.

335
00:17:05,000 --> 00:17:07,460
 Being, just being here and now is something totally

336
00:17:07,460 --> 00:17:11,000
 different and completely opposed.

337
00:17:11,000 --> 00:17:14,120
 In the sense that the more we strive to achieve happiness

338
00:17:14,120 --> 00:17:17,000
 and strive to achieve pleasure,

339
00:17:17,000 --> 00:17:19,450
 the harder it is for us to find happiness and peace here

340
00:17:19,450 --> 00:17:20,000
 and now.

341
00:17:20,000 --> 00:17:22,460
 The more we find peace and happiness here and now, the less

342
00:17:22,460 --> 00:17:25,000
 we want, the less we need of anything.

343
00:17:25,000 --> 00:17:27,450
 If you think just where we are right here, right now, when

344
00:17:27,450 --> 00:17:31,000
 we find happiness here, everything else disappears.

345
00:17:31,000 --> 00:17:35,390
 Suddenly the whole world, all of who we are, our lives, our

346
00:17:35,390 --> 00:17:39,000
 friends, our family, all of our thoughts,

347
00:17:39,000 --> 00:17:43,920
 all of our ambitions have just disappeared, vanished in

348
00:17:43,920 --> 00:17:45,000
 thin air.

349
00:17:45,000 --> 00:17:47,000
 That's the first verse.

350
00:17:47,000 --> 00:17:49,880
 The second verse is an injunction by the Buddha that once

351
00:17:49,880 --> 00:17:52,000
 you see this, if you're a wise person,

352
00:17:52,000 --> 00:17:55,000
 you should not engage in negligence.

353
00:17:55,000 --> 00:17:57,830
 Don't engage in it because it will make it harder for you

354
00:17:57,830 --> 00:17:59,000
 to find happiness.

355
00:17:59,000 --> 00:18:02,080
 It will make you less happy, less satisfied, less at peace

356
00:18:02,080 --> 00:18:03,000
 with yourself.

357
00:18:03,000 --> 00:18:10,000
 Don't get caught up in delight in sensuality.

358
00:18:10,000 --> 00:18:14,000
 This is what the Buddha says because sensuality is really

359
00:18:14,000 --> 00:18:14,000
 that.

360
00:18:14,000 --> 00:18:17,000
 The worst of them.

361
00:18:17,000 --> 00:18:19,000
 It's the most coarse of our desires.

362
00:18:19,000 --> 00:18:22,590
 We may have ambitions or so on, but our desire for sens

363
00:18:22,590 --> 00:18:23,000
uality,

364
00:18:23,000 --> 00:18:27,000
 sights and sounds and smells and tastes and feelings,

365
00:18:27,000 --> 00:18:30,620
 is the worst because it involves the body and it involves

366
00:18:30,620 --> 00:18:33,000
 chemicals and hormones and so on

367
00:18:33,000 --> 00:18:36,760
 and dopamine in the brain and so on and this addiction

368
00:18:36,760 --> 00:18:37,000
 cycle,

369
00:18:37,000 --> 00:18:43,000
 which, as I said, is actually creating what we call sankha,

370
00:18:43,000 --> 00:18:43,000
 right?

371
00:18:43,000 --> 00:18:46,000
 It's forming our reality.

372
00:18:46,000 --> 00:18:49,000
 It's changing who we are from the very core.

373
00:18:49,000 --> 00:18:51,000
 It's not us doing this.

374
00:18:51,000 --> 00:18:53,000
 This doing becomes us.

375
00:18:53,000 --> 00:18:56,000
 This is who we become.

376
00:18:56,000 --> 00:19:00,000
 And he says, "Why should you follow what I say?

377
00:19:00,000 --> 00:19:02,710
 Why should you not be negligent and why should you not

378
00:19:02,710 --> 00:19:05,000
 indulge in delight in sensuality?"

379
00:19:05,000 --> 00:19:08,000
 He says, "Because it's only the apamato,

380
00:19:08,000 --> 00:19:14,000
 only by being heedful, giant and meditating,

381
00:19:14,000 --> 00:19:17,000
 does one attain bapoti,

382
00:19:17,000 --> 00:19:21,000
 wipulang sukhang, the profoundest happiness."

383
00:19:21,000 --> 00:19:25,000
 So the happiness here is the happiness of peace.

384
00:19:25,000 --> 00:19:27,000
 The Buddha said, "Nati santi parang sukhang."

385
00:19:27,000 --> 00:19:30,000
 There is no happiness without peace.

386
00:19:30,000 --> 00:19:32,690
 The happinesses that we call happiness, the pleasures in

387
00:19:32,690 --> 00:19:33,000
 the world,

388
00:19:33,000 --> 00:19:38,890
 the things that we say are our happiness or our way to find

389
00:19:38,890 --> 00:19:40,000
 true satisfaction,

390
00:19:40,000 --> 00:19:43,000
 are not real happiness because they're impermanent.

391
00:19:43,000 --> 00:19:45,000
 They change. They're dependent.

392
00:19:45,000 --> 00:19:50,000
 They require some attainment that is also impermanent.

393
00:19:50,000 --> 00:19:53,000
 They require an object and they create partiality.

394
00:19:53,000 --> 00:19:56,330
 They create their opposite, which is suffering and

395
00:19:56,330 --> 00:19:58,000
 dissatisfaction.

396
00:19:58,000 --> 00:20:02,450
 And so the more one indulges, the more one goes back and

397
00:20:02,450 --> 00:20:03,000
 forth

398
00:20:03,000 --> 00:20:05,000
 between one and the other, pleasure and pain.

399
00:20:05,000 --> 00:20:08,830
 And when it's never at peace, one is never able to just be

400
00:20:08,830 --> 00:20:10,000
 here and now.

401
00:20:10,000 --> 00:20:14,570
 One's life is always about seeking after this, running away

402
00:20:14,570 --> 00:20:15,000
 from that,

403
00:20:15,000 --> 00:20:18,090
 seeking, running, seeking, running, being pushed and pulled

404
00:20:18,090 --> 00:20:19,000
 back and forth.

405
00:20:19,000 --> 00:20:21,000
 And one can never just be here and now.

406
00:20:21,000 --> 00:20:29,160
 One never has the ability or the safety, the stability of

407
00:20:29,160 --> 00:20:31,000
 just being here and now.

408
00:20:31,000 --> 00:20:34,270
 They can build up some kind of pleasant state of being for

409
00:20:34,270 --> 00:20:35,000
 some time

410
00:20:35,000 --> 00:20:38,000
 and then see it washed away in the end.

411
00:20:38,000 --> 00:20:40,800
 At the very end, when they die, they will be in a state of

412
00:20:40,800 --> 00:20:41,000
 loss

413
00:20:41,000 --> 00:20:44,270
 because of their attachments, because of the things that

414
00:20:44,270 --> 00:20:46,000
 they still are clinging to.

415
00:20:46,000 --> 00:20:48,860
 So the profound happiness that the Buddha is talking about

416
00:20:48,860 --> 00:20:50,000
 is peace here and now,

417
00:20:50,000 --> 00:20:53,980
 being able to be in the here and now without any clinging

418
00:20:53,980 --> 00:20:55,000
 or any attachment.

419
00:20:55,000 --> 00:20:57,000
 So that's the teaching today.

420
00:20:57,000 --> 00:21:00,000
 That's Dhammapada verses 26 and 27.

421
00:21:00,000 --> 00:21:01,000
 Thank you for tuning in.

422
00:21:01,000 --> 00:21:04,190
 I wish you all to find peace, happiness and freedom from

423
00:21:04,190 --> 00:21:05,000
 suffering.

424
00:21:05,000 --> 00:21:06,000
 Thank you.

425
00:21:06,000 --> 00:21:07,000
 Thank you.

