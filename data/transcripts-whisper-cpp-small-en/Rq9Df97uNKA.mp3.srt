1
00:00:00,000 --> 00:00:02,000
 This day in the year

2
00:00:02,000 --> 00:00:11,000
 What's so special about this day? Well, look up to the sky

3
00:00:11,000 --> 00:00:15,000
 See that the board is almost full

4
00:00:15,000 --> 00:00:18,000
 It's going to be full tonight

5
00:00:18,000 --> 00:00:28,000
 So this is the board

6
00:00:30,000 --> 00:00:32,000
 That makes it special in the way

7
00:00:32,000 --> 00:00:36,000
 But what's special about this board is that on this day

8
00:00:36,000 --> 00:00:42,000
 The board is having travels for too much

9
00:00:42,000 --> 00:00:50,000
 From

10
00:00:53,000 --> 00:00:59,000
 Gaya

11
00:00:59,000 --> 00:01:09,000
 So on the board he left his five disciples

12
00:01:09,000 --> 00:01:12,000
 When they left him

13
00:01:12,000 --> 00:01:16,000
 They said they were going to follow after him anymore

14
00:01:17,000 --> 00:01:24,000
 The board went to Gaya and gave him the name

15
00:01:24,000 --> 00:01:32,000
 And he sat down under a train and practiced meditation

16
00:01:32,000 --> 00:01:35,690
 He came to understand many things between now and the

17
00:01:35,690 --> 00:01:36,000
 evening

18
00:01:36,000 --> 00:01:41,000
 The normal truth

19
00:01:43,000 --> 00:01:47,000
 He got completely the phone from the flight attendant

20
00:01:47,000 --> 00:01:52,000
 And what he realized was that

21
00:01:52,000 --> 00:01:57,000
 There is a cause for suffering

22
00:01:57,000 --> 00:02:03,730
 And he realized what that cause was and he gave up that

23
00:02:03,730 --> 00:02:04,000
 cause

24
00:02:04,000 --> 00:02:07,000
 He understood the truth of suffering

25
00:02:11,000 --> 00:02:14,000
 And then he traveled this

26
00:02:14,000 --> 00:02:19,000
 120 miles

27
00:02:19,000 --> 00:02:23,000
 And

