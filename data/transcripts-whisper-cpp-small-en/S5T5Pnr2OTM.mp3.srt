1
00:00:00,000 --> 00:00:03,070
 Is it necessary to have an enlightened teacher or anyone

2
00:00:03,070 --> 00:00:05,300
 who has reached a certain level can be a teacher?

3
00:00:05,300 --> 00:00:09,950
 You don't even have to have reached a level. I mean if you

4
00:00:09,950 --> 00:00:12,080
 can parrot what your teachers teach

5
00:00:12,080 --> 00:00:14,800
 I talked about this last week a little bit

6
00:00:14,800 --> 00:00:23,690
 If you take the Visuddhi Maga as as as a guide it says, you

7
00:00:23,690 --> 00:00:24,960
 know anyone can teach

8
00:00:26,080 --> 00:00:28,640
 the more knowledge they have of the Buddha's teaching the

9
00:00:28,640 --> 00:00:30,200
 better they'll be able to teach and

10
00:00:30,200 --> 00:00:33,590
 Even better than having knowledge of the Buddha's teaching

11
00:00:33,590 --> 00:00:35,080
 is having the realizations

12
00:00:35,080 --> 00:00:38,130
 so the best is to go to the Buddha then the bet then go to

13
00:00:38,130 --> 00:00:39,480
 an arahant and go to an

14
00:00:39,480 --> 00:00:42,960
 Anagami than a sakiragami than a sotapana

15
00:00:42,960 --> 00:00:46,120
 If you can't even find a sotapana then go to someone who

16
00:00:46,120 --> 00:00:48,000
 has memorized the whole to Pitaka

17
00:00:48,000 --> 00:00:50,960
 If you can't find such a person go to someone who is memor

18
00:00:50,960 --> 00:00:53,140
ized to two pitaka's one Pitaka

19
00:00:53,560 --> 00:00:55,920
 Has a good knowledge of the Pitaka's and so on

20
00:00:55,920 --> 00:01:00,360
 But there's a story that we give of this monk who

21
00:01:00,360 --> 00:01:04,920
 Taught all of his students and they all became enlightened

22
00:01:04,920 --> 00:01:08,780
 and he himself was an ordinary worldling actually the Vis

23
00:01:08,780 --> 00:01:09,900
uddhi Maga has a few such

24
00:01:09,900 --> 00:01:11,440
 monks

25
00:01:11,440 --> 00:01:13,200
 but this one

26
00:01:13,200 --> 00:01:15,390
 He thought he was a great teacher and he was totally

27
00:01:15,390 --> 00:01:18,180
 oblivious to the fact that he hadn't gained anything until

28
00:01:18,180 --> 00:01:18,840
 one day one of

29
00:01:18,840 --> 00:01:21,960
 His Anagami students came in and said to him. Hey

30
00:01:22,720 --> 00:01:25,620
 How about a lesson today and he said oh, I'm too busy today

31
00:01:25,620 --> 00:01:28,520
. My schedule is full and he said no

32
00:01:28,520 --> 00:01:30,880
 No, I didn't I didn't mean you giving me a lesson

33
00:01:30,880 --> 00:01:34,320
 But here's a lesson for you and he said he sat cross-legged

34
00:01:34,320 --> 00:01:36,840
 and floated up into the air and said

35
00:01:36,840 --> 00:01:39,710
 You don't even have time for yourself. How can you give

36
00:01:39,710 --> 00:01:42,160
 lessons to other people and he flew out the window?

37
00:01:42,160 --> 00:01:44,160
 He said I don't want a lesson from you

38
00:01:44,160 --> 00:01:45,560
 and

39
00:01:45,560 --> 00:01:48,440
 and you know this really you should perturb the

40
00:01:48,920 --> 00:01:52,270
 The worldling monk teacher and he went off to practice

41
00:01:52,270 --> 00:01:54,960
 meditation and eventually became an hour

42
00:01:54,960 --> 00:01:59,240
 So

43
00:01:59,240 --> 00:02:02,840
 There's no no need to be now that the story goes on

44
00:02:02,840 --> 00:02:04,840
 actually I should finish the story for those of you who

45
00:02:04,840 --> 00:02:05,400
 haven't heard it

46
00:02:05,400 --> 00:02:07,520
 He went off into the forest

47
00:02:07,520 --> 00:02:10,800
 Where there was nobody he said he said in that case

48
00:02:10,800 --> 00:02:14,970
 This monk is correct and or he was just horrified by the

49
00:02:14,970 --> 00:02:16,840
 fact that geez here

50
00:02:16,840 --> 00:02:19,530
 My students are totally enlightened and they can read my

51
00:02:19,530 --> 00:02:20,880
 minds and know that I'm not

52
00:02:20,880 --> 00:02:23,680
 How can I possibly stay or he was ashamed?

53
00:02:23,680 --> 00:02:27,160
 And so he went off into the forest where there was no one

54
00:02:27,160 --> 00:02:30,440
 and he just practiced on his own and he did such strenuous

55
00:02:30,440 --> 00:02:32,680
 walking meditation

56
00:02:32,680 --> 00:02:35,440
 Pushing himself really hard and this is what happens when

57
00:02:35,440 --> 00:02:38,000
 people learn too much because they have they feel these

58
00:02:38,000 --> 00:02:40,970
 expectations on them and they have this desire to become a

59
00:02:40,970 --> 00:02:42,320
 lady's expectations and

60
00:02:43,240 --> 00:02:46,180
 Pushing themselves to become a light which is hopeless. It

61
00:02:46,180 --> 00:02:47,880
's it's a ton. Oh, it's itself

62
00:02:47,880 --> 00:02:50,800
 meditation has to be

63
00:02:50,800 --> 00:02:54,360
 Natural it's about coming back to nature

64
00:02:54,360 --> 00:02:57,240
 So the walking and the knowledge of the walking has to come

65
00:02:57,240 --> 00:02:58,680
 naturally slowly slowly

66
00:02:58,680 --> 00:03:03,180
 you know not pushing but probing and and nudging the mind

67
00:03:03,180 --> 00:03:03,520
 and

68
00:03:03,520 --> 00:03:06,280
 Easing the mind into place. You can't you know

69
00:03:06,280 --> 00:03:09,680
 Force the square pegs to fit into the round holes

70
00:03:11,600 --> 00:03:14,710
 and so he got nowhere and he was totally he wound up his

71
00:03:14,710 --> 00:03:17,680
 clock and eventually he it sprung and he got

72
00:03:17,680 --> 00:03:22,410
 He got fed up and he sat down and totally overwhelmed and

73
00:03:22,410 --> 00:03:24,040
 he started crying

74
00:03:24,040 --> 00:03:28,190
 And as he started crying suddenly this angel appeared

75
00:03:28,190 --> 00:03:29,080
 beside him

76
00:03:29,080 --> 00:03:32,460
 Sat down beside him and started crying as well and he looks

77
00:03:32,460 --> 00:03:33,840
 over and there's here's this angel

78
00:03:33,840 --> 00:03:36,410
 Because of course, there's a forest with nobody in it.

79
00:03:36,410 --> 00:03:38,520
 There's this angel sitting crying beside him and he's like

80
00:03:38,520 --> 00:03:40,800
 Who are you?

81
00:03:40,800 --> 00:03:43,620
 That stops crying and he says I'm an angel that lives in

82
00:03:43,620 --> 00:03:45,040
 this forest. What are you doing here?

83
00:03:45,040 --> 00:03:48,060
 Well, I knew you were a great meditation teacher. So I

84
00:03:48,060 --> 00:03:49,760
 wanted to come and practice according to you

85
00:03:49,760 --> 00:03:53,080
 According to your teachings. Well, why are you crying? I

86
00:03:53,080 --> 00:03:56,950
 Saw you crying so I figured about that if he's crying. He's

87
00:03:56,950 --> 00:03:59,120
 a great teacher must be the way to become enlightened

88
00:03:59,120 --> 00:04:01,640
 so the guy's like oh and

89
00:04:01,640 --> 00:04:03,600
 He you know again

90
00:04:03,600 --> 00:04:07,240
 Totally ashamed of himself and but but crying like that can

91
00:04:07,240 --> 00:04:09,400
 be useful sometimes not the crying itself

92
00:04:09,400 --> 00:04:12,180
 But the giving up the feeling how hopeless it is because

93
00:04:12,180 --> 00:04:14,040
 that's the realization of non-self

94
00:04:14,040 --> 00:04:17,600
 The realization that you can't control and that's what

95
00:04:17,600 --> 00:04:20,760
 allows you to finally give up and really start practicing

96
00:04:20,760 --> 00:04:22,950
 some people have to reach that again and again and again

97
00:04:22,950 --> 00:04:25,320
 like banging your head against the wall until you realize

98
00:04:25,320 --> 00:04:27,520
 how futile it is and

99
00:04:27,520 --> 00:04:30,890
 So this is what he did and after that then he became

100
00:04:30,890 --> 00:04:31,880
 enlightened

101
00:04:31,880 --> 00:04:36,930
 So this story is a really good example of how this works.

102
00:04:36,930 --> 00:04:39,120
 You don't need to be enlightened to teach

103
00:04:39,400 --> 00:04:42,510
 But that's not saying anything. That's not it's not saying

104
00:04:42,510 --> 00:04:45,200
 great sign me up. I'll become a great teacher

105
00:04:45,200 --> 00:04:48,560
 And saying don't be as I said last week

106
00:04:48,560 --> 00:04:51,060
 Don't be too attached to the idea of being a good teacher

107
00:04:51,060 --> 00:04:52,720
 or this person being a good teacher

108
00:04:52,720 --> 00:04:55,720
 It doesn't mean nearly as much it doesn't mean

109
00:04:55,720 --> 00:05:00,360
 Quite as much anyway as we think you know

110
00:05:00,360 --> 00:05:03,240
 There are great teachers outside of Buddhism who are able

111
00:05:03,240 --> 00:05:05,360
 to convince their students of their views

112
00:05:07,280 --> 00:05:09,280
 Now of course meditation

113
00:05:09,280 --> 00:05:12,050
 Having its beneficial side effects allows you to be a

114
00:05:12,050 --> 00:05:12,920
 better teacher

115
00:05:12,920 --> 00:05:16,200
 But it's not required to be a good teacher

116
00:05:16,200 --> 00:05:23,800
 The the I guess the only proviso is

117
00:05:23,800 --> 00:05:26,860
 whichever was probably going through everyone's mind is

118
00:05:26,860 --> 00:05:30,080
 that without the meditation practice and without meditative

119
00:05:30,080 --> 00:05:33,960
 Realizations, it's incredibly dangerous, isn't it? No, it's

120
00:05:33,960 --> 00:05:36,780
 not dangerous if the person goes by the Buddhist teaching

121
00:05:36,780 --> 00:05:40,090
 But if the person still is a putu jana a person with full

122
00:05:40,090 --> 00:05:41,900
 greed anger and delusion

123
00:05:41,900 --> 00:05:44,920
 How can we be sure they're going to go according to the tep

124
00:05:44,920 --> 00:05:45,360
idika?

125
00:05:45,360 --> 00:05:47,710
 How can we be sure they're going to go so what generally

126
00:05:47,710 --> 00:05:50,030
 happens people throw out the commentaries throw out

127
00:05:50,030 --> 00:05:51,440
 different suttas throw out?

128
00:05:51,440 --> 00:05:54,240
 various and you know

129
00:05:54,240 --> 00:05:57,830
 Conflicting ideas take a part of the tepidika interpret it

130
00:05:57,830 --> 00:06:01,340
 in their own way and teach some sort of lopsided Dhamma

131
00:06:01,340 --> 00:06:05,880
 Instead of going by the accepted

132
00:06:06,720 --> 00:06:09,730
 Interpretation this is this is more much more common people

133
00:06:09,730 --> 00:06:12,620
 are much more inclined to do this based on their own

134
00:06:12,620 --> 00:06:15,160
 views and opinions and ideas

135
00:06:15,160 --> 00:06:19,480
 This is a sign of the noble people from what I've seen

136
00:06:19,480 --> 00:06:22,560
 if you're asking me and

137
00:06:22,560 --> 00:06:25,400
 From from you are asking me

138
00:06:25,400 --> 00:06:30,910
 The most noble thing that I've seen and the most noble

139
00:06:30,910 --> 00:06:35,320
 people that I've seen from my point of view as being noble

140
00:06:35,720 --> 00:06:37,720
 you know from my

141
00:06:37,720 --> 00:06:41,270
 Observation the people that I have seen and feel that I

142
00:06:41,270 --> 00:06:43,600
 feel are the most noble and enlightened

143
00:06:43,600 --> 00:06:46,810
 Turn out to be the people who have the least opinions will

144
00:06:46,810 --> 00:06:48,140
 give their their

145
00:06:48,140 --> 00:06:54,920
 Observations when they say well, this is kind of like Maha

146
00:06:54,920 --> 00:06:57,320
 Where he would point out and say well, you know, this is

147
00:06:57,320 --> 00:06:58,120
 kind of conflicting

148
00:06:58,120 --> 00:07:01,860
 So we're really not sure here and it may be that there is a

149
00:07:01,860 --> 00:07:04,360
 mistake here. He would say that but

150
00:07:04,920 --> 00:07:06,520
 otherwise

151
00:07:06,520 --> 00:07:08,780
 almost always he would go according to the

152
00:07:08,780 --> 00:07:12,310
 Topitika the commentaries the Visuddhi Maga all of this

153
00:07:12,310 --> 00:07:15,200
 without interpretation without trying to change it

154
00:07:15,200 --> 00:07:19,140
 He would try his best to fit his meditation into that, you

155
00:07:19,140 --> 00:07:20,840
 know as best he could and while still

156
00:07:20,840 --> 00:07:24,250
 You know going according to the observation then from my

157
00:07:24,250 --> 00:07:26,400
 point of view from what I've seen that seems to work

158
00:07:26,400 --> 00:07:28,360
 so it

159
00:07:28,360 --> 00:07:30,640
 takes a certain amount of enlightenment

160
00:07:30,640 --> 00:07:34,100
 I think to do that to give up your own views and opinions

161
00:07:34,100 --> 00:07:35,040
 and to just go

162
00:07:35,040 --> 00:07:38,360
 according to the standard interpretation which

163
00:07:38,360 --> 00:07:42,200
 You know from my boy from what I've seen turns out to be

164
00:07:42,200 --> 00:07:49,280
 You know perfect but a good enough shell to fit the perfect

165
00:07:49,280 --> 00:07:52,040
 What might be the perfect meditation into it?

166
00:07:52,040 --> 00:07:56,010
 So there may be times where you have to kind of finesse it

167
00:07:56,010 --> 00:07:57,760
 and kind of say well, you know

168
00:07:58,000 --> 00:08:00,420
 Don't take that too seriously or that's just one

169
00:08:00,420 --> 00:08:03,970
 interpretation which oftentimes especially the commentaries

170
00:08:03,970 --> 00:08:04,200
 are

171
00:08:04,200 --> 00:08:09,680
 But why I

172
00:08:09,680 --> 00:08:13,850
 Go we don't go to this this depth to explain is because the

173
00:08:13,850 --> 00:08:15,440
 danger of not having

174
00:08:15,440 --> 00:08:18,920
 Enlightenment is that you will misinterpret the teachings

175
00:08:18,920 --> 00:08:24,240
 one two that you will misuse the teachings so

176
00:08:25,200 --> 00:08:29,330
 many many examples of this where a teacher becomes gets a

177
00:08:29,330 --> 00:08:30,600
 guru complex and

178
00:08:30,600 --> 00:08:33,280
 begins to

179
00:08:33,280 --> 00:08:35,840
 You know encourage this

180
00:08:35,840 --> 00:08:39,320
 Attachment to their person and

181
00:08:39,320 --> 00:08:43,760
 Taking them as refuge and so on like that

182
00:08:43,760 --> 00:08:50,480
 Even to the point of you know abusing it as far as

183
00:08:50,480 --> 00:08:54,360
 Taking advantage of one's students

184
00:08:55,000 --> 00:08:59,170
 We take advantage people we these sorts of teachers who I I

185
00:08:59,170 --> 00:09:00,640
 claim I am not one

186
00:09:00,640 --> 00:09:05,720
 Will try to get money from their students for example is

187
00:09:05,720 --> 00:09:09,360
 very common thing to do but less common

188
00:09:09,360 --> 00:09:11,960
 But that does still happen is they will

189
00:09:11,960 --> 00:09:15,190
 And sometimes it seems you know, they're just unable to

190
00:09:15,190 --> 00:09:17,560
 control themselves, but they'll actually have

191
00:09:17,560 --> 00:09:20,610
 Relations with their with their students, you know, they'll

192
00:09:20,610 --> 00:09:23,280
 start to have sexual or romantic relations with their

193
00:09:23,280 --> 00:09:23,800
 students

194
00:09:23,800 --> 00:09:26,790
 Still trying to be a meditation teacher and at the same

195
00:09:26,790 --> 00:09:27,120
 time

196
00:09:27,120 --> 00:09:30,140
 Fooling around with their students because they just can't

197
00:09:30,140 --> 00:09:31,240
 control themselves

198
00:09:31,240 --> 00:09:34,660
 so these are the disadvantages best thing is to find an

199
00:09:34,660 --> 00:09:36,180
 enlightened being but

200
00:09:36,180 --> 00:09:40,410
 Doesn't mean that we should denigrate a person who is not

201
00:09:40,410 --> 00:09:42,680
 enlightened but is able to teach and

202
00:09:42,680 --> 00:09:46,480
 More importantly doesn't it means that we should not

203
00:09:49,440 --> 00:09:51,500
 We should not consider just because someone is a good

204
00:09:51,500 --> 00:09:53,810
 teacher means that they're enlightened just because they

205
00:09:53,810 --> 00:09:55,600
 talk like an enlightened being

206
00:09:55,600 --> 00:09:59,490
 Because when I first started teaching it was kind of

207
00:09:59,490 --> 00:10:01,900
 embarrassing, but it's also you know, it's kind of

208
00:10:01,900 --> 00:10:02,400
 encouraging

209
00:10:02,400 --> 00:10:04,800
 encourages us

210
00:10:04,800 --> 00:10:08,400
 As new teachers said I was able to sound like a gentleman

211
00:10:08,400 --> 00:10:09,440
 so when I my teacher

212
00:10:09,440 --> 00:10:12,600
 So when I talked and when I taught in Thai I would speak

213
00:10:12,600 --> 00:10:15,680
 and and have the same nuances that he had

214
00:10:17,800 --> 00:10:21,350
 So and it worked it did make me a good teacher fairly

215
00:10:21,350 --> 00:10:24,080
 quickly. It doesn't make me a good person

216
00:10:24,080 --> 00:10:26,080
 It doesn't make me an enlightened person

217
00:10:26,080 --> 00:10:29,000
 But

218
00:10:29,000 --> 00:10:31,000
 you know it really

219
00:10:31,000 --> 00:10:35,050
 You could say even it fooled my students and you you could

220
00:10:35,050 --> 00:10:37,220
 say that probably some of my students were fooled into

221
00:10:37,220 --> 00:10:38,040
 thinking that I was

222
00:10:38,040 --> 00:10:40,000
 more

223
00:10:40,000 --> 00:10:42,990
 More than I was thinking maybe he's in our heart. There was

224
00:10:42,990 --> 00:10:43,680
 actually one man who

225
00:10:44,520 --> 00:10:47,600
 That's maybe a different story, but there was one man who

226
00:10:47,600 --> 00:10:50,000
 thought I had told him that I was an Arahant

227
00:10:50,000 --> 00:10:52,730
 I mean he misunderstood what I had said. I think it's a

228
00:10:52,730 --> 00:10:53,440
 different story

229
00:10:53,440 --> 00:10:55,840
 but

230
00:10:55,840 --> 00:10:57,840
 You know at the very least I would say

231
00:10:57,840 --> 00:11:00,960
 Try your best to get a

232
00:11:00,960 --> 00:11:03,760
 an enlightened teacher

233
00:11:03,760 --> 00:11:06,040
 But don't be too concerned about it

234
00:11:06,040 --> 00:11:10,240
 Be concerned that they're going according to the tradition

235
00:11:10,880 --> 00:11:13,670
 Both from the Buddha and the commentaries. You know people

236
00:11:13,670 --> 00:11:15,800
 are going to argue with that

237
00:11:15,800 --> 00:11:18,430
 There's a lot of people who will immediately be turned off

238
00:11:18,430 --> 00:11:21,160
 by the idea of going with any sort of commentaries

239
00:11:21,160 --> 00:11:23,280
 As they prefer to make their own commentaries

240
00:11:23,280 --> 00:11:28,440
 But you know that's my suggestion and

241
00:11:28,440 --> 00:11:35,080
 Don't be but on the other hand once you find a good teacher

242
00:11:35,080 --> 00:11:37,680
 Don't be too sure that they're going to always be in line

243
00:11:37,680 --> 00:11:40,520
 with the suit doesn't the Buddha's teaching

244
00:11:40,520 --> 00:11:46,290
 because they may not be enlightened even though they might

245
00:11:46,290 --> 00:11:46,800
 be able to teach

246
00:11:46,800 --> 00:11:48,800
 you

