1
00:00:00,000 --> 00:00:04,560
 - Masaki asked, "How does one describe that which is indesc

2
00:00:04,560 --> 00:00:08,000
ribable, discovered through meditation?

3
00:00:08,000 --> 00:00:11,310
 How, for example, do I describe things which extend beyond

4
00:00:11,310 --> 00:00:15,000
 the barrier of language, art and visuals to people?

5
00:00:15,000 --> 00:00:19,530
 I wish to help people even though I'm suffering. I see many

6
00:00:19,530 --> 00:00:21,000
 people who are suffering." - Not suffering.

7
00:00:21,000 --> 00:00:24,000
 - I wish you'd correct it underneath. - Cool.

8
00:00:24,000 --> 00:00:27,330
 "I see many people who are suffering and it pains me that

9
00:00:27,330 --> 00:00:30,490
 there are things I have learned in meditation that I cannot

10
00:00:30,490 --> 00:00:31,000
 describe.

11
00:00:31,000 --> 00:00:34,000
 I just want to alleviate suffering."

12
00:00:43,000 --> 00:00:50,870
 By example, if you can't put it in words, do it. Be an

13
00:00:50,870 --> 00:00:57,330
 example. That is probably, anyway, in any case, better than

14
00:00:57,330 --> 00:00:58,000
 words.

15
00:00:58,000 --> 00:01:05,010
 So what you have experienced is not really important to be

16
00:01:05,010 --> 00:01:08,000
 told to other people.

17
00:01:08,000 --> 00:01:13,260
 They don't need to know the words. They don't need to know

18
00:01:13,260 --> 00:01:16,000
 what exactly you have experienced.

19
00:01:16,000 --> 00:01:21,320
 But what would help them is that if your mind has become

20
00:01:21,320 --> 00:01:26,000
 clear and if you have become more peaceful,

21
00:01:26,000 --> 00:01:30,540
 then to share that with the people, then to share your

22
00:01:30,540 --> 00:01:34,000
 clearness and maybe a joy that you gained,

23
00:01:34,000 --> 00:01:39,370
 to share that. Be generous with it and spread it around the

24
00:01:39,370 --> 00:01:40,000
 world.

25
00:01:40,000 --> 00:01:46,000
 And just don't use words.

26
00:01:46,000 --> 00:01:49,000
 Very good. Very well said.

27
00:01:49,000 --> 00:01:55,350
 And another thing to share with them is there's this funny

28
00:01:55,350 --> 00:01:58,000
 children's documentary of Buddhism.

29
00:01:58,000 --> 00:02:04,000
 And it shows the Buddha coming back to the five ascetics.

30
00:02:04,000 --> 00:02:07,320
 And at first, of course, they don't want to take his broban

31
00:02:07,320 --> 00:02:09,000
bulb, but then they do it.

32
00:02:09,000 --> 00:02:16,000
 And it really made me smile to watch this because he says,

33
00:02:16,000 --> 00:02:22,000
 "Oh monks, I have realized the truth and found..."

34
00:02:22,000 --> 00:02:27,510
 He gives this profound exposition of, "I have realized the

35
00:02:27,510 --> 00:02:29,440
 four noble truths and I have come to be free from all

36
00:02:29,440 --> 00:02:30,000
 suffering."

37
00:02:30,000 --> 00:02:33,000
 And then they reply, "How did you do it?"

38
00:02:33,000 --> 00:02:38,990
 Just kind of like he was saying, he had learned some new

39
00:02:38,990 --> 00:02:43,000
 skill or something very simple.

40
00:02:43,000 --> 00:02:46,000
 "How did you do it, Venerable Sir?"

41
00:02:46,000 --> 00:02:50,610
 But this is really a good point because it's really what

42
00:02:50,610 --> 00:02:52,000
 people want.

43
00:02:52,000 --> 00:02:57,610
 What you need to give them is not, as Balanjani said, what

44
00:02:57,610 --> 00:02:58,000
 you've gained from the practice.

45
00:02:58,000 --> 00:03:02,000
 But by showing them that, it's an excellent example.

46
00:03:02,000 --> 00:03:05,820
 Once they see the example, what they want is, "How can I

47
00:03:05,820 --> 00:03:07,000
 get that?"

48
00:03:07,000 --> 00:03:10,020
 And you should at least be able to explain the practices

49
00:03:10,020 --> 00:03:11,000
 that you've done.

50
00:03:11,000 --> 00:03:14,460
 If your practices have led you to peace, happiness and

51
00:03:14,460 --> 00:03:19,000
 freedom from suffering, then explain to them the practice.

52
00:03:19,000 --> 00:03:21,000
 He says, "I mean if you read..."

53
00:03:21,000 --> 00:03:23,840
 That was my thinking when I made this booklet, this booklet

54
00:03:23,840 --> 00:03:25,000
 on how to meditate.

55
00:03:25,000 --> 00:03:30,810
 It doesn't teach much theory at all. It gives you the basic

56
00:03:30,810 --> 00:03:32,000
 how-to.

57
00:03:32,000 --> 00:03:34,710
 And the reason why I started making YouTube videos at all

58
00:03:34,710 --> 00:03:38,000
 was not to put teachings out there actually,

59
00:03:38,000 --> 00:03:41,670
 but to put the how-to guide out there, something so that

60
00:03:41,670 --> 00:03:43,000
 you yourself don't have to learn theory,

61
00:03:43,000 --> 00:03:46,900
 don't have to learn what is Nibbana like or what does it

62
00:03:46,900 --> 00:03:49,000
 mean to be enlightened.

63
00:03:49,000 --> 00:03:55,300
 You have to learn what are the practical steps that I can

64
00:03:55,300 --> 00:03:57,000
 take to reduce my suffering,

65
00:03:57,000 --> 00:04:03,040
 to increase my peace and to become free from defilement of

66
00:04:03,040 --> 00:04:04,000
 mind.

67
00:04:04,000 --> 00:04:08,000
 So the practice I think is something that you should share

68
00:04:08,000 --> 00:04:09,000
 with them.

