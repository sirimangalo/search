1
00:00:00,000 --> 00:00:05,040
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,040 --> 00:00:10,040
 Today we continue on with verse number 127, which reads as

3
00:00:10,040 --> 00:00:11,560
 follows.

4
00:00:11,560 --> 00:00:30,000
 Nāntadike na samudamājhe na pabhata nāngvivarāng pavī

5
00:00:30,000 --> 00:00:32,880
sa nāvijati sa jagati pade sa yatatthi to mujjaya pāpakam

6
00:00:32,880 --> 00:00:33,160
ā

7
00:00:33,160 --> 00:00:51,600
 which means not up in the sky or in the middle of the ocean

8
00:00:51,600 --> 00:00:51,720
.

9
00:00:51,720 --> 00:01:02,690
 Nāntadike na samudamājhe na pabhata nāngvivarāng pavī

10
00:01:02,690 --> 00:01:07,720
sa, not having entered into a cave,

11
00:01:07,720 --> 00:01:10,610
 a cavern in a mountain, entered into the middle of a

12
00:01:10,610 --> 00:01:14,280
 mountain, the center of a mountain.

13
00:01:14,280 --> 00:01:25,660
 Nāvijati sa jagati pade sa it cannot be found such a place

14
00:01:25,660 --> 00:01:27,760
 on this earth.

15
00:01:27,760 --> 00:01:32,680
 One cannot find a place on this earth, on the earth.

16
00:01:32,680 --> 00:01:40,620
 Yatatthi to where standing, mujjaya pāpakamā. One might

17
00:01:40,620 --> 00:01:45,640
 be free from evil karma, evil deeds.

18
00:01:45,640 --> 00:01:47,360
 There's nowhere.

19
00:01:47,360 --> 00:01:54,360
 Not up in the sky, not under the ocean, not in the deepest,

20
00:01:54,360 --> 00:01:56,320
 darkest cave.

21
00:01:56,320 --> 00:01:58,040
 There's nowhere on earth.

22
00:01:58,040 --> 00:02:02,440
 That's the quote.

23
00:02:02,440 --> 00:02:08,470
 So an interesting story, actually three stories, that goes

24
00:02:08,470 --> 00:02:10,280
 with this verse.

25
00:02:10,280 --> 00:02:18,050
 It seems there were three groups of monks, and so we have

26
00:02:18,050 --> 00:02:20,600
 three stories.

27
00:02:20,600 --> 00:02:27,470
 The Buddha was living in Jaitawana, but a first group of

28
00:02:27,470 --> 00:02:30,360
 monks set out to meet the Buddha,

29
00:02:30,360 --> 00:02:33,620
 and on their way they entered a village, a certain village

30
00:02:33,620 --> 00:02:37,440
 for alms, and while they were

31
00:02:37,440 --> 00:02:45,370
 sitting or after they had eaten, or while they were waiting

32
00:02:45,370 --> 00:02:47,200
 for food, someone who was

33
00:02:47,200 --> 00:02:52,990
 cooking food in the morning made their fire too hot, and

34
00:02:52,990 --> 00:02:59,200
 suddenly the flame burst up and

35
00:02:59,200 --> 00:03:05,700
 lit the satch, roof of their hut, and the satch went flying

36
00:03:05,700 --> 00:03:08,080
 up in a ball of flame or

37
00:03:08,080 --> 00:03:15,400
 floating through the air being carried away by the wind.

38
00:03:15,400 --> 00:03:21,880
 At that moment a crow was flying, and so this tuft of satch

39
00:03:21,880 --> 00:03:25,000
 caught the crow and immediately

40
00:03:25,000 --> 00:03:26,480
 burst into flames.

41
00:03:26,480 --> 00:03:30,460
 It was on fire, so the crow burnt to a crisp, fell to the

42
00:03:30,460 --> 00:03:33,040
 ground, dead, right in front of

43
00:03:33,040 --> 00:03:36,040
 the monks.

44
00:03:36,040 --> 00:03:40,970
 And they thought it was kind of remarkable because just one

45
00:03:40,970 --> 00:03:43,080
 tuft of satch flew up and

46
00:03:43,080 --> 00:03:50,440
 caught the crow squarely as it was flying by.

47
00:03:50,440 --> 00:03:54,850
 And so they wondered to themselves, sort of monk talk, "I

48
00:03:54,850 --> 00:03:57,140
 wonder what the karma of this

49
00:03:57,140 --> 00:04:00,160
 bird was that this would happen?"

50
00:04:00,160 --> 00:04:03,080
 And they said, "Who would know besides the Buddha?

51
00:04:03,080 --> 00:04:09,680
 So when we get there we should ask him."

52
00:04:09,680 --> 00:04:11,080
 And so they continued on their way.

53
00:04:11,080 --> 00:04:14,680
 That's the first group.

54
00:04:14,680 --> 00:04:17,050
 A second group of monks also on their way to see the Buddha

55
00:04:17,050 --> 00:04:17,280
.

56
00:04:17,280 --> 00:04:20,550
 I mean, it seems like probably this was a thing where monks

57
00:04:20,550 --> 00:04:21,800
 would come to the Buddha

58
00:04:21,800 --> 00:04:23,360
 with these sorts of questions.

59
00:04:23,360 --> 00:04:26,060
 So I think we shouldn't be surprised that there were three

60
00:04:26,060 --> 00:04:27,000
 groups of them.

61
00:04:27,000 --> 00:04:32,490
 The second group, on their way to see the Buddha, they took

62
00:04:32,490 --> 00:04:34,760
 a boat across the ocean

63
00:04:34,760 --> 00:04:35,760
 somehow.

64
00:04:35,760 --> 00:04:37,160
 Not sure where they were.

65
00:04:37,160 --> 00:04:40,240
 Maybe they were in Burma or Burma's attached.

66
00:04:40,240 --> 00:04:42,200
 Maybe Thailand.

67
00:04:42,200 --> 00:04:43,200
 Maybe they were in Sri Lanka.

68
00:04:43,200 --> 00:04:44,200
 Who knows?

69
00:04:44,200 --> 00:04:45,200
 Probably not.

70
00:04:45,200 --> 00:04:52,100
 It's interesting to think of them being on a ship because

71
00:04:52,100 --> 00:04:55,080
 there was not much record of

72
00:04:55,080 --> 00:04:57,400
 monks outside of India.

73
00:04:57,400 --> 00:05:04,080
 But I'm really not clear about that sort of thing.

74
00:05:04,080 --> 00:05:09,250
 But it happened that in the middle of the ocean their boat

75
00:05:09,250 --> 00:05:10,420
 stopped.

76
00:05:10,420 --> 00:05:12,760
 The wind stopped.

77
00:05:12,760 --> 00:05:14,760
 And they couldn't sail anymore.

78
00:05:14,760 --> 00:05:20,400
 And this was a thing for sailors that if the wind stopped

79
00:05:20,400 --> 00:05:23,400
 for a long period of time they

80
00:05:23,400 --> 00:05:24,400
 would get superstitious.

81
00:05:24,400 --> 00:05:29,070
 And they would think somebody on board is, their karma is

82
00:05:29,070 --> 00:05:31,540
 not allowing us to continue.

83
00:05:31,540 --> 00:05:34,540
 And so they'd throw that person overboard.

84
00:05:34,540 --> 00:05:37,310
 If they could figure out who it was, and the way they did

85
00:05:37,310 --> 00:05:38,960
 it was they found the scientific

86
00:05:38,960 --> 00:05:40,160
 method.

87
00:05:40,160 --> 00:05:43,970
 They'd draw lots and whoever drew the shortest straw would

88
00:05:43,970 --> 00:05:45,720
 get thrown overboard.

89
00:05:45,720 --> 00:05:49,720
 It seems reasonable, no?

90
00:05:49,720 --> 00:05:50,720
 So they did this.

91
00:05:50,720 --> 00:05:54,300
 And we thought whoever gets the shortest straw gets thrown

92
00:05:54,300 --> 00:05:54,880
 over.

93
00:05:54,880 --> 00:05:58,520
 That's just the way karma works.

94
00:05:58,520 --> 00:06:02,600
 I guess.

95
00:06:02,600 --> 00:06:06,920
 Except lo and behold, the captain's wife drew the lot.

96
00:06:06,920 --> 00:06:10,480
 Now the captain's wife was loved by everyone.

97
00:06:10,480 --> 00:06:16,000
 She was young, she was pretty, she was kind, she was great.

98
00:06:16,000 --> 00:06:19,620
 Just all around, not the sort of person you want to throw

99
00:06:19,620 --> 00:06:21,960
 overboard, to say the least.

100
00:06:21,960 --> 00:06:24,080
 Especially since she was the captain's wife.

101
00:06:24,080 --> 00:06:27,760
 And so everyone agreed they couldn't throw her overboard.

102
00:06:27,760 --> 00:06:31,680
 So they said, "Well, we'll draw lots again."

103
00:06:31,680 --> 00:06:36,230
 Then they drew lots and again for a second time the captain

104
00:06:36,230 --> 00:06:38,320
's wife drew the shortest

105
00:06:38,320 --> 00:06:44,120
 straw.

106
00:06:44,120 --> 00:06:46,370
 And they still couldn't, they said there was no way we can

107
00:06:46,370 --> 00:06:46,960
't do this.

108
00:06:46,960 --> 00:06:50,770
 And so a third time they drew straws and a third time the

109
00:06:50,770 --> 00:06:53,280
 captain's wife drew the shortest

110
00:06:53,280 --> 00:06:58,200
 straw.

111
00:06:58,200 --> 00:07:03,860
 So they went to the captain and they said, "This is what

112
00:07:03,860 --> 00:07:06,560
 happened three times.

113
00:07:06,560 --> 00:07:07,560
 It's got to be her.

114
00:07:07,560 --> 00:07:09,560
 She's the one with the bad luck.

115
00:07:09,560 --> 00:07:13,920
 We have to throw her overboard."

116
00:07:13,920 --> 00:07:15,240
 And so they grabbed her.

117
00:07:15,240 --> 00:07:17,900
 Then the captain said, "Well, then yes, I guess that's how

118
00:07:17,900 --> 00:07:18,640
 it has to go.

119
00:07:18,640 --> 00:07:25,050
 It's scientifically shown that she's to blame for the wind

120
00:07:25,050 --> 00:07:25,640
."

121
00:07:25,640 --> 00:07:30,880
 And so he said, "Throw overboard."

122
00:07:30,880 --> 00:07:34,250
 And as they started to throw overboard, she started

123
00:07:34,250 --> 00:07:36,000
 screaming reasonably.

124
00:07:36,000 --> 00:07:40,090
 I mean, she doesn't seem to be that confident in the

125
00:07:40,090 --> 00:07:43,640
 scientific method that they used, but

126
00:07:43,640 --> 00:07:47,720
 she certainly doesn't seem to have wanted to be thrown

127
00:07:47,720 --> 00:07:48,880
 overboard.

128
00:07:48,880 --> 00:07:55,490
 So interestingly, the captain has them take her jewels away

129
00:07:55,490 --> 00:07:55,840
.

130
00:07:55,840 --> 00:07:57,800
 He says, "Well, there's no need to."

131
00:07:57,800 --> 00:08:00,850
 When he heard this, he saw and he said, "Oh, there's no

132
00:08:00,850 --> 00:08:02,760
 need for her jewels to go to waste."

133
00:08:02,760 --> 00:08:07,010
 So he took her jewels and had them wrap her up in a cloth

134
00:08:07,010 --> 00:08:09,440
 and tie a rope around her neck

135
00:08:09,440 --> 00:08:12,730
 so that she couldn't, or wrap her up in a cloth so that she

136
00:08:12,730 --> 00:08:14,000
 wouldn't scream.

137
00:08:14,000 --> 00:08:18,050
 And then tie a rope around her neck and tie it to a big

138
00:08:18,050 --> 00:08:20,480
 heavy pot of sand so that they

139
00:08:20,480 --> 00:08:28,700
 wouldn't see her, so that he wouldn't have to see her

140
00:08:28,700 --> 00:08:33,320
 because he was fond of her.

141
00:08:33,320 --> 00:08:34,320
 And so they threw overboard.

142
00:08:34,320 --> 00:08:38,770
 And as soon as she hit the ocean, sharks and turtles and

143
00:08:38,770 --> 00:08:41,400
 fish and so on ate her, tore her

144
00:08:41,400 --> 00:08:44,240
 to bits and she died.

145
00:08:44,240 --> 00:08:47,200
 The monks on board were watching this and of course didn't

146
00:08:47,200 --> 00:08:48,400
 really have a say in it

147
00:08:48,400 --> 00:08:51,960
 all, but they were shocked as well.

148
00:08:51,960 --> 00:08:56,550
 They couldn't believe that this sort of thing could happen,

149
00:08:56,550 --> 00:08:58,240
 that she could really be at

150
00:08:58,240 --> 00:09:01,740
 the mercy of these people because it was a bit of a

151
00:09:01,740 --> 00:09:04,520
 coincidence that she drew the short

152
00:09:04,520 --> 00:09:05,920
 straw three times.

153
00:09:05,920 --> 00:09:10,690
 It's quite, unless there were only like a few people on

154
00:09:10,690 --> 00:09:14,320
 board, that was quite a coincidence.

155
00:09:14,320 --> 00:09:22,970
 And so they said, "I wonder what karma she did to deserve

156
00:09:22,970 --> 00:09:26,920
 such a horrible fate?"

157
00:09:26,920 --> 00:09:33,760
 And likewise they said, "Well, let's ask the Buddha and

158
00:09:33,760 --> 00:09:35,400
 find out."

159
00:09:35,400 --> 00:09:37,540
 It's important to point out as we go along, because I'm

160
00:09:37,540 --> 00:09:38,800
 sure the question coming up in

161
00:09:38,800 --> 00:09:41,480
 people's minds is, "Well, what about the people who did

162
00:09:41,480 --> 00:09:42,280
 that to her?"

163
00:09:42,280 --> 00:09:46,840
 I mean, it's not karma, it's those people.

164
00:09:46,840 --> 00:09:50,340
 And it has to be mentioned that karma isn't like one thing

165
00:09:50,340 --> 00:09:52,240
 in the past, you blame things

166
00:09:52,240 --> 00:09:53,240
 in your past life.

167
00:09:53,240 --> 00:09:54,680
 That's not true at all.

168
00:09:54,680 --> 00:09:59,080
 Those people who threw that woman into the ocean did a very

169
00:09:59,080 --> 00:10:00,720
, very bad thing.

170
00:10:00,720 --> 00:10:01,720
 There's no question about it.

171
00:10:01,720 --> 00:10:02,720
 That was an evil deed.

172
00:10:02,720 --> 00:10:05,920
 And then Buddhism doesn't condone that.

173
00:10:05,920 --> 00:10:12,810
 But how she got herself in that situation, you see, where

174
00:10:12,810 --> 00:10:16,160
 the likelihood of her being

175
00:10:16,160 --> 00:10:24,190
 subject to that, because these people were not doing it out

176
00:10:24,190 --> 00:10:26,680
 of hate for her.

177
00:10:26,680 --> 00:10:31,200
 They were doing it out of ignorance and superstition.

178
00:10:31,200 --> 00:10:36,520
 But they didn't just randomly pick someone.

179
00:10:36,520 --> 00:10:39,760
 So how did she get herself in that situation?

180
00:10:39,760 --> 00:10:43,280
 The theory is that there's more behind it.

181
00:10:43,280 --> 00:10:45,680
 Our life comes to these points.

182
00:10:45,680 --> 00:10:53,800
 Anyway, that was the second one.

183
00:10:53,800 --> 00:10:59,290
 The third story, there were seven monks who likewise set

184
00:10:59,290 --> 00:11:01,680
 out to see the Buddha.

185
00:11:01,680 --> 00:11:07,800
 And on their way, they came to a certain monastery and they

186
00:11:07,800 --> 00:11:10,660
 asked to stay the night.

187
00:11:10,660 --> 00:11:15,060
 And the seven of them were invited to stay in a special

188
00:11:15,060 --> 00:11:17,680
 cave in the side of the mountain

189
00:11:17,680 --> 00:11:23,200
 that was designated for visiting monks.

190
00:11:23,200 --> 00:11:26,820
 So they went there and they settled down and they fell

191
00:11:26,820 --> 00:11:28,600
 asleep for the night.

192
00:11:28,600 --> 00:11:32,050
 During the night, a huge boulder, it says the size of a pag

193
00:11:32,050 --> 00:11:33,720
oda, which would be very,

194
00:11:33,720 --> 00:11:40,180
 very large, fell down the mountain and covered the entrance

195
00:11:40,180 --> 00:11:43,920
 to the cave where they were staying,

196
00:11:43,920 --> 00:11:52,850
 just out of the blue, blocking their exit and making it

197
00:11:52,850 --> 00:11:58,360
 impossible for them to get out.

198
00:11:58,360 --> 00:12:02,240
 When the resident monks found out what happened, they said,

199
00:12:02,240 --> 00:12:04,280
 "We've got to move that rock.

200
00:12:04,280 --> 00:12:06,320
 There's monks trapped in there."

201
00:12:06,320 --> 00:12:10,830
 And so they gathered men, strong people from all around the

202
00:12:10,830 --> 00:12:13,080
 countryside and they worked

203
00:12:13,080 --> 00:12:19,830
 tirelessly for seven days to remove this rock, but the rock

204
00:12:19,830 --> 00:12:22,080
 wouldn't budge.

205
00:12:22,080 --> 00:12:26,180
 Until finally, on the seventh day, after seven days, the

206
00:12:26,180 --> 00:12:28,480
 rock moved as though it had never

207
00:12:28,480 --> 00:12:30,060
 been stuck there.

208
00:12:30,060 --> 00:12:32,000
 The rock moved very easily.

209
00:12:32,000 --> 00:12:37,990
 I think it even says that it moved by itself away from the

210
00:12:37,990 --> 00:12:39,520
 entrance.

211
00:12:39,520 --> 00:12:46,680
 It suddenly became dislodged.

212
00:12:46,680 --> 00:12:49,810
 And so these seven monks had spent seven days without food,

213
00:12:49,810 --> 00:12:53,440
 without water, were almost dead.

214
00:12:53,440 --> 00:12:57,500
 And yet when they came out, they were able to get water and

215
00:12:57,500 --> 00:12:59,040
 food and survive.

216
00:12:59,040 --> 00:13:02,760
 But they said to themselves, "I wonder what we did.

217
00:13:02,760 --> 00:13:05,880
 It seems a very strange sort of thing to happen.

218
00:13:05,880 --> 00:13:09,800
 I wonder if this is a cause of past karma."

219
00:13:09,800 --> 00:13:12,960
 And so likewise, they decided to ask the Buddha.

220
00:13:12,960 --> 00:13:17,180
 So these three groups of monks met up and this is the story

221
00:13:17,180 --> 00:13:17,240
.

222
00:13:17,240 --> 00:13:22,760
 And then the Buddha tells three stories about their pasts.

223
00:13:22,760 --> 00:13:25,920
 The first, the past of the crow, they go to see the Buddha

224
00:13:25,920 --> 00:13:30,000
 and the Buddha says, "The crow

225
00:13:30,000 --> 00:13:34,680
 is suffering for past deeds."

226
00:13:34,680 --> 00:13:38,350
 And it seems like the story is kind of suggesting that it's

227
00:13:38,350 --> 00:13:40,280
 not just one past deed, but it's

228
00:13:40,280 --> 00:13:43,520
 sort of a habit of bad deeds.

229
00:13:43,520 --> 00:13:45,040
 But he gives examples.

230
00:13:45,040 --> 00:13:50,320
 So he says, "For a long time ago, the crow was a farmer and

231
00:13:50,320 --> 00:13:52,920
 he had an ox and he was trying

232
00:13:52,920 --> 00:13:56,360
 to get this ox to do work for him.

233
00:13:56,360 --> 00:14:01,520
 But try as he might, he couldn't tame the ox.

234
00:14:01,520 --> 00:14:04,140
 He'd get it to work and then it would work for a little bit

235
00:14:04,140 --> 00:14:05,400
 and then it would go lie

236
00:14:05,400 --> 00:14:06,400
 down.

237
00:14:06,400 --> 00:14:12,720
 And then he'd get it to move and it wouldn't move.

238
00:14:12,720 --> 00:14:14,640
 This ox was just terribly, terribly stubborn.

239
00:14:14,640 --> 00:14:19,600
 And so he got increasingly more and more angry until he

240
00:14:19,600 --> 00:14:22,760
 finally got angry enough that the

241
00:14:22,760 --> 00:14:27,840
 ox just lay down, that he just covered it up in straw and

242
00:14:27,840 --> 00:14:29,600
 lit it on fire."

243
00:14:29,600 --> 00:14:33,590
 And the Buddha said, "Because of that evil deed, cruel evil

244
00:14:33,590 --> 00:14:35,080
 deed, he was born in hell

245
00:14:35,080 --> 00:14:36,080
 actually.

246
00:14:36,080 --> 00:14:38,940
 And after being born in hell, he was born back in the human

247
00:14:38,940 --> 00:14:40,360
 realm and he was born back

248
00:14:40,360 --> 00:14:47,660
 in the animal realm as a crow and still suffering from it

249
00:14:47,660 --> 00:14:49,800
 to this day."

250
00:14:49,800 --> 00:14:55,930
 In fact, it says he was seven times in succession, reborn

251
00:14:55,930 --> 00:14:57,240
 as a crow.

252
00:14:57,240 --> 00:15:01,120
 And then we have the story of the woman on the boat.

253
00:15:01,120 --> 00:15:06,550
 This woman in the past, she was a woman who lived in Ben

254
00:15:06,550 --> 00:15:09,680
ares, Varanasi as it's known

255
00:15:09,680 --> 00:15:11,800
 now.

256
00:15:11,800 --> 00:15:12,960
 And she had a dog.

257
00:15:12,960 --> 00:15:16,630
 She was a housewife and so she did all these chores, but

258
00:15:16,630 --> 00:15:18,800
 there was a dog in the house that

259
00:15:18,800 --> 00:15:23,400
 would follow her around everywhere.

260
00:15:23,400 --> 00:15:27,530
 And for some reason, people would tease her because this

261
00:15:27,530 --> 00:15:29,560
 dog was following her like a

262
00:15:29,560 --> 00:15:34,610
 shadow and very, very much, very, very affectionate,

263
00:15:34,610 --> 00:15:38,240
 actually like normal dogs are, but people

264
00:15:38,240 --> 00:15:41,120
 were joking about it because I guess it wasn't a big thing

265
00:15:41,120 --> 00:15:42,880
 for women to have dogs following

266
00:15:42,880 --> 00:15:43,880
 them around.

267
00:15:43,880 --> 00:15:47,720
 In fact, it was a common thing for hunters to have dogs, as

268
00:15:47,720 --> 00:15:49,560
 we learned in our previous

269
00:15:49,560 --> 00:15:52,800
 story.

270
00:15:52,800 --> 00:15:57,630
 So these young men joked about it and said, "Oh, here comes

271
00:15:57,630 --> 00:15:59,720
 the hunter with their dog.

272
00:15:59,720 --> 00:16:01,200
 We're going to have meat to eat tonight."

273
00:16:01,200 --> 00:16:06,780
 There will be meat coming, just joking about her looking

274
00:16:06,780 --> 00:16:09,760
 like a hunter, having this big

275
00:16:09,760 --> 00:16:13,440
 dog go along with it.

276
00:16:13,440 --> 00:16:18,400
 And this woman was, I guess, of a cruel bent.

277
00:16:18,400 --> 00:16:22,940
 And so getting angry and feeling embarrassed and ashamed,

278
00:16:22,940 --> 00:16:25,200
 she picked up a stick and beat

279
00:16:25,200 --> 00:16:28,320
 the dog almost to death.

280
00:16:28,320 --> 00:16:31,970
 But dogs, being the way they are, have a funny resiliency

281
00:16:31,970 --> 00:16:33,800
 to these sorts of things.

282
00:16:33,800 --> 00:16:39,110
 And so the dog was actually unmoved and was still very much

283
00:16:39,110 --> 00:16:41,680
 in love with this woman.

284
00:16:41,680 --> 00:16:47,300
 It turns out, actually, the commentary says that this dog

285
00:16:47,300 --> 00:16:49,760
 used to be her husband.

286
00:16:49,760 --> 00:16:55,120
 And so that was recently her husband in one of her recent

287
00:16:55,120 --> 00:16:56,240
 births.

288
00:16:56,240 --> 00:17:00,010
 And so even though it's impossible to find, they say, you

289
00:17:00,010 --> 00:17:02,040
 can't find someone who hasn't

290
00:17:02,040 --> 00:17:05,160
 been your husband, your wife, your son, your daughter, your

291
00:17:05,160 --> 00:17:06,360
 mother, your father.

292
00:17:06,360 --> 00:17:14,010
 But recent births, there tends to still be some sort of

293
00:17:14,010 --> 00:17:18,600
 affinity or enmity in cases when

294
00:17:18,600 --> 00:17:21,360
 there was enmity before.

295
00:17:21,360 --> 00:17:26,360
 And so she beat this dog and it still came back.

296
00:17:26,360 --> 00:17:31,970
 And so she was increasingly angry, irrationally angry at

297
00:17:31,970 --> 00:17:33,160
 this dog.

298
00:17:33,160 --> 00:17:39,850
 And so lo and behold, when it came close, she picked up a

299
00:17:39,850 --> 00:17:43,000
 rope and she made a loop and waited

300
00:17:43,000 --> 00:17:44,000
 for the dog.

301
00:17:44,000 --> 00:17:46,470
 And when the dog came close, she wrapped the loop around

302
00:17:46,470 --> 00:17:47,840
 the dog and tied it to a pot full

303
00:17:47,840 --> 00:17:52,600
 of sand and threw the pot of sand into this big pool.

304
00:17:52,600 --> 00:17:56,500
 And it rolled down into the pool and the dog was pulled and

305
00:17:56,500 --> 00:17:58,560
 was dragged after it into the

306
00:17:58,560 --> 00:18:03,280
 pool and it drowned.

307
00:18:03,280 --> 00:18:11,270
 And that was the karma that caused her to be thrown

308
00:18:11,270 --> 00:18:14,360
 overboard as well as be spent many

309
00:18:14,360 --> 00:18:17,280
 years in hell.

310
00:18:17,280 --> 00:18:19,120
 That's story number two.

311
00:18:19,120 --> 00:18:23,450
 Story number three, there he tells these, you monks are

312
00:18:23,450 --> 00:18:25,640
 also have done bad things in

313
00:18:25,640 --> 00:18:26,640
 the past.

314
00:18:26,640 --> 00:18:35,160
 But one time you were cowherds and you came upon this huge

315
00:18:35,160 --> 00:18:39,200
 lizard and I guess it was something

316
00:18:39,200 --> 00:18:42,540
 that they would like to eat, people would like to eat.

317
00:18:42,540 --> 00:18:47,220
 And so they ran after it trying to catch this big lizard.

318
00:18:47,220 --> 00:18:51,960
 But it ran into an ant hill that had seven holes.

319
00:18:51,960 --> 00:18:54,400
 Some reason seven is a big number.

320
00:18:54,400 --> 00:18:56,880
 I think it probably just means there were a bunch of holes.

321
00:18:56,880 --> 00:19:02,360
 And so they plugged up all these holes and then they, you

322
00:19:02,360 --> 00:19:05,600
 know, thinking that they could

323
00:19:05,600 --> 00:19:06,800
 catch it at one of the holes.

324
00:19:06,800 --> 00:19:09,200
 But then they said, you know, we just don't have time for

325
00:19:09,200 --> 00:19:09,560
 this.

326
00:19:09,560 --> 00:19:11,980
 We'll plug up all the holes and we'll come back tomorrow

327
00:19:11,980 --> 00:19:13,320
 and we'll catch this lizard

328
00:19:13,320 --> 00:19:16,120
 because there's now no way out of this big, I guess, a term

329
00:19:16,120 --> 00:19:17,560
ite mound or something.

330
00:19:17,560 --> 00:19:19,560
 Something were lizards.

331
00:19:19,560 --> 00:19:20,600
 I don't know.

332
00:19:20,600 --> 00:19:22,360
 Something that lizards like to stay in.

333
00:19:22,360 --> 00:19:24,360
 Big lizard though.

334
00:19:24,360 --> 00:19:26,800
 So we'll come back tomorrow and we'll catch it.

335
00:19:26,800 --> 00:19:31,040
 And so they went home but then they forgot all about it.

336
00:19:31,040 --> 00:19:33,900
 And so for seven days they went and bought their business

337
00:19:33,900 --> 00:19:35,280
 tending cows elsewhere.

338
00:19:35,280 --> 00:19:38,140
 But then on the seventh day they came back and they were

339
00:19:38,140 --> 00:19:39,760
 tending cows and they saw the

340
00:19:39,760 --> 00:19:43,500
 ant hill again and they realized, oh, I wonder what

341
00:19:43,500 --> 00:19:45,520
 happened to that lizard.

342
00:19:45,520 --> 00:19:49,880
 And so they opened up the holes and the lizard at this

343
00:19:49,880 --> 00:19:52,800
 point starved and dehydrated, not

344
00:19:52,800 --> 00:19:56,840
 afraid of its, for its life at all.

345
00:19:56,840 --> 00:19:59,440
 And it's basically at the end of its tether.

346
00:19:59,440 --> 00:20:02,160
 I had to come out.

347
00:20:02,160 --> 00:20:07,340
 And so came out and they said to themselves, they felt,

348
00:20:07,340 --> 00:20:09,560
 they took pity on it.

349
00:20:09,560 --> 00:20:11,280
 They said, oh, that's not killing it.

350
00:20:11,280 --> 00:20:13,080
 This is this poor thing.

351
00:20:13,080 --> 00:20:14,760
 We tortured it terribly.

352
00:20:14,760 --> 00:20:17,320
 So they nursed it and they actually brought it back to life

353
00:20:17,320 --> 00:20:17,560
.

354
00:20:17,560 --> 00:20:21,150
 And he said, the Buddha said, see, because of that you were

355
00:20:21,150 --> 00:20:22,840
 able to escape because you

356
00:20:22,840 --> 00:20:24,440
 came back for this lizard.

357
00:20:24,440 --> 00:20:29,520
 If not, that would have been it for you.

358
00:20:29,520 --> 00:20:32,590
 I think these stories are interesting, whether you believe

359
00:20:32,590 --> 00:20:34,240
 them or not, but they give some

360
00:20:34,240 --> 00:20:38,300
 idea of the nature of karma according to Buddhism and some

361
00:20:38,300 --> 00:20:39,360
 of the ways.

362
00:20:39,360 --> 00:20:40,360
 They're just examples.

363
00:20:40,360 --> 00:20:43,970
 It doesn't mean they're not law, like this has to be like

364
00:20:43,970 --> 00:20:44,600
 this.

365
00:20:44,600 --> 00:20:48,970
 But apparently the way things sometimes turn out, like our

366
00:20:48,970 --> 00:20:51,160
 past deeds influence both in

367
00:20:51,160 --> 00:20:55,430
 this life and the next, they influence the things that

368
00:20:55,430 --> 00:20:56,680
 happen to us.

369
00:20:56,680 --> 00:20:59,800
 And then they said, but is it really that, is it really

370
00:20:59,800 --> 00:21:01,640
 that way that you can't escape

371
00:21:01,640 --> 00:21:02,640
 your karma?

372
00:21:02,640 --> 00:21:04,560
 Isn't there somewhere you can go to escape it?

373
00:21:04,560 --> 00:21:05,560
 Couldn't you run away?

374
00:21:05,560 --> 00:21:07,920
 And the Buddha said, no, you couldn't run away.

375
00:21:07,920 --> 00:21:11,340
 There's no place on earth that you can go to run away from

376
00:21:11,340 --> 00:21:12,320
 your karma.

377
00:21:12,320 --> 00:21:17,280
 There's another jataka that talks about this.

378
00:21:17,280 --> 00:21:22,410
 There's a goat that this Brahman, the goat talks to him and

379
00:21:22,410 --> 00:21:24,840
 the goat says, you know,

380
00:21:24,840 --> 00:21:28,140
 it's this crying, laughing jataka where he cries and then

381
00:21:28,140 --> 00:21:29,800
 he laughs, or he laughs and

382
00:21:29,800 --> 00:21:30,800
 then he cries.

383
00:21:30,800 --> 00:21:34,780
 He's about to be killed and the goat starts laughing and

384
00:21:34,780 --> 00:21:37,640
 then he says, why are you laughing?

385
00:21:37,640 --> 00:21:40,240
 He said, because this is my last life.

386
00:21:40,240 --> 00:21:44,890
 I was a bra, I'm now, this is the last life that I have to

387
00:21:44,890 --> 00:21:46,520
 be born as a goat.

388
00:21:46,520 --> 00:21:49,800
 For 500 years I've been a sacrificial goat.

389
00:21:49,800 --> 00:21:51,080
 This is it.

390
00:21:51,080 --> 00:21:53,380
 And then he starts crying and the guys, the brahmanas, why

391
00:21:53,380 --> 00:21:54,120
 are you crying?

392
00:21:54,120 --> 00:21:57,920
 And he said, because I'm thinking of you.

393
00:21:57,920 --> 00:22:01,670
 Why I was a goat for 500 years, being sacrificed, having my

394
00:22:01,670 --> 00:22:03,720
 head cut off is because before that

395
00:22:03,720 --> 00:22:06,880
 I was a brahman just like you who killed goats.

396
00:22:06,880 --> 00:22:10,360
 So I know this is where you are going to go.

397
00:22:10,360 --> 00:22:12,640
 And the brahman said, oh, then I'll protect you.

398
00:22:12,640 --> 00:22:16,440
 I won't let them kill you.

399
00:22:16,440 --> 00:22:17,680
 And he said, there's nothing you can do.

400
00:22:17,680 --> 00:22:18,680
 There's no way you can stop it.

401
00:22:18,680 --> 00:22:22,110
 And sure enough, the brahman tried to protect him and made

402
00:22:22,110 --> 00:22:23,880
 sure nobody came near him.

403
00:22:23,880 --> 00:22:27,800
 But a rock fell actually on this goat.

404
00:22:27,800 --> 00:22:30,600
 There's some really strange coincidence.

405
00:22:30,600 --> 00:22:34,960
 He ended up dying.

406
00:22:34,960 --> 00:22:38,480
 And the brahmanas like that.

407
00:22:38,480 --> 00:22:41,240
 You see potentially these sorts of things in the world.

408
00:22:41,240 --> 00:22:42,480
 Very strange things happened.

409
00:22:42,480 --> 00:22:49,280
 There was a woman once, the wife of a top Monsanto exec.

410
00:22:49,280 --> 00:22:53,800
 Not that that means anything, but it's interesting.

411
00:22:53,800 --> 00:22:56,820
 Walking down the road, I read this in the paper some years

412
00:22:56,820 --> 00:22:58,400
 ago, walking down the road

413
00:22:58,400 --> 00:23:05,140
 and was suddenly hit by a car and pulled under the car and

414
00:23:05,140 --> 00:23:09,080
 dragged screaming for several

415
00:23:09,080 --> 00:23:13,600
 blocks before she died.

416
00:23:13,600 --> 00:23:15,880
 Dragged under the car.

417
00:23:15,880 --> 00:23:18,700
 Turns out the woman who was driving the car was an old lady

418
00:23:18,700 --> 00:23:20,120
 who could barely see above

419
00:23:20,120 --> 00:23:23,470
 the dash and had no idea what she'd done and probably to

420
00:23:23,470 --> 00:23:25,960
 this day has never been told what

421
00:23:25,960 --> 00:23:31,120
 she did.

422
00:23:31,120 --> 00:23:33,960
 The story said they hadn't told her.

423
00:23:33,960 --> 00:23:36,680
 So it wasn't a bad karma.

424
00:23:36,680 --> 00:23:38,640
 She didn't have the intention to kill.

425
00:23:38,640 --> 00:23:41,900
 Probably some bad karma involved with driving when you

426
00:23:41,900 --> 00:23:43,480
 shouldn't be driving.

427
00:23:43,480 --> 00:23:46,800
 But that's a bit different.

428
00:23:46,800 --> 00:23:50,110
 But it's the kind of sort of, I mean we have no idea why

429
00:23:50,110 --> 00:23:51,360
 that happened.

430
00:23:51,360 --> 00:23:56,180
 People, modern people would say it's just a coincidence,

431
00:23:56,180 --> 00:23:58,400
 but it's interesting to look

432
00:23:58,400 --> 00:23:59,400
 and see.

433
00:23:59,400 --> 00:24:07,610
 I mean if you think in terms of sort of cause and effect,

434
00:24:07,610 --> 00:24:11,440
 the problem I think is that people

435
00:24:11,440 --> 00:24:15,410
 focus too much on physical cause and effect and they call

436
00:24:15,410 --> 00:24:17,520
 the mind, there's this term

437
00:24:17,520 --> 00:24:22,660
 epiphenomenon that the mind is at best just a byproduct

438
00:24:22,660 --> 00:24:26,880
 that is ineffectual, that has no

439
00:24:26,880 --> 00:24:31,110
 consequence that the mind can't affect the body, can't

440
00:24:31,110 --> 00:24:32,600
 affect reality.

441
00:24:32,600 --> 00:24:37,630
 So mind is just this thing that happens sort of like just a

442
00:24:37,630 --> 00:24:40,640
 byproduct, a side product that

443
00:24:40,640 --> 00:24:42,240
 is meaningless.

444
00:24:42,240 --> 00:24:46,510
 But if you think of the mind as being powerful, as being

445
00:24:46,510 --> 00:24:49,520
 potent, then it makes sense to think

446
00:24:49,520 --> 00:24:58,680
 that such a powerful experience should have some cause.

447
00:24:58,680 --> 00:25:04,000
 Like physical things don't just happen coincidentally.

448
00:25:04,000 --> 00:25:08,680
 An explosion takes gunpowder.

449
00:25:08,680 --> 00:25:13,360
 A supernova takes a lot of energy.

450
00:25:13,360 --> 00:25:18,030
 And so the idea that these experiences of being dragged

451
00:25:18,030 --> 00:25:21,320
 under a car should take some,

452
00:25:21,320 --> 00:25:25,000
 should not just happen randomly.

453
00:25:25,000 --> 00:25:26,160
 I think there's something to that.

454
00:25:26,160 --> 00:25:28,440
 I think there's something that we're missing often.

455
00:25:28,440 --> 00:25:31,200
 When we say it's just random, it's just coincidence.

456
00:25:31,200 --> 00:25:38,430
 I think there's an argument, even not from meditation or so

457
00:25:38,430 --> 00:25:41,840
 on, that it would take some

458
00:25:41,840 --> 00:25:44,800
 kind of structure, some kind of cause and effect.

459
00:25:44,800 --> 00:25:49,340
 But for meditative purposes, I mean this is of great

460
00:25:49,340 --> 00:25:53,160
 importance to us, the idea that our

461
00:25:53,160 --> 00:26:00,200
 intentions, our minds have consequences.

462
00:26:00,200 --> 00:26:04,200
 This is something that moves people to meditate and moves

463
00:26:04,200 --> 00:26:06,640
 meditators not to do unwholesome

464
00:26:06,640 --> 00:26:07,640
 deeds.

465
00:26:07,640 --> 00:26:10,870
 Meditation for this reason will change your life because

466
00:26:10,870 --> 00:26:12,560
 you start to see how powerful

467
00:26:12,560 --> 00:26:17,460
 and how poisonous the mind can be, how harmful the mind can

468
00:26:17,460 --> 00:26:20,600
 be when misdirected, how dangerous

469
00:26:20,600 --> 00:26:22,360
 it is.

470
00:26:22,360 --> 00:26:24,120
 You can see these things building.

471
00:26:24,120 --> 00:26:27,680
 You can see how poisonous.

472
00:26:27,680 --> 00:26:31,110
 You can imagine what it's like to do these things and you

473
00:26:31,110 --> 00:26:33,000
 can remember the things that

474
00:26:33,000 --> 00:26:34,560
 you've done.

475
00:26:34,560 --> 00:26:40,830
 And without any prompting, like anyone telling you it's

476
00:26:40,830 --> 00:26:44,200
 wrong or it's bad, you just start

477
00:26:44,200 --> 00:26:47,140
 to feel really bad about the things that you've done, bad

478
00:26:47,140 --> 00:26:48,400
 things you've done.

479
00:26:48,400 --> 00:26:51,550
 It doesn't take someone to tell you that's bad karma or

480
00:26:51,550 --> 00:26:52,400
 something.

481
00:26:52,400 --> 00:26:53,640
 That happens as well.

482
00:26:53,640 --> 00:26:58,520
 You feel guilty because they're told that things are bad.

483
00:26:58,520 --> 00:27:03,200
 But you just can't abide by it because it's so powerful.

484
00:27:03,200 --> 00:27:05,760
 This is where you start to feel the power of these deeds.

485
00:27:05,760 --> 00:27:09,280
 That's karmic and it's built up inside of you.

486
00:27:09,280 --> 00:27:11,240
 That's why our life flashes before our eyes.

487
00:27:11,240 --> 00:27:16,920
 Our life doesn't really flash before our eyes when we die.

488
00:27:16,920 --> 00:27:21,410
 The things that have had an impact on our mind, that's

489
00:27:21,410 --> 00:27:22,260
 karma.

490
00:27:22,260 --> 00:27:26,500
 Those things flash before our eyes.

491
00:27:26,500 --> 00:27:31,810
 So this has importance in our practice and to remind us and

492
00:27:31,810 --> 00:27:34,580
 to reinforce the things that

493
00:27:34,580 --> 00:27:36,920
 we see during our practice.

494
00:27:36,920 --> 00:27:39,650
 But also to encourage people to meditate, to learn about

495
00:27:39,650 --> 00:27:40,520
 these things.

496
00:27:40,520 --> 00:27:46,030
 If you want to become a better person, look at your mind,

497
00:27:46,030 --> 00:27:48,560
 learn about your mind.

498
00:27:48,560 --> 00:27:53,280
 These things occur throughout our lives.

499
00:27:53,280 --> 00:27:56,320
 People do evil things, evil things happen to people who don

500
00:27:56,320 --> 00:27:57,560
't seem to deserve them,

501
00:27:57,560 --> 00:28:11,400
 who don't seem to ever have done anything to deserve them.

502
00:28:11,400 --> 00:28:15,720
 But when through meditation we see that there actually is

503
00:28:15,720 --> 00:28:18,520
 this cause and effect relationship.

504
00:28:18,520 --> 00:28:23,480
 And it's quite reasonable to suggest that it continues into

505
00:28:23,480 --> 00:28:24,720
 the future.

506
00:28:24,720 --> 00:28:27,990
 It's quite reasonable to think whether you have evidence or

507
00:28:27,990 --> 00:28:29,160
 proof of it or not.

508
00:28:29,160 --> 00:28:31,600
 It's quite reasonable to think that it's going to have an

509
00:28:31,600 --> 00:28:32,840
 effect on your next life.

510
00:28:32,840 --> 00:28:37,200
 It's going to have an effect on your choices of rebirth.

511
00:28:37,200 --> 00:28:42,120
 It's going to have an effect on how people react to you in

512
00:28:42,120 --> 00:28:43,520
 the future.

513
00:28:43,520 --> 00:28:44,520
 That things don't go away.

514
00:28:44,520 --> 00:28:49,160
 The key to this verse is that they don't just go away.

515
00:28:49,160 --> 00:28:52,350
 The only way to outrun it would be to become an arahant, an

516
00:28:52,350 --> 00:28:54,680
 enlightened being, before it's

517
00:28:54,680 --> 00:28:55,800
 going to catch up with you.

518
00:28:55,800 --> 00:29:00,590
 And if you die as an arahant, then the future karma doesn't

519
00:29:00,590 --> 00:29:02,800
 have an opportunity to bear

520
00:29:02,800 --> 00:29:04,760
 fruit.

521
00:29:04,760 --> 00:29:10,660
 Anyway, so some stories about karma, some ideas of karma,

522
00:29:10,660 --> 00:29:13,400
 that's the Dhamma Pandapur

523
00:29:13,400 --> 00:29:14,400
 tonight.

524
00:29:14,400 --> 00:29:15,400
 Thank you all for tuning in.

525
00:29:15,400 --> 00:29:17,000
 I wish you all good practice.

526
00:29:17,000 --> 00:29:19,000
 1

