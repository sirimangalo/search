1
00:00:00,000 --> 00:00:06,440
 Okay, good evening.

2
00:00:06,440 --> 00:00:12,000
 Tonight we're continuing our study of the Dhammapada.

3
00:00:12,000 --> 00:00:21,000
 Today we look at verse 201, which goes as follows.

4
00:00:21,000 --> 00:00:33,130
 Jaya yang wei rang passavati, duhkang seiti parajito, upas

5
00:00:33,130 --> 00:00:38,000
anto sukang seiti, hitwa jaya

6
00:00:38,000 --> 00:00:41,000
 parajayam.

7
00:00:41,000 --> 00:00:46,000
 Which means,

8
00:00:46,000 --> 00:01:00,000
 victory breeds enmity or vengeance, weirak.

9
00:01:00,000 --> 00:01:04,000
 Those who are defeated,

10
00:01:04,000 --> 00:01:15,000
 those who are not victorious dwell in suffering.

11
00:01:15,000 --> 00:01:23,000
 Those who are tranquil or who have found peace,

12
00:01:23,000 --> 00:01:27,000
 dwell in happiness,

13
00:01:27,000 --> 00:01:37,000
 having abandoned victory and defeat.

14
00:01:37,000 --> 00:01:46,620
 So this was told in regards, what we're told, it was in

15
00:01:46,620 --> 00:01:52,000
 relation to Ajatasattu.

16
00:01:52,000 --> 00:01:58,320
 Ajatasattu was a prince in the time of the Buddha and he

17
00:01:58,320 --> 00:02:01,000
 fell in with the wrong crowd.

18
00:02:01,000 --> 00:02:07,830
 He became friends with Devadatta who was out to destroy or

19
00:02:07,830 --> 00:02:16,000
 take over the Buddhist congregation.

20
00:02:16,000 --> 00:02:25,000
 So he teamed up with Ajatasattu and Ajatasattu.

21
00:02:25,000 --> 00:02:29,310
 Devadatta suggested that he kill his father, who is the

22
00:02:29,310 --> 00:02:34,000
 king, and he would be able to take over the kingdom.

23
00:02:34,000 --> 00:02:41,000
 Devadatta would kill the Buddha and he would take over the,

24
00:02:41,000 --> 00:02:48,600
 which is what you call it, the religion, I guess, religious

25
00:02:48,600 --> 00:02:53,000
 organization.

26
00:02:53,000 --> 00:02:57,000
 And they tried and Devadatta was unsuccessful, of course.

27
00:02:57,000 --> 00:03:03,000
 Ajatasattu was eventually successful.

28
00:03:03,000 --> 00:03:06,000
 He tried to kill his father.

29
00:03:06,000 --> 00:03:09,730
 He couldn't bear to actually kill his father, so he threw

30
00:03:09,730 --> 00:03:11,000
 him in the dungeon,

31
00:03:11,000 --> 00:03:18,000
 had him tied to the wall or chained to the wall and

32
00:03:18,000 --> 00:03:23,000
 had him starve to death, not bringing any food to him.

33
00:03:23,000 --> 00:03:28,000
 The story goes that Bimbisara was his father,

34
00:03:28,000 --> 00:03:35,610
 was so happy because his jail cell, he was able to see the

35
00:03:35,610 --> 00:03:39,000
 monastery where the Buddha lived.

36
00:03:39,000 --> 00:03:46,000
 And so he was able to survive for quite some time.

37
00:03:46,000 --> 00:03:48,000
 That's a long story.

38
00:03:48,000 --> 00:03:53,000
 In jail he would do walking meditation.

39
00:03:53,000 --> 00:03:58,630
 And I can't remember, he was so happy that he wasn't even

40
00:03:58,630 --> 00:04:00,000
 concerned about being in prison.

41
00:04:00,000 --> 00:04:04,000
 Eventually he chained him to the wall.

42
00:04:04,000 --> 00:04:09,160
 And the queen brought food in secretly and eventually he

43
00:04:09,160 --> 00:04:13,000
 refused to allow the queen to see the king.

44
00:04:13,000 --> 00:04:16,000
 Eventually King Bimbisara died.

45
00:04:16,000 --> 00:04:18,000
 I'll make a long story short.

46
00:04:18,000 --> 00:04:19,000
 But that's not this story.

47
00:04:19,000 --> 00:04:25,000
 This story is about Ajatasattu after he killed the king.

48
00:04:25,000 --> 00:04:30,960
 He became quite an imperialist or a colonialist trying to

49
00:04:30,960 --> 00:04:34,000
 take over the rest of the world.

50
00:04:34,000 --> 00:04:43,870
 And he fought with the king of Kosala, who I guess was Pas

51
00:04:43,870 --> 00:04:45,000
enadi.

52
00:04:45,000 --> 00:04:48,180
 And it says he was his uncle, so I'm not really sure where

53
00:04:48,180 --> 00:04:49,000
 the connection is.

54
00:04:49,000 --> 00:04:53,000
 But he fought with his uncle and he kept beating his uncle.

55
00:04:53,000 --> 00:05:01,550
 And Pasenadi, the king of Kosala was so upset by being

56
00:05:01,550 --> 00:05:07,520
 beaten by Ajatasattu that he refused to eat and he became

57
00:05:07,520 --> 00:05:11,000
 very ill and very weak.

58
00:05:11,000 --> 00:05:14,000
 And he was just tormented lying on his bed.

59
00:05:14,000 --> 00:05:17,000
 It's actually quite a short story.

60
00:05:17,000 --> 00:05:21,380
 But there's much background to it, the background of this

61
00:05:21,380 --> 00:05:25,400
 prince killing his father, of Devadatta trying to kill the

62
00:05:25,400 --> 00:05:26,000
 Buddha,

63
00:05:26,000 --> 00:05:30,000
 of Bimbisara and his greatness at the moment of death.

64
00:05:30,000 --> 00:05:36,520
 Ajatasattu tried to actually stop his father from starving

65
00:05:36,520 --> 00:05:38,000
 to death.

66
00:05:38,000 --> 00:05:41,410
 At the last moment he went to see his father but he was too

67
00:05:41,410 --> 00:05:42,000
 late.

68
00:05:42,000 --> 00:05:46,000
 His father had died.

69
00:05:46,000 --> 00:05:57,000
 But the essence of this is about victory and defeat.

70
00:05:57,000 --> 00:06:14,000
 What are the results of our perspective on the world?

71
00:06:14,000 --> 00:06:27,900
 A person who strives constantly to defeat and to beat

72
00:06:27,900 --> 00:06:30,000
 others.

73
00:06:30,000 --> 00:06:34,400
 So on the surface it appears to just be good religious

74
00:06:34,400 --> 00:06:40,000
 advice telling people not to fight with each other.

75
00:06:40,000 --> 00:06:43,960
 When you fight, that's what we tell kids, when you fight

76
00:06:43,960 --> 00:06:48,000
 you end up hurting others, it's not very nice.

77
00:06:48,000 --> 00:07:00,000
 And you end up breeding enmity and vengeance and so on.

78
00:07:00,000 --> 00:07:05,020
 But there's a deeper lesson here, and a lesson for us as

79
00:07:05,020 --> 00:07:10,190
 meditators is that this perspective is one that leads to

80
00:07:10,190 --> 00:07:11,000
 suffering.

81
00:07:11,000 --> 00:07:21,000
 Dukkangsi di parajito

82
00:07:21,000 --> 00:07:24,000
 Buddhism is very much about our perspective.

83
00:07:24,000 --> 00:07:29,000
 We use the word "diti" a lot when we talk about our views.

84
00:07:29,000 --> 00:07:33,000
 It really refers to the way we look at the world,

85
00:07:33,000 --> 00:07:39,580
 the way we approach experience, the way we approach our

86
00:07:39,580 --> 00:07:41,000
 experience.

87
00:07:41,000 --> 00:07:45,610
 And it's similar to people who have ambition like Ajatasatt

88
00:07:45,610 --> 00:07:50,000
u had ambition, Devadatta had ambition.

89
00:07:50,000 --> 00:07:58,000
 Even simply approaching reality as problems that we have to

90
00:07:58,000 --> 00:07:59,000
 fix.

91
00:07:59,000 --> 00:08:04,290
 People who fall into this game of vengeance, of victory and

92
00:08:04,290 --> 00:08:15,000
 defeat have a sense of what they call a zero-sum game.

93
00:08:15,000 --> 00:08:23,500
 It means that in order for me to benefit, someone else has

94
00:08:23,500 --> 00:08:26,000
 to suffer.

95
00:08:26,000 --> 00:08:29,410
 And so in the end it equals out to zero. It's like a law of

96
00:08:29,410 --> 00:08:32,000
 thermodynamics or something.

97
00:08:32,000 --> 00:08:37,320
 In order to find happiness, it has to be at the expense of

98
00:08:37,320 --> 00:08:39,000
 someone else.

99
00:08:39,000 --> 00:08:43,180
 And so on the face of it, it seems practically or

100
00:08:43,180 --> 00:08:47,000
 conventionally sort of a terrible outlook.

101
00:08:47,000 --> 00:08:54,170
 And yet it is sort of a conventional outlook that people

102
00:08:54,170 --> 00:08:58,000
 have, thieves and warlords.

103
00:08:58,000 --> 00:09:02,120
 But also ordinary people have a sense of trying to find

104
00:09:02,120 --> 00:09:05,900
 happiness through manipulating others, through harming

105
00:09:05,900 --> 00:09:07,000
 others,

106
00:09:07,000 --> 00:09:10,590
 through defeating others. It's something we fall into quite

107
00:09:10,590 --> 00:09:11,000
 easily.

108
00:09:11,000 --> 00:09:18,600
 We're making enemies. We identify people who are in our way

109
00:09:18,600 --> 00:09:19,000
.

110
00:09:19,000 --> 00:09:23,000
 But it's a deeper psychological issue. It's our outlook.

111
00:09:23,000 --> 00:09:27,660
 And these actions affect our outlook and they spring from

112
00:09:27,660 --> 00:09:29,000
 our outlook.

113
00:09:29,000 --> 00:09:34,170
 Our outlook is a much more basic sense of trying to fix

114
00:09:34,170 --> 00:09:37,000
 things, trying to force,

115
00:09:37,000 --> 00:09:46,240
 trying to bring about resolution by sheer force of our disl

116
00:09:46,240 --> 00:09:50,000
iking of the situation.

117
00:09:50,000 --> 00:09:56,070
 It doesn't have to be a person, though people, other human

118
00:09:56,070 --> 00:10:04,000
 beings become galvanizing icons of hatred.

119
00:10:04,000 --> 00:10:08,000
 If you stub your toe, you might get angry at the table, but

120
00:10:08,000 --> 00:10:11,800
 if someone hits you, you can develop a grudge for a

121
00:10:11,800 --> 00:10:14,000
 lifetime.

122
00:10:14,000 --> 00:10:20,800
 If someone says bad things, it becomes a galvanizing, very

123
00:10:20,800 --> 00:10:25,000
 strengthening focal point for hatred.

124
00:10:25,000 --> 00:10:29,710
 But ultimately it's the same habit. We don't like something

125
00:10:29,710 --> 00:10:30,000
.

126
00:10:30,000 --> 00:10:36,060
 And so we fall into this habit of reacting, trying to get

127
00:10:36,060 --> 00:10:39,000
 rid of what it is that we don't like,

128
00:10:39,000 --> 00:10:42,630
 and what it is that we don't like, and what it is that we

129
00:10:42,630 --> 00:10:43,000
 don't like.

130
00:10:43,000 --> 00:10:51,000
 It forces the bad conditions to become good.

131
00:10:51,000 --> 00:10:56,860
 And so it leads directly to our understanding of the

132
00:10:56,860 --> 00:11:03,470
 practice of mindfulness as an antidote for this sort of

133
00:11:03,470 --> 00:11:05,000
 behavior.

134
00:11:05,000 --> 00:11:09,590
 And sort of as an alternative, it shows quite clearly how

135
00:11:09,590 --> 00:11:12,000
 mindfulness is different.

136
00:11:12,000 --> 00:11:17,830
 Mindfulness is not like fixing problems. It's certainly not

137
00:11:17,830 --> 00:11:20,000
 like achieving goals.

138
00:11:20,000 --> 00:11:26,140
 And it's absolutely not like forcing or controlling reality

139
00:11:26,140 --> 00:11:33,000
 or other people, especially, to try and be the way we want.

140
00:11:33,000 --> 00:11:37,000
 Mindfulness is about experiencing things as they are.

141
00:11:37,000 --> 00:11:41,080
 We talk about this and we explain mindfulness as just being

142
00:11:41,080 --> 00:11:43,000
 aware of things as they are.

143
00:11:43,000 --> 00:11:46,170
 But this sort of verse, this sort of teaching, this sort of

144
00:11:46,170 --> 00:11:49,000
 example of the story of a jatasattu,

145
00:11:49,000 --> 00:11:55,180
 this kind of story of people holding a grudge and fighting

146
00:11:55,180 --> 00:12:01,640
 and even killing each other over their desire for some

147
00:12:01,640 --> 00:12:05,000
 beneficial outcome.

148
00:12:05,000 --> 00:12:10,000
 This kind of story shows how different it is to be mindful

149
00:12:10,000 --> 00:12:14,000
 and it shows the contrast with what we're doing.

150
00:12:14,000 --> 00:12:25,000
 Contrast would lead to when someone tries to harm you,

151
00:12:25,000 --> 00:12:29,910
 to change your perception of the situation from trying to

152
00:12:29,910 --> 00:12:34,570
 defend yourself and trying to keep yourself from

153
00:12:34,570 --> 00:12:38,000
 experiencing suffering,

154
00:12:38,000 --> 00:12:43,280
 being able to see the experience as just an experience and

155
00:12:43,280 --> 00:12:46,000
 let it happen and let it go.

156
00:12:46,000 --> 00:12:52,080
 Which is a very radical sort of approach. It shows how

157
00:12:52,080 --> 00:13:07,000
 different this approach is from ordinary way of living.

158
00:13:07,000 --> 00:13:15,010
 But the difference is quite apparent, quite profound as

159
00:13:15,010 --> 00:13:18,000
 well, the difference in result.

160
00:13:18,000 --> 00:13:23,500
 A person who engages frequently, regularly in hatred, of

161
00:13:23,500 --> 00:13:26,000
 course, is going to find suffering.

162
00:13:26,000 --> 00:13:32,000
 They're going to cultivate enemies and enmity.

163
00:13:32,000 --> 00:13:35,680
 And they're going to suffer defeat and they're going to

164
00:13:35,680 --> 00:13:39,000
 lead to situations where people want to hurt them,

165
00:13:39,000 --> 00:13:47,000
 want to manipulate and take advantage of them.

166
00:13:47,000 --> 00:13:53,000
 And so you see at a very deep level this sort of approach,

167
00:13:53,000 --> 00:14:00,000
 this perspective of trying to force, trying to conquer.

168
00:14:00,000 --> 00:14:06,390
 It has very real results. This should be quite obvious that

169
00:14:06,390 --> 00:14:09,000
 this is the way things are.

170
00:14:09,000 --> 00:14:17,110
 When we try to hurt others, it's unsustainable in terms of

171
00:14:17,110 --> 00:14:22,000
 providing peace and happiness.

172
00:14:22,000 --> 00:14:26,000
 Not only does it create enmity but it changes who you are.

173
00:14:26,000 --> 00:14:32,000
 And so really it's this...

174
00:14:32,000 --> 00:14:36,000
 The teaching is that there are two levels to reality.

175
00:14:36,000 --> 00:14:39,680
 There's the conventional level and then there's I guess you

176
00:14:39,680 --> 00:14:42,000
 could call the psychological level.

177
00:14:42,000 --> 00:14:46,000
 And they work off of each other.

178
00:14:46,000 --> 00:14:52,690
 So when we act and speak trying to harm others, it affects

179
00:14:52,690 --> 00:14:54,000
 us psychologically.

180
00:14:54,000 --> 00:14:58,520
 Our psychological makeup, how we relate to experience, how

181
00:14:58,520 --> 00:15:04,000
 we perceive reality, is going to inform our decisions.

182
00:15:04,000 --> 00:15:09,360
 It's going to affect the way we relate to other people, the

183
00:15:09,360 --> 00:15:15,000
 way we relate to problems and situations in our lives.

184
00:15:15,000 --> 00:15:21,330
 So when you take on a philosophy of non-harming that this

185
00:15:21,330 --> 00:15:24,000
 verse sort of suggests,

186
00:15:24,000 --> 00:15:29,220
 of not trying to conquer others, of not seeing situations

187
00:15:29,220 --> 00:15:35,000
 as something to be conquered as a zero-sum game.

188
00:15:35,000 --> 00:15:39,500
 Because of course the reality is not anything like where

189
00:15:39,500 --> 00:15:43,000
 you defeat others and suddenly you're on top,

190
00:15:43,000 --> 00:15:47,000
 you've won something. The reality is that it's a constant

191
00:15:47,000 --> 00:15:52,120
 changing sort of flux where by sometimes you're winning and

192
00:15:52,120 --> 00:15:54,000
 sometimes you're losing.

193
00:15:54,000 --> 00:15:57,160
 And enmity breeds enmity. When you hurt others, you make

194
00:15:57,160 --> 00:15:58,000
 them want to hurt you.

195
00:15:58,000 --> 00:16:02,130
 So it's not a zero-sum game, it's an everybody loses sort

196
00:16:02,130 --> 00:16:03,000
 of game.

197
00:16:03,000 --> 00:16:08,000
 When you hurt others, you make others want to hurt you.

198
00:16:08,000 --> 00:16:12,140
 And all of this affects our psychology, it affects our

199
00:16:12,140 --> 00:16:13,000
 makeup.

200
00:16:13,000 --> 00:16:18,480
 When you cease to do that, when you stop wanting to hurt

201
00:16:18,480 --> 00:16:24,000
 others, when you allow other people their anger against you

202
00:16:24,000 --> 00:16:32,820
 and you experience the results of bad blood without

203
00:16:32,820 --> 00:16:34,000
 reacting,

204
00:16:34,000 --> 00:16:37,730
 this affects your psychology, it affects your mental makeup

205
00:16:37,730 --> 00:16:38,000
.

206
00:16:38,000 --> 00:16:42,130
 It's a very sort of good practical teaching in terms of the

207
00:16:42,130 --> 00:16:44,000
 direction we want to be heading,

208
00:16:44,000 --> 00:16:52,390
 of trying to be more peaceful, happier, free from suffering

209
00:16:52,390 --> 00:16:53,000
.

210
00:16:53,000 --> 00:16:57,000
 But from the other end as well, when we hear about this,

211
00:16:57,000 --> 00:17:02,000
 when we look at this teaching,

212
00:17:02,000 --> 00:17:10,000
 abandoning victory and defeat, "Hitwa Jaya Parajiyam,"

213
00:17:10,000 --> 00:17:14,000
 we see the deeper teaching of being mindful.

214
00:17:14,000 --> 00:17:17,920
 And when you yourself approach reality in terms of

215
00:17:17,920 --> 00:17:23,440
 experiences to be understood rather than problems to be

216
00:17:23,440 --> 00:17:24,000
 fixed

217
00:17:24,000 --> 00:17:29,910
 or challenges to be overcome or whatever, enemies to be

218
00:17:29,910 --> 00:17:33,000
 conquered, absolutely.

219
00:17:33,000 --> 00:17:38,000
 Pain being an enemy, even our defilements as being enemies

220
00:17:38,000 --> 00:17:43,000
 is really a problematic way of looking at them.

221
00:17:43,000 --> 00:17:46,330
 If you look at anger as an enemy or greed as an enemy, it

222
00:17:46,330 --> 00:17:52,000
 takes on an adversarial sort of state of being

223
00:17:52,000 --> 00:17:56,220
 and there's stress involved and you cultivate this habit of

224
00:17:56,220 --> 00:18:04,000
 trying to control and force and change reality.

225
00:18:04,000 --> 00:18:10,310
 But when you take up mindfulness, it's no longer just about

226
00:18:10,310 --> 00:18:12,000
 our actions and our speech,

227
00:18:12,000 --> 00:18:16,160
 but our whole approach to reality, that pain is not an

228
00:18:16,160 --> 00:18:17,000
 enemy.

229
00:18:17,000 --> 00:18:22,560
 Speech, harsh speech from other people is not something to

230
00:18:22,560 --> 00:18:27,000
 be conquered, not something to be fought against.

231
00:18:27,000 --> 00:18:30,940
 I don't deserve to hear that. I don't deserve to be spoken

232
00:18:30,940 --> 00:18:32,000
 to that way.

233
00:18:32,000 --> 00:18:40,000
 Instead, experiencing it as sound, experiencing it as sight

234
00:18:40,000 --> 00:18:40,000
.

235
00:18:40,000 --> 00:18:43,660
 Seeing reality is made up of experiences. This is what it

236
00:18:43,660 --> 00:18:47,000
 truly means, I think, to give up victory and defeat

237
00:18:47,000 --> 00:18:53,860
 because your whole way of looking at reality is no longer

238
00:18:53,860 --> 00:18:56,000
 adversarial.

239
00:18:56,000 --> 00:19:03,490
 You rise above, it might say, or you change the perspective

240
00:19:03,490 --> 00:19:04,000
.

241
00:19:04,000 --> 00:19:09,020
 There's an old saying, "If you're a hammer, everything

242
00:19:09,020 --> 00:19:11,000
 looks like a nail."

243
00:19:11,000 --> 00:19:15,830
 And it points out something, what it means is when your

244
00:19:15,830 --> 00:19:19,590
 outlook is adversarial, you're constantly going to be

245
00:19:19,590 --> 00:19:20,000
 trying to,

246
00:19:20,000 --> 00:19:24,000
 you're going to react in a certain way.

247
00:19:24,000 --> 00:19:31,320
 And it points out a very important reality that the world

248
00:19:31,320 --> 00:19:37,000
 around us, our environment, our experience of the world,

249
00:19:37,000 --> 00:19:41,000
 is very much dependent on our outlook.

250
00:19:41,000 --> 00:19:47,150
 And so, I mean, an argument against this sort of passivity

251
00:19:47,150 --> 00:19:52,000
 whereby you let other people harm you, for example,

252
00:19:52,000 --> 00:19:56,750
 as it may happen, where you are not only nonviolent but you

253
00:19:56,750 --> 00:20:00,000
're passive, you're not only peaceful but you're passive.

254
00:20:00,000 --> 00:20:05,600
 The criticism might be that you're going to allow people to

255
00:20:05,600 --> 00:20:10,740
 hurt you, you're going to be taken advantage of, for

256
00:20:10,740 --> 00:20:12,000
 example.

257
00:20:12,000 --> 00:20:16,900
 And so they would say, "Well, this is, if you look at it

258
00:20:16,900 --> 00:20:21,000
 this way, it's not a very good way to live."

259
00:20:21,000 --> 00:20:23,570
 But the point is that there's a deeper reality going on

260
00:20:23,570 --> 00:20:28,000
 here. You're changing the way you look at things.

261
00:20:28,000 --> 00:20:36,870
 A person who is harmed when someone attacks someone else or

262
00:20:36,870 --> 00:20:43,000
 speaks violently, speaks angrily to someone else.

263
00:20:43,000 --> 00:20:46,000
 The reality is still just experience.

264
00:20:46,000 --> 00:20:50,520
 Another way of looking at this is rather than an advers

265
00:20:50,520 --> 00:20:55,000
arial situation, looking at it as experiences of seeing,

266
00:20:55,000 --> 00:20:58,750
 hearing, smelling, tasting, feeling, thinking. If you can

267
00:20:58,750 --> 00:21:00,000
 see it like that,

268
00:21:00,000 --> 00:21:03,960
 it doesn't change the situation where suddenly people are

269
00:21:03,960 --> 00:21:05,000
 nice to you.

270
00:21:05,000 --> 00:21:11,270
 It changes the perspective and reality is suddenly

271
00:21:11,270 --> 00:21:15,640
 objective, meaning it doesn't matter whether you're being

272
00:21:15,640 --> 00:21:23,000
 hugged or you're being attacked.

273
00:21:23,000 --> 00:21:29,000
 It's a very radical, again, sort of outlook.

274
00:21:29,000 --> 00:21:38,240
 And I think this exemplifies or this shows us clearly how

275
00:21:38,240 --> 00:21:40,000
 radical it is to be mindful

276
00:21:40,000 --> 00:21:47,000
 and the extent to which mindfulness changes our lives.

277
00:21:47,000 --> 00:21:50,900
 It doesn't necessarily change our situation, so it can be

278
00:21:50,900 --> 00:21:53,000
 quite uncomfortable at first.

279
00:21:53,000 --> 00:21:56,140
 We don't have to even talk about other people who might

280
00:21:56,140 --> 00:22:00,000
 want to hurt us, but our own body wants to hurt us.

281
00:22:00,000 --> 00:22:07,750
 Because of our reactions and our emotions, we make the body

282
00:22:07,750 --> 00:22:12,000
 quite sick and we can be quite tense.

283
00:22:12,000 --> 00:22:15,290
 When you first come to practice meditation, it can be quite

284
00:22:15,290 --> 00:22:19,000
 painful. Your own body can cause you great suffering.

285
00:22:19,000 --> 00:22:22,000
 And if you look at that as an adversarial sort of thing,

286
00:22:22,000 --> 00:22:24,000
 something to be fixed, something to be fought against,

287
00:22:24,000 --> 00:22:31,110
 something to be controlled or tamed, you're just going to

288
00:22:31,110 --> 00:22:33,000
 create more stress and suffering.

289
00:22:33,000 --> 00:22:41,330
 You have a never-ending torment and it can be quite painful

290
00:22:41,330 --> 00:22:45,000
 and quite unpleasant.

291
00:22:45,000 --> 00:22:49,150
 So our way of looking at things when we're mindful is quite

292
00:22:49,150 --> 00:22:50,000
 simple.

293
00:22:50,000 --> 00:22:52,720
 It's not at all easy, but it's quite simple. It's about

294
00:22:52,720 --> 00:22:55,000
 seeing things just as they are.

295
00:22:55,000 --> 00:22:58,710
 This is why we repeat to ourselves, for example, pain, pain

296
00:22:58,710 --> 00:22:59,000
.

297
00:22:59,000 --> 00:23:02,740
 If you're happy, you would say happy, happy. Just trying to

298
00:23:02,740 --> 00:23:05,000
 see things as they are without reacting.

299
00:23:05,000 --> 00:23:10,120
 Trying to cultivate a new perspective, a new way of looking

300
00:23:10,120 --> 00:23:11,000
 at things.

301
00:23:11,000 --> 00:23:16,840
 That is not adversarial, not about conquering reality or

302
00:23:16,840 --> 00:23:23,000
 lives or winning or anything, achieving any goal.

303
00:23:23,000 --> 00:23:31,130
 But it's about cultivating a perspective of objectivity, of

304
00:23:31,130 --> 00:23:39,000
 peace, that rises above experiences of victory or defeat.

305
00:23:39,000 --> 00:23:42,070
 So someone might look at you and say, "Wow, that person is

306
00:23:42,070 --> 00:23:45,000
 a real pushover. They're being defeated."

307
00:23:45,000 --> 00:23:48,440
 And you won't even see it that way. In the end, you see it

308
00:23:48,440 --> 00:23:50,000
 only as experience.

309
00:23:50,000 --> 00:23:53,530
 It's quite powerful because the practical reality as

310
00:23:53,530 --> 00:23:59,000
 someone who is mindful is often quite revered, respected.

311
00:23:59,000 --> 00:24:05,280
 You'll find that people respect your opinion. They lose

312
00:24:05,280 --> 00:24:09,000
 their desire to harm you.

313
00:24:09,000 --> 00:24:12,100
 A person who is very mindful often finds that people who

314
00:24:12,100 --> 00:24:14,000
 they thought were their enemies

315
00:24:14,000 --> 00:24:19,020
 are suddenly able to come to terms and work towards a more

316
00:24:19,020 --> 00:24:24,520
 peaceful resolution because the person has changed the

317
00:24:24,520 --> 00:24:26,000
 parameters.

318
00:24:26,000 --> 00:24:36,510
 A lot of our adversarial conflict, a lot of our conflict as

319
00:24:36,510 --> 00:24:41,680
 humans depends on both sides continuing and perpetuating it

320
00:24:41,680 --> 00:24:42,000
.

321
00:24:42,000 --> 00:24:50,760
 Our anger perpetuates conflict. Our reactivity perpetuates

322
00:24:50,760 --> 00:24:52,000
 it.

323
00:24:52,000 --> 00:24:56,720
 So it's not something that you should be scared of or

324
00:24:56,720 --> 00:25:01,000
 perhaps not see it as so radical because it's not as though

325
00:25:01,000 --> 00:25:06,000
 you have to suddenly let people harm you.

326
00:25:06,000 --> 00:25:09,690
 It's about changing the way we look at reality, changing

327
00:25:09,690 --> 00:25:12,710
 our perspective, which also of course means changing our

328
00:25:12,710 --> 00:25:13,000
 lives.

329
00:25:13,000 --> 00:25:16,740
 Gradually, gradually, you come to live a more peaceful life

330
00:25:16,740 --> 00:25:19,000
 where you work out all your problems.

331
00:25:19,000 --> 00:25:24,060
 You start to look at your enemies as just people who have

332
00:25:24,060 --> 00:25:29,380
 problems and your interactions with them rather than as

333
00:25:29,380 --> 00:25:35,000
 fights to be won, their experiences to be understood.

334
00:25:36,000 --> 00:25:41,430
 You rise above, you abandon victory and defeat. That's the

335
00:25:41,430 --> 00:25:43,000
 verse here.

336
00:25:43,000 --> 00:25:46,780
 So that's the Dhammapada, a very simple verse, but I think

337
00:25:46,780 --> 00:25:49,800
 it ties in well with the understanding of what is

338
00:25:49,800 --> 00:25:51,000
 mindfulness.

339
00:25:51,000 --> 00:25:54,000
 So that's I think the lesson we take from it.

340
00:25:54,000 --> 00:25:58,000
 Thank you all for listening. Have a good night.

341
00:25:59,000 --> 00:26:00,000
 Thank you.

