1
00:00:00,000 --> 00:00:05,000
 Okay, good evening everyone.

2
00:00:05,000 --> 00:00:18,000
 Good night, we're doing question and answers.

3
00:00:18,000 --> 00:00:21,790
 If you're looking to ask questions, if you're not here with

4
00:00:21,790 --> 00:00:25,290
 us, you're here with us, you get to ask me questions every

5
00:00:25,290 --> 00:00:26,000
 day anyway.

6
00:00:26,000 --> 00:00:33,350
 But if you're online wondering how do you ask questions for

7
00:00:33,350 --> 00:00:41,330
 this, you have to go to our website at meditation.siri-mung

8
00:00:41,330 --> 00:00:43,000
alo.org

9
00:00:43,000 --> 00:00:50,000
 and ask questions there.

10
00:00:50,000 --> 00:00:55,140
 And you have to sign up, but that's purposeful because we

11
00:00:55,140 --> 00:00:58,000
 want to make it hard for you.

12
00:00:58,000 --> 00:01:03,000
 It's too easy people go and ask too many questions.

13
00:01:03,000 --> 00:01:07,210
 I mean, we get questions that are off topic, so we want to

14
00:01:07,210 --> 00:01:11,000
 ensure that people are serious before they ask questions.

15
00:01:11,000 --> 00:01:15,660
 And also we're only really trying to answer questions about

16
00:01:15,660 --> 00:01:20,170
 this tradition, about our meditation practice that are

17
00:01:20,170 --> 00:01:23,000
 going to be useful for our group.

18
00:01:23,000 --> 00:01:31,000
 So without further ado, we have nine questions so far.

19
00:01:31,000 --> 00:01:35,150
 First, we have a couple of questions. I would prefer if you

20
00:01:35,150 --> 00:01:40,000
 could separate them out, I think.

21
00:01:40,000 --> 00:01:45,000
 I'm not 100% on that, but if you've got multiple questions,

22
00:01:45,000 --> 00:01:48,000
 please do separate them out. It's not really fair to...

23
00:01:48,000 --> 00:01:55,000
 It's just not as clean.

24
00:01:55,000 --> 00:02:00,000
 And... Uh-oh, shut down.

25
00:02:00,000 --> 00:02:07,000
 One minute.

26
00:02:07,000 --> 00:02:13,000
 Okay, so first question.

27
00:02:13,000 --> 00:02:16,270
 Some people criticize Mahasi Sayadaw's teachings saying

28
00:02:16,270 --> 00:02:21,000
 they are overly technical. How would you refute that?

29
00:02:21,000 --> 00:02:25,830
 Well, my immediate feeling about this is that I probably

30
00:02:25,830 --> 00:02:27,000
 wouldn't.

31
00:02:27,000 --> 00:02:32,210
 I'm not really that keen on going out of my way to refute

32
00:02:32,210 --> 00:02:33,000
 people.

33
00:02:33,000 --> 00:02:40,000
 If people want to see something as overly technical, I mean

34
00:02:40,000 --> 00:02:40,000
,

35
00:02:40,000 --> 00:02:44,120
 ultimately I imagine that most people who say that are

36
00:02:44,120 --> 00:02:47,000
 looking to find fault, first of all.

37
00:02:47,000 --> 00:02:51,680
 But then if we are to examine the question, what is it that

38
00:02:51,680 --> 00:02:53,000
 you find too technical?

39
00:02:53,000 --> 00:02:56,760
 The books that he wrote? Because I don't find them any more

40
00:02:56,760 --> 00:03:00,000
 technical than other traditional Buddhist teachings.

41
00:03:00,000 --> 00:03:03,120
 They might be a little more technical than the suttas, only

42
00:03:03,120 --> 00:03:04,000
 a little bit.

43
00:03:04,000 --> 00:03:06,790
 In fact, in some ways they're less technical than the sutt

44
00:03:06,790 --> 00:03:09,310
as because they involve all sorts of stories from the

45
00:03:09,310 --> 00:03:10,000
 commentaries.

46
00:03:10,000 --> 00:03:16,040
 The great thing about Mahasi Sayadaw is he's able to

47
00:03:16,040 --> 00:03:20,000
 inspire at the same time as give such deepened and...

48
00:03:20,000 --> 00:03:23,400
 You might call technical teachings. There's nothing wrong

49
00:03:23,400 --> 00:03:25,000
 with being technical.

50
00:03:25,000 --> 00:03:29,410
 And I think that's a feeling that people have. A criticism

51
00:03:29,410 --> 00:03:34,020
 I would give back is that people are often looking for

52
00:03:34,020 --> 00:03:39,000
 teachings that are vague or not vague, but simple.

53
00:03:39,000 --> 00:03:43,700
 And we might use the word touchy-feely, like they feel good

54
00:03:43,700 --> 00:03:44,000
.

55
00:03:44,000 --> 00:03:59,000
 And I have listened to talks and to be honest, I've...

56
00:03:59,000 --> 00:04:04,470
 Western talks often where some are very good, but sometimes

57
00:04:04,470 --> 00:04:06,770
 you listen to them and you ask yourself, "What did I learn

58
00:04:06,770 --> 00:04:07,000
?"

59
00:04:07,000 --> 00:04:14,080
 "I didn't really learn much, but I enjoyed myself." Dhamma

60
00:04:14,080 --> 00:04:14,820
 talks are sometimes like that, and that's what we wouldn't

61
00:04:14,820 --> 00:04:15,000
 want.

62
00:04:15,000 --> 00:04:20,000
 Technical is often much preferable because it provides you

63
00:04:20,000 --> 00:04:22,000
 with tools, right?

64
00:04:22,000 --> 00:04:26,980
 Technical, and we won't look it up on the internet, but

65
00:04:26,980 --> 00:04:31,000
 technical has to do with skills and tools.

66
00:04:31,000 --> 00:04:33,160
 So if it's just something that makes you feel good, like I

67
00:04:33,160 --> 00:04:36,000
 give you a talk and you feel good afterwards, well...

68
00:04:36,000 --> 00:04:40,550
 I don't feel like I did my... I did the job of inspiring

69
00:04:40,550 --> 00:04:43,000
 you, perhaps, and that's useful.

70
00:04:43,000 --> 00:04:47,620
 But it's not nearly enough and not nearly as good as

71
00:04:47,620 --> 00:04:52,000
 providing a technique, technical advice.

72
00:04:52,000 --> 00:04:54,290
 I think the Mahasya Sayadaw's teachings, I think they're

73
00:04:54,290 --> 00:04:57,940
 brilliant. I've never read anything, even that I'm reading

74
00:04:57,940 --> 00:05:03,090
 them in translation, I've never read anything as wonderful,

75
00:05:03,090 --> 00:05:04,000
 apart from...

76
00:05:04,000 --> 00:05:09,560
 In terms of an explanation of the Buddhist teaching, apart

77
00:05:09,560 --> 00:05:14,000
 from the teachings themselves, of course.

78
00:05:14,000 --> 00:05:18,000
 And if you're talking about the actual practice, I mean, I

79
00:05:18,000 --> 00:05:22,000
'd have to ask for a quorum here and the people in the room.

80
00:05:22,000 --> 00:05:25,880
 I won't ask, but I imagine asking all the people in the

81
00:05:25,880 --> 00:05:29,310
 room here what they think, whether this practice is too

82
00:05:29,310 --> 00:05:30,000
 technical.

83
00:05:30,000 --> 00:05:35,210
 It's fairly formal, but I would use words like concrete. It

84
00:05:35,210 --> 00:05:39,040
's very concrete. You don't have to guess about what you're

85
00:05:39,040 --> 00:05:40,000
 supposed to do.

86
00:05:40,000 --> 00:05:46,190
 It's rather involved and rather taxing, challenging. Inv

87
00:05:46,190 --> 00:05:49,000
olved in the sense there's quite a technique to it.

88
00:05:49,000 --> 00:05:53,250
 So it is quite technical, I guess, in that sense, but I don

89
00:05:53,250 --> 00:05:56,000
't have much more to say about that.

90
00:05:56,000 --> 00:06:03,000
 Then about Ajahn Chah, well, I'm not interested in

91
00:06:03,000 --> 00:06:03,000
 comparing with Ajahn Chah. It's a different teacher in

92
00:06:03,000 --> 00:06:03,000
 Thailand.

93
00:06:03,000 --> 00:06:09,710
 But he says that basically insight has to develop out of

94
00:06:09,710 --> 00:06:13,000
 peace and tranquility.

95
00:06:13,000 --> 00:06:18,730
 Maybe you could argue that. I think without any sort of

96
00:06:18,730 --> 00:06:23,820
 tranquility and peace, it's hard to imagine insight

97
00:06:23,820 --> 00:06:26,000
 becoming mature.

98
00:06:26,000 --> 00:06:29,650
 But then he says the entire process will happen naturally

99
00:06:29,650 --> 00:06:33,000
 of its own accord. You can't force it. We can't force it.

100
00:06:33,000 --> 00:06:37,390
 And I imagine, again, this is a translation from Thai,

101
00:06:37,390 --> 00:06:41,960
 obviously, but I imagine he had the best of intentions when

102
00:06:41,960 --> 00:06:43,000
 he said that.

103
00:06:43,000 --> 00:06:48,100
 And his meaning was, given the benefit of the doubt, was

104
00:06:48,100 --> 00:06:50,000
 good, was right.

105
00:06:50,000 --> 00:06:54,230
 But I have a problem with that statement as it stands, if

106
00:06:54,230 --> 00:06:59,000
 you interpret it to mean that if you practice tranquility,

107
00:06:59,000 --> 00:07:01,320
 if you calm your mind down, insight will grow out of it

108
00:07:01,320 --> 00:07:04,000
 naturally. And that's not true.

109
00:07:04,000 --> 00:07:10,010
 It will not arise naturally of its own accord unless you're

110
00:07:10,010 --> 00:07:15,030
 actually practicing a sort of meditation that's capable of

111
00:07:15,030 --> 00:07:16,000
 providing insight,

112
00:07:16,000 --> 00:07:20,000
 which very simply is a meditation focused on reality.

113
00:07:20,000 --> 00:07:23,000
 If you're not focused on reality, insight will not arise.

114
00:07:23,000 --> 00:07:27,090
 By very definition, insight is seeing impermanent suffering

115
00:07:27,090 --> 00:07:28,000
 and non-self.

116
00:07:28,000 --> 00:07:31,040
 If you're practicing certain types of meditation designed

117
00:07:31,040 --> 00:07:33,800
 to calm you down, Metta, for example, you'll never see

118
00:07:33,800 --> 00:07:36,000
 impermanent suffering and non-self

119
00:07:36,000 --> 00:07:39,740
 because the objects are not impermanent, are not unsatisf

120
00:07:39,740 --> 00:07:42,000
ying, are not uncontrollable.

121
00:07:42,000 --> 00:07:46,050
 I mean, there's a sense of stability and there involves

122
00:07:46,050 --> 00:07:52,240
 entities, people, beings who are actors and entities and

123
00:07:52,240 --> 00:07:55,000
 all sorts of things associated with self.

124
00:07:55,000 --> 00:08:01,100
 So your question, do you need to calm the mind in order for

125
00:08:01,100 --> 00:08:04,870
 insight to arise or will calm naturally arise through

126
00:08:04,870 --> 00:08:06,000
 insight practice?

127
00:08:06,000 --> 00:08:10,000
 This is, again, going back to the Yoganada Sutta.

128
00:08:10,000 --> 00:08:15,240
 We're lucky that we have this sutta that says sometimes our

129
00:08:15,240 --> 00:08:18,840
 hunts will have practiced insight first and then develop

130
00:08:18,840 --> 00:08:20,000
 tranquility after.

131
00:08:20,000 --> 00:08:23,550
 I mean, it's quite clear from the sutta that the Buddha had

132
00:08:23,550 --> 00:08:27,500
 these sort of questions were going or were being asked and

133
00:08:27,500 --> 00:08:31,000
 he gave very clear instruction,

134
00:08:31,000 --> 00:08:35,830
 which I think is it's instructive for those people who want

135
00:08:35,830 --> 00:08:40,000
 to say it has to be like this or it has to be like that.

136
00:08:40,000 --> 00:08:46,440
 When the Buddha quite clearly said it can be like this or

137
00:08:46,440 --> 00:08:49,000
 it can be like that.

138
00:08:49,000 --> 00:08:53,000
 How to use mindfulness to overcome panic attacks phobia?

139
00:08:53,000 --> 00:08:55,590
 I'm sure you've been asked this question before, so sorry

140
00:08:55,590 --> 00:08:57,000
 for a repetitive question.

141
00:08:57,000 --> 00:09:00,290
 Now, I would normally have deleted such a question because,

142
00:09:00,290 --> 00:09:02,000
 yes, I do answer this one a lot.

143
00:09:02,000 --> 00:09:06,850
 But this one particularly, I have a soft spot for it

144
00:09:06,850 --> 00:09:11,920
 because it is one that we get a lot and it's not a trivial

145
00:09:11,920 --> 00:09:13,000
 question.

146
00:09:13,000 --> 00:09:16,470
 But I would recommend, I'm only going to answer by

147
00:09:16,470 --> 00:09:20,270
 recommending that you look up some of my other videos on

148
00:09:20,270 --> 00:09:21,000
 anxiety

149
00:09:21,000 --> 00:09:24,600
 and that you undertake, you read my booklet and try to

150
00:09:24,600 --> 00:09:28,000
 practice accordingly because that's about it.

151
00:09:28,000 --> 00:09:32,150
 Just stop taking medication. Don't get interested in

152
00:09:32,150 --> 00:09:36,000
 medication for any of this stuff.

153
00:09:36,000 --> 00:09:39,000
 Start meditating. Any of that.

154
00:09:39,000 --> 00:09:42,170
 Am I allowed to say that? Stop taking medication? No, I

155
00:09:42,170 --> 00:09:46,000
 shouldn't. Oh, no. That was a mistake.

156
00:09:46,000 --> 00:09:49,770
 When you say I started working with doctors and taking meds

157
00:09:49,770 --> 00:09:51,000
, honestly, they don't help.

158
00:09:51,000 --> 00:09:53,190
 I apologize for saying don't take your medication. It's

159
00:09:53,190 --> 00:09:56,240
 what I want to say, but I can't say it because I'm not a

160
00:09:56,240 --> 00:09:57,000
 doctor.

161
00:09:57,000 --> 00:10:02,000
 So what I'll say instead is I agree that medication is,

162
00:10:02,000 --> 00:10:07,000
 from what I've seen, not as helpful as people think it is.

163
00:10:07,000 --> 00:10:11,960
 And I would recommend meditation as a solution because I

164
00:10:11,960 --> 00:10:15,000
 have seen it to be very helpful.

165
00:10:15,000 --> 00:10:17,780
 Now, if you're on medication, it's not likely that

166
00:10:17,780 --> 00:10:21,320
 meditation will be all that fruitful, so I wouldn't

167
00:10:21,320 --> 00:10:24,000
 recommend mixing them.

168
00:10:24,000 --> 00:10:29,000
 Sorry, that's all I can say, I guess.

169
00:10:29,000 --> 00:10:36,000
 Here's one.

170
00:10:36,000 --> 00:10:39,010
 Is it okay to be mindful of breath and body movements

171
00:10:39,010 --> 00:10:46,860
 during the day, then not distracted or memories and go back

172
00:10:46,860 --> 00:10:49,000
 to noting?

173
00:10:49,000 --> 00:10:54,940
 I think the answer is yes. I mean, you're talking about

174
00:10:54,940 --> 00:10:57,000
 being mindful of your experiences,

175
00:10:57,000 --> 00:11:02,730
 which shows that you've got a good grasp of what we're

176
00:11:02,730 --> 00:11:09,000
 talking about. You're feeling responsible, you're...

177
00:11:09,000 --> 00:11:16,320
 Right. Yeah, you feel like you've done something bad. And

178
00:11:16,320 --> 00:11:21,000
 then, yes.

179
00:11:21,000 --> 00:11:27,590
 If you feel sorry for yourself, note that sorry story and

180
00:11:27,590 --> 00:11:32,000
 then just go back to your practice.

181
00:11:32,000 --> 00:11:35,810
 Is it possible to cultivate faith or is that something that

182
00:11:35,810 --> 00:11:38,000
 is spontaneously developed?

183
00:11:38,000 --> 00:11:42,130
 It's possible. I don't think it's very stable. I mean, a

184
00:11:42,130 --> 00:11:44,810
 lot of theists and other religious people develop great

185
00:11:44,810 --> 00:11:47,000
 faith purposefully.

186
00:11:47,000 --> 00:11:51,040
 I mean, I think like any mental state, there are practices

187
00:11:51,040 --> 00:11:55,000
 you can do to encourage it. It's probably better to say.

188
00:11:55,000 --> 00:11:57,960
 You can't just say, "I'm going to have faith," but you can

189
00:11:57,960 --> 00:12:03,200
 engage in practices like prayer. We do chanting, which

190
00:12:03,200 --> 00:12:05,000
 creates faith.

191
00:12:05,000 --> 00:12:08,990
 Nothing is ever spontaneously developed. Everything has

192
00:12:08,990 --> 00:12:10,000
 conditions.

193
00:12:10,000 --> 00:12:13,290
 And so there are ways of cultivating faith and they'll

194
00:12:13,290 --> 00:12:17,000
 differ depending on your situation and your condition.

195
00:12:17,000 --> 00:12:20,030
 For some people, reading the Buddhist teaching is a great

196
00:12:20,030 --> 00:12:23,000
 way to gain faith for some people, for other people.

197
00:12:23,000 --> 00:12:27,590
 I mean, with the proviso that faith is not what we're

198
00:12:27,590 --> 00:12:31,000
 really trying to focus on building.

199
00:12:31,000 --> 00:12:33,890
 You should really just focus on mindfulness and everything

200
00:12:33,890 --> 00:12:35,000
 else balances out.

201
00:12:35,000 --> 00:12:38,400
 What's really useful is to see which ones you're lacking

202
00:12:38,400 --> 00:12:41,650
 and then be mindful of that if you have doubts or no doub

203
00:12:41,650 --> 00:12:43,000
ting, doubting.

204
00:12:43,000 --> 00:12:47,030
 The teachings on the five faculties are much more for

205
00:12:47,030 --> 00:12:51,000
 helping you pinpoint what you have to be mindful of

206
00:12:51,000 --> 00:12:54,330
 than saying, "Okay, now I'm going to build faith." It's not

207
00:12:54,330 --> 00:12:58,000
 really that useful.

208
00:12:58,000 --> 00:13:05,000
 Again, three questions, so I'd prefer if you broke them up.

209
00:13:05,000 --> 00:13:08,000
 Makes it easier to read at the very least.

210
00:13:08,000 --> 00:13:13,000
 "Before I start to meditate, must I chant texts?" No.

211
00:13:13,000 --> 00:13:16,210
 "Would you recommend to lay people to chant in the morning,

212
00:13:16,210 --> 00:13:17,000
 evening?"

213
00:13:17,000 --> 00:13:21,840
 Yes. I wouldn't recommend people who weren't thinking about

214
00:13:21,840 --> 00:13:26,000
 it, but if someone's looking to gain encouragement, sure.

215
00:13:26,000 --> 00:13:28,000
 Yes, it's a good thing.

216
00:13:28,000 --> 00:13:31,630
 "I am making very good progress in meditation, meditating

217
00:13:31,630 --> 00:13:35,000
 for about 30 to 45 minutes almost every second day."

218
00:13:35,000 --> 00:13:39,000
 Great. "But for a few weeks something strange has happened.

219
00:13:39,000 --> 00:13:42,000
 I have to swallow constantly."

220
00:13:42,000 --> 00:13:46,000
 Yes. "And the fear of swallowing makes you swallow." That's

221
00:13:46,000 --> 00:13:46,000
 fine.

222
00:13:46,000 --> 00:13:50,150
 The fear is an emotion you would say, "Afraid, afraid," or

223
00:13:50,150 --> 00:13:53,000
 "Worried," or "Anxious," or however it appears to you.

224
00:13:53,000 --> 00:13:56,190
 And as you're swallowing, you would say, "Swallowing." It's

225
00:13:56,190 --> 00:13:59,300
 an interesting object of meditation. There's no problem

226
00:13:59,300 --> 00:14:00,000
 there.

227
00:14:00,000 --> 00:14:02,000
 There's no need to handle it.

228
00:14:02,000 --> 00:14:05,600
 Again, handling things is our go-to reaction, but that's

229
00:14:05,600 --> 00:14:07,000
 not how this works.

230
00:14:07,000 --> 00:14:11,860
 We're just trying to stop trying to handle, learn to bear

231
00:14:11,860 --> 00:14:17,640
 and to coexist with that experience, and it'll go away by

232
00:14:17,640 --> 00:14:18,000
 itself.

233
00:14:18,000 --> 00:14:21,000
 It's not actually a problem.

234
00:14:21,000 --> 00:14:25,000
 "I have a restless mind that quickly becomes frightened.

235
00:14:25,000 --> 00:14:31,000
 Our emotions as object correct in the sense of the dumb."

236
00:14:31,000 --> 00:14:34,350
 "I've achieved very good results in just watching with Sati

237
00:14:34,350 --> 00:14:38,000
 my emotions and finally dissolve only by the observation."

238
00:14:38,000 --> 00:14:42,070
 Well, that's not how we practice. We would have you note, "

239
00:14:42,070 --> 00:14:46,000
Afraid, afraid," or "Worried," "Worried," or "Worried."

240
00:14:46,000 --> 00:14:54,000
 Okay, question about reading.

241
00:14:54,000 --> 00:14:58,730
 "I wonder if reading doesn't keep us in a world of concepts

242
00:14:58,730 --> 00:15:03,000
, at least unnecessarily involved in someone else's drama."

243
00:15:03,000 --> 00:15:07,550
 Well, if you mean reading fiction, reading... See, reading

244
00:15:07,550 --> 00:15:10,000
 itself, you're asking, is reading a wholesome activity?

245
00:15:10,000 --> 00:15:14,740
 Reading itself is not wholesome or unwholesome. Reading is

246
00:15:14,740 --> 00:15:19,420
 an act that occurs with the eye and then with the brain and

247
00:15:19,420 --> 00:15:22,000
 then with the mind.

248
00:15:22,000 --> 00:15:26,000
 It's a functional activity.

249
00:15:26,000 --> 00:15:33,230
 Now, reading gives rise to thinking, and thinking, of

250
00:15:33,230 --> 00:15:37,810
 course, can give rise to all sorts of emotions, good or bad

251
00:15:37,810 --> 00:15:39,000
, positive or negative.

252
00:15:39,000 --> 00:15:41,730
 If you read the Buddhist teachings, it's quite possible

253
00:15:41,730 --> 00:15:44,830
 that as a result of that, there will arise all sorts of

254
00:15:44,830 --> 00:15:46,000
 good qualities.

255
00:15:46,000 --> 00:15:50,800
 If you're reading, you know, romance, Harlequin romance,

256
00:15:50,800 --> 00:15:55,020
 less likely, much more likely to give rise to unwholesome

257
00:15:55,020 --> 00:15:56,000
 thoughts.

258
00:15:56,000 --> 00:15:57,800
 Now, they do say it's good for the brain, but that's

259
00:15:57,800 --> 00:15:59,000
 something quite different.

260
00:15:59,000 --> 00:16:01,910
 They say reading, even reading fiction, is good to

261
00:16:01,910 --> 00:16:05,240
 stimulate the brain and maybe even keep you healthy, you

262
00:16:05,240 --> 00:16:07,000
 could argue, perhaps.

263
00:16:07,000 --> 00:16:12,040
 They've done studies on it, but that's totally unrelated to

264
00:16:12,040 --> 00:16:15,000
 Buddhist practice, of course.

265
00:16:15,000 --> 00:16:17,330
 During Vipassana, I'm not able to note the start of

266
00:16:17,330 --> 00:16:21,000
 thinking. Could you give me any tips?

267
00:16:21,000 --> 00:16:24,230
 Yeah, don't worry about it. If you catch the beginning,

268
00:16:24,230 --> 00:16:27,000
 that's great. If you catch it at the end, that's fine.

269
00:16:27,000 --> 00:16:30,280
 Whenever you catch it, try to be mindful. I mean, there's

270
00:16:30,280 --> 00:16:33,770
 so much going on inside that we're not even thinking of

271
00:16:33,770 --> 00:16:35,000
 trying to be perfect.

272
00:16:35,000 --> 00:16:38,310
 We're just trying to catch whenever we know, whenever we

273
00:16:38,310 --> 00:16:39,000
 experience something.

274
00:16:39,000 --> 00:16:42,260
 Oh, I was thinking there, then say to yourself, thinking,

275
00:16:42,260 --> 00:16:47,000
 thinking, once you realize it.

276
00:16:47,000 --> 00:16:53,700
 Is celibacy related to metta in any way? You could probably

277
00:16:53,700 --> 00:16:56,000
 relate them.

278
00:16:56,000 --> 00:17:00,130
 I mean, metta can be problematic for celibacy if you start

279
00:17:00,130 --> 00:17:03,770
 sending good thoughts to someone who is an object of

280
00:17:03,770 --> 00:17:05,000
 attraction.

281
00:17:05,000 --> 00:17:08,300
 They do say that in the text. Be careful about that. But I

282
00:17:08,300 --> 00:17:11,000
 don't think that's what you're asking.

283
00:17:11,000 --> 00:17:15,740
 If a person is celibate, are they better able to practice

284
00:17:15,740 --> 00:17:19,000
 metta, or is it a practice of metta?

285
00:17:19,000 --> 00:17:22,920
 You could argue that. I mean, celibacy is great for the

286
00:17:22,920 --> 00:17:27,230
 mind because it drains a lot of that passion, and it makes

287
00:17:27,230 --> 00:17:31,000
 you much more stable and calm in the mind.

288
00:17:31,000 --> 00:17:36,440
 So, not just metta, but many kinds of states are much

289
00:17:36,440 --> 00:17:39,000
 easier to cultivate.

290
00:17:39,000 --> 00:17:42,370
 If you're not being celibate, then things like metta are

291
00:17:42,370 --> 00:17:46,000
 more difficult because you're more subject to partiality,

292
00:17:46,000 --> 00:17:47,000
 which is...

293
00:17:47,000 --> 00:17:50,830
 If you think about your girlfriend, your boyfriend, and you

294
00:17:50,830 --> 00:17:54,260
 send them metta, well, it's much more likely to become an

295
00:17:54,260 --> 00:17:57,000
 attachment, which is quite dangerous.

296
00:17:57,000 --> 00:18:01,020
 Well, emotionally dangerous because of the suffering, of

297
00:18:01,020 --> 00:18:03,000
 course, that can follow.

298
00:18:03,000 --> 00:18:10,000
 How does one let go without feeling the want to explode?

299
00:18:10,000 --> 00:18:16,350
 How does one let go? You don't let go. Just if you want to

300
00:18:16,350 --> 00:18:18,080
 explode out of the human shell and let out the howl of

301
00:18:18,080 --> 00:18:21,000
 anger, hate, and disruptive, destructive nature.

302
00:18:21,000 --> 00:18:24,200
 Well, that's what you try to be mindful of and eventually

303
00:18:24,200 --> 00:18:28,620
 let go of that because that desire is a response to

304
00:18:28,620 --> 00:18:34,000
 experience and probably a lot of physical energy.

305
00:18:34,000 --> 00:18:36,790
 But it's a bad habit, and so you try to change those habits

306
00:18:36,790 --> 00:18:38,000
 through meditation.

307
00:18:38,000 --> 00:18:46,000
 Read my booklet. It would be a good practice for that.

308
00:18:46,000 --> 00:18:48,010
 According to Buddhism, is there a difference between

309
00:18:48,010 --> 00:18:51,000
 compassion and empathy? We don't use the word empathy much.

310
00:18:51,000 --> 00:18:53,700
 I don't even know that you could find a word that

311
00:18:53,700 --> 00:18:56,510
 translates. Maybe you could, but I can't think of one that

312
00:18:56,510 --> 00:18:58,000
 translates as empathy.

313
00:18:58,000 --> 00:19:03,000
 Empathy means to feel what other people are feeling.

314
00:19:03,000 --> 00:19:06,000
 You can sympathize.

315
00:19:06,000 --> 00:19:10,170
 Sympathy means you appreciate the scope of what people are

316
00:19:10,170 --> 00:19:15,000
 feeling, but to empathize means you feel it with them.

317
00:19:15,000 --> 00:19:17,780
 And that's quite different from compassion. Compassion isn

318
00:19:17,780 --> 00:19:20,000
't about feeling sad when someone's sad.

319
00:19:20,000 --> 00:19:25,500
 It's about appreciating and having a wish for them to be,

320
00:19:25,500 --> 00:19:30,000
 or having an inclination so that everything you do and say

321
00:19:30,000 --> 00:19:33,870
 is towards them being free from that sadness or that

322
00:19:33,870 --> 00:19:35,000
 suffering.

323
00:19:35,000 --> 00:19:43,000
 And that's it.

324
00:19:44,000 --> 00:19:49,790
 Everyone here is a fairly advanced meditator, so I don't

325
00:19:49,790 --> 00:19:55,000
 feel any great need to teach them lots of things.

326
00:19:55,000 --> 00:20:00,000
 I'm very glad that we have over a full house.

327
00:20:00,000 --> 00:20:05,250
 I had some idea that we might be moving soon, but it's a

328
00:20:05,250 --> 00:20:07,000
 tricky thing.

329
00:20:07,000 --> 00:20:12,340
 If there's anybody out there who has a place for us to move

330
00:20:12,340 --> 00:20:21,000
 that's bigger, we're happy to take donations.

331
00:20:21,000 --> 00:20:24,000
 But we're doing fine.

332
00:20:24,000 --> 00:20:27,600
 We can define anywhere. No? Someone slipped another

333
00:20:27,600 --> 00:20:35,000
 question in. Should we take it? Let's take it.

334
00:20:35,000 --> 00:20:39,000
 That's not a question, so. Bye-bye.

335
00:20:39,000 --> 00:20:46,000
 Okay, that's all. Thank you. Have a good night.

336
00:20:46,000 --> 00:21:00,000
 Oh no, I didn't. Oh, now you can see me. That's funny.

337
00:21:00,000 --> 00:21:05,850
 That whole teaching was just... with this transition and

338
00:21:05,850 --> 00:21:08,200
 that picture of me was... that's just the intro. I'm

339
00:21:08,200 --> 00:21:10,000
 supposed to transition out of that.

340
00:21:10,000 --> 00:21:13,000
 Well, hello everyone. Have a good night.

341
00:21:15,000 --> 00:21:16,000
 Thank you.

