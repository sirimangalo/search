1
00:00:00,000 --> 00:00:10,510
 And there won't be a live stream on, there won't be a live

2
00:00:10,510 --> 00:00:12,200
 YouTube tonight because I'm

3
00:00:12,200 --> 00:00:41,200
 going to upload the high quality video later on.

4
00:00:41,200 --> 00:00:42,200
 Okay.

5
00:00:42,200 --> 00:00:43,200
 Looks good.

6
00:00:43,200 --> 00:01:10,720
 Hello and welcome back to our study of the Dhammapada.

7
00:01:10,720 --> 00:01:17,720
 Today we continue on with verse 158 which reads as follows.

8
00:01:17,720 --> 00:01:33,830
 Ata namiva patamang pati rupe nive se ye ata nyamanu sa se

9
00:01:33,830 --> 00:01:40,480
 ye nakide se ye pandido.

10
00:01:40,480 --> 00:01:59,400
 Which means first one should set oneself in what is right.

11
00:01:59,400 --> 00:02:06,900
 Then should one, then if one should teach others, the wise

12
00:02:06,900 --> 00:02:10,840
 one will not sully the teaching

13
00:02:10,840 --> 00:02:12,120
 or sully themselves.

14
00:02:12,120 --> 00:02:15,440
 Will not do something corrupt.

15
00:02:15,440 --> 00:02:23,480
 Will not be guilty.

16
00:02:23,480 --> 00:02:27,520
 So set yourself in what is right first.

17
00:02:27,520 --> 00:02:31,200
 Then when you teach others it won't be wrong.

18
00:02:31,200 --> 00:02:35,560
 It won't be defiled by that.

19
00:02:35,560 --> 00:02:42,610
 It won't be marred by the fact that you're not practicing

20
00:02:42,610 --> 00:02:44,280
 yourself.

21
00:02:44,280 --> 00:02:52,080
 So this is the story based on the story of Upananda.

22
00:02:52,080 --> 00:02:58,580
 Upananda was a Sakya puta, he was a relative of the Buddha

23
00:02:58,580 --> 00:03:01,520
 and he became a monk but he

24
00:03:01,520 --> 00:03:07,060
 still had, not only did he have some fairly greedy or

25
00:03:07,060 --> 00:03:11,680
 luxurious tendencies, some attachments

26
00:03:11,680 --> 00:03:16,670
 to comfort from being royalty but he also seems to have

27
00:03:16,670 --> 00:03:19,760
 been a little bit crooked beyond

28
00:03:19,760 --> 00:03:22,680
 that.

