1
00:00:00,000 --> 00:00:03,090
 Souls of the feet from the source of the feet upwards down

2
00:00:03,090 --> 00:00:05,560
 from the top of the hair from the highest part of the hair

3
00:00:05,560 --> 00:00:06,060
 downwards

4
00:00:06,060 --> 00:00:09,680
 Conchanged in the skin terminated all around by the skin

5
00:00:09,680 --> 00:00:12,900
 Reviewed as full of many kinds of hair

6
00:00:12,900 --> 00:00:16,930
 He sees that this body is packed with the field of various

7
00:00:16,930 --> 00:00:20,040
 kinds of beginning with head hairs

8
00:00:20,040 --> 00:00:23,200
 How in this body they are a head hair and so on

9
00:00:25,140 --> 00:00:29,160
 So this this a kind of meditation meditation is taught at

10
00:00:29,160 --> 00:00:32,640
 every monastery and even in novel as a taught

11
00:00:32,640 --> 00:00:35,580
 You recite these 32 parts again and again

12
00:00:35,580 --> 00:00:39,360
 There are means that are found in this

13
00:00:39,360 --> 00:00:44,830
 In this in this which is expressed as up from the source of

14
00:00:44,830 --> 00:00:46,640
 feet and down from the top of the hair and

15
00:00:46,640 --> 00:00:50,940
 Contain in the skin as full of many kinds of your body that

16
00:00:50,940 --> 00:00:55,000
 the carcass for it is the carcass that is called body

17
00:00:55,020 --> 00:00:56,060
 because it is a

18
00:00:56,060 --> 00:00:59,930
 Compromoration of field because such five things at the

19
00:00:59,930 --> 00:01:04,140
 head hairs etc and the hundred diseases beginning with high

20
00:01:04,140 --> 00:01:04,740
 disease

21
00:01:04,740 --> 00:01:08,040
 Have have it as they are origin

22
00:01:08,040 --> 00:01:10,700
 What is the carcass?

23
00:01:10,700 --> 00:01:14,100
 Yeah, is it dead body

24
00:01:14,100 --> 00:01:17,500
 But it doesn't mean dead body

25
00:01:18,000 --> 00:01:20,000
 Doesn't mean this

26
00:01:20,000 --> 00:01:29,800
 So it is just a body not not necessarily a dead body here

27
00:01:29,800 --> 00:01:37,800
 There's a dead body

28
00:01:37,800 --> 00:01:45,560
 To be just a skeleton to the specific and the bones

29
00:01:47,540 --> 00:01:50,740
 But it here it is just just the body

30
00:01:50,740 --> 00:01:54,540
 Maybe in English

31
00:01:54,540 --> 00:01:57,460
 has

32
00:01:57,460 --> 00:01:59,460
 No other word for the body

33
00:01:59,460 --> 00:02:04,300
 Because in in Pali the first word is Kaya and

34
00:02:04,300 --> 00:02:08,340
 In the Kaya is defined by the another word Sarira

35
00:02:08,340 --> 00:02:12,500
 So the word Sarira is a synonym for Kaya in

36
00:02:12,500 --> 00:02:14,980
 Pali

37
00:02:14,980 --> 00:02:16,780
 There may not be

38
00:02:16,780 --> 00:02:18,580
 another word for

39
00:02:18,580 --> 00:02:20,580
 body in in English

40
00:02:20,580 --> 00:02:24,220
 So although it is

41
00:02:24,220 --> 00:02:28,570
 The word carcass is used we should understand it is not a

42
00:02:28,570 --> 00:02:29,380
 dead body

43
00:02:29,380 --> 00:02:34,580
 Because we we try to see in our living body the head hair

44
00:02:34,580 --> 00:02:35,180
 body hair

45
00:02:35,180 --> 00:02:38,300
 So I try to see the repulsiveness of these things

46
00:02:39,020 --> 00:02:43,420
 I think carcass refers like to the frame you know can be a

47
00:02:43,420 --> 00:02:44,620
 frame the frame of the body was

48
00:02:44,620 --> 00:02:47,460
 emptied by culture

49
00:02:47,460 --> 00:02:50,940
 But yeah, just the body just afraid

50
00:02:50,940 --> 00:02:54,900
 Not another frame here. What is mine with just a bottle?

51
00:02:54,900 --> 00:02:58,640
 because we have to find that here body here

52
00:02:58,640 --> 00:03:02,260
 flesh and all these things in the body and

53
00:03:05,100 --> 00:03:08,660
 Because such five things as the head hair etc. And the

54
00:03:08,660 --> 00:03:11,840
 hundred diseases beginning with eye disease

55
00:03:11,840 --> 00:03:17,340
 F it as their origin so the word Kaya and Pali is

56
00:03:17,340 --> 00:03:21,140
 Explained

57
00:03:21,140 --> 00:03:23,140
 to be a

58
00:03:23,140 --> 00:03:27,140
 compound word KU plus Aya

59
00:03:27,140 --> 00:03:31,380
 so KU plus Aya becomes Kaya

60
00:03:32,740 --> 00:03:35,220
 according to Pali grammar and

61
00:03:35,220 --> 00:03:38,460
 KU here means vile

62
00:03:38,460 --> 00:03:45,250
 KU means despicable or something and Aya means the place of

63
00:03:45,250 --> 00:03:47,180
 the origin here the origin

64
00:03:47,180 --> 00:03:52,220
 the place of head hair and so on and the hundred diseases

65
00:03:52,220 --> 00:03:57,380
 So this is that that is why the body is called Kaya in Pali

66
00:03:57,380 --> 00:04:00,480
 This is the explanation of the word

67
00:04:01,100 --> 00:04:04,740
 and then that it will pass head hair body hairs

68
00:04:04,740 --> 00:04:10,120
 So no one who searches throughout the whole of this pattern

69
00:04:10,120 --> 00:04:11,060
 long carcass

70
00:04:11,060 --> 00:04:13,700
 Studying upward from the soul sort of feet studying

71
00:04:13,700 --> 00:04:16,050
 downward from the top of the head and starting from the

72
00:04:16,050 --> 00:04:16,500
 skin all around

73
00:04:16,500 --> 00:04:20,880
 Ever finds even the minutest at them at all beautiful in it

74
00:04:20,880 --> 00:04:21,420
 such as a

75
00:04:21,420 --> 00:04:25,020
 Gem or barrel. I don't know what barrier is or

76
00:04:25,580 --> 00:04:29,100
 Elos or Saffron or camp or a tail camper on the contrary he

77
00:04:29,100 --> 00:04:31,060
 finds nothing but the various very

78
00:04:31,060 --> 00:04:35,070
 Melodorous offensive dread-looking sort of field consisting

79
00:04:35,070 --> 00:04:37,480
 of the head hairs body hairs and the rest

80
00:04:37,480 --> 00:04:42,410
 Now when you want to practice this kind of meditation the

81
00:04:42,410 --> 00:04:44,860
 first thing you have to do is

82
00:04:44,860 --> 00:04:47,940
 recite

83
00:04:47,940 --> 00:04:50,900
 So this meditation subject consists in giving it

84
00:04:51,660 --> 00:04:56,490
 So first if you learn the seven full skill in learning that

85
00:04:56,490 --> 00:04:59,540
 mean in learning the meditation subject and they are

86
00:04:59,540 --> 00:05:04,380
 for our recitation one to mental recitation and

87
00:05:04,380 --> 00:05:09,060
 Three has to color for as to shape five as to direction six

88
00:05:09,060 --> 00:05:11,340
 as to location and seven as to

89
00:05:11,340 --> 00:05:15,140
 De-limitation we have to understand all these with regard

90
00:05:15,140 --> 00:05:18,220
 to the 32 parts. So the first thing is

91
00:05:18,900 --> 00:05:22,770
 You learn you learn this work by heart. They had everybody

92
00:05:22,770 --> 00:05:25,240
 has made these games in use and so on

93
00:05:25,240 --> 00:05:33,640
 Sometimes the first definition is the dead body of an

94
00:05:33,640 --> 00:05:34,820
 animal especially one

95
00:05:34,820 --> 00:05:38,900
 But the second one is the living body of a human being so

96
00:05:38,900 --> 00:05:40,180
 yeah, you see it away

97
00:05:40,180 --> 00:05:44,740
 Yeah, so that is what what we need here the second second

98
00:05:45,140 --> 00:05:48,210
 Sometimes it is a framework or basic structure as of a room

99
00:05:48,210 --> 00:05:49,300
 building. So it has

100
00:05:49,300 --> 00:06:01,020
 So the first thing we have to do is recite

101
00:06:01,020 --> 00:06:06,860
 Even if one is master of the t-pityga the verbal recitation

102
00:06:06,860 --> 00:06:09,220
 could still be done at the time of first giving it

103
00:06:09,220 --> 00:06:09,920
 attention

104
00:06:09,920 --> 00:06:12,260
 but even though you are

105
00:06:12,260 --> 00:06:15,140
 Well familiar with the the three three pitygas

106
00:06:15,140 --> 00:06:18,020
 you must

107
00:06:18,020 --> 00:06:24,020
 You must do the the recitation first

108
00:06:24,020 --> 00:06:28,660
 For the meditation subject only becomes evident to some

109
00:06:28,660 --> 00:06:29,060
 true

110
00:06:29,060 --> 00:06:32,600
 recitation as it did to the two elders who learned the

111
00:06:32,600 --> 00:06:35,180
 meditation subject from the elder Mahadeva of the hill

112
00:06:35,180 --> 00:06:35,620
 country

113
00:06:35,620 --> 00:06:37,980
 On being asked for the meditation subject

114
00:06:37,980 --> 00:06:42,700
 It seems the elder gave the text of the 32 aspect saying do

115
00:06:42,700 --> 00:06:45,340
 only this recitation for four months

116
00:06:45,340 --> 00:06:48,540
 So you you you recite these that you do but for four months

117
00:06:48,540 --> 00:06:50,100
 and these people

118
00:06:50,100 --> 00:06:54,150
 Although they were familiar respectively with with two and

119
00:06:54,150 --> 00:06:55,940
 three pityga. That means

120
00:06:55,940 --> 00:06:58,460
 They know

121
00:06:58,460 --> 00:07:01,580
 Two pitygas, they know three pitygas. So they are learned

122
00:07:01,580 --> 00:07:03,940
 person. But this mark and this is

123
00:07:03,940 --> 00:07:06,180
 elder is

124
00:07:06,180 --> 00:07:10,730
 Telling them that you recite these that you do pass the

125
00:07:10,730 --> 00:07:12,220
 they must have

126
00:07:12,220 --> 00:07:16,280
 They must already have learned these that into path

127
00:07:16,280 --> 00:07:19,900
 actually, but they accept that is his

128
00:07:19,900 --> 00:07:23,660
 Advice so although they were familiar respectively

129
00:07:23,660 --> 00:07:27,140
 typically with two or three pitygas it was

130
00:07:27,140 --> 00:07:30,570
 It was only at the end of four months of recitation of the

131
00:07:30,570 --> 00:07:34,140
 meditation subject that they became stream and errors

132
00:07:35,580 --> 00:07:40,100
 Actually, it means they became stream and errors just

133
00:07:40,100 --> 00:07:44,860
 reciting the three and 32 parts and

134
00:07:44,860 --> 00:07:48,660
 Here

135
00:07:48,660 --> 00:07:54,580
 They became stream and friends

136
00:07:54,580 --> 00:08:01,790
 Not just by recitation first they did the recitation and

137
00:08:01,790 --> 00:08:02,100
 then

138
00:08:02,500 --> 00:08:04,940
 They review each part as

139
00:08:04,940 --> 00:08:08,260
 Foul

140
00:08:08,260 --> 00:08:11,040
 Load some and then they develop

141
00:08:11,040 --> 00:08:15,060
 We person on it. So without we buzz another can be no

142
00:08:15,060 --> 00:08:16,620
 stream entry

143
00:08:16,620 --> 00:08:19,780
 so although it is set here or

144
00:08:19,780 --> 00:08:25,160
 It would appear that they just reside and became sort of

145
00:08:25,160 --> 00:08:26,220
 sort of banners

146
00:08:26,220 --> 00:08:29,690
 I mean stream entrance, but that is not the case. They

147
00:08:29,690 --> 00:08:31,180
 reside it can be

148
00:08:31,540 --> 00:08:34,420
 for four months they beside that these back and forth back

149
00:08:34,420 --> 00:08:37,820
 and forth and then they got the

150
00:08:37,820 --> 00:08:43,140
 Sign

151
00:08:43,140 --> 00:08:46,380
 counterpart sign and so on and they became

152
00:08:46,380 --> 00:08:49,100
 sort of an us

153
00:08:49,100 --> 00:08:52,440
 After practicing reposs are now on it. So without we bust

154
00:08:52,440 --> 00:08:55,460
 maybe I can be most cream entrance the screen entry

155
00:08:55,460 --> 00:09:00,700
 And with right apprehension of the text

156
00:09:01,300 --> 00:09:03,300
 and then that is

157
00:09:03,300 --> 00:09:08,100
 Not not the translating of the Pali word the Pali word

158
00:09:08,100 --> 00:09:10,700
 there is but that kid not got he died

159
00:09:10,700 --> 00:09:16,700
 That means since they take his advice with respect

160
00:09:16,700 --> 00:09:19,100
 but that King nagahi means

161
00:09:19,100 --> 00:09:23,860
 Taking with respect now they they were learned learned

162
00:09:23,860 --> 00:09:26,420
 these two are learned months

163
00:09:26,420 --> 00:09:29,340
 But when the teacher told them

164
00:09:30,020 --> 00:09:33,940
 You recite the 32 parts for four months. They didn't say we

165
00:09:33,940 --> 00:09:36,480
 have already learned it and we don't have to recite the

166
00:09:36,480 --> 00:09:37,100
 song like that

167
00:09:37,100 --> 00:09:41,450
 They were they have too much respect for for the teacher to

168
00:09:41,450 --> 00:09:42,100
 refuse

169
00:09:42,100 --> 00:09:46,800
 so following with respect the advice of the teacher they

170
00:09:46,800 --> 00:09:47,580
 resided for

171
00:09:47,580 --> 00:09:51,690
 They just recited them for four months and during that time

172
00:09:51,690 --> 00:09:52,300
 to begin

173
00:09:52,300 --> 00:09:55,100
 string enter us

174
00:09:55,580 --> 00:09:58,870
 So with right apprehension of the text, this is not the

175
00:09:58,870 --> 00:10:01,420
 right right translation of the Pali word

176
00:10:01,420 --> 00:10:07,260
 Now when he does the recitation he should divide it up into

177
00:10:07,260 --> 00:10:10,940
 the skin pan set etc and do it forwards and very much

178
00:10:10,940 --> 00:10:18,500
 It took me about one and a half hours

179
00:10:24,660 --> 00:10:26,340
 So

180
00:10:26,340 --> 00:10:28,340
 Please look at the note

181
00:10:28,340 --> 00:10:30,860
 the part is

182
00:10:30,860 --> 00:10:35,660
 32 parts head has body has male teeth skin

183
00:10:35,660 --> 00:10:39,020
 That is the skin panther

184
00:10:39,020 --> 00:10:41,840
 That means the fight ending with skin

185
00:10:41,840 --> 00:10:46,180
 Then next one flesh they news bone bone. I will get me

186
00:10:46,180 --> 00:10:51,980
 fight ending with hitting and then five ending with lights

187
00:10:51,980 --> 00:10:53,740
 and then

188
00:10:55,700 --> 00:10:59,380
 Five ending with brain and then

189
00:10:59,380 --> 00:11:06,180
 Five ending with fat and

190
00:11:06,180 --> 00:11:12,220
 Then six six ending with fat and

191
00:11:12,220 --> 00:11:19,210
 Six ending with urine. So 32 parts are divided into five

192
00:11:19,210 --> 00:11:21,620
 five five five six six

193
00:11:23,100 --> 00:11:25,100
 And

194
00:11:25,100 --> 00:11:27,900
 Residitions should be done this way now

195
00:11:27,900 --> 00:11:32,620
 There is another book

196
00:11:32,620 --> 00:11:36,860
 Another commentary written by the same order

197
00:11:36,860 --> 00:11:41,260
 And it is the

198
00:11:41,260 --> 00:11:44,280
 commentary on the second book of a beat up in the second

199
00:11:44,280 --> 00:11:46,740
 book of a beat of mother is mentioned of the

200
00:11:47,180 --> 00:11:50,540
 Four four foundations of mindfulness and so these are

201
00:11:50,540 --> 00:11:51,820
 mentioned there too

202
00:11:51,820 --> 00:11:58,850
 now the recitation I gave on these notes are the

203
00:11:58,850 --> 00:12:00,060
 combination of

204
00:12:00,060 --> 00:12:04,670
 Statements found in two commentaries both in the path of

205
00:12:04,670 --> 00:12:07,820
 purification and the other book

206
00:12:07,820 --> 00:12:10,980
 So just in the beauty this

207
00:12:10,980 --> 00:12:13,780
 the book path of purification

208
00:12:13,780 --> 00:12:16,540
 there is no mention of

209
00:12:17,740 --> 00:12:19,860
 How many days you are to recite

210
00:12:19,860 --> 00:12:24,740
 each each group and how how to

211
00:12:24,740 --> 00:12:30,820
 How to recite forward and backward and so on but they are

212
00:12:30,820 --> 00:12:35,740
 The man that is given so I combined these two into one

213
00:12:35,740 --> 00:12:39,460
 And it will take how many days

214
00:12:39,460 --> 00:12:43,180
 165 days just to recite

215
00:12:43,220 --> 00:12:46,780
 So you have to spend five and a half months

216
00:12:46,780 --> 00:12:50,260
 Just reciting verbally

217
00:12:50,260 --> 00:12:54,650
 It will take a long time to practice this meditation. So

218
00:12:54,650 --> 00:12:55,980
 the first one

219
00:12:55,980 --> 00:13:03,140
 We say head has body has nail teeth skin. So forward this

220
00:13:03,140 --> 00:13:04,860
 this this way you

221
00:13:04,860 --> 00:13:07,100
 recite five days

222
00:13:07,220 --> 00:13:11,000
 So head has body has nail teeth skin head has body has nail

223
00:13:11,000 --> 00:13:13,900
 teeth skin thousands and thousands of times

224
00:13:13,900 --> 00:13:16,740
 and

225
00:13:16,740 --> 00:13:20,860
 Then the next five days you go backwards

226
00:13:20,860 --> 00:13:27,540
 skin teeth nails body has head hairs skin teeth nails body

227
00:13:27,540 --> 00:13:28,780
 hairs head hairs

228
00:13:28,780 --> 00:13:33,240
 Just verbal recitation just that yeah, just verbal rec

229
00:13:33,240 --> 00:13:33,700
itation

230
00:13:33,700 --> 00:13:36,300
 but when you when you

231
00:13:36,420 --> 00:13:38,420
 recite verbal

232
00:13:38,420 --> 00:13:41,580
 Residation you also know the meaning of the words. So the

233
00:13:41,580 --> 00:13:42,180
 meanings

234
00:13:42,180 --> 00:13:46,940
 May soak into your mind and then then there will come next

235
00:13:46,940 --> 00:13:48,620
 mental recitation

236
00:13:48,620 --> 00:13:53,520
 The first part verbal recitation and it alone will take 165

237
00:13:53,520 --> 00:13:53,900
 days

238
00:13:53,900 --> 00:14:00,880
 So and then forward and backward so that is

239
00:14:01,980 --> 00:14:07,400
 That has body has nail teeth skin skin teeth news head has

240
00:14:07,400 --> 00:14:09,500
 body has back and forth back and forth

241
00:14:09,500 --> 00:14:13,920
 So it was for five days. So for the skin pent up it will

242
00:14:13,920 --> 00:14:15,620
 take 15 days and

243
00:14:15,620 --> 00:14:18,860
 Then next next panda

244
00:14:18,860 --> 00:14:22,460
 Next man that is what

245
00:14:22,460 --> 00:14:27,260
 kidney pendant so flesh sinews bone bone marrow kidney

246
00:14:27,260 --> 00:14:30,740
 forward for five days

247
00:14:30,780 --> 00:14:33,100
 But when you say backwards

248
00:14:33,100 --> 00:14:40,500
 You say not just these five but the previous five also so

249
00:14:40,500 --> 00:14:43,340
 the backwards for kidney pendant is

250
00:14:43,340 --> 00:14:50,900
 Kidney bone marrow bones in use flesh skin teeth news body

251
00:14:50,900 --> 00:14:52,780
 has got a effective number one

252
00:14:52,780 --> 00:14:55,460
 so if you look at the

253
00:14:55,860 --> 00:15:01,180
 Note I put the numbers because it is very boring to do type

254
00:15:01,180 --> 00:15:02,600
 these words again

255
00:15:02,600 --> 00:15:05,460
 even the number

256
00:15:05,460 --> 00:15:11,680
 So kidney pendant at the last five days six seven eight

257
00:15:11,680 --> 00:15:16,500
 nine ten ten nine eight seven six five four three two one

258
00:15:16,500 --> 00:15:16,940
 and

259
00:15:16,940 --> 00:15:22,130
 Then after these two pandas you combine the two pandas and

260
00:15:22,130 --> 00:15:24,780
 then recite them for 15 days

261
00:15:25,100 --> 00:15:29,020
 So that is one two three four five six seven eight nine ten

262
00:15:29,020 --> 00:15:29,940
. It is four

263
00:15:29,940 --> 00:15:32,640
 So one two three four five six seven eight nine ten and so

264
00:15:32,640 --> 00:15:36,080
 on and then backwards ten nine eight seven six five four

265
00:15:36,080 --> 00:15:36,300
 three

266
00:15:36,300 --> 00:15:39,000
 Two one ten nine eight seven six five four three two one

267
00:15:39,000 --> 00:15:41,980
 and then forward and backward one two three four five six

268
00:15:41,980 --> 00:15:42,180
 seven

269
00:15:42,180 --> 00:15:45,460
 Nine ten ten nine eight seven six five four three two one

270
00:15:45,460 --> 00:15:49,060
 That's 15 days and then the next panda

271
00:15:49,500 --> 00:15:55,310
 11 12 13 14 15 11 12 13 14 15 and so on and then backward

272
00:15:55,310 --> 00:15:56,340
 you go back to number one

273
00:15:56,340 --> 00:16:03,540
 until number one 15 14 13 12 11 10 9 8 7 6 5 4 6 5 4 3 2 1

274
00:16:03,540 --> 00:16:04,520
 and then the

275
00:16:04,520 --> 00:16:08,780
 Third five days you recite forward and backward

276
00:16:08,780 --> 00:16:11,500
 so this way

277
00:16:11,500 --> 00:16:16,660
 After each panda you combine with the previous panda

278
00:16:16,740 --> 00:16:21,060
 So two pandas combined together three pandas together four

279
00:16:21,060 --> 00:16:22,340
 pandas together

280
00:16:22,340 --> 00:16:26,260
 five and five groups together and

281
00:16:26,260 --> 00:16:30,620
 Six groups together. So you go this way

282
00:16:30,620 --> 00:16:34,540
 And it will take 165 days

283
00:16:42,820 --> 00:16:49,700
 Most of the day if you're a monk you have to go out for al

284
00:16:49,700 --> 00:16:52,500
ms in the morning and you don't you cannot do

285
00:16:52,500 --> 00:16:56,320
 Even when you are going for alms you can you can do rec

286
00:16:56,320 --> 00:16:58,660
itation or at least mental recitation

287
00:16:58,660 --> 00:17:05,700
 Because monks are

288
00:17:06,580 --> 00:17:11,470
 taught or instructed to go with meditation when they go to

289
00:17:11,470 --> 00:17:12,780
 go for alms in the

290
00:17:12,780 --> 00:17:15,700
 in the village and the city

291
00:17:15,700 --> 00:17:20,190
 So if a monk goes without meditation, he's not supposed to

292
00:17:20,190 --> 00:17:21,940
 be a practicing monk

293
00:17:21,940 --> 00:17:28,870
 So you go to the village or city with some some kind of

294
00:17:28,870 --> 00:17:30,660
 meditation sometimes you may be

295
00:17:30,660 --> 00:17:34,600
 Practicing just mindfulness meditation sometimes this kind

296
00:17:34,600 --> 00:17:35,260
 of meditation

297
00:17:36,100 --> 00:17:38,100
 So almost the whole day

298
00:17:38,100 --> 00:17:42,180
 Because this is

299
00:17:42,180 --> 00:17:48,390
 Intensive practice not not just saying for for a few

300
00:17:48,390 --> 00:17:49,540
 minutes and then

301
00:17:49,540 --> 00:17:54,300
 So these are

302
00:17:54,300 --> 00:17:56,300
 how to recite

303
00:17:56,300 --> 00:17:59,820
 how to do verbal recitation the

304
00:18:00,580 --> 00:18:03,300
 Residations should be done verbally in this way a hundred

305
00:18:03,300 --> 00:18:05,980
 times a thousand times even in hundred thousand times

306
00:18:05,980 --> 00:18:08,660
 paragraph 56

307
00:18:08,660 --> 00:18:11,570
 For it is through verbal recitation that the meditation

308
00:18:11,570 --> 00:18:13,020
 subject becomes familiar

309
00:18:13,020 --> 00:18:15,580
 if you spend 165 days

310
00:18:15,580 --> 00:18:18,380
 it cannot but

311
00:18:18,380 --> 00:18:21,360
 become familiar and the mind being that

312
00:18:21,360 --> 00:18:24,650
 Prevented from running here and there the parts become

313
00:18:24,650 --> 00:18:27,300
 evident and seem like the fingers of effect

314
00:18:27,300 --> 00:18:29,660
 a pair of clasped hands

315
00:18:30,060 --> 00:18:32,060
 like a row of fence posts

316
00:18:32,060 --> 00:18:35,320
 Then comes a mental recitation should be done

317
00:18:35,320 --> 00:18:38,740
 Just as it is done verbally for the verbal recitation is a

318
00:18:38,740 --> 00:18:41,160
 condition for the mental recitation and the mental rec

319
00:18:41,160 --> 00:18:42,660
itation is a condition for the

320
00:18:42,660 --> 00:18:45,380
 Penetration of the characteristic of foulness

321
00:18:45,380 --> 00:18:49,600
 So when you when you recite it mentally again and again the

322
00:18:49,600 --> 00:18:54,340
 sign of wholeness will we will evidence to your mind

323
00:18:55,060 --> 00:18:58,910
 And then you have to review them as to color the color of

324
00:18:58,910 --> 00:19:01,260
 the head hairs etc. Should be defined

325
00:19:01,260 --> 00:19:04,900
 If they are black or in this country

326
00:19:04,900 --> 00:19:11,140
 Blonde and brunette

327
00:19:23,340 --> 00:19:26,380
 Yes, no no anything you can get any color

328
00:19:26,380 --> 00:19:32,060
 And as we shake the shape should be defined with it

329
00:19:32,060 --> 00:19:35,900
 They will be explained later in detail as to direction now

330
00:19:35,900 --> 00:19:36,540
 in this body

331
00:19:36,540 --> 00:19:39,500
 Upward from the navel is the upper direction upper part of

332
00:19:39,500 --> 00:19:42,620
 the body and downwards from it is the downward direction

333
00:19:42,620 --> 00:19:43,780
 lower part of the body

334
00:19:43,780 --> 00:19:46,660
 So navel is supposed to be the middle of the body

335
00:19:46,660 --> 00:19:49,640
 So that the direction should be defined as

336
00:19:49,640 --> 00:19:52,260
 This part is in this direction

337
00:19:52,620 --> 00:19:55,810
 As to location that is there their place the location of

338
00:19:55,810 --> 00:19:59,300
 this or that part should be defined the defined us

339
00:19:59,300 --> 00:20:03,740
 This part is oh doesn't this are

340
00:20:03,740 --> 00:20:08,580
 Their wrong places here right the

341
00:20:08,580 --> 00:20:12,780
 Location of this or that part should be defined thus

342
00:20:12,780 --> 00:20:16,140
 This part is established in this location

343
00:20:19,420 --> 00:20:23,170
 As to delimitation there are two kinds of delimitation that

344
00:20:23,170 --> 00:20:26,370
 is delimitation of the similar and delimitation of the diss

345
00:20:26,370 --> 00:20:26,820
imilar

346
00:20:26,820 --> 00:20:30,340
 Here in delimitation of the similar should be understood in

347
00:20:30,340 --> 00:20:30,900
 this way

348
00:20:30,900 --> 00:20:35,060
 This part is delimited above and below and around by this

349
00:20:35,060 --> 00:20:38,180
 This is called

350
00:20:38,180 --> 00:20:40,180
 delimitation of similar

351
00:20:40,180 --> 00:20:44,660
 It is like saying say this this cup is

352
00:20:45,260 --> 00:20:49,180
 delimited by on this side by the cattle and on the other

353
00:20:49,180 --> 00:20:52,240
 side by the microphone or something like that, so this is

354
00:20:52,240 --> 00:20:52,700
 called

355
00:20:52,700 --> 00:20:57,860
 delimitation of similar and of dissimilar means

356
00:20:57,860 --> 00:21:01,260
 This is here and not

357
00:21:01,260 --> 00:21:05,080
 Head hair and not body hair and when you come to body here,

358
00:21:05,080 --> 00:21:06,600
 this is body hair and not head hair

359
00:21:06,600 --> 00:21:10,320
 so delimiting like that is called delimitation of the diss

360
00:21:10,320 --> 00:21:11,020
imilar and

361
00:21:12,020 --> 00:21:15,340
 Dissimilar here means just not similar, but it does not

362
00:21:15,340 --> 00:21:16,980
 mean opposite

363
00:21:16,980 --> 00:21:23,640
 So delimitation of the dissimilar should be understood as

364
00:21:23,640 --> 00:21:25,540
 non intermixedness in

365
00:21:25,540 --> 00:21:28,910
 This way head hairs are not body hairs and body hairs and

366
00:21:28,910 --> 00:21:30,380
 not head hairs and so on

367
00:21:30,380 --> 00:21:34,140
 When the teacher tells the skill in learning in seven ways

368
00:21:34,140 --> 00:21:37,340
 does he should do so knowing that in certain sodas this?

369
00:21:37,500 --> 00:21:40,570
 Repulsiveness subject is expounded from the point of view

370
00:21:40,570 --> 00:21:43,560
 of repulsiveness and in certain sodas on the point of view

371
00:21:43,560 --> 00:21:43,820
 of

372
00:21:43,820 --> 00:21:46,580
 elements

373
00:21:46,580 --> 00:21:51,780
 For in the Mahasati, Patana soda it is expounded only as

374
00:21:51,780 --> 00:21:53,060
 repulsiveness

375
00:21:53,060 --> 00:21:57,780
 So these that you do but the body meditation are given

376
00:21:57,780 --> 00:21:59,100
 there to

377
00:21:59,100 --> 00:22:05,580
 develop the sense of repulsiveness in the Maha hot teapod

378
00:22:05,580 --> 00:22:05,780
oba

379
00:22:05,780 --> 00:22:07,780
 So that is the name of a soda

380
00:22:07,780 --> 00:22:09,860
 and so that 28 that means

381
00:22:09,860 --> 00:22:16,050
 Middle-length saying so that number 28 in the Mahara who

382
00:22:16,050 --> 00:22:17,980
 low-water soda, okay?

383
00:22:17,980 --> 00:22:22,200
 Mati Monica and the tadu banga so that again much America.

384
00:22:22,200 --> 00:22:25,220
 It is expounded as elements. So

385
00:22:25,220 --> 00:22:28,060
 they are divided into

386
00:22:28,060 --> 00:22:32,040
 two elements actually the first 20 belong to the other

387
00:22:32,040 --> 00:22:34,420
 element and the remaining belong to

388
00:22:35,260 --> 00:22:37,260
 the water element, so

389
00:22:37,260 --> 00:22:41,540
 We would regard to elements they are mentioned there

390
00:22:41,540 --> 00:22:44,260
 in the Gaya capacity soda

391
00:22:44,260 --> 00:22:49,490
 however food genres are expounded with reference to one to

392
00:22:49,490 --> 00:22:53,140
 whom it has appeared as a kala casino now

393
00:22:53,140 --> 00:22:59,300
 When you practice the 32 parts of the body meditation you

394
00:22:59,300 --> 00:23:00,060
 cannot get

395
00:23:01,700 --> 00:23:05,140
 Second Jana and so on you can get only first Jana

396
00:23:05,140 --> 00:23:10,460
 but in that soda four genres are mentioned and four genres

397
00:23:10,460 --> 00:23:12,060
 are mentioned because

398
00:23:12,060 --> 00:23:19,200
 To that month these parts appear as color not not as parts

399
00:23:19,200 --> 00:23:20,140
 of the body

400
00:23:20,140 --> 00:23:24,950
 So when they appear as color to him he pick up the color of

401
00:23:24,950 --> 00:23:25,860
 my casino

402
00:23:25,860 --> 00:23:30,520
 So color casino meditation can lead to all four or five

403
00:23:30,520 --> 00:23:31,420
 channels

404
00:23:32,140 --> 00:23:33,500
 so

405
00:23:33,500 --> 00:23:36,650
 That is why they said here for Janasa expounded with

406
00:23:36,650 --> 00:23:39,700
 reference to one to whom it has appeared as a color

407
00:23:39,700 --> 00:23:47,060
 In some of the

408
00:23:47,060 --> 00:23:51,320
 Meditation they said that you are not to pay attention to

409
00:23:51,320 --> 00:23:52,780
 color, right?

410
00:23:52,780 --> 00:23:57,210
 So if you pay attention to color it becomes color casino

411
00:23:57,210 --> 00:23:58,260
 meditation

412
00:23:58,260 --> 00:24:00,260
 So

413
00:24:00,260 --> 00:24:05,100
 Here it is

414
00:24:05,100 --> 00:24:09,330
 Here in it is an inside meditation subject that is exp

415
00:24:09,330 --> 00:24:11,500
ounded as elements and a serenity

416
00:24:11,500 --> 00:24:15,620
 Meditation subject that is expounded as repulsiveness

417
00:24:15,620 --> 00:24:18,380
 so

418
00:24:18,380 --> 00:24:22,130
 It is an inside meditation subject that is expounded as

419
00:24:22,130 --> 00:24:22,860
 elements

420
00:24:26,140 --> 00:24:30,040
 If we divide them into two two element two kinds of

421
00:24:30,040 --> 00:24:32,500
 elements and try to see the elements try to

422
00:24:32,500 --> 00:24:36,300
 Contemplate on the elements and it is it is serenity

423
00:24:36,300 --> 00:24:41,860
 It is inside meditation and

424
00:24:41,860 --> 00:24:48,040
 It is serenity or somata meditation subject that is exp

425
00:24:48,040 --> 00:24:49,580
ounded that repulsiveness

426
00:24:49,580 --> 00:24:52,820
 So when you see the try to see the repulsiveness of these

427
00:24:52,820 --> 00:24:55,140
 things it is somata meditation

428
00:24:55,740 --> 00:24:58,520
 Consequently, it is only the serenity meditation subject

429
00:24:58,520 --> 00:25:00,340
 that is relevant here

430
00:25:00,340 --> 00:25:03,810
 So in this in this part of the book only the somata

431
00:25:03,810 --> 00:25:06,020
 meditation is mentioned. So

432
00:25:06,020 --> 00:25:13,340
 We should understand and this meditation here as serenity

433
00:25:13,340 --> 00:25:14,940
 or somata meditation

434
00:25:14,940 --> 00:25:18,900
 And then ten full skill in giving attention

435
00:25:18,900 --> 00:25:30,420
 I think we'll do it next week

436
00:25:30,420 --> 00:25:34,780
 So next week

437
00:25:34,780 --> 00:25:41,580
 Why do we end up I think to the end of

438
00:25:41,580 --> 00:26:06,580
 We get to breathing yeah, so yes, right 280

439
00:26:07,540 --> 00:26:09,540
 85

440
00:26:09,540 --> 00:26:11,540
 85

441
00:26:11,540 --> 00:26:13,540
 85

442
00:26:13,540 --> 00:26:15,540
 85

443
00:26:15,540 --> 00:26:17,540
 85

444
00:26:17,540 --> 00:26:19,540
 85

445
00:26:19,540 --> 00:26:21,540
 85

446
00:26:21,540 --> 00:26:23,540
 85

447
00:26:23,540 --> 00:26:25,540
 85

448
00:26:25,540 --> 00:26:27,540
 85

449
00:26:27,540 --> 00:26:29,540
 85

450
00:26:29,540 --> 00:26:31,540
 85

451
00:26:31,540 --> 00:26:33,540
 85

452
00:26:33,540 --> 00:26:35,540
 85

453
00:26:35,540 --> 00:26:37,540
 85

454
00:26:37,540 --> 00:26:39,540
 85

455
00:26:39,540 --> 00:26:41,540
 85

456
00:26:41,540 --> 00:26:43,540
 85

457
00:26:43,540 --> 00:26:45,540
 85

458
00:26:45,540 --> 00:26:47,540
 85

459
00:26:47,540 --> 00:26:49,540
 85

460
00:26:49,540 --> 00:26:51,540
 85

461
00:26:51,540 --> 00:26:53,540
 85

462
00:26:53,540 --> 00:26:55,540
 85

463
00:26:55,540 --> 00:26:57,540
 85

464
00:26:57,540 --> 00:26:59,540
 85

465
00:26:59,540 --> 00:27:01,540
 85

466
00:27:01,540 --> 00:27:03,540
 85

467
00:27:03,540 --> 00:27:05,540
 85

468
00:27:05,540 --> 00:27:07,540
 85

469
00:27:07,540 --> 00:27:09,540
 85

470
00:27:09,540 --> 00:27:11,540
 85

471
00:27:11,540 --> 00:27:13,540
 85

472
00:27:13,540 --> 00:27:15,540
 85

473
00:27:15,540 --> 00:27:17,540
 85

474
00:27:17,540 --> 00:27:19,540
 85

475
00:27:19,540 --> 00:27:21,540
 85

476
00:27:21,540 --> 00:27:23,540
 85

477
00:27:23,540 --> 00:27:25,540
 85

478
00:27:25,540 --> 00:27:27,540
 85

479
00:27:27,540 --> 00:27:29,540
 85

480
00:27:29,540 --> 00:27:31,540
 85

481
00:27:31,540 --> 00:27:33,540
 85

482
00:27:33,540 --> 00:27:35,540
 85

483
00:27:35,540 --> 00:27:37,540
 85

484
00:27:37,540 --> 00:27:39,540
 85

485
00:27:39,540 --> 00:27:41,540
 85

486
00:27:41,540 --> 00:27:43,540
 85

487
00:27:43,540 --> 00:27:45,540
 85

488
00:27:45,540 --> 00:27:47,540
 85

489
00:27:47,540 --> 00:27:49,540
 85

490
00:27:49,540 --> 00:27:51,540
 85

491
00:27:51,540 --> 00:27:53,540
 85

492
00:27:53,540 --> 00:27:55,540
 85

493
00:27:55,540 --> 00:27:57,540
 85

494
00:27:57,540 --> 00:27:59,540
 85

495
00:27:59,540 --> 00:28:01,540
 85

496
00:28:01,540 --> 00:28:03,540
 85

497
00:28:03,540 --> 00:28:05,540
 85

498
00:28:05,540 --> 00:28:07,540
 85

499
00:28:07,540 --> 00:28:09,540
 85

500
00:28:09,540 --> 00:28:11,540
 85

501
00:28:11,540 --> 00:28:13,540
 85

502
00:28:13,540 --> 00:28:15,540
 85

503
00:28:15,540 --> 00:28:17,540
 85

504
00:28:17,540 --> 00:28:19,540
 85

505
00:28:19,540 --> 00:28:21,540
 85

506
00:28:21,540 --> 00:28:23,540
 85

507
00:28:23,540 --> 00:28:25,540
 85

508
00:28:25,540 --> 00:28:27,540
 85

509
00:28:27,540 --> 00:28:29,540
 85

510
00:28:29,540 --> 00:28:31,540
 85

511
00:28:31,540 --> 00:28:33,540
 85

512
00:28:33,540 --> 00:28:35,540
 85

513
00:28:37,540 --> 00:28:39,540
 85

514
00:28:39,540 --> 00:28:41,540
 85

515
00:28:41,540 --> 00:28:43,540
 85

516
00:28:43,540 --> 00:28:45,540
 85

517
00:28:45,540 --> 00:28:47,540
 85

518
00:28:47,540 --> 00:28:49,540
 85

519
00:28:49,540 --> 00:28:51,540
 85

520
00:28:51,540 --> 00:28:53,540
 85

521
00:28:53,540 --> 00:28:55,540
 85

522
00:28:55,540 --> 00:28:57,540
 85

523
00:28:57,540 --> 00:28:59,540
 85

524
00:28:59,540 --> 00:29:01,540
 85

525
00:29:01,540 --> 00:29:03,540
 85

526
00:29:03,540 --> 00:29:05,540
 85

527
00:29:05,540 --> 00:29:07,540
 85

528
00:29:07,540 --> 00:29:09,540
 85

529
00:29:09,540 --> 00:29:11,540
 85

530
00:29:11,540 --> 00:29:13,540
 85

531
00:29:13,540 --> 00:29:15,540
 85

532
00:29:15,540 --> 00:29:17,540
 85

533
00:29:17,540 --> 00:29:19,540
 85

534
00:29:19,540 --> 00:29:21,540
 85

535
00:29:21,540 --> 00:29:23,540
 85

536
00:29:23,540 --> 00:29:25,540
 85

537
00:29:25,540 --> 00:29:27,540
 85

