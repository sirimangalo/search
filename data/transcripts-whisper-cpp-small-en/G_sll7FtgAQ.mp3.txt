 Hi everyone, welcome back to Ask a Monk.
 I'm getting a lot of different questions here, and I'm
 probably not going to be able to answer
 all of them, I'll try my best.
 But some of the questions I get reoccur or are related, and
 so I'm going to try to answer
 some of them in groups.
 The first group that I often get, and I've got from a long
 time and I've always avoided
 answering, is about monks life and about my life in
 specific.
 And honestly I don't think my life is a particularly good
 example of what a monk should be.
 I've had an interesting and tumultuous monks life, although
 it might be indicative of the
 times and exemplary or a good example of what you have to
 put up with in becoming a monk.
 But becoming a monk is basically giving up all of the
 external attachments that we have
 in favor of a more protected lifestyle.
 And by protected I really mean protected from oneself.
 So a lot of the temptations in an external sense are gone.
 We're surrounded by the Buddha's teaching, we're surrounded
 by meditators, we're surrounded
 by other people doing the same sort of thing or who are
 interested in mental development.
 And so we have a much greater chance of developing
 ourselves in the meditation practice.
 Well at the same time a lot of the things that would tempt
 us to get caught up in addiction
 and get caught up in other ways and in wasting our time are
 gone.
 Of course it's not perfect and it certainly is an external
 aspect of our life.
 There's nothing to say that a monk is immediately going to
 become enlightened or that a lay
 person, an ordinary person can't become enlightened.
 But it certainly helps.
 The reason I became a monk is specifically that, that I
 figured it would protect me from
 a lot of the bad habits and bad tendencies that I had.
 And indeed it has.
 It's been a really good aid for me to allow me to grow
 without getting off track.
 As far as becoming a monk, it can be quite difficult
 nowadays.
 It's hard to find a place that's going to accept you to ord
ain for more than a short period
 of time.
 I myself am very much keen on having people ordain.
 Simply because I don't see much of a difference between the
 two, ordinary life and the monk's
 life since it's simply an externality.
 I mean a person who comes to meditate with me and doesn't
 become a monk and a person
 who comes to become a monk, I don't see the difference.
 I don't differentiate the two.
 If you come to meditate, you come to meditate.
 The reason you become a monk, in my mind, is because you
 want to dedicate yourself to
 long term meditation and following this path.
 So I'm keen to have people ordain, you know, once I become
 eligible to ordain, people are
 going to try to ordain our students as novices first and
 give them the basic precepts.
 The basic precepts of being a monk are chastity and poverty
.
 And the other one is the abstinence from entertainment.
 But the big ones are chastity and poverty.
 So the difference between a meditator and a monk, the monk
 has to give up money.
 The monk has to dedicate themselves to the life of poverty.
 So you're wearing only one set of robes and when I wash
 this one, I wear my second robe.
 You have three robes altogether.
 And not keeping food, not touching money and so on.
 So using things which are of benefit.
 I mean I even go so far as using obviously a computer, but
 not keeping things and not
 clinging to things and not living luxuriously.
 So you sleep on the floor.
 But you know, a lot of this is very similar to how medit
ators live.
 The difference is a meditator is short term and for a monk
 it's long term.
 As far as ceremonies and preparing and so on and so on, I
 mean that's really just technical
 details.
 The ordination of a monk is not a ceremony.
 It shouldn't be considered to be a ceremony.
 It's an act.
 It's a formal act of the Buddhist monks.
 It's like the inauguration of a president or so on or of a
 member of Congress.
 You have to go through a certain process, screening process
 and a confirmation process.
 And once you've gone through that, then you can ordain.
 It shouldn't be considered a ritual, a ceremony or some
 kind of religious observance.
 It's joining a club, so to speak, or joining a society and
 you have to be screened and so
 on.
 Okay, so those are some thoughts on the monk's life.
 One last thing that I'd like to do is give you a link to a
 very good, something that's
 much better than anything I could say on the monk's life.
 It's very poetic and very to the point.
 It's something that I always like to read again and again,
 written by a novice monk
 actually who ordained when he was old and wrote this in
 defense of the monastic life.
 Okay, so there's a link in the description of this video.
 Please follow that link if you're interested in
 understanding the real nature of going
 forth of leaving behind the home life, which is what we do
 when we ordain.
 Okay, so thanks for the questions.
 I'll try to get to some more of them tonight and in the
 next few days.
 Of course, I'm busy in real life, but I'll do my best.
