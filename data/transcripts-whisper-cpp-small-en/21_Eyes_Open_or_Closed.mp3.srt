1
00:00:00,000 --> 00:00:05,780
 Eyes open or close. Should the eyes be open or closed

2
00:00:05,780 --> 00:00:07,200
 during walking meditation?

3
00:00:07,200 --> 00:00:11,670
 The eyes should be open during walking meditation to

4
00:00:11,670 --> 00:00:13,120
 maintain balance.

5
00:00:13,120 --> 00:00:23,200
 The mind should be with the feet not the eyes.

