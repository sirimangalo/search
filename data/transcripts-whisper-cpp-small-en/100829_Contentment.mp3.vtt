WEBVTT

00:00:00.000 --> 00:00:05.760
 Okay, so today's talk is going to be about contentment.

00:00:05.760 --> 00:00:13.280
 And so here's a quote to start us off.

00:00:13.280 --> 00:00:19.440
 Health is the greatest possession. Contentment is the

00:00:19.440 --> 00:00:20.400
 greatest wealth.

00:00:20.400 --> 00:00:25.130
 Trustworthiness is the greatest relation. And nirvana is

00:00:25.130 --> 00:00:26.720
 the greatest happiness.

00:00:26.720 --> 00:00:43.270
 So today's talk is on specifically contentment and how it

00:00:43.270 --> 00:00:44.560
 relates to the Buddha's teaching.

00:00:44.560 --> 00:00:49.840
 I was asked about a week ago to give a talk on this subject

00:00:49.840 --> 00:00:49.840
.

00:00:52.000 --> 00:00:56.260
 So here we are. Thank you all for coming. Good to see you

00:00:56.260 --> 00:01:00.880
 on this lovely Sunday afternoon.

00:01:00.880 --> 00:01:04.500
 The great thing about second life is it never rains unless

00:01:04.500 --> 00:01:05.360
 you want it to.

00:01:05.360 --> 00:01:10.030
 We can always make use of this wonderful always burning

00:01:10.030 --> 00:01:11.200
 campfire.

00:01:18.800 --> 00:01:22.310
 So what does contentment have to do with the Buddha's

00:01:22.310 --> 00:01:22.800
 teaching?

00:01:22.800 --> 00:01:25.600
 How does contentment play a part in in Buddhism?

00:01:25.600 --> 00:01:34.050
 I was asked to talk about this and so I had to do a little

00:01:34.050 --> 00:01:36.640
 bit of research.

00:01:36.640 --> 00:01:51.430
 What would I know about contentment in in the Buddha's

00:01:51.430 --> 00:01:51.920
 teaching?

00:01:51.920 --> 00:01:58.300
 Leads me to understand that there are three kinds of

00:01:58.300 --> 00:02:00.000
 contentment.

00:02:05.200 --> 00:02:07.110
 And each one of these three plays a different role in

00:02:07.110 --> 00:02:08.640
 Buddhism in the Buddha's teaching.

00:02:08.640 --> 00:02:26.080
 The first type of contentment to get it out of the way

00:02:26.080 --> 00:02:29.600
 is the type of contentment that the Buddha taught against.

00:02:29.600 --> 00:02:38.000
 So you read in the Buddha's teaching that the Buddha taught

00:02:38.000 --> 00:02:40.640
 if you want to be a great being,

00:02:40.640 --> 00:02:45.760
 if you're striving for true greatness.

00:02:45.760 --> 00:02:49.600
 The Buddha said you have to be full of discontent.

00:02:49.600 --> 00:02:56.240
 And he taught against a certain type of contentment.

00:02:56.240 --> 00:03:08.650
 And so this type of contentment it's important to talk

00:03:08.650 --> 00:03:08.880
 about

00:03:08.880 --> 00:03:15.280
 because it often pops its rears its head

00:03:15.280 --> 00:03:22.240
 both in the world and in in spirituality as well.

00:03:23.360 --> 00:03:27.040
 This is a contentment with one's self.

00:03:27.040 --> 00:03:37.040
 I've often said it's a mark of a high mind

00:03:37.040 --> 00:03:43.040
 that they're constantly working to improve themselves.

00:03:43.040 --> 00:03:47.840
 I would say it's a mark that someone is,

00:03:48.480 --> 00:03:52.140
 it's a sign that someone is spiritually developed, that

00:03:52.140 --> 00:03:54.160
 they're constantly trying to develop themselves.

00:03:54.160 --> 00:04:02.010
 That the undeveloped person is known by their lack of

00:04:02.010 --> 00:04:03.840
 interest in developing themselves.

00:04:03.840 --> 00:04:06.720
 Their feeling that everything's fine the way it is.

00:04:06.720 --> 00:04:15.680
 That there's no need to develop those things that

00:04:16.640 --> 00:04:19.600
 wise people encourage us to develop and there's no need to

00:04:19.600 --> 00:04:24.720
 do away with those things that wise people encourage us to

00:04:24.720 --> 00:04:25.840
 do away with.

00:04:25.840 --> 00:04:33.040
 So

00:04:33.040 --> 00:04:56.240
 I

00:04:56.240 --> 00:05:03.440
 just make sure everyone can hear me here.

00:05:03.440 --> 00:05:12.480
 Okay that's a yes.

00:05:12.480 --> 00:05:15.630
 If you can't hear me it's probably because you don't have

00:05:15.630 --> 00:05:17.680
 voice on either that or you don't have

00:05:17.680 --> 00:05:23.690
 if you can't hear me this is useless. Can someone yeah can

00:05:23.690 --> 00:05:24.720
 someone explain

00:05:26.400 --> 00:05:29.540
 the reason why you can't hear me? It doesn't do much good

00:05:29.540 --> 00:05:33.920
 to speak to you and tell you why you can't hear me.

00:05:33.920 --> 00:05:44.080
 Okay

00:05:54.480 --> 00:06:00.960
 so it's a mark of someone who is developing that they are

00:06:00.960 --> 00:06:03.280
 interested in developing themselves.

00:06:03.280 --> 00:06:04.800
 Obviously.

00:06:04.800 --> 00:06:14.670
 And so it's one easy way to distinguish between spiritual

00:06:14.670 --> 00:06:15.840
 people and people who are

00:06:15.840 --> 00:06:20.160
 not interested in spirituality. It's also sort of a way of

00:06:20.160 --> 00:06:21.520
 reassuring ourselves that

00:06:22.240 --> 00:06:26.510
 even when our mental development is progressing with

00:06:26.510 --> 00:06:30.480
 difficulty that we should be encouraged

00:06:30.480 --> 00:06:34.490
 by the fact that we're trying and effort in the Buddha's

00:06:34.490 --> 00:06:40.640
 teaching is really a very core value.

00:06:40.640 --> 00:06:47.760
 That failure in the meditation is really only possible when

00:06:47.760 --> 00:06:49.200
 one ceases to practice

00:06:50.080 --> 00:06:56.170
 or ceases to practice correctly. But if you when you stop

00:06:56.170 --> 00:07:00.000
 trying or when you start putting out

00:07:00.000 --> 00:07:08.930
 effort in another direction in a way that is antithetical

00:07:08.930 --> 00:07:12.000
 to the path or give up your effort

00:07:12.000 --> 00:07:15.280
 that's the only way you can really fail. As long as you're

00:07:15.280 --> 00:07:17.920
 trying striving and you're watching the

00:07:17.920 --> 00:07:21.420
 breath watching the stomach when it rises and falls for

00:07:21.420 --> 00:07:25.680
 example. But you find you can't focus on it.

00:07:25.680 --> 00:07:30.800
 You try and you try and it's easy to become frustrated and

00:07:30.800 --> 00:07:35.360
 disheartened.

00:07:35.360 --> 00:07:39.840
 But we shouldn't we shouldn't become

00:07:44.160 --> 00:07:52.000
 discouraged just because we're not getting anywhere. Our

00:07:52.000 --> 00:07:55.360
 effort is our discontent and our

00:07:55.360 --> 00:08:00.030
 intention to improve ourselves. Self-improvement is a sign

00:08:00.030 --> 00:08:03.680
 of Buddhism. And so in one way this

00:08:03.680 --> 00:08:06.370
 distinguishes spiritual people between spiritual people and

00:08:06.370 --> 00:08:09.200
 people who are content with being a

00:08:09.200 --> 00:08:13.530
 mediocre person. It also distinguishes between two types of

00:08:13.530 --> 00:08:17.200
 spiritual people. The type of person

00:08:17.200 --> 00:08:20.550
 the type of spiritual person who is content with their

00:08:20.550 --> 00:08:25.760
 level of spirituality and can often become

00:08:25.760 --> 00:08:31.300
 content with simple states of bliss or happiness or sort of

00:08:31.300 --> 00:08:35.200
 pseudo spirituality. Never challenging

00:08:35.200 --> 00:08:38.610
 one's views and beliefs practicing only as it's comfortable

00:08:38.610 --> 00:08:40.720
. So you see often people when they

00:08:40.720 --> 00:08:44.040
 sit in meditation they'll sit against the wall or they'll

00:08:44.040 --> 00:08:46.320
 sit in a chair or they'll sit on a bench

00:08:46.320 --> 00:08:51.350
 or so on. And always striving to make the meditation pleas

00:08:51.350 --> 00:08:55.200
urable. And at any time when it's not

00:08:55.200 --> 00:09:06.090
 pleasurable one either stops practicing or adjusts one's

00:09:06.090 --> 00:09:07.200
 practice. These are the kind of people who

00:09:07.200 --> 00:09:10.420
 when they're sitting in meditation need everybody to be

00:09:10.420 --> 00:09:12.480
 quiet. I had a man asking me what I thought

00:09:12.480 --> 00:09:20.260
 about using earplugs in meditation and I don't think I

00:09:20.260 --> 00:09:24.640
 think it should be pretty clear that

00:09:24.640 --> 00:09:33.520
 earplugs are a sign that you have some sort of attachment

00:09:33.520 --> 00:09:37.360
 to the sound. So this sort of

00:09:37.360 --> 00:09:40.700
 contentment is one that we want to try to avoid resting on

00:09:40.700 --> 00:09:43.520
 our laurels per se. If you're

00:09:43.520 --> 00:09:47.100
 enlightened and you have no defilements left then I think

00:09:47.100 --> 00:09:49.360
 this is the only time one should become

00:09:49.360 --> 00:09:52.620
 content with one's spiritual development. But as long as we

00:09:52.620 --> 00:09:54.480
 know that in ourselves we still have

00:09:54.480 --> 00:09:59.040
 more work to do we should set ourselves on doing that work.

00:09:59.040 --> 00:10:02.800
 So this is the first sense that we

00:10:02.800 --> 00:10:05.700
 might miss this in thinking that the Buddha taught us to be

00:10:05.700 --> 00:10:07.920
 content and so don't worry, don't try,

00:10:07.920 --> 00:10:11.940
 don't do anything. Practicing meditation is too much work.

00:10:11.940 --> 00:10:14.960
 Going on meditation treats is just

00:10:14.960 --> 00:10:19.480
 torturing yourself. You should just live your life

00:10:19.480 --> 00:10:22.800
 peacefully and somehow spiritually.

00:10:23.520 --> 00:10:27.630
 The type of contentment that the Buddha did teach and

00:10:27.630 --> 00:10:28.640
 encourage

00:10:28.640 --> 00:10:32.070
 can be broken up into two types. So there are two other

00:10:32.070 --> 00:10:34.720
 types of contentment that the Buddha did

00:10:34.720 --> 00:10:39.630
 encourage. And the first type is what he generally referred

00:10:39.630 --> 00:10:42.640
 to as contentment itself and the word

00:10:42.640 --> 00:10:55.120
 he uses is santuti which means being contentment. When the

00:10:55.120 --> 00:10:56.880
 Buddha used the word contentment he most

00:10:56.880 --> 00:11:03.360
 often was referring to the conceptual objects of our

00:11:03.360 --> 00:11:07.440
 reality. So in Buddhism we separate reality

00:11:07.440 --> 00:11:12.530
 into two parts the conceptual and the ultimate. Ultimate

00:11:12.530 --> 00:11:15.440
 reality is the building blocks of

00:11:15.440 --> 00:11:20.630
 reality. Just like in physical reality in physics they talk

00:11:20.630 --> 00:11:22.640
 about what are the building blocks,

00:11:22.640 --> 00:11:25.450
 the atoms, and then they had atoms and suddenly there were

00:11:25.450 --> 00:11:27.520
 subatomic particles and so on. And

00:11:27.520 --> 00:11:30.640
 they're still not sure what is the essence of the physical

00:11:30.640 --> 00:11:32.240
 in the scientific community.

00:11:35.440 --> 00:11:40.020
 But in Buddhism we have a very good understanding of what

00:11:40.020 --> 00:11:42.000
 are the essential building blocks.

00:11:42.000 --> 00:11:44.960
 So in the physical we're talking about the four elements

00:11:44.960 --> 00:11:47.840
 and these are simply the four aspects of

00:11:47.840 --> 00:11:52.080
 matter as it's experienced. Matter can be experienced as

00:11:52.080 --> 00:11:56.080
 hot or cold, it can be experienced as

00:11:56.080 --> 00:12:02.080
 hard or soft, and it can be experienced as tense or flaccid

00:12:02.080 --> 00:12:02.160
.

00:12:03.440 --> 00:12:06.850
 The mental side of experience is the awareness of the

00:12:06.850 --> 00:12:10.320
 physical or the awareness of an object.

00:12:10.320 --> 00:12:12.550
 So when we see something and we know that we're seeing when

00:12:12.550 --> 00:12:14.000
 we hear something we know that we're

00:12:14.000 --> 00:12:16.980
 hearing. This is the ultimate reality. Everything else is

00:12:16.980 --> 00:12:19.200
 conceptual. The chair that you're sitting

00:12:19.200 --> 00:12:23.300
 in is conceptual. The word chair arises in your mind. You

00:12:23.300 --> 00:12:25.200
 only know that it's a chair.

00:12:27.440 --> 00:12:33.760
 Once you process the experience in your mind. So without

00:12:33.760 --> 00:12:36.080
 the processing in the mind,

00:12:36.080 --> 00:12:39.120
 there's simply the feelings that arise from touching the

00:12:39.120 --> 00:12:40.720
 chair. And when you see it there's

00:12:40.720 --> 00:12:44.870
 a scene in the same way that the seats that some of us are

00:12:44.870 --> 00:12:47.120
 sitting on here in the virtual

00:12:47.120 --> 00:12:51.600
 reality are only conceptual. So the seats in Second Life

00:12:51.600 --> 00:12:55.760
 that we're seeing are our seats because we

00:12:55.760 --> 00:13:00.870
 process the sight. The seats that we're sitting on in real

00:13:00.870 --> 00:13:03.920
 life are conceptual. They're only seats

00:13:03.920 --> 00:13:07.290
 because we process the feeling or the sight or so on. But

00:13:07.290 --> 00:13:09.920
 at any rate it's still a concept. Our body

00:13:09.920 --> 00:13:15.110
 is a concept because the reality is the experience. This is

00:13:15.110 --> 00:13:17.200
 according to the Buddha.

00:13:17.200 --> 00:13:21.050
 So the first type of contentment is talking about

00:13:21.050 --> 00:13:24.400
 conceptual contentment. Contentment with things.

00:13:25.120 --> 00:13:30.110
 Contentment with objects. This is one of those teachings

00:13:30.110 --> 00:13:35.120
 that is not exactly core. So it's not

00:13:35.120 --> 00:13:38.290
 a theoretical teaching. It's a practical teaching for

00:13:38.290 --> 00:13:43.120
 everyday life. And for most of us it's a very

00:13:43.120 --> 00:13:50.590
 important teaching. One of the criticisms of many types of

00:13:50.590 --> 00:13:53.840
 Buddhism, modern Buddhism, is that it

00:13:53.840 --> 00:13:59.600
 often overlooks practical but conventional teachings like

00:13:59.600 --> 00:14:02.000
 being content with possessions,

00:14:02.000 --> 00:14:04.340
 being content with the things that you own with your

00:14:04.340 --> 00:14:08.160
 belongings. And often though people dive

00:14:08.160 --> 00:14:11.690
 right into the theoretical and talking about ultimate

00:14:11.690 --> 00:14:15.120
 reality and so on. And not that this

00:14:15.120 --> 00:14:19.450
 isn't the most important but we have so many delusions and

00:14:19.450 --> 00:14:21.760
 we're so attached to the conceptual

00:14:21.760 --> 00:14:26.130
 reality that it's like overlooking a very important part of

00:14:26.130 --> 00:14:28.240
 the path which is going to be our

00:14:28.240 --> 00:14:32.000
 relationship with those things that we cling to. Our

00:14:32.000 --> 00:14:33.680
 relationship with those things, those

00:14:33.680 --> 00:14:37.970
 conceptual objects that we interact with. So on a practical

00:14:37.970 --> 00:14:40.640
 level it's very important that we come

00:14:40.640 --> 00:14:49.040
 to terms with our needs, our materialistic desires. Not

00:14:49.040 --> 00:14:52.720
 being content with our clothes,

00:14:52.720 --> 00:14:56.950
 the clothes that we wear. The Buddha said you use clothing

00:14:56.950 --> 00:14:59.200
 to protect the body, to cover up the

00:15:00.640 --> 00:15:07.550
 parts of the body that are best left covered. You use food

00:15:07.550 --> 00:15:11.360
 simply for doing away with

00:15:11.360 --> 00:15:22.240
 hunger to bring energy to the body. You use shelter simply

00:15:22.240 --> 00:15:24.160
 to guard from the elements

00:15:24.160 --> 00:15:31.420
 and medicines to guard from guard off sickness. And the

00:15:31.420 --> 00:15:32.320
 Buddha said these are the four

00:15:32.320 --> 00:15:38.400
 these are the four requisites of a human being. These four

00:15:38.400 --> 00:15:42.000
 things are what we all cannot do

00:15:42.000 --> 00:15:45.360
 without. But for most of us that's not nearly enough.

00:15:45.360 --> 00:15:47.520
 Especially in modern times we're

00:15:47.520 --> 00:15:53.680
 incredibly materialistic. So we are always needing more and

00:15:53.680 --> 00:15:58.080
 we become slaves actually of our

00:15:58.080 --> 00:16:01.510
 belongings and of our addictions that we always need more

00:16:01.510 --> 00:16:06.160
 and more. And as a result our minds are

00:16:06.160 --> 00:16:11.390
 not focused on the here and the now. We're not content with

00:16:11.390 --> 00:16:14.240
 this state of reality. We need another

00:16:14.240 --> 00:16:17.060
 state where there's this object or that object where we can

00:16:17.060 --> 00:16:18.720
 see this thing or hear that thing

00:16:18.720 --> 00:16:23.490
 or feel this or smell that or taste this or even simply

00:16:23.490 --> 00:16:25.520
 just conceive of the fact that we own this

00:16:25.520 --> 00:16:28.800
 and own that and we have so many belongings this and that.

00:16:28.800 --> 00:16:33.520
 Delicious food, beautiful clothes,

00:16:36.560 --> 00:16:44.970
 games and toys and possessions of all sorts. And because

00:16:44.970 --> 00:16:47.600
 our mind is constantly thinking

00:16:47.600 --> 00:16:50.450
 about getting more and more and more and is really addicted

00:16:50.450 --> 00:16:51.760
 to the pleasure that comes from

00:16:51.760 --> 00:16:55.090
 getting what we want, it's very difficult for us to find

00:16:55.090 --> 00:16:57.360
 contentment. It's very difficult for us to

00:16:57.360 --> 00:17:01.680
 stay in the present reality to accept reality for what it

00:17:01.680 --> 00:17:04.640
 is because we're so used to being able to

00:17:06.640 --> 00:17:11.440
 categorize reality or to compartmentalize reality.

00:17:11.440 --> 00:17:15.580
 We don't have to accept everything. We don't have to accept

00:17:15.580 --> 00:17:18.400
 reality for what it is because when it's

00:17:18.400 --> 00:17:21.370
 not the way we like we have this idea that we can change it

00:17:21.370 --> 00:17:23.360
, that we can control it. And so we

00:17:23.360 --> 00:34:08.800
 set ourselves on quite an imbalanced state, imbalanced path

00:34:08.800 --> 00:17:33.840
 and are always unfulfilled,

00:17:33.840 --> 00:17:37.740
 always unsatisfied and always in a state of discontent. And

00:17:37.740 --> 00:17:39.600
 at any time that we don't get

00:17:39.600 --> 00:17:43.920
 what we want, we crash. The perpetual building up and

00:17:43.920 --> 00:17:47.120
 building up of greater states of attachment

00:17:47.120 --> 00:17:52.390
 eventually snowballs and leads us to a great disappointment

00:17:52.390 --> 00:17:53.600
 when we can't get what we want.

00:17:53.600 --> 00:17:59.840
 When we're unable to compartmentalize reality any further.

00:17:59.840 --> 00:18:11.280
 So I think this is perhaps one way of

00:18:11.280 --> 00:18:20.720
 seeing the benefits of meditation practice or one good use

00:18:20.720 --> 00:18:21.680
 that comes, that is,

00:18:23.760 --> 00:18:28.710
 that the meditation practice has in our lives is in looking

00:18:28.710 --> 00:18:31.840
 at our desires for things when we want

00:18:31.840 --> 00:18:34.630
 something, when we need something, looking at our

00:18:34.630 --> 00:18:36.480
 possessions and coming to see that

00:18:36.480 --> 00:18:40.200
 the important truth that these are just conceptual, that we

00:18:40.200 --> 00:18:45.600
 say I own this and I own that and to

00:18:45.600 --> 00:18:51.790
 realize that this entire sentence is, every word of it is

00:18:51.790 --> 00:18:55.200
 false. The I, the owned, this,

00:18:55.200 --> 00:19:05.440
 I is not true. It's not mine. Even our own self is just a

00:19:05.440 --> 00:19:07.040
 conglomeration of

00:19:07.040 --> 00:19:12.930
 mind and body and states arising and ceasing, most of which

00:19:12.930 --> 00:19:14.640
 are very far out of our control.

00:19:16.240 --> 00:19:19.000
 Our states of emotion, our wants and our needs, we find

00:19:19.000 --> 00:19:22.080
 that we really can't control them as we

00:19:22.080 --> 00:19:30.520
 think we do. The own is not true. We can't for it. We can't

00:19:30.520 --> 00:19:34.080
 control things and say,

00:19:34.080 --> 00:19:38.010
 let it always be like this. We can't even control the

00:19:38.010 --> 00:19:41.120
 happiness that comes from the object because

00:19:41.120 --> 00:19:44.100
 after a while we get bored of our possessions and we need

00:19:44.100 --> 00:19:46.400
 more. We need the next thing, the newest

00:19:46.400 --> 00:19:52.940
 thing. And the this or the word this, the identification of

00:19:52.940 --> 00:19:55.280
 the thing as an object

00:19:55.280 --> 00:20:05.700
 is also not a part of reality because the object itself is

00:20:05.700 --> 00:20:06.480
 either seeing,

00:20:06.480 --> 00:20:16.160
 hearing, smelling, tasting, feeling or thinking.

00:20:16.160 --> 00:20:24.800
 When we practice meditation, we're able to

00:20:24.800 --> 00:20:34.130
 to address this issue of materialism, of being attached to

00:20:34.130 --> 00:20:35.520
 things and needing more and not being

00:20:35.520 --> 00:20:40.480
 content. The Buddha said it's a mark of a noble person. It

00:20:40.480 --> 00:20:45.920
's the lineage, which the word wangsa

00:20:45.920 --> 00:20:55.810
 I believe it means. It's the tradition, I guess, of the

00:20:55.810 --> 00:20:59.760
 noble ones to be content with whatever robes

00:20:59.760 --> 00:21:03.420
 or whatever clothes, whatever food, whatever shelter,

00:21:03.420 --> 00:21:06.560
 whatever medicines they get. To be

00:21:06.560 --> 00:21:10.110
 content with these things is the way of the noble ones. It

00:21:10.110 --> 00:21:13.280
's a mark of nobility or noble in the

00:21:13.280 --> 00:21:16.680
 Buddhist sense in terms of being enlightened. So this is

00:21:16.680 --> 00:21:20.480
 something that's very important,

00:21:20.480 --> 00:21:23.660
 something we should always be looking at in our lives. It's

00:21:23.660 --> 00:21:24.960
 easy to think we're practicing

00:21:24.960 --> 00:21:28.100
 meditation and suddenly we're accumulating and accumulating

00:21:28.100 --> 00:21:29.680
 and always getting more and more and

00:21:29.680 --> 00:21:34.680
 new and so on. It's very difficult to control. It's very

00:21:34.680 --> 00:21:37.120
 easy to rationalize why we need more

00:21:37.120 --> 00:21:39.760
 and more in this and that. It's very difficult to be

00:21:39.760 --> 00:21:44.880
 without. But the most important form of

00:21:44.880 --> 00:21:47.290
 contentment is not actually called contentment in the

00:21:47.290 --> 00:21:49.040
 Buddha's teaching as far as I've found.

00:21:49.040 --> 00:21:54.060
 But to me it's the most important type of contentment and

00:21:54.060 --> 00:21:55.280
 it's what is really being

00:21:55.280 --> 00:22:01.030
 talked about when the Buddha talks about contentment with

00:22:01.030 --> 00:22:02.720
 robes and food and so on.

00:22:02.720 --> 00:22:06.660
 He's really talking about contentment with reality as it is

00:22:06.660 --> 00:22:06.960
.

00:22:06.960 --> 00:22:16.240
 Contentment with experience as it is.

00:22:23.760 --> 00:22:27.470
 Being totally in tune with reality to the point that

00:22:27.470 --> 00:22:28.800
 nothing faces you.

00:22:28.800 --> 00:22:33.930
 And it's not called contentment I think because the word

00:22:33.930 --> 00:22:35.600
 contentment can be quite misleading in

00:22:35.600 --> 00:22:42.280
 this sense. It can mean it can be used to it can slip into

00:22:42.280 --> 00:22:45.840
 enjoyment. So we say you know I'm

00:22:45.840 --> 00:22:49.520
 enjoying smelling the flowers, stopping and smelling the

00:22:49.520 --> 00:22:51.440
 flowers and just enjoying life,

00:22:52.000 --> 00:22:56.430
 being content with my life. This isn't what is meant here.

00:22:56.430 --> 00:22:58.160
 What is meant here is contentment with

00:22:58.160 --> 00:23:06.540
 every experience in terms of not being discontent, not

00:23:06.540 --> 00:23:09.760
 being drawn into a judgment about reality,

00:23:09.760 --> 00:23:13.460
 not being drawn into a need. And in this sense there are

00:23:13.460 --> 00:23:15.520
 two kinds of discontent.

00:23:15.520 --> 00:23:19.910
 Discontent based on anger and discontent based on greed.

00:23:19.910 --> 00:23:21.760
 You could even say third,

00:23:21.760 --> 00:23:25.600
 discontent based on delusion. We want to go there as well.

00:23:25.600 --> 00:23:32.480
 But what we what we notice most in our lives is these two,

00:23:32.480 --> 00:23:34.320
 discontentment based on anger and

00:23:34.320 --> 00:23:38.250
 discontentment based on greed. That we'll be sitting in

00:23:38.250 --> 00:23:40.080
 meditation, even just sitting here

00:23:40.080 --> 00:23:43.970
 listening to a talk. And it's very difficult to keep the

00:23:43.970 --> 00:23:46.080
 mind content simply to listen,

00:23:46.080 --> 00:23:47.360
 simply to be here and now.

00:23:47.360 --> 00:23:52.790
 Content with what I'm saying, content with the feelings

00:23:52.790 --> 00:23:54.800
 going on in your body,

00:23:54.800 --> 00:23:57.810
 content with the things around you, content with the

00:23:57.810 --> 00:23:59.280
 thoughts in your mind.

00:23:59.280 --> 00:24:03.440
 There's so many things that make us angry and upset,

00:24:03.440 --> 00:24:07.120
 worried, afraid, bored.

00:24:07.600 --> 00:24:16.370
 And our ordinary way of looking at this is to immediately

00:24:16.370 --> 00:24:19.200
 need to change,

00:24:19.200 --> 00:24:24.640
 need to alter our reality to suit our defilement, to suit

00:24:24.640 --> 00:24:27.360
 our defiled state of mind,

00:24:27.360 --> 00:24:30.130
 to suit our anger, our dislike. When we don't like

00:24:30.130 --> 00:24:31.840
 something we should get rid of it,

00:24:31.840 --> 00:24:35.690
 we should remove it. This is the way we look at things, not

00:24:35.690 --> 00:24:38.480
 just in terms of our innate sense,

00:24:38.480 --> 00:24:41.860
 but also in terms of our views, our idea of what is right.

00:24:41.860 --> 00:24:42.800
 We think that it's right that

00:24:42.800 --> 00:24:45.830
 when you feel pain, you should adjust, you should move, you

00:24:45.830 --> 00:24:47.440
 should do whatever necessary

00:24:47.440 --> 00:24:52.560
 to get rid of the pain. This is one kind of discontent that

00:24:52.560 --> 00:24:54.400
 is antithetical to what is

00:24:54.400 --> 00:24:57.980
 teaching. And it's the purpose of the meditation practice,

00:24:57.980 --> 00:25:00.640
 to remove this sort of discontent.

00:25:02.480 --> 00:25:04.490
 Rather than trying to change the pain, to change the

00:25:04.490 --> 00:25:05.600
 experience,

00:25:05.600 --> 00:25:09.600
 we change the way we look at the experience.

00:25:09.600 --> 00:25:17.160
 So we simply see it for what it is and when we're in pain

00:25:17.160 --> 00:25:21.440
 or maybe too hot, too cold,

00:25:21.440 --> 00:25:26.400
 when we're hungry, when there's loud noises or whatever,

00:25:26.400 --> 00:25:29.440
 when we're thinking about bad things,

00:25:30.800 --> 00:25:32.080
 that we simply see it for what it is and the word bad

00:25:32.080 --> 00:25:34.880
 disappears. So we're no longer thinking about

00:25:34.880 --> 00:25:39.250
 bad things or feeling bad feelings. We're just thinking or

00:25:39.250 --> 00:25:41.440
 feeling or seeing or hearing or

00:25:41.440 --> 00:25:46.820
 smelling and tasting. And all of our experience of reality

00:25:46.820 --> 00:25:50.000
 is one of contentment in the sense of

00:25:50.000 --> 00:25:54.420
 simply seeing it for what it is, not discontent in terms of

00:25:54.420 --> 00:25:56.640
 needing it to be different.

00:25:58.160 --> 00:26:03.100
 The other form of discontent is based on desire. It's not

00:26:03.100 --> 00:26:05.600
 that there's anything wrong with our

00:26:05.600 --> 00:26:09.800
 present state of being here. It's that we want something

00:26:09.800 --> 00:26:12.000
 more or we begin thinking about

00:26:12.000 --> 00:26:16.220
 something and it makes us want it. It makes us desire to do

00:26:16.220 --> 00:26:18.640
 this or do that, to obtain this or

00:26:18.640 --> 00:26:25.370
 obtain that experience. And I would say from the practice,

00:26:25.370 --> 00:26:32.000
 looking at these two, really the only

00:26:32.000 --> 00:26:35.900
 way to deal with them adequately is through meditation, is

00:26:35.900 --> 00:26:37.520
 through breaking them up into

00:26:37.520 --> 00:26:43.350
 their building blocks, the pieces, and seeing that actually

00:26:43.350 --> 00:26:48.000
 we're not talking about an entity

00:26:48.000 --> 00:26:50.360
 or a single experience. We're talking about several

00:26:50.360 --> 00:26:52.720
 different experiences all jumbled up into one.

00:26:52.720 --> 00:26:57.030
 First there's the experience of the object, then there's

00:26:57.030 --> 00:27:00.400
 the feeling, the recognition of it

00:27:00.400 --> 00:27:04.000
 as something good or something bad, then there's the

00:27:04.000 --> 00:27:06.720
 feeling that comes of happiness or pain,

00:27:06.720 --> 00:27:12.000
 then there's the liking it or the disliking it. Once there

00:27:12.000 --> 00:27:14.400
's the liking and the disliking it,

00:27:14.400 --> 00:27:17.760
 there's the intention to do this or to do that, to change

00:27:17.760 --> 00:27:20.400
 the experience, to obtain

00:27:20.400 --> 00:27:23.140
 something different or to remove something from our

00:27:23.140 --> 00:27:28.960
 experience. And only then is there the acting

00:27:28.960 --> 00:27:33.320
 out on it. And all of these can be broken up and separated

00:27:33.320 --> 00:27:35.680
 from each other at any one time.

00:27:35.680 --> 00:27:39.320
 When you see something or let's take an easy one, when you

00:27:39.320 --> 00:27:41.200
 hear something, suppose you're listening

00:27:41.200 --> 00:27:43.770
 to what I'm saying and you really don't like it. Maybe I'm

00:27:43.770 --> 00:27:47.040
 saying nasty things about, maybe what

00:27:47.040 --> 00:27:53.520
 I'm saying just seems totally diametrically opposed to what

00:27:53.520 --> 00:27:58.880
 you believe. Or suppose I start

00:27:58.880 --> 00:28:01.670
 yelling at you and saying nasty things to you or so on.

00:28:01.670 --> 00:28:03.840
 Suppose someone comes into your room and

00:28:03.840 --> 00:28:11.610
 starts complaining and so on. Right away you can say to

00:28:11.610 --> 00:28:14.560
 yourself, hearing, hearing, remind yourself

00:28:14.560 --> 00:28:17.140
 that it's just a sound. You can try that right now when I'm

00:28:17.140 --> 00:28:18.880
 talking. You like it, you don't like it,

00:28:18.880 --> 00:28:22.030
 it's not important. As a meditation exercise, simply when

00:28:22.030 --> 00:28:24.320
 you hear my voice saying to yourself,

00:28:24.320 --> 00:28:27.430
 hearing, hearing, hearing, it's a great way to listen to D

00:28:27.430 --> 00:28:28.320
hamma talks.

00:28:31.120 --> 00:28:33.580
 You can even become enlightened this way, listening to the

00:28:33.580 --> 00:28:33.920
 Dhamma,

00:28:33.920 --> 00:28:38.110
 realizing the truth at the same time. One teacher in

00:28:38.110 --> 00:28:40.560
 Thailand here would always say

00:28:40.560 --> 00:28:42.850
 best way to listen to a Dhamma talk, just say hearing,

00:28:42.850 --> 00:28:44.320
 hearing, hearing the whole time.

00:28:44.320 --> 00:28:47.580
 Because that's really why we're teaching, is to encourage

00:28:47.580 --> 00:28:48.640
 people to meditate.

00:28:48.640 --> 00:28:57.190
 And if that doesn't work, if you're not quick enough with

00:28:57.190 --> 00:29:00.320
 that, then when the feeling arises,

00:29:01.280 --> 00:29:05.080
 when you feel happiness or pain, you just focus on the

00:29:05.080 --> 00:29:09.120
 happy, happy, happy, or pain, pain, or

00:29:09.120 --> 00:29:14.830
 sad, sad, or whatever. And again, you don't let it build

00:29:14.830 --> 00:29:15.360
 into

00:29:15.360 --> 00:29:20.200
 real liking or disliking and the intention to change things

00:29:20.200 --> 00:29:22.720
. You simply, when you have

00:29:22.720 --> 00:29:25.560
 pain in the body, for instance, knowing that it's pain and

00:29:25.560 --> 00:29:27.120
 just seeing it for what it is.

00:29:29.200 --> 00:29:31.620
 And if you can't catch it there, then you can catch it at

00:29:31.620 --> 00:29:32.960
 the liking or the disliking,

00:29:32.960 --> 00:29:37.360
 saying to yourself, liking, liking, or wanting, wanting,

00:29:37.360 --> 00:29:42.000
 disliking, angry, upset, bored, scared, sad.

00:29:42.000 --> 00:29:47.830
 If you still can't catch it there, then you can catch the

00:29:47.830 --> 00:29:49.680
 intention. You want to do this. You

00:29:49.680 --> 00:29:54.240
 want to obtain something. You want to say something. You

00:29:54.240 --> 00:29:57.440
 want to chase someone away or

00:29:58.960 --> 00:30:02.490
 run away or so on. And you can focus on that intention,

00:30:02.490 --> 00:30:04.240
 wanting, wanting, or intending,

00:30:04.240 --> 00:30:07.670
 intending. In this sense, not wanting as greed or as desire

00:30:07.670 --> 00:30:10.000
, but as an intention to do something.

00:30:10.000 --> 00:30:16.640
 If you still can't catch it there, you're in trouble

00:30:16.640 --> 00:30:19.360
 because at that point you're going to

00:30:19.360 --> 00:30:21.840
 go out and do something about it. But the amazing thing is,

00:30:21.840 --> 00:30:23.840
 is when you focus on all of these things,

00:30:23.840 --> 00:30:32.040
 you can see the problem with this line of reaction. You see

00:30:32.040 --> 00:30:34.880
 how when you want things to

00:30:34.880 --> 00:30:38.380
 be different from what they are, your whole body is tense

00:30:38.380 --> 00:30:43.040
 and your mind is in a state of upset,

00:30:43.040 --> 00:30:46.140
 of turmoil. Even if it's a desire and you feel happy and

00:30:46.140 --> 00:30:48.000
 you really want something, you think,

00:30:48.000 --> 00:30:50.630
 "Wow, this is going to make me happy." When you look at it,

00:30:50.630 --> 00:30:52.320
 when you analyze it, you see that it's

00:30:52.320 --> 00:30:56.490
 not so pleasurable at all, really. Your mind is in a state

00:30:56.490 --> 00:30:59.840
 of clinging. You can really feel like

00:30:59.840 --> 00:31:02.320
 you're clinging. It's as though you're grabbing something

00:31:02.320 --> 00:31:03.920
 in your fist and holding tightly.

00:31:03.920 --> 00:31:06.370
 That's how your mind feels at the moment when you want

00:31:06.370 --> 00:31:09.200
 something. When you think you have to do

00:31:09.200 --> 00:31:12.720
 something to obtain your pleasure, the object of your

00:31:12.720 --> 00:31:16.560
 desires, even then you can see that it's not

00:31:16.560 --> 00:31:20.420
 really pleasurable, not to speak of anger when you want to

00:31:20.420 --> 00:31:22.640
 hurt someone. When you look at it,

00:31:22.640 --> 00:31:26.680
 you can see that this is not a wholesome state of mind. It

00:31:26.680 --> 00:31:28.960
's not a proper state of mind.

00:31:42.560 --> 00:31:46.320
 And so this is how we really develop true contentment in

00:31:46.320 --> 00:31:48.960
 Buddhism, coming to separate

00:31:48.960 --> 00:31:52.600
 things into their reality. When you see something instead

00:31:52.600 --> 00:31:54.640
 of being attached to it, wanting it,

00:31:54.640 --> 00:31:57.720
 needing it, clinging to it, simply seeing it for what it is

00:31:57.720 --> 00:31:59.200
, if you're happy or happy,

00:31:59.200 --> 00:32:04.830
 if you're unhappy or unhappy. Contentment in the sense of

00:32:04.830 --> 00:32:07.280
 doing away with any need for things to

00:32:07.280 --> 00:32:12.520
 be different, any need for reality to be other than what it

00:32:12.520 --> 00:32:16.400
 is. This is really the ultimate state.

00:32:16.400 --> 00:32:23.290
 It's difficult. It's dangerous to say this in a sense, just

00:32:23.290 --> 00:32:24.320
 to leave it at that because

00:32:24.320 --> 00:32:28.780
 it sounds as though you don't do anything then. Being

00:32:28.780 --> 00:32:30.080
 content with things as they are,

00:32:30.080 --> 00:32:33.120
 as I said, Buddha taught us to be discontent.

00:32:33.120 --> 00:32:44.490
 But the key is it's not easy. This is not the ordinary

00:32:44.490 --> 00:32:47.280
 nature of our minds. We're working hard

00:32:47.280 --> 00:32:53.170
 striving to stop striving. We have to work in order that we

00:32:53.170 --> 00:32:55.120
 don't have to do anything,

00:32:55.120 --> 00:32:58.890
 in order that we no longer need to attain anything, to do

00:32:58.890 --> 00:33:01.040
 away with our need to attain things.

00:33:02.240 --> 00:33:06.770
 We're working hard to stop our minds from working so hard,

00:33:06.770 --> 00:33:08.320
 in a sense.

00:33:08.320 --> 00:33:17.610
 As we practice, as we develop the meditation practice, we

00:33:17.610 --> 00:33:19.440
 find ourselves more and more content.

00:33:19.440 --> 00:33:27.030
 So through working in this way, through developing

00:33:27.030 --> 00:33:28.640
 ourselves,

00:33:29.840 --> 00:33:32.180
 we're able to experience all things and we no longer

00:33:32.180 --> 00:33:34.400
 compartmentalize reality. We no longer

00:33:34.400 --> 00:33:38.320
 separate things into the bearable and the unbearable, the

00:33:38.320 --> 00:33:41.600
 acceptable and the unacceptable.

00:33:41.600 --> 00:33:47.810
 We're able to accept and react rationally and honestly and

00:33:47.810 --> 00:33:50.960
 with wisdom to everything that arises.

00:33:52.960 --> 00:33:58.440
 This is the the Buddha's teaching on contentment. So that

00:33:58.440 --> 00:34:00.800
 was the Dhamma I would like to give today.

00:34:00.800 --> 00:34:03.840
 And if there are any questions, I'm happy to take them.

00:34:03.840 --> 00:34:08.480
 Otherwise, thanks everyone for coming.

