1
00:00:00,000 --> 00:00:05,080
 When we talk about Nibbana and no rebirths, what are we

2
00:00:05,080 --> 00:00:06,200
 saying that happens?

3
00:00:06,200 --> 00:00:10,080
 Non-existence? End of life?

4
00:00:10,080 --> 00:00:18,150
 Well, it's a pretty simple answer. I mean, existence is

5
00:00:18,150 --> 00:00:20,320
 momentary.

6
00:00:20,320 --> 00:00:26,800
 One moment is one existence and it arises and it ceases.

7
00:00:28,000 --> 00:00:33,480
 That doesn't happen in Nibbana. That's really the easiest

8
00:00:33,480 --> 00:00:36,800
 way to understand it.

9
00:00:36,800 --> 00:00:44,850
 And since life itself is only composed totally of those

10
00:00:44,850 --> 00:00:46,800
 momentary experiences,

11
00:00:46,800 --> 00:00:54,230
 then there really is no such thing as a life that could end

12
00:00:54,230 --> 00:00:54,800
.

13
00:00:54,800 --> 00:01:01,540
 There's only experiences which end every moment and that

14
00:01:01,540 --> 00:01:03,600
 doesn't occur.

15
00:01:03,600 --> 00:01:09,600
 There is no more arising of those momentary experiences,

16
00:01:09,600 --> 00:01:14,910
 of seeing, hearing, smelling, tasting, feeling and thinking

17
00:01:14,910 --> 00:01:17,600
 of any momentary experience.

18
00:01:17,600 --> 00:01:22,040
 Because Nibbana is eternal, that's what makes it eternal or

19
00:01:22,040 --> 00:01:23,600
 undying.

20
00:01:23,600 --> 00:01:30,080
 It's not the end of life, it's eternal life. It's called Am

21
00:01:30,080 --> 00:01:32,400
ata, it means no dying.

22
00:01:32,400 --> 00:01:35,890
 But the reason there's no dying is because there's no being

23
00:01:35,890 --> 00:01:38,400
 born, there's no arising.

24
00:01:38,400 --> 00:01:40,930
 Since there's no arising, there's therefore no ceasing. It

25
00:01:40,930 --> 00:01:44,400
's quite simple.

26
00:01:44,400 --> 00:01:51,620
 The Buddha refused to answer questions of that kind, didn't

27
00:01:51,620 --> 00:01:52,400
 he?

28
00:01:53,200 --> 00:01:55,200
 No.

29
00:01:55,200 --> 00:01:59,970
 I've heard people say that but no, about Nibbana it's quite

30
00:01:59,970 --> 00:02:01,200
 clear this.

31
00:02:01,200 --> 00:02:07,270
 There's questions like, does the Arahant exist after death

32
00:02:07,270 --> 00:02:10,800
 or does he not exist after death?

33
00:02:10,800 --> 00:02:14,000
 Yeah, that's what I was mentioning.

34
00:02:14,000 --> 00:02:18,180
 The point being that there is no Arahant, right? It's a

35
00:02:18,180 --> 00:02:19,200
 concept.

36
00:02:19,200 --> 00:02:23,140
 The reason why the Buddha might not answer, in certain

37
00:02:23,140 --> 00:02:24,800
 occasions didn't answer,

38
00:02:24,800 --> 00:02:27,350
 is because he knew that they listened or wouldn't be able

39
00:02:27,350 --> 00:02:30,000
 to appreciate and wouldn't like that answer.

40
00:02:30,000 --> 00:02:33,600
 Not necessarily that it's unanswerable.

41
00:02:33,600 --> 00:02:35,970
 The answer is that the Arahant never existed in the first

42
00:02:35,970 --> 00:02:38,000
 place, there's only momentary experience.

43
00:02:38,000 --> 00:02:40,640
 In fact I was reading last night in the Majimani Ka'a

44
00:02:40,640 --> 00:02:44,000
 something very interesting how the Buddha didn't answer.

45
00:02:44,000 --> 00:02:47,600
 He didn't want to answer. It was totally unrelated.

46
00:02:47,600 --> 00:02:52,800
 It was the question about whether Brahmins are superior to

47
00:02:52,800 --> 00:02:56,110
 Kathyas, the warrior, the nobles or the Brahmins, which one

48
00:02:56,110 --> 00:02:56,800
 is superior?

49
00:02:56,800 --> 00:03:01,190
 And he just kept not answering because he knew that if he

50
00:03:01,190 --> 00:03:02,800
 said, no they're not superior,

51
00:03:02,800 --> 00:03:05,800
 then the person would be 'but, but, but' and have arguments

52
00:03:05,800 --> 00:03:06,000
.

53
00:03:06,000 --> 00:03:12,250
 So first he let them on this long explanation about how one

54
00:03:12,250 --> 00:03:16,800
 could think that they are unequal and so on.

55
00:03:16,800 --> 00:03:20,000
 Until finally he cornered the guy and he made the guy ask,

56
00:03:20,000 --> 00:03:25,200
 well in this regard are they different?

57
00:03:25,200 --> 00:03:27,930
 He said no, then the Buddha was able to say without

58
00:03:27,930 --> 00:03:31,410
 worrying about, without concern about having this guy argue

59
00:03:31,410 --> 00:03:32,000
 with him.

60
00:03:32,000 --> 00:03:36,000
 No, then, well in that case, in that way they are not.

61
00:03:36,000 --> 00:03:39,240
 In regards to meditation it was something like in regards

62
00:03:39,240 --> 00:03:40,800
 to their purity of mind.

63
00:03:40,800 --> 00:03:44,850
 Because just if any of the, if a kasat, of a kathya or a

64
00:03:44,850 --> 00:03:48,780
 brahmin is to take sticks and rub them together and make

65
00:03:48,780 --> 00:03:48,800
 fire,

66
00:03:48,800 --> 00:03:51,600
 the fire that comes is the same.

67
00:03:51,600 --> 00:03:55,740
 So in the same way if anyone is to practice meditation it

68
00:03:55,740 --> 00:03:58,800
 doesn't matter what caste they are in.

69
00:03:58,800 --> 00:04:03,200
 But in other cases the Buddha did say quite categorically

70
00:04:03,200 --> 00:04:07,110
 that the different types of people are, there is no

71
00:04:07,110 --> 00:04:08,800
 difference among them.

72
00:04:08,800 --> 00:04:11,770
 But I think very much based on the audience there were

73
00:04:11,770 --> 00:04:14,400
 times where he, and then he said to Ananda afterwards,

74
00:04:14,400 --> 00:04:17,720
 you know, if I had told him one or the other he would have

75
00:04:17,720 --> 00:04:19,200
 been more confused.

76
00:04:19,200 --> 00:04:22,700
 So that's why I didn't tell him, but it's not that there is

77
00:04:22,700 --> 00:04:23,600
 no answer.

78
00:04:23,600 --> 00:04:25,590
 And there are certainly, there is the case, I can't

79
00:04:25,590 --> 00:04:28,400
 remember if it was the Buddha or if it was Sariputta,

80
00:04:28,400 --> 00:04:33,200
 who said that there must have been the Buddha as well.

81
00:04:33,200 --> 00:04:40,000
 What is it that happens at Bhairnibhana?

82
00:04:40,000 --> 00:04:46,470
 And I think this monk had a pernicious view that the self

83
00:04:46,470 --> 00:04:50,800
 is destroyed at Bhairnibhana,

84
00:04:50,800 --> 00:04:53,200
 or he had some pernicious view like that.

85
00:04:53,200 --> 00:04:59,930
 And the Buddha reprimands him, or Sariputta, or I can't

86
00:04:59,930 --> 00:05:01,200
 remember, it must have been the Buddha.

87
00:05:01,200 --> 00:05:10,050
 And then the answer to the question what happens at Bhairn

88
00:05:10,050 --> 00:05:12,400
ibhana is that the five aggregates,

89
00:05:12,400 --> 00:05:19,230
 and this is the orthodox answer, is that the five aggreg

90
00:05:19,230 --> 00:05:25,070
ates of clinging which are impermanent, unsatisfying, and

91
00:05:25,070 --> 00:05:26,400
 non-self

92
00:05:26,400 --> 00:05:31,600
 cease without arising again, cease without remainder.

93
00:05:31,600 --> 00:05:37,490
 So nothing ceases that wouldn't cease anyway, but they

94
00:05:37,490 --> 00:05:38,400
 cease without remainder,

95
00:05:38,400 --> 00:05:44,140
 meaning there is no more of these impermanent suffering and

96
00:05:44,140 --> 00:05:46,800
 non-self states arising.

97
00:05:46,800 --> 00:05:49,200
 That's the orthodox answer, and this is in the tibhittaka.

98
00:05:49,200 --> 00:05:55,600
 That's what happens at Bhairnibhana.

