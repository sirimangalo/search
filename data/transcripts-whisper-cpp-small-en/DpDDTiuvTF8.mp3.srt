1
00:00:00,000 --> 00:00:10,000
 [BLANK_AUDIO]

2
00:00:10,000 --> 00:00:20,000
 [BLANK_AUDIO]

3
00:00:20,000 --> 00:00:30,000
 [BLANK_AUDIO]

4
00:00:30,000 --> 00:00:40,000
 [BLANK_AUDIO]

5
00:00:40,000 --> 00:00:50,000
 [BLANK_AUDIO]

6
00:00:50,000 --> 00:01:00,000
 [BLANK_AUDIO]

7
00:01:00,000 --> 00:01:10,000
 [BLANK_AUDIO]

8
00:01:23,000 --> 00:01:29,000
 Good evening everyone. Welcome to our live broadcast.

9
00:01:29,000 --> 00:01:34,000
 [BLANK_AUDIO]

10
00:01:34,000 --> 00:01:43,000
 Today we're looking at Gutturani Kaia Book of Fives,

11
00:01:43,000 --> 00:01:55,000
 Su to 48, Alabaniya Tanasutta, Alabaniya Tanas.

12
00:01:55,000 --> 00:02:07,000
 Tanas is a, Tanas means a place, but here it means a state

13
00:02:07,000 --> 00:02:09,000
 or a case.

14
00:02:09,000 --> 00:02:21,000
 Alabaniya means unattainable, and, Unobtainable.

15
00:02:21,000 --> 00:02:28,190
 So there are five of the Buddhists, Panchimani, Alabaniya,

16
00:02:28,190 --> 00:02:30,000
 and Itana.

17
00:02:30,000 --> 00:02:36,700
 There are these five unattainable states and things that

18
00:02:36,700 --> 00:02:38,000
 are unattainable.

19
00:02:38,000 --> 00:02:43,390
 Samrani, Navab, Brahmani, Navab, Devi, Navamari, Navab,

20
00:02:43,390 --> 00:02:44,000
 Brahmanava,

21
00:02:44,000 --> 00:02:51,000
 Kainachiva, Lokasmi, by anyone in this world, by a, by a

22
00:02:51,000 --> 00:02:54,000
 recluse or a brahmin,

23
00:02:54,000 --> 00:02:58,000
 no matter how religious or spiritually powerful they may be

24
00:02:58,000 --> 00:02:58,000
,

25
00:02:58,000 --> 00:03:06,000
 or even if they're an angel or a demon or a god,

26
00:03:06,000 --> 00:03:10,000
 can't obtain.

27
00:03:10,000 --> 00:03:14,000
 No matter how powerful you are, no matter who you are,

28
00:03:14,000 --> 00:03:29,000
 certain things that we can't get.

29
00:03:29,000 --> 00:03:34,850
 And these four are, five are, in regards to things that are

30
00:03:34,850 --> 00:03:38,000
 of a nature to get old,

31
00:03:38,000 --> 00:03:45,000
 Jaradamang, Madhiri, may they not get old.

32
00:03:45,000 --> 00:03:50,000
 You can't have that. You can't have that.

33
00:03:50,000 --> 00:03:58,000
 It won't come true, no matter what you do.

34
00:03:58,000 --> 00:04:08,470
 Jaradidamang, Madhiri, Pyaadi, what is of a nature to get

35
00:04:08,470 --> 00:04:20,000
 sick, may not get sick.

36
00:04:20,000 --> 00:04:27,490
 Number three, Maranadamang, Maami, that which is of a

37
00:04:27,490 --> 00:04:30,000
 nature to die, may it not die.

38
00:04:30,000 --> 00:04:37,300
 Kaya dhammang, Maakhi, that which is of a nature to be

39
00:04:37,300 --> 00:04:45,000
 destroyed, may it not be destroyed.

40
00:04:45,000 --> 00:04:50,000
 Decay, that which is subject to decay, may it not decay.

41
00:04:50,000 --> 00:04:57,050
 Nastana dhammang, Maanasi, number five, that which is

42
00:04:57,050 --> 00:04:59,000
 subject to destruction.

43
00:04:59,000 --> 00:05:04,000
 So Kaya is more like decay or wasting away.

44
00:05:04,000 --> 00:05:22,000
 Nastana means destruction or nullification.

45
00:05:22,000 --> 00:05:25,240
 The point being here, the interesting thing about this and

46
00:05:25,240 --> 00:05:28,000
 why this is actually worth it,

47
00:05:28,000 --> 00:05:42,000
 and this is very much worth stopping at and going over, is

48
00:05:42,000 --> 00:05:44,000
 that no matter how hard we try,

49
00:05:44,000 --> 00:05:51,000
 we can't fix and we can't perfect life.

50
00:05:51,000 --> 00:05:56,630
 There are certain realities that we have to face that will

51
00:05:56,630 --> 00:06:02,000
 never be able to obtain a state of being

52
00:06:02,000 --> 00:06:06,560
 whereby we always get what we want, we always have what we

53
00:06:06,560 --> 00:06:07,000
 want.

54
00:06:07,000 --> 00:06:11,420
 We can keep what we want and only what we want, and where

55
00:06:11,420 --> 00:06:20,000
 we aren't faced with realities that are unpleasant to us.

56
00:06:20,000 --> 00:06:31,000
 And reality is not based on our desires.

57
00:06:31,000 --> 00:06:34,510
 So he says an ordinary person, someone who doesn't realize

58
00:06:34,510 --> 00:06:41,000
 this, people living in the world,

59
00:06:41,000 --> 00:06:46,280
 for them what is subject to old age grows old, their body

60
00:06:46,280 --> 00:06:50,000
 gets old, their loved ones grow old,

61
00:06:50,000 --> 00:07:02,000
 their possessions age.

62
00:07:02,000 --> 00:07:05,130
 Because they don't see that this isn't part of nature, as

63
00:07:05,130 --> 00:07:09,000
 we were talking about last night about funerals,

64
00:07:09,000 --> 00:07:13,230
 because we're not clearly aware of this as being a part of

65
00:07:13,230 --> 00:07:17,000
 nature, we suffer, suffer because of old age,

66
00:07:17,000 --> 00:07:21,000
 we suffer because of sickness, we suffer because of death,

67
00:07:21,000 --> 00:07:24,000
 we suffer unnecessarily,

68
00:07:24,000 --> 00:07:29,220
 we suffer based not simply on the nature of the experience,

69
00:07:29,220 --> 00:07:32,000
 but based on our expectations

70
00:07:32,000 --> 00:07:40,060
 and our delusions that it could somehow be otherwise. We

71
00:07:40,060 --> 00:07:41,630
 work very hard to avoid death, to avoid old age, to avoid

72
00:07:41,630 --> 00:07:43,000
 sickness, right?

73
00:07:43,000 --> 00:07:48,080
 We work very hard to escape its grasp. People who are

74
00:07:48,080 --> 00:07:52,000
 obsessed with health, who are obsessed with youth,

75
00:07:52,000 --> 00:07:59,270
 we work very hard, people who are obsessed with life, we

76
00:07:59,270 --> 00:08:03,000
 fear death, old age sickness and death,

77
00:08:03,000 --> 00:08:06,010
 it's very much a part of who we are, but also with

78
00:08:06,010 --> 00:08:11,450
 everything, whether even those things that don't actually

79
00:08:11,450 --> 00:08:12,000
 die,

80
00:08:12,000 --> 00:08:21,000
 they're still subject to dissolution, to kaya and nasana.

81
00:08:21,000 --> 00:08:24,740
 We have old age sickness and death, but also simple

82
00:08:24,740 --> 00:08:27,000
 dissolution and destruction,

83
00:08:27,000 --> 00:08:37,000
 and everything we hold dear, even besides our own body,

84
00:08:37,000 --> 00:08:41,000
 there's nothing else that can be a refuge for us.

85
00:08:41,000 --> 00:08:50,590
 So this really, in an ultimate sense, especially for medit

86
00:08:50,590 --> 00:08:54,000
ators, it's working on two levels.

87
00:08:54,000 --> 00:08:59,110
 The idea, the concept of death, or the concept of losing

88
00:08:59,110 --> 00:09:01,000
 what you hold dear,

89
00:09:01,000 --> 00:09:05,210
 the conceptual level is clear, you can lose relatives,

90
00:09:05,210 --> 00:09:10,000
 friends, loved ones, possessions, even your own body.

91
00:09:10,000 --> 00:09:15,950
 But it's all conceptual, it doesn't actually describe what

92
00:09:15,950 --> 00:09:18,000
's really happening.

93
00:09:18,000 --> 00:09:21,210
 And because we get stuck on concepts, we have this idea

94
00:09:21,210 --> 00:09:27,990
 that somehow we can achieve a state whereby our concepts

95
00:09:27,990 --> 00:09:30,000
 are,

96
00:09:30,000 --> 00:09:35,340
 and by concepts I mean things, where the things we own can

97
00:09:35,340 --> 00:09:37,000
 last forever.

98
00:09:37,000 --> 00:09:41,840
 It seems theoretically possible, right, that you should

99
00:09:41,840 --> 00:09:46,000
 work really hard so that you get to keep everything.

100
00:09:46,000 --> 00:09:53,100
 Or you might even say, "Well, at least I can keep things

101
00:09:53,100 --> 00:09:55,000
 for years."

102
00:09:55,000 --> 00:09:59,380
 Or you'd say, "Well, at least I can keep things for months,

103
00:09:59,380 --> 00:10:02,000
 or days, or hours, or minutes.

104
00:10:02,000 --> 00:10:06,160
 At least I have some good experiences that last and are

105
00:10:06,160 --> 00:10:09,000
 dependable and are satisfying."

106
00:10:09,000 --> 00:10:14,230
 Unfortunately, the underlying reality even doesn't allow

107
00:10:14,230 --> 00:10:15,000
 for that,

108
00:10:15,000 --> 00:10:21,580
 because the second level in which this works is ultimate

109
00:10:21,580 --> 00:10:23,000
 reality.

110
00:10:23,000 --> 00:10:33,000
 That death is actually something that occurs every moment.

111
00:10:33,000 --> 00:10:34,000
 And our suffering isn't actually based on concepts,

112
00:10:34,000 --> 00:10:38,660
 it's based on ultimate reality and our inability to see how

113
00:10:38,660 --> 00:10:42,000
 reality is working behind the scenes.

114
00:10:42,000 --> 00:10:50,640
 So we're unable to see how our emotions, how the objects of

115
00:10:50,640 --> 00:10:55,840
 our emotions, the things we like and dislike, how they

116
00:10:55,840 --> 00:10:58,000
 change so quickly.

117
00:10:58,000 --> 00:11:02,790
 So we like something. What we really like is the experience

118
00:11:02,790 --> 00:11:05,000
, the feeling of pleasure,

119
00:11:05,000 --> 00:11:10,510
 or the thing, the experience that we recognize as bringing

120
00:11:10,510 --> 00:11:12,000
 us pleasure.

121
00:11:12,000 --> 00:11:16,000
 But then it changes immediately, it comes and it goes.

122
00:11:16,000 --> 00:11:22,170
 And when it changes, we're left with stress, because we no

123
00:11:22,170 --> 00:11:24,000
 longer have what we want,

124
00:11:24,000 --> 00:11:28,310
 or we don't get what we want, or we want something that we

125
00:11:28,310 --> 00:11:31,000
've gotten, we wanted more.

126
00:11:31,000 --> 00:11:42,000
 We become attached and addicted to these things.

127
00:11:42,000 --> 00:11:46,940
 But the real reason why we can't keep concepts of people

128
00:11:46,940 --> 00:11:49,000
 and places and things

129
00:11:49,000 --> 00:11:54,150
 is because the ultimate reality underneath it is changing

130
00:11:54,150 --> 00:11:56,000
 moment by moment.

131
00:11:56,000 --> 00:12:00,140
 This is what we really see in meditation, not really so

132
00:12:00,140 --> 00:12:03,000
 much about, "Oh dear, I will have to one day die."

133
00:12:03,000 --> 00:12:07,060
 It's much more visceral than that. It's that I'm dying

134
00:12:07,060 --> 00:12:11,000
 every moment, being born and dying.

135
00:12:11,000 --> 00:12:16,000
 Born, die, born, die every moment.

136
00:12:16,000 --> 00:12:20,000
 And that reality is not something that should be clung to,

137
00:12:20,000 --> 00:12:24,000
 that there's no part of reality

138
00:12:24,000 --> 00:12:28,280
 that gives us any reason to think that it could be a refuge

139
00:12:28,280 --> 00:12:30,000
 or satisfaction to us,

140
00:12:30,000 --> 00:12:35,000
 because it lasts by the moment.

141
00:12:35,000 --> 00:12:38,450
 So this whole idea of clinging and why clinging and craving

142
00:12:38,450 --> 00:12:39,000
 is wrong,

143
00:12:39,000 --> 00:12:42,550
 we sometimes doubt and wonder, "Well, why is it wrong to

144
00:12:42,550 --> 00:12:43,000
 like things?

145
00:12:43,000 --> 00:12:46,000
 What would it be like to not like things?"

146
00:12:46,000 --> 00:12:50,700
 It's really a complete misunderstanding of the very

147
00:12:50,700 --> 00:12:53,000
 framework of reality.

148
00:12:53,000 --> 00:12:57,180
 There's nothing to do really with stopping liking things or

149
00:12:57,180 --> 00:12:58,000
 people.

150
00:12:58,000 --> 00:13:02,000
 Those things and people don't exist.

151
00:13:02,000 --> 00:13:05,880
 It's about stopping to cling or concern ourself with what

152
00:13:05,880 --> 00:13:08,000
 really is born and dies,

153
00:13:08,000 --> 00:13:12,000
 our experiences.

154
00:13:12,000 --> 00:13:17,290
 But either way, on both levels, it clearly works on both

155
00:13:17,290 --> 00:13:18,000
 levels.

156
00:13:18,000 --> 00:13:28,000
 And it's something that gives us a real shock,

157
00:13:28,000 --> 00:13:32,950
 something that gives us some sense of alarm and should give

158
00:13:32,950 --> 00:13:36,000
 us some sense of concern.

159
00:13:36,000 --> 00:13:43,000
 The things that we depend upon are not dependable.

160
00:13:43,000 --> 00:13:48,000
 It's our own life, who we are, our identity,

161
00:13:48,000 --> 00:13:53,210
 our very soul itself is subject to change, subject to

162
00:13:53,210 --> 00:13:55,000
 dissolution,

163
00:13:55,000 --> 00:13:59,000
 our very being.

164
00:13:59,000 --> 00:14:04,750
 We are subject to death. All of our family is subject to

165
00:14:04,750 --> 00:14:06,000
 death.

166
00:14:06,000 --> 00:14:09,330
 So to run away from old age, to run away from sickness, to

167
00:14:09,330 --> 00:14:12,000
 run away from death,

168
00:14:12,000 --> 00:14:17,000
 not even the gods have the power to stop.

169
00:14:17,000 --> 00:14:21,000
 It's a part of how nature is made.

170
00:14:21,000 --> 00:14:26,000
 Reality is momentary experiences that constantly creating

171
00:14:26,000 --> 00:14:26,000
 the change

172
00:14:26,000 --> 00:14:31,000
 that we see on the conceptual level of people and places

173
00:14:31,000 --> 00:14:32,000
 and things.

174
00:14:32,000 --> 00:14:37,000
 So not to belabor the point, but worth remembering,

175
00:14:37,000 --> 00:14:42,000
 that's really a good way of understanding why.

176
00:14:42,000 --> 00:14:46,000
 Why am I working so hard to better myself?

177
00:14:46,000 --> 00:14:51,000
 Am I trying to cultivate spiritual realization?

178
00:14:51,000 --> 00:14:56,240
 To free ourselves from the unreasonable expectations of

179
00:14:56,240 --> 00:14:57,000
 youth,

180
00:14:57,000 --> 00:15:04,000
 health, life, stability, continuity, certainty, all this.

181
00:15:04,000 --> 00:15:08,000
 To be able to deal with the truth of change,

182
00:15:08,000 --> 00:15:12,250
 that really is the big thing that we're not able to deal

183
00:15:12,250 --> 00:15:13,000
 with.

184
00:15:13,000 --> 00:15:15,690
 It's really change if everything stayed the same all the

185
00:15:15,690 --> 00:15:16,000
 time,

186
00:15:16,000 --> 00:15:19,000
 when we get used to it.

187
00:15:19,000 --> 00:15:23,000
 But we're constantly disappointed,

188
00:15:23,000 --> 00:15:27,550
 and our expectations are not met because of change, because

189
00:15:27,550 --> 00:15:28,000
 of chaos,

190
00:15:28,000 --> 00:15:31,000
 but mostly because of our delusion, simple delusion.

191
00:15:31,000 --> 00:15:34,790
 It's kind of silly. Well, if you understood that everything

192
00:15:34,790 --> 00:15:35,000
 changes constantly,

193
00:15:35,000 --> 00:15:41,510
 you would never cultivate any expectation of stability or

194
00:15:41,510 --> 00:15:44,000
 partiality.

195
00:15:44,000 --> 00:15:47,000
 And that's really what we come to see.

196
00:15:47,000 --> 00:15:53,000
 We simply see that reality is chaotic.

197
00:15:53,000 --> 00:15:57,000
 And if that is a fact of life, which it turns out to be,

198
00:15:57,000 --> 00:16:03,000
 then we must necessarily give up expectations,

199
00:16:03,000 --> 00:16:16,000
 and our attempts to live long and be healthy and young,

200
00:16:16,000 --> 00:16:20,000
 and to keep the things that we like.

201
00:16:20,000 --> 00:16:24,210
 Eventually we see that even liking itself is part of the

202
00:16:24,210 --> 00:16:25,000
 problem,

203
00:16:25,000 --> 00:16:30,000
 really the source of our problems.

204
00:16:30,000 --> 00:16:34,060
 Because it means being partial to things that are outside

205
00:16:34,060 --> 00:16:35,000
 of our control,

206
00:16:35,000 --> 00:16:42,000
 ultimately. We can't stop them from changing and going.

207
00:16:42,000 --> 00:16:48,460
 So a good memory, a good reminder of what ultimately circum

208
00:16:48,460 --> 00:16:50,000
scribes our happiness.

209
00:16:50,000 --> 00:16:54,000
 That these things cannot be changed by us or anyone.

210
00:16:54,000 --> 00:16:58,000
 There's no practice we could do or prayer we could say,

211
00:16:58,000 --> 00:17:04,930
 and it would give us eternal life, eternal youth, eternal

212
00:17:04,930 --> 00:17:11,000
 health, eternal anything.

213
00:17:11,000 --> 00:17:16,000
 So there you go. That's the dhamma for tonight.

214
00:17:16,000 --> 00:17:23,730
 I think Robin has decided not to join tonight, which is

215
00:17:23,730 --> 00:17:25,000
 fine.

216
00:17:25,000 --> 00:17:33,000
 Certainly she acts above and beyond.

217
00:17:33,000 --> 00:17:37,000
 So we'll give her a night off.

218
00:17:37,000 --> 00:17:41,250
 But nonetheless I have a bunch of questions, so I'll answer

219
00:17:41,250 --> 00:17:42,000
 them.

220
00:17:42,000 --> 00:17:56,000
 [silence]

221
00:17:56,000 --> 00:17:59,000
 Michael, you should go.

222
00:17:59,000 --> 00:18:06,000
 [silence]

223
00:18:06,000 --> 00:18:09,560
 During sitting meditation of rising, falling, and practice

224
00:18:09,560 --> 00:18:11,000
 of daily mindfulness,

225
00:18:11,000 --> 00:18:14,000
 I began to hear and feel my heart beat with more clarity.

226
00:18:14,000 --> 00:18:17,190
 I'm identifying it with the mantra heartbeat, heartbeat,

227
00:18:17,190 --> 00:18:18,000
 heartbeat.

228
00:18:18,000 --> 00:18:22,580
 Is this a correct practice? It's not terrible, but the

229
00:18:22,580 --> 00:18:24,000
 heart is just a concept.

230
00:18:24,000 --> 00:18:26,000
 What you're really experiencing is a feeling.

231
00:18:26,000 --> 00:18:32,000
 So to be more correct, you'd want to say feeling, feeling.

232
00:18:32,000 --> 00:18:36,000
 You can also say something like beating, beating, but

233
00:18:36,000 --> 00:18:39,000
 feeling works just as well.

234
00:18:39,000 --> 00:18:43,000
 [silence]

235
00:18:43,000 --> 00:18:46,150
 The experience of sitting lingers for longer than one

236
00:18:46,150 --> 00:18:47,000
 noting period,

237
00:18:47,000 --> 00:18:50,520
 should I continue to note sitting until that one experience

238
00:18:50,520 --> 00:18:51,000
 ceases.

239
00:18:51,000 --> 00:18:55,060
 Or instead do one noting of it move on, even if the

240
00:18:55,060 --> 00:18:58,000
 experience hasn't ceased on its own.

241
00:18:58,000 --> 00:19:04,000
 Yeah, just do it once. Just do it once and move on.

242
00:19:04,000 --> 00:19:11,000
 Oh, I didn't click. It was to click. It didn't work anyway.

243
00:19:11,000 --> 00:19:30,000
 [silence]

244
00:19:30,000 --> 00:19:35,580
 It wouldn't be wrong, but it starts to get a little bit ad

245
00:19:35,580 --> 00:19:36,000
 hoc,

246
00:19:36,000 --> 00:19:39,000
 better off to stick to the program.

247
00:19:39,000 --> 00:19:45,040
 I wouldn't worry if there's still potential to focus on it

248
00:19:45,040 --> 00:19:46,000
 more.

249
00:19:46,000 --> 00:19:49,000
 Just say sitting once and then move on to the next one.

250
00:19:49,000 --> 00:19:54,000
 But that's for that specific technique that we give to you.

251
00:19:54,000 --> 00:19:58,000
 [silence]

252
00:19:58,000 --> 00:20:05,460
 The noting more steps in walking meditation is it better to

253
00:20:05,460 --> 00:20:06,000
 slow our pace

254
00:20:06,000 --> 00:20:08,000
 in order to make the noting easier.

255
00:20:08,000 --> 00:20:10,950
 Should we strive to speed up the noting to match the

256
00:20:10,950 --> 00:20:12,000
 original pace?

257
00:20:12,000 --> 00:20:16,260
 Well, each, no, it should, I mean the step itself will take

258
00:20:16,260 --> 00:20:18,000
 longer, sure.

259
00:20:18,000 --> 00:20:21,000
 But each movement should be an ordinary speed.

260
00:20:21,000 --> 00:20:23,000
 It shouldn't be too quick or too slow.

261
00:20:23,000 --> 00:20:28,640
 But because there's more steps in it, yeah, it will take

262
00:20:28,640 --> 00:20:31,000
 longer, a little bit anyway.

263
00:20:31,000 --> 00:20:37,000
 [silence]

264
00:20:37,000 --> 00:20:40,000
 I see you use the internet quite frequently.

265
00:20:40,000 --> 00:20:42,740
 Are all Buddhist monks allowed to use the internet to a

266
00:20:42,740 --> 00:20:44,000
 reasonable extent?

267
00:20:44,000 --> 00:20:48,000
 It really depends on where you are, your situation.

268
00:20:48,000 --> 00:20:52,000
 There's obviously no rule about using the internet, though.

269
00:20:52,000 --> 00:20:56,990
 I can understand the potential desire to circumscribe

270
00:20:56,990 --> 00:20:59,000
 access to the internet.

271
00:20:59,000 --> 00:21:03,000
 There's no question that it's problematic.

272
00:21:03,000 --> 00:21:12,000
 [silence]

273
00:21:12,000 --> 00:21:19,090
 Once the banner is realized, how can I be certain that I

274
00:21:19,090 --> 00:21:20,000
 will be released from rebirth cycle for good?

275
00:21:20,000 --> 00:21:22,510
 I'm sure it's a depressed state, but it's as though I don't

276
00:21:22,510 --> 00:21:28,000
 want to come back here for another life.

277
00:21:28,000 --> 00:21:31,000
 Well, I mean the cause of rebirth is craving.

278
00:21:31,000 --> 00:21:37,000
 The benefit of seeing nibbana is it reduces your craving.

279
00:21:37,000 --> 00:21:41,000
 You've seen true peace, and so you crave less and less,

280
00:21:41,000 --> 00:21:49,000
 and you're more objective and neutral, in a sense.

281
00:21:49,000 --> 00:21:52,000
 That's how you know that you're not going to be reborn,

282
00:21:52,000 --> 00:22:00,000
 because you have no more craving left, no more desire.

283
00:22:00,000 --> 00:22:05,000
 [silence]

284
00:22:05,000 --> 00:22:07,440
 What is the difference between realization, awareness, and

285
00:22:07,440 --> 00:22:08,000
 judgment?

286
00:22:08,000 --> 00:22:10,330
 How do you know that what you're saying to yourself when

287
00:22:10,330 --> 00:22:15,000
 meditate is not a judgment of your experience?

288
00:22:15,000 --> 00:22:22,000
 Well, a judgment really would be adding something to it.

289
00:22:22,000 --> 00:22:25,200
 If you feel pain and you say to yourself, "Pain, pain," you

290
00:22:25,200 --> 00:22:29,000
're not adding anything to it. It is pain.

291
00:22:29,000 --> 00:22:31,400
 Of course, if you say to yourself, "Pain, pain," you're

292
00:22:31,400 --> 00:22:33,000
 adding the dislike to it.

293
00:22:33,000 --> 00:22:35,000
 What you're saying is, "This is bad."

294
00:22:35,000 --> 00:22:39,000
 You're not saying it verbally, but you're cultivating that.

295
00:22:39,000 --> 00:22:43,360
 There does need to be a certain quality to your noting that

296
00:22:43,360 --> 00:22:45,000
 has to be neutral.

297
00:22:45,000 --> 00:22:48,260
 But if you were to sit there and say, "Bad, bad, bad," well

298
00:22:48,260 --> 00:22:50,000
, that would be a judgment.

299
00:22:50,000 --> 00:23:07,000
 [silence]

300
00:23:07,000 --> 00:23:12,690
 A couple of questions there that I'm not going to bother

301
00:23:12,690 --> 00:23:15,000
 posting or adding.

302
00:23:15,000 --> 00:23:20,000
 The questions here are meant to be about our tradition and

303
00:23:20,000 --> 00:23:25,000
 for the benefit of people practicing our tradition.

304
00:23:25,000 --> 00:23:29,000
 The ideas are about our meditation practice.

305
00:23:29,000 --> 00:23:36,080
 Or some general Buddhist principles I'm happy to discuss to

306
00:23:36,080 --> 00:23:38,000
 some extent.

307
00:23:38,000 --> 00:23:42,000
 But, yes, speculative or other sorts of questions,

308
00:23:42,000 --> 00:23:45,000
 questions about other types of practice.

309
00:23:45,000 --> 00:23:52,000
 Not really the purpose. Anyway, I hope that was useful.

310
00:23:52,000 --> 00:23:59,000
 This giving of Dhamma, a little bit of a talk every night.

311
00:23:59,000 --> 00:24:02,440
 It's good for the meditators to hear something and to give

312
00:24:02,440 --> 00:24:07,000
 them a reason and remind them of why they're doing it.

313
00:24:07,000 --> 00:24:11,000
 Thank you all for tuning in. I wish you all good practice.

314
00:24:11,000 --> 00:24:12,000
 Have a good night.

