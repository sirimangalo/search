1
00:00:00,000 --> 00:00:14,970
 Good evening everyone. We're broadcasting live. May 18th,

2
00:00:14,970 --> 00:00:21,000
 2016.

3
00:00:21,000 --> 00:00:30,000
 This quote is from the Melinda Panha.

4
00:00:30,000 --> 00:00:47,390
 In the context it's simply a description of the virtues of

5
00:00:47,390 --> 00:00:54,000
 a virtuous person.

6
00:00:54,000 --> 00:01:01,270
 It's interesting to learn the context. It's not necessary

7
00:01:01,270 --> 00:01:03,000
 but it is interesting because the reason why he...

8
00:01:03,000 --> 00:01:19,670
 The Melinda Panha is, if I haven't mentioned this before, a

9
00:01:19,670 --> 00:01:26,100
 very large text. It's a sizable text with questions and

10
00:01:26,100 --> 00:01:31,000
 answers between King Melinda and Nagasena who is an Arahant

11
00:01:31,000 --> 00:01:31,000
.

12
00:01:31,000 --> 00:01:38,430
 It's really a great book for getting some insight into the

13
00:01:38,430 --> 00:01:47,720
 Buddha's stance on various issues and on dilemmas which are

14
00:01:47,720 --> 00:01:54,000
 presented by the canonical text.

15
00:01:54,000 --> 00:02:00,050
 It's so valuable that the Burmese edition of the Tipitika I

16
00:02:00,050 --> 00:02:05,950
 understand includes the Melinda Panha in the Kudakanika

17
00:02:05,950 --> 00:02:09,000
 even though it's much later.

18
00:02:09,000 --> 00:02:14,000
 But it's definitely worth reading if you have the time.

19
00:02:14,000 --> 00:02:24,000
 So the context here is discussing suicide.

20
00:02:24,000 --> 00:02:42,250
 "Nabhiku vay ata nang pa taitabang" - one should not

21
00:02:42,250 --> 00:02:47,350
 destroy oneself. Whoever does so, "Gitada mukare tabo"

22
00:02:47,350 --> 00:02:48,000
 should be dealt with according to the Dhamma, I think.

23
00:02:48,000 --> 00:02:56,040
 Which means if a monk kills himself, there's a pen... I

24
00:02:56,040 --> 00:02:59,330
 think there's... actually I don't know. It's interesting.

25
00:02:59,330 --> 00:03:04,180
 If he's dead, then well, "Gitada mukare tabo" - think if

26
00:03:04,180 --> 00:03:11,420
 someone tries to kill themselves then they're guilty of an

27
00:03:11,420 --> 00:03:13,000
 offense.

28
00:03:13,000 --> 00:03:16,990
 So as I understand, I'm reading the Pali and as usual my

29
00:03:16,990 --> 00:03:21,160
 Pali is not great, but it looks like Melinda if I remember

30
00:03:21,160 --> 00:03:23,000
 correctly as well.

31
00:03:23,000 --> 00:03:27,600
 He says, "Well, the Buddha says this, but he also says that

32
00:03:27,600 --> 00:03:31,000
 Jadhiya, Jaraya, Bhiyadino and so on.

33
00:03:31,000 --> 00:03:35,220
 He teaches the Dhamma for the samu-chidaaya, for the

34
00:03:35,220 --> 00:03:40,060
 cutting off of these things of birth, old age, sickness and

35
00:03:40,060 --> 00:03:41,000
 death.

36
00:03:41,000 --> 00:03:44,610
 So if he teaches the Dhamma for cutting off birth, old age,

37
00:03:44,610 --> 00:03:48,000
 sickness and death, what's wrong with killing yourself?

38
00:03:48,000 --> 00:03:51,000
 Because if you kill yourself, these things only last up

39
00:03:51,000 --> 00:03:53,970
 until death and if you kill yourself, then you don't have

40
00:03:53,970 --> 00:03:55,000
 these things.

41
00:03:55,000 --> 00:03:59,410
 And I guess the idea is, why wouldn't an arahant? Because

42
00:03:59,410 --> 00:04:02,300
 an ordinary person obviously it's not an escape. If you

43
00:04:02,300 --> 00:04:05,000
 kill yourself, you're just going to be born again.

44
00:04:05,000 --> 00:04:16,850
 But why wouldn't an arahant do it? I think that's what he's

45
00:04:16,850 --> 00:04:20,000
 saying.

46
00:04:20,000 --> 00:04:23,610
 And then he said, "Why does he teach both of these? Why can

47
00:04:23,610 --> 00:04:25,000
 these both be true?

48
00:04:25,000 --> 00:04:28,400
 If he teaches one, why does he say it's wrong for an arah

49
00:04:28,400 --> 00:04:30,000
ant to kill himself?"

50
00:04:30,000 --> 00:04:36,190
 And the Buddhists, and then Nagasena says, "Well, this is

51
00:04:36,190 --> 00:04:43,250
 because a sīla-vā, a virtuous person, sīla-sampan-no, ag

52
00:04:43,250 --> 00:04:48,000
adas-samo-sattā-nanga is like..."

53
00:04:48,000 --> 00:04:55,540
 And then he gives these ten similes. "One who is virtuous

54
00:04:55,540 --> 00:04:59,570
 is like an antidote for destroying the poisons of defile

55
00:04:59,570 --> 00:05:01,000
ments in beings."

56
00:05:01,000 --> 00:05:06,640
 So if you have this sickness of greed, anger and delusion,

57
00:05:06,640 --> 00:05:09,000
 what is the antidote?

58
00:05:09,000 --> 00:05:17,000
 Where do you find the cure to the defilements of the mind?

59
00:05:17,000 --> 00:05:19,350
 If you have an anger problem, if you have an addiction

60
00:05:19,350 --> 00:05:22,000
 problem, what is the cure?

61
00:05:22,000 --> 00:05:26,410
 The cure is a virtuous person. And by this he means an arah

62
00:05:26,410 --> 00:05:29,570
ant, someone who has understood the truth and who has freed

63
00:05:29,570 --> 00:05:35,000
 themselves from all greed, anger and delusion.

64
00:05:35,000 --> 00:05:39,430
 If you find such a person, they are the antidote. And if

65
00:05:39,430 --> 00:05:43,710
 you partake of them in terms of listening to their teaching

66
00:05:43,710 --> 00:05:46,990
, associating with them, listening to their teaching,

67
00:05:46,990 --> 00:05:50,000
 remembering their teaching, understanding their teaching

68
00:05:50,000 --> 00:05:53,940
 and putting their teaching into practice, you can cure

69
00:05:53,940 --> 00:05:55,000
 yourself.

70
00:05:55,000 --> 00:06:01,000
 They are the antidote.

71
00:06:01,000 --> 00:06:05,010
 That's the first one. There are ten similes. The second is

72
00:06:05,010 --> 00:06:08,850
 there a healing balm for laying the sickness of defilements

73
00:06:08,850 --> 00:06:10,000
 in beings.

74
00:06:10,000 --> 00:06:15,050
 So if you have the fever of defilements, defilements are

75
00:06:15,050 --> 00:06:19,620
 seen as a fever, greed is a fever, it makes you hot and

76
00:06:19,620 --> 00:06:21,000
 bothered.

77
00:06:21,000 --> 00:06:29,990
 Anger is a fever, it burns you up. Delusion is a fever, it

78
00:06:29,990 --> 00:06:36,000
 makes you drunk and uncontrollable.

79
00:06:36,000 --> 00:06:42,770
 But this is a healing balm that cools and soothes and heals

80
00:06:42,770 --> 00:06:45,000
 your sickness.

81
00:06:45,000 --> 00:06:50,600
 Number three is like a precious gem because they grant all

82
00:06:50,600 --> 00:06:53,000
 beings their wishes.

83
00:06:53,000 --> 00:06:58,480
 So there's this special gem in Indian mythology, a wish-ful

84
00:06:58,480 --> 00:07:00,000
filling gem.

85
00:07:00,000 --> 00:07:04,700
 So this comes up in Buddhism often as a simile, talking

86
00:07:04,700 --> 00:07:08,000
 about the wish-fulfilling jewel.

87
00:07:08,000 --> 00:07:13,190
 This jewel, if you have this, it's like the genie in this

88
00:07:13,190 --> 00:07:15,840
 lamp. But in Indian mythology it's a gem and if you rub the

89
00:07:15,840 --> 00:07:22,000
 gem or if you possess the gem, all your wishes come true.

90
00:07:22,000 --> 00:07:27,000
 And so an Arahant, an enlightened being, a virtuous being,

91
00:07:27,000 --> 00:07:32,530
 is like this gem because they give people what they want,

92
00:07:32,530 --> 00:07:34,000
 happiness.

93
00:07:34,000 --> 00:07:38,530
 Sometimes we don't even know what we want. We think we want

94
00:07:38,530 --> 00:07:42,480
 something and see, an Arahant obviously can't give you

95
00:07:42,480 --> 00:07:45,000
 riches and so on and power.

96
00:07:45,000 --> 00:07:49,000
 But they can give you what you need, I guess you would say.

97
00:07:49,000 --> 00:07:54,000
 They can't give you what you want necessarily.

98
00:07:54,000 --> 00:07:57,700
 But what we want is always happiness. We want certain

99
00:07:57,700 --> 00:08:00,000
 things because we think they bring us happiness.

100
00:08:00,000 --> 00:08:06,210
 We want satisfaction. We're just deluded into thinking that

101
00:08:06,210 --> 00:08:11,000
 things that are unsatisfying are going to satisfy us.

102
00:08:11,000 --> 00:08:15,640
 We're deluded into thinking that things that are imper

103
00:08:15,640 --> 00:08:20,970
manent, things that are uncontrollable are stable and contro

104
00:08:20,970 --> 00:08:22,000
llable.

105
00:08:22,000 --> 00:08:27,140
 An enlightened being, when we follow their teachings, we

106
00:08:27,140 --> 00:08:32,000
 find what is stable, satisfying and controllable.

107
00:08:32,000 --> 00:08:37,060
 Well, not controllable actually. Put that one aside. Stable

108
00:08:37,060 --> 00:08:41,000
 and satisfying anyway.

109
00:08:41,000 --> 00:08:47,500
 That's number three. Number four is like a ship for beings

110
00:08:47,500 --> 00:08:51,000
 to go beyond the four floods.

111
00:08:51,000 --> 00:09:04,910
 So the floods are Kamuga, Babuoka, Avijoga. I forget what

112
00:09:04,910 --> 00:09:10,000
 are the four floods.

113
00:09:10,000 --> 00:09:39,500
 Overcoming the defilements, overcoming the A

114
00:09:39,500 --> 00:09:49,090
 and Avijoga is the flood of ignorance. But in general it's

115
00:09:49,090 --> 00:09:52,500
 just another way of describing the defilements.

116
00:09:52,500 --> 00:09:55,580
 But the idea of the imagery of the ocean is a common one. I

117
00:09:55,580 --> 00:10:00,500
'm sorry it's like an ocean that we all drown and flounder

118
00:10:00,500 --> 00:10:02,500
 around in.

119
00:10:02,500 --> 00:10:07,070
 We're never seeing the shore. We're just lost in this vast

120
00:10:07,070 --> 00:10:12,670
 ocean of nothingness and meaninglessness until we find a

121
00:10:12,670 --> 00:10:15,500
 ship that can sail us.

122
00:10:15,500 --> 00:10:19,340
 Because if you're just swimming around in the ocean, you

123
00:10:19,340 --> 00:10:22,500
 just go around in circles, you get nowhere.

124
00:10:22,500 --> 00:10:27,130
 But if you have a ship, you can cut through the ocean, you

125
00:10:27,130 --> 00:10:32,970
 can go in whatever direction you like. You can find the

126
00:10:32,970 --> 00:10:34,500
 shore.

127
00:10:34,500 --> 00:10:38,870
 He or she is like a caravan leader for taking beings across

128
00:10:38,870 --> 00:10:41,500
 the desert of repeated births.

129
00:10:41,500 --> 00:10:46,610
 We rebirth is like a desert. We're always thirsting, we're

130
00:10:46,610 --> 00:10:51,470
 always hot with our defilements and our addictions and our

131
00:10:51,470 --> 00:10:52,500
 aversions.

132
00:10:52,500 --> 00:10:57,010
 And the suffering that comes from old age sickness and

133
00:10:57,010 --> 00:11:00,500
 death, we suffer like being in a desert.

134
00:11:00,500 --> 00:11:04,500
 But a caravan leader can lead us through these things.

135
00:11:04,500 --> 00:11:08,800
 Here she is like the wind for extinguishing the three

136
00:11:08,800 --> 00:11:13,500
 fierce fires in beings as greed, anger and delusions.

137
00:11:13,500 --> 00:11:18,300
 The wind, they come in and they douse the fire. They blow

138
00:11:18,300 --> 00:11:22,500
 the fire out with the power of their teachings.

139
00:11:22,500 --> 00:11:26,970
 They're like a great rain cloud for filling beings with

140
00:11:26,970 --> 00:11:28,500
 good thoughts.

141
00:11:28,500 --> 00:11:35,010
 So like a great rain cloud fills up the lakes and rivers,

142
00:11:35,010 --> 00:11:47,500
 brings rain and waters the crops and cools the land.

143
00:11:47,500 --> 00:11:52,600
 So too a great leader, a great teacher, a great being,

144
00:11:52,600 --> 00:11:54,500
 someone who has purified themselves.

145
00:11:54,500 --> 00:12:02,350
 This is someone who is enlightened and why they should stay

146
00:12:02,350 --> 00:12:04,500
 or stick around.

147
00:12:04,500 --> 00:12:09,110
 Because they have so much to offer. The world is dry

148
00:12:09,110 --> 00:12:12,500
 without enlightened beings.

149
00:12:12,500 --> 00:12:16,590
 Without them you can't grow anything. People don't grow

150
00:12:16,590 --> 00:12:19,500
 spiritually, not in any meaningful way.

151
00:12:19,500 --> 00:12:29,500
 Just go around in circles, dry up like crops without water.

152
00:12:29,500 --> 00:12:32,330
 Like a teacher for encouraging beings to train themselves

153
00:12:32,330 --> 00:12:33,500
 and what is skilled.

154
00:12:33,500 --> 00:12:38,220
 It's interesting, it's like a teacher, as opposed to just

155
00:12:38,220 --> 00:12:39,500
 being a teacher.

156
00:12:39,500 --> 00:12:44,720
 Acharya Samo, but it's interesting because in many ways the

157
00:12:44,720 --> 00:12:49,570
 enlightened followers of the Buddha aren't considered

158
00:12:49,570 --> 00:12:50,500
 teachers.

159
00:12:50,500 --> 00:12:54,860
 We don't consider ourselves to be teachers per se. We're

160
00:12:54,860 --> 00:12:56,500
 friends. We're good friends.

161
00:12:56,500 --> 00:13:04,020
 Why? Because we give good things. We, as in Buddhists who

162
00:13:04,020 --> 00:13:08,500
 practice,

163
00:13:08,500 --> 00:13:14,220
 we have good things to give. And so, enlightened being and

164
00:13:14,220 --> 00:13:19,500
 Arahant has the greatest to give.

165
00:13:19,500 --> 00:13:22,690
 So we don't consider, we should never, even if one becomes

166
00:13:22,690 --> 00:13:23,500
 enlightened,

167
00:13:23,500 --> 00:13:26,590
 one would not consider oneself a teacher, but rather a

168
00:13:26,590 --> 00:13:29,770
 friend, someone who offers advice and passes on the

169
00:13:29,770 --> 00:13:31,500
 Buddhist teaching.

170
00:13:31,500 --> 00:13:35,960
 But they're like a teacher, because they do the same. They

171
00:13:35,960 --> 00:13:38,500
 might as well be called a teacher,

172
00:13:38,500 --> 00:13:43,500
 because they encourage beings to train themselves.

173
00:13:43,500 --> 00:13:47,670
 And finally, like a good guide for pointing out to beings

174
00:13:47,670 --> 00:13:49,500
 the path to security.

175
00:13:49,500 --> 00:13:58,180
 So we're lost, we're so lost. We're going in the wrong

176
00:13:58,180 --> 00:14:02,500
 direction. We're running around in circles.

177
00:14:02,500 --> 00:14:06,040
 Can't find the way out. If you've ever been in a large

178
00:14:06,040 --> 00:14:09,350
 forest, it's quite scary, easy to get lost, easy to lose

179
00:14:09,350 --> 00:14:10,500
 your direction.

180
00:14:10,500 --> 00:14:16,020
 And you're constantly misjudging. It's very easy to go

181
00:14:16,020 --> 00:14:17,500
 around in circles,

182
00:14:17,500 --> 00:14:19,060
 because you think, "Oh, maybe I have to go a little bit to

183
00:14:19,060 --> 00:14:22,140
 the left," and you constantly think that until you wind up

184
00:14:22,140 --> 00:14:22,500
 back.

185
00:14:22,500 --> 00:14:27,430
 I did this once, wound up back where I started, because I

186
00:14:27,430 --> 00:14:30,500
 actually walked in a circle.

187
00:14:30,500 --> 00:14:32,590
 It's very hard to get out of the forest if you don't know

188
00:14:32,590 --> 00:14:35,730
 where you're going, if you've never been there before, if

189
00:14:35,730 --> 00:14:38,500
 you don't have direction.

190
00:14:38,500 --> 00:14:41,880
 If you don't have a guide, but someone who has been through

191
00:14:41,880 --> 00:14:43,500
 the forest, who knows the forest,

192
00:14:43,500 --> 00:14:53,400
 and who knows the way out of the forest, can guide you out

193
00:14:53,400 --> 00:14:56,500
 of the forest.

194
00:14:56,500 --> 00:15:00,880
 So that's our Dhamma for tonight. Reasons why not to kill

195
00:15:00,880 --> 00:15:01,500
 yourself.

196
00:15:01,500 --> 00:15:05,380
 It seems like killing yourself wouldn't be, you could more

197
00:15:05,380 --> 00:15:07,500
 easily explain it, potentially,

198
00:15:07,500 --> 00:15:11,310
 by just the idea that an enlightened being would have no

199
00:15:11,310 --> 00:15:13,500
 reason to kill themselves.

200
00:15:13,500 --> 00:15:19,500
 Killing yourself would require a desire for it to all end.

201
00:15:19,500 --> 00:15:22,510
 So it seems like an Arahant wouldn't have any desire to

202
00:15:22,510 --> 00:15:23,500
 stick around,

203
00:15:23,500 --> 00:15:30,230
 but they would just stick around and wait, let things work

204
00:15:30,230 --> 00:15:32,500
 themselves out.

205
00:15:32,500 --> 00:15:36,490
 But it seems like there's something a little more here,

206
00:15:36,490 --> 00:15:38,500
 like the idea that they do things.

207
00:15:38,500 --> 00:15:43,150
 They eat alms, for example, why they don't just stop eating

208
00:15:43,150 --> 00:15:43,500
.

209
00:15:43,500 --> 00:15:48,980
 They take alms because of this, because of some sense of

210
00:15:48,980 --> 00:15:49,500
 duty,

211
00:15:49,500 --> 00:15:57,500
 or some rightness to passing on what they have gained.

212
00:15:57,500 --> 00:16:03,080
 And it seems they do, Arahants seem to teach and pass on

213
00:16:03,080 --> 00:16:07,500
 the teaching, rather than just kill themselves.

214
00:16:07,500 --> 00:16:10,640
 Hey, we got a whole bunch of people online, good to see,

215
00:16:10,640 --> 00:16:11,500
 good crowd.

216
00:16:11,500 --> 00:16:15,500
 Thank you all for showing up.

217
00:16:15,500 --> 00:16:20,500
 So it looks like the iOS app is still in the works.

218
00:16:20,500 --> 00:16:25,500
 I'm going to try to proactively revamp this website.

219
00:16:25,500 --> 00:16:29,860
 We've got a new version of it, but I just haven't put any

220
00:16:29,860 --> 00:16:31,500
 effort into getting it online,

221
00:16:31,500 --> 00:16:34,500
 so I'm going to work with the person.

222
00:16:34,500 --> 00:16:38,340
 But that also means probably that the iOS app has to be

223
00:16:38,340 --> 00:16:40,500
 fixed for the new server.

224
00:16:40,500 --> 00:16:43,950
 I'm not sure we're going to have to work together to make

225
00:16:43,950 --> 00:16:44,500
 this all,

226
00:16:44,500 --> 00:16:46,500
 and get it secure as well.

227
00:16:46,500 --> 00:16:54,570
 Our whole server apparently has to have some kind of HTTPS

228
00:16:54,570 --> 00:16:55,500
 thing.

229
00:16:55,500 --> 00:17:01,840
 It's a bit beyond my pay grade, but somebody knows how to

230
00:17:01,840 --> 00:17:02,500
 do it.

231
00:17:02,500 --> 00:17:06,500
 So we've got to all work together to make this happen.

232
00:17:06,500 --> 00:17:10,220
 If anybody wants to volunteer in our organization in any

233
00:17:10,220 --> 00:17:10,500
 way,

234
00:17:10,500 --> 00:17:16,090
 we're always looking for volunteers, I think. I think we

235
00:17:16,090 --> 00:17:16,500
 are.

236
00:17:16,500 --> 00:17:19,950
 Anyway, get in touch, and if we need volunteers, we'll let

237
00:17:19,950 --> 00:17:20,500
 you know.

238
00:17:20,500 --> 00:17:25,880
 As usual, as per last night, we're still looking for a stew

239
00:17:25,880 --> 00:17:26,500
ard.

240
00:17:26,500 --> 00:17:33,500
 So if anybody wants to come and stay with us for a while,

241
00:17:33,500 --> 00:17:38,500
 starting in July, that'd be great.

242
00:17:38,500 --> 00:17:44,500
 Let us know.

243
00:17:44,500 --> 00:17:46,500
 Right, so we've got a couple of questions.

244
00:17:46,500 --> 00:17:50,500
 Does anyone know what are the five skandhas?

245
00:17:50,500 --> 00:17:53,500
 We call them khandhas in Pali.

246
00:17:53,500 --> 00:18:00,500
 Khandhas are aggregates, and these are rupa, the form.

247
00:18:00,500 --> 00:18:03,500
 Vedana means feeling.

248
00:18:03,500 --> 00:18:07,500
 Sanya means recognition, or so on.

249
00:18:07,500 --> 00:18:13,500
 Sankara is mental formations.

250
00:18:13,500 --> 00:18:17,500
 And vinyan is consciousness.

251
00:18:17,500 --> 00:18:20,500
 So when you see something, there is the physical aspect.

252
00:18:20,500 --> 00:18:24,500
 There is the vedana, the feeling about it, pleasant,

253
00:18:24,500 --> 00:18:25,500
 painful, neutral.

254
00:18:25,500 --> 00:18:29,770
 There is a recognition, you recognize it as a cat, or so on

255
00:18:29,770 --> 00:18:30,500
, that you see.

256
00:18:30,500 --> 00:18:34,200
 There's the mental formations, means you judge it, or you

257
00:18:34,200 --> 00:18:36,500
 like it, or dislike it, or so on.

258
00:18:36,500 --> 00:18:40,500
 You examine it, and you react to it.

259
00:18:40,500 --> 00:18:50,340
 And vinyana just means the consciousness, the fact that you

260
00:18:50,340 --> 00:18:56,500
're aware.

261
00:18:56,500 --> 00:19:00,500
 What does unsystematic attention mean?

262
00:19:00,500 --> 00:19:03,500
 Unsystematic is just a loose translation.

263
00:19:03,500 --> 00:19:07,500
 That's not really what the... I think that's re-referring

264
00:19:07,500 --> 00:19:08,500
 to ayonisomanasikara.

265
00:19:08,500 --> 00:19:12,500
 Ayonisom means wise, ayonisom means unwise.

266
00:19:12,500 --> 00:19:17,900
 So when you observe something, and you react to it, this is

267
00:19:17,900 --> 00:19:19,500
 when you like it or dislike it,

268
00:19:19,500 --> 00:19:23,500
 when you let yourself get caught up in reactions to it.

269
00:19:23,500 --> 00:19:27,500
 If you've yonisomanasikara, it means you see it wisely.

270
00:19:27,500 --> 00:19:29,500
 You see it as impermanent suffering and non-self.

271
00:19:29,500 --> 00:19:33,240
 You see it as it is permanent, unsatisfying, uncontrollable

272
00:19:33,240 --> 00:19:33,500
.

273
00:19:33,500 --> 00:19:35,500
 You don't get attached to it.

274
00:19:35,500 --> 00:19:39,080
 You see it objectively, just as something that arises, and

275
00:19:39,080 --> 00:19:41,500
 that's wise attention.

276
00:19:41,500 --> 00:19:50,500
 But wise is probably better than systematic.

277
00:19:50,500 --> 00:19:55,500
 Are all fears instances of... instances of dosa?

278
00:19:55,500 --> 00:19:59,730
 Yeah, vayanyana doesn't fear, according to the vysuddhi

279
00:19:59,730 --> 00:20:00,500
 manga.

280
00:20:00,500 --> 00:20:04,420
 Vayanyana doesn't fear because fear is always associated

281
00:20:04,420 --> 00:20:05,500
 with patika.

282
00:20:05,500 --> 00:20:08,030
 Actually, it's not the vysuddhi manga. I think it's the

283
00:20:08,030 --> 00:20:11,500
 commentary or the sub-commentary to the vysuddhi manga.

284
00:20:11,500 --> 00:20:17,500
 Asks, does vayayana fear? No, it doesn't.

285
00:20:17,500 --> 00:20:20,720
 I think maybe the vysuddhi manga is the one that says, no,

286
00:20:20,720 --> 00:20:21,500
 it doesn't fear.

287
00:20:21,500 --> 00:20:24,810
 And the commentary to the vysuddhi manga says, why doesn't

288
00:20:24,810 --> 00:20:25,500
 it fear?

289
00:20:25,500 --> 00:20:30,260
 Because fear is always associated with patika, with

290
00:20:30,260 --> 00:20:31,500
 aversion.

291
00:20:31,500 --> 00:20:35,500
 So it's not really fear. Vayanyana isn't fear.

292
00:20:35,500 --> 00:20:38,500
 Vayanyana is seeing the danger.

293
00:20:38,500 --> 00:20:43,000
 When you see the danger being reborn, you see the danger in

294
00:20:43,000 --> 00:20:47,500
 attachment, that kind of thing.

295
00:20:47,500 --> 00:20:50,570
 It's like when you see fire off in the distance, this is

296
00:20:50,570 --> 00:20:52,500
 what the vysuddhi manga says.

297
00:20:52,500 --> 00:20:55,360
 You see fire off in the distance, you think, wow, if anyone

298
00:20:55,360 --> 00:20:57,500
 falls into that fire, they're going to burn themselves.

299
00:20:57,500 --> 00:21:03,110
 But you yourself, you're not afraid because it's over there

300
00:21:03,110 --> 00:21:03,500
.

301
00:21:03,500 --> 00:21:05,500
 So dnyana itself isn't afraid.

302
00:21:05,500 --> 00:21:09,500
 But during meditation, of course, you can be afraid.

303
00:21:09,500 --> 00:21:16,500
 It's just that the fear isn't dnyana, the knowledge.

304
00:21:17,500 --> 00:21:20,500
 Visa for Canada, six months usually.

305
00:21:20,500 --> 00:21:22,810
 Usually you can come to Canada for six months if you want

306
00:21:22,810 --> 00:21:23,500
 to extend it.

307
00:21:23,500 --> 00:21:27,470
 I think there are ways to extend it, but there's probably a

308
00:21:27,470 --> 00:21:29,500
 lot of paperwork involved.

309
00:21:29,500 --> 00:21:39,500
 I just got back from New York today.

310
00:21:39,500 --> 00:21:43,500
 We drove since morning.

311
00:21:43,500 --> 00:21:54,270
 So, Saturday, Sunday, we've got something at Cambodian

312
00:21:54,270 --> 00:21:55,500
 monastery.

313
00:21:55,500 --> 00:21:59,500
 We might have another meditator tomorrow, we'll see.

314
00:21:59,500 --> 00:22:02,410
 But I'm also going to try to make it over to my father's

315
00:22:02,410 --> 00:22:04,500
 house, we'll see how that works.

316
00:22:04,500 --> 00:22:06,500
 How am I going to work that?

317
00:22:06,500 --> 00:22:08,500
 We'll all be around.

318
00:22:08,500 --> 00:22:13,310
 Next weekend is waysuck, don't forget Mississauga if you're

319
00:22:13,310 --> 00:22:14,500
 in the area.

320
00:22:14,500 --> 00:22:18,500
 Saturday the 28th, come on out.

321
00:22:36,500 --> 00:22:39,500
 Are there any other questions?

322
00:22:39,500 --> 00:22:58,750
 When a monk or an ant or so forth passes on, would they be

323
00:22:58,750 --> 00:23:01,500
 mindful like when falling asleep?

324
00:23:01,500 --> 00:23:06,220
 I.e. trying to be mindful of each moment of the moment of

325
00:23:06,220 --> 00:23:07,500
 transition.

326
00:23:07,500 --> 00:23:13,500
 On our hunt, when they pass on, they aren't born again.

327
00:23:13,500 --> 00:23:21,500
 As for a monk, well, monks can be of course corrupt.

328
00:23:21,500 --> 00:23:26,500
 It doesn't say anything just because someone is a monk.

329
00:23:26,500 --> 00:23:29,790
 Let's ask if a meditator would be mindful like when falling

330
00:23:29,790 --> 00:23:30,500
 asleep.

331
00:23:30,500 --> 00:23:33,440
 Not like when falling asleep, because death isn't like

332
00:23:33,440 --> 00:23:34,500
 falling asleep.

333
00:23:34,500 --> 00:23:37,500
 Death is like an out of body experience.

334
00:23:37,500 --> 00:23:44,070
 The body stops working, so the mind leaves the body, stops

335
00:23:44,070 --> 00:23:46,500
 working with the body.

336
00:23:46,500 --> 00:23:53,440
 Sometimes it takes a lot, but the mind eventually is, over

337
00:23:53,440 --> 00:23:55,500
 a period of time,

338
00:23:55,500 --> 00:24:00,210
 sees that the body reacts to the fact that the body is no

339
00:24:00,210 --> 00:24:02,500
 longer working properly.

340
00:24:02,500 --> 00:24:04,500
 That's why it leaves.

341
00:24:04,500 --> 00:24:06,830
 You have either a near-death experience if the body starts

342
00:24:06,830 --> 00:24:07,500
 working again,

343
00:24:07,500 --> 00:24:10,290
 or you have a death experience if the body doesn't ever

344
00:24:10,290 --> 00:24:11,500
 stop working again.

345
00:24:11,500 --> 00:24:16,050
 Then the mind goes on and does whatever, maybe back to be

346
00:24:16,050 --> 00:24:17,500
 born again.

347
00:24:17,500 --> 00:24:20,650
 But if you're mindful, it's just that this process becomes

348
00:24:20,650 --> 00:24:21,500
 a lot smoother

349
00:24:21,500 --> 00:24:26,070
 and you're less likely to make improper choices, as with

350
00:24:26,070 --> 00:24:27,500
 everything in mind.

351
00:24:27,500 --> 00:24:33,500
 During the time of death, it's probably pretty quick.

352
00:24:33,500 --> 00:24:35,500
 There's a lot that goes on, and if you're not mindful,

353
00:24:35,500 --> 00:24:44,500
 it's easy to get lost and go the wrong way.

354
00:24:44,500 --> 00:24:46,500
 The 20th here is actually wasted.

355
00:24:46,500 --> 00:24:50,970
 The 20th in Thailand, I think, is wasted, maybe the 24th

356
00:24:50,970 --> 00:24:52,500
 first some places.

357
00:24:52,500 --> 00:24:56,500
 But the 28th is when we're having our celebration here.

358
00:24:56,500 --> 00:25:02,500
 It's just a big thing here in Canada, in Ontario.

359
00:25:02,500 --> 00:25:19,500
 [laughs]

360
00:25:19,500 --> 00:25:21,500
 Okay, well, I'm going to go.

361
00:25:21,500 --> 00:25:28,500
 It's been a bit of a long day, but I'll try and...

362
00:25:28,500 --> 00:25:31,030
 Well, this next little while is going to be a little bit

363
00:25:31,030 --> 00:25:31,500
 erratic,

364
00:25:31,500 --> 00:25:35,500
 but for the next week or so, we should be fairly stable.

365
00:25:35,500 --> 00:25:40,500
 So I'll be back again tomorrow, most likely.

366
00:25:40,500 --> 00:25:44,500
 Anyway, have a good night, everyone. Be well.

367
00:25:44,500 --> 00:25:49,500
 [silence]

