1
00:00:00,000 --> 00:00:02,000
 Okay, go ahead.

2
00:00:02,000 --> 00:00:05,470
 Hi, Bhante. When noting various thoughts, such as rising

3
00:00:05,470 --> 00:00:08,420
 and falling, is it better to simply draw awareness to the

4
00:00:08,420 --> 00:00:12,000
 thought or to state the word in one's mind?

5
00:00:12,000 --> 00:00:16,590
 Draw awareness to the thought. I don't know what the

6
00:00:16,590 --> 00:00:20,000
 meaning is of either of those, really.

7
00:00:25,000 --> 00:00:29,390
 I don't know how you would draw your awareness to the

8
00:00:29,390 --> 00:00:31,600
 thought. Oh, wait. Yeah, no, I don't know how you would do

9
00:00:31,600 --> 00:00:32,000
 that.

10
00:00:32,000 --> 00:00:45,410
 I wouldn't worry about such things. I would, you know, the

11
00:00:45,410 --> 00:00:48,040
 problems that you have with the noting and with the

12
00:00:48,040 --> 00:00:52,000
 practice simply come from inexperience.

13
00:00:52,000 --> 00:00:55,640
 Eventually you figure it out. You get it. You realize what

14
00:00:55,640 --> 00:00:58,000
 the benefit is, what the purpose is.

15
00:00:58,000 --> 00:01:04,090
 You see what leads you to clarity of mind and what leads to

16
00:01:04,090 --> 00:01:05,000
 suffering.

17
00:01:05,000 --> 00:01:10,700
 And so your acknowledgement naturally becomes beneficial

18
00:01:10,700 --> 00:01:13,000
 and you just get better.

19
00:01:13,000 --> 00:01:16,870
 It's like kind of like playing tennis. You'll know when the

20
00:01:16,870 --> 00:01:18,000
 ball goes over the net.

21
00:01:18,000 --> 00:01:22,000
 I always bring up tennis because I had a horrible time.

22
00:01:22,000 --> 00:01:24,020
 Tennis was like a nightmare for me, learning how to play

23
00:01:24,020 --> 00:01:27,300
 and all these rules about how to move your racket, how to

24
00:01:27,300 --> 00:01:29,000
 lift the ball and I could never hit it.

25
00:01:29,000 --> 00:01:33,210
 You'll know when it goes over the net. So you'll know when

26
00:01:33,210 --> 00:01:34,000
 it works.

27
00:01:34,000 --> 00:01:37,100
 And in the beginning, it's just a lot of practice and

28
00:01:37,100 --> 00:01:39,000
 testing and figuring it out.

29
00:01:39,000 --> 00:01:42,420
 With any training, it's like that. Well, not with any

30
00:01:42,420 --> 00:01:46,820
 training. With trainings like this, any intricate, refined

31
00:01:46,820 --> 00:01:50,000
 training, and this is like the most refined training.

32
00:01:50,000 --> 00:01:53,680
 It's really something, a skill that you have to work at,

33
00:01:53,680 --> 00:01:55,000
 maybe for years.

34
00:01:55,000 --> 00:01:59,700
 I mean, the point is when you're truly mindful, the moment

35
00:01:59,700 --> 00:02:03,500
 when you're truly mindful is the moment when you attain the

36
00:02:03,500 --> 00:02:05,000
 eightfold noble path.

37
00:02:05,000 --> 00:02:14,850
 And that moment is the door to cessation. That's the moment

38
00:02:14,850 --> 00:02:18,000
 where you realize nirvana.

39
00:02:18,000 --> 00:02:21,220
 So up until that point, you're really just practicing,

40
00:02:21,220 --> 00:02:23,000
 refining, honing your skill.

41
00:02:23,000 --> 00:02:26,150
 You're still not doing it right until you get to that

42
00:02:26,150 --> 00:02:29,310
 moment where you're actually doing it right and you

43
00:02:29,310 --> 00:02:31,000
 actually see clearly.

44
00:02:31,000 --> 00:02:36,000
 So up until that point, we're practicing.

