1
00:00:00,000 --> 00:00:08,940
 Okay, here we are with Michael. So, first question. Why did

2
00:00:08,940 --> 00:00:12,760
 you start meditating? What

3
00:00:12,760 --> 00:00:17,400
 was it that made you think, "I gotta try meditating?"

4
00:00:17,400 --> 00:00:22,830
 Right. Well, the genius of meditation, it isn't rooted in

5
00:00:22,830 --> 00:00:25,380
 metaphysics, but is very

6
00:00:25,380 --> 00:00:30,650
 deeply and intensely psychological. So, what meditation

7
00:00:30,650 --> 00:00:32,560
 teaches you to do is to

8
00:00:32,560 --> 00:00:35,340
 just be aware and mindful of literally everything that

9
00:00:35,340 --> 00:00:36,400
 happens in your life.

10
00:00:36,400 --> 00:00:40,240
 And through that, you're able to see what is actually

11
00:00:40,240 --> 00:00:42,240
 causing you all the pain and

12
00:00:42,240 --> 00:00:44,530
 the things that you don't like. So, in a way, you kind of

13
00:00:44,530 --> 00:00:45,600
 distance yourself from

14
00:00:45,600 --> 00:00:50,090
 it and you get this understanding that just obliterates

15
00:00:50,090 --> 00:00:51,680
 pretty much everything.

16
00:00:51,680 --> 00:00:54,240
 If you take it seriously enough, you can be sort of

17
00:00:54,240 --> 00:00:55,640
 invincible in a way. So, but

18
00:00:55,640 --> 00:00:58,070
 like, did you have problems that you thought meditation

19
00:00:58,070 --> 00:00:59,240
 would help you with? Oh,

20
00:00:59,240 --> 00:01:03,720
 yeah, for sure. What sort of problems? Well, you know, just

21
00:01:03,720 --> 00:01:04,400
 like the typical

22
00:01:04,400 --> 00:01:08,970
 everyday family stuff, of course, but the other things too,

23
00:01:08,970 --> 00:01:10,240
 you know, you look

24
00:01:10,240 --> 00:01:14,590
 around at the state of the world and you don't really know

25
00:01:14,590 --> 00:01:16,240
 how to help it. But

26
00:01:16,240 --> 00:01:18,920
 then you... So, you started meditation thinking you could

27
00:01:18,920 --> 00:01:20,080
 help people with it? A

28
00:01:20,080 --> 00:01:22,420
 little bit, because you help yourself. You start

29
00:01:22,420 --> 00:01:23,960
 understanding why people feel the

30
00:01:23,960 --> 00:01:27,570
 way they do. And then through that, you educate yourself in

31
00:01:27,570 --> 00:01:28,440
 a way, you know what

32
00:01:28,440 --> 00:01:31,320
 I mean? So, what was your first experience of meditation

33
00:01:31,320 --> 00:01:35,160
 life? Well, the beauty of

34
00:01:35,160 --> 00:01:37,760
 YouTube is that you can really search anything and you

35
00:01:37,760 --> 00:01:39,640
 could find it. And from

36
00:01:39,640 --> 00:01:51,160
 then on, I got into a lot of Tibetan Buddhism at first. And

37
00:01:51,160 --> 00:01:52,040
 so, you started

38
00:01:52,040 --> 00:01:56,960
 meditating via YouTube? Yeah, yeah. Where I'm from, there's

39
00:01:56,960 --> 00:01:57,480
 not a whole lot of

40
00:01:57,480 --> 00:01:59,890
 resources that I can get into. There's not a whole lot of

41
00:01:59,890 --> 00:02:00,760
 temples, not even

42
00:02:00,760 --> 00:02:04,180
 meditation centers. So, I had to start with YouTube and of

43
00:02:04,180 --> 00:02:05,600
 course, actually, I'd

44
00:02:05,600 --> 00:02:08,670
 find you and all that. But it was really from YouTube when

45
00:02:08,670 --> 00:02:09,760
 I learned about it. And

46
00:02:09,760 --> 00:02:11,920
 then I started really reading a lot of books and getting

47
00:02:11,920 --> 00:02:12,840
 into it from there. And

48
00:02:12,840 --> 00:02:17,000
 it just kind of exponentially got better. When you got here

49
00:02:17,000 --> 00:02:17,960
 and started

50
00:02:17,960 --> 00:02:22,430
 meditating, was this your first intensive experience? Oh

51
00:02:22,430 --> 00:02:24,120
 yeah, for sure. How was that?

52
00:02:24,120 --> 00:02:26,720
 How were your first few days here? The first few days were

53
00:02:26,720 --> 00:02:28,640
... I don't want to say

54
00:02:28,640 --> 00:02:33,330
 hell or awful, but it was definitely a huge adjustment, you

55
00:02:33,330 --> 00:02:34,440
 know, when you're

56
00:02:34,440 --> 00:02:36,990
 so used to just doing stuff and thinking and all this stuff

57
00:02:36,990 --> 00:02:38,400
. And it was

58
00:02:38,400 --> 00:02:41,040
 really maybe a week and a half into where you finally get

59
00:02:41,040 --> 00:02:41,960
 settled, you get

60
00:02:41,960 --> 00:02:46,140
 the routine, and you get, I guess, comfortable enough to

61
00:02:46,140 --> 00:02:47,000
 where you can

62
00:02:47,000 --> 00:02:51,520
 actually sit down and meditate for two hours at a time,

63
00:02:51,520 --> 00:02:52,400
 take a break, and then

64
00:02:52,400 --> 00:02:54,280
 keep doing that and keep doing that. And then it gets to

65
00:02:54,280 --> 00:02:55,040
 the point to where you're

66
00:02:55,040 --> 00:02:57,180
 just like, "Wow, I don't want to do anything else but med

67
00:02:57,180 --> 00:02:58,840
itate." But it's very

68
00:02:58,840 --> 00:03:04,780
 hard and it's really intensive and intense. If you ever

69
00:03:04,780 --> 00:03:06,370
 think during the first few days, I don't know what I got

70
00:03:06,370 --> 00:03:06,840
 myself in here.

71
00:03:06,840 --> 00:03:10,770
 Yeah, for sure. Maybe I should go home. Yeah, oh yeah, oh

72
00:03:10,770 --> 00:03:14,040
 yeah. Was there any period of time where you really weren't

73
00:03:14,040 --> 00:03:14,680
 sure if you were

74
00:03:14,680 --> 00:03:19,320
 able to make it and maybe I think I should go home better?

75
00:03:19,320 --> 00:03:19,800
 Yeah, oh yeah.

76
00:03:19,800 --> 00:03:22,170
 Packing up your stuff, kind of thing. Oh, for sure, oh for

77
00:03:22,170 --> 00:03:26,110
 sure. How do you deal with that? Well, one is you accept

78
00:03:26,110 --> 00:03:26,240
 that you

79
00:03:26,240 --> 00:03:29,040
 can't leave. There's no way you actually can. What does

80
00:03:29,040 --> 00:03:31,280
 that mean? Well, I mean, other than

81
00:03:31,280 --> 00:03:34,750
 catch a bus and go all the way back to the airport. I

82
00:03:34,750 --> 00:03:37,520
 thought you meant maybe, like what I often tell people is,

83
00:03:37,520 --> 00:03:41,120
 "You can leave this place, but you take everything with you

84
00:03:41,120 --> 00:03:41,240
."

85
00:03:41,240 --> 00:03:45,440
 Yeah, that's true too. But that's not, you just... Well,

86
00:03:45,440 --> 00:03:49,230
 that's the other thing too, is that you don't want to

87
00:03:49,230 --> 00:03:51,240
 finish it.

88
00:03:51,240 --> 00:03:54,180
 You know, it's just like... You know there's work to be

89
00:03:54,180 --> 00:03:56,810
 done. Exactly, and there's something on the other side of

90
00:03:56,810 --> 00:03:59,790
 this wall. If I could just get over this wall, no matter

91
00:03:59,790 --> 00:04:02,450
 how awful it is, because obviously it's working for a lot

92
00:04:02,450 --> 00:04:03,240
 of people.

93
00:04:03,240 --> 00:04:08,050
 You kind of have to be really determined and really into it

94
00:04:08,050 --> 00:04:13,150
, otherwise it's just not going to work. If you continue to

95
00:04:13,150 --> 00:04:17,000
 fight it and you just despair, like what am I doing? I just

96
00:04:17,000 --> 00:04:19,240
 wasted 600 bucks coming up here and stuff like that.

97
00:04:19,240 --> 00:04:24,580
 So one of the things that people, that we claim about

98
00:04:24,580 --> 00:04:30,550
 meditation is it helps you face and deal with your problems

99
00:04:30,550 --> 00:04:35,640
 or your mental issues. Were there any issues that you found

100
00:04:35,640 --> 00:04:42,420
 yourself confronting that maybe surprised you or that you

101
00:04:42,420 --> 00:04:44,240
 didn't realize about yourself?

102
00:04:44,240 --> 00:04:48,570
 Oh yeah, a lot of subtle little nuances, obviously with the

103
00:04:48,570 --> 00:04:52,750
 bigger problems, but what I really found so profound were

104
00:04:52,750 --> 00:04:57,080
 just the small little habits, little mental habits that you

105
00:04:57,080 --> 00:04:58,240
 would get.

106
00:04:58,240 --> 00:05:00,690
 You're not even aware that you're doing it and then you

107
00:05:00,690 --> 00:05:03,280
 really feel so stupid after it that you realize that this

108
00:05:03,280 --> 00:05:04,240
 is happening.

109
00:05:04,240 --> 00:05:07,600
 Wow, I mean it could just be, I don't even know how to

110
00:05:07,600 --> 00:05:11,410
 explain it, just small little tidbits of thought process

111
00:05:11,410 --> 00:05:15,400
 that don't really do anything for you, but end up going

112
00:05:15,400 --> 00:05:19,540
 into, it's like a branch is on a tree that is branched out

113
00:05:19,540 --> 00:05:23,240
 into this more crap that you have to deal with.

114
00:05:23,240 --> 00:05:28,240
 So I think that's where I found the most improvement.

115
00:05:28,240 --> 00:05:31,240
 Obviously the bigger problems help, but...

116
00:05:31,240 --> 00:05:37,050
 And did it surprise you? Did you, what you experienced in

117
00:05:37,050 --> 00:05:39,950
 the meditation, was it more or less what you expected

118
00:05:39,950 --> 00:05:43,260
 coming in or were there elements that you didn't expect?

119
00:05:43,260 --> 00:05:48,550
 Did it shock you? Did it change you? Was it a life changing

120
00:05:48,550 --> 00:05:50,240
 experience?

121
00:05:50,240 --> 00:05:53,930
 Well, life changing makes it sound huge and I mean it is

122
00:05:53,930 --> 00:05:57,930
 life changing, but not in the way I think, at least to me,

123
00:05:57,930 --> 00:06:00,680
 that I thought. I came in here expecting all, I didn't

124
00:06:00,680 --> 00:06:03,020
 really know what to expect actually, I just expected to

125
00:06:03,020 --> 00:06:05,660
 come out of here and be a happier, better person, which I

126
00:06:05,660 --> 00:06:08,240
 am, but not at all in the ways I would think.

127
00:06:08,240 --> 00:06:13,490
 It's very hard to describe until you actually do it, and it

128
00:06:13,490 --> 00:06:16,240
's really cool, it really is.

129
00:06:16,240 --> 00:06:19,700
 So tell us now about the cool part, what are some of the

130
00:06:19,700 --> 00:06:22,940
 benefits that, first of all, what are some of the

131
00:06:22,940 --> 00:06:26,820
 differences you see about yourself coming out of the course

132
00:06:26,820 --> 00:06:30,640
, as opposed to that weren't there or that were different

133
00:06:30,640 --> 00:06:32,240
 going into the course?

134
00:06:32,240 --> 00:06:38,640
 Well, for starters, I've learned to really, whenever you

135
00:06:38,640 --> 00:06:43,790
 feel any kind of strong emotion, you, at least I find

136
00:06:43,790 --> 00:06:47,970
 myself kind of distancing myself in a way. Like if I'm

137
00:06:47,970 --> 00:06:51,520
 feeling upset about anything, I don't really take it too

138
00:06:51,520 --> 00:06:55,240
 seriously, I don't really put too much effort into it.

139
00:06:55,240 --> 00:06:59,200
 Because if you sink into it and you let it consume you, it

140
00:06:59,200 --> 00:07:03,210
 just keeps going and keeps going into this big cycle that

141
00:07:03,210 --> 00:07:05,240
 you just can't get out of.

142
00:07:05,240 --> 00:07:08,250
 I don't ignore it, I just acknowledge it and I'm able to

143
00:07:08,250 --> 00:07:11,240
 deal with it a lot better, I don't let it consume me.

144
00:07:11,240 --> 00:07:14,840
 And then the other thing is too, I find myself just not

145
00:07:14,840 --> 00:07:19,240
 thinking about extraneous things, no matter what they are.

146
00:07:19,240 --> 00:07:26,080
 I don't really plan ahead, not that I don't plan ahead, but

147
00:07:26,080 --> 00:07:29,240
 not just huge in the future.

148
00:07:29,240 --> 00:07:33,240
 I don't find myself wishing for things or dreaming.

149
00:07:33,240 --> 00:07:38,540
 But this is different from before, before your practice,

150
00:07:38,540 --> 00:07:39,240
 for you?

151
00:07:39,240 --> 00:07:44,480
 Doing all that? Yeah, pretty much. Not like a super psych

152
00:07:44,480 --> 00:07:48,890
 ward way, but I guess the best way to say it is I wasn't

153
00:07:48,890 --> 00:07:53,240
 fully always aware, I wasn't always present.

154
00:07:53,240 --> 00:07:57,440
 I was always spending way too much time up here versus just

155
00:07:57,440 --> 00:08:01,240
 kind of actually seeing things for how they are.

156
00:08:01,240 --> 00:08:07,200
 I was making a lot of my problems in a way. That's where

157
00:08:07,200 --> 00:08:11,240
 you'll really find the most improvement, I think.

158
00:08:11,240 --> 00:08:16,410
 And through that you're much happier and much better, more

159
00:08:16,410 --> 00:08:21,240
 peaceful, more tranquil, I guess is a good term.

160
00:08:21,240 --> 00:08:24,480
 What can you say in general now, if you were to give people

161
00:08:24,480 --> 00:08:29,240
 advice or encouragement? Let's start with encouragement.

162
00:08:29,240 --> 00:08:33,760
 What would you say are the general benefits? You said some,

163
00:08:33,760 --> 00:08:35,240
 but maybe going a little bit more general.

164
00:08:35,240 --> 00:08:39,500
 For people, what would you say are the best reasons that

165
00:08:39,500 --> 00:08:43,940
 they would find to practice this, to go through the course

166
00:08:43,940 --> 00:08:46,240
 that you've just gone through?

167
00:08:46,240 --> 00:08:51,680
 That's easy. You really end up just understanding people

168
00:08:51,680 --> 00:08:55,240
 for what they do and why they are.

169
00:08:55,240 --> 00:08:57,590
 You get patience and you're able to deal with people no

170
00:08:57,590 --> 00:09:00,080
 matter how much they may piss you off or do something that

171
00:09:00,080 --> 00:09:01,240
 you don't think is right.

172
00:09:01,240 --> 00:09:05,630
 You just understand that they don't know. The benefits of

173
00:09:05,630 --> 00:09:10,680
 that is that you can actually teach them now why what they

174
00:09:10,680 --> 00:09:13,240
're doing is wrong and how that's wrong.

175
00:09:13,240 --> 00:09:15,330
 Through just awareness, it's not a matter of opinion or

176
00:09:15,330 --> 00:09:18,110
 anything like that. You just show them the way and they

177
00:09:18,110 --> 00:09:21,730
 teach themselves and you're able to do that now, which is

178
00:09:21,730 --> 00:09:22,240
 good.

179
00:09:22,240 --> 00:09:25,230
 I think it's healthy for people to be able to have patience

180
00:09:25,230 --> 00:09:28,760
 and understanding for people instead of just kind of, "He's

181
00:09:28,760 --> 00:09:30,240
 a bad person."

182
00:09:30,240 --> 00:09:34,860
 No, you don't understand them and they don't understand you

183
00:09:34,860 --> 00:09:36,240
 kind of thing.

184
00:09:49,240 --> 00:09:52,240
 Okay, well, thank you. You're welcome. Thank you.

