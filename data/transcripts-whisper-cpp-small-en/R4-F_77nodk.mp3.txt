 There's this person who really annoys or even anchors me
 sometimes.
 What bothers me about this is to know that it's not him,
 but I, who is causing myself to suffer.
 But why can't I let it go? Why am I attached to suffering?
 It's the question, right?
 That's the curious truth.
 It's a profound question. It's not something that normally
 people ask, "Why do I suffer?"
 And they say, "Well, you're making me suffer." And that's
 the problem.
 The curious thing about suffering is there are different
 levels to our understanding of it,
 which I've talked about before. I can go into it here.
 I think talking about suffering will probably give you some
 background,
 better background to answer this question.
 I think briefly I can say, before I explain the means,
 the simple answer to your question is because that's not
 the practice.
 The practice is not to let go of suffering.
 The practice is to observe reality.
 The result of observing reality closely, objectively,
 clearly,
 is to understand reality for what it is.
 The result of understanding reality for what it is is
 letting go.
 So it's a progress.
 Most people understand suffering to be a feeling that you
 get from time to time.
 And so they try to find a way to avoid the feeling.
 The understanding of suffering is that it's specific.
 This or that experience causes suffering.
 And so their means of escaping suffering is to run away
 from the experience,
 to chase it away, to blame other people, blame external
 events
 and circumstances and entities outside of themselves,
 or of course in their own bodies, even in their own minds.
 Blame those things and try their best to avoid them.
 This is how ordinary people deal with suffering.
 You have a headache, take a pill, you have a backache, get
 a massage,
 you have a stomachache, take some adhesin.
 Eventually, a spiritual person, a person with a little bit
 of wisdom at least,
 will see that this isn't effective, this isn't a viable
 solution to the problem.
 Eventually there are certain sufferings that you can't run
 away from,
 you can't avoid, you can't be free from simply by chasing
 away.
 Old age, sickness, death, even extreme trauma,
 loss of a loved one, loss of wealth, loss of health,
 loss or sadness or any kind of great suffering,
 will make a person realize that this isn't a viable
 solution,
 that these states of suffering you can't avoid.
 So there has to be a better way to be able to actually deal
 with suffering.
 This is what often leads people to the spiritual life
 and for us this is what leads us to come and meditate.
 So this leads to the third understanding of suffering
 as a quality inherent in reality in the same way that fire
 is hot.
 Experience is a cause for suffering.
 We don't really have a word, I can't think of a word for it
,
 but it's like static electricity or potential energy.
 There's no energy in it but it has the potential.
 So a rock that's up high doesn't seem to have any energy,
 it's not moving but it has potential energy because if you
 push it,
 it exhibits extreme amounts of energy as it falls to the
 earth below.
 So reality has this kind of static electricity
 or this potential for causing suffering.
 So we say fire is hot but it's not burning you.
 So when we say that experience is suffering,
 we don't mean that it hurts you,
 it has the potential to hurt you.
 And that which causes that suffering is craving,
 so the cause of all of our suffering is craving.
 So the third is this realization, this observation that
 leads you to see
 that the reason that you suffer is not really because you
 experience this or that,
 but it's because, well the third stage is it's because
 nothing can satisfy you.
 It's because, or it's the realization that all of reality
 is contributing to your suffering because you're partial to
 certain things
 and partial against certain things.
 It's the realization that that's not correct,
 that's due simply to ignorance.
 And it's the realization that all, really all experience is
 the same,
 that pain is really the same as pleasure,
 good memories are the same as bad memories, they're still
 just memories.
 It's really only our judgment and our incorrect judgment
 that points to certain things as being stable, as being
 pleasant, as being controllable
 and therefore leading to expectations and ultimately
 disappointment
 when we don't get what we want.
 That's the third which leads ultimately to a realization
 that,
 or the truth of suffering, and that's what leads you to let
 go.
 So the third and the fourth are kind of like one and the
 same,
 it's the path and then the goal.
 When you finally get it, the fourth one is this moment,
 this sort of gestalt I guess, or this epiphany that you
 have
 when you realize that nothing is worth clinging to,
 when you realize that everything is like this.
 The third is the practice of observing and investigating
 and seeing that the things that you thought were pleasant
 and the cause for happiness and contentment
 are actually making you more discontent, more unhappy, more
 unpleasant,
 more bitter and mean as a person.
 And the fourth one is this final realization
 that absolutely everything is not worth clinging to.
 And so that's the moment where the mind lets go
 and that's the realization of the freedom,
 the freedom from suffering, which we call nirvana.
 So why you can't let go, I would say, if I was going to be
 kind of
 smarmy about it, I would say because you haven't done the
 work,
 you haven't done what is required to let go.
 Letting go is not something you do.
 In fact, the irony there is that you're talking about
 forcing yourself to let go, which are opposites.
 Letting go means letting come, actually.
 Letting go means really stopping to force things.
 Once you can do that, you'll be free from suffering.
 So the question is really a paradox,
 or something like a paradox where you're asking,
 "Why can't I force myself to let go?"
 When you say, "Why can't I let go?"
 You don't let go. The letting go occurs because you see
 things clearly.
 There's just no clinging because you would have no reason
 to cling.
 The question as to why we're attached to suffering is more
 interesting.
 And this is what's curious about this.
 Why the heck are we attached to that which is suffering?
 And this is the key to the essence of Buddhism, is that it
's ignorance.
 We aren't always attached to suffering.
 It's more like we are being tossed about in the ocean.
 And so sometimes we go this way, sometimes we go that way,
 sometimes we can be really good.
 Sometimes people are just naturally good.
 They're going in that direction.
 Sometimes people are naturally horrible, evil, mean
 creatures.
 We go up and down, back and forth, but randomly.
 But it's all based on ignorance.
 Once you know, then you become steered in a specific
 direction.
 And that's towards peace, happiness and freedom from
 suffering.
 So knowledge is something that directs the mind,
 just like the captain directs the ship.
 Without a captain, without mindfulness, the boat just is
 tossed to the waves,
 tossed by the waves to the current.
 So, it's just, you could say kind of by chance,
 it's where you find yourself, or it's one of the currents
 is attachment to suffering.
 It's not ignorance.
