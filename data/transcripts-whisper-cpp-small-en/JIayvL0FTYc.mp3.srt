1
00:00:00,000 --> 00:00:05,000
 Is the mind different from the physical brain?

2
00:00:05,000 --> 00:00:08,000
 Totally different.

3
00:00:08,000 --> 00:00:11,000
 But let's approach this a little bit differently.

4
00:00:11,000 --> 00:00:13,000
 Let's be a little bit more specific here.

5
00:00:13,000 --> 00:00:17,000
 What do we mean by the mind and what do we mean by the

6
00:00:17,000 --> 00:00:18,000
 brain?

7
00:00:18,000 --> 00:00:23,330
 Because if you relate back to what I was just saying,

8
00:00:23,330 --> 00:00:26,000
 neither one exists.

9
00:00:26,000 --> 00:00:30,000
 Because neither one has anything to do with experience.

10
00:00:30,000 --> 00:00:36,000
 The mind is just a concept and the brain is just a concept.

11
00:00:36,000 --> 00:00:39,000
 Reality is experience.

12
00:00:39,000 --> 00:00:42,000
 Something that we have to... It's very difficult.

13
00:00:42,000 --> 00:00:46,000
 It's near impossible, I would say, because of our training

14
00:00:46,000 --> 00:00:49,000
 in modern society.

15
00:00:49,000 --> 00:00:56,730
 We're so brainwashed. It's not like there's anyone brain

16
00:00:56,730 --> 00:00:57,000
washing us,

17
00:00:57,000 --> 00:01:01,000
 but we've been so indoctrinated. This may be a good word.

18
00:01:01,000 --> 00:01:05,260
 We've caught ourselves up so much in this idea of three-

19
00:01:05,260 --> 00:01:06,000
dimensional space,

20
00:01:06,000 --> 00:01:12,000
 of external reality, and so on.

21
00:01:12,000 --> 00:01:15,740
 The funny thing is to read in quantum physics how none of

22
00:01:15,740 --> 00:01:17,000
 it's really valid,

23
00:01:17,000 --> 00:01:20,690
 how our belief in the existence of a three-dimensional

24
00:01:20,690 --> 00:01:26,000
 universe is really quite invalid.

25
00:01:26,000 --> 00:01:33,310
 All we can say about reality is based on experiment, based

26
00:01:33,310 --> 00:01:37,000
 on observations,

27
00:01:37,000 --> 00:01:41,000
 from Buddha's point of view, based on experience.

28
00:01:41,000 --> 00:01:44,000
 I think that should answer your question.

29
00:01:44,000 --> 00:01:50,200
 I really have to spell it out because then what part of

30
00:01:50,200 --> 00:01:54,000
 those two things is in reality?

31
00:01:54,000 --> 00:01:57,340
 If you see a brain, if you cut someone's head open and you

32
00:01:57,340 --> 00:02:00,000
 see a brain, that's seeing.

33
00:02:00,000 --> 00:02:07,000
 If you touch the brain, that's feeling.

34
00:02:07,000 --> 00:02:12,000
 You can never experience the brain, which means the brain

35
00:02:12,000 --> 00:02:13,000
 is not,

36
00:02:13,000 --> 00:02:21,900
 in any sense, the word that I would understand it to be

37
00:02:21,900 --> 00:02:23,000
 real.

38
00:02:23,000 --> 00:02:31,220
 Now, the deal with the mind may be a little more difficult

39
00:02:31,220 --> 00:02:34,000
 to understand.

40
00:02:34,000 --> 00:02:38,270
 But when we think of reality in terms of, well, you can't

41
00:02:38,270 --> 00:02:39,000
 even say it like that.

42
00:02:39,000 --> 00:02:42,720
 You really have to practice meditation to understand this

43
00:02:42,720 --> 00:02:43,000
 one,

44
00:02:43,000 --> 00:02:45,680
 because only through the practice of meditation will you be

45
00:02:45,680 --> 00:02:48,000
 able to break up this continuity

46
00:02:48,000 --> 00:02:52,000
 which creates the idea of a self, of a soul, of a mind.

47
00:02:52,000 --> 00:02:56,000
 When you're able to break it up, you can see that actually

48
00:02:56,000 --> 00:03:01,000
 one mind has often nothing to do with the next mind.

49
00:03:01,000 --> 00:03:04,260
 When you lost your train of thought, I lost my train of

50
00:03:04,260 --> 00:03:05,000
 thought.

51
00:03:05,000 --> 00:03:10,000
 The mind is like getting steamrolled by another mind,

52
00:03:10,000 --> 00:03:12,540
 because suddenly the mind is thinking about something

53
00:03:12,540 --> 00:03:13,000
 different,

54
00:03:13,000 --> 00:03:15,000
 and you've lost that old one.

55
00:03:15,000 --> 00:03:21,000
 If there were a mind, you'd think there would be a way to,

56
00:03:21,000 --> 00:03:24,000
 or that other thought would still be there or so on.

57
00:03:24,000 --> 00:03:27,000
 This sort of thing wouldn't happen.

58
00:03:27,000 --> 00:03:31,490
 But when you practice meditation, you will see the mind

59
00:03:31,490 --> 00:03:32,000
 arising,

60
00:03:32,000 --> 00:03:35,000
 and you will see thinking about one thing, suddenly

61
00:03:35,000 --> 00:03:35,000
 thinking about another thing,

62
00:03:35,000 --> 00:03:41,000
 totally unrelated, and not continuing one from the other.

63
00:03:41,000 --> 00:03:43,000
 So you don't know where it arose from.

64
00:03:43,000 --> 00:03:48,000
 The best you can guess is that it arose from nowhere.

65
00:03:48,000 --> 00:03:51,000
 Where did this mind, this thought come from?

66
00:03:51,000 --> 00:03:53,000
 It seems to have appeared out of nothing,

67
00:03:53,000 --> 00:03:57,560
 and that's really the most accurate description you can

68
00:03:57,560 --> 00:03:58,000
 give,

69
00:03:58,000 --> 00:04:01,000
 is that these things arise based on nothing.

70
00:04:01,000 --> 00:04:05,000
 So the idea of a mind is a red herring.

71
00:04:05,000 --> 00:04:07,000
 It's a fallacy.

72
00:04:07,000 --> 00:04:11,300
 The truth is an experience which has a mental portion and

73
00:04:11,300 --> 00:04:13,000
 has a physical portion.

74
00:04:13,000 --> 00:04:16,000
 Not always a physical, because it can be thoughts,

75
00:04:16,000 --> 00:04:22,410
 but it's an experience that incorporates something that

76
00:04:22,410 --> 00:04:24,000
 could be called mental

77
00:04:24,000 --> 00:04:27,220
 and often incorporates something that could be called

78
00:04:27,220 --> 00:04:28,000
 physical.

79
00:04:28,000 --> 00:04:31,000
 That's all we can really say.

80
00:04:31,000 --> 00:04:50,000
 The idea of the brain and so on is just a concept.

81
00:04:50,000 --> 00:04:52,000
 Nothing to add to you.

