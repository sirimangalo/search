1
00:00:00,000 --> 00:00:05,000
 So in this video I will be talking about a third technique

2
00:00:05,000 --> 00:00:10,140
 of meditation which is generally used as a warm-up practice

3
00:00:10,140 --> 00:00:13,000
 before doing the walking and the sitting meditation.

4
00:00:13,000 --> 00:00:17,000
 This technique is called mindful prostration.

5
00:00:17,000 --> 00:00:20,350
 Now prostration is something which people of various

6
00:00:20,350 --> 00:00:24,000
 religious traditions are familiar with around the world.

7
00:00:25,000 --> 00:00:28,330
 For instance, in Thailand it is used as a means to pay

8
00:00:28,330 --> 00:00:32,710
 respect to one's parents or one's teachers or figures of

9
00:00:32,710 --> 00:00:36,000
 religious reverence.

10
00:00:36,000 --> 00:00:40,110
 In other religious traditions it might be used as a form of

11
00:00:40,110 --> 00:00:46,250
 worship for a divinity, a god or an angel or an object of

12
00:00:46,250 --> 00:00:48,000
 worship.

13
00:00:49,000 --> 00:00:53,630
 In the meditation practice we are using the prostration as

14
00:00:53,630 --> 00:00:58,410
 a means of paying respect to the actual meditation practice

15
00:00:58,410 --> 00:00:59,000
 itself.

16
00:00:59,000 --> 00:01:03,650
 So what we mean by mindful prostration is besides simply

17
00:01:03,650 --> 00:01:08,870
 being a warm-up it is also a way of creating a humble and a

18
00:01:08,870 --> 00:01:14,000
 sincere attitude towards the meditation practice.

19
00:01:15,000 --> 00:01:18,530
 We have a feeling that what we are doing is not something

20
00:01:18,530 --> 00:01:21,730
 as a hobby or just for fun, it is something that we

21
00:01:21,730 --> 00:01:25,720
 seriously hope to incorporate into our lives and to make a

22
00:01:25,720 --> 00:01:27,000
 part of who we are.

23
00:01:27,000 --> 00:01:30,640
 What we are practicing here is something of great

24
00:01:30,640 --> 00:01:35,030
 significance, of great importance and something worthy of

25
00:01:35,030 --> 00:01:36,000
 our respect.

26
00:01:37,000 --> 00:01:41,500
 So this doesn't mean that we are worshipping or we are b

27
00:01:41,500 --> 00:01:46,350
owing down to an individual or an entity of any sort, it is

28
00:01:46,350 --> 00:01:49,870
 a way of paying respect to the practice which we are about

29
00:01:49,870 --> 00:01:51,000
 to undertake.

30
00:01:51,000 --> 00:01:56,000
 It also can be thought of as simply a warm-up exercise.

31
00:01:56,000 --> 00:01:59,140
 So instead of doing a simple prostration we are going to

32
00:01:59,140 --> 00:02:01,780
 actually be focusing on the movements of the body as we do

33
00:02:01,780 --> 00:02:03,000
 the prostration.

34
00:02:04,000 --> 00:02:07,820
 But in order to get a sense of what is meant by prostration

35
00:02:07,820 --> 00:02:12,160
 I am going to give as an example the method of prostration

36
00:02:12,160 --> 00:02:15,870
 used by the Thai people when they would normally pay

37
00:02:15,870 --> 00:02:19,000
 respect to someone higher than themselves.

38
00:02:19,000 --> 00:02:24,840
 This is because that particular prostration method will

39
00:02:24,840 --> 00:02:30,450
 then be used as a model on which we can then do our mindful

40
00:02:30,450 --> 00:02:32,000
 prostration.

41
00:02:33,000 --> 00:02:35,760
 In order that you get an understanding of what the prost

42
00:02:35,760 --> 00:02:40,000
ration should look like I will first perform for you to see

43
00:02:40,000 --> 00:02:43,000
 a traditional Thai prostration.

44
00:02:43,000 --> 00:02:47,140
 And so the prostration is typically three prostrations,

45
00:02:47,140 --> 00:02:49,000
 three bows of respect.

46
00:02:49,000 --> 00:02:52,000
 It is performed on one's knees.

47
00:02:58,000 --> 00:03:02,000
 Traditionally with the person sitting on their toes.

48
00:03:02,000 --> 00:03:06,560
 But if this is uncomfortable you can also sit down on the

49
00:03:06,560 --> 00:03:08,000
 tops of your feet.

50
00:03:08,000 --> 00:03:16,000
 You can do as is most comfortable for you.

51
00:03:16,000 --> 00:03:22,720
 To start the hands are placed on the thighs, the back is

52
00:03:22,720 --> 00:03:24,000
 straight, the eyes are open.

53
00:03:26,000 --> 00:03:30,880
 And one begins by bringing the hands up to the chest, then

54
00:03:30,880 --> 00:03:37,720
 touching at the eyebrows and then out in a W shape with the

55
00:03:37,720 --> 00:03:44,000
 thumbs touching down to touch the floor in front.

56
00:03:47,000 --> 00:03:51,060
 The elbows will touch in front of the knees and the head

57
00:03:51,060 --> 00:03:56,000
 goes down to touch the thumbs between the eyebrows.

58
00:03:56,000 --> 00:04:06,510
 Bringing the head up again and then the hands up to touch

59
00:04:06,510 --> 00:04:10,000
 together in front of the chest.

60
00:04:11,000 --> 00:04:14,840
 This is one prostration, then one goes on to do the second

61
00:04:14,840 --> 00:04:26,000
 prostration up to the forehead and out again and back up.

62
00:04:26,000 --> 00:04:30,000
 And a third time.

63
00:04:39,000 --> 00:04:42,080
 And after the third prostration we then bring the hands up

64
00:04:42,080 --> 00:04:43,000
 to the forehead.

65
00:04:43,000 --> 00:04:46,600
 But instead of out again we bring the hands simply back

66
00:04:46,600 --> 00:04:48,000
 down to the chest.

67
00:04:48,000 --> 00:04:52,000
 And this is a traditional Thai prostration.

68
00:04:52,000 --> 00:04:55,020
 This is going to be the model for what we call the mindful

69
00:04:55,020 --> 00:04:56,000
 prostration.

70
00:04:56,000 --> 00:04:59,300
 So it's a means of using this bodily movement as a

71
00:04:59,300 --> 00:05:03,000
 meditation technique or as I said a means of paying respect

72
00:05:03,000 --> 00:05:07,180
 and building up states of humility and of respect of

73
00:05:07,180 --> 00:05:08,000
 sincerity.

74
00:05:08,000 --> 00:05:10,000
 In regards to the meditation practice.

75
00:05:10,000 --> 00:05:13,220
 So we begin in the same way by putting the hands on the

76
00:05:13,220 --> 00:05:16,660
 thighs and we start with the right hand one hand at a time

77
00:05:16,660 --> 00:05:20,200
 because here we're going to be watching the movements of

78
00:05:20,200 --> 00:05:23,840
 the hand again we're focusing on the body as our primary

79
00:05:23,840 --> 00:05:25,000
 meditation object.

80
00:05:26,000 --> 00:05:30,780
 As the hand turns we say to ourselves turning and we say it

81
00:05:30,780 --> 00:05:35,950
 three times turning turning turning again keeping the mind

82
00:05:35,950 --> 00:05:41,530
 keeping the noting the mantra the clear thought in the

83
00:05:41,530 --> 00:05:42,000
 present moment.

84
00:05:42,000 --> 00:05:45,710
 So as the hand begins to turn we say turning when it's in

85
00:05:45,710 --> 00:05:49,590
 the middle we say turning again and when it finishes

86
00:05:49,590 --> 00:05:53,020
 turning we say it three times saying to ourselves again

87
00:05:53,020 --> 00:05:55,000
 turning turning turning.

88
00:05:55,000 --> 00:05:58,800
 When the hand raises we say to ourselves raising raising

89
00:05:58,800 --> 00:06:02,380
 raising and when it touches the chest we say touching

90
00:06:02,380 --> 00:06:04,000
 touching touching.

91
00:06:04,000 --> 00:06:10,370
 Then the same with the left hand turning turning turning

92
00:06:10,370 --> 00:06:15,000
 raising raising raising touching touching touching.

93
00:06:16,000 --> 00:06:21,050
 Then up to the forehead raising raising raising touching

94
00:06:21,050 --> 00:06:25,730
 touching touching and back down to the chest lowering

95
00:06:25,730 --> 00:06:30,000
 lowering lowering touching touching touching.

96
00:06:30,000 --> 00:06:33,600
 So instead of going out we've brought the hands back down

97
00:06:33,600 --> 00:06:37,220
 to the chest again then we do the actual prostration first

98
00:06:37,220 --> 00:06:41,000
 we bend the back bending bending bending and then again put

99
00:06:41,000 --> 00:06:44,000
 the hand bring the hands out but this time one at a time.

100
00:06:45,000 --> 00:06:52,980
 Lowering lowering lowering touching touching touching

101
00:06:52,980 --> 00:07:00,390
 lowering covering covering covering lowering lowering

102
00:07:00,390 --> 00:07:06,000
 touching touching covering covering.

103
00:07:07,000 --> 00:07:12,250
 Bending bending bending touching touching touching raising

104
00:07:12,250 --> 00:07:17,280
 raising raising turning turning and here it starts over

105
00:07:17,280 --> 00:07:22,180
 again so we can we start with the turning three times this

106
00:07:22,180 --> 00:07:26,400
 time from the floor and then the hand up raising raising

107
00:07:26,400 --> 00:07:29,000
 raising and so on touching touching touching.

108
00:07:30,000 --> 00:07:36,640
 Turning turning turning raising raising raising touching

109
00:07:36,640 --> 00:07:44,380
 touching touching raising raising raising touching touching

110
00:07:44,380 --> 00:07:50,320
 lowering lowering lowering touching touching bending

111
00:07:50,320 --> 00:07:57,000
 bending bending lowering lowering lowering touching

112
00:07:57,000 --> 00:07:58,000
 touching touching.

113
00:07:59,000 --> 00:08:01,640
 Covering covering covering lowering lowering lowering

114
00:08:01,640 --> 00:08:04,230
 touching touching touching covering covering bending

115
00:08:04,230 --> 00:08:10,270
 bending bending touching touching touching raising raising

116
00:08:10,270 --> 00:08:20,000
 raising and then a third time as well exactly the same way.

117
00:08:21,000 --> 00:08:31,000
 And again each one step at a time bending and so on.

118
00:08:35,000 --> 00:08:38,640
 And after the third prostration we come up raising raising

119
00:08:38,640 --> 00:08:42,160
 raising touching touching touching and up again to the

120
00:08:42,160 --> 00:08:45,340
 after the third time up again to the forehead raising

121
00:08:45,340 --> 00:08:49,100
 raising raising touching touching touching lowering

122
00:08:49,100 --> 00:08:53,370
 lowering lowering touching touching touching and instead of

123
00:08:53,370 --> 00:08:55,000
 bending again to do a fourth prostration.

124
00:08:56,000 --> 00:09:01,310
 Instead bring the hands down to rest on the thighs lowering

125
00:09:01,310 --> 00:09:05,620
 lowering lowering touching touching touching covering

126
00:09:05,620 --> 00:09:10,360
 covering covering lowering lowering touching touching

127
00:09:10,360 --> 00:09:14,000
 touching covering covering covering.

128
00:09:14,000 --> 00:09:16,980
 Once we finish that we continue on with the walking

129
00:09:16,980 --> 00:09:20,000
 meditation and then with the sitting meditation.

130
00:09:21,000 --> 00:09:23,990
 And as you do this you don't just get up to do the walking

131
00:09:23,990 --> 00:09:27,280
 meditation you continue on with the mindfulness as you go

132
00:09:27,280 --> 00:09:30,630
 to stand up first you say to yourself sitting sitting

133
00:09:30,630 --> 00:09:33,260
 sitting and then standing standing and you stand up and

134
00:09:33,260 --> 00:09:37,050
 then slowly go to do the walking meditation making sure

135
00:09:37,050 --> 00:09:40,000
 that your mindfulness your awareness your clear awareness

136
00:09:40,000 --> 00:09:42,000
 of the present moment is continuous.

137
00:09:43,000 --> 00:09:46,100
 So that's all for today this is the practice of what we

138
00:09:46,100 --> 00:09:49,330
 call mindful prostration. So I hope again that this

139
00:09:49,330 --> 00:09:52,470
 practice brings you both peace happiness and clarity of

140
00:09:52,470 --> 00:09:54,000
 mind. Thank you.

