1
00:00:00,000 --> 00:00:07,040
 Hi.

2
00:00:07,040 --> 00:00:09,190
 In this video, I'll be talking about some of the ways in

3
00:00:09,190 --> 00:00:10,400
 which we can incorporate the

4
00:00:10,400 --> 00:00:13,900
 meditation practice into our daily life.

5
00:00:13,900 --> 00:00:16,920
 First of all, the first important thing to explain in terms

6
00:00:16,920 --> 00:00:18,600
 of incorporating the meditation

7
00:00:18,600 --> 00:00:21,900
 practice into our daily life are those things which it's

8
00:00:21,900 --> 00:00:24,040
 going to be important for us to

9
00:00:24,040 --> 00:00:27,550
 abstain from if our meditation practice is to be effective

10
00:00:27,550 --> 00:00:29,220
 and is to have good results

11
00:00:29,220 --> 00:00:31,480
 in our daily life.

12
00:00:31,480 --> 00:00:34,410
 As we mentioned in the beginning, meditation is an

13
00:00:34,410 --> 00:00:36,120
 equivalent to medication.

14
00:00:36,120 --> 00:00:38,520
 And as we know with medication, there are always certain

15
00:00:38,520 --> 00:00:39,680
 things which you can't take

16
00:00:39,680 --> 00:00:42,880
 in conjunction with the medication.

17
00:00:42,880 --> 00:00:47,960
 Some certain things which when taken together with the

18
00:00:47,960 --> 00:00:50,320
 medication will cause the medication

19
00:00:50,320 --> 00:00:54,380
 to be ineffective or will be counter indicative to the

20
00:00:54,380 --> 00:00:56,760
 results which we're hoping to get from

21
00:00:56,760 --> 00:00:58,320
 the medication.

22
00:00:58,320 --> 00:01:00,360
 So certain things when we take medication, there's always

23
00:01:00,360 --> 00:01:01,440
 certain things we have to take

24
00:01:01,440 --> 00:01:02,440
 out of our diet.

25
00:01:02,440 --> 00:01:06,000
 But with meditation, it's very similar.

26
00:01:06,000 --> 00:01:08,670
 Meditation is something which is to bring about a certain

27
00:01:08,670 --> 00:01:10,040
 state of clarity or a natural

28
00:01:10,040 --> 00:01:15,550
 state of purity in the mind to bring our minds back to this

29
00:01:15,550 --> 00:01:18,600
 state of clarity and sobriety

30
00:01:18,600 --> 00:01:21,240
 which is free from suffering.

31
00:01:21,240 --> 00:01:23,880
 So then there are certain actions, certain things which we

32
00:01:23,880 --> 00:01:25,040
 can do by way of body or by

33
00:01:25,040 --> 00:01:28,200
 way of speech which are going to be counter indicative,

34
00:01:28,200 --> 00:01:30,320
 which are going to have the opposite

35
00:01:30,320 --> 00:01:33,450
 effect, which are going to create a more clouded state of

36
00:01:33,450 --> 00:01:35,560
 mind, which are going to bring about

37
00:01:35,560 --> 00:01:40,140
 high states of anger, high states of addiction, high states

38
00:01:40,140 --> 00:01:43,160
 of delusion, high states of clouded

39
00:01:43,160 --> 00:01:44,880
 mind states.

40
00:01:44,880 --> 00:01:47,700
 So these things we're going to have to take out of our diet

41
00:01:47,700 --> 00:01:49,480
, so to speak, if our meditation

42
00:01:49,480 --> 00:01:51,280
 is to be effective.

43
00:01:51,280 --> 00:01:54,440
 The first thing that we have to take out is not to kill.

44
00:01:54,440 --> 00:01:58,010
 We have to make a promise to ourselves that we're not going

45
00:01:58,010 --> 00:01:59,800
 to kill living beings, not

46
00:01:59,800 --> 00:02:05,790
 to kill living ants or mosquitoes or any sort of living

47
00:02:05,790 --> 00:02:09,440
 being which is, which breeds and

48
00:02:09,440 --> 00:02:13,480
 which is in and of itself, doesn't wish to die.

49
00:02:13,480 --> 00:02:16,000
 Number two, not to steal.

50
00:02:16,000 --> 00:02:19,540
 So if our meditation is to be effective, we have to be able

51
00:02:19,540 --> 00:02:21,140
 to respect the possessions

52
00:02:21,140 --> 00:02:24,920
 of other people and not to take things without permission.

53
00:02:24,920 --> 00:02:27,990
 Number three, not to commit adultery or sexual misconduct

54
00:02:27,990 --> 00:02:30,280
 to get involved in romantic relationships

55
00:02:30,280 --> 00:02:35,330
 or sexual conduct, which is emotionally or spiritually

56
00:02:35,330 --> 00:02:37,900
 damaging to other people.

57
00:02:37,900 --> 00:02:42,020
 Number four, not to tell lies, not to deceive other people

58
00:02:42,020 --> 00:02:45,720
 or take them away from their reality.

59
00:02:45,720 --> 00:02:49,160
 And number five, not to take drugs or alcohol.

60
00:02:49,160 --> 00:02:51,720
 These are not to take anything really which is going to be

61
00:02:51,720 --> 00:02:53,800
 intoxicating for our minds,

62
00:02:53,800 --> 00:02:57,150
 things which create states of insubriety, take us away from

63
00:02:57,150 --> 00:02:58,740
 our natural, pure, clear

64
00:02:58,740 --> 00:03:00,340
 state of mind.

65
00:03:00,340 --> 00:03:04,450
 These things are, it's very important that we actually abst

66
00:03:04,450 --> 00:03:06,400
ain from them completely if

67
00:03:06,400 --> 00:03:10,000
 our meditation practice is to be successful.

68
00:03:10,000 --> 00:03:13,650
 There are certain other things which can be undertaken, can

69
00:03:13,650 --> 00:03:15,400
 be partaken of, but must be

70
00:03:15,400 --> 00:03:18,510
 partaken of in moderation if our meditation is to be

71
00:03:18,510 --> 00:03:19,560
 successful.

72
00:03:19,560 --> 00:03:22,630
 These things are not unwholesome, but when taken in

73
00:03:22,630 --> 00:03:24,600
 conjunction with the meditation,

74
00:03:24,600 --> 00:03:29,140
 they reduce the effect, they take away from the clarity of

75
00:03:29,140 --> 00:03:31,120
 the mind when taken out of

76
00:03:31,120 --> 00:03:33,260
 moderation.

77
00:03:33,260 --> 00:03:37,200
 These are things like, first of all, eating.

78
00:03:37,200 --> 00:03:38,940
 We have to be careful not to eat too much.

79
00:03:38,940 --> 00:03:40,990
 If we're always obsessed with food, this is one major

80
00:03:40,990 --> 00:03:41,640
 addiction.

81
00:03:41,640 --> 00:03:45,210
 It's something which creates sloth, which creates an

82
00:03:45,210 --> 00:03:47,600
 unhealthy bodily state as well.

83
00:03:47,600 --> 00:03:50,360
 It's something which is overall not conducive for

84
00:03:50,360 --> 00:03:51,720
 meditation practice.

85
00:03:51,720 --> 00:03:53,420
 This is overeating.

86
00:03:53,420 --> 00:03:58,220
 We have to eat to stay alive, but we shouldn't live simply

87
00:03:58,220 --> 00:03:59,080
 to eat.

88
00:03:59,080 --> 00:04:03,110
 Another one is entertainment, watching television, watching

89
00:04:03,110 --> 00:04:05,480
 movies, listening to music, and so

90
00:04:05,480 --> 00:04:06,480
 on.

91
00:04:06,480 --> 00:04:10,190
 They're unwholesome things, but when taken in excess, they

92
00:04:10,190 --> 00:04:11,640
 create states of addiction

93
00:04:11,640 --> 00:04:15,040
 and states of insubriety actually in the mind.

94
00:04:15,040 --> 00:04:18,000
 They take the mind out of its natural clarity.

95
00:04:18,000 --> 00:04:21,390
 Using the internet, for instance, should be done in

96
00:04:21,390 --> 00:04:22,280
 moderation.

97
00:04:22,280 --> 00:04:27,920
 The third one is that as far as sleeping.

98
00:04:27,920 --> 00:04:30,480
 Sleeping can be one addiction that we often overlook, and

99
00:04:30,480 --> 00:04:31,920
 we don't realize that actually

100
00:04:31,920 --> 00:04:34,200
 we enjoy sleeping so much.

101
00:04:34,200 --> 00:04:38,040
 Whatever problems arise, we always find ourselves going to

102
00:04:38,040 --> 00:04:40,360
 take a nap or trying to sleep more.

103
00:04:40,360 --> 00:04:43,850
 Some people actually become insomniac because they are so

104
00:04:43,850 --> 00:04:45,800
 obsessed about their sleep.

105
00:04:45,800 --> 00:04:48,290
 We find that through the meditation practice, actually, we

106
00:04:48,290 --> 00:04:49,440
 need very little sleep.

107
00:04:49,440 --> 00:04:52,000
 A lack of sleep is not a real problem because our minds are

108
00:04:52,000 --> 00:04:53,240
 so calm and so pure all the

109
00:04:53,240 --> 00:04:54,880
 time.

110
00:04:54,880 --> 00:04:57,720
 These are things which have to be taken in moderation.

111
00:04:57,720 --> 00:05:00,620
 Altogether these are things which we have to start to take

112
00:05:00,620 --> 00:05:02,400
 out of our lives if the meditation

113
00:05:02,400 --> 00:05:05,680
 is to be a successful part of our daily life.

114
00:05:05,680 --> 00:05:08,700
 Now onto the question, how do we then incorporate the

115
00:05:08,700 --> 00:05:11,280
 meditation directly into our daily life?

116
00:05:11,280 --> 00:05:13,280
 This we do in two ways.

117
00:05:13,280 --> 00:05:15,280
 First of all, we focus on the body.

118
00:05:15,280 --> 00:05:19,730
 The body in general, it's in one of the four major postures

119
00:05:19,730 --> 00:05:20,920
 at all times.

120
00:05:20,920 --> 00:05:25,550
 Either we're sitting or we're standing or we're walking or

121
00:05:25,550 --> 00:05:27,200
 we're lying down.

122
00:05:27,200 --> 00:05:32,160
 We can use these four postures as a very clear and very

123
00:05:32,160 --> 00:05:35,200
 gross base for us to always find

124
00:05:35,200 --> 00:05:39,400
 something to be mindful of, to create a clear thought.

125
00:05:39,400 --> 00:05:41,620
 When we're walking, instead of simply walking and letting

126
00:05:41,620 --> 00:05:42,840
 our minds wander, we can say to

127
00:05:42,840 --> 00:05:46,680
 ourselves, "Walking, walking, walking, walking."

128
00:05:46,680 --> 00:05:49,240
 When we're standing, we can say, "Standing, standing."

129
00:05:49,240 --> 00:05:51,400
 When we sit, we can say, "Sitting, sitting."

130
00:05:51,400 --> 00:05:54,880
 When we lie down, "Lying, lying, lying."

131
00:05:54,880 --> 00:05:56,760
 This is when we're not actually doing the meditation.

132
00:05:56,760 --> 00:06:00,200
 We can undertake this practice at all times.

133
00:06:00,200 --> 00:06:03,560
 Similarly, with minor movements of the body, when we bend,

134
00:06:03,560 --> 00:06:05,240
 we can say, "Bending," when

135
00:06:05,240 --> 00:06:07,640
 we stretch, "stretching," when we move the hand, "moving,"

136
00:06:07,640 --> 00:06:09,240
 when we brush our teeth, "brushing,"

137
00:06:09,240 --> 00:06:11,830
 when we eat food, "chewing," "chewing," and "swallowing," "

138
00:06:11,830 --> 00:06:13,880
swallowing," and so on.

139
00:06:13,880 --> 00:06:16,510
 Any movement that we make in the body during the day, we

140
00:06:16,510 --> 00:06:17,600
 can be mindful of.

141
00:06:17,600 --> 00:06:20,070
 When we go to the washroom, when we take a shower, when we

142
00:06:20,070 --> 00:06:21,400
 change our clothes, when we

143
00:06:21,400 --> 00:06:24,170
 wash our clothes, whatever we do during the day, we can be

144
00:06:24,170 --> 00:06:25,920
 mindful of that as well, creating

145
00:06:25,920 --> 00:06:30,000
 a clear thought based on the movements of the body.

146
00:06:30,000 --> 00:06:32,560
 This is the first way of incorporating the practice

147
00:06:32,560 --> 00:06:34,200
 directly into our daily life.

148
00:06:34,200 --> 00:06:39,170
 The second way is we can use the senses, so seeing, hearing

149
00:06:39,170 --> 00:06:42,320
, smelling, tasting, and feeling.

150
00:06:42,320 --> 00:06:45,810
 Normally, when we see something, we either enjoy it or we

151
00:06:45,810 --> 00:06:46,960
 get upset by it.

152
00:06:46,960 --> 00:06:49,360
 Well, now we're going to start to use it to create a clear

153
00:06:49,360 --> 00:06:51,040
 thought, not to create a judging

154
00:06:51,040 --> 00:06:52,160
 thought.

155
00:06:52,160 --> 00:06:55,490
 When we see something, just to know that we're seeing, say

156
00:06:55,490 --> 00:06:57,440
 to ourselves, "Seeing, seeing,

157
00:06:57,440 --> 00:06:58,440
 seeing."

158
00:06:58,440 --> 00:07:00,840
 When we hear something instead of listening and enjoying or

159
00:07:00,840 --> 00:07:02,000
 getting angry and upset by

160
00:07:02,000 --> 00:07:04,440
 it, we say to ourselves, "Hearing."

161
00:07:04,440 --> 00:07:05,960
 When we smell, we can say, "Smelling."

162
00:07:05,960 --> 00:07:09,270
 When we taste instead of enjoying it or getting repulsed by

163
00:07:09,270 --> 00:07:11,160
 the taste, we can simply say to

164
00:07:11,160 --> 00:07:14,800
 ourselves, "Tasting, tasting," and keep our mind clear.

165
00:07:14,800 --> 00:07:17,410
 When we feel something on the body, hot or cold, hard or

166
00:07:17,410 --> 00:07:18,920
 soft or whatever feeling, we

167
00:07:18,920 --> 00:07:22,840
 say to ourselves, "Feeling, feeling, feeling."

168
00:07:22,840 --> 00:07:25,970
 These two ways are a general practice which we can

169
00:07:25,970 --> 00:07:28,240
 undertake in our daily life, in the

170
00:07:28,240 --> 00:07:31,350
 way of incorporating the meditation practice directly into

171
00:07:31,350 --> 00:07:32,320
 our daily life.

172
00:07:32,320 --> 00:07:34,280
 Of course, we can also be mindful of all of the things

173
00:07:34,280 --> 00:07:35,560
 which I mentioned in the earlier

174
00:07:35,560 --> 00:07:39,870
 videos, pain, for instance, or the emotions, anger, or

175
00:07:39,870 --> 00:07:41,320
 greed, or so on.

176
00:07:41,320 --> 00:07:43,970
 But besides all of those, this we can say is an addition,

177
00:07:43,970 --> 00:07:45,120
 which once we've mastered

178
00:07:45,120 --> 00:07:47,760
 the four foundations, which I mentioned in an earlier video

179
00:07:47,760 --> 00:07:49,200
, we can then add all of these

180
00:07:49,200 --> 00:07:52,830
 things on as well, as a way of incorporating the practice

181
00:07:52,830 --> 00:07:54,440
 into our daily life.

182
00:07:54,440 --> 00:07:59,920
 This is an explanation of meditation practice in daily life

183
00:07:59,920 --> 00:08:00,160
.

184
00:08:00,160 --> 00:08:04,160
 This concludes the set of videos on how to meditate.

185
00:08:04,160 --> 00:08:07,120
 I would like to thank you all for watching, taking the time

186
00:08:07,120 --> 00:08:08,480
 to watch these videos.

187
00:08:08,480 --> 00:08:12,070
 I really, truly, sincerely hope that these videos bring

188
00:08:12,070 --> 00:08:13,740
 peace and happiness and freedom

189
00:08:13,740 --> 00:08:16,520
 from suffering to you and all of the people around you.

190
00:08:16,520 --> 00:08:17,800
 Again, thank you for tuning in.

191
00:08:17,800 --> 00:08:20,800
 If you'd like more information, you're welcome to contact

192
00:08:20,800 --> 00:08:23,200
 us through the website.

193
00:08:23,200 --> 00:08:23,760
 All the best to you.

194
00:08:23,760 --> 00:08:24,260
 Thank you.

195
00:08:24,260 --> 00:08:40,340
 [

