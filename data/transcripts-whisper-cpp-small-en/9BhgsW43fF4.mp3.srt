1
00:00:00,000 --> 00:00:05,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,000 --> 00:00:13,490
 So today we continue on with verses 256 and 257 which read

3
00:00:13,490 --> 00:00:15,000
 as follows.

4
00:00:17,000 --> 00:00:23,000
 Nadeenahoti Dhammattu yena tang sahsa naye

5
00:00:23,000 --> 00:00:31,000
 yojatang anatansha ubhoni caeya pandito

6
00:00:31,000 --> 00:00:37,000
 asahasena damena samena nayati pare

7
00:00:37,000 --> 00:00:46,000
 damasa guto me dhavi damatoti pujati

8
00:00:46,000 --> 00:00:54,000
 which means

9
00:00:54,000 --> 00:01:11,000
 one is not a judge of the Dhamma

10
00:01:11,000 --> 00:01:15,000
 simply because one judges forcefully.

11
00:01:15,000 --> 00:01:22,000
 One is not a judge, a Dhamma judge, a righteous judge.

12
00:01:22,000 --> 00:01:29,000
 Just because one judges forcefully.

13
00:01:29,000 --> 00:01:38,000
 Indeed one who

14
00:01:38,000 --> 00:01:44,000
 is able to judge both

15
00:01:44,000 --> 00:01:48,000
 cases and not cases.

16
00:01:48,000 --> 00:01:53,000
 So if one judges cases forcefully one is not a Dhamma judge

17
00:01:53,000 --> 00:01:53,000
.

18
00:01:53,000 --> 00:02:00,330
 But if one judges both what are not, what are cases and

19
00:02:00,330 --> 00:02:10,000
 what are not cases wisely.

20
00:02:10,000 --> 00:02:19,000
 Asahasena without force damena righteously samena

21
00:02:19,000 --> 00:02:28,000
 evenly nayati pare one judges others

22
00:02:28,000 --> 00:02:36,110
 should judge without force righteously evenly the cases of

23
00:02:36,110 --> 00:02:38,000
 others.

24
00:02:38,000 --> 00:02:44,360
 Thus one guards the Dhamma, damasa guto me dhavi, a wise

25
00:02:44,360 --> 00:02:45,000
 one.

26
00:02:45,000 --> 00:02:54,240
 Guards the Dhamma, damatoti pujati, thus one is a Dhamma

27
00:02:54,240 --> 00:02:56,000
 judge.

28
00:02:56,000 --> 00:03:05,000
 So this verse comes from a very simple story.

29
00:03:05,000 --> 00:03:10,000
 Though one was quite a good lesson I think.

30
00:03:10,000 --> 00:03:24,370
 The monks were aware of judges in the society in which they

31
00:03:24,370 --> 00:03:25,000
 lived.

32
00:03:25,000 --> 00:03:31,000
 So when they would go on alms round they would pass by the

33
00:03:31,000 --> 00:03:32,000
 court.

34
00:03:32,000 --> 00:03:36,430
 I don't know maybe it was an open air court in ancient

35
00:03:36,430 --> 00:03:37,000
 times.

36
00:03:37,000 --> 00:03:40,940
 And so it was a place where people would come with their

37
00:03:40,940 --> 00:03:42,000
 grievances

38
00:03:42,000 --> 00:03:46,060
 to have them sorted out by the appointed judges, judges who

39
00:03:46,060 --> 00:03:52,000
 would have been appointed by the king perhaps.

40
00:03:52,000 --> 00:03:55,000
 Judges who had great power.

41
00:03:55,000 --> 00:04:02,530
 And of course as normal this institution would have been

42
00:04:02,530 --> 00:04:05,000
 highly respected

43
00:04:05,000 --> 00:04:08,000
 as a matter of course by the populace.

44
00:04:08,000 --> 00:04:15,450
 It's given a reputation by teachers, by culture as being an

45
00:04:15,450 --> 00:04:18,000
 institution of great importance,

46
00:04:18,000 --> 00:04:27,000
 of great esteem.

47
00:04:27,000 --> 00:04:31,430
 And so naturally the monks assumed that there were great

48
00:04:31,430 --> 00:04:33,000
 things happening there

49
00:04:33,000 --> 00:04:39,000
 but one day they stopped and observed the proceedings

50
00:04:39,000 --> 00:04:44,270
 and realized that in fact the judges were not judging

51
00:04:44,270 --> 00:04:46,000
 impartially.

52
00:04:46,000 --> 00:04:55,020
 In fact they were taking bribes from rich land holders in

53
00:04:55,020 --> 00:04:59,000
 order to sway their decisions.

54
00:04:59,000 --> 00:05:06,870
 And often would decide in ways that ruined the less

55
00:05:06,870 --> 00:05:15,000
 fortunate or the less affluent of plaintiffs or defendants

56
00:05:15,000 --> 00:05:22,000
 causing ruin to a great number of people unwarranted,

57
00:05:22,000 --> 00:05:25,000
 basically stealing their property and giving it to others

58
00:05:25,000 --> 00:05:31,000
 based on bribes and faulty decisions.

59
00:05:31,000 --> 00:05:37,500
 And they were shocked and very much disillusioned by the

60
00:05:37,500 --> 00:05:43,000
 court in their area.

61
00:05:43,000 --> 00:05:46,490
 And so they were discussing this and went to see the Buddha

62
00:05:46,490 --> 00:05:49,000
 I think or the Buddha found out

63
00:05:49,000 --> 00:06:00,000
 and as a result the Buddha remarked in this way.

64
00:06:00,000 --> 00:06:07,680
 So the topic today is about judging, which is a good

65
00:06:07,680 --> 00:06:11,000
 Buddhist topic to talk about.

66
00:06:11,000 --> 00:06:14,330
 First of all we can make a brief comment on society and

67
00:06:14,330 --> 00:06:21,000
 remark that it seems to of course not gotten any better

68
00:06:21,000 --> 00:06:24,400
 throughout the world. Corruption of course is rife in

69
00:06:24,400 --> 00:06:31,000
 governments, in legal institutions.

70
00:06:31,000 --> 00:06:37,070
 We hear stories of people using religious views to bias

71
00:06:37,070 --> 00:06:39,000
 their decisions

72
00:06:39,000 --> 00:06:46,000
 but of course also greed, anger and delusion.

73
00:06:46,000 --> 00:06:51,490
 But more importantly of course is how this relates to our

74
00:06:51,490 --> 00:06:54,000
 own meditation practice.

75
00:06:54,000 --> 00:07:00,760
 Judging is a good way to describe a large portion of our

76
00:07:00,760 --> 00:07:04,000
 activity as human beings.

77
00:07:04,000 --> 00:07:10,220
 We're of course constantly judging. Judgment describes much

78
00:07:10,220 --> 00:07:17,000
 of our interaction with the world around us.

79
00:07:17,000 --> 00:07:21,280
 And so the Buddha uses this word forcefully to describe it,

80
00:07:21,280 --> 00:07:25,000
 which I confess I have a hard time translating.

81
00:07:25,000 --> 00:07:29,950
 I'm pretty confident in the translation but if you've ever

82
00:07:29,950 --> 00:07:32,000
 read this verse in English

83
00:07:32,000 --> 00:07:35,640
 you've probably noticed they use the word arbitrary, which

84
00:07:35,640 --> 00:07:37,000
 I don't think fits.

85
00:07:37,000 --> 00:07:40,340
 And I don't really understand how it is a translation of

86
00:07:40,340 --> 00:07:43,000
 this word that I don't really understand.

87
00:07:43,000 --> 00:07:48,000
 I confess. Saha sah sah sah.

88
00:07:48,000 --> 00:07:57,000
 In Thai they say sah haat. Saha haat. Sorry sah haat. Saha

89
00:07:57,000 --> 00:07:57,000
 haat.

90
00:07:57,000 --> 00:08:04,520
 Which they translate it as something like forcefully,

91
00:08:04,520 --> 00:08:07,000
 violently.

92
00:08:07,000 --> 00:08:12,900
 But the idea of being forceful is an interesting one simply

93
00:08:12,900 --> 00:08:17,000
 because it often is something that

94
00:08:17,000 --> 00:08:21,000
 lends a sort of credence.

95
00:08:21,000 --> 00:08:27,120
 If someone is expressing an opinion forcefully it often is

96
00:08:27,120 --> 00:08:32,000
 sufficient to convince your average person of the veracity

97
00:08:32,000 --> 00:08:32,000
 of it.

98
00:08:32,000 --> 00:08:36,640
 If someone says, "I believe this is right or this is wrong

99
00:08:36,640 --> 00:08:40,000
," and they have great conviction it can often sway people.

100
00:08:40,000 --> 00:08:44,510
 Often someone who is a great orator, it often doesn't

101
00:08:44,510 --> 00:08:48,000
 really matter what their stance is.

102
00:08:48,000 --> 00:08:56,490
 People who train in debate, politics, politicians can be

103
00:08:56,490 --> 00:09:04,000
 quite good at swaying people based on emotional appeal.

104
00:09:04,000 --> 00:09:08,530
 But it goes even deeper than that and I think you could

105
00:09:08,530 --> 00:09:13,400
 just use this word sah sah and it is used to describe any

106
00:09:13,400 --> 00:09:19,650
 sort of bias, any sort of biased judgment, where you force

107
00:09:19,650 --> 00:09:21,000
 something.

108
00:09:21,000 --> 00:09:26,980
 And so the idea that I get from this word is like a forcing

109
00:09:26,980 --> 00:09:31,000
 square pegs to fit in round holes.

110
00:09:31,000 --> 00:09:34,550
 You can pretend that it's going to fit or you can be

111
00:09:34,550 --> 00:09:37,000
 convinced that it's going in.

112
00:09:37,000 --> 00:09:41,780
 You can use a lot of force to try to get in but it's not

113
00:09:41,780 --> 00:09:46,000
 going to lead to a smooth or pleasant result.

114
00:09:46,000 --> 00:09:49,220
 Or like trying to force two puzzle pieces that don't fit

115
00:09:49,220 --> 00:09:50,000
 together.

116
00:09:50,000 --> 00:09:53,550
 You have a puzzle and trying to fit a piece in where it

117
00:09:53,550 --> 00:09:55,000
 doesn't belong.

118
00:09:55,000 --> 00:10:01,020
 These sorts of analogies I think aptly describe the

119
00:10:01,020 --> 00:10:07,180
 situation where you try to pass off improper judgment as

120
00:10:07,180 --> 00:10:09,000
 proper judgment.

121
00:10:09,000 --> 00:10:15,970
 And this example fits with a judge in the world who

122
00:10:15,970 --> 00:10:24,550
 presents an opinion or a judgment that goes against reality

123
00:10:24,550 --> 00:10:25,000
.

124
00:10:25,000 --> 00:10:28,000
 They force it through.

125
00:10:28,000 --> 00:10:31,020
 But we do this, this is a way of describing what we do

126
00:10:31,020 --> 00:10:32,000
 quite often.

127
00:10:32,000 --> 00:10:35,360
 We do this out of greed, we do this out of anger, we do

128
00:10:35,360 --> 00:10:37,000
 this out of delusion.

129
00:10:37,000 --> 00:10:42,500
 That's an important point because for these judges and for

130
00:10:42,500 --> 00:10:45,000
 anyone who makes these sorts of improper judgments,

131
00:10:45,000 --> 00:10:48,380
 even in their own personal life, on a moment to moment

132
00:10:48,380 --> 00:10:56,000
 basis, how we judge experience,

133
00:10:56,000 --> 00:11:01,000
 has often a sense of rightness about it.

134
00:11:01,000 --> 00:11:07,400
 For a judge who is taking bribes, it's pretty clear they

135
00:11:07,400 --> 00:11:13,000
 know they're doing something that is dangerous.

136
00:11:13,000 --> 00:11:16,790
 But to say they know that it's wrong is I think an overs

137
00:11:16,790 --> 00:11:22,250
implification because yes they know it is seen as wrong by

138
00:11:22,250 --> 00:11:23,000
 others.

139
00:11:23,000 --> 00:11:27,420
 But a person who takes bribes like that or any sort of

140
00:11:27,420 --> 00:11:31,550
 addict, someone who steals, and this is basically what

141
00:11:31,550 --> 00:11:33,000
 these judges were doing,

142
00:11:33,000 --> 00:11:37,560
 someone who is addicted to anything that causes them to act

143
00:11:37,560 --> 00:11:40,000
 and speak in manipulative ways,

144
00:11:40,000 --> 00:11:46,720
 or even just chase after objects of desire, doesn't have a

145
00:11:46,720 --> 00:11:48,000
 sense that it's wrong.

146
00:11:48,000 --> 00:11:52,660
 People who are addicted to smoking, addicted to drugs,

147
00:11:52,660 --> 00:11:55,000
 addicted to pornography,

148
00:11:55,000 --> 00:11:58,650
 they might hate themselves for it, beat themselves up for

149
00:11:58,650 --> 00:12:03,000
 it, but deep down they don't think it's wrong.

150
00:12:03,000 --> 00:12:09,000
 They are forcing through this, they are glossing over.

151
00:12:09,000 --> 00:12:17,230
 It's like a grinding of the gears that they miss because of

152
00:12:17,230 --> 00:12:21,000
 the power of the desire.

153
00:12:21,000 --> 00:12:24,000
 They only see the good of it.

154
00:12:24,000 --> 00:12:29,450
 This is why it's quite difficult for people to give up sens

155
00:12:29,450 --> 00:12:31,000
ual pleasure,

156
00:12:31,000 --> 00:12:35,950
 give up the things that they are addicted to, the things

157
00:12:35,950 --> 00:12:39,000
 they want, the things they crave,

158
00:12:39,000 --> 00:12:45,360
 because their lack of clarity, the imprecision of their

159
00:12:45,360 --> 00:12:49,000
 judgment, the wrongness of their judgment,

160
00:12:49,000 --> 00:12:55,760
 where they judge something to be satisfying a source of

161
00:12:55,760 --> 00:12:57,000
 pleasure,

162
00:12:57,000 --> 00:13:01,800
 it provides them with a conviction, a sense of the right

163
00:13:01,800 --> 00:13:03,000
ness of it.

164
00:13:03,000 --> 00:13:09,380
 This is, of course, why, a very important point, why it's

165
00:13:09,380 --> 00:13:13,690
 not really possible for someone who is mindful to get

166
00:13:13,690 --> 00:13:15,000
 caught up in this.

167
00:13:15,000 --> 00:13:19,970
 When you're mindful, your addictions seem to melt away at

168
00:13:19,970 --> 00:13:23,000
 the moment when you're mindful.

169
00:13:23,000 --> 00:13:26,760
 When you try to mindfully smoke cigarettes, you find it

170
00:13:26,760 --> 00:13:29,000
 very hard to be attached to it.

171
00:13:29,000 --> 00:13:34,000
 The desire seems to just melt away because there's clarity,

172
00:13:34,000 --> 00:13:36,000
 because there's precision.

173
00:13:36,000 --> 00:13:39,490
 And you can describe it as you're no longer just forcing

174
00:13:39,490 --> 00:13:43,000
 through based on your belief,

175
00:13:43,000 --> 00:13:50,160
 based on your imperfect perception, your perception of it

176
00:13:50,160 --> 00:13:52,000
 as right.

177
00:13:52,000 --> 00:13:56,280
 The greatness of mindfulness is that it gives you this

178
00:13:56,280 --> 00:13:59,000
 clarity and the ability to perceive.

179
00:13:59,000 --> 00:14:03,000
 "Ataṅ anataṅ," as the Buddha said, "What is a case?"

180
00:14:03,000 --> 00:14:07,000
 Those are hard words. I had a hard time translating that.

181
00:14:07,000 --> 00:14:12,770
 "Ata" can mean "case," like you judge a case, like a matter

182
00:14:12,770 --> 00:14:16,100
. It literally means something like matter, a subject, a

183
00:14:16,100 --> 00:14:18,000
 topic or something.

184
00:14:18,000 --> 00:14:23,250
 But it also means benefit. So "ata" is that which is

185
00:14:23,250 --> 00:14:27,000
 meaningful, and "anata," that which is not meaningful.

186
00:14:27,000 --> 00:14:33,720
 But it's extrapolated here to mean something like what is

187
00:14:33,720 --> 00:14:36,000
 right and wrong.

188
00:14:36,000 --> 00:14:40,880
 So if someone presents a case before a judge, the judge can

189
00:14:40,880 --> 00:14:43,000
 see that it has merit.

190
00:14:43,000 --> 00:14:47,260
 They say, "This case has merit." So that would be "ata." It

191
00:14:47,260 --> 00:14:50,000
 has meaning. It's meaningful.

192
00:14:50,000 --> 00:14:53,300
 If a case doesn't have merit, then they have to judge it

193
00:14:53,300 --> 00:14:58,000
 against the person presenting the case. It's "anata."

194
00:14:58,000 --> 00:15:03,240
 So this kind of language, it's just the good or bad, right

195
00:15:03,240 --> 00:15:06,000
 or wrong kind of language.

196
00:15:06,000 --> 00:15:09,570
 So mindfulness provides you with that, the ability to see

197
00:15:09,570 --> 00:15:13,990
 through, to see that taking bribes, there's no universe in

198
00:15:13,990 --> 00:15:16,000
 which that is right.

199
00:15:16,000 --> 00:15:19,990
 Again, it actually seems right to these judges who know

200
00:15:19,990 --> 00:15:23,000
 that they're doing something "wrong."

201
00:15:23,000 --> 00:15:27,760
 But it seems right. Why? Because money is right. Me getting

202
00:15:27,760 --> 00:15:33,000
 money is right. That is the goal.

203
00:15:33,000 --> 00:15:38,000
 This is why people lie and cheat and steal the imprecision.

204
00:15:38,000 --> 00:15:41,990
 In their mind, they are grinding the gears, forcing through

205
00:15:41,990 --> 00:15:42,000
.

206
00:15:42,000 --> 00:15:47,660
 Their inability to see clearly is just providing them with

207
00:15:47,660 --> 00:15:53,250
 a conviction that is able to just ignore all the stress and

208
00:15:53,250 --> 00:16:01,000
 suffering and wretchedness that comes from addiction.

209
00:16:01,000 --> 00:16:06,980
 An addict is so blind to the consequences that it feels

210
00:16:06,980 --> 00:16:11,000
 like bliss to engage in their addiction.

211
00:16:11,000 --> 00:16:14,280
 But any objective observer outside, even not someone

212
00:16:14,280 --> 00:16:18,840
 practicing mindfulness, looks at them and says, "What a w

213
00:16:18,840 --> 00:16:20,000
retch."

214
00:16:20,000 --> 00:16:23,830
 And they're not wrong in saying that. It's quite

215
00:16:23,830 --> 00:16:28,140
 objectively clear this person is wretched, suffering

216
00:16:28,140 --> 00:16:31,000
 terribly in their bliss, right?

217
00:16:31,000 --> 00:16:35,660
 This is, I think, the meaning of "sahasa," forcing. And I

218
00:16:35,660 --> 00:16:40,000
 think it's a really good word, a good way of describing

219
00:16:40,000 --> 00:16:43,720
 what happens when you don't have the clarity of mindfulness

220
00:16:43,720 --> 00:16:44,000
.

221
00:16:44,000 --> 00:16:48,900
 So mindfulness, again, is just the simple act of reminding

222
00:16:48,900 --> 00:16:52,000
 yourself. It's just one moment.

223
00:16:52,000 --> 00:16:58,860
 And in that moment, you adjust your perception, right?

224
00:16:58,860 --> 00:17:03,840
 Because this state of mind that is forceful, that is just

225
00:17:03,840 --> 00:17:08,000
 pushing through, based on the narrative that you provide,

226
00:17:08,000 --> 00:17:12,010
 "This is good for me. This is my goal, money or pleasure or

227
00:17:12,010 --> 00:17:15,000
 whatever," or conversely, anger, right?

228
00:17:15,000 --> 00:17:18,510
 Some people can feel righteous in their anger. I read

229
00:17:18,510 --> 00:17:23,640
 recently that there are Buddhist monks actually defending

230
00:17:23,640 --> 00:17:32,000
 war and killing and saying that some war is justifiable,

231
00:17:32,000 --> 00:17:38,320
 based, I guess, on this attachment to society, right? If

232
00:17:38,320 --> 00:17:43,080
 someone invades your country, the horror of losing your

233
00:17:43,080 --> 00:17:47,600
 country, this thing that you cling to, can lead you to

234
00:17:47,600 --> 00:17:53,000
 great anger and the ability to kill in defense.

235
00:17:53,000 --> 00:17:56,720
 I think defense can be warranted, but certainly not killing

236
00:17:56,720 --> 00:18:01,050
. I think why someone would force through that opinion is

237
00:18:01,050 --> 00:18:05,010
 they're not able to see clearly enough the distinction

238
00:18:05,010 --> 00:18:09,300
 between, perhaps, say, defending yourself and murdering

239
00:18:09,300 --> 00:18:12,000
 someone else.

240
00:18:12,000 --> 00:18:15,900
 And so as a result, there's, I mean, partially, at least,

241
00:18:15,900 --> 00:18:19,650
 because of anger, the aversion to losing what you hold dear

242
00:18:19,650 --> 00:18:22,000
, your country, your society.

243
00:18:22,000 --> 00:18:25,010
 Even Buddhist countries often go to war and are very

244
00:18:25,010 --> 00:18:28,370
 passionate about protecting Buddhism, which is kind of

245
00:18:28,370 --> 00:18:30,000
 horrific to think about.

246
00:18:30,000 --> 00:18:33,790
 But on the other hand, the Buddha didn't spend a lot of

247
00:18:33,790 --> 00:18:40,610
 time trying to fix society. I think wisely he didn't take

248
00:18:40,610 --> 00:18:45,000
 sides, didn't pick parties to follow or to support.

249
00:18:45,000 --> 00:18:49,980
 He praised people who were praiseworthy, but much more than

250
00:18:49,980 --> 00:18:54,930
 people, he praised qualities that were praiseworthy to try

251
00:18:54,930 --> 00:18:59,440
 to convince all sides to better themselves rather than

252
00:18:59,440 --> 00:19:08,000
 taking sides and creating militaristic situations.

253
00:19:08,000 --> 00:19:14,400
 But these states where we are caught up in greed and anger

254
00:19:14,400 --> 00:19:21,230
 and delusion are made up of moments and they're perpetuated

255
00:19:21,230 --> 00:19:23,000
 by moments.

256
00:19:23,000 --> 00:19:29,950
 And so it may seem insignificant to sit on your meditation

257
00:19:29,950 --> 00:19:36,880
 seat and repeat to yourself pain, pain or anger or liking

258
00:19:36,880 --> 00:19:40,000
 or any of these things.

259
00:19:40,000 --> 00:19:47,330
 But you have to understand the again, this nature of how

260
00:19:47,330 --> 00:19:54,980
 our habits form and how they reform every moment that these

261
00:19:54,980 --> 00:19:56,000
 are not

262
00:19:56,000 --> 00:19:59,000
 masses or things that we carry around with us.

263
00:19:59,000 --> 00:20:03,340
 They are processes and they are processes that only

264
00:20:03,340 --> 00:20:08,000
 continue by the perpetuation, the feeding of them.

265
00:20:08,000 --> 00:20:18,000
 We feed them through our continuation of the habit.

266
00:20:18,000 --> 00:20:24,380
 Our anger perpetuates more anger. Our greed cultivates the

267
00:20:24,380 --> 00:20:30,210
 habit of greed. We like our liking. Our anger makes us more

268
00:20:30,210 --> 00:20:32,000
 angry and so on.

269
00:20:32,000 --> 00:20:38,410
 And so every moment that we change this provides us with an

270
00:20:38,410 --> 00:20:44,000
 alternative and it provides us with a clarity.

271
00:20:44,000 --> 00:20:49,530
 And so in that moment, it's like putting on the brakes and

272
00:20:49,530 --> 00:20:53,730
 provides you with this self reflection that is so much a

273
00:20:53,730 --> 00:21:00,000
 part of meditation that allows you to see how am I doing?

274
00:21:00,000 --> 00:21:03,000
 How am I doing this?

275
00:21:03,000 --> 00:21:07,610
 What is the state of my mind as I pass this judgment, as I

276
00:21:07,610 --> 00:21:10,000
 engage in this activity?

277
00:21:10,000 --> 00:21:14,570
 It allows you to see and that's the whole point. Mind

278
00:21:14,570 --> 00:21:20,000
fulness leads to seeing, seeing clearly.

279
00:21:20,000 --> 00:21:23,770
 It's quite, it should be quite apparent. You should be able

280
00:21:23,770 --> 00:21:28,710
 to see from moment to moment that you're less inclined to

281
00:21:28,710 --> 00:21:35,000
 engage in hasty judgment, irrational judgment, reactivity.

282
00:21:35,000 --> 00:21:39,410
 You start instead of reacting, going with it, to seeing

283
00:21:39,410 --> 00:21:44,200
 your reactions. You start to be like you're watching

284
00:21:44,200 --> 00:21:50,650
 yourself in horror as you get angry again and again, as you

285
00:21:50,650 --> 00:21:54,000
 cling again and again.

286
00:21:54,000 --> 00:22:00,750
 And you see it, it, it wash away because you slowly, slowly

287
00:22:00,750 --> 00:22:03,000
 stop feeding it.

288
00:22:03,000 --> 00:22:10,420
 And you replace this forceful quality with a much more or a

289
00:22:10,420 --> 00:22:14,000
 similar sort of forcefulness.

290
00:22:14,000 --> 00:22:17,890
 As I think it possibly, although I don't know about this

291
00:22:17,890 --> 00:22:22,000
 word, you could use it in a way that is not unwholesome.

292
00:22:22,000 --> 00:22:27,960
 If someone is clearly seeing the nature of their experience

293
00:22:27,960 --> 00:22:33,880
, then there is a forcefulness still to their actions, to

294
00:22:33,880 --> 00:22:36,000
 their judgments.

295
00:22:36,000 --> 00:22:38,900
 But the force is the force of knowledge, the force of

296
00:22:38,900 --> 00:22:40,000
 understanding.

297
00:22:40,000 --> 00:22:45,470
 They simply see clearly. They see how smoking, of smoker

298
00:22:45,470 --> 00:22:48,420
 sees how smoking is disgusting and really unpleasant and

299
00:22:48,420 --> 00:22:53,000
 not useful or beneficial in any way.

300
00:22:53,000 --> 00:22:56,890
 Seeing that it is a cause for addiction, it is a cause for

301
00:22:56,890 --> 00:23:00,890
 the stress and the suffering of withdrawal and so on and so

302
00:23:00,890 --> 00:23:01,000
 on.

303
00:23:01,000 --> 00:23:06,870
 And same with anger, for example, with taking bribes apart

304
00:23:06,870 --> 00:23:12,500
 from how it can lead to criminal prosecution and so on, vil

305
00:23:12,500 --> 00:23:14,000
ification.

306
00:23:14,000 --> 00:23:20,050
 Simply the insanity of the cruelty of hurting other people,

307
00:23:20,050 --> 00:23:25,760
 taking away their property for your own benefit, that sort

308
00:23:25,760 --> 00:23:27,000
 of thing.

309
00:23:27,000 --> 00:23:34,000
 Just seeing all this affects a great change in a person.

310
00:23:34,000 --> 00:23:37,630
 But again, there is a great strength and so the force, you

311
00:23:37,630 --> 00:23:46,000
 gain a great force, a great inertia or what is the word?

312
00:23:46,000 --> 00:23:52,100
 A great power in your mind that is based on a stronger

313
00:23:52,100 --> 00:23:54,000
 conviction.

314
00:23:54,000 --> 00:23:58,190
 It is no longer grinding the gears. It is knowing that the

315
00:23:58,190 --> 00:24:00,910
 circular peg fits in the circular hole and going for it

316
00:24:00,910 --> 00:24:02,000
 without any doubt.

317
00:24:02,000 --> 00:24:05,110
 Seeing that, oh wait, this one is square, this goes in the

318
00:24:05,110 --> 00:24:06,000
 square hole.

319
00:24:06,000 --> 00:24:09,020
 Putting things in the right place simply because you see

320
00:24:09,020 --> 00:24:10,000
 more clearly.

321
00:24:10,000 --> 00:24:13,490
 That is the general principle of mindfulness and of vipass

322
00:24:13,490 --> 00:24:14,000
ana.

323
00:24:14,000 --> 00:24:17,910
 So I think that is the lesson that we can take from these

324
00:24:17,910 --> 00:24:19,000
 two verses.

325
00:24:19,000 --> 00:24:21,000
 And that is the Dhamma Padapar today.

326
00:24:21,000 --> 00:24:24,000
 Thank you for listening.

327
00:24:25,000 --> 00:24:28,000
 Thank you.

328
00:24:29,000 --> 00:24:32,000
 Thank you.

