1
00:00:00,000 --> 00:00:06,000
 Okay, so this latest question is from an anonymous poster.

2
00:00:06,000 --> 00:00:09,230
 Hi, Yutadama. Well, I'm very interested in Buddhism and

3
00:00:09,230 --> 00:00:10,000
 meditation.

4
00:00:10,000 --> 00:00:13,070
 I feel very confused and do not know what tradition of

5
00:00:13,070 --> 00:00:14,000
 Buddhism is the best for me.

6
00:00:14,000 --> 00:00:16,000
 How should I choose?

7
00:00:16,000 --> 00:00:19,880
 Well, the first thing I'd say is that probably across the

8
00:00:19,880 --> 00:00:20,000
 board,

9
00:00:20,000 --> 00:00:24,270
 nobody would ever recommend you to pick a tradition of

10
00:00:24,270 --> 00:00:25,000
 Buddhism,

11
00:00:25,000 --> 00:00:28,360
 simply because within each of the major traditions of

12
00:00:28,360 --> 00:00:29,000
 Buddhism,

13
00:00:29,000 --> 00:00:34,000
 there is such an incredible variety.

14
00:00:34,000 --> 00:00:39,610
 Some monasteries will be strict, some monasteries will be

15
00:00:39,610 --> 00:00:40,000
 lax.

16
00:00:40,000 --> 00:00:43,840
 And so you're always better off to pick a center or to pick

17
00:00:43,840 --> 00:00:45,000
 a lineage.

18
00:00:45,000 --> 00:00:49,000
 Lineage meaning a certain set of teachings.

19
00:00:49,000 --> 00:00:52,780
 Like for instance, the teachings that I teach and practice

20
00:00:52,780 --> 00:00:54,000
 are based on Mahasi Sayadaw,

21
00:00:54,000 --> 00:00:58,380
 who was a teacher in Burma who passed away about 30 years

22
00:00:58,380 --> 00:00:59,000
 ago.

23
00:00:59,000 --> 00:01:02,990
 And so all of the centers in the Mahasi Sayadaw lineage

24
00:01:02,990 --> 00:01:05,000
 will practice a very similar technique

25
00:01:05,000 --> 00:01:09,000
 and are generally pretty strict and pretty straightforward.

26
00:01:09,000 --> 00:01:12,780
 They teach a very similar technique to what you find on my

27
00:01:12,780 --> 00:01:15,000
 YouTube channel.

28
00:01:15,000 --> 00:01:18,000
 And they'll vary from monastery to monastery,

29
00:01:18,000 --> 00:01:22,980
 but you find that you can adapt from one to the other quite

30
00:01:22,980 --> 00:01:24,000
 easily.

31
00:01:24,000 --> 00:01:29,850
 So this is of much more benefit, finding a center which you

32
00:01:29,850 --> 00:01:32,000
 can really relate to.

33
00:01:32,000 --> 00:01:36,000
 I wouldn't recommend anything except that which I practice,

34
00:01:36,000 --> 00:01:40,000
 so it's probably useless for me to recommend anything.

35
00:01:40,000 --> 00:01:44,000
 But you should pick according to the meditation practice.

36
00:01:44,000 --> 00:01:49,060
 You should look at the teacher and ask yourself whether

37
00:01:49,060 --> 00:01:53,000
 what they're saying is not only convincing,

38
00:01:53,000 --> 00:01:56,000
 but is something that is practical.

39
00:01:56,000 --> 00:01:59,580
 So it's very easy for people to give these high sounding

40
00:01:59,580 --> 00:02:00,000
 teachings

41
00:02:00,000 --> 00:02:05,000
 on all of the various stages of enlightenment and so on,

42
00:02:05,000 --> 00:02:10,000
 or give teachings which are inspiring or so on.

43
00:02:10,000 --> 00:02:12,000
 But you have to ask yourself, "Is this practical?

44
00:02:12,000 --> 00:02:18,440
 Are you actually able to put these fundamentals or these

45
00:02:18,440 --> 00:02:20,000
 teachings into practice?

46
00:02:20,000 --> 00:02:23,240
 Is it something that I can go ahead, go back to my room and

47
00:02:23,240 --> 00:02:25,000
 sit down and practice?"

48
00:02:25,000 --> 00:02:27,000
 So meditation should be a key.

49
00:02:27,000 --> 00:02:29,750
 And you should ask yourself whether the meditation is

50
00:02:29,750 --> 00:02:31,000
 leading to progress,

51
00:02:31,000 --> 00:02:33,000
 whether you're actually gaining anything when you sit.

52
00:02:33,000 --> 00:02:35,760
 Are you just sitting and closing your eyes and getting

53
00:02:35,760 --> 00:02:37,000
 blissful feelings?

54
00:02:37,000 --> 00:02:39,370
 Or are you actually coming to understand things about

55
00:02:39,370 --> 00:02:40,000
 yourself?

56
00:02:40,000 --> 00:02:42,610
 Are you learning more about yourself and the world around

57
00:02:42,610 --> 00:02:43,000
 you?

58
00:02:43,000 --> 00:02:48,830
 Are you coming to understand anything that you didn't see

59
00:02:48,830 --> 00:02:50,000
 before?

60
00:02:50,000 --> 00:02:52,180
 And I guess in that sense it's not really important which

61
00:02:52,180 --> 00:02:53,000
 centre you choose.

62
00:02:53,000 --> 00:02:55,000
 You can go and try them all out.

63
00:02:55,000 --> 00:03:00,000
 I had a friend who, before I met him,

64
00:03:00,000 --> 00:03:04,250
 he had gone around to all of the centres in the area where

65
00:03:04,250 --> 00:03:05,000
 I was staying.

66
00:03:05,000 --> 00:03:07,000
 Before I came to stay there,

67
00:03:07,000 --> 00:03:10,000
 he had gone around to all of the centres in the area

68
00:03:10,000 --> 00:03:13,000
 and tried them all out and found none of them suitable

69
00:03:13,000 --> 00:03:16,710
 until finally he came to see me and I hadn't even set up a

70
00:03:16,710 --> 00:03:17,000
 centre.

71
00:03:17,000 --> 00:03:20,400
 I was just staying in a Cambodian monastery and he started

72
00:03:20,400 --> 00:03:21,000
 practicing.

73
00:03:21,000 --> 00:03:24,780
 I just showed him the teaching that my teacher had given to

74
00:03:24,780 --> 00:03:25,000
 me

75
00:03:25,000 --> 00:03:27,420
 and right away he realised that's the teaching that he was

76
00:03:27,420 --> 00:03:28,000
 looking for.

77
00:03:28,000 --> 00:03:31,350
 So since then I understand he's been practicing that

78
00:03:31,350 --> 00:03:32,000
 teaching.

79
00:03:32,000 --> 00:03:34,190
 I haven't seen him in a while, but he was convinced that

80
00:03:34,190 --> 00:03:35,000
 this was correct.

81
00:03:35,000 --> 00:03:39,000
 So I think you can just try out whatever is near you.

82
00:03:39,000 --> 00:03:42,200
 And the other good thing about that is it often gives you a

83
00:03:42,200 --> 00:03:44,000
 link with that tradition

84
00:03:44,000 --> 00:03:47,680
 as opposed to say, "Where should I go? Should I fly to

85
00:03:47,680 --> 00:03:51,000
 Tibet or Thailand or Burma?"

86
00:03:51,000 --> 00:03:55,000
 You can go to your local centres if there are any nearby

87
00:03:55,000 --> 00:03:57,000
 and talk to the people there.

88
00:03:57,000 --> 00:03:59,720
 If you start to practice with them and they see that you're

89
00:03:59,720 --> 00:04:00,000
 sincere,

90
00:04:00,000 --> 00:04:03,000
 they'll often encourage you to go to this centre or that

91
00:04:03,000 --> 00:04:03,000
 centre

92
00:04:03,000 --> 00:04:08,420
 and they'll be able to recommend the more strict and

93
00:04:08,420 --> 00:04:13,000
 traditional and real centres,

94
00:04:13,000 --> 00:04:15,780
 the centres that will give you a real understanding of the

95
00:04:15,780 --> 00:04:17,000
 Buddhist teaching.

96
00:04:17,000 --> 00:04:22,000
 So check out what's local and just use your own judgement.

97
00:04:22,000 --> 00:04:26,000
 Try to watch the teacher and not too closely.

98
00:04:26,000 --> 00:04:27,000
 I mean listen to the teachings.

99
00:04:27,000 --> 00:04:30,580
 Don't worry too much about the people because everybody's

100
00:04:30,580 --> 00:04:31,000
 human.

101
00:04:31,000 --> 00:04:33,000
 But look and see.

102
00:04:33,000 --> 00:04:35,000
 Is the teaching giving you something that's useful?

103
00:04:35,000 --> 00:04:38,250
 You're always welcome to come on out to California if you

104
00:04:38,250 --> 00:04:39,000
're anywhere nearby

105
00:04:39,000 --> 00:04:41,000
 and I could give you some pointers.

106
00:04:41,000 --> 00:04:44,660
 If you come and practice here, I could send you off to

107
00:04:44,660 --> 00:04:46,000
 Thailand or wherever.

108
00:04:46,000 --> 00:04:49,930
 I know a few places that you might be able to get your foot

109
00:04:49,930 --> 00:04:51,000
 in the door.

110
00:04:51,000 --> 00:04:54,000
 Okay, so I hope that helps and wish you all the best.

111
00:04:54,000 --> 00:04:56,000
 Good luck in your search.

