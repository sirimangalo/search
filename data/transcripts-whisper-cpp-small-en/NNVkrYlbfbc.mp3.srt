1
00:00:00,000 --> 00:00:07,260
 Hello YouTube, I'm back now for Ask a Monk. I was away for

2
00:00:07,260 --> 00:00:11,000
 a brief time, busy over the weekend.

3
00:00:11,000 --> 00:00:14,990
 So I've got some questions here. I'm going to answer them

4
00:00:14,990 --> 00:00:18,000
 not according to the order they were received, but

5
00:00:18,000 --> 00:00:19,000
 according to popularity.

6
00:00:19,000 --> 00:00:22,000
 So please remember to place your votes.

7
00:00:22,000 --> 00:00:26,150
 So the lead question is, "How should my meditation progress

8
00:00:26,150 --> 00:00:30,180
 after the initial stages of concentration of the breath and

9
00:00:30,180 --> 00:00:33,050
 being witness to a clear thought? And when will I know I

10
00:00:33,050 --> 00:00:34,000
 have arrived?"

11
00:00:34,000 --> 00:00:39,200
 I don't know exactly about when will I know I have arrived

12
00:00:39,200 --> 00:00:44,510
 because it's a long path and it's really a lot more gradual

13
00:00:44,510 --> 00:00:45,000
.

14
00:00:45,000 --> 00:00:48,160
 What happens when you create the clear thought is you start

15
00:00:48,160 --> 00:00:51,070
 to let go of things. You don't cling and you don't worry

16
00:00:51,070 --> 00:00:53,000
 and you don't fret over things.

17
00:00:53,000 --> 00:00:55,960
 You don't hold on to it because you see that there's

18
00:00:55,960 --> 00:00:59,600
 nothing you can do to keep it the way you want it. It's not

19
00:00:59,600 --> 00:01:02,000
 going to bring you peace and happiness.

20
00:01:02,000 --> 00:01:06,050
 These things that we hold on to are modes of reacting to

21
00:01:06,050 --> 00:01:10,460
 things, either clinging to good things and chasing after

22
00:01:10,460 --> 00:01:15,060
 them or running away from bad things and preventing them,

23
00:01:15,060 --> 00:01:18,000
 always denying them.

24
00:01:18,000 --> 00:01:21,540
 This compartmentalization of reality as these things are

25
00:01:21,540 --> 00:01:24,740
 good and these things are bad and only allowing certain

26
00:01:24,740 --> 00:01:26,000
 experiences in.

27
00:01:26,000 --> 00:01:29,550
 We see that that's a cause for suffering, that there's

28
00:01:29,550 --> 00:01:33,110
 nothing that we could hold on to, nothing that we could

29
00:01:33,110 --> 00:01:36,910
 cling to, nothing that we could try to shape and mold into

30
00:01:36,910 --> 00:01:40,000
 the perfect reality that would make us happy.

31
00:01:40,000 --> 00:01:42,700
 And so we let go. The clear thought just helps you to let

32
00:01:42,700 --> 00:01:46,740
 go. It helps you to be here and now without having to be

33
00:01:46,740 --> 00:01:51,000
 somewhere else or something else than what you are.

34
00:01:51,000 --> 00:01:56,300
 So there's really not very far that you have to go. If you

35
00:01:56,300 --> 00:01:59,350
're creating the clear thought, you should be seeing if you

36
00:01:59,350 --> 00:02:01,000
're practicing correctly.

37
00:02:01,000 --> 00:02:04,520
 You should be seeing, learning things about yourselves that

38
00:02:04,520 --> 00:02:08,080
 you didn't know before, coming to see things that you

39
00:02:08,080 --> 00:02:11,000
 couldn't see about yourself before.

40
00:02:11,000 --> 00:02:17,100
 It'll help you to deal with all of the difficulties in life

41
00:02:17,100 --> 00:02:23,220
, to deal with things in a more rational and more wise and a

42
00:02:23,220 --> 00:02:26,000
 more productive way.

43
00:02:26,000 --> 00:02:28,590
 There's a lot of technical detail that I could go into as

44
00:02:28,590 --> 00:02:32,250
 to the stages of practice, but that's not really helpful

45
00:02:32,250 --> 00:02:37,000
 and it's not really advised in the beginning stages anyway.

46
00:02:37,000 --> 00:02:41,140
 The best thing I could advise for you is to find a teacher

47
00:02:41,140 --> 00:02:44,930
 and practice as you are continuously because it's the

48
00:02:44,930 --> 00:02:48,170
 continuous practice that leads to the goal that leads you

49
00:02:48,170 --> 00:02:49,000
 to arrive.

50
00:02:49,000 --> 00:02:51,770
 One thing I would say is that you're going to run into a

51
00:02:51,770 --> 00:02:55,010
 lot of hindrances. You're going to come up against a lot of

52
00:02:55,010 --> 00:02:58,040
 things that try to trick you into thinking that you've hit

53
00:02:58,040 --> 00:02:59,000
 a roadblock.

54
00:02:59,000 --> 00:03:04,200
 There will be states of liking, states of disliking, states

55
00:03:04,200 --> 00:03:09,000
 of boredom, states of worry, states of depression.

56
00:03:09,000 --> 00:03:11,760
 Your mind is going to change as you practice and it's not

57
00:03:11,760 --> 00:03:14,320
 necessarily going to be in a positive way. The mind is

58
00:03:14,320 --> 00:03:15,000
 always changing.

59
00:03:15,000 --> 00:03:17,560
 So you might be thinking that when you practice, you're

60
00:03:17,560 --> 00:03:21,160
 just going to get happier and happier and happier, but what

61
00:03:21,160 --> 00:03:23,070
 you're doing in the meditation is digging up all of the

62
00:03:23,070 --> 00:03:26,000
 positive and negative stuff inside of you.

63
00:03:26,000 --> 00:03:28,960
 So there's going to be a lot of negative stuff coming up

64
00:03:28,960 --> 00:03:32,430
 and you should be very quick to catch that and to apply the

65
00:03:32,430 --> 00:03:36,000
 meditation to everything that arises.

66
00:03:36,000 --> 00:03:38,560
 So when something comes up and you think, "Oh, now what do

67
00:03:38,560 --> 00:03:41,000
 I do?" or you think, "Oh, that's it and I don't have to

68
00:03:41,000 --> 00:03:43,000
 practice anymore," practice on that.

69
00:03:43,000 --> 00:03:45,790
 Look at what's going on in your mind and as long as you're

70
00:03:45,790 --> 00:03:49,000
 doing that continuously, being very honest with yourself,

71
00:03:49,000 --> 00:03:50,000
 the clear thought is all you need.

72
00:03:50,000 --> 00:03:53,760
 As long as you have this clear understanding of things as

73
00:03:53,760 --> 00:03:57,630
 they are, there's nothing else you need and there's no

74
00:03:57,630 --> 00:04:00,000
 other stage or method of practice.

75
00:04:00,000 --> 00:04:03,920
 Once you practice like this, the arrival is the freedom

76
00:04:03,920 --> 00:04:07,760
 from attachment that no longer you need things to be in a

77
00:04:07,760 --> 00:04:09,000
 certain way.

78
00:04:09,000 --> 00:04:12,080
 So if you're asking when you arrive, the best thing I could

79
00:04:12,080 --> 00:04:15,400
 say is when you have no more greed, when you have no more

80
00:04:15,400 --> 00:04:17,000
 anger, when you have no more delusion,

81
00:04:17,000 --> 00:04:19,400
 when you just see things as they are and when you're

82
00:04:19,400 --> 00:04:22,480
 content with things as they are and when nothing can bring

83
00:04:22,480 --> 00:04:24,780
 you stress or suffering, that's when you've reached the

84
00:04:24,780 --> 00:04:25,000
 goal.

85
00:04:25,000 --> 00:04:28,370
 Okay, so I hope that helps. This is my first answer and

86
00:04:28,370 --> 00:04:30,000
 thanks. Keep them coming.

