1
00:00:00,000 --> 00:00:05,000
 Why do Muslims seem to dislike us so much?

2
00:00:05,000 --> 00:00:08,380
 I've tried to explain to a Muslim friend that we do not

3
00:00:08,380 --> 00:00:12,000
 worship idols, but it seems to fall on deaf ears.

4
00:00:12,000 --> 00:00:16,000
 It's actually really two issues here.

5
00:00:16,000 --> 00:00:22,180
 I guess the implication that you're making is that Muslims

6
00:00:22,180 --> 00:00:25,000
 dislike us because we worship idols,

7
00:00:25,000 --> 00:00:30,000
 and Muslim people are very much against worshipping idols.

8
00:00:30,000 --> 00:00:35,000
 Well, you know what? The problem is we do worship idols.

9
00:00:35,000 --> 00:00:38,000
 Sad to say.

10
00:00:38,000 --> 00:00:41,000
 But a lot of Buddhists do worship idols.

11
00:00:41,000 --> 00:00:47,360
 They think of the Buddha as a god, and more especially when

12
00:00:47,360 --> 00:00:50,000
 they don't think of the Buddha as a god.

13
00:00:50,000 --> 00:00:56,870
 But there are people who wear Buddha images around their

14
00:00:56,870 --> 00:00:59,000
 necks, thinking that it's going to protect them.

15
00:00:59,000 --> 00:01:01,850
 And that's an understatement. Scott, you know this, I think

16
00:01:01,850 --> 00:01:05,000
. I think this is all familiar to you.

17
00:01:05,000 --> 00:01:16,270
 That there are people out there who pay millions of dollars

18
00:01:16,270 --> 00:01:23,000
 for a single small Buddha image.

19
00:01:23,000 --> 00:01:26,090
 Yeah, I'm going to get to that. Actually, it is quite a

20
00:01:26,090 --> 00:01:27,000
 generalization.

21
00:01:27,000 --> 00:01:30,570
 But don't worry, I'm not going to let it...we will get

22
00:01:30,570 --> 00:01:32,000
 there.

23
00:01:32,000 --> 00:01:40,240
 So, I mean, there is a valid criticism there that the Sri

24
00:01:40,240 --> 00:01:50,000
 Lankan people, they look down upon this as a good thing.

25
00:01:50,000 --> 00:01:53,000
 They say, "You don't wear a Buddha image around your neck.

26
00:01:53,000 --> 00:01:55,000
 That's an image of the Buddha."

27
00:01:55,000 --> 00:01:58,530
 And the Thai people are all like, "Yeah, buying and selling

28
00:01:58,530 --> 00:02:00,000
 these Buddha images."

29
00:02:00,000 --> 00:02:02,560
 And they have this way of feeling them. You touch the

30
00:02:02,560 --> 00:02:04,000
 Buddha image with your thumb.

31
00:02:04,000 --> 00:02:07,000
 There was this German man who claimed to be able to do it.

32
00:02:07,000 --> 00:02:12,000
 So he was showing me, touching the Buddha image.

33
00:02:12,000 --> 00:02:16,920
 And he would say, "This one is powerful." Or, "This one is

34
00:02:16,920 --> 00:02:18,000
 not powerful."

35
00:02:18,000 --> 00:02:19,420
 Or, "This one has this kind of power. This one has that

36
00:02:19,420 --> 00:02:20,000
 kind of power."

37
00:02:20,000 --> 00:02:23,470
 And so Thai people have this word, "Kang," "Kang," I think

38
00:02:23,470 --> 00:02:24,000
 it is.

39
00:02:24,000 --> 00:02:29,000
 "Kang" which means powerful. "Kang."

40
00:02:29,000 --> 00:02:31,770
 And so they know which one is powerful and which one is not

41
00:02:31,770 --> 00:02:32,000
.

42
00:02:32,000 --> 00:02:35,420
 Nowadays, I don't even know if it's still a fad, but when I

43
00:02:35,420 --> 00:02:40,000
 was in Thailand, there was this disgusting fad of...

44
00:02:40,000 --> 00:02:44,480
 It wasn't even Buddha images anymore. It was this angel

45
00:02:44,480 --> 00:02:47,000
 called Jatukham.

46
00:02:47,000 --> 00:02:51,260
 Jatukham, which is a fictitious being that apparently, that

47
00:02:51,260 --> 00:02:53,000
 is supposed to...

48
00:02:53,000 --> 00:02:55,670
 It was just made up. There's even a story about where it

49
00:02:55,670 --> 00:02:56,000
 was made up.

50
00:02:56,000 --> 00:03:03,740
 It was made up based on a Hindu, based on, not even exactly

51
00:03:03,740 --> 00:03:08,000
, but based on a Hindu deity.

52
00:03:08,000 --> 00:03:14,750
 That's supposed to guard this Jaitiya, this pagoda in

53
00:03:14,750 --> 00:03:16,000
 Thailand.

54
00:03:16,000 --> 00:03:20,770
 And it became this huge fad. And people got rich off of

55
00:03:20,770 --> 00:03:22,000
 these things.

56
00:03:22,000 --> 00:03:30,000
 Because other people can be so dumb and would buy them.

57
00:03:30,000 --> 00:03:33,290
 So even monasteries became quite wealthy by having special

58
00:03:33,290 --> 00:03:34,000
 edition.

59
00:03:34,000 --> 00:03:39,960
 It was like those pong. You remember pong? Those little

60
00:03:39,960 --> 00:03:43,000
 milk bottle caps?

61
00:03:43,000 --> 00:03:49,000
 Collecting pong? I think it was called pong.

62
00:03:49,000 --> 00:03:52,860
 It became collectors items. And they were these round discs

63
00:03:52,860 --> 00:03:55,000
. They look like pong actually.

64
00:03:55,000 --> 00:03:59,670
 And you would collect them. And so they had special edition

65
00:03:59,670 --> 00:04:02,000
 and all of this garbage.

66
00:04:02,000 --> 00:04:06,650
 People say the same about Buddha images. They have this

67
00:04:06,650 --> 00:04:12,000
 huge ceremony to bless and to christen a Buddha image.

68
00:04:12,000 --> 00:04:16,370
 And make it powerful. And they put all sorts of spells over

69
00:04:16,370 --> 00:04:19,000
 it. All sorts of stuff.

70
00:04:19,000 --> 00:04:23,770
 Why I'm even talking about this is because there's really a

71
00:04:23,770 --> 00:04:26,000
 point that has to be made.

72
00:04:26,000 --> 00:04:29,000
 Yeah, I'm really kind of beating around the bush. I'm sorry

73
00:04:29,000 --> 00:04:29,000
.

74
00:04:29,000 --> 00:04:31,000
 But there's a point that has to be made here.

75
00:04:31,000 --> 00:04:37,950
 Is that the... Well, there's really a point there because

76
00:04:37,950 --> 00:04:44,000
 the Buddha never taught us to make images of anything.

77
00:04:44,000 --> 00:04:46,600
 You know, he wasn't critical. It was like, yeah, big deal.

78
00:04:46,600 --> 00:04:49,000
 Make an image. Don't make an image.

79
00:04:49,000 --> 00:04:53,000
 The question never came up.

80
00:04:53,000 --> 00:04:57,910
 There's a famous story and one that we always tell is that

81
00:04:57,910 --> 00:05:03,000
 the first Buddha images were Greek.

82
00:05:03,000 --> 00:05:08,850
 The records that we have of Buddhist India are that they

83
00:05:08,850 --> 00:05:12,000
 never made Buddha images.

84
00:05:12,000 --> 00:05:14,620
 They would never put the Buddha... And in fact, they

85
00:05:14,620 --> 00:05:16,000
 avoided it.

86
00:05:16,000 --> 00:05:18,730
 So when they would want it to tell a story and they would

87
00:05:18,730 --> 00:05:21,000
 inscribe it on rocks, the kings would do this.

88
00:05:21,000 --> 00:05:23,460
 Instead of putting the Buddha, they would put a wheel of

89
00:05:23,460 --> 00:05:27,000
 the Dhamma or they would put a Bodhi leaf.

90
00:05:27,000 --> 00:05:30,000
 Or they would put just an empty.

91
00:05:30,000 --> 00:05:34,120
 You know, when they want to show the Bodhisattva riding

92
00:05:34,120 --> 00:05:38,000
 this horse, they would put just the empty horse.

93
00:05:38,000 --> 00:05:44,000
 Because they would never... It was never done.

94
00:05:44,000 --> 00:05:53,430
 And so the point I'm trying to make is that we really have

95
00:05:53,430 --> 00:05:57,000
 a problem there in that we really are worshipping idols.

96
00:05:57,000 --> 00:06:01,000
 And that should really stop.

97
00:06:01,000 --> 00:06:06,540
 And of course, the defense that's always used that I haven

98
00:06:06,540 --> 00:06:11,610
't used is that we use the Buddha image in order to recol

99
00:06:11,610 --> 00:06:13,000
lect the Buddha.

100
00:06:13,000 --> 00:06:18,000
 Which I think can be a valid excuse.

101
00:06:18,000 --> 00:06:22,000
 But it sets you up for a lot of danger.

102
00:06:22,000 --> 00:06:28,090
 Especially when dealing with people who believe graven

103
00:06:28,090 --> 00:06:35,000
 images are sin and punishable by death or so on.

104
00:06:35,000 --> 00:06:39,650
 So, okay, so let's get back... Try to get back... Relate

105
00:06:39,650 --> 00:06:41,000
 this back to the question.

106
00:06:41,000 --> 00:06:50,000
 Because not all Muslims dislike us.

107
00:06:50,000 --> 00:06:58,630
 But it is true, I think, that there is definitely a bad

108
00:06:58,630 --> 00:07:09,000
 feeling between what is essentially an atheistic religion.

109
00:07:09,000 --> 00:07:16,000
 And the theistic religions.

110
00:07:16,000 --> 00:07:18,680
 I mean, Hinduism is, I guess, an exception. Because in

111
00:07:18,680 --> 00:07:22,000
 Hinduism, they don't exactly worship the gods.

112
00:07:22,000 --> 00:07:27,350
 They interact with them and there's general belief that one

113
00:07:27,350 --> 00:07:29,000
 can become God.

114
00:07:29,000 --> 00:07:32,010
 Judaism also doesn't have so much trouble. Because in

115
00:07:32,010 --> 00:07:36,000
 Judaism, they can argue with God.

116
00:07:36,000 --> 00:07:42,000
 God is a very much a part of the universe.

117
00:07:42,000 --> 00:07:46,960
 But he doesn't play the central role in what it means to be

118
00:07:46,960 --> 00:07:48,000
 Jewish.

119
00:07:48,000 --> 00:07:51,000
 So Jewish people can often come and practice meditation.

120
00:07:51,000 --> 00:07:54,900
 But I think there's no beating around the bush that Muslims

121
00:07:54,900 --> 00:08:01,000
, people who are practicing Muslims, do have a problem with

122
00:08:01,000 --> 00:08:01,000
...

123
00:08:01,000 --> 00:08:06,850
 Just as Christians do, with people who don't believe in God

124
00:08:06,850 --> 00:08:07,000
.

125
00:08:07,000 --> 00:08:14,230
 And that's aggravated when we take some graven image, or

126
00:08:14,230 --> 00:08:16,000
 idol as you say.

127
00:08:16,000 --> 00:08:24,000
 An idol as a substitute.

128
00:08:24,000 --> 00:08:27,000
 Because to them that's the ultimate blasphemy.

129
00:08:27,000 --> 00:08:30,020
 Putting a piece of rock on the same level as the most

130
00:08:30,020 --> 00:08:35,000
 important thing in the universe, which is their God.

131
00:08:35,000 --> 00:08:39,340
 So, regardless of... I mean, obviously it's a silly thing

132
00:08:39,340 --> 00:08:42,890
 to do for us to be paying so much attention to these images

133
00:08:42,890 --> 00:08:43,000
.

134
00:08:43,000 --> 00:08:47,410
 But there's a very valid point there that it's going to get

135
00:08:47,410 --> 00:08:50,750
 us into a lot of heat with those people who feel very

136
00:08:50,750 --> 00:08:52,000
 strongly with God.

137
00:08:52,000 --> 00:08:54,000
 Unnecessarily.

138
00:08:54,000 --> 00:08:57,010
 I think... And why this is actually an important point is

139
00:08:57,010 --> 00:08:59,440
 because this is part of what helped wipe out Buddhism in

140
00:08:59,440 --> 00:09:00,000
 India.

141
00:09:00,000 --> 00:09:03,160
 When the Muslims came to India, and they weren't... It wasn

142
00:09:03,160 --> 00:09:06,130
't just because they were Muslims, but because they were

143
00:09:06,130 --> 00:09:08,000
 militaristic sort of people.

144
00:09:08,000 --> 00:09:12,000
 But Muslim... Islam told them that this was wrong.

145
00:09:12,000 --> 00:09:15,080
 And so it was one of the first things to go with these idol

146
00:09:15,080 --> 00:09:19,990
 worshipers, which were the Buddhists who had their Buddha

147
00:09:19,990 --> 00:09:21,000
 images.

148
00:09:21,000 --> 00:09:25,000
 Hinduism was much better... much more like a chameleon.

149
00:09:25,000 --> 00:09:28,000
 Much better to fit... easier to fit in.

150
00:09:28,000 --> 00:09:32,400
 And much harder to wipe out. But Buddhism is pretty easy to

151
00:09:32,400 --> 00:09:33,000
 wipe out.

152
00:09:33,000 --> 00:09:37,940
 Especially when you have these Buddha images and temples

153
00:09:37,940 --> 00:09:39,000
 and so on.

154
00:09:39,000 --> 00:09:43,740
 If monks were just people wearing rags living in the forest

155
00:09:43,740 --> 00:09:47,000
, I think it'd be a lot harder to wipe them out.

156
00:09:47,000 --> 00:09:52,910
 But when they're sitting ducks, you know, getting fat off

157
00:09:52,910 --> 00:10:00,440
 of donations living in opulent golden palaces, not a hard

158
00:10:00,440 --> 00:10:03,000
 target to hit.

159
00:10:03,000 --> 00:10:10,230
 And I think that can be extrapolated to a more modern

160
00:10:10,230 --> 00:10:12,000
 example.

161
00:10:12,000 --> 00:10:18,780
 Buddhism is not making much headway on a monastic level

162
00:10:18,780 --> 00:10:22,000
 into the rest of the world.

163
00:10:22,000 --> 00:10:27,190
 Because Buddhists, when they build a Buddhist monastery,

164
00:10:27,190 --> 00:10:29,360
 first of all they call it a temple and they have their

165
00:10:29,360 --> 00:10:30,000
 priests.

166
00:10:32,000 --> 00:10:36,340
 And second of all, the way they go about it, for instance

167
00:10:36,340 --> 00:10:41,000
 time monasteries, they build it out of gold, as much gold

168
00:10:41,000 --> 00:10:42,000
 as possible.

169
00:10:42,000 --> 00:10:46,780
 And rich beautiful carpets and huge Buddha images and lots

170
00:10:46,780 --> 00:10:49,000
 of ceremonies and so on.

171
00:10:49,000 --> 00:11:00,000
 It's an affront really to people's sensibilities.

172
00:11:00,000 --> 00:11:04,000
 People go there and they're certainly not looking for that.

173
00:11:04,000 --> 00:11:08,330
 It's making it very difficult for us to bring Buddhism to

174
00:11:08,330 --> 00:11:10,000
 people by...

175
00:11:10,000 --> 00:11:15,280
 It's not just overkill, it's total misrepresentation of the

176
00:11:15,280 --> 00:11:17,000
 Buddha's teaching.

177
00:11:17,000 --> 00:11:21,060
 Because gold has no place in a monastery. A monk can't even

178
00:11:21,060 --> 00:11:22,000
 touch gold.

179
00:11:22,000 --> 00:11:26,880
 Touching gold is an offense. Touching jewels, touching

180
00:11:26,880 --> 00:11:32,000
 anything, any precious material like that is an offense.

181
00:11:32,000 --> 00:11:40,000
 So it's a real danger.

182
00:11:40,000 --> 00:11:45,580
 And I think the idols, the Buddha images are a part of that

183
00:11:45,580 --> 00:11:46,000
.

184
00:11:46,000 --> 00:11:49,000
 We don't need Buddha images to spread Buddhism.

185
00:11:51,000 --> 00:11:54,000
 It's the Dhamma which will spread Buddhism.

186
00:11:54,000 --> 00:11:58,970
 And yes, you have the excuse that it helps us to remember

187
00:11:58,970 --> 00:12:02,000
 the Buddha and think about him.

188
00:12:02,000 --> 00:12:06,020
 So I'm not saying that a Buddha image is totally negative,

189
00:12:06,020 --> 00:12:12,000
 but our obsession with them and our placing them.

190
00:12:12,000 --> 00:12:16,870
 Because if in that case, well why not have a Buddha image

191
00:12:16,870 --> 00:12:20,000
 placed somewhere in its own place?

192
00:12:21,000 --> 00:12:23,290
 Instead of putting it up in front of an altar for you to

193
00:12:23,290 --> 00:12:26,000
 worship and give donations to and so on.

194
00:12:26,000 --> 00:12:33,850
 You have it up as a reminder or something that you can go

195
00:12:33,850 --> 00:12:36,000
 and look at.

196
00:12:36,000 --> 00:12:43,000
 Whatever. It's certainly not the most important thing.

197
00:12:43,000 --> 00:12:47,220
 The Buddha's Rupa-caya was not his most important caya, his

198
00:12:47,220 --> 00:12:49,000
 most important body.

199
00:12:51,000 --> 00:12:53,840
 I had something else to say, but I think that's saying

200
00:12:53,840 --> 00:12:56,000
 quite a bit already.

201
00:12:56,000 --> 00:13:06,000
 There's the danger in the Buddha image.

202
00:13:06,000 --> 00:13:09,290
 I mean it's really an affront to people who believe other

203
00:13:09,290 --> 00:13:10,000
 things.

204
00:13:10,000 --> 00:13:13,820
 And that's really the point, is we don't have to be so

205
00:13:13,820 --> 00:13:16,000
 obvious about who we are.

206
00:13:16,000 --> 00:13:21,680
 As I said, a monk living in the forest really is not a

207
00:13:21,680 --> 00:13:24,000
 threat to anyone.

208
00:13:24,000 --> 00:13:26,570
 They still might go out and kill them because, oh, these

209
00:13:26,570 --> 00:13:28,000
 people don't believe in God.

210
00:13:28,000 --> 00:13:37,000
 But there's not so much of a problem there.

211
00:13:37,000 --> 00:13:41,100
 The other part of the answer, I guess, that I should say is

212
00:13:41,100 --> 00:13:43,000
 it's because these people have wrong views.

213
00:13:43,000 --> 00:13:49,410
 And when people have wrong views, they tend to be judgment

214
00:13:49,410 --> 00:13:52,000
al, but here I am judging them.

215
00:13:52,000 --> 00:13:55,770
 But as people who hold onto views, they believe this is

216
00:13:55,770 --> 00:13:57,000
 right and nothing else is right.

217
00:13:57,000 --> 00:14:03,000
 And so they're apt to criticize anyone who just disagrees.

218
00:14:03,000 --> 00:14:06,000
 They're not just criticized. They might even persecute.

219
00:14:06,000 --> 00:14:08,000
 It's not just the Muslims.

220
00:14:08,000 --> 00:14:11,080
 The Christians were horrible about it, the Spanish

221
00:14:11,080 --> 00:14:13,000
 Inquisition, the Crusades.

222
00:14:13,000 --> 00:14:16,000
 It has to do with belief.

223
00:14:16,000 --> 00:14:20,140
 The stronger your belief, the less tolerant you are of

224
00:14:20,140 --> 00:14:22,000
 people who disagree.

225
00:14:22,000 --> 00:14:27,000
 So the word atheist is just a shock to people.

226
00:14:27,000 --> 00:14:30,000
 It's a four-letter word.

