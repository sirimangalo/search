1
00:00:00,000 --> 00:00:04,000
 [silence]

2
00:00:04,000 --> 00:00:08,000
 Is anybody still here listening?

3
00:00:08,000 --> 00:00:12,000
 I'm late tonight.

4
00:00:12,000 --> 00:00:16,000
 Broadcasting live, February 15th.

5
00:00:16,000 --> 00:00:20,000
 Looks like a lot of people have already

6
00:00:20,000 --> 00:00:24,000
 gone home.

7
00:00:24,000 --> 00:00:28,000
 [silence]

8
00:00:28,000 --> 00:00:32,000
 [silence]

9
00:00:32,000 --> 00:00:36,000
 [silence]

10
00:00:36,000 --> 00:00:40,000
 [silence]

11
00:00:40,000 --> 00:00:44,000
 Anybody still here?

12
00:00:44,000 --> 00:00:48,000
 [silence]

13
00:00:48,000 --> 00:00:52,000
 Anybody listening?

14
00:00:52,000 --> 00:00:56,000
 [silence]

15
00:00:56,000 --> 00:01:00,000
 [silence]

16
00:01:00,000 --> 00:01:04,000
 [silence]

17
00:01:04,000 --> 00:01:08,000
 [silence]

18
00:01:08,000 --> 00:01:12,000
 [silence]

19
00:01:12,000 --> 00:01:16,000
 [silence]

20
00:01:16,000 --> 00:01:20,000
 [silence]

21
00:01:20,000 --> 00:01:24,000
 [silence]

22
00:01:24,000 --> 00:01:28,000
 [silence]

23
00:01:28,000 --> 00:01:32,000
 [silence]

24
00:01:32,000 --> 00:01:36,000
 [silence]

25
00:01:36,000 --> 00:01:40,000
 Hello? Is anybody out there?

26
00:01:40,000 --> 00:01:44,000
 [silence]

27
00:01:44,000 --> 00:01:48,000
 It's live. I'm live. I'm live here.

28
00:01:48,000 --> 00:01:52,000
 I'm assuming you're hearing it live.

29
00:01:52,000 --> 00:01:56,000
 [silence]

30
00:01:56,000 --> 00:02:00,000
 [silence]

31
00:02:00,000 --> 00:02:04,000
 Meditation is all about here and now.

32
00:02:04,000 --> 00:02:08,000
 So I'm live here. You're live there.

33
00:02:08,000 --> 00:02:12,000
 Here and now is interesting.

34
00:02:12,000 --> 00:02:16,000
 Here and now. We very rarely live in the here and now.

35
00:02:16,000 --> 00:02:20,000
 We think about it, how much of our time is there?

36
00:02:20,000 --> 00:02:24,000
 And think about it, how much of our time is spent?

37
00:02:24,000 --> 00:02:28,000
 Running around in circles.

38
00:02:28,000 --> 00:02:32,000
 Running through

39
00:02:32,000 --> 00:02:36,000
 racing. My mind is racing all the time.

40
00:02:36,000 --> 00:02:40,000
 Working overtime.

41
00:02:40,000 --> 00:02:44,000
 Even when we relax

42
00:02:44,000 --> 00:02:48,000
 we begin to daydream more.

43
00:02:48,000 --> 00:02:52,000
 We

44
00:02:52,000 --> 00:02:56,000
 indulge in

45
00:02:56,000 --> 00:03:00,000
 entertainment or conversation with others.

46
00:03:00,000 --> 00:03:04,000
 It's very hard to be in the here and now.

47
00:03:04,000 --> 00:03:08,000
 It's honestly

48
00:03:08,000 --> 00:03:12,000
 not hard in the sense of troublesome.

49
00:03:12,000 --> 00:03:16,000
 It's just very difficult.

50
00:03:16,000 --> 00:03:20,000
 And unless you're

51
00:03:20,000 --> 00:03:24,000
 really good at it, it's quite

52
00:03:24,000 --> 00:03:28,000
 painful. Not because of being in the present moment

53
00:03:28,000 --> 00:03:32,000
 is painful. It's actually quite liberating, right?

54
00:03:32,000 --> 00:03:36,000
 But when you're not doing it right and you're sitting there

55
00:03:36,000 --> 00:03:36,000
 suffering

56
00:03:36,000 --> 00:03:40,000
 forcing yourself to sit still,

57
00:03:40,000 --> 00:03:44,000
 forcing yourself to not

58
00:03:44,000 --> 00:03:48,000
 chase, not run away from the present moment

59
00:03:48,000 --> 00:03:52,000
 but when you can't actually be in the present moment

60
00:03:52,000 --> 00:03:55,580
 that's when it's difficult. Because being in the present

61
00:03:55,580 --> 00:03:56,000
 moment is liberating

62
00:03:56,000 --> 00:04:00,000
 but it also liberates our mind. It liberates

63
00:04:00,000 --> 00:04:04,000
 the deep dark parts of our minds.

64
00:04:04,000 --> 00:04:08,000
 Not even the deep, but just the dark parts.

65
00:04:08,000 --> 00:04:12,000
 So you find yourself accosted

66
00:04:12,000 --> 00:04:16,000
 by your emotions,

67
00:04:16,000 --> 00:04:20,000
 by your desires, by your aversions. Meditation tests you

68
00:04:20,000 --> 00:04:24,000
 being in the present moment.

69
00:04:24,000 --> 00:04:28,000
 It forces you to look at yourself.

70
00:04:28,000 --> 00:04:32,000
 Which, if you can stay in the present moment, actually isn

71
00:04:32,000 --> 00:04:32,000
't

72
00:04:32,000 --> 00:04:36,000
 troublesome. The problem is when you can't be in the

73
00:04:36,000 --> 00:04:36,000
 present moment

74
00:04:36,000 --> 00:04:40,000
 you are tossed and turned

75
00:04:40,000 --> 00:04:44,000
 by your emotions.

76
00:04:44,000 --> 00:04:47,000
 And when you say, "No, I'm not going to indulge in them. I

77
00:04:47,000 --> 00:04:48,000
'm going to sit here

78
00:04:48,000 --> 00:04:51,590
 and learn how to be in the present moment," that can be

79
00:04:51,590 --> 00:04:52,000
 very troublesome.

80
00:04:52,000 --> 00:04:56,000
 A lot of suffering.

81
00:04:56,000 --> 00:05:00,000
 That's why meditation can be

82
00:05:00,000 --> 00:05:04,000
 painful. Meditation itself is

83
00:05:04,000 --> 00:05:08,000
 truly freeing, liberating

84
00:05:08,000 --> 00:05:12,000
 it's the most peaceful, awesome thing.

85
00:05:12,000 --> 00:05:16,000
 But there are only two ways you can really get to

86
00:05:16,000 --> 00:05:20,000
 that awesomeness. One by forcing yourself into it

87
00:05:20,000 --> 00:05:24,000
 by forcing everything away.

88
00:05:24,000 --> 00:05:28,000
 So that living in the here and now

89
00:05:28,000 --> 00:05:32,000
 is just about one thing, one object, one

90
00:05:32,000 --> 00:05:36,000
 state of mind. That's samatha meditation.

91
00:05:36,000 --> 00:05:40,000
 Or you can...

92
00:05:40,000 --> 00:05:48,000
 Or you have to learn how to be flexible.

93
00:05:48,000 --> 00:05:52,000
 How to

94
00:05:52,000 --> 00:05:56,000
 practice the art, really. Like a martial art.

95
00:05:56,000 --> 00:06:00,000
 Being able to

96
00:06:00,000 --> 00:06:04,000
 deflect. Not even deflect, but diffuse

97
00:06:04,000 --> 00:06:08,000
 really disappear

98
00:06:08,000 --> 00:06:12,000
 I guess is the point. This Zen poem

99
00:06:12,000 --> 00:06:16,000
 "Here we sit the mountain and I until only the mountain

100
00:06:16,000 --> 00:06:20,000
 remained."

101
00:06:20,000 --> 00:06:24,000
 It's like that poem.

102
00:06:24,000 --> 00:06:28,000
 Really when you disappear, there's only the

103
00:06:28,000 --> 00:06:32,000
 experience. The experience goes right through you.

104
00:06:32,000 --> 00:06:36,000
 Someone attacks you but there's no you.

105
00:06:36,000 --> 00:06:40,000
 And who are they attacking? Who suffers from the attack?

106
00:06:40,000 --> 00:06:48,000
 So in meditation we

107
00:06:48,000 --> 00:06:52,000
 refocus on the present moment.

108
00:06:52,000 --> 00:06:56,000
 And you have to

109
00:06:56,000 --> 00:07:00,000
 constantly refocus yourself because the moment

110
00:07:00,000 --> 00:07:04,000
 constantly changes. It's like a camera lens that has to

111
00:07:04,000 --> 00:07:08,000
 focus as the subject moves.

112
00:07:08,000 --> 00:07:18,000
 You have to constantly realign yourself.

113
00:07:18,000 --> 00:07:20,800
 That's what the meditation is. You can't rest and that's

114
00:07:20,800 --> 00:07:22,000
 what we don't get in the beginning.

115
00:07:22,000 --> 00:07:26,000
 Our mindfowl know, "Oh, look at how mindfowl I am." And

116
00:07:26,000 --> 00:07:26,000
 then you stop being mindful.

117
00:07:26,000 --> 00:07:30,000
 And then you say, "Well, that was working for a moment but

118
00:07:30,000 --> 00:07:34,000
 now it's not working."

119
00:07:52,000 --> 00:07:56,000
 But it always works. Meditation is always possible.

120
00:07:56,000 --> 00:08:04,000
 Mindfulness, "Oh monks, I declare,"

121
00:08:04,000 --> 00:08:08,000
 is always useful.

122
00:08:08,000 --> 00:08:12,000
 There's no time or place where mindfulness isn't

123
00:08:12,000 --> 00:08:16,000
 useful.

124
00:08:16,000 --> 00:08:20,000
 I suppose an exception would be when you're unconscious or

125
00:08:20,000 --> 00:08:20,000
 asleep.

126
00:08:22,000 --> 00:08:26,000
 I've been barring that. There's no problem.

127
00:08:26,000 --> 00:08:30,000
 You could be in no situation. You could be in...

128
00:08:30,000 --> 00:08:33,190
 I don't know about drunk. I can't say as I've... I mean I

129
00:08:33,190 --> 00:08:34,000
 haven't drank

130
00:08:34,000 --> 00:08:38,000
 alcohol since I started being mindful.

131
00:08:38,000 --> 00:08:42,000
 I would still assume there's a potential to be mindful.

132
00:08:46,000 --> 00:08:50,000
 It wouldn't be easy.

133
00:08:50,000 --> 00:08:56,000
 It's probably still there.

134
00:09:12,000 --> 00:09:16,000
 So today's quote is actually, if you didn't recognize it,

135
00:09:16,000 --> 00:09:18,820
 it's actually what I went over when we talked about Anapam

136
00:09:18,820 --> 00:09:20,000
ma, the last quote on Anapamma Sati.

137
00:09:20,000 --> 00:09:24,000
 This is

138
00:09:24,000 --> 00:09:28,000
 Santo Jiva, where it says

139
00:09:28,000 --> 00:09:32,000
 is peaceful and excellent. Peaceful means Santo.

140
00:09:32,000 --> 00:09:36,000
 Jiva means and also.

141
00:09:36,000 --> 00:09:40,000
 Panito Ja and

142
00:09:40,000 --> 00:09:44,000
 that panito doesn't mean excellent. It means subtle or

143
00:09:44,000 --> 00:09:44,000
 refined.

144
00:09:44,000 --> 00:09:48,000
 It means refined. Not really excellent.

145
00:09:48,000 --> 00:09:54,000
 Asai Janaka Jha means not something. Why doesn't he

146
00:09:54,000 --> 00:09:58,000
 translate it properly? I mean he's doing okay.

147
00:09:58,000 --> 00:10:02,000
 For a purist, he could have done better than that.

148
00:10:02,000 --> 00:10:06,000
 Asai Janaka means it's a negative.

149
00:10:06,000 --> 00:10:10,000
 It's a lack of something. Asai Janaka.

150
00:10:10,000 --> 00:10:14,000
 Asai Janaka means like adulterated or mixed.

151
00:10:14,000 --> 00:10:18,000
 Asai Janaka means unmixed, unadulterated.

152
00:10:18,000 --> 00:10:24,000
 Meaning it's not caught up in the world.

153
00:10:24,000 --> 00:10:32,000
 Sukho Jyobi Haro, that's correct. Pleasant way of living.

154
00:10:32,000 --> 00:10:36,000
 Sukho, happiness, viharal,

155
00:10:36,000 --> 00:10:40,000
 dwelling, or happy dwelling.

156
00:10:40,000 --> 00:10:44,000
 Sukha Vihar.

157
00:11:00,000 --> 00:11:04,000
 Thanasa, at that moment

158
00:11:04,000 --> 00:11:08,000
 is kind of, I think, Thanasa.

159
00:11:08,000 --> 00:11:12,000
 Thanasur, not Thanasa, Thanasur.

160
00:11:12,000 --> 00:11:16,000
 Antara Dapai Dhi.

161
00:11:16,000 --> 00:11:20,000
 Evil Anhosum is Papakoh, Papakkei

162
00:11:20,000 --> 00:11:24,000
 Akhusudaydhammi. In regards to

163
00:11:24,000 --> 00:11:28,000
 evil Anhosum Dhammas,

164
00:11:28,000 --> 00:11:32,000
 Dhradaapai Dhi.

165
00:11:32,000 --> 00:11:36,000
 Dispeles them. Wupasumai Dhi makes them vanish.

166
00:11:36,000 --> 00:11:40,000
 Causes them to be tranquilized.

167
00:11:40,000 --> 00:11:48,000
 And you know the Buddha talked about how mindfulness of

168
00:11:48,000 --> 00:11:48,000
 breath is

169
00:11:48,000 --> 00:11:52,000
 very much about the four foundations of mindfulness.

170
00:11:52,000 --> 00:11:56,000
 So in terms of vipassana, this means watching the four

171
00:11:56,000 --> 00:11:56,000
 elements.

172
00:11:56,000 --> 00:12:00,000
 Whether it be the air element in the stomach,

173
00:12:00,000 --> 00:12:03,620
 which is the pressure, or whether it be the heat or the

174
00:12:03,620 --> 00:12:04,000
 cold at your lip.

175
00:12:04,000 --> 00:12:20,000
 It's a very good object for staying in the present moment

176
00:12:20,000 --> 00:12:20,000
 because it's mostly always there.

177
00:12:20,000 --> 00:12:24,000
 So you can become enlightened

178
00:12:24,000 --> 00:12:28,000
 just by watching your stomach. It's definitely possible.

179
00:12:28,000 --> 00:12:32,000
 Definitely an appropriate object.

180
00:12:32,000 --> 00:12:36,000
 But then aware to find enlightenment, find it in the rising

181
00:12:36,000 --> 00:12:36,000
 form of the stomach.

182
00:12:36,000 --> 00:12:40,000
 Focus on that for

183
00:12:40,000 --> 00:12:44,000
 some people, that's all it takes. There are some people who

184
00:12:44,000 --> 00:12:44,000
 just by

185
00:12:44,000 --> 00:12:48,000
 focusing on that, I would even guess that

186
00:12:48,000 --> 00:12:52,000
 there are some people who have,

187
00:12:52,000 --> 00:12:56,000
 I don't know if I'd go that far, but I bet someone by

188
00:12:56,000 --> 00:12:56,000
 following

189
00:12:56,000 --> 00:13:00,000
 the teachings

190
00:13:00,000 --> 00:13:04,000
 just in that booklet that I put out,

191
00:13:04,000 --> 00:13:08,000
 it would be rare, but I bet there are some people who could

192
00:13:08,000 --> 00:13:08,000
.

193
00:13:08,000 --> 00:13:12,000
 Whether they have or not, I don't know. But it is possible

194
00:13:12,000 --> 00:13:16,000
 someone is just so ready, and all they'd have to do is

195
00:13:16,000 --> 00:13:20,000
 start doing the walking, the sitting meditation,

196
00:13:20,000 --> 00:13:24,000
 they can become at least a sotah-vana.

197
00:13:24,000 --> 00:13:28,000
 Because it's all there. This isn't something

198
00:13:28,000 --> 00:13:32,000
 hard to find. It's not something, I mean,

199
00:13:32,000 --> 00:13:36,000
 something out of reach of many of us, most of us.

200
00:13:36,000 --> 00:13:40,000
 But it's all there. It's only out of reach because we have

201
00:13:40,000 --> 00:13:40,000
 a hard

202
00:13:40,000 --> 00:13:44,000
 time finding it and understanding it. Our minds are not

203
00:13:44,000 --> 00:13:44,000
 ready for it.

204
00:13:44,000 --> 00:13:48,000
 We're too caught up in sensuality.

205
00:13:48,000 --> 00:13:52,000
 For most of us we need a lot more than that. We need to

206
00:13:52,000 --> 00:13:52,000
 actually

207
00:13:52,000 --> 00:13:56,000
 take a course, spend some serious time meditating

208
00:13:56,000 --> 00:14:00,000
 before we can understand it. But it's all there.

209
00:14:00,000 --> 00:14:04,000
 The breath is enough.

210
00:14:04,000 --> 00:14:08,000
 The body and the mind, watching the body and the mind, with

211
00:14:08,000 --> 00:14:08,000
 the technique that lets

212
00:14:08,000 --> 00:14:12,000
 you do it and lets you stay objective. You can

213
00:14:12,000 --> 00:14:16,000
 become enlightened even in this life.

214
00:14:16,000 --> 00:14:20,000
 Does anybody have any questions

215
00:14:20,000 --> 00:14:24,000
 for me tonight? Otherwise I'm going to call it a night.

216
00:14:24,000 --> 00:14:32,000
 So here's some questions.

217
00:14:32,000 --> 00:14:44,000
 Meditation rather than recognizing the

218
00:14:44,000 --> 00:14:48,000
 defilements and waiting for them to subside, can you walk

219
00:14:48,000 --> 00:14:48,000
 around

220
00:14:48,000 --> 00:14:52,000
 and look for a place of freedom?

221
00:14:52,000 --> 00:14:56,000
 Well no, because freedom means freedom from the defilements

222
00:14:56,000 --> 00:14:56,000
.

223
00:14:56,000 --> 00:15:00,000
 If you're giving rise to defilements based on any

224
00:15:00,000 --> 00:15:04,000
 situation it means you're... I mean, that's the problem.

225
00:15:04,000 --> 00:15:08,000
 So the whole point is to experience the same things

226
00:15:08,000 --> 00:15:12,000
 as always without the defilements arising.

227
00:15:12,000 --> 00:15:16,000
 Something gives rise to greed or anger.

228
00:15:16,000 --> 00:15:20,000
 The task is to learn how to experience those

229
00:15:20,000 --> 00:15:24,000
 things without giving rise to greed or anger.

230
00:15:24,000 --> 00:15:28,000
 Rather than avoiding that thing. I mean you can avoid it,

231
00:15:28,000 --> 00:15:28,000
 but

232
00:15:28,000 --> 00:15:32,000
 it's not a long term solution.

233
00:15:32,000 --> 00:15:36,000
 [silence]

234
00:15:36,000 --> 00:15:48,000
 I'm not broadcasting on YouTube. I'm in my room.

235
00:15:48,000 --> 00:15:52,000
 Just got off a conference call with my family. We're doing

236
00:15:52,000 --> 00:15:52,000
 this

237
00:15:52,000 --> 00:15:56,000
 new thing where we call, we do a video call

238
00:15:56,000 --> 00:16:00,000
 for the whole family on everybody's birthday. So today was

239
00:16:00,000 --> 00:16:04,000
 actually tomorrow is my older brother's birthday.

240
00:16:04,000 --> 00:16:08,000
 He's in Taiwan so it's his birthday already for him.

241
00:16:08,000 --> 00:16:12,000
 [silence]

242
00:16:14,000 --> 00:16:18,000
 [silence]

243
00:16:20,000 --> 00:16:24,000
 [silence]

244
00:16:44,000 --> 00:16:48,000
 [silence]

245
00:16:48,000 --> 00:17:08,000
 How should we think about this?

246
00:17:08,000 --> 00:17:12,000
 I don't quite get the question.

247
00:17:14,000 --> 00:17:17,510
 Sometimes we have to strive, practice like our hair is on

248
00:17:17,510 --> 00:17:18,000
 fire

249
00:17:18,000 --> 00:17:22,000
 and sometimes we have to give everything up.

250
00:17:22,000 --> 00:17:24,870
 That's the whole point, we have to strive to give

251
00:17:24,870 --> 00:17:26,000
 everything up.

252
00:17:26,000 --> 00:17:30,000
 Not really two different things.

253
00:17:30,000 --> 00:17:34,000
 [silence]

254
00:17:36,000 --> 00:17:40,000
 [silence]

255
00:17:40,000 --> 00:17:44,000
 I don't want anything.

256
00:17:44,000 --> 00:17:48,000
 The progressions are turning away from wanting.

257
00:17:48,000 --> 00:17:52,000
 I don't know, I hope that what I wrote in

258
00:17:52,000 --> 00:17:56,000
 the second book, second volume on how to

259
00:17:56,000 --> 00:17:59,510
 meditate, the last chapter that I wrote, I was supposed to

260
00:17:59,510 --> 00:18:00,000
 sort of

261
00:18:00,000 --> 00:18:04,000
 make that clear that we're striving to give up wanting.

262
00:18:04,000 --> 00:18:08,000
 [silence]

263
00:18:08,000 --> 00:18:20,000
 Yeah, you should determine to obtain them.

264
00:18:20,000 --> 00:18:24,000
 Determination. Determination is sort of I guess

265
00:18:24,000 --> 00:18:28,000
 the answer to this question of how do you practice if you

266
00:18:28,000 --> 00:18:28,000
've got to give up wanting.

267
00:18:28,000 --> 00:18:31,420
 How do you want to practice? You don't really want to, you

268
00:18:31,420 --> 00:18:32,000
 just have determination.

269
00:18:32,000 --> 00:18:36,000
 Determination means the

270
00:18:36,000 --> 00:18:40,000
 intention which is usually based on wisdom and

271
00:18:40,000 --> 00:18:40,000
 understanding

272
00:18:40,000 --> 00:18:44,000
 and the importance and so on.

273
00:18:44,000 --> 00:18:48,000
 [silence]

274
00:18:48,000 --> 00:18:52,000
 [silence]

275
00:18:52,000 --> 00:19:10,000
 Okay. Thanks everyone for tuning in.

276
00:19:14,000 --> 00:19:18,000
 Maybe we'll do a demo pilot for tomorrow.

277
00:19:18,000 --> 00:19:22,000
 [silence]

278
00:19:22,000 --> 00:19:30,000
 Alright, have a good night.

279
00:19:32,000 --> 00:19:36,000
 [silence]

