1
00:00:00,000 --> 00:00:13,000
 So good evening.

2
00:00:13,000 --> 00:00:31,000
 I'm alive.

3
00:00:31,000 --> 00:00:35,000
 Oh, did everyone already leave yet?

4
00:00:35,000 --> 00:00:42,000
 I assigned him.

5
00:00:42,000 --> 00:00:57,000
 I just got back from our first event.

6
00:00:57,000 --> 00:01:14,000
 I taught meditation this afternoon.

7
00:01:14,000 --> 00:01:34,000
 I'm actually just getting in the door.

8
00:01:34,000 --> 00:01:38,530
 The noble disciple who is utterly devoted to and has unsh

9
00:01:38,530 --> 00:01:40,000
akable faith in the tathāgata

10
00:01:40,000 --> 00:01:45,950
 and have no doubt or wavering concerning the tathāgata or

11
00:01:45,950 --> 00:01:48,000
 his teachings.

12
00:01:48,000 --> 00:01:53,350
 And it may be expected that such a disciple will live res

13
00:01:53,350 --> 00:01:54,000
olute in energy,

14
00:01:54,000 --> 00:01:59,150
 always striving to abandon bad qualities and develop good

15
00:01:59,150 --> 00:02:00,000
 ones,

16
00:02:00,000 --> 00:02:09,220
 and that he will be energetic in exerting himself and will

17
00:02:09,220 --> 00:02:12,000
 not drop good.

18
00:02:12,000 --> 00:02:16,000
 So this is how we...

19
00:02:16,000 --> 00:02:19,000
 This is what we work for.

20
00:02:19,000 --> 00:02:23,000
 We work for the good.

21
00:02:23,000 --> 00:02:30,070
 We work good with body, good with speech, good with mind,

22
00:02:30,070 --> 00:02:37,000
 good for ourselves, good for others.

23
00:02:37,000 --> 00:02:58,000
 Thinking only about welfare, goodness.

24
00:02:58,000 --> 00:03:04,000
 Well, the big city is...

25
00:03:04,000 --> 00:03:07,450
 I don't know, they say wherever you go there you are, I don

26
00:03:07,450 --> 00:03:10,000
't really see that much of a difference.

27
00:03:10,000 --> 00:03:15,140
 More seeing, more hearing, more smelling, more tasting,

28
00:03:15,140 --> 00:03:18,000
 more feeling, more thinking.

29
00:03:18,000 --> 00:03:25,000
 I met a nice person today, Giovanni, an Italian woman who's

30
00:03:25,000 --> 00:03:28,000
 sort of running the Buddhist Insights group

31
00:03:28,000 --> 00:03:40,000
 with Fante, Sudasso.

32
00:03:40,000 --> 00:03:43,000
 She invited us to lunch.

33
00:03:43,000 --> 00:03:49,000
 And then we went to her apartment.

34
00:03:49,000 --> 00:03:53,000
 And I did this evening's interviews, meditation interviews

35
00:03:53,000 --> 00:03:57,000
 in her apartment with her Wi-Fi.

36
00:03:57,000 --> 00:04:03,000
 And then I came home.

37
00:04:03,000 --> 00:04:07,000
 Oh, the Danish booklet, well that's nice.

38
00:04:07,000 --> 00:04:09,000
 I need a...

39
00:04:09,000 --> 00:04:12,070
 I can't do anything with it, don't send it to me until I

40
00:04:12,070 --> 00:04:17,000
 get back to Hamilton and back to Canada.

41
00:04:17,000 --> 00:04:24,860
 Once I get back to Canada, then you can email it to me in

42
00:04:24,860 --> 00:04:29,000
 either ODT or DOC format.

43
00:04:29,000 --> 00:04:32,150
 ODT is actually better if you're using Libra Office or

44
00:04:32,150 --> 00:04:33,000
 something.

45
00:04:33,000 --> 00:04:42,000
 No, I can't take a PDF. I need the actual doc.

46
00:04:42,000 --> 00:04:48,000
 You can send me the doc or docx or ODT or something.

47
00:04:48,000 --> 00:04:52,000
 And format it if you please.

48
00:04:52,000 --> 00:05:02,000
 Make it look nice.

49
00:05:02,000 --> 00:05:07,520
 Anyway, I'm going to go. It's been a bit of a long day, a

50
00:05:07,520 --> 00:05:10,000
 lot of traveling.

51
00:05:10,000 --> 00:05:14,000
 Off the subway. We're going to Subway for breakfast.

52
00:05:14,000 --> 00:05:21,270
 There's a subway close by, so I can use the Canadian gift

53
00:05:21,270 --> 00:05:24,000
 cards in America.

54
00:05:24,000 --> 00:05:28,000
 So I'm inviting the other monk, Sudasso here.

55
00:05:28,000 --> 00:05:32,000
 Invite him for breakfast tomorrow.

56
00:05:32,000 --> 00:05:35,000
 And then tomorrow evening at seven.

57
00:05:35,000 --> 00:05:40,210
 There's our next session and so because it's at seven, I'm

58
00:05:40,210 --> 00:05:42,000
 not sure if I'll be back by nine.

59
00:05:42,000 --> 00:05:45,000
 But we will see.

60
00:05:45,000 --> 00:05:52,000
 Anyway, have a good night everyone. Thanks for tuning in.

61
00:05:52,000 --> 00:05:54,000
 Good night.

