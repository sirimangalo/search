1
00:00:00,000 --> 00:00:17,000
 [silence]

2
00:00:17,000 --> 00:00:20,000
 I'm alive.

3
00:00:20,000 --> 00:00:33,000
 Good morning.

4
00:00:33,000 --> 00:00:43,000
 The Buddha taught.

5
00:00:43,000 --> 00:00:53,000
 He said, "Mā bhikkhuī anātau viharatā."

6
00:00:53,000 --> 00:00:58,000
 Don't live without a refuge.

7
00:00:58,000 --> 00:01:07,000
 "Dukkho bhikkhuī anātau viharatī."

8
00:01:07,000 --> 00:01:32,000
 Someone who is without refuge lives in suffering.

9
00:01:32,000 --> 00:01:35,970
 I don't think this is hard for us to appreciate or

10
00:01:35,970 --> 00:01:37,000
 understand

11
00:01:37,000 --> 00:01:41,000
 something that should be familiar to many of us.

12
00:01:41,000 --> 00:01:45,000
 Those of us who have been in our lives without refuge

13
00:01:45,000 --> 00:01:54,000
 understand how unpleasant it can be.

14
00:01:54,000 --> 00:02:01,000
 It's true, for many people,

15
00:02:01,000 --> 00:02:13,000
 there doesn't seem such an urgent need to find protection.

16
00:02:13,000 --> 00:02:21,000
 At times life can be pleasant, stable, peaceful.

17
00:02:21,000 --> 00:02:25,000
 And for those people at those times,

18
00:02:25,000 --> 00:02:29,860
 it seems like there's not much need for any kind of

19
00:02:29,860 --> 00:02:30,000
 protection

20
00:02:30,000 --> 00:02:36,000
 or refuge.

21
00:02:36,000 --> 00:02:45,000
 It's only when suffering hits, calamity, disaster,

22
00:02:45,000 --> 00:03:00,000
 accident, loss, sickness, conflict,

23
00:03:00,000 --> 00:03:06,000
 then we search for something to protect us from that,

24
00:03:06,000 --> 00:03:13,000
 or we wish we'd had something that would protect us.

25
00:03:13,000 --> 00:03:16,000
 Mostly of course we look outside,

26
00:03:16,000 --> 00:03:19,000
 we look for an external refuge.

27
00:03:19,000 --> 00:03:26,000
 And it's not that there isn't any external refuge.

28
00:03:26,000 --> 00:03:31,000
 The Buddha himself is our refuge,

29
00:03:31,000 --> 00:03:35,000
 "Buddhāng sarananga ca chāmi."

30
00:03:35,000 --> 00:03:38,000
 I go to the Buddha for refuge.

31
00:03:38,000 --> 00:03:43,000
 The Dhamma is our refuge, "Dhammāng sarananga ca chāmi."

32
00:03:43,000 --> 00:03:48,000
 I go to the Dhamma for refuge.

33
00:03:48,000 --> 00:03:53,000
 The Sangha is our refuge, "Sankhāng sarananga ca chāmi."

34
00:03:53,000 --> 00:03:59,000
 I go to the Sangha for refuge.

35
00:03:59,000 --> 00:04:06,000
 But the Buddha also said, "Atahiyatanona toa."

36
00:04:06,000 --> 00:04:13,000
 One is one's own refuge.

37
00:04:13,000 --> 00:04:16,000
 And the Buddha helps, the Dhamma helps, the Sangha helps,

38
00:04:16,000 --> 00:04:29,000
 but really mostly only because we help ourselves.

39
00:04:29,000 --> 00:04:34,420
 The Dhamma, the Sangha, the Buddha help us to help

40
00:04:34,420 --> 00:04:37,000
 ourselves,

41
00:04:37,000 --> 00:04:44,000
 give us the tools for us to do the work.

42
00:04:44,000 --> 00:04:49,000
 Because there are many dangers in the world

43
00:04:49,000 --> 00:04:58,510
 that no external support or help or refuge can protect us

44
00:04:58,510 --> 00:05:00,000
 from.

45
00:05:00,000 --> 00:05:07,000
 No parent or guardian, no relative can keep us,

46
00:05:07,000 --> 00:05:13,050
 relative or friend can keep us from so many types of

47
00:05:13,050 --> 00:05:14,000
 suffering,

48
00:05:14,000 --> 00:05:28,000
 so many sources of danger, externally sickness, pain,

49
00:05:28,000 --> 00:05:33,000
 and even more internally.

50
00:05:33,000 --> 00:05:39,000
 No one can keep us from greed, anger, delusion,

51
00:05:39,000 --> 00:05:50,000
 from depression, anxiety, fear, addiction.

52
00:05:50,000 --> 00:06:02,000
 There's no greater refuge than our own mind.

53
00:06:02,000 --> 00:06:07,500
 There's no other refuge, there's no real refuge outside of

54
00:06:07,500 --> 00:06:09,000
 our own mind.

55
00:06:09,000 --> 00:06:16,000
 And so when the Buddha said, "Don't live without a refuge,"

56
00:06:16,000 --> 00:06:22,000
 he was mainly talking about making yourself a refuge,

57
00:06:22,000 --> 00:06:37,000
 having the foresight to prepare yourself for danger.

58
00:06:37,000 --> 00:06:43,000
 Not in the sense of anticipating danger,

59
00:06:43,000 --> 00:06:51,000
 like sitting around waiting for danger to come.

60
00:06:51,000 --> 00:07:00,000
 But changing your situation from one that is vulnerable

61
00:07:00,000 --> 00:07:03,000
 so that your happiness depends on circumstance.

62
00:07:03,000 --> 00:07:07,480
 "If things are like this, I'll be happy as long as they

63
00:07:07,480 --> 00:07:09,000
 stay like this,

64
00:07:09,000 --> 00:07:13,000
 as long as they aren't like that."

65
00:07:13,000 --> 00:07:18,000
 That's dependent, that's vulnerable.

66
00:07:18,000 --> 00:07:22,000
 It's one that is independent and invulnerable.

67
00:07:22,000 --> 00:07:30,000
 Whether things are like this or like that,

68
00:07:30,000 --> 00:07:35,000
 I am at peace.

69
00:07:35,000 --> 00:07:40,000
 That's invulnerable.

70
00:07:40,000 --> 00:07:43,000
 So how do we make ourselves a refuge?

71
00:07:43,000 --> 00:07:52,000
 How do we gain this invulnerability, this independence?

72
00:07:52,000 --> 00:07:58,410
 Well again, having a refuge doesn't just mean internal

73
00:07:58,410 --> 00:07:59,000
 support.

74
00:07:59,000 --> 00:08:04,000
 The best refuge is your own mind.

75
00:08:04,000 --> 00:08:08,770
 But part of that involves the refuge that comes from the

76
00:08:08,770 --> 00:08:10,000
 support of others.

77
00:08:10,000 --> 00:08:18,000
 The support of others is also dependent on our mind.

78
00:08:18,000 --> 00:08:22,000
 Whether we are the sort of person worth protecting,

79
00:08:22,000 --> 00:08:26,000
 worth supporting,

80
00:08:26,000 --> 00:08:31,000
 whether other people feel comfortable supporting us.

81
00:08:31,000 --> 00:08:44,000
 The Buddha listed ten ways of creating a refuge,

82
00:08:44,000 --> 00:08:58,000
 ten dhammas for building a refuge.

83
00:08:58,000 --> 00:09:07,000
 They're called the Nātakar nadāmā.

84
00:09:07,000 --> 00:09:13,000
 The first one is the śīla, of course.

85
00:09:13,000 --> 00:09:19,000
 So many of the lists start with śīla, morality, ethics.

86
00:09:19,000 --> 00:09:22,000
 This is a great refuge.

87
00:09:22,000 --> 00:09:38,080
 It's a great description of śīla, a great explanation of

88
00:09:38,080 --> 00:09:39,000
 the importance of śīla,

89
00:09:39,000 --> 00:09:42,000
 that it's a refuge.

90
00:09:42,000 --> 00:09:44,000
 śīla is not a burden.

91
00:09:44,000 --> 00:09:47,410
 It's not a burden to stop killing and stealing and lying

92
00:09:47,410 --> 00:09:48,000
 and cheating

93
00:09:48,000 --> 00:09:54,160
 and taking drugs, alcohol, hurting others, speaking harsh

94
00:09:54,160 --> 00:09:56,000
 words and so on.

95
00:09:56,000 --> 00:09:58,000
 It isn't a burden.

96
00:09:58,000 --> 00:10:05,260
 It's a great boon if we are able even to just know these

97
00:10:05,260 --> 00:10:06,000
 things,

98
00:10:06,000 --> 00:10:22,060
 even to have a sense of the importance of abstaining from

99
00:10:22,060 --> 00:10:24,000
 reckless behavior and speech,

100
00:10:24,000 --> 00:10:29,000
 harmful behavior and speech.

101
00:10:29,000 --> 00:10:48,000
 It's a great refuge.

102
00:10:48,000 --> 00:10:56,090
 It's a refuge, of course, because by breaking precepts, by

103
00:10:56,090 --> 00:10:59,000
 doing things that are,

104
00:10:59,000 --> 00:11:03,060
 well, first of all, immoral and unethical, of course, you

105
00:11:03,060 --> 00:11:05,000
're going to get into trouble.

106
00:11:05,000 --> 00:11:15,100
 You get into trouble with the law, but more practically,

107
00:11:15,100 --> 00:11:17,000
 you get in trouble with,

108
00:11:17,000 --> 00:11:23,190
 people, on a very basic level, you get in trouble in your

109
00:11:23,190 --> 00:11:24,000
 own mind,

110
00:11:24,000 --> 00:11:32,880
 feeling guilty, cultivating habits of anger and greed and

111
00:11:32,880 --> 00:11:36,000
 crookedness,

112
00:11:36,000 --> 00:11:39,000
 real crookedness.

113
00:11:39,000 --> 00:11:41,000
 You don't think straight anymore.

114
00:11:41,000 --> 00:11:46,180
 You think in terms of manipulating others, not in terms of

115
00:11:46,180 --> 00:11:49,000
 bringing peace and harmony.

116
00:11:49,000 --> 00:11:54,000
 Your sense of good is skewed. Everything is crooked.

117
00:11:54,000 --> 00:11:58,000
 It's a great danger.

118
00:11:58,000 --> 00:12:00,990
 śīla is a very important part of refuge. It's a very

119
00:12:00,990 --> 00:12:03,000
 important refuge.

120
00:12:03,000 --> 00:12:12,000
 It's a very stable refuge, something we can depend on.

121
00:12:12,000 --> 00:12:17,000
 The second one is bahū sūta.

122
00:12:17,000 --> 00:12:24,000
 Bahu māch sūta, literally, hearing.

123
00:12:24,000 --> 00:12:28,000
 Let's say learning is what it really means.

124
00:12:28,000 --> 00:12:33,060
 In the Buddha's time, everything was learned through voice,

125
00:12:33,060 --> 00:12:36,000
 very little writing.

126
00:12:36,000 --> 00:12:41,220
 Even today, you see, much of the dhamma has come full

127
00:12:41,220 --> 00:12:42,000
 circle.

128
00:12:42,000 --> 00:12:49,000
 For a long time, it was all written.

129
00:12:49,000 --> 00:12:54,000
 Everything would have to be written down.

130
00:12:54,000 --> 00:12:58,000
 In terms of this mass production of dhamma,

131
00:12:58,000 --> 00:13:03,190
 when I learned the dhamma, it was all, in the beginning, it

132
00:13:03,190 --> 00:13:05,000
 was all oral as well.

133
00:13:05,000 --> 00:13:09,390
 Go to a meditation center, the teachings you get are all

134
00:13:09,390 --> 00:13:10,000
 oral,

135
00:13:10,000 --> 00:13:17,000
 someone explaining it to you.

136
00:13:17,000 --> 00:13:23,740
 But we never had this potential to speak, to record our

137
00:13:23,740 --> 00:13:25,000
 voice.

138
00:13:25,000 --> 00:13:30,000
 Now it's very easy to find recorded teachings.

139
00:13:30,000 --> 00:13:33,000
 But the meaning is learning.

140
00:13:33,000 --> 00:13:40,330
 Learning the dhamma from speech or from printed word, it

141
00:13:40,330 --> 00:13:43,000
 doesn't matter.

142
00:13:43,000 --> 00:13:49,000
 Bahū sūta means having much learning.

143
00:13:49,000 --> 00:13:52,640
 Now learning is, of course, something we trivialize to some

144
00:13:52,640 --> 00:13:54,000
 extent in Buddhism.

145
00:13:54,000 --> 00:13:57,000
 Don't rely on learning.

146
00:13:57,000 --> 00:14:01,000
 Don't confuse learning with understanding.

147
00:14:01,000 --> 00:14:04,000
 It's very different.

148
00:14:04,000 --> 00:14:06,980
 You can know everything about the dhamma and know nothing

149
00:14:06,980 --> 00:14:10,000
 about the dhamma at the same time.

150
00:14:10,000 --> 00:14:17,000
 Two very different things.

151
00:14:17,000 --> 00:14:20,000
 But that being said, it is a great refuge.

152
00:14:20,000 --> 00:14:24,000
 Without any learning, you'd have no dhamma.

153
00:14:24,000 --> 00:14:27,000
 You'd have no practice.

154
00:14:27,000 --> 00:14:31,160
 Practice is not something you can find on your own, without

155
00:14:31,160 --> 00:14:33,000
 the learning.

156
00:14:33,000 --> 00:14:35,630
 Either you spend all the countless lifetimes the Buddha

157
00:14:35,630 --> 00:14:39,000
 spent learning for yourself

158
00:14:39,000 --> 00:14:52,000
 or you listen to someone who has been there.

159
00:14:52,000 --> 00:15:02,000
 And more importantly, the much learning,

160
00:15:02,000 --> 00:15:13,270
 is that learning gives you a framework within which to grow

161
00:15:13,270 --> 00:15:14,000
.

162
00:15:14,000 --> 00:15:18,000
 You don't need to learn a lot to just practice.

163
00:15:18,000 --> 00:15:24,000
 But to grow and to keep growing, you need a framework.

164
00:15:24,000 --> 00:15:27,000
 Many of the things we learn in the dhamma are not

165
00:15:27,000 --> 00:15:31,000
 immediately applicable.

166
00:15:31,000 --> 00:15:34,160
 Even you learn about these ten nattak karana dhamma, you

167
00:15:34,160 --> 00:15:35,000
 learn about all these things.

168
00:15:35,000 --> 00:15:38,000
 Some of them won't immediately be practical.

169
00:15:38,000 --> 00:15:43,260
 But when you remember them, when you come back to them,

170
00:15:43,260 --> 00:15:47,000
 then you hear them again and again.

171
00:15:47,000 --> 00:15:51,580
 Even just reminding yourself of them, having them in your

172
00:15:51,580 --> 00:15:56,000
 mind, allows you to grow into them.

173
00:15:56,000 --> 00:16:01,050
 Learning is like that, memorizing. Memorizing the Buddha's

174
00:16:01,050 --> 00:16:02,000
 teaching is great.

175
00:16:02,000 --> 00:16:05,170
 If you're able to distinguish between what you know and

176
00:16:05,170 --> 00:16:09,000
 what you only heard,

177
00:16:09,000 --> 00:16:13,000
 it can be a great protection, a great refuge.

178
00:16:13,000 --> 00:16:16,000
 Danger is when you confuse them, of course.

179
00:16:16,000 --> 00:16:26,000
 You learn everything and then think you know everything.

180
00:16:26,000 --> 00:16:28,000
 Nonetheless, we should learn.

181
00:16:28,000 --> 00:16:33,080
 If we're able, we should learn and we should take care to

182
00:16:33,080 --> 00:16:39,000
 distinguish what we know and what we don't know.

183
00:16:39,000 --> 00:16:43,910
 We can grow into what we don't yet know, the things we've

184
00:16:43,910 --> 00:16:48,000
 heard that we don't yet know for ourselves.

185
00:16:48,000 --> 00:16:56,000
 Suta, bahu suta. Number three is kalyanamita.

186
00:16:56,000 --> 00:17:03,000
 One should be kalyanamito, kalyanamito hoti.

187
00:17:03,000 --> 00:17:12,000
 One should be a good friend, one should have good friends.

188
00:17:12,000 --> 00:17:17,000
 Being a good friend is a great refuge.

189
00:17:17,000 --> 00:17:22,000
 People appreciate when you're friendly.

190
00:17:22,000 --> 00:17:31,000
 Tvamita is so praised by the Buddha.

191
00:17:31,000 --> 00:17:39,000
 Friendliness makes for harmony and peace and a great refuge

192
00:17:39,000 --> 00:17:39,000
.

193
00:17:39,000 --> 00:17:43,000
 And we should surround ourselves with good friends,

194
00:17:43,000 --> 00:17:48,000
 try and associate with people who have good qualities,

195
00:17:48,000 --> 00:17:52,250
 try and incline our minds towards cultivating friendship

196
00:17:52,250 --> 00:17:54,000
 with good people.

197
00:17:54,000 --> 00:18:02,000
 Kalyanamita means beautiful.

198
00:18:02,000 --> 00:18:09,580
 It doesn't mean we have to actively avoid people who are

199
00:18:09,580 --> 00:18:14,000
 unethical and immoral and harmful.

200
00:18:14,000 --> 00:18:18,570
 It means we have to be able to distinguish and understand

201
00:18:18,570 --> 00:18:21,000
 what a true friendship is.

202
00:18:21,000 --> 00:18:29,000
 True friendship requires kindness, friendliness,

203
00:18:29,000 --> 00:18:38,000
 requires trust and support, protection.

204
00:18:38,000 --> 00:18:51,910
 Friendship is a mutual activity of support, care and

205
00:18:51,910 --> 00:18:56,000
 kindness.

206
00:18:56,000 --> 00:19:06,070
 Thoughtfulness is a great refuge to have good friends, of

207
00:19:06,070 --> 00:19:08,000
 course.

208
00:19:08,000 --> 00:19:15,000
 Number four, suwajo, suwaja.

209
00:19:15,000 --> 00:19:17,000
 Suwaja.

210
00:19:17,000 --> 00:19:20,000
 Su means easy in this case.

211
00:19:20,000 --> 00:19:30,180
 Suwaja, easy to speak to is what suwaja is, being easy to

212
00:19:30,180 --> 00:19:34,000
 speak to.

213
00:19:34,000 --> 00:19:41,000
 Easy to admonish is the meaning.

214
00:19:41,000 --> 00:19:49,260
 It's a great danger to us if we are intractable, unamenable

215
00:19:49,260 --> 00:19:51,000
 to change,

216
00:19:51,000 --> 00:19:59,110
 closed to any kind of criticism, reactionary to any kind of

217
00:19:59,110 --> 00:20:05,000
 judgment or pointing out of our faults,

218
00:20:05,000 --> 00:20:11,400
 unable to take criticism, so very grave weakness, great

219
00:20:11,400 --> 00:20:13,000
 danger to friendship,

220
00:20:13,000 --> 00:20:19,730
 a great danger to progress, to spiritual development, obst

221
00:20:19,730 --> 00:20:28,000
inacy, stubbornness, pride,

222
00:20:28,000 --> 00:20:34,860
 grave dangers, danger to most if not all people, anyone who

223
00:20:34,860 --> 00:20:40,000
 is not yet enlightened,

224
00:20:40,000 --> 00:20:44,000
 stubbornness, intractability, the Buddha talked about this

225
00:20:44,000 --> 00:20:47,000
 and he reminded us if you want help,

226
00:20:47,000 --> 00:20:52,000
 you have to be easy to help, like a patient.

227
00:20:52,000 --> 00:20:55,720
 If a patient is difficult, won't take their medicine, won't

228
00:20:55,720 --> 00:20:57,000
 acknowledge that they're sick,

229
00:20:57,000 --> 00:21:01,960
 that's a common one, the most dangerous, not acknowledging

230
00:21:01,960 --> 00:21:03,000
 that you're sick.

231
00:21:03,000 --> 00:21:07,320
 Sometimes we know we're sick, but we don't want to appear

232
00:21:07,320 --> 00:21:08,000
 sick.

233
00:21:08,000 --> 00:21:11,000
 We don't let other people know that we're sick.

234
00:21:11,000 --> 00:21:15,220
 We don't accept when other people tell us we're sick even

235
00:21:15,220 --> 00:21:16,000
 though we know it,

236
00:21:16,000 --> 00:21:22,800
 because it feels like a vulnerability, a weakness, a grave

237
00:21:22,800 --> 00:21:24,000
 danger to keep it hidden.

238
00:21:24,000 --> 00:21:29,110
 No one will help us, no one will feel comfortable or

239
00:21:29,110 --> 00:21:31,000
 confident helping us,

240
00:21:31,000 --> 00:21:40,000
 feel helpless because we're not letting them help us.

241
00:21:40,000 --> 00:21:44,000
 And as with the physical, so with the mental.

242
00:21:44,000 --> 00:21:53,460
 When we don't let other people remind us of our faults, of

243
00:21:53,460 --> 00:21:56,000
 our mistakes,

244
00:21:56,000 --> 00:22:02,230
 point out our mistakes, point out our faults, we're not

245
00:22:02,230 --> 00:22:04,000
 easy to admonish.

246
00:22:04,000 --> 00:22:07,000
 It's a grave danger.

247
00:22:07,000 --> 00:22:13,000
 So it's a great protection for us, a great refuge.

248
00:22:13,000 --> 00:22:19,000
 If we are amenable to instruction.

249
00:22:19,000 --> 00:22:25,260
 A good teacher will not get angry at a student for being

250
00:22:25,260 --> 00:22:29,000
 stubborn, unruly, hard to teach.

251
00:22:29,000 --> 00:22:34,900
 But likewise, most teachers will not, a teacher will

252
00:22:34,900 --> 00:22:40,000
 generally not feel comfortable providing instruction

253
00:22:40,000 --> 00:22:45,000
 to someone who is unruly, intractable.

254
00:22:45,000 --> 00:22:49,000
 To some extent it's pointless.

255
00:22:49,000 --> 00:23:00,950
 It's a waste of energy to try and help someone who isn't

256
00:23:00,950 --> 00:23:03,000
 willing to be helped,

257
00:23:03,000 --> 00:23:06,840
 isn't open to being helped, someone who is stubborn and

258
00:23:06,840 --> 00:23:08,000
 hard to teach.

259
00:23:08,000 --> 00:23:16,000
 It's a great refuge for us if we are humble, easy to teach.

260
00:23:16,000 --> 00:23:25,620
 It's a very important sort of quality that we have to

261
00:23:25,620 --> 00:23:27,000
 develop.

262
00:23:27,000 --> 00:23:31,790
 It's something we have to keep track of, our pride and

263
00:23:31,790 --> 00:23:33,000
 stubbornness.

264
00:23:33,000 --> 00:23:40,000
 We have to be mindful, be vigilant.

265
00:23:40,000 --> 00:23:43,260
 Noticing when it arises and not letting it overwhelm us and

266
00:23:43,260 --> 00:23:47,000
 consume us and keep us from receiving instruction,

267
00:23:47,000 --> 00:23:53,000
 from receiving support, from engaging in this,

268
00:23:53,000 --> 00:24:02,210
 ānyamanyawacj nayana the Buddha said, ānyamanyawacj, āny

269
00:24:02,210 --> 00:24:04,000
amanyawacj means other and other, it means one another.

270
00:24:04,000 --> 00:24:12,000
 ānyamanyawacj means speaking to one another.

271
00:24:12,000 --> 00:24:23,370
 It doesn't mean only listening to our teachers. Sometimes

272
00:24:23,370 --> 00:24:26,000
 we feel indignant because someone tries to teach us,

273
00:24:26,000 --> 00:24:34,480
 but you're not my teacher. We shouldn't be indignant like

274
00:24:34,480 --> 00:24:36,000
 that.

275
00:24:36,000 --> 00:24:41,760
 We shouldn't be angry when someone tries to point us out

276
00:24:41,760 --> 00:24:43,000
 our faults.

277
00:24:43,000 --> 00:24:47,000
 We should consider carefully whether it's true or not true,

278
00:24:47,000 --> 00:24:52,540
 and if it's true, we should thank the person or at least

279
00:24:52,540 --> 00:25:02,000
 objectively understand that it's true.

280
00:25:02,000 --> 00:25:16,230
 Number five is, well it means helping our fellows with

281
00:25:16,230 --> 00:25:22,000
 their work.

282
00:25:22,000 --> 00:25:32,000
 Ṛṇṅgārṇiyānī dānta and dākuhoti.

283
00:25:32,000 --> 00:25:37,000
 Being keen on supporting others in their work.

284
00:25:37,000 --> 00:25:43,730
 The idea here is in a monastery, monks should take an

285
00:25:43,730 --> 00:25:47,000
 interest in each other's work,

286
00:25:47,000 --> 00:25:53,190
 and there's duties and tasks. Suppose one monk's kūti is

287
00:25:53,190 --> 00:25:55,000
 broken, there's a leak or something,

288
00:25:55,000 --> 00:25:59,000
 everyone should come together to help.

289
00:25:59,000 --> 00:26:03,050
 Even in the times when monks spend their time in silent

290
00:26:03,050 --> 00:26:04,000
 meditation,

291
00:26:04,000 --> 00:26:13,000
 they would still help each other silently.

292
00:26:13,000 --> 00:26:17,000
 Dividing duties in monasteries and so on.

293
00:26:17,000 --> 00:26:22,300
 So this is for monks, but the same of course applies in any

294
00:26:22,300 --> 00:26:25,000
 community, in any residence.

295
00:26:25,000 --> 00:26:28,000
 And the general principle is harmony.

296
00:26:28,000 --> 00:26:33,000
 Harmony and mutual support.

297
00:26:33,000 --> 00:26:40,420
 This idea of everyone for themselves, that's of course an

298
00:26:40,420 --> 00:26:46,440
 idea that is antithetical to any kind of community or

299
00:26:46,440 --> 00:26:49,000
 harmony.

300
00:26:49,000 --> 00:26:52,410
 And there's a great strength that comes from communal

301
00:26:52,410 --> 00:26:57,000
 harmony, but it's in sukko sanghasasamaki.

302
00:26:57,000 --> 00:27:04,000
 The community that is in harmony, sukko is happiness.

303
00:27:04,000 --> 00:27:07,000
 It is happiness.

304
00:27:07,000 --> 00:27:13,000
 Happiness is a harmonious community.

305
00:27:13,000 --> 00:27:16,000
 It's a great refuge.

306
00:27:16,000 --> 00:27:19,000
 It's a great refuge to be supportive of each other.

307
00:27:19,000 --> 00:27:22,000
 Others of course will support you.

308
00:27:22,000 --> 00:27:27,560
 The community will be in harmony. The community will grow

309
00:27:27,560 --> 00:27:28,000
 and flourish.

310
00:27:28,000 --> 00:27:33,000
 It will attract support.

311
00:27:33,000 --> 00:27:36,540
 And so this of course is something that is clear for

312
00:27:36,540 --> 00:27:38,000
 religious institutions,

313
00:27:38,000 --> 00:27:42,000
 but the same goes for families and societies.

314
00:27:42,000 --> 00:27:47,000
 Our societies will flourish if we help each other.

315
00:27:47,000 --> 00:27:51,620
 One thing you see in times of trouble, in times of

316
00:27:51,620 --> 00:27:57,680
 difficulty, is the people who come together to help, to

317
00:27:57,680 --> 00:28:01,000
 support each other.

318
00:28:01,000 --> 00:28:04,000
 You see it brings out the best and the worst in people.

319
00:28:04,000 --> 00:28:11,370
 You can see the best and how great a refuge that is for

320
00:28:11,370 --> 00:28:18,360
 society as a whole, for the people on both sides, providing

321
00:28:18,360 --> 00:28:26,000
 each other with refuge, providing each other with support.

322
00:28:26,000 --> 00:28:30,000
 Number six, dhammakamu.

323
00:28:30,000 --> 00:28:33,130
 It's one of the few times the Buddha uses the word kamma in

324
00:28:33,130 --> 00:28:34,000
 a good way.

325
00:28:34,000 --> 00:28:39,000
 Kamma here means love.

326
00:28:39,000 --> 00:28:46,000
 Dhammakamu in love with the dhamma.

327
00:28:46,000 --> 00:28:49,000
 Kamma.

328
00:28:49,000 --> 00:28:52,000
 Dhammakamu bhavang huti.

329
00:28:52,000 --> 00:28:58,020
 Who loves the dhamma, one who is in love with the dhamma,

330
00:28:58,020 --> 00:29:07,750
 is a developed person, is a high-minded individual,

331
00:29:07,750 --> 00:29:12,000
 cultivated person.

332
00:29:12,000 --> 00:29:15,570
 So obviously the Buddha is not saying we should crave the d

333
00:29:15,570 --> 00:29:20,000
hamma or have any kind of lust or anything, passion.

334
00:29:20,000 --> 00:29:26,620
 But he acknowledges this attraction, describes this state

335
00:29:26,620 --> 00:29:29,000
 of attraction to the dhamma.

336
00:29:29,000 --> 00:29:35,300
 Someone who is intent upon the dhamma, there is a real

337
00:29:35,300 --> 00:29:42,380
 feeling of zest, zeal, because of how pure and profound and

338
00:29:42,380 --> 00:29:47,000
 perfect the dhamma is in so many ways.

339
00:29:47,000 --> 00:29:53,960
 This is a great refuge. It would be a great detriment to us

340
00:29:53,960 --> 00:30:08,000
 if we looked at the dhamma with aversion.

341
00:30:08,000 --> 00:30:14,020
 If we were averse or afraid or displeased by the dhamma,

342
00:30:14,020 --> 00:30:18,000
 that would be a great danger to us.

343
00:30:18,000 --> 00:30:20,430
 And it doesn't have to just mean the teaching of the Buddha

344
00:30:20,430 --> 00:30:25,000
. Of course, it's not just because the Buddha taught it.

345
00:30:25,000 --> 00:30:28,520
 It's a grave danger for us to look at anything right as

346
00:30:28,520 --> 00:30:30,000
 though it's wrong.

347
00:30:30,000 --> 00:30:34,640
 It would be a grave danger for us to look down upon any

348
00:30:34,640 --> 00:30:38,000
 kind of practice that is beneficial.

349
00:30:38,000 --> 00:30:41,150
 To have that sort of wrong view is to be very much on the

350
00:30:41,150 --> 00:30:47,470
 wrong path, wrong perspective that of course leads us down

351
00:30:47,470 --> 00:30:50,000
 other paths.

352
00:30:50,000 --> 00:30:53,960
 To not see what is important as important, what is un

353
00:30:53,960 --> 00:30:58,540
important as unimportant, to not see what is beneficial as

354
00:30:58,540 --> 00:31:01,000
 beneficial is a grave danger.

355
00:31:01,000 --> 00:31:06,870
 It is a great support for us not only to know the dhamma

356
00:31:06,870 --> 00:31:13,000
 but to be in love with it in the sense of appreciating it.

357
00:31:13,000 --> 00:31:19,710
 Having our mind leap at the idea of practicing and learning

358
00:31:19,710 --> 00:31:21,000
 the dhamma.

359
00:31:21,000 --> 00:31:26,930
 When the mind leaps at the idea of doing good deeds, jumps

360
00:31:26,930 --> 00:31:31,980
 up, ready to act, that's a great protection, a great refuge

361
00:31:31,980 --> 00:31:33,000
 for us.

362
00:31:33,000 --> 00:31:36,560
 Of course, because it keeps us on the right path, keeps us

363
00:31:36,560 --> 00:31:45,120
 doing good things. It's only our inclination that keeps us

364
00:31:45,120 --> 00:31:49,000
 away from danger.

365
00:31:49,000 --> 00:31:59,000
 All bad inclinations, that's what will lead us to danger.

366
00:31:59,000 --> 00:32:10,110
 Number seven, santuti. Santuti means contentment. Content

367
00:32:10,110 --> 00:32:14,000
ment is a very great refuge.

368
00:32:14,000 --> 00:32:19,000
 What's it a refuge from? It's a refuge from greed.

369
00:32:19,000 --> 00:32:26,050
 What a danger it is to be addicted, to need things and not

370
00:32:26,050 --> 00:32:31,000
 just things but more and more and more.

371
00:32:31,000 --> 00:32:34,810
 Whatever you get will not be enough. Buddha said it could

372
00:32:34,810 --> 00:32:40,000
 rain gold and that would never be enough for one person.

373
00:32:40,000 --> 00:32:45,170
 We hear about billionaires and how they have so much money

374
00:32:45,170 --> 00:32:49,460
 they don't know what to do with and then we hear about

375
00:32:49,460 --> 00:32:52,000
 billionaires becoming trillionaires.

376
00:32:52,000 --> 00:32:58,020
 It will never be enough. That's not how greed works. Greed

377
00:32:58,020 --> 00:33:04,000
 is of a nature to not ever have enough.

378
00:33:04,000 --> 00:33:10,000
 There's no appeasement for greed.

379
00:33:10,000 --> 00:33:13,340
 So being content with nothing is a very different way of

380
00:33:13,340 --> 00:33:18,000
 going. It appears powerless in many ways.

381
00:33:18,000 --> 00:33:23,000
 When you don't have, you are powerless.

382
00:33:23,000 --> 00:33:29,640
 You're powerless but you are impervious. A person without

383
00:33:29,640 --> 00:33:35,000
 wealth, without possessions is without power in many ways

384
00:33:35,000 --> 00:33:39,690
 but they are also without suffering in so many ways because

385
00:33:39,690 --> 00:33:46,000
 they're impervious to loss.

386
00:33:46,000 --> 00:33:52,190
 Contentment doesn't mean not getting or having anything of

387
00:33:52,190 --> 00:33:58,000
 course but it means being content with whatever you get.

388
00:33:58,000 --> 00:34:03,610
 And on a practice level, that's a very important part of

389
00:34:03,610 --> 00:34:07,450
 mindfulness because it no longer applies to possessions and

390
00:34:07,450 --> 00:34:11,000
 things, it applies to experiences.

391
00:34:11,000 --> 00:34:16,430
 Contentment with whatever experience. Not having your

392
00:34:16,430 --> 00:34:21,490
 happiness and your peace depend on the vicissitudes of

393
00:34:21,490 --> 00:34:23,000
 experience.

394
00:34:23,000 --> 00:34:28,520
 The constant unpredictable nature of reality. If our

395
00:34:28,520 --> 00:34:33,670
 happiness depended on that we would always be stressed and

396
00:34:33,670 --> 00:34:35,000
 suffering.

397
00:34:35,000 --> 00:34:43,000
 It is a refuge for us to be content.

398
00:34:43,000 --> 00:34:49,000
 Number seven, number eight.

399
00:34:49,000 --> 00:34:52,000
 Aradhaviriya. Aradhaviriya.

400
00:34:52,000 --> 00:34:59,670
 Aradhaviriya effort. Aradhaviriya means like strong or

401
00:34:59,670 --> 00:35:02,000
 unrelenting.

402
00:35:02,000 --> 00:35:18,000
 Effort is a great refuge because laziness is a great danger

403
00:35:18,000 --> 00:35:18,000
.

404
00:35:18,000 --> 00:35:26,030
 As long as we have this vulnerability, this crookedness in

405
00:35:26,030 --> 00:35:34,360
 the mind, any kind of unwholesomeness in our mind, laziness

406
00:35:34,360 --> 00:35:40,000
 allows it to breed and fester and grow.

407
00:35:40,000 --> 00:35:46,090
 One important characteristic of the path to freedom or the

408
00:35:46,090 --> 00:35:51,000
 right way to live is that it involves effort.

409
00:35:51,000 --> 00:35:55,090
 It doesn't mean we have to force ourselves to do good

410
00:35:55,090 --> 00:35:56,000
 things.

411
00:35:56,000 --> 00:36:00,000
 In fact, forcing ourselves to some extent is lazy.

412
00:36:00,000 --> 00:36:03,500
 It's much easier to try and force yourself to meditate, for

413
00:36:03,500 --> 00:36:07,680
 example, than to actually be mindful. It's a lazy way to be

414
00:36:07,680 --> 00:36:09,000
 effortful.

415
00:36:09,000 --> 00:36:13,090
 It's to just force yourself, push yourself. Effort doesn't

416
00:36:13,090 --> 00:36:18,000
 mean pushing yourself. It means doing it again and again.

417
00:36:18,000 --> 00:36:22,000
 It's quite different from forcing.

418
00:36:22,000 --> 00:36:26,000
 Effort arises in the moment.

419
00:36:26,000 --> 00:36:32,970
 It has to be coupled with mindfulness. And so the effort to

420
00:36:32,970 --> 00:36:42,560
 be mindful means the spark at every moment, reminding

421
00:36:42,560 --> 00:36:44,000
 yourself,

422
00:36:44,000 --> 00:36:48,660
 inclining your mind to be mindful again and again and again

423
00:36:48,660 --> 00:36:51,000
. That's effort.

424
00:36:51,000 --> 00:36:56,270
 Effort, for the most part, is about being methodical,

425
00:36:56,270 --> 00:37:08,870
 consistent, not giving up, not backing off, not slacking

426
00:37:08,870 --> 00:37:11,000
 off.

427
00:37:11,000 --> 00:37:16,740
 So great refuge, to keep our minds in all wholesomeness

428
00:37:16,740 --> 00:37:24,000
 requires effort, repetitive effort, again and again.

429
00:37:24,000 --> 00:37:28,330
 Not letting ourselves become complacent with good things we

430
00:37:28,330 --> 00:37:32,000
've done in the past or what we might do in the future,

431
00:37:32,000 --> 00:37:41,000
 but repetitive inclination of the mind to good things.

432
00:37:41,000 --> 00:37:46,000
 Number nine is, of course, sati.

433
00:37:46,000 --> 00:37:50,930
 Sati is the second most important thing, so it's the second

434
00:37:50,930 --> 00:37:52,000
 last one.

435
00:37:52,000 --> 00:37:58,060
 Sati is the basis of our practice. Sati is the core of our

436
00:37:58,060 --> 00:38:00,000
 practice.

437
00:38:00,000 --> 00:38:05,000
 Sati, to remember.

438
00:38:05,000 --> 00:38:08,070
 Not things that happened in the past or what we have to do

439
00:38:08,070 --> 00:38:11,000
 in the future, but to remember the present, to remember

440
00:38:11,000 --> 00:38:13,000
 what's happening.

441
00:38:13,000 --> 00:38:18,760
 To have the mind stay with the experience without getting

442
00:38:18,760 --> 00:38:25,000
 caught up in extrapolation or reaction.

443
00:38:25,000 --> 00:38:30,590
 The mind that is aware of what's happening right now in the

444
00:38:30,590 --> 00:38:35,000
 body, the feelings, the mind, the dhamma.

445
00:38:35,000 --> 00:38:40,000
 That being in the present moment is the ultimate refuge.

446
00:38:40,000 --> 00:38:44,000
 When the Buddha boiled this all down, he ultimately said,

447
00:38:44,000 --> 00:38:46,000
 when he said, "Be your own refuge."

448
00:38:46,000 --> 00:38:51,680
 "How do you do that?" he said. "Practice the four satipat

449
00:38:51,680 --> 00:38:53,000
anas."

450
00:38:53,000 --> 00:38:59,000
 Sati, mindfulness, remembrance.

451
00:38:59,000 --> 00:39:02,000
 This is the greatest refuge.

452
00:39:02,000 --> 00:39:06,160
 He said, second most important, because the last one is

453
00:39:06,160 --> 00:39:07,000
 wisdom.

454
00:39:07,000 --> 00:39:16,000
 So mindfulness is great because it protects us every moment

455
00:39:16,000 --> 00:39:16,000
,

456
00:39:16,000 --> 00:39:23,000
 but it only protects us at the moment when we're mindful.

457
00:39:23,000 --> 00:39:27,000
 If we stop being mindful, then we lose the protection.

458
00:39:27,000 --> 00:39:33,950
 It's flawed that way. It's limited that way. It's not a

459
00:39:33,950 --> 00:39:41,000
 flaw, it's a limit.

460
00:39:41,000 --> 00:39:49,000
 Sati protects the mind and that's not just a...

461
00:39:49,000 --> 00:39:53,000
 It doesn't make it just a temporary solution.

462
00:39:53,000 --> 00:39:57,190
 It means it's not the final solution. It's not the ultimate

463
00:39:57,190 --> 00:40:00,000
 goal.

464
00:40:00,000 --> 00:40:04,000
 Sati is what leads us to the goal.

465
00:40:04,000 --> 00:40:09,030
 Because protecting our mind in this way, protecting our

466
00:40:09,030 --> 00:40:11,000
 mind from delusion,

467
00:40:11,000 --> 00:40:21,000
 from the darkness of ignorance and wrong view and so on,

468
00:40:21,000 --> 00:40:27,000
 creates clarity in the mind.

469
00:40:27,000 --> 00:40:32,670
 It allows us to see clearly the nature of reality, the

470
00:40:32,670 --> 00:40:35,000
 nature of experience.

471
00:40:35,000 --> 00:40:39,330
 It allows us to see right from wrong and good from bad,

472
00:40:39,330 --> 00:40:44,000
 happiness from suffering.

473
00:40:44,000 --> 00:40:48,000
 That's the real goal of sati, of mindfulness.

474
00:40:48,000 --> 00:40:52,170
 Yes, it's a great protection, but the real refuge, the

475
00:40:52,170 --> 00:40:55,000
 ultimate refuge is wisdom.

476
00:40:55,000 --> 00:41:00,000
 Seeing things clearly and fully and completely as they are.

477
00:41:00,000 --> 00:41:02,000
 And that's what comes from mindfulness.

478
00:41:02,000 --> 00:41:08,000
 The protection that we give to our mind at every moment

479
00:41:08,000 --> 00:41:17,370
 brings us to the ultimate protection that doesn't fade away

480
00:41:17,370 --> 00:41:18,000
.

481
00:41:18,000 --> 00:41:21,000
 When you see things as they are,

482
00:41:21,000 --> 00:41:29,000
 paññāyā parīsūjati, through wisdom, one is purified.

483
00:41:29,000 --> 00:41:44,000
 Purity, the freedom from any kind of stain or fault.

484
00:41:44,000 --> 00:41:49,000
 All it takes is wisdom to be pure.

485
00:41:49,000 --> 00:41:52,000
 You see that there's nothing worth clinging to,

486
00:41:52,000 --> 00:41:55,220
 that everything that arises inside of us and in the world

487
00:41:55,220 --> 00:41:56,000
 around us

488
00:41:56,000 --> 00:42:00,520
 is impermanent, unsatisfying, uncontrollable, not worth

489
00:42:00,520 --> 00:42:03,000
 clinging to.

490
00:42:03,000 --> 00:42:07,000
 Just that wisdom, just that,

491
00:42:07,000 --> 00:42:12,000
 frees us from all kinds of danger,

492
00:42:12,000 --> 00:42:15,340
 all of the dangers of clinging, all of the dangers of mis

493
00:42:15,340 --> 00:42:17,000
apprehending things

494
00:42:17,000 --> 00:42:20,000
 are stable, satisfying, and controllable.

495
00:42:20,000 --> 00:42:25,000
 All the problems that come from misunderstanding reality

496
00:42:25,000 --> 00:42:30,000
 and clinging to reality as though it might provide us with

497
00:42:30,000 --> 00:42:31,000
 stability,

498
00:42:31,000 --> 00:42:37,000
 satisfaction, or control.

499
00:42:37,000 --> 00:42:43,000
 All of that we do away with just by seeing clearly.

500
00:42:43,000 --> 00:42:45,000
 Wisdom is the greatest refuge.

501
00:42:45,000 --> 00:42:49,500
 None of these provide a really good framework for Buddhist

502
00:42:49,500 --> 00:42:50,000
 practice

503
00:42:50,000 --> 00:42:57,000
 and Buddhist theory.

504
00:42:57,000 --> 00:43:02,000
 So that's the Dhamma for this morning.

505
00:43:02,000 --> 00:43:05,000
 A reminder, a refresher,

506
00:43:05,000 --> 00:43:09,000
 something new for those who haven't heard it before.

507
00:43:09,000 --> 00:43:15,000
 The importance of making a refuge for yourself

508
00:43:15,000 --> 00:43:18,000
 and the means by which to do that.

509
00:43:18,000 --> 00:43:21,000
 Thank you all for listening.

510
00:43:21,000 --> 00:43:25,000
 Thank you.

