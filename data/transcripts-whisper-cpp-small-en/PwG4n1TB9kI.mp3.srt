1
00:00:00,000 --> 00:00:05,880
 Okay, so the latest question comes from one of the

2
00:00:05,880 --> 00:00:09,560
 commenters asking whether I'd recommend

3
00:00:09,560 --> 00:00:14,640
 to go to a 10-day Vipassana meditation course.

4
00:00:14,640 --> 00:00:18,120
 I guess the short answer here is no, because I wouldn't

5
00:00:18,120 --> 00:00:20,400
 recommend something that I myself

6
00:00:20,400 --> 00:00:22,720
 didn't teach or practice.

7
00:00:22,720 --> 00:00:26,360
 And generally a 10-day Vipassana meditation course as

8
00:00:26,360 --> 00:00:28,320
 offered around the world is not

9
00:00:28,320 --> 00:00:30,680
 something that I teach.

10
00:00:30,680 --> 00:00:34,230
 As for how it might be a problem, I also can't say, I

11
00:00:34,230 --> 00:00:36,840
 understand that many courses require

12
00:00:36,840 --> 00:00:41,880
 you to sit for many hours a day, sometimes for an hour at a

13
00:00:41,880 --> 00:00:44,400
 time, even for a beginner

14
00:00:44,400 --> 00:00:45,400
 meditator.

15
00:00:45,400 --> 00:00:49,520
 Now the way I teach is to do a course maybe two or three

16
00:00:49,520 --> 00:00:52,020
 weeks, and some people might

17
00:00:52,020 --> 00:00:54,740
 come for a part of a course, because they'll come just for

18
00:00:54,740 --> 00:00:56,000
 a week, which is fine.

19
00:00:56,000 --> 00:01:00,820
 And a full course for a beginner would take, say, three

20
00:01:00,820 --> 00:01:01,720
 weeks.

21
00:01:01,720 --> 00:01:04,360
 Not more than that, but not much less if you've never

22
00:01:04,360 --> 00:01:05,760
 practiced meditation.

23
00:01:05,760 --> 00:01:08,840
 And the way that works is we start the meditator off easily

24
00:01:08,840 --> 00:01:10,800
, and the whole course, the whole

25
00:01:10,800 --> 00:01:13,400
 progression is much more natural.

26
00:01:13,400 --> 00:01:16,430
 It's much more natural also because half of your meditation

27
00:01:16,430 --> 00:01:17,880
 time is done in walking, whereas

28
00:01:17,880 --> 00:01:22,570
 many courses are done with sitting for eight to ten hours a

29
00:01:22,570 --> 00:01:23,240
 day.

30
00:01:23,240 --> 00:01:27,970
 The practice that I teach is according to the Buddha's

31
00:01:27,970 --> 00:01:32,240
 teaching that a monk, or by extrapolation,

32
00:01:32,240 --> 00:01:35,410
 anyone who's serious in meditation should spend half their

33
00:01:35,410 --> 00:01:36,880
 time walking, half their

34
00:01:36,880 --> 00:01:37,880
 time sitting.

35
00:01:37,880 --> 00:01:42,540
 So walk and sit in alternation for the whole of the day,

36
00:01:42,540 --> 00:01:45,160
 and only breaking for eating and

37
00:01:45,160 --> 00:01:47,880
 for sleeping.

38
00:01:47,880 --> 00:01:50,340
 Now for a beginner meditator, this is quite difficult.

39
00:01:50,340 --> 00:01:52,980
 And so we start you off simply.

40
00:01:52,980 --> 00:01:56,040
 We have you do mindful prostration, ten minutes walking and

41
00:01:56,040 --> 00:01:57,280
 ten minutes sitting.

42
00:01:57,280 --> 00:02:03,590
 Then you're allowed to take a break and do according as you

43
00:02:03,590 --> 00:02:06,720
 see fit or you feel capable.

44
00:02:06,720 --> 00:02:10,260
 You can take a break and come back, and we don't have very

45
00:02:10,260 --> 00:02:11,880
 much group practice.

46
00:02:11,880 --> 00:02:14,700
 Generally, we'll do one or two group practices during the

47
00:02:14,700 --> 00:02:16,860
 day, otherwise it's up to you to

48
00:02:16,860 --> 00:02:20,040
 make your own schedule and we expect you to practice as

49
00:02:20,040 --> 00:02:22,000
 much as you can comfortably.

50
00:02:22,000 --> 00:02:24,750
 And in this way, we slowly increase to the point where you

51
00:02:24,750 --> 00:02:26,120
 don't even notice that all

52
00:02:26,120 --> 00:02:29,240
 of a sudden you're walking one hour and sitting one hour.

53
00:02:29,240 --> 00:02:32,670
 And you also don't notice it because you don't have to sit

54
00:02:32,670 --> 00:02:34,120
 the whole of the day.

55
00:02:34,120 --> 00:02:35,920
 You're allowed to get up and do walking.

56
00:02:35,920 --> 00:02:38,680
 You're required to get up and do walking meditation.

57
00:02:38,680 --> 00:02:42,400
 So I would only recommend what I teach and what I practice,

58
00:02:42,400 --> 00:02:44,160
 and that is as I've explained

59
00:02:44,160 --> 00:02:48,770
 the two or three week course where you do walking and

60
00:02:48,770 --> 00:02:52,400
 sitting meditation in tandem together

61
00:02:52,400 --> 00:02:55,500
 and that you do it gradually, slowly building up to the

62
00:02:55,500 --> 00:02:57,360
 point where you don't even notice

63
00:02:57,360 --> 00:03:00,930
 that you're suddenly practicing eight, ten, twelve, even

64
00:03:00,930 --> 00:03:02,960
 more hours of meditation a day.

65
00:03:02,960 --> 00:03:03,960
 It's not difficult.

66
00:03:03,960 --> 00:03:08,480
 If you do it like this, this part isn't difficult.

67
00:03:08,480 --> 00:03:09,820
 The hours aren't difficult.

68
00:03:09,820 --> 00:03:13,410
 It's a difficult course and it's difficult to meditate, but

69
00:03:13,410 --> 00:03:14,880
 you don't feel like it's

70
00:03:14,880 --> 00:03:16,900
 an insurmountable obstacle.

71
00:03:16,900 --> 00:03:21,590
 You suddenly feel like a pro over the course of the

72
00:03:21,590 --> 00:03:22,920
 training.

73
00:03:22,920 --> 00:03:24,800
 So I hope that helps.

74
00:03:24,800 --> 00:03:26,360
 You're always welcome to contact me.

75
00:03:26,360 --> 00:03:30,090
 I can maybe give pointers as to courses in your area that I

76
00:03:30,090 --> 00:03:31,800
 would recommend or maybe

77
00:03:31,800 --> 00:03:35,000
 give some more detailed analysis of the various courses

78
00:03:35,000 --> 00:03:36,800
 that are offered out there.

79
00:03:36,800 --> 00:03:39,760
 You're welcome to contact me.

80
00:03:39,760 --> 00:03:40,760
 Thank you for more information.

81
00:03:40,760 --> 00:03:43,200
 Okay, and if you have any more questions, just leave them.

82
00:03:43,200 --> 00:03:44,200
 Thanks.

