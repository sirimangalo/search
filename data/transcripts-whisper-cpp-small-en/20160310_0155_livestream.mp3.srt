1
00:00:00,000 --> 00:00:27,360
 Good evening everyone.

2
00:00:27,360 --> 00:00:53,240
 Today we

3
00:00:53,240 --> 00:01:04,040
 have a quote about the body, bodily purity.

4
00:01:04,040 --> 00:01:07,200
 How do you clean the body?

5
00:01:07,200 --> 00:01:11,400
 How do you clean the body?

6
00:01:11,400 --> 00:01:21,320
 It's a loaded question from a Buddhist point of view.

7
00:01:21,320 --> 00:01:26,320
 We were talking on Monday about, we've just been reading

8
00:01:26,320 --> 00:01:29,160
 chapters 20 to 25 of the Lotus

9
00:01:29,160 --> 00:01:30,160
 Sutra.

10
00:01:30,160 --> 00:01:46,690
 And in one of those chapters, there's a monk, or I think a

11
00:01:46,690 --> 00:01:51,240
 monk who, a man, somebody, who

12
00:01:51,240 --> 00:02:03,190
 lights their body on fire as an offering to the Buddha,

13
00:02:03,190 --> 00:02:10,520
 burns themselves completely.

14
00:02:10,520 --> 00:02:19,750
 And there's another person who burns their forearm, burns

15
00:02:19,750 --> 00:02:24,440
 off their forearm completely.

16
00:02:24,440 --> 00:02:28,080
 And then make some kind of a vow and they come back.

17
00:02:28,080 --> 00:02:31,680
 I mean, honestly I have less and less good to say about

18
00:02:31,680 --> 00:02:33,840
 this Lotus Sutra the more I read

19
00:02:33,840 --> 00:02:36,840
 it.

20
00:02:36,840 --> 00:02:42,870
 I've got an interesting conversation going about the nature

21
00:02:42,870 --> 00:02:45,320
 of the body according to

22
00:02:45,320 --> 00:02:49,160
 Buddhism.

23
00:02:49,160 --> 00:02:56,790
 Because a common thought or train of thought for spiritual

24
00:02:56,790 --> 00:03:01,120
 people, especially in the West,

25
00:03:01,120 --> 00:03:04,040
 is that the body is somehow sacred.

26
00:03:04,040 --> 00:03:15,170
 It should work to purify the body, you should guard the

27
00:03:15,170 --> 00:03:19,120
 body as a vessel.

28
00:03:19,120 --> 00:03:22,740
 And so the idea that spiritual beings would use part of

29
00:03:22,740 --> 00:03:25,120
 their body as an offering, I mean,

30
00:03:25,120 --> 00:03:30,930
 it's actually common in Mahayana, even today, to offer like

31
00:03:30,930 --> 00:03:32,160
 a finger.

32
00:03:32,160 --> 00:03:34,840
 You just stick a finger into a candle and burn it out.

33
00:03:34,840 --> 00:03:39,400
 And it's an offering to the Buddha.

34
00:03:39,400 --> 00:03:41,700
 So we're arguing whether it is an offering because you're

35
00:03:41,700 --> 00:03:42,960
 not actually giving anything

36
00:03:42,960 --> 00:03:43,960
 to anybody.

37
00:03:43,960 --> 00:03:47,760
 Nobody's benefiting from your burning of a finger.

38
00:03:47,760 --> 00:03:54,820
 Or whether it's just a measure of your determination,

39
00:03:54,820 --> 00:04:00,280
 devotion, it's a measure of devotion.

40
00:04:00,280 --> 00:04:06,220
 But the idea that you should do this, whether or not Buddh

41
00:04:06,220 --> 00:04:09,000
ists like me would agree with

42
00:04:09,000 --> 00:04:16,860
 doing that, we at least can agree that using your body in

43
00:04:16,860 --> 00:04:21,480
 some way to benefit someone else,

44
00:04:21,480 --> 00:04:26,550
 or using your body in any way really is a lot like using

45
00:04:26,550 --> 00:04:28,360
 any other tool.

46
00:04:28,360 --> 00:04:31,080
 We don't see the body as somehow special.

47
00:04:31,080 --> 00:04:35,960
 I mean, it's categorically different from other things

48
00:04:35,960 --> 00:04:39,240
 because there's experiences surrounding

49
00:04:39,240 --> 00:04:48,160
 it, direct experience, experience of pain and pleasure.

50
00:04:48,160 --> 00:04:55,480
 But as an entity, the body is something you can give away.

51
00:04:55,480 --> 00:04:59,010
 Organ donation, blood donation, these sorts of things I

52
00:04:59,010 --> 00:05:01,000
 think are very much in line with

53
00:05:01,000 --> 00:05:02,000
 Buddhism.

54
00:05:02,000 --> 00:05:08,200
 We talked about this in Thailand, how it's a giving blood.

55
00:05:08,200 --> 00:05:09,520
 It's a great thing.

56
00:05:09,520 --> 00:05:12,410
 All the monks and nuns I went with them, we went together

57
00:05:12,410 --> 00:05:13,960
 to the hospital to give blood

58
00:05:13,960 --> 00:05:21,060
 because we consider that to be a great thing that we could

59
00:05:21,060 --> 00:05:21,880
 do.

60
00:05:21,880 --> 00:05:27,160
 Whether it's right or wrong for monks and nuns to give

61
00:05:27,160 --> 00:05:30,240
 blood, I'm not sure, but we all

62
00:05:30,240 --> 00:05:33,400
 did it.

63
00:05:33,400 --> 00:05:40,560
 But the idea is that the body is just another thing.

64
00:05:40,560 --> 00:05:44,320
 More to the point, it's inherently impure.

65
00:05:44,320 --> 00:05:47,960
 There's nothing pure about the body.

66
00:05:47,960 --> 00:05:52,660
 If you want to get technical, it's made up of the four

67
00:05:52,660 --> 00:05:55,720
 elements and so there's nothing

68
00:05:55,720 --> 00:06:00,080
 disgusting about it technically.

69
00:06:00,080 --> 00:06:11,100
 But that being said, it's made up of all numbers of things

70
00:06:11,100 --> 00:06:16,480
 that are, at the very least, without

71
00:06:16,480 --> 00:06:20,720
 the ability to provide satisfaction.

72
00:06:20,720 --> 00:06:24,120
 But that in general are considered repulsive.

73
00:06:24,120 --> 00:06:28,350
 You're going to clean out the body when you have to get rid

74
00:06:28,350 --> 00:06:30,920
 of all the blood and pus and

75
00:06:30,920 --> 00:06:37,800
 snot and bile and urine and feces.

76
00:06:37,800 --> 00:06:49,000
 In a conventional sense, the body is terribly impure.

77
00:06:49,000 --> 00:06:54,240
 But in another sense, and it's in the sense that the Buddha

78
00:06:54,240 --> 00:06:57,240
 is talking here, it is possible

79
00:06:57,240 --> 00:06:59,480
 to purify the body.

80
00:06:59,480 --> 00:07:02,480
 It's totally not what most people would think.

81
00:07:02,480 --> 00:07:06,810
 It's a completely different way of looking at purity and

82
00:07:06,810 --> 00:07:08,640
 looking at the body.

83
00:07:08,640 --> 00:07:11,320
 So how do you purify the body?

84
00:07:11,320 --> 00:07:13,600
 Stop killing.

85
00:07:13,600 --> 00:07:16,600
 Right?

86
00:07:16,600 --> 00:07:24,850
 Purity and impurity have nothing to do with the nature of

87
00:07:24,850 --> 00:07:26,880
 the thing.

88
00:07:26,880 --> 00:07:31,500
 The impurity in regards to the physical realm have

89
00:07:31,500 --> 00:07:34,520
 everything to do with the use.

90
00:07:34,520 --> 00:07:38,640
 Don't purify the body.

91
00:07:38,640 --> 00:07:40,240
 Don't use it to kill.

92
00:07:40,240 --> 00:07:43,760
 Don't use it to steal.

93
00:07:43,760 --> 00:07:51,560
 Don't use it to commit sexual misconduct, to commit

94
00:07:51,560 --> 00:07:53,600
 adultery.

95
00:07:53,600 --> 00:07:58,680
 To commit immoral acts of body.

96
00:07:58,680 --> 00:08:04,240
 Purity for the material realm is in the use we give.

97
00:08:04,240 --> 00:08:07,180
 So this goes not only for the body, but it goes for all

98
00:08:07,180 --> 00:08:08,600
 other things we use.

99
00:08:08,600 --> 00:08:12,000
 The body is in this way not categorically different.

100
00:08:12,000 --> 00:08:15,120
 It's just another thing that we use.

101
00:08:15,120 --> 00:08:20,870
 If you want to talk about purity of your possessions and

102
00:08:20,870 --> 00:08:22,960
 purity of robes.

103
00:08:22,960 --> 00:08:27,330
 My robes pure, they're 100% cotton as far as I know, but

104
00:08:27,330 --> 00:08:30,680
 that's not what makes them pure.

105
00:08:30,680 --> 00:08:39,680
 They're pure if I use them for pure purposes.

106
00:08:39,680 --> 00:08:44,840
 If I use them for play or for fun or if I enjoy them or if

107
00:08:44,840 --> 00:08:47,400
 I find robes that are only

108
00:08:47,400 --> 00:08:51,190
 robes that are beautiful and so on, pick my robes based on

109
00:08:51,190 --> 00:08:53,560
 how beautiful and how comfortable

110
00:08:53,560 --> 00:09:02,080
 and how pleasing they are, then that's impure no matter

111
00:09:02,080 --> 00:09:05,800
 what they're made of.

112
00:09:05,800 --> 00:09:10,170
 If I use my food, the food that I get, if I use it for

113
00:09:10,170 --> 00:09:12,960
 enjoyment or for intoxication

114
00:09:12,960 --> 00:09:20,480
 or for fattening up, I use it for excess.

115
00:09:20,480 --> 00:09:24,440
 That is impure food.

116
00:09:24,440 --> 00:09:26,000
 That's what makes food impure.

117
00:09:26,000 --> 00:09:29,830
 It's not because it's got this ingredient or that

118
00:09:29,830 --> 00:09:31,120
 ingredient.

119
00:09:31,120 --> 00:09:36,040
 A shelter is pure because of how you use it.

120
00:09:36,040 --> 00:09:40,290
 Do you use it to hide your evil deeds or do you use it for

121
00:09:40,290 --> 00:09:44,640
 seclusion to cultivate meditation?

122
00:09:44,640 --> 00:09:50,750
 In medicine, do you use it to heal the body or do you use

123
00:09:50,750 --> 00:09:54,600
 it to become addicted to painkillers

124
00:09:54,600 --> 00:10:05,280
 or to avoid the problems, to avoid suffering?

125
00:10:05,280 --> 00:10:09,440
 Have I extenioned everything else that we use, including

126
00:10:09,440 --> 00:10:10,360
 the body?

127
00:10:10,360 --> 00:10:13,520
 So what are you using the body for?

128
00:10:13,520 --> 00:10:18,420
 You have this body as a tool or are you using it to do evil

129
00:10:18,420 --> 00:10:21,240
 things or are you using it to

130
00:10:21,240 --> 00:10:22,240
 gain purity?

131
00:10:22,240 --> 00:10:29,780
 Are you using it as a tool, as a vehicle, doing walking

132
00:10:29,780 --> 00:10:35,960
 meditation, doing sitting meditation?

133
00:10:35,960 --> 00:10:39,000
 Are you using it for the right purposes?

134
00:10:39,000 --> 00:10:41,240
 Then it's pure.

135
00:10:41,240 --> 00:10:44,240
 Then the body is pure.

136
00:10:44,240 --> 00:10:53,240
 So, simple dhamma for tonight.

137
00:10:53,240 --> 00:11:01,120
 I'm sorry I haven't been all that.

138
00:11:01,120 --> 00:11:03,120
 I really want to do these sessions.

139
00:11:03,120 --> 00:11:08,060
 I can do them often and try to get back into the video but

140
00:11:08,060 --> 00:11:11,240
 this month is going to be really,

141
00:11:11,240 --> 00:11:12,240
 really busy.

142
00:11:12,240 --> 00:11:15,900
 I made a mistake of taking two upper year courses even

143
00:11:15,900 --> 00:11:18,760
 though I'm only taking three courses.

144
00:11:18,760 --> 00:11:28,960
 Two of them are, I've got two essays, two papers to write.

145
00:11:28,960 --> 00:11:33,680
 The peace symposium we're organizing.

146
00:11:33,680 --> 00:11:40,230
 Yeah, so hopefully I'll still have time to do all this but

147
00:11:40,230 --> 00:11:43,040
 the next week is going to

148
00:11:43,040 --> 00:11:44,040
 be pretty harsh.

149
00:11:44,040 --> 00:11:48,960
 I'm going to have to buckle down and do some work,

150
00:11:48,960 --> 00:11:52,880
 especially because I missed a week.

151
00:11:52,880 --> 00:11:57,000
 Anyway, that's none of your concern.

152
00:11:57,000 --> 00:12:03,280
 I'll try and be here as much as possible.

153
00:12:03,280 --> 00:12:08,370
 Maybe we'll do some more dhammapada but we'll see how it

154
00:12:08,370 --> 00:12:09,280
 goes.

155
00:12:09,280 --> 00:12:12,280
 Thank you all for tuning in.

156
00:12:12,280 --> 00:12:16,720
 Oh wait, next part.

157
00:12:16,720 --> 00:12:18,720
 I'm going to post the hangout.

158
00:12:18,720 --> 00:12:25,560
 Anybody who wants to come on the hangout can come on.

159
00:12:25,560 --> 00:12:27,520
 We've got news tonight.

160
00:12:27,520 --> 00:12:29,280
 Robin just finished her course.

161
00:12:29,280 --> 00:12:30,280
 She's here.

162
00:12:30,280 --> 00:12:33,280
 Many of you know Robin.

163
00:12:33,280 --> 00:12:35,280
 She's Robin.

164
00:12:35,280 --> 00:12:37,280
 We let her out of her room today.

165
00:12:37,280 --> 00:12:40,280
 Doesn't she look happy?

166
00:12:40,280 --> 00:12:46,560
 She's still meditating so I don't want to bother her.

167
00:12:46,560 --> 00:12:54,080
 Maybe we'll have a chance to talk before she leaves.

168
00:12:54,080 --> 00:12:57,880
 So if you have questions, come on the hangout.

169
00:12:57,880 --> 00:13:02,880
 I just posted it at meditation.siri-mangalov.org.

170
00:13:02,880 --> 00:13:06,880
 We've got ten viewers on YouTube.

171
00:13:06,880 --> 00:13:08,880
 Small group.

172
00:13:08,880 --> 00:13:10,880
 A lot of meditators.

173
00:13:10,880 --> 00:13:11,880
 It's just nice.

174
00:13:11,880 --> 00:13:14,880
 We've got a long list of meditators tonight.

175
00:13:14,880 --> 00:13:23,880
 All right, well if no one's coming on, I'm going to go.

176
00:13:23,880 --> 00:13:32,880
 I'll be back tomorrow.

177
00:13:32,880 --> 00:13:36,880
 Have a good night everybody.

178
00:13:36,880 --> 00:13:51,880
 Bye.

