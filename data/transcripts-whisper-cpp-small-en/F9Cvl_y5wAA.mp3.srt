1
00:00:00,000 --> 00:00:04,490
 How would someone go about getting you to formally be their

2
00:00:04,490 --> 00:00:07,000
 teacher? What would that entail?

3
00:00:07,000 --> 00:00:11,000
 I get this from time to time.

4
00:00:11,000 --> 00:00:15,860
 How to take you on as my teacher even though I live

5
00:00:15,860 --> 00:00:19,000
 thousands of kilometers away.

6
00:00:19,000 --> 00:00:24,000
 I tried doing some online courses for a while.

7
00:00:24,000 --> 00:00:28,530
 Practically speaking, there's not that much I can do for

8
00:00:28,530 --> 00:00:32,000
 people who aren't in my immediate vicinity.

9
00:00:32,000 --> 00:00:35,000
 So the obvious answer is, well, if you want someone to be

10
00:00:35,000 --> 00:00:38,000
 their teacher, you should go to them.

11
00:00:38,000 --> 00:00:46,000
 I always think back to this movie, King of American Shaolin

12
00:00:46,000 --> 00:00:46,000
.

13
00:00:46,000 --> 00:00:49,810
 About this guy who goes... When I was younger, I saw this,

14
00:00:49,810 --> 00:00:53,000
 it was an 80s movie, I think.

15
00:00:53,000 --> 00:00:59,000
 He goes to the Shaolin monastery in China, I think.

16
00:00:59,000 --> 00:01:02,700
 They make him sit out in the courtyard, kind of like Fight

17
00:01:02,700 --> 00:01:06,000
 Club, which was a 90s movie, I think.

18
00:01:06,000 --> 00:01:08,770
 They make him sit out in the courtyard. They don't let him

19
00:01:08,770 --> 00:01:12,000
 in, so he sits out in the courtyard and he waits.

20
00:01:12,000 --> 00:01:17,080
 He spends days out there in the rain until they finally let

21
00:01:17,080 --> 00:01:18,000
 him in.

22
00:01:18,000 --> 00:01:22,010
 I always think back to, well, maybe that's sometimes how

23
00:01:22,010 --> 00:01:27,060
 hard it can be to get into a meditation center and find a

24
00:01:27,060 --> 00:01:28,000
 teacher.

25
00:01:28,000 --> 00:01:35,000
 It may not be easy, and sometimes expecting it to be easy,

26
00:01:35,000 --> 00:01:42,590
 it just may not be possible to find a teacher in your

27
00:01:42,590 --> 00:01:44,000
 immediate vicinity

28
00:01:44,000 --> 00:01:48,180
 or be able to stay at home and find a teacher. When I was

29
00:01:48,180 --> 00:01:51,270
 looking for a teacher, I got on a plane and flew to

30
00:01:51,270 --> 00:01:53,000
 Thailand

31
00:01:53,000 --> 00:01:59,440
 and just started wandering and finally found a place where

32
00:01:59,440 --> 00:02:02,000
 I began to practice.

33
00:02:02,000 --> 00:02:08,850
 So, practically speaking, there's not much that can be done

34
00:02:08,850 --> 00:02:09,000
.

35
00:02:09,000 --> 00:02:12,480
 But this is a different question. People often want to feel

36
00:02:12,480 --> 00:02:15,000
 somehow that they formally have a teacher.

37
00:02:15,000 --> 00:02:21,180
 It's very traditional and ritualistic, and I kind of shy

38
00:02:21,180 --> 00:02:25,000
 away from the labels that it involves and entails.

39
00:02:25,000 --> 00:02:29,240
 But in our tradition, it is common to take a teacher and

40
00:02:29,240 --> 00:02:33,000
 they have a whole ceremony for asking someone to,

41
00:02:33,000 --> 00:02:37,230
 not just to be your teacher, but asking for the meditation

42
00:02:37,230 --> 00:02:38,000
 training.

43
00:02:38,000 --> 00:02:45,000
 It's called "Kun Ka Madhan" which means entering into the

44
00:02:45,000 --> 00:02:50,600
 state of being a meditator, entering the meditation

45
00:02:50,600 --> 00:02:52,000
 practice.

46
00:02:52,000 --> 00:02:56,020
 So we can do that, but again, not something you do over the

47
00:02:56,020 --> 00:02:58,000
 internet, for example.

48
00:02:58,000 --> 00:03:02,470
 So I guess I'm going to go out on a limb or go out and say

49
00:03:02,470 --> 00:03:05,000
 that you'd have to come here.

50
00:03:05,000 --> 00:03:09,830
 If you want to take me or anyone as your teacher, the thing

51
00:03:09,830 --> 00:03:14,000
 to do is to find that person in person, visit them.

52
00:03:14,000 --> 00:03:17,350
 At the very least, visit them and have a ceremony. I mean,

53
00:03:17,350 --> 00:03:19,000
 that would maybe be something to do.

54
00:03:19,000 --> 00:03:22,430
 Come, do a ceremony and then give you some basic tips,

55
00:03:22,430 --> 00:03:25,000
 pointers, instruction on meditation,

56
00:03:25,000 --> 00:03:28,380
 and then you can go home and continue practice. And then,

57
00:03:28,380 --> 00:03:29,000
 like I know you,

58
00:03:29,000 --> 00:03:32,360
 and then I can have an excuse to say, "Well, this person I

59
00:03:32,360 --> 00:03:37,000
've met, so when they send me emails, I will respond to them

60
00:03:37,000 --> 00:03:37,000
."

61
00:03:37,000 --> 00:03:39,480
 Because otherwise, I don't respond to all the email

62
00:03:39,480 --> 00:03:46,420
 requests and inquiries into practice because it's just too

63
00:03:46,420 --> 00:03:47,000
 much.

64
00:03:47,000 --> 00:03:50,020
 So that would be something, that would be a step. Come,

65
00:03:50,020 --> 00:03:55,000
 meet, and we can even do an opening ceremony

66
00:03:55,000 --> 00:03:59,000
 to give you the meditation practice formally without...

