1
00:00:00,000 --> 00:00:02,500
 I wish to get enlightened, where do I start?

2
00:00:02,500 --> 00:00:06,100
 I love the good simple questions.

3
00:00:06,100 --> 00:00:08,700
 Well, start with my booklet on how to meditate.

4
00:00:08,700 --> 00:00:11,500
 Look up, Yuta demo how to meditate.

5
00:00:11,500 --> 00:00:13,100
 That's what I recommend to everyone.

6
00:00:13,100 --> 00:00:17,300
 Again, it's to toot my own horn,

7
00:00:17,300 --> 00:00:22,220
 but to explain that it's not really something that I've

8
00:00:22,220 --> 00:00:22,600
 taught,

9
00:00:22,600 --> 00:00:25,700
 it's just my explanation of what I've been taught.

10
00:00:25,700 --> 00:00:30,800
 Since you're asking me, this is the best I can do for you.

11
00:00:30,800 --> 00:00:33,300
 If you ask someone else, they'll give you quite a different

12
00:00:33,300 --> 00:00:33,900
 response,

13
00:00:33,900 --> 00:00:37,200
 but my response is what's in that book.

14
00:00:37,200 --> 00:00:40,930
 It's the best way to start to become enlightened from my

15
00:00:40,930 --> 00:00:41,700
 point of view.

16
00:00:41,700 --> 00:00:45,200
 Having done that, having read the booklet,

17
00:00:45,200 --> 00:00:48,560
 then you should make a decision whether you agree or

18
00:00:48,560 --> 00:00:49,200
 disagree,

19
00:00:49,200 --> 00:00:53,000
 or you should put it into practice.

20
00:00:53,000 --> 00:00:57,000
 Once you begin to practice the teachings in the booklet,

21
00:00:57,000 --> 00:00:59,500
 then if you decide you agree,

22
00:00:59,500 --> 00:01:03,030
 and it seems plausible that this is the way to become

23
00:01:03,030 --> 00:01:04,000
 enlightened,

24
00:01:04,000 --> 00:01:07,500
 then there are other steps that should be taken.

25
00:01:07,500 --> 00:01:11,000
 You should undertake a proper meditation course

26
00:01:11,000 --> 00:01:16,500
 with the proper guidance and go from there.

