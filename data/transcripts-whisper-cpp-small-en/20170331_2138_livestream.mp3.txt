 So it's not about grades.
 Another professor apparently is very much against exams.
 We didn't have an exam in her class
 because she doesn't think them useful.
 OK, nice to see you, Delany.
 Thanks for coming out.
 [AUDIO OUT]
 I think if a professor were to-- it
 would be fairly easy and productive and probably
 a really good environment if there were a professor
 or when there is a professor who's actually
 meditating and mindful.
 And I guess those are the best professors, the ones who
 are actually spiritual in the sense--
 in a general sense, not having an ideology that they cling
 to
 but that are actively working to better themselves
 and actually actively concerned about the goodness
 and what good they bring to their students.
