1
00:00:00,000 --> 00:00:04,520
 "Why is there ignorance in the first place?

2
00:00:04,520 --> 00:00:08,050
 I understand that ignorance is the root of all suffering

3
00:00:08,050 --> 00:00:09,320
 and that the best way to overcome

4
00:00:09,320 --> 00:00:11,040
 ignorance is to learn.

5
00:00:11,040 --> 00:00:13,480
 So I'm wondering what caused ignorance?

6
00:00:13,480 --> 00:00:15,440
 Can we even know?"

7
00:00:15,440 --> 00:00:21,080
 Well, there's no original cause to samsara.

8
00:00:21,080 --> 00:00:28,300
 I don't even think there's a sense that time is entirely

9
00:00:28,300 --> 00:00:29,800
 linear.

10
00:00:29,800 --> 00:00:33,600
 It seems like time somehow begins with the present moment

11
00:00:33,600 --> 00:00:35,520
 and so we're creating the past

12
00:00:35,520 --> 00:00:42,200
 as we go and the future is this also ethereal thing that we

13
00:00:42,200 --> 00:00:45,280
're somehow creating.

14
00:00:45,280 --> 00:00:48,590
 Or even more interestingly it seems like just as the past

15
00:00:48,590 --> 00:00:51,040
 can affect the present, the future,

16
00:00:51,040 --> 00:00:53,400
 can also affect the present.

17
00:00:53,400 --> 00:00:56,900
 So you know how in quantum physics they somehow have seen

18
00:00:56,900 --> 00:00:58,880
 that it seems like the future is

19
00:00:58,880 --> 00:01:00,480
 creating the past.

20
00:01:00,480 --> 00:01:05,450
 Well, there is a sense that because it's possible to have a

21
00:01:05,450 --> 00:01:07,920
 vision of the future, to see what's

22
00:01:07,920 --> 00:01:12,210
 going to happen before it happens, there's the sense that

23
00:01:12,210 --> 00:01:14,640
 the future can actually affect

24
00:01:14,640 --> 00:01:19,300
 the present by seeing the future or the future can come

25
00:01:19,300 --> 00:01:22,080
 back and you can actually see it

26
00:01:22,080 --> 00:01:24,040
 before it happens.

27
00:01:24,040 --> 00:01:27,010
 The Buddha was able to do this and it's possible through

28
00:01:27,010 --> 00:01:28,880
 certain types of meditation that you're

29
00:01:28,880 --> 00:01:30,280
 able to do that.

30
00:01:30,280 --> 00:01:33,960
 So anyway, not actually an answer to your question but if

31
00:01:33,960 --> 00:01:35,720
 you're looking for a first

32
00:01:35,720 --> 00:01:39,080
 cause of ignorance or a first cause of anything, there is

33
00:01:39,080 --> 00:01:40,160
 no first cause.

34
00:01:40,160 --> 00:01:42,720
 There is no sense of that.

35
00:01:42,720 --> 00:01:47,330
 There is no sense that the universe is of that sort, linear

36
00:01:47,330 --> 00:01:50,320
, because it's actually illogical

37
00:01:50,320 --> 00:01:53,000
 to think of.

38
00:01:53,000 --> 00:01:58,430
 It's mind-boggling to think of it an infinite timeline but

39
00:01:58,430 --> 00:02:01,520
 it's even more illogical to think

40
00:02:01,520 --> 00:02:04,720
 of a first beginning because then you have to think of

41
00:02:04,720 --> 00:02:07,000
 something before that or something

42
00:02:07,000 --> 00:02:08,880
 outside of time.

43
00:02:08,880 --> 00:02:13,780
 You have to think of, you have to try to understand what it

44
00:02:13,780 --> 00:02:17,360
 could possibly mean for time to begin.

45
00:02:17,360 --> 00:02:21,500
 Now, I guess there's an understanding among material

46
00:02:21,500 --> 00:02:24,360
 scientists or natural scientists,

47
00:02:24,360 --> 00:02:28,800
 modern scientists that time began with the Big Bang.

48
00:02:28,800 --> 00:02:31,510
 So if the Big Bang was a singularity, then there was no

49
00:02:31,510 --> 00:02:33,240
 time before that but that's really

50
00:02:33,240 --> 00:02:39,110
 just time as a function of space, as a part of the four-

51
00:02:39,110 --> 00:02:41,760
dimensional reality that we find

52
00:02:41,760 --> 00:02:45,440
 ourselves in.

53
00:02:45,440 --> 00:02:50,930
 But again, that may be a hint of the fact that time is not

54
00:02:50,930 --> 00:02:52,880
 exactly linear as we think

55
00:02:52,880 --> 00:02:53,880
 it is.

56
00:02:53,880 --> 00:02:57,640
 Time is not quite the way we think so the idea that there

57
00:02:57,640 --> 00:02:59,480
 might be something at the

58
00:02:59,480 --> 00:03:04,440
 beginning is a bit of a misunderstanding of reality.

59
00:03:04,440 --> 00:03:08,260
 If you want to understand the proximate cause of ignorance,

60
00:03:08,260 --> 00:03:10,080
 the Buddha said it's the five

61
00:03:10,080 --> 00:03:14,120
 hindrances and I was alerted to this.

62
00:03:14,120 --> 00:03:16,450
 I've answered this question before and I said, "Well, there

63
00:03:16,450 --> 00:03:19,280
's no ignorance, just is, right?

64
00:03:19,280 --> 00:03:23,800
 Ignorance, you can't cause someone to not know something.

65
00:03:23,800 --> 00:03:27,890
 If I'm thinking of a number between one and ten and I ask

66
00:03:27,890 --> 00:03:30,200
 you what number am I thinking

67
00:03:30,200 --> 00:03:34,630
 of, I didn't cause you to not know what number I'm thinking

68
00:03:34,630 --> 00:03:35,160
 of.

69
00:03:35,160 --> 00:03:38,240
 You never knew what number I was thinking of.

70
00:03:38,240 --> 00:03:41,240
 It was something that was always there.

71
00:03:41,240 --> 00:03:44,280
 The lack of knowledge was there in the beginning.

72
00:03:44,280 --> 00:03:47,380
 So I'm not actually convinced by this person who called me

73
00:03:47,380 --> 00:03:49,240
 out on that saying, "Oh, you're

74
00:03:49,240 --> 00:03:50,240
 wrong.

75
00:03:50,240 --> 00:03:51,720
 There is a cause for ignorance.

76
00:03:51,720 --> 00:03:53,640
 It's the five hindrances."

77
00:03:53,640 --> 00:03:57,100
 But you have to understand there are 24 different kinds of

78
00:03:57,100 --> 00:03:59,200
 causality in the Buddha's teaching.

79
00:03:59,200 --> 00:04:04,070
 And so I was talking about something that created the

80
00:04:04,070 --> 00:04:07,360
 ignorance, that actually caused

81
00:04:07,360 --> 00:04:09,440
 the ignorance to spring into existence.

82
00:04:09,440 --> 00:04:14,720
 But the hindrances are a cause for ignorance, meaning

83
00:04:14,720 --> 00:04:18,280
 because of the hindrances we remain

84
00:04:18,280 --> 00:04:20,020
 ignorant to the truth.

85
00:04:20,020 --> 00:04:23,560
 Because of our lack of clarity we remain ignorant.

86
00:04:23,560 --> 00:04:28,470
 So it's like the fog is preventing us from seeing the road

87
00:04:28,470 --> 00:04:29,980
 in front of us.

88
00:04:29,980 --> 00:04:32,920
 But it didn't create that lack of knowledge.

89
00:04:32,920 --> 00:04:35,440
 We never knew what the road in front of us looked like, but

90
00:04:35,440 --> 00:04:36,600
 then we get to this point

91
00:04:36,600 --> 00:04:38,640
 in the road.

92
00:04:38,640 --> 00:04:41,480
 We still can't see it because there's the fog.

93
00:04:41,480 --> 00:04:45,110
 So in that way the hindrances are what is obscuring us from

94
00:04:45,110 --> 00:04:46,480
 seeing the truth.

95
00:04:46,480 --> 00:04:51,040
 But we didn't ever know.

96
00:04:51,040 --> 00:04:55,040
 There wasn't a time or a point where we actually knew the

97
00:04:55,040 --> 00:04:57,560
 truth, the Four Noble Truths, for

98
00:04:57,560 --> 00:04:58,560
 example.

99
00:04:58,560 --> 00:05:00,840
 That isn't something that was ever there.

100
00:05:00,840 --> 00:05:04,320
 The hindrances are just that which is in our way.

101
00:05:04,320 --> 00:05:07,380
 Once we go to try and once we ask the question, "What are

102
00:05:07,380 --> 00:05:08,900
 the Four Noble Truths?

103
00:05:08,900 --> 00:05:10,900
 What is the truth of reality?"

104
00:05:10,900 --> 00:05:13,200
 We're not able to see it because of the five hindrances.

105
00:05:13,200 --> 00:05:14,200
 That's all.

106
00:05:14,200 --> 00:05:23,980
 An actual beginning to ignorance is something, it's

107
00:05:23,980 --> 00:05:29,000
 reasonably speaking a misleading question,

108
00:05:29,000 --> 00:05:31,000
 misguided question.

109
00:05:31,000 --> 00:05:34,240
 Ignorance is something that was always there.

110
00:05:34,240 --> 00:05:36,240
 Ignorance means a lack of something.

111
00:05:36,240 --> 00:05:38,640
 It's like, "Why is there a lack of light?

112
00:05:38,640 --> 00:05:41,640
 What caused there to be a lack of light?

113
00:05:41,640 --> 00:05:44,600
 What caused there to be a lack of sound?"

114
00:05:44,600 --> 00:05:45,800
 Nothing caused it.

115
00:05:45,800 --> 00:05:52,000
 A lack of something isn't really caused.

116
00:05:52,000 --> 00:05:54,810
 Though you can say there's a reason for it, maybe we're in

117
00:05:54,810 --> 00:05:56,520
 a soundproof room and that's

118
00:05:56,520 --> 00:05:57,780
 why there's no sound.

119
00:05:57,780 --> 00:06:01,040
 But that's not really telling you why there's no sound.

120
00:06:01,040 --> 00:06:05,920
 The no sound is the default.

121
00:06:05,920 --> 00:06:09,700
 I think it's easy to understand this and therefore I think

122
00:06:09,700 --> 00:06:11,600
 that's a proper answer.

123
00:06:11,600 --> 00:06:12,960
 It's not a proper answer to what is the cause of it.

