1
00:00:00,000 --> 00:00:02,000
 Okay.

2
00:00:02,000 --> 00:00:05,000
 When I meditate, I always want to investigate my thought.

3
00:00:05,000 --> 00:00:08,270
 Why I'm feeling that, what is the reason of my fear, what

4
00:00:08,270 --> 00:00:10,000
 is the cause of this.

5
00:00:10,000 --> 00:00:12,000
 Is it a good thing to do?

6
00:00:26,000 --> 00:00:30,000
 You have to understand what the purpose of meditation is.

7
00:00:30,000 --> 00:00:35,150
 Finding out the cause of things is not the true purpose of

8
00:00:35,150 --> 00:00:36,000
 meditation.

9
00:00:36,000 --> 00:00:38,000
 It's interesting.

10
00:00:38,000 --> 00:00:44,000
 But you have to see the flaw in what you're saying.

11
00:00:44,000 --> 00:00:51,000
 The reality is like a river.

12
00:00:51,000 --> 00:00:53,000
 Everything goes in order, right?

13
00:00:53,000 --> 00:00:56,000
 But on the other hand, it's quite complex.

14
00:00:56,000 --> 00:01:03,000
 So to say x is the cause of y is a simplistic understanding

15
00:01:03,000 --> 00:01:05,000
 of how things work.

16
00:01:05,000 --> 00:01:08,330
 If you sit there and what you're talking about is saying,

17
00:01:08,330 --> 00:01:11,000
 okay, I'm angry this morning.

18
00:01:11,000 --> 00:01:20,000
 And the cause is that I was unmindful.

19
00:01:20,000 --> 00:01:30,810
 And I allowed my anger to come to the processing of an

20
00:01:30,810 --> 00:01:33,000
 experience to lead to anger.

21
00:01:33,000 --> 00:01:35,000
 That's one part of the story.

22
00:01:35,000 --> 00:01:39,000
 The question is, why were you unmindful in the first place?

23
00:01:39,000 --> 00:01:40,800
 And what happened in between the time when you were unmind

24
00:01:40,800 --> 00:01:43,000
ful and the time when you got angry?

25
00:01:43,000 --> 00:01:45,000
 There's many things going on.

26
00:01:45,000 --> 00:01:49,170
 So I'm pointing one specific thing as saying this is a

27
00:01:49,170 --> 00:01:50,000
 cause.

28
00:01:50,000 --> 00:01:54,330
 Intellectually, it has some value then because you say, oh,

29
00:01:54,330 --> 00:01:57,000
 well, then I should be more mindful.

30
00:01:57,000 --> 00:02:00,840
 And that sort of thing does happen in meditation, but that

31
00:02:00,840 --> 00:02:03,000
's not meditation.

32
00:02:03,000 --> 00:02:06,000
 It doesn't solve the problem.

33
00:02:08,000 --> 00:02:13,880
 In fact, meditation does begin by identifying momentary

34
00:02:13,880 --> 00:02:15,000
 causes.

35
00:02:15,000 --> 00:02:20,450
 So from one moment to the next, seeing what mind precedes,

36
00:02:20,450 --> 00:02:25,000
 what cause precedes what result.

37
00:02:25,000 --> 00:02:29,440
 But it's still a very limited, a very preliminary basic

38
00:02:29,440 --> 00:02:31,000
 understanding.

39
00:02:31,000 --> 00:02:33,000
 That understanding is also not enough.

40
00:02:33,000 --> 00:02:36,000
 It's not the true reason for practicing.

41
00:02:36,000 --> 00:02:37,000
 It's not the highest insight.

42
00:02:37,000 --> 00:02:41,230
 The highest insight is to see that things arise based on a

43
00:02:41,230 --> 00:02:42,000
 cause.

44
00:02:42,000 --> 00:02:45,000
 To see things are dependently risen.

45
00:02:45,000 --> 00:02:46,000
 That's it.

46
00:02:46,000 --> 00:02:50,920
 To not what was the cause, but to see the things arising

47
00:02:50,920 --> 00:02:52,000
 and ceasing.

48
00:02:52,000 --> 00:02:54,000
 It comes and then it goes.

49
00:02:54,000 --> 00:02:56,000
 So that's to see impermanence.

50
00:02:56,000 --> 00:03:00,000
 It's also to see sadhukha, which means inability to satisfy

51
00:03:00,000 --> 00:03:02,000
 because it's impermanent.

52
00:03:02,000 --> 00:03:05,000
 And non-self, to see that there's no essence to it.

53
00:03:05,000 --> 00:03:09,000
 It's about the Dhamma itself, about the experience itself.

54
00:03:09,000 --> 00:03:11,000
 That's what we're most interested in.

55
00:03:11,000 --> 00:03:19,330
 Because the cause of suffering is simply attachment to

56
00:03:19,330 --> 00:03:21,000
 things.

57
00:03:21,000 --> 00:03:25,260
 So the cause in effect doesn't directly, indirectly can be

58
00:03:25,260 --> 00:03:28,000
 useful, but directly does not tie into that.

59
00:03:28,000 --> 00:03:32,950
 The craving is simply caused by seeing that things are

60
00:03:32,950 --> 00:03:37,000
 stable, satisfying and controllable.

61
00:03:37,000 --> 00:03:40,620
 Once you see that things are not thus, then you let go of

62
00:03:40,620 --> 00:03:41,000
 them.

63
00:03:41,000 --> 00:03:44,000
 And when you let go of them, you become free.

64
00:03:44,000 --> 00:03:46,000
 When you're free, you say, "I'm free."

65
00:03:46,000 --> 00:03:48,000
 Nothing more needs to be done.

66
00:03:48,000 --> 00:03:50,000
 So is it useful?

67
00:03:50,000 --> 00:03:54,440
 Yes, arguably that has some intellectual benefit because it

68
00:03:54,440 --> 00:03:55,000
's going to say,

69
00:03:55,000 --> 00:03:57,400
 "Well, then I shouldn't do that," or "I should adjust

70
00:03:57,400 --> 00:03:58,000
 myself."

71
00:03:58,000 --> 00:04:03,310
 It allows you to be sort of a meta-analysis, M-E-T-A, that

72
00:04:03,310 --> 00:04:05,000
 allows you to adjust your practice.

73
00:04:05,000 --> 00:04:09,000
 This we would call wimangsa, it allows you to succeed.

74
00:04:09,000 --> 00:04:10,000
 So there's benefit to it.

75
00:04:10,000 --> 00:04:12,000
 Don't think that it's a replacement to meditation.

76
00:04:12,000 --> 00:04:13,000
 It's not.

77
00:04:13,000 --> 00:04:17,000
 It's a non-meditative process.

78
00:04:17,000 --> 00:04:21,400
 At that moment, you're actually not meditating because you

79
00:04:21,400 --> 00:04:24,000
're not observing things simply as they are.

80
00:04:24,000 --> 00:04:26,630
 You're reflecting on them, you're intellectualizing about

81
00:04:26,630 --> 00:04:27,000
 them,

82
00:04:27,000 --> 00:04:31,950
 you're reflecting processing your memories of things, which

83
00:04:31,950 --> 00:04:34,000
 in fact can be flawed.

84
00:04:34,000 --> 00:04:35,000
 But that's not the biggest problem.

85
00:04:35,000 --> 00:04:38,000
 The biggest problem is its memories.

86
00:04:38,000 --> 00:04:40,000
 It's, "Okay, this happened like this, happened like this,"

87
00:04:40,000 --> 00:04:42,000
 which is all in the past

88
00:04:42,000 --> 00:04:49,000
 and cannot be a replacement for true insight.

89
00:04:49,000 --> 00:04:54,000
 So, not wrong, not bad, but not insight-meditative.

