1
00:00:00,000 --> 00:00:16,000
 Good evening everyone.

2
00:00:16,000 --> 00:00:32,000
 I'm broadcasting live April 24th, 2016.

3
00:00:32,000 --> 00:00:42,000
 Today's quote is part of a famous story

4
00:00:42,000 --> 00:00:48,000
 about a monk who had dysentery.

5
00:00:48,000 --> 00:00:55,000
 And if you read this,

6
00:00:55,000 --> 00:00:57,000
 the Buddha asks him,

7
00:00:57,000 --> 00:01:00,000
 "Why aren't the other monks looking after you?"

8
00:01:00,000 --> 00:01:01,000
 And he says,

9
00:01:01,000 --> 00:01:03,000
 "Because I'm not useful to them.

10
00:01:03,000 --> 00:01:07,000
 I'm of no use to them."

11
00:01:07,000 --> 00:01:09,000
 And then he goes and asks the monks,

12
00:01:09,000 --> 00:01:10,000
 and the monks say,

13
00:01:10,000 --> 00:01:12,000
 "Why didn't you look after him?"

14
00:01:12,000 --> 00:01:21,000
 And they confirm it's because he is no use to them.

15
00:01:21,000 --> 00:01:27,000
 "Ati pana bhikkhu veta sabhikkhu na upa takoti."

16
00:01:27,000 --> 00:01:29,000
 Is there any monk who's looking after him?

17
00:01:29,000 --> 00:01:30,000
 "Nati bhagavah"

18
00:01:30,000 --> 00:01:34,000
 No, there is no such person.

19
00:01:34,000 --> 00:01:38,000
 "Kisatang bhikkhu na upa tainti."

20
00:01:38,000 --> 00:01:46,000
 For what reason did the bhikkhus not look after?

21
00:01:46,000 --> 00:01:48,000
 So not look after this monk.

22
00:01:48,000 --> 00:01:54,000
 "Eso bhante bhikkhu bhikkhu na akarakoh."

23
00:01:54,000 --> 00:02:00,000
 Akarakoh means he does nothing for them.

24
00:02:00,000 --> 00:02:02,000
 Not quite sure what that means.

25
00:02:02,000 --> 00:02:06,000
 "Bhikkhu bhikkhu na for the bhikkhus akarakoh."

26
00:02:06,000 --> 00:02:09,000
 He doesn't do things for the bhikkhus.

27
00:02:09,000 --> 00:02:11,000
 "Tenatang bhikkhu na upa tainti."

28
00:02:11,000 --> 00:02:17,630
 For that reason, because of that, they don't look after him

29
00:02:17,630 --> 00:02:18,000
.

30
00:02:18,000 --> 00:02:20,000
 The Buddha says something,

31
00:02:20,000 --> 00:02:26,000
 "Nati wo bhikkhu vaymata."

32
00:02:26,000 --> 00:02:28,000
 There is no...

33
00:02:28,000 --> 00:02:32,000
 You have no mother, monks.

34
00:02:32,000 --> 00:02:36,000
 "Nati pita" You have no father.

35
00:02:36,000 --> 00:02:39,000
 "Yevo upata hai yung."

36
00:02:39,000 --> 00:02:45,000
 Who would look after you?

37
00:02:45,000 --> 00:02:53,820
 "Dum hei jay bhikkve anyaman yangna patah upata hi sata ata

38
00:02:53,820 --> 00:03:00,000
 ko jara hi upata hi sati."

39
00:03:00,000 --> 00:03:03,000
 "Tatakko jara hi."

40
00:03:03,000 --> 00:03:04,000
 "Jara hi."

41
00:03:04,000 --> 00:03:06,000
 What is "jara hi"?

42
00:03:06,000 --> 00:03:08,000
 "Koh jara hi."

43
00:03:08,000 --> 00:03:09,000
 Now...

44
00:03:09,000 --> 00:03:16,000
 Oh, I've never seen that before.

45
00:03:16,000 --> 00:03:20,110
 "If you don't look after each other, monks, anyaman yang,

46
00:03:20,110 --> 00:03:26,000
 one to the other, each other, ata ko jara hi."

47
00:03:26,000 --> 00:03:32,000
 Then, "Who now will look after you?"

48
00:03:32,000 --> 00:03:34,000
 Then he says something that's quite famous.

49
00:03:34,000 --> 00:03:40,000
 "Yobikwe mang upata hi ya, so gilaanang upata hi ya."

50
00:03:40,000 --> 00:03:48,000
 "Who would look after me should look after sick people."

51
00:03:48,000 --> 00:03:50,680
 It doesn't actually say monks, but that's the implication

52
00:03:50,680 --> 00:03:52,000
 here.

53
00:03:52,000 --> 00:03:54,000
 Many would argue that that's what it means.

54
00:03:54,000 --> 00:03:57,890
 It's been used to interpret the importance of looking after

55
00:03:57,890 --> 00:03:59,000
 sick people.

56
00:03:59,000 --> 00:04:01,000
 I think that's stretching.

57
00:04:01,000 --> 00:04:03,000
 It's probably more likely...

58
00:04:03,000 --> 00:04:05,810
 Because monks aren't really allowed to look after lay

59
00:04:05,810 --> 00:04:09,000
 people who are sick, that wouldn't be appropriate.

60
00:04:09,000 --> 00:04:13,330
 But it's interesting if you think of it as a lay person, if

61
00:04:13,330 --> 00:04:16,000
 you're not a monk,

62
00:04:16,000 --> 00:04:19,000
 to think about the idea of helping sick people is very...

63
00:04:19,000 --> 00:04:24,500
 It's a very important thing, the Buddha actually found this

64
00:04:24,500 --> 00:04:28,000
 to be important for the monks at least,

65
00:04:28,000 --> 00:04:39,450
 if not for general purposes or in society looking after

66
00:04:39,450 --> 00:04:45,000
 sick people in general.

67
00:04:45,000 --> 00:04:49,000
 But what's of most interest to me is the part where he says

68
00:04:49,000 --> 00:04:50,000
, "Nati wo bhikve mata."

69
00:04:50,000 --> 00:04:55,120
 "You don't have a mother or a father who would look after

70
00:04:55,120 --> 00:04:56,000
 you."

71
00:04:56,000 --> 00:04:59,950
 He's making a very good point here, and I think it goes

72
00:04:59,950 --> 00:05:01,000
 beyond monasticism.

73
00:05:01,000 --> 00:05:05,730
 Obviously for monks it's an important point, is that we don

74
00:05:05,730 --> 00:05:08,000
't have family, we don't have caretakers.

75
00:05:08,000 --> 00:05:15,000
 If we don't look after each other, who will look after us?

76
00:05:15,000 --> 00:05:25,040
 But I think on a broader level, this applies to Buddhists

77
00:05:25,040 --> 00:05:27,000
 in general.

78
00:05:27,000 --> 00:05:33,000
 In Thailand they talk about "nati dhamma," "dhamma nati,"

79
00:05:33,000 --> 00:05:35,000
 relatives by the dhamma,

80
00:05:35,000 --> 00:05:42,000
 sisters and brothers and mothers and fathers in the dhamma.

81
00:05:42,000 --> 00:05:49,240
 Because beyond monastics it's true that, especially when

82
00:05:49,240 --> 00:05:51,000
 you get on a spiritual path,

83
00:05:51,000 --> 00:05:54,620
 when you start to practice Buddhism, you're less and less

84
00:05:54,620 --> 00:05:57,000
 able to rely upon your families

85
00:05:57,000 --> 00:06:01,570
 and the people around you who are not Buddhist or not

86
00:06:01,570 --> 00:06:03,000
 practicing.

87
00:06:03,000 --> 00:06:07,270
 Even in Buddhist countries, if your family is not med

88
00:06:07,270 --> 00:06:10,000
itating, in Buddhist countries they drink alcohol,

89
00:06:10,000 --> 00:06:17,000
 they kill, they lie, they cheat, all these things.

90
00:06:17,000 --> 00:06:20,000
 It's because someone calls themselves even Buddhists.

91
00:06:20,000 --> 00:06:24,330
 But much more for those of us who have become Buddhists or

92
00:06:24,330 --> 00:06:28,000
 come to practice Buddhism,

93
00:06:28,000 --> 00:06:37,440
 maybe against the hopes of our family, maybe against their

94
00:06:37,440 --> 00:06:39,000
 wishes.

95
00:06:39,000 --> 00:06:44,740
 But certainly we leave them behind, we go somewhere that

96
00:06:44,740 --> 00:06:46,000
 they don't follow.

97
00:06:46,000 --> 00:06:50,440
 So in a sense we leave them behind, meaning they're in one

98
00:06:50,440 --> 00:06:55,000
 place, we're in another, spiritually.

99
00:06:55,000 --> 00:06:59,000
 So we stop killing, they still kill, we stop stealing.

100
00:06:59,000 --> 00:07:04,000
 Maybe they don't concern themselves about morality.

101
00:07:04,000 --> 00:07:10,000
 They have no concentration, they don't cultivate wisdom,

102
00:07:10,000 --> 00:07:15,020
 they don't have the same understanding which we would call

103
00:07:15,020 --> 00:07:16,000
 wisdom.

104
00:07:16,000 --> 00:07:24,680
 It's interesting because last night, I just got back from

105
00:07:24,680 --> 00:07:25,000
 New York,

106
00:07:25,000 --> 00:07:29,000
 but last night, yesterday was my mother's birthday.

107
00:07:29,000 --> 00:07:36,370
 And so we had a hangout, we had one of these, basically

108
00:07:36,370 --> 00:07:37,000
 what I'm doing now,

109
00:07:37,000 --> 00:07:41,000
 not live of course, or not public.

110
00:07:41,000 --> 00:07:45,100
 But we got together from all around the world, my brother's

111
00:07:45,100 --> 00:07:46,000
 in Taiwan,

112
00:07:46,000 --> 00:07:50,260
 my other two brothers are in, well they were actually with

113
00:07:50,260 --> 00:07:51,000
 my father.

114
00:07:51,000 --> 00:07:55,000
 So I think they were only, well, they were with my father,

115
00:07:55,000 --> 00:07:58,680
 and then my mother's in Florida and I was in New York, so

116
00:07:58,680 --> 00:08:06,000
 four different places.

117
00:08:06,000 --> 00:08:12,000
 And so we got together, we joined the hangout,

118
00:08:12,000 --> 00:08:19,500
 and it was actually somewhat disappointing, if you're going

119
00:08:19,500 --> 00:08:21,000
 to let yourself get disappointed.

120
00:08:21,000 --> 00:08:24,000
 I mean, what is the right word?

121
00:08:24,000 --> 00:08:32,000
 It was awkward, maybe.

122
00:08:32,000 --> 00:08:38,400
 For a couple, because one of the members of the hangout was

123
00:08:38,400 --> 00:08:40,000
 clearly drunk,

124
00:08:40,000 --> 00:08:42,960
 and because it was Passover last night, if you know the

125
00:08:42,960 --> 00:08:44,000
 Jewish holiday,

126
00:08:44,000 --> 00:08:50,140
 my father's family's Jewish, on Passover they drink a lot

127
00:08:50,140 --> 00:08:52,000
 of wine.

128
00:08:52,000 --> 00:08:57,000
 And can get somewhat drunk, and so at least one person,

129
00:08:57,000 --> 00:09:04,680
 if not more, was somewhat inebriated and slurring their

130
00:09:04,680 --> 00:09:06,000
 words.

131
00:09:06,000 --> 00:09:10,990
 So the conversation quickly turned into things like alcohol

132
00:09:10,990 --> 00:09:12,000
 and marijuana,

133
00:09:12,000 --> 00:09:21,420
 and talking about how, I mean, I was quiet through most of

134
00:09:21,420 --> 00:09:22,000
 it.

135
00:09:22,000 --> 00:09:25,000
 My mother's really great, she doesn't do any of that.

136
00:09:25,000 --> 00:09:28,000
 I think she smokes some marijuana, she used to anyway.

137
00:09:28,000 --> 00:09:33,520
 But she really gave up alcohol and she's interested in a

138
00:09:33,520 --> 00:09:35,000
 clear mind.

139
00:09:35,000 --> 00:09:40,000
 I mean, I think she doesn't drink.

140
00:09:40,000 --> 00:09:47,950
 But then my brother in Taiwan, he started talking about

141
00:09:47,950 --> 00:09:50,000
 this, something hairier,

142
00:09:50,000 --> 00:09:55,000
 this hairier group, and they'd play this game where one, I

143
00:09:55,000 --> 00:09:55,000
 wasn't listening really,

144
00:09:55,000 --> 00:09:58,000
 but was listening to the part where at the end of the game

145
00:09:58,000 --> 00:10:01,000
 they come together

146
00:10:01,000 --> 00:10:05,000
 and they have a champagne breakfast, I think it was called,

147
00:10:05,000 --> 00:10:10,000
 and he was showing pictures of a case like crate, you know,

148
00:10:10,000 --> 00:10:15,280
 a large cooler's full of champagne, whiskey and beer and

149
00:10:15,280 --> 00:10:17,000
 every kind of alcohol.

150
00:10:17,000 --> 00:10:22,000
 And he said he drank for 20 hours straight.

151
00:10:22,000 --> 00:10:25,000
 Twenty hours.

152
00:10:25,000 --> 00:10:27,010
 And he said, "You know, I don't want to talk about it

153
00:10:27,010 --> 00:10:30,000
 because it sounds like I'm bragging."

154
00:10:30,000 --> 00:10:33,000
 And the rest of us were kind of well, you know.

155
00:10:33,000 --> 00:10:35,280
 But then he started saying something that's really

156
00:10:35,280 --> 00:10:36,000
 interesting.

157
00:10:36,000 --> 00:10:39,540
 I mean, there's a point why I'm telling this and why I'm

158
00:10:39,540 --> 00:10:40,000
 talking about things

159
00:10:40,000 --> 00:10:43,220
 that probably I shouldn't, this isn't the kind of thing you

160
00:10:43,220 --> 00:10:45,000
 should really broadcast.

161
00:10:45,000 --> 00:10:49,080
 But, you know, we take this, I'm not really criticizing, I

162
00:10:49,080 --> 00:10:53,000
'm looking at the state of people.

163
00:10:53,000 --> 00:11:06,450
 It's interesting because then he went on about how much

164
00:11:06,450 --> 00:11:08,000
 better it is,

165
00:11:08,000 --> 00:11:13,000
 this kind of life, how much healthier it is,

166
00:11:13,000 --> 00:11:16,000
 and how he looks at people, the rest of us back,

167
00:11:16,000 --> 00:11:19,980
 or people back in Canada, all of his friends and their hair

168
00:11:19,980 --> 00:11:24,000
 is turning gray and they look old.

169
00:11:24,000 --> 00:11:29,540
 And it's because they're serious, they're too serious, they

170
00:11:29,540 --> 00:11:33,000
 take things too seriously.

171
00:11:33,000 --> 00:11:35,990
 And I mean, it was quite clear that drinking for 20 hours

172
00:11:35,990 --> 00:11:39,000
 was somehow

173
00:11:39,000 --> 00:11:44,000
 the fountain of youth or something.

174
00:11:44,000 --> 00:11:48,930
 Living in Taiwan, I'm sorry, I don't mean to bring my

175
00:11:48,930 --> 00:11:50,000
 brother into this really,

176
00:11:50,000 --> 00:11:54,000
 but it's such a good example because we think like this,

177
00:11:54,000 --> 00:12:00,000
 we think happiness leads to happiness.

178
00:12:00,000 --> 00:12:04,320
 I mean, it sounded so much like the angels, how angels

179
00:12:04,320 --> 00:12:06,000
 think of heaven.

180
00:12:06,000 --> 00:12:13,810
 They think that it's the good life, they've won, they think

181
00:12:13,810 --> 00:12:15,000
 they've won.

182
00:12:15,000 --> 00:12:19,370
 They've beaten samsara and they've found the right way to

183
00:12:19,370 --> 00:12:20,000
 live,

184
00:12:20,000 --> 00:12:27,000
 they've earned it and they've found the right path

185
00:12:27,000 --> 00:12:29,940
 and they're doing the right thing when in fact they're

186
00:12:29,940 --> 00:12:31,000
 doing nothing.

187
00:12:31,000 --> 00:12:37,000
 Like Wysakka said, they're eating stale food.

188
00:12:37,000 --> 00:12:43,810
 It means that they're eating up their, using up their good

189
00:12:43,810 --> 00:12:46,000
 karma from the past.

190
00:12:46,000 --> 00:12:49,000
 We all have this potential in life, right?

191
00:12:49,000 --> 00:12:56,000
 We have the potential to use this great opportunity we have

192
00:12:56,000 --> 00:12:57,000
 as human beings.

193
00:12:57,000 --> 00:13:01,000
 We have such power, we can do so many things as humans.

194
00:13:01,000 --> 00:13:04,000
 And people say, well, what's so great about being a human?

195
00:13:04,000 --> 00:13:06,000
 Humans can be horrible people.

196
00:13:06,000 --> 00:13:11,580
 I was just reading about some of the things that have gone

197
00:13:11,580 --> 00:13:12,000
 on,

198
00:13:12,000 --> 00:13:18,000
 like in South and Central America, the dictatorships that

199
00:13:18,000 --> 00:13:21,000
 were set up by the CIA

200
00:13:21,000 --> 00:13:25,000
 or whatever, I don't know, but these dictatorships,

201
00:13:25,000 --> 00:13:36,000
 there was this, they had this prison in a stadium

202
00:13:36,000 --> 00:13:40,000
 and they sat all the prisoners in the crowds

203
00:13:40,000 --> 00:13:44,110
 and then they just like opened fire on them, shot them with

204
00:13:44,110 --> 00:13:45,000
 machine guns.

205
00:13:45,000 --> 00:13:48,000
 But that was in it, it went, like they would drag,

206
00:13:48,000 --> 00:13:51,000
 every so often they would drag someone down from the stands

207
00:13:51,000 --> 00:13:55,000
 and torture them in front of everyone like theater.

208
00:13:55,000 --> 00:14:02,000
 And at that point, human beings can do terrible things.

209
00:14:02,000 --> 00:14:07,030
 And so we have this power, we have the power to live a he

210
00:14:07,030 --> 00:14:09,000
donistic lifestyle,

211
00:14:09,000 --> 00:14:15,550
 to just enjoy as much pleasure as we can, to not take

212
00:14:15,550 --> 00:14:18,000
 anything seriously.

213
00:14:18,000 --> 00:14:21,900
 And no matter what we do, it appears that we get away with

214
00:14:21,900 --> 00:14:22,000
 it,

215
00:14:22,000 --> 00:14:26,160
 for the most part, not always, but much of the time for

216
00:14:26,160 --> 00:14:27,000
 many of the people in this world,

217
00:14:27,000 --> 00:14:33,000
 it appears that we get away with whatever we do.

218
00:14:33,000 --> 00:14:35,990
 But it's quite lazy to think like that, like take alcohol,

219
00:14:35,990 --> 00:14:37,000
 for example,

220
00:14:37,000 --> 00:14:41,000
 someone who says, look at alcohol, drink and be merry,

221
00:14:41,000 --> 00:14:49,000
 and it's a good way to live. Look at me, I'm doing great.

222
00:14:49,000 --> 00:14:52,970
 This anecdotal evidence, look at how great I am, and I'm

223
00:14:52,970 --> 00:14:55,000
 doing this.

224
00:14:55,000 --> 00:14:58,000
 There was a funny story a monk once told me,

225
00:14:58,000 --> 00:15:02,000
 or I overheard him telling someone else.

226
00:15:02,000 --> 00:15:13,000
 This man walked in wrinkled, with a pockmarked face,

227
00:15:13,000 --> 00:15:18,000
 and looked to be like just really aged.

228
00:15:18,000 --> 00:15:27,050
 And this man walked in, but he had bright eyes, he looked

229
00:15:27,050 --> 00:15:28,000
 ancient,

230
00:15:28,000 --> 00:15:32,000
 he had some radiance about him,

231
00:15:32,000 --> 00:15:37,020
 and he started talking about how he's never been in the

232
00:15:37,020 --> 00:15:38,000
 hospital,

233
00:15:38,000 --> 00:15:43,000
 and he's never been sick.

234
00:15:43,000 --> 00:15:51,000
 In all his life, he's never had any serious health issues.

235
00:15:51,000 --> 00:15:56,000
 And these young men were asking him, oh, what do you do?

236
00:15:56,000 --> 00:16:01,000
 And he says, oh, I drink a bottle of whiskey every day,

237
00:16:01,000 --> 00:16:06,610
 smoke a carton of cigarettes, and a pack of cigarettes a

238
00:16:06,610 --> 00:16:08,000
 day,

239
00:16:08,000 --> 00:16:13,010
 and a bottle of whiskey, and on and on, and I just sit

240
00:16:13,010 --> 00:16:15,000
 around and do nothing.

241
00:16:15,000 --> 00:16:18,680
 And they're like, wow, and that works, yeah, it works so

242
00:16:18,680 --> 00:16:21,000
 far, it works for me.

243
00:16:21,000 --> 00:16:27,090
 And then someone asked him, how old are you? And he said,

244
00:16:27,090 --> 00:16:28,000
 25.

245
00:16:28,000 --> 00:16:33,000
 The joke sets him up to seem like he's very old,

246
00:16:33,000 --> 00:16:36,000
 like this old guy is bragging about how he's old.

247
00:16:36,000 --> 00:16:40,510
 It turns out he's a young guy who looks very old, because,

248
00:16:40,510 --> 00:16:41,000
 anyway,

249
00:16:41,000 --> 00:16:43,000
 it was just a silly joke.

250
00:16:43,000 --> 00:16:49,990
 But there are studies on alcohol, what it does to the brain

251
00:16:49,990 --> 00:16:50,000
.

252
00:16:50,000 --> 00:16:54,000
 As I understand, it's not good for the brain.

253
00:16:54,000 --> 00:16:58,000
 It causes long-term brain damage.

254
00:16:58,000 --> 00:17:03,120
 I mean, not severe brain damage, but it reduces your brain

255
00:17:03,120 --> 00:17:04,000
 capacity,

256
00:17:04,000 --> 00:17:12,000
 as does marijuana, according to this study.

257
00:17:12,000 --> 00:17:19,000
 But moreover, happiness doesn't lead to happiness.

258
00:17:19,000 --> 00:17:28,000
 Anybody can be happy when things are good,

259
00:17:28,000 --> 00:17:32,000
 as long as they have the ability to enjoy.

260
00:17:32,000 --> 00:17:38,000
 That's not how you measure someone's greatness.

261
00:17:38,000 --> 00:17:41,000
 You don't measure someone's greatness by how good they are

262
00:17:41,000 --> 00:17:43,000
 at enjoying pleasure.

263
00:17:43,000 --> 00:17:48,290
 You measure someone's greatness by how good they are at

264
00:17:48,290 --> 00:17:51,000
 bearing with adversity.

265
00:17:51,000 --> 00:17:53,580
 When the going gets tough, that's when you know the

266
00:17:53,580 --> 00:17:55,000
 character of the person.

267
00:17:55,000 --> 00:17:58,000
 You can't say, "Wow, that guy sure knows how to have fun."

268
00:17:58,000 --> 00:18:00,000
 Well, it's not that hard to have...

269
00:18:00,000 --> 00:18:02,750
 Okay, yes, it's true. Some people don't know how to have

270
00:18:02,750 --> 00:18:03,000
 fun,

271
00:18:03,000 --> 00:18:10,650
 and you could argue that, but it's much easier when you're

272
00:18:10,650 --> 00:18:12,000
 in your comfort zone.

273
00:18:12,000 --> 00:18:16,440
 So if someone is good at having fun while drinking 20 hours

274
00:18:16,440 --> 00:18:20,000
 a day, that is impressive, I'll admit.

275
00:18:20,000 --> 00:18:24,000
 It's a skill that you've developed.

276
00:18:24,000 --> 00:18:28,030
 But let's see what happens as a result of developing that

277
00:18:28,030 --> 00:18:30,000
 skill and that ability.

278
00:18:30,000 --> 00:18:36,010
 Let's see what happens when you're placed in a situation

279
00:18:36,010 --> 00:18:39,000
 that is challenging to you.

280
00:18:39,000 --> 00:18:43,580
 I mean, does this behavior increase your ability to deal

281
00:18:43,580 --> 00:18:45,000
 with challenges?

282
00:18:45,000 --> 00:18:49,740
 If suddenly the country that you're in becomes a fascist

283
00:18:49,740 --> 00:18:51,000
 dictatorship,

284
00:18:51,000 --> 00:18:55,330
 and they start to torture people, and you are being

285
00:18:55,330 --> 00:18:58,000
 tortured, how do you react?

286
00:18:58,000 --> 00:19:04,000
 How do you bear with it?

287
00:19:04,000 --> 00:19:12,630
 This story of this auditorium, or this stadium, there was a

288
00:19:12,630 --> 00:19:14,000
 guy, one of the leaders,

289
00:19:14,000 --> 00:19:20,240
 he was a musician, I think, and he was leading people in

290
00:19:20,240 --> 00:19:23,000
 chanting the anthem,

291
00:19:23,000 --> 00:19:28,410
 the national anthem or something, and so they called him

292
00:19:28,410 --> 00:19:31,000
 down, and they asked him to sing.

293
00:19:31,000 --> 00:19:35,970
 And so he started singing, and then they strapped him to

294
00:19:35,970 --> 00:19:40,000
 this table, and they smashed his fingers.

295
00:19:40,000 --> 00:19:43,000
 He was playing a guitar, they got him to play the guitar,

296
00:19:43,000 --> 00:19:44,000
 and then they smashed his fingers,

297
00:19:44,000 --> 00:19:46,580
 or like cut them off, cut off his fingers or something like

298
00:19:46,580 --> 00:19:47,000
 that.

299
00:19:47,000 --> 00:19:50,010
 He cut off his fingers and then smashed his hands to a

300
00:19:50,010 --> 00:19:51,000
 bloody pulp.

301
00:19:51,000 --> 00:19:54,000
 They did this, this happened, apparently.

302
00:19:54,000 --> 00:20:00,000
 And then they said, "Now sing, now play your guitar."

303
00:20:00,000 --> 00:20:04,630
 And so he stood up, and he turned to the crowd, to the

304
00:20:04,630 --> 00:20:08,000
 other prisoners in the stadium,

305
00:20:08,000 --> 00:20:13,000
 and he led them in the anthem or something like that.

306
00:20:13,000 --> 00:20:16,150
 It's an interesting story because of how he dealt with the

307
00:20:16,150 --> 00:20:17,000
 adversity.

308
00:20:17,000 --> 00:20:24,130
 Singing is not such a noble thing in our books, but the

309
00:20:24,130 --> 00:20:28,000
 nobility of being able to deal with adversity.

310
00:20:28,000 --> 00:20:33,230
 I'm not saying what is it that leads to the ability to deal

311
00:20:33,230 --> 00:20:35,000
 with adversity,

312
00:20:35,000 --> 00:20:38,750
 but that's the question. What is it that leads to adversity

313
00:20:38,750 --> 00:20:39,000
?

314
00:20:39,000 --> 00:20:43,180
 If you believe that the ability, the skill of being able to

315
00:20:43,180 --> 00:20:46,000
 drink a lot helps you deal with adversity,

316
00:20:46,000 --> 00:20:52,000
 that's one theory.

317
00:20:52,000 --> 00:20:56,000
 But we can't say what the future is going to bring.

318
00:20:56,000 --> 00:21:01,680
 And certainly in Cambodia, for example, before the Pol Pot

319
00:21:01,680 --> 00:21:03,000
 massacre.

320
00:21:03,000 --> 00:21:05,520
 People were living good. There were probably a lot of

321
00:21:05,520 --> 00:21:06,000
 people who were saying,

322
00:21:06,000 --> 00:21:12,690
 "Oh look, life is good, everything's great. Eat, drink and

323
00:21:12,690 --> 00:21:15,000
 be merry. That doesn't help you."

324
00:21:15,000 --> 00:21:21,380
 I would argue it probably doesn't do much to prepare you

325
00:21:21,380 --> 00:21:24,000
 for being tortured.

326
00:21:24,000 --> 00:21:28,000
 I have an extreme example, but examples abound.

327
00:21:28,000 --> 00:21:34,600
 Suppose you get cancer. Is the ability to enjoy pleasure

328
00:21:34,600 --> 00:21:38,000
 going to help you with that?

329
00:21:38,000 --> 00:21:45,070
 So if you begin to practice meditation, you start to see

330
00:21:45,070 --> 00:21:47,000
 the benefits in this way.

331
00:21:47,000 --> 00:21:49,000
 You can reflect on this as a sort of benefit.

332
00:21:49,000 --> 00:21:51,330
 "Wow, I'm really able to deal with things that I wouldn't

333
00:21:51,330 --> 00:21:53,000
 have been able to deal with before."

334
00:21:53,000 --> 00:21:57,810
 That's really something quite obvious in the meditation

335
00:21:57,810 --> 00:21:59,000
 practice.

336
00:21:59,000 --> 00:22:04,190
 You learn, you see your reactions, and you learn how to

337
00:22:04,190 --> 00:22:05,000
 avoid those,

338
00:22:05,000 --> 00:22:13,000
 how to navigate and experience carefully without reacting.

339
00:22:13,000 --> 00:22:19,000
 To be careful, to be conscientious.

340
00:22:19,000 --> 00:22:23,740
 Anyway, that's a bit of a tangent, but my point being there

341
00:22:23,740 --> 00:22:24,000
,

342
00:22:24,000 --> 00:22:32,000
 in many ways we are a family, meditators, Buddhists.

343
00:22:32,000 --> 00:22:37,000
 In many ways we are the ones that care for each other.

344
00:22:37,000 --> 00:22:42,360
 We are the ones that, it's each and every one of us, annya

345
00:22:42,360 --> 00:22:46,000
 mannya for each other,

346
00:22:46,000 --> 00:22:50,920
 who is a support to each other, who we would do well to

347
00:22:50,920 --> 00:22:53,000
 look to for support.

348
00:22:53,000 --> 00:22:56,000
 I mean, I can't look to my family so much for support.

349
00:22:56,000 --> 00:22:59,940
 I can't even visit with them half the time because they're

350
00:22:59,940 --> 00:23:04,000
 drinking alcohol.

351
00:23:04,000 --> 00:23:07,060
 But I know when I'm with meditators, when I was in New York

352
00:23:07,060 --> 00:23:10,000
 at the monastery, I knew it was all good.

353
00:23:10,000 --> 00:23:16,000
 I knew it was all good. I knew it was all good.

354
00:23:16,000 --> 00:23:21,000
 I knew it was all good. I knew it was all good.

355
00:23:21,000 --> 00:23:26,000
 I knew it was all good. I knew it was all good.

356
00:23:26,000 --> 00:23:31,000
 I knew it was all good. I knew it was all good.

357
00:23:31,000 --> 00:23:36,000
 I knew it was all good. I knew it was all good.

358
00:23:36,000 --> 00:23:41,000
 I'm feeling awkward like everyone around me is drunk.

359
00:23:41,000 --> 00:23:45,000
 Everyone around me is lost. It's on a different path.

360
00:23:45,000 --> 00:23:50,990
 We're not putting ourselves above anyone, but on different

361
00:23:50,990 --> 00:23:52,000
 paths.

362
00:23:52,000 --> 00:23:57,430
 So, on that note, I would really like to thank everyone for

363
00:23:57,430 --> 00:24:00,000
 all of the support.

364
00:24:00,000 --> 00:24:06,320
 We have this house and I'm able to live because people are

365
00:24:06,320 --> 00:24:08,000
 supporting me.

366
00:24:08,000 --> 00:24:13,000
 That's many of you. I'd like to thank you.

367
00:24:13,000 --> 00:24:19,000
 I also appreciate the meditation, the amount of meditation,

368
00:24:19,000 --> 00:24:22,220
 and the number of people involved with this community who

369
00:24:22,220 --> 00:24:24,000
 are practicing meditation.

370
00:24:24,000 --> 00:24:28,080
 It's another wonderful support because it means there's all

371
00:24:28,080 --> 00:24:31,000
 these people who we can relate to.

372
00:24:31,000 --> 00:24:34,610
 We have each other and we can relate to each other and we

373
00:24:34,610 --> 00:24:36,000
 can talk with each other

374
00:24:36,000 --> 00:24:42,340
 and encourage each other and push each other further on the

375
00:24:42,340 --> 00:24:43,000
 path.

376
00:24:43,000 --> 00:24:50,000
 Push each other to meditate and to cultivate good things.

377
00:24:50,000 --> 00:24:56,000
 So, we care for each other. This is true family.

378
00:24:56,000 --> 00:24:59,060
 We don't depend on our father and mother. We don't depend

379
00:24:59,060 --> 00:25:02,000
 on our brothers and sisters.

380
00:25:02,000 --> 00:25:09,000
 In spiritual matters, right? In important matters.

381
00:25:09,000 --> 00:25:12,490
 In many ways, we can't depend on them because they just end

382
00:25:12,490 --> 00:25:14,000
 up leading us down the wrong path

383
00:25:14,000 --> 00:25:22,000
 because their understanding is, yes, we would say wrong,

384
00:25:22,000 --> 00:25:26,590
 but at the very least their understanding is different from

385
00:25:26,590 --> 00:25:27,000
 ours.

386
00:25:27,000 --> 00:25:32,620
 So, it would be conflicting for us to depend too much upon

387
00:25:32,620 --> 00:25:34,000
 our family, right?

388
00:25:34,000 --> 00:25:37,660
 If we have a problem, what do we do? We'll buy a big bag of

389
00:25:37,660 --> 00:25:40,000
 pot and smoke it all.

390
00:25:40,000 --> 00:25:44,000
 That's what they were talking about on this Hangout.

391
00:25:44,000 --> 00:25:46,490
 They've got to find a way and they're talking about how to

392
00:25:46,490 --> 00:25:47,000
 find a pot dealer

393
00:25:47,000 --> 00:25:53,000
 or they can get a big bag of... This is my family.

394
00:25:53,000 --> 00:26:01,000
 So, this quote is somewhat apropos to my life.

395
00:26:01,000 --> 00:26:06,000
 I mean, that part of it is.

396
00:26:06,000 --> 00:26:11,000
 Is anybody here? Anybody have any questions?

397
00:26:11,000 --> 00:26:16,000
 We'll take text questions if you got them.

398
00:26:16,000 --> 00:26:20,000
 24 viewers on YouTube. Hello, everyone.

399
00:26:20,000 --> 00:26:27,500
 Somebody's watching and we got a bunch of people on our

400
00:26:27,500 --> 00:26:31,000
 meditation page.

401
00:26:31,000 --> 00:26:46,000
 We got the usual suspects.

402
00:26:46,000 --> 00:26:52,000
 So, Robin, are we going to do some sort of...

403
00:26:52,000 --> 00:26:54,000
 Are we going to give people the opportunity?

404
00:26:54,000 --> 00:26:56,620
 Not obviously pushing for it, but give people the

405
00:26:56,620 --> 00:26:57,000
 opportunity

406
00:26:57,000 --> 00:27:12,000
 if they want to offer a robe to Ajahn Tong. Like, join us.

407
00:27:12,000 --> 00:27:17,000
 Because there was, I think, Aurora, I think.

408
00:27:17,000 --> 00:27:20,360
 Someone, anyway, someone was interested in offering a set

409
00:27:20,360 --> 00:27:23,000
 of robes to Ajahn Tong.

410
00:27:23,000 --> 00:27:27,680
 And because it's not... I think it's quite easy for us to

411
00:27:27,680 --> 00:27:30,000
 add sets of robes to our order,

412
00:27:30,000 --> 00:27:33,990
 but it's just a matter of giving people the opportunity if

413
00:27:33,990 --> 00:27:35,000
 they want.

414
00:27:35,000 --> 00:27:39,070
 We're not coercion. It's not like trying to push people to

415
00:27:39,070 --> 00:27:40,000
 say, "This is a good thing."

416
00:27:40,000 --> 00:27:43,320
 Some people really would be keen to do that, I think, if

417
00:27:43,320 --> 00:27:47,000
 they want to understand.

418
00:27:47,000 --> 00:27:52,000
 I mean, I don't like to put pressure and make people think,

419
00:27:52,000 --> 00:27:52,000
 like,

420
00:27:52,000 --> 00:27:55,000
 "Oh, that's what we're all about is soliciting donations."

421
00:27:55,000 --> 00:27:57,000
 We're not.

422
00:27:57,000 --> 00:28:01,000
 But we are about giving, and I like to give.

423
00:28:01,000 --> 00:28:06,770
 So, if you want to give with us, you're welcome to give

424
00:28:06,770 --> 00:28:08,000
 with us.

425
00:28:08,000 --> 00:28:13,000
 Yeah, we could... Who knows if people want...

426
00:28:13,000 --> 00:28:18,560
 See, the thing is, there was some people who supported me

427
00:28:18,560 --> 00:28:20,000
 to get a ticket,

428
00:28:20,000 --> 00:28:23,410
 but there was lots of support. So I thought, "Well, we can

429
00:28:23,410 --> 00:28:25,000
 get a gift for Ajahn Tong as well."

430
00:28:25,000 --> 00:28:28,000
 Because that's, you know, important.

431
00:28:28,000 --> 00:28:32,220
 And we thought about what kind of gifts, and I thought,

432
00:28:32,220 --> 00:28:33,000
 robes.

433
00:28:33,000 --> 00:28:37,000
 Robes are the best, because robes symbolize monasticism.

434
00:28:37,000 --> 00:28:42,260
 So if you are interested in the idea of becoming a monk, or

435
00:28:42,260 --> 00:28:45,000
 if you want to be close to the monastic life,

436
00:28:45,000 --> 00:28:51,820
 the ascetic or noncient life, robes are a really good

437
00:28:51,820 --> 00:28:53,000
 symbolic gift,

438
00:28:53,000 --> 00:28:59,920
 and practical gift as well, because you're actually helping

439
00:28:59,920 --> 00:29:04,000
 to close Buddhist monks.

440
00:29:04,000 --> 00:29:11,000
 Awesome. So there's still time we can set up a campaign.

441
00:29:11,000 --> 00:29:13,510
 I mean, make it clear that this is... Well, I mean, it is

442
00:29:13,510 --> 00:29:14,000
 pretty clear.

443
00:29:14,000 --> 00:29:20,950
 We just have to... We don't want it to sound like we're

444
00:29:20,950 --> 00:29:25,000
 looking for money or something.

445
00:29:25,000 --> 00:29:27,300
 I don't think we should make a big deal out of it. I don't

446
00:29:27,300 --> 00:29:28,000
 want...

447
00:29:28,000 --> 00:29:37,000
 We've got... We had a couple of other crowd funding things.

448
00:29:37,000 --> 00:29:39,610
 This is just if there was someone out there. We're doing it

449
00:29:39,610 --> 00:29:43,000
 already, so we can always add a robe.

450
00:29:43,000 --> 00:29:51,000
 A set of robes if someone happened to want to do that.

451
00:29:51,000 --> 00:30:00,520
 Okay, well let me know how... if you can work something out

452
00:30:00,520 --> 00:30:02,000
 there.

453
00:30:02,000 --> 00:30:13,000
 Awesome. Thank you. Tomorrow.

454
00:30:13,000 --> 00:30:15,710
 Okay, so we'll talk about that. You can come on the Hangout

455
00:30:15,710 --> 00:30:17,000
 tomorrow, then.

456
00:30:17,000 --> 00:30:22,300
 Talk about it, maybe. Anyway, we'll say good night for

457
00:30:22,300 --> 00:30:25,000
 tonight then. No questions.

458
00:30:25,000 --> 00:30:28,600
 Have a good night, everyone. Thanks for tuning in. Wishing

459
00:30:28,600 --> 00:30:30,000
 you all the best.

