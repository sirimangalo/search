1
00:00:00,000 --> 00:00:02,000
 Fear upon losing awareness

2
00:00:02,000 --> 00:00:05,480
 Question I have a problem

3
00:00:05,480 --> 00:00:09,040
 The moment I lose awareness the fears and negative emotions

4
00:00:09,040 --> 00:00:10,120
 are attacking me

5
00:00:10,120 --> 00:00:13,790
 It is impossible to maintain constant awareness all the

6
00:00:13,790 --> 00:00:14,180
 time

7
00:00:14,180 --> 00:00:16,840
 Can you give me some advice?

8
00:00:16,840 --> 00:00:18,960
 Answer

9
00:00:18,960 --> 00:00:21,840
 The rule problem is in trying to maintain awareness

10
00:00:21,840 --> 00:00:26,220
 Let's call awareness X and negative emotions. Why?

11
00:00:26,840 --> 00:00:31,600
 The general statement is as soon as I lose X Y happens

12
00:00:31,600 --> 00:00:35,100
 The real problem is the dependency on X

13
00:00:35,100 --> 00:00:39,320
 If your peace of mind depends on something that is the cr

14
00:00:39,320 --> 00:00:40,240
utch for you

15
00:00:40,240 --> 00:00:43,380
 That thing that you depend on is the problem

16
00:00:43,380 --> 00:00:46,320
 That is the basis of Buddhist practice

17
00:00:46,320 --> 00:00:50,040
 You have to start by observing the need in this case the

18
00:00:50,040 --> 00:00:52,040
 need to be aware all the time

19
00:00:53,320 --> 00:00:56,300
 What you are seeing is that your mind is not stable

20
00:00:56,300 --> 00:00:59,720
 satisfying or controllable and this makes you think there

21
00:00:59,720 --> 00:01:01,000
 is some kind of problem

22
00:01:01,000 --> 00:01:05,080
 Actually, you are beginning to see the nature of your mind

23
00:01:05,080 --> 00:01:07,840
 you are starting to see things as they are

24
00:01:07,840 --> 00:01:11,970
 This is what you see when you practice mindfulness that you

25
00:01:11,970 --> 00:01:13,840
 can't control even your own mind

26
00:01:13,840 --> 00:01:17,570
 The attachment to some stable constant awareness has to be

27
00:01:17,570 --> 00:01:21,150
 dealt with before you can approach the fear and negative

28
00:01:21,150 --> 00:01:21,840
 emotions

29
00:01:23,120 --> 00:01:25,120
 If you are distracted note

30
00:01:25,120 --> 00:01:27,840
 distracted distracted

31
00:01:27,840 --> 00:01:33,180
 If you want to be alert focused and aware note wanting

32
00:01:33,180 --> 00:01:34,100
 wanting

33
00:01:34,100 --> 00:01:38,720
 If you dislike the fact that you have lost focus note

34
00:01:38,720 --> 00:01:41,680
 disliking disliking

35
00:01:41,680 --> 00:01:43,680
 etc as

36
00:01:43,680 --> 00:01:47,090
 For the fear and negativity even these are not problems.

37
00:01:47,090 --> 00:01:48,740
 They are just experiences

38
00:01:50,120 --> 00:01:53,170
 Seeing them as a problem will only make things worse as you

39
00:01:53,170 --> 00:01:55,720
 will react to them feeding further negativity

40
00:01:55,720 --> 00:01:58,680
 Start just looking at them

41
00:01:58,680 --> 00:02:00,800
 Look at everything that arises

42
00:02:00,800 --> 00:02:04,940
 Do not think to yourself. Oh, this is not good because I'm

43
00:02:04,940 --> 00:02:06,320
 going to get afraid again

44
00:02:06,320 --> 00:02:09,850
 Or what about those negative emotions that I had yesterday?

45
00:02:09,850 --> 00:02:11,320
 What have they come back?

46
00:02:11,320 --> 00:02:14,800
 When you could be focusing on what is right here and now?

47
00:02:16,280 --> 00:02:19,990
 When this is right here and now you can create awareness at

48
00:02:19,990 --> 00:02:23,400
 any moment you cannot lose it because you did not have it

49
00:02:23,400 --> 00:02:26,760
 You created in one moment and that is the moment that you

50
00:02:26,760 --> 00:02:27,800
 have awareness

51
00:02:27,800 --> 00:02:31,940
 Specifically we are trying for a type of awareness that is

52
00:02:31,940 --> 00:02:33,320
 firm and objective

53
00:02:33,320 --> 00:02:37,160
 Where you simply know the object as it is without any kind

54
00:02:37,160 --> 00:02:39,200
 of judgment or extrapolation

55
00:02:39,200 --> 00:02:42,920
 You can create this type of awareness at any moment

56
00:02:43,960 --> 00:02:47,600
 The labeling or noting is a recognition it reaffirms the

57
00:02:47,600 --> 00:02:49,400
 recognition of the object

58
00:02:49,400 --> 00:02:53,570
 When you recognize something you reaffirm that recognition

59
00:02:53,570 --> 00:02:56,640
 with the labeling and you keep the mind at bare recognition

60
00:02:56,640 --> 00:02:59,280
 without getting caught up in it

61
00:02:59,280 --> 00:03:02,970
 When this is done all negative emotions disappear in a

62
00:03:02,970 --> 00:03:06,240
 moment and only the physical manifestations are left

63
00:03:06,240 --> 00:03:11,110
 Negative emotions last for only a moment, but they affect

64
00:03:11,110 --> 00:03:12,720
 the body for example

65
00:03:13,040 --> 00:03:15,440
 You might think that you're still angry because you have a

66
00:03:15,440 --> 00:03:19,660
 headache tension in the body or a fast heartbeat, etc

67
00:03:19,660 --> 00:03:22,830
 But none of those things are anger rather. They are the

68
00:03:22,830 --> 00:03:25,620
 physical manifestations caused by the anger

69
00:03:25,620 --> 00:03:29,720
 You can focus on the physical manifestations and deal with

70
00:03:29,720 --> 00:03:33,040
 them, but the actual anger only lasts for a moment

71
00:03:33,040 --> 00:03:38,190
 If you are unmindful of the physical manifestations anger

72
00:03:38,190 --> 00:03:41,720
 can arise again creating a cycle of stress and suffering

73
00:03:42,480 --> 00:03:44,480
 The same goes for fear

74
00:03:44,480 --> 00:03:47,360
 Not all of what we call fear is actually fear

75
00:03:47,360 --> 00:03:51,600
 fear is a momentary mental state that causes changes in the

76
00:03:51,600 --> 00:03:52,280
 body and

77
00:03:52,280 --> 00:03:56,100
 Then we associate these changes with fear and become upset

78
00:03:56,100 --> 00:03:59,720
 about them. Then we get more afraid and it snowballs

79
00:03:59,720 --> 00:04:03,750
 If you are mindful of both the emotions and the physical

80
00:04:03,750 --> 00:04:05,800
 manifestations moment to moment

81
00:04:05,800 --> 00:04:08,600
 Slowly, they will work themselves out

82
00:04:10,080 --> 00:04:14,950
 Be patient be realistic do not have expectations and

83
00:04:14,950 --> 00:04:16,400
 consider that you are here to learn

84
00:04:16,400 --> 00:04:20,670
 That is why what we say we are doing is to practice rather

85
00:04:20,670 --> 00:04:21,840
 than to perfect

86
00:04:21,840 --> 00:04:25,880
 You are not here to perfect. You are here to practice and

87
00:04:25,880 --> 00:04:30,070
 More importantly you are here to learn it is not a magic

88
00:04:30,070 --> 00:04:33,270
 trick where you sit and meditate and then poof you become

89
00:04:33,270 --> 00:04:33,920
 enlightened

90
00:04:34,840 --> 00:04:38,640
 It is not like pulling a rabbit out of a hat it is learning

91
00:04:38,640 --> 00:04:41,680
 learning more about yourself and learning more about

92
00:04:41,680 --> 00:04:42,000
 reality

93
00:04:42,000 --> 00:04:45,440
 And that is what you have done in noticing this situation

94
00:04:45,440 --> 00:04:48,870
 You really learn something and you are continuing to learn

95
00:04:48,870 --> 00:04:51,800
 about something that you do not want to accept

96
00:04:51,800 --> 00:04:55,310
 You are learning the hard truth that you cannot control the

97
00:04:55,310 --> 00:04:55,680
 mind

98
00:04:55,680 --> 00:04:59,670
 Trying not to feel guilty and trying not to beat yourself

99
00:04:59,670 --> 00:05:01,600
 up is a very important part

100
00:05:02,480 --> 00:05:05,430
 People often experience guilt and anger for not being a

101
00:05:05,430 --> 00:05:06,520
 perfect meditator

102
00:05:06,520 --> 00:05:10,380
 We feel the need to be perfect at everything. So we spend

103
00:05:10,380 --> 00:05:13,470
 most of our lives beating ourselves up because we are not

104
00:05:13,470 --> 00:05:14,000
 perfect

105
00:05:14,000 --> 00:05:16,000
 You

106
00:05:16,000 --> 00:05:18,000
 You

