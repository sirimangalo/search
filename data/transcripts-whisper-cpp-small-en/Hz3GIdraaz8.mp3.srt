1
00:00:00,000 --> 00:00:07,960
 Okay so welcome everyone to our weekly televised internet

2
00:00:07,960 --> 00:00:11,120
 broadcast monk radio

3
00:00:11,120 --> 00:00:16,550
 announcements for this week. We just came back yesterday

4
00:00:16,550 --> 00:00:19,800
 evening from Anuradhapura

5
00:00:19,800 --> 00:00:27,100
 to see the Sri Mahabodhi tree which is a descendant from

6
00:00:27,100 --> 00:00:28,040
 the original tree in

7
00:00:28,040 --> 00:00:33,430
 Bodh Gaya and was actually returned back the one that was

8
00:00:33,430 --> 00:00:35,040
 returned back to Bodh

9
00:00:35,040 --> 00:00:42,440
 Gaya when when the one in Bodh Gaya was killed. So very

10
00:00:42,440 --> 00:00:45,000
 special place and we also

11
00:00:45,000 --> 00:00:50,360
 went to Polonnorewa to see some ancient Buddhist sites. I

12
00:00:50,360 --> 00:00:52,000
 was able to give two

13
00:00:52,000 --> 00:00:56,110
 talks and take two videos so they went up today if you

14
00:00:56,110 --> 00:00:57,040
 haven't seen them they're

15
00:00:57,040 --> 00:01:07,310
 now on YouTube. And the workers have started now on two new

16
00:01:07,310 --> 00:01:09,440
 rooms so

17
00:01:09,440 --> 00:01:12,670
 we're soon going to have two new rooms and this evening the

18
00:01:12,670 --> 00:01:13,800
 carpenter came so

19
00:01:13,800 --> 00:01:19,650
 he's gonna put a roof on the meditation hall by the Bodhi

20
00:01:19,650 --> 00:01:21,400
 tree and then we won't

21
00:01:21,400 --> 00:01:28,080
 be able to do our rain meditation anymore. We'll be dry and

22
00:01:28,080 --> 00:01:31,720
 we'll be able to be able to do

23
00:01:31,720 --> 00:01:35,500
 chanting up there and so on. We'll be more certain, we'll

24
00:01:35,500 --> 00:01:36,720
 be cool during the day you know.

25
00:01:36,720 --> 00:01:41,800
 We'll have a cool place during the day as well.

26
00:01:42,560 --> 00:01:49,030
 Otherwise Kathy's leaving tomorrow no? I'm going to get my

27
00:01:49,030 --> 00:01:52,560
 visa tomorrow hopefully leaving the next day.

28
00:01:52,560 --> 00:01:58,400
 And hopefully leaving the next day you hear that? Hopefully

29
00:01:58,400 --> 00:01:59,360
 getting the visa.

30
00:01:59,360 --> 00:02:03,580
 Hopefully I can leave this wrong place where they make me

31
00:02:03,580 --> 00:02:06,680
 stay intense and

32
00:02:08,880 --> 00:02:14,990
 your fan got destroyed twice. I have no electricity. So

33
00:02:14,990 --> 00:02:18,520
 Kathy maybe you can tell

34
00:02:18,520 --> 00:02:23,790
 us how was your stay here? What's this place like? Is it

35
00:02:23,790 --> 00:02:25,000
 tolerable for

36
00:02:25,000 --> 00:02:31,360
 meditation? Yeah it's very suitable for meditation I think.

37
00:02:31,360 --> 00:02:33,200
 How was the tent? The

38
00:02:33,200 --> 00:02:40,000
 tent was excellent. You'd recommend it to people thinking

39
00:02:40,000 --> 00:02:40,280
 about

40
00:02:40,280 --> 00:02:43,460
 bringing a tent to your communist day in one of our tents?

41
00:02:43,460 --> 00:02:46,520
 Yeah you can come and stay in the tent close to nature.

42
00:02:46,520 --> 00:02:55,560
 What do you think of Sri Lanka? Is this your first time in

43
00:02:55,560 --> 00:02:56,840
 Sri Lanka? Yes my

44
00:02:56,840 --> 00:03:02,440
 first time in Sri Lanka. Sri Lanka is a beautiful country

45
00:03:02,440 --> 00:03:04,040
 and yeah I don't know I

46
00:03:04,040 --> 00:03:07,200
 think it's a bit like I don't know if people might not like

47
00:03:07,200 --> 00:03:08,360
 this but I see it a bit

48
00:03:08,360 --> 00:03:12,920
 like across the team Thailand and India. It's sort of

49
00:03:12,920 --> 00:03:13,280
 similar

50
00:03:13,280 --> 00:03:16,950
 scenery to India but much more relaxed. You can feel that

51
00:03:16,950 --> 00:03:18,000
 it's a Buddhist

52
00:03:18,000 --> 00:03:21,990
 country. It's more of a feel like Thailand but actually

53
00:03:21,990 --> 00:03:22,880
 even more relaxed

54
00:03:22,880 --> 00:03:26,690
 than Thailand. People are friendlier. I was telling you I

55
00:03:26,690 --> 00:03:27,800
 went to Kandy the other

56
00:03:27,800 --> 00:03:32,400
 day and two buses there and three buses back and no one

57
00:03:32,400 --> 00:03:33,960
 would let me pay for the

58
00:03:33,960 --> 00:03:39,600
 bus. They didn't want any money from me. Yeah very friendly

59
00:03:39,600 --> 00:03:41,440
 people. What did you

60
00:03:41,440 --> 00:03:47,360
 think of Anuradhapura? A very spiritual place I think. So

61
00:03:47,360 --> 00:03:48,600
 we stayed, I didn't

62
00:03:48,600 --> 00:03:51,990
 mention actually after I had a blog post, we stayed

63
00:03:51,990 --> 00:03:54,320
 overnight in front of the

64
00:03:54,320 --> 00:03:57,620
 Jethavan as stupa. So we stayed in Jethavan in the ancient

65
00:03:57,620 --> 00:03:58,400
 monastery of

66
00:03:58,400 --> 00:04:03,160
 Jethavan. We actually camped out in this ruined monastery

67
00:04:03,160 --> 00:04:07,880
 under the Jethya and

68
00:04:07,880 --> 00:04:11,740
 some of the as I mentioned in one of the videos these the

69
00:04:11,740 --> 00:04:13,160
 people were coming by

70
00:04:13,160 --> 00:04:15,620
 even during the night they were coming by to go and pay

71
00:04:15,620 --> 00:04:16,600
 respect to the Bodhi

72
00:04:16,600 --> 00:04:19,980
 tree and walk right past where we were meditating. I don't

73
00:04:19,980 --> 00:04:20,560
 know if they stopped

74
00:04:20,560 --> 00:04:24,780
 any of you but they stopped to talk to me and ask me where

75
00:04:24,780 --> 00:04:26,360
 the tree was and who I

76
00:04:26,360 --> 00:04:32,160
 was and where I was from. One man even asked me what I

77
00:04:32,160 --> 00:04:33,160
 think of Buddhism.

78
00:04:33,160 --> 00:04:37,080
 I mentioned it's a funny question to ask of a Buddhist monk

79
00:04:37,080 --> 00:04:38,800
 really. What do you

80
00:04:38,800 --> 00:04:47,820
 think is Buddhism a good religion? What did you think of

81
00:04:47,820 --> 00:04:50,280
 Polanurva? Also very

82
00:04:50,280 --> 00:04:55,810
 interesting place. The parts we went to not as spiritual as

83
00:04:55,810 --> 00:04:58,520
 Andhra Dapura but

84
00:04:58,520 --> 00:05:04,040
 yeah the reclining Buddha was very impressive as an

85
00:05:04,040 --> 00:05:08,200
 interesting history.

86
00:05:08,200 --> 00:05:12,540
 Tomorrow we're going to get visa we're going to try once

87
00:05:12,540 --> 00:05:14,720
 more third times the

88
00:05:14,720 --> 00:05:19,760
 charm to get visas for our two

89
00:05:19,760 --> 00:05:24,210
 initiates I don't know how you say applicants for ord

90
00:05:24,210 --> 00:05:32,280
ination aspirants and

91
00:05:33,440 --> 00:05:37,340
 so yeah so if there's anyone out there who's watching this

92
00:05:37,340 --> 00:05:38,720
 and happens to have

93
00:05:38,720 --> 00:05:43,520
 some connection with someone who can help us please please

94
00:05:43,520 --> 00:05:44,880
 we're we're not

95
00:05:44,880 --> 00:05:48,710
 here to do harm to the country we're just looking to

96
00:05:48,710 --> 00:05:50,800
 continue to practice and

97
00:05:50,800 --> 00:05:54,690
 study and teach and spread the Buddha's teaching and Sri

98
00:05:54,690 --> 00:05:57,320
 Lanka is for I think all

99
00:05:57,320 --> 00:06:00,130
 of us are feeling that Sri Lanka is the place to do it so

100
00:06:00,130 --> 00:06:02,120
 we're just hoping to

101
00:06:02,120 --> 00:06:09,200
 be able to stay to grow our community so that's all for

102
00:06:09,200 --> 00:06:10,120
 announcements anyone have

103
00:06:10,120 --> 00:06:14,470
 anything else to say how's the I'm gonna give a summary of

104
00:06:14,470 --> 00:06:15,400
 the construction yes

105
00:06:15,400 --> 00:06:19,290
 how's the construction going yes thank you just mentioned

106
00:06:19,290 --> 00:06:20,200
 it which is having

107
00:06:20,200 --> 00:06:24,510
 two of your rooms made and that's basically it at the

108
00:06:24,510 --> 00:06:25,760
 moment it's not much

109
00:06:25,760 --> 00:06:30,460
 more happening we there is we're doing our new path you

110
00:06:30,460 --> 00:06:32,360
 know so it's raining

111
00:06:32,360 --> 00:06:36,800
 every day at the moment and there's one part where when it

112
00:06:36,800 --> 00:06:38,400
 rains heavily we have

113
00:06:38,400 --> 00:06:41,440
 problems and hopefully that'll be done in the next two or

114
00:06:41,440 --> 00:06:43,200
 three days that's

115
00:06:43,200 --> 00:06:45,560
 about it

116
00:06:45,560 --> 00:06:59,510
 so made a house the kitchen going kitchen is good we're

117
00:06:59,510 --> 00:07:06,040
 trying to set up a

118
00:07:06,040 --> 00:07:08,630
 routine for the the meditators can you explain the sort of

119
00:07:08,630 --> 00:07:09,240
 things that

120
00:07:09,240 --> 00:07:14,460
 meditators can expect if they come here the food and the

121
00:07:14,460 --> 00:07:16,320
 drinks and so on I'm

122
00:07:16,320 --> 00:07:22,030
 trying to keep it simple just making sure that there's

123
00:07:22,030 --> 00:07:23,680
 enough protein

124
00:07:23,680 --> 00:07:30,290
 sometimes on alms round we don't get offered too many beans

125
00:07:30,290 --> 00:07:32,000
 of dal or soy

126
00:07:32,000 --> 00:07:38,740
 they eat soybeans here so I've started cooking some dal and

127
00:07:38,740 --> 00:07:41,560
 soy some beans to

128
00:07:41,560 --> 00:07:46,740
 we've had a full house lately meditators and just want to

129
00:07:46,740 --> 00:07:50,760
 make sure that you know

130
00:07:50,760 --> 00:07:55,850
 people who are working harder getting enough nutrition just

131
00:07:55,850 --> 00:07:57,520
 protein and also

132
00:07:57,520 --> 00:08:02,840
 some brown rice you get a lot of white rice and Westerners

133
00:08:02,840 --> 00:08:05,360
 tend to be a bit

134
00:08:05,360 --> 00:08:08,560
 picky towards the brown rice

135
00:08:08,560 --> 00:08:13,040
 you should get brown rice because we can get brown or red

136
00:08:13,040 --> 00:08:14,600
 rice and actually we

137
00:08:14,600 --> 00:08:19,620
 can get a rice cooker quite easy that's something we didn't

138
00:08:19,620 --> 00:08:20,280
 think of I mean if

139
00:08:20,280 --> 00:08:23,230
 it is Paul you only thought of that she thought I mean she

140
00:08:23,230 --> 00:08:24,280
 ate a rice cooker so

141
00:08:24,280 --> 00:08:27,570
 easy and you just put rice push the button and then I don't

142
00:08:27,570 --> 00:08:28,360
 have to get up

143
00:08:28,360 --> 00:08:32,240
 at 3.30 you don't anyway don't don't get up so okay but

144
00:08:32,240 --> 00:08:33,600
 then in the morning what

145
00:08:33,600 --> 00:08:36,260
 sort of things do we give to the meditators and and

146
00:08:36,260 --> 00:08:37,400
 specifically where do

147
00:08:37,400 --> 00:08:39,370
 that we get our food from for the meditators you've already

148
00:08:39,370 --> 00:08:39,920
 mentioned it

149
00:08:39,920 --> 00:08:42,620
 but just to make it clear where do we where do they get

150
00:08:42,620 --> 00:08:44,360
 their food and what

151
00:08:44,360 --> 00:08:46,660
 sorts of food and then in the evening what can they drink

152
00:08:46,660 --> 00:08:47,480
 and what do we have

153
00:08:47,480 --> 00:08:51,490
 for them to drink and so on well so I think technically med

154
00:08:51,490 --> 00:08:52,440
itators are supposed

155
00:08:52,440 --> 00:08:59,830
 to wait till sun up to eat anything besides juice or sugar

156
00:08:59,830 --> 00:09:01,160
 sugar water

157
00:09:01,160 --> 00:09:08,410
 drinks but we have made oats quick oats we're trying to

158
00:09:08,410 --> 00:09:09,800
 make that sort of an

159
00:09:09,800 --> 00:09:14,270
 available staple but don't don't expect it but we'll try to

160
00:09:14,270 --> 00:09:15,840
 have it available so

161
00:09:15,840 --> 00:09:22,370
 if you want to make a breakfast of oatmeal that should be

162
00:09:22,370 --> 00:09:23,840
 available and

163
00:09:23,840 --> 00:09:29,960
 then I've been making kind of a rice congee for the monks

164
00:09:29,960 --> 00:09:33,480
 the Buddha allowed

165
00:09:33,480 --> 00:09:36,750
 the monks to have some rice congee before they go on alms

166
00:09:36,750 --> 00:09:37,680
 round because it's

167
00:09:37,680 --> 00:09:42,460
 it's quite a truck I think today I actually timed it it was

168
00:09:42,460 --> 00:09:44,440
 an hour and I

169
00:09:44,440 --> 00:09:49,360
 guess there's a rote that's even longer than that so yeah

170
00:09:49,360 --> 00:09:50,760
 stop doing that one

171
00:09:50,760 --> 00:09:54,630
 and now we're only two of us so it's our excuse for not

172
00:09:54,630 --> 00:09:58,240
 doing that one yeah and on

173
00:09:58,240 --> 00:10:05,020
 alms around there's quite an assortment of Sri Lanka seems

174
00:10:05,020 --> 00:10:07,040
 to be into biscuits

175
00:10:07,040 --> 00:10:10,280
 so we get a lot of biscuits I'm not sure if that was

176
00:10:10,280 --> 00:10:12,080
 brought from the English or

177
00:10:12,080 --> 00:10:21,080
 what happened and they put tea leaves in the food so even

178
00:10:21,080 --> 00:10:21,440
 though we're not

179
00:10:21,440 --> 00:10:25,720
 supposed to have any caffeine here we're still getting tea

180
00:10:25,720 --> 00:10:28,240
 leaves in the food I

181
00:10:28,240 --> 00:10:33,090
 don't think it probably has much effect I don't know the

182
00:10:33,090 --> 00:10:34,360
 food is the food is

183
00:10:34,360 --> 00:10:39,240
 fairly spicy you know and almost entirely vegetarian so

184
00:10:39,240 --> 00:10:40,440
 there's a little

185
00:10:40,440 --> 00:10:47,000
 bit of fish I would say that I speak for myself but I only

186
00:10:47,000 --> 00:10:48,440
 noticed the food being

187
00:10:48,440 --> 00:10:52,710
 really spicy maybe the first week or two so I'd say if you

188
00:10:52,710 --> 00:10:54,720
 stick it out the

189
00:10:54,720 --> 00:10:57,280
 spiciness shouldn't affect you and no one's really been

190
00:10:57,280 --> 00:10:58,320
 getting sick from the

191
00:10:58,320 --> 00:11:01,840
 food so that's a big plus so Paul and Yanni the other

192
00:11:01,840 --> 00:11:02,480
 announcement is that

193
00:11:02,480 --> 00:11:06,240
 Paul and Yanni is not here because she's sick but she's she

194
00:11:06,240 --> 00:11:07,000
 has stomach

195
00:11:07,000 --> 00:11:11,760
 problems and it's chronic so that's more a personal thing

196
00:11:11,760 --> 00:11:14,400
 than with the food or the water here

197
00:11:14,400 --> 00:11:17,030
 but if you have a weak stomach you you know you kind of

198
00:11:17,030 --> 00:11:18,080
 have to put up with it

199
00:11:18,080 --> 00:11:22,040
 so but it's really only been her no everyone else's I mean

200
00:11:22,040 --> 00:11:22,760
 sometimes a little

201
00:11:22,760 --> 00:11:27,480
 bit of diarrhea but I got that in Canada as well it's a

202
00:11:27,480 --> 00:11:29,520
 part of life and that's

203
00:11:29,520 --> 00:11:33,700
 the other thing is the food is mostly vegetarian they

204
00:11:33,700 --> 00:11:36,400
 sometimes offer fish

205
00:11:36,400 --> 00:11:41,900
 and then some people come almost every day people come and

206
00:11:41,900 --> 00:11:44,120
 bring food to the

207
00:11:44,120 --> 00:11:51,240
 monastery as well so there's always enough food available

208
00:11:51,240 --> 00:11:53,520
 as Kathy would say

209
00:11:53,520 --> 00:11:58,650
 it's so much food almost every day this is this is the

210
00:11:58,650 --> 00:12:01,680
 quote of Kathy oh my god

211
00:12:01,680 --> 00:12:05,440
 so much food over we're kind of concerned that the medit

212
00:12:05,440 --> 00:12:06,560
ators may not be

213
00:12:06,560 --> 00:12:11,440
 getting enough protein enough I mean maybe that's just a

214
00:12:11,440 --> 00:12:12,840
 obsession of ours

215
00:12:12,840 --> 00:12:16,180
 but we've kind of agreed to supplement a little bit of

216
00:12:16,180 --> 00:12:17,840
 protein so as you already

217
00:12:17,840 --> 00:12:22,840
 said how about in the afternoon what are we doing now for

218
00:12:22,840 --> 00:12:24,640
 the afternoon so we

219
00:12:24,640 --> 00:12:29,920
 also get offered a lot of local fruits and so there's quite

220
00:12:29,920 --> 00:12:31,480
 a lot of farmland

221
00:12:31,480 --> 00:12:40,050
 around so we've been trying to get juiced fresh fruit ju

222
00:12:40,050 --> 00:12:42,040
iced and strained

223
00:12:42,040 --> 00:12:48,100
 has to be strained for the monks but it's not enough and it

224
00:12:48,100 --> 00:12:48,840
 takes quite a bit

225
00:12:48,840 --> 00:12:55,240
 of fruit to make juice so we're trying to buy non sugary

226
00:12:55,240 --> 00:12:56,080
 drinks for the

227
00:12:56,080 --> 00:13:01,970
 meditators no added sugar drinks so right now we have pome

228
00:13:01,970 --> 00:13:03,440
granate 100% it's

229
00:13:03,440 --> 00:13:10,560
 very concentrated and apple juice and hopefully that'll

230
00:13:10,560 --> 00:13:14,120
 last for a while

231
00:13:14,120 --> 00:13:19,070
 problem is that's all important no so but we know the point

232
00:13:19,070 --> 00:13:20,080
 is to have something

233
00:13:20,080 --> 00:13:23,530
 for the meditators in the afternoon the Buddha allowed med

234
00:13:23,530 --> 00:13:24,840
itators to have the

235
00:13:24,840 --> 00:13:29,490
 monks to have fruit juice in the afternoon so you'll find

236
00:13:29,490 --> 00:13:31,000
 that it's just

237
00:13:31,000 --> 00:13:35,760
 enough no in the afternoon we wouldn't want to have milk or

238
00:13:35,760 --> 00:13:36,960
 something more

239
00:13:36,960 --> 00:13:41,850
 solid because it makes you tired and it tends to lead to

240
00:13:41,850 --> 00:13:43,120
 extravagance and so on

241
00:13:43,120 --> 00:13:46,180
 but juice you'll find it's just enough to keep you going

242
00:13:46,180 --> 00:13:47,360
 but not enough to make

243
00:13:47,360 --> 00:13:54,130
 you intoxicated with your strength could you because I

244
00:13:54,130 --> 00:13:55,160
 found this very

245
00:13:55,160 --> 00:13:57,810
 interesting when I came here I didn't know that there was a

246
00:13:57,810 --> 00:13:59,760
 rule about the

247
00:13:59,760 --> 00:14:02,330
 boiled vegetable leaves and why we don't drink tea in the

248
00:14:02,330 --> 00:14:03,960
 afternoons here could

249
00:14:03,960 --> 00:14:10,040
 you say something about that well boiled boiled vegetables

250
00:14:10,040 --> 00:14:11,800
 is I think it's just

251
00:14:11,800 --> 00:14:17,640
 too there's too much nutrient it becomes much like a food

252
00:14:17,640 --> 00:14:19,200
 or else it's just too

253
00:14:19,200 --> 00:14:23,530
 too much work to boil things I'm not sure but boiled boiled

254
00:14:23,530 --> 00:14:24,680
 vegetables are not

255
00:14:24,680 --> 00:14:29,880
 allowed only fresh pressed vegetable or fruit juice is

256
00:14:29,880 --> 00:14:34,120
 allowed it's probably

257
00:14:34,120 --> 00:14:35,930
 because there's too much you know when you boil something

258
00:14:35,930 --> 00:14:36,500
 you take all the

259
00:14:36,500 --> 00:14:42,720
 nutrients out of it or you know it gained some of the the

260
00:14:42,720 --> 00:14:44,160
 particles of it I

261
00:14:44,160 --> 00:14:52,520
 don't know and tea well tea is tea is a medicine so you

262
00:14:52,520 --> 00:14:53,280
 could take it if you were

263
00:14:53,280 --> 00:15:01,080
 sick and if there were some sickness that tea cured but it

264
00:15:01,080 --> 00:15:05,000
's not a it's not a

265
00:15:05,000 --> 00:15:09,870
 juice of all leaf you can't and I guess that's the thing is

266
00:15:09,870 --> 00:15:10,800
 you can't you can't

267
00:15:10,800 --> 00:15:13,680
 blend up tea leaves and get juice from them so when you

268
00:15:13,680 --> 00:15:14,960
 boil them that you're

269
00:15:14,960 --> 00:15:18,770
 taking something out of them that you wouldn't get from

270
00:15:18,770 --> 00:15:21,080
 from a juicer it's on

271
00:15:21,080 --> 00:15:27,240
 a different level and and the fact that it's not for the

272
00:15:27,240 --> 00:15:28,120
 nutrient for the sugar

273
00:15:28,120 --> 00:15:30,940
 there's no sugar in them it's just for the drugs tea is for

274
00:15:30,940 --> 00:15:32,040
 the drug that's in

275
00:15:32,040 --> 00:15:37,200
 it the caffeine drug or and also you can say antioxidants

276
00:15:37,200 --> 00:15:38,360
 or whatever but it's not

277
00:15:38,360 --> 00:15:40,630
 the same thing the point is to have something that has

278
00:15:40,630 --> 00:15:41,760
 natural fruit sugars

279
00:15:41,760 --> 00:15:47,360
 is that's allowed we try to stay away from

280
00:15:47,360 --> 00:15:52,680
 they call stimulants I think that's great useful I mean

281
00:15:52,680 --> 00:15:53,880
 when I was when I

282
00:15:53,880 --> 00:15:57,780
 was teaching or when I was studying at Jum Tong and with a

283
00:15:57,780 --> 00:15:58,520
 lot of foreign

284
00:15:58,520 --> 00:16:03,440
 meditators I saw two meditators who when we when we asked

285
00:16:03,440 --> 00:16:04,280
 them to stay up all

286
00:16:04,280 --> 00:16:09,880
 night they they would stock up there they would go in and

287
00:16:09,880 --> 00:16:11,360
 every hour have a

288
00:16:11,360 --> 00:16:14,440
 cup of coffee so they ended up having over the course of

289
00:16:14,440 --> 00:16:15,200
 the night like eight

290
00:16:15,200 --> 00:16:18,420
 cups of coffee and went crazy two meditators who actually

291
00:16:18,420 --> 00:16:20,120
 went insane in

292
00:16:20,120 --> 00:16:23,590
 part because of the amount of caffeine that they had you

293
00:16:23,590 --> 00:16:25,160
 know it's really very

294
00:16:25,160 --> 00:16:31,120
 much against the the whole point of the thing is to push

295
00:16:31,120 --> 00:16:34,600
 you and to try to test

296
00:16:34,600 --> 00:16:37,560
 you and train you in this if you're cheating and using

297
00:16:37,560 --> 00:16:38,760
 stimulants to stay

298
00:16:38,760 --> 00:16:42,000
 awake it's not likely that you're going to get the results

299
00:16:42,000 --> 00:16:42,920
 that are intended

300
00:16:42,920 --> 00:16:46,800
 your mind will never have be forced to develop the

301
00:16:46,800 --> 00:16:49,440
 cultivation of the insight

302
00:16:49,440 --> 00:16:55,160
 and mindfulness just won't be there because you're you're

303
00:16:55,160 --> 00:16:56,880
 you're you're just

304
00:16:56,880 --> 00:17:00,020
 using stimulants to enhance your ability to stay awake and

305
00:17:00,020 --> 00:17:01,040
 then of course you

306
00:17:01,040 --> 00:17:04,000
 crash in the morning

307
00:17:04,000 --> 00:17:09,800
 not in the same as the same way as caffeine caffeine is a

308
00:17:09,800 --> 00:17:11,440
 steroid I think

309
00:17:11,440 --> 00:17:15,590
 or it has a very chemical effect in the body sugar just

310
00:17:15,590 --> 00:17:17,940
 gives energy I mean yeah

311
00:17:17,940 --> 00:17:20,770
 you could say anything's a drug all food is in that sense

312
00:17:20,770 --> 00:17:22,380
 of drug but there

313
00:17:22,380 --> 00:17:28,000
 caffeine is on a different level because it specifically

314
00:17:28,000 --> 00:17:29,160
 stimulates some part of

315
00:17:29,160 --> 00:17:34,140
 the body I don't know when I have caffeine I'm perky when I

316
00:17:34,140 --> 00:17:35,100
 used to have

317
00:17:35,100 --> 00:17:38,190
 coffee I could really feel it suddenly I'd feel all bubbly

318
00:17:38,190 --> 00:17:39,760
 and excited and so

319
00:17:39,760 --> 00:17:45,780
 on the whole body starts to kick into a higher gear sugar

320
00:17:45,780 --> 00:17:46,720
 doesn't do that to me

321
00:17:46,720 --> 00:17:54,750
 if I have sugar yeah you get more into you have to be

322
00:17:54,750 --> 00:17:56,040
 careful and white sugar as

323
00:17:56,040 --> 00:18:00,370
 well as there's not so good but anyway so this is the point

324
00:18:00,370 --> 00:18:01,320
 was to just talk a

325
00:18:01,320 --> 00:18:04,280
 little bit about the sort of things that meditators can

326
00:18:04,280 --> 00:18:05,520
 expect when they come

327
00:18:05,520 --> 00:18:11,340
 here and the sort of lifestyle that they'll have and we

328
00:18:11,340 --> 00:18:14,440
 have someone asked

329
00:18:14,440 --> 00:18:19,280
 how do we make sure we get enough protein and how do you

330
00:18:19,280 --> 00:18:21,840
 exercise in the

331
00:18:21,840 --> 00:18:30,040
 end everyone dies sickness is a good good meditation object

332
00:18:30,040 --> 00:18:36,480
 malnutrition malnutrition is a good thing if you're over

333
00:18:36,480 --> 00:18:36,920
 new if you're if

334
00:18:36,920 --> 00:18:43,860
 you're over nutrition you will over nourished then you will

335
00:18:43,860 --> 00:18:44,920
 give rise to all

336
00:18:44,920 --> 00:18:50,530
 sorts of lusts and in passions not just lust but anger and

337
00:18:50,530 --> 00:18:52,040
 people will become

338
00:18:52,040 --> 00:18:55,140
 very become very passionate this is why in some religions

339
00:18:55,140 --> 00:18:56,040
 will actually fast

340
00:18:56,040 --> 00:18:59,900
 they'll stop eating because they think that's a spiritual

341
00:18:59,900 --> 00:19:01,400
 practice of

342
00:19:01,400 --> 00:19:06,160
 suppressing the defilements through not eating at all and

343
00:19:06,160 --> 00:19:06,800
 they'll do it for days

344
00:19:06,800 --> 00:19:09,660
 and days and they'll think that that's somehow beneficial

345
00:19:09,660 --> 00:19:10,320
 to them it's actually

346
00:19:10,320 --> 00:19:15,690
 not it's an extreme and it's missing the point the you need

347
00:19:15,690 --> 00:19:16,520
 to see the

348
00:19:16,520 --> 00:19:19,160
 developments you need to understand them you need to to

349
00:19:19,160 --> 00:19:20,180
 work out the habits

350
00:19:20,180 --> 00:19:22,790
 involved with them and you can't do that if you're if you

351
00:19:22,790 --> 00:19:23,820
're so weak that they

352
00:19:23,820 --> 00:19:26,730
 don't even arise but on the other hand you can't do it when

353
00:19:26,730 --> 00:19:27,400
 they're so strong

354
00:19:27,400 --> 00:19:30,220
 that you're forced to follow after them so you have to be

355
00:19:30,220 --> 00:19:31,480
 very careful with food

356
00:19:31,480 --> 00:19:35,060
 actually my teacher always would go on and on about food

357
00:19:35,060 --> 00:19:36,680
 and and how important

358
00:19:36,680 --> 00:19:39,690
 his moderation in food is so important not on and on but

359
00:19:39,690 --> 00:19:41,040
 every day again and

360
00:19:41,040 --> 00:19:43,680
 again he would bring this up of course then I found out

361
00:19:43,680 --> 00:19:44,720
 that the reason was

362
00:19:44,720 --> 00:19:47,150
 probably because some of the monks were eating in the

363
00:19:47,150 --> 00:19:49,080
 evening so it was his way

364
00:19:49,080 --> 00:19:52,830
 of hinting at them that that's not cool but it is quite

365
00:19:52,830 --> 00:19:54,920
 important you shouldn't

366
00:19:54,920 --> 00:19:57,340
 eat too little and you shouldn't eat too much you should

367
00:19:57,340 --> 00:19:58,460
 eat just enough to stay

368
00:19:58,460 --> 00:20:05,420
 alive and to function normally so that your emotions come

369
00:20:05,420 --> 00:20:06,280
 up but that they're

370
00:20:06,280 --> 00:20:09,850
 not so strongly you can't deal with them and you can't

371
00:20:09,850 --> 00:20:12,000
 examine them and come to

372
00:20:12,000 --> 00:20:18,440
 understand them okay

