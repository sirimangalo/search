1
00:00:00,000 --> 00:00:04,960
 "From time to time I feel insecure about my physical

2
00:00:04,960 --> 00:00:06,900
 appearance and feel hopeless by

3
00:00:06,900 --> 00:00:12,000
 all the materialism, corruption, anger I see around me.

4
00:00:12,000 --> 00:00:14,940
 This affects my meditation practice and study.

5
00:00:14,940 --> 00:00:19,080
 I feel I go steps back and fall back into old ways.

6
00:00:19,080 --> 00:00:20,960
 What am I doing wrong?"

7
00:00:20,960 --> 00:00:28,400
 Well, I read this question and one thing did occur to me.

8
00:00:28,400 --> 00:00:35,600
 First thing to say is that it's quite a broad question.

9
00:00:35,600 --> 00:00:38,340
 You do have some points there, but in general this is the

10
00:00:38,340 --> 00:00:39,920
 kind of question where the best

11
00:00:39,920 --> 00:00:44,190
 answer that you can give theoretically is practice

12
00:00:44,190 --> 00:00:45,520
 meditation.

13
00:00:45,520 --> 00:00:51,170
 Because again and again we have these questions about, "I

14
00:00:51,170 --> 00:00:53,800
 have a problem in my life, what

15
00:00:53,800 --> 00:00:55,800
 should I do?"

16
00:00:55,800 --> 00:01:03,620
 And the answer is the continuation of practice of

17
00:01:03,620 --> 00:01:06,160
 meditation.

18
00:01:06,160 --> 00:01:12,900
 The idea that these insecurities and the hopelessness and

19
00:01:12,900 --> 00:01:17,160
 so on are affecting your meditation is

20
00:01:17,160 --> 00:01:23,300
 sort of a common misunderstanding about the practice of

21
00:01:23,300 --> 00:01:25,040
 meditation.

22
00:01:25,040 --> 00:01:27,880
 I'm assuming that you're practicing correctly and you have

23
00:01:27,880 --> 00:01:29,760
 good knowledge about the background

24
00:01:29,760 --> 00:01:38,950
 of meditation, but it's quite often that we'll miss these

25
00:01:38,950 --> 00:01:43,400
 important mind states and rather

26
00:01:43,400 --> 00:01:46,280
 than meditate on them we'll see them as a problem.

27
00:01:46,280 --> 00:01:49,700
 So you have this idea that the bad things are affecting

28
00:01:49,700 --> 00:01:51,640
 your meditation practice.

29
00:01:51,640 --> 00:01:55,900
 In fact the point is to focus on them as your meditation

30
00:01:55,900 --> 00:01:57,000
 practice.

31
00:01:57,000 --> 00:02:00,350
 When you feel insecure, then insecurity becomes your

32
00:02:00,350 --> 00:02:01,320
 meditation.

33
00:02:01,320 --> 00:02:03,620
 When you feel hopeless, then hopelessness becomes your

34
00:02:03,620 --> 00:02:04,360
 meditation.

35
00:02:04,360 --> 00:02:08,930
 Now in a sense I'm going to assume that you're aware of

36
00:02:08,930 --> 00:02:11,200
 this and that you feel that there's

37
00:02:11,200 --> 00:02:14,560
 something else, something greater than this that is the

38
00:02:14,560 --> 00:02:15,280
 problem.

39
00:02:15,280 --> 00:02:20,460
 And so what struck me about the phrasing of this question

40
00:02:20,460 --> 00:02:23,080
 is the idea of self and the

41
00:02:23,080 --> 00:02:26,720
 idea of entity.

42
00:02:26,720 --> 00:02:31,340
 So I think something useful that might be useful for you is

43
00:02:31,340 --> 00:02:33,360
 an understanding of the

44
00:02:33,360 --> 00:02:37,800
 order in which we do away with the negative states in our

45
00:02:37,800 --> 00:02:40,200
 mind because we don't get rid

46
00:02:40,200 --> 00:02:45,080
 of anger, we don't get rid of greed.

47
00:02:45,080 --> 00:02:50,680
 We don't get rid of our insecurities and our hopelessness

48
00:02:50,680 --> 00:02:52,160
 right away.

49
00:02:52,160 --> 00:02:53,160
 These aren't the first to go.

50
00:02:53,160 --> 00:02:59,110
 The first thing to go is the idea of the existence of an

51
00:02:59,110 --> 00:03:02,160
 entity, of any sort of entity.

52
00:03:02,160 --> 00:03:08,300
 The first thing to realize is that there is no "I" in all

53
00:03:08,300 --> 00:03:09,640
 of this.

54
00:03:09,640 --> 00:03:13,560
 The idea that your meditation is going backwards, the idea

55
00:03:13,560 --> 00:03:15,480
 that you're falling back into your

56
00:03:15,480 --> 00:03:19,190
 old ways, the idea that you're doing something wrong or

57
00:03:19,190 --> 00:03:21,560
 that something is wrong like there

58
00:03:21,560 --> 00:03:28,080
 is a problem, all has to do with the idea of self or the

59
00:03:28,080 --> 00:03:30,720
 idea of an entity.

60
00:03:30,720 --> 00:03:33,330
 So it may very well be that you're not doing anything wrong

61
00:03:33,330 --> 00:03:34,860
, but what is wrong is this

62
00:03:34,860 --> 00:03:38,940
 idea that when you put all of these mind states together,

63
00:03:38,940 --> 00:03:41,520
 you come up with the self, you come

64
00:03:41,520 --> 00:03:48,520
 up with the idea that there's a problem.

65
00:03:48,520 --> 00:03:55,580
 So the first thing that you have to work on is being able

66
00:03:55,580 --> 00:03:59,160
 to separate or being able to

67
00:03:59,160 --> 00:04:06,520
 acknowledge the different parts of the experience one by

68
00:04:06,520 --> 00:04:07,960
 one by one and see them for what they

69
00:04:07,960 --> 00:04:14,430
 are rather than thinking about my meditation practice, my

70
00:04:14,430 --> 00:04:17,480
 study, my old ways, a problem

71
00:04:17,480 --> 00:04:20,150
 that I have, something that I'm doing wrong, my physical

72
00:04:20,150 --> 00:04:21,520
 appearance, obviously.

73
00:04:21,520 --> 00:04:28,460
 That's really the key one there, is the answer to feeling

74
00:04:28,460 --> 00:04:33,040
 insecure about yourself and feeling

75
00:04:33,040 --> 00:04:36,740
 bad about yourself that somehow I am a bad meditator or I

76
00:04:36,740 --> 00:04:39,400
 am having problems in my meditator.

77
00:04:39,400 --> 00:04:41,420
 What we often get is people saying they're stuck in their

78
00:04:41,420 --> 00:04:42,080
 meditation.

79
00:04:42,080 --> 00:04:44,950
 People who have practiced for years feel like they're not

80
00:04:44,950 --> 00:04:45,600
 progressing.

81
00:04:45,600 --> 00:04:49,560
 And it can generally be or it can often be because they

82
00:04:49,560 --> 00:04:51,880
 still have this attachment to

83
00:04:51,880 --> 00:04:53,200
 an eye, to a self.

84
00:04:53,200 --> 00:04:58,630
 So they're thinking in terms of themselves as a being and

85
00:04:58,630 --> 00:05:01,720
 they get this idea that somehow

86
00:05:01,720 --> 00:05:07,430
 they are staying the same or they are stuck because it's

87
00:05:07,430 --> 00:05:09,840
 like a hole and you've got this

88
00:05:09,840 --> 00:05:13,790
 hole and that's the way out, but the self is too big to fit

89
00:05:13,790 --> 00:05:14,920
 in the hole.

90
00:05:14,920 --> 00:05:15,920
 This is the problem.

91
00:05:15,920 --> 00:05:20,820
 So what we're trying to do is fit ourselves in the exit or

92
00:05:20,820 --> 00:05:23,480
 through the door, through the

93
00:05:23,480 --> 00:05:24,480
 way out.

94
00:05:24,480 --> 00:05:26,600
 This is kind of a good metaphor to think of.

95
00:05:26,600 --> 00:05:29,460
 You have to throw away the self.

96
00:05:29,460 --> 00:05:32,120
 The eye can't become enlightened.

97
00:05:32,120 --> 00:05:35,580
 You have to give up this whole idea that I am trying to

98
00:05:35,580 --> 00:05:37,640
 progress somewhere or I am trying

99
00:05:37,640 --> 00:05:43,160
 to become something or I have this quality, I have that

100
00:05:43,160 --> 00:05:44,360
 quality.

101
00:05:44,360 --> 00:05:48,260
 Give up this whole idea of progress and begin to look at

102
00:05:48,260 --> 00:05:50,960
 what's right here and now, whether

103
00:05:50,960 --> 00:05:54,820
 it's an unwholesome state of mind or a wholesome state of

104
00:05:54,820 --> 00:05:57,400
 mind, whether it's a state of good

105
00:05:57,400 --> 00:06:00,080
 concentration, a state of poor concentration, whether it's

106
00:06:00,080 --> 00:06:01,440
 a state of worldliness or a state

107
00:06:01,440 --> 00:06:05,000
 of spirituality or so on.

108
00:06:05,000 --> 00:06:10,720
 And get rid of this idea that I am falling back, that I am

109
00:06:10,720 --> 00:06:13,600
 regressing or so on or that

110
00:06:13,600 --> 00:06:16,210
 we have, if you look up, there was a man here recently who

111
00:06:16,210 --> 00:06:17,720
's been coming on this chat room

112
00:06:17,720 --> 00:06:20,400
 and says, "I'm an alcoholic."

113
00:06:20,400 --> 00:06:24,380
 Even that is a good example of how we begin to identify

114
00:06:24,380 --> 00:06:26,920
 with mind states and we identify

115
00:06:26,920 --> 00:06:32,080
 with individual phenomena.

116
00:06:32,080 --> 00:06:35,500
 Your way forward and all of our way forward is to give all

117
00:06:35,500 --> 00:06:36,840
 that up and look at what's

118
00:06:36,840 --> 00:06:38,840
 here, right here and right now.

119
00:06:38,840 --> 00:06:41,630
 Give up the whole idea of I'm progressing, I'm getting

120
00:06:41,630 --> 00:06:43,320
 better or I'm not progressing,

121
00:06:43,320 --> 00:06:46,280
 I'm getting worse and so on.

122
00:06:46,280 --> 00:06:49,290
 Obviously, the key is the meditation practice, meditating

123
00:06:49,290 --> 00:06:51,040
 on those things that you're talking

124
00:06:51,040 --> 00:06:52,040
 about.

125
00:06:52,040 --> 00:06:56,980
 But I think that's an important point that I wanted to

126
00:06:56,980 --> 00:06:59,560
 bring up, the whole idea of the

127
00:06:59,560 --> 00:07:08,110
 overarching the self, which is over top of all of your

128
00:07:08,110 --> 00:07:10,240
 problems.

129
00:07:10,240 --> 00:07:12,600
 When you give that up, you'll see that your problems are

130
00:07:12,600 --> 00:07:13,720
 not really as big as they seem

131
00:07:13,720 --> 00:07:14,720
 to be.

132
00:07:14,720 --> 00:07:20,940
 I just want to add, until it comes to that moment, that you

133
00:07:20,940 --> 00:07:24,000
 can give up yourself, that

134
00:07:24,000 --> 00:07:29,840
 you have the deep insight and understand that there is not

135
00:07:29,840 --> 00:07:33,000
 a self within the body or the

136
00:07:33,000 --> 00:07:34,000
 mind.

137
00:07:34,000 --> 00:07:40,330
 It is very normal to feel like stepping back or falling

138
00:07:40,330 --> 00:07:42,960
 back into old ways.

139
00:07:42,960 --> 00:07:47,900
 That happens to everyone until the inside, the knowledge

140
00:07:47,900 --> 00:07:50,400
 really is established in the

141
00:07:50,400 --> 00:07:53,680
 mind, really is understood.

142
00:07:53,680 --> 00:07:57,510
 And until that, it will be like that, that you fall back

143
00:07:57,510 --> 00:07:59,360
 from time to time and then you

144
00:07:59,360 --> 00:08:02,240
 will progress and then fall back again.

145
00:08:02,240 --> 00:08:05,600
 So that's very normal.

146
00:08:05,600 --> 00:08:10,390
 And there was one thing that along the same lines that I

147
00:08:10,390 --> 00:08:13,240
 wanted to say is that it's often

148
00:08:13,240 --> 00:08:16,660
 not falling back and meditators will repeatedly become

149
00:08:16,660 --> 00:08:19,000
 discouraged when they think they've

150
00:08:19,000 --> 00:08:22,760
 overcome a problem and see it pop up again.

151
00:08:22,760 --> 00:08:29,410
 And quite often it's not falling back at all, it's the

152
00:08:29,410 --> 00:08:33,360
 echoes or the cycles that are still

153
00:08:33,360 --> 00:08:35,680
 very much a part of our old habits.

154
00:08:35,680 --> 00:08:40,350
 So until these habits work their way out and until you, as

155
00:08:40,350 --> 00:08:42,920
 Pangany said, until you have

156
00:08:42,920 --> 00:08:46,700
 that realization that allows you to break the habits, they

157
00:08:46,700 --> 00:08:48,520
're going to keep coming back

158
00:08:48,520 --> 00:08:49,520
 again and again.

159
00:08:49,520 --> 00:08:53,760
 So, part of what I was trying to say is that the practice

160
00:08:53,760 --> 00:08:56,440
 is not necessarily in the beginning

161
00:08:56,440 --> 00:09:00,270
 to get rid of the habits, but to see the habits is not self

162
00:09:00,270 --> 00:09:00,480
.

163
00:09:00,480 --> 00:09:04,920
 To see this slipping back, it's not you slipping back.

164
00:09:04,920 --> 00:09:08,560
 It's perfectly normal because it's the habit of the mind.

165
00:09:08,560 --> 00:09:13,630
 The beginning basic practice is to see that all of these

166
00:09:13,630 --> 00:09:17,000
 things are impermanent, you can't

167
00:09:17,000 --> 00:09:19,880
 push them away and say they're never going to come back.

168
00:09:19,880 --> 00:09:22,520
 It's not sure, they might come back in the next moment.

169
00:09:22,520 --> 00:09:27,450
 Their suffering means you can't form them into something

170
00:09:27,450 --> 00:09:30,080
 that is going to satisfy you.

171
00:09:30,080 --> 00:09:35,400
 And they're uncontrollable, that they're not really you.

172
00:09:35,400 --> 00:09:41,240
 These negative emotions that are coming up are not really,

173
00:09:41,240 --> 00:09:44,000
 they really have nothing to

174
00:09:44,000 --> 00:09:45,000
 do with you.

175
00:09:45,000 --> 00:09:49,300
 An external thing that, or an external, they're an imperson

176
00:09:49,300 --> 00:09:53,120
al thing that arises again and again.

177
00:09:53,120 --> 00:09:55,370
 Part of the practice is seeing that and so it's kind of a

178
00:09:55,370 --> 00:09:56,560
 shift from getting upset at

179
00:09:56,560 --> 00:09:59,350
 yourself for bad things to seeing that those bad things are

180
00:09:59,350 --> 00:10:00,880
 not really yours, they're not

181
00:10:00,880 --> 00:10:01,880
 really you.

182
00:10:01,880 --> 00:10:04,990
 And that comes from seeing moment to moment to moment,

183
00:10:04,990 --> 00:10:06,120
 looking at them.

184
00:10:06,120 --> 00:10:08,040
 You want to jump in here?

185
00:10:08,040 --> 00:10:09,040
 Okay.

186
00:10:09,040 --> 00:10:09,040
 Okay.

187
00:10:09,040 --> 00:10:10,040
 Okay.

188
00:10:10,040 --> 00:10:10,040
 Okay.

189
00:10:10,040 --> 00:10:11,040
 Okay.

