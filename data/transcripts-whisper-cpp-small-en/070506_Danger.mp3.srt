1
00:00:00,000 --> 00:00:10,470
 Normally before we practice together, I'll take the

2
00:00:10,470 --> 00:00:16,000
 opportunity to give a short talk on the Buddha's teaching.

3
00:00:16,000 --> 00:00:30,230
 It's important to have this sort of background on the

4
00:00:30,230 --> 00:00:33,000
 Buddha's teaching before we start to practice.

5
00:00:33,000 --> 00:00:38,720
 Because it's easy to say that we're practicing, we're easy

6
00:00:38,720 --> 00:00:41,000
 to go off and say that you're practicing,

7
00:00:41,000 --> 00:00:49,000
 but in the Buddha's teaching we have what is right practice

8
00:00:49,000 --> 00:00:51,000
 and what is called wrong practice.

9
00:00:51,000 --> 00:00:59,070
 So it's important that we always have this intellectual

10
00:00:59,070 --> 00:01:03,420
 understanding first of what is the right path and what is

11
00:01:03,420 --> 00:01:04,000
 the wrong path.

12
00:01:04,000 --> 00:01:07,910
 So that when we do set out to practice, we can make sure

13
00:01:07,910 --> 00:01:11,000
 that we are practicing on the right path

14
00:01:11,000 --> 00:01:15,040
 and we don't fall off onto the wrong path. If we do get set

15
00:01:15,040 --> 00:01:20,000
 out on the wrong path, we'll be able to correct ourselves

16
00:01:20,000 --> 00:01:29,000
 based on our understanding on an intellectual level.

17
00:01:29,000 --> 00:01:36,210
 So what I thought I would talk about today is a reason or

18
00:01:36,210 --> 00:01:40,000
 reasons why we should practice meditation,

19
00:01:40,000 --> 00:01:47,730
 reasons why we should undertake the practice of the Buddha

20
00:01:47,730 --> 00:01:50,000
's teaching.

21
00:01:50,000 --> 00:01:59,200
 And the best reason, a reason which is most often given, is

22
00:01:59,200 --> 00:02:04,000
 that on the road ahead of us there is great danger.

23
00:02:04,000 --> 00:02:11,000
 We are faced with great danger in our individual futures.

24
00:02:11,000 --> 00:02:18,360
 Each and every one of us has danger which we have to face

25
00:02:18,360 --> 00:02:25,000
 until we can become safe and free from danger.

26
00:02:25,000 --> 00:02:33,340
 And so the differentiation between people is that some

27
00:02:33,340 --> 00:02:37,700
 people see this danger and some people don't see the danger

28
00:02:37,700 --> 00:02:38,000
.

29
00:02:38,000 --> 00:02:42,880
 So before we continue on with our lives, it's important

30
00:02:42,880 --> 00:02:46,000
 that we understand the dangers which we are facing

31
00:02:46,000 --> 00:02:52,990
 so that we can undertake to clear up or to free ourselves

32
00:02:52,990 --> 00:02:56,000
 from these dangers.

33
00:02:56,000 --> 00:02:59,950
 And the way we do this, of course, is by practicing to pur

34
00:02:59,950 --> 00:03:01,000
ify our minds

35
00:03:01,000 --> 00:03:08,870
 and to remove from our minds all sorts of unwholesome, uns

36
00:03:08,870 --> 00:03:14,000
killful tendencies or mind states.

37
00:03:14,000 --> 00:03:19,110
 So the Lord Buddha's teaching on danger, we have a great

38
00:03:19,110 --> 00:03:23,000
 number of dangers ahead of us.

39
00:03:23,000 --> 00:03:26,670
 And it's important to get an understanding of these before

40
00:03:26,670 --> 00:03:30,810
 we start practicing or before we continue on with our lives

41
00:03:30,810 --> 00:03:31,000
.

42
00:03:31,000 --> 00:03:37,640
 And this gives us a good reason to live our lives in a very

43
00:03:37,640 --> 00:03:44,000
 careful, in a very skillful or wholesome way.

44
00:03:44,000 --> 00:03:49,290
 Because when we fall onto the wrong path, then these

45
00:03:49,290 --> 00:03:54,000
 dangers come and turn into suffering for us.

46
00:03:54,000 --> 00:04:00,940
 The first dangers we have, the danger of birth, the danger

47
00:04:00,940 --> 00:04:07,000
 of old age, the danger of sickness and the danger of death.

48
00:04:07,000 --> 00:04:13,000
 This is the first set of dangers which we have ahead of us.

49
00:04:13,000 --> 00:04:16,550
 But in this lifetime there's no birth ahead of us. We've

50
00:04:16,550 --> 00:04:19,000
 already met with birth.

51
00:04:19,000 --> 00:04:23,850
 It's no longer a danger, it's now a reality. We have been

52
00:04:23,850 --> 00:04:25,000
 born.

53
00:04:25,000 --> 00:04:29,100
 And because of our birth we have to face with all sorts of

54
00:04:29,100 --> 00:04:31,000
 suffering and discomfort,

55
00:04:31,000 --> 00:04:35,790
 which comes along with being human, which comes along with

56
00:04:35,790 --> 00:04:37,000
 being born.

57
00:04:37,000 --> 00:04:40,620
 But the dangers we still have ahead of us are old age,

58
00:04:40,620 --> 00:04:42,000
 sickness and death.

59
00:04:42,000 --> 00:04:49,340
 And these are dangers which in this lifetime we cannot

60
00:04:49,340 --> 00:04:50,000
 avoid.

61
00:04:50,000 --> 00:04:55,410
 But they are a danger for us in that if they catch us

62
00:04:55,410 --> 00:04:58,000
 unaware or unprepared,

63
00:04:58,000 --> 00:05:04,000
 then there's a great danger that we will suffer and become

64
00:05:04,000 --> 00:05:09,000
 in great stress because of these things.

65
00:05:09,000 --> 00:05:12,710
 So if we get old and we're not prepared for old age, we're

66
00:05:12,710 --> 00:05:20,000
 not prepared to be old and bent and aching

67
00:05:20,000 --> 00:05:28,000
 with all sorts of ailments which come along with being old,

68
00:05:28,000 --> 00:05:28,000
 having a poor memory,

69
00:05:28,000 --> 00:05:36,000
 having rotting teeth and so on, false teeth, bent back,

70
00:05:36,000 --> 00:05:37,000
 arthritis,

71
00:05:37,000 --> 00:05:42,000
 and all the many ailments which come along with being old.

72
00:05:42,000 --> 00:05:45,760
 If we're not prepared for these things, if we don't have a

73
00:05:45,760 --> 00:05:47,000
 mind which is well trained

74
00:05:47,000 --> 00:05:53,950
 and we're able to deal with such uncomfortable, undesirable

75
00:05:53,950 --> 00:05:55,000
 circumstances,

76
00:05:55,000 --> 00:05:58,000
 then this could be a great danger to us.

77
00:05:58,000 --> 00:06:00,000
 This could be something which could cause great suffering.

78
00:06:00,000 --> 00:06:05,000
 So it's a danger which even in this lifetime can be avoided

79
00:06:05,000 --> 00:06:05,000
,

80
00:06:05,000 --> 00:06:11,280
 the danger of suffering because of old age, or suffering

81
00:06:11,280 --> 00:06:13,000
 because of sickness.

82
00:06:13,000 --> 00:06:20,650
 If we're not ready for cancer and then we happen to fall

83
00:06:20,650 --> 00:06:23,000
 victim to cancer,

84
00:06:23,000 --> 00:06:28,400
 or if we have diabetes or any sort of heart disease or

85
00:06:28,400 --> 00:06:36,000
 sickness which comes from old age and so on,

86
00:06:36,000 --> 00:06:43,130
 all sorts of cold and flu and fever and viruses of all

87
00:06:43,130 --> 00:06:44,000
 sorts,

88
00:06:44,000 --> 00:06:49,840
 then whenever these things come they will bring us great

89
00:06:49,840 --> 00:06:51,000
 suffering.

90
00:06:51,000 --> 00:06:53,620
 And if we're not ready for death, then when death comes of

91
00:06:53,620 --> 00:06:56,000
 course death will bring us great stress and suffering.

92
00:06:56,000 --> 00:07:03,120
 If we're not ready to leave, if we're afraid of death, if

93
00:07:03,120 --> 00:07:11,790
 we are not able to come to terms with the state or the act

94
00:07:11,790 --> 00:07:13,000
 of dying,

95
00:07:13,000 --> 00:07:16,610
 then this could be something which could bring great

96
00:07:16,610 --> 00:07:18,000
 suffering to us.

97
00:07:18,000 --> 00:07:24,200
 Often the danger in all of these things doesn't come from

98
00:07:24,200 --> 00:07:28,000
 any sort of greed or attachment,

99
00:07:28,000 --> 00:07:31,000
 it simply comes from not understanding these things.

100
00:07:31,000 --> 00:07:35,320
 For instance, not understanding death, not knowing what to

101
00:07:35,320 --> 00:07:38,000
 expect, not knowing what's happening,

102
00:07:38,000 --> 00:07:42,350
 not being able to come to grips with what's happening at

103
00:07:42,350 --> 00:07:44,000
 the time of death.

104
00:07:44,000 --> 00:07:48,860
 And so when we die, we die afraid, we die confused simply

105
00:07:48,860 --> 00:07:57,110
 because we are not able to process the phenomena which are

106
00:07:57,110 --> 00:08:00,000
 coming into our minds.

107
00:08:00,000 --> 00:08:02,880
 We're not able to deal with the impermanence, with

108
00:08:02,880 --> 00:08:04,000
 something new.

109
00:08:04,000 --> 00:08:10,000
 This is something unusual, something out of the ordinary.

110
00:08:10,000 --> 00:08:13,680
 In the practice of meditation we come to break everything

111
00:08:13,680 --> 00:08:16,000
 down into its ultimate experience.

112
00:08:16,000 --> 00:08:19,390
 When we say rising, falling, or when we have pain, when we

113
00:08:19,390 --> 00:08:24,670
 say pain, pain or thinking, or afraid or angry or upset or

114
00:08:24,670 --> 00:08:26,000
 confused.

115
00:08:26,000 --> 00:08:29,570
 When we break things down into their ultimate reality then

116
00:08:29,570 --> 00:08:32,000
 in the end anything which arises,

117
00:08:32,000 --> 00:08:37,140
 no matter how strange or abnormal it may be, it comes to be

118
00:08:37,140 --> 00:08:44,000
 very normal and very ordinary and very easy to deal with.

119
00:08:44,000 --> 00:08:47,630
 So at the time of dying there is still the same familiar

120
00:08:47,630 --> 00:08:49,000
 phenomenon.

121
00:08:49,000 --> 00:08:54,000
 At the time of sickness there is the same pain, aching.

122
00:08:54,000 --> 00:08:59,860
 When we don't let these things become conceptual like "I

123
00:08:59,860 --> 00:09:03,000
 have cancer" or "I am dying",

124
00:09:03,000 --> 00:09:08,130
 when we keep it at an ultimate level of ultimate reality,

125
00:09:08,130 --> 00:09:11,570
 there is pain, there is fear, there is anger, there is

126
00:09:11,570 --> 00:09:14,000
 upset, there is worry and so on.

127
00:09:14,000 --> 00:09:19,070
 When we simply say to ourselves, "Worried, worried, afraid,

128
00:09:19,070 --> 00:09:20,000
 afraid,"

129
00:09:20,000 --> 00:09:23,010
 then we don't get caught off guard by what seems to be

130
00:09:23,010 --> 00:09:28,120
 something new, which seems to be something unordinary,

131
00:09:28,120 --> 00:09:31,000
 extraordinary.

132
00:09:31,000 --> 00:09:34,120
 So this is the first good reason to practice meditation is

133
00:09:34,120 --> 00:09:37,000
 because of the danger that is inherent in these things.

134
00:09:37,000 --> 00:09:41,450
 And of course if we don't practice and we happen to be born

135
00:09:41,450 --> 00:09:42,000
 again,

136
00:09:42,000 --> 00:09:46,020
 well we could be born again exactly the way we are or we

137
00:09:46,020 --> 00:09:50,000
 could be born again in a state of greater suffering,

138
00:09:50,000 --> 00:09:56,140
 in a place where food is scarce, luxury is non-existent, we

139
00:09:56,140 --> 00:09:59,780
 could be reborn as an animal, we could be reborn as a ghost

140
00:09:59,780 --> 00:10:00,000
,

141
00:10:00,000 --> 00:10:05,030
 we could be reborn in hell, we could be reborn any number

142
00:10:05,030 --> 00:10:06,000
 of places.

143
00:10:06,000 --> 00:10:10,070
 And so this is the danger of rebirth and of course the

144
00:10:10,070 --> 00:10:14,000
 danger of old age, sickness and death which would follow.

145
00:10:14,000 --> 00:10:17,510
 So we are in danger of this, we are in danger of being born

146
00:10:17,510 --> 00:10:20,000
 again and again and again and again.

147
00:10:20,000 --> 00:10:24,810
 When we die, if our minds are not clear, we are in danger

148
00:10:24,810 --> 00:10:32,000
 of getting stuck back into the same old set of suffering,

149
00:10:32,000 --> 00:10:35,700
 set of dangers or even worse depending on our state of mind

150
00:10:35,700 --> 00:10:36,000
.

151
00:10:36,000 --> 00:10:39,360
 But if we train our minds and purify our minds, then at the

152
00:10:39,360 --> 00:10:42,000
 moment when we die we may not even have to be born again.

153
00:10:42,000 --> 00:10:46,330
 And if we are born again we will be born in a pure place

154
00:10:46,330 --> 00:10:50,000
 according to our state of mind when we die.

155
00:10:50,000 --> 00:10:55,290
 So this is one, the first good reason, the first set of

156
00:10:55,290 --> 00:10:58,110
 dangers which we can avoid through the practice of

157
00:10:58,110 --> 00:10:59,000
 meditation.

158
00:10:59,000 --> 00:11:05,840
 Another set of dangers is dangers which are existent in

159
00:11:05,840 --> 00:11:09,000
 this very life, in our everyday life.

160
00:11:09,000 --> 00:11:17,480
 And this is the danger of self, self of blame that comes

161
00:11:17,480 --> 00:11:19,000
 from self.

162
00:11:19,000 --> 00:11:26,990
 So ata nuwa ata paeya, the danger of rebuking ourselves or

163
00:11:26,990 --> 00:11:29,000
 blaming ourselves,

164
00:11:29,000 --> 00:11:32,350
 so the danger of feeling guilty for bad things which we

165
00:11:32,350 --> 00:11:34,000
 have done.

166
00:11:34,000 --> 00:11:42,090
 And then para nuwa ata paeya, the danger of other people

167
00:11:42,090 --> 00:11:47,610
 blaming us, the danger of receiving blame from other people

168
00:11:47,610 --> 00:11:48,000
.

169
00:11:48,000 --> 00:11:55,400
 And then tanta paeya, the danger of punishment and tukatip

170
00:11:55,400 --> 00:12:01,000
ay, the danger of rebirth in a bad state.

171
00:12:01,000 --> 00:12:06,000
 This is another set of four dangers which are ahead of us.

172
00:12:06,000 --> 00:12:09,420
 So the danger of feeling guilty or blaming ourselves, this

173
00:12:09,420 --> 00:12:12,000
 is ever present, whenever we do bad things,

174
00:12:12,000 --> 00:12:17,600
 there is always the guilt which follows. If we are a good

175
00:12:17,600 --> 00:12:19,000
 person, we will feel guilty.

176
00:12:19,000 --> 00:12:23,330
 If we are a really evil, deprived person, we might not even

177
00:12:23,330 --> 00:12:25,000
 be able to feel guilty.

178
00:12:25,000 --> 00:12:31,060
 But it will still make us sink deeper and deeper and dirty

179
00:12:31,060 --> 00:12:35,250
 and defile our minds further and further and cause great

180
00:12:35,250 --> 00:12:37,000
 mental suffering.

181
00:12:37,000 --> 00:12:40,990
 At the least we feel afraid all the time when we have done

182
00:12:40,990 --> 00:12:44,000
 bad things, we feel afraid of blame from other people.

183
00:12:44,000 --> 00:12:48,620
 But most ordinary people, they will also feel guilty

184
00:12:48,620 --> 00:12:50,000
 themselves.

185
00:12:50,000 --> 00:12:55,490
 And this is a danger in our future because we are not

186
00:12:55,490 --> 00:13:01,000
 perfect and we haven't cleaned our mind of bad tendencies.

187
00:13:01,000 --> 00:13:04,520
 So we might still have the opportunity to get angry or

188
00:13:04,520 --> 00:13:08,520
 upset at someone else and feel guilty and upset when we do

189
00:13:08,520 --> 00:13:09,000
 so.

190
00:13:09,000 --> 00:13:12,000
 This is a danger which is ahead of us.

191
00:13:12,000 --> 00:13:16,120
 And the danger of receiving blame from other people, of

192
00:13:16,120 --> 00:13:17,000
 course.

193
00:13:17,000 --> 00:13:21,190
 This is equally, if not more common. It's so easy to blame

194
00:13:21,190 --> 00:13:25,000
 other people and we find that everyone around us

195
00:13:25,000 --> 00:13:29,000
 will take the time to blame us for the bad things we do.

196
00:13:29,000 --> 00:13:32,660
 Sometimes even when we don't do anything bad and they

197
00:13:32,660 --> 00:13:36,430
 perceive it as bad, this is a danger that other people will

198
00:13:36,430 --> 00:13:37,000
 blame us.

199
00:13:37,000 --> 00:13:40,150
 And if we are not ready and if we are not able to deal with

200
00:13:40,150 --> 00:13:43,560
 this in a calm and rational way, we might get angry and

201
00:13:43,560 --> 00:13:44,000
 upset.

202
00:13:44,000 --> 00:13:48,040
 We might feel great suffering when other people blame us

203
00:13:48,040 --> 00:13:51,400
 for bad things we have done or things which they perceive

204
00:13:51,400 --> 00:13:52,000
 to be bad.

205
00:13:52,000 --> 00:13:55,570
 It can be the same for ourselves. We can blame ourselves

206
00:13:55,570 --> 00:13:58,000
 for things which were not our fault.

207
00:13:58,000 --> 00:14:01,160
 And if our minds are not well trained, we will do so. We

208
00:14:01,160 --> 00:14:05,300
 will blame ourselves for things which were innocent, for

209
00:14:05,300 --> 00:14:07,000
 innocent deeds.

210
00:14:07,000 --> 00:14:11,230
 So this is a reason to train our minds so that we will not

211
00:14:11,230 --> 00:14:15,520
 have to blame ourselves and we will not blame ourselves for

212
00:14:15,520 --> 00:14:17,000
 things which we didn't do.

213
00:14:17,000 --> 00:14:22,150
 And we will not do bad things for which we could be truly

214
00:14:22,150 --> 00:14:24,000
 blameworthy.

215
00:14:24,000 --> 00:14:28,870
 Then there is the punishment in this life which is legal

216
00:14:28,870 --> 00:14:32,620
 punishment or punishment from other people when we do bad

217
00:14:32,620 --> 00:14:33,000
 things

218
00:14:33,000 --> 00:14:40,080
 and they might inflict punishment, our parents, our spouses

219
00:14:40,080 --> 00:14:46,000
, our friends, the police, the country and so on.

220
00:14:46,000 --> 00:14:49,940
 Even to the point of other countries when they are going to

221
00:14:49,940 --> 00:14:51,000
 war with us.

222
00:14:51,000 --> 00:14:54,480
 This is a real danger which we can even receive punishment

223
00:14:54,480 --> 00:14:58,000
 for things we never did. This is a real danger.

224
00:14:58,000 --> 00:15:02,640
 And if we are not ready to deal with punishment, we might

225
00:15:02,640 --> 00:15:08,000
 be upset and angry and sad and depressed by these things.

226
00:15:08,000 --> 00:15:13,000
 And the fourth one is the punishment in the next life.

227
00:15:13,000 --> 00:15:17,830
 When we die and our minds are full of anger, full of greed

228
00:15:17,830 --> 00:15:23,000
 or full of confusion, we will go to a place of suffering.

229
00:15:23,000 --> 00:15:25,770
 We could go to be born in hell, we could be born as an

230
00:15:25,770 --> 00:15:28,000
 animal, we could be born as a ghost.

231
00:15:28,000 --> 00:15:32,000
 We are born in this life because our mind is like this.

232
00:15:32,000 --> 00:15:36,950
 If our mind becomes full of anger or hatred, we would be

233
00:15:36,950 --> 00:15:41,000
 born in a place of anger and hatred in hell.

234
00:15:41,000 --> 00:15:43,560
 If we have greed, we will be born as a ghost. If we have

235
00:15:43,560 --> 00:15:46,000
 delusion, we will be born as an animal.

236
00:15:46,000 --> 00:15:50,260
 If these things are very strong, we will be born in

237
00:15:50,260 --> 00:15:53,000
 suffering states of existence.

238
00:15:53,000 --> 00:15:56,390
 And this is a real danger in our future. If we still have

239
00:15:56,390 --> 00:15:59,000
 these states of mind arising,

240
00:15:59,000 --> 00:16:03,000
 then it could be a real cause for us to go to these places.

241
00:16:03,000 --> 00:16:10,460
 This is the second set of four. The third set is a set of

242
00:16:10,460 --> 00:16:14,000
 metaphors having to do with the ocean.

243
00:16:14,000 --> 00:16:20,310
 And when we are on the ocean, when we are sailing on a ship

244
00:16:20,310 --> 00:16:25,000
 in the ocean, there are four dangers.

245
00:16:26,000 --> 00:16:32,250
 Suppose we are on a raft, on a raft in the river or in the

246
00:16:32,250 --> 00:16:36,000
 ocean, and we are trying to reach the shore.

247
00:16:36,000 --> 00:16:38,050
 Well, there are four dangers which might stop us from

248
00:16:38,050 --> 00:16:39,000
 reaching the shore.

249
00:16:39,000 --> 00:16:47,000
 The first one is the waves. The second one is crocodiles.

250
00:16:47,000 --> 00:16:55,000
 The third one is whirlpools. And the fourth one is sharks.

251
00:16:55,000 --> 00:16:59,000
 These are four dangers on the ocean.

252
00:16:59,000 --> 00:17:03,130
 So these four are also dangers stopping us from reaching

253
00:17:03,130 --> 00:17:04,000
 our goal,

254
00:17:04,000 --> 00:17:07,730
 from reaching our goal of peace and happiness and freedom

255
00:17:07,730 --> 00:17:09,000
 from suffering.

256
00:17:09,000 --> 00:17:16,300
 The first one, waves, is what they are called the eight

257
00:17:16,300 --> 00:17:18,000
 worldly dhammas,

258
00:17:18,000 --> 00:17:22,000
 the eight worldly attachments, you could say,

259
00:17:22,000 --> 00:17:26,810
 or eight things which we get caught up in. They are worldly

260
00:17:26,810 --> 00:17:28,000
 things.

261
00:17:28,000 --> 00:17:31,950
 So when we come to practice, we try to leave behind worldly

262
00:17:31,950 --> 00:17:32,000
 things.

263
00:17:32,000 --> 00:17:38,130
 We try to do away with our attachment and our aversion to

264
00:17:38,130 --> 00:17:39,000
 worldly things,

265
00:17:39,000 --> 00:17:42,520
 but if we get caught up in them again, they can lead us

266
00:17:42,520 --> 00:17:44,000
 down or lead us away from our goal.

267
00:17:44,000 --> 00:17:53,000
 And these are fame, praise, gain and happiness.

268
00:17:53,000 --> 00:17:58,990
 When we have fame, or many people know us, know who we are,

269
00:17:58,990 --> 00:18:02,000
 we can get caught up in this.

270
00:18:02,000 --> 00:18:05,810
 And when we are praised, when people say good things about

271
00:18:05,810 --> 00:18:06,000
 us,

272
00:18:06,000 --> 00:18:10,150
 we can become caught up in this and become lazy as a result

273
00:18:10,150 --> 00:18:11,000
.

274
00:18:11,000 --> 00:18:15,470
 When we have gain, when we have lots of wealth, we can

275
00:18:15,470 --> 00:18:17,000
 become complacent

276
00:18:17,000 --> 00:18:22,850
 and we don't think about following the path, or when we

277
00:18:22,850 --> 00:18:24,000
 have happiness,

278
00:18:24,000 --> 00:18:27,730
 when our lives are happy and comfortable and we don't right

279
00:18:27,730 --> 00:18:34,000
 now have any states of suffering arising.

280
00:18:34,000 --> 00:18:37,000
 But the Lord Buddha said these are like a wave,

281
00:18:37,000 --> 00:18:39,630
 because there are four other dhammas which follow after

282
00:18:39,630 --> 00:18:40,000
 them.

283
00:18:40,000 --> 00:18:45,000
 Wave means they can be swept away at any time.

284
00:18:45,000 --> 00:18:47,000
 They can sweep you away at any time.

285
00:18:47,000 --> 00:18:55,820
 So fame also has being infamous as its opposite or being

286
00:18:55,820 --> 00:18:57,000
 unknown.

287
00:18:57,000 --> 00:19:00,470
 Sometimes nobody will know who we are, everyone will forget

288
00:19:00,470 --> 00:19:01,000
 about us.

289
00:19:01,000 --> 00:19:04,000
 And then we will feel upset.

290
00:19:04,000 --> 00:19:10,660
 We get caught up in the waves of samsara, the waves of the

291
00:19:10,660 --> 00:19:12,000
 world.

292
00:19:12,000 --> 00:19:15,240
 Sometimes people will blame us, they will say bad things

293
00:19:15,240 --> 00:19:16,000
 about us.

294
00:19:16,000 --> 00:19:20,610
 Sometimes we might lose our belongings, we might even lose

295
00:19:20,610 --> 00:19:23,000
 everything that we own.

296
00:19:23,000 --> 00:19:25,260
 And if we have happiness, then for sure there will be some

297
00:19:25,260 --> 00:19:27,000
 times where we will have unhappiness.

298
00:19:27,000 --> 00:19:30,070
 So Lord Buddha said these eight things, we are never sure,

299
00:19:30,070 --> 00:19:32,000
 they are like a wave.

300
00:19:32,000 --> 00:19:35,000
 They can sweep us away or be swept away at any time.

301
00:19:35,000 --> 00:19:38,000
 This is one danger on the path.

302
00:19:38,000 --> 00:19:43,560
 Another danger is the crocodiles, and this is getting lazy

303
00:19:43,560 --> 00:19:47,000
 or getting indolent.

304
00:19:47,000 --> 00:19:51,000
 Thinking only about our, as they say,

305
00:19:51,000 --> 00:19:56,000
 and thinking only about our mouth and about our stomach.

306
00:19:56,000 --> 00:20:01,590
 When we aren't able to deal with difficulty, we aren't able

307
00:20:01,590 --> 00:20:04,000
 to deal with hardship.

308
00:20:04,000 --> 00:20:07,650
 So when we come to a meditation center and we aren't able

309
00:20:07,650 --> 00:20:10,000
 to stay in a simple dwelling,

310
00:20:10,000 --> 00:20:13,000
 we aren't able to deal with simple accommodations,

311
00:20:13,000 --> 00:20:18,890
 we aren't able to deal with hard floors or poor weather or

312
00:20:18,890 --> 00:20:24,000
 little food or strange food.

313
00:20:24,000 --> 00:20:28,000
 We aren't able to deal with hardships.

314
00:20:28,000 --> 00:20:31,470
 If we are going to truly be free from suffering, we have to

315
00:20:31,470 --> 00:20:35,000
 learn to let go of our attachment to comfort,

316
00:20:35,000 --> 00:20:41,000
 our attachment to luxury.

317
00:20:41,000 --> 00:20:43,890
 So sometimes it can be a good thing when we have to endure

318
00:20:43,890 --> 00:20:45,000
 these hardships.

319
00:20:45,000 --> 00:20:49,360
 We should be very patient and endure, even if we happen to

320
00:20:49,360 --> 00:20:52,000
 not eat, to not have any food.

321
00:20:52,000 --> 00:20:56,460
 If there is no possibility to get food, we should be able

322
00:20:56,460 --> 00:21:00,000
 to stay calm and stay at peace with that.

323
00:21:00,000 --> 00:21:02,570
 But of course here we have food every day and there is no

324
00:21:02,570 --> 00:21:04,000
 need to go without food.

325
00:21:04,000 --> 00:21:08,000
 It also doesn't help us to go without food, so we do eat.

326
00:21:08,000 --> 00:21:11,280
 But we have to be able to deal with hardship and any

327
00:21:11,280 --> 00:21:14,000
 painful feelings which might arise when we practice.

328
00:21:14,000 --> 00:21:17,000
 We shouldn't try to avoid these things.

329
00:21:17,000 --> 00:21:22,890
 This is like a crocodile, something which is a danger which

330
00:21:22,890 --> 00:21:25,000
 will eat us up inside,

331
00:21:25,000 --> 00:21:30,000
 if we are always thinking about comfort and luxury.

332
00:21:30,000 --> 00:21:32,000
 The third one is whirlpools.

333
00:21:32,000 --> 00:21:38,000
 And the whirlpool is our objects of desire.

334
00:21:38,000 --> 00:21:45,280
 This has to do with romance or lust or thoughts of

335
00:21:45,280 --> 00:21:50,000
 sexuality, thoughts of the opposite gender

336
00:21:50,000 --> 00:21:55,000
 or thoughts of someone, the object of our desire.

337
00:21:55,000 --> 00:21:58,000
 And this is like a whirlpool, something which drags us down

338
00:21:58,000 --> 00:21:58,000
.

339
00:21:58,000 --> 00:22:01,090
 So if we aren't able to dispel these thoughts, they can

340
00:22:01,090 --> 00:22:02,000
 drag us down.

341
00:22:02,000 --> 00:22:04,000
 Right now we are practicing the holy life.

342
00:22:04,000 --> 00:22:07,000
 The brahmacarya means celibacy.

343
00:22:07,000 --> 00:22:09,000
 And this is something very high.

344
00:22:09,000 --> 00:22:13,470
 It's called the brahmacarya, the life of Brahma, the life

345
00:22:13,470 --> 00:22:15,000
 of God.

346
00:22:15,000 --> 00:22:18,000
 Because God of course is celibate.

347
00:22:18,000 --> 00:22:23,000
 So we are following after the life of God.

348
00:22:23,000 --> 00:22:26,410
 And when we start to fall back into these thoughts of sens

349
00:22:26,410 --> 00:22:29,000
uality, thoughts of sexuality,

350
00:22:29,000 --> 00:22:33,000
 this is like a whirlpool which drags us down.

351
00:22:33,000 --> 00:22:41,000
 And we lose our holy life, our holiness.

352
00:22:41,000 --> 00:22:45,870
 And the fourth danger in this final, in the final of all

353
00:22:45,870 --> 00:22:49,000
 the dangers is the shark.

354
00:22:49,000 --> 00:22:55,780
 And the shark is all other pleasant phenomena which may

355
00:22:55,780 --> 00:22:57,000
 arise.

356
00:22:57,000 --> 00:23:03,100
 So apart from other people or the objects of our desire,

357
00:23:03,100 --> 00:23:05,000
 these can be objects of desire

358
00:23:05,000 --> 00:23:10,230
 as for food or pleasant sights or pleasant sounds or

359
00:23:10,230 --> 00:23:12,000
 pleasant smells.

360
00:23:12,000 --> 00:23:15,210
 And when these things arise, some meditators might get

361
00:23:15,210 --> 00:23:17,000
 intoxicated or caught up in these.

362
00:23:17,000 --> 00:23:23,120
 When we see bright colors or lights, this can be a danger

363
00:23:23,120 --> 00:23:26,000
 to our practice.

364
00:23:26,000 --> 00:23:31,500
 And when we smell smells or when we hear sounds, this can

365
00:23:31,500 --> 00:23:32,000
 be a danger.

366
00:23:32,000 --> 00:23:35,320
 When we hear music and we get caught up by the music, or

367
00:23:35,320 --> 00:23:36,000
 when we taste the food

368
00:23:36,000 --> 00:23:40,320
 and we enjoy the food and we forget to be mindful tasting,

369
00:23:40,320 --> 00:23:41,000
 tasting,

370
00:23:41,000 --> 00:23:43,160
 we can become obsessed with the taste and always be

371
00:23:43,160 --> 00:23:44,000
 thinking about it.

372
00:23:44,000 --> 00:23:48,000
 And we find it very difficult for us to concentrate.

373
00:23:48,000 --> 00:23:51,000
 This is the fourth danger on the ocean.

374
00:23:51,000 --> 00:23:55,000
 So these four are a danger on the practice, on the path.

375
00:23:55,000 --> 00:23:57,880
 And these we have to be aware of if we are going to follow

376
00:23:57,880 --> 00:23:59,000
 the path.

377
00:23:59,000 --> 00:24:05,000
 And of course we follow the path to overcome all danger.

378
00:24:05,000 --> 00:24:11,000
 And this teaching on danger is to help us to become one

379
00:24:11,000 --> 00:24:12,000
 that the Lord Buddha called,

380
00:24:12,000 --> 00:24:16,000
 "One who sees the danger in samsara."

381
00:24:16,000 --> 00:24:20,230
 Sees the danger in being old, getting sick and dying again

382
00:24:20,230 --> 00:24:21,000
 and again and again.

383
00:24:21,000 --> 00:24:24,510
 Sees the danger in simply living our lives without having

384
00:24:24,510 --> 00:24:26,000
 trained ourselves.

385
00:24:26,000 --> 00:24:30,000
 And this makes us what the Lord Buddha called a bhikkhu.

386
00:24:30,000 --> 00:24:34,530
 A bhikkhu is a word which was used for a monk or for

387
00:24:34,530 --> 00:24:37,000
 someone who begged for food.

388
00:24:37,000 --> 00:24:40,000
 Of course in Buddhism the monks don't beg for food.

389
00:24:40,000 --> 00:24:45,000
 The word bhikkhu here means "samsare bhayang ikati."

390
00:24:45,000 --> 00:24:49,000
 One who sees the danger in samsara.

391
00:24:49,000 --> 00:24:53,530
 So this teaching is given for the purposes of helping the

392
00:24:53,530 --> 00:24:56,000
 meditators see the danger

393
00:24:56,000 --> 00:24:59,410
 and not be caught off guard by the dangers of samsara, the

394
00:24:59,410 --> 00:25:03,000
 dangers which exist in the world around us.

395
00:25:03,000 --> 00:25:09,000
 And that's the teaching that I wish to give on this day.

396
00:25:09,000 --> 00:25:12,470
 So now we'll continue with mindful prostration walking and

397
00:25:12,470 --> 00:25:16,000
 sitting all together until two o'clock.

