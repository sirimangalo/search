1
00:00:00,000 --> 00:00:03,950
 Hello, welcome back to Ask a Monk. I assume some of you

2
00:00:03,950 --> 00:00:05,320
 have noticed that I

3
00:00:05,320 --> 00:00:12,920
 haven't really been posting in the past few months and it's

4
00:00:12,920 --> 00:00:13,640
 mainly because

5
00:00:13,640 --> 00:00:19,930
 I've been trying to take some time alone and there have

6
00:00:19,930 --> 00:00:22,720
 been other

7
00:00:22,720 --> 00:00:25,970
 engagements. I was traveling a little bit and I had to get

8
00:00:25,970 --> 00:00:26,920
 a ticket and now I'll

9
00:00:26,920 --> 00:00:35,580
 be traveling to America. So yeah, it's kind of broken up. I

10
00:00:35,580 --> 00:00:36,720
'm not just

11
00:00:36,720 --> 00:00:43,280
 sitting around making videos. So one thing I wanted to say

12
00:00:43,280 --> 00:00:45,280
 is that for people

13
00:00:45,280 --> 00:00:48,360
 who are waiting for the next videos or looking for me to

14
00:00:48,360 --> 00:00:49,480
 make more videos, I

15
00:00:49,480 --> 00:00:55,010
 just want to be sure that be clear that the purpose is not

16
00:00:55,010 --> 00:00:56,700
 to listen to the

17
00:00:56,700 --> 00:01:00,550
 Buddhist teaching or to listen to talks. People who

18
00:01:00,550 --> 00:01:03,680
 download mp3s and vast

19
00:01:03,680 --> 00:01:08,570
 libraries of Dhamma talks or who read the whole of the

20
00:01:08,570 --> 00:01:09,920
 Buddhist teaching

21
00:01:09,920 --> 00:01:17,200
 again and again, it's not really the point. So the

22
00:01:17,200 --> 00:01:18,000
 questions that have been

23
00:01:18,000 --> 00:01:20,370
 asked, the answers that I'm giving, I'm hoping that they're

24
00:01:20,370 --> 00:01:21,240
 for the purpose of

25
00:01:21,240 --> 00:01:25,050
 taking away to practice meditation. So really in the end it

26
00:01:25,050 --> 00:01:25,740
's not about

27
00:01:25,740 --> 00:01:28,440
 how many questions are answered or how many videos are

28
00:01:28,440 --> 00:01:29,640
 posted, how many talks

29
00:01:29,640 --> 00:01:33,420
 you listen to. It's how much time you actually spend

30
00:01:33,420 --> 00:01:35,160
 practicing and how clear

31
00:01:35,160 --> 00:01:42,220
 is your mind and your awareness of the present moment. So

32
00:01:42,220 --> 00:01:44,040
 if you're looking for

33
00:01:44,040 --> 00:01:46,320
 the next step, the next step is not just to sit around

34
00:01:46,320 --> 00:01:48,180
 waiting for more

35
00:01:48,180 --> 00:01:53,840
 videos. I will be doing more but there's a lot of questions

36
00:01:53,840 --> 00:01:54,160
 people

37
00:01:54,160 --> 00:01:59,120
 have been asking. The next step is to find a way to take

38
00:01:59,120 --> 00:02:00,040
 time to actually

39
00:02:00,040 --> 00:02:05,080
 practice and once you've really begun to engage in the

40
00:02:05,080 --> 00:02:05,680
 meditation

41
00:02:05,680 --> 00:02:09,080
 practice, there aren't nearly so many questions. Most of

42
00:02:09,080 --> 00:02:10,400
 the questions that you

43
00:02:10,400 --> 00:02:15,670
 have answer themselves. So just to make sure that I'm happy

44
00:02:15,670 --> 00:02:16,640
 to answer questions

45
00:02:16,640 --> 00:02:19,750
 but it's not really worth it if people just come back with

46
00:02:19,750 --> 00:02:20,320
 more and more

47
00:02:20,320 --> 00:02:24,400
 questions rather than seeking out the answers which are

48
00:02:24,400 --> 00:02:26,720
 ultimately within. So

49
00:02:26,720 --> 00:02:32,330
 but another thing that comes up which is great about having

50
00:02:32,330 --> 00:02:33,920
 this forum to answer

51
00:02:33,920 --> 00:02:37,430
 questions is that there are a lot of technical questions

52
00:02:37,430 --> 00:02:38,360
 which can't be

53
00:02:38,360 --> 00:02:40,810
 answered. You can't sit down and practice and very easily

54
00:02:40,810 --> 00:02:42,000
 come up with the

55
00:02:42,000 --> 00:02:44,950
 technical answer. What should I be doing here? What should

56
00:02:44,950 --> 00:02:46,400
 I be doing there?

57
00:02:46,400 --> 00:02:49,740
 Often because it's it's quite subjective. You know what is

58
00:02:49,740 --> 00:02:50,880
 the technique here and

59
00:02:50,880 --> 00:02:54,380
 unless you try it and test it out one way you won't be able

60
00:02:54,380 --> 00:02:54,880
 to tell

61
00:02:54,880 --> 00:02:57,280
 whether it's more beneficial to do it this way or that way.

62
00:02:57,280 --> 00:02:57,880
 So technical

63
00:02:57,880 --> 00:03:02,420
 questions are always welcome. All questions are welcome. I

64
00:03:02,420 --> 00:03:05,560
 just don't spend

65
00:03:05,560 --> 00:03:08,390
 all your time asking questions. You should spend most of

66
00:03:08,390 --> 00:03:09,760
 your time finding

67
00:03:09,760 --> 00:03:13,440
 the answers to them. So here are some answers to a couple

68
00:03:13,440 --> 00:03:14,120
 of technical

69
00:03:14,120 --> 00:03:18,000
 questions. This should be short. First of all when you say

70
00:03:18,000 --> 00:03:19,520
 your mind shouldn't be

71
00:03:19,520 --> 00:03:23,500
 in our mind shouldn't be in our head but in our stomach

72
00:03:23,500 --> 00:03:24,760
 when we contemplate

73
00:03:24,760 --> 00:03:28,350
 rising and falling what do you mean? Can you elaborate it

74
00:03:28,350 --> 00:03:32,240
 more? The mind is not

75
00:03:32,240 --> 00:03:37,050
 physical. The mind is that quality of experience or that

76
00:03:37,050 --> 00:03:38,920
 aspect of experience

77
00:03:38,920 --> 00:03:43,230
 that knows that is aware of something. So when I say that

78
00:03:43,230 --> 00:03:44,600
 the mind should be in

79
00:03:44,600 --> 00:03:47,360
 the stomach that's not really correct because the mind

80
00:03:47,360 --> 00:03:49,400
 doesn't take up space

81
00:03:49,400 --> 00:03:51,660
 and it doesn't go here or there so it isn't one moment in

82
00:03:51,660 --> 00:03:52,560
 the head one moment

83
00:03:52,560 --> 00:03:57,880
 in the stomach. It doesn't move. It is aware of the object.

84
00:03:57,880 --> 00:03:59,360
 The idea of space

85
00:03:59,360 --> 00:04:03,360
 only arises when you're talking about the physical and that

86
00:04:03,360 --> 00:04:05,440
's really in a sense

87
00:04:05,440 --> 00:04:08,690
 only a concept. The ultimate reality is the experience

88
00:04:08,690 --> 00:04:10,000
 which has a physical end

89
00:04:10,000 --> 00:04:15,080
 and a mental end and that doesn't take up space. Space is

90
00:04:15,080 --> 00:04:15,400
 only a

91
00:04:15,400 --> 00:04:21,140
 convention when we're talking about the physical realm. So

92
00:04:21,140 --> 00:04:22,240
 what I mean by that

93
00:04:22,240 --> 00:04:28,080
 is that it should feel like the mind is in the stomach in

94
00:04:28,080 --> 00:04:28,600
 the sense that

95
00:04:28,600 --> 00:04:32,160
 you're really knowing the rising. So when you say to

96
00:04:32,160 --> 00:04:34,440
 yourself rising that's

97
00:04:34,440 --> 00:04:40,390
 the mind recognizing this is the rising. It's the mind with

98
00:04:40,390 --> 00:04:41,480
 a clear awareness of

99
00:04:41,480 --> 00:04:44,870
 what that is because this is what the mind does when it

100
00:04:44,870 --> 00:04:45,080
 becomes

101
00:04:45,080 --> 00:04:49,960
 aware of something. It immediately recognizes it as well

102
00:04:49,960 --> 00:04:50,760
 first of all as

103
00:04:50,760 --> 00:04:53,640
 the basic realities of it seeing, hearing, smelling,

104
00:04:53,640 --> 00:04:54,920
 tasting, feeling, thinking but

105
00:04:54,920 --> 00:04:59,830
 generally it will go on to recognize it as good, bad, me,

106
00:04:59,830 --> 00:05:02,320
 mind, a source of

107
00:05:02,320 --> 00:05:05,250
 pleasure, a source of pain, something that is beneficial,

108
00:05:05,250 --> 00:05:06,560
 something that is harmful

109
00:05:06,560 --> 00:05:08,990
 and so on. And so it makes all sorts of judgments and

110
00:05:08,990 --> 00:05:10,880
 categorizations and it

111
00:05:10,880 --> 00:05:14,200
 creates all sorts of intentions based on this and

112
00:05:14,200 --> 00:05:15,640
 eventually leads to

113
00:05:15,640 --> 00:05:21,230
 suffering and stress and busyness. So what we're trying to

114
00:05:21,230 --> 00:05:22,160
 do in the meditation

115
00:05:22,160 --> 00:05:28,470
 is to simply recognize it and to stop there. So the the

116
00:05:28,470 --> 00:05:29,400
 response in the mind

117
00:05:29,400 --> 00:05:33,320
 should only be rising. It shouldn't be, "Oh rising!" and

118
00:05:33,320 --> 00:05:36,440
 you know my stomach is smooth,

119
00:05:36,440 --> 00:05:40,400
 the rising is smooth or it's stuck and it's uncomfortable

120
00:05:40,400 --> 00:05:42,520
 or this or that or

121
00:05:42,520 --> 00:05:45,910
 the idea of trying to, "I should make it longer, I should

122
00:05:45,910 --> 00:05:47,280
 make it shorter, I should

123
00:05:47,280 --> 00:05:54,400
 try to keep it a constant speed" and so on. None of these

124
00:05:54,400 --> 00:05:54,840
 things will

125
00:05:54,840 --> 00:06:00,690
 arise if you're simply recognizing what it is. So the point

126
00:06:00,690 --> 00:06:01,360
 of saying that it

127
00:06:01,360 --> 00:06:05,810
 should be in the stomach is to avoid making it a mental

128
00:06:05,810 --> 00:06:07,360
 exercise where you

129
00:06:07,360 --> 00:06:15,090
 put the brain to work and start to create a concept or a

130
00:06:15,090 --> 00:06:15,680
 convention

131
00:06:15,680 --> 00:06:19,350
 or a mental creation based on the rising because it's very

132
00:06:19,350 --> 00:06:20,440
 easy to sit there and

133
00:06:20,440 --> 00:06:24,250
 say to yourself, "Rising, falling," but it's very difficult

134
00:06:24,250 --> 00:06:25,720
 to actually know that this

135
00:06:25,720 --> 00:06:29,040
 is the rising, this is the falling and to have a strong

136
00:06:29,040 --> 00:06:30,920
 awareness. Normally our

137
00:06:30,920 --> 00:06:34,380
 awareness of the rising or the falling or of anything is

138
00:06:34,380 --> 00:06:35,920
 very superficial so we

139
00:06:35,920 --> 00:06:39,540
 know it and then we're we're off on a tangent thinking we

140
00:06:39,540 --> 00:06:40,320
're back up in the

141
00:06:40,320 --> 00:06:44,810
 head working with actually with the chemicals in the brain

142
00:06:44,810 --> 00:06:45,280
 to create

143
00:06:45,280 --> 00:06:50,690
 pleasure and the ones that create pain and stress and so on

144
00:06:50,690 --> 00:06:51,880
 and the

145
00:06:51,880 --> 00:06:57,880
 interactions with our thoughts and our reactions to what

146
00:06:57,880 --> 00:06:58,320
 should have been

147
00:06:58,320 --> 00:07:03,600
 the very simple object which would be the rising. So it

148
00:07:03,600 --> 00:07:04,560
 should feel like the

149
00:07:04,560 --> 00:07:07,960
 mind is in the rising in the stomach because there is a

150
00:07:07,960 --> 00:07:09,280
 clear awareness at

151
00:07:09,280 --> 00:07:13,600
 that moment of a rising motion which we understand to occur

152
00:07:13,600 --> 00:07:14,920
 in the stomach and

153
00:07:14,920 --> 00:07:21,240
 then the falling, the clear awareness of it. So you can

154
00:07:21,240 --> 00:07:22,440
 really tell the difference,

155
00:07:22,440 --> 00:07:25,880
 the difference between simply saying it and you know it for

156
00:07:25,880 --> 00:07:26,640
 a second and then

157
00:07:26,640 --> 00:07:30,270
 you're up in the brain saying rising and so on and falling

158
00:07:30,270 --> 00:07:31,240
 and actually knowing

159
00:07:31,240 --> 00:07:35,140
 rising, falling and that's really the trick. It's something

160
00:07:35,140 --> 00:07:35,640
 that's quite

161
00:07:35,640 --> 00:07:38,250
 difficult if you've never done it. It's something that

162
00:07:38,250 --> 00:07:40,160
 takes time to perfect and

163
00:07:40,160 --> 00:07:45,040
 time to develop. Okay and another question I'm gonna pack

164
00:07:45,040 --> 00:07:45,360
 them together

165
00:07:45,360 --> 00:07:49,130
 here because they're fairly simple. Second one is sorry

166
00:07:49,130 --> 00:07:50,080
 could you please

167
00:07:50,080 --> 00:07:53,360
 answer a few questions. Well it's actually only one

168
00:07:53,360 --> 00:07:54,680
 question, oh two

169
00:07:54,680 --> 00:07:59,110
 questions. When meditating how to breathe can only breathe

170
00:07:59,110 --> 00:08:00,360
 through your nose or

171
00:08:00,360 --> 00:08:04,720
 inhale through the nose, exhale through your mouth. Okay

172
00:08:04,720 --> 00:08:07,840
 how to breathe. I

173
00:08:07,840 --> 00:08:12,320
 suppose the simplest answer is to say however you normally

174
00:08:12,320 --> 00:08:13,600
 breathe when you're

175
00:08:13,600 --> 00:08:15,940
 relaxed, when you're not thinking and when you're not

176
00:08:15,940 --> 00:08:17,520
 forcing the breath, when

177
00:08:17,520 --> 00:08:20,960
 you're not running, when you're sitting still normally, how

178
00:08:20,960 --> 00:08:21,280
 do you

179
00:08:21,280 --> 00:08:27,590
 breathe? Because the idea of breathing in through your nose

180
00:08:27,590 --> 00:08:28,360
,

181
00:08:28,360 --> 00:08:32,230
 inhale through the nose and exhale through the mouth, that

182
00:08:32,230 --> 00:08:33,560
 sounds like a

183
00:08:33,560 --> 00:08:38,240
 construct. It sounds like you're actively conscious. You're

184
00:08:38,240 --> 00:08:38,960
 making a conscious

185
00:08:38,960 --> 00:08:42,520
 effort to breathe in one and breathe out the other. We

186
00:08:42,520 --> 00:08:43,640
 normally don't breathe that

187
00:08:43,640 --> 00:08:48,760
 way. If you subconsciously, unconsciously breathe that way

188
00:08:48,760 --> 00:08:49,300
 then there's no

189
00:08:49,300 --> 00:08:51,800
 problem with it. But as soon as you start to say okay in

190
00:08:51,800 --> 00:08:52,920
 through the mouth, into the

191
00:08:52,920 --> 00:08:55,630
 nose, out through the mouth or in the mouth, out through

192
00:08:55,630 --> 00:08:56,360
 the nose or however

193
00:08:56,360 --> 00:09:01,960
 you want to do it, if you start to say you know opening

194
00:09:01,960 --> 00:09:02,720
 your mouth and trying

195
00:09:02,720 --> 00:09:04,410
 to breathe through the mouth when normally you'd breathe

196
00:09:04,410 --> 00:09:04,960
 through the nose

197
00:09:04,960 --> 00:09:09,780
 or vice versa, then your mind isn't really with the present

198
00:09:09,780 --> 00:09:10,400
 moment. It isn't

199
00:09:10,400 --> 00:09:13,360
 with the breath, it isn't with the stomach, it isn't with

200
00:09:13,360 --> 00:09:14,240
 the body, with

201
00:09:14,240 --> 00:09:18,040
 reality. It's on this intention okay now through the mouth,

202
00:09:18,040 --> 00:09:18,800
 now through the nose.

203
00:09:18,800 --> 00:09:24,600
 It's not natural. So you have this process on top of the

204
00:09:24,600 --> 00:09:25,500
 observation. You're

205
00:09:25,500 --> 00:09:28,940
 no longer simply observing the emotion which is really what

206
00:09:28,940 --> 00:09:30,040
 we're trying to do

207
00:09:30,040 --> 00:09:32,890
 here. And it should be clearly understood that that's where

208
00:09:32,890 --> 00:09:33,940
 the practice is. The

209
00:09:33,940 --> 00:09:38,440
 practice is in simply observing a motion that is already

210
00:09:38,440 --> 00:09:40,180
 occurring. And so in the

211
00:09:40,180 --> 00:09:43,540
 technique that I follow it, it doesn't really matter what

212
00:09:43,540 --> 00:09:44,440
 goes on up here.

213
00:09:44,440 --> 00:09:47,710
 We're not focused on this aspect of the breathing. We're

214
00:09:47,710 --> 00:09:48,780
 focused on the

215
00:09:48,780 --> 00:09:54,860
 stomach aspect, the aspect of the contact with the body and

216
00:09:54,860 --> 00:09:56,600
 the expansion of the

217
00:09:56,600 --> 00:10:00,420
 body based on the the breath going in and on the

218
00:10:00,420 --> 00:10:02,160
 contraction based on it going

219
00:10:02,160 --> 00:10:06,440
 out. So it shouldn't have any difference. But if you're

220
00:10:06,440 --> 00:10:07,680
 practicing mindfulness of

221
00:10:07,680 --> 00:10:10,810
 breathing, I would say if you're focusing on the nose, I

222
00:10:10,810 --> 00:10:12,480
 would say the same goes

223
00:10:12,480 --> 00:10:16,280
 where you're focusing on the breath area. Then you shouldn

224
00:10:16,280 --> 00:10:18,840
't try to control the

225
00:10:18,840 --> 00:10:22,690
 nature of the breath. You should be watching it. And I

226
00:10:22,690 --> 00:10:24,840
 suppose if you

227
00:10:24,840 --> 00:10:29,650
 were practicing for another purpose, if your purpose was to

228
00:10:29,650 --> 00:10:32,640
 develop certain

229
00:10:32,640 --> 00:10:37,410
 super mundane or supernatural states of magical powers or

230
00:10:37,410 --> 00:10:39,080
 develop great strength

231
00:10:39,080 --> 00:10:45,970
 of mind or great bliss and special meditative states as

232
00:10:45,970 --> 00:10:46,960
 opposed to simply

233
00:10:46,960 --> 00:10:50,480
 trying to understand things as they are, then some control

234
00:10:50,480 --> 00:10:52,120
 can be useful because

235
00:10:52,120 --> 00:10:56,650
 it develops your concentration. You know, Pragna yoga, Pr

236
00:10:56,650 --> 00:10:57,320
ana yoga,

237
00:10:57,320 --> 00:11:01,600
 I don't know how they say it, they're very much into deep

238
00:11:01,600 --> 00:11:02,880
 breaths. And when I

239
00:11:02,880 --> 00:11:06,210
 did martial arts, we were into deep breaths and so on. And

240
00:11:06,210 --> 00:11:07,280
 the idea was to

241
00:11:07,280 --> 00:11:10,240
 get your breath, slow your breath down to one minute per

242
00:11:10,240 --> 00:11:12,200
 breath and so on. So in

243
00:11:12,200 --> 00:11:14,960
 that case, you know, some people even I think can breathe

244
00:11:14,960 --> 00:11:15,800
 in through one

245
00:11:15,800 --> 00:11:18,820
 nostril and out through the other. They have it down to

246
00:11:18,820 --> 00:11:20,160
 such a side. And this

247
00:11:20,160 --> 00:11:23,530
 gives you great power. Now this is not what we're looking

248
00:11:23,530 --> 00:11:24,520
 for here because this

249
00:11:24,520 --> 00:11:30,320
 power is most often accompanied by delusion, the ego, the

250
00:11:30,320 --> 00:11:31,560
 idea that I, that

251
00:11:31,560 --> 00:11:37,840
 this is me, this is I am doing it, the control idea. And it

252
00:11:37,840 --> 00:11:38,560
 works for a while

253
00:11:38,560 --> 00:11:42,110
 but eventually it breaks apart and fall, it breaks down,

254
00:11:42,110 --> 00:11:43,120
 falls apart and

255
00:11:43,120 --> 00:11:48,970
 disappears. And so it isn't really a self or an ego or me

256
00:11:48,970 --> 00:11:51,040
 or mine. And this kind

257
00:11:51,040 --> 00:11:54,070
 of delusion is simply a waste of time and worse it leads

258
00:11:54,070 --> 00:11:55,160
 you on the wrong path.

259
00:11:55,160 --> 00:11:58,810
 It keeps you from understanding things and letting go of

260
00:11:58,810 --> 00:12:00,320
 things, seeing things

261
00:12:00,320 --> 00:12:05,110
 as they are. So no, the breath should be natural. It should

262
00:12:05,110 --> 00:12:06,680
 be however it happens,

263
00:12:06,680 --> 00:12:13,240
 I would say, however it happens when you're asleep or

264
00:12:13,240 --> 00:12:14,160
 however it

265
00:12:14,160 --> 00:12:16,520
 happens when you're just relaxed, when you're not thinking

266
00:12:16,520 --> 00:12:17,560
 about it. It should

267
00:12:17,560 --> 00:12:23,520
 continue on in that manner. And what you'll see is the

268
00:12:23,520 --> 00:12:24,360
 difference between

269
00:12:24,360 --> 00:12:26,900
 your normal breath and your meditative breath is the

270
00:12:26,900 --> 00:12:29,200
 problem. That when you're

271
00:12:29,200 --> 00:12:31,740
 meditating that your breath is forced, your breath is

272
00:12:31,740 --> 00:12:32,920
 controlled and that's the

273
00:12:32,920 --> 00:12:36,570
 problem. And that's why we're meditating because anytime we

274
00:12:36,570 --> 00:12:37,640
 focus on something

275
00:12:37,640 --> 00:12:39,600
 or there'd be something outside of us, anything we pay

276
00:12:39,600 --> 00:12:40,800
 attention to it, we can't,

277
00:12:40,800 --> 00:12:45,940
 we're unable ordinary beings to simply observe and be aware

278
00:12:45,940 --> 00:12:47,320
 of it. We have to

279
00:12:47,320 --> 00:12:52,210
 control, we have to obtain, possess and so on. And this is

280
00:12:52,210 --> 00:12:54,040
 the problem. Once, until

281
00:12:54,040 --> 00:12:57,090
 we learn that this is suffering, this is a cause for stress

282
00:12:57,090 --> 00:12:58,920
, that this is a cause

283
00:12:58,920 --> 00:13:03,040
 for all of the troubles in our lives. Well, then we'll

284
00:13:03,040 --> 00:13:05,120
 always have, have

285
00:13:05,120 --> 00:13:09,660
 difficulties, troubles and stress. Once we realize this,

286
00:13:09,660 --> 00:13:11,280
 then our breath will come

287
00:13:11,280 --> 00:13:15,060
 very much back to normal, our whole body, our whole, our

288
00:13:15,060 --> 00:13:16,600
 brain, the whole of our

289
00:13:16,600 --> 00:13:21,360
 being will be very much more natural. And when we focus on,

290
00:13:21,360 --> 00:13:22,800
 when we are aware of

291
00:13:22,800 --> 00:13:26,600
 something, it won't change. When we're aware of the breath,

292
00:13:26,600 --> 00:13:27,740
 the stomach rising

293
00:13:27,740 --> 00:13:30,750
 and falling will be as if we weren't even paying attention

294
00:13:30,750 --> 00:13:31,880
 to it. It won't change

295
00:13:31,880 --> 00:13:38,360
 in this light. Now this is important. This is the, really

296
00:13:38,360 --> 00:13:42,040
 what we're aiming for. So

297
00:13:42,040 --> 00:13:48,370
 the breath, definitely not a good idea to try to force it

298
00:13:48,370 --> 00:13:49,820
 in one way or another

299
00:13:49,820 --> 00:13:53,340
 unless you're practicing those kinds of meditation. If you

300
00:13:53,340 --> 00:13:53,840
're trying to

301
00:13:53,840 --> 00:13:56,360
 understand things as they are, let the breath go as it will

302
00:13:56,360 --> 00:13:57,600
. And notice when it's

303
00:13:57,600 --> 00:14:03,760
 not natural, when it's not ordinary, when it's not uncond

304
00:14:03,760 --> 00:14:06,480
itioned, and you'll see

305
00:14:06,480 --> 00:14:09,540
 that that's where there is, there is delusion, there is the

306
00:14:09,540 --> 00:14:10,920
 idea of control,

307
00:14:10,920 --> 00:14:13,680
 of forcing and so on. And it's a habit. It's something that

308
00:14:13,680 --> 00:14:14,640
 we've developed and

309
00:14:14,640 --> 00:14:18,140
 that we're trying to do away with. Because once you see

310
00:14:18,140 --> 00:14:19,840
 that it's unpleasant,

311
00:14:19,840 --> 00:14:22,470
 that it's a cause for suffering, this forcing, this control

312
00:14:22,470 --> 00:14:23,840
, you'll let go of it

313
00:14:23,840 --> 00:14:28,730
 after you repeatedly observe this. Eventually your mind

314
00:14:28,730 --> 00:14:30,080
 will change the

315
00:14:30,080 --> 00:14:32,980
 habit. It will say, "No, this is not leading to happiness.

316
00:14:32,980 --> 00:14:33,880
 It's actually leading to

317
00:14:33,880 --> 00:14:37,480
 stress and suffering." Okay, so there's two questions. I'll

318
00:14:37,480 --> 00:14:38,560
 try to answer some more

319
00:14:38,560 --> 00:14:41,720
 now. Actually I'm thinking I'm going to bring my video

320
00:14:41,720 --> 00:14:43,720
 camera, this little

321
00:14:43,720 --> 00:14:49,110
 portable video camera that was donated three years ago and

322
00:14:49,110 --> 00:14:50,080
 has seen a lot of

323
00:14:50,080 --> 00:14:54,370
 use and is a good little camera. I think I'll bring it with

324
00:14:54,370 --> 00:14:55,880
 me when I go to

325
00:14:55,880 --> 00:15:00,370
 America and because I'm not going to be doing so much. So

326
00:15:00,370 --> 00:15:02,040
 probably I'll have some

327
00:15:02,040 --> 00:15:05,070
 time to answer some videos on the road, answer some

328
00:15:05,070 --> 00:15:06,640
 questions on the road. And

329
00:15:06,640 --> 00:15:10,550
 I've got, I think I've still got about a hundred questions

330
00:15:10,550 --> 00:15:12,040
 to answer. I'm sorry, I

331
00:15:12,040 --> 00:15:15,240
 mean I'd like to open it up. I'd like to take questions and

332
00:15:15,240 --> 00:15:16,320
 answer questions that

333
00:15:16,320 --> 00:15:20,380
 people have. What do you do when you've got a hundred

334
00:15:20,380 --> 00:15:22,600
 waiting? It just keeps

335
00:15:22,600 --> 00:15:27,260
 piling up and piling up. So you'll have to bear with me.

336
00:15:27,260 --> 00:15:28,680
 And I know a lot of

337
00:15:28,680 --> 00:15:32,780
 people have as a result of closing it down or stopping to

338
00:15:32,780 --> 00:15:33,800
 accept questions.

339
00:15:33,800 --> 00:15:36,870
 They've started sending questions to me directly which I

340
00:15:36,870 --> 00:15:38,200
 really can't do. That's

341
00:15:38,200 --> 00:15:42,640
 the reason for, that's even worse for me because then I

342
00:15:42,640 --> 00:15:43,800
 have to answer the same

343
00:15:43,800 --> 00:15:49,000
 question when each person asks it. And so I find myself

344
00:15:49,000 --> 00:15:50,560
 with a far bigger

345
00:15:50,560 --> 00:16:01,280
 workload than I'm actually able to handle. So this is the

346
00:16:01,280 --> 00:16:02,080
 best I can do.

347
00:16:02,080 --> 00:16:06,010
 If you really have questions, and this is what I'd really

348
00:16:06,010 --> 00:16:08,280
 like to see, come on

349
00:16:08,280 --> 00:16:12,740
 out. Come on over to Sri Lanka. It's a beautiful place. I

350
00:16:12,740 --> 00:16:15,000
 haven't seen any, well

351
00:16:15,000 --> 00:16:17,630
 it's a one poisonous snake, a couple of scorpions, lots of

352
00:16:17,630 --> 00:16:19,920
 leeches. But other than

353
00:16:19,920 --> 00:16:26,060
 that, it's no worries here. If you come on over you can ask

354
00:16:26,060 --> 00:16:26,360
 all the

355
00:16:26,360 --> 00:16:29,140
 questions you like and I'm happy to answer them as long as

356
00:16:29,140 --> 00:16:29,840
 you dedicate

357
00:16:29,840 --> 00:16:34,120
 yourself to the meditation practice and join us in our

358
00:16:34,120 --> 00:16:37,260
 practice. Okay, so thanks

359
00:16:37,260 --> 00:16:40,680
 for tuning in. All the best.

