1
00:00:00,000 --> 00:00:08,070
 Hello and welcome to Monk Radio. This is August 19th, 2012

2
00:00:08,070 --> 00:00:11,000
 and these are the announcements for this week.

3
00:00:11,000 --> 00:00:16,990
 First announcement is I have two wonderful two. Last week I

4
00:00:16,990 --> 00:00:22,000
 had one. This week I have not one but two assistants.

5
00:00:22,000 --> 00:00:29,480
 So welcome Desmond and Kappila who are in America and Japan

6
00:00:29,480 --> 00:00:32,000
. And they'll be helping to read the questions off.

7
00:00:32,000 --> 00:00:35,240
 I think what we'll do is have you take turns so one person

8
00:00:35,240 --> 00:00:39,000
 read one, the other person, the next person read the next.

9
00:00:39,000 --> 00:00:43,700
 Save me from having to ask and answer as well. And then you

10
00:00:43,700 --> 00:00:47,260
're also welcome to join in and comment and even answer the

11
00:00:47,260 --> 00:00:51,000
 questions as you feel comfortable.

12
00:00:51,000 --> 00:00:55,140
 Next announcement is that we've added a Saturday to our

13
00:00:55,140 --> 00:00:59,160
 broadcast schedule. Many of you are already aware and

14
00:00:59,160 --> 00:01:02,000
 already took part in yesterday's session.

15
00:01:02,000 --> 00:01:07,530
 But every week now on Saturday at the same time as this

16
00:01:07,530 --> 00:01:13,560
 broadcast, which is 2 p.m. UTC or 2 p.m. Greenwich Mean

17
00:01:13,560 --> 00:01:16,000
 Time, I think is what it used to be called.

18
00:01:16,000 --> 00:01:22,000
 We're going to have a dumb group online, live, broadcast.

19
00:01:22,000 --> 00:01:26,700
 So it's going to be a group of people in our community who

20
00:01:26,700 --> 00:01:32,000
 have been added to my elite circles.

21
00:01:32,000 --> 00:01:37,130
 People who have requested to be added to my circles and who

22
00:01:37,130 --> 00:01:42,260
 I think would be a good part of the community, not causing

23
00:01:42,260 --> 00:01:45,000
 trouble and disturbance.

24
00:01:45,000 --> 00:01:50,630
 People who have webcams, people who have fast internet

25
00:01:50,630 --> 00:01:55,360
 connections, and people who are interested in sharing the

26
00:01:55,360 --> 00:01:56,000
 dumb.

27
00:01:56,000 --> 00:02:02,090
 So these people we had yesterday, six or seven people, I

28
00:02:02,090 --> 00:02:06,320
 think most we had seven. A couple of them had slow internet

29
00:02:06,320 --> 00:02:08,000
 connections, so it didn't work out the best.

30
00:02:08,000 --> 00:02:12,710
 But for the most part, it was very successful, went off

31
00:02:12,710 --> 00:02:18,250
 without much of a hitch. We have an agenda that we're

32
00:02:18,250 --> 00:02:20,000
 following.

33
00:02:20,000 --> 00:02:27,970
 We are sharing mamma that we've discovered over the week

34
00:02:27,970 --> 00:02:29,000
 from the internet or from wherever.

35
00:02:29,000 --> 00:02:37,120
 We're reading texts. We're taking presets, which we should

36
00:02:37,120 --> 00:02:41,290
 be doing tonight. We should have done that first. We'll do

37
00:02:41,290 --> 00:02:42,000
 that after this.

38
00:02:42,000 --> 00:02:48,140
 And meditating together and then having a tea time at the

39
00:02:48,140 --> 00:02:52,460
 end. We added yesterday informal chat at the end where we

40
00:02:52,460 --> 00:02:55,990
 sit around and drink tea as though we were all sitting

41
00:02:55,990 --> 00:02:57,000
 together.

42
00:02:57,000 --> 00:02:59,500
 And that's broadcast live, so if you want to just watch,

43
00:02:59,500 --> 00:03:02,000
 you're welcome to come and watch as you're watching today.

44
00:03:02,000 --> 00:03:05,220
 That would have been yesterday and will be the same time

45
00:03:05,220 --> 00:03:08,000
 next week. So that's another announcement.

46
00:03:08,000 --> 00:03:12,460
 The final announcement that I can think of is just a quick

47
00:03:12,460 --> 00:03:17,670
 announcement that looking at my YouTube channel, it looks

48
00:03:17,670 --> 00:03:23,000
 like I'm about to hit two million views, which...

49
00:03:23,000 --> 00:03:26,930
 No, it actually is kind of... When I think about it, it

50
00:03:26,930 --> 00:03:30,230
 doesn't mean so much to me, but just thought it was a

51
00:03:30,230 --> 00:03:35,000
 landmark that I should let everyone know.

52
00:03:35,000 --> 00:03:37,140
 Thanks everyone for your interest and for getting involved

53
00:03:37,140 --> 00:03:43,150
 in sharing the dhamma if it weren't for so many people

54
00:03:43,150 --> 00:03:48,000
 watching the videos and expressing their appreciation.

55
00:03:48,000 --> 00:03:53,000
 Probably would have stopped a long time ago. So thank you.

56
00:03:53,000 --> 00:03:58,590
 And here's to another million views, the next million views

57
00:03:58,590 --> 00:03:59,000
.

58
00:03:59,000 --> 00:04:01,770
 Okay, that's all the announcements I have. Does anyone...

59
00:04:01,770 --> 00:04:04,760
 Larry also joined us now. Welcome Larry. He was there

60
00:04:04,760 --> 00:04:06,000
 yesterday as well.

61
00:04:06,000 --> 00:04:11,000
 Does anyone else have anything they'd like to announce?

62
00:04:11,000 --> 00:04:14,760
 This isn't really going to be a group discussion, so we're

63
00:04:14,760 --> 00:04:18,020
 going to try to skip right ahead to taking people's

64
00:04:18,020 --> 00:04:20,000
 comments and questions.

65
00:04:20,000 --> 00:04:22,000
 So that's all for today.

