1
00:00:00,000 --> 00:00:04,800
 main differences between meditation and prayer.

2
00:00:04,800 --> 00:00:09,040
 Anybody?

3
00:00:09,040 --> 00:00:17,840
 Meditation you're trying to quiet your mind.

4
00:00:17,840 --> 00:00:26,480
 In prayer? I'm talking about kalmabai meditation and

5
00:00:26,960 --> 00:00:35,040
 the goal is to quiet your mind, not so much converse.

6
00:00:35,040 --> 00:00:39,360
 I had these Christians evangelists try to explain to me

7
00:00:39,360 --> 00:00:39,360
 that

8
00:00:39,360 --> 00:00:45,120
 in Christianity is objective.

9
00:00:45,120 --> 00:00:51,200
 And I'm assuming this is the meaning is in terms of prayer

10
00:00:51,200 --> 00:00:54,450
 because you're focusing they said on something outside of

11
00:00:54,450 --> 00:00:55,040
 yourself

12
00:00:55,040 --> 00:00:59,840
 which is objective. So God is objective.

13
00:00:59,840 --> 00:01:04,000
 Of course I turned the tables on them and said no he's not.

14
00:01:04,000 --> 00:01:06,710
 They said you're subjective because you're focusing on

15
00:01:06,710 --> 00:01:07,360
 yourselves

16
00:01:07,360 --> 00:01:13,120
 which is subjective. But no it's not.

17
00:01:13,120 --> 00:01:16,320
 It's interesting how you can totally see

18
00:01:16,320 --> 00:01:21,040
 reality from different perspectives. This

19
00:01:21,040 --> 00:01:26,960
 evangelist, evangelical, was telling me

20
00:01:26,960 --> 00:01:31,040
 focusing on something outside of yourself is objective

21
00:01:31,040 --> 00:01:34,720
 and focusing on something inside is subjective.

22
00:01:34,720 --> 00:01:38,160
 And in Buddhism of course we believe the opposite that

23
00:01:38,160 --> 00:01:44,320
 focusing on your own experience is objective. Focusing on a

24
00:01:44,320 --> 00:01:45,200
 concept outside

25
00:01:45,200 --> 00:01:48,320
 of yourself that you aren't experiencing is subjective.

26
00:01:48,320 --> 00:01:52,000
 And so I said to him, you know your concept of God is

27
00:01:52,000 --> 00:01:54,890
 totally different from the Hindu concept of God for example

28
00:01:54,890 --> 00:01:55,840
. So how objective is

29
00:01:55,840 --> 00:01:58,640
 that? It's actually quite subjective. You believe

30
00:01:58,640 --> 00:02:02,300
 God to be this and this and this and so on and other people

31
00:02:02,300 --> 00:02:03,280
 believe otherwise.

32
00:02:03,280 --> 00:02:07,360
 But experience is what it is. You see, I see.

33
00:02:07,360 --> 00:02:11,200
 Seeing is seeing no matter who sees it.

34
00:02:11,200 --> 00:02:15,120
 So maybe that's a good answer to this question how

35
00:02:15,120 --> 00:02:21,600
 prayer is subjective or objective depending who you ask.

36
00:02:21,600 --> 00:02:26,750
 And meditation is the opposite again depending on who you

37
00:02:26,750 --> 00:02:27,040
 ask.

38
00:02:27,040 --> 00:02:31,360
 And I guess how you pray, you know, is your type of prayer

39
00:02:31,360 --> 00:02:31,920
 one where you're

40
00:02:31,920 --> 00:02:36,880
 just trying to be quiet and sense God or try to converse

41
00:02:36,880 --> 00:02:42,000
 with God and ask as things of God. Because I have read

42
00:02:42,000 --> 00:02:44,400
 accounts of

43
00:02:44,400 --> 00:02:49,440
 Christians reaching the states that I would

44
00:02:49,440 --> 00:02:54,320
 describe as a jhana, the way they described it,

45
00:02:54,320 --> 00:02:59,040
 just from being quiet. You know, in their mind

46
00:02:59,040 --> 00:03:02,160
 got quiet and they had and they experienced

47
00:03:02,160 --> 00:03:06,480
 what seems to be like a jhana type state. You know, they

48
00:03:06,480 --> 00:03:07,040
 would have their

49
00:03:07,040 --> 00:03:12,640
 mythology behind that. But I think it's certainly possible

50
00:03:12,640 --> 00:03:13,040
 for

51
00:03:13,040 --> 00:03:17,760
 a Christian to enter some of these

52
00:03:17,760 --> 00:03:22,400
 Comma Bide states if they get their mind still enough.

53
00:03:22,400 --> 00:03:30,960
 Indeed. So on a deeper level it's a question of where your

54
00:03:30,960 --> 00:03:33,520
 mind is.

55
00:03:33,520 --> 00:03:36,140
 Prayer on a superficial level is quite different from

56
00:03:36,140 --> 00:03:38,240
 meditation because

57
00:03:38,240 --> 00:03:41,760
 I think the word prayer means to ask for something, no?

58
00:03:41,760 --> 00:03:46,320
 To request something. Please this, please that. That's I

59
00:03:46,320 --> 00:03:47,520
 think the definition in

60
00:03:47,520 --> 00:03:52,640
 the dictionary of prayer. But what is the purpose of

61
00:03:52,640 --> 00:03:53,280
 communicating

62
00:03:53,280 --> 00:03:58,400
 with God? If it's, as you say, in some cases it's best not

63
00:03:58,400 --> 00:03:58,640
 called

64
00:03:58,640 --> 00:04:01,540
 prayer, it's better called meditation because it quiets the

65
00:04:01,540 --> 00:04:02,160
 mind.

66
00:04:02,160 --> 00:04:07,680
 Of course, inside meditation is, I would still say,

67
00:04:09,360 --> 00:04:13,840
 I don't think it could ever be compared to prayer.

68
00:04:13,840 --> 00:04:17,440
 No, because again it's not,

69
00:04:17,440 --> 00:04:21,760
 it's on a different level. How would you describe the

70
00:04:21,760 --> 00:04:22,080
 difference

71
00:04:22,080 --> 00:04:25,360
 between insight meditation and prayer, insight meditation

72
00:04:25,360 --> 00:04:27,440
 specifically?

73
00:04:27,440 --> 00:04:30,640
 The difference between insight meditation and prayer.

74
00:04:30,640 --> 00:04:34,290
 Why prayer could never be considered insight meditation and

75
00:04:34,290 --> 00:04:36,480
 vice versa?

76
00:04:38,800 --> 00:04:42,080
 Because insight meditation, you're just looking at the bare

77
00:04:42,080 --> 00:04:46,880
 experience of what is happening. You're not

78
00:04:46,880 --> 00:04:55,440
 trying to communicate to God. You're just getting into

79
00:04:55,440 --> 00:05:02,640
 bare experience and those are just two different things.

80
00:05:03,600 --> 00:05:05,600
 Yeah.

81
00:05:05,600 --> 00:05:12,100
 Prayer, I think, in its essence has a concept as its object

82
00:05:12,100 --> 00:05:12,960
 has.

83
00:05:12,960 --> 00:05:16,480
 And by concept, if we don't want to be, use it in a

84
00:05:16,480 --> 00:05:20,960
 pejorative sense, something that you're not experiencing,

85
00:05:20,960 --> 00:05:04,160
 it's, you try to connect with something beyond your

86
00:05:04,160 --> 00:05:30,000
 experience.

87
00:05:30,000 --> 00:05:35,280
 And so from a Buddhist point of view, it's essentially

88
00:05:35,280 --> 00:05:40,960
 conceptual. I think that's the essence of the answer.

89
00:05:40,960 --> 00:05:47,120
 In insight meditation, you focus on experience and only

90
00:05:47,120 --> 00:05:48,080
 experience.

91
00:05:48,080 --> 00:05:53,520
 So for example, if you see God, it's just seeing. It's not

92
00:05:53,520 --> 00:05:54,400
 really God.

93
00:05:54,400 --> 00:05:56,830
 So that there's a good example. If you see God, prayer

94
00:05:56,830 --> 00:05:57,920
 would be to talk to him.

95
00:05:57,920 --> 00:06:01,760
 Insight meditation would be to say seeing, seeing, seeing.

96
00:06:01,760 --> 00:06:04,240
 Quite the right.

97
00:06:04,240 --> 00:06:08,560
 So

