 Hello and welcome back to Ask a Monk. Today I'll be
 answering the question as
 to what a Buddhist does when someone they know dies, which
 is not
 directly related to the meditation practice, but as Buddh
ists and
 meditators in the Buddhist tradition, this is something
 that we, an experience
 that we have to deal with, that for most of us will be on
 the extreme end,
 something that is quite difficult to deal with and to come
 to terms with.
 So simply meditating in a way is not really sufficient. So
 there are things
 that we should talk about in regards to this. From a
 practical point of view, a
 Buddhist point of view, I would say there are three things
 that we should do, or
 that we should keep in mind, three duties as Buddhists, or
 three points to keep in
 mind when a person we know dies. The first one is that we
 should never ever
 grieve or think that grieving is somehow appropriate,
 somehow useful, somehow good.
 The reason why we grieve is because it's great suffering
 for us. You can think of
 a small child who has to stay with a babysitter the first
 time when their
 parents go away, will often cry all night thinking that
 they're not
 even thinking that their parents have left, but simply
 suffering terribly
 because of the clinging that they have, that they've
 developed as a small child
 for their parents. So there's this terrible suffering that
 goes on at loss,
 which really carries over into everything in our lives.
 People cry over many
 different things. Now the point being that we tend to cling
 to people a lot
 more than we cling to any other object, really. It's there,
 people are,
 other people are things that we can interact with, that
 have great deep
 meaning for us in our lives, and so we cling to them
 greatly. This is why we
 feel suffering when a person passes away. It's not based on
 any logic or reason
 that a person should grieve, and we should really get this
 point across
 that there is no reason to mourn. It is something that is
 based on our
 addiction. The reason why we cry is because of the
 chemicals involved with
 crying. Crying is a reaction that we have developed as a
 species to deal with
 difficult situations. It's something that releases pleasure
 chemicals and
 endorphins, I think it makes you feel calm and happy and
 peaceful, so it
 becomes an addiction actually. This is how grieving works.
 It can be
 crying, some people take to alcohol or any sort of
 addiction, but
 crying is again another addiction. We should never think
 that this is
 somehow important or a part of the process, a part of the
 grieving process.
 Many people will go on for years suffering because a person
 has passed
 away. The point is that there's no benefit to it. You don't
 help the
 person who passed away, you don't help yourself, you don't
 do something that
 they would want for you, and there's no one who wants their
 loved ones to
 suffer, to mourn, to have to be without. In the end it's
 really just
 useless, and in fact worse than useless, it drags you down
 in many ways. It drags
 you down into a cycle of suffering and depression and also
 addiction.
 It makes you become, because you start to find a way out,
 you try to find a
 way to block it out and not have to think about it, which
 gives rise to
 many different types of addiction. Now this brings up a
 good point that often
 people are unable to follow this advice. This first point,
 important as it is, it's
 best phrased as don't see it as a good thing, because if
 you say to people
 don't mourn, well unless they had no attachment to the
 person they're going
 to feel sad. I've given this talk before at funerals and
 then after you
 give a talk the people come up and cry. So you think well
 why did I explain to
 them about not to cry and how it's not really useful or any
 benefit, but they
 didn't intend to. You can't blame people for crying when a
 loved one dies. So it's
 but some people go to the next level and think yes, yes
 this is good and this is
 important and so they encourage it in themselves and they
 encourage the
 phenomenon and so they end up crying more and they think of
 it as somehow a
 good thing and so they can get caught up and actually waste
 a lot of time and
 energy doing so. But this also raises another point that we
 most often get
 ourselves into this position. Why when a person dies we can
 go on for days and
 weeks and months and even years mourning is because we've
 been
 negligent, we've been heedless in regards to our mind. We
 haven't been
 paying attention to our attachments and you know we live
 our lives thinking that
 attaching to other people is good. It's intimacy and
 relationships and love
 and so on. But this really has nothing to do with love, it
 has to do
 with our own clinging. When you love someone it's not what
 they can do for
 you and what they bring to you and the great things that
 they give to
 you. That's a kind of, we use the word love, but it really
 is an attachment.
 Love is when you wish for good things for other people and
 there's nothing good
 that comes from crying. You don't somehow respect the
 person's memory by crying by
 bringing yourself suffering. You don't do something that
 they would want you to do
 or that they would want for you. So this is the first point
. But
 no, what I was going to say is that for this reason because
 we get ourselves
 into this mess, part of this is even before a person dies
 we should be quite
 careful with our relationships. Our relationships should be
 meaningful but
 the meaning should come from our love for each other, our
 wish for each
 other to be happy. And in fact our wish for all beings to
 be happy. The only way
 we can really be purely have pure love for other beings is
 to have it be
 impartial. Where it's not that we don't love anyone, we end
 up loving everyone.
 This is the Buddha said that we should strive to love all
 beings just as a
 mother might love her only child. And if we can get to this
 point, if we really
 have love for people it won't matter who comes and who goes
. Once it's true love
 then the next person you see is a loved one for you, is the
 one you love.
 And it continues on when they go, there's nothing in love
 that
 says they need to do anything for you or be anything for
 you. So when
 they're gone there's no suffering, there's no stress, there
's no
 sadness. So this is an important reason why we should be
 practicing
 meditation because when we don't practice meditation we
 cling and cling
 and cling and we set ourselves up for so much stress and
 suffering that we see
 all around us. Many people are living their lives in great
 peace and happiness
 simply because they haven't met with the inevitable crash,
 the inevitable loss,
 loss of youth when they get old, so many things that they
 can't enjoy and
 suddenly they're confronted with old age or sickness when
 people get
 cancer and eventually death. The death of loved ones or the
 death of one's
 own death was totally unprepared for. Meditation practice
 is to allow us to
 have meaningful but non-attached relationships with other
 people where we
 give and we spread the goodness all around us and try our
 best to
 make other people happy and to bring peace to them instead
 of wishing for
 them to bring good things to us, which is of course a
 totally uncertain something
 we can't depend on. So this is the first point, is that it
's really
 useless to grieve and it's something that as Buddhists we
 should not do, we
 should on the other hand spend our lives thinking about the
 inevitability of
 death and this brings me to the second point that when a
 person dies it should
 be a reminder to us and the Buddha was very clear about
 this for monks when a
 person dies and even for lay people when a person dies that
 we
 should consider this to be a sign, a sign that we too will
 have to go, we
 should be clear that this is something that we can't avoid,
 it's a fact of life.
 So the contemplation on the reality of death is a very
 important part of our
 acceptance when a person dies. So first of all the
 realization that
 this was a part of that person's life and that's a part of
 nature and not
 getting caught up in this idea of a person or this one life
 or this
 being existed and now they're gone or so on. But thinking
 in terms of reality, in
 terms of our experience and that this is the nature of
 reality that all
 things that arise in our mind, the idea of a person will
 eventually cease.
 Everything that we cling to, a person, a place, a thing,
 even an ideology,
 eventually we will have to give it up, nothing will last
 forever. And so we use
 death as a really good wake-up call for us to realize the
 truth of
 reality and also to reflect on our own mortality. When a
 person dies we should
 take that as a warning to ourselves, we should watch and
 see and look and
 think of the person who is dead and think this will also
 come to me one day,
 will I be ready for it? As would as we, this is an
 important, not only is it a
 good meditation and useful for ourselves but it's a really
 good way to come to
 terms with the person's death because instead of grieving
 or thinking oh now
 that person's gone, you realize that you remind yourself
 that this is
 reality and the real problem is our attachments to a
 specific state of being,
 of a person, a place, a thing being like this, not being
 like that or being with
 us and not being away from us. Once we start to look at
 reality, you know, and
 look at the experience of reality from moment to moment now
, what's happening
 and so on, then the idea of how things should be or how
 things are, could be or
 how things used to be or how things will be or so on is
 really irrelevant and it
 doesn't have the same hold on us. We're able to live here
 and now in peace and
 happiness and that here and now extends forever of course.
 When you're able to be
 happy here and now then you're able to be happy for
 eternity and that's the
 reality of life. So that's number two, is that we should
 use it as an opportunity
 to reflect on our own mortality and reflect on the inev
itability of death,
 that it comes to all beings and we should remind ourselves
 that this is
 really the reality of that person and it shouldn't actually
 be of any
 positive or negative significance, it should be seen as the
 way things are.
 We should not be happy or unhappy about it, should not be
 regretful or mourning or
 so on. This is what we should do, is we should take it as
 an opportunity to
 reflect on the way things go in life. Now this is more for
 ourselves, for the
 person who passed away, most people want to know what we
 should do for the person.
 This is the third point, that as Buddhists we should do
 things on behalf of
 the person who has passed away. We should, whatever good
 things that person
 undertook or even if they didn't do any good things, we
 should carry on their
 name by or celebrate their life by giving, by building
 things, by
 building things that will be of use to people, dedicating
 our works to them and
 so on, dedicating books or whatever we do, dedicating our
 work, our good deeds to
 that person, be it things that they did in their life and
 we can continue on or
 just things done in their name with some mention of them.
 Because this, this is
 really a way of celebrating the person's life and it's a
 way of bringing meaning
 to that person's life, it's a way of saying that this
 person's life had real
 meaning. If this person hadn't lived then these good deeds
 wouldn't come.
 We do these deeds especially for that person and therefore
 they are those
 person's good deeds that have been done. It's a way of
 respecting and celebrating
 and honoring the person who passed away. It's also a way of
 coming to terms with
 the death in the best way possible, celebrating the person
's life, saying,
 "This person passed away." Most people, the best, the best
 they can do is to
 remember the good things the person did and say, "Oh yeah,
 I was such a good
 person." So they celebrate and they have a party and they
 get drunk and so on and
 that's it. But that's not really celebrating a person's
 life. When you
 celebrate a person's life it should be all the good things
 that they did should
 be continued on or goodness should be done in their name.
 So if it's a person
 who means something to you and you think their life had
 meaning then you should
 do something. It may not be a lot but you should make
 effort to do something in
 their name and that will be your way of finding closure and
 of ending that
 person's life on a meaningful note. Saying, "This person
 died and the result
 was this. We have done this on behalf of that person."
 Sometimes what that means is
 an inheritance that we get. We use part of the inheritance
 to honor the person,
 to do something, not because out of duty or something but
 as a good deed
 for us and a good deed for them and something bringing
 goodness to the world
 on behalf of that person. What you find more often, which
 is really the worst
 thing a person could possibly do when another person dies,
 is people fight over
 the inheritance of their parents or people who passed away,
 which is really
 a horrible thing but apparently goes on quite often. And
 this is something as
 Buddhists we should guard against totally. We should never
 think of a
 person's, another person's possessions as somehow belonging
 to us or that somehow
 we deserve them. Never. We should never look at a person
 and think, "This person
 owes me something." As Buddhists we should never think like
 that. So the idea, even
 if your parents, you help them or whatever, you should
 never think that
 somehow that's going to give you some inheritance and you
're going to get
 something. We should be very careful to guard against this
 because it will crop
 up and when they die you'll find yourself fighting and
 breaking apart your family
 really. Could you imagine what your parents would, what the
 parents would
 think when they see their children you know at each other's
 throats over things
 that they actually left behind, that their parents had to
 throw away that's
 really garbage. It could be money, it could be a hole in
 possession, it's all
 garbage because in the end they had to leave it behind and
 you have to leave it
 behind. Much better is whatever good things that they have
 you can take some
 of it if it's useful to support you but making sure that
 everyone else is taken
 care of and giving things in their name. You know if the
 least you do is giving
 up some some hold that you have on what you know this this
 fights that people
 have over inheritance in order that harmony might be
 bestowed upon your
 family. For the sake of harmony and out of respect for the
 person who
 passed away I'm not going to fight over this. If that's the
 least you
 then you've done a great thing in their memory and you've
 honored their memory.
 So you know there are many ways that you can do this but
 this could be the
 least is to not for goodness sake don't fight over people
 who passed away fight
 over their belongings. It's a terrible terrible thing and
 be very careful not
 to let that happen to you because it might come into the
 mind we can never be
 sure we might find ourselves doing that. So these are three
 points that I think
 you should keep in mind when a person you know passes away
 as a Buddhist or as a
 person who follows these sorts of teachings and really any
 good teachings
 it's nothing specifically to do with with Buddhism. So hope
 this helps. Thanks
 for tuning in and all the best.
