1
00:00:00,000 --> 00:00:03,740
 When I practice sitting meditation, usually about 20

2
00:00:03,740 --> 00:00:05,160
 minutes into it, I feel

3
00:00:05,160 --> 00:00:09,760
 very relaxed and my vision with my eyes closed gets darker

4
00:00:09,760 --> 00:00:11,600
 and darker and I have

5
00:00:11,600 --> 00:00:15,560
 a feeling of falling. When this happens to me, am I on the

6
00:00:15,560 --> 00:00:17,880
 right track? Do I have

7
00:00:17,880 --> 00:00:23,300
 a focused concentration at this time or it's just nothing?

8
00:00:23,300 --> 00:00:36,260
 First of all, you should just observe as it is when it is

9
00:00:36,260 --> 00:00:37,220
 getting darker.

10
00:00:37,220 --> 00:00:46,700
 Observe seeing, seeing because it's your vision. It's

11
00:00:46,700 --> 00:00:47,860
 getting darker in your

12
00:00:47,860 --> 00:00:53,000
 vision you describe. So I would say seeing, seeing at that

13
00:00:53,000 --> 00:00:55,340
 time and then you

14
00:00:55,340 --> 00:01:00,560
 have the feeling of falling and then I would say feeling,

15
00:01:00,560 --> 00:01:03,260
 feeling, feeling and not

16
00:01:03,260 --> 00:01:07,750
 make a deal out of it. So when you think of it, when you

17
00:01:07,750 --> 00:01:09,660
 ponder over it and want

18
00:01:09,660 --> 00:01:18,030
 it to be something, something that your concentration is

19
00:01:18,030 --> 00:01:20,420
 high or so then

20
00:01:20,420 --> 00:01:26,340
 you are looking for again and this can lead you into what

21
00:01:26,340 --> 00:01:27,500
 we were talking

22
00:01:27,500 --> 00:01:34,280
 earlier into one of the Vipassana Pakilesa. So it is

23
00:01:34,280 --> 00:01:37,900
 dangerous to want to

24
00:01:37,900 --> 00:01:42,270
 know that. Is that something good or is that just nothing?

25
00:01:42,270 --> 00:01:44,340
 Be relaxed about it.

26
00:01:44,340 --> 00:01:53,640
 You feel relaxed, that's good. Notice relaxed, relaxed and

27
00:01:53,640 --> 00:01:57,260
 then you will be on

28
00:01:57,260 --> 00:02:03,340
 the right track. When you are looking for the gain or when

29
00:02:03,340 --> 00:02:05,340
 you are about to

30
00:02:05,340 --> 00:02:11,170
 judge is this good or is this not good then you are not on

31
00:02:11,170 --> 00:02:13,100
 the right track.

32
00:02:13,100 --> 00:02:19,100
 Again this very basic principle that I mentioned, it's the

33
00:02:19,100 --> 00:02:20,980
 practice that is the

34
00:02:20,980 --> 00:02:24,030
 right track. It's the practice which leads to progress. It

35
00:02:24,030 --> 00:02:27,780
's a very obvious

36
00:02:27,780 --> 00:02:31,140
 principle and yet it's so easy to lose sight of it because

37
00:02:31,140 --> 00:02:32,140
 we're talking about

38
00:02:32,140 --> 00:02:36,140
 human beings who are generally irrational. We act in very

39
00:02:36,140 --> 00:02:37,020
 irrational ways

40
00:02:37,020 --> 00:02:41,610
 and so if you think about it, I think if you think about it

41
00:02:41,610 --> 00:02:42,180
 objectively

42
00:02:42,180 --> 00:02:45,130
 you should be able to realize for yourself that no how

43
00:02:45,130 --> 00:02:46,020
 could you be on

44
00:02:46,020 --> 00:02:48,520
 the right track. At that moment you're no longer meditating

45
00:02:48,520 --> 00:02:50,980
 but because of our

46
00:02:50,980 --> 00:02:57,820
 irrational attachment to the strange, the mystical, the

47
00:02:57,820 --> 00:03:00,340
 super mundane, the special

48
00:03:00,340 --> 00:03:09,340
 we think that maybe now all my problems will be over. Maybe

49
00:03:09,340 --> 00:03:09,540
 now

50
00:03:09,540 --> 00:03:13,180
 suffering will cease. Maybe I don't have to practice

51
00:03:13,180 --> 00:03:17,140
 anymore and this is all

52
00:03:17,140 --> 00:03:22,290
 based on defilement and detachment. The practice is that

53
00:03:22,290 --> 00:03:24,700
 which leads to

54
00:03:24,700 --> 00:03:28,630
 progress and only the practice. Another thing you can

55
00:03:28,630 --> 00:03:30,620
 always remind yourself of,

56
00:03:30,620 --> 00:03:36,460
 my teacher said, this is not that. This is a motto you

57
00:03:36,460 --> 00:03:39,500
 should have. This is not that.

58
00:03:39,500 --> 00:03:43,730
 Whatever it is it's not that. It's not what you're looking

59
00:03:43,730 --> 00:03:45,660
 for. So if you just

60
00:03:45,660 --> 00:03:48,680
 keep reminding yourself of that then you won't cling to

61
00:03:48,680 --> 00:03:50,860
 anything and everything

62
00:03:50,860 --> 00:03:55,510
 that comes up you'll be able to let it go. The other thing

63
00:03:55,510 --> 00:03:56,180
 that I want to

64
00:03:56,180 --> 00:04:00,300
 say, though it's sort of already said, is that another

65
00:04:00,300 --> 00:04:02,140
 principle that we always

66
00:04:02,140 --> 00:04:05,590
 have to remind people of is we're not trying to gain

67
00:04:05,590 --> 00:04:08,260
 concentration. It's not a

68
00:04:08,260 --> 00:04:11,390
 sign that you're on the right track, that you have

69
00:04:11,390 --> 00:04:13,820
 concentration. Concentration is

70
00:04:13,820 --> 00:04:16,880
 a mind state that has to be balanced with energy. True

71
00:04:16,880 --> 00:04:18,940
 concentration when

72
00:04:18,940 --> 00:04:21,810
 balanced with energy doesn't feel concentrated at all. You

73
00:04:21,810 --> 00:04:22,540
 don't even notice

74
00:04:22,540 --> 00:04:27,740
 it. It's totally natural. You feel that you're just present

75
00:04:27,740 --> 00:04:29,140
 and that's

76
00:04:29,140 --> 00:04:33,690
 in a sense more focused than concentration. Focus is where

77
00:04:33,690 --> 00:04:34,060
 you're

78
00:04:34,060 --> 00:04:38,860
 perfectly clear. When a camera is focused it's not that it

79
00:04:38,860 --> 00:04:42,300
's all the way in.

80
00:04:42,300 --> 00:04:46,940
 It's halfway. It's right in the middle where you can see

81
00:04:46,940 --> 00:04:47,140
 things

82
00:04:47,140 --> 00:04:52,140
 clearly. This is very important because people get the

83
00:04:52,140 --> 00:04:52,900
 wrong idea

84
00:04:52,900 --> 00:04:55,820
 that meditation is about concentration. It's not what the

85
00:04:55,820 --> 00:04:57,020
 word meditation means.

86
00:04:57,020 --> 00:05:03,100
 It's not what insight is. It has nothing intrinsically to

87
00:05:03,100 --> 00:05:06,500
 do with development of

88
00:05:06,500 --> 00:05:10,840
 understanding. Understanding comes from focus, which means

89
00:05:10,840 --> 00:05:12,540
 to focus the lens and

90
00:05:12,540 --> 00:05:17,820
 to balance your faculties. Whenever you have some extreme

91
00:05:17,820 --> 00:05:18,580
 experience of

92
00:05:18,580 --> 00:05:24,250
 concentration or energy or whatever, this isn't the path.

93
00:05:24,250 --> 00:05:25,140
 It's some

94
00:05:25,140 --> 00:05:30,570
 byproduct side phenomenon. The path is being mindful of it,

95
00:05:30,570 --> 00:05:31,260
 acknowledging it

96
00:05:31,260 --> 00:05:35,020
 just as you do with everything else.

