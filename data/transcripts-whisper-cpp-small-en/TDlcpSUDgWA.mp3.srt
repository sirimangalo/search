1
00:00:00,000 --> 00:00:08,040
 Hello, I'm here today to announce something that I think

2
00:00:08,040 --> 00:00:09,940
 might be of interest to the online

3
00:00:09,940 --> 00:00:15,280
 community of people who follow my work.

4
00:00:15,280 --> 00:00:20,630
 This October I will be heading off to Thailand to pay

5
00:00:20,630 --> 00:00:24,440
 respect and visit, learn from and just

6
00:00:24,440 --> 00:00:31,140
 to see my teacher Ajahn Tong Siri Mangalow who will be 91

7
00:00:31,140 --> 00:00:34,640
 this year and has been probably

8
00:00:34,640 --> 00:00:42,840
 the biggest influence as a living person on my life, well

9
00:00:42,840 --> 00:00:45,480
 by far has been.

10
00:00:45,480 --> 00:00:48,280
 So someone who is very important to me.

11
00:00:48,280 --> 00:00:52,120
 As part of the trip, I will also be practicing on my own

12
00:00:52,120 --> 00:00:54,880
 and teaching others leading retreats

13
00:00:54,880 --> 00:00:58,170
 up on the mountain somewhere but as part of the trip to see

14
00:00:58,170 --> 00:00:59,720
 my teacher I will be making

15
00:00:59,720 --> 00:01:05,080
 an offering which is customary at the end of the reigns to

16
00:01:05,080 --> 00:01:07,520
 the offering of robes.

17
00:01:07,520 --> 00:01:10,770
 So at the end of the reigns after having three months where

18
00:01:10,770 --> 00:01:12,600
 you couldn't go anywhere monks

19
00:01:12,600 --> 00:01:15,480
 will often be in need of a new set of robes.

20
00:01:15,480 --> 00:01:18,500
 So the tradition is that at the end the monks would gather

21
00:01:18,500 --> 00:01:20,320
 together all the cloth that they

22
00:01:20,320 --> 00:01:25,350
 had made and present at least one set of robes to some monk

23
00:01:25,350 --> 00:01:27,160
 who needed them.

24
00:01:27,160 --> 00:01:29,800
 Probably his robes would be worn out or maybe it was just a

25
00:01:29,800 --> 00:01:31,360
 monk that they respect if there

26
00:01:31,360 --> 00:01:32,800
 was no one who really needed them.

27
00:01:32,800 --> 00:01:36,030
 But it would be a tradition called the Katina where they

28
00:01:36,030 --> 00:01:38,360
 would get together and make a symbolic

29
00:01:38,360 --> 00:01:39,720
 offering of robes.

30
00:01:39,720 --> 00:01:44,020
 Now it's evolved into, or even in the beginning it could be

31
00:01:44,020 --> 00:01:46,600
 expanded into offering many sets

32
00:01:46,600 --> 00:01:47,600
 of robes.

33
00:01:47,600 --> 00:01:51,170
 So I won't be obviously the only person offering robes on

34
00:01:51,170 --> 00:01:53,960
 that day but what I'd like to do

35
00:01:53,960 --> 00:01:58,090
 is open this up to people who might be interested in also

36
00:01:58,090 --> 00:01:59,600
 offering robes.

37
00:01:59,600 --> 00:02:02,300
 Now this doesn't mean that I need you to send me sets of

38
00:02:02,300 --> 00:02:02,880
 robes.

39
00:02:02,880 --> 00:02:04,200
 I think that's impractical.

40
00:02:04,200 --> 00:02:06,800
 I won't even be bringing robes from Canada myself.

41
00:02:06,800 --> 00:02:10,190
 We'll be getting them when I get to Thailand and offering

42
00:02:10,190 --> 00:02:10,760
 them.

43
00:02:10,760 --> 00:02:14,450
 But in order to open this up to people who are interested,

44
00:02:14,450 --> 00:02:16,400
 who want to get excited about

45
00:02:16,400 --> 00:02:20,140
 doing good deeds, who want to cultivate wholesomeness in

46
00:02:20,140 --> 00:02:22,920
 this way, who have the resources by which

47
00:02:22,920 --> 00:02:27,490
 they could offer a set of robes and would like to do

48
00:02:27,490 --> 00:02:30,800
 something to cement their own mind

49
00:02:30,800 --> 00:02:34,480
 in the practice and cultivation of goodness.

50
00:02:34,480 --> 00:02:39,010
 We have thereby, the organization that I run or am involved

51
00:02:39,010 --> 00:02:41,180
 in and my board of directors

52
00:02:41,180 --> 00:02:47,670
 and I, we have put together an Indiegogo project which you

53
00:02:47,670 --> 00:02:49,080
 can go to.

54
00:02:49,080 --> 00:02:54,480
 It's in the link in the description to this video.

55
00:02:54,480 --> 00:02:59,320
 It basically allows you to get involved and you can go

56
00:02:59,320 --> 00:03:02,320
 there and get details if you want

57
00:03:02,320 --> 00:03:06,400
 to be involved with donating a set of robes.

58
00:03:06,400 --> 00:03:12,550
 Now our goal is potentially to offer 84 sets of Buddhist

59
00:03:12,550 --> 00:03:15,720
 monk's robes to my teacher.

60
00:03:15,720 --> 00:03:17,800
 Why 84 you ask?

61
00:03:17,800 --> 00:03:20,920
 84 itself is a symbolic number.

62
00:03:20,920 --> 00:03:24,700
 84,000 is the number of teachings that are said to be

63
00:03:24,700 --> 00:03:26,720
 contained in the scriptures that

64
00:03:26,720 --> 00:03:29,880
 we have recorded of the Buddha.

65
00:03:29,880 --> 00:03:33,080
 So it's symbolic of also paying respect to the Dhamma.

66
00:03:33,080 --> 00:03:36,760
 But the real question is what could one monk possibly do

67
00:03:36,760 --> 00:03:38,720
 with 84 sets of robes?

68
00:03:38,720 --> 00:03:44,150
 Well the monastery where my teacher lives and teaches has

69
00:03:44,150 --> 00:03:47,160
 evolved from a small community

70
00:03:47,160 --> 00:03:52,810
 temple to be a huge multi-acre meditation center with

71
00:03:52,810 --> 00:03:56,800
 hundreds of meditators and actually

72
00:03:56,800 --> 00:04:00,340
 over a hundred monks who will stay during the rains.

73
00:04:00,340 --> 00:04:03,420
 So by the end of the rains there will be at least a hundred

74
00:04:03,420 --> 00:04:04,920
 monks staying there.

75
00:04:04,920 --> 00:04:08,080
 We actually won't be able to give a set of robes to all of

76
00:04:08,080 --> 00:04:09,720
 them unless we get over our

77
00:04:09,720 --> 00:04:13,800
 limit and decide to offer more than 84.

78
00:04:13,800 --> 00:04:16,610
 Maybe we could switch to 108 which is also a symbolic

79
00:04:16,610 --> 00:04:17,200
 number.

80
00:04:17,200 --> 00:04:20,030
 But for now we're setting the limit at 84 which is the

81
00:04:20,030 --> 00:04:21,880
 number that I've actually offered

82
00:04:21,880 --> 00:04:22,880
 before.

83
00:04:22,880 --> 00:04:24,800
 I've done this before and it worked really well.

84
00:04:24,800 --> 00:04:27,110
 The monks were ecstatic, they were able to get quality

85
00:04:27,110 --> 00:04:28,400
 monks robes that many of them

86
00:04:28,400 --> 00:04:30,160
 had never seen.

87
00:04:30,160 --> 00:04:35,810
 Many of the young monks who don't have any followers or so

88
00:04:35,810 --> 00:04:38,400
 on, they're never able to

89
00:04:38,400 --> 00:04:42,780
 get this kind of support and so they've never seen such

90
00:04:42,780 --> 00:04:44,840
 quality monks robes.

91
00:04:44,840 --> 00:04:48,200
 So it's something that is highly appreciated.

92
00:04:48,200 --> 00:04:51,230
 It's also a great feeling to be able to come in there with

93
00:04:51,230 --> 00:04:52,720
 such a huge number of robes

94
00:04:52,720 --> 00:04:57,420
 and to really show the extent of our appreciation for our

95
00:04:57,420 --> 00:04:58,440
 teacher.

96
00:04:58,440 --> 00:05:03,920
 But more than that it's a symbolic gesture on our part to

97
00:05:03,920 --> 00:05:06,720
 support the monastic order.

98
00:05:06,720 --> 00:05:10,240
 If you're someone who is keen on the idea of becoming a

99
00:05:10,240 --> 00:05:12,240
 monk one day or of supporting

100
00:05:12,240 --> 00:05:15,590
 monks in their dissemination of the Buddha's teaching and

101
00:05:15,590 --> 00:05:17,760
 their dedication to disseminating

102
00:05:17,760 --> 00:05:21,770
 and practicing and upholding the teachings of the Buddha,

103
00:05:21,770 --> 00:05:23,920
 especially in this case because

104
00:05:23,920 --> 00:05:25,400
 this is a monastery.

105
00:05:25,400 --> 00:05:29,190
 Many of these monks have ordained specifically at this

106
00:05:29,190 --> 00:05:32,080
 monastery because they were interested

107
00:05:32,080 --> 00:05:35,440
 in the meditation practice.

108
00:05:35,440 --> 00:05:40,140
 This is a direct way to support the monastic tradition in

109
00:05:40,140 --> 00:05:41,600
 this lineage.

110
00:05:41,600 --> 00:05:46,640
 So symbolically it means a lot to make this kind of a

111
00:05:46,640 --> 00:05:48,000
 donation.

112
00:05:48,000 --> 00:05:52,140
 For me at least it means something great to be able to

113
00:05:52,140 --> 00:05:54,960
 offer one of the basic requisites

114
00:05:54,960 --> 00:05:58,600
 for a monk which is this robes that I'm wearing.

115
00:05:58,600 --> 00:06:01,040
 Each set of robes has three robes in it.

116
00:06:01,040 --> 00:06:03,950
 This is the upper robe that you see and underneath there's

117
00:06:03,950 --> 00:06:05,920
 the lower robe and they're big pieces

118
00:06:05,920 --> 00:06:09,400
 of cloth and they're not easy to sew.

119
00:06:09,400 --> 00:06:14,440
 So it really is a great thing to be able to give this.

120
00:06:14,440 --> 00:06:18,860
 Most people don't ever have the opportunity and this

121
00:06:18,860 --> 00:06:22,080
 special occasion is really the right

122
00:06:22,080 --> 00:06:25,820
 time to offer because as I said it's when it's needed and

123
00:06:25,820 --> 00:06:28,080
 it's when it's expected.

124
00:06:28,080 --> 00:06:30,440
 It's when everyone comes together to do this.

125
00:06:30,440 --> 00:06:33,480
 So it will be I think a great opportunity if we can

126
00:06:33,480 --> 00:06:34,880
 actually do this.

127
00:06:34,880 --> 00:06:39,340
 If we don't get 84 people interested or enough support

128
00:06:39,340 --> 00:06:42,440
 together gather together 84 sets of

129
00:06:42,440 --> 00:06:44,400
 robes then we'll just offer whatever we have.

130
00:06:44,400 --> 00:06:47,640
 If we get more as I said maybe we'll increase it.

131
00:06:47,640 --> 00:06:51,630
 But just wanted to put it out there in case there's anyone

132
00:06:51,630 --> 00:06:53,600
 and I'm assuming there is who

133
00:06:53,600 --> 00:06:57,450
 wants to get involved with this great endeavor to help us

134
00:06:57,450 --> 00:06:59,960
 to support the Buddhist teaching,

135
00:06:59,960 --> 00:07:03,960
 to support the Buddhist religion, to support our own

136
00:07:03,960 --> 00:07:06,800
 practice of Buddhism by cultivating

137
00:07:06,800 --> 00:07:13,870
 generosity and respect and renunciation and putting our

138
00:07:13,870 --> 00:07:18,360
 vote, our resources and our effort

139
00:07:18,360 --> 00:07:20,360
 behind something that we truly believe in.

140
00:07:20,360 --> 00:07:24,860
 So I'd like to encourage you in this but only, I'm not

141
00:07:24,860 --> 00:07:27,680
 asking for this, this is not something

142
00:07:27,680 --> 00:07:31,000
 that you need to feel obliged to do but something that I

143
00:07:31,000 --> 00:07:33,280
 would encourage because I myself believe

144
00:07:33,280 --> 00:07:34,440
 in it.

145
00:07:34,440 --> 00:07:36,780
 And so if it's something that you'd like to do and would

146
00:07:36,780 --> 00:07:38,040
 come from your heart and you

147
00:07:38,040 --> 00:07:43,340
 really feel would make you feel good to do then absolutely

148
00:07:43,340 --> 00:07:46,320
 this is a great way to cultivate

149
00:07:46,320 --> 00:07:51,480
 generosity and respect and so on and all those things.

150
00:07:51,480 --> 00:07:56,880
 So let us know if you're interested, visit the IndieGoGo

151
00:07:56,880 --> 00:07:59,920
 project below and you can leave

152
00:07:59,920 --> 00:08:05,080
 comments if you have anything to say or get in touch with

153
00:08:05,080 --> 00:08:08,320
 our organization via our website

154
00:08:08,320 --> 00:08:12,200
 which I'll also put in the link below.

155
00:08:12,200 --> 00:08:17,070
 But yeah, so thank you for your consideration and most

156
00:08:17,070 --> 00:08:20,320
 importantly thanks for the support

157
00:08:20,320 --> 00:08:25,420
 and moral support that people have been giving just by

158
00:08:25,420 --> 00:08:29,040
 following and liking and commenting

159
00:08:29,040 --> 00:08:35,550
 on and practicing the teachings that are presented in these

160
00:08:35,550 --> 00:08:36,480
 videos.

161
00:08:36,480 --> 00:08:41,650
 I get so many awesome comments from people about how these

162
00:08:41,650 --> 00:08:44,040
 teachings help and that's

163
00:08:44,040 --> 00:08:45,400
 really all that we're trying to do.

164
00:08:45,400 --> 00:08:48,780
 I'm not trying to put myself out as some kind of big

165
00:08:48,780 --> 00:08:51,440
 teacher, obviously I don't have anything

166
00:08:51,440 --> 00:08:55,720
 special to offer personally but what I have in Buddha's

167
00:08:55,720 --> 00:08:58,240
 teaching is something special

168
00:08:58,240 --> 00:09:01,930
 and I think a lot of these teachings are not well

169
00:09:01,930 --> 00:09:05,360
 represented or well understood by a great

170
00:09:05,360 --> 00:09:09,150
 portion of the world and so it seems to be a good thing to

171
00:09:09,150 --> 00:09:11,920
 spread those so that's what

172
00:09:11,920 --> 00:09:12,920
 that's all about.

173
00:09:12,920 --> 00:09:14,360
 But a little bit off topic anyway.

174
00:09:14,360 --> 00:09:19,040
 A little bit off topic is offering 84 sets of robes to my

175
00:09:19,040 --> 00:09:21,320
 teacher who will then offer

176
00:09:21,320 --> 00:09:23,320
 them to the monks.

177
00:09:23,320 --> 00:09:25,350
 We'll share them with the monks, staying with him during

178
00:09:25,350 --> 00:09:25,880
 the rains.

179
00:09:25,880 --> 00:09:28,160
 We'll have it all on video when I go there.

180
00:09:28,160 --> 00:09:33,470
 I hope someone will have someone take a video and you can

181
00:09:33,470 --> 00:09:36,000
 see the goodness of it.

182
00:09:36,000 --> 00:09:38,520
 And the other thing is for some people this kind of thing

183
00:09:38,520 --> 00:09:39,940
 is important just to mention

184
00:09:39,940 --> 00:09:43,600
 that the robe sets, I don't have one here.

185
00:09:43,600 --> 00:09:44,600
 There's one over there.

186
00:09:44,600 --> 00:09:49,070
 But you can look on the internet, Google Buddhist monks

187
00:09:49,070 --> 00:09:52,320
 robes and you'll see they're in a set

188
00:09:52,320 --> 00:09:55,340
 and we're going to put, we can put, if you'd rather not, we

189
00:09:55,340 --> 00:09:57,560
 don't have to, but we'll put

190
00:09:57,560 --> 00:10:02,590
 the names or any name you choose as long as it's not any

191
00:10:02,590 --> 00:10:06,200
 reasonable name, a serious name

192
00:10:06,200 --> 00:10:08,600
 on the monks robes when we offer them.

193
00:10:08,600 --> 00:10:11,470
 So if you want to dedicate, meaning is if you want to

194
00:10:11,470 --> 00:10:13,360
 dedicate an offering to someone,

195
00:10:13,360 --> 00:10:14,480
 you can do that.

196
00:10:14,480 --> 00:10:19,000
 So if it's someone who's passed away or someone who is sick

197
00:10:19,000 --> 00:10:21,440
 or someone who is, who you think

198
00:10:21,440 --> 00:10:26,410
 would benefit from dedicating this offering to, for some

199
00:10:26,410 --> 00:10:28,920
 people that's a neat idea and

200
00:10:28,920 --> 00:10:30,560
 might be appropriate.

201
00:10:30,560 --> 00:10:32,360
 So you're welcome to do that.

202
00:10:32,360 --> 00:10:38,460
 You're welcome to offer many sets or even part of a set.

203
00:10:38,460 --> 00:10:42,580
 You can gather together, it's all the information's on the

204
00:10:42,580 --> 00:10:43,880
 Indigogo site.

205
00:10:43,880 --> 00:10:47,570
 And so this is just me letting you know about it if it's of

206
00:10:47,570 --> 00:10:48,960
 interest to you.

207
00:10:48,960 --> 00:10:54,720
 So thanks for all those people who do get involved.

208
00:10:54,720 --> 00:10:58,680
 May this be a support for all of our practice and for our

209
00:10:58,680 --> 00:11:01,340
 cultivation of goodness, for our

210
00:11:01,340 --> 00:11:05,810
 dedication to purity of mind, wholesomeness, goodness,

211
00:11:05,810 --> 00:11:08,600
 Buddhism, the Buddhist religion,

212
00:11:08,600 --> 00:11:14,140
 the Buddhist monastic order, my teacher, our teacher, and

213
00:11:14,140 --> 00:11:17,680
 just the cultivation of the spiritual

214
00:11:17,680 --> 00:11:18,680
 life.

215
00:11:18,680 --> 00:11:22,400
 May we all obtain true peace, happiness and freedom from

216
00:11:22,400 --> 00:11:24,760
 suffering and may this be a support

217
00:11:24,760 --> 00:11:28,160
 for our progress towards that goal.

218
00:11:28,160 --> 00:11:29,840
 Thank you and wishing you all.

