1
00:00:00,000 --> 00:00:05,000
 Good evening everyone.

2
00:00:05,000 --> 00:00:10,000
 Broadcasting Live, October 26, 2015.

3
00:00:10,000 --> 00:00:14,000
 Tonight we're not going to do the Dhammapada.

4
00:00:14,000 --> 00:00:17,400
 I'm feeling a little bit under the weather. I don't think I

5
00:00:17,400 --> 00:00:21,830
'm sick, but I didn't get around to looking at the Dhammap

6
00:00:21,830 --> 00:00:22,000
ada.

7
00:00:22,000 --> 00:00:25,000
 Probably going to call it an early night.

8
00:00:25,000 --> 00:00:29,790
 And there's a couple of people visiting here and downstairs

9
00:00:29,790 --> 00:00:30,000
.

10
00:00:30,000 --> 00:00:34,230
 Nothing tonight and tomorrow night as well. Tomorrow is the

11
00:00:34,230 --> 00:00:35,000
 full moon.

12
00:00:35,000 --> 00:00:39,000
 So officially end of the rains.

13
00:00:39,000 --> 00:00:44,000
 We enter into the cold season officially.

14
00:00:44,000 --> 00:00:48,000
 And tomorrow evening I will be at Stony Creek.

15
00:00:48,000 --> 00:00:53,850
 I'm doing an official ceremony with the other monks there

16
00:00:53,850 --> 00:00:56,000
 to leave the rains.

17
00:00:56,000 --> 00:00:58,990
 It's not even a ceremony to leave the rains, it's just we

18
00:00:58,990 --> 00:01:02,000
 ask forgiveness from each other basically.

19
00:01:02,000 --> 00:01:08,000
 We give permission to the others to criticize us.

20
00:01:08,000 --> 00:01:11,000
 It's kind of a nice ceremony.

21
00:01:11,000 --> 00:01:14,000
 Maybe I'll take them up on it and start criticizing them.

22
00:01:14,000 --> 00:01:15,000
 It's something you never do.

23
00:01:15,000 --> 00:01:18,690
 No one ever actually goes ahead and criticizes, but I think

24
00:01:18,690 --> 00:01:23,000
 I'll maybe joke about it or something with them.

25
00:01:23,000 --> 00:01:26,000
 Start telling them all their faults.

26
00:01:26,000 --> 00:01:29,000
 We're talking all this talk about criticism.

27
00:01:29,000 --> 00:01:32,160
 Tomorrow's the one day of the year where it's open season

28
00:01:32,160 --> 00:01:33,000
 on everybody.

29
00:01:33,000 --> 00:01:35,000
 You're supposed to be allowed to criticize each other

30
00:01:35,000 --> 00:01:39,000
 because you invite each other to criticize.

31
00:01:39,000 --> 00:01:45,890
 But no, they're good guys. There's nothing major to

32
00:01:45,890 --> 00:01:47,000
 criticize.

33
00:01:47,000 --> 00:01:49,780
 So we'll take some questions. I don't think there's that

34
00:01:49,780 --> 00:01:52,000
 many. We've got a few here.

35
00:01:52,000 --> 00:01:55,000
 And then...

36
00:01:55,000 --> 00:01:58,700
 I have a question on the, you know, where you ask

37
00:01:58,700 --> 00:02:00,000
 forgiveness.

38
00:02:00,000 --> 00:02:03,870
 Is it actually, want your forgiveness or is it more just

39
00:02:03,870 --> 00:02:12,000
 for the month to the criticism?

40
00:02:12,000 --> 00:02:15,000
 We've got a lot of reasons.

41
00:02:15,000 --> 00:02:20,000
 Yeah, you're using open speakers as well.

42
00:02:20,000 --> 00:02:22,210
 That's right. I didn't really catch what you said, but I

43
00:02:22,210 --> 00:02:25,730
 need to have it on because I need to record your what you

44
00:02:25,730 --> 00:02:27,000
're asking.

45
00:02:27,000 --> 00:02:30,000
 Let me turn my depth.

46
00:02:30,000 --> 00:02:34,610
 Yeah, I'm just wondering what the intention is when you're

47
00:02:34,610 --> 00:02:39,000
 asking or when you're opening yourself up to criticism.

48
00:02:39,000 --> 00:02:44,000
 Is it, you know, what is the intention of that?

49
00:02:44,000 --> 00:02:47,900
 Well, to end the reigns on a good note so that there's no

50
00:02:47,900 --> 00:02:55,000
 hard feelings and no pent up presentment.

51
00:02:55,000 --> 00:03:00,000
 It's to be accountable to each other and to clear the air.

52
00:03:00,000 --> 00:03:04,000
 Because you've spent three months together. Theoretically,

53
00:03:04,000 --> 00:03:08,000
 I actually haven't. I've spent a lot of it away, but...

54
00:03:08,000 --> 00:03:12,030
 Having spent three months together, usually a lot of stuff

55
00:03:12,030 --> 00:03:15,000
 can happen. You know, you can get...

56
00:03:15,000 --> 00:03:20,670
 You can build up animosity. So in the end, you invite each

57
00:03:20,670 --> 00:03:23,000
 other to clear the air.

58
00:03:23,000 --> 00:03:25,800
 You accept other people, the problems other people have had

59
00:03:25,800 --> 00:03:32,000
. You listen and you accept and let it go.

60
00:03:32,000 --> 00:03:34,000
 Seems like a really good thing to do.

61
00:03:34,000 --> 00:03:40,000
 Yeah.

62
00:03:40,000 --> 00:03:56,250
 We do have some questions. I only have you of an Apple iPod

63
00:03:56,250 --> 00:03:56,780
. I do have Chrome. Would I be able to meet with this? Would

64
00:03:56,780 --> 00:03:57,000
 I also need you?

65
00:03:57,000 --> 00:04:02,340
 You can, I think you can get hangouts for iOS. So you don't

66
00:04:02,340 --> 00:04:06,000
 use Chrome. You'll use Hangouts.

67
00:04:06,000 --> 00:04:15,780
 Google Hangouts. So as long as they have that for Chrome,

68
00:04:15,780 --> 00:04:20,000
 which I think there is.

69
00:04:20,000 --> 00:04:27,550
 Oh, and Nikki. Nikki posted, but I did talk to her, so that

70
00:04:27,550 --> 00:04:30,000
 worked out fine.

71
00:04:30,000 --> 00:04:36,280
 Hello, Bunthe. Sounds in my head, which are caused by

72
00:04:36,280 --> 00:04:38,400
 concentration, get sometimes very strong and continues for

73
00:04:38,400 --> 00:04:50,000
 a long time. Is it normal? Thanks.

74
00:04:50,000 --> 00:04:53,250
 It's not normal, but we're not concerned so much about

75
00:04:53,250 --> 00:04:57,220
 normal. It's it's real and that's all that's important. So

76
00:04:57,220 --> 00:05:00,000
 it's a sound and you should say hearing, hearing.

77
00:05:00,000 --> 00:05:04,940
 There's not really a normal. What would that mean? Everyone

78
00:05:04,940 --> 00:05:10,000
 gets it. Everyone has different conditions.

79
00:05:10,000 --> 00:05:13,480
 So everyone will ask that sort of question. Is this normal?

80
00:05:13,480 --> 00:05:16,760
 There really is no normal. There's you and there's what you

81
00:05:16,760 --> 00:05:20,240
're experiencing. So it doesn't matter whether it's normal

82
00:05:20,240 --> 00:05:21,000
 or normal.

83
00:05:21,000 --> 00:05:26,980
 Abnormal is just a judgment call. When you hear, you should

84
00:05:26,980 --> 00:05:31,000
 say hearing, hearing. Just let it go.

85
00:05:31,000 --> 00:05:33,580
 You should acknowledge it for some time. If it doesn't go

86
00:05:33,580 --> 00:05:36,940
 away, then just ignore it. And then if it drags your

87
00:05:36,940 --> 00:05:40,890
 attention back later, you can always go back and

88
00:05:40,890 --> 00:05:43,000
 acknowledge it again.

89
00:05:43,000 --> 00:05:48,850
 If you dislike it or you like it or whatever, worried about

90
00:05:48,850 --> 00:05:55,000
 it, afraid of it, should acknowledge all of that as well.

91
00:05:55,000 --> 00:06:05,000
 Yeah.

92
00:06:05,000 --> 00:06:12,000
 Pretty obvious, isn't it? I guess is there more to that?

93
00:06:12,000 --> 00:06:15,850
 Because a monk, a sense that monks, their state of mind is

94
00:06:15,850 --> 00:06:19,900
 more calm, so they're less liable to be attacked by animals

95
00:06:19,900 --> 00:06:22,000
, but it certainly happens.

96
00:06:22,000 --> 00:06:32,000
 Crushed by an elephant, gored by a, gored by a wild pig,

97
00:06:32,000 --> 00:06:39,000
 eaten by a lion or a tiger, killed by mosquitoes.

98
00:06:39,000 --> 00:06:44,000
 Yeah. A lot of death by mosquitoes.

99
00:06:44,000 --> 00:06:49,000
 Does that happen? You should make forest monks?

100
00:06:49,000 --> 00:06:52,850
 There aren't that many forest monks, but there's also not

101
00:06:52,850 --> 00:06:55,000
 that many wild animals anymore.

102
00:06:55,000 --> 00:06:59,500
 So depends which country Sri Lanka still has some, but

103
00:06:59,500 --> 00:07:02,920
 snakes would probably, number one would probably be

104
00:07:02,920 --> 00:07:04,000
 mosquitoes.

105
00:07:04,000 --> 00:07:06,880
 I don't know about death through mosquitoes, but yeah,

106
00:07:06,880 --> 00:07:11,290
 there's probably death by mosquitoes, death by snake bite,

107
00:07:11,290 --> 00:07:16,000
 and death by disease, probably the worst.

108
00:07:16,000 --> 00:07:19,000
 There's a lot of bacteria in the rainforests.

109
00:07:19,000 --> 00:07:25,710
 Maybe not the most, but it is up there like an infection

110
00:07:25,710 --> 00:07:33,000
 and fungus, just really awful stuff in the rainforest.

111
00:07:33,000 --> 00:07:36,870
 In this area we can tick-serve a problem of Lyme disease,

112
00:07:36,870 --> 00:07:41,000
 which is originally from the area of Lyme, Connecticut.

113
00:07:41,000 --> 00:07:44,000
 I see.

114
00:07:44,000 --> 00:07:47,990
 I teach with the group that's here in Second Life, and I've

115
00:07:47,990 --> 00:07:51,380
 been spreading the word about our management group and

116
00:07:51,380 --> 00:07:53,000
 study group and so on.

117
00:07:53,000 --> 00:07:55,700
 Since they go to the Buddhist group, it would be a good

118
00:07:55,700 --> 00:07:58,000
 thing to let them know, is this appropriate?

119
00:07:58,000 --> 00:08:01,290
 The members are Zen Buddhism, which we might adhere to a

120
00:08:01,290 --> 00:08:03,000
 certain kind of Buddhism.

121
00:08:03,000 --> 00:08:06,170
 Also, would it be able to stream the live stream really

122
00:08:06,170 --> 00:08:09,430
 public so they can listen to them in the area of the Second

123
00:08:09,430 --> 00:08:10,000
 Life?

124
00:08:10,000 --> 00:08:13,000
 Thank you, Madhguru.

125
00:08:15,000 --> 00:08:19,050
 I don't really think we want people from other traditions

126
00:08:19,050 --> 00:08:22,000
 coming to meditate here.

127
00:08:22,000 --> 00:08:30,000
 It really should be our tradition. That's my feeling.

128
00:08:30,000 --> 00:08:33,000
 I think I'd rather stick to our tradition.

129
00:08:33,000 --> 00:08:37,500
 But if they want to listen to the live stream, that's

130
00:08:37,500 --> 00:08:40,000
 absolutely welcome too.

131
00:08:40,000 --> 00:08:44,210
 We haven't required it, but I think we'd encourage that if

132
00:08:44,210 --> 00:08:48,000
 you're coming here, you're practicing our tradition.

133
00:08:48,000 --> 00:08:56,000
 I don't see a reason to go outside of that.

134
00:08:56,000 --> 00:09:02,640
 Because it will just get weird otherwise, especially in the

135
00:09:02,640 --> 00:09:04,000
 chat box.

136
00:09:04,000 --> 00:09:10,000
 You'd have to be like that when the students were watching.

137
00:09:10,000 --> 00:09:14,000
 The Korean students.

138
00:09:14,000 --> 00:09:18,000
 Weird stuff happened.

139
00:09:18,000 --> 00:09:22,000
 Can you talk about today's book?

140
00:09:22,000 --> 00:09:32,000
 Sorry, I gotta go. I just came on to say hello tonight.

141
00:09:32,000 --> 00:09:34,000
 Thank you, Bhankar.

142
00:09:34,000 --> 00:09:38,330
 Sorry, guys. Thanks for showing up. Thanks for meditating

143
00:09:38,330 --> 00:09:39,000
 with us.

144
00:09:39,000 --> 00:09:41,000
 Keep up the good work.

145
00:09:41,000 --> 00:09:46,010
 Darwin, I'll try tomorrow. Maybe tomorrow you have a

146
00:09:46,010 --> 00:09:47,000
 session.

147
00:09:47,000 --> 00:09:50,000
 What time is that?

148
00:09:50,000 --> 00:09:56,000
 Everyone go to Darwin's session in Second Life.

149
00:09:56,000 --> 00:10:09,770
 If you can post it in the chat there, let us know. I'll try

150
00:10:09,770 --> 00:10:11,000
 to make it.

151
00:10:11,000 --> 00:10:14,000
 Okay, anyway, good night everyone.

152
00:10:14,000 --> 00:10:17,000
 Good night, Bhankar.

153
00:10:18,000 --> 00:10:20,000
 Thank you.

