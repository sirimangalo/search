 Good evening everyone. Welcome to our hangout, our live
 broadcast.
 Today we are looking at suta number 33 in the book of th
rees in Gutturani Kaya.
 First the Buddha warns, sorry Buddha, sorry Buddha comes to
 see the Buddha and his respect and the Buddha warns him.
 Sankite na pe koahang saarikutta damang de seya.
 I may teach the Dhamma in brief.
 Sankite na pe koahang saarikutta damang de seya. I may
 teach the Dhamma in detail.
 Sankite na pe koahang saarikutta damang de seya.
 I may teach the Dhamma both in brief and in detail, but añ
ataru jadulambha.
 It's hard to find those who will understand. Those who will
 understand are difficult to find.
 It's hard to find those who understand the Dhamma, should
 not expect everyone to understand it.
 Which to an outside ear sounds a bit like a cop-out that
 when people don't understand or don't appreciate Buddhism,
 when people don't appreciate it we say, "Oh, they just don
't understand."
 They're just too undeveloped. Of course that's what's an
 easy cop-out.
 But of course it has to be true to some extent.
 If there were a teaching that as Buddhism claims to be,
 were the highest, most pure form of wisdom,
 for certain many people would not understand it.
 So I think it's a fair thing to say that when you're
 talking about the truth,
 most people won't understand it. Most people aren't even
 interested in trying to understand it.
 Many people are on the wrong path, purposefully and
 knowingly.
 Well, not knowingly, but without any disregard for whether
 it's the right or wrong path,
 or without real care or concern for the path that they're
 on.
 Following it for various reasons, sometimes simply because
 it's what they were taught,
 or what their culture pushes on them.
 [silence]
 Often just because it's what pleases them, because of the
 desires and so on.
 But ultimately it comes down to three things.
 Remember we're in the Book of Three, so we have three
 really the best overarching, simple way of understanding
 the problems that we have.
 These three terms, this is a really good classification, of
 course.
 There's many different classifications of the problem, but
 a really good one is these three.
 Diti mana tanha, or diti tanha mana in this form.
 Views, craving and conceit. These are the three categories
 of defilements,
 three main defilements that are problems for us that cause
 us stress and suffering.
 Now they're described here as ahangkara, mamangkara and man
a.
 In Thai they say, ahangkara, mamangkara.
 The first time I heard this was in Thai. I think Ajahn Tong
 was saying it.
 I heard him say, ahangkara, mamangkara.
 And it's apparently, I think it's something that Thai
 people say.
 They put them together like this, but they've taken it
 directly from the Pali.
 Ahangkara, mamangkara. Ahangkan, mamangkan means the ideas
 clinging to things as me as mine.
 That's exactly how you translate it. Making things out to
 be me or mine.
 Ahangkara, kara means making. Ahang means I. So I making.
 Mamang means mine. Maybe it means mamma.
 I don't think it's mamang, but it somehow becomes mamang.
 Mamamang.
 Maybe it can be. I don't think so.
 Mamangkara means my making.
 Mana means conceit. So ahangkara, making something out to
 be I,
 or even not just I, but making something out to be an
 entity, a self.
 That's called ahangkara. And it comes from view. That's a
 description of views.
 So when you say this is a thing, this is a self, this is I,
 that's views.
 This is the self, that is the self.
 It's a view. And this goes with anything. This is God, that
 is God.
 Those are any of these sort of religious claims.
 Something happens and you believe it's God. You get this a
 lot.
 This is how Christians explain their faith.
 Recently I had it explained to me by Christians.
 I say Christians because they're the most common around
 these parts.
 But they're always claiming that I know God and I felt God
 in my life
 and I've experienced God. That's all just views.
 You haven't experienced anything of the sort.
 You've experienced a seeing, hearing, smelling, tasting,
 feeling, thinking.
 Even if a being came and stood before you in all their
 glory
 and called themselves God, it would still not be God.
 It would still just be seeing.
 As soon as you make it more than that, you're ahang kara.
 It's not exactly ahang kara, I suppose, because it's not I,
 but it's along the same lines.
 It's just this problematic because you make anything out to
 be an entity
 and suddenly you've got something you can cling to,
 something that's permanent, stable, satisfying, which doesn
't actually exist.
 It is dependent on things that are impermanent, unsatisf
ying and uncontrolled.
 Mamangkara is from tanha, craving.
 Mamangkara is saying this is mine, possessions.
 And it's not just mine, it's wishing for things to be mine,
 right?
 Wishing to obtain things.
 So it's all under the heading of mamangkara.
 Whether you look at things, conceive of things to be yours
 or whether you conceive of things as worth being yours,
 as appealing, where the idea of ownership is appealing.
 A liking of things of any sort is under mamangkara.
 It is a mind.
 Of course, the problem here is not just with the fact that
 it's
 not even just with the fact that it's impermanent, unsatisf
ying and uncontrollable,
 but that tanha is addictive, right?
 You get something, even you can get what you want.
 It cultivates further addiction and doesn't actually
 satisfy you.
 I mean, the real problem is impermanent, unsatisfying and
 uncontrollable,
 but the danger is that you become addicted to these things.
 You're addicted to things that can't satisfy you,
 and so you need more and need more,
 and you actually become as a person less and less happy.
 You think this is making me happy, right?
 Because there's pleasure involved.
 But over time, you can see that the more addicted you are,
 the less happier you are as a person.
 That's mamangkara.
 And the third one is mana.
 Mana means conceit.
 Conceit is different from views because conceit doesn't
 actually say
 this is self, that is self, doesn't actually claim the
 existence of something.
 It doesn't put in the form of a claim.
 Well, not per se.
 It's an estimation when you esteem yourself to be less than
 someone else,
 or you esteem yourself to be better than someone else,
 or you esteem yourself to be equal to someone else.
 There are nine types of conceit.
 You can conceive of yourself as being greater than someone
 else
 and actually be greater than them.
 Still conceit.
 You can conceive of someone as being greater than them.
 Conceive of yourself as being greater than someone,
 and you're actually equal to them.
 That's conceit.
 Or you can conceive of yourself as being greater than
 someone,
 and you're lesser than them.
 And so on with equal or lesser as well.
 Three times three times.
 Three plus three plus three.
 Three cubed.
 So nine types of conceit.
 Now, asotapan, I can only give rise to three types of conce
it,
 as far as I remember.
 They give rise to the conceit of being greater than someone
 when they're actually greater than them.
 They can have conceit of conceiving themselves as equal to
 someone
 when they're actually equal to someone.
 And they can conceive of themselves as being less than
 someone
 when they're actually lesser than someone.
 If I remember correctly, asotapan, I can only give rise to
 those three.
 They can't have the misrepresentation of themselves.
 It's of the clarity of mind.
 But it seems like it would more be in regards to equality.
 So if they observed equality in someone and thought--
 and that quality actually made them greater than a person,
 then they couldn't misconceive that that actually made them
 worse,
 I think is the point.
 It's not like you could look at someone and say,
 "Oh, I'm greater than them," but you don't realize.
 Asotapan, I could probably still not realize
 that actually that person was greater than them.
 But they aren't able to misunderstand qualities
 in the same way as an ordinary person.
 Anyway, conceit-- even that is conceit.
 Even when you're greater than someone, if you believe you
're greater than them,
 that feeling, that estimation is conceit.
 It happens.
 It's a part of our makeup.
 It's what we have to work at.
 It's not something you can just fix and say,
 "No, I won't be conceited," which is what a lot of people
 try to do.
 They try to be humble artificially.
 It doesn't really fix the problem.
 It's a temporary solution, sort of faking it.
 And you can fake it till you make it,
 but making it is much different when you actually come to
 see
 that there's no I, there's no me, there's no mine,
 nothing less cleanings.
 Slowly give up your conceit, an arahant,
 as one who gives up conceit entirely.
 So this-- Nisuta isn't actually about those three.
 That's not the numeration.
 The numeration is to not have ditimana, dititana and mana
 in regard to the savinjana kai,
 this body and mind, this body that has a mind,
 this body that has consciousness.
 One should not have these things in regards to one's own
 being.
 This body and this mind are not I, are not mine, are not me
.
 Nor should one have it in regards to other things.
 [speaking in foreign language]
 All in regards to all signs that are external,
 which means anything you perceive,
 anything you conceive, any nimitta.
 You see, the nimitta is that which gives rise to views
 and opinions and beliefs and attachments.
 So when you see a person, the nimitta is going to arise
 that tells you that's a person, sort of the sannya,
 the memory that that's a person.
 Now, if you're not mindful, that memory is going to give
 rise
 to all sorts of reactions.
 Oh, that's a person I like, that's a person I don't like.
 You see a plate of food in front of you.
 Oh, that's food I like, it's food I don't like, it's my
 food.
 That kind of thing.
 It is food, which is a view, you know,
 when you think of it as a thing, this is an apple,
 this apple exists, you have that view that things exist,
 which we mostly only have towards beings, people,
 the belief that people exist, the belief that we exist
 as an entity, these beliefs.
 It's not so much about what is true and not true,
 we're not really so concerned, although seeing what's true
 is what allows you to give these up, it just means the
 experience.
 We're only talking about a view is an experience where you
 say to yourself,
 that's a person, that, or this is the self, this is I, that
 kind of thing.
 And when you like something or want something, when you
 possess something,
 this is mine, and you really cling to it.
 It's not that you aren't able to tell which is yours
 and which is someone else's, but when you cling to it,
 and you think of it as a possession of yours such that you
 have the soul
 right over it, such that if anyone were to challenge or
 threaten that,
 it would cause you stress and suffering and upset, anger.
 So not internally and not in regards to external objects,
 and finally enter into and dwell in that liberation of mind
,
 liberation by wisdom, to which there is no more ahaṅkara,
 māmāṅkara, or mana,
 añusaya, the añusaya, no more potential for giving rise
 to these.
 What's we talking about nirbāṇa? When someone attains n
irbāṇa,
 there is no, the underlying tendency is to give rise to
 these things,
 are broken off through wisdom, through seeing and through a
 change of perspective.
 Once one sees nirbāṇa, one's being changes.
 And through seeing and through the act of letting go
 and through the act of experiencing cessation of suffering,
 one sees that there's nothing worth clinging to
 and that the things that we cling to don't really exist in
 any substantial form.
 They come and they go and they arise and cease, not even as
 entities, but as experiences.
 So these three, good to remember, dīti-mana-tama,
 the three, lumpal-shodokha, this monk, here he always
 refers to them as nyanagati-dhamma,
 which I tried to find, but I couldn't find exactly that
 list.
 I found something in one of Lady Sayadaw's books.
 It was the only thing I could find about these three,
 but it's probably in a commentary or sub-commentary
 somewhere.
 Couldn't find it anyway.
 But nyanagatika-dhamma, or the dhammas, or he called them n
yanagatidhamma,
 but I found them as nyanagatika.
 They're the things that masquerade as nyanag, as knowledge,
 but they're not knowledge.
 So you can have a view and it appears that it's knowledge.
 You can have craving and it masquerades.
 You think you're realized and you're enlightened,
 but actually it's just your craving that's speaking, making
 you think that you are.
 And mana is conceit.
 You conceive of yourself as better or worse as one,
 and that leads you to believe, leads you to think.
 Or that these three things prevent you from seeing clearly.
 These three things, unless you break them, you can't
 actually see clearly.
 He talks about these three as you can't really cut them
 unless you practice vipassana.
 I think he talks about how using the mantra is what really
 allows you to cut through these three.
 So if you just observe, if you just know that you're
 walking, know that you're sitting,
 he says, well, dogs know that they're walking, dogs know
 that they're sitting.
 And he's quoting the Satipatthana commentary when he says
 that.
 If you listen to his talks, he says, "Magalu, dogs know
 when they're walking.
 They don't become enlightened."
 That's what the commentary says.
 Now, this is a special kind of knowing.
 It's a knowing that Kani says it's because dogs can't get
 rid of titimana tanha, he mentions these three.
 And he talks about how using the mantra, reminding yourself
, "Tira sanya."
 I found that all by myself.
 Tira sanya is the proximate cause of mindfulness.
 It makes it very, very clear.
 Sanya is the recognition. This is this.
 When you say to yourself, this is pain, there arises tira s
anya, fixed.
 Perception and remembrance, recognition of something as it
 is.
 And that gives rise. That's really what we would call sati
 in the Satipatthana sutta.
 Anyway, so that's the Dhamma for tonight.
 Do you have any questions?
 We do, Bante.
 I need some instruction with the advanced states in
 meditation.
 I'm confused in that you advocate jhana meditation, but say
 access concentration isn't part of the practice.
 I am wondering how you access jhana without access
 concentration.
 I've never experienced jhana without it.
 Should I set up an appointment with you?
 I don't advocate jhana meditation.
 So therefore, access concentration isn't part of the
 practice.
 I think you're misunderstanding our practice.
 We don't practice samatha jhana meditation.
 I mean, you can talk about luvipas and ajanas, but they're
 quite different.
 And you can talk about nokutta-najana, which is in all
 other categories of its own.
 But if you're talking about the samatha jhanas, as normally
 described by,
 as an absorbed state fixed on a single object, that's not
 what we're about here.
 So I'm not sure where you got the idea that we advocate jh
ana meditation.
 I mean, it's not that we don't advocate it.
 We just don't. I just don't teach it.
 Not samatha jhana meditation anyway.
 Bandh, I saw this question among the comments on YouTube on
 one of your videos a while ago,
 and am wondering the same thing.
 What are your thoughts on Ramana Maharshi?
 Two people asking for a practice to take up.
 We know that.
 It's long.
 Because I don't have any thoughts on Ramana Maharshi.
 Sorry.
 That's a no-no. I don't really answer questions about other
 people,
 especially when I'm not familiar with them.
 It's not really what this is for.
 It's just too, there's too many of those questions, and
 they're not really on point.
 I don't compare what I do to anyone else.
 I think people think better than comparing it. I don't know
.
 I guess that's what people want to do because you want to
 find the practice for you.
 But it doesn't work like that.
 You either do our teaching and follow it solely or not.
 There's not really a mixing and matching, like comparing
 and that kind of thing.
 You should pick one tradition and stick to it because it's
 too easy to mix and conflate
 and not realize that you're actually conflating things that
 are not the same.
 This isn't exactly a question. It's just kind of some
 discussion that was going on.
 But maybe good to clarify a question. You don't need Jhana
 for stream entry?
 Yes, you do. But you need Lokutra Jhana.
 It's a different kind of Jhana. Lokutra means super mundane
.
 Jhana is the Jhana with Nibbana as its object.
 You don't need to practice Samatha Jhana beforehand.
 I have a problem with an object of meditation, and it is
 when I think in something that is very unfair.
 Let's say that the object of meditation is injustice.
 The problem is that I cannot say injustice and justice.
 And I know that in particular what I am thinking, so I
 shouldn't acknowledge it as injustice.
 Do you want me to go on?
 It's hard to read, no?
 Oh no, I mean I can read it. I just... Okay.
 But it is kind of thinking something that I don't
 understand what it is,
 and I feel aversion to that because I just don't understand
 it.
 If I don't understand what it is, how can I see it for what
 it is?
 I'm not sure of how to solve this problem. Thank you.
 Okay, you think of something that is unfair. Well, that's
 thinking.
 Of course you never say injustice because that's not an
 experience.
 You don't experience injustice even though you might think
 you do.
 You experience thinking. You probably have an aversion to
 it.
 Well, if there's an aversion, then you should say disliking
, disliking.
 If it makes you angry, you would say angry, angry.
 If you judge it, you could say judging, judging would make
 work.
 It's not pejorative. You're not judging these experiences.
 You're just trying to be objective about them.
 Yeah, you see, I feel aversion. Well, there you go, disl
iking.
 If you're confused, you can say confused, confused.
 Unsure, you can say unsure, unsure. All of this is very
 good fodder for meditation.
 While noting my experiences when I'm practicing, it seems
 like the noting of the momentary experience
 gets in the way of clearly seeing it as it is in some way,
 even when I try to do it in a very subtle way.
 How should I deal with this?
 Well, stop trying to do it in a subtle way. That won't help
 the problem.
 But yes, it's designed to get in the way. We don't want to
 see the details of the object.
 The Buddha said clearly, and I've answered this question.
 It's a good question. It's just a very, very common
 question and sort of complaint, I guess.
 It's a common complaint and it's valid or it's invalid, but
 we have to explain why it's invalid
 because people don't realize that the Buddha actually didn
't want us to see clearly the objects in their details.
 The Buddha said, "Nanimita gahi, nanubyanshin agahi."
 One doesn't grasp the characteristics or the particulars
 because that's where problems arise.
 Seeing should just be seeing. Hearing should just be
 hearing. And that's it.
 You don't have to really investigate it.
 All you have to know about the things is that they're imper
manent, unsatisfying and uncontrollable.
 Noting allows you to do that because as soon as you know it
, you'll see they cease.
 Generally, they come and they go. What you will see about
 them is that, "No, it's gone. That's enough. Move on."
 That's all you needed to see because that's what you're
 trying to realize is that,
 "Oh, actually, there's nothing to learn about these things.
 They just come and they go."
 None of them are special and have any meaning beyond that.
 If it feels uncomfortable, it's meant to take you out of
 your comfort zone.
 So you should know that as well.
 If you have doubt about it, which often is accompanied by
 these sorts of complaints,
 to say doubting, doubting. You'll find you don't doubt so
 much anymore.
 Not that you believe everything, but you're free from that
 concern because,
 "Oh, I don't have to doubt it." Just say doubting, doubting
, and the doubt's gone.
 It's like everything else that comes and it goes. It's not
 me. It's not mine. It's not I.
 I just came upon a message regarding not asking you the
 question about the Jhanas.
 So sorry about that. I didn't have a chance to kind of pre-
read everything.
 Don't ask me that question.
 Yeah, but maybe it was useful to other people that were
 wondering too.
 "As I understand, it is okay to note something that
 happened very recently as long as it is fresh in the mind."
 In other words, an idea of getting an apple arises.
 If I happen to note the thinking, is it appropriate to also
 note wanting after thinking in that particular case?
 "It is fleeting indeed and it is confusing to me for we can
't grasp these fleeting states and shouldn't know what is
 not present."
 "As I understand, we can't want something and be mindful in
 the same moment."
 So in that sense, it seems we can't technically know
 anything as it actually happens.
 That's right. It's not possible and we're not trying to.
 It's misleading the idea that you should be in the present
 moment and note in the present moment because you can't
 actually.
 We're not trying to. That's not the point.
 The point is to find an alternative to reacting to whatever
 you've just experienced.
 So when you experience something, the next thing is going
 to be a reaction.
 Even if it's a reaction itself, if you like something, the
 next step is going to be a reaction to the liking.
 It's a lot more technically, if you get down to the moment
 by moment mind states, it's a little bit more technical
 than that.
 But basically, it's that. You react to everything,
 including your reaction.
 So to stop that at some point, you make a determination to
 change that, to remind yourself and to experience something
 just as it is.
 To react as it is. So when you like something and then you
 say after you like it, you say liking.
 It's just reminding yourself that that's liking.
 Instead of saying, oh, that's a problem, I have to go and
 get it.
 Or that's, I'm so glad I like that thing. Wow, I should go
 get it.
 With seeing, it's right after you see with hearing, it's
 right after you hear pain, it's right after.
 It's not meant to be right at the moment.
 But as far as your question about thinking and wanting, if
 you say thinking and then it disappears and there's no more
 wanting,
 you wouldn't then follow it up with wanting.
 Thinking would have been enough.
 But if you experience it as wanting, you should say wanting
.
 And if the wanting lingers, then you should say it.
 But you shouldn't say two or three things because in that
 moment there seemed to be, or there were moments of both.
 Pick one, note it, and if anything is still left, then you
 can note that as well.
 If nothing's left, move on.
 It's not important that you catch everything.
 It's not like we're checking off a list or something.
 We're just trying to stay, we're trying to train ourselves
 to see things clearly.
 And all I mean by that is to be objective, to not react
ively.
 See it come, see it come, that's it.
 What is the Buddhist view on dying hair bright colors? Is
 there anything against it?
 Dying the hair bright colors sounds a lot like attachment
 to image.
 What would be the practical purpose?
 If it's somehow kept mosquitoes away, that would be a
 practical purpose.
 I doubt it does.
 But otherwise it's an attachment to image.
 It's based on a desire for people to give rise to a
 reaction.
 Usually you want people to find you attractive.
 Or maybe you want to anger your parents for that kind of
 thing.
 You want people to think you're cool, hip, chic, that kind
 of thing.
 It's all ahangara, mamangara.
 It's all based on diti, mana, and tanha.
 When you first said diti, mana, and tanha, I actually knew
 what all three of those were and I immediately got conceded
.
 It's terrible.
 Can someone explain to me in the eightfold path that says
 right speech,
 which I believe is the idea of not saying things to hurt
 others,
 but one of the five precepts is that you should be honest.
 What happens if being honest is going to cause upset to
 someone?
 Can someone clarify?
 Well, yes, you could argue that the truth can hurt.
 One of the five precepts is not that you should be honest.
 The fun of the five precepts is that you should not tell a
 lie.
 But it doesn't mean if someone looks fat in a dress, you
 have to say,
 "Boy, you look fat in that dress."
 It doesn't mean every time a truth comes to you, you have
 to blurt it out, let people know.
 Like someone's walking down the street and a dress makes
 them look fat.
 You don't have to run after them and tell them that the
 dress is fine, obviously.
 It would be silly to think that you did.
 So it's not about being honest.
 It's about not lying.
 That being said, right speech is also not about saying
 things that don't hurt others per se,
 because what depends on your meaning of hurt, if something
 actually is to a person's detriment,
 then you could argue that that's pretty easily understood
 as wrong speech.
 On the other hand, if something "hurt" someone, but in the
 end is good for them,
 it makes them upset, but it's something that they had to
 hear,
 then that is a case where it could be right speech.
 The Buddha said, "He's careful about speech that is
 unpleasant."
 He doesn't speak speech that is unpleasant. He's careful
 about it.
 He knows the right time for it.
 Important is that speech is truthful and beneficial.
 If it's not truthful and not beneficial, it's wrong speech.
 If it's truthful and beneficial, then if it's pleasant,
 then it's always right speech.
 Or if it's something that someone would be happy to hear,
 then it's always right speech.
 If it's something that's unpleasant, then the Buddha knows
 the right time and the wrong time.
 He knows the right time when to say it.
 The point is, it's right speech if it's said at the right
 time.
 Meaning if it's said in such a way, at such a time, and
 with such an understanding,
 that it will actually benefit a person.
 On the other hand, that might be going too far and worrying
 about concerning yourself
 with those things might be a little bit overwhelming.
 Right speech is actually four types. It means no lying.
 There's no false speech, no gossiping in the sense of
 saying something.
 Someone says something to you, or you find something out
 somewhere,
 and you go and tell it to someone else to try to break
 these people up.
 Trying to break people up.
 Speech used to cause divisive speech, I guess is the
 definition.
 Harsh speech, speech meant to inflict pain, the intention
 to inflict pain,
 and useless speech, speech that is without purpose, speech
 for the sake of speaking.
 That's wrong speech.
 Only an hour hunt gets rid of that kind of speech.
 The other three are done away with an anagami.
 Well, the first one is done away with that sotapana.
 The second and third are done away with by anagami,
 but the fourth one is only done away with by an hour hunt.
 So even an anagami can have useless speech.
 Sometimes I feel formal practice is too overwhelming,
 so sometimes I just sit in my room doing nothing and noting
,
 "Is this a good practice?"
 Sure.
 Sure.
 When you want to look at why formal practice is
 overwhelming,
 it can become a sort of a stigma.
 You get stigmatized, right?
 Where just the thought of practicing is unpleasant to you.
 But if you sit there and note while you're basically doing
 the same thing,
 it's just there is a stigma against it.
 You have to look at whether you're forcing yourself,
 whether you're causing a disliking, an aversion to the
 practice.
 You have to meditate on the aversion so that it doesn't get
 in your way.
 And eventually, you don't want to avoid,
 you don't want to cultivate an aversion to practice,
 so you do want to overcome it.
 But being mindful of just sitting there is great.
 It's really what you're supposed to do in meditation anyway
.
 I think we're all caught up on questions one day.
 But I just wanted to mention we were talking about having a
 volunteer meeting on Sunday,
 and as it turns out, a bunch of people can't make it on
 Sunday,
 so now we're thinking tomorrow night, Saturday night before
 the broadcast.
 So did you have a particular time in mind, Bonthe?
 Maybe we'll do 7 o'clock.
 7 o'clock sounds good.
 So at 7 o'clock tomorrow night or a few minutes before,
 I'll post a link in the meditation site,
 and also on our Facebook volunteer site and our Slack site.
 We have multiple platforms where people keep in touch,
 volunteer projects.
 I haven't announced anything well. I'm supposed to pass
 this along.
 This is a temporary site. Don't bookmark this, but go take
 a look.
 This is a prototype of our new meditation. That's in mong
olo.org.
 And it looks a little bit different.
 It's a little bit, probably take a little bit of getting
 used to,
 but there's a chat.
 It looks more like the Android app. I think that's what we
 modeled it around.
 But it's new, and it doesn't have all the features of the
 old one,
 but this will be the new version. Make no mistake, there's
 no,
 "Do you like it? Do you think we should change?"
 This is what we're moving towards very soon.
 For the main reason, the main reason has nothing to do with
 how it looks
 or what features it has. The main reason is that the site
 we're using now is unprofessional. It's a poorly made hack.
 And so it's potentially unsafe. It's hard to upgrade.
 It makes things a lot more difficult than they have to be
 and a lot more problematic.
 Even if you're comfortable with the site as we have it,
 just let us know what features and what things about it you
 like,
 because the move has nothing to do with how it looks
 or the way it works from a user experience.
 The move has much more to do with the backend,
 what's working underneath that you don't ever see,
 because the new one is based on, as I understand,
 an established framework that is professional.
 And it's got some snazzy features.
 It doesn't have that circle.
 You need to think of that meditation circle with the
 circles in it.
 That's actually really cool. I wonder if there's a way to
 add that in
 at some point.
 It's just JavaScript. It's actually a hack in and of itself
.
 Okay.
 You just copy the JavaScript and fix it up.
 And I think I might suggest that it does put it in
 somewhere.
 The problem is that the format here with the tabs doesn't
 really
 lend itself well to--
 you just put underneath the finished meditating, right?
 Underneath the meditating list.
 You can also have the fourth tab, meditation circle.
 Or it can be above the finished meditating,
 and we wouldn't have a list of people who are currently med
itating.
 We just have the circle.
 I don't know.
 Is it simpler to have a list?
 You probably looked at the circle and said,
 "What are you doing?"
 It's super simple.
 Could be.
 But yeah, our volunteers have been working on this for a
 while,
 and they had security in mind.
 And thank you.
 Thanks so much to our IT team.
 I can view it on Android or Firefox.
 It's funny that you can't.
 [water lapping]
 While we were looking at that, there was another question
 submitted.
 What is your view on social media and posting pictures of
 yourself
 or things on social media?
 Is that trying to boost your ego, et cetera?
 It's been on my mind for a while.
 I think 99% of the time it probably is.
 It doesn't have to be.
 You'll notice that I'm averse to it.
 I can see the big--I can personally see a big problem with
 it.
 I tend to see how it's used to promote self, one self.
 Self-promotion, that's a good--
 And everyone was saying that about this
 when I started posting videos on YouTube.
 There were some snickers and criticisms about self-prom
otion.
 I never worried about that.
 What am I going to do?
 Have a picture of someone else or a picture of something
 else while I'm talking?
 This has never been for that reason.
 I think that would be silly to try and avoid showing your
 own face.
 To some extent, there's a good point there is that I wouldn
't want to
 because you want to make a connection.
 Having me sitting here talking is beneficial, I would say.
 Being able to watch the teacher teach, watch the person
 teaching teach.
 So it doesn't--simply showing yourself.
 But a picture--what does a picture of yourself do?
 Make sure a picture of yourself--what does it do except ag
grandize yourself?
 What does it do except promote yourself?
 It doesn't have to. It's not--it's true.
 And I have used myself in pictures personally
 without any sort of sense of self-aggrandizing.
 Just because I'm a monk and a monk makes a good subject for
--
 it would usually be because of a quote.
 It wouldn't be because, "Hey, look at me."
 I don't think--I think you do have to be careful.
 Everyone's posting selfies.
 I think it's a very, very problematic tradition.
 Is there anything you want to do less than post a selfie?
 That's why everyone likes your non-selfies, Bante.
 Yeah, well, that's the thing.
 And then what you do when you're-- when you're traveling,
 you want people to know
 you're okay, you're still alive with your--and keep posting
 non-selfies.
 It seems neutral enough.
 And a question from Ryan. Yes, it looks like with the new
 site,
 you would create an account, but you can use your same name
, I think,
 because it's pretty empty right now.
 You can get it while it's hot. And it's actually not your
 name.
 Your name can be duplicates, actually, which might be a
 problem.
 Well, not really. Everyone--different people might have the
 same name.
 The name is separate from the user. The user is not a user
 name anymore.
 It's an email. So one email, one user.
 And your name is totally arbitrary. You can make your name
 whatever you want.
 Although we might ban you if it's objectionable.
 But otherwise, you know, it'd be nice if everyone used
 their real names, honestly.
 At least their first name.
 User names are, you know, aliases. This isn't really the
 community for aliases.
 You go to a Dhamma group and say, "Oh, my name is Zweebak33
," or something.
 "No, my name is Joe. My name is Jeff."
 Tell us who you are. We don't want that.
 Unless you're in hiding in a closet meditator or something,
 living in a repressive religious household,
 where you might get fired if people find out you meditate.
 There was a guy in Israel who didn't want me to--
 I was doing recording before I was a monk. I was doing some
 video recording
 and taking shots in the group. And he came up to me and
 said,
 "Please don't record me because it's dangerous for him."
 And then a question--
 No, I'm done.
 Okay, just a couple more questions. If there's more
 questions, tomorrow.
 Okay, this was just a question on the volunteer meeting.
 It's going to be recorded and posted.
 Actually, not sure. Sometimes people are not really
 comfortable
 when things are being recorded.
 And our volunteer group that was showing up to the meeting
 kind of shrunk down a lot.
 So I'm not sure if part of that was because people were
 uncomfortable
 being recorded and on air.
 So if that is the consensus, we may just take notes from
 the meeting
 and just take notes.
 I'm broadcasting.
 Monte has run away.
 Tomorrow morning I actually have an appointment at 9 o'
clock.
 I can come after that.
 All right, sir.
 Okay, thank you. Thank you, sir.
 I'll see you.
 Sorry, Niamh.
 These monks have been trying to get in touch with me.
 Tomorrow morning they do want me to go to
 They have--
 I think it's they have supporters who speak English
 and they want me to give talks.
 So I go for lunch and I give the talk.
 Just like 10 minutes.
 It's nice to meet all these nice people.
 And I really appreciate it.
 Usually they have kids.
 It's probably mainly because of the kids who don't
 understand Sinhalese.
 They want them to have a little bit of English dhamma.
 Is that right in Hamilton, Bande?
 No, Mississauga.
 Oh, okay.
 Toronto.
 So we're still using this site for now.
 You only ever have to come to meditation.siri-mongolo.org,
 but we'd like you to try out the new site
 and potentially post your meditations there as well.
 And if you know how to use GitHub,
 you can post issues to let us know if there's any bugs.
 I don't know if he has a GitHub link there.
 But anyway, save your questions for tomorrow.
 We'll try to get back to them.
 That's all for tonight.
 Good night, everyone.
 Thanks, Robin.
 Thank you, Bande.
 Good night.
 Thank you.
