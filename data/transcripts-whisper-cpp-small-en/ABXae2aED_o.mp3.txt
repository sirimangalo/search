 Okay, good evening everyone.
 I'm broadcasting live October 12th.
 Thanksgiving I think.
 So we're going to go with the Dhammapada again today.
 Get started on that then.
 Test.
 Hello and welcome back to our study of the Dhammapada.
 Today we continue on with verse number 84 which reads as
 follows.
 Nā ata hetu nā parasa hetu nā putam iché nā dhanam nā
 raktāng nā ichayya adhammena samidimatanau
 nā ata hetu nā parasa hetu not with oneself as the cause,
 not with another as the cause.
 Not because of oneself, not for the cause of oneself, not
 for one's own benefit, just
 for one's own benefit, not for the benefit of another.
 Nā putam iché, not for desires in regards to children, n
ā dhanam or wealth, nā raktāng
 or even a kingdom.
 Nā iché yā adhammena samidimatanau one should not wish
 for success, for worldly,
 for one's own prosperity by means of adhamma, by un
righteous means.
 Such a one should be siloah, have morality or ethics.
 Pānyavā should be wives, dhamikauśya, thus they should
 be righteous.
 Dhamika, the word dhamika is one who has dhamma, ika means
 one who has.
 So this verse was spoken in regards to dhamika, the dhamik
atera, who was a monk who had a very
 good name, but he also lived up to his name.
 It seems that he was living well, lived according to the d
hamma, so maybe this is how he got
 the name, maybe it wasn't his real name but it's what they
 called him because he was righteous.
 Could have also just been his name.
 Sometimes names do give power in that way, you know.
 Name someone something and they feel like they have to live
 up to it or it guides them
 through their lives.
 So naming someone Tiffany or something might not be as
 meaningful.
 Noah is interesting, he's got an interesting meaning.
 It certainly affected me, you know, you've got these names.
 On the other hand, what's in a name, right?
 There was another story of a man named Arya and he was, I
 think he was a fisherman.
 And so the Buddha went, he was walking along one day and he
 came across this man named
 Arya who was really quite proud of his name and thought he
 had such a great name.
 And the Buddha, I think will have this, I think pretty sure
 it's in the dhamma-pada,
 so we'll come to it.
 So just a little spoiler, he turns to all the monks and he
 asks them, "What's your
 name?"
 And all the monks are telling them their names.
 And Arya, this man who's sitting there, maybe he came to
 listen to the Buddha teach or something.
 He's like, "Oh, I hope the Buddha asks me my name."
 And finally the Buddha gets around and says, "So what's
 your name?"
 He says, "My name's Arya."
 And he says, "It's not a very good name for you.
 You don't call someone an Arya who kills."
 Anyway, so this guy whose name was Dhammika, having
 listened to the Buddha's teaching often
 became desirous of ordaining.
 And so he said to his wife, "Look, I want to ordain."
 But his wife was pregnant.
 And so she said, "Wait, please, if you must go forth,
 please wait until our baby is born."
 And so he waited and sure enough, baby was born and then he
 waited until the baby was
 old enough to walk.
 And then he said to his wife again, "I'm really ready.
 I've been patient, but I really would like to become a monk
."
 And his wife says, "Well, wait at least until he's come of
 age."
 So he said, "Until our son is left home."
 And the man was silent, but then he thought to himself, "
What good is it to try and wait
 for her permission?"
 And he just went off and became a monk.
 In the reminiscent of the Bodhisatta, when he left home,
 and everyone's always critical
 of this, how can these Buddhists allow their students,
 allow their people to just go forth
 leaving behind wife and child?
 How could the Buddha set that kind of example?
 Not taking care of his family, so they like to criticize
 him this way.
 When we talk about this verse, there should be something to
 say on that, so I'll wait.
 But yeah, so he just left home and became an Arahant,
 practiced, obtained meditation
 subject and strived and struggled and became an Arahant.
 After becoming an Arahant, he went back to Savati to where
 his wife and child were living
 and they gave him Om's food and he taught the Dhamma to
 them.
 He would go and sit in their house and teach them the Dham
ma and sure enough, his son became
 interested, let's follow the family line, and went forth
 and became a monk himself,
 and in no long time became an Arahant himself.
 And last but not least, the wife realized that thinking to
 herself, "What is there left
 for me in the home life?" became a bhikkhuni, a monk, and
 became an Arahant herself.
 So this is one example of the greatness of ordaining, and
 in fact it's a good example
 to think about.
 They would criticize in the time when the Buddha went back
 to Kapilavatu, all the Sakyam
 men followed him, and even the women eventually followed
 him and became the first bhikkhunis.
 And everyone was up in arms saying, "He's taking away our
 children.
 How can these people just take our children away from us?"
 And so they told the Buddha this, and the Buddha said, "
Tell them..."
 This was when Buddhism was new, you have to remember, so
 the word dhamma wasn't very well
 understood, but he said, "Tell them they ordain according
 to the dhamma."
 And that worked somehow, so they went and when people
 criticized or scolded them for
 this, they said, "People ordain under us according to the d
hamma, according to the truth, according
 to righteousness."
 And so at that point, if anyone wanted to criticize, the
 point was they would have to criticize
 the teachings.
 It made people think and say, "Well, that's true.
 If you become a monk, if it's for a good reason, if it's
 according to the truth, and if there's
 truth behind the teachings, then how can you criticize that
?"
 And to some extent, how can you even criticize leaving
 behind wife and child, but we'll get
 into that.
 That's what this verse really addresses.
 So there's not much more to the story.
 They all became ordained and the monks were talking about
 it and thought it was exceptional,
 that he was able to set an example for his son and
 eventually his wife followed him.
 And the Buddha used this as an example and said, "This is
 really the way we practice
 in Buddhism.
 This is the direction in which we're headed."
 And he gave this verse teaching.
 And it's an important verse, I think, one that I always
 think about, "Nata het una prasa
 het," and usually can't remember the rest of it, but at
 least that part.
 Not for one's own benefit or for the benefit of another.
 Should one do evil is the first part of this.
 So the most extreme aspect of it is there's never any
 reason to do something against the
 Dhamma, a Dhamma in the Samindi Mantana.
 One should not wish for success in an evil way, success for
 oneself in a way that goes
 against the Dhamma.
 But there's more to that.
 Because as the Buddha says, a wise man, he says more
 specifically, more directly, one
 should never desire success at all, really, for one's own
 sake or for another.
 Meaning worldly success.
 And this is sort of the idea in the Buddha's teaching that,
 well, all of what we're taught
 and all that our culture pushes us towards is suspect at
 best.
 We would consider this to be faulty, is based on delusion.
 The idea that there is something meaningful about worldly
 success.
 The Buddha said, as we'll read later on, better than kings
hip, better than lordship over the
 angels or earth, better than to be the emperor of the world
 is the attainment of sotapanda.
 Sotapati palamwaram.
 Because really, what purpose can there be in worldly things
, in samsara at all?
 People ask, what is the meaning of life?
 What is the purpose of life?
 There's no reason to think that the universe admits of any
 purpose, that there can be found
 any purpose in samsara.
 The only way you could think of such a purpose is if you
 had a God, a divine being who was
 running everything.
 In which case, it's more like a prison than anything.
 Because there's no perfection to be found in the world.
 It's just always changing.
 It's quite natural.
 It appears to be quite as one would expect it to appear to
 be, just random and chaotic,
 organized according to laws and rules and patterns.
 But in the end, there's no protection from suffering.
 There's no protection from loss.
 There's no protection from the dissolution of all the
 things that we strive for.
 So there's more to it than just not doing evil.
 One should not wish for sons, wealth, or even a kingdom.
 One should do nothing by anjan, just means.
 One should not seek these things out, certainly.
 There's two things being said here, really.
 The Buddha is often like this in the sense of couching it,
 because he doesn't want to
 alienate people, whereas I am not so careful, perhaps, when
 the commentaries may be also
 not.
 But the Buddha does say two things here.
 He says, you should not desire these things.
 But then he also couches it and says, at the very least-- I
 mean, that's what it seems
 like he's saying, because in the next part, he says, by
 unjust means, adhammina.
 Perhaps you could also interpret this, I think, to mean one
 should not seek wealth outside
 of the dhamma.
 Adhamma, in this sense, could be outside of the path to
 freedom.
 So if you're looking for success, it should not be outside
 of the dhamma, which is basically
 what's being said here.
 Of course, the translation usually of adhamma is unjust, un
righteous, so evil.
 But in the end, it does boil down to the same thing,
 because we would consider on a very,
 very ultimate-- if you're going to nitpick-- on an ultimate
 level, any desire for sons,
 for wealth, even for a kingdom, for anything, any desire
 for anything, is by a Buddhist
 definition, evil.
 Evil in the sense that it's going to lead to stress and
 suffering.
 It has an unpleasant result, a result that is not going to
 favor us.
 It's not going to make us happy.
 It's going to keep us being reborn again and again and keep
 us in the prison of samsara.
 So these are things we should look at in the practice.
 It's something we should check in ourselves, because these
 are views that are deeply ingrained
 by society that we've been taught since we were born.
 As you grow up, you're taught by example of your parents.
 So the great thing about this story is here we have someone
 who is actually an example
 for their family.
 Going back to his family, could you imagine having a parent
 who was a monk?
 In Thailand, I hear about this.
 Someone in their family was a monk.
 And he was actually a role model for his child, who said, I
 want to be like my father when
 I grow up.
 And even for his wife, in the end, who followed along, it
 points to the greatness of ordaining,
 how it can affect your family.
 And again, the defense is that it's based on the dhamma.
 So leaving your family behind to go and live a negligent
 life would be definitely a blameworthy.
 But to go and to live it in a spiritually enlightening way
 can only help both oneself and the people
 close.
 But most of us get a different sort of role model.
 We're taught to be physically attractive.
 We're taught to be eloquent and charismatic.
 We're taught to be powerful, confident.
 I mean, confident isn't bad, even power, even eloquence,
 these aren't bad things.
 But we're taught to be things that are going in a way that
's going to help us in the world.
 We're taught of the value of money, the value of power over
 others, the value of social
 status.
 We've taught so many things, the value of stability, which
 is totally, totally overrated.
 But it's so seriously ingrained that we're so afraid of
 laws or of being shaken up.
 We become so inflexible in our lives.
 We need to have a stable job and a stable income.
 And we often set ourselves up in these castles that become
 prisons, prisons of debt, prisons
 of responsibility, the prison of ownership.
 It's an interesting thing.
 We think it's so great to have possessions, but our
 possessions end up owning us because
 then we need locks to keep them safe and we need to protect
 them and to guard them and
 to take care for them and so on.
 We can't move as easily, we can't pick up and go anywhere
 we want and so on.
 So all of these things.
 And then we're taught about progress, getting education for
 the means of getting a good
 job, getting a good job for a means of having a retirement
 plan and so on.
 In the end, it leads to the same death at the end.
 It doesn't lead anywhere different.
 In fact, it doesn't lead to anything greater, something
 maybe we feel proud of at the end
 of our life, sure, but even that pride is in and of itself
 a problem.
 Nothing we should feel proud of, nothing that is of any use
.
 On the other hand, when we live our lives focused on the
 dumb, focused on being moral,
 ethical, charitable, generous, kind, compassionate, and to
 be calm and collected and clear-minded
 and wise, who wouldn't be proud of such things as this?
 And of course, not be proud exactly, but feel good and feel
 content, feel confident.
 These are things that we should feel confident about.
 So these are things that we have to look at in our
 meditation, that we have to look at
 not just when we're meditating, but in our lives and try
 and identify them because they
 can get in the way of meditation when you want to do
 meditation, but at the same time
 you're keen on getting a raise or getting a better job or a
 bigger house or a nicer
 car or getting married.
 Some students who are keen for romance, it's an obsession
 for some people.
 All of these things we have to look at and really remind
 ourselves, scold ourselves even,
 "This is not really going to make me happy."
 There's really no benefit in those people who have strived
 for these things or have
 never ended up content and happy as a result of them.
 Their contentment and happiness is dependent on other
 things, is dependent on their state
 of mind, their clarity and their purity of mind.
 These are things that the Buddha reminds us to stay away
 from and it paints a clear picture
 of the difference between the way of the world and the way
 of the Dhamma which go diametrically
 opposed.
 Mahasya Sayadaw gives a great brief talk about this in one
 of his books.
 Most people don't like to hear the Dhamma because it's diam
etrically opposed to their
 way of life.
 For most people, these things that I'm talking about are
 the right way.
 Whenever I talk about things like exercise and what, music,
 art, beauty, there's always
 someone who's going to be disappointed and upset and turned
 off.
 It's important for us to choose sides in the sense which
 should choose directions, not
 sides but directions because they don't go in the same
 direction.
 Nahi Dhamma Adhamu, Nahi Dhammo Adhamo jaa, uvosanta vipak
ina, vipakino vipakina, I can't
 remember.
 The Dhamma and that which is outside of the Dhamma don't go
 in the same direction.
 Adhammo niriangni, dhi dhammo pape di sugutti.
 The Dhamma leads to suffering, to hell actually.
 Dhamma leads to heaven or a good way, leads in a good way,
 leads to a good becoming or
 a good state.
 Eventually leads to nirvana.
 That's the Dhamma Pada for this evening.
 Thank you all for tuning in, keep practicing, and be well.
 I don't know why I'm getting the static from you as well
 now.
 For me?
 Yeah.
 I don't hear static.
 Yeah, but it'll probably be in the recording.
 I wonder why that is.
 I shouldn't do that.
 It's probably because I'm on Linux or something.
 Anyway.
 I don't have any questions.
 Yes, we do.
 Okay.
 Next one is, hello, you, Dhammo.
 I'm a singer and I have been losing interest in my job
 since I meditate.
 Now I have a big conflict in my mind to continue my job or
 not to continue.
 Is that a good sign and do you have any suggestions?
 Yeah, the problem of being a singer because it's kind of
 playing on people's sensual desires
 and emotions.
 It's encouraging an emotional response.
 It's not considered to be the best livelihood.
 It's not wrong livelihood per se.
 It's just not the best.
 I mean, people will argue that there are songs and lyrics
 that are meaningful, but that's
 not the reason it's a song.
 The song is for sensual pleasure now.
 I think you could put a better argument for poetry, poetry
 reading.
 If you're a song or you're a singer, you might also be a
 writer.
 If you're a writer, then the things that you say in song,
 you could put into written word.
 Probably not as, but it's your voice that you're more
 relying upon, right?
 But no, relying upon your voice is relying upon sensuality.
 So yeah, I mean, it's a great sign that you are becoming
 more inclined towards renunciation.
 You're not just from singing, but the idea of becoming
 bored of one's worldly interests
 or one's worldly practices is in general a good sign.
 I wouldn't worry that much about it while you're doing it,
 but for your own peace of
 mind in the end, yeah, it's probably in your interest in
 some way to live a more meaningful
 life instead of one that is.
 The thing about being a performer as well is ego plays a
 part often and emotions, your
 own emotions are playing with them sometimes.
 Acting is one that I'm not really all that, I mean, I was
 an actor for just an amateur
 summer theater actor for a few seasons and you really get a
 sense that actors have their
 emotions really played with and it's hard to really be
 mindful, I would say, when you're
 pretending all the time, you're evoking emotion and that
 kind of thing.
 Definitely evoking emotion in others is going to have its,
 I mean, it's minor, but it's
 just not the best.
 The best would be helping people.
 And we make an argument, you know, that songs sometimes
 help people, but it's not really,
 it's like a feel-good solution.
 What does help, I would say, you could argue, is the lyrics
 and so there is something there.
 It is a medium by which you can reach people who are
 addicted to pleasant sounds, rhythm
 and that kind of thing, but it's still an addiction.
 I'm going to get downvoted for that for sure, all the music
 lovers.
 I have a question.
 Should one find a job?
 Should one's priority be helping others or focusing on
 mindfulness meditation?
 When you help others, you help yourself.
 Or when you help yourself, you help others, it's not really
 all that distinct from each
 other.
 But the monk life is the ideal livelihood.
 So the closer you can get to that, the better.
 Yeah.
 When there's not a Buddhist community that, you know, will
 definitely support you in terms
 of food and shelter.
 It's difficult to make that move.
 It's always difficult to make that move, but it doesn't
 mean it's impossible.
 Yes.
 When you think of that guy who went and sat in the Shaolin
 courtyard for days, do you
 have that kind of determination?
 I see.
 Next question.
 In walking meditation, when I'm very focused, I often
 almost always find myself taking a
 deep breath at the end of the path.
 About 20 feet takes seven minutes, as if I wasn't breathing
 at all from A to B. It can't
 be, I guess, but it does feel that way.
 It doesn't bother me, but it could mean that I'm putting
 too much effort.
 Should I pay attention to my breathing while walking or
 relax or something, or just not
 worry about it?
 I wouldn't worry about it.
 If you find yourself worrying about it, say worrying.
 Doubting, doubting.
 Sometimes you do have to pay attention to your state of
 mind.
 You find yourself getting really stressed out, then maybe
 you have to acknowledge that
 as well, stress, stress, to really tense, say tense, tense.
 It's good to stop and acknowledge these things.
 Often we miss something important in meditation.
 We're not acknowledging something.
 It's not that harmful because eventually it's going to get
 so blatantly obvious that you're
 just going to note it, but don't ever forget anything.
 Be clear that we're not just noting the rising, falling, or
 the stepping right, stepping left.
 There are a lot more things to note than that.
 That's why we have to remember the four satipatanas.
 That's why there are four satipatanas to point out the four
 different types of things that
 might arise.
 Bonté, you said in previous videos that mindfulness cannot
 arise without noting.
 I use noting and agree it's very useful to establish
 mindfulness, but isn't this statement
 too radical since language is more artificial than
 mindfulness?
 If I said that I was probably misspeaking or just general
izing, I don't know exactly
 what I said in the video.
 I'm betting it probably wasn't exactly what you're hoping
 it wasn't, but it's not generally
 something that I say.
 I might have said that in our tradition, what we consider
 to be satipatana and mindfulness
 and from an Abhidhamma point of view, I think is present in
 every wholesome mind, even when
 you're not meditating.
 It's quite clear that there is mindfulness in many types of
 mind.
 What we're looking for, the type of mindfulness that we're
 looking for, and it's a specific
 state that we call mindfulness, we would argue that, not
 even that it can't arise.
 I may have said that if I did, I'm sorry.
 It's not really true.
 Objectivity.
 No, I mean, even object, it can all arise.
 You don't have to use this technique.
 This technique is, I would say, the best way for it to
 arise.
 Another sense, it does only arise in this technique, and
 what only arises in this technique
 is the recognition, the remembrance.
 So if you aren't saying to yourself, "This is this," like
 the Buddha said, "Ditetitamatang
 buhisati," when you see, let it only be seeing.
 So if in your mind there's something different from seeing,
 if your mind is not clearly aware
 this is seeing, then it's not really mindfulness.
 In that sense, it is mindfulness in the Abhidhamma sense,
 but not in the Satipatthana sense, the
 establishment or the establishing of mindfulness or the
 fixing of mindfulness.
 So you've answered this next question many times, I believe
.
 What do you think about modern education?
 I actually replied that if you have a video on the topic,
 you just have to search for
 modern education on your channel.
 Probably.
 I think we even answered it recently on this program.
 Yeah, I asked that.
 One thing I wanted to mention is YouTube has this new thing
 where you can splice videos
 and make new videos.
 So if anyone finds a question, if there's any question on
 here that someone says, "Hey,
 he hasn't done a video on that," and that answer would make
 a good video on its own,
 if you can give me the start time and the end time of that
 question and answer, we can
 cut it and make a new video and then put it on the video
 archive.
 So I mean these sessions here where we have many questions,
 if there's any question that
 anyone thinks, "Hey, that would make a good video and it
 hasn't been done before," if
 you can help me out by finding these start and end times,
 then I think it's pretty easy
 to go in and cut the video and make a new one and then we
 can...
 Oh, okay.
 Because what I used to do is I used to record these twice,
 right?
 But I'm not going to do it.
 It's a bit distracting to do that.
 Next question, "Can samsara be enjoyed?"
 I think they mean, "For the reason of searching for better
 experience."
 It can be, yeah.
 The question is, "Should it be?"
 No, it shouldn't be.
 Because if you do that, you're just going to lead to more
 suffering, more disappointment,
 more wandering on ups and downs, lots and lots of
 dissatisfaction.
 If you have an anxious state of mind before you die, for
 example, if you are afraid of
 the fact that you are dying, what kind of life can you
 expect to be born in?
 Thank you.
 It's not so simple that you can just say this or that, but
 it's not wholesome, so it's going
 to send you in a bad way.
 You could read the Chula Kamma Vibhanga Sutta, I think.
 Probably the Mahakam Vibhanga Sutta as well.
 In the Majjhima Nikaya, there's two sutta's that...
 Let's see if I can keep going.
 135, Majjhima Nikaya 135 and 136.
 Those are two sutta's that talk about it.
 I mean, the first one does anyway.
 I can't remember if the second one.
 135, definitely.
 136 is more intricate.
 Yeah, it's a little bit different, but no, they're both on
 the subject.
 So I would read those.
 The second one tells a little bit how more complicated it
 is.
 I have a similar question.
 Could only our haunts enter into Nibbana, or could you
 enter in Nibbana if you enter
 into a state of cessation?
 A sotapana enters into Nibbana, not just Naruhan.
 But the first time you see Nibbana, that's your sotapana.
 If you were to die in that state, would you go to Nibbana?
 No.
 No, it doesn't work that way.
 Yeah, because you're still craving, right?
 You're still is craving.
 Okay.
 Of course, the sotapana can become an arahant at the moment
 of death.
 So that's how it would work.
 That's what you would call it.
 So the sotapana can become an arahant at the moment of
 death.
 It can happen.
 It's a bit more complicated than just entering into
 cessation.
 Is it okay to evoke emotions to teach the Dhamma?
 It's a good question.
 Sometimes you might have to, I would think.
 But yeah, there is a problem with it.
 I would say it's not the best idea.
 You shouldn't be evoking emotions necessarily.
 You should be evoking a sense of urgency.
 You should be evoking mindfulness in people, understanding.
 Sometimes we use emotions to convince people that we
 shouldn't really.
 We should be using wisdom to convince people, which is more
 difficult.
 It's easy to make someone want to practice.
 It's harder to make someone understand why they should
 practice.
 I mean, it's not easy to make someone want to practice.
 It's easier though.
 There are lots of ways you can manipulate people into
 wanting to practice.
 It doesn't have the best results in the end.
 Maybe music is like a drug that causes addiction, whereas
 meditation is like medicine for the
 mind.
 Yeah.
 Bonté, when someone is talking to me, should I practice
 mindfulness by thinking hearing,
 hearing?
 Or should I be mindful to the words they are saying?
 If it is the former, how can one carry on normal
 conversations while maintaining mindfulness
 as one would not actually hear what the other person was
 saying?
 You can do both.
 It works well enough.
 The mindfulness just stabilizes your mind.
 When you say hearing, it stabilizes you and keeps you from
 being judgmental.
 It doesn't keep you from understanding what's being said.
 Your mind goes back and forth.
 Your mind is quick enough to do both.
 It is true.
 In ordinary discourse, you can do both.
 Mindfulness is enough to stabilize you, keep you objective,
 but at the same time, you understand
 what the person is saying.
 If one would enter an immaterial jhana, how should it be
 noted?
 You can't note when you are in the jhanas, summit to
 meditation.
 It is a different kind of practice.
 I mean, there is a lot of controversy around this.
 It is not always clear what the Buddha meant by jhanas.
 It is quite complex, actually.
 If you read about ten teachers on the jhanas, you will find
 ten interpretations.
 There are states where you are in a trance and you can't be
 mindful.
 You can't acknowledge.
 It is a different kind of meditation.
 It is a trance.
 I have a question.
 Is it ever acceptable to lie if you determine it is not the
 right moment to tell the truth?
 No.
 Okay, next question.
 What is a monk's view on energy?
 It seems like the goal is cessation.
 Is energy an evil thing?
 Energy is a wholesome mind state.
 If you are talking about a person's energy, energy is
 useful.
 It is wholesome.
 In the end, it is worldly.
 It is not something that is in and of itself a goal.
 In the end, you have to let it go.
 Why is it not possible for a being to experience more
 satisfaction than dissatisfaction and
 seeking pleasure?
 What underlying mechanism or law is at work?
 Thank you, Bonté.
 It is possible, but it is not sustainable.
 So you will go back and forth.
 If that is what you want in life, brief moments of
 satisfaction, long periods of striving
 and moments of dissatisfaction or long periods of
 dissatisfaction.
 In the end, the point is that it is meaningless.
 It has led you here.
 This is the result of it.
 If you are satisfied with the results, then keep going.
 Congratulations.
 This isn't even the median because it gets a lot worse than
 this.
 Mostly we are born as animals.
 So living life as an animal has sort of the average.
 Like a wild animal is your thing.
 You think, well, that sounds like a great life.
 The killer be killed, survival of the fittest kind of thing
.
 Even power to you, but it is not really.
 If you lived a life that was, let's say, 25% extreme
 suffering, 25% great suffering, 25%
 great happiness, and 50% sort of nothing, would that seem
 to you to be the ideal way
 to live?
 I mean, I am just giving out figures at random, but suppose
 it were like that.
 Let's say the universe is balanced for suffering and
 happiness.
 Is that really an ideal life?
 What you find as you practice is that it is inherently not,
 that there is no benefit to
 the happiness and that it is not something that you feel at
 all interested in clinging
 to or striving for.
 In fact, it is the striving that leads to the negative.
 The more you strive.
 I guess another way to answer, just pointing out, is this,
 that you say, well, why shouldn't
 we seek after pleasure?
 It is possible that you could get more pleasure than
 suffering, but it is actually the key
 that you are missing is that the striving itself is what
 leads to the suffering side.
 So yes, people do have great states of happiness, but it is
 at the times when they are striving
 for it the least.
 So the less you strive for samsara, the less you strive for
 pleasure, paradoxically the
 more pleasure you have, the more happiness you have.
 And so Narahant has great states of pleasure and happiness.
 You just don't cling to them.
 Whereas a person who is in great suffering is always
 wanting happiness, wants only happiness
 and is obsessed with it.
 And as a result, they suffer horribly.
 You see, so it is not something that is just sustainable.
 Let's just go and try and get as much happiness, strive for
 as much happiness as we can.
 It actually works the opposite way.
 The more you strive, the less satisfied you are.
 Are we all caught up?
 Yes, we are.
 All right.
 I was just thinking about what you said, like to experience
 happiness but not cling to it,
 because if you feel happy, it's pleasurable.
 How could you not crave it or want to continue to
 experience it?
 Well, that's the practice, right?
 I would say to yourself, happy, happy, happy, and just
 acknowledge it, be objective about
 it.
 Mm-hmm.
 So.
 We're all caught up.
 Thank you, Stefano, for filling in.
 Thank you, Robin.
 My pleasure.
 Thank you, Bonté, for the beautiful Dump-Dump-Apana talk
 and answering everyone's questions.
 Thanks everyone for tuning in.
 Have a good night.
 Have a good night.
 Thank you.
