1
00:00:00,000 --> 00:00:02,000
 Sublime Abiding

2
00:00:02,000 --> 00:00:05,740
 As I meditate, I first want to develop a context of equanim

3
00:00:05,740 --> 00:00:07,960
ity and loving-kindness with respect

4
00:00:07,960 --> 00:00:11,480
 to my thoughts and emotions as they arise.

5
00:00:11,480 --> 00:00:14,800
 We live in a culture of attack or be attacked.

6
00:00:14,800 --> 00:00:19,700
 Can you do a short guided meditation on calm abiding please

7
00:00:19,700 --> 00:00:19,840
?

8
00:00:19,840 --> 00:00:23,120
 Calm abiding or meditations on altruistic emotions like

9
00:00:23,120 --> 00:00:24,960
 friendliness can be very useful

10
00:00:24,960 --> 00:00:27,760
 as companions to vipassana meditation.

11
00:00:27,760 --> 00:00:31,060
 I would not recommend to practice developing loving-kind

12
00:00:31,060 --> 00:00:32,800
ness or equanimity alone because

13
00:00:32,800 --> 00:00:36,050
 that can lead to avoiding the emotions they are meant to

14
00:00:36,050 --> 00:00:36,840
 counter.

15
00:00:36,840 --> 00:00:40,040
 When you are angry and pretend that you love everyone, it

16
00:00:40,040 --> 00:00:41,520
 is really not a way of gaining

17
00:00:41,520 --> 00:00:45,090
 sincere friendliness or appreciation of the suffering that

18
00:00:45,090 --> 00:00:47,160
 other beings are going through.

19
00:00:47,160 --> 00:00:50,160
 The best way to develop friendliness, compassion,

20
00:00:50,160 --> 00:00:52,840
 appreciation and equanimity is together with

21
00:00:52,840 --> 00:00:54,520
 vipassana meditation.

22
00:00:54,520 --> 00:00:57,340
 I would caution against the view that you need to develop

23
00:00:57,340 --> 00:00:58,920
 them before you can adequately

24
00:00:58,920 --> 00:01:01,240
 develop insight meditation.

25
00:01:01,240 --> 00:01:04,310
 You should examine the emotions that are causing you to

26
00:01:04,310 --> 00:01:05,360
 think that way.

27
00:01:05,360 --> 00:01:08,240
 Maybe you have states of great anger or hatred in your mind

28
00:01:08,240 --> 00:01:09,840
 that you are trying to shy away

29
00:01:09,840 --> 00:01:13,630
 from or maybe there are states of attachment to the peace

30
00:01:13,630 --> 00:01:15,400
 and the happiness wanting to

31
00:01:15,400 --> 00:01:17,280
 feel love and equanimity.

32
00:01:17,280 --> 00:01:21,240
 In the end, you have to put aside such partiality and try

33
00:01:21,240 --> 00:01:23,840
 to face your negative and positive

34
00:01:23,840 --> 00:01:26,880
 emotions with equal objectivity.

35
00:01:26,880 --> 00:01:30,360
 When you feel angry, you have to learn about anger.

36
00:01:30,360 --> 00:01:33,880
 The best way to do away with it is to understand it.

37
00:01:33,880 --> 00:01:37,580
 When you want to feel love or peace or when you do not like

38
00:01:37,580 --> 00:01:39,720
 the ups and downs of the mind,

39
00:01:39,720 --> 00:01:43,960
 where the mind is liking, disliking, liking, disliking and

40
00:01:43,960 --> 00:01:46,320
 you wish you could be equanimous,

41
00:01:46,320 --> 00:01:49,670
 you have to examine that wanting for equanimity and the

42
00:01:49,670 --> 00:01:51,920
 aversion towards the turbulence in

43
00:01:51,920 --> 00:01:53,520
 the mind.

44
00:01:53,520 --> 00:01:56,250
 During the practice of epacana meditation, you can switch

45
00:01:56,250 --> 00:01:57,720
 to friendliness or compassion

46
00:01:57,720 --> 00:02:00,440
 when overwhelming emotions arise.

47
00:02:00,440 --> 00:02:04,000
 When you feel hatred or rage towards other beings, that is

48
00:02:04,000 --> 00:02:06,000
 an appropriate time to develop

49
00:02:06,000 --> 00:02:09,430
 friendliness or compassion as a means of weakening the

50
00:02:09,430 --> 00:02:12,040
 emotion, redirecting the mind towards

51
00:02:12,040 --> 00:02:14,240
 wholesome mind states.

52
00:02:14,240 --> 00:02:17,900
 A simple exercise in friendliness is to first focus on

53
00:02:17,900 --> 00:02:20,400
 yourself and make an explicit wish

54
00:02:20,400 --> 00:02:23,640
 that you may be happy and free from suffering.

55
00:02:23,640 --> 00:02:25,240
 May I be happy.

56
00:02:25,240 --> 00:02:29,120
 Next, make the same wish towards others starting with those

57
00:02:29,120 --> 00:02:30,680
 who are closest to you.

58
00:02:30,680 --> 00:02:33,890
 It can be the people who are in closest physical proximity

59
00:02:33,890 --> 00:02:35,640
 to you or those who are closest

60
00:02:35,640 --> 00:02:37,360
 to you emotionally.

61
00:02:37,360 --> 00:02:40,010
 This is because either group is the easiest to send

62
00:02:40,010 --> 00:02:41,800
 thoughts of friendliness to.

63
00:02:41,800 --> 00:02:45,780
 Say to yourself, may they be happy, may they find peace,

64
00:02:45,780 --> 00:02:47,840
 then gradually do this for beings

65
00:02:47,840 --> 00:02:51,100
 who are further and further away from you, either

66
00:02:51,100 --> 00:02:53,200
 physically or emotionally.

67
00:02:53,200 --> 00:02:56,360
 Physically you can start with beings in the same building,

68
00:02:56,360 --> 00:02:58,000
 then those in the same city,

69
00:02:58,000 --> 00:03:01,700
 country and finally all beings in the entire world.

70
00:03:01,700 --> 00:03:05,660
 You can also specify humans first, then do the same things

71
00:03:05,660 --> 00:03:08,360
 for animals, angels, ghosts,

72
00:03:08,360 --> 00:03:09,360
 etc.

73
00:03:09,360 --> 00:03:12,760
 Emotionally, after those who are close to you, focus on

74
00:03:12,760 --> 00:03:14,600
 those who you do not have either

75
00:03:14,600 --> 00:03:18,550
 positive or negative feelings towards and finally focus on

76
00:03:18,550 --> 00:03:20,600
 those who you do have negative

77
00:03:20,600 --> 00:03:24,840
 feelings towards or who have negative feelings towards you.

78
00:03:24,840 --> 00:03:27,710
 If you are not able to send love to any group, you can

79
00:03:27,710 --> 00:03:29,920
 return to focus on the previous group

80
00:03:29,920 --> 00:03:32,920
 until you are stronger.

81
00:03:32,920 --> 00:03:35,660
 Compassion is practiced in the same way as friendliness,

82
00:03:35,660 --> 00:03:36,960
 except their friendliness is

83
00:03:36,960 --> 00:03:40,000
 the wish for beings to be happy.

84
00:03:40,000 --> 00:03:43,440
 Compassion is wishing for them to be free from suffering.

85
00:03:43,440 --> 00:03:46,410
 Compassion is useful for when you feel sorry for someone or

86
00:03:46,410 --> 00:03:47,800
 sad about the suffering of

87
00:03:47,800 --> 00:03:51,500
 others, it is a way of calming your mind so that you are

88
00:03:51,500 --> 00:03:54,040
 not incapacitated by your sadness

89
00:03:54,040 --> 00:03:58,080
 so you can be a grounded support for those who are in

90
00:03:58,080 --> 00:03:58,880
 trouble.

91
00:03:58,880 --> 00:04:02,900
 Appreciation is appreciation for the attainments of other

92
00:04:02,900 --> 00:04:03,680
 beings.

93
00:04:03,680 --> 00:04:07,200
 This is often cultivated in ordinary life using the

94
00:04:07,200 --> 00:04:09,640
 expression sadhu which means good.

95
00:04:09,640 --> 00:04:13,690
 When we tell someone that we are happy for them or offer

96
00:04:13,690 --> 00:04:16,440
 congratulations, this is mudhita.

97
00:04:16,440 --> 00:04:20,230
 It is helpful in giving up jealousy and resentment, helping

98
00:04:20,230 --> 00:04:22,800
 let go of comparing oneself with others,

99
00:04:22,800 --> 00:04:27,060
 wishing for the things they have or not wanting them to get

100
00:04:27,060 --> 00:04:28,880
 the things that we have.

101
00:04:28,880 --> 00:04:31,750
 Appreciation is another important quality of mind that

102
00:04:31,750 --> 00:04:33,360
 brings peace of mind and helps

103
00:04:33,360 --> 00:04:36,000
 us to practice meditation.

104
00:04:36,000 --> 00:04:39,960
 As for equanimity, there are two kinds, equanimity towards

105
00:04:39,960 --> 00:04:43,160
 beings and equanimity towards experiences.

106
00:04:43,160 --> 00:04:47,180
 In Vipassana meditation, we practice cultivating equanimity

107
00:04:47,180 --> 00:04:48,800
 towards experiences.

108
00:04:48,800 --> 00:04:52,210
 In the context of the other three qualities discussed here,

109
00:04:52,210 --> 00:04:54,080
 the related practice is cultivating

110
00:04:54,080 --> 00:04:58,930
 equanimity towards beings which involves an acknowledgement

111
00:04:58,930 --> 00:04:59,800
 of karma.

112
00:04:59,800 --> 00:05:02,960
 When you see beings suffering or benefiting and you remind

113
00:05:02,960 --> 00:05:05,040
 yourself, they are going according

114
00:05:05,040 --> 00:05:06,440
 to their karma.

115
00:05:06,440 --> 00:05:08,640
 This is cultivating equanimity.

116
00:05:08,640 --> 00:05:11,820
 It is a useful practice to do away with anger or sadness in

117
00:05:11,820 --> 00:05:13,800
 the face of perceived injustice

118
00:05:13,800 --> 00:05:16,520
 related to politics or economics.

119
00:05:16,520 --> 00:05:20,050
 When you see the disparity between the very rich and very

120
00:05:20,050 --> 00:05:21,880
 poor and you feel angry, reminding

121
00:05:21,880 --> 00:05:26,000
 yourself all beings go according to their karma can help

122
00:05:26,000 --> 00:05:28,020
 you keep a level head so you

123
00:05:28,020 --> 00:05:31,120
 are not consumed by negative emotions.

124
00:05:31,120 --> 00:05:34,880
 When we see beings in great states of loss, suffering or in

125
00:05:34,880 --> 00:05:37,160
 great states of power, influence

126
00:05:37,160 --> 00:05:40,700
 and affluence, we can remind ourselves that if these people

127
00:05:40,700 --> 00:05:42,480
 in the state of great affluence

128
00:05:42,480 --> 00:05:45,540
 continue to repress those who are impoverished, then they

129
00:05:45,540 --> 00:05:47,480
 themselves will be doomed to attain

130
00:05:47,480 --> 00:05:50,280
 the same state of poverty in the future.

131
00:05:50,280 --> 00:05:53,670
 It is a cycle of cause and effect where inevitably our

132
00:05:53,670 --> 00:05:55,960
 destiny reflects our actions.

133
00:05:55,960 --> 00:05:59,110
 When you think like this, you do away with any sense of

134
00:05:59,110 --> 00:06:00,040
 unfairness.

135
00:06:00,040 --> 00:06:03,690
 You are able to understand that life is ultimately fair and

136
00:06:03,690 --> 00:06:05,560
 there is no need to get angry at

137
00:06:05,560 --> 00:06:08,800
 those who oppress others as they are going to have to

138
00:06:08,800 --> 00:06:10,920
 suffer the same fate in the future

139
00:06:10,920 --> 00:06:13,740
 and the reason why oppressed beings suffer in the present

140
00:06:13,740 --> 00:06:14,960
 is because they have done it

141
00:06:14,960 --> 00:06:17,080
 to others in the past.

142
00:06:17,080 --> 00:06:20,050
 Reflecting in this way brings peace of mind and so it too

143
00:06:20,050 --> 00:06:23,560
 is a support for vipassana meditation.

144
00:06:23,560 --> 00:06:27,010
 Using vipassana meditation is likewise the best support for

145
00:06:27,010 --> 00:06:28,680
 developing all four states

146
00:06:28,680 --> 00:06:29,680
 of mind.

147
00:06:29,680 --> 00:06:33,740
 In vipassana meditation, one becomes equanimous towards all

148
00:06:33,740 --> 00:06:35,880
 things as perceptions of beings

149
00:06:35,880 --> 00:06:39,810
 become secondary to the primary perspective of reality as

150
00:06:39,810 --> 00:06:41,960
 moments of experience arising

151
00:06:41,960 --> 00:06:43,260
 and seizing.

152
00:06:43,260 --> 00:06:47,030
 As a result, you understand the experiences of other beings

153
00:06:47,030 --> 00:06:47,120
.

154
00:06:47,120 --> 00:06:50,100
 When they are suffering, you can appreciate what they are

155
00:06:50,100 --> 00:06:51,040
 going through.

156
00:06:51,040 --> 00:06:54,230
 Even when they are angry or upset or enraged in immoral

157
00:06:54,230 --> 00:06:56,360
 acts, you can understand them and

158
00:06:56,360 --> 00:06:59,460
 you can feel pity for them that they are stuck in a cycle

159
00:06:59,460 --> 00:07:01,320
 of karma that they cannot break

160
00:07:01,320 --> 00:07:04,400
 free from and rather than blame the individual, you

161
00:07:04,400 --> 00:07:07,000
 understand the cause to be the development

162
00:07:07,000 --> 00:07:10,910
 of habitual tendencies through ignorance, through simply

163
00:07:10,910 --> 00:07:12,960
 not knowing what they are doing.

164
00:07:12,960 --> 00:07:15,960
 When beings are suffering, you can empathize with them.

165
00:07:15,960 --> 00:07:19,040
 When beings are prosperous, you do not feel jealous.

166
00:07:19,040 --> 00:07:21,870
 You appreciate the nature of good qualities because you

167
00:07:21,870 --> 00:07:23,760
 know that they lead to happiness.

168
00:07:23,760 --> 00:07:26,980
 You are happy and content yourself so you do not feel the

169
00:07:26,980 --> 00:07:28,600
 need to compare yourself to

170
00:07:28,600 --> 00:07:32,770
 others and ultimately see other beings with equanimity, not

171
00:07:32,770 --> 00:07:34,560
 preferring to be with this

172
00:07:34,560 --> 00:07:36,380
 person or that person.

173
00:07:36,380 --> 00:07:39,900
 You accept reality for what it is and you realize that

174
00:07:39,900 --> 00:07:41,820
 partiality is not of any use

175
00:07:41,820 --> 00:07:46,560
 or benefit as it does not lead to real happiness.

176
00:07:46,560 --> 00:07:49,600
 If you want to develop a meditation based on loving

177
00:07:49,600 --> 00:07:51,880
 kindness or equanimity, you should

178
00:07:51,880 --> 00:07:55,620
 formulate as explained and cultivate the associated incl

179
00:07:55,620 --> 00:07:58,560
inations according to your own situation.

180
00:07:58,560 --> 00:08:01,920
 As mentioned, it can be based on those who are in physical

181
00:08:01,920 --> 00:08:03,560
 or emotional proximity but

182
00:08:03,560 --> 00:08:06,350
 it is something that should come naturally and something

183
00:08:06,350 --> 00:08:08,060
 that you should develop in tandem

184
00:08:08,060 --> 00:08:10,080
 with bhipassana meditation.

185
00:08:10,080 --> 00:08:13,350
 I would not really recommend spending too much of your time

186
00:08:13,350 --> 00:08:15,300
 in terms of developing intensive

187
00:08:15,300 --> 00:08:18,930
 meditation in this area simply because it is time consuming

188
00:08:18,930 --> 00:08:20,680
 and you would be much better

189
00:08:20,680 --> 00:08:24,240
 served by training in bhipassana meditation and when an

190
00:08:24,240 --> 00:08:26,440
 emotion becomes overwhelming, use

191
00:08:26,440 --> 00:08:29,500
 these practices as a support for your main meditation

192
00:08:29,500 --> 00:08:30,320
 practice.

193
00:08:30,320 --> 00:08:31,320
 Thank you.

194
00:08:31,320 --> 00:08:47,400
 [

