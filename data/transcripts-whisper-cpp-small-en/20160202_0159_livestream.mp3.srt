1
00:00:00,000 --> 00:00:03,920
 Good evening everyone.

2
00:00:03,920 --> 00:00:13,880
 We're broadcasting live February 1st, 2016.

3
00:00:13,880 --> 00:00:21,000
 Tonight we have a new guest.

4
00:00:21,000 --> 00:00:24,480
 Patrick is here from the US.

5
00:00:24,480 --> 00:00:28,990
 He's the first of our online meditators to finish the

6
00:00:28,990 --> 00:00:31,440
 online meditation course that we

7
00:00:31,440 --> 00:00:35,520
 offer and then continue it here.

8
00:00:35,520 --> 00:00:40,160
 So we'll see how that goes.

9
00:00:40,160 --> 00:00:42,600
 And Teemu is still here.

10
00:00:42,600 --> 00:00:48,360
 Just finished his second advanced review course, third

11
00:00:48,360 --> 00:00:51,160
 advanced review course.

12
00:00:51,160 --> 00:00:52,160
 Second review course.

13
00:00:52,160 --> 00:00:56,530
 He did a foundation course and he's done two advanced

14
00:00:56,530 --> 00:00:58,160
 review courses.

15
00:00:58,160 --> 00:01:00,160
 Still got one more.

16
00:01:00,160 --> 00:01:05,600
 Still got time for one more.

17
00:01:05,600 --> 00:01:17,610
 So this is what you call taking advantage of Buddhism for

18
00:01:17,610 --> 00:01:21,040
 the right purpose.

19
00:01:21,040 --> 00:01:23,170
 Taking advantage because that's the words that our quote

20
00:01:23,170 --> 00:01:23,520
 uses.

21
00:01:23,520 --> 00:01:25,520
 It talks about the advantage, right?

22
00:01:25,520 --> 00:01:28,480
 Is that the word it uses?

23
00:01:28,480 --> 00:01:29,480
 The advantage.

24
00:01:29,480 --> 00:01:32,240
 It's a strange word.

25
00:01:32,240 --> 00:01:48,880
 And benefit maybe is a better word, better translation.

26
00:01:48,880 --> 00:01:51,640
 I used to record my teacher's talks and one of the first

27
00:01:51,640 --> 00:01:52,800
 talks I recorded.

28
00:01:52,800 --> 00:01:57,120
 I think in fact the second or the third talk.

29
00:01:57,120 --> 00:01:58,120
 Because I got them in a list.

30
00:01:58,120 --> 00:02:01,120
 Number one, number two, number three, up to 138.

31
00:02:01,120 --> 00:02:06,380
 And then I gave them over to the monastery and they started

32
00:02:06,380 --> 00:02:07,720
 using them.

33
00:02:07,720 --> 00:02:08,720
 Number three I think.

34
00:02:08,720 --> 00:02:11,720
 The two are number three.

35
00:02:11,720 --> 00:02:27,600
 The teaching of the Buddha is like a tree, big tree.

36
00:02:27,600 --> 00:02:38,400
 That is, the Buddha means it's shady, it's cool, it has

37
00:02:38,400 --> 00:02:41,280
 cool shade.

38
00:02:41,280 --> 00:02:44,200
 And we all come under this great tree.

39
00:02:44,200 --> 00:02:51,200
 It's the tree of Buddhism.

40
00:02:51,200 --> 00:02:53,740
 And so we think to ourselves, what do we get out of

41
00:02:53,740 --> 00:02:54,520
 Buddhism?

42
00:02:54,520 --> 00:02:56,520
 What are we here for?

43
00:02:56,520 --> 00:03:11,480
 When Buddha talked about benefits like the parts of a tree.

44
00:03:11,480 --> 00:03:19,180
 And so some people when they go looking to cut down a tree,

45
00:03:19,180 --> 00:03:22,680
 come back with leaves.

46
00:03:22,680 --> 00:03:23,880
 Come back with leaves on the tree.

47
00:03:23,880 --> 00:03:28,200
 And if someone were to do this and come out and say, "Oh, I

48
00:03:28,200 --> 00:03:29,800
 went and got some.

49
00:03:29,800 --> 00:03:33,440
 I was looking for wood and I came back with leaves."

50
00:03:33,440 --> 00:03:35,000
 Anyone would think they're crazy.

51
00:03:35,000 --> 00:03:36,000
 They don't know.

52
00:03:36,000 --> 00:03:38,000
 They don't know the truth about trees.

53
00:03:38,000 --> 00:03:45,200
 They don't know what is useful about a tree.

54
00:03:45,200 --> 00:03:49,920
 The Buddha said this is like someone who comes to Buddhism.

55
00:03:49,920 --> 00:03:53,560
 Excuse me.

56
00:03:53,560 --> 00:03:59,980
 Comes to study Buddhism and becomes content with the

57
00:03:59,980 --> 00:04:01,080
 accommodations.

58
00:04:01,080 --> 00:04:04,880
 Don't know a better word for that.

59
00:04:04,880 --> 00:04:11,360
 But for monks it means, the word is the accoutrements, this

60
00:04:11,360 --> 00:04:13,160
 may be better.

61
00:04:13,160 --> 00:04:18,320
 But it means, for monks it means gain.

62
00:04:18,320 --> 00:04:24,240
 Sometimes when you become a monk, people support you.

63
00:04:24,240 --> 00:04:26,440
 They feed you, they give you clothing and so on.

64
00:04:26,440 --> 00:04:28,440
 They give you a hut to live in.

65
00:04:28,440 --> 00:04:30,440
 They give you medicines.

66
00:04:30,440 --> 00:04:34,040
 They give you the four requisites.

67
00:04:34,040 --> 00:04:36,180
 But for, for, for, for, for, for, for one of you monks

68
00:04:36,180 --> 00:04:37,360
 listening to this, I don't think

69
00:04:37,360 --> 00:04:38,360
 very few of you anyway.

70
00:04:38,360 --> 00:04:41,640
 I don't know if there's any monks listening.

71
00:04:41,640 --> 00:04:45,100
 If you're not a monastic, it doesn't really apply, but we

72
00:04:45,100 --> 00:04:46,880
 can talk about it in terms of

73
00:04:46,880 --> 00:04:51,720
 meditation center or even visiting monasteries.

74
00:04:51,720 --> 00:04:59,460
 You become attached to the comforts of the, of the, and the

75
00:04:59,460 --> 00:05:02,600
 beauty of the monastery when

76
00:05:02,600 --> 00:05:04,400
 people go to visit.

77
00:05:04,400 --> 00:05:07,630
 But when you come to stay, which is what we're interested

78
00:05:07,630 --> 00:05:09,760
 in, is you become comfortable with

79
00:05:09,760 --> 00:05:12,200
 the accommodations.

80
00:05:12,200 --> 00:05:15,600
 Maybe the food's really good.

81
00:05:15,600 --> 00:05:27,760
 Maybe the plush carpets or so on, nice seats, whatever.

82
00:05:27,760 --> 00:05:31,390
 Most meditators do get kind of lazy because of this,

83
00:05:31,390 --> 00:05:33,440
 because we offer a nice place to

84
00:05:33,440 --> 00:05:34,440
 stay.

85
00:05:34,440 --> 00:05:37,150
 And as a result, they don't get quite what they might if

86
00:05:37,150 --> 00:05:38,480
 they were living in the forest

87
00:05:38,480 --> 00:05:43,170
 and in hardship because, and they see this when they go

88
00:05:43,170 --> 00:05:43,560
 back.

89
00:05:43,560 --> 00:05:46,390
 They leave and they'll say things like they weren't

90
00:05:46,390 --> 00:05:47,520
 prepared for it.

91
00:05:47,520 --> 00:05:50,200
 And part of that is because the benefit that they got from

92
00:05:50,200 --> 00:05:51,720
 the meditation center, part

93
00:05:51,720 --> 00:05:56,510
 of the benefit was just from having a quiet place and it

94
00:05:56,510 --> 00:05:59,640
 makes you calm and you feel good.

95
00:05:59,640 --> 00:06:03,320
 Quiet place, nice people.

96
00:06:03,320 --> 00:06:05,520
 But that's not the real benefit of the meditation.

97
00:06:05,520 --> 00:06:08,160
 And so when they go back, they feel like betrayed.

98
00:06:08,160 --> 00:06:13,680
 They, hey, it just wore off when I went home.

99
00:06:13,680 --> 00:06:14,680
 That's important.

100
00:06:14,680 --> 00:06:18,480
 It's important not to get complacent.

101
00:06:18,480 --> 00:06:22,370
 Sometimes you've got to wonder whether it's good to provide

102
00:06:22,370 --> 00:06:24,240
 proper accommodations for

103
00:06:24,240 --> 00:06:25,240
 meditators.

104
00:06:25,240 --> 00:06:29,270
 Now when I started, back in the day, we always had these

105
00:06:29,270 --> 00:06:31,680
 stories, back in my day, my day

106
00:06:31,680 --> 00:06:32,680
 it wasn't that bad.

107
00:06:32,680 --> 00:06:33,680
 The food was really good.

108
00:06:33,680 --> 00:06:40,670
 But the accommodation was, we had to sleep on wooden floor

109
00:06:40,670 --> 00:06:43,440
 with a very thin mat.

110
00:06:43,440 --> 00:06:45,280
 And I remember it was very painful.

111
00:06:45,280 --> 00:06:47,280
 There was no question it was hard.

112
00:06:47,280 --> 00:06:51,160
 I don't know whether that helped me or not.

113
00:06:51,160 --> 00:06:52,160
 Certainly wasn't comfortable.

114
00:06:52,160 --> 00:06:56,040
 We didn't have any chairs to sit on or anything.

115
00:06:56,040 --> 00:06:59,240
 We didn't even have these kind of plush cushions.

116
00:06:59,240 --> 00:07:00,240
 These are plush.

117
00:07:00,240 --> 00:07:10,880
 They just have a very thin mat and it was wood.

118
00:07:10,880 --> 00:07:18,960
 Only cold water, no hot water showers, no heaters.

119
00:07:18,960 --> 00:07:22,200
 And it would be in winter when you go to Thailand.

120
00:07:22,200 --> 00:07:23,200
 It's winter.

121
00:07:23,200 --> 00:07:24,200
 So it gets down.

122
00:07:24,200 --> 00:07:26,860
 First year I was there when I did my course, it got down to

123
00:07:26,860 --> 00:07:27,800
 negative two.

124
00:07:27,800 --> 00:07:34,530
 Where I was it got down to maybe, not negative two, but

125
00:07:34,530 --> 00:07:35,640
 zero.

126
00:07:35,640 --> 00:07:44,400
 That's zero Canadian weather, not zero American weather.

127
00:07:44,400 --> 00:07:45,400
 Which is freezing, it got down.

128
00:07:45,400 --> 00:07:48,330
 And you know, it's actually not as cold as it is here, but

129
00:07:48,330 --> 00:07:49,480
 we had no heaters.

130
00:07:49,480 --> 00:07:54,480
 So it was very cold.

131
00:07:54,480 --> 00:08:01,200
 And then you compare that to monks living off in the forest

132
00:08:01,200 --> 00:08:03,200
 and hardship.

133
00:08:03,200 --> 00:08:05,400
 A lot of monks get comfortable.

134
00:08:05,400 --> 00:08:09,870
 But if you ever go and live off in the forest, sometimes

135
00:08:09,870 --> 00:08:11,640
 just in a straw hut.

136
00:08:11,640 --> 00:08:16,170
 One time I spent a few nights just on a mat in the forest,

137
00:08:16,170 --> 00:08:18,720
 sleeping on the mat, living

138
00:08:18,720 --> 00:08:19,720
 on the mat.

139
00:08:19,720 --> 00:08:23,640
 In the cold season, then there's not so many mosquitoes in

140
00:08:23,640 --> 00:08:24,720
 some places.

141
00:08:24,720 --> 00:08:28,880
 So you can actually do that.

142
00:08:28,880 --> 00:08:32,850
 Anyway this is not, this is, the Buddha said this is like

143
00:08:32,850 --> 00:08:33,720
 leaves.

144
00:08:33,720 --> 00:08:39,240
 If you get caught up in the requisites, or you get caught

145
00:08:39,240 --> 00:08:42,200
 up in enjoying the gains that

146
00:08:42,200 --> 00:08:47,640
 come from being a meditator or a monk.

147
00:08:47,640 --> 00:08:50,340
 You can get caught up in the peace of being in such a place

148
00:08:50,340 --> 00:08:50,600
.

149
00:08:50,600 --> 00:08:53,080
 You find that that doesn't last.

150
00:08:53,080 --> 00:08:57,930
 It's nice while you're here, but you go home and lose it

151
00:08:57,930 --> 00:08:58,640
 all.

152
00:08:58,640 --> 00:09:03,240
 The second thing, that, the second kind of person, they go

153
00:09:03,240 --> 00:09:05,320
 into the forest and looking

154
00:09:05,320 --> 00:09:07,320
 for wood and they come back with branches.

155
00:09:07,320 --> 00:09:10,990
 And while you think, well branches are kind of useful, but

156
00:09:10,990 --> 00:09:14,320
 you can't do much with them.

157
00:09:14,320 --> 00:09:17,560
 You can burn them.

158
00:09:17,560 --> 00:09:18,560
 That's not really wood.

159
00:09:18,560 --> 00:09:21,320
 It's not real wood.

160
00:09:21,320 --> 00:09:24,600
 Just branches.

161
00:09:24,600 --> 00:09:28,540
 So the Buddha said this is like a person who gets content

162
00:09:28,540 --> 00:09:29,920
 with morality.

163
00:09:29,920 --> 00:09:31,320
 They keep the precepts.

164
00:09:31,320 --> 00:09:33,000
 They keep the life.

165
00:09:33,000 --> 00:09:44,520
 So people, monks or lay people, they practice.

166
00:09:44,520 --> 00:09:46,080
 They live the life of a monastic.

167
00:09:46,080 --> 00:09:48,230
 You know, we don't have such a problem here because our

168
00:09:48,230 --> 00:09:51,240
 focus is much more on meditation.

169
00:09:51,240 --> 00:09:54,990
 In some monasteries you have monks who just spend their

170
00:09:54,990 --> 00:09:57,280
 time doing chores and living the

171
00:09:57,280 --> 00:10:01,040
 life and debating about rules and carrying out the rules,

172
00:10:01,040 --> 00:10:03,040
 worrying a lot about the rules.

173
00:10:03,040 --> 00:10:05,680
 The life, the lifestyle.

174
00:10:05,680 --> 00:10:10,320
 Some monasteries, that's practice for them.

175
00:10:10,320 --> 00:10:18,380
 They sleep all day and they clean. They spend hours going

176
00:10:18,380 --> 00:10:19,200
 on alms round.

177
00:10:19,200 --> 00:10:21,080
 Walk for hours on alms round.

178
00:10:21,080 --> 00:10:33,000
 Which can be meditated, but often isn't.

179
00:10:33,000 --> 00:10:35,700
 So if you get content just with the livelihood, the

180
00:10:35,700 --> 00:10:37,840
 lifestyle of a monasteries, that's just

181
00:10:37,840 --> 00:10:38,840
 branches.

182
00:10:38,840 --> 00:10:44,090
 A third type of person goes into the forest looking for

183
00:10:44,090 --> 00:10:48,040
 wood, looking for hardwood actually.

184
00:10:48,040 --> 00:10:51,820
 They're looking for the main wood, but they come back with

185
00:10:51,820 --> 00:10:52,400
 bark.

186
00:10:52,400 --> 00:10:56,000
 They take the bark of the tree and they say, "I found some

187
00:10:56,000 --> 00:10:58,520
 wood. I found the tree."

188
00:10:58,520 --> 00:11:05,320
 They come back and realize this also is pretty useless.

189
00:11:05,320 --> 00:11:11,840
 The bark of the tree, the Buddha said, is like

190
00:11:11,840 --> 00:11:14,880
 concentration, samatha.

191
00:11:14,880 --> 00:11:16,200
 When you calm the mind.

192
00:11:16,200 --> 00:11:19,120
 It doesn't just mean the calm of being in a nice place.

193
00:11:19,120 --> 00:11:22,520
 This means when you still your mind to the point that there

194
00:11:22,520 --> 00:11:24,240
's no thoughts and you focus

195
00:11:24,240 --> 00:11:32,200
 purely on one thing.

196
00:11:32,200 --> 00:11:34,560
 This again we don't spend so much time with, so we don't

197
00:11:34,560 --> 00:11:36,160
 have such problem.

198
00:11:36,160 --> 00:11:39,720
 Meditators will always fall into these states of calm.

199
00:11:39,720 --> 00:11:40,720
 They're not bad.

200
00:11:40,720 --> 00:11:42,120
 They're just not the goal.

201
00:11:42,120 --> 00:11:48,030
 If you think that's the goal, they're in for a rude

202
00:11:48,030 --> 00:11:53,600
 awakening when it goes away.

203
00:11:53,600 --> 00:11:56,230
 A forest type of person, and this is of more interest to us

204
00:11:56,230 --> 00:11:57,960
, is a type of person who gains

205
00:11:57,960 --> 00:11:58,960
 nyanandasana.

206
00:11:58,960 --> 00:12:03,000
 They gain knowledge and vision.

207
00:12:03,000 --> 00:12:07,460
 Couldn't be magical powers, but it could also be through

208
00:12:07,460 --> 00:12:08,480
 insight.

209
00:12:08,480 --> 00:12:09,960
 Then they become content with that.

210
00:12:09,960 --> 00:12:12,660
 You get this sometimes where a meditator goes partway

211
00:12:12,660 --> 00:12:14,520
 through the course and they think,

212
00:12:14,520 --> 00:12:16,640
 "Well, I gained something."

213
00:12:16,640 --> 00:12:19,960
 They feel like they're ready to go home.

214
00:12:19,960 --> 00:12:22,760
 Sometimes they figured out some problems in their lives and

215
00:12:22,760 --> 00:12:24,200
 they think they're ready.

216
00:12:24,200 --> 00:12:27,080
 You kind of feel sorry for them and there's not much you

217
00:12:27,080 --> 00:12:28,720
 can say because they don't know

218
00:12:28,720 --> 00:12:30,400
 what's up the road.

219
00:12:30,400 --> 00:12:33,120
 They've gone halfway down the road and they don't know what

220
00:12:33,120 --> 00:12:34,520
's up the head, so they think,

221
00:12:34,520 --> 00:12:36,520
 "Oh, it's just more of the same.

222
00:12:36,520 --> 00:12:37,520
 I've got enough."

223
00:12:37,520 --> 00:12:41,760
 They truly don't understand the benefits.

224
00:12:41,760 --> 00:12:47,270
 It's like when you're boiling water, when you're trying to

225
00:12:47,270 --> 00:12:48,720
 light a fire.

226
00:12:48,720 --> 00:12:52,880
 The olden days, of course, they were rubbed, they were

227
00:12:52,880 --> 00:12:55,120
 stick together or get a bow and

228
00:12:55,120 --> 00:12:59,080
 they have two pieces of wood rubbing together to get hot.

229
00:12:59,080 --> 00:13:01,740
 It's like letting it get hot and then saying, "Oh, that's

230
00:13:01,740 --> 00:13:02,520
 good enough.

231
00:13:02,520 --> 00:13:04,200
 If I continue, it's just going to get hotter."

232
00:13:04,200 --> 00:13:06,600
 Well, this is hot enough.

233
00:13:06,600 --> 00:13:12,240
 They've never seen fire before, so they don't know to keep

234
00:13:12,240 --> 00:13:13,480
 rubbing.

235
00:13:13,480 --> 00:13:15,860
 Some genius finally went far enough and realized, "Oh, it

236
00:13:15,860 --> 00:13:16,520
 changes."

237
00:13:16,520 --> 00:13:22,800
 When you get it hot enough, suddenly something changes.

238
00:13:22,800 --> 00:13:25,480
 Inside meditation is like that.

239
00:13:25,480 --> 00:13:29,360
 It's not just a steady increase in insight.

240
00:13:29,360 --> 00:13:32,780
 Eventually, you get to an aha moment where you finally let

241
00:13:32,780 --> 00:13:34,280
 go and then you get what's

242
00:13:34,280 --> 00:13:36,440
 called super mundane insight.

243
00:13:36,440 --> 00:13:41,240
 You see nirvana, nirvana for the first time.

244
00:13:41,240 --> 00:13:44,760
 It's categorically different.

245
00:13:44,760 --> 00:13:45,800
 That's what this quote says.

246
00:13:45,800 --> 00:13:51,560
 It says that that is the reason why we practice.

247
00:13:51,560 --> 00:13:55,930
 In our Buddhism class tonight, we were talking about nir

248
00:13:55,930 --> 00:13:56,640
vana.

249
00:13:56,640 --> 00:14:00,070
 He's trying to talk about Mahayana Buddhism, but it keeps

250
00:14:00,070 --> 00:14:02,120
 getting bogged down because everyone

251
00:14:02,120 --> 00:14:04,280
 has questions about the early stuff.

252
00:14:04,280 --> 00:14:08,270
 Tonight, he got into early Buddhist theory and couldn't get

253
00:14:08,270 --> 00:14:09,640
 past it because people had

254
00:14:09,640 --> 00:14:14,120
 all these questions about it, but it wasn't really what he

255
00:14:14,120 --> 00:14:16,040
 wanted to talk about.

256
00:14:16,040 --> 00:14:21,380
 The question about nirvana was, "If nirvana is uncondition

257
00:14:21,380 --> 00:14:24,280
ed, then how is it that the practice

258
00:14:24,280 --> 00:14:29,080
 leads to nirvana?"

259
00:14:29,080 --> 00:14:33,760
 He didn't answer it directly.

260
00:14:33,760 --> 00:14:35,400
 It's not a difficult question to answer.

261
00:14:35,400 --> 00:14:38,140
 It's an interesting question because this idea that

262
00:14:38,140 --> 00:14:39,880
 everything in samsara is based on

263
00:14:39,880 --> 00:14:42,200
 causes and conditions, so it's impermanent.

264
00:14:42,200 --> 00:14:45,680
 It's based on the power of its causes and conditions.

265
00:14:45,680 --> 00:14:46,680
 What about nirvana?

266
00:14:46,680 --> 00:14:49,640
 Isn't nirvana based on the power of its causes and

267
00:14:49,640 --> 00:14:50,760
 conditions?

268
00:14:50,760 --> 00:14:54,200
 The thing is, you don't cause nirvana.

269
00:14:54,200 --> 00:15:01,120
 You cause the cessation of everything else, basically.

270
00:15:01,120 --> 00:15:03,800
 When everything else is gone, that's called nirvana.

271
00:15:03,800 --> 00:15:06,640
 It's quite simple.

272
00:15:06,640 --> 00:15:09,600
 Nirvana is not hard to understand.

273
00:15:09,600 --> 00:15:20,200
 It's that state of everything else ceasing.

274
00:15:20,200 --> 00:15:22,760
 You don't actually create it.

275
00:15:22,760 --> 00:15:27,880
 You just practice to the point where there's no clinging.

276
00:15:27,880 --> 00:15:30,680
 The mind isn't craving, so it's not creating.

277
00:15:30,680 --> 00:15:31,760
 It's not producing.

278
00:15:31,760 --> 00:15:36,890
 When it's not producing, it slips out all of everything

279
00:15:36,890 --> 00:15:38,680
 else.

280
00:15:38,680 --> 00:15:46,800
 It disconnects from everything and all that's left.

281
00:15:46,800 --> 00:15:50,840
 What's left is called nirvana and nirvana.

282
00:15:50,840 --> 00:15:51,840
 That's the goal.

283
00:15:51,840 --> 00:15:54,120
 That's really the only reason.

284
00:15:54,120 --> 00:15:58,250
 Everything else, that's the only thing of true worth of

285
00:15:58,250 --> 00:15:58,520
 value.

286
00:15:58,520 --> 00:16:09,940
 Everything else has conventional value or conditional value

287
00:16:09,940 --> 00:16:10,440
.

288
00:16:10,440 --> 00:16:19,560
 Conditional doesn't last.

289
00:16:19,560 --> 00:16:23,380
 As my teacher said, you have to think about what you want

290
00:16:23,380 --> 00:16:24,760
 out of Buddhism.

291
00:16:24,760 --> 00:16:27,040
 What's the profit?

292
00:16:27,040 --> 00:16:29,810
 Look at your bank account at the end of the day and see

293
00:16:29,810 --> 00:16:31,160
 what you've gained.

294
00:16:31,160 --> 00:16:33,360
 Did you come out with just leaves?

295
00:16:33,360 --> 00:16:37,240
 Did you come out with branches?

296
00:16:37,240 --> 00:16:38,240
 Buddhism has it all.

297
00:16:38,240 --> 00:16:40,240
 Did you come out with bark?

298
00:16:40,240 --> 00:16:44,640
 Sorry, I didn't explain the simile fully.

299
00:16:44,640 --> 00:16:51,120
 The outer wood, sort of punky wood, soft wood.

300
00:16:51,120 --> 00:16:52,120
 That's nirvana.

301
00:16:52,120 --> 00:16:54,000
 That's knowledge.

302
00:16:54,000 --> 00:16:56,210
 When you start to see impermanent suffering and non-self,

303
00:16:56,210 --> 00:16:57,360
 when you start to understand

304
00:16:57,360 --> 00:17:01,150
 how your mind is working, you start to let go, that's the

305
00:17:01,150 --> 00:17:02,160
 outer wood.

306
00:17:02,160 --> 00:17:08,160
 But nirvana, nirvana, that's the heartwood.

307
00:17:08,160 --> 00:17:09,840
 So Buddhism has all of this.

308
00:17:09,840 --> 00:17:13,400
 It's up to us when we decide to get out of it.

309
00:17:13,400 --> 00:17:15,400
 The tree doesn't care.

310
00:17:15,400 --> 00:17:18,320
 It's not up to the tree what you get out of it.

311
00:17:18,320 --> 00:17:22,480
 It's up to you what you take away.

312
00:17:22,480 --> 00:17:24,960
 It's all there.

313
00:17:24,960 --> 00:17:29,920
 Some of it is not unattainable.

314
00:17:29,920 --> 00:17:32,160
 Just maybe harder to get at.

315
00:17:32,160 --> 00:17:39,680
 It's easier to just pick some leaves and go home, right?

316
00:17:39,680 --> 00:17:40,680
 That's our quote for tonight.

317
00:17:40,680 --> 00:17:41,680
 Thank you all for tuning in.

318
00:17:41,680 --> 00:17:42,680
 Have a good practice.

319
00:17:42,680 --> 00:17:42,680
 See you all soon.

320
00:17:42,680 --> 00:17:44,680
 1

321
00:17:44,680 --> 00:17:46,680
 1

