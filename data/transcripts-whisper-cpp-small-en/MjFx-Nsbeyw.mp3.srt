1
00:00:00,000 --> 00:00:05,840
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,840 --> 00:00:13,000
 Today we look at verse 174 which reads as follows.

3
00:00:13,000 --> 00:00:20,600
 Andabhutto ayangloko tanuketa vipassati sakunto jala-mutto

4
00:00:20,600 --> 00:00:26,520
 apo sagayagacchiti.

5
00:00:26,520 --> 00:00:31,640
 Which means blind andabhuta.

6
00:00:31,640 --> 00:00:34,800
 Blind is this world.

7
00:00:34,800 --> 00:00:36,920
 Few here see clearly.

8
00:00:36,920 --> 00:00:44,000
 Vipassati comes the same as vipassana means to see clearly.

9
00:00:44,000 --> 00:00:45,760
 Sakunojala-mutto vah.

10
00:00:45,760 --> 00:00:51,800
 Just like birds that free themselves from a net, not so

11
00:00:51,800 --> 00:00:53,640
 many.

12
00:00:53,640 --> 00:00:56,280
 A bird caught up in a net is well caught.

13
00:00:56,280 --> 00:00:58,240
 Few are those who go to heaven.

14
00:00:58,240 --> 00:01:02,940
 As few as those birds that are able to free themselves from

15
00:01:02,940 --> 00:01:06,040
 a net.

16
00:01:06,040 --> 00:01:11,000
 So this story, this verse was told in regards to another

17
00:01:11,000 --> 00:01:13,480
 fairly famous story.

18
00:01:13,480 --> 00:01:21,480
 It's the story of the Pesakara Dita, the daughter of a we

19
00:01:21,480 --> 00:01:22,000
aver.

20
00:01:22,000 --> 00:01:27,000
 A weaver's daughter.

21
00:01:27,000 --> 00:01:30,560
 But it was dwelling at Alavi.

22
00:01:30,560 --> 00:01:38,750
 And in Alavi he was invited to, for the meal, and he was

23
00:01:38,750 --> 00:01:40,760
 invited to teach.

24
00:01:40,760 --> 00:01:49,480
 So after eating he began to teach the Dhammapada.

25
00:01:49,480 --> 00:01:51,970
 He happened to be teaching on that day because it was a

26
00:01:51,970 --> 00:01:53,520
 general audience, I guess.

27
00:01:53,520 --> 00:01:55,320
 He was teaching on death.

28
00:01:55,320 --> 00:01:57,520
 So he taught them.

29
00:01:57,520 --> 00:02:04,360
 Practice meditation on death.

30
00:02:04,360 --> 00:02:08,080
 My life is unstable.

31
00:02:08,080 --> 00:02:09,080
 Unstable.

32
00:02:09,080 --> 00:02:15,620
 As you don't know, it's not going to last forever and death

33
00:02:15,620 --> 00:02:18,520
 could come at any time.

34
00:02:18,520 --> 00:02:23,560
 My death is certain.

35
00:02:23,560 --> 00:02:30,880
 In the end for me I must die.

36
00:02:30,880 --> 00:02:34,640
 So you have people actually meditate on this.

37
00:02:34,640 --> 00:02:38,180
 Of course you can do it in English because the Pali's

38
00:02:38,180 --> 00:02:40,520
 probably not very meaningful for

39
00:02:40,520 --> 00:02:43,800
 most people.

40
00:02:43,800 --> 00:02:45,720
 Death is the end of my life.

41
00:02:45,720 --> 00:02:47,360
 My life is uncertain.

42
00:02:47,360 --> 00:02:51,600
 Death is certain.

43
00:02:51,600 --> 00:02:54,000
 And he taught, he said, "Practice this way.

44
00:02:54,000 --> 00:02:56,840
 Cultivate this."

45
00:02:56,840 --> 00:03:00,360
 And then he left.

46
00:03:00,360 --> 00:03:08,450
 And in the entire audience, I guess it was a large audience

47
00:03:08,450 --> 00:03:12,960
, only one person was affected

48
00:03:12,960 --> 00:03:14,560
 by it, was moved by it.

49
00:03:14,560 --> 00:03:16,720
 Everybody else went back to their daily life.

50
00:03:16,720 --> 00:03:20,200
 That's interesting.

51
00:03:20,200 --> 00:03:23,560
 Back to the same old thing.

52
00:03:23,560 --> 00:03:28,440
 But there was one young girl and she was the daughter of a

53
00:03:28,440 --> 00:03:29,440
 weaver.

54
00:03:29,440 --> 00:03:31,320
 That's the story.

55
00:03:31,320 --> 00:03:37,320
 That's the story about her.

56
00:03:37,320 --> 00:03:40,840
 And she took it quite seriously.

57
00:03:40,840 --> 00:03:44,430
 And so from then on, she remembered the Buddha's words and

58
00:03:44,430 --> 00:03:46,320
 she would repeat that to herself.

59
00:03:46,320 --> 00:03:54,440
 "Dhuvang ji whit bai ji whit dang duvang marnam."

60
00:03:54,440 --> 00:03:59,400
 Every day, constantly, for three years.

61
00:03:59,400 --> 00:04:02,970
 And it so happened that three years later, the Buddha, and

62
00:04:02,970 --> 00:04:04,560
 they say that every night

63
00:04:04,560 --> 00:04:10,230
 the Buddha would enter into some kind of higher state of

64
00:04:10,230 --> 00:04:12,880
 mind and send his mind out and get

65
00:04:12,880 --> 00:04:15,960
 a sense of who was able to understand his teachings.

66
00:04:15,960 --> 00:04:18,440
 Every night he would do this.

67
00:04:18,440 --> 00:04:23,210
 Three years later, on one night, the Buddha was reflecting

68
00:04:23,210 --> 00:04:25,640
 and he thought about this girl

69
00:04:25,640 --> 00:04:32,720
 and he thought, "By now she is well able to understand my

70
00:04:32,720 --> 00:04:34,640
 teaching."

71
00:04:34,640 --> 00:04:35,960
 And so he went back to Alavi.

72
00:04:35,960 --> 00:04:38,460
 Of course, Alavi wasn't where he was living most of the

73
00:04:38,460 --> 00:04:38,880
 time.

74
00:04:38,880 --> 00:04:42,120
 Most of the time he was in Tjetoana.

75
00:04:42,120 --> 00:04:48,160
 But he traveled back to wherever Alavi is.

76
00:04:48,160 --> 00:04:50,800
 And again, he was invited for a meal.

77
00:04:50,800 --> 00:04:53,320
 People remembered and thought, "Oh, this is a wise man."

78
00:04:53,320 --> 00:04:55,960
 They hadn't done anything about his teaching.

79
00:04:55,960 --> 00:05:01,720
 But they thought, "Oh yes, well, what he says is true."

80
00:05:01,720 --> 00:05:07,530
 People who believe in, have faith in teaching, it's a lot

81
00:05:07,530 --> 00:05:10,640
 more than those who actually put

82
00:05:10,640 --> 00:05:17,880
 into practice as far as religion goes.

83
00:05:17,880 --> 00:05:20,780
 So they had faith in him and confidence and appreciation

84
00:05:20,780 --> 00:05:22,400
 and so they invited him and again

85
00:05:22,400 --> 00:05:24,400
 invited him to teach.

86
00:05:24,400 --> 00:05:27,520
 Buddha sat down and he didn't teach.

87
00:05:27,520 --> 00:05:30,880
 He just sat there.

88
00:05:30,880 --> 00:05:37,470
 They invited for a meal and then after the meal was over,

89
00:05:37,470 --> 00:05:40,240
 they just sat there.

90
00:05:40,240 --> 00:05:44,950
 So it turns out this young girl who had diligently

91
00:05:44,950 --> 00:05:49,080
 practiced his teaching for three years, turns

92
00:05:49,080 --> 00:05:52,440
 out that she heard that the Buddha was coming and she was

93
00:05:52,440 --> 00:05:53,520
 very excited.

94
00:05:53,520 --> 00:05:57,720
 But then her father said to her, "I need more.

95
00:05:57,720 --> 00:06:01,440
 I need you to fill this shuttle, they call it.

96
00:06:01,440 --> 00:06:04,730
 This shuttle is the thing you send back and forth between

97
00:06:04,730 --> 00:06:05,480
 the loom.

98
00:06:05,480 --> 00:06:07,920
 I need you to fill it up with thread."

99
00:06:07,920 --> 00:06:13,480
 And she thought, "Oh no, I'll miss this Herman.

100
00:06:13,480 --> 00:06:17,680
 If I don't, my father will beat me."

101
00:06:17,680 --> 00:06:23,440
 The commentary says her father wasn't a nice guy, I guess.

102
00:06:23,440 --> 00:06:26,480
 Or maybe she was, I don't know.

103
00:06:26,480 --> 00:06:28,870
 Anyway this is what she thought to herself, that he would

104
00:06:28,870 --> 00:06:29,440
 beat her.

105
00:06:29,440 --> 00:06:30,760
 So I better go and fill it up.

106
00:06:30,760 --> 00:06:32,480
 So she went to the workshop.

107
00:06:32,480 --> 00:06:34,000
 I guess was far away from the house.

108
00:06:34,000 --> 00:06:38,460
 She had to walk some ways through the town, get to the

109
00:06:38,460 --> 00:06:41,440
 workshop and fill up the shuttle.

110
00:06:41,440 --> 00:06:45,080
 And on her way back she took a detour.

111
00:06:45,080 --> 00:06:46,800
 But this took her some time.

112
00:06:46,800 --> 00:06:48,800
 And during this time the Buddha waited for her.

113
00:06:48,800 --> 00:06:52,640
 He thought, "I came all this way just to teach her because

114
00:06:52,640 --> 00:06:54,600
 these other people, well we know

115
00:06:54,600 --> 00:06:59,200
 what happens when they hear the good teaching."

116
00:06:59,200 --> 00:07:01,600
 So he waited.

117
00:07:01,600 --> 00:07:04,350
 And it says that everyone else, when he didn't speak, no

118
00:07:04,350 --> 00:07:06,080
 one else spoke as well because when

119
00:07:06,080 --> 00:07:11,040
 the Buddha is sitting quiet nobody dares to speak.

120
00:07:11,040 --> 00:07:13,680
 So they all just waited.

121
00:07:13,680 --> 00:07:18,830
 Finally this young daughter of a weaver arrives at the

122
00:07:18,830 --> 00:07:22,120
 congregation, stands in the back and

123
00:07:22,120 --> 00:07:27,070
 looks and tries to get an idea of what the Buddha is

124
00:07:27,070 --> 00:07:28,480
 teaching.

125
00:07:28,480 --> 00:07:36,080
 The Buddha turns and he looks at her.

126
00:07:36,080 --> 00:07:40,760
 And she knows right away the Buddha wants to talk to me.

127
00:07:40,760 --> 00:07:43,850
 I guess everyone else probably saw this and turned to see,

128
00:07:43,850 --> 00:07:45,320
 "Oh, who is this person the

129
00:07:45,320 --> 00:07:48,680
 Buddha is looking at?"

130
00:07:48,680 --> 00:07:58,200
 Either way, so she walks up to where the Buddha is sitting.

131
00:07:58,200 --> 00:07:59,720
 She stands right in front of him.

132
00:07:59,720 --> 00:08:00,720
 Maybe sits down.

133
00:08:00,720 --> 00:08:04,000
 No, what does she do?

134
00:08:04,000 --> 00:08:08,760
 No, she stands.

135
00:08:08,760 --> 00:08:10,520
 She stands in front of her.

136
00:08:10,520 --> 00:08:13,000
 The Buddha was probably on a higher seat.

137
00:08:13,000 --> 00:08:15,320
 They tended to put religious teachers up a bit high.

138
00:08:15,320 --> 00:08:21,400
 So she stood in front of where he was sitting.

139
00:08:21,400 --> 00:08:24,960
 And the Buddha asks her four questions.

140
00:08:24,960 --> 00:08:32,840
 "Kuto kumari ke young girl.

141
00:08:32,840 --> 00:08:40,920
 Kuto aagachasi, where are you coming from?"

142
00:08:40,920 --> 00:08:46,560
 And she replies, "Najanami bhante.

143
00:08:46,560 --> 00:08:49,840
 Katagamisa si, where are you going?"

144
00:08:49,840 --> 00:08:54,760
 "Najanami bhante, I don't know Venerable Sir."

145
00:08:54,760 --> 00:08:59,200
 "Najanasi, you don't know?"

146
00:08:59,200 --> 00:09:04,000
 "Najanami bhante, I know Venerable Sir."

147
00:09:04,000 --> 00:09:14,840
 "Najanasi, najanami bhante, I don't know Venerable Sir."

148
00:09:14,840 --> 00:09:18,450
 And the whole crowd starts to get quite upset and says, "

149
00:09:18,450 --> 00:09:19,880
Who is this girl?

150
00:09:19,880 --> 00:09:21,040
 What kind of answers are these?"

151
00:09:21,040 --> 00:09:22,840
 The Buddha asked her, "Where is she coming from?"

152
00:09:22,840 --> 00:09:24,000
 She says, "I don't know."

153
00:09:24,000 --> 00:09:25,480
 "You know where you're coming from?

154
00:09:25,480 --> 00:09:27,080
 You're coming from your father's workshop."

155
00:09:27,080 --> 00:09:31,040
 "You've got this shuttle here clearly here.

156
00:09:31,040 --> 00:09:33,560
 Where you ask where you're going, you say you don't know,

157
00:09:33,560 --> 00:09:34,760
 you're going home.

158
00:09:34,760 --> 00:09:38,760
 You're going to, obviously you know where you're going."

159
00:09:38,760 --> 00:09:41,080
 And then what's this, "Don't you know?"

160
00:09:41,080 --> 00:09:42,920
 Buddha challenges you and you say you know.

161
00:09:42,920 --> 00:09:47,240
 And then he says, asks you to confirm that you know and you

162
00:09:47,240 --> 00:09:48,920
 say you don't know.

163
00:09:48,920 --> 00:09:49,920
 Don't you know?

164
00:09:49,920 --> 00:09:50,920
 I do know.

165
00:09:50,920 --> 00:09:51,920
 Do you know?

166
00:09:51,920 --> 00:09:52,920
 I don't know."

167
00:09:52,920 --> 00:09:53,920
 What a crazy young girl.

168
00:09:53,920 --> 00:09:54,920
 And that little brat.

169
00:09:54,920 --> 00:10:04,720
 And when the Buddha holds up his hand or something, he gets

170
00:10:04,720 --> 00:10:08,000
 them to be quiet.

171
00:10:08,000 --> 00:10:09,000
 And he asked her, "What did you mean?

172
00:10:09,000 --> 00:10:11,000
 What did you mean by those answers?"

173
00:10:11,000 --> 00:10:13,880
 And she says, "Well, I know.

174
00:10:13,880 --> 00:10:16,960
 Buddhas don't just ask idle questions.

175
00:10:16,960 --> 00:10:23,340
 Clearly you're not asking about me coming, you know where I

176
00:10:23,340 --> 00:10:25,360
'm coming from.

177
00:10:25,360 --> 00:10:27,160
 And I know how you teach.

178
00:10:27,160 --> 00:10:31,840
 I've heard you teach before.

179
00:10:31,840 --> 00:10:33,920
 And so I know when you ask, where are you coming from?

180
00:10:33,920 --> 00:10:37,910
 It's about reality, about life, and I don't know where I'm

181
00:10:37,910 --> 00:10:39,040
 coming from.

182
00:10:39,040 --> 00:10:40,680
 Before I was born, that's all I have.

183
00:10:40,680 --> 00:10:42,440
 That's the only memory I have.

184
00:10:42,440 --> 00:10:45,960
 I don't even remember my birth.

185
00:10:45,960 --> 00:10:49,560
 But before that, no idea.

186
00:10:49,560 --> 00:10:50,960
 And where am I going?

187
00:10:50,960 --> 00:10:53,240
 I also don't know.

188
00:10:53,240 --> 00:11:00,040
 It depends very much on my state of mind, as you've taught.

189
00:11:00,040 --> 00:11:03,440
 And then the other two, when you ask me, "Don't I know?"

190
00:11:03,440 --> 00:11:06,200
 I have to admit that I do know that I will die.

191
00:11:06,200 --> 00:11:10,280
 So I do know something.

192
00:11:10,280 --> 00:11:13,720
 But then when you challenge me, do I really know?

193
00:11:13,720 --> 00:11:16,450
 And actually about my death, even about my death, I don't

194
00:11:16,450 --> 00:11:16,920
 know.

195
00:11:16,920 --> 00:11:18,320
 I don't know where it's going to be.

196
00:11:18,320 --> 00:11:20,720
 I don't know when it's going to be.

197
00:11:20,720 --> 00:11:23,880
 I don't know why it's going to happen.

198
00:11:23,880 --> 00:11:25,520
 I don't know where they're going to throw my body.

199
00:11:25,520 --> 00:11:27,120
 I don't know where my mind's going to go.

200
00:11:27,120 --> 00:11:29,760
 I don't know much about anything at all.

201
00:11:29,760 --> 00:11:33,640
 Nothing is certain.

202
00:11:33,640 --> 00:11:36,840
 And the Buddha says, "Sadhu."

203
00:11:36,840 --> 00:11:37,840
 Does he hold up his hand?

204
00:11:37,840 --> 00:11:39,840
 Sometimes he holds up his hand.

205
00:11:39,840 --> 00:11:42,840
 "Sadhu karang dattva."

206
00:11:42,840 --> 00:11:44,840
 He said, "Sadhu" three times.

207
00:11:44,840 --> 00:11:50,840
 Oh no, he says, "Sadhu" after everyone, every time.

208
00:11:50,840 --> 00:11:56,200
 "Sadhu karang dattva."

209
00:11:56,200 --> 00:11:57,640
 And then he asks about the second question.

210
00:11:57,640 --> 00:12:01,640
 "What do you mean by this one?"

211
00:12:01,640 --> 00:12:10,280
 And then he turns to the people and he says, "All of you,

212
00:12:10,280 --> 00:12:11,960
 all of you had no idea.

213
00:12:11,960 --> 00:12:15,000
 All of you, you couldn't understand my questions, just like

214
00:12:15,000 --> 00:12:18,760
 you couldn't understand my teaching

215
00:12:18,760 --> 00:12:19,760
 because of your delusion."

216
00:12:19,760 --> 00:12:21,800
 What does he say?

217
00:12:21,800 --> 00:12:26,920
 Because you're blind.

218
00:12:26,920 --> 00:12:31,160
 And you scolded her because you're blind.

219
00:12:31,160 --> 00:12:47,450
 And only those who have the eye of wisdom, only they can

220
00:12:47,450 --> 00:12:48,920
 see.

221
00:12:48,920 --> 00:12:54,960
 And then he pronounced the verse.

222
00:12:54,960 --> 00:12:57,280
 So there's a few lessons here, or two lessons I guess.

223
00:12:57,280 --> 00:13:00,750
 The lesson of the story, I mean the big lesson that the

224
00:13:00,750 --> 00:13:02,840
 story gives us is about mindfulness

225
00:13:02,840 --> 00:13:05,140
 of death, which is a curious thing as to why the Buddha

226
00:13:05,140 --> 00:13:06,640
 taught mindfulness of death.

227
00:13:06,640 --> 00:13:12,920
 You might ask, "Is that really the Buddha's teaching?"

228
00:13:12,920 --> 00:13:15,560
 In fact, mindfulness of death is quite useful.

229
00:13:15,560 --> 00:13:19,750
 It's a very useful gateway to bring people to understand

230
00:13:19,750 --> 00:13:21,600
 about impermanence.

231
00:13:21,600 --> 00:13:25,740
 I mean, it is a teaching on impermanence.

232
00:13:25,740 --> 00:13:29,630
 We live our lives often as though we're going to live

233
00:13:29,630 --> 00:13:30,160
 forever.

234
00:13:30,160 --> 00:13:32,560
 Maybe we don't have the view I'm going to live forever, but

235
00:13:32,560 --> 00:13:35,200
 our short-sightedness and

236
00:13:35,200 --> 00:13:40,020
 our narrow-mindedness is such that everything we do is only

237
00:13:40,020 --> 00:13:42,760
 for our present gratification,

238
00:13:42,760 --> 00:13:57,390
 is for building up results that are in conflict with our

239
00:13:57,390 --> 00:13:59,720
 death, such that when we die we're

240
00:13:59,720 --> 00:14:03,080
 going to lose it all.

241
00:14:03,080 --> 00:14:06,400
 Suppose you become smart, you learn a lot.

242
00:14:06,400 --> 00:14:09,280
 You forget it all when you die.

243
00:14:09,280 --> 00:14:14,400
 Suppose you amass all sorts of wealth and security, this

244
00:14:14,400 --> 00:14:17,360
 house, and I look at my security

245
00:14:17,360 --> 00:14:22,310
 and you put money in the bank and you've got a good

246
00:14:22,310 --> 00:14:24,040
 retirement.

247
00:14:24,040 --> 00:14:27,980
 It's not wrong to do that, but if that's all you're living

248
00:14:27,980 --> 00:14:29,920
 for, it's in conflict with the

249
00:14:29,920 --> 00:14:35,530
 fact that you're going to lose all that security and have

250
00:14:35,530 --> 00:14:37,600
 to start all over.

251
00:14:37,600 --> 00:14:40,670
 That's your goal, to start over and do this again and again

252
00:14:40,670 --> 00:14:40,960
.

253
00:14:40,960 --> 00:14:44,630
 That's one thing, but mostly we do it with the idea that it

254
00:14:44,630 --> 00:14:47,040
's going to bring us satisfaction,

255
00:14:47,040 --> 00:14:49,360
 which of course it can do.

256
00:14:49,360 --> 00:14:55,600
 It's only temporary.

257
00:14:55,600 --> 00:14:58,160
 The mindfulness of death is a great, oh, eye-opener.

258
00:14:58,160 --> 00:15:00,560
 It reminds you that there's something more.

259
00:15:00,560 --> 00:15:04,620
 If you're going to do all this, you're missing out on the

260
00:15:04,620 --> 00:15:07,160
 most important aspect of reality.

261
00:15:07,160 --> 00:15:11,240
 Life is much more than being a human in this life.

262
00:15:11,240 --> 00:15:14,920
 It's much more than this person that I've been born as.

263
00:15:14,920 --> 00:15:17,740
 We live our lives as though we are this person, which is

264
00:15:17,740 --> 00:15:20,040
 kind of funny from a Buddhist perspective

265
00:15:20,040 --> 00:15:23,960
 because it's more like a role we're playing for a short

266
00:15:23,960 --> 00:15:24,680
 time.

267
00:15:24,680 --> 00:15:27,040
 We've given us this funny name.

268
00:15:27,040 --> 00:15:33,180
 We've got this funny shaped body with all these funny

269
00:15:33,180 --> 00:15:34,920
 things in it.

270
00:15:34,920 --> 00:15:37,840
 Not so funny, actually, a lot of suffering and stress.

271
00:15:37,840 --> 00:15:46,200
 But it's not us, it's just this short, temporary occupation

272
00:15:46,200 --> 00:15:46,760
.

273
00:15:46,760 --> 00:15:53,480
 In the whole scheme of things, it's only a very short stint

274
00:15:53,480 --> 00:15:56,320
 at being this person.

275
00:15:56,320 --> 00:15:57,320
 So that's the first.

276
00:15:57,320 --> 00:16:00,040
 It's useful if you want to talk to people about Buddhism.

277
00:16:00,040 --> 00:16:04,790
 I mean, one of the first things you can do for people who

278
00:16:04,790 --> 00:16:06,120
 are...

279
00:16:06,120 --> 00:16:08,750
 It doesn't mean you to Buddhism, but people who aren't

280
00:16:08,750 --> 00:16:10,120
 thinking spiritually.

281
00:16:10,120 --> 00:16:12,910
 To get them to start thinking spiritually is that, well,

282
00:16:12,910 --> 00:16:15,040
 you know, you're going to die.

283
00:16:15,040 --> 00:16:16,640
 Are you ready for death?

284
00:16:16,640 --> 00:16:20,000
 Are you living in accordance with this fact?

285
00:16:20,000 --> 00:16:30,260
 Or are you living ignorant of the fact that the things you

286
00:16:30,260 --> 00:16:31,520
 do might be taken away from

287
00:16:31,520 --> 00:16:34,080
 you at any time?

288
00:16:34,080 --> 00:16:40,590
 And that the things you obtain or attain are going to be

289
00:16:40,590 --> 00:16:44,600
 obliterated or meaningless.

290
00:16:44,600 --> 00:16:49,550
 The lesson from the verse is a little bit different. It

291
00:16:49,550 --> 00:16:51,480
 talks about blindness.

292
00:16:51,480 --> 00:16:53,600
 So I think there's two things we get from the verse.

293
00:16:53,600 --> 00:16:58,400
 The first is a sense of urgency, a reminder that Buddhism

294
00:16:58,400 --> 00:17:00,720
 is not a simple teaching.

295
00:17:00,720 --> 00:17:04,440
 It's fine if you are a sort of person who...

296
00:17:04,440 --> 00:17:09,200
 I mean, it isn't evil or wrong to be a person who appreci

297
00:17:09,200 --> 00:17:11,920
ates the Buddha's teachings and

298
00:17:11,920 --> 00:17:14,850
 reveres the Buddha and calls yourself a Buddhist because

299
00:17:14,850 --> 00:17:17,480
 you have such faith in the Buddha.

300
00:17:17,480 --> 00:17:21,120
 I mean, there's wholesomeness there.

301
00:17:21,120 --> 00:17:24,960
 But that's not really...

302
00:17:24,960 --> 00:17:28,160
 It's certainly not all the Buddha taught.

303
00:17:28,160 --> 00:17:32,760
 And it's not even necessarily enough to help you to go to

304
00:17:32,760 --> 00:17:36,600
 heaven or to go to a good place,

305
00:17:36,600 --> 00:17:37,600
 to have a good future.

306
00:17:37,600 --> 00:17:40,440
 There are Buddhists who do very rotten things.

307
00:17:40,440 --> 00:17:46,000
 Nowadays you hear a lot of rotten things that Buddhists do.

308
00:17:46,000 --> 00:17:49,360
 There's apparently a lot of violence in Buddhist countries

309
00:17:49,360 --> 00:17:52,840
 these days, which is very bizarre.

310
00:17:52,840 --> 00:17:53,840
 Not totally.

311
00:17:53,840 --> 00:17:56,480
 If you've been to these countries, you understand how

312
00:17:56,480 --> 00:17:59,160
 religion becomes institutionalized and

313
00:17:59,160 --> 00:18:02,680
 then it becomes corrupted.

314
00:18:02,680 --> 00:18:06,080
 So that it's much more important to hold on to your

315
00:18:06,080 --> 00:18:08,120
 institutions than to actually adhere

316
00:18:08,120 --> 00:18:15,520
 to the teachings.

317
00:18:15,520 --> 00:18:19,320
 So a reminder that most people are blind.

318
00:18:19,320 --> 00:18:25,160
 Fewer those who actually see clearly.

319
00:18:25,160 --> 00:18:33,000
 That it's not enough to just be an average ordinary person.

320
00:18:33,000 --> 00:18:36,400
 Buddhism isn't about living your life in an ordinary way.

321
00:18:36,400 --> 00:18:39,360
 There's much more, and then coming to see clearly is really

322
00:18:39,360 --> 00:18:40,600
 what needs to be done.

323
00:18:40,600 --> 00:18:43,880
 If you want to go to a good place, the only real reass

324
00:18:43,880 --> 00:18:45,960
urance is to have clear insight

325
00:18:45,960 --> 00:18:49,000
 into how things work.

326
00:18:49,000 --> 00:18:53,550
 Without that insight, you're in so much danger of doing the

327
00:18:53,550 --> 00:18:54,840
 wrong thing.

328
00:18:54,840 --> 00:18:57,310
 Building the wrong habits, of concentrating your mind in

329
00:18:57,310 --> 00:18:58,600
 the wrong way because of your

330
00:18:58,600 --> 00:19:01,600
 ignorance.

331
00:19:01,600 --> 00:19:05,070
 So it's not about teaching you or believing in certain

332
00:19:05,070 --> 00:19:06,200
 teachings.

333
00:19:06,200 --> 00:19:09,920
 It's about seeing for yourself.

334
00:19:09,920 --> 00:19:12,250
 And the other side of that is for those of us who are

335
00:19:12,250 --> 00:19:14,400
 practicing, this is an encouragement.

336
00:19:14,400 --> 00:19:16,920
 It's an encouragement to this young girl.

337
00:19:16,920 --> 00:19:20,990
 It's an encouragement to everyone who undertakes to

338
00:19:20,990 --> 00:19:23,680
 practice insight meditation.

339
00:19:23,680 --> 00:19:28,490
 You're doing something that's quite rare, quite valuable,

340
00:19:28,490 --> 00:19:31,440
 quite worthwhile, quite powerful.

341
00:19:31,440 --> 00:19:38,770
 It's the power to bring you a good future, a guarantee of

342
00:19:38,770 --> 00:19:43,120
 happiness, of peace, freedom

343
00:19:43,120 --> 00:19:45,840
 from suffering.

344
00:19:45,840 --> 00:19:49,360
 So at the end of the verse, the young girl became a sotap

345
00:19:49,360 --> 00:19:49,960
ana.

346
00:19:49,960 --> 00:19:52,720
 She was primed for it, quite a special person, just

347
00:19:52,720 --> 00:19:55,160
 standing there listening, standing up,

348
00:19:55,160 --> 00:19:56,160
 she became a sotapana.

349
00:19:56,160 --> 00:20:00,220
 This is what the commentary says, believe it or not, as you

350
00:20:00,220 --> 00:20:00,880
 like.

351
00:20:00,880 --> 00:20:02,680
 And then she apparently went home.

352
00:20:02,680 --> 00:20:05,920
 And when she got home, her father was asleep.

353
00:20:05,920 --> 00:20:07,320
 And so she closed the door.

354
00:20:07,320 --> 00:20:10,680
 She banged something.

355
00:20:10,680 --> 00:20:16,320
 And he woke up and he was startled.

356
00:20:16,320 --> 00:20:19,430
 He jerked something and the loom went stabbing something

357
00:20:19,430 --> 00:20:21,320
 and the spindle, maybe, I don't

358
00:20:21,320 --> 00:20:27,360
 know, whatever, went stabbing into her chest and she died.

359
00:20:27,360 --> 00:20:32,990
 And immediately was born in one of the high heavens, tusita

360
00:20:32,990 --> 00:20:36,480
 heavens, if you're interested.

361
00:20:36,480 --> 00:20:41,400
 Her father was so upset.

362
00:20:41,400 --> 00:20:43,840
 He went to see the Buddha, apparently.

363
00:20:43,840 --> 00:20:47,400
 Having gone to see the Buddha, he became a monk.

364
00:20:47,400 --> 00:20:51,230
 Buddha taught him this teaching of, because he was so sad,

365
00:20:51,230 --> 00:20:53,000
 he taught him the teaching

366
00:20:53,000 --> 00:20:56,640
 of all the waters and all the oceans are nothing compared

367
00:20:56,640 --> 00:20:57,960
 to all the tears.

368
00:20:57,960 --> 00:21:03,250
 You've cried over your daughter, over the death of your

369
00:21:03,250 --> 00:21:04,600
 daughter.

370
00:21:04,600 --> 00:21:09,480
 How many lifetimes?

371
00:21:09,480 --> 00:21:11,720
 Point being, there's no benefit from crying.

372
00:21:11,720 --> 00:21:14,920
 You don't get anything out of it.

373
00:21:14,920 --> 00:21:18,320
 Better ways to spend your time, life is uncertain.

374
00:21:18,320 --> 00:21:20,960
 Death is certain.

375
00:21:20,960 --> 00:21:23,710
 And tusita became a monk and he became enlightened,

376
00:21:23,710 --> 00:21:25,400
 apparently, eventually.

377
00:21:25,400 --> 00:21:29,970
 So that's the story, that's verse 174, that's the Dhammap

378
00:21:29,970 --> 00:21:31,480
ada for this week.

379
00:21:31,480 --> 00:21:33,920
 Thank you all for tuning in, wish you all the best.

