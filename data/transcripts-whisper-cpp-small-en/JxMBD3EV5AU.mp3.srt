1
00:00:00,000 --> 00:00:04,930
 Does an enlightened being feel any pain? I saw this video

2
00:00:04,930 --> 00:00:06,960
 when Tik Kwon Dook

3
00:00:06,960 --> 00:00:10,720
 burned himself and never moved. He sat still and seemed not

4
00:00:10,720 --> 00:00:11,720
 at all bothered by

5
00:00:11,720 --> 00:00:17,440
 this. Well there's there's many ways to be free from pain.

6
00:00:17,440 --> 00:00:18,200
 A person doesn't have

7
00:00:18,200 --> 00:00:22,160
 to be enlightened and I don't know that who this person is

8
00:00:22,160 --> 00:00:23,720
 but I imagine he

9
00:00:23,720 --> 00:00:26,500
 didn't consider himself to be enlightened. I don't know. I

10
00:00:26,500 --> 00:00:27,760
 don't imagine

11
00:00:27,760 --> 00:00:31,980
 enlightened being would set themselves on fire and I assume

12
00:00:31,980 --> 00:00:33,720
 he didn't claim to

13
00:00:33,720 --> 00:00:38,850
 be enlightened. I don't know who he is. But there are ways

14
00:00:38,850 --> 00:00:40,480
 to overcome pain and

15
00:00:40,480 --> 00:00:44,010
 there are ways to to keep yourself from feeling pain mostly

16
00:00:44,010 --> 00:00:45,840
 having to do mostly

17
00:00:45,840 --> 00:00:48,920
 based on samatha practice. An enlightened being does feel

18
00:00:48,920 --> 00:00:49,960
 pain but they aren't

19
00:00:49,960 --> 00:00:56,880
 bothered by it. And that being the case they may still move

20
00:00:56,880 --> 00:00:58,240
 out of the way

21
00:00:58,240 --> 00:01:02,590
 when they feel pain because they're acting naturally. Tr

22
00:01:02,590 --> 00:01:03,760
anquility meditation,

23
00:01:03,760 --> 00:01:06,980
 transcendental meditation is in many ways unnatural. It's

24
00:01:06,980 --> 00:01:07,960
 not our habit. It's

25
00:01:07,960 --> 00:01:11,280
 not our way of being human. You're not born a human because

26
00:01:11,280 --> 00:01:11,960
 you've practiced a

27
00:01:11,960 --> 00:01:16,270
 lot of tranquility meditation. So these states that monks

28
00:01:16,270 --> 00:01:17,320
 will enter into

29
00:01:17,320 --> 00:01:20,560
 and there was a monk who burnt his, I heard about he burnt

30
00:01:20,560 --> 00:01:21,220
 I think a small

31
00:01:21,220 --> 00:01:24,280
 finger. He stuck his little finger into a candle flame and

32
00:01:24,280 --> 00:01:25,400
 just burnt his entire

33
00:01:25,400 --> 00:01:30,120
 finger to the ground. He wanted to to make an offering to

34
00:01:30,120 --> 00:01:31,440
 the Buddha and he

35
00:01:31,440 --> 00:01:36,340
 offered his pinky finger apparently. And my teacher was

36
00:01:36,340 --> 00:01:37,960
 talking about this and he

37
00:01:37,960 --> 00:01:41,500
 said he said you know that's real strength of mind. He's

38
00:01:41,500 --> 00:01:42,260
 able to separate

39
00:01:42,260 --> 00:01:45,680
 the mind from the body and if your mind is focused enough

40
00:01:45,680 --> 00:01:46,920
 pain is really not a

41
00:01:46,920 --> 00:01:51,480
 big deal. It's not something that bothers you if your mind

42
00:01:51,480 --> 00:01:52,840
 is fixed and

43
00:01:52,840 --> 00:01:56,700
 focused and these monks have very fixed and focused

44
00:01:56,700 --> 00:02:03,880
 attention. So there's

45
00:02:03,880 --> 00:02:06,550
 two different states here. One is where you don't even feel

46
00:02:06,550 --> 00:02:07,280
 the pain and

47
00:02:07,280 --> 00:02:10,280
 that's in tranquility meditation and the other is where you

48
00:02:10,280 --> 00:02:11,520
 feel the pain but you

49
00:02:11,520 --> 00:02:16,040
 don't respond and that's totally different. And in Arahant

50
00:02:16,040 --> 00:02:16,120
 I

51
00:02:16,120 --> 00:02:19,230
 can't imagine them burning their finger and unless it was

52
00:02:19,230 --> 00:02:20,200
 it happened to be a

53
00:02:20,200 --> 00:02:23,590
 habit or something that they had developed over time

54
00:02:23,590 --> 00:02:24,280
 because it's not

55
00:02:24,280 --> 00:02:26,940
 natural and they don't have any reason to do it. They have

56
00:02:26,940 --> 00:02:28,080
 reason when say their

57
00:02:28,080 --> 00:02:30,570
 robes are on fire they have reason to put their put the

58
00:02:30,570 --> 00:02:31,800
 fire out. It's natural.

59
00:02:31,800 --> 00:02:38,840
 It's their habit. It's a part of their being to do that.

