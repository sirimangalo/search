 A question, to extend the question about sleeping, I get
 tired when meditating.
 Is it better to sleep when I'm not sleepy than meditate?
 Sleepiness is actually a great object of meditation.
 It's something that is overlooked, I think, by many medit
ators.
 Yeah, sometimes you have to just fall asleep, but the best
 thing is for that to come naturally.
 If you fall asleep, you fall asleep.
 When you're sleeping, first before you go to sleep, try to
 focus on the sleepiness,
 focus on the tired feeling.
 It's difficult in daily life when you're not in a
 meditation center because you really
 are exhausted a lot of the time from work and just thinking
 a lot and so much activity.
 You can try it, focus on the tiredness, and you'll find
 that sometimes it gives you energy.
 Sometimes you're able to burst the bubble, so to speak, and
 it just disappears.
 Suddenly you're not tired anymore.
 And you'll find you have suddenly a lot of energy and you
're able to do a really good
 meditation center.
 Drowsiness is a hindrance.
 It's something that drags you down and stops you from med
itating.
 If you're able to pierce it and overcome it, you'll find
 that suddenly your mind is clear
 and your practice goes a lot better and meditation can be
 quite fruitful after that point.
 So we should be careful not to forget about the state of
 mind that is fatigue or drowsiness.
 We shouldn't forget that it's a part of the meditation.
 When it comes up, it's a valid object of observation.
 It's a good one too.
 There's two ways.
 One way is you'll get a lot of energy by focusing on it, by
 acknowledging it, for example, tired,
 and the other is you'll just fall asleep.
 If you're really good, it'll be one or the other, and
 neither is a problem.
 The problem with sleeping first and then meditating is that
 sleep drains your awareness and your
 mindfulness.
 We try to have meditators sleep less and less, and
 sometimes they don't even sleep at all.
 They'll just do meditation.
 Sleep is an uncontrolled state.
 It's actually just a construct.
 Sleep doesn't have any ultimate reality.
 We think of sleep as being a necessary part of reality, but
 it's only a conditioned state
 that we've developed as human beings or as I suppose most
 animals have developed.
 It's not a part of ultimate reality.
 It's nothing intrinsic in the mind.
 It's just an aspect of this period of our existence that we
 have a very coarse body and it falls
 asleep sometimes.
 During the time that we're asleep, the mind is in a
 specific state that's not very wholesome.
 It's very free to do what it wants and to make assumptions
 and conclusions.
 People will commit dastardly deeds in their dreams that can
 happen.
 You can do all sorts of bad things.
 You can do good things, but sometimes you fight and kill
 and so on depending on the
 state of mind.
 It usually develops unwholesomeness when you sleep.
 Most people develop unwholesomeness.
 When they wake up, the mind is not fresh, the mind is not
 clear.
 It's clouded and diluted.
 You can feel that.
 If you're in a meditation center, you can feel that after
 you sleep you don't feel better.
 You feel actually less sharp and less clear.
 You have to sleep.
 As human beings, we do have to sleep.
 That's the important point here, to try to sleep mindfully
 before you sleep.
 Do meditation, whether it's lying meditation, saying to
 yourself, "Rising, falling," or
 whether it's some sitting meditation before you lie down.
 That'll help your sleep to be a lot more quiet and a lot
 clearer.
 You find you don't dream so much.
 Your mind is not so distracted when you sleep and you wake
 up a lot more refreshed.
 You can actually find that sleep is much more beneficial
 and it's not as much of a hindrance
 to your meditation.
 Thank you.
