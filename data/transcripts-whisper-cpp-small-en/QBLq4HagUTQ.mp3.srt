1
00:00:00,000 --> 00:00:06,360
 Hello, this is an attempt to re-answer a question I was

2
00:00:06,360 --> 00:00:09,920
 recently asked on Monk Radio about

3
00:00:09,920 --> 00:00:13,600
 the nature of right and wrong.

4
00:00:13,600 --> 00:00:18,490
 Specifically, in the case where it is true, or if it were

5
00:00:18,490 --> 00:00:21,480
 true, that there were no meaning

6
00:00:21,480 --> 00:00:26,890
 or purpose to anything, how could you have a sense of right

7
00:00:26,890 --> 00:00:28,080
 or wrong?

8
00:00:28,080 --> 00:00:33,470
 And so I think I'm going to have to clarify, first of all,

9
00:00:33,470 --> 00:00:36,540
 my stance and what I understand

10
00:00:36,540 --> 00:00:40,900
 to be the Buddha's stance on purpose and meaning and value.

11
00:00:40,900 --> 00:00:44,230
 Because the Buddha did give some fairly specific teachings

12
00:00:44,230 --> 00:00:46,600
 on what has purpose, what has meaning,

13
00:00:46,600 --> 00:00:49,760
 what has value.

14
00:00:49,760 --> 00:00:52,830
 But it's the kind of teaching that you have to understand

15
00:00:52,830 --> 00:00:53,680
 in context.

16
00:00:53,680 --> 00:00:58,130
 And so you have to understand some of the things that I

17
00:00:58,130 --> 00:01:00,200
 have said about purpose and

18
00:01:00,200 --> 00:01:02,840
 value in context.

19
00:01:02,840 --> 00:01:07,160
 And that was what I was trying to get across in my answer,

20
00:01:07,160 --> 00:01:09,620
 is that the two criticisms that

21
00:01:09,620 --> 00:01:15,300
 people will level against this idea of lack of purpose is,

22
00:01:15,300 --> 00:01:18,420
 first of all, that having lack

23
00:01:18,420 --> 00:01:26,230
 of purpose will most likely lead you to depression, apathy,

24
00:01:26,230 --> 00:01:28,080
 hopelessness.

25
00:01:28,080 --> 00:01:30,760
 It has a negative effect on one's psyche.

26
00:01:30,760 --> 00:01:36,750
 Whereas they would argue having a purpose gives you

27
00:01:36,750 --> 00:01:40,560
 motivation and direction and so

28
00:01:40,560 --> 00:01:42,840
 on.

29
00:01:42,840 --> 00:01:46,760
 Which is all well and good, and certainly not to be denied.

30
00:01:46,760 --> 00:01:50,180
 The other accusation that is leveled, charge that is

31
00:01:50,180 --> 00:01:52,280
 leveled, is that a person who has

32
00:01:52,280 --> 00:01:56,290
 no meaning, purpose, he's no value in anything, isn't able

33
00:01:56,290 --> 00:01:58,600
 to differentiate between that which

34
00:01:58,600 --> 00:02:05,200
 is valuable and that which is valueless, worthless, is that

35
00:02:05,200 --> 00:02:08,160
 they won't have a sense of right or

36
00:02:08,160 --> 00:02:09,160
 wrong.

37
00:02:09,160 --> 00:02:10,680
 And that's what this question was asking.

38
00:02:10,680 --> 00:02:13,440
 How can there be right or wrong if you don't have purpose

39
00:02:13,440 --> 00:02:14,900
 or value, if you aren't able

40
00:02:14,900 --> 00:02:18,520
 to make value judgments?

41
00:02:18,520 --> 00:02:23,750
 And so it's important to understand what the meaning behind

42
00:02:23,750 --> 00:02:25,160
 this idea is.

43
00:02:25,160 --> 00:02:32,740
 First of all, these two accusations are unfounded, but they

44
00:02:32,740 --> 00:02:37,240
're unfounded for a person who truly

45
00:02:37,240 --> 00:02:42,320
 has, you could say, transcended meaning and value.

46
00:02:42,320 --> 00:02:50,850
 And so the problem is that we're projecting our own muddled

47
00:02:50,850 --> 00:02:55,120
 unenlightened state when we

48
00:02:55,120 --> 00:02:56,320
 answer this question.

49
00:02:56,320 --> 00:03:00,830
 So we say, "What would it be like if I had no purpose, if I

50
00:03:00,830 --> 00:03:03,500
 were to accept that philosophy

51
00:03:03,500 --> 00:03:06,220
 as though it were just a philosophy that anyone could

52
00:03:06,220 --> 00:03:06,760
 accept?"

53
00:03:06,760 --> 00:03:09,120
 And that's not the case.

54
00:03:09,120 --> 00:03:13,920
 The point is that as you understand reality, you start to

55
00:03:13,920 --> 00:03:14,840
 change.

56
00:03:14,840 --> 00:03:17,820
 You stop finding meaning and purpose and value in the

57
00:03:17,820 --> 00:03:20,040
 things that you used to find meaning,

58
00:03:20,040 --> 00:03:22,600
 purpose and value in.

59
00:03:22,600 --> 00:03:31,850
 As a result of those changes, you lose your ability to

60
00:03:31,850 --> 00:03:37,920
 perform both right and wrong acts.

61
00:03:37,920 --> 00:03:45,420
 And in a sense, certainly the ability to perform evil deeds

62
00:03:45,420 --> 00:03:46,480
 disappears.

63
00:03:46,480 --> 00:03:49,760
 But at the same time, a person has no sense of good either,

64
00:03:49,760 --> 00:03:51,400
 in the sense that even when

65
00:03:51,400 --> 00:03:57,150
 they do something to help others, they're simply doing it

66
00:03:57,150 --> 00:04:00,280
 as a functional act of response

67
00:04:00,280 --> 00:04:03,480
 as kind of the path of least resistance.

68
00:04:03,480 --> 00:04:08,480
 To deny someone is to create complication.

69
00:04:08,480 --> 00:04:11,080
 It requires a certain amount of evil to do.

70
00:04:11,080 --> 00:04:15,300
 So a person simply does good because the alternative would

71
00:04:15,300 --> 00:04:17,920
 require something that is lacking in

72
00:04:17,920 --> 00:04:18,920
 them, which is evil.

73
00:04:18,920 --> 00:04:25,490
 It would require friction and that sort of friction and def

74
00:04:25,490 --> 00:04:28,480
ilement in the mind and all

75
00:04:28,480 --> 00:04:32,530
 of the stress in the mind disappears from the person who

76
00:04:32,530 --> 00:04:34,680
 has become enlightened.

77
00:04:34,680 --> 00:04:38,550
 So the important thing to realize is that when you say that

78
00:04:38,550 --> 00:04:40,320
 purposeless need and all

79
00:04:40,320 --> 00:04:44,750
 of this leads to apathy or that it leads to immorality

80
00:04:44,750 --> 00:04:47,800
 because a person who has no purpose

81
00:04:47,800 --> 00:04:52,240
 can then just do whatever they want, you are confusing two

82
00:04:52,240 --> 00:04:54,360
 very different states.

83
00:04:54,360 --> 00:04:58,870
 The state of someone who has actually realized what you

84
00:04:58,870 --> 00:05:01,400
 could call true purposelessness in

85
00:05:01,400 --> 00:05:03,880
 the sense that they don't put value in anything.

86
00:05:03,880 --> 00:05:06,880
 They simply live their lives.

87
00:05:06,880 --> 00:05:10,510
 And a person who still has attachment to things as this is

88
00:05:10,510 --> 00:05:12,440
 what I'm aiming for, this is what

89
00:05:12,440 --> 00:05:20,720
 I'm hoping for, this is what I'm wishing for.

90
00:05:20,720 --> 00:05:25,750
 There's a person who has given up all of that, has no

91
00:05:25,750 --> 00:05:28,840
 potential for evil to arise in their

92
00:05:28,840 --> 00:05:29,840
 mind.

93
00:05:29,840 --> 00:05:35,040
 They have no potential for apathy to arise in their mind.

94
00:05:35,040 --> 00:05:38,560
 They have no potential for any disappointment.

95
00:05:38,560 --> 00:05:41,650
 The thing is, it's funny to even suggest that purpose

96
00:05:41,650 --> 00:05:43,680
lessness would lead to depression

97
00:05:43,680 --> 00:05:47,550
 or any negative emotion whatsoever when the negative

98
00:05:47,550 --> 00:05:50,440
 emotions are based on disappointment.

99
00:05:50,440 --> 00:05:55,680
 They're based on a desire for value, a desire for purpose.

100
00:05:55,680 --> 00:05:59,900
 The only reason a person feels depressed when they have no

101
00:05:59,900 --> 00:06:02,120
 purpose is because they want

102
00:06:02,120 --> 00:06:05,600
 one, because they still crave for purpose.

103
00:06:05,600 --> 00:06:09,800
 A person who has gone beyond that isn't able to feel

104
00:06:09,800 --> 00:06:11,080
 depressed.

105
00:06:11,080 --> 00:06:13,960
 It's impossible for that to come to them because they have

106
00:06:13,960 --> 00:06:15,040
 no expectations.

107
00:06:15,040 --> 00:06:18,560
 They have no desire for anything.

108
00:06:18,560 --> 00:06:22,400
 That's what purposelessness does to you.

109
00:06:22,400 --> 00:06:25,320
 And as a result, they could never do anything that would

110
00:06:25,320 --> 00:06:27,360
 cause harm, that would cause stress

111
00:06:27,360 --> 00:06:30,040
 or suffering.

112
00:06:30,040 --> 00:06:32,200
 There is no attachment that would lead to that.

113
00:06:32,200 --> 00:06:33,880
 So that was all I was trying to say.

114
00:06:33,880 --> 00:06:35,920
 It's quite a simple answer and I think I muddled it up a

115
00:06:35,920 --> 00:06:36,480
 little bit.

116
00:06:36,480 --> 00:06:38,160
 So here I am again.

117
00:06:38,160 --> 00:06:39,240
 Thanks for tuning in everyone.

118
00:06:39,240 --> 00:06:40,480
 See you next time.

