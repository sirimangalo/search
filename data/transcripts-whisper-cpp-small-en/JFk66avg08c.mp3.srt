1
00:00:00,000 --> 00:00:04,000
 Hi everyone, another video log update here.

2
00:00:04,000 --> 00:00:09,000
 A few things, mainly based on my last video update.

3
00:00:09,000 --> 00:00:13,000
 First, the idea of the bus tour this year.

4
00:00:13,000 --> 00:00:17,690
 We've had quite a few people respond from all over the east

5
00:00:17,690 --> 00:00:19,000
 coast, which is really neat.

6
00:00:19,000 --> 00:00:23,480
 So I've set up a preliminary sort of, it's a bit rough, but

7
00:00:23,480 --> 00:00:27,850
 there's a Google map of all the destinations and possible

8
00:00:27,850 --> 00:00:29,000
 destinations.

9
00:00:29,000 --> 00:00:32,000
 I'm putting in two categories.

10
00:00:32,000 --> 00:00:39,750
 So if you've already expressed some intention to set up

11
00:00:39,750 --> 00:00:45,000
 some kind of event, like a talk or even a meditation course

12
00:00:45,000 --> 00:00:46,000
 in your area,

13
00:00:46,000 --> 00:00:50,540
 for us for me to run, then I put you on the destination

14
00:00:50,540 --> 00:00:51,000
 list.

15
00:00:51,000 --> 00:00:53,850
 So for sure we'll try to get to those places that actually

16
00:00:53,850 --> 00:00:55,000
 do set up such a thing.

17
00:00:55,000 --> 00:00:58,400
 Those places where there are people who have just mentioned

18
00:00:58,400 --> 00:01:00,000
 that they'd like to have us,

19
00:01:00,000 --> 00:01:03,000
 but are not sure if they could set anything up.

20
00:01:03,000 --> 00:01:05,810
 But if you're passing through kind of thing, I put on the

21
00:01:05,810 --> 00:01:07,000
 possibilities.

22
00:01:07,000 --> 00:01:10,580
 And I may have missed some, it's a little bit disorganized

23
00:01:10,580 --> 00:01:12,000
 here, there's no computers,

24
00:01:12,000 --> 00:01:17,000
 so I'm just running this all through a smartphone actually.

25
00:01:17,000 --> 00:01:19,000
 So hopefully that works.

26
00:01:19,000 --> 00:01:22,000
 You can check it out, the link will be in the description.

27
00:01:22,000 --> 00:01:25,250
 I follow everything I do either on Facebook or Google+, or

28
00:01:25,250 --> 00:01:27,000
 on my web blog.

29
00:01:27,000 --> 00:01:30,000
 I'm pretty well connected.

30
00:01:30,000 --> 00:01:32,000
 So yeah, that's great.

31
00:01:32,000 --> 00:01:36,130
 Thanks everyone for responding and showing that it actually

32
00:01:36,130 --> 00:01:40,000
 is something that people would like to see happen.

33
00:01:40,000 --> 00:01:41,000
 So let's see how it goes.

34
00:01:41,000 --> 00:01:42,000
 It'll be an experiment anyway.

35
00:01:42,000 --> 00:01:44,000
 And again, there's the website.

36
00:01:44,000 --> 00:01:48,520
 I can go there and I've already put in just some bullet

37
00:01:48,520 --> 00:01:53,000
 notes of the places that will potentially be going.

38
00:01:53,000 --> 00:01:58,000
 And we'll be ending up in Florida near Tampa.

39
00:01:58,000 --> 00:02:01,500
 So if you're in that area, there's already some people in

40
00:02:01,500 --> 00:02:03,910
 that area who have expressed interest in setting something

41
00:02:03,910 --> 00:02:04,000
 up.

42
00:02:04,000 --> 00:02:07,180
 And I'll probably be there for a few days, maybe even a

43
00:02:07,180 --> 00:02:09,000
 week before heading back.

44
00:02:09,000 --> 00:02:13,540
 So yeah, if you're not on the list and if you have a place

45
00:02:13,540 --> 00:02:16,000
 that you'd like to see us come,

46
00:02:16,000 --> 00:02:19,000
 head over to the website, jarica.ceremongolo.org.

47
00:02:19,000 --> 00:02:21,000
 I'll put that in the description.

48
00:02:21,000 --> 00:02:24,000
 And you can let us know.

49
00:02:24,000 --> 00:02:28,040
 A few people have already used the website to do that,

50
00:02:28,040 --> 00:02:30,000
 which shows that it works.

51
00:02:30,000 --> 00:02:31,000
 So that's the first thing.

52
00:02:31,000 --> 00:02:34,000
 The second thing is the translation of my booklet.

53
00:02:34,000 --> 00:02:39,150
 We've got ten translations, well, nine translations in the

54
00:02:39,150 --> 00:02:41,000
 original English.

55
00:02:41,000 --> 00:02:46,280
 So I've got ten languages that we're going to put together

56
00:02:46,280 --> 00:02:50,000
 as a set and offer to my teacher.

57
00:02:50,000 --> 00:02:53,390
 Most of the people who have offered to translate are still

58
00:02:53,390 --> 00:02:56,000
 underway and haven't been able to get it done in time,

59
00:02:56,000 --> 00:02:57,000
 which is fine actually.

60
00:02:57,000 --> 00:02:59,770
 I'd rather have a good translation than a quick translation

61
00:02:59,770 --> 00:03:00,000
.

62
00:03:00,000 --> 00:03:02,000
 I think we all would.

63
00:03:02,000 --> 00:03:05,280
 So the Thai translation also, I'm going to have to go

64
00:03:05,280 --> 00:03:07,900
 through it because, probably because it's the only one I

65
00:03:07,900 --> 00:03:09,000
 actually understand.

66
00:03:09,000 --> 00:03:12,000
 And so I feel the need to correct a lot of things.

67
00:03:12,000 --> 00:03:14,930
 It's probably going to take some time for me to get the

68
00:03:14,930 --> 00:03:16,000
 time to do that.

69
00:03:16,000 --> 00:03:18,230
 But we have a new Russian translation and a Dutch

70
00:03:18,230 --> 00:03:21,000
 translation, apart from the original eight languages.

71
00:03:21,000 --> 00:03:23,560
 So I'll put together ten and hopefully I'll get a picture

72
00:03:23,560 --> 00:03:28,040
 of that or maybe even a video to show you of the boxed set

73
00:03:28,040 --> 00:03:31,000
 that we'll be offering to my teacher.

74
00:03:31,000 --> 00:03:33,830
 And of course you can download any of the ten languages

75
00:03:33,830 --> 00:03:35,000
 from the website.

76
00:03:35,000 --> 00:03:38,230
 I'll put that link in this video as well, in case you're

77
00:03:38,230 --> 00:03:40,000
 just hearing about it now.

78
00:03:40,000 --> 00:03:45,120
 Again, if there's a language that isn't on that webpage as

79
00:03:45,120 --> 00:03:49,830
 potentially under translation and you're ready to translate

80
00:03:49,830 --> 00:03:52,580
, ready to start translating into that language, please do

81
00:03:52,580 --> 00:03:53,000
 let us know.

82
00:03:53,000 --> 00:03:58,080
 And send me a note and we'll add you to the list and then

83
00:03:58,080 --> 00:04:02,920
 you can send us the doc file or an ODT file or something

84
00:04:02,920 --> 00:04:07,040
 that we can edit and turn into a PDF and an EPUB to put on

85
00:04:07,040 --> 00:04:08,000
 the website.

86
00:04:08,000 --> 00:04:10,350
 So that's great as well. It's been amazing to see how many

87
00:04:10,350 --> 00:04:11,000
 languages.

88
00:04:11,000 --> 00:04:15,120
 Probably by the end I figure we'll get about 20 languages

89
00:04:15,120 --> 00:04:16,000
 in total.

90
00:04:16,000 --> 00:04:19,000
 It looks pretty good to get another ten at least.

91
00:04:19,000 --> 00:04:22,850
 So that's really neat to think of all those different

92
00:04:22,850 --> 00:04:24,000
 languages.

93
00:04:24,000 --> 00:04:26,510
 Another thing, the third thing that I wanted to talk about

94
00:04:26,510 --> 00:04:29,000
 related to this is I'm planning to write a second book.

95
00:04:29,000 --> 00:04:32,520
 Some people were asking about some teachings that were a

96
00:04:32,520 --> 00:04:36,060
 little more advanced because obviously this booklet is just

97
00:04:36,060 --> 00:04:37,000
 for beginners.

98
00:04:37,000 --> 00:04:41,340
 So the second, the sequel to the book would be sort of a

99
00:04:41,340 --> 00:04:43,000
 map of the paths.

100
00:04:43,000 --> 00:04:46,520
 I try to make it fairly general because again I don't want

101
00:04:46,520 --> 00:04:50,050
 to go into great detail about those things in a meditator

102
00:04:50,050 --> 00:04:54,430
 experiences because without the active participation with a

103
00:04:54,430 --> 00:05:01,650
 teacher it can be, not dangerous at least, can lead you ast

104
00:05:01,650 --> 00:05:02,000
ray.

105
00:05:02,000 --> 00:05:05,380
 And I don't want to lead people to think that a book can

106
00:05:05,380 --> 00:05:08,000
 somehow take the place of a teacher.

107
00:05:08,000 --> 00:05:10,900
 So it'll be general but I'll try to go through all the

108
00:05:10,900 --> 00:05:14,770
 different stages and the concepts and principles that you

109
00:05:14,770 --> 00:05:18,360
 have to keep in mind during the practice once you've begun

110
00:05:18,360 --> 00:05:23,000
 to undertake the practice either intensively or day to day

111
00:05:23,000 --> 00:05:27,070
 over months or years for those people who haven't had a

112
00:05:27,070 --> 00:05:30,000
 chance to undertake an intensive course.

113
00:05:30,000 --> 00:05:34,160
 So look for that. Hopefully I'll have a couple of chapters

114
00:05:34,160 --> 00:05:38,050
 up this year. Again I'm studying this year trying to get

115
00:05:38,050 --> 00:05:42,000
 these poly exams that my teacher asked me to study for.

116
00:05:42,000 --> 00:05:45,330
 So be a little bit busy but hopefully all these different

117
00:05:45,330 --> 00:05:49,060
 projects will, some of them will actually come to fruit. I

118
00:05:49,060 --> 00:05:51,740
 guess I can't say they all will but let's hope that at

119
00:05:51,740 --> 00:05:53,000
 least some of it comes to fruit.

120
00:05:53,000 --> 00:05:56,340
 And again thanks everyone for the great support and

121
00:05:56,340 --> 00:06:00,000
 response that you've given to all of these projects.

122
00:06:00,000 --> 00:06:06,440
 Without that none of this would happen so please keep up

123
00:06:06,440 --> 00:06:12,330
 the appreciation at least. Let us know that you're using

124
00:06:12,330 --> 00:06:17,030
 and taking part and benefiting from all of this stuff just

125
00:06:17,030 --> 00:06:19,000
 so I do keep doing it.

126
00:06:19,000 --> 00:06:23,090
 And show your support. If you want to support our

127
00:06:23,090 --> 00:06:27,830
 organization you're welcome to do that via our website or

128
00:06:27,830 --> 00:06:32,380
 at least just let us know that you're out there and

129
00:06:32,380 --> 00:06:34,000
 following along.

130
00:06:34,000 --> 00:06:37,010
 Anyway you can see that you are. It's a great thing to see.

131
00:06:37,010 --> 00:06:40,440
 We've got millions of people or millions of views on

132
00:06:40,440 --> 00:06:44,360
 YouTube and thousands of people following these videos so

133
00:06:44,360 --> 00:06:46,000
 it's quite encouraging.

134
00:06:46,000 --> 00:06:49,010
 So I'm wishing you all the best and please do keep it up.

135
00:06:49,010 --> 00:06:52,100
 Keep practicing. Keep on the path and let's find peace,

136
00:06:52,100 --> 00:06:55,000
 happiness and freedom from suffering together.

137
00:06:55,000 --> 00:06:58,000
 Thanks. Peace. All the best.

138
00:06:58,000 --> 00:07:00,000
 All the best.

