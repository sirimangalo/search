 Good morning.
 I'm broadcasting here from Myeong-san again.
 I'm going to talk about something called the Idi Pada,
 which is...
 Idi means power or strength or might.
 Power is one of the best translations,
 sometimes translated as success or dominance.
 Pada means the path or the steps that lead to something.
 So these are often translated as the four means of
 succeeding,
 of bringing something to its culmination,
 to fruit, to bearing, to having our work bear fruit.
 And so the most important aspect of this is how we apply
 them to our practice,
 how we apply these dhammas in order to succeed in our
 practice.
 But another useful way of looking at them
 is in terms of how the practice helps us succeed.
 So how meditation practice actually helps us cultivate
 these dhammas.
 It's helpful because sometimes we lose confidence in the
 practice,
 find it hard to remember why we're torturing ourselves.
 We have a hard time sometimes building up the interest in
 the practice.
 So it's often good to talk about how the practice helps us
 in our lives
 spiritually, how it leads us to greater things.
 So we'll look at this two ways then,
 in terms of how the four Idi Pada help us practice,
 in terms of how the practice brings us closer to Idi,
 closer to power or success,
 closer just to making sure our lives bear fruit or are
 fruitful.
 So the first Idi Pada is something called chanda,
 chanda which means desire or interest.
 And so it can be used in a bad way.
 We have something called kama chanda,
 which means sensual desire,
 which is in Buddhism of course considered to be unhelpful,
 harmful.
 Dragging one down.
 But then there's dhamma chanda, which is desire for truth.
 And so desire isn't a very good translation exactly.
 It's an interest or zeal.
 I guess colloquially we have to use the word desire.
 But it's important to understand it isn't like attachment
 to meditating.
 Something like that.
 It's the genuine desire to better oneself.
 And so this is the first means of succeeding in dhamma and
 in life.
 You need to be interested in what you're doing in order to
 succeed.
 I think that's difficult in insight meditation because it's
 not very pleasant sometimes.
 And also because there's nothing to cling to.
 Because meditation is about letting go,
 there's nothing during the practice that you can cling to
 and say,
 "Yes, that's what I want."
 It's all about realizing that the things that you want are
 not worth clinging to.
 The Buddha said, "Sabdhe dhamma na langa bhini reh sahya."
 No dhamma is worth clinging to. Nothing is worth clinging
 to.
 Not even meditation practice, not even goodness, nothing.
 Clinging isn't the path to happiness.
 And so it's quite difficult, as I said, to cultivate this
 desire for the meditation.
 This is why the word "desire" is kind of unhelpful here.
 Because even wanting to meditate is usually misleading,
 misled.
 When someone wants to meditate, it's usually for the wrong
 reasons.
 Because wanting doesn't help you in your meditation.
 But the interest, the contentment, the zest, the zeal for
 meditation, this is important.
 And we gain this in different ways through reminding
 ourselves of the benefits of meditation.
 We gain it through the meditation practice itself,
 and seeing because meditation leads you on, the truth leads
 you to greater truth.
 As you see the truth, it encourages you.
 Through thinking about the Buddha, this gives encouragement
 about the example,
 through surrounding yourself with people who are good
 examples,
 people who remind you of the path and the goal,
 people who set an example for you.
 Through listening to the Dhamma, through studying the Dham
ma, all of these things,
 through thinking about death and the inevitability of the
 dangers and the troubles that await us,
 especially if we're not prepared.
 But it's important to recognize this and to recognize our
 discontent with the practice and to work through that.
 In fact, the discontent is probably more important
 ultimately,
 because ultimately there's no desire for the practice.
 There's only a sort of sense of settling into a mindful
 state,
 which comes with getting rid of discontent.
 And so we have to work on this discontent, which is in two
 ways.
 There's the discontent with the practice because it's
 unpleasant,
 because it shows us things we'd rather not look at,
 and discontent based on desire, because we want something
 else,
 we want pleasures, we want entertainment.
 So examining those and reminding ourselves, liking one
 thing, wanting one thing,
 and being able to see through them, to see that they're not
 actually satisfying,
 that they can't actually make us happy, bring us true
 happiness.
 To see them for what they are is something that arises and
 sees them and let them go.
 Then the mind settles into this sort of contentment, which
 is a sort of chanda in itself.
 At any rate, we have to be careful to overcome discontent.
 But the great thing about meditation is that it brings us
 chanda,
 it brings us content, the right kind of desire.
 It refines our desires.
 So if you look at the nature of a human being,
 when they're children, as children we have very coarse
 desires,
 we find ourselves pleased by simple things,
 and bright colors and loud noises and so on.
 And that changes over time, and why it changes is because
 our sense of pleasure and happiness,
 our desires become more refined.
 It becomes refined through experience.
 We come to realize that the happiness that we find as a
 child from bright lights and colors and sounds
 is actually quite coarse and disturbing.
 It's tiring, laughing all the time, giggling, screaming.
 All of these things are tiresome.
 So we lose interest in them quite quickly.
 Part of the process of growing up is refining one's desires
.
 And then you have teenagers who get interested in all sorts
 of drugs
 and entertainment, loud music and so on, and that changes.
 And children look at adults and think of them as kind of
 boring and dull,
 but a lot of that is because they've seen through it and
 they've realized how stressful it actually is
 to seek out entertainment all the time.
 So they refine their desires.
 This goes on through life. It's not the only process.
 There's a lot of repression that goes on as well.
 Part of it is the refinement of desires.
 So in a way, meditation is just an extension of that, or an
 acceleration of that process.
 Because in meditation you clearly come to refine your
 ambitions and your desires
 to the point where you cut out so many things that ordinary
 people think would bring happiness.
 You start to realize that that is only a cause for more
 stress and suffering.
 So you give it up in favor of more peaceful and more truly
 happy pursuits.
 Live a more simple life. Start giving away your possessions
, not out of repression,
 but because you realize how stressful it is to hold on to
 them.
 So this is how happiness brings about chanda.
 It purifies your desires.
 The second one is ririya.
 I talked about this yesterday. I mentioned how important r
iriya is.
 In order to succeed in anything, you have to work at it.
 Meditation is no exception.
 Meditation isn't just about relaxing.
 There's a lot of tweaking that has to go on.
 Finding your practice doesn't mean just letting it go
 naturally.
 This is a mistake that so many people make in meditation.
 Thinking that if you just relax it will all work itself out
,
 but it obviously doesn't, otherwise we'd all be enlightened
 by now.
 Because our natural way of processing things is to file
 this unwholesome.
 It's based on clinging, it's based on identification, and
 so on.
 So we have to work just to get to a state of normal,
 just to get to a balanced state.
 We have to do a lot of tweaking.
 It's like the Buddha said, it's like a tree leaning in one
 direction.
 If you just let it fall, it's going to fall in that
 direction.
 The problem is our minds are inclined in the wrong
 direction,
 so you can't just let them fall.
 You have to work hard to pull our minds in another
 direction.
 There's a lot of work that has to be done to do that.
 It doesn't have to be stressful exactly,
 but there should be a sense of work until our minds are
 inclined in the right direction,
 and then it's much more pleasant and peaceful.
 Once the mind gets on the right path, then it's just smooth
 sailing.
 But until that time, you have to be clear in terms of real
igning the mind.
 But meditation also brings effort.
 So the good that meditation does for you, it helps us
 succeed in life
 because we have less dragging us down.
 All of these coarse desires are eliminated, so we have much
 more energy.
 They talk about meditators being zombies, but it's so far
 from the truth.
 The zombies are the ones that seek out pleasure
 and spend so much of their energy on mindless pursuits.
 They end up being very dull and uninspired.
 If there's someone who has clear meditation, they find they
 have great energy.
 They're able to do whatever they'd like to accomplish,
 and they're able to accomplish great things because of
 their energy and their effort,
 that they are preserving through mindfulness.
 Just being mindful itself preserves so much energy
 because all the stress from liking and disliking and
 getting caught up in things,
 you can see that when you meditate.
 It wears you out and drags you down.
 This is really it.
 The third one, "titta." "Titta" means to focus your mind,
 keeping the task in mind, keeping your mind on the object,
 on the task.
 So this is the, in meditation, this is the aspect of
 keeping our focus on mindfulness,
 and finding ourselves again and again.
 The ability to be mindful, to not forget yourself
 and realize that you've been eating or talking or working
 or walking or whatever,
 even just sitting and not being mindful.
 The ability to catch yourself and actually be mindful of
 the experiences as they occur.
 This is something that grows over time.
 This is why we come back and do courses.
 This is why we try to do a daily practice.
 This is why we insist that meditators try to be mindful
 even when they're not practicing.
 It has to become habitual, and eventually it does.
 Eventually it seeps into your mind and your brain.
 It becomes part of your life so that some people even get
 so that they dream mindfully.
 Not exactly mindfully, but they're repeating things in
 their dreams.
 Not mindfully, but it gets so drilled into them.
 It's only really in the beginning when it's still only a
 preliminary state.
 But the ability to recognize when you get angry, to catch
 it,
 to realize the correct response is to say angry and to
 remind yourself.
 When you want something to catch yourself and not run after
 it.
 Obviously to succeed in anything you need to keep it in
 mind,
 and meditation is equally true.
 So try, even when you're not meditating, try to be mindful.
 In between walking and sitting, try to catch yourself as
 you're walking,
 as you sit down and so on.
 As soon as you finish meditation, try to set yourself for
 the rest of the day.
 But meditation has, on the flip side, meditation actually
 helps our focus, of course.
 We're able to keep our minds on things, we get less
 distracted.
 The great benefits of meditation is the ability to think
 clearer,
 the ability to solve problems better, to focus on work,
 and more sincerely, more with greater dedication,
 to complete tasks with greater efficiency.
 Because our minds are clear, there's less garbage piling up
, getting in the way.
 And we're able to clear it out.
 As it comes up and file things better, our minds become
 more sorted and more organized.
 Our memories improve and so on.
 This is jitta.
 Jitta is something that improves with meditation, something
 that improves our meditation.
 The number four is vimansa.
 Vimansa is a compliment to jitta. Jitta means to keep
 things in mind.
 But vimansa means to adjust.
 Vimansa means discernment.
 So it's not enough in any work, in most works, to just plow
 ahead on thinking.
 This is often a mistake people make in many realms.
 They just work without discernment.
 But even in menial tasks, even in physical work, physical
 labor,
 you can see the difference between someone who is disc
erning and someone who is not.
 The discerning person will be able to adjust their efforts,
 will be able to retrain even their bodies to refine their
 effort.
 So vimansa is like being able to step back and observe your
 work, your practice,
 and adjust its wisdom.
 Not just plowing ahead in meditation, this is clearly, not
 clearly,
 this is another mistake that people make.
 They plow ahead, trying to do as many hours a day as they
 can.
 It's great if you can do lots and lots of hours of
 meditation,
 but it's quite possible to do lots and lots of hours of not
 meditating, of fake meditating.
 Just walking and sitting without benefit.
 And so you have to be able to adjust.
 So as I said yesterday, the ability to go to the next level
, always the next level,
 not to be content or not to be stuck on one level of
 ability.
 So you cultivate a certain level of ability and then you
 think of that as sufficient.
 But the mind will constantly be surprising you, and you
 constantly have to adapt.
 Because it's really, these are the nots, our nots, they're
 what we've always been stuck on.
 They're the definition of problems, they are the sticking
 points in our mind.
 They are what confuses us, what challenges us.
 That's what we're focusing on.
 So the very definition of these things, the very nature of
 these things is going to challenge us.
 That's what they are, they're the challenges in our mind.
 We can't just plow through, you have to be discerning.
 In practice this just means truly being mindful.
 Truly being, it's an aspect of mindfulness to clearly
 understand the experience.
 Sampa Janya, to clearly see the experience.
 Don't get complacent and think you already know the
 experience.
 Every experience would be something, there's some aspect to
 it that is challenging you.
 That's why it still comes up.
 So always look at every experience with a fresh mind,
 trying to understand it,
 trying to see it clearly as though it was something you'd
 never seen before.
 And you'll see things that you've never really noticed
 before, this is how it works.
 So discernment is an important part of meditation.
 Meditation also leads to discernment in our lives, in our
 spiritual path,
 in terms of spiritual well-being.
 Discernment is our ability to solve our problems.
 It's amazing how quick the mind is able to fix things.
 When your mind is clear you're able to solve problems that
 other people
 become totally crippled by.
 You're able to see through all of the delusion and illusion
,
 to see the truth of the situation,
 and to realize the nature and the solution to the problem.
 You're able to put many pieces together in the mind.
 In terms of worldly success it's very easy to succeed in
 business,
 and it's easy to build things, to create things.
 The mind works much clearer and much more efficiently.
 But it's also much more clever in terms of seeing the big
 picture
 and being able to work towards goals and so on.
 Life becomes a lot easier to live through the meditation
 practice,
 and the mind becomes stronger and quicker, sharper, more
 clever.
 On one side this is an admonishment to cultivate these four
 things for our practice,
 but on the other side it's an encouragement that the
 practice is cultivating these things.
 Practice actually has a benefit, has great benefit in our
 lives
 for those who are struggling with this.
 So that's the Dhamma for today. Thank you for tuning in.
 Keep practicing.
