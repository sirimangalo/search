1
00:00:00,000 --> 00:00:07,000
 Okay, here's a good question.

2
00:00:07,000 --> 00:00:10,660
 "Is it true that Mahasi Sayadaw said that eventually the

3
00:00:10,660 --> 00:00:14,000
 mental labeling and meditation can be dropped?"

4
00:00:14,000 --> 00:00:17,390
 Very good question. It shows that we have someone who knows

5
00:00:17,390 --> 00:00:23,000
 something about the Mahasi Sayadaw tradition and technique

6
00:00:23,000 --> 00:00:31,000
 and all of the little intricacies.

7
00:00:31,000 --> 00:00:34,000
 My answer, no. Mahasi Sayadaw never said that.

8
00:00:34,000 --> 00:00:38,020
 There's a book, and I probably have it here in my closet,

9
00:00:38,020 --> 00:00:41,000
 that seems to suggest that.

10
00:00:41,000 --> 00:00:45,600
 It's written in English. There is a Thai book that is

11
00:00:45,600 --> 00:00:52,000
 translated in Thai that actually explicitly states that.

12
00:00:52,000 --> 00:01:02,000
 Now, there is also...I'm trying to think, I researched this

13
00:01:02,000 --> 00:01:05,190
 because there's one statement where Mahasi Sayadaw said,

14
00:01:05,190 --> 00:01:07,000
 let me get it, okay?

15
00:01:07,000 --> 00:01:28,000
 [pause]

16
00:01:28,000 --> 00:01:33,000
 Well, maybe I don't have it. Maybe it's here somewhere.

17
00:01:33,000 --> 00:01:37,310
 Oh, I know, it's got to be in my suitcase. I'm not going to

18
00:01:37,310 --> 00:01:38,000
 find it. I won't dig it out.

19
00:01:38,000 --> 00:01:43,580
 He says...and the Thai is actually quite good. The Thai

20
00:01:43,580 --> 00:01:47,000
 explains what the Burmese is actually saying.

21
00:01:47,000 --> 00:01:49,490
 You'd have to go to the Burmese and see exactly what he

22
00:01:49,490 --> 00:01:50,000
 says.

23
00:01:50,000 --> 00:01:55,640
 What he says is that some people criticize this technique

24
00:01:55,640 --> 00:01:59,000
 of labeling, of noting the mantra, no?

25
00:01:59,000 --> 00:02:05,050
 They criticize it saying that as a result, the meditator

26
00:02:05,050 --> 00:02:11,170
 will focus only on concepts, or the meditator will be...the

27
00:02:11,170 --> 00:02:13,000
 mind will be focused on the concepts, right?

28
00:02:13,000 --> 00:02:17,430
 So when you say rising, you'll be focused on a concept of

29
00:02:17,430 --> 00:02:21,820
 the stomach rising, as opposed to actually focusing on the

30
00:02:21,820 --> 00:02:24,000
 yodhatu or the air element.

31
00:02:24,000 --> 00:02:29,450
 Mahasi Sayadaw's answer to that is, it is true...well, it

32
00:02:29,450 --> 00:02:34,910
 is true that in the beginning, a meditator will see only

33
00:02:34,910 --> 00:02:37,000
 concepts.

34
00:02:37,000 --> 00:02:41,680
 Panyati, I believe, is the word that he used. It's the word

35
00:02:41,680 --> 00:02:43,000
 that they use in Thai.

36
00:02:43,000 --> 00:02:49,080
 As the meditator progresses, the concept will fall away,

37
00:02:49,080 --> 00:02:51,880
 and all that will be left is the ultimate experience of the

38
00:02:51,880 --> 00:02:53,000
 ultimate reality.

39
00:02:53,000 --> 00:02:58,840
 He never says that the meditator will cease to use the

40
00:02:58,840 --> 00:03:00,000
 words.

41
00:03:00,000 --> 00:03:05,110
 Now, what the Thai does, this terrible Thai book does, is

42
00:03:05,110 --> 00:03:06,000
 it goes...

43
00:03:06,000 --> 00:03:11,400
 Whereas in the beginning, the meditator will experience

44
00:03:11,400 --> 00:03:17,970
 only concepts, and then in parentheses, it says noting, the

45
00:03:17,970 --> 00:03:21,000
 word, or the noting.

46
00:03:21,000 --> 00:03:28,140
 As the meditator progresses, they will drop the concept, or

47
00:03:28,140 --> 00:03:31,300
 they will experience only the ultimate reality, or

48
00:03:31,300 --> 00:03:33,000
 something making it very clear.

49
00:03:33,000 --> 00:03:35,560
 And I think even putting a footnote, explaining what it

50
00:03:35,560 --> 00:03:38,940
 means, that eventually the meditator doesn't have to use

51
00:03:38,940 --> 00:03:40,000
 the noting anymore,

52
00:03:40,000 --> 00:03:46,050
 which is certainly not what is said there. The English also

53
00:03:46,050 --> 00:03:48,000
 misinterprets it in that way,

54
00:03:48,000 --> 00:03:50,590
 but looking at the Thai, you can get a sense of what he's

55
00:03:50,590 --> 00:03:52,000
 actually saying in Burmese,

56
00:03:52,000 --> 00:03:56,440
 and I bet that's quite clearly what he says, because the

57
00:03:56,440 --> 00:03:58,000
 Thai actually puts it in parentheses.

58
00:03:58,000 --> 00:04:02,780
 The English glosses it over, and just says, eventually the

59
00:04:02,780 --> 00:04:05,000
 words will be given up, or something.

60
00:04:05,000 --> 00:04:08,000
 But the word that he uses is not word, it's concepts.

61
00:04:08,000 --> 00:04:12,360
 And that's clearly in line with the theory that we follow,

62
00:04:12,360 --> 00:04:17,000
 that in the beginning, the meditator through this practice,

63
00:04:17,000 --> 00:04:20,280
 will see only the concepts of the stomach rising, and so on

64
00:04:20,280 --> 00:04:23,000
. But eventually, the concepts will disappear,

65
00:04:23,000 --> 00:04:26,220
 and when they say rising, they would simply be aware of the

66
00:04:26,220 --> 00:04:27,000
 wai o dhatu.

67
00:04:27,000 --> 00:04:31,520
 So no, as far as I understand, Mahasya Sayyada never said

68
00:04:31,520 --> 00:04:35,000
 that the mental labeling can be dropped.

69
00:04:35,000 --> 00:04:38,000
 He may have elsewhere, but it would be a great shock to me.

70
00:04:38,000 --> 00:04:40,940
 It was a shock to me to read that in English, because it

71
00:04:40,940 --> 00:04:42,000
 makes no sense.

72
00:04:42,000 --> 00:04:47,030
 Another answer that you might give to that is, the point

73
00:04:47,030 --> 00:04:50,000
 when the noting can be dropped,

74
00:04:50,000 --> 00:04:52,750
 it can be dropped at some point, it can be dropped at the

75
00:04:52,750 --> 00:04:57,000
 point when the meditator reaches Anulomanyan,

76
00:04:57,000 --> 00:05:05,790
 the 12th stage of knowledge, and the 13th is Gautarbu, 14th

77
00:05:05,790 --> 00:05:07,000
, 15th, yeah.

78
00:05:07,000 --> 00:05:10,980
 So the 12th stage of knowledge, because at that point, the

79
00:05:10,980 --> 00:05:16,000
 mind needs no assistance.

80
00:05:16,000 --> 00:05:20,630
 At that point, the mind is seeing perfectly clearly the

81
00:05:20,630 --> 00:05:22,000
 Four Noble Truths.

82
00:05:22,000 --> 00:05:26,320
 From Anulomanyan all the way to Palanyana, it's seeing the

83
00:05:26,320 --> 00:05:28,000
 Four Noble Truths.

84
00:05:28,000 --> 00:05:32,730
 And that happens automatically. So at that point, most

85
00:05:32,730 --> 00:05:36,000
 especially when the meditator enters into Nimbana,

86
00:05:36,000 --> 00:05:40,170
 and is meditating, you could say, is meditating on Nimbana,

87
00:05:40,170 --> 00:05:43,000
 or is in the experience of cessation,

88
00:05:43,000 --> 00:05:46,000
 then there's no need for the noting.

89
00:05:46,000 --> 00:05:49,280
 You can also say that an arahant has no need to practice

90
00:05:49,280 --> 00:05:50,000
 the noting,

91
00:05:50,000 --> 00:05:52,500
 because what that means is the meditator has no need to med

92
00:05:52,500 --> 00:05:53,000
itate.

93
00:05:53,000 --> 00:05:58,170
 So whether they actually do noting or not, you'd have to

94
00:05:58,170 --> 00:06:01,000
 ask the arahant themselves.

95
00:06:01,000 --> 00:06:14,000
 Okay.

96
00:06:14,000 --> 00:06:17,000
 Ah, what is that question?

97
00:06:17,000 --> 00:06:27,000
 [

