1
00:00:00,000 --> 00:00:02,600
 Okay, so welcome back to our study of the Dhammapada.

2
00:00:02,600 --> 00:00:07,930
 Today we continue on with verse number 41, which reads as

3
00:00:07,930 --> 00:00:08,680
 follows.

4
00:00:08,680 --> 00:00:15,300
 "Achirang vata yanga yo bata vingadhi se sati"

5
00:00:15,300 --> 00:00:24,320
 "Tur tu ho apeta vinyanu niratang vaka linga rang"

6
00:00:24,320 --> 00:00:28,280
 Which means

7
00:00:29,160 --> 00:00:35,640
 "Before long indeed, achirang vata, ayanga yo this body,

8
00:00:35,640 --> 00:00:40,720
 bata vingadhi se sati will lie on the earth."

9
00:00:40,720 --> 00:00:47,960
 "Chur do" means discarded.

10
00:00:47,960 --> 00:00:53,880
 "Apetu vinyanu" without consciousness or with consciousness

11
00:00:53,880 --> 00:00:54,560
 having left,

12
00:00:54,560 --> 00:00:58,160
 consciousness having departed from it.

13
00:00:58,840 --> 00:01:02,800
 "Niratang vata, niratang vata linga rang"

14
00:01:02,800 --> 00:01:09,370
 As useless as a log, as useless as a dead piece of wood or

15
00:01:09,370 --> 00:01:09,820
 a log.

16
00:01:09,820 --> 00:01:18,540
 So this is seemingly a little bit depressing verse, no?

17
00:01:18,540 --> 00:01:26,200
 Referring to death, "Before long we will be dead."

18
00:01:26,440 --> 00:01:31,640
 This is a reminder of the negative aspects of life.

19
00:01:31,640 --> 00:01:34,560
 So this was told in regards to

20
00:01:34,560 --> 00:01:41,520
 It's a fairly famous story and it's a fairly famous verse.

21
00:01:41,520 --> 00:01:44,920
 This verse is used

22
00:01:44,920 --> 00:01:49,360
 as a reminder of the inevitability of death.

23
00:01:50,240 --> 00:01:53,900
 And so it's often used at giving discourses and used in

24
00:01:53,900 --> 00:01:57,560
 ceremonies in Buddhist, in traditional Buddhism.

25
00:01:57,560 --> 00:02:05,200
 So the story goes that there was a

26
00:02:05,200 --> 00:02:11,560
 man in Sawati

27
00:02:11,560 --> 00:02:18,280
 who, having listened to the Buddha's teaching, became very

28
00:02:20,240 --> 00:02:22,940
 convinced of the Buddha's greatness and the greatness of

29
00:02:22,940 --> 00:02:27,520
 the teaching to the point that he decided to go forth.

30
00:02:27,520 --> 00:02:29,520
 And it says, "Urang tatwa bambad tito"

31
00:02:29,520 --> 00:02:35,640
 He gave his heart to where he gave himself up to the

32
00:02:35,640 --> 00:02:38,280
 Buddhist asana

33
00:02:38,280 --> 00:02:40,560
 and went forth.

34
00:02:40,560 --> 00:02:45,320
 Now in a very short time he became quite sick.

35
00:02:46,760 --> 00:02:49,450
 For whatever reason he contracted some sort of illness and

36
00:02:49,450 --> 00:02:54,120
 had these boils all over his body, kind of like the chicken

37
00:02:54,120 --> 00:02:55,240
 pox or something.

38
00:02:55,240 --> 00:02:59,960
 They grew bigger and bigger and I believe it says

39
00:02:59,960 --> 00:03:05,160
 eventually they burst and he was covered in this filthy

40
00:03:05,160 --> 00:03:10,240
 fluid and even eventually his bones

41
00:03:10,240 --> 00:03:14,800
 broke, you know, just some horrible sickness that

42
00:03:15,720 --> 00:03:19,630
 totally destroyed his body to the point where he was about

43
00:03:19,630 --> 00:03:20,000
 to die.

44
00:03:20,000 --> 00:03:25,980
 But he became so violent and putrid that the monks didn't

45
00:03:25,980 --> 00:03:27,320
 want to take care of him.

46
00:03:27,320 --> 00:03:29,320
 So they left him alone and

47
00:03:29,320 --> 00:03:33,830
 he was lying on his bed in his own urine and feces without

48
00:03:33,830 --> 00:03:35,640
 anyone to look after him.

49
00:03:35,640 --> 00:03:38,880
 Now the Buddha

50
00:03:38,880 --> 00:03:42,480
 and the commentary here says that the Buddha

51
00:03:43,880 --> 00:03:47,600
 it's his nature, it's his

52
00:03:47,600 --> 00:03:55,560
 his way to twice a day to look out into the

53
00:03:55,560 --> 00:03:59,490
 world around him to see who would most benefit from his

54
00:03:59,490 --> 00:04:00,400
 teachings.

55
00:04:00,400 --> 00:04:03,680
 So sometimes he would look out

56
00:04:03,680 --> 00:04:08,860
 through the whole universe, looking around through the

57
00:04:08,860 --> 00:04:12,120
 whole of the earth and all the heavens and so on.

58
00:04:12,880 --> 00:04:16,620
 And then all the way back to where he was staying or

59
00:04:16,620 --> 00:04:19,880
 sometimes he would start in the monastery and work his way

60
00:04:19,880 --> 00:04:20,480
 out.

61
00:04:20,480 --> 00:04:26,280
 Whoever his mind alighted upon

62
00:04:26,280 --> 00:04:30,360
 as someone who could benefit from his teachings,

63
00:04:30,360 --> 00:04:32,720
 he would

64
00:04:32,720 --> 00:04:34,720
 make his way to them and

65
00:04:34,720 --> 00:04:37,680
 give them the teaching that they needed.

66
00:04:38,320 --> 00:04:43,310
 So on this day or on one day near the point where this monk

67
00:04:43,310 --> 00:04:45,360
 was getting closer to death,

68
00:04:45,360 --> 00:04:49,920
 the Buddha saw him in his mind's eye and

69
00:04:49,920 --> 00:04:52,940
 knew that besides him, there was no besides the Buddha

70
00:04:52,940 --> 00:04:53,280
 himself.

71
00:04:53,280 --> 00:04:56,200
 There was no one else who was willing to be his refuge or

72
00:04:56,200 --> 00:04:57,360
 who could be his refuge.

73
00:04:57,360 --> 00:05:01,800
 And so he headed out

74
00:05:01,800 --> 00:05:04,420
 into the monastery. This was in the same monastery as the

75
00:05:04,420 --> 00:05:07,600
 Buddha was staying and he walked out and he firstly went to

76
00:05:07,600 --> 00:05:07,840
 the

77
00:05:08,000 --> 00:05:10,000
 place where they

78
00:05:10,000 --> 00:05:12,000
 lit fires,

79
00:05:12,000 --> 00:05:15,050
 especially designated place in the monastery for lighting

80
00:05:15,050 --> 00:05:16,360
 fires and he lit a fire and

81
00:05:16,360 --> 00:05:18,800
 boiled some water.

82
00:05:18,800 --> 00:05:23,520
 And he brought the water to this monk's room and

83
00:05:23,520 --> 00:05:27,320
 proceeded to,

84
00:05:27,320 --> 00:05:30,260
 or began to help the monk to wash himself. And then when

85
00:05:30,260 --> 00:05:33,400
 the monks, of course, when they saw the Buddha come in,

86
00:05:33,400 --> 00:05:36,190
 they said, "Please, let us do that." And so they helped and

87
00:05:36,190 --> 00:05:36,960
 they helped and

88
00:05:37,400 --> 00:05:41,400
 with the monks' help, they were able to take his robes off

89
00:05:41,400 --> 00:05:44,080
 and wash his robes and wash him and wash the bed and

90
00:05:44,080 --> 00:05:47,240
 lie him back down in a clean robe.

91
00:05:47,240 --> 00:05:53,110
 And then the Buddha gave him this teaching. So this is a

92
00:05:53,110 --> 00:05:53,560
 teaching

93
00:05:53,560 --> 00:05:57,960
 specifically given to a very sick person and

94
00:05:57,960 --> 00:06:05,310
 you might even think that it's a little bit of a specific

95
00:06:05,310 --> 00:06:05,880
 teaching.

96
00:06:07,840 --> 00:06:10,600
 It's dedicated specifically for someone who is sick or

97
00:06:10,600 --> 00:06:12,320
 someone who is dying.

98
00:06:12,320 --> 00:06:18,980
 But I think I'd like to talk a little bit about this verse

99
00:06:18,980 --> 00:06:22,280
 and how, as usual, how it

100
00:06:22,280 --> 00:06:27,280
 relates to our practice. But I

101
00:06:27,280 --> 00:24:51,060
 think it's an important verse to understand. It's important

102
00:24:51,060 --> 00:06:35,600
 to understand that it has a broader reach than simply

103
00:06:35,920 --> 00:06:37,920
 dealing with sick people.

104
00:06:37,920 --> 00:06:42,160
 This very

105
00:06:42,160 --> 00:06:46,330
 short and brief teaching, this very brief teaching in the

106
00:06:46,330 --> 00:06:46,960
 Buddha,

107
00:06:46,960 --> 00:06:54,480
 happens to be the one of the

108
00:06:54,480 --> 00:06:59,330
 major reasons why people begin to practice the Buddha's

109
00:06:59,330 --> 00:07:00,720
 teaching. So

110
00:07:02,840 --> 00:07:05,650
 we ask ourselves, "What was the use of this teaching at

111
00:07:05,650 --> 00:07:08,920
 that time? How is it that this teaching helped that monk?"

112
00:07:08,920 --> 00:07:13,040
 Because according to the text here, he was able to become

113
00:07:13,040 --> 00:07:15,640
 an arahant as a result of listening to this teaching in the

114
00:07:15,640 --> 00:07:15,880
 Buddha.

115
00:07:15,880 --> 00:07:18,000
 Just this very simple teaching

116
00:07:18,000 --> 00:07:21,480
 was enough to set him on the path to become enlightened. So

117
00:07:21,480 --> 00:07:25,080
 you would understand that this was the beginning for that

118
00:07:25,080 --> 00:07:28,600
 monk and just as it's the beginning for all of us on

119
00:07:28,600 --> 00:07:31,080
 our path to

120
00:07:31,080 --> 00:07:33,080
 the realization of the truth.

121
00:07:33,080 --> 00:07:36,160
 If we think back to the Buddha's story himself,

122
00:07:36,160 --> 00:07:39,770
 one of the reasons why the Buddha went forth is because he

123
00:07:39,770 --> 00:07:43,480
 saw a very sick person and he realized that sickness was

124
00:07:43,480 --> 00:07:49,170
 something that he himself was subject to as well. He saw a

125
00:07:49,170 --> 00:07:52,370
 very old person. He saw a very sick person. He saw a dead

126
00:07:52,370 --> 00:07:52,720
 person.

127
00:07:52,720 --> 00:07:55,760
 And these were three things that moved him

128
00:07:56,160 --> 00:08:00,150
 to the point of wanting to find a way out of suffering, to

129
00:08:00,150 --> 00:08:01,720
 move him to realize that

130
00:08:01,720 --> 00:08:05,240
 there was no point in

131
00:08:05,240 --> 00:08:08,000
 pursuing

132
00:08:08,000 --> 00:08:10,000
 sensual pleasures or

133
00:08:10,000 --> 00:08:13,630
 things outside of the body or even pursuing the the

134
00:08:13,630 --> 00:08:15,280
 wellness of the body itself.

135
00:08:15,280 --> 00:08:19,840
 Even pursuing life itself. There was no point in trying to

136
00:08:19,840 --> 00:08:23,720
 extend life because eventually it would be all for naught.

137
00:08:23,720 --> 00:08:33,640
 And so on the first and most important way to understand

138
00:08:33,640 --> 00:08:36,680
 this verse is as an impetus for us to begin the practice.

139
00:08:36,680 --> 00:08:40,700
 So here we have this monk who was feeling useless, feeling

140
00:08:40,700 --> 00:08:41,000
 like

141
00:08:41,000 --> 00:08:45,520
 perhaps that maybe feeling afraid or feeling

142
00:08:45,520 --> 00:08:48,890
 great suffering and depression because of his sickness,

143
00:08:48,890 --> 00:08:50,440
 feeling like he was now

144
00:08:51,160 --> 00:08:53,700
 going to be unable to practice the Buddha's teaching until

145
00:08:53,700 --> 00:08:54,840
 the Buddha reminded him

146
00:08:54,840 --> 00:08:58,720
 of this,

147
00:08:58,720 --> 00:09:01,940
 of the importance of now of putting this teaching into

148
00:09:01,940 --> 00:09:05,000
 practice. And this allowed the monk to

149
00:09:05,000 --> 00:09:09,620
 approach things in a different way, to begin to look at the

150
00:09:09,620 --> 00:09:13,320
 body and the whole world as being not self,

151
00:09:13,320 --> 00:09:15,560
 as not belonging to us and

152
00:09:15,560 --> 00:09:18,280
 to begin to give it up.

153
00:09:18,760 --> 00:09:21,960
 This is how the Buddha began his quest to give up

154
00:09:21,960 --> 00:09:23,080
 attachment.

155
00:09:23,080 --> 00:09:27,640
 And it's a very common reason for people to

156
00:09:27,640 --> 00:09:32,780
 give up the world or to begin meditation practices when

157
00:09:32,780 --> 00:09:34,400
 they get sick or when they realize

158
00:09:34,400 --> 00:09:37,440
 that sickness, old age sickness and death

159
00:09:37,440 --> 00:09:41,540
 are an inevitable part of life. For some people this can be

160
00:09:41,540 --> 00:09:44,040
 a great crisis, the realization that one day

161
00:09:44,040 --> 00:09:46,000
 they are going to have to die and all the people that they

162
00:09:46,000 --> 00:09:48,120
 love and all the things that they

163
00:09:48,760 --> 00:09:50,760
 care for are going to leave them.

164
00:09:50,760 --> 00:09:58,280
 And it also happens to be the beginning of a much longer

165
00:09:58,280 --> 00:10:00,040
 teaching,

166
00:10:00,040 --> 00:10:02,520
 which

167
00:10:02,520 --> 00:10:07,310
 gets closer and closer and closer to a direct realization

168
00:10:07,310 --> 00:10:07,560
 of

169
00:10:07,560 --> 00:10:11,080
 impermanent suffering and non-self. So this is really the

170
00:10:11,080 --> 00:10:13,740
 beginning of the practice or one of the important

171
00:10:13,740 --> 00:10:16,640
 beginnings. If people ask, "Why should I practice

172
00:10:16,640 --> 00:10:17,600
 meditation?

173
00:10:17,600 --> 00:10:20,840
 Why should I torture myself? Why should I be concerned

174
00:10:20,840 --> 00:10:23,970
 about the purification of the mind, about understanding the

175
00:10:23,970 --> 00:10:24,280
 mind?

176
00:10:24,280 --> 00:10:26,280
 Why should I be concerned about

177
00:10:26,280 --> 00:10:28,600
 trying to

178
00:10:28,600 --> 00:10:31,240
 try to let go of attachment?

179
00:10:31,240 --> 00:10:37,200
 Why shouldn't I keep clinging to these things or pursuing

180
00:10:37,200 --> 00:10:39,760
 worldly

181
00:10:39,760 --> 00:10:41,760
 pleasures and so on?"

182
00:10:44,840 --> 00:10:48,920
 And if this sort of teaching that really answers this

183
00:10:48,920 --> 00:10:51,400
 question for us and

184
00:10:51,400 --> 00:10:54,320
 makes us, turns us away from

185
00:10:54,320 --> 00:10:58,270
 our pursuit of central pleasures because you begin to

186
00:10:58,270 --> 00:10:59,000
 realize that

187
00:10:59,000 --> 00:11:04,480
 let alone those things outside of us that we might pursue,

188
00:11:04,480 --> 00:11:07,760
 the things outside of us,

189
00:11:07,760 --> 00:11:14,080
 even our own body, our own being,

190
00:11:14,960 --> 00:11:18,000
 we can't say that it belongs to us. In the end, this is the

191
00:11:18,000 --> 00:11:20,640
 meaning of chudho, a beta vinyano.

192
00:11:20,640 --> 00:11:24,000
 The mind will eventually be gone from this body.

193
00:11:24,000 --> 00:11:28,230
 This very body, this very being that we hold to be ours,

194
00:11:28,230 --> 00:11:30,600
 that we hold to be ourselves,

195
00:11:30,600 --> 00:11:33,880
 this one thing that we cling to more than anything in the

196
00:11:33,880 --> 00:11:34,360
 world,

197
00:11:34,360 --> 00:11:38,520
 even this one thing, is in no way ours,

198
00:11:39,520 --> 00:11:43,510
 is in no way under our control. It's not something that we

199
00:11:43,510 --> 00:11:44,240
 can keep. At best,

200
00:11:44,240 --> 00:11:47,270
 you can say we're renting it at a high price, having to

201
00:11:47,270 --> 00:11:49,480
 feed and to clothe and to care for it,

202
00:11:49,480 --> 00:11:52,560
 and having to suffer a lot because of it.

203
00:11:52,560 --> 00:11:56,160
 So we're renting it, kind of like renting this thing

204
00:11:56,160 --> 00:11:58,430
 because we think it's going to bring us pleasure, or we're

205
00:11:58,430 --> 00:11:59,320
 trying to find pleasure

206
00:11:59,320 --> 00:12:01,320
 based with it,

207
00:12:01,320 --> 00:12:06,570
 until we come to realize that actually it's not worth the

208
00:12:06,570 --> 00:12:09,160
 cost, that it's actually not something that

209
00:12:09,800 --> 00:12:12,440
 is going to bring lasting peace and happiness.

210
00:12:12,440 --> 00:12:17,190
 It's not something that we can never fix, that we can never

211
00:12:17,190 --> 00:12:20,200
 hold on to. In fact, the nature of

212
00:12:20,200 --> 00:12:23,840
 reality is

213
00:12:23,840 --> 00:12:25,760
 nothing

214
00:12:25,760 --> 00:12:29,330
 can never possibly be so. There's nothing in this universe

215
00:12:29,330 --> 00:12:29,720
 which

216
00:12:29,720 --> 00:12:34,440
 when clung to will lead to happiness. The nature of

217
00:12:36,200 --> 00:12:38,980
 reality of objects, of possessions, is that they are

218
00:12:38,980 --> 00:12:42,480
 temporary, that they come and they go, that

219
00:12:42,480 --> 00:12:45,440
 all of the work that we put into

220
00:12:45,440 --> 00:12:47,880
 keeping them and maintaining them

221
00:12:47,880 --> 00:12:50,680
 is in the end,

222
00:12:50,680 --> 00:12:55,080
 leads to no lasting and sustainable purpose,

223
00:12:55,080 --> 00:13:00,080
 due to the change and the inevitability of losing

224
00:13:00,080 --> 00:13:03,400
 everything. So let alone all of the things in the world,

225
00:13:03,400 --> 00:13:05,400
 this very body that we have,

226
00:13:05,600 --> 00:13:08,840
 it's not even ourselves. The body is really the

227
00:13:08,840 --> 00:13:12,480
 the key possession, you know.

228
00:13:12,480 --> 00:13:16,220
 So when we talk about the body as being not self as the

229
00:13:16,220 --> 00:13:19,000
 body that's not being belonging to us, we also,

230
00:13:19,000 --> 00:13:24,250
 by extension, understand the rest of the universe, the rest

231
00:13:24,250 --> 00:13:25,080
 of the world to be so.

232
00:13:25,080 --> 00:13:27,800
 When the body is not

233
00:13:27,800 --> 00:13:30,200
 subject to our control, how could we possibly find

234
00:13:30,200 --> 00:13:33,080
 control and

235
00:13:33,920 --> 00:13:37,680
 stability in the things outside of the body?

236
00:13:37,680 --> 00:13:43,520
 So starting from here, this is where we begin this movement

237
00:13:43,520 --> 00:13:43,880
 inwards.

238
00:13:43,880 --> 00:13:46,880
 This is the first realization among many realizations,

239
00:13:46,880 --> 00:13:48,760
 where we come to realize that

240
00:13:48,760 --> 00:13:51,400
 let alone this

241
00:13:51,400 --> 00:13:56,080
 inevitability of death. There's actually the inevitability

242
00:13:56,080 --> 00:13:56,560
 of change

243
00:13:56,560 --> 00:13:59,800
 from

244
00:13:59,800 --> 00:14:02,440
 increasingly

245
00:14:04,360 --> 00:14:07,180
 shorter intervals. So when I was young, I was one way and

246
00:14:07,180 --> 00:14:10,800
 now I'm older and then I'm getting older.

247
00:14:10,800 --> 00:14:16,040
 When our body changes and when we begin to get sick,

248
00:14:16,040 --> 00:14:21,610
 when we begin to get old, when our physical appearance or

249
00:14:21,610 --> 00:14:22,960
 our physical

250
00:14:22,960 --> 00:14:26,960
 nature changes, when the mind changes, sometimes

251
00:14:28,160 --> 00:14:30,980
 when we find ourselves becoming addicted to something or

252
00:14:30,980 --> 00:14:33,120
 attached to something or we find ourselves

253
00:14:33,120 --> 00:14:36,680
 the mind changing, maybe forgetting things more and so on,

254
00:14:36,680 --> 00:14:41,240
 the change that comes in the mind will happen, and the body

255
00:14:41,240 --> 00:14:43,880
 in the mind will occur many times over the course of our

256
00:14:43,880 --> 00:14:44,120
 lives.

257
00:14:44,120 --> 00:14:47,920
 Eventually getting shorter and shorter and shorter until we

258
00:14:47,920 --> 00:14:51,760
 realize that actually this idea of death is

259
00:14:53,040 --> 00:14:55,940
 really just the tip of the iceberg and actually death is

260
00:14:55,940 --> 00:14:58,240
 something that occurs at every moment.

261
00:14:58,240 --> 00:15:03,320
 This teaching is something that begins us on this course to

262
00:15:03,320 --> 00:15:05,920
 look inwards until eventually we see that

263
00:15:05,920 --> 00:15:09,970
 every moment we're born and die, every moment things are

264
00:15:09,970 --> 00:15:10,920
 changing. There's

265
00:15:10,920 --> 00:15:13,680
 no

266
00:15:13,680 --> 00:15:16,520
 reality that exists more than one moment.

267
00:15:18,840 --> 00:15:23,840
 An ordinary person when they're sitting, for all of us,

268
00:15:23,840 --> 00:15:25,880
 when we come to practice meditation,

269
00:15:25,880 --> 00:15:30,580
 at first we see the body as being an entity. We think of it

270
00:15:30,580 --> 00:15:34,240
 as being a stable, lasting entity and

271
00:15:34,240 --> 00:15:38,840
 an ordinary person who has never thought about these things

272
00:15:38,840 --> 00:15:40,160
 will even

273
00:15:40,160 --> 00:15:42,620
 will generally give rise to this idea that this is

274
00:15:42,620 --> 00:15:45,320
 something that is going to last, something that I can

275
00:15:45,320 --> 00:15:45,960
 depend upon.

276
00:15:47,240 --> 00:15:49,720
 Then they will become negligent in terms of

277
00:15:49,720 --> 00:15:53,560
 clinging to it. They will expect it to be

278
00:15:53,560 --> 00:15:56,840
 there the next moment, the next day, the next month, the

279
00:15:56,840 --> 00:15:58,440
 next year, and they will plan things out.

280
00:15:58,440 --> 00:16:01,970
 So people will have plans for tomorrow, next month, next

281
00:16:01,970 --> 00:16:02,340
 year,

282
00:16:02,340 --> 00:16:06,160
 even many years off into the future. They have a retirement

283
00:16:06,160 --> 00:16:07,120
 plan, they have

284
00:16:07,120 --> 00:16:10,910
 very, very long, you know, having grandchildren and so on,

285
00:16:10,910 --> 00:16:12,460
 and just plans like this.

286
00:16:16,720 --> 00:16:19,720
 And then this realization comes that

287
00:16:19,720 --> 00:16:23,380
 eventually you're going to die and for some people it comes

288
00:16:23,380 --> 00:16:23,920
 quite quickly.

289
00:16:23,920 --> 00:16:25,920
 Suddenly they get sick and the doctor says they have a

290
00:16:25,920 --> 00:16:29,080
 month to live and this is an incredible shock that

291
00:16:29,080 --> 00:16:33,080
 changes their whole outlook on life. Suddenly from

292
00:16:33,080 --> 00:16:35,080
 from

293
00:16:35,080 --> 00:16:38,880
 months or years it becomes days. What am I going to do

294
00:16:38,880 --> 00:16:39,640
 tomorrow?

295
00:16:39,640 --> 00:16:43,440
 Realizing that

296
00:16:43,840 --> 00:16:47,600
 you can't cling to things beyond maybe the next few days or

297
00:16:47,600 --> 00:16:50,040
 the next few weeks or even months.

298
00:16:50,040 --> 00:16:54,430
 That's how we begin to come and practice meditation with

299
00:16:54,430 --> 00:16:55,560
 these sorts of

300
00:16:55,560 --> 00:16:58,520
 realizations, realizing that we can't possibly find

301
00:16:58,520 --> 00:17:01,080
 satisfaction and in the end we do have to come to terms

302
00:17:01,080 --> 00:17:01,920
 with

303
00:17:01,920 --> 00:17:05,060
 change, with the inevitability of losing the things that we

304
00:17:05,060 --> 00:17:05,760
 cling to.

305
00:17:05,760 --> 00:17:09,520
 Once we begin to practice meditation,

306
00:17:09,520 --> 00:17:11,520
 we

307
00:17:11,520 --> 00:17:15,600
 take this to the next step of starting to see

308
00:17:15,600 --> 00:17:18,890
 the mind is changing from moment to moment, the body is

309
00:17:18,890 --> 00:17:21,280
 changing from moment to moment, that even sitting here

310
00:17:21,280 --> 00:17:23,880
 our experience determines

311
00:17:23,880 --> 00:17:26,520
 the reality.

312
00:17:26,520 --> 00:17:30,040
 So the reality is not this body lasting from one moment to

313
00:17:30,040 --> 00:17:32,240
 moment, not even from

314
00:17:32,240 --> 00:17:35,880
 moment to moment, let alone from days, day to day or week

315
00:17:35,880 --> 00:17:37,080
 to week or months to month.

316
00:17:37,080 --> 00:17:40,630
 It's not that we're going to die in days or weeks or months

317
00:17:40,630 --> 00:17:41,120
 or years.

318
00:17:41,720 --> 00:17:45,450
 It's that actually the we, the body is something that dies,

319
00:17:45,450 --> 00:17:48,000
 is born and dies at every moment.

320
00:17:48,000 --> 00:17:50,590
 As we begin to practice meditation you'll begin to see this

321
00:17:50,590 --> 00:17:53,160
, you'll begin to see that actually there is no body.

322
00:17:53,160 --> 00:17:55,400
 There is only the experience.

323
00:17:55,400 --> 00:17:58,480
 So when you look at another person's body, look at your

324
00:17:58,480 --> 00:17:58,920
 body,

325
00:17:58,920 --> 00:18:01,970
 then you see and at that moment the body arises as a

326
00:18:01,970 --> 00:18:05,320
 thought in the mind based on the image that you see.

327
00:18:05,320 --> 00:18:08,520
 When you experience the tension in the legs, the tension in

328
00:18:08,520 --> 00:18:10,000
 the back of sitting here,

329
00:18:10,440 --> 00:18:13,470
 that's when the body arises. There's the mind and the body

330
00:18:13,470 --> 00:18:14,360
 in that moment.

331
00:18:14,360 --> 00:18:17,970
 Once the mind moves somewhere else to hearing or seeing or

332
00:18:17,970 --> 00:18:21,280
 so on, that experience disappears and

333
00:18:21,280 --> 00:18:24,850
 therefore the body disappears. The body is at that moment

334
00:18:24,850 --> 00:18:26,960
 gone. The reality of the body is

335
00:18:26,960 --> 00:18:30,160
 ceased.

336
00:18:30,160 --> 00:18:37,820
 And so this sort of realization starts with what is

337
00:18:37,820 --> 00:18:39,040
 explained in this verse.

338
00:18:39,040 --> 00:18:41,210
 So that's really the importance of this verse. We should

339
00:18:41,210 --> 00:18:44,760
 understand it as the beginnings of Buddhist practice or as

340
00:18:44,760 --> 00:18:47,400
 an example of how we begin Buddhist practice.

341
00:18:47,400 --> 00:18:52,160
 We begin by accepting these very simple truths of life that

342
00:18:52,160 --> 00:18:54,480
 death is inevitable. This body,

343
00:18:54,480 --> 00:18:59,240
 anyone who clings to this body has to face the question of

344
00:18:59,240 --> 00:19:05,210
 what they're going to do when at death or what is the

345
00:19:05,210 --> 00:19:06,600
 purpose of clinging

346
00:19:06,960 --> 00:19:10,370
 when we're going to lose this body in the end. Why would

347
00:19:10,370 --> 00:19:12,720
 you worry? Why are you worrying about this body?

348
00:19:12,720 --> 00:19:14,720
 Why are you concerned with it? Why are you

349
00:19:14,720 --> 00:19:18,700
 attached to it when it's not obviously going to bring you

350
00:19:18,700 --> 00:19:20,000
 lasting satisfaction?

351
00:19:20,000 --> 00:19:23,870
 In the end you're going to lose, in the end this body is

352
00:19:23,870 --> 00:19:25,000
 going to lie like a useless log.

353
00:19:25,000 --> 00:19:28,120
 It's a vivid image for people who cling to the body because

354
00:19:28,120 --> 00:19:33,910
 we tend not to like to think of our body as something that

355
00:19:33,910 --> 00:19:34,940
 we have to throw away.

356
00:19:35,040 --> 00:19:37,640
 We try our best to not have to lose the body,

357
00:19:37,640 --> 00:19:40,160
 to care for it and concerned about it.

358
00:19:40,160 --> 00:19:43,190
 And we'll be concerned about all the limbs and organs and

359
00:19:43,190 --> 00:19:46,000
 if we lose a finger, if we lose a hand, if we lose a

360
00:19:46,000 --> 00:19:49,960
 leg, you know, this would be travesty. If we go blind, if

361
00:19:49,960 --> 00:19:53,240
 we go deaf, if we lose a tooth, even if we lose a tooth,

362
00:19:53,240 --> 00:19:55,240
 it's an incredible

363
00:19:55,240 --> 00:19:58,640
 travesty for us because

364
00:20:00,960 --> 00:20:04,310
 we cling to this body as being me, as being mine, as being

365
00:20:04,310 --> 00:20:07,000
 stable. As the last verse said, it's like a pot and these

366
00:20:07,000 --> 00:20:07,520
 verses

367
00:20:07,520 --> 00:20:11,640
 clearly go together. They're along the same sort of theme.

368
00:20:11,640 --> 00:20:16,790
 That the body is fragile and in the end it's going to break

369
00:20:16,790 --> 00:20:19,400
 and become useless like a charred log.

370
00:20:19,400 --> 00:20:23,340
 Once we begin on this sort of introspection and many other

371
00:20:23,340 --> 00:20:26,240
 introspections, the idea that

372
00:20:26,240 --> 00:20:29,360
 habits are

373
00:20:29,720 --> 00:20:32,660
 can change the whole of your being, for example. This

374
00:20:32,660 --> 00:20:34,240
 realization that clinging leads to

375
00:20:34,240 --> 00:20:39,100
 craving leads to clinging leads to suffering and so on. The

376
00:20:39,100 --> 00:20:41,680
 realization that the way we're carrying out our life is

377
00:20:41,680 --> 00:20:41,960
 unsustainable.

378
00:20:41,960 --> 00:20:44,600
 But in the end we're going to lose

379
00:20:44,600 --> 00:20:49,200
 we're going to lose this due to the inevitability of things

380
00:20:49,200 --> 00:20:50,800
 like death and change.

381
00:20:50,800 --> 00:20:54,840
 And this leads us inward. It leads us to approach life from

382
00:20:54,840 --> 00:20:56,680
 a different point of view.

383
00:20:56,680 --> 00:21:00,410
 And it's the beginning of a longer course of practice to

384
00:21:00,410 --> 00:21:00,720
 see

385
00:21:00,720 --> 00:21:04,920
 not only that death is inevitable, but that death occurs at

386
00:21:04,920 --> 00:21:05,480
 every moment.

387
00:21:05,480 --> 00:21:09,870
 And that not only can we not cling to things next year or

388
00:21:09,870 --> 00:21:10,920
 ten years from now,

389
00:21:10,920 --> 00:21:13,640
 we can't even cling to things in the next moment.

390
00:21:13,640 --> 00:21:16,830
 That they're going to change and they could come at any

391
00:21:16,830 --> 00:21:19,280
 time. Death could come at any moment.

392
00:21:19,280 --> 00:21:21,700
 We don't know what the experience is going to be in the

393
00:21:21,700 --> 00:21:23,200
 next moment. Once we begin to practice

394
00:21:24,000 --> 00:21:27,220
 meditation, we will begin to see the mind is changing, the

395
00:21:27,220 --> 00:21:30,280
 body is changing and this happens, in the beginning it

396
00:21:30,280 --> 00:21:31,200
 occurs in terms of

397
00:21:31,200 --> 00:21:33,760
 day by day, right? So you come here to practice meditation

398
00:21:33,760 --> 00:21:35,440
 and today was a really good day.

399
00:21:35,440 --> 00:21:38,420
 So you come and tell me about it and they're very happy and

400
00:21:38,420 --> 00:21:40,400
 then the next day you come back and tell me how horrible it

401
00:21:40,400 --> 00:21:40,640
 was.

402
00:21:40,640 --> 00:21:43,180
 You can't understand how all of a sudden today was a very,

403
00:21:43,180 --> 00:21:43,800
 very bad day.

404
00:21:43,800 --> 00:21:47,330
 Or today was a very bad day and you're thinking of quitting

405
00:21:47,330 --> 00:21:48,880
 and then tomorrow is a very good day.

406
00:21:49,400 --> 00:21:52,470
 And then this happens again and again until finally you

407
00:21:52,470 --> 00:21:56,090
 realize that this is the nature of life, that you can't

408
00:21:56,090 --> 00:21:56,680
 depend on it.

409
00:21:56,680 --> 00:21:59,250
 And then you begin to look closer and closer until you

410
00:21:59,250 --> 00:22:00,160
 realize that it's

411
00:22:00,160 --> 00:22:03,820
 first meditation practice to meditation practice. You can't

412
00:22:03,820 --> 00:22:05,240
 expect it to be the same.

413
00:22:05,240 --> 00:22:09,530
 You can't cling to even a single meditation practice as

414
00:22:09,530 --> 00:22:10,760
 being dependable.

415
00:22:10,760 --> 00:22:13,090
 The next meditation practice might be totally different and

416
00:22:13,090 --> 00:22:15,160
 then eventually it gets to the point of

417
00:22:15,480 --> 00:22:17,900
 realizing you can't cling from one moment to the next and

418
00:22:17,900 --> 00:22:20,280
 eventually this is how you would let go of everything.

419
00:22:20,280 --> 00:22:23,340
 In the end you're able to let go of not only your body but

420
00:22:23,340 --> 00:22:26,400
 the mind as well and not be concerned about any experience.

421
00:22:26,400 --> 00:22:30,600
 Realizing that the true cause of suffering is our

422
00:22:30,600 --> 00:22:32,200
 attachment to things.

423
00:22:32,200 --> 00:22:36,240
 So it's this sort of realization that sets us on them.

424
00:22:36,240 --> 00:22:40,350
 This was the benefit of this verse, especially for this

425
00:22:40,350 --> 00:22:42,600
 monk who was being sick, was to remind him and

426
00:22:43,160 --> 00:22:46,040
 to alert him to the fact that this was

427
00:22:46,040 --> 00:22:49,640
 real and was a serious

428
00:22:49,640 --> 00:22:53,930
 fact of life that he was going to have to face now and to

429
00:22:53,930 --> 00:22:55,840
 set him on the right path and help him to

430
00:22:55,840 --> 00:22:58,720
 come to terms with this and to not be lax.

431
00:22:58,720 --> 00:23:01,040
 When people are sick they'll often,

432
00:23:01,040 --> 00:23:04,020
 they think that we think the correct response is to get sad

433
00:23:04,020 --> 00:23:07,040
 and upset and cry and depressed and angry and

434
00:23:07,040 --> 00:23:10,720
 cry and cry out for medicines and

435
00:23:11,600 --> 00:23:15,380
 you know or maybe even cling to, cling more to happy things

436
00:23:15,380 --> 00:23:15,880
 and say well

437
00:23:15,880 --> 00:23:17,800
 I've got to get, now I've only got so many days to live I

438
00:23:17,800 --> 00:23:19,680
 got to get as much happiness in

439
00:23:19,680 --> 00:23:22,250
 before I die not realizing that this is going to make you

440
00:23:22,250 --> 00:23:24,680
 cling more and more and be less and less satisfied.

441
00:23:24,680 --> 00:23:30,590
 And so, so, you know, for the most part people get

442
00:23:30,590 --> 00:23:31,240
 depressed and

443
00:23:31,240 --> 00:23:34,310
 upset because they're not going to be able to cling to the

444
00:23:34,310 --> 00:23:35,680
 things that they're clinging to.

445
00:23:35,680 --> 00:23:38,360
 And so here's the Buddha reminding him that

446
00:23:39,560 --> 00:23:43,090
 it's too serious for us to be lax and negligent in this way

447
00:23:43,090 --> 00:23:44,040
. Realize that

448
00:23:44,040 --> 00:23:47,080
 in the end the body, this body will lie on the earth.

449
00:23:47,080 --> 00:23:50,080
 It's something that we can always come back and think about

450
00:23:50,080 --> 00:23:52,040
. It's something always useful to come back and

451
00:23:52,040 --> 00:23:56,470
 remind yourself that this body is like a log. Eventually it

452
00:23:56,470 --> 00:24:00,960
's going to fall and lie useless on the earth just as a log.

453
00:24:00,960 --> 00:24:04,160
 It's something that

454
00:24:04,160 --> 00:24:07,020
 there's a reason why this verse became, has become quite

455
00:24:07,020 --> 00:24:11,120
 famous because it's a good meditation to use to remind

456
00:24:11,120 --> 00:24:12,680
 yourself from time to time

457
00:24:12,680 --> 00:24:15,360
 whenever you're clinging to the body or worried about the

458
00:24:15,360 --> 00:24:16,840
 body or when you find yourself.

459
00:24:16,840 --> 00:24:21,040
 Or you can use it to test yourself to point out to yourself

460
00:24:21,040 --> 00:24:22,640
 how ridiculous is your

461
00:24:22,640 --> 00:24:25,790
 love and your attachment to your body as being beautiful

462
00:24:25,790 --> 00:24:27,160
 and wonderful and

463
00:24:27,160 --> 00:24:30,720
 a source of stability and happiness and so on.

464
00:24:30,720 --> 00:24:32,800
 So,

465
00:24:32,800 --> 00:24:35,400
 just one more verse. We're going through these one by one

466
00:24:35,400 --> 00:24:35,600
 and

467
00:24:35,600 --> 00:24:38,710
 using each of them to give us a little bit more food for

468
00:24:38,710 --> 00:24:41,640
 thought and explore the Buddha's teaching from

469
00:24:41,640 --> 00:24:45,120
 one more point of view. So thanks for tuning in. Thanks for

470
00:24:45,120 --> 00:24:46,080
 listening and

471
00:24:46,080 --> 00:24:49,000
 until next time, back to our practice.

472
00:24:49,000 --> 00:25:15,080
 [

