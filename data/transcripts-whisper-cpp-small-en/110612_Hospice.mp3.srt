1
00:00:00,000 --> 00:00:07,210
 Welcome back to Ask a Monk. Today I will be talking about

2
00:00:07,210 --> 00:00:14,000
 dealing with terminally ill patients, hospice care, and

3
00:00:14,000 --> 00:00:22,020
 some of the ways that I would think one can approach people

4
00:00:22,020 --> 00:00:26,250
 in this condition and help them through the practice of the

5
00:00:26,250 --> 00:00:27,000
 meditation.

6
00:00:27,000 --> 00:00:34,410
 The first thing to note or to discuss is in regards to

7
00:00:34,410 --> 00:00:42,330
 general practices in terms of teaching meditation, whether

8
00:00:42,330 --> 00:00:45,770
 it's terminally ill, it doesn't just relate to this

9
00:00:45,770 --> 00:00:50,260
 specific group of people, but in any case when you're

10
00:00:50,260 --> 00:00:53,600
 teaching meditation there are certain principles that you

11
00:00:53,600 --> 00:00:55,000
 have to keep in mind.

12
00:00:55,000 --> 00:00:58,960
 The first one is that any time that you're teaching

13
00:00:58,960 --> 00:01:03,600
 meditation you should also be mindful yourself and that's

14
00:01:03,600 --> 00:01:10,000
 probably the most important principle to keep in mind that

15
00:01:10,000 --> 00:01:18,050
 it's not good enough to simply know the principles and to

16
00:01:18,050 --> 00:01:21,000
 have book theory.

17
00:01:21,000 --> 00:01:26,280
 In order to teach and in order to be effective, in order to

18
00:01:26,280 --> 00:01:31,560
 really be able to respond and interact with the patient,

19
00:01:31,560 --> 00:01:36,000
 the student, one has to be practicing.

20
00:01:36,000 --> 00:01:39,770
 So at the time when you're teaching, at the time when you

21
00:01:39,770 --> 00:01:43,810
're speaking even, you should try to remind yourself of the

22
00:01:43,810 --> 00:01:49,380
 things that you're saying. Otherwise you fall into all

23
00:01:49,380 --> 00:01:55,360
 sorts of biases and emotions and you become stressed

24
00:01:55,360 --> 00:02:01,000
 yourself when the person doesn't respond favorably or you

25
00:02:01,000 --> 00:02:04,000
 can get caught up in your own emotions,

26
00:02:04,000 --> 00:02:10,510
 you can go overboard, you can become overconfident and over

27
00:02:10,510 --> 00:02:16,680
bearing and it becomes difficult to remain objective and

28
00:02:16,680 --> 00:02:20,000
 impartial and impart the truth.

29
00:02:20,000 --> 00:02:24,640
 I mean the truth is something that is very difficult to

30
00:02:24,640 --> 00:02:29,810
 understand and very difficult to keep in mind. It's easy

31
00:02:29,810 --> 00:02:34,480
 for us to slip off into illusion and fantasy and give the

32
00:02:34,480 --> 00:02:36,000
 wrong advice.

33
00:02:36,000 --> 00:02:40,800
 When we're teaching we want to give something useful, we

34
00:02:40,800 --> 00:02:45,400
 want to be appreciated and so if we're not careful we can

35
00:02:45,400 --> 00:02:50,400
 easily slip into bias and prejudice and find ourselves just

36
00:02:50,400 --> 00:02:55,100
 saying something that sounds good or that is going to give

37
00:02:55,100 --> 00:02:59,000
 a favorable response from our listeners,

38
00:02:59,000 --> 00:03:02,570
 which you may not always be in line with the truth.

39
00:03:02,570 --> 00:03:07,000
 Something that sounds good, a feel good sort of philosophy,

40
00:03:07,000 --> 00:03:09,000
 it's not good enough.

41
00:03:09,000 --> 00:03:13,420
 And that leads to the second point that in general I would

42
00:03:13,420 --> 00:03:18,000
 recommend for anyone who's going to teach meditation,

43
00:03:18,000 --> 00:03:24,270
 besides practicing by yourself, have it clearly in mind

44
00:03:24,270 --> 00:03:28,000
 that your approach to it should be a meditation of the mind

45
00:03:28,000 --> 00:03:28,000
.

46
00:03:28,000 --> 00:03:32,800
 It should be a meditative interaction with the person and

47
00:03:32,800 --> 00:03:37,500
 the only thing that you're giving to the person should be

48
00:03:37,500 --> 00:03:39,000
 information.

49
00:03:39,000 --> 00:03:46,180
 You shouldn't be giving them yourself, it shouldn't be

50
00:03:46,180 --> 00:03:53,820
 about you or your guru status, you shouldn't be giving them

51
00:03:53,820 --> 00:04:00,260
 an image or a presentation of some charismatic or

52
00:04:00,260 --> 00:04:03,000
 impressive personality.

53
00:04:03,000 --> 00:04:09,730
 The whole idea of guruship or impressing something on your

54
00:04:09,730 --> 00:04:16,610
 students is quite dangerous and it doesn't have the desired

55
00:04:16,610 --> 00:04:23,680
 effect. You can impress someone emotionally and I've tried

56
00:04:23,680 --> 00:04:28,000
 this or if you try it you can see for yourself the results.

57
00:04:28,000 --> 00:04:31,030
 The result is people have great faith in you but they don't

58
00:04:31,030 --> 00:04:34,240
 really understand what you're saying. They put more

59
00:04:34,240 --> 00:04:39,000
 emphasis on the messenger rather than the message.

60
00:04:39,000 --> 00:04:42,680
 And so it's easy to get caught up in this as well, the

61
00:04:42,680 --> 00:04:46,750
 giving yourself as the message and pumping people up and

62
00:04:46,750 --> 00:04:51,260
 giving them confidence and encouragement, but not really

63
00:04:51,260 --> 00:04:53,000
 giving them any skills.

64
00:04:53,000 --> 00:04:57,320
 It's much more useful than giving an image or a confidence

65
00:04:57,320 --> 00:05:01,640
 or encouragement and being a support for people to lean on

66
00:05:01,640 --> 00:05:05,000
 is helping them be a support for yourself.

67
00:05:05,000 --> 00:05:08,360
 That's I guess the third thing I would say is that you're

68
00:05:08,360 --> 00:05:11,790
 not there to be a support for them, you're there to teach

69
00:05:11,790 --> 00:05:16,180
 them how to support themselves, which is very often

70
00:05:16,180 --> 00:05:18,000
 overlooked.

71
00:05:18,000 --> 00:05:21,600
 We're much better at or much more inclined to be a support

72
00:05:21,600 --> 00:05:25,460
 for others and have them lean on us and depend on us, which

73
00:05:25,460 --> 00:05:29,690
 as I'm going to explain is really a part of the problem,

74
00:05:29,690 --> 00:05:31,000
 the dependency.

75
00:05:31,000 --> 00:05:34,840
 Once they can be strong, which is really the problem that

76
00:05:34,840 --> 00:05:38,920
 we face because we've gone our whole lives without building

77
00:05:38,920 --> 00:05:42,840
 ourselves up, without strengthening our minds and giving

78
00:05:42,840 --> 00:05:46,000
 ourselves the ability to deal with difficulty.

79
00:05:46,000 --> 00:05:50,720
 And so when it comes, we're still children. We act like

80
00:05:50,720 --> 00:05:55,470
 young children. We can cry, grown adults will cry and moan

81
00:05:55,470 --> 00:06:00,000
 and just want to take drugs and medication and so on.

82
00:06:00,000 --> 00:06:07,000
 So what you should be there to give is information.

83
00:06:07,000 --> 00:06:10,470
 Now the point about meditating is that you'll give the

84
00:06:10,470 --> 00:06:14,060
 right information and the information you give will be

85
00:06:14,060 --> 00:06:18,000
 unbiased, impartial and will be a service to the person.

86
00:06:18,000 --> 00:06:24,220
 Like you're there as a book or you're there as a kind of

87
00:06:24,220 --> 00:06:28,630
 like a coach, someone to stop you when you're going in the

88
00:06:28,630 --> 00:06:32,000
 wrong direction and push you in the right direction and

89
00:06:32,000 --> 00:06:33,000
 encourage you.

90
00:06:33,000 --> 00:06:37,090
 But because you're mindful, because you're there, you're

91
00:06:37,090 --> 00:06:41,250
 present, you're in the present moment, you're able to

92
00:06:41,250 --> 00:06:46,280
 respond and you're able to catch the emotions of the other

93
00:06:46,280 --> 00:06:49,300
 person and you're able to see the state of their mind, feel

94
00:06:49,300 --> 00:06:53,100
 and experience it and react appropriately and interact with

95
00:06:53,100 --> 00:06:57,000
 the situation without tangling your own emotions up in it.

96
00:06:57,000 --> 00:07:01,700
 So stay mindful, but don't give impressions, give

97
00:07:01,700 --> 00:07:07,260
 information and continue to give information and constantly

98
00:07:07,260 --> 00:07:12,170
 give unbiased information, telling people the results of

99
00:07:12,170 --> 00:07:15,350
 this act, not saying don't do this, don't do that, but

100
00:07:15,350 --> 00:07:18,750
 explaining to them why it's better to do this and better to

101
00:07:18,750 --> 00:07:20,000
 do that and so on.

102
00:07:20,000 --> 00:07:23,330
 So these are general principles that I would follow in

103
00:07:23,330 --> 00:07:28,390
 terms of the meditation. First, be mindful. Second, try to

104
00:07:28,390 --> 00:07:33,270
 give information rather than some kind of feeling, because

105
00:07:33,270 --> 00:07:34,860
 it's the information that they will use and it's the

106
00:07:34,860 --> 00:07:36,000
 technique that they will use.

107
00:07:36,000 --> 00:07:39,460
 It has to come from their feeling. It has to be their

108
00:07:39,460 --> 00:07:44,560
 emotion. It has to be their volition and their courage in

109
00:07:44,560 --> 00:07:47,000
 themselves. They have to be looking for it.

110
00:07:47,000 --> 00:07:51,540
 If you are pushing the meditation on them and they're not

111
00:07:51,540 --> 00:07:56,910
 there, it's not coming from their heart, they'll do it to

112
00:07:56,910 --> 00:08:02,000
 keep you there, to impress you and so on.

113
00:08:02,000 --> 00:08:05,410
 And when you're gone and even when you're there, it won't

114
00:08:05,410 --> 00:08:09,300
 be from the heart and it won't have the desired results. So

115
00:08:09,300 --> 00:08:16,430
 you have to step back and let their heart come forth, let

116
00:08:16,430 --> 00:08:20,000
 their intention come forth.

117
00:08:20,000 --> 00:08:30,390
 And thirdly, the point is to make themselves reliant. So by

118
00:08:30,390 --> 00:08:34,680
 stepping back, by just giving them information and not

119
00:08:34,680 --> 00:08:39,010
 bringing your own ego into the equation, you allow them to

120
00:08:39,010 --> 00:08:42,340
 step up to the plate and take their own future into their

121
00:08:42,340 --> 00:08:43,000
 hands.

122
00:08:43,000 --> 00:08:47,970
 But this is more pronounced, I think, with people who are

123
00:08:47,970 --> 00:08:51,950
 in great suffering. First of all, the one thing I'd say

124
00:08:51,950 --> 00:08:54,720
 about people who are in great suffering, there's an

125
00:08:54,720 --> 00:08:58,000
 advantage and a disadvantage that they have.

126
00:08:58,000 --> 00:09:01,990
 The advantage is they see things that many of us don't see.

127
00:09:01,990 --> 00:09:06,000
 They are experiencing the reason for practicing meditation.

128
00:09:06,000 --> 00:09:13,000
 And in practice, one of the great reasons anyway is because

129
00:09:13,000 --> 00:09:19,280
 of the eventuality or the danger that we all face, that we

130
00:09:19,280 --> 00:09:21,000
 might be in this situation at some point.

131
00:09:21,000 --> 00:09:24,140
 Well, they're in it and they can see the need to meditate

132
00:09:24,140 --> 00:09:27,220
 or the need to do something. They're looking for a way out

133
00:09:27,220 --> 00:09:28,000
 of suffering.

134
00:09:28,000 --> 00:09:30,940
 They have the suffering that many of us are blind to. We

135
00:09:30,940 --> 00:09:35,030
 forget exists. And so as a result, we have no strength

136
00:09:35,030 --> 00:09:39,520
 afforded to the mind. And when it comes, we like them, like

137
00:09:39,520 --> 00:09:44,000
 people who are in it already. We're unable to deal with it.

138
00:09:44,000 --> 00:09:47,690
 So they're looking for it. This is the advantage is that

139
00:09:47,690 --> 00:09:51,230
 the people who are suffering greatly at the end of their

140
00:09:51,230 --> 00:09:56,210
 lives, people who are terminally ill, they're not looking

141
00:09:56,210 --> 00:10:02,000
 to pass the time or to seek entertainment and so on.

142
00:10:02,000 --> 00:10:04,600
 They have a problem and they're looking to fix it. They're

143
00:10:04,600 --> 00:10:06,000
 looking to find a solution.

144
00:10:06,000 --> 00:10:09,760
 So they can be a really good audience in this sense. And

145
00:10:09,760 --> 00:10:13,680
 this is why in this case giving information is often enough

146
00:10:13,680 --> 00:10:14,000
.

147
00:10:14,000 --> 00:10:17,220
 But it has to be confident information and you have to be

148
00:10:17,220 --> 00:10:20,510
 able to give it in a way that they understand. You're not

149
00:10:20,510 --> 00:10:23,120
 just giving them a book to read or you're not just reading

150
00:10:23,120 --> 00:10:24,000
 a book to them.

151
00:10:24,000 --> 00:10:26,250
 You're explaining to them and you're going through it. I'll

152
00:10:26,250 --> 00:10:29,070
 try to explain basically what the sort of things that I

153
00:10:29,070 --> 00:10:30,000
 would teach.

154
00:10:30,000 --> 00:10:34,940
 Now the disadvantage with teaching people who are termin

155
00:10:34,940 --> 00:10:39,750
ally ill and so on is because you're often fighting with an

156
00:10:39,750 --> 00:10:44,000
 alternative or a different way of treatment.

157
00:10:44,000 --> 00:10:50,260
 So the treatment in hospitals, which quite often has to do

158
00:10:50,260 --> 00:10:56,200
 with medication, will very often get in the way and it's

159
00:10:56,200 --> 00:11:02,290
 going to be something that you'll have to work around and

160
00:11:02,290 --> 00:11:07,000
 it's always going to lessen the effects of the meditation.

161
00:11:07,000 --> 00:11:11,280
 People who are on medication will have a very difficult

162
00:11:11,280 --> 00:11:17,820
 time. Medication based on or medication of the sort that is

163
00:11:17,820 --> 00:11:23,310
 meant to relieve pain or dull the pain is a real hindrance

164
00:11:23,310 --> 00:11:25,000
 to meditation practice.

165
00:11:25,000 --> 00:11:30,310
 Because not only does it dull the mind and muddle the mind

166
00:11:30,310 --> 00:11:36,000
 but it also reinforces the avoidance of the difficulty.

167
00:11:36,000 --> 00:11:40,120
 Just like alcohol or recreational drugs, it's a form of

168
00:11:40,120 --> 00:11:41,000
 escape.

169
00:11:41,000 --> 00:11:45,250
 So not only does it hurt the body and affect our brain's

170
00:11:45,250 --> 00:11:49,970
 ability to process information but it also affects the mind

171
00:11:49,970 --> 00:11:55,000
's willingness and ability to deal with pain and suffering.

172
00:11:55,000 --> 00:11:57,000
 It sends you on the wrong direction.

173
00:11:57,000 --> 00:12:00,190
 So I would say one of the first things that you should

174
00:12:00,190 --> 00:12:03,690
 explain to people who are on medication or who are in this

175
00:12:03,690 --> 00:12:07,340
 position, hopefully people who haven't yet decided what

176
00:12:07,340 --> 00:12:11,200
 form of treatment they're going to take, is explaining the

177
00:12:11,200 --> 00:12:13,000
 differences in the treatment.

178
00:12:13,000 --> 00:12:18,170
 Yes, medication is something that is going to solve the

179
00:12:18,170 --> 00:12:23,230
 problem in the short term but in the long term it's going

180
00:12:23,230 --> 00:12:28,780
 to lead to a dependence and an addiction and the only way

181
00:12:28,780 --> 00:12:33,300
 to have it effectively work is to drug you up to the point

182
00:12:33,300 --> 00:12:35,000
 where you're unconscious.

183
00:12:35,000 --> 00:12:38,140
 Because it's going to have less and less of an effect, your

184
00:12:38,140 --> 00:12:41,100
 body is going to become more tolerant to the drugs and so

185
00:12:41,100 --> 00:12:43,000
 you'll need more and more and so on.

186
00:12:43,000 --> 00:12:46,500
 The pain is going to become more intense and your aversion

187
00:12:46,500 --> 00:12:49,920
 to the pain will become more intense. I had a direct

188
00:12:49,920 --> 00:12:53,840
 experience, my grandmother was quite ill and they drugged

189
00:12:53,840 --> 00:12:56,560
 her up in a nursing home to the point where she couldn't

190
00:12:56,560 --> 00:12:58,000
 even recognize people.

191
00:12:58,000 --> 00:13:01,780
 Because this was their way of dealing with it, it was laz

192
00:13:01,780 --> 00:13:05,660
iness and sloppiness and it was negligence. When she had

193
00:13:05,660 --> 00:13:08,880
 pain and she complained about it, they just increased her

194
00:13:08,880 --> 00:13:12,000
 medication rather than going to see a doctor or so on.

195
00:13:12,000 --> 00:13:15,590
 They just increased it to the point where she still had no

196
00:13:15,590 --> 00:13:18,970
 pain but she was more or less unconscious to the world

197
00:13:18,970 --> 00:13:20,000
 around her.

198
00:13:20,000 --> 00:13:23,360
 When this was realized, they took her off the medication

199
00:13:23,360 --> 00:13:27,490
 but then she was in incredible suffering. She wasn't able

200
00:13:27,490 --> 00:13:32,890
 to deal with this pain that she had been teaching herself

201
00:13:32,890 --> 00:13:38,670
 to avoid, to run away from, to stop, to escape through the

202
00:13:38,670 --> 00:13:40,000
 medication.

203
00:13:40,000 --> 00:13:45,600
 As a result, she was in terrible suffering that was most

204
00:13:45,600 --> 00:13:49,000
 difficult for her to deal with.

205
00:13:49,000 --> 00:13:55,450
 We should explain this and try to make it clear that there

206
00:13:55,450 --> 00:14:01,550
 are alternatives. It's actually in our better interest to

207
00:14:01,550 --> 00:14:06,000
 come to terms with the pain and to die with a clear mind.

208
00:14:06,000 --> 00:14:11,680
 If we're going to die, our minds should be clear, we should

209
00:14:11,680 --> 00:14:14,950
 know what our do we're doing, we should have the ability to

210
00:14:14,950 --> 00:14:17,380
 find closure with our relatives and so on with a clear and

211
00:14:17,380 --> 00:14:18,000
 alert mind.

212
00:14:18,000 --> 00:14:23,520
 Give confidence that there is another way and explain

213
00:14:23,520 --> 00:14:28,000
 basically the theory of the meditation.

214
00:14:28,000 --> 00:14:31,420
 Here goes exactly the sorts of things that I would say to

215
00:14:31,420 --> 00:14:34,940
 them. I would talk to them, get right to the point about

216
00:14:34,940 --> 00:14:40,380
 pain and say that this is going to be our training in the

217
00:14:40,380 --> 00:14:45,640
 meditation practice to deal with the pain or to approach

218
00:14:45,640 --> 00:14:50,000
 the pain and to change the way we look at the pain.

219
00:14:50,000 --> 00:14:56,530
 You explain first about the nature of feelings, that the

220
00:14:56,530 --> 00:15:03,390
 physical feeling is actually not really the problem. The

221
00:15:03,390 --> 00:15:08,880
 problem is our inability to accept it, the fact that it

222
00:15:08,880 --> 00:15:10,000
 bothers us.

223
00:15:10,000 --> 00:15:13,230
 Explain to people how when you actually look at the pain

224
00:15:13,230 --> 00:15:16,530
 and when you're actually there with it, you can see that

225
00:15:16,530 --> 00:15:19,690
 there's two things. There's the pain and then there's your

226
00:15:19,690 --> 00:15:22,000
 aversion to the pain and these are quite separate.

227
00:15:22,000 --> 00:15:26,900
 Encourage people to look at it, to examine it and to become

228
00:15:26,900 --> 00:15:29,000
 comfortable with it.

229
00:15:29,000 --> 00:15:32,840
 Explain that as you look at the pain and as you examine it,

230
00:15:32,840 --> 00:15:37,000
 you're able to see that it's something that comes and goes.

231
00:15:37,000 --> 00:15:40,000
 It actually has no effect on the mind.

232
00:15:40,000 --> 00:15:45,060
 The problem is that we've developed this wrong idea that

233
00:15:45,060 --> 00:15:50,230
 there's something bad about the pain based on our worries

234
00:15:50,230 --> 00:15:55,330
 and our feeling that somehow it's going to lead to injury

235
00:15:55,330 --> 00:16:00,030
 or it means something more significant in terms of an

236
00:16:00,030 --> 00:16:03,000
 illness or causing death.

237
00:16:03,000 --> 00:16:07,020
 Eventually to the point where any little pain causes

238
00:16:07,020 --> 00:16:11,220
 suffering in our mind. We can explain to people that we're

239
00:16:11,220 --> 00:16:15,140
 actually causing the suffering ourselves by our aversion to

240
00:16:15,140 --> 00:16:16,000
 the pain.

241
00:16:16,000 --> 00:16:21,060
 Explain to people the relationship between the experience

242
00:16:21,060 --> 00:16:25,480
 and the suffering, how in the beginning there is the

243
00:16:25,480 --> 00:16:30,200
 tension and the pressure in the body, the body being in a

244
00:16:30,200 --> 00:16:34,460
 specific position and the pressure build up and the

245
00:16:34,460 --> 00:16:37,000
 stiffness build up and so on.

246
00:16:37,000 --> 00:16:40,980
 Then there arises this painful feeling. Once there arises

247
00:16:40,980 --> 00:16:44,620
 this painful feeling, the mind picks it up and starts to

248
00:16:44,620 --> 00:16:46,000
 run away with it.

249
00:16:46,000 --> 00:16:49,950
 It depends on the state of mind of the person how much you

250
00:16:49,950 --> 00:16:53,910
 want to go into detail but at the very least you should be

251
00:16:53,910 --> 00:16:58,000
 able to explain to them that the feelings are going to be

252
00:16:58,000 --> 00:16:59,000
 our focus.

253
00:16:59,000 --> 00:17:02,420
 Make clear to them what we're going to be dealing with,

254
00:17:02,420 --> 00:17:05,920
 what the meditation is designed to do. It's designed to

255
00:17:05,920 --> 00:17:08,940
 help us to work through the feelings and to come to let go

256
00:17:08,940 --> 00:17:12,030
 of them and actually overcome them to the point that they

257
00:17:12,030 --> 00:17:13,000
 don't bother us.

258
00:17:13,000 --> 00:17:17,360
 Explain how we're going to do that, how we're going to

259
00:17:17,360 --> 00:17:22,050
 actually focus on the pain and try to remind ourselves the

260
00:17:22,050 --> 00:17:27,000
 nature of the pain. It's just a pain or a feeling.

261
00:17:27,000 --> 00:17:31,120
 We use this word that we have. Explain to them the

262
00:17:31,120 --> 00:17:34,550
 technique of meditation, the word mantra if they've ever

263
00:17:34,550 --> 00:17:38,000
 heard of this or if not, explain to them what a mantra is.

264
00:17:38,000 --> 00:17:43,240
 It's something that helps you focus on an object, something

265
00:17:43,240 --> 00:17:47,870
 that helps you to come to see only the object and to keep

266
00:17:47,870 --> 00:17:53,280
 other things from interfering, from distracting you, so to

267
00:17:53,280 --> 00:17:56,000
 keep yourself focused on a single object.

268
00:17:56,000 --> 00:18:00,270
 Now what that does is keeps you focused on simply on the

269
00:18:00,270 --> 00:18:03,770
 pain. Now reminding people that the pain is not the problem

270
00:18:03,770 --> 00:18:04,000
.

271
00:18:04,000 --> 00:18:07,620
 Once you're focused on just the sensation for itself, what

272
00:18:07,620 --> 00:18:10,940
 you come to realize is that it's actually not a negative

273
00:18:10,940 --> 00:18:14,000
 experience. It's not something unpleasant.

274
00:18:14,000 --> 00:18:17,270
 And what doesn't have a chance to come in is this judgment,

275
00:18:17,270 --> 00:18:20,000
 this disliking, this upset about the pain.

276
00:18:20,000 --> 00:18:23,700
 We're going to be able to separate the feelings of upset

277
00:18:23,700 --> 00:18:27,330
 and stress and anger and frustration about the pain,

278
00:18:27,330 --> 00:18:31,360
 sadness from the actual pain itself and just experience the

279
00:18:31,360 --> 00:18:33,000
 pain for what it is.

280
00:18:33,000 --> 00:18:35,810
 And you can reassure them that it works, tell them that you

281
00:18:35,810 --> 00:18:38,040
've done this and so on and they should try it for

282
00:18:38,040 --> 00:18:42,300
 themselves. If it doesn't work, they're welcome to stop and

283
00:18:42,300 --> 00:18:44,000
 try the medication.

284
00:18:44,000 --> 00:18:47,760
 So basically saying to people that we want to try this as

285
00:18:47,760 --> 00:18:51,280
 an alternative and see what you think and if it doesn't

286
00:18:51,280 --> 00:18:55,120
 work, you've always got the medication to go back up on as

287
00:18:55,120 --> 00:18:56,000
 a backup.

288
00:18:56,000 --> 00:18:59,430
 Now once you've explained this, this is the theory, this is

289
00:18:59,430 --> 00:19:02,500
 something that they have to keep in mind, then you can

290
00:19:02,500 --> 00:19:04,000
 start them on something simpler.

291
00:19:04,000 --> 00:19:07,610
 Say this mantra, this word that we use, the idea is to

292
00:19:07,610 --> 00:19:10,000
 allow us to see things clearly.

293
00:19:10,000 --> 00:19:14,680
 So we teach people to start practicing it and find a simple

294
00:19:14,680 --> 00:19:17,000
 object to practice it on.

295
00:19:17,000 --> 00:19:20,370
 The basic object of our contemplation, as many of you are

296
00:19:20,370 --> 00:19:24,710
 aware, is the stomach. And this is very useful for people

297
00:19:24,710 --> 00:19:27,000
 who are in bed, who are lying down.

298
00:19:27,000 --> 00:19:30,220
 When you're lying down and you're relaxed, you'll find that

299
00:19:30,220 --> 00:19:33,000
 the stomach is quite evident and easy to follow.

300
00:19:33,000 --> 00:19:37,280
 And so we watch it rising, falling, rising, falling and

301
00:19:37,280 --> 00:19:39,000
 keep our mind on it.

302
00:19:39,000 --> 00:19:42,070
 And we have this word that allows our mind to focus only on

303
00:19:42,070 --> 00:19:46,320
 the stomach. When it rises, say to yourself, "Rise." When

304
00:19:46,320 --> 00:19:49,000
 it falls, say to yourself, "Fall."

305
00:19:49,000 --> 00:19:52,410
 And your mind will focus on that object and that object

306
00:19:52,410 --> 00:19:57,000
 only. Nothing else will come in to distract it.

307
00:19:57,000 --> 00:19:59,820
 Or your mind won't be flitting here and there, your mind

308
00:19:59,820 --> 00:20:02,000
 will become focused and concentrated.

309
00:20:02,000 --> 00:20:05,880
 And then for a time, you'll find that you're able to find

310
00:20:05,880 --> 00:20:08,000
 peace and clarity of mind.

311
00:20:08,000 --> 00:20:12,640
 Now, as you do that, you'll find that from time to time,

312
00:20:12,640 --> 00:20:14,000
 pain arises.

313
00:20:14,000 --> 00:20:16,880
 And especially if you're terminally ill, if you have a

314
00:20:16,880 --> 00:20:19,930
 sickness and so on, you'll find that these sharp and

315
00:20:19,930 --> 00:20:22,000
 unpleasant sensations arise.

316
00:20:22,000 --> 00:20:24,520
 Now it's at that point, and you can lead people through

317
00:20:24,520 --> 00:20:27,660
 that. You can have them start to watch the stomach and then

318
00:20:27,660 --> 00:20:30,000
 start to explain as they're practicing,

319
00:20:30,000 --> 00:20:33,180
 as they're watching the stomach, explain to them about the

320
00:20:33,180 --> 00:20:35,000
 sorts of things that might arise.

321
00:20:35,000 --> 00:20:39,000
 The most prominent being the feelings, the pain and so on.

322
00:20:39,000 --> 00:20:42,670
 And say when the pain arises, try, you know, we can always

323
00:20:42,670 --> 00:20:45,000
 go back to the medication, reassuring them that they don't,

324
00:20:45,000 --> 00:20:47,590
 and this isn't going to be, we're not trying to torture

325
00:20:47,590 --> 00:20:53,000
 them, but reassure them to try first to acknowledge pain,

326
00:20:53,000 --> 00:20:56,570
 and to just remind themselves of the pain and keep their

327
00:20:56,570 --> 00:20:58,000
 minds on the pain.

328
00:20:58,000 --> 00:21:01,820
 Because the truth is, it's only when their mind slips away

329
00:21:01,820 --> 00:21:08,440
 from the pain into judgment, into aversion, into dislike of

330
00:21:08,440 --> 00:21:10,000
 the pain,

331
00:21:10,000 --> 00:21:12,780
 that it begins to give rise to thoughts of how to get away

332
00:21:12,780 --> 00:21:14,000
 and how to change it,

333
00:21:14,000 --> 00:21:18,000
 how to find some way, any way to escape the situation.

334
00:21:18,000 --> 00:21:21,260
 As long as you're focusing on the situation, seeing it for

335
00:21:21,260 --> 00:21:22,000
 what it is,

336
00:21:22,000 --> 00:21:24,360
 it doesn't even enter your mind that you have to change the

337
00:21:24,360 --> 00:21:25,000
 situation.

338
00:21:25,000 --> 00:21:28,000
 It doesn't enter your mind that this is a bad situation.

339
00:21:28,000 --> 00:21:32,310
 There's no room for that. Your mind is fully aware. It's

340
00:21:32,310 --> 00:21:35,000
 fully focused on the object itself.

341
00:21:35,000 --> 00:21:38,400
 You don't have to explain all this to them, but have them

342
00:21:38,400 --> 00:21:43,000
 try it, have them see for themselves whether this is true

343
00:21:43,000 --> 00:21:43,000
 or not,

344
00:21:43,000 --> 00:21:45,000
 what the result is.

345
00:21:45,000 --> 00:21:48,340
 I tried this with a woman who had stomach cancer, and she

346
00:21:48,340 --> 00:21:51,440
 had had it for about seven years, she was in her final

347
00:21:51,440 --> 00:21:52,000
 stages.

348
00:21:52,000 --> 00:21:55,220
 She was on some pretty heavy pain medication, and right

349
00:21:55,220 --> 00:21:56,700
 before she was going to take it, I said, "Well, let's try

350
00:21:56,700 --> 00:21:57,000
 this."

351
00:21:57,000 --> 00:21:59,800
 This is exactly what I did with her. I led her through

352
00:21:59,800 --> 00:22:03,000
 watching the stomach, rising, falling,

353
00:22:03,000 --> 00:22:05,530
 and then suddenly this pain arose in her stomach and she

354
00:22:05,530 --> 00:22:07,000
 wanted to take her medication.

355
00:22:07,000 --> 00:22:11,400
 They said, "Well, try this first. See what it does. Say to

356
00:22:11,400 --> 00:22:13,000
 yourself, 'Pain.'"

357
00:22:13,000 --> 00:22:19,760
 So she closed her eyes and she tried. She tried it, she

358
00:22:19,760 --> 00:22:22,000
 said to herself, "Pain."

359
00:22:22,000 --> 00:22:30,080
 Then she fell asleep. She passed out into unconsciousness,

360
00:22:30,080 --> 00:22:34,330
 probably because she had not been sleeping very well due to

361
00:22:34,330 --> 00:22:36,000
 the pain and the suffering.

362
00:22:36,000 --> 00:22:38,400
 But as a result, she didn't have to take the medication at

363
00:22:38,400 --> 00:22:39,000
 that time.

364
00:22:39,000 --> 00:22:41,890
 No, I didn't have time to stay with her and she obviously

365
00:22:41,890 --> 00:22:44,000
 would have stayed on the medication.

366
00:22:44,000 --> 00:22:48,960
 But if you can keep up with this, if you can help people

367
00:22:48,960 --> 00:22:54,000
 through it and explain to people again and again,

368
00:22:54,000 --> 00:22:56,370
 and keep encouraging them through it and leading them

369
00:22:56,370 --> 00:22:59,450
 through it and meditating with them and saying, "Let's try

370
00:22:59,450 --> 00:23:00,000
 it again,"

371
00:23:00,000 --> 00:23:04,000
 eventually you can, if not wean them off the medication,

372
00:23:04,000 --> 00:23:07,300
 you can at least help them to keep the medication at a

373
00:23:07,300 --> 00:23:08,000
 minimum

374
00:23:08,000 --> 00:23:12,000
 or only when it's incredibly severe.

375
00:23:12,000 --> 00:23:17,940
 And in fact, as I said, these sorts of people can be prime

376
00:23:17,940 --> 00:23:20,000
 material for the meditation practice

377
00:23:20,000 --> 00:23:24,000
 and can gain great insights and wisdom.

378
00:23:24,000 --> 00:23:27,000
 They can actually mitigate some of the effects of illness

379
00:23:27,000 --> 00:23:29,000
 and some people are able to overcome the illness

380
00:23:29,000 --> 00:23:32,990
 as a result of the easing up of the tension and the body's

381
00:23:32,990 --> 00:23:35,000
 better ability to heal itself.

382
00:23:35,000 --> 00:23:40,280
 But at the very least, they'll be able to deal with the end

383
00:23:40,280 --> 00:23:41,000
 of life

384
00:23:41,000 --> 00:23:45,440
 and they should find that, especially with the help of

385
00:23:45,440 --> 00:23:48,000
 someone who's done it before and who's encouraging them

386
00:23:48,000 --> 00:23:49,000
 through it

387
00:23:49,000 --> 00:23:56,060
 and reminding them about these things, that their whole

388
00:23:56,060 --> 00:23:58,000
 attitude will change

389
00:23:58,000 --> 00:24:03,500
 and they will become, in many ways, a new person, much

390
00:24:03,500 --> 00:24:08,000
 better able to deal with the difficulties and the problem.

391
00:24:08,000 --> 00:24:12,140
 So I think that's my cue to finish. I'd like to thank you

392
00:24:12,140 --> 00:24:14,000
 all for listening, for tuning in.

393
00:24:14,000 --> 00:24:19,480
 And I hope this helps too. And I hope that people are able

394
00:24:19,480 --> 00:24:22,000
 to use this for helping people in these sorts of situations

395
00:24:22,000 --> 00:24:22,000
.

396
00:24:22,000 --> 00:24:25,000
 So thanks for tuning in all of it.

397
00:24:27,000 --> 00:24:32,000
 [ S

398
00:24:34,000 --> 00:24:39,000
 s

