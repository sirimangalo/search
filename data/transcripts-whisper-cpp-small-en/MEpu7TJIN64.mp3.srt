1
00:00:00,000 --> 00:00:08,000
 Good evening everyone. Welcome to our daily dhamma.

2
00:00:08,000 --> 00:00:16,350
 Almost daily because Wednesdays and Thursdays I'm off, but

3
00:00:16,350 --> 00:00:19,000
 otherwise pretty much every day.

4
00:00:28,000 --> 00:00:33,870
 Today I wanted to look at the very beginnings of insight

5
00:00:33,870 --> 00:00:37,000
 meditation in our study group.

6
00:00:37,000 --> 00:00:47,440
 Today we just finished the paññabhumi, the soil of

7
00:00:47,440 --> 00:00:52,000
 understanding or the foundation of understanding.

8
00:00:53,000 --> 00:00:58,380
 So a lot of technical stuff that was really a slog and it's

9
00:00:58,380 --> 00:01:02,750
 to the credit of the students that everyone made it through

10
00:01:02,750 --> 00:01:03,000
 that.

11
00:01:03,000 --> 00:01:07,950
 But next we're going to be looking at the beginnings of

12
00:01:07,950 --> 00:01:14,000
 insight, beginnings of practical insight, knowledge.

13
00:01:17,000 --> 00:01:20,770
 And so it's good for us as meditators. It's not just the

14
00:01:20,770 --> 00:01:25,130
 very first stages, it's the very foundation of knowledge on

15
00:01:25,130 --> 00:01:28,000
 which we base the rest of our practice from here on in.

16
00:01:28,000 --> 00:01:31,430
 So it's important that we're clear about this and always

17
00:01:31,430 --> 00:01:38,000
 referring back to it. It's not just beginner stuff.

18
00:01:44,000 --> 00:01:48,720
 The very foundation of insight meditation is on the, it's

19
00:01:48,720 --> 00:01:55,330
 called diti-vi-sundhi. It's based on, it's a purification

20
00:01:55,330 --> 00:01:58,550
 of view that's based on an understanding of what really

21
00:01:58,550 --> 00:01:59,000
 exists.

22
00:02:04,000 --> 00:02:08,040
 And not just an intellectual understanding but it's

23
00:02:08,040 --> 00:02:12,590
 starting to see what, see the nature of reality, what it is

24
00:02:12,590 --> 00:02:13,000
.

25
00:02:14,000 --> 00:02:20,910
 It's starting to see that in all of us, in each of us, in

26
00:02:20,910 --> 00:02:28,130
 the universe in which each of us inhabits the reality that

27
00:02:28,130 --> 00:02:33,000
 each of us occupies, there are two aspects.

28
00:02:34,000 --> 00:02:40,100
 These are called rupa and nama or nama and rupa. Each and

29
00:02:40,100 --> 00:02:47,460
 every one of us has two parts made up of two things, nama

30
00:02:47,460 --> 00:02:49,000
 and rupa.

31
00:02:54,000 --> 00:02:59,590
 Because all that each of us has is our experiences. If we

32
00:02:59,590 --> 00:03:05,320
 get down to the very basics of what actually exists, all we

33
00:03:05,320 --> 00:03:11,000
 see in the end is moment after moment of experience.

34
00:03:11,000 --> 00:03:16,800
 That's as far down as we can go. Of course based on these

35
00:03:16,800 --> 00:03:20,720
 experiences we come to conjecture that a lot of other

36
00:03:20,720 --> 00:03:22,000
 things exist.

37
00:03:22,000 --> 00:03:28,990
 You may have the idea that our bodies exist. Of course you

38
00:03:28,990 --> 00:03:33,220
 get the idea that the room around us, the world around us

39
00:03:33,220 --> 00:03:37,080
 exists, the objects that we possess or that we come in

40
00:03:37,080 --> 00:03:38,000
 contact with.

41
00:03:39,000 --> 00:03:43,000
 Other people actually exist but all this is conjecture.

42
00:03:43,000 --> 00:03:48,520
 What we can know for sure is that experience exists. That's

43
00:03:48,520 --> 00:03:53,000
 what we can perceive and verify. That's it.

44
00:03:55,000 --> 00:03:58,820
 Beyond that it's all abstraction and this is important. It

45
00:03:58,820 --> 00:04:02,640
's not a philosophical argument of whether something exists

46
00:04:02,640 --> 00:04:06,740
 or doesn't exist. It's that there's a qualitative

47
00:04:06,740 --> 00:04:15,150
 difference, categorical difference between experiencing or

48
00:04:15,150 --> 00:04:23,000
 knowing an experience and knowing say a friend or an object

49
00:04:23,000 --> 00:04:23,000
.

50
00:04:24,000 --> 00:04:29,580
 When you look into a tree and you say that tree exists,

51
00:04:29,580 --> 00:04:34,690
 knowing that is far less concrete and real than knowing

52
00:04:34,690 --> 00:04:37,610
 that they're seeing when you see the tree that there's an

53
00:04:37,610 --> 00:04:41,600
 experience of seeing. The experience of seeing is far more

54
00:04:41,600 --> 00:04:42,000
 real.

55
00:04:44,000 --> 00:04:48,440
 That's the basis of all of reality but then we see

56
00:04:48,440 --> 00:04:55,000
 something curious about reality. It's made up of two parts.

57
00:05:02,000 --> 00:05:07,040
 Normally we call these the physical and the mental. Nama

58
00:05:07,040 --> 00:05:12,140
 and Rupa. Nama is the mental. It's called Saramanah, that

59
00:05:12,140 --> 00:05:16,240
 which takes an object. Different from Rupa which is Anar

60
00:05:16,240 --> 00:05:19,000
amanah. It doesn't take an object.

61
00:05:20,000 --> 00:05:24,150
 What that means is Nama is able to know things. Nama is

62
00:05:24,150 --> 00:05:28,410
 that aspect of reality that is aware, conscious, it's

63
00:05:28,410 --> 00:05:30,000
 consciousness.

64
00:05:30,000 --> 00:05:35,530
 Rupa is that acknowledged aspect of reality that doesn't

65
00:05:35,530 --> 00:05:42,000
 know anything but is a part of experience all the same.

66
00:05:43,000 --> 00:05:47,750
 When each of our experiences is different, there's so much

67
00:05:47,750 --> 00:05:52,520
 involved in experience. You can say it's all experience but

68
00:05:52,520 --> 00:05:54,000
 there's so much.

69
00:05:55,000 --> 00:05:59,660
 It seems that seeing is different from hearing, hearing is

70
00:05:59,660 --> 00:06:06,500
 different from smelling. Liking is different from disliking

71
00:06:06,500 --> 00:06:07,000
.

72
00:06:07,000 --> 00:06:12,000
 Even the various experiences of the body are different.

73
00:06:12,000 --> 00:06:16,330
 Experiencing heat and cold is different from experiencing

74
00:06:16,330 --> 00:06:19,000
 tension or hardness, softness.

75
00:06:22,000 --> 00:06:26,290
 When we breathe in our stomach, our body expands. The

76
00:06:26,290 --> 00:06:31,150
 expansion is accompanied with tension. That tension is a

77
00:06:31,150 --> 00:06:34,000
 part of the experience. It is an experience.

78
00:06:34,000 --> 00:06:38,730
 And then when the breath goes out, there's a release and

79
00:06:38,730 --> 00:06:44,000
 that release is of a different quality from the expansion.

80
00:06:44,000 --> 00:06:47,000
 It has a different feeling to it.

81
00:06:47,000 --> 00:07:02,000
 [silence]

82
00:07:03,000 --> 00:07:06,820
 But the idea behind all of this is to remove from our minds

83
00:07:06,820 --> 00:07:10,430
 and from our psyche all the views that we carry around

84
00:07:10,430 --> 00:07:14,000
 about what's real. The confusion.

85
00:07:14,000 --> 00:07:19,370
 The idea that there's a self or a soul, the idea that the

86
00:07:19,370 --> 00:07:24,160
 body exists. It allows us to differentiate between concept

87
00:07:24,160 --> 00:07:28,000
 and reality and to see that.

88
00:07:31,000 --> 00:07:34,600
 And to see that so much of what we think of as real is

89
00:07:34,600 --> 00:07:37,000
 really just all in our minds.

90
00:07:37,000 --> 00:07:41,520
 It's important because it sets the stage for our

91
00:07:41,520 --> 00:07:45,000
 understanding of how reality works.

92
00:07:45,000 --> 00:07:48,490
 Which is in turn important because that's where all the

93
00:07:48,490 --> 00:07:52,990
 problems come from. We really do have problems. We have bad

94
00:07:52,990 --> 00:07:57,310
 habits. We cause ourselves suffering. We cause suffering to

95
00:07:57,310 --> 00:07:58,000
 others.

96
00:08:00,000 --> 00:08:01,880
 So just like if you want to fix a car or an engine, you

97
00:08:01,880 --> 00:08:05,920
 have to take it apart and look at what's really going on

98
00:08:05,920 --> 00:08:10,240
 inside. You can't just kick it or keep trying to turn the

99
00:08:10,240 --> 00:08:15,000
 key and hoping that it works. You can't pray to the gods.

100
00:08:23,000 --> 00:08:26,140
 You have to actually look inside. It's the difference

101
00:08:26,140 --> 00:08:29,260
 between a person who owns a car and the mechanic who fixes

102
00:08:29,260 --> 00:08:30,000
 the car.

103
00:08:30,000 --> 00:08:33,950
 Ordinary people are like people who drive cars and when it

104
00:08:33,950 --> 00:08:38,000
's broken they take it to the mechanic and magic gets fixed.

105
00:08:38,000 --> 00:08:42,310
 So they think in terms of the car. But the mechanic looks

106
00:08:42,310 --> 00:08:45,390
 at a car and they know all the pieces of the car. That's

107
00:08:45,390 --> 00:08:48,750
 what we're trying to become through our meditation practice

108
00:08:48,750 --> 00:08:49,000
.

109
00:08:50,000 --> 00:08:53,000
 We're trying to learn how to fix the car.

110
00:08:53,000 --> 00:08:59,370
 So we have to see deeper than ordinary reality. Ordinary

111
00:08:59,370 --> 00:09:02,050
 reality will get you through the day. I mean if you think

112
00:09:02,050 --> 00:09:05,490
 of this room existing, these people existing, it's very

113
00:09:05,490 --> 00:09:06,000
 useful.

114
00:09:07,000 --> 00:09:09,230
 It's useful to think that the car exists because then you

115
00:09:09,230 --> 00:09:11,470
 know, okay, if I go outside that car is there, you don't

116
00:09:11,470 --> 00:09:14,510
 have to just stand there saying, "seeing, seeing." You know

117
00:09:14,510 --> 00:09:17,850
 that if you pull, the door will open and you can sit down

118
00:09:17,850 --> 00:09:20,000
 and turn the key and drive away.

119
00:09:20,000 --> 00:09:27,560
 You can't go the other route of understanding the nature of

120
00:09:27,560 --> 00:09:34,000
 reality and freeing yourself from suffering.

121
00:09:34,000 --> 00:09:41,380
 And so what we're doing here is quite different. We're

122
00:09:41,380 --> 00:09:46,670
 trying to take apart the motor, so to speak, take apart the

123
00:09:46,670 --> 00:09:52,000
 engine and clean it and fix it and put it back together.

124
00:09:55,000 --> 00:10:03,950
 So Rupa. Rupa basically is the four elements of what exists

125
00:10:03,950 --> 00:10:09,750
, what we experience. We experience hardness and softness,

126
00:10:09,750 --> 00:10:12,000
 that's the Earth's element.

127
00:10:13,000 --> 00:10:16,270
 The Earth is just a name. It doesn't actually mean there's

128
00:10:16,270 --> 00:10:20,260
 Earth there. It's just really a term for it. We have this

129
00:10:20,260 --> 00:10:24,760
 aspect of the physicality. We experience what's physical,

130
00:10:24,760 --> 00:10:28,000
 sometimes there's softness, sometimes there's hardness.

131
00:10:33,000 --> 00:10:35,850
 And there's the tension when you watch the stomach rising,

132
00:10:35,850 --> 00:10:38,300
 there's the tension in the stomach when you watch it

133
00:10:38,300 --> 00:10:42,000
 falling. There's the flaccidity, the release of tension.

134
00:10:42,000 --> 00:10:45,580
 That's the air element. That's what we call it. It's just a

135
00:10:45,580 --> 00:10:46,000
 name.

136
00:10:49,000 --> 00:10:52,810
 The fire element, you experience the heat in the body or

137
00:10:52,810 --> 00:10:57,260
 cold. Both of those are the fire element. And the water

138
00:10:57,260 --> 00:11:01,360
 element isn't really part of experience, but it's conceived

139
00:11:01,360 --> 00:11:06,190
 to be there as well. It's the cohesion, the stickiness. You

140
00:11:06,190 --> 00:11:08,000
 don't actually experience it.

141
00:11:09,000 --> 00:11:13,620
 That's just the technicality. The point being, this is what

142
00:11:13,620 --> 00:11:18,240
 we mean by Rupa. So when you watch the stomach rising, what

143
00:11:18,240 --> 00:11:22,000
 is it that's rising? Is it the Nama or the Rupa?

144
00:11:35,000 --> 00:11:39,000
 What is it that's rising? Does anyone have the answer?

145
00:11:39,000 --> 00:11:44,440
 Let me just confuse the heck out of you. What is it that

146
00:11:44,440 --> 00:11:47,000
 rises? Nama or Rupa?

147
00:11:47,000 --> 00:11:53,000
 Rupa is...

148
00:11:53,000 --> 00:11:56,000
 Dhar says Nama is rising.

149
00:11:56,000 --> 00:11:59,000
 It depends on if you're mindful.

150
00:11:59,000 --> 00:12:11,000
 What is it that's rising? Nama or Rupa?

151
00:12:11,000 --> 00:12:15,000
 Rupa.

152
00:12:15,000 --> 00:12:22,040
 Rupa. Rupa rises. What is it that knows that the Rupa is

153
00:12:22,040 --> 00:12:23,000
 rising?

154
00:12:23,000 --> 00:12:25,000
 Nama.

155
00:12:25,000 --> 00:12:28,000
 When you walk, which one walks? Rupa or Nama?

156
00:12:28,000 --> 00:12:33,000
 Which one knows that you're walking?

157
00:12:33,000 --> 00:12:39,000
 When you talk, which one talks? Rupa or Nama?

158
00:12:39,000 --> 00:12:45,000
 Rupa.

159
00:12:45,000 --> 00:12:47,420
 Rupa. Which one knows that you're talking or knows what you

160
00:12:47,420 --> 00:12:49,000
're going to say? Nama.

161
00:12:49,000 --> 00:12:51,000
 Does Rupa know what you're going to say?

162
00:12:51,000 --> 00:12:53,000
 No.

163
00:12:54,000 --> 00:13:04,000
 [Pause]

164
00:13:04,000 --> 00:13:08,080
 It's a very basis of insight to be able to see these. It

165
00:13:08,080 --> 00:13:10,600
 doesn't really matter. You don't have to think about them

166
00:13:10,600 --> 00:13:11,000
 too much.

167
00:13:11,000 --> 00:13:14,680
 It's just the fact that you can see them means you've gone

168
00:13:14,680 --> 00:13:17,000
 beyond the idea of me and mine and I.

169
00:13:18,000 --> 00:13:24,000
 You've gone beyond views of self and so on.

170
00:13:24,000 --> 00:13:29,000
 We're not talking about the... we're not cultivating a view

171
00:13:29,000 --> 00:13:31,000
 that the self doesn't exist or something like that.

172
00:13:31,000 --> 00:13:34,240
 What we're saying is stop thinking in terms of views and

173
00:13:34,240 --> 00:13:38,000
 start looking in terms of experiences and observations.

174
00:13:38,000 --> 00:13:44,250
 To get beyond that. Many people when they hear this, they

175
00:13:44,250 --> 00:13:46,000
 start to worry and think,

176
00:13:47,000 --> 00:13:49,270
 "What's going to happen to myself?" They're missing the

177
00:13:49,270 --> 00:13:51,000
 whole point because they're still thinking.

178
00:13:51,000 --> 00:13:56,000
 They're not yet... they haven't yet penetrated this wall

179
00:13:56,000 --> 00:14:00,730
 between intellectualization, reviews, ideas and abstract

180
00:14:00,730 --> 00:14:04,000
ions about reality and actual experience of reality.

181
00:14:09,000 --> 00:14:12,870
 So the use of asking these questions and talking about this

182
00:14:12,870 --> 00:14:19,110
 is to make clear the difference and to help to see the

183
00:14:19,110 --> 00:14:23,000
 distinction between reality and concept.

184
00:14:27,000 --> 00:14:30,110
 So what you should start to see, you should start to see

185
00:14:30,110 --> 00:14:34,480
 that nama and rupa arise and cease. When you walk, stepping

186
00:14:34,480 --> 00:14:37,260
 right, there's the arising of the experience and the

187
00:14:37,260 --> 00:14:38,000
 cessation.

188
00:14:38,000 --> 00:14:41,610
 When you move your left foot, stepping left, there's the

189
00:14:41,610 --> 00:14:44,000
 arising and there's the cessation.

190
00:14:44,000 --> 00:14:48,510
 So contrary to what we might believe about reality, reality

191
00:14:48,510 --> 00:14:51,000
 is born and dies every moment.

192
00:14:55,000 --> 00:14:58,100
 Anything that we think of as existing, as lasting, is

193
00:14:58,100 --> 00:15:01,000
 really just a thought, a concept in our minds.

194
00:15:01,000 --> 00:15:06,520
 We conceive of the same experiences or similar experiences

195
00:15:06,520 --> 00:15:08,000
 again and again.

196
00:15:08,000 --> 00:15:14,870
 We conceive of them as being an entity. You see the same

197
00:15:14,870 --> 00:15:18,000
 thing and you say, "Oh, that's a car."

198
00:15:19,000 --> 00:15:22,570
 Yep, car is still there. You look again, you see again the

199
00:15:22,570 --> 00:15:27,220
 car. But the reality, the experience arises and ceases. It

200
00:15:27,220 --> 00:15:29,000
's born and dies.

201
00:15:40,000 --> 00:15:45,260
 This is what gets through that idea, the view of self. The

202
00:15:45,260 --> 00:15:49,370
 second aspect is how they relate to each other in terms of

203
00:15:49,370 --> 00:15:51,000
 cause and effect.

204
00:15:51,000 --> 00:15:54,000
 And this gets into the idea of karma.

205
00:15:55,000 --> 00:15:58,900
 We're talking about karma today. I mentioned that karma is

206
00:15:58,900 --> 00:16:02,230
 not quite what we think it is. I think of karma as you do

207
00:16:02,230 --> 00:16:04,920
 something like you kill someone and then you suffer the

208
00:16:04,920 --> 00:16:07,830
 retribution in a future life. Maybe someone kills you, but

209
00:16:07,830 --> 00:16:10,000
 that's not what karma really means.

210
00:16:13,000 --> 00:16:17,850
 Karma is not the precise truth about karma. Karma is a

211
00:16:17,850 --> 00:16:22,000
 moment when you have an ethical mind state.

212
00:16:22,000 --> 00:16:26,540
 And ethical, we just mean something that is going to have

213
00:16:26,540 --> 00:16:32,230
 consequences. There's a value placed on the experience. You

214
00:16:32,230 --> 00:16:35,000
 like something or you dislike something.

215
00:16:35,000 --> 00:16:39,000
 Or you have a clear objective mind.

216
00:16:40,000 --> 00:16:44,960
 So there's an attachment, an aversion, or a delusion, or

217
00:16:44,960 --> 00:16:50,290
 there's clarity and contentment and different ways of

218
00:16:50,290 --> 00:16:55,000
 reacting to experiences. That's karma.

219
00:16:56,000 --> 00:17:02,350
 And that experience will change or will bring about results

220
00:17:02,350 --> 00:17:08,220
, but the result of karma is just more experience. So

221
00:17:08,220 --> 00:17:11,000
 experiences are actually the result of karma.

222
00:17:13,000 --> 00:17:16,950
 The second thing that you begin to see in the practice is

223
00:17:16,950 --> 00:17:20,930
 you begin to see not only are these things arising and ce

224
00:17:20,930 --> 00:17:24,580
asing when I step or when I sit or when I think or when I

225
00:17:24,580 --> 00:17:27,000
 feel that they're related.

226
00:17:27,000 --> 00:17:29,990
 If I get really angry, I'm going to feel a lot of pain in

227
00:17:29,990 --> 00:17:33,410
 the head, tension in the body. If I get worried, I'm going

228
00:17:33,410 --> 00:17:35,000
 to feel stress in the body.

229
00:17:38,000 --> 00:17:42,000
 I saw that karma means that we cause ourselves suffering.

230
00:17:42,000 --> 00:17:45,330
 On the other hand, if I'm mindful, if I see things clearly,

231
00:17:45,330 --> 00:17:48,310
 it's like untying a knot and suddenly I'm free and clear

232
00:17:48,310 --> 00:17:50,000
 and peaceful in the mind.

233
00:17:50,000 --> 00:17:53,000
 For the result is positive.

234
00:17:54,000 --> 00:17:58,090
 This is, of course, a very important thing to see. This is

235
00:17:58,090 --> 00:18:01,000
 the beginning of the Four Noble Truths.

236
00:18:01,000 --> 00:18:06,350
 You start to see that suffering is caused by all sorts of

237
00:18:06,350 --> 00:18:11,780
 defilements in the mind, any kind of clinging, any kind of

238
00:18:11,780 --> 00:18:16,000
 attachment to the experience or reaction to it.

239
00:18:17,000 --> 00:18:20,530
 And that peace and freedom from suffering comes from not

240
00:18:20,530 --> 00:18:24,430
 reacting, but rather experiencing and seeing things as they

241
00:18:24,430 --> 00:18:25,000
 are.

242
00:18:25,000 --> 00:18:53,000
 There was, in one of, I was reading an article about

243
00:18:54,000 --> 00:18:57,150
 childbirth. It was a really interesting article about

244
00:18:57,150 --> 00:19:06,000
 childbirth. This woman, she gave birth in a hospital and

245
00:19:06,000 --> 00:19:14,990
 she recounts the trauma involved, the pain and the stress

246
00:19:14,990 --> 00:19:21,000
 and the physical torture involved.

247
00:19:22,000 --> 00:19:26,000
 She had scarring by the end of it and had to have surgery.

248
00:19:26,000 --> 00:19:35,320
 And she was completely drugged up. It was quite a shocking

249
00:19:35,320 --> 00:19:41,620
 thing to read, just to think about how painful childbirth

250
00:19:41,620 --> 00:19:42,000
 can be.

251
00:19:43,000 --> 00:19:46,590
 So reading it just made me think of, wow, this is part of

252
00:19:46,590 --> 00:19:52,150
 life, part of the nature of life. But then she goes on to

253
00:19:52,150 --> 00:19:55,850
 talk about how she got pregnant a second time and started

254
00:19:55,850 --> 00:19:58,000
 thinking about this suffering.

255
00:19:59,000 --> 00:20:02,580
 Is this a part of life? Is this what I have to look forward

256
00:20:02,580 --> 00:20:06,580
 to? And she was terrified of having to give birth. And then

257
00:20:06,580 --> 00:20:12,410
 she started to do some research. And she found that, this

258
00:20:12,410 --> 00:20:15,660
 curious idea that people were talking about giving birth at

259
00:20:15,660 --> 00:20:19,140
 home, and anyway, the idea is eventually she gave birth,

260
00:20:19,140 --> 00:20:23,240
 but she had her second pregnancy, her second childbirth at

261
00:20:23,240 --> 00:20:24,000
 home.

262
00:20:25,000 --> 00:20:29,900
 And the reason why I'm telling this is because of the lack

263
00:20:29,900 --> 00:20:35,270
 of stress, the childbirth was completely different. And it

264
00:20:35,270 --> 00:20:39,490
 was peaceful. She didn't have to have any medication. There

265
00:20:39,490 --> 00:20:46,540
 was no problems or trauma or stress. She didn't. It was a

266
00:20:46,540 --> 00:20:49,000
 completely different story.

267
00:20:50,000 --> 00:20:53,910
 So we've given this article to read, one of our professors

268
00:20:53,910 --> 00:20:58,070
 gave it to us. It just made me think of, it's a very good

269
00:20:58,070 --> 00:21:01,520
 example of how the mind and the body work together. A very

270
00:21:01,520 --> 00:21:05,000
 real example and a very powerful one.

271
00:21:06,000 --> 00:21:09,970
 You often think that childbirth is, for those who have

272
00:21:09,970 --> 00:21:13,990
 experience with it, you often think that childbirth is just

273
00:21:13,990 --> 00:21:19,340
 a traumatic experience. It's very painful, scary. It's

274
00:21:19,340 --> 00:21:24,000
 scary because it's painful, because the body is unprepared.

275
00:21:25,000 --> 00:21:29,640
 And the only thing is that the body can be quite prepared.

276
00:21:29,640 --> 00:21:34,010
 And the body is quite plastic as it relates to the mind,

277
00:21:34,010 --> 00:21:39,050
 meaning the mind has such power over the body. I think this

278
00:21:39,050 --> 00:21:41,000
 is a great example.

279
00:21:42,000 --> 00:21:45,680
 How the mind can cause you stress in the body when you're

280
00:21:45,680 --> 00:21:49,740
 anxious, your heart starts beating rapidly. You ever think

281
00:21:49,740 --> 00:21:53,810
 about how that works? What really happens is that the def

282
00:21:53,810 --> 00:21:58,160
ilements in the mind, the bad habits in the mind cause

283
00:21:58,160 --> 00:22:01,000
 stress, cause physical harm.

284
00:22:04,000 --> 00:22:07,430
 But nothing says it more than this reality of childbirth,

285
00:22:07,430 --> 00:22:12,030
 how stressful it can be. And then you think, it's mainly

286
00:22:12,030 --> 00:22:16,880
 caused by the fact that my mind was stressed and I was

287
00:22:16,880 --> 00:22:18,000
 afraid.

288
00:22:21,000 --> 00:22:24,030
 These are the consequences. This is the importance of

289
00:22:24,030 --> 00:22:27,240
 understanding how Rupa and Nama work. The theory that I'm

290
00:22:27,240 --> 00:22:30,950
 giving you is not important, but it challenges you to ask

291
00:22:30,950 --> 00:22:34,000
 yourself, "Am I seeing Nama and Rupa?"

292
00:22:35,000 --> 00:22:38,410
 And that's what's important, because once you see it, once

293
00:22:38,410 --> 00:22:41,480
 you look and you see Nama and Rupa for yourself, that's

294
00:22:41,480 --> 00:22:45,160
 where the doors open. That's where your life begins to

295
00:22:45,160 --> 00:22:49,000
 change and where you enter on this path of purification.

296
00:22:50,000 --> 00:22:54,390
 Usually, slowly over time, long time, short time depends on

297
00:22:54,390 --> 00:22:58,390
 you. You free yourself from all these bad habits and all

298
00:22:58,390 --> 00:23:02,540
 these problems and you stop hurting yourself. You stop

299
00:23:02,540 --> 00:23:05,000
 causing yourself stress and suffering.

300
00:23:06,000 --> 00:23:09,500
 So I think that's important. It's important at least to

301
00:23:09,500 --> 00:23:13,130
 affirm this and to remind ourselves. The basis of our

302
00:23:13,130 --> 00:23:17,380
 practice is Nama Rupa. The basis of our practice is the

303
00:23:17,380 --> 00:23:21,000
 physical and mental aspects of experience.

304
00:23:21,000 --> 00:23:28,210
 It's not me or mine or I. It's not beings. It's just

305
00:23:28,210 --> 00:23:33,000
 moments, moments of experience.

306
00:23:34,000 --> 00:23:36,270
 When we do walking meditation, we're not trying to walk to

307
00:23:36,270 --> 00:23:39,360
 get to the wall. What's the point? You just have to turn

308
00:23:39,360 --> 00:23:41,000
 around and walk back.

309
00:23:41,000 --> 00:23:47,590
 The point is to be aware of each movement, because that's N

310
00:23:47,590 --> 00:23:49,000
ama Rupa.

311
00:23:49,000 --> 00:23:52,460
 When you sit, it's not about getting control of the breath.

312
00:23:52,460 --> 00:23:55,780
 It's about watching each movement of the breath, because

313
00:23:55,780 --> 00:24:00,260
 that's Nama Rupa. When you watch the Rupa, you're going to

314
00:24:00,260 --> 00:24:01,000
 see Nama.

315
00:24:02,000 --> 00:24:04,800
 When you watch the stomach rising and falling, you'll see

316
00:24:04,800 --> 00:24:07,470
 you learn all sorts of things about the Nama, about your

317
00:24:07,470 --> 00:24:08,000
 mind.

318
00:24:08,000 --> 00:24:12,370
 Sometimes there's stress over it, sometimes there's desire

319
00:24:12,370 --> 00:24:16,000
 over it, expectations over it, fear, worry.

320
00:24:16,000 --> 00:24:20,640
 And sometimes there's clarity, sometimes there's

321
00:24:20,640 --> 00:24:23,000
 mindfulness, wisdom.

322
00:24:28,000 --> 00:24:32,000
 This is where all of our insight meditation occurs.

323
00:24:32,000 --> 00:24:37,310
 There you go. A little bit of Dhamma for tonight. Thank you

324
00:24:37,310 --> 00:24:41,000
 all for tuning in. I wish you all good practice.

