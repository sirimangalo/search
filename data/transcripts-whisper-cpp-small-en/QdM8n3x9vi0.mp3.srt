1
00:00:00,000 --> 00:00:06,100
 Okay, is mindfulness on the breath a beginner's practice?

2
00:00:06,100 --> 00:00:09,110
 Isn't the goal to be mindful of everything, not just the

3
00:00:09,110 --> 00:00:10,000
 breath?

4
00:00:10,000 --> 00:00:15,730
 Well, it depends what you mean by mindfulness on the breath

5
00:00:15,730 --> 00:00:19,000
, mindfulness of the breath, I guess.

6
00:00:19,000 --> 00:00:26,550
 I'm not sure if I said it, I did answer another question I

7
00:00:26,550 --> 00:00:32,510
 think last week on something of the sort, why we focus on

8
00:00:32,510 --> 00:00:34,000
 the abdomen, that's what it was, right?

9
00:00:34,000 --> 00:00:40,020
 So I did talk about it, that the breath is a concept. So it

10
00:00:40,020 --> 00:00:46,000
's not a matter of being a beginner's practice,

11
00:00:46,000 --> 00:00:50,850
 it's a matter of being a conceptual practice, a practice

12
00:00:50,850 --> 00:00:55,000
 that only has the potential ability to calm the mind,

13
00:00:55,000 --> 00:00:58,790
 and it doesn't by itself have the ability to clear the mind

14
00:00:58,790 --> 00:01:03,340
, to allow one to see clearly, because it's not focused on n

15
00:01:03,340 --> 00:01:07,000
ama-rupa, which relates to the question I just answered.

16
00:01:07,000 --> 00:01:11,260
 If you're not focusing on nama-rupa, you'll never get rid

17
00:01:11,260 --> 00:01:15,520
 of this idea of self, the ego. There are many meditative

18
00:01:15,520 --> 00:01:18,000
 contemplatives out there who believe in a self,

19
00:01:18,000 --> 00:01:22,190
 who believe in God, who believe in the soul and so on, and

20
00:01:22,190 --> 00:01:25,000
 have this view because they haven't seen nama-rupa,

21
00:01:25,000 --> 00:01:28,310
 and they haven't come to have this profound realization

22
00:01:28,310 --> 00:01:31,860
 that it doesn't matter what you talk about, if you talk

23
00:01:31,860 --> 00:01:34,000
 about God or a soul or a self,

24
00:01:34,000 --> 00:01:36,190
 what you're really talking about is seeing, hearing,

25
00:01:36,190 --> 00:01:38,990
 smelling, tasting, feeling and thinking, which are all

26
00:01:38,990 --> 00:01:41,000
 nothing more than nama-rupa,

27
00:01:41,000 --> 00:01:52,900
 rupa and nama. So, you know, it's mindfulness specifically

28
00:01:52,900 --> 00:01:59,000
 or strictly about the breath is a samatha practice,

29
00:01:59,000 --> 00:02:03,720
 or a practice which only has the potential to calm the mind

30
00:02:03,720 --> 00:02:07,260
. Now, that doesn't mean that what we generally call

31
00:02:07,260 --> 00:02:11,000
 mindfulness of the breath can't be used to see clearly,

32
00:02:11,000 --> 00:02:15,110
 but if it's the breath, specifically the breath that you're

33
00:02:15,110 --> 00:02:18,570
 focusing on, then it is, and for many people this is the

34
00:02:18,570 --> 00:02:21,000
 case, they focus on it and their mind becomes calm.

35
00:02:21,000 --> 00:02:25,100
 That calm can be useful, it can be used to develop insight,

36
00:02:25,100 --> 00:02:29,370
 but it doesn't have to be, and many people don't develop

37
00:02:29,370 --> 00:02:31,000
 subsequently insight,

38
00:02:31,000 --> 00:02:33,970
 and there's content with the calm and they think they're

39
00:02:33,970 --> 00:02:36,000
 enlightened, and so on and so on.

40
00:02:36,000 --> 00:02:45,170
 But if you focus on the physical experience of the breath,

41
00:02:45,170 --> 00:02:48,000
 and this is why we focus on the stomach, rising, falling,

42
00:02:48,000 --> 00:02:49,000
 because that's the breath,

43
00:02:49,000 --> 00:02:52,360
 anyone who says that's not mindfulness of the breath is

44
00:02:52,360 --> 00:02:54,000
 really being dogmatic, right?

45
00:02:54,000 --> 00:02:58,540
 And this is what I was saying about the nose, right, last

46
00:02:58,540 --> 00:03:03,000
 week. The stomach is equally mindfulness of breathing.

47
00:03:03,000 --> 00:03:06,400
 If the texts say focus on the nose or focus on the stomach,

48
00:03:06,400 --> 00:03:11,420
 it's not really clear. But it's clear in the text, and what

49
00:03:11,420 --> 00:03:12,000
 I didn't mention,

50
00:03:12,000 --> 00:03:16,240
 I promised I was going to say last week, was where in the

51
00:03:16,240 --> 00:03:21,240
 texts they talk about mindfulness of the rising and the

52
00:03:21,240 --> 00:03:22,000
 falling of the abdomen.

53
00:03:22,000 --> 00:03:25,670
 And it's in one sutta at least, but I have to look it up

54
00:03:25,670 --> 00:03:30,150
 where else it is. It's in the samadity sutta, I believe, if

55
00:03:30,150 --> 00:03:31,000
 I'm not wrong,

56
00:03:31,000 --> 00:03:35,720
 where Sariputta talks about the winds in the body, and it's

57
00:03:35,720 --> 00:03:39,200
 something that the Buddha has talked about, and the wind in

58
00:03:39,200 --> 00:03:41,000
 the body, the wind element.

59
00:03:41,000 --> 00:03:48,770
 And it's the winds in the stomach, this is the part of the

60
00:03:48,770 --> 00:03:51,000
 wind element.

61
00:03:51,000 --> 00:03:54,480
 And so it's a part of our physical experience, and by wind

62
00:03:54,480 --> 00:03:56,730
 means the pressure. So the pressure that comes in the

63
00:03:56,730 --> 00:03:57,000
 stomach,

64
00:03:57,000 --> 00:04:02,140
 it's specifically mentioned in the samadity sutta of the

65
00:04:02,140 --> 00:04:06,000
 Majjhima Nikaya, I believe.

66
00:04:06,000 --> 00:04:12,450
 I'm not a scholar by any means. But okay, so if you're

67
00:04:12,450 --> 00:04:15,520
 focusing on that aspect of the breath, the physical

68
00:04:15,520 --> 00:04:18,000
 experience that relates to the breath,

69
00:04:18,000 --> 00:04:22,310
 then it certainly is vipassana, it is insight meditation,

70
00:04:22,310 --> 00:04:27,000
 and it's not a beginner's practice.

71
00:04:27,000 --> 00:04:30,660
 Because one thing we have to realize in meditation is that

72
00:04:30,660 --> 00:04:33,000
 we're really not going anywhere.

73
00:04:33,000 --> 00:04:37,530
 We're not trying to get somewhere. The path is like this

74
00:04:37,530 --> 00:04:39,000
 imploding path.

75
00:04:39,000 --> 00:04:42,490
 The eightfold noble path doesn't take you somewhere. You're

76
00:04:42,490 --> 00:04:46,990
 not going out there, you're coming back inside until the

77
00:04:46,990 --> 00:04:49,000
 mind doesn't go anywhere.

78
00:04:49,000 --> 00:04:53,440
 Some people call it the inward path. There's the Buddhist

79
00:04:53,440 --> 00:04:57,290
 publishing group in Malaysia called the inward path, which

80
00:04:57,290 --> 00:04:58,000
 is a really good name,

81
00:04:58,000 --> 00:05:01,930
 because that's where this path is leading. So there's

82
00:05:01,930 --> 00:05:06,450
 really no such thing as a beginner's practice, anything

83
00:05:06,450 --> 00:05:08,000
 that is useful in the beginning.

84
00:05:08,000 --> 00:05:11,480
 If it's really truly useful, it's going to lead you all the

85
00:05:11,480 --> 00:05:12,000
 way.

86
00:05:12,000 --> 00:05:15,650
 And from the very beginning, the practice that you start

87
00:05:15,650 --> 00:05:18,000
 with has the potential to take you all the way to the end.

88
00:05:18,000 --> 00:05:22,140
 Because as long as it's focusing on nama and rupa, it's

89
00:05:22,140 --> 00:05:26,360
 going to take you, or it has the potential to take you all

90
00:05:26,360 --> 00:05:28,000
 the way to the end.

91
00:05:28,000 --> 00:05:36,550
 That's one way of answering. But in practical terms, the

92
00:05:36,550 --> 00:05:42,490
 problem with focusing on the stomach is not that it's not

93
00:05:42,490 --> 00:05:45,000
 going to bring about insight and realization of the truth.

94
00:05:45,000 --> 00:05:49,000
 The problem is that it's not enough.

95
00:05:49,000 --> 00:05:59,110
 And, well, the idea of avoiding other things is a good

96
00:05:59,110 --> 00:06:03,000
 point, but it's not really the most important point.

97
00:06:03,000 --> 00:06:08,060
 The most important point is that the mind is more active

98
00:06:08,060 --> 00:06:13,600
 than that, and the mind will fail to, at least in the

99
00:06:13,600 --> 00:06:14,000
 beginning,

100
00:06:14,000 --> 00:06:17,600
 will fail to fully grasp the reality of what it's

101
00:06:17,600 --> 00:06:19,000
 experiencing.

102
00:06:19,000 --> 00:06:25,360
 And because the mind is distracted, and because the mind is

103
00:06:25,360 --> 00:06:28,000
 running all over the place, especially in the beginning,

104
00:06:28,000 --> 00:06:36,070
 that you have to give it some other object to pay attention

105
00:06:36,070 --> 00:06:38,000
 to.

106
00:06:38,000 --> 00:06:43,310
 And, yeah, so in that sense, really, it's true, and we

107
00:06:43,310 --> 00:06:49,000
 never recommend for anyone to focus simply on the abdomen.

108
00:06:49,000 --> 00:06:54,420
 If you're questioning about someone who practices anapanas

109
00:06:54,420 --> 00:06:59,120
ati, then generally they're practicing anapanasati for the

110
00:06:59,120 --> 00:07:01,000
 purpose of gaining calm.

111
00:07:01,000 --> 00:07:04,150
 And they have different theories on this as well, but

112
00:07:04,150 --> 00:07:06,790
 mostly, they're ideas that they're going to gain calm first

113
00:07:06,790 --> 00:07:07,000
.

114
00:07:07,000 --> 00:07:10,430
 If you're going to gain calm, then, yeah, you do lose the

115
00:07:10,430 --> 00:07:11,000
 rest.

116
00:07:11,000 --> 00:07:13,710
 You're trying to avoid them, and you're trying to ignore

117
00:07:13,710 --> 00:07:18,000
 them because you're trying to enter into a trance state,

118
00:07:18,000 --> 00:07:21,570
 or a state of maybe not exact, maybe trance is a pejorative

119
00:07:21,570 --> 00:07:25,000
 word, but it sort of is a trance.

120
00:07:25,000 --> 00:07:28,880
 I mean, you're trying to get your mind into a new level of

121
00:07:28,880 --> 00:07:32,000
 experience, a conditioned level of experience,

122
00:07:32,000 --> 00:07:35,000
 and that blocks out quite a bit.

123
00:07:35,000 --> 00:07:41,000
 It cuts off quite a bit of the spectrum of experience.

124
00:07:41,000 --> 00:07:45,680
 On the other hand, if you read the satipatthana sutta, the

125
00:07:45,680 --> 00:07:49,000
 discourse on the four foundations of mindfulness,

126
00:07:49,000 --> 00:07:52,110
 it seems like the idea is to do the opposite, that if you

127
00:07:52,110 --> 00:07:55,680
 feel angry or upset, then you know that you're angry or

128
00:07:55,680 --> 00:07:56,000
 upset,

129
00:07:56,000 --> 00:07:59,220
 and therefore you do incorporate everything into your

130
00:07:59,220 --> 00:08:00,000
 practice.

131
00:08:00,000 --> 00:08:02,000
 That's the practice that I teach.

132
00:08:02,000 --> 00:08:06,010
 As I said, we don't tell people to focus simply on the

133
00:08:06,010 --> 00:08:07,000
 stomach.

134
00:08:07,000 --> 00:08:11,780
 We use that as, well, yeah, as you said, a beginner's

135
00:08:11,780 --> 00:08:13,000
 practice.

136
00:08:13,000 --> 00:08:17,000
 So what I was saying, but it's not a beginner's practice,

137
00:08:17,000 --> 00:08:21,000
 is that it has the potential to take you all the way.

138
00:08:21,000 --> 00:08:25,260
 But maybe better than a beginner's practice, it's an

139
00:08:25,260 --> 00:08:27,000
 example practice.

140
00:08:27,000 --> 00:08:33,420
 It's just one of an infinite number of practices that you

141
00:08:33,420 --> 00:08:35,000
 could undertake.

142
00:08:35,000 --> 00:08:39,070
 Once you learn how to do that, then we start telling you,

143
00:08:39,070 --> 00:08:40,000
 well, actually in the beginning,

144
00:08:40,000 --> 00:08:43,280
 we'll tell you about the feelings, about the mind, about

145
00:08:43,280 --> 00:08:44,000
 the dhammas.

146
00:08:44,000 --> 00:08:48,000
 There are four satipatthanas, no kaya vida najita dhamma.

147
00:08:48,000 --> 00:08:52,130
 If you've read my booklet on how to meditate, you will see

148
00:08:52,130 --> 00:08:53,000
 that,

149
00:08:53,000 --> 00:08:56,110
 and I think I explained that quite clearly, that actually

150
00:08:56,110 --> 00:08:58,000
 the abdomen is just an example.

151
00:08:58,000 --> 00:09:00,800
 But why I stressed in the beginning, because what was on my

152
00:09:00,800 --> 00:09:04,000
 mind was the power of this rising and the falling,

153
00:09:04,000 --> 00:09:07,490
 and I think a lot of people miss that, how incredible it

154
00:09:07,490 --> 00:09:09,000
 can be to simply say to yourself,

155
00:09:09,000 --> 00:09:12,860
 "Rising and falling," and the profound insight that can

156
00:09:12,860 --> 00:09:14,000
 come from it.

157
00:09:14,000 --> 00:09:18,200
 Why I deny that it's a beginner's practice is because I don

158
00:09:18,200 --> 00:09:22,000
't want you to think that just because,

159
00:09:22,000 --> 00:09:24,670
 you know, the first thing that we give you, and then later

160
00:09:24,670 --> 00:09:26,000
 we give you other practices,

161
00:09:26,000 --> 00:09:29,420
 that it was only the first thing, or it was only the first

162
00:09:29,420 --> 00:09:30,000
 step.

163
00:09:30,000 --> 00:09:33,880
 Simply watching the rising and the falling can lead you to

164
00:09:33,880 --> 00:09:35,000
 enlightenment,

165
00:09:35,000 --> 00:09:40,930
 and it certainly leads everyone who practices it to change

166
00:09:40,930 --> 00:09:44,000
 the way they look at reality.

167
00:09:44,000 --> 00:09:46,740
 And that's often the problem, because there's one video I

168
00:09:46,740 --> 00:09:49,000
 did that I was really happy about,

169
00:09:49,000 --> 00:09:51,450
 because it's one that really needs to be done and needs to

170
00:09:51,450 --> 00:09:52,000
 be said.

171
00:09:52,000 --> 00:09:57,040
 It's in regards to this, it's what was it called, "

172
00:09:57,040 --> 00:10:00,120
Experience of Reality," or something I titled it, some silly

173
00:10:00,120 --> 00:10:01,000
 name.

174
00:10:01,000 --> 00:10:05,170
 But it was basically, what is the meditation, and all of

175
00:10:05,170 --> 00:10:09,000
 those bad experiences that we have,

176
00:10:09,000 --> 00:10:14,000
 that are actually right practice.

177
00:10:14,000 --> 00:10:17,990
 And it was basically, I hope in that video, or I think in

178
00:10:17,990 --> 00:10:22,950
 that video, I talked about how the rising and the falling

179
00:10:22,950 --> 00:10:24,000
 is a good example,

180
00:10:24,000 --> 00:10:27,140
 because it becomes stressful and it becomes unpleasant, and

181
00:10:27,140 --> 00:10:30,000
 we can't control it, and we try to control it,

182
00:10:30,000 --> 00:10:35,960
 and therefore we think this meditation is bad and terrible

183
00:10:35,960 --> 00:10:38,000
 and wrong for us.

184
00:10:38,000 --> 00:10:39,000
 Maybe we're just not good at it.

185
00:10:39,000 --> 00:10:44,000
 I love how my students from Thailand would always tell me,

186
00:10:44,000 --> 00:10:46,000
 "I'm just not suited to the rising and falling,"

187
00:10:46,000 --> 00:10:48,000
 or "I just don't find it suitable."

188
00:10:48,000 --> 00:10:52,610
 "Maitanat," we use this word, "maitanat," which means, "I'm

189
00:10:52,610 --> 00:10:57,000
 just not into it," basically.

190
00:10:57,000 --> 00:11:00,570
 Which is always funny, because it means that they're not

191
00:11:00,570 --> 00:11:02,000
 having fun with it.

192
00:11:02,000 --> 00:11:07,000
 It's much more fun to count in one, out one, into.

193
00:11:07,000 --> 00:11:10,000
 I mean, it's much more peaceful, much more dependable,

194
00:11:10,000 --> 00:11:13,000
 because you know that after one comes two, and then three,

195
00:11:13,000 --> 00:11:14,000
 and then four,

196
00:11:14,000 --> 00:11:16,000
 it's quite reassuring, actually.

197
00:11:16,000 --> 00:11:18,830
 But when you go rising, falling, you don't know what's

198
00:11:18,830 --> 00:11:20,000
 going to come next.

199
00:11:20,000 --> 00:11:22,000
 And so it forces you to let go.

200
00:11:22,000 --> 00:11:24,610
 That's all I wanted to say, but it is the first thing we

201
00:11:24,610 --> 00:11:28,000
 give, and there's a lot more that you can focus on,

202
00:11:28,000 --> 00:11:30,770
 and in the end, you should really be focusing on everything

203
00:11:30,770 --> 00:11:31,000
.

204
00:11:31,000 --> 00:11:33,000
 But don't discard the rising and falling.

205
00:11:33,000 --> 00:11:34,000
 Always come back to it.

206
00:11:34,000 --> 00:11:38,000
 It's a wonderful, fairly unique object, I suppose.

207
00:11:38,000 --> 00:11:41,150
 It's always there, and it's always there to teach you

208
00:11:41,150 --> 00:11:42,000
 something.

209
00:11:42,000 --> 00:11:45,570
 You know, and the layers of things that it can teach you is

210
00:11:45,570 --> 00:11:47,000
 quite incredible.

