 Namat dura ta na tayasa.
 Welcome everyone.
 Today we're broadcasting here live from Wat Thay of Los
 Angeles.
 Broadcasting over the internet via my web blog.
 And today I will be starting again to give talks at 1pm.
 So this is our first 1pm talk this week.
 Today's talk I thought I would talk about Dharma or Dhamma,
 the teaching of the Buddha
 in the working world or how it relates to work.
 So this is a topic which we can deal with on various levels
.
 And mainly we have the talking about work in terms of
 working in the world and then
 we have work in terms of working in the Dhamma, working in
 the Buddhist religion.
 For instance say working as a monk or working as a nun.
 And I do see quite often that people try to separate these
 two out.
 And I think this is one way of approaching the topic of
 work, how the Buddha Dhamma relates
 to work, is to separate out one's working life and one's
 Dharma practice.
 And another way of going about it is to try and bring one's
 life so that there is no difference
 between work and Dharma or work and Dhamma, work and
 practice.
 And so I'd like to sort of try to deal with these different
 ways of looking at how to
 live our lives in the world or how to make our Dhamma
 practice fit with our work life.
 And I think this is a question for people who aren't
 Buddhist as well.
 It's a question for all people how to make their public and
 their private life or their
 work and their home life mesh together or how to juggle the
 two.
 So I think it's an interesting topic to discuss.
 First when we talk about anything in Buddhism we have to
 look at it in terms of the Eightfold
 Noble Path or the three trainings which form the core of
 Buddhism.
 So how we often answer the question of how to fit one's
 livelihood into Dhamma practice,
 practice of the Buddha's teaching, we often answer it by
 referring to morality which is
 the first part of the path of the Buddha.
 So we will talk about the various ways that livelihood is
 incorrect or incompatible with
 Buddhism.
 And often we never go beyond this but so well it's
 important to start here, start with this
 as our basis as in all things, start with morality as the
 base.
 It's important that we then look beyond this.
 And so I'd like to say that I'm going to start here and
 explain this but this certainly isn't
 where we want to stop.
 I think it's a shame that often this is the only aspect of
 livelihood that ever gets discussed
 is that these things that you shouldn't do like as though
 if you didn't do this and this
 and this then you would be somehow proof you would be a
 good Buddhist or you would be guaranteed
 to progress on the path.
 The basis of right livelihood is morality because
 livelihood of course is an external
 thing it's generally a physical and a verbal undertaking
 where we undertake certain acts
 or certain speech for the purpose of feeding ourselves or
 the purpose of providing our
 necessities.
 And so as the very basis of, in all things, as the very
 basis of our entrance into Buddhism
 there are certain things which we're going to have to cut
 off.
 This is for, in the case for instance of people who had
 never practiced Buddhism before, where
 do you start?
 You want to start taking up meditation?
 Well what are the first things that you have to adjust in
 order that your meditation will
 progress?
 And of course morality is the first thing that we have to
 adjust.
 There are certain things, actions and certain speech that
 we have to give up.
 And in livelihood this is doubly so because livelihood is
 sort of the one thing that we
 have to carry out and continue in our lives.
 We may not have the opportunity to kill or to steal for
 other reasons but these can become
 very obvious or frequent in the case of livelihood.
 So for people who are hunters or are butchers or are
 thieves for instance, people who are
 prostitutes, even salespeople who lie or use deception to
 gain their livelihood.
 All of these things are really worth noting whereas we
 normally just talk about morality
 in terms of the things in general that we shouldn't do.
 It's worth noting that our livelihood is a place where we
're going to really have to
 examine and look at it as an obvious place for improvement
 because it's a sphere of our
 life that's going to lead us to do and to say bad things on
 a regular basis potentially.
 So in order to sell our product or in order to carry out
 our lives because it's something
 we do repetitively.
 It's very common that we're repeatedly breaking the precept
s.
 So if it's not killing or stealing then it's lying or
 cheating and so on.
 Doing bad things using speech in an improper way on a
 regular basis.
 And this is the basis of moral, a basis of right livelihood
.
 So when people ask what's a good job for me, is this a good
 job or how can I make a living
 and still practice the Dharma or practice the Buddhist
 teaching.
 And this is the first answer is that as long as we're able
 to keep to the five precepts
 and our livelihood doesn't lead us to do bad, do things
 which are immoral or say things
 which are immoral then it's really not against Dharma
 practice, against practice of the Buddhist
 teaching.
 So for instance we work in an office and we're a secretary
 or we are a office manager or something
 that really has no moral consequences then we can say well
 this is a livelihood which
 is pure.
 This is a livelihood which is not going to be a hindrance
 in our practice of the Buddhist
 teaching on the level of morality.
 And so as I say often we stop there in terms of livelihood
 but I really think this is some
 one area that deserves much deeper attention because in the
 end livelihood is really just
 about living our lives and even monks have a livelihood of
 sorts.
 But it's clear that even someone who in the world is
 carrying out their livelihood in
 the moral ethical "moral ethical way" is still carrying out
 a very different sort of livelihood
 than say a monk or a nun.
 So the difference here is worth looking at and worth coming
 to understand, coming to
 discuss and to more thinking about.
 And this the difference here is not so much on the level of
 morality but it comes to be
 on the level of concentration and on the level of wisdom.
 So we have these other two parts of the Buddhist teaching.
 But in this sense we can see it isn't just an issue of
 morality.
 That when we work in the world what we're dealing with is a
 real and clear disruption
 of our concentration, of our level of mental quietude and
 mental calm.
 That we're stressed out even with things which are "moral".
 And why I say "unquote" is because really it's a kind of
 immorality that's going on
 in the mind.
 The mind is still giving rise to greed and to anger and to
 stress and to worry and to
 depression.
 All of these negative mind states are still arising.
 This is a normal part of a moral ethical lifestyle on the
 lay level.
 So when we're talking about livelihood as being moral, as
 being perfectly right and
 a part of the Buddhist teaching, it really on a deeper
 level isn't so.
 And this is where people start to realize that they have to
 make a decision or they
 have to find some way to compromise or to come to terms
 with this seeming dichotomy
 or this split.
 There's the worldly life where you have to eat, you have to
 provide for your necessities.
 And then there's the "dhamma", there's the practice of
 meditation.
 Sometimes these two can seem very incompatible.
 You work 9-5 and at 5 o'clock you get home and you're
 trying to meditate but you're so
 stressed out that it can be very difficult to meet with any
 progress on the path.
 It just feels like you're taking three steps forward, two
 steps back, if that.
 So this is where people will often have to make a decision.
 People are often too quick to say there must be a way to
 have your cake and eat it too,
 in this sense to stay as an ordinary person, working in an
 office and still expect to find
 real progress on the path.
 I think more realistically people come to the realization
 that due to their situation,
 they may have debts or they may have responsibilities that
 they aren't able to ordain.
 They aren't able to devote their lives to the practice of
 the Buddha's teaching.
 And so they stay as lay people.
 But I think there's a lot of people also who don't realize
 the great benefit or this great
 path which exists and this is the renunciation of the home
 life.
 People might often think that it's just a luxury that a
 very few people have or it's
 something that is taking advantage of other people or so on
.
 The life of say a monk or a nun is in its ideal form.
 It's the giving up of all of our luxuries.
 The reason we're able to do away with this nine to five
 work ethic and we're able to
 be free from this need to stress and need to bother is
 because we've given up everything.
 We've given up money, we've given up possessions, we've
 given up all attachments in the home
 life that we aren't able to seek out the entertainments
 that other people have.
 We aren't able to seek out the luxuries that other people
 have.
 Our food intake is one meal a day.
 We have all of these rules.
 Our clothes, our rags, our simple cloth and everything else
 that we use.
 It's only for the purpose of spreading and teaching and
 helping other people.
 We're not allowed to even say have a computer for the
 purpose of surfing the net for instance.
 It has to be for the purpose of spreading the Buddha's
 teaching.
 So we take on a life of great discipline and it's a great
 work and it's a livelihood that
 is undertaken.
 As a result we're able to do away with this.
 There's this stress and this immorality in the mind or this
 distraction which is inherent
 in the lay life, in the life of trying to make a living in
 the world that is done away
 with.
 So at the level of concentration we have to... we can't
 just come to say that well you know
 my job isn't hurting anyone.
 And here is where people will try to find a livelihood in
 the world that is also in
 line with the Dharma.
 So trying to say teach meditation and use it as a
 livelihood.
 So some people will be meditation teachers and will receive
 donations, monetary donations
 or even charge for their meditation services.
 Some people go one step down and will do things which are
 generally useful.
 People wanting to become doctors or nurses.
 Even people who teach something similar to meditation,
 maybe psychology or social workers
 or motivational speakers or so on.
 Thinking that this is something which is generally useful
 for people and so it's somehow in line
 with the Dharma.
 I think there's some merit here for some people to find
 this sort of a livelihood.
 I think it can be a way of compromising or coming to a
 compromise where you don't have
 to become a monk or a nun and really give up this luxury of
 being able to buy things,
 being able to have the things and being able to get
 whatever you want when you want it.
 And still being able to practice the Dharma or being in
 line with the Buddhist teaching.
 But the problem is you're always going to run into this
 sort of greed or this necessity
 to beg almost or to hint or to insinuate until you're able
 to give up.
 See because being a monk you're at this, you've taken it to
 the extreme where if you don't
 get food you don't eat.
 And it's really just not possible if you're still living in
 the world because you have
 rent, you have a car, you have all these overhead expenses.
 The idea behind becoming a monk or a nun is doing away with
 all overhead expenses.
 So you don't own anything, you don't have any debts, you
 live wherever there's a place
 to stay.
 So in a place that people have created for you, you would
 live in a meditation center
 which was solely for the purpose of people to come and med
itate.
 You couldn't say go out and have your own private house or
 private dwelling where you
 could just stay and live.
 Because this is overhead then you need to pay the rent and
 you need to pay the utilities
 and so on.
 Becoming a monk or a nun is if necessary you live on the
 side of the street, you live in
 a park or you live in a forest or so on.
 And we run into trouble in places like America so we have
 to kind of fudge it a little bit.
 But the same principle remains that you're staying in the
 meditation center, you're staying
 in the monastery where that people have created for the
 purpose of spreading Buddhism.
 You don't have your own house.
 It's a public place.
 It's a place where for the purpose and for the benefit of
 all people.
 Sort of like the equivalent of a lay meditation teacher
 having to live in the meditation center
 and not having their own private house or private home.
 And so as a monk or a nun if you don't have support and if
 it comes to the point where
 it seems like there's not going to be even enough food
 donated or any of your requisites
 then you simply do without.
 And this is a frequent, it's likely being a homeless person
.
 For anyone who's ever been in that position and they can
 understand how it is.
 For anyone who's never been in that position it can be
 quite a difficult situation but
 it's quite different from say trying to live an ordinary
 life as say a Dharma teacher or
 one who is teaching or helping to spread the Buddhist
 teaching because you always have to
 insinuate or hint at the very least if not outright begging
 or asking or charging for
 your services.
 And this is again something which is going to bother one's
 concentration.
 It creates feelings of guilt and it can create feelings of
 stress and anxiety.
 Always wondering or worrying whether you're going to get
 enough funds to pay for your
 necessities and so on.
 So what we're talking about here is as being a monk or a
 nun is going the next step where
 we give up all, even to that extent we give up all of the,
 all of our necessities, all
 of our requirements, all of our attachments to the home
 life and we become homeless.
 So in terms of concentration we have, we can see that if we
're really going to gain a very
 strong state of focus and concentration then we have to
 start to change many more things
 about who we are and how we live our lives.
 Many people can do this by say finding a simple life and
 maybe a life which is in line with
 the Dharma but in the end it's much more complicated or
 complex or subtle than simply saying I
 won't do this, I won't make my livelihood in this way or in
 that way.
 And there are many people who think that they can do this
 or that job and still be able
 to help people or do something good and beneficial for
 other people.
 On a moral, on a level of morality this may be true but on
 the level of concentration
 it's going to be much more difficult and so this is I think
 a real, a real reason why
 people might decide to actually leave their jobs behind and
 become a monk or a nun.
 Although it's not necessary and I suppose it's worth
 mentioning that it is possible
 to gain some level of concentration, it's just something
 which is going to be much more
 difficult and cause a lot more unnecessary stress and
 difficulty because of the amount
 of work that we have to do.
 And finally on the level of wisdom, when we talk about
 livelihood we can look at it in
 terms of the wisdom or the wisdom which we're gaining in
 our lives or our understanding.
 And so the problem with livelihood living in the world is
 that it often has a lot to
 do with creating states of ego and states of delusion,
 states of attachment to self
 and conceit.
 And when we examine our livelihood in terms of wisdom we
 have to see that we're actually
 doing and saying things which are more or less futile or
 useless and actually in many
 ways just creating more and more delusion for people.
 So for instance when we sell things we're often selling
 things to people that they don't
 really need, when we work in an office we're often being
 part of the economy and helping
 people to get things that they don't need or do things
 maybe for questionable ends and
 so on.
 And that well it may not be breaking precepts and it may
 not even be bothering our concentration
 it's in the end creating the basis for both of these that's
 simply the wrong, you know
 like working in an office that working for this company or
 that company, working for
 one of the big retailers or so on, that we end up fostering
 or encouraging these very
 things we're encouraging stress, we're encouraging greed
 and we're encouraging anger and hatred
 and fear and worry and so on.
 We're being a part of this and we're creating it in
 ourselves as well because of the very
 nature of the job that maybe we can go through our work
 being at peace with ourselves but
 in the end it's a job which is not bringing real peace and
 happiness to people, it's not
 helping people to come closer to the Buddha's teaching, it
's not helping people to become
 free from suffering.
 And so we have to look at really the basis of what we do,
 the amount of delusion involved
 with our work and we can see that the amount of time that
 we have to spend on this earth
 and how we're spending it, how we're using this time and
 what benefit it's bringing to
 ourselves and to other people and that actually through the
 encouraging people to get and
 to chase after and to build up their egos and their conce
its and their desires and their
 aversions and so on.
 We're actually creating delusion in the world through our
 work, we're becoming a part of
 these organizations and part of society and part of the
 economy and part of the government
 and so on.
 We're just another cog in the wheel and this is really at
 the basis of why the world is
 in such a mess that we're getting very much caught up with
 systems and structures which
 are creating suffering and this is something which then is
 going to lead to more and more.
 This is the reason why then we have these stressful jobs
 and this is the reason why
 we then do and say bad things.
 Wisdom is actually the core, it's the root of the problem
 which we're trying to root
 out, we're trying to cut out.
 In terms of livelihood this is something that is kind of
 the deepest level that we can look
 at our work where when we're working in the world for
 instance that we might decide for
 ourselves that it's time to simplify things that rather
 than getting a big house and a
 nice car and a good retirement plan we might decide to live
 a much simpler life which might
 be much more outwardly difficult and not as secure but is
 in the end a lot more noble,
 a lot more peaceful, a lot more based on wisdom and it's
 going to give us great concentration
 and allow us to live moral and ethical lives.
 We really have to decide for ourselves and sometimes it
 takes a compromise.
 There are many people out there who are not going to be
 able to leave the home life but
 there are also many people who do change their livelihood.
 I had one student who was a biologist and he used to cut up
 rats before he started meditating
 and once he started meditating he realized that he couldn't
 do that anymore and now he
 works for the government.
 He does something for the government, he was able to change
 his livelihood to that extent.
 I would encourage I think a greater sort of change of
 lifestyle and the ultimate sort
 of perfection of a livelihood for someone who has not
 become a monk is epitomized in
 the Buddhist Canon, the Buddhist Bible I guess if you will,
 with this man who makes pots
 and he goes down to the river and he gets loose clay that
 was dug up by rats or cave
 dined by the rain or so on, washed up by the river and he
 gathered up the clay and he would
 go into the forest and collect wood that had fallen down
 and he would mold the clay into
 pots and fire them up using the discarded dead wood and he
 would make these simple pots
 and put them by the side of the road and sit there all day
 as people walked by and people
 would walk by and they would ask him how much is that pot,
 how much is that pot and whatever
 pot they pointed to he would just say well you leave some
 beans, leave some rice, whatever
 you think it's worth and take whatever you like and this is
 how he made his livelihood,
 you know just living by the side of the road or maybe
 living in a simple dwelling or so
 on and selling these pots and so while we couldn't do
 something exactly like this here
 in the west you still need or in the modern world you still
 need to pay rent and so on
 and so on, we take this as an example and we live our lives
 trying to emulate this kind
 of idea or even better to emulate the life of a monk where
 we know we have to make some
 compromises and we will never be able to live a pure life
 unless we are able to make the
 jump and give up our attachments to the world but we make
 steps in this direction so rather
 than thinking we want to become the manager or the boss or
 the head or become rich and
 make a fortune or become famous or become a big shot we are
 content with something very
 simple and something which the simpler the better and if it
 means we have to leave our
 house and get a smaller apartment or we have to sell our
 car and get a cheaper car or so
 on this or this or these many different things we have to
 give up some of our luxuries then
 I think this is the steps that we have to take and these
 are the steps in the right
 direction and to the point where we are able to do away
 with all of our burdens all of
 our things that are required of us in the world and are
 able to make the leap and become
 a monk or a nun or become a full time meditator or go to
 live in a monastery even some people
 who don't ordain but go to live and help out in a monastery
 or help out in a meditation
 centre, help to build a meditation centre.
 I know many people who aren't able to ordain but they are
 working very hard to build up
 meditation centres in their area so that once they can
 build it up then they will be able
 to give up this sort of a life so finding a way to get
 closer and closer to the dharma
 and not sort of fooling ourselves into thinking that we can
 have our cake and eat it too.
 I think I would advocate that we should be trying to change
 our lifestyles and trying
 all the time to get closer and closer step by step to
 lifestyle where we are living the
 dharma and I think I am justified in this.
 I think many people they are going to say it is pretty
 biased to just say you have to
 become a monk or you have to become a nun or you have to
 leave the whole life but you
 know we are on this earth for 80 years maybe.
 Nobody lives to be 100 anymore I don't think very few
 anyway.
 80 years at most let's say and then that's it.
 Then we don't know where we are going we don't know what
 happens and here we are chasing
 after nothing, chasing after something that is meaningless,
 living our lives running around
 in a hamster wheel for nothing that in the end we will have
 to leave behind and not know
 where we are going, not know what comes next and dying in
 uncertainty with the mind which
 is still distracted and unfocused and unclear and impure
 that we still have many things
 about ourselves that we feel unresolved inside that there
 are many things about ourselves
 that we don't understand and that we will live our lives,
 we will die in a confused state
 of mind.
 So it's just sort of some thoughts on how we should live
 our lives and sort of what
 livelihood means in a real sense that we are not just
 talking about what are the right
 kinds of work that we can do in lay life that aren't going
 to be against, aren't going to
 be wrong or sinful for instance.
 In Buddhism we are actually talking about changing the way
 we live our lives and giving
 up our burdens, giving up our attachments more and more to
 the point where we are able to
 free ourselves from all suffering.
 So thank you all I think I'm recovering still from this flu
 but hopefully over the next
 few days I will be able to give more and more talks.
 Thank you all for tuning in and if anyone would like to
 discuss I'm happy to stick around
 for a little bit longer.
 But that's all for today thanks for coming, bye.
