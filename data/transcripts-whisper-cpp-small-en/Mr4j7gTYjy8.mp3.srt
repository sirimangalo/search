1
00:00:00,000 --> 00:00:04,770
 Those who are new can have a hard time to understand nir

2
00:00:04,770 --> 00:00:05,000
vana.

3
00:00:05,000 --> 00:00:08,000
 It can take multiple lives to achieve enlightenment.

4
00:00:08,000 --> 00:00:09,000
 But we live now.

5
00:00:09,000 --> 00:00:12,460
 How do you explain Buddhism is different from other hyp

6
00:00:12,460 --> 00:00:15,880
nosis theorems, like religion, etc., just to give the mind

7
00:00:15,880 --> 00:00:17,000
 some sense of purpose?

8
00:00:17,000 --> 00:00:22,400
 Well, first of all, giving the mind some sense of purpose

9
00:00:22,400 --> 00:00:24,000
 is a good thing, no?

10
00:00:24,000 --> 00:00:29,890
 But just giving the mind some sense of purpose implies that

11
00:00:29,890 --> 00:00:34,000
 it actually doesn't lead to that purpose.

12
00:00:34,000 --> 00:00:39,000
 It doesn't achieve the purpose that it inclines towards.

13
00:00:39,000 --> 00:00:41,000
 That's not the case in Buddhism.

14
00:00:41,000 --> 00:00:44,290
 The purpose that it gives you is actually something you can

15
00:00:44,290 --> 00:00:46,000
 achieve in this lifetime.

16
00:00:46,000 --> 00:00:48,000
 Now, you may not.

17
00:00:48,000 --> 00:00:50,000
 It may, as you say, take lifetimes.

18
00:00:50,000 --> 00:00:55,410
 But that's, I think, inconsequential. The difference is, in

19
00:00:55,410 --> 00:00:59,000
 the sense that your question is being asked,

20
00:00:59,000 --> 00:01:03,630
 those other systems, whichever they might be, that provide

21
00:01:03,630 --> 00:01:09,000
 a sense of purpose but do nothing else,

22
00:01:09,000 --> 00:01:14,000
 have no development.

23
00:01:14,000 --> 00:01:17,100
 What you're saying is that there are systems that don't

24
00:01:17,100 --> 00:01:19,000
 actually lead to any result.

25
00:01:19,000 --> 00:01:22,930
 Now, first of all, having a sense of purpose can be highly

26
00:01:22,930 --> 00:01:24,000
 empowering.

27
00:01:24,000 --> 00:01:26,000
 So there's that, and I assume you acknowledge that.

28
00:01:26,000 --> 00:01:28,000
 But Buddhism, of course, does much more than that.

29
00:01:28,000 --> 00:01:33,150
 Buddhism does lead you to nirvana, albeit potentially quite

30
00:01:33,150 --> 00:01:34,000
 slowly.

31
00:01:34,000 --> 00:01:40,000
 For others, it can happen in this life.

32
00:01:40,000 --> 00:01:48,640
 So there's really no comparison between Buddhism actually

33
00:01:48,640 --> 00:01:52,000
 does lead you there.

34
00:01:52,000 --> 00:01:58,000
 So the question kind of is missing that element.

35
00:01:58,000 --> 00:02:03,280
 Now, as to not being able to understand nirvana, I think

36
00:02:03,280 --> 00:02:06,000
 there's another element to this question.

37
00:02:06,000 --> 00:02:14,380
 It has to do with people's inability to really strive for n

38
00:02:14,380 --> 00:02:16,000
irvana.

39
00:02:16,000 --> 00:02:21,000
 You're not quite asking that, but there is that sense of

40
00:02:21,000 --> 00:02:27,000
 what is it that we're aiming for.

41
00:02:27,000 --> 00:02:34,200
 And in that sense, usually this is a valid statement that

42
00:02:34,200 --> 00:02:36,000
 yes, indeed,

43
00:02:36,000 --> 00:02:48,000
 the biggest problem with Buddhist practice is the ideation,

44
00:02:48,000 --> 00:02:54,000
 the conceptualization of nirvana as something, as a thing,

45
00:02:54,000 --> 00:02:59,000
 often a very scary thing, something that is, "I'll be gone.

46
00:02:59,000 --> 00:03:06,000
 What will happen to me if I achieve nirvana?"

47
00:03:06,000 --> 00:03:10,000
 But in fact, nirvana means freedom.

48
00:03:10,000 --> 00:03:14,000
 It's a way of saying, well, it's a specific way of saying

49
00:03:14,000 --> 00:03:18,000
 freedom in the sense of freeing yourself, letting go.

50
00:03:18,000 --> 00:03:21,000
 So nirvana is not being bound up.

51
00:03:21,000 --> 00:03:23,000
 That's literally what it means.

52
00:03:23,000 --> 00:03:27,000
 It simply refers to the culmination of letting go,

53
00:03:27,000 --> 00:03:35,390
 which for the meditator, it's quite quickly evident to be a

54
00:03:35,390 --> 00:03:36,000
 good thing

55
00:03:36,000 --> 00:03:38,680
 as we see that the things that we're holding on to are

56
00:03:38,680 --> 00:03:40,000
 causing a suffering.

57
00:03:40,000 --> 00:03:45,360
 We see that our desires are causing a suffering, our

58
00:03:45,360 --> 00:03:49,000
 aversion is causing a suffering.

59
00:03:49,000 --> 00:03:52,710
 All of these things, our delusion, our arrogance, our conce

60
00:03:52,710 --> 00:03:53,000
it,

61
00:03:53,000 --> 00:03:56,000
 all of these things are causing us suffering.

62
00:03:56,000 --> 00:03:58,820
 So we see that this kind of nirvana in the sense of letting

63
00:03:58,820 --> 00:03:59,000
 go,

64
00:03:59,000 --> 00:04:02,000
 which is all it means, is a good thing.

65
00:04:02,000 --> 00:04:07,690
 Nirvana simply means that final where you just say, "Enough

66
00:04:07,690 --> 00:04:09,000
," and you let go,

67
00:04:09,000 --> 00:04:14,640
 and the mind is, it's like the mind is here, all the

68
00:04:14,640 --> 00:04:17,000
 experiences are around it.

69
00:04:17,000 --> 00:04:20,000
 So the mind keeps going out to different things.

70
00:04:20,000 --> 00:04:24,000
 Nirvana is when the mind says, "Enough," and it stops.

71
00:04:24,000 --> 00:04:26,000
 It doesn't go anywhere.

72
00:04:26,000 --> 00:04:31,000
 It doesn't run away and leave its center.

73
00:04:31,000 --> 00:04:36,000
 It becomes perfectly centered.

74
00:04:36,000 --> 00:04:39,000
 In a sense, there's a cessation, so there's no thinking,

75
00:04:39,000 --> 00:04:41,000
 there's no awareness in that sense.

76
00:04:41,000 --> 00:04:47,000
 But it shouldn't be scary, it's just that no more.

77
00:04:47,000 --> 00:04:50,000
 Enough. That's all.

