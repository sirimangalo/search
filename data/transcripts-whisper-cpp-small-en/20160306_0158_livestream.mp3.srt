1
00:00:00,000 --> 00:00:09,000
 Good evening everyone.

2
00:00:09,000 --> 00:00:23,040
 I'm broadcasting live March 5th.

3
00:00:23,040 --> 00:00:33,540
 Today is quote, it's actually quite an interesting little

4
00:00:33,540 --> 00:00:35,040
 passage.

5
00:00:35,040 --> 00:00:40,390
 When the Buddha was staying in Kapilawatu in the park of

6
00:00:40,390 --> 00:00:44,040
 the Banyan tree.

7
00:00:44,040 --> 00:00:53,040
 I think that's Nigrodarama.

8
00:00:53,040 --> 00:00:59,040
 Nigrodarama which was built by his family.

9
00:00:59,040 --> 00:01:05,280
 His mother and his father, their families got together and

10
00:01:05,280 --> 00:01:12,040
 built Nigrodarama.

11
00:01:12,040 --> 00:01:22,040
 Mahanama, there are two Mahanamas that we know of.

12
00:01:22,040 --> 00:01:30,670
 One of them was actually the first five disciples of the

13
00:01:30,670 --> 00:01:31,040
 Buddha.

14
00:01:31,040 --> 00:01:34,040
 But that's not this one.

15
00:01:34,040 --> 00:01:37,040
 Mahanama is actually an interesting word.

16
00:01:37,040 --> 00:01:48,040
 That means great name, kind of like a well known person.

17
00:01:48,040 --> 00:01:58,040
 Mahanama was a name they gave to princes.

18
00:01:58,040 --> 00:02:08,040
 But this Mahanama most likely was Anuruddha's brother.

19
00:02:08,040 --> 00:02:16,040
 Because it happened that when Anuruddha became a monk,

20
00:02:16,040 --> 00:02:20,040
 he did so after discussion with his brother Mahanama.

21
00:02:20,040 --> 00:02:25,670
 Mahanama said to Anuruddha, "All these other Sakyans are

22
00:02:25,670 --> 00:02:29,040
 going forth and becoming monks."

23
00:02:29,040 --> 00:02:34,040
 But no one from our family has become a monk yet.

24
00:02:34,040 --> 00:02:38,040
 So either you become a monk or I become a monk.

25
00:02:38,040 --> 00:02:43,040
 One of us has too because our family is unrepresented.

26
00:02:43,040 --> 00:02:47,150
 Everyone's going to say, "Oh look at these, this family's

27
00:02:47,150 --> 00:02:49,040
 too lazy to become monks."

28
00:02:49,040 --> 00:02:59,040
 And they're not following after the tradition of Siddhata.

29
00:02:59,040 --> 00:03:04,770
 So Anuruddha said, "Well, gee, becoming a monk, that sounds

30
00:03:04,770 --> 00:03:07,040
 quite tough."

31
00:03:07,040 --> 00:03:13,430
 "I've heard what that's like to live under trees and stuff

32
00:03:13,430 --> 00:03:14,040
."

33
00:03:14,040 --> 00:03:17,040
 "That sounds difficult to me."

34
00:03:17,040 --> 00:03:22,040
 "You become a monk and I'll stay and I'll be a lay person."

35
00:03:22,040 --> 00:03:25,040
 "I'll stay as a lay person and live the lay life."

36
00:03:25,040 --> 00:03:29,040
 It's got to be easier, right?

37
00:03:29,040 --> 00:03:34,810
 Mahanama says very well and he starts to explain to Anurudd

38
00:03:34,810 --> 00:03:35,040
ha

39
00:03:35,040 --> 00:03:37,040
 all the things that he'd have to do.

40
00:03:37,040 --> 00:03:38,710
 He says, "Well, they'll have to show you how to be a lay

41
00:03:38,710 --> 00:03:40,040
 person."

42
00:03:40,040 --> 00:03:44,040
 "How to run the affairs of our household."

43
00:03:44,040 --> 00:03:47,040
 He tells them all about the farms that they own

44
00:03:47,040 --> 00:03:49,640
 and that they have to care for and the workers that have to

45
00:03:49,640 --> 00:03:50,040
 be paid

46
00:03:50,040 --> 00:03:55,940
 and crops that have to come in and the merchants that have

47
00:03:55,940 --> 00:03:57,040
 to be dealt with

48
00:03:57,040 --> 00:04:05,560
 and all the work that has to be done to cultivate and reap,

49
00:04:05,560 --> 00:04:09,040
 harvest and prepare

50
00:04:09,040 --> 00:04:15,040
 and care for the crops and animals and people.

51
00:04:15,040 --> 00:04:19,650
 And he goes on and on and Anuruddha says, "Stop, stop, stop

52
00:04:19,650 --> 00:04:20,040
."

53
00:04:20,040 --> 00:04:26,040
 "This is what it takes to be a lay person."

54
00:04:26,040 --> 00:04:29,040
 "This is what it takes to live in the world."

55
00:04:29,040 --> 00:04:31,040
 "There's no way being a monk could be harder than that."

56
00:04:31,040 --> 00:04:34,040
 He said, "I'll become a monk. I've changed my life."

57
00:04:34,040 --> 00:04:37,040
 "I'll become a monk and you stay as a lay person."

58
00:04:37,040 --> 00:04:42,040
 That sounds like hell to me.

59
00:04:42,040 --> 00:04:48,470
 And so Mahanama almost became a monk, but because he was

60
00:04:48,470 --> 00:04:49,040
 kind,

61
00:04:49,040 --> 00:04:54,040
 he let his younger brother become a monk and stand in.

62
00:04:54,040 --> 00:05:03,770
 He stayed to run their household, run their family business

63
00:05:03,770 --> 00:05:05,040
.

64
00:05:05,040 --> 00:05:12,040
 But we get a lot of these talks between him and the Buddha.

65
00:05:12,040 --> 00:05:17,040
 It seems the Buddha had a special place in his heart for...

66
00:05:17,040 --> 00:05:20,510
 He even kept a special place for Mahanama, or Mahanama had

67
00:05:20,510 --> 00:05:23,040
 a special place for the Buddha

68
00:05:23,040 --> 00:05:29,510
 and in the sense of having a special interest in coming to

69
00:05:29,510 --> 00:05:30,040
 see the Buddha

70
00:05:30,040 --> 00:05:34,290
 because he wasn't able to ordain, so he wanted to get

71
00:05:34,290 --> 00:05:37,040
 teachings whenever he could.

72
00:05:37,040 --> 00:05:41,480
 And so this is one such teaching and he's sort of the

73
00:05:41,480 --> 00:05:54,040
 iconic or the exemplary lay person.

74
00:05:54,040 --> 00:05:59,420
 As a result of his backstory, he makes the perfect foil for

75
00:05:59,420 --> 00:06:00,040
 the Buddha

76
00:06:00,040 --> 00:06:03,040
 to talk about how lay people should live.

77
00:06:03,040 --> 00:06:06,390
 So his talks are always quite interesting for lay people, I

78
00:06:06,390 --> 00:06:07,040
 think.

79
00:06:07,040 --> 00:06:10,410
 And so whereas many of the talks we say, "Well, this is

80
00:06:10,410 --> 00:06:11,040
 more directed towards monks

81
00:06:11,040 --> 00:06:14,040
 and so you have to adapt it if you want to practice it as a

82
00:06:14,040 --> 00:06:15,040
 lay person."

83
00:06:15,040 --> 00:06:18,480
 This one is the other way. It's much more relatable to lay

84
00:06:18,480 --> 00:06:19,040
 people.

85
00:06:19,040 --> 00:06:22,880
 So his first question is, "How does one become a lay

86
00:06:22,880 --> 00:06:24,040
 disciple?"

87
00:06:24,040 --> 00:06:27,040
 And so here we have the Buddha's words.

88
00:06:27,040 --> 00:06:33,080
 If you were wondering whether there was any text-based

89
00:06:33,080 --> 00:06:36,040
 method of becoming a Buddhist

90
00:06:36,040 --> 00:06:39,760
 or a lay Buddhist, the Buddha says, "When one has taken

91
00:06:39,760 --> 00:06:44,040
 refuge in the Buddha, the Dhamma, the Sangha."

92
00:06:44,040 --> 00:06:47,040
 That's all it takes.

93
00:06:47,040 --> 00:06:51,040
 Then one is a lay disciple.

94
00:06:51,040 --> 00:06:55,040
 One has made a determination that the Buddha is my refuge,

95
00:06:55,040 --> 00:06:58,040
 his teaching is my refuge.

96
00:06:58,040 --> 00:07:02,150
 And the teachers who pass on his teaching, they are my

97
00:07:02,150 --> 00:07:03,040
 refuge.

98
00:07:03,040 --> 00:07:07,040
 Makes that determination, one is then a lay protestant.

99
00:07:07,040 --> 00:07:10,040
 That's all it takes.

100
00:07:10,040 --> 00:07:15,370
 And so then he goes on to say, "Well, how is it lay

101
00:07:15,370 --> 00:07:18,040
 disciple virtuous?"

102
00:07:18,040 --> 00:07:20,960
 Just because you're a lay person, right, doesn't actually

103
00:07:20,960 --> 00:07:24,040
 mean you're any good at anything.

104
00:07:24,040 --> 00:07:27,230
 And so after taking refuge, the next step is how to be

105
00:07:27,230 --> 00:07:28,040
 virtuous,

106
00:07:28,040 --> 00:07:32,530
 how to actually be a good person, a good Buddhist, not just

107
00:07:32,530 --> 00:07:35,040
 a Buddhist, but a good Buddhist.

108
00:07:35,040 --> 00:07:37,040
 And so he gives the five precepts.

109
00:07:37,040 --> 00:07:40,800
 And these two together really make up what it means to be a

110
00:07:40,800 --> 00:07:42,040
 good Buddhist.

111
00:07:42,040 --> 00:07:46,310
 If you're breaking the five precepts, you could say you're

112
00:07:46,310 --> 00:07:47,040
 still Buddhist,

113
00:07:47,040 --> 00:07:50,040
 which is not a very good one.

114
00:07:50,040 --> 00:07:53,760
 You don't take refuge in the three refuges, Buddha, the D

115
00:07:53,760 --> 00:07:55,040
hamma, and Sangha,

116
00:07:55,040 --> 00:07:58,040
 and you're not a Buddhist at all.

117
00:07:58,040 --> 00:08:02,060
 But these together make you not only a Buddhist, but a good

118
00:08:02,060 --> 00:08:03,040
 Buddhist.

119
00:08:03,040 --> 00:08:06,440
 And you take the three refuges, you're Buddhist, when you

120
00:08:06,440 --> 00:08:09,700
 keep the five precepts, you're a good Buddhist, you're

121
00:08:09,700 --> 00:08:11,040
 virtuous.

122
00:08:11,040 --> 00:08:14,190
 And then two more questions which are probably more

123
00:08:14,190 --> 00:08:16,040
 interesting is,

124
00:08:16,040 --> 00:08:19,040
 how do you help yourself and not help others?

125
00:08:19,040 --> 00:08:23,040
 And how do you help yourself and others?

126
00:08:23,040 --> 00:08:26,790
 They're interesting especially because they talk about lay

127
00:08:26,790 --> 00:08:28,040
 people teaching,

128
00:08:28,040 --> 00:08:35,150
 lay people encouraging others, passing the teaching on to

129
00:08:35,150 --> 00:08:36,040
 others.

130
00:08:36,040 --> 00:08:41,040
 So not only having faith, virtue, renunciation,

131
00:08:41,040 --> 00:08:47,770
 and desire to hear the Dhamma, desire to practice the Dham

132
00:08:47,770 --> 00:08:49,040
ma.

133
00:08:50,040 --> 00:08:56,040
 [silence]

134
00:08:56,040 --> 00:09:02,600
 Cultivating these things in oneself, that's helping oneself

135
00:09:02,600 --> 00:09:03,040
.

136
00:09:03,040 --> 00:09:08,040
 [silence]

137
00:09:08,040 --> 00:09:16,040
 But the idea behind this, the last paragraph,

138
00:09:16,040 --> 00:09:22,040
 the last question, how one helps others also is that,

139
00:09:22,040 --> 00:09:25,040
 there's more than just helping yourself.

140
00:09:25,040 --> 00:09:33,040
 You should strive to establish such things in others.

141
00:09:33,040 --> 00:09:36,040
 I mean should, should, shouldn't, it's funny, you see,

142
00:09:36,040 --> 00:09:41,040
 it's curious how it avoids such words as should.

143
00:09:41,040 --> 00:09:45,040
 Don't just help yourself but help others as well.

144
00:09:45,040 --> 00:09:49,040
 But that's important because not everyone is a teacher

145
00:09:49,040 --> 00:09:52,040
 and not everyone is in a position to teach others

146
00:09:52,040 --> 00:09:57,040
 and it's not about going out and finding students

147
00:09:57,040 --> 00:10:01,040
 and pushing to be a teacher or wanting to be a teacher

148
00:10:01,040 --> 00:10:04,740
 or wanting to, wanting other people to understand the Dhamm

149
00:10:04,740 --> 00:10:05,040
ins.

150
00:10:05,040 --> 00:10:09,040
 It's about the kindness of it.

151
00:10:09,040 --> 00:10:13,040
 It's about the compassion.

152
00:10:13,040 --> 00:10:18,040
 And to just cut people off and to not share good things.

153
00:10:18,040 --> 00:10:20,040
 It's like not sharing your possessions,

154
00:10:20,040 --> 00:10:25,150
 not sharing your treasure with your family, with your

155
00:10:25,150 --> 00:10:27,040
 friends,

156
00:10:27,040 --> 00:10:31,040
 with your children, to keep it all to yourself.

157
00:10:31,040 --> 00:10:36,040
 It's like keeping a great treasure to yourself.

158
00:10:36,040 --> 00:10:43,040
 You have these, you have these things for yourself.

159
00:10:43,040 --> 00:10:45,040
 It's a question of whether you share them with others.

160
00:10:45,040 --> 00:10:49,040
 Now, there may not be the opportunity and,

161
00:10:49,040 --> 00:10:52,040
 so you may have to keep the treasures to yourself.

162
00:10:52,040 --> 00:10:54,040
 And people might not want your treasures,

163
00:10:54,040 --> 00:10:57,040
 they might not see them as treasure.

164
00:10:57,040 --> 00:11:12,040
 And maybe no one who is able to share the treasure with you

165
00:11:12,040 --> 00:11:12,040
.

166
00:11:12,040 --> 00:11:16,040
 But we try and help others.

167
00:11:16,040 --> 00:11:22,040
 You see other people suffering and often we can't and then,

168
00:11:22,040 --> 00:11:24,040
 there's no need to be afraid.

169
00:11:24,040 --> 00:11:27,040
 You can't share your treasure, this is a good thing.

170
00:11:27,040 --> 00:11:31,580
 Every moment that we're mindful, every moment that a person

171
00:11:31,580 --> 00:11:32,040
's mindful,

172
00:11:32,040 --> 00:11:36,040
 it's a clear thought, a clear moment.

173
00:11:36,040 --> 00:11:38,040
 That's a good thing.

174
00:11:38,040 --> 00:11:40,040
 If you can explain that to someone,

175
00:11:40,040 --> 00:11:44,030
 if you can bring someone else to have a moment of clear

176
00:11:44,030 --> 00:11:45,040
 thought,

177
00:11:45,040 --> 00:11:47,040
 that's a good thing.

178
00:11:47,040 --> 00:11:49,040
 You've helped them.

179
00:11:49,040 --> 00:11:54,040
 You've not only helped yourself, but you helped others.

180
00:11:54,040 --> 00:11:57,040
 You should never be afraid to help.

181
00:11:57,040 --> 00:12:01,040
 You'll be afraid to share the teaching.

182
00:12:01,040 --> 00:12:04,630
 It doesn't take a lot, sometimes all it takes is showing

183
00:12:04,630 --> 00:12:06,040
 people how to do

184
00:12:06,040 --> 00:12:09,070
 sitting meditation, how to say to themselves, "Rise in the

185
00:12:09,070 --> 00:12:10,040
 Father",

186
00:12:10,040 --> 00:12:14,040
 how to focus their minds.

187
00:12:14,040 --> 00:12:17,390
 Sometimes it doesn't even take that, you can just pass a

188
00:12:17,390 --> 00:12:18,040
 book along

189
00:12:18,040 --> 00:12:21,040
 and pass a link along,

190
00:12:21,040 --> 00:12:28,040
 send people in the right direction.

191
00:12:28,040 --> 00:12:33,040
 And you help others.

192
00:12:33,040 --> 00:12:36,030
 So that's what this is all about, helping ourselves,

193
00:12:36,030 --> 00:12:39,040
 helping others.

194
00:12:39,040 --> 00:12:43,040
 That's what this group is all about.

195
00:12:43,040 --> 00:12:46,040
 I really appreciate all of you coming together,

196
00:12:46,040 --> 00:12:52,040
 all of us coming together as a community.

197
00:12:52,040 --> 00:12:55,040
 It's great to have this online group.

198
00:12:55,040 --> 00:12:58,240
 It gives me encouragement, and I can see it gives other

199
00:12:58,240 --> 00:13:00,040
 people encouragement as well.

200
00:13:00,040 --> 00:13:03,040
 We shouldn't take these things lightly.

201
00:13:03,040 --> 00:13:06,040
 We should be proud of ourselves.

202
00:13:06,040 --> 00:13:10,040
 Not proud like that, but we should feel happy.

203
00:13:10,040 --> 00:13:15,180
 We should rejoice in the goodness of coming together in

204
00:13:15,180 --> 00:13:17,040
 this way,

205
00:13:17,040 --> 00:13:20,740
 the goodness that we develop as a community and through

206
00:13:20,740 --> 00:13:25,040
 just being here for each other.

207
00:13:25,040 --> 00:13:31,040
 So may this community live long, may we help our help,

208
00:13:31,040 --> 00:13:35,040
 may we find help for ourselves,

209
00:13:35,040 --> 00:13:40,040
 may we bring help to each other, to this community,

210
00:13:40,040 --> 00:13:43,040
 and through our practice.

211
00:13:43,040 --> 00:13:49,040
 So, thank you. Have a good night.

