1
00:00:00,000 --> 00:00:04,380
 Okay, this is a question in response to a comment made

2
00:00:04,380 --> 00:00:06,600
 during one of the meditation sessions.

3
00:00:06,600 --> 00:00:10,920
 Someone mentioned that he/she should meditate more than

4
00:00:10,920 --> 00:00:13,000
 reading or listening to Buddhist

5
00:00:13,000 --> 00:00:16,120
 talks because it is also a pleasure.

6
00:00:16,120 --> 00:00:19,310
 Do you suggest us to stop reading or watching Buddhist

7
00:00:19,310 --> 00:00:23,480
 YouTube talks and just meditate for

8
00:00:23,480 --> 00:00:24,480
 now?

9
00:00:24,480 --> 00:00:26,440
 Just meditate for now.

10
00:00:26,440 --> 00:00:31,280
 Well, not all pleasure is wrong.

11
00:00:31,280 --> 00:00:32,920
 In fact, no pleasure is wrong.

12
00:00:32,920 --> 00:00:35,760
 Pleasure isn't the problem.

13
00:00:35,760 --> 00:00:40,240
 Not all pleasure is associated with negative mind states.

14
00:00:40,240 --> 00:00:42,950
 There's a difference between pleasure and attachment or

15
00:00:42,950 --> 00:00:44,360
 pleasure and liking, if you

16
00:00:44,360 --> 00:00:48,520
 want to say.

17
00:00:48,520 --> 00:00:51,540
 There's also a kind of, you could say there's a kind of

18
00:00:51,540 --> 00:00:53,600
 liking that is in a roundabout way

19
00:00:53,600 --> 00:00:55,480
 beneficial.

20
00:00:55,480 --> 00:00:59,110
 So people who like to practice meditation, people who like

21
00:00:59,110 --> 00:01:00,720
 to listen to the Buddhist

22
00:01:00,720 --> 00:01:05,350
 teaching, who are waiting for these sessions because it

23
00:01:05,350 --> 00:01:07,760
 stimulates them and so on, it's

24
00:01:07,760 --> 00:01:10,710
 kind of a negative thing because what they're really

25
00:01:10,710 --> 00:01:12,880
 attached to is the feelings and maybe

26
00:01:12,880 --> 00:01:16,360
 when I joke or when they hear stories.

27
00:01:16,360 --> 00:01:20,000
 There are these Buddhist teachers who give talks and

28
00:01:20,000 --> 00:01:22,640
 everyone likes to hear their stories.

29
00:01:22,640 --> 00:01:25,000
 If you read some of the old Buddhist texts, there's a lot

30
00:01:25,000 --> 00:01:26,280
 of nice stories in there and

31
00:01:26,280 --> 00:01:27,760
 it's stimulating in the mind.

32
00:01:27,760 --> 00:01:29,680
 It makes you feel happy.

33
00:01:29,680 --> 00:01:30,680
 It makes you feel sad.

34
00:01:30,680 --> 00:01:33,400
 It's like when we watch a movie or when we read a novel or

35
00:01:33,400 --> 00:01:35,640
 something, it stimulates you.

36
00:01:35,640 --> 00:01:39,800
 So that's kind of negative, that aspect.

37
00:01:39,800 --> 00:01:44,510
 In a roundabout way, it can be helpful because of the

38
00:01:44,510 --> 00:01:45,760
 content.

39
00:01:45,760 --> 00:01:54,250
 Because actually when you get here, the focus changes or

40
00:01:54,250 --> 00:01:58,120
 the focus is different.

41
00:01:58,120 --> 00:02:00,980
 So the point really is in the content.

42
00:02:00,980 --> 00:02:03,960
 If the content of the teachings is truly the Buddhist

43
00:02:03,960 --> 00:02:06,000
 teaching and you truly are paying

44
00:02:06,000 --> 00:02:13,010
 attention and making effort to understand and are con

45
00:02:13,010 --> 00:02:18,280
forming your mind to the teachings,

46
00:02:18,280 --> 00:02:21,800
 trying to get your head around it, then for sure it's a

47
00:02:21,800 --> 00:02:23,880
 good thing to do all of that.

48
00:02:23,880 --> 00:02:26,800
 You can become enlightened listening to the dhamma.

49
00:02:26,800 --> 00:02:29,390
 I hope that was kind of clear when we were talking about

50
00:02:29,390 --> 00:02:30,560
 the last question.

51
00:02:30,560 --> 00:02:33,170
 As you're listening, just listening to me talk can be a

52
00:02:33,170 --> 00:02:34,760
 meditation practice in itself

53
00:02:34,760 --> 00:02:41,220
 because you're going back again to your moment to moment

54
00:02:41,220 --> 00:02:45,440
 experience and just becoming aware

55
00:02:45,440 --> 00:02:57,000
 of the practical aspects of the teaching.

56
00:02:57,000 --> 00:03:01,270
 But as to the amount, you really need to meditate more than

57
00:03:01,270 --> 00:03:03,640
 you go on YouTube and listen to

58
00:03:03,640 --> 00:03:05,040
 my talks.

59
00:03:05,040 --> 00:03:10,730
 I would say if you've watched all my talks, that's probably

60
00:03:10,730 --> 00:03:12,080
 too much.

61
00:03:12,080 --> 00:03:15,300
 The best thing would be if you could watch a selection of

62
00:03:15,300 --> 00:03:17,040
 them and then go meditate.

63
00:03:17,040 --> 00:03:19,320
 I don't know, it depends.

64
00:03:19,320 --> 00:03:26,280
 But not just talking about my talks.

65
00:03:26,280 --> 00:03:27,440
 You use it in moderation.

66
00:03:27,440 --> 00:03:33,560
 I guess I've only got 300 and some videos if it was just

67
00:03:33,560 --> 00:03:34,480
 mine.

68
00:03:34,480 --> 00:03:37,400
 One a day you've got a whole year's worth or two a day or

69
00:03:37,400 --> 00:03:38,280
 whatever.

70
00:03:38,280 --> 00:03:42,580
 There are teachings from all sorts of different traditions,

71
00:03:42,580 --> 00:03:44,560
 all different teachers.

72
00:03:44,560 --> 00:03:48,240
 It has to be in moderation and I think there are people out

73
00:03:48,240 --> 00:03:48,920
 there.

74
00:03:48,920 --> 00:03:56,110
 This is an endemic, this is a problem in Buddhism that

75
00:03:56,110 --> 00:04:01,200
 people become intellectual Buddhists and

76
00:04:01,200 --> 00:04:07,050
 do a lot of studying and sometimes a lot of talking,

77
00:04:07,050 --> 00:04:12,880
 discussing, arguing, debating, thinking.

78
00:04:12,880 --> 00:04:14,970
 If you watched my talk that the Sri Lankan people really

79
00:04:14,970 --> 00:04:16,240
 like this talk, the Sri Lankan

80
00:04:16,240 --> 00:04:19,630
 meditators who I've talked to really like this one about, I

81
00:04:19,630 --> 00:04:20,440
 really like it too.

82
00:04:20,440 --> 00:04:21,640
 I give this talk often.

83
00:04:21,640 --> 00:04:23,520
 It's not my talk actually.

84
00:04:23,520 --> 00:04:26,140
 First of all it's based on the sutta and second of all it's

85
00:04:26,140 --> 00:04:28,680
 a talk my teacher always gives.

86
00:04:28,680 --> 00:04:31,920
 That's why I picked it up.

87
00:04:31,920 --> 00:04:35,280
 On the five types of people in the world, only one of which

88
00:04:35,280 --> 00:04:36,840
 is said to be someone, or

89
00:04:36,840 --> 00:04:40,840
 five types of Buddhists, only one of which is said to be

90
00:04:40,840 --> 00:04:43,280
 someone who lives by the Dhamma.

91
00:04:43,280 --> 00:04:46,580
 So I think the title is called "One Who Lives by the Dhamma

92
00:04:46,580 --> 00:04:48,280
" or something like that.

93
00:04:48,280 --> 00:04:52,920
 The Pali is Dhamma-rihari.

94
00:04:52,920 --> 00:04:55,270
 And so a person who just studies the Buddhist teaching,

95
00:04:55,270 --> 00:04:57,360
 this isn't considered to be a Dhamma-rihari.

96
00:04:57,360 --> 00:05:03,670
 A person who studies and then goes and teaches others, this

97
00:05:03,670 --> 00:05:06,520
 isn't a Dhamma-rihari.

98
00:05:06,520 --> 00:05:10,140
 A person who thinks about the teachings, goes and considers

99
00:05:10,140 --> 00:05:11,680
 and reflects and so on, this

100
00:05:11,680 --> 00:05:13,560
 isn't a Dhamma-rihari.

101
00:05:13,560 --> 00:05:17,970
 A person who chants and recites and memorizes the teachings

102
00:05:17,970 --> 00:05:20,640
, this also isn't a Dhamma-rihari.

103
00:05:20,640 --> 00:05:23,020
 But a person who does two things is considered to be

104
00:05:23,020 --> 00:05:24,760
 someone who lives by the Dhamma.

105
00:05:24,760 --> 00:05:28,380
 A person who takes the teachings, and the Buddha uses the

106
00:05:28,380 --> 00:05:30,140
 word studies, a person after

107
00:05:30,140 --> 00:05:33,630
 they study, after they become full of knowledge of the

108
00:05:33,630 --> 00:05:35,160
 Buddha's teaching.

109
00:05:35,160 --> 00:05:40,440
 So he says, he's clear that the knowledge is a good thing.

110
00:05:40,440 --> 00:05:44,610
 Then they go off and practice tranquility meditation, calm

111
00:05:44,610 --> 00:05:46,560
 their mind down and develop

112
00:05:46,560 --> 00:05:47,560
 insight.

113
00:05:47,560 --> 00:05:53,070
 "Uttarintyasa Panyaya Atang Vajjanati" They come to see

114
00:05:53,070 --> 00:05:56,040
 clearly the meaning of that

115
00:05:56,040 --> 00:06:05,530
 knowledge, the meaning or the essence, the realization of

116
00:06:05,530 --> 00:06:10,320
 what is meant by the teachings.

117
00:06:10,320 --> 00:06:12,920
 So we can memorize the teachings and we can logically

118
00:06:12,920 --> 00:06:14,320
 accept them and we hear an "Ichang

119
00:06:14,320 --> 00:06:18,280
 duhkanganata" and permanent suffering non-stop.

120
00:06:18,280 --> 00:06:22,320
 But the actual meaning we get from the practice.

121
00:06:22,320 --> 00:06:34,400
 "Uttarintyasa Panyaya" "Uttarintyasa Panyaya" "Uttarintyasa

122
00:06:34,400 --> 00:06:34,400
 Panyaya"

123
00:06:34,400 --> 00:06:44,400
 "Uttarintyasa Panyaya" "Uttarintyasa Panyaya"

124
00:06:44,400 --> 00:06:49,640
 The meaning that is higher than that knowledge.

125
00:06:49,640 --> 00:06:52,360
 So don't watch too much.

126
00:06:52,360 --> 00:06:55,290
 Don't let yourself be just, as the Buddha said at the end

127
00:06:55,290 --> 00:06:57,000
 of this suta, he said, don't

128
00:06:57,000 --> 00:07:03,580
 be negligent, don't just be a person who studies a lot or

129
00:07:03,580 --> 00:07:07,200
 so on, go and meditate after you've

130
00:07:07,200 --> 00:07:11,200
 studied or based on your studies.

131
00:07:11,200 --> 00:07:13,530
 So if you're not meditating at all then you've got a

132
00:07:13,530 --> 00:07:15,280
 problem with just studying and so on.

133
00:07:15,280 --> 00:07:17,880
 Or if you're only meditating a little bit.

134
00:07:17,880 --> 00:07:21,120
 But the purpose is to meditate.

135
00:07:21,120 --> 00:07:24,280
 Studying is just a road map.

136
00:07:24,280 --> 00:07:27,360
 If you don't read the road map it's hard to find your way.

137
00:07:27,360 --> 00:07:29,730
 But if you just sit there reading the road map it's a

138
00:07:29,730 --> 00:07:30,360
 problem.

