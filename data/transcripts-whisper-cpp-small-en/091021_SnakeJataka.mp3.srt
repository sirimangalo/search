1
00:00:00,000 --> 00:00:09,720
 Next, the story of the Uraga Jataka.

2
00:00:09,720 --> 00:00:13,360
 Man quits his mortal frame, and so on.

3
00:00:13,360 --> 00:00:16,520
 This story, the master, well-dwelling in Jaitavana, told,

4
00:00:16,520 --> 00:00:18,000
 concerning a landowner whose son had

5
00:00:18,000 --> 00:00:19,840
 died.

6
00:00:19,840 --> 00:00:22,800
 The introductory story is just the same as that of the man

7
00:00:22,800 --> 00:00:24,240
 who lost both his wife and

8
00:00:24,240 --> 00:00:25,240
 father.

9
00:00:25,240 --> 00:00:28,240
 Here, too, the master, in the same way, went to the man's

10
00:00:28,240 --> 00:00:29,880
 house, and after saluting him

11
00:00:29,880 --> 00:00:33,990
 as he was seated, asked him, saying, "Pray, sir, are you

12
00:00:33,990 --> 00:00:35,080
 grieving?"

13
00:00:35,080 --> 00:00:37,400
 And on his replying, "Yes, reverend, sir.

14
00:00:37,400 --> 00:00:40,440
 Ever since my son's death, I grieve."

15
00:00:40,440 --> 00:00:46,200
 He said, "Sir, verily, that which is subject to dissolution

16
00:00:46,200 --> 00:00:48,160
 is dissolved, and that which

17
00:00:48,160 --> 00:00:52,090
 is subject to destruction is destroyed, and this happens

18
00:00:52,090 --> 00:00:54,160
 not to one man only, nor in one

19
00:00:54,160 --> 00:00:57,230
 village merely, but in countless spheres and in the three

20
00:00:57,230 --> 00:00:58,480
 modes of existence.

21
00:00:58,480 --> 00:01:02,800
 There is no creature that is not subject to death, nor is

22
00:01:02,800 --> 00:01:05,120
 there any existing thing that

23
00:01:05,120 --> 00:01:11,960
 is capable of abiding in the same condition.

24
00:01:11,960 --> 00:01:16,140
 All beings are subject to death, and all compounds are

25
00:01:16,140 --> 00:01:18,400
 subject to dissolution."

26
00:01:18,400 --> 00:01:21,200
 But sages of old, when they lost a son, said, "That which

27
00:01:21,200 --> 00:01:23,960
 is subject to destruction is destroyed,

28
00:01:23,960 --> 00:01:26,080
 and grieved not."

29
00:01:26,080 --> 00:01:30,750
 And hereupon at the man's request he relayed a story of the

30
00:01:30,750 --> 00:01:31,560
 past.

31
00:01:31,560 --> 00:01:34,330
 Once upon a time when Brahmadatta was reigning in Benares,

32
00:01:34,330 --> 00:01:35,960
 the Bodhisatta was born in a Brahman

33
00:01:35,960 --> 00:01:39,870
 household in a village outside the gates of Benares, and re

34
00:01:39,870 --> 00:01:41,280
aring a family he supported

35
00:01:41,280 --> 00:01:43,480
 them by field labor.

36
00:01:43,480 --> 00:01:46,440
 He had two children, a son and a daughter.

37
00:01:46,440 --> 00:01:49,390
 When the son was grown up, the father brought a wife home

38
00:01:49,390 --> 00:01:51,000
 for him from a family of equal

39
00:01:51,000 --> 00:01:53,560
 rank with his own.

40
00:01:53,560 --> 00:01:57,210
 As with a female servant, they composed a household of six,

41
00:01:57,210 --> 00:01:59,240
 the Bodhisatta and his wife,

42
00:01:59,240 --> 00:02:03,970
 the son and daughter, the daughter-in-law and the female

43
00:02:03,970 --> 00:02:05,040
 servant.

44
00:02:05,040 --> 00:02:07,520
 They lived happily and affectionately together.

45
00:02:07,520 --> 00:02:12,110
 The Bodhisatta thus admonished the other five, "According

46
00:02:12,110 --> 00:02:14,800
 as ye have received, give alms.

47
00:02:14,800 --> 00:02:16,400
 Observe holy days.

48
00:02:16,400 --> 00:02:18,080
 Keep the moral law.

49
00:02:18,080 --> 00:02:19,400
 Dwell on the thought of death.

50
00:02:19,400 --> 00:02:22,160
 Be mindful of your mortal state.

51
00:02:22,160 --> 00:02:26,260
 For in the case of beings like ourselves death is certain,

52
00:02:26,260 --> 00:02:27,680
 life uncertain.

53
00:02:27,680 --> 00:02:30,760
 All existing things are transitory and subject to decay.

54
00:02:30,760 --> 00:02:35,280
 Therefore take heed to your ways day and night."

55
00:02:35,280 --> 00:02:39,920
 They readily accepted his teaching and dwelled earnestly on

56
00:02:39,920 --> 00:02:41,840
 the thought of death.

57
00:02:41,840 --> 00:02:45,410
 Now one day the Bodhisatta went with his son to plow his

58
00:02:45,410 --> 00:02:46,080
 field.

59
00:02:46,080 --> 00:02:50,280
 The son gathered together the rubbish and set fire to it.

60
00:02:50,280 --> 00:02:53,840
 Too far from where he was lived a snake in an anthill.

61
00:02:53,840 --> 00:02:56,720
 The smoke hurt the snake's eyes.

62
00:02:56,720 --> 00:02:59,980
 Coming out of his hole in a rage it thought, "This is all

63
00:02:59,980 --> 00:03:01,920
 due to that fellow, and fastening

64
00:03:01,920 --> 00:03:05,400
 upon him with its four teeth it bit him.

65
00:03:05,400 --> 00:03:08,460
 The youth fell down dead."

66
00:03:08,460 --> 00:03:12,080
 The Bodhisatta, on seeing him fall, left his oxen and came

67
00:03:12,080 --> 00:03:12,760
 to him.

68
00:03:12,760 --> 00:03:15,640
 And finding that he was dead he took him up and laid him at

69
00:03:15,640 --> 00:03:17,280
 the foot of a certain tree,

70
00:03:17,280 --> 00:03:21,000
 and covering him up with his cloak he neither wept nor

71
00:03:21,000 --> 00:03:22,000
 lamented.

72
00:03:22,000 --> 00:03:25,710
 He said to himself, "That which is subject to dissolution

73
00:03:25,710 --> 00:03:26,980
 is dissolved, and that which

74
00:03:26,980 --> 00:03:29,680
 is subject to death is dead.

75
00:03:29,680 --> 00:03:36,120
 All compound things are transitory and liable to death."

76
00:03:36,120 --> 00:03:40,120
 And recognizing the transitory nature of things he went on

77
00:03:40,120 --> 00:03:41,600
 with his plowing.

78
00:03:41,600 --> 00:03:44,470
 Seeing a neighbor pass close by the field he asked, "Friend

79
00:03:44,470 --> 00:03:45,820
, are you going home?"

80
00:03:45,820 --> 00:03:49,900
 And on his answering, "Yes," the Bodhisatta said, "Please

81
00:03:49,900 --> 00:03:51,800
 then, go to our house and say

82
00:03:51,800 --> 00:03:55,560
 to the mistress, 'You are not today as formerly to bring

83
00:03:55,560 --> 00:03:57,740
 food for two, but to bring it for

84
00:03:57,740 --> 00:04:00,160
 only one.'

85
00:04:00,160 --> 00:04:03,670
 And hitherto the female slave came alone and brought the

86
00:04:03,670 --> 00:04:05,480
 food, but today all four of

87
00:04:05,480 --> 00:04:08,040
 you are to put on clean garments and to come with perfumes

88
00:04:08,040 --> 00:04:09,480
 and flowers in your hands."

89
00:04:09,480 --> 00:04:15,060
 "All right," he said, and went and spoke those very words

90
00:04:15,060 --> 00:04:17,540
 to the Brahmin's wife.

91
00:04:17,540 --> 00:04:20,400
 She asked, "By whom, sir, was this message given?"

92
00:04:20,400 --> 00:04:24,680
 "By the Brahmin lady," he replied.

93
00:04:24,680 --> 00:04:29,490
 Then she understood that her son was dead, but she did not

94
00:04:29,490 --> 00:04:31,360
 so much as tremble.

95
00:04:31,360 --> 00:04:33,710
 Thus showing perfect self-control and wearing white

96
00:04:33,710 --> 00:04:35,600
 garments, and with perfumes and flowers

97
00:04:35,600 --> 00:04:38,520
 in her hand she bathed them bring food and accompanied the

98
00:04:38,520 --> 00:04:39,920
 other members of the family

99
00:04:39,920 --> 00:04:42,080
 to the field.

100
00:04:42,080 --> 00:04:46,540
 But no one of them all either shed a tear or made lament

101
00:04:46,540 --> 00:04:47,440
ation.

102
00:04:47,440 --> 00:04:51,210
 The Bodhisatta was still sitting in the shade where the

103
00:04:51,210 --> 00:04:53,120
 youth lay, ate his food, and when

104
00:04:53,120 --> 00:04:56,440
 the meal was finished they all took up firewood, and

105
00:04:56,440 --> 00:04:58,120
 lifting the body on to the funeral par

106
00:04:58,120 --> 00:05:01,950
 they made offerings of perfume and flowers, and then set

107
00:05:01,950 --> 00:05:02,860
 fire to it.

108
00:05:02,860 --> 00:05:07,040
 But not a single tear was shed by any one.

109
00:05:07,040 --> 00:05:10,600
 All were dwelling on the thought of death.

110
00:05:10,600 --> 00:05:14,560
 Such was the efficacy of their virtue that the throne of

111
00:05:14,560 --> 00:05:17,280
 Sakka, the king of the angels,

112
00:05:17,280 --> 00:05:21,680
 manifested signs of heat, as it often did when someone on

113
00:05:21,680 --> 00:05:23,880
 earth did a virtuous act.

114
00:05:23,880 --> 00:05:27,090
 Sakka said, "Who, I wonder, is anxious to bring me down

115
00:05:27,090 --> 00:05:28,880
 from my throne," meaning who

116
00:05:28,880 --> 00:05:32,540
 will take his place, because to be born as Sakka, the king

117
00:05:32,540 --> 00:05:34,200
 of the angels, one has to

118
00:05:34,200 --> 00:05:36,200
 do virtue.

119
00:05:36,200 --> 00:05:41,520
 And thus this throne becomes hot because it no longer is

120
00:05:41,520 --> 00:05:43,760
 deserved to sink solely by Sakka

121
00:05:43,760 --> 00:05:45,720
 himself.

122
00:05:45,720 --> 00:05:48,110
 And on reflection he discovered that the heat was due to

123
00:05:48,110 --> 00:05:49,480
 the force of virtue existing in

124
00:05:49,480 --> 00:05:51,120
 these people.

125
00:05:51,120 --> 00:05:54,280
 And being highly pleased he said, "I must go to them in

126
00:05:54,280 --> 00:05:56,220
 utter a loud cry of exaltation,

127
00:05:56,220 --> 00:06:00,100
 like the roaring of a lion, and immediately go afterwards

128
00:06:00,100 --> 00:06:01,960
 to fill their dwelling places

129
00:06:01,960 --> 00:06:05,000
 with the seven treasures."

130
00:06:05,000 --> 00:06:07,540
 And going there in haste he stood by the side of the

131
00:06:07,540 --> 00:06:09,400
 funeral pyre and said, "What are you

132
00:06:09,400 --> 00:06:10,400
 doing?"

133
00:06:10,400 --> 00:06:13,680
 "We are burning the body of a man, my lord."

134
00:06:13,680 --> 00:06:17,480
 "It is no man that you are burning.

135
00:06:17,480 --> 00:06:20,170
 He said he thinks you are roasting the flesh of some beast

136
00:06:20,170 --> 00:06:21,400
 that you have slain."

137
00:06:21,400 --> 00:06:23,560
 "Not so, my lord," they said.

138
00:06:23,560 --> 00:06:28,320
 "It is merely the body of a man that we are burning."

139
00:06:28,320 --> 00:06:32,120
 Then he said, "It must have been some enemy."

140
00:06:32,120 --> 00:06:35,950
 The bodhisattva replied, "It is our own true son, and no

141
00:06:35,950 --> 00:06:36,760
 enemy."

142
00:06:36,760 --> 00:06:40,480
 "Then he could not have been a very dear as a son to you."

143
00:06:40,480 --> 00:06:43,280
 "He was very dear, my lord."

144
00:06:43,280 --> 00:06:47,400
 "Then why do you not weep?"

145
00:06:47,400 --> 00:06:51,940
 Then the bodhisattva to explain the reason why he did not

146
00:06:51,940 --> 00:06:54,320
 weep under the first stanza.

147
00:06:54,320 --> 00:06:59,440
 Man quits his mortal frame when joy in life is past.

148
00:06:59,440 --> 00:07:04,400
 Ian as a snake is wont, its worn-out slaw to cast.

149
00:07:04,400 --> 00:07:07,800
 No friend's lament can touch the ashes of the dead.

150
00:07:07,800 --> 00:07:09,360
 Why should I grieve?

151
00:07:09,360 --> 00:07:14,000
 He fares the way he had to tread.

152
00:07:14,000 --> 00:07:17,360
 "Ska," on hearing the words of the bodhisattva, asked the

153
00:07:17,360 --> 00:07:18,760
 brahmin's wife.

154
00:07:18,760 --> 00:07:24,200
 "How, lady, did the dead man stand to you?"

155
00:07:24,200 --> 00:07:27,540
 I sheltered him ten months in my womb, and suckled him at

156
00:07:27,540 --> 00:07:28,880
 my breast, and directed the

157
00:07:28,880 --> 00:07:31,160
 movements of his hands and feet.

158
00:07:31,160 --> 00:07:35,160
 And he was my grown-up son, my lord.

159
00:07:35,160 --> 00:07:38,120
 Granted lady that a father from the nature of a man may not

160
00:07:38,120 --> 00:07:38,640
 weep.

161
00:07:38,640 --> 00:07:40,660
 A mother's heart surely is tender.

162
00:07:40,660 --> 00:07:44,040
 Why then do you not weep?"

163
00:07:44,040 --> 00:07:48,780
 And to explain why she did not weep, she uttered a couple

164
00:07:48,780 --> 00:07:50,720
 of stanzas as well.

165
00:07:50,720 --> 00:07:55,560
 Uncalled he hither came, unbidden soon to go.

166
00:07:55,560 --> 00:08:00,720
 Ian as he came he went, what cause is here for woe?

167
00:08:00,720 --> 00:08:04,420
 No friend's lament can touch the ashes of the dead.

168
00:08:04,420 --> 00:08:05,420
 Why should I grieve?

169
00:08:05,420 --> 00:08:10,160
 He fares the way he had to tread.

170
00:08:10,160 --> 00:08:13,200
 On hearing the words of the brahmin's wife, Saka asked the

171
00:08:13,200 --> 00:08:15,600
 sister, "Lady, what was the

172
00:08:15,600 --> 00:08:17,840
 dead man to you?"

173
00:08:17,840 --> 00:08:19,800
 He was my brother, my lord.

174
00:08:19,800 --> 00:08:23,000
 "Lady, sisters surely are loving towards their brothers.

175
00:08:23,000 --> 00:08:25,640
 Why do you not weep?"

176
00:08:25,640 --> 00:08:28,020
 But she, to explain the reason why she did not weep,

177
00:08:28,020 --> 00:08:29,540
 repeated a couple of stanzas as

178
00:08:29,540 --> 00:08:30,540
 well.

179
00:08:30,540 --> 00:08:36,560
 "Though I should fast and weep, how would it profit me?

180
00:08:36,560 --> 00:08:40,680
 My kitten-kin, alas, would more unhappy be.

181
00:08:40,680 --> 00:08:43,920
 No friend's lament can touch the ashes of the dead.

182
00:08:43,920 --> 00:08:44,920
 Why should I grieve?

183
00:08:44,920 --> 00:08:50,680
 He fares the way he had to tread."

184
00:08:50,680 --> 00:08:54,730
 Saka on hearing the words of the sister asked his wife, the

185
00:08:54,730 --> 00:08:56,760
 dead man's wife, "Lady, what

186
00:08:56,760 --> 00:08:58,240
 was he to you?"

187
00:08:58,240 --> 00:09:01,800
 He was my husband, my lord.

188
00:09:01,800 --> 00:09:05,770
 Then surely when a husband dies his widows are helpless,

189
00:09:05,770 --> 00:09:07,360
 why do you not weep?

190
00:09:07,360 --> 00:09:11,340
 But she, to explain the reason why she did not weep,

191
00:09:11,340 --> 00:09:14,000
 uttered two stanzas of her own.

192
00:09:14,000 --> 00:09:18,200
 As children cry in vain to grasp the moon above, so mortals

193
00:09:18,200 --> 00:09:20,240
 idly mourn the loss of those

194
00:09:20,240 --> 00:09:22,400
 they love.

195
00:09:22,400 --> 00:09:24,800
 No friend's lament can touch the ashes of the dead.

196
00:09:24,800 --> 00:09:25,800
 Why should I grieve?

197
00:09:25,800 --> 00:09:28,800
 He fares the way he had to tread.

198
00:09:28,800 --> 00:09:32,800
 Saka, on hearing the words of the wife, asked the handmaid,

199
00:09:32,800 --> 00:09:35,240
 saying, "Woman, what was he to

200
00:09:35,240 --> 00:09:37,080
 you?"

201
00:09:37,080 --> 00:09:40,080
 He was my master, my lord.

202
00:09:40,080 --> 00:09:42,980
 No doubt you must have been abused and beaten and oppressed

203
00:09:42,980 --> 00:09:44,760
 by him, and therefore, thinking

204
00:09:44,760 --> 00:09:48,800
 he is happily dead, you weep not.

205
00:09:48,800 --> 00:09:52,860
 Speak not, so, my lord, that this does not suit his case.

206
00:09:52,860 --> 00:09:56,080
 My young master was full of long-suffering and love and

207
00:09:56,080 --> 00:09:58,020
 pity for me, and was as a foster

208
00:09:58,020 --> 00:10:00,720
 child to me.

209
00:10:00,720 --> 00:10:03,680
 Then why do you not weep?

210
00:10:03,680 --> 00:10:08,290
 And she, to explain why she did not weep, uttered a couple

211
00:10:08,290 --> 00:10:10,160
 of stanzas herself.

212
00:10:10,160 --> 00:10:14,160
 A broken pot of earth, ah, who can peace again?

213
00:10:14,160 --> 00:10:19,440
 So too to mourn the dead is not but labor vain.

214
00:10:19,440 --> 00:10:21,880
 No friend's lament can touch the ashes of the dead.

215
00:10:21,880 --> 00:10:22,880
 Why should I grieve?

216
00:10:22,880 --> 00:10:26,800
 He fares the way he had to tread.

217
00:10:26,800 --> 00:10:29,420
 Saka, on hearing what they all had to say, was greatly

218
00:10:29,420 --> 00:10:31,520
 pleased, and said, "Ye have carefully

219
00:10:31,520 --> 00:10:33,840
 dwelt on the thought of death.

220
00:10:33,840 --> 00:10:36,920
 Henceforth ye are not to labor with your own hands.

221
00:10:36,920 --> 00:10:39,880
 I am Saka, king of heaven.

222
00:10:39,880 --> 00:10:44,010
 I will create the seven treasures in countless abundance in

223
00:10:44,010 --> 00:10:45,080
 your house.

224
00:10:45,080 --> 00:10:48,390
 Ye are to give alms, to keep the moral law, to observe holy

225
00:10:48,390 --> 00:10:49,880
 days, and to take heed to

226
00:10:49,880 --> 00:10:51,680
 your ways."

227
00:10:51,680 --> 00:10:54,000
 And thus admonishing them, he filled their house with

228
00:10:54,000 --> 00:10:55,520
 countless wealth, and so parted

229
00:10:55,520 --> 00:10:58,160
 from them.

230
00:10:58,160 --> 00:11:01,270
 The master, having finished his exposition of the truth,

231
00:11:01,270 --> 00:11:03,160
 declared the four noble truths

232
00:11:03,160 --> 00:11:05,480
 and identified the birth.

233
00:11:05,480 --> 00:11:08,060
 At the conclusion of the truths, the landowner attained the

234
00:11:08,060 --> 00:11:10,120
 fruit of the first path, which

235
00:11:10,120 --> 00:11:14,600
 is the path of Sotapanna, means he has seen the truth.

236
00:11:14,600 --> 00:11:19,100
 At that time Kudjutara was the female slave, Upala Vana was

237
00:11:19,100 --> 00:11:21,440
 the daughter, Rahula was the

238
00:11:21,440 --> 00:11:27,560
 son, Kama was the mother, and I myself was the brahmin.

239
00:11:27,560 --> 00:11:28,560
 The end.

240
00:11:28,560 --> 00:11:30,080
 So that's all for today.

241
00:11:30,080 --> 00:11:32,400
 Thank you for coming to listen.

