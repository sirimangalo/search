1
00:00:00,000 --> 00:00:11,350
 Hello everyone, we're now broadcasting live January 3rd

2
00:00:11,350 --> 00:00:15,720
 2016.

3
00:00:15,720 --> 00:00:18,560
 So a lot of travel recently.

4
00:00:18,560 --> 00:00:22,140
 Yesterday I went to Stony Creek, today I went to St. Cathar

5
00:00:22,140 --> 00:00:22,720
ines.

6
00:00:22,720 --> 00:00:28,120
 Tomorrow I'm going all the way to Toronto.

7
00:00:28,120 --> 00:00:32,800
 And there's rumors that I might even be going to Taiwan in

8
00:00:32,800 --> 00:00:35,120
 February and Thailand.

9
00:00:35,120 --> 00:00:39,120
 That's our, that's the new buzz.

10
00:00:39,120 --> 00:00:41,120
 Let's see.

11
00:00:41,120 --> 00:00:44,240
 If you imagine, I'll make a week trip to Thailand.

12
00:00:44,240 --> 00:00:46,360
 It'll be like a dream.

13
00:00:46,360 --> 00:00:50,920
 I'll get over jet lag when I get back.

14
00:00:50,920 --> 00:00:55,910
 I'll just become accustomed to time change and I'll be back

15
00:00:55,910 --> 00:00:57,120
 in Canada.

16
00:00:57,120 --> 00:01:00,120
 Kind of ridiculous really.

17
00:01:00,120 --> 00:01:07,220
 But it would be nice to go and talk to Ajahn Tong about our

18
00:01:07,220 --> 00:01:10,120
 new monastery.

19
00:01:10,120 --> 00:01:16,120
 And that's about it.

20
00:01:16,120 --> 00:01:20,120
 My brother's in Taiwan so I could visit him.

21
00:01:20,120 --> 00:01:27,120
 And there's a conference in Taiwan.

22
00:01:27,120 --> 00:01:30,860
 A conference, and I found out what it is, it's a conference

23
00:01:30,860 --> 00:01:33,120
 on religious freedom.

24
00:01:33,120 --> 00:01:37,460
 Because the Taiwanese are concerned about their religious

25
00:01:37,460 --> 00:01:38,120
 freedom.

26
00:01:38,120 --> 00:01:42,120
 Or about religious freedom in China for example.

27
00:01:42,120 --> 00:01:46,090
 That's a big reason why they don't want to become a part of

28
00:01:46,090 --> 00:01:47,120
 China I think.

29
00:01:47,120 --> 00:01:50,980
 Because China has problems with religious freedom as I

30
00:01:50,980 --> 00:01:52,120
 understand.

31
00:01:52,120 --> 00:02:00,120
 Anyway, it's a thing.

32
00:02:00,120 --> 00:02:03,120
 What language is the conference in Manbe?

33
00:02:03,120 --> 00:02:06,120
 I don't know.

34
00:02:06,120 --> 00:02:10,290
 I'll just be tagging along with a head monk from Stony

35
00:02:10,290 --> 00:02:11,120
 Creek.

36
00:02:11,120 --> 00:02:14,120
 He's the one who got invited, not me.

37
00:02:14,120 --> 00:02:20,120
 He's invited me to go with him.

38
00:02:20,120 --> 00:02:22,120
 I mean they're very concerned.

39
00:02:22,120 --> 00:02:28,790
 The Khmer Krom people are, they'd like to do anything they

40
00:02:28,790 --> 00:02:31,810
 can to preserve whatever religious freedom is left in South

41
00:02:31,810 --> 00:02:33,120
 Vietnam.

42
00:02:33,120 --> 00:02:36,510
 Because Vietnam doesn't have a very good record of

43
00:02:36,510 --> 00:02:42,330
 religious freedom. Well, they've practically tried to wipe

44
00:02:42,330 --> 00:02:55,120
 out Theravada Buddhism in Khmer Krom.

45
00:02:55,120 --> 00:02:58,120
 So that's that.

46
00:02:58,120 --> 00:03:02,120
 Anything else? No other big announcements?

47
00:03:02,120 --> 00:03:08,120
 We've got our meetings today.

48
00:03:08,120 --> 00:03:10,120
 Do we have questions?

49
00:03:10,120 --> 00:03:12,120
 Me too.

50
00:03:12,120 --> 00:03:14,120
 What are the four great elements?

51
00:03:14,120 --> 00:03:17,430
 I know they refer to our direct experience and not

52
00:03:17,430 --> 00:03:19,120
 something like Adam's.

53
00:03:19,120 --> 00:03:24,890
 But I don't understand what exactly each element is

54
00:03:24,890 --> 00:03:27,120
 experienced as.

55
00:03:27,120 --> 00:03:31,120
 The water element isn't experienced, but it's inferred

56
00:03:31,120 --> 00:03:37,120
 based on how the other three present themselves.

57
00:03:37,120 --> 00:03:43,120
 So the air element is stiffness.

58
00:03:43,120 --> 00:03:45,120
 The earth element is hardness.

59
00:03:45,120 --> 00:03:47,120
 The fire element is heat.

60
00:03:47,120 --> 00:03:49,120
 The water element is cohesion.

61
00:03:49,120 --> 00:03:52,590
 But for example, on the roof of your mouth, when your

62
00:03:52,590 --> 00:03:54,120
 tongue sticks to the roof of your mouth,

63
00:03:54,120 --> 00:03:57,120
 that's because of the water element.

64
00:03:57,120 --> 00:04:00,120
 But you experience it still has tension.

65
00:04:00,120 --> 00:04:02,120
 But the tension is there because of the cohesion.

66
00:04:02,120 --> 00:04:05,310
 So when your hands stick together, you pull them apart, you

67
00:04:05,310 --> 00:04:06,120
 feel the tension.

68
00:04:06,120 --> 00:04:10,640
 But the tension arises not because you're pushing, but

69
00:04:10,640 --> 00:04:15,120
 because of the stickiness.

70
00:04:15,120 --> 00:04:20,730
 We're actually talking about these elements in the Visuddhi

71
00:04:20,730 --> 00:04:22,120
 Maga class.

72
00:04:22,120 --> 00:04:25,540
 In the Visuddhi Maga you see, you're actually talking about

73
00:04:25,540 --> 00:04:27,120
 blood, for example.

74
00:04:27,120 --> 00:04:29,120
 Blood is an example of the water element.

75
00:04:29,120 --> 00:04:33,120
 But it's really not.

76
00:04:33,120 --> 00:04:38,120
 I'm not the expert on all of this, actually.

77
00:04:38,120 --> 00:04:43,120
 Especially the water element and exactly how it works.

78
00:04:43,120 --> 00:04:49,120
 These are conceptual water.

79
00:04:49,120 --> 00:04:54,770
 I think they called it mobile water and it was mobile water

80
00:04:54,770 --> 00:04:57,120
 and something else.

81
00:04:57,120 --> 00:05:02,030
 Maybe it was mobile blood. I'm not sure. Something was

82
00:05:02,030 --> 00:05:03,120
 mobile.

83
00:05:03,120 --> 00:05:05,800
 If I assign 10 minutes for walking and 10 minutes for

84
00:05:05,800 --> 00:05:06,120
 sitting,

85
00:05:06,120 --> 00:05:09,490
 will the timer sound go off after 10 minutes or only at the

86
00:05:09,490 --> 00:05:13,120
 end of the 20 minute period?

87
00:05:13,120 --> 00:05:18,120
 It should go off after both if it's working properly.

88
00:05:18,120 --> 00:05:20,900
 For some people the timer doesn't work, but it's always

89
00:05:20,900 --> 00:05:27,120
 worked on my phone and my PC.

90
00:05:27,120 --> 00:05:32,120
 It depends.

91
00:05:32,120 --> 00:05:37,700
 Is there a way to upload a profile picture without using

92
00:05:37,700 --> 00:05:39,120
 graviton?

93
00:05:39,120 --> 00:05:44,290
 It should be. You can't upload, but you have to put it

94
00:05:44,290 --> 00:05:48,120
 somewhere on the internet and link to it.

95
00:05:48,120 --> 00:05:53,120
 We don't have a place for you to upload it.

96
00:05:53,120 --> 00:05:57,400
 I think I uploaded mine from PhotoBucket, but it came out

97
00:05:57,400 --> 00:05:58,120
 wrong.

98
00:05:58,120 --> 00:06:01,120
 If you notice it's now been fixed.

99
00:06:01,120 --> 00:06:03,120
 Did you fix that, Anthony?

100
00:06:03,120 --> 00:06:05,120
 I uploaded a version to our website.

101
00:06:05,120 --> 00:06:09,930
 Thank you. I was meant to do that. If you upload from Photo

102
00:06:09,930 --> 00:06:13,120
Bucket, your photo comes out oval.

103
00:06:13,120 --> 00:06:18,120
 No, that's not it. You uploaded a non-square photo.

104
00:06:18,120 --> 00:06:22,370
 It says quite clearly on the instructions you need a square

105
00:06:22,370 --> 00:06:23,120
 photo.

106
00:06:23,120 --> 00:06:26,120
 If you don't have a square photo, it comes out oval.

107
00:06:26,120 --> 00:06:30,120
 You're supposed to crop it as a square then?

108
00:06:30,120 --> 00:06:37,020
 Yes. You need a square photo. Otherwise, I think I could

109
00:06:37,020 --> 00:06:38,120
 probably figure it out a way, but I'm not clever enough to.

110
00:06:38,120 --> 00:06:42,830
 I probably could, but I don't have the time to figure out

111
00:06:42,830 --> 00:06:44,120
 how to work.

112
00:06:44,120 --> 00:06:48,350
 It's just so much easier if the photo is square to make a

113
00:06:48,350 --> 00:06:49,120
 circle.

114
00:06:49,120 --> 00:06:54,120
 Good to know.

115
00:06:54,120 --> 00:06:57,440
 I've been practicing kung fu as a form of meditation for

116
00:06:57,440 --> 00:06:59,860
 years now, and I'd like to know your opinion on Shaolin

117
00:06:59,860 --> 00:07:02,120
 monks and kung fu meditation.

118
00:07:02,120 --> 00:07:05,570
 Maybe talking about some of the clear differences between

119
00:07:05,570 --> 00:07:08,120
 this kind of practice and sports or fitness,

120
00:07:08,120 --> 00:07:10,860
 which ultimate goal is just to do physical exercise in

121
00:07:10,860 --> 00:07:14,120
 order to have a good body or be more attractive to others.

122
00:07:14,120 --> 00:07:16,120
 Thanks and advance.

123
00:07:16,120 --> 00:07:19,160
 Yeah, I don't have anything to say about Shaolin. It's not

124
00:07:19,160 --> 00:07:21,120
 what I teach.

125
00:07:21,120 --> 00:07:33,370
 So I kind of made a rule of not answering questions about

126
00:07:33,370 --> 00:07:40,120
 other people's practices.

127
00:07:40,120 --> 00:07:45,120
 Do the weekly commitments tick over individually from when

128
00:07:45,120 --> 00:07:45,120
 the button is pushed?

129
00:07:45,120 --> 00:07:51,120
 Or is there a time when they all refresh each week?

130
00:07:51,120 --> 00:07:54,120
 If it's a weekly thing, it just gives the last seven days.

131
00:07:54,120 --> 00:07:57,120
 If it's a monthly thing, it gives the last month.

132
00:07:57,120 --> 00:08:03,120
 It's a daily thing. I think it's just the last 24 hours.

133
00:08:03,120 --> 00:08:08,240
 I think I can't remember actually. Daily might be a bit

134
00:08:08,240 --> 00:08:09,120
 different.

135
00:08:09,120 --> 00:08:13,780
 Yeah, that sounds right because it's not like there's one

136
00:08:13,780 --> 00:08:17,120
 point in the week where it all gets refreshed.

137
00:08:17,120 --> 00:08:19,990
 But I can't remember if the daily maybe there's an

138
00:08:19,990 --> 00:08:27,620
 exception for that because daily you kind of want to do it

139
00:08:27,620 --> 00:08:30,120
 for that day, right?

140
00:08:30,120 --> 00:08:39,120
 Well, with the commitments, there's kind of a neat feature.

141
00:08:39,120 --> 00:08:39,120
 I've noticed if you're good with your commitment, it's

142
00:08:39,120 --> 00:08:39,120
 green.

143
00:08:39,120 --> 00:08:42,960
 If you're partly done with your commitment, it's sort of

144
00:08:42,960 --> 00:08:44,120
 yellowish green.

145
00:08:44,120 --> 00:08:46,880
 And if you haven't done your commitment at all by the end

146
00:08:46,880 --> 00:08:48,120
 of the day, it's red.

147
00:08:48,120 --> 00:08:52,210
 So it does seem to kind of let you know your progress if

148
00:08:52,210 --> 00:08:56,360
 you've committed for so many minutes and you've only done

149
00:08:56,360 --> 00:08:57,120
 half of that.

150
00:08:57,120 --> 00:09:02,810
 It will be like a yellowish. If you hover over it, it tells

151
00:09:02,810 --> 00:09:03,120
 you.

152
00:09:03,120 --> 00:09:09,120
 Tells you the percentage.

153
00:09:09,120 --> 00:09:14,880
 Someone criticized that because they're 10. Did you know

154
00:09:14,880 --> 00:09:18,630
 that 10% of people in the world are colorblind and can't

155
00:09:18,630 --> 00:09:21,120
 tell the difference between red and green?

156
00:09:21,120 --> 00:09:25,790
 So the color scheme is apparently not that. Oh. Yeah,

157
00:09:25,790 --> 00:09:29,780
 shouldn't be red to green, but whatever. I'm not colorblind

158
00:09:29,780 --> 00:09:35,120
. That's not fair, is it?

159
00:09:35,120 --> 00:09:38,060
 Well, if you're colorblind, you can hover over it and see

160
00:09:38,060 --> 00:09:39,120
 the percentages.

161
00:09:39,120 --> 00:09:45,640
 Yeah, I mean, it doesn't change anything. Yeah. Just kind

162
00:09:45,640 --> 00:09:48,120
 of a warning.

163
00:09:48,120 --> 00:09:52,190
 I think the quality of my sitting meditation practice is

164
00:09:52,190 --> 00:09:56,060
 very good, but I wonder if it can be improved during daily

165
00:09:56,060 --> 00:09:57,120
 meditation.

166
00:09:57,120 --> 00:10:00,110
 Is it better to meditate for as long as one is able to in a

167
00:10:00,110 --> 00:10:07,780
 single setting or is it more beneficial to practice several

168
00:10:07,780 --> 00:10:14,120
 shorter meditation sessions each day?

169
00:10:14,120 --> 00:10:18,120
 Well, you should do both walking and sitting.

170
00:10:18,120 --> 00:10:22,120
 So not just single sitting.

171
00:10:22,120 --> 00:10:27,250
 And it's good to make it consistent. So if you practice, if

172
00:10:27,250 --> 00:10:31,120
 you practice, I mean, I would say there's a happy medium.

173
00:10:31,120 --> 00:10:35,400
 If you're practicing like 10 minutes walking, 10 minutes

174
00:10:35,400 --> 00:10:38,120
 sitting, five times a day,

175
00:10:38,120 --> 00:10:40,940
 the benefit to that is that you're continuously mindful

176
00:10:40,940 --> 00:10:44,210
 throughout the day. But the detriment is or the bad side is

177
00:10:44,210 --> 00:10:46,120
 that you're never really testing yourself.

178
00:10:46,120 --> 00:10:50,600
 None of it's really serious meditation. Whereas if you do

179
00:10:50,600 --> 00:10:54,120
 two hours at once and then nothing for the rest of the day,

180
00:10:54,120 --> 00:10:57,120
 then the benefit is you've really pushed yourself.

181
00:10:57,120 --> 00:11:00,780
 But the problem is that the rest of the time you're not

182
00:11:00,780 --> 00:11:03,120
 doing any form of meditation.

183
00:11:03,120 --> 00:11:06,830
 So sort of a happy medium is to do two sessions a day,

184
00:11:06,830 --> 00:11:10,460
 maybe three if you have time, maybe two long ones, one in

185
00:11:10,460 --> 00:11:12,120
 the morning, one in the evening,

186
00:11:12,120 --> 00:11:15,930
 and then during the day, during work or whatever, take time

187
00:11:15,930 --> 00:11:17,120
 to do a shorter one.

188
00:11:17,120 --> 00:11:20,960
 Because there's two aspects here. One is pushing yourself

189
00:11:20,960 --> 00:11:27,090
 to really testing yourself and forcing the difficult

190
00:11:27,090 --> 00:11:30,960
 conditions to come up and challenging yourself to be

191
00:11:30,960 --> 00:11:32,120
 patient with me.

192
00:11:32,120 --> 00:11:38,120
 But the other is to make it consistent throughout the day.

193
00:11:38,120 --> 00:11:43,690
 So you have to keep them both in mind. Certainly doing once

194
00:11:43,690 --> 00:11:47,120
 a day is not really enough.

195
00:11:47,120 --> 00:11:53,180
 But doing two short of sessions is also not all that

196
00:11:53,180 --> 00:12:02,810
 beneficial. So it should be fairly long and a couple of

197
00:12:02,810 --> 00:12:08,120
 times or a few times a day.

198
00:12:08,120 --> 00:12:14,760
 Is too much sitting meditation bad? Can you be addicted to

199
00:12:14,760 --> 00:12:19,120
 it? Is meditation a need or a want? Is meditation a need or

200
00:12:19,120 --> 00:12:19,120
 a want?

201
00:12:19,120 --> 00:12:24,800
 The dude has a yellow question mark. I think I get to pass

202
00:12:24,800 --> 00:12:26,120
 on that one.

203
00:12:26,120 --> 00:12:29,120
 No, he's meditating.

204
00:12:29,120 --> 00:12:32,120
 I don't get to pass on that one.

205
00:12:32,120 --> 00:12:38,860
 No, he's been meditating just not in the last couple of

206
00:12:38,860 --> 00:12:40,120
 hours.

207
00:12:40,120 --> 00:12:44,120
 All right.

208
00:12:44,120 --> 00:12:50,710
 He's Canadian as well, so he gets a pass. We'll give him a

209
00:12:50,710 --> 00:12:52,120
 pass.

210
00:12:52,120 --> 00:12:57,100
 Is too much sitting meditation bad? No. Well, it can be

211
00:12:57,100 --> 00:13:01,120
 harmful to the body, so you should do walking as well.

212
00:13:01,120 --> 00:13:07,120
 Can you be addicted to meditation?

213
00:13:07,120 --> 00:13:11,640
 I don't see why not. I mean, at least intellectually, it

214
00:13:11,640 --> 00:13:13,120
 can be an ego thing.

215
00:13:13,120 --> 00:13:18,330
 I think it'd be hard pressed to lust after meditation, you

216
00:13:18,330 --> 00:13:21,120
 know, in the same way that you lust after food.

217
00:13:21,120 --> 00:13:23,790
 Like you see you're sitting matinee and you're saying, "Oh,

218
00:13:23,790 --> 00:13:27,120
 I think that's unlikely."

219
00:13:27,120 --> 00:13:31,510
 But, you know, I suppose you could conceive of a

220
00:13:31,510 --> 00:13:33,120
 possibility.

221
00:13:33,120 --> 00:13:38,190
 Highly unlikely, though. Is meditation a need or a want? No

222
00:13:38,190 --> 00:13:52,120
, meditation is a practice.

223
00:13:52,120 --> 00:14:00,120
 You know, that will quote up with the questions.

224
00:14:00,120 --> 00:14:04,120
 Lots of meditators today. I guess it's the weekend.

225
00:14:04,120 --> 00:14:08,800
 Let's see, who do we have? We've got somebody from Cambodia

226
00:14:08,800 --> 00:14:13,120
. Too many Americans, as usual.

227
00:14:13,120 --> 00:14:16,120
 You say that like the bad thing.

228
00:14:16,120 --> 00:14:20,370
 Oh, it's a terrible thing. Did you know that like 70% of

229
00:14:20,370 --> 00:14:24,120
 the people who follow me on YouTube are Americans?

230
00:14:24,120 --> 00:14:27,680
 Well, sure. I mean, you speak English and there's a lot of

231
00:14:27,680 --> 00:14:28,120
 us.

232
00:14:28,120 --> 00:14:32,040
 There are a lot of people. Think of all the good you're

233
00:14:32,040 --> 00:14:33,120
 doing.

234
00:14:33,120 --> 00:14:37,640
 It just goes with the job of being Canadian. I have to

235
00:14:37,640 --> 00:14:39,120
 criticize America.

236
00:14:39,120 --> 00:14:44,650
 Look down, my Canadian knows that, you know, lots of

237
00:14:44,650 --> 00:14:46,120
 Americans.

238
00:14:46,120 --> 00:14:51,360
 Hey, power to you. You're putting us the rest of the world

239
00:14:51,360 --> 00:14:54,120
 to shame. All your meditation.

240
00:14:54,120 --> 00:14:56,910
 Americans make really good meditators, actually. I've said

241
00:14:56,910 --> 00:14:59,120
 this before many times.

242
00:14:59,120 --> 00:15:09,120
 They have a certain confidence. They do things through.

243
00:15:09,120 --> 00:15:12,050
 I mean, it's a gross generalization, of course, but you can

244
00:15:12,050 --> 00:15:13,120
 generalize.

245
00:15:13,120 --> 00:15:16,540
 When you've taught around the world, especially teaching in

246
00:15:16,540 --> 00:15:20,130
 Thailand, I would see so many people from so many different

247
00:15:20,130 --> 00:15:21,120
 countries.

248
00:15:21,120 --> 00:15:25,360
 And Americans tend to be strong. They go for it. They

249
00:15:25,360 --> 00:15:27,120
 finish what they set out to do.

250
00:15:27,120 --> 00:15:30,120
 Canadians get halfway through the course and go home

251
00:15:30,120 --> 00:15:34,120
 because, of course, we're known for waffling.

252
00:15:34,120 --> 00:15:39,890
 And it's the truth. Again, a gross generalization, but

253
00:15:39,890 --> 00:15:42,120
 interesting nonetheless.

254
00:15:42,120 --> 00:15:48,270
 Samantha, who is Sri Lankan living in the UK, we have a

255
00:15:48,270 --> 00:15:52,120
 Brazilian, another UK fifth rounder.

256
00:15:52,120 --> 00:15:55,120
 Is that a drinking reference?

257
00:15:55,120 --> 00:15:58,120
 Maybe a boxing reference.

258
00:15:58,120 --> 00:16:02,120
 Maybe a football reference that I don't get.

259
00:16:02,120 --> 00:16:07,160
 Another Canadian. Two people with no country. I feel sorry

260
00:16:07,160 --> 00:16:08,120
 for you.

261
00:16:08,120 --> 00:16:14,660
 No, actually, not having a country can be a very good thing

262
00:16:14,660 --> 00:16:15,120
.

263
00:16:15,120 --> 00:16:18,120
 Countries can be irksome.

264
00:16:18,120 --> 00:16:25,120
 Israel is just no Argentinian.

265
00:16:25,120 --> 00:16:27,120
 Magda from Argentina.

266
00:16:27,120 --> 00:16:31,790
 Well, thanks everyone for joining us. A couple of people

267
00:16:31,790 --> 00:16:33,120
 with hearts beside their names.

268
00:16:33,120 --> 00:16:36,120
 We should all put hearts beside our names.

269
00:16:36,120 --> 00:16:43,120
 Send love to each other. May you all be well and happy.

270
00:16:43,120 --> 00:16:50,120
 There we go. See some hearts.

271
00:16:50,120 --> 00:16:54,120
 All right. Have a good night, everyone.

272
00:16:54,120 --> 00:16:55,120
 You as well, Father.

273
00:16:55,120 --> 00:16:59,280
 Oh, this is what I wanted to say. I think we're going to

274
00:16:59,280 --> 00:17:01,120
 have to switch.

275
00:17:01,120 --> 00:17:06,810
 Tuesdays and Thursdays are going to be Dhammapada from now

276
00:17:06,810 --> 00:17:07,120
 on.

277
00:17:07,120 --> 00:17:16,120
 Because my Mondays and Wednesdays are pretty busy.

278
00:17:16,120 --> 00:17:19,010
 I think Tuesdays and Thursdays. That'll make it more

279
00:17:19,010 --> 00:17:20,120
 sustainable as well.

280
00:17:20,120 --> 00:17:25,550
 You know, it puts less pressure on me to have to research

281
00:17:25,550 --> 00:17:27,120
 twice a week.

282
00:17:27,120 --> 00:17:33,120
 I know I was doing seven before, right?

283
00:17:33,120 --> 00:17:38,120
 What are your thoughts on the Buddhism basics?

284
00:17:38,120 --> 00:17:39,120
 Buddhism what?

285
00:17:39,120 --> 00:17:44,040
 The Buddhism basics. You were thinking of maybe restarting

286
00:17:44,040 --> 00:17:46,120
 the basic Buddhism series.

287
00:17:46,120 --> 00:17:49,120
 Well, let's see.

288
00:17:49,120 --> 00:17:53,750
 You can do one Dhammapada a week and one other thing a week

289
00:17:53,750 --> 00:17:54,120
.

290
00:17:54,120 --> 00:17:58,120
 Or we could do a third one on Saturday would be a good day.

291
00:17:58,120 --> 00:18:03,010
 You do something different on Saturday. Tuesdays, Thursdays

292
00:18:03,010 --> 00:18:06,120
, and then Saturday, something else.

293
00:18:06,120 --> 00:18:15,120
 Weekends probably be good.

294
00:18:15,120 --> 00:18:19,120
 That sounds good.

295
00:18:19,120 --> 00:18:23,120
 Okay. With that, wish you good night.

296
00:18:23,120 --> 00:18:26,120
 Thanks, Robin, for joining me.

297
00:18:26,120 --> 00:18:28,120
 Thank you, Monty.

298
00:18:28,120 --> 00:18:29,120
 Thank you, everyone.

299
00:18:30,120 --> 00:18:31,120
 Thank you.

300
00:18:32,120 --> 00:18:33,120
 Thank you.

