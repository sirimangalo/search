 Next, the story of the Uraga Jataka.
 Man quits his mortal frame, and so on.
 This story, the master, well-dwelling in Jaitavana, told,
 concerning a landowner whose son had
 died.
 The introductory story is just the same as that of the man
 who lost both his wife and
 father.
 Here, too, the master, in the same way, went to the man's
 house, and after saluting him
 as he was seated, asked him, saying, "Pray, sir, are you
 grieving?"
 And on his replying, "Yes, reverend, sir.
 Ever since my son's death, I grieve."
 He said, "Sir, verily, that which is subject to dissolution
 is dissolved, and that which
 is subject to destruction is destroyed, and this happens
 not to one man only, nor in one
 village merely, but in countless spheres and in the three
 modes of existence.
 There is no creature that is not subject to death, nor is
 there any existing thing that
 is capable of abiding in the same condition.
 All beings are subject to death, and all compounds are
 subject to dissolution."
 But sages of old, when they lost a son, said, "That which
 is subject to destruction is destroyed,
 and grieved not."
 And hereupon at the man's request he relayed a story of the
 past.
 Once upon a time when Brahmadatta was reigning in Benares,
 the Bodhisatta was born in a Brahman
 household in a village outside the gates of Benares, and re
aring a family he supported
 them by field labor.
 He had two children, a son and a daughter.
 When the son was grown up, the father brought a wife home
 for him from a family of equal
 rank with his own.
 As with a female servant, they composed a household of six,
 the Bodhisatta and his wife,
 the son and daughter, the daughter-in-law and the female
 servant.
 They lived happily and affectionately together.
 The Bodhisatta thus admonished the other five, "According
 as ye have received, give alms.
 Observe holy days.
 Keep the moral law.
 Dwell on the thought of death.
 Be mindful of your mortal state.
 For in the case of beings like ourselves death is certain,
 life uncertain.
 All existing things are transitory and subject to decay.
 Therefore take heed to your ways day and night."
 They readily accepted his teaching and dwelled earnestly on
 the thought of death.
 Now one day the Bodhisatta went with his son to plow his
 field.
 The son gathered together the rubbish and set fire to it.
 Too far from where he was lived a snake in an anthill.
 The smoke hurt the snake's eyes.
 Coming out of his hole in a rage it thought, "This is all
 due to that fellow, and fastening
 upon him with its four teeth it bit him.
 The youth fell down dead."
 The Bodhisatta, on seeing him fall, left his oxen and came
 to him.
 And finding that he was dead he took him up and laid him at
 the foot of a certain tree,
 and covering him up with his cloak he neither wept nor
 lamented.
 He said to himself, "That which is subject to dissolution
 is dissolved, and that which
 is subject to death is dead.
 All compound things are transitory and liable to death."
 And recognizing the transitory nature of things he went on
 with his plowing.
 Seeing a neighbor pass close by the field he asked, "Friend
, are you going home?"
 And on his answering, "Yes," the Bodhisatta said, "Please
 then, go to our house and say
 to the mistress, 'You are not today as formerly to bring
 food for two, but to bring it for
 only one.'
 And hitherto the female slave came alone and brought the
 food, but today all four of
 you are to put on clean garments and to come with perfumes
 and flowers in your hands."
 "All right," he said, and went and spoke those very words
 to the Brahmin's wife.
 She asked, "By whom, sir, was this message given?"
 "By the Brahmin lady," he replied.
 Then she understood that her son was dead, but she did not
 so much as tremble.
 Thus showing perfect self-control and wearing white
 garments, and with perfumes and flowers
 in her hand she bathed them bring food and accompanied the
 other members of the family
 to the field.
 But no one of them all either shed a tear or made lament
ation.
 The Bodhisatta was still sitting in the shade where the
 youth lay, ate his food, and when
 the meal was finished they all took up firewood, and
 lifting the body on to the funeral par
 they made offerings of perfume and flowers, and then set
 fire to it.
 But not a single tear was shed by any one.
 All were dwelling on the thought of death.
 Such was the efficacy of their virtue that the throne of
 Sakka, the king of the angels,
 manifested signs of heat, as it often did when someone on
 earth did a virtuous act.
 Sakka said, "Who, I wonder, is anxious to bring me down
 from my throne," meaning who
 will take his place, because to be born as Sakka, the king
 of the angels, one has to
 do virtue.
 And thus this throne becomes hot because it no longer is
 deserved to sink solely by Sakka
 himself.
 And on reflection he discovered that the heat was due to
 the force of virtue existing in
 these people.
 And being highly pleased he said, "I must go to them in
 utter a loud cry of exaltation,
 like the roaring of a lion, and immediately go afterwards
 to fill their dwelling places
 with the seven treasures."
 And going there in haste he stood by the side of the
 funeral pyre and said, "What are you
 doing?"
 "We are burning the body of a man, my lord."
 "It is no man that you are burning.
 He said he thinks you are roasting the flesh of some beast
 that you have slain."
 "Not so, my lord," they said.
 "It is merely the body of a man that we are burning."
 Then he said, "It must have been some enemy."
 The bodhisattva replied, "It is our own true son, and no
 enemy."
 "Then he could not have been a very dear as a son to you."
 "He was very dear, my lord."
 "Then why do you not weep?"
 Then the bodhisattva to explain the reason why he did not
 weep under the first stanza.
 Man quits his mortal frame when joy in life is past.
 Ian as a snake is wont, its worn-out slaw to cast.
 No friend's lament can touch the ashes of the dead.
 Why should I grieve?
 He fares the way he had to tread.
 "Ska," on hearing the words of the bodhisattva, asked the
 brahmin's wife.
 "How, lady, did the dead man stand to you?"
 I sheltered him ten months in my womb, and suckled him at
 my breast, and directed the
 movements of his hands and feet.
 And he was my grown-up son, my lord.
 Granted lady that a father from the nature of a man may not
 weep.
 A mother's heart surely is tender.
 Why then do you not weep?"
 And to explain why she did not weep, she uttered a couple
 of stanzas as well.
 Uncalled he hither came, unbidden soon to go.
 Ian as he came he went, what cause is here for woe?
 No friend's lament can touch the ashes of the dead.
 Why should I grieve?
 He fares the way he had to tread.
 On hearing the words of the brahmin's wife, Saka asked the
 sister, "Lady, what was the
 dead man to you?"
 He was my brother, my lord.
 "Lady, sisters surely are loving towards their brothers.
 Why do you not weep?"
 But she, to explain the reason why she did not weep,
 repeated a couple of stanzas as
 well.
 "Though I should fast and weep, how would it profit me?
 My kitten-kin, alas, would more unhappy be.
 No friend's lament can touch the ashes of the dead.
 Why should I grieve?
 He fares the way he had to tread."
 Saka on hearing the words of the sister asked his wife, the
 dead man's wife, "Lady, what
 was he to you?"
 He was my husband, my lord.
 Then surely when a husband dies his widows are helpless,
 why do you not weep?
 But she, to explain the reason why she did not weep,
 uttered two stanzas of her own.
 As children cry in vain to grasp the moon above, so mortals
 idly mourn the loss of those
 they love.
 No friend's lament can touch the ashes of the dead.
 Why should I grieve?
 He fares the way he had to tread.
 Saka, on hearing the words of the wife, asked the handmaid,
 saying, "Woman, what was he to
 you?"
 He was my master, my lord.
 No doubt you must have been abused and beaten and oppressed
 by him, and therefore, thinking
 he is happily dead, you weep not.
 Speak not, so, my lord, that this does not suit his case.
 My young master was full of long-suffering and love and
 pity for me, and was as a foster
 child to me.
 Then why do you not weep?
 And she, to explain why she did not weep, uttered a couple
 of stanzas herself.
 A broken pot of earth, ah, who can peace again?
 So too to mourn the dead is not but labor vain.
 No friend's lament can touch the ashes of the dead.
 Why should I grieve?
 He fares the way he had to tread.
 Saka, on hearing what they all had to say, was greatly
 pleased, and said, "Ye have carefully
 dwelt on the thought of death.
 Henceforth ye are not to labor with your own hands.
 I am Saka, king of heaven.
 I will create the seven treasures in countless abundance in
 your house.
 Ye are to give alms, to keep the moral law, to observe holy
 days, and to take heed to
 your ways."
 And thus admonishing them, he filled their house with
 countless wealth, and so parted
 from them.
 The master, having finished his exposition of the truth,
 declared the four noble truths
 and identified the birth.
 At the conclusion of the truths, the landowner attained the
 fruit of the first path, which
 is the path of Sotapanna, means he has seen the truth.
 At that time Kudjutara was the female slave, Upala Vana was
 the daughter, Rahula was the
 son, Kama was the mother, and I myself was the brahmin.
 The end.
 So that's all for today.
 Thank you for coming to listen.
