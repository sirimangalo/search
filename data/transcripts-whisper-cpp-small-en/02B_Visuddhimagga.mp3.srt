1
00:00:00,000 --> 00:00:08,000
 [ Pause ]

2
00:00:08,000 --> 00:00:13,920
 -- sounds and what the restraint here means is not to have

3
00:00:13,920 --> 00:00:16,600
 agus a la when we come across

4
00:00:16,600 --> 00:00:20,000
 these objects.

5
00:00:20,000 --> 00:00:42,150
 And the third paragraph C is the virtue of livelihood pur

6
00:00:42,150 --> 00:00:42,240
ification.

7
00:00:42,240 --> 00:00:52,240
 Now for monks, pure livelihood means --

8
00:00:52,240 --> 00:00:58,240
 [ Pause ]

9
00:00:58,240 --> 00:01:18,240
 -- getting requisites by proper means.

10
00:01:18,240 --> 00:01:24,240
 That means by going for arms-on, monks get food.

11
00:01:24,240 --> 00:01:33,260
 And by picking up pieces of cloth and making these pieces

12
00:01:33,260 --> 00:01:38,240
 of cloth into a robe and wearing

13
00:01:38,240 --> 00:01:39,240
 it and so on.

14
00:01:39,240 --> 00:01:44,240
 So these are the good livelihoods of the monks.

15
00:01:44,240 --> 00:01:50,290
 But if monks do not follow these rules and there is no

16
00:01:50,290 --> 00:01:52,240
 purity or virtue of livelihood

17
00:01:52,240 --> 00:01:58,240
 purification, it will also be explained in detail later.

18
00:01:58,240 --> 00:02:04,620
 Here, abstinence from such wrong livelihoods entails trans

19
00:02:04,620 --> 00:02:06,240
gression of the six training

20
00:02:06,240 --> 00:02:11,150
 precepts announced with respect to livelihood and entails

21
00:02:11,150 --> 00:02:13,240
 the evil states beginning with

22
00:02:13,240 --> 00:02:18,740
 scheming, talking, hinting, belittling, pursuing gain with

23
00:02:18,740 --> 00:02:21,240
 gain is virtue of livelihood purification.

24
00:02:21,240 --> 00:02:26,100
 If you have read through this chapter, you will know what

25
00:02:26,100 --> 00:02:27,240
 these are.

26
00:02:27,240 --> 00:02:36,240
 And I'm afraid that you know more about the tricks.

27
00:02:36,240 --> 00:02:39,240
 [ Laughter ]

28
00:02:39,240 --> 00:02:48,240
 That's a bad monk's use in obtaining what they want.

29
00:02:48,240 --> 00:02:58,590
 Now, the six training precepts we will find in paragraph 60

30
00:02:58,590 --> 00:02:59,240
.

31
00:02:59,240 --> 00:03:11,240
 So in paragraph 60, these are given one by one.

32
00:03:11,240 --> 00:03:15,110
 "With livelihood as cause, with livelihood as reason, one

33
00:03:15,110 --> 00:03:17,240
 of evil wishes, a prey to wishes,

34
00:03:17,240 --> 00:03:21,180
 lays claim to a higher than human state that does not exist

35
00:03:21,180 --> 00:03:23,240
 and not effect the contravention

36
00:03:23,240 --> 00:03:26,240
 of which is defeat."

37
00:03:26,240 --> 00:03:35,250
 That means because, suppose, I want to be rich, so I want

38
00:03:35,250 --> 00:03:37,240
 to possess many,

39
00:03:37,240 --> 00:03:42,680
 I want to be rich, so I want to be rich, so I want to be

40
00:03:42,680 --> 00:03:43,240
 rich, so I want to be rich,

41
00:03:43,240 --> 00:03:47,850
 so I want to be rich, so I want to be rich, so I want to be

42
00:03:47,850 --> 00:03:49,240
 rich, so I want to be rich,

43
00:03:49,240 --> 00:03:52,850
 so I want to be rich, so I want to be rich, so I want to be

44
00:03:52,850 --> 00:03:55,240
 rich, so I want to be rich,

45
00:03:55,240 --> 00:03:59,980
 so I want to be rich, so I want to be rich, so I want to be

46
00:03:59,980 --> 00:04:01,240
 rich, so I want to be rich,

47
00:04:01,240 --> 00:04:09,570
 so such a breach of the rule is caused by something like

48
00:04:09,570 --> 00:04:11,240
 livelihood.

49
00:04:11,240 --> 00:04:15,670
 That means I don't want to go out for arms, I want to be at

50
00:04:15,670 --> 00:04:18,240
 the monastery and just eat

51
00:04:18,240 --> 00:04:24,240
 what people bring to me, maybe rich food and so on.

52
00:04:24,240 --> 00:04:34,320
 So these six rules are proclaimed or laid down with regard

53
00:04:34,320 --> 00:04:37,240
 to monks' livelihood.

54
00:04:37,240 --> 00:04:43,240
 So defeat, you see the word defeat there, right?

55
00:04:43,240 --> 00:04:48,240
 And these are technical words.

56
00:04:48,240 --> 00:04:54,880
 Defeat means if a monk breaks this rule, then he is

57
00:04:54,880 --> 00:04:56,240
 finished as a monk,

58
00:04:56,240 --> 00:04:59,730
 he is defeated as a monk, he is no longer a monk, even

59
00:04:59,730 --> 00:05:01,240
 though he may be,

60
00:05:01,240 --> 00:05:05,240
 he may still go on wearing the robes and even though he may

61
00:05:05,240 --> 00:05:10,240
 still go on admitting himself as a monk,

62
00:05:10,240 --> 00:05:16,780
 but he is in fact in reality not a monk. The moment he says

63
00:05:16,780 --> 00:05:23,240
 this and the other understands it.

64
00:05:23,240 --> 00:05:32,610
 So there are these six rules, we'll come to these six rules

65
00:05:32,610 --> 00:05:34,240
 later.

66
00:05:34,240 --> 00:05:41,290
 If the person spoken to does not understand at that moment,

67
00:05:41,290 --> 00:05:46,240
 then he is not defeated.

68
00:05:46,240 --> 00:05:49,680
 But if the person to whom he is speaking knows at the

69
00:05:49,680 --> 00:05:55,240
 moment that this monk is saying that he is enlightened

70
00:05:55,240 --> 00:05:59,350
 and the monk is not really enlightened, then there is

71
00:05:59,350 --> 00:06:00,240
 defeat.

72
00:06:00,240 --> 00:06:04,240
 So Mr. Beat depends on somebody else recognizing him.

73
00:06:04,240 --> 00:06:25,240
 Yeah, understanding, yes.

74
00:06:25,240 --> 00:06:31,850
 And I want you to read, I think you have read, the "

75
00:06:31,850 --> 00:06:35,240
Resource and Others" for monks,

76
00:06:35,240 --> 00:06:38,660
 which are resource for monks and which are not resource for

77
00:06:38,660 --> 00:06:39,240
 monks.

78
00:06:39,240 --> 00:06:44,630
 "Resource" here means which monks must engage in and which

79
00:06:44,630 --> 00:06:47,240
 monks must not engage in.

80
00:06:47,240 --> 00:06:58,220
 Now on page 17 in paragraph 44, about middle of the

81
00:06:58,220 --> 00:07:00,240
 paragraph you find,

82
00:07:00,240 --> 00:07:04,500
 "Here someone makes a lively food by gifts of bamboos or by

83
00:07:04,500 --> 00:07:06,240
 gifts of leaves or by gifts of flowers,

84
00:07:06,240 --> 00:07:09,650
 fruits, bathing powder and tooth sticks or by flattery or

85
00:07:09,650 --> 00:07:13,240
 by bean soupery."

86
00:07:13,240 --> 00:07:20,240
 You know that?

87
00:07:20,240 --> 00:07:25,240
 "By fondling." Now I'll come to bean soupery later.

88
00:07:25,240 --> 00:07:32,970
 "By fondling" really means by babysitting, by picking up a

89
00:07:32,970 --> 00:07:36,240
 baby and by taking care of it.

90
00:07:36,240 --> 00:07:41,240
 Not just by fondling, by taking care of a baby.

91
00:07:41,240 --> 00:07:45,040
 See, somebody leaves a baby with you and with a monk and if

92
00:07:45,040 --> 00:07:48,240
 he takes care of the baby

93
00:07:48,240 --> 00:07:52,780
 in order to please that man, that person, then it is called

94
00:07:52,780 --> 00:07:54,240
 "here fondly."

95
00:07:54,240 --> 00:08:01,840
 Now bean soupery, that means living by a livelihood which

96
00:08:01,840 --> 00:08:04,240
 resembles bean soupery.

97
00:08:04,240 --> 00:08:12,240
 And you don't still understand, right? So please turn to

98
00:08:12,240 --> 00:08:12,240
 page 28.

99
00:08:12,240 --> 00:08:23,240
 Paragraph 75, "Bean soupery is resemblance to bean soup.

100
00:08:23,240 --> 00:08:27,310
 For just as when beans are being cooked, only a few do not

101
00:08:27,310 --> 00:08:30,240
 get cooked, the rest get cooked.

102
00:08:30,240 --> 00:08:34,410
 So to the person in whose speech only a little is true, the

103
00:08:34,410 --> 00:08:43,240
 rest being false, is called a bean soup."

104
00:08:43,240 --> 00:08:45,240
 That's called bean soupery.

105
00:08:45,240 --> 00:08:56,240
 So most of what he said, it just lies.

106
00:08:56,240 --> 00:08:59,690
 There's an explanation in English but not meaning the same

107
00:08:59,690 --> 00:09:00,240
 thing.

108
00:09:00,240 --> 00:09:06,240
 Half-baked or something. But that doesn't mean that...

109
00:09:06,240 --> 00:09:10,240
 Why is cracking?

110
00:09:10,240 --> 00:09:14,240
 Why is cracking? I don't know.

111
00:09:14,240 --> 00:09:19,820
 If what most of what I say is not true and only a little is

112
00:09:19,820 --> 00:09:20,240
 true,

113
00:09:20,240 --> 00:09:32,240
 then I'm guilty of this bean soupery.

114
00:09:32,240 --> 00:09:39,220
 And on page 18, the resort, we have to understand this

115
00:09:39,220 --> 00:09:40,240
 properly.

116
00:09:40,240 --> 00:09:43,750
 Now, proper resort. There is proper resort and improper

117
00:09:43,750 --> 00:09:44,240
 resort.

118
00:09:44,240 --> 00:09:48,690
 Hearing what is improper resort. Here someone has

119
00:09:48,690 --> 00:09:50,240
 prostitutes as resort,

120
00:09:50,240 --> 00:09:53,570
 or he has widows, old maids, eunuchs, bikunis, or devans as

121
00:09:53,570 --> 00:09:54,240
 resort.

122
00:09:54,240 --> 00:09:58,670
 Having them as resort means being friends with them, being

123
00:09:58,670 --> 00:10:00,240
 intimate with them,

124
00:10:00,240 --> 00:10:14,020
 frequenting their houses. That is what is meant by having

125
00:10:14,020 --> 00:10:18,240
 them as resort.

126
00:10:18,240 --> 00:10:25,150
 So there are some places which monks must always avoid, the

127
00:10:25,150 --> 00:10:29,240
 places of these people.

128
00:10:29,240 --> 00:10:38,260
 And then on page 19, paragraph 49, another kind of resort,

129
00:10:38,260 --> 00:10:40,240
 three kinds of resort.

130
00:10:40,240 --> 00:10:44,890
 Proper resort as support, proper resort as guarding, and

131
00:10:44,890 --> 00:10:48,240
 proper resort as anchoring.

132
00:10:48,240 --> 00:10:50,240
 Hearing what is proper resort as support.

133
00:10:50,240 --> 00:10:54,630
 A good friend who exhibits the 10 instances of talk we are

134
00:10:54,630 --> 00:10:56,240
 given in the footnote.

135
00:10:56,240 --> 00:11:00,700
 In whose presence one hears what has not been heard and so

136
00:11:00,700 --> 00:11:01,240
 on.

137
00:11:01,240 --> 00:11:07,150
 In whose presence really means depending on whom, not just

138
00:11:07,150 --> 00:11:09,240
 in his presence.

139
00:11:09,240 --> 00:11:14,240
 That means from him. You hear something from him.

140
00:11:14,240 --> 00:11:17,630
 That is what is meant here. And hears that what has not

141
00:11:17,630 --> 00:11:20,240
 been heard, corrects what has been heard,

142
00:11:20,240 --> 00:11:24,890
 but gets rid of doubt, rectifies one's view, and gains

143
00:11:24,890 --> 00:11:26,240
 confidence.

144
00:11:26,240 --> 00:11:31,240
 Now these are the benefits of hearing to Dhamma talk.

145
00:11:31,240 --> 00:11:37,220
 There are, Buddha said in one sutta, that there are five

146
00:11:37,220 --> 00:11:43,240
 benefits to be gained from listening to Dhamma talk.

147
00:11:43,240 --> 00:11:48,550
 The first one is you hear what you have not heard before,

148
00:11:48,550 --> 00:11:51,240
 new information that is.

149
00:11:51,240 --> 00:11:54,240
 And then you correct what has been heard.

150
00:11:54,240 --> 00:11:58,840
 That means you can clarify what you have heard before when

151
00:11:58,840 --> 00:12:00,240
 you hear it again.

152
00:12:00,240 --> 00:12:04,240
 And then get rid of doubt, that is the third benefit.

153
00:12:04,240 --> 00:12:08,540
 You can remove doubts about something when you hear Dhamma

154
00:12:08,540 --> 00:12:09,240
 talk.

155
00:12:09,240 --> 00:12:14,000
 Rectifies one's view. If you have a wrong view, then you

156
00:12:14,000 --> 00:12:18,240
 can set it right when you listen to Dhamma talk.

157
00:12:18,240 --> 00:12:23,690
 And gains confidence. That means your mind becomes full of

158
00:12:23,690 --> 00:12:25,240
 confidence.

159
00:12:25,240 --> 00:12:32,040
 So these are the five benefits to be gained from listening

160
00:12:32,040 --> 00:12:34,240
 to Dhamma talk.

161
00:12:34,240 --> 00:12:39,530
 Or by training under whom, and actually not training under

162
00:12:39,530 --> 00:12:45,240
 whom, by following his example, by following whose example?

163
00:12:45,240 --> 00:12:51,110
 One grows in faith, virtue, learning generosity and

164
00:12:51,110 --> 00:12:53,240
 understanding.

165
00:12:53,240 --> 00:12:59,240
 And next paragraph, what is proper resort as guarding?

166
00:12:59,240 --> 00:13:03,550
 "A bhikkhu having entered inside a house, having gone into

167
00:13:03,550 --> 00:13:05,240
 a street and so on."

168
00:13:05,240 --> 00:13:14,240
 He always made this mistake. "Nyanam holi."

169
00:13:14,240 --> 00:13:23,240
 Now, the Pali word here is "antara-gara".

170
00:13:23,240 --> 00:13:30,800
 Now "gara" means a house, and "antara" means in, between or

171
00:13:30,800 --> 00:13:32,240
 within.

172
00:13:32,240 --> 00:13:37,780
 So "antara-gara", the Pali word, he translated as "inside a

173
00:13:37,780 --> 00:13:42,540
 house" because "antara" can mean inside, and "gara" means

174
00:13:42,540 --> 00:13:43,240
 house.

175
00:13:43,240 --> 00:13:52,260
 But the real meaning is "a place which has houses in it",

176
00:13:52,260 --> 00:13:56,240
 so it means a village.

177
00:13:56,240 --> 00:14:01,600
 So having entered a village, having gone to a street, goes

178
00:14:01,600 --> 00:14:05,240
 with downcast eyes, seeing the length of a plow yoke.

179
00:14:05,240 --> 00:14:12,170
 So not inside a house. Monks must keep their eyes down when

180
00:14:12,170 --> 00:14:18,180
 they go out into the village or into the town, not just

181
00:14:18,180 --> 00:14:20,240
 inside a house.

182
00:14:20,240 --> 00:14:28,240
 So this is the wrong rendering of the Pali word.

183
00:14:28,240 --> 00:14:33,850
 And then, seeing the length of a plow yoke, in fact, it is

184
00:14:33,850 --> 00:14:37,240
 not plow yoke, it is carriage yoke.

185
00:14:37,240 --> 00:14:44,220
 And a yoke is said to be of the, about four qubits long,

186
00:14:44,220 --> 00:14:49,240
 that means six feet, six feet long.

187
00:14:49,240 --> 00:14:55,570
 So a monk is to look about six feet on the ground, not look

188
00:14:55,570 --> 00:14:58,240
 up or look sideways.

189
00:14:58,240 --> 00:15:06,930
 It may be possible where it is not so crowded, but in

190
00:15:06,930 --> 00:15:10,240
 modern cities it is impossible.

191
00:15:10,240 --> 00:15:16,730
 You have to look. If you just look down and walk, you will

192
00:15:16,730 --> 00:15:21,240
 be knocked down by a car or something.

193
00:15:21,240 --> 00:15:29,070
 But monks are trained to keep their eyes down, not looking

194
00:15:29,070 --> 00:15:31,840
 at an elephant, not looking at a horse, not looking at a

195
00:15:31,840 --> 00:15:32,240
 carriage,

196
00:15:32,240 --> 00:15:42,240
 but just being a woman, man and so on.

197
00:15:42,240 --> 00:15:49,240
 So now we come to the end of Pali mokha, restraint.

198
00:15:49,240 --> 00:15:54,210
 Seeing fear in the slightest fault means seeing danger in

199
00:15:54,210 --> 00:15:56,240
 the slightest fault.

200
00:15:56,240 --> 00:16:02,480
 So even a very slight fault or a transgression can do harm

201
00:16:02,480 --> 00:16:07,240
 to you, because especially when you practice meditation,

202
00:16:07,240 --> 00:16:11,600
 this can be a great obstacle to your progress or to your

203
00:16:11,600 --> 00:16:13,240
 concentration.

204
00:16:13,240 --> 00:16:20,240
 Now, a monk is instructed to keep the rules intact.

205
00:16:20,240 --> 00:16:27,800
 Even if he has broken a minor rule and if he broke it

206
00:16:27,800 --> 00:16:35,230
 intentionally, then the feeling of guilt is always with him

207
00:16:35,230 --> 00:16:35,240
.

208
00:16:35,240 --> 00:16:40,390
 And this feeling of guilt will torment him when he

209
00:16:40,390 --> 00:16:43,240
 practices meditation.

210
00:16:43,240 --> 00:16:51,410
 So the slightest transgression is, there is danger even in

211
00:16:51,410 --> 00:16:55,240
 slightest transgression.

212
00:16:55,240 --> 00:17:00,740
 So seeing danger in slightest transgression, he keeps his

213
00:17:00,740 --> 00:17:08,240
 sealer, his virtue really pure.

214
00:17:08,240 --> 00:17:27,240
 So we come to the end of the Pali mokha restraint today.

215
00:17:27,240 --> 00:17:31,290
 How many pages do we have to go until the end of the first

216
00:17:31,290 --> 00:17:38,240
 chapter? 58 pages. So two more weeks maybe.

217
00:17:38,240 --> 00:17:47,240
 So please read on as much as you can.

218
00:17:47,240 --> 00:17:58,240
 The description of the virtue? Yes.

219
00:18:12,240 --> 00:18:25,240
 Okay. It's close to nine o'clock now.

220
00:18:25,240 --> 00:18:54,240
 I didn't turn it on, so I was not aware of time.

221
00:18:54,240 --> 00:19:23,240
 Okay.

222
00:19:23,240 --> 00:19:52,240
 Okay.

223
00:19:52,240 --> 00:20:21,240
 Okay.

