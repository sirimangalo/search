1
00:00:00,000 --> 00:00:07,280
 Hi, next question is from nomad1545.

2
00:00:07,280 --> 00:00:10,440
 I hope this is a proper question to the series, since it is

3
00:00:10,440 --> 00:00:12,120
 a subject that has been in my

4
00:00:12,120 --> 00:00:14,400
 mind for a very long time.

5
00:00:14,400 --> 00:00:18,450
 How does and how can meditation practice help one that had

6
00:00:18,450 --> 00:00:20,740
 a relationship torn apart with

7
00:00:20,740 --> 00:00:25,580
 another person?

8
00:00:25,580 --> 00:00:30,300
 I suppose it's important to state off the bat that

9
00:00:30,300 --> 00:00:32,760
 meditation doesn't particularly

10
00:00:32,760 --> 00:00:37,660
 help with relationships, specific relationships with other

11
00:00:37,660 --> 00:00:38,160
 people.

12
00:00:38,160 --> 00:00:44,790
 And it does often, in fact, weaken our bond with the people

13
00:00:44,790 --> 00:00:48,100
 who we used to find ourselves

14
00:00:48,100 --> 00:00:50,160
 very attracted to.

15
00:00:50,160 --> 00:00:52,800
 And why it does this is because meditation helps us to

16
00:00:52,800 --> 00:00:54,600
 relate to humankind in general.

17
00:00:54,600 --> 00:00:57,760
 It helps us to relate to humanity.

18
00:00:57,760 --> 00:01:02,600
 And so as opposed to being partial to certain people, which

19
00:01:02,600 --> 00:01:05,200
 we're less inclined to do, we

20
00:01:05,200 --> 00:01:09,160
 come to accept all people and to be able to see into the

21
00:01:09,160 --> 00:01:11,040
 nature of all people.

22
00:01:11,040 --> 00:01:17,890
 We often, I would say a meditator often finds themselves

23
00:01:17,890 --> 00:01:22,640
 less inclined to become close with

24
00:01:22,640 --> 00:01:25,730
 the majority of humankind, even the people who are also

25
00:01:25,730 --> 00:01:27,960
 striving to practice meditation.

26
00:01:27,960 --> 00:01:31,980
 Because what you start to realize is that you have bad

27
00:01:31,980 --> 00:01:34,280
 things inside and the people

28
00:01:34,280 --> 00:01:38,120
 around you who you used to think were perfect also have bad

29
00:01:38,120 --> 00:01:39,800
 things inside of them.

30
00:01:39,800 --> 00:01:43,440
 And all that happens when you get involved with them is you

31
00:01:43,440 --> 00:01:44,120
 clash.

32
00:01:44,120 --> 00:01:47,170
 These defilements, these bad things inside, they clash

33
00:01:47,170 --> 00:01:47,920
 together.

34
00:01:47,920 --> 00:01:55,480
 And this is why monasticism and solitude are so much

35
00:01:55,480 --> 00:02:01,520
 encouraged in the Buddha's teaching.

36
00:02:01,520 --> 00:02:08,000
 So first off, as far as helping to solidify or to maintain

37
00:02:08,000 --> 00:02:11,320
 a relationship, I would say

38
00:02:11,320 --> 00:02:19,300
 not very likely. Meditation helps you to deal with the

39
00:02:19,300 --> 00:02:19,880
 problem of having a relationship

40
00:02:19,880 --> 00:02:23,450
 torn apart or the sense of loss that comes from the, you've

41
00:02:23,450 --> 00:02:25,380
 got to say it, from the addiction

42
00:02:25,380 --> 00:02:28,600
 to the other person, from the need to be close to a certain

43
00:02:28,600 --> 00:02:30,920
 individual, the partiality towards

44
00:02:30,920 --> 00:02:31,920
 them.

45
00:02:31,920 --> 00:02:35,910
 By helping you to become an island unto yourself, helping

46
00:02:35,910 --> 00:02:37,920
 you to become strong and to see the

47
00:02:37,920 --> 00:02:42,600
 nature of reality, that actually what's going on when you

48
00:02:42,600 --> 00:02:44,920
 are engaged in a relationship

49
00:02:44,920 --> 00:02:51,920
 is a whole lot of addiction and attachment and the pleasure

50
00:02:51,920 --> 00:02:55,400
 and the need for the pleasure,

51
00:02:55,400 --> 00:02:58,690
 the need for the happiness that comes and the memories that

52
00:02:58,690 --> 00:03:00,200
 are associated with that,

53
00:03:00,200 --> 00:03:04,180
 all the good times, how they used to be and so on and so on

54
00:03:04,180 --> 00:03:04,600
.

55
00:03:04,600 --> 00:03:08,460
 So meditation helping us to see these things clearly helps

56
00:03:08,460 --> 00:03:10,600
 us to realize that, on another

57
00:03:10,600 --> 00:03:14,530
 level helps us to realize that there is no I and there is

58
00:03:14,530 --> 00:03:16,480
 no you, there is no us and

59
00:03:16,480 --> 00:03:20,520
 them, there is no parts to the relationship.

60
00:03:20,520 --> 00:03:23,240
 When there is a relationship going on, there is just

61
00:03:23,240 --> 00:03:25,360
 phenomena arising and ceasing, there

62
00:03:25,360 --> 00:03:28,380
 is the mind and there is mind and there is body, there is

63
00:03:28,380 --> 00:03:30,360
 seeing, there is hearing, there

64
00:03:30,360 --> 00:03:33,020
 is smelling, there is tasting, there is feeling and there

65
00:03:33,020 --> 00:03:33,800
 is thinking.

66
00:03:33,800 --> 00:03:37,900
 And these things come and go and are always in a state of

67
00:03:37,900 --> 00:03:38,640
 flux.

68
00:03:38,640 --> 00:03:42,360
 And so we come to be less attached to the concept of an

69
00:03:42,360 --> 00:03:44,560
 individual and we come to see

70
00:03:44,560 --> 00:03:50,220
 experience as the essential nature of reality.

71
00:03:50,220 --> 00:03:53,510
 And so as a result, as I said in the beginning, we become

72
00:03:53,510 --> 00:03:55,440
 more universal in our love and in

73
00:03:55,440 --> 00:03:58,710
 our appreciation of the good parts of humanity, of the good

74
00:03:58,710 --> 00:04:00,360
 qualities and in our love and

75
00:04:00,360 --> 00:04:04,370
 our compassion towards other beings so that we only see the

76
00:04:04,370 --> 00:04:06,400
 evil, the good, the suffering

77
00:04:06,400 --> 00:04:11,430
 and the happiness and we don't see individuals and we don't

78
00:04:11,430 --> 00:04:12,960
 become partial.

79
00:04:12,960 --> 00:04:15,350
 Meditation certainly helps you to deal with a torn

80
00:04:15,350 --> 00:04:17,240
 relationship, it just might not lead

81
00:04:17,240 --> 00:04:22,620
 you to go back to that relationship and instead it might

82
00:04:22,620 --> 00:04:26,440
 bring you to a much more comfortable,

83
00:04:26,440 --> 00:04:30,560
 peaceful state of good relationship with all beings or

84
00:04:30,560 --> 00:04:33,200
 proper relationship and in general

85
00:04:33,200 --> 00:04:38,000
 much less of a relationship outwardly with all beings where

86
00:04:38,000 --> 00:04:40,200
 you start to look inwardly

87
00:04:40,200 --> 00:04:44,350
 and you see the nature of your own existence and you come

88
00:04:44,350 --> 00:04:46,800
 to understand the nature of all

89
00:04:46,800 --> 00:04:50,340
 existence and you don't mess around with external

90
00:04:50,340 --> 00:04:53,600
 relationships because you see that that's

91
00:04:53,600 --> 00:04:56,280
 generally bound up in defilement.

92
00:04:56,280 --> 00:04:59,580
 So I hope that is somehow getting at the question and

93
00:04:59,580 --> 00:05:02,000
 giving you a bit of an answer in terms

94
00:05:02,000 --> 00:05:03,000
 of a Buddhist perspective.

95
00:05:03,000 --> 00:05:04,000
 Okay, thanks for the question.

96
00:05:04,000 --> 00:05:04,000
 Keep them coming.

97
00:05:04,000 --> 00:05:05,400
 1

