1
00:00:00,000 --> 00:00:04,370
 Dislacking Q&A - Meditating when you do not want to med

2
00:00:04,370 --> 00:00:04,800
itate

3
00:00:04,800 --> 00:00:10,630
 Question - How do I meditate when I do not want to meditate

4
00:00:10,630 --> 00:00:10,640
?

5
00:00:10,640 --> 00:00:14,030
 I know it sounds like a frivolous question but it is a

6
00:00:14,030 --> 00:00:15,840
 serious issue for me.

7
00:00:15,840 --> 00:00:19,890
 When I sit down, I can meditate for a short time but

8
00:00:19,890 --> 00:00:22,160
 eventually I get frustrated.

9
00:00:22,160 --> 00:00:26,720
 My muscles cramp up and it feels like my head is shaking

10
00:00:26,720 --> 00:00:29,040
 and it becomes really difficult

11
00:00:29,040 --> 00:00:33,450
 so I end up just getting up. Whatever it is, I make an

12
00:00:33,450 --> 00:00:34,160
 excuse.

13
00:00:34,160 --> 00:00:38,560
 I guess my ego is against it or something.

14
00:00:38,560 --> 00:00:42,160
 I hope it is not a frivolous question because a lot of

15
00:00:42,160 --> 00:00:45,040
 people recommend just sitting through it.

16
00:00:45,040 --> 00:00:50,560
 Either I do not have the willpower for it or I am just lazy

17
00:00:50,560 --> 00:00:50,560
.

18
00:00:50,560 --> 00:00:53,600
 But is there any practical advice for that?

19
00:00:55,520 --> 00:01:00,570
 Answer - We sometimes make the mistake of forcing ourselves

20
00:01:00,570 --> 00:01:03,280
 to meditate which is not great because

21
00:01:03,280 --> 00:01:07,480
 it will cultivate an aversion to meditation. That being

22
00:01:07,480 --> 00:01:10,640
 said, in the beginning you do not know what

23
00:01:10,640 --> 00:01:14,480
 is good for you. The untrained mind is like a child.

24
00:01:14,480 --> 00:01:17,600
 Parents have to push children to learn.

25
00:01:17,600 --> 00:01:21,630
 They do not force them but they have to at least direct

26
00:01:21,630 --> 00:01:22,320
 them.

27
00:01:23,120 --> 00:01:27,790
 Forcing does not generally have the desired effect and as a

28
00:01:27,790 --> 00:01:30,560
 result, parents who push their children

29
00:01:30,560 --> 00:01:36,140
 too hard find they rebel or worse. Pushing a child too hard

30
00:01:36,140 --> 00:01:40,080
 can destroy their lives due to intense

31
00:01:40,080 --> 00:01:45,310
 stress. In terms of mental development, most people are

32
00:01:45,310 --> 00:01:49,120
 like children. They have never truly

33
00:01:49,120 --> 00:01:53,510
 grown up. The spiritual part of most of us is quite

34
00:01:53,510 --> 00:01:57,680
 immature. This is especially true if you

35
00:01:57,680 --> 00:02:01,530
 are not comfortable practicing meditation. When first

36
00:02:01,530 --> 00:02:05,120
 starting out, you have to treat your mind

37
00:02:05,120 --> 00:02:10,740
 like a child. In the beginning, you have to be a little bit

38
00:02:10,740 --> 00:02:14,080
 insistent. Consider how you would

39
00:02:14,080 --> 00:02:19,550
 teach a child. Hopefully, you would not force your will on

40
00:02:19,550 --> 00:02:22,480
 a child to the extent that it would cause

41
00:02:22,480 --> 00:02:26,880
 them unwarranted stress and suffering. But on the other

42
00:02:26,880 --> 00:02:30,080
 hand, you would not just let your child sit

43
00:02:30,080 --> 00:02:35,210
 around eating candy or watching cartoons all day. Instead,

44
00:02:35,210 --> 00:02:38,480
 you would try to direct them skillfully

45
00:02:38,480 --> 00:02:43,380
 in a wholesome direction. There is no easy answer to

46
00:02:43,380 --> 00:02:46,480
 cultivating the desire to meditate

47
00:02:46,480 --> 00:02:51,570
 in the beginning stages of the practice except to say do

48
00:02:51,570 --> 00:02:54,720
 not simply force yourself to meditate

49
00:02:54,720 --> 00:02:59,240
 and the other hand do not say well then I just won't med

50
00:02:59,240 --> 00:03:03,520
itate. I am sure you know this otherwise

51
00:03:03,520 --> 00:03:07,820
 you would not be asking this question. You want to meditate

52
00:03:07,820 --> 00:03:10,640
. You just do not want to meditate.

53
00:03:10,640 --> 00:03:14,630
 You know it is good for you intellectually because

54
00:03:14,630 --> 00:03:18,560
 intellectually you are all grown up but

55
00:03:18,560 --> 00:03:23,730
 part of you is still a child. A big part of the problem is

56
00:03:23,730 --> 00:03:27,120
 a disconnect from the actual benefits

57
00:03:27,120 --> 00:03:31,920
 of meditation. We are not machines where you can input the

58
00:03:31,920 --> 00:03:34,800
 benefits of meditation and have those

59
00:03:34,800 --> 00:03:40,010
 inputs stored in our programming. You can tell yourself the

60
00:03:40,010 --> 00:03:42,960
 benefits or listen to a talk on how

61
00:03:42,960 --> 00:03:47,860
 great meditation is and think meditation is awesome I want

62
00:03:47,860 --> 00:03:51,280
 to meditate but then your mind will change

63
00:03:51,280 --> 00:03:55,980
 and you will lose this feeling. It is common during

64
00:03:55,980 --> 00:04:00,160
 beginner meditation courses where you will be

65
00:04:00,160 --> 00:04:04,760
 practicing and say wow this is so wonderful meditation

66
00:04:04,760 --> 00:04:07,440
 helps me so much and the next day

67
00:04:07,440 --> 00:04:12,450
 you will feel completely different and say I want to leave

68
00:04:12,450 --> 00:04:15,840
 this is useless what am I even doing here

69
00:04:17,040 --> 00:04:22,140
 the reason for this is that we are organic our mind is part

70
00:04:22,140 --> 00:04:25,200
 of the organism and it is not like

71
00:04:25,200 --> 00:04:30,850
 a computer it is going to act irrationally so a part of the

72
00:04:30,850 --> 00:04:34,080
 practice is going to be stepping back

73
00:04:34,080 --> 00:04:40,380
 and saying okay yes I do not want to meditate I acknowledge

74
00:04:40,380 --> 00:04:43,920
 that and not ignoring it but not

75
00:04:43,920 --> 00:04:48,640
 clinging to it either when you acknowledge the mind state

76
00:04:48,640 --> 00:04:51,200
 that does not want to meditate

77
00:04:51,200 --> 00:04:56,220
 and you will find that you are able to take the frustration

78
00:04:56,220 --> 00:04:58,640
 and disliking as an object

79
00:04:58,640 --> 00:05:03,760
 and you find it evaporates with patient observation try to

80
00:05:03,760 --> 00:05:07,600
 be very aware of the aversion

81
00:05:07,600 --> 00:05:13,140
 to meditation understanding that after some time you do

82
00:05:13,140 --> 00:05:17,280
 grow up and as you grow up you will want

83
00:05:17,280 --> 00:05:23,460
 to meditate more this is not an insult as most of us start

84
00:05:23,460 --> 00:05:27,840
 out in a similar situation understand

85
00:05:27,840 --> 00:05:33,310
 that in the beginning you are going to have to coax and caj

86
00:05:33,310 --> 00:05:36,720
ole and play games with your inner child

87
00:05:36,720 --> 00:05:40,700
 to help it grow up beyond this there is room for some

88
00:05:40,700 --> 00:05:44,160
 amount of philosophical conversation with

89
00:05:44,160 --> 00:05:48,610
 yourself about the benefits of meditation as long as you

90
00:05:48,610 --> 00:05:51,680
 acknowledge both sides of the argument

91
00:05:51,680 --> 00:05:57,330
 if you do not give the devitements their fair trial once

92
00:05:57,330 --> 00:06:00,240
 the aversion to meditation arises

93
00:06:00,240 --> 00:06:05,030
 it is too late it is already on wholesome so pushing it

94
00:06:05,030 --> 00:06:07,280
 away is not going to help

95
00:06:07,280 --> 00:06:12,140
 this is why the buddha said that when aversion arises in

96
00:06:12,140 --> 00:06:16,240
 the mind the key is to see that

97
00:06:16,240 --> 00:06:20,990
 aversion has a reason in the mind and not be prejudiced

98
00:06:20,990 --> 00:06:23,920
 against it before you see clearly

99
00:06:23,920 --> 00:06:29,200
 its true nature this is how a judge works they do not judge

100
00:06:29,200 --> 00:06:32,080
 rather they observe and they come

101
00:06:32,080 --> 00:06:36,820
 to decision based on the facts instead of any kind of

102
00:06:36,820 --> 00:06:40,240
 judgment you should be able to feel the

103
00:06:40,240 --> 00:06:44,350
 difference between forcing yourself to accept something and

104
00:06:44,350 --> 00:06:47,120
 truly understanding it just like

105
00:06:47,120 --> 00:06:51,330
 some religious people believe in god because they are

106
00:06:51,330 --> 00:06:54,400
 afraid to go to hell or their parents have

107
00:06:54,400 --> 00:06:58,920
 told them to or even intellectually as opposed to actually

108
00:06:58,920 --> 00:07:02,160
 truly understanding whether god exists

109
00:07:02,160 --> 00:07:06,010
 you will feel the difference between accepting meditation

110
00:07:06,010 --> 00:07:08,160
 because someone told you it was

111
00:07:08,160 --> 00:07:12,460
 beneficial or because intellectually you believe it is

112
00:07:12,460 --> 00:07:15,840
 right and actually getting a sense of how

113
00:07:15,840 --> 00:07:22,880
 good meditation is for you the latter is much more valuable

