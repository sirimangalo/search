1
00:00:00,000 --> 00:00:05,000
 Good evening everyone.

2
00:00:05,000 --> 00:00:16,000
 I'm broadcasting live July 8th.

3
00:00:16,000 --> 00:00:25,500
 Today's quote is about sickness.

4
00:00:25,500 --> 00:00:30,500
 Two kinds of sickness.

5
00:00:30,500 --> 00:00:36,440
 It's nice to look up. Two kinds of sickness, physical and

6
00:00:36,440 --> 00:00:50,500
 mental.

7
00:00:50,500 --> 00:01:01,500
 Somebody says there are differences with physical sickness.

8
00:01:01,500 --> 00:01:08,830
 You see people without physical sickness for days, weeks,

9
00:01:08,830 --> 00:01:17,500
 months, years on end.

10
00:01:17,500 --> 00:01:24,500
 Nobody escapes mental sickness even for a moment.

11
00:01:24,500 --> 00:01:28,500
 Except for those who are in life.

12
00:01:28,500 --> 00:01:35,290
 Mental illness is something that most people are plagued by

13
00:01:35,290 --> 00:01:40,500
 moment after moment, day in and day out.

14
00:01:40,500 --> 00:01:55,500
 It's a type of sickness that everyone carries with them.

15
00:01:55,500 --> 00:02:03,400
 So in Buddhism mental illness isn't simply the extreme

16
00:02:03,400 --> 00:02:11,960
 cases of clinical depression or anxiety or OCD or phobias,

17
00:02:11,960 --> 00:02:13,500
 psychosis.

18
00:02:13,500 --> 00:02:21,500
 All of these things are just extreme, extreme to the extent

19
00:02:21,500 --> 00:02:30,500
 that an ordinary, uninstructed person can recognize them

20
00:02:30,500 --> 00:02:38,500
 and latch on to them and identify them as a problem.

21
00:02:38,500 --> 00:02:45,180
 But most of the time we don't recognize and identify how

22
00:02:45,180 --> 00:02:48,500
 our mental illness is for what they are.

23
00:02:48,500 --> 00:03:03,650
 It's just illness and discord, distress until we come to

24
00:03:03,650 --> 00:03:05,500
 meditate.

25
00:03:05,500 --> 00:03:10,690
 Many people when they begin to meditate for the first time,

26
00:03:10,690 --> 00:03:16,220
 it's like revelation, realization that our minds aren't

27
00:03:16,220 --> 00:03:19,500
 quite as healthy as we thought they were.

28
00:03:19,500 --> 00:03:31,500
 That our minds are plagued by illness.

29
00:03:31,500 --> 00:03:43,950
 The illness of greed, the illness of anger, the illness of

30
00:03:43,950 --> 00:03:49,500
 delusion, the illnesses.

31
00:03:49,500 --> 00:03:56,210
 That really is the illness and the problem that we should

32
00:03:56,210 --> 00:04:03,110
 focus on and should understand Buddhism to focus on more so

33
00:04:03,110 --> 00:04:06,500
 than simply suffering.

34
00:04:06,500 --> 00:04:08,500
 More important is the cause of suffering.

35
00:04:08,500 --> 00:04:12,500
 There's suffering well and we can live with it.

36
00:04:12,500 --> 00:04:17,120
 It is possible to be at peace with what we would normally

37
00:04:17,120 --> 00:04:19,500
 term suffering.

38
00:04:19,500 --> 00:04:22,500
 There's no peace for one who has mental illness.

39
00:04:22,500 --> 00:04:28,500
 The suffering comes because we have mental illness.

40
00:04:28,500 --> 00:04:41,200
 It's really suffering. It becomes suffering because of our

41
00:04:41,200 --> 00:04:45,430
 mental illness, because of our greed, our anger, our

42
00:04:45,430 --> 00:04:46,500
 delusion.

43
00:04:46,500 --> 00:04:50,270
 We'll put in another place, it's on the subject of sickness

44
00:04:50,270 --> 00:04:50,500
.

45
00:04:50,500 --> 00:04:58,500
 There's sickness from utu, which means the environment,

46
00:04:58,500 --> 00:05:09,590
 sickness from food, ahaara, sickness from the mind, jitta,

47
00:05:09,590 --> 00:05:19,500
 jeda, and sickness from karma.

48
00:05:19,500 --> 00:05:23,500
 There's four types of sickness.

49
00:05:23,500 --> 00:05:27,630
 So in a little more detail, physical sickness is either

50
00:05:27,630 --> 00:05:33,770
 from the environment or from what is around you or from

51
00:05:33,770 --> 00:05:36,500
 what you take inside of you.

52
00:05:36,500 --> 00:05:43,170
 So things like viruses and radiation and environmental

53
00:05:43,170 --> 00:05:50,010
 distress and the environment, discomfort, these would be ut

54
00:05:50,010 --> 00:05:50,500
u.

55
00:05:50,500 --> 00:05:58,500
 Ahaara from food, what we eat and drink causes us sickness.

56
00:05:58,500 --> 00:06:02,500
 These are the causes of physical sickness.

57
00:06:02,500 --> 00:06:07,540
 Mental sickness caused by the mind or caused by karma,

58
00:06:07,540 --> 00:06:12,360
 which is basically the same thing, but karma means in the

59
00:06:12,360 --> 00:06:13,500
 past.

60
00:06:13,500 --> 00:06:21,500
 So you've done nasty things in the past.

61
00:06:21,500 --> 00:06:29,750
 As a result, you have physical and mental impairment, why

62
00:06:29,750 --> 00:06:37,850
 people are born with certain disabilities, why we suddenly

63
00:06:37,850 --> 00:06:46,280
 get cancer or any debilitating disease, multiple sclerosis,

64
00:06:46,280 --> 00:06:47,500
 etc.

65
00:06:47,500 --> 00:06:53,540
 Not to mention diseases that come from our greed and those

66
00:06:53,540 --> 00:06:57,500
 are more food type.

67
00:06:57,500 --> 00:07:02,370
 But the fourth one from the mind means simply the sickness

68
00:07:02,370 --> 00:07:07,500
 that comes from being mentally sick, from being angry.

69
00:07:07,500 --> 00:07:12,500
 Anytime we're angry, this is considered a mental illness.

70
00:07:12,500 --> 00:07:15,720
 The fact that we have anger is considered to be a mental

71
00:07:15,720 --> 00:07:16,500
 illness.

72
00:07:16,500 --> 00:07:20,800
 When we are addicted to things, when we want things, just

73
00:07:20,800 --> 00:07:24,500
 wanting something is considered to be a mental illness.

74
00:07:24,500 --> 00:07:28,840
 It's a wrong in the mind. It's a cause of suffering for the

75
00:07:28,840 --> 00:07:29,500
 mind.

76
00:07:29,500 --> 00:07:34,500
 It's a cause of stress and distress.

77
00:07:34,500 --> 00:07:43,020
 Anytime we have delusion, arrogance, conceit, or the fact

78
00:07:43,020 --> 00:07:47,100
 that we have these in our mind, this is a cause for illness

79
00:07:47,100 --> 00:07:47,500
.

80
00:07:47,500 --> 00:07:55,500
 It's a type of illness.

81
00:07:55,500 --> 00:08:00,660
 This is the Dhamma of the Buddha. This is how we look at

82
00:08:00,660 --> 00:08:01,500
 things.

83
00:08:01,500 --> 00:08:05,600
 It sounds kind of depressing, I suppose. I've actually had

84
00:08:05,600 --> 00:08:09,020
 people say to me that it's not really fair to call these

85
00:08:09,020 --> 00:08:13,500
 things illness when they're all manageable.

86
00:08:13,500 --> 00:08:16,850
 As though there's a categorical difference between someone

87
00:08:16,850 --> 00:08:19,690
 who is clinically depressed and someone who is just

88
00:08:19,690 --> 00:08:20,500
 depressed.

89
00:08:20,500 --> 00:08:25,870
 There isn't. There's not a categorical difference until you

90
00:08:25,870 --> 00:08:29,500
 create a category because it's just a matter of degree.

91
00:08:29,500 --> 00:08:36,550
 Eventually, the perpetuation, the feedback loop that

92
00:08:36,550 --> 00:08:42,500
 becomes so strong that it becomes unmanageable.

93
00:08:42,500 --> 00:08:45,750
 You dislike something or you're depressed about something

94
00:08:45,750 --> 00:08:49,500
 and that makes you depressed, more depressed and depressed.

95
00:08:49,500 --> 00:08:53,100
 You enter into this feedback loop where you're actually

96
00:08:53,100 --> 00:08:56,500
 feeding the depression with depression.

97
00:08:56,500 --> 00:08:59,200
 You get angry and then you're angry at the fact that

98
00:08:59,200 --> 00:09:00,500
 someone's made you angry.

99
00:09:00,500 --> 00:09:06,500
 You stew in your anger and it becomes unmanageable.

100
00:09:06,500 --> 00:09:11,840
 Anxiety becomes anxious that you're anxious, afraid of your

101
00:09:11,840 --> 00:09:13,500
 fear and so on.

102
00:09:13,500 --> 00:09:22,500
 You just feed them and by feeding them you end up clinical.

103
00:09:22,500 --> 00:09:26,180
 It's just a matter of degree and so certainly something

104
00:09:26,180 --> 00:09:29,830
 that we should be concerned about and we should be with the

105
00:09:29,830 --> 00:09:31,500
 gents towards.

106
00:09:31,500 --> 00:09:45,500
 So it doesn't become unmanageable.

107
00:09:45,500 --> 00:09:53,500
 We have no questions today.

108
00:09:53,500 --> 00:10:03,500
 We have a long argument here.

109
00:10:12,500 --> 00:10:16,860
 Why is entertainment dangerous? Understanding the nature of

110
00:10:16,860 --> 00:10:18,640
 it being impermanent and suffering a non-self, what makes

111
00:10:18,640 --> 00:10:20,500
 it dangerous?

112
00:10:20,500 --> 00:10:30,500
 Attachment.

113
00:10:30,500 --> 00:10:33,500
 That's some good talk, I suppose.

114
00:10:33,500 --> 00:10:37,500
 I'm not going to read through it all. I really think you

115
00:10:37,500 --> 00:10:44,020
 should put the cue before the question otherwise it's hard

116
00:10:44,020 --> 00:10:46,500
 to go through here.

117
00:10:46,500 --> 00:10:52,450
 I'm going to have to insist. I'm not going to go back

118
00:10:52,450 --> 00:10:57,500
 through all that talk to find questions.

119
00:10:57,500 --> 00:11:01,770
 So make a question, click on the yellow or green question

120
00:11:01,770 --> 00:11:05,860
 mark. That puts the right tag before your question then I

121
00:11:05,860 --> 00:11:07,500
 can easily identify.

122
00:11:07,500 --> 00:11:10,910
 Could my unsatisfying lack of progress in meditation, my

123
00:11:10,910 --> 00:11:19,880
 lack of competence in seeing the hindrances be due to kamma

124
00:11:19,880 --> 00:11:20,500
?

125
00:11:20,500 --> 00:11:26,050
 Do you have doubt about your practice? Doubt about your

126
00:11:26,050 --> 00:11:28,500
 progress? Because that's a hindrance.

127
00:11:28,500 --> 00:11:33,500
 You can say doubting, doubting.

128
00:11:33,500 --> 00:11:36,280
 It may be not easy to understand what is the progress but

129
00:11:36,280 --> 00:11:39,180
 if you're practicing correctly it's hard to imagine that

130
00:11:39,180 --> 00:11:40,500
 you're not progressing.

131
00:11:40,500 --> 00:11:55,380
 You're not wanting to see yourself clearly and to refine

132
00:11:55,380 --> 00:12:00,500
 your character.

133
00:12:00,500 --> 00:12:07,180
 How do we face with such continued adversary, violence,

134
00:12:07,180 --> 00:12:12,350
 racism in the world today? How do we teach others to come

135
00:12:12,350 --> 00:12:16,500
 to understand the same benefits of meditation?

136
00:12:16,500 --> 00:12:29,500
 Without seeing unconcerned with our non-reaction.

137
00:12:29,500 --> 00:12:32,920
 If you find that it's considered unjust or cruel not to

138
00:12:32,920 --> 00:12:40,980
 have a fighting opinion when existing in a world that

139
00:12:40,980 --> 00:12:54,500
 demands outrage over every negative situation that arises.

140
00:12:54,500 --> 00:13:00,930
 Well yeah society is messed up, nothing new there. We

141
00:13:00,930 --> 00:13:08,800
 esteem all the wrong things. We esteem addiction for what

142
00:13:08,800 --> 00:13:10,500
 it's worth.

143
00:13:10,500 --> 00:13:20,340
 We certainly esteem opinions, views, we esteem outrage. We

144
00:13:20,340 --> 00:13:26,570
 have esteem for things that in the end don't turn out to be

145
00:13:26,570 --> 00:13:29,500
 all that useful.

146
00:13:29,500 --> 00:13:45,300
 We get caught up in things that end up being inconsequ

147
00:13:45,300 --> 00:13:48,500
ential.

148
00:13:48,500 --> 00:13:55,290
 You see what is fundamental or what is essential as being

149
00:13:55,290 --> 00:14:01,500
 unessential or what is unessential as being essential.

150
00:14:01,500 --> 00:14:23,720
 They don't obtain such people don't come to it as essential

151
00:14:23,720 --> 00:14:29,110
 because they're always feeding on the wrong, feeding in the

152
00:14:29,110 --> 00:14:30,500
 wrong pasture.

153
00:14:30,500 --> 00:14:43,120
 Nor are they. They let their mind wander and minds wander

154
00:14:43,120 --> 00:14:48,500
 in the wrong pastures.

155
00:14:48,500 --> 00:14:59,600
 So to answer your question, it's not easy to teach yourself

156
00:14:59,600 --> 00:15:03,500
 let alone others.

157
00:15:03,500 --> 00:15:07,240
 This is why we focus on our own well-being. You become an

158
00:15:07,240 --> 00:15:10,500
 example to others. People see a better way.

159
00:15:10,500 --> 00:15:18,220
 They see a way to free themselves and to help the world.

160
00:15:18,220 --> 00:15:30,500
 But only if you become an example. So work on yourself.

161
00:15:30,500 --> 00:15:38,270
 If you hold fast, if you hold fast to the fact that you're

162
00:15:38,270 --> 00:15:43,250
 not outraged at things, there will be people who appreciate

163
00:15:43,250 --> 00:15:48,880
 that and who esteem that and who are affected positively by

164
00:15:48,880 --> 00:15:49,500
 that.

165
00:15:49,500 --> 00:15:57,050
 Those people who get angry at you, it says more about them

166
00:15:57,050 --> 00:15:59,500
 than about you.

167
00:15:59,500 --> 00:16:17,500
 Sometimes better to people who are raging, let them rage.

168
00:16:17,500 --> 00:16:20,500
 Yes, but to be patient with them, unmoved.

169
00:16:20,500 --> 00:16:24,140
 It's kind of silly to get arranged about everything when

170
00:16:24,140 --> 00:16:28,010
 this sort of thing, outrageous things have been sort of the

171
00:16:28,010 --> 00:16:29,500
 norm of humanity.

172
00:16:29,500 --> 00:16:37,280
 To be surprised by them is naive. And to fight for that to

173
00:16:37,280 --> 00:16:42,540
 change, it's much better to accept it as part of the human

174
00:16:42,540 --> 00:16:47,510
 condition and to work with it in the sense of actually

175
00:16:47,510 --> 00:16:48,500
 trying to change,

176
00:16:48,500 --> 00:16:52,300
 not getting angry at the fact that that's part of humanity,

177
00:16:52,300 --> 00:16:56,050
 that there's evil in humanity. But to work on good, to

178
00:16:56,050 --> 00:16:59,500
 better yourself, to better those around you,

179
00:16:59,500 --> 00:17:04,810
 to help people to come to let go of their evil inside, help

180
00:17:04,810 --> 00:17:08,370
 people to see that getting angry at bad things is making

181
00:17:08,370 --> 00:17:09,500
 things worse.

182
00:17:09,500 --> 00:17:16,530
 It's just cultivating a habit of anger. You just end up

183
00:17:16,530 --> 00:17:20,500
 burnt out. You don't believe this theory.

184
00:17:20,500 --> 00:17:24,690
 Look at those people who get burnt out from activism, who

185
00:17:24,690 --> 00:17:27,500
 get burnt out from social justice.

186
00:17:27,500 --> 00:17:31,500
 They don't end up helping people, not in any meaningful way

187
00:17:31,500 --> 00:17:33,500
. Well, it's maybe not true.

188
00:17:33,500 --> 00:17:38,210
 They do often end up, but not because of the anger, they

189
00:17:38,210 --> 00:17:42,190
 often end up, because of their determination, they'll end

190
00:17:42,190 --> 00:17:43,500
 up changing the system.

191
00:17:43,500 --> 00:17:48,100
 You don't even be angry to change the system, like some of

192
00:17:48,100 --> 00:17:52,830
 the civil rights movement in America, for example, that we

193
00:17:52,830 --> 00:17:53,500
 hear about.

194
00:17:53,500 --> 00:17:57,910
 A lot of it wasn't very angry. It was a matter of just

195
00:17:57,910 --> 00:18:02,520
 having sit-ins and sitting in the wrong place on the bus

196
00:18:02,520 --> 00:18:04,500
 and that kind of thing.

197
00:18:04,500 --> 00:18:07,610
 Just to be adamant. I mean, you still might argue from a

198
00:18:07,610 --> 00:18:12,500
 Buddhist point of view, but that sort of thing is much more

199
00:18:12,500 --> 00:18:14,500
, much more

200
00:18:14,500 --> 00:18:21,750
 unconscionable, much more acceptable to a Buddhist. The

201
00:18:21,750 --> 00:18:24,930
 Buddha himself gave silent protests. There's this one case

202
00:18:24,930 --> 00:18:27,500
 where this king was going.

203
00:18:27,500 --> 00:18:34,190
 It's a long story. Basically, this guy was a bastard son of

204
00:18:34,190 --> 00:18:39,500
 one of the Buddhist relatives.

205
00:18:39,500 --> 00:18:43,270
 So they wouldn't recognize him, but they didn't want to

206
00:18:43,270 --> 00:18:46,500
 anger him, so they pretended to recognize him.

207
00:18:46,500 --> 00:18:49,390
 In the end, they wouldn't even let him eat at the same

208
00:18:49,390 --> 00:18:51,500
 table with them. They were so proud.

209
00:18:51,500 --> 00:18:59,130
 And this king got so angry that he vowed to kill all the

210
00:18:59,130 --> 00:19:01,500
 Buddhist relatives.

211
00:19:01,500 --> 00:19:05,200
 So he went with his army one time, and the Buddha

212
00:19:05,200 --> 00:19:09,740
 intercepted him, and the Buddha was sitting there under a

213
00:19:09,740 --> 00:19:10,500
 tree.

214
00:19:10,500 --> 00:19:14,150
 But the tree had no leaves, or very few leaves. It was an

215
00:19:14,150 --> 00:19:15,500
 almost dead tree.

216
00:19:15,500 --> 00:19:20,020
 And the king was marching by, and he was told that the

217
00:19:20,020 --> 00:19:24,620
 Buddha was sitting there, and so he got down and went to

218
00:19:24,620 --> 00:19:26,500
 see the Buddha.

219
00:19:26,500 --> 00:19:30,480
 And he said to the Buddha, he actually had some faith in

220
00:19:30,480 --> 00:19:37,500
 the Buddha, even though he was a warmongering king.

221
00:19:37,500 --> 00:19:41,500
 And he said to the Buddha, "What are you doing, Ven.?.

222
00:19:41,500 --> 00:19:44,290
 "I'm sitting under this tree with no shade. Why don't you

223
00:19:44,290 --> 00:19:45,500
 find a better tree?"

224
00:19:45,500 --> 00:19:49,820
 And the Buddha said, "Oh, that's okay. I live under the

225
00:19:49,820 --> 00:19:52,500
 cool shade of my relatives."

226
00:19:52,500 --> 00:19:55,930
 So he wouldn't even go to the king and tell him, "Look, you

227
00:19:55,930 --> 00:19:57,500
're wrong. Stop it."

228
00:19:57,500 --> 00:20:06,460
 Instead, he sought to remind the king that his relatives

229
00:20:06,460 --> 00:20:09,500
 had good in them.

230
00:20:09,500 --> 00:20:16,120
 They may have been pompous jerks, but they were still

231
00:20:16,120 --> 00:20:17,500
 people.

232
00:20:17,500 --> 00:20:21,660
 There was still support to humanity in some way, support to

233
00:20:21,660 --> 00:20:23,500
 the Buddha in some way.

234
00:20:23,500 --> 00:20:32,010
 So the king, he realized how the Buddha benefits from his

235
00:20:32,010 --> 00:20:33,500
 relatives.

236
00:20:33,500 --> 00:20:38,500
 So he turned around, called off the war.

237
00:20:38,500 --> 00:20:41,740
 Except he couldn't keep down his anger, so he ended up

238
00:20:41,740 --> 00:20:43,500
 heading off to war again.

239
00:20:43,500 --> 00:20:46,990
 And again, the Buddha did the same thing, but then the

240
00:20:46,990 --> 00:20:48,500
 third time, it's a really interesting story,

241
00:20:48,500 --> 00:20:51,970
 the third time the king says, "Look, I'm just going to do

242
00:20:51,970 --> 00:20:52,500
 it."

243
00:20:52,500 --> 00:20:56,820
 And the Buddha sort of reflected on the situation and didn

244
00:20:56,820 --> 00:20:57,500
't go.

245
00:20:57,500 --> 00:21:01,500
 He decided it wasn't going to come to a good end.

246
00:21:01,500 --> 00:21:04,500
 There was nothing he could do to stop it.

247
00:21:04,500 --> 00:21:07,500
 In the end, many of the Buddha's relatives were wiped out,

248
00:21:07,500 --> 00:21:10,500
 the majority of them.

249
00:21:10,500 --> 00:21:13,500
 Because the Buddha just let it happen.

250
00:21:13,500 --> 00:21:16,200
 In the end, he felt he couldn't... he would have had to get

251
00:21:16,200 --> 00:21:19,500
 angry, he would have had to get upset.

252
00:21:19,500 --> 00:21:25,500
 In a sense, that's...

253
00:21:25,500 --> 00:21:30,660
 You can't stop people from dying, you can't stop bad things

254
00:21:30,660 --> 00:21:32,500
 from happening.

255
00:21:32,500 --> 00:21:36,500
 What we have to stop is things like anger.

256
00:21:36,500 --> 00:21:41,330
 And so getting angry doesn't help, it sends the wrong

257
00:21:41,330 --> 00:21:42,500
 message.

258
00:21:42,500 --> 00:21:47,000
 Much more important is to send a message, stop killing each

259
00:21:47,000 --> 00:21:47,500
 other.

260
00:21:47,500 --> 00:21:53,690
 To actually try and stop one killing or one catastrophe is

261
00:21:53,690 --> 00:21:55,500
 not all that important.

262
00:21:55,500 --> 00:22:03,180
 These beings are in cycle, they're born and die and born

263
00:22:03,180 --> 00:22:08,500
 and die, it's part of a circle of life.

264
00:22:08,500 --> 00:22:13,890
 But the mind, the mind doesn't die, it dies every moment,

265
00:22:13,890 --> 00:22:24,500
 but the mind of a person keeps going.

266
00:22:24,500 --> 00:22:28,500
 Anyway, sickness.

267
00:22:28,500 --> 00:22:32,500
 All right, we're into questions.

268
00:22:32,500 --> 00:22:34,500
 There's only one or two questions.

269
00:22:34,500 --> 00:22:39,500
 Must have answered all the questions last night.

270
00:22:39,500 --> 00:22:47,500
 But in that case, have a good evening everyone.

271
00:22:47,500 --> 00:22:51,510
 Tomorrow I might be busy, I might not... I should be here,

272
00:22:51,510 --> 00:22:53,500
 I should be here.

273
00:22:53,500 --> 00:22:58,500
 I'm not here some days, my apologies, I should be here.

274
00:22:58,500 --> 00:23:01,500
 Anyway, have a good night.

275
00:23:02,500 --> 00:23:05,500
 Thank you.

276
00:23:06,500 --> 00:23:08,500
 Thank you.

