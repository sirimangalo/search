1
00:00:00,000 --> 00:00:05,110
 After a psychedelic experience I realized the ego is not

2
00:00:05,110 --> 00:00:05,520
 real. I feel this

3
00:00:05,520 --> 00:00:09,670
 has had a lasting impression on me. I have tried to use

4
00:00:09,670 --> 00:00:11,240
 meditation to help me

5
00:00:11,240 --> 00:00:15,010
 stay tethered to this state but I feel myself clinging to

6
00:00:15,010 --> 00:00:16,380
 old patterns. What

7
00:00:16,380 --> 00:00:23,520
 should I do? We should break this up into I guess at least

8
00:00:23,520 --> 00:00:24,960
 let's say two parts. I

9
00:00:24,960 --> 00:00:27,870
 can see two parts here that are essential, two different

10
00:00:27,870 --> 00:00:28,640
 things that you

11
00:00:28,640 --> 00:00:35,000
 have to be able to break up. The Buddha was described as a

12
00:00:35,000 --> 00:00:41,200
 vibhajjavading. What distinguishes the Buddha's teaching is

13
00:00:41,200 --> 00:00:41,480
 his

14
00:00:41,480 --> 00:00:46,480
 ability to to separate realities and not get them muddled.

15
00:00:46,480 --> 00:00:49,440
 Now you say you had a

16
00:00:49,440 --> 00:00:53,360
 realization, now that's momentary, there's a realization

17
00:00:53,360 --> 00:00:54,320
 that the ego is not real,

18
00:00:54,320 --> 00:00:58,020
 then you say this had a lasting impression on you. So

19
00:00:58,020 --> 00:00:59,960
 something resulted

20
00:00:59,960 --> 00:01:03,250
 from that realization. These are two different things. So

21
00:01:03,250 --> 00:01:04,880
 you want to stay

22
00:01:04,880 --> 00:01:10,640
 tethered to the state which for sure is the the second one,

23
00:01:10,640 --> 00:01:12,240
 the latter. This

24
00:01:12,240 --> 00:01:17,360
 usually pleasant, calm, peaceful, something like that,

25
00:01:17,360 --> 00:01:19,520
 something positive, something

26
00:01:19,520 --> 00:01:22,370
 that is actually clung to as a result. It's something that

27
00:01:22,370 --> 00:01:24,120
 is pleasant, it's a

28
00:01:24,120 --> 00:01:28,930
 relief from suffering but it's also impermanent suffering

29
00:01:28,930 --> 00:01:29,920
 and non-self. And

30
00:01:29,920 --> 00:01:33,880
 so when you cling to that you feel unpleasant, you feel

31
00:01:33,880 --> 00:01:35,360
 frustrated, you feel

32
00:01:35,360 --> 00:01:39,210
 discouraged because you can't cling to it because it

33
00:01:39,210 --> 00:01:40,840
 disappears. Now the

34
00:01:40,840 --> 00:01:45,960
 knowledge, well it doesn't really matter, the knowledge

35
00:01:45,960 --> 00:01:46,640
 came and went. The

36
00:01:46,640 --> 00:01:51,720
 realization was something that most likely came from your,

37
00:01:51,720 --> 00:01:51,960
 well you

38
00:01:51,960 --> 00:01:55,770
 say it came from a psychedelic experience, but it was a

39
00:01:55,770 --> 00:01:57,040
 trigger for some

40
00:01:57,040 --> 00:02:02,960
 peaceful, pleasant, powerful state, some state of a state

41
00:02:02,960 --> 00:02:04,080
 that you equate with a

42
00:02:04,080 --> 00:02:10,500
 realization, with an enlightenment of some sort. But that

43
00:02:10,500 --> 00:02:11,880
 state is a positive

44
00:02:11,880 --> 00:02:14,660
 state, meaning positive in the sense that it's something

45
00:02:14,660 --> 00:02:15,800
 that has arisen and

46
00:02:15,800 --> 00:02:20,630
 anything that arises ceases. So that state is not something

47
00:02:20,630 --> 00:02:21,400
 that should be

48
00:02:21,400 --> 00:02:24,600
 clinging to. In fact what you're clinging to, you're not

49
00:02:24,600 --> 00:02:25,560
 just clinging to old

50
00:02:25,560 --> 00:02:28,940
 patterns, you're also clinging to this state. So you're

51
00:02:28,940 --> 00:02:30,880
 trying to stay

52
00:02:30,880 --> 00:02:41,520
 tethered to a state which is wrong, which is a cause for

53
00:02:41,520 --> 00:02:43,080
 clinging. It's something

54
00:02:43,080 --> 00:02:47,420
 that will create more attachment, partiality, you're

55
00:02:47,420 --> 00:02:48,400
 partial to this state.

56
00:02:48,400 --> 00:02:52,720
 So why I'm giving this such a treatment is because this is

57
00:02:52,720 --> 00:02:53,800
 common. It's

58
00:02:53,800 --> 00:02:56,010
 common for people to have these realizations and then say,

59
00:02:56,010 --> 00:02:56,280
 "But I

60
00:02:56,280 --> 00:02:58,730
 couldn't keep it." What do you mean you couldn't keep it? A

61
00:02:58,730 --> 00:03:00,160
 realization is a

62
00:03:00,160 --> 00:03:03,130
 moment. So what you're talking about keeping is this

63
00:03:03,130 --> 00:03:04,800
 powerful state that's

64
00:03:04,800 --> 00:03:11,290
 temporary but comes from a release. It's something that is

65
00:03:11,290 --> 00:03:13,240
 a relief to you and

66
00:03:13,240 --> 00:03:17,110
 so you cling to that and as a result want it to come back.

67
00:03:17,110 --> 00:03:19,080
 Realizations are

68
00:03:19,080 --> 00:03:25,460
 like, you could say, like meals along the way. They rejuven

69
00:03:25,460 --> 00:03:25,880
ate you.

70
00:03:25,880 --> 00:03:28,760
 But it's not the realization that we're looking for. The

71
00:03:28,760 --> 00:03:31,280
 final goal is

72
00:03:31,280 --> 00:03:37,640
 something that isn't clung to, can't be clung to. It's not

73
00:03:37,640 --> 00:03:38,320
 a positive

74
00:03:38,320 --> 00:03:41,550
 state in the sense that it doesn't arise. It's the

75
00:03:41,550 --> 00:03:43,320
 cessation and that's nirvana

76
00:03:43,320 --> 00:03:46,210
 and there will be no clinging. There can be no clinging to

77
00:03:46,210 --> 00:03:47,480
 that. It's something

78
00:03:47,480 --> 00:03:52,700
 different. So be very careful not to cling to the product

79
00:03:52,700 --> 00:03:53,200
 of your

80
00:03:53,200 --> 00:03:56,000
 realizations, these insights that come. Don't cling to them

81
00:03:56,000 --> 00:03:56,760
. It's called jnana.

82
00:03:56,760 --> 00:04:01,600
 When we cling to knowledge, it's an upakiles, it's a defile

83
00:04:01,600 --> 00:04:02,600
ment or a

84
00:04:02,600 --> 00:04:08,160
 detriment to our practice. It keeps us from progressing.

85
00:04:08,160 --> 00:04:18,160
 [BLANK_AUDIO]

