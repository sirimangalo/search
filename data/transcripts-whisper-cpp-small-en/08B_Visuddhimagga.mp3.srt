1
00:00:00,000 --> 00:00:07,150
 Now, suppose you have all five jhanas and also all alubavaj

2
00:00:07,150 --> 00:00:10,160
ara jhanas and also you can

3
00:00:10,160 --> 00:00:15,560
 experience what we call supernormal knowledge or abhinya.

4
00:00:15,560 --> 00:00:19,280
 Abhinya is especially developed

5
00:00:19,280 --> 00:00:27,160
 fifth jhana and abhinyas are those that enable yogis to see

6
00:00:27,160 --> 00:00:30,720
 things far away and to hear sounds

7
00:00:30,720 --> 00:00:36,750
 far away and so on or to perform some miracles. Now,

8
00:00:36,750 --> 00:00:42,080
 suppose you want to hear the sounds far away.

9
00:00:44,080 --> 00:00:49,920
 Now, suppose you have all five jhanas and also all alubavaj

10
00:00:49,920 --> 00:00:53,200
ara jhanas and also you can

11
00:00:53,200 --> 00:00:58,100
 experience what we call supernormal knowledge or abhinya.

12
00:00:58,100 --> 00:01:03,200
 Abhinya is especially developed fifth jhana

13
00:01:04,480 --> 00:01:14,880
 and alubavajara jhana is also a very important part of the

14
00:01:14,880 --> 00:01:16,720
 jhana. So,

15
00:01:16,720 --> 00:01:34,500
 suppose you have all five jhana and also all alubavajara jh

16
00:01:34,500 --> 00:01:36,720
ana is also a very important part of the

17
00:01:36,720 --> 00:01:48,790
 jhana. So, the ten kasinas among these 40 meditation

18
00:01:48,790 --> 00:01:50,960
 subjects need be extended for it is

19
00:01:50,960 --> 00:01:55,910
 within just so much space as one is intent upon. That means

20
00:01:55,910 --> 00:01:59,680
 one covers one extent over with the

21
00:01:59,680 --> 00:02:03,050
 kasinas that one can hear sound with the divine ear element

22
00:02:03,050 --> 00:02:05,760
, see visible objects with the divine eye

23
00:02:05,760 --> 00:02:09,210
 and know the minds of other beings with the mind. So,

24
00:02:09,210 --> 00:02:12,720
 before you experience the

25
00:02:12,720 --> 00:02:19,510
 supernormal knowledge you have to practice, you have to

26
00:02:19,510 --> 00:02:24,800
 extend the sign, the counterpart sign.

27
00:02:26,880 --> 00:02:35,920
 That means you are defining the area within which your

28
00:02:35,920 --> 00:02:38,000
 supernormal knowledge will apply.

29
00:02:38,000 --> 00:02:43,640
 So, they need be extended. Mindfulness occupied with the

30
00:02:43,640 --> 00:02:45,840
 body and the ten kinds of foulness

31
00:02:45,840 --> 00:02:48,730
 need not be extended. So, these subjects need not be

32
00:02:48,730 --> 00:02:51,120
 extended. Why? Because they have a definite

33
00:02:51,120 --> 00:02:59,720
 location. You have to define these things and so you cannot

34
00:02:59,720 --> 00:03:02,080
 extend them and if you extend them

35
00:03:02,080 --> 00:03:05,450
 there is no benefit because there is no benefit in it. The

36
00:03:05,450 --> 00:03:07,280
 definiteness of their location will

37
00:03:07,280 --> 00:03:12,090
 become clear in explaining the method of development. Now,

38
00:03:12,090 --> 00:03:15,040
 when the other comes to explaining

39
00:03:15,040 --> 00:03:20,130
 how to practice meditation on dead bodies and so on and it

40
00:03:20,130 --> 00:03:22,080
 will become clear.

41
00:03:22,080 --> 00:03:25,730
 If the letters are extended it is only a quantity of

42
00:03:25,730 --> 00:03:28,720
 corpses that is extended with regard to foulness

43
00:03:28,720 --> 00:03:34,970
 meditation. You can extend the corpses, I mean in your mind

44
00:03:34,970 --> 00:03:37,520
 corpses, but there is no benefit

45
00:03:38,160 --> 00:03:39,720
 and it is said in answer to the question of Soba Ghar,

46
00:03:39,720 --> 00:03:45,120
 perception of visible forms is quite clear.

47
00:03:45,120 --> 00:03:48,510
 Perception of bones is not clear. For here the perception

48
00:03:48,510 --> 00:03:50,480
 of visible form is called quite clear

49
00:03:50,480 --> 00:03:53,810
 in the sense of extension of the sign. When the perception

50
00:03:53,810 --> 00:03:55,680
 of bones is called not quite clear,

51
00:03:55,680 --> 00:04:00,120
 perception of bones has to do with foulness meditation. In

52
00:04:00,120 --> 00:04:04,240
 the sense it's non-extension.

53
00:04:04,240 --> 00:04:07,830
 For the words, I was intent upon this whole earth with the

54
00:04:07,830 --> 00:04:10,240
 perception of a skeleton that was uttered

55
00:04:10,240 --> 00:04:14,890
 by one elder. Now, they are said in the manner of

56
00:04:14,890 --> 00:04:18,320
 appearance to one who has acquired that

57
00:04:18,320 --> 00:04:23,520
 perception. That means one who has acquired that perception

58
00:04:23,520 --> 00:04:27,840
 before and now he extends this

59
00:04:27,840 --> 00:04:34,250
 perception. So it is all right because he is not practicing

60
00:04:34,250 --> 00:04:37,520
 to get that perception. He has already

61
00:04:37,520 --> 00:04:41,950
 got that perception and so he extends it. For just as in D

62
00:04:41,950 --> 00:04:44,320
hammasoka's time the Karawika

63
00:04:44,320 --> 00:04:47,610
 got uttered his sweet song when it saw its own reflection

64
00:04:47,610 --> 00:04:49,760
 in the looking glass walls all round

65
00:04:49,760 --> 00:04:53,390
 and perceived Karawika in every direction. So the elder

66
00:04:53,390 --> 00:04:56,160
 single up the thoughts when he saw the sign

67
00:04:56,160 --> 00:04:59,220
 appearing in all directions through his acquisition of

68
00:04:59,220 --> 00:05:01,600
 perception of a skeleton that the whole earth

69
00:05:01,600 --> 00:05:05,870
 was covered with bones. And then there's the footnote on

70
00:05:05,870 --> 00:05:08,480
 Karawika but it's interesting but

71
00:05:08,480 --> 00:05:13,950
 it's difficult to believe. It's a kind of word and it is

72
00:05:13,950 --> 00:05:16,960
 said that its sound is very sweet.

73
00:05:17,760 --> 00:05:24,920
 And so the queen asked the community whose voice or whose

74
00:05:24,920 --> 00:05:28,960
 sound is sweet as the community said

75
00:05:28,960 --> 00:05:34,080
 the Karawika birds. So the queen wanted to listen to the

76
00:05:34,080 --> 00:05:38,960
 sound of the Karawika bird and so she asked

77
00:05:40,400 --> 00:05:45,100
 her king, King Asoka, to bring a Karawika bird. So what As

78
00:05:45,100 --> 00:05:49,920
oka did was just hand the cage. The cage flew

79
00:05:49,920 --> 00:05:54,370
 through the air and they lay down near the bird and the

80
00:05:54,370 --> 00:05:57,840
 bird got in the cage and the cage flew back to

81
00:05:57,840 --> 00:06:02,550
 the city and so on. But after reaching the city he would

82
00:06:02,550 --> 00:06:05,920
 not utter his sound because he was depressed.

83
00:06:07,200 --> 00:06:14,890
 So the king asked why the bird did not make any sound. So

84
00:06:14,890 --> 00:06:17,680
 they said because he was lonely.

85
00:06:17,680 --> 00:06:24,200
 If the bird had companions then he would make noise. So the

86
00:06:24,200 --> 00:06:26,720
 king put the mirrors around him

87
00:06:26,720 --> 00:06:30,440
 and so the bird saw his images in the mirror and he saw

88
00:06:30,440 --> 00:06:32,640
 there were other birds. So he was

89
00:06:33,680 --> 00:06:38,110
 happy and so he made a sound. And the queen when he when

90
00:06:38,110 --> 00:06:41,040
 she heard the sound she was so pleased and

91
00:06:41,040 --> 00:06:46,960
 she was very happy and dwelling on that happiness or

92
00:06:46,960 --> 00:06:51,280
 practicing meditation on that happiness

93
00:06:51,280 --> 00:06:57,200
 she practiced with Vasana and she became an extreme winner

94
00:06:57,200 --> 00:07:00,560
 established in the friction of

95
00:07:00,560 --> 00:07:05,650
 string entry that means she became a sword-a-banna. This is

96
00:07:05,650 --> 00:07:08,320
 just an example.

97
00:07:08,320 --> 00:07:13,430
 Just as the Karawika bird saw many other birds in the

98
00:07:13,430 --> 00:07:14,640
 mirror and so

99
00:07:14,640 --> 00:07:21,770
 made sound the elder here when he saw the sign appearing in

100
00:07:21,770 --> 00:07:24,240
 all directions through his

101
00:07:24,240 --> 00:07:28,260
 acquisition of the perception of a skeleton about who was

102
00:07:28,260 --> 00:07:31,040
 covered with bones. So it appeared to him

103
00:07:31,040 --> 00:07:38,590
 as that. It is not that he extended the casino sign. And

104
00:07:38,590 --> 00:07:42,800
 then next paragraph it is if that is so

105
00:07:42,800 --> 00:07:46,600
 then is what is called the measurelessness of the object of

106
00:07:46,600 --> 00:07:49,120
 Jana produced on foulness contradicted.

107
00:07:49,920 --> 00:07:55,920
 Now the the Jana produced on foulness is mentioned as

108
00:07:55,920 --> 00:08:00,640
 measureless or it is mentioned as

109
00:08:00,640 --> 00:08:05,360
 with measure. So is that contradicted then the answer is no

110
00:08:05,360 --> 00:08:05,440
.

111
00:08:05,440 --> 00:08:15,520
 When a person looks at the small corpse then his object is

112
00:08:15,520 --> 00:08:17,040
 said to be

113
00:08:17,680 --> 00:08:22,640
 with measure and if he looks at a big corpse then his

114
00:08:22,640 --> 00:08:26,240
 object is said to be measureless although it

115
00:08:26,240 --> 00:08:31,240
 is not really measureless but measureless here means large

116
00:08:31,240 --> 00:08:34,320
 so the large object and small object.

117
00:08:34,320 --> 00:08:44,140
 And then paragraph 115 as regards the immaterial states as

118
00:08:44,140 --> 00:08:44,800
 objects

119
00:08:46,560 --> 00:08:51,790
 it should read as regards objects of the immaterial states

120
00:08:51,790 --> 00:08:56,000
 not immaterial states and objects

121
00:08:56,000 --> 00:09:02,430
 but objects of the immaterial states. Space need not be

122
00:09:02,430 --> 00:09:06,080
 extended since it is the mere removal of

123
00:09:06,080 --> 00:09:12,000
 the casino. So with regard to the objects of Aruba Vajraj

124
00:09:12,000 --> 00:09:14,960
ana. Now the object of the first

125
00:09:14,960 --> 00:09:20,920
 Aruba Vajrajana is what infinite space so that cannot be

126
00:09:20,920 --> 00:09:25,840
 extended because it is nothing.

127
00:09:25,840 --> 00:09:32,050
 It is obtained through the removal of casino and removal of

128
00:09:32,050 --> 00:09:33,520
 casino means

129
00:09:34,640 --> 00:09:39,650
 not paying attention to the casino sign. First there is

130
00:09:39,650 --> 00:09:42,480
 casino sign in in his mind and then he

131
00:09:42,480 --> 00:09:47,000
 stops paying attention to that casino sign so the casino

132
00:09:47,000 --> 00:09:49,920
 sign disappears and in the place of that

133
00:09:49,920 --> 00:09:53,910
 casino sign just the space remains so that that space is

134
00:09:53,910 --> 00:09:56,320
 space so that cannot be extended.

135
00:10:00,560 --> 00:10:04,790
 If he extends it nothing further happens so nothing will

136
00:10:04,790 --> 00:10:07,440
 happen and consciousness

137
00:10:07,440 --> 00:10:12,090
 need not be extended. Actually consciousness should not be

138
00:10:12,090 --> 00:10:13,760
 or could not be extended since

139
00:10:13,760 --> 00:10:17,050
 it is a state consisting in an individual essence that

140
00:10:17,050 --> 00:10:18,720
 means it is a paramata it is a

141
00:10:18,720 --> 00:10:22,660
 an ultimate reality a reality which has its own

142
00:10:22,660 --> 00:10:26,080
 characteristic or individual essence.

143
00:10:27,520 --> 00:10:32,650
 Only the concept can be extended not the real the ultimate

144
00:10:32,650 --> 00:10:35,200
 reality ultimate reality is just

145
00:10:35,200 --> 00:10:40,610
 ultimate reality and it does not lend itself to to being

146
00:10:40,610 --> 00:10:45,120
 extended. So this consciousness or

147
00:10:45,120 --> 00:10:48,530
 the first consciousness cannot be the first Aruba Vajra

148
00:10:48,530 --> 00:10:50,720
 consciousness cannot be extended

149
00:10:52,080 --> 00:10:56,640
 and the disappearance of consciousness need not be extended

150
00:10:56,640 --> 00:10:58,800
 because actually it is concept and it

151
00:10:58,800 --> 00:11:02,780
 is non-existent so what is non-existent cannot be extended

152
00:11:02,780 --> 00:11:06,480
 and then the last one the base consisting

153
00:11:06,480 --> 00:11:09,800
 of neither perception or non-perception here also the

154
00:11:09,800 --> 00:11:12,080
 object of the base consisting of neither

155
00:11:12,080 --> 00:11:16,330
 perception or non-perception need not be extended since it

156
00:11:16,330 --> 00:11:19,040
 is it true is a state consisting in an

157
00:11:19,040 --> 00:11:22,880
 individual essence. Now do you remember the object of the

158
00:11:22,880 --> 00:11:24,480
 fourth Aruba Vajrajana?

159
00:11:24,480 --> 00:11:33,330
 The object of the fourth Aruba Vajrajana is the third Aruba

160
00:11:33,330 --> 00:11:37,760
 Vajra consciousness. So the third

161
00:11:37,760 --> 00:11:43,050
 Aruba Vajra consciousness is again an ultimate reality

162
00:11:43,050 --> 00:11:45,840
 having its own individual essence so

163
00:11:45,840 --> 00:11:50,860
 it cannot be extended because it is not not a concept so

164
00:11:50,860 --> 00:11:54,240
 they cannot be extended so this is as to

165
00:11:54,240 --> 00:11:57,520
 whether it can be extended or not

166
00:11:57,520 --> 00:12:08,140
 and then as to the object paragraph 117 of these 40

167
00:12:08,140 --> 00:12:10,080
 meditation subjects 22 have

168
00:12:10,080 --> 00:12:18,450
 counterpart sign as object. Now all the charts under the

169
00:12:18,450 --> 00:12:20,080
 column nimitta

170
00:12:20,080 --> 00:12:26,480
 counterpart sign pt means counterpart sign

171
00:12:29,920 --> 00:12:37,750
 so it says 22 have counterpart signs 10 casinos 10 asubas

172
00:12:37,750 --> 00:12:40,800
 of fallenness meditation

173
00:12:40,800 --> 00:12:44,900
 and these two counterpart signs that is to say the 10

174
00:12:44,900 --> 00:12:47,200
 casinos the 10 kinds of fallenness

175
00:12:47,200 --> 00:12:50,630
 mindfulness of breathing and mindfulness occupied with the

176
00:12:50,630 --> 00:12:51,920
 body the rest do not have

177
00:12:51,920 --> 00:12:55,820
 counterpart signs as object then 12 have states consisting

178
00:12:55,820 --> 00:12:57,840
 an individual essence as object

179
00:12:58,800 --> 00:13:01,820
 that is to say eight of the 10 recollections except

180
00:13:01,820 --> 00:13:04,720
 mindfulness of breathing and mindfulness

181
00:13:04,720 --> 00:13:07,340
 occupied with the body the perception of the pastness and

182
00:13:07,340 --> 00:13:08,880
 nutriment the defining of the four

183
00:13:08,880 --> 00:13:12,390
 elements the base consisting of boundless consciousness and

184
00:13:12,390 --> 00:13:13,920
 the base consisting neither

185
00:13:13,920 --> 00:13:20,040
 perception or non-perception so they have the the ultimate

186
00:13:20,040 --> 00:13:22,640
 reality as object

187
00:13:25,520 --> 00:13:30,000
 and the 22 have counterpart signs as object that is to say

188
00:13:30,000 --> 00:13:32,080
 the 10 casinos the 10 kinds of fallenness

189
00:13:32,080 --> 00:13:34,980
 mindfulness of breathing and mindfulness of mindfulness

190
00:13:34,980 --> 00:13:36,480
 occupied with the body while the

191
00:13:36,480 --> 00:13:42,000
 remaining six have not so classifiable objects so these are

192
00:13:42,000 --> 00:13:44,720
 the description of the objects of

193
00:13:44,720 --> 00:13:49,230
 meditation in different ways and the age of mobile objects

194
00:13:49,230 --> 00:13:52,160
 in the early states through the counterpart

195
00:13:53,920 --> 00:13:59,200
 and though the counterpart is stationary that is to say the

196
00:13:59,200 --> 00:14:01,760
 festering the bleeding the warm

197
00:14:01,760 --> 00:14:05,050
 fester mindfulness of breathing and water signal the fire g

198
00:14:05,050 --> 00:14:07,040
azina the air gazina and in the case of

199
00:14:07,040 --> 00:14:10,790
 light gazina the object consisting of a circle of sunlight

200
00:14:10,790 --> 00:14:13,440
 etc so they are shaking objects they can

201
00:14:13,440 --> 00:14:18,280
 be shaking objects the rest have mobile objects now they

202
00:14:18,280 --> 00:14:23,440
 have shaking objects only in the preliminary

203
00:14:23,440 --> 00:14:26,700
 stage but when when the yogi reaches the counterpart uh

204
00:14:26,700 --> 00:14:31,040
 counterpart sign stage then they are stationary

205
00:14:31,040 --> 00:14:35,680
 so it is only in the preliminary stage that they and they

206
00:14:35,680 --> 00:14:38,880
 have shaking objects or mobile objects

207
00:14:38,880 --> 00:14:46,470
 and as to the plane that means as to the different uh 31

208
00:14:46,470 --> 00:14:49,760
 planes of existence 12 namely the 10 kinds

209
00:14:49,760 --> 00:14:52,080
 of fallenness mindfulness occupied with the body and

210
00:14:52,080 --> 00:14:54,400
 perception of repulsiveness in nutriment

211
00:14:54,400 --> 00:14:59,820
 do not occur among deities that mean that in the fourth jh

212
00:14:59,820 --> 00:15:02,640
ana the breathing stops

213
00:15:02,640 --> 00:15:07,440
 that's right yes you caught the bite

214
00:15:07,440 --> 00:15:15,360
 yes

215
00:15:17,920 --> 00:15:23,740
 we will come to that in the description of the breathing

216
00:15:23,740 --> 00:15:28,080
 breathing meditation so that's right

217
00:15:28,080 --> 00:15:36,790
 and then as to apprehending that means as to taking the

218
00:15:36,790 --> 00:15:41,120
 object by by sight by by hearing by

219
00:15:41,120 --> 00:15:46,890
 seeing and so on so here according to sight touch and hears

220
00:15:46,890 --> 00:15:50,240
ay and hearsay means just hearing

221
00:15:50,240 --> 00:15:54,860
 something and these 19 that is to say nine casinos omitting

222
00:15:54,860 --> 00:15:56,560
 the air gazina and the 10

223
00:15:56,560 --> 00:15:59,880
 kinds of fallenness must be apprehended by sight so that

224
00:15:59,880 --> 00:16:02,080
 means you look at something and practice

225
00:16:02,080 --> 00:16:05,610
 meditation the meaning is that in the early stage the

226
00:16:05,610 --> 00:16:08,480
 assigned must be apprehended by constantly

227
00:16:08,480 --> 00:16:11,860
 looking with the eye in the case of mindfulness occupied

228
00:16:11,860 --> 00:16:14,320
 with the body the five parts ending with

229
00:16:14,320 --> 00:16:19,840
 skin must be apprehended by sight and the rest by hearsay

230
00:16:19,840 --> 00:16:22,800
 head hair body hair nails teeth skin

231
00:16:22,800 --> 00:16:27,560
 these you look at with your eyes and practice meditation on

232
00:16:27,560 --> 00:16:28,000
 them

233
00:16:30,480 --> 00:16:35,740
 and the others some some you cannot see like liver or

234
00:16:35,740 --> 00:16:38,880
 intestines and other things so that

235
00:16:38,880 --> 00:16:41,200
 you practice through hearsay

236
00:16:41,200 --> 00:16:48,330
 mindfulness of breathing must be apprehended by touch so

237
00:16:48,330 --> 00:16:49,440
 when you practice mindfulness

238
00:16:49,440 --> 00:16:53,260
 meditation you keep your mind here and be mindful of the

239
00:16:53,260 --> 00:16:55,280
 the sensation of touch here

240
00:16:56,000 --> 00:17:03,520
 with the air going going in and out of the nostrils the air

241
00:17:03,520 --> 00:17:05,760
 casino by sight and touch

242
00:17:05,760 --> 00:17:10,120
 it will become clearer when we come to the description of

243
00:17:10,120 --> 00:17:12,240
 how to practice air casino

244
00:17:12,240 --> 00:17:18,880
 sometimes you look at something moving say branches of tree

245
00:17:18,880 --> 00:17:23,440
 or a banner in the wind and you

246
00:17:24,560 --> 00:17:30,320
 and you practice air casino on that but sometimes the wind

247
00:17:30,320 --> 00:17:32,880
 is blowing against your body and you have

248
00:17:32,880 --> 00:17:36,670
 the feeling of touch here and so do you concentrate on this

249
00:17:36,670 --> 00:17:40,320
 and concentrate on the air element here

250
00:17:40,320 --> 00:17:45,230
 so in that case you practice by the sense of touch the

251
00:17:45,230 --> 00:17:49,120
 remaining 18 by hearsay that means just by

252
00:17:50,560 --> 00:17:54,710
 hearing the divine abiding of equanimity and the four imm

253
00:17:54,710 --> 00:17:56,160
aterial states are not

254
00:17:56,160 --> 00:17:59,520
 apprehendable by a beginner so you cannot practice

255
00:17:59,520 --> 00:18:06,790
 uhupika and the four arugavajarajanas at the beginning

256
00:18:06,790 --> 00:18:11,120
 because in order to get arugavajarajanas

257
00:18:11,120 --> 00:18:14,920
 you must have you must you must have got the five rupavajar

258
00:18:14,920 --> 00:18:18,320
ajanas and in order to practice

259
00:18:19,520 --> 00:18:26,140
 real upika upika brahma vihara then you have to like you

260
00:18:26,140 --> 00:18:28,800
 have to have practiced the first three

261
00:18:28,800 --> 00:18:37,560
 loving kindness confession and sympathetic joy so as a as a

262
00:18:37,560 --> 00:18:42,000
 real divine abiding equanimity cannot

263
00:18:42,000 --> 00:18:45,590
 be practiced at the beginning only after you have practiced

264
00:18:45,590 --> 00:18:47,520
 the other three can you practice

265
00:18:47,520 --> 00:18:51,280
 equanimity and then as to condition

266
00:18:51,280 --> 00:19:00,250
 the 10 casinos i mean nine casinos omitting the space

267
00:19:00,250 --> 00:19:03,440
 casino are conditions for immaterial states

268
00:19:03,440 --> 00:19:08,180
 that means if you want to get arugavajarajanas and you

269
00:19:08,180 --> 00:19:08,640
 practice

270
00:19:08,640 --> 00:19:15,010
 one of the nine casinos omitting the space casino because

271
00:19:15,010 --> 00:19:16,080
 you have to practice

272
00:19:18,080 --> 00:19:21,980
 the removing of the casino object and get the space so

273
00:19:21,980 --> 00:19:26,080
 space cannot be removed the space is

274
00:19:26,080 --> 00:19:31,210
 space so the space casino is exempted from from those that

275
00:19:31,210 --> 00:19:33,920
 are conditions for immaterial

276
00:19:33,920 --> 00:19:38,160
 states or arugavajarajanas the 10 casinos are conditions

277
00:19:38,160 --> 00:19:40,480
 for the kinds of direct knowledge

278
00:19:42,640 --> 00:19:48,600
 so if you want to get the direct knowledge or abhinia that

279
00:19:48,600 --> 00:19:51,920
 means supernormal power then you

280
00:19:51,920 --> 00:19:57,000
 practice first the one of the 10 casinos and actually if

281
00:19:57,000 --> 00:20:00,800
 you want to get different different

282
00:20:01,440 --> 00:20:09,590
 results then you you practice different different casinos

283
00:20:09,590 --> 00:20:16,880
 you know if you want to shake something

284
00:20:16,880 --> 00:20:23,480
 suppose you want to shake the shake the the city hall

285
00:20:23,480 --> 00:20:28,640
 building by your supernormal power then first

286
00:20:29,760 --> 00:20:35,950
 you you must practice water casino or air casino but not

287
00:20:35,950 --> 00:20:39,840
 the the earth casino if you practice earth

288
00:20:39,840 --> 00:20:44,500
 casino and try to shake it you it will not shake there is a

289
00:20:44,500 --> 00:20:47,520
 story of a novice who went up to heaven

290
00:20:47,520 --> 00:20:52,560
 i mean the abode of god and he said i will i will shake

291
00:20:52,560 --> 00:20:56,000
 your mansion and he he tried to shake it

292
00:20:56,000 --> 00:21:01,450
 and he could not so he so the the celestial name made fun

293
00:21:01,450 --> 00:21:04,720
 of him and so he was ashamed and he went

294
00:21:04,720 --> 00:21:08,260
 back to his teacher and he told his teacher that he was

295
00:21:08,260 --> 00:21:10,880
 ashamed by the name so because he he could

296
00:21:10,880 --> 00:21:15,740
 not shake the shake their mansion and he asked his teacher

297
00:21:15,740 --> 00:21:20,000
 why so his teacher said look at something

298
00:21:20,000 --> 00:21:25,160
 there so a a cow dung was floating in the in the river so

299
00:21:25,160 --> 00:21:28,720
 he got the hint and so next time he went

300
00:21:28,720 --> 00:21:33,660
 he went back to the the celestial abode and this time he he

301
00:21:33,660 --> 00:21:36,080
 practiced water magic i mean

302
00:21:36,080 --> 00:21:42,400
 water casino first and then maybe maybe mansion shake so

303
00:21:42,400 --> 00:21:45,920
 according to what you want from

304
00:21:46,880 --> 00:21:49,750
 from the casinos you have been practiced different kinds of

305
00:21:49,750 --> 00:21:51,440
 casinos and the idea mentioned in the

306
00:21:51,440 --> 00:21:56,710
 later chapters so the ten casinos are conditioned for the

307
00:21:56,710 --> 00:22:00,320
 kinds of direct knowledge three divine

308
00:22:00,320 --> 00:22:03,250
 abidings and conditions for the fourth divine abiding so

309
00:22:03,250 --> 00:22:05,200
 the fourth divine abiding cannot be

310
00:22:05,200 --> 00:22:09,140
 practiced at the beginning but only after the first three

311
00:22:09,140 --> 00:22:11,760
 each lower immaterial state is a

312
00:22:11,760 --> 00:22:14,420
 condition for each higher one the base consisting of

313
00:22:14,420 --> 00:22:16,800
 neither perception or non-perception is a

314
00:22:16,800 --> 00:22:20,150
 condition for the attainment of cessation that means

315
00:22:20,150 --> 00:22:22,240
 cessation of mental activities

316
00:22:22,240 --> 00:22:30,890
 cessation of perception and feeling actually cessation of

317
00:22:30,890 --> 00:22:32,320
 mental activities

318
00:22:32,320 --> 00:22:36,420
 all are conditions for living in bliss that means living in

319
00:22:36,420 --> 00:22:40,480
 bliss in this very life for insight and

320
00:22:40,480 --> 00:22:45,450
 for the fortunate kinds of becoming that means for a good

321
00:22:45,450 --> 00:22:48,000
 life in the future as to suitability

322
00:22:48,000 --> 00:22:50,830
 to temperament and they are important here the exposition

323
00:22:50,830 --> 00:22:52,800
 should be understood according to what

324
00:22:52,800 --> 00:22:58,520
 is suitable to the temperament and then describes which

325
00:22:58,520 --> 00:23:02,080
 subjects of meditation are suitable for

326
00:23:02,080 --> 00:23:12,880
 which kind of temperament

327
00:23:24,800 --> 00:23:34,390
 right but the the normal procedure is to practice the

328
00:23:34,390 --> 00:23:39,040
 loving kindness first and then practice the

329
00:23:39,040 --> 00:23:43,270
 second one and the third one and this this this this means

330
00:23:43,270 --> 00:23:46,720
 you practice so that you get jhana

331
00:23:46,720 --> 00:23:52,020
 from this practice if you do not get to the to the state

332
00:23:52,020 --> 00:23:56,080
 state of jhana then even equanimity you can

333
00:23:56,080 --> 00:24:04,280
 practice but here it is meant for jhana because the equanim

334
00:24:04,280 --> 00:24:09,200
ity leads to the fifth jhana so in order

335
00:24:09,200 --> 00:24:12,730
 to get the fifth jhana you need to have the first second

336
00:24:12,730 --> 00:24:15,120
 that and fourth jhana and those can be

337
00:24:15,840 --> 00:24:19,370
 obtained through the practice of the the lower three on the

338
00:24:19,370 --> 00:24:21,760
 other three divine abiding

339
00:24:21,760 --> 00:24:28,000
 you you reach jhana but not not to the fifth jhana you read

340
00:24:28,000 --> 00:24:29,280
 to the fourth jhana only

341
00:24:29,280 --> 00:24:35,520
 and by the practice of equanimity you you reach the fifth j

342
00:24:35,520 --> 00:24:35,840
hana

343
00:24:39,920 --> 00:24:46,460
 and now the different kinds of meditation suitable for

344
00:24:46,460 --> 00:24:49,280
 different types of temperaments

345
00:24:49,280 --> 00:24:53,680
 and all this has been stated in the form of direct

346
00:24:53,680 --> 00:24:57,280
 opposition and complete suitability

347
00:24:57,280 --> 00:25:01,860
 that means if if it says this this meditation is suitable

348
00:25:01,860 --> 00:25:05,040
 for this temperament it means that

349
00:25:05,840 --> 00:25:09,220
 this meditation is the direct opposite of that temperament

350
00:25:09,220 --> 00:25:11,840
 and it is very suitable for it but

351
00:25:11,840 --> 00:25:16,340
 it does not mean that you cannot practice other meditation

352
00:25:16,340 --> 00:25:18,560
 so but there is actually no

353
00:25:18,560 --> 00:25:22,570
 profitable development that does not suppress greed etc and

354
00:25:22,570 --> 00:25:25,840
 help faith and so on so in fact

355
00:25:25,840 --> 00:25:31,570
 you can practice any meditation but here the meditations

356
00:25:31,570 --> 00:25:34,800
 subjects of meditation and temperaments

357
00:25:34,800 --> 00:25:41,230
 are given to show that the they are the the direct opposite

358
00:25:41,230 --> 00:25:45,040
 of the temperament and they are the most

359
00:25:45,040 --> 00:25:53,780
 suitable suppose i am of diluted temperament then the most

360
00:25:53,780 --> 00:25:57,520
 suitable meditation for me is

361
00:25:57,520 --> 00:26:01,520
 breathing meditation but that does not mean that i cannot

362
00:26:01,520 --> 00:26:03,760
 practice any other meditation

363
00:26:05,280 --> 00:26:09,600
 because any meditation will help me to to suppress mental

364
00:26:09,600 --> 00:26:12,480
 defilements and to develop

365
00:26:12,480 --> 00:26:15,920
 wholesome mental states

366
00:26:15,920 --> 00:26:22,160
 even in the sodas bumbura was advising rahula and other

367
00:26:22,160 --> 00:26:26,480
 persons to practice different kinds

368
00:26:26,480 --> 00:26:29,790
 of meditation not only one meditation but different kinds

369
00:26:29,790 --> 00:26:32,000
 of meditation to one and only person so

370
00:26:33,440 --> 00:26:37,590
 any meditation can be practiced by anyone but if you want

371
00:26:37,590 --> 00:26:40,480
 to get the best out of it then you choose

372
00:26:40,480 --> 00:26:44,480
 the one which is most suitable for your temperament

373
00:26:44,480 --> 00:26:54,240
 all casinos as object imply seeing consciousness before

374
00:26:56,400 --> 00:27:04,620
 as i show process yes because see casinos should be

375
00:27:04,620 --> 00:27:08,800
 practiced first by looking at them so when you

376
00:27:08,800 --> 00:27:12,180
 look at them then you have to be seeing consciousness and

377
00:27:12,180 --> 00:27:14,400
 then you try to memorize or you try to

378
00:27:14,400 --> 00:27:18,730
 to take it into your mind that means you you you close your

379
00:27:18,730 --> 00:27:20,880
 eyes and try to take that off

380
00:27:20,880 --> 00:27:25,860
 the the image so when you can get the image clearly in your

381
00:27:25,860 --> 00:27:27,600
 mind then you are said to get

382
00:27:27,600 --> 00:27:33,120
 and the the what sign

383
00:27:33,120 --> 00:27:40,640
 learning they call learning sign actually the cross sign

384
00:27:43,200 --> 00:27:47,980
 and after you you you get the learning sign and you don't

385
00:27:47,980 --> 00:27:50,960
 you you no longer need the actual

386
00:27:50,960 --> 00:27:57,740
 actual disc actual object but you develop you you dwell on

387
00:27:57,740 --> 00:28:02,160
 the on the sign you you get in your mind

388
00:28:02,160 --> 00:28:06,460
 so at that moment at that time on it is not seeing

389
00:28:06,460 --> 00:28:10,960
 consciousness but the the other

390
00:28:11,680 --> 00:28:18,340
 manora i mean through mind or not through not through eye

391
00:28:18,340 --> 00:28:21,920
 door so first first through eye door

392
00:28:21,920 --> 00:28:26,370
 you look at the the casino and practice meditation and

393
00:28:26,370 --> 00:28:30,720
 after you get the learning sign then your

394
00:28:30,720 --> 00:28:39,310
 meditation is through mind door you see through mind but

395
00:28:39,310 --> 00:28:43,920
 not through the eye and then dedicating

396
00:28:43,920 --> 00:28:48,800
 oneself to the blessed one or to the teacher that means

397
00:28:48,800 --> 00:28:52,480
 giving or rather than quishing oneself

398
00:28:52,480 --> 00:28:58,750
 and to them do do to the blessed one or to the voter it's

399
00:28:58,750 --> 00:28:59,280
 all right but to the teacher

400
00:29:01,040 --> 00:29:02,080
 i do not recommend

401
00:29:02,080 --> 00:29:06,400
 because

402
00:29:06,400 --> 00:29:12,400
 not not all teachers are to be trusted

403
00:29:12,400 --> 00:29:19,890
 considering what's happening these days so on the on phase

404
00:29:19,890 --> 00:29:24,880
 119 said when he dedicates himself

405
00:29:24,880 --> 00:29:29,300
 to a teacher i run and question this my person to you so i

406
00:29:29,300 --> 00:29:31,600
 give up i give myself up to you

407
00:29:31,600 --> 00:29:37,360
 it it may be dangerous if if a teacher has

408
00:29:37,360 --> 00:29:43,820
 what called arterial motives so it is better to to give

409
00:29:43,820 --> 00:29:47,360
 yourself to the Buddha not to the teacher

410
00:29:48,720 --> 00:29:55,280
 these days okay and then sincere

411
00:29:55,280 --> 00:30:01,220
 inclination and resolution i think they are not so

412
00:30:01,220 --> 00:30:03,440
 difficult now

413
00:30:03,440 --> 00:30:12,610
 in paragraph 128 about four lines for it is one of such

414
00:30:12,610 --> 00:30:16,080
 sincere inclination which arrives at

415
00:30:16,080 --> 00:30:19,210
 one of the three kinds of enlightenment that means

416
00:30:19,210 --> 00:30:20,880
 enlightenment as a buddha

417
00:30:20,880 --> 00:30:26,760
 enlightenment as a bhachigabhuda and enlightenment as an a

418
00:30:26,760 --> 00:30:28,800
rahant so these are three kinds of

419
00:30:28,800 --> 00:30:32,890
 enlightenment and then six kinds of inclination lead to the

420
00:30:32,890 --> 00:30:35,360
 maturing of the enlightenment of the

421
00:30:35,360 --> 00:30:39,590
 bodhisattvas these may be something like a parameter as

422
00:30:39,590 --> 00:30:40,960
 found in mahayana

423
00:30:44,560 --> 00:30:49,190
 is non-greet non-delusion and so on these are the six incl

424
00:30:49,190 --> 00:30:51,440
inations i mean six

425
00:30:51,440 --> 00:30:55,300
 i'd give you six qualities which was bodhisattvas

426
00:30:55,300 --> 00:31:01,520
 especially develop so with the inclination to

427
00:31:01,520 --> 00:31:05,760
 non-greet bodhisattvas see the fruit see the fourth in gree

428
00:31:05,760 --> 00:31:08,160
 with the inclination to non-hate

429
00:31:08,160 --> 00:31:13,670
 bodhisattvas see the fourth in hate and so on they are not

430
00:31:13,670 --> 00:31:16,720
 found although it is a quotation

431
00:31:16,720 --> 00:31:22,240
 we cannot trace this condition to any any text available

432
00:31:22,240 --> 00:31:24,960
 nowadays so some some text may have

433
00:31:24,960 --> 00:31:30,780
 been lost or it may refer to some some other some sort does

434
00:31:30,780 --> 00:31:34,000
 not belong into thera-wara

435
00:31:37,920 --> 00:31:45,600
 and then the last paragraph 132 apprehend the sign now the

436
00:31:45,600 --> 00:31:49,120
 in in pali the word nimitta is used

437
00:31:49,120 --> 00:31:53,400
 in different different meanings so here apprehend the sign

438
00:31:53,400 --> 00:31:56,080
 means just paying paying close attention

439
00:31:56,080 --> 00:31:59,250
 to what you hear from the teacher so this is the preceding

440
00:31:59,250 --> 00:32:01,520
 clause this is the subsequent clause

441
00:32:01,520 --> 00:32:05,150
 this is the meaning this is the intention this is the sim

442
00:32:05,150 --> 00:32:07,360
ile and so on so paying close attention

443
00:32:07,360 --> 00:32:11,790
 and trying to understand first hear the words of the the

444
00:32:11,790 --> 00:32:14,640
 the teacher and then trying to understand

445
00:32:14,640 --> 00:32:18,160
 it is called here apprehending the sign so it is not like

446
00:32:18,160 --> 00:32:20,480
 apprehending the sign when you practice

447
00:32:20,480 --> 00:32:24,160
 meditation so apprehending of sign will come later in

448
00:32:24,160 --> 00:32:28,480
 chapter chapter four so here apprehending the

449
00:32:28,480 --> 00:32:34,290
 sign means just paying close attention to to what the

450
00:32:34,290 --> 00:32:38,640
 teacher said so this is preceding clause this

451
00:32:38,640 --> 00:32:41,770
 is subsequent clause this is its meaning and so on when he

452
00:32:41,770 --> 00:32:44,000
 listens attentively apprehending the

453
00:32:44,000 --> 00:32:48,390
 sign in this way his meditation subject is well apprehended

454
00:32:48,390 --> 00:32:50,800
 so he knows he knows what he he

455
00:32:52,160 --> 00:32:55,690
 what he should know about meditation and and because of

456
00:32:55,690 --> 00:32:58,880
 that he successfully attains distinction

457
00:32:58,880 --> 00:33:03,750
 attains distinction means attains jhanas attains super

458
00:33:03,750 --> 00:33:06,480
normal knowledge and attains

459
00:33:06,480 --> 00:33:13,360
 enlightenment but not others not otherwise but not others

460
00:33:13,360 --> 00:33:18,880
 he will successfully attain distinction

461
00:33:18,880 --> 00:33:23,020
 distinction but not others who do not listen attentively

462
00:33:23,020 --> 00:33:25,840
 and apprehend the sign and so on so

463
00:33:25,840 --> 00:33:32,470
 otherwise should be corrected to others not otherwise on

464
00:33:32,470 --> 00:33:35,600
 page 121 first line

465
00:33:35,600 --> 00:33:43,170
 he successfully attains distinction but not others on this

466
00:33:43,170 --> 00:33:43,760
 page

467
00:33:47,040 --> 00:33:51,970
 that is the end of that chapter so we are we are still

468
00:33:51,970 --> 00:33:53,200
 preparing

469
00:33:53,200 --> 00:34:06,350
 preparing is not not not over yet you have to you have to

470
00:34:06,350 --> 00:34:07,520
 avoid 18

471
00:34:07,520 --> 00:34:11,800
 faulty monasteries and also find a suitable place for

472
00:34:11,800 --> 00:34:12,800
 meditation

473
00:34:12,800 --> 00:34:26,720
 it's interesting about the parmitas it almost works it

474
00:34:26,720 --> 00:34:29,040
 works for four of them and the other

475
00:34:29,040 --> 00:34:36,970
 two it's a little bit of a stretch but yeah interesting

476
00:34:36,970 --> 00:34:41,760
 thank you very much we're going to

477
00:34:41,760 --> 00:34:46,800
 continue uh in march sometime in march we'll do another 12

478
00:34:46,800 --> 00:34:50,240
 weeks and that will go from we hope

479
00:34:50,720 --> 00:34:59,360
 from chapter four chapter eight chapter three chapter eight

480
00:34:59,360 --> 00:35:10,000
 yes

