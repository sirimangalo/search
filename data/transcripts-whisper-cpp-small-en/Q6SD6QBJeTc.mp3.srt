1
00:00:00,000 --> 00:00:04,450
 Will asks, "Is clear comprehension the same as direct

2
00:00:04,450 --> 00:00:07,320
 understanding? How do they relate to mindfulness?"

3
00:00:07,320 --> 00:00:13,560
 These are just words. It's very difficult to give an answer

4
00:00:13,560 --> 00:00:21,000
. But if I translate these directly,

5
00:00:21,000 --> 00:00:25,160
 then clear comprehension... Well, clear comprehension is

6
00:00:25,160 --> 00:00:28,240
 often used as a translation of sampajanya.

7
00:00:28,240 --> 00:00:34,560
 But sampajanya doesn't exactly mean clear comprehension.

8
00:00:34,560 --> 00:00:46,180
 Sam means full, complete or right. And bajjanya means full.

9
00:00:46,180 --> 00:00:47,920
 It also means full, no?

10
00:00:47,920 --> 00:00:54,920
 And janya means knowledge. So sampajanya means full and

11
00:00:54,920 --> 00:00:57,480
 clear, full and right knowledge.

12
00:00:57,480 --> 00:00:59,670
 Direct understanding could be a translation of yata bhuta y

13
00:00:59,670 --> 00:01:06,040
ana dasana,

14
00:01:06,040 --> 00:01:11,180
 which means knowledge and vision of things as they are. It

15
00:01:11,180 --> 00:01:14,200
 could also be a translation of

16
00:01:14,200 --> 00:01:17,810
 pañjanati, which means full understanding or full

17
00:01:17,810 --> 00:01:23,160
 knowledge. But yeah, direct understanding

18
00:01:23,160 --> 00:01:31,390
 could be a translation of sacheek karana, the making,

19
00:01:31,390 --> 00:01:37,160
 seeing something for yourself or so on.

20
00:01:37,160 --> 00:01:46,030
 Both of those first two are, I would say, synonyms for

21
00:01:46,030 --> 00:01:50,960
 wisdom, synonyms for knowledge of things as

22
00:01:50,960 --> 00:01:59,160
 they are. And so are proper to be said as being the same.

23
00:01:59,160 --> 00:02:00,680
 So I mean, it's hard when you're using

24
00:02:00,680 --> 00:02:05,350
 English words. What does the word comprehension mean? It

25
00:02:05,350 --> 00:02:08,320
 does mean very much understanding,

26
00:02:08,320 --> 00:02:11,330
 and I don't think that there's much difference that can be

27
00:02:11,330 --> 00:02:12,880
 gained. Unless you talk about the

28
00:02:12,880 --> 00:02:16,090
 four types of sampajanya, and then you see that sampajanya

29
00:02:16,090 --> 00:02:20,040
 has a specific meaning. There's

30
00:02:20,040 --> 00:02:25,900
 gochara sampajanya, and so on. I can't remember the more.

31
00:02:25,900 --> 00:02:28,560
 Asamuha sampajanya is the one that

32
00:02:28,560 --> 00:02:32,160
 relates to mindfulness. Then you have the word mindfulness,

33
00:02:32,160 --> 00:02:33,640
 which is actually a difficult word.

34
00:02:33,640 --> 00:02:37,850
 And mindfulness is actually probably a fairly good

35
00:02:37,850 --> 00:02:42,360
 translation of sampajanya as well, even though we

36
00:02:42,360 --> 00:02:45,870
 use it as a translation for sati. Because sampajanya, again

37
00:02:45,870 --> 00:02:48,080
, is full, this idea of being full,

38
00:02:48,080 --> 00:02:53,390
 having a full awareness or knowledge, or knowledge of

39
00:02:53,390 --> 00:02:56,600
 something fully and rightly.

40
00:02:56,600 --> 00:03:05,290
 But how they relate to sati, they are very often conjoined

41
00:03:05,290 --> 00:03:07,160
 in the Buddha's teaching. The Buddha

42
00:03:07,160 --> 00:03:12,240
 will often talk about sampajanya as being connected. But

43
00:03:12,240 --> 00:03:14,120
 sati is like the taking hold,

44
00:03:14,120 --> 00:03:21,090
 and sampajanya is like the cutting, or the cutting away of

45
00:03:21,090 --> 00:03:24,080
 when you grab hold of rice,

46
00:03:24,080 --> 00:03:26,430
 and then you cut with a knife, you grab hold of it, and

47
00:03:26,430 --> 00:03:29,840
 then you cut. So they perform a different

48
00:03:29,840 --> 00:03:33,650
 function. The mindfulness, or the sati, is the recognition

49
00:03:33,650 --> 00:03:37,840
 of the object, the act of cognizing

50
00:03:37,840 --> 00:03:41,770
 the object rightly. And the sampajanya is what results,

51
00:03:41,770 --> 00:03:44,960
 which is the right comprehension. So

52
00:03:44,960 --> 00:03:48,780
 sati is the action that you perform. Sampajanya is the

53
00:03:48,780 --> 00:03:51,560
 result, the wisdom that results, or that

54
00:03:51,560 --> 00:03:55,160
 should result, but they have to work together. And you can

55
00:03:55,160 --> 00:03:57,520
't just use the noting, for example,

56
00:03:57,520 --> 00:03:59,880
 and not really know what's going on. But you can't just

57
00:03:59,880 --> 00:04:01,560
 know what's going on without having a firm

58
00:04:01,560 --> 00:04:05,600
 grasp of it, or else it will give rise to diversification.

59
00:04:05,600 --> 00:04:07,360
 So if you're talking about sati

60
00:04:07,360 --> 00:04:10,450
 and sampajanya, this is their relationship. Sati is the

61
00:04:10,450 --> 00:04:12,080
 grasping so that the mind doesn't

62
00:04:12,080 --> 00:04:15,190
 waver from the knowing, and sampajanya is that knowing

63
00:04:15,190 --> 00:04:18,000
 itself. And you need both of them. Without

64
00:04:18,000 --> 00:04:21,010
 the knowing, it's just grasping, and with just

65
00:04:21,010 --> 00:04:23,640
 concentration, saying to yourself, pain, pain,

66
00:04:23,640 --> 00:04:26,680
 pain, for example, without really focusing on the pain or

67
00:04:26,680 --> 00:04:28,680
 knowing it. And just knowing the pain

68
00:04:28,680 --> 00:04:34,840
 without grasping it, just having the knowing without a firm

69
00:04:34,840 --> 00:04:37,880
 and stable grasp of it. So without

70
00:04:37,880 --> 00:04:40,440
 using the noting, it's so too easy to give rise to other

71
00:04:40,440 --> 00:04:42,480
 thoughts rather than just the thought of

72
00:04:42,480 --> 00:04:44,890
 this as being pain. It's too easy to give rise to the

73
00:04:44,890 --> 00:04:47,760
 thought of this is me, this is mine, even not

74
00:04:47,760 --> 00:04:52,620
 just the thought, but the perception still has room to grow

75
00:04:52,620 --> 00:04:55,800
 without the sati. So it requires both

76
00:04:55,800 --> 00:04:59,640
 of these.

77
00:04:59,640 --> 00:05:09,640
 [BLANK_AUDIO]

