1
00:00:00,000 --> 00:00:13,200
 So good evening everyone.

2
00:00:13,200 --> 00:00:21,160
 I'm broadcasting live sometime in February.

3
00:00:21,160 --> 00:00:33,600
 February 5th 2016.

4
00:00:33,600 --> 00:00:37,320
 So a couple of announcements.

5
00:00:37,320 --> 00:00:38,320
 One announcement.

6
00:00:38,320 --> 00:00:41,520
 One big announcement.

7
00:00:41,520 --> 00:00:46,760
 I'd say there is a fundraising campaign up.

8
00:00:46,760 --> 00:00:51,280
 Well, I can't talk too much about it.

9
00:00:51,280 --> 00:00:54,840
 I can't get involved with fundraising except to say that...

10
00:00:54,840 --> 00:00:59,840
 Except to say what?

11
00:00:59,840 --> 00:01:05,400
 That if we have the support we'll keep doing this.

12
00:01:05,400 --> 00:01:11,440
 If we don't have the support, well, I might have to head

13
00:01:11,440 --> 00:01:14,920
 back to Stony Creek or Thailand.

14
00:01:14,920 --> 00:01:17,920
 Maybe I'll just go back to Sri Lanka.

15
00:01:17,920 --> 00:01:21,320
 Live in the cave somewhere.

16
00:01:21,320 --> 00:01:24,000
 That was the other announcement was...

17
00:01:24,000 --> 00:01:29,040
 We got some visitors.

18
00:01:29,040 --> 00:01:30,440
 Aliens.

19
00:01:30,440 --> 00:01:32,240
 Foreigners.

20
00:01:32,240 --> 00:01:35,760
 And that's...

21
00:01:35,760 --> 00:01:37,320
 Wait.

22
00:01:37,320 --> 00:01:41,320
 That's Patrick.

23
00:01:41,320 --> 00:01:44,320
 And that's Thomas.

24
00:01:44,320 --> 00:01:47,720
 They're in meditation.

25
00:01:47,720 --> 00:01:49,720
 That's why they look so somber.

26
00:01:49,720 --> 00:01:52,960
 This meditation makes you suffer.

27
00:01:52,960 --> 00:01:58,880
 Don't they look like they're suffering?

28
00:01:58,880 --> 00:02:02,680
 Meditation is a very terrible thing.

29
00:02:02,680 --> 00:02:04,680
 Brings out the worst in people.

30
00:02:04,680 --> 00:02:08,680
 It actually does, you know.

31
00:02:08,680 --> 00:02:11,360
 Really brings out the worst in people.

32
00:02:11,360 --> 00:02:13,280
 Brings it out and throws it out.

33
00:02:13,280 --> 00:02:18,280
 It gets rid of it.

34
00:02:18,280 --> 00:02:24,280
 Apropos of the quote tonight.

35
00:02:24,280 --> 00:02:27,280
 The quote is apropos.

36
00:02:27,280 --> 00:02:30,280
 I don't know how that works.

37
00:02:30,280 --> 00:02:34,280
 Tonight's quote is...

38
00:02:34,280 --> 00:02:36,280
 It's a nice Pali quote.

39
00:02:36,280 --> 00:02:38,280
 It's very quotable.

40
00:02:38,280 --> 00:02:41,280
 This is something you could quote the Buddha as saying.

41
00:02:41,280 --> 00:02:45,280
 Where is it? It's by the Pali.

42
00:02:45,280 --> 00:02:49,280
 Chitta sanghi lei sah bhikkhu ei sata sanghi lee santhi.

43
00:02:49,280 --> 00:02:52,280
 Chitta wo dhaana.

44
00:02:52,280 --> 00:02:55,280
 Chitta wo dhaana.

45
00:02:55,280 --> 00:03:01,280
 Sata wisugjanti.

46
00:03:01,280 --> 00:03:06,280
 When the mind is defiled,

47
00:03:06,280 --> 00:03:08,280
 beings are defiled.

48
00:03:08,280 --> 00:03:12,280
 Chitta wo dhaana.

49
00:03:12,280 --> 00:03:15,280
 When the mind is cleansed,

50
00:03:15,280 --> 00:03:17,280
 sata wisugjanti.

51
00:03:17,280 --> 00:03:20,280
 Beings are purified.

52
00:03:20,280 --> 00:03:24,280
 Purification is in the mind.

53
00:03:24,280 --> 00:03:30,280
 So it may seem kind of trite or obvious.

54
00:03:30,280 --> 00:03:38,750
 But there's a couple of important implications to this

55
00:03:38,750 --> 00:03:40,280
 verse.

56
00:03:40,280 --> 00:03:48,280
 The first is sort of an attack on the concepts of Puritans.

57
00:03:48,280 --> 00:04:02,280
 In the time of the Buddha, they would...

58
00:04:02,280 --> 00:04:08,280
 Well, even today, they bathe in the Ganga River in India,

59
00:04:08,280 --> 00:04:13,280
 thinking that it's going to purify them.

60
00:04:13,280 --> 00:04:18,280
 That's the idea of purification in Hinduism.

61
00:04:18,280 --> 00:04:22,000
 That was one idea, not the idea, but a lot of people think

62
00:04:22,000 --> 00:04:22,280
 that.

63
00:04:22,280 --> 00:04:27,300
 So they take treks up to the mountains, the source of the G

64
00:04:27,300 --> 00:04:28,280
anga Nadi,

65
00:04:28,280 --> 00:04:37,280
 the River Ganges as the Brits called it,

66
00:04:37,280 --> 00:04:40,280
 thinking that it would purify them.

67
00:04:40,280 --> 00:04:43,280
 It was like that way in the Buddha's time,

68
00:04:43,280 --> 00:04:51,280
 even today there's this idea that it purifies.

69
00:04:51,280 --> 00:04:54,280
 In other religions, in many religions,

70
00:04:54,280 --> 00:05:00,280
 there's the idea that purification comes from God.

71
00:05:00,280 --> 00:05:07,280
 Purification comes through prayer.

72
00:05:07,280 --> 00:05:17,280
 Purification comes through ritual.

73
00:05:17,280 --> 00:05:24,700
 The idea that there are certain acts you have to perform to

74
00:05:24,700 --> 00:05:28,280
 become purified.

75
00:05:28,280 --> 00:05:33,750
 Some religions are about purification of water, pur

76
00:05:33,750 --> 00:05:36,280
ification of smoke,

77
00:05:36,280 --> 00:05:42,280
 many different ways of purification.

78
00:05:42,280 --> 00:05:44,280
 Some religions are religious movements,

79
00:05:44,280 --> 00:05:48,280
 beliefs are torturing yourself, that's what leads to purity

80
00:05:48,280 --> 00:05:49,280
,

81
00:05:49,280 --> 00:05:51,280
 all these different ideas.

82
00:05:51,280 --> 00:05:54,280
 So this is a big thing that the Buddha was addressing,

83
00:05:54,280 --> 00:05:56,280
 no, it's the mind.

84
00:05:56,280 --> 00:05:59,280
 The mind is what purifies me.

85
00:05:59,280 --> 00:06:02,780
 So that's the obvious one, that's what we often hear talked

86
00:06:02,780 --> 00:06:03,280
 about.

87
00:06:03,280 --> 00:06:07,280
 I think a more important implication of this,

88
00:06:07,280 --> 00:06:13,280
 what it's saying, is that being, the being,

89
00:06:13,280 --> 00:06:18,280
 is dependent on the mind, not the other way around.

90
00:06:18,280 --> 00:06:21,520
 The mind doesn't come from the being, the being comes from

91
00:06:21,520 --> 00:06:22,280
 the mind.

92
00:06:22,280 --> 00:06:26,280
 Mark that well, that's important.

93
00:06:26,280 --> 00:06:35,280
 So ordinary thinking is who you are determines your mind

94
00:06:35,280 --> 00:06:36,280
 state.

95
00:06:36,280 --> 00:06:41,250
 An angry person gets angry. Why? Because they're an angry

96
00:06:41,250 --> 00:06:42,280
 person.

97
00:06:42,280 --> 00:06:47,280
 It's sloppy thinking, but we fall into it quite easily.

98
00:06:47,280 --> 00:06:53,280
 I have, you know, I'm an alcoholic, I'm an addict.

99
00:06:53,280 --> 00:06:56,820
 I don't agree with it really. Just off the top of my head

100
00:06:56,820 --> 00:06:57,280
 it sounds wrong.

101
00:06:57,280 --> 00:06:59,850
 I know alcoholics anonymous is big in this, identifying

102
00:06:59,850 --> 00:07:02,280
 yourself as an alcoholic.

103
00:07:02,280 --> 00:07:05,530
 Kind of understand the rationale behind it, but there's

104
00:07:05,530 --> 00:07:07,280
 something there that's a little bit,

105
00:07:07,280 --> 00:07:10,780
 I don't know, I don't want to go attacking what appears to

106
00:07:10,780 --> 00:07:14,280
 be a really good addiction counseling service,

107
00:07:14,280 --> 00:07:19,280
 but not convinced that it's entirely perfect.

108
00:07:19,280 --> 00:07:23,430
 I mean, hey, I bet meditation would be a really good

109
00:07:23,430 --> 00:07:25,280
 addiction service.

110
00:07:25,280 --> 00:07:30,280
 Can't say that.

111
00:07:30,280 --> 00:07:32,700
 I don't know off the top of my head of any alcoholics who

112
00:07:32,700 --> 00:07:35,280
 came and gave it up through meditation.

113
00:07:35,280 --> 00:07:39,290
 I do remember one guy, after he finished his course,

114
00:07:39,290 --> 00:07:43,280
 wearing white clothes in Thailand,

115
00:07:43,280 --> 00:07:47,280
 went down into the village and ordered three beers.

116
00:07:47,280 --> 00:07:51,690
 I mean, it seems kind of, you know, so what, but, well, he

117
00:07:51,690 --> 00:07:53,280
 just finished an entire meditation course

118
00:07:53,280 --> 00:07:55,280
 and he was still wearing his white clothes.

119
00:07:55,280 --> 00:07:57,860
 And so there I am up on the mountain in the meditation

120
00:07:57,860 --> 00:07:59,280
 center and someone comes to me

121
00:07:59,280 --> 00:08:05,660
 and says, "One of your meditators snuck out to go get drunk

122
00:08:05,660 --> 00:08:06,280
."

123
00:08:06,280 --> 00:08:09,280
 And he was still staying with us, that's the thing.

124
00:08:09,280 --> 00:08:12,280
 He had given up eight precepts, so he was on five precepts,

125
00:08:12,280 --> 00:08:17,520
 and he didn't understand that five precepts, the point

126
00:08:17,520 --> 00:08:18,280
 being you're staying with us,

127
00:08:18,280 --> 00:08:21,280
 and oh boy, did I tear into it.

128
00:08:21,280 --> 00:08:25,280
 And you know what his response was, what his defense was?

129
00:08:25,280 --> 00:08:29,820
 "Well, I ordered six beers, but I only drank three of them

130
00:08:29,820 --> 00:08:30,280
."

131
00:08:30,280 --> 00:08:33,460
 He really thought that was that, and he didn't realize that

132
00:08:33,460 --> 00:08:35,280
 not drinking alcohol was part of the five precepts,

133
00:08:35,280 --> 00:08:42,280
 which is probably my fault, shows lack of instruction.

134
00:08:42,280 --> 00:08:46,500
 Anyway, yeah, I'm hoping that that's not a, I mean, it's

135
00:08:46,500 --> 00:08:48,280
 kind of shameful to know that

136
00:08:48,280 --> 00:08:51,500
 someone who finishes our course is still going to go out

137
00:08:51,500 --> 00:08:52,280
 and drink.

138
00:08:52,280 --> 00:08:54,280
 It shouldn't happen like that.

139
00:08:54,280 --> 00:08:58,640
 But I mean, I would defend, the defense would be some

140
00:08:58,640 --> 00:09:01,280
 people slip through the cracks.

141
00:09:01,280 --> 00:09:05,590
 But I would argue that it's probably a pretty, why wouldn't

142
00:09:05,590 --> 00:09:06,280
 it be?

143
00:09:06,280 --> 00:09:11,280
 But anyway, totally off track there, apologize.

144
00:09:11,280 --> 00:09:20,480
 The point I was trying to make was that our state of being,

145
00:09:20,480 --> 00:09:24,280
 "who we are,"

146
00:09:24,280 --> 00:09:31,380
 is based simply and solely on the habits that we form, not

147
00:09:31,380 --> 00:09:37,280
 on some predetermined genetic, etc., etc., state of being.

148
00:09:37,280 --> 00:09:41,860
 It's all habitual. It's all habits. It's artifices,

149
00:09:41,860 --> 00:09:43,280
 artificial.

150
00:09:43,280 --> 00:09:45,440
 So if you say you're an angry person, what that means is

151
00:09:45,440 --> 00:09:49,280
 you've developed over time a habit to get angry.

152
00:09:49,280 --> 00:09:53,440
 If you're an addicted person, well, you've cultivated

153
00:09:53,440 --> 00:09:54,280
 addiction.

154
00:09:54,280 --> 00:10:04,200
 And by that very nature, by that very fact, the habits can

155
00:10:04,200 --> 00:10:05,280
 be unlearned.

156
00:10:05,280 --> 00:10:08,280
 You can head in the other direction.

157
00:10:08,280 --> 00:10:12,270
 You could argue it gets more and more difficult, the more

158
00:10:12,270 --> 00:10:14,280
 you get addicted

159
00:10:14,280 --> 00:10:18,270
 and the more deeply ingrained the habit becomes, and I'd

160
00:10:18,270 --> 00:10:20,280
 agree with that for sure.

161
00:10:20,280 --> 00:10:24,280
 It doesn't mean it's who you are.

162
00:10:24,280 --> 00:10:31,280
 The mind comes first. The mind precedes all things.

163
00:10:31,280 --> 00:10:36,280
 It's just a matter of what you're working for.

164
00:10:36,280 --> 00:10:40,470
 So if you want to talk about why we are the way we are, we

165
00:10:40,470 --> 00:10:42,280
 start with the mind.

166
00:10:42,280 --> 00:10:45,280
 We start with the habits that we cultivate.

167
00:10:45,280 --> 00:10:49,280
 And so meditation is just another habit.

168
00:10:49,280 --> 00:10:54,070
 We're cultivating a wholesome habit, a habit of objectivity

169
00:10:54,070 --> 00:10:56,280
, a habit of mental clarity.

170
00:10:56,280 --> 00:10:59,280
 That's how you should look at it. It's not magic.

171
00:10:59,280 --> 00:11:02,280
 It's not like you can count up how many hours you've done

172
00:11:02,280 --> 00:11:03,280
 and somehow that mean anything.

173
00:11:03,280 --> 00:11:06,280
 It doesn't mean anything.

174
00:11:06,280 --> 00:11:16,740
 It doesn't mean anything is how often, how frequently and

175
00:11:16,740 --> 00:11:21,280
 how clearly you're able to set your mind.

176
00:11:21,280 --> 00:11:26,490
 So every moment that your mind is clear, you're cultivating

177
00:11:26,490 --> 00:11:29,280
 that habit of clarity.

178
00:11:29,280 --> 00:11:31,830
 Every moment you're objective, you're cultivating object

179
00:11:31,830 --> 00:11:32,280
ivity.

180
00:11:32,280 --> 00:11:34,980
 Every moment you're in reality, you're cultivating an

181
00:11:34,980 --> 00:11:36,280
 awareness of reality.

182
00:11:36,280 --> 00:11:38,280
 And that's habitual.

183
00:11:38,280 --> 00:11:44,050
 It, over time, begins to erode other habits, begins to take

184
00:11:44,050 --> 00:11:45,280
 their place.

185
00:11:45,280 --> 00:11:50,280
 And the power of it leads to understanding.

186
00:11:50,280 --> 00:11:58,680
 You start to see things clearer and that understanding

187
00:11:58,680 --> 00:12:04,280
 works to erode, to weaken bad habits that are based on

188
00:12:04,280 --> 00:12:05,280
 misunderstanding.

189
00:12:05,280 --> 00:12:08,980
 Because when you understand that something's wrong, when

190
00:12:08,980 --> 00:12:14,280
 you used to think it was right, that changes everything.

191
00:12:14,280 --> 00:12:16,280
 Many habits don't arise.

192
00:12:16,280 --> 00:12:19,940
 When a person becomes a sotapana, there are certain habits

193
00:12:19,940 --> 00:12:22,280
 that just get cut off from wisdom.

194
00:12:22,280 --> 00:12:25,280
 But until that point, we're building habits.

195
00:12:25,280 --> 00:12:30,320
 We're building up purity of mind to the extent that we can

196
00:12:30,320 --> 00:12:31,280
 let go.

197
00:12:31,280 --> 00:12:35,640
 To the extent that we can slip through the cracks, so not

198
00:12:35,640 --> 00:12:37,280
 clinging on to everything.

199
00:12:37,280 --> 00:12:45,280
 Let go and out to nirvana.

200
00:12:45,280 --> 00:12:50,770
 Once you've seen nirvana, game-change. At that point,

201
00:12:50,770 --> 00:12:54,280
 habits disappear.

202
00:12:54,280 --> 00:13:03,280
 The residual, residue of them, physical residue and so on,

203
00:13:03,280 --> 00:13:07,020
 chemicals in the brain that are associated with anger or

204
00:13:07,020 --> 00:13:09,280
 addiction must still be there.

205
00:13:09,280 --> 00:13:14,280
 But the mental aspect isn't there.

206
00:13:14,280 --> 00:13:21,230
 So the second point is just that the being isn't the one

207
00:13:21,230 --> 00:13:25,280
 that holds our defilements or our problems.

208
00:13:25,280 --> 00:13:28,280
 It's the mind and they're just mind states.

209
00:13:28,280 --> 00:13:33,190
 All problems, depression, anxiety, can all be distilled

210
00:13:33,190 --> 00:13:35,280
 down to mind states.

211
00:13:35,280 --> 00:13:39,640
 The schizophrenia, bipolar, all these things can be, in the

212
00:13:39,640 --> 00:13:43,750
 end, separated out into physical states and mental states

213
00:13:43,750 --> 00:13:45,280
 that arise and cease.

214
00:13:45,280 --> 00:13:47,280
 Often incessantly.

215
00:13:47,280 --> 00:13:52,280
 So people would argue that's not a habit.

216
00:13:52,280 --> 00:13:56,780
 The only way you can say it's not a habit is if you follow

217
00:13:56,780 --> 00:14:02,280
 modern thinking that life starts at conception.

218
00:14:02,280 --> 00:14:07,280
 That birth creates mind, not mind creates birth.

219
00:14:07,280 --> 00:14:11,280
 So Buddhism doesn't think that.

220
00:14:11,280 --> 00:14:16,310
 Buddhism claims that mind creates birth, not the other way

221
00:14:16,310 --> 00:14:17,280
 around.

222
00:14:17,280 --> 00:14:20,340
 And so something like, as I was thinking, I think I was

223
00:14:20,340 --> 00:14:23,230
 just talking about last night, the mind creates

224
00:14:23,230 --> 00:14:25,280
 schizophrenia, it's a habit.

225
00:14:25,280 --> 00:14:36,280
 It's based on some bad habits somehow.

226
00:14:36,280 --> 00:14:38,280
 So that's all.

227
00:14:38,280 --> 00:14:43,280
 I wanted to get Robin on here, so she's on here now.

228
00:14:43,280 --> 00:14:52,280
 Let's see if I can patch you into the audio.

229
00:14:52,280 --> 00:14:59,280
 Let's see.

230
00:14:59,280 --> 00:15:11,280
 Okay, Robin, are you there?

231
00:15:11,280 --> 00:15:18,280
 I'm here. So you are now on.

232
00:15:18,280 --> 00:15:21,350
 Are you responsible for this new campaign, this new thing

233
00:15:21,350 --> 00:15:22,280
 on YouCaring?

234
00:15:22,280 --> 00:15:26,280
 Yes.

235
00:15:26,280 --> 00:15:30,040
 I started working on the new campaign, but I didn't even

236
00:15:30,040 --> 00:15:31,280
 release it yet.

237
00:15:31,280 --> 00:15:36,280
 Oh, well, I just released it for you.

238
00:15:36,280 --> 00:15:42,280
 It said it's live. I just got an email saying it's live.

239
00:15:42,280 --> 00:15:48,280
 That's right. I guess that's why I wasn't informed about it

240
00:15:48,280 --> 00:15:48,280
 because it's still in the works.

241
00:15:48,280 --> 00:15:52,220
 It has no pictures on it or anything yet. It doesn't have

242
00:15:52,220 --> 00:15:53,280
 any pictures.

243
00:15:53,280 --> 00:15:57,280
 I was going to suggest them to put some pictures on it.

244
00:15:57,280 --> 00:16:02,280
 It was really just in progress, completely in progress.

245
00:16:02,280 --> 00:16:07,280
 Well, 218 people have seen it on Facebook.

246
00:16:07,280 --> 00:16:12,280
 One person's already shared it, so it's going viral.

247
00:16:12,280 --> 00:16:17,280
 Okay. Well, we'll get some pictures up.

248
00:16:17,280 --> 00:16:21,280
 Maybe release it.

249
00:16:21,280 --> 00:16:23,280
 Well, I can talk about it anyway.

250
00:16:23,280 --> 00:16:24,280
 Go ahead.

251
00:16:24,280 --> 00:16:27,280
 Sure.

252
00:16:27,280 --> 00:16:32,280
 Sorry, I was just really unprepared for this.

253
00:16:32,280 --> 00:16:37,280
 The monastery has been up and running for six months now,

254
00:16:37,280 --> 00:16:38,280
 and things are really going well.

255
00:16:38,280 --> 00:16:40,280
 There's a lot of meditators coming.

256
00:16:40,280 --> 00:16:44,240
 The meditator schedule is booked for a couple of months out

257
00:16:44,240 --> 00:16:45,280
 in advance.

258
00:16:45,280 --> 00:16:49,280
 It's great. There's a lot of things going on,

259
00:16:49,280 --> 00:16:53,820
 and we're looking to gauge support for whether this

260
00:16:53,820 --> 00:16:58,280
 monastery is able to continue on well past

261
00:16:58,280 --> 00:17:02,280
 when our lease ends, which is in August.

262
00:17:02,280 --> 00:17:05,280
 We're just looking to gauge support for that.

263
00:17:05,280 --> 00:17:08,860
 Hopefully, people are interested in supporting the

264
00:17:08,860 --> 00:17:10,280
 monastery and the meditation center

265
00:17:10,280 --> 00:17:15,280
 well beyond what we already have the funds for, which is...

266
00:17:15,280 --> 00:17:17,460
 Actually, this was another thing I needed to check on, but

267
00:17:17,460 --> 00:17:21,280
 it's somewhere between June and August.

268
00:17:21,280 --> 00:17:27,280
 Just looking to gauge support beyond that.

269
00:17:27,280 --> 00:17:31,280
 Okay. Thank you for that.

270
00:17:31,280 --> 00:17:35,280
 Thank you, Bante.

271
00:17:35,280 --> 00:17:39,280
 With that, I think we'll end for the night.

272
00:17:39,280 --> 00:17:44,280
 Thank you all for tuning in. Have a good night.

273
00:17:44,280 --> 00:17:52,280
 Thank you, Bante.

274
00:17:52,280 --> 00:17:58,280
 Oops. It really wasn't quick ready yet.

