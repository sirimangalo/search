1
00:00:00,000 --> 00:00:07,000
 oops forgot something

2
00:00:07,000 --> 00:00:16,440
 ok

3
00:00:16,440 --> 00:00:21,080
 hello everyone from broadcasting live

4
00:00:21,080 --> 00:00:25,120
 daily broadcast just to say hello

5
00:00:25,120 --> 00:00:29,120
 maybe provide some dhamma

6
00:00:29,120 --> 00:00:36,120
 do you have any dhamma for us today Robin?

7
00:00:36,120 --> 00:00:41,750
 there is no dhamma but it is Thanksgiving in the United

8
00:00:41,750 --> 00:00:44,120
 States so everyone is asleep after eating all their turkey

9
00:00:44,120 --> 00:00:45,120
 I think

10
00:00:45,120 --> 00:00:49,320
 I had three people wish me happy Thanksgiving today and I

11
00:00:49,320 --> 00:00:51,760
 don't know if it was the right thing to say but I said we

12
00:00:51,760 --> 00:00:53,120
 don't have Thanksgiving again

13
00:00:53,120 --> 00:01:00,560
 or we had it last month certainly I mean not that it would

14
00:01:00,560 --> 00:01:04,550
 mean anything to me anyway probably but still I think it's

15
00:01:04,550 --> 00:01:08,120
 kind of funny that people are wishing us Thanksgiving

16
00:01:08,120 --> 00:01:15,120
 I was in America I guess people would it would be proper

17
00:01:15,120 --> 00:01:15,120
 right?

18
00:01:15,120 --> 00:01:18,810
 yeah Thanksgiving in America is one of those holidays that

19
00:01:18,810 --> 00:01:22,740
's not controversial there's so much ridiculous controversy

20
00:01:22,740 --> 00:01:27,120
 with other holidays like Donald Trump is all up in arms

21
00:01:27,120 --> 00:01:29,550
 wait you don't think Thanksgiving has any controversy

22
00:01:29,550 --> 00:01:30,120
 associated with it?

23
00:01:30,120 --> 00:01:34,120
 well no ok let me step back on that of course it does

24
00:01:34,120 --> 00:01:38,140
 because of the Native American thing but I was just

25
00:01:38,140 --> 00:01:42,390
 thinking of the more political type of things like Donald

26
00:01:42,390 --> 00:01:43,120
 Trump was up in arms

27
00:01:43,120 --> 00:01:47,360
 because Starbucks has a plain red cup for their holiday cup

28
00:01:47,360 --> 00:01:52,120
 this year it doesn't say Merry Christmas or Happy Holidays

29
00:01:52,120 --> 00:01:53,120
 or anything like that

30
00:01:53,120 --> 00:01:56,460
 and he was actually kind of going on and on about it how it

31
00:01:56,460 --> 00:01:59,130
 was such a terrible thing and when he's president everyone

32
00:01:59,130 --> 00:02:01,300
's going to say Merry Christmas so it gets kind of

33
00:02:01,300 --> 00:02:03,120
 ridiculous with holidays

34
00:02:03,120 --> 00:02:06,320
 but Thanksgiving other than the whole Native American thing

35
00:02:06,320 --> 00:02:09,450
 is a pretty mild holiday where everybody's celebrating the

36
00:02:09,450 --> 00:02:11,120
 same thing more or less

37
00:02:11,120 --> 00:02:15,280
 I imagine the First Nations people would disagree with you

38
00:02:15,280 --> 00:02:16,120
 on that one

39
00:02:16,120 --> 00:02:17,120
 I think so too

40
00:02:17,120 --> 00:02:22,470
 I think that's not very pleasant I mean the idea of

41
00:02:22,470 --> 00:02:26,280
 Thanksgiving is pretty awesome it's just how it arose I

42
00:02:26,280 --> 00:02:29,770
 think or what it's associated with it's somehow being

43
00:02:29,770 --> 00:02:36,120
 thankful for having taken over some other people's land

44
00:02:36,120 --> 00:02:42,120
 it's not really invaded it's a celebration of an invasion

45
00:02:42,120 --> 00:02:44,610
 of that I mean not quite invasion because people here didn

46
00:02:44,610 --> 00:02:48,120
't have the idea of land ownership but really invasion

47
00:02:48,120 --> 00:02:59,950
 like an invasive species that just comes by force and by

48
00:02:59,950 --> 00:03:11,120
 sheer inertia steam rollers over another culture and people

49
00:03:11,120 --> 00:03:17,150
 I mean to passively wipe out another species to me doesn't

50
00:03:17,150 --> 00:03:28,460
 seem problematic like if it's just oh they eventually just

51
00:03:28,460 --> 00:03:31,120
 naturally became the dominant group

52
00:03:31,120 --> 00:03:35,170
 but that's not really how it happened there was forced

53
00:03:35,170 --> 00:03:40,740
 conversion and really theft and treachery trickery to get

54
00:03:40,740 --> 00:03:45,970
 their lands and to get them to sign horribly unfair

55
00:03:45,970 --> 00:03:48,120
 treaties and so on

56
00:03:48,120 --> 00:03:54,600
 just really bad stuff from what I hear in the name of

57
00:03:54,600 --> 00:04:05,420
 imperialism the name of the Queen the name of the King and

58
00:04:05,420 --> 00:04:13,120
 slavery

59
00:04:13,120 --> 00:04:20,400
 human beings can be so much more noble than animals but

60
00:04:20,400 --> 00:04:23,120
 they can be so much more evil as well

61
00:04:23,120 --> 00:04:31,120
 they were all so convinced that their their way was right

62
00:04:31,120 --> 00:04:36,670
 and well you know the pilgrims the settlers they were so

63
00:04:36,670 --> 00:04:39,120
 convinced that their way was right that

64
00:04:39,120 --> 00:04:43,740
 I don't know that's part of it yeah but I think that's sort

65
00:04:43,740 --> 00:04:46,750
 of from maybe from religious point of view or from our

66
00:04:46,750 --> 00:04:49,120
 cultural point of view but there was a lot of greed

67
00:04:49,120 --> 00:04:50,120
 involved I think as well

68
00:04:50,120 --> 00:04:54,450
 like just sheer opportunism oh these people are naive in a

69
00:04:54,450 --> 00:04:58,400
 sense which I guess you could say they were but it just

70
00:04:58,400 --> 00:05:02,070
 means that they were simple people who weren't worried

71
00:05:02,070 --> 00:05:08,120
 about land ownership and mineral rights and you know things

72
00:05:08,120 --> 00:05:12,120
 like forest

73
00:05:12,120 --> 00:05:16,550
 you know cutting down trees and they wouldn't they didn't

74
00:05:16,550 --> 00:05:20,780
 rape and pillage the land either and so they when the

75
00:05:20,780 --> 00:05:25,300
 people came and saw these natural resources that were maybe

76
00:05:25,300 --> 00:05:29,120
 just starting to run short in Europe I don't know but we're

77
00:05:29,120 --> 00:05:31,120
 harder to get at

78
00:05:31,120 --> 00:05:42,120
 that old gold and whatever just land

79
00:05:42,120 --> 00:05:47,120
 a lot of greed

80
00:05:47,120 --> 00:05:53,120
 greed is the biggest reason for war

81
00:05:53,120 --> 00:05:56,480
 it's usually the least publicized because everyone tries to

82
00:05:56,480 --> 00:05:59,120
 excuse it with other reasons but

83
00:05:59,120 --> 00:06:03,800
 I think deep down most wars have been have been fought out

84
00:06:03,800 --> 00:06:14,120
 of greed, this was sheer greed

85
00:06:14,120 --> 00:06:18,120
 so on that pleasant note

86
00:06:18,120 --> 00:06:21,280
 so I guess Thanksgiving is a little more controversial than

87
00:06:21,280 --> 00:06:27,400
 I was thinking I think so I mean the concept is awesome as

88
00:06:27,400 --> 00:06:29,120
 I said it's just

89
00:06:29,120 --> 00:06:32,210
 it used to be called Columbus Day didn't it or is that a

90
00:06:32,210 --> 00:06:35,120
 different day that's a different day it's a different day

91
00:06:35,120 --> 00:06:38,250
 you've got a day that celebrates this horrible horrible

92
00:06:38,250 --> 00:06:39,120
 person

93
00:06:39,120 --> 00:06:43,120
 well maybe not horrible but pretty bad

94
00:06:43,120 --> 00:06:47,030
 well the day that we have this Columbus Day I believe is

95
00:06:47,030 --> 00:06:49,120
 the Canadian Thanksgiving

96
00:06:49,120 --> 00:06:54,120
 but our Thanksgiving is a month later

97
00:06:54,120 --> 00:06:57,990
 yeah you celebrate the guy who he was an opportunist

98
00:06:57,990 --> 00:07:02,240
 apparently just coming looking for gold and he thought he'd

99
00:07:02,240 --> 00:07:09,120
 reached India so that's why he called him Indian

100
00:07:09,120 --> 00:07:15,120
 anyway this is very skirting the edge of what is not Dhamma

101
00:07:15,120 --> 00:07:19,660
 so you better jump back in and see what do we have any the

102
00:07:19,660 --> 00:07:22,120
 quote is kind of neat

103
00:07:22,120 --> 00:07:27,120
 talks about grasping and worry

104
00:07:27,120 --> 00:07:31,120
 so we have the five aggregates and that's what we cling to

105
00:07:31,120 --> 00:07:33,420
 and e-clinging is always done to one of the five or

106
00:07:33,420 --> 00:07:36,120
 combination of the five

107
00:07:36,120 --> 00:07:42,120
 concepts that have arisen based on the five aggregates

108
00:07:42,120 --> 00:07:47,620
 so we cling to the body and when it changes we worry we it

109
00:07:47,620 --> 00:07:51,490
 upsets us it disturbs us not just the body but other

110
00:07:51,490 --> 00:07:56,120
 physical forms as well our belongings our possession my

111
00:07:56,120 --> 00:07:58,120
 robe

112
00:07:58,120 --> 00:08:03,120
 these little ties on the end of the room

113
00:08:03,120 --> 00:08:08,120
 these little ties here

114
00:08:08,120 --> 00:08:14,120
 one of them up here ripped yesterday or ripped out

115
00:08:14,120 --> 00:08:15,120
 yesterday

116
00:08:15,120 --> 00:08:18,120
 and to sew it back

117
00:08:18,120 --> 00:08:21,120
 but that didn't upset me that wasn't a big deal

118
00:08:21,120 --> 00:08:24,120
 it's easy to get upset

119
00:08:24,120 --> 00:08:28,700
 I remember I think when I first got this robe like a few

120
00:08:28,700 --> 00:08:35,120
 days later it got some stain on it or something

121
00:08:35,120 --> 00:08:42,120
 when I got

122
00:08:42,120 --> 00:08:46,300
 we cling to things we cling to our possessions we cling to

123
00:08:46,300 --> 00:08:47,120
 people

124
00:08:47,120 --> 00:08:57,120
 we cling to form and we cling to feelings

125
00:08:57,120 --> 00:09:02,960
 we cling to pleasant feelings we try to obtain them any way

126
00:09:02,960 --> 00:09:04,120
 possible

127
00:09:04,120 --> 00:09:10,120
 and we're constantly on guard against painful feelings

128
00:09:10,120 --> 00:09:17,430
 often specific painful feelings so if someone has maybe a

129
00:09:17,430 --> 00:09:18,120
 bad back then they're constantly worried about

130
00:09:18,120 --> 00:09:23,120
 constantly but again and again and again

131
00:09:23,120 --> 00:09:26,820
 there's a little bit of pain and immediately it's I don't

132
00:09:26,820 --> 00:09:32,120
 know my back problem becomes an obsession

133
00:09:32,120 --> 00:09:40,120
 we get very much caught up

134
00:09:40,120 --> 00:09:43,120
 our attachment to feelings

135
00:09:43,120 --> 00:09:46,120
 our attachment to perceptions

136
00:09:46,120 --> 00:09:51,120
 recognition I mean it's this one not always by itself but

137
00:09:51,120 --> 00:09:57,120
 it's recognition that

138
00:09:57,120 --> 00:10:01,300
 is the trigger for much of our addiction much of our

139
00:10:01,300 --> 00:10:02,120
 aversion

140
00:10:02,120 --> 00:10:07,120
 we recognize something if you didn't recognize things

141
00:10:07,120 --> 00:10:11,580
 you wouldn't recognize a spider people who are afraid of

142
00:10:11,580 --> 00:10:14,120
 spiders if you didn't recognize that that was a spider it

143
00:10:14,120 --> 00:10:16,120
 could walk all over you and say

144
00:10:16,120 --> 00:10:18,540
 you'd have no idea what was going on but you'd feel

145
00:10:18,540 --> 00:10:25,120
 something and no just a feeling

146
00:10:25,120 --> 00:10:29,120
 if we weren't for recognition if it weren't for our

147
00:10:29,120 --> 00:10:34,120
 perceptions of things

148
00:10:34,120 --> 00:10:37,440
 we couldn't give rise to liking or disliking because we

149
00:10:37,440 --> 00:10:43,120
 couldn't associate

150
00:10:43,120 --> 00:10:48,450
 you see a piece of cheesecake and you associate it with

151
00:10:48,450 --> 00:10:52,270
 cheesecake you associate it with tastes and textures and

152
00:10:52,270 --> 00:10:53,120
 feelings

153
00:10:53,120 --> 00:10:56,700
 and so it makes you feel happy just to see it but if you

154
00:10:56,700 --> 00:11:02,120
 didn't know what it was you couldn't recognize it

155
00:11:02,120 --> 00:11:08,190
 so recognition is actually quite neutral but we cling to

156
00:11:08,190 --> 00:11:13,120
 this we are greedy for that simple recognition

157
00:11:13,120 --> 00:11:14,780
 because there are many things that aren't inherently

158
00:11:14,780 --> 00:11:18,120
 pleasant seeing a piece of cheesecake isn't inherently

159
00:11:18,120 --> 00:11:19,120
 pleasant

160
00:11:19,120 --> 00:11:23,630
 seeing a spider isn't inherently unpleasant but because we

161
00:11:23,630 --> 00:11:27,120
 react to them because we cling to that

162
00:11:27,120 --> 00:11:34,120
 the recognition of a spider is the moment when we get upset

163
00:11:34,120 --> 00:11:44,120
 and then sankara we cling to them we cling to the sankaras

164
00:11:44,120 --> 00:11:47,120
 of comparison when you compare some things

165
00:11:47,120 --> 00:11:53,120
 so if you look at your body and you're really fat like me

166
00:11:53,120 --> 00:11:55,120
 then you start to worry about your weight

167
00:11:55,120 --> 00:11:58,690
 and like I have to go on a diet I have to stop eating so

168
00:11:58,690 --> 00:12:02,120
 much and I'll get fat

169
00:12:02,120 --> 00:12:05,190
 every time I...everywhere I go people always tell me I'm

170
00:12:05,190 --> 00:12:06,120
 getting thinner

171
00:12:06,120 --> 00:12:09,550
 it's funny whenever I see people I haven't seen for a while

172
00:12:09,550 --> 00:12:12,120
 they keep telling me I'm getting thinner

173
00:12:12,120 --> 00:12:16,460
 people have been telling me for 15 years that I'm getting

174
00:12:16,460 --> 00:12:20,120
 thinner so we worry about these things

175
00:12:20,120 --> 00:12:23,330
 I'm too thin I'm too fat some people it becomes an

176
00:12:23,330 --> 00:12:28,120
 obsession and so they become bulimic or anorexic

177
00:12:28,120 --> 00:12:36,120
 too tall I'm too short

178
00:12:36,120 --> 00:12:40,290
 we cling to our thoughts about things we cling to our

179
00:12:40,290 --> 00:12:48,120
 emotions I'm depressed and then I identify with it

180
00:12:48,120 --> 00:12:51,850
 or I like something and it's like I like it and you become

181
00:12:51,850 --> 00:12:54,120
 attached to this desire and you say yes

182
00:12:54,120 --> 00:12:59,510
 gotta get that desire gotta follow that desire whenever it

183
00:12:59,510 --> 00:13:00,120
 comes

184
00:13:00,120 --> 00:13:05,290
 but this leads to worry and stress when you can't get what

185
00:13:05,290 --> 00:13:08,120
 you want when you're denied

186
00:13:08,120 --> 00:13:14,120
 leads to anger, to grief, to sorrow, to suffering

187
00:13:14,120 --> 00:13:16,510
 in consciousness while consciousness is another one of the

188
00:13:16,510 --> 00:13:21,120
 neutral ones there's nothing ostensibly problematic about

189
00:13:21,120 --> 00:13:22,120
 consciousness right?

190
00:13:22,120 --> 00:13:29,450
 except that consciousness is like a capsule for all the

191
00:13:29,450 --> 00:13:32,120
 rest if it weren't for consciousness you couldn't have

192
00:13:32,120 --> 00:13:34,120
 judgments

193
00:13:34,120 --> 00:13:40,120
 you couldn't have any of the problems

194
00:13:40,120 --> 00:13:43,610
 it's like if you don't have a spider's web you don't have a

195
00:13:43,610 --> 00:13:44,120
 spider no?

196
00:13:44,120 --> 00:13:52,580
 you don't have an ant home you don't have ants if you don't

197
00:13:52,580 --> 00:13:55,390
 have it's like a breathing ground for let's say for

198
00:13:55,390 --> 00:13:57,120
 bacteria or something

199
00:13:57,120 --> 00:14:03,310
 like these a rusty nail you know how everyone tells you you

200
00:14:03,310 --> 00:14:06,120
 have to be very careful you don't step on a rusty nail

201
00:14:06,120 --> 00:14:10,960
 and if you do you have to get it cleaned out and you have

202
00:14:10,960 --> 00:14:13,980
 to get a shot or something you have to get a shot in case

203
00:14:13,980 --> 00:14:15,120
 you step on a rusty nail

204
00:14:15,120 --> 00:14:18,330
 when I was younger I did step I lived on a farm and lots of

205
00:14:18,330 --> 00:14:22,460
 rusty nails around I did step on a rusty nail but I had my

206
00:14:22,460 --> 00:14:24,120
 tetanus shot

207
00:14:24,120 --> 00:14:30,280
 when I was in California I stepped on a rusty something, a

208
00:14:30,280 --> 00:14:34,120
 metal something I was walking barefoot in Tarzana

209
00:14:34,120 --> 00:14:41,270
 I went every day to a French restaurant it was the most

210
00:14:41,270 --> 00:14:45,120
 exotic alms round ever every day I would go on alms round

211
00:14:45,120 --> 00:14:51,120
 and I would go to a French restaurant and a Thai restaurant

212
00:14:51,120 --> 00:14:59,120
 and I think some Thai people stopped me on the way as well

213
00:14:59,120 --> 00:15:03,300
 and then back at the house some people at the house would

214
00:15:03,300 --> 00:15:05,120
 offer alms as well

215
00:15:05,120 --> 00:15:11,860
 so every day I was having waffles or pancakes with mounds

216
00:15:11,860 --> 00:15:18,270
 of real it was high quality and this is LA sort of

217
00:15:18,270 --> 00:15:23,120
 Hollywood high so French restaurant because it was owned

218
00:15:23,120 --> 00:15:25,120
 actually by Thai people

219
00:15:25,120 --> 00:15:32,570
 but one day I stepped on a piece of metal and they took me

220
00:15:32,570 --> 00:15:38,830
 right away to the emergency room and it ended up costing

221
00:15:38,830 --> 00:15:41,120
 like $500 to what?

222
00:15:41,120 --> 00:15:45,420
 can't remember just to have it checked out and then get me

223
00:15:45,420 --> 00:15:50,400
 a tetanus shot I think that was like $500 that sounds about

224
00:15:50,400 --> 00:15:55,120
 right yes to walking to walk in the door yes $500

225
00:15:55,120 --> 00:16:02,120
 Americans crazy people crazy country

226
00:16:02,120 --> 00:16:08,420
 but my point there was totally off track is there's nothing

227
00:16:08,420 --> 00:16:12,310
 wrong with rest so it got me thinking I had always thought

228
00:16:12,310 --> 00:16:16,120
 that rest was rest give you tetanus

229
00:16:16,120 --> 00:16:19,400
 rest gives you lock jaw because if you get rest in your

230
00:16:19,400 --> 00:16:23,010
 bloodstream it must cause what we call lock jaw or tetanus

231
00:16:23,010 --> 00:16:27,120
 where your body it's a really horrible sickness

232
00:16:27,120 --> 00:16:31,750
 but it's not actually the case it's there's a virus known

233
00:16:31,750 --> 00:16:37,120
 virus bacteria and the bacteria lives in in the rusty metal

234
00:16:37,120 --> 00:16:41,120
 because rust makes holes in metal

235
00:16:41,120 --> 00:16:45,160
 so you have a metal nail and when the rest creates pockets

236
00:16:45,160 --> 00:16:49,290
 and pockets where bacteria can live in one of the bacteria

237
00:16:49,290 --> 00:16:53,180
 is that likes to live in those pockets or is potentially

238
00:16:53,180 --> 00:16:58,240
 living in those pockets is the these tetanus bacteria I can

239
00:16:58,240 --> 00:16:59,120
't remember what they're called

240
00:16:59,120 --> 00:17:02,490
 and and it's just so perfect because it's like injecting

241
00:17:02,490 --> 00:17:06,020
 yourself with those bacteria when you get a nail right into

242
00:17:06,020 --> 00:17:09,430
 your deep into your bloodstream that is why you get it you

243
00:17:09,430 --> 00:17:12,120
 don't always get it and certainly not from the rest

244
00:17:12,120 --> 00:17:16,240
 so my point being consciousness you could argue it's not a

245
00:17:16,240 --> 00:17:20,440
 problem it's neutral but consciousness is like this rusty

246
00:17:20,440 --> 00:17:24,960
 nail it's got lots of bad things potentially in it or it's

247
00:17:24,960 --> 00:17:32,120
 a it's very nature allows it to be a vessel for bad things

248
00:17:32,120 --> 00:17:38,150
 there's an analogy for you so that's the Dhamma there is

249
00:17:38,150 --> 00:17:42,930
 some Dhamma today you know it's a good teaching you've got

250
00:17:42,930 --> 00:17:48,720
 to stand in awe of the Buddha and how powerful his

251
00:17:48,720 --> 00:17:50,120
 teachings are

252
00:17:50,120 --> 00:17:53,530
 I always sound like a religious zealot and I never thought

253
00:17:53,530 --> 00:17:56,860
 I would be a religious zealot because I always made fun of

254
00:17:56,860 --> 00:18:01,640
 religious zealots but you know if if if they were right and

255
00:18:01,640 --> 00:18:09,120
 they would be they would be justified to be zealous

256
00:18:09,120 --> 00:18:16,460
 just we're right so well I mean don't be quite like that it

257
00:18:16,460 --> 00:18:25,120
's not worth gloating over for sure but Buddha is awesome

258
00:18:25,120 --> 00:18:28,980
 Oh Fernando sent me some subtitles okay let me do that now

259
00:18:28,980 --> 00:18:33,170
 let's see here let's do it while we're online he sent me

260
00:18:33,170 --> 00:18:39,120
 Spanish subtitles and this is an example to everyone

261
00:18:39,120 --> 00:18:43,120
 wait no don't go

262
00:18:43,120 --> 00:19:01,120
 so we're gonna do go find the video

263
00:19:01,120 --> 00:19:06,950
 we've still got this old series on how to meditate and this

264
00:19:06,950 --> 00:19:18,120
 one is how to meditate HD

265
00:19:18,120 --> 00:19:24,440
 we should really redo these videos again no many years old

266
00:19:24,440 --> 00:19:32,280
 already set video language which language English English

267
00:19:32,280 --> 00:19:34,120
 or English UK

268
00:19:34,120 --> 00:19:38,490
 that's funny there's two language choices three language

269
00:19:38,490 --> 00:19:42,480
 choices English and English United Kingdom and French

270
00:19:42,480 --> 00:19:47,150
 Canada why is America the default English now the British

271
00:19:47,150 --> 00:19:49,120
 aren't English enough

272
00:19:49,120 --> 00:19:57,150
 is this through Google yeah English United Kingdom that's

273
00:19:57,150 --> 00:19:59,120
 the default

274
00:19:59,120 --> 00:20:01,820
 okay so we've published we've already published several I

275
00:20:01,820 --> 00:20:04,520
 remember I did a thing back back in the day where I got

276
00:20:04,520 --> 00:20:09,240
 people to send me subtitles so we've got English Indonesian

277
00:20:09,240 --> 00:20:12,120
 Portuguese Swedish and Vietnamese

278
00:20:12,120 --> 00:20:29,120
 so I'm going to add Fernando's Spanish subtitles

279
00:20:29,120 --> 00:20:31,420
 how do you access these Monte are these are these on your

280
00:20:31,420 --> 00:20:36,650
 channel as well you go to CC I think okay so we've got

281
00:20:36,650 --> 00:20:43,120
 Spanish Spanish Latin American Spanish Mexico and Spanish

282
00:20:43,120 --> 00:20:43,120
 Spain

283
00:20:43,120 --> 00:20:48,430
 and those from Spain I think they're going to say I think

284
00:20:48,430 --> 00:20:54,040
 Fernando's from Mexico is Fernando there Fernando which

285
00:20:54,040 --> 00:20:59,120
 country is it well for the flags meditation site Mexico

286
00:20:59,120 --> 00:21:03,410
 which language are you using Spanish Spain or Spanish

287
00:21:03,410 --> 00:21:10,120
 Mexican they're not that different are they

288
00:21:10,120 --> 00:21:17,020
 I don't know most people in my area that speak Spanish are

289
00:21:17,020 --> 00:21:23,560
 from Puerto Rico about 35% of the people in my town are

290
00:21:23,560 --> 00:21:30,120
 from Puerto Rico and that's a little different as well

291
00:21:30,120 --> 00:21:45,120
 so what that's different from Spain

292
00:21:45,120 --> 00:22:01,120
 upload a file

293
00:22:01,120 --> 00:22:12,120
 hola bienvenidos esta ciri de videos a curca de como medita

294
00:22:12,120 --> 00:22:17,950
 and esta premier video hablar a curca de cual es el

295
00:22:17,950 --> 00:22:24,120
 significado de la palabra meditation

296
00:22:24,120 --> 00:22:32,720
 en preme prima prima doguar la palabra meditation puede p

297
00:22:32,720 --> 00:22:42,120
uede puede that must be can it's very much like

298
00:22:42,120 --> 00:22:51,510
 it's like Latin similar anyway but it is probably can pass

299
00:22:51,510 --> 00:22:57,400
 you know 29% of English comes from Latin learn that this

300
00:22:57,400 --> 00:22:58,120
 week

301
00:22:58,120 --> 00:23:00,120
 learning Latin is useful the word possible comes from pos

302
00:23:00,120 --> 00:23:24,120
 and sumas okay it's up I think oh no it's not published

303
00:23:24,120 --> 00:23:28,890
 okay it's up check it out Fernando check it out everyone go

304
00:23:28,890 --> 00:23:33,120
 to my first how to meditate it meditate video

305
00:23:33,120 --> 00:23:42,120
 how do you choose your language Spanish

306
00:23:42,120 --> 00:23:49,690
 hola bienvenidos hi and welcome to this series of videos on

307
00:23:49,690 --> 00:23:53,820
 how to meditate in this the first video I'll be talking

308
00:23:53,820 --> 00:23:59,720
 about what is the meaning it has a thing saying talking

309
00:23:59,720 --> 00:24:01,120
 about

310
00:24:01,120 --> 00:24:04,140
 the DVD that's why people keep are still contacting me

311
00:24:04,140 --> 00:24:08,140
 about the DVD because these videos all talk about it I'll

312
00:24:08,140 --> 00:24:23,110
 have a thing about it to remove that we're not doing the

313
00:24:23,110 --> 00:24:28,120
 DVDs

314
00:24:28,120 --> 00:24:38,660
 the website may mention the DVD as well so yeah it also

315
00:24:38,660 --> 00:24:45,120
 mentions about the downloading a bit torrent is that still

316
00:24:45,120 --> 00:24:48,120
 I don't know so it's still current

317
00:24:48,120 --> 00:24:58,800
 we can get rid of it it's all on YouTube that's good enough

318
00:24:58,800 --> 00:25:02,120
 who needs a DVD

319
00:25:02,120 --> 00:25:19,110
 yes as our how to meditate DVD is available via bit torrent

320
00:25:19,110 --> 00:25:31,120
 so just get rid of that

321
00:25:31,120 --> 00:25:36,220
 okay so how are we doing any questions now you have

322
00:25:36,220 --> 00:25:42,180
 answered all the questions wanted finally come to that

323
00:25:42,180 --> 00:25:43,120
 point

324
00:25:43,120 --> 00:25:48,120
 that's a good thing right

325
00:25:48,120 --> 00:25:57,120
 it's a good thing

326
00:25:57,120 --> 00:26:01,570
 more we're going to try the med mob meditation mob thing

327
00:26:01,570 --> 00:26:08,120
 again we'll see if that works how that works

328
00:26:08,120 --> 00:26:14,120
 this time people are actually going to come out what else

329
00:26:14,120 --> 00:26:20,120
 I think I think I want to mention that the road around

330
00:26:20,120 --> 00:26:23,120
 I believe I want to mention that the road around

331
00:26:24,120 --> 00:26:47,120
 roads I guess it's a set right

332
00:26:47,120 --> 00:26:54,760
 thank you all for coming out for tuning in for practicing

333
00:26:54,760 --> 00:26:56,120
 with us being long list of people

334
00:26:56,120 --> 00:27:00,120
 have a good night and thank you Robin for joining me

335
00:27:00,120 --> 00:27:26,120
 thank you

