1
00:00:00,000 --> 00:00:04,960
 Hello, welcome back to Ask a Monk. Just a reminder to

2
00:00:04,960 --> 00:00:09,280
 everyone, still waiting on videos

3
00:00:09,280 --> 00:00:14,370
 of everyone meditating. So far I have six video responses.

4
00:00:14,370 --> 00:00:17,200
 Don't be shy, please. You're

5
00:00:17,200 --> 00:00:22,550
 doing a great service to the meditation practice. The other

6
00:00:22,550 --> 00:00:24,640
 thing is that if you want to support

7
00:00:24,640 --> 00:00:28,570
 this work, support the things that I do, support these

8
00:00:28,570 --> 00:00:31,400
 videos, don't forget to click on the

9
00:00:31,400 --> 00:00:36,200
 like button if you like the video because that affects the

10
00:00:36,200 --> 00:00:38,420
 search results and allows

11
00:00:38,420 --> 00:00:41,570
 more people to see it and takes it higher and the search

12
00:00:41,570 --> 00:00:43,320
 results makes it featured or

13
00:00:43,320 --> 00:00:47,270
 something. Remember to subscribe and post these videos to

14
00:00:47,270 --> 00:00:49,120
 your favorites so that more

15
00:00:49,120 --> 00:00:53,820
 people can see them if you like them. Okay, so next

16
00:00:53,820 --> 00:00:58,120
 question comes from Sinan who asks,

17
00:00:58,120 --> 00:01:02,680
 "Hi, I'm confused about morals. In reading No More Mr. Nice

18
00:01:02,680 --> 00:01:04,360
 Guy, it seems many people

19
00:01:04,360 --> 00:01:08,340
 are being nice to get approval. Being selfish, is it really

20
00:01:08,340 --> 00:01:10,360
 that bad? Is it really bad to

21
00:01:10,360 --> 00:01:13,520
 have desires and needs? Is it always bad to hurt or make

22
00:01:13,520 --> 00:01:15,760
 others suffer?" I don't know

23
00:01:15,760 --> 00:01:20,410
 if this is a book plug or something. I've never heard of

24
00:01:20,410 --> 00:01:22,760
 this book, but maybe we'll

25
00:01:22,760 --> 00:01:26,890
 keep it out of the question. But yeah, there's several

26
00:01:26,890 --> 00:01:29,560
 parts here. So obviously being nice

27
00:01:29,560 --> 00:01:34,370
 to get approval is less than noble. It's ignoble and it's a

28
00:01:34,370 --> 00:01:39,000
 kind of desire. You want something.

29
00:01:39,000 --> 00:01:42,630
 Being selfish, is it really that bad? And is it really bad

30
00:01:42,630 --> 00:01:44,480
 to have desires and needs?

31
00:01:44,480 --> 00:01:48,810
 Being selfish isn't necessarily bad. The problem is that we

32
00:01:48,810 --> 00:01:51,160
 don't know what's really for our

33
00:01:51,160 --> 00:01:55,700
 benefit. The Buddha said, "When you help yourself, you help

34
00:01:55,700 --> 00:01:57,840
 other people. When you help other

35
00:01:57,840 --> 00:02:02,700
 people, you help yourself." It's one and the same. So by

36
00:02:02,700 --> 00:02:08,720
 extrapolation, when you hurt other

37
00:02:08,720 --> 00:02:11,770
 people, you hurt yourself. And when you hurt yourself, you

38
00:02:11,770 --> 00:02:13,160
 hurt other people. Because you

39
00:02:13,160 --> 00:02:19,050
 affect your own mind and you affect the relationship that

40
00:02:19,050 --> 00:02:22,440
 other people have towards you when you

41
00:02:22,440 --> 00:02:33,780
 do either one. So when we talk about being selfish, we mean

42
00:02:33,780 --> 00:02:34,720
 being concerned with our

43
00:02:34,720 --> 00:02:40,400
 own benefit, with our own welfare. So being stingy or not

44
00:02:40,400 --> 00:02:42,800
 wanting to help other people

45
00:02:42,800 --> 00:02:47,100
 is actually contrary to one's own benefit or welfare. Often

46
00:02:47,100 --> 00:02:49,480
 people who have a great amount

47
00:02:49,480 --> 00:02:52,640
 of craving and attachment don't want to help other people.

48
00:02:52,640 --> 00:02:54,120
 That doesn't mean that they're

49
00:02:54,120 --> 00:02:56,510
 looking out for their best interest. They think they are,

50
00:02:56,510 --> 00:02:57,880
 but they don't realize what is in

51
00:02:57,880 --> 00:03:00,930
 their best interest and that in fact it's in their best

52
00:03:00,930 --> 00:03:02,940
 interest to help other people.

53
00:03:02,940 --> 00:03:07,350
 So selfishness is fine in the sense of wanting to help

54
00:03:07,350 --> 00:03:10,080
 yourself, but in the sense of not

55
00:03:10,080 --> 00:03:15,540
 wanting to help other people or denying other people's

56
00:03:15,540 --> 00:03:18,840
 request for help is detrimental to

57
00:03:18,840 --> 00:03:21,970
 your own well-being. So you can't be said to be looking out

58
00:03:21,970 --> 00:03:23,680
 for your own best interest.

59
00:03:23,680 --> 00:03:27,200
 This is because it's caught up in this kind of craving. So

60
00:03:27,200 --> 00:03:29,080
 when you ask, "Is it really

61
00:03:29,080 --> 00:03:33,970
 bad to have desires and needs?" Yes, because of the nature

62
00:03:33,970 --> 00:03:36,320
 of desires and needs. The more

63
00:03:36,320 --> 00:03:39,180
 needy you are, the more clingy you are, the more desires

64
00:03:39,180 --> 00:03:40,720
 you have, the more suffering

65
00:03:40,720 --> 00:03:45,610
 you have, the less of reality you're able to accept. The

66
00:03:45,610 --> 00:03:47,960
 fewer desires and needs and

67
00:03:47,960 --> 00:03:50,900
 wants that you have, the more of reality that you're able

68
00:03:50,900 --> 00:03:52,360
 to accept. And as a result, the

69
00:03:52,360 --> 00:03:57,830
 more content, peaceful and happy you are. As far as it

70
00:03:57,830 --> 00:03:59,500
 always being bad to hurt or make

71
00:03:59,500 --> 00:04:03,480
 other people suffer, I think it's very much included in

72
00:04:03,480 --> 00:04:05,240
 this. I think for most people

73
00:04:05,240 --> 00:04:10,770
 this isn't really a question. But you're asking an

74
00:04:10,770 --> 00:04:12,080
 interesting question here because

75
00:04:12,080 --> 00:04:14,650
 for most people it's obvious you don't want to hurt other

76
00:04:14,650 --> 00:04:16,320
 people because it causes suffering

77
00:04:16,320 --> 00:04:19,850
 for them. Well, we don't have to enter into that kind of

78
00:04:19,850 --> 00:04:24,080
 mental gymnastics in reality

79
00:04:24,080 --> 00:04:30,300
 in terms of finding a real and a scientific moral because

80
00:04:30,300 --> 00:04:33,120
 you don't have to think about

81
00:04:33,120 --> 00:04:37,800
 the other person's well-being, sacrificing your own well-

82
00:04:37,800 --> 00:04:39,920
being. When you do something

83
00:04:39,920 --> 00:04:43,170
 that is truly beneficial to someone else, you're benefiting

84
00:04:43,170 --> 00:04:44,960
 yourself. When you are helping

85
00:04:44,960 --> 00:04:50,320
 a person, you're also helping yourself. Or in helping

86
00:04:50,320 --> 00:04:53,440
 someone, you factor in your own

87
00:04:53,440 --> 00:04:56,820
 well-being as well. So you do something, not thinking them

88
00:04:56,820 --> 00:04:58,680
 or us, you do something thinking

89
00:04:58,680 --> 00:05:03,480
 about bringing the greatest amount of peace and happiness.

90
00:05:03,480 --> 00:05:05,120
 But in fact, I would say that

91
00:05:05,120 --> 00:05:09,730
 one should put oneself first simply because there's only so

92
00:05:09,730 --> 00:05:11,840
 much you can do for someone

93
00:05:11,840 --> 00:05:16,200
 else. So when we look at the benefit of sacrificing ourself

94
00:05:16,200 --> 00:05:19,000
 for someone else's benefit, we often

95
00:05:19,000 --> 00:05:23,000
 are thinking in terms of helping them externally because

96
00:05:23,000 --> 00:05:25,340
 besides explaining to them the way

97
00:05:25,340 --> 00:05:29,160
 for them to help themselves, you can't bring someone closer

98
00:05:29,160 --> 00:05:31,200
 to enlightenment. You can't

99
00:05:31,200 --> 00:05:34,290
 bring someone to the realization of the truth. They have to

100
00:05:34,290 --> 00:05:37,080
 do that for themselves. So I

101
00:05:37,080 --> 00:05:42,000
 would recommend, as I've said in another video, a sort of

102
00:05:42,000 --> 00:05:44,820
 selfishness in the sense that everyone

103
00:05:44,820 --> 00:05:48,320
 looks out for themselves. Everyone looks after their own

104
00:05:48,320 --> 00:05:50,360
 minds, purifies themselves. And

105
00:05:50,360 --> 00:05:52,950
 I mean, this has been repeated over and over again

106
00:05:52,950 --> 00:05:54,880
 throughout spirituality. If you want

107
00:05:54,880 --> 00:05:59,240
 to change the world, change yourself. Start from within.

108
00:05:59,240 --> 00:06:01,400
 The path to peace lies inside

109
00:06:01,400 --> 00:06:07,570
 of all of ourselves. If we are accepting open, caring

110
00:06:07,570 --> 00:06:12,400
 beings, then we've achieved world peace,

111
00:06:12,400 --> 00:06:16,020
 if we can start from within. So I would say that one should

112
00:06:16,020 --> 00:06:17,520
 look to one's own benefit

113
00:06:17,520 --> 00:06:19,820
 and not sacrifice it for other people's benefit. That's

114
00:06:19,820 --> 00:06:21,400
 actually what the Buddha said. There's

115
00:06:21,400 --> 00:06:24,030
 a quote Buddha said, "When you know what's really in your

116
00:06:24,030 --> 00:06:25,560
 own best interest, you should

117
00:06:25,560 --> 00:06:28,170
 follow that." And the thing is that that is for the best

118
00:06:28,170 --> 00:06:29,640
 interest of the world because

119
00:06:29,640 --> 00:06:33,130
 that is doing your job to promote world peace, to promote

120
00:06:33,130 --> 00:06:35,360
 happiness, to promote harmony in

121
00:06:35,360 --> 00:06:41,160
 the world. When you have cleaned your mind and your reality

122
00:06:41,160 --> 00:06:44,360
, then everything you touch,

123
00:06:44,360 --> 00:06:50,430
 everything you affect is benefited. So you have done your

124
00:06:50,430 --> 00:06:53,200
 job as opposed to trying to

125
00:06:53,200 --> 00:06:55,490
 change other people or change the world and forgetting

126
00:06:55,490 --> 00:06:56,960
 about your own benefit. I've talked

127
00:06:56,960 --> 00:07:02,080
 about this before in another video. But I think it's an

128
00:07:02,080 --> 00:07:05,480
 interesting question and the most

129
00:07:05,480 --> 00:07:08,300
 interesting part of it is the fact that if you ask me and

130
00:07:08,300 --> 00:07:10,760
 if you read the Buddha's teaching,

131
00:07:10,760 --> 00:07:14,220
 the best thing to do is to help yourself because that's

132
00:07:14,220 --> 00:07:16,080
 considered doing your job. And if everyone

133
00:07:16,080 --> 00:07:18,550
 were to do their own job, to do their own work and to clean

134
00:07:18,550 --> 00:07:19,920
 their own minds, to purify

135
00:07:19,920 --> 00:07:25,800
 themselves, then the world would be free from suffering and

136
00:07:25,800 --> 00:07:28,880
 stress and evil of all kinds.

137
00:07:28,880 --> 00:07:30,080
 Okay, so thanks for the question.

