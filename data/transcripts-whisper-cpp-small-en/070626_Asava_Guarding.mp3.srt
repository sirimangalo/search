1
00:00:00,000 --> 00:00:07,330
 Today I will be talking about the second in this series of

2
00:00:07,330 --> 00:00:10,440
 talks on the Asavat, the second

3
00:00:10,440 --> 00:00:17,340
 in the series of groups of Asavat, of defilements, which

4
00:00:17,340 --> 00:00:21,880
 cause the mind to ferment or cause the

5
00:00:21,880 --> 00:00:23,280
 mind to go sour.

6
00:00:23,280 --> 00:00:29,450
 It's just a poetic way of saying they make the mind dirty

7
00:00:29,450 --> 00:00:32,600
 or they destroy the mind.

8
00:00:32,600 --> 00:00:36,840
 And the second group of Asavat, or the second method,

9
00:00:36,840 --> 00:00:39,080
 really it's a method to overcome the

10
00:00:39,080 --> 00:00:43,970
 Asavat or a certain of mental defilement or mental

11
00:00:43,970 --> 00:00:45,600
 pollution.

12
00:00:45,600 --> 00:00:51,040
 And the second type is called Samwara Pahatapa.

13
00:00:51,040 --> 00:00:55,620
 Those Asavat or those mental pollutions which should be

14
00:00:55,620 --> 00:00:58,800
 removed through guarding or through

15
00:00:58,800 --> 00:00:59,800
 restraining.

16
00:00:59,800 --> 00:01:05,120
 And in this case it means restraining the six senses.

17
00:01:05,120 --> 00:01:11,760
 So today I'll be talking about guarding the six senses.

18
00:01:11,760 --> 00:01:16,480
 First a little bit more about the Asavat or about mental

19
00:01:16,480 --> 00:01:19,880
 pollutions, or mental fermentation

20
00:01:19,880 --> 00:01:29,510
 or things which make the mind rotten, make the mind go sour

21
00:01:29,510 --> 00:01:29,720
.

22
00:01:29,720 --> 00:01:32,820
 It's well known that the Lord Buddha's teaching is for the

23
00:01:32,820 --> 00:01:34,520
 purpose of the purification of

24
00:01:34,520 --> 00:01:35,520
 the mind.

25
00:01:35,520 --> 00:01:39,200
 Of course in the Satipatanasuta it's the first goal of the

26
00:01:39,200 --> 00:01:41,560
 practice, Satanangvistuddhiya,

27
00:01:41,560 --> 00:01:43,040
 the purification of beings.

28
00:01:43,040 --> 00:01:51,800
 In this case the purification of the minds of beings.

29
00:01:51,800 --> 00:01:56,290
 And in the Satipatanasuta of course it's very clear how

30
00:01:56,290 --> 00:01:59,000
 this comes about, how it comes about

31
00:01:59,000 --> 00:02:01,440
 that our mind becomes pure.

32
00:02:01,440 --> 00:02:04,610
 We all want our minds to be pure but we can't figure out

33
00:02:04,610 --> 00:02:06,600
 how it is that we can do away with

34
00:02:06,600 --> 00:02:10,520
 these things which cause stress and which cause suffering

35
00:02:10,520 --> 00:02:14,080
 and which cause strife and quarrel

36
00:02:14,080 --> 00:02:24,400
 inside of ourselves and among between us and other people.

37
00:02:24,400 --> 00:02:29,100
 So the Lord Buddha laid down a course of practice which

38
00:02:29,100 --> 00:02:32,080
 leads us to make our minds pure.

39
00:02:32,080 --> 00:02:36,980
 And in the Sabasva sutta the Lord Buddha gave several

40
00:02:36,980 --> 00:02:40,320
 methods or several directions we can

41
00:02:40,320 --> 00:02:41,600
 take in order to overcome.

42
00:02:41,600 --> 00:02:45,730
 In the end it all comes all back to the four foundations of

43
00:02:45,730 --> 00:02:48,160
 mindfulness which were taught

44
00:02:48,160 --> 00:02:51,450
 in the Satipatanasuta where the Lord Buddha said, "Kacant

45
00:02:51,450 --> 00:02:53,600
owa gachamiti bachamati," and

46
00:02:53,600 --> 00:02:54,960
 so on.

47
00:02:54,960 --> 00:02:58,800
 When going he knows I'm going.

48
00:02:58,800 --> 00:03:04,400
 When standing he knows I'm standing, or she.

49
00:03:04,400 --> 00:03:06,000
 When sitting one knows one is sitting.

50
00:03:06,000 --> 00:03:07,800
 When lying one knows one is lying.

51
00:03:07,800 --> 00:03:12,210
 When one has a painful feeling one knows I'm experiencing a

52
00:03:12,210 --> 00:03:13,760
 painful feeling.

53
00:03:13,760 --> 00:03:17,750
 When one has a thought of greed one knows this is a thought

54
00:03:17,750 --> 00:03:18,680
 of greed.

55
00:03:18,680 --> 00:03:23,100
 When one's mind is full of anger one knows this mind is

56
00:03:23,100 --> 00:03:24,600
 full of anger.

57
00:03:24,600 --> 00:03:26,720
 And the key here is one knows.

58
00:03:26,720 --> 00:03:33,520
 One knows the bare truth of the phenomenon.

59
00:03:33,520 --> 00:03:37,360
 And this comes back to the very essence of what it means to

60
00:03:37,360 --> 00:03:38,400
 be mindful.

61
00:03:38,400 --> 00:03:41,740
 The very essence of the word sati which means really to

62
00:03:41,740 --> 00:03:42,640
 remember.

63
00:03:42,640 --> 00:03:47,320
 And I mean simply to remember, to bring about remembrance

64
00:03:47,320 --> 00:03:50,240
 in the mind of the pure and clear

65
00:03:50,240 --> 00:03:53,560
 nature of the reality.

66
00:03:53,560 --> 00:03:56,530
 And it's something which I've started to call in English in

67
00:03:56,530 --> 00:03:58,200
 order to explain to Westerners

68
00:03:58,200 --> 00:04:02,280
 clear thought.

69
00:04:02,280 --> 00:04:04,640
 In the West it's sometimes very difficult to understand why

70
00:04:04,640 --> 00:04:05,720
 we would go around all day

71
00:04:05,720 --> 00:04:09,640
 saying to ourselves, "Rising, falling."

72
00:04:09,640 --> 00:04:12,290
 Why we would, when we walk we say, "Walking, walking," when

73
00:04:12,290 --> 00:04:13,760
 we say, "Sitting, sitting."

74
00:04:13,760 --> 00:04:16,920
 When we say, "Seeing, hearing, smelling, tasting, feeling."

75
00:04:16,920 --> 00:04:21,810
 Why we say to ourselves again and again as though we were

76
00:04:21,810 --> 00:04:22,840
 robots.

77
00:04:22,840 --> 00:04:26,830
 And so we can see that sometimes it comes across in the

78
00:04:26,830 --> 00:04:28,880
 wrong way because of how it's

79
00:04:28,880 --> 00:04:34,080
 explained and because it's explained in a very robotic, in

80
00:04:34,080 --> 00:04:36,760
 a very mechanical fashion.

81
00:04:36,760 --> 00:04:39,680
 But if we come back and explain why it is that we're doing

82
00:04:39,680 --> 00:04:40,920
 this it's easy to see that

83
00:04:40,920 --> 00:04:43,320
 there is a real purpose to it.

84
00:04:43,320 --> 00:04:46,820
 It's for the purpose of creating clear thought.

85
00:04:46,820 --> 00:04:48,910
 Because in our minds it's not true that if we don't do this

86
00:04:48,910 --> 00:04:50,480
 our minds are not going to

87
00:04:50,480 --> 00:04:52,600
 think anything.

88
00:04:52,600 --> 00:04:55,750
 And this is maybe a problem which we come to the meditation

89
00:04:55,750 --> 00:04:56,280
 with.

90
00:04:56,280 --> 00:04:59,960
 We think the meditation is for the purpose of not thinking

91
00:04:59,960 --> 00:05:01,400
 anything anymore.

92
00:05:01,400 --> 00:05:04,920
 That maybe somehow we can just stop thinking.

93
00:05:04,920 --> 00:05:08,640
 And then here we give the meditator a new set of thoughts.

94
00:05:08,640 --> 00:05:10,000
 When you see, think seeing.

95
00:05:10,000 --> 00:05:14,600
 When you hear, think hearing and so on.

96
00:05:14,600 --> 00:05:16,960
 And I think this isn't really what they expect.

97
00:05:16,960 --> 00:05:22,930
 But the truth is any practice you do that blocks out all

98
00:05:22,930 --> 00:05:26,320
 thought, you might find a way

99
00:05:26,320 --> 00:05:31,690
 to block out your thought for a minute or an hour or even a

100
00:05:31,690 --> 00:05:33,200
 day or so on.

101
00:05:33,200 --> 00:05:39,120
 It can't possibly bring about that state of being forever.

102
00:05:39,120 --> 00:05:43,180
 As soon as the power of our concentration which blocked out

103
00:05:43,180 --> 00:05:44,920
 the thought disappears the

104
00:05:44,920 --> 00:05:47,800
 thoughts will come back.

105
00:05:47,800 --> 00:05:58,240
 And so we have to come to terms with the thoughts

106
00:05:58,240 --> 00:06:02,280
 themselves, not actually instead

107
00:06:02,280 --> 00:06:05,920
 of trying to destroy all thoughts, have there be no

108
00:06:05,920 --> 00:06:06,920
 thoughts.

109
00:06:06,920 --> 00:06:08,900
 Instead we have to come to terms with the nature of the

110
00:06:08,900 --> 00:06:09,480
 thoughts.

111
00:06:09,480 --> 00:06:14,200
 We have to actually be responsible for our thoughts.

112
00:06:14,200 --> 00:06:17,780
 We can't let our thoughts wander off into greed and anger

113
00:06:17,780 --> 00:06:18,880
 and delusion.

114
00:06:18,880 --> 00:06:21,330
 Because our minds will continue to think and think about

115
00:06:21,330 --> 00:06:23,080
 the past and the future and create

116
00:06:23,080 --> 00:06:27,960
 all sorts of mental constructs inside.

117
00:06:27,960 --> 00:06:32,360
 And based on these constructs we'll create views and conce

118
00:06:32,360 --> 00:06:34,720
it and stinginess and jealousy

119
00:06:34,720 --> 00:06:37,760
 and so on and so on.

120
00:06:37,760 --> 00:06:42,360
 Cruelty, deceit and so on.

121
00:06:42,360 --> 00:06:46,240
 All sorts of other, all sorts of defilements, things which

122
00:06:46,240 --> 00:06:48,280
 cause the mind to ferment, to

123
00:06:48,280 --> 00:06:49,280
 go rotten.

124
00:06:49,280 --> 00:06:55,390
 And keep the mind from being pure and being clean, being

125
00:06:55,390 --> 00:06:56,520
 fresh.

126
00:06:56,520 --> 00:07:02,600
 Which make the mind pickle the mind.

127
00:07:02,600 --> 00:07:09,010
 So the purpose of meditation then is not to do away with

128
00:07:09,010 --> 00:07:12,560
 our natural state of being, as

129
00:07:12,560 --> 00:07:16,360
 in thinking or any other state of being, any sort of pain

130
00:07:16,360 --> 00:07:18,640
 in the body or state in the body,

131
00:07:18,640 --> 00:07:25,140
 but to purify, to make it pure, to make our actions, our

132
00:07:25,140 --> 00:07:28,760
 words and our thoughts pure.

133
00:07:28,760 --> 00:07:32,820
 So at the time when we say to ourselves, rising, falling,

134
00:07:32,820 --> 00:07:34,680
 all we're trying to do is come to

135
00:07:34,680 --> 00:07:38,320
 terms with something which is really and truly there.

136
00:07:38,320 --> 00:07:40,970
 When we're walking, while we're walking anyway, but instead

137
00:07:40,970 --> 00:07:42,440
 of letting our minds think while

138
00:07:42,440 --> 00:07:47,400
 we're going to think anyway, instead of letting our minds

139
00:07:47,400 --> 00:07:50,280
 think about how wonderful our body

140
00:07:50,280 --> 00:07:54,990
 is or how awful our body is or create all sorts of

141
00:07:54,990 --> 00:07:58,520
 delusions about what we're doing

142
00:07:58,520 --> 00:08:00,800
 at this moment.

143
00:08:00,800 --> 00:08:04,230
 Instead we simply come to terms with the bare reality of

144
00:08:04,230 --> 00:08:05,160
 the action.

145
00:08:05,160 --> 00:08:08,340
 So when we walk, walking, walking, and when we see things,

146
00:08:08,340 --> 00:08:09,920
 when we hear things, when we

147
00:08:09,920 --> 00:08:13,170
 smell things, when we taste, when we feel, when we think

148
00:08:13,170 --> 00:08:14,920
 anything, we come to terms with

149
00:08:14,920 --> 00:08:16,720
 the bare reality.

150
00:08:16,720 --> 00:08:21,840
 And so this is today's topic on specifically the six senses

151
00:08:21,840 --> 00:08:22,200
.

152
00:08:22,200 --> 00:08:26,730
 The seeing, hearing, smelling, tasting, feeling and

153
00:08:26,730 --> 00:08:27,800
 thinking.

154
00:08:27,800 --> 00:08:32,840
 And this is another very important core concept in the

155
00:08:32,840 --> 00:08:36,400
 Buddha's teaching, is the teaching

156
00:08:36,400 --> 00:08:39,320
 on the senses.

157
00:08:39,320 --> 00:08:44,620
 Because for most people in the world, these are a reality

158
00:08:44,620 --> 00:08:47,240
 which is overlooked when we

159
00:08:47,240 --> 00:08:51,300
 interact with the world around us.

160
00:08:51,300 --> 00:08:56,820
 Everything we see is simply a sight, simply a vision,

161
00:08:56,820 --> 00:09:00,280
 simply light touching the eye.

162
00:09:00,280 --> 00:09:03,880
 Everything we hear is simply a sound touching the ear.

163
00:09:03,880 --> 00:09:08,150
 Everything we smell is simply a scent touching the nose,

164
00:09:08,150 --> 00:09:10,520
 taste touching the tongue, thoughts

165
00:09:10,520 --> 00:09:14,920
 touching the body, sorry, sensations touching the body and

166
00:09:14,920 --> 00:09:17,280
 thoughts coming in contact with

167
00:09:17,280 --> 00:09:18,280
 the mind.

168
00:09:18,280 --> 00:09:23,920
 Our thoughts are rising in the mind, I would say.

169
00:09:23,920 --> 00:09:26,560
 But what happens for the average person is when we see

170
00:09:26,560 --> 00:09:28,320
 something, it becomes something

171
00:09:28,320 --> 00:09:33,420
 either desirable or something undesirable, something

172
00:09:33,420 --> 00:09:37,920
 pleasant or something unpleasant.

173
00:09:37,920 --> 00:09:40,020
 Based on our past experiences in the past, this is

174
00:09:40,020 --> 00:09:41,680
 something which has brought pleasure

175
00:09:41,680 --> 00:09:42,760
 to us.

176
00:09:42,760 --> 00:09:45,840
 In the past, this is something which has brought pain to us

177
00:09:45,840 --> 00:09:49,120
 and we're afraid or we're excited

178
00:09:49,120 --> 00:09:53,480
 about the fruit which it will bring this time as well.

179
00:09:53,480 --> 00:09:57,130
 And based on this memory, we then create our thoughts about

180
00:09:57,130 --> 00:09:57,560
 it.

181
00:09:57,560 --> 00:10:00,520
 And this is where our impure thoughts arise.

182
00:10:00,520 --> 00:10:03,540
 So if it has brought us happiness in the past, we want to

183
00:10:03,540 --> 00:10:04,160
 get it.

184
00:10:04,160 --> 00:10:06,560
 And we seek out for it.

185
00:10:06,560 --> 00:10:11,220
 We seek out for a way to come to possess it and we find

186
00:10:11,220 --> 00:10:14,800
 ourselves leaving behind our duties

187
00:10:14,800 --> 00:10:23,160
 and our obligations in search for pleasure, in search for

188
00:10:23,160 --> 00:10:26,160
 the objects of our desire.

189
00:10:26,160 --> 00:10:31,160
 And we find ourselves unable to do without these things.

190
00:10:31,160 --> 00:10:34,680
 And on the other hand, when bad things, unpleasant things

191
00:10:34,680 --> 00:10:36,880
 come, things which we don't like, we

192
00:10:36,880 --> 00:10:39,800
 will do anything to be free from them.

193
00:10:39,800 --> 00:10:42,820
 Sometimes we will say bad things or do bad things, simply

194
00:10:42,820 --> 00:10:44,200
 do not have to see and hear

195
00:10:44,200 --> 00:10:49,310
 and smell and taste and feel and think these bad things

196
00:10:49,310 --> 00:10:50,480
 anymore.

197
00:10:50,480 --> 00:10:53,360
 Now somewhere, the Lord Buddha's teaching on how to destroy

198
00:10:53,360 --> 00:10:54,560
 these defilements which

199
00:10:54,560 --> 00:10:59,770
 arise at the eye, the ear, the nose, the tongue, the body

200
00:10:59,770 --> 00:11:02,000
 and the heart is simply the same

201
00:11:02,000 --> 00:11:05,080
 Lord Buddha's same teaching on mindfulness.

202
00:11:05,080 --> 00:11:08,680
 Simple remembrance reminding ourselves that when we see,

203
00:11:08,680 --> 00:11:10,120
 this is only seeing.

204
00:11:10,120 --> 00:11:15,240
 Ah, so what is it that we do when we see something?

205
00:11:15,240 --> 00:11:18,000
 Why is it that we say to ourselves, "Seeing?"

206
00:11:18,000 --> 00:11:21,250
 We're creating a bare remembrance in our mind, a clear

207
00:11:21,250 --> 00:11:23,240
 thought of what is the reality of

208
00:11:23,240 --> 00:11:24,880
 the experience.

209
00:11:24,880 --> 00:11:28,130
 So it's no longer a bad thing or a good thing, something

210
00:11:28,130 --> 00:11:29,800
 which is going to lead us to get

211
00:11:29,800 --> 00:11:35,820
 angry or get greedy or lead to all sorts of views and conce

212
00:11:35,820 --> 00:11:37,800
its and ideas.

213
00:11:37,800 --> 00:11:41,880
 Instead it's simply going to be light touching the eye.

214
00:11:41,880 --> 00:11:45,900
 And not only does this prevent the bad things from arising,

215
00:11:45,900 --> 00:11:48,000
 it also creates a deeper fruit

216
00:11:48,000 --> 00:11:51,440
 as well which is the fruit of insight.

217
00:11:51,440 --> 00:11:54,630
 It helps us to see that really the whole world around us is

218
00:11:54,630 --> 00:11:55,600
 simply this.

219
00:11:55,600 --> 00:12:01,750
 It's simply six things, sights, sounds, smells, tastes,

220
00:12:01,750 --> 00:12:04,360
 feelings and thoughts.

221
00:12:04,360 --> 00:12:09,020
 The problem is 90% of what we think is real is simply

222
00:12:09,020 --> 00:12:10,120
 thought.

223
00:12:10,120 --> 00:12:14,840
 When we see something it becomes trees, it becomes people,

224
00:12:14,840 --> 00:12:17,480
 it becomes beautiful, it becomes

225
00:12:17,480 --> 00:12:24,320
 ugly, it becomes me and mine and them and theirs.

226
00:12:24,320 --> 00:12:27,130
 We create all sorts of views and leaps about the things

227
00:12:27,130 --> 00:12:28,840
 which we see when actually it's

228
00:12:28,840 --> 00:12:32,160
 simply light touching the eye.

229
00:12:32,160 --> 00:12:39,680
 We start to see that the reality of the whole world around

230
00:12:39,680 --> 00:12:42,040
 us is simply a very simple, a

231
00:12:42,040 --> 00:12:46,530
 very ordinary thing which is sight and then sound and smell

232
00:12:46,530 --> 00:12:48,520
 and taste and feeling and

233
00:12:48,520 --> 00:12:51,120
 thought and these things arise and cease and arise and

234
00:12:51,120 --> 00:12:51,680
 cease.

235
00:12:51,680 --> 00:12:54,460
 This is where we come to see the truth of what the Lord

236
00:12:54,460 --> 00:12:56,240
 Buddha called impermanence,

237
00:12:56,240 --> 00:12:59,480
 suffering and non-self.

238
00:12:59,480 --> 00:13:03,840
 First we see that there is the mind and there is the body.

239
00:13:03,840 --> 00:13:10,640
 The body is the light and the eye or the sound in the ear.

240
00:13:10,640 --> 00:13:15,770
 And there is the mind which goes to touch, touch the eye,

241
00:13:15,770 --> 00:13:17,400
 touch the ear.

242
00:13:17,400 --> 00:13:19,680
 Sometimes we can see, we can be looking at something with

243
00:13:19,680 --> 00:13:21,160
 our eyes but our mind is somewhere

244
00:13:21,160 --> 00:13:25,680
 else so we don't even see what our eyes are looking at.

245
00:13:25,680 --> 00:13:30,390
 Sometimes we are, a sound comes to the ear, someone calls

246
00:13:30,390 --> 00:13:30,960
 us.

247
00:13:30,960 --> 00:13:33,290
 But because our mind is preoccupied with something else we

248
00:13:33,290 --> 00:13:37,600
 don't even hear the sound or a smell

249
00:13:37,600 --> 00:13:38,840
 or a taste.

250
00:13:38,840 --> 00:13:40,880
 Sometimes we are eating food and it's very tasty but our

251
00:13:40,880 --> 00:13:42,280
 minds are thinking about something

252
00:13:42,280 --> 00:13:43,280
 else.

253
00:13:43,280 --> 00:13:46,480
 We don't even taste the food which we are eating.

254
00:13:46,480 --> 00:13:48,680
 And so when we practice we start to see this with our

255
00:13:48,680 --> 00:13:51,000
 things, it's called body and mind.

256
00:13:51,000 --> 00:13:54,530
 And unless the two are together there will be no arising of

257
00:13:54,530 --> 00:13:56,320
 a sight, a sound, a smell,

258
00:13:56,320 --> 00:13:57,560
 a taste, a feeling and thought.

259
00:13:57,560 --> 00:14:03,680
 But when they come together, one of these six things will

260
00:14:03,680 --> 00:14:04,800
 arise.

261
00:14:04,800 --> 00:14:07,530
 And then we start to see this deeper truth that these

262
00:14:07,530 --> 00:14:09,760
 things, this seeing, this hearing,

263
00:14:09,760 --> 00:14:13,190
 this smelling, this tasting, this feeling and this thinking

264
00:14:13,190 --> 00:14:14,720
 is not me, it's not mine,

265
00:14:14,720 --> 00:14:17,920
 it's not permanent and it's not under my control.

266
00:14:17,920 --> 00:14:23,670
 It's not something which is subject to being the way I want

267
00:14:23,670 --> 00:14:26,000
 it to be all the time.

268
00:14:26,000 --> 00:14:30,330
 It is something which will often be in a certain way that I

269
00:14:30,330 --> 00:14:32,560
 don't wish it for it to be.

270
00:14:32,560 --> 00:14:35,570
 So I will often see things which are unpleasant, hear

271
00:14:35,570 --> 00:14:37,640
 things which are unpleasant, smell things

272
00:14:37,640 --> 00:14:41,100
 which are unpleasant and so on.

273
00:14:41,100 --> 00:14:43,440
 And this is something which I can't avoid.

274
00:14:43,440 --> 00:14:46,810
 So we see these three, what the lone Buddha called the

275
00:14:46,810 --> 00:14:48,840
 three characteristics, the three

276
00:14:48,840 --> 00:14:50,480
 natural realities.

277
00:14:50,480 --> 00:14:53,210
 Just like in science we talk about the characteristics of

278
00:14:53,210 --> 00:14:55,280
 matter and the characteristics of energy

279
00:14:55,280 --> 00:14:56,280
 and so on.

280
00:14:56,280 --> 00:15:00,080
 Well, the Buddha was a very pure scientist and he said that

281
00:15:00,080 --> 00:15:02,720
 reality has three characteristics.

282
00:15:02,720 --> 00:15:08,630
 It's impermanent, it's unsatisfying and it's not under our

283
00:15:08,630 --> 00:15:09,880
 control.

284
00:15:09,880 --> 00:15:13,830
 And when we see this, this is what changes our mind so that

285
00:15:13,830 --> 00:15:15,520
 we don't want to hold on

286
00:15:15,520 --> 00:15:16,520
 to these things.

287
00:15:16,520 --> 00:15:19,980
 We don't want to seek out, seek after these things which we

288
00:15:19,980 --> 00:15:21,800
 used to find pleasant, which

289
00:15:21,800 --> 00:15:23,920
 we used to think were going to bring us happiness.

290
00:15:23,920 --> 00:15:26,790
 We can see that these are things which actually pickle the

291
00:15:26,790 --> 00:15:28,480
 mind, which actually cause the

292
00:15:28,480 --> 00:15:34,730
 mind to go rotten, which create addiction, which create all

293
00:15:34,730 --> 00:15:37,600
 sorts of displeasure, all

294
00:15:37,600 --> 00:15:41,280
 sorts of frustration and anger and hatred.

295
00:15:41,280 --> 00:15:47,360
 And we start to let go of all of our illusions, all of our

296
00:15:47,360 --> 00:15:50,640
 old habits, all of our old ways

297
00:15:50,640 --> 00:15:52,760
 of reacting to things.

298
00:15:52,760 --> 00:15:55,980
 And we find that that's simply all that there is in this

299
00:15:55,980 --> 00:15:59,200
 body and in this mind, is old habits,

300
00:15:59,200 --> 00:16:02,380
 old ways of reacting to things which are really no longer

301
00:16:02,380 --> 00:16:04,440
 applicable now that we can see that

302
00:16:04,440 --> 00:16:07,440
 these are only seeing, hearing, smelling, tasting, feeling

303
00:16:07,440 --> 00:16:08,280
 and thinking.

304
00:16:08,280 --> 00:16:13,270
 And we start to find these, become bored, become disench

305
00:16:13,270 --> 00:16:15,720
anted with these old ways of

306
00:16:15,720 --> 00:16:16,720
 reacting.

307
00:16:16,720 --> 00:16:19,680
 And we come to see where true peace and true happiness lies

308
00:16:19,680 --> 00:16:21,200
 and that is in simply having

309
00:16:21,200 --> 00:16:24,910
 a clear thought about the present reality, to know simply

310
00:16:24,910 --> 00:16:27,360
 what is this that we're experiencing

311
00:16:27,360 --> 00:16:29,380
 right here and now.

312
00:16:29,380 --> 00:16:32,270
 As we sit here, what is happening around us, there are

313
00:16:32,270 --> 00:16:34,400
 sights, there are sounds, there

314
00:16:34,400 --> 00:16:37,330
 are smells, there are tastes, there are feelings and there

315
00:16:37,330 --> 00:16:38,960
 are thoughts, and these make up

316
00:16:38,960 --> 00:16:41,040
 the universe.

317
00:16:41,040 --> 00:16:46,800
 And when we see in this way, when we practice in this way,

318
00:16:46,800 --> 00:16:49,600
 we will come to reach what is

319
00:16:49,600 --> 00:16:53,800
 the goal of life, the goal of Buddhism, the goal of the

320
00:16:53,800 --> 00:16:55,840
 meditation practice.

321
00:16:55,840 --> 00:17:00,350
 And that is of course the path and the fruition, the nibb

322
00:17:00,350 --> 00:17:01,840
ana of freedom.

323
00:17:01,840 --> 00:17:06,440
 To put it simply, it means to become free.

324
00:17:06,440 --> 00:17:11,720
 We will become unbound, we will become free as a bird from

325
00:17:11,720 --> 00:17:12,680
 a cage.

326
00:17:12,680 --> 00:17:14,760
 We no longer will imprison ourselves.

327
00:17:14,760 --> 00:17:18,310
 We will no longer be imprisoned by our addictions and by

328
00:17:18,310 --> 00:17:19,600
 our aversions.

329
00:17:19,600 --> 00:17:24,620
 We will be able to live in this world, to go anywhere in

330
00:17:24,620 --> 00:17:27,640
 peace and in freedom, without

331
00:17:27,640 --> 00:17:31,890
 any sort of fetter, without any sort of bond, without any

332
00:17:31,890 --> 00:17:34,040
 fear or any angst or any worry

333
00:17:34,040 --> 00:17:39,920
 or any stress, taking everything as it comes because we see

334
00:17:39,920 --> 00:17:42,160
 it as for what it is.

335
00:17:42,160 --> 00:17:46,540
 This is the Lord Buddha's teaching on samvara apahata-pah,

336
00:17:46,540 --> 00:17:48,720
 the method of overcoming the

337
00:17:48,720 --> 00:17:54,470
 bad things in the mind by guarding or by restraining the

338
00:17:54,470 --> 00:17:58,880
 senses, by not letting our minds fly out,

339
00:17:58,880 --> 00:18:03,240
 follow out after the objects of the sense.

340
00:18:03,240 --> 00:18:04,240
 And that's all for today.

341
00:18:04,240 --> 00:18:05,880
 Now we'll practice together.

