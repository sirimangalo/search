1
00:00:00,000 --> 00:00:09,090
 She would come to the sessions crying and she would cry and

2
00:00:09,090 --> 00:00:11,000
 I never asked once what the problem was.

3
00:00:11,000 --> 00:00:13,680
 I didn't know what the problem was. Obviously there was

4
00:00:13,680 --> 00:00:17,000
 something intense that she actually wanted me to ask

5
00:00:17,000 --> 00:00:19,820
 and was used to people asking what's wrong with you and so

6
00:00:19,820 --> 00:00:21,000
 on. So I didn't.

7
00:00:21,000 --> 00:00:28,670
 And I challenged her in this way by not and by being kind

8
00:00:28,670 --> 00:00:33,730
 and you know, really I guess an important part is by being

9
00:00:33,730 --> 00:00:35,000
 not interested.

10
00:00:35,000 --> 00:00:40,000
 You know, kind of like yes, yes. That's the way it is.

11
00:00:40,000 --> 00:00:45,200
 And as a result teaching her that yeah, it is like that and

12
00:00:45,200 --> 00:00:48,000
 helping her to accept and overcome and let go.

13
00:00:48,000 --> 00:00:53,110
 Finally after the course and after she had been with us for

14
00:00:53,110 --> 00:00:57,300
 some time, I asked her or she told, I didn't really ask but

15
00:00:57,300 --> 00:00:59,000
 she told me what had happened.

16
00:00:59,000 --> 00:01:07,460
 And obviously she was 41 at the time and you wouldn't be

17
00:01:07,460 --> 00:01:10,000
 able to tell it.

18
00:01:10,000 --> 00:01:14,490
 It was really something that had crippled her for so many

19
00:01:14,490 --> 00:01:19,290
 years for let's say 30, 40 years. 30 years she would have

20
00:01:19,290 --> 00:01:20,000
 been crippled.

21
00:01:20,000 --> 00:01:25,540
 She lost a better part of her life to that from looks of it

22
00:01:25,540 --> 00:01:26,000
.

23
00:01:26,000 --> 00:01:29,270
 She left and when she came back she wanted to do another

24
00:01:29,270 --> 00:01:32,000
 course. She looked like a totally different person.

25
00:01:32,000 --> 00:01:35,560
 She had color in her face or her eyes at all, another look

26
00:01:35,560 --> 00:01:37,000
 about her and so on.

27
00:01:37,000 --> 00:01:41,000
 But clearly this is something devastating.

28
00:01:41,000 --> 00:01:45,430
 So, you know, it's wrong. And that's the first thing I want

29
00:01:45,430 --> 00:01:48,000
 to say is just because I say this doesn't break a precept

30
00:01:48,000 --> 00:01:51,500
 or that doesn't break a precept doesn't mean by any sense

31
00:01:51,500 --> 00:01:53,000
 that it's right.

32
00:01:53,000 --> 00:01:55,600
 That's why I say the precepts are kind of curious in this

33
00:01:55,600 --> 00:01:56,000
 way.

34
00:01:56,000 --> 00:02:01,140
 The fifth precept actually, it's a very small rule for

35
00:02:01,140 --> 00:02:07,100
 monks. For a monk to drink alcohol, it's not a small rule

36
00:02:07,100 --> 00:02:09,000
 but it's not one of the major rules.

37
00:02:09,000 --> 00:02:15,160
 You're still a monk. A monk can get drunk, stinking drunk,

38
00:02:15,160 --> 00:02:20,390
 pass out and still not have to undergo any probation or

39
00:02:20,390 --> 00:02:23,000
 anything of the sort.

40
00:02:23,000 --> 00:02:26,850
 That doesn't mean it's terribly wrong. It just means that

41
00:02:26,850 --> 00:02:30,280
 it's, you know, it puts it in its place. This is one of the

42
00:02:30,280 --> 00:02:32,730
 precepts and this is one of the things that you shouldn't

43
00:02:32,730 --> 00:02:33,000
 do.

44
00:02:33,000 --> 00:02:41,830
 The punishment side of things is, or the weight of the

45
00:02:41,830 --> 00:02:46,000
 precepts, it's not indicative of how bad the act is.

46
00:02:46,000 --> 00:02:48,970
 It's just, you know, filing things as being wrong and

47
00:02:48,970 --> 00:02:52,300
 saying that this is something that you're not to do. If you

48
00:02:52,300 --> 00:02:54,000
 do it, you should convince.

49
00:02:54,000 --> 00:03:00,790
 You should, what's the word? You should confess. Confess

50
00:03:00,790 --> 00:03:07,000
 that you've done it. See, too many languages.

51
00:03:07,000 --> 00:03:12,360
 So, but I would say in this case, rape probably, or it

52
00:03:12,360 --> 00:03:17,000
 could be seen to fit in with the third precept.

53
00:03:17,000 --> 00:03:29,370
 And so I would say any sensual engagement, romantic

54
00:03:29,370 --> 00:03:42,660
 engagement with another human being that causes, that

55
00:03:42,660 --> 00:03:46,000
 breaks a trust

56
00:03:46,000 --> 00:03:50,720
 or something of that sort. So, rape because it's without

57
00:03:50,720 --> 00:03:54,000
 the permission of the other party. Right?

58
00:03:54,000 --> 00:03:58,300
 Adultery because it's without the permission or it's

59
00:03:58,300 --> 00:04:02,830
 against the wishes, let's say against the wishes of a third

60
00:04:02,830 --> 00:04:05,000
 party. Right?

61
00:04:05,000 --> 00:04:12,480
 And so I would say that in general is how we should

62
00:04:12,480 --> 00:04:21,770
 understand the third precept. I hope that answers your

63
00:04:21,770 --> 00:04:24,000
 question.

