1
00:00:00,000 --> 00:00:14,120
 Good evening everyone.

2
00:00:14,120 --> 00:00:24,320
 We're broadcasting live December 29th, 2015.

3
00:00:24,320 --> 00:00:36,400
 We have a little over two days left in the year.

4
00:00:36,400 --> 00:00:44,840
 But don't worry, there's another year to come.

5
00:00:44,840 --> 00:00:54,840
 I had an argument with my youngest brother about rebirth.

6
00:00:54,840 --> 00:00:59,080
 Stubborn.

7
00:00:59,080 --> 00:01:28,560
 He said, "Well, we'll see who's right when we die."

8
00:01:28,560 --> 00:01:31,760
 We'll see who's right.

9
00:01:31,760 --> 00:01:38,800
 It's going to be a shame for him if there are consequences

10
00:01:38,800 --> 00:01:41,080
 to his actions.

11
00:01:41,080 --> 00:01:45,200
 He can't escape them by dying.

12
00:01:45,200 --> 00:01:53,710
 He said, "Well, if I'm right, you'll have to deal with the

13
00:01:53,710 --> 00:01:56,800
 consequences if I'm right."

14
00:01:56,800 --> 00:02:01,180
 So, no, actually, if you're right, there are no

15
00:02:01,180 --> 00:02:02,800
 consequences.

16
00:02:02,800 --> 00:02:06,320
 If you're right, it'd be great.

17
00:02:06,320 --> 00:02:08,400
 It means we can do whatever we want.

18
00:02:08,400 --> 00:02:18,080
 But it's quite a dangerous belief.

19
00:02:18,080 --> 00:02:22,580
 The belief in death is a dangerous belief because of the

20
00:02:22,580 --> 00:02:24,560
 consequences of it.

21
00:02:24,560 --> 00:02:28,690
 Even in this life, the consequences are people are less

22
00:02:28,690 --> 00:02:31,200
 likely to do good deeds, more likely

23
00:02:31,200 --> 00:02:40,080
 to do evil deeds.

24
00:02:40,080 --> 00:02:45,560
 Even if it was true, it would be better for us to believe

25
00:02:45,560 --> 00:02:49,080
 that there were consequences.

26
00:02:49,080 --> 00:02:53,520
 It would be better for the world.

27
00:02:53,520 --> 00:02:57,040
 That's a funny situation.

28
00:02:57,040 --> 00:03:00,370
 If it were true that when we died, there was nothing, it

29
00:03:00,370 --> 00:03:02,160
 would still be better for all

30
00:03:02,160 --> 00:03:11,160
 of us and for the world and for society for us to believe

31
00:03:11,160 --> 00:03:16,840
 that there were consequences.

32
00:03:16,840 --> 00:03:18,320
 And so we argued.

33
00:03:18,320 --> 00:03:23,430
 And then right before I left this morning, I said, "So, we

34
00:03:23,430 --> 00:03:25,920
'll have a safe travels."

35
00:03:25,920 --> 00:03:26,920
 And I said, "Thank you."

36
00:03:26,920 --> 00:03:33,500
 And he said, "So, I'll see you if not in this life, then in

37
00:03:33,500 --> 00:03:35,080
 the next."

38
00:03:35,080 --> 00:03:47,360
 And we laughed.

39
00:03:47,360 --> 00:03:52,200
 So I had one announcement today that seems a lot of people

40
00:03:52,200 --> 00:03:55,280
 didn't know about the appointment

41
00:03:55,280 --> 00:03:56,840
 schedule that we have.

42
00:03:56,840 --> 00:04:01,910
 So if you go to meditation.siri-mongolo.org, there's a meet

43
00:04:01,910 --> 00:04:04,000
 page and it's got slots where

44
00:04:04,000 --> 00:04:07,720
 you can sign up for a meditation course.

45
00:04:07,720 --> 00:04:10,000
 And we use Google Hangouts.

46
00:04:10,000 --> 00:04:15,150
 So you have to make sure you've got Google Hangouts

47
00:04:15,150 --> 00:04:17,960
 installed and working.

48
00:04:17,960 --> 00:04:24,420
 And then if you show up at the right time, you can meet

49
00:04:24,420 --> 00:04:25,880
 with me.

50
00:04:25,880 --> 00:04:32,960
 But you must be practicing at least an hour of meditation

51
00:04:32,960 --> 00:04:34,400
 per day.

52
00:04:34,400 --> 00:04:41,040
 Let me try to get you up to two hours.

53
00:04:41,040 --> 00:04:44,560
 And you have to keep Buddhist precepts and stuff.

54
00:04:44,560 --> 00:04:46,800
 But it's for people who are serious about the meditation

55
00:04:46,800 --> 00:04:48,000
 practice and want to go the

56
00:04:48,000 --> 00:04:51,800
 next, take the next step.

57
00:04:51,800 --> 00:05:01,920
 Right now we have nine thoughts available.

58
00:05:01,920 --> 00:05:16,960
 I just got back from Florida today.

59
00:05:16,960 --> 00:05:20,200
 I'm not planning on sticking around too long this evening.

60
00:05:20,200 --> 00:05:23,720
 Make a short night of it.

61
00:05:23,720 --> 00:05:25,640
 Do you have any questions?

62
00:05:25,640 --> 00:05:26,640
 We have questions.

63
00:05:26,640 --> 00:05:30,200
 We have lots of questions, don't we?

64
00:05:30,200 --> 00:05:32,200
 Let's skip to the good ones.

65
00:05:32,200 --> 00:05:35,670
 Regarding meetings, is it required to have a camera and a

66
00:05:35,670 --> 00:05:37,800
 microphone to make an individual

67
00:05:37,800 --> 00:05:38,800
 meeting?

68
00:05:38,800 --> 00:05:42,320
 I have social anxiety, so it's very difficult for me.

69
00:05:42,320 --> 00:05:44,200
 And my English is bad too.

70
00:05:44,200 --> 00:05:45,200
 Thanks.

71
00:05:45,200 --> 00:05:47,600
 Yeah, we can do with just a...

72
00:05:47,600 --> 00:05:49,280
 You need a microphone, absolutely.

73
00:05:49,280 --> 00:05:54,880
 But I'd like to have a camera.

74
00:05:54,880 --> 00:05:59,290
 I think it's important because normally you would come into

75
00:05:59,290 --> 00:06:01,080
 the room and meet me.

76
00:06:01,080 --> 00:06:04,320
 And we're trying to make it as close to that as possible.

77
00:06:04,320 --> 00:06:14,640
 So I really would like for you to have a camera.

78
00:06:14,640 --> 00:06:18,300
 Every time I do sitting meditation, I seem to keep dropping

79
00:06:18,300 --> 00:06:19,360
 off to sleep.

80
00:06:19,360 --> 00:06:21,320
 This can be any time of day.

81
00:06:21,320 --> 00:06:23,730
 Apart from walking meditation, what can I do to help with

82
00:06:23,730 --> 00:06:24,840
 my sitting meditation?

83
00:06:24,840 --> 00:06:29,280
 Well, walking meditation is good.

84
00:06:29,280 --> 00:06:32,880
 What's wrong with that?

85
00:06:32,880 --> 00:06:34,880
 Be careful.

86
00:06:34,880 --> 00:06:36,880
 Be mindful, tired, tired.

87
00:06:36,880 --> 00:06:37,880
 It helps.

88
00:06:37,880 --> 00:06:41,160
 But it probably has a lot to do with your lifestyle.

89
00:06:41,160 --> 00:06:46,230
 You have to put up with falling asleep because of the state

90
00:06:46,230 --> 00:06:47,880
 your mind is in.

91
00:06:47,880 --> 00:06:58,840
 During sitting meditation, I find it hard to note thoughts

92
00:06:58,840 --> 00:07:00,840
 that arise and cease quickly

93
00:07:00,840 --> 00:07:02,760
 while I'm noting primary objectives.

94
00:07:02,760 --> 00:07:06,600
 However, I am aware that this happens and it is interrupt

95
00:07:06,600 --> 00:07:08,360
ing my concentration.

96
00:07:08,360 --> 00:07:11,310
 How it happens is that while I'm waiting for the rising, a

97
00:07:11,310 --> 00:07:13,160
 thought will manifest and cease,

98
00:07:13,160 --> 00:07:15,480
 and I will go on noting the rising.

99
00:07:15,480 --> 00:07:18,740
 My question is, should I in this case be noting thinking,

100
00:07:18,740 --> 00:07:20,960
 thinking, thinking after the fact,

101
00:07:20,960 --> 00:07:22,960
 or should I note distracted?

102
00:07:22,960 --> 00:07:26,330
 If so, how many times should I note before returning to my

103
00:07:26,330 --> 00:07:27,320
 objectives?

104
00:07:27,320 --> 00:07:28,320
 You can.

105
00:07:28,320 --> 00:07:35,930
 Just once, or you can acknowledge knowing a couple of times

106
00:07:35,930 --> 00:07:36,440
.

107
00:07:36,440 --> 00:07:38,040
 It's not magic or something.

108
00:07:38,040 --> 00:07:42,960
 Just do it as it seems appropriate.

109
00:07:42,960 --> 00:07:46,720
 Knowing would be knowing that you were thinking.

110
00:07:46,720 --> 00:07:51,510
 Is there a certain time of day in which meditation is best

111
00:07:51,510 --> 00:07:54,600
 to be practiced in, in the morning,

112
00:07:54,600 --> 00:07:56,600
 for instance?

113
00:07:56,600 --> 00:07:57,600
 No.

114
00:07:57,600 --> 00:08:05,410
 How would you convince a non-Buddhist that reincarnation is

115
00:08:05,410 --> 00:08:06,560
 real?

116
00:08:06,560 --> 00:08:11,040
 Is there a way one can know who one was in a past life?

117
00:08:11,040 --> 00:08:13,520
 And thank you so much for taking the time to answer.

118
00:08:13,520 --> 00:08:16,600
 Yeah, I'm going to skip that one.

119
00:08:16,600 --> 00:08:17,600
 Okay.

120
00:08:17,600 --> 00:08:19,000
 Sounds like you've already had that.

121
00:08:19,000 --> 00:08:21,720
 I just wanted to say about consequences.

122
00:08:21,720 --> 00:08:23,600
 You don't even have to wait until the next life for the

123
00:08:23,600 --> 00:08:24,320
 consequences.

124
00:08:24,320 --> 00:08:27,470
 I mean, you feel them right now when you try to do anything

125
00:08:27,470 --> 00:08:27,760
.

126
00:08:27,760 --> 00:08:30,470
 Yeah, I was arguing that with him and he said, "I get that

127
00:08:30,470 --> 00:08:30,760
."

128
00:08:30,760 --> 00:08:33,920
 He said, my younger brother said he knows that, but, and he

129
00:08:33,920 --> 00:08:35,440
 said, "But," and then I

130
00:08:35,440 --> 00:08:40,200
 said, "But do you think that there are no cons to the cons

131
00:08:40,200 --> 00:08:40,680
?"

132
00:08:40,680 --> 00:08:55,770
 In the end, when you die, you get up scot-free and he said,

133
00:08:55,770 --> 00:08:58,560
 "Yeah."

134
00:08:58,560 --> 00:09:01,980
 When I'm sitting, I'm not sitting, sitting, but how far do

135
00:09:01,980 --> 00:09:02,880
 I need to go?

136
00:09:02,880 --> 00:09:06,600
 Do I need to be mindful of the pressure below my butt or

137
00:09:06,600 --> 00:09:08,800
 just know that I'm sitting?

138
00:09:08,800 --> 00:09:10,680
 Is sitting a concept?

139
00:09:10,680 --> 00:09:14,190
 When my abdomen is rising, I note rising, rising, but do I

140
00:09:14,190 --> 00:09:15,680
 need to follow the rising

141
00:09:15,680 --> 00:09:17,920
 of the abdomen at every position?

142
00:09:17,920 --> 00:09:18,920
 Thanks.

143
00:09:18,920 --> 00:09:22,760
 Yeah, you should follow the rising from beginning to end.

144
00:09:22,760 --> 00:09:24,160
 You're not just saying it.

145
00:09:24,160 --> 00:09:27,320
 It's not just words in your head.

146
00:09:27,320 --> 00:09:33,800
 Sitting as well, sitting is an expression of the sensations

147
00:09:33,800 --> 00:09:34,240
.

148
00:09:34,240 --> 00:09:36,640
 So yeah, you should be aware, but you don't have to focus

149
00:09:36,640 --> 00:09:37,080
 on it.

150
00:09:37,080 --> 00:09:40,600
 The only reason you know that you're sitting is because of

151
00:09:40,600 --> 00:09:42,360
 the sensations in your back

152
00:09:42,360 --> 00:09:55,080
 and in your bottom.

153
00:09:55,080 --> 00:09:57,500
 Is there a difference between mindfulness achieved during

154
00:09:57,500 --> 00:09:58,720
 meditation and mindfulness

155
00:09:58,720 --> 00:10:00,920
 during everyday activities?

156
00:10:00,920 --> 00:10:04,250
 Is mindfulness achieved in formal meditation practice more

157
00:10:04,250 --> 00:10:05,160
 beneficial?

158
00:10:05,160 --> 00:10:09,160
 Thank you, Bante.

159
00:10:09,160 --> 00:10:12,160
 Mindfulness is mindfulness.

160
00:10:12,160 --> 00:10:15,280
 Hello, Bante.

161
00:10:15,280 --> 00:10:19,200
 I'd like to start off by saying thank you for all your

162
00:10:19,200 --> 00:10:21,480
 teachings and dedication.

163
00:10:21,480 --> 00:10:24,640
 They truly have helped me put things in better perspective.

164
00:10:24,640 --> 00:10:27,520
 I'll be taking a couple of college classes in a city called

165
00:10:27,520 --> 00:10:29,240
 Sioux Falls, South Dakota,

166
00:10:29,240 --> 00:10:32,490
 starting February 2016, and thinking of forming a small

167
00:10:32,490 --> 00:10:34,640
 meditation group on campus with interested

168
00:10:34,640 --> 00:10:35,640
 individuals.

169
00:10:35,640 --> 00:10:39,010
 If you'll be willing to meet with us for about an hour or

170
00:10:39,010 --> 00:10:41,120
 less once a week via Skype or another

171
00:10:41,120 --> 00:10:47,560
 form of video communication, I think that was a question.

172
00:10:47,560 --> 00:10:49,620
 I'm not certain of the number of people who will be

173
00:10:49,620 --> 00:10:51,120
 interested in such a group, but if

174
00:10:51,120 --> 00:10:54,990
 any, it would be wonderful to have you guide us along this

175
00:10:54,990 --> 00:10:57,440
 path to freedom from suffering.

176
00:10:57,440 --> 00:11:04,840
 So I think the question was, would you be potentially able

177
00:11:04,840 --> 00:11:07,480
 to do such a thing?

178
00:11:07,480 --> 00:11:12,440
 I don't know.

179
00:11:12,440 --> 00:11:13,440
 Probably not.

180
00:11:13,440 --> 00:11:19,440
 I'm doing a lot already.

181
00:11:19,440 --> 00:11:24,900
 You just go according to the booklet and people want to

182
00:11:24,900 --> 00:11:26,440
 learn more.

183
00:11:26,440 --> 00:11:29,200
 They can contact me.

184
00:11:29,200 --> 00:11:31,200
 They can do a course.

185
00:11:31,200 --> 00:11:35,080
 They can join out and look at yourself.

186
00:11:35,080 --> 00:11:36,080
 It's great that you're doing it.

187
00:11:36,080 --> 00:11:41,630
 I want to encourage you in it, but I don't think I'm

188
00:11:41,630 --> 00:11:44,440
 already doing too much.

189
00:11:44,440 --> 00:11:50,440
 Hi, Bonthe.

190
00:11:50,440 --> 00:11:51,440
 I have a job interview in one week.

191
00:11:51,440 --> 00:11:52,990
 I start a lot when I need to talk and have difficulty to

192
00:11:52,990 --> 00:11:53,440
 breathe.

193
00:11:53,440 --> 00:11:56,440
 Do you have any advice besides noting?

194
00:11:56,440 --> 00:11:59,800
 And is it a good thing to imagine the interview beforehand

195
00:11:59,800 --> 00:12:01,440
 and observe my reaction?

196
00:12:01,440 --> 00:12:02,440
 Thanks.

197
00:12:02,440 --> 00:12:07,350
 No, I mean, I teach meditation, so you know what I've got

198
00:12:07,350 --> 00:12:08,440
 to offer.

199
00:12:08,440 --> 00:12:15,440
 If it helps, take it.

200
00:12:15,440 --> 00:12:23,440
 If it doesn't help, leave it.

201
00:12:23,440 --> 00:12:24,440
 I guess this is a question.

202
00:12:24,440 --> 00:12:26,440
 Do you know that you're just the best?

203
00:12:26,440 --> 00:12:34,440
 I think that was a compliment, but I had a question mark.

204
00:12:34,440 --> 00:12:37,440
 We're all worthless.

205
00:12:37,440 --> 00:12:39,440
 Is it better to meditate in a group rather than alone?

206
00:12:39,440 --> 00:12:41,440
 If so, why?

207
00:12:41,440 --> 00:12:43,440
 Thank you, Bonthe.

208
00:12:43,440 --> 00:12:46,440
 You're always alone.

209
00:12:46,440 --> 00:12:49,440
 You're never in a group.

210
00:12:49,440 --> 00:12:54,440
 But much of meditation is an artifice.

211
00:12:54,440 --> 00:12:56,440
 It's artificial.

212
00:12:56,440 --> 00:13:01,440
 So there's lots of artificial supports for meditation,

213
00:13:01,440 --> 00:13:06,440
 and one of them is having a teacher, being in a group.

214
00:13:06,440 --> 00:13:09,440
 These are artificial supports.

215
00:13:09,440 --> 00:13:12,440
 So yeah, I think they can support your meditation,

216
00:13:12,440 --> 00:13:15,440
 provide encouragement and so on.

217
00:13:15,440 --> 00:13:24,440
 Lots of things that can support your meditation.

218
00:13:24,440 --> 00:13:27,440
 I'm sitting every day and making progress.

219
00:13:27,440 --> 00:13:29,440
 Is there any benefit in doing your one-on-one course,

220
00:13:29,440 --> 00:13:32,440
 or is it just for those who need advice?

221
00:13:32,440 --> 00:13:33,440
 Thank you.

222
00:13:33,440 --> 00:13:38,440
 Well, if you're just practicing according to my booklet,

223
00:13:38,440 --> 00:13:40,440
 that's only the first step.

224
00:13:40,440 --> 00:13:43,440
 And so yeah, we take you through the steps

225
00:13:43,440 --> 00:13:47,440
 and we guide you through the course.

226
00:13:47,440 --> 00:13:54,670
 So I would say there's a lot of benefit in taking an online

227
00:13:54,670 --> 00:13:59,440
 course.

228
00:13:59,440 --> 00:14:05,440
 What's in the booklet is just the very beginning.

229
00:14:05,440 --> 00:14:07,440
 And with that, we've made it through all the questions.

230
00:14:07,440 --> 00:14:18,440
 Good.

231
00:14:18,440 --> 00:14:27,440
 So we have tomorrow we might do Dhammapada, probably.

232
00:14:27,440 --> 00:14:31,440
 Should be okay.

233
00:14:31,440 --> 00:14:36,440
 Then on Thursday, we have the last day of the year.

234
00:14:36,440 --> 00:14:37,440
 I'll be here.

235
00:14:37,440 --> 00:14:44,440
 We'll see who shows up.

236
00:14:44,440 --> 00:14:47,440
 Thanks for your muffins, Vante.

237
00:14:47,440 --> 00:14:48,440
 What?

238
00:14:48,440 --> 00:14:50,440
 Thanks for your muffins, Vante.

239
00:14:50,440 --> 00:14:51,440
 Are you giving out muffins?

240
00:14:51,440 --> 00:14:52,440
 No.

241
00:14:52,440 --> 00:14:57,440
 Okay.

242
00:14:57,440 --> 00:14:58,440
 Muffins.

243
00:14:58,440 --> 00:15:05,440
 Someone thank you for your muffins.

244
00:15:05,440 --> 00:15:06,440
 I don't get it.

245
00:15:06,440 --> 00:15:07,440
 You don't get it.

246
00:15:07,440 --> 00:15:10,440
 I think it's a joke.

247
00:15:10,440 --> 00:15:11,440
 Maybe.

248
00:15:11,440 --> 00:15:18,440
 It's in my head.

249
00:15:18,440 --> 00:15:21,440
 Anyway, good night, everyone.

250
00:15:21,440 --> 00:15:22,440
 Good night, Vante.

251
00:15:22,440 --> 00:15:24,440
 See you all tomorrow.

252
00:15:24,440 --> 00:15:25,440
 Thank you.

253
00:15:25,440 --> 00:15:26,440
 Thank you, Robin.

254
00:15:27,440 --> 00:15:28,440
 Thank you, Robin.

255
00:15:29,440 --> 00:15:30,440
 Thank you.

256
00:15:32,440 --> 00:15:33,440
 Thank you.

