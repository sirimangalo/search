1
00:00:00,000 --> 00:00:13,440
 Okay, good evening everyone.

2
00:00:13,440 --> 00:00:17,560
 Welcome to our live broadcast.

3
00:00:17,560 --> 00:00:26,640
 I'm told that the sound is too quiet, so hopefully that's

4
00:00:26,640 --> 00:00:27,960
 improved.

5
00:00:27,960 --> 00:00:49,960
 I think it should be a little bit louder now.

6
00:00:49,960 --> 00:00:57,330
 I think it's fair to say it's a universal characteristic of

7
00:00:57,330 --> 00:00:58,960
 beings that we seek out

8
00:00:58,960 --> 00:01:08,600
 what is valuable.

9
00:01:08,600 --> 00:01:20,690
 What we work for, what we strive for, what we plan for,

10
00:01:20,690 --> 00:01:27,320
 what we seek out in all aspects

11
00:01:27,320 --> 00:01:33,720
 of our life is what is valuable.

12
00:01:33,720 --> 00:01:37,540
 We come to meditate because we think that there's something

13
00:01:37,540 --> 00:01:39,240
 valuable to be gained from

14
00:01:39,240 --> 00:01:45,680
 meditation.

15
00:01:45,680 --> 00:01:55,660
 We have a job, employment, we work in the world, and what

16
00:01:55,660 --> 00:01:59,320
 is valuable.

17
00:01:59,320 --> 00:02:09,350
 And we seek out many different kinds of things that are

18
00:02:09,350 --> 00:02:11,880
 valuable.

19
00:02:11,880 --> 00:02:14,640
 Different people see different things as valuable.

20
00:02:14,640 --> 00:02:21,870
 Of course, it goes without saying that some things are not

21
00:02:21,870 --> 00:02:23,600
 valuable.

22
00:02:23,600 --> 00:02:29,720
 Some people seek out things that are not valuable, thinking

23
00:02:29,720 --> 00:02:31,920
 they're valuable.

24
00:02:31,920 --> 00:02:36,510
 The things that we generally think of as valuable, we

25
00:02:36,510 --> 00:02:40,240
 recognize in Buddhism, in the world, the

26
00:02:40,240 --> 00:02:52,080
 things that we think of as valuable gain, status, praise,

27
00:02:52,080 --> 00:02:53,080
 pleasure.

28
00:02:53,080 --> 00:03:00,760
 These are the things we see as valuable in the world.

29
00:03:00,760 --> 00:03:13,760
 We seek out, we work hard, we go to school, we get a job

30
00:03:13,760 --> 00:03:15,000
 for our gain.

31
00:03:15,000 --> 00:03:21,480
 We wish to gain money, possessions.

32
00:03:21,480 --> 00:03:26,230
 This we get in the world, this we seek out, and this we

33
00:03:26,230 --> 00:03:31,920
 strive for, and this we obtain,

34
00:03:31,920 --> 00:03:40,080
 thinking or feeling or understanding that this is valuable.

35
00:03:40,080 --> 00:03:57,440
 We seek out status, wanting to be well known or esteemed,

36
00:03:57,440 --> 00:04:00,240
 looking for a promotion, looking

37
00:04:00,240 --> 00:04:12,290
 for some sort of rank or status in society to not be a

38
00:04:12,290 --> 00:04:13,520
 nobody.

39
00:04:13,520 --> 00:04:15,080
 This is valuable.

40
00:04:15,080 --> 00:04:31,820
 It's valuable to be powerful, well known, high ranked in

41
00:04:31,820 --> 00:04:35,320
 society.

42
00:04:35,320 --> 00:04:51,350
 We seek out praise, we'll do anything we can to get people

43
00:04:51,350 --> 00:04:56,760
's good opinion.

44
00:04:56,760 --> 00:05:01,860
 We work and we work and then we wait and we watch.

45
00:05:01,860 --> 00:05:04,080
 Do people like me?

46
00:05:04,080 --> 00:05:07,040
 What are people saying about me?

47
00:05:07,040 --> 00:05:12,480
 We think it's valuable to have people's good opinion.

48
00:05:12,480 --> 00:05:20,880
 It's valuable to be praised, to have people like you, to

49
00:05:20,880 --> 00:05:24,800
 have people esteem you.

50
00:05:24,800 --> 00:05:26,680
 People feel you to be valuable.

51
00:05:26,680 --> 00:05:32,160
 We all want to be valuable ourselves.

52
00:05:32,160 --> 00:05:35,760
 And of course happiness, pleasure.

53
00:05:35,760 --> 00:05:40,440
 Much of what we do is simply to seek out pleasure.

54
00:05:40,440 --> 00:05:48,430
 We have so many entertainment sources designed just for

55
00:05:48,430 --> 00:05:50,840
 this purpose.

56
00:05:50,840 --> 00:05:56,440
 We have pleasurable sights and sounds and smells and tastes

57
00:05:56,440 --> 00:05:59,000
 and feelings and thoughts.

58
00:05:59,000 --> 00:06:03,970
 We seek out, we strive for, we work towards, we work to

59
00:06:03,970 --> 00:06:06,400
 maintain and cultivate.

60
00:06:06,400 --> 00:06:13,120
 We cling to, we hold on to as valuable.

61
00:06:13,120 --> 00:06:14,640
 What would life be right?

62
00:06:14,640 --> 00:06:18,720
 Without the pleasures, without pleasure what would life be?

63
00:06:18,720 --> 00:06:24,760
 It would be meaningless, worthless.

64
00:06:24,760 --> 00:06:32,320
 That's just how we feel about pleasure.

65
00:06:32,320 --> 00:06:36,650
 So putting aside the question of whether these things are

66
00:06:36,650 --> 00:06:38,440
 actually valuable.

67
00:06:38,440 --> 00:06:41,960
 Even if we accept for the moment that these are valuable.

68
00:06:41,960 --> 00:06:45,190
 It's hard to say really because what would it mean to say

69
00:06:45,190 --> 00:06:46,840
 that they are valuable?

70
00:06:46,840 --> 00:06:50,240
 Most people would say well I value them and therefore they

71
00:06:50,240 --> 00:06:51,320
're valuable.

72
00:06:51,320 --> 00:06:53,120
 Suppose that's a good enough reason.

73
00:06:53,120 --> 00:06:58,070
 If you see something as valuable it's hard really to,

74
00:06:58,070 --> 00:07:01,040
 without going deeper into Buddhism

75
00:07:01,040 --> 00:07:05,000
 which we will, but on the surface it's hard to argue.

76
00:07:05,000 --> 00:07:09,520
 If you say it's valuable it's valuable.

77
00:07:09,520 --> 00:07:14,760
 The more it applies it doesn't mean anything to say it's

78
00:07:14,760 --> 00:07:16,080
 valuable.

79
00:07:16,080 --> 00:07:24,170
 Then we have to ask what is it valuable for? What is it

80
00:07:24,170 --> 00:07:24,240
 good for?

81
00:07:24,240 --> 00:07:29,520
 This is where we start to run into a problem.

82
00:07:29,520 --> 00:07:42,280
 Because as much as you might say that gain is valuable.

83
00:07:42,280 --> 00:07:46,760
 Gain is not something that you can control.

84
00:07:46,760 --> 00:07:48,760
 Gain is not something that you're in charge of.

85
00:07:48,760 --> 00:07:53,520
 You can work really hard and maybe you'll become rich.

86
00:07:53,520 --> 00:07:55,440
 Maybe you'll stay poor.

87
00:07:55,440 --> 00:08:07,440
 The point is that gain is paired with loss.

88
00:08:07,440 --> 00:08:12,340
 As much in our life, many times in our life, or much of our

89
00:08:12,340 --> 00:08:14,280
 life is filled with gain.

90
00:08:14,280 --> 00:08:17,750
 Getting what we want, getting what is valuable, getting

91
00:08:17,750 --> 00:08:19,880
 what we esteem.

92
00:08:19,880 --> 00:08:29,640
 Money, possessions, friends.

93
00:08:29,640 --> 00:08:38,350
 But likewise much of our life is filled with loss and as

94
00:08:38,350 --> 00:08:42,600
 much as we cultivate gain.

95
00:08:42,600 --> 00:08:47,780
 We're always going to be disappointed and displeased by

96
00:08:47,780 --> 00:08:48,600
 loss.

97
00:08:48,600 --> 00:08:51,720
 Truth is they go together and if you're attached to gain,

98
00:08:51,720 --> 00:08:55,600
 if you're attached to your possessions

99
00:08:55,600 --> 00:08:56,600
 there's a problem there.

100
00:08:56,600 --> 00:09:01,020
 It makes you susceptible to loss, that in fact a steaming

101
00:09:01,020 --> 00:09:02,600
 gain is valuable.

102
00:09:02,600 --> 00:09:05,670
 Holding on to it and appreciating it and liking it and

103
00:09:05,670 --> 00:09:09,040
 clinging to it is really just a recipe

104
00:09:09,040 --> 00:09:10,040
 for suffering.

105
00:09:10,040 --> 00:09:13,240
 You're always going to be dissatisfied.

106
00:09:13,240 --> 00:09:14,240
 You're always going to be vulnerable.

107
00:09:14,240 --> 00:09:19,550
 You're always going to be crushed when you lose the things

108
00:09:19,550 --> 00:09:22,200
 that are most dear to you.

109
00:09:22,200 --> 00:09:29,220
 I might argue that it's valuable to have money, for example

110
00:09:29,220 --> 00:09:32,600
, so you can buy the things you

111
00:09:32,600 --> 00:09:33,600
 need.

112
00:09:33,600 --> 00:09:41,000
 I wouldn't say that really makes it valuable.

113
00:09:41,000 --> 00:09:49,880
 It makes it useful in a practical sense.

114
00:09:49,880 --> 00:09:51,560
 Stiffing from something being valuable.

115
00:09:51,560 --> 00:09:54,480
 Money isn't valuable.

116
00:09:54,480 --> 00:10:00,040
 Money is something that's like a debt really.

117
00:10:00,040 --> 00:10:05,120
 Without it you have to cough it up just in order to survive

118
00:10:05,120 --> 00:10:05,160
.

119
00:10:05,160 --> 00:10:09,190
 And after the gaining money you're just working so that

120
00:10:09,190 --> 00:10:11,240
 people don't starve you.

121
00:10:11,240 --> 00:10:20,890
 Working just so that you can feed your body and protect

122
00:10:20,890 --> 00:10:23,280
 your life.

123
00:10:23,280 --> 00:10:28,600
 We're actually just slaves in that sense.

124
00:10:28,600 --> 00:10:29,600
 Being forced to work.

125
00:10:29,600 --> 00:10:30,600
 We couldn't stop working.

126
00:10:30,600 --> 00:10:31,600
 It's not really valuable.

127
00:10:31,600 --> 00:10:34,320
 It's a trap that we're stuck in.

128
00:10:34,320 --> 00:10:48,090
 Status, I think, is less common and less of a big deal in

129
00:10:48,090 --> 00:10:54,080
 this society anyway.

130
00:10:54,080 --> 00:10:55,680
 Maybe that's not true.

131
00:10:55,680 --> 00:10:58,000
 I mean, of course, for many people it is important.

132
00:10:58,000 --> 00:11:04,790
 But we don't have a ranked society, a caste system like in

133
00:11:04,790 --> 00:11:05,600
 India.

134
00:11:05,600 --> 00:11:10,190
 Still, there's always a sense of being somebody, you know,

135
00:11:10,190 --> 00:11:12,400
 making a mark in the world.

136
00:11:12,400 --> 00:11:16,360
 You don't just want to be some nobody-flipping-burgers.

137
00:11:16,360 --> 00:11:20,230
 You want to be a manager or a boss or someone that your

138
00:11:20,230 --> 00:11:22,400
 parents will be proud of.

139
00:11:22,400 --> 00:11:25,820
 Someone who has some kind of power, some kind of influence,

140
00:11:25,820 --> 00:11:27,600
 some kind of worth, value.

141
00:11:27,600 --> 00:11:36,960
 And then, of course, status is a fickle thing.

142
00:11:36,960 --> 00:11:41,600
 You can be esteemed one day and vilified the next or

143
00:11:41,600 --> 00:11:43,200
 unknown the next.

144
00:11:43,200 --> 00:11:46,880
 You can be a high class, low class.

145
00:11:46,880 --> 00:11:50,600
 There's always going to be the threat.

146
00:11:50,600 --> 00:11:53,010
 When you get a raise or a promotion, there's always going

147
00:11:53,010 --> 00:11:55,120
 to be the threat of someone else

148
00:11:55,120 --> 00:12:04,000
 wanting to take your position, the threat of losing it by

149
00:12:04,000 --> 00:12:08,720
 not performing up to snuff.

150
00:12:08,720 --> 00:12:11,680
 And praise comes with blame.

151
00:12:11,680 --> 00:12:13,800
 We love to be praised.

152
00:12:13,800 --> 00:12:19,160
 Well, in place of praise, there will always be blame.

153
00:12:19,160 --> 00:12:21,160
 They're paired with each other.

154
00:12:21,160 --> 00:12:27,720
 There is no one in this world who is not blamed at some

155
00:12:27,720 --> 00:12:29,040
 point.

156
00:12:29,040 --> 00:12:31,000
 They don't talk bad about it.

157
00:12:31,000 --> 00:12:32,000
 Right?

158
00:12:32,000 --> 00:12:36,700
 You're never going to have everyone speak well about you

159
00:12:36,700 --> 00:12:39,080
 and it's devastating if you

160
00:12:39,080 --> 00:12:43,210
 have a strong sense of self-esteem or if you place

161
00:12:43,210 --> 00:12:46,320
 importance on what people think of you.

162
00:12:46,320 --> 00:12:49,930
 I mean, this is really the silly thing, is our concern for

163
00:12:49,930 --> 00:12:51,600
 what other people think of

164
00:12:51,600 --> 00:12:53,800
 you.

165
00:12:53,800 --> 00:12:54,800
 Here's the secret.

166
00:12:54,800 --> 00:12:57,570
 In an ultimate sense, it means nothing what other people

167
00:12:57,570 --> 00:12:58,440
 think of you.

168
00:12:58,440 --> 00:13:00,440
 Really doesn't.

169
00:13:00,440 --> 00:13:03,980
 Again, you might say practically speaking, of course it

170
00:13:03,980 --> 00:13:05,440
 makes a difference.

171
00:13:05,440 --> 00:13:10,890
 If everyone hates you well, practically or conventionally,

172
00:13:10,890 --> 00:13:12,720
 that's a problem.

173
00:13:12,720 --> 00:13:14,880
 But ultimately, it's not.

174
00:13:14,880 --> 00:13:18,560
 Ultimately, that's not the issue.

175
00:13:18,560 --> 00:13:22,000
 That's not what leads to suffering.

176
00:13:22,000 --> 00:13:26,000
 To suffering is your own reaction, your experience, your

177
00:13:26,000 --> 00:13:27,320
 own estimation.

178
00:13:27,320 --> 00:13:31,840
 If everyone hates you but you're at peace with yourself,

179
00:13:31,840 --> 00:13:33,920
 then you're far happier and

180
00:13:33,920 --> 00:13:39,970
 far better off than someone that everyone loves and is

181
00:13:39,970 --> 00:13:44,480
 always afraid of censure, vulnerable

182
00:13:44,480 --> 00:13:45,480
 to censure.

183
00:13:45,480 --> 00:13:52,540
 When you get a hundred upvotes on your video and then you

184
00:13:52,540 --> 00:13:56,480
 fret about the one downvote,

185
00:13:56,480 --> 00:13:59,830
 when everyone says nice things about you, it says the

186
00:13:59,830 --> 00:14:01,200
 quality of the internet.

187
00:14:01,200 --> 00:14:04,750
 People say nice things and then one person says something

188
00:14:04,750 --> 00:14:06,600
 nasty and it just devastates

189
00:14:06,600 --> 00:14:07,600
 you.

190
00:14:07,600 --> 00:14:08,600
 We're vulnerable.

191
00:14:08,600 --> 00:14:16,550
 This is what placing value in these things that are frail,

192
00:14:16,550 --> 00:14:24,320
 fragile, are uncertain, unstable,

193
00:14:24,320 --> 00:14:28,200
 unpredictable.

194
00:14:28,200 --> 00:14:31,000
 And happiness, of course, pleasure.

195
00:14:31,000 --> 00:14:34,180
 Even if pleasure is to be understood as valuable, it's hard

196
00:14:34,180 --> 00:14:35,800
 to say one way or the other.

197
00:14:35,800 --> 00:14:37,480
 What does it mean to say it's valuable?

198
00:14:37,480 --> 00:14:38,480
 We like it.

199
00:14:38,480 --> 00:14:43,240
 We'd have to say, "Does it improve your quality of life?"

200
00:14:43,240 --> 00:14:46,560
 Or we might even say, "Does pleasure make you happy?"

201
00:14:46,560 --> 00:14:52,840
 We'd have to say, "Of course it makes me happy."

202
00:14:52,840 --> 00:14:56,270
 Unfortunately this is what we see in meditation and in fact

203
00:14:56,270 --> 00:14:57,120
 it doesn't.

204
00:14:57,120 --> 00:15:00,570
 As you watch, all it does is make you cling to it, make you

205
00:15:00,570 --> 00:15:02,200
 desire it more and stress

206
00:15:02,200 --> 00:15:08,840
 and feel disappointed when you don't get it frustrated.

207
00:15:08,840 --> 00:15:12,670
 It's very easy to live and to be content when you always

208
00:15:12,670 --> 00:15:14,200
 get what you want.

209
00:15:14,200 --> 00:15:20,640
 As long as the good things come to us, you can never really

210
00:15:20,640 --> 00:15:23,560
 measure the level of your

211
00:15:23,560 --> 00:15:27,120
 happiness, of your mental development.

212
00:15:27,120 --> 00:15:29,240
 Why meditation is so uncomfortable?

213
00:15:29,240 --> 00:15:32,680
 It's because it takes this away from you.

214
00:15:32,680 --> 00:15:37,660
 We live our lives thinking, "I'm a pretty content

215
00:15:37,660 --> 00:15:39,280
 individual.

216
00:15:39,280 --> 00:15:42,520
 Nothing's perfect, not everything's perfect."

217
00:15:42,520 --> 00:15:47,120
 But mostly that's just because we're generally getting what

218
00:15:47,120 --> 00:15:48,000
 we want.

219
00:15:48,000 --> 00:15:53,640
 We generally have enough gain to get by.

220
00:15:53,640 --> 00:15:55,360
 People aren't saying nasty things about us.

221
00:15:55,360 --> 00:15:59,320
 We have a position in society and we can find pleasure.

222
00:15:59,320 --> 00:16:04,040
 We have entertainment, ways of enjoying ourselves.

223
00:16:04,040 --> 00:16:07,240
 So we think, "I'm a pretty content individual."

224
00:16:07,240 --> 00:16:10,460
 Until you come to meditate and you don't have any of that

225
00:16:10,460 --> 00:16:14,080
 and it's stress to the max, suddenly

226
00:16:14,080 --> 00:16:21,600
 you have to deal with your mind and only your mind without

227
00:16:21,600 --> 00:16:23,080
 all these things that you hold

228
00:16:23,080 --> 00:16:44,080
 valuable and realize that all that's done is leave you

229
00:16:44,080 --> 00:16:50,200
 discontent.

230
00:16:50,200 --> 00:17:00,080
 So the Buddha taught a series of things that are truly

231
00:17:00,080 --> 00:17:02,600
 valuable.

232
00:17:02,600 --> 00:17:07,520
 Monks are these seven treasures, seven valuable things.

233
00:17:07,520 --> 00:17:19,040
 These are called the seven noble treasures.

234
00:17:19,040 --> 00:17:23,800
 So if we compare all these worldly things with the Dhamma,

235
00:17:23,800 --> 00:17:25,960
 we can start to get a sense

236
00:17:25,960 --> 00:17:29,640
 of what it means for something to be truly valuable.

237
00:17:29,640 --> 00:17:35,340
 We can get a sense of how limited the value is of worldly

238
00:17:35,340 --> 00:17:36,560
 things.

239
00:17:36,560 --> 00:17:43,740
 The first one is saddha, which means confidence or

240
00:17:43,740 --> 00:17:46,000
 conviction.

241
00:17:46,000 --> 00:17:50,040
 Because these are things that can't be taken away from you.

242
00:17:50,040 --> 00:17:53,480
 Someone can't take your conviction away from you.

243
00:17:53,480 --> 00:17:58,040
 If they can, then it's a problem with your conviction.

244
00:17:58,040 --> 00:18:04,440
 It means you don't have enough of it.

245
00:18:04,440 --> 00:18:05,800
 Money is not like that.

246
00:18:05,800 --> 00:18:07,800
 Wealth is not like that.

247
00:18:07,800 --> 00:18:08,800
 Pleasure is not like that.

248
00:18:08,800 --> 00:18:11,600
 It's not the more you get, the harder it is to take away.

249
00:18:11,600 --> 00:18:18,760
 Conviction, the more you have, the harder it is to take

250
00:18:18,760 --> 00:18:19,920
 away.

251
00:18:19,920 --> 00:18:25,780
 The more you develop conviction through seeing the truth,

252
00:18:25,780 --> 00:18:28,320
 through practicing the Buddha's

253
00:18:28,320 --> 00:18:33,400
 teaching and coming to understand the way things are.

254
00:18:33,400 --> 00:18:37,580
 Once you practice meditation and you see form is imper

255
00:18:37,580 --> 00:18:40,080
manent, feeling is impermanent and

256
00:18:40,080 --> 00:18:45,640
 so on, according to the discourses we talked about the past

257
00:18:45,640 --> 00:18:47,280
 few sessions.

258
00:18:47,280 --> 00:18:57,000
 The more you see that, the more conviction you gain.

259
00:18:57,000 --> 00:18:58,000
 You don't waver.

260
00:18:58,000 --> 00:19:00,600
 You don't have any doubt about, "Is this valuable?

261
00:19:00,600 --> 00:19:01,600
 Is that valuable?

262
00:19:01,600 --> 00:19:02,600
 Is this right?

263
00:19:02,600 --> 00:19:03,600
 Is that right?"

264
00:19:03,600 --> 00:19:07,680
 A real treasure and conviction that most people don't have,

265
00:19:07,680 --> 00:19:09,800
 because they don't have a sense

266
00:19:09,800 --> 00:19:10,800
 of direction.

267
00:19:10,800 --> 00:19:14,470
 We might have beliefs, but they're based on shaky

268
00:19:14,470 --> 00:19:17,400
 foundations and as a result, not very

269
00:19:17,400 --> 00:19:20,280
 stable.

270
00:19:20,280 --> 00:19:24,680
 The conviction of most people is not very strong.

271
00:19:24,680 --> 00:19:28,480
 They're lacking in this treasure.

272
00:19:28,480 --> 00:19:30,820
 Even very religious people have to work really hard to

273
00:19:30,820 --> 00:19:32,480
 maintain their conviction because

274
00:19:32,480 --> 00:19:36,800
 it's based on shaky principles.

275
00:19:36,800 --> 00:19:51,120
 It's based on principles or beliefs that are unverifiable.

276
00:19:51,120 --> 00:19:52,120
 So that's the first treasure.

277
00:19:52,120 --> 00:19:57,480
 The second treasure is Sila, ethics.

278
00:19:57,480 --> 00:20:02,440
 People can't take your ethics away from you.

279
00:20:02,440 --> 00:20:13,160
 The more ethically set you are, the harder it is to take it

280
00:20:13,160 --> 00:20:14,800
 away.

281
00:20:14,800 --> 00:20:17,390
 This is another treasure that is very hard to take away

282
00:20:17,390 --> 00:20:18,760
 from someone who has it.

283
00:20:18,760 --> 00:20:21,840
 It's easy for those who don't really have it.

284
00:20:21,840 --> 00:20:24,830
 Even many Buddhists, if they say, "Yes, I keep the five

285
00:20:24,830 --> 00:20:28,400
 precepts," but if they're being

286
00:20:28,400 --> 00:20:32,760
 attacked by a vicious beast like a mosquito, they'll be

287
00:20:32,760 --> 00:20:35,320
 quick to change their tune in order

288
00:20:35,320 --> 00:20:39,320
 to save their blood.

289
00:20:39,320 --> 00:20:46,800
 A person who truly has ethics is unshakable.

290
00:20:46,800 --> 00:20:49,560
 They have no problem dying.

291
00:20:49,560 --> 00:20:55,660
 Ethics on a noble level is not something you can bargain

292
00:20:55,660 --> 00:20:56,640
 with.

293
00:20:56,640 --> 00:20:58,240
 It's not something that you wouldn't bargain with.

294
00:20:58,240 --> 00:21:00,360
 It goes beyond that.

295
00:21:00,360 --> 00:21:06,560
 The power of it is that there's not a question of breaking

296
00:21:06,560 --> 00:21:09,160
 it to save your life.

297
00:21:09,160 --> 00:21:11,320
 Your life is less valuable than ethics.

298
00:21:11,320 --> 00:21:18,390
 It's a very powerful thing because, of course, our life is

299
00:21:18,390 --> 00:21:21,880
 not as valuable as ethics.

300
00:21:21,880 --> 00:21:25,400
 You might say it's valuable, but it's only valuable in

301
00:21:25,400 --> 00:21:27,400
 terms of how ethical we are.

302
00:21:27,400 --> 00:21:33,520
 If your life is full of corruption and evil, your life is

303
00:21:33,520 --> 00:21:37,120
 actually whatever the opposite

304
00:21:37,120 --> 00:21:39,800
 of valuable is.

305
00:21:39,800 --> 00:21:45,630
 You accumulate debt through your life, the debt of evil,

306
00:21:45,630 --> 00:21:48,280
 the debt of corruption.

307
00:21:48,280 --> 00:22:01,480
 When you die, you have to pay up.

308
00:22:01,480 --> 00:22:05,400
 The third treasure is hiri.

309
00:22:05,400 --> 00:22:09,720
 The third and the fourth are hiri and otapa.

310
00:22:09,720 --> 00:22:13,440
 These go together, so they have to be explained together.

311
00:22:13,440 --> 00:22:15,320
 They're shame and fear.

312
00:22:15,320 --> 00:22:18,320
 Those are the words we use to describe them.

313
00:22:18,320 --> 00:22:22,180
 It doesn't do them justice, but that is the words the

314
00:22:22,180 --> 00:22:23,440
 Buddha used.

315
00:22:23,440 --> 00:22:26,680
 The meaning isn't that you actually feel bad or so on.

316
00:22:26,680 --> 00:22:31,190
 Shame means a disinclination to perform evil deeds, to

317
00:22:31,190 --> 00:22:34,080
 perform those unethical deeds.

318
00:22:34,080 --> 00:22:40,100
 Here is a state of mind that focuses on karma and says

319
00:22:40,100 --> 00:22:47,320
 about bad karma just through understanding,

320
00:22:47,320 --> 00:22:49,640
 through experience.

321
00:22:49,640 --> 00:22:55,550
 When you see again and again what your evil deeds do, what

322
00:22:55,550 --> 00:22:58,760
 your impurities are, you see

323
00:22:58,760 --> 00:23:03,160
 what certain deeds, certain states of mind, certain incl

324
00:23:03,160 --> 00:23:05,760
inations, you see their results

325
00:23:05,760 --> 00:23:07,360
 in meditation again and again.

326
00:23:07,360 --> 00:23:14,920
 You see where your mind leads you.

327
00:23:14,920 --> 00:23:18,000
 You shy away from it.

328
00:23:18,000 --> 00:23:19,000
 You think about those.

329
00:23:19,000 --> 00:23:23,680
 The idea of doing this or inclining in this direction is

330
00:23:23,680 --> 00:23:25,160
 undesirable.

331
00:23:25,160 --> 00:23:29,680
 There's an aversion in a sense to it.

332
00:23:29,680 --> 00:23:30,680
 There's a recoiling from it.

333
00:23:30,680 --> 00:23:33,600
 That's hiri, that's shame and otapa, fear.

334
00:23:33,600 --> 00:23:37,680
 Otapa is the same idea, but it's focused on results.

335
00:23:37,680 --> 00:23:41,440
 It's the fear of the results of bad deeds.

336
00:23:41,440 --> 00:23:49,000
 When you think about certain acts or thoughts or words that

337
00:23:49,000 --> 00:23:54,160
 you might do or say or think,

338
00:23:54,160 --> 00:24:00,120
 you consider that there's the result of those deeds.

339
00:24:00,120 --> 00:24:02,280
 What would be the outcome?

340
00:24:02,280 --> 00:24:05,560
 What would they lead to?

341
00:24:05,560 --> 00:24:09,820
 You recoil from that, meaning wisely you understand that

342
00:24:09,820 --> 00:24:11,880
 that's not a good result.

343
00:24:11,880 --> 00:24:16,630
 Doing that, saying that, thinking that, killing, stealing,

344
00:24:16,630 --> 00:24:18,200
 lying, cheating.

345
00:24:18,200 --> 00:24:23,880
 By careful observation and thorough understanding, you come

346
00:24:23,880 --> 00:24:26,840
 to see how negative the result of

347
00:24:26,840 --> 00:24:29,760
 these things is.

348
00:24:29,760 --> 00:24:36,320
 The results are hiri and otapa.

349
00:24:36,320 --> 00:24:44,040
 This is not something that someone can take away from you.

350
00:24:44,040 --> 00:24:46,970
 You have these things, if you understand these things, and

351
00:24:46,970 --> 00:24:48,520
 you have no concern for people

352
00:24:48,520 --> 00:24:53,160
 forcing you to do evil deeds or coercing you.

353
00:24:53,160 --> 00:24:56,680
 There's nothing more valuable than that.

354
00:24:56,680 --> 00:24:58,480
 You start to see what's truly valuable.

355
00:24:58,480 --> 00:25:02,480
 They might give you money, bribe you money to do this, to

356
00:25:02,480 --> 00:25:03,320
 do that.

357
00:25:03,320 --> 00:25:09,710
 There was a story of a leper, I think, sending a very poor

358
00:25:09,710 --> 00:25:10,600
 man.

359
00:25:10,600 --> 00:25:15,670
 Mara, one of the gods, came to test him and said, "I'll

360
00:25:15,670 --> 00:25:18,280
 make you a rich man if you just

361
00:25:18,280 --> 00:25:20,280
 renounce the Buddha.

362
00:25:20,280 --> 00:25:22,440
 Say I don't take refuge in the Buddha, the Dhamma, and the

363
00:25:22,440 --> 00:25:23,000
 Sangha."

364
00:25:23,000 --> 00:25:26,320
 He was a sotapan of this guy.

365
00:25:26,320 --> 00:25:28,680
 He says, "Who are you?

366
00:25:28,680 --> 00:25:29,880
 Get away from me.

367
00:25:29,880 --> 00:25:31,880
 I'm not poor.

368
00:25:31,880 --> 00:25:34,400
 I don't need money.

369
00:25:34,400 --> 00:25:35,400
 I'm rich."

370
00:25:35,400 --> 00:25:45,640
 We have to ask ourselves what is truly valuable.

371
00:25:45,640 --> 00:25:51,030
 These are things that are truly valuable that no one can

372
00:25:51,030 --> 00:25:53,200
 take away from you.

373
00:25:53,200 --> 00:25:55,400
 Number five is sutta.

374
00:25:55,400 --> 00:25:57,560
 Sutta means learning.

375
00:25:57,560 --> 00:26:00,640
 Learning is something that can be taken away from you.

376
00:26:00,640 --> 00:26:02,040
 You can forget it.

377
00:26:02,040 --> 00:26:05,240
 Nonetheless, it's very valuable.

378
00:26:05,240 --> 00:26:08,600
 It can't be taken away by ordinary means.

379
00:26:08,600 --> 00:26:14,480
 You can't rip it from someone's mind without giving them a

380
00:26:14,480 --> 00:26:15,920
 lobotomy.

381
00:26:15,920 --> 00:26:18,080
 It's really for most of us, it's there to stay.

382
00:26:18,080 --> 00:26:21,240
 It's a very valuable thing to have knowledge.

383
00:26:21,240 --> 00:26:26,070
 Of course, there's knowledge in a worldly sense, and we

384
00:26:26,070 --> 00:26:28,680
 know that that's valuable.

385
00:26:28,680 --> 00:26:34,500
 But here we're really talking about learning spiritual

386
00:26:34,500 --> 00:26:39,160
 teachings, learning those teachings

387
00:26:39,160 --> 00:26:43,770
 that are going to help us on our path to become free from

388
00:26:43,770 --> 00:26:45,120
 suffering.

389
00:26:45,120 --> 00:26:46,400
 That's of great value.

390
00:26:46,400 --> 00:26:50,900
 Even just memorizing the Buddha's teachings or learning it

391
00:26:50,900 --> 00:26:52,120
 thoroughly.

392
00:26:52,120 --> 00:26:59,360
 It's like having a guide on the path.

393
00:26:59,360 --> 00:27:01,680
 It's invaluable.

394
00:27:01,680 --> 00:27:06,240
 Something that is priceless.

395
00:27:06,240 --> 00:27:07,720
 Something that you can't do without.

396
00:27:07,720 --> 00:27:12,620
 It's necessary, and you would say, "Without it, you're

397
00:27:12,620 --> 00:27:14,360
 wandering lost."

398
00:27:14,360 --> 00:27:17,830
 So learning in Buddhism, we talk a lot about how it's not

399
00:27:17,830 --> 00:27:19,640
 enough and maybe presented as

400
00:27:19,640 --> 00:27:23,500
 something that's inferior to practice, which of course it

401
00:27:23,500 --> 00:27:23,960
 is.

402
00:27:23,960 --> 00:27:29,120
 Nonetheless, great learning is a great treasure.

403
00:27:29,120 --> 00:27:33,000
 Something you don't have to go and look it up in a book.

404
00:27:33,000 --> 00:27:37,530
 When you know something, when you've learned something, you

405
00:27:37,530 --> 00:27:39,320
 can carry it with you.

406
00:27:39,320 --> 00:27:47,110
 The sixth treasure is jhaga, which means generosity or

407
00:27:47,110 --> 00:27:48,440
 giving.

408
00:27:48,440 --> 00:27:50,120
 Giving is maybe the best.

409
00:27:50,120 --> 00:27:51,120
 Why?

410
00:27:51,120 --> 00:28:02,680
 Because giving also means giving up.

411
00:28:02,680 --> 00:28:07,560
 Giving the ability to give things away.

412
00:28:07,560 --> 00:28:10,170
 Charity is an important part of this, being generous to

413
00:28:10,170 --> 00:28:10,680
 others.

414
00:28:10,680 --> 00:28:12,680
 This is something people can't take away from you.

415
00:28:12,680 --> 00:28:13,960
 They can take away all.

416
00:28:13,960 --> 00:28:18,660
 You can hoard up all your treasure and it can be taken away

417
00:28:18,660 --> 00:28:20,240
 in a heartbeat.

418
00:28:20,240 --> 00:28:25,700
 But charity is something people can't take away unless you

419
00:28:25,700 --> 00:28:28,480
 have nothing, I suppose.

420
00:28:28,480 --> 00:28:31,260
 But you see, it's really impossible to have nothing,

421
00:28:31,260 --> 00:28:32,880
 especially if you have all these

422
00:28:32,880 --> 00:28:34,480
 noble treasures.

423
00:28:34,480 --> 00:28:39,290
 They might take away your belongings, your material

424
00:28:39,290 --> 00:28:42,440
 possessions, but you can still be

425
00:28:42,440 --> 00:28:48,100
 very generous and kind, helping others, teaching others,

426
00:28:48,100 --> 00:28:51,640
 advising others, relating the things

427
00:28:51,640 --> 00:28:54,760
 you've learned.

428
00:28:54,760 --> 00:28:57,920
 Generosity is a great gift.

429
00:28:57,920 --> 00:29:03,640
 The ability to give is a great wealth.

430
00:29:03,640 --> 00:29:06,440
 So many good things come from it.

431
00:29:06,440 --> 00:29:11,400
 Friendship, esteem, really all the good things in life.

432
00:29:11,400 --> 00:29:14,360
 The worldly material of the good things in life, they all

433
00:29:14,360 --> 00:29:15,880
 come not from hoarding them

434
00:29:15,880 --> 00:29:19,400
 or not from seeking them out, but they come from generosity

435
00:29:19,400 --> 00:29:21,320
, from things like generosity

436
00:29:21,320 --> 00:29:26,800
 and kindness and sharing, giving these things up.

437
00:29:26,800 --> 00:29:30,800
 When you give them up, you're more inclined to, people are

438
00:29:30,800 --> 00:29:32,840
 more inclined to appreciate

439
00:29:32,840 --> 00:29:33,840
 you.

440
00:29:33,840 --> 00:29:42,080
 Be grateful for you.

441
00:29:42,080 --> 00:29:51,020
 And the last one is panya, the wealth, the treasure of

442
00:29:51,020 --> 00:29:52,840
 wisdom.

443
00:29:52,840 --> 00:29:59,280
 It's important to see that this is separate from suta.

444
00:29:59,280 --> 00:30:04,720
 So what we mean here is not wisdom through learning or

445
00:30:04,720 --> 00:30:07,640
 intellectual activity.

446
00:30:07,640 --> 00:30:12,000
 Suta means learning.

447
00:30:12,000 --> 00:30:26,040
 Panya means wisdom in the sense of understanding.

448
00:30:26,040 --> 00:30:27,360
 And this is what comes from.

449
00:30:27,360 --> 00:30:31,520
 This is what we mean when we talk about vipassana.

450
00:30:31,520 --> 00:30:36,460
 This is what we strive for through the meditation practice,

451
00:30:36,460 --> 00:30:39,560
 insight meditation, mindfulness

452
00:30:39,560 --> 00:30:41,560
 meditation.

453
00:30:41,560 --> 00:30:45,220
 As you practice, you come to understand about yourself and

454
00:30:45,220 --> 00:30:48,280
 about the world around you, impermanent.

455
00:30:48,280 --> 00:30:51,940
 The things that you cling to as stable, as satisfying, as

456
00:30:51,940 --> 00:30:53,960
 valuable, you come to see that

457
00:30:53,960 --> 00:30:54,960
 they're impermanent.

458
00:30:54,960 --> 00:30:59,320
 And in fact, not really valuable at all.

459
00:30:59,320 --> 00:31:01,320
 They can't really make you happy.

460
00:31:01,320 --> 00:31:02,320
 So, let's go.

461
00:31:02,320 --> 00:31:03,320
 Okay.

462
00:31:03,320 --> 00:31:04,320
 Okay.

463
00:31:04,320 --> 00:31:05,320
 Okay.

464
00:31:05,320 --> 00:31:06,320
 Okay.

465
00:31:06,320 --> 00:31:07,320
 Okay.

466
00:31:07,320 --> 00:31:08,320
 Okay.

467
00:31:08,320 --> 00:31:09,320
 Okay.

468
00:31:09,320 --> 00:31:10,320
 Okay.

469
00:31:10,320 --> 00:31:11,320
 Okay.

470
00:31:11,320 --> 00:31:12,320
 Okay.

471
00:31:12,320 --> 00:31:13,320
 Okay.

472
00:31:13,320 --> 00:31:14,320
 Okay.

473
00:31:14,320 --> 00:31:15,320
 Okay.

474
00:31:15,320 --> 00:31:16,320
 Okay.

475
00:31:16,320 --> 00:31:17,320
 Okay.

476
00:31:17,320 --> 00:31:18,320
 Okay.

477
00:31:18,320 --> 00:31:19,320
 Okay.

478
00:31:19,320 --> 00:31:20,320
 Okay.

479
00:31:20,320 --> 00:31:21,320
 Okay.

480
00:31:21,320 --> 00:31:22,320
 Okay.

481
00:31:22,320 --> 00:31:23,320
 Okay.

482
00:31:23,320 --> 00:31:24,320
 Okay.

483
00:31:24,320 --> 00:31:25,320
 Okay.

484
00:31:25,320 --> 00:31:26,320
 Okay.

485
00:31:26,320 --> 00:31:27,320
 Okay.

486
00:31:27,320 --> 00:31:28,320
 Okay.

487
00:31:28,320 --> 00:31:29,320
 Okay.

488
00:31:29,320 --> 00:31:30,320
 Okay.

489
00:31:30,320 --> 00:31:31,320
 Okay.

490
00:31:31,320 --> 00:31:35,320
 Sorry, we're having a little trouble with the audio.

491
00:31:35,320 --> 00:31:37,320
 I don't know if that helps.

492
00:31:37,320 --> 00:31:44,320
 It's really hard to adjust this.

493
00:31:44,320 --> 00:31:48,320
 All right.

494
00:31:48,320 --> 00:31:53,820
 Seeing things as they are, learning to adjust your sentence

495
00:31:53,820 --> 00:31:58,800
 of what's valuable, what's useful,

496
00:31:58,800 --> 00:32:02,370
 see what's right and what's wrong, and to align yourself

497
00:32:02,370 --> 00:32:04,480
 with what's right, with what's

498
00:32:04,480 --> 00:32:10,010
 actually going to make you happy, to understand what's

499
00:32:10,010 --> 00:32:12,000
 truly valuable.

500
00:32:12,000 --> 00:32:18,680
 It's really what we aim for in Buddhism.

501
00:32:18,680 --> 00:32:21,140
 Wisdom is the most valuable, something people can't take

502
00:32:21,140 --> 00:32:22,480
 away from you, certainly.

503
00:32:22,480 --> 00:32:28,160
 If you have true wisdom, it can never leave you.

504
00:32:28,160 --> 00:32:35,170
 Wisdom is something that changes the core of who you are,

505
00:32:35,170 --> 00:32:39,040
 changes your future, changes

506
00:32:39,040 --> 00:32:40,560
 your destiny.

507
00:32:40,560 --> 00:32:45,400
 Wisdom is that which really changes and really frees you

508
00:32:45,400 --> 00:32:48,320
 from all the bad things, all the

509
00:32:48,320 --> 00:32:53,460
 things, all the bad paths you might follow through lack of

510
00:32:53,460 --> 00:32:57,520
 wisdom, through lack of understanding,

511
00:32:57,520 --> 00:32:58,520
 right?

512
00:32:58,520 --> 00:33:03,580
 If we knew something was bad for us, why would we go that

513
00:33:03,580 --> 00:33:04,280
 way?

514
00:33:04,280 --> 00:33:08,220
 The only way to truly be free from suffering is to

515
00:33:08,220 --> 00:33:11,600
 cultivate understanding of those things

516
00:33:11,600 --> 00:33:14,900
 that cause us suffering and those things that actually

517
00:33:14,900 --> 00:33:16,440
 truly lead to happiness.

518
00:33:16,440 --> 00:33:19,220
 That's a very difficult thing to do because it's easy to

519
00:33:19,220 --> 00:33:21,200
 think that something is valuable.

520
00:33:21,200 --> 00:33:25,810
 It's much more difficult to actually know and be sure

521
00:33:25,810 --> 00:33:28,880
 through understanding, through

522
00:33:28,880 --> 00:33:34,440
 realization.

523
00:33:34,440 --> 00:33:35,440
 That's the dhamma for tonight.

524
00:33:35,440 --> 00:33:40,560
 I apologize if the recordings were not that good, but

525
00:33:40,560 --> 00:33:44,200
 dealing with limited, really hacking

526
00:33:44,200 --> 00:33:52,880
 this to get a broadcast up at all.

527
00:33:52,880 --> 00:34:12,880
 Hopefully, it was at least more or less understandable.

528
00:34:12,880 --> 00:34:16,280
 Okay, so we have a question on the site.

529
00:34:16,280 --> 00:34:26,720
 Is the site working again?

530
00:34:26,720 --> 00:34:32,520
 Yes.

531
00:34:32,520 --> 00:34:35,200
 Why do you have people ask questions about my monastic life

532
00:34:35,200 --> 00:34:35,440
?

533
00:34:35,440 --> 00:34:38,520
 Is it really all that interesting?

534
00:34:38,520 --> 00:35:02,240
 There's a bunch of questions on the site.

535
00:35:02,240 --> 00:35:03,240
 You can go.

536
00:35:03,240 --> 00:35:04,240
 Yes, please.

537
00:35:04,240 --> 00:35:07,440
 Rather, you guys don't stay around for the questions

538
00:35:07,440 --> 00:35:09,480
 because they're not related to your

539
00:35:09,480 --> 00:35:12,480
 practice.

540
00:35:12,480 --> 00:35:29,960
 They're not related to practice at all.

541
00:35:29,960 --> 00:35:34,920
 So when I ordained, there were 18 of us ordained, but 17 of

542
00:35:34,920 --> 00:35:37,120
 them were just ordaining.

543
00:35:37,120 --> 00:35:41,040
 Well, 15 of them were just ordaining temporarily.

544
00:35:41,040 --> 00:35:43,200
 That's the answer to that.

545
00:35:43,200 --> 00:35:45,400
 Two of them were kind of not sure, I think.

546
00:35:45,400 --> 00:35:46,400
 Those were the two foreigners.

547
00:35:46,400 --> 00:35:49,400
 Actually, I don't really know.

548
00:35:49,400 --> 00:35:50,880
 I don't really know the story of all 18.

549
00:35:50,880 --> 00:35:54,020
 I know that 15 of them were ordaining for a specific

550
00:35:54,020 --> 00:35:54,840
 function.

551
00:35:54,840 --> 00:35:57,640
 They were ordaining for the king's birthday.

552
00:35:57,640 --> 00:36:10,840
 So that was that.

