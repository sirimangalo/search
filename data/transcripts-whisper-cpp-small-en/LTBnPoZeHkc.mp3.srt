1
00:00:00,000 --> 00:00:05,000
 Hello, Bhante. Buddhism is a religion that is made to make

2
00:00:05,000 --> 00:00:10,000
 one individual the path to enlightenment.

3
00:00:10,000 --> 00:00:13,180
 How can it be that you don't hear about Buddhists that

4
00:00:13,180 --> 00:00:17,000
 become enlightened, arahant or a Buddha? Thanks.

5
00:00:17,000 --> 00:00:20,000
 Well, you're not talking to the right people, I guess.

6
00:00:20,000 --> 00:00:25,000
 There's a lot of people out there who do talk about people

7
00:00:25,000 --> 00:00:27,000
 becoming enlightened, arahants or Buddha.

8
00:00:27,000 --> 00:00:32,320
 But I imagine in Israel you wouldn't hear so much. But the

9
00:00:32,320 --> 00:00:34,550
 other thing, the obvious answer here is that people don't

10
00:00:34,550 --> 00:00:35,000
 talk about it.

11
00:00:35,000 --> 00:00:38,160
 And enlightened beings are incredibly disinclined to talk

12
00:00:38,160 --> 00:00:40,000
 about their own enlightenment.

13
00:00:40,000 --> 00:00:46,480
 Even the sauta-pana is someone who has just begun on the

14
00:00:46,480 --> 00:00:52,720
 path to enlightenment, has given up that idea and is

15
00:00:52,720 --> 00:01:00,740
 incredibly disinclined towards bragging or announcing their

16
00:01:00,740 --> 00:01:03,000
 enlightenment.

17
00:01:03,000 --> 00:01:06,880
 So even Buddhists don't hear about it and we generally

18
00:01:06,880 --> 00:01:10,320
 speculate about our teachers and so on. And often that

19
00:01:10,320 --> 00:01:12,790
 speculation goes overboard and suddenly everybody's an arah

20
00:01:12,790 --> 00:01:15,000
ant, everybody's enlightened.

21
00:01:15,000 --> 00:01:18,540
 The reason you don't hear about people becoming Buddha is

22
00:01:18,540 --> 00:01:22,000
 because it's apparently not possible at this point.

23
00:01:22,000 --> 00:01:26,720
 I think it would be impossible for two reasons. One is that

24
00:01:26,720 --> 00:01:29,000
 the Buddha-Sassana is still here.

25
00:01:29,000 --> 00:01:32,840
 And two, so the world is not currently capable of

26
00:01:32,840 --> 00:01:37,000
 supporting a private Buddha, a particular Buddha.

27
00:01:37,000 --> 00:01:42,790
 And two, a perfectly enlightened Buddha could not arise in

28
00:01:42,790 --> 00:01:46,000
 this age. This is theory, I mean, it may all be wrong.

29
00:01:46,000 --> 00:01:49,100
 I think it's just what the commentaries say. But the theory

30
00:01:49,100 --> 00:01:52,810
 is that there's too many defilements and their lifespan is

31
00:01:52,810 --> 00:01:56,000
 too short at this point for a Buddha to arise.

32
00:01:56,000 --> 00:01:59,470
 A Buddha would not arise at this time. The other thing is,

33
00:01:59,470 --> 00:02:03,490
 first of all, we have a prophecy of when the next Buddha is

34
00:02:03,490 --> 00:02:09,480
 coming and we have an understanding that it's a very rare

35
00:02:09,480 --> 00:02:11,000
 thing for a Buddha to arise.

36
00:02:11,000 --> 00:02:14,420
 So the chances even without that prophecy of a Buddha

37
00:02:14,420 --> 00:02:18,050
 arising in this day and age or anytime soon is very, very

38
00:02:18,050 --> 00:02:20,000
 rare because we just had one.

39
00:02:20,000 --> 00:02:22,140
 And there's some kind of gravity involved, I would say. I

40
00:02:22,140 --> 00:02:25,230
 would speculate that anybody who was even close to becoming

41
00:02:25,230 --> 00:02:28,570
 a Buddha had to have been drawn towards this Buddha, like

42
00:02:28,570 --> 00:02:31,000
 Mahakat Jayana, I think.

43
00:02:31,000 --> 00:02:33,580
 Mahakat Jayana, I think, if I remember, or it was Kasapa,

44
00:02:33,580 --> 00:02:36,420
 one or the other, was destined to become a Buddha, had made

45
00:02:36,420 --> 00:02:39,000
 a determination to become a Buddha and gave it up.

46
00:02:39,000 --> 00:02:42,770
 I can't remember which one it was. See, bad teacher. I don

47
00:02:42,770 --> 00:02:47,920
't know my stuff. But one of them. And they gave it up when

48
00:02:47,920 --> 00:02:52,060
 they met the Buddha because they realized that it was much

49
00:02:52,060 --> 00:02:56,470
 better for them to become enlightened here and now and pass

50
00:02:56,470 --> 00:03:00,000
 on the teachings, help spread the teachings here and now.

