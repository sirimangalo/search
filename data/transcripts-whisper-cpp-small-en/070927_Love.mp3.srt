1
00:00:00,000 --> 00:00:12,000
 Today it's my turn again to give a talk.

2
00:00:12,000 --> 00:00:22,240
 Today's talk I thought would be on love.

3
00:00:22,240 --> 00:00:28,620
 Last night we got a call from the British Broadcasting

4
00:00:28,620 --> 00:00:36,000
 Corporation, they call the BBC,

5
00:00:36,000 --> 00:00:43,440
 and left a comment on a news article about Burma.

6
00:00:43,440 --> 00:00:46,280
 And they got back to me and asked if they could call me.

7
00:00:46,280 --> 00:00:48,360
 It was quite a surprise.

8
00:00:48,360 --> 00:00:59,130
 And they called and I talked with some different people

9
00:00:59,130 --> 00:01:03,680
 live on the BBC for almost half an

10
00:01:03,680 --> 00:01:10,520
 hour.

11
00:01:10,520 --> 00:01:15,800
 And I think this is an appropriate reason to, or it's

12
00:01:15,800 --> 00:01:18,960
 appropriate to connect this with

13
00:01:18,960 --> 00:01:23,760
 the talk on love.

14
00:01:23,760 --> 00:01:33,860
 Because what we find in the world nowadays are many

15
00:01:33,860 --> 00:01:37,440
 situations where we find ourselves

16
00:01:37,440 --> 00:01:45,480
 as global citizens, as citizens of various nations, as

17
00:01:45,480 --> 00:01:52,000
 members of various religious organizations.

18
00:01:52,000 --> 00:01:56,350
 In this case we find ourselves as Buddhists being

19
00:01:56,350 --> 00:01:59,640
 confronted with situations where we

20
00:01:59,640 --> 00:02:09,360
 are forced to find something to say.

21
00:02:09,360 --> 00:02:18,720
 We are forced to take a position.

22
00:02:18,720 --> 00:02:23,930
 It's very easy for us simply to say, "Well, we're medit

23
00:02:23,930 --> 00:02:24,920
ators.

24
00:02:24,920 --> 00:02:31,240
 The world doesn't matter to us."

25
00:02:31,240 --> 00:02:36,400
 And not say anything at all.

26
00:02:36,400 --> 00:02:41,280
 And in some ways this is most appealing.

27
00:02:41,280 --> 00:02:45,150
 If indeed we have no connection with the outside world,

28
00:02:45,150 --> 00:02:47,520
 then there's no one calling us on the

29
00:02:47,520 --> 00:02:50,520
 phone.

30
00:02:50,520 --> 00:03:00,090
 Then most likely the most comfortable thing for us to do is

31
00:03:00,090 --> 00:03:06,720
 simply to just continue meditating.

32
00:03:06,720 --> 00:03:17,850
 But when we undertake to become meditators on a long-term

33
00:03:17,850 --> 00:03:22,840
 basis, it comes to be a part

34
00:03:22,840 --> 00:03:25,000
 of our daily life.

35
00:03:25,000 --> 00:03:31,560
 Or our daily life comes to be a part of the meditation

36
00:03:31,560 --> 00:03:33,320
 practice.

37
00:03:33,320 --> 00:03:36,110
 And we find ourselves confronted with situations where we

38
00:03:36,110 --> 00:03:37,880
 have to teach meditation, where we

39
00:03:37,880 --> 00:03:44,280
 have to teach the Buddhist teaching.

40
00:03:44,280 --> 00:03:47,150
 And we start to see that meditation actually is a much

41
00:03:47,150 --> 00:03:49,200
 bigger subject than simply walking

42
00:03:49,200 --> 00:03:53,080
 and sitting.

43
00:03:53,080 --> 00:03:58,990
 The ways of explaining about meditation, about freedom from

44
00:03:58,990 --> 00:04:01,920
 suffering are actually a very

45
00:04:01,920 --> 00:04:04,960
 broad subject.

46
00:04:04,960 --> 00:04:07,340
 When we're talking to people who have never meditated

47
00:04:07,340 --> 00:04:09,200
 before, perhaps would never think

48
00:04:09,200 --> 00:04:11,080
 of meditating.

49
00:04:11,080 --> 00:04:16,360
 We have to use different words, different phrases, and

50
00:04:16,360 --> 00:04:19,200
 start at different levels.

51
00:04:19,200 --> 00:04:25,110
 We cannot simply close our eyes and continue practicing

52
00:04:25,110 --> 00:04:29,080
 because we find ourselves surrounded

53
00:04:29,080 --> 00:04:38,420
 by these people, and we find ourselves building a sense of

54
00:04:38,420 --> 00:04:42,520
 love for these people.

55
00:04:42,520 --> 00:04:46,120
 Now I'll get into the idea of love in just a second, but

56
00:04:46,120 --> 00:04:48,000
 just a little bit more about

57
00:04:48,000 --> 00:04:52,760
 the situation which we're talking about in Burma.

58
00:04:52,760 --> 00:05:01,910
 It appears that the situation has become so repressive and

59
00:05:01,910 --> 00:05:06,640
 so terrible or uncomfortable

60
00:05:06,640 --> 00:05:10,890
 for the people of Burma that they can take it no longer,

61
00:05:10,890 --> 00:05:15,640
 and they are willing to die,

62
00:05:15,640 --> 00:05:21,720
 they're willing to be killed, rather than continue to face

63
00:05:21,720 --> 00:05:24,600
 the horrible repression and

64
00:05:24,600 --> 00:05:33,560
 suffering conditions.

65
00:05:33,560 --> 00:05:38,180
 But we might want to say that these people are simply

66
00:05:38,180 --> 00:05:40,800
 suffering from their karma and

67
00:05:40,800 --> 00:05:47,450
 should be quiet and continue to suffer and wait for the end

68
00:05:47,450 --> 00:05:50,360
 to come or wait for the end

69
00:05:50,360 --> 00:05:57,360
 of their suffering to come.

70
00:05:57,360 --> 00:06:00,400
 But it's probably not wise to say that.

71
00:06:00,400 --> 00:06:04,660
 It probably doesn't take into account the many factors of

72
00:06:04,660 --> 00:06:06,920
 the situation that most likely

73
00:06:06,920 --> 00:06:09,040
 the end of suffering is here and this is how the end of

74
00:06:09,040 --> 00:06:10,600
 their suffering is coming for many

75
00:06:10,600 --> 00:06:12,920
 people.

76
00:06:12,920 --> 00:06:20,130
 That bad karma which has run its course is now coming to an

77
00:06:20,130 --> 00:06:23,200
 end and there is a change

78
00:06:23,200 --> 00:06:29,270
 coming, perhaps for the better for some people, perhaps for

79
00:06:29,270 --> 00:06:32,080
 the worse for some people.

80
00:06:32,080 --> 00:06:39,450
 But the important point and the important topic for myself

81
00:06:39,450 --> 00:06:42,380
 and I think for all of us

82
00:06:42,380 --> 00:06:48,950
 is the position which the Buddhist monks involved in the

83
00:06:48,950 --> 00:06:52,520
 protests should be taking.

84
00:06:52,520 --> 00:06:57,900
 And this is where I like to start to talk about the idea of

85
00:06:57,900 --> 00:07:00,600
 love because the Buddhist

86
00:07:00,600 --> 00:07:07,980
 monks have taken the stance in Burma of wishing for love,

87
00:07:07,980 --> 00:07:10,800
 for there to be love.

88
00:07:10,800 --> 00:07:13,640
 And perhaps love is a bad word but it's important to use it

89
00:07:13,640 --> 00:07:15,240
 as a title because I'd like to talk

90
00:07:15,240 --> 00:07:17,280
 about the different kinds of love.

91
00:07:17,280 --> 00:07:23,570
 But here they mean loving kindness or kindness which

92
00:07:23,570 --> 00:07:26,200
 springs from love.

93
00:07:26,200 --> 00:07:31,000
 And so we see pictures and here's the monks chanting the

94
00:07:31,000 --> 00:07:34,200
 metasuta, the discourse on loving

95
00:07:34,200 --> 00:07:39,860
 kindness, wishing for there to be sympathy and caring and

96
00:07:39,860 --> 00:07:42,760
 love among the people of Burma

97
00:07:42,760 --> 00:07:51,600
 rather than repression and authoritarianism and so on.

98
00:07:51,600 --> 00:07:55,780
 Personally though I could certainly be wrong, I don't think

99
00:07:55,780 --> 00:07:58,680
 this is a bad thing to see monks

100
00:07:58,680 --> 00:08:07,240
 even taking to the street to let it be known, to announce

101
00:08:07,240 --> 00:08:11,920
 or to proclaim to the people in

102
00:08:11,920 --> 00:08:17,290
 charge, in power, to the people in public in Burma, even to

103
00:08:17,290 --> 00:08:19,880
 the ordinary people, to proclaim

104
00:08:19,880 --> 00:08:25,650
 to them the truth and proclaim that which is wrong, to be

105
00:08:25,650 --> 00:08:28,240
 wrong, that which is right,

106
00:08:28,240 --> 00:08:30,280
 to be right.

107
00:08:30,280 --> 00:08:39,190
 And I think to this extent proclaiming denouncing hatred

108
00:08:39,190 --> 00:08:44,320
 and stinginess and miserliness for

109
00:08:44,320 --> 00:08:50,680
 what it is and wishing for people to be more loving and

110
00:08:50,680 --> 00:08:54,640
 more caring, that the people in

111
00:08:54,640 --> 00:09:02,360
 charge might be more caring for their citizens.

112
00:09:02,360 --> 00:09:08,000
 But I think it's important that it stops there.

113
00:09:08,000 --> 00:09:12,980
 The teaching I would like to talk about today is the

114
00:09:12,980 --> 00:09:15,160
 teaching on love.

115
00:09:15,160 --> 00:09:18,620
 And I think this is the most powerful weapon which we have

116
00:09:18,620 --> 00:09:20,480
 and which the people of Burma

117
00:09:20,480 --> 00:09:25,520
 now have.

118
00:09:25,520 --> 00:09:32,690
 Or it's one of the most powerful weapons we will have in

119
00:09:32,690 --> 00:09:37,200
 dealing with enemy forces.

120
00:09:37,200 --> 00:09:47,120
 In dealing with people who are antagonistic towards us.

121
00:09:47,120 --> 00:09:52,430
 We hear around the world of people, of nations, of those

122
00:09:52,430 --> 00:09:55,520
 people in power getting ready to

123
00:09:55,520 --> 00:09:58,120
 impose sanctions on Burma.

124
00:09:58,120 --> 00:10:02,750
 And of course we have sanctions years and years and years,

125
00:10:02,750 --> 00:10:04,920
 we already have sanctions

126
00:10:04,920 --> 00:10:12,520
 towards Burma.

127
00:10:12,520 --> 00:10:14,760
 And I don't have anything to say really on that.

128
00:10:14,760 --> 00:10:19,470
 I think it's a fairly political subject which I would

129
00:10:19,470 --> 00:10:21,840
 rather stay away from.

130
00:10:21,840 --> 00:10:27,130
 But I think a much more important and a much more

131
00:10:27,130 --> 00:10:30,960
 beneficial thing right now is to examine

132
00:10:30,960 --> 00:10:34,690
 the situation in Burma, examine the situations in our own

133
00:10:34,690 --> 00:10:36,880
 societies, in our own lives, in

134
00:10:36,880 --> 00:10:38,760
 our own families.

135
00:10:38,760 --> 00:10:46,180
 And come to see that this world is very much lacking in the

136
00:10:46,180 --> 00:10:49,800
 virtue of loving kindness.

137
00:10:49,800 --> 00:10:52,680
 What we need for Burma now is love.

138
00:10:52,680 --> 00:11:00,580
 What we need for each other is a sense of love.

139
00:11:00,580 --> 00:11:05,210
 And surely it is not something which will change things

140
00:11:05,210 --> 00:11:06,360
 overnight.

141
00:11:06,360 --> 00:11:13,320
 It probably won't stop people in Burma from being killed.

142
00:11:13,320 --> 00:11:15,720
 But then you can't stop people from dying.

143
00:11:15,720 --> 00:11:20,140
 In the end, everyone, whether they're killed or run over by

144
00:11:20,140 --> 00:11:22,000
 a car or die of old age, in

145
00:11:22,000 --> 00:11:23,960
 the end we all die.

146
00:11:23,960 --> 00:11:27,520
 This is only a short part of our trip.

147
00:11:27,520 --> 00:11:32,090
 If we die of blood wound, maybe next life we'll come back

148
00:11:32,090 --> 00:11:34,960
 and live a long and happy life.

149
00:11:34,960 --> 00:11:39,280
 If we live a long and pleasurable life this time, it could

150
00:11:39,280 --> 00:11:41,320
 be that next life, we will

151
00:11:41,320 --> 00:11:48,980
 be born in a repressive situation ourselves based on our

152
00:11:48,980 --> 00:11:51,840
 good or bad deeds.

153
00:11:51,840 --> 00:11:57,680
 If we wish for these things to disappear from the world,

154
00:11:57,680 --> 00:12:01,040
 then the most important thing for

155
00:12:01,040 --> 00:12:05,150
 us is to develop these qualities of mind such as loving

156
00:12:05,150 --> 00:12:06,240
 kindness.

157
00:12:06,240 --> 00:12:11,050
 And so today I'd like to talk about love given this is an

158
00:12:11,050 --> 00:12:14,880
 introduction so that we may all

159
00:12:14,880 --> 00:12:18,660
 work to develop it in our own hearts for the benefit of not

160
00:12:18,660 --> 00:12:20,600
 only ourselves but also the

161
00:12:20,600 --> 00:12:24,120
 world around us.

162
00:12:24,120 --> 00:12:27,760
 The idea of love, it's a very dangerous subject.

163
00:12:27,760 --> 00:12:34,500
 You can get angry at someone for not being loving.

164
00:12:34,500 --> 00:12:39,590
 You can get angry at someone for telling you that you're

165
00:12:39,590 --> 00:12:41,680
 not loving enough.

166
00:12:41,680 --> 00:12:45,280
 We can be very tricky with the word love.

167
00:12:45,280 --> 00:12:49,560
 We can kill ourselves over love.

168
00:12:49,560 --> 00:12:53,320
 We can kill other people over love.

169
00:12:53,320 --> 00:13:04,360
 So we see we have many different kinds of love.

170
00:13:04,360 --> 00:13:09,240
 Often this subject comes up when I talk about detachment.

171
00:13:09,240 --> 00:13:12,820
 Detachment is not a very popular subject.

172
00:13:12,820 --> 00:13:15,960
 When you give a talk on detachment to people who know very

173
00:13:15,960 --> 00:13:18,120
 little or are very little interested

174
00:13:18,120 --> 00:13:21,430
 in Buddhism, maybe only curious, one of the first things

175
00:13:21,430 --> 00:13:23,040
 they'll ask is, "What about

176
00:13:23,040 --> 00:13:28,120
 all the people I love?

177
00:13:28,120 --> 00:13:29,160
 I love my children.

178
00:13:29,160 --> 00:13:33,440
 You want me to be the type in them?"

179
00:13:33,440 --> 00:13:44,080
 And this question, this very question is a very good way to

180
00:13:44,080 --> 00:13:47,840
 explain to people how love

181
00:13:47,840 --> 00:13:51,880
 can be a good thing or it can be a bad thing.

182
00:13:51,880 --> 00:13:56,250
 And so I always say that your attachment to your children

183
00:13:56,250 --> 00:13:57,440
 is not love.

184
00:13:57,440 --> 00:14:01,320
 Your attachment to your children is attachment.

185
00:14:01,320 --> 00:14:03,360
 Your love for them is your love for them.

186
00:14:03,360 --> 00:14:06,520
 Your attachment to them is your attachment to them.

187
00:14:06,520 --> 00:14:11,010
 The very big thing in your heart towards your children or

188
00:14:11,010 --> 00:14:13,600
 towards your family or towards

189
00:14:13,600 --> 00:14:17,440
 your friends, this very big thing which makes the heart

190
00:14:17,440 --> 00:14:20,080
 expand, which makes the heart grow,

191
00:14:20,080 --> 00:14:23,040
 this is called love.

192
00:14:23,040 --> 00:14:29,870
 That very little thing which contracts the heart, which def

193
00:14:29,870 --> 00:14:33,160
iles the heart, which makes

194
00:14:33,160 --> 00:14:39,840
 the heart grow, makes the heart suffer.

195
00:14:39,840 --> 00:14:44,040
 This is called attachment.

196
00:14:44,040 --> 00:14:47,520
 And the difference can be understood in terms of the

197
00:14:47,520 --> 00:14:50,080
 expression, an expression of love and

198
00:14:50,080 --> 00:14:53,120
 an expression of attachment.

199
00:14:53,120 --> 00:14:57,750
 Whereas we call some things love actually, in reality it's

200
00:14:57,750 --> 00:14:59,720
 nothing to do with love, it

201
00:14:59,720 --> 00:15:02,160
 has only to do with attachment.

202
00:15:02,160 --> 00:15:06,200
 Or it has to do with love in an English sense but not in

203
00:15:06,200 --> 00:15:08,520
 terms of a Buddhist sense.

204
00:15:08,520 --> 00:15:13,570
 Or perhaps the word love is simply just a bad word, too

205
00:15:13,570 --> 00:15:15,320
 general a word.

206
00:15:15,320 --> 00:15:17,690
 But the expression of love and the expression of attachment

207
00:15:17,690 --> 00:15:18,720
 are quite different.

208
00:15:18,720 --> 00:15:27,000
 The expression of attachment is, "May you make me happy."

209
00:15:27,000 --> 00:15:36,680
 The expression of love is, "May I make you happy."

210
00:15:36,680 --> 00:15:39,890
 So when we do something or we say something or we even

211
00:15:39,890 --> 00:15:42,480
 think something, wishing for another

212
00:15:42,480 --> 00:15:46,800
 person to be happy, this is called love.

213
00:15:46,800 --> 00:15:53,400
 This is not attachment.

214
00:15:53,400 --> 00:15:56,520
 And so a detached person can clearly be very full of love.

215
00:15:56,520 --> 00:15:59,150
 A person who has no attachment to any person in the world

216
00:15:59,150 --> 00:16:00,640
 can be the most loving person

217
00:16:00,640 --> 00:16:01,640
 in the world.

218
00:16:01,640 --> 00:16:02,640
 Why?

219
00:16:02,640 --> 00:16:07,130
 Because in their heart there is no, "What have you got for

220
00:16:07,130 --> 00:16:07,760
 me?"

221
00:16:07,760 --> 00:16:12,760
 There's no, "Be this way.

222
00:16:12,760 --> 00:16:14,360
 If you be this way, I'll be happy.

223
00:16:14,360 --> 00:16:16,480
 If you be that way, I'll be unhappy.

224
00:16:16,480 --> 00:16:19,000
 Do this for me, don't do that for me.

225
00:16:19,000 --> 00:16:24,120
 Don't do that for me."

226
00:16:24,120 --> 00:16:27,260
 For a person who is very attached, it's clear that their

227
00:16:27,260 --> 00:16:28,920
 heart will shrink and their heart

228
00:16:28,920 --> 00:16:36,650
 will become rotten even to the point where people kill

229
00:16:36,650 --> 00:16:41,000
 themselves, where they destroy

230
00:16:41,000 --> 00:16:43,920
 other people, kill other people.

231
00:16:43,920 --> 00:16:49,920
 I'm always saying, "Make me happy, make me happy."

232
00:16:49,920 --> 00:16:52,560
 And we can see the result if you have a relationship, for

233
00:16:52,560 --> 00:16:54,600
 instance, any relationship between two

234
00:16:54,600 --> 00:16:59,600
 people, two people who are full of attachment.

235
00:16:59,600 --> 00:17:01,710
 If two people are full of attachment, they're always asking

236
00:17:01,710 --> 00:17:02,760
 from each other, wanting from

237
00:17:02,760 --> 00:17:05,520
 each other.

238
00:17:05,520 --> 00:17:07,640
 So how much does each person get zero?

239
00:17:07,640 --> 00:17:11,320
 The other one gets anything.

240
00:17:11,320 --> 00:17:16,060
 If you have a relationship between two people who are one

241
00:17:16,060 --> 00:17:19,200
 hundred percent giving, then both

242
00:17:19,200 --> 00:17:20,680
 get a hundred percent.

243
00:17:20,680 --> 00:17:22,320
 Both get everything.

244
00:17:22,320 --> 00:17:25,780
 I give you my hundred percent, you give me your one hundred

245
00:17:25,780 --> 00:17:26,560
 percent.

246
00:17:26,560 --> 00:17:30,320
 Neither of us needs anything and we both get everything.

247
00:17:30,320 --> 00:17:35,260
 It's very hard for people to see and they ask these

248
00:17:35,260 --> 00:17:36,840
 questions.

249
00:17:36,840 --> 00:17:38,600
 They criticize Buddhism.

250
00:17:38,600 --> 00:17:43,820
 It tells people to be cold and detached, very dry and color

251
00:17:43,820 --> 00:17:45,080
less life.

252
00:17:45,080 --> 00:17:49,420
 And then you watch them running around like crazy people

253
00:17:49,420 --> 00:17:52,080
 and you think, "Well, if that's

254
00:17:52,080 --> 00:17:59,590
 color, I'll take black and white and I'll take white and

255
00:17:59,590 --> 00:18:02,560
 give up the black."

256
00:18:02,560 --> 00:18:06,880
 But we find our lives full of color.

257
00:18:06,880 --> 00:18:11,340
 We find our lives full of warmth, kindness, love,

258
00:18:11,340 --> 00:18:14,720
 satisfaction, peace, happiness.

259
00:18:14,720 --> 00:18:18,090
 All the things which the people in the world strive for

260
00:18:18,090 --> 00:18:20,320
 proclaim as the ultimate virtues.

261
00:18:20,320 --> 00:18:26,290
 They write it on their, they tattoo it on their arms, they

262
00:18:26,290 --> 00:18:29,280
 write in their notebooks, they

263
00:18:29,280 --> 00:18:37,640
 write poems and sing songs, they watch movies, read books.

264
00:18:37,640 --> 00:18:45,160
 We try their utmost to bring this love into their lives.

265
00:18:45,160 --> 00:18:48,470
 But they get all mixed up and they come to attach more than

266
00:18:48,470 --> 00:18:49,640
 actually love.

267
00:18:49,640 --> 00:18:57,520
 They're unable to find the way to love.

268
00:18:57,520 --> 00:19:04,530
 And so, I think a crucial point which we have to understand

269
00:19:04,530 --> 00:19:07,920
 as people and as meditators

270
00:19:07,920 --> 00:19:14,990
 is that in the Lord Buddha's teaching, the best way for a

271
00:19:14,990 --> 00:19:18,240
 person to be loving are the

272
00:19:18,240 --> 00:19:25,680
 most efficient, the most fruitful path to become a loving,

273
00:19:25,680 --> 00:19:27,640
 kind person.

274
00:19:27,640 --> 00:19:30,530
 It's not simply the practice of love, the practice of

275
00:19:30,530 --> 00:19:32,160
 wishing good things for other

276
00:19:32,160 --> 00:19:35,600
 people.

277
00:19:35,600 --> 00:19:43,230
 The greatest path to find love is the path of gaining

278
00:19:43,230 --> 00:19:48,860
 wisdom, gaining understanding.

279
00:19:48,860 --> 00:19:52,430
 Because we see the world going around, traveling around,

280
00:19:52,430 --> 00:19:54,800
 loving, loving, but it's mixed with

281
00:19:54,800 --> 00:19:57,560
 attachment.

282
00:19:57,560 --> 00:20:00,390
 All the time loving, all the time loving, but without any

283
00:20:00,390 --> 00:20:03,840
 sense of wisdom or understanding,

284
00:20:03,840 --> 00:20:06,840
 without any mindfulness.

285
00:20:06,840 --> 00:20:10,720
 We see people who, we say, "Oh, that person loves their dog

286
00:20:10,720 --> 00:20:11,040
.

287
00:20:11,040 --> 00:20:15,720
 Boy, she really loves her dog."

288
00:20:15,720 --> 00:20:19,720
 In the next moment we see the person hitting their dog.

289
00:20:19,720 --> 00:20:28,540
 You see, the mind is full of good things and bad things and

290
00:20:28,540 --> 00:20:31,960
 it only knows its own inherent

291
00:20:31,960 --> 00:20:34,240
 character.

292
00:20:34,240 --> 00:20:38,470
 We know how to do things the way we have always done things

293
00:20:38,470 --> 00:20:38,800
.

294
00:20:38,800 --> 00:20:44,640
 Sometimes good, sometimes bad, good mixed with bad, love

295
00:20:44,640 --> 00:20:48,160
 mixed with affection or attachment.

296
00:20:48,160 --> 00:20:52,440
 And so we're unable to be truly helpful for people.

297
00:20:52,440 --> 00:21:00,120
 There's a song which goes, this meditation teacher once

298
00:21:00,120 --> 00:21:04,880
 pointed out to us, he said, "Sting

299
00:21:04,880 --> 00:21:08,920
 once said in his song, 'If you love someone, set them free

300
00:21:08,920 --> 00:21:09,440
.'"

301
00:21:09,440 --> 00:21:14,910
 I thought about that a lot, but really it's a very clever

302
00:21:14,910 --> 00:21:16,520
 thing to say.

303
00:21:16,520 --> 00:21:22,000
 It's not just a good song lyric.

304
00:21:22,000 --> 00:21:24,790
 Sometimes song lyrics are not good, don't give good advice,

305
00:21:24,790 --> 00:21:25,280
 I think.

306
00:21:25,280 --> 00:21:28,440
 But in this case I think this song lyric gives good advice.

307
00:21:28,440 --> 00:21:33,720
 "If you love someone, set them free."

308
00:21:33,720 --> 00:21:39,920
 When you love someone and then you give them gifts, you

309
00:21:39,920 --> 00:21:44,520
 know, on their birthday or on holidays,

310
00:21:44,520 --> 00:21:48,820
 this is really a lot more out of affection.

311
00:21:48,820 --> 00:21:52,450
 You want to see them happy and when they're happy you feel

312
00:21:52,450 --> 00:21:53,120
 happy.

313
00:21:53,120 --> 00:22:00,960
 You like to rejoice with them and laugh and have fun and so

314
00:22:00,960 --> 00:22:01,840
 on.

315
00:22:01,840 --> 00:22:06,880
 Because so much of the rest of your life is full of drear

316
00:22:06,880 --> 00:22:10,200
iness and boredom and depression

317
00:22:10,200 --> 00:22:15,350
 and so many different things that you rejoice in these

318
00:22:15,350 --> 00:22:17,000
 festivities.

319
00:22:17,000 --> 00:22:21,750
 But if you really want to help someone, giving them gifts

320
00:22:21,750 --> 00:22:24,400
 and this and that, this is very

321
00:22:24,400 --> 00:22:29,430
 nice and useful and a good thing, a very good practice to

322
00:22:29,430 --> 00:22:30,480
 keep up.

323
00:22:30,480 --> 00:22:33,220
 In Thailand on people's birthdays, on someone's birthday

324
00:22:33,220 --> 00:22:34,720
 you don't give them gifts, they give

325
00:22:34,720 --> 00:22:36,720
 you gifts.

326
00:22:36,720 --> 00:22:41,960
 Adjunct's birthday this year was a little bit different

327
00:22:41,960 --> 00:22:43,160
 than most years.

328
00:22:43,160 --> 00:22:48,250
 He was very structured this year and I'm not sure what

329
00:22:48,250 --> 00:22:51,680
 their reasoning was but Adjunct didn't

330
00:22:51,680 --> 00:22:57,280
 give out gifts to everyone who came to see him right away.

331
00:22:57,280 --> 00:22:58,600
 He gave out gifts afterwards.

332
00:22:58,600 --> 00:23:01,790
 So I managed to slip in near the end and get some of his

333
00:23:01,790 --> 00:23:03,600
 gifts but I think the meditators

334
00:23:03,600 --> 00:23:07,160
 weren't able to get the gifts he had to give.

335
00:23:07,160 --> 00:23:12,400
 It was a picture and some other things.

336
00:23:12,400 --> 00:23:16,750
 But what we did see probably is some of the monks who were

337
00:23:16,750 --> 00:23:19,840
 coming in and we saw the refrigerators

338
00:23:19,840 --> 00:23:28,250
 being carted away at and giving away 189 refrigerators, 189

339
00:23:28,250 --> 00:23:33,240
 refrigerators as well as robes and fans.

340
00:23:33,240 --> 00:23:45,040
 Every year, this year perhaps, the biggest gift giving yet.

341
00:23:45,040 --> 00:23:46,850
 Probably some of the monks flying from Bangkok wondering

342
00:23:46,850 --> 00:23:48,120
 how they're going to get their fridge

343
00:23:48,120 --> 00:23:52,040
 back to Bangkok.

344
00:23:52,040 --> 00:23:55,110
 But these are good things to do but in the end what makes

345
00:23:55,110 --> 00:23:57,120
 Adjunct our teacher so famous

346
00:23:57,120 --> 00:24:02,320
 and so well loved, it's not his giving of gifts.

347
00:24:02,320 --> 00:24:03,920
 It's his giving of the highest gift.

348
00:24:03,920 --> 00:24:06,440
 It's his giving of the gift of freedom.

349
00:24:06,440 --> 00:24:13,200
 This is the meditation practice which we follow.

350
00:24:13,200 --> 00:24:17,200
 The greatest thing which we can do for the world whether it

351
00:24:17,200 --> 00:24:19,280
 be in reference to a specific

352
00:24:19,280 --> 00:24:23,190
 country and Burma is not the only situation in the world

353
00:24:23,190 --> 00:24:25,500
 where people are suffering.

354
00:24:25,500 --> 00:24:28,820
 Even in very well developed countries there are many people

355
00:24:28,820 --> 00:24:29,760
 suffering.

356
00:24:29,760 --> 00:24:33,830
 There are rich people suffering horribly, horrible

357
00:24:33,830 --> 00:24:36,240
 suffering, rich people, people well

358
00:24:36,240 --> 00:24:41,880
 off, people taking anti-depressant drugs, people in upper

359
00:24:41,880 --> 00:24:48,240
 class families taking anti-depressants.

360
00:24:48,240 --> 00:24:50,430
 There are countries in the world where people don't have

361
00:24:50,430 --> 00:24:51,960
 enough food to eat worse than Burma

362
00:24:51,960 --> 00:25:00,120
 I would think, although I don't know.

363
00:25:00,120 --> 00:25:03,420
 There have been situations in every country of the world

364
00:25:03,420 --> 00:25:05,760
 where people have suffered horribly.

365
00:25:05,760 --> 00:25:09,430
 The Cambodian people, how much worse their suffering was

366
00:25:09,430 --> 00:25:11,240
 than the suffering of Burma

367
00:25:11,240 --> 00:25:15,040
 now.

368
00:25:15,040 --> 00:25:17,680
 So much so that when I talked to a, I was a young monk at

369
00:25:17,680 --> 00:25:19,120
 the time and it was probably

370
00:25:19,120 --> 00:25:23,460
 my own fault, I was talking to a Cambodian monk about

371
00:25:23,460 --> 00:25:25,840
 Cambodia and he was talking about

372
00:25:25,840 --> 00:25:28,020
 all the horrors and tragedy that I couldn't just, I just

373
00:25:28,020 --> 00:25:29,240
 couldn't understand and I was

374
00:25:29,240 --> 00:25:32,430
 willing to accept, yes I can understand, but I said, "Well

375
00:25:32,430 --> 00:25:33,760
 it's really all just their

376
00:25:33,760 --> 00:25:34,760
 bad karma."

377
00:25:34,760 --> 00:25:48,880
 And he got his face turned red and he got very upset.

378
00:25:48,880 --> 00:25:52,680
 Their suffering in this world is quite incredible.

379
00:25:52,680 --> 00:25:56,040
 It's something that we often don't see because our

380
00:25:56,040 --> 00:25:58,080
 situation is so wonderful.

381
00:25:58,080 --> 00:26:05,820
 When we get depressed, when we lose a job or when we fail a

382
00:26:05,820 --> 00:26:10,560
 test, when we lose a girlfriend,

383
00:26:10,560 --> 00:26:13,440
 people will kill themselves when they lose a girlfriend.

384
00:26:13,440 --> 00:26:16,600
 There's people in the world who don't have enough food to

385
00:26:16,600 --> 00:26:17,120
 eat.

386
00:26:17,120 --> 00:26:21,080
 People who see their children taken away from them.

387
00:26:21,080 --> 00:26:26,200
 Children who see their parents killed before their eyes,

388
00:26:26,200 --> 00:26:29,000
 tortured before their eyes.

389
00:26:29,000 --> 00:26:31,120
 What this world needs is love.

390
00:26:31,120 --> 00:26:37,240
 It needs to develop the wisdom which can allow people to

391
00:26:37,240 --> 00:26:41,600
 love one another and express kindness

392
00:26:41,600 --> 00:26:43,240
 towards each other.

393
00:26:43,240 --> 00:26:45,960
 All of us need this love.

394
00:26:45,960 --> 00:26:49,400
 All of us, we need this wisdom.

395
00:26:49,400 --> 00:26:54,640
 So I got a fairly harsh email this morning from someone,

396
00:26:54,640 --> 00:26:59,520
 who's a good friend of mine.

397
00:26:59,520 --> 00:27:03,290
 Maybe not harsh, but hard email telling me I had to stand

398
00:27:03,290 --> 00:27:05,240
 up with my brothers in Burma

399
00:27:05,240 --> 00:27:11,040
 in solidarity.

400
00:27:11,040 --> 00:27:14,880
 And I thought about it and I replied, but after I replied I

401
00:27:14,880 --> 00:27:16,600
 thought more about it and

402
00:27:16,600 --> 00:27:21,660
 I realized, really I am standing up in solidarity with my

403
00:27:21,660 --> 00:27:23,640
 brothers in Burma.

404
00:27:23,640 --> 00:27:27,060
 Because my brothers in Burma are off meditating in the

405
00:27:27,060 --> 00:27:27,800
 forest.

406
00:27:27,800 --> 00:27:35,480
 The young monks in Rangoon are doing something very noble,

407
00:27:35,480 --> 00:27:37,080
 I'm sure.

408
00:27:37,080 --> 00:27:43,880
 Some of them are losing their mindfulness and becoming less

409
00:27:43,880 --> 00:27:45,560
 than noble.

410
00:27:45,560 --> 00:27:47,860
 And I think that's the key, is that we will see in the

411
00:27:47,860 --> 00:27:49,560
 future, in the near future, we

412
00:27:49,560 --> 00:27:52,810
 will see a lot of suffering because people are losing their

413
00:27:52,810 --> 00:27:54,640
 mindfulness, are losing their

414
00:27:54,640 --> 00:28:00,370
 sense of love because they want something which they can't

415
00:28:00,370 --> 00:28:01,160
 get.

416
00:28:01,160 --> 00:28:06,490
 And that goal might be very noble and might be very

417
00:28:06,490 --> 00:28:08,120
 wonderful.

418
00:28:08,120 --> 00:28:13,430
 But the goal is meaningless if your means are heading you

419
00:28:13,430 --> 00:28:15,880
 in another direction.

420
00:28:15,880 --> 00:28:20,090
 The greatest thing about the monks, the Buddhist monk is

421
00:28:20,090 --> 00:28:22,680
 his moral, or Buddhist nun is his

422
00:28:22,680 --> 00:28:28,840
 or her moral superiority.

423
00:28:28,840 --> 00:28:30,040
 We are not in the world.

424
00:28:30,040 --> 00:28:34,000
 We can comment on the world, we can teach the world because

425
00:28:34,000 --> 00:28:36,040
 we are not involved.

426
00:28:36,040 --> 00:28:40,310
 We can talk on Burma because we don't take sides and say, "

427
00:28:40,310 --> 00:28:41,800
The janta is bad.

428
00:28:41,800 --> 00:28:45,040
 People of Burma deserve to be free.

429
00:28:45,040 --> 00:28:49,880
 Democracy is we don't take sides."

430
00:28:49,880 --> 00:28:57,080
 We explain to people, all people, that we are all people.

431
00:28:57,080 --> 00:28:59,400
 People who repress, these are people.

432
00:28:59,400 --> 00:29:02,760
 Maybe they have family, maybe they have good things inside

433
00:29:02,760 --> 00:29:03,400
 them.

434
00:29:03,400 --> 00:29:08,440
 They can be very, very evil people and suddenly they can

435
00:29:08,440 --> 00:29:09,480
 change.

436
00:29:09,480 --> 00:29:14,020
 Maybe in a past life they were repressed, oppressed,

437
00:29:14,020 --> 00:29:16,840
 persecuted, and they got angry about

438
00:29:16,840 --> 00:29:20,120
 it, so now they came back and they want revenge.

439
00:29:20,120 --> 00:29:24,830
 This is ordinary people, we have so much bitterness and

440
00:29:24,830 --> 00:29:27,960
 cruelty inside of us that we have to do

441
00:29:27,960 --> 00:29:28,960
 away with.

442
00:29:28,960 --> 00:29:32,520
 We are all people.

443
00:29:32,520 --> 00:29:36,840
 Whether we are in Thailand or Burma, I know there are a

444
00:29:36,840 --> 00:29:39,640
 great many people who understand

445
00:29:39,640 --> 00:29:41,120
 this.

446
00:29:41,120 --> 00:29:44,690
 Whether they be on the streets chanting, loving kindness,

447
00:29:44,690 --> 00:29:46,640
 wishing for loving kindness, or

448
00:29:46,640 --> 00:29:49,850
 whether they be in a meditation center practicing to

449
00:29:49,850 --> 00:29:52,480
 develop real and true loving kindness in

450
00:29:52,480 --> 00:29:58,560
 their hearts.

451
00:29:58,560 --> 00:30:02,720
 This point is the most important point in regards to the

452
00:30:02,720 --> 00:30:04,880
 problems which exist in this

453
00:30:04,880 --> 00:30:05,880
 world.

454
00:30:05,880 --> 00:30:14,960
 When we can learn to have love and compassion and joy.

455
00:30:14,960 --> 00:30:18,970
 We can have the wisdom which is able to see clearly all

456
00:30:18,970 --> 00:30:21,440
 things inside of ourselves and

457
00:30:21,440 --> 00:30:22,440
 in the world around us.

458
00:30:22,440 --> 00:30:29,520
 This is how we can truly help the world.

459
00:30:29,520 --> 00:30:34,150
 For this reason we come to, for this and many reasons, we

460
00:30:34,150 --> 00:30:36,680
 come to practice, we pass into

461
00:30:36,680 --> 00:30:37,680
 meditation.

462
00:30:37,680 --> 00:30:44,420
 Something which helps our own heart, builds patience and

463
00:30:44,420 --> 00:30:48,200
 forbearance, removes lust and

464
00:30:48,200 --> 00:30:55,530
 greed and addiction and hatred, delusion and conceit and so

465
00:30:55,530 --> 00:30:59,120
 many bad things, stinginess,

466
00:30:59,120 --> 00:31:04,080
 jealousy which exist in our hearts.

467
00:31:04,080 --> 00:31:09,360
 But it also helps the world, gives us the tools to support

468
00:31:09,360 --> 00:31:11,720
 other people and be a real

469
00:31:11,720 --> 00:31:15,360
 guide for other people.

470
00:31:15,360 --> 00:31:17,680
 Something which has done immeasurable benefit to the world

471
00:31:17,680 --> 00:31:19,120
 in many ways that the world doesn't

472
00:31:19,120 --> 00:31:22,800
 even understand and may never know.

473
00:31:22,800 --> 00:31:29,060
 But the teachings of the Lord Buddha are timeless and they

474
00:31:29,060 --> 00:31:32,680
 are very present here and now.

475
00:31:32,680 --> 00:31:37,410
 It's only a matter of whether people practice them or would

476
00:31:37,410 --> 00:31:39,880
 rather go out and shout and yell

477
00:31:39,880 --> 00:31:55,520
 and scream and fight and build, do protests and all around

478
00:31:55,520 --> 00:31:56,720
 the world people are looking

479
00:31:56,720 --> 00:32:00,920
 for solutions to the problems, problems.

480
00:32:00,920 --> 00:32:03,790
 Whenever a problem comes up they look for a solution but

481
00:32:03,790 --> 00:32:05,240
 they never look at the biggest

482
00:32:05,240 --> 00:32:06,240
 problem.

483
00:32:06,240 --> 00:32:10,380
 The problem is inside of our own hearts and it's always

484
00:32:10,380 --> 00:32:11,760
 been here.

485
00:32:11,760 --> 00:32:16,380
 The situation in Burma is not something that came up two

486
00:32:16,380 --> 00:32:17,560
 weeks ago.

487
00:32:17,560 --> 00:32:20,400
 Situation in this world is something which we have all had

488
00:32:20,400 --> 00:32:21,640
 a part in and we continue

489
00:32:21,640 --> 00:32:26,900
 to have a part in for as long as we have greed, anger and

490
00:32:26,900 --> 00:32:29,640
 delusion in our hearts.

491
00:32:29,640 --> 00:32:33,250
 I'd like for all of us if we could to take this opportunity

492
00:32:33,250 --> 00:32:35,320
 today to practice especially

493
00:32:35,320 --> 00:32:41,960
 thinking of the suffering of the people in the world.

494
00:32:41,960 --> 00:32:49,750
 It can be the especially thinking about the people in Burma

495
00:32:49,750 --> 00:32:53,000
 but to take time to take a

496
00:32:53,000 --> 00:32:57,480
 little bit of time out of our practice to send our love to

497
00:32:57,480 --> 00:32:59,880
 all beings, extend our love

498
00:32:59,880 --> 00:33:05,740
 immeasurably to all beings, wishing that all beings might

499
00:33:05,740 --> 00:33:08,480
 be free from suffering.

500
00:33:08,480 --> 00:33:12,440
 This can be a support for our own practice and it will be a

501
00:33:12,440 --> 00:33:13,640
 support for the world and

502
00:33:13,640 --> 00:33:18,630
 an encouragement for the world to practice and develop

503
00:33:18,630 --> 00:33:22,200
 themselves in the right direction.

504
00:33:22,200 --> 00:33:27,060
 This is my talk giving a little bit of understanding and

505
00:33:27,060 --> 00:33:29,800
 direction for us in regards to love and

506
00:33:29,800 --> 00:33:34,410
 what sort of love is beneficial and what sort of love is

507
00:33:34,410 --> 00:33:36,800
 perhaps unbeneficial.

508
00:33:36,800 --> 00:33:39,320
 So it's all for today.

509
00:33:39,320 --> 00:33:42,200
 Thank you.

