1
00:00:00,000 --> 00:00:04,810
 Hello welcome back to our study of the Dhamtada. Today we

2
00:00:04,810 --> 00:00:06,160
 continue on with verse

3
00:00:06,160 --> 00:00:15,070
 number five which goes, "Nahi we reina we rani sammanti dha

4
00:00:15,070 --> 00:00:17,920
 kudachanam, awe reina

5
00:00:17,920 --> 00:00:26,520
 chalsamanti, ezadam o sanantanam." Which means not indeed

6
00:00:26,520 --> 00:00:29,200
 through enmity, his enmity

7
00:00:29,200 --> 00:00:35,610
 appeased, not here or anywhere. "Awe reina chalsamanti." It

8
00:00:35,610 --> 00:00:36,960
 is appeased through

9
00:00:36,960 --> 00:00:45,040
 non enmity. This is an eternal truth. This teaching was

10
00:00:45,040 --> 00:00:46,160
 given in regards to a

11
00:00:46,160 --> 00:00:50,480
 famous story, another famous one, about two women who got

12
00:00:50,480 --> 00:00:52,200
 very much caught up in

13
00:00:52,200 --> 00:00:56,920
 the cycle of enmity or revenge and would never have gotten

14
00:00:56,920 --> 00:00:58,040
 out if it weren't for

15
00:00:58,040 --> 00:01:02,380
 the teaching of the Buddha. This is our understanding.

16
00:01:02,380 --> 00:01:03,800
 These two women were

17
00:01:03,800 --> 00:01:07,560
 married to the same man and one of them was barren, the

18
00:01:07,560 --> 00:01:09,080
 other one was fertile and

19
00:01:09,080 --> 00:01:12,740
 the fertile woman got pregnant three times and each time

20
00:01:12,740 --> 00:01:13,920
 the barren woman

21
00:01:13,920 --> 00:01:21,060
 being afraid of the being left out or being cast aside

22
00:01:21,060 --> 00:01:23,000
 caused an abortion in

23
00:01:23,000 --> 00:01:28,760
 the fertile woman, slipping some kind of medicine into her

24
00:01:28,760 --> 00:01:30,480
 food. And the third

25
00:01:30,480 --> 00:01:34,240
 time the woman actually died as a result of the abortion

26
00:01:34,240 --> 00:01:35,600
 and while she

27
00:01:35,600 --> 00:01:41,960
 was dying she cursed her murderer saying, "Vowing revenge,

28
00:01:41,960 --> 00:01:43,660
 vowing to come back

29
00:01:43,660 --> 00:01:56,620
 someday and pay the pay revenge for this evil deed." And

30
00:01:56,620 --> 00:01:58,400
 she died. And the

31
00:01:58,400 --> 00:02:03,190
 woman who killed her or who caused the abortion was beaten

32
00:02:03,190 --> 00:02:05,200
 by her husband for

33
00:02:05,200 --> 00:02:10,020
 being such an evil person and so on and her husband was a

34
00:02:10,020 --> 00:02:10,440
 fairly rough

35
00:02:10,440 --> 00:02:15,910
 person I guess and died as a result as well. She got sick,

36
00:02:15,910 --> 00:02:17,160
 was beaten to the

37
00:02:17,160 --> 00:02:20,990
 point where she got so sick she died and was born in the

38
00:02:20,990 --> 00:02:23,320
 same house as a hen, a

39
00:02:23,320 --> 00:02:30,360
 female chicken. And so happened that the woman who died

40
00:02:30,360 --> 00:02:31,200
 from the abortion

41
00:02:31,200 --> 00:02:34,460
 was born in the same house as a cat. So you can see where

42
00:02:34,460 --> 00:02:36,800
 this is going. The hen

43
00:02:36,800 --> 00:02:41,460
 laid eggs and the chicks hatched and the cat came along and

44
00:02:41,460 --> 00:02:43,560
 ate the chicks three

45
00:02:43,560 --> 00:02:47,620
 times and the third time actually ate the chicken as well

46
00:02:47,620 --> 00:02:48,440
 or killed the

47
00:02:48,440 --> 00:02:53,180
 chicken as well. The chicken died and was born as a leopard

48
00:02:53,180 --> 00:02:55,760
. The cat died and was

49
00:02:55,760 --> 00:03:04,400
 born as a deer and the leopard came and they were well

50
00:03:04,400 --> 00:03:05,120
 familiar with each

51
00:03:05,120 --> 00:03:10,480
 other at this point. The leopard came and ate the deer's f

52
00:03:10,480 --> 00:03:12,160
awns and the

53
00:03:12,160 --> 00:03:19,200
 third time he or killed the the doe as well. The doe came

54
00:03:19,200 --> 00:03:20,160
 back each time vowing

55
00:03:20,160 --> 00:03:23,210
 revenge. The person who was killed would each time keep it

56
00:03:23,210 --> 00:03:23,720
 in their mind.

57
00:03:23,720 --> 00:03:27,780
 When the hen was dying she said, "You've done this to me

58
00:03:27,780 --> 00:03:30,040
 three times now. One day

59
00:03:30,040 --> 00:03:33,250
 I'm going to come back." And indeed she came back and then

60
00:03:33,250 --> 00:03:34,320
 the deer are the same

61
00:03:34,320 --> 00:03:40,930
 and the deer came back as an ogre. This is the woman who

62
00:03:40,930 --> 00:03:41,360
 had died from

63
00:03:41,360 --> 00:03:44,480
 the abortion and made a vow to come back to eat this woman

64
00:03:44,480 --> 00:03:45,480
's children.

65
00:03:45,480 --> 00:03:52,870
 Dye then was born as an ogre and the leopard was born as a

66
00:03:52,870 --> 00:03:55,080
 woman, an ordinary

67
00:03:55,080 --> 00:04:00,240
 woman living near I believe in Sawati, the great city in

68
00:04:00,240 --> 00:04:00,680
 the time of the

69
00:04:00,680 --> 00:04:07,640
 Buddha. Twice this ogre, an ogre is like an evil spirit

70
00:04:07,640 --> 00:04:10,760
 that eats your children.

71
00:04:10,760 --> 00:04:14,520
 Again this part of the story I don't vouch for the idea

72
00:04:14,520 --> 00:04:15,240
 that there could

73
00:04:15,240 --> 00:04:22,060
 be children eating ogres 2,500 years ago seems to me a

74
00:04:22,060 --> 00:04:22,400
 little bit

75
00:04:22,400 --> 00:04:26,990
 far-fetched. It may just have been a woman who was a

76
00:04:26,990 --> 00:04:27,680
 cannibal or

77
00:04:27,680 --> 00:04:31,360
 something. It may have just been whatever it was because

78
00:04:31,360 --> 00:04:31,800
 these things

79
00:04:31,800 --> 00:04:36,400
 do happen. Even in our life there are people who eat other

80
00:04:36,400 --> 00:04:36,920
 people who

81
00:04:36,920 --> 00:04:38,890
 kill and murder them and so on. There are terrible things

82
00:04:38,890 --> 00:04:39,680
 that happen in this

83
00:04:39,680 --> 00:04:44,000
 world. So I don't disbelieve that. This is just an

84
00:04:44,000 --> 00:04:44,760
 extraordinary

85
00:04:44,760 --> 00:04:49,400
 example. But that it was an ogre, well okay, it's not so

86
00:04:49,400 --> 00:04:50,520
 important because it

87
00:04:50,520 --> 00:04:56,480
 doesn't affect the moral of the story at all really. But

88
00:04:56,480 --> 00:04:57,040
 let's go

89
00:04:57,040 --> 00:05:01,040
 along with it. It was an ogre and ate this woman's children

90
00:05:01,040 --> 00:05:01,880
 twice and the third

91
00:05:01,880 --> 00:05:07,380
 time the woman saw her coming and ran. How it happened was

92
00:05:07,380 --> 00:05:08,440
 the third time the

93
00:05:08,440 --> 00:05:10,540
 woman left with her husband and said we've got to get away

94
00:05:10,540 --> 00:05:11,360
 from this. This is

95
00:05:11,360 --> 00:05:16,020
 like every lifetime we're in the cycle of revenge and so

96
00:05:16,020 --> 00:05:16,640
 she ran away.

97
00:05:16,640 --> 00:05:25,360
 The ogre chased after her and found her near Jethuana,

98
00:05:25,360 --> 00:05:31,680
 which is the monastery of the Buddha near Sabatthi. She and

99
00:05:31,680 --> 00:05:31,760
 her

100
00:05:31,760 --> 00:05:35,050
 husband were bathing in the pool outside and her husband

101
00:05:35,050 --> 00:05:35,920
 went to bathe and she

102
00:05:35,920 --> 00:05:39,120
 was holding her child and then she turned and saw there was

103
00:05:39,120 --> 00:05:40,000
 the ogre coming.

104
00:05:40,000 --> 00:05:43,180
 Right away they recognized each other and she knew right

105
00:05:43,180 --> 00:05:44,040
 away who this was.

106
00:05:44,040 --> 00:05:48,840
 It was like they had this bond. The woman ran into Jethuana

107
00:05:48,840 --> 00:05:50,680
 and the Buddha at

108
00:05:50,680 --> 00:05:55,700
 that moment was giving a talk to a large group of people,

109
00:05:55,700 --> 00:05:55,800
 his

110
00:05:55,800 --> 00:06:00,000
 followers. She ran right up to the Buddha and dropped her

111
00:06:00,000 --> 00:06:00,280
 son at the

112
00:06:00,280 --> 00:06:03,670
 Buddha's feet and said here's my child please protect him,

113
00:06:03,670 --> 00:06:07,000
 save his life.

114
00:06:07,000 --> 00:06:13,120
 Fortunately the angels that guarded Jethuana at the time

115
00:06:13,120 --> 00:06:14,520
 didn't let the

116
00:06:14,520 --> 00:06:19,230
 ogre in. They have power over it and spirits go and go like

117
00:06:19,230 --> 00:06:19,840
 this. The idea

118
00:06:19,840 --> 00:06:22,930
 that there might be evil spirits doesn't really faze me.

119
00:06:22,930 --> 00:06:23,840
 The idea that they might

120
00:06:23,840 --> 00:06:27,520
 eat your children is a little bit far-fetched.

121
00:06:27,520 --> 00:06:34,080
 There was a contact and this evil spirit wasn't allowed in.

122
00:06:34,080 --> 00:06:38,920
 Until the Buddha called, told Ananda, his attendant, to go

123
00:06:38,920 --> 00:06:39,520
 and bring in the

124
00:06:39,520 --> 00:06:45,430
 ogre to invite her in. The Buddha said what are you doing?

125
00:06:45,430 --> 00:06:46,080
 He said how can

126
00:06:46,080 --> 00:06:50,320
 you not realize where this is headed? Three lifetimes you

127
00:06:50,320 --> 00:06:50,640
've been,

128
00:06:50,640 --> 00:06:55,510
 you haven't been the aggressor every time. You can't hope

129
00:06:55,510 --> 00:06:57,400
 to possibly win

130
00:06:57,400 --> 00:07:01,070
 every time and you're creating a cycle of revenge. He said

131
00:07:01,070 --> 00:07:02,040
 if you didn't come

132
00:07:02,040 --> 00:07:05,720
 and find me, if you didn't have someone here to explain to

133
00:07:05,720 --> 00:07:06,480
 you the

134
00:07:06,480 --> 00:07:10,150
 problem inherent in this sort of behavior, you would have

135
00:07:10,150 --> 00:07:11,040
 continued on like this

136
00:07:11,040 --> 00:07:15,760
 forever. And then he said his verse, this verse that goes

137
00:07:15,760 --> 00:07:17,640
 not by enmity, is enmity

138
00:07:17,640 --> 00:07:23,830
 appeased, not here nor anywhere, but only by non enmity, is

139
00:07:23,830 --> 00:07:25,200
 enmity appeased. This

140
00:07:25,200 --> 00:07:29,500
 is an eternal truth. And just by waking them up and

141
00:07:29,500 --> 00:07:31,000
 explaining it to them in

142
00:07:31,000 --> 00:07:34,880
 this way, he was able to help them to overcome it and

143
00:07:34,880 --> 00:07:36,440
 actually the ogre was

144
00:07:36,440 --> 00:07:42,460
 able to let go. Now why I think this is a good lesson, this

145
00:07:42,460 --> 00:07:43,400
 has a good lesson for

146
00:07:43,400 --> 00:07:49,390
 us, because it's part of this helping us to open up and see

147
00:07:49,390 --> 00:07:51,280
 the bigger picture,

148
00:07:51,280 --> 00:07:54,800
 and see where we're leading ourselves and where our minds

149
00:07:54,800 --> 00:07:56,480
 are taking us. That

150
00:07:56,480 --> 00:08:01,310
 every act that we do does have an effect on our minds and

151
00:08:01,310 --> 00:08:03,320
 or can potentially

152
00:08:03,320 --> 00:08:05,960
 have an effect on our mind if we cling to it. There's

153
00:08:05,960 --> 00:08:07,280
 obviously some incredible

154
00:08:07,280 --> 00:08:10,560
 clinging going on here, this incredible clinging to revenge

155
00:08:10,560 --> 00:08:12,040
. This person heard me

156
00:08:12,040 --> 00:08:15,560
 as I said in the other verses we have here, a kochi manga,

157
00:08:15,560 --> 00:08:15,560
 wadhi manga,

158
00:08:15,560 --> 00:08:19,350
 dinhi manga, hazi me, he hit me, he beat me, he hurt me and

159
00:08:19,350 --> 00:08:21,600
 so on. When we think like

160
00:08:21,600 --> 00:08:26,500
 that, it builds up in our mind and in this story we see an

161
00:08:26,500 --> 00:08:27,640
 example of

162
00:08:27,640 --> 00:08:31,460
 the result and the consequences. It's not only in this life

163
00:08:31,460 --> 00:08:33,080
. And this is

164
00:08:33,080 --> 00:08:39,560
 something that we obviously lose sight of for the most part

165
00:08:39,560 --> 00:08:40,160
 in this world,

166
00:08:40,160 --> 00:08:44,180
 because we think of things only in terms of this one life

167
00:08:44,180 --> 00:08:45,560
 and so we think of in

168
00:08:45,560 --> 00:08:49,400
 terms of getting ahead. We think of getting the better. We

169
00:08:49,400 --> 00:08:49,880
 talk about the

170
00:08:49,880 --> 00:08:54,560
 doggy dog world and how if you can get the better of other

171
00:08:54,560 --> 00:08:55,920
 people and come out

172
00:08:55,920 --> 00:08:59,330
 on top and so on. So that by the time you're in your middle

173
00:08:59,330 --> 00:09:00,500
 age you've got

174
00:09:00,500 --> 00:09:02,820
 money and you've got power and you've got influence and you

175
00:09:02,820 --> 00:09:03,800
've got stability,

176
00:09:03,800 --> 00:09:08,480
 that somehow you've achieved something. You've won in that

177
00:09:08,480 --> 00:09:09,240
 sense. If you can do

178
00:09:09,240 --> 00:09:12,690
 it morally and great if you can't, it's not really

179
00:09:12,690 --> 00:09:15,360
 important because in the end

180
00:09:15,360 --> 00:09:19,770
 you're going to die anyway. So better to get the better of

181
00:09:19,770 --> 00:09:21,200
 other people. And I

182
00:09:21,200 --> 00:09:25,650
 think most of us in this world or the majority of people in

183
00:09:25,650 --> 00:09:26,960
 this world do

184
00:09:26,960 --> 00:09:31,680
 partake of this to some extent. We don't mind so much

185
00:09:31,680 --> 00:09:32,840
 getting the better of

186
00:09:32,840 --> 00:09:36,400
 other people. In business we don't mind cheating other

187
00:09:36,400 --> 00:09:38,520
 people, we don't mind

188
00:09:38,520 --> 00:09:41,750
 conning other people and so on. We don't mind trying to get

189
00:09:41,750 --> 00:09:42,960
 the better of people

190
00:09:42,960 --> 00:09:45,870
 and getting their money and so on. Just so long as we can

191
00:09:45,870 --> 00:09:46,960
 somehow come out on

192
00:09:46,960 --> 00:09:51,920
 top or we can get something good for ourselves at the time.

193
00:09:51,920 --> 00:09:53,400
 And I think this

194
00:09:53,400 --> 00:09:56,720
 is, here we have a really good example of these two women

195
00:09:56,720 --> 00:09:59,900
 trying to secure a

196
00:09:59,900 --> 00:10:03,200
 happiness for themselves against their rival, this woman

197
00:10:03,200 --> 00:10:04,360
 who was jealous of the

198
00:10:04,360 --> 00:10:13,690
 woman who was fertile. But the point we have in common with

199
00:10:13,690 --> 00:10:14,120
 all of our

200
00:10:14,120 --> 00:10:20,070
 feuds is that it carries on. And it's really quite easy to

201
00:10:20,070 --> 00:10:20,960
 see and this is why

202
00:10:20,960 --> 00:10:24,930
 it's really quite easy to understand. It's funny to see so

203
00:10:24,930 --> 00:10:25,840
 many people having

204
00:10:25,840 --> 00:10:28,490
 trouble with the idea that the mind might continue on. We

205
00:10:28,490 --> 00:10:29,560
're building up an

206
00:10:29,560 --> 00:10:37,040
 incredible power or an incredible energy through our deeds.

207
00:10:37,040 --> 00:10:38,000
 People who cling,

208
00:10:38,000 --> 00:10:41,200
 people who build up this cycle of revenge, who have quarrel

209
00:10:41,200 --> 00:10:41,880
s with other

210
00:10:41,880 --> 00:10:47,630
 people, who fight and dispute and hurt and harm others and

211
00:10:47,630 --> 00:10:48,840
 fight and

212
00:10:48,840 --> 00:10:51,730
 are victorious even over other people. They build up this

213
00:10:51,730 --> 00:10:52,840
 incredible energy in

214
00:10:52,840 --> 00:10:56,770
 their mind that drags them down and actually puts them in

215
00:10:56,770 --> 00:10:57,840
 the place of the

216
00:10:57,840 --> 00:11:00,800
 victim in the future as a result of the defilement of the

217
00:11:00,800 --> 00:11:01,680
 mind. Because

218
00:11:01,680 --> 00:11:05,820
 defilement is, as I've said it, the problem with it is that

219
00:11:05,820 --> 00:11:06,600
 it drags your

220
00:11:06,600 --> 00:11:12,300
 mind down. It defiles and it soils, it sullies the mind. It

221
00:11:12,300 --> 00:11:13,960
 hurts you, it

222
00:11:13,960 --> 00:11:18,040
 takes away your ability to function, it cripples you. So

223
00:11:18,040 --> 00:11:19,480
 the reason why we're in

224
00:11:19,480 --> 00:11:24,180
 these unpleasant states is because of our crippled mind in

225
00:11:24,180 --> 00:11:25,280
 the past. When a

226
00:11:25,280 --> 00:11:28,640
 person comes out on top they've developed all this clinging

227
00:11:28,640 --> 00:11:28,880
 and all

228
00:11:28,880 --> 00:11:32,930
 this evil in their mind and even though they might have

229
00:11:32,930 --> 00:11:33,960
 great luxury

230
00:11:33,960 --> 00:11:38,000
 and power externally, it drags them down and if they don't

231
00:11:38,000 --> 00:11:39,240
 use their power in a

232
00:11:39,240 --> 00:11:44,840
 good way it will drag them down and the cycle of revenge

233
00:11:44,840 --> 00:11:45,840
 will continue.

234
00:11:45,840 --> 00:11:52,420
 Our inability to see beyond one life leads us to, this is

235
00:11:52,420 --> 00:11:53,280
 something I've said,

236
00:11:53,280 --> 00:11:58,350
 it doesn't really matter whether the mind does continue on.

237
00:11:58,350 --> 00:11:59,000
 You can see the

238
00:11:59,000 --> 00:12:03,590
 difference in our beliefs. If you believe that at death

239
00:12:03,590 --> 00:12:05,400
 something stops and

240
00:12:05,400 --> 00:12:07,800
 that's it and you don't have to go on then it really doesn

241
00:12:07,800 --> 00:12:08,680
't matter what you

242
00:12:08,680 --> 00:12:13,110
 do and we can see how that's the effect that's happening on

243
00:12:13,110 --> 00:12:13,640
 the world that

244
00:12:13,640 --> 00:12:18,260
 we're, it's deteriorating and we're depleting our resources

245
00:12:18,260 --> 00:12:19,080
. You know the

246
00:12:19,080 --> 00:12:21,580
 depletion of resources, well who cares? I mean when I die

247
00:12:21,580 --> 00:12:23,240
 that's it, right? Well

248
00:12:23,240 --> 00:12:28,320
 wrong really. I mean the effect of your mind states and the

249
00:12:28,320 --> 00:12:28,960
 greed that's in

250
00:12:28,960 --> 00:12:33,220
 your mind alone is something that'll last beyond because of

251
00:12:33,220 --> 00:12:34,360
 our attachment and

252
00:12:34,360 --> 00:12:39,480
 our clinging. On the other hand if you understand life

253
00:12:39,480 --> 00:12:40,480
 after, if you understand

254
00:12:40,480 --> 00:12:44,100
 or let's say if you believe, if we think of it parallel in

255
00:12:44,100 --> 00:12:44,920
 terms of belief

256
00:12:44,920 --> 00:12:51,400
 that the mind doesn't stop and this experience continues on

257
00:12:51,400 --> 00:12:53,280
 and works

258
00:12:53,280 --> 00:12:56,780
 itself out so the energy that we build up through our

259
00:12:56,780 --> 00:12:58,400
 actions has eventually to

260
00:12:58,400 --> 00:13:02,980
 bring consequences, then how do you really think, do you

261
00:13:02,980 --> 00:13:04,080
 think it's possible you

262
00:13:04,080 --> 00:13:07,800
 would go to war? Do you think it's possible you would cling

263
00:13:07,800 --> 00:13:08,560
 to things or

264
00:13:08,560 --> 00:13:12,790
 if you really believe this would you ever have any reason

265
00:13:12,790 --> 00:13:15,080
 to build up and to try

266
00:13:15,080 --> 00:13:20,170
 to find pleasure in the year and now to try to you know to

267
00:13:20,170 --> 00:13:21,280
 become addicted to

268
00:13:21,280 --> 00:13:29,590
 things and to reach to crave for things or to run after ep

269
00:13:29,590 --> 00:13:30,680
hemeral pleasures?

270
00:13:30,680 --> 00:13:34,800
 You'd have no reason to. If you understand this you really,

271
00:13:34,800 --> 00:13:34,960
 when you

272
00:13:34,960 --> 00:13:37,130
 look, when you sit back and think about it you think you

273
00:13:37,130 --> 00:13:38,120
 know this is really

274
00:13:38,120 --> 00:13:41,830
 whatever I get in this world, whatever I achieve, whatever

275
00:13:41,830 --> 00:13:43,440
 I accomplish, if I

276
00:13:43,440 --> 00:13:47,620
 become rich, if I become famous, it's meaningless and in

277
00:13:47,620 --> 00:13:50,080
 the end it's quite

278
00:13:50,080 --> 00:13:58,600
 quite inconsequential because it lasts for a moment and

279
00:13:58,600 --> 00:14:00,160
 then it's gone and

280
00:14:00,160 --> 00:14:04,700
 this is the idea that it will be gone, that everything does

281
00:14:04,700 --> 00:14:08,040
 cease and the evil

282
00:14:08,040 --> 00:14:13,690
 that we do now will drag us down and bring evil unto us. So

283
00:14:13,690 --> 00:14:14,960
 this is

284
00:14:14,960 --> 00:14:20,020
 something that helps us to see the true nature of reality

285
00:14:20,020 --> 00:14:21,160
 and it answers a lot

286
00:14:21,160 --> 00:14:24,660
 of questions in regards to why people are victims, why

287
00:14:24,660 --> 00:14:26,320
 people suffer, why people

288
00:14:26,320 --> 00:14:32,110
 are in a place, in a position of great inequality where

289
00:14:32,110 --> 00:14:33,760
 some people are very

290
00:14:33,760 --> 00:14:37,720
 rich, some people are very poor and so on. We can see that

291
00:14:37,720 --> 00:14:38,920
 there are these waves

292
00:14:38,920 --> 00:14:43,330
 and the person who is rich now may be poor in the next life

293
00:14:43,330 --> 00:14:44,540
 depending on the

294
00:14:44,540 --> 00:14:46,760
 state of their mind really. If a rich person is very

295
00:14:46,760 --> 00:14:47,960
 generous then there's no

296
00:14:47,960 --> 00:14:51,440
 reason, if they use their power and their influence in the

297
00:14:51,440 --> 00:14:51,600
 right

298
00:14:51,600 --> 00:14:54,480
 way then you can see that this is something that is going

299
00:14:54,480 --> 00:14:55,240
 to be to last

300
00:14:55,240 --> 00:14:58,390
 because their mind is powerful, their mind is strong, they

301
00:14:58,390 --> 00:14:59,000
're sure of

302
00:14:59,000 --> 00:15:03,320
 themselves and they have this great charge of good energy

303
00:15:03,320 --> 00:15:04,360
 which will come

304
00:15:04,360 --> 00:15:08,200
 back to them. Obviously it's something that you can feel. A

305
00:15:08,200 --> 00:15:08,840
 person who does

306
00:15:08,840 --> 00:15:12,850
 good deeds is able to hold their head up, they're able to

307
00:15:12,850 --> 00:15:14,120
 sit up tall and they

308
00:15:14,120 --> 00:15:21,680
 don't shrink down, there's none of this clinging that

309
00:15:21,680 --> 00:15:26,310
 drags them down and actually drags them to hell. Something

310
00:15:26,310 --> 00:15:27,720
 that lifts them up and

311
00:15:27,720 --> 00:15:32,770
 makes them feel light and makes them feel free from guilt

312
00:15:32,770 --> 00:15:34,480
 and so on. So this

313
00:15:34,480 --> 00:15:39,780
 helps us, this lesson is in regards to helping us to give

314
00:15:39,780 --> 00:15:40,760
 up our quarrels

315
00:15:40,760 --> 00:15:45,700
 because any kind of victory that we could gain is ephemeral

316
00:15:45,700 --> 00:15:47,200
, is temporary and

317
00:15:47,200 --> 00:15:52,660
 it only encourages more and more quarreling, more and more

318
00:15:52,660 --> 00:15:53,640
 enmity as the

319
00:15:53,640 --> 00:15:57,840
 person who's defeated. Unless they can somehow give up,

320
00:15:57,840 --> 00:15:59,660
 unless one of us can in

321
00:15:59,660 --> 00:16:03,480
 the end let go then they will surely seek revenge.

322
00:16:03,480 --> 00:16:04,720
 Regardless of whether they

323
00:16:04,720 --> 00:16:07,830
 speak revenge, the evil that we have done will drag us down

324
00:16:07,830 --> 00:16:09,720
 and drag us down to a

325
00:16:09,720 --> 00:16:15,960
 state of suffering and stress and so on. So that's the

326
00:16:15,960 --> 00:16:16,920
 lesson here, this is

327
00:16:16,920 --> 00:16:21,760
 verse number five. Thanks for tuning in and all the best.

