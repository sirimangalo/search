 Sometimes when I feel sleepy in the morning I do walking
 meditation before sitting.
 Do you recommend it for every session? How can I understand
 the best moment to attain, to shift from walking to sitting
?
 Yes, we recommend walking before every round of sitting, a
 formal meditation.
 Now in daily life it's sometimes inconvenient and even
 undesirable to do walking before each round.
 Potentially because you've been walking during the day, you
've been physically active or you're drained at the end of
 the day so often it can be often the case that you'd rather
 just do sitting.
 On the other hand you'll find that sometimes walking is
 more helpful for you. After a long day of stress it's
 sometimes difficult to sit down and you'll find that
 walking allows you to calm down, allows you to focus
 yourself, center yourself before you have to sit.
 And it sets you up for good meditation.
 Our rule of thumb is to do half walking and half sitting
 except for, in the case for an advanced meditator who is
 deeply involved in meditation.
 Eventually, and this is for someone who is practicing
 intensively, it means all day, potentially all night, they
 can come to a point where they decide for themselves to do
 walking as long as they wish and then sitting.
 This is in our tradition. In our tradition we almost always
 prefer to do half walking, half sitting.
 One good reason because otherwise you just follow your
 partiality. You're partial to one and so you do it and in
 fact that is the wrong reason.
 That's the reason you should switch to be able to break and
 let go of the partiality.
 Wanting to sit so you walk, wanting to walk and so you sit.
 And so the half and half thing, equalizing, doing them
 equal amounts helps to break that attachment.
