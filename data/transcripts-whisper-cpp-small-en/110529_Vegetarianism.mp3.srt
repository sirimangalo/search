1
00:00:00,000 --> 00:00:05,120
 Hello, welcome back to Ask a Monk. This is a response in

2
00:00:05,120 --> 00:00:07,000
 regards to the question as to

3
00:00:07,000 --> 00:00:12,730
 whether or not Buddhists must be vegetarian, or what are my

4
00:00:12,730 --> 00:00:15,760
 thoughts on vegetarianism.

5
00:00:15,760 --> 00:00:19,850
 And this comes at an interesting time. Just as I was

6
00:00:19,850 --> 00:00:24,160
 intending to reply to this question,

7
00:00:24,160 --> 00:00:30,730
 it's now come up in the news that the founder of Facebook,

8
00:00:30,730 --> 00:00:35,160
 Mark Zuckerberg, has adopted

9
00:00:35,160 --> 00:00:42,950
 the philosophy that we're often confronted with, that if

10
00:00:42,950 --> 00:00:46,520
 one is going to consume the

11
00:00:46,520 --> 00:00:52,480
 flesh of other animals, then one should undertake to take

12
00:00:52,480 --> 00:00:56,320
 the life of those animals oneself.

13
00:00:56,320 --> 00:01:01,270
 So one should be the killer rather than, as the theory goes

14
00:01:01,270 --> 00:01:03,720
, allowing someone else to

15
00:01:03,720 --> 00:01:18,430
 kill for you. So this makes this question quite topical or

16
00:01:18,430 --> 00:01:23,040
 quite timely. So just to

17
00:01:23,040 --> 00:01:28,660
 give my thoughts on vegetarianism, it's not a simple

18
00:01:28,660 --> 00:01:30,640
 discussion. And it's not, this isn't

19
00:01:30,640 --> 00:01:36,140
 a question in fact that should be taken lightly.

20
00:01:36,140 --> 00:01:40,880
 Surprisingly, it might seem like it's not

21
00:01:40,880 --> 00:01:48,150
 a terribly important topic, but it involves the very core

22
00:01:48,150 --> 00:01:52,800
 issue of Buddhist ethics. And

23
00:01:52,800 --> 00:01:57,310
 an understanding of, it requires, an answer to this

24
00:01:57,310 --> 00:02:00,480
 question requires an understanding

25
00:02:00,480 --> 00:02:07,720
 of what exactly Buddhist ethics are. And the sort of

26
00:02:07,720 --> 00:02:11,040
 philosophy that the Buddha had. So

27
00:02:11,040 --> 00:02:14,110
 first of all, I want to say that this is probably one of

28
00:02:14,110 --> 00:02:16,960
 those questions that's going to get

29
00:02:16,960 --> 00:02:21,480
 negative responses from both sides, from the people who

30
00:02:21,480 --> 00:02:24,640
 believe Buddhists should be vegetarian,

31
00:02:24,640 --> 00:02:28,370
 and from those who believe, not Buddhists, but those who

32
00:02:28,370 --> 00:02:30,200
 sort of believe that people

33
00:02:30,200 --> 00:02:39,910
 like Mark Zuckerberg are correct, that it's better to kill,

34
00:02:39,910 --> 00:02:44,120
 better to be the person who

35
00:02:44,120 --> 00:02:49,080
 does the killing rather than be hypocritical and eat meat.

36
00:02:49,080 --> 00:02:51,640
 Eat meat, but don't agree with

37
00:02:51,640 --> 00:02:57,780
 killing. If your stance is that eating meat is okay, then

38
00:02:57,780 --> 00:03:00,800
 you are not therefore able to

39
00:03:00,800 --> 00:03:04,450
 hold the stance that killing is not okay, and therefore you

40
00:03:04,450 --> 00:03:06,160
 should actually be better

41
00:03:06,160 --> 00:03:14,210
 off to kill for yourself rather than happily be careful. So

42
00:03:14,210 --> 00:03:14,520
 I don't know what to say that

43
00:03:14,520 --> 00:03:19,110
 except that, to be clear that I'm aware that this isn't

44
00:03:19,110 --> 00:03:21,440
 going to be universally held, and

45
00:03:21,440 --> 00:03:25,180
 I don't want people to think that my view is the view held

46
00:03:25,180 --> 00:03:27,000
 by all Buddhists or by all

47
00:03:27,000 --> 00:03:37,040
 people who follow the Buddhist teaching or who consider

48
00:03:37,040 --> 00:03:38,120
 themselves to be Buddhists, let's

49
00:03:38,120 --> 00:03:44,880
 put it that way. So we have to talk about two things here.

50
00:03:44,880 --> 00:03:47,400
 The first one is an understanding

51
00:03:47,400 --> 00:03:56,890
 of Buddhist ethics. Buddhist ethics are based on ultimate

52
00:03:56,890 --> 00:03:59,880
 reality, and this shouldn't be

53
00:03:59,880 --> 00:04:03,390
 a scary term if you've been following the meditation

54
00:04:03,390 --> 00:04:07,520
 teachings that I've teach or that

55
00:04:07,520 --> 00:04:14,240
 are presented by the Buddha himself. What it means simply

56
00:04:14,240 --> 00:04:17,440
 is the experience of the reality

57
00:04:17,440 --> 00:04:22,000
 around us in the present moment. So Buddhist ethics are not

58
00:04:22,000 --> 00:04:23,960
 intellectual, they are not

59
00:04:23,960 --> 00:04:29,100
 passed down by some higher authority, they're scientific.

60
00:04:29,100 --> 00:04:32,280
 Something is unethical, not because

61
00:04:32,280 --> 00:04:36,650
 of specifically the suffering that it brings to the victim

62
00:04:36,650 --> 00:04:38,520
 in the case where there is a

63
00:04:38,520 --> 00:04:45,520
 victim or to other people. They're not unethical because

64
00:04:45,520 --> 00:04:50,800
 they cause suffering. They're unethical

65
00:04:50,800 --> 00:04:54,300
 because they change the mind of the person who undertakes

66
00:04:54,300 --> 00:04:56,400
 them. Specifically they lead

67
00:04:56,400 --> 00:05:03,030
 to one's own detriment. Something is ethical simply because

68
00:05:03,030 --> 00:05:06,720
 it, by its very nature, degrades

69
00:05:06,720 --> 00:05:14,010
 the state of one's mind. So when Mark Zuckerberg cuts the

70
00:05:14,010 --> 00:05:21,320
 throat of a goat, immediately what

71
00:05:21,320 --> 00:05:23,910
 comes to my mind is not the suffering that the goat is

72
00:05:23,910 --> 00:05:25,640
 going through, because that's

73
00:05:25,640 --> 00:05:28,930
 actually relatively minor. It's not a big deal to have your

74
00:05:28,930 --> 00:05:30,400
 throat slit. That might

75
00:05:30,400 --> 00:05:34,850
 seem like a horrific thing to have happen, and indeed it is

76
00:05:34,850 --> 00:05:36,920
, but relative to the state

77
00:05:36,920 --> 00:05:42,800
 of mind of a person who would do that to another living

78
00:05:42,800 --> 00:05:46,780
 being, I find that far scarier. That's

79
00:05:46,780 --> 00:05:50,970
 not an intellectual view, it's one that comes from looking

80
00:05:50,970 --> 00:05:53,240
 at my own history and the things

81
00:05:53,240 --> 00:05:57,570
 that I've done, and being able to compare them with these

82
00:05:57,570 --> 00:05:59,600
 sorts of actions. I cringe

83
00:05:59,600 --> 00:06:03,100
 at the thought of someone doing that to another living

84
00:06:03,100 --> 00:06:05,640
 being, and that comes from cringing

85
00:06:05,640 --> 00:06:11,520
 at my own actions and being able to see the results of my

86
00:06:11,520 --> 00:06:14,640
 own actions and how they've

87
00:06:14,640 --> 00:06:19,950
 affected me and how wrong it was, feeling bad about them

88
00:06:19,950 --> 00:06:22,640
 from the core of my being.

89
00:06:22,640 --> 00:06:26,360
 And it comes from an ability to empathize, which we gain

90
00:06:26,360 --> 00:06:28,800
 through the meditation practice,

91
00:06:28,800 --> 00:06:32,010
 because someone who empathizes with the suffering of others

92
00:06:32,010 --> 00:06:35,200
 will never do these things. And

93
00:06:35,200 --> 00:06:42,020
 it's only through the pushing away or the repression of our

94
00:06:42,020 --> 00:06:44,640
 empathy for the suffering

95
00:06:44,640 --> 00:06:48,280
 of others that we're able to do these sorts of things. It's

96
00:06:48,280 --> 00:06:49,920
 a horrific thing to kill another

97
00:06:49,920 --> 00:06:56,500
 living being, and the fact that that's not clear to us is

98
00:06:56,500 --> 00:06:59,680
 just a sign that we haven't

99
00:06:59,680 --> 00:07:03,930
 taken the time to look at the nature of our actions. People

100
00:07:03,930 --> 00:07:06,080
 who take an intellectual point

101
00:07:06,080 --> 00:07:08,840
 of view will often see nothing wrong with killing, and in

102
00:07:08,840 --> 00:07:10,240
 fact will often postulate

103
00:07:10,240 --> 00:07:15,250
 situations such as this, where killing is the preferable

104
00:07:15,250 --> 00:07:17,360
 method. Now, when you look

105
00:07:17,360 --> 00:07:20,980
 at it in this way, and indeed you should understand that

106
00:07:20,980 --> 00:07:23,040
 this is how the Buddha would have us

107
00:07:23,040 --> 00:07:27,260
 look at ethics, then to compare the two situations, before

108
00:07:27,260 --> 00:07:29,840
 we talk about vegetarianism, comparing

109
00:07:29,840 --> 00:07:34,290
 the two situations, one who a person who eats meat and a

110
00:07:34,290 --> 00:07:37,200
 person who kills, from this point

111
00:07:37,200 --> 00:07:41,200
 of view, putting aside any intellectual arguments, and I'll

112
00:07:41,200 --> 00:07:43,320
 try to get to that in a second, simply

113
00:07:43,320 --> 00:07:47,850
 from the experiential point of view, which is the proper

114
00:07:47,850 --> 00:07:50,280
 view according to the Buddha,

115
00:07:50,280 --> 00:07:54,900
 whether you agree with it or not. It's a world of

116
00:07:54,900 --> 00:07:58,560
 difference. You can eat meat without any

117
00:07:58,560 --> 00:08:01,800
 unwholesomeness in your mind. It's possible. I think that

118
00:08:01,800 --> 00:08:03,440
 that should go without saying

119
00:08:03,440 --> 00:08:06,890
 that a person can be mindful of something no matter what it

120
00:08:06,890 --> 00:08:08,920
 is, putting it in your mouth,

121
00:08:08,920 --> 00:08:14,650
 swallowing it, and tasting it and so on, and making use of

122
00:08:14,650 --> 00:08:17,720
 the energy without having any

123
00:08:17,720 --> 00:08:26,100
 malice or any thoughts of causing suffering to any being at

124
00:08:26,100 --> 00:08:28,080
 all. It's only when we become

125
00:08:28,080 --> 00:08:32,990
 intellectual about it and when we make the assumption, for

126
00:08:32,990 --> 00:08:35,400
 instance, that more beings

127
00:08:35,400 --> 00:08:39,280
 are going to have to die or someone is going to have to

128
00:08:39,280 --> 00:08:41,440
 kill for us, or I guess in the

129
00:08:41,440 --> 00:08:44,320
 sense of people like Mark Zuckerberg, and many people have

130
00:08:44,320 --> 00:08:45,600
 this view that one should

131
00:08:45,600 --> 00:08:50,860
 kill for oneself, the idea that you won't respect the

132
00:08:50,860 --> 00:08:57,160
 significance of the act, but really

133
00:08:57,160 --> 00:09:03,150
 the person who kills cannot help but experience a state of

134
00:09:03,150 --> 00:09:07,120
 crushing and repressing our natural

135
00:09:07,120 --> 00:09:12,360
 empathy for each other as living beings and will degrade

136
00:09:12,360 --> 00:09:14,880
 the state of their mind. This

137
00:09:14,880 --> 00:09:19,810
 is empirical. This is something most people can't see. I

138
00:09:19,810 --> 00:09:21,120
 have a story that I always tell

139
00:09:21,120 --> 00:09:25,980
 about when I was young and I was hunting with my father. I

140
00:09:25,980 --> 00:09:29,040
 wasn't always a Buddhist. I killed

141
00:09:29,040 --> 00:09:33,740
 a deer actually once, which is my claim to shame, but it

142
00:09:33,740 --> 00:09:36,280
 was very difficult. It was a

143
00:09:36,280 --> 00:09:40,070
 very difficult thing to pull the trigger, which was strange

144
00:09:40,070 --> 00:09:41,800
 because I had no qualms about

145
00:09:41,800 --> 00:09:48,240
 it at the time. I wasn't at all afraid of hurting other

146
00:09:48,240 --> 00:09:52,640
 beings. I had some empathy, but not anything

147
00:09:52,640 --> 00:09:57,750
 nearly as advanced as it should have been. Nonetheless, my

148
00:09:57,750 --> 00:10:00,000
 whole body shook and it was

149
00:10:00,000 --> 00:10:03,660
 a difficult thing to do. After that, it became a lot easier

150
00:10:03,660 --> 00:10:05,600
. After the first time, as I got

151
00:10:05,600 --> 00:10:10,020
 into it and got used to it, became accustomed to it, it got

152
00:10:10,020 --> 00:10:12,480
 a lot more difficult. Simply

153
00:10:12,480 --> 00:10:15,550
 because of the repression that goes on and the degradation

154
00:10:15,550 --> 00:10:17,040
 of the mind until you become

155
00:10:17,040 --> 00:10:23,740
 numb to it. This is also something that's easy to see when

156
00:10:23,740 --> 00:10:26,760
 you talk to soldiers or when

157
00:10:26,760 --> 00:10:30,250
 we take... I took peace studies in university and we

158
00:10:30,250 --> 00:10:32,480
 studied war, so we studied how it is

159
00:10:32,480 --> 00:10:36,120
 that you get people to kill. They found that in the First

160
00:10:36,120 --> 00:10:38,480
 World War, there was an incredibly

161
00:10:38,480 --> 00:10:47,860
 low accuracy rate in terms of bullets shot and enemies

162
00:10:47,860 --> 00:10:52,880
 killed or people killed. This was

163
00:10:52,880 --> 00:10:55,640
 because they found out that most of the soldiers didn't

164
00:10:55,640 --> 00:10:57,600
 want to kill their fellow human beings

165
00:10:57,600 --> 00:11:01,250
 and they would shoot over each other's heads. Apparently,

166
00:11:01,250 --> 00:11:03,040
 this is a documented fact. This

167
00:11:03,040 --> 00:11:06,470
 frustrated the leaders greatly and so they tried to figure

168
00:11:06,470 --> 00:11:07,920
 out how it is that you can

169
00:11:07,920 --> 00:11:12,250
 get your soldiers to kill. How can you teach someone to

170
00:11:12,250 --> 00:11:16,560
 kill? The way to do it is to destroy

171
00:11:16,560 --> 00:11:20,640
 the empathy that they might have for the other human being

172
00:11:20,640 --> 00:11:23,320
 by creating distance between them.

173
00:11:23,320 --> 00:11:27,830
 This is why you have all these derogatory terms for the

174
00:11:27,830 --> 00:11:31,160
 enemy. You hear why we had in

175
00:11:31,160 --> 00:11:34,760
 the early part of the last century, a lot of propaganda and

176
00:11:34,760 --> 00:11:36,400
 derogatory terms used to

177
00:11:36,400 --> 00:11:42,750
 refer to the enemies, the names for the Germans, the names

178
00:11:42,750 --> 00:11:45,920
 for the Japanese and so on. The

179
00:11:45,920 --> 00:11:52,260
 movies that show them vicious and mean and cruel. They did

180
00:11:52,260 --> 00:11:53,280
 everything they could to create

181
00:11:53,280 --> 00:11:58,200
 a distance and destroy the ability to empathize with the

182
00:11:58,200 --> 00:12:01,520
 other living being. They found that

183
00:12:01,520 --> 00:12:05,550
 this helped. What they found that helped even more is when

184
00:12:05,550 --> 00:12:08,080
 they started using more technologically

185
00:12:08,080 --> 00:12:12,970
 advanced means of killing each other, bombing. What the

186
00:12:12,970 --> 00:12:15,520
 soldiers would say is, "It's just

187
00:12:15,520 --> 00:12:19,460
 like a video game." They had very little qualms about

188
00:12:19,460 --> 00:12:22,480
 dropping bombs and so on. I think this

189
00:12:22,480 --> 00:12:29,460
 points quite clearly to the relationship between empathy

190
00:12:29,460 --> 00:12:34,040
 and the act of killing and how you

191
00:12:34,040 --> 00:12:38,680
 need to crush your empathy in order to do it. I think

192
00:12:38,680 --> 00:12:40,160
 people who use the computer a

193
00:12:40,160 --> 00:12:47,020
 lot, it gets easier for them because if you're not careful,

194
00:12:47,020 --> 00:12:49,920
 it's easy to build up intense

195
00:12:49,920 --> 00:12:54,950
 states of concentration that are able to crush your

196
00:12:54,950 --> 00:12:58,880
 emotions, crush your sense of reality

197
00:12:58,880 --> 00:13:02,690
 and empathy. It's very easy to think logically that this is

198
00:13:02,690 --> 00:13:04,520
 somehow a better way of doing

199
00:13:04,520 --> 00:13:08,960
 things. That's on the one side, but this also helps us to

200
00:13:08,960 --> 00:13:11,600
 understand what the Buddhist answer

201
00:13:11,600 --> 00:13:17,980
 to the question, "Should we be vegetarians?" No. The answer

202
00:13:17,980 --> 00:13:21,440
 is, in regards to the requirement

203
00:13:21,440 --> 00:13:23,940
 for being a vegetarian, no, there is no requirement and

204
00:13:23,940 --> 00:13:26,200
 there shouldn't be a requirement. In fact,

205
00:13:26,200 --> 00:13:29,810
 I would postulate, but this is actually a little bit of a

206
00:13:29,810 --> 00:13:31,680
 different argument, I would

207
00:13:31,680 --> 00:13:37,120
 postulate that it's better not to be totally vegetarian.

208
00:13:37,120 --> 00:13:38,800
 What I would advocate is passive

209
00:13:38,800 --> 00:13:43,110
 vegetarianism. That's in the sense of not desiring meat,

210
00:13:43,110 --> 00:13:45,880
 but this is actually a different argument.

211
00:13:45,880 --> 00:13:50,590
 I want to finish the first argument. The first argument is

212
00:13:50,590 --> 00:13:55,600
 pointing out that in terms of

213
00:13:55,600 --> 00:13:58,920
 the empirical nature or the experiential nature of eating

214
00:13:58,920 --> 00:14:00,920
 meat and killing, it's two completely

215
00:14:00,920 --> 00:14:04,560
 different things. A person who eats meat can do so free

216
00:14:04,560 --> 00:14:06,640
 from guilt. This is my understanding.

217
00:14:06,640 --> 00:14:10,260
 This is what we have in the earliest Buddhist texts that I

218
00:14:10,260 --> 00:14:12,520
'm aware of. Not in all Buddhist

219
00:14:12,520 --> 00:14:16,940
 texts, but this is the understanding that I follow. I think

220
00:14:16,940 --> 00:14:18,840
 in my mind it's quite clear,

221
00:14:18,840 --> 00:14:25,810
 even though many people will disagree. The other argument

222
00:14:25,810 --> 00:14:30,000
 that has to do with the intellectual

223
00:14:30,000 --> 00:14:42,080
 side of eating meat. The argument goes that, irregardless

224
00:14:42,080 --> 00:14:45,840
 of the states in the person's

225
00:14:45,840 --> 00:14:53,820
 mind at the time, that there is an ethical responsibility

226
00:14:53,820 --> 00:14:57,680
 to refrain from eating meat

227
00:14:57,680 --> 00:15:04,820
 because animals will be killed. I think there's an

228
00:15:04,820 --> 00:15:07,880
 understanding generally that you're not

229
00:15:07,880 --> 00:15:11,840
 eating meat viciously thinking, "Die, die, die," because

230
00:15:11,840 --> 00:15:14,280
 you know it's already dead.

231
00:15:14,280 --> 00:15:18,400
 The ethics that they're talking about, which I submit is

232
00:15:18,400 --> 00:15:20,920
 not really true Buddhist ethics,

233
00:15:20,920 --> 00:15:26,560
 is that it's going to lead to beings being killed. If we

234
00:15:26,560 --> 00:15:29,760
 don't eat meat, then fewer beings

235
00:15:29,760 --> 00:15:34,500
 will be killed. This is why I thought it's important to

236
00:15:34,500 --> 00:15:35,720
 explain the nature of Buddhist

237
00:15:35,720 --> 00:15:45,370
 ethics as I follow it, as I see it. This sort of ethics

238
00:15:45,370 --> 00:15:49,400
 really doesn't hold up in my mind

239
00:15:49,400 --> 00:15:53,720
 because all beings have to die. As I said, the real problem

240
00:15:53,720 --> 00:15:55,840
 isn't that beings are killed

241
00:15:55,840 --> 00:15:59,620
 or so on because we all have to die. More suffering, less

242
00:15:59,620 --> 00:16:01,960
 suffering is really all relative

243
00:16:01,960 --> 00:16:05,630
 and it's really not the biggest problem because eventually

244
00:16:05,630 --> 00:16:07,640
 we're all going to have to learn

245
00:16:07,640 --> 00:16:12,100
 to understand and to let go and to not be affected by

246
00:16:12,100 --> 00:16:14,280
 suffering. It's not necessarily

247
00:16:14,280 --> 00:16:16,860
 the case that suffering is going to be a bad thing or

248
00:16:16,860 --> 00:16:18,840
 something we have to worry about.

249
00:16:18,840 --> 00:16:22,910
 What we really have to worry about is the state of our

250
00:16:22,910 --> 00:16:28,520
 minds. A person who eats meat

251
00:16:28,520 --> 00:16:33,760
 does so with a pure mind. A person who kills obviously does

252
00:16:33,760 --> 00:16:36,080
 so with an impure mind. That's

253
00:16:36,080 --> 00:16:41,670
 really where the difference lies. A person who eats meat is

254
00:16:41,670 --> 00:16:45,440
 not thinking of or has no

255
00:16:45,440 --> 00:16:53,370
 real connection with the act of killing that goes on. The

256
00:16:53,370 --> 00:16:56,120
 idea that somehow you can stop

257
00:16:56,120 --> 00:17:00,670
 beings from being killed is really a bit short-sighted. I

258
00:17:00,670 --> 00:17:03,840
 think considering the fact that when you

259
00:17:03,840 --> 00:17:09,380
 look at the world around us, the amount of killing that

260
00:17:09,380 --> 00:17:13,200
 goes on is something to an incredible

261
00:17:13,200 --> 00:17:19,960
 goal, the magnitude of the problem. If your solution or if

262
00:17:19,960 --> 00:17:23,000
 your goal is to find a solution

263
00:17:23,000 --> 00:17:29,020
 to the problem of killing, then just stepping out your door

264
00:17:29,020 --> 00:17:32,000
 is going to cause difficulty

265
00:17:32,000 --> 00:17:38,050
 for you because of the murder that goes on in every quarter

266
00:17:38,050 --> 00:17:40,560
 by every type of being on

267
00:17:40,560 --> 00:17:44,130
 earth by most types of beings. If you're not a predator,

268
00:17:44,130 --> 00:17:48,680
 you're praying for the most part.

269
00:17:48,680 --> 00:17:52,330
 This points to the second part of the argument that

270
00:17:52,330 --> 00:17:55,440
 Buddhism is a path towards simplicity.

271
00:17:55,440 --> 00:17:58,240
 This sort of intellectual argument has no place in Buddhism

272
00:17:58,240 --> 00:17:59,500
 because we're not trying

273
00:17:59,500 --> 00:18:02,810
 to change the world. We're not trying to fix the world. The

274
00:18:02,810 --> 00:18:04,840
 world is by nature something

275
00:18:04,840 --> 00:18:09,590
 that we've created out of our ignorance. It's something

276
00:18:09,590 --> 00:18:11,920
 that we've put together based on

277
00:18:11,920 --> 00:18:17,740
 our craving, based on the idea that somehow we're going to

278
00:18:17,740 --> 00:18:20,560
 create a stable and lasting

279
00:18:20,560 --> 00:18:24,710
 state of existence. What the Buddha realizes is that you

280
00:18:24,710 --> 00:18:26,680
 can't do it because everything

281
00:18:26,680 --> 00:18:31,600
 is unstable and changing. The more you try to cling to

282
00:18:31,600 --> 00:18:33,600
 things that's stable, the more

283
00:18:33,600 --> 00:18:42,320
 you create suffering. This is another part of the argument.

284
00:18:42,320 --> 00:18:43,880
 The third thing that I wanted

285
00:18:43,880 --> 00:18:47,990
 to say, the second is that we're not ... These sort of

286
00:18:47,990 --> 00:18:50,760
 intellectual arguments ... The point

287
00:18:50,760 --> 00:18:56,230
 is that if you open up the can of worms of the intellectual

288
00:18:56,230 --> 00:18:58,880
 responsibility to not eat

289
00:18:58,880 --> 00:19:04,780
 meat, then you'll never find rest because every part of

290
00:19:04,780 --> 00:19:07,720
 your life in some way causes

291
00:19:07,720 --> 00:19:11,030
 suffering for other beings. When you drive a car, you

292
00:19:11,030 --> 00:19:13,360
 shouldn't drive a car because most

293
00:19:13,360 --> 00:19:16,760
 likely the beings are going to die as a result of you

294
00:19:16,760 --> 00:19:20,000
 driving a car. You shouldn't use most

295
00:19:20,000 --> 00:19:23,210
 beauty products or razor blades because they've all been

296
00:19:23,210 --> 00:19:24,960
 tested on animals. There are people

297
00:19:24,960 --> 00:19:31,550
 who think like this, but it goes to such ... There is no

298
00:19:31,550 --> 00:19:34,640
 line you can draw. I would submit that

299
00:19:34,640 --> 00:19:39,600
 there is no need to find a line. The line is quite clear in

300
00:19:39,600 --> 00:19:42,240
 terms of where our intentions

301
00:19:42,240 --> 00:19:45,520
 are. This is throughout the Buddha's teaching. Buddha said,

302
00:19:45,520 --> 00:19:46,640
 "When you step on an ant, if

303
00:19:46,640 --> 00:19:49,310
 you didn't know you were stepping on the ant, you're not

304
00:19:49,310 --> 00:19:50,800
 guilty of anything." The point

305
00:19:50,800 --> 00:19:53,650
 is what goes on in your mind because this is what's going

306
00:19:53,650 --> 00:19:55,000
 to change who you are and

307
00:19:55,000 --> 00:20:00,620
 this is what's going to affect your future. It's what's

308
00:20:00,620 --> 00:20:04,600
 going to actually change the world

309
00:20:04,600 --> 00:20:08,180
 because if our minds are pure, we'll never have any desire

310
00:20:08,180 --> 00:20:11,040
 to kill. If our mind is pure,

311
00:20:11,040 --> 00:20:15,230
 we'll change the minds of other people. We will help the

312
00:20:15,230 --> 00:20:17,600
 people around us to decide that

313
00:20:17,600 --> 00:20:21,550
 killing is wrong and so on. The problem will never arise.

314
00:20:21,550 --> 00:20:23,520
 The point is to stop people from

315
00:20:23,520 --> 00:20:28,790
 wanting to kill, not to stop people from wanting to eat

316
00:20:28,790 --> 00:20:32,560
 meat. Why I say I think passive vegetarianism

317
00:20:32,560 --> 00:20:38,360
 is good and what that means, there is ... This is the third

318
00:20:38,360 --> 00:20:41,520
 part that passive vegetarianism,

319
00:20:41,520 --> 00:20:44,940
 I think, is a good thing. I don't think this is nearly as

320
00:20:44,940 --> 00:20:46,960
 important, but it's something

321
00:20:46,960 --> 00:20:51,190
 for us to think about because a person who wants to eat

322
00:20:51,190 --> 00:20:53,800
 meat, a person who is actively

323
00:20:53,800 --> 00:20:58,820
 seeking out meat, is doing something that we should all be

324
00:20:58,820 --> 00:21:01,260
 aware of. They are guilty

325
00:21:01,260 --> 00:21:06,750
 of something and a fairly extreme form of karma, of this

326
00:21:06,750 --> 00:21:08,760
 type of karma. The type of

327
00:21:08,760 --> 00:21:14,690
 karma I'm thinking of is the karma based on greed, based on

328
00:21:14,690 --> 00:21:16,940
 desire. It's a prime example

329
00:21:16,940 --> 00:21:22,640
 of how desire leads to the degradation of our world. Our

330
00:21:22,640 --> 00:21:26,480
 desire for meat has, no doubt,

331
00:21:26,480 --> 00:21:32,310
 done a lot to change the nature of this earth. The fact

332
00:21:32,310 --> 00:21:36,240
 that we do kill, the fact that there

333
00:21:36,240 --> 00:21:43,240
 is an incredible amount of killing going on, has degraded

334
00:21:43,240 --> 00:21:46,920
 the world to a great extent.

335
00:21:46,920 --> 00:21:53,190
 They say that the only reason people kill is because people

336
00:21:53,190 --> 00:21:56,080
 want, there is a desire for

337
00:21:56,080 --> 00:21:59,550
 meat and therefore people are killing. That's a simplistic

338
00:21:59,550 --> 00:22:03,400
 thing to say. People kill because

339
00:22:03,400 --> 00:22:05,740
 they have defilements in their mind because they don't

340
00:22:05,740 --> 00:22:07,360
 understand that killing is wrong.

341
00:22:07,360 --> 00:22:09,870
 They're the ones who are guilty of the killing, the person

342
00:22:09,870 --> 00:22:11,280
 who killed. The people who have

343
00:22:11,280 --> 00:22:18,840
 greed, who push them to do it in a sense or make it viable

344
00:22:18,840 --> 00:22:24,720
 for them to do, have some responsibility

345
00:22:24,720 --> 00:22:32,440
 for the situation but are not guilty of murdering. When a

346
00:22:32,440 --> 00:22:34,440
 person desires to eat meat, if you're

347
00:22:34,440 --> 00:22:38,300
 someone who is going out of your way to find meat, and this

348
00:22:38,300 --> 00:22:40,680
 might even include buying meat,

349
00:22:40,680 --> 00:22:45,050
 although I would consider that to be a little more neutral

350
00:22:45,050 --> 00:22:47,320
 in the sense that you go to the

351
00:22:47,320 --> 00:22:50,750
 grocery store and you buy the food that you know is going

352
00:22:50,750 --> 00:22:52,960
 to be healthy. But when a person

353
00:22:52,960 --> 00:22:57,760
 is actively seeking it out, when a person is actively

354
00:22:57,760 --> 00:23:00,720
 looking for meat or encouraging

355
00:23:00,720 --> 00:23:08,600
 people asking for it and so on, then there is the

356
00:23:08,600 --> 00:23:12,760
 degradation. The only type of vegetarianism

357
00:23:12,760 --> 00:23:18,640
 that I really wouldn't advise is the active vegetarianism

358
00:23:18,640 --> 00:23:21,680
 where you refuse meat when it's

359
00:23:21,680 --> 00:23:26,520
 given to you. As monks, this is the type of vegetarianism

360
00:23:26,520 --> 00:23:30,680
 that we don't subscribe to.

361
00:23:30,680 --> 00:23:35,830
 As monks, we are guilty if we ask for meat, if we desire

362
00:23:35,830 --> 00:23:39,040
 meat, if we're looking for it,

363
00:23:39,040 --> 00:23:44,130
 then this is considered to be an offense against our mon

364
00:23:44,130 --> 00:23:47,760
astic code. But we are expected to

365
00:23:47,760 --> 00:23:52,170
 eat the food that people give us. I think this is the line

366
00:23:52,170 --> 00:23:53,920
 in the sand or the line that

367
00:23:53,920 --> 00:23:59,640
 we should draw because at this point there is no guilt and

368
00:23:59,640 --> 00:24:02,360
 no responsibility. The only

369
00:24:02,360 --> 00:24:05,080
 way I can see that a person has responsibility is not

370
00:24:05,080 --> 00:24:07,220
 because intellectually somehow they

371
00:24:07,220 --> 00:24:12,050
 are contributing, but because they are actively encouraging

372
00:24:12,050 --> 00:24:15,040
, promoting, increasing the demand

373
00:24:15,040 --> 00:24:18,570
 because they actually demand it. When a person takes

374
00:24:18,570 --> 00:24:22,400
 whatever food they're given, and this

375
00:24:22,400 --> 00:24:25,640
 doesn't just apply to monks, so when we go to someone's

376
00:24:25,640 --> 00:24:27,280
 house and they offer us food

377
00:24:27,280 --> 00:24:36,680
 and they offer us meat, we are a passive recipient of the

378
00:24:36,680 --> 00:24:41,840
 meat. We're not actively seeking it

379
00:24:41,840 --> 00:24:45,540
 out and we have no greed for it and we eat it for our nour

380
00:24:45,540 --> 00:24:49,440
ishment. I would say that this

381
00:24:49,440 --> 00:25:01,520
 is ethically neutral, an ethically neutral act. For this

382
00:25:01,520 --> 00:25:03,000
 reason I would say that no,

383
00:25:03,000 --> 00:25:06,080
 it's not necessary to be vegetarian. I would say that there

384
00:25:06,080 --> 00:25:07,640
 is an argument that we should

385
00:25:07,640 --> 00:25:14,690
 not actively seek out meat. If a person is looking to

386
00:25:14,690 --> 00:25:17,980
 practice meditation and follow

387
00:25:17,980 --> 00:25:22,570
 the Buddhist teaching, I would say that even buying meat is

388
00:25:22,570 --> 00:25:24,580
 not a real problem if your

389
00:25:24,580 --> 00:25:28,050
 intention is not to specifically buy meat but simply to buy

390
00:25:28,050 --> 00:25:29,800
 food that is going to sustain

391
00:25:29,800 --> 00:25:39,290
 you with the only object for the practice of meditation. As

392
00:25:39,290 --> 00:25:41,800
 I said, the point of the

393
00:25:41,800 --> 00:25:46,620
 Buddhist teaching is the simplification of our lives, that

394
00:25:46,620 --> 00:25:48,760
 our lives should be simply

395
00:25:48,760 --> 00:25:51,890
 about our experience. When we see, it's only seeing, when

396
00:25:51,890 --> 00:25:53,560
 we hear, it's only hearing. The

397
00:25:53,560 --> 00:25:56,190
 Buddha said this is the way to enlightenment. The way we

398
00:25:56,190 --> 00:25:57,740
 should train ourselves is that

399
00:25:57,740 --> 00:26:02,250
 seeing becomes only seeing, hearing becomes only hearing,

400
00:26:02,250 --> 00:26:04,800
 smelling, tasting, feeling and

401
00:26:04,800 --> 00:26:15,400
 seeing. Meat is something that only has significance in an

402
00:26:15,400 --> 00:26:18,200
 intellectual sense. On an experiential

403
00:26:18,200 --> 00:26:22,840
 level it's something that has been abandoned. There is no

404
00:26:22,840 --> 00:26:25,280
 mind involved, so there is no

405
00:26:25,280 --> 00:26:32,980
 empathy and no sympathy and no connection to ourselves that

406
00:26:32,980 --> 00:26:36,760
 needs to arise in the mind.

407
00:26:36,760 --> 00:26:42,640
 It's the physical that has been abandoned by the being to

408
00:26:42,640 --> 00:26:45,480
 whom it once belonged. That

409
00:26:45,480 --> 00:26:50,880
 is my understanding of vegetarianism. It's not quite as

410
00:26:50,880 --> 00:26:53,600
 simple as one might think and

411
00:26:53,600 --> 00:27:00,410
 it does show that Buddhist ethics are actually quite

412
00:27:00,410 --> 00:27:05,600
 logical, scientific and quite simple,

413
00:27:05,600 --> 00:27:08,890
 easy to understand, I should think, especially if you're

414
00:27:08,890 --> 00:27:10,680
 practicing meditation. The only

415
00:27:10,680 --> 00:27:14,620
 time it would be unethical is in the same way that anything

416
00:27:14,620 --> 00:27:16,600
 that has to do with involves

417
00:27:16,600 --> 00:27:21,060
 greed is unethical. Any time we have greed we can see why

418
00:27:21,060 --> 00:27:23,200
 the world is being changed

419
00:27:23,200 --> 00:27:27,240
 in so many ways. It's totally about greed. Our greed for

420
00:27:27,240 --> 00:27:29,280
 oil, our greed for land, our

421
00:27:29,280 --> 00:27:32,800
 greed for gold, money, our greed for computers and

422
00:27:32,800 --> 00:27:34,840
 electronics. This is a good example for

423
00:27:34,840 --> 00:27:38,920
 instance. The electronics industry and all of the metals

424
00:27:38,920 --> 00:27:41,080
 and so on is actually creating

425
00:27:41,080 --> 00:27:45,820
 great conflict in the world. There are people who will say

426
00:27:45,820 --> 00:27:48,680
 that using the electronic devices

427
00:27:48,680 --> 00:27:51,920
 that we have with the computer chips and so on with all of

428
00:27:51,920 --> 00:27:53,680
 their metals that have to be

429
00:27:53,680 --> 00:27:58,740
 mined and therefore have to be fought over. The people who

430
00:27:58,740 --> 00:28:01,140
 are using these are actually

431
00:28:01,140 --> 00:28:05,130
 contributing to the conflict in places like many African

432
00:28:05,130 --> 00:28:07,680
 nations where they are doing mining

433
00:28:07,680 --> 00:28:11,740
 and there's not one group, everyone's fighting over them

434
00:28:11,740 --> 00:28:14,040
 and there's a lot of suffering that

435
00:28:14,040 --> 00:28:18,250
 goes on. My point is that it's not direct because a person

436
00:28:18,250 --> 00:28:20,840
 obviously who uses the electronic

437
00:28:20,840 --> 00:28:24,190
 devices has no thought in their mind that they're doing

438
00:28:24,190 --> 00:28:26,320
 that. The point is those people

439
00:28:26,320 --> 00:28:30,460
 who are fighting over these things are, if they were to

440
00:28:30,460 --> 00:28:33,080
 give up then there would obviously

441
00:28:33,080 --> 00:28:37,740
 be no problem when we let go and the people who are

442
00:28:37,740 --> 00:28:41,440
 encouraging the fighting or pushing

443
00:28:41,440 --> 00:28:46,510
 to get these minerals and so on. Yet we can see how our

444
00:28:46,510 --> 00:28:49,760
 greed affects that, how our need

445
00:28:49,760 --> 00:28:58,060
 for these high tech gadgets and so on. This is an unethical

446
00:28:58,060 --> 00:29:00,000
 part of our being that we

447
00:29:00,000 --> 00:29:03,930
 should give up. If we can use things like technology that

448
00:29:03,930 --> 00:29:06,120
 exist without greed, without

449
00:29:06,120 --> 00:29:10,210
 attachment, without need for them and without need for

450
00:29:10,210 --> 00:29:12,880
 really everything then there would

451
00:29:12,880 --> 00:29:17,700
 never be any need to go to war for anything. We'd all be

452
00:29:17,700 --> 00:29:20,280
 able to use what we have but you

453
00:29:20,280 --> 00:29:26,260
 can see it's totally based on greed that we're fighting

454
00:29:26,260 --> 00:29:29,560
 over rocks as an example. So many

455
00:29:29,560 --> 00:29:35,130
 other things. This is a different aspect of the issue of

456
00:29:35,130 --> 00:29:38,360
 vegetarianism as it is an aspect

457
00:29:38,360 --> 00:29:45,300
 of all of our actions, all of the part of our lives that

458
00:29:45,300 --> 00:29:48,640
 has to do with our desires

459
00:29:48,640 --> 00:29:52,430
 and our need for this or that. So I hope this has been

460
00:29:52,430 --> 00:29:54,440
 interesting and helped to put somehow

461
00:29:54,440 --> 00:29:58,270
 in perspective the issue of vegetarianism. I think it's sad

462
00:29:58,270 --> 00:30:00,160
 that people might feel guilty

463
00:30:00,160 --> 00:30:05,370
 when they eat meat unreasonably, thinking that somehow they

464
00:30:05,370 --> 00:30:07,760
 are responsible for some sort

465
00:30:07,760 --> 00:30:12,030
 of killing. I know there are many people who feel that way

466
00:30:12,030 --> 00:30:14,240
 and actually probably quite

467
00:30:14,240 --> 00:30:17,260
 disagree with what I'm saying here. But I would ask those

468
00:30:17,260 --> 00:30:18,720
 people to look in their own

469
00:30:18,720 --> 00:30:21,600
 minds and to see the state of mind even as they're

470
00:30:21,600 --> 00:30:23,880
 listening to this video because it's

471
00:30:23,880 --> 00:30:27,060
 the state of our mind that is the most important. It's the

472
00:30:27,060 --> 00:30:29,120
 only thing that's going to determine

473
00:30:29,120 --> 00:30:32,260
 our future. If I eat meat I'm not worried about it because

474
00:30:32,260 --> 00:30:33,920
 it means nothing to me. I

475
00:30:33,920 --> 00:30:39,650
 know that the being is gone. But when I see living beings,

476
00:30:39,650 --> 00:30:43,200
 even farm animals or so on,

477
00:30:43,200 --> 00:30:46,830
 knowing that they have to be slaughtered, it's really a

478
00:30:46,830 --> 00:30:49,240
 totally different state of affairs.

479
00:30:49,240 --> 00:30:53,440
 I could never do some of the things that I did before

480
00:30:53,440 --> 00:30:56,560
 because I understand the suffering

481
00:30:56,560 --> 00:31:00,220
 involved. It's quite clear I think if you meditate to see

482
00:31:00,220 --> 00:31:02,200
 the difference that you would

483
00:31:02,200 --> 00:31:06,900
 never wish suffering on another being if you understand the

484
00:31:06,900 --> 00:31:09,120
 nature of suffering and the

485
00:31:09,120 --> 00:31:13,320
 nature of the defilements that lead us to cause suffering

486
00:31:13,320 --> 00:31:15,960
 for others. So I think that's

487
00:31:15,960 --> 00:31:18,900
 enough for this question. Thanks for tuning in. All the

488
00:31:18,900 --> 00:31:19,160
 best.

