1
00:00:00,000 --> 00:00:14,100
 Here we have something unique. These men are prisoners,

2
00:00:14,100 --> 00:00:23,360
 convict, and they've been engaged

3
00:00:23,360 --> 00:00:33,650
 to come to help us for a day. So they're bringing sand and

4
00:00:33,650 --> 00:00:37,520
 rocks and this man is offered to

5
00:00:37,520 --> 00:00:43,460
 cut down this tree for me. It's already dead, I think. He's

6
00:00:43,460 --> 00:00:44,920
 just going to get it out of

7
00:00:44,920 --> 00:00:51,160
 the way. So this is a very welcome surprise. 20 men coming

8
00:00:51,160 --> 00:00:53,760
 to do their groundwork. This

9
00:00:53,760 --> 00:00:59,780
 is what has made building here really difficult. This is

10
00:00:59,780 --> 00:01:03,320
 sand and rocks and cement all the

11
00:01:03,320 --> 00:01:09,840
 way down here. So this will make it a lot easier to build.

12
00:01:09,840 --> 00:01:11,720
 Now we have the sand just

13
00:01:11,720 --> 00:01:16,330
 coming soon. Next they'll bring the rocks and today the

14
00:01:16,330 --> 00:01:18,800
 engineer is coming. I said he

15
00:01:18,800 --> 00:01:22,070
 was coming yesterday but it changed. So here's where we're

16
00:01:22,070 --> 00:01:24,440
 going to build two cuties. I think

17
00:01:24,440 --> 00:01:30,840
 the idea now is to build two rooms here, two rooms together

18
00:01:30,840 --> 00:01:33,680
. Because it's a little bit

19
00:01:33,680 --> 00:01:37,620
 cheaper to make two rooms joined with one wall joining them

20
00:01:37,620 --> 00:01:39,720
. And people feel safer that

21
00:01:39,720 --> 00:01:44,330
 way knowing that it's someone else. So we have two here and

22
00:01:44,330 --> 00:01:46,480
 this one we've demolished.

23
00:01:46,480 --> 00:01:52,030
 I demolished, pushed. I'm good at demolishing. Building is

24
00:01:52,030 --> 00:01:55,080
 not so proficient. I think we'll

25
00:01:55,080 --> 00:01:57,400
 leave this for now. This one's going to be more difficult

26
00:01:57,400 --> 00:01:58,520
 because we have to join it

27
00:01:58,520 --> 00:02:03,520
 with the cave wall. But eventually we'll build a third cut

28
00:02:03,520 --> 00:02:06,040
ie here. So another update.

29
00:02:06,040 --> 00:02:07,040
 [gunshot]

30
00:02:07,040 --> 00:02:37,040
 [

