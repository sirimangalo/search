1
00:00:00,000 --> 00:00:07,610
 Good evening everyone and welcome to our live Dhamma

2
00:00:07,610 --> 00:00:13,200
 broadcast and our nightly Dhamma for those of you here in

3
00:00:13,200 --> 00:00:15,000
 Hamilton.

4
00:00:24,000 --> 00:00:31,000
 And I thought to talk about fear.

5
00:00:39,000 --> 00:00:50,880
 Bya, bya in poly and sphere or danger or fearsomeness,

6
00:00:50,880 --> 00:00:54,000
 terror.

7
00:00:58,000 --> 00:01:05,430
 Those of you who still dare to turn on the news, I'm sure

8
00:01:05,430 --> 00:01:10,000
 are inundated by fear mongering.

9
00:01:10,000 --> 00:01:18,860
 We call terrorism the intentional cultivation of fear in

10
00:01:18,860 --> 00:01:21,000
 others.

11
00:01:21,000 --> 00:01:42,830
 Sometimes there are other reasons for other goals but quite

12
00:01:42,830 --> 00:01:44,000
 often,

13
00:01:44,000 --> 00:01:51,830
 it's quite common now to see this seeming single-minded

14
00:01:51,830 --> 00:02:02,000
 purpose of psychological warfare or terrorism.

15
00:02:04,000 --> 00:02:31,000
 [Silence]

16
00:02:31,000 --> 00:02:42,580
 The first thing for us to understand about terror and

17
00:02:42,580 --> 00:02:48,000
 terrorism is that being psychological warfare,

18
00:02:48,000 --> 00:02:56,180
 of course, it relies upon the victim, it relies upon the

19
00:02:56,180 --> 00:03:00,000
 vulnerability of the victim

20
00:03:00,000 --> 00:03:09,800
 and the sensitivity of the victim. It doesn't work against

21
00:03:09,800 --> 00:03:16,000
 people who are not afraid.

22
00:03:16,000 --> 00:03:28,000
 And it seems somewhat trite or simplistic to suggest that

23
00:03:28,000 --> 00:03:30,000
 part of the solution is to not be afraid.

24
00:03:30,000 --> 00:03:37,330
 But it's important to understand, it's really important to

25
00:03:37,330 --> 00:03:39,000
 understand as a Buddhist,

26
00:03:39,000 --> 00:03:51,000
 the mechanics of suffering, the mechanics of conflict.

27
00:03:51,000 --> 00:03:57,110
 As we know, if you go deeply into the meditation practice,

28
00:03:57,110 --> 00:04:00,000
 even physical pain is only true suffering

29
00:04:00,000 --> 00:04:08,930
 when you let it upset you. Even physical pain, even true

30
00:04:08,930 --> 00:04:22,000
 direct physical violence is only truly successful

31
00:04:22,000 --> 00:04:30,420
 when the victim is psychologically vulnerable. But

32
00:04:30,420 --> 00:04:35,280
 terrorism, terrorism which seems to usually involve a lot

33
00:04:35,280 --> 00:04:37,000
 of physical violence,

34
00:04:37,000 --> 00:04:43,220
 is conducted in such a way as to have overwhelming

35
00:04:43,220 --> 00:04:48,000
 collateral damage psychologically.

36
00:04:48,000 --> 00:04:55,720
 So of course not to minimalize the terrible tragedy of

37
00:04:55,720 --> 00:05:04,370
 death, the physical pain that comes from these acts of

38
00:05:04,370 --> 00:05:06,000
 terror.

39
00:05:06,000 --> 00:05:14,660
 But to remark upon what makes them so effective is how it

40
00:05:14,660 --> 00:05:17,000
 scares people.

41
00:05:17,000 --> 00:05:22,240
 And of course I don't think it's the solution, it's not the

42
00:05:22,240 --> 00:05:31,000
 entire solution for us to stop being afraid of such things.

43
00:05:31,000 --> 00:05:37,080
 But it is worth remarking that between being afraid of them

44
00:05:37,080 --> 00:05:40,000
 and being not afraid of them,

45
00:05:40,000 --> 00:05:47,910
 not being afraid or frightened by acts of gross and

46
00:05:47,910 --> 00:05:59,640
 horrific violence, is far preferable to be unshaken, to be

47
00:05:59,640 --> 00:06:05,000
 impervious.

48
00:06:05,000 --> 00:06:10,280
 It's far preferable not just for one's own self but for one

49
00:06:10,280 --> 00:06:21,060
's ability to react properly and to consider the situation

50
00:06:21,060 --> 00:06:24,000
 wisely.

51
00:06:24,000 --> 00:06:26,740
 In the Buddhist time, I mean, I can't think of any,

52
00:06:26,740 --> 00:06:29,750
 obviously there were no suicide bombers in the Buddhist

53
00:06:29,750 --> 00:06:30,000
 time,

54
00:06:30,000 --> 00:06:34,370
 there were no violent shootings, I can't think of any

55
00:06:34,370 --> 00:06:38,610
 violent killings of innocent individuals simply for the

56
00:06:38,610 --> 00:06:43,650
 purpose of cultivating terror or targeting a specific group

57
00:06:43,650 --> 00:06:44,000
.

58
00:06:44,000 --> 00:06:50,000
 Imagine if I thought for a while I could come up with some.

59
00:06:50,000 --> 00:06:56,000
 But there are specific acts of terrorism, mild acts,

60
00:06:56,000 --> 00:07:03,550
 that give you the general, I still give the general idea of

61
00:07:03,550 --> 00:07:12,000
 responses and means of coping with or dealing with fear.

62
00:07:12,000 --> 00:07:14,700
 First of all about fear in general we have the dajjika s

63
00:07:14,700 --> 00:07:18,000
utra, which is a very important sutra really.

64
00:07:18,000 --> 00:07:22,460
 Not in terms of having any core value for leading one to

65
00:07:22,460 --> 00:07:27,140
 enlightenment, but it's very important in creating Buddhist

66
00:07:27,140 --> 00:07:32,000
 culture and creating a Buddhist outlook on life.

67
00:07:32,000 --> 00:07:39,230
 Because it deals directly with fear and deals directly with

68
00:07:39,230 --> 00:07:47,000
 the triple gem, these three powerful objects of reflection,

69
00:07:47,000 --> 00:07:49,000
 the Buddha, the Dhamma and the Sangha.

70
00:07:49,000 --> 00:07:52,910
 So the Buddha said, he talked about this war and he was

71
00:07:52,910 --> 00:07:55,000
 telling a story about Indra.

72
00:07:55,000 --> 00:07:58,310
 How Indra said, well if you're afraid in battle, all the

73
00:07:58,310 --> 00:08:02,000
 angels fighting against the, whatever the other guys were,

74
00:08:02,000 --> 00:08:06,780
 the non-angels, asuras they called them, fighting against

75
00:08:06,780 --> 00:08:07,000
 them.

76
00:08:07,000 --> 00:08:13,560
 He said, if you're afraid, look at my banner, look at the d

77
00:08:13,560 --> 00:08:16,000
ajjika at the top of my banner, you'll see the flag.

78
00:08:16,000 --> 00:08:19,200
 And when you see my standard there and you know that I

79
00:08:19,200 --> 00:08:22,950
 haven't fallen, it will give you courage and your fear will

80
00:08:22,950 --> 00:08:24,000
 disappear.

81
00:08:24,000 --> 00:08:29,950
 And he said, if you don't look at mine, we'll look at this

82
00:08:29,950 --> 00:08:35,660
 general and that general and these gods, look at their

83
00:08:35,660 --> 00:08:37,000
 banners.

84
00:08:37,000 --> 00:08:41,460
 And the Buddha said, you know, he can say this all he wants

85
00:08:41,460 --> 00:08:45,930
, but truth is, if they look at his banner, maybe the fear

86
00:08:45,930 --> 00:08:49,000
 will disappear and maybe it won't.

87
00:08:49,000 --> 00:08:52,460
 It's because Indra's not a very good role model. He's not

88
00:08:52,460 --> 00:08:54,640
 himself free from fear. He's not someone that when you

89
00:08:54,640 --> 00:08:59,000
 think of him, your mind is calm and you have a good example

90
00:08:59,000 --> 00:09:04,280
 and a reminder of right and wrong and example of a pure and

91
00:09:04,280 --> 00:09:08,000
 unshaken individual who's not afraid of anything.

92
00:09:08,000 --> 00:09:13,000
 To remind you that this is the best way.

93
00:09:13,000 --> 00:09:17,800
 He said, but if you it, I say to you monks, if you ever are

94
00:09:17,800 --> 00:09:22,190
 often the forest and you get afraid, or let's say those of

95
00:09:22,190 --> 00:09:25,000
 us who are living in society and we're afraid.

96
00:09:25,000 --> 00:09:28,580
 He said, think of the Buddha. And this is where we actually

97
00:09:28,580 --> 00:09:32,190
 get these main chants. He said, think of all the qualities

98
00:09:32,190 --> 00:09:33,000
 of the Buddha.

99
00:09:33,000 --> 00:09:36,360
 And so if you ever hear Buddhist chanting in the Theravada

100
00:09:36,360 --> 00:09:41,000
 tradition in any country, they all recite these.

101
00:09:41,000 --> 00:09:50,360
 He said, or if you don't think about me, think about the D

102
00:09:50,360 --> 00:09:59,000
hamma. Think about the Dhamma, think about the Sangha.

103
00:09:59,000 --> 00:10:04,110
 So, I mean, not to suggest that that's a solution to

104
00:10:04,110 --> 00:10:09,010
 terrorism or it's a way for us to be free from fear of

105
00:10:09,010 --> 00:10:12,000
 gross physical violence.

106
00:10:12,000 --> 00:10:16,180
 But on the other hand, as Buddhists, as Buddhist meditators

107
00:10:16,180 --> 00:10:20,100
, and even as people who are not Buddhists, but just as med

108
00:10:20,100 --> 00:10:29,180
itators, the idea of remembering, the idea of recollecting

109
00:10:29,180 --> 00:10:34,000
 yourself and remembering the path that we're on,

110
00:10:34,000 --> 00:10:38,220
 remembering those who have trod the path, remembering the

111
00:10:38,220 --> 00:10:41,000
 ones who have taught the path, remembering there.

112
00:10:41,000 --> 00:10:48,270
 Their greatness, their nobility, their freedom from fear

113
00:10:48,270 --> 00:10:54,820
 gives us a good grounding and reminds us that fear is not

114
00:10:54,820 --> 00:11:01,000
 useful, doesn't help us, it makes us a victim.

115
00:11:01,000 --> 00:11:04,940
 And if we can free ourselves from our reactions, then

116
00:11:04,940 --> 00:11:09,130
 really terrorism loses a lot of its strength, a lot of its

117
00:11:09,130 --> 00:11:10,000
 power.

118
00:11:10,000 --> 00:11:13,000
 So certainly it is part of the solution, I think.

119
00:11:13,000 --> 00:11:18,030
 But this concept, I mean, this is a key concept in Buddhism

120
00:11:18,030 --> 00:11:22,000
, the idea that reactions are the problem.

121
00:11:22,000 --> 00:11:29,000
 It also applies to terrorists, those who create terror.

122
00:11:29,000 --> 00:11:35,450
 We create terror because we have goals, we have ambitions,

123
00:11:35,450 --> 00:11:41,450
 evil goals, evil ambitions, or else evil means, evil

124
00:11:41,450 --> 00:11:46,000
 intentions, evil minds, evil mind states.

125
00:11:46,000 --> 00:11:49,970
 We have these inside. When a bully picks on someone weaker

126
00:11:49,970 --> 00:11:54,120
 than them, when older siblings frighten their younger

127
00:11:54,120 --> 00:11:59,000
 siblings, when parents scare their children, yell at them,

128
00:11:59,000 --> 00:12:04,000
 shout at them, raise their fists, or even hit them.

129
00:12:04,000 --> 00:12:08,000
 All of this is terrorism. I mean, part of it is terrorism.

130
00:12:08,000 --> 00:12:17,130
 Sometimes it's just the desire to inflict pain, sometimes

131
00:12:17,130 --> 00:12:22,000
 it's the desire to frighten.

132
00:12:22,000 --> 00:12:29,270
 All of this is reaction as well. It's based on reaction. It

133
00:12:29,270 --> 00:12:34,000
's based on an inability to be at peace.

134
00:12:34,000 --> 00:12:39,930
 So I mean, really the solution, if one can call it that,

135
00:12:39,930 --> 00:12:45,520
 because there's no question that it's not likely to be

136
00:12:45,520 --> 00:12:49,000
 successful, not anytime soon.

137
00:12:49,000 --> 00:12:57,000
 But the work that we do to fix and to solve these problems

138
00:12:57,000 --> 00:13:01,000
 is to teach.

139
00:13:01,000 --> 00:13:07,980
 A lot of terrorism is based on antagonism, enmity between

140
00:13:07,980 --> 00:13:15,000
 groups, religious groups, is what we're seeing now.

141
00:13:15,000 --> 00:13:23,800
 We see Islam. Islam is not Islam, but Muslims are very

142
00:13:23,800 --> 00:13:32,360
 angry, and not just Muslims, some people from these

143
00:13:32,360 --> 00:13:40,000
 countries, Muslim countries in general, are very angry,

144
00:13:40,000 --> 00:13:43,000
 angry at Christians, angry at Americans.

145
00:13:43,000 --> 00:13:49,390
 There have been religious wars going back centuries,

146
00:13:49,390 --> 00:13:54,000
 millennia maybe, and Greek and Jews.

147
00:13:54,000 --> 00:13:59,410
 And then you have the other way. Americans are, and many

148
00:13:59,410 --> 00:14:06,790
 Europeans, Canadians, have anger and antagonism towards

149
00:14:06,790 --> 00:14:10,090
 Muslims, towards people who come from Muslim countries,

150
00:14:10,090 --> 00:14:12,000
 regardless of whether they're Muslim or not.

151
00:14:12,000 --> 00:14:19,400
 So we have racism. We have whatever it is to be prejudiced

152
00:14:19,400 --> 00:14:24,000
 against another person's religion.

153
00:14:24,000 --> 00:14:29,470
 This is all reactions, right? We have going back

154
00:14:29,470 --> 00:14:37,000
 generations, this bad blood, where we can't stop this cycle

155
00:14:37,000 --> 00:14:37,000
.

156
00:14:37,000 --> 00:14:41,430
 People from these, with these religious or cultural ethnic

157
00:14:41,430 --> 00:14:45,000
 backgrounds, are fighting with each other.

158
00:14:45,000 --> 00:14:52,070
 White against black against brown against red, yellow,

159
00:14:52,070 --> 00:14:59,160
 Muslim against Christian against Jew, Buddhist against

160
00:14:59,160 --> 00:15:00,000
 Hindu.

161
00:15:01,000 --> 00:15:12,780
 We haven't learned to just be, we haven't learned object

162
00:15:12,780 --> 00:15:15,000
ivity.

163
00:15:15,000 --> 00:15:19,740
 We haven't learned to experience life without reacting,

164
00:15:19,740 --> 00:15:24,680
 without judging, without building up these prejudices and

165
00:15:24,680 --> 00:15:27,000
 these cruel intentions.

166
00:15:27,000 --> 00:15:40,790
 The real solution, it bears repeating, that suffering comes

167
00:15:40,790 --> 00:15:45,000
 from our reactions, not from our experiences.

168
00:15:45,000 --> 00:15:54,000
 If we could learn to just experience things as they are, we

169
00:15:54,000 --> 00:15:58,110
'd let go of them. We wouldn't cling. We'd fly away, leave

170
00:15:58,110 --> 00:15:59,000
 all of our suffering behind.

171
00:15:59,000 --> 00:16:11,000
 And then whatever happened. I have two other stories.

172
00:16:11,000 --> 00:16:16,010
 The first one is about these monks who went off into the

173
00:16:16,010 --> 00:16:23,000
 forest to practice meditation.

174
00:16:23,000 --> 00:16:26,920
 The angels up in the trees had to come down. They were

175
00:16:26,920 --> 00:16:31,960
 Buddhist, I guess, or they were, probably not Buddhist, but

176
00:16:31,960 --> 00:16:34,000
 they would have been respectful towards recklessness.

177
00:16:34,000 --> 00:16:36,560
 And when the monks went into the forest, the angels fell

178
00:16:36,560 --> 00:16:39,640
 out. "Oh, we have to come down from the trees." They were

179
00:16:39,640 --> 00:16:42,000
 tree angels or sprites or whatever.

180
00:16:42,000 --> 00:16:45,700
 And they had to leave their homes up in the trees because,

181
00:16:45,700 --> 00:16:49,470
 out of respect for the monks, out of respect for these reck

182
00:16:49,470 --> 00:16:53,410
lessness, I guess, it's some sort of, you know, maybe it was

183
00:16:53,410 --> 00:16:58,060
 because Indra had instituted from the high heavens, because

184
00:16:58,060 --> 00:17:00,000
 he was Buddhist.

185
00:17:00,000 --> 00:17:02,720
 Maybe he had said, "Well, you have to, monks go into the

186
00:17:02,720 --> 00:17:05,600
 forest. It's a law in the angel world, maybe, I don't know

187
00:17:05,600 --> 00:17:06,000
."

188
00:17:06,000 --> 00:17:08,960
 But they came down and it kind of irked them, and they

189
00:17:08,960 --> 00:17:13,490
 weren't really happy about it. So they thought, "Well, what

190
00:17:13,490 --> 00:17:19,000
 can we do to get these monks to leave our area?"

191
00:17:19,000 --> 00:17:23,950
 And so all day and night they cultivated fear. They gave,

192
00:17:23,950 --> 00:17:28,650
 they sent these visions to the monks of headless bodies and

193
00:17:28,650 --> 00:17:34,230
 bodyless heads, and gruesome ghosts and apparitions of all

194
00:17:34,230 --> 00:17:37,000
 sorts and sounds and so on.

195
00:17:37,000 --> 00:17:40,310
 And the monks were unable to be, unable to focus. They were

196
00:17:40,310 --> 00:17:43,620
 totally out of their minds, freaking out, and they said, "

197
00:17:43,620 --> 00:17:45,000
We can't stay here."

198
00:17:45,000 --> 00:17:48,480
 And so they went back to the Buddha, and the Buddha said to

199
00:17:48,480 --> 00:17:53,450
 them, "Oh, well, first time you went to the forest you didn

200
00:17:53,450 --> 00:17:58,320
't have a weapon, a weapon to fight this terror, this

201
00:17:58,320 --> 00:17:59,000
 terrorism."

202
00:17:59,000 --> 00:18:03,210
 This is a good example of Buddhist terrorism. It's mild, I

203
00:18:03,210 --> 00:18:08,780
 know. There was no, oh, there was some, sort of significant

204
00:18:08,780 --> 00:18:13,000
, can you imagine one of those, being in a horror film,

205
00:18:13,000 --> 00:18:16,290
 that these terrible visions, they didn't know what was

206
00:18:16,290 --> 00:18:20,670
 going on. They thought these were actually demons, maybe,

207
00:18:20,670 --> 00:18:22,000
 out to get them.

208
00:18:22,000 --> 00:18:27,000
 The Buddha said, "Well, you need a weapon to fight this."

209
00:18:27,000 --> 00:18:31,000
 And he taught them what we call the Karraniya Mitta Sutta.

210
00:18:31,000 --> 00:18:34,000
 Karraniya Mitta Sutta.

211
00:18:34,000 --> 00:18:39,950
 They said, "Go back, and as you walk into the forest chant

212
00:18:39,950 --> 00:18:41,000
 this."

213
00:18:41,000 --> 00:18:43,390
 And so they chanted the loving kindness, the Sutta loving

214
00:18:43,390 --> 00:18:47,000
 kindness. Karraniya Mitta Kossalaya Na Ya Tung.

215
00:18:47,000 --> 00:18:51,000
 Yantang San Tung Phattang Nabi Samhicha.

216
00:18:51,000 --> 00:18:59,000
 Sako Jojo Sojo Jojo Suwa Jojo San Suwa Jojo Sojo Jojo.

217
00:18:59,000 --> 00:19:06,000
 Sojo Jojo.

218
00:19:06,000 --> 00:19:09,000
 They chanted.

219
00:19:09,000 --> 00:19:20,060
 And Anmana Sabe Sattaa Bhuantu Suki Tattaa. May all beings

220
00:19:20,060 --> 00:19:29,000
 be happy in their minds.

221
00:19:29,000 --> 00:19:37,120
 Which speaks of one sort of conventional way of dealing

222
00:19:37,120 --> 00:19:44,000
 with antagonism, dealing with enmity.

223
00:19:44,000 --> 00:19:50,160
 A good way to change reactions is to apply the opposite,

224
00:19:50,160 --> 00:19:54,000
 when confronted by hate, or apply with love.

225
00:19:54,000 --> 00:19:58,920
 It's conventional. It's not a deep Buddhist teaching, but

226
00:19:58,920 --> 00:20:01,000
 it is a Buddhist teaching.

227
00:20:01,000 --> 00:20:05,460
 That when you supplant something with its opposite, you are

228
00:20:05,460 --> 00:20:08,000
 able to change the course of events.

229
00:20:08,000 --> 00:20:11,690
 It takes work. It takes effort. It's not something that's

230
00:20:11,690 --> 00:20:14,000
 sustainable over the long term.

231
00:20:14,000 --> 00:20:20,160
 It takes effort to constantly have a loving attitude when

232
00:20:20,160 --> 00:20:24,000
 people are throwing hate at you.

233
00:20:24,000 --> 00:20:29,000
 But it worked. These monks, what they did is they taught.

234
00:20:29,000 --> 00:20:32,260
 It was a teaching. It wasn't just sending love to these

235
00:20:32,260 --> 00:20:33,000
 angels.

236
00:20:33,000 --> 00:20:40,000
 They were chanting it, and it was a reminder.

237
00:20:40,000 --> 00:20:43,670
 It was a reminder of the suffering that comes from these

238
00:20:43,670 --> 00:20:45,000
 bad intentions.

239
00:20:45,000 --> 00:20:49,000
 The intent to cause fear.

240
00:20:49,000 --> 00:21:00,440
 The best way we can defeat our enemies is to give them what

241
00:21:00,440 --> 00:21:02,000
 is most precious.

242
00:21:02,000 --> 00:21:11,000
 Give them knowledge and wisdom. Give them truth.

243
00:21:11,000 --> 00:21:16,450
 The other example, it's a very small sort of insignificant

244
00:21:16,450 --> 00:21:17,000
 example,

245
00:21:17,000 --> 00:21:23,930
 but it speaks to the larger picture of the context of these

246
00:21:23,930 --> 00:21:25,000
 acts.

247
00:21:25,000 --> 00:21:31,090
 I think there's more than one actually. There's examples of

248
00:21:31,090 --> 00:21:32,000
 Mara.

249
00:21:32,000 --> 00:21:37,890
 The monks would be sitting in meditation, and suddenly an

250
00:21:37,890 --> 00:21:41,000
 ox would come along

251
00:21:41,000 --> 00:21:47,080
 and walk up near where the bulls, their ceramic bulls were

252
00:21:47,080 --> 00:21:48,000
 stacked.

253
00:21:48,000 --> 00:21:51,430
 The monks would freak out, "We have to get up. There's this

254
00:21:51,430 --> 00:21:54,000
 big ox coming. It's going to break all our bulls."

255
00:21:54,000 --> 00:21:57,050
 So they all got agitated, and the Buddha said to them, "

256
00:21:57,050 --> 00:21:59,000
That's not an ox. That's Mara."

257
00:21:59,000 --> 00:22:02,000
 Apparently this happened several times in different ways.

258
00:22:02,000 --> 00:22:07,000
 Mara would do anything he couldn't to cause fear.

259
00:22:07,000 --> 00:22:12,000
 I only bring that insignificant example up, and who knows?

260
00:22:12,000 --> 00:22:16,000
 It seems kind of a strange thing to happen.

261
00:22:16,000 --> 00:22:23,170
 To sort of think about what aspect of samsara we're talking

262
00:22:23,170 --> 00:22:25,000
 about here.

263
00:22:25,000 --> 00:22:30,000
 Fear is Mara's domain.

264
00:22:30,000 --> 00:22:36,750
 Fear is part of this, part of samsara of those who delight

265
00:22:36,750 --> 00:22:38,000
 in chaos,

266
00:22:38,000 --> 00:22:41,000
 those who delight in suffering.

267
00:22:41,000 --> 00:22:50,000
 There are angels, human beings, who are bent on this.

268
00:22:50,000 --> 00:22:55,700
 And just to be clear that we don't want to be one of those

269
00:22:55,700 --> 00:22:57,000
 people.

270
00:22:57,000 --> 00:23:01,000
 We don't want to be involved with that part of samsara.

271
00:23:01,000 --> 00:23:05,000
 It's a part of samsara that will probably always be around.

272
00:23:05,000 --> 00:23:08,070
 Probably not something that's ever going to disappear

273
00:23:08,070 --> 00:23:09,000
 completely.

274
00:23:09,000 --> 00:23:17,000
 It might ebb and swell.

275
00:23:17,000 --> 00:23:22,000
 But the universe is a big place.

276
00:23:22,000 --> 00:23:27,290
 And so really our goal is to, well, the Buddha said, "Keep

277
00:23:27,290 --> 00:23:28,000
 your minds calm.

278
00:23:28,000 --> 00:23:35,000
 Keep your minds set. Keep your minds objective."

279
00:23:35,000 --> 00:23:39,650
 So bring it up as well because it's really an instruction

280
00:23:39,650 --> 00:23:41,000
 to meditate.

281
00:23:41,000 --> 00:23:45,200
 A reminder to us that many things will come to disturb our

282
00:23:45,200 --> 00:23:46,000
 state of mind,

283
00:23:46,000 --> 00:23:50,000
 both in meditation and out.

284
00:23:50,000 --> 00:23:54,320
 And they're the problem. It's Mara. I think it's Mara. It's

285
00:23:54,320 --> 00:23:55,000
 Satan.

286
00:23:55,000 --> 00:23:58,120
 It's that part of samsara that wants to pull us back in,

287
00:23:58,120 --> 00:24:00,000
 that doesn't want to see us free.

288
00:24:00,000 --> 00:24:08,310
 It doesn't want to see us happy. It doesn't want to allow

289
00:24:08,310 --> 00:24:10,000
 us peace.

290
00:24:10,000 --> 00:24:17,850
 So our practice is to free ourselves from the hooks, from

291
00:24:17,850 --> 00:24:20,000
 the grasp,

292
00:24:20,000 --> 00:24:25,000
 free ourselves from the vulnerability to these forces,

293
00:24:25,000 --> 00:24:28,000
 forces of Mara.

294
00:24:28,000 --> 00:24:30,000
 And to help others.

295
00:24:30,000 --> 00:24:35,630
 And the best way we can overcome terrorists is by freeing

296
00:24:35,630 --> 00:24:37,000
 them from the need,

297
00:24:37,000 --> 00:24:42,300
 helping them be free from this need to torture others and

298
00:24:42,300 --> 00:24:44,000
 thereby harm themselves

299
00:24:44,000 --> 00:24:50,030
 and corrupt their own mind. Sentence themselves to great

300
00:24:50,030 --> 00:24:52,000
 suffering.

301
00:24:52,000 --> 00:24:59,000
 There you go. Just some thoughts on fear, terror, terrorism

302
00:24:59,000 --> 00:24:59,000
.

303
00:24:59,000 --> 00:25:02,000
 I think they're somewhat apropos.

304
00:25:02,000 --> 00:25:07,000
 There's the demo for tonight. Thank you all for tuning in.

