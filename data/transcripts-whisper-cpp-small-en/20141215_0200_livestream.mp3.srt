1
00:00:00,000 --> 00:00:05,000
 Okay, good morning everyone.

2
00:00:05,000 --> 00:00:11,000
 Broadcasting here from Bangkok, Thailand.

3
00:00:11,000 --> 00:00:15,000
 I missed yesterday.

4
00:00:15,000 --> 00:00:19,000
 I gave a talk yesterday in Thai.

5
00:00:19,000 --> 00:00:22,000
 I'll put that up on the internet.

6
00:00:22,000 --> 00:00:25,000
 Someone recorded it.

7
00:00:25,000 --> 00:00:42,130
 And, well, I think that's a good topic to talk about in

8
00:00:42,130 --> 00:00:43,000
 English as well.

9
00:00:43,000 --> 00:00:48,040
 So this morning I'll give you a breakdown of what I talked

10
00:00:48,040 --> 00:00:49,000
 about.

11
00:00:49,000 --> 00:01:00,000
 See, in Thailand there are political issues right now.

12
00:01:00,000 --> 00:01:18,520
 And so we were talking about the causes of political

13
00:01:18,520 --> 00:01:24,000
 problems and solutions.

14
00:01:24,000 --> 00:01:30,020
 And it's really pretty simple from a Buddhist point of view

15
00:01:30,020 --> 00:01:32,000
 what the problem is and what the solutions are.

16
00:01:32,000 --> 00:01:38,000
 We have the solutions to all the world's problems.

17
00:01:38,000 --> 00:01:43,000
 So from a Buddhist point of view it's kind of...

18
00:01:43,000 --> 00:01:49,430
 You almost want to say it's kind of amusing, but it's...or

19
00:01:49,430 --> 00:01:54,580
 I think it's not really amusing. It's more exasperating to

20
00:01:54,580 --> 00:01:59,660
 see people running around expounding upon solutions and

21
00:01:59,660 --> 00:02:12,000
 causes and so on and so on.

22
00:02:12,000 --> 00:02:16,730
 As though their political systems were a part of nature and

23
00:02:16,730 --> 00:02:20,000
 they had specific laws specific to them.

24
00:02:20,000 --> 00:02:28,350
 Like communism could be...or capitalism or someism could be

25
00:02:28,350 --> 00:02:34,000
 intrinsically better than another system.

26
00:02:34,000 --> 00:02:39,260
 When in fact these are all just human concepts and

27
00:02:39,260 --> 00:02:45,320
 constructs that have basis in reality but aren't in and of

28
00:02:45,320 --> 00:02:48,000
 themselves reality.

29
00:02:48,000 --> 00:02:52,000
 The problem, of course, is people's virtue.

30
00:02:52,000 --> 00:02:55,680
 It's the goodness of people. You can have a kingdom. If the

31
00:02:55,680 --> 00:03:00,960
 king is good, he's virtuous, then the kingdom will be...

32
00:03:00,960 --> 00:03:03,000
will run wonderfully.

33
00:03:03,000 --> 00:03:07,000
 If the king is really a king and really in charge.

34
00:03:07,000 --> 00:03:10,810
 Of course that's not the case in Thailand. The king's not

35
00:03:10,810 --> 00:03:13,600
 really in charge so he does what good he can and everyone

36
00:03:13,600 --> 00:03:15,000
 seems to say he's a good person.

37
00:03:15,000 --> 00:03:20,240
 As far as I know he's a pretty good person. He just doesn't

38
00:03:20,240 --> 00:03:22,000
 have a lot of power.

39
00:03:22,000 --> 00:03:26,210
 So he said himself that you can't stop everyone from being

40
00:03:26,210 --> 00:03:28,000
...you can't make everyone good.

41
00:03:28,000 --> 00:03:33,820
 But the important thing is to stop...is to not let bad

42
00:03:33,820 --> 00:03:36,000
 people into power.

43
00:03:36,000 --> 00:03:41,360
 And that's really it. From a Buddhist point of view, that

44
00:03:41,360 --> 00:03:46,760
 of course raises him in my estimation and in many people's

45
00:03:46,760 --> 00:03:49,000
 estimation to say that.

46
00:03:49,000 --> 00:03:53,230
 The problem isn't the system. The problem is the people in

47
00:03:53,230 --> 00:03:54,000
 power.

48
00:03:54,000 --> 00:03:58,150
 If the people in power are corrupt, then it doesn't matter

49
00:03:58,150 --> 00:04:02,860
 what system of government you have. It doesn't really

50
00:04:02,860 --> 00:04:04,000
 matter.

51
00:04:04,000 --> 00:04:09,790
 So we try to put all these checks in place to keep...to try

52
00:04:09,790 --> 00:04:15,610
 and...many people try to put these checks and balances in

53
00:04:15,610 --> 00:04:19,000
 place to keep bad people from...

54
00:04:19,000 --> 00:04:25,390
 ...who are in power from exercising that power and

55
00:04:25,390 --> 00:04:32,000
 potentially to keep bad people from getting into power.

56
00:04:32,000 --> 00:04:36,820
 But it misses really the point that we have to figure out a

57
00:04:36,820 --> 00:04:40,000
 way to stop people from being bad.

58
00:04:40,000 --> 00:04:44,050
 And so yesterday I just gave a talk about what makes a

59
00:04:44,050 --> 00:04:46,000
 person a good person.

60
00:04:46,000 --> 00:04:51,010
 They have this word in Pali. 'Sapurisa'. 'Sap' means good

61
00:04:51,010 --> 00:04:54,000
 in this case. And 'Purisa' means man.

62
00:04:54,000 --> 00:04:56,980
 It's actually a masculine word referring specifically to

63
00:04:56,980 --> 00:04:58,000
 the male gender.

64
00:04:58,000 --> 00:05:04,180
 But it doesn't actually...it isn't meant to specify just

65
00:05:04,180 --> 00:05:10,860
 the male gender. It's just in a male-dominated culture like

66
00:05:10,860 --> 00:05:12,000
 India.

67
00:05:12,000 --> 00:05:18,000
 It's like it was an idiom like the word 'gentleman'.

68
00:05:18,000 --> 00:05:22,750
 Or...yeah. So it was quite a masculine. If we want to

69
00:05:22,750 --> 00:05:26,620
 translate it more gender neutral, we might say 'good fellow

70
00:05:26,620 --> 00:05:27,000
'.

71
00:05:27,000 --> 00:05:30,180
 So this is the translation I use for 'Sapurisa'. Even

72
00:05:30,180 --> 00:05:33,480
 though 'gentleman' would probably be better, 'good fellow'

73
00:05:33,480 --> 00:05:37,960
 works just as well and doesn't alienate 50% of the

74
00:05:37,960 --> 00:05:39,000
 population.

75
00:05:39,000 --> 00:05:43,880
 Isn't it 'gentleman'? Well, there are more...higher

76
00:05:43,880 --> 00:05:50,100
 percentage than that, but 50% that is eligible to become a

77
00:05:50,100 --> 00:05:52,000
 gentleman.

78
00:05:52,000 --> 00:05:55,900
 So 'Sapurisa'. There are seven dhammas that make someone a

79
00:05:55,900 --> 00:05:57,000
 'Sapurisa'.

80
00:05:57,000 --> 00:06:01,200
 And so when I give a talk in Thai, when you give a...I don

81
00:06:01,200 --> 00:06:03,000
't know if anyone does this in English.

82
00:06:03,000 --> 00:06:06,170
 I know they'll say 'namotasa' three times, but normally

83
00:06:06,170 --> 00:06:09,000
 when you give a talk in Thai, you say 'namotasa'.

84
00:06:09,000 --> 00:06:14,000
 'Bhagawato arahato sam bhutasa'. You say that three times

85
00:06:14,000 --> 00:06:17,000
 and then you say...then you repeat a verse.

86
00:06:17,000 --> 00:06:21,000
 And so I had to come up with a verse to do a 'Sapurisa'.

87
00:06:21,000 --> 00:06:25,000
 And there's a really good one in the Ratana Sutta.

88
00:06:25,000 --> 00:06:28,160
 So you bring up a verse that sort of exemplifies...it's

89
00:06:28,160 --> 00:06:30,000
 like you're...it's almost like writing a book, you know.

90
00:06:30,000 --> 00:06:33,100
 You write a book and at the beginning you put a quote. Or

91
00:06:33,100 --> 00:06:35,460
 at the beginning of every chapter some books there are

92
00:06:35,460 --> 00:06:36,000
 quotes.

93
00:06:36,000 --> 00:06:39,160
 So it's like that. And it's supposed to set the tone. It's

94
00:06:39,160 --> 00:06:40,000
 quite nice.

95
00:06:40,000 --> 00:06:44,000
 But you don't see people doing that in English so often.

96
00:06:44,000 --> 00:06:48,610
 It's the Thai way, it's the Sri Lankan way. I'm not sure

97
00:06:48,610 --> 00:06:50,000
 about other countries.

98
00:06:50,000 --> 00:06:57,210
 Cambodian and Lao...Lao shouldn't do the same thing. I'm

99
00:06:57,210 --> 00:07:00,000
 not sure about Burmese.

100
00:07:00,000 --> 00:07:07,000
 Anyway, so the verse I brought up, what I used was...

101
00:07:07,000 --> 00:07:13,500
 "Yatinte keelobattawingsitosya, chatubhiwa tebhiya sampak

102
00:07:13,500 --> 00:07:19,170
ampiyo, tatupamang sapurisangwadami yo arya satchani avichap

103
00:07:19,170 --> 00:07:24,920
asati, idampi sange rata nangpani tange tena satchina suvat

104
00:07:24,920 --> 00:07:26,000
ihoto."

105
00:07:26,000 --> 00:07:31,850
 So this uses the word...tatupamang sapurisangwadami. I call

106
00:07:31,850 --> 00:07:34,000
 this person a 'Sapurisa'.

107
00:07:34,000 --> 00:07:38,000
 That's how it connects with the topic.

108
00:07:38,000 --> 00:07:44,290
 "Yatinte keelobattawingsitosya", just as a 'indakila'. 'Ind

109
00:07:44,290 --> 00:07:48,600
akila' is an interesting word. It means a pillar, Indra's

110
00:07:48,600 --> 00:07:50,000
 pillar.

111
00:07:50,000 --> 00:07:53,490
 But I did some research quite a ways back on this. I can't

112
00:07:53,490 --> 00:07:56,000
 remember so clearly.

113
00:07:56,000 --> 00:08:02,520
 It means basically that the 'indakila' is the foundation

114
00:08:02,520 --> 00:08:06,000
 pillar for a city, I think.

115
00:08:06,000 --> 00:08:09,920
 Or it's just like in a city, like in Chiang Mai, there's an

116
00:08:09,920 --> 00:08:11,000
 'indakila'.

117
00:08:11,000 --> 00:08:15,050
 In the very center of the city, it's now a monastery called

118
00:08:15,050 --> 00:08:18,000
 Watindakin, which means it's the center of the city.

119
00:08:18,000 --> 00:08:22,250
 It's the pillar of...I'm not sure exactly how it works, but

120
00:08:22,250 --> 00:08:25,960
 the meaning is 'indakila' is the main supporting pillar of

121
00:08:25,960 --> 00:08:27,000
 a building.

122
00:08:27,000 --> 00:08:32,240
 So just as this main supporting pillar, or a deeply

123
00:08:32,240 --> 00:08:38,470
 embedded fortified Indra's pillar, is not shaken by the

124
00:08:38,470 --> 00:08:40,000
 four winds.

125
00:08:40,000 --> 00:08:47,440
 So too the 'sappurisa' who sees without doubt, or has unsh

126
00:08:47,440 --> 00:08:52,000
aken vision of the Four Noble Truths,

127
00:08:52,000 --> 00:08:55,730
 that up among 'sappurisa' and 'wadami'. Thus I say is the '

128
00:08:55,730 --> 00:09:01,640
sappurisa' who sees the Four Noble Truths without wavering

129
00:09:01,640 --> 00:09:03,000
 about them.

130
00:09:03,000 --> 00:09:09,400
 So the enlightened being. This is the treasure of the San

131
00:09:09,400 --> 00:09:16,000
gha. This is the Ratana that we call the Sangha.

132
00:09:16,000 --> 00:09:19,690
 And then he says, "By the power of this goodness may there

133
00:09:19,690 --> 00:09:21,000
 be well-being."

134
00:09:21,000 --> 00:09:27,160
 So the Ratana Sutta was actually delivered as a means of

135
00:09:27,160 --> 00:09:31,000
 bringing peace to the country.

136
00:09:31,000 --> 00:09:34,700
 It's used as a protection and when you recite it, it's

137
00:09:34,700 --> 00:09:37,000
 supposed to have power to it.

138
00:09:37,000 --> 00:09:41,870
 That's what people believe. But the power of the teaching

139
00:09:41,870 --> 00:09:53,250
 at the time was able to actually influence the angels and

140
00:09:53,250 --> 00:09:58,000
 the people and stop a lot of problems at the time.

141
00:09:58,000 --> 00:10:01,880
 Anyway, the point of the Sutta is in regards to what it

142
00:10:01,880 --> 00:10:07,000
 means to be a 'sappurisa', a good fellow or a gentleman.

143
00:10:07,000 --> 00:10:09,410
 And so there are seven things that lead one to be a good

144
00:10:09,410 --> 00:10:10,000
 fellow.

145
00:10:10,000 --> 00:10:17,290
 This is quite relevant to our meditation practice as well

146
00:10:17,290 --> 00:10:23,360
 because they have a lot to do with the benefits of

147
00:10:23,360 --> 00:10:33,000
 meditation and the focus that our meditation should take.

148
00:10:33,000 --> 00:10:43,000
 So the seven are Dhammanyuta, one knows the Dhamma.

149
00:10:43,000 --> 00:10:52,120
 Ata-nuta, one knows the meaning of the Dhamma. Ata-nuta,

150
00:10:52,120 --> 00:11:01,000
 one knows oneself. Mata-nuta, one knows moderation.

151
00:11:01,000 --> 00:11:11,480
 Gala-nuta, one knows time. Pari-san-nuta, one knows

152
00:11:11,480 --> 00:11:24,000
 communities or gatherings or groups of people.

153
00:11:24,000 --> 00:11:32,480
 Companies, who understands company. And finally, pukala-par

154
00:11:32,480 --> 00:11:37,000
o-parunyuta, one is able to discriminate between people.

155
00:11:37,000 --> 00:11:43,640
 One knows from people, one knows people from people, people

156
00:11:43,640 --> 00:11:46,000
 from other people.

157
00:11:46,000 --> 00:11:51,370
 These are the seven sappurisa dhammas. Worth remembering,

158
00:11:51,370 --> 00:11:53,000
 worth understanding.

159
00:11:53,000 --> 00:11:59,150
 The first one, dhammanyuta means to know the Dhamma. So

160
00:11:59,150 --> 00:12:03,660
 studying the Dhamma, knowing concepts like impermanent

161
00:12:03,660 --> 00:12:08,000
 suffering, non-self.

162
00:12:08,000 --> 00:12:12,680
 And the second one, ata-nuta means knowing the meaning of

163
00:12:12,680 --> 00:12:14,000
 the Dhammas.

164
00:12:14,000 --> 00:12:19,140
 And these go together. In Buddhism this is common to talk

165
00:12:19,140 --> 00:12:24,000
 about these two different things, Dhamma and Ata.

166
00:12:24,000 --> 00:12:28,910
 And important because it is common for people to know a lot

167
00:12:28,910 --> 00:12:34,630
 of Dhamma, sometimes just memorizing tracks and tracks

168
00:12:34,630 --> 00:12:37,000
 without actually understanding it.

169
00:12:37,000 --> 00:12:40,580
 You know, like novices here, they're very good at memor

170
00:12:40,580 --> 00:12:44,380
izing, but they don't have a clue what they're taught, what

171
00:12:44,380 --> 00:12:46,000
 they're memorizing.

172
00:12:46,000 --> 00:12:52,380
 They just memorize for the exams. And this happens in all

173
00:12:52,380 --> 00:12:56,000
 corners of Buddhism.

174
00:12:56,000 --> 00:13:00,330
 You have lay people who know the precepts but never keep

175
00:13:00,330 --> 00:13:06,320
 them, monks who know the precepts but never keep them. This

176
00:13:06,320 --> 00:13:11,000
 kind of thing.

177
00:13:11,000 --> 00:13:15,180
 But it's not quite, it means not understanding. And

178
00:13:15,180 --> 00:13:18,000
 understanding of course comes in different layers.

179
00:13:18,000 --> 00:13:23,200
 You can understand the rules intellectually, but

180
00:13:23,200 --> 00:13:28,800
 understanding the meaning, the true meaning actually goes a

181
00:13:28,800 --> 00:13:30,000
 lot deeper than that.

182
00:13:30,000 --> 00:13:33,020
 It's not really enough just to know things intellectually.

183
00:13:33,020 --> 00:13:35,840
 You can even, you can know the Dhamma and you can

184
00:13:35,840 --> 00:13:39,000
 understand it, but it's all only on a superficial level.

185
00:13:39,000 --> 00:13:54,140
 If you want to really understand the Dhamma, you would have

186
00:13:54,140 --> 00:13:57,000
 true understanding, it has to go deeper than that.

187
00:13:57,000 --> 00:14:00,690
 It has to come from experience. There are three kinds of

188
00:14:00,690 --> 00:14:01,000
 learning.

189
00:14:01,000 --> 00:14:07,760
 Learning from hearing, learning from thinking and learning

190
00:14:07,760 --> 00:14:13,000
 from experience, from mental development.

191
00:14:13,000 --> 00:14:21,000
 It's only this last one that gives you true understanding.

192
00:14:21,000 --> 00:14:26,470
 Anyway, so these things make one a good person. I mean this

193
00:14:26,470 --> 00:14:30,600
 is really the key in society as well. For our world to be

194
00:14:30,600 --> 00:14:34,100
 at peace, for our countries to be at peace, for our

195
00:14:34,100 --> 00:14:38,500
 societies, for our families, for even our own minds to be

196
00:14:38,500 --> 00:14:42,480
 at peace, to be a benefit to ourselves, the world, our

197
00:14:42,480 --> 00:14:46,000
 society and ourselves.

198
00:14:46,000 --> 00:14:50,720
 We need to understand, first and foremost, we need to

199
00:14:50,720 --> 00:14:53,000
 understand the truth.

200
00:14:53,000 --> 00:15:03,620
 We need to know what the truth is and we need to understand

201
00:15:03,620 --> 00:15:06,410
 it. Otherwise our actions are going to be a cause for

202
00:15:06,410 --> 00:15:10,630
 suffering because we go against reality, go against what is

203
00:15:10,630 --> 00:15:11,000
 right.

204
00:15:11,000 --> 00:15:17,000
 We want happiness but instead we cause suffering.

205
00:15:17,000 --> 00:15:21,950
 The third one ties into this and this is "attanyuta",

206
00:15:21,950 --> 00:15:24,000
 knowing yourself.

207
00:15:24,000 --> 00:15:29,620
 Of course in Buddhism we talk about non-self a lot, but it

208
00:15:29,620 --> 00:15:34,340
 isn't quite what people think it is or the emphasis

209
00:15:34,340 --> 00:15:38,000
 certainly isn't on the existence of a self.

210
00:15:38,000 --> 00:15:41,550
 But that's not the meaning here either. The meaning isn't

211
00:15:41,550 --> 00:15:45,520
 knowing your soul or something like that. The meaning is

212
00:15:45,520 --> 00:15:50,000
 knowing to not forget yourself.

213
00:15:50,000 --> 00:15:56,000
 In this idiomatic usage we say, "Don't forget yourself."

214
00:15:56,000 --> 00:16:10,530
 This means that one wasn't aware of what one was doing, one

215
00:16:10,530 --> 00:16:24,000
 didn't understand one's position in life and so on.

216
00:16:24,000 --> 00:16:29,220
 So "attanyuta" means this, to know yourself. In a

217
00:16:29,220 --> 00:16:33,060
 conventional sense it means to know your position in life,

218
00:16:33,060 --> 00:16:37,670
 your social status, your responsibilities, to your

219
00:16:37,670 --> 00:16:44,800
 superiors, to your inferiors, to your husband, your wife,

220
00:16:44,800 --> 00:16:50,000
 your children, your parents, your friends and so on.

221
00:16:50,000 --> 00:16:55,850
 All of these responsibilities and our duties towards our

222
00:16:55,850 --> 00:17:00,450
 parents and so on. This kind of thing is what keeps society

223
00:17:00,450 --> 00:17:02,000
 at peace and harmony.

224
00:17:02,000 --> 00:17:07,000
 Having relationships, having roles and responsibilities.

225
00:17:07,000 --> 00:17:11,000
 Buddhism certainly doesn't denounce this.

226
00:17:11,000 --> 00:17:14,950
 In fact it seems in general to encourage roles and

227
00:17:14,950 --> 00:17:19,340
 responsibilities and duties towards each other for the

228
00:17:19,340 --> 00:17:21,730
 purpose of creating harmony which is conducive to

229
00:17:21,730 --> 00:17:23,000
 meditation practice.

