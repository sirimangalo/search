1
00:00:00,000 --> 00:00:03,290
 How do you find balance between loving kindness and

2
00:00:03,290 --> 00:00:05,700
 compassion towards a spouse,

3
00:00:05,700 --> 00:00:11,630
 children, and other close family and non-attachment and

4
00:00:11,630 --> 00:00:14,220
 other desires that

5
00:00:14,220 --> 00:00:21,310
 arise due to these sorts of people? That's not quite clear

6
00:00:21,310 --> 00:00:23,060
 this question.

7
00:00:23,060 --> 00:00:27,070
 What are you trying to find a balance between? I guess

8
00:00:27,070 --> 00:00:29,580
 attachment and desires.

9
00:00:29,580 --> 00:00:39,300
 What do you do in regards to all of these various states?

10
00:00:39,300 --> 00:00:41,910
 Well compassion and loving kindness can go together. Comp

11
00:00:41,910 --> 00:00:43,120
assion and loving kindness

12
00:00:43,120 --> 00:00:47,290
 complement each other. Compassion is not wanting beings to

13
00:00:47,290 --> 00:00:52,520
 suffer. Love is

14
00:00:52,520 --> 00:00:55,240
 wanting them to be happy. So one is positive and the other

15
00:00:55,240 --> 00:00:56,280
 is negative. One is

16
00:00:56,280 --> 00:01:00,390
 desire for them to be free from suffering. The other is

17
00:01:00,390 --> 00:01:01,420
 desire for them

18
00:01:01,420 --> 00:01:04,990
 to be happy. Not exactly desire but the feeling of friend

19
00:01:04,990 --> 00:01:07,640
liness and non-cruelty.

20
00:01:07,640 --> 00:01:15,400
 So being friendly in the sense of acting in such a way as

21
00:01:15,400 --> 00:01:15,860
 to bring them

22
00:01:15,860 --> 00:01:21,030
 happiness and non-cruelty in the sense of acting in such a

23
00:01:21,030 --> 00:01:21,840
 way as to not bring

24
00:01:21,840 --> 00:01:26,040
 them suffering. So these two impulses complement each other

25
00:01:26,040 --> 00:01:27,180
 and they are very

26
00:01:27,180 --> 00:01:32,050
 important in family and so on. The less attachment you have

27
00:01:32,050 --> 00:01:33,180
 to other people, and

28
00:01:33,180 --> 00:01:37,310
 I know it doesn't sound nice, but the less attachment, but

29
00:01:37,310 --> 00:01:38,040
 it happens to be

30
00:01:38,040 --> 00:01:41,690
 true, that the less attachment you have towards people the

31
00:01:41,690 --> 00:01:43,320
 more peaceful your

32
00:01:43,320 --> 00:01:49,790
 life will be, your relationships will be. So the better.

33
00:01:49,790 --> 00:01:50,340
 The less

34
00:01:50,340 --> 00:01:53,540
 attachment you have to your family and loved ones the

35
00:01:53,540 --> 00:01:55,260
 better. Because

36
00:01:55,260 --> 00:01:59,840
 attachment is wanting them to make you happy. Wanting to be

37
00:01:59,840 --> 00:02:01,660
 in a certain way.

38
00:02:01,660 --> 00:02:04,810
 Grieving when they are in a different way. If your family

39
00:02:04,810 --> 00:02:06,100
 member is hurt or dies

40
00:02:06,100 --> 00:02:09,880
 it's really because it makes you sad. We don't like it.

41
00:02:09,880 --> 00:02:14,700
 That we suffer. It's

42
00:02:14,700 --> 00:02:19,670
 our own attachment to them. It doesn't make it any better

43
00:02:19,670 --> 00:02:21,100
 for them. It makes it

44
00:02:21,100 --> 00:02:23,560
 better for them when you have compassion and you try to

45
00:02:23,560 --> 00:02:24,620
 help them. So trying to

46
00:02:24,620 --> 00:02:29,330
 help people is always a good thing. To an extent until it

47
00:02:29,330 --> 00:02:29,620
 becomes

48
00:02:29,620 --> 00:02:34,180
 an attachment and becomes actually wanting them to be in

49
00:02:34,180 --> 00:02:35,260
 this way or that

50
00:02:35,260 --> 00:02:43,490
 way. I guess the most important thing is to know that

51
00:02:43,490 --> 00:02:44,500
 everyone has to find their

52
00:02:44,500 --> 00:02:49,060
 own happiness. You can't bring true happiness to people.

53
00:02:49,060 --> 00:02:49,780
 You can't truly

54
00:02:49,780 --> 00:02:52,920
 free people from suffering just by alleviating their

55
00:02:52,920 --> 00:02:54,300
 physical suffering or

56
00:02:54,300 --> 00:02:59,300
 bringing them physical pleasure or happiness. This can only

57
00:02:59,300 --> 00:03:00,100
 come from one's

58
00:03:00,100 --> 00:03:03,680
 own practice and it can only come with an example of

59
00:03:03,680 --> 00:03:05,100
 someone else practicing in

60
00:03:05,100 --> 00:03:14,560
 that way. So the best thing that you can do for in a family

61
00:03:14,560 --> 00:03:17,020
 situation and you...

62
00:03:17,020 --> 00:03:25,660
 The best thing you can... The best way for you to live even

63
00:03:25,660 --> 00:03:28,700
 in the world is to be

64
00:03:28,700 --> 00:03:31,660
 mindful and let go.

