1
00:00:00,000 --> 00:00:06,640
 Hello and welcome back to our study of the Dhammapada.

2
00:00:06,640 --> 00:00:13,550
 Today we continue on with verses 73 and 74 which read as

3
00:00:13,550 --> 00:00:14,640
 follows.

4
00:00:14,640 --> 00:00:31,510
 Asantang bhavanami caeya purikaranja pikusu Awase su isar

5
00:00:31,510 --> 00:00:33,080
 isaryang pujabh parakule suja

6
00:00:33,080 --> 00:00:52,100
 mamevakata manyantu kihi pabhajita ubo mamevati vasah asu k

7
00:00:52,100 --> 00:00:52,680
icha kichesu kismichi

8
00:00:52,680 --> 00:01:06,870
 tibhalasa sankapo icha manoja vadhatiti which means there's

9
00:01:06,870 --> 00:01:08,440
 a lot in here.

10
00:01:08,440 --> 00:01:16,280
 Asantang bhavanami caeya may they know one might wish or

11
00:01:16,280 --> 00:01:18,600
 one would wish a fool

12
00:01:18,600 --> 00:01:27,620
 would wish for people to hold them up for an, for the kind

13
00:01:27,620 --> 00:01:29,400
 of bhavana.

14
00:01:29,400 --> 00:01:31,900
 It's an interesting issue of the word bhavana. Bhavana

15
00:01:31,900 --> 00:01:33,960
 means cultivation.

16
00:01:33,960 --> 00:01:37,510
 Cultivation that doesn't exist. They would wish for people

17
00:01:37,510 --> 00:01:42,200
 to hold them up high falsely.

18
00:01:42,200 --> 00:01:48,840
 Purikaran, which means preeminence, bhikusu among monks.

19
00:01:48,840 --> 00:01:55,320
 They would wish for eminence amongst other monks.

20
00:01:55,320 --> 00:02:00,600
 So to be number one, be the top.

21
00:02:00,600 --> 00:02:06,740
 Awase su in regards to dwellings. They want to be isaryang,

22
00:02:06,740 --> 00:02:09,000
 they want to have power.

23
00:02:09,000 --> 00:02:13,110
 They want to be in charge of dwellings, in charge of the

24
00:02:13,110 --> 00:02:13,800
 monastery.

25
00:02:13,800 --> 00:02:16,200
 In charge of the monastery.

26
00:02:16,200 --> 00:02:25,000
 Puja parakule suja and they wish to be revered by other

27
00:02:25,000 --> 00:02:29,800
 families, by lay people.

28
00:02:29,800 --> 00:02:35,200
 And they think, mamevakata manyantu, may they think of it

29
00:02:35,200 --> 00:02:38,200
 as, may they think that I have done it.

30
00:02:38,200 --> 00:02:42,030
 May it be in people's minds that I have done it, that it

31
00:02:42,030 --> 00:02:46,440
 was done by me.

32
00:02:46,440 --> 00:02:52,760
 Both lay people and those who have gone forth.

33
00:02:52,760 --> 00:02:58,720
 May they be under my power in regards to, may they under my

34
00:02:58,720 --> 00:02:59,400
 influence,

35
00:02:59,400 --> 00:03:03,220
 may they rely upon me in regards to what should be done and

36
00:03:03,220 --> 00:03:04,280
 what should not be done.

37
00:03:04,280 --> 00:03:13,450
 So in regards to teachings, in regards to the exhortation,

38
00:03:13,450 --> 00:03:14,760
 may they listen to me.

39
00:03:14,760 --> 00:03:18,600
 May I be the one to tell people what to do.

40
00:03:18,600 --> 00:03:24,530
 Kismichi, every kind of, every kind of thing that should be

41
00:03:24,530 --> 00:03:25,720
 done and should not be done.

42
00:03:25,720 --> 00:03:28,720
 May they only listen to me and may my word be the final

43
00:03:28,720 --> 00:03:31,880
 word on what should and what shouldn't be done.

44
00:03:31,880 --> 00:03:37,320
 Iti mala sasa sankapo, these are the thoughts of the fool.

45
00:03:37,320 --> 00:03:46,770
 Itchama nocha wantiti and such a person's desires and conce

46
00:03:46,770 --> 00:03:49,640
it increase.

47
00:03:49,640 --> 00:03:57,080
 So this is dealing with specifically ambition and conceit.

48
00:03:57,080 --> 00:04:01,690
 These things that we can recognize in ourselves, I think

49
00:04:01,690 --> 00:04:02,920
 many of us.

50
00:04:02,920 --> 00:04:08,600
 Quite interesting for religious figures especially.

51
00:04:08,600 --> 00:04:16,920
 So story goes that there was a lay person named Chitta,

52
00:04:16,920 --> 00:04:20,680
 who was a very nice person, who listened to us,

53
00:04:20,680 --> 00:04:25,280
 a sutta or discourse by Mahanama, one of the first five

54
00:04:25,280 --> 00:04:27,560
 disciples of the Buddha,

55
00:04:27,560 --> 00:04:31,400
 and became a sotapana just by listening to the teaching and

56
00:04:31,400 --> 00:04:34,920
 by, of course, practicing in regards to it,

57
00:04:34,920 --> 00:04:40,600
 in practicing based on the teaching.

58
00:04:40,600 --> 00:04:43,930
 And as a result became, of course, a devout Buddhist and

59
00:04:43,930 --> 00:04:48,920
 later on heard the teachings of the two chief disciples.

60
00:04:48,920 --> 00:04:53,100
 And so our story begins with him listening to the two chief

61
00:04:53,100 --> 00:04:53,960
 disciples,

62
00:04:53,960 --> 00:04:59,960
 because once he became a devoted disciple of the Buddha,

63
00:04:59,960 --> 00:05:02,600
 he looked after the monks and he looked after one monk in

64
00:05:02,600 --> 00:05:04,440
 particular, Sudhamma.

65
00:05:04,440 --> 00:05:09,630
 Sudhamma came to live in the monastery near where Chitta

66
00:05:09,630 --> 00:05:10,840
 was staying.

67
00:05:10,840 --> 00:05:14,360
 So Chitta was looking after this monk, Sudhamma.

68
00:05:14,360 --> 00:05:18,980
 And as things went, Sudhamma, of course, became quite

69
00:05:18,980 --> 00:05:20,680
 content with the arrangement,

70
00:05:20,680 --> 00:05:24,360
 because Chitta would have been attentive and kind and

71
00:05:24,360 --> 00:05:29,240
 caring for the religious followers of the Buddha.

72
00:05:29,240 --> 00:05:33,300
 And so he would have gotten good food and nice dwelling and

73
00:05:33,300 --> 00:05:35,400
 all the things he needed to live as a monk

74
00:05:35,400 --> 00:05:38,760
 would have been provided for him.

75
00:05:38,760 --> 00:05:43,720
 And so you can see where this is going, maybe guess.

76
00:05:43,720 --> 00:05:48,940
 Chitta at one point listened to a talk by Sariputta, the

77
00:05:48,940 --> 00:05:52,200
 two chief disciples came and Sariputta gave this talk.

78
00:05:52,200 --> 00:05:55,620
 And again, just by listening to the talk, he became a Sakad

79
00:05:55,620 --> 00:05:58,920
agami in the second stage of enlightenment,

80
00:05:58,920 --> 00:06:03,320
 by listening and by applying it and practicing it.

81
00:06:03,320 --> 00:06:07,610
 And he was so impressed, of course, and just elated that it

82
00:06:07,610 --> 00:06:10,360
 immediately came to him that he should invite these

83
00:06:10,360 --> 00:06:14,830
 two chief disciples of the Buddha to take the meal at his

84
00:06:14,830 --> 00:06:15,720
 house.

85
00:06:15,720 --> 00:06:18,650
 So he went to them and he bowed down and he said, "Please

86
00:06:18,650 --> 00:06:23,560
 accept the meal tomorrow at my house."

87
00:06:23,560 --> 00:06:27,480
 And the two chief disciples accepted.

88
00:06:27,480 --> 00:06:31,030
 They accepted by staying silent, which is sort of the way

89
00:06:31,030 --> 00:06:32,680
 they would accept things and those

90
00:06:32,680 --> 00:06:36,160
 monks would accept things, because he didn't want to seem

91
00:06:36,160 --> 00:06:37,320
 too keen on it,

92
00:06:37,320 --> 00:06:40,420
 make it appear like you were going to be a burden. He

93
00:06:40,420 --> 00:06:44,520
 wanted to be clear, "Well, if this is what you want,

94
00:06:44,520 --> 00:06:51,160
 then we will accept." But the other monk started, I guess,

95
00:06:51,160 --> 00:06:53,000
 to have this sort of

96
00:06:53,000 --> 00:06:58,230
 green-eyed monster arise in him. And then Chitta, realizing

97
00:06:58,230 --> 00:07:00,040
 that he missed something,

98
00:07:00,040 --> 00:07:04,050
 he turned to the other elder and he said, "Please, oh,

99
00:07:04,050 --> 00:07:06,280
 please, you come as well."

100
00:07:06,280 --> 00:07:09,400
 Almost as an afterthought, like he almost forgot.

101
00:07:09,400 --> 00:07:14,600
 And Chitta, the elder Sudhama, wasn't happy about this.

102
00:07:14,600 --> 00:07:19,480
 As a result, he got very angry inside and he refused to go.

103
00:07:19,480 --> 00:07:23,200
 He refused the invitation. He said, "I'm not, you know, no,

104
00:07:23,200 --> 00:07:23,720
 thank you."

105
00:07:23,720 --> 00:07:27,100
 I said, "Oh, please come. I would like you to come. I'm

106
00:07:27,100 --> 00:07:28,200
 sorry.

107
00:07:28,200 --> 00:07:33,000
 I meant to invite you as well."

108
00:07:33,000 --> 00:07:38,360
 And again and again, he refused to go to the meal in the

109
00:07:38,360 --> 00:07:39,560
 morning.

110
00:07:39,560 --> 00:07:43,800
 So finally, Chitta, unable to persuade, the elder left.

111
00:07:43,800 --> 00:07:47,140
 And in the morning, the elder tried to stay and do his

112
00:07:47,140 --> 00:07:48,680
 meditation, but couldn't

113
00:07:48,680 --> 00:07:54,740
 get into a meditative state. So he made his way down to Ch

114
00:07:54,740 --> 00:07:58,120
itta's house.

115
00:07:58,120 --> 00:08:02,610
 His intention, he was so angry, his intention was actually

116
00:08:02,610 --> 00:08:03,640
 to find fault.

117
00:08:03,640 --> 00:08:12,270
 If you aren't a recipient of people's good deeds, then well

118
00:08:12,270 --> 00:08:12,280
,

119
00:08:12,280 --> 00:08:18,040
 let's try and belittle them. If people are...

120
00:08:18,040 --> 00:08:22,040
 This is a sign of a weak individual that they

121
00:08:22,040 --> 00:08:24,600
 have to belittle the good deeds of others and feel

122
00:08:24,600 --> 00:08:26,200
 threatened by them.

123
00:08:26,200 --> 00:08:29,960
 So he went and he was looking for fault and he criticized

124
00:08:29,960 --> 00:08:30,680
 something. He said he

125
00:08:30,680 --> 00:08:33,240
 was missing and I don't quite understand the nuance of it,

126
00:08:33,240 --> 00:08:33,880
 but he said, "You're

127
00:08:33,880 --> 00:08:37,690
 missing sesame cakes," which I guess is a real insult

128
00:08:37,690 --> 00:08:38,600
 because

129
00:08:38,600 --> 00:08:40,890
 you don't need sesame cakes. I mean, he had so many other

130
00:08:40,890 --> 00:08:41,800
 foods that I guess it

131
00:08:41,800 --> 00:08:45,480
 was meaningless that he had no sesame cakes.

132
00:08:45,480 --> 00:08:49,480
 And so Chitta turned around and called him a crow

133
00:08:49,480 --> 00:08:53,720
 and it sort of devolved from there. And the elder

134
00:08:53,720 --> 00:08:56,990
 stormed off and went to see the Buddha and told him that Ch

135
00:08:56,990 --> 00:08:57,720
itta had

136
00:08:57,720 --> 00:09:01,400
 called him a crow or something like that. And the Buddha

137
00:09:01,400 --> 00:09:02,200
 turns around and

138
00:09:02,200 --> 00:09:05,320
 blames it completely on the elder because this monk is

139
00:09:05,320 --> 00:09:05,960
 behaving like a

140
00:09:05,960 --> 00:09:11,080
 child. And he said, "You're the one who's to blame.

141
00:09:11,080 --> 00:09:14,460
 I don't blame Chitta for what he said at all." He said, "

142
00:09:14,460 --> 00:09:15,000
You,

143
00:09:15,000 --> 00:09:23,080
 some silly little monk, have insulted a true disciple."

144
00:09:23,080 --> 00:09:26,460
 Because Chitta, being even a lay person, the Buddha called

145
00:09:26,460 --> 00:09:27,640
 him a true disciple

146
00:09:27,640 --> 00:09:32,050
 because he was a saccadacam. He was actually an enlightened

147
00:09:32,050 --> 00:09:33,080
 individual.

148
00:09:33,080 --> 00:09:37,080
 And Sudhama realized his fault and realized that, "Oh,

149
00:09:37,080 --> 00:09:43,880
 the Buddha is blaming me for this. Well, I better go and

150
00:09:43,880 --> 00:09:44,600
 ask forgiveness."

151
00:09:44,600 --> 00:09:46,820
 So he goes back to Chitta to ask forgiveness. Chitta doesn

152
00:09:46,820 --> 00:09:49,480
't forgive him.

153
00:09:49,480 --> 00:09:53,000
 And he's confused as to why Chitta refuses. He goes back,

154
00:09:53,000 --> 00:09:57,910
 travels all the way alone, goes to see Chitta and asks

155
00:09:57,910 --> 00:09:58,440
 forgiveness.

156
00:09:58,440 --> 00:10:01,640
 Chitta refuses to forgive him, goes back to see the Buddha.

157
00:10:01,640 --> 00:10:02,200
 He says to the

158
00:10:02,200 --> 00:10:07,320
 Buddha, "He refuses to accept my forgiveness."

159
00:10:07,320 --> 00:10:09,600
 And the Buddha knows, knew that he wasn't going to forgive

160
00:10:09,600 --> 00:10:10,440
 him and knows why he

161
00:10:10,440 --> 00:10:15,240
 didn't forgive him and doesn't say anything. And he says,

162
00:10:15,240 --> 00:10:17,440
 "Well, let's just let him stew for a while." And Sudhama

163
00:10:17,440 --> 00:10:18,200
 goes away and he

164
00:10:18,200 --> 00:10:20,290
 doesn't know what to do. The Buddha's angry. The Buddha's

165
00:10:20,290 --> 00:10:21,160
 not angry.

166
00:10:21,160 --> 00:10:24,840
 The Buddha is disappointed in him. Not disappointed, but

167
00:10:24,840 --> 00:10:27,750
 you know, whatever you can say about a Buddha. The Buddha

168
00:10:27,750 --> 00:10:29,720
 has disparaged him.

169
00:10:29,720 --> 00:10:34,120
 Chitta has disparaged him. He can't go back. He can't stay

170
00:10:34,120 --> 00:10:34,280
 in the

171
00:10:34,280 --> 00:10:39,880
 monastery. So finally, something lets go inside of him.

172
00:10:39,880 --> 00:10:43,080
 And he goes back to the Buddha and he bows down and he ple

173
00:10:43,080 --> 00:10:44,040
ads and he begs and

174
00:10:44,040 --> 00:10:47,880
 he says, "Please, Venerable Sir, I've done a

175
00:10:47,880 --> 00:10:50,780
 terrible thing. Please, let me tell me how I can make this

176
00:10:50,780 --> 00:10:52,840
 right."

177
00:10:52,840 --> 00:10:59,160
 And the Buddha says, "Take a friend. Go back to see Chitta

178
00:10:59,160 --> 00:10:59,240
 and

179
00:10:59,240 --> 00:11:01,510
 ask forgiveness, but this time take a friend." And it's

180
00:11:01,510 --> 00:11:02,280
 interesting that he

181
00:11:02,280 --> 00:11:05,800
 says this because when he goes back, Chitta sees that there

182
00:11:05,800 --> 00:11:06,280
 are two of them

183
00:11:06,280 --> 00:11:09,880
 there and he accepts it. It's almost as though there's a

184
00:11:09,880 --> 00:11:10,200
 sense

185
00:11:10,200 --> 00:11:15,160
 that the need to make it official. Like you

186
00:11:15,160 --> 00:11:19,000
 really have to, it has to be clear that you have

187
00:11:19,000 --> 00:11:22,840
 accepted the fault in this. And so by bringing a friend,

188
00:11:22,840 --> 00:11:26,360
 there's a witness, you see, and it's going to be

189
00:11:26,360 --> 00:11:28,870
 known by the community what happened. Like there was a

190
00:11:28,870 --> 00:11:30,120
 witness to what happened

191
00:11:30,120 --> 00:11:32,980
 and Sudhama wouldn't be able to go away and say that it

192
00:11:32,980 --> 00:11:33,720
 happened like this, it

193
00:11:33,720 --> 00:11:38,760
 happened like that. It was as though Chitta was

194
00:11:40,760 --> 00:11:46,040
 required a witness because he had seen how this monk could

195
00:11:46,040 --> 00:11:47,480
 be. Anyway,

196
00:11:47,480 --> 00:11:50,840
 that's the origin story. It goes on and on after the... and

197
00:11:50,840 --> 00:11:51,160
 then

198
00:11:51,160 --> 00:11:54,800
 they go back to the Buddha and the Buddha then tells these

199
00:11:54,800 --> 00:11:55,560
 two verses.

200
00:11:55,560 --> 00:11:59,320
 And then the story goes on and on to tell about

201
00:11:59,320 --> 00:12:03,470
 I think past life stuff, but we're not so interested in

202
00:12:03,470 --> 00:12:03,880
 that.

203
00:12:03,880 --> 00:12:07,400
 Or there's more, he talks more about it anyway. What we're

204
00:12:07,400 --> 00:12:08,440
 interested in is how

205
00:12:08,440 --> 00:12:12,920
 these two verses relate to our practice.

206
00:12:12,920 --> 00:12:18,760
 Again, it's a good verse for practice because it's dealing

207
00:12:18,760 --> 00:12:19,880
 with specific mind

208
00:12:19,880 --> 00:12:24,680
 states. So we're dealing with, in this case, mainly with

209
00:12:24,680 --> 00:12:25,640
 desire and

210
00:12:25,640 --> 00:12:30,120
 conceit. But it's dealing with those desires in

211
00:12:30,120 --> 00:12:33,720
 general to become something, to be something,

212
00:12:33,720 --> 00:12:38,790
 to be special and to be seen as special. We have this sense

213
00:12:38,790 --> 00:12:41,720
 of sort of a need to

214
00:12:41,720 --> 00:12:47,720
 to be something. We want people to look at us

215
00:12:47,720 --> 00:12:53,640
 well. We want to have special people in our lives who

216
00:12:53,640 --> 00:12:56,580
 smile when we enter the room and this kind of thing, people

217
00:12:56,580 --> 00:12:56,920
 who

218
00:12:56,920 --> 00:13:01,810
 who look up to us. We tend to of course have this part of

219
00:13:01,810 --> 00:13:02,920
 us that has

220
00:13:02,920 --> 00:13:05,960
 low self-esteem. So we think little of ourselves and as a

221
00:13:05,960 --> 00:13:11,160
 result need our ego to be boosted. And when like

222
00:13:11,160 --> 00:13:16,520
 Sundama, our sense of importance is

223
00:13:16,520 --> 00:13:22,760
 challenged, jealousy arises, we get angry and upset.

224
00:13:22,760 --> 00:13:30,120
 And so the first thing is the recognition. In regards to

225
00:13:30,120 --> 00:13:30,680
 our practice,

226
00:13:30,680 --> 00:13:33,040
 the first thing for us to do is to recognize these states

227
00:13:33,040 --> 00:13:33,880
 when they come up.

228
00:13:33,880 --> 00:13:38,040
 This is where meditation starts. It starts not by trying to

229
00:13:38,040 --> 00:13:39,080
 change anything

230
00:13:39,080 --> 00:13:43,080
 or to fix anything, but just to realize that we want these

231
00:13:43,080 --> 00:13:46,520
 things. Sometimes it's in regards to people's

232
00:13:46,520 --> 00:13:50,920
 esteem, sometimes it's in regards to leadership,

233
00:13:50,920 --> 00:13:54,550
 wanting to be in control, wanting to be the head of the

234
00:13:54,550 --> 00:13:55,880
 house, wanting to be the

235
00:13:55,880 --> 00:13:59,300
 head of the monastery, wanting to be the head of the family

236
00:13:59,300 --> 00:13:59,320
,

237
00:13:59,320 --> 00:14:02,920
 head of the company, wanting people to notice

238
00:14:02,920 --> 00:14:06,420
 the good work that you've done, wanting to become something

239
00:14:06,420 --> 00:14:06,920
. It's really

240
00:14:06,920 --> 00:14:10,800
 Baba Tanha that we're talking about, which is the desire

241
00:14:10,800 --> 00:14:11,560
 for something to

242
00:14:11,560 --> 00:14:16,360
 come in the future, the desire for being or in the present

243
00:14:16,360 --> 00:14:20,680
 something you've gotten to not lose it.

244
00:14:20,680 --> 00:14:23,650
 Pudja, we want to be worshipped. We want people to pay

245
00:14:23,650 --> 00:14:24,600
 reverence. Well this is

246
00:14:24,600 --> 00:14:27,640
 more of course talking about monks. I think

247
00:14:27,640 --> 00:14:29,760
 most lay people aren't at that level where they want to be

248
00:14:29,760 --> 00:14:30,520
 worshipped, but

249
00:14:30,520 --> 00:14:36,360
 monks fall into this. It's quite obscene how

250
00:14:36,360 --> 00:14:40,040
 people can get off on this worship thing where

251
00:14:40,040 --> 00:14:42,890
 wanting to people to bow down and feeling insulted when

252
00:14:42,890 --> 00:14:43,800
 people don't bow

253
00:14:43,800 --> 00:14:47,880
 down. It's hard as a monk you get used to people.

254
00:14:47,880 --> 00:14:50,360
 If you live in a Buddhist society you get used to people b

255
00:14:50,360 --> 00:14:51,320
owing to you and

256
00:14:51,320 --> 00:14:55,050
 respecting you and being just respectful. In general,

257
00:14:55,050 --> 00:14:55,960
 people will respect

258
00:14:55,960 --> 00:15:01,240
 religious individuals in Buddhist countries.

259
00:15:01,240 --> 00:15:05,400
 Then you come to the west and people are like, "Hey,

260
00:15:05,400 --> 00:15:12,040
 how are you?" Good people asking all sorts of strange

261
00:15:12,040 --> 00:15:13,240
 questions like, "Can monks

262
00:15:13,240 --> 00:15:16,440
 get married?" I've had women come up to, you know, women

263
00:15:16,440 --> 00:15:20,780
 who get to know me and then start asking questions like, "

264
00:15:20,780 --> 00:15:21,160
Can monks

265
00:15:21,160 --> 00:15:27,640
 get married?" "Are you going to stay a monk forever?"

266
00:15:27,640 --> 00:15:31,160
 No, but I'm a little bit off track. You're just talking

267
00:15:31,160 --> 00:15:32,360
 specifically about the

268
00:15:32,360 --> 00:15:37,480
 sort of sense of, it's quite common. It's interesting

269
00:15:37,480 --> 00:15:40,760
 going from Thailand to Sri Lanka. In Sri Lanka, I think

270
00:15:40,760 --> 00:15:41,480
 people treat

271
00:15:41,480 --> 00:15:45,720
 monks a lot more reasonably.

272
00:15:45,720 --> 00:15:49,240
 And maybe reasonably is the bad word, but they treat monks

273
00:15:49,240 --> 00:15:53,240
 differently. In Thailand, there's a real sense of reverence

274
00:15:53,240 --> 00:15:53,240
.

275
00:15:53,240 --> 00:15:57,320
 Sometimes it's fake, sometimes it's just a show.

276
00:15:57,320 --> 00:16:00,430
 But when you go to Sri Lanka, it's quite a shock to have

277
00:16:00,430 --> 00:16:01,480
 people

278
00:16:01,480 --> 00:16:05,640
 treat you like an ordinary individual. Monks are treated

279
00:16:05,640 --> 00:16:05,640
 like,

280
00:16:05,640 --> 00:16:07,950
 well, sort of like how you'd imagine they would, they

281
00:16:07,950 --> 00:16:09,000
 should be, or they would have

282
00:16:09,000 --> 00:16:13,080
 been in the Buddha's time. That the Buddha himself was

283
00:16:13,080 --> 00:16:13,560
 treated

284
00:16:13,560 --> 00:16:20,120
 like a bum, you know, a beggar at times.

285
00:16:20,120 --> 00:16:24,700
 Wanting people to know the deeds that you have done, you

286
00:16:24,700 --> 00:16:25,080
 know.

287
00:16:25,080 --> 00:16:27,750
 Lay people fall into this as well. They go to monasteries

288
00:16:27,750 --> 00:16:28,920
 and they donate this or

289
00:16:28,920 --> 00:16:32,600
 they donate that. And they have a big celebration and they

290
00:16:32,600 --> 00:16:32,600
 have

291
00:16:32,600 --> 00:16:35,310
 they put themselves up at the front and they make speeches

292
00:16:35,310 --> 00:16:36,040
 and

293
00:16:36,040 --> 00:16:38,940
 they tell everyone, you know, it's hard to, it's hard, you

294
00:16:38,940 --> 00:16:39,560
 don't want to

295
00:16:39,560 --> 00:16:42,660
 sometimes, but you can see people, they somehow make it

296
00:16:42,660 --> 00:16:43,400
 slip in. Or when

297
00:16:43,400 --> 00:16:47,000
 someone else mentions, you know, mentions it for them,

298
00:16:47,000 --> 00:16:50,840
 they feel proud and puffed up. It's very dangerous.

299
00:16:50,840 --> 00:16:52,970
 I mean, we fall into it. These aren't, I'm not talking

300
00:16:52,970 --> 00:16:54,280
 about people being evil.

301
00:16:54,280 --> 00:16:58,530
 We all have these things inside of us that we have to work

302
00:16:58,530 --> 00:16:59,080
 on.

303
00:16:59,080 --> 00:17:03,170
 Wanting people to be under your power, wanting people to

304
00:17:03,170 --> 00:17:04,280
 come to you and to

305
00:17:04,280 --> 00:17:10,680
 have you as the, wanting to be the source of

306
00:17:10,680 --> 00:17:13,390
 what's right and wrong, you know, to have the last say on

307
00:17:13,390 --> 00:17:14,520
 things.

308
00:17:14,520 --> 00:17:17,860
 So you get to say what should be done and what shouldn't be

309
00:17:17,860 --> 00:17:18,120
 done

310
00:17:18,120 --> 00:17:23,250
 in the monastery. Very undemocratic. This sort of thing. I

311
00:17:23,250 --> 00:17:24,760
 suppose this is not

312
00:17:24,760 --> 00:17:27,160
 universal. Of course, some people are very good team

313
00:17:27,160 --> 00:17:29,000
 players or don't want to be in

314
00:17:29,000 --> 00:17:31,400
 charge. Some people want to hide in the

315
00:17:31,400 --> 00:17:34,520
 background. I always like working behind the scenes.

316
00:17:34,520 --> 00:17:39,800
 I think it's a very, it's so liberating to work behind the

317
00:17:39,800 --> 00:17:42,620
 scenes because you don't have to worry. You don't have to.

318
00:17:42,620 --> 00:17:43,960
 You can't get puffed up

319
00:17:43,960 --> 00:17:46,840
 in the same way. You can be proud of what you do behind the

320
00:17:46,840 --> 00:17:47,560
 scenes and you can

321
00:17:47,560 --> 00:17:51,100
 boast about it as well. But working behind the scenes is

322
00:17:51,100 --> 00:17:51,400
 always

323
00:17:51,400 --> 00:17:57,080
 more enjoyable. In Thailand, they call it

324
00:17:57,080 --> 00:18:02,820
 sticking gold on the back of the Buddha. So in Thailand,

325
00:18:02,820 --> 00:18:03,640
 they put gold plate,

326
00:18:03,640 --> 00:18:08,200
 gold foil on the Buddha and that's the saying. So it's just

327
00:18:08,200 --> 00:18:08,920
 a

328
00:18:08,920 --> 00:18:14,680
 practice. But the saying is, it's a, what do you, an idiom,

329
00:18:14,680 --> 00:18:17,140
 when you put gold on the back of the Buddha means doing

330
00:18:17,140 --> 00:18:18,200
 something without,

331
00:18:18,200 --> 00:18:20,790
 what no one knows, doing a good deed and not letting anyone

332
00:18:20,790 --> 00:18:22,280
 know about it.

333
00:18:22,280 --> 00:18:27,310
 It's actually interesting because it's not that we shouldn

334
00:18:27,310 --> 00:18:27,640
't,

335
00:18:27,640 --> 00:18:31,480
 all of this is not to say that we shouldn't be

336
00:18:31,480 --> 00:18:34,920
 famous or

337
00:18:35,560 --> 00:18:39,160
 powerful, that we shouldn't do things in front of others,

338
00:18:39,160 --> 00:18:39,880
 that we shouldn't do

339
00:18:39,880 --> 00:18:43,590
 good deeds and let others know. All of this is in fact a

340
00:18:43,590 --> 00:18:45,640
 good thing. I mean

341
00:18:45,640 --> 00:18:48,920
 especially doing good deeds and letting people know about

342
00:18:48,920 --> 00:18:49,320
 it.

343
00:18:49,320 --> 00:18:51,920
 Letting people know about your good deeds is an awesome

344
00:18:51,920 --> 00:18:52,440
 thing.

345
00:18:52,440 --> 00:18:55,290
 It's just very dangerous, you see. The question is, why are

346
00:18:55,290 --> 00:18:56,200
 you doing it?

347
00:18:56,200 --> 00:18:58,910
 Why is it a good thing? It's a good thing because it allows

348
00:18:58,910 --> 00:18:59,880
 people to

349
00:18:59,880 --> 00:19:03,320
 appreciate it. You know, Sudhamma here should have been

350
00:19:03,320 --> 00:19:07,700
 appreciating. He should have been overjoyed that Jitta was,

351
00:19:07,700 --> 00:19:08,440
 this lay person

352
00:19:08,440 --> 00:19:12,800
 was inviting these monks who would have, you know, if he

353
00:19:12,800 --> 00:19:13,240
 had

354
00:19:13,240 --> 00:19:16,900
 stayed, he could have learned something wonderful, profound

355
00:19:16,900 --> 00:19:17,560
 from these two

356
00:19:17,560 --> 00:19:23,400
 enlightened beings. Instead, even then, you know, you see

357
00:19:23,400 --> 00:19:26,040
 what Jitta, this example of Jitta is still an

358
00:19:26,040 --> 00:19:29,520
 interesting example because even if it makes people jealous

359
00:19:29,520 --> 00:19:29,560
,

360
00:19:29,560 --> 00:19:33,320
 you're often afraid to, Buddhists especially, I think,

361
00:19:33,320 --> 00:19:37,320
 can often be afraid to announce their good deeds because

362
00:19:37,320 --> 00:19:38,040
 people will feel

363
00:19:38,040 --> 00:19:42,600
 jealous or think they're just boasting or showing off.

364
00:19:42,600 --> 00:19:45,880
 Even then, it's an opportunity for people to see

365
00:19:45,880 --> 00:19:49,560
 and to come to terms with their own jealousy as Sudhamma

366
00:19:49,560 --> 00:19:52,520
 in the end had. We should never be afraid of goodness.

367
00:19:52,520 --> 00:19:55,560
 The Buddhist is a quote from the Buddha, "Mabhikawe bhaita

368
00:19:55,560 --> 00:19:56,360
 punyanam." Don't be

369
00:19:56,360 --> 00:20:01,160
 afraid of good deeds and certainly don't be afraid to

370
00:20:01,160 --> 00:20:03,640
 let others know. It doesn't mean you always have to

371
00:20:03,640 --> 00:20:07,880
 let people know. Just, it's not something that is wrong

372
00:20:07,880 --> 00:20:08,360
 because it

373
00:20:08,360 --> 00:20:13,560
 allows others to appreciate and to take it as an example.

374
00:20:13,560 --> 00:20:18,880
 It lets people feel what we call mudita. Mudita is the

375
00:20:18,880 --> 00:20:19,560
 third of the

376
00:20:19,560 --> 00:20:25,240
 Brahma bhiharas and it's an opportunity for others to do

377
00:20:25,240 --> 00:20:30,310
 good deeds themselves. Being powerful, being famous. I

378
00:20:30,310 --> 00:20:30,520
 think

379
00:20:30,520 --> 00:20:33,220
 sometimes about the internet, it's interesting how easy it

380
00:20:33,220 --> 00:20:33,880
 is to become

381
00:20:33,880 --> 00:20:37,160
 famous on the internet and I think that's an example of

382
00:20:37,160 --> 00:20:40,840
 potentially what the Buddha said asantang bhavanam

383
00:20:40,840 --> 00:20:47,160
 in verse 73 where he says this

384
00:20:47,160 --> 00:20:53,080
 false gains or he wanting people to pump you up

385
00:20:53,080 --> 00:20:56,540
 falsely. So the internet is a chance to become famous when

386
00:20:56,540 --> 00:20:57,160
 you may not even

387
00:20:57,160 --> 00:21:00,680
 deserve it. I always think about these teachers in Asia who

388
00:21:00,680 --> 00:21:02,280
 work so hard and

389
00:21:02,280 --> 00:21:05,330
 the world may never know about them because they're doing

390
00:21:05,330 --> 00:21:06,120
 their thing and

391
00:21:06,120 --> 00:21:08,930
 then all you have to do is go on YouTube and post some

392
00:21:08,930 --> 00:21:09,880
 videos and suddenly

393
00:21:09,880 --> 00:21:13,400
 you're famous.

394
00:21:13,400 --> 00:21:17,040
 There's nothing wrong with fame, there's nothing wrong with

395
00:21:17,040 --> 00:21:17,720
 power, there's

396
00:21:17,720 --> 00:21:20,760
 nothing wrong with being in charge of a monastery, there's

397
00:21:20,760 --> 00:21:23,400
 nothing wrong with people looking up to you, there's

398
00:21:23,400 --> 00:21:24,600
 nothing wrong with

399
00:21:24,600 --> 00:21:29,480
 being the boss in a company or this kind of thing.

400
00:21:29,480 --> 00:21:32,840
 It's the wishes, it's your state of mind and that's where

401
00:21:32,840 --> 00:21:33,560
 meditation

402
00:21:33,560 --> 00:21:37,320
 starts and that's where it ends. Meditation is all about

403
00:21:37,320 --> 00:21:42,680
 our state of mind. It's not usually what you do but how you

404
00:21:42,680 --> 00:21:44,520
 do it.

405
00:21:44,520 --> 00:21:49,000
 So that's what our training in meditation is.

406
00:21:49,000 --> 00:21:51,920
 We're training ourselves so that when we walk we're just

407
00:21:51,920 --> 00:21:52,440
 walking.

408
00:21:52,440 --> 00:21:56,890
 We just walk back and forth and the point is to learn how

409
00:21:56,890 --> 00:21:59,560
 to do something

410
00:21:59,560 --> 00:22:04,440
 mindfully, to learn how to do something with a clear mind.

411
00:22:04,440 --> 00:22:08,760
 The idea is you start with just taking steps,

412
00:22:08,760 --> 00:22:12,120
 okay that seems kind of silly but this is

413
00:22:12,120 --> 00:22:15,960
 where we start. If you can do this then the next step,

414
00:22:15,960 --> 00:22:19,320
 you can take it into your life and you can talk to people

415
00:22:19,320 --> 00:22:20,120
 mindfully,

416
00:22:20,120 --> 00:22:24,520
 you can listen mindfully, you can work mindfully,

417
00:22:24,520 --> 00:22:30,700
 you can live your life mindfully, you can learn how to do

418
00:22:30,700 --> 00:22:31,000
 things

419
00:22:31,000 --> 00:22:35,960
 and with a pure mind. So you can be a boss mindfully,

420
00:22:35,960 --> 00:22:39,990
 you can run a monastery mindfully, you can be famous mind

421
00:22:39,990 --> 00:22:40,840
fully.

422
00:22:40,840 --> 00:22:45,000
 All of this is possible. It takes the training of the mind.

423
00:22:45,000 --> 00:22:46,040
 Meditation is

424
00:22:46,040 --> 00:22:49,400
 simply that. It's nothing more, nothing less than the

425
00:22:49,400 --> 00:22:53,080
 training of the mind. Training of the mind to be pure, to

426
00:22:53,080 --> 00:22:53,320
 be

427
00:22:53,320 --> 00:22:57,800
 clear, to be calm, to be

428
00:22:57,800 --> 00:23:04,760
 clean. So that's how this verse relates. When we are in

429
00:23:04,760 --> 00:23:05,960
 these positions all you

430
00:23:05,960 --> 00:23:09,200
 need is a clear mind. Conceited is not something that you

431
00:23:09,200 --> 00:23:09,240
 can

432
00:23:09,240 --> 00:23:12,670
 easily be mindful of because the conceited mind is unmind

433
00:23:12,670 --> 00:23:13,480
ful. The same

434
00:23:13,480 --> 00:23:16,680
 goes with desire but desire is something you can

435
00:23:16,680 --> 00:23:20,440
 actually feel more viscerally and it's easier to be mindful

436
00:23:20,440 --> 00:23:21,080
, wanting,

437
00:23:21,080 --> 00:23:24,520
 wanting to remind yourself this is just wanting.

438
00:23:24,520 --> 00:23:27,750
 If you have conceit you can sometimes acknowledge knowing,

439
00:23:27,750 --> 00:23:28,280
 knowing that you're

440
00:23:28,280 --> 00:23:32,760
 conceited or so on. You get this feeling, this

441
00:23:32,760 --> 00:23:35,890
 puffed up feeling in your chest. You can acknowledge

442
00:23:35,890 --> 00:23:36,280
 feeling

443
00:23:36,280 --> 00:23:41,000
 feeling as well. These are not good things.

444
00:23:41,000 --> 00:23:43,800
 Conceit is as you can see what leads to jealousy,

445
00:23:43,800 --> 00:23:50,380
 which leads to anger when you aren't appreciated, greed,

446
00:23:50,380 --> 00:23:50,920
 wanting to

447
00:23:50,920 --> 00:23:53,400
 be, ambition. You know we often get questions

448
00:23:53,400 --> 00:23:56,120
 about ambition. People think, well is it wrong to have

449
00:23:56,120 --> 00:23:57,640
 ambition? I say yeah it's

450
00:23:57,640 --> 00:24:01,480
 pretty wrong to have ambition. Ambition is habit-forming.

451
00:24:01,480 --> 00:24:02,760
 It makes you

452
00:24:02,760 --> 00:24:07,960
 worry about and cling to good things,

453
00:24:07,960 --> 00:24:15,640
 cling to good states and

454
00:24:15,640 --> 00:24:18,930
 you're much better off to just live your life simply. You

455
00:24:18,930 --> 00:24:20,600
 know if you have enough,

456
00:24:20,600 --> 00:24:23,800
 if you are surviving, if you are alive and well

457
00:24:23,800 --> 00:24:28,040
 and able to develop yourself, able to better yourself,

458
00:24:28,040 --> 00:24:32,030
 able to do good things, then that's enough. You don't need

459
00:24:32,030 --> 00:24:34,200
 ambition.

460
00:24:34,200 --> 00:24:38,920
 Lao Tzu says you weaken your ambition and strengthen your

461
00:24:38,920 --> 00:24:40,280
 resolve.

462
00:24:40,280 --> 00:24:44,760
 It's an interesting saying, sounds sort of Buddhist.

463
00:24:44,760 --> 00:24:48,360
 Anyway that's all for today. Thank you for tuning in

464
00:24:48,360 --> 00:24:53,590
 and we'll try and keep this up and get through the Dhammap

465
00:24:53,590 --> 00:24:54,600
ada as much and

466
00:24:54,600 --> 00:24:57,660
 as quickly as we can. Thank you. Wishing you all peace,

467
00:24:57,660 --> 00:25:01,000
 happiness and freedom.

