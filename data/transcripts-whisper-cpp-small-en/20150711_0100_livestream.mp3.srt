1
00:00:00,000 --> 00:00:06,260
 Okay, good evening. I'm broadcasting from Stony Creek,

2
00:00:06,260 --> 00:00:07,000
 Ontario, July 10th, 2015. Following

3
00:00:07,000 --> 00:00:19,010
 the book, "Buddhavachana" by Venerable Bhimika. For July 10

4
00:00:19,010 --> 00:00:19,960
th, we have the four brahbhoiharas

5
00:00:19,960 --> 00:00:27,010
 and you can read. I encourage you to read the passage, but

6
00:00:27,010 --> 00:00:29,520
 I'm not going to read it

7
00:00:30,520 --> 00:00:34,150
 to you. Brahma-viharas are a part of Buddhism that is

8
00:00:34,150 --> 00:00:37,520
 either undervalued or overvalued too

9
00:00:37,520 --> 00:00:45,340
 much. So there's people who undervalue it and I think

10
00:00:45,340 --> 00:00:50,360
 people who overvalue it as well.

11
00:00:50,360 --> 00:00:56,610
 There's some people, compassion for example, some Buddhists

12
00:00:56,610 --> 00:00:59,360
, compassion is central to

13
00:00:59,680 --> 00:01:04,190
 control. And they put too much emphasis on obsessing over

14
00:01:04,190 --> 00:01:06,680
 other people I would say, for

15
00:01:06,680 --> 00:01:13,900
 example. Or they put too much emphasis, they put a lot of

16
00:01:13,900 --> 00:01:17,640
 emphasis on equanimity. I mean

17
00:01:17,640 --> 00:01:22,480
 there's disadvantages to obsessing over these. Love, metta,

18
00:01:22,480 --> 00:01:24,640
 metta becomes a very primary

19
00:01:25,080 --> 00:01:28,290
 practice for people and that can be problematic. I mean

20
00:01:28,290 --> 00:01:30,640
 there's the problems that are mentioned

21
00:01:30,640 --> 00:01:33,520
 in this passage, which by the way isn't Buddhavachana, it

22
00:01:33,520 --> 00:01:35,880
 isn't actually the words of the Buddha.

23
00:01:35,880 --> 00:01:41,270
 This is as far as I know from the "Visuddhimagga" which is

24
00:01:41,270 --> 00:01:42,880
 a later text. But the disadvantages

25
00:01:42,880 --> 00:01:49,600
 are, in mentions, seem reasonable. And love can lead to

26
00:01:49,600 --> 00:01:53,760
 partiality, affection, and love

27
00:01:54,760 --> 00:01:58,750
 can lead to partiality, affection, attachment. Compassion

28
00:01:58,750 --> 00:02:01,760
 can lead to sadness, feeling upset,

29
00:02:01,760 --> 00:02:07,470
 the suffering of others. Joy, appreciation of others can

30
00:02:07,470 --> 00:02:09,760
 lead to exuberance, jumping

31
00:02:09,760 --> 00:02:17,380
 around for joy. Celebration, that celebration in the sense

32
00:02:17,380 --> 00:02:21,640
 of getting carried away, jumping

33
00:02:22,680 --> 00:02:27,200
 up and down, yelling and screaming. And equanimity can get

34
00:02:27,200 --> 00:02:29,680
 carried away to a point where one

35
00:02:29,680 --> 00:02:34,610
 doesn't care. Doesn't care enough to meditate, doesn't care

36
00:02:34,610 --> 00:02:38,680
 enough to do good deeds. That's

37
00:02:38,680 --> 00:02:44,300
 one's life going by. These are dangers. But it can also

38
00:02:44,300 --> 00:02:45,720
 become a problem to obsess over,

39
00:02:49,080 --> 00:02:53,430
 simply to put too much more focus on these things. On the

40
00:02:53,430 --> 00:02:55,120
 other hand, you hear people

41
00:02:55,120 --> 00:02:59,950
 talk about with many things in Buddhism. This included how

42
00:02:59,950 --> 00:03:02,120
 it's not necessary. And you should

43
00:03:02,120 --> 00:03:08,270
 just focus solely on insight meditation. That's a problem

44
00:03:08,270 --> 00:03:11,360
 in our tradition, I think. A lot

45
00:03:11,360 --> 00:03:16,170
 of people will focus simply on insight meditation. And

46
00:03:16,170 --> 00:03:18,360
 theoretically it works. I mean, you don't

47
00:03:18,360 --> 00:03:22,440
 need morality, honestly. You don't need anything if you're

48
00:03:22,440 --> 00:03:25,360
 mindful. But we're not machines.

49
00:03:25,360 --> 00:03:32,360
 We're not a robot that you can just program to be mindful.

50
00:03:32,360 --> 00:03:32,360
 We're very complicated beings.

51
00:03:32,360 --> 00:03:40,750
 And it's very important that we approach the Buddha's

52
00:03:40,750 --> 00:03:45,120
 practice from the position that we

53
00:03:45,120 --> 00:03:49,530
 find ourselves in. What are our problems? What are our

54
00:03:49,530 --> 00:03:52,120
 difficulties? What is causing

55
00:03:52,120 --> 00:03:58,350
 us suffering? What are we doing that we can learn about,

56
00:03:58,350 --> 00:04:00,720
 that we can change? And so there's

57
00:04:00,720 --> 00:04:08,200
 a lot of complication in the practice. We have to deal with

58
00:04:08,200 --> 00:04:08,920
... For instance, today we

59
00:04:11,760 --> 00:04:16,240
 traveled to Toronto to a meeting for this new Buddhist cha

60
00:04:16,240 --> 00:04:18,760
plaincy program that's coming

61
00:04:18,760 --> 00:04:25,240
 up at the University of Toronto. And totally unnecessary.

62
00:04:25,240 --> 00:04:27,880
 There's no need or reason for

63
00:04:27,880 --> 00:04:35,180
 our tradition, from the point of view of our tradition, to

64
00:04:35,180 --> 00:04:38,320
 go out of their way to work

65
00:04:38,320 --> 00:04:44,030
 in a prison or a hospital to help people. But this speaks

66
00:04:44,030 --> 00:04:45,320
 to our humanity. I mean, we're

67
00:04:45,320 --> 00:04:52,380
 beings with emotions and it's part of who we are to act and

68
00:04:52,380 --> 00:04:54,120
 interact. And acting and

69
00:05:05,040 --> 00:05:08,330
 interacting are not problems. Arahants also act and

70
00:05:08,330 --> 00:05:12,040
 interact. It's just following their

71
00:05:12,040 --> 00:05:19,040
 nature. It's the reacting that we have to worry about. But

72
00:05:19,040 --> 00:05:19,040
 we shouldn't try to remove

73
00:05:19,040 --> 00:05:27,420
 ourselves necessarily from our lives. Our lives should

74
00:05:27,420 --> 00:05:31,640
 become a part of the practice.

75
00:05:33,520 --> 00:05:38,290
 And the Four Brahmal Iharas are a very good tool for that.

76
00:05:38,290 --> 00:05:39,880
 For the times when we get carried

77
00:05:39,880 --> 00:05:44,290
 away and we can think of which Brahmal Ihara is useful for

78
00:05:44,290 --> 00:05:46,880
 us. When we're very angry.

79
00:05:46,880 --> 00:05:54,180
 The Mita is quite useful. When we're very greedy,

80
00:05:54,180 --> 00:05:59,520
 compassion and appreciation are very

81
00:05:59,520 --> 00:06:04,210
 helpful for doing we're jealous. Equanimity, all four of

82
00:06:04,210 --> 00:06:06,520
 these are quite useful in our

83
00:06:06,520 --> 00:06:14,520
 emotions of the wellness.

84
00:06:21,520 --> 00:06:25,580
 We shouldn't discard this. We don't necessarily need to

85
00:06:25,580 --> 00:06:28,520
 make it our primary meditation practice,

86
00:06:28,520 --> 00:06:38,630
 although even that is reasonable. There's great benefits to

87
00:06:38,630 --> 00:06:40,520
 the Four Brahmal Ihara's.

88
00:06:40,520 --> 00:06:44,910
 Not the least of which is leading one to be born in the

89
00:06:44,910 --> 00:06:47,520
 Brahma realms. But it can lead

90
00:06:47,520 --> 00:06:52,410
 directly to enlightenment. It can help, it can support your

91
00:06:52,410 --> 00:06:54,520
 mind, support your state

92
00:06:54,520 --> 00:06:59,540
 of mind. Certainly it's a great help. But we should

93
00:06:59,540 --> 00:07:01,520
 understand the benefit. We should

94
00:07:01,520 --> 00:07:06,740
 be able to see the purpose and the benefit of the Four Brah

95
00:07:06,740 --> 00:07:09,520
mal Ihara's and incorporate

96
00:07:10,520 --> 00:07:15,110
 them into our practice. Just as we incorporate charity and

97
00:07:15,110 --> 00:07:17,520
 morality and renunciation.

98
00:07:17,520 --> 00:07:32,360
 There's a lot of...we have to be comprehensive in our

99
00:07:32,360 --> 00:07:33,520
 practice. We have to know those parts

100
00:07:36,520 --> 00:07:42,520
 of our lives that require love so that we can have love.

101
00:07:42,520 --> 00:07:43,520
 There was one monk at this

102
00:07:43,520 --> 00:07:47,560
 round table that we had today about the chaplains program

103
00:07:47,560 --> 00:07:49,520
 talking about meditation and whether

104
00:07:49,520 --> 00:07:55,520
 we should require chaplains to be proficient in meditation.

105
00:07:55,520 --> 00:07:56,520
 The consensus was that...well,

106
00:07:56,520 --> 00:08:02,290
 there wasn't a consensus. I was pushing, I guess. A couple

107
00:08:02,290 --> 00:08:04,520
 of us had made points that

108
00:08:05,520 --> 00:08:09,110
 just from a scholarly point of view you can't make it a

109
00:08:09,110 --> 00:08:12,520
 requirement to complete a university

110
00:08:12,520 --> 00:08:17,470
 course, a university course program. But it would be

111
00:08:17,470 --> 00:08:20,520
 something that would be expected

112
00:08:20,520 --> 00:08:25,560
 anyway of any Buddhist chaplain. But the point was that I

113
00:08:25,560 --> 00:08:28,520
 just wanted to talk about this one

114
00:08:29,520 --> 00:08:33,380
 monk, this Mahayana monk, who was a part of the

115
00:08:33,380 --> 00:08:36,520
 organization and hosted the meeting today.

116
00:08:36,520 --> 00:08:41,160
 He was just wonderful. He had such love and a smile and a

117
00:08:41,160 --> 00:08:43,520
 space you could tell that he

118
00:08:43,520 --> 00:08:48,550
 had some great practice with the Brahmaprihara's compassion

119
00:08:48,550 --> 00:08:51,520
. They focused more on compassion.

120
00:08:51,520 --> 00:08:55,090
 But he didn't just have compassion. He clearly had love and

121
00:08:55,090 --> 00:08:57,520
 he used it in his life. He used

122
00:08:58,520 --> 00:09:03,360
 it in his interactions with all of us. You could feel the

123
00:09:03,360 --> 00:09:05,520
 love coming from the most elderly

124
00:09:05,520 --> 00:09:16,060
 monk. And he was not judgmental, not argumentative. This

125
00:09:16,060 --> 00:09:21,520
 was the kind of thing that, the benefit

126
00:09:22,520 --> 00:09:28,870
 of Brahmaprihara is something to keep in mind. They keep us

127
00:09:28,870 --> 00:09:29,520
 from conflict and they keep problems

128
00:09:29,520 --> 00:09:35,410
 from cropping up. They create, they're like the grease in

129
00:09:35,410 --> 00:09:38,520
 the joints, in the gears. It

130
00:09:38,520 --> 00:09:44,160
 keeps the machine going smoothly. They keep us from having

131
00:09:44,160 --> 00:09:46,520
 enemies, keep us from obsessing

132
00:09:51,520 --> 00:09:57,170
 over others when part of the Brahmaprihara's is about

133
00:09:57,170 --> 00:10:01,520
 letting go as well. Not trying to

134
00:10:01,520 --> 00:10:06,760
 fix people's problems for them, but providing them the

135
00:10:06,760 --> 00:10:10,520
 support so that they can help themselves.

136
00:10:11,520 --> 00:10:21,220
 It's the answer to what we can do to help others start with

137
00:10:21,220 --> 00:10:23,520
 love, have love for them,

138
00:10:23,520 --> 00:10:27,520
 encourage them because encouragement is such a great gift.

139
00:10:27,520 --> 00:10:35,520
 Providing people with something,

140
00:10:36,520 --> 00:10:43,830
 words or actions and just a smile that reassures them and

141
00:10:43,830 --> 00:10:47,520
 propels them and keeps them and steadies

142
00:10:47,520 --> 00:10:54,430
 them on the right path. And so these Brahmaprihara's are

143
00:10:54,430 --> 00:10:57,520
 considered to be a protection, they protect

144
00:10:58,520 --> 00:11:03,650
 our insight practice, protect our quest, protect our minds

145
00:11:03,650 --> 00:11:06,520
 on the quest for enlightenment, protect

146
00:11:06,520 --> 00:11:10,940
 our minds from our own defilements, but also from the wrath

147
00:11:10,940 --> 00:11:15,520
 and the scorn and the conflict

148
00:11:15,520 --> 00:11:20,020
 that might arise with others. So something worth studying,

149
00:11:20,020 --> 00:11:21,520
 definitely this passage is

150
00:11:21,520 --> 00:11:24,750
 one that I come back to often, often bring up for people

151
00:11:24,750 --> 00:11:26,520
 when they ask about these four

152
00:11:27,520 --> 00:11:31,040
 questions, what is the characteristic of the four Brahmapri

153
00:11:31,040 --> 00:11:33,520
hara's, what are their functions,

154
00:11:33,520 --> 00:11:37,630
 how they manifested and what is the proximate cause, and

155
00:11:37,630 --> 00:11:39,520
 the difference between them when

156
00:11:39,520 --> 00:11:43,390
 they succeed and when they fail because it's easy to take

157
00:11:43,390 --> 00:11:47,520
 them too far. That's when we let

158
00:11:47,520 --> 00:11:54,440
 our defilements lead the way. So I'm not going to go into

159
00:11:54,440 --> 00:11:55,520
 detail about the Brahmaprihara's

160
00:11:56,520 --> 00:12:00,520
 but just a little bit of food for thought about them,

161
00:12:00,520 --> 00:12:02,520
 something that we can bring to practice

162
00:12:02,520 --> 00:12:07,720
 in our daily life as an adjunct, as a complement to our

163
00:12:07,720 --> 00:12:11,520
 insight practice. Thanks for tuning

164
00:12:11,520 --> 00:12:13,520
 in, keep practicing.

