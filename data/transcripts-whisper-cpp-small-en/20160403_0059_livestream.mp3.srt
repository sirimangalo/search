1
00:00:00,000 --> 00:00:05,000
 Good evening everyone.

2
00:00:05,000 --> 00:00:12,000
 Broadcasting Live, April 2nd.

3
00:00:12,000 --> 00:00:18,000
 Today's quote is about the unskillful.

4
00:00:18,000 --> 00:00:23,030
 It comes from a sutta that talks about both the skillful

5
00:00:23,030 --> 00:00:25,000
 and the unskillful.

6
00:00:25,000 --> 00:00:38,000
 In fact, it's a bit of a paraphrase, I think, or a summary.

7
00:00:38,000 --> 00:00:42,000
 I'm actually not sure if this is the proper...

8
00:00:42,000 --> 00:00:51,000
 It actually is from this part of the scene.

9
00:00:51,000 --> 00:00:57,000
 Looks like it.

10
00:00:57,000 --> 00:01:00,000
 Anyway, the sutta starts off with the Buddha saying there

11
00:01:00,000 --> 00:01:05,000
 are three roots of unhorsingness.

12
00:01:05,000 --> 00:01:11,000
 Dhinimānibhika vai akusu mūlāni.

13
00:01:11,000 --> 00:01:19,000
 The roots of akusu.

14
00:01:19,000 --> 00:01:24,000
 Akusu is usually translated as unwholesome.

15
00:01:24,000 --> 00:01:28,000
 Maybe that's a better translation here.

16
00:01:28,000 --> 00:01:30,000
 Because it's not...

17
00:01:30,000 --> 00:01:33,870
 Well, unskillful is a part of it, but it doesn't quite have

18
00:01:33,870 --> 00:01:37,000
 the same feeling as unwholesome.

19
00:01:37,000 --> 00:01:41,000
 Unskillful is too cold and technical.

20
00:01:41,000 --> 00:01:43,520
 But it's useful because that's really what we're talking

21
00:01:43,520 --> 00:01:44,000
 about.

22
00:01:44,000 --> 00:01:47,000
 What is unwholesome? What is bad?

23
00:01:47,000 --> 00:01:50,000
 It's really just a matter of doing something unskillful,

24
00:01:50,000 --> 00:01:54,150
 doing something that goes against your own interests,

25
00:01:54,150 --> 00:01:55,000
 really.

26
00:01:55,000 --> 00:01:58,000
 There are three things that...

27
00:01:58,000 --> 00:01:59,000
 The basis...

28
00:01:59,000 --> 00:02:03,910
 Three bases of roots of things that go against our own

29
00:02:03,910 --> 00:02:05,000
 interests

30
00:02:05,000 --> 00:02:09,000
 that cause us to do and say and think things

31
00:02:09,000 --> 00:02:15,000
 that are against whatever interests we may have.

32
00:02:15,000 --> 00:02:20,000
 That cause us to suffer,

33
00:02:20,000 --> 00:02:23,000
 lead us to have improper interests

34
00:02:23,000 --> 00:02:28,000
 and interests that are self-conflicting in the sense

35
00:02:28,000 --> 00:02:33,020
 we want them to bear positive fruit, they end up bearing

36
00:02:33,020 --> 00:02:34,000
 negative fruit.

37
00:02:34,000 --> 00:02:40,000
 The three roots are dosa, loba dosa, and moha.

38
00:02:40,000 --> 00:02:46,000
 Loba means desire to get wanting.

39
00:02:46,000 --> 00:02:51,000
 Dosa means desire to harm or to hurt

40
00:02:51,000 --> 00:02:57,000
 or basically to not get, desire to destroy, to eradicate,

41
00:02:57,000 --> 00:03:00,620
 to do away with something or change something that you don

42
00:03:00,620 --> 00:03:03,000
't like, disliking.

43
00:03:03,000 --> 00:03:11,000
 Moha is delusion, so anything like arrogance or conceit,

44
00:03:11,000 --> 00:03:17,020
 self-righteousness, wrong view, ignorance, confusion, doubt

45
00:03:17,020 --> 00:03:19,000
, all these things.

46
00:03:19,000 --> 00:03:25,000
 Under the mind that is muddled, moha is like muddled,

47
00:03:25,000 --> 00:03:34,000
 it's probably the same ancient root.

48
00:03:34,000 --> 00:03:39,000
 So then he talks about these three in detail, he says loba,

49
00:03:39,000 --> 00:03:43,000
 loba is a kusulamula,

50
00:03:43,000 --> 00:03:47,000
 whatever greed there may be that has...

51
00:03:47,000 --> 00:03:55,380
 Ya dapilika vai loba, whatever greed there is that is the

52
00:03:55,380 --> 00:03:56,000
 root of unwholesome.

53
00:03:56,000 --> 00:04:06,000
 Whatever the greedy one should do with body, speech or mind

54
00:04:06,000 --> 00:04:06,000
,

55
00:04:06,000 --> 00:04:18,000
 or no, abhisankaroti should intend not to do right, or not

56
00:04:18,000 --> 00:04:18,000
 to do.

57
00:04:18,000 --> 00:04:23,990
 That, yeah, right, with body, speech and mind, that is unwh

58
00:04:23,990 --> 00:04:27,000
olesome.

59
00:04:27,000 --> 00:04:33,000
 So the crucial, and this should be fairly common knowledge

60
00:04:33,000 --> 00:04:35,000
 to our community by now,

61
00:04:35,000 --> 00:04:44,000
 but this is a crucial understanding of good and evil,

62
00:04:44,000 --> 00:04:50,000
 how they exist in the mind, or they're based on the mind,

63
00:04:50,000 --> 00:04:54,610
 they're based on the quality of the mind, not on the acts

64
00:04:54,610 --> 00:04:56,000
 themselves.

65
00:04:56,000 --> 00:05:01,770
 And hence the, as I say many times, the importance of

66
00:05:01,770 --> 00:05:03,000
 meditation

67
00:05:03,000 --> 00:05:11,000
 in terms of changing these aspects of our minds,

68
00:05:11,000 --> 00:05:18,000
 our reactions to things, to cultivate habits of objectivity

69
00:05:18,000 --> 00:05:23,000
 or simplicity.

70
00:05:23,000 --> 00:05:26,990
 It's very simple to see things as they are, it's not easy

71
00:05:26,990 --> 00:05:28,000
 but it's simple.

72
00:05:28,000 --> 00:05:33,330
 If you can get to the point, to that simple state of just

73
00:05:33,330 --> 00:05:34,000
 seeing things as they are,

74
00:05:34,000 --> 00:05:39,540
 which seems actually kind of silly because we're so used to

75
00:05:39,540 --> 00:05:41,000
 complicating things

76
00:05:41,000 --> 00:05:44,810
 and we think truth must be complicated, but it's really not

77
00:05:44,810 --> 00:05:45,000
.

78
00:05:45,000 --> 00:05:49,360
 Truth is when seeing is seeing, it's such a powerful

79
00:05:49,360 --> 00:05:51,000
 statement of the Buddha.

80
00:05:51,000 --> 00:06:01,000
 It sounds like a child, it sounds childlike.

81
00:06:01,000 --> 00:06:07,850
 But the idea is when seeing is just seeing, then there's no

82
00:06:07,850 --> 00:06:11,000
 root for unwholesomeness.

83
00:06:11,000 --> 00:06:13,000
 There's no bad intention.

84
00:06:13,000 --> 00:06:17,000
 To say that you then could do bad things or say bad things

85
00:06:17,000 --> 00:06:21,000
 or even think bad things is not possible, it's not correct.

86
00:06:21,000 --> 00:06:25,440
 Even though people might still blame you and probably will

87
00:06:25,440 --> 00:06:27,000
 still blame you.

88
00:06:27,000 --> 00:06:33,110
 The blame of others is not the reason for something being

89
00:06:33,110 --> 00:06:35,000
 deemed wrong.

90
00:06:35,000 --> 00:06:38,990
 The question is what makes something wrong or what makes

91
00:06:38,990 --> 00:06:40,000
 you right?

92
00:06:40,000 --> 00:06:43,590
 And so Buddhism only goes so far as to say it's right if it

93
00:06:43,590 --> 00:06:46,000
 actually does conduce to proper

94
00:06:46,000 --> 00:06:51,000
 and to coherent goals.

95
00:06:51,000 --> 00:06:54,000
 Greed, anger and delusion don't do that.

96
00:06:54,000 --> 00:06:57,600
 With greed you want to be happy, with anger you want to be

97
00:06:57,600 --> 00:06:59,000
 free from suffering.

98
00:06:59,000 --> 00:07:04,000
 With delusion you don't know what you want.

99
00:07:04,000 --> 00:07:07,000
 But because greed and anger and delusion don't lead to

100
00:07:07,000 --> 00:07:10,000
 getting what you want,

101
00:07:10,000 --> 00:07:14,950
 instead they lead to getting what you don't want, not

102
00:07:14,950 --> 00:07:17,000
 getting what you want.

103
00:07:17,000 --> 00:07:21,070
 Therefore they're unskillful, they're unhelpful,

104
00:07:21,070 --> 00:07:23,000
 problematic,

105
00:07:23,000 --> 00:07:32,130
 causing, inherently causing problems, inherently problem-ca

106
00:07:32,130 --> 00:07:35,000
using.

107
00:07:35,000 --> 00:07:40,000
 So these are the three, there's only three basis.

108
00:07:40,000 --> 00:07:44,000
 So when we meditate we also meditate directly on them.

109
00:07:44,000 --> 00:07:47,540
 But I said when your anger is in the mind you know this

110
00:07:47,540 --> 00:07:49,000
 anger is in the mind.

111
00:07:49,000 --> 00:07:53,000
 Simply say to ourselves, "Angry, angry."

112
00:07:53,000 --> 00:07:57,000
 Not thinking of it as anything else.

113
00:07:57,000 --> 00:08:00,000
 You want something wanting, wanting.

114
00:08:00,000 --> 00:08:02,920
 When you're confused or deluded you can say, "Confused,

115
00:08:02,920 --> 00:08:04,000
 confused."

116
00:08:04,000 --> 00:08:21,630
 Doubting, doubting, anxious, whatever the kind of delusion

117
00:08:21,630 --> 00:08:24,000
 it is.

118
00:08:24,000 --> 00:08:27,340
 And then he makes a simile, he talks a little bit more, but

119
00:08:27,340 --> 00:08:31,000
 in summary he gives a simile of a tree.

120
00:08:31,000 --> 00:08:34,330
 Suppose there was a tree that was choked and enveloped by

121
00:08:34,330 --> 00:08:38,000
 three Malwa creepers.

122
00:08:38,000 --> 00:08:43,940
 You know anything about vines? Our country doesn't have

123
00:08:43,940 --> 00:08:45,000
 vines really,

124
00:08:45,000 --> 00:08:49,420
 but other countries they have vines that strangle and can

125
00:08:49,420 --> 00:08:52,000
 actually take down trees.

126
00:08:52,000 --> 00:08:55,550
 If you go into the rainforest it's amazing these thick

127
00:08:55,550 --> 00:09:00,000
 vines that just cover the tree.

128
00:09:00,000 --> 00:09:03,620
 And it's just from like a seed. A bird goes up into the

129
00:09:03,620 --> 00:09:06,000
 tree and defecates out a seed.

130
00:09:06,000 --> 00:09:12,440
 And that one little seed ends up killing a huge, like,

131
00:09:12,440 --> 00:09:15,000
 hundred foot tree.

132
00:09:15,000 --> 00:09:21,000
 I guess usually it doesn't kill the tree, but it can.

133
00:09:21,000 --> 00:09:25,270
 And so here he talks about these Malwa creepers that if the

134
00:09:25,270 --> 00:09:29,000
 tree was choked with the creepers

135
00:09:29,000 --> 00:09:36,000
 it would meet with calamity with disaster.

136
00:09:36,000 --> 00:09:40,080
 This is like, remember the last night or the recent one, we

137
00:09:40,080 --> 00:09:41,000
 said the mind is luminous

138
00:09:41,000 --> 00:09:45,000
 but the defilements visit the mind.

139
00:09:45,000 --> 00:09:48,710
 So this is the, he's giving the same teaching in another

140
00:09:48,710 --> 00:09:49,000
 way.

141
00:09:49,000 --> 00:09:51,890
 He says, "There's nothing wrong with the tree, but it's

142
00:09:51,890 --> 00:09:53,000
 these creepers."

143
00:09:53,000 --> 00:09:58,000
 When the creepers enter into the tree they strangle it.

144
00:09:58,000 --> 00:10:00,000
 And so our desires are like that.

145
00:10:00,000 --> 00:10:05,000
 Desires are unwholesome roots.

146
00:10:05,000 --> 00:10:10,000
 They have like a stranglehold and we become slaves.

147
00:10:10,000 --> 00:10:13,000
 We become slaves to our defilements, that's the idea.

148
00:10:13,000 --> 00:10:20,500
 We can't act other than wanting certain things and angry

149
00:10:20,500 --> 00:10:22,000
 about certain other things,

150
00:10:22,000 --> 00:10:28,000
 or getting confused or deluded about other certain things.

151
00:10:28,000 --> 00:10:30,000
 And we're controlled by this.

152
00:10:30,000 --> 00:10:34,000
 Our actions, our speech, and our thoughts are under control

153
00:10:34,000 --> 00:10:37,000
 of these habits basically.

154
00:10:37,000 --> 00:10:40,360
 We've just built up habits, reinforced them with our

155
00:10:40,360 --> 00:10:41,000
 emotions,

156
00:10:41,000 --> 00:11:02,000
 the power of our mental activity.

157
00:11:02,000 --> 00:11:05,000
 We're choked like a tree by the creepers.

158
00:11:05,000 --> 00:11:12,140
 And then you've got another tree and he does the opposite,

159
00:11:12,140 --> 00:11:13,000
 which is common in the suttas.

160
00:11:13,000 --> 00:11:16,850
 The first half is about the unwholesome and that's I think

161
00:11:16,850 --> 00:11:18,000
 where our quote is from.

162
00:11:18,000 --> 00:11:22,190
 The second half is about a person who has abandoned unwh

163
00:11:22,190 --> 00:11:26,000
olesome qualities born of greed, anger, and delusion.

164
00:11:26,000 --> 00:11:30,000
 Cut them off at the root, made them like a palm stump,

165
00:11:30,000 --> 00:11:32,000
 obliterated them

166
00:11:32,000 --> 00:11:36,000
 so that they are no more subject to future arising.

167
00:11:36,000 --> 00:11:43,000
 I'm going to find this one.

168
00:11:43,000 --> 00:11:51,620
 "Evarupasa bhikkha vipugalasa lobhajapapaka akhusadadamah b

169
00:11:51,620 --> 00:11:52,000
hahina"

170
00:11:52,000 --> 00:11:57,000
 Bhahina is abandoned, right? Or eliminated, destroyed.

171
00:11:57,000 --> 00:12:02,000
 "Uchinnamula" is cut the root. "Mula" again is root.

172
00:12:02,000 --> 00:12:08,000
 This idea of cutting the root is important in Buddhism

173
00:12:08,000 --> 00:12:10,000
 because some people will say,

174
00:12:10,000 --> 00:12:13,510
 "Well, enlightenment is Buddhism is to be free from defile

175
00:12:13,510 --> 00:12:16,000
ment, free from craving."

176
00:12:16,000 --> 00:12:19,000
 No craving, no suffering. But that's wrong.

177
00:12:19,000 --> 00:12:21,630
 We see the manga points out that that's wrong because, well

178
00:12:21,630 --> 00:12:23,000
, we don't always have cravings.

179
00:12:23,000 --> 00:12:25,890
 So then at times when I don't have craving, does that mean

180
00:12:25,890 --> 00:12:33,000
 I'm enlightened? No, it doesn't.

181
00:12:33,000 --> 00:12:38,990
 And it's important because there are people who feel like

182
00:12:38,990 --> 00:12:39,000
 they're,

183
00:12:39,000 --> 00:12:44,000
 "Maybe I'm enlightened because I don't feel any greed."

184
00:12:44,000 --> 00:12:56,790
 There was a story of this ascetic who had such powerful

185
00:12:56,790 --> 00:13:01,000
 meditation that he could fly through the air.

186
00:13:01,000 --> 00:13:06,880
 And then one day he went to stay with this king and every

187
00:13:06,880 --> 00:13:10,000
 day he would go in for alms.

188
00:13:10,000 --> 00:13:12,930
 And this king invited him to stay at the, it was one of the

189
00:13:12,930 --> 00:13:14,000
 jataka ascetic.

190
00:13:14,000 --> 00:13:20,000
 And the king was so happy to have this ascetic there,

191
00:13:20,000 --> 00:13:25,120
 a very powerful ascetic who was like he was enlightened,

192
00:13:25,120 --> 00:13:26,000
 right?

193
00:13:26,000 --> 00:13:29,900
 But one day the king had to go on business and so he told

194
00:13:29,900 --> 00:13:32,000
 the queen to serve the ascetic.

195
00:13:32,000 --> 00:13:37,000
 And so the ascetic came into the room to get alms.

196
00:13:37,000 --> 00:13:40,000
 And the queen had fallen asleep waiting for him.

197
00:13:40,000 --> 00:13:43,000
 And so when she heard him, she stood up.

198
00:13:43,000 --> 00:13:47,000
 And as she stood up her robe fell off and she was naked.

199
00:13:47,000 --> 00:13:52,250
 And she was actually quite beautiful, but she was

200
00:13:52,250 --> 00:13:57,000
 considered in a worldly sense to be beautiful.

201
00:13:57,000 --> 00:14:03,860
 And so he was, you know, he had never seen something and it

202
00:14:03,860 --> 00:14:07,000
 really, similarly isn't it?

203
00:14:07,000 --> 00:14:10,290
 I think the text is like it came up like a snake, his

204
00:14:10,290 --> 00:14:14,000
 desire, and bit him.

205
00:14:14,000 --> 00:14:18,650
 Just in that moment he was bitten and totally infatuated

206
00:14:18,650 --> 00:14:20,000
 with this queen.

207
00:14:20,000 --> 00:14:25,190
 Normally when he came to get the alms he would fly in

208
00:14:25,190 --> 00:14:28,000
 through a window and then fly back out through the window.

209
00:14:28,000 --> 00:14:32,100
 But he found this time after he took the food from her that

210
00:14:32,100 --> 00:14:34,000
 he had to walk out the door.

211
00:14:34,000 --> 00:14:40,050
 And ended up getting very sick, couldn't eat, couldn't

212
00:14:40,050 --> 00:14:43,000
 think of anything except the queen.

213
00:14:43,000 --> 00:14:46,000
 So he ended up lying sick on his bed.

214
00:14:46,000 --> 00:14:50,000
 And the king came back and found out what happened.

215
00:14:50,000 --> 00:14:54,150
 Went and told the queen and the queen said, "I've got an

216
00:14:54,150 --> 00:14:57,000
 idea. We'll solve this problem."

217
00:14:57,000 --> 00:15:00,640
 And so they called the ascetic, or they went to see the asc

218
00:15:00,640 --> 00:15:02,000
etic and the king said,

219
00:15:02,000 --> 00:15:06,360
 "I understand what the problem is and I'm going to give you

220
00:15:06,360 --> 00:15:08,000
 my queen as your wife.

221
00:15:08,000 --> 00:15:11,000
 So you can go and be a lay person.

222
00:15:11,000 --> 00:15:14,420
 I'll give you a house and I'll give you a salary and a job,

223
00:15:14,420 --> 00:15:16,000
 everything you need.

224
00:15:16,000 --> 00:15:20,000
 You can have what you want. We appreciate you so much."

225
00:15:20,000 --> 00:15:23,520
 And anyway the story goes on that the queen ended up giving

226
00:15:23,520 --> 00:15:25,000
 him so much work to do

227
00:15:25,000 --> 00:15:30,000
 and telling him, "Do this, do that," and nagging him.

228
00:15:30,000 --> 00:15:32,950
 And what she did, she sent him back to the palace to fetch

229
00:15:32,950 --> 00:15:34,000
 this, fetch that.

230
00:15:34,000 --> 00:15:37,200
 And he ended up spending all his time just fetching him

231
00:15:37,200 --> 00:15:40,000
 until finally she grabbed him by his beard.

232
00:15:40,000 --> 00:15:43,000
 Because ascetics of course had long beards.

233
00:15:43,000 --> 00:15:47,000
 And said, "Wake up!"

234
00:15:47,000 --> 00:15:52,270
 And he gave him a shock and said, "Look at you. You used to

235
00:15:52,270 --> 00:15:53,000
 be able to fly through the air.

236
00:15:53,000 --> 00:15:57,490
 You used to wield magical power and now you're reduced to a

237
00:15:57,490 --> 00:16:00,000
 servant messenger boy.

238
00:16:00,000 --> 00:16:05,000
 Aren't you ashamed?"

239
00:16:05,000 --> 00:16:10,360
 So the point is that there's a lot of meditation that can

240
00:16:10,360 --> 00:16:13,000
 make you think that you're enlightened.

241
00:16:13,000 --> 00:16:16,000
 And even this meditation you'll get a certain way and then

242
00:16:16,000 --> 00:16:16,000
 you'll think,

243
00:16:16,000 --> 00:16:19,000
 "Wow, I really accomplished something."

244
00:16:19,000 --> 00:16:28,060
 And it's easy to over-esteem your practice, overestimate

245
00:16:28,060 --> 00:16:30,000
 yourself.

246
00:16:30,000 --> 00:16:39,590
 Thalawatu kata having made like a palm's thumb, right? Like

247
00:16:39,590 --> 00:16:41,000
 a thalawat.

248
00:16:41,000 --> 00:16:43,000
 Something like that.

249
00:16:43,000 --> 00:16:49,000
 Anambha wang kata having made to not exist.

250
00:16:49,000 --> 00:16:56,970
 Ayatung anupadadham to make it not arise, to not have any

251
00:16:56,970 --> 00:17:00,000
 arising in the future.

252
00:17:00,000 --> 00:17:04,850
 Titevadham me sukha. These are referring to the defilements

253
00:17:04,850 --> 00:17:05,000
.

254
00:17:05,000 --> 00:17:09,000
 Someone who has made these through enlightenment.

255
00:17:09,000 --> 00:17:12,730
 The enlightenment makes it so that these things can't arise

256
00:17:12,730 --> 00:17:13,000
.

257
00:17:13,000 --> 00:17:18,280
 It's not like a samatha practice where you can suppress

258
00:17:18,280 --> 00:17:19,000
 them,

259
00:17:19,000 --> 00:17:22,000
 make it seem like they're not going to arise.

260
00:17:22,000 --> 00:17:25,000
 No, enlightenment means wisdom.

261
00:17:25,000 --> 00:17:28,350
 Through understanding the things that would normally cause

262
00:17:28,350 --> 00:17:29,000
 a reaction,

263
00:17:29,000 --> 00:17:33,140
 by understanding them, understanding them as impermanent,

264
00:17:33,140 --> 00:17:35,000
 as suffering this non-self.

265
00:17:35,000 --> 00:17:38,520
 The things that we meditate on, understanding, even just

266
00:17:38,520 --> 00:17:40,000
 the rising and falling of the stomach.

267
00:17:40,000 --> 00:17:45,150
 If you understand it and learn to see it just as rising,

268
00:17:45,150 --> 00:17:48,000
 you can become enlightened just on that.

269
00:17:48,000 --> 00:17:51,040
 All it takes is really understanding that says, "Oh yeah,

270
00:17:51,040 --> 00:17:54,000
 it just arises and ceases."

271
00:17:54,000 --> 00:17:58,000
 Once you can look at things that way, then these things,

272
00:17:58,000 --> 00:18:02,000
 you find no reason to get angry, no reason to be greedy.

273
00:18:02,000 --> 00:18:05,390
 There's nothing to want or like. There's nothing to be

274
00:18:05,390 --> 00:18:07,000
 angry or upset about.

275
00:18:07,000 --> 00:18:14,640
 Then, "Diteva dhamme sukhaṁ vīhratī avigahtāṁ anukpa

276
00:18:14,640 --> 00:18:15,000
 yasam."

277
00:18:15,000 --> 00:18:18,000
 "Aparilaham."

278
00:18:18,000 --> 00:18:22,300
 They dwell, such a person dwells in happiness in this

279
00:18:22,300 --> 00:18:24,000
 present reality.

280
00:18:24,000 --> 00:18:30,000
 "Avigahtāṁ," they are unvex-thing.

281
00:18:30,000 --> 00:18:37,000
 Unannoyed and distressed.

282
00:18:37,000 --> 00:18:42,000
 "Anukpa yasam."

283
00:18:42,000 --> 00:18:44,000
 "Soral-less."

284
00:18:44,000 --> 00:18:48,000
 "Parilaham," unfevered. "Aparilahas" is a fever.

285
00:18:48,000 --> 00:18:57,000
 This is a common word.

286
00:18:57,000 --> 00:19:01,000
 The parilahas are the fevers, so they have mental fevers.

287
00:19:01,000 --> 00:19:03,000
 They're likened to a fever.

288
00:19:03,000 --> 00:19:06,000
 Greed is like having a fever because it's like being sick.

289
00:19:06,000 --> 00:19:09,000
 Anger is like being sick.

290
00:19:09,000 --> 00:19:12,000
 It is being sick, so the Buddha called them fevers.

291
00:19:12,000 --> 00:19:17,000
 "Diteva dhamme parīn nibhāyati."

292
00:19:17,000 --> 00:19:24,000
 One is fully freed or extinguished in this very reality.

293
00:19:24,000 --> 00:19:28,000
 "Diteva dhamme." "Dite" means "in."

294
00:19:28,000 --> 00:19:33,000
 It means "that which is seen." "Dhamma" is the "dhamma."

295
00:19:33,000 --> 00:19:44,000
 You can see here and now, basically.

296
00:19:44,000 --> 00:19:49,000
 Such a person is like a person who...

297
00:19:49,000 --> 00:19:52,510
 Suppose there was a tree that was strangled by three creep

298
00:19:52,510 --> 00:19:53,000
ers.

299
00:19:53,000 --> 00:19:56,000
 These are the three defilements.

300
00:19:56,000 --> 00:19:58,420
 "Then the man would come along bringing a shovel in a

301
00:19:58,420 --> 00:19:59,000
 basket.

302
00:19:59,000 --> 00:20:02,000
 He would cut down the creepers at their roots."

303
00:20:02,000 --> 00:20:05,000
 I did this once in Thailand.

304
00:20:05,000 --> 00:20:07,830
 I didn't do it, actually, because we're not allowed to cut

305
00:20:07,830 --> 00:20:08,000
 them,

306
00:20:08,000 --> 00:20:11,000
 but I kind of hinted at the problem.

307
00:20:11,000 --> 00:20:18,000
 One of the laymen cut a vine.

308
00:20:18,000 --> 00:20:21,000
 I don't remember how I did it, but I didn't actually say,

309
00:20:21,000 --> 00:20:24,000
 "Cut that vine." I kind of went around it,

310
00:20:24,000 --> 00:20:27,520
 because we were concerned that if the tree died and fell

311
00:20:27,520 --> 00:20:28,000
 over,

312
00:20:28,000 --> 00:20:32,000
 it would hit one of the buildings.

313
00:20:32,000 --> 00:20:35,000
 It turned out that there was no problem with the vine.

314
00:20:35,000 --> 00:20:38,000
 Some of them have symbiosis with the trees.

315
00:20:38,000 --> 00:20:43,000
 Someone told me it was a shame to cut the vines.

316
00:20:43,000 --> 00:20:45,000
 Anyway, they do this.

317
00:20:45,000 --> 00:20:47,000
 "Then dig it up, pull out the roots."

318
00:20:47,000 --> 00:20:50,000
 Not only does he cut down the creeper,

319
00:20:50,000 --> 00:20:53,000
 but he dug them up, pulled out the roots,

320
00:20:53,000 --> 00:20:56,000
 even the fine rootlets and root fiber.

321
00:20:56,000 --> 00:20:58,000
 "He would cut the creepers into pieces,

322
00:20:58,000 --> 00:21:00,000
 split the pieces and reduce them to slivers."

323
00:21:00,000 --> 00:21:04,000
 So basically, oh, no, and then it's not even done.

324
00:21:04,000 --> 00:21:07,000
 "Then he would dry the slivers in the wind and sun,

325
00:21:07,000 --> 00:21:10,000
 burn them in a fire, reduce them to ashes,

326
00:21:10,000 --> 00:21:12,000
 and winnow the ashes in a strong wind."

327
00:21:12,000 --> 00:21:15,000
 That means blow them in the wind.

328
00:21:15,000 --> 00:21:20,000
 "Then he would let them be carried away by the swift

329
00:21:20,000 --> 00:21:20,000
 current of a river."

330
00:21:20,000 --> 00:21:25,000
 Basically totally overboard, but absolutely...

331
00:21:25,000 --> 00:21:28,000
 The point is absolutely to the very end.

332
00:21:28,000 --> 00:21:33,000
 Gotten to the point where you've completely removed

333
00:21:33,000 --> 00:21:37,000
 and the potential for regrowth.

334
00:21:37,000 --> 00:21:40,000
 So then you could say they were cut off at the root,

335
00:21:40,000 --> 00:21:43,000
 made like a palm stump, obliterated so that they are no

336
00:21:43,000 --> 00:21:43,000
 more subject

337
00:21:43,000 --> 00:21:48,000
 to future arising.

338
00:21:48,000 --> 00:21:50,000
 This is the N

339
00:21:50,000 --> 00:21:53,000
 book of threes.

340
00:21:53,000 --> 00:21:55,000
 And that's how these discourses go.

341
00:21:55,000 --> 00:21:57,000
 The Buddha gives a teaching and they're just listed.

342
00:21:57,000 --> 00:21:59,000
 It's a wonderful book.

343
00:21:59,000 --> 00:22:01,000
 The N is very much worth...

344
00:22:01,000 --> 00:22:04,000
 I couldn't read it through it, but skimming and dipping

345
00:22:04,000 --> 00:22:05,000
 into.

346
00:22:05,000 --> 00:22:08,000
 It's enough for a lifetime.

347
00:22:08,000 --> 00:22:11,690
 The Buddha's teaching, it's not like the Bible or something

348
00:22:11,690 --> 00:22:12,000
.

349
00:22:12,000 --> 00:22:15,000
 Most people don't even read the Bible, but so much more.

350
00:22:15,000 --> 00:22:18,000
 The Buddha's teaching, how could you read it all?

351
00:22:18,000 --> 00:22:21,000
 Most people don't ever have the time.

352
00:22:21,000 --> 00:22:24,000
 But there's so much. It's like an ocean.

353
00:22:24,000 --> 00:22:28,630
 Diving down in the ocean, you come across such wonderful

354
00:22:28,630 --> 00:22:29,000
 things

355
00:22:29,000 --> 00:22:39,000
 that surprise you.

356
00:22:39,000 --> 00:22:43,000
 Anyway, that's the dhamma for tonight.

357
00:22:43,000 --> 00:22:52,000
 Thank you all for tuning in.

358
00:22:52,000 --> 00:22:55,350
 Okay, I'm looking at the chat now. I've just put the hang

359
00:22:55,350 --> 00:22:56,000
out in.

360
00:22:56,000 --> 00:23:04,000
 Mundi is back. Hi, Mundi.

361
00:23:04,000 --> 00:23:07,000
 Simon read my essay. It was very nice.

362
00:23:07,000 --> 00:23:11,000
 That's good to hear.

363
00:23:11,000 --> 00:23:15,000
 The thing about writing essays, it was for school.

364
00:23:15,000 --> 00:23:20,700
 It was more an exercise of getting back into the habit of

365
00:23:20,700 --> 00:23:21,000
...

366
00:23:21,000 --> 00:23:28,770
 or even refining the ability to actually write in a

367
00:23:28,770 --> 00:23:33,000
 scholarly paper.

368
00:23:33,000 --> 00:23:42,000
 That's something I've never really been that good at.

369
00:23:42,000 --> 00:23:45,210
 Anything in the heart sutra that goes against the Buddha's

370
00:23:45,210 --> 00:23:46,000
 teaching?

371
00:23:46,000 --> 00:23:49,000
 I don't even remember. Is the heart sutra?

372
00:23:49,000 --> 00:23:55,000
 Which one's the heart sutra? That's the...

373
00:23:55,000 --> 00:24:04,000
 I think that's the Rupang Shunyattan.

374
00:24:04,000 --> 00:24:08,000
 Let's see here.

375
00:24:08,000 --> 00:24:13,000
 I don't know what it's going to tell you.

376
00:24:13,000 --> 00:24:15,000
 Right.

377
00:24:15,000 --> 00:24:28,000
 Sariputra Rupang Shunyattan.

378
00:24:28,000 --> 00:24:35,000
 So what I take...

379
00:24:35,000 --> 00:24:36,000
 What's the word?

380
00:24:36,000 --> 00:24:38,000
 Take issue with.

381
00:24:38,000 --> 00:24:40,000
 It's not the dhamma.

382
00:24:40,000 --> 00:24:45,000
 The dhamma is that form is void, not form is voidness.

383
00:24:45,000 --> 00:24:48,000
 And I know...

384
00:24:48,000 --> 00:24:52,000
 Absolutely, anything I could say about this is going to be

385
00:24:52,000 --> 00:24:53,000
 derided

386
00:24:53,000 --> 00:24:56,000
 or just rejected by Mahayana.

387
00:24:56,000 --> 00:24:57,000
 They'll say, "Oh no."

388
00:24:57,000 --> 00:25:03,010
 Because they have very sophisticated arguments to explain

389
00:25:03,010 --> 00:25:05,000
 why form is voidness

390
00:25:05,000 --> 00:25:07,000
 or what is meant by that.

391
00:25:07,000 --> 00:25:10,810
 As I said to me at Saphism, "Yeah, yeah, talk all you want

392
00:25:10,810 --> 00:25:11,000
."

393
00:25:11,000 --> 00:25:14,000
 It's not the truth. Form is void. It's the teaching.

394
00:25:14,000 --> 00:25:17,000
 That's an important teaching because it's void of self.

395
00:25:17,000 --> 00:25:24,000
 It's void of pleasure. It's void of stability.

396
00:25:24,000 --> 00:25:28,030
 But the heart sutra does something that is not the Buddha's

397
00:25:28,030 --> 00:25:29,000
 teaching.

398
00:25:29,000 --> 00:25:34,000
 So, you can quote me on that.

399
00:25:34,000 --> 00:25:38,000
 Could you interpret this in a dharmic way? Sure, probably.

400
00:25:38,000 --> 00:25:40,000
 But look, where does it go?

401
00:25:40,000 --> 00:25:44,380
 "Sarabhuta, the character of voidness of all dharmas is non

402
00:25:44,380 --> 00:25:49,000
-arising, non-ceasing, non-defiled, non-pure."

403
00:25:49,000 --> 00:25:55,000
 It's ill-rigling. It's like neither this nor that.

404
00:25:55,000 --> 00:25:58,000
 In void there is no form feeling conscious.

405
00:25:58,000 --> 00:26:02,000
 So in the void, well, that's kind of dharmic.

406
00:26:02,000 --> 00:26:06,000
 I'm talking about nimbana, then in the void.

407
00:26:06,000 --> 00:26:08,000
 But then look, it's going to get weird.

408
00:26:08,000 --> 00:26:13,000
 No ignorance and also no ending of ignorance.

409
00:26:13,000 --> 00:26:18,000
 No old age and death and no ending of old age and death.

410
00:26:18,000 --> 00:26:21,000
 Yeah, yeah, I could write something like this.

411
00:26:21,000 --> 00:26:23,000
 A six-year-old could write something like this.

412
00:26:23,000 --> 00:26:28,000
 Sorry, that's not going to be taken very well.

413
00:26:29,000 --> 00:26:39,000
 "Therefore the Prajna Paramita is the great magic spell."

414
00:26:39,000 --> 00:26:43,000
 I think dhārini is the word, right?

415
00:26:43,000 --> 00:26:46,000
 So the idea is that if you chant this, it's a protection.

416
00:26:46,000 --> 00:26:48,000
 What should we do in ours?

417
00:26:48,000 --> 00:26:49,000
 And then there's this weird thing at the end,

418
00:26:49,000 --> 00:26:54,000
 "Gatte gatte paro gatte parasan gatte bodhisvaha."

419
00:26:54,000 --> 00:27:01,000
 People like these, this is not high or high dharma or high

420
00:27:01,000 --> 00:27:01,000
 religion.

421
00:27:01,000 --> 00:27:03,000
 This is low religion.

422
00:27:03,000 --> 00:27:04,000
 People like these.

423
00:27:04,000 --> 00:27:07,000
 In Thailand they have all these.

424
00:27:07,000 --> 00:27:22,000
 "Gatte gatte paro gatte parasan gatte bodhisvaha."

425
00:27:22,000 --> 00:27:26,000
 People believe that's powerful.

426
00:27:26,000 --> 00:27:33,000
 Anyway, sorry to trash talk it, but I don't value this.

427
00:27:33,000 --> 00:27:35,000
 It's not valuable to me.

428
00:27:35,000 --> 00:27:37,000
 It's very short, huh?

429
00:27:37,000 --> 00:27:40,000
 I didn't realize it was that short.

430
00:27:40,000 --> 00:28:01,000
 "Should meditators have a pet?"

431
00:28:01,000 --> 00:28:03,700
 I don't want to get into it, but no, I think meditators

432
00:28:03,700 --> 00:28:04,000
 should not have pets.

433
00:28:04,000 --> 00:28:07,000
 They are just a burden.

434
00:28:07,000 --> 00:28:09,000
 I mean, I've given talks on this.

435
00:28:09,000 --> 00:28:12,000
 So again, this is why I don't answer questions in text,

436
00:28:12,000 --> 00:28:16,400
 because most of the questions you ask, I've given videos on

437
00:28:16,400 --> 00:28:17,000
 this.

438
00:28:17,000 --> 00:28:20,000
 It's again and again the same thing.

439
00:28:20,000 --> 00:28:25,630
 But briefly, if you've got an animal or if you're going to

440
00:28:25,630 --> 00:28:27,000
 take care of an animal,

441
00:28:27,000 --> 00:28:29,600
 that's fine, but for someone to say, "Hey, I'm going to go

442
00:28:29,600 --> 00:28:34,000
 get a pet," that's a bad idea.

443
00:28:34,000 --> 00:28:35,000
 It's just a burden.

444
00:28:35,000 --> 00:28:41,000
 Especially for meditators, it ends up causing distraction.

445
00:28:41,000 --> 00:28:43,000
 It's a long answer.

446
00:28:43,000 --> 00:28:46,350
 I've talked about this, and not everyone agrees with the

447
00:28:46,350 --> 00:28:47,000
 things I say,

448
00:28:47,000 --> 00:28:50,350
 but part of the thing is you're trying to help a being that

449
00:28:50,350 --> 00:28:52,000
 is very difficult to help.

450
00:28:52,000 --> 00:28:55,000
 There's only so much you can do for an animal,

451
00:28:55,000 --> 00:28:58,000
 and if you're going out of your way to do that, you say,

452
00:28:58,000 --> 00:29:00,960
 "Hey, I'll go to the pound and pick up an animal or

453
00:29:00,960 --> 00:29:02,000
 something."

454
00:29:02,000 --> 00:29:05,000
 It's all about efficiency.

455
00:29:05,000 --> 00:29:08,000
 You end up spending so much time and money and effort,

456
00:29:08,000 --> 00:29:10,000
 and you get attached.

457
00:29:10,000 --> 00:29:13,000
 Of course, there's all the defilements associated with that

458
00:29:13,000 --> 00:29:13,000
.

459
00:29:13,000 --> 00:29:16,000
 For very little benefit, relatively speaking,

460
00:29:16,000 --> 00:29:21,000
 as opposed to Michael, who's here now, from America.

461
00:29:21,000 --> 00:29:24,440
 So I put my effort into him, and it's actually very little

462
00:29:24,440 --> 00:29:25,000
 effort.

463
00:29:25,000 --> 00:29:28,480
 He works very hard, and I just have to teach him, and the

464
00:29:28,480 --> 00:29:30,000
 benefit is awesome.

465
00:29:30,000 --> 00:29:33,000
 His life could change from this.

466
00:29:33,000 --> 00:29:35,000
 We'll have an interview when he's done the course,

467
00:29:35,000 --> 00:29:38,440
 and hopefully he'll be up to that, and you can all hear

468
00:29:38,440 --> 00:29:40,000
 what he has to say.

469
00:29:40,000 --> 00:29:43,000
 So Michael's here.

470
00:29:43,000 --> 00:29:46,000
 He's planning on sticking around for some months.

471
00:29:46,000 --> 00:29:49,000
 Hopefully he can act as a steward for a while.

472
00:29:49,000 --> 00:29:52,000
 He's from America, so he can't stay long-term.

473
00:29:52,000 --> 00:29:55,000
 Or maybe he can. Maybe we can work that out.

474
00:29:55,000 --> 00:29:58,000
 But he'll stay some month.

475
00:29:58,000 --> 00:30:15,000
 [Silence]

476
00:30:15,000 --> 00:30:19,000
 Hi, Ken. You've joined the Hangout.

477
00:30:19,000 --> 00:30:23,000
 Do you have a question for me?

478
00:30:23,000 --> 00:30:28,000
 I don't know.

479
00:30:28,000 --> 00:30:33,000
 You're live. Do I know you?

480
00:30:33,000 --> 00:30:37,000
 You look familiar.

481
00:30:37,000 --> 00:30:39,000
 You're muted again.

482
00:30:39,000 --> 00:30:52,000
 [Silence]

483
00:30:52,000 --> 00:31:01,000
 Okay. Well, in that case, I'm going to say good night.

484
00:31:01,000 --> 00:31:03,000
 Have a good night, everyone.

485
00:31:03,000 --> 00:31:06,000
 We should be mugging tomorrow at...

486
00:31:06,000 --> 00:31:10,000
 I don't even remember when it is two o'clock, right?

487
00:31:10,000 --> 00:31:15,000
 Tomorrow at two.

488
00:31:15,000 --> 00:31:18,000
 Good night.

489
00:31:18,000 --> 00:31:28,000
 [Silence]

