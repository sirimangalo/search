1
00:00:00,000 --> 00:00:03,000
 How do you feel Mr. Fed?

2
00:00:03,000 --> 00:00:06,000
 It's difficult or easy?

3
00:00:06,000 --> 00:00:08,000
 It's real.

4
00:00:08,000 --> 00:00:10,000
 How do you feel after you practice?

5
00:00:10,000 --> 00:00:13,000
 It's difficult or easy?

6
00:00:13,000 --> 00:00:16,000
 Difficult to practice or easy if you do?

7
00:00:16,000 --> 00:00:18,000
 It's difficult, difficult.

8
00:00:18,000 --> 00:00:23,000
 It's the beginning to concentrate.

9
00:00:23,000 --> 00:00:27,000
 When we are together, it's what we needed.

10
00:00:27,000 --> 00:00:31,000
 You know why I let you walk for a long time

11
00:00:31,000 --> 00:00:33,000
 and let you sit for a long time?

12
00:00:33,000 --> 00:00:35,000
 I know it's difficult for you

13
00:00:35,000 --> 00:00:39,000
 because all of you are students.

14
00:00:39,000 --> 00:00:45,000
 You should know the proper way, the correct way.

15
00:00:45,000 --> 00:00:50,000
 Because if you know the proper way and the correct way,

16
00:00:50,000 --> 00:00:56,000
 one day you can keep instruction to other people.

17
00:00:56,000 --> 00:01:00,950
 After you go home, I believe your friends will ask all of

18
00:01:00,950 --> 00:01:02,000
 you that,

19
00:01:02,000 --> 00:01:04,000
 what you did in Thailand.

20
00:01:04,000 --> 00:01:08,360
 We are going to learn meditation at Wat Pha Tha, second

21
00:01:08,360 --> 00:01:09,000
 five.

22
00:01:09,000 --> 00:01:13,000
 And what you are doing, what you did, what is your

23
00:01:13,000 --> 00:01:14,000
 experience?

24
00:01:14,000 --> 00:01:19,000
 So you can explain to your friends that

25
00:01:19,000 --> 00:01:23,000
 when you are coming here, what I told you,

26
00:01:23,000 --> 00:01:26,000
 how can I give you instruction?

27
00:01:26,000 --> 00:01:32,000
 I explained to all of you before you started practicing.

28
00:01:32,000 --> 00:01:38,000
 I tried to explain to all of you for details

29
00:01:38,000 --> 00:01:42,720
 how to do, how to start, how to practice, how to walk, how

30
00:01:42,720 --> 00:01:44,000
 to sit.

31
00:01:44,000 --> 00:01:50,000
 Because meditation practice, I told you, I said that

32
00:01:50,000 --> 00:01:57,190
 anyone can practice anywhere, any day, any time, not just

33
00:01:57,190 --> 00:01:59,000
 for Buddhists.

34
00:01:59,000 --> 00:02:05,500
 Any religions can practice, but Buddhists only can teach in

35
00:02:05,500 --> 00:02:06,000
 meditation.

36
00:02:06,000 --> 00:02:08,000
 The others never happen.

37
00:02:08,000 --> 00:02:11,660
 They cannot teach because they never have experience about

38
00:02:11,660 --> 00:02:13,000
 meditation.

39
00:02:13,000 --> 00:02:15,000
 I can say that.

40
00:02:15,000 --> 00:02:20,000
 So you don't worry that if you practice meditation,

41
00:02:20,000 --> 00:02:27,190
 I am not sure if maybe some of you are Christian or Muslim,

42
00:02:27,190 --> 00:02:28,000
 whatever.

43
00:02:28,000 --> 00:02:31,000
 You don't worry about real religions.

44
00:02:31,000 --> 00:02:37,000
 You just tell yourself that now you are coming here

45
00:02:37,000 --> 00:02:41,000
 to learning how to practice meditation.

46
00:02:41,000 --> 00:02:48,070
 I told all of you that meditation, anyone can practice

47
00:02:48,070 --> 00:02:51,000
 anywhere, any day, any time.

48
00:02:51,000 --> 00:02:55,450
 Even though you are walking on the road normally, you are

49
00:02:55,450 --> 00:02:57,000
 walking on the street normally,

50
00:02:57,000 --> 00:02:59,000
 you can practice. How to do?

51
00:02:59,000 --> 00:03:03,220
 You just try to be mindful every step you are stepping,

52
00:03:03,220 --> 00:03:07,000
 right, left, right, left, right, left, that is meditation.

53
00:03:07,000 --> 00:03:11,100
 And even though you are standing and waiting for a bus,

54
00:03:11,100 --> 00:03:12,000
 when you go to school,

55
00:03:12,000 --> 00:03:16,810
 you are standing, standing, standing, standing, be alright,

56
00:03:16,810 --> 00:03:18,000
 your body is standing.

57
00:03:18,000 --> 00:03:22,000
 It means you try to practice already.

58
00:03:22,000 --> 00:03:26,760
 Or even though you are sitting in the aeroplane, rising,

59
00:03:26,760 --> 00:03:28,000
 falling, rising, falling,

60
00:03:28,000 --> 00:03:33,000
 you can practice, you see. Now you know how to do, right?

61
00:03:33,000 --> 00:03:37,220
 So when you are walking, you just know that you are walking

62
00:03:37,220 --> 00:03:41,000
 right, left, right, left, right, right, left.

63
00:03:41,000 --> 00:03:44,000
 And then when you are sitting, you sit wherever you can,

64
00:03:44,000 --> 00:03:46,000
 wherever you like.

65
00:03:46,000 --> 00:03:48,920
 You are sitting like this in the aeroplane, rising, falling

66
00:03:48,920 --> 00:03:50,000
, rising, falling.

67
00:03:50,000 --> 00:03:53,000
 If you sleep, sleepy, sleepy, sleepy.

68
00:03:53,000 --> 00:03:59,000
 And at home tonight, when you go to bed,

69
00:03:59,000 --> 00:04:02,480
 after you let out your body on your bed, you can meditate

70
00:04:02,480 --> 00:04:03,000
 too.

71
00:04:03,000 --> 00:04:06,670
 Just after you let out your body on your bed, you put your

72
00:04:06,670 --> 00:04:08,000
 hands on the aeroplane,

73
00:04:08,000 --> 00:04:13,000
 rising, falling, rising, falling, until you fall asleep.

74
00:04:13,000 --> 00:04:17,000
 See, you can practice anywhere, any day.

75
00:04:17,000 --> 00:04:24,000
 You don't want to wait in. Anywhere, any day, any time.

76
00:04:24,000 --> 00:04:31,000
 Okay? So today, at least, you know how to practice.

77
00:04:31,000 --> 00:04:38,000
 And you know it is not easy to practice meditation, right?

78
00:04:38,000 --> 00:04:45,000
 But please try to practice continuously after you go home.

79
00:04:45,000 --> 00:04:50,270
 Because, you know, meditation practice, if anyone can

80
00:04:50,270 --> 00:04:52,000
 practice every day,

81
00:04:52,000 --> 00:04:58,540
 every 15 minutes a day, it is very, very good for all of

82
00:04:58,540 --> 00:04:59,000
 you.

83
00:04:59,000 --> 00:05:04,220
 Because when you practice meditation, it means you try to

84
00:05:04,220 --> 00:05:07,000
 develop your concentration,

85
00:05:07,000 --> 00:05:09,000
 try to develop your mindfulness.

86
00:05:09,000 --> 00:05:11,910
 If you have good mindfulness, if you have good

87
00:05:11,910 --> 00:05:13,000
 concentration,

88
00:05:13,000 --> 00:05:18,000
 you can have good memory.

89
00:05:18,000 --> 00:05:21,000
 Your memory will be very good.

90
00:05:21,000 --> 00:05:25,000
 You try only 15 minutes before you go to bed.

91
00:05:25,000 --> 00:05:29,000
 You are just sitting, rising, falling, rising, falling,

92
00:05:29,000 --> 00:05:32,570
 only 10 minutes or 15 minutes in the morning before you go

93
00:05:32,570 --> 00:05:33,000
 to work,

94
00:05:33,000 --> 00:05:35,000
 before you go to school.

95
00:05:35,000 --> 00:05:41,000
 You try to sit quietly about 15 minutes.

96
00:05:41,000 --> 00:05:45,000
 You know, if you cannot remember rising, falling,

97
00:05:45,000 --> 00:05:49,800
 you can try the other technique, like you can count on your

98
00:05:49,800 --> 00:05:51,000
 breathing.

99
00:05:51,000 --> 00:05:54,000
 You sit on the chair, because in your country,

100
00:05:54,000 --> 00:05:57,720
 I know you don't want to sit like this, it is difficult for

101
00:05:57,720 --> 00:05:58,000
 you.

102
00:05:58,000 --> 00:06:02,000
 But you can sit on the chair or on your bed,

103
00:06:02,000 --> 00:06:06,000
 and then sitting like this, or like this,

104
00:06:06,000 --> 00:06:11,410
 breathing in, breathing out, in out one, in out two, in out

105
00:06:11,410 --> 00:06:13,000
 three, in out four,

106
00:06:13,000 --> 00:06:17,000
 up to ten, and recover again, about 15 minutes.

107
00:06:17,000 --> 00:06:22,020
 And then, if you do like that, it means you can develop

108
00:06:22,020 --> 00:06:23,000
 your mindfulness,

109
00:06:23,000 --> 00:06:28,760
 you can develop your concentration, and then you can have

110
00:06:28,760 --> 00:06:30,000
 good memory.

111
00:06:30,000 --> 00:06:36,000
 Okay, anyone has a question? No more questions? Nothing?

112
00:06:36,000 --> 00:06:38,000
 Please.

113
00:06:38,000 --> 00:06:43,000
 [ Inaudible question ]

114
00:06:43,000 --> 00:06:46,000
 Yes, because you should walk in before you are sitting,

115
00:06:46,000 --> 00:06:50,500
 because if you are just sitting, it is more difficult for

116
00:06:50,500 --> 00:06:53,000
 you to have good concentration.

117
00:06:53,000 --> 00:06:57,120
 Because when you are walking, it means you try to develop

118
00:06:57,120 --> 00:07:00,000
 your concentration, you know?

119
00:07:00,000 --> 00:07:04,000
 But you have to try to pay attention.

120
00:07:04,000 --> 00:07:07,080
 Standing, be aware of your body is standing, awareness of

121
00:07:07,080 --> 00:07:08,000
 body is standing,

122
00:07:08,000 --> 00:07:13,130
 because that moment, you try to slow down your thought,

123
00:07:13,130 --> 00:07:14,000
 right?

124
00:07:14,000 --> 00:07:18,000
 You know, "Oh, this moment I am standing, so standing,

125
00:07:18,000 --> 00:07:20,000
 standing, standing."

126
00:07:20,000 --> 00:07:22,000
 You are not thinking.

127
00:07:22,000 --> 00:07:26,000
 But if you are just sitting, your mind always runs away.

128
00:07:26,000 --> 00:07:30,000
 It is difficult for you to have good concentration.

129
00:07:30,000 --> 00:07:35,000
 So that is the reason I explain to you about walking first.

130
00:07:35,000 --> 00:07:37,000
 I show you how to walk first.

131
00:07:37,000 --> 00:07:42,100
 But most of meditation that they are teaching in Europe or

132
00:07:42,100 --> 00:07:43,000
 America,

133
00:07:43,000 --> 00:07:45,000
 they don't want to walk.

134
00:07:45,000 --> 00:07:47,000
 Just sitting.

135
00:07:47,000 --> 00:07:51,000
 And they use the sitting question is very important,

136
00:07:51,000 --> 00:07:54,000
 because they don't like to have suffering.

137
00:07:54,000 --> 00:07:57,000
 They don't want to have the pain.

138
00:07:57,000 --> 00:08:01,040
 So they take the sitting question high and it is very

139
00:08:01,040 --> 00:08:02,000
 affordable.

140
00:08:02,000 --> 00:08:04,000
 But it is not good.

141
00:08:04,000 --> 00:08:08,910
 Because if you don't see, if you don't have experience

142
00:08:08,910 --> 00:08:10,000
 about suffering,

143
00:08:10,000 --> 00:08:14,820
 you cannot have experience about, you don't understand what

144
00:08:14,820 --> 00:08:16,000
 is suffering.

145
00:08:16,000 --> 00:08:20,210
 So you should know when you are sitting in meditation, if

146
00:08:20,210 --> 00:08:23,000
 you are in pain, how do you feel?

147
00:08:23,000 --> 00:08:28,460
 And then not pain, pain, pain, because even pain, many

148
00:08:28,460 --> 00:08:29,000
 kinds of pains,

149
00:08:29,000 --> 00:08:34,000
 sometimes your legs look like deep pain.

150
00:08:34,000 --> 00:08:38,980
 But sometimes it is rising, rising, rising and then

151
00:08:38,980 --> 00:08:40,000
 disappear.

152
00:08:40,000 --> 00:08:42,320
 Sometimes it increases, increases, increases and then

153
00:08:42,320 --> 00:08:43,000
 disappears.

154
00:08:43,000 --> 00:08:48,000
 Sometimes pain increases, increases and stays.

155
00:08:48,000 --> 00:08:51,000
 For a long time they will disappear.

156
00:08:51,000 --> 00:08:55,000
 So even if it is pain, it is staying, you know this fear?

157
00:08:55,000 --> 00:08:58,000
 You try to understand how do you feel?

158
00:08:58,000 --> 00:09:03,000
 Sometimes it looks like a fire, burn your leg.

159
00:09:03,000 --> 00:09:06,000
 Sometimes you feel like your leg is broken.

160
00:09:06,000 --> 00:09:10,970
 Sometimes you feel like your body is broken now, but it is

161
00:09:10,970 --> 00:09:12,000
 not.

162
00:09:12,000 --> 00:09:16,000
 See? So you try to understand.

163
00:09:16,000 --> 00:09:21,000
 And you know when you practice meditation,

164
00:09:21,000 --> 00:09:28,500
 the biggest benefit you get is you know how to give up your

165
00:09:28,500 --> 00:09:30,000
 suffering.

166
00:09:30,000 --> 00:09:37,590
 For example, one day now you are happy, you don't know how

167
00:09:37,590 --> 00:09:39,000
 to, what is suffering.

168
00:09:39,000 --> 00:09:43,880
 But one day if you have mental problem, if you broken heart

169
00:09:43,880 --> 00:09:45,000
, for example,

170
00:09:45,000 --> 00:09:49,000
 if you broken heart you will be sad, you will be upset.

171
00:09:49,000 --> 00:09:54,130
 You have to try to understand that everything is imper

172
00:09:54,130 --> 00:09:55,000
manent.

173
00:09:55,000 --> 00:09:59,480
 And then you can overcome your suffering, you can overcome

174
00:09:59,480 --> 00:10:01,000
 your painful.

175
00:10:01,000 --> 00:10:04,000
 Try to understand.

176
00:10:04,000 --> 00:10:10,210
 Before we stop, I will lead you to the radiation of loving

177
00:10:10,210 --> 00:10:11,000
 kindness.

178
00:10:11,000 --> 00:10:13,000
 Just repeat after me.

179
00:10:13,000 --> 00:10:15,000
 Because of why?

180
00:10:15,000 --> 00:10:21,000
 Because of usually when we are practicing in the temple

181
00:10:21,000 --> 00:10:24,000
 every time, every day,

182
00:10:24,000 --> 00:10:27,810
 three times a day, after we finish meditating, we have to

183
00:10:27,810 --> 00:10:31,000
 do radiation of loving kindness.

184
00:10:31,000 --> 00:10:35,300
 Because we can share our loving kindness to other people,

185
00:10:35,300 --> 00:10:40,000
 for yourself and for other people.

186
00:10:40,000 --> 00:10:43,000
 You draw your hands like this, please.

187
00:10:43,000 --> 00:10:46,000
 And repeat after me.

188
00:10:46,000 --> 00:10:56,000
 Ahaan, suki to, ho me, may I be happy.

189
00:10:56,000 --> 00:11:03,000
 Need to ho ho me, may I be free from suffering.

190
00:11:03,000 --> 00:11:11,000
 Ave ro ho me, may I be free from enmity.

191
00:11:11,000 --> 00:11:20,000
 Ape ya pa show ho me, may I be free from hurtfulness.

192
00:11:20,000 --> 00:11:29,000
 Ani ho ho me, may I be free from troubles of body and mind.

193
00:11:29,000 --> 00:11:40,260
 Suki ataram, pariharami, may I be able to protect my own

194
00:11:40,260 --> 00:11:42,000
 happiness.

195
00:11:42,000 --> 00:11:45,000
 This is for yourself and then for other people.

196
00:11:45,000 --> 00:11:56,260
 Sape sata, whatever beings they are, suki ta ho to, may

197
00:11:56,260 --> 00:11:59,000
 they be happy.

198
00:11:59,000 --> 00:12:08,000
 Need to ho ho me, may they be free from suffering.

199
00:12:08,000 --> 00:12:16,000
 Ave ra ho ho me, may they be free from enmity.

200
00:12:16,000 --> 00:12:25,000
 Ape ya pa show ho me, may they be free from hurtfulness.

201
00:12:25,000 --> 00:12:35,350
 Ani ho ho ho me, may they be free from troubles of body and

202
00:12:35,350 --> 00:12:37,000
 mind.

203
00:12:37,000 --> 00:12:48,460
 Suki ataram, pariharami to, may they be able to protect

204
00:12:48,460 --> 00:12:51,000
 their own happiness.

205
00:12:51,000 --> 00:12:55,000
 This is radiation of loving kindness and then I leading you

206
00:12:55,000 --> 00:12:58,000
 to do dedication of marriage.

207
00:12:58,000 --> 00:13:02,000
 Because you know when you are practicing meditation,

208
00:13:02,000 --> 00:13:07,000
 it means you are doing the biggest marriage for your life,

209
00:13:07,000 --> 00:13:09,000
 the biggest marriage for your life.

210
00:13:09,000 --> 00:13:13,000
 So you should share your marriage to other people.

211
00:13:13,000 --> 00:13:17,000
 Just repeat after me.

212
00:13:17,000 --> 00:13:22,000
 The marriage performed by me,

213
00:13:22,000 --> 00:13:28,000
 I am happy to share with my parents,

214
00:13:28,000 --> 00:13:38,440
 my grandfather, grandmother, teachers, preceptors, lect

215
00:13:38,440 --> 00:13:44,000
urers, everybody, no limit.

216
00:13:44,000 --> 00:13:51,000
 Please accept and receive the merit.

217
00:13:51,000 --> 00:13:56,000
 The merit we dedicate to you.

218
00:13:56,000 --> 00:14:02,000
 If you are suffering, may be free from suffering.

219
00:14:02,000 --> 00:14:10,960
 If you are happy, may you be happy more and more and more

220
00:14:10,960 --> 00:14:12,000
 forever.

221
00:14:12,000 --> 00:14:18,000
 See, this is a dedication of merit.

222
00:14:18,000 --> 00:14:23,000
 So if you practice at home, you should do.

223
00:14:23,000 --> 00:14:28,380
 You don't want to say pari because at-home, there is pari

224
00:14:28,380 --> 00:14:29,000
 language.

225
00:14:29,000 --> 00:14:38,000
 You can say in your language, you can say in your language.

226
00:14:38,000 --> 00:14:42,870
 And you can say like, I dedicate my marriage performed by

227
00:14:42,870 --> 00:14:44,000
 me to my parents,

228
00:14:44,000 --> 00:14:46,740
 my father, my grandfather, my grandmother, something like

229
00:14:46,740 --> 00:14:50,000
 that in your language.

230
00:14:50,000 --> 00:14:53,000
 See, it's very good for you.

231
00:14:53,000 --> 00:14:57,320
 And if you dedicate, if you are already patient of loving

232
00:14:57,320 --> 00:14:58,000
 kindness every day,

233
00:14:58,000 --> 00:15:01,000
 every day, your heart will be full of loving kindness.

234
00:15:01,000 --> 00:15:05,300
 Loving kindness is very good if you go to anywhere you will

235
00:15:05,300 --> 00:15:06,000
 be saved

236
00:15:06,000 --> 00:15:10,000
 because your heart, your mind is full of loving kindness.

237
00:15:10,000 --> 00:15:13,000
 I would like to give you blessing, okay?

238
00:15:13,000 --> 00:15:15,000
 Draw your hands now.

239
00:15:15,000 --> 00:15:18,000
 Draw your hands, please, and give you blessing.

240
00:15:18,000 --> 00:15:21,410
 Any kind of merit performed by me, I'm happy to share all

241
00:15:21,410 --> 00:15:22,000
 of merit

242
00:15:22,000 --> 00:15:25,000
 performed by me to all of you.

243
00:15:25,000 --> 00:15:28,000
 May you have what I have.

244
00:15:28,000 --> 00:15:32,000
 May all of you be good at power, that my power,

245
00:15:32,000 --> 00:15:35,000
 and sign of power, we protect all of you.

246
00:15:35,000 --> 00:15:42,000
 May all of you have four blessings like long life, beauty,

247
00:15:42,000 --> 00:15:44,000
 happiness, and luck forever.

248
00:15:44,000 --> 00:15:47,840
 May you have good trip and safe trip to your country,

249
00:15:47,840 --> 00:15:49,000
 everybody.

250
00:15:49,000 --> 00:15:52,000
 Okay, thank you all very, very much.

251
00:15:52,000 --> 00:15:55,000
 And I say it in Pali again.

252
00:15:55,000 --> 00:16:00,000
 (speaking in Pali)

253
00:16:00,000 --> 00:16:05,000
 (speaking in Pali)

254
00:16:05,000 --> 00:16:10,000
 (speaking in Pali)

255
00:16:10,000 --> 00:16:15,000
 (speaking in Pali)

256
00:16:15,000 --> 00:16:20,000
 (speaking in Pali)

257
00:16:20,000 --> 00:16:25,000
 (speaking in Pali)

258
00:16:25,000 --> 00:16:30,000
 (speaking in Pali)

259
00:16:30,000 --> 00:16:35,000
 (speaking in Pali)

260
00:16:35,000 --> 00:16:40,000
 (speaking in Pali)

261
00:16:40,000 --> 00:16:45,000
 (speaking in Pali)

262
00:16:45,000 --> 00:16:50,000
 (speaking in Pali)

263
00:16:50,000 --> 00:16:55,000
 (speaking in Pali)

264
00:16:55,000 --> 00:16:58,000
 Okay, good luck to you.

265
00:16:58,000 --> 00:17:03,000
 (music)

266
00:17:03,000 --> 00:17:08,000
 (music)

267
00:17:08,000 --> 00:17:13,000
 (music)

268
00:17:13,000 --> 00:17:18,000
 (music)

269
00:17:18,000 --> 00:17:23,000
 (music)

270
00:17:23,000 --> 00:17:28,000
 (music)

271
00:17:28,000 --> 00:17:33,000
 (music)

272
00:17:33,000 --> 00:17:38,000
 (music)

273
00:17:38,000 --> 00:17:43,000
 (music)

