1
00:00:00,000 --> 00:00:05,320
 Okay, good evening everyone.

2
00:00:05,320 --> 00:00:09,240
 I'm broadcasting live October 1st.

3
00:00:09,240 --> 00:00:16,160
 It's now officially October.

4
00:00:16,160 --> 00:00:19,160
 I was just informed, just reminded that we've got less than

5
00:00:19,160 --> 00:00:24,600
 a month left of the rains.

6
00:00:24,600 --> 00:00:27,330
 Tonight I will be heading back, probably have to sign off

7
00:00:27,330 --> 00:00:27,880
 early.

8
00:00:27,880 --> 00:00:30,560
 I'll be heading back to Stony Creek.

9
00:00:30,560 --> 00:00:41,960
 We'll come back tomorrow morning here to Hamilton.

10
00:00:41,960 --> 00:00:47,720
 Today I went walking through the forest.

11
00:00:47,720 --> 00:00:50,080
 There's a nature trail.

12
00:00:50,080 --> 00:00:56,680
 Actually quite close by it's half a kilometer away.

13
00:00:56,680 --> 00:01:01,050
 Yes, get into the nature trail and it goes straight to

14
00:01:01,050 --> 00:01:03,200
 McMaster University.

15
00:01:03,200 --> 00:01:08,920
 So I took it and then I went on another trail in the loop.

16
00:01:08,920 --> 00:01:12,760
 Hey, I wonder...

17
00:01:12,760 --> 00:01:18,240
 Oh, no, you'll see it.

18
00:01:18,240 --> 00:01:24,540
 The reason was, besides going on a nature walk, next Friday

19
00:01:24,540 --> 00:01:26,560
 we'll be holding a peace

20
00:01:26,560 --> 00:01:27,560
 walk.

21
00:01:27,560 --> 00:01:32,610
 A walk for peace in the tradition of the Buddha, Mahatma

22
00:01:32,610 --> 00:01:37,800
 Gandhi, Martin Luther King, Maha Gosananda,

23
00:01:37,800 --> 00:01:41,130
 Cambodian monk who walked for peace when they were killing

24
00:01:41,130 --> 00:01:42,480
 each other in Cambodia.

25
00:01:42,480 --> 00:01:47,620
 They would walk in between the fighting, walk across the

26
00:01:47,620 --> 00:01:50,480
 battlefield, trying to get the

27
00:01:50,480 --> 00:01:52,400
 people to stop shooting each other.

28
00:01:52,400 --> 00:01:58,220
 Of course we won't be doing that, but we'll be walking for

29
00:01:58,220 --> 00:01:58,960
 peace.

30
00:01:58,960 --> 00:02:03,710
 So inner peace, we will be trying to be peaceful and do it

31
00:02:03,710 --> 00:02:06,400
 as a walking meditation and just

32
00:02:06,400 --> 00:02:08,160
 to make a statement.

33
00:02:08,160 --> 00:02:11,920
 I was thinking we could have like placards with peace walk

34
00:02:11,920 --> 00:02:13,440
 on them just to let people

35
00:02:13,440 --> 00:02:14,440
 know what we were doing.

36
00:02:14,440 --> 00:02:19,360
 Of course we'll be in the forest most of the way, so maybe

37
00:02:19,360 --> 00:02:22,000
 that's not so applicable.

38
00:02:22,000 --> 00:02:25,220
 There are two peace organizations on campus that we're

39
00:02:25,220 --> 00:02:27,600
 going to be coordinating with,

40
00:02:27,600 --> 00:02:28,600
 hopefully.

41
00:02:28,600 --> 00:02:31,040
 Contact with both of them about it.

42
00:02:31,040 --> 00:02:35,220
 It will take about an hour, so we're going to walk from

43
00:02:35,220 --> 00:02:36,720
 McMaster to here.

44
00:02:36,720 --> 00:02:41,430
 Then when we get here, we'll have hot apple cider and

45
00:02:41,430 --> 00:02:42,640
 cookies.

46
00:02:42,640 --> 00:02:48,600
 I've got to get my organization on that.

47
00:02:48,600 --> 00:02:52,520
 Bring apple cider and we'll have it hot.

48
00:02:52,520 --> 00:03:00,520
 Apparently Food Basic sells apple cider.

49
00:03:00,520 --> 00:03:04,920
 We need a big pot maybe to keep it hot.

50
00:03:04,920 --> 00:03:08,440
 Have it ready.

51
00:03:08,440 --> 00:03:12,040
 Get some apple cider.

52
00:03:12,040 --> 00:03:16,040
 Oh good fall stuff is like popcorn.

53
00:03:16,040 --> 00:03:20,360
 Apple cider donuts, apples.

54
00:03:20,360 --> 00:03:22,360
 Pumpkin pie.

55
00:03:22,360 --> 00:03:28,560
 Hey, when is Thanksgiving in Canada?

56
00:03:28,560 --> 00:03:31,760
 I have no idea.

57
00:03:31,760 --> 00:03:33,960
 It's different from America, that's all I know.

58
00:03:33,960 --> 00:03:35,600
 Yours is later I think.

59
00:03:35,600 --> 00:03:37,440
 Ours is probably this month.

60
00:03:37,440 --> 00:03:43,040
 Yes, ours is third week of November.

61
00:03:43,040 --> 00:03:56,360
 I think ours is like third week of October.

62
00:03:56,360 --> 00:03:57,360
 So there's that.

63
00:03:57,360 --> 00:04:02,240
 Anyway, shouldn't talk too much because I got to go.

64
00:04:02,240 --> 00:04:03,240
 Let's look at the quote.

65
00:04:03,240 --> 00:04:07,720
 Robin, would you do us the one that we're reading?

66
00:04:07,720 --> 00:04:11,420
 Was an innocent baby boy lying on his back or because of

67
00:04:11,420 --> 00:04:13,640
 some carelessness of his nurse

68
00:04:13,640 --> 00:04:16,560
 to put a stick or a stone into his mouth?

69
00:04:16,560 --> 00:04:19,360
 His nurse would immediately do something to remove it.

70
00:04:19,360 --> 00:04:21,470
 And if she could not get it out immediately, she would hold

71
00:04:21,470 --> 00:04:22,760
 the child's head with her left

72
00:04:22,760 --> 00:04:26,250
 hand and with the finger of her right hand get it out even

73
00:04:26,250 --> 00:04:28,000
 if she had to draw blood.

74
00:04:28,000 --> 00:04:29,300
 And why?

75
00:04:29,300 --> 00:04:32,920
 Because such a thing is a danger to the child by no means

76
00:04:32,920 --> 00:04:33,520
 harmless.

77
00:04:33,520 --> 00:04:35,920
 Also the nurse would do such a thing out of love for the

78
00:04:35,920 --> 00:04:37,480
 child's benefit, out of kindness

79
00:04:37,480 --> 00:04:39,240
 and compassion.

80
00:04:39,240 --> 00:04:42,460
 But when that boy is older and more wise, then the nurse no

81
00:04:42,460 --> 00:04:44,920
 longer looks after him thinking

82
00:04:44,920 --> 00:04:46,680
 the boy can look after himself.

83
00:04:46,680 --> 00:04:48,880
 He is done with foolishness.

84
00:04:48,880 --> 00:04:53,410
 In the same way, if due to lack of faith, self-respect,

85
00:04:53,410 --> 00:04:56,240
 fear of blame, energy and wisdom,

86
00:04:56,240 --> 00:04:59,710
 good things are not practiced by one, then one must be

87
00:04:59,710 --> 00:05:01,160
 watched over by me.

88
00:05:01,160 --> 00:05:03,850
 But when good things are practiced, then I need not look

89
00:05:03,850 --> 00:05:05,020
 after one thinking.

90
00:05:05,020 --> 00:05:06,840
 He can now look after himself.

91
00:05:06,840 --> 00:05:08,840
 He is done with foolishness.

92
00:05:08,840 --> 00:05:10,840
 Thank you.

93
00:05:10,840 --> 00:05:12,840
 Thank you.

94
00:05:12,840 --> 00:05:18,840
 Give me a second here.

95
00:05:18,840 --> 00:05:38,840
 I'm just doing something.

96
00:05:38,840 --> 00:05:44,840
 Just take a second.

97
00:05:44,840 --> 00:05:49,840
 Okay.

98
00:05:49,840 --> 00:05:52,840
 Yes.

99
00:05:52,840 --> 00:06:13,060
 So this is actually a variant on a popular teaching of the

100
00:06:13,060 --> 00:06:13,340
 Buddha.

101
00:06:13,340 --> 00:06:23,140
 Where he is asked whether he teaches people in, whether he

102
00:06:23,140 --> 00:06:27,840
 engages in harsh speech.

103
00:06:27,840 --> 00:06:33,190
 And the Buddha said, "No, I don't engage in harsh speech,

104
00:06:33,190 --> 00:06:35,880
 like speech designed to hurt

105
00:06:35,880 --> 00:06:38,380
 others."

106
00:06:38,380 --> 00:06:46,460
 And then he was asked, "Well, isn't it true that you have

107
00:06:46,460 --> 00:06:48,840
 said this or said that, that

108
00:06:48,840 --> 00:06:49,840
 hurt other people?"

109
00:06:49,840 --> 00:06:55,010
 And he said, "Well, sometimes you have to give people a

110
00:06:55,010 --> 00:06:56,840
 hard teaching."

111
00:06:56,840 --> 00:07:04,830
 It's the, in the Digha Nikaya there's this story, very

112
00:07:04,830 --> 00:07:06,560
 similar.

113
00:07:06,560 --> 00:07:14,260
 The question is whether you should ever, whether you should

114
00:07:14,260 --> 00:07:17,200
 ever use tough love.

115
00:07:17,200 --> 00:07:24,480
 And so he draws this comparison and he points out that

116
00:07:24,480 --> 00:07:28,960
 sometimes you'll do something to,

117
00:07:28,960 --> 00:07:32,510
 even to hurt, even to something that hurts if you know that

118
00:07:32,510 --> 00:07:34,240
 it's actually going to benefit

119
00:07:34,240 --> 00:07:35,240
 in the long run.

120
00:07:35,240 --> 00:07:39,750
 And I think to some extent you could apply that to Buddhism

121
00:07:39,750 --> 00:07:41,280
 because we tell meditators

122
00:07:41,280 --> 00:07:44,640
 to meditate, right?

123
00:07:44,640 --> 00:07:48,560
 To sit still when you have pain, not to relent.

124
00:07:48,560 --> 00:07:51,320
 And so by saying that, we're actually telling you to,

125
00:07:51,320 --> 00:07:53,160
 telling you something that's going

126
00:07:53,160 --> 00:07:56,120
 to make you feel pain, right?

127
00:07:56,120 --> 00:07:57,870
 Meditation is something that's probably going to bring a

128
00:07:57,870 --> 00:07:58,840
 lot of people a lot of pain.

129
00:07:58,840 --> 00:08:02,490
 So you can say, "Wow, these Buddhist meditation teachers

130
00:08:02,490 --> 00:08:04,540
 making a lot of bad karma hurting

131
00:08:04,540 --> 00:08:09,120
 their students, right?"

132
00:08:09,120 --> 00:08:13,570
 So this is the whole old adage, "Has to hurt if it's to

133
00:08:13,570 --> 00:08:14,080
 heal."

134
00:08:14,080 --> 00:08:16,400
 But to some extent we defend ourselves.

135
00:08:16,400 --> 00:08:20,060
 And I think there's a definite line that the Buddha would

136
00:08:20,060 --> 00:08:21,180
 never cross.

137
00:08:21,180 --> 00:08:24,400
 Like if you look at how the Buddha, what the Buddha uses to

138
00:08:24,400 --> 00:08:26,400
 compare himself, he talks about

139
00:08:26,400 --> 00:08:27,400
 himself.

140
00:08:27,400 --> 00:08:30,070
 He doesn't say, "Well, I was, so I would draw blood on my

141
00:08:30,070 --> 00:08:30,880
 students."

142
00:08:30,880 --> 00:08:33,720
 Obviously he would never do that.

143
00:08:33,720 --> 00:08:39,160
 But he will watch over them.

144
00:08:39,160 --> 00:08:43,980
 So he uses a much more tame comparison.

145
00:08:43,980 --> 00:08:47,710
 Like the worst the Buddha would do according to the Digha,

146
00:08:47,710 --> 00:08:50,120
 the story in the Dighan dikaya.

147
00:08:50,120 --> 00:08:52,260
 Actually I'm not sure, I think now that I think it was in

148
00:08:52,260 --> 00:08:55,160
 the Majjhima nikaya, not the

149
00:08:55,160 --> 00:08:56,160
 Dighan nikaya.

150
00:08:56,160 --> 00:09:01,160
 It's the prince Abhaya, I think.

151
00:09:01,160 --> 00:09:09,400
 Abhaya sutta and then Majjhima nikaya.

152
00:09:09,400 --> 00:09:12,520
 I don't remember.

153
00:09:12,520 --> 00:09:14,920
 See, my memory is going.

154
00:09:14,920 --> 00:09:18,280
 So they say, "I'm going to get old."

155
00:09:18,280 --> 00:09:20,440
 It shouldn't go like that though.

156
00:09:20,440 --> 00:09:23,760
 It's just my mind has got other things in it.

157
00:09:23,760 --> 00:09:28,760
 Like peace walks and stuff.

158
00:09:28,760 --> 00:09:37,080
 But the prince Abhaya, same story.

159
00:09:37,080 --> 00:09:43,640
 But to prince Abhaya he says that he would, how would he,

160
00:09:43,640 --> 00:09:47,200
 Abhaya I think is the one where

161
00:09:47,200 --> 00:09:50,080
 he trains horses.

162
00:09:50,080 --> 00:09:52,200
 And he says, "Well, what would you do if you had a..."

163
00:09:52,200 --> 00:09:54,480
 No, no, maybe I'm mixing them up now.

164
00:09:54,480 --> 00:09:56,040
 Oh, that's terrible.

165
00:09:56,040 --> 00:10:00,830
 Anyway, one point the Buddha says, "How would he, how he

166
00:10:00,830 --> 00:10:02,880
 compares this when the child has

167
00:10:02,880 --> 00:10:06,160
 the stone in its mouth or the toy in its mouth?"

168
00:10:06,160 --> 00:10:10,650
 He said, "When I have a, I give a harsh teaching, I will

169
00:10:10,650 --> 00:10:13,480
 tell them, tell my students."

170
00:10:13,480 --> 00:10:16,480
 I'm mixing these up.

171
00:10:16,480 --> 00:10:17,480
 That's terrible.

172
00:10:17,480 --> 00:10:19,120
 This is what happens.

173
00:10:19,120 --> 00:10:24,530
 I see in order to really address this I have to study first

174
00:10:24,530 --> 00:10:24,680
.

175
00:10:24,680 --> 00:10:27,760
 But my point being he would never, he doesn't actually harm

176
00:10:27,760 --> 00:10:28,720
 his students.

177
00:10:28,720 --> 00:10:31,880
 He doesn't do something quite so drastic as like pulling

178
00:10:31,880 --> 00:10:33,200
 this out of their mouth, but

179
00:10:33,200 --> 00:10:35,160
 he'll tell people don't do that.

180
00:10:35,160 --> 00:10:37,830
 He tells his students don't do this, don't do that, which

181
00:10:37,830 --> 00:10:39,080
 is considered to be a harsh

182
00:10:39,080 --> 00:10:44,520
 teaching as opposed to telling people to do this, do that.

183
00:10:44,520 --> 00:10:49,850
 It's an interesting distinction, especially for a teacher

184
00:10:49,850 --> 00:10:52,400
 because ideally you don't ever

185
00:10:52,400 --> 00:10:56,330
 want to have to tell your students don't do this, don't do

186
00:10:56,330 --> 00:10:56,680
 that.

187
00:10:56,680 --> 00:10:58,880
 Ideally you just want to be able to do this, do that, and

188
00:10:58,880 --> 00:11:00,160
 then they'll say, "Okay, I'll

189
00:11:00,160 --> 00:11:02,880
 do that," and then they do it.

190
00:11:02,880 --> 00:11:06,840
 The problem is we stray.

191
00:11:06,840 --> 00:11:10,270
 So when meditators do wrong things you have to say, "Don't

192
00:11:10,270 --> 00:11:12,000
 do this, don't do that."

193
00:11:12,000 --> 00:11:15,720
 It's an interesting distinction that prefer, the nicer

194
00:11:15,720 --> 00:11:17,880
 thing, the more ideal is when we

195
00:11:17,880 --> 00:11:20,320
 just have to tell people what to do and we never have to

196
00:11:20,320 --> 00:11:21,760
 tell people what not to do.

197
00:11:21,760 --> 00:11:26,160
 Because ideally we're all doing good things.

198
00:11:26,160 --> 00:11:35,080
 But that's an example of what the Buddha means by a harsh

199
00:11:35,080 --> 00:11:37,280
 teaching.

200
00:11:37,280 --> 00:11:41,360
 And another part of this is at the end I like how this

201
00:11:41,360 --> 00:11:44,360
 talks about watching over because

202
00:11:44,360 --> 00:11:49,700
 it's an interesting aspect of the Buddha's teaching that we

203
00:11:49,700 --> 00:11:52,560
 don't really follow our students.

204
00:11:52,560 --> 00:11:55,470
 It's very much about we're trying to make our students

205
00:11:55,470 --> 00:11:56,440
 independent.

206
00:11:56,440 --> 00:11:58,440
 So that's what you should become out of this.

207
00:11:58,440 --> 00:12:01,190
 If you find as you practice you're more and more clinging

208
00:12:01,190 --> 00:12:02,600
 to your teacher, it's a bit

209
00:12:02,600 --> 00:12:04,440
 of a sign of a problem.

210
00:12:04,440 --> 00:12:09,140
 It's a sign that you might be, well, in the long run, in

211
00:12:09,140 --> 00:12:11,840
 the beginning of course you have

212
00:12:11,840 --> 00:12:16,960
 to but ideally you want to become independent.

213
00:12:16,960 --> 00:12:20,040
 Like you in some ways become a clone of the Buddha.

214
00:12:20,040 --> 00:12:23,430
 It's not like you become independent and you start making

215
00:12:23,430 --> 00:12:24,920
 up your own religion.

216
00:12:24,920 --> 00:12:27,040
 That's a sign that you didn't really get it.

217
00:12:27,040 --> 00:12:33,400
 But you take on so much from the Buddha, from the Sangha

218
00:12:33,400 --> 00:12:37,160
 that you become another, almost

219
00:12:37,160 --> 00:12:38,160
 like a clone.

220
00:12:38,160 --> 00:12:41,880
 It's just kind of a scary thing to say, I suppose we're

221
00:12:41,880 --> 00:12:43,920
 just making an army of clones

222
00:12:43,920 --> 00:12:44,920
 or something.

223
00:12:44,920 --> 00:12:47,960
 But it kind of is, you know?

224
00:12:47,960 --> 00:12:50,320
 There's only one truth and we claim to have it.

225
00:12:50,320 --> 00:12:53,190
 And so if you believe that and if you practice so that you

226
00:12:53,190 --> 00:12:55,120
 realize, hey, yeah, they've got

227
00:12:55,120 --> 00:12:58,530
 the truth, then you just wind up saying the same things as

228
00:12:58,530 --> 00:13:00,220
 your teacher did and acting

229
00:13:00,220 --> 00:13:05,000
 the same way, having the same sort of character in the

230
00:13:05,000 --> 00:13:07,760
 sense of being at peace with yourself,

231
00:13:07,760 --> 00:13:16,760
 comfortable, mindful, with a clear mind, so on.

232
00:13:16,760 --> 00:13:19,760
 So he ends on an interesting note.

233
00:13:19,760 --> 00:13:24,000
 He says, you don't have to watch over.

234
00:13:24,000 --> 00:13:26,470
 And it's interesting because it's an interesting comparison

235
00:13:26,470 --> 00:13:27,680
 with the boy because yes, young

236
00:13:27,680 --> 00:13:30,210
 children you have to look after them and that's what we are

237
00:13:30,210 --> 00:13:30,440
.

238
00:13:30,440 --> 00:13:34,860
 We are children when we come into the religion, when we

239
00:13:34,860 --> 00:13:36,800
 come into the practice.

240
00:13:36,800 --> 00:13:39,840
 And as you practice, you start to grow.

241
00:13:39,840 --> 00:13:44,080
 Sariputta Moggallana had a relationship like this.

242
00:13:44,080 --> 00:13:47,570
 Sariputta was like the one who gave birth, like a mother

243
00:13:47,570 --> 00:13:49,680
 who gives birth because he would

244
00:13:49,680 --> 00:13:52,350
 lead his students up to Sotapana and then he would set them

245
00:13:52,350 --> 00:13:53,560
 free, but he would send

246
00:13:53,560 --> 00:13:55,480
 them to Moggallana.

247
00:13:55,480 --> 00:13:57,920
 So Moggallana was like the nurse mate.

248
00:13:57,920 --> 00:14:01,760
 He would take them all the way to Arahanshup.

249
00:14:01,760 --> 00:14:04,520
 This was the relationship they had.

250
00:14:04,520 --> 00:14:08,940
 Sariputta would do arguably the harder work trying to just

251
00:14:08,940 --> 00:14:11,120
 straighten their views.

252
00:14:11,120 --> 00:14:15,390
 Once they were on the straight path, Moggallana would

253
00:14:15,390 --> 00:14:18,120
 encourage them and raise them.

254
00:14:18,120 --> 00:14:21,650
 I guess you have to ask which is the harder work, giving

255
00:14:21,650 --> 00:14:22,760
 birth or raising.

256
00:14:22,760 --> 00:14:24,960
 It's an interesting question.

257
00:14:24,960 --> 00:14:26,520
 Robin, what would you say?

258
00:14:26,520 --> 00:14:27,960
 You've done both, right?

259
00:14:27,960 --> 00:14:28,960
 Yes.

260
00:14:28,960 --> 00:14:31,840
 One is short and intense and one is long and intense.

261
00:14:31,840 --> 00:14:34,730
 You've got the nine months in womb, the nine months

262
00:14:34,730 --> 00:14:36,080
 carrying the child.

263
00:14:36,080 --> 00:14:37,960
 How's that not so difficult?

264
00:14:37,960 --> 00:14:40,360
 It can be difficult.

265
00:14:40,360 --> 00:14:42,120
 The beginning and the end is difficult.

266
00:14:42,120 --> 00:14:43,280
 The middle is not so bad.

267
00:14:43,280 --> 00:14:46,190
 But honestly, raising the child is probably the harder job,

268
00:14:46,190 --> 00:14:46,720
 right?

269
00:14:46,720 --> 00:14:47,720
 More tiring, sure.

270
00:14:47,720 --> 00:14:51,960
 I don't think it's quite that way because remember we're

271
00:14:51,960 --> 00:14:54,120
 dealing with Sotapanas.

272
00:14:54,120 --> 00:15:02,440
 It's not quite exact analogy.

273
00:15:02,440 --> 00:15:05,880
 I guess you could say that raising is maybe more rewarding.

274
00:15:05,880 --> 00:15:10,200
 I don't want to compare too much.

275
00:15:10,200 --> 00:15:18,040
 I've never given birth.

276
00:15:18,040 --> 00:15:20,040
 So anyway, that's our quote for today.

277
00:15:20,040 --> 00:15:23,720
 Do you have any questions today before I have to go?

278
00:15:23,720 --> 00:15:26,670
 I might be interrupted at any moment and have to get in a

279
00:15:26,670 --> 00:15:27,720
 vehicle and go.

280
00:15:27,720 --> 00:15:28,720
 Sure.

281
00:15:28,720 --> 00:15:32,320
 We do have one question.

282
00:15:32,320 --> 00:15:35,040
 Will the meditation tradition that you teach allow me to

283
00:15:35,040 --> 00:15:37,280
 investigate the truth about reincarnation

284
00:15:37,280 --> 00:15:38,280
 for myself?

285
00:15:38,280 --> 00:15:39,280
 No.

286
00:15:39,280 --> 00:15:42,720
 There is no truth in reincarnation.

287
00:15:42,720 --> 00:15:46,320
 Well, I know what you're talking about though.

288
00:15:46,320 --> 00:15:49,650
 You're talking about the continuation of the mind at the

289
00:15:49,650 --> 00:15:50,320
 moment of death.

290
00:15:50,320 --> 00:15:52,620
 And some extended will because you'll start to see that

291
00:15:52,620 --> 00:15:54,160
 reality is just moment after moment

292
00:15:54,160 --> 00:15:55,160
 of experience.

293
00:15:55,160 --> 00:15:58,780
 And you'll lose this idea that somehow it's dependent on

294
00:15:58,780 --> 00:15:59,720
 the brain.

295
00:15:59,720 --> 00:16:02,800
 You'll see how the mind can continue on from moment to

296
00:16:02,800 --> 00:16:05,080
 moment through the power of craving,

297
00:16:05,080 --> 00:16:07,720
 how it can give rise to new becoming.

298
00:16:07,720 --> 00:16:09,440
 You'll see that from moment to moment.

299
00:16:09,440 --> 00:16:12,950
 And so it'll give you a framework by which you could

300
00:16:12,950 --> 00:16:15,000
 understand the continuation of that

301
00:16:15,000 --> 00:16:17,560
 consciousness even when the body fails.

302
00:16:17,560 --> 00:16:19,000
 It's not called reincarnation.

303
00:16:19,000 --> 00:16:22,660
 It's just we don't-- it'll help you let go of the idea of

304
00:16:22,660 --> 00:16:23,480
 death.

305
00:16:23,480 --> 00:16:25,360
 That's more apt.

306
00:16:25,360 --> 00:16:30,800
 Is it OK to meditate on a chair?

307
00:16:30,800 --> 00:16:31,800
 Yeah.

308
00:16:31,800 --> 00:16:34,320
 That's fine.

309
00:16:34,320 --> 00:16:39,620
 You might want to try to learn how to sit on the floor

310
00:16:39,620 --> 00:16:43,120
 because it can be beneficial.

311
00:16:43,120 --> 00:16:46,680
 Actually sit in meditation pose on my chairs now too.

312
00:16:46,680 --> 00:16:49,830
 Hey, could you guys try-- people who are watching or at the

313
00:16:49,830 --> 00:16:51,440
 meditation page, could you guys

314
00:16:51,440 --> 00:16:57,240
 try clicking the Anamudana joined hands?

315
00:16:57,240 --> 00:17:01,120
 If you like what someone says, I'd like to test this out.

316
00:17:01,120 --> 00:17:05,280
 If you like what someone says, just click and let's see if

317
00:17:05,280 --> 00:17:06,920
 it actually works.

318
00:17:06,920 --> 00:17:09,520
 Someone has a good question like Panagali had a good

319
00:17:09,520 --> 00:17:10,280
 question.

320
00:17:10,280 --> 00:17:11,280
 So, oh, there we go.

321
00:17:11,280 --> 00:17:12,280
 Two pluses.

322
00:17:12,280 --> 00:17:13,280
 OK.

323
00:17:13,280 --> 00:17:14,280
 So it is working.

324
00:17:14,280 --> 00:17:15,280
 Yay.

325
00:17:15,280 --> 00:17:17,560
 Don't just click them randomly, please.

326
00:17:17,560 --> 00:17:20,940
 It's more if you think someone said something or asked

327
00:17:20,940 --> 00:17:22,720
 something worth asking.

328
00:17:22,720 --> 00:17:26,380
 Bhandi, is it unwholesome for a monk to whip his disciple

329
00:17:26,380 --> 00:17:28,200
 for being late to meditation

330
00:17:28,200 --> 00:17:31,960
 practice with the mindset that he wants said disciple to

331
00:17:31,960 --> 00:17:33,180
 meditate more?

332
00:17:33,180 --> 00:17:37,420
 This is apparently something that happens at monasteries in

333
00:17:37,420 --> 00:17:38,240
 Bhutan.

334
00:17:38,240 --> 00:17:45,200
 I would argue strongly that it's probably unwholesome.

335
00:17:45,200 --> 00:17:47,560
 But more on the delusion than on the anger.

336
00:17:47,560 --> 00:17:50,720
 I would say there probably is some anger involved, but more

337
00:17:50,720 --> 00:17:52,080
 on the delusion side.

338
00:17:52,080 --> 00:17:55,310
 The idea that somehow that's going to work, it's like a

339
00:17:55,310 --> 00:17:55,960
 trick.

340
00:17:55,960 --> 00:17:57,580
 I don't agree.

341
00:17:57,580 --> 00:18:00,600
 This is why Zen meditators snap.

342
00:18:00,600 --> 00:18:01,600
 They call it Satori.

343
00:18:01,600 --> 00:18:03,500
 I don't believe it's Satori.

344
00:18:03,500 --> 00:18:05,800
 When you wind them so tight that they snap.

345
00:18:05,800 --> 00:18:08,280
 I don't know.

346
00:18:08,280 --> 00:18:10,440
 I mean, that's how I look at it and I shouldn't be too

347
00:18:10,440 --> 00:18:13,760
 critical of someone else's tradition.

348
00:18:13,760 --> 00:18:14,800
 That's my opinion.

349
00:18:14,800 --> 00:18:17,840
 It's not something I would ever do for that reason.

350
00:18:17,840 --> 00:18:22,620
 But they, I'm sure, have arguments, very persuasive

351
00:18:22,620 --> 00:18:25,040
 arguments in favor of it.

352
00:18:25,040 --> 00:18:34,880
 That's fine.

353
00:18:34,880 --> 00:18:45,240
 I'm not sure if this was a question for you or just in

354
00:18:45,240 --> 00:18:50,720
 general, but it also says, "Anybody

355
00:18:50,720 --> 00:18:54,940
 know resources discussing safety concerns as in damaging

356
00:18:54,940 --> 00:18:56,960
 the knee joints?"

357
00:18:56,960 --> 00:18:58,960
 No.

358
00:18:58,960 --> 00:19:04,080
 No, I don't think there's any safety concerns.

359
00:19:04,080 --> 00:19:06,040
 You mean in meditating?

360
00:19:06,040 --> 00:19:08,960
 I'm assuming that's what he's referring to, yes.

361
00:19:08,960 --> 00:19:10,760
 I don't think there's any.

362
00:19:10,760 --> 00:19:13,170
 If you've got really problem, you know, if your legs are

363
00:19:13,170 --> 00:19:14,520
 causing a real problem, then

364
00:19:14,520 --> 00:19:19,920
 yeah, you should move, go slowly into sitting cross-legged.

365
00:19:19,920 --> 00:19:23,870
 That's the only thing is trying to sit cross-legged when

366
00:19:23,870 --> 00:19:26,560
 you have bad knees or something or bad

367
00:19:26,560 --> 00:19:27,560
 back.

368
00:19:27,560 --> 00:19:32,120
 So you should be a little bit concerned with propping up.

369
00:19:32,120 --> 00:19:34,720
 A little bit, pain is not a problem.

370
00:19:34,720 --> 00:19:38,730
 Pain is something you should learn about because it's the

371
00:19:38,730 --> 00:19:40,880
 one that we're going to give rise

372
00:19:40,880 --> 00:19:42,160
 to anger towards.

373
00:19:42,160 --> 00:19:44,520
 So we want to learn to overcome that.

374
00:19:44,520 --> 00:19:47,250
 But if it's intense and it's actually going to lead, it's

375
00:19:47,250 --> 00:19:48,760
 the difference between pain

376
00:19:48,760 --> 00:19:50,600
 and an injury.

377
00:19:50,600 --> 00:19:52,600
 You can injure yourself.

378
00:19:52,600 --> 00:19:54,680
 You can practice meditation and feel pain.

379
00:19:54,680 --> 00:19:57,700
 But if you practice meditation, if you stretch and strain

380
00:19:57,700 --> 00:19:59,560
 yourself to the point of an injury,

381
00:19:59,560 --> 00:20:05,600
 well, that's not helpful in any way.

382
00:20:05,600 --> 00:20:10,360
 Do people feel a sublime happiness just after a cessation?

383
00:20:10,360 --> 00:20:13,400
 Generally, yeah.

384
00:20:13,400 --> 00:20:17,610
 They'll feel a little bit disoriented maybe because in the

385
00:20:17,610 --> 00:20:20,000
 sense like what just happened,

386
00:20:20,000 --> 00:20:25,730
 like it was beyond anything, no concept of what just

387
00:20:25,730 --> 00:20:26,280
 happened.

388
00:20:26,280 --> 00:20:33,120
 And then they'll feel peace often for hours or days as a

389
00:20:33,120 --> 00:20:34,120
 result of it.

390
00:20:34,120 --> 00:20:37,270
 In the beginning, it's just this amazing sense of the

391
00:20:37,270 --> 00:20:39,440
 change that's gone through them.

392
00:20:39,440 --> 00:20:43,530
 And so there's great bliss and peace and happiness and just

393
00:20:43,530 --> 00:20:45,640
 a sense of awe and a new way of looking

394
00:20:45,640 --> 00:20:46,640
 at the world.

395
00:20:46,640 --> 00:20:50,600
 And it wears off.

396
00:20:50,600 --> 00:20:57,600
 The novelty of it wears off, but the peace stays.

397
00:20:57,600 --> 00:21:15,840
 Whoa, we've got a long line of meditators here.

398
00:21:15,840 --> 00:21:18,400
 We do.

399
00:21:18,400 --> 00:21:22,920
 And most people are green.

400
00:21:22,920 --> 00:21:39,080
 It's taking more time to click on everybody.

401
00:21:39,080 --> 00:21:42,580
 How is the first official residential meditation course

402
00:21:42,580 --> 00:21:44,760
 gearing up that starts about a week

403
00:21:44,760 --> 00:21:45,760
 from now?

404
00:21:45,760 --> 00:21:49,080
 I don't know if anyone's actually coming.

405
00:21:49,080 --> 00:21:51,280
 I think there may be one person signed up.

406
00:21:51,280 --> 00:21:56,060
 I'm just going to check on Facebook because actually I don

407
00:21:56,060 --> 00:21:58,040
't think in Facebook it.

408
00:21:58,040 --> 00:22:00,040
 Where are my events?

409
00:22:00,040 --> 00:22:04,040
 Is that on my page?

410
00:22:04,040 --> 00:22:06,560
 How does this work?

411
00:22:06,560 --> 00:22:10,400
 It should be on your page.

412
00:22:10,400 --> 00:22:13,400
 Events, upcoming events.

413
00:22:13,400 --> 00:22:16,400
 Only one person.

414
00:22:16,400 --> 00:22:17,400
 Four.

415
00:22:17,400 --> 00:22:18,400
 Wait, four going.

416
00:22:18,400 --> 00:22:19,400
 Oh, I see.

417
00:22:19,400 --> 00:22:20,400
 Uh-huh.

418
00:22:20,400 --> 00:22:24,400
 Well, one lives in New York.

419
00:22:24,400 --> 00:22:28,400
 I think I contacted her already.

420
00:22:28,400 --> 00:22:37,680
 Another looks like he lives in India, I said.

421
00:22:37,680 --> 00:22:41,360
 Lives in Bangladesh.

422
00:22:41,360 --> 00:22:43,800
 That's a road trip.

423
00:22:43,800 --> 00:22:49,080
 I'm not convinced that he's coming.

424
00:22:49,080 --> 00:22:53,710
 We discussed it above, but would leg falling asleep going

425
00:22:53,710 --> 00:22:56,280
 numb be good pain to sit through

426
00:22:56,280 --> 00:22:58,080
 and take note of?

427
00:22:58,080 --> 00:23:01,720
 Would sitting through what?

428
00:23:01,720 --> 00:23:03,990
 Through your leg falling asleep or going numb, should you

429
00:23:03,990 --> 00:23:04,840
 sit through that?

430
00:23:04,840 --> 00:23:05,840
 Yeah.

431
00:23:05,840 --> 00:23:07,600
 Just take note of it.

432
00:23:07,600 --> 00:23:15,560
 That won't cause permanent damage.

433
00:23:15,560 --> 00:23:18,480
 People do that all the time.

434
00:23:18,480 --> 00:23:22,160
 Just be careful when you stand up afterwards.

435
00:23:22,160 --> 00:23:24,510
 The danger can come if you stand up and try to stand on it

436
00:23:24,510 --> 00:23:25,560
 and break your leg.

437
00:23:25,560 --> 00:23:26,560
 It can happen.

438
00:23:26,560 --> 00:23:28,320
 Yeah, I've had that happen.

439
00:23:28,320 --> 00:23:30,750
 I stood up too quickly not realizing how much my foot was

440
00:23:30,750 --> 00:23:33,920
 asleep and I fell right back down.

441
00:23:33,920 --> 00:23:34,920
 You can break your leg.

442
00:23:34,920 --> 00:23:35,920
 People have done it.

443
00:23:35,920 --> 00:23:36,920
 Yeah.

444
00:23:36,920 --> 00:23:39,950
 You recently read that some Buddhist monks sleep in the

445
00:23:39,950 --> 00:23:41,200
 sitting position.

446
00:23:41,200 --> 00:23:44,880
 Have you ever tried this, Bantai?

447
00:23:44,880 --> 00:23:47,080
 Yeah.

448
00:23:47,080 --> 00:23:52,000
 Yeah, that's perfectly valid.

449
00:23:52,000 --> 00:23:58,210
 It's a good way to keep the sitters practice, the non-lying

450
00:23:58,210 --> 00:24:00,160
 down practice.

451
00:24:00,160 --> 00:24:08,240
 We did seven days not lying down.

452
00:24:08,240 --> 00:24:13,510
 The last two days were, the last two nights were for my

453
00:24:13,510 --> 00:24:15,960
 teacher's birthday.

454
00:24:15,960 --> 00:24:19,140
 It was the seventh day, seventh night was the night before

455
00:24:19,140 --> 00:24:20,480
 my teacher's birthday.

456
00:24:20,480 --> 00:24:23,480
 I got the monks in the monastery together to do it and we

457
00:24:23,480 --> 00:24:25,080
 all sat outside of his room

458
00:24:25,080 --> 00:24:27,930
 meditating and when he came out in the morning, I think I

459
00:24:27,930 --> 00:24:29,680
 had just woken up from like three

460
00:24:29,680 --> 00:24:35,430
 hours of sitting sleep and he came out and he was impressed

461
00:24:35,430 --> 00:24:35,840
.

462
00:24:35,840 --> 00:24:37,840
 He said, "Oh, very good effort.

463
00:24:37,840 --> 00:24:38,840
 Congratulations."

464
00:24:38,840 --> 00:24:40,600
 He was very happy to see us.

465
00:24:40,600 --> 00:24:41,600
 That was nice.

466
00:24:41,600 --> 00:24:44,390
 Right outside of his room, his bedroom because his bedroom

467
00:24:44,390 --> 00:24:46,280
 at the time was inside this meditation

468
00:24:46,280 --> 00:24:47,280
 hall.

469
00:24:47,280 --> 00:24:50,490
 We were in the meditation hall like 10 monks doing an all-

470
00:24:50,490 --> 00:24:52,200
night meditation practice in

471
00:24:52,200 --> 00:24:53,200
 his honor.

472
00:24:53,200 --> 00:24:56,240
 Was he aware that you'd been doing that for seven days?

473
00:24:56,240 --> 00:24:57,240
 No.

474
00:24:57,240 --> 00:24:59,160
 I don't think I even ever told him.

475
00:24:59,160 --> 00:25:04,080
 I had been up at Doy Soutêpe doing it and then I came down

476
00:25:04,080 --> 00:25:05,920
 for his birthday.

477
00:25:05,920 --> 00:25:09,720
 What do you think of Sam Harris' approach to mindfulness?

478
00:25:09,720 --> 00:25:16,350
 I don't really know it that well except in an end of faith,

479
00:25:16,350 --> 00:25:19,080
 Sam Harris, right?

480
00:25:19,080 --> 00:25:22,030
 In the end of faith, he goes somewhere that I don't think

481
00:25:22,030 --> 00:25:22,760
 is valid.

482
00:25:22,760 --> 00:25:26,280
 He talks a lot about compassion.

483
00:25:26,280 --> 00:25:28,620
 Just today someone was telling me about his new book called

484
00:25:28,620 --> 00:25:29,240
 Waking Up.

485
00:25:29,240 --> 00:25:32,280
 Well, if someone wants to send it to me, I'd be happy to

486
00:25:32,280 --> 00:25:33,760
 read it and talk about it.

487
00:25:33,760 --> 00:25:36,120
 I don't have that book.

488
00:25:36,120 --> 00:25:39,200
 Apparently, Waking Up is more about mindfulness.

489
00:25:39,200 --> 00:25:40,200
 Okay.

490
00:25:40,200 --> 00:25:41,200
 I'm going to read it.

491
00:25:41,200 --> 00:25:42,200
 I'm going to read it.

492
00:25:42,200 --> 00:25:43,200
 I'm going to read it.

493
00:25:43,200 --> 00:25:44,200
 I'm going to read it.

494
00:25:44,200 --> 00:25:45,200
 I'm going to read it.

495
00:25:45,200 --> 00:25:46,200
 I'm going to read it.

496
00:25:46,200 --> 00:25:47,200
 I'm going to read it.

497
00:25:47,200 --> 00:25:48,200
 I'm going to read it.

498
00:25:48,200 --> 00:25:49,200
 I'm going to read it.

499
00:25:49,200 --> 00:25:50,200
 I'm going to read it.

500
00:25:50,200 --> 00:25:51,200
 I'm going to read it.

501
00:25:51,200 --> 00:25:52,200
 I'm going to read it.

502
00:25:52,200 --> 00:25:53,200
 I'm going to read it.

503
00:25:53,200 --> 00:25:54,200
 I'm going to read it.

504
00:25:54,200 --> 00:25:55,200
 I'm going to read it.

505
00:25:55,200 --> 00:25:56,200
 I'm going to read it.

506
00:25:56,200 --> 00:25:57,200
 I'm going to read it.

507
00:25:57,200 --> 00:25:58,200
 I'm going to read it.

508
00:25:58,200 --> 00:25:59,200
 I'm going to read it.

509
00:25:59,200 --> 00:26:00,200
 I'm going to read it.

510
00:26:00,200 --> 00:26:01,200
 I'm going to read it.

511
00:26:01,200 --> 00:26:02,200
 I'm going to read it.

512
00:26:02,200 --> 00:26:03,200
 I'm going to read it.

513
00:26:03,200 --> 00:26:04,200
 I'm going to read it.

514
00:26:05,200 --> 00:26:06,200
 I'm going to read it.

515
00:26:06,200 --> 00:26:07,200
 I'm going to read it.

516
00:26:07,200 --> 00:26:08,200
 I'm going to read it.

517
00:26:08,200 --> 00:26:09,200
 I'm going to read it.

518
00:26:09,200 --> 00:26:10,200
 I'm going to read it.

519
00:26:10,200 --> 00:26:11,200
 I'm going to read it.

520
00:26:11,200 --> 00:26:12,200
 I'm going to read it.

521
00:26:12,200 --> 00:26:13,200
 I'm going to read it.

522
00:26:13,200 --> 00:26:14,200
 I'm going to read it.

523
00:26:14,200 --> 00:26:15,200
 I'm going to read it.

524
00:26:15,200 --> 00:26:16,200
 I'm going to read it.

525
00:26:16,200 --> 00:26:17,200
 I'm going to read it.

526
00:26:17,200 --> 00:26:18,200
 I'm going to read it.

527
00:26:18,200 --> 00:26:19,200
 I'm going to read it.

528
00:26:19,200 --> 00:26:20,200
 I'm going to read it.

529
00:26:20,200 --> 00:26:21,200
 I'm going to read it.

530
00:26:21,200 --> 00:26:22,200
 I'm going to read it.

531
00:26:22,200 --> 00:26:23,200
 I'm going to read it.

532
00:26:23,200 --> 00:26:24,200
 I'm going to read it.

533
00:26:24,200 --> 00:26:25,200
 I'm going to read it.

