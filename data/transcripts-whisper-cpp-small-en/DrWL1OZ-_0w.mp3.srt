1
00:00:00,000 --> 00:00:06,680
 Does the mind have a gender or is it only a property of the

2
00:00:06,680 --> 00:00:07,680
 body?

3
00:00:07,680 --> 00:00:09,760
 Gender is only a property of the body.

4
00:00:09,760 --> 00:00:11,560
 Good question really.

5
00:00:11,560 --> 00:00:14,760
 People don't realize that, but yeah.

6
00:00:14,760 --> 00:00:22,630
 Gender actually has a, in Buddhism, it's a physical, it's

7
00:00:22,630 --> 00:00:25,480
 called itindrya and puri, I

8
00:00:25,480 --> 00:00:29,780
 think puri-sindrya I think is the other one.

9
00:00:29,780 --> 00:00:35,500
 So females have physical, female, faculty, males have the

10
00:00:35,500 --> 00:00:38,080
 male and I suppose you could

11
00:00:38,080 --> 00:00:42,080
 conjecture that some people have both or neither.

12
00:00:42,080 --> 00:00:46,320
 I don't really know how it works.

13
00:00:46,320 --> 00:00:52,880
 But the important point is that yes, it's only physical.

14
00:00:52,880 --> 00:00:56,320
 There's no mental aspect to gender.

15
00:00:56,320 --> 00:01:02,400
 But as to first of all, A, how the physical affects the

16
00:01:02,400 --> 00:01:06,560
 mental because the female physical

17
00:01:06,560 --> 00:01:12,190
 faculty produces certain chemicals and the male faculty

18
00:01:12,190 --> 00:01:15,300
 produces different chemicals

19
00:01:15,300 --> 00:01:18,160
 and those chemicals affect the mind differently.

20
00:01:18,160 --> 00:01:24,780
 And to some extent, I think, I'm not a biologist, but they

21
00:01:24,780 --> 00:01:29,200
 affect the mind in different ways.

22
00:01:29,200 --> 00:01:33,140
 So condition the person's mind in different ways.

23
00:01:33,140 --> 00:01:37,260
 This is why you would expect that females exhibit certain

24
00:01:37,260 --> 00:01:39,600
 characteristics whereas males

25
00:01:39,600 --> 00:01:44,430
 exhibit certain different characteristics in general

26
00:01:44,430 --> 00:01:47,200
 because of the influence of the

27
00:01:47,200 --> 00:01:50,520
 different chemical, I would say, among other things.

28
00:01:50,520 --> 00:01:53,880
 Of course, the physical characteristics, differences

29
00:01:53,880 --> 00:01:56,320
 between the female body and the male body

30
00:01:56,320 --> 00:01:59,700
 will also affect one's behavior indirectly through

31
00:01:59,700 --> 00:02:00,960
 conditioning.

32
00:02:00,960 --> 00:02:08,340
 And then on top of it, you have culture and society that

33
00:02:08,340 --> 00:02:12,440
 highly specializes in gender

34
00:02:12,440 --> 00:02:20,840
 roles and gender expectations and so on.

35
00:02:20,840 --> 00:02:25,730
 The way we look at gender and all of that, of course, has a

36
00:02:25,730 --> 00:02:27,720
 profound impact on the mind.

