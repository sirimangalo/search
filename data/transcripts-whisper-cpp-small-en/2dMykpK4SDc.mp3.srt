1
00:00:00,000 --> 00:00:05,940
 Hello fellow YouTubers, just thought I'd give a shout to

2
00:00:05,940 --> 00:00:09,250
 let everyone know that I'll be starting to give talks in

3
00:00:09,250 --> 00:00:13,690
 Second Life, which is this online virtual reality that you

4
00:00:13,690 --> 00:00:17,000
 can look up at SecondLife.com.

5
00:00:17,000 --> 00:00:21,140
 I know it sounds a little bit strange. I myself was not

6
00:00:21,140 --> 00:00:25,560
 really interested until I found out that there was a Buddha

7
00:00:25,560 --> 00:00:29,890
 Center in Second Life and they have teachers come and give

8
00:00:29,890 --> 00:00:31,000
 talks there.

9
00:00:31,000 --> 00:00:36,210
 And they've asked me to give talks there starting this

10
00:00:36,210 --> 00:00:42,130
 Thursday. So I'll be giving talks almost every day, every

11
00:00:42,130 --> 00:00:48,000
 day except for Mondays and Wednesdays at 12 noon.

12
00:00:48,000 --> 00:00:52,650
 So if you want you can check me out, come and listen to the

13
00:00:52,650 --> 00:00:57,080
 talk at Second Life at the Buddha Center, just look up

14
00:00:57,080 --> 00:01:01,000
 Buddha Center under the places in Second Life.

15
00:01:01,000 --> 00:01:04,880
 And I'm also around there giving instruction on meditation

16
00:01:04,880 --> 00:01:09,100
 if you'd like to talk about your practice, get some advice.

17
00:01:09,100 --> 00:01:11,960
 We can sit and have a chat around a virtual fireplace or

18
00:01:11,960 --> 00:01:14,000
 under virtual trees or so on.

19
00:01:14,000 --> 00:01:18,410
 It's actually a pretty good setting and quite a useful tool

20
00:01:18,410 --> 00:01:23,000
 for teaching and for spreading the meditation teaching.

21
00:01:23,000 --> 00:01:27,840
 So hope to see you there. If you have any trouble, please

22
00:01:27,840 --> 00:01:29,000
 let me know.

23
00:01:29,000 --> 00:01:32,040
 Or if you'd like more information, just give me a shout.

24
00:01:32,040 --> 00:01:34,000
 Okay. Thanks for tuning in and hope to see you there.

25
00:01:35,000 --> 00:01:44,000
 [BLANK_AUDIO]

