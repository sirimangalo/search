1
00:00:00,000 --> 00:00:04,080
 So today we continue on with our study of the Dhammapada,

2
00:00:04,080 --> 00:00:06,440
 going on to verses 19 and 20,

3
00:00:06,440 --> 00:00:10,880
 which comprise a single story.

4
00:00:10,880 --> 00:00:11,880
 Verses are as follows.

5
00:00:11,880 --> 00:00:20,920
 Bhahumpi jay sanghita bhasamano nathakaro hoti naro pamato

6
00:00:20,920 --> 00:00:30,480
 kopova ga wo ganayang pareesang na bhagavah samanyasah hoti

7
00:00:30,480 --> 00:00:39,610
 and 20 apampi jay sanghita bhasamano dhammasah hoti anudham

8
00:00:39,610 --> 00:00:40,200
achari

9
00:00:40,200 --> 00:00:51,240
 ragancha dosancha pahayamohang samma pajano tama pajano

10
00:00:51,240 --> 00:01:04,860
 suimutachito anupadiyano idhavahurangvah satbhagavah samany

11
00:01:04,860 --> 00:01:05,800
asah hoti

12
00:01:05,800 --> 00:01:11,280
 or two long verses.

13
00:01:11,280 --> 00:01:16,280
 So what they mean, bahumpi, bahum means much as opposed to

14
00:01:16,280 --> 00:01:19,320
 apam which means little.

15
00:01:19,320 --> 00:01:23,520
 Bhahumpi jay sanghita bhasamano, speaking or being able to

16
00:01:23,520 --> 00:01:25,800
 teach much that is related

17
00:01:25,800 --> 00:01:31,080
 to the goal or that is of use.

18
00:01:31,080 --> 00:01:35,450
 That nathakaro hoti, being one who doesn't practice

19
00:01:35,450 --> 00:01:41,360
 accordingly, a person who doesn't

20
00:01:41,360 --> 00:01:45,720
 practice accordingly, who is rather bamato, who is

21
00:01:45,720 --> 00:01:51,760
 negligent or heedless.

22
00:01:51,760 --> 00:01:54,940
 So as a person who, though they know a lot and they teach a

23
00:01:54,940 --> 00:01:57,000
 lot, doesn't practice accordingly

24
00:01:57,000 --> 00:02:00,520
 and is negligent.

25
00:02:00,520 --> 00:02:04,400
 Gopo waga wo ganayam kure sango, like a person who looks

26
00:02:04,400 --> 00:02:06,920
 after the cows of another, who counts

27
00:02:06,920 --> 00:02:10,160
 the cows of another person.

28
00:02:10,160 --> 00:02:14,790
 Naba agawasam anyasah hoti, such a person doesn't partake

29
00:02:14,790 --> 00:02:16,760
 of the blessings of the holy

30
00:02:16,760 --> 00:02:21,320
 life or the fruits of the holy life.

31
00:02:21,320 --> 00:02:24,830
 And the opposing verse, apampi jay sanghita bhasamano, a

32
00:02:24,830 --> 00:02:26,640
 person who knows even a little

33
00:02:26,640 --> 00:02:30,600
 bit, even though they know a little bit or are able to

34
00:02:30,600 --> 00:02:32,920
 speak a little bit of the truth.

35
00:02:32,920 --> 00:02:38,050
 But they are dhammasa, they are anudamachari, a person who

36
00:02:38,050 --> 00:02:40,960
 practices the dhamma according

37
00:02:40,960 --> 00:02:44,470
 to the dhamma or in order to realize the dhamma and

38
00:02:44,470 --> 00:02:46,280
 realizes the dhamma.

39
00:02:46,280 --> 00:02:54,150
 Raghan jedbo sangcha vahayam o hang, they abandon, with the

40
00:02:54,150 --> 00:02:54,400
 abandoning of delusion,

41
00:02:54,400 --> 00:02:59,440
 greed, anger and delusion.

42
00:02:59,440 --> 00:03:05,810
 They have clear comprehension, samat pajano and suvimutajid

43
00:03:05,810 --> 00:03:08,760
to, their mind is well freed

44
00:03:08,760 --> 00:03:10,760
 or well liberated.

45
00:03:10,760 --> 00:03:17,140
 Anupadi ano, not clinging to anything, idawa hurangwa, here

46
00:03:17,140 --> 00:03:19,960
 or hereafter, in this life

47
00:03:19,960 --> 00:03:24,080
 or in terms of the future, the future lives.

48
00:03:24,080 --> 00:03:29,210
 Sat bhagavasamanyasohoti, such a person partakes as one who

49
00:03:29,210 --> 00:03:31,400
 partakes in the holy life or in

50
00:03:31,400 --> 00:03:34,080
 the fruits of the holy life.

51
00:03:34,080 --> 00:03:36,920
 So this is word by word translation basically, it means a

52
00:03:36,920 --> 00:03:38,520
 person who, though they know a

53
00:03:38,520 --> 00:03:43,360
 lot, that doesn't practice accordingly and are negligent

54
00:03:43,360 --> 00:03:45,760
 and heedless and so on, they

55
00:03:45,760 --> 00:03:50,160
 don't have a part in the fruit of the teachings.

56
00:03:50,160 --> 00:03:53,860
 But a person who even though they've studied a little bit

57
00:03:53,860 --> 00:03:55,920
 and maybe know very little and

58
00:03:55,920 --> 00:04:01,550
 can explain very little, but still by practicing according

59
00:04:01,550 --> 00:04:04,720
 to the teaching, they become one

60
00:04:04,720 --> 00:04:07,160
 who partakes on the fruit of the truth.

61
00:04:07,160 --> 00:04:10,330
 Now obviously this makes sense, through practicing the

62
00:04:10,330 --> 00:04:12,720
 teachings, if you practice accordingly

63
00:04:12,720 --> 00:04:19,340
 and give up lobha dosamoharaga, lust, dosa, anger and moha,

64
00:04:19,340 --> 00:04:22,360
 delusion, someone who has

65
00:04:22,360 --> 00:04:23,360
 clear comprehension.

66
00:04:23,360 --> 00:04:27,190
 So it's actually practicing the Buddhist teaching on

67
00:04:27,190 --> 00:04:30,320
 mindfulness and becomes suvi-murtajidto,

68
00:04:30,320 --> 00:04:34,080
 has freedom in the mind.

69
00:04:34,080 --> 00:04:36,120
 So this is given in regards to a story.

70
00:04:36,120 --> 00:04:40,110
 Again I'm pulling out the commentary here because it turns

71
00:04:40,110 --> 00:04:42,160
 out that the English is quite

72
00:04:42,160 --> 00:04:46,960
 negligent, pomato, the person who translated it didn't do a

73
00:04:46,960 --> 00:04:48,360
 very good job.

74
00:04:48,360 --> 00:04:50,820
 So in the last story and in this story I realize I'm

75
00:04:50,820 --> 00:04:53,440
 probably going to have to use this book

76
00:04:53,440 --> 00:04:58,760
 to check everyone and actually read the Pali because it's

77
00:04:58,760 --> 00:05:01,760
 not precise, the meaning is not

78
00:05:01,760 --> 00:05:02,760
 as given.

79
00:05:02,760 --> 00:05:04,930
 In the last one it was actually quite important because

80
00:05:04,930 --> 00:05:06,320
 what it said is that a person who's

81
00:05:06,320 --> 00:05:12,860
 reached the second stage, this woman who called her father

82
00:05:12,860 --> 00:05:16,480
 a younger brother, and they say

83
00:05:16,480 --> 00:05:20,520
 the reason she got sick is because she wanted a husband,

84
00:05:20,520 --> 00:05:22,560
 but she was a sakitagami.

85
00:05:22,560 --> 00:05:25,920
 So the idea that she would starve herself because she didn

86
00:05:25,920 --> 00:05:27,360
't have a husband is a bit

87
00:05:27,360 --> 00:05:28,360
 ludicrous.

88
00:05:28,360 --> 00:05:31,390
 She is someone who is very high in teaching and that's not

89
00:05:31,390 --> 00:05:34,320
 actually what the text says.

90
00:05:34,320 --> 00:05:39,220
 This one is not so important but it's mixing up some parts

91
00:05:39,220 --> 00:05:40,000
 of it.

92
00:05:40,000 --> 00:05:42,800
 It's saying that the, anyway I'll read the story and then I

93
00:05:42,800 --> 00:05:44,440
'll explain what the disparity

94
00:05:44,440 --> 00:05:45,440
 is.

95
00:05:45,440 --> 00:05:49,250
 So there were these two friends, inseparable friends, and

96
00:05:49,250 --> 00:05:51,520
 they became monks in the Buddha's

97
00:05:51,520 --> 00:05:52,520
 teaching.

98
00:05:52,520 --> 00:05:56,750
 One was older, one was, they weren't the same age, one

99
00:05:56,750 --> 00:05:58,600
 older, one younger.

100
00:05:58,600 --> 00:06:02,810
 And the Buddha said, so, told them that there are two

101
00:06:02,810 --> 00:06:05,720
 duties in the Buddha's teaching.

102
00:06:05,720 --> 00:06:07,820
 When someone becomes a monk they have two duties.

103
00:06:07,820 --> 00:06:11,810
 One is to practice vipassana and the other is to study the

104
00:06:11,810 --> 00:06:14,080
 text or to memorize the text.

105
00:06:14,080 --> 00:06:17,100
 The older one said, "Well, being old, probably it's going

106
00:06:17,100 --> 00:06:19,720
 to be difficult for me to learn

107
00:06:19,720 --> 00:06:22,290
 all of the teachings and to memorize them and be able to

108
00:06:22,290 --> 00:06:23,160
 recite them.

109
00:06:23,160 --> 00:06:26,200
 Can I just do the vipassana part?"

110
00:06:26,200 --> 00:06:28,440
 Or he said to himself, "I'll just do the vipassana part.

111
00:06:28,440 --> 00:06:32,330
 I'll just do the part of having to see things clearly as

112
00:06:32,330 --> 00:06:33,360
 they are."

113
00:06:33,360 --> 00:06:38,040
 So he took up some rag robes.

114
00:06:38,040 --> 00:06:41,640
 He went and got some rags and put together a set of robes

115
00:06:41,640 --> 00:06:43,400
 and went to the Buddha and

116
00:06:43,400 --> 00:06:45,200
 asked for a meditation teaching.

117
00:06:45,200 --> 00:06:48,250
 And the Buddha gave him a teaching in regards to the

118
00:06:48,250 --> 00:06:50,680
 practice of vipassana meditation all

119
00:06:50,680 --> 00:06:52,800
 the way up to Arahan's ship.

120
00:06:52,800 --> 00:06:55,760
 He went off into the forest and became an Arahan.

121
00:06:55,760 --> 00:06:57,930
 The other one, because he was younger, he said, "Well, I'll

122
00:06:57,930 --> 00:07:00,280
 just do the study side of things."

123
00:07:00,280 --> 00:07:02,950
 So he said, "Okay, I'll take up and learn all of the Buddha

124
00:07:02,950 --> 00:07:03,840
's teaching."

125
00:07:03,840 --> 00:07:06,920
 And he actually was able to memorize all of the tepidaka.

126
00:07:06,920 --> 00:07:09,760
 I believe that is what it says.

127
00:07:09,760 --> 00:07:12,740
 Anyway, he was able to memorize quite a bit of the, if not

128
00:07:12,740 --> 00:07:14,680
 all of the tepidaka, the Buddha's

129
00:07:14,680 --> 00:07:17,400
 teaching.

130
00:07:17,400 --> 00:07:18,960
 But he didn't practice.

131
00:07:18,960 --> 00:07:21,580
 And because he was so famous from his teaching he had lots

132
00:07:21,580 --> 00:07:23,040
 and lots of students coming to

133
00:07:23,040 --> 00:07:24,040
 see him.

134
00:07:24,040 --> 00:07:26,560
 He was a very well-known teacher and well-respected.

135
00:07:26,560 --> 00:07:30,320
 He had lots of something like 18 groups or something.

136
00:07:30,320 --> 00:07:35,120
 I don't know what it says in here.

137
00:07:35,120 --> 00:07:39,080
 And the one in the forest as an Arahan, it so happened that

138
00:07:39,080 --> 00:07:40,960
 a group of monks, they got

139
00:07:40,960 --> 00:07:42,740
 training from the Buddha and then they went off to the

140
00:07:42,740 --> 00:07:43,880
 forest to live and went to where

141
00:07:43,880 --> 00:07:48,280
 he was staying, practiced under him and became Arahan's as

142
00:07:48,280 --> 00:07:49,000
 well.

143
00:07:49,000 --> 00:07:51,600
 When they had become Arahan, they went to see the elder and

144
00:07:51,600 --> 00:07:52,840
 they asked permission to

145
00:07:52,840 --> 00:07:53,840
 go back and see the Buddha.

146
00:07:53,840 --> 00:07:55,840
 They said, "We'd like to go see the Buddha."

147
00:07:55,840 --> 00:07:57,280
 The Buddha said, "Fine.

148
00:07:57,280 --> 00:08:00,280
 Go back and pay respect to the Buddha and pay respect to

149
00:08:00,280 --> 00:08:02,240
 the Buddha's two chief disciples

150
00:08:02,240 --> 00:08:05,000
 and all of the 18 great disciples and then go and pay

151
00:08:05,000 --> 00:08:06,940
 respect to my friend and tell him

152
00:08:06,940 --> 00:08:10,360
 you're paying respect in the name of your teacher."

153
00:08:10,360 --> 00:08:11,360
 So they did this.

154
00:08:11,360 --> 00:08:12,360
 They went to the Buddha and so on.

155
00:08:12,360 --> 00:08:15,130
 And then they went to find his old friend and said, "We pay

156
00:08:15,130 --> 00:08:16,840
 respect in the name of our teacher."

157
00:08:16,840 --> 00:08:19,960
 And he said, "Well, who is your teacher?"

158
00:08:19,960 --> 00:08:22,200
 And he said, "It's your friend who you became a monk

159
00:08:22,200 --> 00:08:23,120
 together with."

160
00:08:23,120 --> 00:08:24,880
 And he said, "He has students.

161
00:08:24,880 --> 00:08:25,880
 What did he teach you?

162
00:08:25,880 --> 00:08:26,880
 Did he teach you?

163
00:08:26,880 --> 00:08:28,200
 What could you have learned from him?

164
00:08:28,200 --> 00:08:33,120
 Could you have learned even one nikai or one pitaka?

165
00:08:33,120 --> 00:08:36,420
 What could you possibly have learned from him?

166
00:08:36,420 --> 00:08:37,420
 Because he doesn't know anything.

167
00:08:37,420 --> 00:08:38,420
 He didn't study.

168
00:08:38,420 --> 00:08:39,420
 How could he?

169
00:08:39,420 --> 00:08:40,420
 What do you mean, teacher?"

170
00:08:40,420 --> 00:08:43,660
 And so he said this kind of thing and then he thought to

171
00:08:43,660 --> 00:08:45,680
 himself, "When this monk comes,

172
00:08:45,680 --> 00:08:48,550
 I'm going to ask him some questions and I'm going to show

173
00:08:48,550 --> 00:08:49,920
 that he show everything."

174
00:08:49,920 --> 00:08:53,780
 And he said, "I want everyone that is a fraud because he

175
00:08:53,780 --> 00:08:55,360
 can't be a teacher.

176
00:08:55,360 --> 00:08:59,320
 He doesn't know anything."

177
00:08:59,320 --> 00:09:03,230
 And it so happened that eventually this senior, the Arahant

178
00:09:03,230 --> 00:09:05,320
, came back to where the Buddha

179
00:09:05,320 --> 00:09:08,810
 was, paid respect to the Buddha and went to see his old

180
00:09:08,810 --> 00:09:10,360
 friend and sat down.

181
00:09:10,360 --> 00:09:15,290
 And the younger monk, who was the study monk, he thought, "

182
00:09:15,290 --> 00:09:18,480
Okay, now I'll ask him some questions

183
00:09:18,480 --> 00:09:19,480
 and I'll show."

184
00:09:19,480 --> 00:09:21,400
 And his students were there and the older monk's students

185
00:09:21,400 --> 00:09:22,400
 were there and he said, "In

186
00:09:22,400 --> 00:09:25,500
 front of all these people, we'll show everyone with this

187
00:09:25,500 --> 00:09:26,000
 monk.

188
00:09:26,000 --> 00:09:28,280
 Let this monk know nothing."

189
00:09:28,280 --> 00:09:29,480
 And the Buddha found out about this.

190
00:09:29,480 --> 00:09:32,150
 He knew what was going on and got the gist of it and

191
00:09:32,150 --> 00:09:34,360
 realized that if he let this happen,

192
00:09:34,360 --> 00:09:35,920
 this younger monk might go to hell.

193
00:09:35,920 --> 00:09:38,330
 And he says, "Niriyang," he says in here somewhere that, "I

194
00:09:38,330 --> 00:09:40,200
 don't know, it's on the last pit,

195
00:09:40,200 --> 00:09:41,920
 that's why I don't see it."

196
00:09:41,920 --> 00:09:44,200
 He said, "If I let him be, he'll go to niriyang."

197
00:09:44,200 --> 00:09:50,400
 He'll go to hell, niriyang, but he might go to hell if I

198
00:09:50,400 --> 00:09:52,680
 let this happen.

199
00:09:52,680 --> 00:09:57,160
 So the Buddha went walking through the monastery and went

200
00:09:57,160 --> 00:09:59,200
 to see these two monks.

201
00:09:59,200 --> 00:10:00,980
 And he sat down on the seat that they had ready because

202
00:10:00,980 --> 00:10:02,040
 they always have a Buddha's

203
00:10:02,040 --> 00:10:04,960
 seat ready.

204
00:10:04,960 --> 00:10:08,800
 And the Buddha asked questions.

205
00:10:08,800 --> 00:10:11,920
 And this is where the disparity is.

206
00:10:11,920 --> 00:10:17,700
 Because what really happens is he asks first, he asks the

207
00:10:17,700 --> 00:10:21,040
 study monk about the first jhana,

208
00:10:21,040 --> 00:10:22,240
 and the first monk can't answer it.

209
00:10:22,240 --> 00:10:25,460
 He asks the other monk, the monk has been practicing about

210
00:10:25,460 --> 00:10:27,040
 the first jhana, that monk,

211
00:10:27,040 --> 00:10:28,040
 no problem answering it.

212
00:10:28,040 --> 00:10:34,660
 And he asks the second jhana, same thing, third jhana,

213
00:10:34,660 --> 00:10:37,680
 fourth jhana, all of the eight

214
00:10:37,680 --> 00:10:41,920
 more rupajanas and the four arupajanas he asks about all of

215
00:10:41,920 --> 00:10:42,640
 them.

216
00:10:42,640 --> 00:10:47,490
 And it says in the text that the study monk wasn't able to

217
00:10:47,490 --> 00:10:49,920
 answer a single one of them.

218
00:10:49,920 --> 00:10:52,710
 And on the other hand, the practice monk was able to answer

219
00:10:52,710 --> 00:10:54,560
 every single one of them, was

220
00:10:54,560 --> 00:10:56,240
 able to answer them.

221
00:10:56,240 --> 00:11:00,560
 Then he asked about the first path, the sotapanan path.

222
00:11:00,560 --> 00:11:03,470
 And the study monk wasn't able to answer, and his practice

223
00:11:03,470 --> 00:11:04,160
 monk was.

224
00:11:04,160 --> 00:11:06,140
 So he's asking questions that relate directly to the

225
00:11:06,140 --> 00:11:06,680
 practice.

226
00:11:06,680 --> 00:11:09,680
 So it must be something like, what is this experience like?

227
00:11:09,680 --> 00:11:10,680
 What is that experience?

228
00:11:10,680 --> 00:11:12,520
 Or what happens during this experience?

229
00:11:12,520 --> 00:11:15,120
 Can you describe it to me?

230
00:11:15,120 --> 00:11:17,280
 And so because it wasn't in the text, it wasn't something

231
00:11:17,280 --> 00:11:18,280
 he learned from heart.

232
00:11:18,280 --> 00:11:23,800
 He had no knowledge of it, no answer.

233
00:11:23,800 --> 00:11:28,080
 So the first, second, third, fourth, and each time the

234
00:11:28,080 --> 00:11:31,240
 Buddha, when the practice monk answered

235
00:11:31,240 --> 00:11:35,700
 correctly he would hold his hands up and say, "Sadhu," and

236
00:11:35,700 --> 00:11:38,160
 express his appreciation.

237
00:11:38,160 --> 00:11:42,030
 And then all of the students of the study monk, they

238
00:11:42,030 --> 00:11:44,640
 started muttering about this and

239
00:11:44,640 --> 00:11:47,430
 they said, "Why does the Buddha keep praising the younger

240
00:11:47,430 --> 00:11:47,920
 monk?"

241
00:11:47,920 --> 00:11:49,760
 And they said, "Isn't it crazy that the Buddha keeps

242
00:11:49,760 --> 00:11:51,600
 praising this monk when there's

243
00:11:51,600 --> 00:11:52,600
 nothing?"

244
00:11:52,600 --> 00:11:54,380
 And the Buddha turned to them and said, "What are you

245
00:11:54,380 --> 00:11:55,120
 talking about?"

246
00:11:55,120 --> 00:11:58,180
 And they explained what they're saying and the Buddha said,

247
00:11:58,180 --> 00:11:59,560
 "Monks, your teacher is

248
00:11:59,560 --> 00:12:02,760
 like a person who looks after the cows of another farmer.

249
00:12:02,760 --> 00:12:05,320
 He looks after them.

250
00:12:05,320 --> 00:12:06,520
 He doesn't get any of the milk.

251
00:12:06,520 --> 00:12:08,080
 He doesn't get the butter.

252
00:12:08,080 --> 00:12:10,000
 He doesn't get the cheese.

253
00:12:10,000 --> 00:12:12,360
 He doesn't get any of the fruits of the cow.

254
00:12:12,360 --> 00:12:18,800
 But my son, Mama Puta," Mama Puta is what they said, I

255
00:12:18,800 --> 00:12:21,960
 think, I believe, "is like the

256
00:12:21,960 --> 00:12:25,320
 farmer who owns the cow."

257
00:12:25,320 --> 00:12:28,120
 And then he gave these two verses.

258
00:12:28,120 --> 00:12:31,490
 The discrepancy is that the English translation makes it

259
00:12:31,490 --> 00:12:33,680
 out that the jhanas, that the study

260
00:12:33,680 --> 00:12:38,120
 monk was able to answer them, which isn't the case here.

261
00:12:38,120 --> 00:12:40,480
 Anyway, just a small point.

262
00:12:40,480 --> 00:12:43,230
 More important, this is a very important one from a

263
00:12:43,230 --> 00:12:45,340
 practical point of view, especially

264
00:12:45,340 --> 00:12:48,770
 because it details some points of practice and some

265
00:12:48,770 --> 00:12:51,000
 characteristics of a person who doesn't

266
00:12:51,000 --> 00:12:53,120
 practice.

267
00:12:53,120 --> 00:12:56,460
 So this is a reoccurring theme in the Buddhist teaching,

268
00:12:56,460 --> 00:12:58,520
 that the study of the texts doesn't

269
00:12:58,520 --> 00:13:01,940
 make one a follower of the Buddha.

270
00:13:01,940 --> 00:13:04,790
 Even though you can study all of this, and many monks will

271
00:13:04,790 --> 00:13:06,040
 practically memorize this

272
00:13:06,040 --> 00:13:08,000
 commentary.

273
00:13:08,000 --> 00:13:09,000
 This is how they learned Pali.

274
00:13:09,000 --> 00:13:10,840
 I did a little bit of it.

275
00:13:10,840 --> 00:13:14,000
 I learned maybe half of this.

276
00:13:14,000 --> 00:13:16,200
 I tried.

277
00:13:16,200 --> 00:13:18,600
 And it's a good thing to study.

278
00:13:18,600 --> 00:13:21,420
 But the person who, not takkaro hoti, a person who doesn't

279
00:13:21,420 --> 00:13:23,400
 practice accordingly, who doesn't

280
00:13:23,400 --> 00:13:26,800
 take the teachings to heart and isn't mindful, pamatto, if

281
00:13:26,800 --> 00:13:28,560
 you know the last words of the

282
00:13:28,560 --> 00:13:33,240
 Buddha, it was apamadin sampadet.

283
00:13:33,240 --> 00:13:34,240
 So don't be negligent.

284
00:13:34,240 --> 00:13:37,600
 You see, these were his last words.

285
00:13:37,600 --> 00:13:40,420
 So here, a person who is being negligent is very much the

286
00:13:40,420 --> 00:13:41,880
 core of what the Buddha told

287
00:13:41,880 --> 00:13:43,640
 us not to do.

288
00:13:43,640 --> 00:13:47,410
 So it means that for someone who is teaching, wasting their

289
00:13:47,410 --> 00:13:49,440
 time just with teaching, just

290
00:13:49,440 --> 00:13:52,720
 with spreading the Dhamma to others and not actually

291
00:13:52,720 --> 00:13:55,160
 practicing on their own, that it's

292
00:13:55,160 --> 00:13:57,400
 considered to be negligent.

293
00:13:57,400 --> 00:14:00,480
 And of course in their mind, they're heedless.

294
00:14:00,480 --> 00:14:02,280
 They're not aware when the emotions arise.

295
00:14:02,280 --> 00:14:06,600
 And you see this in many scholarly monks or scholars of any

296
00:14:06,600 --> 00:14:07,240
 type.

297
00:14:07,240 --> 00:14:14,170
 They can become quite conceited and attached to their

298
00:14:14,170 --> 00:14:17,240
 knowledge and aren't able to recognize

299
00:14:17,240 --> 00:14:19,390
 when, aren't able to recognize the defilements in

300
00:14:19,390 --> 00:14:21,120
 themselves and aren't able to eradicate

301
00:14:21,120 --> 00:14:23,000
 them.

302
00:14:23,000 --> 00:14:26,930
 On the other hand, a person who doesn't even study anything

303
00:14:26,930 --> 00:14:28,760
 or let's say doesn't study

304
00:14:28,760 --> 00:14:29,760
 anything, apa.

305
00:14:29,760 --> 00:14:33,990
 Apa means little because the story goes that he studied

306
00:14:33,990 --> 00:14:36,400
 enough to become an arahant.

307
00:14:36,400 --> 00:14:38,720
 This is like when someone has a map and one studies the

308
00:14:38,720 --> 00:14:39,760
 route one's going.

309
00:14:39,760 --> 00:14:41,800
 One doesn't have to study over here or over there.

310
00:14:41,800 --> 00:14:43,800
 One just has to study this way.

311
00:14:43,800 --> 00:14:46,110
 Okay, and then I reach here and there and then there's this

312
00:14:46,110 --> 00:14:47,080
 sign and turn right and

313
00:14:47,080 --> 00:14:48,080
 so on.

314
00:14:48,080 --> 00:14:49,080
 And that's it.

315
00:14:49,080 --> 00:14:52,420
 You don't have to worry about the rest of the map.

316
00:14:52,420 --> 00:14:53,420
 This is what it means by apa.

317
00:14:53,420 --> 00:14:54,420
 It means enough.

318
00:14:54,420 --> 00:14:57,960
 You learn just enough.

319
00:14:57,960 --> 00:15:02,840
 And so this is the difference between these two monks.

320
00:15:02,840 --> 00:15:05,600
 One of them learned the whole map, which is useful because

321
00:15:05,600 --> 00:15:06,920
 he had lots of students and

322
00:15:06,920 --> 00:15:08,920
 maybe some of his students became enlightened.

323
00:15:08,920 --> 00:15:11,280
 There was a story that I always tell him about this monk

324
00:15:11,280 --> 00:15:12,720
 who just did teaching and all of

325
00:15:12,720 --> 00:15:14,680
 his students became enlightened.

326
00:15:14,680 --> 00:15:18,410
 One of his students became an anagami and thought, "What is

327
00:15:18,410 --> 00:15:19,480
 our teacher?

328
00:15:19,480 --> 00:15:21,840
 What attainments has our teacher gained?"

329
00:15:21,840 --> 00:15:24,230
 And he thought about it and he realized our teacher is

330
00:15:24,230 --> 00:15:25,840
 still an ordinary human being.

331
00:15:25,840 --> 00:15:28,840
 So he went and he performed some magic and he levitated off

332
00:15:28,840 --> 00:15:30,160
 the floor in front of his

333
00:15:30,160 --> 00:15:31,160
 teacher.

334
00:15:31,160 --> 00:15:33,160
 He said, "Time for a lesson."

335
00:15:33,160 --> 00:15:34,480
 But he didn't mean it the way his teacher thought.

336
00:15:34,480 --> 00:15:36,040
 And he said, "I have no time for a lesson.

337
00:15:36,040 --> 00:15:37,040
 I'm going to be so busy."

338
00:15:37,040 --> 00:15:39,220
 And then he floated up in the air and he said, "You have no

339
00:15:39,220 --> 00:15:40,160
 time for yourself.

340
00:15:40,160 --> 00:15:42,160
 I don't want a lesson from you."

341
00:15:42,160 --> 00:15:47,720
 He flew out the window, apparently.

342
00:15:47,720 --> 00:15:51,560
 And this monk got very distressed and he actually left and

343
00:15:51,560 --> 00:15:53,960
 went off to practice meditation as

344
00:15:53,960 --> 00:15:54,960
 a result.

345
00:15:54,960 --> 00:15:57,990
 And if you've heard the story, if you haven't heard the

346
00:15:57,990 --> 00:15:59,960
 story, he goes off into the forest

347
00:15:59,960 --> 00:16:01,560
 and he's all alone.

348
00:16:01,560 --> 00:16:05,490
 But he's so famous that the angels think they can learn

349
00:16:05,490 --> 00:16:06,480
 from him.

350
00:16:06,480 --> 00:16:08,940
 So the angels come and they want to, "Oh, he's practicing

351
00:16:08,940 --> 00:16:09,680
 meditation.

352
00:16:09,680 --> 00:16:11,960
 Let's practice accordingly."

353
00:16:11,960 --> 00:16:14,320
 And so he walks and they walk and he sits and they sit.

354
00:16:14,320 --> 00:16:16,480
 And then he gets so frustrated that he's not gaining

355
00:16:16,480 --> 00:16:18,040
 anything that he sits down and starts

356
00:16:18,040 --> 00:16:19,360
 crying.

357
00:16:19,360 --> 00:16:22,310
 And suddenly this angel appears beside him and it's sitting

358
00:16:22,310 --> 00:16:23,200
 there crying.

359
00:16:23,200 --> 00:16:26,760
 And he said, "What are you crying for?"

360
00:16:26,760 --> 00:16:28,320
 And the angel said, "Well, I see you crying.

361
00:16:28,320 --> 00:16:35,760
 I thought that must be the way to practice."

362
00:16:35,760 --> 00:16:38,410
 He felt so ashamed that he actually started taking it

363
00:16:38,410 --> 00:16:40,000
 seriously and he woke up and he

364
00:16:40,000 --> 00:16:41,000
 said, "This is stupid."

365
00:16:41,000 --> 00:16:44,690
 And he just gave himself a slap in the face and sobered up,

366
00:16:44,690 --> 00:16:46,600
 I guess, and then he became

367
00:16:46,600 --> 00:16:50,160
 an orahant.

368
00:16:50,160 --> 00:16:51,600
 So the key is to not be negligent.

369
00:16:51,600 --> 00:16:54,720
 The person who is negligent means someone who is not being

370
00:16:54,720 --> 00:16:55,440
 mindful.

371
00:16:55,440 --> 00:16:58,630
 It's the Buddha said, "The person who is always mindful,

372
00:16:58,630 --> 00:17:00,840
 this is when you use to be vigilant

373
00:17:00,840 --> 00:17:06,600
 or to be heedful."

374
00:17:06,600 --> 00:17:09,560
 And so here in the next verse he talks about what are some

375
00:17:09,560 --> 00:17:11,120
 of the requisites for being

376
00:17:11,120 --> 00:17:12,120
 heedful.

377
00:17:12,120 --> 00:17:17,230
 "Dhammasahoti anudam machari," someone who practices the

378
00:17:17,230 --> 00:17:18,440
 teaching.

379
00:17:18,440 --> 00:17:20,400
 So you take the four foundations of mindfulness.

380
00:17:20,400 --> 00:17:23,290
 They don't sit around thinking about them or studying about

381
00:17:23,290 --> 00:17:24,520
 them or teaching them or

382
00:17:24,520 --> 00:17:28,880
 reciting them or however you take the time to go and find a

383
00:17:28,880 --> 00:17:30,960
 cave or find a tree or find

384
00:17:30,960 --> 00:17:36,560
 a hut and practice.

385
00:17:36,560 --> 00:17:37,560
 And then what do you do?

386
00:17:37,560 --> 00:17:41,430
 "Raga anchado sanchahah yamohang," this is key because some

387
00:17:41,430 --> 00:17:44,040
 people don't quite, it isn't

388
00:17:44,040 --> 00:17:46,830
 quite clear what we're trying to do and what are the

389
00:17:46,830 --> 00:17:48,720
 measurement of our practice.

390
00:17:48,720 --> 00:17:51,740
 The way we measure our practice is based on the amount of r

391
00:17:51,740 --> 00:17:53,440
aga, the amount of dosa, the

392
00:17:53,440 --> 00:17:57,240
 amount of moha, greed, anger and delusion.

393
00:17:57,240 --> 00:17:59,360
 The more of this that we're able to do away with, the

394
00:17:59,360 --> 00:18:02,720
 further we've gone in the practice.

395
00:18:02,720 --> 00:18:05,650
 The measure of our progress is how much we've been able to

396
00:18:05,650 --> 00:18:06,800
 do away with this.

397
00:18:06,800 --> 00:18:11,370
 So do we have less raga, less lust, less anger, less

398
00:18:11,370 --> 00:18:13,840
 delusion and the fourth.

399
00:18:13,840 --> 00:18:17,980
 And then the way to abandon it is in the next verse, "samma

400
00:18:17,980 --> 00:18:20,320
, pajano," through the practice

401
00:18:20,320 --> 00:18:26,320
 of samma, which means right and pajano, which means clear

402
00:18:26,320 --> 00:18:29,600
 seeing or seeing thoroughly.

403
00:18:29,600 --> 00:18:35,160
 They translate this often as clear comprehension, "sampaj

404
00:18:35,160 --> 00:18:37,920
ano," "sampajanya," but it really

405
00:18:37,920 --> 00:18:43,240
 means the perfect awareness of reality.

406
00:18:43,240 --> 00:18:46,250
 So in a sense when you watch the stomach, it's a simple

407
00:18:46,250 --> 00:18:48,200
 example, perfectly knowing what's

408
00:18:48,200 --> 00:18:51,010
 going on, this is the rising, this is the falling, when you

409
00:18:51,010 --> 00:18:52,720
're walking, knowing that your foot

410
00:18:52,720 --> 00:18:56,270
 is moving, when you're sitting, knowing that you're sitting

411
00:18:56,270 --> 00:18:57,040
 and so on.

412
00:18:57,040 --> 00:18:59,980
 When you're in this awareness, when you have this full and

413
00:18:59,980 --> 00:19:01,680
 complete and samma, which means

414
00:19:01,680 --> 00:19:07,760
 proper awareness, so it means it's just full knowing.

415
00:19:07,760 --> 00:19:11,430
 You know this is rising, you don't like it or dislike it or

416
00:19:11,430 --> 00:19:13,080
 cling to it and whatever

417
00:19:13,080 --> 00:19:16,440
 experience arises, so we hear the noise here, not getting

418
00:19:16,440 --> 00:19:18,080
 upset or attracted to it.

419
00:19:18,080 --> 00:19:22,720
 You see something not to be attached to it or repulsed by

420
00:19:22,720 --> 00:19:23,320
 it.

421
00:19:23,320 --> 00:19:28,800
 To just rightly seeing it as it is, this is samma, pajano.

422
00:19:28,800 --> 00:19:33,030
 Suvi mutti ji, suvi mutta ji to, with a mind that is well

423
00:19:33,030 --> 00:19:34,160
 released.

424
00:19:34,160 --> 00:19:37,440
 Well released means, there are different kinds of release,

425
00:19:37,440 --> 00:19:39,360
 the commentary talks about these,

426
00:19:39,360 --> 00:19:40,560
 but in the next phase.

427
00:19:40,560 --> 00:19:44,990
 But the most important one is, it's like the covering up of

428
00:19:44,990 --> 00:19:46,000
 the roof.

429
00:19:46,000 --> 00:19:48,730
 You can cover it up with morality, you can cover it up with

430
00:19:48,730 --> 00:19:49,760
 concentration.

431
00:19:49,760 --> 00:19:54,430
 The most important is to cover up the roof with wisdom, to

432
00:19:54,430 --> 00:19:56,600
 create a seal with wisdom

433
00:19:56,600 --> 00:20:00,400
 so that your mind has no, there's no room for it to arise

434
00:20:00,400 --> 00:20:02,600
 because the mind is perfectly

435
00:20:02,600 --> 00:20:07,280
 airtight, you have perfect, complete knowledge, you know

436
00:20:07,280 --> 00:20:08,320
 the truth.

437
00:20:08,320 --> 00:20:11,410
 I mean basically it means to see nibhana because once you

438
00:20:11,410 --> 00:20:13,400
 see nibhana your mind is perfectly

439
00:20:13,400 --> 00:20:17,510
 protected. The first time when a person becomes a sotaphana

440
00:20:17,510 --> 00:20:18,840
, the mind is perfectly protected

441
00:20:18,840 --> 00:20:23,530
 from, or is perfectly freed from, what is it, sakayaditi,

442
00:20:23,530 --> 00:20:33,160
 view of self, vichikiccha, silabhata

443
00:20:33,160 --> 00:20:39,180
 paramasa which means attachment to wrong practices and

444
00:20:39,180 --> 00:20:41,320
 wrong morality.

445
00:20:41,320 --> 00:20:44,670
 So abstaining from things that it's not necessary to abst

446
00:20:44,670 --> 00:20:46,960
ain from, taking on practices that

447
00:20:46,960 --> 00:20:50,170
 are not necessary to practice, thinking that by abstaining

448
00:20:50,170 --> 00:20:51,840
 and by practicing in this way,

449
00:20:51,840 --> 00:20:53,890
 or thinking that there's abstentions and these practices

450
00:20:53,890 --> 00:20:54,600
 are necessary.

451
00:20:54,600 --> 00:20:57,010
 You never do that because you know what is the right

452
00:20:57,010 --> 00:20:58,720
 practice and you know what leads

453
00:20:58,720 --> 00:21:02,640
 to the result because you've been there.

454
00:21:02,640 --> 00:21:05,680
 And vichikiccha which means no doubt, because you've seen

455
00:21:05,680 --> 00:21:07,480
 nibhana, you have no doubt about

456
00:21:07,480 --> 00:21:11,560
 whether nibhana exists or not, because you've come to see

457
00:21:11,560 --> 00:21:13,560
 the truth from yourself.

458
00:21:13,560 --> 00:21:16,910
 And when you go to sakitakami, anagami and arahantah, when

459
00:21:16,910 --> 00:21:18,680
 you become an arahantah the

460
00:21:18,680 --> 00:21:23,510
 mind is sui mutta jita, the mind is sui mutta which means

461
00:21:23,510 --> 00:21:25,400
 well-free. There's no greed, no

462
00:21:25,400 --> 00:21:30,840
 anger, no delusion, no ignorance, full understanding of the

463
00:21:30,840 --> 00:21:32,960
 Four Noble Truths. Everything that

464
00:21:32,960 --> 00:21:39,510
 you experience is seen just for what it is, samma-pajana,

465
00:21:39,510 --> 00:21:41,440
 sampa-janya.

466
00:21:41,440 --> 00:21:47,550
 So that's the final story of the Yamaka-vaga, a group of

467
00:21:47,550 --> 00:21:52,440
 pairs, the first vaga. And it's

468
00:21:52,440 --> 00:21:56,190
 a reminder for us that what we are doing is the proper

469
00:21:56,190 --> 00:21:58,560
 practice of the Buddha. We shouldn't

470
00:21:58,560 --> 00:22:02,510
 be proud because many people, even people who are born

471
00:22:02,510 --> 00:22:05,000
 Buddhist or have found Buddhism,

472
00:22:05,000 --> 00:22:08,260
 Westerners who find Buddhism will often just spend their

473
00:22:08,260 --> 00:22:10,160
 lives studying and talking and

474
00:22:10,160 --> 00:22:15,390
 discussing and trying to appreciate intellectual. And they

475
00:22:15,390 --> 00:22:16,920
 consider intellectual appreciation

476
00:22:16,920 --> 00:22:21,590
 to be the realization. And they miss the fruit of the holy

477
00:22:21,590 --> 00:22:23,800
 life because they don't actually

478
00:22:23,800 --> 00:22:28,070
 practice it. So you should feel proud, not proud and conce

479
00:22:28,070 --> 00:22:30,760
ited, but you should feel confident

480
00:22:30,760 --> 00:22:33,290
 and feel good about yourself. And we should remind

481
00:22:33,290 --> 00:22:35,040
 ourselves that what we're doing is

482
00:22:35,040 --> 00:22:39,420
 a good thing. Remind ourselves means look at the benefits

483
00:22:39,420 --> 00:22:41,080
 that we've gained and not

484
00:22:41,080 --> 00:22:44,820
 disparage the wisdom that we're gaining in the practice.

485
00:22:44,820 --> 00:22:45,640
 Because every moment you're

486
00:22:45,640 --> 00:22:50,200
 gaining something, learning something more about yourself.

487
00:22:50,200 --> 00:22:51,080
 At the same time it's easy

488
00:22:51,080 --> 00:22:54,390
 to become discouraged because it's very difficult and you

489
00:22:54,390 --> 00:22:56,600
're experiencing things, you're dealing

490
00:22:56,600 --> 00:23:00,560
 with things that you're so used to covering up and avoiding

491
00:23:00,560 --> 00:23:01,760
. So when they come up again

492
00:23:01,760 --> 00:23:06,470
 there's the instinct is to repress them, to push them away,

493
00:23:06,470 --> 00:23:08,440
 to run away from you. So it

494
00:23:08,440 --> 00:23:12,270
 can be quite disheartening and some people actually decide

495
00:23:12,270 --> 00:23:14,120
 they want to run away, they

496
00:23:14,120 --> 00:23:16,990
 don't want to deal with it anymore. It's important to

497
00:23:16,990 --> 00:23:19,400
 remind ourselves that this is really what

498
00:23:19,400 --> 00:23:22,900
 we've gained already or what we're doing here is something

499
00:23:22,900 --> 00:23:25,080
 quite important and quite incredible.

500
00:23:25,080 --> 00:23:30,960
 If you remember the goodness of what we're doing, be able

501
00:23:30,960 --> 00:23:33,760
 to don't lose sight of what

502
00:23:33,760 --> 00:23:36,630
 we've gained or what we're gaining because you're learning

503
00:23:36,630 --> 00:23:38,040
 all the time about yourself,

504
00:23:38,040 --> 00:23:44,490
 about how the mind works, about how reality works. And that

505
00:23:44,490 --> 00:23:47,360
's a very precious gift. So

506
00:23:47,360 --> 00:23:52,730
 that's the story for today. I'm sure that I'm going to be

507
00:23:52,730 --> 00:23:55,720
 part of this session and tomorrow

508
00:23:55,720 --> 00:23:59,950
 or whenever we'll get on to the second chapter. So thanks

509
00:23:59,950 --> 00:24:02,880
 for tuning in and back to meditation.

510
00:24:02,880 --> 00:24:18,960
 [

