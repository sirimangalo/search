1
00:00:00,000 --> 00:00:06,150
 Okay, good evening everyone. Welcome back to our live

2
00:00:06,150 --> 00:00:10,000
 broadcast.

3
00:00:10,000 --> 00:00:20,540
 It's sort of the time for all of us to act together as a

4
00:00:20,540 --> 00:00:25,000
 community, to come together as a community.

5
00:00:25,000 --> 00:00:35,000
 Take the time to reflect on our practice.

6
00:00:35,000 --> 00:00:39,800
 As my teachers used to say, or as you often hear monks in

7
00:00:39,800 --> 00:00:44,000
 Thailand say, "Kitban Chi."

8
00:00:44,000 --> 00:00:52,870
 "Kitban Chi," which is colloquialism for looking at our

9
00:00:52,870 --> 00:01:01,090
 bank account or calculating our bank, calculating our

10
00:01:01,090 --> 00:01:02,000
 profit.

11
00:01:02,000 --> 00:01:18,370
 Looking at what we've gotten out of the practice, what's

12
00:01:18,370 --> 00:01:26,000
 come of our practice, what we've gained from the practice.

13
00:01:26,000 --> 00:01:39,190
 We come together to get direction on our practice, to

14
00:01:39,190 --> 00:01:52,000
 direct us in the right direction, to adjust our focus.

15
00:01:52,000 --> 00:02:01,360
 But we also come together to talk about what we've gained

16
00:02:01,360 --> 00:02:05,000
 from the practice.

17
00:02:05,000 --> 00:02:08,060
 It's a subject of constant concern, right? What am I

18
00:02:08,060 --> 00:02:11,000
 getting out of this?

19
00:02:11,000 --> 00:02:20,670
 You have to look at the bottom line. You're putting a lot

20
00:02:20,670 --> 00:02:31,000
 of effort. What good is it? What good does it do?

21
00:02:31,000 --> 00:02:36,240
 The Buddhism is all about cause and effect, results,

22
00:02:36,240 --> 00:02:42,590
 actions and their results. Kamma is actions, vipaka means

23
00:02:42,590 --> 00:02:44,000
 results.

24
00:02:44,000 --> 00:02:48,050
 So we have results of many different things. First of all,

25
00:02:48,050 --> 00:02:54,650
 there's the results of our ordinary efforts to try and

26
00:02:54,650 --> 00:02:58,000
 solve our problems.

27
00:02:58,000 --> 00:03:06,950
 When something comes to mind that is undesirable, we work

28
00:03:06,950 --> 00:03:20,000
 very hard to avoid it, to quench it or quelch it,

29
00:03:20,000 --> 00:03:28,680
 to be free from it. When something good comes to mind, the

30
00:03:28,680 --> 00:03:35,260
 actions are to obtain it, to sustain it, to maintain it, to

31
00:03:35,260 --> 00:03:37,000
 get what we want,

32
00:03:37,000 --> 00:03:47,550
 to always get what we want. And we have strategies for this

33
00:03:47,550 --> 00:03:51,140
. We have, as I was talking about yesterday, we have ways of

34
00:03:51,140 --> 00:03:52,000
 thinking positive,

35
00:03:52,000 --> 00:03:58,450
 or ways of setting up our lives in such a way that we are

36
00:03:58,450 --> 00:04:04,790
 protected from the things that we don't like, and that we

37
00:04:04,790 --> 00:04:08,000
're able to protect the things that we do like,

38
00:04:08,000 --> 00:04:25,360
 so that we can obtain and experience them frequently,

39
00:04:25,360 --> 00:04:36,000
 repeatedly, at our pleasure.

40
00:04:36,000 --> 00:04:38,550
 And this is the worst thing to do, of course. This is what

41
00:04:38,550 --> 00:04:42,760
 we're starting to realize through the practice. This is

42
00:04:42,760 --> 00:04:49,000
 where disaster comes.

43
00:04:49,000 --> 00:04:54,750
 If you spend all your time cultivating this attachment and

44
00:04:54,750 --> 00:05:00,430
 aversion, these habits of avoiding the unpleasant and

45
00:05:00,430 --> 00:05:06,000
 obtaining the pleasant, highly unstable.

46
00:05:06,000 --> 00:05:14,160
 It's the sort of thing that can come crashing down on you,

47
00:05:14,160 --> 00:05:20,860
 and when your defenses are down, it can lead to great

48
00:05:20,860 --> 00:05:23,000
 stress and suffering.

49
00:05:23,000 --> 00:05:30,530
 When you don't get what you want, when you get what you don

50
00:05:30,530 --> 00:05:32,000
't want.

51
00:05:32,000 --> 00:05:35,620
 And we're learning about these habits when we practice. As

52
00:05:35,620 --> 00:05:39,580
 you begin to meditate, you start to see these habits, these

53
00:05:39,580 --> 00:05:42,000
 strategies, these practices,

54
00:05:42,000 --> 00:05:45,760
 and you realize it's not really the best way. The results

55
00:05:45,760 --> 00:05:47,000
 are not so good.

56
00:05:47,000 --> 00:06:07,000
 [silence]

57
00:06:07,000 --> 00:06:11,710
 Another strategy, of course, is to cultivate tranquility

58
00:06:11,710 --> 00:06:21,800
 meditation. So we cultivate states and habits that replace

59
00:06:21,800 --> 00:06:27,000
 our reactions.

60
00:06:27,000 --> 00:06:30,000
 Sort of avoid the problem entirely, right?

61
00:06:30,000 --> 00:06:34,940
 So if you're angry at someone, immerse yourself in

62
00:06:34,940 --> 00:06:40,000
 universal love, and the problem never comes up.

63
00:06:40,000 --> 00:06:44,130
 Spend time sitting, cultivating love for all beings, love

64
00:06:44,130 --> 00:06:50,620
 for those that you hate. It's a great method. It works

65
00:06:50,620 --> 00:06:53,000
 wonders.

66
00:06:53,000 --> 00:07:00,030
 If you have lust, passion for bodily carnal pleasure,

67
00:07:00,030 --> 00:07:03,000
 cultivate mindfulness of the body.

68
00:07:03,000 --> 00:07:07,390
 Look at the different body parts and see them as unpleasant

69
00:07:07,390 --> 00:07:12,260
, as disgusting. It's not worth clinging to, and it supp

70
00:07:12,260 --> 00:07:14,000
resses the desire.

71
00:07:14,000 --> 00:07:20,870
 It replaces it with a sobering awareness of the true nature

72
00:07:20,870 --> 00:07:28,160
 of the body as not being in any way desirable or worthy of

73
00:07:28,160 --> 00:07:31,000
 clinging to.

74
00:07:31,000 --> 00:07:36,790
 And so on. Of course, the more universal meditations like

75
00:07:36,790 --> 00:07:42,030
 fixing your mind on a color, for example, or any sort of

76
00:07:42,030 --> 00:07:43,000
 concept.

77
00:07:43,000 --> 00:07:51,000
 Meditating on the Buddha, for example.

78
00:07:51,000 --> 00:07:58,560
 And these have much better results. The results here are

79
00:07:58,560 --> 00:08:11,120
 lasting, stable, and unadulterated. They're not tainted by

80
00:08:11,120 --> 00:08:14,000
 defilement.

81
00:08:14,000 --> 00:08:17,910
 So when you stop practicing, you're not any worse off than

82
00:08:17,910 --> 00:08:22,000
 you were before. You're in fact better because you now have

83
00:08:22,000 --> 00:08:26,000
 this skill, this talent of entering into these states.

84
00:08:26,000 --> 00:08:30,210
 And so it's a great thing to do. People often wonder about

85
00:08:30,210 --> 00:08:33,470
 this sort of meditation, whether it's actually any good or

86
00:08:33,470 --> 00:08:34,000
 any use.

87
00:08:34,000 --> 00:08:41,520
 Well, this is the use. It provides you with a way out,

88
00:08:41,520 --> 00:08:49,000
 provides you with a sort of a freedom from suffering.

89
00:08:49,000 --> 00:08:55,000
 And yet, it too is not permanent.

90
00:08:55,000 --> 00:08:59,480
 And so as great of a temporary solution as it might be, it

91
00:08:59,480 --> 00:09:04,260
's not by any means a permanent solution. It can't actually

92
00:09:04,260 --> 00:09:06,000
 free you from suffering.

93
00:09:06,000 --> 00:09:14,600
 Because when and if you ever stop or lose the interest or

94
00:09:14,600 --> 00:09:23,480
 the initiative or the impulsion to continue, you just wind

95
00:09:23,480 --> 00:09:26,000
 up where you started.

96
00:09:26,000 --> 00:09:33,000
 You begin to cultivate bad habits again, or your bad habits

97
00:09:33,000 --> 00:09:35,000
 crop up again.

98
00:09:35,000 --> 00:09:40,240
 So even that's not the solution. Good results, but not the

99
00:09:40,240 --> 00:09:44,000
 results we're looking for.

100
00:09:44,000 --> 00:09:51,110
 So we go further. This is really why we bother to torture

101
00:09:51,110 --> 00:09:57,000
 ourselves with insight meditation.

102
00:09:57,000 --> 00:10:03,280
 Because we see there's got to be a more permanent solution,

103
00:10:03,280 --> 00:10:08,050
 a more stable solution, something that can allow us to

104
00:10:08,050 --> 00:10:11,000
 truly change and truly become free.

105
00:10:11,000 --> 00:10:14,810
 And so cultivate insight meditation. Now, insight

106
00:10:14,810 --> 00:10:23,000
 meditation has this great power of universal applicability.

107
00:10:23,000 --> 00:10:28,760
 It's not just when you're angry you cultivate love or when

108
00:10:28,760 --> 00:10:34,760
 you're passionate or lustful you cultivate mindfulness of

109
00:10:34,760 --> 00:10:36,000
 the body.

110
00:10:36,000 --> 00:10:40,490
 Insight meditation cultivates wisdom. And wisdom gives you

111
00:10:40,490 --> 00:10:48,350
 an understanding of all aspects of reality that frees you

112
00:10:48,350 --> 00:10:51,000
 from clinging.

113
00:10:51,000 --> 00:10:59,060
 It gives you an awareness, a presence. It's like a solvent

114
00:10:59,060 --> 00:11:10,000
 that dissolves all of the stickiness in the mind.

115
00:11:10,000 --> 00:11:17,320
 It's far more lasting and stable and requires so much less

116
00:11:17,320 --> 00:11:25,020
 effort to upkeep because it's innate, it's pure, it's not

117
00:11:25,020 --> 00:11:29,000
 based on effort per se, it's based on wisdom.

118
00:11:29,000 --> 00:11:33,450
 Meaning it's not something you have to force. It's not

119
00:11:33,450 --> 00:11:36,000
 artificial, it's natural.

120
00:11:36,000 --> 00:11:40,400
 That being said, something that is not acknowledged nearly

121
00:11:40,400 --> 00:11:44,000
 enough is that even insight is not permanent.

122
00:11:44,000 --> 00:11:52,000
 Even insight, having been gained, can be lost.

123
00:11:52,000 --> 00:11:58,240
 A person on the path of insight can gain profound insights

124
00:11:58,240 --> 00:12:04,480
 into reality and think maybe they've really succeeded in

125
00:12:04,480 --> 00:12:06,000
 the practice,

126
00:12:06,000 --> 00:12:15,530
 only to fall away when they give up the practice and lose

127
00:12:15,530 --> 00:12:21,000
 everything they've gained.

128
00:12:21,000 --> 00:12:24,360
 It makes one kind of thing, "Well, why go through all the

129
00:12:24,360 --> 00:12:26,590
 effort? Maybe I'll go back to Samatha meditation if that's

130
00:12:26,590 --> 00:12:27,000
 the case."

131
00:12:27,000 --> 00:12:30,350
 I thought this was going to be a one-time thing where I don

132
00:12:30,350 --> 00:12:33,530
't have to go through all the torture of actually dealing

133
00:12:33,530 --> 00:12:36,000
 with my problems and figuring them out.

134
00:12:36,000 --> 00:12:41,670
 I'm just going to get them all back again. But to be honest

135
00:12:41,670 --> 00:12:46,000
, it probably lasts a lot longer and it's a lot more stable.

136
00:12:46,000 --> 00:12:50,850
 It's the most stable of all of them all so far. That's not

137
00:12:50,850 --> 00:12:52,000
 permanent.

138
00:12:52,000 --> 00:12:56,260
 This life, maybe you keep it if you get really strong

139
00:12:56,260 --> 00:12:59,000
 insight, there are certain things that you won't lose

140
00:12:59,000 --> 00:13:01,000
 throughout your life.

141
00:13:01,000 --> 00:13:04,550
 You can lose them in the next life or the next life or some

142
00:13:04,550 --> 00:13:12,000
 life in the future. They're not permanent.

143
00:13:12,000 --> 00:13:16,000
 But not to despair, there is light at the end of the tunnel

144
00:13:16,000 --> 00:13:16,000
.

145
00:13:16,000 --> 00:13:19,200
 In fact, insight meditation leads to something else. It

146
00:13:19,200 --> 00:13:23,000
 leads to freedom. It leads to nirvana.

147
00:13:23,000 --> 00:13:30,510
 It leads to the experience of what we call the deathless or

148
00:13:30,510 --> 00:13:36,000
 the un-risen, perhaps.

149
00:13:36,000 --> 00:13:43,000
 It leads to an experience that is outside of samsara.

150
00:13:43,000 --> 00:13:46,560
 And so we have to distinguish between these two. Being on

151
00:13:46,560 --> 00:13:49,000
 the path of insight is not enough.

152
00:13:49,000 --> 00:13:53,810
 It's not something you accumulate and never lose, something

153
00:13:53,810 --> 00:13:55,000
 you accumulate.

154
00:13:55,000 --> 00:14:02,860
 And if you accumulate enough of it, you can break through

155
00:14:02,860 --> 00:14:10,000
 to the point where you reach another level of action,

156
00:14:10,000 --> 00:14:20,000
 with another level of results.

157
00:14:20,000 --> 00:14:23,900
 So the results of insight are stable and lasting but not

158
00:14:23,900 --> 00:14:25,000
 permanent.

159
00:14:25,000 --> 00:14:30,180
 But the results of nirvana, the results of entering into

160
00:14:30,180 --> 00:14:33,000
 the manganyana, paldanyana,

161
00:14:33,000 --> 00:14:38,130
 of experiencing nirvana, even just a moment, even for just

162
00:14:38,130 --> 00:14:40,000
 three moments of time,

163
00:14:40,000 --> 00:14:44,270
 that's apparently how the shortest amount of time you can

164
00:14:44,270 --> 00:14:49,000
 experience nirvana or something, or maybe two moments.

165
00:14:49,000 --> 00:14:53,000
 But for the first time, I think it's three.

166
00:14:53,000 --> 00:15:01,000
 Of course, it can be longer, it can be hours or days.

167
00:15:01,000 --> 00:15:06,140
 But even for just that moment, this is, and again, it

168
00:15:06,140 --> 00:15:08,000
 requires perhaps little faith,

169
00:15:08,000 --> 00:15:16,320
 but something you can verify for yourself, that this state

170
00:15:16,320 --> 00:15:18,000
 is the point of no return.

171
00:15:18,000 --> 00:15:21,000
 You can't go back.

172
00:15:21,000 --> 00:15:25,740
 A person who has seen nirvana, even for just a moment, will

173
00:15:25,740 --> 00:15:30,000
 be changed permanently.

174
00:15:30,000 --> 00:15:36,350
 Permanently, they've set in motion the cessation of

175
00:15:36,350 --> 00:15:41,000
 suffering, the escape from samsara.

176
00:15:41,000 --> 00:15:48,200
 They're permanently altered and removed from their very

177
00:15:48,200 --> 00:15:51,840
 being, sort of the linchpin to this whole mass of suffering

178
00:15:51,840 --> 00:15:55,000
, ignorance.

179
00:15:55,000 --> 00:16:00,000
 They've come to see outside samsara.

180
00:16:00,000 --> 00:16:08,180
 And this happiness, this peace, this great freedom is

181
00:16:08,180 --> 00:16:14,000
 lasting, there's no return from it.

182
00:16:14,000 --> 00:16:20,000
 So to be clear, our daily practice of meditation is great,

183
00:16:20,000 --> 00:16:22,000
 it's lasting, it's quite beneficial,

184
00:16:22,000 --> 00:16:25,570
 and it is what eventually leads to true freedom, but it's

185
00:16:25,570 --> 00:16:27,000
 not true freedom.

186
00:16:27,000 --> 00:16:33,510
 To truly be free and to have a perfect release, you need to

187
00:16:33,510 --> 00:16:35,000
 truly understand samsara.

188
00:16:35,000 --> 00:16:39,410
 You need to get to the point where you really get it, where

189
00:16:39,410 --> 00:16:41,000
 you have this epiphany

190
00:16:41,000 --> 00:16:45,520
 that tells you that nothing is worth clinging to, that

191
00:16:45,520 --> 00:16:52,000
 ceases where the mind drops out, unplugs,

192
00:16:52,000 --> 00:17:06,000
 ceases to reach out, ceases to advert to samsara.

193
00:17:06,000 --> 00:17:10,540
 There's an experience that is beyond samsara, that is

194
00:17:10,540 --> 00:17:12,000
 outside of it.

195
00:17:12,000 --> 00:17:14,800
 From that moment on, one is called asotapan, which means

196
00:17:14,800 --> 00:17:17,000
 they have attained the stream,

197
00:17:17,000 --> 00:17:19,480
 they have entered into the stream, meaning they're on the

198
00:17:19,480 --> 00:17:26,000
 current, leading them to complete freedom from suffering.

199
00:17:26,000 --> 00:17:35,800
 So, this doesn't mean to say that we should not all be

200
00:17:35,800 --> 00:17:37,000
 praised and appreciated

201
00:17:37,000 --> 00:17:40,860
 for all the good merit that we're gaining in our practices

202
00:17:40,860 --> 00:17:42,000
 of all sorts.

203
00:17:42,000 --> 00:17:46,480
 We practice the Buddhist teaching on all levels, but it's a

204
00:17:46,480 --> 00:17:49,000
 reminder for us not to be complacent,

205
00:17:49,000 --> 00:17:53,240
 that there is a goal, that this isn't just a hobby or

206
00:17:53,240 --> 00:17:58,000
 something that you take up to do on a daily basis,

207
00:17:58,000 --> 00:18:03,000
 it's something that you use to achieve true understanding

208
00:18:03,000 --> 00:18:07,730
 and realization of the ultimate state and the ultimate

209
00:18:07,730 --> 00:18:09,000
 freedom.

210
00:18:09,000 --> 00:18:14,000
 It's what we're aiming for. It has the greatest result,

211
00:18:14,000 --> 00:18:23,000
 greatest effect, and has a true benefit, a true purpose,

212
00:18:23,000 --> 00:18:27,000
 true meaning.

213
00:18:27,000 --> 00:18:33,440
 There you know, there's our Dhamma for today. The results,

214
00:18:33,440 --> 00:18:35,000
 getting results.

215
00:18:35,000 --> 00:18:39,570
 Thank you all for tuning in, for coming out. Have a good

216
00:18:39,570 --> 00:18:40,000
 day.

