1
00:00:00,000 --> 00:00:03,520
 I find it very hard to concentrate on things unless I am

2
00:00:03,520 --> 00:00:05,500
 highly interested in something.

3
00:00:05,500 --> 00:00:08,080
 It leads me to procrastinate a lot.

4
00:00:08,080 --> 00:00:13,000
 Is that just lack of discipline?

5
00:00:13,000 --> 00:00:14,260
 It can be.

6
00:00:14,260 --> 00:00:21,000
 It has to do a lot with our desires and aversions.

7
00:00:21,000 --> 00:00:24,000
 If you're a person who has a lot of desires and aversions,

8
00:00:24,000 --> 00:00:25,360
 then it's quite likely that

9
00:00:25,360 --> 00:00:29,000
 you'll procrastinate in a great number of things.

10
00:00:29,000 --> 00:00:33,870
 If you have less desire and less aversion, then you'll be

11
00:00:33,870 --> 00:00:36,120
 able to focus on many more

12
00:00:36,120 --> 00:00:41,680
 things because your ability to develop interest will be

13
00:00:41,680 --> 00:00:45,220
 heightened because you'll be able

14
00:00:45,220 --> 00:00:50,120
 to just be interested in whatever is in the present moment.

15
00:00:50,120 --> 00:00:53,220
 You won't have a desire to go and do things, but you'll be

16
00:00:53,220 --> 00:00:55,200
 interested in whatever the task

17
00:00:55,200 --> 00:00:58,440
 is in front of you.

18
00:00:58,440 --> 00:01:04,440
 The other aspect is that your lack of interest is actually

19
00:01:04,440 --> 00:01:09,800
 a natural outcome of the meditation.

20
00:01:09,800 --> 00:01:14,610
 It can also have to do with the uselessness of the things

21
00:01:14,610 --> 00:01:16,560
 that you're doing.

22
00:01:16,560 --> 00:01:20,840
 I found it quite difficult to focus on school.

23
00:01:20,840 --> 00:01:23,270
 After I practiced meditation, I went back and did two

24
00:01:23,270 --> 00:01:24,120
 different years.

25
00:01:24,120 --> 00:01:26,770
 The first year, it was quite easy because I was really keen

26
00:01:26,770 --> 00:01:27,920
 on it because there were

27
00:01:27,920 --> 00:01:31,200
 some Buddhist courses I could take and Sanskrit and so on.

28
00:01:31,200 --> 00:01:33,200
 Actually, I was quite into it.

29
00:01:33,200 --> 00:01:36,320
 The second year, going back totally the opposite.

30
00:01:36,320 --> 00:01:40,850
 There were no more Buddhist courses to take and I just

31
00:01:40,850 --> 00:01:43,560
 totally found it impossible to

32
00:01:43,560 --> 00:01:44,560
 focus on it.

33
00:01:44,560 --> 00:01:46,600
 There was no interest there.

34
00:01:46,600 --> 00:01:49,800
 I don't think that's necessarily a bad thing.

35
00:01:49,800 --> 00:01:55,380
 It just means that if the things that you're supposed to

36
00:01:55,380 --> 00:01:57,880
 concentrate on are useless, then

37
00:01:57,880 --> 00:02:06,920
 it just becomes a fact of the matter that it's very

38
00:02:06,920 --> 00:02:10,600
 difficult to become interested.

39
00:02:10,600 --> 00:02:13,760
 It's unwholesome and unhealthy actually to become

40
00:02:13,760 --> 00:02:16,000
 interested or to force yourself to

41
00:02:16,000 --> 00:02:24,200
 do things that are of no benefit.

42
00:02:24,200 --> 00:02:27,480
 For most of us, it's just defilements.

43
00:02:27,480 --> 00:02:31,650
 But once we start to meditate, there's that aspect as well

44
00:02:31,650 --> 00:02:33,520
 that it can have to do with

45
00:02:33,520 --> 00:02:34,520
 our meditation practice.

46
00:02:34,520 --> 00:02:37,990
 You'll find that as a result of meditating, you don't want

47
00:02:37,990 --> 00:02:39,640
 to get involved with a great

48
00:02:39,640 --> 00:02:44,050
 number of things that you before found easy to get involved

49
00:02:44,050 --> 00:02:44,560
 in.

