1
00:00:00,000 --> 00:00:06,000
 Hello, good evening everyone.

2
00:00:06,000 --> 00:00:13,000
 I'm broadcasting live November 1st, 2015.

3
00:00:13,000 --> 00:00:27,190
 And our non-Dhammapada nights have now taken on a bit of a

4
00:00:27,190 --> 00:00:30,000
...

5
00:00:30,000 --> 00:00:33,390
 I think the word is a pallor. They're less colorful, less

6
00:00:33,390 --> 00:00:34,000
 lively.

7
00:00:34,000 --> 00:00:37,440
 Not sure what we're going to do now that I've decided not

8
00:00:37,440 --> 00:00:39,000
 to do Dhammapada every night.

9
00:00:39,000 --> 00:00:43,000
 Maybe we won't do anything. Maybe I'll say hello.

10
00:00:43,000 --> 00:00:47,000
 Thank you all for practicing.

11
00:00:47,000 --> 00:00:51,350
 I guess I can take the time now to talk about our online

12
00:00:51,350 --> 00:00:52,000
 courses.

13
00:00:52,000 --> 00:00:57,000
 It's not like there's any good in advertising them.

14
00:00:57,000 --> 00:01:02,000
 Unless I start to get a little bit more vigilant.

15
00:01:02,000 --> 00:01:10,000
 Actually, it's not really about that. There's 28 slots and

16
00:01:10,000 --> 00:01:15,000
 24 of them have been claimed so far right now.

17
00:01:15,000 --> 00:01:21,090
 And that goes up and down, but 24 is about as high as we've

18
00:01:21,090 --> 00:01:22,000
 gotten.

19
00:01:22,000 --> 00:01:28,150
 Pretty much everyone comes out as expected and is

20
00:01:28,150 --> 00:01:35,000
 practicing diligently at least an hour or two a day.

21
00:01:35,000 --> 00:01:39,380
 So that's really been... it's become sort of the highlight

22
00:01:39,380 --> 00:01:40,000
 for me.

23
00:01:40,000 --> 00:01:46,000
 It's a real focus because the biggest problem that I've had

24
00:01:46,000 --> 00:01:50,000
 in sort of sharing this meditation technique

25
00:01:50,000 --> 00:01:54,910
 is that I've always felt unable to share more than the

26
00:01:54,910 --> 00:01:56,000
 first step.

27
00:01:56,000 --> 00:01:59,000
 So everyone's been thinking, "Well, this is it."

28
00:01:59,000 --> 00:02:02,380
 This is the meditation that I'm supposed to do for the rest

29
00:02:02,380 --> 00:02:03,000
 of my life.

30
00:02:03,000 --> 00:02:06,140
 If I follow this tradition, it's just stepping right,

31
00:02:06,140 --> 00:02:08,000
 stepping left, and rising, falling.

32
00:02:08,000 --> 00:02:11,320
 It's a very simple technique, but as many of the people who

33
00:02:11,320 --> 00:02:14,000
 have started taking the course have found,

34
00:02:14,000 --> 00:02:17,000
 it gets quite complex actually, quite involved.

35
00:02:17,000 --> 00:02:20,400
 And there's quite a bit to it, not just the adding of extra

36
00:02:20,400 --> 00:02:22,000
 elements to the practice,

37
00:02:22,000 --> 00:02:28,430
 but there's also the interview process and there's even

38
00:02:28,430 --> 00:02:29,000
 more.

39
00:02:29,000 --> 00:02:32,560
 There's the next step, which will be having people come to

40
00:02:32,560 --> 00:02:34,000
 do intensive courses,

41
00:02:34,000 --> 00:02:38,710
 which of course is the best, but also the most difficult to

42
00:02:38,710 --> 00:02:40,000
 arrange for us to arrange

43
00:02:40,000 --> 00:02:43,710
 and for the individuals to arrange to actually get out here

44
00:02:43,710 --> 00:02:44,000
.

45
00:02:44,000 --> 00:02:48,620
 But at the very least what we're doing is, well, besides

46
00:02:48,620 --> 00:02:51,000
 giving an introduction to the practice,

47
00:02:51,000 --> 00:02:55,270
 we're also reducing the amount of time that you would have

48
00:02:55,270 --> 00:02:58,000
 to spend practicing intensively to finish the course.

49
00:02:58,000 --> 00:03:05,050
 So my idea was that if you do this for a while, we could

50
00:03:05,050 --> 00:03:10,000
 finish the foundation course intensively in about a week.

51
00:03:10,000 --> 00:03:17,670
 And to get through, to get that far would probably take 12

52
00:03:17,670 --> 00:03:25,000
 to 14 weeks, maybe more.

53
00:03:25,000 --> 00:03:28,610
 So you'd finish most of the, you'd get all the technique

54
00:03:28,610 --> 00:03:30,000
 down and then you'd come here

55
00:03:30,000 --> 00:03:35,390
 and put it in, put it into practice intensively for about a

56
00:03:35,390 --> 00:03:42,000
 week and you could pretty, pretty easily finish the course.

57
00:03:42,000 --> 00:03:44,900
 So there's still four slots available if someone wants to

58
00:03:44,900 --> 00:03:49,000
 take Tuesday morning or Thursday afternoon

59
00:03:49,000 --> 00:03:55,000
 or there's one on Saturday morning that's now available.

60
00:03:55,000 --> 00:04:01,850
 And apart from that, we have 24 people doing it, which is

61
00:04:01,850 --> 00:04:03,000
 great.

62
00:04:03,000 --> 00:04:06,040
 I don't know. I just ask them every time, "What did I give

63
00:04:06,040 --> 00:04:07,000
 you last time?"

64
00:04:07,000 --> 00:04:13,880
 I don't keep track of anything. They could be lying. They

65
00:04:13,880 --> 00:04:24,000
 could...

66
00:04:24,000 --> 00:04:27,780
 Our tradition, I think it's new, but our tradition isn't

67
00:04:27,780 --> 00:04:29,000
 all that huge.

68
00:04:29,000 --> 00:04:32,000
 I don't know any other traditions.

69
00:04:32,000 --> 00:04:37,470
 It's not something that I think is very well set up, very

70
00:04:37,470 --> 00:04:39,000
 commonly set up,

71
00:04:39,000 --> 00:04:41,620
 where you'd have this kind of online system. I do know that

72
00:04:41,620 --> 00:04:43,000
 people have done...

73
00:04:43,000 --> 00:04:46,190
 So if you're just talking about having online courses, I do

74
00:04:46,190 --> 00:04:48,000
 know people even in this tradition,

75
00:04:48,000 --> 00:04:52,090
 another monk under my teacher was probably the one who gave

76
00:04:52,090 --> 00:04:53,000
 me this idea

77
00:04:53,000 --> 00:04:55,990
 because he does this. So he's done this for a long time

78
00:04:55,990 --> 00:04:57,000
 where people would meet,

79
00:04:57,000 --> 00:05:00,120
 we would talk to him once a week and he'd lead them through

80
00:05:00,120 --> 00:05:01,000
 the course.

81
00:05:01,000 --> 00:05:10,000
 He's from Israel. As far as I know, he still does this.

82
00:05:10,000 --> 00:05:13,260
 So he'd benefit from this kind of setup. I could set him up

83
00:05:13,260 --> 00:05:14,000
 a website,

84
00:05:14,000 --> 00:05:17,300
 but there's a little bit of tension, you know, because the

85
00:05:17,300 --> 00:05:19,000
 whole international group of people,

86
00:05:19,000 --> 00:05:22,570
 the people I started with, they're not comfortable working

87
00:05:22,570 --> 00:05:23,000
 with me

88
00:05:23,000 --> 00:05:36,590
 because I had a falling out with their head guy many years

89
00:05:36,590 --> 00:05:43,000
 ago.

90
00:05:43,000 --> 00:05:53,000
 Are you able to get on the site yet?

91
00:05:53,000 --> 00:06:03,040
 Strange. Maybe there's a cable cut between Connecticut and

92
00:06:03,040 --> 00:06:05,000
 Ontario.

93
00:06:05,000 --> 00:06:08,000
 Well, we're going through Google that one.

94
00:06:08,000 --> 00:06:13,000
 This is in Pennsylvania, the server is in Pennsylvania.

95
00:06:13,000 --> 00:06:15,500
 It's just a question of where the server is. There might be

96
00:06:15,500 --> 00:06:21,000
 a problem. Who knows?

97
00:06:21,000 --> 00:06:39,000
 Oh.

98
00:06:39,000 --> 00:06:41,180
 The problem we have here is no one hears. If you're

99
00:06:41,180 --> 00:06:44,540
 listening to the audio, you don't hear Robin. So she's

100
00:06:44,540 --> 00:06:52,000
 talking to me.

101
00:06:52,000 --> 00:06:56,000
 Yeah, I know. I saw it.

102
00:06:56,000 --> 00:07:00,000
 So there's a group practicing karma, pa meditation.

103
00:07:00,000 --> 00:07:07,000
 I'm not too enthusiastic about it. It's worth going.

104
00:07:07,000 --> 00:07:10,060
 I don't think I have anything about karma, pa meditation. I

105
00:07:10,060 --> 00:07:12,000
 mean, I can't say I can't actually,

106
00:07:12,000 --> 00:07:14,490
 I don't think this was actually directed at me. It's kind

107
00:07:14,490 --> 00:07:16,000
 of what do you guys think?

108
00:07:16,000 --> 00:07:24,000
 So I'm just one of the guys, but.

109
00:07:24,000 --> 00:07:28,070
 Well, I don't have an informed opinion about karma, pa. I

110
00:07:28,070 --> 00:07:30,000
 don't really, I mean,

111
00:07:30,000 --> 00:07:32,740
 I could, I guess I can, I don't really have that much of an

112
00:07:32,740 --> 00:07:36,000
 informed opinion on going to other meditation groups.

113
00:07:36,000 --> 00:07:38,600
 I know that when I have done it, I would just do my own

114
00:07:38,600 --> 00:07:41,000
 meditation because it's not like they have thought police

115
00:07:41,000 --> 00:07:44,000
 that are making sure you do their meditation.

116
00:07:44,000 --> 00:07:47,590
 But it can also be a little bit awkward. I remember when I

117
00:07:47,590 --> 00:07:50,000
 was first starting in this technique, I would only do this.

118
00:07:50,000 --> 00:07:53,150
 And I went to a Dharma group and they did walking

119
00:07:53,150 --> 00:07:57,000
 meditation in a circle and I was kind of rude about it.

120
00:07:57,000 --> 00:07:59,750
 I just went off into, well, it wasn't rude. I went off into

121
00:07:59,750 --> 00:08:03,000
 a corner and did my own walking meditation back and forth.

122
00:08:03,000 --> 00:08:06,410
 But I think, you know, if I, if I, if that were to happen

123
00:08:06,410 --> 00:08:09,220
 today, I certainly wouldn't go off and do my own walking

124
00:08:09,220 --> 00:08:10,000
 meditation.

125
00:08:10,000 --> 00:08:13,140
 I would walk with them in a circle because they walk

126
00:08:13,140 --> 00:08:19,000
 together. They walked around the room in a line, you know?

127
00:08:19,000 --> 00:08:25,770
 So I wouldn't get as stubborn as I was. But for the most

128
00:08:25,770 --> 00:08:27,870
 part, you can do your own meditation, do a little bit of

129
00:08:27,870 --> 00:08:32,000
 their meditation, if you like.

130
00:08:32,000 --> 00:08:36,570
 It's nice to go in a group, to be in a group, but I think

131
00:08:36,570 --> 00:08:39,020
 in the end you might find it more trouble than it's worth

132
00:08:39,020 --> 00:08:58,000
 unless you like their meditation technique.

133
00:08:58,000 --> 00:09:05,000
 Not, not no one. Most people don't though. You never know.

134
00:09:05,000 --> 00:09:08,000
 You might find someone who can read your mind.

135
00:09:08,000 --> 00:09:26,000
 So,

136
00:09:26,000 --> 00:09:37,060
 trying to know that I get caught in the train of thought

137
00:09:37,060 --> 00:09:48,350
 and then I automatically go back to noting my walking or

138
00:09:48,350 --> 00:09:49,000
 whatever.

139
00:09:49,000 --> 00:09:54,710
 I then recognize I was thinking, I see. Yeah, I mean, you

140
00:09:54,710 --> 00:10:00,000
 shouldn't automatically go back to noting walking.

141
00:10:00,000 --> 00:10:02,540
 You should try to catch the thought and say thinking. You

142
00:10:02,540 --> 00:10:11,000
 should stop walking and say thinking, thinking.

143
00:10:11,000 --> 00:10:15,270
 Or you can just ignore it. And again, if you recognize it,

144
00:10:15,270 --> 00:10:19,000
 walking you can to some extent just leave it alone.

145
00:10:19,000 --> 00:10:23,000
 Keep it simpler than worrying about this. Just in general,

146
00:10:23,000 --> 00:10:27,330
 if any one thing is taking you away significantly from the

147
00:10:27,330 --> 00:10:30,710
 walking, then that's when you'd want to really definitely

148
00:10:30,710 --> 00:10:33,000
 stop and start acknowledging it.

149
00:10:33,000 --> 00:10:37,060
 But to some extent, and it's a judgment call on your part,

150
00:10:37,060 --> 00:10:40,130
 you can do it either way. You don't have to stop to note

151
00:10:40,130 --> 00:10:43,090
 every single thing, but normally we would stop and put our

152
00:10:43,090 --> 00:10:46,830
 feet together and then say thinking, thinking or distracted

153
00:10:46,830 --> 00:10:49,000
, distracted.

154
00:10:51,000 --> 00:11:16,000
 So,

155
00:11:16,000 --> 00:11:22,000
 slow night.

156
00:11:22,000 --> 00:11:24,900
 All right. Well, originally I just intended this to be a

157
00:11:24,900 --> 00:11:27,840
 sort of come online and say hello kind of thing. So we don

158
00:11:27,840 --> 00:11:33,000
't have to sit around here for long periods of time.

159
00:11:33,000 --> 00:11:38,000
 It's just a way to connect with people.

160
00:11:38,000 --> 00:11:42,450
 Here we have somebody please put a, put a question tag at

161
00:11:42,450 --> 00:11:45,000
 the beginning of your posts.

162
00:11:45,000 --> 00:12:02,880
 Like Patrick, oh, just did start your post with a Q colon

163
00:12:02,880 --> 00:12:06,000
 space.

164
00:12:06,000 --> 00:12:10,420
 I don't know what awareness release and discernment release

165
00:12:10,420 --> 00:12:11,000
 are.

166
00:12:11,000 --> 00:12:14,280
 I think you're talking about Jaita, we Muthi and Panya, we

167
00:12:14,280 --> 00:12:21,290
 Muthi Jaita, we Muthi is it really it's, it depends exactly

168
00:12:21,290 --> 00:12:23,000
 who you're talking to.

169
00:12:23,000 --> 00:12:26,000
 I mean, Jaita, we Muthi involves Samatha meditation.

170
00:12:26,000 --> 00:12:37,100
 Panya, we Muthi involves we pass into meditation. Now you

171
00:12:37,100 --> 00:12:37,910
 might say that Jaita, we Muthi is only summit to meditation

172
00:12:37,910 --> 00:12:38,710
. So it's entering into the mundane Jhanas and Panya, we M

173
00:12:38,710 --> 00:12:45,410
uthi is attainment of Nibana or you can say Panya, we Muthi

174
00:12:45,410 --> 00:12:49,150
 is the practice attainment of Nibana without the Jhanas,

175
00:12:49,150 --> 00:12:52,000
 without the mundane Jhanas.

176
00:12:52,000 --> 00:12:57,350
 And Jaita, we Muthi is attainment of Nibana with also

177
00:12:57,350 --> 00:13:01,000
 having attained the Samatha Jhanas.

178
00:13:01,000 --> 00:13:04,440
 So there was a group of monks in the time of the Buddha who

179
00:13:04,440 --> 00:13:07,670
 this another monk came who became our hands and the monk

180
00:13:07,670 --> 00:13:10,980
 came up to them and asked them said, oh, so do you have all

181
00:13:10,980 --> 00:13:14,620
 these magical powers and have you attained the formless J

182
00:13:14,620 --> 00:13:16,000
hanas and so on.

183
00:13:16,000 --> 00:13:17,630
 And they said no. And he said, they said, well, how could

184
00:13:17,630 --> 00:13:22,500
 that be? And they said, well, we're Panya, we Muthi, we M

185
00:13:22,500 --> 00:13:27,580
uthi is is freedom and Panya, I guess is discernment, it's

186
00:13:27,580 --> 00:13:31,000
 wisdom is more common translation.

187
00:13:31,000 --> 00:13:33,730
 But they're just terms and you have to see how they're used

188
00:13:33,730 --> 00:13:37,670
. So Panya, we Muthi is said to is thought, I think by the

189
00:13:37,670 --> 00:13:40,070
 commentaries to mean that they didn't practice Samatha

190
00:13:40,070 --> 00:13:44,000
 meditation, they only practice we pass.

191
00:13:44,000 --> 00:13:47,730
 And it's curious, it's curious that in that suit, he doesn

192
00:13:47,730 --> 00:13:51,300
't ask and they don't deny having attained the first four J

193
00:13:51,300 --> 00:13:52,000
hanas.

194
00:13:52,000 --> 00:13:56,590
 And we argued about this some years back, there was a

195
00:13:56,590 --> 00:14:01,610
 debate about what this meant on one of the internet forums

196
00:14:01,610 --> 00:14:05,000
 back when I was involved with them.

197
00:14:05,000 --> 00:14:06,650
 And some and someone was saying, you know, trying to

198
00:14:06,650 --> 00:14:08,500
 explain that away and say, well, just because they didn't

199
00:14:08,500 --> 00:14:11,450
 doesn't mean they actually attain them. And I said, well,

200
00:14:11,450 --> 00:14:13,760
 honestly, they have to have attained the first four Jhanas

201
00:14:13,760 --> 00:14:17,000
 because they attain them when they realize Nibana.

202
00:14:17,000 --> 00:14:19,830
 So that's why in my mind, that's the reason why they couldn

203
00:14:19,830 --> 00:14:23,020
't ask about that. Because absolutely, if you become an Arah

204
00:14:23,020 --> 00:14:25,000
ant, you have to have attained Jhana.

205
00:14:25,000 --> 00:14:28,680
 It's just a super mundane Jhana. But you haven't you haven

206
00:14:28,680 --> 00:14:32,520
't attained or you don't have to attain the Arupa Jhanas or

207
00:14:32,520 --> 00:14:36,680
 magical powers, because those are specific to Samatha Jhana

208
00:14:36,680 --> 00:14:37,000
.

209
00:14:37,000 --> 00:14:50,000
 So it's a bit of a technical question.

210
00:14:50,000 --> 00:14:55,900
 Things can keep juice until dawn. As soon as dawn arises,

211
00:14:55,900 --> 00:14:58,490
 the time when you would then be able to eat solid food, the

212
00:14:58,490 --> 00:15:04,200
 accepted opinion is that the juice has to be thrown out or

213
00:15:04,200 --> 00:15:15,000
 used for other purposes, whatever those might be.

214
00:15:15,000 --> 00:15:18,740
 There's some debate or some talk about how if you accept it

215
00:15:18,740 --> 00:15:22,000
 for food, then you have to eat it. But I don't think that

216
00:15:22,000 --> 00:15:23,000
 applies to juice.

217
00:15:23,000 --> 00:15:25,360
 But so if your intention is to have it in the morning when

218
00:15:25,360 --> 00:15:27,860
 you receive it and then you decide, oh, instead I'll have

219
00:15:27,860 --> 00:15:30,380
 it in the afternoon. There's something about that where it

220
00:15:30,380 --> 00:15:31,000
's a problem.

221
00:15:31,000 --> 00:15:34,310
 But I'm not entirely sure how that that applies. I don't

222
00:15:34,310 --> 00:15:36,000
 really pay much attention to that.

223
00:15:36,000 --> 00:15:43,910
 It's but there is something like if you keep if you keep

224
00:15:43,910 --> 00:15:49,900
 salt as a medicine, you can't then the next day, sprinkle

225
00:15:49,900 --> 00:15:52,000
 it on your food.

226
00:15:52,000 --> 00:15:55,020
 But that doesn't have anything to do with your intention.

227
00:15:55,020 --> 00:15:57,090
 That's just you've kept it more than a day. So using it as

228
00:15:57,090 --> 00:15:59,000
 food isn't allowed.

229
00:15:59,000 --> 00:16:02,870
 But using salt as a medicine, you can keep salt for your

230
00:16:02,870 --> 00:16:07,000
 whole life. So yeah, as long as it wouldn't work anyway.

231
00:16:07,000 --> 00:16:10,650
 But yeah, juice is something that even if you get it in the

232
00:16:10,650 --> 00:16:22,000
 morning, you can keep it until the next morning.

233
00:16:22,000 --> 00:16:26,000
 Yeah, yeah, juice is great.

234
00:16:26,000 --> 00:16:32,000
 It's the answer to this.

235
00:16:32,000 --> 00:16:36,010
 The answer I mean, it's the happy medium, because a lot of

236
00:16:36,010 --> 00:16:39,560
 Buddhists, Theravada Buddhists who have who keep the rule

237
00:16:39,560 --> 00:16:42,520
 not to eat in the evening actually don't keep the rule

238
00:16:42,520 --> 00:16:47,770
 because they have a lot of soy milk and milk and even more

239
00:16:47,770 --> 00:16:52,420
 even heavier things like they were trying to argue to have

240
00:16:52,420 --> 00:16:55,000
 this soft tofu drinks like it's a ginger, sweet ginger,

241
00:16:55,000 --> 00:17:00,000
 sweet ginger drink with soft, soft tofu in it.

242
00:17:00,000 --> 00:17:04,430
 No, it's it's chunks of very soft tofu, the chunks of them

243
00:17:04,430 --> 00:17:09,000
 in the ginger drink. It's a Thai thing.

244
00:17:09,000 --> 00:17:11,530
 It's too sweet, but otherwise it's pretty good. I mean,

245
00:17:11,530 --> 00:17:18,810
 having ginger and tofu is great, but probably a bit too

246
00:17:18,810 --> 00:17:24,000
 sweet to be healthy.

247
00:17:24,000 --> 00:17:44,800
 Yeah, I mean, if someone comes in the morning and offers a

248
00:17:44,800 --> 00:17:47,610
 juice drink as well, then I can keep that for the afternoon

249
00:17:47,610 --> 00:17:48,000
.

250
00:17:48,000 --> 00:17:53,720
 But I can also because people have already already given me

251
00:17:53,720 --> 00:17:59,500
, they've given me the the meals in advance through the gift

252
00:17:59,500 --> 00:18:04,950
 cards, I can go to Tim Hortons potentially and get a juice

253
00:18:04,950 --> 00:18:06,000
 there.

254
00:18:06,000 --> 00:18:12,810
 Yeah, yeah, they have orange and apple juice. I think it's

255
00:18:12,810 --> 00:18:24,000
 it's overpriced, but I'm sure it's overpriced, but

256
00:18:24,000 --> 00:18:25,400
 it's a little bit more

257
00:18:41,600 --> 00:18:53,400
 Yeah.

258
00:18:53,400 --> 00:18:59,400
 Okay, well, thank you for working on the subtitles.

259
00:18:59,400 --> 00:19:15,950
 Okay, so you can't I guess you can't that means you can't

260
00:19:15,950 --> 00:19:25,400
 upload them yourself. You've looked and can't.

261
00:19:25,400 --> 00:19:34,770
 There's a YouTube subtitles add on that's probably the best

262
00:19:34,770 --> 00:19:39,400
. You cannot add a subtitle track to someone else's video.

263
00:19:39,400 --> 00:19:49,400
 You just have to share it.

264
00:19:49,400 --> 00:20:18,400
 So I have to be responsible to put this up.

265
00:20:18,400 --> 00:20:28,780
 Well, monastery fridges are, I mean, technically, it has to

266
00:20:28,780 --> 00:20:32,900
 be a little bit more organized than that. There has to be a

267
00:20:32,900 --> 00:20:35,400
 room in the monastery.

268
00:20:35,400 --> 00:20:39,210
 I mean, maybe even a building, I don't know how, if you

269
00:20:39,210 --> 00:20:43,040
 want to get really technical, but at least a room that is

270
00:20:43,040 --> 00:20:46,690
 designated as a, I think it's called a copy Agara or

271
00:20:46,690 --> 00:20:51,400
 something of room for the copy for the something like that.

272
00:20:51,400 --> 00:20:55,040
 I don't know. I can't remember the exact poly term, but it

273
00:20:55,040 --> 00:20:58,550
's, it's a place where the lay people that belongs to the

274
00:20:58,550 --> 00:21:02,400
 lay people. So the stuff in there is not for the monks.

275
00:21:02,400 --> 00:21:06,160
 So I, if, if, if I had juice in my room, if someone kept

276
00:21:06,160 --> 00:21:09,050
 juice in my room, then I couldn't drink it even if they'd

277
00:21:09,050 --> 00:21:10,400
 off, they offered it to me.

278
00:21:10,400 --> 00:21:13,510
 Like if there was a refrigerator in my room, but we

279
00:21:13,510 --> 00:21:17,320
 consider the kitchen to be off limits to me. All the stuff

280
00:21:17,320 --> 00:21:21,150
 in the kitchen is not mine. All the food and all the, any

281
00:21:21,150 --> 00:21:23,400
 of the drinks and any of the stuff.

282
00:21:23,400 --> 00:21:26,530
 Except the ice cubes. I had to make ice cubes a couple of

283
00:21:26,530 --> 00:21:29,670
 nights ago because no one had, no one made ice cubes. That

284
00:21:29,670 --> 00:21:34,400
's an important first aid item. We didn't have any.

285
00:21:34,400 --> 00:21:40,860
 Are you listening, Aruna? No, he's not. I did. I injured

286
00:21:40,860 --> 00:21:44,360
 myself. Yeah. I was putting up a screen. A couple of the

287
00:21:44,360 --> 00:21:48,400
 guys came from the Buddhism association came by on Sunday,

288
00:21:48,400 --> 00:21:52,400
 wait, Saturday, yesterday, Friday, maybe Friday.

289
00:21:52,400 --> 00:21:55,430
 And they wanted to watch a Buddhist movie or Buddhist

290
00:21:55,430 --> 00:21:58,400
 themed movie. I think they ended up watching just in order.

291
00:21:58,400 --> 00:22:01,230
 I don't know what they ended up watching. I didn't, I was

292
00:22:01,230 --> 00:22:04,450
 kind of, I came down and it was in the middle of actually,

293
00:22:04,450 --> 00:22:07,870
 I don't even want to say it was in the middle of something

294
00:22:07,870 --> 00:22:09,400
 that probably wasn't appropriate for a Buddhist monastery.

295
00:22:09,400 --> 00:22:14,560
 But anyway, I think probably we're going to have to talk

296
00:22:14,560 --> 00:22:18,310
 about if we want to do that again. But definitely if we

297
00:22:18,310 --> 00:22:20,720
 have Buddhist movies, I'm happy that they come. I think

298
00:22:20,720 --> 00:22:23,140
 they just didn't have a sense of what was a good Buddhist

299
00:22:23,140 --> 00:22:24,400
 movie worth watching.

300
00:22:24,400 --> 00:22:27,720
 But they, they, they asked if they could come over, do some

301
00:22:27,720 --> 00:22:31,400
 meditation and watch a Buddhist movie, which that's great.

302
00:22:31,400 --> 00:22:33,820
 So I put up the screen for them in the basement. But as I

303
00:22:33,820 --> 00:22:37,400
 was putting up the screen, I pinched my finger.

304
00:22:37,400 --> 00:22:42,850
 Pretty bad, actually, I'm surprised it didn't get all blood

305
00:22:42,850 --> 00:22:44,400
, blood blister.

306
00:22:44,400 --> 00:22:47,400
 But I immediately went and shoved it on.

307
00:22:47,400 --> 00:22:51,200
 Let's place it against something really, really cold in the

308
00:22:51,200 --> 00:22:55,110
 freezer, probably the thing that creates the ice and just

309
00:22:55,110 --> 00:22:57,400
 left it there for a bit.

310
00:22:57,400 --> 00:23:06,680
 Well, I looked for, well, I looked for ice cubes and couldn

311
00:23:06,680 --> 00:23:08,400
't find any.

312
00:23:08,400 --> 00:23:11,420
 Yeah, no, and I made ice cubes. There's ice cubes there. I

313
00:23:11,420 --> 00:23:14,750
 mean, I filled up the ice cube trays. They just weren't

314
00:23:14,750 --> 00:23:24,400
 full.

315
00:23:24,400 --> 00:23:49,640
 It doesn't matter whether the lay person intends to offer

316
00:23:49,640 --> 00:23:52,400
 it, it matters where it's kept.

317
00:23:52,400 --> 00:23:56,200
 It has to be kept outside of the monastic. If food or

318
00:23:56,200 --> 00:23:59,670
 anything is kept in the monastic quarters, that's not

319
00:23:59,670 --> 00:24:09,620
 allowed. The monks can't. It's called anta something kept

320
00:24:09,620 --> 00:24:12,400
 inside.

321
00:24:12,400 --> 00:24:17,580
 Yeah, I mean, we haven't kept, we haven't officially

322
00:24:17,580 --> 00:24:21,500
 designated the kitchen as being the special room, but that

323
00:24:21,500 --> 00:24:23,400
's sort of just a given.

324
00:24:23,400 --> 00:24:26,920
 In fact, I guess I could say the only part of this that

325
00:24:26,920 --> 00:24:30,400
 this really the monastic part is is the upstairs.

326
00:24:30,400 --> 00:24:41,680
 I would suggest that that is off limits. Keeping stuff

327
00:24:41,680 --> 00:24:52,960
 upstairs is not allowed or it's allowed, but I couldn't

328
00:24:52,960 --> 00:24:56,400
 partake of it.

329
00:24:56,400 --> 00:25:01,530
 Yeah, meditators, as long as the food is not kept in my,

330
00:25:01,530 --> 00:25:08,110
 not kept in my dwelling, not kept by me and not cooked by

331
00:25:08,110 --> 00:25:10,400
 me. Those are the three aspects.

332
00:25:10,400 --> 00:25:28,400
 They're not allowed.

333
00:25:28,400 --> 00:25:33,400
 Okay, enough. No.

334
00:25:33,400 --> 00:25:37,330
 Don't want to think was just sit here and people will start

335
00:25:37,330 --> 00:25:40,400
 to try and think up questions to throw at me.

336
00:25:40,400 --> 00:25:48,630
 If you have quite if they had if people had questions, they

337
00:25:48,630 --> 00:25:57,180
 would have asked them by now. If you had really, really had

338
00:25:57,180 --> 00:25:59,400
 questions.

339
00:25:59,400 --> 00:26:04,400
 Okay.

340
00:26:04,400 --> 00:26:08,530
 We'll have gundala Casey the story of gundala Casey, which

341
00:26:08,530 --> 00:26:11,400
 is a demo pad the next time a powder verse.

342
00:26:11,400 --> 00:26:13,550
 See, when we go without it for a couple of days and

343
00:26:13,550 --> 00:26:19,400
 everybody's looking forward to it. So, it's more special.

344
00:26:19,400 --> 00:26:22,400
 Okay, thank you everyone. Thanks Robin for helping out.

345
00:26:22,400 --> 00:26:48,400
 Okay.

