 Hello and welcome back to our study of the Dhammapada.
 Today we continue with verse 218, which reads as follows.
 Chanda Jato Anakati Manasaaja Putosya
 Kamisu Apatibhadhacito Udangso Toti Ujati
 which means, Chanda Jato, one whose mind is...
 No, one who is intent upon or inclined towards Anakati,
 that which is indescribable.
 One is intent upon that which is indescribable, inclined
 towards it, keen on it.
 Manasaaja Putosya Putosya, if one's mind leaps or plunges
 into, really gets into, is thrilled by, is excited by,
 is fully into, we would say in English, into that, the Anak
ata, the ineffable.
 Kamisu Apatibhadhacito, if one's mind is not bound up or
 tied down by karma, by sensuality.
 Udangso Toti Ujati, such a person is called Udangso Toti,
 one who is going upstream.
 One who has a stream going upwards.
 So this story was taught, this verse was taught in response
 to a story about an elder monk who was getting old and his
 students, his sada wiharika,
 the monk who lived with him. So in the time of the Buddha,
 it was established that new monks should spend some time
 with a senior monk
 in order to learn how to become a good monk. And so they
 would spend time, sometimes even staying in the same
 bedroom.
 You know, they wouldn't have beds, they wouldn't have the
 same bed, but they would live quite close so that the young
 monk could learn how to live from the old monk.
 Just watching and following and really taking care of the
 older monk and so on. So it became sort of mentorship.
 So it might not be correct to say this was a teacher-
student relationship, but it was a mentor and dependent
 relationship.
 Let's explain like that. Just sada wiharika, one who lives
 with you, learn how you be a good monk.
 And so the monks, I guess there was more than one living
 with him, asked him, they said, "Venerable Sir, what sort
 of benefits have you gained from the practice?
 What sort of results? Anything special?"
 Now this monk had indeed practiced and indeed gained
 something quite special from the Dhamma. He had become what
 we call an anagami.
 Anagami, sorry. Anagami. Anagami means one who doesn't come
 back.
 It's the third of four stages of enlightenment.
 When one first experiences nirvana, they're called as sotap
ana. Here we have this word sota again. Sota means a stream.
 It means that word is used because it means someone who is
 not going to fall back. They're being pulled towards
 enlightenment.
 It is the word stream to indicate that they're on their way
.
 It's like you get on an escalator and you can't go back
 down again. It just takes you up.
 But such a person still has a lot of defilements left,
 potentially.
 And so they may still get angry. They may still be craving
 things. They may still get worried.
 They won't be jealous or stingy, but they can still crave
 and get upset. Sad, frustrated, bored.
 But far less than ordinary people, they've come a long way
 and they've given up any kind of wrong view about the Dham
ma, any doubt about the Dhamma.
 Any wrong practices. They have no inclination to do things
 that are useless.
 They'll never make a mistake about what is useful and what
 is useless thinking.
 Something that is useless is actually spiritually
 beneficial, like rituals or rules.
 They know the way to become enlightened. But the higher
 stages start to get rid of more of the defilements.
 And so we have what's called a Sakadagami, someone who will
 come back, but will come back only once.
 That's because they've worked harder than a Sotapana. They
've moved on from there.
 And they've continued the work to the point where they're
 only going to come back once.
 And then after that, they'll free themselves completely
 from suffering.
 But an Anagami, the third stage, an Anagami has gotten rid
 of completely any kind of sensual desire.
 They've come to see it for the illusion that it is.
 And any kind of aversion or disliking of things.
 Any hatred of others, any anger, frustration or irritation.
 And so this is who this man was. He was actually quite
 advanced.
 Anyone who can claim to be free from all any form of sens
ual desire is quite impressive.
 Any kind of aversion or irritation is quite a powerful
 state.
 But he thought to himself, you see, because there's a
 fourth stage.
 And the fourth stage is very powerful. They say someone who
 becomes an Arahant.
 See, an Arahant is someone who has gotten rid of all
 delusion as well.
 So an Anagami might still get distracted, might still have
 conceit.
 Yeah, conceit is another big one. An Arahant might still be
 conceited, might still be proud of themselves.
 Might still have low self-confidence.
 Feeling bad about the fact that they're not yet an Arahant.
 And it seems that that's what this monk was feeling.
 He felt kind of guilty for not being an Arahant because he
 thought to himself, you know, with an Arahant, an Arahant,
 if a lay person becomes an Arahant, they have to ordain.
 If they don't, they say they will die.
 There's just two ways. Either they pass away or they become
 a monk and they go an Arahant so they don't have to engage
 in practices that they just see no use in.
 I mean like getting a job and seeking out food and that
 sort of thing.
 But an Anagami, there are many lay people who became Anagam
is.
 And so he thought to himself, this is just, I'm no better
 than a lay person.
 Even a lay person could become an Anagami.
 And he thought to himself, I'm not going to declare my
 attainment until I become an Arahant.
 I'll wait until I become an Arahant and then I'll let these
 guys know what I've gotten out of the practice.
 I really wait until I've realized the complete goal.
 And so he didn't say anything.
 And again and again, time and again, they would ask him and
 they started to get worried thinking, why isn't he
 answering?
 He must have gained nothing from the practice and was
 feeling guilty about it.
 So they were concerned and they asked him again and again
 and never said anything.
 And then he died.
 He died without becoming an Arahant and so when he passed
 away, he was an Anagami.
 He didn't return. Where did he go? He went to what we call
 the Sudha Vasa.
 Sudha means pure and Awasa. Awasa means abode or dwelling.
 So the pure abodes.
 They are Brahma worlds, but they're very special Brahma
 world.
 They're a world that beings exist to have become Anagami
 and that's it.
 Only people have become Anagami.
 Only beings that have become Anagamis are reborn there.
 It's a very special realm of existence.
 And these monks were quite upset because they didn't know
 any of this and they thought their teacher had, or their
 mentor,
 the guy who they looked up to had gotten nothing out of the
 practice.
 They felt bad for him. Maybe they felt bad for themselves
 for wasting their time on such a teacher.
 And they went to the Buddha and they were crying apparently
 and moaning and wailing and so on.
 And the Buddha said, "What's this all about?"
 And he said, "Oh, Venerable Sir, our poor teacher just
 passed away."
 And the Buddha said, "Well, haven't I taught that this is
 the nature of things to pass away?
 Why are you crying about someone dying? Aren't you real
 monks? Like hasn't your teacher taught you anything?"
 Crying about death when death is such an obvious and
 glaringly certain part of life.
 It's not something to be shocked about or upset about.
 And they said, "Well, it's not that. We know that, Vener
able Sir. It's just that he died without getting anything.
 We asked him again and again and he didn't say anything. He
 must have died and who knows where he went
 because he had no benefit from the practice."
 And the Buddha explained to them, he said, "Oh, no, no. My
 son has been reborn in the Sudavasa.
 He just was embarrassed because he was only an Anagami and
 he thought he'd wait until he became an Arhat."
 And then he taught this verse explaining that really they
 should have known or could have known
 or maybe not based on their own state of mind, but it
 wouldn't be possible to know.
 There's no possibility that that monk could have gone
 anywhere else because not of some category,
 but because of his state of mind, his qualities of mind.
 And that's what this verse is meant to highlight.
 So we can get a couple of things from this story.
 The first is, of course, the most obvious one and the main
 lesson of the story, I think, is not to be complacent.
 An Anagami is such a high attainment and yet even people or
 maybe especially people have become Anagamis.
 One thing it does to them is make them not complacent.
 I think there was a story about some Anagamis that became
 complacent and the Buddha had to scold them, admonish them.
 But here we have this shows that this monk pointed out an
 important lesson that even when you have gotten to that
 state,
 you have to remember that you still have defilements.
 Why would it mean that there's a difference between an Anag
ami and an Arhat is that you still have to concede and rest
lessness and ignorance.
 Still have desire for form and formlessness and so on.
 No central desire left.
 And so it's a really good example for us, for people who
 have maybe not gained anything from the practice to remind
 ourselves,
 you know, some people become content just with calm.
 They practice meditation for a bit and they feel calm and
 they become content with that.
 Some people gain insight, gain some good understanding
 about how to live their lives and they become content with
 that.
 They become content with something that's really
 ineffective in the long run.
 You know, it will have an effect on our lives, but it has
 no potential for or it has no guarantee.
 It has no power to free you from suffering.
 You feel calm and it's very easy to lose that and get back
 to square one.
 You gain insight. Maybe it changes your course in life,
 which is a very good thing.
 But who knows how long that will last. It will go away. You
'll forget about it.
 The Buddha said, "Asantuti, Bahuloh." You want to be a
 great being. You want to really do something great,
 something really and truly great.
 You have to be discontent.
 Not discontent in the way we normally think about it. The
 Buddha taught and praised contentment,
 but discontent about your own spiritual attainments, your
 own quality of mind,
 until you have freed yourself from all bad habits, bad
 qualities of mind,
 until you've freed yourself from all ignorance and you've
 come to understand reality just as it is.
 You should not be complacent. You should not be content
 with what you've done.
 Of course, people who have attained sotapana become compl
acent with good reason because it's quite a change.
 It's quite an accomplishment and your whole being feels
 different with the peace and the security, the feeling of
 stability of mind.
 Sakadagami, Anagami, it's still possible to become compl
acent.
 So just a reminder, of course, that in our practice, if
 even an Anagami is able to admonish themselves not to be
 complacent,
 we should remember the reason why we're practicing.
 It's not just because it's beneficial to us. That's one way
 of looking at it, but also because we have things that are
 unbeneficial that we need to address.
 We really do have an imperative task to perform.
 That is to learn, to study, to become familiar and
 understand and overcome our bad habit, our unwholesome
 qualities of mind.
 So that's the first lesson.
 The second lesson, I think, is a little more subtle and may
 or may not be there, but it appears that the Buddha is
 perhaps,
 or you could take this as an admonishment or as a lesson
 for us,
 not to put too much emphasis on titles, proclamations,
 identities.
 Like we have these four stages of enlightenment, it's very
 easy for people to obsess about them.
 I've heard people say, "My goal is to become a sotapana in
 this life."
 I don't mean to criticize that, but it really isn't a great
 way to focus.
 I mean, it isn't the most important thing to focus on. Your
 focus should be on, of course, being mindful.
 And this is a good example of the distinction. These monks
 were so focused on this idea,
 this switch that they could turn in their minds where they
 switched from thinking of him as an ordinary person to an
 enlightened person,
 that they never even saw how enlightened he was.
 Because if they lived in the same room with him, it's
 certainly possible that it's very hard.
 I mean, it is maybe a third lesson. It is hard to know
 whether someone else is enlightened.
 It's a very personal thing. But they lived so close to him.
 And I think it's quite possible that had they not been so
 obsessed with trying to determine whether he was or wasn't,
 they would be able to instead be very mindful and become in
 tune with his state of mind
 and really get a sense that most likely he was not an
 ordinary being.
 I think it's very hard to mistake an anagami for an
 ordinary being.
 But third lesson, it can be difficult.
 You see, even these monks who lived with the anagami didn't
 know.
 And they didn't know because, two reasons.
 One, because they weren't paying attention.
 Two, because it is very hard to know.
 The Dhamma is bhachatang, to be known for oneself.
 It really isn't always the best to proclaim your attainment
.
 The monks aren't allowed to talk about them with people who
 aren't monks.
 But even among monks, sometimes it's better just to focus
 more on qualities of mind.
 Focus more on what's important and focus more on oneself.
 Someone asks you, "Are you enlightened?"
 They say, "Why do you care about me?"
 You should turn it back on them and say, "The only
 enlightenment you should be concerned with is your own."
 Sometimes people try to test.
 I don't get it so much, but I suppose I have.
 I don't notice it so much, but I've heard of monks getting
 tested.
 I think it happens a lot in Buddhist societies.
 You have Buddhists who know a lot, and they're very
 skeptical of other Buddhists.
 And so they go around to monks and they try to test them to
 see how enlightened they are.
 In some ways it's a good service because it does test the
 monks.
 They might try and make them irritated or annoyed.
 But on the other hand, it's really a waste of that person's
 life.
 Why are they so focused on determining whether other people
 are enlightened?
 And it becomes a bit of a problem from the other end as
 well
 when someone does identify another person as an enlightened
 being correctly or incorrectly.
 I think it's very common to incorrectly or with
 insufficient evidence
 identify someone else as being enlightened or oneself even.
 I think it's possible to overestimate one's own attainment.
 And it becomes, you see this is the thing about labels, it
 becomes an entity,
 a person who is an arahant, a person who is an anagami.
 And you put them up on a pedestal and it makes you feel
 good if they're your teacher, right?
 My teacher is such and such, which is really awful because
 them being such and such says nothing about you even if
 they are.
 I've seen very pure monks who have very, very poor and imp
ure students or followers.
 Just because you follow the Buddha even said it, just
 because you follow around.
 If you were to follow around holding on to my robe, I can't
 take you to nibbana.
 That's not what he said exactly, but basically that.
 Sit with me all day, hang out with me all day is nothing to
 you.
 It has potential to be completely unbeneficial when you
 could be off meditating and following my teachings.
 He did say that, basically.
 So there's the lessons from the story.
 Don't be complacent, but also don't obsess about titles and
 categories and so on.
 The focus should be on what the verse focuses on and why I
 think it may be very well with the Buddha,
 a part of the Buddha's lesson, this sort of admonishment
 not to do that.
 It's because of what the Buddha says. He says, look, of
 course he was an anagami.
 He had all these qualities, where he had, that's not a lot
 of qualities,
 but this basic quality of mind that distinguishes someone
 who is not coming back from someone who is coming back.
 So what is it?
 Akata is something that is explained or discussed,
 something that is told.
 So nibana, anakate is the thing which cannot be told, which
 cannot be explained, not be described.
 And apparently that's what ineffable means. So we can say
 it's the ineffable.
 It's very different. It's ineffable.
 The problem with those sorts of words is that it gives it
 this sort of mystery, which it really shouldn't.
 We should absolutely understand that that is an essential
 quality of it, that being ineffable.
 It doesn't mean it's something that if only we were smarter
 we could talk about, we could explain, or we could
 understand, we could conceive of it.
 It's like the inconceivable. It's a thing that is literally
, like actually inconceivable.
 In the sense of it's not a, there's no concept involved
 with it. There's no entity per se.
 It exists, but it exists in a different way than formed
 things that you can describe, conceive of, that they exist.
 And that's exactly what it is. It's that thing that has
 those qualities, has the quality of being indescribable.
 It's the thing that is indescribable. It's that which is
 outside the realm of being describable.
 That's just what it is. It's not something mysterious. It's
 just that thing.
 I haven't made it much clearer by saying that, but I just
 want to explain that it's not meant to make it mysterious
 or obfuscate the meaning or anything.
 It's just talking about what is different from everything
 else.
 Here I am talking a lot about it. Just trying to explain
 why it's what it is.
 And so, chanda jata. Chanda is an important word. He uses
 this, I think, because chanda can be used both ways.
 Chanda actually is often used as a replacement for desire.
 In Thai they say paujai, which I think is a good, we don't
 really have a similar way of talking about things in
 English.
 But someone is paujai. Jai is the heart. Pau is enough. Pau
jai means contentment or finding something to be enough.
 But they use it to mean liking. If you're paujai in regards
 to food, it means you like it.
 And so it's kind of used in the sense of liking something,
 but it more literally means interest or inclination,
 intention, in the sense of being intent on something.
 So one translator, I think, uses intent on the ineffable.
 Inclined is a good word because the Buddha uses this
 imagery of a tree leaning in one direction.
 And this is where the distinction is made here. Someone who
 is inclined towards nibbana is really very incategorically
 different from someone who is inclined towards kama,
 towards sensuality.
 And that's the real distinction here. It's a very simple
 description of the quality of enlightenment, that it incl
ines away from sensuality and towards the opposite of sens
uality.
 And so this is, it's an important point about this, is that
 it's a description of the problem, what's wrong with us,
 with ordinary life, with ordinary inclination.
 Because when you hear about nibbana, for example, when you
 hear about nibbana, when you hear about the path, when you
 hear about the direction, it's often quite hard to swallow.
 It's unpalatable, fearsome even. Some people who really
 like what the Buddha said will become inclined to practice.
 You know, they meet Buddhists and they really feel
 attracted to the path and to the ideas behind it.
 But then they hear about nibbana and it scares them.
 And what is this I've gotten myself into? One meditator
 told me, "Wait, do you know what this means? Do you really
 get what we're doing here?"
 It just shocked him and he couldn't go on. He couldn't get
 past this because he liked the idea of gaining benefit from
 meditation, but he understood what the meaning was here.
 You know, nibbana involves non-arising. And so often our
 practice seems to be trying to change our opinion about nib
bana, trying to like it, trying to incline towards it.
 We want to create in our minds this inclination towards nib
bana, but it's not quite like that.
 It's in fact the disinclination that we take as our object.
 It's not that we practice liking it. It's not that we
 practice or I guess more common.
 It's not that we repress our distaste for nibbana, our dist
aste for having to give up all of this, all of what we enjoy
, all of what we seek, all of what we cling to,
 all of what we are patibaddajita, all of what our minds are
 stuck on, are attached to.
 Our practice is understanding this distaste. I mean, the
 struggle in the mind of a Buddhist to come to terms with
 something as the goal that they really don't like,
 they really don't even like the idea of it. They might not
 admit it, they'll say yes, yes, nibbana, but when they
 think about it, it scares them and it's repulsive.
 It's not about repressing that or trying to be the person
 who likes that. No, it's about looking at it.
 That's the actual problem, looking at this, not just that,
 but looking at the whole reason why that is, why it is that
 we're disinclined towards something that really, I mean, it
's ineffable.
 How could you be disinclined? How could you hate it? You
 see, because our distaste for it, fear of it.
 It's nothing to do with it. There's nothing to fear about
 nibbana. There's nothing fearsome. It doesn't have fangs or
 claws or poison.
 It's not going to say bad things about you or betray you.
 It's not even going to arise.
 No, it's all of the things that it stands in opposition to,
 all of those things again that we cling to.
 And so our whole of our practice, our practice, this is why
 our practice is not about nibbana. Why is our practice
 about suffering?
 Because all of those things that we cling to are actually
 suffering, are actually dukkha. They may not be painful,
 not all the time, but they're caught up in pain.
 They're caught up in disappointment. They have no sukha. D
ukkha means they can't bring sukha, they can't make us happy
.
 They can't satisfy us. We strive for them again and again
 and again. And we always have in our mind this idea that
 they are going to, those things that I don't have yet, are
 going to bring me happiness.
 Not even intellectually, it's just an inclination. We're a
 tree leaning in the wrong direction. So that's the way we
're going to fall.
 It's endless. There's no goal. We're like a dog that has a
 rash and it keeps scratching.
 Because there's some sort of appeasement that comes from
 scratching. But the scratching just makes it worse.
 And so either they end up scratching themselves to death
 until they bleed and infect it and die. Or they stop. They
 find a way to stop themselves.
 Perhaps they stop for some time because of the pain that
 comes from it. But there's no end. You can't get satisfied
 by getting the things you want.
 And so our practice is to learn this, is to understand this
, is to become freed from the bind, from the shackles of
 sensual desire.
 And we're really shackled to it. We can't be free. We can't
 be at peace. Because our minds are forcing us, really. We
're prisoners to our own pati-baddhajita, our minds that are
 bound to sensuality.
 It's like we're being pulled in that direction by a strong
 rope in the meditation way and we need a rope in the other
 direction.
 And we pull ourselves until we incline towards the ineff
able. And that's how you become udang sota.
 Udang sota, if you think of it as against the stream, it's
 like a fish going against the stream.
 If you think of it as upstream, I think it probably means
 more like has a stream going upwards. They're on the escal
ator upwards.
 They're heading in the right direction. They're bound in
 the right direction.
 Why? Because their mind is inclined in the right direction.
 And that's really this emphasis and this focus on qualities
. How does the mind incline?
 Does your mind leap at the thought of peace? Or does it
 incline towards conflict?
 When someone you hear someone does something, says
 something wrong, do you incline to correct them or make
 them punish them for it?
 When something pleasurable comes to you, incline to get it,
 to keep it, to get it as close to you and as much, possess
 it as much as possible.
 Or do you see it mindfully? Do you see it experience it and
 let it come and let it go?
 One is the way to stress and prison. The other is the way
 to freedom and peace.
 So I think focusing on this is a very simple way to
 describe this distinction. That you shouldn't be concerned
 that you don't like peace.
 That you aren't inclined towards things like nimbana and
 freedom from suffering and cessation.
 To never be born again. You shouldn't be concerned about
 that. That's the whole reason. That's the whole crux of
 this. That's what you have to look at.
 It's like a challenge in a way. Pointing out nimbana,
 pointing out freedom from suffering is a challenge.
 It shows you, you have to justify. You really do have to
 examine because now you have an alternative. The thing you
 don't want.
 No, I want these. You have to be able to justify to
 yourself that it's better than this ultimate peace.
 Ultimate freedom. And so it means you examine. You don't
 worry about nimbana for sure, not.
 You examine the tree that you're in until you see there's
 no fruit on this tree and then you can fly away.
 Some thoughts on the verse for tonight. That's Dhammapada
 218. Thank you all for listening.
