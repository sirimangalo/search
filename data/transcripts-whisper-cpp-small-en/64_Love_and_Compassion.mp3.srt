1
00:00:00,000 --> 00:00:02,000
 Love and compassion.

2
00:00:02,000 --> 00:00:06,080
 What is the difference between metta and compassion?

3
00:00:06,080 --> 00:00:10,800
 Metta and karuna are like two sides of the same coin.

4
00:00:10,800 --> 00:00:15,530
 Metta means friendliness. It comes from the word mitta,

5
00:00:15,530 --> 00:00:16,320
 meaning a friend.

6
00:00:16,320 --> 00:00:20,730
 You strengthen the 'I' and it becomes an 'E' and so you get

7
00:00:20,730 --> 00:00:23,440
 metta, the state of being a friend.

8
00:00:23,720 --> 00:00:28,150
 Metta refers to the inclination that beings should be happy

9
00:00:28,150 --> 00:00:32,150
, leading to act, speak and think in ways that promote the

10
00:00:32,150 --> 00:00:33,440
 happiness of others.

11
00:00:33,440 --> 00:00:37,840
 Karuna, compassion, is the complementary wish for beings to

12
00:00:37,840 --> 00:00:38,880
 not suffer.

13
00:00:38,880 --> 00:00:42,960
 It leads one to act, speak and think in ways that help free

14
00:00:42,960 --> 00:00:45,360
 other beings from suffering.

15
00:00:45,360 --> 00:00:50,200
 Neither metta nor karuna themselves refer to actions.

16
00:00:50,360 --> 00:00:53,480
 They are states of mind that will inform your actions,

17
00:00:53,480 --> 00:00:56,160
 helping to bring about positive results.

18
00:00:56,160 --> 00:01:00,730
 One cultivates metta by repeating a mantra like 'May all

19
00:01:00,730 --> 00:01:02,000
 beings be happy'

20
00:01:02,000 --> 00:01:05,580
 and the result will be an inclination to bring happiness

21
00:01:05,580 --> 00:01:06,840
 and peace to others.

22
00:01:06,840 --> 00:01:10,560
 With karuna, the mantra is something like 'May all beings

23
00:01:10,560 --> 00:01:12,160
 be free from suffering'

24
00:01:12,160 --> 00:01:15,660
 which will result in an inclination to work to relieve

25
00:01:15,660 --> 00:01:16,680
 suffering.

26
00:01:17,240 --> 00:01:21,370
 In the end, both are working toward the same goal, as true

27
00:01:21,370 --> 00:01:23,720
 happiness is freedom from suffering.

28
00:01:23,720 --> 00:01:26,240
 They are one in the same.

29
00:01:26,240 --> 00:01:28,800
 But the state of mind is different.

30
00:01:28,800 --> 00:01:32,480
 Friendliness is an inclination towards positive results

31
00:01:32,480 --> 00:01:36,420
 and compassion is an inclination away from negative results

32
00:01:36,420 --> 00:01:36,720
.

33
00:01:36,720 --> 00:01:40,640
 In brief, karuna is the inclination to take away suffering

34
00:01:40,640 --> 00:01:44,440
 and metta is the inclination to bestow happiness.

35
00:01:44,440 --> 00:01:46,440
 [Music]

