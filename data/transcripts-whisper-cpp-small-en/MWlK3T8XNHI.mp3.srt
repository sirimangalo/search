1
00:00:00,000 --> 00:00:03,790
 Thinking certain ways and to behave in certain ways, we

2
00:00:03,790 --> 00:00:09,930
 become accustomed and habituated to, you know, to

3
00:00:09,930 --> 00:00:11,800
 conceptualize certain things.

4
00:00:11,800 --> 00:00:19,330
 And sometimes they create stress, they create detriment in

5
00:00:19,330 --> 00:00:20,800
 your lives.

6
00:00:20,800 --> 00:00:25,020
 And it's not until you actually gravitate towards spiritual

7
00:00:25,020 --> 00:00:28,960
 practices such as meditation that you realize that those

8
00:00:28,960 --> 00:00:31,800
 things are truly dangerous to your mind.

9
00:00:31,800 --> 00:00:35,750
 It's, you know, as you start to really calm down and shar

10
00:00:35,750 --> 00:00:41,710
pen your mind and really observe what you have been learning

11
00:00:41,710 --> 00:00:45,360
 for your entire life, you start to realize that you've been

12
00:00:45,360 --> 00:00:46,800
 really just hurting yourself.

13
00:00:46,800 --> 00:00:51,850
 So yeah, I would say that it is a process of learning what

14
00:00:51,850 --> 00:00:54,800
 your society has taught you.

15
00:00:54,800 --> 00:00:59,710
 I wouldn't say it's a process of unlearning all of social

16
00:00:59,710 --> 00:01:00,800
 reality.

17
00:01:00,800 --> 00:01:04,800
 Yeah.

18
00:01:04,800 --> 00:01:09,550
 So yeah, you basically start to see clearly what your

19
00:01:09,550 --> 00:01:15,200
 society has taught you that's detrimental. You might not

20
00:01:15,200 --> 00:01:17,800
 just forget everything.

21
00:01:17,800 --> 00:01:20,800
 Yeah, it's being able to see what is useful.

22
00:01:20,800 --> 00:01:25,560
 Yeah, exactly. Yeah, because, you know, basically

23
00:01:25,560 --> 00:01:29,870
 everything that we do has been socialized to some extent. I

24
00:01:29,870 --> 00:01:33,800
'm not saying that everything, you know, like...

25
00:01:33,800 --> 00:01:39,060
 Yeah, but yeah, and some of them are beneficial and some of

26
00:01:39,060 --> 00:01:41,800
 them are neutral and some of them are detrimental.

27
00:01:41,800 --> 00:01:46,260
 And part of the meditation practice is to see clearly on

28
00:01:46,260 --> 00:01:49,870
 what is good for us, that we should strengthen it more,

29
00:01:49,870 --> 00:01:52,800
 what is bad for us, that we should, you know, minimize it.

30
00:01:52,800 --> 00:01:56,830
 And basically just, you know, and it's a progressive

31
00:01:56,830 --> 00:02:01,090
 practice, the more we do it, the more we see it clearly and

32
00:02:01,090 --> 00:02:03,800
 the more peace we can maintain.

33
00:02:03,800 --> 00:02:08,770
 So yeah, I guess in some ways you can deviate from your

34
00:02:08,770 --> 00:02:13,090
 society. But I guess that's sometimes you just have to do

35
00:02:13,090 --> 00:02:16,800
 that to maintain that sort of peace for yourself.

36
00:02:16,800 --> 00:02:21,800
 Bravo.

37
00:02:21,800 --> 00:02:26,710
 Isn't he great? It's not often that a teacher can show off

38
00:02:26,710 --> 00:02:27,800
 his students.

39
00:02:27,800 --> 00:02:31,560
 I'm not showing off. It's just an interesting... I thought

40
00:02:31,560 --> 00:02:36,060
 it would be interesting for everyone to see here we have a

41
00:02:36,060 --> 00:02:37,800
 real live meditator.

42
00:02:37,800 --> 00:02:41,990
 Someone... and you know, he's not... he doesn't look... he

43
00:02:41,990 --> 00:02:42,800
 smiles.

44
00:02:42,800 --> 00:02:47,650
 He's been practicing diligently for 21 days. Excellent

45
00:02:47,650 --> 00:02:52,800
 response. In fact, you know, I learned something from it.

46
00:02:52,800 --> 00:02:53,800
 Thank you.

