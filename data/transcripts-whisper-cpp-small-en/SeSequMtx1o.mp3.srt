1
00:00:00,000 --> 00:00:05,000
 How should one deal with things he hasn't experienced yet?

2
00:00:05,000 --> 00:00:09,460
 For example, rebirth, heaven and hell, realms, psychic

3
00:00:09,460 --> 00:00:10,000
 powers?

4
00:00:10,000 --> 00:00:15,000
 What do you think?

5
00:00:15,000 --> 00:00:21,000
 I think we shouldn't care about them.

6
00:00:21,000 --> 00:00:23,000
 I think I'm with you.

7
00:00:23,000 --> 00:00:27,000
 You should only deal with what you can experience.

8
00:00:29,000 --> 00:00:33,400
 I think that you have already experienced rebirth, you just

9
00:00:33,400 --> 00:00:35,000
 don't remember.

10
00:00:35,000 --> 00:00:40,000
 Yeah, I mean there's something to it.

11
00:00:40,000 --> 00:00:43,990
 How do you know you've experienced that maybe you don't

12
00:00:43,990 --> 00:00:45,000
 remember?

13
00:00:45,000 --> 00:00:49,000
 Well, where did you come from?

14
00:00:49,000 --> 00:00:54,000
 There's no beginning to this cycle of life.

15
00:00:54,000 --> 00:00:57,000
 There's no beginning, but there is an end.

16
00:00:57,000 --> 00:00:59,810
 And we're clearly not at the end if we're still

17
00:00:59,810 --> 00:01:01,000
 experiencing.

18
00:01:01,000 --> 00:01:07,000
 You just said that there is an end?

19
00:01:07,000 --> 00:01:09,000
 Yes, there is.

20
00:01:09,000 --> 00:01:13,680
 I don't understand how you can know that there is no

21
00:01:13,680 --> 00:01:17,000
 beginning, but there is an end.

22
00:01:17,000 --> 00:01:22,710
 If you have faith, I guess, I mean I haven't experienced it

23
00:01:22,710 --> 00:01:25,000
 personally, but...

24
00:01:25,000 --> 00:01:27,000
 That's the question that begs the question.

25
00:01:27,000 --> 00:01:32,130
 If you already have faith, then you need to experience

26
00:01:32,130 --> 00:01:37,000
 something to think about it or to believe in it.

27
00:01:37,000 --> 00:01:42,910
 I mean, I feel like on the path you start to see the

28
00:01:42,910 --> 00:01:49,000
 results come pretty much become more and more evident.

29
00:01:49,000 --> 00:01:56,110
 And you start to see the first, second, and fourth, and

30
00:01:56,110 --> 00:02:00,000
 then eventually you'll see the third.

31
00:02:00,000 --> 00:02:02,200
 So what you mean by the end, you're talking about nimbana,

32
00:02:02,200 --> 00:02:03,000
 just to clarify.

33
00:02:03,000 --> 00:02:06,800
 It's not really an end of the universe because the universe

34
00:02:06,800 --> 00:02:10,040
 keeps going on, but the end for that person is what you're

35
00:02:10,040 --> 00:02:11,000
 talking about.

36
00:02:11,000 --> 00:02:13,000
 The cycle of rebirth.

37
00:02:13,000 --> 00:02:16,000
 Right, the ending to the cycle of rebirth.

38
00:02:16,000 --> 00:02:19,830
 Yeah, because it says at the beginning there is no

39
00:02:19,830 --> 00:02:21,000
 beginning.

40
00:02:21,000 --> 00:02:23,000
 Well, there's a potential end.

41
00:02:23,000 --> 00:02:26,800
 I think a curious thing, if I'm not mistaken, is that there

42
00:02:26,800 --> 00:02:31,000
's no guarantee that every being is going to reach nimbana.

43
00:02:31,000 --> 00:02:37,000
 There's an idea that perhaps beings are also infinite,

44
00:02:37,000 --> 00:02:40,000
 which is interesting.

45
00:02:40,000 --> 00:02:45,060
 It's all kind of mind-blowing and gets us very much off the

46
00:02:45,060 --> 00:02:46,000
 topic.

47
00:02:46,000 --> 00:02:50,000
 Ignoring all this stuff can actually be useful.

48
00:02:50,000 --> 00:02:53,000
 What I don't like is when people get bigoted about it.

49
00:02:53,000 --> 00:02:57,160
 It seems to me, as soon as anyone brings up rebirth, it

50
00:02:57,160 --> 00:03:01,000
 just gets, "Oh, that's not Buddhism," and so on.

51
00:03:01,000 --> 00:03:05,100
 When people talk about magic powers and say, "That's not

52
00:03:05,100 --> 00:03:08,440
 Buddhism," well, maybe magical powers you could have a

53
00:03:08,440 --> 00:03:09,000
 point.

54
00:03:09,000 --> 00:03:12,170
 But why then does the Buddha seem to, according to our

55
00:03:12,170 --> 00:03:15,580
 scriptures, again and again and again talk about these

56
00:03:15,580 --> 00:03:16,000
 things?

57
00:03:16,000 --> 00:03:20,060
 When the Buddha outlines the entire path, both remembering

58
00:03:20,060 --> 00:03:25,270
 your past lives and gaining magical powers are a part of

59
00:03:25,270 --> 00:03:26,000
 that,

60
00:03:26,000 --> 00:03:29,690
 or every time a part of that path, every time the Buddha

61
00:03:29,690 --> 00:03:31,000
 talks about it.

62
00:03:31,000 --> 00:03:35,000
 When he condenses it, you'll see that they disappear.

63
00:03:35,000 --> 00:03:39,270
 But when he talks about the full and entire path, they seem

64
00:03:39,270 --> 00:03:41,000
 to be very much a part of...

65
00:03:41,000 --> 00:03:44,300
 You can't say they're not a part of the Buddha's teaching

66
00:03:44,300 --> 00:03:46,000
 because he teaches them.

67
00:03:46,000 --> 00:03:49,120
 He obviously doesn't say they're the end, and he says quite

68
00:03:49,120 --> 00:03:53,000
 often they're not the end, but he teaches them.

69
00:03:53,000 --> 00:03:56,490
 We always try to make the point that you don't need these

70
00:03:56,490 --> 00:03:59,000
 things, and you don't have to think about them.

71
00:03:59,000 --> 00:04:02,750
 If they're causing you problems, well, then you're barking

72
00:04:02,750 --> 00:04:04,000
 up the wrong tree.

73
00:04:04,000 --> 00:04:10,490
 But they do have a place in Buddhism. They're quite

74
00:04:10,490 --> 00:04:12,000
 interesting.

75
00:04:12,000 --> 00:04:14,000
 It might even be something to talk about.

76
00:04:14,000 --> 00:04:17,900
 The rebirth can be quite useful to help you to gain an

77
00:04:17,900 --> 00:04:23,000
 understanding of reality that's more in line with reality,

78
00:04:23,000 --> 00:04:29,080
 as opposed to the Western mindset of physical death equals

79
00:04:29,080 --> 00:04:37,070
 death, which is totally out of line with experience and

80
00:04:37,070 --> 00:04:41,000
 experience of reality.

81
00:04:41,000 --> 00:04:44,000
 Are we okay with that?

82
00:04:44,000 --> 00:04:49,000
 Yeah. I have nothing else to say.

83
00:04:49,000 --> 00:04:51,000
 Okay. Let's quit.

