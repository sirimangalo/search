1
00:00:00,000 --> 00:00:07,000
 Hey, good evening.

2
00:00:07,000 --> 00:00:12,200
 Hi, Robin.

3
00:00:12,200 --> 00:00:18,640
 Can you hear me?

4
00:00:18,640 --> 00:00:22,920
 I don't hear you.

5
00:00:22,920 --> 00:00:28,400
 Okay, we're ready to start.

6
00:00:28,400 --> 00:00:31,600
 So good evening, everyone.

7
00:00:31,600 --> 00:00:39,560
 Broadcasting live from Stony Creek, Ontario, August 9th.

8
00:00:39,560 --> 00:00:42,640
 Oh, I heard you there.

9
00:00:42,640 --> 00:00:46,280
 I hear you.

10
00:00:46,280 --> 00:00:54,040
 You hear me?

11
00:00:54,040 --> 00:00:54,960
 I heard you.

12
00:00:54,960 --> 00:01:01,680
 I heard something.

13
00:01:01,680 --> 00:01:03,200
 I'm joined by Robin tonight.

14
00:01:03,200 --> 00:01:06,370
 If you'd like to join the Hangout, well, you have to get

15
00:01:06,370 --> 00:01:08,840
 special permission.

16
00:01:08,840 --> 00:01:11,200
 But it's not hard to get, just ask.

17
00:01:11,200 --> 00:01:14,720
 Well, actually, yeah, we probably wouldn't.

18
00:01:14,720 --> 00:01:19,120
 This isn't something I'm going to open up to everyone.

19
00:01:19,120 --> 00:01:25,800
 The idea here is to talk about this verse and answer people

20
00:01:25,800 --> 00:01:27,800
's questions.

21
00:01:27,800 --> 00:01:30,500
 But tonight we have something special as well that I'd like

22
00:01:30,500 --> 00:01:31,240
 to bring up.

23
00:01:31,240 --> 00:01:34,180
 And I was hoping Robin could maybe talk about it, but she's

24
00:01:34,180 --> 00:01:34,720
 muted.

25
00:01:34,720 --> 00:01:36,840
 I don't hear her.

26
00:01:36,840 --> 00:01:37,840
 No.

27
00:01:37,840 --> 00:01:41,960
 I see your lips are moving.

28
00:01:41,960 --> 00:01:43,960
 No sound is coming up.

29
00:01:43,960 --> 00:01:45,920
 It was for a second.

30
00:01:45,920 --> 00:01:46,920
 I heard something.

31
00:01:46,920 --> 00:01:49,400
 It might be a cable or something.

32
00:01:49,400 --> 00:01:50,400
 How about that?

33
00:01:50,400 --> 00:01:51,400
 Can you hear me now?

34
00:01:51,400 --> 00:01:54,000
 Now I can hear you, but the sound's pretty awful.

35
00:01:54,000 --> 00:01:55,000
 That might be on my end.

36
00:01:55,000 --> 00:01:57,200
 That might be on my end, because that was happening before.

37
00:01:57,200 --> 00:01:58,440
 Just a second.

38
00:01:58,440 --> 00:02:00,600
 That was when you were on Firefox.

39
00:02:00,600 --> 00:02:04,040
 Okay, go again.

40
00:02:04,040 --> 00:02:07,040
 How is it now?

41
00:02:07,040 --> 00:02:08,040
 No.

42
00:02:08,040 --> 00:02:11,920
 Yeah, it's at my end.

43
00:02:11,920 --> 00:02:18,800
 Some problem with Ubuntu.

44
00:02:18,800 --> 00:02:23,050
 Are you able to go on Google at all, on Google Chrome for

45
00:02:23,050 --> 00:02:23,760
 this?

46
00:02:23,760 --> 00:02:27,860
 Because I think last time the problem was when you were on

47
00:02:27,860 --> 00:02:28,840
 Firefox.

48
00:02:28,840 --> 00:02:32,280
 No, I've got a problem.

49
00:02:32,280 --> 00:02:34,440
 It's the same on mumble.

50
00:02:34,440 --> 00:02:46,440
 I get some bug with Ubuntu or something.

51
00:02:46,440 --> 00:02:47,440
 Unplugged.

52
00:02:47,440 --> 00:02:48,440
 Okay.

53
00:02:48,440 --> 00:03:09,440
 I'm going to go on Google Chrome.

54
00:03:09,440 --> 00:03:27,640
 Okay, how about now?

55
00:03:27,640 --> 00:03:28,640
 How is that?

56
00:03:28,640 --> 00:03:29,640
 No, you're still there.

57
00:03:29,640 --> 00:03:30,960
 But that's just at my end.

58
00:03:30,960 --> 00:03:34,120
 I don't think anyone else is bothered by it.

59
00:03:34,120 --> 00:03:39,040
 Just got to deal with it.

60
00:03:39,040 --> 00:04:05,000
 Anyway, enough of this.

61
00:04:05,000 --> 00:04:07,520
 Good evening, as I said.

62
00:04:07,520 --> 00:04:09,000
 Welcome.

63
00:04:09,000 --> 00:04:18,840
 Today we're looking at a quote by the Buddha.

64
00:04:18,840 --> 00:04:22,800
 Actually a conversation between him and the Brahmin.

65
00:04:22,800 --> 00:04:31,240
 The Brahmin says...

66
00:04:31,240 --> 00:04:33,240
 Again with his views.

67
00:04:33,240 --> 00:04:35,240
 Brahmin has his view.

68
00:04:35,240 --> 00:04:40,100
 Again we have this clear distinction between the Buddha and

69
00:04:40,100 --> 00:04:41,360
 others.

70
00:04:41,360 --> 00:04:43,320
 Buddha's not interested in views.

71
00:04:43,320 --> 00:04:46,920
 So again we have someone coming to the Buddha saying, "All

72
00:04:46,920 --> 00:04:50,040
 right, here's what I believe."

73
00:04:50,040 --> 00:04:52,720
 Just have to.

74
00:04:52,720 --> 00:04:56,700
 Keeping his views all bottled up is the thing about views,

75
00:04:56,700 --> 00:04:58,720
 because they're hard to hold

76
00:04:58,720 --> 00:04:59,720
 on to.

77
00:04:59,720 --> 00:05:01,560
 It takes energy and effort.

78
00:05:01,560 --> 00:05:03,920
 So you have to exercise.

79
00:05:03,920 --> 00:05:09,590
 And so because you're constantly assuaged by your own doubt

80
00:05:09,590 --> 00:05:12,400
, because it's a view.

81
00:05:12,400 --> 00:05:16,520
 It's not based on empirical evidence.

82
00:05:16,520 --> 00:05:21,440
 So in order to maintain it, you have to push.

83
00:05:21,440 --> 00:05:24,680
 And the push is in trying to get other people to believe

84
00:05:24,680 --> 00:05:26,280
 the same things as you.

85
00:05:26,280 --> 00:05:28,040
 This is why people proselytize.

86
00:05:28,040 --> 00:05:30,900
 They may say it's for whatever reason, but it's actually to

87
00:05:30,900 --> 00:05:32,500
 reaffirm their own belief,

88
00:05:32,500 --> 00:05:35,880
 because belief is difficult to keep.

89
00:05:35,880 --> 00:05:38,000
 So this Brahmin, he can't keep it.

90
00:05:38,000 --> 00:05:40,960
 He's holding on to this view and he's like, believes it so

91
00:05:40,960 --> 00:05:41,480
 much.

92
00:05:41,480 --> 00:05:44,280
 But in order to maintain that belief, whenever it starts to

93
00:05:44,280 --> 00:05:45,720
 slip, he would have to go and

94
00:05:45,720 --> 00:05:50,480
 tell others.

95
00:05:50,480 --> 00:05:51,560
 That's why he comes to the Buddha.

96
00:05:51,560 --> 00:05:53,360
 That's why people would come to the Buddha.

97
00:05:53,360 --> 00:05:56,910
 They would come to me with this sort of things, all their

98
00:05:56,910 --> 00:05:57,600
 views.

99
00:05:57,600 --> 00:06:00,000
 It's nice to have the example of the Buddha.

100
00:06:00,000 --> 00:06:04,560
 They say, mm-hmm, yeah, well, that's your view.

101
00:06:04,560 --> 00:06:08,480
 There was an article someone posted recently and I was

102
00:06:08,480 --> 00:06:11,080
 discussing it with them about this

103
00:06:11,080 --> 00:06:17,160
 fact, the idea that people think that their view, the idea

104
00:06:17,160 --> 00:06:20,040
 of an opinion, where people

105
00:06:20,040 --> 00:06:24,750
 will come to you and say, well, my opinion, in my opinion,

106
00:06:24,750 --> 00:06:25,240
 X.

107
00:06:25,240 --> 00:06:30,110
 And it would be something like, in my opinion, vaccines

108
00:06:30,110 --> 00:06:33,320
 give you Alzheimer's, something like

109
00:06:33,320 --> 00:06:34,320
 that.

110
00:06:34,320 --> 00:06:37,070
 I mean, I don't know about vaccines giving you Alzheimer's,

111
00:06:37,070 --> 00:06:38,480
 but the thrust of the article

112
00:06:38,480 --> 00:06:41,300
 is that, well, in many cases, it's not even really an

113
00:06:41,300 --> 00:06:42,000
 opinion.

114
00:06:42,000 --> 00:06:45,840
 It's just a mistake and it's an erroneous statement.

115
00:06:45,840 --> 00:06:47,800
 It's just wrong.

116
00:06:47,800 --> 00:06:56,680
 An opinion is I like onions or I like lavender or something

117
00:06:56,680 --> 00:06:56,880
.

118
00:06:56,880 --> 00:07:00,680
 But there's this idea that you could hide behind something

119
00:07:00,680 --> 00:07:03,120
 because it's your opinion.

120
00:07:03,120 --> 00:07:06,640
 And you also have something that's just my opinion, like

121
00:07:06,640 --> 00:07:08,480
 everyone's entitled to their

122
00:07:08,480 --> 00:07:09,480
 opinions.

123
00:07:09,480 --> 00:07:14,340
 I mean, well, yeah, but it doesn't mean anything that you

124
00:07:14,340 --> 00:07:16,000
 have an opinion.

125
00:07:16,000 --> 00:07:21,820
 And so we were talking about whether there's any meaning to

126
00:07:21,820 --> 00:07:24,040
 having an opinion.

127
00:07:24,040 --> 00:07:28,630
 And besides how it affects your own mind, there's really

128
00:07:28,630 --> 00:07:31,040
 nothing, it means nothing when

129
00:07:31,040 --> 00:07:35,240
 you tell me I believe X, it's really meaningless.

130
00:07:35,240 --> 00:07:39,040
 If 90% of humans believed something, it would still be

131
00:07:39,040 --> 00:07:40,080
 meaningless.

132
00:07:40,080 --> 00:07:43,440
 It would still have no bearing on the truth.

133
00:07:43,440 --> 00:07:46,740
 If 99% of humans believed something, it would still have no

134
00:07:46,740 --> 00:07:48,040
 bearing on the truth.

135
00:07:48,040 --> 00:07:52,760
 So belief in and of itself is of no importance.

136
00:07:52,760 --> 00:07:56,380
 Anyway, people seem to think it is important, but it's

137
00:07:56,380 --> 00:07:58,600
 important that they tell you.

138
00:07:58,600 --> 00:08:03,440
 So he came to the Buddha and said, "I, this is my opinion.

139
00:08:03,440 --> 00:08:07,360
 There's no harm in telling people the truth."

140
00:08:07,360 --> 00:08:09,600
 It's an interesting statement.

141
00:08:09,600 --> 00:08:16,420
 It reminds one of the question of whether telling the truth

142
00:08:16,420 --> 00:08:19,720
 means not lying means always telling

143
00:08:19,720 --> 00:08:20,720
 the truth.

144
00:08:20,720 --> 00:08:22,520
 Does not lying always mean telling the truth?

145
00:08:22,520 --> 00:08:26,460
 And from a Buddha's point of view, obviously, especially

146
00:08:26,460 --> 00:08:28,280
 here, the answer is no.

147
00:08:28,280 --> 00:08:32,260
 This isn't what we mean by not telling a lie.

148
00:08:32,260 --> 00:08:34,000
 Not telling a lie is actually lying.

149
00:08:34,000 --> 00:08:37,200
 If you omit the truth and as a result someone has a

150
00:08:37,200 --> 00:08:39,920
 mistaken understanding, that's not the

151
00:08:39,920 --> 00:08:41,440
 same as lying.

152
00:08:41,440 --> 00:08:44,690
 If you do anything to intentionally mislead someone, well,

153
00:08:44,690 --> 00:08:46,400
 that could be considered lying.

154
00:08:46,400 --> 00:08:49,670
 But withholding the truth is not considered lying because

155
00:08:49,670 --> 00:08:51,540
 withholding the truth can often

156
00:08:51,540 --> 00:08:52,600
 be quite beneficial.

157
00:08:52,600 --> 00:08:55,800
 The problem with lying is that it's a distortion.

158
00:08:55,800 --> 00:08:58,480
 It's a perversion of reality.

159
00:08:58,480 --> 00:09:01,640
 What you're doing is you're intentionally taking someone

160
00:09:01,640 --> 00:09:02,960
 away from the truth.

161
00:09:02,960 --> 00:09:07,690
 And so inherently, it's antithetical to the practice of

162
00:09:07,690 --> 00:09:09,520
 Buddhism and attainment of the

163
00:09:09,520 --> 00:09:12,760
 truth.

164
00:09:12,760 --> 00:09:15,520
 But the other thing about this statement that the Buddha

165
00:09:15,520 --> 00:09:17,440
 gives raises, I don't say that.

166
00:09:17,440 --> 00:09:20,880
 I don't hold on to such a view.

167
00:09:20,880 --> 00:09:23,530
 Instead, the Buddha looks at things from a completely

168
00:09:23,530 --> 00:09:25,160
 different point of view and it's

169
00:09:25,160 --> 00:09:29,600
 in one sense utilitarian if it's beneficial.

170
00:09:29,600 --> 00:09:34,550
 If it helps, if saying something helps, then he will say it

171
00:09:34,550 --> 00:09:34,560
.

172
00:09:34,560 --> 00:09:37,000
 If saying something doesn't help, then he won't say it.

173
00:09:37,000 --> 00:09:41,570
 Now the question then comes, well, could you lie if it was

174
00:09:41,570 --> 00:09:43,880
 beneficial or could you kill

175
00:09:43,880 --> 00:09:46,040
 if it was beneficial?

176
00:09:46,040 --> 00:09:48,600
 And the point is that, of course, that's not possible.

177
00:09:48,600 --> 00:09:57,520
 If a lie is problematic, is inherently harmful, then so no.

178
00:09:57,520 --> 00:10:01,290
 Doing something that is harmful is not in the capability,

179
00:10:01,290 --> 00:10:03,080
 actually, of an enlightened

180
00:10:03,080 --> 00:10:09,880
 being, but a capable, intentional harm to themselves or

181
00:10:09,880 --> 00:10:11,200
 others.

182
00:10:11,200 --> 00:10:14,600
 So intentionally telling a lie is not possible.

183
00:10:14,600 --> 00:10:17,570
 Intentionally stealing, intentionally killing,

184
00:10:17,570 --> 00:10:20,280
 intentionally doing something that harms others.

185
00:10:20,280 --> 00:10:29,360
 Intend for the intention to harm more oneself.

186
00:10:29,360 --> 00:10:35,970
 Having the unwholesome mind state that would give rise to

187
00:10:35,970 --> 00:10:40,560
 killing or stealing is not possible.

188
00:10:40,560 --> 00:10:43,910
 But the point here is in the difference in the Buddha's

189
00:10:43,910 --> 00:10:46,080
 approach to reality and in regards

190
00:10:46,080 --> 00:10:52,750
 to not whether it's this way or that way, but whether it's

191
00:10:52,750 --> 00:10:54,200
 helpful.

192
00:10:54,200 --> 00:10:57,900
 So the Buddha was quite malleable and quite flexible in

193
00:10:57,900 --> 00:10:59,800
 regards to a lot of the views

194
00:10:59,800 --> 00:11:03,200
 that arose.

195
00:11:03,200 --> 00:11:06,570
 So as far as telling the truth, it's quite a simple

196
00:11:06,570 --> 00:11:07,520
 teaching.

197
00:11:07,520 --> 00:11:11,090
 I mean, the actual teaching here is just that it's not

198
00:11:11,090 --> 00:11:13,280
 always useful to tell the truth.

199
00:11:13,280 --> 00:11:18,160
 But the premise behind it is more interesting.

200
00:11:18,160 --> 00:11:21,920
 And it's that in regards to views, they're not really of

201
00:11:21,920 --> 00:11:22,920
 any import.

202
00:11:22,920 --> 00:11:23,920
 Opinions are not useful.

203
00:11:23,920 --> 00:11:26,800
 The Buddha doesn't hold on to them, doesn't give them any

204
00:11:26,800 --> 00:11:27,400
 weight.

205
00:11:27,400 --> 00:11:33,160
 What he gave weight to is truth, value, benefit.

206
00:11:33,160 --> 00:11:38,160
 So that's the Dhamma for tonight.

207
00:11:38,160 --> 00:11:46,480
 Does anybody have any questions?

208
00:11:46,480 --> 00:11:49,080
 What if somebody can handle the truth?

209
00:11:49,080 --> 00:12:07,760
 I don't understand.

210
00:12:07,760 --> 00:12:08,760
 Such as the non-existence.

211
00:12:08,760 --> 00:12:12,870
 You mean cannot handle the truth, is that what you're

212
00:12:12,870 --> 00:12:13,760
 asking?

213
00:12:13,760 --> 00:12:19,160
 I think you've got some grammar issues.

214
00:12:19,160 --> 00:12:22,190
 If someone can't handle the truth, like they believe that

215
00:12:22,190 --> 00:12:23,640
 God exists, so you shouldn't

216
00:12:23,640 --> 00:12:30,560
 tell them that God doesn't exist.

217
00:12:30,560 --> 00:12:33,320
 I mean, this is another you don't have to.

218
00:12:33,320 --> 00:12:35,520
 It's important to always be right.

219
00:12:35,520 --> 00:12:36,520
 It's not always important.

220
00:12:36,520 --> 00:12:43,170
 It's not always wise to try to convince other people that

221
00:12:43,170 --> 00:12:45,240
 you're right.

222
00:12:45,240 --> 00:12:46,920
 There's a big difference.

223
00:12:46,920 --> 00:12:55,080
 It's not our duty to fix other people.

224
00:12:55,080 --> 00:12:56,080
 It's not our role.

225
00:12:56,080 --> 00:13:00,880
 Our goal doesn't help to try.

226
00:13:00,880 --> 00:13:05,520
 It's our role and our duty to be clear in the mind.

227
00:13:05,520 --> 00:13:09,440
 To be mindful and to be beneficial.

228
00:13:09,440 --> 00:13:22,720
 To be helpful, to be kind, to be pure.

229
00:13:22,720 --> 00:13:24,920
 So we have something that we wanted to talk about.

230
00:13:24,920 --> 00:13:28,740
 And if Robin's amenable, maybe I could ask her to say

231
00:13:28,740 --> 00:13:31,240
 something about our project.

232
00:13:31,240 --> 00:13:32,240
 Sure.

233
00:13:32,240 --> 00:13:34,320
 Can you still hear me?

234
00:13:34,320 --> 00:13:35,320
 Yes, I can.

235
00:13:35,320 --> 00:13:36,720
 And about tomorrow.

236
00:13:36,720 --> 00:13:39,370
 So if there's anybody who wants to join us tomorrow, they

237
00:13:39,370 --> 00:13:39,760
 can.

238
00:13:39,760 --> 00:13:41,800
 We're at seven o'clock.

239
00:13:41,800 --> 00:13:42,800
 Just let them know.

240
00:13:42,800 --> 00:13:47,760
 They can contact and I'm allowed them to the hang up.

241
00:13:47,760 --> 00:13:48,960
 Sure.

242
00:13:48,960 --> 00:13:52,450
 So what we're meeting on tomorrow at seven for anyone who's

243
00:13:52,450 --> 00:13:54,280
 interested is we're putting

244
00:13:54,280 --> 00:13:57,080
 together an online campaign.

245
00:13:57,080 --> 00:14:01,720
 Bonté is interested in establishing a monastery up in

246
00:14:01,720 --> 00:14:05,280
 Canada in the area of McMaster University.

247
00:14:05,280 --> 00:14:08,960
 Which is a really exciting project.

248
00:14:08,960 --> 00:14:12,670
 It's going to be great for the students at McMaster to have

249
00:14:12,670 --> 00:14:14,840
 a Buddhist monastery, a place

250
00:14:14,840 --> 00:14:16,920
 to go and learn meditation.

251
00:14:16,920 --> 00:14:20,020
 But it's also going to be a benefit to people who may never

252
00:14:20,020 --> 00:14:21,720
 make it to Canada or although

253
00:14:21,720 --> 00:14:22,720
 it's not that far.

254
00:14:22,720 --> 00:14:27,160
 But even for people that are far away, it's still a really

255
00:14:27,160 --> 00:14:28,800
 interesting concept.

256
00:14:28,800 --> 00:14:32,150
 What Bonté does now with broadcasting things that are

257
00:14:32,150 --> 00:14:34,400
 going on on the internet, it's just

258
00:14:34,400 --> 00:14:39,310
 a wonderful idea of having local meditation courses and

259
00:14:39,310 --> 00:14:42,600
 local daily dhamma and broadcasting

260
00:14:42,600 --> 00:14:44,000
 it as well to us.

261
00:14:44,000 --> 00:14:46,820
 So it's really, really exciting and definitely want to

262
00:14:46,820 --> 00:14:48,000
 support him in that.

263
00:14:48,000 --> 00:14:50,560
 So we have a campaign like the type that you see.

264
00:14:50,560 --> 00:14:53,300
 It's through a website called YouCaring.

265
00:14:53,300 --> 00:14:56,320
 And we should have it ready tomorrow to roll out.

266
00:14:56,320 --> 00:14:59,330
 And hopefully people will be interested in supporting it

267
00:14:59,330 --> 00:15:01,120
 and sharing it with other people

268
00:15:01,120 --> 00:15:03,280
 who might be interested in supporting it.

269
00:15:03,280 --> 00:15:05,040
 Awesome, thank you.

270
00:15:05,040 --> 00:15:07,540
 Yeah, I mean, part of it is having a support group here to

271
00:15:07,540 --> 00:15:08,880
 do these sorts of things.

272
00:15:08,880 --> 00:15:11,080
 I mean, right now I'm doing everything on my own.

273
00:15:11,080 --> 00:15:15,080
 If you see, I just recorded the video for the project here.

274
00:15:15,080 --> 00:15:16,560
 There's the camera.

275
00:15:16,560 --> 00:15:20,880
 And then over there, there's a place to...

276
00:15:20,880 --> 00:15:23,760
 Anyway, I'm not going to show you my room.

277
00:15:23,760 --> 00:15:25,800
 It's all missing.

278
00:15:25,800 --> 00:15:30,000
 There's a station where that's currently or just finished

279
00:15:30,000 --> 00:15:31,960
 transcoding the video.

280
00:15:31,960 --> 00:15:33,440
 So it goes from this station to this...

281
00:15:33,440 --> 00:15:37,600
 But I'm doing it all here in this small, small room.

282
00:15:37,600 --> 00:15:42,120
 And so the idea is that that probably shouldn't change.

283
00:15:42,120 --> 00:15:48,310
 Yeah, so we're going to be starting up a place, but we need

284
00:15:48,310 --> 00:15:52,240
 help and organizing and so, yeah,

285
00:15:52,240 --> 00:15:54,420
 support in making it happen.

286
00:15:54,420 --> 00:15:57,200
 So tomorrow we're going to have a meeting.

287
00:15:57,200 --> 00:16:01,830
 And if anyone would like to join the meeting, it's going to

288
00:16:01,830 --> 00:16:02,920
 be online.

289
00:16:02,920 --> 00:16:05,640
 Just let us know and we'll add you to the Hangout at 7 p.m.

290
00:16:05,640 --> 00:16:08,040
 Eastern.

291
00:16:08,040 --> 00:16:11,200
 And other than that, just wait for the YouTube video to

292
00:16:11,200 --> 00:16:11,880
 come out.

293
00:16:11,880 --> 00:16:12,880
 I should have...

294
00:16:12,880 --> 00:16:13,880
 I mean, it's ready now.

295
00:16:13,880 --> 00:16:16,400
 I just have to upload it.

296
00:16:16,400 --> 00:16:18,800
 And then there's going to be links and everything there.

297
00:16:18,800 --> 00:16:24,360
 So I don't have to get involved.

298
00:16:24,360 --> 00:16:28,550
 Actually, maybe we can kind of premiere it here with this

299
00:16:28,550 --> 00:16:31,080
 group tomorrow before...

300
00:16:31,080 --> 00:16:35,000
 Or at what point will it go live on YouTube?

301
00:16:35,000 --> 00:16:36,000
 Tonight.

302
00:16:36,000 --> 00:16:37,000
 Yeah.

303
00:16:37,000 --> 00:16:39,800
 I mean, I can leave it private tonight, right?

304
00:16:39,800 --> 00:16:43,360
 Yeah, maybe premiere it here tomorrow with this group here.

305
00:16:43,360 --> 00:16:50,360
 That'd be nice.

306
00:16:50,360 --> 00:17:02,200
 Anyway, back to some questions.

307
00:17:02,200 --> 00:17:05,420
 When a viewer opinion arises, I find myself verbalizing in

308
00:17:05,420 --> 00:17:07,080
 the head instead, along with

309
00:17:07,080 --> 00:17:08,080
 my noting.

310
00:17:08,080 --> 00:17:09,640
 Is there a way to not do so?

311
00:17:09,640 --> 00:17:11,840
 But you can't control, you see.

312
00:17:11,840 --> 00:17:15,260
 This is the thing that you're learning, is that we're not

313
00:17:15,260 --> 00:17:16,720
 in control exactly.

314
00:17:16,720 --> 00:17:20,600
 We can change, we can steer ourselves in a different

315
00:17:20,600 --> 00:17:23,280
 direction, but it's got to be moment

316
00:17:23,280 --> 00:17:27,160
 to moment.

317
00:17:27,160 --> 00:17:29,980
 And so for something to just not happen, it's like turning

318
00:17:29,980 --> 00:17:30,680
 a switch.

319
00:17:30,680 --> 00:17:33,080
 It doesn't work that way.

320
00:17:33,080 --> 00:17:38,900
 Right now, you're heading in a certain bent or a certain

321
00:17:38,900 --> 00:17:40,800
 direction that includes that

322
00:17:40,800 --> 00:17:47,760
 sort of verbal, verbalization, mentalization.

323
00:17:47,760 --> 00:17:50,230
 You can steer away from that if you start to cultivate

324
00:17:50,230 --> 00:17:51,280
 different habits.

325
00:17:51,280 --> 00:17:55,570
 You have to identify which habits or which activities are

326
00:17:55,570 --> 00:17:57,400
 promoting the habit.

327
00:17:57,400 --> 00:18:01,270
 And you start to change as you see that those are unbenef

328
00:18:01,270 --> 00:18:03,480
icial and they're leading to harmful

329
00:18:03,480 --> 00:18:08,560
 habits, et cetera, et cetera.

330
00:18:08,560 --> 00:18:17,770
 So when their father wants their children to be Catholic

331
00:18:17,770 --> 00:18:22,480
 and they don't want...

332
00:18:22,480 --> 00:18:25,890
 Yeah, I mean, sometimes you might just go along with

333
00:18:25,890 --> 00:18:27,400
 something, right?

334
00:18:27,400 --> 00:18:30,230
 But you don't go along to it to the extent that it means

335
00:18:30,230 --> 00:18:31,200
 you're lying.

336
00:18:31,200 --> 00:18:34,920
 But you just might not say, "Father, I'm not a Catholic."

337
00:18:34,920 --> 00:18:37,200
 You don't have to tell him that you're not a Catholic.

338
00:18:37,200 --> 00:18:40,240
 It doesn't really help him, not likely.

339
00:18:40,240 --> 00:18:43,520
 I mean, if you think it could, this is the thing the Buddha

340
00:18:43,520 --> 00:18:45,360
 said, "If it's beneficial,

341
00:18:45,360 --> 00:18:48,510
 the problem is it's hard for us to tell what is truly

342
00:18:48,510 --> 00:18:49,640
 beneficial."

343
00:18:49,640 --> 00:18:52,320
 So that's where the uncertainty comes in.

344
00:18:52,320 --> 00:18:54,930
 There are no certain answers, but there are certain answers

345
00:18:54,930 --> 00:18:55,160
.

346
00:18:55,160 --> 00:18:57,360
 We just don't have them.

347
00:18:57,360 --> 00:19:02,080
 So it's more about us trying in our muddled way to discern

348
00:19:02,080 --> 00:19:04,240
 what is right.

349
00:19:04,240 --> 00:19:05,240
 It's not just, "Yes, always."

350
00:19:05,240 --> 00:19:10,330
 It would be much easier if the answer was, "Always tell

351
00:19:10,330 --> 00:19:12,760
 people the truth," when the opportunity

352
00:19:12,760 --> 00:19:13,760
 arises.

353
00:19:13,760 --> 00:19:21,360
 But clearly, that's not the right way.

354
00:19:21,360 --> 00:19:24,880
 So normal that labeling things seems more distracting than

355
00:19:24,880 --> 00:19:26,640
 just noting them, noticing

356
00:19:26,640 --> 00:19:28,800
 them but not picking a word.

357
00:19:28,800 --> 00:19:32,810
 Yeah, it's not distracting, but it's disturbing, and it's

358
00:19:32,810 --> 00:19:33,900
 meant to be.

359
00:19:33,900 --> 00:19:39,420
 It's meant to disturb your habitual stream, their habitual

360
00:19:39,420 --> 00:19:40,520
 reactions.

361
00:19:40,520 --> 00:19:44,850
 So we're comfortable in the way that we deal with things,

362
00:19:44,850 --> 00:19:47,720
 but that being comfortable involves

363
00:19:47,720 --> 00:19:51,200
 defilements, and so we're trying to disrupt it.

364
00:19:51,200 --> 00:19:55,470
 The other thing is it's showing you how chaotic things

365
00:19:55,470 --> 00:19:56,260
 really are.

366
00:19:56,260 --> 00:20:01,060
 So we have a way of surfing over the waves, the

367
00:20:01,060 --> 00:20:06,600
 disturbances, just superficial investigation,

368
00:20:06,600 --> 00:20:10,000
 keeping it superficial enough so that it doesn't bother us.

369
00:20:10,000 --> 00:20:14,360
 This meditation is bringing you right in there, and that's

370
00:20:14,360 --> 00:20:15,200
 not easy.

371
00:20:15,200 --> 00:20:18,330
 So as a result, you're seeing how difficult it is and how

372
00:20:18,330 --> 00:20:19,400
 chaotic it is.

373
00:20:19,400 --> 00:20:25,640
 The third is that it's just a part of the beginner stages

374
00:20:25,640 --> 00:20:27,880
 in the practice.

375
00:20:27,880 --> 00:20:32,580
 For an advanced meditator, they're much better able to have

376
00:20:32,580 --> 00:20:35,820
 a greater than superficial involvement

377
00:20:35,820 --> 00:20:37,580
 with reality.

378
00:20:37,580 --> 00:20:40,900
 And so as a result, it's not so disturbing.

379
00:20:40,900 --> 00:20:47,000
 But in the beginning, you're doing it a lot wrong, it's

380
00:20:47,000 --> 00:20:49,640
 clumsy, it's unskillful, and so

381
00:20:49,640 --> 00:20:55,200
 as a result, it's going to be chaotic.

382
00:20:55,200 --> 00:20:56,960
 But no, it's not actually distracting.

383
00:20:56,960 --> 00:21:00,010
 That may be how it appears, because you have this idea that

384
00:21:00,010 --> 00:21:01,840
 it's a better meditation somehow

385
00:21:01,840 --> 00:21:06,400
 to just go with it, but it's actually not disturbing.

386
00:21:06,400 --> 00:21:09,640
 It's not distracting, it's just disturbing.

387
00:21:09,640 --> 00:21:11,120
 And it's disturbing for these reasons.

388
00:21:11,120 --> 00:21:13,400
 First of all, reality is kind of disturbing.

389
00:21:13,400 --> 00:21:17,290
 Second of all, you're not in the beginning very good at

390
00:21:17,290 --> 00:21:19,160
 approaching reality.

391
00:21:19,160 --> 00:21:22,140
 And third, it's a part of the actual practice to be

392
00:21:22,140 --> 00:21:24,560
 disturbed, because the superficial

393
00:21:24,560 --> 00:21:31,540
 ability to surf over the experiences belies the fact that

394
00:21:31,540 --> 00:21:34,520
 it's actually not a way to find

395
00:21:34,520 --> 00:21:37,360
 happiness, that this is not sustainable.

396
00:21:37,360 --> 00:21:42,040
 It's not conducive to true and lasting happiness.

397
00:21:42,040 --> 00:21:46,030
 In truth, once you see reality clearly, you reject it

398
00:21:46,030 --> 00:21:48,880
 entirely, and you leave it behind.

399
00:21:48,880 --> 00:21:53,900
 And in favor of freedom, leave behind everything step by

400
00:21:53,900 --> 00:21:55,760
 step in your life.

401
00:21:55,760 --> 00:21:58,990
 You start letting go of the more complicated aspects of

402
00:21:58,990 --> 00:22:01,760
 your life, simplifying, until eventually

403
00:22:01,760 --> 00:22:06,870
 you just let go of everything and have no interest in being

404
00:22:06,870 --> 00:22:08,640
 involved at all.

405
00:22:08,640 --> 00:22:13,280
 Right, okay, so some questions about this project that we

406
00:22:13,280 --> 00:22:15,720
 kind of vaguely mentioned.

407
00:22:15,720 --> 00:22:18,760
 I mean, I have to be a little vague, because I can't say,

408
00:22:18,760 --> 00:22:20,080
 but we need support.

409
00:22:20,080 --> 00:22:22,320
 To make this happen, we need people to help.

410
00:22:22,320 --> 00:22:25,030
 So if you want to help organize, there's a meeting, but we

411
00:22:25,030 --> 00:22:26,440
're going to set up something

412
00:22:26,440 --> 00:22:28,440
 on a crowdsourcing site.

413
00:22:28,440 --> 00:22:29,440
 Yes.

414
00:22:29,440 --> 00:22:31,240
 So, Robin, you want to explain?

415
00:22:31,240 --> 00:22:32,240
 Sure.

416
00:22:32,240 --> 00:22:36,680
 It's a nice site called YouCaring, and there's a ton of

417
00:22:36,680 --> 00:22:39,720
 crowdsourcing funding sites, but

418
00:22:39,720 --> 00:22:42,790
 we were able to find one that doesn't charge a percentage

419
00:22:42,790 --> 00:22:44,880
 other than the PayPal percentage,

420
00:22:44,880 --> 00:22:46,760
 which everyone has to charge.

421
00:22:46,760 --> 00:22:48,200
 That's a PayPal policy.

422
00:22:48,200 --> 00:22:52,700
 So in other words, Bonta's organization will receive the

423
00:22:52,700 --> 00:22:55,080
 maximum amount of donations that

424
00:22:55,080 --> 00:22:57,160
 are given to support this project.

425
00:22:57,160 --> 00:23:01,910
 So we have the amount that we are hoping to raise, which is

426
00:23:01,910 --> 00:23:05,160
 initially $6,000 to pay for

427
00:23:05,160 --> 00:23:08,470
 some very basic expenses, the first and last month's rent

428
00:23:08,470 --> 00:23:10,280
 and utilities, and just real

429
00:23:10,280 --> 00:23:12,120
 basic things like that.

430
00:23:12,120 --> 00:23:15,860
 So that will be available tomorrow, and it will be on

431
00:23:15,860 --> 00:23:18,120
 YouTube, and the basic kind of

432
00:23:18,120 --> 00:23:21,100
 thing that you can share on your Facebook if you so choose,

433
00:23:21,100 --> 00:23:22,480
 or if you know particular

434
00:23:22,480 --> 00:23:25,420
 people that you know might be interested, please share it

435
00:23:25,420 --> 00:23:26,120
 with them.

436
00:23:26,120 --> 00:23:29,150
 They can be shared by email, many different methods of just

437
00:23:29,150 --> 00:23:30,360
 getting the word out, and

438
00:23:30,360 --> 00:23:33,040
 it would be a great way to support.

439
00:23:33,040 --> 00:23:36,070
 And as far as the 7 o'clock meeting tomorrow, Bonta, a

440
00:23:36,070 --> 00:23:38,040
 couple of people were asking where

441
00:23:38,040 --> 00:23:39,040
 to go.

442
00:23:39,040 --> 00:23:40,200
 Now, that's actually on Google Hangouts, right?

443
00:23:40,200 --> 00:23:41,200
 Yeah.

444
00:23:41,200 --> 00:23:42,200
 You have to get an invite.

445
00:23:42,200 --> 00:23:47,130
 So you have to just send us an email or a message, and send

446
00:23:47,130 --> 00:23:49,280
 one of us a message, and

447
00:23:49,280 --> 00:23:50,280
 add new.

448
00:23:50,280 --> 00:23:51,360
 But that's an organizational meeting.

449
00:23:51,360 --> 00:23:54,480
 It's only for if you want to be involved in organizing.

450
00:23:54,480 --> 00:23:59,230
 And we don't need, I suppose, we're going to need a few

451
00:23:59,230 --> 00:24:01,160
 people involved in spreading

452
00:24:01,160 --> 00:24:07,030
 the word, and coming, brainstorming, and working out

453
00:24:07,030 --> 00:24:09,680
 logistics, and so on.

454
00:24:09,680 --> 00:24:11,320
 We have to find a property.

455
00:24:11,320 --> 00:24:13,900
 We've been looking at rental properties, and we have to

456
00:24:13,900 --> 00:24:14,960
 find the right one.

457
00:24:14,960 --> 00:24:16,560
 We have to go take a look at it.

458
00:24:16,560 --> 00:24:20,410
 So hopefully when I go to McMaster, we're going to find a

459
00:24:20,410 --> 00:24:22,560
 community there who some people

460
00:24:22,560 --> 00:24:25,880
 might be able to help out with these sorts of things.

461
00:24:25,880 --> 00:24:28,460
 I just put my email in the chat window if anyone is

462
00:24:28,460 --> 00:24:30,440
 interested in helping out and being

463
00:24:30,440 --> 00:24:31,440
 a volunteer.

464
00:24:31,440 --> 00:24:34,760
 Or even just showing up just to kind of give some feedback.

465
00:24:34,760 --> 00:24:37,650
 We'll be looking at the video and everything for the first

466
00:24:37,650 --> 00:24:39,360
 time together, and just making

467
00:24:39,360 --> 00:24:40,680
 sure we didn't overlook anything.

468
00:24:40,680 --> 00:24:44,270
 So if anyone's interested in attending or helping out, just

469
00:24:44,270 --> 00:24:45,880
 send me an email, and I'll

470
00:24:45,880 --> 00:24:48,960
 make sure Bonta has that information.

471
00:24:48,960 --> 00:24:53,560
 Okay, another question.

472
00:24:53,560 --> 00:24:55,890
 Is Buddhism a religion that focuses on worship towards

473
00:24:55,890 --> 00:24:57,680
 Buddha rather than your own well-being?

474
00:24:57,680 --> 00:25:02,400
 I would say Jacob, no.

475
00:25:02,400 --> 00:25:04,840
 Simon says Jacob, no.

476
00:25:04,840 --> 00:25:05,840
 Absolutely.

477
00:25:05,840 --> 00:25:07,960
 And pretty much the opposite, right?

478
00:25:07,960 --> 00:25:11,020
 It's much more about your own well-being and very little

479
00:25:11,020 --> 00:25:12,760
 about any sort of worship of any

480
00:25:12,760 --> 00:25:13,760
 kind.

481
00:25:13,760 --> 00:25:17,150
 So we do kind of worship the Buddha in the sense that we

482
00:25:17,150 --> 00:25:19,160
 think he is someone worthy of

483
00:25:19,160 --> 00:25:21,800
 our reverence.

484
00:25:21,800 --> 00:25:24,680
 But it's more like reverence than worship.

485
00:25:24,680 --> 00:25:30,440
 The common word worship, I think, has become too much of

486
00:25:30,440 --> 00:25:33,880
 being devoted towards the simple

487
00:25:33,880 --> 00:25:37,360
 ritual of as a god or something.

488
00:25:37,360 --> 00:25:42,760
 The Buddha wasn't certainly a god.

489
00:25:42,760 --> 00:25:46,760
 What would be your specific qualifications for ordination?

490
00:25:46,760 --> 00:25:50,160
 Rule number one, don't ask me about ordination.

491
00:25:50,160 --> 00:25:53,840
 Qualification number one, stop asking about ordination.

492
00:25:53,840 --> 00:25:58,470
 And that's not really chtung-chtung and cheek because, I

493
00:25:58,470 --> 00:26:00,680
 mean it is, but it isn't because

494
00:26:00,680 --> 00:26:04,520
 anyone who comes to me looking to ordain at this point, it

495
00:26:04,520 --> 00:26:05,960
's just a red flag.

496
00:26:05,960 --> 00:26:08,840
 I'm sorry, that sounds probably pretty bad.

497
00:26:08,840 --> 00:26:11,990
 And it's probably in your case not true, which is why I

498
00:26:11,990 --> 00:26:13,840
 feel comfortable saying it.

499
00:26:13,840 --> 00:26:16,680
 Because focus on meditation.

500
00:26:16,680 --> 00:26:18,840
 And I'm not chastising you or anything.

501
00:26:18,840 --> 00:26:23,920
 I'm saying you want it to happen, you're that serious?

502
00:26:23,920 --> 00:26:27,470
 Then it has to be, I'm so serious about meditation, I want

503
00:26:27,470 --> 00:26:29,600
 to put on a rectangle of cloth and

504
00:26:29,600 --> 00:26:30,600
 keep doing it.

505
00:26:30,600 --> 00:26:31,600
 It has to be that.

506
00:26:31,600 --> 00:26:34,890
 And it can't be, but I want to become a monk because I

507
00:26:34,890 --> 00:26:37,840
 think it's cool to wear a toga.

508
00:26:37,840 --> 00:26:38,840
 Right?

509
00:26:38,840 --> 00:26:46,340
 So, think of it like that 80s kung fu movie that I always

510
00:26:46,340 --> 00:26:50,360
 reference, where he sat in the

511
00:26:50,360 --> 00:26:54,400
 courtyard and they wouldn't let him in.

512
00:26:54,400 --> 00:26:56,280
 They said no, no, a fight club as well.

513
00:26:56,280 --> 00:27:00,660
 One of the last movies I saw before I became a monk, a

514
00:27:00,660 --> 00:27:03,760
 fight club where they make you stand

515
00:27:03,760 --> 00:27:04,760
 outside.

516
00:27:04,760 --> 00:27:08,580
 But there was an earlier movie, this Shaolin movie, where

517
00:27:08,580 --> 00:27:10,600
 they made him, he sat outside

518
00:27:10,600 --> 00:27:13,050
 and they said no, no, we don't accept foreigners and they

519
00:27:13,050 --> 00:27:14,480
 just ignored him and sat in front

520
00:27:14,480 --> 00:27:17,600
 of the monastery until they let him in.

521
00:27:17,600 --> 00:27:22,500
 And of course he ended up causing a ruckus and a big upset

522
00:27:22,500 --> 00:27:24,440
 in the monastery.

523
00:27:24,440 --> 00:27:27,400
 So yeah, there's that.

524
00:27:27,400 --> 00:27:31,600
 But that's how you have to approach it.

525
00:27:31,600 --> 00:27:34,630
 You come and do a meditation course and then we'll talk

526
00:27:34,630 --> 00:27:36,600
 about whether you can do a second

527
00:27:36,600 --> 00:27:37,600
 meditation course.

528
00:27:37,600 --> 00:27:41,200
 And if you've done that, then we can talk about, well,

529
00:27:41,200 --> 00:27:44,000
 maybe a third meditation course.

530
00:27:44,000 --> 00:27:49,480
 We kind of get the point, get the picture because if we let

531
00:27:49,480 --> 00:27:51,840
 you stay, then you can stay.

532
00:27:51,840 --> 00:28:20,600
 So, thank you.

533
00:28:20,600 --> 00:28:37,240
 I think you've answered all the questions, Bante.

534
00:28:37,240 --> 00:28:39,240
 Yeah, well, good enough.

535
00:28:39,240 --> 00:28:40,240
 Amazing.

536
00:28:40,240 --> 00:28:41,240
 It's good.

537
00:28:41,240 --> 00:28:42,840
 We do every day like this.

538
00:28:42,840 --> 00:28:45,000
 The answers don't pile up.

539
00:28:45,000 --> 00:28:46,000
 That's what I was thinking.

540
00:28:46,000 --> 00:28:47,000
 Yeah.

541
00:28:47,000 --> 00:28:50,560
 I think 200 questions piled up on Google Moderator.

542
00:28:50,560 --> 00:28:52,040
 So this is nice.

543
00:28:52,040 --> 00:28:53,040
 All caught up.

544
00:28:53,040 --> 00:28:54,040
 Awesome.

545
00:28:54,040 --> 00:28:57,840
 Thank you, Robin, for being there to help.

546
00:28:57,840 --> 00:28:58,840
 Oh, thank you.

547
00:28:58,840 --> 00:28:59,840
 All right.

548
00:28:59,840 --> 00:29:01,960
 Let's call it a night.

549
00:29:01,960 --> 00:29:04,360
 See you all tomorrow.

550
00:29:04,360 --> 00:29:05,360
 Thank you, Bante.

551
00:29:05,360 --> 00:29:05,760
 Be well.

552
00:29:05,760 --> 00:29:06,760
 Thank you.

553
00:29:06,760 --> 00:29:06,760
 Thank you.

554
00:29:06,760 --> 00:29:07,760
 Thank you.

555
00:29:07,760 --> 00:29:07,760
 Thank you.

556
00:29:07,760 --> 00:29:08,760
 Thank you.

557
00:29:08,760 --> 00:29:09,760
 Thank you.

