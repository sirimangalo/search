1
00:00:00,000 --> 00:00:26,000
 [birds chirping]

2
00:00:26,000 --> 00:00:34,000
 Good evening everyone and welcome back to evening Dhamma.

3
00:00:34,000 --> 00:00:41,410
 And tonight I'd like to look at, oh you know, it's one

4
00:00:41,410 --> 00:00:42,000
 second.

5
00:00:42,000 --> 00:00:51,000
 So many switches.

6
00:00:51,000 --> 00:00:53,000
 Hello?

7
00:00:53,000 --> 00:00:56,000
 Can you hear that?

8
00:00:56,000 --> 00:00:58,000
 Is it loud enough?

9
00:00:58,000 --> 00:01:02,000
 Hello? That's better.

10
00:01:02,000 --> 00:01:04,000
 Too loud?

11
00:01:04,000 --> 00:01:05,000
 That was excellent.

12
00:01:05,000 --> 00:01:08,000
 That's good.

13
00:01:08,000 --> 00:01:13,000
 So today we'll be looking at the Vaya Bhairava Sutta.

14
00:01:21,000 --> 00:01:24,290
 If you want to follow along, it's Majjhima Nikaya Sutta

15
00:01:24,290 --> 00:01:30,000
 number four.

16
00:01:30,000 --> 00:01:41,000
 In this talk the Buddha is approached by Brahman Janusoni

17
00:01:41,000 --> 00:01:42,000
 who,

18
00:01:42,000 --> 00:01:46,470
 who basically asks whether the followers of the Buddha take

19
00:01:46,470 --> 00:01:49,000
 after his example.

20
00:01:49,000 --> 00:01:53,000
 Meaning, do they practice as you practice?

21
00:01:53,000 --> 00:02:11,530
 Which is, basically the idea is that the students are

22
00:02:11,530 --> 00:02:15,000
 capable of the same sort of practice as the Buddha.

23
00:02:15,000 --> 00:02:20,450
 Of course, in that time, or in religion in general, it can

24
00:02:20,450 --> 00:02:25,000
 happen that the headmaster,

25
00:02:25,000 --> 00:02:30,560
 the guru is capable of practices that the students are not

26
00:02:30,560 --> 00:02:32,000
 capable of.

27
00:02:40,000 --> 00:02:43,220
 Do your students actually practice the things that you

28
00:02:43,220 --> 00:02:44,000
 practice?

29
00:02:44,000 --> 00:02:46,000
 That's basically what he's acting.

30
00:02:46,000 --> 00:02:49,000
 Asking, do they follow your advice?

31
00:02:49,000 --> 00:02:51,000
 Do they do what you do?

32
00:02:51,000 --> 00:02:54,000
 Are they able to keep your teachings?

33
00:02:54,000 --> 00:02:56,000
 And the Buddha affirms all this.

34
00:02:56,000 --> 00:02:59,000
 He says, yes indeed, my students do.

35
00:02:59,000 --> 00:03:00,000
 Look to me for guidance.

36
00:03:00,000 --> 00:03:07,000
 They take my advice and they follow my example.

37
00:03:07,000 --> 00:03:10,790
 Which in and of itself is quite impressive because the

38
00:03:10,790 --> 00:03:15,000
 Buddha's practice of course was quite rigorous.

39
00:03:15,000 --> 00:03:19,880
 The Buddha himself only slept, only lay down for three or

40
00:03:19,880 --> 00:03:22,000
 four hours a night.

41
00:03:22,000 --> 00:03:26,170
 And the rest of the time was mostly actually spent in

42
00:03:26,170 --> 00:03:27,000
 teaching.

43
00:03:27,000 --> 00:03:34,660
 But they say even those three or four hours when he lay

44
00:03:34,660 --> 00:03:39,000
 down were not actually sleeping.

45
00:03:39,000 --> 00:03:41,000
 He would actually teach them as well.

46
00:03:45,000 --> 00:03:54,040
 But the talk here is about specifically the practice of

47
00:03:54,040 --> 00:03:58,000
 living in the jungle.

48
00:03:58,000 --> 00:04:07,320
 So Janasoni says, wow, I would think that it would be hard

49
00:04:07,320 --> 00:04:10,000
 for the monks to live in the jungle.

50
00:04:10,000 --> 00:04:14,490
 I would think that if someone were to go off and live in

51
00:04:14,490 --> 00:04:16,000
 the jungle alone,

52
00:04:16,000 --> 00:04:22,000
 if they weren't focused, if they didn't have a strong mind,

53
00:04:22,000 --> 00:04:25,000
 they'd go crazy.

54
00:04:25,000 --> 00:04:39,000
 Let's see what the Pali is for go crazy.

55
00:04:39,000 --> 00:04:46,000
 Haranti mange mano naani samadhi alabhamana sabhikuno.

56
00:04:46,000 --> 00:04:50,000
 Haranti mano.

57
00:04:50,000 --> 00:04:58,000
 Haranti means take away mano, the mind.

58
00:04:58,000 --> 00:05:01,000
 No, I don't get it.

59
00:05:01,000 --> 00:05:06,000
 Haranti.

60
00:05:06,000 --> 00:05:09,000
 I'll get back to that one.

61
00:05:09,000 --> 00:05:20,000
 Your mind will be, you'll lose your mind basically.

62
00:05:20,000 --> 00:05:21,000
 And the Buddha affirms this.

63
00:05:21,000 --> 00:05:25,000
 He says, yes, it's hard to endure living in the forest.

64
00:05:25,000 --> 00:05:28,000
 It's hard to live in seclusion.

65
00:05:28,000 --> 00:05:33,000
 It's hard to enjoy solitude, one would think.

66
00:05:33,000 --> 00:05:35,000
 Living in the jungle would drive you crazy.

67
00:05:35,000 --> 00:05:38,000
 If you have no concentration.

68
00:05:38,000 --> 00:05:45,000
 If your mind is not focused.

69
00:05:45,000 --> 00:05:48,150
 And then he goes through, why I brought this sutta hap is

70
00:05:48,150 --> 00:05:53,000
 because he goes through a bunch of qualities that are,

71
00:05:53,000 --> 00:05:57,000
 we can say they're required for practice.

72
00:05:57,000 --> 00:06:00,200
 And so it well answers the questions of the question of

73
00:06:00,200 --> 00:06:04,000
 what sort of qualities we need to develop.

74
00:06:04,000 --> 00:06:08,920
 And the sort of qualities that differentiate a person who's

75
00:06:08,920 --> 00:06:12,000
 capable of living in seclusion.

76
00:06:12,000 --> 00:06:14,000
 And one who is not.

77
00:06:14,000 --> 00:06:16,000
 What's it going to take?

78
00:06:16,000 --> 00:06:17,000
 Useful for us.

79
00:06:17,000 --> 00:06:20,320
 What's it going to take for us to be able to successfully

80
00:06:20,320 --> 00:06:25,000
 participate and complete a meditation course?

81
00:06:25,000 --> 00:06:30,790
 What's it going to take for us to be able to stay alone

82
00:06:30,790 --> 00:06:33,000
 with ourselves?

83
00:06:33,000 --> 00:06:38,620
 And to do the work that is required to understand ourselves

84
00:06:38,620 --> 00:06:39,000
.

85
00:06:39,000 --> 00:06:46,000
 So this is what this sutta is about.

86
00:06:46,000 --> 00:06:48,000
 The Buddha uses himself as an example.

87
00:06:48,000 --> 00:06:53,000
 He says, when I was unenlightened, I thought about that.

88
00:06:53,000 --> 00:06:55,000
 I thought about how hard it would be.

89
00:06:55,000 --> 00:06:59,000
 And then I thought about the qualities that I would need.

90
00:06:59,000 --> 00:07:06,320
 And so the first three qualities you need are purity of

91
00:07:06,320 --> 00:07:08,000
 conduct.

92
00:07:08,000 --> 00:07:14,000
 Physical conduct, verbal conduct, mental conduct.

93
00:07:14,000 --> 00:07:19,270
 If you're a person who is full of corruption in body,

94
00:07:19,270 --> 00:07:21,000
 speech, or mind,

95
00:07:21,000 --> 00:07:24,000
 then it's not easy to stay alone with yourself.

96
00:07:24,000 --> 00:07:28,030
 You're going to be overwhelmed with remorse if you're a

97
00:07:28,030 --> 00:07:32,630
 murderer, if you're a thief, if you're a liar, if you're a

98
00:07:32,630 --> 00:07:35,000
 cheat.

99
00:07:35,000 --> 00:07:42,050
 If you take drugs and alcohol, if you're addicted to this

100
00:07:42,050 --> 00:07:43,000
 or that,

101
00:07:43,000 --> 00:07:47,680
 it's not easy to stay alone in the forest or to stay alone

102
00:07:47,680 --> 00:07:49,000
 in general.

103
00:07:49,000 --> 00:07:52,000
 Honestly, living in the jungle is quite peaceful.

104
00:07:52,000 --> 00:07:56,440
 It's sort of a romantic notion that often people think

105
00:07:56,440 --> 00:07:57,000
 about.

106
00:07:57,000 --> 00:07:59,000
 "Hey, I'll just go off and live in the jungle."

107
00:07:59,000 --> 00:08:02,000
 It's not actually the jungle that's a problem.

108
00:08:02,000 --> 00:08:06,000
 There's other reasons why the jungle is dangerous.

109
00:08:06,000 --> 00:08:12,640
 Sickness and dangerous animals, even the ants will kill you

110
00:08:12,640 --> 00:08:13,000
.

111
00:08:13,000 --> 00:08:18,000
 But as far as just living alone in the forest,

112
00:08:18,000 --> 00:08:24,900
 I lived alone in a tent in the jungle trying to set up a

113
00:08:24,900 --> 00:08:27,000
 forest monastery.

114
00:08:27,000 --> 00:08:30,350
 I've told this story before about how I got chased out by a

115
00:08:30,350 --> 00:08:31,000
 tiger.

116
00:08:31,000 --> 00:08:37,000
 So there are those kinds of dangers.

117
00:08:37,000 --> 00:08:42,000
 But simply living alone in the forest is quite peaceful.

118
00:08:42,000 --> 00:08:45,380
 It's really interesting how difficult it is for us to be at

119
00:08:45,380 --> 00:08:46,000
 peace.

120
00:08:46,000 --> 00:08:49,000
 It's really the crux of the situation.

121
00:08:49,000 --> 00:08:54,000
 It's not that being alone is stressful or suffering.

122
00:08:54,000 --> 00:08:57,000
 It's that we're not able to be at peace.

123
00:08:57,000 --> 00:09:00,000
 We're not at peace.

124
00:09:00,000 --> 00:09:04,530
 And so we find ways to distract ourselves to avoid dealing

125
00:09:04,530 --> 00:09:07,000
 with ourselves.

126
00:09:07,000 --> 00:09:13,930
 We have no tools. We have no means of dealing with

127
00:09:13,930 --> 00:09:16,000
 ourselves.

128
00:09:16,000 --> 00:09:19,780
 And so again, this is a list of ways of coming to terms

129
00:09:19,780 --> 00:09:21,000
 with ourselves,

130
00:09:21,000 --> 00:09:25,340
 being able to be alone with ourselves, to go inside and to

131
00:09:25,340 --> 00:09:27,000
 experience here and now,

132
00:09:27,000 --> 00:09:32,000
 to be without always getting what we want

133
00:09:32,000 --> 00:09:40,500
 or being stimulated by delightful things that are add

134
00:09:40,500 --> 00:09:44,000
ictions.

135
00:09:44,000 --> 00:09:49,320
 And so the first three steps are purity of conduct,

136
00:09:49,320 --> 00:09:50,000
 morality.

137
00:09:50,000 --> 00:09:52,000
 It always starts with morality.

138
00:09:52,000 --> 00:09:55,520
 It's not even that morality is all that important because

139
00:09:55,520 --> 00:09:56,000
 you're not going to be

140
00:09:56,000 --> 00:09:59,100
 killing or stealing or lying or cheating when you're here

141
00:09:59,100 --> 00:10:00,000
 meditating.

142
00:10:00,000 --> 00:10:04,000
 It's more that morality is like a roadblock.

143
00:10:04,000 --> 00:10:07,900
 If you're not a moral, ethical sort of person, you won't

144
00:10:07,900 --> 00:10:09,000
 even get in the door.

145
00:10:09,000 --> 00:10:11,000
 You won't want to come and meditate.

146
00:10:11,000 --> 00:10:16,000
 You try to sit still and you'll be completely perturbed,

147
00:10:16,000 --> 00:10:18,000
 distracted.

148
00:10:18,000 --> 00:10:23,080
 Great stress will come from the sickness in the mind that's

149
00:10:23,080 --> 00:10:28,000
 a result of unwholesomeness.

150
00:10:28,000 --> 00:10:30,000
 That's not to say that you can't.

151
00:10:30,000 --> 00:10:32,000
 Having done bad deeds, you can't meditate.

152
00:10:32,000 --> 00:10:34,000
 It just makes it difficult.

153
00:10:34,000 --> 00:10:38,000
 It might drive you crazy if you've done a lot of bad deeds.

154
00:10:38,000 --> 00:10:41,000
 You have to go slowly.

155
00:10:41,000 --> 00:10:43,990
 This is a reason why we make a determination to keep the

156
00:10:43,990 --> 00:10:45,000
 five precepts.

157
00:10:45,000 --> 00:10:48,000
 One good reason is to give you confidence that,

158
00:10:48,000 --> 00:10:51,530
 "Okay, I did bad things in the past, but I don't have to

159
00:10:51,530 --> 00:10:53,000
 feel guilty because I've changed.

160
00:10:53,000 --> 00:10:58,670
 I understand that that was wrong and I hurt people and so

161
00:10:58,670 --> 00:11:01,000
 on,

162
00:11:01,000 --> 00:11:03,000
 but I've changed now."

163
00:11:03,000 --> 00:11:06,240
 You get a great confidence by knowing that you're now a

164
00:11:06,240 --> 00:11:07,000
 good person.

165
00:11:07,000 --> 00:11:20,000
 It's all really psychological. That's what it's all about.

166
00:11:20,000 --> 00:11:23,000
 Number four is livelihood.

167
00:11:23,000 --> 00:11:26,000
 Livelihood relates to the first three.

168
00:11:26,000 --> 00:11:34,000
 He says if people are impure of action or of livelihood

169
00:11:34,000 --> 00:11:36,000
 means their business,

170
00:11:36,000 --> 00:11:41,610
 their means of livelihood is corrupt, cheating others and

171
00:11:41,610 --> 00:11:42,000
 so on,

172
00:11:42,000 --> 00:11:46,000
 or hurting others, or manipulating others to make a living,

173
00:11:46,000 --> 00:11:49,010
 they're going to go crazy if they try to be alone with

174
00:11:49,010 --> 00:11:50,000
 themselves.

175
00:11:50,000 --> 00:11:52,000
 It's not going to be easy.

176
00:11:52,000 --> 00:11:54,000
 He said, "I'm not like that."

177
00:11:54,000 --> 00:11:59,000
 This gave him great confidence.

178
00:11:59,000 --> 00:12:15,000
 He says, "I found great solace in dwelling in the forest."

179
00:12:15,000 --> 00:12:31,000
 "Ploma pading hara nyivi hara ya."

180
00:12:31,000 --> 00:12:33,000
 Then he goes the next step.

181
00:12:33,000 --> 00:12:39,020
 That starts to talk about once you've talked about your

182
00:12:39,020 --> 00:12:40,000
 conduct,

183
00:12:40,000 --> 00:12:46,760
 then the next thing that you have to work on is the mental

184
00:12:46,760 --> 00:12:48,000
 state.

185
00:12:48,000 --> 00:12:52,300
 He says some people are covetous and full of lust, great

186
00:12:52,300 --> 00:12:54,000
 desires,

187
00:12:54,000 --> 00:12:58,340
 addictions, wanting this, wanting that, liking this, liking

188
00:12:58,340 --> 00:12:59,000
 that.

189
00:12:59,000 --> 00:13:01,000
 We don't see the danger.

190
00:13:01,000 --> 00:13:04,330
 I often get this question of why should we give up the

191
00:13:04,330 --> 00:13:06,000
 things that we like?

192
00:13:06,000 --> 00:13:10,000
 They make us very happy.

193
00:13:10,000 --> 00:13:14,000
 You get your answer when you try to sit still with yourself

194
00:13:14,000 --> 00:13:14,000
.

195
00:13:14,000 --> 00:13:17,510
 When you try to do this very peaceful, seemingly simple

196
00:13:17,510 --> 00:13:19,000
 thing of just being,

197
00:13:19,000 --> 00:13:26,390
 just sitting, it's very difficult because your mind is tor

198
00:13:26,390 --> 00:13:27,000
mented,

199
00:13:27,000 --> 00:13:30,480
 really tormented, and you wonder why meditation is so

200
00:13:30,480 --> 00:13:31,000
 stressful.

201
00:13:31,000 --> 00:13:34,000
 It's not that meditation is so stressful.

202
00:13:34,000 --> 00:13:38,740
 It's that wanting things and not getting those things is

203
00:13:38,740 --> 00:13:40,000
 stressful.

204
00:13:40,000 --> 00:13:48,000
 You can't blame the meditation for your addictions.

205
00:13:48,000 --> 00:13:55,000
 Other people are full of ill will or hate.

206
00:13:55,000 --> 00:13:59,000
 For some people, their mind is on fire.

207
00:13:59,000 --> 00:14:12,000
 A good example of this is just hatred of pain,

208
00:14:12,000 --> 00:14:20,000
 the inability to stand, discomfort.

209
00:14:20,000 --> 00:14:23,000
 We blame pain. We think pain is a bad thing.

210
00:14:23,000 --> 00:14:25,000
 Pain is a problem.

211
00:14:25,000 --> 00:14:27,000
 Why should I sit with pain?

212
00:14:27,000 --> 00:14:29,000
 If I have pain, I should change my position.

213
00:14:29,000 --> 00:14:35,000
 I should go somewhere else, do something else.

214
00:14:35,000 --> 00:14:36,000
 The Buddha doesn't blame pain.

215
00:14:36,000 --> 00:14:40,000
 He says it's not the pain, it's the aversion.

216
00:14:40,000 --> 00:14:44,000
 It's the disliking of the pain.

217
00:14:44,000 --> 00:14:46,000
 Very hard to see.

218
00:14:46,000 --> 00:14:51,000
 But our mind in any way, inflamed with hate,

219
00:14:51,000 --> 00:14:54,000
 if you hate, if you have anger towards other people,

220
00:14:54,000 --> 00:14:57,000
 if you feel you've been wronged,

221
00:14:57,000 --> 00:15:03,510
 if you're consumed by thoughts of vengeance or thoughts of

222
00:15:03,510 --> 00:15:05,000
 anger.

223
00:15:05,000 --> 00:15:09,710
 Very difficult to be with yourself, very difficult to be

224
00:15:09,710 --> 00:15:11,000
 alone.

225
00:15:11,000 --> 00:15:18,000
 You think about all the bad things people have done to you.

226
00:15:18,000 --> 00:15:26,000
 Very hard to be at peace.

227
00:15:26,000 --> 00:15:31,000
 Someone else is overcome by sloth and torpor, laziness,

228
00:15:31,000 --> 00:15:34,000
 sluggishness of mind.

229
00:15:34,000 --> 00:15:37,000
 The ordinary mind is weak.

230
00:15:37,000 --> 00:15:41,000
 Sloth and torpor is like the equivalent of a person

231
00:15:41,000 --> 00:15:44,000
 who has never done any exercise.

232
00:15:44,000 --> 00:15:46,000
 Physically they're very weak.

233
00:15:46,000 --> 00:15:48,580
 They're not able to lift any weight, it's not able to do

234
00:15:48,580 --> 00:15:49,000
 any work.

235
00:15:49,000 --> 00:15:53,000
 If you give them manual labor, say go and lift those blocks

236
00:15:53,000 --> 00:15:57,000
 or build that wall or something,

237
00:15:57,000 --> 00:16:02,000
 where they're not able to use their bodies.

238
00:16:02,000 --> 00:16:06,000
 This feeling of being too weak for something.

239
00:16:06,000 --> 00:16:09,540
 When you get that feeling in the mind, that's a sign of sl

240
00:16:09,540 --> 00:16:11,000
oth and torpor.

241
00:16:11,000 --> 00:16:13,000
 The mind not being strong enough.

242
00:16:13,000 --> 00:16:15,000
 You want to be mindful but your mind is not strong.

243
00:16:15,000 --> 00:16:19,580
 You have a feeling, you can feel that the mind is not able

244
00:16:19,580 --> 00:16:21,000
 to stay with the object.

245
00:16:21,000 --> 00:16:27,000
 It's lazy, it gets easily distracted.

246
00:16:27,000 --> 00:16:30,000
 It doesn't want to put out the effort.

247
00:16:30,000 --> 00:16:36,760
 It's reluctant, it's stiff, it's unwieldy, it's weak, it's

248
00:16:36,760 --> 00:16:40,000
 sloth and torpor.

249
00:16:40,000 --> 00:16:43,500
 It's for such a person very difficult to stay alone in the

250
00:16:43,500 --> 00:16:44,000
 forest,

251
00:16:44,000 --> 00:16:47,530
 and difficult to stay alone because of course you get

252
00:16:47,530 --> 00:16:56,000
 easily distracted and caught up.

253
00:16:56,000 --> 00:17:03,000
 You find it very difficult to be present.

254
00:17:03,000 --> 00:17:11,000
 Difficult to deal with all the challenges that arise.

255
00:17:11,000 --> 00:17:15,000
 Another person is full of doubt, uncertainty.

256
00:17:15,000 --> 00:17:18,370
 Without an uncertainty, it's very difficult to stay alone

257
00:17:18,370 --> 00:17:19,000
 with yourself,

258
00:17:19,000 --> 00:17:24,000
 especially in the forest or the jungle.

259
00:17:24,000 --> 00:17:29,540
 Without the certainty, the power of mind, you easily get

260
00:17:29,540 --> 00:17:32,000
 lost and discouraged,

261
00:17:32,000 --> 00:17:37,720
 overwhelmed by your defilements, your wants and your a

262
00:17:37,720 --> 00:17:39,000
versions.

263
00:17:39,000 --> 00:17:47,030
 You very easily get overwhelmed and lose your ability to

264
00:17:47,030 --> 00:17:50,000
 continue with the practice.

265
00:17:50,000 --> 00:17:52,660
 Some meditators are so full of doubt that it's very

266
00:17:52,660 --> 00:17:57,000
 difficult for them to progress in the meditation practice.

267
00:17:57,000 --> 00:18:00,490
 It's encouraging some of them are able to really break

268
00:18:00,490 --> 00:18:02,000
 through and give it up,

269
00:18:02,000 --> 00:18:05,680
 but it often happens that the doubt leads a meditator to

270
00:18:05,680 --> 00:18:10,000
 stop practicing, at least temporarily.

271
00:18:10,000 --> 00:18:15,070
 They'd be so discouraged and confused, they don't know

272
00:18:15,070 --> 00:18:18,000
 right or wrong, good or bad.

273
00:18:18,000 --> 00:18:28,000
 They have so much doubt.

274
00:18:28,000 --> 00:18:32,000
 The Buddha said, "I've gone beyond doubt." So these are the

275
00:18:32,000 --> 00:18:32,000
 five hindrances,

276
00:18:32,000 --> 00:18:36,000
 and these are really our enemies in the practice.

277
00:18:36,000 --> 00:18:40,280
 Regardless of whether you think of them as right or wrong

278
00:18:40,280 --> 00:18:42,000
 or dangerous or not,

279
00:18:42,000 --> 00:18:46,000
 they get in the way of progress in meditation.

280
00:18:46,000 --> 00:18:50,350
 Really, they get in the way of peace. They get in the way

281
00:18:50,350 --> 00:18:52,000
 of clarity of mind.

282
00:18:52,000 --> 00:18:57,400
 They get in the way of happiness. They are a cause for

283
00:18:57,400 --> 00:19:00,000
 stress and suffering.

284
00:19:00,000 --> 00:19:11,000
 They prevent us from succeeding.

285
00:19:11,000 --> 00:19:18,080
 So the Buddha said, "I develop uncovertousness. I give up

286
00:19:18,080 --> 00:19:20,000
 my desires.

287
00:19:20,000 --> 00:19:23,300
 I have a mind of loving kindness. I am without sloth and

288
00:19:23,300 --> 00:19:24,000
 torpor.

289
00:19:24,000 --> 00:19:29,000
 I have a peaceful mind, not restless."

290
00:19:29,000 --> 00:19:33,000
 I missed one, right? Restlessness. After sloth and torpor,

291
00:19:33,000 --> 00:19:36,000
 we have restlessness.

292
00:19:36,000 --> 00:19:39,230
 So the opposite of sloth and torpor is when your mind is

293
00:19:39,230 --> 00:19:43,000
 too active, a hyperactive mind,

294
00:19:43,000 --> 00:19:49,000
 jumping here, jumping there, not able to focus on anything.

295
00:19:49,000 --> 00:19:54,000
 This is really a good focus for our practice.

296
00:19:54,000 --> 00:19:57,000
 It's a good focus to think of these things.

297
00:19:57,000 --> 00:20:01,000
 This is a very big part of what we have to be mindful of.

298
00:20:01,000 --> 00:20:04,380
 When we practice meditation, a very large part of our

299
00:20:04,380 --> 00:20:10,000
 practice is to try and be mindful of the hindrances

300
00:20:10,000 --> 00:20:14,440
 and to learn to let them go, to fix them, to straighten our

301
00:20:14,440 --> 00:20:15,000
 minds,

302
00:20:15,000 --> 00:20:20,990
 and to cultivate such firmness and rectitude of mind that

303
00:20:20,990 --> 00:20:24,000
 these defilements don't have power over us.

304
00:20:24,000 --> 00:20:32,000
 They don't have a chance to consume our mind.

305
00:20:32,000 --> 00:20:36,420
 Some meditators, some religious people are given to self

306
00:20:36,420 --> 00:20:41,000
 praise and disparagement of others.

307
00:20:41,000 --> 00:20:48,200
 Some are subject to alarm and terror, fearful. They have

308
00:20:48,200 --> 00:20:50,000
 great fear.

309
00:20:50,000 --> 00:20:55,000
 Some are desirous of gain, honor, and renown.

310
00:20:55,000 --> 00:21:01,280
 Very difficult to go off into the forest if you have

311
00:21:01,280 --> 00:21:03,000
 ambition.

312
00:21:03,000 --> 00:21:09,000
 Some are lazy and wanting an energy, some are unmindful and

313
00:21:09,000 --> 00:21:11,000
 not fully aware.

314
00:21:11,000 --> 00:21:15,000
 Some are unconcentrated and with straying minds.

315
00:21:15,000 --> 00:21:21,380
 Here's a list of other more general qualities that are

316
00:21:21,380 --> 00:21:23,000
 required.

317
00:21:23,000 --> 00:21:33,820
 Self praise and disparagement of others. Ego. Ego is a real

318
00:21:33,820 --> 00:21:35,000
 obstacle to the practice.

319
00:21:35,000 --> 00:21:41,950
 Because an egoist, someone who is egotistical, requires an

320
00:21:41,950 --> 00:21:43,000
 audience.

321
00:21:43,000 --> 00:21:49,000
 They require praise. They require constant validation.

322
00:21:49,000 --> 00:21:56,870
 We love to make other people proud of us, have them look on

323
00:21:56,870 --> 00:22:00,000
 us positively.

324
00:22:00,000 --> 00:22:04,000
 Very difficult to find reality that way.

325
00:22:04,000 --> 00:22:10,710
 Very difficult to stay with the present moment when you're

326
00:22:10,710 --> 00:22:16,000
 consumed by the need for praise.

327
00:22:16,000 --> 00:22:20,000
 Or when you're disparaging of others.

328
00:22:20,000 --> 00:22:25,000
 Sort of a nasty state.

329
00:22:25,000 --> 00:22:33,000
 Subject to alarm and terror is...

330
00:22:33,000 --> 00:22:35,330
 Well, particularly in the forest, it's very difficult to be

331
00:22:35,330 --> 00:22:36,000
 in the forest.

332
00:22:36,000 --> 00:22:39,000
 They're so peaceful at night.

333
00:22:39,000 --> 00:22:42,260
 But can you imagine being full of fear and dread of the

334
00:22:42,260 --> 00:22:43,000
 forest?

335
00:22:43,000 --> 00:22:45,640
 I grew up in the forest, so going into the forest at night

336
00:22:45,640 --> 00:22:47,000
 was never a scary thing.

337
00:22:47,000 --> 00:22:53,120
 Of course it's kind of troublesome about snakes and scorp

338
00:22:53,120 --> 00:22:55,000
ions and so on.

339
00:22:55,000 --> 00:22:58,000
 There are real dangers.

340
00:22:58,000 --> 00:23:02,000
 And then of course there's fear of ghosts, fear of spirits.

341
00:23:02,000 --> 00:23:05,000
 There were times when I was in the forest and the noises,

342
00:23:05,000 --> 00:23:12,000
 sometimes the noises were so disturbing.

343
00:23:12,000 --> 00:23:16,280
 But just being alone with yourself, if you're a worry, if

344
00:23:16,280 --> 00:23:22,490
 you're a worried... someone who worries a lot, if you have

345
00:23:22,490 --> 00:23:24,000
 great fear,

346
00:23:24,000 --> 00:23:31,930
 being alone is... it can be such a fearsome, crippling

347
00:23:31,930 --> 00:23:34,000
 experience.

348
00:23:34,000 --> 00:23:37,570
 If you're lazy... so I guess Lautendorf, we can't call laz

349
00:23:37,570 --> 00:23:40,000
iness, it's just a weakness of mind.

350
00:23:40,000 --> 00:23:46,000
 But laziness is when you really just can't be bothered.

351
00:23:46,000 --> 00:23:51,160
 A lazy person living alone in the jungle will not last very

352
00:23:51,160 --> 00:23:52,000
 long.

353
00:23:52,000 --> 00:23:58,000
 It's not something that's going to...

354
00:23:58,000 --> 00:24:04,630
 they'll constantly be seeking out more comfort and avoiding

355
00:24:04,630 --> 00:24:10,000
 the cultivation of good wholesome qualities.

356
00:24:10,000 --> 00:24:13,000
 All of the evil will seep in.

357
00:24:13,000 --> 00:24:18,620
 Laziness and not putting out effort, it's not actually in

358
00:24:18,620 --> 00:24:21,000
 and of itself evil.

359
00:24:21,000 --> 00:24:25,320
 It just is basically saying allowing the evil to continue

360
00:24:25,320 --> 00:24:26,000
 and grow.

361
00:24:26,000 --> 00:24:29,260
 Those things that cause us suffering, those things that

362
00:24:29,260 --> 00:24:34,000
 create stress in our minds, they grow because of laziness.

363
00:24:34,000 --> 00:24:39,120
 You might think, "Well, why should I bother? What's wrong

364
00:24:39,120 --> 00:24:41,000
 with just living?"

365
00:24:41,000 --> 00:24:44,090
 The problem is that we're not just living. Our minds are

366
00:24:44,090 --> 00:24:48,000
 full of things that cause us stress and suffering.

367
00:24:48,000 --> 00:24:56,000
 And our laziness is like permission for them to continue.

368
00:24:56,000 --> 00:25:00,000
 Some meditators are unmindful and not fully aware.

369
00:25:00,000 --> 00:25:05,000
 The Buddha said, "I am established in mindfulness."

370
00:25:05,000 --> 00:25:12,520
 It's of course key. If you're not mindful, this is where

371
00:25:12,520 --> 00:25:21,000
 all of the problems of the mind gain hold, take control.

372
00:25:21,000 --> 00:25:26,280
 Why we suffer, why we're traumatized, why we react

373
00:25:26,280 --> 00:25:29,000
 violently to experiences.

374
00:25:29,000 --> 00:25:32,870
 It's because we're not clearly aware of the nature of the

375
00:25:32,870 --> 00:25:35,000
 experiences as they happen.

376
00:25:35,000 --> 00:25:38,000
 If you're mindful, you can never react improperly.

377
00:25:38,000 --> 00:25:42,840
 If you're truly aware, in that moment there's a pure

378
00:25:42,840 --> 00:25:46,000
 understanding of the experience.

379
00:25:46,000 --> 00:25:52,270
 And because of that pure understanding, there's no reaction

380
00:25:52,270 --> 00:25:55,000
 of liking or disliking.

381
00:25:55,000 --> 00:26:01,000
 There's just experience.

382
00:26:01,000 --> 00:26:04,360
 Some meditators are of unconcentrated and with straying

383
00:26:04,360 --> 00:26:08,000
 minds. "I am possessed of concentration," he says.

384
00:26:08,000 --> 00:26:11,000
 So there's things he thought to himself.

385
00:26:11,000 --> 00:26:14,120
 "I'm not like this. I can go off and live in the forest. I

386
00:26:14,120 --> 00:26:17,000
 can be alone with myself.

387
00:26:17,000 --> 00:26:23,220
 Finally, because I'm focused." If your mind is not focused,

388
00:26:23,220 --> 00:26:27,940
 again, very easy for your mind to stray into unwholesomen

389
00:26:27,940 --> 00:26:28,000
ess

390
00:26:28,000 --> 00:26:31,330
 and things that cause you stress and suffering. By evil and

391
00:26:31,330 --> 00:26:35,000
 by unwholesomeness, we just mean those things that cause

392
00:26:35,000 --> 00:26:36,000
 you stress and suffering.

393
00:26:36,000 --> 00:26:41,000
 Just sitting here is not always a pleasant experience

394
00:26:41,000 --> 00:26:44,450
 because there is desire, because there is aversion, because

395
00:26:44,450 --> 00:26:46,000
 there's all these things.

396
00:26:46,000 --> 00:26:49,210
 It's a fairly comprehensive list, or it's a good list, of

397
00:26:49,210 --> 00:26:54,250
 the sorts of things that we're going to have to work on in

398
00:26:54,250 --> 00:26:59,000
 order to succeed in our meditation practice.

399
00:26:59,000 --> 00:27:03,000
 He says something else after that that's interesting.

400
00:27:03,000 --> 00:27:08,250
 This question of whether we should avoid unpleasantness, he

401
00:27:08,250 --> 00:27:12,000
 says, he's talking particularly about fear.

402
00:27:12,000 --> 00:27:20,830
 He says, when someone is devoid of wisdom, then they get

403
00:27:20,830 --> 00:27:26,730
 afraid by living in the forest. They get upset living in

404
00:27:26,730 --> 00:27:29,000
 the forest.

405
00:27:29,000 --> 00:27:32,000
 Oh, no, that's not the important point.

406
00:27:32,000 --> 00:27:39,600
 Oh, yeah, no, the last one is wisdom. So the final quality

407
00:27:39,600 --> 00:27:42,000
 is wisdom.

408
00:27:42,000 --> 00:27:46,520
 If one is unwise, this is the understanding. If one doesn't

409
00:27:46,520 --> 00:27:51,000
 understand reality, take pain, for example.

410
00:27:51,000 --> 00:27:55,040
 Now, true understanding of pain is really that pain is

411
00:27:55,040 --> 00:27:59,000
 neutral. It's an experience.

412
00:27:59,000 --> 00:28:03,030
 If you don't have that understanding, if you haven't seen

413
00:28:03,030 --> 00:28:10,250
 that clearly, which most of us haven't, pain is intolerable

414
00:28:10,250 --> 00:28:11,000
.

415
00:28:11,000 --> 00:28:16,060
 Pain is something to be done away with. Why? Because we don

416
00:28:16,060 --> 00:28:19,000
't understand it.

417
00:28:19,000 --> 00:28:23,520
 An important claim in Buddhism that I repeat often is that

418
00:28:23,520 --> 00:28:28,000
 when you understand something, it has no power over you.

419
00:28:28,000 --> 00:28:32,750
 If you understand reality as it is, if you have a clear

420
00:28:32,750 --> 00:28:36,520
 understanding of reality, it's not possible for you to be

421
00:28:36,520 --> 00:28:41,000
 disturbed, stressed, upset.

422
00:28:41,000 --> 00:28:52,550
 Suffering can't arise. Suffering is a product of delusion,

423
00:28:52,550 --> 00:28:56,000
 of ignorance.

424
00:28:56,000 --> 00:28:59,540
 When you experience pain, if you see it just as pain, when

425
00:28:59,540 --> 00:29:04,840
 we say to ourselves, "Pain, pain," it has no power over you

426
00:29:04,840 --> 00:29:05,000
.

427
00:29:05,000 --> 00:29:07,630
 When you see something that would normally make you very

428
00:29:07,630 --> 00:29:11,510
 angry or you hear something that would make you very lust

429
00:29:11,510 --> 00:29:15,000
ful or ambition or desire it, so on.

430
00:29:15,000 --> 00:29:22,800
 If you're mindful, that doesn't occur. You experience it,

431
00:29:22,800 --> 00:29:26,000
 it arises, it ceases.

432
00:29:26,000 --> 00:29:30,580
 So, but no, the next thing I wanted to talk about was just

433
00:29:30,580 --> 00:29:36,000
 as a note because he talks about it as well in the suttas.

434
00:29:36,000 --> 00:29:39,460
 When these experiences arise, what should you do? Should

435
00:29:39,460 --> 00:29:43,920
 you do something else? And the Buddha's practice as a bod

436
00:29:43,920 --> 00:29:48,570
hisattva before he became a Buddha was to not run away from

437
00:29:48,570 --> 00:29:49,000
 it.

438
00:29:49,000 --> 00:29:54,640
 Suppose there's something that makes you afraid. Well, what

439
00:29:54,640 --> 00:30:02,000
 he did is he went to forest shrines on the holidays.

440
00:30:02,000 --> 00:30:09,700
 It's basically the equivalent of going to a haunted house

441
00:30:09,700 --> 00:30:12,000
 on Halloween.

442
00:30:12,000 --> 00:30:15,750
 In India there would be these forest shrines where the

443
00:30:15,750 --> 00:30:19,490
 angels or the demons, right, they would go to perpetuate

444
00:30:19,490 --> 00:30:22,000
 these spirits, evil spirits, good spirits.

445
00:30:22,000 --> 00:30:25,490
 And these spirits were said to be active on the full moon,

446
00:30:25,490 --> 00:30:29,000
 especially active on the full moon, on the new moon.

447
00:30:29,000 --> 00:30:32,890
 It's superstition, but basically it's the equivalent of a

448
00:30:32,890 --> 00:30:35,000
 haunted house on Halloween.

449
00:30:35,000 --> 00:30:39,900
 It's the scariest possible situation. He said, "I went

450
00:30:39,900 --> 00:30:42,000
 there and I did walking meditation.

451
00:30:42,000 --> 00:30:47,000
 And if when I was walking, I got afraid, I would say to

452
00:30:47,000 --> 00:30:54,330
 myself, I'm not going to stop walking until this fear goes

453
00:30:54,330 --> 00:30:55,000
."

454
00:30:55,000 --> 00:30:59,790
 And if I was sitting, I would say, "I'm not going to stop

455
00:30:59,790 --> 00:31:04,000
 sitting." Meaning he didn't even change his posture.

456
00:31:04,000 --> 00:31:10,140
 The point is, when the fear came, he made a promise that he

457
00:31:10,140 --> 00:31:14,000
 would stay and deal with the fear.

458
00:31:14,000 --> 00:31:18,970
 It's actually just one example of the sorts of things that

459
00:31:18,970 --> 00:31:23,930
 can arise, but it's an example of how to deal with any sort

460
00:31:23,930 --> 00:31:28,000
 of problem when you're very angry.

461
00:31:28,000 --> 00:31:30,950
 As a meditator, you shouldn't run away from the thing that

462
00:31:30,950 --> 00:31:32,000
 makes you angry.

463
00:31:32,000 --> 00:31:36,240
 In fact, to some extent, following the Buddha's example, it

464
00:31:36,240 --> 00:31:41,250
's good for you to be in association with the things that

465
00:31:41,250 --> 00:31:43,000
 make you angry to the extent that you're able.

466
00:31:43,000 --> 00:31:47,840
 If you're strong enough to be able to change the way you

467
00:31:47,840 --> 00:31:53,260
 look at the experience, it can be quite powerful when you

468
00:31:53,260 --> 00:31:56,120
're able to experience the same thing without getting

469
00:31:56,120 --> 00:32:05,000
 disturbed, stressed, upset by it.

470
00:32:05,000 --> 00:32:10,010
 So with all these qualities, it's not something to be

471
00:32:10,010 --> 00:32:13,000
 afraid of or discouraged by.

472
00:32:13,000 --> 00:32:17,790
 If you have any of these problems that are going to get in

473
00:32:17,790 --> 00:32:21,890
 the way of your practice, really the best way to deal with

474
00:32:21,890 --> 00:32:25,000
 them is to spend some time being mindful of them.

475
00:32:25,000 --> 00:32:28,830
 If you're lazy, be mindful of your laziness. If you're

476
00:32:28,830 --> 00:32:32,000
 angry, be mindful of your anger.

477
00:32:32,000 --> 00:32:35,630
 If you want something or you're addicted to something,

478
00:32:35,630 --> 00:32:39,970
 spend time being mindful of it in all of its aspects, the

479
00:32:39,970 --> 00:32:49,000
 feelings, the emotions, the thoughts, the experiences.

480
00:32:49,000 --> 00:32:51,800
 And then he goes through the progression of enlightenment,

481
00:32:51,800 --> 00:32:54,000
 which is fairly standard and I won't go into it.

482
00:32:54,000 --> 00:32:57,240
 I thought this would be interesting to talk about these. It

483
00:32:57,240 --> 00:33:02,330
 gives us a chance to go over things that get in the way of

484
00:33:02,330 --> 00:33:04,000
 our practice.

485
00:33:04,000 --> 00:33:07,010
 We're all very interested in learning how to stay with

486
00:33:07,010 --> 00:33:10,260
 ourselves, how to be with ourselves, and how to have it be

487
00:33:10,260 --> 00:33:12,000
 a productive experience.

488
00:33:12,000 --> 00:33:16,390
 This sutta does a good job explaining the ways that the

489
00:33:16,390 --> 00:33:20,670
 difference between a productive meditator and an un

490
00:33:20,670 --> 00:33:23,000
productive meditator.

491
00:33:23,000 --> 00:33:26,810
 So there you go. That's the Dhamma for tonight. Thank you

492
00:33:26,810 --> 00:33:30,000
 all for tuning in, for coming out.

493
00:33:30,000 --> 00:33:35,000
 Saturdays now we have visitors. It's advertised.

494
00:33:35,000 --> 00:33:38,880
 We also give talks Monday and Wednesday. You're welcome to

495
00:33:38,880 --> 00:33:42,000
 come out then. And of course our center is open anytime.

496
00:33:42,000 --> 00:33:44,000
 You're welcome to just come by.

497
00:33:44,000 --> 00:33:50,330
 If you want to learn how to meditate, just make an

498
00:33:50,330 --> 00:33:56,000
 appointment and we can set something up.

499
00:33:56,000 --> 00:33:58,680
 So questions are for Wednesday. I'm going to try that for a

500
00:33:58,680 --> 00:34:02,000
 while, see how that goes. No questions tonight.

501
00:34:02,000 --> 00:34:06,060
 We'll save all our questions once a week. There's too many

502
00:34:06,060 --> 00:34:10,440
 questions. No, there's lots of questions. So Wednesday will

503
00:34:10,440 --> 00:34:12,000
 be question day.

504
00:34:12,000 --> 00:34:17,380
 Saturday is giving an official talk based on some teaching

505
00:34:17,380 --> 00:34:19,000
 of the Buddha.

506
00:34:19,000 --> 00:34:23,000
 So thank you all for coming out. Again, have a good night.

507
00:34:23,000 --> 00:34:30,000
 [

