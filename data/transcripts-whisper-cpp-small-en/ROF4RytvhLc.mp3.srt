1
00:00:00,000 --> 00:00:05,300
 Can you explain alms? How do you help a monk? Can you buy

2
00:00:05,300 --> 00:00:06,000
 them food?

3
00:00:06,000 --> 00:00:10,580
 Yeah, I mean this is a big thing really. Westerners, they

4
00:00:10,580 --> 00:00:12,000
 appreciate monks.

5
00:00:12,000 --> 00:00:16,570
 Many Western people appreciate monks and get teachings from

6
00:00:16,570 --> 00:00:17,000
 them.

7
00:00:17,000 --> 00:00:19,990
 But they don't know what to do. I guess there's also monks

8
00:00:19,990 --> 00:00:23,000
 that you don't get teachings from, but you think somebody

9
00:00:23,000 --> 00:00:24,000
 should help them.

10
00:00:24,000 --> 00:00:27,890
 Which is good. Feeding stray cats is good. Why isn't

11
00:00:27,890 --> 00:00:30,000
 feeding stray monks good?

12
00:00:30,000 --> 00:00:33,310
 Or feeding monks good? Yeah, you can buy them food. Don't

13
00:00:33,310 --> 00:00:35,000
 buy them food in the evening.

14
00:00:35,000 --> 00:00:38,480
 Buy them food for the day. Monks aren't allowed to keep

15
00:00:38,480 --> 00:00:39,000
 food.

16
00:00:39,000 --> 00:00:44,000
 Theravada Buddhist monks aren't allowed to store food.

17
00:00:44,000 --> 00:00:47,270
 So if there are Theravada monks, you should give them food

18
00:00:47,270 --> 00:00:49,000
 that they can eat in the morning.

19
00:00:49,000 --> 00:00:52,670
 It should be given after dawn and before noon, and it

20
00:00:52,670 --> 00:00:55,000
 should be consumed on that day.

21
00:00:55,000 --> 00:00:59,550
 If you're interested in learning about alms, I wrote an

22
00:00:59,550 --> 00:01:04,000
 article that...

23
00:01:04,000 --> 00:01:06,800
 I don't know. Maybe it's probably not what you're looking

24
00:01:06,800 --> 00:01:10,000
 for. But there's an interesting article.

25
00:01:10,000 --> 00:01:14,230
 It was talking about the idea of giving alms and what it

26
00:01:14,230 --> 00:01:18,000
 means. It shouldn't be considered an exchange.

27
00:01:18,000 --> 00:01:21,870
 It's considered fulfilling a need. So when monks go on alms

28
00:01:21,870 --> 00:01:26,000
, it's because they need food.

29
00:01:26,000 --> 00:01:29,290
 Just like when cats come to your door and start meowing, it

30
00:01:29,290 --> 00:01:31,000
's because they need food.

31
00:01:31,000 --> 00:01:33,620
 So yeah, giving them food is a great thing. Especially

32
00:01:33,620 --> 00:01:37,000
 because you consider that, wow, they're probably doing good

33
00:01:37,000 --> 00:01:38,000
 things for people.

34
00:01:38,000 --> 00:01:40,600
 They're certainly... yeah, they're probably doing good

35
00:01:40,600 --> 00:01:43,000
 things for themselves and good things for other people.

36
00:01:43,000 --> 00:01:49,000
 It's a better bet than giving to a cat.

37
00:01:49,000 --> 00:01:52,930
 That's the best way to help. Monks are not allowed to ask

38
00:01:52,930 --> 00:01:55,000
 for things. They're not allowed to beg.

39
00:01:55,000 --> 00:01:58,460
 But unless a person says to the monk, and you have to be

40
00:01:58,460 --> 00:02:00,000
 careful about this,

41
00:02:00,000 --> 00:02:05,460
 but if you say to a monk or a male or female monk, "Look,

42
00:02:05,460 --> 00:02:07,000
 if you need anything, let me know.

43
00:02:07,000 --> 00:02:11,010
 I'll be happy to do... I'll try my best to... if it's

44
00:02:11,010 --> 00:02:14,000
 within my power, I'll try to get it for you."

45
00:02:14,000 --> 00:02:20,250
 Then they can ask. And they're allowed to ask for four

46
00:02:20,250 --> 00:02:24,000
 months. This is the official allowance.

47
00:02:24,000 --> 00:02:27,490
 From that point in time, they can ask you for another four

48
00:02:27,490 --> 00:02:28,000
 months.

49
00:02:28,000 --> 00:02:30,460
 Unless you say to them, "For the rest of my life, you can

50
00:02:30,460 --> 00:02:32,000
 ask me," or something like that.

51
00:02:32,000 --> 00:02:37,190
 Or, "For the rest of this week, you can ask me." Unless you

52
00:02:37,190 --> 00:02:40,000
 set a limit, then it's four months.

53
00:02:40,000 --> 00:02:44,090
 But anyway, then you don't have to guess what they need,

54
00:02:44,090 --> 00:02:49,750
 and you can supply them with something that they may not

55
00:02:49,750 --> 00:02:51,000
 have.

56
00:02:51,000 --> 00:02:54,340
 The best thing you can do to help monks is to go and learn

57
00:02:54,340 --> 00:02:58,000
 from them and become part of the community.

58
00:02:58,000 --> 00:03:01,970
 Because there's so much work that needs to be done to

59
00:03:01,970 --> 00:03:04,000
 facilitate teaching, for example.

60
00:03:04,000 --> 00:03:08,000
 So get involved in the organization.

61
00:03:08,000 --> 00:03:11,360
 Anyway, this article I wrote, I don't know if it's actually

62
00:03:11,360 --> 00:03:12,000
 relating to this,

63
00:03:12,000 --> 00:03:17,360
 but it relates to the idea of supporting an organization

64
00:03:17,360 --> 00:03:21,000
 and how we should best view supporting organizations,

65
00:03:21,000 --> 00:03:27,000
 especially Buddhist organizations. It's called, "There is

66
00:03:27,000 --> 00:03:30,000
 two such things as a free lunch."

67
00:03:30,000 --> 00:03:33,000
 It's on my web blog.

