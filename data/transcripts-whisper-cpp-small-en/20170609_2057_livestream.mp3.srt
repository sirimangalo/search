1
00:00:00,000 --> 00:00:09,240
 Okay, good evening everyone.

2
00:00:09,240 --> 00:00:11,240
 Welcome.

3
00:00:11,240 --> 00:00:14,960
 Tonight's Dama Talk.

4
00:00:14,960 --> 00:00:19,880
 Probably not going to be that long tonight, we'll see.

5
00:00:19,880 --> 00:00:25,060
 But I have something short but important to bring us back

6
00:00:25,060 --> 00:00:28,440
 to make us think of.

7
00:00:28,440 --> 00:00:37,810
 What I'd like to talk about tonight is the Baddhika Rata

8
00:00:37,810 --> 00:00:42,240
 Sutta, which is often translated

9
00:00:42,240 --> 00:00:52,400
 as the single excellent night.

10
00:00:52,400 --> 00:00:58,520
 It's the sutta on how to have one good day actually.

11
00:00:58,520 --> 00:01:03,500
 In Pali they use the word rati, which means night, to

12
00:01:03,500 --> 00:01:05,640
 signify a day.

13
00:01:05,640 --> 00:01:08,240
 They count by the nights.

14
00:01:08,240 --> 00:01:14,080
 So in English we count by days, so we say how many days.

15
00:01:14,080 --> 00:01:20,520
 So we'd say one good day.

16
00:01:20,520 --> 00:01:24,240
 Why it's interesting to talk about this is because the

17
00:01:24,240 --> 00:01:25,800
 Buddha found it, seems to have

18
00:01:25,800 --> 00:01:31,240
 found it fairly, and outstandingly important.

19
00:01:31,240 --> 00:01:34,810
 Something that he taught repeatedly we have, and the people

20
00:01:34,810 --> 00:01:36,840
 who put together the compilation

21
00:01:36,840 --> 00:01:45,550
 of the Buddha's teaching that we have, gave us four

22
00:01:45,550 --> 00:01:51,040
 different suttas on Baddhika Rata,

23
00:01:51,040 --> 00:02:05,440
 and the Majimini Kaya, 31-34.

24
00:02:05,440 --> 00:02:07,920
 One good day.

25
00:02:07,920 --> 00:02:12,530
 Something my teacher would often, well, would quite often

26
00:02:12,530 --> 00:02:13,520
 bring up.

27
00:02:13,520 --> 00:02:18,410
 It sums up, it's one of those things that suttas that sums

28
00:02:18,410 --> 00:02:20,800
 up mindfulness quite well.

29
00:02:20,800 --> 00:02:30,360
 And it's quite useful as advice for meditators.

30
00:02:30,360 --> 00:02:38,370
 I mean, just in and of itself it's a teaching, how to have

31
00:02:38,370 --> 00:02:40,320
 a good day.

32
00:02:40,320 --> 00:02:45,600
 Because what it says is it says, first of all, how to be

33
00:02:45,600 --> 00:02:48,560
 perfect, but also how to live

34
00:02:48,560 --> 00:02:49,560
 now.

35
00:02:49,560 --> 00:02:50,560
 Right?

36
00:02:50,560 --> 00:02:56,880
 We talk about having a good day.

37
00:02:56,880 --> 00:02:59,160
 This is the answer to our questions.

38
00:02:59,160 --> 00:03:04,290
 How do we turn our hopes and our desires and our wants and

39
00:03:04,290 --> 00:03:05,720
 our wishes?

40
00:03:05,720 --> 00:03:10,000
 How do we find what we're looking for here and now?

41
00:03:10,000 --> 00:03:19,080
 How do we make it real so we can have that day?

42
00:03:19,080 --> 00:03:22,840
 And also, according to Buddhism, what does it mean to have

43
00:03:22,840 --> 00:03:24,280
 a good day, right?

44
00:03:24,280 --> 00:03:27,430
 Because for most of us, most people in the world, a good

45
00:03:27,430 --> 00:03:29,480
 day is something quite different

46
00:03:29,480 --> 00:03:32,080
 from what the Buddha had in mind.

47
00:03:32,080 --> 00:03:37,240
 And that kind of good day is not terribly meaningful.

48
00:03:37,240 --> 00:03:41,260
 Because when we talk about having a good day, it's

49
00:03:41,260 --> 00:03:45,400
 something that lasts a single day, right?

50
00:03:45,400 --> 00:03:48,080
 I know the Buddha meant something quite different.

51
00:03:48,080 --> 00:03:57,990
 But how do you get to the point where your day is good in a

52
00:03:57,990 --> 00:04:01,440
 transformative way?

53
00:04:01,440 --> 00:04:05,040
 So it's not just you have your good day and then it's over.

54
00:04:05,040 --> 00:04:11,850
 You get to the point where you achieve, you accomplish, you

55
00:04:11,850 --> 00:04:15,280
 become something worthwhile

56
00:04:15,280 --> 00:04:22,160
 in that one day.

57
00:04:22,160 --> 00:04:27,290
 So it starts with a very famous, very, I think, well-known

58
00:04:27,290 --> 00:04:29,880
 teaching of the Buddha.

59
00:04:29,880 --> 00:04:35,280
 "Adhitaanan wakameyana patigankeyana agatam."

60
00:04:35,280 --> 00:04:39,790
 That will not go back to the past nor worry about the

61
00:04:39,790 --> 00:04:41,600
 future, right?

62
00:04:41,600 --> 00:04:43,620
 You may not have heard those words exactly before, but you

63
00:04:43,620 --> 00:04:44,600
 know that this is what the

64
00:04:44,600 --> 00:04:47,320
 Buddha taught.

65
00:04:47,320 --> 00:04:49,720
 Don't go back to the past.

66
00:04:49,720 --> 00:04:52,320
 Don't bring up the past.

67
00:04:52,320 --> 00:04:55,320
 Don't worry about the future.

68
00:04:55,320 --> 00:05:01,520
 "Adhitaan bahinantanga patancha anagatam."

69
00:05:01,520 --> 00:05:03,200
 What's in the past is gone already.

70
00:05:03,200 --> 00:05:06,400
 What's in the future has not yet come.

71
00:05:06,400 --> 00:05:12,040
 This is quite, it's obvious, right?

72
00:05:12,040 --> 00:05:15,490
 This is not a teaching that we have to be taught, do you

73
00:05:15,490 --> 00:05:16,200
 think?

74
00:05:16,200 --> 00:05:18,780
 I don't suppose there's anyone here who didn't know that

75
00:05:18,780 --> 00:05:20,280
 the past is already gone or that

76
00:05:20,280 --> 00:05:24,600
 the future hasn't come yet.

77
00:05:24,600 --> 00:05:31,400
 But it's important to repeat.

78
00:05:31,400 --> 00:05:37,870
 We act as though the future is here and that the future is

79
00:05:37,870 --> 00:05:40,200
 a certain thing.

80
00:05:40,200 --> 00:05:44,280
 We act as though the past is here as well.

81
00:05:44,280 --> 00:05:47,160
 When we think about the past, it makes us suffer.

82
00:05:47,160 --> 00:05:53,980
 The bad things make us suffer just as they did when they

83
00:05:53,980 --> 00:05:56,960
 actually happened.

84
00:05:56,960 --> 00:06:02,720
 When we think about good things, we pine away after them.

85
00:06:02,720 --> 00:06:06,560
 We never really, well, we often don't live in the present.

86
00:06:06,560 --> 00:06:11,840
 We're caught up in the past and the future, pushed and

87
00:06:11,840 --> 00:06:12,960
 pulled.

88
00:06:12,960 --> 00:06:16,380
 When in fact the only thing that's real and the only thing

89
00:06:16,380 --> 00:06:18,000
 that ever has been or ever

90
00:06:18,000 --> 00:06:24,560
 will be real is now, is the present.

91
00:06:24,560 --> 00:06:25,760
 Don't go back to the past.

92
00:06:25,760 --> 00:06:29,440
 Don't go ahead to the future.

93
00:06:29,440 --> 00:06:40,800
 These people who make plans, make plans for the future,

94
00:06:40,800 --> 00:06:43,920
 only to have them ruined by the

95
00:06:43,920 --> 00:06:48,320
 reality, by the uncertainty of life.

96
00:06:48,320 --> 00:06:52,270
 In fact, you might say that all of our disappointments in

97
00:06:52,270 --> 00:06:54,480
 life come from making plans.

98
00:06:54,480 --> 00:06:57,160
 If we didn't have expectations, if we didn't live in the

99
00:06:57,160 --> 00:06:58,880
 future and think, "Hmm, maybe

100
00:06:58,880 --> 00:07:01,600
 tomorrow I'll get this or that.

101
00:07:01,600 --> 00:07:04,560
 Maybe tomorrow I'll have what I want."

102
00:07:04,560 --> 00:07:07,710
 If we weren't looking for something, "Hey, even just the

103
00:07:07,710 --> 00:07:09,680
 next moment, if I open the fridge,

104
00:07:09,680 --> 00:07:13,120
 there will be some delicious food there."

105
00:07:13,120 --> 00:07:17,930
 If we didn't have any of that, we wouldn't be disappointed

106
00:07:17,930 --> 00:07:21,280
 when things turned out differently.

107
00:07:21,280 --> 00:07:26,000
 When we talk about the past, we have this story of Patach

108
00:07:26,000 --> 00:07:28,320
ara who lost her two sons and

109
00:07:28,320 --> 00:07:32,800
 her whole family and her husband in the same day and went

110
00:07:32,800 --> 00:07:33,680
 crazy.

111
00:07:33,680 --> 00:07:39,790
 She was totally lost her mind and went out of her mind with

112
00:07:39,790 --> 00:07:42,960
 grief for losing her husband

113
00:07:42,960 --> 00:07:47,680
 and both of her sons and then her whole family.

114
00:07:47,680 --> 00:07:53,520
 The Buddha said to her, "Yes, it's true that you've lost

115
00:07:53,520 --> 00:07:56,080
 quite a bit and that this is something

116
00:07:56,080 --> 00:08:00,730
 that is making you very sad, but all the tears that you've

117
00:08:00,730 --> 00:08:03,360
 cried and the rounds of samsara

118
00:08:03,360 --> 00:08:05,920
 are greater than the waters in all the ocean."

119
00:08:05,920 --> 00:08:28,800
 Meaning that the past is gone and the past is...

120
00:08:28,800 --> 00:08:40,080
 past is quite insignificant what happened in the past at

121
00:08:40,080 --> 00:08:43,440
 this time or that time.

122
00:08:43,440 --> 00:08:47,040
 All we have now is in the present.

123
00:08:47,040 --> 00:08:55,280
 So, then what should we do?

124
00:08:55,280 --> 00:08:57,880
 If we shouldn't go back to the past or we shouldn't bring

125
00:08:57,880 --> 00:08:59,040
 up the past or worry about

126
00:08:59,040 --> 00:09:00,040
 the future.

127
00:09:00,040 --> 00:09:01,040
 "Patyupanang chiyodamang datta vipassati."

128
00:09:01,040 --> 00:09:02,040
 This I talk about a lot.

129
00:09:02,040 --> 00:09:03,040
 Many of you have probably heard me mention this before.

130
00:09:03,040 --> 00:09:04,040
 "Patyupanang" is the present moment.

131
00:09:04,040 --> 00:09:05,040
 "Yodamang yodamang."

132
00:09:05,040 --> 00:09:06,040
 Whatever "dama," "dama" is a thing, it's a thing.

133
00:09:06,040 --> 00:09:07,040
 "Dama" is a thing.

134
00:09:07,040 --> 00:09:08,040
 "Dama" is a thing.

135
00:09:08,040 --> 00:09:09,040
 "Dama" is a thing.

136
00:09:09,040 --> 00:09:10,040
 "Dama" is a thing.

137
00:09:10,040 --> 00:09:11,040
 "Dama" is a thing.

138
00:09:11,040 --> 00:09:12,040
 "Dama" is a thing.

139
00:09:12,040 --> 00:09:13,040
 "Dama" is a thing.

140
00:09:13,040 --> 00:09:14,040
 "Dama" is a thing.

141
00:09:14,040 --> 00:09:15,040
 "Dama" is a thing.

142
00:09:15,040 --> 00:09:16,040
 "Dama" is a thing.

143
00:09:16,040 --> 00:09:23,640
 "Yodam yodamang."

144
00:09:23,640 --> 00:09:28,660
 Whatever "dama," "dama" is a thing or an experience in this

145
00:09:28,660 --> 00:09:31,640
 case, arises in the present.

146
00:09:31,640 --> 00:09:35,000
 "Tatta tatta vipassati," "vipassati."

147
00:09:35,000 --> 00:09:36,840
 Do you hear this word?

148
00:09:36,840 --> 00:09:39,840
 "Vipassati" is where we get the word "vipassana" from.

149
00:09:39,840 --> 00:09:44,360
 "Vipassana" means insight or it means seeing clearly.

150
00:09:44,360 --> 00:09:49,040
 "Vipassati" means he or she sees clearly.

151
00:09:49,040 --> 00:09:52,720
 It's the verb form.

152
00:09:52,720 --> 00:09:56,280
 So when someone doesn't go back to the past or the future,

153
00:09:56,280 --> 00:09:58,200
 one sees what's in the present

154
00:09:58,200 --> 00:10:01,080
 clearly.

155
00:10:01,080 --> 00:10:04,320
 Whatever arises in the present.

156
00:10:04,320 --> 00:10:05,800
 That's the essence of our practice.

157
00:10:05,800 --> 00:10:08,400
 That should be clear to everyone.

158
00:10:08,400 --> 00:10:14,110
 But I think it bears mentioning and it bears reaffirming

159
00:10:14,110 --> 00:10:16,640
 how powerful that is.

160
00:10:16,640 --> 00:10:24,000
 And the far reaching implications of it.

161
00:10:24,000 --> 00:10:30,650
 That actually every problem that we have becomes an

162
00:10:30,650 --> 00:10:32,720
 experience.

163
00:10:32,720 --> 00:10:37,760
 It really is applicable everywhere.

164
00:10:37,760 --> 00:10:39,640
 Someone's beating you with a stick.

165
00:10:39,640 --> 00:10:41,760
 You really can be mindful.

166
00:10:41,760 --> 00:10:44,720
 If you're present, it's not really a problem.

167
00:10:44,720 --> 00:10:47,660
 But if you're worrying about when they're going to hit you

168
00:10:47,660 --> 00:10:50,560
 next or if you're sad or

169
00:10:50,560 --> 00:10:54,280
 angry or upset about when they just hit you.

170
00:10:54,280 --> 00:10:56,910
 Meaning if you're upset by this experience, that's what

171
00:10:56,910 --> 00:10:57,960
 causes suffering.

172
00:10:57,960 --> 00:11:04,520
 If you're caught up in the past and the future.

173
00:11:04,520 --> 00:11:07,810
 When you're really in the present moment, there's only dham

174
00:11:07,810 --> 00:11:09,720
ma, there's only experience.

175
00:11:09,720 --> 00:11:13,270
 If you lose your job, if you get kicked out of your

176
00:11:13,270 --> 00:11:15,800
 apartment, if you're living on the

177
00:11:15,800 --> 00:11:20,680
 street, if you're starving to death.

178
00:11:20,680 --> 00:11:25,150
 It's a wonderful thing about Buddhism and about this idea

179
00:11:25,150 --> 00:11:27,920
 of the present moment is that

180
00:11:27,920 --> 00:11:30,600
 it's never going to end.

181
00:11:30,600 --> 00:11:35,240
 There's no failure in that sense.

182
00:11:35,240 --> 00:11:38,600
 It's not like you can do anything wrong or it's not like

183
00:11:38,600 --> 00:11:40,720
 you can do anything irrevocably

184
00:11:40,720 --> 00:11:44,080
 wrong.

185
00:11:44,080 --> 00:11:47,550
 If you mess up this life really bad, then just come back in

186
00:11:47,550 --> 00:11:48,680
 the next life.

187
00:11:48,680 --> 00:11:53,050
 I remember talking, I mentioned this, I think talking to a

188
00:11:53,050 --> 00:11:55,180
 friend of mine who had taken

189
00:11:55,180 --> 00:12:01,430
 a Buddhism course with me in university many years ago

190
00:12:01,430 --> 00:12:04,480
 before I was a monk, I think.

191
00:12:04,480 --> 00:12:08,830
 She took the Buddhism course and she said, she was Catholic

192
00:12:08,830 --> 00:12:12,400
 and she said, "It's incredible."

193
00:12:12,400 --> 00:12:16,600
 This idea of rebirth, the idea that you could have another

194
00:12:16,600 --> 00:12:19,040
 chance because in Christianity

195
00:12:19,040 --> 00:12:27,220
 and in Catholicism, this is it, one chance, do it right or

196
00:12:27,220 --> 00:12:30,920
 go to hell for eternity.

197
00:12:30,920 --> 00:12:33,320
 In Buddhism, hell is internal.

198
00:12:33,320 --> 00:12:38,400
 There's always a second chance.

199
00:12:38,400 --> 00:12:45,680
 There's power of not fearing the future, not fearing

200
00:12:45,680 --> 00:12:50,600
 consequences, learning to be what

201
00:12:50,600 --> 00:12:54,300
 we say it would be says next, "Asang hirang asang kupang,"

202
00:12:54,300 --> 00:12:56,320
 this is invincible, this is

203
00:12:56,320 --> 00:12:59,320
 unshakable.

204
00:12:59,320 --> 00:13:07,340
 "Tangvidwa mannubruhi," let him know that and be sure of it

205
00:13:07,340 --> 00:13:11,880
, invincibly, unshakably.

206
00:13:11,880 --> 00:13:14,760
 Present moment is invincible, it's unshakable.

207
00:13:14,760 --> 00:13:22,550
 There's no experience, no situation, no conflict that can't

208
00:13:22,550 --> 00:13:26,800
 be solved really, vanquished,

209
00:13:26,800 --> 00:13:32,560
 defeated, conquered by just being mindful.

210
00:13:32,560 --> 00:13:35,320
 It's the difference between trying to change the world

211
00:13:35,320 --> 00:13:36,960
 around you and learning to dance

212
00:13:36,960 --> 00:13:43,520
 with it, to be flexible with it, to roll with it, to stop

213
00:13:43,520 --> 00:13:46,560
 reacting to it really.

214
00:13:46,560 --> 00:13:50,440
 So that's the first half.

215
00:13:50,440 --> 00:13:53,700
 We're halfway there, halfway to having a good day.

216
00:13:53,700 --> 00:14:00,700
 The second half, "Ajjwa kitamata pangko janyamaranansuwe,"

217
00:14:00,700 --> 00:14:00,700
 well this isn't exactly advice, this is an

218
00:14:00,700 --> 00:14:05,720
 admonishment, a reminder.

219
00:14:05,720 --> 00:14:09,780
 Most of the second half is more of a reminder.

220
00:14:09,780 --> 00:14:13,740
 Today, and this is something we remind ourselves of, "Ajjwa

221
00:14:13,740 --> 00:14:15,960
 kitamata pang," today is when we

222
00:14:15,960 --> 00:14:17,600
 should do the work.

223
00:14:17,600 --> 00:14:21,200
 So I just said you always have a second chance, but

224
00:14:21,200 --> 00:14:24,160
 eventually it's going to have to be done

225
00:14:24,160 --> 00:14:25,160
 today.

226
00:14:25,160 --> 00:14:31,860
 And in fact, if it's not done today, this today, today,

227
00:14:31,860 --> 00:14:35,480
 today, who knows when you'll

228
00:14:35,480 --> 00:14:38,270
 have another chance, you might die tomorrow, "kujanyamaran

229
00:14:38,270 --> 00:14:40,200
ansuwe," who knows whether death

230
00:14:40,200 --> 00:14:42,480
 might be even tomorrow.

231
00:14:42,480 --> 00:14:52,720
 "Nahino sangarangtena mahase nina matyuna," there is no

232
00:14:52,720 --> 00:14:57,160
 bargaining with him.

233
00:14:57,160 --> 00:15:04,160
 No bargain with death, together with his great armies, or

234
00:15:04,160 --> 00:15:07,160
 death's great armies.

235
00:15:07,160 --> 00:15:12,580
 I don't know what the great army of death is, I'm not sure

236
00:15:12,580 --> 00:15:17,120
 of that mythology, but mahase

237
00:15:17,120 --> 00:15:20,990
 nina, I have to look up in the commentaries, but death in

238
00:15:20,990 --> 00:15:24,560
 his hordes, death in his armies,

239
00:15:24,560 --> 00:15:27,400
 his army.

240
00:15:27,400 --> 00:15:33,120
 There's no bargaining with death, it's just imagery.

241
00:15:33,120 --> 00:15:41,060
 Maybe the hordes of death are disease, illness, old age,

242
00:15:41,060 --> 00:15:46,560
 injury, war, murder, those are maybe

243
00:15:46,560 --> 00:15:50,320
 the armies of death.

244
00:15:50,320 --> 00:15:56,960
 You can die crossing the street, you can die from food, you

245
00:15:56,960 --> 00:16:00,600
 can die because some psychopath

246
00:16:00,600 --> 00:16:21,560
 decides to stab you, or blow you up, or shoot you.

247
00:16:21,560 --> 00:16:24,500
 You do it now, you don't know when you'll get another

248
00:16:24,500 --> 00:16:25,200
 chance.

249
00:16:25,200 --> 00:16:29,730
 I mean, that's not even the most important reason, you do

250
00:16:29,730 --> 00:16:32,000
 it now because it's the best

251
00:16:32,000 --> 00:16:36,440
 thing you could possibly do.

252
00:16:36,440 --> 00:16:38,510
 Being in the present moment, there's no reason to do

253
00:16:38,510 --> 00:16:41,040
 anything else, to be anything else.

254
00:16:41,040 --> 00:16:44,400
 The past doesn't make you happy, the future doesn't make

255
00:16:44,400 --> 00:16:45,240
 you happy.

256
00:16:45,240 --> 00:16:48,560
 Clinging to things certainly doesn't make you happy.

257
00:16:48,560 --> 00:16:52,970
 Wishing or wanting for things, fighting, all the things

258
00:16:52,970 --> 00:16:54,920
 that we waste our time with, how

259
00:16:54,920 --> 00:16:57,880
 much time do we waste fighting with each other, holding gr

260
00:16:57,880 --> 00:17:00,160
udges, bickering, you wouldn't believe

261
00:17:00,160 --> 00:17:03,920
 some of the things that go on in Buddhist monasteries and

262
00:17:03,920 --> 00:17:05,880
 even meditation centers.

263
00:17:05,880 --> 00:17:11,000
 How we waste our time and energy fighting.

264
00:17:11,000 --> 00:17:16,800
 As I've said, it's understandable, everyone has defilements

265
00:17:16,800 --> 00:17:19,960
, even people who are meditating

266
00:17:19,960 --> 00:17:20,960
 get upset.

267
00:17:20,960 --> 00:17:26,650
 But we should remind ourselves not to turn it into conflict

268
00:17:26,650 --> 00:17:29,400
, not to waste our time with

269
00:17:29,400 --> 00:17:33,390
 these things, we don't know when we're going to die, we

270
00:17:33,390 --> 00:17:35,800
 have this great opportunity.

271
00:17:35,800 --> 00:17:46,160
 "I wang wiharigatapim," dwelling thus ardently.

272
00:17:46,160 --> 00:17:49,600
 All the ardor you're putting out here, is that the right

273
00:17:49,600 --> 00:17:50,200
 word?

274
00:17:50,200 --> 00:17:55,000
 All the energy and effort the meditators are putting out.

275
00:17:55,000 --> 00:18:04,010
 This is the greatness, "Hohora tam matanditam," both day

276
00:18:04,010 --> 00:18:08,640
 and night, relentlessly.

277
00:18:08,640 --> 00:18:11,720
 So some of you have been practicing late into the night,

278
00:18:11,720 --> 00:18:17,640
 maybe even not sleeping at night,

279
00:18:17,640 --> 00:18:20,840
 doing great work.

280
00:18:20,840 --> 00:18:24,470
 And those who have put this effort out, if you've listened

281
00:18:24,470 --> 00:18:26,120
 to some of the people who've

282
00:18:26,120 --> 00:18:31,210
 gone through these courses, how incredible the

283
00:18:31,210 --> 00:18:35,320
 transformation can be in some cases.

284
00:18:35,320 --> 00:18:54,320
 And how such people can affirm, "Yes, they had a good day."

285
00:18:54,320 --> 00:19:01,710
 The peaceful sage has called this one who has had a good

286
00:19:01,710 --> 00:19:04,800
 night or a good day.

287
00:19:04,800 --> 00:19:08,610
 So it's not a very complex teaching, it happens to be one

288
00:19:08,610 --> 00:19:10,920
 of the most important ones, to stay

289
00:19:10,920 --> 00:19:15,520
 in the present moment, to not go back to the past or head

290
00:19:15,520 --> 00:19:18,120
 to the future, to do your work

291
00:19:18,120 --> 00:19:24,120
 today, to remember that now is when the work must be done.

292
00:19:24,120 --> 00:19:28,680
 Right now, listening to me, are you mindful?

293
00:19:28,680 --> 00:19:32,840
 Are you here or are you in the past, in the future?

294
00:19:32,840 --> 00:19:37,480
 Are you caught up in concepts and illusions?

295
00:19:37,480 --> 00:19:44,490
 Or are you clearly aware of reality as it's happening every

296
00:19:44,490 --> 00:19:45,840
 moment?

297
00:19:45,840 --> 00:19:51,060
 There's only one way, there's only one thing to do, having

298
00:19:51,060 --> 00:19:53,560
 a good day, it's not that hard

299
00:19:53,560 --> 00:19:55,560
 to do.

300
00:19:55,560 --> 00:20:01,280
 Or it's not that complicated, it's actually something very

301
00:20:01,280 --> 00:20:02,800
 challenging.

302
00:20:02,800 --> 00:20:08,790
 But it's the work that is worth doing, it's the task that

303
00:20:08,790 --> 00:20:11,880
 is worth accomplishing.

304
00:20:11,880 --> 00:20:16,910
 So much appreciation to our meditators here who are working

305
00:20:16,910 --> 00:20:19,640
 diligently everywhere, some

306
00:20:19,640 --> 00:20:23,160
 in the, here in the main hall, some in the room, some

307
00:20:23,160 --> 00:20:23,960
 outside.

308
00:20:23,960 --> 00:20:29,960
 But so many people wanting to come, Javan's going to have

309
00:20:29,960 --> 00:20:32,800
 to move into a tent soon.

310
00:20:32,800 --> 00:20:36,440
 It's not a joke, he actually is.

311
00:20:36,440 --> 00:20:39,840
 And jealous kind of, I used to live in a tent.

312
00:20:39,840 --> 00:20:42,210
 Anyway, that's the dama for tonight, thank you all for

313
00:20:42,210 --> 00:20:42,880
 coming up.

314
00:20:42,880 --> 00:21:05,640
 We got the questions up now.

315
00:21:05,640 --> 00:21:16,680
 Can you clarify your statement yesterday?

316
00:21:16,680 --> 00:21:19,880
 You can go, you all don't have to stick around for this.

317
00:21:19,880 --> 00:21:23,680
 Can you clarify your statement yesterday that the second

318
00:21:23,680 --> 00:21:25,880
 way to have mindfulness go wrong

319
00:21:25,880 --> 00:21:28,770
 is to have mindfulness of a concept in light of the fact

320
00:21:28,770 --> 00:21:30,880
 that we have traditional meditations

321
00:21:30,880 --> 00:21:35,800
 on concepts such as mindfulness of the Buddha.

322
00:21:35,800 --> 00:21:39,840
 So I thought I made it clear, and I thought we talked about

323
00:21:39,840 --> 00:21:41,360
 this afterwards.

324
00:21:41,360 --> 00:21:46,710
 In fact it seems to me this question came up, that it's not

325
00:21:46,710 --> 00:21:49,200
 wrong in a general sense,

326
00:21:49,200 --> 00:21:56,240
 it's wrong, it's wrong for cultivating insight.

327
00:21:56,240 --> 00:22:03,200
 It's like, if you want to go to Bangkok, you go this way.

328
00:22:03,200 --> 00:22:04,200
 Toronto's better.

329
00:22:04,200 --> 00:22:09,120
 If you want to go to Toronto, you got to take the 403.

330
00:22:09,120 --> 00:22:13,360
 Don't get on the QEW to Niagara, it's the wrong way.

331
00:22:13,360 --> 00:22:16,610
 Doesn't mean there's anything wrong with the way to Niagara

332
00:22:16,610 --> 00:22:16,720
.

333
00:22:16,720 --> 00:22:19,580
 Way to Niagara is a perfectly fine highway, but it won't

334
00:22:19,580 --> 00:22:20,800
 get you to Toronto.

335
00:22:20,800 --> 00:22:25,920
 That's what it means by wrong.

336
00:22:25,920 --> 00:22:29,600
 When you're practicing insight, concepts are wrong because

337
00:22:29,600 --> 00:22:30,400
 they won't get you where you're

338
00:22:30,400 --> 00:22:31,800
 trying to go.

339
00:22:31,800 --> 00:22:37,010
 What are your thoughts on meditating on loving kindness and

340
00:22:37,010 --> 00:22:38,480
 compassion?

341
00:22:38,480 --> 00:22:39,480
 I think they're good.

342
00:22:39,480 --> 00:22:43,850
 I wouldn't take it as my primary meditation, though you

343
00:22:43,850 --> 00:22:44,600
 could.

344
00:22:44,600 --> 00:22:47,440
 But I think I've talked about this actually before.

345
00:22:47,440 --> 00:22:49,000
 They're supportive meditations.

346
00:22:49,000 --> 00:22:52,120
 They're useful to support insight meditation.

347
00:22:52,120 --> 00:22:57,000
 I think, yes, it's good to practice some every day.

348
00:22:57,000 --> 00:23:03,600
 Is there a way to change a username?

349
00:23:03,600 --> 00:23:06,040
 I have to talk to our IT people.

350
00:23:06,040 --> 00:23:09,440
 You can send them an email.

351
00:23:09,440 --> 00:23:18,120
 Luckily, I'm blessed with a really awesome IT team now.

352
00:23:18,120 --> 00:23:20,440
 We've got a real ... and just a great team in general.

353
00:23:20,440 --> 00:23:24,520
 Shout out to our volunteer community who's put together

354
00:23:24,520 --> 00:23:26,880
 this lovely website and so much

355
00:23:26,880 --> 00:23:31,000
 more and just keeps this place running.

356
00:23:31,000 --> 00:23:32,800
 At this point, I couldn't do it alone anymore.

357
00:23:32,800 --> 00:23:39,350
 It's a group effort of real impressive proportions and just

358
00:23:39,350 --> 00:23:40,720
 growing.

359
00:23:40,720 --> 00:23:41,720
 Keep it up.

360
00:23:41,720 --> 00:23:42,720
 Thank you, everyone.

361
00:23:42,720 --> 00:23:53,520
 Thank you.

362
00:23:53,520 --> 00:23:56,160
 Is this normal or am I doing something wrong?

363
00:23:56,160 --> 00:23:57,160
 Okay.

364
00:23:57,160 --> 00:23:59,840
 We should have a note at the top.

365
00:23:59,840 --> 00:24:04,400
 Please do not ask if X is normal.

366
00:24:04,400 --> 00:24:07,720
 It's not useful to ask whether something is normal.

367
00:24:07,720 --> 00:24:09,260
 You have an experience.

368
00:24:09,260 --> 00:24:12,500
 Rather than asking me whether it's normal, try and figure

369
00:24:12,500 --> 00:24:14,080
 out what to do about it, how

370
00:24:14,080 --> 00:24:17,040
 you should relate to it.

371
00:24:17,040 --> 00:24:21,060
 As I said, your real question should be, "Is it ... " When

372
00:24:21,060 --> 00:24:21,920
 you say, "Am I doing something

373
00:24:21,920 --> 00:24:23,780
 wrong?"

374
00:24:23,780 --> 00:24:28,360
 Your real question should be, "There you go.

375
00:24:28,360 --> 00:24:29,360
 Am I doing something wrong?

376
00:24:29,360 --> 00:24:30,360
 Is this wrong?

377
00:24:30,360 --> 00:24:33,640
 Is this a bad state?"

378
00:24:33,640 --> 00:24:37,730
 I can answer that for you, but it's not really where we go

379
00:24:37,730 --> 00:24:40,080
 to tell you whether something

380
00:24:40,080 --> 00:24:41,240
 is wrong or right.

381
00:24:41,240 --> 00:24:48,920
 In fact, in the end, it's not even a valid question.

382
00:24:48,920 --> 00:24:50,920
 The only thing that is right is mindfulness.

383
00:24:50,920 --> 00:24:55,100
 If you're mindful about the state that you're in, how you

384
00:24:55,100 --> 00:24:57,800
 see things differently, then you'll

385
00:24:57,800 --> 00:25:01,720
 let it go because you're potentially conceited about it.

386
00:25:01,720 --> 00:25:04,240
 You get conceited thinking, "Hey, look at me.

387
00:25:04,240 --> 00:25:09,100
 I'm progressing, feeling good about yourself, confident,"

388
00:25:09,100 --> 00:25:12,320
 which can be misleading and misguided.

389
00:25:12,320 --> 00:25:22,120
 You'd like this new perspective and so on.

390
00:25:22,120 --> 00:25:25,680
 I'm not going to tell you whether it's right or wrong.

391
00:25:25,680 --> 00:25:30,580
 I want you to be mindful of it and learn to let it go.

392
00:25:30,580 --> 00:25:35,400
 Don't let go of the good things.

393
00:25:35,400 --> 00:25:42,730
 Again, we don't have ... Someone wanting me to give their

394
00:25:42,730 --> 00:25:44,920
 thoughts, "Do you want me to

395
00:25:44,920 --> 00:25:48,080
 tell you whether this is normal?"

396
00:25:48,080 --> 00:25:50,800
 Same advice.

397
00:25:50,800 --> 00:25:54,240
 I'm not going to tell you one way or the other.

398
00:25:54,240 --> 00:25:55,240
 Look at it.

399
00:25:55,240 --> 00:25:56,880
 Be mindful of it and you'll see what leads to suffering and

400
00:25:56,880 --> 00:25:57,640
 you'll see what leads to

401
00:25:57,640 --> 00:25:58,800
 happiness.

402
00:25:58,800 --> 00:26:02,800
 When one is fully mindful, are there any emotions?

403
00:26:02,800 --> 00:26:06,000
 Yes, there can be.

404
00:26:06,000 --> 00:26:08,160
 There aren't always, but there can be.

405
00:26:08,160 --> 00:26:11,220
 I mean, it really depends what you mean by emotion because

406
00:26:11,220 --> 00:26:12,640
 that's a Western word.

407
00:26:12,640 --> 00:26:15,840
 We don't use that word in Buddhism.

408
00:26:15,840 --> 00:26:17,200
 Don't have such a word really.

409
00:26:17,200 --> 00:26:26,960
 I mean, maybe I could think of some, but not really.

410
00:26:26,960 --> 00:26:30,400
 There are various states of mind.

411
00:26:30,400 --> 00:26:33,880
 Regards to wrong meditation.

412
00:26:33,880 --> 00:26:36,450
 In walking meditation, I have a tendency to keep the mantra

413
00:26:36,450 --> 00:26:37,240
 on autopilot.

414
00:26:37,240 --> 00:26:39,240
 My mind is drifting elsewhere.

415
00:26:39,240 --> 00:26:44,360
 I devise a new scheme.

416
00:26:44,360 --> 00:26:45,740
 Schemes are bad.

417
00:26:45,740 --> 00:26:49,010
 Schemes are bad from the outright because you're trying to

418
00:26:49,010 --> 00:26:52,280
 fix and that's not our goal.

419
00:26:52,280 --> 00:26:54,400
 Your mind drifts elsewhere.

420
00:26:54,400 --> 00:26:56,680
 Mind drifting elsewhere is teaching you something.

421
00:26:56,680 --> 00:26:59,960
 It's teaching you non-self that you're not in control.

422
00:26:59,960 --> 00:27:02,240
 It's frustrating.

423
00:27:02,240 --> 00:27:07,000
 That frustration and the exploration, there's no quick way.

424
00:27:07,000 --> 00:27:09,800
 You have to study that, that distraction.

425
00:27:09,800 --> 00:27:13,100
 You have to be meticulous about it and catch it every time

426
00:27:13,100 --> 00:27:15,000
 again and again and stop trying

427
00:27:15,000 --> 00:27:16,520
 to fix it.

428
00:27:16,520 --> 00:27:21,080
 Stop trying to stop it from happening.

429
00:27:21,080 --> 00:27:26,340
 How it stops happening is when you let go of that which is

430
00:27:26,340 --> 00:27:27,760
 causing it.

431
00:27:27,760 --> 00:27:31,260
 Some kind of, potentially some kind of attachment or

432
00:27:31,260 --> 00:27:34,080
 curiosity about whatever it is that's causing

433
00:27:34,080 --> 00:27:36,520
 you to drift.

434
00:27:36,520 --> 00:27:39,640
 Sometimes it's just laziness.

435
00:27:39,640 --> 00:27:43,190
 Desire to just stop working so hard so you just go on autop

436
00:27:43,190 --> 00:27:45,040
ilots more comfortable.

437
00:27:45,040 --> 00:27:46,360
 That kind of thing.

438
00:27:46,360 --> 00:27:49,320
 Look at those states.

439
00:27:49,320 --> 00:27:50,320
 Learn about them.

440
00:27:50,320 --> 00:27:57,280
 Once you see them clearly, you'll let them go.

441
00:27:57,280 --> 00:27:59,950
 How much solo practice do you recommend before coming to

442
00:27:59,950 --> 00:28:01,720
 your meditation course for the most

443
00:28:01,720 --> 00:28:02,720
 benefit?

444
00:28:02,720 --> 00:28:05,400
 Is there anything else that should be done beforehand?

445
00:28:05,400 --> 00:28:09,450
 The best would be to do an online course if you have time

446
00:28:09,450 --> 00:28:11,120
 to spend some time.

447
00:28:11,120 --> 00:28:14,200
 We meet once a week and we have a schedule on this site.

448
00:28:14,200 --> 00:28:16,960
 If you look and find it under the menu, you can reserve a

449
00:28:16,960 --> 00:28:19,240
 spot and then just call me up

450
00:28:19,240 --> 00:28:23,920
 using Google Hangouts and we can talk every week.

451
00:28:23,920 --> 00:28:28,320
 Practicing solo, it's only the first exercise.

452
00:28:28,320 --> 00:28:32,470
 What I give in the booklet is only the first step in this

453
00:28:32,470 --> 00:28:33,560
 practice.

454
00:28:33,560 --> 00:28:37,800
 It's more fruitful generally to actually do a course and we

455
00:28:37,800 --> 00:28:39,440
 can do that online.

456
00:28:39,440 --> 00:28:43,960
 Other than that, there's no ... Other than that, come and

457
00:28:43,960 --> 00:28:45,520
 do a course as soon as you

458
00:28:45,520 --> 00:28:49,830
 can because the course is the best way to gain a good

459
00:28:49,830 --> 00:28:51,240
 foundation.

460
00:28:51,240 --> 00:28:54,980
 Should I go into a meditation center when having severe

461
00:28:54,980 --> 00:28:56,920
 stiffness and tension in my

462
00:28:56,920 --> 00:28:59,920
 back and neck?

463
00:28:59,920 --> 00:29:04,960
 I mean, yes.

464
00:29:04,960 --> 00:29:09,920
 Find a meditation center that can accommodate that.

465
00:29:09,920 --> 00:29:12,720
 That gives you a chair or allows you to sit in a chair or

466
00:29:12,720 --> 00:29:14,280
 even lie down or sit against

467
00:29:14,280 --> 00:29:16,400
 the wall or that kind of thing.

468
00:29:16,400 --> 00:29:18,440
 The body position is not that important.

469
00:29:18,440 --> 00:29:23,080
 It's much more important what's going on in the mind.

470
00:29:23,080 --> 00:29:27,380
 We have a woman here right now who has muscular dystrophy

471
00:29:27,380 --> 00:29:29,680
 and she uses a walker to do her

472
00:29:29,680 --> 00:29:30,680
 walking.

473
00:29:30,680 --> 00:29:33,040
 I said, "Dora, I'm going to use her as an example."

474
00:29:33,040 --> 00:29:35,290
 My teacher would always drag us out, the foreigners, and

475
00:29:35,290 --> 00:29:37,640
 say, "See if they can do it.

476
00:29:37,640 --> 00:29:40,640
 These people from other countries come here and do it.

477
00:29:40,640 --> 00:29:43,560
 You Thai people can do it as well."

478
00:29:43,560 --> 00:29:45,120
 I'll use her as an example.

479
00:29:45,120 --> 00:29:48,600
 I'd like to have her come here and talk about her practice.

480
00:29:48,600 --> 00:29:50,080
 She's done great.

481
00:29:50,080 --> 00:29:53,440
 Finish the first course onto the second course.

482
00:29:53,440 --> 00:30:00,080
 If she can do it, you can.

483
00:30:00,080 --> 00:30:02,120
 This talk you mentioned a psychopath shoots you.

484
00:30:02,120 --> 00:30:05,800
 A psychopathy, a result of past karmic actions, a result of

485
00:30:05,800 --> 00:30:07,360
 unfortunate injury.

486
00:30:07,360 --> 00:30:08,360
 I'm not sure.

487
00:30:08,360 --> 00:30:09,360
 I don't really know.

488
00:30:09,360 --> 00:30:12,320
 Psychopathy is just a word.

489
00:30:12,320 --> 00:30:14,240
 They say a person who has no feelings.

490
00:30:14,240 --> 00:30:16,960
 There's been studies that show, I did a little research,

491
00:30:16,960 --> 00:30:18,520
 show that they actually do have

492
00:30:18,520 --> 00:30:19,520
 feelings.

493
00:30:19,520 --> 00:30:22,600
 They're just repressed or the brain is in such a way that

494
00:30:22,600 --> 00:30:24,160
 it's very weak and that kind

495
00:30:24,160 --> 00:30:28,520
 of thing.

496
00:30:28,520 --> 00:30:29,520
 What is psychopathy?

497
00:30:29,520 --> 00:30:30,520
 I don't know.

498
00:30:30,520 --> 00:30:33,990
 I'd imagine there are various things that are labeled by

499
00:30:33,990 --> 00:30:36,080
 psychiatrists or psychotherapists

500
00:30:36,080 --> 00:30:42,280
 as psychopathy.

501
00:30:42,280 --> 00:30:46,960
 They differentiate from psychosis and neurosis.

502
00:30:46,960 --> 00:30:50,080
 Psychosis is thought to be that which is biological.

503
00:30:50,080 --> 00:30:51,800
 It's a problem with the brain.

504
00:30:51,800 --> 00:30:55,200
 Neurosis is something mental, something that's learned.

505
00:30:55,200 --> 00:30:57,200
 They try to differentiate.

506
00:30:57,200 --> 00:30:59,320
 I would say it's a combination.

507
00:30:59,320 --> 00:31:04,820
 Anyway, it's a fairly speculative question, so I wouldn't

508
00:31:04,820 --> 00:31:07,320
 worry about it too much.

509
00:31:07,320 --> 00:31:12,320
 "Cancel talking, dear Park, due to study group on Friday."

510
00:31:12,320 --> 00:31:13,320
 Is this today Friday?

511
00:31:13,320 --> 00:31:15,080
 Oh, are we supposed to have a meeting today?

512
00:31:15,080 --> 00:31:16,840
 Our study group today?

513
00:31:16,840 --> 00:31:20,520
 Wait, what's our schedule?

514
00:31:20,520 --> 00:31:23,560
 Did I miss study group today?

515
00:31:23,560 --> 00:31:24,560
 I thought Sunday.

516
00:31:24,560 --> 00:31:25,720
 We're going to do one more Sunday.

517
00:31:25,720 --> 00:31:30,520
 I didn't even know it was Friday.

518
00:31:30,520 --> 00:31:35,280
 There you go, living in the present.

519
00:31:35,280 --> 00:31:38,470
 It's a problem with a problem for keeping appointments, I

520
00:31:38,470 --> 00:31:39,200
 suppose.

521
00:31:39,200 --> 00:31:42,260
 Well, you guys better let me know what our schedule for

522
00:31:42,260 --> 00:31:42,880
 that is.

523
00:31:42,880 --> 00:31:48,240
 Otherwise, I guess from now on, no more Fridays.

524
00:31:48,240 --> 00:31:51,240
 Oh.

525
00:31:51,240 --> 00:32:01,640
 There's supposed to be a

526
00:32:01,640 --> 00:32:06,280
 meeting today.

527
00:32:06,280 --> 00:32:07,280
 I'm sorry.

528
00:32:07,280 --> 00:32:08,280
 No, let's do next Friday then.

529
00:32:08,280 --> 00:32:09,280
 Take another break.

530
00:32:09,280 --> 00:32:10,280
 Sorry.

531
00:32:10,280 --> 00:32:16,800
 "How to deal with sub-perceptible sensations."

532
00:32:16,800 --> 00:32:20,900
 It feels like I have a lot crammed down from poor habits in

533
00:32:20,900 --> 00:32:21,880
 the past.

534
00:32:21,880 --> 00:32:25,960
 I don't understand.

535
00:32:25,960 --> 00:32:29,440
 Patience, I wouldn't worry about it.

536
00:32:29,440 --> 00:32:31,600
 Whatever arises, be mindful of that.

537
00:32:31,600 --> 00:32:38,640
 I'm really sorry about the VCD manga.

538
00:32:38,640 --> 00:32:51,360
 I'm clearly not very good at appointments.

539
00:32:51,360 --> 00:32:55,920
 I think that's enough for tonight, so try again next week.

540
00:32:55,920 --> 00:32:56,920
 Apologies.

541
00:32:56,920 --> 00:33:01,040
 Have a good night, everyone.

542
00:33:01,040 --> 00:33:02,640
 Thank you.

