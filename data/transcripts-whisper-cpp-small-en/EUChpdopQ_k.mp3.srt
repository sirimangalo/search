1
00:00:00,000 --> 00:00:05,000
 Good evening everyone.

2
00:00:05,000 --> 00:00:27,800
 In broadcasting live March 31st.

3
00:00:27,800 --> 00:00:32,800
 Today's quote from the young good.

4
00:00:32,800 --> 00:00:38,800
 Baba sarangi dang bhikkha vee jitang.

5
00:00:38,800 --> 00:00:45,800
 Luminous or radiant is this mind.

6
00:00:45,800 --> 00:00:59,800
 Tanjuku ho aa gantuke hi upa kirei se hi upa kirei dang.

7
00:00:59,800 --> 00:01:10,580
 It is defiled by visiting defilements or incoming defile

8
00:01:10,580 --> 00:01:10,800
ments.

9
00:01:10,800 --> 00:01:18,800
 That come from without. It's a good explanation.

10
00:01:18,800 --> 00:01:22,800
 Agantuka is that which comes.

11
00:01:22,800 --> 00:01:25,800
 Agantuka is often used to talk about a guest.

12
00:01:25,800 --> 00:01:31,800
 Monks are called agantuka when they come.

13
00:01:31,800 --> 00:01:36,800
 Agantuka. Agantu.

14
00:01:36,800 --> 00:01:40,800
 Not quite sure actually.

15
00:01:40,800 --> 00:01:48,800
 Dang asutva putudjano yata bhutang napajanati.

16
00:01:48,800 --> 00:01:56,800
 We have this phrase asutva putudjano yata bhutudjano.

17
00:01:56,800 --> 00:01:59,800
 A uninstructed world thing.

18
00:01:59,800 --> 00:02:03,800
 It's a phrase that the Buddha uses often.

19
00:02:03,800 --> 00:02:11,800
 Talk about those who have never looked at their minds.

20
00:02:11,800 --> 00:02:15,800
 Never thought about spiritual teachings.

21
00:02:15,800 --> 00:02:23,080
 Maybe not even spiritual teachings so much as those who

22
00:02:23,080 --> 00:02:28,800
 never thought about the things that the Buddha taught.

23
00:02:28,800 --> 00:02:32,110
 Who have never looked at their own minds or learned

24
00:02:32,110 --> 00:02:33,800
 anything.

25
00:02:33,800 --> 00:02:36,800
 Who spend their time in all this worldly affairs.

26
00:02:36,800 --> 00:02:43,800
 Usus asutva, someone who has not heard the truth.

27
00:02:43,800 --> 00:02:49,800
 Putudjano, one who is full of defilements.

28
00:02:49,800 --> 00:02:55,180
 Yata bhutang napajanati. They don't understand this as it

29
00:02:55,180 --> 00:02:55,800
 is.

30
00:02:55,800 --> 00:03:04,160
 So people think that the mind is or that their person is

31
00:03:04,160 --> 00:03:05,800
 static.

32
00:03:05,800 --> 00:03:12,760
 We think of our characters, characteristics, our character

33
00:03:12,760 --> 00:03:14,800
 types as fixed.

34
00:03:14,800 --> 00:03:18,800
 So I say I'm an angry person or I get angry at this.

35
00:03:18,800 --> 00:03:22,500
 Someone told me just today, really nice person, but she

36
00:03:22,500 --> 00:03:23,800
 said I get.

37
00:03:23,800 --> 00:03:30,800
 I get ticked off at people's crap or something like that.

38
00:03:30,800 --> 00:03:33,080
 It was in the context of a bigger state of a larger

39
00:03:33,080 --> 00:03:38,800
 statement, but she mentioned that in passing that.

40
00:03:38,800 --> 00:03:41,800
 She can get frustrated at people.

41
00:03:41,800 --> 00:03:45,800
 And we say things like this, like this is how we talk.

42
00:03:45,800 --> 00:03:47,800
 And this is how we think.

43
00:03:47,800 --> 00:03:53,800
 We think of ourselves as having a certain character type.

44
00:03:53,800 --> 00:03:59,800
 So we think of the mind as being inherently idiosyncratic.

45
00:03:59,800 --> 00:04:05,560
 Buddha said no, no, the mind is luminous. The mind is pure,

46
00:04:05,560 --> 00:04:07,800
 basically.

47
00:04:07,800 --> 00:04:10,870
 And funny enough, people, there are groups that

48
00:04:10,870 --> 00:04:13,790
 misinterpret this text to mean that the mind is like a

49
00:04:13,790 --> 00:04:15,800
 glowing ball.

50
00:04:15,800 --> 00:04:18,800
 The Dhammakaya movement in Thailand, I heard the Vai Sabat.

51
00:04:18,800 --> 00:04:23,750
 I sat with him for five hours while he explained to a small

52
00:04:23,750 --> 00:04:27,800
 group of us about what this passage means.

53
00:04:27,800 --> 00:04:31,000
 And it's that the mind is a bright light, which is coinc

54
00:04:31,000 --> 00:04:34,800
identally the meditation object that they use.

55
00:04:34,800 --> 00:04:37,800
 They imagine a bright light, a crystal.

56
00:04:37,800 --> 00:04:40,800
 They say the mind is luminous.

57
00:04:40,800 --> 00:04:44,800
 So they take it to be a physical light that you can see.

58
00:04:44,800 --> 00:04:47,800
 Dhammakaya is meant here.

59
00:04:47,800 --> 00:04:54,800
 It is related to kilaisa, upakilesa, defilements.

60
00:04:54,800 --> 00:05:00,800
 So that's the first.

61
00:05:00,800 --> 00:05:02,800
 This is the ikkannipata.

62
00:05:02,800 --> 00:05:10,090
 So each sutra, you could say, is just a couple of sentences

63
00:05:10,090 --> 00:05:10,800
.

64
00:05:10,800 --> 00:05:13,790
 So that first part is the first sutra. It says bhattamam,

65
00:05:13,790 --> 00:05:15,800
 the first.

66
00:05:15,800 --> 00:05:17,800
 This is the first sutra, just that part.

67
00:05:17,800 --> 00:05:25,910
 And then the second sutra is just a inversion of it, where

68
00:05:25,910 --> 00:05:26,800
 he says,

69
00:05:26,800 --> 00:05:35,800
 sutavato ariya saba ka sajita bhavana ati wadami.

70
00:05:35,800 --> 00:05:37,800
 Oh, wait. Sorry.

71
00:05:37,800 --> 00:05:39,800
 I didn't finish the first one.

72
00:05:39,800 --> 00:05:43,910
 So an un-instructed ordinary worldling doesn't understand

73
00:05:43,910 --> 00:05:45,800
 this as it is.

74
00:05:45,800 --> 00:05:50,800
 Tasma asutavato bhutu jana sajita bhavana nati.

75
00:05:50,800 --> 00:05:56,550
 Therefore, for an un-instructed worldling, jita bhavana n

76
00:05:56,550 --> 00:05:56,800
ati,

77
00:05:56,800 --> 00:06:02,800
 there is no cultivation of the mind.

78
00:06:02,800 --> 00:06:04,800
 They do not cultivate their minds.

79
00:06:04,800 --> 00:06:08,800
 So that's not a literal translation.

80
00:06:08,800 --> 00:06:11,800
 A literal translation is, there is no, and it's in quotes.

81
00:06:11,800 --> 00:06:16,800
 Therefore, I say, wadami.

82
00:06:16,800 --> 00:06:22,500
 Therefore, I say, asutavato, for an un-instructed, putu j

83
00:06:22,500 --> 00:06:23,800
ana sa, worldling,

84
00:06:23,800 --> 00:06:30,680
 jita bhavana, the training of the mind, nati, doesn't exist

85
00:06:30,680 --> 00:06:30,800
.

86
00:06:30,800 --> 00:06:34,800
 You all know what the word nati means, right?

87
00:06:34,800 --> 00:06:39,800
 Nati is a famous, there's a story behind the word, right?

88
00:06:39,800 --> 00:06:46,320
 Nati jita bhavana, there is no jita bhavana, there is no bh

89
00:06:46,320 --> 00:06:48,800
avana for the jita.

90
00:06:48,800 --> 00:06:50,800
 And then the second one is the inversion.

91
00:06:50,800 --> 00:06:55,800
 So, sutang sutavah arihasavokohyata bhutang bhajanati.

92
00:06:55,800 --> 00:07:01,800
 That instructed student of the enlightened ones understands

93
00:07:01,800 --> 00:07:02,800
 it as it is,

94
00:07:02,800 --> 00:07:05,800
 yata bhutang bhajanati.

95
00:07:05,800 --> 00:07:11,800
 The sma, therefore, or so, from that, literally,

96
00:07:11,800 --> 00:07:17,800
 sutavato arihasavokahsa jita bhavana ati.

97
00:07:17,800 --> 00:07:26,230
 For a instructed student of the enlightened ones, there is

98
00:07:26,230 --> 00:07:26,800
 ati,

99
00:07:26,800 --> 00:07:33,270
 there is jita bhavana, the cultivation of the mind or the

100
00:07:33,270 --> 00:07:34,800
 development of the mind,

101
00:07:34,800 --> 00:07:42,800
 mental development, jita bhavana.

102
00:07:42,800 --> 00:07:45,800
 That's said, and it says dutiyang, which means the second,

103
00:07:45,800 --> 00:07:48,800
 and then it goes on, there's ten in this waga.

104
00:07:48,800 --> 00:07:53,930
 So, the nguetar nikai is sorted by wagas, which are like

105
00:07:53,930 --> 00:07:55,800
 chapters, sort of.

106
00:07:55,800 --> 00:08:01,800
 But they don't, they're usually chapters are,

107
00:08:01,800 --> 00:08:04,800
 well, sometimes they're sorted by subject.

108
00:08:04,800 --> 00:08:06,800
 Sometimes they're just packed in there.

109
00:08:06,800 --> 00:08:20,800
 Here, the third one is about meta jita, the mind of,

110
00:08:20,800 --> 00:08:27,800
 meta jita, achara, sangha, tamatampi.

111
00:08:27,800 --> 00:08:31,800
 Anyway, it's been a long day.

112
00:08:31,800 --> 00:08:37,800
 I had an exam this morning on peace,

113
00:08:37,800 --> 00:08:40,800
 which is always a good thing to be tested on.

114
00:08:40,800 --> 00:08:43,800
 But it was actually kind of disappointing.

115
00:08:43,800 --> 00:08:51,800
 The questions on it were surprising.

116
00:08:51,800 --> 00:08:58,960
 Questions were about, well, I guess it was just kind of

117
00:08:58,960 --> 00:08:59,800
 strange.

118
00:08:59,800 --> 00:09:01,800
 It wasn't the things that I'd studied.

119
00:09:01,800 --> 00:09:07,800
 We were asked for meetings that had led up to UN resolution

120
00:09:07,800 --> 00:09:10,800
 70.1, 70/1.

121
00:09:10,800 --> 00:09:15,380
 I wasn't paying attention to the names of the meetings and

122
00:09:15,380 --> 00:09:15,800
 some of which.

123
00:09:15,800 --> 00:09:20,800
 I guess I'm a little bit out of my element there

124
00:09:20,800 --> 00:09:23,800
 because I'm not really interested in worldly affairs.

125
00:09:23,800 --> 00:09:29,020
 I'm more interested in the content than the stuff that's

126
00:09:29,020 --> 00:09:29,800
 going on,

127
00:09:29,800 --> 00:09:31,800
 which is a fault of mine.

128
00:09:31,800 --> 00:09:34,930
 I can never really be a peace worker in the sense that they

129
00:09:34,930 --> 00:09:35,800
 want me to be.

130
00:09:35,800 --> 00:09:39,800
 They want us to be worldly peace workers, I think,

131
00:09:39,800 --> 00:09:42,800
 getting involved with governments and doing our civic duty,

132
00:09:42,800 --> 00:09:43,800
 I guess,

133
00:09:43,800 --> 00:09:47,800
 and getting involved.

134
00:09:47,800 --> 00:09:49,800
 Unfortunately, I'm not a citizen.

135
00:09:49,800 --> 00:09:53,800
 I'm out of society.

136
00:09:53,800 --> 00:09:57,490
 Then we had to write an essay about, we had one of three

137
00:09:57,490 --> 00:09:57,800
 topics,

138
00:09:57,800 --> 00:09:59,800
 and we're talking about happiness.

139
00:09:59,800 --> 00:10:12,800
 Which one did I talk about?

140
00:10:12,800 --> 00:10:14,800
 Bizarre.

141
00:10:14,800 --> 00:10:18,800
 I'm just wondering if I wrote about the wrong topic.

142
00:10:18,800 --> 00:10:23,800
 I wrote about UN resolution 70/1,

143
00:10:23,800 --> 00:10:27,800
 Sustainable Development Report, just because it was easy.

144
00:10:27,800 --> 00:10:29,800
 But I started thinking, you know,

145
00:10:29,800 --> 00:10:34,010
 this resolution doesn't really hit at the root of the

146
00:10:34,010 --> 00:10:34,800
 problem,

147
00:10:34,800 --> 00:10:39,800
 which is this quote.

148
00:10:39,800 --> 00:10:43,800
 The root of the problem is our minds are defiled.

149
00:10:43,800 --> 00:10:46,800
 There was no call for meditation.

150
00:10:46,800 --> 00:10:48,800
 How do we solve the world's problems?

151
00:10:48,800 --> 00:10:52,800
 Meditation wasn't on the list.

152
00:10:52,800 --> 00:10:59,800
 I was missing something.

153
00:10:59,800 --> 00:11:01,800
 And then I just finished my essay.

154
00:11:01,800 --> 00:11:02,800
 Why am I talking about this?

155
00:11:02,800 --> 00:11:04,800
 Because I wanted to talk about my essay.

156
00:11:04,800 --> 00:11:07,800
 So I did this essay.

157
00:11:07,800 --> 00:11:11,800
 And I think it's somewhat interesting.

158
00:11:11,800 --> 00:11:18,800
 It's a little too academic for my tastes.

159
00:11:18,800 --> 00:11:21,800
 Meaning like...

160
00:11:21,800 --> 00:11:23,800
 It's fine when I write for school.

161
00:11:23,800 --> 00:11:25,800
 It's a little bit artificial.

162
00:11:25,800 --> 00:11:28,800
 Like you kind of have to make it fit with the course.

163
00:11:28,800 --> 00:11:33,800
 And, well, then don't do any courses on meditation.

164
00:11:33,800 --> 00:11:37,370
 So I suppose I somehow play into their hands a little too

165
00:11:37,370 --> 00:11:37,800
 much.

166
00:11:37,800 --> 00:11:39,800
 I could probably have done a...

167
00:11:39,800 --> 00:11:41,800
 found a way to do an essay on meditation.

168
00:11:41,800 --> 00:11:46,800
 But, you know, East Asian Buddhism.

169
00:11:46,800 --> 00:11:49,800
 I was interested in Lotus Sutra.

170
00:11:49,800 --> 00:11:59,800
 So I'm interested in...

171
00:11:59,800 --> 00:12:01,800
 in making clear what is the Dharma

172
00:12:01,800 --> 00:12:07,800
 and what isn't the Dharma.

173
00:12:07,800 --> 00:12:13,800
 That you can't just get away with...

174
00:12:13,800 --> 00:12:15,800
 putting words in the Buddhist Nautama.

175
00:12:15,800 --> 00:12:17,800
 It's really kind of disgusted by Lotus Sutra.

176
00:12:17,800 --> 00:12:21,800
 For what it blatantly appears to do to the Buddha

177
00:12:21,800 --> 00:12:26,800
 and to Sariputra.

178
00:12:26,800 --> 00:12:29,800
 And to the Dharma in general.

179
00:12:29,800 --> 00:12:34,640
 How it just throws out the 45 years of the Buddha's

180
00:12:34,640 --> 00:12:35,800
 teaching.

181
00:12:35,800 --> 00:12:38,800
 And just makes bizarre claims.

182
00:12:38,800 --> 00:12:41,800
 I mean, of course there were lots of texts like this.

183
00:12:41,800 --> 00:12:44,800
 But the Lotus Sutra became famous.

184
00:12:44,800 --> 00:12:48,800
 And I'm looking at why it became famous.

185
00:12:48,800 --> 00:12:51,800
 So, why I'm talking about this...

186
00:12:51,800 --> 00:12:58,420
 because I thought maybe somebody out there would like to

187
00:12:58,420 --> 00:12:58,800
 read it.

188
00:12:58,800 --> 00:13:03,800
 And hopefully...

189
00:13:03,800 --> 00:13:05,800
 look at it for me.

190
00:13:05,800 --> 00:13:09,800
 And find typos and stuff.

191
00:13:09,800 --> 00:13:14,800
 So...

192
00:13:14,800 --> 00:13:16,800
 I don't want to put it like that on the internet.

193
00:13:16,800 --> 00:13:19,800
 Especially not when it's not finished.

194
00:13:19,800 --> 00:13:22,800
 And because it would just mean getting too many people

195
00:13:22,800 --> 00:13:24,800
 writing comments on it.

196
00:13:24,800 --> 00:13:26,800
 I've done that before.

197
00:13:26,800 --> 00:13:28,800
 And why did I get lots and lots of comments

198
00:13:28,800 --> 00:13:31,800
 that I had to just discard.

199
00:13:31,800 --> 00:13:34,800
 So I'm posting it here on the site.

200
00:13:34,800 --> 00:13:36,800
 If you want to go read it.

201
00:13:36,800 --> 00:13:38,800
 It's fairly academic I think.

202
00:13:38,800 --> 00:13:40,800
 Hopefully it's not a turn off.

203
00:13:40,800 --> 00:13:42,800
 But I can write.

204
00:13:42,800 --> 00:13:44,800
 My writing is good.

205
00:13:44,800 --> 00:13:46,800
 I know my writing is good.

206
00:13:46,800 --> 00:13:50,800
 So, interested if there's anyone out there who

207
00:13:50,800 --> 00:13:54,800
 would go ahead and read through it.

208
00:13:54,800 --> 00:13:57,800
 And at least give me some feedback.

209
00:13:57,800 --> 00:13:59,800
 More grammar.

210
00:13:59,800 --> 00:14:01,800
 We're supposed to do this before we hand our work in.

211
00:14:01,800 --> 00:14:03,800
 Find someone who will read it.

212
00:14:03,800 --> 00:14:06,800
 So, I got all you guys to read it.

213
00:14:06,800 --> 00:14:09,800
 Anyway, I post a link in case there's someone out there

214
00:14:09,800 --> 00:14:16,800
 who has the time to go through it.

215
00:14:16,800 --> 00:14:20,800
 That's about it.

216
00:14:20,800 --> 00:14:22,800
 Not much.

217
00:14:22,800 --> 00:14:24,800
 Yeah, so I just finished that paper tonight.

218
00:14:24,800 --> 00:14:26,800
 It's not a long paper.

219
00:14:26,800 --> 00:14:28,800
 In fact, if it seems terse and it's like,

220
00:14:28,800 --> 00:14:31,800
 "Wow, why did you write so little?"

221
00:14:31,800 --> 00:14:34,800
 And just, it feels like I felt like I was skipping

222
00:14:34,800 --> 00:14:35,800
 from point to point.

223
00:14:35,800 --> 00:14:38,800
 Because we had a word limit of 2,000 words.

224
00:14:38,800 --> 00:14:42,800
 And the papers that I came out with was 2,200.

225
00:14:42,800 --> 00:14:48,800
 And that's being excruciatingly parsimonious.

226
00:14:48,800 --> 00:14:55,800
 So, I could have written it four times as much probably.

227
00:14:55,800 --> 00:14:58,800
 At least twice as much would have been pretty easy.

228
00:14:58,800 --> 00:15:00,800
 Would have been more comfortable writing twice as much.

229
00:15:00,800 --> 00:15:03,800
 But it would have been more work.

230
00:15:03,800 --> 00:15:07,800
 So, I'm kind of thankful that it was only short.

231
00:15:07,800 --> 00:15:11,800
 And he really likes, this professor is really big on

232
00:15:11,800 --> 00:15:16,800
 concision.

233
00:15:16,800 --> 00:15:18,800
 Yeah.

234
00:15:18,800 --> 00:15:23,430
 Because I guess a lot of people just end up with words

235
00:15:23,430 --> 00:15:23,800
 salad

236
00:15:23,800 --> 00:15:28,800
 and they just, he said they write an introduction

237
00:15:28,800 --> 00:15:30,800
 that's totally unrelated to their paper

238
00:15:30,800 --> 00:15:33,800
 and they just don't really bother on about this and that.

239
00:15:33,800 --> 00:15:40,800
 So, making it so short forces you to be on track.

240
00:15:40,800 --> 00:15:43,800
 Because you have to get through your whole argument.

241
00:15:43,800 --> 00:15:45,800
 So, anyway, you don't want to hear this.

242
00:15:45,800 --> 00:15:47,800
 This is not related.

243
00:15:47,800 --> 00:15:51,800
 But tonight's verse, getting back to the verse was awesome.

244
00:15:51,800 --> 00:15:53,800
 It's an awesome little verse.

245
00:15:53,800 --> 00:15:59,800
 And very much related to our meditation practice.

246
00:15:59,800 --> 00:16:05,800
 We practice to not change the mind in terms of cultivating

247
00:16:05,800 --> 00:16:09,800
 a new I, a new me, that's Buddhist.

248
00:16:09,800 --> 00:16:13,800
 But in terms of just coming back to a natural state.

249
00:16:13,800 --> 00:16:18,800
 It's very important in Buddhism, in meditation.

250
00:16:18,800 --> 00:16:22,800
 That we're just trying to sort everything out.

251
00:16:22,800 --> 00:16:25,800
 Untie all the knots.

252
00:16:25,800 --> 00:16:27,800
 Which is a bigger task than it may sound.

253
00:16:27,800 --> 00:16:29,800
 I mean, it's not like you're just going to go back

254
00:16:29,800 --> 00:16:33,800
 to the ordinary person you were before or you generally are

255
00:16:33,800 --> 00:16:33,800
.

256
00:16:33,800 --> 00:16:35,800
 It is a profound change.

257
00:16:35,800 --> 00:16:38,800
 But it's only a profound change because,

258
00:16:38,800 --> 00:16:41,950
 because really everything that makes us human is an art

259
00:16:41,950 --> 00:16:42,800
ifice.

260
00:16:42,800 --> 00:16:47,800
 The whole being a human is not natural.

261
00:16:47,800 --> 00:16:49,800
 And we get back into this idea of what's natural.

262
00:16:49,800 --> 00:16:52,800
 But it's not natural in the sense that we've made it.

263
00:16:52,800 --> 00:16:55,800
 We've cultivated it.

264
00:16:55,800 --> 00:16:58,800
 We've cultivated it arbitrarily.

265
00:16:58,800 --> 00:17:02,440
 There's nothing particularly, it's not like you look at the

266
00:17:02,440 --> 00:17:02,800
 universe

267
00:17:02,800 --> 00:17:04,800
 and say, oh, that needs humans.

268
00:17:04,800 --> 00:17:07,800
 The perfect representation of the universe,

269
00:17:07,800 --> 00:17:09,800
 of being in the universe, a human being.

270
00:17:09,800 --> 00:17:12,800
 No, I mean, that's what Christianity, Judaism,

271
00:17:12,800 --> 00:17:14,800
 theistic religions tried to do.

272
00:17:14,800 --> 00:17:16,800
 They said, well, why are humans here?

273
00:17:16,800 --> 00:17:18,800
 Well, we must be the perfect, you know,

274
00:17:18,800 --> 00:17:21,800
 somehow they got to that.

275
00:17:21,800 --> 00:17:23,800
 It's not true.

276
00:17:23,800 --> 00:17:33,800
 Anyone who believes that is grossly deluding themselves.

277
00:17:33,800 --> 00:17:39,800
 So, but there is something luminous and pure in us all

278
00:17:39,800 --> 00:17:43,800
 underneath all that garbage, no matter who you are,

279
00:17:43,800 --> 00:17:45,800
 no matter what you've done.

280
00:17:45,800 --> 00:17:53,800
 Even David Datta in the end, gone on the right track.

281
00:17:53,800 --> 00:17:57,800
 Anyway, that's all for tonight.

282
00:17:57,800 --> 00:17:58,800
 Take a rest tomorrow.

283
00:17:58,800 --> 00:18:01,800
 I'm going to spend some of the day going over any comments

284
00:18:01,800 --> 00:18:04,800
 that people have on my paper, if anyone does look at it.

285
00:18:04,800 --> 00:18:08,620
 And going over it, and then I got to submit it before the

286
00:18:08,620 --> 00:18:09,800
 end of tomorrow.

287
00:18:09,800 --> 00:18:11,800
 So, and then I'm done.

288
00:18:11,800 --> 00:18:16,160
 Got a couple exams, but oh, and the symposium next week is

289
00:18:16,160 --> 00:18:17,800
 the peace symposium.

290
00:18:17,800 --> 00:18:21,790
 So, yeah, this week I'm going to be spending on organizing

291
00:18:21,790 --> 00:18:24,800
 the symposium.

292
00:18:24,800 --> 00:18:27,800
 Okay, that's all. Have a good night.

293
00:18:27,800 --> 00:18:30,800
 Oh, yeah, have a good night.

294
00:18:30,800 --> 00:18:31,800
 No questions tonight.

295
00:18:31,800 --> 00:18:35,580
 Not that anyone ever asked them, but if you happen to have

296
00:18:35,580 --> 00:18:36,800
 questions,

297
00:18:36,800 --> 00:18:38,800
 come back tomorrow.

298
00:18:38,800 --> 00:18:42,800
 Okay.

299
00:18:44,800 --> 00:18:46,800
 Thank you.

