1
00:00:00,000 --> 00:00:03,980
 I'm planning to do samatha in the morning for energy during

2
00:00:03,980 --> 00:00:06,000
 the day.

3
00:00:06,000 --> 00:00:09,000
 Right, and in the evening balance with vipassana.

4
00:00:09,000 --> 00:00:13,100
 Is that okay or should I do it always in combination or

5
00:00:13,100 --> 00:00:14,000
 completely apart?

6
00:00:14,000 --> 00:00:18,000
 So, that's interesting though.

7
00:00:18,000 --> 00:00:22,000
 Well, power to you. I mean, jeez, the Buddha said samatha

8
00:00:22,000 --> 00:00:24,000
 and vipassana are the two greatest,

9
00:00:24,000 --> 00:00:29,000
 are just the greatest benefit, the most wonderful things.

10
00:00:29,000 --> 00:00:31,000
 Are you doing both of them? Wow.

11
00:00:31,000 --> 00:00:34,000
 It's like, good for you.

12
00:00:34,000 --> 00:00:38,000
 I can think of far worse things you could be doing.

13
00:00:38,000 --> 00:00:42,000
 But there are issues with doing two different techniques.

14
00:00:42,000 --> 00:00:44,440
 If you're doing one technique in the morning and one

15
00:00:44,440 --> 00:00:46,000
 technique in the evening,

16
00:00:46,000 --> 00:00:49,000
 then you have twice as much to train your mind in.

17
00:00:49,000 --> 00:00:53,000
 Because your mind has to become accustomed to the practice.

18
00:00:53,000 --> 00:00:57,380
 So, why not do something that has both samatha and vipass

19
00:00:57,380 --> 00:00:58,000
ana together?

20
00:00:58,000 --> 00:01:03,000
 Why not practice both of them together?

21
00:01:03,000 --> 00:01:05,870
 You say samatha brings you energy. Well, that's interesting

22
00:01:05,870 --> 00:01:06,000
.

23
00:01:06,000 --> 00:01:11,000
 That's a good reason, I suppose.

24
00:01:11,000 --> 00:01:14,000
 I never found the need to practice samatha for energy.

25
00:01:14,000 --> 00:01:17,000
 I remember doing tree planting.

26
00:01:17,000 --> 00:01:19,000
 I spent months doing tree planting.

27
00:01:19,000 --> 00:01:22,000
 And here, look, I was not much bigger than I am now.

28
00:01:22,000 --> 00:01:27,000
 Well, a little bit more muscular.

29
00:01:27,000 --> 00:01:30,000
 And I just did vipassana, and it was incredible for energy.

30
00:01:30,000 --> 00:01:33,680
 It was amazing that I was able to keep up with some of the

31
00:01:33,680 --> 00:01:35,000
 best planters out there

32
00:01:35,000 --> 00:01:38,000
 because I just had such dedication.

33
00:01:38,000 --> 00:01:45,230
 Vipassana gives you such ability to do without feeling,

34
00:01:45,230 --> 00:01:47,000
 without flagging,

35
00:01:47,000 --> 00:01:53,180
 to push yourself physically, without worrying, without

36
00:01:53,180 --> 00:01:56,000
 stopping, without giving in.

37
00:01:56,000 --> 00:02:03,000
 I don't know.

38
00:02:03,000 --> 00:02:06,360
 I'm not keen on the whole samatha in the morning for energy

39
00:02:06,360 --> 00:02:07,000
 thing.

40
00:02:07,000 --> 00:02:11,000
 Nik, samatha is something that you should do in the forest,

41
00:02:11,000 --> 00:02:20,130
 or in some sort of seclusion, because if you can't get free

42
00:02:20,130 --> 00:02:21,000
 from the...

43
00:02:21,000 --> 00:02:24,000
 Ajahn Chai even said samatha is based on craving.

44
00:02:24,000 --> 00:02:28,000
 I don't know if I'd go so far, but he said this.

45
00:02:28,000 --> 00:02:31,440
 Or based on clinging, or one or the other, it's either

46
00:02:31,440 --> 00:02:34,000
 based on craving or clinging.

47
00:02:34,000 --> 00:02:36,680
 If you're only practicing it for energy, well, then it

48
00:02:36,680 --> 00:02:38,000
 might actually be detrimental

49
00:02:38,000 --> 00:02:45,000
 because it'll create a sort of addiction to it almost.

50
00:02:45,000 --> 00:02:49,270
 If you don't get into the jhanas, where you're free from

51
00:02:49,270 --> 00:02:50,000
 that.

52
00:02:50,000 --> 00:02:54,000
 And even if you do, there's an argument to be made for it,

53
00:02:54,000 --> 00:02:59,000
 the intellectual or the abstract clinging to the jhanas.

54
00:02:59,000 --> 00:03:02,000
 When you come out of them, then your craving comes back.

55
00:03:02,000 --> 00:03:05,000
 You can actually crave for them, or become addicted,

56
00:03:05,000 --> 00:03:09,000
 or even give rise to views.

57
00:03:09,000 --> 00:03:15,000
 I would say vipassana is much more...

58
00:03:15,000 --> 00:03:18,610
 a much more potential value to someone who has to go to

59
00:03:18,610 --> 00:03:19,000
 work,

60
00:03:19,000 --> 00:03:24,150
 someone who's not a monk, because it relates directly to

61
00:03:24,150 --> 00:03:26,000
 your experience.

62
00:03:26,000 --> 00:03:32,000
 Samatha is an escape from your experience.

63
00:03:32,000 --> 00:03:39,000
 I think that's valid. Anybody?

64
00:03:39,000 --> 00:03:42,000
 Anybody else who's here?

65
00:03:42,000 --> 00:03:50,700
 I started off meditating, doing samatha, and then I moved

66
00:03:50,700 --> 00:03:57,000
 over to insight meditation later.

67
00:03:57,000 --> 00:04:00,590
 I think they both have their benefits. I mean, I don't do

68
00:04:00,590 --> 00:04:03,000
 samatha at the moment.

69
00:04:03,000 --> 00:04:07,010
 It can be very good for quieting down the mind, you know,

70
00:04:07,010 --> 00:04:08,000
 samatha,

71
00:04:08,000 --> 00:04:13,000
 giving you the kind of calmness which you can build on.

72
00:04:13,000 --> 00:04:16,000
 Yeah.

73
00:04:16,000 --> 00:04:24,000
 What if you were to use the abdomen in a way for samatha,

74
00:04:24,000 --> 00:04:29,000
 for your samatha practice?

75
00:04:29,000 --> 00:04:32,360
 That's difficult, because the abdomen is impermanent, it's

76
00:04:32,360 --> 00:04:33,000
 changing.

77
00:04:33,000 --> 00:04:38,000
 You need an object that is stable, that you can rely upon.

78
00:04:38,000 --> 00:04:46,000
 The abdomen can't be relied upon.

79
00:04:46,000 --> 00:04:49,000
 If you do samatha, it's not really bad.

80
00:04:49,000 --> 00:04:51,000
 The bigger problem I think is with having two techniques.

81
00:04:51,000 --> 00:04:56,000
 For me it seems too much trouble.

82
00:04:56,000 --> 00:04:57,990
 You see, we always want to make things more complicated

83
00:04:57,990 --> 00:05:01,000
 than they need be.

84
00:05:01,000 --> 00:05:03,790
 But with two different meditation techniques, think of what

85
00:05:03,790 --> 00:05:05,000
 that's doing to your poor brain,

86
00:05:05,000 --> 00:05:09,820
 that has to now grapple with two different modes of mental

87
00:05:09,820 --> 00:05:11,000
 behavior.

88
00:05:11,000 --> 00:05:14,620
 Maybe that's unfair, because it's actually maybe good for

89
00:05:14,620 --> 00:05:17,000
 the brain to have to work out.

90
00:05:17,000 --> 00:05:22,000
 But if you're working, if you have to work during the day,

91
00:05:22,000 --> 00:05:27,000
 it just seems better to keep it simple.

92
00:05:27,000 --> 00:05:31,000
 I don't know.

93
00:05:31,000 --> 00:05:33,000
 Nothing wrong with either of those things.

94
00:05:33,000 --> 00:05:37,000
 Some of it doesn't look as good as it looks.

95
00:05:37,000 --> 00:05:39,000
 If it gets to be a routine where you do one every morning,

96
00:05:39,000 --> 00:05:44,000
 one every evening, sure, why not?

