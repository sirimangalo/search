1
00:00:00,000 --> 00:00:05,360
 Hello and welcome back to our study of the Dhanapada.

2
00:00:05,360 --> 00:00:13,950
 Today we continue on with verses 155 and 156 which read as

3
00:00:13,950 --> 00:00:15,120
 follows.

4
00:00:15,120 --> 00:00:24,100
 Acharytva Brahma Charyang Alanda Yobhane Dhanang Chinn Kaur

5
00:00:24,100 --> 00:00:32,720
 Kaun Chava Jayanti Kheerna Machewa Paladeh

6
00:00:32,720 --> 00:00:41,890
 Acharytva Brahma Charyang Alanda Yobhane Dhanang Sainty Ch

7
00:00:41,890 --> 00:00:49,280
apa Tikhinawa Puranani Anuttanang

8
00:00:49,280 --> 00:00:57,660
 Which means having not practiced the holy life, having not

9
00:00:57,660 --> 00:01:02,560
 gained wealth in one's youth.

10
00:01:02,800 --> 00:01:12,990
 One wastes away, they waste away like old herons, old her

11
00:01:12,990 --> 00:01:16,480
ons in a pond with,

12
00:01:16,480 --> 00:01:20,800
 in a pond where the fish are gone, like old herons in a

13
00:01:20,800 --> 00:01:23,360
 pond where the fish are gone.

14
00:01:28,160 --> 00:01:31,990
 That's the first verse, the second verse, having not lived

15
00:01:31,990 --> 00:01:32,880
 the holy life,

16
00:01:32,880 --> 00:01:35,440
 having not gained wealth in one's youth.

17
00:01:35,440 --> 00:01:44,000
 They lie like arrows shot from a bow,

18
00:01:44,000 --> 00:01:55,680
 lamenting over things, over,

19
00:01:55,680 --> 00:02:00,920
 lamenting over things in the past, lamenting over times

20
00:02:00,920 --> 00:02:01,600
 gone.

21
00:02:01,600 --> 00:02:14,480
 Again this chapter is a little bit on the negative side

22
00:02:14,480 --> 00:02:16,400
 which is important,

23
00:02:16,400 --> 00:02:18,400
 it's important to talk about these things.

24
00:02:21,760 --> 00:02:24,010
 And I'll get into that, I'd like this to actually be a

25
00:02:24,010 --> 00:02:26,160
 positive experience, this one I think is quite

26
00:02:26,160 --> 00:02:38,620
 positive for us. So the story goes that there was a young

27
00:02:38,620 --> 00:02:43,360
 boy and his parents were quite rich,

28
00:02:45,280 --> 00:02:52,800
 they were in fact so rich that his parents thought to

29
00:02:52,800 --> 00:02:53,280
 themselves that

30
00:02:53,280 --> 00:02:58,320
 why would we teach him how to make money, why would we

31
00:02:58,320 --> 00:03:01,040
 teach him how to live, how to work,

32
00:03:01,040 --> 00:03:06,480
 there's so much wealth he can never use at all.

33
00:03:08,720 --> 00:03:15,360
 And so they simply instructed him on how to enjoy himself,

34
00:03:15,360 --> 00:03:17,200
 thinking that his life would be a life of

35
00:03:17,200 --> 00:03:20,990
 luxury, a life of pleasure. Instructed him in singing and

36
00:03:20,990 --> 00:03:23,680
 in playing musical instruments,

37
00:03:23,680 --> 00:03:25,840
 and that was all he received.

38
00:03:25,840 --> 00:03:31,130
 And there was, in the same city, there was a very rich

39
00:03:31,130 --> 00:03:32,640
 woman about his age,

40
00:03:33,200 --> 00:03:38,220
 and her parents did likewise, and they spoiled them both.

41
00:03:38,220 --> 00:03:39,440
 Let me use the word spoiled, I mean

42
00:03:39,440 --> 00:03:44,640
 here it's put quite neutrally, it's not spoiled, it's just

43
00:03:44,640 --> 00:03:47,120
 taught him the things that would be

44
00:03:47,120 --> 00:03:50,340
 important to him, which is basically nothing, and taught

45
00:03:50,340 --> 00:03:52,480
 him how to enjoy himself, thinking

46
00:03:52,480 --> 00:03:54,720
 nothing else could possibly be important, right?

47
00:03:59,040 --> 00:04:01,860
 And eventually his parents, eventually these two got

48
00:04:01,860 --> 00:04:04,240
 married, right? So this woman married this

49
00:04:04,240 --> 00:04:08,480
 treasurer's son or this rich man's son, they got married,

50
00:04:08,480 --> 00:04:12,240
 and so they had immeasurable wealth,

51
00:04:12,240 --> 00:04:16,190
 a lot of money, and as a result they were good friends with

52
00:04:16,190 --> 00:04:17,600
 the king, and so they would go and

53
00:04:17,600 --> 00:04:21,360
 wait upon the king. One day on his way to see the king,

54
00:04:22,960 --> 00:04:29,830
 he saw this group of men by the side of the road, and these

55
00:04:29,830 --> 00:04:34,400
 guys had, they knew that this

56
00:04:34,400 --> 00:04:39,440
 guy was really rich, but actually quite naive, and so they

57
00:04:39,440 --> 00:04:41,600
 thought it was a plan, a scam,

58
00:04:41,600 --> 00:26:28,270
 and so they sat by the side of the road when he went by,

59
00:26:28,270 --> 00:04:50,720
 and they sat drinking alcohol,

60
00:04:51,440 --> 00:04:54,820
 and enjoying themselves, and as he was going by he saw

61
00:04:54,820 --> 00:04:57,680
 these men who seemed really happy,

62
00:04:57,680 --> 00:05:04,630
 and they were acting up to appear as happy and as joyful,

63
00:05:04,630 --> 00:05:08,960
 and talking about the benefits of what

64
00:05:08,960 --> 00:05:10,940
 they were drinking, and so he asked them, "What are you

65
00:05:10,940 --> 00:05:12,960
 drinking?" He said, "We're drinking alcohol."

66
00:05:14,160 --> 00:05:18,840
 And he said, "Does it taste good?" And they said, "Oh, in

67
00:05:18,840 --> 00:05:21,600
 this world there's no drink like it."

68
00:05:21,600 --> 00:05:26,590
 And they said, "Well then, well then I must have some." And

69
00:05:26,590 --> 00:05:28,800
 so being the naive

70
00:05:28,800 --> 00:05:32,420
 man that he was, he had never even heard of alcohol or had

71
00:05:32,420 --> 00:05:34,080
 anything to do with it,

72
00:05:34,080 --> 00:05:37,990
 and these men gave him some alcohol first a little and then

73
00:05:37,990 --> 00:05:39,840
 a little more until eventually

74
00:05:40,800 --> 00:05:44,500
 he became of course addicted, I mean perhaps not physically

75
00:05:44,500 --> 00:05:46,480
 addicted, although that's quite

76
00:05:46,480 --> 00:05:51,920
 possible as well, but certainly mentally addicted. This was

77
00:05:51,920 --> 00:05:56,000
 one of the drugs at that time, I mean,

78
00:05:56,000 --> 00:06:00,700
 even today it's a common drug of choice for a lot of people

79
00:06:00,700 --> 00:06:03,840
. He became an alcoholic, but I think part

80
00:06:03,840 --> 00:06:07,840
 of the reason he became an alcoholic is simply because he

81
00:06:07,840 --> 00:06:10,560
 was accustomed to enjoying whatever

82
00:06:10,560 --> 00:06:12,790
 he could get his hands on. When something came that was

83
00:06:12,790 --> 00:06:14,880
 enjoyable, he had no sense that there

84
00:06:14,880 --> 00:06:19,400
 might be a problem of getting addicted to it or overindul

85
00:06:19,400 --> 00:06:24,000
ging it. So whereas a little bit of

86
00:06:24,000 --> 00:06:28,860
 alcohol is a bad thing, a lot of alcohol is a terrible

87
00:06:28,860 --> 00:06:32,320
 thing. And so eventually he became

88
00:06:33,040 --> 00:06:37,350
 an alcoholic and as a result lost all of his sense of

89
00:06:37,350 --> 00:06:40,560
 moderation, whatever little he had.

90
00:06:40,560 --> 00:06:44,560
 And whereas before living an ordinary life it would have

91
00:06:44,560 --> 00:06:48,000
 been quite difficult for him to use

92
00:06:48,000 --> 00:06:54,970
 up all his money as a result of his excessive or his

93
00:06:54,970 --> 00:06:59,440
 extreme state of intoxication,

94
00:07:01,200 --> 00:07:04,580
 he squandered his money and he was actually able to waste

95
00:07:04,580 --> 00:07:06,320
 it away. If you've ever seen,

96
00:07:06,320 --> 00:07:11,720
 I mean this is the funny thing about alcohol, if you've

97
00:07:11,720 --> 00:07:15,280
 ever been to Las Vegas, to the casinos,

98
00:07:15,280 --> 00:07:18,020
 apparently one of the important things is to ply the

99
00:07:18,020 --> 00:07:20,160
 customers with alcohol because it makes them

100
00:07:20,160 --> 00:07:24,350
 lose whatever it is that keeps you from gambling away your

101
00:07:24,350 --> 00:07:27,200
 life savings. Likewise bars, I mean,

102
00:07:27,840 --> 00:07:31,360
 a bar, it's incredible the bar tabs that people can rack up

103
00:07:31,360 --> 00:07:34,080
, not just that they will drink alcohol

104
00:07:34,080 --> 00:07:36,880
 at all but that they'll pay exorbitant amounts of money for

105
00:07:36,880 --> 00:07:41,200
 the alcohol as a result of being

106
00:07:41,200 --> 00:07:48,070
 without their general sense of reason. And so he acted in

107
00:07:48,070 --> 00:07:51,760
 this way, giving money away left,

108
00:07:51,760 --> 00:07:54,730
 right, center and of course the people around him were

109
00:07:54,730 --> 00:07:57,680
 these men who had introduced

110
00:07:57,680 --> 00:08:01,760
 him to strong drink and they were robbing him blind, didn't

111
00:08:01,760 --> 00:08:03,520
 even have to go behind his back,

112
00:08:03,520 --> 00:08:07,540
 they just convinced him to give away all his money.

113
00:08:07,540 --> 00:08:10,320
 Eventually he gave away all his money and

114
00:08:10,320 --> 00:08:13,350
 he had to sell all of his things or he found out that he

115
00:08:13,350 --> 00:08:15,040
 had no money and he said, "Well,

116
00:08:15,040 --> 00:08:19,240
 what about my wife? Does she have money?" And so he had his

117
00:08:19,240 --> 00:08:21,520
 wife give him all of her money as well,

118
00:08:21,520 --> 00:08:27,360
 she had no idea, they had no sense of saving the money and

119
00:08:27,360 --> 00:08:30,960
 so squandered all of her money as well.

120
00:08:30,960 --> 00:08:34,900
 Eventually had to sell all of his belongings, his carriages

121
00:08:34,900 --> 00:08:37,600
 and his fine clothes and his jewelry and

122
00:08:37,600 --> 00:08:42,890
 eventually his house, eventually he was so much in debt

123
00:08:42,890 --> 00:08:46,160
 that he was turned out of his house

124
00:08:48,320 --> 00:08:56,150
 and eventually became bigger. Living in the streets of Var

125
00:08:56,150 --> 00:08:59,600
anasi which is usually we're in

126
00:08:59,600 --> 00:09:02,800
 Sawati but this time we're in Varanasi when the Buddha was

127
00:09:02,800 --> 00:09:04,480
 dwelling in Isipatana which is

128
00:09:04,480 --> 00:09:08,040
 interesting as well because Buddha didn't spend much time

129
00:09:08,040 --> 00:09:10,640
 at Isipatana. Anyway.

130
00:09:15,600 --> 00:09:18,650
 One day the Buddha was walking through the city on Amr's

131
00:09:18,650 --> 00:09:22,080
 Round and he came upon these two old people

132
00:09:22,080 --> 00:09:32,880
 begging for food.

133
00:09:32,880 --> 00:09:37,870
 I saw the man anyway and so I'm begging for food and the

134
00:09:37,870 --> 00:09:40,000
 Buddha stopped and smiled

135
00:09:42,240 --> 00:09:47,070
 again with a sense of humor of the Buddha. Ananda asked him

136
00:09:47,070 --> 00:09:48,640
, "Venerable Sir, why are you smiling?"

137
00:09:48,640 --> 00:09:53,140
 They said, "You see that man over there? That man used to

138
00:09:53,140 --> 00:09:55,280
 be one of the richest men in the city."

139
00:09:55,280 --> 00:10:00,880
 And now after having squandered all of his life savings and

140
00:10:00,880 --> 00:10:03,040
 all of his wife's life savings,

141
00:10:04,800 --> 00:10:12,470
 all caused by the indulgence in alcohol and his parents

142
00:10:12,470 --> 00:10:16,240
 lack of any kind of education

143
00:10:16,240 --> 00:10:23,430
 or even common sense in terms of how to maintain your

144
00:10:23,430 --> 00:10:23,920
 wealth,

145
00:10:23,920 --> 00:10:31,340
 is now reduced to poverty and destitution. He has become

146
00:10:31,340 --> 00:10:33,440
 like a heron in a dried up,

147
00:10:33,440 --> 00:10:37,680
 a heron in a pond, a heron in a dried up pond.

148
00:10:37,680 --> 00:10:48,090
 And Buddha says something interesting. He says if he had in

149
00:10:48,090 --> 00:10:51,200
 his, if you want to understand the

150
00:10:51,200 --> 00:10:58,220
 real consequences of what he's done, if in his youth he had

151
00:10:58,220 --> 00:11:03,920
 invested his money and if he had

152
00:11:03,920 --> 00:11:07,920
 applied himself to business, he could have been the number

153
00:11:07,920 --> 00:11:10,560
 one businessman in the world. He had

154
00:11:10,560 --> 00:11:14,540
 enough wealth and he had enough intelligence that if he had

155
00:11:14,540 --> 00:11:16,960
 only applied himself, he could have been

156
00:11:16,960 --> 00:11:21,200
 the richest man in the city. If he had gone forth, if he

157
00:11:21,200 --> 00:11:24,560
 had left the home life and become a reckless,

158
00:11:24,560 --> 00:11:28,060
 become a Buddhist monk, he could have become an arahant.

159
00:11:28,060 --> 00:11:30,400
 His wife would have become an anagami.

160
00:11:30,400 --> 00:11:37,320
 That was if when he was young he had done it. And or in his

161
00:11:37,320 --> 00:11:43,360
 middle years, if he had applied himself

162
00:11:43,360 --> 00:11:45,750
 to business, he could have been the number two rich man in

163
00:11:45,750 --> 00:11:48,640
 the world. Not as good because he

164
00:11:48,640 --> 00:11:52,860
 wouldn't have had enough time and enough energy and enough

165
00:11:52,860 --> 00:11:56,080
 capital. And if he had gone forth,

166
00:11:56,080 --> 00:12:00,150
 he would have become an anagami. And if he had gone forth

167
00:12:00,150 --> 00:12:01,120
 as an adult,

168
00:12:01,120 --> 00:12:08,160
 and his wife would have become a saccada, saccada gan.

169
00:12:12,800 --> 00:12:18,310
 If when he was in his third, sort of the middle-aged, sort

170
00:12:18,310 --> 00:12:19,440
 of older

171
00:12:19,440 --> 00:12:25,090
 persons years, the third period of adulthood, whatever that

172
00:12:25,090 --> 00:12:25,360
 is,

173
00:12:25,360 --> 00:12:29,600
 if he had applied himself for business, he could have been

174
00:12:29,600 --> 00:12:31,120
 come, well a very rich man,

175
00:12:31,120 --> 00:12:37,020
 perhaps the third richest. And if he had gone forth, he

176
00:12:37,020 --> 00:12:38,400
 would have become a saccada gami and

177
00:12:38,400 --> 00:12:42,180
 his wife would have become a sotapana. But now he's nothing

178
00:12:42,180 --> 00:12:46,800
. Now he's lost any chance of being

179
00:12:46,800 --> 00:12:51,350
 wealthy as a layman, as a householder. He's also lost any

180
00:12:51,350 --> 00:12:54,000
 opportunity to become enlightened.

181
00:12:54,000 --> 00:12:58,450
 His mind is too corrupt. Perhaps he's got liver disease and

182
00:12:58,450 --> 00:13:01,040
 his brain has been addled by the

183
00:13:01,040 --> 00:13:09,280
 alcohol. And so then he spoke these two verses about if

184
00:13:09,280 --> 00:13:14,000
 they haven't led the holy life or done

185
00:13:14,000 --> 00:13:17,490
 anything in the world, and they're useless on both counts.

186
00:13:17,490 --> 00:13:21,680
 They're like a heron in a dried up pond

187
00:13:21,680 --> 00:13:25,750
 without fish. They're like an arrow cast from a bow. It's

188
00:13:25,750 --> 00:13:28,560
 quite poetic. When you shoot an arrow

189
00:13:28,560 --> 00:13:32,360
 off into the forest, it just lies there useless. Something

190
00:13:32,360 --> 00:13:34,320
 that is cast off, something that is

191
00:13:34,320 --> 00:13:44,140
 thrown away, gone far from any use, any value. So again,

192
00:13:44,140 --> 00:13:49,360
 fairly unpleasant story, a sad story.

193
00:13:51,200 --> 00:13:57,510
 Of course the Buddha found it humorous. But again, with the

194
00:13:57,510 --> 00:14:00,000
 Buddha and with our hunts,

195
00:14:00,000 --> 00:14:06,110
 it's an expression of the great peace and the great joy in

196
00:14:06,110 --> 00:14:10,400
 having freed oneself from samsara.

197
00:14:10,400 --> 00:14:15,990
 I mean, these two people, they're not doomed. They're just

198
00:14:15,990 --> 00:14:17,760
 stuck. And that's samsara. I mean,

199
00:14:17,760 --> 00:14:21,520
 this is the way of things. They're going to be reborn again

200
00:14:21,520 --> 00:14:23,600
 and they'll have another chance

201
00:14:23,600 --> 00:14:26,570
 and they'll keep going. That's the way of the world. There

202
00:14:26,570 --> 00:14:28,400
's not anything you can do about it.

203
00:14:28,400 --> 00:14:35,870
 We can't save everyone. But that's really the wonderful

204
00:14:35,870 --> 00:14:37,920
 thing is that we should,

205
00:14:37,920 --> 00:14:41,880
 this first gives us cause to be joyful. The first thing it

206
00:14:41,880 --> 00:14:44,000
 should do, and it's not the only thing,

207
00:14:44,000 --> 00:14:47,860
 we're not gloating over those people who are unable to

208
00:14:47,860 --> 00:14:49,440
 become enlightened.

209
00:14:49,440 --> 00:14:54,860
 But I think it's important because we often get down on

210
00:14:54,860 --> 00:14:55,920
 ourselves.

211
00:14:55,920 --> 00:15:02,210
 Give all these talks on mindfulness and insight. And it

212
00:15:02,210 --> 00:15:04,240
 sounds like to be of any worth,

213
00:15:04,240 --> 00:15:09,250
 you somehow have to attain some high level of enlightenment

214
00:15:09,250 --> 00:15:13,040
. But really, we should all be

215
00:15:14,160 --> 00:15:18,250
 proud and encouraged anyway. Pride is maybe not so good,

216
00:15:18,250 --> 00:15:21,680
 but be encouraged about ourselves.

217
00:15:21,680 --> 00:15:25,630
 Because we've done good things. Even just coming here to

218
00:15:25,630 --> 00:15:26,960
 listen or even just

219
00:15:26,960 --> 00:15:31,370
 coming to this YouTube channel and turning on the video.

220
00:15:31,370 --> 00:15:33,920
 Even just coming to Second Life,

221
00:15:33,920 --> 00:15:37,110
 to the Buddha Center thinking, maybe I'll find something of

222
00:15:37,110 --> 00:15:37,920
 value here.

223
00:15:39,360 --> 00:15:43,660
 It's an incredibly rare thing and it's an incredibly prais

224
00:15:43,660 --> 00:15:48,640
eworthy activity. It shows some

225
00:15:48,640 --> 00:15:54,620
 incredible foresight on your part. You might not think that

226
00:15:54,620 --> 00:15:57,920
. It might seem quite insignificant.

227
00:15:57,920 --> 00:16:05,870
 But we underestimate the power of this. How many people

228
00:16:05,870 --> 00:16:07,680
 would ridicule someone who came to the

229
00:16:07,680 --> 00:16:10,920
 Buddha Center? How many people would ridicule someone? How

230
00:16:10,920 --> 00:16:12,320
 many people would ridicule me for

231
00:16:12,320 --> 00:16:17,400
 making these videos? How silly. Who is this guy? What is he

232
00:16:17,400 --> 00:16:20,960
 doing? And those of you who watch,

233
00:16:20,960 --> 00:16:26,540
 thinking you're all sheep or you're all wasting your time

234
00:16:26,540 --> 00:16:28,240
 when you could be

235
00:16:28,240 --> 00:16:34,190
 living your life to the fullest. Carpe diem. Eat, drink and

236
00:16:34,190 --> 00:16:35,360
 be merry.

237
00:16:35,360 --> 00:16:43,520
 I say it's a rare thing in the world when the Buddha said

238
00:16:43,520 --> 00:16:44,160
 as well, but

239
00:16:44,160 --> 00:16:50,000
 I don't think it takes a Buddha to see how wonderful it is

240
00:16:50,000 --> 00:16:52,880
 for people to actually

241
00:16:52,880 --> 00:16:57,850
 incline in a good way. The Buddha said it's of immeasurable

242
00:16:57,850 --> 00:17:00,320
 benefit to meditate even for a moment.

243
00:17:03,520 --> 00:17:06,960
 If we have one moment of mindfulness where we're here,

244
00:17:06,960 --> 00:17:09,280
 where we're present, where we see things as

245
00:17:09,280 --> 00:17:13,180
 they are, aware of the body, the feelings, the mind, the d

246
00:17:13,180 --> 00:17:15,840
hamma, just one moment is an incredible thing.

247
00:17:15,840 --> 00:17:21,390
 How much preparation it takes to get to that moment where

248
00:17:21,390 --> 00:17:23,360
 you're actually mindful.

249
00:17:23,360 --> 00:17:28,480
 Don't underestimate it. Don't underestimate the power of

250
00:17:28,480 --> 00:17:30,800
 the mind that's inclined in the right direction.

251
00:17:31,520 --> 00:17:35,440
 The second thing, of course, that it has to do is give us a

252
00:17:35,440 --> 00:17:37,120
 kick in the pants,

253
00:17:37,120 --> 00:17:42,960
 reminding us that the clock is ticking, as with much in

254
00:17:42,960 --> 00:17:47,680
 this chapter. It's the old age chapter.

255
00:17:47,680 --> 00:17:52,220
 Stealing with the fact that we'll all get old and if we've

256
00:17:52,220 --> 00:17:55,440
 done nothing of any benefit by the time

257
00:17:55,440 --> 00:17:58,380
 we get old and just waste the waste, squander this

258
00:17:58,380 --> 00:18:02,240
 opportunity that we have, we will pay for it.

259
00:18:02,240 --> 00:18:06,970
 There will be consequences. We'll have wasted this valuable

260
00:18:06,970 --> 00:18:08,000
 opportunity.

261
00:18:08,000 --> 00:18:14,300
 That's not about failing or succeeding. It's about using or

262
00:18:14,300 --> 00:18:17,040
 wasting. They have this opportunity.

263
00:18:17,040 --> 00:18:20,760
 Are you trying? Much of Buddhism is about trying and

264
00:18:20,760 --> 00:18:21,600
 failing.

265
00:18:23,520 --> 00:18:25,900
 There was, I saw something on Facebook that I've seen

266
00:18:25,900 --> 00:18:28,400
 before, it was, or on Reddit maybe,

267
00:18:28,400 --> 00:18:32,480
 this Wholesome Memes subreddit.

268
00:18:32,480 --> 00:18:39,130
 I haven't, what is it, I haven't failed. I've just found a

269
00:18:39,130 --> 00:18:40,880
 thousand ways that didn't work.

270
00:18:40,880 --> 00:18:48,880
 I think that's a good saying. I mean, an important thing,

271
00:18:48,880 --> 00:18:50,320
 important way of looking at things.

272
00:18:51,200 --> 00:18:53,440
 Because you only really fail when you stop trying.

273
00:18:53,440 --> 00:18:58,470
 And trying itself is succeeding. It's an activity that

274
00:18:58,470 --> 00:19:01,040
 makes you a better person.

275
00:19:01,040 --> 00:19:05,760
 Much of meditation is about failing. It's about realizing.

276
00:19:05,760 --> 00:19:08,320
 It's about coming to understand

277
00:19:08,320 --> 00:19:13,120
 gradually how you're doing everything wrong, how you can't

278
00:19:13,120 --> 00:19:16,320
 control things, how you aren't in charge.

279
00:19:17,920 --> 00:19:20,230
 And so much of meditation is just banging your head against

280
00:19:20,230 --> 00:19:21,840
 the wall until you realize that

281
00:19:21,840 --> 00:19:26,310
 your ordinary way of approaching things, even meditation,

282
00:19:26,310 --> 00:19:30,480
 is useless, is harmful.

283
00:19:30,480 --> 00:19:39,270
 And so don't be discouraged. Many people are discouraged

284
00:19:39,270 --> 00:19:40,880
 thinking, "I can't meditate. I'm just

285
00:19:40,880 --> 00:19:45,790
 not able. I can't focus. I try to meditate, but I can't

286
00:19:45,790 --> 00:19:49,200
 focus." Well, that is a great reason to

287
00:19:49,200 --> 00:19:52,770
 learn how to meditate. Anyone who says they can't meditate

288
00:19:52,770 --> 00:19:54,960
 because their mind won't stay still

289
00:19:54,960 --> 00:20:00,990
 is misunderstanding the whole point of meditation. It has

290
00:20:00,990 --> 00:20:04,160
 to be told anyway.

291
00:20:04,160 --> 00:20:07,030
 That's the reason why you meditate, because you can't focus

292
00:20:07,030 --> 00:20:10,160
. You meditate to understand that.

293
00:20:10,640 --> 00:20:15,090
 You meditate to overcome that. You meditate to let go of

294
00:20:15,090 --> 00:20:15,840
 that.

295
00:20:15,840 --> 00:20:20,060
 You don't meditate to force yourself not to get distracted.

296
00:20:20,060 --> 00:20:21,440
 You don't meditate

297
00:20:21,440 --> 00:20:24,870
 because somehow some people are able to find a switch where

298
00:20:24,870 --> 00:20:26,560
 they turn their mind off.

299
00:20:26,560 --> 00:20:30,200
 And we want to meditate on the distraction. We want to

300
00:20:30,200 --> 00:20:32,960
 learn about our distractions. We want

301
00:20:32,960 --> 00:26:28,270
 to learn about our failures. That's all that's required. We

302
00:26:28,270 --> 00:20:39,680
're not required to succeed in any way.

303
00:20:40,560 --> 00:20:42,910
 Succeeding comes from understanding. It's understanding

304
00:20:42,910 --> 00:20:44,320
 what you're doing wrong.

305
00:20:44,320 --> 00:20:49,820
 The third thing that this verse tells us, of course, is the

306
00:20:49,820 --> 00:20:52,480
 importance of good advice,

307
00:20:52,480 --> 00:20:59,130
 which is why we should never take for granted what we've

308
00:20:59,130 --> 00:21:03,120
 gained from the Buddhist teaching.

309
00:21:04,320 --> 00:21:09,070
 And we should never waste an opportunity. And I'm going to

310
00:21:09,070 --> 00:21:12,240
 sound like a proselytizing theist

311
00:21:12,240 --> 00:21:15,530
 when I say this, but waste an opportunity to share the dumb

312
00:21:15,530 --> 00:21:16,480
 with others.

313
00:21:16,480 --> 00:21:22,230
 I mean, I think it would be fine if Christians and theists

314
00:21:22,230 --> 00:21:25,120
 were, if they went about sharing things,

315
00:21:25,120 --> 00:21:27,610
 let's say, other religions went about sharing their

316
00:21:27,610 --> 00:21:29,760
 religions. If only their religion was true,

317
00:21:29,760 --> 00:21:33,970
 it was the truth. Was it all useful? And in fact, some of

318
00:21:33,970 --> 00:21:35,440
 the things they do share,

319
00:21:35,440 --> 00:21:39,480
 we could say, are useful. So that's okay. But I would be

320
00:21:39,480 --> 00:21:43,040
 completely on board with,

321
00:21:43,040 --> 00:21:47,210
 I mean, I am completely on board. It's not proselytizing or

322
00:21:47,210 --> 00:21:48,640
 sharing is wrong.

323
00:21:48,640 --> 00:21:53,530
 Two things. First, if the teachings are wrong, don't share

324
00:21:53,530 --> 00:21:56,800
 them. Give them up. That's a problem.

325
00:21:57,920 --> 00:22:01,570
 But second, it doesn't mean pushing the teachings on others

326
00:22:01,570 --> 00:22:03,760
. And part of being able to, I think,

327
00:22:03,760 --> 00:22:06,500
 the next set of verses that we're going to learn about, or

328
00:22:06,500 --> 00:22:07,520
 one's coming up,

329
00:22:07,520 --> 00:22:13,520
 we'll talk about this in more detail. But an opportunity to

330
00:22:13,520 --> 00:22:15,360
 share the Dhamma is not,

331
00:22:15,360 --> 00:22:17,880
 "Hey, there's someone I think they should learn. Let me

332
00:22:17,880 --> 00:22:18,640
 teach them."

333
00:22:18,640 --> 00:22:22,070
 An opportunity to share the Dhamma is someone suffering.

334
00:22:22,070 --> 00:22:25,680
 They're asking you, or they are

335
00:22:26,560 --> 00:22:30,420
 close enough to you that you're in a position, you fulfill

336
00:22:30,420 --> 00:22:34,080
 a role as a relative, a friend,

337
00:22:34,080 --> 00:22:44,880
 a companion. You fulfill a role whereby they would, will

338
00:22:44,880 --> 00:22:50,640
 listen and make use of things that you have

339
00:22:50,640 --> 00:22:53,240
 learned. "Hey, I heard about this meditation technique. I

340
00:22:53,240 --> 00:22:54,880
've been trying it. It really works

341
00:22:54,880 --> 00:22:59,730
 for me. If you need help, try it. Maybe it works." Someone

342
00:22:59,730 --> 00:23:03,520
 who's looking, or someone who is

343
00:23:03,520 --> 00:23:12,840
 pleading, is asking, or is open to it. Not to convince. It

344
00:23:12,840 --> 00:23:15,120
's not about convincing people.

345
00:23:16,160 --> 00:23:23,520
 If you have to convince someone beyond simply presenting a

346
00:23:23,520 --> 00:23:25,440
 case, if you have to convince them,

347
00:23:25,440 --> 00:23:32,250
 then it's not likely to turn out all that well. Take the

348
00:23:32,250 --> 00:23:33,840
 Buddha's example.

349
00:23:33,840 --> 00:23:38,880
 For those who were unable to understand, he just smiled.

350
00:23:44,320 --> 00:23:47,060
 I think that's hard for people to swallow. Smiling at other

351
00:23:47,060 --> 00:23:48,560
 people's misfortune.

352
00:23:48,560 --> 00:23:53,440
 Well, what can I say? That's how the Buddha rolls.

353
00:23:53,440 --> 00:24:07,210
 But take the time to share the Dhamma. Remember, some

354
00:24:07,210 --> 00:24:11,680
 people like these two rich people,

355
00:24:13,120 --> 00:24:15,690
 if only when they were young, or if the Buddha had caught

356
00:24:15,690 --> 00:24:16,320
 them earlier,

357
00:24:16,320 --> 00:24:20,010
 they could have done something, and they could have done

358
00:24:20,010 --> 00:24:22,640
 something with their lives, with just

359
00:24:22,640 --> 00:24:27,130
 the right friendship, the right advice, the right support.

360
00:24:27,130 --> 00:24:30,960
 Instead, they fell into wrong friendship.

361
00:24:30,960 --> 00:24:33,830
 And of course, one more thing that this first teaches us is

362
00:24:33,830 --> 00:24:37,760
 the evils of bad friends, the evils

363
00:24:37,760 --> 00:24:44,040
 of alcohol, things like that. Evil, evil, evil. Again, I

364
00:24:44,040 --> 00:24:47,360
 don't want to sound like I'm preaching, but

365
00:24:47,360 --> 00:24:53,440
 I'm not going to argue that alcohol is evil. I know it's

366
00:24:53,440 --> 00:24:55,360
 bad, bad, bad.

367
00:24:55,360 --> 00:25:00,670
 It's the opposite of being mindful. It has the opposite

368
00:25:00,670 --> 00:25:01,680
 effect.

369
00:25:01,680 --> 00:25:07,200
 Mindfulness is about really experiencing reality.

370
00:25:07,840 --> 00:25:12,420
 It's about experiencing all parts of reality. We take

371
00:25:12,420 --> 00:25:15,280
 alcohol to avoid who we are.

372
00:25:15,280 --> 00:25:17,520
 You know, I'm the kind of person who gets upset. I'm the

373
00:25:17,520 --> 00:25:18,880
 kind of person who worries and

374
00:25:18,880 --> 00:25:21,600
 stresses about everything. Well, I'll just take alcohol and

375
00:25:21,600 --> 00:25:23,040
 then I won't worry and stress.

376
00:25:23,040 --> 00:25:27,630
 It's like any drug. It takes you away from your problems

377
00:25:27,630 --> 00:25:30,640
 rather than helps you deal with them.

378
00:25:32,480 --> 00:25:35,400
 And as we can see, and as we see here, and we see other

379
00:25:35,400 --> 00:25:37,840
 places, and as I've talked about earlier,

380
00:25:37,840 --> 00:25:43,480
 alcohol is quite special in that it removes your ability to

381
00:25:43,480 --> 00:25:48,880
 reason. It leads you to squander your

382
00:25:48,880 --> 00:25:55,580
 wealth, leads you to act in terribly embarrassing and un

383
00:25:55,580 --> 00:26:01,120
mindful ways. It removes your mindfulness.

384
00:26:03,040 --> 00:26:08,780
 So a lot to learn from this fun, well, this sad, sad story

385
00:26:08,780 --> 00:26:11,840
 of these two people.

386
00:26:11,840 --> 00:26:19,600
 Wherever they are now, they're certainly getting a lesson,

387
00:26:19,600 --> 00:26:21,680
 wherever that may be.

388
00:26:21,680 --> 00:26:26,850
 So there's the Dhammapada for today. Thank you all for

389
00:26:26,850 --> 00:26:28,080
 tuning in.

