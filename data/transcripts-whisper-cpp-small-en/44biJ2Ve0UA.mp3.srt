1
00:00:00,000 --> 00:00:04,120
 And so that will go up as its own separate video tonight,

2
00:00:04,120 --> 00:00:07,200
 and you'll see it probably tomorrow.

3
00:00:07,200 --> 00:00:09,640
 This video is going to be--

4
00:00:09,640 --> 00:00:18,240
 just answering questions.

5
00:00:24,720 --> 00:00:32,160
 And maybe talking about a verse or a quote.

6
00:00:32,160 --> 00:00:36,240
 So with me tonight is Robin.

7
00:00:36,240 --> 00:00:37,680
 Robin.

8
00:00:37,680 --> 00:00:40,440
 Hello, Monty.

9
00:00:40,440 --> 00:00:47,040
 And all the people at meditation.ceremongolo.org.

10
00:00:47,040 --> 00:00:49,360
 Is there anybody there?

11
00:00:49,360 --> 00:00:50,760
 We have questions.

12
00:00:50,760 --> 00:00:53,160
 Oh, good.

13
00:00:53,160 --> 00:00:55,200
 OK, shoot.

14
00:00:55,200 --> 00:00:57,720
 OK, this is a long question.

15
00:00:57,720 --> 00:01:00,960
 Dear Monty, when one starts practicing satipatthana,

16
00:01:00,960 --> 00:01:03,480
 vipassana, without any prior training

17
00:01:03,480 --> 00:01:07,000
 in any kind of meditation, is it necessary to have

18
00:01:07,000 --> 00:01:09,320
 distraction-free surroundings?

19
00:01:09,320 --> 00:01:10,960
 I understand that the practice has

20
00:01:10,960 --> 00:01:13,380
 to do with being mindful of absolutely any kind of

21
00:01:13,380 --> 00:01:13,960
 experience

22
00:01:13,960 --> 00:01:17,080
 that arises out of the six sense doors,

23
00:01:17,080 --> 00:01:19,560
 no matter how chaotic the environment.

24
00:01:19,560 --> 00:01:22,920
 Always noting the most distinct and strong experience.

25
00:01:22,920 --> 00:01:24,680
 So basically, there is no such thing

26
00:01:24,680 --> 00:01:27,960
 as necessity for distraction-free surroundings.

27
00:01:27,960 --> 00:01:30,560
 But at the beginning, it feels like the practice cannot go

28
00:01:30,560 --> 00:01:33,420
 anywhere when there are so many things happening every

29
00:01:33,420 --> 00:01:34,440
 second.

30
00:01:34,440 --> 00:01:38,760
 I understand that the practice is not about concentration.

31
00:01:38,760 --> 00:01:41,440
 Is one guaranteed to eventually progress on the path

32
00:01:41,440 --> 00:01:44,120
 by following the teaching, even if the practicing

33
00:01:44,120 --> 00:01:44,680
 environment

34
00:01:44,680 --> 00:01:47,240
 is chaotic from the beginning, just

35
00:01:47,240 --> 00:01:50,520
 by being able to have mindful moments here and there?

36
00:01:50,520 --> 00:01:52,360
 Even though momentary concentration

37
00:01:52,360 --> 00:01:54,600
 is the only kind of concentration that has something

38
00:01:54,600 --> 00:01:57,040
 to do with the practice, does one anyway

39
00:01:57,040 --> 00:01:59,520
 need stillness and quietness at the beginning?

40
00:01:59,520 --> 00:02:05,280
 I think you might be confusing two things,

41
00:02:05,280 --> 00:02:08,280
 because it's not the environment.

42
00:02:08,280 --> 00:02:10,480
 But if you're talking about--

43
00:02:10,480 --> 00:02:14,880
 so you mentioned something about just having mindful

44
00:02:14,880 --> 00:02:15,320
 moments

45
00:02:15,320 --> 00:02:17,520
 here and there.

46
00:02:17,520 --> 00:02:20,720
 But OK, see, if you meant by that,

47
00:02:20,720 --> 00:02:22,720
 that you don't have an environment

48
00:02:22,720 --> 00:02:26,120
 that you can actually physically sit down and meditate in,

49
00:02:26,120 --> 00:02:28,470
 like if you do that, people are going to hit you or scold

50
00:02:28,470 --> 00:02:28,760
 you

51
00:02:28,760 --> 00:02:32,440
 or think you're crazy, throw garbage at you or something,

52
00:02:32,440 --> 00:02:34,040
 then yeah, that's problematic.

53
00:02:34,040 --> 00:02:41,960
 If you're talking just about the distraction of your mind,

54
00:02:41,960 --> 00:02:45,160
 well, there are limits, I think.

55
00:02:45,160 --> 00:02:47,520
 And the whole reason for doing formal meditation

56
00:02:47,520 --> 00:02:54,920
 in the first place is to limit our distractions.

57
00:02:54,920 --> 00:02:58,400
 So yes, there is something to what you're saying,

58
00:02:58,400 --> 00:03:05,640
 that it shouldn't be just full contact right away.

59
00:03:05,640 --> 00:03:08,120
 That being said, there are different levels

60
00:03:08,120 --> 00:03:09,400
 of distraction.

61
00:03:09,400 --> 00:03:12,800
 If people are talking around you, that can be really

62
00:03:12,800 --> 00:03:13,440
 difficult.

63
00:03:13,440 --> 00:03:19,560
 But I've had meditators who were able to sit through it.

64
00:03:19,560 --> 00:03:21,280
 I've done it.

65
00:03:21,280 --> 00:03:25,000
 I remember once doing a course in Thailand,

66
00:03:25,000 --> 00:03:27,800
 and there were monks sitting right outside my kuti

67
00:03:27,800 --> 00:03:32,240
 at a table right outside my hut, talking all day.

68
00:03:32,240 --> 00:03:35,520
 They sat there talking right in front of my hut.

69
00:03:35,520 --> 00:03:38,360
 So I remember doing walking and sitting.

70
00:03:38,360 --> 00:03:40,720
 I couldn't really understand.

71
00:03:40,720 --> 00:03:42,000
 It was difficult.

72
00:03:42,000 --> 00:03:47,160
 But eventually, really the key is that eventually your mind

73
00:03:47,160 --> 00:03:50,680
 is able to put it aside.

74
00:03:50,680 --> 00:03:52,680
 And that's really what we're looking for.

75
00:03:52,680 --> 00:03:54,720
 You say your practice isn't able to go anywhere.

76
00:03:54,720 --> 00:03:57,280
 Well, I wonder where you're trying to go.

77
00:03:57,280 --> 00:03:59,400
 It doesn't mean going anywhere.

78
00:03:59,400 --> 00:04:01,280
 As the whole desire to go somewhere

79
00:04:01,280 --> 00:04:02,840
 is really a problem.

80
00:04:02,840 --> 00:04:04,200
 You're not trying to go somewhere.

81
00:04:04,200 --> 00:04:07,240
 You're trying to understand what's going on in your mind.

82
00:04:07,240 --> 00:04:09,400
 So at that moment, what's going on in your mind?

83
00:04:09,400 --> 00:04:11,920
 Your mind is being distracted by things.

84
00:04:11,920 --> 00:04:13,280
 So you're learning about that.

85
00:04:13,280 --> 00:04:15,920
 Meditation is for the purpose of learning about that,

86
00:04:15,920 --> 00:04:17,640
 understanding it.

87
00:04:17,640 --> 00:04:19,720
 What's going on?

88
00:04:19,720 --> 00:04:22,190
 It's about learning about your habits, how you react to

89
00:04:22,190 --> 00:04:22,640
 things.

90
00:04:22,640 --> 00:04:24,880
 It's about learning to react differently,

91
00:04:24,880 --> 00:04:26,920
 learning why your reactions have problems,

92
00:04:26,920 --> 00:04:29,920
 learning about the things that you're reacting to,

93
00:04:29,920 --> 00:04:33,240
 and realizing that they're not worth reacting to.

94
00:04:33,240 --> 00:04:35,680
 So it's actually a very important learning practice.

95
00:04:35,680 --> 00:04:38,600
 Now, if it's overwhelming, then you

96
00:04:38,600 --> 00:04:45,160
 have to decide for yourself how much of a retreat you need.

97
00:04:45,160 --> 00:04:49,840
 But that's certainly not inherently counterproductive.

98
00:04:49,840 --> 00:04:51,320
 It just means you're weak.

99
00:04:51,320 --> 00:04:52,560
 Most of us are fairly weak.

100
00:04:52,560 --> 00:04:55,720
 So if you want, that's what you're suggesting here.

101
00:04:55,720 --> 00:04:59,560
 And yes, you can admit your weakness, and you can back off.

102
00:04:59,560 --> 00:05:01,520
 But you have to admit your weakness and say,

103
00:05:01,520 --> 00:05:03,480
 OK, this isn't a problem with the situation.

104
00:05:03,480 --> 00:05:05,920
 This is a problem with me.

105
00:05:05,920 --> 00:05:08,640
 And then you have to assess as to whether you're hiding

106
00:05:08,640 --> 00:05:14,020
 from your challenges or whether you are retreating

107
00:05:14,020 --> 00:05:15,040
 temporarily,

108
00:05:15,040 --> 00:05:18,120
 because eventually you're going to have to deal with it.

109
00:05:18,120 --> 00:05:21,320
 I remember once I was in Israel, and I

110
00:05:21,320 --> 00:05:25,800
 went and did meditation on the beach in Tel Aviv.

111
00:05:25,800 --> 00:05:27,240
 And these flies came.

112
00:05:27,240 --> 00:05:29,400
 And my whole face was covered in flies.

113
00:05:29,400 --> 00:05:32,160
 Flies fly.

114
00:05:32,160 --> 00:05:32,960
 That was bizarre.

115
00:05:32,960 --> 00:05:35,280
 Well, not covered, but there were maybe five or 10 flies.

116
00:05:35,280 --> 00:05:36,700
 And at first I thought, but if you've

117
00:05:36,700 --> 00:05:41,080
 ever had a fly on your face, imagine five or 10.

118
00:05:41,080 --> 00:05:44,160
 There, it's not like having an ant or something.

119
00:05:44,160 --> 00:05:47,320
 House flies, because they have tongues on their feet

120
00:05:47,320 --> 00:05:48,760
 or something.

121
00:05:48,760 --> 00:05:50,960
 They have really--

122
00:05:50,960 --> 00:05:55,040
 I shook them off and had to walk away.

123
00:05:55,040 --> 00:05:58,960
 And I went and told the teacher, this Israeli man who's now

124
00:05:58,960 --> 00:05:59,400
 a monk--

125
00:05:59,400 --> 00:06:00,880
 he's a regular teacher--

126
00:06:00,880 --> 00:06:05,080
 and he said, Noah, if you don't deal with it now,

127
00:06:05,080 --> 00:06:06,600
 you'll have to deal with it eventually.

128
00:06:06,600 --> 00:06:12,480
 He had this accent, but he had this thick, thick Middle

129
00:06:12,480 --> 00:06:15,640
 Eastern accent.

130
00:06:15,640 --> 00:06:17,640
 He said, whatever experience you don't deal with,

131
00:06:17,640 --> 00:06:19,520
 now you'll have to deal with eventually.

132
00:06:19,520 --> 00:06:21,920
 I'm still a bit skeptical, because man, that

133
00:06:21,920 --> 00:06:25,080
 was pretty tough.

134
00:06:25,080 --> 00:06:26,640
 No, he's right.

135
00:06:26,640 --> 00:06:28,920
 But I don't know that I totally agree

136
00:06:28,920 --> 00:06:31,240
 that you'll have to eventually deal with it.

137
00:06:31,240 --> 00:06:33,240
 That's not really that.

138
00:06:33,240 --> 00:06:36,240
 That's that it's just a sign that you are weak.

139
00:06:36,240 --> 00:06:39,040
 And I think maybe what he was meaning

140
00:06:39,040 --> 00:06:41,560
 and what he was saying was not that eventually I'd

141
00:06:41,560 --> 00:06:43,080
 have to deal with flies on my face.

142
00:06:43,080 --> 00:06:44,520
 And that's not really the point.

143
00:06:44,520 --> 00:06:46,720
 But there's some reason why I can't deal with it,

144
00:06:46,720 --> 00:06:49,240
 why I couldn't deal with the flies on my face.

145
00:06:49,240 --> 00:06:52,120
 And eventually I'm going to have to deal with that reason.

146
00:06:52,120 --> 00:06:52,840
 There's no way out.

147
00:06:52,840 --> 00:06:56,960
 There's no way around our versions,

148
00:06:56,960 --> 00:06:58,640
 no way around our problem.

149
00:06:58,640 --> 00:07:00,240
 Eventually you have to deal with it.

150
00:07:00,240 --> 00:07:03,240
 [SNIFFLING]

151
00:07:03,240 --> 00:07:10,520
 So that's sort of an answer, yeah.

152
00:07:10,520 --> 00:07:12,760
 I don't think anyone needs stillness or quiet,

153
00:07:12,760 --> 00:07:17,520
 but you-- I already explained that you retreat sometimes.

154
00:07:17,520 --> 00:07:19,760
 Just be careful, because I know that I had one meditator

155
00:07:19,760 --> 00:07:20,560
 once.

156
00:07:20,560 --> 00:07:22,240
 I was watching one meditator once.

157
00:07:22,240 --> 00:07:23,600
 I think I've mentioned this.

158
00:07:23,600 --> 00:07:25,760
 He started sitting on the floor, and then we

159
00:07:25,760 --> 00:07:26,720
 said he could use a cushion.

160
00:07:26,720 --> 00:07:28,560
 And so he put a little cushion underneath.

161
00:07:28,560 --> 00:07:31,720
 And I watched him throughout the day to day,

162
00:07:31,720 --> 00:07:33,160
 over a couple of days.

163
00:07:33,160 --> 00:07:36,480
 His seat got higher and higher and higher.

164
00:07:36,480 --> 00:07:38,480
 And he might as well be sitting on a chair.

165
00:07:38,480 --> 00:07:42,760
 And I said, we're really trying to go the other way.

166
00:07:42,760 --> 00:07:45,440
 Normal feet get lower as you go on.

167
00:07:45,440 --> 00:07:51,560
 Get more tolerance, pain, and so on.

168
00:07:51,560 --> 00:07:53,920
 So that's important, just to be careful that you're not

169
00:07:53,920 --> 00:07:55,440
 going in the wrong direction.

170
00:07:55,440 --> 00:07:58,440
 [SNIFFLING]

171
00:07:58,440 --> 00:08:01,480
 Hello, Bante.

172
00:08:01,480 --> 00:08:04,160
 There are times when I become disconcerted by the breath

173
00:08:04,160 --> 00:08:07,280
 that I draw, whether it be too little or too much

174
00:08:07,280 --> 00:08:10,120
 and feel anxious and forced to control it,

175
00:08:10,120 --> 00:08:12,040
 instead of observing it objectively

176
00:08:12,040 --> 00:08:14,040
 as it arises naturally.

177
00:08:14,040 --> 00:08:18,280
 I also have a diagnosed anxiety disorder, and was wondering

178
00:08:18,280 --> 00:08:18,280
,

179
00:08:18,280 --> 00:08:20,600
 is the anxiety acting as a hindrance

180
00:08:20,600 --> 00:08:23,200
 by arising alongside this disruption?

181
00:08:23,200 --> 00:08:26,280
 Or is my anxiety only another object

182
00:08:26,280 --> 00:08:29,080
 that could use more attention as it arises?

183
00:08:29,080 --> 00:08:30,680
 And therefore, is the disruption I

184
00:08:30,680 --> 00:08:34,080
 mentioned only the product of a working, yet challenging

185
00:08:34,080 --> 00:08:35,080
 practice?

186
00:08:35,080 --> 00:08:37,040
 I'm sorry if this is a little confusing.

187
00:08:37,040 --> 00:08:49,880
 Anxiety is acting as a hindrance.

188
00:08:49,880 --> 00:08:50,560
 That's certain.

189
00:08:50,560 --> 00:08:58,160
 But it's only an hindrance in a sense of hindering

190
00:08:58,160 --> 00:09:02,120
 your happiness, your peace of mind.

191
00:09:02,120 --> 00:09:05,520
 In Vipassana, we don't really think of them as hindrances.

192
00:09:05,520 --> 00:09:07,600
 We call them hindrances, but only because that's

193
00:09:07,600 --> 00:09:09,480
 what they're called elsewhere.

194
00:09:09,480 --> 00:09:13,960
 In the practice, hindrances are considered

195
00:09:13,960 --> 00:09:16,640
 important meditation objects.

196
00:09:16,640 --> 00:09:18,680
 So you do take them as a meditation object

197
00:09:18,680 --> 00:09:20,200
 just like anything else.

198
00:09:20,200 --> 00:09:21,720
 And I've talked about anxiety before.

199
00:09:21,720 --> 00:09:25,600
 Not only anxiety, you also have to focus on the--

200
00:09:25,600 --> 00:09:27,080
 as with everything, you have to focus

201
00:09:27,080 --> 00:09:30,320
 on what surrounds the anxiety, the physical aspects.

202
00:09:30,320 --> 00:09:32,640
 So the tension that's going to arise in your breath,

203
00:09:32,640 --> 00:09:36,600
 probably, the headache, sometimes tension

204
00:09:36,600 --> 00:09:39,800
 in the shoulders, the heart beating fast,

205
00:09:39,800 --> 00:09:41,440
 that kind of thing.

206
00:09:41,440 --> 00:09:42,400
 Many different things.

207
00:09:42,400 --> 00:09:43,600
 Fear is involved.

208
00:09:49,440 --> 00:09:53,520
 So you're seeing how your mind works.

209
00:09:53,520 --> 00:09:55,840
 And it works not very well.

210
00:09:55,840 --> 00:09:57,920
 That's normal.

211
00:09:57,920 --> 00:10:05,080
 There's nothing that should be disconcerting about that.

212
00:10:05,080 --> 00:10:08,320
 Shouldn't be something that--

213
00:10:08,320 --> 00:10:09,080
 disheartening.

214
00:10:09,080 --> 00:10:10,840
 There's nothing disheartening about that.

215
00:10:10,840 --> 00:10:18,920
 What you're trying to do is understand and learn

216
00:10:18,920 --> 00:10:22,040
 to let go of what you can let go of.

217
00:10:22,040 --> 00:10:26,560
 And well, through understanding, to not exactly change,

218
00:10:26,560 --> 00:10:28,600
 but the understanding itself will

219
00:10:28,600 --> 00:10:34,280
 change many things about your mind.

220
00:10:34,280 --> 00:10:36,080
 It will help change the anxiety.

221
00:10:36,080 --> 00:10:38,600
 As you watch the anxiety, you're basically

222
00:10:38,600 --> 00:10:42,880
 teaching yourself the problem with anxiety.

223
00:10:42,880 --> 00:10:44,360
 And your mind starts to--

224
00:10:44,360 --> 00:10:50,840
 [AUDIO OUT]

225
00:10:50,840 --> 00:10:53,960
 --starts to change how it reacts to things, being less

226
00:10:53,960 --> 00:10:55,120
 anxious,

227
00:10:55,120 --> 00:10:57,840
 less inclined to react with anxiety,

228
00:10:57,840 --> 00:10:59,400
 or get worried about things.

229
00:10:59,400 --> 00:11:01,840
 Because you see that anxiety is useless.

230
00:11:01,840 --> 00:11:03,040
 It's harmful.

231
00:11:03,040 --> 00:11:03,880
 You really see it.

232
00:11:03,880 --> 00:11:05,680
 Normally, we think, oh, I know it's useful.

233
00:11:05,680 --> 00:11:06,200
 I'm useless.

234
00:11:06,200 --> 00:11:07,400
 I want to get rid of it.

235
00:11:07,400 --> 00:11:09,680
 It's not the same.

236
00:11:09,680 --> 00:11:13,030
 Still have this aspect of our mind that thinks that it's

237
00:11:13,030 --> 00:11:13,520
 good.

238
00:11:13,520 --> 00:11:17,560
 So you change that by learning that it's not good.

239
00:11:17,560 --> 00:11:29,280
 And then just a continuation of the earlier question

240
00:11:29,280 --> 00:11:34,120
 about things being chaotic.

241
00:11:34,120 --> 00:11:36,080
 I kind of meant that the environment is

242
00:11:36,080 --> 00:11:37,680
 chaotic from the beginning.

243
00:11:37,680 --> 00:11:40,360
 Can the meditator ever really have mindful moments?

244
00:11:40,360 --> 00:11:47,200
 [AUDIO OUT]

245
00:11:47,200 --> 00:11:48,160
 Absolutely.

246
00:11:48,160 --> 00:11:50,840
 You have to understand what mindfulness is.

247
00:11:50,840 --> 00:11:53,120
 Even if the environment is chaotic, what does that mean?

248
00:11:53,120 --> 00:11:54,080
 There is sound.

249
00:11:54,080 --> 00:11:56,120
 If there's sound, then you say hearing, hearing,

250
00:11:56,120 --> 00:11:57,800
 and then you're mindful.

251
00:11:57,800 --> 00:12:01,200
 What does it mean to say the environment is chaotic?

252
00:12:01,200 --> 00:12:05,760
 If someone's hitting you, then you say pain, pain.

253
00:12:05,760 --> 00:12:07,160
 Usually, that would mean sound, right?

254
00:12:07,160 --> 00:12:08,840
 The environment is chaotic just means

255
00:12:08,840 --> 00:12:10,280
 there's sound arising at the ear.

256
00:12:10,280 --> 00:12:11,480
 Well, sound is just sound.

257
00:12:11,480 --> 00:12:12,400
 You say hearing.

258
00:12:12,400 --> 00:12:20,120
 So even if your entire meditation was noting hearing,

259
00:12:20,120 --> 00:12:21,640
 that would be OK?

260
00:12:21,640 --> 00:12:23,840
 Sound arises and ceases.

261
00:12:23,840 --> 00:12:25,960
 Did I tell you the story of what was watching someone?

262
00:12:25,960 --> 00:12:27,400
 And they were saying--

263
00:12:27,400 --> 00:12:28,640
 they were teaching meditation.

264
00:12:28,640 --> 00:12:31,880
 And they said, when you see, you say seeing, seeing.

265
00:12:31,880 --> 00:12:33,480
 When you hear, say hearing.

266
00:12:33,480 --> 00:12:35,160
 And as they said that, they were clearly

267
00:12:35,160 --> 00:12:36,440
 following their own advice.

268
00:12:36,440 --> 00:12:40,480
 And as they said, you say hearing, hearing.

269
00:12:40,480 --> 00:12:41,680
 And they literally did this.

270
00:12:41,680 --> 00:12:51,920
 And they stayed like that for a minute or two.

271
00:12:51,920 --> 00:12:55,480
 And the meditator was confused.

272
00:12:55,480 --> 00:13:01,560
 And I was sitting watching this happen infriendly.

273
00:13:01,560 --> 00:13:03,400
 Incredible.

274
00:13:03,400 --> 00:13:07,120
 It means you can become enlightened through sound

275
00:13:07,120 --> 00:13:08,440
 if your mind is clear enough.

276
00:13:08,440 --> 00:13:17,160
 Hey, Bante.

277
00:13:17,160 --> 00:13:20,280
 My area is very dominated by fundamentalist Christians

278
00:13:20,280 --> 00:13:23,040
 who are very judgmental of anyone who doesn't follow

279
00:13:23,040 --> 00:13:25,120
 their particular religion.

280
00:13:25,120 --> 00:13:27,960
 What would be the Buddhist way to approach this?

281
00:13:27,960 --> 00:13:30,040
 I'm not out spreading Buddhism on the street corners

282
00:13:30,040 --> 00:13:31,680
 or making myself known.

283
00:13:31,680 --> 00:13:34,720
 But I do not want to draw unwanted attention to myself

284
00:13:34,720 --> 00:13:36,560
 or be an object of discrimination.

285
00:13:36,560 --> 00:13:48,640
 Nothing hits me right away.

286
00:13:48,640 --> 00:13:51,360
 I mean, be mindful.

287
00:13:51,360 --> 00:13:56,600
 There's a story of a monk who came to the Buddha.

288
00:13:56,600 --> 00:14:00,760
 I think he was an Arahant.

289
00:14:00,760 --> 00:14:02,400
 But he came to the Buddha.

290
00:14:02,400 --> 00:14:06,080
 And he said to the Buddha he was going back to his homeland

291
00:14:06,080 --> 00:14:09,120
 or he was going to stay in a certain land.

292
00:14:09,120 --> 00:14:12,370
 And the Buddha said, oh, well, those people are really

293
00:14:12,370 --> 00:14:13,280
 harsh.

294
00:14:13,280 --> 00:14:15,440
 They're not very friendly.

295
00:14:15,440 --> 00:14:17,360
 He said, what if they say nasty things to you?

296
00:14:17,360 --> 00:14:19,520
 What will you do?

297
00:14:19,520 --> 00:14:22,480
 I said, Venerable Sir, if they say nasty things to me,

298
00:14:22,480 --> 00:14:25,350
 I'll just think to myself, oh, these people are so

299
00:14:25,350 --> 00:14:26,640
 civilized

300
00:14:26,640 --> 00:14:29,000
 that they don't hit me.

301
00:14:29,000 --> 00:14:31,960
 And the Buddha said, well, what if they hit you?

302
00:14:31,960 --> 00:14:33,400
 And he said, well, then I'll just

303
00:14:33,400 --> 00:14:35,840
 think these people are so civilized that they

304
00:14:35,840 --> 00:14:39,720
 don't break my bones or cause real harm.

305
00:14:39,720 --> 00:14:44,240
 And he said, well, what if they cause real harm?

306
00:14:44,240 --> 00:14:45,640
 And then he said, well, if they do,

307
00:14:45,640 --> 00:14:47,800
 I'll just think, well, these people are civilized,

308
00:14:47,800 --> 00:14:52,280
 so civilized that they don't kill me.

309
00:14:52,280 --> 00:14:55,720
 He said, what if they kill you?

310
00:14:55,720 --> 00:14:57,680
 And he said, if they kill me, Venerable Sir,

311
00:14:57,680 --> 00:15:05,920
 then I'll just think to myself, many of the beings who

312
00:15:05,920 --> 00:15:07,760
 have sought for an assassin--

313
00:15:07,760 --> 00:15:08,760
 it's kind of weird.

314
00:15:08,760 --> 00:15:11,960
 And I think you have to be an Arahant to really understand

315
00:15:11,960 --> 00:15:11,960
--

316
00:15:11,960 --> 00:15:13,120
 well, not even an Arahant.

317
00:15:13,120 --> 00:15:16,880
 You have to understand the idea of an Arahant to get this.

318
00:15:16,880 --> 00:15:23,240
 He said, many, many are the enlightened beings.

319
00:15:23,240 --> 00:15:28,600
 Many followers of the Buddha have wished for an assassin,

320
00:15:28,600 --> 00:15:32,040
 someone to end their life.

321
00:15:32,040 --> 00:15:34,040
 I don't know if wished is exactly the right word,

322
00:15:34,040 --> 00:15:38,280
 but what he's trying to say is that there's

323
00:15:38,280 --> 00:15:40,080
 no problem with death.

324
00:15:40,080 --> 00:15:43,440
 Death isn't a problem because life is--

325
00:15:43,440 --> 00:15:45,960
 there's no meaning anymore for a person who is enlightened.

326
00:15:45,960 --> 00:15:49,080
 They are free from attachment.

327
00:15:49,080 --> 00:15:52,600
 And meaning he had no fear of death.

328
00:15:52,600 --> 00:15:53,760
 It wasn't a problem for him.

329
00:15:53,760 --> 00:15:55,560
 He said, OK, in that case, you're

330
00:15:55,560 --> 00:15:57,800
 ready to go and live with these people.

331
00:15:57,800 --> 00:15:58,400
 And he did.

332
00:15:58,400 --> 00:16:00,240
 And he actually established the Sangha there.

333
00:16:00,240 --> 00:16:03,000
 That's what the suta ends by saying, he went and lived

334
00:16:03,000 --> 00:16:03,160
 there.

335
00:16:03,160 --> 00:16:06,720
 And he established a Buddhist Sangha people meditating

336
00:16:06,720 --> 00:16:08,320
 to be free from suffering.

337
00:16:08,320 --> 00:16:12,840
 And a place was full of barbarians.

338
00:16:12,840 --> 00:16:17,800
 So I think some of these places are kind of barbaric.

339
00:16:17,800 --> 00:16:21,880
 People are-- you're probably not surrounded by barbarians,

340
00:16:21,880 --> 00:16:25,720
 but to some extent, religious fundamentalists

341
00:16:25,720 --> 00:16:26,840
 can be pretty barbaric.

342
00:16:26,840 --> 00:16:33,480
 Very judgmental.

343
00:16:33,480 --> 00:16:35,440
 So you just laugh it off.

344
00:16:35,440 --> 00:16:38,750
 You have to learn to be mindful and not let it be like

345
00:16:38,750 --> 00:16:39,240
 water

346
00:16:39,240 --> 00:16:42,520
 off of a lotus leaf.

347
00:16:42,520 --> 00:16:45,440
 It's like a lotus doesn't get wet.

348
00:16:45,440 --> 00:16:50,920
 Water, the raindrops, they just fall off.

349
00:16:50,920 --> 00:16:54,600
 [END PLAYBACK]

350
00:16:54,600 --> 00:16:57,000
 Like, what do we say in English, water often ducks back?

351
00:16:57,000 --> 00:16:57,640
 Is that what we say?

352
00:16:57,640 --> 00:16:58,920
 You say something like that?

353
00:16:58,920 --> 00:16:59,420
 Yes.

354
00:16:59,420 --> 00:17:01,920
 [LAUGHS]

355
00:17:01,920 --> 00:17:08,560
 Best way to deal with Christians--

356
00:17:08,560 --> 00:17:10,600
 I've talked about this before-- religious people

357
00:17:10,600 --> 00:17:12,000
 is to ask them about their religion.

358
00:17:12,000 --> 00:17:13,920
 Learn about it.

359
00:17:13,920 --> 00:17:15,000
 It takes energy, though.

360
00:17:15,000 --> 00:17:17,960
 I think after a while, you'd probably get tired.

361
00:17:17,960 --> 00:17:20,800
 But if they approach you, it is often the best way

362
00:17:20,800 --> 00:17:22,880
 just to just ask them--

363
00:17:22,880 --> 00:17:25,160
 or else just be silent, I suppose.

364
00:17:25,160 --> 00:17:26,720
 You really just don't want to talk.

365
00:17:26,720 --> 00:17:29,360
 You just don't say anything.

366
00:17:29,360 --> 00:17:31,320
 But if you ask them questions, they

367
00:17:31,320 --> 00:17:40,600
 end up tying their own noose, hanging themselves,

368
00:17:40,600 --> 00:17:46,600
 meaning they end up contradicting themselves,

369
00:17:46,600 --> 00:17:51,450
 because these religions are not very well-founded on

370
00:17:51,450 --> 00:17:51,920
 reality.

371
00:17:51,920 --> 00:17:58,160
 It's by being unchallenged and unquestioned--

372
00:17:58,160 --> 00:18:07,160
 not challenged, but not being forced to back up

373
00:18:07,160 --> 00:18:09,520
 their claims and that kind of thing--

374
00:18:09,520 --> 00:18:12,000
 that they continue that they're able to stay strong.

375
00:18:12,000 --> 00:18:14,440
 So there's that.

376
00:18:14,440 --> 00:18:21,280
 [AUDIO OUT]

377
00:18:21,280 --> 00:18:26,200
 And a follow-up on the question about concentration

378
00:18:26,200 --> 00:18:28,240
 in a chaotic environment.

379
00:18:28,240 --> 00:18:31,880
 But if there is absolutely no concentration,

380
00:18:31,880 --> 00:18:34,500
 can someone without any prior experience in meditation

381
00:18:34,500 --> 00:18:37,840
 just decide to be mindful, now in a chaotic situation,

382
00:18:37,840 --> 00:18:39,560
 and have benefit?

383
00:18:39,560 --> 00:18:41,160
 Don't worry about concentration.

384
00:18:41,160 --> 00:18:45,160
 Concentration arises through mindfulness.

385
00:18:45,160 --> 00:18:47,320
 Yeah, you start being mindful in one moment.

386
00:18:47,320 --> 00:18:50,440
 A person who's new to it is just not very good

387
00:18:50,440 --> 00:18:53,800
 at creating mindfulness.

388
00:18:53,800 --> 00:18:57,200
 Yeah, no, maybe you're right.

389
00:18:57,200 --> 00:19:00,760
 It is a point that they're probably just going

390
00:19:00,760 --> 00:19:04,680
 to get distracted and frustrated and so on.

391
00:19:04,680 --> 00:19:08,240
 But I think that's more of a view than anything.

392
00:19:08,240 --> 00:19:13,240
 If you have the view that, oh, I can't meditate.

393
00:19:13,240 --> 00:19:16,280
 This is too chaotic, then that's the problem.

394
00:19:16,280 --> 00:19:18,640
 So if someone-- like I've explained,

395
00:19:18,640 --> 00:19:25,480
 and as Simon, I think, added in something useful there,

396
00:19:25,480 --> 00:19:28,680
 if you can see it just as experience,

397
00:19:28,680 --> 00:19:31,000
 then it's not very difficult to be mindful.

398
00:19:31,000 --> 00:19:33,240
 And it's, in fact, quite rewarding.

399
00:19:33,240 --> 00:19:35,040
 But again, it is more challenging.

400
00:19:35,040 --> 00:19:38,480
 It's much easier to have a quiet place

401
00:19:38,480 --> 00:19:41,600
 to meditate with one thing to be mindful of.

402
00:19:41,600 --> 00:19:45,480
 Still very challenging.

403
00:19:45,480 --> 00:19:47,280
 Say you had a choice, Bonté.

404
00:19:47,280 --> 00:19:50,640
 Say the house is noisy, but the backyard is quiet.

405
00:19:50,640 --> 00:19:54,000
 Is there anything inherently wrong with just seeking out

406
00:19:54,000 --> 00:19:54,760
 the quiet spot?

407
00:19:54,760 --> 00:20:00,880
 Well, it makes it--

408
00:20:00,880 --> 00:20:03,680
 it is indulging a preference, right?

409
00:20:03,680 --> 00:20:05,400
 Potentially.

410
00:20:05,400 --> 00:20:06,600
 There are other reasons, I suppose.

411
00:20:06,600 --> 00:20:14,560
 Could be endorsing quiet in the sense

412
00:20:14,560 --> 00:20:18,080
 that the reasons why it's loud could

413
00:20:18,080 --> 00:20:22,240
 be that people are engaging in entertainment or talking,

414
00:20:22,240 --> 00:20:25,360
 yacking, chatting, that kind of thing.

415
00:20:25,360 --> 00:20:27,720
 But if your preference, if you're not

416
00:20:27,720 --> 00:20:39,120
 able to deal with the noise, if it's just a matter of music

417
00:20:39,120 --> 00:20:40,720
 or--

418
00:20:40,720 --> 00:20:41,880
 I don't know.

419
00:20:41,880 --> 00:20:43,440
 There are certain noises that are difficult.

420
00:20:43,440 --> 00:20:46,560
 Music is probably difficult. But definitely talking

421
00:20:46,560 --> 00:20:50,200
 is difficult. Difficult, why?

422
00:20:50,200 --> 00:20:56,680
 Because your mind gets naturally involved in that.

423
00:20:56,680 --> 00:20:58,600
 It's really distracting.

424
00:20:58,600 --> 00:21:00,920
 It's really difficult to stay undistracted.

425
00:21:00,920 --> 00:21:04,680
 Because our minds are receptive of that.

426
00:21:04,680 --> 00:21:06,920
 But just loud noises, like let's say a big one

427
00:21:06,920 --> 00:21:13,400
 where I were in Stony Creek was the car noises.

428
00:21:13,400 --> 00:21:15,280
 And that is just noise.

429
00:21:15,280 --> 00:21:17,840
 And it's interesting because we don't get irritated by it

430
00:21:17,840 --> 00:21:20,720
 and distracted, and it interrupts our concentration.

431
00:21:20,720 --> 00:21:23,280
 But that's kind of a good thing.

432
00:21:23,280 --> 00:21:25,960
 It lets you see how you react to things.

433
00:21:25,960 --> 00:21:32,400
 I think there's definitely a potential problem.

434
00:21:32,400 --> 00:21:37,960
 Like if in that case you went and sought out a quieter spot

435
00:21:37,960 --> 00:21:37,960
,

436
00:21:37,960 --> 00:21:39,640
 you're only going to hurt yourself in the end

437
00:21:39,640 --> 00:21:44,080
 because you're going to be more averse to noise.

438
00:21:44,080 --> 00:21:47,640
 Like when I had this alarm clock ticking, tick, tick, tick.

439
00:21:47,640 --> 00:21:49,600
 And I had to--

440
00:21:49,600 --> 00:21:51,720
 I got more and more fed up until finally I just

441
00:21:51,720 --> 00:21:55,160
 had to suppress the sound.

442
00:21:55,160 --> 00:21:57,840
 And then when I got moved to another place that had two

443
00:21:57,840 --> 00:22:00,880
 clocks on the wall and this ticking, it just--

444
00:22:00,880 --> 00:22:03,000
 it made me insane.

445
00:22:03,000 --> 00:22:04,760
 And then I realized, this isn't the way

446
00:22:04,760 --> 00:22:07,320
 I have to go another way.

447
00:22:07,320 --> 00:22:08,600
 I learned just to be mindful.

448
00:22:08,600 --> 00:22:10,920
 Like it was actually affecting my walking.

449
00:22:10,920 --> 00:22:12,480
 Lifting, yep.

450
00:22:12,480 --> 00:22:14,960
 Ding, mmm, thing, play, sing.

451
00:22:14,960 --> 00:22:18,160
 Because I couldn't get out of sync with the tick.

452
00:22:18,160 --> 00:22:20,440
 Until finally I just said, OK, if I'm going to be in sync,

453
00:22:20,440 --> 00:22:21,480
 I'm going to be in sync.

454
00:22:21,480 --> 00:22:23,640
 I have to learn to let go of this.

455
00:22:23,640 --> 00:22:24,360
 And I really did.

456
00:22:24,360 --> 00:22:26,200
 It was really quite rewarding.

457
00:22:26,200 --> 00:22:41,440
 And I think with that, we're all caught up with questions.

458
00:22:41,440 --> 00:22:45,320
 OK.

459
00:22:45,320 --> 00:22:47,120
 Well done.

460
00:22:47,120 --> 00:22:48,280
 Good night, everyone.

461
00:22:48,280 --> 00:22:49,720
 Thank you, Robin, for your help.

462
00:22:49,720 --> 00:22:52,080
 Thanks, everyone, for your questions and for watching

463
00:22:52,080 --> 00:22:55,080
 and getting involved.

464
00:22:55,080 --> 00:22:57,440
 I haven't had a panel up soon.

465
00:22:57,440 --> 00:22:59,360
 Thank you, Bante.

466
00:22:59,360 --> 00:23:21,840
 [AUDIO OUT]

467
00:23:21,840 --> 00:23:22,840
 Thank you, Bante.

468
00:23:22,840 --> 00:23:24,400
 Thank you.

469
00:23:24,400 --> 00:23:28,880
 [AUDIO OUT]

