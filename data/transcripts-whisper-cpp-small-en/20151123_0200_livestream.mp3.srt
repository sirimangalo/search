1
00:00:00,000 --> 00:00:07,000
 good evening everyone

2
00:00:07,000 --> 00:00:14,880
 we're broadcasting live

3
00:00:14,880 --> 00:00:19,120
 broadcasting live

4
00:00:19,120 --> 00:00:26,120
 October, November 22nd

5
00:00:28,120 --> 00:00:31,060
 so I was talking today with

6
00:00:31,060 --> 00:00:38,060
 one of the meditators

7
00:00:38,060 --> 00:00:53,880
 we were talking about

8
00:00:53,880 --> 00:00:56,680
 Sasha

9
00:00:56,680 --> 00:00:59,320
 we're talking about

10
00:00:59,320 --> 00:01:02,720
 the Dhammapada

11
00:01:02,720 --> 00:01:06,460
 and he's making a case for how

12
00:01:06,460 --> 00:01:09,360
 how useful it is and how important it is

13
00:01:09,360 --> 00:01:12,040
 how beneficial it is and how I keep dissing it

14
00:01:12,040 --> 00:01:13,440
 keep saying how

15
00:01:13,440 --> 00:01:16,280
 how useless it is

16
00:01:16,280 --> 00:01:18,490
 how much my other videos are so much better and you should

17
00:01:18,490 --> 00:01:19,440
 all go listen to my other

18
00:01:19,440 --> 00:01:26,200
 well go watch my other videos because there's so much more

19
00:01:26,200 --> 00:01:28,040
 useful

20
00:01:28,040 --> 00:01:29,520
 so he challenged that

21
00:01:29,520 --> 00:01:33,880
 I understand

22
00:01:33,880 --> 00:01:36,680
 but I argued back

23
00:01:36,680 --> 00:01:38,560
 that

24
00:01:38,560 --> 00:01:43,640
 his idea was that it's

25
00:01:43,640 --> 00:01:45,970
 good encouragement to people who are meditating but my

26
00:01:45,970 --> 00:01:47,360
 point was what if that's all people

27
00:01:47,360 --> 00:01:48,800
 ever get

28
00:01:48,800 --> 00:01:51,720
 when new people come to my channel that's all they see now

29
00:01:51,720 --> 00:01:54,810
 because those are the latest videos and if they don't ever

30
00:01:54,810 --> 00:01:55,760
 go back and watch

31
00:01:55,760 --> 00:01:56,920
 my old videos

32
00:01:56,920 --> 00:01:58,160
 get some

33
00:01:58,160 --> 00:01:59,080
 find that I've

34
00:01:59,080 --> 00:02:00,790
 the other things I've been teaching or maybe they look at

35
00:02:00,790 --> 00:02:02,320
 them and say oh that's the old stuff

36
00:02:02,320 --> 00:02:04,400
 let's look at what he's doing now which

37
00:02:04,400 --> 00:02:05,920
 I think that's reasonable

38
00:02:05,920 --> 00:02:09,990
 and this is all they get is the Dhammapada stories it's not

39
00:02:09,990 --> 00:02:11,800
 really enough

40
00:02:11,800 --> 00:02:14,960
 I have a suggestion

41
00:02:14,960 --> 00:02:18,760
 didn't you you started Buddhism 101

42
00:02:18,760 --> 00:02:21,870
 yeah yeah that's actually what I just thought today I

43
00:02:21,870 --> 00:02:22,560
 thought I better finish

44
00:02:22,560 --> 00:02:26,250
 what I should really do is continue Buddhism 101 because I

45
00:02:26,250 --> 00:02:27,240
 stopped that in a lurch

46
00:02:27,240 --> 00:02:29,720
 before we actually got to Buddhism

47
00:02:29,720 --> 00:02:30,800
 maybe

48
00:02:30,800 --> 00:02:35,520
 two Dhammapada and one Buddhism 101 videos in a week

49
00:02:35,520 --> 00:02:36,680
 would that be a

50
00:02:36,680 --> 00:02:39,880
 yeah and Buddhism 101 can be quite extended because there's

51
00:02:39,880 --> 00:02:43,680
 lots of good 101 topics

52
00:02:43,680 --> 00:02:47,480
 but that may have to wait until January

53
00:02:47,480 --> 00:02:51,320
 it doesn't have to but maybe I will wait until January

54
00:02:51,320 --> 00:02:55,040
 just because I can put more attention on it then

55
00:02:55,040 --> 00:02:58,520
 I'll just quit school

56
00:02:58,520 --> 00:03:01,520
 really focus on teaching I mean this week I have to write

57
00:03:01,520 --> 00:03:02,120
 an essay

58
00:03:02,120 --> 00:03:06,240
 about John Locke and free will

59
00:03:06,240 --> 00:03:08,420
 and Locke is pretty good I mean as far as being able to

60
00:03:08,420 --> 00:03:10,040
 understand him but the people I have to

61
00:03:10,040 --> 00:03:12,760
 write about the people who write about him

62
00:03:12,760 --> 00:03:14,930
 and the people who write about him are really hard to

63
00:03:14,930 --> 00:03:15,640
 understand

64
00:03:15,640 --> 00:03:21,040
 it's likely to try to out intellectualize him or something

65
00:03:21,040 --> 00:03:23,560
 so Locke is you know you have to

66
00:03:23,560 --> 00:03:26,120
 it's a bit challenging to read

67
00:03:26,120 --> 00:03:27,440
 and it's like

68
00:03:27,440 --> 00:03:31,440
 they feel like they have to be more challenging to read or

69
00:03:31,440 --> 00:03:33,080
 something

70
00:03:33,080 --> 00:03:36,200
 that's school

71
00:03:36,200 --> 00:03:38,480
 I was working on that last night

72
00:03:38,480 --> 00:03:40,640
 started my essay

73
00:03:40,640 --> 00:03:46,120
 due next weekend

74
00:03:46,120 --> 00:03:49,240
 and then exams

75
00:03:49,240 --> 00:03:52,440
 and then after exams I'm flying to Florida

76
00:03:52,440 --> 00:03:54,680
 and then after Florida I think that's it

77
00:03:54,680 --> 00:03:58,800
 no more school for me

78
00:03:58,800 --> 00:04:00,560
 it's just not worth it

79
00:04:00,560 --> 00:04:03,360
 not with the change not with all this new work

80
00:04:03,360 --> 00:04:05,520
 new teaching

81
00:04:05,520 --> 00:04:09,240
 stuff and then we can add more hours to the

82
00:04:09,240 --> 00:04:10,920
 appointments

83
00:04:10,920 --> 00:04:14,520
 add some more slots

84
00:04:14,520 --> 00:04:17,880
 that might be complicated because then I had the only issue

85
00:04:17,880 --> 00:04:18,920
 with the slots right now is

86
00:04:18,920 --> 00:04:22,840
 remembering who has told me they can't make it on a week so

87
00:04:22,840 --> 00:04:22,920
 that

88
00:04:22,920 --> 00:04:25,800
 if they haven't told me then I cancel them

89
00:04:25,800 --> 00:04:29,640
 and then we don't and then they lose their slot

90
00:04:29,640 --> 00:04:33,440
 but if they have told me then I keep them

91
00:04:33,440 --> 00:04:35,900
 so now I'm afraid to cancel because I can't remember if

92
00:04:35,900 --> 00:04:37,640
 they told me or not

93
00:04:37,640 --> 00:04:40,280
 and sometimes it seems like there's technical difficulties

94
00:04:40,280 --> 00:04:42,510
 and a couple of people have said they didn't get the button

95
00:04:42,510 --> 00:04:42,560
 to pop up

96
00:04:42,560 --> 00:04:43,680
 the other thing is

97
00:04:43,680 --> 00:04:48,360
 well the button hasn't been a problem lately I don't think

98
00:04:48,360 --> 00:04:50,000
 people maybe aren't using it so

99
00:04:50,000 --> 00:04:53,560
 Samantha just had a problem with it this past weekend

100
00:04:53,560 --> 00:04:58,960
 she was waiting and it didn't pop up for her

101
00:04:58,960 --> 00:05:05,600
 we didn't know that

102
00:05:05,600 --> 00:05:07,960
 well the bigger problem has been people

103
00:05:07,960 --> 00:05:11,190
 calling me like just now someone called me I didn't get a

104
00:05:11,190 --> 00:05:11,480
 call

105
00:05:11,480 --> 00:05:13,240
 I don't think

106
00:05:13,240 --> 00:05:16,040
 actually was

107
00:05:16,040 --> 00:05:17,950
 I may have been out of the room when they called but it

108
00:05:17,950 --> 00:05:20,200
 didn't notify me that someone was trying to call me or had

109
00:05:20,200 --> 00:05:22,440
 tried to call me

110
00:05:22,440 --> 00:05:29,440
 it's really erratic

111
00:05:29,440 --> 00:05:37,360
 and that's happened quite a few times

112
00:05:37,360 --> 00:05:40,560
 it's nice to know that the computers are reaffirming

113
00:05:40,560 --> 00:05:45,760
 the three characteristics

114
00:05:45,760 --> 00:05:49,200
 if it was always stable then we'd think oh yes

115
00:05:49,200 --> 00:05:53,380
 what is this nonsense about impermanent suffering and non-

116
00:05:53,380 --> 00:05:53,400
self

117
00:05:53,400 --> 00:05:55,560
 here we have something that's permanent that's stable

118
00:05:55,560 --> 00:05:58,680
 satisfying and controllable

119
00:05:58,680 --> 00:06:00,880
 well we know that's not the case

120
00:06:00,880 --> 00:06:05,600
 so good for us well then Google

121
00:06:05,600 --> 00:06:11,040
 teaching us Buddhism

122
00:06:11,040 --> 00:06:14,100
 if someone is due for an appointment but the button doesn't

123
00:06:14,100 --> 00:06:15,440
 pop up can they just

124
00:06:15,440 --> 00:06:18,550
 create a just invite you to a regular hangout would it be

125
00:06:18,550 --> 00:06:19,280
 any different

126
00:06:19,280 --> 00:06:22,590
 no that's exactly what it does the button just creates a

127
00:06:22,590 --> 00:06:23,200
 hangout and

128
00:06:23,200 --> 00:06:25,290
 tells you to invite me and then you click on invite you

129
00:06:25,290 --> 00:06:26,720
 have to actually still invite me

130
00:06:26,720 --> 00:06:32,200
 it just gives you my email address you just click invite

131
00:06:32,200 --> 00:06:35,520
 so rather than miss an appointment someone could just start

132
00:06:35,520 --> 00:06:38,000
 up a hangout and invite you

133
00:06:38,000 --> 00:06:45,000
 to go to the meeting

134
00:06:45,000 --> 00:06:48,100
 if we had two more people if we had probably four more

135
00:06:48,100 --> 00:06:50,240
 slots every day we'll see if I can handle that

136
00:06:50,240 --> 00:06:53,920
 should be able to maybe not you know because some days

137
00:06:53,920 --> 00:06:55,120
 there are other things going on

138
00:06:55,120 --> 00:06:58,720
 it's interesting can I really add more slots every day

139
00:06:58,720 --> 00:07:01,040
 maybe Monday to Friday maybe I'll keep the weekends a

140
00:07:01,040 --> 00:07:03,720
 little more open

141
00:07:03,720 --> 00:07:06,480
 and could you add another

142
00:07:06,480 --> 00:07:10,560
 tab to the meditation website here

143
00:07:10,560 --> 00:07:13,680
 sort of a message board for

144
00:07:13,680 --> 00:07:15,600
 you know you're saying you're not sure when people tell you

145
00:07:15,600 --> 00:07:20,280
 that they can't make it

146
00:07:20,280 --> 00:07:23,560
 like for example you know similar to

147
00:07:23,560 --> 00:07:27,680
 the chat but if somebody put their message in chat it would

148
00:07:27,680 --> 00:07:28,320
 get lost

149
00:07:28,320 --> 00:07:31,040
 but if you had a dedicated panel

150
00:07:31,040 --> 00:07:34,800
 or messages strictly related to the meditation meetings

151
00:07:34,800 --> 00:07:37,560
 that might help to keep things organized

152
00:07:37,560 --> 00:07:44,200
 just creating more work for you

153
00:07:44,200 --> 00:07:51,200
 but it might help to keep things more organized

154
00:07:51,200 --> 00:07:58,200
 maybe I could add a notes section to everyone's profile

155
00:07:58,200 --> 00:07:58,200
 that they could add notes that I that

156
00:08:20,240 --> 00:08:24,720
 that I could see so that when I click on their profile then

157
00:08:24,720 --> 00:08:25,200
 I'd see

158
00:08:25,200 --> 00:08:29,600
 I'm not going to be here this week this day I'll tell them

159
00:08:29,600 --> 00:08:30,920
 to put the note on their profile page

160
00:08:30,920 --> 00:08:33,520
 because that's where I go right away to see is this person

161
00:08:33,520 --> 00:08:35,680
 meditating why isn't this person

162
00:08:35,680 --> 00:08:40,320
 calling me that's a good thing

163
00:08:40,320 --> 00:08:47,320
 especially with the holidays coming up people may be away

164
00:08:47,320 --> 00:08:54,320
 but we have for them today

165
00:08:54,320 --> 00:09:10,160
 good quote

166
00:09:10,160 --> 00:09:13,560
 now

167
00:09:13,560 --> 00:09:20,560
 so let's not go there we have any questions we have

168
00:09:20,560 --> 00:09:20,560
 questions

169
00:09:20,560 --> 00:09:24,320
 this is this is a nice question

170
00:09:24,320 --> 00:09:27,350
 venerable sir is it better to have a shorter teaching video

171
00:09:27,350 --> 00:09:29,960
 to appease a larger group of viewers or better to have a

172
00:09:29,960 --> 00:09:30,320
 longer

173
00:09:30,320 --> 00:09:31,640
 teaching video

174
00:09:31,640 --> 00:09:34,460
 for the fewer viewers that care about each side story and

175
00:09:34,460 --> 00:09:36,360
 subtlety and carefully consider

176
00:09:36,360 --> 00:09:43,360
 your every movement while conveying the sat-dam dharma

177
00:09:43,360 --> 00:09:50,360
 the

178
00:09:50,360 --> 00:09:55,080
 that a trick question I think it's definitely related to

179
00:09:55,080 --> 00:09:55,480
 the

180
00:09:55,480 --> 00:09:58,840
 the comment that you relate that someone said your

181
00:09:58,840 --> 00:10:03,000
 your last time I thought it was too long

182
00:10:03,000 --> 00:10:09,680
 weird

183
00:10:09,680 --> 00:10:15,200
 we say in Thailand and it's actually probably but in

184
00:10:15,200 --> 00:10:15,200
 Thailand

185
00:10:15,200 --> 00:10:18,200
 not yet means

186
00:10:18,200 --> 00:10:24,280
 mind is I guess it means mind is

187
00:10:24,280 --> 00:10:27,680
 manifold various mind is very

188
00:10:27,680 --> 00:10:30,240
 mind varies

189
00:10:30,240 --> 00:10:34,530
 like to each their own basically everyone has a different

190
00:10:34,530 --> 00:10:35,960
 opinion there are many many different

191
00:10:35,960 --> 00:10:39,000
 opinions

192
00:10:39,000 --> 00:10:40,360
 none and to

193
00:10:40,360 --> 00:10:42,360
 weird word I think they're corrupting the polly

194
00:10:42,360 --> 00:10:46,960
 actually

195
00:10:46,960 --> 00:10:53,960
 be not a good time probably

196
00:10:53,960 --> 00:11:00,960
 the next question

197
00:11:00,960 --> 00:11:16,960
 next question

198
00:11:16,960 --> 00:11:19,680
 why did I answer that one already

199
00:11:19,680 --> 00:11:20,840
 I don't think so

200
00:11:20,840 --> 00:11:23,120
 what was it about

201
00:11:23,120 --> 00:11:26,880
 the question is is it better to have shorter teachings that

202
00:11:26,880 --> 00:11:26,880
 appear

203
00:11:26,880 --> 00:11:30,880
 that appeal to a larger group or longer teachings that

204
00:11:30,880 --> 00:11:37,880
 appeal to your more dedicated followers

205
00:11:37,880 --> 00:11:43,640
 well it's interesting talking last night

206
00:11:43,640 --> 00:11:45,520
 I brought up the idea that

207
00:11:45,520 --> 00:11:50,180
 a doctor isn't worried about people who are already on the

208
00:11:50,180 --> 00:11:50,640
 mend

209
00:11:50,640 --> 00:11:52,990
 they only concern themselves with people who will die

210
00:11:52,990 --> 00:11:57,160
 without their care

211
00:11:57,160 --> 00:11:59,640
 there's that

212
00:11:59,640 --> 00:12:04,760
 some extent we need to reach those people who are

213
00:12:04,760 --> 00:12:07,160
 not easily impressed

214
00:12:07,160 --> 00:12:14,160
 reach those people who are not already hooked

215
00:12:14,160 --> 00:12:17,580
 but that's not entirely fair because to some extent we have

216
00:12:17,580 --> 00:12:19,760
 to help those people who

217
00:12:19,760 --> 00:12:22,320
 are actually going to take the medicine

218
00:12:22,320 --> 00:12:25,740
 you don't want to pander to people who aren't even going to

219
00:12:25,740 --> 00:12:26,920
 take the medicine

220
00:12:26,920 --> 00:12:28,400
 why waste all your time

221
00:12:28,400 --> 00:12:31,680
 teaching people who aren't even going to practice

222
00:12:31,680 --> 00:12:37,520
 so that's a good point

223
00:12:37,520 --> 00:12:39,880
 thank you Monte

224
00:12:39,880 --> 00:12:42,410
 I'm trying to find a good way to respond to cravings when

225
00:12:42,410 --> 00:12:43,960
 not meditating

226
00:12:43,960 --> 00:12:45,680
 right now it goes like this

227
00:12:45,680 --> 00:12:48,520
 there's a sight or a thought that starts a craving

228
00:12:48,520 --> 00:12:50,760
 the mind locks onto that sight or thought

229
00:12:50,760 --> 00:12:52,640
 there's an unpleasant feeling

230
00:12:52,640 --> 00:12:54,600
 I remember to be mindful

231
00:12:54,600 --> 00:12:57,110
 there is an awareness that there is an aversion to the

232
00:12:57,110 --> 00:12:57,560
 craving

233
00:12:57,560 --> 00:13:00,400
 I say disliking, disliking

234
00:13:00,400 --> 00:13:03,400
 or averting, averting or something

235
00:13:03,400 --> 00:13:06,220
 this goes on for as short as 30 seconds or sometimes for 10

236
00:13:06,220 --> 00:13:07,880
 minutes or longer

237
00:13:07,880 --> 00:13:10,040
 rather than coming to an end

238
00:13:10,040 --> 00:13:12,510
 usually another sight or thought comes along and the mind

239
00:13:12,510 --> 00:13:14,880
 follows going in a different direction

240
00:13:14,880 --> 00:13:17,820
 you have often talked about a middle way for responding to

241
00:13:17,820 --> 00:13:18,360
 cravings

242
00:13:18,360 --> 00:13:25,360
 which you elaborate

243
00:13:25,360 --> 00:13:38,280
 I don't quite understand what the question is

244
00:13:38,280 --> 00:13:41,400
 like as though you have as though

245
00:13:41,400 --> 00:13:44,960
 something's wrong with what you're describing

246
00:13:44,960 --> 00:13:49,760
 I mean something is wrong but you know what's wrong

247
00:13:49,760 --> 00:13:51,400
 what's wrong is

248
00:13:51,400 --> 00:13:52,640
 reality is wrong

249
00:13:52,640 --> 00:13:55,440
 it's impermanent suffering and non-self and that's what you

250
00:13:55,440 --> 00:13:56,440
're experiencing

251
00:13:56,440 --> 00:13:57,640
 the chaos

252
00:13:57,640 --> 00:13:59,480
 the unpleasantness

253
00:13:59,480 --> 00:14:01,920
 the unwieldiness

254
00:14:01,920 --> 00:14:04,920
 the not how I want it

255
00:14:04,920 --> 00:14:09,840
 wantedness

256
00:14:09,840 --> 00:14:11,120
 is it how you want it?

257
00:14:11,120 --> 00:14:12,840
 is it how you'd like it? No

258
00:14:12,840 --> 00:14:14,680
 that's it

259
00:14:14,680 --> 00:14:15,840
 that's reality

260
00:14:15,840 --> 00:14:17,680
 why? Because

261
00:14:17,680 --> 00:14:22,240
 that will allow you to let go

262
00:14:22,240 --> 00:14:24,520
 that will lead you to want to let go

263
00:14:24,520 --> 00:14:27,480
 not want to cling anymore

264
00:14:27,480 --> 00:14:34,480
 so you're doing fine

265
00:14:34,480 --> 00:14:38,480
 although I would recommend going back as often as possible

266
00:14:38,480 --> 00:14:39,560
 to the rising and falling

267
00:14:39,560 --> 00:14:41,680
 it keeps you grounded

268
00:14:41,680 --> 00:14:43,670
 don't jump from one thing to another unless you're

269
00:14:43,670 --> 00:14:44,680
 immediately pulled

270
00:14:44,680 --> 00:14:48,280
 if you're not pulled then go back to the rising and falling

271
00:14:48,280 --> 00:14:50,840
 and then acknowledge the next thing

272
00:14:50,840 --> 00:14:54,170
 but try to always come back to the rising and falling when

273
00:14:54,170 --> 00:14:54,200
 you can

274
00:14:54,200 --> 00:14:56,720
 after noting whatever you have noted

275
00:14:56,720 --> 00:14:58,600
 whatever you have

276
00:14:58,600 --> 00:15:05,120
 experienced

277
00:15:05,120 --> 00:15:07,480
 so the last part of the question was

278
00:15:07,480 --> 00:15:10,910
 you have talked about a middle way for responding to c

279
00:15:10,910 --> 00:15:11,400
ravings

280
00:15:11,400 --> 00:15:14,160
 and that's a little bit like the question of

281
00:15:14,160 --> 00:15:15,980
 you know if you have to do something should you try to do

282
00:15:15,980 --> 00:15:20,720
 with the wholesome version of it?

283
00:15:20,720 --> 00:15:25,280
 last night there was a similar question about if you have

284
00:15:25,280 --> 00:15:26,240
 to give into something

285
00:15:26,240 --> 00:15:28,560
 should you

286
00:15:28,560 --> 00:15:31,080
 give in to the most wholesome version of it

287
00:15:31,080 --> 00:15:33,160
 or something similar?

288
00:15:33,160 --> 00:15:36,320
 they were saying remember to be mindful

289
00:15:36,320 --> 00:15:37,200
 they know

290
00:15:37,200 --> 00:15:38,760
 everything is fine

291
00:15:38,760 --> 00:15:40,480
 that is the middle way

292
00:15:40,480 --> 00:15:46,360
 that is the middle way I talk about

293
00:15:46,360 --> 00:15:49,470
 I don't think, I think there may be, yeah, it may be taking

294
00:15:49,470 --> 00:15:53,200
 what I said last night

295
00:15:53,200 --> 00:15:55,840
 somehow making more of it

296
00:15:55,840 --> 00:15:58,040
 and the whole point is to be mindful

297
00:15:58,040 --> 00:16:00,760
 if you can't, if you find yourself

298
00:16:00,760 --> 00:16:03,600
 giving in, well then you give in

299
00:16:03,600 --> 00:16:06,960
 it's not the middle way

300
00:16:06,960 --> 00:16:08,960
 but it happens

301
00:16:08,960 --> 00:16:13,080
 you pick yourself up and you try again

302
00:16:13,080 --> 00:16:16,840
 we're learning, we're trying to learn

303
00:16:16,840 --> 00:16:23,840
 trying to study, trying to understand

304
00:16:23,840 --> 00:16:26,440
 dear Bhante, I have been meditating for a while and I feel

305
00:16:26,440 --> 00:16:27,360
 like it's making me

306
00:16:27,360 --> 00:16:30,600
 unenthusiastic about things I formerly like to do

307
00:16:30,600 --> 00:16:33,720
 such as working out, watching television etc

308
00:16:33,720 --> 00:16:36,240
 I want to be clear, I'm not depressed but I feel fine

309
00:16:36,240 --> 00:16:38,280
 sitting around while they're doing nothing

310
00:16:38,280 --> 00:16:45,280
 this is natural, thanks

311
00:16:45,280 --> 00:16:48,240
 rape is natural

312
00:16:48,240 --> 00:16:49,840
 murder is natural

313
00:16:49,840 --> 00:16:53,200
 theft is natural

314
00:16:53,200 --> 00:16:56,240
 excruciating suffering is natural

315
00:16:56,240 --> 00:16:58,560
 terrible evil is natural

316
00:16:58,560 --> 00:17:00,040
 craving is natural

317
00:17:00,040 --> 00:17:04,200
 addiction is natural

318
00:17:04,200 --> 00:17:11,200
 so what do you mean by this question, is it natural?

319
00:17:11,200 --> 00:17:15,360
 answering a question with a question

320
00:17:15,360 --> 00:17:18,640
 and some elaboration

321
00:17:18,640 --> 00:17:23,980
 sometimes you have to, when someone asks you a question you

322
00:17:23,980 --> 00:17:24,840
 have to

323
00:17:24,840 --> 00:17:27,160
 pick a part

324
00:17:27,160 --> 00:17:31,280
 you can't answer directly

325
00:17:31,280 --> 00:17:36,980
 and sometimes you have to ask a question, a counter

326
00:17:36,980 --> 00:17:38,280
 question

327
00:17:38,280 --> 00:17:45,280
 I think this is one of those cases

328
00:17:45,280 --> 00:17:51,800
 and to follow up on the

329
00:17:51,800 --> 00:17:54,710
 earlier question about cravings, I can't follow the craving

330
00:17:54,710 --> 00:17:58,640
 to the end, is that a problem?

331
00:17:58,640 --> 00:18:01,800
 the end of what?

332
00:18:01,800 --> 00:18:05,440
 the end of the craving

333
00:18:05,440 --> 00:18:09,320
 can't stick it out until the craving goes away

334
00:18:09,320 --> 00:18:13,240
 the craving lasts actually ten minutes

335
00:18:13,240 --> 00:18:14,640
 apparently yes

336
00:18:14,640 --> 00:18:16,840
 I don't think so

337
00:18:16,840 --> 00:18:20,600
 you say to yourself wanting

338
00:18:20,600 --> 00:18:23,330
 it should go away for a moment and then you come back to

339
00:18:23,330 --> 00:18:27,440
 the rising and falling, if it comes back then you go back

340
00:18:27,440 --> 00:18:27,440
 to it

341
00:18:27,440 --> 00:18:31,160
 but when it goes away come back to the rising and falling

342
00:18:31,160 --> 00:18:37,160
 nothing lasts for ten minutes

343
00:18:37,160 --> 00:18:39,880
 if it does, then just say it

344
00:18:39,880 --> 00:18:46,880
 stick it out for a while and then ignore it after a while

345
00:18:46,880 --> 00:18:46,880
 and then say it

346
00:18:46,880 --> 00:18:51,920
 Lante, how can I make progress if I can't meet with you to

347
00:18:51,920 --> 00:18:52,680
 do a course?

348
00:18:52,680 --> 00:18:59,680
 is there something more I can read?

349
00:18:59,680 --> 00:19:05,060
 well if you are meditating, which looks like you are, good,

350
00:19:05,060 --> 00:19:07,800
 green question mark

351
00:19:07,800 --> 00:19:16,200
 you could read the second book on how to meditate

352
00:19:16,200 --> 00:19:19,800
 it's not quite finished but mostly finished

353
00:19:19,800 --> 00:19:26,800
 most intense and purposes it's finished

354
00:19:26,800 --> 00:19:35,800
 that would be a good thing maybe to read

355
00:19:35,800 --> 00:19:43,110
 we're all filled up on the meetings, right? is there one

356
00:19:43,110 --> 00:19:44,240
 empty spot?

357
00:19:46,800 --> 00:19:49,960
 Tuesday at 23.30 UTC time

358
00:19:49,960 --> 00:19:55,880
 which looks like 6.30 PM Eastern Time

359
00:19:55,880 --> 00:20:02,880
 grab it before it's gone

360
00:20:02,880 --> 00:20:12,480
 just looking for the link for

361
00:20:12,480 --> 00:20:19,480
 how to meditate part two

362
00:20:19,480 --> 00:20:26,480
 and with that we're all filled up on questions

363
00:20:26,480 --> 00:20:33,480
 and with that we're all filled up on questions

364
00:20:33,480 --> 00:20:52,800
 tomorrow we have a med mob

365
00:20:52,800 --> 00:20:56,940
 we're going to try a med mob or I'm going to maybe I'll be

366
00:20:56,940 --> 00:20:57,080
 there alone

367
00:20:57,080 --> 00:21:02,880
 do meditation in public see how it goes

368
00:21:02,880 --> 00:21:06,920
 do you have any signs? no

369
00:21:06,920 --> 00:21:10,880
 put a little sandwich board out there to explain what you

370
00:21:10,880 --> 00:21:11,200
're doing

371
00:21:11,200 --> 00:21:15,320
 put a little hat

372
00:21:15,320 --> 00:21:19,440
 no signs

373
00:21:19,440 --> 00:21:26,440
 no that's the whole thing about a flash mob

374
00:21:26,440 --> 00:21:30,960
 you don't really even tell people what you're doing

375
00:21:30,960 --> 00:21:37,960
 they're just figured out

376
00:21:37,960 --> 00:21:44,960
 okay

377
00:21:44,960 --> 00:21:55,210
 so that's all for tonight thank you all for practicing with

378
00:21:55,210 --> 00:21:55,320
 us

379
00:21:55,320 --> 00:21:59,240
 for tuning in

380
00:21:59,240 --> 00:22:02,880
 thanks Robin for joining me thank you Bante

381
00:22:02,880 --> 00:22:09,880
 have a good day

