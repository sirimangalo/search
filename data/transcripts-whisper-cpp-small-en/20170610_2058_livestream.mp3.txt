 Okay, good evening everyone.
 Welcome to our evening Dhamma session.
 Last night we talked about having one good night.
 The night I thought we'd talk about one exceptionally good
 night,
 which is the night the Buddha became enlightened.
 Both for simply how inspiring it is to think of the Buddha,
 and to think of how we're following in his footsteps.
 And because of how instructive it is, what the Buddha's
 experience teaches us.
 So a little bit of background for those who don't know.
 When the Buddha was born in India, or what was at the time
 the Indus Valley.
 It's now modern Nepal actually.
 About 2500 years ago.
 So the world was quite different back then.
 And he wandered around on foot throughout the Indus Valley.
 He left home at 29 years of age after living a life of
 luxury,
 and then wandered around India until he finally decided to
 just spend his time
 pushing his body to its limit.
 And purposefully torturing himself in order to burn away
 defilements didn't work.
 And then after six years of that, he gave up torturing
 himself
 and he went back to eating solid food and taking better
 care of himself
 and practicing meditation.
 Living a balanced life, and this is why he came to talk
 about the middle way.
 Because the ascetics who were practicing with him abandoned
 him.
 They said, "Oh, you've given up the holy life of torturing
 yourself."
 So they gave him up.
 I don't think the Buddha was disturbed by that, because at
 this point
 he was pretty set on his own meditation.
 And I imagine them leaving helped in the end because it
 gave him focus
 and allowed him to focus on his own practice.
 And he went and sat under the Bodhi tree, the tree that was
 to become,
 it's a very big, it's even there today, or a descendant of
 the original,
 they say, is still there to this day in the forest in Bodhg
aya.
 And he sat under this tree and spent the night there.
 So this is his one good night.
 Didn't sleep.
 He said to himself, he wasn't going to get up from that
 seat
 until he became enlightened.
 And there are many stories about his time under the tree.
 Of course, the most flowery one that many people have heard
 about
 or read about or watched in various depictions
 is where Mara comes down with all of his armies
 and attacks the Buddha and confronts the Buddha
 and says to the Buddha, you don't deserve to sit under the
 tree of enlightenment
 when you're doing, thinking you can become enlightenment.
 And the Buddha reflected back on all of his perfections
 and all of the work he had done, time after lifetime,
 to become a Buddha.
 And he put his hand, and all right, so the story goes that
 Mara says, all of these people back me, all of his armies,
 many demons, the idea is it's just a representation of all
 evil.
 Mara is in this legend, it's just the armies of Mara
 are actually defilements in the mind.
 The daughters of Mara that come and tempt the Buddha,
 the daughters of Mara are just defilements.
 They're just attachment, addiction and so on.
 But Mara says, all of these people are my witness.
 You're not worthy of being here. Who is your witness?
 And the Buddha looked around and he had no witness
 and so he put his hand on the earth and he said,
 the earth is my witness.
 As the earth is my witness, I am worthy to become
 enlightened.
 And the earth shook and sent forth water that swept away
 the armies of Mara.
 I get the feeling that it was embellished over time to get
 to that state,
 that the original idea was that the Buddha was assailed by
 defilements
 as we all are when we practice.
 Meditation centers are often hotbeds of defilement
 because we're letting down our guards and we're living in
 close quarters.
 It's quite amazing that all six of us can live here
 without tearing each other's heads off, right?
 Imagine a hundred people living in a meditation center and
 long term,
 fights do break out from time to time.
 These are the armies of Mara.
 It's the armies of Mara that cause conflict.
 So with them gone, once the Buddha's mind was purified of
 the defilements,
 and understand that when your mind,
 just because your mind is purified from defilements, doesn
't mean you're enlightened.
 You can have a very...
 And this is why the texts are quite clear.
 If you talk about enlightenment as being freedom from def
ilements,
 you can't quite say that because it's possible for a medit
ator to think
 they have no defilements because they don't at that time.
 Once the Buddha's mind was pure and he was sitting in
 states of clarity and bliss and calm,
 then he started to take stock of...
 And he finally was in a position to understand the universe
, to understand the truth.
 And so he started by looking at the universe.
 Well, he started by looking at himself actually, looking at
 the past.
 He remembered all the way back to his birth, recollecting,
 reflecting on his life,
 the indulgence, the self-torture.
 And then he broke through, went through back through the
 six years of torture,
 back through the 29 years of self-indulgment,
 all the way back to when he was six years old, six months
 old.
 How many months old? I don't remember.
 Sitting and lying under a tree and he entered into the jh
ana when he was just a baby.
 Back to when he was five days old and the ascetic asita
 came and proclaimed that he would be a Buddha.
 Back to when he was born, the day he was born in Lungbini
 garden.
 And legend says he actually took seven steps and proclaimed
 himself to be a future Buddha.
 And he went back before that day into his mother's womb and
 back into his past lives.
 The point is, he went, "This is if you want to remember
 your past lives.
 You need to remember like that.
 Enter into high states of tranquility and then send your
 mind back and back and back."
 We don't teach that here.
 But now I've just taught it so someday if you ever want to
 try it when you're alone and you have time.
 I know a man who actually tried it once and said he was
 able to remember some things.
 And so the Buddha remembered all the way back. He
 remembered all of his past lives.
 Not all of them, of course, is infinite, but he remembered
 as far back as he wished.
 He remembered things and imagined the history of that were
 actually possible.
 Imagine what you could learn if you could remember not just
 this life, but lifetime after lifetime
 and really get an incredible sense of cause and effect.
 We don't have that luxury. We can read about the Buddha's
 past life stories.
 Luckily, 500 or so of them have been written down and ret
old,
 perhaps with a little embellishment, but interesting anyway
.
 What the Buddha was able to actually remember.
 There are modern day gurus who write books about these
 things
 and claim to be able to hypnotize people into remembering
 past lives.
 It's interesting to read. You might want to take it with a
 grain of salt.
 Certainly an interesting thing to explore, the idea of past
 lives.
 The real problem is, of course, it's in the past.
 And as we talked about last night, it ends up being not
 intrinsically beneficial.
 It's quite easy to get lost in the past, caught up by it
 and distracted by it.
 So he moved on. I mean, that was interesting and useful.
 But a part of his is coming to be a Buddha, understanding
 this about rebirth.
 He moved on and he started to consider other beings.
 And he entered into, or he started to see through,
 I mean, this is all through quite powerful states of mind.
 He was able to apparently see beings arising and passing
 away.
 So being born and dying.
 He was able to send his mind out and to watch,
 to monitor the minds of others and watch them die and watch
 them be reborn.
 By knowing their minds, he could watch them pass away from
 here and die there.
 So he was able to see the, on a broader sense, the nature
 of cause and effect.
 Simply remembering his own past lives was interesting.
 But watching all the many people and watching the
 tendencies,
 this is what gave him this understanding of karma, cause
 and effect.
 Again, it's very much all about cause and effect.
 And this is why the Buddha would all the time talk about
 cause and effect
 and why the Four Noble Truths are framed in terms of cause
 and effect.
 And then finally in the third watch of the night,
 so this was three different things that happened.
 The third thing that happened is he started to look closer
 at the general picture,
 or the bigger picture.
 And so this is where he came up with what we call dependent
 origination
 or paticca samuppada, where he saw that in general it's
 people's cravings,
 people's desires that lead them to be reborn again.
 People want something and then they die wanting that and
 they go on in that way
 or they're attached to something.
 Maybe they're afraid of something or averse to that thing,
 but that's a power, that's the tension, the fuel that leads
 to rebirth.
 And he saw that it was because they thought those things
 were going to bring them happiness and peace.
 But he also saw that none of these people ever found
 happiness or peace.
 And he realized that it was ignorance, that this was wrong,
 that this was where the problem was.
 The problem lay in thinking, that wanting things or
 clinging to things,
 either positive clinging, negative clinging, that was the
 problem.
 So that's where he came up with the Four Noble Truths,
 that craving is the cause of suffering,
 thirst is the cause of suffering.
 And he came up with paticca samuppada,
 this very important teaching that I often talk about, which
 is avijja-pachaya-sankhara.
 Sankhara are all of our ambitions, you might say, so karma
 really,
 when you want to do something, do something good, do
 something evil,
 but all of our ambitions are caused by ignorance.
 You realize that there's no ambition, there's no
 inclination,
 not inclination, but no desire that can possibly satisfy
 you,
 or lead to satisfaction.
 And you realize that that's all it took,
 that once you got rid of that ignorance,
 once you got rid of your ignorance about reality,
 thinking that there was something you could find that was
 stable,
 satisfying, controllable, me, mine, going to hold on to
 this,
 once you give that up, you stop suffering, you're free.
 And I guess technically how it worked is,
 I mean not guess, but how it works is, you see, this
 ignorance,
 how the Buddha came to understand this is because he got
 rid of his ignorance.
 He came to see, "Hey, these things that I'm clinging to,
 these things are not stable, not satisfying, not controll
able."
 Which is what you're all doing here, right?
 Every moment when you see the things that you cling to,
 this is good, this is bad, this is right, this is wrong,
 this is me, this is mine,
 all of our judgments, when you see how stressful they are,
 when you see that they're not really you,
 you're not the one in control of them,
 they're unwieldy, uncontrollable, unpredictable,
 you're the one who's going to go of them.
 And that knowledge that over-dispel is the ignorance,
 that's enough to free you from all suffering.
 The Buddha had a really good night.
 And this is our leader, right?
 Many of us call ourselves Buddhists, it doesn't really
 matter,
 but whether you just think of the Buddha as a great guy,
 or whether you'd really take the Buddha as your refuge,
 in the sense that you really feel that his teachings are
 something to dedicate your life to,
 it doesn't really matter, but nonetheless, this is the guy,
 this is our point of reference,
 and it's quite inspiring to think of, right, because like
 other religions have their person,
 Jesus, Mohammed, Hindus have Krishna, and we have the
 Buddha,
 and this is the story. But also, the Buddha's enlightenment
 is the enlightenment,
 in many ways, the enlightenment that we're striving for,
 not completely.
 We may never remember our past lives or be able to see
 other people being born and dying,
 but we sure can understand the cause of suffering and free
 ourselves from it,
 through understanding suffering.
 So there you go, a little bit of dharma for tonight.
 Thank you all for coming out.
 I have a few questions here.
 [silence]
 Can you be totally free from attachments if you still want
 to eat enough to live and have the intention to meditate?
 Want to eat enough to live? Probably not, because that's
 kind of an attachment.
 So an enlightened being doesn't have that want, which means
,
 I remember talking to someone who's gone to the end, right,
 because enlightenment is sort of a gradual thing.
 Once you've seen nibbana, you still have attachments.
 It's just they're quite weakened because there's no wrong
 view to support them.
 Wrong view is gone. So you don't think it's right that you
 want to eat and want to live,
 but you still do. You still haven't worked it all out.
 Because you don't think it's right, it'll eventually peter
 out and fade away.
 But once you become completely enlightened, then in many
 cases, yes,
 there is just a starving to death because you really aren't
 concerned with it.
 I mean, this was a thing in India already.
 Starving to death is kind of a religious thing in India,
 because before the Buddha they were thinking,
 "This is a way to free yourself from attachment."
 But because they still had the attachment to food, it was
 just a forcing, and that doesn't work.
 But it's interesting, horrific for most people, I think,
 how could you possibly do such a thing?
 Quite interesting from a philosophical point of view.
 I mean, if you're free from suffering, why would you worry
 about eating to live?
 These strong emotions are dominating during my formal
 meditation.
 Should I stay with them until they go away?
 As I'm doing now, the cost of doing much less walking
 during a session,
 or ignore them and get back to focusing.
 If they don't go away, you can ignore them.
 But it's often good to change postures if they're really
 strong.
 I mean, it's something you generally just have to...
 There's no answer. I don't have a solution for you.
 So don't expect me to be able to fix that for you.
 It's something you have to be patient with.
 It's great that you're seeing them and learning about them.
 Just take your time to learn more about them and understand
 them better.
 So, I mean, in the end, it's not going to matter so much
 what you do.
 Just try to be flexible, and over time they will recede.
 They'll become more manageable.
 Because a lot of those kind of things have to do with the
 time before we were mindful,
 before we were practicing meditation.
 Just don't feed them.
 Would you say mindfulness is the same?
 I've already given a talk on mindfulness.
 So I'm not going to answer that because I want you to...
 I gave my answer to what mindfulness is, so I'm not going
 to say mindfulness.
 Because you're the second person who's asked this recently.
 It makes me wonder whether I actually explained it well
 enough.
 Maybe I didn't.
 But I hope that you go and read that, and I hope it does
 explain it.
 If not, you could read my booklet because that should give
 you some idea.
 Hopefully.
 It feels like there's no excuse to not drop everything.
 No excuse to drop.
 No excuse to not drop everything and follow a monk life.
 However, this feels wrong somehow.
 Is this something you work towards as a result of craving,
 or is this sense of extremism an wholesome?
 Thumb up, check, remove circle.
 It's a bit vague what you're saying, but I'm assuming that
 you mean...
 I'm assuming what's going on here is this conflicting sense
 of what's right.
 Because of course it's probably, I would assume it's fairly
 new to you,
 this idea that becoming a monk is the right thing to do.
 And what's been ingrained into your mind is that the right
 thing to do
 is be a responsible member of society.
 So it's just a question of when you can let go of that
 wrong view,
 or wrong perception.
 Because there's no reason to support society.
 Society is just a bunch of people running around doing
 meaningless things.
 Well, maybe not entirely meaningless.
 But in the end, I mean, in the end it's all meaningless.
 There's no, I mean, meaningless in the sense that there's
 no reason to support it, right?
 The only reason one could have is in terms of helping each
 other,
 in terms of helping each other be free from suffering,
 which is a fairly big and complicated goal.
 So I mean, that's the sense that I think society is
 important, useful.
 We have Buddhist society, monastic society, and it's
 important.
 But to that end, I mean, what helps people the most,
 what helps you or anyone else the most?
 It's obviously becoming enlightened.
 I mean, there should be no conflict here.
 It's just that most likely you're conflicted as a non-Budd
hist become Buddhist.
 And I mean, it's interesting how even in Asia that's the
 case.
 People who grew up as "Buddhists" have the same conflict
 because they're also taught the wrong way.
 They're also taught that the way to do the right thing
 is to make lots of money and be successful
 and maybe support your parents as well.
 But become a monk? No, that's not responsible. That's
 irresponsible.
 Hopefully you get over that.
 Okay, that's the demo for tonight.
 Thank you all for coming out.
 Thank you.
 Thank you.
