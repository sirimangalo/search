1
00:00:00,000 --> 00:00:07,110
 Good evening and welcome back to our study of the Dhammap

2
00:00:07,110 --> 00:00:07,600
ada.

3
00:00:07,600 --> 00:00:14,600
 Today we continue on with verse 151 which reads as follows.

4
00:00:14,600 --> 00:00:27,160
 Chiranti vayrajara tah sujitha ato sari rampi charangupeti

5
00:00:27,160 --> 00:00:30,520
 satancha dhammona charangupeti santo

6
00:00:30,520 --> 00:00:48,950
 hoe sabhi pawedayanti which means indeed just as the char

7
00:00:48,950 --> 00:00:54,120
iot of a king, beautiful chariot

8
00:00:54,120 --> 00:01:06,400
 and well decorated. Ages, even the king's chariot gets old,

9
00:01:06,400 --> 00:01:12,840
 falls apart. Ato likewise

10
00:01:12,840 --> 00:01:25,770
 sari rampi charangupeti. This body too, even this body goes

11
00:01:25,770 --> 00:01:29,600
 to old age. Satancha dhammona

12
00:01:29,600 --> 00:01:37,520
 charangupeti but the good dhamma never gets old. Santo hoe

13
00:01:37,520 --> 00:01:45,960
 sabhi pawedayanti. The peaceful

14
00:01:45,960 --> 00:02:02,180
 indeed. That is called peaceful by the wise. That indeed is

15
00:02:02,180 --> 00:02:05,800
 called peaceful by the wise.

16
00:02:05,800 --> 00:02:30,520
 So

17
00:02:30,520 --> 00:02:41,000
 this verse was taught in relation to one of these famous

18
00:02:41,000 --> 00:02:43,160
 stories. It's a story I actually

19
00:02:43,160 --> 00:02:49,200
 told recently on a meditation retreat in New York. I can't

20
00:02:49,200 --> 00:02:52,120
 remember some in response to

21
00:02:52,120 --> 00:03:01,100
 a question. The story goes, this is the story of Queen Mal

22
00:03:01,100 --> 00:03:06,360
ika. Malika was a queen in Sawati,

23
00:03:06,360 --> 00:03:13,280
 I believe the queen of Pisenadi. Pisenadi was a supporter

24
00:03:13,280 --> 00:03:16,880
 of the Buddha, though he wasn't

25
00:03:16,880 --> 00:03:20,810
 enlightened even at the lower levels. He was still an

26
00:03:20,810 --> 00:03:22,800
 ordinary human being with all of

27
00:03:22,800 --> 00:03:32,430
 his faults and wrinkles. His queen was the same, but they

28
00:03:32,430 --> 00:03:37,400
 were supporters of the Buddha.

29
00:03:37,400 --> 00:03:44,780
 They did many good things towards the Buddha and towards

30
00:03:44,780 --> 00:03:49,240
 his religion. We owe quite a bit

31
00:03:49,240 --> 00:03:53,590
 to these two if the tradition is anything to go by because

32
00:03:53,590 --> 00:03:55,840
 they supported Buddhism in

33
00:03:55,840 --> 00:04:07,010
 those early stages. The story is, Queen Malika was in

34
00:04:07,010 --> 00:04:11,680
 general a really good person, but again

35
00:04:11,680 --> 00:04:16,200
 she had her failings. She was just an ordinary world thing.

36
00:04:16,200 --> 00:04:20,120
 One day she was in the bath house

37
00:04:20,120 --> 00:04:29,850
 and the king was up in the palace looking down. As she was

38
00:04:29,850 --> 00:04:32,920
 bathing, she finished bathing,

39
00:04:32,920 --> 00:04:41,580
 washing her torso and her face. She bent down to wash her

40
00:04:41,580 --> 00:04:46,840
 legs and her dog was sitting there.

41
00:04:46,840 --> 00:04:49,710
 His dog came up to her and we don't have the details of

42
00:04:49,710 --> 00:04:53,120
 exactly what happened, but it says

43
00:04:53,120 --> 00:05:01,040
 that it started to sport with her and she let it continue.

44
00:05:01,040 --> 00:05:25,080
 As sadamma santavang katum arabi began to do that which was

45
00:05:25,080 --> 00:05:30,960
 unwholesome, unwholesome intercourse.

46
00:05:30,960 --> 00:05:41,850
 I'm not quite sure what that means. Santavas is to do with

47
00:05:41,850 --> 00:05:48,600
 sexual intercourse. She let it

48
00:05:48,600 --> 00:05:56,810
 continue and the king saw her. When she got back to the

49
00:05:56,810 --> 00:06:00,320
 palace, the king was ready to

50
00:06:00,320 --> 00:06:17,620
 banish her. He said, nasa wasali. Wasali is I think a very

51
00:06:17,620 --> 00:06:23,080
 bad word. Nasa means as close

52
00:06:23,080 --> 00:06:26,830
 as you get to the swear word in Bali. Nasa means it's

53
00:06:26,830 --> 00:06:29,040
 imperative, it's like go to hell

54
00:06:29,040 --> 00:06:35,930
 kind of thing. Nasa tia means to perish, one perishes, to

55
00:06:35,930 --> 00:06:40,360
 be destroyed. Nasa is like a

56
00:06:40,360 --> 00:06:47,950
 command, get lost, may you perish basically. The queen

57
00:06:47,950 --> 00:06:49,960
 asked him, what's wrong, what did

58
00:06:49,960 --> 00:06:56,960
 I do? He said, I saw you in the bathroom with that dog, how

59
00:06:56,960 --> 00:07:00,560
 could you be so disgusting and

60
00:07:00,560 --> 00:07:09,040
 nasa? Go away. The queen says, what are you talking about?

61
00:07:09,040 --> 00:07:09,600
 She's thinking quick on her

62
00:07:09,600 --> 00:07:13,680
 feet. She says, I did no such thing. He says, I saw you. He

63
00:07:13,680 --> 00:07:15,720
 says, what do you mean? She says,

64
00:07:15,720 --> 00:07:19,280
 what do you mean Nasa? I was up on the balcony and I looked

65
00:07:19,280 --> 00:07:21,160
 down into the bath house and

66
00:07:21,160 --> 00:07:24,860
 I saw you sporting with this dog. The queen says, I did no

67
00:07:24,860 --> 00:07:26,840
 such thing. She said, your

68
00:07:26,840 --> 00:07:33,100
 majesty you must know that there's a strange nature of that

69
00:07:33,100 --> 00:07:36,000
 bath house that anyone who

70
00:07:36,000 --> 00:07:41,610
 goes in there appears to be devil. He said, what are you

71
00:07:41,610 --> 00:07:45,600
 talking about? You utter falsehood.

72
00:07:45,600 --> 00:07:50,120
 You're telling a lie. He says, if you don't believe me, you

73
00:07:50,120 --> 00:07:52,240
 go in and I'll watch you.

74
00:07:52,240 --> 00:07:57,460
 This is the thing about the sanity. The commentary says he

75
00:07:57,460 --> 00:08:00,480
 was such a simpleton as to believe

76
00:08:00,480 --> 00:08:07,760
 what she said. He went into the bath house and she goes,

77
00:08:07,760 --> 00:08:11,680
 your majesty, why are you sporting

78
00:08:11,680 --> 00:08:18,040
 with that goat? He says, I am not. He says, I can see you.

79
00:08:18,040 --> 00:08:22,000
 Look at you with that goat.

80
00:08:22,000 --> 00:08:26,470
 Somehow he believes that it was all just an optical vision.

81
00:08:26,470 --> 00:08:29,800
 The point of the story and

82
00:08:29,800 --> 00:08:35,090
 why it's actually quite interesting to us is that she felt

83
00:08:35,090 --> 00:08:37,640
 quite guilty about this.

84
00:08:37,640 --> 00:08:42,840
 She was such a good person but she felt guilty thinking to

85
00:08:42,840 --> 00:08:45,720
 herself, how can I go and face

86
00:08:45,720 --> 00:08:48,660
 the Buddha when he'll know right away that I was lying and

87
00:08:48,660 --> 00:08:50,040
 he'll know what I did with

88
00:08:50,040 --> 00:09:00,240
 the dog and he'll know how evil and corrupt I am at heart

89
00:09:00,240 --> 00:09:05,000
 and I lied to the king and if

90
00:09:05,000 --> 00:09:10,940
 I go see the Buddha, he'll know right away. So she felt

91
00:09:10,940 --> 00:09:14,600
 very guilty and she tried to avoid

92
00:09:14,600 --> 00:09:20,730
 the Buddha. From that point on, she was tormented with

93
00:09:20,730 --> 00:09:24,120
 guilt for having this one time given

94
00:09:24,120 --> 00:09:29,720
 into some indiscretion. As a result, when she died, she

95
00:09:29,720 --> 00:09:30,860
 wasn't thinking about all the

96
00:09:30,860 --> 00:09:32,980
 good deeds that she'd done. She wasn't thinking about all

97
00:09:32,980 --> 00:09:34,160
 the wonderful things and all the

98
00:09:34,160 --> 00:09:39,960
 support. Pisanity was responsible for this great gift.

99
00:09:39,960 --> 00:09:42,080
 There was the gift beyond compare

100
00:09:42,080 --> 00:09:49,740
 where they, I think it was where they were fighting over

101
00:09:49,740 --> 00:09:54,120
 the townspeople, the people of

102
00:09:54,120 --> 00:09:58,240
 the city were competing with the king to give the greatest

103
00:09:58,240 --> 00:10:01,320
 gift or something. I can't remember

104
00:10:01,320 --> 00:10:04,340
 how Pisanity was responsible and Malika was right there

105
00:10:04,340 --> 00:10:05,960
 with him doing all these good

106
00:10:05,960 --> 00:10:08,990
 deeds supporting the Buddha and always going to listen to

107
00:10:08,990 --> 00:10:10,760
 the Buddha's teaching and trying

108
00:10:10,760 --> 00:10:19,950
 their best to understand it. She forgot all that. When she

109
00:10:19,950 --> 00:10:21,680
 died as a result, she died

110
00:10:21,680 --> 00:10:29,600
 not long after she went to hell. She was reborn in hell. As

111
00:10:29,600 --> 00:10:34,280
 a result not of the evil deed itself,

112
00:10:34,280 --> 00:10:44,840
 but of the guilt and the torment and the negative mind

113
00:10:44,840 --> 00:10:52,440
 state that she had gotten obsessed in.

114
00:10:52,440 --> 00:10:59,640
 Pisanity was much aggrieved, overcome with grief. After

115
00:10:59,640 --> 00:11:02,800
 performing the funeral rites,

116
00:11:02,800 --> 00:11:05,930
 he thought to himself, "I'll go to the teacher, the Buddha,

117
00:11:05,930 --> 00:11:07,600
 and ask him where she's been reborn."

118
00:11:07,600 --> 00:11:12,010
 Thinking to himself, "Well, at least that will give me some

119
00:11:12,010 --> 00:11:14,000
 comfort. If I know where

120
00:11:14,000 --> 00:11:18,320
 she is, then I can think well of her and be happy knowing

121
00:11:18,320 --> 00:11:20,760
 that she's in a good place."

122
00:11:20,760 --> 00:11:25,220
 So he goes to the Buddha and the Buddha thought to himself,

123
00:11:25,220 --> 00:11:27,520
 "Hmm, if he asks me, he's not

124
00:11:27,520 --> 00:11:33,100
 the sort of person to be understanding about such a

125
00:11:33,100 --> 00:11:38,280
 situation. He's most likely to disbelieve

126
00:11:38,280 --> 00:11:43,400
 me and perhaps even lose faith in Buddhism. If she's done

127
00:11:43,400 --> 00:11:46,120
 such good things, why is she

128
00:11:46,120 --> 00:11:50,190
 then born in hell? Why is the Buddha saying such things?

129
00:11:50,190 --> 00:11:52,480
 How could it be possible? Does

130
00:11:52,480 --> 00:11:55,230
 it mean that giving all these gifts and supporting Buddhism

131
00:11:55,230 --> 00:11:56,960
 and listening to Buddha's teaching

132
00:11:56,960 --> 00:12:04,830
 is of no benefit?" So he made a determination, the Buddha

133
00:12:04,830 --> 00:12:07,800
 made a determination in his mind

134
00:12:07,800 --> 00:12:12,930
 such that the king would not ask where his queen had been

135
00:12:12,930 --> 00:12:15,080
 reborn. And sure enough, the

136
00:12:15,080 --> 00:12:18,880
 king forgot. It was the power of determination. If you make

137
00:12:18,880 --> 00:12:20,640
 a determination and your will

138
00:12:20,640 --> 00:12:28,710
 is strong enough, there's some magic to it. And so he came

139
00:12:28,710 --> 00:12:30,000
 and he sat down and he listened

140
00:12:30,000 --> 00:12:34,140
 to the Buddha and the Buddha gave him some teachings. He

141
00:12:34,140 --> 00:12:36,440
 listened attentively and when

142
00:12:36,440 --> 00:12:39,320
 he left, just as he, as he kind of the monastery, he

143
00:12:39,320 --> 00:12:41,800
 remembered, "Oh, I forgot. I was going

144
00:12:41,800 --> 00:12:45,180
 to ask him about Queen Malika." So he went back to the

145
00:12:45,180 --> 00:12:47,280
 palace. He thought, "Well, never

146
00:12:47,280 --> 00:12:50,080
 mind. I'll go back tomorrow and see him again." And again

147
00:12:50,080 --> 00:12:51,680
 he went back to the Buddha. Again

148
00:12:51,680 --> 00:12:58,790
 the Buddha makes him forget and he does this for seven days

149
00:12:58,790 --> 00:12:59,720
. And at the end of the seventh

150
00:12:59,720 --> 00:13:09,890
 day, Queen Malika leaves hell and is reborn in heaven. Two

151
00:13:09,890 --> 00:13:11,920
 sita in fact, one in the good

152
00:13:11,920 --> 00:13:19,040
 heaven, one in the really high heaven. And on the eighth

153
00:13:19,040 --> 00:13:20,120
 morning, the Buddha, before

154
00:13:20,120 --> 00:13:22,790
 the king can come, the Buddha goes to the palace and goes

155
00:13:22,790 --> 00:13:24,480
 for alms around standing outside

156
00:13:24,480 --> 00:13:30,120
 of the king's residence. The king hears that the Buddha is

157
00:13:30,120 --> 00:13:31,920
 there and he comes down and

158
00:13:31,920 --> 00:13:37,720
 he takes the Buddha's bowl and he feeds him with royal food

159
00:13:37,720 --> 00:13:40,600
 and sits him down and he asks

160
00:13:40,600 --> 00:13:47,360
 him on the eighth day, "Reverend sir, please tell me where,

161
00:13:47,360 --> 00:13:50,560
 where Queen Malika is reborn."

162
00:13:50,560 --> 00:13:56,200
 And he says, "Oh, in the world of two sita, in the two sita

163
00:13:56,200 --> 00:13:59,200
 heaven." And he says, "If,

164
00:13:59,200 --> 00:14:02,140
 if she had not been born in there, how could anyone be born

165
00:14:02,140 --> 00:14:03,640
 there? She was such a good

166
00:14:03,640 --> 00:14:10,990
 person. But of course she was born there." And so wherever

167
00:14:10,990 --> 00:14:14,560
 she sat, wherever she stood,

168
00:14:14,560 --> 00:14:18,430
 she was always thinking about good deeds. She cared not for

169
00:14:18,430 --> 00:14:20,080
 else but to make, to give

170
00:14:20,080 --> 00:14:26,770
 gifts and be a good person. And then he started to feel sad

171
00:14:26,770 --> 00:14:29,920
 and he said, "Ever since she

172
00:14:29,920 --> 00:14:33,700
 went to the old, the other world, ever since she left this

173
00:14:33,700 --> 00:14:35,720
 world, my own person has been

174
00:14:35,720 --> 00:14:41,400
 non-existent. I have been nothing, nothing without her."

175
00:14:41,400 --> 00:14:42,040
 And this is where the Buddha

176
00:14:42,040 --> 00:14:47,980
 taught this verse. He said, "Oh, great King, just like this

177
00:14:47,980 --> 00:14:50,480
." He asks and he looks at the

178
00:14:50,480 --> 00:14:53,100
 chariot and he says, "Whose chariot is that?" And I guess

179
00:14:53,100 --> 00:14:56,160
 there were three chariots or something.

180
00:14:56,160 --> 00:14:59,040
 He's in the palace so he sees his, he sees this old chariot

181
00:14:59,040 --> 00:15:00,800
 and he says, "Whose is that?

182
00:15:00,800 --> 00:15:04,700
 That's, oh, that's my grandfather's." And "Whose is that?

183
00:15:04,700 --> 00:15:06,440
 Oh, that's my father's."

184
00:15:06,440 --> 00:15:09,280
 And he says, "What about this new one here? Oh, that's mine

185
00:15:09,280 --> 00:15:16,440
." And he said, "Like, just like those chariots,

186
00:15:16,440 --> 00:15:22,200
 the old ones get old, stop using them, they break down in

187
00:15:22,200 --> 00:15:25,120
 the same way that body breaks

188
00:15:25,120 --> 00:15:30,790
 down and is subject to old age, sickness and death." And

189
00:15:30,790 --> 00:15:33,840
 then he taught this verse, basically

190
00:15:33,840 --> 00:15:43,080
 saying that we all die. What doesn't die is goodness. What

191
00:15:43,080 --> 00:15:46,960
 doesn't die is truth. What doesn't

192
00:15:46,960 --> 00:15:56,340
 die is the dhamma. So there's two reasons why this story is

193
00:15:56,340 --> 00:15:58,040
 useful to us. Of course,

194
00:15:58,040 --> 00:16:00,660
 the reason for the verse or the actual verse itself relates

195
00:16:00,660 --> 00:16:01,760
 to death. We've been talking

196
00:16:01,760 --> 00:16:05,480
 quite a bit about this. Remember this is the believer in

197
00:16:05,480 --> 00:16:08,600
 the Jaravagga, the old age chapter.

198
00:16:08,600 --> 00:16:16,390
 So there's a lot about how we get old, which is important.

199
00:16:16,390 --> 00:16:18,760
 So we don't make these plans

200
00:16:18,760 --> 00:16:23,400
 thinking that we're going to live forever and have short-

201
00:16:23,400 --> 00:16:25,800
sighted ambitions for power

202
00:16:25,800 --> 00:16:31,070
 and money and pleasure that can't last, that can't satisfy,

203
00:16:31,070 --> 00:16:33,600
 that only set us up for disappointment

204
00:16:33,600 --> 00:16:39,160
 and stress when we get old and sick and eventually leave it

205
00:16:39,160 --> 00:16:41,400
 all behind. But the other interesting

206
00:16:41,400 --> 00:16:45,530
 thing is in regards to the story. This is a useful story to

207
00:16:45,530 --> 00:16:47,480
 think of guilt. Guilt is

208
00:16:47,480 --> 00:16:52,110
 something we also have to let go of. What's most curious

209
00:16:52,110 --> 00:16:54,480
 about this story is not really

210
00:16:54,480 --> 00:16:58,350
 the verse at all. It's this idea of guilt and it's a common

211
00:16:58,350 --> 00:16:59,840
 problem for meditators,

212
00:16:59,840 --> 00:17:03,720
 especially in the West, I think. We're good at feeling

213
00:17:03,720 --> 00:17:05,880
 guilty and hating ourselves for

214
00:17:05,880 --> 00:17:09,460
 things that we've done or feeling inadequate. It's not

215
00:17:09,460 --> 00:17:11,560
 something you see such a problem

216
00:17:11,560 --> 00:17:18,670
 with in Asia. But regardless, it's a problem that all medit

217
00:17:18,670 --> 00:17:21,060
ators face. Because we, to some

218
00:17:21,060 --> 00:17:24,380
 extent, think that torturing yourself over bad things is

219
00:17:24,380 --> 00:17:26,200
 somehow useful. It's a bit of

220
00:17:26,200 --> 00:17:31,630
 a defense mechanism. Instead of actually trying to better

221
00:17:31,630 --> 00:17:34,800
 ourselves, we somehow think that

222
00:17:34,800 --> 00:17:41,870
 if we hate what we've done enough, we'll never want to do

223
00:17:41,870 --> 00:17:43,980
 it again. Somehow by hating

224
00:17:43,980 --> 00:17:49,430
 what you wanted to do, you can drive out the desire. That's

225
00:17:49,430 --> 00:17:52,360
 what Malika did. She thought

226
00:17:52,360 --> 00:17:55,320
 if she hated herself enough, if she felt guilty and bad

227
00:17:55,320 --> 00:17:57,160
 about what she'd done enough, the

228
00:17:57,160 --> 00:18:01,830
 anger would consume the lust would consume whatever it was

229
00:18:01,830 --> 00:18:03,800
 that drove her to do such

230
00:18:03,800 --> 00:18:11,460
 things. In fact, it's foolish. It's like trying to dig out

231
00:18:11,460 --> 00:18:13,760
 one thorn with another

232
00:18:13,760 --> 00:18:17,230
 thorn. You get a thorn in your side and then you stick

233
00:18:17,230 --> 00:18:19,720
 another, jab another thorn in there

234
00:18:19,720 --> 00:18:24,430
 trying to get it out. You don't get a thorn out with a

235
00:18:24,430 --> 00:18:27,920
 thorn. You just hurt yourself more.

236
00:18:27,920 --> 00:18:31,370
 You're liable to get the second one stuck in there. Greed

237
00:18:31,370 --> 00:18:33,040
 isn't solved by anger. And

238
00:18:33,040 --> 00:18:37,310
 that's really what guilt is, feeling bad, feeling upset

239
00:18:37,310 --> 00:18:40,880
 about what you've done. So it's important

240
00:18:40,880 --> 00:18:43,710
 to be mindful. She would have had no problem if she'd

241
00:18:43,710 --> 00:18:45,600
 learned to be mindful of her guilt

242
00:18:45,600 --> 00:18:49,320
 and mindful of what she'd done. It isn't something that

243
00:18:49,320 --> 00:18:51,000
 should have centered hell.

244
00:18:51,000 --> 00:18:54,400
 As you can see, it was only for seven days, but it wasn't

245
00:18:54,400 --> 00:18:56,360
 even the deed itself, not even

246
00:18:56,360 --> 00:19:02,060
 the lying, which people lie. Unless they become compulsive

247
00:19:02,060 --> 00:19:05,020
 and habitual liars, it's not likely

248
00:19:05,020 --> 00:19:09,400
 to have great consequences, but the guilt. There's a story

249
00:19:09,400 --> 00:19:11,200
 in one of the, I think in

250
00:19:11,200 --> 00:19:18,470
 the Jataka maybe, of a monk who, as he was going down the

251
00:19:18,470 --> 00:19:22,000
 river on a boat, he broke a

252
00:19:22,000 --> 00:19:24,950
 blade of grass on the shore, which is against the rules,

253
00:19:24,950 --> 00:19:26,680
 and he felt really guilty about

254
00:19:26,680 --> 00:19:29,900
 it. But he couldn't find anyone to confess it to, so he

255
00:19:29,900 --> 00:19:31,720
 died not having confessed his

256
00:19:31,720 --> 00:19:34,530
 offense. Confessing is something we do as monks. We tell

257
00:19:34,530 --> 00:19:38,000
 someone, "Hey, I broke a rule.

258
00:19:38,000 --> 00:19:39,820
 Here I'm letting you know, and I will try not to do it

259
00:19:39,820 --> 00:19:41,600
 again." If we don't do that,

260
00:19:41,600 --> 00:19:48,070
 we've got an offense against us that we have to eventually

261
00:19:48,070 --> 00:19:50,160
 confess. And he felt so guilty

262
00:19:50,160 --> 00:19:54,900
 about it, he went to hell just for that. So again, a

263
00:19:54,900 --> 00:19:59,680
 reminder that it's not the deeds.

264
00:19:59,680 --> 00:20:04,950
 Being something is never evil. It's the intention. So you

265
00:20:04,950 --> 00:20:08,040
 have the intention to commit the deed,

266
00:20:08,040 --> 00:20:10,710
 but then you have the intention also to be angry and upset

267
00:20:10,710 --> 00:20:12,040
 about it and hate yourself

268
00:20:12,040 --> 00:20:19,670
 because of it. The meditators, it's a good thing for us to

269
00:20:19,670 --> 00:20:22,720
 remember. All of these, the

270
00:20:22,720 --> 00:20:27,140
 mind is chief. It's the mind which leads us to heaven. It's

271
00:20:27,140 --> 00:20:29,040
 the mind which leads us to

272
00:20:29,040 --> 00:20:32,560
 hell. No question about it. It's actually quite a scary

273
00:20:32,560 --> 00:20:36,800
 thing. And again, an important

274
00:20:36,800 --> 00:20:39,460
 reason for us to think about old age, sickness, and death,

275
00:20:39,460 --> 00:20:40,920
 that we don't really know where

276
00:20:40,920 --> 00:20:47,120
 we're going. We may very well do lots of good deeds during

277
00:20:47,120 --> 00:20:50,120
 our life, and then when we die,

278
00:20:50,120 --> 00:20:55,310
 be so upset that we wind up in hell for at least a short

279
00:20:55,310 --> 00:20:57,620
 time. So our determination should

280
00:20:57,620 --> 00:21:03,890
 be not to be born here or born there. It should be to set

281
00:21:03,890 --> 00:21:07,360
 ourselves on the good. The greatest

282
00:21:07,360 --> 00:21:10,940
 blessing is not to get good things, but to set yourself on

283
00:21:10,940 --> 00:21:12,760
 the right path because good

284
00:21:12,760 --> 00:21:23,220
 comes from good. So there you go. That's the Dhamma Padav

285
00:21:23,220 --> 00:21:25,560
arsa for today. Thank you all for

286
00:21:25,560 --> 00:21:26,880
 tuning in. Have a good night.

