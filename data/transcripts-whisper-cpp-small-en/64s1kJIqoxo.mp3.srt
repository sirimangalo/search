1
00:00:00,000 --> 00:00:06,850
 In many suttas we read that we should not indulge in any

2
00:00:06,850 --> 00:00:11,400
 sensual ill-will, harming thought

3
00:00:11,400 --> 00:00:14,240
 and should remove it completely.

4
00:00:14,240 --> 00:00:16,440
 But is it not said?

5
00:00:16,440 --> 00:00:18,320
 It's not said how.

6
00:00:18,320 --> 00:00:23,650
 In our tradition we can say just note it and move on, I

7
00:00:23,650 --> 00:00:26,560
 guess is what that means.

8
00:00:26,560 --> 00:00:33,860
 Yeah, not indulging in the three types of micha-sankappa,

9
00:00:33,860 --> 00:00:36,760
 wrong thought, is actually

10
00:00:36,760 --> 00:00:40,880
 a little bit more than just note it.

11
00:00:40,880 --> 00:00:43,910
 Because there's an aspect of the teaching here that the

12
00:00:43,910 --> 00:00:45,640
 Buddha is telling us to remind

13
00:00:45,640 --> 00:00:49,480
 ourselves not to do the opposite, which is not to follow

14
00:00:49,480 --> 00:00:50,720
 the thoughts.

15
00:00:50,720 --> 00:00:53,280
 And in meditation you have to do that.

16
00:00:53,280 --> 00:00:56,640
 You can't note something until you get it into your head

17
00:00:56,640 --> 00:00:58,500
 that you should be noting.

18
00:00:58,500 --> 00:01:01,880
 So the teaching here is actually on two levels.

19
00:01:01,880 --> 00:01:05,700
 The preliminary aspect is where you remind yourself, "Hey,

20
00:01:05,700 --> 00:01:07,800
 I should stop daydreaming"

21
00:01:07,800 --> 00:01:12,080
 or "Hey, I should stop plotting revenge" and realize that

22
00:01:12,080 --> 00:01:15,200
 that's not the proper way

23
00:01:15,200 --> 00:01:18,560
 to behave and then get into noting.

24
00:01:18,560 --> 00:01:23,770
 So when the Buddha says "removing it completely" and not, I

25
00:01:23,770 --> 00:01:26,960
 think how he puts it, basically

26
00:01:26,960 --> 00:01:32,240
 not indulging in it, not harboring it, I don't know what

27
00:01:32,240 --> 00:01:36,360
 the word is, not indulging in it,

28
00:01:36,360 --> 00:01:45,040
 not allowing it to cultivate.

29
00:01:45,040 --> 00:01:46,720
 There's two aspects to that I think practically.

30
00:01:46,720 --> 00:01:53,790
 As I said, the act of giving it up and the act of then

31
00:01:53,790 --> 00:01:57,960
 neutralizing it by just saying

32
00:01:57,960 --> 00:02:01,750
 "thinking, thinking" or "liking, liking, wanting, wanting"

33
00:02:01,750 --> 00:02:03,840
 or "angry, angry, disliking"

34
00:02:03,840 --> 00:02:05,040
 and so on.

35
00:02:05,040 --> 00:02:09,760
 But thoughts are something that you have to deal with

36
00:02:09,760 --> 00:02:13,160
 intellectually first and reprimand

37
00:02:13,160 --> 00:02:17,810
 yourself and say "this is not" or not reprimand necessarily

38
00:02:17,810 --> 00:02:23,200
 but question yourself and investigate

39
00:02:23,200 --> 00:02:26,440
 and see for yourself that that's not helping you.

40
00:02:26,440 --> 00:02:29,000
 Ideally this can just work by noting and as you note you

41
00:02:29,000 --> 00:02:30,700
 start to realize that the thoughts

42
00:02:30,700 --> 00:02:34,410
 are useless and not beneficial but there is a sense of

43
00:02:34,410 --> 00:02:36,880
 intellectual activity where you

44
00:02:36,880 --> 00:02:40,870
 tell yourself "Look, you know where this is leading" and

45
00:02:40,870 --> 00:02:42,720
 catching yourself and saying

46
00:02:42,720 --> 00:02:43,960
 "Oh, I should be meditating."

47
00:02:43,960 --> 00:02:48,370
 That person had another post, a couple of follow-up

48
00:02:48,370 --> 00:02:52,400
 questions, as I said, or some contemplative

49
00:02:52,400 --> 00:02:58,120
 practice maybe, the least probable would be to just repress

50
00:02:58,120 --> 00:03:02,360
 it inside us, question mark.

51
00:03:02,360 --> 00:03:05,480
 Yeah, repressing is an interesting idea.

52
00:03:05,480 --> 00:03:07,520
 I think I've talked about this before.

53
00:03:07,520 --> 00:03:11,020
 It's not really clear that you can actually repress

54
00:03:11,020 --> 00:03:12,080
 something.

55
00:03:12,080 --> 00:03:14,310
 What you're doing when you say repressing is actually

56
00:03:14,310 --> 00:03:15,440
 reacting negatively to it and

57
00:03:15,440 --> 00:03:18,820
 building up a habit of negative reaction so it's not really

58
00:03:18,820 --> 00:03:19,400
 good.

59
00:03:19,400 --> 00:03:22,120
 But there are cases where the Buddha says you should do

60
00:03:22,120 --> 00:03:23,680
 just that when there's no other

61
00:03:23,680 --> 00:03:29,600
 choice and it's just bugging you and where you just can't

62
00:03:29,600 --> 00:03:32,320
 get it out of your mind.

63
00:03:32,320 --> 00:03:36,000
 You have to then be strong with yourself, be firm with

64
00:03:36,000 --> 00:03:37,800
 yourself and say "no."

65
00:03:37,800 --> 00:03:41,220
 That comes up in different ways in meditation where you

66
00:03:41,220 --> 00:03:43,200
 have a persistent state of mind

67
00:03:43,200 --> 00:03:48,000
 and instead of noting it, you have to reprimand yourself

68
00:03:48,000 --> 00:03:50,680
 and say "no, this is wrong."

69
00:03:50,680 --> 00:03:55,150
 We actually encourage meditators sometimes to just say to

70
00:03:55,150 --> 00:03:56,960
 themselves "Stop."

71
00:03:56,960 --> 00:04:01,770
 Their mind is just making mental notes stop and then it'll

72
00:04:01,770 --> 00:04:02,520
 stop.

73
00:04:02,520 --> 00:04:04,480
 It can stop.

74
00:04:04,480 --> 00:04:08,510
 Not totally under control but this is a stopgap measure

75
00:04:08,510 --> 00:04:11,040
 that you use while you're finding

76
00:04:11,040 --> 00:04:14,930
 your feet and learning how to note but it can be useful at

77
00:04:14,930 --> 00:04:15,680
 times.

78
00:04:15,680 --> 00:04:17,360
 The best is just to note.

79
00:04:17,360 --> 00:04:20,580
 If you can remember to note, just note the thoughts, note

80
00:04:20,580 --> 00:04:21,600
 the emotions.

81
00:04:21,600 --> 00:04:24,790
 Pick it apart, see it for what it is and realize that, you

82
00:04:24,790 --> 00:04:26,960
 know, teach yourself that these

83
00:04:26,960 --> 00:04:31,730
 thoughts are causing you headaches and suffering and stress

84
00:04:31,730 --> 00:04:34,320
 and getting you lost and leading

85
00:04:34,320 --> 00:04:35,320
 you nowhere.

86
00:04:35,320 --> 00:04:39,280
 Most importantly, taking you away from reality because it's

87
00:04:39,280 --> 00:04:41,120
 easy to get lost in illusion

88
00:04:41,120 --> 00:04:45,500
 and fantasy even for a long time because it's much

89
00:04:45,500 --> 00:04:47,880
 preferable to reality.

90
00:04:47,880 --> 00:04:51,840
 In every aspect, illusion is preferable to reality in every

91
00:04:51,840 --> 00:04:53,800
 aspect except that it's not

92
00:04:53,800 --> 00:04:54,800
 real.

93
00:04:54,800 --> 00:04:57,720
 So it messes with you, it messes with reality.

94
00:04:57,720 --> 00:05:01,680
 This is why reality is changing in some bad ways now.

95
00:05:01,680 --> 00:05:05,200
 If you look at our reality, all of us sitting in front of

96
00:05:05,200 --> 00:05:07,400
 this computer, it's not really

97
00:05:07,400 --> 00:05:10,250
 a pleasant, it's not really as pleasant, I mean from my

98
00:05:10,250 --> 00:05:11,860
 point of view, as pleasant as

99
00:05:11,860 --> 00:05:17,110
 maybe sitting on a rock in the forest but we've changed a

100
00:05:17,110 --> 00:05:19,280
 lot and a lot of it has to

101
00:05:19,280 --> 00:05:24,000
 do with our addiction to entertainment.

102
00:05:24,000 --> 00:05:27,270
 I used to give talks in Second Life and we were just

103
00:05:27,270 --> 00:05:29,360
 talking about this last night.

104
00:05:29,360 --> 00:05:37,400
 I was showing my old videos from Second Life to Laurie

105
00:05:37,400 --> 00:05:40,600
 before he left and Second Life is

106
00:05:40,600 --> 00:05:43,420
 really a good example of this where we go to this place

107
00:05:43,420 --> 00:05:44,880
 that's so beautiful and in the

108
00:05:44,880 --> 00:05:49,070
 forest and people make houses there and have beautiful

109
00:05:49,070 --> 00:05:51,600
 bodies and beautiful houses and

110
00:05:51,600 --> 00:05:54,580
 beautiful, you know, everything wonderful.

111
00:05:54,580 --> 00:05:59,890
 And the truth is they're just sitting in front of a black

112
00:05:59,890 --> 00:06:02,560
 box at a desk somewhere and that's

113
00:06:02,560 --> 00:06:06,360
 kind of the depressing thing is that the further we get

114
00:06:06,360 --> 00:06:08,880
 engaged in illusion, and this has a

115
00:06:08,880 --> 00:06:12,670
 lot to do with these three kinds of thoughts, the worse off

116
00:06:12,670 --> 00:06:14,320
 we become in reality.

117
00:06:14,320 --> 00:06:18,560
 Our reality becomes less and less agreeable, less and less,

118
00:06:18,560 --> 00:06:20,520
 on two levels, mentally we

119
00:06:20,520 --> 00:06:24,470
 become less appreciative of our reality and physically our

120
00:06:24,470 --> 00:06:26,520
 reality changes, it becomes

121
00:06:26,520 --> 00:06:32,640
 more coarse and chaotic because we're not cultivating peace

122
00:06:32,640 --> 00:06:34,240
 and harmony.

123
00:06:34,240 --> 00:06:40,100
 I mean much better than that we should stay in reality and

124
00:06:40,100 --> 00:06:43,260
 cultivate our, bring our dreams

125
00:06:43,260 --> 00:06:48,320
 into reality, make our life a dream, not a dream but bring

126
00:06:48,320 --> 00:06:50,880
 our dreams to fruition rather

127
00:06:50,880 --> 00:06:53,880
 than dreaming.

128
00:06:53,880 --> 00:06:58,190
 On the subject of meditation, one listener writes, "Hi, B

129
00:06:58,190 --> 00:07:00,360
onti, I'm getting irregular

130
00:07:00,360 --> 00:07:01,360
 heart beat."

131
00:07:01,360 --> 00:07:03,360
 Oh, we better stop then, wait, that's a different question,

132
00:07:03,360 --> 00:07:04,040
 let me stop this.

