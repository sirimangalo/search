 Hi, welcome back to Ask a Monk. Today's question comes from
 Fiskdams Consulting.
 What is really the mind and how did the Buddha use the last
 four parts of the five aggregates
 and nama in nama-rupa to explain the mind? Did the Buddha
 use any other ways of explaining
 the mind? Understanding what the mind is is hard. Please
 help.
 What is really the mind? The mind, the definition of mind,
 and here this is the meaning of nama
 or vinyana or dita, which in the end are all just different
 ways of saying basically the
 same thing. It's that which knows. In reality we understand
 there are two parts. One part
 one part of reality isn't aware of anything. It's the
 unconscious part of reality. That
 part we call rupa and in English we translate that into
 form or material or the physical.
 The other part knows something. It's aware of things,
 generally aware of the physical
 but also aware of other mental states. This knowing we call
 nama. This is all that really
 exists in the mind and the other aspects of it are the
 quality of knowing because not
 every knowing is the same. Sometimes this knowing, this
 awareness, this observation of things
 is based on the judgment, anger, greed. Sometimes it's
 impartial. Sometimes it's happy. Sometimes
 it's unhappy. Not every knowledge is the same. These are
 the various qualities of the mind.
 But essentially the meaning of mind is that which knows. In
 this sense it's really not
 very difficult to understand at all. It's very much a part
 of meditation practice and
 if you practice sincere meditation it's quite easy to see
 that which is the mind. For instance
 when you're watching the stomach, if you watch it rising
 and falling, there is the reality
 of the stomach rising and falling but there is also the
 knowing of it and these are two
 distinct things. We know this because you're not always
 aware of the rising and falling.
 If the mind doesn't go out to the object then there's no
 awareness. Sometimes the mind is
 elsewhere. The rising and falling is still occurring but
 the mind is not aware of it.
 So until the mind and the body come in contact there's no
 awareness. This is what is meant
 by the mind. The body is one thing, the mind is another and
 when they come together that's
 called experience. As far as the five aggregates, this is
 one very important way of understanding
 what is the body and mind and essentially what is reality.
 The way the Buddha explained
 the five aggregates, they are something that arises at what
 are called the six senses.
 So we always talk about the five senses but actually there
 are six, the five that we normally
 understand and the sixth one is the mind, the purely mental
, the thinking, our mental
 landscape. So when you see something there are five things
 that arise. There's the physical
 which is the eye and the light that touches it, that's a
 physical reality. Then there
 is number two, the feeling which is in the beginning
 neutral but can evolve into a pleasant
 or an unpleasant feeling based on what we see whether it's
 beautiful or ugly or how
 we react to it in the mind. Number three is our recognition
 of it, our perception of it
 as yellow, white, blue, as a car, a bird, a tree. Remember
 recognizing it as this person
 or that person. Number four is what we think of it, our
 judgments liking it, disliking,
 getting angry or becoming attached to it and so on. Number
 five is the basic awareness
 which governs all of the other, which governs the whole of
 the experience. These five things
 are called the five aggregates. That's how the Buddha
 explained it and that's what happens
 when you meditate. When you see something and you say to
 yourself seeing, seeing, seeing,
 you're aware of all of this and sometimes one is clearer,
 sometimes the feeling is clearer,
 sometimes the memory, the recognition that this is
 something I've seen before. Sometimes
 it's the, what you think of it, the judgment of it.
 Sometimes it's just the clear awareness,
 the fact that you're aware of things, you know that you're
 seeing this knowledge of
 it. When you hear it's the same, when you smell, when you
 taste, when you feel and when
 you think. When you think there's the physical but it's not
 really in play there, the physical
 is required as a base for the human experience but then
 there are the, basically the four
 experiences based on thought, based on a thought that has
 arisen which can arise because of
 the physical or because of the mental. So that's, did the
 Buddha use other ways of explaining
 the mind? Yeah, this is the most standard experience of
 explanation of how to understand
 the mind but there's, the Buddha used a lot of ways to
 explain both body and mind and
 the most comprehensive, if you're really interested in that
 sort of thing is in what is called
 the Abhidhamma which is the most detailed exposition on the
 body and the mind which
 includes 121 different types of mind state and it's
 basically a permutation of the various
 types of mind states that can come together. For instance,
 there are eight types of greed,
 of consciousness based on greed or craving and you get the
 number eight because there
 are three factors. There are greed states that are
 associated with pleasure and those
 that are associated with equanimity. There are greed states
 that are accompanied by wrong
 view, by the understanding that it's good to be greedy and
 there are greed states that
 are not associated with view so the understanding that it's
 wrong but still wanting something,
 the understanding that it's not useful for you but still
 wanting something. And the third
 one whether it is prompted or unprompted, whether it comes
 from itself or whether there's an
 external stimulus like someone urging you to steal or so on
 or some kind of other factor
 that it doesn't spontaneously arise. So with these three
 factors you have two times two
 times two and you get eight different kinds of greed minds.
 There are two kinds of anger
 minds, two kinds of delusion minds that are simply based on
 delusion and those are the
 twelve unwholesome minds and then it goes on and on and on.
 There are eight great wholesoms
 or eight sensual sphere wholesoms and eight, I can't even
 remember, it's been a long time
 but I've got the book and you can go through it and there's
 121 of them and some of them
 are super mundane, some of them are based on meditative
 states. And then there are 52,
 I'm probably getting it wrong, there's a whole bunch of
 mental states that are involved here
 like the ones that are involved in all of these minds, the
 mental qualities that are
 only in some of the minds, only in the wholesome minds,
 only in the unwholesome minds and so
 on and so on. So it's a very complex breakdown of the
 mental landscape. All of that's really
 theoretical but it does give a good understanding of the
 various details of the things that
 you're going to run into in your meditation practice and in
 your life, understanding what
 a mind state is and it also helps you to break down some of
 the, as I said, entities where
 we think this is a problem, something's going on, to be
 able to break it down and see, oh
 it's actually just states of greed, states of anger, things
 that are arising in my mind,
 it's my extrapolation of something very simple. But
 certainly too much for a short video,
 I'm not going into detail about that but just so you
 understand there are many different
 aspects or ways that the Buddha used of explaining the mind
. And as you say, in the end understanding
 the mind is hard. There's no getting around that. You can
 theorize, you can read texts
 and so on but until you sit down and practice meditation,
 you'll never really understand
 the mind. And once you sit down and practice meditation, it
 may still not be clear how
 that relates to the five aggregates or how the Buddha
 talked about mind or consciousness
 or so on. But it's not really important because you simply
 need someone like me to explain
 to you what is happening here and here and here as I've
 just done hopefully helps with
 that. But the point is that you're seeing reality. It's not
 important that you understand
 the Buddha's concept of mind. It's important you understand
 the truth of mind, the truth
 of reality. It's not so important that you can say that's
 mind, that's body, that's this,
 that's this. It's much more important that you see it for
 what it is. It is what it is
 at that moment. You see it clearly and you're not judging
 it. You don't have to be clearly
 aware of what part of the Buddha's teaching that fits into
 in terms of which one of the
 121 minds it is, for example. When you can see that certain
 mind states are unwholesome,
 then you give those up. When you see that certain mind
 states are wholesome, then you
 follow those. And this comes naturally through the practice
 and that's most important. But
 certainly it's not easy and as far as the fact that the
 mind is difficult to understand,
 there's a quote of the Buddha that's often given and in the
 Buddha's language it goes,
 "Dulangamang ekachalang asari rang guhasayang yeh chitang s
anyami santi mukhanti mala bandhana,"
 which means basically, "Dulangamang" it travels far, "ekach
alang" it travels alone, "asari
 rang" it has no form, "guhasayang" it dwells in a cave. And
 the point of this first part
 is to give you the idea of some terrible beast or demon or
 ghost, something that is very
 mysterious because it travels far and wide alone without
 form in living in a cave. And
 then the next part is "yeh chitang sanyami santi mukhanti m
ala bandhana," which means
 a person who can calm the mind, this person becomes free
 from the bonds of "mara," the
 bonds of evil. So the mind is considered something that
 goes far, travels far and can't be controlled.
 In a moment you can be thinking of something a million
 miles or a thousand miles away,
 and it travels alone means that you are only aware of one
 thing at a time, so the mind
 is always on one thing or another, it's never in two places
 at once and it's certainly not
 able to comprehend everything. If you don't catch it and
 fix it and focus on one thing,
 it's going to jump back and forth between objects. "Asari
 rang" means it's very difficult
 to understand, and this is why in Buddhist meditation we
 always have you focus on the
 body. And many people don't understand this, they'll say, "
What's the point? I really
 want to understand the mind, I want to understand how my
 mind works," and that's where the problem
 is. True, but it doesn't have a physical form, "Asari rang
," the mind is not something
 you can grab at and say, "There it is, it's something that
 relies on the physical." And
 so we have you focus on the physical, and this is another
 thing that the texts talk
 about, saying that we have you always focus on the physical
 first, and the point is that
 when you focus on the physical, the mind becomes clear by
 itself. Once the body becomes clear,
 then the mind becomes clear automatically, and you don't
 have to do anything special.
 You don't have to be chasing after the mind, just like a
 hunter when they want to catch
 a deer or a wild animal. They don't go running through the
 forest after the wild animal,
 they sit by the place where they know the deer is going to
 come, either the water hole
 or the fruit trees or someplace where they know that the
 deer are bound to come at some
 point and they sit there and they wait, and sure enough the
 mind will come to them and
 they'll be able to catch the, do what they want. The same
 with in our meditation, we
 don't have to go chasing after the mind. When we focus on
 the rising and the falling, well
 that's the mind going out to the rising and to the stomach,
 and we're going to see how
 the mind works. We're going to get angry sometimes,
 frustrated at it sometimes, we're going to
 be happy sometimes, unhappy sometimes, we're going to see
 how our mind reacts to things,
 we're going to see the quality and the characteristics of
 our mind, our habits and so on. So it's
 very difficult to catch the mind without this sort of a
 technique. This is why we use the
 body first, because it's a saree lang, it has no physical
 form. And guha sa yang means
 it is the other part of this, it dwells in a cave and that
 cave is the body, that this
 mind is somehow trapped in the cage of the body. It always
 has to come back and be dependent
 on this. So when you focus on any part of the physical
 reality as it arises, you're
 going to be able to see the mind, you're going to be able
 to catch it. It's like finding
 the cage of the YMVs, where its limits are, you just watch
 those limits, which is in this
 case the body. Okay, so good luck training the mind,
 calming the mind and keeping the
 mind clear and out of difficulty. This is the way, as the
 Buddha said, to overcome the
 bonds of Mara, which is like the Buddhist version of Satan,
 but it just means the evil
 that exists in our minds, and it's the way to overcome
 these things. Okay, so thanks
 for the question, hope that helps.
