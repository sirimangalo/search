1
00:00:00,000 --> 00:00:07,040
 Hello and welcome back to our study of the Dhammapada.

2
00:00:07,040 --> 00:00:12,360
 Today we begin the Balawaga, the chapter on fools.

3
00:00:12,360 --> 00:00:18,210
 So all the verses in here in this chapter will have

4
00:00:18,210 --> 00:00:23,440
 something to do with fools or foolishness.

5
00:00:23,440 --> 00:00:32,080
 Verse 61, today which reads as follows.

6
00:00:32,080 --> 00:00:39,040
 Charanti nadi gat jaya saiyang sadhi samatano.

7
00:00:39,040 --> 00:00:46,670
 Ekang charang da ekacharyang dal hankayira nati bali sahay

8
00:00:46,670 --> 00:00:47,440
ata.

9
00:00:47,440 --> 00:00:53,440
 Which means, Charanti nadi gat jaya saiyang sadhi samatano.

10
00:00:53,440 --> 00:01:00,510
 If one doesn't find, if when wandering or faring through s

11
00:01:00,510 --> 00:01:05,440
amsara one doesn't find someone who is once equal or better,

12
00:01:05,440 --> 00:01:10,440
 better or at least equal to oneself.

13
00:01:10,440 --> 00:01:13,440
 Ekacharyang da harangayira.

14
00:01:13,440 --> 00:01:28,440
 One should then fare on alone with steadfast resolution.

15
00:01:28,440 --> 00:01:37,440
 Nati bali sahayata for there is no association with fools.

16
00:01:37,440 --> 00:01:42,350
 So the story behind this verse goes that when the Buddha

17
00:01:42,350 --> 00:01:48,440
 was dwelling in Sawati, Maha Kasapa was staying in his cave

18
00:01:48,440 --> 00:01:50,440
 above Rajagaha,

19
00:01:50,440 --> 00:01:53,440
 which you can still see today if you go there.

20
00:01:53,440 --> 00:01:58,600
 Apparently they have a place that they pinpointed as his

21
00:01:58,600 --> 00:01:59,440
 cave.

22
00:01:59,440 --> 00:02:03,440
 Anyway, you can still go into Rajagaha.

23
00:02:03,440 --> 00:02:08,090
 Up on the mountain above Rajagaha and he would go every day

24
00:02:08,090 --> 00:02:11,440
 down the mountain into the city for alms.

25
00:02:11,440 --> 00:02:17,930
 And at the time he had two students, two novices, staying

26
00:02:17,930 --> 00:02:19,440
 with him.

27
00:02:19,440 --> 00:02:23,850
 And they would attend upon him and one of them was a good

28
00:02:23,850 --> 00:02:32,440
 student and one of them was a rather recalcitrant student.

29
00:02:32,440 --> 00:02:39,620
 He was lazy in performing his duties and practicing the d

30
00:02:39,620 --> 00:02:40,440
hamma.

31
00:02:40,440 --> 00:02:44,580
 And so how it would go is the one student who was quite

32
00:02:44,580 --> 00:02:54,440
 diligent and faithful and obedient and so on.

33
00:02:54,440 --> 00:02:59,610
 He would prepare the elder's water in the morning and his

34
00:02:59,610 --> 00:03:02,440
 toothbrush sticks.

35
00:03:02,440 --> 00:03:05,360
 He would wait upon his elder and perform all these duties

36
00:03:05,360 --> 00:03:09,970
 and he would heat up water for the elder's bath and leave

37
00:03:09,970 --> 00:03:12,440
 the water out in the morning.

38
00:03:12,440 --> 00:03:16,880
 And the other disciple would watch or would sleep in and

39
00:03:16,880 --> 00:03:21,200
 then would come out when everything was done and look and

40
00:03:21,200 --> 00:03:23,440
 see that everything was done and go to tell the elder.

41
00:03:23,440 --> 00:03:28,630
 Well, sir, the tooth sticks have been put out and water has

42
00:03:28,630 --> 00:03:32,440
 been put out for washing and for bathing.

43
00:03:32,440 --> 00:03:38,440
 Please come and it's time to do your morning routine.

44
00:03:38,440 --> 00:03:42,720
 And so the elder, as a result, got to thinking that this

45
00:03:42,720 --> 00:03:46,710
 student who kept coming to tell him and to invite him to

46
00:03:46,710 --> 00:03:51,390
 use the things that had been set out for him had performed

47
00:03:51,390 --> 00:03:52,440
 duties himself.

48
00:03:52,440 --> 00:03:55,330
 And the other student who had actually done all the work

49
00:03:55,330 --> 00:03:58,790
 sat back and watched and after some time realized what this

50
00:03:58,790 --> 00:04:02,650
 other student was doing, that he was taking credit for the

51
00:04:02,650 --> 00:04:04,440
 work that he hadn't done.

52
00:04:04,440 --> 00:04:07,440
 He decided, he said, "Well, I know what to do then."

53
00:04:07,440 --> 00:04:10,910
 And so he, in the morning, when it was time for the elder's

54
00:04:10,910 --> 00:04:17,640
 bath, he went to the room where they boiled the water and

55
00:04:17,640 --> 00:04:21,160
 he put just a little bit of water in the bottom of the big

56
00:04:21,160 --> 00:04:21,440
 pot,

57
00:04:21,440 --> 00:04:24,440
 the big cauldron for boiling bath water.

58
00:04:24,440 --> 00:04:26,440
 And he did that.

59
00:04:26,440 --> 00:04:28,360
 And so just so you could see the steam coming up, but there

60
00:04:28,360 --> 00:04:29,440
 was very little water in it.

61
00:04:29,440 --> 00:04:32,330
 And then the other novice, when he came over and saw the

62
00:04:32,330 --> 00:04:34,970
 steam coming out, he said, "Oh, I must have put out the

63
00:04:34,970 --> 00:04:35,440
 water."

64
00:04:35,440 --> 00:04:38,640
 And he went to tell the elder, "Menderable sir, your bath

65
00:04:38,640 --> 00:04:41,440
 water is ready. Come and take your bath."

66
00:04:41,440 --> 00:04:43,690
 And the elder went into the bathroom and said, "Okay, bring

67
00:04:43,690 --> 00:04:44,440
 out the water."

68
00:04:44,440 --> 00:04:50,400
 And the novice went into the boiling room and looked in the

69
00:04:50,400 --> 00:04:53,090
 cauldron and realized that there was just a very little bit

70
00:04:53,090 --> 00:04:55,440
 of water and got very angry and shouted.

71
00:04:55,440 --> 00:04:57,440
 And he said, "That's scoundrel."

72
00:04:57,440 --> 00:05:00,690
 He only put a very little bit of water in and then so he

73
00:05:00,690 --> 00:05:04,430
 rushed off to the river to get more water, wherever the

74
00:05:04,430 --> 00:05:05,440
 river was.

75
00:05:05,440 --> 00:05:08,780
 He rushed off to get water from wherever they kept the

76
00:05:08,780 --> 00:05:09,440
 water.

77
00:05:09,440 --> 00:05:11,620
 Maybe he must have had the gun down and the rajjagahadiyi's

78
00:05:11,620 --> 00:05:14,440
 water.

79
00:05:14,440 --> 00:05:19,200
 And the elder watched him go and when he came back, the

80
00:05:19,200 --> 00:05:23,440
 elder said, "What did you mean by that?

81
00:05:23,440 --> 00:05:26,630
 Are you saying that you didn't do any of this and you've

82
00:05:26,630 --> 00:05:29,440
 been taking credit for this all the time?"

83
00:05:29,440 --> 00:05:31,450
 And he said, "That's not how..." and the elder scolded him

84
00:05:31,450 --> 00:05:34,440
 and said, "That's not how a monk should behave.

85
00:05:34,440 --> 00:05:37,800
 One should not take credit for something that one hasn't

86
00:05:37,800 --> 00:05:38,440
 done."

87
00:05:38,440 --> 00:05:45,720
 And the novice got very angry and went off in half and

88
00:05:45,720 --> 00:05:49,440
 began to develop a grudge against the elder.

89
00:05:49,440 --> 00:05:54,440
 The next day or on subsequent days, they went off.

90
00:05:54,440 --> 00:05:57,630
 When they went off for alms, I guess the next day, when

91
00:05:57,630 --> 00:06:02,860
 they went for alms, he as a result, he decided not to go on

92
00:06:02,860 --> 00:06:05,440
 alms with the elder.

93
00:06:05,440 --> 00:06:08,150
 And so when the elder and the other novice went off for al

94
00:06:08,150 --> 00:06:12,440
ms, they went alone and the disobedient novice,

95
00:06:12,440 --> 00:06:17,170
 he stayed back and was just sulked at the kuti, at the cave

96
00:06:17,170 --> 00:06:20,440
, and the elder went with his other novice.

97
00:06:20,440 --> 00:06:24,250
 And then when they were gone, he went off to one of the

98
00:06:24,250 --> 00:06:26,440
 elder's supporters' houses,

99
00:06:26,440 --> 00:06:29,760
 went down into rajjagahadiyi's on his own and went to one

100
00:06:29,760 --> 00:06:31,440
 of the elder's supporters

101
00:06:31,440 --> 00:06:34,890
 and said to them that the elder was sick. They said, "Oh,

102
00:06:34,890 --> 00:06:36,440
 where's the elder? Why are you coming alone?"

103
00:06:36,440 --> 00:06:40,440
 He said, "Oh, he's sick, but he needs some special food.

104
00:06:40,440 --> 00:06:47,440
 Please give him very delicate and good tasting food and so

105
00:06:47,440 --> 00:06:47,440
 on

106
00:06:47,440 --> 00:06:50,440
 so he can recover from his sickness."

107
00:06:50,440 --> 00:06:52,490
 And so they gave him all this special food and prepared all

108
00:06:52,490 --> 00:06:53,440
 this special food for him.

109
00:06:53,440 --> 00:07:02,000
 And he took it back and ate it himself. And the elder went

110
00:07:02,000 --> 00:07:03,440
 on alms round with the other novice

111
00:07:03,440 --> 00:07:08,040
 and happened to get a full set of robes, new nice robes

112
00:07:08,040 --> 00:07:09,440
 from one family.

113
00:07:09,440 --> 00:07:12,440
 And so he gave them to the novice that went with him.

114
00:07:12,440 --> 00:07:16,230
 And the novice changed out of his old set and put on this

115
00:07:16,230 --> 00:07:17,440
 new set of robes.

116
00:07:17,440 --> 00:07:23,440
 And they went back to the monastery. On yet another day,

117
00:07:23,440 --> 00:07:24,440
 the elder went to sea,

118
00:07:24,440 --> 00:07:28,440
 happened to go to see this supporter who had been tricked.

119
00:07:28,440 --> 00:07:31,440
 And they said to the elder, "Oh, are you vulnerable, sir?

120
00:07:31,440 --> 00:07:33,440
 How are you doing? We heard that you were sick.

121
00:07:33,440 --> 00:07:39,310
 But we gave food to the novice when he came. Hope the food

122
00:07:39,310 --> 00:07:43,440
 helped you to get better."

123
00:07:43,440 --> 00:07:47,590
 And the elder just stood there and didn't say anything and

124
00:07:47,590 --> 00:07:49,440
 received food from him and went off.

125
00:07:49,440 --> 00:07:52,930
 And they went back to the monastery and he confronted this

126
00:07:52,930 --> 00:07:53,440
 novice.

127
00:07:53,440 --> 00:07:56,900
 And he said to him, "Is it true that you've been going

128
00:07:56,900 --> 00:07:57,440
 around?

129
00:07:57,440 --> 00:07:59,910
 Is it true that you went to these people and told them that

130
00:07:59,910 --> 00:08:00,440
 I was sick

131
00:08:00,440 --> 00:08:03,440
 and convinced them to give you all this special food?"

132
00:08:03,440 --> 00:08:07,440
 And he said, "What do you mean? It's just a..."

133
00:08:07,440 --> 00:08:11,440
 He scolded him. Mahakasipa scolded him and said, "That's

134
00:08:11,440 --> 00:08:12,440
 not something that a monk should do."

135
00:08:12,440 --> 00:08:16,440
 And the novice was totally fed up at this point.

136
00:08:16,440 --> 00:08:18,440
 He said, "Over a little bit of water, the elder gets upset

137
00:08:18,440 --> 00:08:21,440
 and then he gets upset about a little bit of food.

138
00:08:21,440 --> 00:08:25,080
 And on top of that, he gives a full set of robes to the

139
00:08:25,080 --> 00:08:26,440
 other novice."

140
00:08:26,440 --> 00:08:29,580
 So this novice got very, very angry and then he said, "I

141
00:08:29,580 --> 00:08:31,440
 know what I'm going to do."

142
00:08:31,440 --> 00:08:34,440
 And the next morning when the elder went off on Amstrand,

143
00:08:34,440 --> 00:08:39,670
 he went around the cave and broke up all the pots and

144
00:08:39,670 --> 00:08:40,440
 everything,

145
00:08:40,440 --> 00:08:43,550
 and just tore up everything and ripped up everything, all

146
00:08:43,550 --> 00:08:44,440
 the bedding and everything,

147
00:08:44,440 --> 00:08:48,440
 and then set the whole place on fire. And then he ran off.

148
00:08:48,440 --> 00:08:52,440
 And I think he ended up in hell as a result.

149
00:08:52,440 --> 00:08:58,270
 But the elder came back and saw all this and fixed it all

150
00:08:58,270 --> 00:08:59,440
 up as best he could

151
00:08:59,440 --> 00:09:01,440
 and of course went on with his life.

152
00:09:01,440 --> 00:09:06,790
 So this is a story of these two novices, one who was a good

153
00:09:06,790 --> 00:09:09,440
 companion and the other was a bad companion.

154
00:09:09,440 --> 00:09:12,410
 Eventually, the story got back to the Buddha. One of the

155
00:09:12,410 --> 00:09:14,440
 monks from Rajagah

156
00:09:14,440 --> 00:09:17,920
 wandered all the way to Savatthi and Buddha asked him, "How

157
00:09:17,920 --> 00:09:20,440
 was Mahakashiva doing?"

158
00:09:20,440 --> 00:09:24,040
 And he said, "Oh, he's doing fine." But he happened to have

159
00:09:24,040 --> 00:09:25,440
 these two students

160
00:09:25,440 --> 00:09:30,240
 and one of them was very, very good and followed the elder

161
00:09:30,240 --> 00:09:32,440
's instruction and advice

162
00:09:32,440 --> 00:09:34,440
 and practiced the elder's teachings.

163
00:09:34,440 --> 00:09:40,630
 And the other one was an evil scoundrel and not only did he

164
00:09:40,630 --> 00:09:41,440
 not practice,

165
00:09:41,440 --> 00:09:47,440
 but he also ended up destroying the elder's residence.

166
00:09:47,440 --> 00:09:51,840
 And the Buddha said, "Oh, yes, yes. The elder is better off

167
00:09:51,840 --> 00:09:53,440
 without this novice."

168
00:09:53,440 --> 00:09:56,520
 He told the story of the past. This is the introduction to

169
00:09:56,520 --> 00:09:57,440
 one of the Jataka stories

170
00:09:57,440 --> 00:10:00,440
 where he was a monkey and he tore up this bird's nest.

171
00:10:00,440 --> 00:10:03,440
 I'm not going to tell the story. It's not much to it.

172
00:10:03,440 --> 00:10:07,440
 He said, "Basically, this novice, this is the way he was."

173
00:10:07,440 --> 00:10:11,090
 And he said, "Better off without him." He said, "There's no

174
00:10:11,090 --> 00:10:12,440
 association with fools.

175
00:10:12,440 --> 00:10:17,060
 If the only people you can hang out with and rely upon are

176
00:10:17,060 --> 00:10:17,440
 fools,

177
00:10:17,440 --> 00:10:20,480
 well, you're better off without them." And then he told

178
00:10:20,480 --> 00:10:21,440
 this verse.

179
00:10:21,440 --> 00:10:25,030
 So a simple story, a story that I'm sure we can all relate

180
00:10:25,030 --> 00:10:25,440
 to,

181
00:10:25,440 --> 00:10:30,440
 us all living in caves having novices attend upon us.

182
00:10:30,440 --> 00:10:33,770
 No, all of us having good friends and bad friends, people

183
00:10:33,770 --> 00:10:34,440
 who we can rely upon

184
00:10:34,440 --> 00:10:38,440
 and people who we shouldn't rely upon, people who help us

185
00:10:38,440 --> 00:10:38,440
 up

186
00:10:38,440 --> 00:10:41,440
 and people who drag us down.

187
00:10:41,440 --> 00:10:49,750
 And so as meditators, this is one of the most important

188
00:10:49,750 --> 00:10:50,440
 aspects of our practice

189
00:10:50,440 --> 00:10:53,440
 is the association with good people.

190
00:10:53,440 --> 00:10:56,100
 The Buddha said it's all of the holy life, all of the

191
00:10:56,100 --> 00:10:57,440
 spiritual life.

192
00:10:57,440 --> 00:11:00,050
 The spiritual life is all about associating with the right

193
00:11:00,050 --> 00:11:00,440
 people,

194
00:11:00,440 --> 00:11:04,160
 having a good friend, having someone to guide you and teach

195
00:11:04,160 --> 00:11:06,440
 you and raise you up.

196
00:11:06,440 --> 00:11:11,440
 In many places he said that friends are good friends,

197
00:11:11,440 --> 00:11:15,440
 are most valuable and do not associate with fools.

198
00:11:15,440 --> 00:11:17,440
 Of course, there's the famous, the Mangala Sutta,

199
00:11:17,440 --> 00:11:20,580
 the first thing that the Buddha says is, "Not associating

200
00:11:20,580 --> 00:11:21,440
 with fools.

201
00:11:21,440 --> 00:11:26,440
 This is the greatest blessing."

202
00:11:26,440 --> 00:11:27,440
 So how does this go?

203
00:11:27,440 --> 00:11:30,440
 The verse is broken down quite well by the commentary,

204
00:11:30,440 --> 00:11:33,440
 so I'll go through that first and then talk a little bit

205
00:11:33,440 --> 00:11:33,440
 about

206
00:11:33,440 --> 00:11:37,440
 how it relates to meditation particularly.

207
00:11:37,440 --> 00:11:42,650
 First of all, the word "charaṁ," which means to fare or

208
00:11:42,650 --> 00:11:44,440
 to wander or to travel,

209
00:11:44,440 --> 00:11:51,440
 to move about, to live your life basically.

210
00:11:51,440 --> 00:11:55,080
 The commentary says it doesn't refer to moving about with

211
00:11:55,080 --> 00:11:55,440
 your body,

212
00:11:55,440 --> 00:12:00,890
 it refers to the movements of the mind or the way of the

213
00:12:00,890 --> 00:12:02,440
 mind.

214
00:12:02,440 --> 00:12:04,440
 So it has nothing to do with being close to someone.

215
00:12:04,440 --> 00:12:06,980
 It's not like if you shouldn't sit on the subway next to

216
00:12:06,980 --> 00:12:07,440
 someone

217
00:12:07,440 --> 00:12:10,440
 unless you're sure they're a wise person.

218
00:12:10,440 --> 00:12:13,590
 It also doesn't necessarily mean that you can't work or

219
00:12:13,590 --> 00:12:16,440
 live or cohabitate with such people.

220
00:12:16,440 --> 00:12:22,170
 So it says, "mānasa-charaṁ," which means the fairing of

221
00:12:22,170 --> 00:12:23,440
 the mind.

222
00:12:23,440 --> 00:12:25,440
 Don't let them into your heart basically.

223
00:12:25,440 --> 00:12:35,530
 Don't take their counsel or take them up as your intimate

224
00:12:35,530 --> 00:12:38,440
 associates.

225
00:12:38,440 --> 00:12:41,440
 And then what is meant by "sayyāng sādīsa-mātanos,"

226
00:12:41,440 --> 00:12:43,440
 and what is meant by one who is greater than you

227
00:12:43,440 --> 00:12:45,680
 or one who is equal to you, the commentary says it's

228
00:12:45,680 --> 00:12:47,440
 specifically in regards to

229
00:12:47,440 --> 00:12:50,740
 "sīla-sama-dīpañña," morality, concentration, and

230
00:12:50,740 --> 00:12:51,440
 wisdom.

231
00:12:51,440 --> 00:12:54,440
 So if someone has greater morality than you,

232
00:12:54,440 --> 00:12:57,440
 if there's someone who is practicing higher morality,

233
00:12:57,440 --> 00:13:02,440
 for instance, monks who are practicing celibacy and poverty

234
00:13:02,440 --> 00:13:14,490
 and who are taking themselves out of all of the distracting

235
00:13:14,490 --> 00:13:15,440
 activities

236
00:13:15,440 --> 00:13:17,440
 that laypeople will engage in,

237
00:13:17,440 --> 00:13:20,000
 then it's something that can help us to cultivate

238
00:13:20,000 --> 00:13:21,440
 concentration ourselves.

239
00:13:21,440 --> 00:13:26,300
 If we associate with such people, if we are around people

240
00:13:26,300 --> 00:13:29,440
 who are equal to us,

241
00:13:29,440 --> 00:13:34,440
 then our morality will not fade away.

242
00:13:34,440 --> 00:13:37,440
 If we're around people with greater concentration,

243
00:13:37,440 --> 00:13:39,440
 then their concentration will be a support for us.

244
00:13:39,440 --> 00:13:41,440
 It will be an example for us.

245
00:13:41,440 --> 00:13:46,930
 It will be something that we can rely upon or we can

246
00:13:46,930 --> 00:13:47,440
 emulate

247
00:13:47,440 --> 00:13:49,440
 to cultivate concentration ourselves.

248
00:13:49,440 --> 00:13:53,210
 And if we hang around people who have greater wisdom than

249
00:13:53,210 --> 00:13:53,440
 us,

250
00:13:53,440 --> 00:13:59,620
 then it will be something that inspires us and teaches us,

251
00:13:59,620 --> 00:14:00,440
 of course.

252
00:14:00,440 --> 00:14:04,440
 So the best thing, of course, is to hang out with someone

253
00:14:04,440 --> 00:14:06,440
 to associate with people who are greater than you

254
00:14:06,440 --> 00:14:11,440
 because you will develop what the good qualities of

255
00:14:11,440 --> 00:14:11,440
 morality,

256
00:14:11,440 --> 00:14:15,440
 concentration, and wisdom will increase if we're around

257
00:14:15,440 --> 00:14:15,440
 people

258
00:14:15,440 --> 00:14:19,940
 who are of greater morality, concentration, and wisdom than

259
00:14:19,940 --> 00:14:20,440
 us.

260
00:14:20,440 --> 00:14:24,330
 And if we are around people who are of equal, at an equal

261
00:14:24,330 --> 00:14:25,440
 level to us,

262
00:14:25,440 --> 00:14:28,440
 then we can be assured that we at least won't fall away

263
00:14:28,440 --> 00:14:31,440
 from our morality, concentration, and wisdom.

264
00:14:31,440 --> 00:14:37,550
 And as a result, through our own practice, we'll be able to

265
00:14:37,550 --> 00:14:38,440
 develop.

266
00:14:38,440 --> 00:14:41,440
 The point is that the people around us won't drag us down.

267
00:14:41,440 --> 00:14:44,440
 If you're around people who are inferior to you,

268
00:14:44,440 --> 00:14:47,440
 and if you rely upon them, and if you emulate them

269
00:14:47,440 --> 00:14:53,440
 and cultivate their qualities, then you will fall away

270
00:14:53,440 --> 00:14:56,440
 from your own morality and concentration,

271
00:14:56,440 --> 00:15:02,440
 because it comes under attack if you let them in too close.

272
00:15:02,440 --> 00:15:05,440
 Now, if you can't find someone who is greater

273
00:15:05,440 --> 00:15:07,440
 or someone who is equal to you, the Buddha said,

274
00:15:07,440 --> 00:15:11,440
 "Actually, it's preferable to hang out on your own."

275
00:15:11,440 --> 00:15:17,440
 Now, the key word here is "dalhang,"

276
00:15:17,440 --> 00:15:20,440
 because "dalhang" means steadfast or resolute,

277
00:15:20,440 --> 00:15:23,440
 and the point is that you actually need resolution.

278
00:15:23,440 --> 00:15:28,700
 You're on your own. Many people are always contacting us

279
00:15:28,700 --> 00:15:29,440
 constantly.

280
00:15:29,440 --> 00:15:32,160
 We get these emails from people saying that they're all

281
00:15:32,160 --> 00:15:32,440
 alone

282
00:15:32,440 --> 00:15:35,440
 and they need some guidance and bemoaning the fact

283
00:15:35,440 --> 00:15:38,440
 that they're not able to find a sangha or community.

284
00:15:38,440 --> 00:15:40,440
 And the Buddhists gave reassurance,

285
00:15:40,440 --> 00:15:43,090
 and we should reassure such people that you can practice on

286
00:15:43,090 --> 00:15:43,440
 your own.

287
00:15:43,440 --> 00:15:46,440
 You don't really need so much guidance.

288
00:15:46,440 --> 00:15:50,080
 It just takes more resolution, because if you're not

289
00:15:50,080 --> 00:15:50,440
 surrounded

290
00:15:50,440 --> 00:15:53,440
 by examples or people who are reminding you

291
00:15:53,440 --> 00:15:57,440
 of what you're doing, reminding you of the cultivation

292
00:15:57,440 --> 00:15:59,440
 of greater morality, concentration, and wisdom,

293
00:15:59,440 --> 00:16:01,440
 it's easier for yourself to fall away.

294
00:16:01,440 --> 00:16:07,440
 It's like a plant or a young tree that requires a cage

295
00:16:07,440 --> 00:16:09,440
 or some kind of support to keep it from falling over,

296
00:16:09,440 --> 00:16:13,440
 to keep it from breaking in the wind and so on.

297
00:16:13,440 --> 00:16:16,440
 That support is helpful unless the tree itself

298
00:16:16,440 --> 00:16:21,440
 is able to maintain its own strength and grow on its own.

299
00:16:21,440 --> 00:16:27,440
 So the benefit of having a good companion

300
00:16:27,440 --> 00:16:30,440
 is that they support you through your growth.

301
00:16:30,440 --> 00:16:32,440
 Not timbale sahayat dada.

302
00:16:32,440 --> 00:16:36,440
 Why is it that we should stay on our own

303
00:16:36,440 --> 00:16:38,440
 if we can't find such a person?

304
00:16:38,440 --> 00:16:40,440
 Because the association with fools.

305
00:16:40,440 --> 00:16:43,440
 There is no association with fools.

306
00:16:43,440 --> 00:16:46,440
 The meaning being that none of the good things that come

307
00:16:46,440 --> 00:16:50,440
 from associating with good people, morality, concentration,

308
00:16:50,440 --> 00:16:53,440
 and wisdom, all of the good kattawattu,

309
00:16:53,440 --> 00:16:58,440
 the ten types of profitable speech,

310
00:16:58,440 --> 00:17:03,440
 all of the dutanga practices, all of the vipassana insight,

311
00:17:03,440 --> 00:17:08,440
 and even the attainment of nirvana becomes difficult

312
00:17:08,440 --> 00:17:11,440
 and becomes threatened by fools.

313
00:17:11,440 --> 00:17:13,440
 It becomes surrounded by people who are distracting you,

314
00:17:13,440 --> 00:17:15,440
 who are talking about useless things,

315
00:17:15,440 --> 00:17:19,440
 and who are indulging in harmful pursuits,

316
00:17:19,440 --> 00:17:22,440
 killing and stealing and lying and cheating and so on.

317
00:17:22,440 --> 00:17:25,440
 All of these things will drag you down,

318
00:17:25,440 --> 00:17:27,440
 will make it more difficult for you to practice

319
00:17:27,440 --> 00:17:32,440
 and will even cause you to fall away from the practice.

320
00:17:32,440 --> 00:17:35,440
 So this is the, you know, it's quite a simple meaning,

321
00:17:35,440 --> 00:17:42,440
 but specifically in regards to the meditation practice,

322
00:17:42,440 --> 00:17:45,440
 it says, the point is that people will distract us

323
00:17:45,440 --> 00:17:48,820
 and people will sully or will be a cause for us to sully

324
00:17:48,820 --> 00:17:49,440
 our minds.

325
00:17:49,440 --> 00:17:54,440
 The question that always comes up is whether or not,

326
00:17:54,440 --> 00:17:57,440
 whether or not this means that we should ignore people

327
00:17:57,440 --> 00:18:01,440
 who are in trouble morally or have poor concentration,

328
00:18:01,440 --> 00:18:04,190
 people who are on the wrong path, shouldn't we help such

329
00:18:04,190 --> 00:18:04,440
 people?

330
00:18:04,440 --> 00:18:06,440
 And of course this is the exception.

331
00:18:06,440 --> 00:18:11,440
 The Buddha said, "Anyatra, anudaya, anyatra, anukampa,"

332
00:18:11,440 --> 00:18:16,440
 which means, and you shouldn't hang out,

333
00:18:16,440 --> 00:18:27,440
 "Etang, etang polisa, eso poliso, so poliso, nasevitable."

334
00:18:27,440 --> 00:18:32,440
 Such a person should not be associated with,

335
00:18:32,440 --> 00:18:36,440
 except in the case of pity or compassion.

336
00:18:36,440 --> 00:18:39,930
 So the commentary explains, which is exactly how it should

337
00:18:39,930 --> 00:18:42,440
 be explained,

338
00:18:42,440 --> 00:18:44,740
 that if you have compassion, if out of compassion for

339
00:18:44,740 --> 00:18:45,440
 someone,

340
00:18:45,440 --> 00:18:48,060
 it's possible or not even, you know, we should have the

341
00:18:48,060 --> 00:18:48,440
 compassion.

342
00:18:48,440 --> 00:18:51,440
 So if you think that a person is capable

343
00:18:51,440 --> 00:18:54,440
 and you think you're able to help them,

344
00:18:54,440 --> 00:18:56,440
 then you should, you should hang out with them

345
00:18:56,440 --> 00:18:58,440
 and you should try to support their practice

346
00:18:58,440 --> 00:19:00,440
 and you should help them to cultivate.

347
00:19:00,440 --> 00:19:03,440
 If you're not able to, if you try and you fail,

348
00:19:03,440 --> 00:19:07,670
 and it seems like this person is not going to develop

349
00:19:07,670 --> 00:19:09,440
 themselves,

350
00:19:09,440 --> 00:19:13,650
 then of course, then you should stay away from them

351
00:19:13,650 --> 00:19:14,440
 actually

352
00:19:14,440 --> 00:19:17,930
 and you should go your own way because they will drag you

353
00:19:17,930 --> 00:19:18,440
 down.

354
00:19:18,440 --> 00:19:23,440
 So in meditation, this is especially important

355
00:19:23,440 --> 00:19:28,930
 and this is why, because meditation is the direct

356
00:19:28,930 --> 00:19:31,440
 cultivation of wholesome mental states.

357
00:19:31,440 --> 00:19:38,440
 So if you're constantly, if you're distracted or led astray

358
00:19:38,440 --> 00:19:40,440
 by other people,

359
00:19:40,440 --> 00:19:45,950
 then your mind, it becomes impossible to cultivate wholes

360
00:19:45,950 --> 00:19:47,440
ome states of mind.

361
00:19:47,440 --> 00:19:55,440
 This is why the Buddha often talks about staying alone,

362
00:19:55,440 --> 00:19:57,440
 even away from good friends,

363
00:19:57,440 --> 00:20:07,440
 because even wholesome talk, if engaged in over much,

364
00:20:07,440 --> 00:20:13,440
 can lead to distraction as well.

365
00:20:13,440 --> 00:20:18,840
 Because meditation is the cultivation of the mind and our

366
00:20:18,840 --> 00:20:21,440
 environment

367
00:20:21,440 --> 00:20:26,730
 and our engagement with the environment and with the world

368
00:20:26,730 --> 00:20:27,440
 around us

369
00:20:27,440 --> 00:20:34,440
 is of utmost importance.

370
00:20:34,440 --> 00:20:37,650
 So we have to be careful how we engage with the world

371
00:20:37,650 --> 00:20:38,440
 around us.

372
00:20:38,440 --> 00:20:44,320
 So for a meditator, even good people we should only engage

373
00:20:44,320 --> 00:20:45,440
 in in moderation

374
00:20:45,440 --> 00:20:49,440
 and much less, much more so.

375
00:20:49,440 --> 00:20:58,640
 We should be careful when we associate or relate to or

376
00:20:58,640 --> 00:21:01,440
 interact with foolish people.

377
00:21:01,440 --> 00:21:05,440
 They will distract us, they take away our focus of mind,

378
00:21:05,440 --> 00:21:09,270
 they take away our clarity of mind, they stop us from

379
00:21:09,270 --> 00:21:10,440
 seeing things as they are.

380
00:21:10,440 --> 00:21:13,450
 We're not able to stay in the present because such people

381
00:21:13,450 --> 00:21:16,440
 are constantly in the past or in the future.

382
00:21:16,440 --> 00:21:19,340
 We're not able to stay in reality because such people are

383
00:21:19,340 --> 00:21:20,440
 always talking about concepts

384
00:21:20,440 --> 00:21:24,470
 and we're not able to stay with what is important because

385
00:21:24,470 --> 00:21:25,440
 such people are always engaged

386
00:21:25,440 --> 00:21:31,520
 and concerned with and intend upon what is unimportant,

387
00:21:31,520 --> 00:21:33,440
 what is unessential.

388
00:21:33,440 --> 00:21:40,930
 So unless we can help such people easily without interrupt

389
00:21:40,930 --> 00:21:41,440
ing our own practice,

390
00:21:41,440 --> 00:21:43,440
 we should be very careful to stay on our own.

391
00:21:43,440 --> 00:21:46,440
 So for many people this is I think an important reminder.

392
00:21:46,440 --> 00:21:50,100
 If you can't find someone who can lead you, who can help

393
00:21:50,100 --> 00:21:51,440
 you, who can support your practice,

394
00:21:51,440 --> 00:21:55,320
 stay on your own, you're better off on your own because

395
00:21:55,320 --> 00:21:58,440
 just having friends isn't,

396
00:21:58,440 --> 00:22:01,980
 just the fact that you have friends alone isn't necessarily

397
00:22:01,980 --> 00:22:03,440
 a useful or a good thing,

398
00:22:03,440 --> 00:22:05,930
 something that leads to your happiness. You need good

399
00:22:05,930 --> 00:22:06,440
 friends.

400
00:22:06,440 --> 00:22:08,950
 You need friends who lead you to good things, friends who

401
00:22:08,950 --> 00:22:11,440
 help you, who support you,

402
00:22:11,440 --> 00:22:15,440
 who encourage you in your own spiritual development.

403
00:22:15,440 --> 00:22:18,440
 So simple teaching but one that we should all keep in mind

404
00:22:18,440 --> 00:22:22,090
 and a very important aspect of our life is who we associate

405
00:22:22,090 --> 00:22:22,440
 with.

406
00:22:22,440 --> 00:22:25,050
 So that's the teaching for tonight. Thank you all for

407
00:22:25,050 --> 00:22:27,440
 tuning in and wish you all peace, happiness,

408
00:22:27,440 --> 00:22:29,440
 and freedom from suffering. Thank you.

