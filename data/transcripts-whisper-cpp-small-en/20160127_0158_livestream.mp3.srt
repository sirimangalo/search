1
00:00:00,000 --> 00:00:10,000
 [BLANK_AUDIO]

2
00:00:10,000 --> 00:00:20,000
 [BLANK_AUDIO]

3
00:00:35,000 --> 00:00:43,500
 Good evening everyone, Broadcasting Live, January 26, 2016.

4
00:00:43,500 --> 00:00:53,500
 [BLANK_AUDIO]

5
00:00:53,500 --> 00:00:57,000
 No teacher like the Buddha.

6
00:00:57,000 --> 00:01:00,000
 [BLANK_AUDIO]

7
00:01:00,000 --> 00:01:04,210
 I have a quote today that says, there's no teacher like the

8
00:01:04,210 --> 00:01:04,500
 Buddha.

9
00:01:04,500 --> 00:01:09,000
 It's a shame really we don't have the Buddha here with us.

10
00:01:09,000 --> 00:01:12,500
 [BLANK_AUDIO]

11
00:01:12,500 --> 00:01:17,500
 Either that or we should be practicing the Mahayana where

12
00:01:17,500 --> 00:01:22,500
 apparently the Buddha is always up in heaven.

13
00:01:22,500 --> 00:01:24,500
 [BLANK_AUDIO]

14
00:01:24,500 --> 00:01:30,000
 And some kind of super realm never really dies.

15
00:01:30,000 --> 00:01:32,000
 [BLANK_AUDIO]

16
00:01:32,000 --> 00:01:35,500
 Because if he were to die, if he were to pass away,

17
00:01:35,500 --> 00:01:39,960
 he would enter into Parinibbana and never be in contact

18
00:01:39,960 --> 00:01:42,500
 with the world.

19
00:01:42,500 --> 00:01:45,000
 [BLANK_AUDIO]

20
00:01:45,000 --> 00:01:47,000
 That would be rather selfish of him.

21
00:01:47,000 --> 00:01:51,000
 [BLANK_AUDIO]

22
00:01:51,000 --> 00:01:56,000
 So they say, they say there are three bodies of the Buddha.

23
00:01:56,000 --> 00:02:01,000
 There's the Dhamma Kayana, and there's the Sambhoga Kayana,

24
00:02:01,000 --> 00:02:03,000
 and there's the Nimitta Kayana.

25
00:02:03,000 --> 00:02:08,000
 So what we see is only a replica of the Buddha.

26
00:02:08,000 --> 00:02:11,000
 The actual Buddha never came to Earth.

27
00:02:11,000 --> 00:02:13,000
 [BLANK_AUDIO]

28
00:02:13,000 --> 00:02:17,000
 He's just living up in heaven, some kind of heavenly realm.

29
00:02:17,000 --> 00:02:19,000
 [BLANK_AUDIO]

30
00:02:19,000 --> 00:02:21,000
 That's Mahayana Buddhism.

31
00:02:21,000 --> 00:02:25,280
 In Theravada there are three guys, three bodies of the

32
00:02:25,280 --> 00:02:27,000
 Buddha as well.

33
00:02:27,000 --> 00:02:29,000
 But it's a bit different.

34
00:02:29,000 --> 00:02:36,000
 We have the Nama Kaya, the Rupa Kaya, and the Dhamma Kaya.

35
00:02:36,000 --> 00:02:45,000
 The Nama Kaya is the thoughts and intentions and

36
00:02:45,000 --> 00:02:51,000
 mental qualities of the Buddha.

37
00:02:51,000 --> 00:02:57,000
 The Rupa Kaya is the physical aspect of the Buddha.

38
00:02:57,000 --> 00:03:00,000
 The bones and the flesh.

39
00:03:00,000 --> 00:03:07,000
 [BLANK_AUDIO]

40
00:03:07,000 --> 00:03:10,140
 And the Dhamma Kaya is the body of teachings that he left

41
00:03:10,140 --> 00:03:11,000
 behind.

42
00:03:11,000 --> 00:03:14,000
 These are the three Kayas that we recognize.

43
00:03:14,000 --> 00:03:17,000
 [BLANK_AUDIO]

44
00:03:17,000 --> 00:03:22,000
 So the Nama Kaya of the Buddha is gone, it's extinguished,

45
00:03:22,000 --> 00:03:24,000
 blown out like a light.

46
00:03:24,000 --> 00:03:26,000
 [BLANK_AUDIO]

47
00:03:26,000 --> 00:03:30,300
 If it weren't that way, there would not be any point to

48
00:03:30,300 --> 00:03:31,000
 this.

49
00:03:31,000 --> 00:03:38,060
 If the Buddha hadn't left Samsara, then there would be no

50
00:03:38,060 --> 00:03:39,000
 point.

51
00:03:39,000 --> 00:03:41,000
 He wouldn't have been a leader.

52
00:03:41,000 --> 00:03:47,000
 [BLANK_AUDIO]

53
00:03:47,000 --> 00:03:51,000
 It would mean his teaching didn't lead to nirvana,

54
00:03:51,000 --> 00:03:53,000
 didn't lead to freedom.

55
00:03:53,000 --> 00:03:56,000
 He couldn't really trust what he said.

56
00:03:56,000 --> 00:04:02,000
 [BLANK_AUDIO]

57
00:04:02,000 --> 00:04:05,000
 So it depends who you ask.

58
00:04:05,000 --> 00:04:09,000
 But if you ask the people who recorded this quote, or

59
00:04:09,000 --> 00:04:13,000
 we believe this quote comes from the Buddha, so the people

60
00:04:13,000 --> 00:04:13,000
 who

61
00:04:13,000 --> 00:04:17,000
 recorded this, recorded the Buddha as saying this.

62
00:04:17,000 --> 00:04:20,000
 [BLANK_AUDIO]

63
00:04:20,000 --> 00:04:24,260
 It says the Buddha acts as he speaks and he speaks as he

64
00:04:24,260 --> 00:04:25,000
 acts.

65
00:04:25,000 --> 00:04:28,000
 He is consistent.

66
00:04:28,000 --> 00:04:30,990
 And the other schools of Buddhism who say he was not

67
00:04:30,990 --> 00:04:33,000
 consistent.

68
00:04:33,000 --> 00:04:37,000
 He said that he was going into Parinibbana, well, not

69
00:04:37,000 --> 00:04:37,000
 exactly.

70
00:04:37,000 --> 00:04:42,000
 [BLANK_AUDIO]

71
00:04:42,000 --> 00:04:46,000
 For us, this is very important.

72
00:04:46,000 --> 00:04:50,000
 It's important to understand why an enlightened being will

73
00:04:50,000 --> 00:04:52,000
 leave Samsara.

74
00:04:52,000 --> 00:04:55,320
 Because people think isn't that selfish to leave the world

75
00:04:55,320 --> 00:04:56,000
 behind?

76
00:04:56,000 --> 00:05:01,000
 [BLANK_AUDIO]

77
00:05:01,000 --> 00:05:04,000
 This charge of selfishness is really interesting.

78
00:05:04,000 --> 00:05:06,000
 As I've said before, who cares?

79
00:05:06,000 --> 00:05:08,000
 What if it is selfish?

80
00:05:08,000 --> 00:05:11,000
 [BLANK_AUDIO]

81
00:05:11,000 --> 00:05:13,000
 If you do everything that makes you happy,

82
00:05:13,000 --> 00:05:16,000
 [BLANK_AUDIO]

83
00:05:16,000 --> 00:05:18,000
 If everything you do is to make you happy,

84
00:05:18,000 --> 00:05:21,000
 [BLANK_AUDIO]

85
00:05:21,000 --> 00:05:23,000
 Who cares whether it's selfish, right?

86
00:05:23,000 --> 00:05:26,000
 [BLANK_AUDIO]

87
00:05:26,000 --> 00:05:28,600
 That doesn't strike us well, but it's a very powerful

88
00:05:28,600 --> 00:05:29,000
 statement.

89
00:05:29,000 --> 00:05:32,000
 [BLANK_AUDIO]

90
00:05:32,000 --> 00:05:35,000
 We think that's horrible, that's so selfish.

91
00:05:35,000 --> 00:05:37,000
 Don't care about anyone else's happiness.

92
00:05:37,000 --> 00:05:43,000
 [BLANK_AUDIO]

93
00:05:43,000 --> 00:05:45,000
 But it's the most elegant.

94
00:05:45,000 --> 00:05:48,000
 It's not horrible, it's not selfish, it's elegant.

95
00:05:48,000 --> 00:05:50,000
 Because it makes sense.

96
00:05:50,000 --> 00:05:52,000
 You can understand that on a primal level, yes.

97
00:05:52,000 --> 00:05:55,000
 If I'm happy, then where's the problem?

98
00:05:55,000 --> 00:05:57,000
 Immediately you think,

99
00:05:57,000 --> 00:05:59,000
 [BLANK_AUDIO]

100
00:05:59,000 --> 00:06:02,000
 The problem is, what about other people?

101
00:06:02,000 --> 00:06:05,000
 Does that mean happiness at the expense of others?

102
00:06:05,000 --> 00:06:07,000
 Well, the elegance of it is that it doesn't.

103
00:06:07,000 --> 00:06:11,000
 [BLANK_AUDIO]

104
00:06:11,000 --> 00:06:12,000
 It may mean,

105
00:06:12,000 --> 00:06:22,000
 [BLANK_AUDIO]

106
00:06:22,000 --> 00:06:24,000
 It may mean forgoing, helping others.

107
00:06:24,000 --> 00:06:29,000
 [BLANK_AUDIO]

108
00:06:29,000 --> 00:06:31,000
 But if your goal is to help others, you've got a big

109
00:06:31,000 --> 00:06:32,000
 problem,

110
00:06:32,000 --> 00:06:36,000
 because there's always gonna be more people to help.

111
00:06:36,000 --> 00:06:38,740
 And furthermore, if everyone goes around helping everyone

112
00:06:38,740 --> 00:06:39,000
 else,

113
00:06:39,000 --> 00:06:42,000
 then who's helping oneself?

114
00:06:42,000 --> 00:06:45,000
 Who's working for themselves?

115
00:06:45,000 --> 00:06:47,000
 Who's ever gonna be happy?

116
00:06:47,000 --> 00:06:50,000
 [BLANK_AUDIO]

117
00:06:50,000 --> 00:06:52,000
 So instead of teaching people to help others,

118
00:06:52,000 --> 00:06:55,000
 we teach people to help themselves.

119
00:06:55,000 --> 00:06:59,350
 It's really what works the best, because the help you can

120
00:06:59,350 --> 00:07:00,000
 give to others

121
00:07:00,000 --> 00:07:03,000
 turns out to be quite small,

122
00:07:03,000 --> 00:07:06,000
 compared to the help you can give to yourself.

123
00:07:06,000 --> 00:07:09,690
 Even the Buddha, he taught people to be a refuge for

124
00:07:09,690 --> 00:07:11,000
 themselves.

125
00:07:11,000 --> 00:07:15,000
 The things he taught were about self-teaching.

126
00:07:15,000 --> 00:07:18,000
 It wasn't about giving people fish, it was about giving

127
00:07:18,000 --> 00:07:18,000
 people,

128
00:07:18,000 --> 00:07:21,000
 teaching people how to fish.

129
00:07:21,000 --> 00:07:29,000
 [BLANK_AUDIO]

130
00:07:29,000 --> 00:07:34,860
 So the Buddha's gone, and that's the best thing he could

131
00:07:34,860 --> 00:07:36,000
 have done,

132
00:07:36,000 --> 00:07:39,000
 the most powerful thing he could have done,

133
00:07:39,000 --> 00:07:41,000
 because it gives us a true path,

134
00:07:41,000 --> 00:07:43,220
 a way that it does make sense, it does have meaning, it

135
00:07:43,220 --> 00:07:45,000
 does have an end.

136
00:07:45,000 --> 00:07:49,000
 It's not without an end, it's not without a goal.

137
00:07:49,000 --> 00:07:58,000
 [BLANK_AUDIO]

138
00:07:58,000 --> 00:08:01,000
 And moreover, there was no other way.

139
00:08:01,000 --> 00:08:03,000
 If the Buddha were to stay behind, as I said,

140
00:08:03,000 --> 00:08:06,390
 it would mean he weren't enlightened, it would mean he hadn

141
00:08:06,390 --> 00:08:08,000
't reached the goal.

142
00:08:08,000 --> 00:08:11,000
 It's not possible for an Arahant even if they wanted to,

143
00:08:11,000 --> 00:08:15,080
 because of course they can't want to stay behind

144
00:08:15,080 --> 00:08:16,000
 continually.

145
00:08:16,000 --> 00:08:21,000
 [BLANK_AUDIO]

146
00:08:21,000 --> 00:08:23,000
 So this is the goal that we're aiming for,

147
00:08:23,000 --> 00:08:26,000
 this is the legacy that the Buddha left behind.

148
00:08:26,000 --> 00:08:29,000
 He opened the door for us, and then he stepped through.

149
00:08:29,000 --> 00:08:33,000
 It's up to us whether we're going to follow him.

150
00:08:33,000 --> 00:08:38,800
 His practice is for cessation, for the cessation of

151
00:08:38,800 --> 00:08:40,000
 suffering,

152
00:08:40,000 --> 00:08:46,000
 suffering that we have, we have a way to make it cease,

153
00:08:46,000 --> 00:08:49,000
 we have a way to be free from suffering.

154
00:08:49,000 --> 00:08:54,360
 However much you practice, that's how much suffering you'll

155
00:08:54,360 --> 00:08:55,000
 do away with.

156
00:08:55,000 --> 00:08:58,380
 You practice a lot, you'll free yourself from a lot of

157
00:08:58,380 --> 00:08:59,000
 suffering.

158
00:08:59,000 --> 00:09:04,350
 Practice to the end of the path, you'll free yourself from

159
00:09:04,350 --> 00:09:06,000
 all suffering.

160
00:09:06,000 --> 00:09:10,000
 The door is open, it's up to us to walk through.

161
00:09:10,000 --> 00:09:17,000
 [BLANK_AUDIO]

162
00:09:17,000 --> 00:09:19,810
 Today I was teaching meditation all day, more five minute

163
00:09:19,810 --> 00:09:21,000
 meditation lessons.

164
00:09:21,000 --> 00:09:24,000
 [BLANK_AUDIO]

165
00:09:24,000 --> 00:09:28,570
 And then I had to do this interview for a talk that I'm

166
00:09:28,570 --> 00:09:30,000
 giving downtown.

167
00:09:30,000 --> 00:09:35,000
 I think it didn't go very well, because I was very tired

168
00:09:35,000 --> 00:09:38,760
 and didn't really have my lines memorized, the talk memor

169
00:09:38,760 --> 00:09:39,000
ized.

170
00:09:39,000 --> 00:09:44,000
 So probably not going to be giving a TED talk after all.

171
00:09:44,000 --> 00:09:47,460
 They asked me, they asked me, why do you want to give this

172
00:09:47,460 --> 00:09:48,000
 talk?

173
00:09:48,000 --> 00:09:50,000
 Why do you really want to give this talk?

174
00:09:50,000 --> 00:09:54,440
 And I said, to be honest, I'm kind of non-committal about

175
00:09:54,440 --> 00:09:55,000
 it.

176
00:09:55,000 --> 00:09:58,460
 If there's somebody else who wants it more, you should give

177
00:09:58,460 --> 00:10:00,000
 it to them.

178
00:10:00,000 --> 00:10:03,940
 I mean, the interview went pretty well, but when I had to

179
00:10:03,940 --> 00:10:05,000
 give the talk,

180
00:10:05,000 --> 00:10:07,410
 I had to have some of it memorized and it wasn't really

181
00:10:07,410 --> 00:10:08,000
 memorized.

182
00:10:08,000 --> 00:10:11,550
 And I had to go by the slides and I couldn't remember which

183
00:10:11,550 --> 00:10:14,000
 slide was which.

184
00:10:14,000 --> 00:10:17,000
 I don't think they were that impressed.

185
00:10:17,000 --> 00:10:22,590
 Anyway, it's been a long day, so I think I'm going to stop

186
00:10:22,590 --> 00:10:23,000
 there.

187
00:10:23,000 --> 00:10:26,000
 I want to show all good night.

188
00:10:26,000 --> 00:10:29,000
 Keep practicing, no?

189
00:10:29,000 --> 00:10:30,000
 Night.

190
00:10:30,000 --> 00:10:40,000
 [BLANK_AUDIO]

