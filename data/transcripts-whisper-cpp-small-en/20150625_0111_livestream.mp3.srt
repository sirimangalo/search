1
00:00:00,000 --> 00:00:27,680
 Okay, we are online.

2
00:00:27,680 --> 00:00:31,040
 Hello and welcome back to our study of the Dhammapada.

3
00:00:31,040 --> 00:00:38,390
 Once more we continue on with first number 75, which reads

4
00:00:38,390 --> 00:00:40,200
 as follows.

5
00:00:40,200 --> 00:00:49,560
 "Anya hi labhupani sa, anya nimban gamini, eva metang abhin

6
00:00:49,560 --> 00:01:00,380
 yaya bhikkhu buddha saavakau, sakkara nabi nandeya viveka

7
00:01:00,380 --> 00:01:04,280
 manupra yeti."

8
00:01:04,280 --> 00:01:16,720
 Which means the path or that which is a means to gain, that

9
00:01:16,720 --> 00:01:21,080
 which is a means for gain, labhupani.

10
00:01:21,080 --> 00:01:27,190
 That which is a means for gain and that which leads to nimb

11
00:01:27,190 --> 00:01:28,040
ana.

12
00:01:28,040 --> 00:01:30,680
 These two are "anya" meaning they are different.

13
00:01:30,680 --> 00:01:33,480
 They are not the same.

14
00:01:33,480 --> 00:01:37,960
 One way is they are other than each other.

15
00:01:37,960 --> 00:01:41,960
 They are apart from each other.

16
00:01:41,960 --> 00:01:49,960
 "Ewa metang abhin yaya, abhin yaya."

17
00:01:49,960 --> 00:01:56,410
 For someone who understands this fully or in an ultimate

18
00:01:56,410 --> 00:01:58,680
 sense, deeply.

19
00:01:58,680 --> 00:02:06,880
 Abhikkhu who is a savaka, student of the Buddha, should not

20
00:02:06,880 --> 00:02:14,040
 be overjoyed by sakkara, by worship

21
00:02:14,040 --> 00:02:17,880
 or respect or offerings.

22
00:02:17,880 --> 00:02:27,440
 Should not by gains, by that which won by wealth and so on.

23
00:02:27,440 --> 00:02:33,190
 Thinking about viveka manupra yayi, thinking about or res

24
00:02:33,190 --> 00:02:36,400
olute upon viveka, which means

25
00:02:36,400 --> 00:02:38,760
 seclusion.

26
00:02:38,760 --> 00:02:42,290
 So a person who is a student of the Buddha should not be

27
00:02:42,290 --> 00:02:44,400
 inclined towards gain or fame

28
00:02:44,400 --> 00:02:47,920
 or wealth or so on.

29
00:02:47,920 --> 00:02:53,280
 They should be inclined towards seclusion.

30
00:02:53,280 --> 00:02:57,660
 The meaning and why these two are put together is because

31
00:02:57,660 --> 00:03:00,160
 of the story which has to do with

32
00:03:00,160 --> 00:03:08,890
 a novice who has this obvious choice between great gain and

33
00:03:08,890 --> 00:03:12,800
 wealth and prosperity in a

34
00:03:12,800 --> 00:03:19,060
 worldly sense and chooses to leave it behind for the forest

35
00:03:19,060 --> 00:03:19,200
.

36
00:03:19,200 --> 00:03:24,400
 There he goes, that there was an aged brahmin.

37
00:03:24,400 --> 00:03:29,130
 From the time of the Buddha the religion was brahminism and

38
00:03:29,130 --> 00:03:31,520
 there were these priests who

39
00:03:31,520 --> 00:03:37,560
 would oversee rituals, ceremonies.

40
00:03:37,560 --> 00:03:41,770
 And so this was a very poor brahmin, aged and maybe forget

41
00:03:41,770 --> 00:03:43,760
ful anyway, not very well

42
00:03:43,760 --> 00:03:47,790
 known and maybe in a place where I guess with the advent of

43
00:03:47,790 --> 00:03:49,840
 Buddhism there wasn't much for

44
00:03:49,840 --> 00:03:52,480
 the brahmin to do because people weren't all that

45
00:03:52,480 --> 00:03:54,640
 interested in rituals and ceremonies

46
00:03:54,640 --> 00:03:58,040
 anymore.

47
00:03:58,040 --> 00:04:04,090
 So he never had much food or much wealth but he had great

48
00:04:04,090 --> 00:04:07,280
 faith in Sariputta, the Buddha's

49
00:04:07,280 --> 00:04:10,000
 chief disciple.

50
00:04:10,000 --> 00:04:13,160
 And so Sariputta would go for alms to this brahmin's house

51
00:04:13,160 --> 00:04:14,800
 but the brahmin had nothing

52
00:04:14,800 --> 00:04:20,150
 to give him and so when he knew that Sariputta was coming

53
00:04:20,150 --> 00:04:23,400
 he would hide and avoid the elder

54
00:04:23,400 --> 00:04:26,660
 out of embarrassment that he had nothing to give.

55
00:04:26,660 --> 00:04:29,950
 And time and again he would do this when he knew that Sarip

56
00:04:29,950 --> 00:04:31,600
utta was coming and then one

57
00:04:31,600 --> 00:04:38,410
 day he was able to obtain a single portion of rice gruel

58
00:04:38,410 --> 00:04:41,360
 and a piece of cloth.

59
00:04:41,360 --> 00:04:45,250
 And immediately he thought that he had done some ceremony

60
00:04:45,250 --> 00:04:47,200
 and had gotten this as a gift

61
00:04:47,200 --> 00:04:51,480
 or payment for his services.

62
00:04:51,480 --> 00:04:55,200
 And so he immediately thought that this would make a great

63
00:04:55,200 --> 00:04:57,160
 offering for the elder who he

64
00:04:57,160 --> 00:05:00,320
 hadn't been able to support.

65
00:05:00,320 --> 00:05:04,990
 And so when the elder came to his door he leapt at the

66
00:05:04,990 --> 00:05:08,200
 opportunity and started pouring

67
00:05:08,200 --> 00:05:10,320
 the rice gruel into the elder's bowl and he got halfway

68
00:05:10,320 --> 00:05:11,640
 done and the elder covered his

69
00:05:11,640 --> 00:05:16,550
 bowl up signifying that he didn't want to take all the food

70
00:05:16,550 --> 00:05:18,160
 from the brahmin.

71
00:05:18,160 --> 00:05:21,040
 You want to leave some for him to eat as well.

72
00:05:21,040 --> 00:05:24,900
 But the brahmin refused and said, "Listen, my wish for this

73
00:05:24,900 --> 00:05:26,520
 is to be happy in the next

74
00:05:26,520 --> 00:05:27,520
 life.

75
00:05:27,520 --> 00:05:31,510
 I'm doing this for the purpose of my benefit, even at the

76
00:05:31,510 --> 00:05:33,840
 expense of my well-being right

77
00:05:33,840 --> 00:05:34,840
 now."

78
00:05:34,840 --> 00:05:38,120
 So he was going to go hungry without eating.

79
00:05:38,120 --> 00:05:43,220
 And so he pushed the elder to accept the entire meal and

80
00:05:43,220 --> 00:05:46,520
 then he gave him the piece of cloth

81
00:05:46,520 --> 00:05:50,250
 and he said, "Through the power of this offering may I

82
00:05:50,250 --> 00:05:53,120
 obtain the realization, the same realization

83
00:05:53,120 --> 00:05:54,120
 that you have gained."

84
00:05:54,120 --> 00:06:01,720
 And Sariputta said, "Ey wang ho tu, may it be thus."

85
00:06:01,720 --> 00:06:10,760
 I believe that's what he said.

86
00:06:10,760 --> 00:06:12,160
 And went on his way.

87
00:06:12,160 --> 00:06:15,510
 Now when the brahmin died out of his great reverence and

88
00:06:15,510 --> 00:06:17,480
 respect for Sariputta, he was

89
00:06:17,480 --> 00:06:22,960
 born in the womb of one of Sariputta's chief female

90
00:06:22,960 --> 00:06:27,320
 disciples, lay disciples.

91
00:06:27,320 --> 00:06:32,480
 And they say during the time of her pregnancy, she had

92
00:06:32,480 --> 00:06:34,640
 these strange cravings.

93
00:06:34,640 --> 00:06:39,130
 They say pregnant people will have cravings for this or

94
00:06:39,130 --> 00:06:39,920
 that.

95
00:06:39,920 --> 00:06:42,550
 And I guess the understanding is that it has something to

96
00:06:42,550 --> 00:06:44,560
 do with the belief, or whatever,

97
00:06:44,560 --> 00:06:47,320
 something to do with the child, the nature of the child.

98
00:06:47,320 --> 00:06:50,680
 So they say she had cravings to give offerings to the monks

99
00:06:50,680 --> 00:06:52,360
 all the time and go to listen

100
00:06:52,360 --> 00:06:53,800
 to the Dhamma.

101
00:06:53,800 --> 00:06:56,000
 She had these strange cravings.

102
00:06:56,000 --> 00:07:02,440
 Anyway, this boy is born and they bring him to the elder

103
00:07:02,440 --> 00:07:06,000
 and the elder names him Tissa.

104
00:07:06,000 --> 00:07:12,660
 And he has such great karma or merit or goodness that he's

105
00:07:12,660 --> 00:07:16,280
 stocked up just from this one act

106
00:07:16,280 --> 00:07:18,040
 of giving with a pure heart.

107
00:07:18,040 --> 00:07:22,340
 And the commentary remarks that there's something special

108
00:07:22,340 --> 00:07:26,880
 about giving when you're hard up yourself.

109
00:07:26,880 --> 00:07:29,720
 There's a quote Jack London, I think.

110
00:07:29,720 --> 00:07:32,760
 He said, "Rich people don't know how to give.

111
00:07:32,760 --> 00:07:35,630
 When a rich person gives, they don't really understand

112
00:07:35,630 --> 00:07:36,240
 giving.

113
00:07:36,240 --> 00:07:38,720
 They don't have a sense of what it means to really give.

114
00:07:38,720 --> 00:07:42,880
 But a poor person understands what need is and therefore is

115
00:07:42,880 --> 00:07:44,080
 able to give.

116
00:07:44,080 --> 00:07:49,930
 And so there's generally a much higher sense of the import

117
00:07:49,930 --> 00:07:51,600
 of the deed."

118
00:07:51,600 --> 00:07:54,720
 So as a result of this great wholesomeness that he

119
00:07:54,720 --> 00:07:57,040
 cultivated just with the one act of

120
00:07:57,040 --> 00:08:02,010
 sacrificing his own meal and his own possession of the

121
00:08:02,010 --> 00:08:06,320
 cloth, he was reborn with great merit.

122
00:08:06,320 --> 00:08:11,220
 And later on he became a novice when he was, I guess, seven

123
00:08:11,220 --> 00:08:12,400
 years old.

124
00:08:12,400 --> 00:08:14,400
 So he put or ordained him.

125
00:08:14,400 --> 00:08:19,720
 And when he went for alms food, he would get hundreds of

126
00:08:19,720 --> 00:08:25,240
 people coming to offer him food.

127
00:08:25,240 --> 00:08:28,740
 And then he would give all this excess food that he got

128
00:08:28,740 --> 00:08:30,040
 from the people.

129
00:08:30,040 --> 00:08:32,000
 He would give it out to all the monks.

130
00:08:32,000 --> 00:08:35,860
 And so they called him Tissa the alms giver because he was

131
00:08:35,860 --> 00:08:37,880
 always going around giving

132
00:08:37,880 --> 00:08:42,900
 alms to all the other monks who weren't as lucky to have as

133
00:08:42,900 --> 00:08:45,240
 much support as he did.

134
00:08:45,240 --> 00:08:47,560
 And so that was his name for a while.

135
00:08:47,560 --> 00:08:50,740
 And then in the winter it got really cold and he noticed

136
00:08:50,740 --> 00:08:52,440
 that the monks were without

137
00:08:52,440 --> 00:08:56,350
 blankets and the monks were cold or were huddling around

138
00:08:56,350 --> 00:08:59,220
 fires and he said, "Why are you rubbing

139
00:08:59,220 --> 00:09:01,920
 yourselves and why are you warming yourself by the fire?"

140
00:09:01,920 --> 00:09:03,520
 He said, "Well, it's winter.

141
00:09:03,520 --> 00:09:04,520
 It's cold."

142
00:09:04,520 --> 00:09:08,080
 And he said, "Well, why don't you just get a blanket?"

143
00:09:08,080 --> 00:09:11,900
 And the monk said, "Well, it's easy for you to say 'he' of

144
00:09:11,900 --> 00:09:13,120
 great merit.

145
00:09:13,120 --> 00:09:15,560
 Maybe it's easy for you to get a blanket but not easy for

146
00:09:15,560 --> 00:09:15,960
 us."

147
00:09:15,960 --> 00:09:19,400
 And he said, "Well, in that case, come with me."

148
00:09:19,400 --> 00:09:23,100
 And so he rounded up all the monks and he took them into

149
00:09:23,100 --> 00:09:26,120
 the, into Sawati and immediately

150
00:09:26,120 --> 00:09:28,760
 people came and gave him blankets.

151
00:09:28,760 --> 00:09:31,760
 And so he got hundreds and hundreds of blankets.

152
00:09:31,760 --> 00:09:36,120
 And so then they called him the Disa the Blanket Giver and

153
00:09:36,120 --> 00:09:38,640
 that was his name for a while.

154
00:09:38,640 --> 00:09:42,710
 And this went on and he was able to obtain whatever he

155
00:09:42,710 --> 00:09:45,040
 wanted but he was still quite

156
00:09:45,040 --> 00:09:47,880
 unsatisfied.

157
00:09:47,880 --> 00:09:50,730
 And he realized that if he were to stay in Sawati he would

158
00:09:50,730 --> 00:09:52,200
 never accomplish the goal

159
00:09:52,200 --> 00:09:55,660
 of the holy life because there were too many visitors, too

160
00:09:55,660 --> 00:09:57,680
 many of his relatives and people

161
00:09:57,680 --> 00:09:59,880
 coming to see him.

162
00:09:59,880 --> 00:10:04,920
 So he ran away and went off to live in the forest.

163
00:10:04,920 --> 00:10:07,720
 And then he got a new name and so his new name and so by

164
00:10:07,720 --> 00:10:09,440
 this time he had three names

165
00:10:09,440 --> 00:10:11,200
 according to the commentary.

166
00:10:11,200 --> 00:10:14,520
 His third name was Disa the Forest Dweller.

167
00:10:14,520 --> 00:10:16,330
 So they had all these different names and they were trying

168
00:10:16,330 --> 00:10:17,320
 to figure out which one to

169
00:10:17,320 --> 00:10:18,320
 call him.

170
00:10:18,320 --> 00:10:23,440
 Anyway, in the end he was called Disa the Forest Dweller.

171
00:10:23,440 --> 00:10:29,260
 And the story goes that while he was in the forest he would

172
00:10:29,260 --> 00:10:32,560
 say, whenever he came to see

173
00:10:32,560 --> 00:10:37,130
 the people he stayed so much to himself that he would never

174
00:10:37,130 --> 00:10:39,480
 say much to the lay people.

175
00:10:39,480 --> 00:10:44,520
 And this is sort of a common trait especially for monks

176
00:10:44,520 --> 00:10:51,080
 intent upon attaining realization.

177
00:10:51,080 --> 00:10:55,240
 He would just say, "Sukhi hota dukka pumuchata."

178
00:10:55,240 --> 00:11:02,470
 "May you be happy, may you be well, and may you be free

179
00:11:02,470 --> 00:11:05,320
 from suffering."

180
00:11:05,320 --> 00:11:07,240
 That was all he would say.

181
00:11:07,240 --> 00:11:08,240
 Every day, every day.

182
00:11:08,240 --> 00:11:11,040
 Same thing, same thing.

183
00:11:11,040 --> 00:11:13,930
 And so they got started to get the feeling that this guy

184
00:11:13,930 --> 00:11:15,560
 was, this was all he was good

185
00:11:15,560 --> 00:11:20,200
 for but they were happy to take care of him.

186
00:11:20,200 --> 00:11:23,500
 And then one day his preceptor, Sariputta came with a whole

187
00:11:23,500 --> 00:11:24,760
 bunch of other monks.

188
00:11:24,760 --> 00:11:29,020
 Actually they say he came with all the senior elder monks

189
00:11:29,020 --> 00:11:30,720
 because this novice was a sort

190
00:11:30,720 --> 00:11:35,360
 of a very special person.

191
00:11:35,360 --> 00:11:38,560
 And then the Buddha came.

192
00:11:38,560 --> 00:11:42,500
 And anyway, the only point here is that at one point they

193
00:11:42,500 --> 00:11:44,680
 wanted to ask him, they asked

194
00:11:44,680 --> 00:11:46,200
 the Buddha to give a talk.

195
00:11:46,200 --> 00:11:49,040
 It was either the Buddha or Sariputta.

196
00:11:49,040 --> 00:11:50,640
 I think maybe Sariputta actually.

197
00:11:50,640 --> 00:11:53,500
 Anyway, asked to give a talk and so they turned to Tissa

198
00:11:53,500 --> 00:11:55,480
 and said, "Okay, Tissa, you give

199
00:11:55,480 --> 00:11:56,480
 the talk."

200
00:11:56,480 --> 00:11:59,400
 And the people were like, "What's he going to say?"

201
00:11:59,400 --> 00:12:01,600
 He said, "This guy can't teach.

202
00:12:01,600 --> 00:12:05,230
 He's never taught since the day he came to live in this

203
00:12:05,230 --> 00:12:06,160
 forest."

204
00:12:06,160 --> 00:12:10,050
 And then he gets up on the dhamma seat and he gives a long

205
00:12:10,050 --> 00:12:12,000
 and wonderful sermon.

206
00:12:12,000 --> 00:12:15,860
 And everyone's a little bit confused and wondering why half

207
00:12:15,860 --> 00:12:18,280
 the people are kind of upset because

208
00:12:18,280 --> 00:12:19,560
 it was just with Sariputta.

209
00:12:19,560 --> 00:12:22,160
 The Buddha hadn't come yet.

210
00:12:22,160 --> 00:12:25,310
 And half the people were upset because they're like, "Well,

211
00:12:25,310 --> 00:12:27,120
 why is he staying here just quiet

212
00:12:27,120 --> 00:12:31,040
 by himself and not talking to anyone, not teaching?"

213
00:12:31,040 --> 00:12:33,000
 When he can teach like this, why didn't he?

214
00:12:33,000 --> 00:12:34,320
 So half the people were upset.

215
00:12:34,320 --> 00:12:37,290
 And then half of the people were overjoyed that they had

216
00:12:37,290 --> 00:12:39,040
 such a wonderful monk staying

217
00:12:39,040 --> 00:12:40,040
 with them.

218
00:12:40,040 --> 00:12:43,000
 They hadn't realized what a wonderful monk he was.

219
00:12:43,000 --> 00:12:49,400
 People are divided according to their character types.

220
00:12:49,400 --> 00:12:52,570
 And then the Buddha came because the Buddha saw that this

221
00:12:52,570 --> 00:12:54,240
 was going to be a problem and

222
00:12:54,240 --> 00:12:56,560
 so he came to sort it out.

223
00:12:56,560 --> 00:12:59,540
 And he pointed out how great it was that they had such a

224
00:12:59,540 --> 00:13:01,680
 monk as this, someone who was intent

225
00:13:01,680 --> 00:13:08,000
 upon a solitude and as a result of his great qualities and

226
00:13:08,000 --> 00:13:12,600
 his great devotion to the practice.

227
00:13:12,600 --> 00:13:16,560
 He pointed out how all these great monks then came to visit

228
00:13:16,560 --> 00:13:18,640
 him and pointed out to the lay

229
00:13:18,640 --> 00:13:22,600
 people how lucky they were as a result.

230
00:13:22,600 --> 00:13:24,160
 This is the story of Tisa.

231
00:13:24,160 --> 00:13:28,970
 As usual, it's sort of long and there's many aspects to it

232
00:13:28,970 --> 00:13:31,080
 that don't all relate to the

233
00:13:31,080 --> 00:13:32,080
 verse.

234
00:13:32,080 --> 00:13:37,590
 But the verse came about because monks were amazed that T

235
00:13:37,590 --> 00:13:40,080
isa was able to do this.

236
00:13:40,080 --> 00:13:43,890
 For many of them, there wasn't a choice to live in such

237
00:13:43,890 --> 00:13:44,880
 hardship.

238
00:13:44,880 --> 00:13:50,350
 But for Tisa, he had relatives and he had friends and

239
00:13:50,350 --> 00:13:53,700
 people who were devoted to him.

240
00:13:53,700 --> 00:13:56,770
 And so if he had stayed in Savadhi, he could have gotten

241
00:13:56,770 --> 00:13:58,640
 lots of almsfood or blankets or

242
00:13:58,640 --> 00:13:59,640
 whatever he wanted.

243
00:13:59,640 --> 00:14:05,140
 He had these other names for a reason because he was very

244
00:14:05,140 --> 00:14:08,120
 full of great merit and was always

245
00:14:08,120 --> 00:14:12,920
 able to live in luxury and get whatever he wanted all the

246
00:14:12,920 --> 00:14:13,720
 time.

247
00:14:13,720 --> 00:14:15,670
 And they said it's a difficult thing that he's done to go

248
00:14:15,670 --> 00:14:16,720
 off in the forest and live

249
00:14:16,720 --> 00:14:17,720
 in hardship.

250
00:14:17,720 --> 00:14:20,200
 And the Buddha heard them and asked what they were talking

251
00:14:20,200 --> 00:14:20,680
 about.

252
00:14:20,680 --> 00:14:24,200
 And when they told him, he said, "Indeed, he's done a

253
00:14:24,200 --> 00:14:25,720
 difficult thing."

254
00:14:25,720 --> 00:14:29,320
 And then he told this verse and he said, "There are two

255
00:14:29,320 --> 00:14:33,200
 very different paths to choose from,

256
00:14:33,200 --> 00:14:41,590
 the path of sakkara, of gain and fame and affluence and

257
00:14:41,590 --> 00:14:47,400
 pleasure and the path to freedom."

258
00:14:47,400 --> 00:14:52,400
 It's an important point and this is the point for our

259
00:14:52,400 --> 00:14:53,760
 practice.

260
00:14:53,760 --> 00:14:57,390
 It's a claim that we make that happiness doesn't come from

261
00:14:57,390 --> 00:14:59,280
 gain, doesn't come from getting

262
00:14:59,280 --> 00:15:01,840
 what you want.

263
00:15:01,840 --> 00:15:05,660
 It doesn't come from pleasure, it doesn't come from worldly

264
00:15:05,660 --> 00:15:06,240
 pursuits.

265
00:15:06,240 --> 00:15:12,820
 There are two different paths, the path of gain and the

266
00:15:12,820 --> 00:15:15,200
 path to nirvana.

267
00:15:15,200 --> 00:15:20,750
 And one sees this and the word the Buddha uses, abhinaya,

268
00:15:20,750 --> 00:15:31,640
 which abhi is like in the higher.

