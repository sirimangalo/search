1
00:00:00,000 --> 00:00:03,000
 Good evening everyone.

2
00:00:03,000 --> 00:00:12,000
 Welcome to our live broadcast November 8th, 2015.

3
00:00:12,000 --> 00:00:17,000
 Robin, now I'm echoing on your speakers.

4
00:00:17,000 --> 00:00:19,000
 Really? I don't hear it on my end.

5
00:00:19,000 --> 00:00:22,800
 You won't because it's the same way I didn't hear that you

6
00:00:22,800 --> 00:00:26,000
 were echoing through my speakers.

7
00:00:26,000 --> 00:00:27,000
 Okay.

8
00:00:27,000 --> 00:00:31,360
 Not anymore. Oh wait, go ahead and speak. Oh wait, no, it's

9
00:00:31,360 --> 00:00:32,000
 me who speaks.

10
00:00:32,000 --> 00:00:35,000
 Now I don't hear it. Now it's fine.

11
00:00:35,000 --> 00:00:47,000
 Okay, let me know. I can put on a headset if need be.

12
00:00:47,000 --> 00:00:53,000
 So yeah, I was invited to something else at 9pm.

13
00:00:53,000 --> 00:00:59,980
 But I rejected it. It's never nice to refuse an invitation,

14
00:00:59,980 --> 00:01:02,000
 right?

15
00:01:02,000 --> 00:01:07,000
 But it was too similar to what I'm doing here.

16
00:01:07,000 --> 00:01:11,000
 Yeah, I am echoing again.

17
00:01:11,000 --> 00:01:15,000
 On and off.

18
00:01:15,000 --> 00:01:22,000
 So they wanted me to teach meditation and answer questions.

19
00:01:22,000 --> 00:01:25,000
 But I don't know really what the group is.

20
00:01:25,000 --> 00:01:29,000
 I was on this group before once and I did this.

21
00:01:29,000 --> 00:01:34,400
 But to teach meditation again, it's, you know, I don't know

22
00:01:34,400 --> 00:01:37,000
 that it's really worthwhile.

23
00:01:37,000 --> 00:01:42,000
 If they want, they can come here and learn meditation.

24
00:01:42,000 --> 00:01:47,000
 They can read my booklet. It teaches how to meditate.

25
00:01:47,000 --> 00:01:51,140
 We have 28 people signed up, I think, for the meetings. So

26
00:01:51,140 --> 00:01:52,000
 that's great.

27
00:01:52,000 --> 00:01:55,540
 A little bit of problem. If you're one of those people and

28
00:01:55,540 --> 00:01:57,000
 you're, we haven't met yet,

29
00:01:57,000 --> 00:02:01,000
 you have to really get a sense of how it works.

30
00:02:01,000 --> 00:02:05,000
 And you really should visit the hangouts.google.com link.

31
00:02:05,000 --> 00:02:09,000
 Because that's your homepage. That should be your hub.

32
00:02:09,000 --> 00:02:11,530
 You don't really need to use the meeting page after you've

33
00:02:11,530 --> 00:02:12,000
 signed up.

34
00:02:12,000 --> 00:02:17,190
 As long as you get the time right, you just call me on Hang

35
00:02:17,190 --> 00:02:18,000
outs.

36
00:02:18,000 --> 00:02:22,000
 Of course, there's a link for the first meeting.

37
00:02:22,000 --> 00:02:24,600
 There's a link, a button that comes up and you push that

38
00:02:24,600 --> 00:02:29,000
 button and it starts a Hangout for you to get you into it.

39
00:02:29,000 --> 00:02:33,240
 But from there, I'm going to use the actual hangouts.google

40
00:02:33,240 --> 00:02:36,000
.com page to send you stuff.

41
00:02:36,000 --> 00:02:40,490
 So you need to have that open. And you need to know that

42
00:02:40,490 --> 00:02:42,000
 you're calling me. I'm not calling you.

43
00:02:42,000 --> 00:02:47,000
 Because, well, I don't have your contact information.

44
00:02:47,000 --> 00:02:49,890
 First of all, and second of all, I'm not going to go m

45
00:02:49,890 --> 00:02:51,000
ucking around.

46
00:02:51,000 --> 00:02:57,000
 It's up to you to... The mountain doesn't go to Muhammad.

47
00:02:57,000 --> 00:03:00,650
 Muhammad has to go to the mountain. I don't know if that's

48
00:03:00,650 --> 00:03:02,000
 proper, but I can't.

49
00:03:02,000 --> 00:03:09,000
 I don't really have the...

50
00:03:09,000 --> 00:03:12,380
 Well, it's easier for 29 people to contact you than for you

51
00:03:12,380 --> 00:03:14,000
 to contact 29 people.

52
00:03:14,000 --> 00:03:18,000
 That's right. Maybe.

53
00:03:18,000 --> 00:03:21,000
 Yeah, because I have to do it 29 times. 28.

54
00:03:21,000 --> 00:03:28,000
 Yes.

55
00:03:28,000 --> 00:03:32,000
 And so I've been thinking to maybe add another slot, but...

56
00:03:32,000 --> 00:03:36,000
 Or two, or three, or four.

57
00:03:36,000 --> 00:03:40,000
 But that might require quitting university.

58
00:03:40,000 --> 00:03:43,540
 So we'll see how that works. You might just decide that

59
00:03:43,540 --> 00:03:47,000
 this university thing wasn't worth anything after all.

60
00:03:47,000 --> 00:03:52,000
 I mean, I already know it's not worth that much.

61
00:03:52,000 --> 00:03:56,490
 I mean, I didn't expect this really to be set up and doing

62
00:03:56,490 --> 00:03:58,000
 so much teaching.

63
00:03:58,000 --> 00:04:04,000
 So I have to change based on the situation. Is that useful?

64
00:04:04,000 --> 00:04:07,000
 No, that's not useful.

65
00:04:07,000 --> 00:04:16,000
 I should have the light over here. I'll do that later.

66
00:04:16,000 --> 00:04:19,000
 So do we have any questions?

67
00:04:19,000 --> 00:04:23,350
 We do. First question. Do you have Pulse Audio, the loop

68
00:04:23,350 --> 00:04:27,000
back devices, and Dark Ice turned on?

69
00:04:27,000 --> 00:04:29,000
 Yes.

70
00:04:29,000 --> 00:04:32,000
 Okay. Very good. Then we're ready for real questions.

71
00:04:32,000 --> 00:04:35,320
 Bante, we have a group of Tibetan monks coming to our

72
00:04:35,320 --> 00:04:38,060
 college this coming week, and their schedule involves

73
00:04:38,060 --> 00:04:39,000
 building a sand mandala.

74
00:04:39,000 --> 00:04:42,610
 Is that a sort of tradition for monks, or does it have any

75
00:04:42,610 --> 00:04:45,000
 spiritual value? Thank you.

76
00:04:45,000 --> 00:04:48,000
 It's not working. It's not going to work.

77
00:04:48,000 --> 00:04:51,000
 What's not going to work?

78
00:04:51,000 --> 00:04:53,000
 That's what I need. Go ahead and speak.

79
00:04:53,000 --> 00:04:57,000
 Test. Test.

80
00:04:57,000 --> 00:05:01,000
 Test. Test.

81
00:05:01,000 --> 00:05:04,000
 Hmm.

82
00:05:04,000 --> 00:05:12,000
 Loopback to no output from...

83
00:05:12,000 --> 00:05:17,000
 Loopback of monitor.

84
00:05:17,000 --> 00:05:21,000
 All right. I forget. I don't know. Can you speak again?

85
00:05:21,000 --> 00:05:25,000
 Yes. Can you hear me?

86
00:05:25,000 --> 00:05:32,000
 Yeah, but why isn't that doing it?

87
00:05:32,000 --> 00:05:38,000
 Test. Test. Test.

88
00:05:38,000 --> 00:05:42,350
 Yeah, I think it's working. Maybe, maybe not. Go ahead. Can

89
00:05:42,350 --> 00:05:44,000
 you say again?

90
00:05:44,000 --> 00:05:47,000
 Go for a question.

91
00:05:47,000 --> 00:05:50,000
 Oh, the question. Okay.

92
00:05:50,000 --> 00:05:53,170
 Question is, Bante, we have a group of Tibetan monks coming

93
00:05:53,170 --> 00:05:56,090
 to our college this coming week, and their schedule

94
00:05:56,090 --> 00:05:58,000
 involves building a sand mandala.

95
00:05:58,000 --> 00:06:00,850
 Is that a sort of tradition for monks? Does it have any

96
00:06:00,850 --> 00:06:04,000
 spiritual value? Thank you.

97
00:06:04,000 --> 00:06:06,750
 Yeah, they're Tibetan monks. I'm not a Tibetan monk, so I'm

98
00:06:06,750 --> 00:06:13,000
 not really well-equipped to answer that question. Sorry.

99
00:06:13,000 --> 00:06:17,240
 Bante, I have a question concerning right livelihood. What

100
00:06:17,240 --> 00:06:20,240
 are your thoughts about working in the games industry and

101
00:06:20,240 --> 00:06:23,740
 potentially making games or simulations that contain

102
00:06:23,740 --> 00:06:25,000
 violent aspects?

103
00:06:25,000 --> 00:06:29,040
 I'm not so concerned about the violent aspects. I

104
00:06:29,040 --> 00:06:31,550
 understand that concern, but it's not, I think, as

105
00:06:31,550 --> 00:06:34,000
 concerning as people make it out to be.

106
00:06:34,000 --> 00:06:39,840
 I think there is a concern there, sure, but the bigger

107
00:06:39,840 --> 00:06:43,000
 concern is the entertainment.

108
00:06:43,000 --> 00:06:47,140
 Maybe not the bigger concern. I don't know. But personally,

109
00:06:47,140 --> 00:06:50,960
 to me, it's more about the entertainment, doing something

110
00:06:50,960 --> 00:06:54,000
 that's not of any value to people, not of any use to them.

111
00:06:54,000 --> 00:07:04,100
 And in fact, it's helping people to waste time and to

112
00:07:04,100 --> 00:07:07,000
 become negligent.

113
00:07:07,000 --> 00:07:11,150
 So anything to do with entertainment, I think you can look

114
00:07:11,150 --> 00:07:13,000
 at the eight precepts.

115
00:07:13,000 --> 00:07:15,610
 So breaking the five precepts, if livelihood involves

116
00:07:15,610 --> 00:07:18,000
 breaking the five precepts, that's really bad.

117
00:07:18,000 --> 00:07:26,120
 But if it involves breaking the eight precepts, then it's

118
00:07:26,120 --> 00:07:35,770
 problematic because you're involved in activities that

119
00:07:35,770 --> 00:07:46,000
 encourage other people to engage in activities that hinder

120
00:07:46,000 --> 00:07:47,000
 them on the path,

121
00:07:47,000 --> 00:07:52,790
 obstruct them on the path. So that isn't the case with

122
00:07:52,790 --> 00:07:59,450
 livelihood like selling food or necessities, things that

123
00:07:59,450 --> 00:08:03,540
 people need, so many things that you can offer or you can

124
00:08:03,540 --> 00:08:08,640
 provide that actually are either neutral or actually are

125
00:08:08,640 --> 00:08:14,000
 conducive to spiritual benefit.

126
00:08:14,000 --> 00:08:19,550
 So it's definitely a problem. It's definitely not without

127
00:08:19,550 --> 00:08:29,260
 its problems. It's not wrong, wrong, but it's maybe a

128
00:08:29,260 --> 00:08:32,000
 little wrong.

129
00:08:32,000 --> 00:08:35,570
 As to the violent aspect, I mean, you're not actually

130
00:08:35,570 --> 00:08:40,140
 killing anything. And I wouldn't say you're necessarily,

131
00:08:40,140 --> 00:08:42,940
 when I used to, when I was young, we played Doom and Wolf

132
00:08:42,940 --> 00:08:44,000
enstein.

133
00:08:44,000 --> 00:08:51,320
 We played Wolfenstein before it was 3D and Warcraft,

134
00:08:51,320 --> 00:08:54,640
 Starcraft. A lot of killing, no? A lot of games with

135
00:08:54,640 --> 00:09:00,000
 killing. Duke Nukem, he played.

136
00:09:00,000 --> 00:09:03,950
 The killing aspect is not really, it's more like tag,

137
00:09:03,950 --> 00:09:08,400
 really, or yeah, it's more like tag than anything. When I

138
00:09:08,400 --> 00:09:11,000
 was younger, we played Paintball.

139
00:09:11,000 --> 00:09:14,390
 We actually went out into the, we all had paint guns and we

140
00:09:14,390 --> 00:09:17,610
 went out into the forest and we spent more time trying to

141
00:09:17,610 --> 00:09:21,000
 find each other than actually shooting each other.

142
00:09:21,000 --> 00:09:23,160
 But shooting each other, no one was, there was no sense

143
00:09:23,160 --> 00:09:26,190
 that you were actually hurting each other. It was a

144
00:09:26,190 --> 00:09:29,000
 competition.

145
00:09:29,000 --> 00:09:33,680
 So really that aspect, I think, isn't nearly as problematic

146
00:09:33,680 --> 00:09:37,310
. I mean, if someone is, they're the problem, I suppose, is

147
00:09:37,310 --> 00:09:43,320
 if someone is psychopathic or sociopathic, then they'll

148
00:09:43,320 --> 00:09:44,000
 take it in a whole other way.

149
00:09:44,000 --> 00:09:50,600
 And for them, it's completely, it's not tag anymore. It's

150
00:09:50,600 --> 00:09:56,630
 actually a means of being cruel or enacting fantasies of

151
00:09:56,630 --> 00:09:59,000
 cruelty and evil.

152
00:09:59,000 --> 00:10:05,230
 So there is that, there is a problem there. But for most

153
00:10:05,230 --> 00:10:11,280
 people, I don't think it's really all that it's made out to

154
00:10:11,280 --> 00:10:12,000
 be.

155
00:10:12,000 --> 00:10:17,460
 I spent a lot of time playing video games online and greed

156
00:10:17,460 --> 00:10:21,000
 and delusion are rampant.

157
00:10:21,000 --> 00:10:24,620
 Online, I imagine it's even worse because you've got the

158
00:10:24,620 --> 00:10:26,000
 internet community.

159
00:10:26,000 --> 00:10:29,710
 I heard something about, there's a lot of awful, awful

160
00:10:29,710 --> 00:10:32,660
 stuff that goes on in the chats and they actually talk to

161
00:10:32,660 --> 00:10:35,000
 each other, right? You've got headsets.

162
00:10:35,000 --> 00:10:40,000
 Of course. We talk on Mumble. We do raids on Mumble.

163
00:10:40,000 --> 00:10:43,150
 And so there's a lot of people calling each other names and

164
00:10:43,150 --> 00:10:46,980
 insulting each other's mothers, apparently. It's a big

165
00:10:46,980 --> 00:10:48,000
 thing.

166
00:10:48,000 --> 00:10:50,000
 Am I right?

167
00:10:50,000 --> 00:10:53,100
 Well, I mean, there's a lot of trolling, sure. But there's

168
00:10:53,100 --> 00:10:55,000
 text chat as well.

169
00:10:55,000 --> 00:10:59,360
 So trade chat, they call it. That's where there's a lot of

170
00:10:59,360 --> 00:11:02,000
 trolling, which is a chat for the whole realm.

171
00:11:02,000 --> 00:11:06,940
 But there's a lot of greed and a lot of delusion. If you're

172
00:11:06,940 --> 00:11:11,770
 trying to meditate and control that kind of thing, online

173
00:11:11,770 --> 00:11:14,000
 games are probably the wrong direction.

174
00:11:14,000 --> 00:11:17,000
 At least that type.

175
00:11:27,000 --> 00:11:31,220
 How to accept people who treat you with no respect. I'm

176
00:11:31,220 --> 00:11:34,000
 finding it hard to let go of my feelings.

177
00:11:34,000 --> 00:11:41,000
 Yeah, well, a lot of meditation. Have you read my booklet?

178
00:11:41,000 --> 00:11:46,000
 I recommend reading my booklet, starting to practice.

179
00:11:46,000 --> 00:11:48,590
 It should help you let go. I mean, letting go isn't

180
00:11:48,590 --> 00:11:53,930
 something you can do, something that comes naturally when

181
00:11:53,930 --> 00:11:56,000
 you see clearly.

182
00:11:56,000 --> 00:12:03,240
 And then a follow up question on the Right Livelihood about

183
00:12:03,240 --> 00:12:06,000
 the game industry.

184
00:12:06,000 --> 00:12:10,180
 Would the response differ if the games were nonviolent? In

185
00:12:10,180 --> 00:12:15,000
 other words, encouraging collaboration and cooperation?

186
00:12:15,000 --> 00:12:20,340
 Yeah, I mean, maybe a little bit. But again, it depends on

187
00:12:20,340 --> 00:12:25,520
 whether they're actually helping you cultivate wholesome

188
00:12:25,520 --> 00:12:27,000
 mind states.

189
00:12:27,000 --> 00:12:31,400
 Again, it's fake, right? So you're not actually doing

190
00:12:31,400 --> 00:12:35,000
 anything for anybody, not most of the time.

191
00:12:35,000 --> 00:12:40,790
 So what's the point of it is to have fun or to spend time

192
00:12:40,790 --> 00:12:46,000
 with each other? Then it's probably not so problematic.

193
00:12:46,000 --> 00:12:49,000
 It's still probably a problem with the eight presets.

194
00:12:49,000 --> 00:12:51,670
 Isn't that what Second Life is like, though? I've never

195
00:12:51,670 --> 00:12:54,000
 actually signed into Second Life.

196
00:12:54,000 --> 00:12:58,240
 It's a platform. It's not a game. There's no objective. It

197
00:12:58,240 --> 00:13:00,000
's a world. It's virtual reality.

198
00:13:00,000 --> 00:13:05,050
 It's what we all expected to be already happening. We

199
00:13:05,050 --> 00:13:06,000
 expected everyone to be on Second Life.

200
00:13:06,000 --> 00:13:09,020
 But it's never really taken off the way it was expected to.

201
00:13:09,020 --> 00:13:10,000
 Not yet, anyway.

202
00:13:10,000 --> 00:13:12,970
 Now with maybe all this new virtual reality stuff, it'll

203
00:13:12,970 --> 00:13:14,000
 actually happen.

204
00:13:14,000 --> 00:13:19,690
 Second Life is a world. You buy land, you pay real money to

205
00:13:19,690 --> 00:13:22,000
 buy land and set up a house.

206
00:13:22,000 --> 00:13:28,820
 There's a lot of dating and stuff. People walk around half

207
00:13:28,820 --> 00:13:30,000
 naked.

208
00:13:30,000 --> 00:13:33,000
 You pay real money or some kind of a game gold?

209
00:13:33,000 --> 00:13:36,000
 It's game money, but you buy it. You have to buy it.

210
00:13:36,000 --> 00:13:39,000
 With real money? Oh.

211
00:13:39,000 --> 00:13:41,460
 Yeah, there's no real way to make it. You can give each

212
00:13:41,460 --> 00:13:42,000
 other.

213
00:13:42,000 --> 00:13:48,030
 People gave me like $4,000. I have like $4,000 Linden

214
00:13:48,030 --> 00:13:52,000
 Dollars, which I think is like $40 US or something.

215
00:13:52,000 --> 00:13:54,000
 That's awesome. Virtual, Donna.

216
00:13:54,000 --> 00:14:01,000
 Yeah, well, I said, "Please stop because I can't use it."

217
00:14:01,000 --> 00:14:03,000
 That's funny.

218
00:14:03,000 --> 00:14:06,000
 I just see them.

219
00:14:06,000 --> 00:14:15,310
 I'm six foot seven, and when I sit cross-legged, my foot

220
00:14:15,310 --> 00:14:17,000
 ends up hurting.

221
00:14:17,000 --> 00:14:20,790
 As the position I'm in, my right foot is under my left

222
00:14:20,790 --> 00:14:22,000
 crossed leg.

223
00:14:22,000 --> 00:14:31,580
 What position should my left foot be in so I can stop this

224
00:14:31,580 --> 00:14:35,000
 from happening?

225
00:14:35,000 --> 00:14:38,000
 Yeah, I tried to just put one leg in front of the other

226
00:14:38,000 --> 00:14:43,000
 with the heel touching the shin of the back leg.

227
00:14:43,000 --> 00:14:45,260
 But it's still probably not going to be easy in the

228
00:14:45,260 --> 00:14:48,000
 beginning. It just takes time to get used to it.

229
00:14:48,000 --> 00:14:52,000
 You can start with cushions under your knees and your legs.

230
00:14:52,000 --> 00:14:56,000
 You can start sitting with your butt a little bit higher.

231
00:14:56,000 --> 00:14:58,740
 It's just a matter of time. It's like anything. It takes

232
00:14:58,740 --> 00:15:00,000
 training.

233
00:15:00,000 --> 00:15:03,930
 Is that what's called the Burmese position, or is that

234
00:15:03,930 --> 00:15:05,000
 different?

235
00:15:05,000 --> 00:15:11,000
 Yeah, I think that's what it's called.

236
00:15:11,000 --> 00:15:14,000
 I'm not sure why, but...

237
00:15:14,000 --> 00:15:27,000
 Someone in Burma must have been the first.

238
00:15:27,000 --> 00:15:31,340
 Apparently, you can't hear me on the feed, but as long as

239
00:15:31,340 --> 00:15:33,000
 they can hear you.

240
00:15:33,000 --> 00:15:36,000
 George said, "Can't hear me on the feed."

241
00:15:36,000 --> 00:15:39,000
 As long as they can hear the answer, I guess that's the...

242
00:15:39,000 --> 00:15:42,690
 That must have been... It's a bit that's outdated, because

243
00:15:42,690 --> 00:15:44,000
 I'm seeing it pick you up.

244
00:15:44,000 --> 00:15:47,000
 Oh, okay. That was seven minutes ago.

245
00:15:47,000 --> 00:15:50,770
 Bante, I tend to let my words slip and curse somewhat often

246
00:15:50,770 --> 00:15:53,440
, and from what I understand, this is not in line with right

247
00:15:53,440 --> 00:15:54,000
 speech.

248
00:15:54,000 --> 00:15:57,320
 Is it just a matter of a case of practice and being more

249
00:15:57,320 --> 00:16:01,000
 mindful of when I speak, or is it not so much of a problem?

250
00:16:01,000 --> 00:16:09,000
 I feel like it's not good to do.

251
00:16:09,000 --> 00:16:15,000
 Well, cursing isn't a big deal.

252
00:16:15,000 --> 00:16:19,730
 It's not really wrong speech, and not exactly, but it's

253
00:16:19,730 --> 00:16:24,000
 usually associated with negative mind states.

254
00:16:24,000 --> 00:16:26,000
 Now, worse speech is if you call someone names.

255
00:16:26,000 --> 00:16:31,080
 Harsh speech is like calling someone a dog, or a pig, or a

256
00:16:31,080 --> 00:16:33,000
 monkey, or a goat,

257
00:16:33,000 --> 00:16:37,980
 or calling people nasty names, hurting other people with

258
00:16:37,980 --> 00:16:39,000
 speech.

259
00:16:39,000 --> 00:16:44,390
 If you just say, "Damn it," or like this Israeli monk who

260
00:16:44,390 --> 00:16:48,000
 kept saying bullshit all the time.

261
00:16:48,000 --> 00:16:50,890
 I tried to explain to him, that's probably not the best

262
00:16:50,890 --> 00:16:53,000
 thing to say around Western people,

263
00:16:53,000 --> 00:16:56,220
 about American people, because we... I don't know how it is

264
00:16:56,220 --> 00:16:58,000
 in Israel, but that word means...

265
00:16:58,000 --> 00:17:01,000
 It's a little bit coarse.

266
00:17:01,000 --> 00:17:04,000
 But that's not considered harsh speech.

267
00:17:04,000 --> 00:17:08,570
 It's not harsh speech, as if someone says something and you

268
00:17:08,570 --> 00:17:09,000
 say, "Oh, you're speaking...

269
00:17:09,000 --> 00:17:16,030
 All you ever speak is bullshit," or something. That would

270
00:17:16,030 --> 00:17:25,000
 be harsh speech.

271
00:17:25,000 --> 00:17:33,000
 I'll cut up on questions. Are there any announcements?

272
00:17:33,000 --> 00:17:38,000
 No. Oh, we have this... No, I've already mentioned it.

273
00:17:38,000 --> 00:17:43,000
 Not for me. Tomorrow, another Dhammapada.

274
00:17:43,000 --> 00:17:47,000
 Tonight, that's all for now.

275
00:17:48,000 --> 00:17:50,000
 Okay.

276
00:17:51,000 --> 00:18:16,000
 [Silence]

277
00:18:16,000 --> 00:18:20,610
 Well, hopefully that worked with the audio. I don't really

278
00:18:20,610 --> 00:18:21,000
 know.

279
00:18:21,000 --> 00:18:23,340
 But the problem is that I'm using these, which are

280
00:18:23,340 --> 00:18:26,000
 Bluetooth, and it's all nice stuff.

281
00:18:26,000 --> 00:18:29,610
 So hopefully that worked. Anyway, thank you all for coming

282
00:18:29,610 --> 00:18:30,000
 out.

283
00:18:30,000 --> 00:18:32,000
 Thanks, Robin, for your help.

284
00:18:32,000 --> 00:18:35,000
 Thank you, Bante. Good night.

285
00:18:35,000 --> 00:18:37,000
 Good night.

286
00:18:38,000 --> 00:18:44,000
 [Silence]

