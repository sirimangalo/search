1
00:00:00,000 --> 00:00:03,800
 Can walking meditation be as effective as possible as

2
00:00:03,800 --> 00:00:08,510
 sitting meditation? Of course. Meditation is not walking or

3
00:00:08,510 --> 00:00:10,560
 sitting meditation is the state of mind.

4
00:00:10,560 --> 00:00:14,060
 Sitting meditation has the added benefit of

5
00:00:14,060 --> 00:00:17,400
 having less

6
00:00:17,400 --> 00:00:20,320
 objects, fewer objects to focus on.

7
00:00:20,320 --> 00:00:24,960
 But walking meditation or meditation in any position can be

8
00:00:24,960 --> 00:00:26,440
 equally effective.

9
00:00:26,720 --> 00:00:31,080
 The Buddha said whatever "yata yata wa pani hitang"

10
00:00:31,080 --> 00:00:36,910
 Something like that. However the mind is, however posture

11
00:00:36,910 --> 00:00:38,620
 you put the mind in,

12
00:00:38,620 --> 00:00:43,940
 be mindful of that. So it doesn't matter what posture or

13
00:00:43,940 --> 00:00:47,560
 what movement or what experience comes about.

14
00:00:47,560 --> 00:00:50,600
 It can be effective. Now walking meditation,

15
00:00:52,160 --> 00:00:55,690
 sitting meditation structurally is the most beneficial

16
00:00:55,690 --> 00:00:58,640
 because there's very few objects to begin with.

