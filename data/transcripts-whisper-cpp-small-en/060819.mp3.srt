1
00:00:00,000 --> 00:00:16,230
 Today we continue talking about the 38 Blessings of the

2
00:00:16,230 --> 00:00:20,360
 Lord Buddha.

3
00:00:20,360 --> 00:00:28,280
 We'll finish talking about 6 and then we'll go on to 7 to

4
00:00:28,280 --> 00:00:29,840
 10.

5
00:00:29,840 --> 00:00:50,710
 The next stanza goes, "Bahu satchan chasipan chawinayo chas

6
00:00:50,710 --> 00:00:52,240
usikit o, tupasita chayawacha nyutama kalamata."

7
00:00:52,240 --> 00:01:03,480
 "Bahu satcha" who have great knowledge, who have studied a

8
00:01:03,480 --> 00:01:04,920
 lot.

9
00:01:04,920 --> 00:01:10,520
 Sipan chasipan chawinayo chasusikit o, tupasita chayawacha

10
00:01:10,520 --> 00:01:10,520
 nyutama kalamata.

11
00:01:10,520 --> 00:01:21,040
 To be proficient in some sort of handicraft or in skill.

12
00:01:21,040 --> 00:01:26,570
 To be proficient in the practice or in the performance of

13
00:01:26,570 --> 00:01:29,000
 what one has learned.

14
00:01:29,000 --> 00:01:40,170
 "Bhuinayo chasusikit o," to have a discipline which is well

15
00:01:40,170 --> 00:01:42,000
 trained, to be well trained

16
00:01:42,000 --> 00:01:44,000
 in discipline.

17
00:01:44,000 --> 00:01:54,360
 "Tupasita chayawacha" Whatever speech is well spoken.

18
00:01:54,360 --> 00:02:01,730
 "Ithamaan kalamuthamaan," these are blessings in the

19
00:02:01,730 --> 00:02:04,200
 highest sense.

20
00:02:04,200 --> 00:02:22,080
 "Bahu satcha" to have great knowledge and learning.

21
00:02:22,080 --> 00:02:27,350
 Knowledge and learning, on a basic level it means having

22
00:02:27,350 --> 00:02:30,600
 studied, having either listened

23
00:02:30,600 --> 00:02:34,380
 to many talks on the Dhamma or had many teachers teaching

24
00:02:34,380 --> 00:02:36,800
 about the Dhamma, teaching about

25
00:02:36,800 --> 00:02:40,360
 the truth.

26
00:02:40,360 --> 00:02:43,450
 Of course it could mean even in a worldly sense, it could

27
00:02:43,450 --> 00:02:45,280
 mean nothing to do with the teaching

28
00:02:45,280 --> 00:02:50,280
 of the Lord Buddha.

29
00:02:50,280 --> 00:02:54,640
 Here we understand it to mean the teaching of the Lord

30
00:02:54,640 --> 00:02:57,520
 Buddha but actually it could even

31
00:02:57,520 --> 00:03:04,690
 be considered a blessing just to have learned a lot in a

32
00:03:04,690 --> 00:03:07,120
 worldly sense.

33
00:03:07,120 --> 00:03:11,760
 Knowledge is power.

34
00:03:11,760 --> 00:03:25,210
 Whatever realm we work in, we work in business or we work

35
00:03:25,210 --> 00:03:37,560
 in education, we work in any field

36
00:03:37,560 --> 00:03:45,930
 in the world, we find that the more we have studied what we

37
00:03:45,930 --> 00:03:50,160
 are doing, the greater will

38
00:03:50,160 --> 00:03:55,960
 be our power and our ability to carry out our work.

39
00:03:55,960 --> 00:03:58,640
 In the Dhamma it's the same.

40
00:03:58,640 --> 00:04:02,410
 Of course even much more of a blessing because the Dhamma

41
00:04:02,410 --> 00:04:04,900
 is that which keeps us from falling

42
00:04:04,900 --> 00:04:09,240
 into evil, falling into suffering.

43
00:04:09,240 --> 00:04:13,380
 When we have great knowledge or learning about the Dhamma,

44
00:04:13,380 --> 00:04:15,580
 for sure we can much better put

45
00:04:15,580 --> 00:04:21,480
 it into practice.

46
00:04:21,480 --> 00:04:25,890
 If we learn all 38 of the blessings of the Lord Buddha, we

47
00:04:25,890 --> 00:04:27,720
'll be much better off to put

48
00:04:27,720 --> 00:04:31,280
 them all into practice.

49
00:04:31,280 --> 00:04:34,160
 Rather than we know one or two, we only know the very

50
00:04:34,160 --> 00:04:38,520
 basics of the Lord Buddha's teaching.

51
00:04:38,520 --> 00:04:41,440
 We learn a lot about the Buddha's teaching.

52
00:04:41,440 --> 00:04:46,250
 It would be much easier for us to put it all into practice

53
00:04:46,250 --> 00:04:48,720
 because we understand it on

54
00:04:48,720 --> 00:04:49,720
 a more broad level.

55
00:04:49,720 --> 00:04:54,560
 It's a great blessing to have studied.

56
00:04:54,560 --> 00:05:06,440
 With study you can also apply it according to the teachers,

57
00:05:06,440 --> 00:05:07,920
 according to the teachings

58
00:05:07,920 --> 00:05:12,640
 on great knowledge, great learning.

59
00:05:12,640 --> 00:05:19,110
 It can also apply to practice and it can also apply to the

60
00:05:19,110 --> 00:05:23,320
 results one gets from practice.

61
00:05:23,320 --> 00:05:28,850
 Here we can find it only through book learning or through

62
00:05:28,850 --> 00:05:32,000
 listening to what other people

63
00:05:32,000 --> 00:05:33,000
 have to say.

64
00:05:33,000 --> 00:05:39,060
 But really it can also mean because the knowledge which we

65
00:05:39,060 --> 00:05:42,560
 gain from books and from hearing

66
00:05:42,560 --> 00:05:46,420
 is really only very superficial knowledge.

67
00:05:46,420 --> 00:05:51,330
 It doesn't constitute real understanding or real learning

68
00:05:51,330 --> 00:05:52,680
 of a subject.

69
00:05:52,680 --> 00:05:58,840
 So we put ourselves to the task.

70
00:05:58,840 --> 00:06:05,560
 It really can't be expected to bring us any great fruit.

71
00:06:05,560 --> 00:06:11,300
 So we'll combine it to this because the next one, sipang

72
00:06:11,300 --> 00:06:14,200
 means handicraft or the ability

73
00:06:14,200 --> 00:06:17,440
 to do the work.

74
00:06:17,440 --> 00:06:23,440
 This applies more to the realm of practice.

75
00:06:23,440 --> 00:06:34,050
 We need to have the ability to put into practice whatever

76
00:06:34,050 --> 00:06:37,480
 we've learned.

77
00:06:37,480 --> 00:06:41,650
 In the time of the Lord Buddha there were many stories of

78
00:06:41,650 --> 00:06:43,760
 people who had studied but

79
00:06:43,760 --> 00:06:49,000
 had never put into practice what they had learned.

80
00:06:49,000 --> 00:06:52,280
 One story is the story of empty book.

81
00:06:52,280 --> 00:06:56,640
 The Lord Buddha called him empty book.

82
00:06:56,640 --> 00:07:02,830
 His name was Poteela which meant the form of a book in

83
00:07:02,830 --> 00:07:06,600
 those times or a palm leaf, a

84
00:07:06,600 --> 00:07:15,080
 manuscript, something used to write on.

85
00:07:15,080 --> 00:07:17,840
 He had studied the whole of the Buddha's teaching, memor

86
00:07:17,840 --> 00:07:19,520
ized the entire teaching of the Lord

87
00:07:19,520 --> 00:07:24,520
 Buddha.

88
00:07:24,520 --> 00:07:29,030
 When he went to see the Lord Buddha he thought that of

89
00:07:29,030 --> 00:07:32,280
 course the Lord Buddha would praise

90
00:07:32,280 --> 00:07:35,560
 him for his attainment.

91
00:07:35,560 --> 00:07:42,340
 He had 500 students or 1000 students and he thought that

92
00:07:42,340 --> 00:07:45,240
 the Lord Buddha would for sure

93
00:07:45,240 --> 00:07:47,360
 have something good to say about him.

94
00:07:47,360 --> 00:07:49,500
 So he went to see the Lord Buddha, pay respect to the Lord

95
00:07:49,500 --> 00:07:50,720
 Buddha and the Lord Buddha saw

96
00:07:50,720 --> 00:07:56,480
 him coming and said, "Oh here comes a good old empty book.

97
00:07:56,480 --> 00:08:00,920
 Oh there goes empty book.

98
00:08:00,920 --> 00:08:05,560
 Oh empty book sitting down.

99
00:08:05,560 --> 00:08:06,560
 Empty book is getting up."

100
00:08:06,560 --> 00:08:14,320
 They called him by the name empty book and Poteela was very

101
00:08:14,320 --> 00:08:17,120
 ashamed to hear this from

102
00:08:17,120 --> 00:08:23,280
 the Lord Buddha.

103
00:08:23,280 --> 00:08:27,570
 He realized that it was because he had studied but never

104
00:08:27,570 --> 00:08:28,760
 practiced.

105
00:08:28,760 --> 00:08:32,520
 That the study which he had gained was really nothing.

106
00:08:32,520 --> 00:08:35,960
 It was not something that really meant anything.

107
00:08:35,960 --> 00:08:43,840
 It was still an empty book.

108
00:08:43,840 --> 00:08:47,530
 So he went off and tried to find a teacher to teach him but

109
00:08:47,530 --> 00:08:49,480
 no one would teach him until

110
00:08:49,480 --> 00:08:54,810
 finally he found a novice, a seven year old novice who

111
00:08:54,810 --> 00:08:57,800
 taught him meditation.

112
00:08:57,800 --> 00:09:01,720
 Because no one else would dare to teach him.

113
00:09:01,720 --> 00:09:06,560
 It would be quite difficult to teach because he knew

114
00:09:06,560 --> 00:09:08,920
 everything already.

115
00:09:08,920 --> 00:09:14,440
 People who know everything are very difficult to teach.

116
00:09:14,440 --> 00:09:22,260
 The novice had a way of breaking his attachment to his

117
00:09:22,260 --> 00:09:27,720
 learning and in the end he did attain

118
00:09:27,720 --> 00:09:37,800
 to become an arahant through the practice of meditation.

119
00:09:37,800 --> 00:09:43,910
 The Sipancha means the ability to practice, the skill in

120
00:09:43,910 --> 00:09:45,440
 practice.

121
00:09:45,440 --> 00:09:57,040
 In the world it means being skilled in a trade, skilled in

122
00:09:57,040 --> 00:10:05,800
 a handicraft, skilled in whatever

123
00:10:05,800 --> 00:10:12,640
 field one applies oneself to.

124
00:10:12,640 --> 00:10:18,120
 This is a great blessing in the world.

125
00:10:18,120 --> 00:10:24,010
 Something which is highly prized, someone who is skilled,

126
00:10:24,010 --> 00:10:26,520
 someone who has a skill in

127
00:10:26,520 --> 00:10:27,520
 what they do.

128
00:10:27,520 --> 00:10:31,360
 It's a blessing, it's something you can take anywhere with

129
00:10:31,360 --> 00:10:31,840
 us.

130
00:10:31,840 --> 00:10:34,980
 If you have a business, you can't take the business with

131
00:10:34,980 --> 00:10:36,480
 you but if you have a skill

132
00:10:36,480 --> 00:10:38,120
 you can take it anywhere.

133
00:10:38,120 --> 00:10:41,660
 Whatever skills that we have we can take them anywhere with

134
00:10:41,660 --> 00:10:42,080
 us.

135
00:10:42,080 --> 00:10:44,940
 But this is a great blessing, something we should all

136
00:10:44,940 --> 00:10:45,400
 strive for.

137
00:10:45,400 --> 00:10:49,110
 If we live in the world we should strive to have some skill

138
00:10:49,110 --> 00:10:50,960
 which is well trained.

139
00:10:50,960 --> 00:10:54,480
 It can be a blessing.

140
00:10:54,480 --> 00:10:59,030
 In the meditation practice, in the Buddhist teaching of

141
00:10:59,030 --> 00:11:01,480
 course, it means the skill in

142
00:11:01,480 --> 00:11:02,480
 meditation practice.

143
00:11:02,480 --> 00:11:07,690
 We should have skill in attaining to the states of calm

144
00:11:07,690 --> 00:11:11,960
 which are conducive for insight meditation

145
00:11:11,960 --> 00:11:15,280
 insight realization.

146
00:11:15,280 --> 00:11:19,930
 If we really want to be skillful we should be skillful at

147
00:11:19,930 --> 00:11:22,720
 attaining the magical powers,

148
00:11:22,720 --> 00:11:29,960
 reading people's minds, remembering past lives, lying

149
00:11:29,960 --> 00:11:32,800
 through the air, shape changing and

150
00:11:32,800 --> 00:11:37,440
 so on.

151
00:11:37,440 --> 00:11:41,140
 The divine eye, the divine ear, all these see things far

152
00:11:41,140 --> 00:11:42,880
 away, hear things far away

153
00:11:42,880 --> 00:11:46,560
 and so on.

154
00:11:46,560 --> 00:11:49,980
 But whether we attain to those magical powers, magical

155
00:11:49,980 --> 00:11:52,720
 states or not, most important is we

156
00:11:52,720 --> 00:11:57,280
 become skillful in seeing the three characteristics.

157
00:11:57,280 --> 00:12:00,280
 Become skillful in watching the rising and falling,

158
00:12:00,280 --> 00:12:02,480
 watching the feelings, watching the

159
00:12:02,480 --> 00:12:07,320
 mind, watching the mirize and seeing that all the things

160
00:12:07,320 --> 00:12:09,280
 around us which used to make

161
00:12:09,280 --> 00:12:14,900
 us so upset, they're really only body and mind, they're

162
00:12:14,900 --> 00:12:18,760
 impermanent, they're unsatisfying

163
00:12:18,760 --> 00:12:24,200
 and they're not under our control and to let them go.

164
00:12:24,200 --> 00:12:28,370
 Gain skill in attaining to the complete freedom from

165
00:12:28,370 --> 00:12:29,600
 suffering.

166
00:12:29,600 --> 00:12:32,750
 Skill in attaining what we call peaceful cessation, where

167
00:12:32,750 --> 00:12:34,520
 the mind lets go, where the mind is

168
00:12:34,520 --> 00:12:42,320
 free, where the mind doesn't fall into suffering.

169
00:12:42,320 --> 00:12:51,520
 So skill in, talking about skill in attaining, skill in

170
00:12:51,520 --> 00:12:57,000
 practice, being able to put into

171
00:12:57,000 --> 00:13:04,840
 practice what we've learned.

172
00:13:04,840 --> 00:13:19,000
 Number nine is a well-trained discipline.

173
00:13:19,000 --> 00:13:22,080
 Discipline is, as I always say, the most important

174
00:13:22,080 --> 00:13:23,920
 foundation of the practice.

175
00:13:23,920 --> 00:13:31,350
 It's a great blessing to be skilled, to be well-trained in

176
00:13:31,350 --> 00:13:33,440
 discipline.

177
00:13:33,440 --> 00:13:36,640
 As you even see it in the world among people who haven't

178
00:13:36,640 --> 00:13:38,680
 practiced Buddhism, they have

179
00:13:38,680 --> 00:13:44,760
 a rudimentary sort of discipline.

180
00:13:44,760 --> 00:13:48,280
 You see it among soldiers.

181
00:13:48,280 --> 00:13:55,360
 I even saw it once among an ordinary person in the world.

182
00:13:55,360 --> 00:14:02,100
 There was a woman who was from Cambodia and she was

183
00:14:02,100 --> 00:14:08,160
 Buddhist, so you can describe it somewhat

184
00:14:08,160 --> 00:14:13,740
 to her Buddhist background, but I'd never seen anyone like

185
00:14:13,740 --> 00:14:14,480
 her.

186
00:14:14,480 --> 00:14:16,970
 She had a teacher and she brought her teacher who was a

187
00:14:16,970 --> 00:14:18,560
 Westerner to come to see us in the

188
00:14:18,560 --> 00:14:21,240
 monastery.

189
00:14:21,240 --> 00:14:23,040
 And her teacher was not very polite.

190
00:14:23,040 --> 00:14:36,560
 Her teacher was not mean, but she was somewhat demanding.

191
00:14:36,560 --> 00:14:40,020
 Her discipline was not very strong, but the student who

192
00:14:40,020 --> 00:14:42,000
 brought her teacher with her to

193
00:14:42,000 --> 00:14:47,660
 see us in the monastery was probably the most well-discipl

194
00:14:47,660 --> 00:14:52,000
ined person I'd ever met, maybe

195
00:14:52,000 --> 00:14:56,320
 ever have met still.

196
00:14:56,320 --> 00:15:01,150
 She had some sort of training in manners and she was, "Yes,

197
00:15:01,150 --> 00:15:03,560
 Miss, no, Miss, can I help

198
00:15:03,560 --> 00:15:06,200
 you, Miss?"

199
00:15:06,200 --> 00:15:10,000
 Someone who was very well-trained, very disciplined and

200
00:15:10,000 --> 00:15:13,480
 would not show any sort of anger or upset.

201
00:15:13,480 --> 00:15:17,360
 She'd sit the way she sat, the way she walked.

202
00:15:17,360 --> 00:15:23,040
 There was someone you could see who had received great

203
00:15:23,040 --> 00:15:26,360
 training and discipline.

204
00:15:26,360 --> 00:15:30,000
 Discipline means to have body and to have speech which is

205
00:15:30,000 --> 00:15:36,560
 well refined, which is well-trained.

206
00:15:36,560 --> 00:15:41,790
 Some people on the other hand are always moving, always f

207
00:15:41,790 --> 00:15:45,680
idgeting about, always saying useless

208
00:15:45,680 --> 00:15:51,180
 things, always talking about, and the way they talk is not

209
00:15:51,180 --> 00:15:57,200
 trained or not polite or disciplined.

210
00:15:57,200 --> 00:16:03,640
 In the practice of Buddhism, of course, it takes on a more

211
00:16:03,640 --> 00:16:07,440
 specific meaning, a meaning

212
00:16:07,440 --> 00:16:12,190
 of refraining from evil deeds with the body and with the

213
00:16:12,190 --> 00:16:13,200
 speech.

214
00:16:13,200 --> 00:16:18,480
 Because a person can be disciplined and still be able to do

215
00:16:18,480 --> 00:16:19,840
 evil deeds.

216
00:16:19,840 --> 00:16:26,080
 For instance, soldiers or police officers or even thieves

217
00:16:26,080 --> 00:16:29,120
 and villains are able to have

218
00:16:29,120 --> 00:16:32,480
 certain type of discipline.

219
00:16:32,480 --> 00:16:45,760
 When it comes to refraining from evil deeds, they might not

220
00:16:45,760 --> 00:16:48,760
 hesitate.

221
00:16:48,760 --> 00:16:51,380
 So when we mean discipline in the Buddhist teaching, we're

222
00:16:51,380 --> 00:16:52,760
 talking about refraining from

223
00:16:52,760 --> 00:16:57,960
 deeds which are inappropriate.

224
00:16:57,960 --> 00:17:02,340
 When we're lay people, we have to keep five precepts or

225
00:17:02,340 --> 00:17:03,960
 eight precepts.

226
00:17:03,960 --> 00:17:07,910
 These are rules which we have to train ourselves in for the

227
00:17:07,910 --> 00:17:11,160
 purpose of the attainment of concentration.

228
00:17:11,160 --> 00:17:15,240
 Morality is always, discipline is always for the purpose of

229
00:17:15,240 --> 00:17:17,440
 attaining to concentration.

230
00:17:17,440 --> 00:17:19,760
 For monks, we have so many rules.

231
00:17:19,760 --> 00:17:24,070
 But the truth is when we keep all of these rules, we can

232
00:17:24,070 --> 00:17:26,760
 realize that our concentration

233
00:17:26,760 --> 00:17:35,800
 really improves.

234
00:17:35,800 --> 00:17:39,800
 When this year I stopped to touch money, as a monk we're

235
00:17:39,800 --> 00:17:42,360
 not supposed to be touching money,

236
00:17:42,360 --> 00:17:46,070
 we're supposed to be buying things or involved with

237
00:17:46,070 --> 00:17:47,520
 touching money.

238
00:17:47,520 --> 00:17:52,470
 So I started to train myself in this and since I started

239
00:17:52,470 --> 00:17:55,560
 training you can see clearly that

240
00:17:55,560 --> 00:17:57,520
 one certainly does lead to concentration.

241
00:17:57,520 --> 00:17:59,720
 It leads your mind to be fixed and composed.

242
00:17:59,720 --> 00:18:03,080
 You don't have to be worrying or thinking about this or

243
00:18:03,080 --> 00:18:04,040
 about that.

244
00:18:04,040 --> 00:18:08,110
 All the things that surround you in the world, your mind

245
00:18:08,110 --> 00:18:10,520
 becomes fixed and focused in one

246
00:18:10,520 --> 00:18:20,440
 place, not restless or disturbed as before.

247
00:18:20,440 --> 00:18:25,730
 But even in regards to all of the precepts of five, eight,

248
00:18:25,730 --> 00:18:28,880
 or 227 rules that the monks

249
00:18:28,880 --> 00:18:38,030
 have to keep, the most important, the more important

250
00:18:38,030 --> 00:18:40,900
 discipline is the discipline which

251
00:18:40,900 --> 00:18:46,200
 comes from the mind which is well trained.

252
00:18:46,200 --> 00:18:49,120
 When the mind is not flitting here or flitting there or

253
00:18:49,120 --> 00:18:53,320
 falling into bad or unwholesome states.

254
00:18:53,320 --> 00:18:57,070
 The disciplined mind, when we talk about disciplining the

255
00:18:57,070 --> 00:18:59,640
 body and the speech, well, people can

256
00:18:59,640 --> 00:19:05,350
 force themselves to do that and still be evil people inside

257
00:19:05,350 --> 00:19:05,760
.

258
00:19:05,760 --> 00:19:10,800
 But the discipline of the mind is just the much more

259
00:19:10,800 --> 00:19:12,880
 important thing.

260
00:19:12,880 --> 00:19:23,080
 To have a mind which is well disciplined.

261
00:19:23,080 --> 00:19:27,930
 We don't let our mind get angry or we don't let our mind

262
00:19:27,930 --> 00:19:30,720
 become greedy or we don't let

263
00:19:30,720 --> 00:19:31,720
 our mind become deluded.

264
00:19:31,720 --> 00:19:35,180
 We come to see that these things are the cause of suffering

265
00:19:35,180 --> 00:19:37,360
 or they're suffering themselves

266
00:19:37,360 --> 00:19:44,360
 and they let go of them.

267
00:19:44,360 --> 00:19:46,870
 This comes of course from the practice of meditation which

268
00:19:46,870 --> 00:19:48,080
 we're all undertaking.

269
00:19:48,080 --> 00:19:53,000
 This is the most important thing for us is always to push

270
00:19:53,000 --> 00:19:56,040
 ourselves, to pull ourselves,

271
00:19:56,040 --> 00:20:01,250
 to bring ourselves up to the level where we don't get angry

272
00:20:01,250 --> 00:20:03,360
 even when people do bad things

273
00:20:03,360 --> 00:20:04,360
 to us.

274
00:20:04,360 --> 00:20:09,460
 We want to be better than them or we don't get angry at

275
00:20:09,460 --> 00:20:10,360
 them.

276
00:20:10,360 --> 00:20:19,860
 We send love to them, may they be happy, forgive them, may

277
00:20:19,860 --> 00:20:24,880
 they be free from suffering.

278
00:20:24,880 --> 00:20:30,350
 So on where our minds don't become upset or even attached

279
00:20:30,350 --> 00:20:33,000
 to good things in the world.

280
00:20:33,000 --> 00:20:35,720
 This is called Vinayo Chasusikit.

281
00:20:35,720 --> 00:20:43,960
 I have a well trained discipline, a disciplined mind.

282
00:20:43,960 --> 00:20:48,800
 And number 10, Supa Sita Yawata.

283
00:20:48,800 --> 00:20:51,800
 Whatever words, whatever speech is well spoken.

284
00:20:51,800 --> 00:20:57,470
 This is the greatest, this is a blessing in the greatest

285
00:20:57,470 --> 00:21:00,580
 sense, the highest sense.

286
00:21:00,580 --> 00:21:08,960
 Number 10 is whatever speech is well spoken.

287
00:21:08,960 --> 00:21:14,590
 This is a blessing in the world is kind of funny because

288
00:21:14,590 --> 00:21:18,040
 sometimes you meet people whose

289
00:21:18,040 --> 00:21:20,440
 speech is so sweet.

290
00:21:20,440 --> 00:21:24,260
 They're called the flatterer, someone who says always

291
00:21:24,260 --> 00:21:25,400
 saying good.

292
00:21:25,400 --> 00:21:29,760
 You hear them saying good things.

293
00:21:29,760 --> 00:21:33,390
 And sometimes it happens that you meet someone who to your

294
00:21:33,390 --> 00:21:35,360
 face they say so many nice good

295
00:21:35,360 --> 00:21:37,360
 things.

296
00:21:37,360 --> 00:21:41,620
 But if you're perceptive you know that right behind your

297
00:21:41,620 --> 00:21:43,880
 back for sure they're not saying

298
00:21:43,880 --> 00:21:47,120
 good things about you.

299
00:21:47,120 --> 00:21:48,560
 You know why they're saying it?

300
00:21:48,560 --> 00:21:51,520
 They're not saying it because they mean it or because of

301
00:21:51,520 --> 00:21:52,840
 loving kindness which exists

302
00:21:52,840 --> 00:21:53,840
 in their heart.

303
00:21:53,840 --> 00:21:59,640
 They're saying it because they know when they say it.

304
00:21:59,640 --> 00:22:11,920
 It's a way of ingratiating themselves with you.

305
00:22:11,920 --> 00:22:18,150
 Sometimes you see people smiling but you know behind their

306
00:22:18,150 --> 00:22:21,360
 smile they're really looking

307
00:22:21,360 --> 00:22:29,240
 at you as though you're some kind of beast or so on.

308
00:22:29,240 --> 00:22:33,600
 These kind of people what I mean is that sometimes people

309
00:22:33,600 --> 00:22:36,160
 have sweet words and you think oh that's

310
00:22:36,160 --> 00:22:37,720
 well said.

311
00:22:37,720 --> 00:22:43,180
 But really the intentions behind their speech are not

312
00:22:43,180 --> 00:22:45,800
 wholesome, not pure.

313
00:22:45,800 --> 00:22:52,000
 For speech to really be truly well spoken has to come from

314
00:22:52,000 --> 00:22:53,800
 a pure heart.

315
00:22:53,800 --> 00:23:00,350
 For instance in the Buddha's time there was one monk who

316
00:23:00,350 --> 00:23:03,120
 lived in the forest.

317
00:23:03,120 --> 00:23:09,080
 And all his teaching was one story.

318
00:23:09,080 --> 00:23:12,300
 He had heard many things but all he remembered he kept in

319
00:23:12,300 --> 00:23:14,240
 mind only one story of the Buddha

320
00:23:14,240 --> 00:23:20,090
 but he was so good at telling it that every time when he

321
00:23:20,090 --> 00:23:23,120
 told the story when he came to

322
00:23:23,120 --> 00:23:28,210
 the verse at the end and finished up the story that all of

323
00:23:28,210 --> 00:23:31,200
 the angels and all of the beings

324
00:23:31,200 --> 00:23:35,400
 living in the forest they would all say "sadhu, sadhu" and

325
00:23:35,400 --> 00:23:37,720
 this great noise would ring through

326
00:23:37,720 --> 00:23:39,720
 the forest.

327
00:23:39,720 --> 00:23:44,860
 And this one day a monk came to visit him and he saw this

328
00:23:44,860 --> 00:23:47,520
 and he said "oh, these people

329
00:23:47,520 --> 00:23:51,200
 don't know good teaching when they hear it.

330
00:23:51,200 --> 00:23:54,280
 I'll go up and give a talk."

331
00:23:54,280 --> 00:23:56,780
 And he talked and talked and talked and talked and gave a

332
00:23:56,780 --> 00:23:58,280
 long talk about many different

333
00:23:58,280 --> 00:23:59,280
 things.

334
00:23:59,280 --> 00:24:06,440
 There was a monk who had great learning, vast learning.

335
00:24:06,440 --> 00:24:12,610
 And at the end he finished the talk and he sat and waited

336
00:24:12,610 --> 00:24:15,760
 and there was no noise, he

337
00:24:15,760 --> 00:24:20,960
 didn't hear anything.

338
00:24:20,960 --> 00:24:24,580
 And the other monk said "yes, yes" and the other monk got

339
00:24:24,580 --> 00:24:26,400
 up on the chair and just said

340
00:24:26,400 --> 00:24:29,760
 his one verse, the end verse of the story and immediately

341
00:24:29,760 --> 00:24:31,280
 the whole forest shook with

342
00:24:31,280 --> 00:24:38,120
 sadhu.

343
00:24:38,120 --> 00:24:43,520
 The monk who, because it was given from his heart.

344
00:24:43,520 --> 00:24:47,070
 When we say things we have to be sure that our intentions

345
00:24:47,070 --> 00:24:47,920
 are pure.

346
00:24:47,920 --> 00:24:51,040
 We can't just say things to flatter other people.

347
00:24:51,040 --> 00:24:55,480
 And we can't just be like a parrot repeating the words of

348
00:24:55,480 --> 00:24:56,920
 other people.

349
00:24:56,920 --> 00:25:02,170
 When we wish people good goodness for themselves we have to

350
00:25:02,170 --> 00:25:05,680
 see do we really wish them goodness.

351
00:25:05,680 --> 00:25:07,840
 When we teach the teaching of the Lord Buddha do we really

352
00:25:07,840 --> 00:25:08,320
 mean it?

353
00:25:08,320 --> 00:25:10,320
 Do we really understand the teaching?

354
00:25:10,320 --> 00:25:16,910
 Otherwise we are going to just be like an empty book, like

355
00:25:16,910 --> 00:25:18,400
 a parrot.

356
00:25:18,400 --> 00:25:26,420
 Like the Lord Buddha said, someone who feeds cows, someone

357
00:25:26,420 --> 00:25:30,200
 who looks at what do they call

358
00:25:30,200 --> 00:25:36,980
 a shepherd or a cowherd, someone who looks after other

359
00:25:36,980 --> 00:25:39,280
 people's cows.

360
00:25:39,280 --> 00:25:48,250
 When you look after other people's cows you never get to

361
00:25:48,250 --> 00:25:53,200
 possess the fruits of looking

362
00:25:53,200 --> 00:25:57,130
 after the cow, the fruits of having the milk or having

363
00:25:57,130 --> 00:25:59,800
 cheese or butter, the products which

364
00:25:59,800 --> 00:26:03,760
 come from the cow.

365
00:26:03,760 --> 00:26:10,130
 If the cows are not your cows, they are someone else's cows

366
00:26:10,130 --> 00:26:10,480
.

367
00:26:10,480 --> 00:26:17,190
 You will never get to taste the fruits that come from the

368
00:26:17,190 --> 00:26:18,120
 cow.

369
00:26:18,120 --> 00:26:21,000
 Buddha said in the same way even though someone knows a lot

370
00:26:21,000 --> 00:26:22,600
, a lot of the teaching is able

371
00:26:22,600 --> 00:26:26,680
 to teach it.

372
00:26:26,680 --> 00:26:28,560
 If they haven't come to practice or understand the

373
00:26:28,560 --> 00:26:30,720
 teachings they are just like someone who

374
00:26:30,720 --> 00:26:43,510
 looks after other people's cows, they won't get the true

375
00:26:43,510 --> 00:26:50,000
 flavor of the tambourine.

376
00:26:50,000 --> 00:26:56,390
 For something to be well spoken it needs to be at the right

377
00:26:56,390 --> 00:26:57,400
 time.

378
00:26:57,400 --> 00:27:01,660
 Sometimes you say something good but it's at the wrong time

379
00:27:01,660 --> 00:27:01,840
.

380
00:27:01,840 --> 00:27:08,560
 It has to be spoken and it has to be the truth.

381
00:27:08,560 --> 00:27:23,960
 Sometimes we speak nice things but it's not the truth.

382
00:27:23,960 --> 00:27:31,880
 It has to be an appropriate thing to say.

383
00:27:31,880 --> 00:27:41,850
 It can't be something useless or something which is unhelp

384
00:27:41,850 --> 00:27:43,120
ful.

385
00:27:43,120 --> 00:27:55,680
 It has to be said with love, with loving kindness in one's

386
00:27:55,680 --> 00:27:57,120
 heart and so on.

387
00:27:57,120 --> 00:28:02,190
 There are many factors which lead for something to be well

388
00:28:02,190 --> 00:28:03,200
 spoken.

389
00:28:03,200 --> 00:28:04,800
 This is considered a great blessing.

390
00:28:04,800 --> 00:28:07,040
 It's a blessing for the person who speaks.

391
00:28:07,040 --> 00:28:09,120
 It's a blessing for the person who hears it.

392
00:28:09,120 --> 00:28:13,720
 When we hear the teachings of the Lord Buddha, it's one of

393
00:28:13,720 --> 00:28:16,000
 the greatest blessings.

394
00:28:16,000 --> 00:28:18,890
 When we talk about the teachings of the Lord Buddha, when

395
00:28:18,890 --> 00:28:20,800
 we talk about meditation practice,

396
00:28:20,800 --> 00:28:25,310
 it's one of the greatest blessings that leads to peace and

397
00:28:25,310 --> 00:28:27,680
 to happiness in the world.

398
00:28:27,680 --> 00:28:30,260
 Even animals, when they hear the teaching of the Lord

399
00:28:30,260 --> 00:28:32,040
 Buddha, because it's so pure and

400
00:28:32,040 --> 00:28:36,250
 there's nothing mean or evil being said, their minds can

401
00:28:36,250 --> 00:28:37,520
 become pure.

402
00:28:37,520 --> 00:28:41,720
 In the time of the Lord Buddha, there's a story of a frog

403
00:28:41,720 --> 00:28:44,160
 that was reborn in heaven simply

404
00:28:44,160 --> 00:28:46,160
 from listening to the Lord Buddha's teaching.

405
00:28:46,160 --> 00:28:53,870
 He didn't understand a word but was sitting absorbing the

406
00:28:53,870 --> 00:28:56,920
 sound vibrations and as a result

407
00:28:56,920 --> 00:29:01,760
 of the wholesome mind which arose as a result, at the

408
00:29:01,760 --> 00:29:05,240
 moment when the frog died, it was reborn

409
00:29:05,240 --> 00:29:13,080
 in heaven.

410
00:29:13,080 --> 00:29:14,400
 So it's considered a blessing.

411
00:29:14,400 --> 00:29:18,530
 Whatever words are well spoken, it's considered a blessing

412
00:29:18,530 --> 00:29:19,320
 as well.

413
00:29:19,320 --> 00:29:23,360
 This is the blessings numbers 7 to 10.

414
00:29:23,360 --> 00:29:24,360
 Thank you.

415
00:29:24,360 --> 00:29:25,360
 Thank you.

416
00:29:25,360 --> 00:29:26,360
 Thank you.

