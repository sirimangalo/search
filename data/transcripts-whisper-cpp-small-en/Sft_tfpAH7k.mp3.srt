1
00:00:00,000 --> 00:00:11,200
 Good evening everyone.

2
00:00:11,200 --> 00:00:22,660
 Broadcasting live from Stony Creek Ontario, August 7th,

3
00:00:22,660 --> 00:00:26,000
 2015.

4
00:00:26,000 --> 00:00:35,360
 Today we have another verse on Mita or not verse, passage.

5
00:00:35,360 --> 00:00:37,680
 This one is actually one of the more popular passages.

6
00:00:37,680 --> 00:00:40,880
 It may not be very well known in the West, but in Thailand

7
00:00:40,880 --> 00:00:42,960
 and in Asia we actually, some

8
00:00:42,960 --> 00:00:45,880
 places they'll chant it.

9
00:00:45,880 --> 00:00:48,470
 When I stayed at the Thai monastery in Los Angeles they

10
00:00:48,470 --> 00:00:51,240
 would chant this every day.

11
00:00:51,240 --> 00:00:58,070
 Mita ya bhikka vay cheddhoi muthi ya, asse vita ya bah vita

12
00:00:58,070 --> 00:01:05,680
 ya bahudhi kata ya, yani

13
00:01:05,680 --> 00:01:16,350
 kata ya vatukata ya, anuktivta ya, parichitta ya, su samar

14
00:01:16,350 --> 00:01:17,520
ata ya.

15
00:01:17,520 --> 00:01:39,680
 Sounds nice in the Pali.

16
00:01:39,680 --> 00:01:52,680
 So here we have 11 eka dasa nisangsa, without doubt, to 11

17
00:01:52,680 --> 00:01:57,840
 benefits, anisangsa.

18
00:01:57,840 --> 00:02:01,120
 So I saw people were asking what are the 11, because in the

19
00:02:01,120 --> 00:02:02,400
 English it's not clear, but

20
00:02:02,400 --> 00:02:06,700
 if you click on the link to the Pali, if you're using

21
00:02:06,700 --> 00:02:09,640
 Firefox and you have the digital Pali,

22
00:02:09,640 --> 00:02:14,230
 the Pali reader installed, and on top of that you click on

23
00:02:14,230 --> 00:02:16,600
 the link, it will take you to

24
00:02:16,600 --> 00:02:18,600
 the Pali.

25
00:02:18,600 --> 00:02:23,320
 If you read that you can see in the second paragraph it's

26
00:02:23,320 --> 00:02:24,200
 clear.

27
00:02:24,200 --> 00:02:26,760
 Of course you have to also know Pali.

28
00:02:26,760 --> 00:02:30,680
 A lot of ifs.

29
00:02:30,680 --> 00:02:36,320
 Sukang supatthi, one dreams or sleeps in happiness.

30
00:02:36,320 --> 00:02:51,210
 Because one is not bothered by guilt or remorse, hatred or

31
00:02:51,210 --> 00:02:53,400
 fear.

32
00:02:53,400 --> 00:02:58,900
 Sukang patthi bhudshati, one wakes up happily, so doesn't

33
00:02:58,900 --> 00:03:01,160
 wake up feeling guilty or bad about

34
00:03:01,160 --> 00:03:04,480
 the things that they've done either.

35
00:03:04,480 --> 00:03:10,980
 And the papa kang supinang patthi, one doesn't see any evil

36
00:03:10,980 --> 00:03:14,440
 dreams, because one's mind is

37
00:03:14,440 --> 00:03:16,440
 clear.

38
00:03:16,440 --> 00:03:28,340
 Love is like this pure vibe that keeps your mind on a

39
00:03:28,340 --> 00:03:34,320
 wavelength, on a positive wavelength,

40
00:03:34,320 --> 00:03:39,480
 so the bad brainwaves can't arise.

41
00:03:39,480 --> 00:03:45,290
 The bad mental activity is quenched because the mental

42
00:03:45,290 --> 00:03:49,040
 activity is on a positive level.

43
00:03:49,040 --> 00:03:53,930
 I mean there's probably a more organic way to explain it,

44
00:03:53,930 --> 00:03:56,400
 but basically it's like that.

45
00:03:56,400 --> 00:04:06,260
 It's like a good vibes and good vibrations have the effect

46
00:04:06,260 --> 00:04:10,560
 of focusing one's state of

47
00:04:10,560 --> 00:04:14,160
 mind.

48
00:04:14,160 --> 00:04:16,440
 So one doesn't see bad dreams either.

49
00:04:16,440 --> 00:04:23,040
 Manusanang pyohoti, one is dear to humans.

50
00:04:23,040 --> 00:04:31,760
 Manusanang pyohoti, one is dear to non-humans.

51
00:04:31,760 --> 00:04:37,600
 Non-humans is usually, in modern times they use the word to

52
00:04:37,600 --> 00:04:40,880
 mean like ghosts or evil spirits.

53
00:04:40,880 --> 00:04:46,880
 But Amanusa could mean any non-human, could mean animals.

54
00:04:46,880 --> 00:04:50,380
 People who have love are very good with animals, people

55
00:04:50,380 --> 00:04:51,560
 full of metta.

56
00:04:51,560 --> 00:04:57,390
 You can often tell someone someone's love by how good they

57
00:04:57,390 --> 00:04:59,520
 are with animals.

58
00:04:59,520 --> 00:05:04,770
 How good they are with children, how good they are with

59
00:05:04,770 --> 00:05:05,960
 animals.

60
00:05:05,960 --> 00:05:10,220
 Usually you can't tell how good they are with ghosts or

61
00:05:10,220 --> 00:05:11,720
 angels or so on.

62
00:05:11,720 --> 00:05:14,840
 Love's so easy.

63
00:05:14,840 --> 00:05:20,320
 Devatara kanti, the angels guard the person, they're

64
00:05:20,320 --> 00:05:22,520
 guarded by angels.

65
00:05:22,520 --> 00:05:26,240
 Angels see fit to guard because they appreciate the

66
00:05:26,240 --> 00:05:28,200
 goodness of the person.

67
00:05:28,200 --> 00:05:32,050
 They try to make sure that no harm comes to this being

68
00:05:32,050 --> 00:05:33,240
 apparently.

69
00:05:33,240 --> 00:05:37,790
 Even though we can't normally see them, this apparently

70
00:05:37,790 --> 00:05:38,760
 happens.

71
00:05:38,760 --> 00:05:50,800
 Nasa agiwa wisangwa satangwa kamati, fire poison and swords

72
00:05:50,800 --> 00:05:52,200
 do not affect one.

73
00:05:52,200 --> 00:05:58,040
 Do not affect one.

74
00:05:58,040 --> 00:06:05,320
 They're not the end terms.

75
00:06:05,320 --> 00:06:09,560
 They can't penetrate.

76
00:06:09,560 --> 00:06:15,560
 That's the meaning.

77
00:06:15,560 --> 00:06:19,810
 Not for that person to agi and not into that person to fire

78
00:06:19,810 --> 00:06:22,120
 and poison and weapons enter.

79
00:06:22,120 --> 00:06:25,120
 So a person with love, if they have this, remember we're

80
00:06:25,120 --> 00:06:27,000
 talking here about jito imurti,

81
00:06:27,000 --> 00:06:32,830
 which means a state of jhana, of intense super, kind of

82
00:06:32,830 --> 00:06:34,240
 super mundane concentration.

83
00:06:34,240 --> 00:06:39,240
 It's still mundane, but it's beyond the central realm.

84
00:06:39,240 --> 00:06:43,330
 So there's a state of vibration that is so intense that it

85
00:06:43,330 --> 00:06:44,840
 actually takes one out of

86
00:06:44,840 --> 00:06:46,960
 this sphere.

87
00:06:46,960 --> 00:06:50,000
 And so the idea is that weapons don't even penetrate.

88
00:06:50,000 --> 00:06:53,160
 You can get hit with a sword or shot with a gun.

89
00:06:53,160 --> 00:06:56,800
 They talk about this in Thailand.

90
00:06:56,800 --> 00:07:01,170
 There's been cases where monks or meditators were shot with

91
00:07:01,170 --> 00:07:03,240
 guns and it didn't hit them

92
00:07:03,240 --> 00:07:04,240
 somehow.

93
00:07:04,240 --> 00:07:09,400
 I mean, the truth of it is something else.

94
00:07:09,400 --> 00:07:10,800
 But this is the idea.

95
00:07:10,800 --> 00:07:13,360
 This is something apparently the Buddha said.

96
00:07:13,360 --> 00:07:14,360
 Poison.

97
00:07:14,360 --> 00:07:21,600
 If you're full of love, there's a strength of mind to it.

98
00:07:21,600 --> 00:07:22,600
 So how many do we have?

99
00:07:22,600 --> 00:07:29,040
 One sukang, supati, dreams, sleeps in happiness.

100
00:07:29,040 --> 00:07:30,720
 Two wakes in happiness.

101
00:07:30,720 --> 00:07:32,360
 Three no evil dreams.

102
00:07:32,360 --> 00:07:34,320
 Four dear to humans.

103
00:07:34,320 --> 00:07:37,040
 Five dear to nonhumans.

104
00:07:37,040 --> 00:07:40,360
 Six angels guard in person.

105
00:07:40,360 --> 00:07:45,080
 Seven these things don't hurt the person.

106
00:07:45,080 --> 00:07:48,400
 Tuvatang jitang samadhyanti.

107
00:07:48,400 --> 00:07:56,080
 Number eight, one gets a well established concentration.

108
00:07:56,080 --> 00:07:58,720
 Mind concentrates of tuvatangs quickly.

109
00:07:58,720 --> 00:08:02,150
 Mind becomes concentrated quickly because one doesn't have

110
00:08:02,150 --> 00:08:03,760
 to deal with negativity.

111
00:08:03,760 --> 00:08:08,560
 The mind is full of positivity, wishing only good things.

112
00:08:08,560 --> 00:08:12,160
 Positivity supports the mind.

113
00:08:12,160 --> 00:08:16,040
 That's eight, right?

114
00:08:16,040 --> 00:08:20,520
 Nine, mukavarno vipasi dati.

115
00:08:20,520 --> 00:08:26,320
 One has a radiant complexion.

116
00:08:26,320 --> 00:08:30,240
 Number ten, one dies.

117
00:08:30,240 --> 00:08:34,040
 Asamulho kalang karate.

118
00:08:34,040 --> 00:08:38,410
 One makes one's time, means dies, makes an end to one's

119
00:08:38,410 --> 00:08:39,160
 time.

120
00:08:39,160 --> 00:08:42,920
 Asamulho, without confusion.

121
00:08:42,920 --> 00:08:47,040
 I'm confused because the mind is so sharp and strong and

122
00:08:47,040 --> 00:08:49,360
 focused, clearly aware of what

123
00:08:49,360 --> 00:08:52,120
 is good.

124
00:08:52,120 --> 00:09:00,880
 And if one doesn't further develop insight meditation, ut

125
00:09:00,880 --> 00:09:05,080
ari apati vijanto, if one doesn't

126
00:09:05,080 --> 00:09:13,020
 develop further, brahma lokupagohoti, one is set to arise

127
00:09:13,020 --> 00:09:15,960
 in the brahma loka.

128
00:09:15,960 --> 00:09:18,600
 So yeah, love is a great thing.

129
00:09:18,600 --> 00:09:19,600
 Love is awesome.

130
00:09:19,600 --> 00:09:22,560
 Love is these benefits, but there's clearly a limit to

131
00:09:22,560 --> 00:09:23,640
 those benefits.

132
00:09:23,640 --> 00:09:27,200
 There's no matter how great they are.

133
00:09:27,200 --> 00:09:34,920
 Finally there's a provision that it can only lead so far.

134
00:09:34,920 --> 00:09:41,450
 It leads to be born as a god or to arise in the brahma loka

135
00:09:41,450 --> 00:09:44,920
, which is still finite and

136
00:09:44,920 --> 00:09:46,920
 still a part of samsara.

137
00:09:46,920 --> 00:09:50,890
 Nonetheless, it's great and it's quite helpful in the

138
00:09:50,890 --> 00:09:53,560
 practice of insight meditation.

139
00:09:53,560 --> 00:09:57,710
 Today it's fortuitous or serendipitous, fortuitous, serend

140
00:09:57,710 --> 00:10:01,080
ipitous that this first comes today.

141
00:10:01,080 --> 00:10:09,000
 Today we were invited to, for lunch, at a restaurant in

142
00:10:09,000 --> 00:10:13,040
 Aaron Mills, somewhere near

143
00:10:13,040 --> 00:10:18,000
 Mississauga, Toronto area.

144
00:10:18,000 --> 00:10:19,800
 And I wasn't really keen to go.

145
00:10:19,800 --> 00:10:22,800
 I said, I got a call and said, "You want to go?"

146
00:10:22,800 --> 00:10:24,800
 I said, "No, not really."

147
00:10:24,800 --> 00:10:26,840
 But then, oh, I think I know what it was.

148
00:10:26,840 --> 00:10:29,320
 I knew it was, there was a monk who had visited here and he

149
00:10:29,320 --> 00:10:30,920
's such a kind monk, someone who's

150
00:10:30,920 --> 00:10:34,740
 so full of love, you could just get a feeling for how kind

151
00:10:34,740 --> 00:10:35,400
 he is.

152
00:10:35,400 --> 00:10:37,970
 So that, well, if I don't go, it's going to be

153
00:10:37,970 --> 00:10:39,560
 disappointing to him.

154
00:10:39,560 --> 00:10:44,840
 You see how great his love is able to manipulate people.

155
00:10:44,840 --> 00:10:48,100
 Even without trying, he manipulates me into wanting to go

156
00:10:48,100 --> 00:10:50,100
 because I wouldn't want to disappoint

157
00:10:50,100 --> 00:10:52,520
 someone so full of love.

158
00:10:52,520 --> 00:10:58,110
 So I went and it turned out it was a meeting of all the,

159
00:10:58,110 --> 00:11:01,600
 many of the monks who had helped

160
00:11:01,600 --> 00:11:06,250
 organize or been involved in the celebration of Wesak in,

161
00:11:06,250 --> 00:11:08,680
 in, in celebration square in

162
00:11:08,680 --> 00:11:12,400
 Mississauga in May.

163
00:11:12,400 --> 00:11:15,290
 So I don't have any pictures I don't think, but there's

164
00:11:15,290 --> 00:11:17,000
 lots of pictures I'm sure up on

165
00:11:17,000 --> 00:11:18,000
 Facebook by now.

166
00:11:18,000 --> 00:11:22,230
 If you check out Bandai Sarnapala's page, I bet he's got

167
00:11:22,230 --> 00:11:23,200
 some up.

168
00:11:23,200 --> 00:11:25,040
 But it was awesome.

169
00:11:25,040 --> 00:11:27,600
 It was, there was so much just in the short time that we

170
00:11:27,600 --> 00:11:29,320
 were together for lunch, maybe

171
00:11:29,320 --> 00:11:30,320
 two hours.

172
00:11:30,320 --> 00:11:37,120
 There was such love and just kindness and you get a sense

173
00:11:37,120 --> 00:11:40,160
 of how great it is to be in

174
00:11:40,160 --> 00:11:45,990
 good company, how great it is to be with people who have

175
00:11:45,990 --> 00:11:48,400
 this love and this caring.

176
00:11:48,400 --> 00:11:50,640
 And then you can also feel where there's not so much love.

177
00:11:50,640 --> 00:11:54,560
 Like there are times where defilements arise and so you get

178
00:11:54,560 --> 00:11:56,760
 a sense of, well, that person

179
00:11:56,760 --> 00:12:02,320
 who not really in the spirit, that kind of thing.

180
00:12:02,320 --> 00:12:04,970
 And you can taste, you can tell that you can taste the

181
00:12:04,970 --> 00:12:07,000
 difference, kind of different flavor

182
00:12:07,000 --> 00:12:09,200
 to it.

183
00:12:09,200 --> 00:12:15,090
 So there's no, no, never underestimate the power or the

184
00:12:15,090 --> 00:12:17,400
 greatness of love.

185
00:12:17,400 --> 00:12:21,160
 It's a wonderful thing and brings lots of benefits.

186
00:12:21,160 --> 00:12:24,800
 So that's the dumb for today.

187
00:12:24,800 --> 00:12:26,480
 Now we can get into some questions.

188
00:12:26,480 --> 00:12:27,880
 Does anybody have any questions?

189
00:12:27,880 --> 00:12:30,760
 I'm going to stipulate that they should probably be about

190
00:12:30,760 --> 00:12:32,440
 meditation because I don't want to

191
00:12:32,440 --> 00:12:36,160
 get overwhelmed here.

192
00:12:36,160 --> 00:12:45,680
 So let's try to keep them to meditation question.

193
00:12:45,680 --> 00:13:07,240
 What do you do if you have a bad dream but cannot wake up?

194
00:13:07,240 --> 00:13:11,720
 The problem of dreaming is there's no mindfulness, the

195
00:13:11,720 --> 00:13:15,040
 variability that's required to grasp,

196
00:13:15,040 --> 00:13:20,230
 to grasp what's happening and to experience it clearly and

197
00:13:20,230 --> 00:13:22,520
 to realize this is a dream

198
00:13:22,520 --> 00:13:23,520
 is generally lacking.

199
00:13:23,520 --> 00:13:28,790
 And as soon as you realize it's a dream, you wake up, I

200
00:13:28,790 --> 00:13:29,720
 think.

201
00:13:29,720 --> 00:13:33,680
 There are people, I guess, who have lucid dreams.

202
00:13:33,680 --> 00:13:42,080
 But in that case, it's much less a dream to know so much

203
00:13:42,080 --> 00:13:46,120
 about lucid dreaming.

204
00:13:46,120 --> 00:13:51,520
 Anyway, I don't know.

205
00:13:51,520 --> 00:13:54,480
 I'd probably wake up.

206
00:13:54,480 --> 00:13:57,320
 It's not that big of a deal.

207
00:13:57,320 --> 00:13:58,320
 It happens.

208
00:13:58,320 --> 00:13:59,840
 There's not much you can do about it.

209
00:13:59,840 --> 00:14:03,390
 The more you meditate, the less you dream, certainly the

210
00:14:03,390 --> 00:14:05,240
 less bad dreams you have.

211
00:14:05,240 --> 00:14:11,260
 Prevention, prevent yourself from having the bad dreams in

212
00:14:11,260 --> 00:14:13,240
 the first place.

213
00:14:13,240 --> 00:14:34,010
 They're generally a sign of guilt or other negative mind

214
00:14:34,010 --> 00:14:38,040
 states.

215
00:14:38,040 --> 00:14:40,130
 So if you're listening, if you're watching on YouTube and

216
00:14:40,130 --> 00:14:41,280
 you want to know what's going

217
00:14:41,280 --> 00:14:44,570
 on here, we're actually asking and we're doing the question

218
00:14:44,570 --> 00:14:46,080
 thing in another place.

219
00:14:46,080 --> 00:14:52,720
 We're doing questions at meditation.siri-mongolot.org.

220
00:14:52,720 --> 00:14:56,000
 And so I'm not answering questions on YouTube right now.

221
00:14:56,000 --> 00:15:00,920
 I'm reading questions people are posting in our chat box,

222
00:15:00,920 --> 00:15:05,960
 our shout box over at meditation.siri-mongolot.org.

223
00:15:05,960 --> 00:15:10,000
 But I'm also broadcasting simultaneously on YouTube.

224
00:15:10,000 --> 00:15:19,520
 Just because there's lots of people on YouTube.

225
00:15:19,520 --> 00:15:22,810
 We recommend practice of the ability to lucid dream at will

226
00:15:22,810 --> 00:15:23,080
.

227
00:15:23,080 --> 00:15:24,080
 No.

228
00:15:24,080 --> 00:15:27,080
 No, I teach based on a specific meditation practice.

229
00:15:27,080 --> 00:15:28,080
 I think there's a link.

230
00:15:28,080 --> 00:15:31,170
 Yes, there's a link to the booklet up near the top of the

231
00:15:31,170 --> 00:15:31,640
 page.

232
00:15:31,640 --> 00:15:54,280
 So if you click on that link, you'll see what I recommend.

233
00:15:54,280 --> 00:15:56,940
 Any advice for countering the attachment to the idea of

234
00:15:56,940 --> 00:15:58,800
 progress in the meditation practice

235
00:15:58,800 --> 00:16:01,680
 or is such desire acceptable?

236
00:16:01,680 --> 00:16:04,120
 It's acceptable I'd say in the beginning.

237
00:16:04,120 --> 00:16:08,320
 It's in the end you do away with it.

238
00:16:08,320 --> 00:16:14,800
 In the beginning you're doing everything wrong.

239
00:16:14,800 --> 00:16:18,560
 In the end you're just mindful.

240
00:16:18,560 --> 00:16:25,520
 There's no countering.

241
00:16:25,520 --> 00:16:29,240
 We're not in charge here that we can counter.

242
00:16:29,240 --> 00:16:32,240
 We're just trying to dodge the bullets.

243
00:16:32,240 --> 00:16:34,920
 Duck and weave, duck and weave.

244
00:16:34,920 --> 00:16:38,790
 Not exactly duck and weave like avoid but be in the right

245
00:16:38,790 --> 00:16:41,920
 place in the right situation.

246
00:16:41,920 --> 00:16:46,210
 Experience every experience with the right frame of mind,

247
00:16:46,210 --> 00:16:48,360
 with a clear frame of mind.

248
00:16:48,360 --> 00:16:51,000
 It's like dodging bullets in a sense.

249
00:16:51,000 --> 00:16:53,920
 You dodge all the problems.

250
00:16:53,920 --> 00:17:00,960
 It takes a lot of maneuvering, flexibility.

251
00:17:00,960 --> 00:17:07,320
 Are there any landmarks besides nimbana that one will

252
00:17:07,320 --> 00:17:11,240
 continue on the right path in the

253
00:17:11,240 --> 00:17:12,240
 next life?

254
00:17:12,240 --> 00:17:13,240
 No.

255
00:17:13,240 --> 00:17:14,240
 Well, no.

256
00:17:14,240 --> 00:17:20,160
 Yeah, there's one and it's called jula-sotapan.

257
00:17:20,160 --> 00:17:23,450
 If one attains at least the second stage of knowledge, the

258
00:17:23,450 --> 00:17:24,720
 tradition goes that such a

259
00:17:24,720 --> 00:17:32,520
 person will not be born in a bad state in their next life.

260
00:17:32,520 --> 00:17:34,240
 Just reaching the second.

261
00:17:34,240 --> 00:17:37,610
 Understanding about it, seeing cause and effect, practicing

262
00:17:37,610 --> 00:17:39,200
 meditation to the point where you

263
00:17:39,200 --> 00:17:42,280
 experience cause and effect and you're able to see how the

264
00:17:42,280 --> 00:17:43,640
 body and the mind work together

265
00:17:43,640 --> 00:17:52,700
 and how good deeds lead to good results, bad deeds or bad

266
00:17:52,700 --> 00:17:58,400
 mind states lead to bad results.

267
00:17:58,400 --> 00:18:02,010
 How much control does a novice have over how much formal

268
00:18:02,010 --> 00:18:03,640
 practice they can do?

269
00:18:03,640 --> 00:18:06,920
 Or how much can a novice expect to be able to formally

270
00:18:06,920 --> 00:18:07,840
 practice?

271
00:18:07,840 --> 00:18:15,090
 I guess it sounds like you have the idea that Buddhism is

272
00:18:15,090 --> 00:18:20,240
 somehow monolithic, homogenous.

273
00:18:20,240 --> 00:18:21,640
 Every monastery you go to is different.

274
00:18:21,640 --> 00:18:26,560
 You can't ask me that unless you're talking about coming

275
00:18:26,560 --> 00:18:28,240
 here to ordain.

276
00:18:28,240 --> 00:18:31,600
 So ordination is a vehicle.

277
00:18:31,600 --> 00:18:37,260
 It depends very much what you do with it and where you go

278
00:18:37,260 --> 00:18:38,480
 with it.

279
00:18:38,480 --> 00:18:41,640
 And so yeah, it depends also on your teachers.

280
00:18:41,640 --> 00:18:47,320
 Maybe they make you sweep out the latrines every day.

281
00:18:47,320 --> 00:18:48,320
 Sweep the forest.

282
00:18:48,320 --> 00:18:50,040
 In Thailand they like to sweep the forest.

283
00:18:50,040 --> 00:18:53,040
 It's just one of the most absurd things to...

284
00:18:53,040 --> 00:18:58,720
 We have a very different view from over here.

285
00:18:58,720 --> 00:19:01,550
 They'll sweep out the entire forest so there's no more

286
00:19:01,550 --> 00:19:03,000
 leaves on the ground and then they'll

287
00:19:03,000 --> 00:19:05,000
 burn all the leaves.

288
00:19:05,000 --> 00:19:13,120
 Keep the snakes away, the scorpions I guess.

289
00:19:13,120 --> 00:19:19,560
 Well if you came to ordain here, you would have lots and

290
00:19:19,560 --> 00:19:24,000
 lots of time to meditate conceivably

291
00:19:24,000 --> 00:19:29,280
 because there's not much else to do.

292
00:19:29,280 --> 00:19:31,400
 I mean you can learn Pali.

293
00:19:31,400 --> 00:19:37,520
 You can do Pali courses and study you can do.

294
00:19:37,520 --> 00:19:42,920
 But you certainly would have time to do courses.

295
00:19:42,920 --> 00:19:53,720
 You'd be expected to do meditation courses regularly.

296
00:19:53,720 --> 00:19:56,110
 And you recommend to meditate right before going to bed for

297
00:19:56,110 --> 00:19:57,240
 preventing bad dreams.

298
00:19:57,240 --> 00:20:05,520
 Yeah, it's a good recommendation I think.

299
00:20:05,520 --> 00:20:08,580
 Can you talk about tensions in the body, especially in the

300
00:20:08,580 --> 00:20:09,000
 jaw?

301
00:20:09,000 --> 00:20:11,560
 Is it okay to let the jaw relax and have the mouth hang

302
00:20:11,560 --> 00:20:12,840
 open when meditating?

303
00:20:12,840 --> 00:20:15,040
 Yeah sure.

304
00:20:15,040 --> 00:20:19,160
 Your mouth dries, it tends to dry out if you do that.

305
00:20:19,160 --> 00:20:26,140
 You can let it relax but maybe relaxed and closed is better

306
00:20:26,140 --> 00:20:26,200
.

307
00:20:26,200 --> 00:20:33,880
 There's nothing wrong with tension as long as you're able

308
00:20:33,880 --> 00:20:36,360
 to maintain it.

309
00:20:36,360 --> 00:20:57,600
 Yeah, insects are a tough one.

310
00:20:57,600 --> 00:21:08,100
 When I was in Sri Lanka, I had termites in my cave and they

311
00:21:08,100 --> 00:21:12,000
 got into my books and all

312
00:21:12,000 --> 00:21:13,000
 my tent.

313
00:21:13,000 --> 00:21:16,800
 They actually ate a hole through the bottom of the tent.

314
00:21:16,800 --> 00:21:19,890
 There's a mosquito tent that I was sleeping in to get at

315
00:21:19,890 --> 00:21:21,440
 the books that were inside the

316
00:21:21,440 --> 00:21:22,960
 tent.

317
00:21:22,960 --> 00:21:25,130
 They weren't interested in the plastic but it got in their

318
00:21:25,130 --> 00:21:26,160
 way so they just ate a hole

319
00:21:26,160 --> 00:21:38,400
 through the bottom of the tent.

320
00:21:38,400 --> 00:21:40,920
 So I had to find a way to get them out of the cave.

321
00:21:40,920 --> 00:21:46,140
 So what I did is I funneled them all into a jar and then I

322
00:21:46,140 --> 00:21:50,040
 took a funnel, another funnel.

323
00:21:50,040 --> 00:21:53,490
 I went outside and found a place where the termites were

324
00:21:53,490 --> 00:21:55,400
 and I opened a little hole in

325
00:21:55,400 --> 00:22:01,160
 it and I funneled them all into the termite mound.

326
00:22:01,160 --> 00:22:10,720
 Sorry, what's the question about the repassant retreat?

327
00:22:10,720 --> 00:22:15,040
 I don't see the question exactly.

328
00:22:15,040 --> 00:22:16,040
 Hoping I'd have a protip.

329
00:22:16,040 --> 00:22:22,720
 I don't really know what a protip is.

330
00:22:22,720 --> 00:22:26,430
 I mean I can't recommend meditation courses in other

331
00:22:26,430 --> 00:22:28,760
 traditions so unless it's in our

332
00:22:28,760 --> 00:22:31,560
 tradition it's not really much I can say.

333
00:22:31,560 --> 00:22:35,960
 You have to talk to people in that tradition.

334
00:22:35,960 --> 00:22:45,000
 Except to say that it's probably a good thing.

335
00:22:45,000 --> 00:22:56,040
 And I can't say too much about other traditions.

336
00:22:56,040 --> 00:23:01,560
 Okay well that's it.

337
00:23:01,560 --> 00:23:06,440
 Thank you all for tuning in then and keep up the practice.

338
00:23:06,440 --> 00:23:11,120
 We'll see you again tomorrow.

339
00:23:11,120 --> 00:23:11,560
 Good night.

340
00:23:11,560 --> 00:23:13,560
 Thank you.

341
00:23:13,560 --> 00:23:23,560
 [BLANK_AUDIO]

