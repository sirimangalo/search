1
00:00:00,000 --> 00:00:26,680
 [ Silence ]

2
00:00:26,680 --> 00:00:28,160
 >> Good evening, everyone.

3
00:00:30,000 --> 00:00:31,080
 Broadcasting live.

4
00:00:31,080 --> 00:00:35,000
 May --

5
00:00:35,000 --> 00:00:42,480
 Okay, good evening.

6
00:00:42,480 --> 00:00:43,180
 We're live.

7
00:00:43,180 --> 00:00:48,420
 May 9th, 2016.

8
00:00:48,420 --> 00:00:54,640
 Happy birthday, everyone.

9
00:00:54,640 --> 00:01:03,980
 I said that to someone today and they said, "What do you

10
00:01:03,980 --> 00:01:04,760
 mean?"

11
00:01:04,760 --> 00:01:10,080
 I guess it's not customary to wish other people to be happy

12
00:01:10,080 --> 00:01:10,740
 on your birthday.

13
00:01:10,740 --> 00:01:15,100
 And it's everybody's birthday anyway.

14
00:01:15,100 --> 00:01:19,920
 We're all born every moment.

15
00:01:19,920 --> 00:01:29,920
 So today's quote is about rejoicing.

16
00:01:29,920 --> 00:01:32,500
 It's about doing good.

17
00:01:32,500 --> 00:01:36,240
 It's about the benefits of doing good.

18
00:01:36,240 --> 00:01:40,720
 But I said there are four benefits of doing good.

19
00:01:40,720 --> 00:01:46,720
 You -- you feel good about yourself.

20
00:01:48,400 --> 00:01:51,220
 Other people say -- what are the four?

21
00:01:51,220 --> 00:01:53,680
 Good things happen to you in this life.

22
00:01:53,680 --> 00:01:57,840
 Why do I think there are four?

23
00:01:57,840 --> 00:02:00,800
 Other people say good things about you.

24
00:02:00,800 --> 00:02:05,760
 You feel happy because you've done good things.

25
00:02:05,760 --> 00:02:06,960
 So good things happen to you.

26
00:02:06,960 --> 00:02:08,600
 I don't remember.

27
00:02:08,600 --> 00:02:11,460
 The fourth one is good things happen in the next life.

28
00:02:16,220 --> 00:02:20,820
 This is actually one of the Dhammapada verses that we've

29
00:02:20,820 --> 00:02:21,660
 been over, right?

30
00:02:21,660 --> 00:02:35,820
 [ Foreign Language ]

31
00:02:35,820 --> 00:02:42,450
 Having seen the goodness, the purity of their own deeds,

32
00:02:42,450 --> 00:02:44,780
 they rejoice.

33
00:02:45,020 --> 00:02:49,640
 So the modati means to be happy or rejoice.

34
00:02:49,640 --> 00:02:56,160
 The modati is pa, it adds a pa, which means they are

35
00:02:56,160 --> 00:02:57,580
 exceedingly joyful.

36
00:02:57,580 --> 00:03:03,520
 They're happy, they're very happy when they see the good --

37
00:03:03,520 --> 00:03:04,700
 the purity of their deeds.

38
00:03:09,680 --> 00:03:14,460
 In the modati they rejoice here, pejal modati they rejoice

39
00:03:14,460 --> 00:03:15,240
 hereafter.

40
00:03:15,240 --> 00:03:28,640
 One who has done good rejoices both times.

41
00:03:38,120 --> 00:03:45,550
 Oh, sister Tikuanduk, is that sister formerly known as

42
00:03:45,550 --> 00:03:46,600
 Valerie?

43
00:03:46,600 --> 00:03:49,160
 Is she here?

44
00:03:49,160 --> 00:03:51,400
 I don't see her on.

45
00:03:51,400 --> 00:03:53,080
 She came on.

46
00:03:53,080 --> 00:03:56,120
 Happy first noble truth.

47
00:03:56,120 --> 00:03:57,960
 Yeah, that's right.

48
00:03:57,960 --> 00:04:02,040
 Jahadipiduka, birth is suffering.

49
00:04:02,040 --> 00:04:14,440
 Wow, oh no, this is someone in the US.

50
00:04:14,440 --> 00:04:16,040
 Where is Tikuanduk?

51
00:04:16,040 --> 00:04:19,080
 Thought that was Valerie.

52
00:04:19,080 --> 00:04:29,640
 So, a simple quote, you do good, you get good.

53
00:04:29,640 --> 00:04:31,160
 You do bad, you get bad.

54
00:04:31,800 --> 00:04:40,440
 We don't talk about rebirth too much, no?

55
00:04:40,440 --> 00:04:42,920
 It's not that important.

56
00:04:42,920 --> 00:04:47,160
 But talking about, thinking about death is important.

57
00:04:47,160 --> 00:04:49,000
 It's important to understand that we die.

58
00:04:49,000 --> 00:04:53,080
 The story of the weaver's daughter who came to see the

59
00:04:53,080 --> 00:04:55,800
 Buddha and the Buddha asked her,

60
00:04:55,800 --> 00:04:56,600
 "Where are you going?"

61
00:04:56,600 --> 00:04:58,680
 And she said, "I don't know."

62
00:04:58,680 --> 00:05:00,200
 And he said, "Where are you coming from?"

63
00:05:00,200 --> 00:05:00,840
 "I don't know."

64
00:05:00,840 --> 00:05:03,320
 And he said, "Do you know?

65
00:05:03,320 --> 00:05:03,880
 Don't you know?"

66
00:05:03,880 --> 00:05:05,240
 And she said, "I do."

67
00:05:05,240 --> 00:05:06,520
 And he said, "Do you know?"

68
00:05:06,520 --> 00:05:07,400
 She said, "I don't."

69
00:05:07,400 --> 00:05:13,000
 And people thought she was crazy.

70
00:05:13,000 --> 00:05:14,600
 Why don't you answer the Buddha's question?

71
00:05:14,600 --> 00:05:18,980
 And then the Buddha said, "What did you mean by those

72
00:05:18,980 --> 00:05:19,480
 answers?"

73
00:05:19,480 --> 00:05:21,480
 She said, "Well, the Buddha doesn't care.

74
00:05:21,480 --> 00:05:24,050
 The Buddha's not going to ask me where am I going, just

75
00:05:24,050 --> 00:05:25,800
 like in this life,

76
00:05:25,800 --> 00:05:27,400
 what he means by where am I going?"

77
00:05:29,160 --> 00:05:31,720
 Because she had spent a lot of time thinking about death

78
00:05:31,720 --> 00:05:33,720
 after she'd heard the Buddha talk previously.

79
00:05:33,720 --> 00:05:37,820
 She said, "Where am I going means in the next life, of

80
00:05:37,820 --> 00:05:38,360
 course.

81
00:05:38,360 --> 00:05:41,140
 That's where I'm really going, but I don't know where I'm

82
00:05:41,140 --> 00:05:41,720
 going."

83
00:05:41,720 --> 00:05:45,880
 "Where I've come from, I don't know that either."

84
00:05:45,880 --> 00:05:50,440
 "I don't know where I was before this life.

85
00:05:50,440 --> 00:05:51,000
 I don't know."

86
00:05:51,000 --> 00:05:55,400
 And then, "Do you know?

87
00:05:55,400 --> 00:05:56,040
 Don't you know?"

88
00:05:56,040 --> 00:05:58,680
 "Well, yes, I do know something.

89
00:05:58,680 --> 00:06:00,200
 I know I'm going to die.

90
00:06:00,200 --> 00:06:01,160
 So, yes, I do know."

91
00:06:01,160 --> 00:06:03,240
 And then, "Do you know?

92
00:06:03,240 --> 00:06:06,760
 Do I know when, how, where?"

93
00:06:06,760 --> 00:06:10,120
 Five things we don't know.

94
00:06:10,120 --> 00:06:13,000
 We don't know when we're going to die.

95
00:06:13,000 --> 00:06:15,320
 We don't know where we're going to die.

96
00:06:15,320 --> 00:06:17,000
 We don't know how we're going to die.

97
00:06:17,000 --> 00:06:20,040
 We don't know where our body is going to go.

98
00:06:20,040 --> 00:06:22,280
 And we don't know where our mind is going to go.

99
00:06:22,280 --> 00:06:25,720
 So, I don't know.

100
00:06:28,280 --> 00:06:29,560
 Death could come any time.

101
00:06:29,560 --> 00:06:30,440
 It could come today.

102
00:06:30,440 --> 00:06:32,840
 It could come tomorrow.

103
00:06:32,840 --> 00:06:37,880
 Who knows whether death could come tomorrow?

104
00:06:37,880 --> 00:06:41,400
 Nobody knows.

105
00:06:41,400 --> 00:06:42,840
 Well, very few people know.

106
00:06:42,840 --> 00:06:52,810
 And that's somewhat disturbing as a Buddhist, because are

107
00:06:52,810 --> 00:06:53,320
 you ready?

108
00:06:53,320 --> 00:06:56,200
 Ready for the big life change?

109
00:06:56,200 --> 00:06:57,560
 I mean, death isn't really death.

110
00:06:58,680 --> 00:07:00,120
 Death is just a big change.

111
00:07:00,120 --> 00:07:01,160
 It's a life change.

112
00:07:01,160 --> 00:07:03,480
 So, are you ready for that life change?

113
00:07:03,480 --> 00:07:06,920
 Ready for what comes next?

114
00:07:06,920 --> 00:07:17,400
 Most of us are not.

115
00:07:17,400 --> 00:07:22,440
 But this is what we do, and this is what meditation does.

116
00:07:22,440 --> 00:07:23,640
 It prepares you for death.

117
00:07:23,640 --> 00:32:34,550
 When you meditate, all of your life flashes before your

118
00:32:34,550 --> 00:07:27,080
 eyes.

119
00:07:27,080 --> 00:07:29,640
 Just as if you were going to die.

120
00:07:29,640 --> 00:07:31,550
 You see all sorts of things that you thought you'd

121
00:07:31,550 --> 00:07:32,360
 forgotten about.

122
00:07:32,360 --> 00:07:34,680
 Everything comes up.

123
00:07:34,680 --> 00:07:39,320
 And so, once you've worked it all out through meditation,

124
00:07:39,320 --> 00:07:42,840
 when you die, you're comfortable.

125
00:07:42,840 --> 00:07:46,840
 There's no surprises, because you've seen the ins and outs

126
00:07:46,840 --> 00:07:47,400
 of your mind,

127
00:07:47,400 --> 00:07:48,280
 the good and the bad.

128
00:07:48,280 --> 00:07:51,560
 So, you're much better prepared for whatever comes.

129
00:07:51,560 --> 00:07:55,640
 Once the body fades away, and all that's left is the mind,

130
00:07:57,320 --> 00:08:00,280
 you have to taste your own mind, then it's real meditation.

131
00:08:00,280 --> 00:08:02,440
 Then it becomes real.

132
00:08:02,440 --> 00:08:10,280
 All you're left with is your good and bad deeds.

133
00:08:10,280 --> 00:08:22,440
 So, this week's kind of quiet, but just got,

134
00:08:22,440 --> 00:08:24,680
 just had a video conference with my family, and

135
00:08:26,680 --> 00:08:27,960
 talking about things.

136
00:08:27,960 --> 00:08:30,280
 I don't know.

137
00:08:30,280 --> 00:08:32,920
 They want me to come and stay.

138
00:08:32,920 --> 00:08:38,920
 But my brother's coming back from Taiwan on Saturday.

139
00:08:38,920 --> 00:08:44,920
 And the things they're talking about,

140
00:08:44,920 --> 00:08:48,760
 he wants a case of beer ready for him when he arrives.

141
00:08:48,760 --> 00:08:51,320
 I mean, it's not shocking.

142
00:08:51,320 --> 00:08:54,520
 It's just like, what am I going to do with these people?

143
00:08:55,480 --> 00:08:56,600
 How can I relate to them?

144
00:08:56,600 --> 00:09:00,730
 And like last week, my stepmother was talking about having

145
00:09:00,730 --> 00:09:01,720
 a big pot of,

146
00:09:01,720 --> 00:09:04,300
 she was drunk at the time, and she was talking about having

147
00:09:04,300 --> 00:09:05,160
 a big pot of weed,

148
00:09:05,160 --> 00:09:07,640
 big bag of weed ready for him.

149
00:09:07,640 --> 00:09:10,120
 Of marijuana ready for him.

150
00:09:10,120 --> 00:09:13,640
 So, I don't know what I'm going to do.

151
00:09:13,640 --> 00:09:17,510
 I was kind of, I suppose, a little bit snarky on the hang

152
00:09:17,510 --> 00:09:18,120
out.

153
00:09:18,120 --> 00:09:21,320
 I said, "Oh, I'm sorry you're not going to be..."

154
00:09:21,320 --> 00:09:24,040
 Because the thing is, this weekend, I'm actually busy,

155
00:09:24,040 --> 00:09:29,960
 and I've accepted some appointments because, I don't know,

156
00:09:29,960 --> 00:09:31,640
 I think they're maybe more important.

157
00:09:31,640 --> 00:09:34,520
 So, Saturday night, I'm giving a talk in Mississauga.

158
00:09:34,520 --> 00:09:36,960
 You're all welcome to come if you're in the area at the

159
00:09:36,960 --> 00:09:40,280
 West End Buddhist Monastery.

160
00:09:40,280 --> 00:09:45,330
 And then Sunday, we have this interfaith meeting with the

161
00:09:45,330 --> 00:09:46,600
 Interfaith Group in Hamilton.

162
00:09:46,600 --> 00:09:53,000
 And then Monday, Monday morning, we're going to New York.

163
00:09:53,640 --> 00:09:57,240
 I think I mentioned this, but I'm going to drive to Bekub

164
00:09:57,240 --> 00:09:58,760
odi's monastery.

165
00:09:58,760 --> 00:10:03,560
 And I just got a call this morning from the head monk in St

166
00:10:03,560 --> 00:10:04,600
ony Creek,

167
00:10:04,600 --> 00:10:08,040
 and he's driving to New York Monday morning.

168
00:10:08,040 --> 00:10:10,120
 So, he's going to drive with us.

169
00:10:10,120 --> 00:10:12,360
 He was going to drive all the way.

170
00:10:12,360 --> 00:10:16,680
 So, I said, "Well, gee, that's, it's bizarre really.

171
00:10:16,680 --> 00:10:19,240
 Me and him have some kind of really strong connection.

172
00:10:19,240 --> 00:10:21,480
 This isn't coincidence."

173
00:10:21,480 --> 00:10:23,320
 I said to him, "This isn't a coincidence."

174
00:10:23,320 --> 00:10:26,460
 And he said, "No, we're both going to New York on the same

175
00:10:26,460 --> 00:10:26,920
 day.

176
00:10:26,920 --> 00:10:30,440
 We've been very close for a long time, even though

177
00:10:30,440 --> 00:10:34,450
 always felt like he's very, he's, we must have known each

178
00:10:34,450 --> 00:10:35,240
 other before.

179
00:10:35,240 --> 00:10:36,760
 He's someone I can say that about.

180
00:10:36,760 --> 00:10:39,480
 Not too many people I can say that about.

181
00:10:39,480 --> 00:10:41,800
 He's one of them."

182
00:10:41,800 --> 00:10:49,290
 So, he'll be coming with us, and we're going to go see Bek

183
00:10:49,290 --> 00:10:49,720
ubod,

184
00:10:49,720 --> 00:10:51,720
 go and meet Bekubodi for the first time.

185
00:10:52,440 --> 00:10:57,080
 So, there's all that.

186
00:10:57,080 --> 00:11:04,440
 Right.

187
00:11:04,440 --> 00:11:05,560
 Anybody have questions?

188
00:11:05,560 --> 00:11:06,920
 You should go ahead.

189
00:11:06,920 --> 00:11:12,920
 Click the green, click the green question mark,

190
00:11:12,920 --> 00:11:15,400
 and then write in your question.

191
00:11:15,400 --> 00:11:25,400
 [silence]

192
00:11:27,400 --> 00:11:35,400
 [silence]

193
00:11:35,400 --> 00:11:45,400
 [silence]

194
00:11:45,400 --> 00:11:53,400
 [silence]

195
00:11:53,400 --> 00:12:03,400
 [silence]

196
00:12:03,400 --> 00:12:07,960
 [silence]

197
00:12:07,960 --> 00:12:11,240
 How do you meditate with more than one's khanda?

198
00:12:11,240 --> 00:12:12,120
 Khanda you mean.

199
00:12:12,120 --> 00:12:16,040
 I mean, khanda is the same word, right?

200
00:12:16,040 --> 00:12:19,400
 I don't understand.

201
00:12:23,160 --> 00:12:24,920
 I'm sorry, I don't understand the question.

202
00:12:24,920 --> 00:12:29,880
 It could mean many different things.

203
00:12:29,880 --> 00:12:41,960
 [silence]

204
00:12:41,960 --> 00:12:47,720
 This month is Wesak, Wesaka Puja, the full moon of Wesaka.

205
00:12:47,720 --> 00:12:51,960
 On the 28th, we're having a big celebration in Mississauga

206
00:12:51,960 --> 00:12:53,400
 Celebration Square.

207
00:12:53,400 --> 00:12:57,240
 If you're in the Toronto area, come on out.

208
00:12:57,240 --> 00:12:59,000
 Maybe we'll have a meditation tent.

209
00:12:59,000 --> 00:13:02,440
 I think I'm giving a talk.

210
00:13:02,440 --> 00:13:06,260
 There was some talk of me giving a talk, so we'll see if

211
00:13:06,260 --> 00:13:08,440
 that happens.

212
00:13:08,440 --> 00:13:09,480
 It's okay if it doesn't.

213
00:13:09,480 --> 00:13:12,920
 I'll just sit and put up our meditation sign.

214
00:13:14,840 --> 00:13:18,120
 Sight, sound, taste at the same time.

215
00:13:18,120 --> 00:13:19,960
 Well, those are actually the same khanda.

216
00:13:19,960 --> 00:13:21,880
 Those are the rupakhanda.

217
00:13:21,880 --> 00:13:25,800
 Sight, sounds, and tastes are all rupakhanda.

218
00:13:25,800 --> 00:13:31,800
 But there's no such thing.

219
00:13:31,800 --> 00:13:33,640
 They can't happen at the same time.

220
00:13:33,640 --> 00:13:36,360
 You focus on whatever's present at that moment.

221
00:13:36,360 --> 00:13:40,680
 I mean, practically speaking, this means just pick one.

222
00:13:43,400 --> 00:13:44,840
 Pick whichever one is clearest.

223
00:13:44,840 --> 00:13:47,400
 Don't worry about, "Is this one at this moment?

224
00:13:47,400 --> 00:13:48,360
 Is this one?"

225
00:13:48,360 --> 00:13:51,240
 If something's happening, then just if you see something,

226
00:13:51,240 --> 00:13:51,880
 say, "Seeing."

227
00:13:51,880 --> 00:13:53,160
 If you hear something, say, "Here."

228
00:13:53,160 --> 00:13:55,400
 Doesn't really matter which one.

229
00:13:55,400 --> 00:14:03,320
 Occasional contemplation of death is a meditation practice.

230
00:14:03,320 --> 00:14:09,400
 You can look up my video on protective meditations.

231
00:14:09,400 --> 00:14:12,280
 There's actually four types of meditation that protect you.

232
00:14:13,000 --> 00:14:14,520
 Protect your insight meditation.

233
00:14:14,520 --> 00:14:15,880
 Recommend looking at that video.

234
00:14:15,880 --> 00:14:24,520
 Maybe one of my faithful followers here can link it.

235
00:14:24,520 --> 00:14:26,540
 Because there's some people who are so good at linking

236
00:14:26,540 --> 00:14:26,920
 videos.

237
00:14:26,920 --> 00:14:30,440
 Find on the four protective meditations.

238
00:14:30,440 --> 00:14:36,200
 I'll give you an answer to that question.

239
00:14:36,200 --> 00:14:39,240
 There's also, if you don't know, there's a website called

240
00:14:39,240 --> 00:14:41,240
 video.siri-mongolo.org.

241
00:14:41,960 --> 00:14:43,000
 I think it's still there.

242
00:14:43,000 --> 00:14:48,840
 And it has, oops, I just lost it.

243
00:14:48,840 --> 00:14:54,760
 Yeah, it has quite a few of my videos categorized.

244
00:14:54,760 --> 00:14:57,850
 I don't think it's up to date, but it has a lot of the

245
00:14:57,850 --> 00:14:58,360
 older ones.

246
00:14:58,360 --> 00:15:01,800
 Back when I was doing Ask a Monk and that kind of thing.

247
00:15:09,560 --> 00:15:22,140
 Question regarding, quote, oh, we got some happy birthday

248
00:15:22,140 --> 00:15:24,600
 stuff.

249
00:15:24,600 --> 00:15:32,200
 Why does the doer delight?

250
00:15:32,200 --> 00:15:34,680
 Isn't it best to give without thoughts of return?

251
00:15:34,680 --> 00:15:39,240
 Yes, but that's because when you do that,

252
00:15:39,240 --> 00:15:40,200
 you feel happy.

253
00:15:40,200 --> 00:15:47,680
 It's kind of ironic, actually, kind of funny if you're

254
00:15:47,680 --> 00:15:49,320
 looking for happiness.

255
00:15:49,320 --> 00:15:52,600
 No, it's not best to give without thoughts of return.

256
00:15:52,600 --> 00:15:54,440
 If you're giving without thoughts of return, it's kind of

257
00:15:54,440 --> 00:15:54,840
 stupid.

258
00:15:54,840 --> 00:15:56,600
 Why are you giving?

259
00:15:56,600 --> 00:15:57,480
 What is the purpose?

260
00:15:57,480 --> 00:16:00,040
 You're giving to help the other person?

261
00:16:00,040 --> 00:16:00,840
 How ridiculous.

262
00:16:00,840 --> 00:16:05,630
 If everyone give to help everyone else, then no one would

263
00:16:05,630 --> 00:16:06,200
 ever benefit.

264
00:16:06,200 --> 00:16:08,600
 No one would ever benefit from giving.

265
00:16:10,600 --> 00:16:12,680
 We give because it makes us happy.

266
00:16:12,680 --> 00:16:16,520
 Altruism is wrong.

267
00:16:16,520 --> 00:16:19,880
 Altruism is problematic.

268
00:16:19,880 --> 00:16:24,440
 And it's not real, anyway.

269
00:16:24,440 --> 00:16:28,700
 The only people who say they're altruistic are actually

270
00:16:28,700 --> 00:16:31,720
 doing it because it makes them happy,

271
00:16:31,720 --> 00:16:33,560
 or it makes them feel good about themselves,

272
00:16:33,560 --> 00:16:35,240
 or it makes them feel proud of themselves.

273
00:16:35,240 --> 00:16:37,960
 And boy, I'm so altruistic, that kind of thing.

274
00:16:39,080 --> 00:16:41,000
 But normally, just because it makes them happy,

275
00:16:41,000 --> 00:16:44,040
 because it maintains their happiness,

276
00:16:44,040 --> 00:16:47,570
 because it prevents them from suffering, from the pain of

277
00:16:47,570 --> 00:16:49,800
 jealousy and stinginess and so on.

278
00:16:49,800 --> 00:17:03,240
 Well, just Google "yuttadammo" protective meditation.

279
00:17:03,240 --> 00:17:05,080
 I think that's what it's called.

280
00:17:06,520 --> 00:17:09,720
 But I could find it for you.

281
00:17:09,720 --> 00:17:22,280
 Yeah, I mean, happiness is not all associated with greed.

282
00:17:22,280 --> 00:17:25,000
 There's happiness that is associated with greed,

283
00:17:25,000 --> 00:17:27,480
 and there's happiness that's associated with wholesomeness.

284
00:17:27,480 --> 00:17:32,040
 You can do a wholesome deed happily.

285
00:17:33,000 --> 00:17:34,920
 What is pervasive suffering?

286
00:17:34,920 --> 00:17:36,680
 I don't know.

287
00:17:36,680 --> 00:17:39,880
 Pervasive.

288
00:17:39,880 --> 00:17:46,600
 Pervasive means constant, right?

289
00:17:46,600 --> 00:17:47,960
 It's like it's...

290
00:17:47,960 --> 00:17:52,280
 Everywhere.

291
00:17:52,280 --> 00:17:54,600
 All-encompassing.

292
00:17:54,600 --> 00:17:59,880
 Let's get a clear understanding of the word pervasive.

293
00:17:59,880 --> 00:18:03,160
 Spreading widely throughout, right?

294
00:18:03,160 --> 00:18:11,160
 Now the khandas are dukkha, so they're pretty pervasive.

295
00:18:11,160 --> 00:18:16,680
 But I don't know what you're referring to.

296
00:18:16,680 --> 00:18:18,200
 You have to give me a source.

297
00:18:18,200 --> 00:18:23,000
 Where are you getting this word from?

298
00:18:28,920 --> 00:18:38,620
 [

299
00:18:38,620 --> 00:18:40,120
 Really, nobody can find her, there's no one looking.]

300
00:18:40,120 --> 00:18:42,280
 Yuttadammo...

301
00:18:42,280 --> 00:18:45,000
 protective...

302
00:18:45,000 --> 00:18:47,720
 meditation.

303
00:18:47,720 --> 00:18:52,520
 It's not even there.

304
00:18:52,520 --> 00:18:56,920
 Yuttadammo protection.

305
00:18:58,920 --> 00:19:00,920
 No, I think it's other meditation.

306
00:19:00,920 --> 00:19:03,160
 Right? No.

307
00:19:03,160 --> 00:19:04,680
 It's not even there.

308
00:19:04,680 --> 00:19:11,320
 Other useful meditation practices.

309
00:19:11,320 --> 00:19:12,600
 Maybe it's not a very good video.

310
00:19:12,600 --> 00:19:15,880
 Maybe it's got a lot of down-

311
00:19:15,880 --> 00:19:17,720
 72 up for today's quest.

312
00:19:17,720 --> 00:19:18,440
 Probably, okay.

313
00:19:20,440 --> 00:19:20,940
 Yeah.

314
00:19:20,940 --> 00:19:29,720
 Yeah, I know I have to find it because I remember how I

315
00:19:29,720 --> 00:19:32,880
 titled it other useful meditation practices or something

316
00:19:32,880 --> 00:19:33,640
 like that.

317
00:19:33,640 --> 00:19:41,160
 Simon, you're usually pretty good at that.

318
00:19:41,160 --> 00:19:42,200
 You missed this one.

319
00:19:42,200 --> 00:19:44,920
 I kind of set you on the wild goose chase.

320
00:19:44,920 --> 00:19:45,960
 It wasn't protective.

321
00:19:45,960 --> 00:19:48,200
 I didn't label it as protective.

322
00:19:50,200 --> 00:19:52,600
 Yeah, those are the four.

323
00:19:52,600 --> 00:19:55,240
 You know what they are.

324
00:19:55,240 --> 00:20:03,560
 I can't remember whether it was my teacher or

325
00:20:04,760 --> 00:20:19,560
 my mom that I heard these from first.

326
00:20:19,560 --> 00:20:46,520
 Okay, well, we're not Tibetan Buddhists, so better not to

327
00:20:46,520 --> 00:20:46,920
 mix.

328
00:20:48,920 --> 00:20:51,640
 Go ask them about their explanations.

329
00:20:51,640 --> 00:20:54,920
 We follow Theravada Buddhism here, so if you want.

330
00:20:54,920 --> 00:20:58,520
 I mean, if you're new, I suggest you read my booklet on how

331
00:20:58,520 --> 00:20:59,240
 to meditate.

332
00:20:59,240 --> 00:21:02,120
 It's linked at the top of this page.

333
00:21:02,120 --> 00:21:05,000
 Check out some of my videos on YouTube.

334
00:21:05,000 --> 00:21:08,520
 That's- I mean, I'm a fairly- I have a fairly specific

335
00:21:08,520 --> 00:21:10,600
 focus, and

336
00:21:10,600 --> 00:21:14,920
 so you have to kind of be into what I do to be interested

337
00:21:14,920 --> 00:21:16,200
 in the things I'm going to say.

338
00:21:17,320 --> 00:21:21,240
 I wouldn't say I'm very esoteric or specific.

339
00:21:21,240 --> 00:21:23,960
 Well, specific maybe, but fairly straight.

340
00:21:23,960 --> 00:21:28,390
 It's not like I have specific things I want you to

341
00:21:28,390 --> 00:21:31,080
 visualize or something, but fairly basic,

342
00:21:31,080 --> 00:21:43,160
 fundamental.

343
00:21:46,040 --> 00:21:48,840
 Do you remember what initially helped you keep constant

344
00:21:48,840 --> 00:21:49,880
 sense restraint?

345
00:21:49,880 --> 00:21:52,040
 Keeping your head down.

346
00:21:52,040 --> 00:21:55,800
 Not looking around.

347
00:21:55,800 --> 00:21:59,030
 You know, monks have kind of a rule that when we walk up in

348
00:21:59,030 --> 00:21:59,320
 the world,

349
00:21:59,320 --> 00:22:00,840
 we're not supposed to look at things.

350
00:22:00,840 --> 00:22:11,880
 But mindfulness is of course the best.

351
00:22:11,880 --> 00:22:22,040
 If you want to learn about sense restraint, there's some

352
00:22:22,040 --> 00:22:23,560
 interesting stuff in the Visuddhi

353
00:22:23,560 --> 00:22:26,440
 Maga I think under Sila Nideasa.

354
00:22:26,440 --> 00:22:30,460
 In the first part of the Visuddhi Maga, look up- because

355
00:22:30,460 --> 00:22:31,880
 morality is fourfold.

356
00:22:31,880 --> 00:22:34,280
 Look up fourfold morality.

357
00:22:34,280 --> 00:22:38,870
 I may have even done a video on that actually, but it talks

358
00:22:38,870 --> 00:22:41,240
 a little bit about sense restraint.

359
00:22:42,840 --> 00:22:46,640
 It'll give you some sort of fleshed out idea of sense

360
00:22:46,640 --> 00:22:50,280
 restraint in different kinds and

361
00:22:50,280 --> 00:22:54,200
 different ways and give some stories of monks who practice

362
00:22:54,200 --> 00:22:55,640
 sense restraint.

363
00:22:55,640 --> 00:23:03,170
 There was this monk who lived in a cave and he never looked

364
00:23:03,170 --> 00:23:03,880
 up.

365
00:23:06,360 --> 00:23:09,500
 He only knew what season it was by the flowers, that they

366
00:23:09,500 --> 00:23:11,080
 had these flowering trees.

367
00:23:11,080 --> 00:23:14,420
 When he saw the flowers on the ground, then he knew it was

368
00:23:14,420 --> 00:23:15,160
 spring.

369
00:23:15,160 --> 00:23:20,350
 These monks came to visit him once and they noticed that

370
00:23:20,350 --> 00:23:22,760
 painted on the walls of his cave

371
00:23:22,760 --> 00:23:27,170
 were all these beautiful murals and pictures of the Buddha

372
00:23:27,170 --> 00:23:30,440
's various stages of the Buddha's life.

373
00:23:30,440 --> 00:23:31,880
 They said, "Wow, these are impressive."

374
00:23:33,080 --> 00:23:36,120
 He said to them, "Oh, I never noticed."

375
00:23:36,120 --> 00:23:39,370
 He'd lived there for years and he'd never even noticed that

376
00:23:39,370 --> 00:23:41,320
 there was anything on the wall.

377
00:23:41,320 --> 00:23:46,760
 The king heard about this and he invited him to come and

378
00:23:46,760 --> 00:23:49,800
 give a talk and the monk refused.

379
00:23:49,800 --> 00:24:00,460
 The king made an proclamation in order to have all the

380
00:24:00,460 --> 00:24:02,520
 women in the kingdom had to have their

381
00:24:02,520 --> 00:24:12,170
 breasts tied, sewn up with cloth and it was against the law

382
00:24:12,170 --> 00:24:15,560
 to feed their babies

383
00:24:15,560 --> 00:24:18,120
 until this monk came down.

384
00:24:18,120 --> 00:24:22,930
 His way of forcing the monk to come down was to starve all

385
00:24:22,930 --> 00:24:25,240
 the babies in the kingdom.

386
00:24:25,240 --> 00:24:39,190
 Finally, the monk came down of compassion for these poor

387
00:24:39,190 --> 00:24:42,040
 babies and their mothers.

388
00:24:42,040 --> 00:24:49,000
 He came to see the king, the king and the queen.

389
00:24:49,000 --> 00:24:52,010
 He came before the king and the queen and he said, "When

390
00:24:52,010 --> 00:24:53,720
 the king came and paid respect to him,

391
00:24:53,720 --> 00:24:57,120
 he said, 'May the king long live the king.' When the queen

392
00:24:57,120 --> 00:24:58,520
 came and paid respect to him,

393
00:24:58,520 --> 00:24:59,560
 he said, 'Long live the king.'"

394
00:24:59,560 --> 00:25:05,560
 They asked him, "Why do you say long live the king twice?"

395
00:25:05,560 --> 00:25:08,040
 He said, "Well, I don't know which one of you it is."

396
00:25:08,040 --> 00:25:10,920
 He didn't even look up to see which one it was.

397
00:25:13,240 --> 00:25:26,040
 To be safe, he just said, "Long live the king." "May the

398
00:25:26,040 --> 00:25:26,040
 king live long" is something like that.

399
00:25:26,040 --> 00:25:29,400
 A brief explanation of how we are dying and being born from

400
00:25:29,400 --> 00:25:30,440
 moment to moment.

401
00:25:30,440 --> 00:25:36,120
 Well, have you read my booklet on how to meditate?

402
00:25:36,120 --> 00:25:39,240
 Recommend that as a start.

403
00:25:42,920 --> 00:25:45,430
 I'm not going to explain it to you. I think you have to

404
00:25:45,430 --> 00:25:46,200
 experience it.

405
00:25:46,200 --> 00:25:54,120
 Read my booklet, start practicing and find it out that way.

406
00:25:54,120 --> 00:26:00,590
 Is there ever an exception to right speech livelihoods

407
00:26:00,590 --> 00:26:01,640
 actions?

408
00:26:01,640 --> 00:26:07,960
 I don't understand.

409
00:26:10,520 --> 00:26:13,430
 Like you mean, can you lie and it still be good? Because

410
00:26:13,430 --> 00:26:14,120
 you can't.

411
00:26:14,120 --> 00:26:15,080
 I'm Theravada.

412
00:26:15,080 --> 00:26:21,000
 Can the primary elements be noticed with hearing, seeing,

413
00:26:21,000 --> 00:26:21,880
 smelling as well?

414
00:26:21,880 --> 00:26:25,800
 Or can we only notice secondary elements with them?

415
00:26:25,800 --> 00:26:30,200
 Why, Sangha? Why do you give me these tough ones?

416
00:26:30,200 --> 00:26:38,120
 They're derived. Except for the physical.

417
00:26:39,080 --> 00:26:45,300
 The physical is not derived. The physical, you feel the

418
00:26:45,300 --> 00:26:46,200
 direct.

419
00:26:46,200 --> 00:26:51,080
 You're asking with hearing, seeing, smelling as well. No,

420
00:26:51,080 --> 00:26:56,040
 they are, they have a special name.

421
00:26:56,040 --> 00:27:03,160
 The Pasadas, right? The Chakupasada.

422
00:27:03,160 --> 00:27:06,920
 Pasada means sensitivity or something like that.

423
00:27:08,920 --> 00:27:09,880
 So it's not direct.

424
00:27:09,880 --> 00:27:16,000
 But it's based on, you just don't, it's right, you don't,

425
00:27:16,000 --> 00:27:20,120
 when you smell, you don't, you're not

426
00:27:20,120 --> 00:27:23,630
 smelling the particles. You're smelling that which is

427
00:27:23,630 --> 00:27:26,440
 derived from the contact of the particles.

428
00:27:26,440 --> 00:27:29,720
 But it's still part, you know, it's still the, it's still R

429
00:27:29,720 --> 00:27:31,960
upa. But no, it's not. It's not the

430
00:27:31,960 --> 00:27:37,560
 primaries. It's derived.

431
00:27:37,560 --> 00:27:49,130
 The whole story for the Manko Great Sense Restraint, I

432
00:27:49,130 --> 00:27:52,120
 think it's in the Vasudh

433
00:27:52,120 --> 00:27:53,480
 Dhammanga. I think that's where I'm remembering it from.

434
00:27:53,480 --> 00:27:55,960
 Yeah, yeah, it's from the Vasudh Dhammanga.

435
00:27:55,960 --> 00:27:58,120
 I'm not sure if the whole thing is or if I've heard it

436
00:27:58,120 --> 00:28:01,160
 elsewhere as well, but I'm pretty sure.

437
00:28:02,200 --> 00:28:05,890
 Check out the, the end of the Sila Nidhasa, the end of the

438
00:28:05,890 --> 00:28:09,160
 first part of the Vasudh Dhammanga.

439
00:28:09,160 --> 00:28:13,960
 The Path of Purification in English. It's on the internet.

440
00:28:13,960 --> 00:28:19,740
 I don't answer questions about my own attainment. That

441
00:28:19,740 --> 00:28:23,800
 would be against the monastic rules.

442
00:28:23,800 --> 00:28:28,530
 It's just not appropriate. Nor do I think it's all that

443
00:28:28,530 --> 00:28:29,640
 helpful.

444
00:28:29,640 --> 00:28:41,240
 Sangha, you should get the Bhikkhu Bodhi's

445
00:28:41,240 --> 00:28:47,320
 compendium of Abhidhamma. Is it called?

446
00:28:47,320 --> 00:28:53,660
 Well, the Abhidhammata Sangha had a translation and

447
00:28:53,660 --> 00:28:56,600
 explanation of the Abhidhammata Sangha.

448
00:28:56,600 --> 00:29:10,520
 Good for you, one. Well done. Sounds like you're doing the

449
00:29:10,520 --> 00:29:11,000
 right thing.

450
00:29:11,000 --> 00:29:14,120
 But also use mindfulness, you know, if you haven't read my

451
00:29:14,120 --> 00:29:15,240
 booklet, read my booklet,

452
00:29:15,240 --> 00:29:20,590
 you might find it useful. That kind of meditation is quite

453
00:29:20,590 --> 00:29:22,440
 useful for keep guarding the senses.

454
00:29:22,440 --> 00:29:42,280
 Yeah.

455
00:29:50,200 --> 00:29:52,840
 Singhala Abhidhamma, I guess probably,

456
00:29:52,840 --> 00:29:55,880
 Kathukurundinyan, no, no, no, no, no.

457
00:29:55,880 --> 00:30:02,510
 Rheorukarni, what's his name? Rheorukarni Sandoheemana? No,

458
00:30:02,510 --> 00:30:03,880
 Rheorukarni, yeah,

459
00:30:03,880 --> 00:30:05,860
 I think something like that. You know who I'm talking about

460
00:30:05,860 --> 00:30:06,040
.

461
00:30:06,040 --> 00:30:18,030
 No, I didn't know Sangha when I was in Sri Lanka the first

462
00:30:18,030 --> 00:30:19,640
 time. I don't think we ever met, right?

463
00:30:19,640 --> 00:30:23,690
 Did we? I don't think we met when I was in Sri Lanka the

464
00:30:23,690 --> 00:30:25,560
 first time. Or am I forgetting?

465
00:30:25,560 --> 00:30:31,720
 I don't think so. I didn't meet Sangha until I went back,

466
00:30:31,720 --> 00:30:32,680
 right?

467
00:30:32,680 --> 00:30:37,960
 I met Sangha on the internet actually.

468
00:30:44,920 --> 00:30:49,570
 Rheorukarni Sandoheemana, yeah, he's apparently really good

469
00:30:49,570 --> 00:30:51,560
. I got all of his books intending

470
00:30:51,560 --> 00:30:55,080
 to learn singhalis to read them and never didn't get around

471
00:30:55,080 --> 00:30:56,760
 to learning singhalis.

472
00:30:56,760 --> 00:31:00,840
 Not well enough, anyway.

473
00:31:00,840 --> 00:31:28,680
 [Silence]

474
00:31:28,680 --> 00:31:33,240
 Okay, well then that's all for tonight. Thank you all for

475
00:31:33,240 --> 00:31:38,520
 tuning in. See you all next time.

476
00:31:38,520 --> 00:31:53,160
 Oops. You don't have to stay up here.

477
00:31:54,280 --> 00:31:56,280
 I just had to quit if you want.

478
00:31:56,280 --> 00:31:58,280
 You know what to do.

479
00:31:58,280 --> 00:32:09,400
 So we're going to be four of us going on a Saturday,

480
00:32:09,400 --> 00:32:13,800
 a Saturday enough to come and listen to the talk.

481
00:32:19,800 --> 00:32:23,800
 We're going to the airport to see my brother.

482
00:32:23,800 --> 00:32:31,800
 [Inaudible]

483
00:32:31,800 --> 00:32:33,800
 First thing he says when he gets on the hagat.

