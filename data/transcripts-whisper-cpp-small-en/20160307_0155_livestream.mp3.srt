1
00:00:00,000 --> 00:00:10,000
 [Silence]

2
00:00:10,000 --> 00:00:15,000
 Good evening everyone.

3
00:00:15,000 --> 00:00:31,000
 Broadcasting Live, Mark 6.

4
00:00:31,000 --> 00:00:41,000
 Today's quote is about the five hindrances.

5
00:00:41,000 --> 00:00:54,000
 These are our namasai.

6
00:00:54,000 --> 00:01:04,000
 These are the bad guys.

7
00:01:04,000 --> 00:01:12,940
 So we set out on this journey to find clarity of mind, to

8
00:01:12,940 --> 00:01:26,000
 find enlightenment, to find the truth.

9
00:01:26,000 --> 00:01:35,620
 And we work so hard to see the truth, to discover the truth

10
00:01:35,620 --> 00:01:41,000
, to investigate the truth.

11
00:01:41,000 --> 00:01:48,000
 Sometimes we don't see the truth.

12
00:01:48,000 --> 00:01:52,000
 Sometimes we can't see the truth.

13
00:01:52,000 --> 00:01:57,000
 There are reasons why we can't see the truth.

14
00:01:57,000 --> 00:02:02,000
 And we can't find the truth.

15
00:02:02,000 --> 00:02:07,000
 And that's what this quote is all about.

16
00:02:07,000 --> 00:02:14,000
 Think of the mind as a bowl of water.

17
00:02:14,000 --> 00:02:22,630
 Or you think of those tide pools by the ocean. If you look

18
00:02:22,630 --> 00:02:25,850
 into the tide pool, when I was 13 we drove along the

19
00:02:25,850 --> 00:02:27,000
 California coast.

20
00:02:27,000 --> 00:02:32,680
 I remember looking at these tide pools full of hermit crabs

21
00:02:32,680 --> 00:02:40,000
 and starfish and all sorts of interesting things.

22
00:02:40,000 --> 00:02:45,000
 But the thing is the water has to be clear.

23
00:02:45,000 --> 00:02:59,000
 If the water is all stirred up, the water is polluted.

24
00:02:59,000 --> 00:03:11,190
 There are only a number of things. There are these five

25
00:03:11,190 --> 00:03:11,720
 conditions of the mind that are like the five conditions of

26
00:03:11,720 --> 00:03:12,000
 a bowl of water.

27
00:03:12,000 --> 00:03:23,720
 If a bowl of water is mixed up with black turmeric, blue or

28
00:03:23,720 --> 00:03:26,000
 yellow dye,

29
00:03:26,000 --> 00:03:30,250
 you wouldn't be able to see into the pool here. It's

30
00:03:30,250 --> 00:03:32,000
 talking about reflection.

31
00:03:32,000 --> 00:03:36,120
 If you look into that pool of water and try and see your

32
00:03:36,120 --> 00:03:38,000
 reflection in the pool of water,

33
00:03:38,000 --> 00:03:46,000
 it's all full of turmeric or dye or something.

34
00:03:46,000 --> 00:04:04,000
 And you wouldn't see a reflection the way it truly was.

35
00:04:04,000 --> 00:04:09,350
 In the same way if the mind is obsessed by sensual desires,

36
00:04:09,350 --> 00:04:14,250
 desires for pleasant sights and sounds and smells and

37
00:04:14,250 --> 00:04:23,000
 tastes and feelings.

38
00:04:23,000 --> 00:04:26,630
 Again, you can't tell right from wrong. You can't see the

39
00:04:26,630 --> 00:04:31,000
 truth about yourself and about the world.

40
00:04:31,000 --> 00:04:35,290
 You can't see the truth about things. You get deluded by

41
00:04:35,290 --> 00:04:42,000
 things, confused by things, all mixed up.

42
00:04:42,000 --> 00:04:48,910
 So you can't see clearly. The water is not clear. The water

43
00:04:48,910 --> 00:04:57,000
 of the mind is not clear.

44
00:04:57,000 --> 00:05:01,000
 What if the water is heated up to boiling and bubbling?

45
00:05:01,000 --> 00:05:05,000
 Then you're to look at this water and think,

46
00:05:05,000 --> 00:05:09,000
 "Hmm, maybe I'll see what my face looks like."

47
00:05:09,000 --> 00:05:22,810
 If the pot of water is boiling and bubbling, you won't be

48
00:05:22,810 --> 00:05:28,000
 able to see much of anything, not alone in your face.

49
00:05:28,000 --> 00:05:35,060
 You would liken this to anger. If your mind is full of

50
00:05:35,060 --> 00:05:40,350
 anger and ill-will, how could you possibly understand the

51
00:05:40,350 --> 00:05:41,000
 truth?

52
00:05:41,000 --> 00:05:47,910
 How could you see clearly? Your mind is a flame, is boiling

53
00:05:47,910 --> 00:05:52,000
 over its rage and anger.

54
00:05:52,000 --> 00:05:56,560
 You can't see the truth. You can't tell truth from

55
00:05:56,560 --> 00:06:01,000
 falsehood, right from wrong, good from bad.

56
00:06:01,000 --> 00:06:19,000
 All you know is what you like and dislike.

57
00:06:19,000 --> 00:06:26,000
 Suppose the water was overgrown with plants, water plants.

58
00:06:26,000 --> 00:06:30,030
 Then you come to this pool of water and thinking, "Oh, look

59
00:06:30,030 --> 00:06:34,000
 and see how my reflection looks like."

60
00:06:34,000 --> 00:06:37,280
 You come upon it and, "Oh, you can't see anything because

61
00:06:37,280 --> 00:06:39,000
 it's overgrown with water."

62
00:06:39,000 --> 00:06:50,930
 Let's go overgrown with plants. This is like when the mind

63
00:06:50,930 --> 00:07:01,000
 is caught up in laziness, sloth, torpor, drowsiness.

64
00:07:01,000 --> 00:07:06,340
 You can't see anything in the mind. So the mind doesn't

65
00:07:06,340 --> 00:07:10,890
 have the effort to go out to the object, to be mindful of

66
00:07:10,890 --> 00:07:14,000
 the objects as they arise.

67
00:07:14,000 --> 00:07:20,150
 You see the objects as they are. Note them, the energy that

68
00:07:20,150 --> 00:07:25,000
 it takes to note the object is not there.

69
00:07:25,000 --> 00:07:34,000
 It gets lazy. You won't see anything.

70
00:07:34,000 --> 00:07:40,020
 Suppose the water was stirred up by the wind and there were

71
00:07:40,020 --> 00:07:44,000
 waves and ripples caused by the wind.

72
00:07:44,000 --> 00:07:49,440
 Same way if anyone looks in that pool, there's no question

73
00:07:49,440 --> 00:07:54,000
 they're not going to see their reflection as it is.

74
00:07:54,000 --> 00:07:58,600
 And they would have likened this to if the mind is full of

75
00:07:58,600 --> 00:08:01,000
 restlessness and worry.

76
00:08:01,000 --> 00:08:05,660
 If you're restless and you're not focused on one thing and

77
00:08:05,660 --> 00:08:15,000
 your mind is running hither and thither, caught up,

78
00:08:15,000 --> 00:08:24,020
 and you end up with thoughts of the past and the future or

79
00:08:24,020 --> 00:08:30,000
 worried about the past or the future,

80
00:08:30,000 --> 00:08:35,000
 the mind isn't settled enough. If the mind isn't settled,

81
00:08:35,000 --> 00:08:37,000
 you can't see anything.

82
00:08:37,000 --> 00:08:45,550
 It's sitting about like a butterfly. You can't focus on

83
00:08:45,550 --> 00:08:49,000
 anything. You can't understand anything clearly.

84
00:08:49,000 --> 00:08:54,840
 So you do have to focus on everything. Be conscientious

85
00:08:54,840 --> 00:08:58,000
 about everything that arises.

86
00:08:58,000 --> 00:09:04,270
 Treat everything that arises like a guest. Don't dismiss

87
00:09:04,270 --> 00:09:08,000
 them. Don't overlook the guests.

88
00:09:08,000 --> 00:09:12,100
 You have to see whatever guest, just like a good host, has

89
00:09:12,100 --> 00:09:13,000
 to see what the guest needs.

90
00:09:13,000 --> 00:09:18,890
 You have to stop and greet the guest. It could be a bad

91
00:09:18,890 --> 00:09:22,240
 person or a good person, but you won't know unless you

92
00:09:22,240 --> 00:09:24,000
 greet them.

93
00:09:24,000 --> 00:09:30,140
 You have to take the time to check them out. So you have to

94
00:09:30,140 --> 00:09:34,790
 be conscientious with every experience, not being

95
00:09:34,790 --> 00:09:39,000
 superficial about them.

96
00:09:39,000 --> 00:09:46,020
 And finally, suppose there's a bowl of water that is muddy,

97
00:09:46,020 --> 00:09:50,780
 so stirred up on the bottom, so it's all muddy and set in

98
00:09:50,780 --> 00:09:52,000
 the dark.

99
00:09:52,000 --> 00:09:57,940
 Suppose it's dark and you look in a mud puddle. This is the

100
00:09:57,940 --> 00:10:02,000
 puddle that likens delusion, ignorance.

101
00:10:02,000 --> 00:10:07,000
 Doubt, sorry, doubt.

102
00:10:07,000 --> 00:10:25,000
 Doubt.

