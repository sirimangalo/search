1
00:00:00,000 --> 00:00:04,780
 How can one cope with having a glimpse of about three weeks

2
00:00:04,780 --> 00:00:10,000
 of Nibbana and then loosen it? Why does this happen?

3
00:00:10,000 --> 00:00:14,000
 I got a question. What is a glimpse of Nibbana?

4
00:00:14,000 --> 00:00:19,460
 I think he claims he had an enlightenment, some kind of

5
00:00:19,460 --> 00:00:22,000
 experience of enlightenment.

6
00:00:22,000 --> 00:00:25,000
 It lasted for three weeks and then faded off.

7
00:00:25,000 --> 00:00:31,070
 That doesn't mean sense to me. I don't understand that. Do

8
00:00:31,070 --> 00:00:34,000
 you understand that, you go, Donama?

9
00:00:34,000 --> 00:00:37,750
 Well, I understand it to probably not be Nibbana or

10
00:00:37,750 --> 00:00:41,010
 enlightenment. Because remember, there's a lot of things

11
00:00:41,010 --> 00:00:44,000
 that seem to be that and are not that.

12
00:00:44,000 --> 00:00:49,450
 What was happening for those... The experience of Nibbana

13
00:00:49,450 --> 00:00:54,000
 can't be for three weeks unless you're in for a Nibbana.

14
00:00:54,000 --> 00:00:58,620
 It could be, but the technical manuals say seven days is

15
00:00:58,620 --> 00:01:04,000
 the maximum of total Nibbana for a non...

16
00:01:04,000 --> 00:01:10,490
 Besides for a Nibbana, for someone who still has a body

17
00:01:10,490 --> 00:01:15,000
 safe, who still has something left to be lived out.

18
00:01:15,000 --> 00:01:19,000
 But that seven days is... There's no khanda, there's no

19
00:01:19,000 --> 00:01:23,250
 aggregates, there's no physical, there's no feelings, there

20
00:01:23,250 --> 00:01:26,000
's no perceptions, there's no thoughts.

21
00:01:26,000 --> 00:01:31,290
 And there's no consciousness except in the technical sense

22
00:01:31,290 --> 00:01:36,000
 of a consciousness that takes Nibbana as an object.

23
00:01:36,000 --> 00:01:40,310
 But what is that? I'm skeptical, but it's just a technical

24
00:01:40,310 --> 00:01:41,000
ity.

25
00:01:41,000 --> 00:01:45,830
 Three weeks after that, some of the aspects of that

26
00:01:45,830 --> 00:01:51,200
 experience, if it is an experience of Nibbana, will wear

27
00:01:51,200 --> 00:01:52,000
 off.

28
00:01:52,000 --> 00:01:56,790
 What won't wear off, at the very least, are what are called

29
00:01:56,790 --> 00:01:59,000
 the three lower fetters.

30
00:01:59,000 --> 00:02:04,740
 That person has no more view of self. It's not possible for

31
00:02:04,740 --> 00:02:10,010
 them to give rise to a view, to hold on to, or to ascribe

32
00:02:10,010 --> 00:02:12,000
 to a view of self.

33
00:02:12,000 --> 00:02:17,070
 It's no longer possible for that person to consider as

34
00:02:17,070 --> 00:02:21,000
 essential that which is unessential.

35
00:02:21,000 --> 00:02:23,720
 For instance, thinking that rites or rituals are going to

36
00:02:23,720 --> 00:02:25,000
 make them enlightened.

37
00:02:25,000 --> 00:02:28,140
 And it's no longer possible for that person to have doubt

38
00:02:28,140 --> 00:02:31,000
 about the Buddha, the Dhamma, and the Sangha.

39
00:02:31,000 --> 00:02:34,000
 And then of course it goes on and on.

40
00:02:34,000 --> 00:02:37,690
 If you see Nibbana again and again and again, or if you get

41
00:02:37,690 --> 00:02:41,020
 to the point where you're an anagami, then you have no more

42
00:02:41,020 --> 00:02:42,000
 greed and anger.

43
00:02:42,000 --> 00:02:46,220
 And if you practice and become an arahant, then there's no

44
00:02:46,220 --> 00:02:49,000
 more greed, anger, or delusion.

45
00:02:49,000 --> 00:02:53,870
 And that doesn't fade away. Enlightenment doesn't fade away

46
00:02:53,870 --> 00:02:54,000
.

