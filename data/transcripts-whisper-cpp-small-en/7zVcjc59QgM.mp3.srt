1
00:00:00,000 --> 00:00:02,000
 Good evening everyone.

2
00:00:02,000 --> 00:00:08,000
 Broadcasting Live, May 11th.

3
00:00:08,000 --> 00:00:19,000
 Today's topic is friendship, friends.

4
00:00:22,000 --> 00:00:32,510
 It's from the Sigalawada Sutta, the exhortation or the

5
00:00:32,510 --> 00:00:36,460
 teaching to Singala, or Singalika, depending how you say

6
00:00:36,460 --> 00:00:37,000
 his name.

7
00:00:39,000 --> 00:00:52,360
 It's widely considered to be the definitive source for

8
00:00:52,360 --> 00:01:07,000
 worldly right conduct and department and practice.

9
00:01:08,000 --> 00:01:13,020
 So how to practice the dhamma in the world, living in the

10
00:01:13,020 --> 00:01:14,000
 world.

11
00:01:14,000 --> 00:01:20,830
 So a lot of it is fairly, I guess, relatively superficial

12
00:01:20,830 --> 00:01:27,130
 in the sense that there's much of it isn't about deep

13
00:01:27,130 --> 00:01:31,000
 spiritual truths, but it's all very dhammic.

14
00:01:32,000 --> 00:01:37,290
 It's about very righteous, very good, about living a good

15
00:01:37,290 --> 00:01:41,990
 life, living an honest life, living a successful life,

16
00:01:41,990 --> 00:01:47,160
 finding happiness in this life, happiness in the next life,

17
00:01:47,160 --> 00:01:51,000
 and cultivating spirituality even as a lay person.

18
00:01:52,000 --> 00:01:58,430
 This is one of the better known passages from it. It's

19
00:01:58,430 --> 00:02:05,160
 about the four types of people who are called Amitamitapad

20
00:02:05,160 --> 00:02:10,000
irupa or Mitapadirupaka.

21
00:02:11,000 --> 00:02:15,590
 Amitamit means they are not friends. Mitapadirupaka means

22
00:02:15,590 --> 00:02:19,000
 they appear to be friends.

23
00:02:19,000 --> 00:02:30,000
 They have a appearance or they are like friends.

24
00:02:31,000 --> 00:02:34,230
 They would be known as appearing to be friends, but not

25
00:02:34,230 --> 00:02:37,560
 friends. And then we have the four types of people who are

26
00:02:37,560 --> 00:02:43,000
 called Nita Suhada.

27
00:02:43,000 --> 00:02:47,880
 Nita means they are friends. Suhada means they are real

28
00:02:47,880 --> 00:02:52,000
 friends. I don't know where that word comes from, Suhada.

29
00:02:57,000 --> 00:03:00,060
 And so neat about this passage is it does go into some

30
00:03:00,060 --> 00:03:04,520
 detail about each of the four, but first we step back and

31
00:03:04,520 --> 00:03:08,040
 let's talk about this distinction because the interesting

32
00:03:08,040 --> 00:03:16,100
 point being made here is a questioning that we must

33
00:03:16,100 --> 00:03:24,000
 question, analyze, and apply wisdom to friendship.

34
00:03:25,000 --> 00:03:30,000
 That this is an important aspect of our spiritual practice.

35
00:03:30,000 --> 00:03:35,350
 What makes a person a friend? Is it the fact that you have

36
00:03:35,350 --> 00:03:41,280
 known them for a long time? Is it a fact that they bring

37
00:03:41,280 --> 00:03:44,000
 you pleasure or joy?

38
00:03:45,000 --> 00:03:52,680
 Is it the fact that you engage in amusement or

39
00:03:52,680 --> 00:03:57,000
 entertainment with them?

40
00:03:58,000 --> 00:04:05,600
 And all of these are reasons why we choose friends. And too

41
00:04:05,600 --> 00:04:12,670
 often you hear people lamenting the relationship that they

42
00:04:12,670 --> 00:04:17,680
 have with their friends, but yet still trying to make it

43
00:04:17,680 --> 00:04:22,000
 work because why? Because he or she is my friend.

44
00:04:23,000 --> 00:04:25,800
 Which is curious because if the person is a through and

45
00:04:25,800 --> 00:04:29,000
 through corrupt individual, why are they your friend?

46
00:04:29,000 --> 00:04:33,800
 So the reason why we pick friends or why we consider people

47
00:04:33,800 --> 00:04:38,990
 to be our friends is often somewhat arbitrary. I remember

48
00:04:38,990 --> 00:04:45,040
 in high school, very interesting time in a sort of a sick

49
00:04:45,040 --> 00:04:47,000
 and twisted way.

50
00:04:48,000 --> 00:04:52,080
 Because you could be friends in a group and then suddenly

51
00:04:52,080 --> 00:04:56,170
 one person would become ostracized and forced out of the

52
00:04:56,170 --> 00:04:57,000
 group.

53
00:04:57,000 --> 00:05:00,810
 Just because it just seemed seemingly on a whim. Suddenly

54
00:05:00,810 --> 00:05:04,690
 everyone shunned someone. I remember this. I was never on

55
00:05:04,690 --> 00:05:06,000
 the receiving end of it.

56
00:05:07,000 --> 00:05:12,620
 But I remember being part of that and cliques and really

57
00:05:12,620 --> 00:05:17,910
 bizarre looking back and wondering why? Why did we pick the

58
00:05:17,910 --> 00:05:19,000
 people we picked?

59
00:05:19,000 --> 00:05:26,000
 Often it's a mob mentality. Very scary.

60
00:05:30,000 --> 00:05:34,840
 And the friends that I chose were often for how excited

61
00:05:34,840 --> 00:05:38,230
 they are. We choose friends for how excited we become

62
00:05:38,230 --> 00:05:42,560
 around them. Often because we look up to them or we envy

63
00:05:42,560 --> 00:05:43,000
 them.

64
00:05:43,000 --> 00:05:47,550
 Sometimes because they provide us simply with entertainment

65
00:05:47,550 --> 00:05:50,000
 or we consider them to be witty.

66
00:05:51,000 --> 00:05:57,450
 We can be wise even but usually just smart or witty.

67
00:05:57,450 --> 00:06:02,000
 Sometimes there's an amount of competition involved.

68
00:06:02,000 --> 00:06:07,000
 People who we compete with in friendly competition.

69
00:06:07,000 --> 00:06:09,820
 Remember when I returned back from having meditated, I lost

70
00:06:09,820 --> 00:06:12,930
 most of my old friends. I tried to hook up with them but I

71
00:06:12,930 --> 00:06:16,370
 was only really interested in bringing them to come and

72
00:06:16,370 --> 00:06:18,000
 practice meditation.

73
00:06:20,000 --> 00:06:24,310
 Which they generally weren't interested in. And I was doing

74
00:06:24,310 --> 00:06:30,170
 a poor job of being human really. Very much indoctrinated

75
00:06:30,170 --> 00:06:32,000
 at the time.

76
00:06:35,000 --> 00:06:43,570
 But yeah, this is the key here is the importance of right

77
00:06:43,570 --> 00:06:50,050
 friendship. The most important thing for us is our inner

78
00:06:50,050 --> 00:06:51,000
 practice.

79
00:06:52,000 --> 00:07:06,000
 The second most important thing is gotta be friendship.

80
00:07:06,000 --> 00:07:07,690
 Because it's your place in what it means to be a being, you

81
00:07:07,690 --> 00:07:08,000
 know.

82
00:07:09,000 --> 00:07:16,240
 In sentience. Where you place yourself on the scale of sent

83
00:07:16,240 --> 00:07:23,000
ience. The positive or the negative. The good or the bad.

84
00:07:23,000 --> 00:07:27,750
 It's very much to do with your friends. If you place

85
00:07:27,750 --> 00:07:31,340
 yourself in a group of friends, group of people who are

86
00:07:31,340 --> 00:07:33,000
 committed to unwholesomeness.

87
00:07:35,000 --> 00:07:40,280
 That's where you place yourself in your state of being. So

88
00:07:40,280 --> 00:07:47,370
 it's your external. It's your environment. It's the

89
00:07:47,370 --> 00:07:48,000
 external.

90
00:07:48,000 --> 00:07:51,230
 The internal is all about your own meditation and that

91
00:07:51,230 --> 00:07:57,310
 doesn't matter who you're surrounded by. But the external,

92
00:07:57,310 --> 00:08:04,000
 the external is that which supports and that which is the

93
00:08:06,000 --> 00:08:09,000
 nourishes your spiritual practice.

94
00:08:13,000 --> 00:08:20,030
 And so surrounding ourselves with whoever is really a poor

95
00:08:20,030 --> 00:08:26,140
 way of going about it. The Buddha gives us some fairly

96
00:08:26,140 --> 00:08:33,000
 clear guidelines and a sharp sort of distinction.

97
00:08:34,000 --> 00:08:38,410
 How should we go about making friends or choosing friends?

98
00:08:38,410 --> 00:08:41,940
 Choosing who we associate with. Now it's important to

99
00:08:41,940 --> 00:08:45,600
 understand this doesn't refer to who we should be friendly

100
00:08:45,600 --> 00:08:46,000
 towards.

101
00:08:46,000 --> 00:08:51,280
 I think that's fairly obvious but sometimes we confuse the

102
00:08:51,280 --> 00:08:55,200
 two. Just because someone you don't consider someone worthy

103
00:08:55,200 --> 00:08:58,420
 of friendship doesn't mean you shouldn't be friendly to

104
00:08:58,420 --> 00:08:59,000
 them.

105
00:09:00,000 --> 00:09:02,000
 I think there's a distinction because I don't think

106
00:09:02,000 --> 00:09:04,460
 everyone should be worthy of our friendship. Everyone

107
00:09:04,460 --> 00:09:07,770
 should be considered worthy of our friendship. Some people

108
00:09:07,770 --> 00:09:10,000
 are simply not worthy of it.

109
00:09:10,000 --> 00:09:15,360
 I don't know if that sounds somewhat arrogant or cold or so

110
00:09:15,360 --> 00:09:18,580
 on. Maybe it's the wrong way of placing it. Worthy,

111
00:09:18,580 --> 00:09:23,160
 unworthy is maybe not the best word but I think in a

112
00:09:23,160 --> 00:09:26,000
 technical sense it does make sense.

113
00:09:27,000 --> 00:09:30,410
 It's not worth having this friendship. This friendship isn

114
00:09:30,410 --> 00:09:34,370
't good for either of you. This friendship isn't something

115
00:09:34,370 --> 00:09:38,250
 that is beneficial. There's no benefit therefore it's not

116
00:09:38,250 --> 00:09:41,000
 worth it. That's all I really mean.

117
00:09:41,000 --> 00:09:45,430
 Don't mean to put ourselves above others. Just there's no

118
00:09:45,430 --> 00:09:50,990
 benefit to it. If all you do is drag each other down, not a

119
00:09:50,990 --> 00:09:52,000
 good thing.

120
00:09:53,000 --> 00:09:58,170
 Letting evil people take advantage of you is very bad for

121
00:09:58,170 --> 00:10:03,270
 them. It's not something you want to cultivate not just for

122
00:10:03,270 --> 00:10:05,000
 your own benefit. For theirs as well.

123
00:10:05,000 --> 00:10:14,110
 So the four types of English. These are pretty good. I

124
00:10:14,110 --> 00:10:18,000
 better not because I'll get mixed up.

125
00:10:19,000 --> 00:10:23,520
 So there's the greedy person. That's not the right

126
00:10:23,520 --> 00:10:27,600
 translation. Anyway, the greedy person, the one who speaks

127
00:10:27,600 --> 00:10:31,000
 but does not act. He's not translating directly.

128
00:10:32,000 --> 00:10:54,060
 Anya datu haro. One who looks only for their own benefit. I

129
00:10:54,060 --> 00:10:55,000
'm not quite sure.

130
00:10:56,000 --> 00:11:03,320
 One who is only, in Thai they say, ri tapur. Means they say

131
00:11:03,320 --> 00:11:09,000
 good things but that's it. Parama means the extent.

132
00:11:10,000 --> 00:11:23,900
 Vadji is speech. They only talk a good game. Anupia bhani,

133
00:11:23,900 --> 00:11:28,000
 one who speaks.

134
00:11:29,000 --> 00:11:41,000
 Two flatter you. The flatter.

135
00:11:42,000 --> 00:11:47,610
 And number four, apaya sahayo. Not the squander. Apaya sah

136
00:11:47,610 --> 00:11:53,040
ayo. One who is your companion in ruin. One who leads you to

137
00:11:53,040 --> 00:11:54,000
 ruin.

138
00:11:55,000 --> 00:12:08,330
 One who is only good for the person who is only out for

139
00:12:08,330 --> 00:12:13,000
 their own gain. The one who is only good for empty promises

140
00:12:13,000 --> 00:12:13,000
. That kind of thing.

141
00:12:14,000 --> 00:12:23,570
 The person who flatters, the flatterer and the companion in

142
00:12:23,570 --> 00:12:25,000
 ruin.

143
00:12:25,000 --> 00:12:31,910
 And then he gives four reasons for each. So the greedy one,

144
00:12:31,910 --> 00:12:35,760
 the one who is only looking for their own, they're greedy,

145
00:12:35,760 --> 00:12:39,000
 they give little and ask much.

146
00:12:40,000 --> 00:12:47,320
 Anyway, you can read through these. They're in the dighanik

147
00:12:47,320 --> 00:12:49,790
aya, the sigalavadasutta. Dighanikaya number 31. Definitely

148
00:12:49,790 --> 00:12:51,000
 worth reading.

149
00:12:51,000 --> 00:12:55,370
 Not going to go into it. Just talking a little bit about

150
00:12:55,370 --> 00:12:58,000
 friendship and good friends.

151
00:12:59,000 --> 00:13:05,000
 The four kinds of good friends, people who should be known

152
00:13:05,000 --> 00:13:09,000
 as friends, are, let's go to Pali.

153
00:13:12,000 --> 00:13:21,740
 Upakaru, one who does, one who helps. Samana sukhandukku,

154
00:13:21,740 --> 00:13:28,240
 one who is the same in happiness and suffering. Atakayi,

155
00:13:28,240 --> 00:13:30,000
 one who...

156
00:13:36,000 --> 00:13:43,230
 One who speaks, kai, one who speaks, what is beneficial. An

157
00:13:43,230 --> 00:13:47,000
ukampakou, one who is compassionate.

158
00:13:47,000 --> 00:13:54,000
 That's sympathetic. Yeah, sympathetic.

159
00:13:54,000 --> 00:13:58,000
 These are four real friends.

160
00:13:59,000 --> 00:14:02,030
 So the first four, I'll talk at least a little bit

161
00:14:02,030 --> 00:14:04,870
 explaining these, right? So the first one is the greedy one

162
00:14:04,870 --> 00:14:08,280
. This is the kind of person who just is only your friend

163
00:14:08,280 --> 00:14:10,000
 for their own benefit.

164
00:14:10,000 --> 00:14:15,280
 Maybe they borrow money from you, maybe they mooch off of

165
00:14:15,280 --> 00:14:19,490
 you. They're only in it because of the benefit it is to

166
00:14:19,490 --> 00:14:23,000
 them, the material benefit that you give them.

167
00:14:23,000 --> 00:14:28,000
 Not beneficial for either of you, really.

168
00:14:29,000 --> 00:14:31,000
 Whatji... No.

169
00:14:31,000 --> 00:14:34,930
 Whatji paramo, one who speaks. This is a person who talks

170
00:14:34,930 --> 00:14:38,940
 about, it says in the description, they talk about things

171
00:14:38,940 --> 00:14:42,590
 they've done for you in the past, they talk about things

172
00:14:42,590 --> 00:14:46,200
 they'll do for you in the future, but they never do

173
00:14:46,200 --> 00:14:47,000
 anything.

174
00:14:48,000 --> 00:14:52,390
 They don't do anything to help others, but they're always

175
00:14:52,390 --> 00:14:55,760
 bragging about how much they've done for you in the past or

176
00:14:55,760 --> 00:14:59,000
 how much what they'll do for you in the future.

177
00:14:59,000 --> 00:15:03,120
 One said a man, when I was in Thailand, he was just lying

178
00:15:03,120 --> 00:15:07,000
 through his teeth. It was really impressive.

179
00:15:08,000 --> 00:15:11,140
 He told us he had all this money coming to him and he was

180
00:15:11,140 --> 00:15:15,970
 going to give us $250,000 to start a monastery. And then he

181
00:15:15,970 --> 00:15:18,000
 just up and disappeared.

182
00:15:18,000 --> 00:15:21,960
 So he got me to help him with a bunch of stuff and after I

183
00:15:21,960 --> 00:15:26,240
 realized that was it, he was just buttering me up with all

184
00:15:26,240 --> 00:15:28,000
 these flat out lies.

185
00:15:28,000 --> 00:15:32,000
 These people actually do exist.

186
00:15:33,000 --> 00:15:36,000
 Con artists.

187
00:15:36,000 --> 00:15:41,480
 Anupia Bhani, one who flatters you. Yes, I've been through

188
00:15:41,480 --> 00:15:43,640
 this. I've had people... When I was in Sri Lanka, this

189
00:15:43,640 --> 00:15:46,000
 happened. It was incredible.

190
00:15:47,000 --> 00:16:01,440
 I was given into his care by a monk, a fairly well-known

191
00:16:01,440 --> 00:16:06,000
 monk in the US.

192
00:16:07,000 --> 00:16:11,460
 And it was really bizarre finding out that people were

193
00:16:11,460 --> 00:16:16,230
 giving him donations for me and he was squandering them on

194
00:16:16,230 --> 00:16:19,000
 women and drink and gambling.

195
00:16:19,000 --> 00:16:24,480
 Incredible. He had... Anyway, that's a long story. But he

196
00:16:24,480 --> 00:16:27,000
 was definitely a flatterer.

197
00:16:28,000 --> 00:16:33,510
 It was very dangerous because it's easy to get a big head

198
00:16:33,510 --> 00:16:36,000
 when people flatter you.

199
00:16:36,000 --> 00:16:43,000
 And then you lose your objectivity.

200
00:16:43,000 --> 00:16:48,160
 Apaya Sahayo, this is a person who leads you to ruin. So

201
00:16:48,160 --> 00:16:52,000
 this is the kind of person we often become friends with.

202
00:16:53,000 --> 00:16:56,750
 It was an interesting thing coming back and looking at

203
00:16:56,750 --> 00:17:01,410
 people who I knew and realizing that many of the people who

204
00:17:01,410 --> 00:17:06,000
 I had focused my energy on were exactly the type of people

205
00:17:06,000 --> 00:17:08,000
 who were...

206
00:17:09,000 --> 00:17:17,940
 ...undeteramental leading to ruin. People who like to drink

207
00:17:17,940 --> 00:17:19,000
, the people who like to...

208
00:17:19,000 --> 00:17:24,600
 ...people who are obsessed with sensuality, sexuality, that

209
00:17:24,600 --> 00:17:26,000
 kind of thing.

210
00:17:27,000 --> 00:17:31,470
 And a lot of the people who I had ignored were kind of

211
00:17:31,470 --> 00:17:37,140
 marginalized because they were boring, because they were

212
00:17:37,140 --> 00:17:41,000
 straight, you know, on the straight and narrow.

213
00:17:41,000 --> 00:17:46,120
 Looking at them, they really became... Wow, these are the

214
00:17:46,120 --> 00:17:49,500
 kind of people I would have liked to have stayed in contact

215
00:17:49,500 --> 00:17:50,000
 with.

216
00:17:51,000 --> 00:17:54,300
 Some of them I have actually gotten back in contact with

217
00:17:54,300 --> 00:17:56,000
 because they were good people.

218
00:18:02,000 --> 00:18:08,570
 That's the thing is we often are friends, we have friends

219
00:18:08,570 --> 00:18:14,980
 not because of the good they do for us, but because of how

220
00:18:14,980 --> 00:18:21,340
 they reaffirm or they assuage our guilty feelings at our

221
00:18:21,340 --> 00:18:24,000
 indulgences because they indulge with us.

222
00:18:25,000 --> 00:18:30,650
 The only one and therefore we are less concerned about our

223
00:18:30,650 --> 00:18:33,000
 own wholesomeness.

224
00:18:33,000 --> 00:18:37,250
 We have someone else doing it as well. It can't be all that

225
00:18:37,250 --> 00:18:38,000
 bad, right?

226
00:18:42,000 --> 00:18:48,400
 And the four who are real friends, one who actually helps

227
00:18:48,400 --> 00:18:55,030
 you, friends who help each other, who provide actual

228
00:18:55,030 --> 00:18:56,000
 support.

229
00:18:56,000 --> 00:18:59,360
 Somebody is your friend when they think to help you, when

230
00:18:59,360 --> 00:19:04,090
 they think to do something to your benefit, when they guard

231
00:19:04,090 --> 00:19:08,000
 you and they got your back.

232
00:19:10,000 --> 00:19:12,850
 Samana sukadukko is someone who doesn't abandon you in

233
00:19:12,850 --> 00:19:17,200
 times of trouble. When the going gets hard, the going gets

234
00:19:17,200 --> 00:19:18,000
 tough.

235
00:19:18,000 --> 00:19:21,290
 That's when you know your real friends, when they stick

236
00:19:21,290 --> 00:19:25,000
 with you. They don't abandon you when you're in difficulty.

237
00:19:25,000 --> 00:19:29,860
 This person is no fun anymore, now he's all got all these

238
00:19:29,860 --> 00:19:33,610
 problems. Let's go find people who don't have all these

239
00:19:33,610 --> 00:19:37,000
 problems so we can have a happy time again, you know?

240
00:19:38,000 --> 00:19:41,340
 Someone who sticks with you in happy times and in unhappy

241
00:19:41,340 --> 00:19:42,000
 times.

242
00:19:42,000 --> 00:19:45,790
 And other people are like that, when things get good, when

243
00:19:45,790 --> 00:19:49,090
 things are bad they stick with you, but when things get

244
00:19:49,090 --> 00:19:52,230
 good they become intoxicated with their pleasure and they

245
00:19:52,230 --> 00:19:53,000
 treat you poorly.

246
00:19:56,000 --> 00:20:00,880
 Atakāi is someone who speaks benefit, who speaks

247
00:20:00,880 --> 00:20:05,070
 meaningful things. This is a person who you definitely have

248
00:20:05,070 --> 00:20:08,070
 to stick with. This is like the Buddha, Buddha is the

249
00:20:08,070 --> 00:20:12,070
 greatest friend because he speaks that which is beneficial,

250
00:20:12,070 --> 00:20:13,000
 that which is helpful.

251
00:20:14,000 --> 00:20:19,140
 Stay with such people, people who say good things, people

252
00:20:19,140 --> 00:20:23,870
 who instruct or advise or remind you of what's good and

253
00:20:23,870 --> 00:20:27,280
 what's bad, even if it hurts sometimes. Sometimes it's

254
00:20:27,280 --> 00:20:31,000
 unpleasant to be around people who point out your faults.

255
00:20:31,000 --> 00:20:35,130
 But you should consider them as pointing out buried

256
00:20:35,130 --> 00:20:38,000
 treasure, it's far more valuable.

257
00:20:40,000 --> 00:20:46,090
 Anukampakō is one who sympathizes. One who sympathizes is

258
00:20:46,090 --> 00:20:51,560
 one who wishes to help, one who is compassionate. When you

259
00:20:51,560 --> 00:20:55,860
're in suffering they try to find a way for you to get out

260
00:20:55,860 --> 00:20:57,000
 of suffering.

261
00:20:58,000 --> 00:21:04,060
 There's someone who is in tune with you when you need a

262
00:21:04,060 --> 00:21:10,930
 friend to listen, when you need someone to listen to you,

263
00:21:10,930 --> 00:21:12,000
 they listen.

264
00:21:12,000 --> 00:21:17,120
 When you need someone to support, they're there to support

265
00:21:17,120 --> 00:21:20,000
 you because they sympathize.

266
00:21:20,000 --> 00:21:34,000
 Right?

267
00:21:35,000 --> 00:21:38,640
 They're sad at your misfortunes or choices in your good

268
00:21:38,640 --> 00:21:42,500
 fortune, or strains others from speaking ill of you and he

269
00:21:42,500 --> 00:21:46,000
 commends those who speak well of you.

270
00:21:46,000 --> 00:21:51,380
 So, a little bit about friendship, definitely worth reading

271
00:21:51,380 --> 00:21:57,000
 the whole suttā.

272
00:21:58,000 --> 00:22:02,560
 Okay, we got some questions here from last night. Wasn't An

273
00:22:02,560 --> 00:22:06,000
anda being altruistic by tending to the Buddha's needs?

274
00:22:06,000 --> 00:22:11,710
 Potentially altruistic, but he was only a suttāpanda, so

275
00:22:11,710 --> 00:22:16,000
 it doesn't mean it was right that he did it.

276
00:22:17,000 --> 00:22:22,780
 But no, Ananda was chosen. He didn't ask to be Buddha's

277
00:22:22,780 --> 00:22:27,820
 attendant. He was asked to do it. So he was doing it out of

278
00:22:27,820 --> 00:22:29,000
 gratitude.

279
00:22:29,000 --> 00:22:34,270
 But my point about altruism is that people are altruistic

280
00:22:34,270 --> 00:22:39,570
 because it pleases them to be altruistic. And it's because

281
00:22:39,570 --> 00:22:41,000
 it pleases them.

282
00:22:42,000 --> 00:22:50,950
 If being altruistic was thoroughly reprehensible, or

283
00:22:50,950 --> 00:22:59,000
 thoroughly repulsive to you, why would you be altruistic if

284
00:22:59,000 --> 00:23:03,000
 it was thoroughly unpleasing to you?

285
00:23:04,000 --> 00:23:08,360
 So what we do, we do because it pleases us. So it's not

286
00:23:08,360 --> 00:23:13,250
 really altruistic. We help others because it pleases us to

287
00:23:13,250 --> 00:23:14,000
 do so.

288
00:23:14,000 --> 00:23:16,550
 We may not say it, and we'll say, "No, that's not why we do

289
00:23:16,550 --> 00:23:22,810
 it." But it is. We do it because it has a positive

290
00:23:22,810 --> 00:23:26,000
 association in our minds.

291
00:23:29,000 --> 00:23:34,000
 It's still for our own benefit, that's the thing.

292
00:23:34,000 --> 00:23:38,060
 Due to also having unhealthy thinking and views, I noticed

293
00:23:38,060 --> 00:23:43,090
 that it was difficult to make progress. Now I've been

294
00:23:43,090 --> 00:23:45,000
 practicing.

295
00:23:46,000 --> 00:23:53,950
 When and/or how can I assess that I'm wholesome and guarded

296
00:23:53,950 --> 00:24:01,000
 well enough to start insight meditation again?

297
00:24:01,000 --> 00:24:04,200
 Having unhealthy thinking and views, well, thinking isn't

298
00:24:04,200 --> 00:24:09,000
 unhealthy. This is the problem if you judge your thoughts.

299
00:24:09,000 --> 00:24:13,000
 That's the problem, not the thoughts themselves.

300
00:24:14,000 --> 00:24:16,860
 Try and let the thoughts be, just acknowledge them thinking

301
00:24:16,860 --> 00:24:21,870
. It's a process. You should never put aside insight

302
00:24:21,870 --> 00:24:25,000
 meditation unless you're insane.

303
00:24:25,000 --> 00:24:29,400
 Then you've got to become insane. If you can't be mindful,

304
00:24:29,400 --> 00:24:33,000
 period, then you can't practice.

305
00:24:34,000 --> 00:24:37,680
 As long as you can, my teacher said if you can show someone

306
00:24:37,680 --> 00:24:41,510
 a cup of water and a cup of rice, and if they can tell the

307
00:24:41,510 --> 00:24:45,000
 difference, they can practice insight meditation.

308
00:24:45,000 --> 00:24:50,230
 Difficult to make progress. You're saying it was difficult

309
00:24:50,230 --> 00:24:54,190
 to make progress. Absolutely, it's difficult to make

310
00:24:54,190 --> 00:24:58,470
 progress. If it was easy, we'd all be enlightened. That's

311
00:24:58,470 --> 00:25:00,000
 not a reason to stop.

312
00:25:01,000 --> 00:25:04,460
 In fact, it's a sign that you're probably doing something

313
00:25:04,460 --> 00:25:05,000
 right.

314
00:25:05,000 --> 00:25:08,740
 As if it was very, very easy, you might want to be

315
00:25:08,740 --> 00:25:14,000
 suspicious. Not always. Some people have easy practice, but

316
00:25:14,000 --> 00:25:16,880
 for the most part, it should be difficult because it

317
00:25:16,880 --> 00:25:18,000
 challenges you.

318
00:25:18,000 --> 00:25:21,160
 So just because you're banging your head against the wall,

319
00:25:21,160 --> 00:25:24,250
 it feels like banging your head against the wall, doesn't

320
00:25:24,250 --> 00:25:26,000
 mean you're doing it wrong.

321
00:25:27,000 --> 00:25:29,690
 At the very least, you're learning that banging your head

322
00:25:29,690 --> 00:25:33,010
 against the wall hurts. That's very much a part of what our

323
00:25:33,010 --> 00:25:34,000
 practice is.

324
00:25:34,000 --> 00:25:38,280
 Learning how much it hurts to bang our heads against the

325
00:25:38,280 --> 00:25:42,000
 wall. So in the future, we don't even think to do it.

326
00:25:47,000 --> 00:25:51,190
 And then we have someone named Milind teaching here. I'm

327
00:25:51,190 --> 00:25:54,370
 always wary of people coming on here and teaching because I

328
00:25:54,370 --> 00:25:57,000
 don't know if they're teaching what I teach.

329
00:25:57,000 --> 00:25:59,510
 So everything here should be taken with a grain of salt,

330
00:25:59,510 --> 00:26:02,090
 and we don't endorse the views of anyone who posts. We

331
00:26:02,090 --> 00:26:04,000
 should have a little disclaimer somewhere.

332
00:26:04,000 --> 00:26:08,290
 Are there advantages or disadvantages to metastating

333
00:26:08,290 --> 00:26:14,000
 outside in nature? Who is this? Looks familiar.

334
00:26:14,000 --> 00:26:19,000
 Former academic.

335
00:26:19,000 --> 00:26:28,070
 I've answered this before, actually. A couple of times, I

336
00:26:28,070 --> 00:26:29,000
 think.

337
00:26:30,000 --> 00:26:34,180
 So nature, my view is that nature is beneficial not for

338
00:26:34,180 --> 00:26:38,990
 what it is, but for what it isn't. Nature is about the most

339
00:26:38,990 --> 00:26:51,000
 recognizable or comfortable environment for a human being

340
00:26:51,000 --> 00:26:55,000
 to be in because it's still very much in our psyche.

341
00:26:56,000 --> 00:26:59,580
 That's why when we go to nature, we feel peaceful because

342
00:26:59,580 --> 00:27:03,000
 it doesn't have all this stuff that is not familiar.

343
00:27:03,000 --> 00:27:12,000
 So because nature is free from all of this jarring stimuli,

344
00:27:12,000 --> 00:27:15,400
 therefore I think there is a benefit to practicing in

345
00:27:15,400 --> 00:27:16,000
 nature.

346
00:27:17,000 --> 00:27:20,740
 And the Buddha was clear about it. It's not the kind of

347
00:27:20,740 --> 00:27:22,950
 thing you want to put too much emphasis on, obviously,

348
00:27:22,950 --> 00:27:25,420
 because in the end it's all just seeing, hearing, smelling,

349
00:27:25,420 --> 00:27:27,000
 tasting, feeling, thinking.

350
00:27:27,000 --> 00:27:30,320
 But nature makes it easier, which may or may not be a good

351
00:27:30,320 --> 00:27:34,000
 thing, but in the beginning it's generally a good thing.

352
00:27:34,000 --> 00:27:38,190
 And it makes your practice go quicker and it makes it

353
00:27:38,190 --> 00:27:42,650
 stronger, so generally good. Not because of what it is, but

354
00:27:42,650 --> 00:27:44,000
 because of what it isn't.

355
00:27:46,000 --> 00:27:50,480
 Because it isn't jarring, it isn't stress-inducing, so it

356
00:27:50,480 --> 00:27:53,000
 makes you calm quite quickly.

357
00:27:53,000 --> 00:28:03,030
 Considering taking refuge in the five precepts, what

358
00:28:03,030 --> 00:28:11,690
 considering or change consideration should I make? Consider

359
00:28:11,690 --> 00:28:15,000
 to these arrangements after formerly two.

360
00:28:16,000 --> 00:28:20,100
 I already made arrangements for cremation of our bodies. I

361
00:28:20,100 --> 00:28:25,010
 wouldn't worry about the body. The body is just ashes to

362
00:28:25,010 --> 00:28:29,000
 ashes and dust to dust, whatever that means.

363
00:28:29,000 --> 00:28:32,000
 It's just dust in the wind.

364
00:28:35,000 --> 00:28:39,950
 But good for you, taking refuge in five precepts. I mean,

365
00:28:39,950 --> 00:28:44,510
 it's a bit, I mean, the kind of the cynic in me wants to

366
00:28:44,510 --> 00:28:49,000
 say, there really isn't a ceremony.

367
00:28:49,000 --> 00:28:55,370
 I want to ruin this whole thing for you all because taking

368
00:28:55,370 --> 00:29:00,000
 refuge means taking refuge. Do you take refuge? Do you

369
00:29:00,000 --> 00:29:03,740
 listen to the Buddha's teaching, follow it and appreciate

370
00:29:03,740 --> 00:29:04,000
 it?

371
00:29:05,000 --> 00:29:09,670
 And the five precepts, you know, do you keep them? But over

372
00:29:09,670 --> 00:29:13,720
 time there has evolved to be a ceremony, so there is a

373
00:29:13,720 --> 00:29:15,000
 ceremony.

374
00:29:15,000 --> 00:29:20,170
 And I'm happy to do it. In fact, we do it every week when

375
00:29:20,170 --> 00:29:26,000
 we have our sutta study class, or we sudimagga study class.

376
00:29:27,000 --> 00:29:30,830
 We just haven't had any of these classes, but for that

377
00:29:30,830 --> 00:29:35,160
 class we've been doing it before, every time before we do

378
00:29:35,160 --> 00:29:36,000
 the class.

379
00:29:36,000 --> 00:29:39,880
 Now, like in Thailand, when they come for, when they come

380
00:29:39,880 --> 00:29:43,380
 to offer lunch to the monks, often the monks will give them

381
00:29:43,380 --> 00:29:46,000
 the five precepts and the three refuges.

382
00:29:47,000 --> 00:29:51,960
 They'll do a ceremony. So, I mean, I would, you know, it's

383
00:29:51,960 --> 00:29:56,450
 done every day in Buddhist monasteries in Thailand. And Sri

384
00:29:56,450 --> 00:29:58,000
 Lanka, that's right.

385
00:29:58,000 --> 00:30:02,260
 Also for Sri Lankans, when they offer monk to the, offer

386
00:30:02,260 --> 00:30:06,000
 lunch to the monks, monks give them the five precepts.

387
00:30:11,000 --> 00:30:14,050
 And the three refuges. So it's not, it's not that big of a

388
00:30:14,050 --> 00:30:17,260
 deal, but there is a ceremony and it's something you can do

389
00:30:17,260 --> 00:30:18,000
 anytime.

390
00:30:18,000 --> 00:30:21,480
 I don't, I mean, I see a lot of Westerners really making a

391
00:30:21,480 --> 00:30:24,770
 big deal out of it and telling me about other people who

392
00:30:24,770 --> 00:30:26,000
 did the ceremony.

393
00:30:26,000 --> 00:30:28,970
 I'm like, well, you know, we do that every day in Thailand.

394
00:30:28,970 --> 00:30:31,450
 I don't even know, people do it once a week or whenever

395
00:30:31,450 --> 00:30:33,000
 they do it, some people chant it every day.

396
00:30:34,000 --> 00:30:38,120
 I used to chant the five precepts and the eight precepts. I

397
00:30:38,120 --> 00:30:41,660
 chanted the eight precepts on the most of the day and the

398
00:30:41,660 --> 00:30:43,000
 five precepts regularly.

399
00:30:43,000 --> 00:30:46,480
 That was so wonderful for me because I'd broken all the

400
00:30:46,480 --> 00:30:48,000
 precepts, right?

401
00:30:48,000 --> 00:30:52,720
 And to just have this power where now I was finally doing

402
00:30:52,720 --> 00:30:56,850
 something that I could see was right, like a know was right

403
00:30:56,850 --> 00:30:59,000
. It's pretty powerful.

404
00:31:00,000 --> 00:31:03,470
 I'd say just go ahead and do it for the most part. I don't

405
00:31:03,470 --> 00:31:07,030
 think that was quite your question. I wouldn't worry about

406
00:31:07,030 --> 00:31:08,000
 those arrangements.

407
00:31:08,000 --> 00:31:19,880
 How close to Chan, CNC and Dangchi? I don't have no idea

408
00:31:19,880 --> 00:31:21,000
 what you're saying.

409
00:31:21,000 --> 00:31:27,000
 Sorry.

410
00:31:29,000 --> 00:31:33,150
 It's not a very good, well-worded question. Statement,

411
00:31:33,150 --> 00:31:34,000
 sentence.

412
00:31:34,000 --> 00:31:41,700
 Do we need to see the rising and falling of the stomach in

413
00:31:41,700 --> 00:31:48,380
 terms of the primary elements? If so, does it become dhamma

414
00:31:48,380 --> 00:31:51,000
-nupasana at that point instead of kahay-nupasana?

415
00:31:52,000 --> 00:31:56,200
 Sankar, you're overthinking things as usual. No, it's

416
00:31:56,200 --> 00:31:57,000
 usually overthinking things.

417
00:31:57,000 --> 00:32:00,000
 Dhamma-nupasana.

418
00:32:00,000 --> 00:32:05,060
 Rising and falling is the wild out there. It's stiffness.

419
00:32:05,060 --> 00:32:06,000
 That's them.

420
00:32:06,000 --> 00:32:10,000
 You can't help but see it. It is. It is.

421
00:32:10,000 --> 00:32:13,850
 It's like, should we see a tiger as a tiger? When you see a

422
00:32:13,850 --> 00:32:16,000
 tree, should you see the tree in it?

423
00:32:16,000 --> 00:32:18,510
 I mean, it is a tree. You don't have to go looking for the

424
00:32:18,510 --> 00:32:19,000
 tree.

425
00:32:20,000 --> 00:32:22,000
 This is why odattu. That's what it is.

426
00:32:22,000 --> 00:32:28,650
 See, that's the problem with dhamma-nupasana. It's a really

427
00:32:28,650 --> 00:32:36,710
 difficult one to explain because it's much more about what

428
00:32:36,710 --> 00:32:38,000
 it is.

429
00:32:38,000 --> 00:32:44,090
 That's highly unexplanatory. But it's about them being

430
00:32:44,090 --> 00:32:45,000
 groups.

431
00:32:46,000 --> 00:32:50,000
 It's not about the individual parts of the five aggregates.

432
00:32:50,000 --> 00:32:52,840
 It's about the concept of the five aggregates, the teaching

433
00:32:52,840 --> 00:32:55,000
, the dhamma of the five aggregates.

434
00:32:55,000 --> 00:33:00,230
 Five aggregates as dhamma is different from the five aggreg

435
00:33:00,230 --> 00:33:01,000
ates.

436
00:33:01,000 --> 00:33:09,170
 As dhamma, it's an idea, the idea that there is no self,

437
00:33:09,170 --> 00:33:12,000
 right? That we are just made up of the five aggregates.

438
00:33:15,000 --> 00:33:20,000
 I mean, that's not even entirely comprehensive.

439
00:33:20,000 --> 00:33:25,840
 But the dhamma is very hard to explain for me. I've never

440
00:33:25,840 --> 00:33:29,000
 had a really satisfactory explanation,

441
00:33:29,000 --> 00:33:33,290
 which makes me feel that it's actually not something that

442
00:33:33,290 --> 00:33:36,000
 can be explained homogeneously.

443
00:33:36,000 --> 00:33:40,460
 The different aspects of dhamma refer to different things

444
00:33:40,460 --> 00:33:44,000
 and different meanings of the word dhamma.

445
00:33:44,000 --> 00:33:47,890
 But they all are teachings of the Buddha. So dhamma-nupas

446
00:33:47,890 --> 00:33:51,060
ana is much more. I like to translate dhamma there as

447
00:33:51,060 --> 00:33:52,000
 teachings.

448
00:33:52,000 --> 00:33:55,750
 So when you're practicing satipatthana, you need to keep

449
00:33:55,750 --> 00:34:00,100
 these things in mind as teachings because they play an

450
00:34:00,100 --> 00:34:02,000
 important role in your meditation.

451
00:34:02,000 --> 00:34:05,000
 That's about the best I can do about dhamma-nupasana.

452
00:34:08,000 --> 00:34:11,640
 Brent, there's a sutta where the Buddha instructs Moggall

453
00:34:11,640 --> 00:34:14,000
ana on how to overcome drowsiness.

454
00:34:14,000 --> 00:34:18,000
 Moggallana is practicing the signless concentration.

455
00:34:18,000 --> 00:34:20,990
 It's just the same as seeing the impermanence that the

456
00:34:20,990 --> 00:34:23,000
 noting practice leads to, yes.

457
00:34:23,000 --> 00:34:28,620
 Signless, signless is anmita, it's not anicca. In Pali it

458
00:34:28,620 --> 00:34:30,000
 would be animitta.

459
00:34:31,000 --> 00:34:35,370
 Animitta is a sign. Animitta means no sign, means no

460
00:34:35,370 --> 00:34:44,000
 warning basically. There's no precursor.

461
00:34:45,000 --> 00:34:57,070
 There's nothing that... I can't think of the word fore

462
00:34:57,070 --> 00:35:00,850
shadow or anything that gives you an indication that

463
00:35:00,850 --> 00:35:03,000
 something's going to happen.

464
00:35:03,000 --> 00:35:06,000
 I can't remember the word.

465
00:35:06,000 --> 00:35:18,250
 So it refers to impermanence because things don't have

466
00:35:18,250 --> 00:35:24,000
 warning. They change chaotically.

467
00:35:24,000 --> 00:35:27,000
 You can't predict, you can't expect.

468
00:35:32,000 --> 00:35:36,730
 So trying to fix, trying to hold on is a sure recipe for

469
00:35:36,730 --> 00:35:39,000
 disaster and suffering.

470
00:35:39,000 --> 00:35:56,000
 Are you trying to ask how close we are to these things?

471
00:35:58,000 --> 00:36:03,140
 Are we close to being Chan, Dharma realm? Because that's

472
00:36:03,140 --> 00:36:05,000
 none of us. None of that is us.

473
00:36:05,000 --> 00:36:07,950
 I would recommend that you read my booklet on how to med

474
00:36:07,950 --> 00:36:11,560
itate because that's where you start and then you can take

475
00:36:11,560 --> 00:36:13,000
 it from there and see what you think.

476
00:36:13,000 --> 00:36:21,120
 Anyway, let's stop there then. Thank you all. Have a good

477
00:36:21,120 --> 00:36:22,000
 night.

478
00:36:22,000 --> 00:36:27,000
 We will probably see you all tomorrow.

479
00:36:28,000 --> 00:36:31,000
 Or you'll see me tomorrow. Good night.

480
00:36:32,000 --> 00:36:33,000
 Thank you.

481
00:36:34,000 --> 00:36:35,000
 Thank you.

482
00:36:36,000 --> 00:36:37,000
 Thank you.

483
00:36:39,000 --> 00:36:40,000
 Thank you.

