1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:13,940
 Today we continue with verses 246 to 248, which read as

3
00:00:13,940 --> 00:00:15,000
 follows.

4
00:00:15,000 --> 00:00:32,370
 Yopana matipatiti mussawadhancha bhassita loke adinam ady

5
00:00:32,370 --> 00:00:37,000
ati paradaa rancha gachati

6
00:00:37,000 --> 00:00:46,970
 sura mariapanancha yonaro anu yunjati idheva meso lokasming

7
00:00:46,970 --> 00:00:52,000
 moolang khanati atanu

8
00:00:52,000 --> 00:01:06,160
 evangbho purisa jhana hi papadhamma asanyata matang lobho

9
00:01:06,160 --> 00:01:12,000
 adhammo jat jirangdu kaya randayong

10
00:01:12,000 --> 00:01:21,000
 which means

11
00:01:21,000 --> 00:01:36,080
 whoever engages in killing, engages in taking life, engages

12
00:01:36,080 --> 00:01:42,000
 in false speech.

13
00:01:42,000 --> 00:01:48,590
 Whoever engages in this world and taking what isn't given

14
00:01:48,590 --> 00:01:56,000
 and going to someone else's spouse.

15
00:01:56,000 --> 00:02:08,000
 Whoever engages in drinking alcohol and taking intoxicants.

16
00:02:08,000 --> 00:02:18,090
 In this very world such a person pulls themselves up by the

17
00:02:18,090 --> 00:02:19,000
 root.

18
00:02:19,000 --> 00:02:23,000
 Know this.

19
00:02:23,000 --> 00:02:26,000
 Know this, O man, purisa.

20
00:02:26,000 --> 00:02:29,000
 Know this, purisa.

21
00:02:29,000 --> 00:02:37,350
 Papadhamma asanyata, one who is unrestrained, will become

22
00:02:37,350 --> 00:02:44,000
 of evil nature, will have an evil nature.

23
00:02:44,000 --> 00:02:56,580
 Don't let greed and unrighteousness, evil, lead you to

24
00:02:56,580 --> 00:03:00,000
 suffering for a long time.

25
00:03:00,000 --> 00:03:11,690
 Jirang. These verses were taught in response to a group of

26
00:03:11,690 --> 00:03:14,000
 lay disciples of the Buddha

27
00:03:14,000 --> 00:03:20,430
 for people who called themselves lay disciples. Meaning not

28
00:03:20,430 --> 00:03:24,000
 monks, they were just people living their lives

29
00:03:24,000 --> 00:03:29,990
 trying to follow the Buddha's teaching as best they could

30
00:03:29,990 --> 00:03:34,180
 but they took up a curious practice of keeping one or

31
00:03:34,180 --> 00:03:36,000
 another of the precepts.

32
00:03:36,000 --> 00:03:40,420
 Maybe some of them kept two or three I don't know but out

33
00:03:40,420 --> 00:03:44,500
 of the five basic precepts that I just mentioned in the

34
00:03:44,500 --> 00:03:45,000
 verse

35
00:03:45,000 --> 00:03:51,790
 they would generally keep one of them and they argued one

36
00:03:51,790 --> 00:03:56,000
 day about whose precept was harder to keep.

37
00:03:56,000 --> 00:04:01,630
 Telling each other, "Oh I don't ever take life but that's a

38
00:04:01,630 --> 00:04:05,510
 really hard precept and I'm proud of that because that one

39
00:04:05,510 --> 00:04:07,000
's a hard one to keep."

40
00:04:07,000 --> 00:04:11,540
 Another one would say, "Oh, it's nothing. No matter what we

41
00:04:11,540 --> 00:04:15,000
'll never steal anything from anyone else."

42
00:04:15,000 --> 00:04:20,000
 That's the hardest one to keep. You're wrong.

43
00:04:20,000 --> 00:04:25,340
 Sexual misconduct, not committing adultery, not breaking up

44
00:04:25,340 --> 00:04:30,040
 other people's relationships, that one's the hardest

45
00:04:30,040 --> 00:04:33,000
 because when you want something you just take it.

46
00:04:33,000 --> 00:04:37,700
 That one's the hardest, I'm very proud of that. I assume

47
00:04:37,700 --> 00:04:41,000
 they were all proud, that's why they argued.

48
00:04:41,000 --> 00:04:50,810
 And another one of course would say, "Lying." Not telling

49
00:04:50,810 --> 00:04:54,000
 false speech, that one's the hardest for sure.

50
00:04:54,000 --> 00:04:57,240
 But for many people of course even the fifth precept is the

51
00:04:57,240 --> 00:05:02,990
 hardest. Not taking any alcohol, not taking any intoxicants

52
00:05:02,990 --> 00:05:03,000
.

53
00:05:03,000 --> 00:05:10,120
 It's difficult both in terms of having to then deal with

54
00:05:10,120 --> 00:05:17,000
 social pressure and anxiety and depression and so on.

55
00:05:17,000 --> 00:05:22,260
 And also just because of the pressure of society, the

56
00:05:22,260 --> 00:05:28,460
 pressure to, like everyone else, engage in such things, in

57
00:05:28,460 --> 00:05:33,000
 social situations and just as a general part of life.

58
00:05:36,000 --> 00:05:39,620
 So they went to the Buddha and they brought this question

59
00:05:39,620 --> 00:05:44,130
 to the Buddha, said, "Look, we can't decide. Wondering

60
00:05:44,130 --> 00:05:50,000
 venerable, sir, which precept is hardest to keep?"

61
00:05:50,000 --> 00:05:54,920
 And the Buddha wasn't having any of it. He refused to say

62
00:05:54,920 --> 00:05:58,000
 this one is more difficult or that one is more difficult.

63
00:05:58,000 --> 00:06:03,300
 He said instead, "All the precepts are hard to keep." And

64
00:06:03,300 --> 00:06:07,000
 then he taught this verse, these verses.

65
00:06:12,000 --> 00:06:17,340
 So the basic lesson I think is that not just that all

66
00:06:17,340 --> 00:06:23,320
 precepts are hard to keep, but more importantly that

67
00:06:23,320 --> 00:06:29,150
 keeping one or another of the precept is never going to cut

68
00:06:29,150 --> 00:06:30,000
 it.

69
00:06:30,000 --> 00:06:36,210
 It's never going to suffice. They're a group of five

70
00:06:36,210 --> 00:06:41,470
 precepts for a reason and it's not some magical group

71
00:06:41,470 --> 00:06:46,560
 whereby just keeping them is somehow a magical thing that

72
00:06:46,560 --> 00:06:49,400
 is going to solve all your problems or lead you to

73
00:06:49,400 --> 00:06:55,500
 enlightenment or be enough to lead to spiritual development

74
00:06:55,500 --> 00:06:56,000
.

75
00:06:56,000 --> 00:07:06,410
 But I'll say two things. First of all, that indeed keeping

76
00:07:06,410 --> 00:07:14,000
 one or another of the precepts is absolutely a positive act

77
00:07:14,000 --> 00:07:14,000
.

78
00:07:14,000 --> 00:07:18,010
 And first of all, let's take another step back and talk

79
00:07:18,010 --> 00:07:21,000
 about what it means to keep a precept.

80
00:07:21,000 --> 00:07:25,950
 Keeping a precept is not simply not doing something because

81
00:07:25,950 --> 00:07:30,820
 technically a person can go their whole life without being

82
00:07:30,820 --> 00:07:35,000
 confronted with the opportunity to do something.

83
00:07:35,000 --> 00:07:41,910
 And that's a good thing, but it's not keeping the precepts.

84
00:07:41,910 --> 00:07:45,400
 Keeping a precept, keeping a rule means when the

85
00:07:45,400 --> 00:07:49,000
 opportunity arises, not engaging in it.

86
00:07:49,000 --> 00:07:52,500
 It's a good thing that you're never in the position because

87
00:07:52,500 --> 00:07:55,890
 then you never have to deal with that psychological mind

88
00:07:55,890 --> 00:07:58,000
 state of having to deal with it.

89
00:07:58,000 --> 00:08:02,180
 But it isn't technically anything because it doesn't say

90
00:08:02,180 --> 00:08:06,860
 anything about your state of mind. An evil person can keep

91
00:08:06,860 --> 00:08:11,450
 a precept just as well as a pure person provided they're

92
00:08:11,450 --> 00:08:14,000
 not given the opportunity.

93
00:08:14,000 --> 00:08:19,400
 But a person who keeps a precept when the opportunity to

94
00:08:19,400 --> 00:08:24,000
 kill comes up, they abstain from killing.

95
00:08:24,000 --> 00:08:28,000
 There's a goodness in that for whatever reason they do it.

96
00:08:28,000 --> 00:08:32,110
 Of course, meaning is better than the alternative of

97
00:08:32,110 --> 00:08:35,000
 actually engaging in breaking the precept.

98
00:08:35,000 --> 00:08:39,280
 So these people who kept one or another, even just the

99
00:08:39,280 --> 00:08:44,000
 intention to keep that one precept is a wholesome thing.

100
00:08:44,000 --> 00:08:48,870
 The problem, and the problem especially with the five

101
00:08:48,870 --> 00:08:55,480
 precepts, is that any one of them is enough to corrupt the

102
00:08:55,480 --> 00:09:01,920
 mind to a sufficient degree that you really have a hard

103
00:09:01,920 --> 00:09:04,000
 time maintaining a basic state of humanity.

104
00:09:04,000 --> 00:09:10,800
 There are all five of them, real and true obstacles to

105
00:09:10,800 --> 00:09:17,000
 spiritual development, to purity of mind, to clarity of

106
00:09:17,000 --> 00:09:18,000
 mind.

107
00:09:18,000 --> 00:09:23,040
 They're the five precepts for a reason. And they don't

108
00:09:23,040 --> 00:09:28,160
 encompass everything. Not every evil deed is included in

109
00:09:28,160 --> 00:09:29,000
 them.

110
00:09:29,000 --> 00:09:32,550
 They're not commandments. They're not the things the Buddha

111
00:09:32,550 --> 00:09:37,880
 required people to do. But they're the five things that can

112
00:09:37,880 --> 00:09:47,630
 be singled out as being sufficiently harmful to have

113
00:09:47,630 --> 00:09:53,000
 serious repercussions on one's spiritual state of mind.

114
00:09:53,000 --> 00:09:56,780
 So someone who keeps all five of the precepts is going to

115
00:09:56,780 --> 00:10:00,860
 be an order of magnitude better off than someone who keeps

116
00:10:00,860 --> 00:10:03,000
 one, two, three, or four.

117
00:10:03,000 --> 00:10:07,700
 It's just hard to really praise someone who keeps some of

118
00:10:07,700 --> 00:10:12,430
 them and not all of them. It's really not praiseworthy at

119
00:10:12,430 --> 00:10:13,000
 all.

120
00:10:13,000 --> 00:10:16,280
 You can't say, "Oh, well, four out of five, that's not bad,

121
00:10:16,280 --> 00:10:21,640
 80 percent." It doesn't work like that. It really is all or

122
00:10:21,640 --> 00:10:26,000
 nothing. 90 percent it is.

123
00:10:26,000 --> 00:10:29,150
 You could say maybe if you keep four of them, you get 10

124
00:10:29,150 --> 00:10:33,820
 percent credit or something. You get very little credit,

125
00:10:33,820 --> 00:10:39,000
 but some credit because they're that important.

126
00:10:39,000 --> 00:10:45,440
 This story might seem a little bit odd or maybe even far-

127
00:10:45,440 --> 00:10:49,000
fetched, strange anyway.

128
00:10:49,000 --> 00:10:51,760
 But it actually is the sort of thing that comes up in

129
00:10:51,760 --> 00:10:56,640
 Buddhist circles. I've seen people talk about being able to

130
00:10:56,640 --> 00:11:01,150
 keep four precepts, and there's in fact this movement to

131
00:11:01,150 --> 00:11:05,000
 change the way people request the precepts.

132
00:11:05,000 --> 00:11:12,000
 So in some Buddhist circles now, for a long time actually,

133
00:11:12,000 --> 00:11:16,320
 they're requesting the five precepts from a monk, which is

134
00:11:16,320 --> 00:11:23,240
 a formal ceremony of asking to recite them for you or do

135
00:11:23,240 --> 00:11:27,680
 this repeat after me sort of thing where you affirm them in

136
00:11:27,680 --> 00:11:30,000
 the presence of the monk.

137
00:11:30,000 --> 00:11:34,180
 But instead of asking for all five of them as a group, you

138
00:11:34,180 --> 00:11:40,310
 ask for them, "Wisung, wisung rakhanataya," that I may keep

139
00:11:40,310 --> 00:11:44,000
 them individually.

140
00:11:44,000 --> 00:11:46,940
 You specify that because you know you're not going to keep

141
00:11:46,940 --> 00:11:50,310
 all five of them. So the idea is if I break one, well, I'm

142
00:11:50,310 --> 00:11:52,000
 still keeping the others.

143
00:11:52,000 --> 00:11:55,380
 And then the idea is it leaves you free to choose which

144
00:11:55,380 --> 00:12:00,000
 ones you want to keep. Of course, anyone is free to choose.

145
00:12:00,000 --> 00:12:08,000
 In Buddhism, we don't have commandments, not for laypeople.

146
00:12:08,000 --> 00:12:11,670
 But there's really very little benefit in keeping some of

147
00:12:11,670 --> 00:12:16,000
 the five precepts. It really doesn't work that way.

148
00:12:16,000 --> 00:12:21,240
 Furthermore, the simple psychological benefit of keeping

149
00:12:21,240 --> 00:12:24,000
 one of the precepts is valuable.

150
00:12:24,000 --> 00:12:30,510
 Someone who holds that up, not killing, I hold this up as a

151
00:12:30,510 --> 00:12:36,000
 valuable thing, that's pure, that's noble.

152
00:12:36,000 --> 00:12:40,770
 But more noble than that, of course, is someone who holds

153
00:12:40,770 --> 00:12:46,250
 the five precepts up as a noble thing, someone who sees not

154
00:12:46,250 --> 00:12:50,200
 just these five things, but the principle behind them, the

155
00:12:50,200 --> 00:12:52,000
 principle of basic humanity.

156
00:12:52,000 --> 00:12:55,800
 These five things and anything else like them, I take as a

157
00:12:55,800 --> 00:13:02,000
 basis of ethics and morality. That's a very pure intention.

158
00:13:02,000 --> 00:13:06,000
 So even when you don't have the opportunity to break them,

159
00:13:06,000 --> 00:13:10,260
 I think it is valid to say that someone who undertakes to

160
00:13:10,260 --> 00:13:13,000
 keep them has done a great thing.

161
00:13:13,000 --> 00:13:16,730
 They have made a great start, cultivated a great foundation

162
00:13:16,730 --> 00:13:20,000
. That's how the Buddha talks about morality.

163
00:13:20,000 --> 00:13:23,870
 See leh patitaya, standing upon ethics. And here in this

164
00:13:23,870 --> 00:13:26,000
 verse he talks about the opposite.

165
00:13:26,000 --> 00:13:30,010
 If you don't keep ethics, what is the result? You uproot

166
00:13:30,010 --> 00:13:34,070
 yourself. So ethics are the root of the tree. They're what

167
00:13:34,070 --> 00:13:36,000
 everything is founded on.

168
00:13:36,000 --> 00:13:41,550
 Because think about it, when you're engaged in any

169
00:13:41,550 --> 00:13:47,000
 unethical behavior, everything in your life becomes shaky.

170
00:13:47,000 --> 00:13:51,590
 There's an instability in your mind, in your livelihood, in

171
00:13:51,590 --> 00:13:56,990
 your social relationships. There's an element of corruption

172
00:13:56,990 --> 00:13:59,000
, of degradation.

173
00:13:59,000 --> 00:14:03,060
 And conversely, for someone who does have a solid ethical

174
00:14:03,060 --> 00:14:07,000
 framework, everything in your life becomes solid.

175
00:14:07,000 --> 00:14:10,830
 No matter what attacks might come or problems or conflicts

176
00:14:10,830 --> 00:14:15,000
 might come, you have a solid, what they say, moral compass.

177
00:14:15,000 --> 00:14:19,230
 But it's more than that. You have a solid foundation. You

178
00:14:19,230 --> 00:14:24,000
 know where you stand. You have integrity.

179
00:14:24,000 --> 00:14:27,580
 And that integrity runs deep, depending on how deep you

180
00:14:27,580 --> 00:14:32,280
 take the precepts. Meaning how deeply you take them in your

181
00:14:32,280 --> 00:14:33,000
 mind.

182
00:14:33,000 --> 00:14:38,310
 It becomes an integrity on a meditative level, which is

183
00:14:38,310 --> 00:14:44,000
 where this verse really is important for us as meditators.

184
00:14:44,000 --> 00:14:48,630
 There's an integrity of our state of mind, meaning our mind

185
00:14:48,630 --> 00:14:53,890
 is not wavering, is not feeling guilty, is not feeling

186
00:14:53,890 --> 00:14:59,000
 manipulative or crooked in any way.

187
00:14:59,000 --> 00:15:05,120
 There's an integrity, a rectitude, a righteousness in the

188
00:15:05,120 --> 00:15:06,000
 mind.

189
00:15:06,000 --> 00:15:10,380
 So how we should understand this verse or think of this

190
00:15:10,380 --> 00:15:14,000
 verse in terms of a meditative practice?

191
00:15:14,000 --> 00:15:17,700
 We have to think of the precepts as going deeper, or of

192
00:15:17,700 --> 00:15:21,000
 ethics as going deeper than just the precepts.

193
00:15:21,000 --> 00:15:27,860
 When the Buddha talks about a foundation on a worldly level

194
00:15:27,860 --> 00:15:34,580
, it's useful to understand the precepts as leading to an

195
00:15:34,580 --> 00:15:40,000
 integrity of mind, a peace of mind.

196
00:15:40,000 --> 00:15:47,840
 And the Buddha said joy, happiness that comes from keeping

197
00:15:47,840 --> 00:15:49,000
 them.

198
00:15:49,000 --> 00:15:53,100
 Normally in the world there's this idea that breaking the

199
00:15:53,100 --> 00:15:58,410
 precepts, breaking rules, breaking ethics is quite often a

200
00:15:58,410 --> 00:16:01,000
 source of happiness.

201
00:16:01,000 --> 00:16:04,780
 We don't steal because we know the problems, but if you did

202
00:16:04,780 --> 00:16:08,230
 steal, you'd get what you want and that would make you

203
00:16:08,230 --> 00:16:09,000
 happy.

204
00:16:09,000 --> 00:16:12,230
 We often think of ethics as being something that makes you

205
00:16:12,230 --> 00:16:13,000
 unhappy.

206
00:16:13,000 --> 00:16:17,010
 We think, well, maybe it's better to be unhappy and ethical

207
00:16:17,010 --> 00:16:19,000
, but it's actually not true.

208
00:16:19,000 --> 00:16:25,760
 True ethics, someone who has a profound and strong practice

209
00:16:25,760 --> 00:16:32,000
 in ethics will find great joy and great peace of mind.

210
00:16:32,000 --> 00:16:36,360
 And that joy, that peace of mind leads to stability, leads

211
00:16:36,360 --> 00:16:38,000
 to concentration.

212
00:16:38,000 --> 00:16:42,070
 And that concentration leads to understanding. So you

213
00:16:42,070 --> 00:16:43,000
 understand yourself better, you understand people better,

214
00:16:43,000 --> 00:16:46,390
 you're able to see things not in terms of us versus them,

215
00:16:46,390 --> 00:16:49,000
 but I have these issues and they have those issues.

216
00:16:49,000 --> 00:16:52,230
 And you see things much more as they are, as opposed to

217
00:16:52,230 --> 00:16:57,000
 what can I get out of this? What am I losing from this?

218
00:16:57,000 --> 00:17:01,310
 Which a lot of immorality cultivates when you're engaging

219
00:17:01,310 --> 00:17:05,990
 in theft and manipulation and killing and harming others

220
00:17:05,990 --> 00:17:07,000
 and so on.

221
00:17:07,000 --> 00:17:11,000
 Unethical behavior changes your perspective.

222
00:17:11,000 --> 00:17:13,890
 Once you're more ethical, you don't have a horse in the

223
00:17:13,890 --> 00:17:15,000
 race, as they say.

224
00:17:15,000 --> 00:17:20,090
 Nothing is really personal for you. You're able to see

225
00:17:20,090 --> 00:17:23,340
 things, "Ah, this person is doing this because of this and

226
00:17:23,340 --> 00:17:24,000
 that."

227
00:17:24,000 --> 00:17:28,180
 It's a very good way to cultivate focus, concentration and

228
00:17:28,180 --> 00:17:32,000
 wisdom. But that's really only on a worldly level.

229
00:17:32,000 --> 00:17:35,900
 So understanding ethics from a Buddhist perspective has to

230
00:17:35,900 --> 00:17:37,000
 go deeper.

231
00:17:37,000 --> 00:17:41,680
 The Buddha said, "Papa dhamma asanjata," in the third verse

232
00:17:41,680 --> 00:17:42,000
.

233
00:17:42,000 --> 00:17:46,620
 And that means one who is unrestrained is of an evil nature

234
00:17:46,620 --> 00:17:47,000
.

235
00:17:47,000 --> 00:17:51,630
 The ethical precepts and ethics in general is about

236
00:17:51,630 --> 00:17:53,000
 restraint.

237
00:17:53,000 --> 00:17:56,230
 And so as I said about restraint when you want to do

238
00:17:56,230 --> 00:17:59,140
 something and you decide not to do it or want to say

239
00:17:59,140 --> 00:18:01,000
 something and you decide not to say it.

240
00:18:01,000 --> 00:18:08,570
 But more importantly, it's a restraint of action as a

241
00:18:08,570 --> 00:18:17,000
 experience, meaning every moment where we move the body,

242
00:18:17,000 --> 00:18:21,000
 every moment where we engage in speech,

243
00:18:21,000 --> 00:18:25,150
 every moment of our experience has the potential to have an

244
00:18:25,150 --> 00:18:30,300
 ethical quality to it. Everything we see has the potential

245
00:18:30,300 --> 00:18:35,320
 to have an ethical component where we cultivate something

246
00:18:35,320 --> 00:18:37,000
 unethical or something ethical,

247
00:18:37,000 --> 00:18:42,030
 where we can go either way, depending on our habit,

248
00:18:42,030 --> 00:18:46,000
 depending on our interaction with it.

249
00:18:46,000 --> 00:18:50,260
 And so when we do walking meditation, each step has an

250
00:18:50,260 --> 00:18:52,000
 ethical quality to it.

251
00:18:52,000 --> 00:18:55,870
 And the practice of walking meditation is a training, a

252
00:18:55,870 --> 00:19:03,420
 cultivation, an attempt to evoke ethical states or ethical

253
00:19:03,420 --> 00:19:08,210
 action in each step, where each step becomes an ethical

254
00:19:08,210 --> 00:19:09,000
 action.

255
00:19:09,000 --> 00:19:13,840
 And of course, as a result, our life and everything we do

256
00:19:13,840 --> 00:19:18,000
 begins to take on a more ethical quality to it.

257
00:19:18,000 --> 00:19:21,490
 It goes on that deep of a level so that when it comes to

258
00:19:21,490 --> 00:19:25,170
 things that are so coarse, like the five precepts, like

259
00:19:25,170 --> 00:19:26,000
 killing,

260
00:19:26,000 --> 00:19:29,560
 it just, the idea or the act of killing could never arise

261
00:19:29,560 --> 00:19:33,040
 because you're so much deeper and so much more granular

262
00:19:33,040 --> 00:19:34,000
 than that.

263
00:19:34,000 --> 00:19:38,890
 Your mind is so pure at every moment that there could never

264
00:19:38,890 --> 00:19:43,120
 arise the conclusion, "I should harm someone. I should

265
00:19:43,120 --> 00:19:46,000
 steal something."

266
00:19:46,000 --> 00:19:52,190
 And that kind of ethics leads to a deeper state of focus or

267
00:19:52,190 --> 00:19:56,470
 concentration than simply a peaceful or a calm state of

268
00:19:56,470 --> 00:19:57,000
 mind.

269
00:19:57,000 --> 00:20:02,280
 It leads to a clear focus. I like to use the word focus

270
00:20:02,280 --> 00:20:05,070
 more than concentration because I think it gets the point

271
00:20:05,070 --> 00:20:06,000
 across better.

272
00:20:06,000 --> 00:20:09,390
 Concentration, you can see as a state where the mind is

273
00:20:09,390 --> 00:20:12,000
 just quiet or the mind is undisturbed.

274
00:20:12,000 --> 00:20:16,180
 It's like, can be like this helmet that you wear or this

275
00:20:16,180 --> 00:20:18,000
 armor that you put on.

276
00:20:18,000 --> 00:20:22,000
 But focus is something actually very different, right?

277
00:20:22,000 --> 00:20:25,810
 If you're out of focus, you don't see things clearly. You

278
00:20:25,810 --> 00:20:29,000
 make mistakes. It's like being in the dark.

279
00:20:29,000 --> 00:20:32,280
 When you turn off the lights and you can dimly see or not

280
00:20:32,280 --> 00:20:36,000
 even see, you can cause a lot of problems that way.

281
00:20:36,000 --> 00:20:40,820
 But when you focus, the blurriness fades and the things

282
00:20:40,820 --> 00:20:44,000
 that were always there, you don't see anything new,

283
00:20:44,000 --> 00:20:48,510
 but the things that were already there that you come in

284
00:20:48,510 --> 00:20:53,000
 contact with every day suddenly become clear to you.

285
00:20:53,000 --> 00:20:57,920
 Like when I was 13 and we suddenly realized that everyone

286
00:20:57,920 --> 00:21:01,000
 was seeing things that I couldn't see.

287
00:21:01,000 --> 00:21:05,070
 And then I got glasses and, "Oh, the world doesn't look

288
00:21:05,070 --> 00:21:07,000
 like I thought it did."

289
00:21:07,000 --> 00:21:14,010
 When you cultivate meditation, mindfulness, this is what

290
00:21:14,010 --> 00:21:16,000
 comes from it.

291
00:21:16,000 --> 00:21:19,630
 This is the concentration that every moment walking the

292
00:21:19,630 --> 00:21:22,710
 movement of the foot, which seemed like a very ordinary

293
00:21:22,710 --> 00:21:24,000
 thing,

294
00:21:24,000 --> 00:21:26,620
 the rising and falling of the stomach, which seems so banal

295
00:21:26,620 --> 00:21:29,000
 that, "Of course, I know what rising is.

296
00:21:29,000 --> 00:21:32,000
 Why am I looking at this thing that's so familiar to me?"

297
00:21:32,000 --> 00:21:37,320
 You realize how unfamiliar your own body is to you, not to

298
00:21:37,320 --> 00:21:39,000
 mention the mind.

299
00:21:39,000 --> 00:21:43,320
 And you start to see things about the mind that were always

300
00:21:43,320 --> 00:21:45,000
 there, always there.

301
00:21:45,000 --> 00:21:48,760
 They weren't hiding from you. You just didn't have the

302
00:21:48,760 --> 00:21:51,000
 vision, the focus to see them.

303
00:21:51,000 --> 00:21:53,740
 And that's the real wisdom that comes. It's not the wisdom

304
00:21:53,740 --> 00:21:55,000
 about yourself or someone else.

305
00:21:55,000 --> 00:22:02,000
 It's a wisdom about reality, about moments of experience.

306
00:22:02,000 --> 00:22:05,000
 So ethics isn't really about keeping the precepts.

307
00:22:05,000 --> 00:22:08,840
 When someone talks about keeping only four of the precepts

308
00:22:08,840 --> 00:22:11,000
 for a meditator, that's just absurd

309
00:22:11,000 --> 00:22:14,100
 because these five things are so coarse that anyone who

310
00:22:14,100 --> 00:22:15,000
 breaks any one of them,

311
00:22:15,000 --> 00:22:19,780
 you can immediately cut them out of the people who have any

312
00:22:19,780 --> 00:22:22,000
 potential to go deeper.

313
00:22:22,000 --> 00:22:24,550
 You're drinking alcohol. It really doesn't matter what else

314
00:22:24,550 --> 00:22:25,000
 you do.

315
00:22:25,000 --> 00:22:28,050
 You're not going anywhere. If you're killing, you're

316
00:22:28,050 --> 00:22:29,000
 stealing,

317
00:22:29,000 --> 00:22:32,140
 meditation isn't really possible for you until you stop

318
00:22:32,140 --> 00:22:33,000
 doing that.

319
00:22:33,000 --> 00:22:38,670
 They're just so coarse that it's absurd to think that you

320
00:22:38,670 --> 00:22:41,000
 could decide to keep some of them.

321
00:22:41,000 --> 00:22:43,910
 If you're not keeping the five precepts, there's so much

322
00:22:43,910 --> 00:22:45,000
 more you have to do that.

323
00:22:45,000 --> 00:22:48,470
 You don't even have a hope. You do have a hope, but your

324
00:22:48,470 --> 00:22:50,000
 hope is in keeping them.

325
00:22:50,000 --> 00:22:54,000
 They're not arbitrary.

326
00:22:54,000 --> 00:22:57,630
 In some religions, think of the precepts that you have to

327
00:22:57,630 --> 00:22:58,000
 keep.

328
00:22:58,000 --> 00:23:02,050
 You can't eat certain things. You have to wear certain

329
00:23:02,050 --> 00:23:03,000
 things.

330
00:23:03,000 --> 00:23:07,000
 You can't wear certain things. You can't do certain things.

331
00:23:07,000 --> 00:23:12,880
 On certain days, for example, there's none of that really

332
00:23:12,880 --> 00:23:14,000
 in Buddhism.

333
00:23:14,000 --> 00:23:17,600
 I mean, that's certainly nothing like that in the five

334
00:23:17,600 --> 00:23:20,000
 precepts or even the eight precepts.

335
00:23:20,000 --> 00:23:24,830
 They're specifically designed or related to the practice of

336
00:23:24,830 --> 00:23:26,000
 meditation,

337
00:23:26,000 --> 00:23:31,000
 the practice of the cultivation of mental development.

338
00:23:31,000 --> 00:23:35,990
 For the purpose, ultimately, of wisdom on a momentary level

339
00:23:35,990 --> 00:23:36,000
,

340
00:23:36,000 --> 00:23:41,410
 to see impermanent suffering, non-self, to see about our

341
00:23:41,410 --> 00:23:43,000
 realities,

342
00:23:43,000 --> 00:23:46,680
 those things that we cling to, that none of them, and

343
00:23:46,680 --> 00:23:50,000
 nothing in the world, is worth clinging to.

344
00:23:50,000 --> 00:23:53,000
 The things that we cling to are actually not worth it,

345
00:23:53,000 --> 00:23:56,180
 which has the power, of course, then to lead us to freedom

346
00:23:56,180 --> 00:23:57,000
 from suffering,

347
00:23:57,000 --> 00:24:03,000
 which, of course, is the goal of everything.

348
00:24:03,000 --> 00:24:05,520
 So that's the Dhammapada for tonight. Thank you all for

349
00:24:05,520 --> 00:24:06,000
 listening.

350
00:24:06,000 --> 00:24:09,000
 I wish you all the best.

