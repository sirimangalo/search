1
00:00:00,000 --> 00:00:15,770
 I'm grieving to broadcasting live from Stony Creek, Ontario

2
00:00:15,770 --> 00:00:21,880
, Canada.

3
00:00:21,880 --> 00:00:24,230
 Coming to give a little bit of dumb, trying to give a

4
00:00:24,230 --> 00:00:28,120
 little bit of dumb my food.

5
00:00:28,120 --> 00:00:34,610
 There's a lot to give, a lot of dumb teaching of the Budd

6
00:00:34,610 --> 00:00:35,120
has.

7
00:00:35,120 --> 00:00:44,120
 Ananda said, Ananda, who was the Buddha's attendant, said

8
00:00:44,120 --> 00:00:50,120
 he had 84,000 teachings of the Buddha, kept in memory.

9
00:00:50,120 --> 00:00:58,470
 That's what the texts say. But we have all the texts, and

10
00:00:58,470 --> 00:01:05,120
 whether there's 84,000 or not, there's a lot.

11
00:01:05,120 --> 00:01:11,020
 Sometimes this is daunting. This makes us question as to

12
00:01:11,020 --> 00:01:16,120
 whether we can truly know the Buddha's teaching.

13
00:01:16,120 --> 00:01:25,870
 People always try to simplify or pinpoint the essence of

14
00:01:25,870 --> 00:01:30,120
 the Buddha's teaching.

15
00:01:30,120 --> 00:01:35,110
 Sometimes we'll bring up the Eightfold Noble Path, sort of

16
00:01:35,110 --> 00:01:40,120
 the essential heart of Buddhism.

17
00:01:40,120 --> 00:01:49,120
 That's eight things, and that's something we can deal with.

18
00:01:49,120 --> 00:01:52,270
 Actually, often we'll talk about the Four Noble Truths,

19
00:01:52,270 --> 00:01:54,120
 thinking that that's easier.

20
00:01:54,120 --> 00:01:58,120
 Well, there's only four of those, right?

21
00:01:58,120 --> 00:02:01,760
 The problem with that is the Four Noble Truths don't really

22
00:02:01,760 --> 00:02:07,120
 work as far as pinpointing the Buddha's teaching.

23
00:02:07,120 --> 00:02:12,660
 Because there's a lot. If you want to sum it up, you've got

24
00:02:12,660 --> 00:02:14,120
 the Four Noble Truths,

25
00:02:14,120 --> 00:02:16,950
 but then, well, the fourth one is the Eightfold Noble Paths

26
00:02:16,950 --> 00:02:18,120
, and then you've got eight more there.

27
00:02:18,120 --> 00:02:21,940
 To really explain the Four Noble Truths, you have to

28
00:02:21,940 --> 00:02:25,120
 separate each truth into three parts.

29
00:02:25,120 --> 00:02:32,260
 There's the truth itself, there's the duty or the task that

30
00:02:32,260 --> 00:02:38,120
 must be accomplished in regards to that truth,

31
00:02:38,120 --> 00:02:45,810
 and then there's the attainment or the realization or the

32
00:02:45,810 --> 00:02:49,120
 accomplishment of the task.

33
00:02:49,120 --> 00:02:52,850
 There's the two anyway, there's the truth itself, and then

34
00:02:52,850 --> 00:02:55,120
 there's the task, which are two different things, actually.

35
00:02:55,120 --> 00:02:59,120
 You can't just say the First Noble Truth is suffering.

36
00:02:59,120 --> 00:03:02,380
 You have to say the First Noble Truth is, first of all,

37
00:03:02,380 --> 00:03:04,120
 that there is suffering.

38
00:03:04,120 --> 00:03:09,120
 Second of all, that suffering should be fully understood.

39
00:03:09,120 --> 00:03:15,120
 And third, that suffering has been fully understood,

40
00:03:15,120 --> 00:03:18,000
 and the only way you can understand the First Noble Truth

41
00:03:18,000 --> 00:03:21,990
 is by fully realizing it for yourself, fully understanding

42
00:03:21,990 --> 00:03:23,120
 it.

43
00:03:23,120 --> 00:03:27,420
 Buddha made this clear, not saying... You can't say you

44
00:03:27,420 --> 00:03:33,120
 understand the truth just by knowing that this is suffering

45
00:03:33,120 --> 00:03:37,120
, or X is suffering.

46
00:03:37,120 --> 00:03:41,910
 You have to also understand that it should be fully

47
00:03:41,910 --> 00:03:43,120
 understood,

48
00:03:43,120 --> 00:03:46,810
 whereas your task in regards to suffering, to fully

49
00:03:46,810 --> 00:03:48,120
 understand it.

50
00:03:48,120 --> 00:03:52,130
 But even then, you can't say you fully understand the truth

51
00:03:52,130 --> 00:03:53,120
 or suffering.

52
00:03:53,120 --> 00:03:58,820
 Only once you've fully understood suffering, then you can

53
00:03:58,820 --> 00:04:05,120
 say you know what is the First Noble Truth.

54
00:04:05,120 --> 00:04:07,690
 So, and in the Second Noble Truth, and the Third, and the

55
00:04:07,690 --> 00:04:11,120
 Fourth, each one has a task.

56
00:04:11,120 --> 00:04:14,360
 If you want to go a little bit more to the point, we can

57
00:04:14,360 --> 00:04:16,120
 just talk about the Eightfold Noble Path,

58
00:04:16,120 --> 00:04:21,430
 because practicing the Eightfold Noble Path allows you to

59
00:04:21,430 --> 00:04:23,120
 understand suffering.

60
00:04:23,120 --> 00:04:26,950
 And so we're down to one thing with eight parts, and so

61
00:04:26,950 --> 00:04:31,120
 eight, well, that's still a lot,

62
00:04:31,120 --> 00:04:35,210
 and each of the eight has to be explained in detail before

63
00:04:35,210 --> 00:04:37,120
 you can understand it.

64
00:04:37,120 --> 00:04:51,120
 But we can simplify it, and we can do so with authority,

65
00:04:51,120 --> 00:04:56,070
 because we... with confidence, because we have the

66
00:04:56,070 --> 00:04:58,120
 authority of the Buddha,

67
00:04:58,120 --> 00:05:01,910
 that actually the Eightfold Noble Path is only three things

68
00:05:01,910 --> 00:05:05,120
, morality, concentration, and wisdom.

69
00:05:05,120 --> 00:05:12,200
 So we have the three trainings, right speech, right action,

70
00:05:12,200 --> 00:05:17,120
 and right livelihood, these are morality,

71
00:05:17,120 --> 00:05:21,290
 right effort, right mindfulness, and right concentration,

72
00:05:21,290 --> 00:05:25,120
 these are under samadhi or concentration,

73
00:05:25,120 --> 00:05:30,120
 and right view and right thought, these are under wisdom.

74
00:05:30,120 --> 00:05:35,980
 So morality, concentration, and wisdom is a summary of the

75
00:05:35,980 --> 00:05:38,120
 Eightfold Noble Path.

76
00:05:38,120 --> 00:05:40,120
 And down to just three.

77
00:05:40,120 --> 00:05:43,660
 And when we get down to just three, then we can take out

78
00:05:43,660 --> 00:05:46,120
 the salient point of these three,

79
00:05:46,120 --> 00:05:57,180
 and that's the training, what we call sika, sika, the three

80
00:05:57,180 --> 00:05:59,120
 sikas,

81
00:05:59,120 --> 00:06:05,380
 the person who trains in these three things is called a s

82
00:06:05,380 --> 00:06:13,120
ika, someone who has become accomplished in them,

83
00:06:13,120 --> 00:06:16,120
 someone who learns to be moral.

84
00:06:16,120 --> 00:06:23,120
 Sika becomes capable, to become capable at something.

85
00:06:23,120 --> 00:06:30,360
 It's similar to the verb that means to be capable, sakoti,

86
00:06:30,360 --> 00:06:32,120
 sakoti.

87
00:06:32,120 --> 00:06:36,120
 Apparently they have something in common.

88
00:06:36,120 --> 00:06:44,540
 So sika means to become competent, to train yourself to

89
00:06:44,540 --> 00:06:50,120
 learn a skill, to learn to be moral,

90
00:06:50,120 --> 00:06:59,120
 to learn to be focused, and to learn to be wise.

91
00:06:59,120 --> 00:07:04,120
 So if you talk about morality, concentration, and wisdom,

92
00:07:04,120 --> 00:07:08,220
 we've studied a lot there, in morality there are four types

93
00:07:08,220 --> 00:07:09,120
 of morality,

94
00:07:09,120 --> 00:07:17,170
 the morality of keeping precepts, the morality of living a

95
00:07:17,170 --> 00:07:20,120
 life, a utilitarian life,

96
00:07:20,120 --> 00:07:25,120
 in the sense of using things but not letting them own you.

97
00:07:25,120 --> 00:07:36,500
 So being moral in your consumption, not living an excessive

98
00:07:36,500 --> 00:07:37,120
 life,

99
00:07:37,120 --> 00:07:45,400
 with fancy clothes and high-end food and dwelling and

100
00:07:45,400 --> 00:07:47,120
 medicines,

101
00:07:47,120 --> 00:07:52,860
 to only use them as our nest that's necessary, use our

102
00:07:52,860 --> 00:08:00,120
 requisite use things for a purpose that is truly useful.

103
00:08:00,120 --> 00:08:04,110
 We have to be moral in regards to our livelihood, our life

104
00:08:04,110 --> 00:08:05,120
 morally,

105
00:08:05,120 --> 00:08:09,120
 and moral in regards to our senses, so seeing, hearing,

106
00:08:09,120 --> 00:08:10,120
 smelling, tasting,

107
00:08:10,120 --> 00:08:15,710
 feeling and thinking not getting carried away by pleasant

108
00:08:15,710 --> 00:08:17,120
 or unpleasant sight,

109
00:08:17,120 --> 00:08:22,000
 sound, smells, tastes, feelings and thoughts, not giving

110
00:08:22,000 --> 00:08:24,120
 free rein to our senses,

111
00:08:24,120 --> 00:08:33,890
 regarding not staring or seeking out pleasant sights or

112
00:08:33,890 --> 00:08:36,120
 sounds or so on.

113
00:08:36,120 --> 00:08:40,120
 So morality, there's quite a bit in there.

114
00:08:40,120 --> 00:08:46,480
 Concentration, there's samatha concentration and vipassana

115
00:08:46,480 --> 00:08:48,120
 concentration,

116
00:08:48,120 --> 00:08:58,120
 kanika samadhi and upachara samadhi and apana samadhi.

117
00:08:58,120 --> 00:09:04,300
 Samadhi is samma, means same, it's a cognate from English

118
00:09:04,300 --> 00:09:05,120
 word same,

119
00:09:05,120 --> 00:09:12,120
 but in the sense of being balanced.

120
00:09:12,120 --> 00:09:19,120
 Samadhi comes from samma, or what exactly it comes from,

121
00:09:19,120 --> 00:09:23,120
 but samma means level,

122
00:09:23,120 --> 00:09:29,910
 or having a balanced mind, a mind that is tuned, in tune

123
00:09:29,910 --> 00:09:32,120
 and tuned perfectly,

124
00:09:32,120 --> 00:09:37,120
 not too much, not too little.

125
00:09:37,120 --> 00:09:41,850
 So we can be in tune with, we can be focused, have our mind

126
00:09:41,850 --> 00:09:43,120
, our lens in focus

127
00:09:43,120 --> 00:09:47,710
 on a single object, a conceptual object that we have rise

128
00:09:47,710 --> 00:09:49,120
 to in our minds,

129
00:09:49,120 --> 00:09:59,000
 or that we stare at with our eyes, or absorb, we become

130
00:09:59,000 --> 00:10:01,120
 absorbed in the object.

131
00:10:01,120 --> 00:10:04,580
 We are perfectly focused on the object, seeing it perfectly

132
00:10:04,580 --> 00:10:06,120
 clearly.

133
00:10:06,120 --> 00:10:14,580
 Or we can focus our minds on reality, and become focused,

134
00:10:14,580 --> 00:10:17,120
 so imperfect focus,

135
00:10:17,120 --> 00:10:24,310
 on reality, seeing things as they are, our experiences,

136
00:10:24,310 --> 00:10:28,120
 focusing on them.

137
00:10:28,120 --> 00:10:36,630
 And then wisdom, we train in wisdom from learning, we train

138
00:10:36,630 --> 00:10:38,120
 in wisdom from thinking,

139
00:10:38,120 --> 00:10:45,970
 considering, we train in wisdom from meditation, from

140
00:10:45,970 --> 00:10:51,120
 training, from seeing,

141
00:10:51,120 --> 00:11:01,120
 from developing our mind, from practicing meditation.

142
00:11:01,120 --> 00:11:04,400
 But we can simplify this down, as I said, to simply

143
00:11:04,400 --> 00:11:05,120
 training,

144
00:11:05,120 --> 00:11:11,670
 so we become trained, or we train ourselves, we learn about

145
00:11:11,670 --> 00:11:12,120
 these things

146
00:11:12,120 --> 00:11:21,750
 and then train ourselves to live a straight life, to have a

147
00:11:21,750 --> 00:11:25,120
 mind that is straight and clear.

148
00:11:25,120 --> 00:11:30,500
 This is why the Buddha, you can see why mindfulness was

149
00:11:30,500 --> 00:11:34,120
 singled out as sort of the,

150
00:11:34,120 --> 00:11:40,590
 that which kickstarts our practice, keeps us on the

151
00:11:40,590 --> 00:11:43,120
 practice, singled out as the quality

152
00:11:43,120 --> 00:11:49,130
 that the Buddha said is useful everywhere, is always useful

153
00:11:49,130 --> 00:11:51,120
, because it's the act of training.

154
00:11:51,120 --> 00:11:55,680
 Training in an interesting, in a very little sense of the

155
00:11:55,680 --> 00:11:56,120
 word,

156
00:11:56,120 --> 00:12:03,250
 to set your mind on the track, like on a train track, to

157
00:12:03,250 --> 00:12:05,120
 train your mind,

158
00:12:05,120 --> 00:12:11,120
 to get it to follow a track.

159
00:12:11,120 --> 00:12:14,300
 When you think of morality, concentration, and wisdom, you

160
00:12:14,300 --> 00:12:15,120
 get on the track,

161
00:12:15,120 --> 00:12:21,570
 it's like a train, so you never go off, you never go

162
00:12:21,570 --> 00:12:25,120
 outside of the training.

163
00:12:25,120 --> 00:12:29,490
 We use mindfulness to train ourselves, and we slowly become

164
00:12:29,490 --> 00:12:30,120
 more moral,

165
00:12:30,120 --> 00:12:34,520
 we become more focused, we become more wise, we become more

166
00:12:34,520 --> 00:12:35,120
 clear.

167
00:12:35,120 --> 00:12:38,620
 We practice the simple teachings of mindfulness, reminding

168
00:12:38,620 --> 00:12:39,120
 ourselves,

169
00:12:39,120 --> 00:12:43,510
 this is seeing, this is hearing, this is standing, this is

170
00:12:43,510 --> 00:12:45,120
 sitting, this is walking,

171
00:12:45,120 --> 00:12:52,120
 this is liking, this is distancing, see things as they are.

172
00:12:52,120 --> 00:12:55,120
 To train ourselves in this, we don't actually have to think

173
00:12:55,120 --> 00:12:57,120
 about things like morality, for example.

174
00:12:57,120 --> 00:13:00,680
 When you're mindful, you naturally gain morality, you don't

175
00:13:00,680 --> 00:13:02,120
 kill, you don't steal,

176
00:13:02,120 --> 00:13:05,200
 you don't lie, you don't cheat, your faculties are

177
00:13:05,200 --> 00:13:06,120
 controlled.

178
00:13:06,120 --> 00:13:09,120
 All it takes is for us to be mindful.

179
00:13:09,120 --> 00:13:11,510
 And the great thing about mindfulness is it's something you

180
00:13:11,510 --> 00:13:12,120
 practice now,

181
00:13:12,120 --> 00:13:17,300
 you don't have to have practiced it in the past, it's

182
00:13:17,300 --> 00:13:21,120
 something you start ever and again, in every moment.

183
00:13:21,120 --> 00:13:26,120
 So this is what we do.

184
00:13:26,120 --> 00:13:29,120
 In meditation practice we're training ourselves,

185
00:13:29,120 --> 00:13:33,030
 training in morality, concentration and wisdom, it comes

186
00:13:33,030 --> 00:13:35,120
 simply from being mindful.

187
00:13:35,120 --> 00:13:39,240
 And as a result we cultivate the eightfold noble path,

188
00:13:39,240 --> 00:13:40,120
 first the preliminary path,

189
00:13:40,120 --> 00:13:44,510
 and eventually we train our mind so perfectly that we

190
00:13:44,510 --> 00:13:48,120
 become noble when we enter into the noble path.

191
00:13:48,120 --> 00:13:52,910
 And with the noble path comes the noble fruit, with the

192
00:13:52,910 --> 00:13:56,120
 noble fruit comes enlightenment.

193
00:13:56,120 --> 00:14:01,550
 The understanding that we are free, we understand that we

194
00:14:01,550 --> 00:14:03,120
 are free,

195
00:14:03,120 --> 00:14:10,120
 that there's nothing left to be done, we've found the goal.

196
00:14:10,120 --> 00:14:14,690
 So that's the dhamma for tonight. Thank you all for tuning

197
00:14:14,690 --> 00:14:15,120
 in.

