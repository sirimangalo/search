 Noting, knowing, question. What should one do if one cannot
 find a name for an arisen
 phenomenon? Answer. In most cases, noting either knowing,
 knowing, or feeling, feeling
 will be accurate enough to create objective awareness of
 any phenomena that is not as
 easily categorizable as seeing or hearing, etc.
 Question. What should one do when a phenomena is so brief?
 One is only aware of it after
 it has already disappeared? Answer. At that moment there is
 a knowledge of the disappearance
 of the phenomena. Noting, knowing, knowing is enough to
 prevent any uncertainty or judgment
 about the object that has already ceased.
 Question.
 Question.
 Answer.
 Answer.
