1
00:00:00,000 --> 00:00:05,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:05,000 --> 00:00:11,130
 Once more we continue on with first number 75, which reads

3
00:00:11,130 --> 00:00:13,000
 as follows.

4
00:00:13,000 --> 00:00:21,000
 "Anya hi labhupani sa, anya nimban ghamini,

5
00:00:21,000 --> 00:00:28,000
 eva metang abhin yaya bhikkhu buddhasa savako,

6
00:00:28,000 --> 00:00:38,000
 sakarangnabi nandeya viveka manupruhayati."

7
00:00:38,000 --> 00:00:49,000
 Which means the path or that which is a means to gain,

8
00:00:49,000 --> 00:00:54,000
 that which is a means for gain, labhupani,

9
00:00:54,000 --> 00:01:00,410
 that which is a means for gain and that which leads to nimb

10
00:01:00,410 --> 00:01:01,000
ana.

11
00:01:01,000 --> 00:01:05,000
 These two are "anya," meaning they are different.

12
00:01:05,000 --> 00:01:07,000
 They are not the same.

13
00:01:07,000 --> 00:01:12,000
 One way is they are other than each other.

14
00:01:12,000 --> 00:01:18,000
 They are apart from each other.

15
00:01:18,000 --> 00:01:23,000
 "Ewa metang abhin yaya, abhin yaya."

16
00:01:23,000 --> 00:01:31,500
 For someone who understands this fully or in an ultimate

17
00:01:31,500 --> 00:01:33,000
 sense deeply,

18
00:01:33,000 --> 00:01:39,000
 abhikkhu, who is a savaka, student of the Buddha,

19
00:01:39,000 --> 00:01:49,000
 should not be overjoyed by sakkara, by worship or respect

20
00:01:49,000 --> 00:01:51,000
 or offerings,

21
00:01:51,000 --> 00:02:01,000
 should not by gains, by that which won by wealth and so on.

22
00:02:01,000 --> 00:02:05,400
 "Thinking about, viveka manupruhayi," "Thinking about or

23
00:02:05,400 --> 00:02:08,000
 resolute upon."

24
00:02:08,000 --> 00:02:12,000
 "Viveka," which means seclusion.

25
00:02:12,000 --> 00:02:15,380
 So a person who is a student of the Buddha should not be

26
00:02:15,380 --> 00:02:17,000
 inclined towards gain

27
00:02:17,000 --> 00:02:22,000
 or fame or wealth or so on.

28
00:02:22,000 --> 00:02:27,000
 They should be inclined towards seclusion.

29
00:02:27,000 --> 00:02:31,010
 The meaning and why these two are put together is because

30
00:02:31,010 --> 00:02:33,000
 of the story

31
00:02:33,000 --> 00:02:40,160
 which has to do with a novice who has this obvious choice

32
00:02:40,160 --> 00:02:42,000
 between

33
00:02:42,000 --> 00:02:48,000
 great gain and wealth and prosperity in a worldly sense

34
00:02:48,000 --> 00:02:53,000
 and chooses to leave it behind for the forest.

35
00:02:53,000 --> 00:02:59,000
 The story goes that there was an aged brahmin.

36
00:02:59,000 --> 00:03:03,000
 In the time of the Buddha, the religion was brahminism

37
00:03:03,000 --> 00:03:09,560
 and there were these priests who would oversee rituals,

38
00:03:09,560 --> 00:03:11,000
 ceremonies.

39
00:03:11,000 --> 00:03:16,210
 And so this was a very poor brahmin, aged and maybe forget

40
00:03:16,210 --> 00:03:17,000
ful anyway,

41
00:03:17,000 --> 00:03:21,000
 not very well known and maybe in a place where,

42
00:03:21,000 --> 00:03:23,640
 I guess with the advent of Buddhism, there wasn't much for

43
00:03:23,640 --> 00:03:25,000
 the brahmin to do

44
00:03:25,000 --> 00:03:28,520
 because people weren't all that interested in rituals and

45
00:03:28,520 --> 00:03:32,000
 ceremonies anymore.

46
00:03:32,000 --> 00:03:37,000
 So he never had much food or much wealth,

47
00:03:37,000 --> 00:03:42,300
 but he had great faith in Sariputta, the Buddha's chief

48
00:03:42,300 --> 00:03:44,000
 disciple.

49
00:03:44,000 --> 00:03:48,000
 And so Sariputta would go for alms to this brahmin's house,

50
00:03:48,000 --> 00:03:51,000
 and the brahmin had nothing to give him.

51
00:03:51,000 --> 00:03:54,690
 And so when he knew that Sariputta was coming, he would

52
00:03:54,690 --> 00:03:55,000
 hide

53
00:03:55,000 --> 00:03:58,810
 and avoid the elder out of embarrassment that he had

54
00:03:58,810 --> 00:04:01,000
 nothing to give.

55
00:04:01,000 --> 00:04:04,170
 And time and again he would do this when he knew that Sarip

56
00:04:04,170 --> 00:04:05,000
utta was coming,

57
00:04:05,000 --> 00:04:11,330
 and then one day he was able to obtain a single portion of

58
00:04:11,330 --> 00:04:12,000
 rice gruel

59
00:04:12,000 --> 00:04:15,000
 and a piece of cloth.

60
00:04:15,000 --> 00:04:19,000
 And immediately he thought that he had done some ceremony

61
00:04:19,000 --> 00:04:25,000
 and had gotten this as a gift or payment for his services.

62
00:04:25,000 --> 00:04:29,550
 And so he immediately thought that this would make a great

63
00:04:29,550 --> 00:04:30,000
 offering

64
00:04:30,000 --> 00:04:34,000
 for the elder who he hadn't been able to support.

65
00:04:34,000 --> 00:04:38,170
 And so when the elder came to his door, he leapt at the

66
00:04:38,170 --> 00:04:39,000
 opportunity

67
00:04:39,000 --> 00:04:44,000
 and started pouring the rice gruel into the elder's bowl,

68
00:04:44,000 --> 00:04:47,000
 and halfway done the elder covered his bowl up,

69
00:04:47,000 --> 00:04:51,570
 signifying that he didn't want to take all the food from

70
00:04:51,570 --> 00:04:53,000
 the brahmin,

71
00:04:53,000 --> 00:04:55,000
 and to leave some for him to eat as well.

72
00:04:55,000 --> 00:04:57,000
 But the brahmin refused and said,

73
00:04:57,000 --> 00:05:01,000
 "Listen, my wish for this is to be happy in the next life.

74
00:05:01,000 --> 00:05:05,000
 I'm giving this for the purpose of my benefit,

75
00:05:05,000 --> 00:05:08,000
 even at the expense of my well-being right now."

76
00:05:08,000 --> 00:05:11,000
 So he was going to go hungry without eating.

77
00:05:11,000 --> 00:05:19,000
 And so he pushed the elder to accept the entire meal,

78
00:05:19,000 --> 00:05:21,000
 and then he gave him the piece of cloth,

79
00:05:21,000 --> 00:05:24,000
 and he said, "Through the power of this offering,

80
00:05:24,000 --> 00:05:27,000
 may I obtain the realization, the same realization,

81
00:05:27,000 --> 00:05:31,000
 that you have gained, Venerable Sir."

82
00:05:31,000 --> 00:05:37,000
 And Sariputta said, "Ei wong ho tu, may it be thus."

83
00:05:37,000 --> 00:05:44,000
 I believe that's what he said.

84
00:05:44,000 --> 00:05:46,000
 And went on his way.

85
00:05:46,000 --> 00:05:48,000
 Now when the brahmin died,

86
00:05:48,000 --> 00:05:51,000
 out of his great reverence and respect for Sariputta,

87
00:05:51,000 --> 00:05:56,990
 he was born in the womb of one of Sariputta's chief female

88
00:05:56,990 --> 00:05:58,000
 disciples,

89
00:05:58,000 --> 00:06:01,000
 the lay disciples.

90
00:06:01,000 --> 00:06:06,000
 And they say during the time of her pregnancy,

91
00:06:06,000 --> 00:06:08,000
 she had these strange cravings.

92
00:06:08,000 --> 00:06:12,550
 They say pregnant people will have cravings for this or

93
00:06:12,550 --> 00:06:13,000
 that.

94
00:06:13,000 --> 00:06:16,870
 And I guess the understanding is that it has something to

95
00:06:16,870 --> 00:06:17,000
 do,

96
00:06:17,000 --> 00:06:19,560
 or the belief I would ever have something to do with the

97
00:06:19,560 --> 00:06:20,000
 child,

98
00:06:20,000 --> 00:06:21,000
 the nature of the child.

99
00:06:21,000 --> 00:06:25,020
 So they say she had cravings to give offerings to the monks

100
00:06:25,020 --> 00:06:26,000
 all the time,

101
00:06:26,000 --> 00:06:28,000
 and go to listen to the Dhamma.

102
00:06:28,000 --> 00:06:29,000
 She had these strange cravings.

103
00:06:29,000 --> 00:06:37,000
 Anyway, this boy is born and they bring him to the elder,

104
00:06:37,000 --> 00:06:40,000
 and the elder name is Yiddhissa.

105
00:06:40,000 --> 00:06:45,000
 And he has such great karma or merit or goodness

106
00:06:45,000 --> 00:06:51,040
 that he's stocked up just from this one act given with a

107
00:06:51,040 --> 00:06:52,000
 pure heart.

108
00:06:52,000 --> 00:06:56,000
 And the commentary remarks that there's something special

109
00:06:56,000 --> 00:06:57,000
 about giving

110
00:06:57,000 --> 00:07:01,000
 when you're hard up yourself.

111
00:07:01,000 --> 00:07:04,000
 There's a quote, Jack London, I think.

112
00:07:04,000 --> 00:07:07,000
 He said, "Rich people don't know how to give,

113
00:07:07,000 --> 00:07:09,700
 and a rich person gives, they don't really understand

114
00:07:09,700 --> 00:07:10,000
 giving.

115
00:07:10,000 --> 00:07:13,000
 They don't have a sense of what it means to really give,

116
00:07:13,000 --> 00:07:17,150
 but a poor person understands what need is and therefore is

117
00:07:17,150 --> 00:07:18,000
 able to give."

118
00:07:18,000 --> 00:07:24,950
 And so there's generally a much higher sense of the import

119
00:07:24,950 --> 00:07:26,000
 of the need.

120
00:07:26,000 --> 00:07:29,000
 And as a result of this great wholesomeness that he

121
00:07:29,000 --> 00:07:29,000
 cultivated

122
00:07:29,000 --> 00:07:33,610
 just with the one act of sacrificing his own meal and his

123
00:07:33,610 --> 00:07:37,000
 own possession, the cloth,

124
00:07:37,000 --> 00:07:40,000
 he was reborn with great merit.

125
00:07:40,000 --> 00:07:45,380
 And later on he became a novice when he was, I guess, seven

126
00:07:45,380 --> 00:07:46,000
 years old.

127
00:07:46,000 --> 00:07:48,000
 So he put ordained him.

128
00:07:48,000 --> 00:07:50,000
 And when he went for alms food,

129
00:07:50,000 --> 00:07:56,000
 he would get hundreds of people coming to offer him food.

130
00:07:56,000 --> 00:08:01,000
 And so they called him, and then he would give all this

131
00:08:01,000 --> 00:08:02,000
 excess food

132
00:08:02,000 --> 00:08:05,270
 that he got from the people, he would give it out to all

133
00:08:05,270 --> 00:08:06,000
 the monks.

134
00:08:06,000 --> 00:08:10,000
 And so they called him the dista, the alms giver,

135
00:08:10,000 --> 00:08:12,430
 because he was always going around giving alms to all the

136
00:08:12,430 --> 00:08:13,000
 other monks

137
00:08:13,000 --> 00:08:19,000
 who weren't as lucky to have as much support as he did.

138
00:08:19,000 --> 00:08:21,000
 And so that was his name for a while.

139
00:08:21,000 --> 00:08:25,000
 And then in the winter it got really cold,

140
00:08:25,000 --> 00:08:27,000
 and he noticed that the monks were without blankets,

141
00:08:27,000 --> 00:08:31,000
 and the monks were cold, were huddling around fires,

142
00:08:31,000 --> 00:08:34,000
 and he said, "Well, why are you rubbing yourselves

143
00:08:34,000 --> 00:08:36,000
 and why are you warming yourself by the fire?"

144
00:08:36,000 --> 00:08:38,000
 He said, "Well, it's winter, it's cold."

145
00:08:38,000 --> 00:08:41,000
 And he said, "Well, why don't you just get a blanket?"

146
00:08:41,000 --> 00:08:45,040
 And the monk said, "Well, it's easy for you to say 'he of

147
00:08:45,040 --> 00:08:46,000
 great merit.'

148
00:08:46,000 --> 00:08:49,660
 Maybe it's easy for you to get a blanket, but not easy for

149
00:08:49,660 --> 00:08:50,000
 us."

150
00:08:50,000 --> 00:08:53,000
 And he said, "Well, in that case, come with me."

151
00:08:53,000 --> 00:08:56,000
 And so he rounded up all the monks,

152
00:08:56,000 --> 00:08:59,000
 and he took them into the, into Savatthi,

153
00:08:59,000 --> 00:09:02,000
 and immediately people came and gave him blankets,

154
00:09:02,000 --> 00:09:05,000
 and so he got hundreds and hundreds of blankets.

155
00:09:05,000 --> 00:09:09,000
 And so then they called him the dista, the blanket giver,

156
00:09:09,000 --> 00:09:12,000
 and that was his name for a while.

157
00:09:12,000 --> 00:09:16,550
 And this went on, and he was able to obtain whatever he

158
00:09:16,550 --> 00:09:17,000
 wanted,

159
00:09:17,000 --> 00:09:21,000
 but he was still quite unsatisfied.

160
00:09:21,000 --> 00:09:24,000
 And he realized that if he were to stay in Savatthi,

161
00:09:24,000 --> 00:09:27,000
 he would never accomplish the goal of the holy life,

162
00:09:27,000 --> 00:09:30,000
 because there were too many visitors,

163
00:09:30,000 --> 00:09:33,000
 too many of his relatives and people coming to see him.

164
00:09:33,000 --> 00:09:38,000
 So he ran away and went off to live in the forest.

165
00:09:38,000 --> 00:09:41,000
 And then he got a new name,

166
00:09:41,000 --> 00:09:44,000
 and so his new name, and so by this time he had three names

167
00:09:44,000 --> 00:09:44,000
,

168
00:09:44,000 --> 00:09:45,000
 according to the commentary.

169
00:09:45,000 --> 00:09:48,000
 His third name was dista, the forest dweller.

170
00:09:48,000 --> 00:09:50,000
 So they had all these different names,

171
00:09:50,000 --> 00:09:52,000
 and they were trying to figure out which one to call him.

172
00:09:52,000 --> 00:09:57,000
 Anyway, in the end he was called dista, the forest dweller.

173
00:09:57,000 --> 00:10:04,000
 But the story goes that while he was in the forest,

174
00:10:04,000 --> 00:10:07,000
 he would say, whenever he came to see the people,

175
00:10:07,000 --> 00:10:12,360
 he stated so much to himself that he would never say much

176
00:10:12,360 --> 00:10:13,000
 to the lay people.

177
00:10:13,000 --> 00:10:16,000
 And this is sort of a common trait,

178
00:10:16,000 --> 00:10:25,000
 especially for monks intent upon attaining realization.

179
00:10:25,000 --> 00:10:33,000
 He would just say, "Sukhi hota dukkha pumu-chata."

180
00:10:33,000 --> 00:10:38,380
 "May you be happy, may you be well, and may you be free

181
00:10:38,380 --> 00:10:40,000
 from suffering."

182
00:10:40,000 --> 00:10:42,830
 That was all he would say, every day, every day, same thing

183
00:10:42,830 --> 00:10:44,000
, same thing.

184
00:10:44,000 --> 00:10:48,000
 And so they got the feeling that this guy was,

185
00:10:48,000 --> 00:10:52,630
 this was all he was good for, but they were happy to take

186
00:10:52,630 --> 00:10:54,000
 care of him.

187
00:10:54,000 --> 00:10:57,720
 And then one day his preceptor, Sariputta, came with a

188
00:10:57,720 --> 00:10:59,000
 whole bunch of other monks.

189
00:10:59,000 --> 00:11:02,000
 Actually they say he came with all the senior elder monks

190
00:11:02,000 --> 00:11:08,000
 because this novice was sort of a very special person.

191
00:11:08,000 --> 00:11:12,000
 And then the Buddha came.

192
00:11:12,000 --> 00:11:16,400
 And anyway, the only point here is that at one point they

193
00:11:16,400 --> 00:11:17,000
 wanted to ask him,

194
00:11:17,000 --> 00:11:20,000
 they asked the Buddha to give a talk.

195
00:11:20,000 --> 00:11:23,000
 It was either the Buddha or Sariputta.

196
00:11:23,000 --> 00:11:25,000
 I think maybe Sariputta actually.

197
00:11:25,000 --> 00:11:28,000
 Anyway, asked to give a talk and so they turned to dista

198
00:11:28,000 --> 00:11:28,000
 and said,

199
00:11:28,000 --> 00:11:31,000
 "Okay, dista, you give the talk."

200
00:11:31,000 --> 00:11:34,000
 And the people were like, "What's he going to say?"

201
00:11:34,000 --> 00:11:38,150
 "This guy can't teach. He's never taught since the day he

202
00:11:38,150 --> 00:11:40,000
 came to live in this forest."

203
00:11:40,000 --> 00:11:44,340
 And then he gets up on the dhamma seat and he gives a long

204
00:11:44,340 --> 00:11:46,000
 and wonderful sermon.

205
00:11:46,000 --> 00:11:50,380
 And everyone's a little bit confused and wondering why half

206
00:11:50,380 --> 00:11:51,000
 the people are kind of upset

207
00:11:51,000 --> 00:11:55,960
 because it was just with Sariputta, the Buddha hadn't come

208
00:11:55,960 --> 00:11:56,000
 yet.

209
00:11:56,000 --> 00:11:59,000
 And half the people were upset because they're like,

210
00:11:59,000 --> 00:12:02,260
 "Well, why is he staying here just quiet by himself and not

211
00:12:02,260 --> 00:12:05,000
 talking to anyone, not teaching?"

212
00:12:05,000 --> 00:12:07,000
 "When he can teach like this, why didn't he?"

213
00:12:07,000 --> 00:12:09,000
 So half the people were upset.

214
00:12:09,000 --> 00:12:12,030
 And then half of the people were overjoyed that they had

215
00:12:12,030 --> 00:12:14,000
 such a wonderful monk staying with them.

216
00:12:14,000 --> 00:12:17,000
 They hadn't realized what a wonderful monk he was.

217
00:12:17,000 --> 00:12:23,000
 So people are divided according to their character types.

218
00:12:23,000 --> 00:12:25,000
 And then the Buddha came.

219
00:12:25,000 --> 00:12:28,000
 Because the Buddha saw that this was going to be a problem

220
00:12:28,000 --> 00:12:30,000
 and so he came to sort it out.

221
00:12:30,000 --> 00:12:33,640
 And he pointed out how great it was that they had such a

222
00:12:33,640 --> 00:12:34,000
 monk as this,

223
00:12:34,000 --> 00:12:38,000
 someone who was intent upon a solitude.

224
00:12:38,000 --> 00:12:44,400
 And as a result of his great qualities and his great

225
00:12:44,400 --> 00:12:47,000
 devotion to the practice,

226
00:12:47,000 --> 00:12:50,710
 he pointed out how all these great monks then came to visit

227
00:12:50,710 --> 00:12:51,000
 him

228
00:12:51,000 --> 00:12:55,140
 and pointed out to the lay people how lucky they were as a

229
00:12:55,140 --> 00:12:56,000
 result.

230
00:12:56,000 --> 00:12:58,000
 So this is the story of Tissa.

231
00:12:58,000 --> 00:13:03,000
 As usual, it's sort of long and there's many aspects to it

232
00:13:03,000 --> 00:13:06,000
 that don't all relate to the verse.

233
00:13:06,000 --> 00:13:11,080
 But the verse came about because the monks were amazed that

234
00:13:11,080 --> 00:13:14,000
 Tissa was able to do this.

235
00:13:14,000 --> 00:13:18,150
 For many of them, there wasn't a choice to live in such

236
00:13:18,150 --> 00:13:19,000
 hardship.

237
00:13:19,000 --> 00:13:24,070
 But for Tissa, he had relatives and he had friends and

238
00:13:24,070 --> 00:13:27,000
 people who were devoted to him.

239
00:13:27,000 --> 00:13:31,000
 And so if he had stayed in Sawati, he could have gotten

240
00:13:31,000 --> 00:13:34,000
 lots of almsfood or blankets or whatever he wanted.

241
00:13:34,000 --> 00:13:38,400
 He had these other names for a reason because he was very

242
00:13:38,400 --> 00:13:40,000
 full of great merit

243
00:13:40,000 --> 00:13:45,900
 and was always able to live in luxury and get whatever he

244
00:13:45,900 --> 00:13:48,000
 wanted all the time.

245
00:13:48,000 --> 00:13:50,220
 And they said it's a difficult thing that he's done to go

246
00:13:50,220 --> 00:13:52,000
 off in the forest and live in hardship.

247
00:13:52,000 --> 00:13:54,640
 And the Buddha heard them and asked what they were talking

248
00:13:54,640 --> 00:13:55,000
 about.

249
00:13:55,000 --> 00:13:58,240
 And when they told him, he said, "Indeed, he's done a

250
00:13:58,240 --> 00:14:00,000
 difficult thing."

251
00:14:00,000 --> 00:14:04,180
 And then he told this verse and said, "There are two very

252
00:14:04,180 --> 00:14:07,000
 different paths to choose from.

253
00:14:07,000 --> 00:14:17,130
 The path of sakkara, of gain and fame and affluence and

254
00:14:17,130 --> 00:14:21,000
 pleasure and the path to freedom."

255
00:14:21,000 --> 00:14:25,730
 It's an important point and this is the point for our

256
00:14:25,730 --> 00:14:27,000
 practice.

257
00:14:27,000 --> 00:14:31,350
 It's a claim that we make that happiness doesn't come from

258
00:14:31,350 --> 00:14:32,000
 gain.

259
00:14:32,000 --> 00:14:35,000
 It doesn't come from getting what you want.

260
00:14:35,000 --> 00:14:40,390
 It doesn't come from pleasure. It doesn't come from worldly

261
00:14:40,390 --> 00:14:42,000
 pursuits.

262
00:14:42,000 --> 00:14:46,680
 There are two different paths, the path of gain and the

263
00:14:46,680 --> 00:14:49,000
 path to nirvana.

264
00:14:49,000 --> 00:14:53,240
 When one sees this and the word the Buddha uses abhinnyaya,

265
00:14:53,240 --> 00:14:56,000
 which abhi is like in a higher sense.

266
00:14:56,000 --> 00:14:59,210
 So not just knowing this. It's not about knowing this

267
00:14:59,210 --> 00:15:00,000
 intellectually.

268
00:15:00,000 --> 00:15:03,230
 Because I think for those of us who are at least somewhat

269
00:15:03,230 --> 00:15:07,000
 interested in spirituality, we all know this.

270
00:15:07,000 --> 00:15:12,000
 Intellectually, we're never going to be satisfied.

271
00:15:12,000 --> 00:15:15,780
 We've seen this. We've seen that we get and get and get and

272
00:15:15,780 --> 00:15:17,000
 it never satisfies us.

273
00:15:17,000 --> 00:15:22,000
 But this isn't what the Buddha points out here.

274
00:15:22,000 --> 00:15:25,650
 What he's talking about is a higher realization. You have

275
00:15:25,650 --> 00:15:28,000
 to realize it for yourself.

276
00:15:28,000 --> 00:15:32,000
 So in our practice, it's not about judging or prejudice.

277
00:15:32,000 --> 00:15:39,600
 It's about observing and realizing and seeing how our

278
00:15:39,600 --> 00:15:43,000
 wanting for specific experiences,

279
00:15:43,000 --> 00:15:47,630
 wanting to feel pleasure, wanting to feel calm, wanting to

280
00:15:47,630 --> 00:15:55,000
 see this or that, or experience this or experience that.

281
00:15:55,000 --> 00:15:57,910
 Seeing that this isn't the path, this doesn't lead us to

282
00:15:57,910 --> 00:16:00,000
 happiness, it doesn't satisfy us.

283
00:16:00,000 --> 00:16:03,440
 Seeking these things out actually leads to attachment to

284
00:16:03,440 --> 00:16:04,000
 them.

285
00:16:04,000 --> 00:16:07,000
 So we'll get a good experience in our practice.

286
00:16:07,000 --> 00:16:10,360
 And then when we don't get it, the next time we're

287
00:16:10,360 --> 00:16:12,000
 frustrated and upset.

288
00:16:12,000 --> 00:16:16,000
 And we see that this is like a microcosm for life.

289
00:16:16,000 --> 00:16:23,000
 By experiencing it firsthand, how this pattern of wanting,

290
00:16:23,000 --> 00:16:29,000
 getting, and then expecting and being frustrated,

291
00:16:29,000 --> 00:16:32,690
 it starts to change the way we look at the world from a

292
00:16:32,690 --> 00:16:35,000
 very basic foundational level.

293
00:16:35,000 --> 00:16:38,000
 And as a result, that informs our whole life.

294
00:16:38,000 --> 00:16:42,230
 When we take that out into the world, when wanting arises,

295
00:16:42,230 --> 00:16:46,000
 we're not as quick to leap and to chase

296
00:16:46,000 --> 00:16:51,000
 because we've seen firsthand the nature of it.

297
00:16:51,000 --> 00:16:58,040
 We've watched and observed on a very moment level, a very

298
00:16:58,040 --> 00:17:00,000
 basic level.

299
00:17:00,000 --> 00:17:03,260
 So someone who sees this, this sort of person is a true

300
00:17:03,260 --> 00:17:05,000
 disciple of the Buddha.

301
00:17:05,000 --> 00:17:10,000
 And they give up, and they are not delighted by gain.

302
00:17:10,000 --> 00:17:14,220
 Again, it's not about not gaining anything, although there

303
00:17:14,220 --> 00:17:19,000
's something to be said for living a very simple life.

304
00:17:19,000 --> 00:17:25,240
 Obviously this novice did a great thing by living in a life

305
00:17:25,240 --> 00:17:27,000
 of hardship.

306
00:17:27,000 --> 00:17:32,150
 There's often nothing better for the practice than to live

307
00:17:32,150 --> 00:17:35,330
 without anything, to live maybe even without a place to

308
00:17:35,330 --> 00:17:36,000
 live,

309
00:17:36,000 --> 00:17:42,240
 to stay at the foot of a tree, to allow yourself to be

310
00:17:42,240 --> 00:17:47,000
 perfectly flexible with whatever comes.

311
00:17:47,000 --> 00:17:52,500
 Eating only one meal a day, eating perhaps very little some

312
00:17:52,500 --> 00:17:53,000
 days.

313
00:17:53,000 --> 00:18:01,600
 And putting up with insects and putting up with hardships

314
00:18:01,600 --> 00:18:04,000
 of all sorts.

315
00:18:04,000 --> 00:18:07,630
 But the point is not to be attached and not to be delighted

316
00:18:07,630 --> 00:18:09,000
 by these things.

317
00:18:09,000 --> 00:18:17,120
 So for many of us, this living in the forest isn't

318
00:18:17,120 --> 00:18:20,000
 convenient to say the least.

319
00:18:20,000 --> 00:18:25,000
 And on the other hand, we aren't getting sakara.

320
00:18:25,000 --> 00:18:28,200
 Sakara means donations and gifts that religious people

321
00:18:28,200 --> 00:18:31,000
 might get because people have faith in them.

322
00:18:31,000 --> 00:18:39,000
 So for many of us, you can't fit it exactly.

323
00:18:39,000 --> 00:18:43,300
 But the word the Buddha used is viveka, someone who is res

324
00:18:43,300 --> 00:18:45,000
olute on seclusion.

325
00:18:45,000 --> 00:18:47,470
 And there are three kinds of seclusion, and they don't

326
00:18:47,470 --> 00:18:49,000
 really have anything to do with the forest.

327
00:18:49,000 --> 00:18:53,000
 But the point is seclusion isn't really the hardship.

328
00:18:53,000 --> 00:18:57,510
 It's that we don't involve ourselves in society because

329
00:18:57,510 --> 00:19:00,000
 society is where all of the gain comes from.

330
00:19:00,000 --> 00:19:04,000
 It's where all of our pleasure comes from.

331
00:19:04,000 --> 00:19:09,800
 It's where we become intoxicated with people, with sights

332
00:19:09,800 --> 00:19:13,000
 and sounds and smells and tastes that are exotic.

333
00:19:13,000 --> 00:19:16,000
 So seclusion, first is physical seclusion.

334
00:19:16,000 --> 00:19:19,570
 Physical seclusion means taking yourself away from the busy

335
00:19:19,570 --> 00:19:23,000
ness, away from those things that are going to entice you,

336
00:19:23,000 --> 00:19:25,000
 are going to overwhelm you.

337
00:19:25,000 --> 00:19:28,000
 This is useful especially for calming the mind.

338
00:19:28,000 --> 00:19:35,000
 It's not as useful for understanding or as important.

339
00:19:35,000 --> 00:19:38,990
 But very much in the beginning, it can be necessary even

340
00:19:38,990 --> 00:19:41,000
 for insight meditation.

341
00:19:41,000 --> 00:19:45,170
 You have to seclude your body in order to begin to come to

342
00:19:45,170 --> 00:19:47,000
 terms with reality.

343
00:19:47,000 --> 00:19:50,000
 Some people will say you shouldn't seclude yourself.

344
00:19:50,000 --> 00:19:55,840
 You should just be mindful in your daily life because that

345
00:19:55,840 --> 00:19:57,000
's real.

346
00:19:57,000 --> 00:19:59,000
 If you take yourself away, you're just running away.

347
00:19:59,000 --> 00:20:02,140
 But it's not really like that because again we're looking

348
00:20:02,140 --> 00:20:03,000
 at a microcosm.

349
00:20:03,000 --> 00:20:07,000
 We're looking at the very fundamentals of reality.

350
00:20:07,000 --> 00:20:11,000
 Those are not people, places or things.

351
00:20:11,000 --> 00:20:13,430
 They are seeing, hearing, smelling, tasting, feeling,

352
00:20:13,430 --> 00:20:14,000
 thinking.

353
00:20:14,000 --> 00:20:17,040
 The problem with being in society and doing this is that we

354
00:20:17,040 --> 00:20:18,000
're overwhelmed

355
00:20:18,000 --> 00:20:23,140
 and we're much quicker to react and interact than we are to

356
00:20:23,140 --> 00:20:24,000
 observe.

357
00:20:24,000 --> 00:20:29,380
 In order to observe, we have to turn the faucet off, turn

358
00:20:29,380 --> 00:20:31,000
 the water source

359
00:20:31,000 --> 00:20:36,460
 and the metaphorical source of our experience off and just

360
00:20:36,460 --> 00:20:39,000
 turn it on just a little bit, one by one.

361
00:20:39,000 --> 00:20:42,650
 This is why we start walking because we're walking back and

362
00:20:42,650 --> 00:20:46,000
 forth and sitting watching the stomach

363
00:20:46,000 --> 00:20:48,860
 because we're trying to do something very simple to give a

364
00:20:48,860 --> 00:20:52,000
 very basic appreciation of

365
00:20:52,000 --> 00:20:55,000
 what does it mean to experience?

366
00:20:55,000 --> 00:20:58,000
 How are the different ways we can experience something?

367
00:20:58,000 --> 00:21:01,000
 What does it mean to experience one thing?

368
00:21:01,000 --> 00:21:04,000
 What does it mean to react with anger and all these things?

369
00:21:04,000 --> 00:21:09,000
 You see them one by one through our seclusion.

370
00:21:09,000 --> 00:21:12,000
 The second seclusion is mental seclusion.

371
00:21:12,000 --> 00:21:15,650
 When we seclude ourselves from the hindrances, liking, disl

372
00:21:15,650 --> 00:21:19,000
iking, drowsiness, distraction, doubt.

373
00:21:19,000 --> 00:21:23,140
 So we do this through the practice and this can be done,

374
00:21:23,140 --> 00:21:25,000
 theoretically can be done anywhere.

375
00:21:25,000 --> 00:21:31,000
 Again, it's much easier when you're in a secluded place.

376
00:21:31,000 --> 00:21:37,580
 The second one is seclusion of the mind, so the mind that

377
00:21:37,580 --> 00:21:38,000
 is secluded.

378
00:21:38,000 --> 00:21:41,760
 But the third one is seclusion of defilements or you could

379
00:21:41,760 --> 00:21:46,000
 say seclusion from arising,

380
00:21:46,000 --> 00:21:52,850
 seclusion from unwholesome tendencies, which is nirvana or

381
00:21:52,850 --> 00:21:56,000
 freedom from suffering.

382
00:21:56,000 --> 00:22:01,000
 So the key is to live in these three seclusions.

383
00:22:01,000 --> 00:22:04,280
 For lay people there is this point that has to be made that

384
00:22:04,280 --> 00:22:07,000
 we should be careful not to be

385
00:22:07,000 --> 00:22:10,000
 overwhelmed by society or caught up by society.

386
00:22:10,000 --> 00:22:14,200
 This was the decision that Tissa came to, that he was

387
00:22:14,200 --> 00:22:16,000
 surrounded by people,

388
00:22:16,000 --> 00:22:20,000
 people who were friendly and helpful and wonderful people.

389
00:22:20,000 --> 00:22:23,600
 But he realized that he wouldn't be able to go the next

390
00:22:23,600 --> 00:22:30,000
 step, to rise above this worldly state,

391
00:22:30,000 --> 00:22:35,360
 which can be quite pleasant at times, but is in the end

392
00:22:35,360 --> 00:22:39,000
 ultimately unsatisfying.

393
00:22:41,000 --> 00:22:44,940
 And so even for lay people we can make this decision to

394
00:22:44,940 --> 00:22:51,000
 take the time to find a place in our house that is quiet

395
00:22:51,000 --> 00:22:56,390
 or leave the house to go practice outside or to go to a

396
00:22:56,390 --> 00:23:02,000
 monastery or a meditation center from time to time,

397
00:23:02,000 --> 00:23:08,000
 even just to close our room and sit quietly alone.

398
00:23:10,000 --> 00:23:13,170
 Avoiding the sort of thing that the Buddha is talking about

399
00:23:13,170 --> 00:23:15,000
 here was sakkara.

400
00:23:15,000 --> 00:23:21,520
 Sakkara is a specific word having to do with offerings, but

401
00:23:21,520 --> 00:23:25,000
 the point is any kind of gain.

402
00:23:25,000 --> 00:23:30,000
 So not getting intoxicated.

403
00:23:30,000 --> 00:23:33,010
 The thing that keeps us in society, one of the things that

404
00:23:33,010 --> 00:23:35,000
 keeps us is our attachment to things,

405
00:23:35,000 --> 00:23:44,330
 to getting new possessions and a car and a house, money and

406
00:23:44,330 --> 00:23:47,000
 relationships and all of these things.

407
00:23:47,000 --> 00:23:50,000
 We become obsessed with them.

408
00:23:50,000 --> 00:23:53,770
 All of this is in the same category, all of the things that

409
00:23:53,770 --> 00:23:55,000
 we shouldn't be delighted with,

410
00:23:55,000 --> 00:24:00,000
 shouldn't get caught up with, caught up by.

411
00:24:00,000 --> 00:24:07,000
 So another simple teaching, but again quite powerful,

412
00:24:07,000 --> 00:24:16,050
 taking a firm stand on the idea that you can't dedicate

413
00:24:16,050 --> 00:24:18,000
 yourself,

414
00:24:18,000 --> 00:24:20,900
 you can't consider yourself to be fully following the

415
00:24:20,900 --> 00:24:26,390
 Buddha's teaching and still indulge and enjoy and get

416
00:24:26,390 --> 00:24:29,000
 caught up in sensuality.

417
00:24:29,000 --> 00:24:36,010
 So we have this war inside and we understand we have to

418
00:24:36,010 --> 00:24:39,000
 come to it as they are not compatible.

419
00:24:39,000 --> 00:24:44,040
 One way is the way that leads to attaining the things that

420
00:24:44,040 --> 00:24:45,000
 we desire,

421
00:24:45,000 --> 00:24:48,670
 and the other way is the way to be free, to be free from

422
00:24:48,670 --> 00:24:51,000
 desire and free from suffering,

423
00:24:51,000 --> 00:24:54,000
 to be truly at peace with ourselves.

424
00:24:54,000 --> 00:24:57,390
 We have to accept it because it's often people will often

425
00:24:57,390 --> 00:24:59,000
 try to take the middle way,

426
00:24:59,000 --> 00:25:03,000
 which is really having your cake and eating it too.

427
00:25:03,000 --> 00:25:06,280
 You want to practice meditation and become enlightened, but

428
00:25:06,280 --> 00:25:08,750
 you don't want to give anything up, you don't want to let

429
00:25:08,750 --> 00:25:09,000
 go.

430
00:25:09,000 --> 00:25:12,240
 It's like wanting to fly while you're still clinging, while

431
00:25:12,240 --> 00:25:14,000
 the bird is still clinging onto the tree.

432
00:25:14,000 --> 00:25:17,960
 Wanting to hold onto the tree and still fly, it's not

433
00:25:17,960 --> 00:25:20,000
 possible. You have to do one way or the other.

434
00:25:20,000 --> 00:25:25,550
 So that's the teaching for today. Thank you all for tuning

435
00:25:25,550 --> 00:25:26,000
 in. See you all next time.

