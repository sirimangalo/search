1
00:00:00,000 --> 00:00:02,560
 Is walking meditation necessary?

2
00:00:02,560 --> 00:00:06,740
 Is walking meditation necessary?

3
00:00:06,740 --> 00:00:09,740
 No, there is no specific posture

4
00:00:09,740 --> 00:00:12,540
 that is necessary in meditation.

5
00:00:12,540 --> 00:00:17,140
 Walking meditation is a means of keeping the body in shape

6
00:00:17,140 --> 00:00:19,920
 as well as cultivating effort

7
00:00:19,920 --> 00:00:22,620
 to balance excessive concentration

8
00:00:22,620 --> 00:00:24,600
 that can lead to drowsiness.

9
00:00:24,600 --> 00:00:29,160
 If one is unable to practice walking meditation properly

10
00:00:29,160 --> 00:00:32,960
 due to injuries, sickness, or disability,

11
00:00:32,960 --> 00:00:37,800
 it is recommended to use a cane or stationary object

12
00:00:37,800 --> 00:00:42,800
 such as a wall, railing, or table to balance oneself,

13
00:00:42,800 --> 00:00:46,520
 noting the movements of the stabilizing hand.

14
00:00:46,520 --> 00:00:50,840
 If one is unable to practice walking meditation at all,

15
00:00:50,840 --> 00:00:55,240
 one can improvise by performing other bodily movements

16
00:00:55,240 --> 00:00:58,840
 like moving one's legs, wheeling a wheelchair,

17
00:00:58,840 --> 00:01:02,760
 et cetera, for the purpose of keeping the body healthy

18
00:01:02,760 --> 00:01:06,920
 and cultivating effort while still maintaining mindfulness.

19
00:01:06,920 --> 00:01:09,680
 (static buzzing)

