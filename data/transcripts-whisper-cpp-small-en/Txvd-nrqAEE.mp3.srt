1
00:00:00,000 --> 00:00:08,640
 What is the point or goal of meditation? Happiness. The

2
00:00:08,640 --> 00:00:09,720
 point of everything is

3
00:00:09,720 --> 00:00:13,830
 happiness. You don't do something unless you think it's

4
00:00:13,830 --> 00:00:14,960
 going to make you happy.

5
00:00:14,960 --> 00:00:21,510
 So we practice meditation to become happy. You should never

6
00:00:21,510 --> 00:00:22,200
 give up

7
00:00:22,200 --> 00:00:28,370
 happiness. You should never avoid happiness. You should

8
00:00:28,370 --> 00:00:30,920
 never run away from it. You should

9
00:00:30,920 --> 00:00:37,260
 never practice meditation thinking that it's good to suffer

10
00:00:37,260 --> 00:00:41,160
 and so on. We should suffer

11
00:00:41,160 --> 00:00:44,320
 in meditation knowing that the suffering is not because of

12
00:00:44,320 --> 00:00:45,920
 the meditation, but

13
00:00:45,920 --> 00:01:00,270
 because of our mind states and our qualities that interrupt

14
00:01:00,270 --> 00:01:00,880
 the meditation

15
00:01:00,880 --> 00:01:05,670
 practice, interrupt our clear awareness of things. You

16
00:01:05,670 --> 00:01:07,120
 should not cultivate

17
00:01:07,120 --> 00:01:10,240
 states of pain in meditation. You should not cultivate

18
00:01:10,240 --> 00:01:11,840
 states of stress in

19
00:01:11,840 --> 00:01:16,960
 meditation. Meditation shouldn't be something that you

20
00:01:16,960 --> 00:01:17,920
 build up, build up,

21
00:01:17,920 --> 00:01:21,280
 build up a state of concentration. You shouldn't force

22
00:01:21,280 --> 00:01:22,320
 anything in the

23
00:01:22,320 --> 00:01:26,320
 meditation trying to become enlightened, trying to be happy

24
00:01:26,320 --> 00:01:28,360
, trying to find peace.

25
00:01:28,360 --> 00:01:33,560
 You shouldn't build up this stress. Meditation should be

26
00:01:33,560 --> 00:01:34,640
 that which leads

27
00:01:34,640 --> 00:01:39,440
 you to peace and thereby to true happiness.

