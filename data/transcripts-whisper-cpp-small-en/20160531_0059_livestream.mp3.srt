1
00:00:00,000 --> 00:00:16,120
 Good evening everyone.

2
00:00:16,120 --> 00:00:23,820
 Broadcasting live May 30th.

3
00:00:23,820 --> 00:00:27,760
 This will be our last broadcast for a while.

4
00:00:27,760 --> 00:00:32,760
 We'll do some audio broadcasts from Asia, but probably no

5
00:00:32,760 --> 00:00:36,200
 video broadcasts for a while.

6
00:00:36,200 --> 00:00:44,520
 I'm heading off to Thailand, Sri Lanka, maybe even Taiwan,

7
00:00:44,520 --> 00:00:48,400
 but I'll be back June 29th.

8
00:00:48,400 --> 00:00:54,320
 So for a whole month.

9
00:00:54,320 --> 00:01:04,150
 I'm going to see my teacher to let him know about our new

10
00:01:04,150 --> 00:01:06,560
 monastery, our new center here.

11
00:01:06,560 --> 00:01:15,760
 To get his blessing, to offer him some robes.

12
00:01:15,760 --> 00:01:19,320
 And make a stop in Sri Lanka.

13
00:01:19,320 --> 00:01:24,880
 Just because it's Sri Lanka.

14
00:01:24,880 --> 00:01:33,440
 Do some teaching in Sri Lanka as well I guess.

15
00:01:33,440 --> 00:01:35,710
 And then come back and there's lots of people already

16
00:01:35,710 --> 00:01:37,400
 signed up for meditation courses in

17
00:01:37,400 --> 00:01:39,400
 July and August.

18
00:01:39,400 --> 00:01:46,320
 So lots to do over the summer.

19
00:01:46,320 --> 00:01:53,600
 This quote is about bathing.

20
00:01:53,600 --> 00:02:01,320
 And this quote comes from the Watupa Masuta.

21
00:02:01,320 --> 00:02:04,160
 Discourse on the Simile of the cloth.

22
00:02:04,160 --> 00:02:18,880
 Just a rather famous sutta among Buddhists that talks about

23
00:02:18,880 --> 00:02:21,600
 purity.

24
00:02:21,600 --> 00:02:25,200
 It's an important simile.

25
00:02:25,200 --> 00:02:34,760
 A clean cloth, a pure cloth, can be dyed in any color dye.

26
00:02:34,760 --> 00:02:37,440
 And it takes the dye, it takes the color well.

27
00:02:37,440 --> 00:02:40,680
 So it's beautiful no matter what color you dye it.

28
00:02:40,680 --> 00:02:44,680
 You dip it in.

29
00:02:44,680 --> 00:02:51,510
 But a dirty cloth, an impure cloth, likewise doesn't matter

30
00:02:51,510 --> 00:02:54,240
 what dye you dip it in.

31
00:02:54,240 --> 00:02:57,160
 It's not going to be beautiful.

32
00:02:57,160 --> 00:03:01,160
 Doesn't matter what color.

33
00:03:01,160 --> 00:03:07,880
 What is the cause?

34
00:03:07,880 --> 00:03:08,880
 Why is that?

35
00:03:08,880 --> 00:03:10,880
 Because of the purity of the cloth.

36
00:03:10,880 --> 00:03:16,400
 And the mind is the same.

37
00:03:16,400 --> 00:03:24,830
 And he gives this simile, chite asankivite sugati patiganka

38
00:03:24,830 --> 00:03:25,400
.

39
00:03:25,400 --> 00:03:38,400
 Chite asankivite dugati patiganka.

40
00:03:38,400 --> 00:03:45,990
 Chite asankivite in the case of the mind that is defiled

41
00:03:45,990 --> 00:03:50,400
 dugati, an unpleasant future.

42
00:03:50,400 --> 00:03:58,400
 Patiganka without doubt is to be had without doubt.

43
00:03:58,400 --> 00:04:07,730
 Chite asankivite for a mind that is undefiled, that is pure

44
00:04:07,730 --> 00:04:09,400
. Happiness is a future without

45
00:04:09,400 --> 00:04:15,200
 doubt.

46
00:04:15,200 --> 00:04:19,120
 Life is unsure of aduangmi divitang.

47
00:04:19,120 --> 00:04:24,330
 Think of all the changes in our lives, all the changes we

48
00:04:24,330 --> 00:04:27,040
've gone through, all the uncertainty

49
00:04:27,040 --> 00:04:33,520
 we've had to face.

50
00:04:33,520 --> 00:04:42,720
 And without a guide, without a compass of some sort.

51
00:04:42,720 --> 00:04:50,240
 Future is very much uncertain.

52
00:04:50,240 --> 00:04:55,520
 In seeing this and knowing this, we try to find, we try to

53
00:04:55,520 --> 00:04:58,760
 direct and control our futures.

54
00:04:58,760 --> 00:05:04,680
 But mostly the way we work it is based on what we want.

55
00:05:04,680 --> 00:05:08,160
 Based on our preconceived desires and ideas.

56
00:05:08,160 --> 00:05:17,200
 And that doesn't ever make us happy.

57
00:05:17,200 --> 00:05:22,900
 We try to get happiness, but trying to get happiness doesn

58
00:05:22,900 --> 00:05:24,800
't make us happy.

59
00:05:24,800 --> 00:05:30,320
 All it does is make us want happiness more.

60
00:05:30,320 --> 00:05:36,960
 So there's nothing wrong with wanting happiness, right?

61
00:05:36,960 --> 00:05:41,800
 Theoretically, wanting happiness is the right idea.

62
00:05:41,800 --> 00:05:45,360
 Everyone should want to be happy.

63
00:05:45,360 --> 00:05:49,520
 You can open the window if you want.

64
00:05:49,520 --> 00:05:58,480
 We go about it the wrong way.

65
00:05:58,480 --> 00:06:03,400
 The Buddha taught purity for a very important reason, which

66
00:06:03,400 --> 00:06:05,720
 he states in this, which he

67
00:06:05,720 --> 00:06:06,720
 makes clear.

68
00:06:06,720 --> 00:06:13,130
 Which is why we teach the concept of karma, it's not to

69
00:06:13,130 --> 00:06:15,520
 make us unhappy.

70
00:06:15,520 --> 00:06:20,220
 The law of karma is to help us find happiness, it's to

71
00:06:20,220 --> 00:06:22,640
 provide that compass.

72
00:06:22,640 --> 00:06:24,840
 You don't have to worry about so many things in life.

73
00:06:24,840 --> 00:06:26,810
 You don't have to worry about what you're going to get,

74
00:06:26,810 --> 00:06:29,000
 what's going to come your way.

75
00:06:29,000 --> 00:06:30,000
 It's going to change.

76
00:06:30,000 --> 00:06:36,320
 Our lives are always going to change.

77
00:06:36,320 --> 00:06:38,880
 All we have to worry about is the source.

78
00:06:38,880 --> 00:06:46,390
 We can't control, we can't be sure what's going to come to

79
00:06:46,390 --> 00:06:48,240
 us in life.

80
00:06:48,240 --> 00:06:50,080
 We can be sure of one thing.

81
00:06:50,080 --> 00:06:55,320
 If the mind is pure, we'll be happy in the future.

82
00:06:55,320 --> 00:06:59,080
 If the mind is impure, we will suffer from it.

83
00:06:59,080 --> 00:07:03,700
 We'll be upset, we'll be unhappy, we'll have expectations

84
00:07:03,700 --> 00:07:05,920
 that are unmet, no disappointments,

85
00:07:05,920 --> 00:07:06,920
 and displeasure.

86
00:07:06,920 --> 00:07:11,710
 But if the mind is pure, we will do good things for

87
00:07:11,710 --> 00:07:15,760
 ourselves, we will do good things to help

88
00:07:15,760 --> 00:07:19,480
 others, we will make good friends wherever we go.

89
00:07:19,480 --> 00:07:23,630
 We don't know who those friends will be and we won't be

90
00:07:23,630 --> 00:07:26,160
 able to choose necessarily.

91
00:07:26,160 --> 00:07:32,910
 But we will choose those who we meet, we will choose to

92
00:07:32,910 --> 00:07:37,000
 associate with those who we meet

93
00:07:37,000 --> 00:07:39,520
 who are pure.

94
00:07:39,520 --> 00:07:44,270
 If our minds are impure, we will choose to associate with

95
00:07:44,270 --> 00:07:46,320
 those who are impure.

96
00:07:46,320 --> 00:07:51,360
 We'll hurt ourselves, we'll hurt each other.

97
00:07:51,360 --> 00:08:00,640
 Goodness, purity, all these things that we fail often to

98
00:08:00,640 --> 00:08:05,240
 grasp, to grasp all that are

99
00:08:05,240 --> 00:08:06,800
 right in front of us.

100
00:08:06,800 --> 00:08:11,600
 We all know what goodness is, we all know what purity is.

101
00:08:11,600 --> 00:08:19,800
 We fail to see it, we fail to grasp.

102
00:08:19,800 --> 00:08:23,380
 And so the Buddha gives this discourse and he talks about

103
00:08:23,380 --> 00:08:25,280
 related things, it's a very

104
00:08:25,280 --> 00:08:27,680
 good discourse, very much worth reading.

105
00:08:27,680 --> 00:08:33,140
 But then he gets to this quote and he talks about how one

106
00:08:33,140 --> 00:08:36,280
 becomes pure through insight

107
00:08:36,280 --> 00:08:38,880
 meditation and so on.

108
00:08:38,880 --> 00:08:48,530
 So then he says, "Aiyang uchati bhikkave bhikkhu, sinato

109
00:08:48,530 --> 00:08:52,520
 antarena sinaneenati."

110
00:08:52,520 --> 00:08:57,930
 This is called bhikkhu bhikkhus, oh bhikkhus, this is

111
00:08:57,930 --> 00:09:01,440
 called abhikkhu, abhikkhu who is

112
00:09:01,440 --> 00:09:09,220
 bathed by the internal bathing, cleansed by the internal

113
00:09:09,220 --> 00:09:11,200
 cleansing.

114
00:09:11,200 --> 00:09:14,040
 And he uses the word bathing specifically because there's

115
00:09:14,040 --> 00:09:15,440
 this one guy sitting in his

116
00:09:15,440 --> 00:09:19,280
 audience, who he wants to reach out to.

117
00:09:19,280 --> 00:09:23,680
 He goes to the commentary and says he knows that this guy

118
00:09:23,680 --> 00:09:25,960
 has the potential to become

119
00:09:25,960 --> 00:09:26,960
 a knight.

120
00:09:26,960 --> 00:09:33,470
 He specifically mentions this and right away this Brahmin,

121
00:09:33,470 --> 00:09:37,000
 Sundarika Bharadwajja, asks the

122
00:09:37,000 --> 00:09:38,000
 Buddha a question.

123
00:09:38,000 --> 00:09:47,000
 He says, "Ungat chati bhana bhavang gautamo bhakukang nad

124
00:09:47,000 --> 00:09:50,160
ing sinayitung."

125
00:09:50,160 --> 00:09:56,630
 Does the Venerable Gautama go to the Bahukha River to bathe

126
00:09:56,630 --> 00:09:57,120
?

127
00:09:57,120 --> 00:10:04,720
 And the Buddha says, "King Brahmanam bhukai anadiya, king b

128
00:10:04,720 --> 00:10:07,800
hukanadikari sati."

129
00:10:07,800 --> 00:10:13,880
 He says, "What's with the Bahukha River Brahman?

130
00:10:13,880 --> 00:10:19,200
 What's so special about it?

131
00:10:19,200 --> 00:10:24,800
 What will the Bahukha River do?

132
00:10:24,800 --> 00:10:25,800
 What is it?

133
00:10:25,800 --> 00:10:26,800
 What good is it?"

134
00:10:26,800 --> 00:10:35,050
 And he shrugs and says, "Loka samatha ye bhogotamo gautama,

135
00:10:35,050 --> 00:10:37,480
 bhukanadi bhujanasa punya samatha

136
00:10:37,480 --> 00:10:47,200
 ye bhogotamo, bhukanadi bhujanasa, bhukai apa una."

137
00:10:47,200 --> 00:10:49,200
 Anyway, he goes on.

138
00:10:49,200 --> 00:10:51,800
 He says, "Loka samatha" means the world.

139
00:10:51,800 --> 00:10:55,840
 I think it means it's actually a weird spelling here.

140
00:10:55,840 --> 00:10:56,840
 It should be loka.

141
00:10:56,840 --> 00:10:57,840
 Oh, I see.

142
00:10:57,840 --> 00:11:04,840
 It could be moka samatha.

143
00:11:04,840 --> 00:11:10,480
 The Bahukha River is understood, is agreed to samatha.

144
00:11:10,480 --> 00:11:16,000
 Samatha means it's agreed to be.

145
00:11:16,000 --> 00:11:26,720
 It's thought to be liberating.

146
00:11:26,720 --> 00:11:37,960
 It's thought to be, it's thought to be for goodness by many

147
00:11:37,960 --> 00:11:40,080
 people.

148
00:11:40,080 --> 00:11:47,040
 "Bhapakamang katang bhavi bhavahit."

149
00:11:47,040 --> 00:11:53,120
 It is thought to remove, to wash away sin bhapakam.

150
00:11:53,120 --> 00:11:55,440
 Bhapakam means evil deeds.

151
00:11:55,440 --> 00:12:02,490
 Evil deeds that have been done, it is thought to wash them

152
00:12:02,490 --> 00:12:03,560
 away.

153
00:12:03,560 --> 00:12:05,040
 This was a thing in India.

154
00:12:05,040 --> 00:12:08,920
 Having this sort of language was important to address the

155
00:12:08,920 --> 00:12:11,120
 idea that it's still prevalent

156
00:12:11,120 --> 00:12:16,250
 in India that the rivers will wash away your evil deeds,

157
00:12:16,250 --> 00:12:19,040
 wash away your impurities.

158
00:12:19,040 --> 00:12:22,280
 And again, talking about faith, faith does this.

159
00:12:22,280 --> 00:12:23,920
 People have faith in this.

160
00:12:23,920 --> 00:12:27,420
 I actually cultivate wholesomeness because they have this

161
00:12:27,420 --> 00:12:28,040
 faith.

162
00:12:28,040 --> 00:12:31,330
 Unfortunately, they cultivate also a lot of unwholesomeness

163
00:12:31,330 --> 00:12:33,040
 with their wrong views, which

164
00:12:33,040 --> 00:12:36,630
 causes them to ignore the importance of actually getting

165
00:12:36,630 --> 00:12:38,480
 beyond things like faith.

166
00:12:38,480 --> 00:12:45,750
 Because faith is good, not nearly enough, especially when

167
00:12:45,750 --> 00:12:48,960
 it's in the wrong thing.

168
00:12:48,960 --> 00:12:50,960
 So the Buddha shakes his head, I assume.

169
00:12:50,960 --> 00:12:54,600
 I don't think he would shake his head.

170
00:12:54,600 --> 00:13:00,800
 The Buddha says, "All these rivers do nothing.

171
00:13:00,800 --> 00:13:08,160
 Nityam pi balo pakandho kankakam moh na sujhati.

172
00:13:08,160 --> 00:13:10,560
 Kannaka moh na sujhati."

173
00:13:10,560 --> 00:13:15,670
 One is to constantly, consistently, one is ever to bathe in

174
00:13:15,670 --> 00:13:18,880
 these rivers, all these bhahukha

175
00:13:18,880 --> 00:13:24,600
 rivers, the adhika kantya, adhika kha river, Gaya river,

176
00:13:24,600 --> 00:13:26,400
 Sundari karena.

177
00:13:26,400 --> 00:13:33,520
 Kannaka moh na sujhati, "Can't purify black kamma."

178
00:13:33,520 --> 00:13:36,320
 Evil beings.

179
00:13:36,320 --> 00:13:40,280
 King Sundari kakareseati, what will the Sundari kareva do?

180
00:13:40,280 --> 00:13:42,280
 King Bhagaya, Payaga.

181
00:13:42,280 --> 00:13:45,600
 What will the Payaga river do?

182
00:13:45,600 --> 00:13:51,280
 What will the Bhahukha river do?

183
00:13:51,280 --> 00:13:58,160
 Vayring katta kimbi san na rang nahi nang suh day bhapa kam

184
00:13:58,160 --> 00:14:01,040
ina.

185
00:14:01,040 --> 00:14:03,040
 Kimbi san.

186
00:14:03,040 --> 00:14:05,040
 Let's look at the translation.

187
00:14:05,040 --> 00:14:15,040
 Ah, kimbi san is cruel deeds, I think cruel.

188
00:14:15,040 --> 00:14:20,040
 They cannot purify an evil doer, a man who has done cruel

189
00:14:20,040 --> 00:14:21,920
 and brutal deeds.

190
00:14:21,920 --> 00:14:23,920
 So what is true purity?

191
00:14:23,920 --> 00:14:33,120
 Sudas away sada bhaggu, bhaggu, sudasupo sato sada.

192
00:14:33,120 --> 00:14:38,920
 "When pure and hard has ever more, the feast of spring the

193
00:14:38,920 --> 00:14:40,640
 holy day."

194
00:14:40,640 --> 00:14:48,240
 Bhaggu is a special holy day.

195
00:14:48,240 --> 00:14:57,090
 So for Brahmins on the moon of Bhaggu, Bhaguna, they have a

196
00:14:57,090 --> 00:14:59,040
 day of purification.

197
00:14:59,040 --> 00:15:02,080
 It's a special purification day and probably in Hinduism

198
00:15:02,080 --> 00:15:06,640
 they still consider it to be a day of purification.

199
00:15:06,640 --> 00:15:13,090
 So the Buddha says one who is pure of mind always has a

200
00:15:13,090 --> 00:15:14,640
 holiday.

201
00:15:14,640 --> 00:15:18,440
 "One fair in act, one pure in heart brings his virtue to

202
00:15:18,440 --> 00:15:19,640
 perfection.

203
00:15:19,640 --> 00:15:23,080
 It is here, Brahman, that you should be to make yourself a

204
00:15:23,080 --> 00:15:24,640
 refuge for all beings.

205
00:15:24,640 --> 00:15:30,790
 If you speak no falsehood, nor work harm for living beings,

206
00:15:30,790 --> 00:15:32,640
 nor take what is offered not,

207
00:15:32,640 --> 00:15:37,900
 with faith and free from avarice would need for you to go

208
00:15:37,900 --> 00:15:44,640
 to Gaya, for any well will be your Gaya."

209
00:15:44,640 --> 00:15:57,640
 King Gaya, kahasi Gaya nghadbwa, udappano bhi te Gaya.

210
00:15:57,640 --> 00:16:02,830
 Actually I think Tamika gets it better than Bodhi, I'm not

211
00:16:02,830 --> 00:16:05,640
 sure why Bodhi translates it as that.

212
00:16:05,640 --> 00:16:15,640
 Udappana means well. Your well will be your Gaya.

213
00:16:15,640 --> 00:16:23,000
 Might as well be Gaya. Meaning the water in your well is

214
00:16:23,000 --> 00:16:25,640
 the same as Gaya.

215
00:16:25,640 --> 00:16:28,640
 It's the same water, it's just water.

216
00:16:28,640 --> 00:16:34,120
 And it means that you have Gaya everywhere you go, this

217
00:16:34,120 --> 00:16:36,640
 idea of purification.

218
00:16:36,640 --> 00:16:39,250
 People have strange ideas of purification. A lot of people

219
00:16:39,250 --> 00:16:42,640
 believe in the importance of purifying the body.

220
00:16:42,640 --> 00:16:50,640
 It's funny how much emphasis we put on the body.

221
00:16:50,640 --> 00:16:56,640
 Healthy body, healthy mind they say.

222
00:16:56,640 --> 00:17:01,100
 Which arguably there is some benefit to having a healthy

223
00:17:01,100 --> 00:17:03,640
 body or taking care of your body.

224
00:17:03,640 --> 00:17:06,170
 But it's funny that no one thinks to actually make the mind

225
00:17:06,170 --> 00:17:07,640
 healthy, and make the mind pure.

226
00:17:07,640 --> 00:17:11,240
 And it's not true, but we put far less emphasis on making

227
00:17:11,240 --> 00:17:12,640
 the mind healthy.

228
00:17:12,640 --> 00:17:16,930
 The mind we want just to get what we want. Even our

229
00:17:16,930 --> 00:17:22,070
 spirituality tends to be based on seeking pleasure, seeking

230
00:17:22,070 --> 00:17:22,640
 happiness.

231
00:17:22,640 --> 00:17:27,050
 Which you know, as I said, it's ostensibly good to want to

232
00:17:27,050 --> 00:17:28,640
 be happy, right?

233
00:17:28,640 --> 00:17:32,640
 Nobody wants to suffer. The problem is wanting isn't enough

234
00:17:32,640 --> 00:17:32,640
.

235
00:17:32,640 --> 00:17:36,640
 And if your focus is happiness you'll never be happy.

236
00:17:36,640 --> 00:17:42,640
 It's funny, huh? Your focus is trying to find happiness.

237
00:17:42,640 --> 00:17:46,640
 Your focus is the happiness.

238
00:17:46,640 --> 00:17:51,640
 It's a problem because happiness doesn't lead to happiness.

239
00:17:51,640 --> 00:17:58,640
 Happiness might lead to good, it might lead to bad.

240
00:17:58,640 --> 00:18:02,140
 But it's not happiness that leads to happiness, just like

241
00:18:02,140 --> 00:18:04,640
 it's not suffering that leads to suffering.

242
00:18:04,640 --> 00:18:10,850
 It's goodness that leads to happiness, it's evil that leads

243
00:18:10,850 --> 00:18:12,640
 to suffering.

244
00:18:12,640 --> 00:18:14,900
 This is why we don't have to worry about our future. We can

245
00:18:14,900 --> 00:18:16,640
't be sure what it's going to be like.

246
00:18:16,640 --> 00:18:21,970
 Sometimes that's disconcerting to know that our future

247
00:18:21,970 --> 00:18:26,640
 might not be what we thought it was going to be.

248
00:18:26,640 --> 00:18:30,720
 So we have to reassess and readjust, reevaluate what's

249
00:18:30,720 --> 00:18:32,640
 important about life.

250
00:18:32,640 --> 00:18:37,000
 It's not the situation, it's not whether I go to Asia or

251
00:18:37,000 --> 00:18:37,640
 not.

252
00:18:37,640 --> 00:18:41,280
 It's not whether I have a meditation center here in

253
00:18:41,280 --> 00:18:43,640
 Hamilton or somewhere else.

254
00:18:43,640 --> 00:18:47,560
 It's not whether I get this job or that job, it's not

255
00:18:47,560 --> 00:18:55,640
 whether I have these friends or this family.

256
00:18:55,640 --> 00:19:00,710
 It's about your mind, it's about your outlook, it's about

257
00:19:00,710 --> 00:19:03,640
 what you bring to the situation,

258
00:19:03,640 --> 00:19:07,400
 how you approach and how you live, how you are, who you are

259
00:19:07,400 --> 00:19:09,640
, and what you are.

260
00:19:09,640 --> 00:19:14,040
 Are you pure in mind with good intentions for yourself and

261
00:19:14,040 --> 00:19:18,900
 others, or corrupt in mind with evil intentions for

262
00:19:18,900 --> 00:19:20,640
 yourself and others?

263
00:19:20,640 --> 00:19:25,780
 This is what meditation is all about. It's about

264
00:19:25,780 --> 00:19:31,680
 cultivating misperity, striving to be pure in heart, pure

265
00:19:31,680 --> 00:19:32,640
 in mind.

266
00:19:32,640 --> 00:19:39,800
 That's our quote for tonight. It's about bathing. We should

267
00:19:39,800 --> 00:19:47,640
 all be sure to bathe internally.

268
00:19:47,640 --> 00:19:53,640
 Let's see if we have some questions.

269
00:19:53,640 --> 00:19:57,310
 We have a question about karma. We have a bunch of green

270
00:19:57,310 --> 00:19:59,640
 people, which is always nice to see.

271
00:19:59,640 --> 00:20:03,640
 Most of you are green. Thank you, that's appreciated.

272
00:20:03,640 --> 00:20:06,760
 We meditate together. Sometimes it's a hardship to have to

273
00:20:06,760 --> 00:20:07,640
 meditate at the same time.

274
00:20:07,640 --> 00:20:11,780
 I know for many people it's not possible because we're a

275
00:20:11,780 --> 00:20:12,640
 global.

276
00:20:12,640 --> 00:20:17,640
 It's the middle of the night for some people, I understand.

277
00:20:17,640 --> 00:20:20,890
 That people make the effort to come and meditate together

278
00:20:20,890 --> 00:20:22,640
 is very much appreciated.

279
00:20:22,640 --> 00:20:26,510
 So blame and good, bad aren't these concepts just an excuse

280
00:20:26,510 --> 00:20:28,640
 to judge yourself or others?

281
00:20:28,640 --> 00:20:32,640
 The thing about karma is it's not that it's a good thing.

282
00:20:32,640 --> 00:20:36,640
 It's not that we're happy that it's this way.

283
00:20:36,640 --> 00:20:40,140
 It's that, man, that's kind of a real bummer that it's this

284
00:20:40,140 --> 00:20:40,640
 way.

285
00:20:40,640 --> 00:20:47,470
 It's a bummer that we have to now deal with the things that

286
00:20:47,470 --> 00:20:50,640
 we've done in the past.

287
00:20:50,640 --> 00:20:54,640
 It's a bummer that we have to feel the effects of karma.

288
00:20:54,640 --> 00:20:58,640
 It would be great if we didn't have to.

289
00:20:58,640 --> 00:21:01,640
 I suppose in terms of the world now it would be horrific

290
00:21:01,640 --> 00:21:08,640
 because we'd kill each other and do whatever we want.

291
00:21:08,640 --> 00:21:12,640
 But it's not that way.

292
00:21:12,640 --> 00:21:16,640
 I guess it's how you look at it.

293
00:21:16,640 --> 00:21:22,180
 It's actually quite liberating to know that if you get mug

294
00:21:22,180 --> 00:21:22,640
ged,

295
00:21:22,640 --> 00:21:26,640
 that it's actually because you mugged people in the past.

296
00:21:26,640 --> 00:21:30,640
 It's hardly ever quite so simple.

297
00:21:30,640 --> 00:21:33,640
 It's hardly ever anywhere near as simple as that.

298
00:21:33,640 --> 00:21:36,660
 There are often trends of that sort and wouldn't that be

299
00:21:36,660 --> 00:21:37,640
 great to know?

300
00:21:37,640 --> 00:21:40,640
 That's what Buddhists do. When they get mugged, they say,

301
00:21:40,640 --> 00:21:43,660
 "It's probably my karma. I probably mugged that person in

302
00:21:43,660 --> 00:21:44,640
 the past life."

303
00:21:44,640 --> 00:21:47,640
 I mean, that's a gross oversimplification.

304
00:21:47,640 --> 00:21:53,280
 It ignores the fact that the other person is cultivating

305
00:21:53,280 --> 00:21:54,640
 new karma.

306
00:21:54,640 --> 00:21:56,640
 But it's quite freeing.

307
00:21:56,640 --> 00:21:59,660
 It again gets back to not worrying so much about your

308
00:21:59,660 --> 00:22:00,640
 circumstances

309
00:22:00,640 --> 00:22:04,980
 and getting a sense that all of that, or much of that, is

310
00:22:04,980 --> 00:22:06,640
 preordained.

311
00:22:06,640 --> 00:22:09,160
 As much of our circumstances is going to be beyond our

312
00:22:09,160 --> 00:22:09,640
 control.

313
00:22:09,640 --> 00:22:11,920
 Things are going to happen that we didn't foresee and

314
00:22:11,920 --> 00:22:13,640
 couldn't foresee.

315
00:22:13,640 --> 00:22:16,900
 A lot of that's just because of how we've situated

316
00:22:16,900 --> 00:22:17,640
 ourselves,

317
00:22:17,640 --> 00:22:23,640
 the situation we've got ourselves in.

318
00:22:23,640 --> 00:22:28,580
 I think I need Robin back on here to point the questions

319
00:22:28,580 --> 00:22:29,640
 out to me.

320
00:22:29,640 --> 00:22:32,860
 When meditation leads someone to be immune to hostility of

321
00:22:32,860 --> 00:22:33,640
 society

322
00:22:33,640 --> 00:22:36,640
 and help someone become a better version of themselves,

323
00:22:36,640 --> 00:22:41,020
 "Yeah, I saw that, but this was a person who hadn't med

324
00:22:41,020 --> 00:22:42,640
itated with us."

325
00:22:42,640 --> 00:22:45,640
 Thanks. Where did everyone go?

326
00:22:45,640 --> 00:22:48,640
 I didn't want to answer because I figured if they started

327
00:22:48,640 --> 00:22:48,640
 to meditate,

328
00:22:48,640 --> 00:22:52,640
 they'd get the answer.

329
00:22:52,640 --> 00:23:02,640
 Because I think the answer is simply yes.

330
00:23:02,640 --> 00:23:06,640
 Where the weekly practice interview happened during June,

331
00:23:06,640 --> 00:23:11,290
 should the time is going to be wonky, but I think the times

332
00:23:11,290 --> 00:23:12,640
 are okay for me.

333
00:23:12,640 --> 00:23:16,640
 I think one of them is like 5.30 or something in Sri Lanka.

334
00:23:16,640 --> 00:23:21,050
 Luckily Thailand is 12 hours, or 11 hours from me, time-

335
00:23:21,050 --> 00:23:21,640
wise.

336
00:23:21,640 --> 00:23:23,640
 But Sri Lanka is a little bit.

337
00:23:23,640 --> 00:23:26,640
 It's just going to mean they'll have interviews at 5.30,

338
00:23:26,640 --> 00:23:28,640
 starting at 5.30am.

339
00:23:28,640 --> 00:23:35,280
 So the evening interviews will be at 5.30am, which is fine,

340
00:23:35,280 --> 00:23:36,640
 of course.

341
00:23:36,640 --> 00:23:39,640
 The time is going to be all strange anyway.

342
00:23:39,640 --> 00:23:44,650
 You know how the brain is when we fly halfway across the

343
00:23:44,650 --> 00:23:45,640
 world.

344
00:23:45,640 --> 00:23:47,640
 It'll be okay though.

345
00:23:47,640 --> 00:23:50,640
 Wow, we've got a huge list today.

346
00:23:50,640 --> 00:23:54,640
 Almost 30 people meditated here.

347
00:23:54,640 --> 00:23:59,640
 And some people more than once.

348
00:23:59,640 --> 00:24:09,640
 Awesome.

349
00:24:09,640 --> 00:24:11,640
 Okay, that's all for the questions.

350
00:24:11,640 --> 00:24:13,640
 I'm going to say good night.

351
00:24:13,640 --> 00:24:16,640
 I'm wishing you all the best.

352
00:24:16,640 --> 00:24:20,640
 For those of you who rely on YouTube to get these teachings

353
00:24:20,640 --> 00:24:20,640
,

354
00:24:20,640 --> 00:24:24,290
 you might have to come on over to meditation.siri-mongolo.

355
00:24:24,290 --> 00:24:24,640
org

356
00:24:24,640 --> 00:24:29,440
 because I'm guessing some of it's going to be audio for the

357
00:24:29,440 --> 00:24:30,640
 next month.

358
00:24:30,640 --> 00:24:33,640
 We won't have so much video.

359
00:24:33,640 --> 00:24:38,640
 I'm not bringing my video camera to Asia.

360
00:24:38,640 --> 00:24:43,640
 Maybe someone else will record some talks I give,

361
00:24:43,640 --> 00:24:54,640
 otherwise you just have to be patient.

362
00:24:54,640 --> 00:24:57,640
 Heck, there's so many videos on YouTube already.

363
00:24:57,640 --> 00:25:00,640
 Just go watch some of the old ones.

364
00:25:00,640 --> 00:25:02,640
 Anyway, wishing you all the best.

365
00:25:02,640 --> 00:25:04,640
 Have a good night.

366
00:25:05,640 --> 00:25:30,640
 [

