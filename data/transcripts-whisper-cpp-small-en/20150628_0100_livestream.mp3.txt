 Okay, we're connected.
 Good evening, everyone.
 Broadcasting live from Stony Creek, Ontario.
 I don't know if anyone's actually listening tonight, but
 we have these recorded, so
 assume that there may be people listening.
 We talk a lot about meditation being difficult.
 There's no denying the fact that for most of us,
 meditation is difficult.
 It should be the most difficult thing we've ever done,
 because if we'd ever done something like this before,
 we'd have no need to do it again.
 It's not something you could have ever done before.
 Maybe that's not true.
 There's a certain stage that you can get to.
 You can practice some of the way and then drop off
 and leave the meditation behind.
 So that's possible that we've done some before.
 But compared to other things we do in this life,
 the difficulty of meditation, it's surprising.
 The wonderful thing about meditation is how deep it goes,
 how deep the changes are.
 It's something that surprises you.
 It's not like anything else.
 Think of any description you can use to what meditation
 does for you.
 It will ultimately fall short because it's teaching you
 something new.
 It's reaching down to the very depths of our being.
 It's like when a computer loses its power supply
 or when you mess with a computer's power supply.
 You can't go any deeper than that.
 It's the ultimate.
 It's at the core of who we are.
 It takes away that core.
 It's like an atom bomb, the splitting of an atom.
 You split the atom of a being.
 The point is it's profound, difficult, surprising.
 But the thing about difficulty, the difficulty of
 meditation,
 it's a unique sort of difficulty as well.
 In fact, I don't know that it's fair to say that the
 meditation itself is difficult.
 I guess in some way it's fair.
 When we say meditation is difficult, what we're usually
 talking about is
 the stress and suffering that is associated with inside
 meditation.
 Tranquility meditation is difficult, but it's difficult in
 a different way.
 It's difficult in terms of the challenge.
 It just takes a bit of work to cultivate tranquility.
 But inside meditation is difficult when you're doing it
 well.
 When you're doing it well, it's difficult because it shows
 you
 what you're doing wrong.
 So much of the difficulty of inside meditation isn't
 actually the meditation.
 It's that we're...
 It's the problems with our mind that we're seeing.
 Through the practice of meditation, we see what we're doing
 wrong.
 So the problem with the suffering that comes when we med
itate
 is not caused by the meditation.
 Inside meditation is designed to help you see the problem.
 It doesn't cause the problem. It doesn't cause suffering.
 It allows us to see it.
 It puts us in a position where we can just watch
 all the ways that we cause ourselves suffering.
 That's what inside meditation is for.
 So in a way you can't say that it's the meditation that's
 difficult.
 It's us that's difficult.
 Meditation is actually the process of fixing the problem,
 fixing the difficulty, making things easier.
 The Buddha is often compared to a doctor.
 You could think of the Buddha as like a mechanic as well,
 or Buddhism as like fixing an automobile or something.
 When you go in and you look around, you see the problem.
 When a doctor goes in or when a mechanic goes in, they see
 the problem.
 And so there's some difficulty involved in surgery or
 mechanics,
 to say the least, but it's about fixing things,
 about fixing problems, curing the problem.
 Meditation feels like this. It feels very difficult at
 times.
 Doing it again and again. Sometimes we don't want to do it.
 Our aversion to meditation, to inside meditation especially
,
 has nothing to do with the meditation itself,
 because the meditation is wonderful.
 The thing about insight meditation is you can't cling to it
.
 There's nothing to hold on to.
 With any other exercise you can hold on to something,
 hold on to a goal like a diet, and then suddenly I'll look
 more beautiful,
 so you hold on to that, or I'll exercise and that'll make
 me stronger,
 and so you hold on to that.
 Even tranquility meditation, I'll feel calm.
 Most people meditate because they have a goal that they
 want to feel calm.
 That's why eventually a meditator wants to run away.
 In insight meditation there comes a time when you just want
 to stop,
 because you can't hold on anymore.
 You start to freak out and there's nothing to hold on to.
 You get the calm and that doesn't satisfy you.
 You can feel happy or you learn things and it just doesn't
 satisfy you.
 Your mind is looking for satisfaction.
 You can't find it anywhere.
 In insight meditation is difficult, difficult because it
 shows us things
 we otherwise don't want to see.
 In that sense it's not really difficult at all because all
 of those things
 are already present in our lives and could come up at any
 time.
 They're already there.
 What we're talking about is the difficulties of life.
 The problem is the reason as we come to meditate is often
 to escape.
 These problems which is really the root of the problem.
 It's reacting to things.
 The meditation teacher's job is often not entirely honest.
 It actually doesn't really matter how honest you are
 because in the beginning
 it's hard to get through.
 You don't really get through to the meditator.
 You can tell them how difficult it's going to be.
 They don't realize the sense in which it's difficult.
 Meditation is not difficult. We're difficult from the
 problem.
 But until you meditate you can't really understand that.
 You can't really understand the depth of what that means.
 When you meditate you start to see.
 The difficulty in meditation is really that you can't cling
 to it.
 So you practice and you feel really good that you've gotten
.
 You feel really confident that you've gotten somewhere.
 But anytime you think about it you can't hold on to it and
 say,
 "Yeah, I'm going to go and do that."
 There's no desire that can arise.
 You can in the beginning if you have ideas about goals and
 so on.
 But true meditation, true insight, you can't cling to it.
 It's very tricky in that way.
 But we shouldn't despair.
 Either way we shouldn't despair because meditation brings
 up things
 that we're not able to deal with.
 Or we shouldn't despair that meditation is difficult, is a
 challenge or whatever.
 Why shouldn't we despair? We shouldn't despair because we
've already done
 very difficult things to get where we are today.
 We're beings who have survived, who have succeeded through
 great difficulty,
 which is a good sign.
 If we're looking for encouragement, something good about
 ourselves,
 we should look here and realize the difficulty that we've
 already faced.
 We face difficulty just in being born a human being.
 No matter who we are or the bad things we've done or how
 guilty we feel
 about how useless or lazy we are, no matter how deep those
 feelings can go.
 We've got to admit we did something right.
 It has to be accepted that we're born as a being that's not
 easy to be born.
 Whether you believe in past lives or not, it's something
 special.
 There's something special about us.
 Suppose you want to believe that it was just chance or not
 chance,
 but some totally meaningless reason, natural selection,
 however you put it, or no reason at all I was born a human
 being.
 Something more interesting, more complex, more powerful
 than being born as an ordinary animal.
 In Buddhism we believe that that was something good that we
 did,
 something powerful that we did in the past.
 So if you believe that we've done that.
 Even if you don't believe that one,
 we've done something very difficult to live our lives
 successfully,
 to not die yet,
 to not lose a limb, to learn to speak, to learn to read,
 to learn to get a job and work and all these things.
 We've made a lot of mistakes, surely,
 but we've done good things to get where we are.
 We've managed to survive, managed to learn enough to
 continue our lives.
 Beyond that we have gained the opportunity to meet with
 spiritual teachings,
 with not just spiritual teachings but Buddhist teachings.
 I know this because you're listening right now.
 I consider this to be a Buddhist teaching, the Buddha
 taught.
 "Kit-chang-sadhama-savanam" or "desanam"
 It's hard to obtain the teaching of the Dhamma.
 It's not an easy thing to meet with.
 "Kit-chou-manus" or "Kit-cho"
 Difficult, I can't remember the poly, difficult to obtain a
 human birth.
 "Kit-chou-machan-jivitam" Difficult is a human life.
 All these teachings of the Buddha,
 so all this teaching of the Buddha, difficult to find.
 You've got good luck at the least if you believe it's all
 luck or whatever.
 But no, we don't believe that.
 Something praiseworthy.
 We've done a difficult thing to meet with the Buddhist
 teaching as a human being.
 And finally we've done a very difficult thing by taking the
 opportunity
 to listen to the Buddhist teaching and study the Buddhist
 teaching.
 I was just today in Calla, the east, north of Toronto.
 They had an ordination and establishing of the boundaries
 for an ordination hall.
 It's quite neat to see.
 It's quite interesting we have these people who have met
 with the Buddhist teaching.
 Sometimes you look and you see people who are even born
 Buddhists.
 Some of them there obviously knew some things,
 but it was also clear that some of the kids there who were
 ordained,
 still hadn't met with the Buddhist teaching,
 still hadn't listened, still hadn't heard,
 and certainly hadn't come to the point where they were
 actually practicing.
 So they had these novices that were sitting during one of
 the ceremonies.
 They were sitting or waiting around and they were sitting
 talking about Zorro,
 someone killing Zorro with a knife.
 I'm still not sure whether it was a game they were talking
 about
 or a movie or some imagination, I don't know.
 But it was certainly quite off topic.
 Killing Zorro with a knife.
 That was how someone they knew killed Zorro with a knife.
 It was a video game of sorts.
 Anyway, these sorts of things and just watching it.
 Even if you meet with the Buddhist teaching with all of
 them,
 in this time the whole world really has the opportunity to
 practice,
 not the whole world, but throughout the world there are
 opportunities to practice.
 The Buddhist teaching and how many people don't ever take
 it.
 Buddha said it's difficult to find teachings.
 Then he said it's difficult for people even to want to.
 He said few are those people who actually want to practice.
 But then he said fewer still.
 Few among those people who actually want to practice.
 Are those whoever get around to practicing?
 So we've done all of this.
 That's something.
 So we shouldn't despair.
 We shouldn't take the difficulty that we encounter in the
 practice too seriously.
 We should try to encourage ourselves in this way.
 We appreciate ourselves and our potential.
 Meditation isn't all that hard.
 It's the most difficult thing we've ever done, but in the
 end it's us that's the problem.
 If we can see that and just get rid of that whole ego thing
,
 then meditation isn't so difficult.
 It actually gets easier the more you practice.
 Easier, more peaceful, happier, all of these things come.
 My teacher said it's like eating sugar cane.
 If you take the whole sugar cane,
 the bottom is where all the sweet is and the top is not so
 sweet.
 So they said that ordinary pleasures and pursuits
 are like eating sugar cane from the bottom up.
 It starts off very sweet, but it gets less and less as you
 eat.
 Dhamma practice is the opposite.
 In the beginning it's not very sweet at all, but as you
 continue to practice,
 it gets sweeter into its sweetness is beyond compare.
 That's the dhamma for tonight.
 Thank you all for tuning in. Have a good night.
