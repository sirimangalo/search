1
00:00:00,000 --> 00:00:16,090
 Welcome everyone to our international celebration of the

2
00:00:16,090 --> 00:00:21,800
 anniversary of Buddhism, really.

3
00:00:21,800 --> 00:00:27,080
 This is the anniversary of the first day that the Buddha

4
00:00:27,080 --> 00:00:30,080
 gave an official teaching and the

5
00:00:30,080 --> 00:00:36,520
 first day that there was someone who put into practice and

6
00:00:36,520 --> 00:00:40,640
 came to realize the truth according

7
00:00:40,640 --> 00:00:45,960
 to the Buddha's teaching.

8
00:00:45,960 --> 00:00:49,920
 And so I'll talk a little bit about that after, but we

9
00:00:49,920 --> 00:00:52,320
 should be aware that this is a very

10
00:00:52,320 --> 00:00:54,480
 important day.

11
00:00:54,480 --> 00:01:01,760
 It's also the day before the rainy season.

12
00:01:01,760 --> 00:01:07,040
 In India they split the year up into three seasons.

13
00:01:07,040 --> 00:01:13,880
 So this is the beginning of the wet season.

14
00:01:13,880 --> 00:01:18,900
 And so this has become a tradition based on the conditions

15
00:01:18,900 --> 00:01:21,160
 in India that this would be

16
00:01:21,160 --> 00:01:27,210
 the time of year where we undertake intensive practice of

17
00:01:27,210 --> 00:01:30,040
 the Buddha's teaching.

18
00:01:30,040 --> 00:01:33,120
 Because when it's raining you won't move around as much and

19
00:01:33,120 --> 00:01:34,880
 so the monks are required to stay

20
00:01:34,880 --> 00:01:36,720
 put.

21
00:01:36,720 --> 00:01:40,570
 Starting tomorrow I won't be going anywhere for three

22
00:01:40,570 --> 00:01:41,400
 months.

23
00:01:41,400 --> 00:01:44,440
 Not that I go anywhere anyway.

24
00:01:44,440 --> 00:01:47,830
 The Buddha required that the monks stay put for three out

25
00:01:47,830 --> 00:01:49,560
 of the four months of the rainy

26
00:01:49,560 --> 00:01:50,560
 season.

27
00:01:50,560 --> 00:01:54,880
 You can either do the first three months or the second

28
00:01:54,880 --> 00:01:56,360
 three months.

29
00:01:56,360 --> 00:01:59,920
 And so we'll be starting that tomorrow.

30
00:01:59,920 --> 00:02:07,730
 So this is an important landmark or it's a point in the

31
00:02:07,730 --> 00:02:11,400
 year, it's the beginning of the

32
00:02:11,400 --> 00:02:13,000
 year in Buddhism.

33
00:02:13,000 --> 00:02:19,040
 That's how we count the years is by rains.

34
00:02:19,040 --> 00:02:22,640
 And this would be a time for everyone to...

35
00:02:22,640 --> 00:02:26,990
 It's an excuse for us to undertake reflection especially

36
00:02:26,990 --> 00:02:29,760
 since this is the time where practice

37
00:02:29,760 --> 00:02:32,360
 of the Buddha's teaching first began.

38
00:02:32,360 --> 00:02:37,440
 This is the time of year where these five monks came to

39
00:02:37,440 --> 00:02:40,640
 listen to the Buddha's teaching

40
00:02:40,640 --> 00:02:47,840
 and then spent three months practicing it with the Buddha

41
00:02:47,840 --> 00:02:49,400
 in India.

42
00:02:49,400 --> 00:02:51,530
 For people who know about these things or for people who

43
00:02:51,530 --> 00:02:52,800
 are interested in these things

44
00:02:52,800 --> 00:02:59,960
 this is an important day of the year in Buddhism.

45
00:02:59,960 --> 00:03:07,080
 So with that in mind we have something, some way of

46
00:03:07,080 --> 00:03:11,800
 recognizing it today and that is we're

47
00:03:11,800 --> 00:03:16,120
 going to take the refuges and precepts.

48
00:03:16,120 --> 00:03:20,680
 We're going to commit ourselves to the practice of the

49
00:03:20,680 --> 00:03:23,920
 Buddha's teaching and the abstention

50
00:03:23,920 --> 00:03:30,560
 from those things which are detrimental for our practice.

51
00:03:30,560 --> 00:03:38,060
 And so we have this ceremony and it's simply an affirmation

52
00:03:38,060 --> 00:03:41,920
 in our minds of the intentions

53
00:03:41,920 --> 00:03:50,580
 that we already have reminding us of our intentions and of

54
00:03:50,580 --> 00:03:52,440
 our path.

55
00:03:52,440 --> 00:03:55,590
 So the way it works is we have this text below and you can

56
00:03:55,590 --> 00:03:57,800
 choose from either the five precepts

57
00:03:57,800 --> 00:03:59,400
 or the eight precepts.

58
00:03:59,400 --> 00:04:02,700
 But you have to know what you're getting yourself into so

59
00:04:02,700 --> 00:04:04,900
 if you choose the eight precepts it's

60
00:04:04,900 --> 00:04:08,960
 really important that you actually intend to keep them.

61
00:04:08,960 --> 00:04:12,360
 Now normally people would only keep these during periods of

62
00:04:12,360 --> 00:04:14,280
 intensive meditation practice

63
00:04:14,280 --> 00:04:18,080
 but it is possible to keep them on a daily basis for

64
00:04:18,080 --> 00:04:20,800
 certain people depending on your

65
00:04:20,800 --> 00:04:26,670
 lifestyle but you should read through them and understand

66
00:04:26,670 --> 00:04:28,160
 them first.

67
00:04:28,160 --> 00:04:33,060
 If that's not feasible for you or you're not sure about the

68
00:04:33,060 --> 00:04:35,740
 eight precepts then generally

69
00:04:35,740 --> 00:04:39,470
 we say a person who's interested in practicing these things

70
00:04:39,470 --> 00:04:41,280
 is required to take the five

71
00:04:41,280 --> 00:04:43,540
 precepts.

72
00:04:43,540 --> 00:04:46,900
 It's not an institutional requirement.

73
00:04:46,900 --> 00:04:54,720
 It's a requirement based on the reality of it, the reality

74
00:04:54,720 --> 00:04:56,680
 of the mind.

75
00:04:56,680 --> 00:05:00,510
 If you don't keep these things your practice will not bring

76
00:05:00,510 --> 00:05:02,660
 fruit and there's the requirement.

77
00:05:02,660 --> 00:05:07,650
 If you don't keep these there's no way you can really

78
00:05:07,650 --> 00:05:10,400
 advance in the practice.

79
00:05:10,400 --> 00:05:12,540
 It's quite difficult.

80
00:05:12,540 --> 00:05:15,790
 There's many other things that make it difficult or could

81
00:05:15,790 --> 00:05:17,720
 pose problems but these five are

82
00:05:17,720 --> 00:05:24,520
 singled out as quite extreme in their effect on the mind.

83
00:05:24,520 --> 00:05:28,010
 Their basic morality not to kill, not to steal, not to

84
00:05:28,010 --> 00:05:31,600
 cheat, not to lie and not to take intoxicants,

85
00:05:31,600 --> 00:05:35,680
 drugs and alcohol.

86
00:05:35,680 --> 00:05:41,340
 I think for most people it would be just taking the five

87
00:05:41,340 --> 00:05:44,800
 precepts but you're welcome to take

88
00:05:44,800 --> 00:05:57,660
 whatever precepts appear to you doable and pollinable that

89
00:05:57,660 --> 00:06:05,120
 are appropriate for you.

90
00:06:05,120 --> 00:06:08,010
 How it works is I'm going to have you repeat after me for

91
00:06:08,010 --> 00:06:09,320
 the whole ceremony.

92
00:06:09,320 --> 00:06:13,610
 We'll do it piece by piece and I'm recording this so you'll

93
00:06:13,610 --> 00:06:15,720
 be able to go over it again

94
00:06:15,720 --> 00:06:25,880
 on the website and practice it for next time as well.

95
00:06:25,880 --> 00:06:28,940
 Without further ado here we'll get started unless someone

96
00:06:28,940 --> 00:06:30,600
 has any questions or problems.

97
00:06:30,600 --> 00:06:47,000
 Okay, well if problems come up or if you have questions

98
00:06:47,000 --> 00:06:56,760
 afterwards you're welcome to ask

99
00:06:56,760 --> 00:07:00,080
 them after.

100
00:07:00,080 --> 00:07:02,780
 Starting at the beginning you can click on either the five

101
00:07:02,780 --> 00:07:04,360
 precepts or the eight precepts.

102
00:07:04,360 --> 00:07:07,880
 We're starting at what is the request.

103
00:07:07,880 --> 00:07:12,100
 I've changed the text a little bit from the original source

104
00:07:12,100 --> 00:07:14,160
 because it's actually more

105
00:07:14,160 --> 00:07:18,160
 appropriate to do it as one person.

106
00:07:18,160 --> 00:07:22,690
 The poly originally said we request the precepts but you're

107
00:07:22,690 --> 00:07:25,200
 doing it individually and you don't

108
00:07:25,200 --> 00:07:28,600
 know whether anyone else is taking the same precepts.

109
00:07:28,600 --> 00:07:31,550
 We should all just do it individually and say I am

110
00:07:31,550 --> 00:07:33,400
 requesting these things.

111
00:07:33,400 --> 00:07:37,280
 Especially since the rest of the ceremony already says I do

112
00:07:37,280 --> 00:07:39,320
 this, I do that, I undertake

113
00:07:39,320 --> 00:07:40,320
 not to do this.

114
00:07:40,320 --> 00:07:43,090
 It's all singular except for the request so the request

115
00:07:43,090 --> 00:07:44,680
 also should be singular which

116
00:07:44,680 --> 00:07:47,600
 I've done a change.

117
00:07:47,600 --> 00:07:49,880
 So repeat after me.

118
00:07:49,880 --> 00:07:56,910
 And I'm going to be repeating the eight precepts so I will

119
00:07:56,910 --> 00:08:00,280
 say ata silani yajami at the end

120
00:08:00,280 --> 00:08:01,280
 there.

121
00:08:01,280 --> 00:08:06,030
 For those of you taking the five precepts you will say pan

122
00:08:06,030 --> 00:08:07,960
cha silani yajami.

123
00:08:07,960 --> 00:08:10,560
 And when we go to the precepts they'll be a little bit

124
00:08:10,560 --> 00:08:12,280
 different and I'll walk you through

125
00:08:12,280 --> 00:08:13,280
 that.

126
00:08:13,280 --> 00:08:16,010
 The third precept is different for eight and for five and I

127
00:08:16,010 --> 00:08:17,240
'll repeat both and have you

128
00:08:17,240 --> 00:08:21,320
 repeat them after me depending on which set you're taking.

129
00:08:21,320 --> 00:08:24,820
 The sixth, seventh and eighth precepts are obviously when

130
00:08:24,820 --> 00:08:26,720
 you're taking the five precepts

131
00:08:26,720 --> 00:08:30,520
 just stay quiet during that time and wait for us to finish.

132
00:08:30,520 --> 00:08:33,610
 If you're taking the eight precepts just continue on with

133
00:08:33,610 --> 00:08:34,000
 us.

134
00:08:34,000 --> 00:08:38,230
 And one last thing is in the top right corner of the text

135
00:08:38,230 --> 00:08:40,880
 frame there's a plus sign beside

136
00:08:40,880 --> 00:08:41,880
 the word pronunciation.

137
00:08:41,880 --> 00:08:47,150
 If you click on the plus you get this little box that pops

138
00:08:47,150 --> 00:08:49,920
 up and it has pronunciation

139
00:08:49,920 --> 00:08:53,360
 hints so try to keep those in mind because some of the

140
00:08:53,360 --> 00:08:55,440
 letters are not going to be the

141
00:08:55,440 --> 00:09:01,360
 same as they are in English.

142
00:09:01,360 --> 00:09:07,880
 C is always cha and the M with the dot under it is an ung.

143
00:09:07,880 --> 00:09:13,050
 So if you see an M with a dot under it it's not an um it's

144
00:09:13,050 --> 00:09:14,400
 ung and g.

145
00:09:14,400 --> 00:09:17,760
 Those are the two big ones.

146
00:09:17,760 --> 00:09:20,400
 And the V is a W.

147
00:09:20,400 --> 00:09:23,640
 Few things that you have to remember.

148
00:09:23,640 --> 00:09:41,640
 Okay so repeat after me.

149
00:09:41,640 --> 00:09:43,020
 I am the

150
00:09:43,020 --> 00:10:03,520
 I am the

151
00:10:03,520 --> 00:10:20,400
 I am the

152
00:10:20,400 --> 00:10:36,280
 I am the

153
00:10:36,280 --> 00:10:52,160
 I am the

154
00:10:52,160 --> 00:11:15,040
 I am the

155
00:11:15,040 --> 00:11:31,920
 I am the

156
00:11:31,920 --> 00:11:54,800
 I am the

157
00:11:54,800 --> 00:12:10,680
 I am the

158
00:12:10,680 --> 00:12:23,680
 I am the

159
00:12:23,680 --> 00:12:43,560
 I am the

160
00:12:43,560 --> 00:13:06,440
 I am the

161
00:13:06,440 --> 00:13:23,320
 I am the

162
00:13:23,320 --> 00:13:46,200
 I am the

163
00:13:46,200 --> 00:14:03,080
 I am the

164
00:14:03,080 --> 00:14:19,960
 I am the

165
00:14:19,960 --> 00:14:42,840
 I am the

166
00:14:42,840 --> 00:14:58,720
 I am the

167
00:14:58,720 --> 00:15:07,720
 I am the

168
00:15:07,720 --> 00:15:23,600
 I am the

169
00:15:23,600 --> 00:15:36,600
 I am the

170
00:15:36,600 --> 00:15:52,480
 I am the

171
00:15:52,480 --> 00:16:10,360
 I am the

172
00:16:10,360 --> 00:16:26,240
 I am the

173
00:16:26,240 --> 00:16:49,120
 I am the

174
00:16:49,120 --> 00:17:05,000
 I am the

175
00:17:05,000 --> 00:17:15,000
 I am the

176
00:17:15,000 --> 00:17:30,880
 I am the

177
00:17:30,880 --> 00:17:46,760
 I am the

178
00:17:46,760 --> 00:17:56,760
 I am the

179
00:17:56,760 --> 00:18:12,640
 I am the

180
00:18:12,640 --> 00:18:28,520
 I am the

181
00:18:28,520 --> 00:18:38,520
 I am the

182
00:18:38,520 --> 00:18:54,400
 I am the

183
00:18:54,400 --> 00:19:04,400
 I am the

184
00:19:04,400 --> 00:19:11,280
 I am the

185
00:19:11,280 --> 00:19:17,280
 I am the

186
00:19:17,280 --> 00:19:20,280
 I am the

187
00:19:20,280 --> 00:19:24,280
 I am the

188
00:19:24,280 --> 00:19:27,280
 I am the

189
00:19:27,280 --> 00:19:29,280
 I am the

190
00:19:29,280 --> 00:19:31,280
 I am the

191
00:19:31,280 --> 00:19:33,280
 I am the

192
00:19:33,280 --> 00:19:35,280
 I am the

193
00:19:35,280 --> 00:19:37,280
 I am the

194
00:19:37,280 --> 00:19:39,280
 I am the

195
00:19:39,280 --> 00:19:41,280
 I am the

196
00:19:41,280 --> 00:19:43,280
 I am the

197
00:19:43,280 --> 00:19:45,280
 I am the

198
00:19:45,280 --> 00:19:47,280
 I am the

199
00:19:47,280 --> 00:19:49,280
 I am the

200
00:19:49,280 --> 00:19:51,280
 I am the

201
00:19:51,280 --> 00:19:53,280
 I am the

202
00:19:53,280 --> 00:19:55,280
 I am the

203
00:19:55,280 --> 00:19:57,280
 I am the

204
00:19:57,280 --> 00:19:59,280
 I am the

205
00:19:59,280 --> 00:20:01,280
 I am the

206
00:20:01,280 --> 00:20:03,280
 I am the

207
00:20:03,280 --> 00:20:05,280
 I am the

208
00:20:05,280 --> 00:20:07,280
 I am the

