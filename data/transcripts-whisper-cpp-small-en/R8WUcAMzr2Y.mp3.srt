1
00:00:00,000 --> 00:00:04,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,000 --> 00:00:12,290
 Today we continue on with verses 244 and 245, which read as

3
00:00:12,290 --> 00:00:13,000
 follows.

4
00:00:13,000 --> 00:00:25,000
 Sūtjīvanga hirikeṇa kakasurena dangsina

5
00:00:25,000 --> 00:00:32,000
 pakandinā pagambhena sankiditena jivitam

6
00:00:32,000 --> 00:00:39,000
 hirimata chadu jivang nityāng sūtjīnga vesina

7
00:00:39,000 --> 00:00:46,000
 aline na pagambhena sundajīvena pasata

8
00:00:46,000 --> 00:00:52,000
 which means

9
00:00:52,000 --> 00:00:59,000
 easy is the life of one who is shameless

10
00:00:59,000 --> 00:01:11,850
 brazen like a crow, impudent, bold, reckless, living a

11
00:01:11,850 --> 00:01:16,000
 corrupted life

12
00:01:16,000 --> 00:01:21,000
 and hard is the life of the conscientious

13
00:01:21,000 --> 00:01:27,000
 ever pure in their search

14
00:01:27,000 --> 00:01:32,000
 vigilant, dedicated

15
00:01:32,000 --> 00:01:39,000
 not boastful or bold or impudent or brazen

16
00:01:39,000 --> 00:01:45,000
 living a pure life with vision

17
00:01:45,000 --> 00:01:53,100
 so this set of verses was taught in regards to a very short

18
00:01:53,100 --> 00:01:54,000
 story

19
00:01:54,000 --> 00:02:01,000
 the occasion of a certain monk who traded medical services

20
00:02:01,000 --> 00:02:03,000
 for food

21
00:02:03,000 --> 00:02:06,000
 and so he had been a doctor perhaps before he became a monk

22
00:02:06,000 --> 00:02:10,690
 and once he became a monk he realized that by continuing to

23
00:02:10,690 --> 00:02:12,000
 offer medical services

24
00:02:12,000 --> 00:02:25,000
 he could acquire luxurious requisites

25
00:02:25,000 --> 00:02:30,000
 so he would spend his time tending to rich laypeople

26
00:02:30,000 --> 00:02:36,260
 and in return getting the choices to foods and robes and

27
00:02:36,260 --> 00:02:38,000
 medicines and so on

28
00:02:38,000 --> 00:02:42,000
 and one day when he was coming back from getting food he

29
00:02:42,000 --> 00:02:42,000
 saw Sariputta

30
00:02:42,000 --> 00:02:49,000
 the Buddha's chief disciple accompanied by a group of monks

31
00:02:49,000 --> 00:02:49,000
 I guess

32
00:02:49,000 --> 00:02:52,110
 and he saw Sariputta and he said, "Venerable sir, please, I

33
00:02:52,110 --> 00:02:53,000
've received all this food

34
00:02:53,000 --> 00:02:57,640
 come partake in it" and he explained to him, he said, "I

35
00:02:57,640 --> 00:02:59,000
 offer my medical services to the people

36
00:02:59,000 --> 00:03:04,020
 and it's such a great idea because they give me all this

37
00:03:04,020 --> 00:03:05,000
 good food"

38
00:03:05,000 --> 00:03:09,640
 and Sariputta looked at him and listened to what he had to

39
00:03:09,640 --> 00:03:10,000
 say

40
00:03:10,000 --> 00:03:14,000
 and turned around and walked away

41
00:03:14,000 --> 00:03:19,630
 now the monks hearing this went to see the Buddha and told

42
00:03:19,630 --> 00:03:21,000
 this to the Buddha

43
00:03:21,000 --> 00:03:26,910
 and the Buddha simply said, "Indeed, that monk is living an

44
00:03:26,910 --> 00:03:28,000
 easy life

45
00:03:28,000 --> 00:03:34,450
 in his corrupt ways providing him with a pure life and it's

46
00:03:34,450 --> 00:03:36,000
 hard to be someone who is pure

47
00:03:36,000 --> 00:03:43,000
 their life will be hard" and then he taught these verses

48
00:03:43,000 --> 00:03:49,460
 it's a deceptively profound teaching and I think hard to

49
00:03:49,460 --> 00:03:53,000
 understand if you investigate it

50
00:03:53,000 --> 00:04:00,000
 it's a hard one to appreciate and to really get behind

51
00:04:00,000 --> 00:04:03,190
 because it appears to be saying that what we're doing, all

52
00:04:03,190 --> 00:04:06,000
 of what we're doing is the wrong path

53
00:04:06,000 --> 00:04:10,490
 we are purposefully making life harder for ourselves, that

54
00:04:10,490 --> 00:04:12,000
's what it sounds like

55
00:04:12,000 --> 00:04:18,510
 because the options, being corrupt or at the very least

56
00:04:18,510 --> 00:04:20,000
 seeking out pleasure

57
00:04:20,000 --> 00:04:24,340
 taking out the sources of pleasure, that's the right way,

58
00:04:24,340 --> 00:04:28,000
 it leads to well pleasure

59
00:04:28,000 --> 00:04:32,000
 it seems to go very much against the law of karma

60
00:04:32,000 --> 00:04:37,600
 which we understand to mean that being of a pure mind or

61
00:04:37,600 --> 00:04:41,000
 having a pure character

62
00:04:41,000 --> 00:04:45,000
 living an ethical life would lead to good things

63
00:04:45,000 --> 00:04:48,330
 why does the Buddha appear to be saying the opposite? That

64
00:04:48,330 --> 00:04:50,000
 it's corruption that leads to good things

65
00:04:50,000 --> 00:04:53,000
 an easy life

66
00:04:53,000 --> 00:04:58,390
 well we know this to be true if you look at the world, this

67
00:04:58,390 --> 00:04:59,000
 is the case

68
00:04:59,000 --> 00:05:09,000
 it's a very simple fact that all other things being equal

69
00:05:09,000 --> 00:05:14,750
 chasing after the pleasure that is available is going to

70
00:05:14,750 --> 00:05:18,000
 lead to more pleasure than not chasing after the pleasure

71
00:05:18,000 --> 00:05:22,000
 most obviously there's no question here

72
00:05:22,000 --> 00:05:26,420
 when you have a set of potentials in front of you, seizing

73
00:05:26,420 --> 00:05:27,000
 the potential

74
00:05:27,000 --> 00:05:32,430
 seize the day, eat, drink and be merry, do it because the

75
00:05:32,430 --> 00:05:34,000
 opportunity is there

76
00:05:34,000 --> 00:05:41,240
 taking that opportunity makes life more pleasurable,

77
00:05:41,240 --> 00:05:45,000
 generally makes life easier

78
00:05:45,000 --> 00:05:48,010
 we have to appreciate this, we have to understand this

79
00:05:48,010 --> 00:05:53,000
 because it's the essence of how we think of happiness

80
00:05:53,000 --> 00:05:56,000
 we see this, this is an obvious truth

81
00:05:56,000 --> 00:06:01,980
 and so our thinking of how to obtain happiness is always

82
00:06:01,980 --> 00:06:04,000
 along these lines

83
00:06:04,000 --> 00:06:08,470
 it's the way in the way in the conventional world and it's

84
00:06:08,470 --> 00:06:10,000
 the way even in meditation

85
00:06:10,000 --> 00:06:14,000
 this leads over into our meditation practice

86
00:06:14,000 --> 00:06:17,710
 where it's often quite discouraging when your practice on a

87
00:06:17,710 --> 00:06:23,000
 Buddhist path is unpleasant, is hard

88
00:06:23,000 --> 00:06:32,000
 and it seems to be moving away from pleasure in many ways

89
00:06:32,000 --> 00:06:35,720
 the Buddha even acknowledged this and reminded us to

90
00:06:35,720 --> 00:06:40,000
 acknowledge the gratification of sensuality

91
00:06:40,000 --> 00:06:46,000
 that there is the gratification and it is there

92
00:06:46,000 --> 00:06:53,160
 but it doesn't take much to see the potential danger, now

93
00:06:53,160 --> 00:06:57,000
 with the example of this monk of course

94
00:06:57,000 --> 00:07:00,170
 we're talking about sort of the very purest sort of

95
00:07:00,170 --> 00:07:03,940
 livelihood, it doesn't seem dangerous for someone to

96
00:07:03,940 --> 00:07:06,000
 exchange

97
00:07:06,000 --> 00:07:11,110
 medical services for food for example, I mean no matter how

98
00:07:11,110 --> 00:07:12,000
 luxurious it is

99
00:07:12,000 --> 00:07:18,000
 but it just speaks to how high a standard monks are held to

100
00:07:18,000 --> 00:07:23,330
 becoming a monk is a complete acceptance of, what we say, a

101
00:07:23,330 --> 00:07:30,000
 complete rejection of this whole system of pleasure seeking

102
00:07:30,000 --> 00:07:32,890
 but we can see this happening in the world and it's much

103
00:07:32,890 --> 00:07:36,000
 more easy to see if we talk about it on a gross level

104
00:07:36,000 --> 00:07:43,090
 we see how the addiction to drugs for example certainly

105
00:07:43,090 --> 00:07:46,000
 engages this on a very extreme level

106
00:07:46,000 --> 00:07:50,000
 this idea that the opportunity for pleasure exists

107
00:07:50,000 --> 00:07:54,000
 therefore seeking it is the way to of course find pleasure

108
00:07:54,000 --> 00:08:00,070
 but implicitly to find happiness because we equate pleasure

109
00:08:00,070 --> 00:08:02,000
 with this word happiness

110
00:08:02,000 --> 00:08:06,210
 we equate them, we think of them, well pleasure means

111
00:08:06,210 --> 00:08:09,590
 happiness, happiness means pleasure, they are one and the

112
00:08:09,590 --> 00:08:10,000
 same

113
00:08:10,000 --> 00:08:14,830
 and yet a person who engages in drugs for example ends up

114
00:08:14,830 --> 00:08:18,000
 being more unhappy obviously

115
00:08:18,000 --> 00:08:23,000
 their life becomes more unhappy

116
00:08:23,000 --> 00:08:29,160
 it involves the, it's because there's what you might

117
00:08:29,160 --> 00:08:38,000
 consider a catch 22 to invoke that literary title

118
00:08:38,000 --> 00:08:43,980
 that in order to experience pleasure you have to chase

119
00:08:43,980 --> 00:08:45,000
 after it

120
00:08:45,000 --> 00:08:49,800
 but a person who chases after pleasure or the act of

121
00:08:49,800 --> 00:08:54,360
 chasing after pleasure reduces the amount of pleasure that

122
00:08:54,360 --> 00:08:55,000
 you gain

123
00:08:55,000 --> 00:08:59,000
 reduces the potentiality to obtain pleasure

124
00:08:59,000 --> 00:09:04,020
 if you think about a drug addict it reduces the brain's

125
00:09:04,020 --> 00:09:08,000
 capacity to gain pleasure from the same experiences

126
00:09:08,000 --> 00:09:12,710
 but if you think of it in a worldly sense, an external

127
00:09:12,710 --> 00:09:17,170
 sense as well, a drug addict loses respect, they lose

128
00:09:17,170 --> 00:09:18,000
 friends

129
00:09:18,000 --> 00:09:21,000
 they get in trouble with the law but through stealing

130
00:09:21,000 --> 00:09:26,000
 because the means of obtaining become more and more extreme

131
00:09:26,000 --> 00:09:30,160
 as you follow this to its logical conclusion if seeking

132
00:09:30,160 --> 00:09:33,000
 pleasure is always the right answer

133
00:09:33,000 --> 00:09:39,760
 then the consequences or the means of obtaining it becoming

134
00:09:39,760 --> 00:09:44,000
 consequential, the details of how you obtain it

135
00:09:44,000 --> 00:09:49,420
 you ignore them, you go beyond what is appropriate, so for

136
00:09:49,420 --> 00:09:52,000
 a monk of course what is appropriate is very little

137
00:09:52,000 --> 00:10:03,000
 and we dedicate ourselves to no pursuit of central pleasure

138
00:10:03,000 --> 00:10:07,050
 but for a lay person we can see it in things like drug

139
00:10:07,050 --> 00:10:12,100
 addiction, we can also see it in business, we see it in the

140
00:10:12,100 --> 00:10:13,000
 stock market

141
00:10:13,000 --> 00:10:18,610
 I mention the stock market because coincidentally there's a

142
00:10:18,610 --> 00:10:23,360
 very big story in the news that I noticed and started

143
00:10:23,360 --> 00:10:26,000
 reading about

144
00:10:26,000 --> 00:10:32,120
 apparently there's this in the stock market, a little bit

145
00:10:32,120 --> 00:10:35,000
 of a tangent, but very much related

146
00:10:35,000 --> 00:10:42,850
 they take money and say I'll owe you a certain number of

147
00:10:42,850 --> 00:10:48,400
 stocks, they say stocks are worth $10, give me $100 and I

148
00:10:48,400 --> 00:10:50,000
'll owe you $10

149
00:10:50,000 --> 00:10:53,840
 and they do this, so they take the money, but they do it

150
00:10:53,840 --> 00:10:56,000
 with stocks that are going down in value

151
00:10:56,000 --> 00:11:00,200
 so as the value goes down they can buy the stocks and give

152
00:11:00,200 --> 00:11:04,000
 them back and they made money on it because they buy later

153
00:11:04,000 --> 00:11:04,000
 when it's lower

154
00:11:04,000 --> 00:11:07,220
 they're betting on the fact that it's going to go lower, so

155
00:11:07,220 --> 00:11:10,000
 apparently the entire stock market or the rich people

156
00:11:10,000 --> 00:11:18,000
 bought a hundred and borrowed 140% of the stocks of this

157
00:11:18,000 --> 00:11:22,480
 company which there's only 100% means they borrowed too

158
00:11:22,480 --> 00:11:23,000
 many

159
00:11:23,000 --> 00:11:29,030
 another group of amateur investors found out about this and

160
00:11:29,030 --> 00:11:33,760
 now the whole stock market's in a tizzy because they can't

161
00:11:33,760 --> 00:11:34,000
 pay it back

162
00:11:34,000 --> 00:11:38,010
 and billionaires are losing millions of dollars and amateur

163
00:11:38,010 --> 00:11:42,000
 investors are making millions of dollars and so on

164
00:11:42,000 --> 00:11:47,460
 I bring it up because just reading about it and looking at

165
00:11:47,460 --> 00:11:56,540
 this, this is the epitome of this seeking out of gain and

166
00:11:56,540 --> 00:11:59,000
 the greed involved

167
00:11:59,000 --> 00:12:06,000
 and ultimately how it leads in the stock market especially

168
00:12:06,000 --> 00:12:11,490
 to an increase in greed until you manipulate the stock

169
00:12:11,490 --> 00:12:15,000
 market and engage in illegal practices

170
00:12:15,000 --> 00:12:19,830
 and so on and you can lose your entire wealth and income

171
00:12:19,830 --> 00:12:29,310
 and there's no end to it so the whole system of pleasure

172
00:12:29,310 --> 00:12:33,000
 seeking is caught up in a catch-22

173
00:12:33,000 --> 00:12:38,000
 that the more you seek out the more trouble you get into

174
00:12:38,000 --> 00:12:42,000
 when you stop seeking out, of course you don't get the

175
00:12:42,000 --> 00:12:45,880
 pleasure and so on a fundamental level pleasure cannot be

176
00:12:45,880 --> 00:12:47,000
 equated with happiness

177
00:12:47,000 --> 00:12:51,540
 it can't make you happy because you have two choices and

178
00:12:51,540 --> 00:12:53,000
 neither one works

179
00:12:53,000 --> 00:12:56,450
 you seek out the pleasure and you need more pleasure, you

180
00:12:56,450 --> 00:13:00,230
 stop seeking out the pleasure and you don't get any

181
00:13:00,230 --> 00:13:01,000
 pleasure

182
00:13:01,000 --> 00:13:05,000
 it has a system, it just doesn't work

183
00:13:05,000 --> 00:13:09,700
 so when we talk about in this verse the idea of living an

184
00:13:09,700 --> 00:13:11,000
 easier life

185
00:13:11,000 --> 00:13:14,910
 we're really talking about on the short term and we're

186
00:13:14,910 --> 00:13:18,000
 talking about on the external level

187
00:13:18,000 --> 00:13:24,600
 a person who engages in betting on the stock market,

188
00:13:24,600 --> 00:13:29,730
 engages in drug addiction, even a monk who engages in

189
00:13:29,730 --> 00:13:31,000
 medical practices

190
00:13:31,000 --> 00:13:35,830
 obsessed with the idea that they're going to gain food is

191
00:13:35,830 --> 00:13:40,000
 inevitably going to come to greater suffering

192
00:13:40,000 --> 00:13:45,950
 their life becomes easier as a result but their mind

193
00:13:45,950 --> 00:13:48,000
 becomes corrupt

194
00:13:48,000 --> 00:13:52,800
 and so when we think of the law of karma we have to

195
00:13:52,800 --> 00:13:57,000
 understand it on a mental level first

196
00:13:57,000 --> 00:14:03,000
 karma works on its very base on a mental level

197
00:14:03,000 --> 00:14:06,700
 we can't look at what happens in the external world because

198
00:14:06,700 --> 00:14:09,000
 of course it's far too complicated

199
00:14:09,000 --> 00:14:12,560
 and because it involves past consequences, being born as a

200
00:14:12,560 --> 00:14:16,200
 human being we have so much potential for obtaining

201
00:14:16,200 --> 00:14:17,000
 pleasure

202
00:14:17,000 --> 00:14:20,050
 and our corruption that the corruption that comes from

203
00:14:20,050 --> 00:14:23,000
 seeking the pleasure isn't going to catch up with us

204
00:14:23,000 --> 00:14:28,410
 not quickly because we're protected by all the many factors

205
00:14:28,410 --> 00:14:31,000
 that are set, the physical factors

206
00:14:31,000 --> 00:14:35,800
 but the mental factors are immediate, a person who engages

207
00:14:35,800 --> 00:14:40,000
 in the acquisition of central pleasure is immediately

208
00:14:40,000 --> 00:14:47,000
 caught up in this web of craving, increased desire

209
00:14:47,000 --> 00:14:49,960
 this is where meditation comes in and this is what you see

210
00:14:49,960 --> 00:14:54,530
 much more clearly on the meditative level, the experiential

211
00:14:54,530 --> 00:14:55,000
 level

212
00:14:55,000 --> 00:14:58,080
 it's very difficult to see karma working in the world

213
00:14:58,080 --> 00:15:02,230
 around us when we see rich people engaging in these

214
00:15:02,230 --> 00:15:04,000
 manipulative practices

215
00:15:04,000 --> 00:15:08,460
 or even drug addicts, if you look at them externally it's

216
00:15:08,460 --> 00:15:14,000
 hard to see what's really happening, where the danger is

217
00:15:14,000 --> 00:15:17,120
 for this monk it's hard to see what the danger is, he

218
00:15:17,120 --> 00:15:19,800
 appeared to be living quite well and the Buddha remarked on

219
00:15:19,800 --> 00:15:20,000
 it

220
00:15:20,000 --> 00:15:25,470
 but the danger is very real and very fundamental, much more

221
00:15:25,470 --> 00:15:28,450
 fundamental than the externalities because we have rich

222
00:15:28,450 --> 00:15:30,000
 billionaires now quite upset

223
00:15:30,000 --> 00:15:33,760
 because they're losing money and they're certainly not

224
00:15:33,760 --> 00:15:37,910
 going to be living on the streets as a result of this

225
00:15:37,910 --> 00:15:38,000
 upheaval

226
00:15:38,000 --> 00:15:43,440
 but they're certainly upset about it and the people who are

227
00:15:43,440 --> 00:15:47,000
 angry at them are upset for breaking the law and so on

228
00:15:47,000 --> 00:15:54,230
 you can be surrounded by great pleasure and in fact the

229
00:15:54,230 --> 00:15:57,000
 more pleasure you surround yourself with

230
00:15:57,000 --> 00:16:01,580
 the more unhappy you become, the more dissatisfied you

231
00:16:01,580 --> 00:16:08,000
 become because the act of acquiring the pleasure increases

232
00:16:08,000 --> 00:16:09,000
 your desire for it

233
00:16:09,000 --> 00:16:15,140
 it's a habit, everything in the mind works in terms of

234
00:16:15,140 --> 00:16:19,420
 habits, whatever you cultivate, whatever you engage in

235
00:16:19,420 --> 00:16:20,000
 becomes your habit

236
00:16:20,000 --> 00:16:26,760
 desiring something, going after it increases the impulse to

237
00:16:26,760 --> 00:16:28,000
 go after it

238
00:16:28,000 --> 00:16:31,840
 so it doesn't lead to contentment, it's not "if I get this

239
00:16:31,840 --> 00:16:35,270
 I'll be content" because you're engaging this impulse to

240
00:16:35,270 --> 00:16:36,000
 obtain

241
00:16:36,000 --> 00:16:42,800
 and that increases the habit of needing to obtain, it's

242
00:16:42,800 --> 00:16:46,000
 impossible to satisfy

243
00:16:46,000 --> 00:16:50,700
 so when the Buddha said that a person who lives corrupt,

244
00:16:50,700 --> 00:16:55,400
 lives an easy life, he was only talking about the extern

245
00:16:55,400 --> 00:16:56,000
ality

246
00:16:56,000 --> 00:17:01,340
 he was pointing out what we have to understand about

247
00:17:01,340 --> 00:17:06,000
 Buddhism and about meditation practice specifically

248
00:17:06,000 --> 00:17:10,180
 that it's not going to bring us more pleasure, it's not

249
00:17:10,180 --> 00:17:12,000
 going to make life easier

250
00:17:12,000 --> 00:17:15,770
 in many many cases, most cases we might say, it's going to

251
00:17:15,770 --> 00:17:17,000
 make life harder

252
00:17:17,000 --> 00:17:22,000
 that's not intentional and that's certainly not across the

253
00:17:22,000 --> 00:17:26,000
 board and it's certainly not a long term problem

254
00:17:26,000 --> 00:17:29,440
 because of course when you engage in purity people respect

255
00:17:29,440 --> 00:17:33,000
 you more, your mind becomes more focused

256
00:17:33,000 --> 00:17:37,220
 many good things start to come to you, but no matter how

257
00:17:37,220 --> 00:17:41,630
 many good things come to you, you refrain from seeking them

258
00:17:41,630 --> 00:17:42,000
 out

259
00:17:42,000 --> 00:17:48,000
 you refrain from indulging your desire for them

260
00:17:48,000 --> 00:17:52,000
 we have to understand this, this will always be the case

261
00:17:52,000 --> 00:17:55,000
 and it's intentionally the case

262
00:17:55,000 --> 00:18:02,350
 because our intention is to move in another direction, our

263
00:18:02,350 --> 00:18:07,000
 happiness, our search for happiness is in another direction

264
00:18:07,000 --> 00:18:10,900
 our happiness is outside of externalities, our happiness is

265
00:18:10,900 --> 00:18:18,000
 independent of our experiences, that's the goal

266
00:18:18,000 --> 00:18:23,040
 so when we practice meditation, this is where I think it

267
00:18:23,040 --> 00:18:31,590
 hits home most viscerally and really most importantly for

268
00:18:31,590 --> 00:18:32,000
 us

269
00:18:32,000 --> 00:18:36,650
 meditation can be, and quite often is, and perhaps you

270
00:18:36,650 --> 00:18:39,000
 could say should be difficult

271
00:18:39,000 --> 00:18:42,090
 meditation is for the purpose of overcoming craving,

272
00:18:42,090 --> 00:18:44,000
 freeing ourselves from craving

273
00:18:44,000 --> 00:18:46,410
 so if you're free from craving you don't have to practice

274
00:18:46,410 --> 00:18:50,000
 meditation, but in so far as we have craving

275
00:18:50,000 --> 00:18:53,670
 it's going to be difficult because it's going to challenge

276
00:18:53,670 --> 00:18:59,000
 us, it's going to change our attitude

277
00:18:59,000 --> 00:19:03,260
 it's going to change our interaction with those things that

278
00:19:03,260 --> 00:19:04,000
 we crave

279
00:19:04,000 --> 00:19:10,000
 it's going to help us or force us to see them objectively

280
00:19:10,000 --> 00:19:16,140
 it's not so much about denying yourself pleasure, it doesn

281
00:19:16,140 --> 00:19:21,000
't work in the conventional sense of abstaining

282
00:19:21,000 --> 00:19:26,130
 mindfulness doesn't work that way, it works in the way of

283
00:19:26,130 --> 00:19:28,000
 seeing objectively

284
00:19:28,000 --> 00:19:33,290
 so a thing that you might desire or crave after rather than

285
00:19:33,290 --> 00:19:35,000
 saying no no I refuse

286
00:19:35,000 --> 00:19:41,450
 you can see as the middle wave, absolutely, the middle way

287
00:19:41,450 --> 00:19:44,000
 of seeing that object as it is

288
00:19:44,000 --> 00:19:48,750
 not, oh this is nice I want this, but also not, this is

289
00:19:48,750 --> 00:19:51,000
 dangerous I must stay away

290
00:19:51,000 --> 00:19:58,210
 this is this, it avoids the pitfalls of both courses of

291
00:19:58,210 --> 00:19:59,000
 action

292
00:19:59,000 --> 00:20:03,500
 if you are of course as we said chasing after it this is

293
00:20:03,500 --> 00:20:06,000
 good, you cultivate that

294
00:20:06,000 --> 00:20:10,960
 but by avoiding it, you don't gain any special knowledge

295
00:20:10,960 --> 00:20:13,000
 about pleasure and desire

296
00:20:13,000 --> 00:20:18,720
 you create instead an impulse of aversion and you can see

297
00:20:18,720 --> 00:20:22,000
 this, you'll start to become bitter and unhappy

298
00:20:22,000 --> 00:20:25,530
 in the beginning for many monks and meditators it can be

299
00:20:25,530 --> 00:20:30,000
 quite an unhappy life because you find yourself doing that

300
00:20:30,000 --> 00:20:33,510
 you remind yourself that oh this way is not right and this

301
00:20:33,510 --> 00:20:38,000
 is what the Buddha found himself doing for six years

302
00:20:38,000 --> 00:20:42,960
 repressing, repressing the desires, pushing away the desire

303
00:20:42,960 --> 00:20:45,000
, rejecting it

304
00:20:45,000 --> 00:20:47,990
 until he found this what he called the middle way and he

305
00:20:47,990 --> 00:20:51,000
 saw pleasure isn't the problem

306
00:20:51,000 --> 00:20:54,480
 pleasure is just pleasure, the problem is that I want the

307
00:20:54,480 --> 00:20:57,000
 pleasure, the problem is just that

308
00:20:57,000 --> 00:21:01,100
 there's a distinction there, I don't have to reject

309
00:21:01,100 --> 00:21:02,000
 pleasure he said

310
00:21:02,000 --> 00:21:06,730
 I just have to see it clearly and free myself from the

311
00:21:06,730 --> 00:21:08,000
 desire for it

312
00:21:08,000 --> 00:21:19,020
 so what we see quite often with new meditators is they

313
00:21:19,020 --> 00:21:23,000
 become discouraged

314
00:21:23,000 --> 00:21:27,050
 they become discouraged because the results are not as they

315
00:21:27,050 --> 00:21:28,000
 would expect

316
00:21:28,000 --> 00:21:34,980
 what we expect ordinarily from meditation is pleasure

317
00:21:34,980 --> 00:21:37,000
 because that's how we understand happiness

318
00:21:37,000 --> 00:21:41,720
 oh I'm unhappy meditation will make me happy therefore it

319
00:21:41,720 --> 00:21:44,000
 must bring me pleasure and it doesn't

320
00:21:44,000 --> 00:21:46,770
 mindfulness meditation doesn't and there are many

321
00:21:46,770 --> 00:21:51,000
 experiences in meditation that can bring that pleasure

322
00:21:51,000 --> 00:21:54,920
 many types of meditation that can bring pleasure and so we

323
00:21:54,920 --> 00:21:56,000
 seek out those

324
00:21:56,000 --> 00:21:59,000
 and we become discouraged when practicing mindfulness

325
00:21:59,000 --> 00:22:00,000
 because it doesn't seem to be doing that

326
00:22:00,000 --> 00:22:04,090
 it doesn't seem to be bringing us happiness at all, not in

327
00:22:04,090 --> 00:22:06,000
 the beginning

328
00:22:06,000 --> 00:22:10,000
 until we make that paradigm shift, until we come to see

329
00:22:10,000 --> 00:22:13,000
 beyond pleasure

330
00:22:13,000 --> 00:22:17,280
 seeing pleasure for pleasure, pain for pain, experience

331
00:22:17,280 --> 00:22:19,000
 just as experience

332
00:22:19,000 --> 00:22:26,190
 then you start to understand what true happiness is, true

333
00:22:26,190 --> 00:22:29,000
 happiness is inextricable from freedom

334
00:22:29,000 --> 00:22:34,650
 from independence, it requires that you free yourself from

335
00:22:34,650 --> 00:22:40,000
 need, from desire, from addiction, from aversion

336
00:22:40,000 --> 00:22:44,120
 it requires objectivity and clarity and so the last word of

337
00:22:44,120 --> 00:22:47,000
 these two verses that the Buddha used is

338
00:22:47,000 --> 00:22:51,100
 perhaps the most important, passata, it seems somewhat un

339
00:22:51,100 --> 00:22:52,000
connected to the rest

340
00:22:52,000 --> 00:22:57,100
 it's not an equivalent of, see the verses are parallels,

341
00:22:57,100 --> 00:22:59,000
 one talking about being corrupt

342
00:22:59,000 --> 00:23:01,480
 the other talking about being pure but then he adds the

343
00:23:01,480 --> 00:23:03,000
 last word passata

344
00:23:03,000 --> 00:23:08,670
 passata means seeing or one who sees because that's

345
00:23:08,670 --> 00:23:10,000
 ultimately what purity means

346
00:23:10,000 --> 00:23:13,760
 it doesn't mean rejecting pleasure, it means seeing

347
00:23:13,760 --> 00:23:17,000
 pleasure and pain equally and clearly as they are

348
00:23:17,000 --> 00:23:20,930
 so that's what we try to do in meditation and I think as

349
00:23:20,930 --> 00:23:22,000
 you can hopefully see

350
00:23:22,000 --> 00:23:25,070
 it's a very important teaching, an important verse,

351
00:23:25,070 --> 00:23:27,000
 something that challenges us and helps us to see

352
00:23:27,000 --> 00:23:31,190
 the depth of the teaching as not just a better way to find

353
00:23:31,190 --> 00:23:32,000
 pleasure

354
00:23:32,000 --> 00:23:36,740
 but a different way to understand pleasure and a better way

355
00:23:36,740 --> 00:23:38,000
 to understand happiness

356
00:23:38,000 --> 00:23:42,570
 so that's the Dhamma Padavar to-night, thank you for

357
00:23:42,570 --> 00:23:45,000
 listening, I wish you all peace, happiness

358
00:23:45,000 --> 00:23:48,000
 and freedom from suffering, thank you

359
00:23:50,000 --> 00:24:14,580
 [

