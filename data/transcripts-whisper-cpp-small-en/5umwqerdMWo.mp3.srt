1
00:00:00,000 --> 00:00:05,030
 Good evening. We're continuing on with our study of the D

2
00:00:05,030 --> 00:00:06,000
hammapada.

3
00:00:06,000 --> 00:00:12,000
 Today we're looking at verse 200, which reads,

4
00:00:12,000 --> 00:00:19,000
 "Susukang vataji vam yeisangno nati kin chanam

5
00:00:19,000 --> 00:00:29,000
 pitibha ka bhvisama deva abhasarayata."

6
00:00:29,000 --> 00:00:41,000
 Which means, happily indeed we live. We who have nothing.

7
00:00:41,000 --> 00:00:51,000
 We will eat, we will feed off of rapture, like the angels

8
00:00:51,000 --> 00:00:56,000
 in the abhasara realm.

9
00:00:56,000 --> 00:01:00,000
 The abhasara angels.

10
00:01:00,000 --> 00:01:10,000
 Ones who have a radiance, an abha.

11
00:01:10,000 --> 00:01:13,000
 So this is the third, if you remember the last two verses

12
00:01:13,000 --> 00:01:14,000
 were also

13
00:01:14,000 --> 00:01:18,000
 "Susukang vataji vam" - happily indeed we live.

14
00:01:18,000 --> 00:01:23,000
 This is the "Sukavagga". So it's talking about happiness.

15
00:01:23,000 --> 00:01:26,000
 I think the Buddha was talking always about suffering.

16
00:01:26,000 --> 00:01:30,000
 The only reason he talked about suffering was in order to

17
00:01:30,000 --> 00:01:32,000
 find happiness.

18
00:01:32,000 --> 00:01:36,000
 Freedom from suffering.

19
00:01:36,000 --> 00:01:42,190
 This verse was taught in regards to a story about Mara,

20
00:01:42,190 --> 00:01:44,000
 really.

21
00:01:44,000 --> 00:01:55,060
 The Buddha was dwelling in Panchasala, a Brahmin village

22
00:01:55,060 --> 00:01:59,000
 called Panchasala.

23
00:01:59,000 --> 00:02:07,000
 He thought to himself, "Who can I teach today?"

24
00:02:07,000 --> 00:02:11,490
 He had this vision of this large group of maidens, young

25
00:02:11,490 --> 00:02:14,000
 women.

26
00:02:14,000 --> 00:02:18,000
 Kumadika, young women.

27
00:02:18,000 --> 00:02:23,240
 He thought, "They will understand the Dhamma if they hear

28
00:02:23,240 --> 00:02:24,000
 it."

29
00:02:24,000 --> 00:02:29,530
 So he went into the village on alms, but he didn't get

30
00:02:29,530 --> 00:02:31,000
 anything.

31
00:02:31,000 --> 00:02:35,000
 The text says that Mara, it's about Mara,

32
00:02:35,000 --> 00:02:41,640
 Mara came into the village and went into their hearts and

33
00:02:41,640 --> 00:02:45,580
 tricked or somehow made it so that none of them gave

34
00:02:45,580 --> 00:02:46,000
 anything.

35
00:02:46,000 --> 00:02:51,000
 People who would, I guess, otherwise have given.

36
00:02:51,000 --> 00:02:54,000
 And the Buddha came out of the village and Mara came to him

37
00:02:54,000 --> 00:02:56,000
 and taunted him and said,

38
00:02:56,000 --> 00:03:02,270
 "Oh, so you went into the village on alms and didn't get

39
00:03:02,270 --> 00:03:04,000
 anything?"

40
00:03:04,000 --> 00:03:09,000
 Did you get anything? The Buddha said, "No, I didn't."

41
00:03:09,000 --> 00:03:12,240
 He said, "Oh, well, go back into the village and try again

42
00:03:12,240 --> 00:03:13,000
 then."

43
00:03:13,000 --> 00:03:19,870
 And Mara thought, "If he goes back into the village again,

44
00:03:19,870 --> 00:03:25,000
 I'll get people to be mean and nasty too."

45
00:03:25,000 --> 00:03:32,420
 And at that time, this large group of young women came to

46
00:03:32,420 --> 00:03:34,000
 the Buddha when he was sitting there.

47
00:03:34,000 --> 00:03:37,000
 And so it was an opportunity to teach the Dhamma.

48
00:03:37,000 --> 00:03:40,970
 Mara was taunting him and said, "Oh, you must be really

49
00:03:40,970 --> 00:03:44,000
 hungry. You've got nothing to eat."

50
00:03:44,000 --> 00:03:49,260
 And the Buddha said, "I have nothing. It's true, but it's

51
00:03:49,260 --> 00:03:54,000
 not true that I will not feed."

52
00:03:54,000 --> 00:04:01,040
 He said, "I have no hunger because I feed off the rapture

53
00:04:01,040 --> 00:04:04,000
 of enlightenment."

54
00:04:04,000 --> 00:04:09,090
 It's a very, very simple teaching. It's also a fairly well-

55
00:04:09,090 --> 00:04:14,000
known teaching. It's a memorable one.

56
00:04:14,000 --> 00:04:22,820
 Because being without is a part of life. It's a reminder to

57
00:04:22,820 --> 00:04:25,000
 us of a good attitude to have

58
00:04:25,000 --> 00:04:31,080
 in the face of what really just amounts to a change of

59
00:04:31,080 --> 00:04:33,000
 experience.

60
00:04:33,000 --> 00:04:37,000
 That's the key here, and that's the key in Buddhism.

61
00:04:37,000 --> 00:04:43,920
 So the real lesson here in the story, in the verse, is

62
00:04:43,920 --> 00:04:50,000
 about not getting what you want.

63
00:04:50,000 --> 00:04:54,320
 How hunger comes from not having something that you want or

64
00:04:54,320 --> 00:05:00,000
 wanting something that you don't have.

65
00:05:00,000 --> 00:05:08,640
 For some people, happiness depends upon a great deal of

66
00:05:08,640 --> 00:05:10,000
 things.

67
00:05:10,000 --> 00:05:14,650
 Not just on food. For some people, for addicts without

68
00:05:14,650 --> 00:05:18,000
 drugs, there's no happiness.

69
00:05:18,000 --> 00:05:22,010
 For ordinary people without the objects of the sense that

70
00:05:22,010 --> 00:05:27,000
 delight us, there's no happiness.

71
00:05:27,000 --> 00:05:38,350
 But even for people who are otherwise not addicted to sens

72
00:05:38,350 --> 00:05:41,000
ual pleasure,

73
00:05:41,000 --> 00:05:44,370
 without things like food or what we would call the

74
00:05:44,370 --> 00:05:52,000
 necessities of life, there can be no happiness.

75
00:05:52,000 --> 00:05:59,760
 And it really highlights the shift in perception that we

76
00:05:59,760 --> 00:06:03,000
 undertake in our practice of mindfulness.

77
00:06:03,000 --> 00:06:06,000
 What does mindfulness change? What good is mindfulness?

78
00:06:06,000 --> 00:06:08,000
 What does it do?

79
00:06:08,000 --> 00:06:15,000
 It changes the way you look at your life, at reality.

80
00:06:15,000 --> 00:06:21,320
 And instead of looking at your experience, your situation,

81
00:06:21,320 --> 00:06:26,000
 in terms of what you have and what you don't have,

82
00:06:26,000 --> 00:06:29,990
 you come to see it of what you're experiencing. You come to

83
00:06:29,990 --> 00:06:34,000
 see your reality in terms of experience.

84
00:06:34,000 --> 00:06:40,210
 The idea of getting or not getting, having or not having

85
00:06:40,210 --> 00:06:42,000
 disappears.

86
00:06:42,000 --> 00:06:48,000
 There's a quote, George Orwell, once read a book by...

87
00:06:48,000 --> 00:06:53,000
 He was writing memoirs of when he had nothing.

88
00:06:53,000 --> 00:06:59,000
 He fell into great poverty living in Paris and London.

89
00:06:59,000 --> 00:07:04,560
 And he said there's a great freedom that comes from when

90
00:07:04,560 --> 00:07:09,000
 you reach rock bottom, when you have nothing.

91
00:07:09,000 --> 00:07:14,000
 Because you know there's no lower you can go.

92
00:07:14,000 --> 00:07:21,790
 Your perspective changes. There's nothing to be afraid of

93
00:07:21,790 --> 00:07:27,000
 because it can't get any worse.

94
00:07:27,000 --> 00:07:34,720
 This is a reason why, a good reason to think about becoming

95
00:07:34,720 --> 00:07:44,000
 a Buddhist monk or leaving home and undertaking a reclusive

96
00:07:44,000 --> 00:07:45,000
 life.

97
00:07:45,000 --> 00:07:49,210
 Because it changes your perspective and it brings into

98
00:07:49,210 --> 00:07:52,000
 contrast what you have and what you don't have.

99
00:07:52,000 --> 00:07:59,000
 It's what you have and what you need to have to be happy.

100
00:07:59,000 --> 00:08:03,880
 And you're able to see all the things that you require to

101
00:08:03,880 --> 00:08:05,000
 be happy.

102
00:08:05,000 --> 00:08:09,910
 You're able to change your perspective and work on these c

103
00:08:09,910 --> 00:08:11,000
ravings.

104
00:08:11,000 --> 00:08:19,000
 Work on this suffering.

105
00:08:19,000 --> 00:08:25,670
 So people will say things like you need friends to be happy

106
00:08:25,670 --> 00:08:26,000
.

107
00:08:26,000 --> 00:08:30,000
 You need possessions to be happy.

108
00:08:30,000 --> 00:08:34,000
 You need something to entertain you in order to be happy.

109
00:08:34,000 --> 00:08:38,000
 The very least you need food to be happy.

110
00:08:38,000 --> 00:08:47,000
 Our idea of happiness is in the externals, in things.

111
00:08:47,000 --> 00:08:51,040
 Because we dwell in the realm of things. We think of me

112
00:08:51,040 --> 00:08:56,030
 eating food as getting a thing called food and putting it

113
00:08:56,030 --> 00:08:57,000
 in my body.

114
00:08:57,000 --> 00:09:00,560
 We don't think of it as just an experience. We aren't able

115
00:09:00,560 --> 00:09:05,440
 to see that eating food and the feeling of fulfillment that

116
00:09:05,440 --> 00:09:06,000
 comes,

117
00:09:06,000 --> 00:09:12,090
 the pleasure that comes from eating the food is all just

118
00:09:12,090 --> 00:09:14,000
 experiences.

119
00:09:14,000 --> 00:09:19,600
 And so we lack, well we cultivate partiality and we lack

120
00:09:19,600 --> 00:09:24,380
 the flexibility that comes from seeing reality as

121
00:09:24,380 --> 00:09:26,000
 experience.

122
00:09:26,000 --> 00:09:30,690
 When you're able to perceive food and friends and

123
00:09:30,690 --> 00:09:36,250
 possessions and all the things that make us happy as simply

124
00:09:36,250 --> 00:09:38,000
 experiences,

125
00:09:38,000 --> 00:09:41,600
 you gain a flexibility. You come to see that there's no

126
00:09:41,600 --> 00:09:45,000
 difference between one experience and another.

127
00:09:45,000 --> 00:09:49,730
 You can't cling to an experience. An experience can't be

128
00:09:49,730 --> 00:09:53,000
 something that you wish for or want for.

129
00:09:53,000 --> 00:09:59,000
 Once you see the momentary experiences,

130
00:09:59,000 --> 00:10:03,580
 not talking about an experience in a conceptual sense, but

131
00:10:03,580 --> 00:10:07,910
 in a momentary sense, you can't cling to it because it

132
00:10:07,910 --> 00:10:09,000
 arises and ceases.

133
00:10:09,000 --> 00:10:21,630
 And it has no quality that is good or bad or even desirable

134
00:10:21,630 --> 00:10:26,000
 or undesirable.

135
00:10:26,000 --> 00:10:29,360
 When you look at things in terms of experience, you're

136
00:10:29,360 --> 00:10:33,000
 watching, when we say to ourselves, pain, pain, thinking,

137
00:10:33,000 --> 00:10:34,000
 thinking.

138
00:10:34,000 --> 00:10:43,550
 When you watch in that way, you gain this flexibility, sort

139
00:10:43,550 --> 00:10:48,000
 of an adaptability or flexibility.

140
00:10:48,000 --> 00:11:02,120
 The peace that comes from being content with whatever comes

141
00:11:02,120 --> 00:11:03,000
.

142
00:11:03,000 --> 00:11:06,990
 So we can talk about contentment in things, but ultimately

143
00:11:06,990 --> 00:11:09,000
 if we have craving, if we have attachment,

144
00:11:09,000 --> 00:11:12,720
 there are things that we like and things that we don't like

145
00:11:12,720 --> 00:11:13,000
.

146
00:11:13,000 --> 00:11:18,000
 A thing is something you can attach to. It's a concept in

147
00:11:18,000 --> 00:11:24,000
 the mind, something that you can remember having.

148
00:11:24,000 --> 00:11:30,740
 It's a thing that can provide stability, satisfaction,

149
00:11:30,740 --> 00:11:32,000
 control.

150
00:11:32,000 --> 00:11:39,790
 You can control things, you can keep things, you can be

151
00:11:39,790 --> 00:11:43,000
 satisfied with things,

152
00:11:43,000 --> 00:11:52,330
 comfortable furniture or pleasant food, electronic devices,

153
00:11:52,330 --> 00:11:55,000
 people, places.

154
00:11:55,000 --> 00:12:04,120
 All of these things can provide stability and satisfaction

155
00:12:04,120 --> 00:12:06,000
 and control.

156
00:12:06,000 --> 00:12:10,570
 But they don't really exist. The problem that we have with

157
00:12:10,570 --> 00:12:15,000
 things is that

158
00:12:15,000 --> 00:12:19,160
 they're dependent on the underlying experiences and

159
00:12:19,160 --> 00:12:22,000
 experiences are unpredictable.

160
00:12:22,000 --> 00:12:25,130
 When you talk about having food, what you really mean is

161
00:12:25,130 --> 00:12:28,000
 having the potential to have experiences,

162
00:12:28,000 --> 00:12:31,050
 moments of experience where you're eating, even where you

163
00:12:31,050 --> 00:12:34,000
're just seeing the food is an experience.

164
00:12:34,000 --> 00:12:37,000
 And these are unpredictable, uncontrollable.

165
00:12:37,000 --> 00:12:45,410
 Another important lesson of this story is about Mara, about

166
00:12:45,410 --> 00:12:46,000
 evil.

167
00:12:46,000 --> 00:12:50,390
 Whether or not you believe that there was this malevolent

168
00:12:50,390 --> 00:12:54,740
 spirit taking over people's minds and hearts and making

169
00:12:54,740 --> 00:12:56,000
 them stingy.

170
00:12:56,000 --> 00:13:03,400
 Stinginess does exist. And manipulation, people's

171
00:13:03,400 --> 00:13:11,000
 circumstances of your life are plotting against you,

172
00:13:11,000 --> 00:13:15,740
 evil plotting against you, just chance, fate plotting

173
00:13:15,740 --> 00:13:20,000
 against you, your own bad karma coming back to haunt you.

174
00:13:20,000 --> 00:13:26,680
 So many things. But evil is in particular something that we

175
00:13:26,680 --> 00:13:28,000
 have to recognize.

176
00:13:28,000 --> 00:13:32,830
 Whether you recognize Mara as a being who came to the

177
00:13:32,830 --> 00:13:35,000
 Buddha and taunted him,

178
00:13:35,000 --> 00:13:39,030
 going into a village and not getting any alms is a common

179
00:13:39,030 --> 00:13:43,000
 experience for monks. It certainly happens.

180
00:13:43,000 --> 00:13:49,120
 And by extension, anything, any activity that we undertake,

181
00:13:49,120 --> 00:13:54,000
 trying to obtain something that we want and not getting it,

182
00:13:54,000 --> 00:13:59,000
 that's common to everyone.

183
00:13:59,000 --> 00:14:02,960
 Often because of the evil intentions of others, stinginess,

184
00:14:02,960 --> 00:14:06,550
 miserliness, not wanting to share, not wanting to give, not

185
00:14:06,550 --> 00:14:09,000
 wanting to help,

186
00:14:09,000 --> 00:14:11,800
 wanting you to be without so they steal from you or they

187
00:14:11,800 --> 00:14:17,990
 break things of yours or they hurt you with speech or

188
00:14:17,990 --> 00:14:20,000
 actions.

189
00:14:20,000 --> 00:14:26,170
 We think this body is a source of so much pleasure, but

190
00:14:26,170 --> 00:14:30,780
 someone with bad intentions comes to you, they can do great

191
00:14:30,780 --> 00:14:34,000
 harm and cause great suffering.

192
00:14:34,000 --> 00:14:38,370
 All because of our attachment to things, the concept of

193
00:14:38,370 --> 00:14:44,000
 something that is stable, satisfying, controllable.

194
00:14:44,000 --> 00:14:53,000
 And in fact, reality is not like that.

195
00:14:53,000 --> 00:14:58,230
 So this really speaks to the power of mindfulness, the

196
00:14:58,230 --> 00:15:03,590
 power of wisdom, the power of seeing things just as they

197
00:15:03,590 --> 00:15:04,000
 are,

198
00:15:04,000 --> 00:15:08,070
 being able to see things just as they are rather than react

199
00:15:08,070 --> 00:15:11,000
 to them or conceive in them like,

200
00:15:11,000 --> 00:15:17,070
 "This is food that I'm eating. This is my body that is

201
00:15:17,070 --> 00:15:23,000
 healthy, that is full and nourished with food."

202
00:15:23,000 --> 00:15:27,000
 The Buddha indeed, every day went to nourish his body.

203
00:15:27,000 --> 00:15:30,900
 But when he didn't receive nourishment, there was no

204
00:15:30,900 --> 00:15:32,000
 suffering.

205
00:15:32,000 --> 00:15:36,380
 The Buddha had a greater nourishment and that's the nour

206
00:15:36,380 --> 00:15:40,080
ishment of the present moment, the nourishment of

207
00:15:40,080 --> 00:15:44,000
 mindfulness, the nourishment of enlightenment.

208
00:15:44,000 --> 00:15:51,000
 Being enlightened means seeing things clearly.

209
00:15:51,000 --> 00:15:55,860
 Being without food is simply a different sort of experience

210
00:15:55,860 --> 00:15:56,000
.

211
00:15:56,000 --> 00:16:01,020
 It may make you incapable of doing certain things, but it

212
00:16:01,020 --> 00:16:05,000
 simply means a different form of experience.

213
00:16:05,000 --> 00:16:10,150
 The state where the body is weak, is tired because of food,

214
00:16:10,150 --> 00:16:13,970
 is an equally valid experience to the state where the body

215
00:16:13,970 --> 00:16:18,000
 is energetic and full of effort.

216
00:16:18,000 --> 00:16:22,520
 For a person who is attached to things like food, nour

217
00:16:22,520 --> 00:16:25,000
ishment can be a detriment.

218
00:16:25,000 --> 00:16:29,710
 When you are too well fed, you can become intoxicated by

219
00:16:29,710 --> 00:16:34,410
 the strength of the body, the chemicals in the body start

220
00:16:34,410 --> 00:16:39,000
 to work and you feel aroused.

221
00:16:39,000 --> 00:16:42,320
 Having too much food can be a problem for people who are

222
00:16:42,320 --> 00:16:45,900
 attached because it leads to addiction, it leads to, it can

223
00:16:45,900 --> 00:16:47,000
 lead to belligerence.

224
00:16:47,000 --> 00:16:52,510
 People are well fed, much more capable of fighting with

225
00:16:52,510 --> 00:16:54,000
 each other.

226
00:16:54,000 --> 00:16:58,210
 Having less food can sometimes be good and for certain not

227
00:16:58,210 --> 00:17:02,330
 getting what you want is always good, as I said, for

228
00:17:02,330 --> 00:17:05,000
 becoming a monk or even a recluse of some sort.

229
00:17:05,000 --> 00:17:10,370
 There's a great power and benefit to it because it teaches

230
00:17:10,370 --> 00:17:14,000
 you, gives you a new perspective.

231
00:17:14,000 --> 00:17:16,770
 The life of someone who has everything they want, gets

232
00:17:16,770 --> 00:17:22,090
 everything they want, is fraught with peril, the potential

233
00:17:22,090 --> 00:17:26,000
 for losing what you get, what you have,

234
00:17:26,000 --> 00:17:30,850
 and the potential for clinging to what you have and des

235
00:17:30,850 --> 00:17:35,750
iring more, becoming addicted to it, and needing it and

236
00:17:35,750 --> 00:17:40,000
 being dissatisfied whenever you can't get it.

237
00:17:40,000 --> 00:17:45,470
 The happiness of having nothing is a very powerful thing

238
00:17:45,470 --> 00:17:50,590
 and it's not that having things is a cause of suffering

239
00:17:50,590 --> 00:17:52,000
 necessarily,

240
00:17:52,000 --> 00:17:57,740
 but the power and the happiness that comes from not needing

241
00:17:57,740 --> 00:18:03,120
 anything, being happy without things, having your happiness

242
00:18:03,120 --> 00:18:05,000
 not depend on things.

243
00:18:05,000 --> 00:18:07,700
 It's the highest sort of happiness there is. It's

244
00:18:07,700 --> 00:18:13,000
 invincible. You have no vulnerability.

245
00:18:13,000 --> 00:18:18,520
 A person whose happiness depends on things, my happiness

246
00:18:18,520 --> 00:18:23,650
 depending on me going for food and getting food, is very

247
00:18:23,650 --> 00:18:29,000
 dangerous for me, setting myself up for suffering.

248
00:18:29,000 --> 00:18:32,760
 Now if I could control everything and everything went as

249
00:18:32,760 --> 00:18:39,920
 planned, that would be great, but this first highlight is a

250
00:18:39,920 --> 00:18:42,000
 very important part of reality,

251
00:18:42,000 --> 00:18:46,070
 that is the potential for us not to get what we want, the

252
00:18:46,070 --> 00:18:48,000
 potential for evil.

253
00:18:48,000 --> 00:18:50,290
 We don't know what's going to happen, we don't know what

254
00:18:50,290 --> 00:18:54,490
 our future is, but more specifically, we don't know when

255
00:18:54,490 --> 00:18:56,000
 evil might hit.

256
00:18:56,000 --> 00:19:03,300
 It could be in the form of another person, they might harm

257
00:19:03,300 --> 00:19:10,400
 us, steal from us, manipulate or lie to us, cheat us, might

258
00:19:10,400 --> 00:19:15,000
 come in the form of the body, sickness,

259
00:19:15,000 --> 00:19:23,060
 might come in the form of accidents, weather, natural

260
00:19:23,060 --> 00:19:26,690
 disasters, certainly come in the form of our own defile

261
00:19:26,690 --> 00:19:27,000
ments,

262
00:19:27,000 --> 00:19:30,390
 when we get angry and do things that harm ourselves, when

263
00:19:30,390 --> 00:19:34,060
 we get greedy and become addicted and attached to things

264
00:19:34,060 --> 00:19:43,000
 and create ambitions and actions that cause us suffering.

265
00:19:43,000 --> 00:19:46,950
 Happy without anything, I mean it's a real powerful

266
00:19:46,950 --> 00:19:51,930
 statement, we can use it to apply to not getting anything

267
00:19:51,930 --> 00:19:53,000
 in our lives,

268
00:19:53,000 --> 00:20:01,080
 but we can always think of the Buddha when we don't get

269
00:20:01,080 --> 00:20:05,700
 what we want and remember that even when the Buddha had no

270
00:20:05,700 --> 00:20:06,000
 food,

271
00:20:06,000 --> 00:20:10,030
 which is something that for many of us is a very difficult

272
00:20:10,030 --> 00:20:14,000
 thing psychologically, I mean emotionally to be without,

273
00:20:14,000 --> 00:20:18,100
 just the fear of not having enough food to eat, the stress

274
00:20:18,100 --> 00:20:24,960
 of going hungry, worry about not being able to nourish your

275
00:20:24,960 --> 00:20:26,000
 body.

276
00:20:26,000 --> 00:20:31,760
 Think about the Buddha and how he said he had a greater

277
00:20:31,760 --> 00:20:37,650
 nourishment than the food, his happiness wasn't dependent

278
00:20:37,650 --> 00:20:40,000
 on getting what he wanted,

279
00:20:40,000 --> 00:20:45,460
 because his way of looking at things was not in terms of

280
00:20:45,460 --> 00:20:51,590
 things that you can get, it's in terms of experiences which

281
00:20:51,590 --> 00:21:01,130
 frees you and liberates you from the slavery really of

282
00:21:01,130 --> 00:21:04,000
 needing things.

283
00:21:04,000 --> 00:21:07,690
 So that's the Dhammapada for tonight, thank you all for

284
00:21:07,690 --> 00:21:10,000
 listening, wish you all the best.

