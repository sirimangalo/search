1
00:00:00,000 --> 00:00:05,000
 Okay, good evening.

2
00:00:05,000 --> 00:00:22,600
 We're broadcasting live to the light of today's passage is

3
00:00:22,600 --> 00:00:24,000
 about Hataka.

4
00:00:24,000 --> 00:00:31,000
 Or you could say it's specifically about modesty.

5
00:00:31,000 --> 00:00:37,000
 Or you could say it's about eight things.

6
00:00:37,000 --> 00:00:49,000
 List of eight qualities that Hataka is posing down with.

7
00:00:49,000 --> 00:00:53,190
 Hataka is an interesting individual, an eminent lay

8
00:00:53,190 --> 00:00:54,000
 disciple.

9
00:00:54,000 --> 00:01:04,000
 It's not made clear by this passage.

10
00:01:04,000 --> 00:01:08,550
 But he was actually not a monk, which makes this story even

11
00:01:08,550 --> 00:01:09,000
 curious,

12
00:01:09,000 --> 00:01:15,360
 because he's expressing a wish that there were no lay

13
00:01:15,360 --> 00:01:22,000
 people present to hear when the Buddha praised him.

14
00:01:22,000 --> 00:01:26,590
 So here he is a lay person, and even as a lay person he's

15
00:01:26,590 --> 00:01:30,000
 afraid, not afraid,

16
00:01:30,000 --> 00:01:36,400
 but concerned of the wish that no one should know, no other

17
00:01:36,400 --> 00:01:42,000
 lay people should know of his being praised by the Buddha.

18
00:01:42,000 --> 00:01:47,000
 There's danger in praise, from just sort of jealousy.

19
00:01:47,000 --> 00:01:58,620
 But the Buddha expresses the opinion that Hataka was

20
00:01:58,620 --> 00:02:03,450
 reluctant or have the wish that no one should know about

21
00:02:03,450 --> 00:02:05,000
 the Buddha's praise,

22
00:02:05,000 --> 00:02:10,340
 or the fewness of wishes. It's translated here as modesty,

23
00:02:10,340 --> 00:02:15,360
 but the word is 'apicchata', which means not wanting for

24
00:02:15,360 --> 00:02:20,000
 much, not wanting much.

25
00:02:20,000 --> 00:02:27,280
 Sort of contentment, you might say, what modesty might be,

26
00:02:27,280 --> 00:02:29,000
 if you will.

27
00:02:29,000 --> 00:02:46,560
 He was a lay person, and he was always accompanied by 500

28
00:02:46,560 --> 00:02:52,000
 lay people, 500 people, and he had a large retinue.

29
00:02:52,000 --> 00:03:00,270
 The Buddha asked him once how he was able to gain such a

30
00:03:00,270 --> 00:03:07,200
 large retinue, such a large company of friends, how was he

31
00:03:07,200 --> 00:03:09,000
 so popular?

32
00:03:09,000 --> 00:03:16,460
 And Hataka answered that there are these four bases of

33
00:03:16,460 --> 00:03:23,000
 sympathy that allowed him to become so popular,

34
00:03:23,000 --> 00:03:30,670
 meaning these four acts, or four means by which people

35
00:03:30,670 --> 00:03:34,000
 become sympathetic.

36
00:03:34,000 --> 00:03:59,000
 And this four word, giving gifts, so you might say bribery,

37
00:03:59,000 --> 00:04:03,000
 but no, it's not bribery, being generous with others.

38
00:04:03,000 --> 00:04:07,000
 When people see that you're generous, they appreciate that.

39
00:04:07,000 --> 00:04:14,000
 It's dingy, not holding back, not afraid to share the gift.

40
00:04:14,000 --> 00:04:18,380
 By kindly words, by speaking nicely about people. Of course

41
00:04:18,380 --> 00:04:20,000
, you don't gain friends by being rough.

42
00:04:20,000 --> 00:04:28,440
 Some people think that being blunt can be charming, certain

43
00:04:28,440 --> 00:04:33,000
 people have this charm, they think about themselves,

44
00:04:33,000 --> 00:04:36,840
 that they are blunt, and they say it as it is, and they don

45
00:04:36,840 --> 00:04:41,140
't mince words, and so on, but kind words are important,

46
00:04:41,140 --> 00:04:45,000
 kind words are preferred.

47
00:04:45,000 --> 00:04:48,030
 There are times where you want to be harsh in order to get

48
00:04:48,030 --> 00:04:50,920
 a point across, but just being harsh as a sort of

49
00:04:50,920 --> 00:04:53,000
 personality type is a problem,

50
00:04:53,000 --> 00:05:03,880
 because it jars with people, and excites defilements in

51
00:05:03,880 --> 00:05:06,000
 others.

52
00:05:06,000 --> 00:05:13,170
 Whereas kindly words are immediately greeted by calming of

53
00:05:13,170 --> 00:05:19,000
 other people's minds, and by appreciation.

54
00:05:19,000 --> 00:05:25,030
 Number three, kindly deeds, doing good deeds to others,

55
00:05:25,030 --> 00:05:33,000
 helping others, being helpful.

56
00:05:33,000 --> 00:05:42,010
 Number four, by equality of treatment, by non-favoritism,

57
00:05:42,010 --> 00:05:45,000
 avoiding favoritism.

58
00:05:45,000 --> 00:05:49,670
 It's an interesting quality in life, and we tend to favor

59
00:05:49,670 --> 00:05:55,000
 greatly certain people, our family, our friends,

60
00:05:55,000 --> 00:06:00,390
 and this idea that we go way back, something like that, I

61
00:06:00,390 --> 00:06:04,720
 don't want to give him up, he's my friend, someone does bad

62
00:06:04,720 --> 00:06:05,000
 things,

63
00:06:05,000 --> 00:06:10,810
 and you say, well, I know that they are on a bad path, but

64
00:06:10,810 --> 00:06:15,000
 they're my friend, no, this is not proper.

65
00:06:15,000 --> 00:06:20,400
 Even family, you know, the best is, I'd say it's not easy,

66
00:06:20,400 --> 00:06:25,680
 and it's kind of difficult for especially living in society

67
00:06:25,680 --> 00:06:26,000
,

68
00:06:26,000 --> 00:06:35,000
 but the best is to be able to love the one you're with,

69
00:06:35,000 --> 00:06:39,000
 which means whoever you're with at that time, at the moment

70
00:06:39,000 --> 00:06:39,000
,

71
00:06:39,000 --> 00:06:42,720
 you have to be fully present and to not be thinking about

72
00:06:42,720 --> 00:06:46,000
 someone else or comparing them to other people,

73
00:06:46,000 --> 00:06:52,000
 to treat people evenly, fairly, justly.

74
00:06:52,000 --> 00:06:55,490
 It doesn't mean treat everyone exactly the same, it means

75
00:06:55,490 --> 00:06:58,000
 treat people according to who they are.

76
00:06:58,000 --> 00:07:03,190
 If someone is an evil, evil person, well, you should avoid

77
00:07:03,190 --> 00:07:04,000
 them.

78
00:07:04,000 --> 00:07:08,510
 To not act out of favor to some liking someone more than

79
00:07:08,510 --> 00:07:10,000
 someone else.

80
00:07:10,000 --> 00:07:13,300
 If someone is an evil, evil person, you still treat them

81
00:07:13,300 --> 00:07:18,000
 appropriately with the desire to help them as best you can.

82
00:07:18,000 --> 00:07:21,160
 So these four are not in this suttan, but it's just another

83
00:07:21,160 --> 00:07:25,000
 one of the things about Hataka, he was very popular,

84
00:07:25,000 --> 00:07:29,470
 very well known, and the Buddha praised him, as we can see

85
00:07:29,470 --> 00:07:31,000
 in this passage.

86
00:07:31,000 --> 00:07:35,000
 We praised him for eight things, let's go through these

87
00:07:35,000 --> 00:07:36,000
 eight now.

88
00:07:36,000 --> 00:07:41,580
 We have saddha, which means faith, confidence, but it means

89
00:07:41,580 --> 00:07:43,000
 faith in the Buddha.

90
00:07:43,000 --> 00:07:46,630
 Generally it is used to refer to having confidence in the

91
00:07:46,630 --> 00:07:49,000
 Buddha, the Dhamma and the Sangha,

92
00:07:49,000 --> 00:07:53,110
 either through practice or through study or simply through

93
00:07:53,110 --> 00:07:57,000
 blind faith and sort of this confidence.

94
00:07:57,000 --> 00:07:59,770
 You can be blind exactly, but when you see the Buddha or

95
00:07:59,770 --> 00:08:02,980
 think about the Buddha, you think, "Wow, he must be some

96
00:08:02,980 --> 00:08:04,000
 great being."

97
00:08:04,000 --> 00:08:10,000
 Just this kind of unreasonable faith or unreasonable faith.

98
00:08:10,000 --> 00:08:12,850
 It's still good, it's wholesome in the mind because it

99
00:08:12,850 --> 00:08:14,000
 gives you confidence.

100
00:08:14,000 --> 00:08:18,600
 So if you have faith in the proper object, it can propel

101
00:08:18,600 --> 00:08:20,000
 you to practice.

102
00:08:20,000 --> 00:08:24,460
 Probably with being blind faith, you never know whether you

103
00:08:24,460 --> 00:08:27,000
 have faith in the right object.

104
00:08:27,000 --> 00:08:31,150
 Virtue, which means morality, so he was the person who kept

105
00:08:31,150 --> 00:08:32,000
 the five precepts,

106
00:08:32,000 --> 00:08:35,100
 probably the eight precepts because he was an anagami, I

107
00:08:35,100 --> 00:08:36,000
 think.

108
00:08:36,000 --> 00:08:49,000
 When he passed away, he went to one of the anagamis.

109
00:08:49,000 --> 00:08:53,000
 Conscientiousness, which is usually translated as shame,

110
00:08:53,000 --> 00:08:59,000
 but it's sort of a recoiling away from unwholesome deeds.

111
00:08:59,000 --> 00:09:07,010
 The aversion, not disliking, but the natural disinclination

112
00:09:07,010 --> 00:09:09,000
 to perform bad deeds,

113
00:09:09,000 --> 00:09:13,000
 based on the deeds themselves as being unwholesome.

114
00:09:13,000 --> 00:09:18,780
 And 'autapa', which is fear of blame, not blame, fear of

115
00:09:18,780 --> 00:09:20,000
 consequences.

116
00:09:20,000 --> 00:09:23,220
 And that isn't even translated. The word is 'autapa' or '

117
00:09:23,220 --> 00:09:24,000
autapi'.

118
00:09:24,000 --> 00:09:29,000
 One who has 'autapa', which is just fear.

119
00:09:29,000 --> 00:09:31,880
 But it's not fear in the sense of being afraid, it's fear

120
00:09:31,880 --> 00:09:37,000
 in the sense of disinclination from evil deeds,

121
00:09:37,000 --> 00:09:39,000
 because of their consequences.

122
00:09:39,000 --> 00:09:43,360
 So the first one is the actual deeds themselves are the

123
00:09:43,360 --> 00:09:44,000
 object,

124
00:09:44,000 --> 00:09:47,000
 and the second one is the consequences of the object.

125
00:09:47,000 --> 00:09:51,710
 They're really two describing the same idea, and they must

126
00:09:51,710 --> 00:09:53,000
 come very much together.

127
00:09:53,000 --> 00:09:57,000
 But when one thinks about the consequences of deeds,

128
00:09:57,000 --> 00:10:01,000
 one is disinclined to perform those deeds,

129
00:10:01,000 --> 00:10:04,190
 and one thinks about the deeds as being unwholesome

130
00:10:04,190 --> 00:10:06,000
 themselves.

131
00:10:07,000 --> 00:10:14,000
 That's called 'hiri' or 'hiri' or 'hiri' or 'hiri' or 'hiri

132
00:10:14,000 --> 00:10:14,000
' or 'hiri' or 'hiri' or 'hiri'.

133
00:10:14,000 --> 00:10:16,620
 Now I take issue with the next one, because he translated

134
00:10:16,620 --> 00:10:17,000
 as...

135
00:10:17,000 --> 00:10:20,000
 Oh, that's not true. No, he translated correctly.

136
00:10:20,000 --> 00:10:24,000
 Sorry. What was I reading?

137
00:10:24,000 --> 00:10:27,000
 He translated it correctly.

138
00:10:27,000 --> 00:10:34,000
 Sorry. How much learning the 'hosuta'.

139
00:10:34,000 --> 00:10:40,090
 He was someone who had studied a lot, learned much from the

140
00:10:40,090 --> 00:10:41,000
 Buddha's teaching,

141
00:10:41,000 --> 00:10:44,000
 from the Buddha himself and from other teachers.

142
00:10:44,000 --> 00:10:47,000
 He would have approached monks and asked them questions

143
00:10:47,000 --> 00:10:50,000
 about the Buddha's teaching.

144
00:10:50,000 --> 00:10:54,360
 But when he heard there would be Dhammataks, he would go

145
00:10:54,360 --> 00:10:57,000
 and listen to him.

146
00:10:57,000 --> 00:11:00,900
 Remember, this is a lay person, so these qualities are

147
00:11:00,900 --> 00:11:03,000
 quite useful for

148
00:11:03,000 --> 00:11:09,380
 all of you audience members who are then assuming mostly

149
00:11:09,380 --> 00:11:11,000
 lay people.

150
00:11:11,000 --> 00:11:14,570
 Yesterday we were talking about monks. We had these ten

151
00:11:14,570 --> 00:11:15,000
 qualities.

152
00:11:15,000 --> 00:11:18,620
 I didn't go through them all in detail, but they were

153
00:11:18,620 --> 00:11:21,000
 specifically relating to monks.

154
00:11:21,000 --> 00:11:24,000
 These ones are sort of an answer to the question,

155
00:11:24,000 --> 00:11:30,000
 'What does it mean to be a good Buddhist lay person?'

156
00:11:30,000 --> 00:11:35,000
 'Chagawah' means 'generous'.

157
00:11:35,000 --> 00:11:38,000
 He translated it as 'generous', 'generosity'.

158
00:11:38,000 --> 00:11:40,000
 He was endowed with generosity.

159
00:11:40,000 --> 00:11:44,000
 Generosity is usually relating to the...

160
00:11:44,000 --> 00:11:47,180
 Not really, but you could say there's an emphasis on

161
00:11:47,180 --> 00:11:49,000
 relating to the monastics.

162
00:11:49,000 --> 00:11:52,000
 Lay people have a relationship with the monastics.

163
00:11:52,000 --> 00:11:55,000
 I'm trying to be honest here.

164
00:11:55,000 --> 00:11:58,000
 It's awkward as a monk to say these sorts of things,

165
00:11:58,000 --> 00:12:01,000
 but I think if we're going to be honest, that is true.

166
00:12:01,000 --> 00:12:06,820
 Lay people do a great thing by being generous towards the

167
00:12:06,820 --> 00:12:08,000
 monastics.

168
00:12:08,000 --> 00:12:11,000
 But generosity here could mean many different things.

169
00:12:11,000 --> 00:12:16,000
 Like, 'Anatta Pindika' was generous to homeless people.

170
00:12:16,000 --> 00:12:18,000
 That's what the word 'Anatta Pindika'.

171
00:12:18,000 --> 00:12:21,000
 'Anatta' means one who has no refuge,

172
00:12:21,000 --> 00:12:28,000
 I mean someone who gives rice, who gives alms,

173
00:12:28,000 --> 00:12:31,000
 who gives charity to people who have nothing,

174
00:12:31,000 --> 00:12:36,000
 which is the poor people, it wasn't monks.

175
00:12:36,000 --> 00:12:42,000
 Generous towards the homeless, the poor.

176
00:12:42,000 --> 00:12:45,000
 So definitely that should play a part.

177
00:12:45,000 --> 00:12:50,000
 But in general being generous.

178
00:12:50,000 --> 00:12:53,000
 Generous is a great thing no matter who you are,

179
00:12:53,000 --> 00:12:56,000
 whatever you have to give to share.

180
00:12:56,000 --> 00:12:59,000
 Awesome.

181
00:12:59,000 --> 00:13:03,000
 And 'Panyawa', number seven.

182
00:13:03,000 --> 00:13:07,000
 Number seven is 'Panyawa' means you have wisdom.

183
00:13:07,000 --> 00:13:11,000
 He was a wise person.

184
00:13:11,000 --> 00:13:13,740
 And this is different from 'Bahu Surtha' means not just

185
00:13:13,740 --> 00:13:15,000
 having learned a lot.

186
00:13:15,000 --> 00:13:16,810
 Some people think they're wise because they've learned a

187
00:13:16,810 --> 00:13:17,000
 lot.

188
00:13:17,000 --> 00:13:20,000
 Or they think they're wise because they can argue or they

189
00:13:20,000 --> 00:13:20,000
 can teach

190
00:13:20,000 --> 00:13:22,000
 and explain the dhamma.

191
00:13:22,000 --> 00:13:23,000
 This isn't 'Panyawa'.

192
00:13:23,000 --> 00:13:26,000
 'Panyawa' means he was an anagami, so he had practiced

193
00:13:26,000 --> 00:13:31,290
 and had come to understand the nature of reality deeply,

194
00:13:31,290 --> 00:13:34,000
 almost perfectly.

195
00:13:34,000 --> 00:13:38,000
 His understanding of reality was profound.

196
00:13:38,000 --> 00:13:40,000
 So there's another thing.

197
00:13:40,000 --> 00:13:42,000
 So these seven the Buddha talked about.

198
00:13:42,000 --> 00:13:48,290
 And then 'Hathaka' says, 'Well, I hope there were no other

199
00:13:48,290 --> 00:13:51,000
 lay people present to hear that.'

200
00:13:51,000 --> 00:13:55,000
 Because the idea is that...

201
00:13:55,000 --> 00:13:59,000
 It's funny, you know, most Buddhists are not like this.

202
00:13:59,000 --> 00:14:02,000
 It's a very good lesson because most Buddhists are...

203
00:14:02,000 --> 00:14:05,000
 Most of us are not like that.

204
00:14:05,000 --> 00:14:13,000
 We crave, praise from our teachers.

205
00:14:13,000 --> 00:14:16,000
 If the Buddha were to say great things about us,

206
00:14:16,000 --> 00:14:19,700
 we would just live our lives wishing and hoping for the

207
00:14:19,700 --> 00:14:21,000
 Buddha to say something good about us.

208
00:14:21,000 --> 00:14:24,000
 And this is the case with many monks in the Buddhist style.

209
00:14:24,000 --> 00:14:27,640
 When the Buddha praised them or the Buddha didn't praise

210
00:14:27,640 --> 00:14:28,000
 them...

211
00:14:28,000 --> 00:14:30,000
 No, I guess not when the Buddha praised them,

212
00:14:30,000 --> 00:14:34,210
 because if he praised them they would be full of... they

213
00:14:34,210 --> 00:14:36,000
 would already be half fulfilled this.

214
00:14:36,000 --> 00:14:39,130
 But there are many monks in the case where the Buddha didn

215
00:14:39,130 --> 00:14:40,000
't praise them

216
00:14:40,000 --> 00:14:46,540
 and they would be devastated, hoping for the Buddha's

217
00:14:46,540 --> 00:14:50,000
 praise and not getting it.

218
00:14:50,000 --> 00:14:52,000
 I know this quite well.

219
00:14:52,000 --> 00:14:56,900
 I was always craving and we all were always craving our two

220
00:14:56,900 --> 00:14:59,000
 Ajahn Tong prays

221
00:14:59,000 --> 00:15:06,000
 and as a teacher I know the feeling of having your students

222
00:15:06,000 --> 00:15:11,000
 wait anxiously for you to praise them

223
00:15:11,000 --> 00:15:14,000
 or come to you and ask you how their practice is going.

224
00:15:14,000 --> 00:15:17,410
 Often, I mean this is quite innocent, but meditators can be

225
00:15:17,410 --> 00:15:19,000
 quite self-conscious

226
00:15:19,000 --> 00:15:23,030
 and have doubt worrying that their practice is not

227
00:15:23,030 --> 00:15:24,000
 progressing.

228
00:15:24,000 --> 00:15:26,360
 And so they come to the teacher and they want the teacher

229
00:15:26,360 --> 00:15:29,000
 to... that's actually different.

230
00:15:29,000 --> 00:15:31,000
 That's not seeking praise.

231
00:15:31,000 --> 00:15:34,670
 That's seeking confirmation that you're not doing something

232
00:15:34,670 --> 00:15:38,000
 wrong because you just don't know.

233
00:15:38,000 --> 00:15:42,000
 But it's quite common when a meditator wants the teacher,

234
00:15:42,000 --> 00:15:44,980
 hopes that the teacher will say, "Oh, this is one of my

235
00:15:44,980 --> 00:15:46,000
 star meditators."

236
00:15:46,000 --> 00:15:54,480
 I remember when I was in California teaching and it was a

237
00:15:54,480 --> 00:15:56,000
 community, you know, the Thai community there.

238
00:15:56,000 --> 00:16:00,490
 So these were people, they were all friends and I couldn't

239
00:16:00,490 --> 00:16:01,000
...

240
00:16:01,000 --> 00:16:05,000
 Anytime I would praise anyone, everyone else would just...

241
00:16:05,000 --> 00:16:09,480
 I mean good people, but there was such jealousy because

242
00:16:09,480 --> 00:16:11,000
 everyone wanted to be praised.

243
00:16:11,000 --> 00:16:16,000
 So I realized after a while that I couldn't praise anyone.

244
00:16:16,000 --> 00:16:18,390
 I had to praise everyone. You don't praise everyone

245
00:16:18,390 --> 00:16:20,000
 generally.

246
00:16:20,000 --> 00:16:27,000
 You're just asking for trouble.

247
00:16:27,000 --> 00:16:31,230
 So this is an example for us to emulate. We shouldn't need

248
00:16:31,230 --> 00:16:32,000
 praise.

249
00:16:32,000 --> 00:16:37,530
 The Buddha said, "A great being is someone who is alike in

250
00:16:37,530 --> 00:16:40,000
 both praise and blame."

251
00:16:40,000 --> 00:16:42,820
 Which means when you praise them, it's almost as though

252
00:16:42,820 --> 00:16:44,000
 they didn't hear it.

253
00:16:44,000 --> 00:16:47,390
 It's almost as though they didn't... It's almost as though

254
00:16:47,390 --> 00:16:49,000
 it doesn't register.

255
00:16:49,000 --> 00:16:52,950
 But it's not that it doesn't register. It's that they don't

256
00:16:52,950 --> 00:16:54,000
...

257
00:16:54,000 --> 00:17:01,000
 It doesn't hold any meaning for them.

258
00:17:01,000 --> 00:17:04,310
 Because it's conceited to say, "Oh, you're such a great

259
00:17:04,310 --> 00:17:05,000
 person."

260
00:17:05,000 --> 00:17:09,830
 It doesn't mean anything to someone who has no attachment

261
00:17:09,830 --> 00:17:11,000
 to the person.

262
00:17:11,000 --> 00:17:14,370
 Someone says, "You're such a horrible person and you're

263
00:17:14,370 --> 00:17:20,000
 boring, you're mean, you're ugly."

264
00:17:20,000 --> 00:17:25,000
 It also doesn't register because they have no attachment.

265
00:17:25,000 --> 00:17:31,000
 They have no attachment to self.

266
00:17:31,000 --> 00:17:34,840
 It's almost as though they don't hear when people insult

267
00:17:34,840 --> 00:17:36,000
 them as well.

268
00:17:36,000 --> 00:17:39,000
 Modesty. This is what the Buddha said.

269
00:17:39,000 --> 00:17:44,000
 He said, "Well, this is the eighth quality of akhaka."

270
00:17:44,000 --> 00:17:47,000
 So there you are. Some dhamma for laypeople.

271
00:17:47,000 --> 00:17:55,000
 Qualities that laypeople can use as a good guideline.

272
00:17:55,000 --> 00:17:58,490
 Well, this is a guideline for how to be a great, special

273
00:17:58,490 --> 00:17:59,000
 person.

274
00:17:59,000 --> 00:18:01,650
 These are things we can emulate that shouldn't feel

275
00:18:01,650 --> 00:18:04,000
 discouraged if we're not perfect in all of them.

276
00:18:04,000 --> 00:18:09,020
 There's not anything in there that's beyond what it's

277
00:18:09,020 --> 00:18:11,000
 really expected.

278
00:18:11,000 --> 00:18:16,000
 They're all pretty stable.

279
00:18:16,000 --> 00:18:21,000
 That's dhamma for tonight. Thank you all for tuning in.

280
00:18:22,000 --> 00:18:23,000
 Thank you.

