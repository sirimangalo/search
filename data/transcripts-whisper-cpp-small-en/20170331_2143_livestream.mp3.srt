1
00:00:00,000 --> 00:00:04,680
 outwardly or respect formally. In the sense, this is a

2
00:00:04,680 --> 00:00:06,320
 senior monk, I will respect that.

3
00:00:06,320 --> 00:00:14,240
 And it's not artificial. It's just, it has nothing to do

4
00:00:14,240 --> 00:00:16,080
 with the person's personality. It's their

5
00:00:16,080 --> 00:00:19,080
 station. So respecting a station, I think, is very

6
00:00:19,080 --> 00:00:21,200
 important, very useful in a society.

7
00:00:21,200 --> 00:00:25,680
 Very useful in a community.

8
00:00:25,680 --> 00:00:37,520
 Anyway, if there are no more questions, then have a good

9
00:00:37,520 --> 00:00:46,320
 night everyone and see you next time.

10
00:00:46,320 --> 00:00:52,960
 So

11
00:00:54,000 --> 00:00:59,600
 so

12
00:00:59,600 --> 00:01:06,240
 so

13
00:01:06,240 --> 00:01:12,880
 so

14
00:01:12,880 --> 00:01:19,520
 so

15
00:01:19,520 --> 00:01:26,160
 so

16
00:01:26,160 --> 00:01:32,800
 so

17
00:01:33,760 --> 00:01:39,440
 so

18
00:01:39,440 --> 00:01:46,080
 so

19
00:01:46,080 --> 00:01:52,720
 so

