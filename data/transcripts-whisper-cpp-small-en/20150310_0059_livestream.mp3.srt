1
00:00:00,000 --> 00:00:23,560
 Okay, so here we are live at Stony Creek, Ontario.

2
00:00:23,560 --> 00:00:35,960
 In Buddhism we have something we call the Abhidhamma.

3
00:00:35,960 --> 00:00:47,440
 There's some confusion and a lot of ignorance surrounding

4
00:00:47,440 --> 00:00:49,120
 the Abhidhamma.

5
00:00:49,120 --> 00:00:54,640
 But ignorance just means many people don't really know what

6
00:00:54,640 --> 00:00:56,880
 the Abhidhamma is.

7
00:00:56,880 --> 00:01:03,680
 Maybe many people have never heard of it.

8
00:01:03,680 --> 00:01:07,800
 They practice mindfulness and Buddhist teaching without

9
00:01:07,800 --> 00:01:13,960
 ever having heard about the Abhidhamma.

10
00:01:13,960 --> 00:01:34,480
 Abhi means higher, higher dhamma for absolute.

11
00:01:34,480 --> 00:01:46,010
 Abhi means strong, strengthening, so it's more dhamma than

12
00:01:46,010 --> 00:01:48,120
 dhamma.

13
00:01:48,120 --> 00:01:51,280
 In Buddhism we recognize two levels of dhamma.

14
00:01:51,280 --> 00:02:08,530
 There are the conceptual dhammas and then there are dhammas

15
00:02:08,530 --> 00:02:15,520
 in ultimate reality.

16
00:02:15,520 --> 00:02:36,510
 Conceptually refers to that level of reality that is people

17
00:02:36,510 --> 00:02:45,000
, places, things, concepts.

18
00:02:45,000 --> 00:03:07,240
 Concepts like who we are, man, woman, monk.

19
00:03:07,240 --> 00:03:14,800
 Concepts like where we are, canada.

20
00:03:14,800 --> 00:03:20,150
 We say all of these things exist, but they only exist in

21
00:03:20,150 --> 00:03:23,400
 the sense that a chair exists.

22
00:03:23,400 --> 00:03:31,080
 You see a chair, we say it's a chair.

23
00:03:31,080 --> 00:03:37,350
 We believe that the chair exists and we have reason to

24
00:03:37,350 --> 00:03:39,400
 believe that.

25
00:03:39,400 --> 00:03:40,900
 And this is real.

26
00:03:40,900 --> 00:03:48,890
 On one level this is the truth, but on a higher level it's

27
00:03:48,890 --> 00:03:51,480
 not true at all.

28
00:03:51,480 --> 00:03:56,630
 Differences in how you look at the chair or that which you

29
00:03:56,630 --> 00:03:59,200
 experienced or thought of as

30
00:03:59,200 --> 00:04:04,200
 the chair.

31
00:04:04,200 --> 00:04:15,270
 Whether you argue that the chair exists or doesn't exist,

32
00:04:15,270 --> 00:04:20,000
 we understand there's a higher

33
00:04:20,000 --> 00:04:27,080
 level of existence in reality.

34
00:04:27,080 --> 00:04:32,940
 There's a level of reality that is more real than the chair

35
00:04:32,940 --> 00:04:36,520
 and that's simply the experience

36
00:04:36,520 --> 00:04:37,520
 of the chair.

37
00:04:37,520 --> 00:04:41,620
 When the experience actually of light, when you see the

38
00:04:41,620 --> 00:04:43,960
 chair or sound when you hear the

39
00:04:43,960 --> 00:04:56,040
 chair, thought when you think about the chair.

40
00:04:56,040 --> 00:05:07,650
 This is the very basis of what we deal with in insight

41
00:05:07,650 --> 00:05:11,240
 meditation.

42
00:05:11,240 --> 00:05:22,000
 The abhidhamma is what we focus on in insight meditation.

43
00:05:22,000 --> 00:05:31,880
 And so it's certainly something we should be interested in.

44
00:05:31,880 --> 00:05:42,660
 In fact we must be able to differentiate between ordinary

45
00:05:42,660 --> 00:05:47,720
 reality and ultimate reality.

46
00:05:47,720 --> 00:06:07,750
 Because ordinary reality being conceptual turns out to have

47
00:06:07,750 --> 00:06:13,320
 this fatal flaw of...

48
00:06:13,320 --> 00:06:29,810
 Of misleading us, misleading us into believing in stability

49
00:06:29,810 --> 00:06:33,920
, misleading us into believing

50
00:06:33,920 --> 00:06:45,880
 in pleasure or satisfaction, misleading us to believe in

51
00:06:45,880 --> 00:07:01,760
 self, entity, control, ownership.

52
00:07:01,760 --> 00:07:12,480
 Self is the ultimate reality.

53
00:07:12,480 --> 00:07:18,820
 Because underlying every concept is another level of

54
00:07:18,820 --> 00:07:21,400
 ultimate reality.

55
00:07:21,400 --> 00:07:26,150
 And so the chair, the truth about the chair is it could

56
00:07:26,150 --> 00:07:29,560
 break, fall apart and disintegrate

57
00:07:29,560 --> 00:07:33,560
 and disappear.

58
00:07:33,560 --> 00:07:38,240
 With enough alteration there's no more chair.

59
00:07:38,240 --> 00:07:45,960
 Chair simply ceases to be.

60
00:07:45,960 --> 00:07:50,230
 If you clench your hand you see a fist but when you open

61
00:07:50,230 --> 00:07:52,760
 your hand you could ask where's

62
00:07:52,760 --> 00:07:55,760
 the fist?

63
00:07:55,760 --> 00:08:00,760
 Where'd it go?

64
00:08:00,760 --> 00:08:03,760
 You make it again and there's the fist again.

65
00:08:03,760 --> 00:08:17,760
 And then it disappears.

66
00:08:17,760 --> 00:08:22,760
 The truth about concepts is they only exist in the mind.

67
00:08:22,760 --> 00:08:26,700
 The chair doesn't know it's a chair, doesn't have anything

68
00:08:26,700 --> 00:08:28,760
 about it, no chairness about

69
00:08:28,760 --> 00:08:30,760
 it at all.

70
00:08:30,760 --> 00:08:38,230
 There's nothing essentially chair-like about a chair except

71
00:08:38,230 --> 00:08:41,760
 in our minds that assimilate

72
00:08:41,760 --> 00:08:46,410
 the information and associate it with other information and

73
00:08:46,410 --> 00:08:48,760
 say, "Oh that's like that.

74
00:08:48,760 --> 00:08:51,760
 This is like that."

75
00:08:51,760 --> 00:09:01,980
 It compares and associates and then decides that that's a

76
00:09:01,980 --> 00:09:03,760
 chair.

77
00:09:03,760 --> 00:09:09,760
 But the chair is only in our mind.

78
00:09:09,760 --> 00:09:17,560
 It's an interpretation of the experience.

79
00:09:17,560 --> 00:09:27,790
 So that reality will always be on a lower level, more

80
00:09:27,790 --> 00:09:32,560
 abstract level than the truth

81
00:09:32,560 --> 00:09:36,360
 of the reality of the experience.

82
00:09:36,360 --> 00:09:48,360
 So this is what the abhidhamma deals with.

83
00:09:48,360 --> 00:09:49,360
 Many, many pages of many books.

84
00:09:49,360 --> 00:09:53,950
 The abhidhamma makes up many volumes just explaining

85
00:09:53,950 --> 00:09:55,360
 experience.

86
00:09:55,360 --> 00:10:01,120
 In fact, the ultimate reality only comes down to four

87
00:10:01,120 --> 00:10:02,360
 things.

88
00:10:02,360 --> 00:10:07,360
 This is what we call the abhidhamma.

89
00:10:07,360 --> 00:10:10,360
 The abhidhamma is really only four things.

90
00:10:10,360 --> 00:10:13,360
 This is jitta.

91
00:10:13,360 --> 00:10:17,360
 Jitta is the mind.

92
00:10:17,360 --> 00:10:22,360
 So the mind is real.

93
00:10:22,360 --> 00:10:25,360
 But not real in the way we think of it.

94
00:10:25,360 --> 00:10:28,630
 The mind lasting from one moment to the next, that's not

95
00:10:28,630 --> 00:10:31,360
 what we mean by mind.

96
00:10:31,360 --> 00:10:34,460
 The reality of mind is something that arises and ceases

97
00:10:34,460 --> 00:10:35,360
 every moment.

98
00:10:35,360 --> 00:10:38,360
 The mind only exists one moment.

99
00:10:38,360 --> 00:10:43,360
 When we say jitta, by mind we mean one moment.

100
00:10:43,360 --> 00:10:46,360
 One mind.

101
00:10:46,360 --> 00:10:49,360
 Seeing.

102
00:10:49,360 --> 00:10:57,360
 In fact, seeing is made up of many moments of mind.

103
00:10:57,360 --> 00:10:59,360
 Many mind moments.

104
00:10:59,360 --> 00:11:04,360
 Each moment has one mind.

105
00:11:04,360 --> 00:11:11,360
 First there's the mind that reaches out to the object.

106
00:11:11,360 --> 00:11:15,360
 Then there's the mind that experiences the object.

107
00:11:15,360 --> 00:11:25,360
 And there's the mind that analyzes the object.

108
00:11:25,360 --> 00:11:29,530
 And then there's the mind that recognizes and decides about

109
00:11:29,530 --> 00:11:30,360
 the object.

110
00:11:30,360 --> 00:11:33,360
 And there's the mind that reacts to the object.

111
00:11:33,360 --> 00:11:38,490
 There's all these minds moment after moment just to see

112
00:11:38,490 --> 00:11:41,360
 something.

113
00:11:41,360 --> 00:11:44,470
 But we don't have to go so deep and detailed for our

114
00:11:44,470 --> 00:11:45,360
 purposes.

115
00:11:45,360 --> 00:11:52,560
 All we have to know is mind, the awareness, every moment of

116
00:11:52,560 --> 00:11:54,360
 awareness that's real.

117
00:11:54,360 --> 00:11:58,060
 When you're aware of the body, aware of the pain, aware of

118
00:11:58,060 --> 00:12:01,360
 the thoughts, aware of the emotions.

119
00:12:01,360 --> 00:12:09,360
 Awareness with all these things, that's mind.

120
00:12:09,360 --> 00:12:13,020
 The second thing that's real is, the second that we've done

121
00:12:13,020 --> 00:12:14,360
 is called jita-sika.

122
00:12:14,360 --> 00:12:17,360
 And jita-sika actually is part of mind.

123
00:12:17,360 --> 00:12:21,360
 But it describes the qualities of mind.

124
00:12:21,360 --> 00:12:27,360
 Wholesome qualities, unwholesome qualities.

125
00:12:27,360 --> 00:12:31,360
 Pleasant, unpleasant.

126
00:12:31,360 --> 00:12:41,360
 Functional, all sorts of functional qualities.

127
00:12:41,360 --> 00:12:45,360
 Memory in here, recognition when you recognize something.

128
00:12:45,360 --> 00:12:51,360
 It's a quality of a certain mind to recognize.

129
00:12:51,360 --> 00:12:54,360
 Pain, pleasure, these are qualities of mind.

130
00:12:54,360 --> 00:12:59,360
 Liking, disliking, these are qualities of mind.

131
00:12:59,360 --> 00:13:03,360
 Love and compassion are qualities of mind.

132
00:13:03,360 --> 00:13:07,360
 Anger, quality of mind.

133
00:13:07,360 --> 00:13:14,360
 Wisdom, ignorance, all these things.

134
00:13:14,360 --> 00:13:17,360
 Many of them are just functional that allow us to,

135
00:13:17,360 --> 00:13:29,360
 qualities of mind that allow it to observe an object.

136
00:13:29,360 --> 00:13:32,360
 The third abhidhamma is rupa.

137
00:13:32,360 --> 00:13:37,430
 Rupa is form, first to the material aspect of the

138
00:13:37,430 --> 00:13:38,360
 experience.

139
00:13:38,360 --> 00:13:42,780
 Material world, we say, does the world exist, does the

140
00:13:42,780 --> 00:13:44,360
 universe exist?

141
00:13:44,360 --> 00:13:47,360
 On an ultimate level, not in the way we think.

142
00:13:47,360 --> 00:13:51,950
 On the ultimate level, rupa is just another aspect of

143
00:13:51,950 --> 00:13:53,360
 experience.

144
00:13:53,360 --> 00:14:00,360
 It's the hardness and softness.

145
00:14:00,360 --> 00:14:23,360
 It's the physical aspect of experience.

146
00:14:23,360 --> 00:14:30,360
 It's what is studied in physics.

147
00:14:30,360 --> 00:14:33,360
 It's energy, the mass and energy,

148
00:14:33,360 --> 00:14:38,630
 combined to give us sensations of heat and cold and

149
00:14:38,630 --> 00:14:40,360
 hardness and softness

150
00:14:40,360 --> 00:14:51,360
 and pressure, cohesion, all of this.

151
00:14:51,360 --> 00:14:57,180
 These three dhammas make up all of the world that we live

152
00:14:57,180 --> 00:14:57,360
 in,

153
00:14:57,360 --> 00:15:08,360
 all the worlds, all experiences, all mundane experiences.

154
00:15:08,360 --> 00:15:16,360
 The key is in differentiating this from ordinary reality.

155
00:15:16,360 --> 00:15:21,540
 It's actually quite important that during our practice we

156
00:15:21,540 --> 00:15:27,360
 attempt to shift our point of view

157
00:15:27,360 --> 00:15:34,360
 from concepts to ultimate reality, to experience,

158
00:15:34,360 --> 00:15:42,360
 seeing each experience just as an experience.

159
00:15:42,360 --> 00:15:47,510
 I agree on the story of a monk who saw a beautiful woman

160
00:15:47,510 --> 00:15:48,360
 running down the street,

161
00:15:48,360 --> 00:15:50,360
 down the road.

162
00:15:50,360 --> 00:15:53,360
 She was running away from her husband.

163
00:15:53,360 --> 00:15:58,360
 She was all decked out and finery.

164
00:15:58,360 --> 00:16:02,360
 She saw him and she thought he looked ridiculous.

165
00:16:02,360 --> 00:16:04,360
 So she laughed.

166
00:16:04,360 --> 00:16:10,360
 He looked up and saw her teeth.

167
00:16:10,360 --> 00:16:15,960
 He became enlightened, looking at her teeth because he was

168
00:16:15,960 --> 00:16:24,360
 so absorbed in this meditation.

169
00:16:24,360 --> 00:16:27,910
 That it triggered something in him, he saw it as bones

170
00:16:27,910 --> 00:16:28,360
 actually.

171
00:16:28,360 --> 00:16:33,360
 He saw the teeth of this woman as bones.

172
00:16:33,360 --> 00:16:39,010
 He even gave him the clarity of mind that allowed him to

173
00:16:39,010 --> 00:16:41,360
 see clearly and become enlightened.

174
00:16:41,360 --> 00:16:48,360
 But the point is he didn't see the woman.

175
00:16:48,360 --> 00:16:52,190
 So this is the difference between seeing concepts and

176
00:16:52,190 --> 00:16:53,360
 seeing reality.

177
00:16:53,360 --> 00:17:01,570
 For practice we have to try and see just reality, ultimate

178
00:17:01,570 --> 00:17:03,360
 reality.

179
00:17:03,360 --> 00:17:14,360
 As we do this our mind begins to align with nature.

180
00:17:14,360 --> 00:17:22,390
 So instead of meeting with disappointment and setting

181
00:17:22,390 --> 00:17:28,360
 ourselves up for unfulfilled expectations,

182
00:17:28,360 --> 00:17:32,360
 we're able to see exactly what's going on.

183
00:17:32,360 --> 00:17:37,190
 We're able to understand concepts as conceptual and see the

184
00:17:37,190 --> 00:17:38,360
 realities behind them

185
00:17:38,360 --> 00:17:43,820
 so that we're never caught off guard and in line with

186
00:17:43,820 --> 00:17:45,360
 reality.

187
00:17:45,360 --> 00:17:49,130
 Being in line with reality is another interesting effect in

188
00:17:49,130 --> 00:17:53,360
 that it means no more clinging to reality.

189
00:17:53,360 --> 00:17:58,360
 So the mind begins to release and let go.

190
00:17:58,360 --> 00:18:05,100
 Let's go until finally reality stops interfering, the

191
00:18:05,100 --> 00:18:07,360
 mundane reality stops interfering.

192
00:18:07,360 --> 00:18:13,360
 The mind enters into a state of peace.

193
00:18:13,360 --> 00:18:16,630
 It's like closing the door of your room or turning up the

194
00:18:16,630 --> 00:18:18,360
 light when you go to sleep.

195
00:18:18,360 --> 00:18:23,150
 This rest, this peace, the world is still there but we've

196
00:18:23,150 --> 00:18:29,360
 stopped communicating, we've stopped engaging.

197
00:18:29,360 --> 00:18:32,360
 And this is the fourth ultimate reality.

198
00:18:32,360 --> 00:18:36,360
 Abhidhamma is called Nibana or Nirvana.

199
00:18:36,360 --> 00:18:42,070
 It's a state where the mind ceases to go out, ceases to

200
00:18:42,070 --> 00:18:47,360
 engage with the mundane universe.

201
00:18:47,360 --> 00:18:51,360
 And it enters into a state of cessation.

202
00:18:51,360 --> 00:18:56,880
 It means no thinking, no seeing, no hearing, no smelling,

203
00:18:56,880 --> 00:19:01,360
 no tasting, no feeling, no thinking.

204
00:19:02,360 --> 00:19:12,360
 No more arising, no more ceasing.

205
00:19:12,360 --> 00:19:17,320
 So this is our aim, to be more in line with this, more in

206
00:19:17,320 --> 00:19:19,360
 tune with this reality

207
00:19:19,360 --> 00:19:26,360
 so that we can see what is useful, what is harmful.

208
00:19:26,360 --> 00:19:31,360
 So that we can see why we suffer, what causes us suffering.

209
00:19:31,360 --> 00:19:35,360
 We can see how to find happiness and peace.

210
00:19:35,360 --> 00:19:38,360
 Be able to see the difference.

211
00:19:38,360 --> 00:19:42,060
 This is the Abhidhamma, something very important for medit

212
00:19:42,060 --> 00:19:46,360
ators to understand at least, at least on this level.

213
00:19:46,360 --> 00:19:49,820
 It doesn't mean you have to go studying all the books of

214
00:19:49,820 --> 00:19:51,360
 the Abhidhamma though.

215
00:19:51,360 --> 00:19:56,580
 It's certainly enlightening to browse through and see just

216
00:19:56,580 --> 00:20:06,360
 how deep is the Buddhist body of knowledge.

217
00:20:06,360 --> 00:20:09,360
 Much more useful though is to actually practice.

218
00:20:09,360 --> 00:20:12,360
 This is what we're aiming for.

219
00:20:12,360 --> 00:20:15,360
 So, we keep practicing.

220
00:20:15,360 --> 00:20:16,360
 Here we have more Dhamma.

221
00:20:16,360 --> 00:20:21,640
 I'll try to be here Monday to Friday, see if I can keep

222
00:20:21,640 --> 00:20:22,360
 that up.

223
00:20:22,360 --> 00:20:27,360
 In the meantime, keep practicing.

