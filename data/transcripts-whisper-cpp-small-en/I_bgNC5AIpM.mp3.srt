1
00:00:00,000 --> 00:00:02,000
 Hello everyone

2
00:00:02,000 --> 00:00:27,680
 So good evening, Robin hello

3
00:00:27,680 --> 00:00:29,680
 Elbante

4
00:00:29,680 --> 00:00:37,500
 Terrible things happening in America as usual

5
00:00:37,500 --> 00:00:42,130
 Yes, you mean the hostage situation at the Planned

6
00:00:42,130 --> 00:00:43,300
 Parenthood

7
00:00:43,300 --> 00:00:48,000
 Yeah, I

8
00:00:48,000 --> 00:00:51,500
 Mean whether it actually had anything to do with it. There

9
00:00:51,500 --> 00:00:53,480
's this big thing in America that

10
00:00:53,480 --> 00:00:57,040
 worship

11
00:00:57,680 --> 00:01:00,850
 It's interesting I've had of course thoughts on it because

12
00:01:00,850 --> 00:01:01,520
 of course

13
00:01:01,520 --> 00:01:07,220
 I'm a woman and the other uterus and so on no because

14
00:01:07,220 --> 00:01:11,880
 The whole idea of life

15
00:01:11,880 --> 00:01:16,720
 Whether it is a wrong having the passion

16
00:01:16,720 --> 00:01:21,000
 Problems with it, I think I am pro-choice

17
00:01:21,000 --> 00:01:25,680
 That's really the issue here. I never really got it before

18
00:01:26,600 --> 00:01:28,600
 because

19
00:01:28,600 --> 00:01:30,800
 What

20
00:01:30,800 --> 00:01:34,230
 Pro-choice people say women should have control of their

21
00:01:34,230 --> 00:01:38,320
 own body, right? And the answer is always well

22
00:01:38,320 --> 00:01:40,320
 But what about the baby's body?

23
00:01:40,320 --> 00:01:45,180
 But I think the part of the argument that's being made is

24
00:01:45,180 --> 00:01:47,640
 not just ignoring of the baby's body

25
00:01:47,640 --> 00:01:50,400
 That's not really the point. The point is that the woman

26
00:01:50,400 --> 00:01:53,320
 has

27
00:01:53,320 --> 00:01:55,920
 It's like it's her own

28
00:01:55,920 --> 00:01:57,920
 country right her own state

29
00:01:57,920 --> 00:02:00,520
 so she has

30
00:02:00,520 --> 00:02:03,440
 That she's the government of her own body

31
00:02:03,440 --> 00:02:08,300
 Meaning everyone in there has to you know, she is still it

32
00:02:08,300 --> 00:02:10,680
 still sounds cruel and awful to me

33
00:02:10,680 --> 00:02:14,940
 You know that you should kill all your subject all your

34
00:02:14,940 --> 00:02:16,680
 citizens, you know

35
00:02:16,680 --> 00:02:20,720
 It's basically what it's saying. But nonetheless, I do

36
00:02:20,720 --> 00:02:24,640
 believe that people should have choice. I do believe it's

37
00:02:24,640 --> 00:02:31,000
 I'm not keen on society. I

38
00:02:31,000 --> 00:02:35,500
 Mean I'm not I guess I shouldn't say I know I think there's

39
00:02:35,500 --> 00:02:37,920
 an argument to be made from a Buddhist point of view

40
00:02:37,920 --> 00:02:40,200
 for giving people this kind of freedom

41
00:02:40,200 --> 00:02:43,480
 you know because

42
00:02:43,480 --> 00:02:48,640
 Especially living in a

43
00:02:48,640 --> 00:02:50,920
 pluralistic society

44
00:02:50,920 --> 00:02:55,160
 Where we we only we only have a reason just to

45
00:02:55,160 --> 00:03:02,950
 Impose morality where it where it inhibits people's

46
00:03:02,950 --> 00:03:04,840
 freedoms and so on I

47
00:03:04,840 --> 00:03:09,960
 Don't know I guess anyway, it's an interesting debate and I

48
00:03:09,960 --> 00:03:11,280
'm not clearly

49
00:03:11,280 --> 00:03:17,340
 Anti-abortion pro-life is a say and pro-life pro-life, but

50
00:03:17,340 --> 00:03:18,960
 also probably approach eyes

51
00:03:20,400 --> 00:03:22,560
 I don't think the Buddha was very keen on laws

52
00:03:22,560 --> 00:03:25,800
 You just would have us follow the laws

53
00:03:25,800 --> 00:03:31,640
 But it goes without saying obviously that as a Buddhist one

54
00:03:31,640 --> 00:03:34,680
 should never have an abortion

55
00:03:34,680 --> 00:03:38,080
 Not cool

56
00:03:38,080 --> 00:03:40,680
 But whether as a Buddhist the government should

57
00:03:40,680 --> 00:03:43,280
 be should

58
00:03:43,280 --> 00:03:45,320
 illegal eyes abortions

59
00:03:45,320 --> 00:03:48,740
 I don't think that's fair. I'm not sure that that's fair

60
00:03:49,260 --> 00:03:51,260
 On

61
00:03:51,260 --> 00:03:53,590
 The other hand I should probably just be quiet because it's

62
00:03:53,590 --> 00:03:55,220
 none of my business and I'm just a monk

63
00:03:55,220 --> 00:03:58,220
 Let society work these things out

64
00:03:58,220 --> 00:04:05,290
 Any thoughts Robin you have children I do I have four

65
00:04:05,290 --> 00:04:09,620
 children thoughts on abortion

66
00:04:09,620 --> 00:04:14,900
 Not exactly abortion because I think we're probably pretty

67
00:04:14,900 --> 00:04:16,220
 similar but on

68
00:04:17,220 --> 00:04:19,220
 the abortion movement

69
00:04:19,220 --> 00:04:22,980
 Okay, the anti-abortion movement when it becomes you know

70
00:04:22,980 --> 00:04:27,180
 vigilant like that when it becomes violent like that is

71
00:04:27,180 --> 00:04:29,660
 ridiculous because

72
00:04:29,660 --> 00:04:32,500
 You're killing people to make your point not to kill people

73
00:04:32,500 --> 00:04:35,060
 so

74
00:04:35,060 --> 00:04:38,710
 I don't think they actually know the the motive with this

75
00:04:38,710 --> 00:04:40,460
 particular case tonight

76
00:04:40,460 --> 00:04:43,750
 They talked about you know, it could be domestic violence

77
00:04:43,750 --> 00:04:45,860
 or a disgruntled worker or anything

78
00:04:45,860 --> 00:04:48,420
 They really they hadn't specifically said that it was

79
00:04:48,420 --> 00:04:52,500
 One of these sort of violent pro-life people

80
00:04:52,500 --> 00:04:55,420
 The fact that he was in a Planned Parenthood

81
00:04:55,420 --> 00:04:58,620
 office those kind of

82
00:04:58,620 --> 00:05:03,520
 Box likely right it is likely. Yeah, and they said that he

83
00:05:03,520 --> 00:05:05,950
 brought some some things in that they thought might be

84
00:05:05,950 --> 00:05:06,700
 bombs or something

85
00:05:06,700 --> 00:05:09,820
 So it's a lot of questions

86
00:05:09,820 --> 00:05:13,900
 What are people thinking? Did you see that comic I posted?

87
00:05:13,900 --> 00:05:16,700
 It actually got quite a bit of you got some negative flag

88
00:05:16,700 --> 00:05:19,540
 someone posted this comic that I thought was really neat

89
00:05:19,540 --> 00:05:22,820
 it said it was of all these world leaders and they're all

90
00:05:22,820 --> 00:05:23,780
 killing each other and

91
00:05:23,780 --> 00:05:27,080
 like like there's a lot of there's terrorists on the list

92
00:05:27,080 --> 00:05:27,860
 and there's

93
00:05:27,860 --> 00:05:32,000
 Like Dick Cheney was on the list Netanyahu and they're all

94
00:05:32,000 --> 00:05:34,420
 talking about how if we kill people it's going to

95
00:05:34,420 --> 00:05:37,100
 We're going to achieve our goals

96
00:05:37,100 --> 00:05:39,140
 Yeah

97
00:05:39,140 --> 00:05:42,270
 And then this is killing people hasn't really had the

98
00:05:42,270 --> 00:05:45,940
 effect we were hoping for maybe we should try something new

99
00:05:45,940 --> 00:05:50,210
 Yeah, like not killing people to show how killing people is

100
00:05:50,210 --> 00:05:50,620
 wrong

101
00:05:50,620 --> 00:05:57,470
 It's it's kind of the same with capital punishment you can

102
00:05:57,470 --> 00:05:59,640
 say, you know, here we are killing someone to show how

103
00:05:59,640 --> 00:06:00,700
 killing people is wrong

104
00:06:00,700 --> 00:06:06,140
 There's a lot of that

105
00:06:06,860 --> 00:06:10,110
 Yeah, I mean an eye for an eye is I don't think a very well

106
00:06:10,110 --> 00:06:12,420
 thought-out strategy

107
00:06:12,420 --> 00:06:20,380
 Doesn't seem to be I guess arguably it's a deterrent right

108
00:06:20,380 --> 00:06:20,980
 but

109
00:06:20,980 --> 00:06:25,180
 Doesn't seem to be a deterrent

110
00:06:25,180 --> 00:06:29,280
 They'll do their horrible things and and then someone else

111
00:06:29,280 --> 00:06:31,900
 has to kill the person because it's their job

112
00:06:31,900 --> 00:06:33,900
 And then they have that bad karma, right?

113
00:06:36,500 --> 00:06:38,500
 Hmm

114
00:06:38,500 --> 00:06:43,940
 We have some questions, oh

115
00:06:43,940 --> 00:06:49,950
 Okay, shoot, okay. Will the med mob be recorded and put up

116
00:06:49,950 --> 00:06:51,040
 on your channel

117
00:06:51,040 --> 00:06:56,060
 I put a picture up. Oh Facebook

118
00:06:56,060 --> 00:06:59,060
 How did it go?

119
00:06:59,060 --> 00:07:01,180
 There were five of us

120
00:07:01,180 --> 00:07:04,430
 Which was expected. I wasn't expecting a large it wasn't a

121
00:07:04,430 --> 00:07:05,620
 mob. It was just a

122
00:07:06,140 --> 00:07:08,140
 group

123
00:07:08,140 --> 00:07:11,860
 So Monday, I'm gonna try to do it in the atrium this

124
00:07:11,860 --> 00:07:15,420
 Ancient I'm told that's not the right word at Arboretum

125
00:07:15,420 --> 00:07:18,180
 Although it's also an atrium

126
00:07:18,180 --> 00:07:21,380
 Big open space with some treason

127
00:07:21,380 --> 00:07:26,550
 It's actually a perfect room for it and it's usually quite

128
00:07:26,550 --> 00:07:29,850
 it's very quiet and there's people sometimes sitting at the

129
00:07:29,850 --> 00:07:30,380
 benches

130
00:07:32,380 --> 00:07:36,090
 So Monday will do that that's an indoor room with trees in

131
00:07:36,090 --> 00:07:39,020
 it. Yeah, that sounds really nice. Mm-hmm

132
00:07:39,020 --> 00:07:41,650
 yeah, it's neat that they've done this big window, you know

133
00:07:41,650 --> 00:07:42,900
 one walls just windows and

134
00:07:42,900 --> 00:07:47,660
 Big trees

135
00:07:47,660 --> 00:07:52,060
 Evergreens

136
00:07:52,060 --> 00:07:55,780
 That sounds really nice

137
00:07:56,020 --> 00:07:59,850
 And the waterfall but the waterfall is chlorinated which is

138
00:07:59,850 --> 00:08:03,340
 absurd because the whole room smells like chlorine anyway

139
00:08:03,340 --> 00:08:06,020
 Kind of just a funny

140
00:08:06,020 --> 00:08:08,860
 People I don't understand

141
00:08:08,860 --> 00:08:10,900
 Yeah, I

142
00:08:10,900 --> 00:08:13,620
 Think there are some pretty serious

143
00:08:13,620 --> 00:08:17,330
 Problems with water that that's not purified that wasn't

144
00:08:17,330 --> 00:08:19,620
 there with that where um

145
00:08:19,620 --> 00:08:24,500
 What is that the people get when they go on cruise ships

146
00:08:24,500 --> 00:08:24,900
 and

147
00:08:25,820 --> 00:08:28,870
 Never mind, you know, you can salt the water my mother's

148
00:08:28,870 --> 00:08:32,300
 pool in Florida has salted not that this is at all

149
00:08:32,300 --> 00:08:33,980
 Really? Let's take the Buddhist

150
00:08:33,980 --> 00:08:35,980
 questions

151
00:08:35,980 --> 00:08:37,660
 But yeah

152
00:08:37,660 --> 00:08:40,460
 Back on track next question next question

153
00:08:40,460 --> 00:08:43,090
 Have you found that people's minds tend to have a bias or

154
00:08:43,090 --> 00:08:45,880
 higher propensity towards one particular delusion over

155
00:08:45,880 --> 00:08:46,580
 another?

156
00:08:46,580 --> 00:08:50,060
 For example greed anger, etc

157
00:08:50,060 --> 00:08:52,060
 I

158
00:08:52,060 --> 00:08:59,080
 Suppose yeah, everyone's different most people have all of

159
00:08:59,080 --> 00:09:01,740
 this all the comments mixed up inside

160
00:09:01,740 --> 00:09:08,100
 We're mostly full put to put them in full

161
00:09:08,100 --> 00:09:11,700
 Full of defilements put to China

162
00:09:16,380 --> 00:09:19,500
 When I first started doing walking meditation, I wobbled a

163
00:09:19,500 --> 00:09:21,780
 lot and lost my balance a lot now

164
00:09:21,780 --> 00:09:24,170
 I'm better at balancing but a lot of times my legs will

165
00:09:24,170 --> 00:09:26,380
 shake when I'm walking and I feel like I'm not strong

166
00:09:26,380 --> 00:09:26,780
 enough

167
00:09:26,780 --> 00:09:30,220
 Am I not strong enough or am I walking too much or too

168
00:09:30,220 --> 00:09:32,780
 little or am I just doing it wrong?

169
00:09:32,780 --> 00:09:35,980
 Yeah, just human

170
00:09:35,980 --> 00:09:39,160
 It's not supposed to be perfect. It's in fact supposed to

171
00:09:39,160 --> 00:09:41,260
 be just supposed to show you the imperfections

172
00:09:41,260 --> 00:09:44,810
 We're doing it right and the fact that you're seeing the

173
00:09:44,810 --> 00:09:48,540
 wobbling is good. You're seeing that your body is imperfect

174
00:09:48,540 --> 00:09:51,980
 But what you're also seeing is that you want it to be

175
00:09:51,980 --> 00:09:52,460
 perfect

176
00:09:52,460 --> 00:09:55,410
 I have this tendency to desire for your body to get

177
00:09:55,410 --> 00:09:58,700
 stronger and better and so on it's not gonna happen

178
00:09:58,700 --> 00:10:02,900
 Guess what? You're gonna get a weaker more feeble

179
00:10:02,900 --> 00:10:05,500
 less balanced

180
00:10:05,500 --> 00:10:09,460
 Until you get crippled and bedridden and die. That's your

181
00:10:09,460 --> 00:10:10,140
 future. Oh

182
00:10:11,660 --> 00:10:14,340
 Congratulations. Well done for being born

183
00:10:14,340 --> 00:10:18,030
 It's amazing how when you do something and you don't think

184
00:10:18,030 --> 00:10:21,600
 about it if you're not walking mindfully if you're just

185
00:10:21,600 --> 00:10:22,780
 walking you feel

186
00:10:22,780 --> 00:10:25,860
 Like you walk great, but the more you really are in tune

187
00:10:25,860 --> 00:10:27,620
 with the movements of walking

188
00:10:27,620 --> 00:10:31,700
 The worse you realize you walk. It's but you're not

189
00:10:31,700 --> 00:10:36,800
 You're not exactly letting it flow. We're kind of interrupt

190
00:10:36,800 --> 00:10:38,100
ing the process

191
00:10:39,420 --> 00:10:43,660
 On purpose and we're you know, it's a bit more challenging

192
00:10:43,660 --> 00:10:47,740
 but I think more to the point we get kind of

193
00:10:47,740 --> 00:10:52,860
 We as you do wise to continue walking meditation if you're

194
00:10:52,860 --> 00:10:58,290
 not being very mindful of everything of all your movements

195
00:10:58,290 --> 00:10:58,580
 and

196
00:10:58,580 --> 00:11:01,580
 It's very easy to build up

197
00:11:01,580 --> 00:11:04,820
 Aversion or

198
00:11:04,820 --> 00:11:08,300
 Distraction or so on to the point where it it affects you

199
00:11:08,300 --> 00:11:10,810
 because if you're very mindful if you're very calm if you

200
00:11:10,810 --> 00:11:11,500
're very focused

201
00:11:11,500 --> 00:11:13,500
 Walking meditation should be fairly easy

202
00:11:13,500 --> 00:11:21,050
 But even still the body is not well equipped to do slow

203
00:11:21,050 --> 00:11:22,460
 movements like that

204
00:11:22,460 --> 00:11:26,300
 You're not as well equipped as it is to do ordinary walking

205
00:11:26,300 --> 00:11:28,100
 which is easier I would say

206
00:11:30,140 --> 00:11:34,660
 ordinary walking is I get into rhythm and rhythms are good

207
00:11:34,660 --> 00:11:36,100
 for the body because

208
00:11:36,100 --> 00:11:39,700
 For the mind because you build up this static

209
00:11:39,700 --> 00:11:44,830
 Charge and so the mind is able to send out the instructions

210
00:11:44,830 --> 00:11:45,660
 on her

211
00:11:45,660 --> 00:11:48,970
 It's just comfortable. You know, the mind is good with this

212
00:11:48,970 --> 00:11:50,700
 getting an erupt we kind of break that up

213
00:11:50,700 --> 00:11:55,380
 On purpose because we want to see how the mind is going the

214
00:11:55,380 --> 00:11:57,980
 mind reacts. We want to challenge and we want

215
00:11:59,100 --> 00:12:03,700
 Sort of push the mind push it to its limits until it

216
00:12:03,700 --> 00:12:06,780
 It's or force it to

217
00:12:06,780 --> 00:12:10,070
 To show if it's going to react, you know, so if the mind's

218
00:12:10,070 --> 00:12:12,080
 going to get upset about something we want to see that

219
00:12:12,080 --> 00:12:16,220
 You know to change that we have to push it to the point

220
00:12:16,220 --> 00:12:17,540
 where it would get upset

221
00:12:17,540 --> 00:12:22,160
 So that we can learn how to deal with difficulty and not

222
00:12:22,160 --> 00:12:22,780
 get upset

223
00:12:22,780 --> 00:12:25,560
 So it should be challenging. It should be uncomfortable. It

224
00:12:25,560 --> 00:12:27,620
 should force you out of your comfort zone

225
00:12:29,180 --> 00:12:31,340
 So if it was just ordinary walking and if it was easy that

226
00:12:31,340 --> 00:12:34,900
 wouldn't do that it should be impermanent unstable and

227
00:12:34,900 --> 00:12:36,660
 satisfying and constant

228
00:12:36,660 --> 00:12:41,620
 Uncontrolling that's what you should see. So good for you

229
00:12:41,620 --> 00:12:53,780
 We get caught up we are caught up

230
00:12:53,780 --> 00:12:57,300
 It's 958

231
00:12:57,300 --> 00:12:59,300
 So that's a good hour

232
00:12:59,300 --> 00:13:03,580
 I'm a father would be up tonight tomorrow morning probably

233
00:13:03,580 --> 00:13:08,740
 So let's call it a night

234
00:13:08,740 --> 00:13:11,540
 Thanks Robin

235
00:13:11,540 --> 00:13:13,940
 Patiently sitting with me

236
00:13:13,940 --> 00:13:16,380
 Thank you everyone for tuning in

237
00:13:16,380 --> 00:13:20,620
 Have a good night. Thank you, Bonta. Good night

238
00:13:20,620 --> 00:13:22,620
 You

239
00:13:22,620 --> 00:13:24,620
 You

240
00:13:24,620 --> 00:13:26,620
 You

241
00:13:26,620 --> 00:13:28,620
 You

242
00:13:28,620 --> 00:13:30,620
 You

