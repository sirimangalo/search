1
00:00:00,000 --> 00:00:07,820
 So here I am in Thailand, staying at one of my close

2
00:00:07,820 --> 00:00:10,000
 friends' houses.

3
00:00:10,000 --> 00:00:13,000
 Sort of a relative.

4
00:00:13,000 --> 00:00:20,000
 And here I am back in Thailand.

5
00:00:20,000 --> 00:00:29,510
 One thing that I think is quite interesting is how it doesn

6
00:00:29,510 --> 00:00:32,000
't mean as much as it used to,

7
00:00:32,000 --> 00:00:35,000
 to travel and to go places.

8
00:00:35,000 --> 00:00:39,000
 It's interesting how when you practice meditation,

9
00:00:39,000 --> 00:00:45,000
 you stop looking for the right place to be,

10
00:00:45,000 --> 00:00:56,000
 and focus much more on your own place in wherever you are,

11
00:00:56,000 --> 00:01:02,340
 meaning how you fit in and how you work and how you react

12
00:01:02,340 --> 00:01:04,000
 to the situation.

13
00:01:04,000 --> 00:01:07,310
 You start to be more concerned with what's inside than what

14
00:01:07,310 --> 00:01:08,000
's outside.

15
00:01:08,000 --> 00:01:13,630
 And that's really what we're trying to develop in the

16
00:01:13,630 --> 00:01:17,000
 practice, what we try to teach in regards to meditation,

17
00:01:17,000 --> 00:01:24,000
 that wherever you are, it's much more important who you are

18
00:01:24,000 --> 00:01:29,000
 and how you react to it than what's around.

19
00:01:29,000 --> 00:01:33,450
 So this looks like a wonderful place and everyone's always

20
00:01:33,450 --> 00:01:36,410
 jealous when they see me going to these beautiful,

21
00:01:36,410 --> 00:01:37,000
 wonderful places,

22
00:01:37,000 --> 00:01:45,570
 but you can get the same sense of peace, happiness and

23
00:01:45,570 --> 00:01:49,000
 freedom wherever you are.

24
00:01:49,000 --> 00:01:54,980
 And this sort of beauty and nature and peace and serenity

25
00:01:54,980 --> 00:02:01,560
 outside is totally lost if your mind is not in a good place

26
00:02:01,560 --> 00:02:02,000
.

27
00:02:02,000 --> 00:02:08,430
 So just a thought of the day. If you want to find your

28
00:02:08,430 --> 00:02:11,000
 place, it's all about what's inside.

29
00:02:11,000 --> 00:02:15,110
 You have to find your place in how you react to the world

30
00:02:15,110 --> 00:02:16,000
 around you.

31
00:02:16,000 --> 00:02:23,050
 Because in the end, it's all seeing, hearing, smelling,

32
00:02:23,050 --> 00:02:26,000
 tasting, feeling and thinking.

33
00:02:26,000 --> 00:02:33,260
 There's nothing beyond those six things. Wherever you go,

34
00:02:33,260 --> 00:02:35,000
 they're all the same.

35
00:02:35,000 --> 00:02:39,990
 What's different is your reaction, your ability to accept

36
00:02:39,990 --> 00:02:46,000
 and to be happy wherever you are at all times.

37
00:02:46,000 --> 00:02:53,360
 So this is my first video from Thailand and I just thought

38
00:02:53,360 --> 00:02:58,000
 I'd say hi and share my thoughts.

39
00:02:58,000 --> 00:03:00,000
 Okay, all the best.

40
00:03:02,000 --> 00:03:03,000
 Thank you.

