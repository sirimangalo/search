1
00:00:00,000 --> 00:00:05,060
 What to note? Question. When many phenomena are present,

2
00:00:05,060 --> 00:00:08,000
 which phenomena should one note? Answer.

3
00:00:08,000 --> 00:00:11,340
 One should note what is most prevalent in one's field of

4
00:00:11,340 --> 00:00:14,160
 awareness. The particular object one

5
00:00:14,160 --> 00:00:17,460
 notes is far less important than the fact that one is

6
00:00:17,460 --> 00:00:20,480
 noting. It is not necessary to try to catch

7
00:00:20,480 --> 00:00:24,620
 every phenomena, as noting is just the means of cultivating

8
00:00:24,620 --> 00:00:27,120
 objectivity. If one tries to note

9
00:00:27,120 --> 00:00:30,920
 every phenomenon that arises, one will become overwhelmed

10
00:00:30,920 --> 00:00:32,880
 and unable to manage objective

11
00:00:32,880 --> 00:00:36,950
 observation. Noting should therefore be used to augment one

12
00:00:36,950 --> 00:00:39,360
's ordinary perception at a rate that

13
00:00:39,360 --> 00:00:42,750
 is comfortable and consistent, generally not more

14
00:00:42,750 --> 00:00:45,120
 frequently than once per second.

