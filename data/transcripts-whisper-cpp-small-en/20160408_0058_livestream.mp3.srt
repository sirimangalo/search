1
00:00:00,000 --> 00:00:21,000
 [Silence]

2
00:00:21,000 --> 00:00:35,000
 Good evening everyone. I'm broadcasting live April 7th.

3
00:00:35,000 --> 00:00:41,000
 Today's quote has actually got quite a bit in it.

4
00:00:41,000 --> 00:00:46,000
 Even though it's a simple message.

5
00:00:46,000 --> 00:00:57,000
 This is from the "Ithi Vutaka".

6
00:00:57,000 --> 00:01:01,000
 The Buddha says, "Even should one

7
00:01:01,000 --> 00:01:19,000
 "Sankhati karni jee bhi bhikure" "Even if one should, the

8
00:01:19,000 --> 00:01:19,000
 corner of my robe, gahitwa, grasp, take hold of it."

9
00:01:19,000 --> 00:01:25,450
 "Even if one having taken hold of the corner of my robe, b

10
00:01:25,450 --> 00:01:31,730
hiitthito, bhiitthito, anubhando, asa, should follow after

11
00:01:31,730 --> 00:01:32,000
 me."

12
00:01:32,000 --> 00:01:36,000
 "Bhiitthito, bhiittho, follow behind my back."

13
00:01:36,000 --> 00:01:39,000
 One after the other.

14
00:01:39,000 --> 00:01:49,000
 "Padhe padhe padha padang padhe padang nih khipanto"

15
00:01:49,000 --> 00:01:57,000
 "Stepping in my footsteps."

16
00:01:57,000 --> 00:02:02,000
 "Putting down his feet in my footsteps."

17
00:02:02,000 --> 00:02:06,230
 It's literally getting just about as close as you could to

18
00:02:06,230 --> 00:02:09,000
 the Buddha, following him everywhere.

19
00:02:09,000 --> 00:02:23,330
 "So, jahoti, and if he is ambijalu, with great desires,

20
00:02:23,330 --> 00:02:27,000
 full of desire."

21
00:02:27,000 --> 00:02:38,750
 "Tithi basarago, kame suthi basarago, with sharp, extreme

22
00:02:38,750 --> 00:02:44,000
 lust for sensuality."

23
00:02:44,000 --> 00:02:55,000
 "Bhiapannathito, with a mind full of ill will."

24
00:02:55,000 --> 00:02:59,950
 "Angry at other people, hating other people with only

25
00:02:59,950 --> 00:03:03,000
 bitter thoughts for others."

26
00:03:03,000 --> 00:03:13,000
 "Badhuta manasangapu, with corrupted mental formations."

27
00:03:13,000 --> 00:03:20,920
 "The thoughts that are corrupted, thinking to harm others,

28
00:03:20,920 --> 00:03:29,000
 and to manipulate others, and to oppress and abuse others."

29
00:03:29,000 --> 00:03:35,000
 "Muthasati, with confused mindfulness."

30
00:03:35,000 --> 00:03:40,710
 "Confused recognition, or reaction, reacting to things in a

31
00:03:40,710 --> 00:03:45,000
 confused way, without wisdom, without clarity."

32
00:03:45,000 --> 00:03:51,790
 "When things arise, reacting, based on being in the dark,

33
00:03:51,790 --> 00:03:56,000
 based on habit, based on ignorance."

34
00:03:56,000 --> 00:04:01,000
 "So it gives rise to greed and anger."

35
00:04:01,000 --> 00:04:06,000
 "Asampajano, without full comprehension."

36
00:04:06,000 --> 00:04:20,000
 "Without full, but is full jah, is knowledge."

37
00:04:20,000 --> 00:04:24,000
 "Without full knowledge."

38
00:04:24,000 --> 00:04:30,270
 "Asamahito, without concentration or focus, without level-

39
00:04:30,270 --> 00:04:32,000
headedness."

40
00:04:32,000 --> 00:04:40,590
 "So disturbed, with their mind as not impartial, objective,

41
00:04:40,590 --> 00:04:45,000
 able to see things as they are."

42
00:04:45,000 --> 00:04:47,610
 As I said, there's a lot in here, because it looks like a

43
00:04:47,610 --> 00:04:48,000
 simple quote,

44
00:04:48,000 --> 00:04:52,280
 but each of these words has a very important meaning to

45
00:04:52,280 --> 00:04:53,000
 them.

46
00:04:53,000 --> 00:05:00,000
 "Uibhanta-tito," does that mean? It's just more desire, no?

47
00:05:00,000 --> 00:05:07,000
 "Uibhanta," a wandering mind, a confused mind, "Uibhanta,"

48
00:05:07,000 --> 00:05:13,000
 "Uibhamati," to roam, to stray.

49
00:05:13,000 --> 00:05:17,860
 "Uibhanta," with a wandering mind, the mind that doesn't

50
00:05:17,860 --> 00:05:19,000
 stay focused,

51
00:05:19,000 --> 00:05:23,960
 the mind that goes hither and thither, thinking about this

52
00:05:23,960 --> 00:05:25,000
 and that.

53
00:05:25,000 --> 00:05:35,830
 "Pakatindryo," with unguarded, untrained, sort of run-of-

54
00:05:35,830 --> 00:05:37,000
the-mill,

55
00:05:37,000 --> 00:05:40,990
 maybe it doesn't mean that, but "pakatim" can mean ordinary

56
00:05:40,990 --> 00:05:41,000
,

57
00:05:41,000 --> 00:05:48,000
 but here it means unguarded or untrained senses.

58
00:05:48,000 --> 00:05:53,000
 So the senses are our doors by which we experience things,

59
00:05:53,000 --> 00:05:56,450
 but the problem is we don't just experience things, we

60
00:05:56,450 --> 00:05:59,000
 react to them.

61
00:05:59,000 --> 00:06:03,230
 So this is untrained. We're based on habit and memory and

62
00:06:03,230 --> 00:06:04,000
 experience.

63
00:06:04,000 --> 00:06:11,000
 We react to every little thing in a very specific way,

64
00:06:11,000 --> 00:06:18,160
 often as a cause for great suffering for us and others, all

65
00:06:18,160 --> 00:06:21,000
 cause for bad karma.

66
00:06:21,000 --> 00:06:28,270
 So all of these things, so imagine this person holding on

67
00:06:28,270 --> 00:06:33,000
 to the Buddha's coat-tail, so to speak,

68
00:06:33,000 --> 00:06:41,000
 walking in his footsteps, but he's got all these things. He

69
00:06:41,000 --> 00:06:41,000
's full of greed,

70
00:06:41,000 --> 00:06:46,500
 has many desires, has craving or lust for sensuality, has

71
00:06:46,500 --> 00:06:48,000
 ill will,

72
00:06:48,000 --> 00:06:53,000
 has a corrupted thoughts, mental activity.

73
00:06:53,000 --> 00:07:04,220
 He's confused, is with the muddled awareness, without

74
00:07:04,220 --> 00:07:07,000
 comprehension and understanding,

75
00:07:07,000 --> 00:07:13,040
 without focus or samahito, without this kind of balanced,

76
00:07:13,040 --> 00:07:14,000
 calm mind,

77
00:07:14,000 --> 00:07:22,000
 with a wandering mind and with untrained faculties.

78
00:07:22,000 --> 00:07:24,000
 So what about him? What about her?

79
00:07:24,000 --> 00:07:29,000
 "Atokoso arakkava"

80
00:07:29,000 --> 00:07:35,000
 Such a person, well such a person, "Arakkava maiham"

81
00:07:35,000 --> 00:07:42,000
 is very far from me, maiham to me.

82
00:07:42,000 --> 00:07:46,000
 They say, "Far away from me."

83
00:07:46,000 --> 00:07:50,000
 It's as though very far from me.

84
00:07:50,000 --> 00:07:57,000
 "Ahan chatasa" and "me" from him or her.

85
00:07:57,000 --> 00:08:01,000
 "Dangisa heto" what's the cause of that?

86
00:08:01,000 --> 00:08:06,000
 "Daman hi so bhikkve bhikkhu napasati"

87
00:08:06,000 --> 00:08:12,000
 For the Dhamma, bhikkhu, that monk doesn't see.

88
00:08:12,000 --> 00:08:14,000
 They don't see the Dhamma.

89
00:08:14,000 --> 00:08:20,000
 "Dhammaang apasanto namang pasati"

90
00:08:20,000 --> 00:08:31,000
 Not seeing the Dhamma, they don't see me.

91
00:08:31,000 --> 00:08:36,000
 So the meaning here is that, in two ways,

92
00:08:36,000 --> 00:08:39,540
 having all those things is a sign of course of not seeing

93
00:08:39,540 --> 00:08:40,000
 the Dhamma.

94
00:08:40,000 --> 00:08:44,000
 It's really sort of the definition of not seeing the Dhamma

95
00:08:44,000 --> 00:08:44,000
,

96
00:08:44,000 --> 00:08:51,000
 because seeing the Dhamma is to see things as they are

97
00:08:51,000 --> 00:08:59,000
 and to live life impartially, objectively, wisely.

98
00:08:59,000 --> 00:09:02,000
 And all of these things are not that.

99
00:09:02,000 --> 00:09:06,470
 But also the way of explaining it is that these things stop

100
00:09:06,470 --> 00:09:09,000
 us from seeing truth.

101
00:09:09,000 --> 00:09:11,000
 These get in the way.

102
00:09:11,000 --> 00:09:16,000
 You can't see the Dhamma if you have all of these things.

103
00:09:16,000 --> 00:09:19,000
 Even if you live with the Buddha.

104
00:09:19,000 --> 00:09:23,000
 This is why Ajahn Tong said, my teacher said,

105
00:09:23,000 --> 00:09:29,000
 because he's very famous, fairly famous in Thailand.

106
00:09:29,000 --> 00:09:32,000
 And I guess now he's very, very famous.

107
00:09:32,000 --> 00:09:39,380
 But we were going to visit him thousands of people on his

108
00:09:39,380 --> 00:09:44,000
 birthday, just pouring in.

109
00:09:44,000 --> 00:09:53,390
 And so he said, well, going to see noble people, going to

110
00:09:53,390 --> 00:09:54,000
 live with them

111
00:09:54,000 --> 00:10:00,160
 or even wait on them or care for them, or attend to them as

112
00:10:00,160 --> 00:10:01,000
 their student,

113
00:10:01,000 --> 00:10:05,000
 and as their attendant,

114
00:10:05,000 --> 00:10:08,000
 it's not the same as becoming a noble one yourself.

115
00:10:08,000 --> 00:10:10,000
 It's a simple thing to say.

116
00:10:10,000 --> 00:10:18,290
 It's one of those quotes that got published and passed

117
00:10:18,290 --> 00:10:20,000
 around.

118
00:10:20,000 --> 00:10:25,120
 So this is an important quote and an important sort of

119
00:10:25,120 --> 00:10:26,000
 teaching

120
00:10:26,000 --> 00:10:29,450
 in the context of religion, because a lot of people end up

121
00:10:29,450 --> 00:10:30,000
 doing that.

122
00:10:30,000 --> 00:10:33,480
 They think that by associating with good people, that's

123
00:10:33,480 --> 00:10:34,000
 enough.

124
00:10:34,000 --> 00:10:38,290
 The Buddha did say that associating with good people is the

125
00:10:38,290 --> 00:10:40,000
 whole of the holy life.

126
00:10:40,000 --> 00:10:43,550
 But the thing about it is that wise people tend to say

127
00:10:43,550 --> 00:10:45,000
 things like this.

128
00:10:45,000 --> 00:10:50,000
 They say, look, it's not about following me around.

129
00:10:50,000 --> 00:10:52,000
 It's about changing yourself.

130
00:10:52,000 --> 00:10:54,620
 And of course, the great thing about associating with wise

131
00:10:54,620 --> 00:10:58,000
 people is that they teach these things.

132
00:10:58,000 --> 00:11:02,000
 They push you to become a better person.

133
00:11:02,000 --> 00:11:16,000
 They don't let you walk around holding on to their robes.

134
00:11:16,000 --> 00:11:19,590
 And then there's the idea about being far and being near to

135
00:11:19,590 --> 00:11:20,000
 the Buddha,

136
00:11:20,000 --> 00:11:29,000
 which kind of has greater implications

137
00:11:29,000 --> 00:11:39,000
 in terms of the idea of space and the concepts of reality.

138
00:11:39,000 --> 00:11:43,000
 What is real?

139
00:11:43,000 --> 00:11:51,170
 We think of space as being important, location being

140
00:11:51,170 --> 00:11:52,000
 important,

141
00:11:52,000 --> 00:11:58,580
 when in fact it's really just a part of the impression that

142
00:11:58,580 --> 00:12:01,000
 we have of things.

143
00:12:01,000 --> 00:12:06,000
 Someone on Facebook today, an old friend from high school,

144
00:12:06,000 --> 00:12:11,000
 he posted, he said, what was it?

145
00:12:11,000 --> 00:12:14,000
 There's nothing wrong with my brain.

146
00:12:14,000 --> 00:12:17,350
 The anxiety I'm experiencing is just a reflection of all

147
00:12:17,350 --> 00:12:21,000
 the things that are messed up in the world.

148
00:12:21,000 --> 00:12:26,570
 And I replied and I said, the problem is that the world is

149
00:12:26,570 --> 00:12:28,000
 all in your brain.

150
00:12:28,000 --> 00:12:30,000
 Because it's really true.

151
00:12:30,000 --> 00:12:32,000
 It's not even in the mind.

152
00:12:32,000 --> 00:12:36,700
 The mind gets everything from the brain, which projects

153
00:12:36,700 --> 00:12:41,000
 everything and creates things.

154
00:12:41,000 --> 00:12:43,190
 They did a study that said, when you look at something, you

155
00:12:43,190 --> 00:12:45,000
 don't look at the whole object.

156
00:12:45,000 --> 00:12:48,390
 So we think in the room around us, there's all these

157
00:12:48,390 --> 00:12:50,000
 objects that we're seeing.

158
00:12:50,000 --> 00:12:53,000
 In fact, we only see very small bits of it.

159
00:12:53,000 --> 00:12:57,190
 We only see enough to be able to create the picture in our

160
00:12:57,190 --> 00:12:58,000
 minds.

161
00:12:58,000 --> 00:13:03,530
 And they did tests on people that they were able to trick

162
00:13:03,530 --> 00:13:04,000
 them

163
00:13:04,000 --> 00:13:07,000
 into not being able to see something on the pictures.

164
00:13:07,000 --> 00:13:09,000
 They changed something, but you couldn't see it.

165
00:13:09,000 --> 00:13:11,000
 Because you weren't actually seeing the picture.

166
00:13:11,000 --> 00:13:22,480
 You weren't actually seeing the light is only the impetus

167
00:13:22,480 --> 00:13:24,000
 or the spark,

168
00:13:24,000 --> 00:13:29,900
 then the instigation that leads the mind to create the

169
00:13:29,900 --> 00:13:31,000
 image.

170
00:13:33,000 --> 00:13:38,540
 Anyway, the point of how that relates is the world is

171
00:13:38,540 --> 00:13:41,000
 really all in our heads,

172
00:13:41,000 --> 00:13:48,000
 all in our minds, as you have to say.

173
00:13:48,000 --> 00:13:53,150
 So this is how, I mean, this kind of theory, right, of some

174
00:13:53,150 --> 00:13:54,000
 people would challenge it

175
00:13:54,000 --> 00:13:56,000
 and say that's not true.

176
00:13:56,000 --> 00:13:58,610
 The world does exist out there, whether we experience it or

177
00:13:58,610 --> 00:13:59,000
 not.

178
00:13:59,000 --> 00:14:10,000
 But under this theory, the idea of mind being primary,

179
00:14:10,000 --> 00:14:15,000
 this is how the whole idea of rebirth and karma works,

180
00:14:15,000 --> 00:14:20,360
 because we create these experiences, we create these exist

181
00:14:20,360 --> 00:14:21,000
ences.

182
00:14:21,000 --> 00:14:28,000
 And so it's how we understand our relationships with others

183
00:14:28,000 --> 00:14:28,000
,

184
00:14:28,000 --> 00:14:35,000
 why we're brought into contact with the people around us,

185
00:14:35,000 --> 00:14:44,000
 people who we meet in life.

186
00:14:44,000 --> 00:14:49,000
 Because we see with them, but we see them.

187
00:14:49,000 --> 00:14:53,000
 We see in the same way as them.

188
00:14:53,000 --> 00:14:55,470
 And so to be close to the Buddha, the only way to be close

189
00:14:55,470 --> 00:14:56,000
 to the Buddha

190
00:14:56,000 --> 00:15:01,270
 is to see in the way he sees, to see things the way he sees

191
00:15:01,270 --> 00:15:02,000
 them.

192
00:15:02,000 --> 00:15:06,000
 Because the way you see things, the way you look at things,

193
00:15:06,000 --> 00:15:08,750
 the way you react to things, that's the whole of who you

194
00:15:08,750 --> 00:15:09,000
 are.

195
00:15:09,000 --> 00:15:11,000
 That's what makes you who you are.

196
00:15:11,000 --> 00:15:17,000
 That's what determines your life, your future.

197
00:15:17,000 --> 00:15:19,000
 So really that's all we're doing in meditation,

198
00:15:19,000 --> 00:15:25,020
 is learning to see things the way the Buddha saw things,

199
00:15:25,020 --> 00:15:26,000
 which is as they are.

200
00:15:26,000 --> 00:15:30,000
 Why we call him the Buddha is because he awoke to things as

201
00:15:30,000 --> 00:15:30,000
 they are,

202
00:15:30,000 --> 00:15:35,000
 or he came to understand things as they are.

203
00:15:35,000 --> 00:15:39,210
 We claim that we don't see things as they are, we react to

204
00:15:39,210 --> 00:15:40,000
 them.

205
00:15:40,000 --> 00:15:42,530
 I mean, we see things as they are, of course, but we do

206
00:15:42,530 --> 00:15:44,000
 much more than that.

207
00:15:44,000 --> 00:15:50,240
 As soon as you see something as it is, you get lost in how

208
00:15:50,240 --> 00:15:51,000
 you want it to be,

209
00:15:51,000 --> 00:15:56,580
 or how you want it not to be, what you think of it, what it

210
00:15:56,580 --> 00:15:58,000
 makes you think of,

211
00:15:58,000 --> 00:16:06,000
 God in the past, future, not seeing things as they are.

212
00:16:06,000 --> 00:16:11,000
 Because we don't see that, we can't see the Buddha.

213
00:16:11,000 --> 00:16:14,000
 Very far from the Buddha, but right next to him, and then

214
00:16:14,000 --> 00:16:14,000
 he says,

215
00:16:14,000 --> 00:16:20,000
 "Yojana sataye caipi sao bhikkhu e bhikkhu viharaya."

216
00:16:20,000 --> 00:16:28,860
 Suppose a bhikkhu monk, suppose there was a monk, living a

217
00:16:28,860 --> 00:16:29,000
 hundred,

218
00:16:29,000 --> 00:16:34,000
 hundred yojana, just like a league, a hundred yojana,

219
00:16:34,000 --> 00:16:40,110
 like 16 kilometers or something, I don't know, a hundred y

220
00:16:40,110 --> 00:16:41,000
ojana, a hundred far distances.

221
00:16:41,000 --> 00:16:47,000
 So jahoti, and then he is not any of those things.

222
00:16:47,000 --> 00:16:56,000
 Anabijalu, not with many desires, kamisu, nati bhasaranga.

223
00:16:56,000 --> 00:17:00,000
 Actually, I'm not sure which one the kamisu goes with.

224
00:17:00,000 --> 00:17:08,000
 Anabijalu kamisu, maybe it's that way.

225
00:17:08,000 --> 00:17:10,000
 Not with many desires in regards.

226
00:17:10,000 --> 00:17:11,000
 No, I think it's in the way.

227
00:17:11,000 --> 00:17:17,000
 Kamisu, nati bhasaranga, not with sharp lust,

228
00:17:17,000 --> 00:17:22,000
 with strong lust in regards to sensuality.

229
00:17:22,000 --> 00:17:34,000
 Abhyapan, abhyapanajito, abhyapanajito, without ill will.

230
00:17:34,000 --> 00:17:41,000
 Apadurthamanasankapu, with uncorrupted mental formations.

231
00:17:41,000 --> 00:17:48,000
 Upati tasati, with established mindfulness.

232
00:17:48,000 --> 00:17:55,000
 With a mind that is able to grasp things.

233
00:17:55,000 --> 00:17:58,910
 The way of grasping, you say in English, we use the word

234
00:17:58,910 --> 00:18:00,000
 grasp in two ways.

235
00:18:00,000 --> 00:18:04,000
 Because if you grasp something, it means you understand it.

236
00:18:04,000 --> 00:18:07,070
 Not like grasping something, like grasping onto something,

237
00:18:07,070 --> 00:18:08,000
 or grasping for something.

238
00:18:08,000 --> 00:18:10,000
 Grasping in general.

239
00:18:10,000 --> 00:18:13,000
 When you grasp something, it means you understand it.

240
00:18:13,000 --> 00:18:15,000
 And that's what we do in the meditation.

241
00:18:15,000 --> 00:18:18,000
 You're trying to grasp it, just for a moment.

242
00:18:18,000 --> 00:18:20,000
 Just grasp that seeing.

243
00:18:20,000 --> 00:18:23,000
 And you grasp it and you say, "Ah yes, that's seeing."

244
00:18:23,000 --> 00:18:26,070
 Instead of saying, "That's good, that's bad, that's me,

245
00:18:26,070 --> 00:18:27,000
 that's mine."

246
00:18:27,000 --> 00:18:31,000
 And so on.

247
00:18:31,000 --> 00:18:39,360
 Sampajano, fully comprehending, full awareness, full

248
00:18:39,360 --> 00:18:42,000
 knowledge, full and right knowledge.

249
00:18:42,000 --> 00:18:47,000
 Samahito, well balanced, well focused.

250
00:18:47,000 --> 00:18:57,000
 Ekangajito, with a one pointed mind.

251
00:18:57,000 --> 00:19:09,000
 Sanguttindryo is trained and restrained senses.

252
00:19:09,000 --> 00:19:12,000
 Atakoso Santikeva, my hang.

253
00:19:12,000 --> 00:19:17,300
 That person is as though in my presence, ahan chatasa, and

254
00:19:17,300 --> 00:19:18,000
 me and his.

255
00:19:18,000 --> 00:19:21,000
 Dangyasahito, which is the cause.

256
00:19:21,000 --> 00:19:25,000
 Damang Hisobhikvibhikubhasati.

257
00:19:25,000 --> 00:19:32,000
 For that monk sees the dhamma, dhammangpasanto manbhasati.

258
00:19:32,000 --> 00:19:38,000
 Seeing the dhamma, he sees me.

259
00:19:38,000 --> 00:19:46,000
 There you go.

260
00:19:46,000 --> 00:19:54,000
 That's the dhamma for tonight.

261
00:19:54,000 --> 00:19:57,000
 Does anyone have any questions?

262
00:19:57,000 --> 00:20:00,000
 I think I won't make you come on the hang out.

263
00:20:00,000 --> 00:20:03,000
 I'll just, we'll go back to text questions.

264
00:20:03,000 --> 00:20:06,000
 You know what I was thinking?

265
00:20:06,000 --> 00:20:10,330
 What we could do is instead of like doing questions and

266
00:20:10,330 --> 00:20:12,000
 videos about their answers,

267
00:20:12,000 --> 00:20:16,640
 is we could set up some kind of page where people could

268
00:20:16,640 --> 00:20:18,000
 vote on topics.

269
00:20:18,000 --> 00:20:21,000
 Just like one word topics.

270
00:20:21,000 --> 00:20:24,000
 And do a video on this.

271
00:20:24,000 --> 00:20:26,000
 And people could vote on it.

272
00:20:26,000 --> 00:20:30,000
 And I could do a video on certain topics.

273
00:20:30,000 --> 00:20:34,000
 And that would work well.

274
00:20:34,000 --> 00:20:45,000
 Someone has to make such a page.

275
00:20:45,000 --> 00:20:55,000
 If there's any questions, I'll take text questions tonight.

276
00:20:55,000 --> 00:20:58,000
 meditation.siri-mongoloa.org.

277
00:20:58,000 --> 00:21:03,000
 Those of you over at YouTube.

278
00:21:03,000 --> 00:21:07,000
 You have to find our meditation page.

279
00:21:07,000 --> 00:21:10,000
 And they have to be live unfortunately.

280
00:21:10,000 --> 00:21:15,000
 I'm not going to go digging back through the past chat and

281
00:21:15,000 --> 00:21:17,000
 find dead comments.

282
00:21:19,000 --> 00:21:46,000
 [silence]

283
00:21:46,000 --> 00:21:48,000
 About New York, huh?

284
00:21:48,000 --> 00:21:53,000
 Well I posted some events on Facebook.

285
00:21:53,000 --> 00:21:58,000
 So I shared some events that were shared with me.

286
00:21:58,000 --> 00:22:05,000
 So I think that's what you got to do is go to those events.

287
00:22:05,000 --> 00:22:07,000
 Let me see.

288
00:22:07,000 --> 00:22:18,000
 There's a April 16th event at Rockaway Beach.

289
00:22:18,000 --> 00:22:22,000
 90-16 Rockaway Beach Boulevard.

290
00:22:22,000 --> 00:22:23,000
 Wait.

291
00:22:23,000 --> 00:22:28,000
 11693 Rockaway Beach Queens.

292
00:22:28,000 --> 00:22:30,000
 It's at 3pm on April 16th.

293
00:22:30,000 --> 00:22:31,000
 I don't know.

294
00:22:31,000 --> 00:22:33,000
 I mean I'm just getting these as you guys are.

295
00:22:33,000 --> 00:22:37,000
 I didn't plan any of this.

296
00:22:37,000 --> 00:22:41,000
 And then I'm doing a neon meditation.

297
00:22:41,000 --> 00:22:47,000
 7pm at Light Bright Neon 232 3rd Street.

298
00:22:47,000 --> 00:22:50,000
 Number C102 Brooklyn.

299
00:22:50,000 --> 00:22:52,000
 That's April 19th.

300
00:22:52,000 --> 00:22:58,000
 7pm.

301
00:22:58,000 --> 00:23:00,000
 I think like 16th and 17th.

302
00:23:00,000 --> 00:23:03,000
 17th and 18th.

303
00:23:03,000 --> 00:23:07,000
 There's other stuff that I'm not actually the teacher all

304
00:23:07,000 --> 00:23:07,000
 these days.

305
00:23:07,000 --> 00:23:08,000
 I don't know.

306
00:23:08,000 --> 00:23:11,000
 Because these are the only ones I got.

307
00:23:11,000 --> 00:23:14,000
 16th, 19th and the 20th I don't know.

308
00:23:14,000 --> 00:23:18,000
 But the 21st.

309
00:23:18,000 --> 00:23:21,000
 That's Dharma Punks I think.

310
00:23:21,000 --> 00:23:23,000
 No?

311
00:23:23,000 --> 00:23:27,000
 Maybe.

312
00:23:27,000 --> 00:23:31,000
 Yeah, Dharma Punks in Manhattan.

313
00:23:31,000 --> 00:23:32,000
 Don't know.

314
00:23:32,000 --> 00:23:35,000
 You have to find the Dharma Punks place.

315
00:23:35,000 --> 00:23:39,000
 A Lila Yoga Dharma and Wellness.

316
00:23:39,000 --> 00:23:42,000
 That's the place I think.

317
00:23:42,000 --> 00:23:50,000
 And then April 22nd.

318
00:23:50,000 --> 00:23:53,000
 Fort Green Book Brooklyn.

319
00:23:53,000 --> 00:24:04,000
 Happy Lotus Yoga and Vegan Cafe.

320
00:24:04,000 --> 00:24:24,000
 They're all on my page now.

321
00:24:24,000 --> 00:24:26,000
 How is the mind so powerful yet?

322
00:24:26,000 --> 00:24:33,000
 So I think you're trying to say naive.

323
00:24:33,000 --> 00:24:36,000
 Naive is not spelt that way but that's okay.

324
00:24:36,000 --> 00:24:41,000
 I think you're saying naive.

325
00:24:41,000 --> 00:24:45,420
 Well powerful doesn't mean that it's in control of the

326
00:24:45,420 --> 00:24:48,000
 power, right?

327
00:24:48,000 --> 00:24:51,000
 The mind is not in control of its own power.

328
00:24:51,000 --> 00:24:55,000
 That's really the problem.

329
00:24:55,000 --> 00:25:00,000
 And powerful doesn't mean good.

330
00:25:00,000 --> 00:25:02,000
 So the mind ends up...

331
00:25:02,000 --> 00:25:05,000
 And the mind can be very wise, no?

332
00:25:05,000 --> 00:25:12,000
 And the mind can be very smart, clever and so on.

333
00:25:12,000 --> 00:25:17,000
 But it can also be very naive so it twists itself up

334
00:25:17,000 --> 00:25:22,000
 and ends up creating problems for itself.

335
00:25:22,000 --> 00:25:25,000
 Because it has no direction.

336
00:25:25,000 --> 00:25:37,000
 The mind lacks is training.

337
00:25:37,000 --> 00:25:43,000
 Training is a very specific thing.

338
00:25:43,000 --> 00:25:47,000
 The mind that is just going randomly

339
00:25:47,000 --> 00:25:51,000
 is very unlikely to even think about training itself.

340
00:25:51,000 --> 00:25:53,000
 So as a result you see that.

341
00:25:53,000 --> 00:25:57,000
 Most of us don't think of training ourselves.

342
00:25:57,000 --> 00:25:59,000
 I really didn't.

343
00:25:59,000 --> 00:26:02,000
 I went to Asia looking for wisdom, looking for insight

344
00:26:02,000 --> 00:26:05,000
 but didn't think I'd have to work for it.

345
00:26:05,000 --> 00:26:07,000
 Most people don't.

346
00:26:07,000 --> 00:26:09,440
 Most people who come to meditate don't even think of it as

347
00:26:09,440 --> 00:26:10,000
 work.

348
00:26:10,000 --> 00:26:13,360
 They think, "Yeah, meditation, that'll be like a pill I can

349
00:26:13,360 --> 00:26:14,000
 swallow

350
00:26:14,000 --> 00:26:18,050
 and I'll just be... I'll sit down on my mat and poof, I'll

351
00:26:18,050 --> 00:26:19,000
 be happy."

352
00:26:19,000 --> 00:26:23,000
 And they're like, "Oh crap, this is tough."

353
00:26:23,000 --> 00:26:26,470
 A lot of people don't like the meditation that I teach when

354
00:26:26,470 --> 00:26:28,000
 I first teach it.

355
00:26:28,000 --> 00:26:30,120
 You can't teach this to everyone. Not everyone's going to

356
00:26:30,120 --> 00:26:31,000
 appreciate it.

357
00:26:31,000 --> 00:26:33,000
 I'm sure that'll be the case in New York.

358
00:26:33,000 --> 00:26:36,930
 There will be many people who are not very happy about what

359
00:26:36,930 --> 00:26:38,000
 I'm teaching.

360
00:26:38,000 --> 00:26:42,320
 I mean, they won't say it but it won't stick with most

361
00:26:42,320 --> 00:26:43,000
 people.

362
00:26:43,000 --> 00:26:48,000
 So people want comfort, they want an escape.

363
00:26:48,000 --> 00:26:52,470
 It's hard to get people to grow up and learn to look at

364
00:26:52,470 --> 00:26:54,000
 their problems.

365
00:26:54,000 --> 00:26:58,000
 That's maybe harsh language but I don't mean...

366
00:26:58,000 --> 00:27:01,970
 I try to... don't take that too hard and let's not be too

367
00:27:01,970 --> 00:27:03,000
 critical

368
00:27:03,000 --> 00:27:05,000
 but it really is the case.

369
00:27:05,000 --> 00:27:08,280
 I'm not condescending or looking down on such people and I

370
00:27:08,280 --> 00:27:10,000
 was the same.

371
00:27:10,000 --> 00:27:14,700
 We're all in this position where we just... we have to grow

372
00:27:14,700 --> 00:27:15,000
 up.

373
00:27:15,000 --> 00:27:19,000
 We have to start really taking the bull by the horns

374
00:27:19,000 --> 00:27:23,000
 and stop running away and stop fooling ourselves.

375
00:27:23,000 --> 00:27:25,000
 We're going to be able to just ignore the problems

376
00:27:25,000 --> 00:27:29,000
 and we're going to escape our problems.

377
00:27:29,000 --> 00:27:34,000
 Facing your problems is the hardest thing.

378
00:27:34,000 --> 00:27:42,000
 Anyway, kind of went off on a tangent there but...

379
00:27:42,000 --> 00:27:46,000
 the mind is just complex, I suppose.

380
00:27:46,000 --> 00:27:51,000
 As a result it kind of does to itself.

381
00:27:51,000 --> 00:27:54,000
 Right?

382
00:27:54,000 --> 00:27:57,000
 It's like the ocean is powerful, I suppose you could say,

383
00:27:57,000 --> 00:28:02,000
 but most of the time the ocean doesn't do anything.

384
00:28:02,000 --> 00:28:04,730
 You say the ocean is powerful, what the heck does that mean

385
00:28:04,730 --> 00:28:05,000
?

386
00:28:05,000 --> 00:28:07,000
 You go to the ocean, "Ooh, ah!"

387
00:28:07,000 --> 00:28:10,000
 It's not doing anything powerful, right?

388
00:28:10,000 --> 00:28:12,000
 But then you have a tsunami, right?

389
00:28:12,000 --> 00:28:19,010
 Or you have a hurricane and then you see the power of the

390
00:28:19,010 --> 00:28:20,000
 ocean.

391
00:28:20,000 --> 00:28:22,510
 When the ocean is in its natural state, the mind in its

392
00:28:22,510 --> 00:28:25,000
 natural state is diffuse.

393
00:28:25,000 --> 00:28:32,000
 It's full of contradictions that use up all its energy.

394
00:28:32,000 --> 00:28:35,000
 So if you... when you train your mind,

395
00:28:35,000 --> 00:28:38,000
 you have all this energy that's very powerful

396
00:28:38,000 --> 00:28:41,000
 that you have to concentrate it.

397
00:28:41,000 --> 00:29:02,000
 I'm not sure if you're an English native speaker,

398
00:29:02,000 --> 00:29:06,000
 but the words "grow up" in English, it's an idiom,

399
00:29:06,000 --> 00:29:08,000
 it doesn't mean "get older."

400
00:29:08,000 --> 00:29:15,000
 It means "stop being juvenile," "stop being immature."

401
00:29:15,000 --> 00:29:17,000
 It has nothing to do with age.

402
00:29:17,000 --> 00:29:25,000
 "Grow up" means "stop being immature."

403
00:29:34,000 --> 00:29:38,000
 Anyway, if that's all, I'll say goodnight.

404
00:29:38,000 --> 00:29:41,000
 I've still got stuff.

405
00:29:41,000 --> 00:29:45,000
 This Sunday we're having a storytelling,

406
00:29:45,000 --> 00:29:47,810
 and some of you know about this storytelling thing we're

407
00:29:47,810 --> 00:29:49,000
 having on Sunday

408
00:29:49,000 --> 00:29:51,000
 at the Spectator Auditorium.

409
00:29:51,000 --> 00:29:54,000
 If any of you are local, you're welcome to come on over.

410
00:29:54,000 --> 00:29:58,000
 I'll be telling a story from the Jataka about a bird.

411
00:29:58,000 --> 00:30:01,000
 It's the one I told in Second Life recently, I think.

412
00:30:01,000 --> 00:30:07,000
 It's about a bird that stays by its tree,

413
00:30:07,000 --> 00:30:10,020
 stays in its tree even after the tree kind of dries up,

414
00:30:10,020 --> 00:30:12,000
 dies,

415
00:30:12,000 --> 00:30:17,000
 and it stays there out of gratitude to the tree.

416
00:30:17,000 --> 00:30:23,640
 The story... the event is about relating religion to the

417
00:30:23,640 --> 00:30:25,000
 environment

418
00:30:25,000 --> 00:30:27,000
 because there's the issues of climate change.

419
00:30:27,000 --> 00:30:31,000
 How do our religions relate and deal with climate change,

420
00:30:31,000 --> 00:30:34,740
 relate to the environment and protection of the environment

421
00:30:34,740 --> 00:30:35,000
?

422
00:30:35,000 --> 00:30:39,000
 The moral of this story is about contentment, really,

423
00:30:39,000 --> 00:30:44,630
 which is Buddhism's probably the best way that Buddhism

424
00:30:44,630 --> 00:30:45,000
 relates

425
00:30:45,000 --> 00:30:49,000
 to environmental protection and so on.

426
00:30:49,000 --> 00:30:56,000
 Not being greedy and not taking more than is appropriate,

427
00:30:56,000 --> 00:30:58,000
 that kind of thing.

428
00:30:58,000 --> 00:31:06,000
 Not going to seek out and also to be appreciative.

429
00:31:06,000 --> 00:31:09,000
 So the idea of appreciating the environment,

430
00:31:09,000 --> 00:31:14,000
 this bird stays with the tree and appreciates it

431
00:31:14,000 --> 00:31:16,000
 and is content with it.

432
00:31:16,000 --> 00:31:19,000
 So it's about... the Buddha tells the story to a monk

433
00:31:19,000 --> 00:31:24,000
 who gets kind of upset because a village burns down.

434
00:31:24,000 --> 00:31:27,260
 So he's staying and depending on his village for the rains

435
00:31:27,260 --> 00:31:28,000
 retreat

436
00:31:28,000 --> 00:31:31,000
 and the village burned down, I think.

437
00:31:31,000 --> 00:31:35,000
 So it was very hard for him to get food.

438
00:31:35,000 --> 00:31:37,000
 And so he told us the Buddha and the Buddha said,

439
00:31:37,000 --> 00:31:39,000
 "Oh, you know, don't just go buy food.

440
00:31:39,000 --> 00:31:42,000
 Food isn't a good indicator."

441
00:31:42,000 --> 00:31:46,420
 When you get the requisites, that's not a good indicator of

442
00:31:46,420 --> 00:31:49,000
 what a good place is.

443
00:31:49,000 --> 00:31:55,000
 If you can meditate well there, you should stay there.

444
00:31:55,000 --> 00:31:57,630
 And then he told the story of this bird that stayed with

445
00:31:57,630 --> 00:31:58,000
 this tree

446
00:31:58,000 --> 00:32:04,000
 because it was content even when the tree died.

447
00:32:04,000 --> 00:32:06,000
 Anyway, it's a little bit longer than that.

448
00:32:06,000 --> 00:32:08,000
 You have to hear me tell the story.

449
00:32:08,000 --> 00:32:11,000
 But it's just a simple thing.

450
00:32:11,000 --> 00:32:13,000
 So I got to prepare for that kind of...

451
00:32:13,000 --> 00:32:16,000
 And then I got two exams next week.

452
00:32:16,000 --> 00:32:17,000
 And then I'm done.

453
00:32:17,000 --> 00:32:20,000
 And then off to New York.

454
00:32:20,000 --> 00:32:22,000
 And then it's already almost May.

455
00:32:22,000 --> 00:32:25,000
 And then in May we've got one event, the Buddha's birthday

456
00:32:25,000 --> 00:32:28,000
 and this is Saga.

457
00:32:28,000 --> 00:32:33,000
 And then in June I'm off to Asia for a month.

458
00:32:33,000 --> 00:32:37,000
 And then back here for the rains.

459
00:32:37,000 --> 00:32:41,080
 Lots of people interested in coming to meditate all the way

460
00:32:41,080 --> 00:32:43,000
 into like November,

461
00:32:43,000 --> 00:32:50,720
 which is a bit awkward because I can't think that far ahead

462
00:32:50,720 --> 00:32:51,000
.

463
00:32:51,000 --> 00:32:55,650
 I mean, because in the past I've had trouble thinking far

464
00:32:55,650 --> 00:32:56,000
 ahead.

465
00:32:56,000 --> 00:32:58,000
 Now it seems pretty stable.

466
00:32:58,000 --> 00:33:01,910
 So maybe we can start tentatively accepting long-term

467
00:33:01,910 --> 00:33:03,000
 reservations.

468
00:33:03,000 --> 00:33:08,170
 I'm just hesitant to do so because plans change, the future

469
00:33:08,170 --> 00:33:10,000
 is uncertain.

470
00:33:10,000 --> 00:33:13,000
 They could end up moving back to Asia or something.

471
00:33:13,000 --> 00:33:17,000
 Not likely.

472
00:33:17,000 --> 00:33:21,750
 I think we can start to be a little more accepting of

473
00:33:21,750 --> 00:33:29,000
 future plans of this sort these days.

474
00:33:29,000 --> 00:33:31,000
 But we maybe need a bigger place.

475
00:33:31,000 --> 00:33:34,590
 Maybe we have to start thinking about finding a bigger

476
00:33:34,590 --> 00:33:36,000
 house in the area.

477
00:33:36,000 --> 00:33:40,200
 I know it's not nice to have to move, but this house really

478
00:33:40,200 --> 00:33:41,000
 isn't.

479
00:33:41,000 --> 00:33:43,000
 I mean, it's fairly big.

480
00:33:43,000 --> 00:33:45,000
 It's just not well laid out for meditations.

481
00:33:45,000 --> 00:33:48,430
 There are houses close to the university that have like six

482
00:33:48,430 --> 00:33:49,000
 bedrooms

483
00:33:49,000 --> 00:33:52,380
 because they were built and they were renovated to house

484
00:33:52,380 --> 00:33:53,000
 students.

485
00:33:53,000 --> 00:33:56,680
 So you're trying to fit as many students in houses as you

486
00:33:56,680 --> 00:33:57,000
 can,

487
00:33:57,000 --> 00:34:00,000
 which kind of fits our situation.

488
00:34:00,000 --> 00:34:06,000
 They've got six-bedroom houses that are fairly reasonable.

489
00:34:06,000 --> 00:34:09,000
 We should maybe talk about that anyway.

490
00:34:09,000 --> 00:34:14,000
 Anyway, that's all for tonight.

491
00:34:14,000 --> 00:34:16,000
 Thank you guys.

492
00:34:16,000 --> 00:34:18,000
 Thanks for coming out.

493
00:34:18,000 --> 00:34:20,000
 Have a good night.

494
00:34:20,000 --> 00:34:21,000
 [

