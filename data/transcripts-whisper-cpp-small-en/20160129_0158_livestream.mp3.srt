1
00:00:00,000 --> 00:00:24,000
 [ Silence ]

2
00:00:24,500 --> 00:00:32,500
 Good evening everyone. Broadcasting Live, January 28, 2016.

3
00:00:32,500 --> 00:00:41,000
 Today's quote is about admonishing others.

4
00:00:45,000 --> 00:00:55,000
 Serenitypidus, because today, this came up today.

5
00:00:55,000 --> 00:01:00,000
 Well, tangentially, the idea of admonishing others.

6
00:01:00,000 --> 00:01:08,730
 Today I did our meditation table, teaching people 5-minute

7
00:01:08,730 --> 00:01:12,000
 meditation lessons again.

8
00:01:13,000 --> 00:01:15,460
 And I have to do a video to show you all how I do a 5-

9
00:01:15,460 --> 00:01:17,000
minute meditation lesson.

10
00:01:17,000 --> 00:01:22,900
 Once you've done about 100 of them, you can sort of learn

11
00:01:22,900 --> 00:01:25,000
 it by heart.

12
00:01:25,000 --> 00:01:33,570
 But then in the afternoon I had to interrupt it to go to a

13
00:01:33,570 --> 00:01:42,000
 seminar, a panel actually, on peace and health.

14
00:01:44,000 --> 00:01:47,760
 And I say Serenitypidus because during the panel, one of

15
00:01:47,760 --> 00:01:53,540
 the panelists was talking about Palestine and the health

16
00:01:53,540 --> 00:01:59,000
 effects, the ramifications of war in Palestine.

17
00:02:03,000 --> 00:02:09,620
 And one of the audience members got up afterwards and

18
00:02:09,620 --> 00:02:14,750
 denounced this guy as being pro-Palestinian and as not

19
00:02:14,750 --> 00:02:18,000
 representing the true facts and so on.

20
00:02:21,000 --> 00:02:24,530
 And it was bizarre really because we weren't expecting, you

21
00:02:24,530 --> 00:02:27,720
 know, you don't expect when you go to an academic, or I don

22
00:02:27,720 --> 00:02:31,090
't anyway, an academic panel like that to have someone be so

23
00:02:31,090 --> 00:02:34,000
 harshly accusatory in the middle of the panel.

24
00:02:35,000 --> 00:02:40,160
 A woman off to the side spat out at the man who stood up

25
00:02:40,160 --> 00:02:46,590
 and said, "You are a hate-monkard, you really viciously

26
00:02:46,590 --> 00:02:50,000
 attack, you are a hate-monkard."

27
00:02:50,000 --> 00:02:55,000
 She said it like twice or three times.

28
00:02:56,000 --> 00:02:59,220
 Which, I mean, I guess that in and of itself was kind of

29
00:02:59,220 --> 00:03:03,000
 ironic because she definitely hated what this guy had said.

30
00:03:03,000 --> 00:03:08,640
 But what she meant was he was, he was mongering hate, he

31
00:03:08,640 --> 00:03:14,060
 wasn't just hateful, he was trying to cultivate it,

32
00:03:14,060 --> 00:03:19,890
 cultivate division and she accused him of something else

33
00:03:19,890 --> 00:03:22,000
 some other time.

34
00:03:24,000 --> 00:03:27,850
 The most bizarre was it was a peace, you know, this was

35
00:03:27,850 --> 00:03:32,000
 supposed to be a peace seminar or a peace panel, right?

36
00:03:32,000 --> 00:03:36,750
 They'd just given all these talks about peace and conflict

37
00:03:36,750 --> 00:03:41,000
 and then to have real-time conflict flare up.

38
00:03:41,000 --> 00:03:46,360
 It's not like it's absurd or anything but it was quite, and

39
00:03:46,360 --> 00:03:50,000
 all of us, dewie-eyed students,

40
00:03:52,000 --> 00:03:56,190
 sitting there trying to process what's going on. But the

41
00:03:56,190 --> 00:03:58,000
 moderator was really good.

42
00:03:58,000 --> 00:04:02,660
 The thing about people who work in peace is that they're

43
00:04:02,660 --> 00:04:06,390
 ready for these things. That's why peace studies is so

44
00:04:06,390 --> 00:04:09,000
 awesome. It really is a thing.

45
00:04:09,000 --> 00:04:12,280
 Learning how to cultivate peace, learning how to deal with

46
00:04:12,280 --> 00:04:16,600
 conflict, these are the people who resolve conflicts, who

47
00:04:16,600 --> 00:04:18,000
 diffuse conflicts.

48
00:04:19,000 --> 00:04:25,000
 [Silence]

49
00:04:25,000 --> 00:04:28,190
 I just thought it was interesting from the point of view of

50
00:04:28,190 --> 00:04:29,000
 the quote.

51
00:04:29,000 --> 00:04:34,440
 What the quote says is, "You shouldn't admonish people

52
00:04:34,440 --> 00:04:38,000
 unless you're free from faults."

53
00:04:40,000 --> 00:04:42,640
 Because otherwise people would say, "Well hey, what about

54
00:04:42,640 --> 00:04:45,890
 you? Why don't you free yourself from these faults first

55
00:04:45,890 --> 00:04:47,000
 before you accuse him?"

56
00:04:47,000 --> 00:04:52,000
 It's a hypocrisy. It's against hypocrisy.

57
00:04:52,000 --> 00:04:58,780
 It also seems to be subtly against criticism, against admon

58
00:04:58,780 --> 00:05:02,000
ishing others, but not entirely so.

59
00:05:03,000 --> 00:05:06,190
 It's just a challenge that you have to look at yourself

60
00:05:06,190 --> 00:05:10,000
 first. The Buddha is very much about looking at yourself.

61
00:05:10,000 --> 00:05:15,990
 Admonishment of others, it's like a necessary evil in a

62
00:05:15,990 --> 00:05:19,980
 sense. It's what the community has to do if a monk really

63
00:05:19,980 --> 00:05:21,000
 misbehaves.

64
00:05:21,000 --> 00:05:24,000
 But even then you have to be very careful.

65
00:05:27,000 --> 00:05:28,260
 I think it shouldn't be done. I think in those cases it

66
00:05:28,260 --> 00:05:31,400
 really should be done. It would be nice sometimes if we

67
00:05:31,400 --> 00:05:34,000
 were more critical of each other.

68
00:05:34,000 --> 00:05:41,510
 But there's something too paying more attention to yourself

69
00:05:41,510 --> 00:05:42,000
.

70
00:05:46,000 --> 00:05:52,040
 I talk every day, or I talk today all day, I was talking to

71
00:05:52,040 --> 00:05:59,400
 people about diffusing situations, how to deal with

72
00:05:59,400 --> 00:06:02,000
 conflict.

73
00:06:07,000 --> 00:06:12,530
 The best way is to look at yourself, to bring yourself back

74
00:06:12,530 --> 00:06:16,000
 to your own experience of things.

75
00:06:16,000 --> 00:06:19,920
 So if someone does something that you don't like, you have

76
00:06:19,920 --> 00:06:22,000
 to look at your own dislike.

77
00:06:22,000 --> 00:06:25,620
 Like this woman, I think she's a good example. Even though

78
00:06:25,620 --> 00:06:28,000
 maybe she was right about this guy,

79
00:06:29,000 --> 00:06:32,880
 it's funny how she got really angry and used the word hate,

80
00:06:32,880 --> 00:06:36,370
 accused someone else of hate, and she was very, very angry

81
00:06:36,370 --> 00:06:37,000
 herself.

82
00:06:37,000 --> 00:06:41,390
 Had that happened to me once, an old monk yelled at me like

83
00:06:41,390 --> 00:06:46,790
 that. I had made a lot of mistakes. I've made many mistakes

84
00:06:46,790 --> 00:06:47,000
.

85
00:06:47,000 --> 00:06:51,930
 I had made some mistakes. But he turned all red in the face

86
00:06:51,930 --> 00:06:56,000
 and said, "A meditation teacher has to have love.

87
00:06:58,000 --> 00:07:02,590
 Has to have met that kindness." And he was really angry at

88
00:07:02,590 --> 00:07:03,000
 me.

89
00:07:03,000 --> 00:07:07,830
 I think it was false anger, but he worked himself up and he

90
00:07:07,830 --> 00:07:12,970
 was trying to accuse me, I think, to throw the blame off

91
00:07:12,970 --> 00:07:14,000
 himself.

92
00:07:14,000 --> 00:07:21,090
 But anyway, I accepted it. It's kind of bizarre to be

93
00:07:21,090 --> 00:07:24,000
 yelled at about not being kind enough.

94
00:07:27,000 --> 00:07:29,480
 And that's what this quote says, actually. I mean, that's

95
00:07:29,480 --> 00:07:32,000
 very apropos, that one, because it says, you know,

96
00:07:32,000 --> 00:07:34,590
 before you admonish someone, you should ask yourself, "Do

97
00:07:34,590 --> 00:07:37,000
 you have love in your heart?" Because if you don't,

98
00:07:37,000 --> 00:07:41,820
 people are going to say, "Hey, why don't you cultivate some

99
00:07:41,820 --> 00:07:45,000
 of that love you're talking about?"

100
00:07:45,000 --> 00:07:49,000
 It's just bizarre to be yelled at like that.

101
00:07:52,000 --> 00:07:56,750
 But it's interesting, this quote is especially applicable

102
00:07:56,750 --> 00:08:04,000
 to something about the, maybe the European mindset,

103
00:08:04,000 --> 00:08:07,640
 or they call the modern mindset, maybe it's just the modern

104
00:08:07,640 --> 00:08:10,000
 Western mindset. We can be very critical.

105
00:08:11,000 --> 00:08:18,000
 Ah, heck, people can be critical of each other, no matter

106
00:08:18,000 --> 00:08:22,000
 what society. People like to be critical.

107
00:08:22,000 --> 00:08:26,000
 They like to pick on others, they like to admonish others,

108
00:08:26,000 --> 00:08:29,000
 they like to tell each other what to do,

109
00:08:29,000 --> 00:08:35,000
 they like to put down on each other, you know, our faults.

110
00:08:39,000 --> 00:08:42,140
 But I specifically wanted to talk about how Westerners go

111
00:08:42,140 --> 00:08:45,520
 to Buddhism, go to Buddhist monasteries and get really

112
00:08:45,520 --> 00:08:46,000
 critical.

113
00:08:46,000 --> 00:08:54,640
 I think I was guilty of this with my first teacher, he was

114
00:08:54,640 --> 00:08:56,000
 a layman.

115
00:08:56,000 --> 00:09:02,180
 And I ended up breaking up with that whole group because,

116
00:09:02,180 --> 00:09:04,000
 you know, he was,

117
00:09:05,000 --> 00:09:09,890
 he seemed to be fairly egotistical to me and authoritarian

118
00:09:09,890 --> 00:09:15,000
 and obsessed with, well, I don't know,

119
00:09:15,000 --> 00:09:18,430
 he just didn't really appeal to me. And so I started to

120
00:09:18,430 --> 00:09:21,000
 obsess over his faults and it eventually led to a falling

121
00:09:21,000 --> 00:09:21,000
 out

122
00:09:21,000 --> 00:09:24,680
 and to this day we still haven't reconciled. He's still

123
00:09:24,680 --> 00:09:27,000
 teaching at my teacher's monastery.

124
00:09:27,000 --> 00:09:30,170
 So it's always been a bit of a contention, but I was

125
00:09:30,170 --> 00:09:33,000
 thinking, you know, this quote's quite apt.

126
00:09:34,000 --> 00:09:37,240
 Yeah, he's not perfect. No, I don't think I'd want to take

127
00:09:37,240 --> 00:09:41,000
 him as my teacher again, but, you know,

128
00:09:41,000 --> 00:09:47,440
 we look too often for perfection, we expect too much. And

129
00:09:47,440 --> 00:09:50,000
 it's either all or nothing sometimes.

130
00:09:50,000 --> 00:09:54,030
 You look at your teachers and you think of them as perfect

131
00:09:54,030 --> 00:09:55,000
 and then when you find out their faults,

132
00:09:55,000 --> 00:10:02,000
 you think that they're useless. We do this with people.

133
00:10:03,000 --> 00:10:06,490
 If we're not careful, it's easy to dismiss someone because

134
00:10:06,490 --> 00:10:08,000
 of their faults.

135
00:10:08,000 --> 00:10:11,820
 The Buddha said not to do this. Not in this quote, in

136
00:10:11,820 --> 00:10:15,000
 another quote, he said people are like cloths.

137
00:10:15,000 --> 00:10:20,000
 When you find a, when a monk finds a cloth, they,

138
00:10:20,000 --> 00:10:23,600
 they need the cloth to make other robes. So they find

139
00:10:23,600 --> 00:10:27,000
 pieces of cloth and they stitch them together.

140
00:10:28,000 --> 00:10:31,500
 Sometimes you'll find a piece of cloth and part of it's

141
00:10:31,500 --> 00:10:37,000
 rotten, part of it's decomposing.

142
00:10:37,000 --> 00:10:41,610
 So a part of it is bad. What do you do? Do you keep the

143
00:10:41,610 --> 00:10:43,000
 whole, do you throw the whole cloth out?

144
00:10:43,000 --> 00:10:50,500
 Because of one part? No. You tear off the part that is unus

145
00:10:50,500 --> 00:10:53,000
able and you keep the rest.

146
00:10:55,000 --> 00:10:59,160
 And he said people are like that. Like in people to a, a

147
00:10:59,160 --> 00:11:03,000
 pool of water.

148
00:11:03,000 --> 00:11:06,770
 Suppose you find a pool of water but there's scum on top of

149
00:11:06,770 --> 00:11:07,000
 it.

150
00:11:07,000 --> 00:11:10,420
 And you're very thirsty and you want to drink from the pool

151
00:11:10,420 --> 00:11:12,000
 of water but you can't because there's scum.

152
00:11:12,000 --> 00:11:16,000
 So what you do is you take your hands and you part the scum

153
00:11:16,000 --> 00:11:19,000
 with your hands and then you drink from the pure water.

154
00:11:19,000 --> 00:11:22,000
 And he said people are like that.

155
00:11:24,000 --> 00:11:26,910
 Sometimes their thoughts are pure, sometimes their speech

156
00:11:26,910 --> 00:11:31,000
 is pure, sometimes just part of them is impure.

157
00:11:31,000 --> 00:11:37,460
 And for your own sake and for harmony and for the avoiding

158
00:11:37,460 --> 00:11:44,450
 of conflict and suffering and just this sense of enmity and

159
00:11:44,450 --> 00:11:47,000
 vengeance and so on.

160
00:11:48,000 --> 00:11:54,260
 The hatred, the avoiding of you. Try and find the good in

161
00:11:54,260 --> 00:11:55,000
 people.

162
00:11:55,000 --> 00:12:07,000
 And you focus on that. I mean you at least see it.

163
00:12:07,000 --> 00:12:12,230
 Because it's one thing to think of everyone as perfect or

164
00:12:12,230 --> 00:12:14,000
 anyone as perfect.

165
00:12:14,000 --> 00:12:20,670
 It's pretty much the same thing to think of someone as evil

166
00:12:20,670 --> 00:12:25,710
, as entirely evil to entirely rotten. It's really not

167
00:12:25,710 --> 00:12:26,000
 likely.

168
00:12:26,000 --> 00:12:39,380
 The meditation helps us with this. It helps us see into our

169
00:12:39,380 --> 00:12:40,000
 own minds.

170
00:12:41,000 --> 00:12:44,310
 And you become humble when you see into your own mind

171
00:12:44,310 --> 00:12:48,000
 because you say you stop expecting perfection in others

172
00:12:48,000 --> 00:12:50,000
 because you've realized you don't have it in yourself.

173
00:12:50,000 --> 00:12:56,090
 You know there's a saying that we only hate what we don't

174
00:12:56,090 --> 00:13:01,000
 like about ourselves. I don't think that's really true.

175
00:13:01,000 --> 00:13:04,160
 I think Hermann Hesse was the one who said that and it

176
00:13:04,160 --> 00:13:07,000
 really has always struck me as just wrong.

177
00:13:10,000 --> 00:13:14,040
 I mean I think it's a sentiment is you only hate people

178
00:13:14,040 --> 00:13:17,000
 because you have hate in yourself.

179
00:13:17,000 --> 00:13:22,030
 A person who doesn't have evil in themselves will not hate

180
00:13:22,030 --> 00:13:26,000
 the evil of others because they don't have hate.

181
00:13:26,000 --> 00:13:29,080
 So there's something like that. But I think it's quite

182
00:13:29,080 --> 00:13:32,000
 possible to dislike something that someone else has that

183
00:13:32,000 --> 00:13:33,000
 you don't have.

184
00:13:35,000 --> 00:13:41,270
 I mean if someone is say sexist or prejudiced or racist or

185
00:13:41,270 --> 00:13:44,570
 that kind of thing you don't have to be a racist and

186
00:13:44,570 --> 00:13:47,000
 dislike someone who is racist.

187
00:13:47,000 --> 00:13:54,210
 You don't have to be bigoted to hate bigotry. You just need

188
00:13:54,210 --> 00:13:55,000
 hate.

189
00:13:55,000 --> 00:14:20,000
 But what we can see is that we have in ourselves faults.

190
00:14:21,000 --> 00:14:25,240
 And unless we're faultless we're either faultless or we don

191
00:14:25,240 --> 00:14:29,470
't see our own faults and that's what causes us to criticize

192
00:14:29,470 --> 00:14:30,000
 others.

193
00:14:30,000 --> 00:14:36,520
 Most of the time it's the latter that we don't see our own

194
00:14:36,520 --> 00:14:41,570
 faults and that's why we criticize others. Once you med

195
00:14:41,570 --> 00:14:44,620
itate you start to see your own faults and you're far less

196
00:14:44,620 --> 00:14:47,000
 interested in criticizing others.

197
00:14:47,000 --> 00:14:53,000
 You become humble.

198
00:14:53,000 --> 00:14:57,940
 That's the difference between new meditators and old medit

199
00:14:57,940 --> 00:15:01,970
ators in a big meditation center. The new ones are often

200
00:15:01,970 --> 00:15:05,300
 very critical of everything. The food, the people, the

201
00:15:05,300 --> 00:15:09,000
 other meditators, the monks, the teachers.

202
00:15:10,000 --> 00:15:15,270
 Eventually you start to see 'I've got all those faults in

203
00:15:15,270 --> 00:15:20,050
 me as well' and you really become so in tune with your

204
00:15:20,050 --> 00:15:25,170
 faults that you stop trying to find them or stop obsessing

205
00:15:25,170 --> 00:15:29,990
 about them and others and say 'okay, we've all got faults.

206
00:15:29,990 --> 00:15:33,000
 We're none of us perfect.'

207
00:15:34,000 --> 00:15:40,160
 It's important. It's important for harmony. To admonish

208
00:15:40,160 --> 00:15:47,720
 only. You can admonish I think as a general admonishment

209
00:15:47,720 --> 00:15:53,630
 like 'we should' kind of say 'hey guys we should all try to

210
00:15:53,630 --> 00:15:58,000
 be more, include yourself in the picture.'

211
00:15:59,000 --> 00:16:04,240
 I think sometimes when you admonish others it can be part

212
00:16:04,240 --> 00:16:10,000
 of admonishing yourself. 'We've got to work on this.'

213
00:16:19,000 --> 00:16:25,580
 Anyway, some thoughts on admonishment. I guess that's all

214
00:16:25,580 --> 00:16:29,750
 for tonight. Thank you all for tuning in. Have a good night

215
00:16:29,750 --> 00:16:30,000
.

