1
00:00:00,000 --> 00:00:05,000
 Okay, so continuing our study of the Dhammapada.

2
00:00:05,000 --> 00:00:10,000
 We are at verse number 18.

3
00:00:10,000 --> 00:00:12,000
 Today I've brought out a bigger book.

4
00:00:12,000 --> 00:00:17,000
 This is the commentary on the Dhammapada.

5
00:00:17,000 --> 00:00:21,000
 So it has the same verses, but it also has the stories.

6
00:00:21,000 --> 00:00:25,000
 Because today's story has a bit of a problem to it.

7
00:00:25,000 --> 00:00:28,430
 But it's a very simple story, and the verse itself is very

8
00:00:28,430 --> 00:00:29,000
 much

9
00:00:29,000 --> 00:00:32,000
 the same as the past three verses.

10
00:00:32,000 --> 00:00:33,000
 So this one is,

11
00:00:33,000 --> 00:00:36,000
 ida nandati, pecha nandati,

12
00:00:36,000 --> 00:00:40,000
 kata punyau ubayata nandati,

13
00:00:40,000 --> 00:00:43,000
 punyangmi katanti nandati,

14
00:00:43,000 --> 00:00:50,000
 bhiyo nandati, sukati kathoti.

15
00:00:50,000 --> 00:00:52,000
 Sukati kathoti.

16
00:00:52,000 --> 00:01:02,000
 Here she is blissful, and here after she is blissful,

17
00:01:02,000 --> 00:01:05,000
 or has joy.

18
00:01:05,000 --> 00:01:10,000
 ata punyau, the doer of goodness,

19
00:01:10,000 --> 00:01:13,000
 rejoices in both places,

20
00:01:13,000 --> 00:01:15,000
 blissful in both places.

21
00:01:15,000 --> 00:01:18,000
 unyangmi katanti,

22
00:01:18,000 --> 00:01:22,000
 thinking I have done good, she rejoices.

23
00:01:22,000 --> 00:01:25,000
 biyo nandati, sukati kathoti.

24
00:01:25,000 --> 00:01:29,000
 And even more she rejoices having gone to heaven.

25
00:01:29,000 --> 00:01:31,000
 So I use the feminine here.

26
00:01:31,000 --> 00:01:34,000
 There's no need to use the masculine all the time,

27
00:01:34,000 --> 00:01:36,000
 but specifically here.

