1
00:00:00,000 --> 00:00:04,960
 Hello and welcome back to our study of the Dhammapada.

2
00:00:04,960 --> 00:00:10,400
 Today we continue on with verse number 97 which reads as

3
00:00:10,400 --> 00:00:12,920
 follows.

4
00:00:12,920 --> 00:00:14,430
 Asa do acatanu santhi chay do de onaro hatavakaso wantaso

5
00:00:14,430 --> 00:00:33,440
 sawe uttama pori so.

6
00:00:33,440 --> 00:00:41,600
 One who is faithless, asa do acatanu, one who is ungrateful

7
00:00:41,600 --> 00:00:46,280
, santhi chay do a robber, someone

8
00:00:46,280 --> 00:00:52,050
 who breaks into other people's houses, to onaro, whatever

9
00:00:52,050 --> 00:00:53,760
 man or person.

10
00:00:53,760 --> 00:01:00,350
 Hatavakaso has destroyed opportunity, has cut off, has

11
00:01:00,350 --> 00:01:07,040
 opportunity cut off, wantaso is hopeless.

12
00:01:07,040 --> 00:01:09,520
 Sathwe uttama pori so.

13
00:01:09,520 --> 00:01:13,320
 Such a person is the height of humanity.

14
00:01:13,320 --> 00:01:20,960
 So a rather odd verse coming from the Buddha but you have

15
00:01:20,960 --> 00:01:25,200
 to understand the context and

16
00:01:25,200 --> 00:01:29,800
 then we'll explain that on face value it means one thing

17
00:01:29,800 --> 00:01:33,560
 but when you understand the context

18
00:01:33,560 --> 00:01:36,770
 and when you think about when a person looks at the Pali

19
00:01:36,770 --> 00:01:38,760
 words it means something quite

20
00:01:38,760 --> 00:01:40,980
 different.

21
00:01:40,980 --> 00:01:46,160
 So the story is again about Sariputta and it's in regards

22
00:01:46,160 --> 00:01:48,520
 to thirty monks who had been

23
00:01:48,520 --> 00:01:54,550
 dwelling in the forest intently practicing meditation and

24
00:01:54,550 --> 00:01:57,520
 the Buddha saw that there was

25
00:01:57,520 --> 00:01:59,720
 somehow he saw that they needed something.

26
00:01:59,720 --> 00:02:07,280
 They needed perhaps to move away from faith one might think

27
00:02:07,280 --> 00:02:10,160
 based on the story.

28
00:02:10,160 --> 00:02:12,440
 The Buddha anyway saw that they needed some push and if

29
00:02:12,440 --> 00:02:13,680
 they were given a push in the

30
00:02:13,680 --> 00:02:18,090
 right direction it might put them back on the right path

31
00:02:18,090 --> 00:02:21,080
 and lead them to become enlightened.

32
00:02:21,080 --> 00:02:29,330
 He saw potential in them and so he used Sariputta as his

33
00:02:29,330 --> 00:02:35,680
 sort of his catalyst by asking Sariputta

34
00:02:35,680 --> 00:02:38,160
 a question when he when they were in his presence.

35
00:02:38,160 --> 00:02:40,440
 So these monks came to the Buddha, they were back.

36
00:02:40,440 --> 00:02:43,590
 The Buddha decided what he was how he was going to teach

37
00:02:43,590 --> 00:02:44,120
 them.

38
00:02:44,120 --> 00:02:49,650
 He turned to Sariputta and he says Sariputta do you have

39
00:02:49,650 --> 00:02:54,800
 faith sad sadhahasi do you do

40
00:02:54,800 --> 00:03:02,630
 you believe basically do you believe that the faculty of

41
00:03:02,630 --> 00:03:05,740
 faith when cultivated when

42
00:03:05,740 --> 00:03:15,700
 made much of leads to the deathless and ends in the death

43
00:03:15,700 --> 00:03:15,800
less.

44
00:03:15,800 --> 00:03:18,210
 And asked about all five of the faculties it's not just the

45
00:03:18,210 --> 00:03:19,280
 faith faculty but there

46
00:03:19,280 --> 00:03:22,720
 are five faculties there's sadhah which is faith or

47
00:03:22,720 --> 00:03:26,080
 confidence, vidya which is effort,

48
00:03:26,080 --> 00:03:34,140
 sati which is mindfulness, samadhi concentration or focus

49
00:03:34,140 --> 00:03:38,360
 and bhanyah which is wisdom.

50
00:03:38,360 --> 00:03:42,320
 So he asked about these five faculties but he asked do you

51
00:03:42,320 --> 00:03:44,320
 believe and Sariputta gave

52
00:03:44,320 --> 00:03:51,790
 a sort of a nuanced reply he didn't just say yes or no he

53
00:03:51,790 --> 00:03:55,400
 said it's not out of faith.

54
00:03:55,400 --> 00:04:03,680
 He said nakkwa hang bante ita bhagavato sadhah yagacam y I

55
00:04:03,680 --> 00:04:06,600
 don't get to that out of faith

56
00:04:06,600 --> 00:04:12,360
 in the blessed one.

57
00:04:12,360 --> 00:04:15,340
 As we're in regards to the faith faculty being when

58
00:04:15,340 --> 00:04:17,760
 cultivated leading to nirvana and same

59
00:04:17,760 --> 00:04:21,260
 with the effort faculty and the mindfulness faculty and the

60
00:04:21,260 --> 00:04:22,880
 concentration faculty and

61
00:04:22,880 --> 00:04:26,760
 the wisdom faculty.

62
00:04:26,760 --> 00:04:32,480
 So basically he says I don't go to faith in that regard,

63
00:04:32,480 --> 00:04:34,800
 faith in the blessed one in that

64
00:04:34,800 --> 00:04:37,670
 regard that's a Pali way of saying I don't have faith in

65
00:04:37,670 --> 00:04:39,280
 the blessed one in that regard

66
00:04:39,280 --> 00:04:44,940
 or it can be interpreted to mean that.

67
00:04:44,940 --> 00:04:48,640
 And he said but someone who has not seen the truth who has

68
00:04:48,640 --> 00:04:50,560
 not come to realize the truth

69
00:04:50,560 --> 00:05:00,350
 for themselves such a person would have faith in the

70
00:05:00,350 --> 00:05:04,280
 blessed one thus.

71
00:05:04,280 --> 00:05:09,200
 They would have faith in someone else thus or would get to

72
00:05:09,200 --> 00:05:11,920
 this by faith in someone else.

73
00:05:11,920 --> 00:05:15,400
 So I don't go to faith in the blessed one I don't get to

74
00:05:15,400 --> 00:05:17,560
 that through faith in the blessed

75
00:05:17,560 --> 00:05:19,360
 one.

76
00:05:19,360 --> 00:05:25,220
 And as Buddhists we tend to understand what he meant like

77
00:05:25,220 --> 00:05:28,060
 it's not out of faith because

78
00:05:28,060 --> 00:05:32,640
 he says for someone who has not seen, not understood for

79
00:05:32,640 --> 00:05:34,440
 themselves they need to have

80
00:05:34,440 --> 00:05:35,440
 faith.

81
00:05:35,440 --> 00:05:38,530
 So it's actually pretty clear but the Pali somehow makes it

82
00:05:38,530 --> 00:05:40,080
 ambiguous and these thirty

83
00:05:40,080 --> 00:05:45,130
 monks again with the criticism start talking amongst

84
00:05:45,130 --> 00:05:48,720
 themselves and saying even Sariputta

85
00:05:48,720 --> 00:05:52,080
 it's amazing he hasn't given up his wrong views he's still

86
00:05:52,080 --> 00:05:53,000
 faithless.

87
00:05:53,000 --> 00:05:57,700
 He has no faith in the Buddha he still after all this time

88
00:05:57,700 --> 00:06:00,120
 doesn't have this faith in the

89
00:06:00,120 --> 00:06:01,120
 Buddha.

90
00:06:01,120 --> 00:06:05,550
 It's a valid criticism I mean it would be if they hadn't

91
00:06:05,550 --> 00:06:07,720
 misunderstood what he was

92
00:06:07,720 --> 00:06:13,510
 saying if he had actually meant that he just had no faith

93
00:06:13,510 --> 00:06:16,600
 in the Buddha whatsoever.

94
00:06:16,600 --> 00:06:19,470
 But of course the Buddha hears about what they're saying

95
00:06:19,470 --> 00:06:20,880
 and says monks what are you

96
00:06:20,880 --> 00:06:23,520
 talking about?

97
00:06:23,520 --> 00:06:29,650
 When they tell him he explains to them he says but Sariput

98
00:06:29,650 --> 00:06:32,400
ta when I asked him he answered

99
00:06:32,400 --> 00:06:36,460
 me quite clearly and what he meant was that it's not out of

100
00:06:36,460 --> 00:06:39,520
 faith it's through understanding

101
00:06:39,520 --> 00:06:43,940
 and because he has seen the truth through the practice of

102
00:06:43,940 --> 00:06:46,360
 Samatha meditation, Vipassana

103
00:06:46,360 --> 00:06:50,600
 meditation has realized the path and fruition.

104
00:06:50,600 --> 00:06:58,050
 Sadaya nagacchati doesn't require faith it doesn't get

105
00:06:58,050 --> 00:07:00,680
 there through faith in another

106
00:07:00,680 --> 00:07:06,300
 he gets there through his own knowledge and then he said

107
00:07:06,300 --> 00:07:09,160
 this verse and so what's interesting

108
00:07:09,160 --> 00:07:12,720
 about this verse is it confuses them even more right he

109
00:07:12,720 --> 00:07:14,760
 says someone who is faithless

110
00:07:14,760 --> 00:07:18,720
 is the best is the height and the meaning is they don't

111
00:07:18,720 --> 00:07:20,920
 they don't have faith they don't

112
00:07:20,920 --> 00:07:25,780
 require faith to know the truth such a person who has seen

113
00:07:25,780 --> 00:07:28,360
 the truth for themselves and

114
00:07:28,360 --> 00:07:32,710
 no longer requires no longer has to believe right so you

115
00:07:32,710 --> 00:07:34,960
 tell them the truth and they

116
00:07:34,960 --> 00:07:39,340
 can verify or deny it they can verify the truth without

117
00:07:39,340 --> 00:07:41,800
 requiring belief you say it's

118
00:07:41,800 --> 00:07:45,200
 like this they don't have to believe you they know it is

119
00:07:45,200 --> 00:07:47,360
 the truth it's better obviously

120
00:07:47,360 --> 00:07:52,590
 Akatanu and then he goes on to confuse them more or play

121
00:07:52,590 --> 00:07:55,520
 with them more he says Akatanu

122
00:07:55,520 --> 00:08:01,300
 Akatanu literally means one who he literally means one of

123
00:08:01,300 --> 00:08:04,400
 two things but Akata means not

124
00:08:04,400 --> 00:08:13,110
 means done and Anu Anya u means one who knows so it usually

125
00:08:13,110 --> 00:08:14,960
 means one who knows not what

126
00:08:14,960 --> 00:08:19,270
 was done in the sense of knowing not what someone else has

127
00:08:19,270 --> 00:08:21,200
 done for you so if I do you

128
00:08:21,200 --> 00:08:25,270
 a favor and you don't keep that in mind you aren't you aren

129
00:08:25,270 --> 00:08:27,160
't conscious of the fact that

130
00:08:27,160 --> 00:08:30,560
 something's been done for you it's a way of saying you're

131
00:08:30,560 --> 00:08:32,440
 ungrateful Akatanu is a common

132
00:08:32,440 --> 00:08:35,980
 word that means it's a compound that means someone who is

133
00:08:35,980 --> 00:08:37,920
 ungrateful for the things other

134
00:08:37,920 --> 00:08:42,330
 people have done for them it can also and the Buddha's

135
00:08:42,330 --> 00:08:44,200
 using it here in a different

136
00:08:44,200 --> 00:08:50,170
 way it means one who knows that which is not done Akata Anu

137
00:08:50,170 --> 00:08:52,920
 Akata means what is not done

138
00:08:52,920 --> 00:08:56,800
 or what is not made the unmade and the unmade here is Nibb

139
00:08:56,800 --> 00:08:59,040
ana so it's a play on words that

140
00:08:59,040 --> 00:09:06,760
 is of course lost in the English but Akatanu means one who

141
00:09:06,760 --> 00:09:09,880
 knows Nibbana. Sandi Ceda literally

142
00:09:09,880 --> 00:09:15,650
 means one who has cut the chain Sandi means the connection

143
00:09:15,650 --> 00:09:17,800
 or the chain and it is a word

144
00:09:17,800 --> 00:09:20,890
 that it is a compound that was used to mean one who breaks

145
00:09:20,890 --> 00:09:22,720
 into people's houses cutting

146
00:09:22,720 --> 00:09:29,730
 through the locks the lock on the door but here is someone

147
00:09:29,730 --> 00:09:32,560
 who has picked the lock on

148
00:09:32,560 --> 00:09:36,320
 samsara and unlocked or broken the chain it's more correct

149
00:09:36,320 --> 00:09:38,480
 broken the chain of rebirth so

150
00:09:38,480 --> 00:09:43,970
 being born all sick and dying is a chain and so one who has

151
00:09:43,970 --> 00:09:46,440
 who has broken that it's one

152
00:09:46,440 --> 00:09:52,050
 who is free from samsara. Hata bokaso one who has destroyed

153
00:09:52,050 --> 00:09:55,200
 opportunity or has had opportunity

154
00:09:55,200 --> 00:09:59,780
 cut off means one who is who has no opportunities for them

155
00:09:59,780 --> 00:10:02,720
 that's what it normally would mean

156
00:10:02,720 --> 00:10:06,330
 but here it's a specific meaning again it's a play on words

157
00:10:06,330 --> 00:10:08,000
 but it means for one one for

158
00:10:08,000 --> 00:10:12,090
 whom there is no opportunity for further becoming so

159
00:10:12,090 --> 00:10:15,240
 opportunity is in a sense a another word

160
00:10:15,240 --> 00:10:19,160
 for becoming this or becoming that you know they don't they

161
00:10:19,160 --> 00:10:21,560
 don't have ambitions or plans

162
00:10:21,560 --> 00:10:26,140
 because they don't think about the future and finally one t

163
00:10:26,140 --> 00:10:28,760
aso means hopeless and hopeless

164
00:10:28,760 --> 00:10:32,900
 is of course a very good thing it's an excellent thing to

165
00:10:32,900 --> 00:10:35,480
 be hopeless means you have no hope

166
00:10:35,480 --> 00:10:39,010
 you don't hope for anything because you have no

167
00:10:39,010 --> 00:10:42,260
 expectations no expectations no desires

168
00:10:42,260 --> 00:10:45,240
 you are living in the future again you're living only in

169
00:10:45,240 --> 00:10:47,080
 the present perfectly flexible

170
00:10:47,080 --> 00:10:52,680
 and content no matter what happens no matter what comes or

171
00:10:52,680 --> 00:10:54,480
 doesn't come so this is the

172
00:10:54,480 --> 00:10:59,760
 meaning of Buddha said that way uttamapuri so such a one is

173
00:10:59,760 --> 00:11:02,560
 indeed the height of humanity

174
00:11:02,560 --> 00:11:07,150
 so what does this mean for our practice well first before

175
00:11:07,150 --> 00:11:09,880
 we actually get into the context

176
00:11:09,880 --> 00:11:12,680
 in the story the Buddha talks about the five faculties

177
00:11:12,680 --> 00:11:14,160
 which in and of themselves are a

178
00:11:14,160 --> 00:11:17,220
 really good teaching this is a teaching that has been taken

179
00:11:17,220 --> 00:11:18,440
 from the sanyuta nikaya and

180
00:11:18,440 --> 00:11:23,040
 it's an important teaching we have the concept of these

181
00:11:23,040 --> 00:11:25,960
 five faculties and talk about them

182
00:11:25,960 --> 00:11:30,170
 in the meditation practice you have to balance confidence

183
00:11:30,170 --> 00:11:32,480
 with wisdom and cultivate them

184
00:11:32,480 --> 00:11:35,910
 both but in tandem so if you have too much confidence not

185
00:11:35,910 --> 00:11:37,600
 enough wisdom that's a bad

186
00:11:37,600 --> 00:11:40,240
 thing you have a lot of wisdom but you don't have any

187
00:11:40,240 --> 00:11:42,040
 confidence in it so you know a lot

188
00:11:42,040 --> 00:11:45,970
 of things not really wisdom and but without the confidence

189
00:11:45,970 --> 00:11:47,880
 you're always doubting the

190
00:11:47,880 --> 00:11:50,460
 things that you learn and the things that you come to

191
00:11:50,460 --> 00:11:52,240
 understand do I really did I really

192
00:11:52,240 --> 00:11:56,700
 see that is this really useful effort and concentration

193
00:11:56,700 --> 00:11:58,880
 also have to be balanced so

194
00:11:58,880 --> 00:12:01,350
 if you have a lot of effort you'll become distracted

195
00:12:01,350 --> 00:12:03,160
 without enough focus if the effort

196
00:12:03,160 --> 00:12:06,660
 isn't focused enough if you have a lot of focus but not the

197
00:12:06,660 --> 00:12:08,080
 effort you'll fall into

198
00:12:08,080 --> 00:12:12,910
 a stupor and you'll become tired and fall asleep so these

199
00:12:12,910 --> 00:12:15,160
 two have to be balanced as

200
00:12:15,160 --> 00:12:18,850
 well and they both have to be cultivated now sati sati is

201
00:12:18,850 --> 00:12:20,960
 the ability to see things as they

202
00:12:20,960 --> 00:12:26,910
 are basically to recognize reality sati means to recognize

203
00:12:26,910 --> 00:12:30,560
 or to remember to remember yourself

204
00:12:30,560 --> 00:12:34,960
 I mean to remind yourself the ability to see things and to

205
00:12:34,960 --> 00:12:37,280
 recognize things as they are

206
00:12:37,280 --> 00:12:42,170
 without any kind of extrapolation not seeing things as good

207
00:12:42,170 --> 00:12:44,600
 or bad or so on so mindfulness

208
00:12:44,600 --> 00:12:49,590
 is actually what balances the other faculties because you

209
00:12:49,590 --> 00:12:52,520
 don't get off in one way or another

210
00:12:52,520 --> 00:12:55,570
 you don't get caught up in too much energy too much

211
00:12:55,570 --> 00:12:57,820
 concentration you become natural

212
00:12:57,820 --> 00:13:02,720
 you just let things sort themselves out without forcing

213
00:13:02,720 --> 00:13:06,120
 anything so absolutely these five

214
00:13:06,120 --> 00:13:09,990
 when when they work together and when you use mindfulness

215
00:13:09,990 --> 00:13:11,800
 to balance the other four

216
00:13:11,800 --> 00:13:15,290
 are definitely the path to the deathless and by the death

217
00:13:15,290 --> 00:13:17,320
less is meant this you know this

218
00:13:17,320 --> 00:13:20,650
 freedom from the rounds of rebirth freedom from being born

219
00:13:20,650 --> 00:13:22,120
 again the only way to not

220
00:13:22,120 --> 00:13:25,570
 die is to not be born and so if we're born again and again

221
00:13:25,570 --> 00:13:27,440
 and again we'll always have

222
00:13:27,440 --> 00:13:33,420
 to die again and again and again so deathless just means

223
00:13:33,420 --> 00:13:36,840
 that state of freedom from that

224
00:13:36,840 --> 00:13:40,230
 that's the first lesson second lesson of course again this

225
00:13:40,230 --> 00:13:42,400
 lesson criticizing others can be

226
00:13:42,400 --> 00:13:46,760
 a dangerous thing to do if you're not clear but the other

227
00:13:46,760 --> 00:13:49,160
 hand these thirty monks were

228
00:13:49,160 --> 00:13:53,070
 provided the perfect opportunity for the Buddha to teach

229
00:13:53,070 --> 00:13:55,720
 the Dhamma so there is that something

230
00:13:55,720 --> 00:13:58,580
 when something's uncertain unclear and when something

231
00:13:58,580 --> 00:14:00,400
 appears to be wrong it is important

232
00:14:00,400 --> 00:14:03,250
 not to keep quiet and think oh well I don't want to stir up

233
00:14:03,250 --> 00:14:04,720
 the boat it is important to

234
00:14:04,720 --> 00:14:12,180
 bring it up and ask why why is it like that and why does it

235
00:14:12,180 --> 00:14:16,760
 seem like this monk is faithless

236
00:14:16,760 --> 00:14:20,790
 and then when we get into the actual verse we have some

237
00:14:20,790 --> 00:14:23,280
 important things to say I think

238
00:14:23,280 --> 00:14:27,480
 asa do is really an important point it's a difference

239
00:14:27,480 --> 00:14:30,480
 between Buddhism and other religions

240
00:14:30,480 --> 00:14:34,280
 it doesn't mean that you shouldn't believe anything it just

241
00:14:34,280 --> 00:14:36,080
 means that belief believing

242
00:14:36,080 --> 00:14:38,990
 as something is inferior to knowing it for yourself and

243
00:14:38,990 --> 00:14:40,600
 Buddhism will always hold it

244
00:14:40,600 --> 00:14:43,660
 to be so whereas other religions believe that faith is

245
00:14:43,660 --> 00:14:46,280
 ultimate they believe there are certain

246
00:14:46,280 --> 00:14:50,120
 things that you can't know that it is somehow important to

247
00:14:50,120 --> 00:14:52,040
 have faith in and so they set

248
00:14:52,040 --> 00:14:56,090
 up this system whereby you cultivate faith and you have to

249
00:14:56,090 --> 00:14:57,920
 work hard to cultivate it

250
00:14:57,920 --> 00:15:01,970
 because faith is not something that is stable not like

251
00:15:01,970 --> 00:15:04,600
 wisdom or not like mindfulness not

252
00:15:04,600 --> 00:15:09,230
 like awareness faith is something well not like wisdom

253
00:15:09,230 --> 00:15:12,240
 actually because wisdom is stable

254
00:15:12,240 --> 00:15:18,550
 when you come to see things clearly you you don't have to

255
00:15:18,550 --> 00:15:21,440
 work to maintain it well it's

256
00:15:21,440 --> 00:15:26,100
 something that is stronger whereas with faith you have to

257
00:15:26,100 --> 00:15:28,320
 push and push and you have to

258
00:15:28,320 --> 00:15:36,840
 you have to always be repressing your doubts you could say

259
00:15:36,840 --> 00:15:40,960
 faith that comes with wisdom

260
00:15:40,960 --> 00:15:44,060
 when they're both balanced the faith that comes with wisdom

261
00:15:44,060 --> 00:15:45,440
 because in fact you could

262
00:15:45,440 --> 00:15:49,580
 also say that Sariputta has perfect confidence in the truth

263
00:15:49,580 --> 00:15:51,920
 you know his perfect confidence

264
00:15:51,920 --> 00:15:54,500
 in the things that the Buddha was asking him he said the

265
00:15:54,500 --> 00:15:56,120
 answer is yes and he has perfect

266
00:15:56,120 --> 00:16:01,100
 confidence in that but it's faith that's based on wisdom

267
00:16:01,100 --> 00:16:03,960
 based on knowledge and so again

268
00:16:03,960 --> 00:16:08,170
 how you have to how you have to balance them but the Buddha

269
00:16:08,170 --> 00:16:10,440
 was fairly critical about the

270
00:16:10,440 --> 00:16:13,800
 idea of people who just take things on faith which it was a

271
00:16:13,800 --> 00:16:15,480
 big thing in the Buddhist time

272
00:16:15,480 --> 00:16:19,770
 as well the Brahmins were big on faith they believed that

273
00:16:19,770 --> 00:16:21,760
 their rituals had some higher

274
00:16:21,760 --> 00:16:26,080
 meaning to them without any rational explanation or they

275
00:16:26,080 --> 00:16:28,560
 believed in this God or that God without

276
00:16:28,560 --> 00:16:31,680
 having seen or heard they believed in Brahma so they

277
00:16:31,680 --> 00:16:34,200
 created all these beliefs and then they just

278
00:16:34,200 --> 00:16:37,270
 had faith in them they didn't have any knowledge or

279
00:16:37,270 --> 00:16:40,080
 understanding about their truth or falsehood

280
00:16:40,080 --> 00:16:45,010
 so in meditation this is our this should be our emphasis

281
00:16:45,010 --> 00:16:47,480
 not on believing that somehow this

282
00:16:47,480 --> 00:16:50,400
 meditation is going to help us but in using the meditation

283
00:16:50,400 --> 00:16:53,000
 to learn about ourselves so that we

284
00:16:53,000 --> 00:16:54,990
 don't need faith we don't need to believe anyone else that

285
00:16:54,990 --> 00:16:57,280
's really what we're doing you want I

286
00:16:57,280 --> 00:17:00,350
 can tell you all about the mind how the mind works and how

287
00:17:00,350 --> 00:17:02,080
 what reality is like I can give

288
00:17:02,080 --> 00:17:07,040
 you lecture after lecture but that's always going to be

289
00:17:07,040 --> 00:17:10,800
 inferior to you actually opening up the box

290
00:17:10,800 --> 00:17:13,960
 and looking inside which is what you do in meditation you

291
00:17:13,960 --> 00:17:15,640
 start you open up and you look

292
00:17:15,640 --> 00:17:20,350
 and you see how our things going on the the technique that

293
00:17:20,350 --> 00:17:22,440
 we give you is just to provide

294
00:17:22,440 --> 00:17:26,320
 you with a framework like a tool in which to look like when

295
00:17:26,320 --> 00:17:29,560
 you peer into a microscope and give you

296
00:17:29,560 --> 00:17:32,180
 this and you look in the microscope and whatever is there

297
00:17:32,180 --> 00:17:33,920
 you'll see so watching your stomach

298
00:17:33,920 --> 00:17:38,250
 rising and falling as you watch it and you use the mantra

299
00:17:38,250 --> 00:17:41,760
 to keep yourself objective you'll start to

300
00:17:41,760 --> 00:17:44,490
 see things you'll be in an objective frame of mind like you

301
00:17:44,490 --> 00:17:47,560
're focusing the microscope and once you

302
00:17:47,560 --> 00:17:50,020
 get it well focused you'll be able to see things as they

303
00:17:50,020 --> 00:17:52,040
 are and then you won't need to believe me

304
00:17:52,040 --> 00:17:58,530
 Akatanyu is about what this is all about the height the

305
00:17:58,530 --> 00:18:02,440
 pinnacle is to see that which is not made or

306
00:18:02,440 --> 00:18:06,330
 to know that which is not made and point being that as you

307
00:18:06,330 --> 00:18:08,320
 look in this microscope through the

308
00:18:08,320 --> 00:18:11,900
 meditation what you're going to see is that everything that

309
00:18:11,900 --> 00:18:13,760
 arises ceases and you come to

310
00:18:13,760 --> 00:18:17,760
 the realization or the understanding that there's nothing

311
00:18:17,760 --> 00:18:20,280
 in the world that is a truly and intrinsically

312
00:18:20,280 --> 00:18:23,300
 of value there's no experience that you can have no goal

313
00:18:23,300 --> 00:18:26,440
 you can attain in the world because everything

314
00:18:26,440 --> 00:18:31,110
 is is made by causes and conditions it arises and it ceases

315
00:18:31,110 --> 00:18:35,160
 and it's a part of a web of causation now

316
00:18:35,160 --> 00:18:39,180
 nirvana or this there is there is that which is outside of

317
00:18:39,180 --> 00:18:41,240
 this cause of causation it doesn't

318
00:18:41,240 --> 00:18:45,070
 arise and therefore it doesn't cease and this so this is

319
00:18:45,070 --> 00:18:47,800
 considered to be the height when someone

320
00:18:47,800 --> 00:18:51,910
 realizes this it's unique they have this experience of some

321
00:18:51,910 --> 00:18:54,520
 unique state or unique entity or unique

322
00:18:54,520 --> 00:19:01,150
 reality that is unmade that is unformed unborn so that is

323
00:19:01,150 --> 00:19:05,520
 the height it's something that changes

324
00:19:05,520 --> 00:19:08,780
 the way we look at reality because in contrast everything

325
00:19:08,780 --> 00:19:11,280
 else becomes meaningless and when one

326
00:19:11,280 --> 00:19:16,220
 who has realized this or seen this they'll never be they'll

327
00:19:16,220 --> 00:19:19,320
 never be enticed by or intoxicated by

328
00:19:19,320 --> 00:19:23,400
 those things that arise and cease because they're clearly

329
00:19:23,400 --> 00:19:24,800
 categorically inferior

330
00:19:24,800 --> 00:19:31,450
 Santi cheda means cutting the link so another great thing

331
00:19:31,450 --> 00:19:34,200
 is one doesn't have to be born old sick

332
00:19:34,200 --> 00:19:37,990
 and die again and people sometimes most humans are fairly

333
00:19:37,990 --> 00:19:40,960
 much attached to life and you ask them

334
00:19:40,960 --> 00:19:43,460
 would you like to come back and do this all again and again

335
00:19:43,460 --> 00:19:45,520
 many of them would I think say yes please

336
00:19:45,520 --> 00:19:48,230
 it would give me an opportunity to learn more and to

337
00:19:48,230 --> 00:19:50,720
 experience things I didn't experience in this

338
00:19:50,720 --> 00:19:56,580
 life it'd be great to come back forgetting as we do this is

339
00:19:56,580 --> 00:19:59,920
 we have this confirmation bias or

340
00:19:59,920 --> 00:20:04,110
 positive bias so we were very quick to forget about all the

341
00:20:04,110 --> 00:20:06,880
 problems in life and moreover when when

342
00:20:06,880 --> 00:20:09,780
 you think it through what you're talking about is an

343
00:20:09,780 --> 00:20:12,400
 infinite recursion right you're talking about

344
00:20:12,400 --> 00:20:16,250
 this infinite infinite cycle and so it's that that is

345
00:20:16,250 --> 00:20:19,360
 fairly problematic because it it really

346
00:20:19,360 --> 00:20:22,690
 becomes meaningless after a while and it ends up being not

347
00:20:22,690 --> 00:20:24,800
 nearly as much trouble as it's worth

348
00:20:24,800 --> 00:20:28,420
 being a human being is a lot of trouble and clearly we've

349
00:20:28,420 --> 00:20:30,720
 in all the time we've spent in the rounds

350
00:20:30,720 --> 00:20:33,940
 of rebirths we keep coming back to things like this we go

351
00:20:33,940 --> 00:20:36,240
 up and down and so sometimes it's great and

352
00:20:36,240 --> 00:20:40,230
 sometimes it's horrible sometimes it's mind-numbingly

353
00:20:40,230 --> 00:20:44,160
 painful and and cruel and and unpleasant

354
00:20:44,160 --> 00:20:49,280
 sometimes it's pleasant incredibly uh stupefyingly pleasant

355
00:20:49,280 --> 00:20:52,960
 but in the end it's just a round of

356
00:20:52,960 --> 00:20:56,190
 rebirth again and again and in the end it's such a

357
00:20:56,190 --> 00:20:59,600
 completely unsatisfying it's not something that

358
00:20:59,600 --> 00:21:02,960
 you intellectually or or let go out of faith you don't have

359
00:21:02,960 --> 00:21:04,720
 to believe me the point is that when

360
00:21:04,720 --> 00:21:07,880
 you look closely you naturally incline away from it so

361
00:21:07,880 --> 00:21:10,240
 through the meditation you become less

362
00:21:10,240 --> 00:21:14,090
 inclined to create less inclined to chase after this

363
00:21:14,090 --> 00:21:17,040
 pleasure because it's a lot of work and then

364
00:21:17,040 --> 00:21:19,920
 you have it and then it disappears and you're left with

365
00:21:19,920 --> 00:21:22,160
 nothing except your your attachment to it

366
00:21:22,160 --> 00:21:26,360
 which then leads you to lots of suffering so cutting the

367
00:21:26,360 --> 00:21:29,600
 stream this is the cutting the chain

368
00:21:29,600 --> 00:21:33,620
 this is the part of the goal and it's just it this again

369
00:21:33,620 --> 00:21:35,920
 with the theme it just goes back to

370
00:21:35,920 --> 00:21:38,810
 knowledge not belief it's not something to be afraid of you

371
00:21:38,810 --> 00:21:40,000
 can't just fall into it

372
00:21:40,000 --> 00:21:43,520
 most people i think are afraid of the idea of nibban or

373
00:21:43,520 --> 00:21:44,400
 some kind of

374
00:21:44,400 --> 00:21:53,250
 unknowable mysterious state of so-called freedom but when

375
00:21:53,250 --> 00:21:55,280
 you've experienced it there's no question

376
00:21:55,280 --> 00:22:00,460
 in the mind which is preferable mind lets go immediately of

377
00:22:00,460 --> 00:22:02,480
 those things that are

378
00:22:02,480 --> 00:22:04,880
 completely unbeneficial and unsatisfying

379
00:22:04,880 --> 00:22:12,040
 hata waka so means they've given up the opportunity for

380
00:22:12,040 --> 00:22:14,000
 becoming and this has to do with giving up

381
00:22:14,000 --> 00:22:18,570
 defilements it means giving up all kinds of unwholesomeness

382
00:22:18,570 --> 00:22:20,960
 but also any kind of wholesomeness

383
00:22:20,960 --> 00:22:24,070
 so giving up the desire to even do good in the world to

384
00:22:24,070 --> 00:22:26,560
 bring the world to good to save the world

385
00:22:26,560 --> 00:22:30,000
 to change the world all of these things one eventually

386
00:22:30,000 --> 00:22:31,760
 gives up and that's considered

387
00:22:31,760 --> 00:22:34,610
 preferable because in the end you can't change the world

388
00:22:34,610 --> 00:22:35,840
 you can't fix the world

389
00:22:35,840 --> 00:22:39,390
 in the end everything that you've done in the world will

390
00:22:39,390 --> 00:22:40,720
 cease and fade away

391
00:22:40,720 --> 00:22:45,250
 over time in the face of eternity it's really nothing you

392
00:22:45,250 --> 00:22:46,800
 want to save the planet save the

393
00:22:46,800 --> 00:22:51,440
 environment to some extent that's a good deed it's kind of

394
00:22:51,440 --> 00:22:53,840
 you to think about other people and to

395
00:22:53,840 --> 00:22:56,660
 think about society and want everyone to be happy so it's

396
00:22:56,660 --> 00:22:58,560
 good and for most of us it's a great thing

397
00:22:58,560 --> 00:23:02,210
 for someone who becomes enlightened they give up any idea

398
00:23:02,210 --> 00:23:05,600
 of fixing things because in the end it's

399
00:23:05,600 --> 00:23:09,030
 inherently broken of course along the way they do a lot of

400
00:23:09,030 --> 00:23:11,360
 good and by becoming enlightened they

401
00:23:11,360 --> 00:23:14,540
 also do a lot of good and end up making the world a better

402
00:23:14,540 --> 00:23:16,880
 place for everyone because they give up

403
00:23:16,880 --> 00:23:20,090
 their own greed their own attachments their own conflicts

404
00:23:20,090 --> 00:23:22,160
 and anger and their own arrogance and

405
00:23:22,160 --> 00:23:26,120
 conceit and so on so there's no there's there's really not

406
00:23:26,120 --> 00:23:29,280
 nothing to criticize about it but they

407
00:23:29,280 --> 00:23:33,330
 certainly give up everything they have no opportunity for

408
00:23:33,330 --> 00:23:36,000
 for this future becoming no more ambition

409
00:23:38,560 --> 00:23:42,280
 and one does so they're hopeless so our practice is to give

410
00:23:42,280 --> 00:23:44,560
 up our hopes because if you hope it

411
00:23:44,560 --> 00:23:48,210
 means you still have a goal and when someone becomes free

412
00:23:48,210 --> 00:23:50,400
 from suffering when they attain the

413
00:23:50,400 --> 00:23:54,820
 ultimate goal then there's no other reason for them to hope

414
00:23:54,820 --> 00:23:58,160
 or wish they say that hopes and wishes

415
00:23:58,160 --> 00:24:01,310
 are actually the cause of suffering because even if you get

416
00:24:01,310 --> 00:24:03,200
 what you want get what you wish for

417
00:24:04,000 --> 00:24:07,140
 you'll still you'll at the same time be giving rise to

418
00:24:07,140 --> 00:24:09,840
 attachment to it and then you build up

419
00:24:09,840 --> 00:24:14,280
 and build up the attachment until eventually that goal is

420
00:24:14,280 --> 00:24:17,920
 attained and and lost over the over time

421
00:24:17,920 --> 00:24:22,030
 at which point all you're left with is the attachment which

422
00:24:22,030 --> 00:24:23,760
 again leads to suffering

423
00:24:25,840 --> 00:24:30,730
 so this is an explanation of that verse and sort of a

424
00:24:30,730 --> 00:24:34,720
 outline of the sorts of qualities

425
00:24:34,720 --> 00:24:38,090
 of an enlightened being for our own practice we emulate

426
00:24:38,090 --> 00:24:40,080
 some of these and we cultivate some of

427
00:24:40,080 --> 00:24:44,080
 these and we keep some of them in mind philosophically to

428
00:24:44,080 --> 00:24:46,240
 help us know how to direct our minds in the

429
00:24:46,240 --> 00:24:50,690
 meditation practice so another useful if a bit i would say

430
00:24:50,690 --> 00:24:53,040
 tongue in cheek and i don't really want

431
00:24:53,040 --> 00:24:56,350
 to implicate any implicate the buddha in any way but it's

432
00:24:56,350 --> 00:24:58,320
 it's definitely a clever verse

433
00:24:58,320 --> 00:25:05,360
 meant perhaps it seems to to shock and to throw off guard

434
00:25:05,360 --> 00:25:08,080
 the listener so

435
00:25:08,080 --> 00:25:11,110
 that's the verse for tonight thank you all for tuning in

436
00:25:11,110 --> 00:25:14,000
 wishing you all the best be well

