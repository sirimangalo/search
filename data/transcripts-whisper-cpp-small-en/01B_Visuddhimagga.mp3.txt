 Not yet come to virtue itself. We are an introductory part
 of the discussion. I think next time
 it will be better when you have read the passages. Today it
's just listening to something you are
 not familiar with. So you may be lost in some places. So
 this is just the introductory talk
 before the other really explains what virtue is and then
 what is the meaning of the word
 what is virtue and so on. So he proceeded in this order.
 First he put questions and
 then he gives answers to these questions one by one. So the
 first question is what is virtue?
 What is seela? And the answer is it is the states beginning
 with volition present in
 one who abstains from killing living things etc. So when we
 abstain from killing living things or
 abstain from stealing and so on, a consciousness arises in
 our minds. What kind of consciousness?
 Wholesome or unwholesome? When we abstain from it wholesome
. So together with that wholesome
 consciousness mental states or mental factors arise. Among
 these mental factors there is one
 which is called volition, chitana. So here the answer is
 what is virtue? Chitana is virtue. So
 when you say virtue we mean chitana and chitana which
 accompanies the type of consciousness which
 arises in our minds when we abstain from killing stealing
 and and so on. For this is said in the
 Bhadisambhita. It is the name of a book Bhadisambhita. It
 is in our countries we include this in the text
 and it is sotabhita. But actually they are something like
 appendix to sotabhita. And the
 Bhadisambhita was not taught by the Buddha but by the Vener
able Saripura, his chief disciple. And the
 commentator, the Venerable Buddha goes ahead much respect
 for this book Bhadisambhita. He quotes
 from this book very often throughout his book the Vissothi
 Maga. So what is virtue? There is
 virtue as volition, virtue as consciousness concomitence,
 virtue as restraint, virtue as
 non-aggression. This is what is said in that book Bhadisamb
hita Maga. So in that book it is said
 volition is virtue. Mantrofactors are virtue, restraint is
 virtue, non-transgression is virtue.
 So virtue can mean any of these things. Consciousness con
comitant just means what? According to our
 understanding. Jtsekas. So sometimes Jtsekas are called
 virtue. Sometimes restraint. We have
 abstinances among the 50 do Jtsekas. You remember them?
 Right speech, right action, right livelihood.
 So they are called restraint here. And then there may be
 other restraints too. And virtue as
 non-transgression and non-transgression is also called
 virtue. So you know you know volition.
 Volition arises with the consciousness when we abstain from
 something. Vargue as consciousness
 concomitant is the abstinance in one who attains from
 killing, living and so on. Abstain from killing,
 living beings, living things and so on. So here
 consciousness concomitant means abstinances,
 three abstinances. So sometimes three abstinances are
 called virtue. So sometimes Kirtana is called
 vaju. Sometimes three abstinances are called vaju. And then
 in another way, vaju as volition is the
 seven volitions that accompany the first seven of the ten
 causes of action, comma, in one who
 abandons the killing of living beings and so on. Now there
 are ten, what we call ten causes of
 action. That means abstaining from killing, abstaining from
 stealing, from secular misconduct,
 from lying, from backbiting, from harsh speech and from
 talking nonsense. So this seven and then
 we'll come to the three later. So when the author says
 seven, he meant these. So abstain from killing
 and so on. So three misconduct by body and four misconduct
 by speech. So they are seven. These
 seven kinds of volitions are called volition here or seela.
 Vaju as consciousness concomitant is the
 three remaining states consisting of non-covertousness, non
-ill will and right view. These three plus
 the seven mentioned above are called ten causes of the
 wholesome action, kusala. So non-covertousness,
 non-ill will and right view or right understanding. Stated
 in the very beginning, abandoning
 covetousness, it dwells with mind, free from covetousness
 and so on. Now vaju as restraint
 should be understood here as restrained in five years.
 There are five kinds of restraints mentioned
 in the soldiers. The first is restrained by the rules of
 community. That means rules of community
 really means rules for monks. So there are 227 rules of
 monks. So when a monk keeps these rules
 and he has to restrain his bodily actions and speech of
 verbal actions. So there is a strain
 according to the rules of party moka. Restrained by
 mindfulness. Mindfulness is sometimes called
 restraint. Restrained by knowledge. Understanding or
 knowledge is restrained. Restrained by patience
 and restrained by energy. Here in restraint by the party m
oka is this. He is fully furnished
 with this party moka restraint. Restrained by mindfulness
 is that he got the eye fagality
 and does a restraint of the eye fagality. That is when you
 see something you try to just see it and
 not go to liking it or disliking it. So if you can just see
 it and avoid the unwholesome mental
 states from arising then you are said to be restraining the
 eye. Restraining the eye fagality.
 In fact restraining the eye fagality doesn't mean closing
 your eyes and not look at things. But you
 look at things you see but you do not get akusala from
 seeing. That is the restraint in the eye. So
 in order to in order not to be attached to things you see
 or not to be upset by things you see you
 have to keep mindfulness. So that is why mindfulness is
 called restraint here. And knowledge is
 also called restraint. The currents in the world that flew
 ajita said the blessed one are stamped
 by means of mindfulness. The currency means akusala. So
 these akusala are stamped by mindfulness. When
 there is mindfulness they cannot arise. They are stopped or
 something like that. Restraint of
 currents eye proclaim by understanding they are damned. So
 by understanding they are totally
 subdued or suppressed. So here understanding is called
 restraint. And the use of requisites
 is here combined with this. That means use of requisites is
 also called restraint here. Use of
 requisites means this is for monks. Monks have to use requ
isites with reflection with understanding.
 That means whenever I put on this robe I have to say to
 myself or I have to make a reflection I
 use this robe just to ward off coal to ward off heat and to
 ward off bites by insects something
 like that. And when we eat something also you say I eat not
 for you know not to take pride in my
 strength not to make myself beautiful but I eat this just
 to be able to practice Buddha's teachings
 and so on. And also when we use the we are a dwelling place
 we have to reflect that I use this
 just for get protection from heat and coal and so on. And
 when you take medicine also I take
 medicine just to get rid of disease. So monks have to do
 this whenever they make use of these
 full requisites. And if there is no understanding or no
 knowledge then we cannot do it. Therefore
 the use of requisites is also called restraint by knowledge
. What is called restraint by patients
 this is he is one who bears cold and heat so we bear cold
 and we are patient with heat we are
 patient with thirst we are patient with hunger and that is
 called restraint by patients. Actually
 patient itself is restrained here. And restrained by energy
 means he does not endure a thought of
 sense desires when it arises. That means you have to have
 you have to make effort or you have to have
 energy to to not to endure such thoughts. So when as soon
 as such thoughts arise in your mind you
 just stop it. And that is called here restrained by energy
 because without energy you cannot do
 that. So you have to be alert and you have to make effort
 to stop as soon as these thoughts arise in
 your mind. And purification of livelihood is here combined
 with this. Purification of livelihood is
 also meant for monks. So monks must have a very pure
 livelihood. That means monk must not work
 to earn money. Monk must not tell fortunes read poems so as
 to get something from people. So if
 we do that then our livelihood is said to be impure. So in
 order to to have pure livelihood
 what must we do. We must go out for arms. So we must make
 effort. We must we must have energy.
 That is the only purity of livelihood for monks. So monks
 must not work to earn money or monk must
 not ask people to bring food to them. So they must go out
 for arms. I'm in a very different country
 and so we monks cannot do that in this country. Go out for
 arms. So now we have to ask people to
 bring food to the monastery. So purification of livelihood
 means you must depend on yourself.
 In the books it is said you must you must rely on your foot
 muscles. That means you walk and to
 collect food from from house to house and then return to
 the monastery. So this pifu
 restraint and the abstinence and cleansemen who dread evil
 from any chance of transgression
 method should all be understood to be virtue as restraint
 and virtue as non-transgression. That
 is just non-transgression not breaking the rules by body or
 speech of precepts of virtue that have
 been undertaken. So once you take the precepts you keep
 them you do not break them and precepts
 of body and of speech. Now we must understand that Sīla
 has to do with bodily actions and
 verbal actions not thoughts. You may think of killing a
 living being but so long as you do not
 kill it then you are killing that rule and you are not
 breaking that rule. You may think of
 telling a lie to another person but so long as you do not
 tell a lie you are not breaking that rule.
 So the Sīla is to control the the bodily and verbal
 actions of a person.
 We have a precept which is do not harbor ill will. How does
 that work? It is included in Samadhi and
 in the Theravada Buddhism. Now Samadhi is for control of
 mind, control of thoughts because just
 thinking something bad does not constitute the breaking of
 rules because these rules control
 the actions of the body and actions of the mother of the
 speaks. So although it is not good to have
 and wholesome thoughts still if you do not do it with your
 body or with your speech then you are
 still keeping these rules you are not breaking these rules.
 So in Theravada Buddhism, Sīla is
 for bodily and verbal actions to control them and Samadhi
 is for control of mind and Panya is for
 eradication of mental defilements. Although it is not a
 precept in Theravada Buddhism and non
 ill will is the same as do not harbor thoughts of
 aggression or thoughts of hatred. That is
 included in the three three causes of actions of mind and
 non cover justness, non ill will and
 right view. They are included in the karma of mind. We have
 three kinds of karma, bodily karma,
 verbal karma and mental karma. So they are included in
 mental karma.
 Now what is the meaning of the word Sīla? It may not be
 interesting to those who are not
 interested in Pali but now the word Sīla is explained here
 as meaning composing or upholding.
 So the word Sīla means composing or upholding. Composing
 what? It is either a coordinating
 meaning non inconsistency of bodily action etc due to
 virtuousness. Now non inconsistency really
 means non scattering of one's actions. That means if we do
 bad actions then our actions are said to
 be scattered not coordinated. Yeah I think scattered is
 better than inconsistency. Consistency means
 it must not be different from others or something like that
. You are consistent means you do this
 thing always. It can be a bad habit. That's right yeah but
 here it is me not scattered. Your actions
 are not scattered when you have Sīla and the other meaning
 is to uphold meaning a state of basis
 owing to its serving as foundation for profitable states.
 Only when you have Sīla can you have
 wholesome mental states. So Sīla is something like
 upholding. So these are the two meanings of
 Sīla. For those who understand etymology that means
 grammarians admit only these two meanings.
 Others however comment on the meaning here in the way
 beginning the meaning of virtue is the meaning
 of head Sīla. The meaning of virtue is the meaning of cool
 Sīla that is playing upon the word. You
 know the body word Sīla is close to the word Sīla and
 also close to the word Sīla. So they may
 explain it in this way but it is not accepted by the by the
 commentator here. And what are the
 characteristics function manifestation and proximate cause
 of Sīla. I think you are familiar with these
 characteristics and so on. Whenever we have to understand
 something we have to understand that
 by way of characteristic and so on. Only then do you
 understand this thoroughly and especially when
 you practice meditation you come to understand things
 sometimes by characteristic. That means
 you see the characteristic of in some time. Sometimes you
 see the function. Sometimes you
 see manifestation and sometimes you see proximate cause. So
 with reference to these four we have to
 understand things. So Sīla is also to be understood in
 these four aspects. So what is its characteristic.
 That means what is its individual essence or its nature. So
 the characteristic of it is just
 composing the same as the one I mentioned above. Just as
 visibleness is the characteristic of the
 visible database even when analyzed into the various
 categories of blue yellow etc. because
 even when analyzed into these categories it does not exceed
 the effectiveness. So also this same
 composing described above as the coordinating of bodily
 action etc. and as the foundation of
 profitable states is the characteristic of what you even
 when analyzed into the various categories
 of police and etc. because even when analyzed into these
 categories it does not exceed the state of
 coordination and foundation. Now visible database means
 just the visible object. Among the twenty
 twenty eight material properties there is one thing which
 can be seen and that is the only
 thing which can be seen by our eyes and that is translated
 as form or visible object. Now that
 visible object the characteristic of that visible object is
 just visibleness that it can be seen.
 So that is its characteristic although we can say that
 visible object is blue yellow red white and
 so on it they may differ in these aspects but according to
 characteristics they are only one.
 They are only one visible data which is a characteristic of
 visibleness. So in the same
 way we may describe CELA as Cheetana is CELA, Cheetah
 seekers are CELA, restraint is CELA,
 non-translating is CELA but however varied and different
 kinds of CELA may be the characteristic
 the common nature of all different kinds of CELA is just
 composing. So it is the characteristic of
 composing. And then what is its function? Now here the word
 function or the Pāli-vādrasa has two
 meanings one is action or function and the other is
 achievement. Sometimes the function as action
 is used and sometimes function as achievement is used but
 here both are mentioned. So what is the
 function of CELA stopping misconduct it is its function. So
 when you have CELA you do not break
 precepts and so you do not do misconduct. So stopping
 misconduct is its function,
 function as its nature and blamelessness is its function as
 achievement. That means when you achieve
 purity of CELA then you are blameless. So blamelessness is
 actually the outcome of the purity of CELA. So
 blamelessness is also said to be the function of CELA. So
 the Pāli-vādrasa has two meanings function
 or action and achievement and it is manifestation. Now what
 you so say those who know itself as
 purity will show and for its proximate cause they tell the
 pair conscience and shame as well. Now the
 manifestation of CELA is just purity. When we concentrate
 on CELA it appears to us as purity.
 So CELA is real purity. So purity is the manifestation of C
ELA and virtue and its proximate
 cause is conscience and shame. Its proximate cause is shame
, actually shame and fear. Shame to do
 wrong things, shame to do unwholesome things and fear to do
 unwholesome things because if we do
 unwholesome things, if we do if we are moral conduct is not
 here we will get bad results. So this
 shame and fear these two are called the proximate cause of
 CELA. So long as people have this
 conscience and shame or shame and fear evil will keep
 precepts and the moment these two leave people
 then people will do anything they like. There are two kinds
 of causes and near cause and far cause.
 So it is near cause. That's right, yes. Okay, I've got only
 nine pages. So next
 week I don't know how you can get each copy of this book.
 Can you make copies? Xerox,
 I suppose we could Xerox some of the pages. Yes, so that
 they can read before they come to the class.
 Downstairs in the reading room there's at least one copy. I
'm trying to see if there are two copies on the reserve.
 Xerox, maybe the first chapter. Yes, so please read about
 20 or 30 pages for next week. And after
 you're read and you come to the class and we can discuss.
 So I didn't mention very much what the role
 of the Sutti Maga was in studying in Theravada countries
 but I've heard that if you want to
 practice Buddhism they often say you need two things you
 need a cave and a copy of this book.
 That's right, yes. It's held in very high esteem in Therav
ada countries. It's next to the Buddha,
 actually. This is the dumbest edition of the book in Pali.
 It's the first volume of the book.
 Any other questions? Yes. This time, not many questions.
 Okay, the first two chapters you have to be patient with
 because it deals with the
 practice, I mean the rules of monks and practices of monks.
 But we can we can adapt some of the
 statements in this chapter and the next chapter to Sila for
 laypeople. So monks are exalted to
 be very strict in keeping their rules. They not to have
 broken even a small rule and it is said
 later on we will find that seeing danger in the smallest
 transgression or something. So in the
 same way if you're going to practice meditation then you
 have to you have to clear the basis you
 have to establish a firm foundation of moral purity first.
 That means as a layperson you take
 precepts at least five precepts and keep them. So you are
 not to break any one of these rules,
 you are to keep them intact and you may be exhorted not to
 break any of these rules even though your
 life is in danger or something like that. So we can adapt
 the admonition or the adverse given in
 this chapter to the practice of laypeople too. But this
 chapter deals with monk behavior,
 monk Sila and I'm afraid you will find many improper
 contacts and resort mentioned here.
 So you know how monks are clever in acquiring things for
 them and clever in talking not
 direct lie but something like that. Something like white
 lie or something. So and one thing
 good about these these commentaries is they give many
 stories so we can learn from these stories.
 So from the third chapter onwards it's for those who
 practice meditation whether you're a layperson
 or a monk although the emphasis is on the monks because
 monks are those who practice meditation
 more than laypeople but it is no longer true. Laypeople are
 also very interested in meditation
 now especially in Burma about the turn of the century monks
 became interested in the practice
 of Vipassana meditation and also give chance to laypeople
 to practice Vipassana meditation.
 Formerly laypeople did not think that they could practice
 much meditation because they
 had things to do in the world and even though you are a
 monk if you want to practice meditation
 then they think you have to go to a very secluded place and
 be there alone and practice meditation.
 But the teacher of my teacher Mahasi Sehra was one of the
 pioneers of the rekindling of interest
 in the practice of meditation both by monks and laypeople
 and I think he was the first who taught
 meditation to laypeople and who accepted laypeople to come
 to the monastery and practice intensive
 meditation like we do now. His personal name was Unarda but
 he was known as Mingunzido Gunseyaro.
 His name was mentioned in Jana Bonika's book, The Heart of
 Buddhist Meditation.
 So now many laypeople practice Vipassana meditation in
 Burma and also in this country now people are
 very interested in meditation. So I think this is a good
 sign of a good trend Buddhism is going
 towards because after all practice is what counts just
 understanding or just knowing theoretically
 will not help us much we have to put this theoretical
 knowledge to practice.
 And the study and practice I think should go together.
 Just study will not help us much in understanding and
 something you see through
 the practice of meditation helps you to understand, to have
 deeper understanding of
 what you know from books. So these two should go together
 practice and meditation.
 Okay thank you.
 So
