 There's a video where you talk about how to be calm and
 avoid anxiety and you say that
 the important thing is how you react, but does this help
 remove the anxiety from our
 heads?
 Yes.
 Yes, the important thing is to separate the anxiety from
 the result, the resultant physical
 manifestations of anxiety, the butterflies in your stomach,
 the tension in your shoulders
 and the headache and the fluttering of the heart, the
 beating of the heart and so on.
 All of that is not anxiety and that's really the problem
 even with meditation because you'll
 say to yourself anxious, anxious and the effects don't
 disappear.
 You still have butterflies in your stomach, you still have
 a heart beating fast and so
 on.
 And so as a result you think it did nothing and then you
 get anxious again.
 You say, "Oh no, it's not working.
 Oh no, I'm still anxious."
 And then you get more and more anxious and it snowballs.
 This is what it does if you're not meditating.
 You feel the effects in the body, you get more anxious.
 You get anxious and the effects in the body get worse.
 You feel the effects in the body and you get totally
 paralyzed as a result.
 If you focus on the anxiety and focus on the effects of the
 body objectively, seeing them
 for what they are, you don't need the bodily manifestations
 to disappear.
 This is something that we don't get.
 You can be totally calm in the mind and have your body
 freaking out.
 This is logically logical.
 So you don't have to worry about what's going on in my
 heart.
 My heart is beating very fast, I must be anxious because no
, you can't stop that.
 It's something that's going to take a few minutes to calm
 down.
 But immediately you can do away with the anxiety in the
 head.
 It's important to see that and it's important to make the
 distinction because I've given
 talks before where my heart was beating really, really fast
 and I had butterflies in the stomach.
 One might say anxious, but where I had caught the anxiety
 and was acknowledging the feelings
 in the body, I was clearly aware of them and gave a great
 talk and was perfectly clear
 and perfectly calm and happy even though my voice was even
 shaking because of the fluttering
 in the body.
 That's really funny which is going along with what you're
 saying is what I've noticed
 and is myself and other people say, "But I'm not supposed
 to be feeling like this."
 You're not supposed to be feeling like this.
 This is how you're feeling.
 It's like people with lifelong depression, they're trying
 to always find that drug or
 that therapy that's going to cure them, but maybe if they
 were to just realize, "Oh,
 this is the way I've always felt.
 Maybe they can adjust with it better and go on."
 Actually, that's what had taken place with myself was, "Oh,
 but I'm not supposed to be
 feeling like this."
 My own sister had to say, "Hey, dummy, you've been feeling
 like this since you've been
 two years old.
 So have I.
 We inherited it from our parents.
 It's genetic."
 It's like, "Oh, you mean so I am supposed to be feeling
 like this?
 Yeah?"
 It's like, "Oh, okay.
 Well, I'm going to choose to start feeling other things."
 It allowed me that opening to acknowledging it.
 It's like the idea of like, "Oh, you're not supposed to
 feel pain."
 What do you mean you're not supposed to feel pain?
 The more you're saying that, the more you're going to feel
 the pain, obviously.
 When it comes to physical pain, we all know that when you
 start not resisting it, but
 settle into it, allow yourself to acknowledge it, you
 realize that the pain starts alleviating
 itself on its own.
 I just had to make that comment there because when you can
 sit back and just say, "Oh, the
 chaos going on around me, well, that's actually normal.
 That's samsara.
 Okay, let me just work on not thinking that this is all out
 of order."
 No, I just have to direct my own mind and just be calm.
 Because really, that's the case.
 We're living in chaos constantly, whether it's internal or
 external, whether it's our
 own perception of our own pain or the perception out around
 us.
 Maybe for a moment there, we have that little bubble of
 where we feel everything's all right
 but then burst.
 Part of the joy of practicing dharma is to go with those
 ups and downs and the bursts
 and the deaths of all these different experiences.
 I probably said a little too much, but I just had to...
 No, that's exactly...
 I'm with you there.
 I think one of the best examples of this that I can think
 of, especially as it relates to
 anxiety, is insomnia.
 People are only insomniac because they want to sleep.
 That's obvious, right?
 Because I should be sleeping.
 It's the worst form.
 I used to be insomniac and I'd be up until 3am because I
 thought I should be sleeping.
 The quickest things that Buddhism is able to cure, as far
 as things that need to be
 cured, is insomnia.
 All it takes is for the teacher to say, "You don't need to
 sleep.
 You would be far better off if you were to stay awake all
 night and be mindful.
 So do it."
 It comes into play, especially when they do an intensive
 meditation course.
 Even people in daily life, when you tell them, "Well, then
 don't sleep.
 Stay up all night and meditate."
 You say, "Good for you.
 I'm so happy for you.
 I have to argue with my students to get them down to 6
 hours of sleep at night."
 "Oh, I could never do 6 hours of sleep."
 And then these insomniacs are what I get 2-3 hours and I
 say, "Great.
 Good for you.
 More time for meditation.
 Meditate all night."
 And immediately, of course, they fall asleep and have no
 problems at all.
 But the idea that you need to sleep more and so on, that
 you should be asleep.
 I thought that's a...
 That conflict.
 Yeah.
 Stress.
 I mean anxiety, it certainly is.
 Someone adds to this and says, someone's saying, "Can you
 talk about anxiety and how to deal
 with it?"
 Which I think we've done.
 And the second one right above it says, "Anxiety and
 insecurity."
 Insecurity might be an interesting topic.
 That's a good one.
 Yeah.
 It's a little bit different than anxiety, isn't it?
 It's a...
 I don't know if it's a Western world problem largely, but
 there's a lot of insecurity I
 find in the social circles that I run in.
 Yeah.
 The feeling that you are not...
 You're inadequate or worrying that you might be inadequate.
 Right.
 Yeah.
 Remembering things that you've done and, "Oh, I was such an
 idiot to do that."
 And what they must think of me and so on.
 We're so much harder on ourselves than other people are,
 aren't we?
 You know, you've ever been to one of those situations where
 you just did something you
 thought must have...
 Everyone must think you're an idiot and you just really
 ruined your relationship with
 everyone and no one else really noticed.
 And they're kind of like, "Oh yeah, whatever."
 And we're like killing ourselves.
 They're thinking about themselves.
 Yeah, exactly.
 We're thinking about ourselves, they're thinking...
 So they don't really care.
 It's an interesting thing that that tends to be the case.
 Our insecurity is thinking, "How are they looking at us?
 They must really be judging us."
 People don't care.
 People don't really judge you.
 They judge you for a second and then they go back to their
 own problems because they
 got lots of them.
 So how do we deal with insecurity?
 Is it just almost like insomnia?
 Is it a similar situation where it's okay to feel insecure
 or...
 No, I mean, obviously you do, but I think it's much more
 difficult because it has to
 do with ego.
 Insecurity is the core.
 It's part of the core which is to give up self.
 Until you can be totally fine with being an idiot, you'll
 never be free from suffering.
 Until you can say something that just really makes you look
 stupid and be fine with that
 because my teacher once did that.
 He said something that was really wrong and it didn't faze
 him.
 He was like, "Yes.
 Oh, yes.
 Sorry.
 That was wrong," and just went on with it.
 Well, I got no problem saying stupid things.
 Of course, I guess we would have to say, based on how you
 said that, it's kind of like a
 joke.
 I wonder if it's possible to be saying lots of stupid
 things.
 There are people who say lots of stupid things and are fine
 with it and aren't in life.
 That's different, isn't it?
 Yeah.
 But they don't have insecurity.
 There are people who...
 That's not true, no?
 Some people who...
 I'm thinking of jokers, for example, people who just class
 clowns and so on, who thrive
 off of it.
 Deep down, are they really secure?
 Interesting topic.
 Interesting question.
 That's interesting because I'm very much like that.
 Sorry, when does it work?
 I was just kind of like had that being in school still, a
 lot of people around this
 age are very, very insecure, self-conscious of just every
 little thing they do.
 They're always thinking, "What do people think of me?
 How do I look right now?"
 Those thoughts come to my mind as well.
 I think a fun, easy way to deal with that is just focus on
 what you're doing at the
 moment.
 You're thinking.
 It's really what you're doing.
 You're overthinking actually.
 What else are you doing?
 Walking, sitting, whatever else.
 I think that's a good way to deal with being insecure or
 self-conscious because in reality,
 you're making up this big story about what other people
 would think of you.
 Really, yeah.
 They do have their own problems.
 Of course, you'll see it as you meditate more that people
 really don't care about other
 people at all.
 They care more about themselves.
 Some people are very altruistic and empathic.
 Some people actually do.
 Yeah, some people are.
 People aren't even caring even about themselves.
 What they're doing is they're just perpetuating this myth
 in their mind.
 Part of their myth could even be masochism or constantly
 putting themselves down.
 The idea that they're really caring about themselves, well,
 the term care, maybe that's
 not the case, but they're definitely constantly playing
 this mythology.
 In that mythology, well, of course, other people really don
't matter because they're not really
 dealing with the real person.
 They're dealing with the image of that person in contact in
 their myth of them.
 You're my daughter.
 You're my son.
 You're my mother.
 Very rarely are they saying, "Oh, I'm a person and you're a
 person."
 It's always in context with this fabrication of how they
 want to create reality as opposed
 to the natural order of reality.
 Very well said.
 It's interesting, too, we talk about caring and insecurity.
 They don't care.
 In some situations where you find someone really does care,
 it helps with insecurity.
 In fact, it doesn't seem to have as much ground.
 It seems like if you care as much as someone might not
 circumstance, it also seems to have
 an effect on insecurity.
 It doesn't seem to remove a perfect way.
 That person that comes in your life and they really, really
 care, what's your response?
 What the hell do they want from me?
 Sometimes.
 Well, what do they really want?
 I don't like them because they're too direct.
 They're too honest to me.
 I fear that.
 That's a common response I've seen.
 Yeah.
 Isn't insecurity a part of the symptom of anxiety?
 No, I think it has ego more.
 I think it's very much of anxiety.
 What they think about me, "Oh, I feel bad.
 What he might think about me.
 I know that I suffer from anxiety."
 It's a very part of anxiety.
 But I think the insecurity comes first and the anxiety
 comes after, or the insecurity
 is talking about something bigger than anxiety, much more
 ego based.
 The real problem there is, as Lou said, much better than I
 could.
 The myths that we're playing, it's all just whether you
 care about it.
 I don't think I was totally correct to say that no one
 cares.
 We don't normally care, but we care more about ourselves
 and others, or we're more focused
 on it because some people are not.
 But regardless, as Lou said, if they're focused on
 themselves, focused on others, the problem
 is simply the myth.
 It's that we don't relate to each other as we really are.
 I was talking today to one of the meditators, and she was
 just asking about something just
 curious offhand about this seeming kind of special power
 that she had to see auras.
 So she could look at someone and she could tell ... It wasn
't their emotions, but she
 explained it.
 One example was how she was able to tell a person's sphere
 of influence, basically how
 powerful a person was, how big they were.
 She got this sense of a person being big and a person being
 small.
 I've heard a monk tell me the same thing in Thailand.
 He said he was able to see some people just filled the room
.
 When they walked into the room, the whole room was full,
 and other people just seemed
 very, very small to them.
 So she was asking about this in general, and how it relates
 to what we're talking about.
 The answer I gave was the idea that it doesn't matter
 whether you can see people's state
 of mind, their emotions, or who they are, because that's
 only who they are at that moment.
 I said to her, "You're going through this intensive course,
 and you can see your own
 mind.
 It's not one train.
 It's not one coherent message.
 Who you are changes from moment to moment, and in totally
 chaotic ways.
 So you might have built up this sankara, this formation,
 this state, this habit, and you
 might have a totally contradictory habit.
 People can suddenly do wonderful things.
 Why is that?"
 I guess the point is just how important it is to shift our
 understanding of beings, and
 to point out how different it is, our ideas of people, and
 the subsequent insecurity that
 comes from thinking of people as people, or as immutable
 entities, including ourselves,
 like I am this sort of person, to seeing the things as a
 moment to moment, as aggregates
 of moment to moment experience, and to see people in that
 way.
 Is it related to listening to the person carefully and not
 having an idea of the person?
 Like Andrew Liu or yourself spoke about mother, child,
 daughter.
 For sure.
 Is that related?
 I think so.
 Because the reason for listening closely should be that you
 know, you understand, this isn't
 the same person that they were last time.
 I can't rely on my idea of who they are.
 They could be totally different at this time, and we are.
 This is something that is very underappreciated, how change
able we really are.
 We could come up with something totally different.
 The person who you see, "Oh, this is this person," and you
 expect them to act in a certain
 way, and they may suddenly come up with some crazy past
 life habit that they've never exhibited
 before.
 They may.
 I mean, of course, talking about past lives, which of
 course is taboo, but I don't care.
 I'm not breaking taboos, so that's what I'm all about.
 That's your past life, talking.
 One of them.
 One of the many.
 Okay, so did we get anxiety?
 Are we all anxiety-doubt?
 I'm going to quit.
