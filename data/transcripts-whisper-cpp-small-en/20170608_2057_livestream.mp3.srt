1
00:00:00,000 --> 00:00:09,960
 Okay, good evening everyone.

2
00:00:09,960 --> 00:00:24,320
 Welcome to our Dhamma broadcast.

3
00:00:24,320 --> 00:00:36,130
 Tonight I'd like to talk about the Mahavitaka, the

4
00:00:36,130 --> 00:00:44,560
 reflections of a great being.

5
00:00:44,560 --> 00:00:55,560
 That's what it's called, the reflections of a great, or the

6
00:00:55,560 --> 00:00:59,760
 great reflections.

7
00:00:59,760 --> 00:01:06,380
 And these great reflections are described as being

8
00:01:06,380 --> 00:01:10,960
 reflections that a great being has.

9
00:01:10,960 --> 00:01:17,360
 But specifically they're about a specific topic which is

10
00:01:17,360 --> 00:01:22,400
 what sort of person is this

11
00:01:22,400 --> 00:01:30,880
 teaching for?

12
00:01:30,880 --> 00:01:37,070
 Now when you hear that it makes you think maybe this

13
00:01:37,070 --> 00:01:41,160
 teaching isn't for everyone.

14
00:01:41,160 --> 00:01:47,030
 Makes us think that, makes us wonder and it feeds the doubt

15
00:01:47,030 --> 00:01:49,440
 that maybe this isn't right

16
00:01:49,440 --> 00:01:52,840
 for us.

17
00:01:52,840 --> 00:01:55,880
 Maybe we're not right for this practice.

18
00:01:55,880 --> 00:02:01,920
 Maybe this practice wasn't made for me.

19
00:02:01,920 --> 00:02:07,480
 And we hear claims that this meditation is for everyone.

20
00:02:07,480 --> 00:02:14,400
 Where we like to think that the Buddhist teaching is

21
00:02:14,400 --> 00:02:16,400
 universal.

22
00:02:16,400 --> 00:02:20,270
 So to understand what it means when we say the sort of

23
00:02:20,270 --> 00:02:22,840
 person that this Dhamma is, that

24
00:02:22,840 --> 00:02:27,710
 this teaching is for, we have to understand what we mean by

25
00:02:27,710 --> 00:02:28,640
 person.

26
00:02:28,640 --> 00:02:30,640
 Right?

27
00:02:30,640 --> 00:02:34,160
 A person is not a static entity.

28
00:02:34,160 --> 00:02:36,720
 We're changing all the time.

29
00:02:36,720 --> 00:02:39,240
 Every day we're a different person.

30
00:02:39,240 --> 00:02:40,240
 Right?

31
00:02:40,240 --> 00:02:42,720
 Every hour it seems.

32
00:02:42,720 --> 00:02:48,480
 In the morning I'm energetic, in the afternoon I'm tired.

33
00:02:48,480 --> 00:02:50,120
 Maybe I like walking in the morning.

34
00:02:50,120 --> 00:02:55,520
 Maybe I prefer sitting in the evening.

35
00:02:55,520 --> 00:03:03,560
 But we change.

36
00:03:03,560 --> 00:03:06,460
 And so that's of course the more important question that we

37
00:03:06,460 --> 00:03:11,080
 often have in our minds is

38
00:03:11,080 --> 00:03:12,600
 how can we change?

39
00:03:12,600 --> 00:03:14,920
 How should we change?

40
00:03:14,920 --> 00:03:18,000
 What sort of person do I want to become?

41
00:03:18,000 --> 00:03:23,320
 How can I become the sort of person that I want to become?

42
00:03:23,320 --> 00:03:28,930
 And so these reflections are really more like admonishments

43
00:03:28,930 --> 00:03:29,160
.

44
00:03:29,160 --> 00:03:30,160
 Self admonishments.

45
00:03:30,160 --> 00:03:37,400
 A great being reflects.

46
00:03:37,400 --> 00:03:40,880
 Reflects on ways that they can improve.

47
00:03:40,880 --> 00:03:45,080
 And someone, more importantly here I think, is someone who

48
00:03:45,080 --> 00:03:47,320
 understands this teaching.

49
00:03:47,320 --> 00:03:55,640
 First to understand that, well, we really aren't prepared.

50
00:03:55,640 --> 00:04:02,560
 Or we really aren't good at this.

51
00:04:02,560 --> 00:04:08,240
 Or we can't be any old person.

52
00:04:08,240 --> 00:04:10,240
 Right?

53
00:04:10,240 --> 00:04:14,140
 Of all the persons that we might be and all the qualities

54
00:04:14,140 --> 00:04:15,680
 we might develop.

55
00:04:15,680 --> 00:04:18,330
 There are some requirements that we're going to have to

56
00:04:18,330 --> 00:04:19,920
 fulfill if we want to succeed.

57
00:04:19,920 --> 00:04:24,160
 But it doesn't mean come here having fulfilled them.

58
00:04:24,160 --> 00:04:28,800
 It means fulfill them as a part of our practice.

59
00:04:28,800 --> 00:04:32,600
 So the direction that our practice has to take.

60
00:04:32,600 --> 00:04:37,210
 So the story goes that Anuruddha was staying in a deer park

61
00:04:37,210 --> 00:04:37,600
.

62
00:04:37,600 --> 00:04:43,480
 In, oh no, blessed was staying in a deer park.

63
00:04:43,480 --> 00:04:46,480
 Anuruddha was staying in a jiti.

64
00:04:46,480 --> 00:04:50,600
 Jiti is a place, I guess.

65
00:04:50,600 --> 00:05:02,920
 Among the jiti is in a bamboo park.

66
00:05:02,920 --> 00:05:06,040
 And we have to know about Anuruddha.

67
00:05:06,040 --> 00:05:08,540
 He's, you know the story about the nutty cakes, many of you

68
00:05:08,540 --> 00:05:09,000
 know.

69
00:05:09,000 --> 00:05:15,510
 But Anuruddha, you might say maybe he was a little bit,

70
00:05:15,510 --> 00:05:17,520
 well he was a little bit distracted.

71
00:05:17,520 --> 00:05:20,640
 I think we can say about Anuruddha.

72
00:05:20,640 --> 00:05:25,320
 Anuruddha was one of the Buddha's relatives.

73
00:05:25,320 --> 00:05:29,880
 And he had a brother, Mahanama.

74
00:05:29,880 --> 00:05:35,360
 And all the Buddha's relatives were becoming monks.

75
00:05:35,360 --> 00:05:38,610
 And so Mahanama said to Anuruddha, look no one from our

76
00:05:38,610 --> 00:05:40,640
 family has become a monk following

77
00:05:40,640 --> 00:05:44,240
 the Buddha, has gone and become a student of the Buddha.

78
00:05:44,240 --> 00:05:45,240
 So one of us should.

79
00:05:45,240 --> 00:05:50,880
 Now otherwise what are they going to say about us?

80
00:05:50,880 --> 00:05:54,840
 And he said, so Anuruddha you pick.

81
00:05:54,840 --> 00:05:55,840
 You want to be a monk?

82
00:05:55,840 --> 00:06:02,520
 Go and live with the Buddha or you want to stay as a

83
00:06:02,520 --> 00:06:04,960
 householder?

84
00:06:04,960 --> 00:06:06,640
 Stay in the world.

85
00:06:06,640 --> 00:06:10,510
 Get a job and make sure it all gets in at the right time

86
00:06:10,510 --> 00:06:12,360
 and then you have to so on

87
00:06:12,360 --> 00:06:16,150
 and so on and bring the grain in and thresh it and whatever

88
00:06:16,150 --> 00:06:18,040
 it is that you do with grain

89
00:06:18,040 --> 00:06:20,240
 and turn it into flour and take it to market.

90
00:06:20,240 --> 00:06:36,120
 You have to make sure everyone's working.

