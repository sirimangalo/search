1
00:00:00,000 --> 00:00:11,000
 [ The sound of a door opening]

2
00:00:11,000 --> 00:00:14,000
 Okay, good evening everyone.

3
00:00:14,000 --> 00:00:17,000
 Welcome to our live broadcast.

4
00:00:17,000 --> 00:00:21,000
 November 24th.

5
00:00:21,000 --> 00:00:25,000
 Oh, this daily thing is working out well.

6
00:00:25,000 --> 00:00:32,000
 Today is the... excuse me, the shaving day in Thailand.

7
00:00:32,000 --> 00:00:36,000
 Tomorrow's the full moon, I think.

8
00:00:36,000 --> 00:00:39,890
 Which means... or maybe today, it depends on which... there

9
00:00:39,890 --> 00:00:42,000
's two traditions in Thailand.

10
00:00:42,000 --> 00:00:46,000
 One's strict and one's not so strict.

11
00:00:46,000 --> 00:00:49,000
 One's strict and one's traditional, I think you could say.

12
00:00:49,000 --> 00:00:53,910
 Because technically, sometimes the moon is full after

13
00:00:53,910 --> 00:00:57,000
 midnight, sometimes it's full before midnight.

14
00:00:57,000 --> 00:01:04,230
 But the traditional group goes more by calculations than by

15
00:01:04,230 --> 00:01:06,000
 exact hour.

16
00:01:06,000 --> 00:01:12,000
 So... yeah, there's two ways of calculating.

17
00:01:12,000 --> 00:01:14,910
 I don't know which one I'm saying tomorrow is, but many of

18
00:01:14,910 --> 00:01:18,000
 the full moons are still the same.

19
00:01:18,000 --> 00:01:26,000
 So anyway, shaving today.

20
00:01:26,000 --> 00:01:28,000
 Shave once a month.

21
00:01:28,000 --> 00:01:30,000
 It's a big thing in Thailand.

22
00:01:30,000 --> 00:01:32,000
 Some monks do shave more regularly.

23
00:01:32,000 --> 00:01:37,480
 In Sri Lanka they shave seemingly every couple days or

24
00:01:37,480 --> 00:01:39,000
 every week.

25
00:01:39,000 --> 00:01:41,000
 It's arguable which one's better.

26
00:01:41,000 --> 00:01:44,540
 Because if you shave like once a week, or once every few

27
00:01:44,540 --> 00:01:46,000
 days, you can shave everything.

28
00:01:46,000 --> 00:01:49,000
 You can shave your beard and your head all at once.

29
00:01:49,000 --> 00:01:52,000
 And then it never gets so long that it's a bit of a drag

30
00:01:52,000 --> 00:01:53,000
 because every month...

31
00:01:53,000 --> 00:01:57,000
 Shaving once a month, and it's a lot of hair to shave.

32
00:01:57,000 --> 00:02:05,000
 But it seems like less fuss to shave once a month.

33
00:02:05,000 --> 00:02:09,000
 Probably get the inevitable question, "Why do monks shave?"

34
00:02:09,000 --> 00:02:13,000
 I think it's cleaner.

35
00:02:13,000 --> 00:02:16,000
 It's less fuss.

36
00:02:16,000 --> 00:02:19,430
 There's no ego attachment to the hair and hairstyle because

37
00:02:19,430 --> 00:02:23,000
 monks were styling their hair in the beginning, sort of.

38
00:02:23,000 --> 00:02:26,880
 And that's why the Buddha instated the rule not to let it

39
00:02:26,880 --> 00:02:28,000
 get longer.

40
00:02:28,000 --> 00:02:35,000
 It can get this long, two fingers long.

41
00:02:35,000 --> 00:02:37,000
 Or two months.

42
00:02:37,000 --> 00:02:40,000
 If you haven't shaved in two months, you have to shave.

43
00:02:40,000 --> 00:02:45,060
 And if it gets longer than two fingers, then you have to

44
00:02:45,060 --> 00:02:46,000
 shave.

45
00:02:46,000 --> 00:02:49,460
 So that also apparently goes for the beard, or technically

46
00:02:49,460 --> 00:02:53,000
 goes for the beard, so that some monks were testing that.

47
00:02:53,000 --> 00:02:56,000
 There are monks who let their beard grow out.

48
00:02:56,000 --> 00:02:59,000
 But still two months you have to shave your beard.

49
00:02:59,000 --> 00:03:01,000
 What's going on with your beard, though?

50
00:03:01,000 --> 00:03:02,000
 After two months.

51
00:03:02,000 --> 00:03:05,000
 There's quite a lot of beard.

52
00:03:05,000 --> 00:03:07,000
 Mostly we don't go by that.

53
00:03:07,000 --> 00:03:11,050
 If I go by that one in Thailand, I have a real thing

54
00:03:11,050 --> 00:03:14,720
 against facial hair in Thailand because most Thai people

55
00:03:14,720 --> 00:03:17,000
 have very little facial hair.

56
00:03:17,000 --> 00:03:22,000
 But still it's, you're a rogue if you have facial hair.

57
00:03:22,000 --> 00:03:34,000
 Big on appearances, unfortunately.

58
00:03:34,000 --> 00:03:36,000
 So, good evening.

59
00:03:36,000 --> 00:03:39,180
 Today I taught two people how to meditate, or one person I

60
00:03:39,180 --> 00:03:40,000
 guess.

61
00:03:40,000 --> 00:03:44,330
 This friend I'm talking about, my university friend, got

62
00:03:44,330 --> 00:03:49,130
 one of her friends to come and learn how to meditate, who

63
00:03:49,130 --> 00:03:50,000
 also is struggling.

64
00:03:50,000 --> 00:03:55,000
 She suffers from many of the same issues as she does.

65
00:03:55,000 --> 00:03:57,000
 But was a very good meditator.

66
00:03:57,000 --> 00:04:00,000
 This new person was picked up like nothing.

67
00:04:00,000 --> 00:04:03,770
 Most people, most of us, I include myself in this, aren't

68
00:04:03,770 --> 00:04:06,000
 able to pick it up very quickly.

69
00:04:06,000 --> 00:04:09,000
 But the odd person gets it really quickly.

70
00:04:09,000 --> 00:04:12,000
 Like her synchronicity.

71
00:04:12,000 --> 00:04:13,000
 It's simple things.

72
00:04:13,000 --> 00:04:16,490
 You can tell when they do the walking, she's able to say "

73
00:04:16,490 --> 00:04:18,000
step" being right.

74
00:04:18,000 --> 00:04:20,000
 So she's in time with...

75
00:04:20,000 --> 00:04:22,000
 That's rare, actually.

76
00:04:22,000 --> 00:04:24,500
 For someone who hasn't done it, it's actually a bit of a

77
00:04:24,500 --> 00:04:25,000
 skill.

78
00:04:25,000 --> 00:04:30,000
 But I was impressed by just that simple thing.

79
00:04:30,000 --> 00:04:31,000
 And then we did...

80
00:04:31,000 --> 00:04:34,130
 She was sat very still when I led her through the

81
00:04:34,130 --> 00:04:38,000
 meditation, and afterwards said she was able to do it.

82
00:04:38,000 --> 00:04:46,000
 There you go, one person at a time.

83
00:04:46,000 --> 00:04:52,830
 I'd really like to see if she keeps it up to bring her a

84
00:04:52,830 --> 00:04:54,000
 book.

85
00:04:54,000 --> 00:05:00,000
 About this charity thing that we're looking at.

86
00:05:00,000 --> 00:05:04,530
 Sandamali got in touch with me, and there's a children's

87
00:05:04,530 --> 00:05:12,000
 home in Tampa that they've been involved with.

88
00:05:12,000 --> 00:05:14,000
 But Robin, you sent me something about dogs.

89
00:05:14,000 --> 00:05:18,050
 Actually, if there was something about cats, or just pets

90
00:05:18,050 --> 00:05:23,000
 in general, because my step-father's a real cat person.

91
00:05:23,000 --> 00:05:28,000
 I guess dogs have more trouble, right?

92
00:05:28,000 --> 00:05:33,000
 Bigger, harder to take care of, suffer more.

93
00:05:33,000 --> 00:05:36,000
 They can't take care of themselves like cats can, maybe?

94
00:05:36,000 --> 00:05:38,000
 Wild cats?

95
00:05:38,000 --> 00:05:41,000
 I'm not sure. There may be something for cats.

96
00:05:41,000 --> 00:05:43,000
 I didn't look that far.

97
00:05:43,000 --> 00:05:45,000
 I was thinking about dogs, too, and I just looked it up.

98
00:05:45,000 --> 00:05:47,000
 I can check for cats, too.

99
00:05:47,000 --> 00:05:50,000
 I was really thinking about helping humans, actually.

100
00:05:50,000 --> 00:05:56,000
 Okay.

101
00:05:56,000 --> 00:06:03,000
 Yeah, I guess humans are important, too.

102
00:06:03,000 --> 00:06:06,000
 They have to look after the dogs, after all.

103
00:06:06,000 --> 00:06:09,000
 Yes. And the cats.

104
00:06:09,000 --> 00:06:17,650
 They have to praise the cats. They have to slave over the

105
00:06:17,650 --> 00:06:30,000
 cats. That's their role in life.

106
00:06:30,000 --> 00:06:36,140
 What are you thinking to do for the children's home? Should

107
00:06:36,140 --> 00:06:39,000
 we do an online campaign? What were you thinking?

108
00:06:39,000 --> 00:06:47,000
 I don't know if I can say yes to that.

109
00:06:47,000 --> 00:06:52,000
 I guess so. I guess I can say that's sort of a neat idea,

110
00:06:52,000 --> 00:06:57,000
 because I just can't talk about money.

111
00:06:57,000 --> 00:07:03,000
 I'm not sure. Can you tell me the name of the organization?

112
00:07:03,000 --> 00:07:07,260
 We haven't decided on one, but I guess it's the Send a M

113
00:07:07,260 --> 00:07:11,000
elly. I guess we can just decide on this children's home.

114
00:07:11,000 --> 00:07:15,580
 Although, hopefully, Send a Melly is watching. She watches

115
00:07:15,580 --> 00:07:20,000
 this. She did watch last night.

116
00:07:20,000 --> 00:07:27,400
 Children's home.org. That's it. Children's home.org. And

117
00:07:27,400 --> 00:07:27,850
 they have a visit. They also have something over the

118
00:07:27,850 --> 00:07:28,000
 holidays.

119
00:07:28,000 --> 00:07:39,000
 It's a nice website. Since 1892. They're in Tampa.

120
00:07:39,000 --> 00:07:42,530
 We could just go for a tour. I could give a gift and we

121
00:07:42,530 --> 00:07:46,000
 could go for a tour. VIP tours.

122
00:07:46,000 --> 00:08:08,040
 So what's a VIP tour? Do you have to give a certain amount

123
00:08:08,040 --> 00:08:15,000
 to become a VIP?

124
00:08:15,000 --> 00:08:18,400
 Looks like they're having a big event on Saturday, December

125
00:08:18,400 --> 00:08:23,000
 12, but you'll be there a little after that.

126
00:08:23,000 --> 00:08:29,400
 Yeah, that's to be early. Though we could give if we... but

127
00:08:29,400 --> 00:08:33,000
 that's when that's an open house.

128
00:08:33,000 --> 00:08:37,270
 Make a donation from our list of most needed items. So they

129
00:08:37,270 --> 00:08:43,000
 have like a wish list of some sort.

130
00:08:43,000 --> 00:08:46,000
 I wonder if there's a way to find out how to become a VIP.

131
00:08:46,000 --> 00:08:51,000
 Like what's the criteria? Then we'd have a goal, right?

132
00:08:51,000 --> 00:08:54,000
 Yeah.

133
00:08:54,000 --> 00:09:05,000
 How you can help.

134
00:09:05,000 --> 00:09:11,040
 Make a financial donation. Here, make a gift in honor of

135
00:09:11,040 --> 00:09:27,000
 someone you care about. Become part of a surrogate family.

136
00:09:27,000 --> 00:09:37,980
 So a benefactor is $1000 each year for five years. That's

137
00:09:37,980 --> 00:09:51,000
 kind of pushing it probably.

138
00:09:51,000 --> 00:09:57,450
 Well, they have a list of their most urgent needs. So we

139
00:09:57,450 --> 00:10:02,000
 could ask people if they'd like to send these items.

140
00:10:02,000 --> 00:10:04,960
 Although I don't think you'd probably want to have to carry

141
00:10:04,960 --> 00:10:07,740
 them down there. So it would be better if we could send

142
00:10:07,740 --> 00:10:11,000
 them to someone in the area.

143
00:10:11,000 --> 00:10:15,950
 But on their most urgent needs, they're looking for women's

144
00:10:15,950 --> 00:10:20,550
 and men's hoodies and personal care items like body wash

145
00:10:20,550 --> 00:10:33,000
 and hair products and things. Batteries.

146
00:10:33,000 --> 00:10:38,460
 Oh, and they have a much longer list of ongoing needs, just

147
00:10:38,460 --> 00:10:44,000
 all sorts of personal care items and recreational items.

148
00:10:44,000 --> 00:10:52,190
 So we could either, you know, look for donations of items

149
00:10:52,190 --> 00:11:00,910
 or do an online campaign or donate gift cards. We've got a

150
00:11:00,910 --> 00:11:05,000
 lot of options here.

151
00:11:05,000 --> 00:11:08,000
 Back to school supplies.

152
00:11:08,000 --> 00:11:15,770
 I'm trying to think of something that could get my family

153
00:11:15,770 --> 00:11:22,000
 involved or to be able to give us a gift to them.

154
00:11:22,000 --> 00:11:28,620
 Or a gift on their behalf would be the best way to give on

155
00:11:28,620 --> 00:11:32,000
 someone else's behalf.

156
00:11:32,000 --> 00:11:41,700
 If we had a bunch of supplies, then we could we could all

157
00:11:41,700 --> 00:11:46,000
 together deliver it.

158
00:11:46,000 --> 00:11:51,540
 We would just need a place to send them to send them down

159
00:11:51,540 --> 00:11:57,090
 there so you wouldn't have to try to carry them all with

160
00:11:57,090 --> 00:11:58,000
 you.

161
00:11:58,000 --> 00:12:04,000
 There's urgent needs in November.

162
00:12:04,000 --> 00:12:23,000
 Batteries.

163
00:12:23,000 --> 00:12:31,000
 They actually serve children and adults.

164
00:12:31,000 --> 00:12:35,980
 They suggest having a donation drive, which kind of sounds

165
00:12:35,980 --> 00:12:52,000
 like maybe what we're talking about, maybe.

166
00:12:52,000 --> 00:13:04,000
 When you're ready, there are a couple of questions.

167
00:13:04,000 --> 00:13:07,000
 All right, let's get back to the number.

168
00:13:07,000 --> 00:13:10,090
 I know I'm starting to feel like a volunteer meeting there

169
00:13:10,090 --> 00:13:11,000
 for a moment.

170
00:13:11,000 --> 00:13:14,150
 At the moment someone dies, how long do they stay near

171
00:13:14,150 --> 00:13:17,000
 their body? Are they still aware of their former selves?

172
00:13:17,000 --> 00:13:20,000
 How long before they are reborn?

173
00:13:20,000 --> 00:13:23,000
 I have no idea.

174
00:13:23,000 --> 00:13:32,000
 I'm not qualified to answer that question. Those questions.

175
00:13:32,000 --> 00:13:36,150
 They're all good questions. I have to find someone who

176
00:13:36,150 --> 00:13:37,000
 knows.

177
00:13:37,000 --> 00:13:43,000
 Some of it's the Buddha has passed things down.

178
00:13:43,000 --> 00:13:50,000
 But not really to that extent. Not really in that detail.

179
00:13:50,000 --> 00:13:50,000
 We have stories that have been passed down.

180
00:13:50,000 --> 00:13:55,000
 Spirits. There was one spirit hanging out by its dead body.

181
00:13:55,000 --> 00:13:58,370
 And a monk came and took the cloth off the body because

182
00:13:58,370 --> 00:14:01,990
 they usually wrap them up in a white cloth and throw them

183
00:14:01,990 --> 00:14:05,000
 in the rubbish in the charnel ground.

184
00:14:05,000 --> 00:14:08,100
 And he pulled the cloth off thinking it would make a nice

185
00:14:08,100 --> 00:14:11,270
 robe cloth. And the spirit got really upset and went back

186
00:14:11,270 --> 00:14:16,000
 into the body and stood up and chased after the monk.

187
00:14:16,000 --> 00:14:20,680
 Chased them all the way back to his kutti, where he closed

188
00:14:20,680 --> 00:14:26,930
 the door and the body fell down, dropped down against the

189
00:14:26,930 --> 00:14:30,000
 door and the spirit left.

190
00:14:30,000 --> 00:14:35,000
 He wanted his cloth back. A real Buddhist zombie story.

191
00:14:35,000 --> 00:14:38,000
 He wanted the cloth back.

192
00:14:38,000 --> 00:14:40,960
 So there was something, the Buddha instated a rule as a

193
00:14:40,960 --> 00:14:44,000
 result of that. I can't remember what the rule was.

194
00:14:44,000 --> 00:14:49,150
 I think there was a rule instated like making sure the body

195
00:14:49,150 --> 00:14:53,000
's really dead or something. What was the rule?

196
00:14:53,000 --> 00:14:57,730
 It wasn't about don't take cloth from dead bodies. That's

197
00:14:57,730 --> 00:15:00,000
 really a neat thing to do.

198
00:15:00,000 --> 00:15:03,350
 And it's not considered stealing because really you're dead

199
00:15:03,350 --> 00:15:07,000
, dude. Move on, get over it.

200
00:15:07,000 --> 00:15:12,080
 But there was something, some sort of minor, minor rule in

201
00:15:12,080 --> 00:15:16,850
 the instated to sort of make sure you didn't get chased by

202
00:15:16,850 --> 00:15:18,000
 zombies.

203
00:15:18,000 --> 00:15:22,000
 Maybe cover the body up with something else?

204
00:15:22,000 --> 00:15:26,000
 I don't remember.

205
00:15:26,000 --> 00:15:29,980
 Dear Bhante, the more I give up doubts, the more I give up

206
00:15:29,980 --> 00:15:33,330
 doubts regarding the practice, the more I understand the

207
00:15:33,330 --> 00:15:36,490
 practice, the more I dedicate myself to the practice and

208
00:15:36,490 --> 00:15:38,000
 the more I let go.

209
00:15:38,000 --> 00:15:40,850
 The more there develops an issue that could probably be

210
00:15:40,850 --> 00:15:43,440
 seen as relating to what you describe as the second

211
00:15:43,440 --> 00:15:47,000
 imperfection of insight, the imperfection of knowledge.

212
00:15:47,000 --> 00:15:50,330
 What has been happening lately is a kind of circle or sees

213
00:15:50,330 --> 00:16:16,640
aw motion or something. I attain more clarity regarding

214
00:16:16,640 --> 00:16:19,370
 the practice, but soon afterwards, when indulging in the

215
00:16:19,370 --> 00:16:22,930
 many activities as before and dealing with the same people

216
00:16:22,930 --> 00:16:26,080
 as before, which tends to be bound to happen due to being a

217
00:16:26,080 --> 00:16:29,110
 lay person living in the world, my experience with these

218
00:16:29,110 --> 00:16:32,800
 activities and people is now different because of how the

219
00:16:32,800 --> 00:16:33,640
 practice has affected the mind.

220
00:16:33,640 --> 00:16:37,140
 Everything is now so much easier and smoother and sometimes

221
00:16:37,140 --> 00:16:40,640
 it even feels like I could clearly accomplish anything.

222
00:16:40,640 --> 00:16:44,130
 This tends to entice or allure very strongly to play the

223
00:16:44,130 --> 00:16:47,440
 mundane game of life, to play this mundane game of life for

224
00:16:47,440 --> 00:16:50,300
 at least a bit longer, as it feels like I'm now almost

225
00:16:50,300 --> 00:16:53,400
 looking at these activities and interactions from the

226
00:16:53,400 --> 00:16:55,640
 outside as some kind of super user.

227
00:16:55,640 --> 00:16:59,220
 Of course, this doesn't last for very long, but it makes

228
00:16:59,220 --> 00:17:00,640
 things difficult.

229
00:17:00,640 --> 00:17:03,790
 Even when it happens, I still always kind of remember the

230
00:17:03,790 --> 00:17:06,910
 importance of the practice and the worthlessness of the

231
00:17:06,910 --> 00:17:10,210
 things I mentioned earlier, but at least this slows things

232
00:17:10,210 --> 00:17:15,640
 down considerably. Can you talk about this? Thank you.

233
00:17:15,640 --> 00:17:19,530
 I think Robin just didn't talk about that. What's the

234
00:17:19,530 --> 00:17:23,590
 question? I'm sorry, Robin, that you had to... That's not

235
00:17:23,590 --> 00:17:28,640
 really fair, is it? I mean, it's needed to hear about, but

236
00:17:28,640 --> 00:17:28,640
...

237
00:17:28,640 --> 00:17:31,950
 Yeah, I think we probably understand what he's talking

238
00:17:31,950 --> 00:17:33,640
 about, that big change.

239
00:17:33,640 --> 00:17:36,970
 Ask a question. I think you needed to talk about it. I mean

240
00:17:36,970 --> 00:17:41,040
, normally, Robin, you're going to say it, but I have pity

241
00:17:41,040 --> 00:17:44,640
 on Robin that she has to say it all.

242
00:17:44,640 --> 00:17:47,930
 That's okay. I don't mind reading long questions, if it's

243
00:17:47,930 --> 00:17:48,640
 helpful.

244
00:17:48,640 --> 00:17:52,670
 Well, in the end, it was no question. What's the question?

245
00:17:52,670 --> 00:17:55,850
 I mean, good for you. It's great to hear about your

246
00:17:55,850 --> 00:18:00,160
 practice, but... No, it's not fair. You have to ask a

247
00:18:00,160 --> 00:18:04,060
 question. It should be short and concise and simple. Easy

248
00:18:04,060 --> 00:18:08,640
 to understand. Is that person still around?

249
00:18:08,640 --> 00:18:10,640
 Yes.

250
00:18:10,640 --> 00:18:16,630
 Well, then ask a question and make it simple. Keep it

251
00:18:16,630 --> 00:18:24,640
 simple. Those are the rules.

252
00:18:24,640 --> 00:18:27,920
 And we don't need so much background, really, because you

253
00:18:27,920 --> 00:18:31,050
'll find that when you do come up with a question, if there

254
00:18:31,050 --> 00:18:34,790
 is one, that you didn't really need to give all the

255
00:18:34,790 --> 00:18:36,640
 background details.

256
00:18:36,640 --> 00:18:41,640
 You have to learn to be mindful. You don't think so much.

257
00:18:41,640 --> 00:18:45,700
 It sounds like there's maybe too much mental activity. If

258
00:18:45,700 --> 00:18:50,040
 you have a question, ask it. Zen, think Zen. You don't be

259
00:18:50,040 --> 00:18:52,640
 hitty with a stick.

260
00:18:52,640 --> 00:18:56,750
 We'll have to implement the Twitter limitation there. What

261
00:18:56,750 --> 00:18:57,640
's that, 120 characters?

262
00:18:57,640 --> 00:19:01,860
 It was originally at something like, what, 400 characters

263
00:19:01,860 --> 00:19:06,640
 or something? 200, and then people, oh, no, it's too short.

264
00:19:06,640 --> 00:19:10,560
 How many posts did that person use just to ask that not a

265
00:19:10,560 --> 00:19:11,640
 question?

266
00:19:11,640 --> 00:19:13,640
 It was just two.

267
00:19:13,640 --> 00:19:16,640
 Oh, Timo, this is the guy who's coming in December.

268
00:19:16,640 --> 00:19:25,660
 So he's going to put a little more information there, I

269
00:19:25,660 --> 00:19:27,640
 believe.

270
00:19:27,640 --> 00:19:34,640
 No, there's no need for background. Just ask a question.

271
00:19:34,640 --> 00:19:36,200
 See, and if you can't formulate a question, then you've got

272
00:19:36,200 --> 00:19:41,280
 a problem. That's the reason. It's a litmus test that you

273
00:19:41,280 --> 00:19:44,560
 still have, still not clear in your mind, which means that

274
00:19:44,560 --> 00:19:45,640
's your problem.

275
00:19:45,640 --> 00:19:49,140
 You have to start thinking, thinking, and looking at your

276
00:19:49,140 --> 00:19:52,360
 mind and maybe read my booklet again to figure out if you

277
00:19:52,360 --> 00:19:56,940
're actually meditating correctly, because it's easier to

278
00:19:56,940 --> 00:20:00,640
 get meditating on the wrong path.

279
00:20:00,640 --> 00:20:04,460
 Maybe you have states of bliss or happiness. Look at the 10

280
00:20:04,460 --> 00:20:06,900
 imperfections of insight and see if you have any of the

281
00:20:06,900 --> 00:20:09,640
 other ones. You feel happy or calm.

282
00:20:09,640 --> 00:20:14,260
 You feel powerful. Well, that's a confidence. That's sandh

283
00:20:14,260 --> 00:20:21,640
ana. Adi moka, I think, is the defilement.

284
00:20:21,640 --> 00:20:32,020
 So you have to say happy, happy, confident, knowing,

285
00:20:32,020 --> 00:20:36,640
 knowing kind of thing.

286
00:20:36,640 --> 00:20:46,680
 I noticed on a calendar, Bonthe, that tomorrow is Anapanas

287
00:20:46,680 --> 00:20:50,640
ati day. Is that a different tradition?

288
00:20:50,640 --> 00:20:52,640
 I don't know.

289
00:20:52,640 --> 00:20:58,760
 Which is the full moon day. I guess it's what they say when

290
00:20:58,760 --> 00:21:03,530
 they say the Buddha taught the Anapanasati sutta. So those

291
00:21:03,530 --> 00:21:09,640
 people who are keen on Anapanasati have created a day.

292
00:21:09,640 --> 00:21:10,640
 Oh, okay.

293
00:21:10,640 --> 00:21:24,270
 Which I sound kind of grumpy there. I didn't mean to sound

294
00:21:24,270 --> 00:21:27,640
 grumpy. It's a good thing. It's good to have that.

295
00:21:27,640 --> 00:21:29,500
 I guess a little bit grumpy because it's like, wow, really.

296
00:21:29,500 --> 00:21:33,480
 It's not really a holiday like Magabuja or Guisaka Puja or

297
00:21:33,480 --> 00:21:40,640
 Asalha Puja. These are the three big ones.

298
00:21:40,640 --> 00:21:44,910
 Because they're old and traditional and meaningful. To have

299
00:21:44,910 --> 00:21:47,660
 a day just for the sutta, well, it's nice. There's nothing

300
00:21:47,660 --> 00:21:51,680
 wrong with it, but it's not on the level of a real Buddhist

301
00:21:51,680 --> 00:21:52,640
 holiday.

302
00:21:52,640 --> 00:21:56,110
 Is there anything special that they do in Thailand on that

303
00:21:56,110 --> 00:21:56,640
 day?

304
00:21:56,640 --> 00:21:59,700
 No, I mean, this is, I don't know which group this is. I

305
00:21:59,700 --> 00:22:02,940
 think it's Thanissaro Bhikkhu that popularized that. I don

306
00:22:02,940 --> 00:22:07,640
't know. Maybe it's his group. Maybe the Dhammayutanikai.

307
00:22:07,640 --> 00:22:12,640
 Oh.

308
00:22:12,640 --> 00:22:35,640
 Certainly not a big thing in Thailand as far as I know.

309
00:22:35,640 --> 00:22:40,770
 Do you think it is important to keep attention on body in

310
00:22:40,770 --> 00:22:44,640
 your daily life? Well, not meditating rather than thoughts.

311
00:22:44,640 --> 00:22:49,280
 One can understand Anicha, Dukkha, Anitta through thoughts

312
00:22:49,280 --> 00:22:49,640
 too.

313
00:22:49,640 --> 00:22:53,550
 Is well not meditating. Question is, while not meditating,

314
00:22:53,550 --> 00:22:56,440
 can one give importance to thoughts or should one confine

315
00:22:56,440 --> 00:22:57,640
 oneself to the body?

316
00:22:57,640 --> 00:23:01,440
 Well, the body is easier when you're not meditating. The

317
00:23:01,440 --> 00:23:04,710
 body is easier to be mindful of. Thoughts can be a bit

318
00:23:04,710 --> 00:23:08,210
 overwhelming, but certainly you should be mindful of all

319
00:23:08,210 --> 00:23:10,640
 four suttipatana during your life.

320
00:23:10,640 --> 00:23:14,740
 We tend to recommend doing the body. It's easier when you

321
00:23:14,740 --> 00:23:18,430
're standing, walking, sitting and lying down. It's just

322
00:23:18,430 --> 00:23:20,640
 easier to be mindful of.

323
00:23:20,640 --> 00:23:24,780
 And that's for most people. For some people, the mind is

324
00:23:24,780 --> 00:23:29,760
 easier. Some people benefit more from focusing on one or

325
00:23:29,760 --> 00:23:32,640
 another of the suttipatanas.

326
00:23:32,640 --> 00:23:38,070
 So I say, no, no. There's no rules like that. Should this,

327
00:23:38,070 --> 00:23:39,640
 should that.

328
00:23:39,640 --> 00:23:46,000
 Suttipatanas are like four weapons that you have and you're

329
00:23:46,000 --> 00:23:52,330
 a gorilla soldier fighting a messy war. And so it's not, it

330
00:23:52,330 --> 00:23:55,640
's not, it's not neat. It's messy.

331
00:23:55,640 --> 00:23:58,460
 Sometimes like this, sometimes like that, you have to

332
00:23:58,460 --> 00:24:03,290
 change tactics depending on the enemy. And so depending on

333
00:24:03,290 --> 00:24:07,640
 the situation, you have to be clever like a boxer.

334
00:24:07,640 --> 00:24:13,840
 You have to know when to duck and when to weave and when to

335
00:24:13,840 --> 00:24:16,640
 punch and when to jab.

336
00:24:16,640 --> 00:24:21,640
 And you have to know the long game. You have to be patient.

337
00:24:21,640 --> 00:24:31,640
 Expect a knockout in one punch.

338
00:24:31,640 --> 00:24:35,880
 It's kind of like a war of attrition. It's not that what is

339
00:24:35,880 --> 00:24:36,640
 that? What does that mean?

340
00:24:36,640 --> 00:24:40,640
 You bleed each other. You bleed your enemies to death.

341
00:24:40,640 --> 00:24:43,980
 Starve your enemies to death. That's really what it is. We

342
00:24:43,980 --> 00:24:48,640
're starving our defilement.

343
00:24:48,640 --> 00:25:10,640
 We're going to note them to death.

344
00:25:10,640 --> 00:25:13,570
 What is this? Can it be that I've answered all the

345
00:25:13,570 --> 00:25:16,640
 questions possible and no one has more questions?

346
00:25:16,640 --> 00:25:20,000
 Viewer sheep is down. Right? Fewer people are watching,

347
00:25:20,000 --> 00:25:22,640
 which is fine. It's expected.

348
00:25:22,640 --> 00:25:30,050
 Who's got time to watch some funny monk for every day? Got

349
00:25:30,050 --> 00:25:33,640
 things they need to be doing?

350
00:25:33,640 --> 00:25:37,640
 That's fine. This is just sort of a hello.

351
00:25:37,640 --> 00:25:40,480
 Tomorrow we'll do another Dhammapada. But again, that's not

352
00:25:40,480 --> 00:25:45,640
 going to be broadcast live. It'll be audio live.

353
00:25:45,640 --> 00:25:51,640
 Tomorrow is another Dhammapada.

354
00:25:51,640 --> 00:25:54,640
 That's enough. That's all. Good night. Thank you.

355
00:25:54,640 --> 00:26:00,640
 There was one more question if you had time.

356
00:26:00,640 --> 00:26:05,150
 Hi, Bante. Is an Arahant protected from physical harm only

357
00:26:05,150 --> 00:26:07,640
 during meditating or always?

358
00:26:07,640 --> 00:26:10,880
 They're not protected from physical harm even when med

359
00:26:10,880 --> 00:26:11,640
itating.

360
00:26:11,640 --> 00:26:16,630
 There's these stories about when you enter into Jhana or

361
00:26:16,630 --> 00:26:21,640
 Samapati of some sort, you're invincible.

362
00:26:21,640 --> 00:26:25,740
 But that's nothing to do with being an Arahant, per se. It

363
00:26:25,740 --> 00:26:28,940
's to do with the state of attainment, the state of

364
00:26:28,940 --> 00:26:30,640
 meditation.

365
00:26:30,640 --> 00:26:34,990
 Even non-Arahants could theoretically have that sort of

366
00:26:34,990 --> 00:26:36,640
 imperviousness.

367
00:26:36,640 --> 00:26:40,240
 It's not exactly or directly related to their state of

368
00:26:40,240 --> 00:26:45,730
 being an Arahant. It's the state of meditation that they go

369
00:26:45,730 --> 00:26:46,640
 into.

370
00:26:46,640 --> 00:26:50,180
 Didn't the Buddha hurt his foot or have some sort of an

371
00:26:50,180 --> 00:26:52,640
 injury after he was enlightened?

372
00:26:52,640 --> 00:26:56,580
 Mughalana, the most powerful meditative monk who had the

373
00:26:56,580 --> 00:27:01,640
 most powerful attainment, was beaten almost to death.

374
00:27:01,640 --> 00:27:05,110
 Had all the bones in his body broken, apparently. He had

375
00:27:05,110 --> 00:27:09,190
 horrible karma. He had tortured his parents, beaten his

376
00:27:09,190 --> 00:27:12,640
 parents because he didn't want to take care of them.

377
00:27:12,640 --> 00:27:16,640
 He went to hell for that.

378
00:27:16,640 --> 00:27:20,640
 This is a follow-up question from Timu.

379
00:27:20,640 --> 00:27:23,520
 I guess I was looking for help regarding the seesaw motion

380
00:27:23,520 --> 00:27:25,640
 between the practice and the lay life.

381
00:27:25,640 --> 00:27:29,470
 I think the question seemed kind of conceited. Also, sorry,

382
00:27:29,470 --> 00:27:31,640
 Robin, for the long questions. Don't be sorry.

383
00:27:31,640 --> 00:27:35,640
 It wasn't even a question. Where's your question?

384
00:27:35,640 --> 00:27:40,350
 The question is, what sort of help can you give me for

385
00:27:40,350 --> 00:27:47,120
 understanding or dealing with the seesaw motion between the

386
00:27:47,120 --> 00:27:50,640
 practice and the lay life?

387
00:27:50,640 --> 00:27:55,640
 I don't know. I don't like general help questions either.

388
00:27:55,640 --> 00:27:59,090
 They're too complicated. It's like you want me to write a

389
00:27:59,090 --> 00:28:01,000
... You're not alone. Many people ask these sorts of

390
00:28:01,000 --> 00:28:04,640
 questions, but it's like you want me to plan your life out.

391
00:28:04,640 --> 00:28:08,840
 General advice. It's too difficult. I don't know you. I don

392
00:28:08,840 --> 00:28:12,050
't know how to read the Sigalavada Sutta. I'll give you some

393
00:28:12,050 --> 00:28:13,640
 general advice.

394
00:28:13,640 --> 00:28:21,960
 Dighani Kayed 31, I think. Sigalavada Sutta. Really good s

395
00:28:21,960 --> 00:28:22,640
utta, but there's a lot.

396
00:28:22,640 --> 00:28:25,720
 Because it's not that simple. I can't just tell you

397
00:28:25,720 --> 00:28:28,640
 something that's going to help you with that.

398
00:28:28,640 --> 00:28:31,840
 You have to give specific examples, like, is this right? Is

399
00:28:31,840 --> 00:28:32,640
 that right?

400
00:28:32,640 --> 00:28:36,050
 I think maybe to some extent you already know the answer to

401
00:28:36,050 --> 00:28:39,380
 some of the questions that you might ask, but you're

402
00:28:39,380 --> 00:28:40,640
 avoiding them.

403
00:28:40,640 --> 00:28:44,920
 You have to just look at the individual aspects of your

404
00:28:44,920 --> 00:28:49,620
 life. Dealing with people, feeling egotistical, feeling

405
00:28:49,620 --> 00:28:50,640
 ambitious, feeling...

406
00:28:50,640 --> 00:28:53,180
 I'm not sure you're not egotistical. I don't know you said

407
00:28:53,180 --> 00:28:57,640
 that. I mean, ambitious was kind of like you were saying.

408
00:28:57,640 --> 00:29:01,640
 And you know that that's ambition.

409
00:29:01,640 --> 00:29:04,860
 How to deal with things is always just meditate. It's

410
00:29:04,860 --> 00:29:08,220
 always just going to be learned to meditate, learn how to

411
00:29:08,220 --> 00:29:10,640
 see them clearly, learn how to let go.

412
00:29:10,640 --> 00:29:16,540
 But the details of living lay life don't. I mean, the

413
00:29:16,540 --> 00:29:20,950
 ultimate advice, it just comes down to become a monk if you

414
00:29:20,950 --> 00:29:22,640
 want real answers.

415
00:29:22,640 --> 00:29:26,640
 Not an easy thing to do.

416
00:29:26,640 --> 00:29:38,640
 Anyway, apologies. Probably not satisfying, but...

417
00:29:38,640 --> 00:29:46,290
 He's very grateful for the how to meditate books. So that's

418
00:29:46,290 --> 00:29:48,640
 good.

419
00:29:48,640 --> 00:29:56,640
 Okay. Enough. Good night. Thank you, Robin.

420
00:29:56,640 --> 00:29:59,640
 Thank you, Pante. Good night.

421
00:29:59,640 --> 00:30:04,070
 We don't have another question, do we? Are you... Not that

422
00:30:04,070 --> 00:30:04,640
 you see?

423
00:30:04,640 --> 00:30:06,640
 No.

424
00:30:06,640 --> 00:30:07,640
 Okay.

425
00:30:07,640 --> 00:30:09,640
 Okay. Good night, Pante. Thank you.

