1
00:00:00,000 --> 00:00:04,490
 If a person barged into your house, surroundings, clearly

2
00:00:04,490 --> 00:00:05,400
 with the intention

3
00:00:05,400 --> 00:00:08,880
 to kill either you or someone near you, would you have let

4
00:00:08,880 --> 00:00:09,880
 him do it and just

5
00:00:09,880 --> 00:00:14,100
 console yourself with the thought that karma will deal with

6
00:00:14,100 --> 00:00:17,400
 him? Let him

7
00:00:17,400 --> 00:00:19,500
 do it. Well, you know, there are some there's some leeway

8
00:00:19,500 --> 00:00:20,520
 there, but okay,

9
00:00:20,520 --> 00:00:24,480
 let's assuming that the only options were kill this person

10
00:00:24,480 --> 00:00:27,320
 or be killed or

11
00:00:27,320 --> 00:00:30,970
 let him kill someone near you. I mean, if it's going to be

12
00:00:30,970 --> 00:00:32,520
 someone else, then you

13
00:00:32,520 --> 00:00:38,400
 might try to sacrifice yourself instead. But again, how do

14
00:00:38,400 --> 00:00:38,960
 you know what the

15
00:00:38,960 --> 00:00:41,610
 result is going to be? The funny thing about these

16
00:00:41,610 --> 00:00:42,640
 situations is they are

17
00:00:42,640 --> 00:00:46,280
 karmic. Most of the time they're based on very strong past

18
00:00:46,280 --> 00:00:47,640
 karma. So you

19
00:00:47,640 --> 00:00:51,250
 have cases where the perfectly plotted murder is

20
00:00:51,250 --> 00:00:52,960
 unsuccessful or you have

21
00:00:52,960 --> 00:00:58,920
 cases where the perfectly plotted rescue is botched and

22
00:00:58,920 --> 00:01:00,560
 people die, the person

23
00:01:00,560 --> 00:01:04,230
 dies no matter what people try to do. So because you can't

24
00:01:04,230 --> 00:01:05,600
 know the outcome, it's

25
00:01:05,600 --> 00:01:08,260
 quite dangerous to play God and to think that you're going

26
00:01:08,260 --> 00:01:09,480
 to rule the situation

27
00:01:09,480 --> 00:01:15,240
 and try to fix things in any situation. So it's important,

28
00:01:15,240 --> 00:01:16,160
 this is why as Buddhists

29
00:01:16,160 --> 00:01:21,480
 we're very careful to go to the root and to stick to our

30
00:01:21,480 --> 00:01:22,600
 guns in

31
00:01:22,600 --> 00:01:26,190
 regards to the idea that wholesomeness leads to goodness,

32
00:01:26,190 --> 00:01:27,040
 it leads to happiness

33
00:01:27,040 --> 00:01:30,230
 and unwholesomeness leads to suffering. And so we're very

34
00:01:30,230 --> 00:01:31,200
 careful that our

35
00:01:31,200 --> 00:01:35,170
 actions, our speech and our thoughts are wholesome and we

36
00:01:35,170 --> 00:01:36,640
 take that as a refuge,

37
00:01:36,640 --> 00:01:40,880
 that we are not to blame for bad things if our minds are

38
00:01:40,880 --> 00:01:44,040
 pure. And so we let the

39
00:01:44,040 --> 00:01:47,930
 world, sometimes the world can barge in on us like this. We

40
00:01:47,930 --> 00:01:49,200
 take it to be other

41
00:01:49,200 --> 00:01:52,930
 people's business or our past business, not our present

42
00:01:52,930 --> 00:01:54,560
 business, nothing that we

43
00:01:54,560 --> 00:01:58,680
 want to be involved in now. So that's what I would console

44
00:01:58,680 --> 00:01:59,800
 myself in, not the

45
00:01:59,800 --> 00:02:02,550
 thought that karma will deal with him. The fact that karma

46
00:02:02,550 --> 00:02:03,320
 will deal with the

47
00:02:03,320 --> 00:02:08,720
 person who does harm to others should be a cause for

48
00:02:08,720 --> 00:02:10,160
 remorse if anything,

49
00:02:10,160 --> 00:02:16,420
 although remorse is unwholesome, but compassion and the

50
00:02:16,420 --> 00:02:17,640
 belief that no one

51
00:02:17,640 --> 00:02:22,430
 deserves to suffer, even those people who have done bad,

52
00:02:22,430 --> 00:02:23,480
 not that no one deserves

53
00:02:23,480 --> 00:02:26,820
 to suffer, suffering is never a good thing no matter who it

54
00:02:26,820 --> 00:02:27,520
 is. There's no

55
00:02:27,520 --> 00:02:31,240
 just, there's no gloating over people who have done bad

56
00:02:31,240 --> 00:02:32,560
 things that would be

57
00:02:32,560 --> 00:02:37,880
 unwholesome, it would be unwholesome to gloat. And so we

58
00:02:37,880 --> 00:02:40,320
 would feel compassion

59
00:02:40,320 --> 00:02:45,250
 and wish for that person to find a way to become free from

60
00:02:45,250 --> 00:02:46,520
 their suffering as

61
00:02:46,520 --> 00:02:51,540
 well. We don't take, so we don't ever take take consolation

62
00:02:51,540 --> 00:02:52,520
 in other people's

63
00:02:52,520 --> 00:02:55,930
 suffering, he'll get his karma and get him back, that kind

64
00:02:55,930 --> 00:02:56,760
 of thing. It's not a

65
00:02:56,760 --> 00:03:04,240
 Buddhist thing to do. Nor is it a Buddhist thing to pass

66
00:03:04,240 --> 00:03:04,840
 judgment on

67
00:03:04,840 --> 00:03:07,180
 people because bad things have happened to them. So if

68
00:03:07,180 --> 00:03:08,120
 someone's born with a

69
00:03:08,120 --> 00:03:11,180
 disability, for example, it's not a Buddhist thing to

70
00:03:11,180 --> 00:03:13,160
 dismiss them, thinking

71
00:03:13,160 --> 00:03:17,330
 that they've done bad things in their past life. There's a,

72
00:03:17,330 --> 00:03:17,720
 we have this

73
00:03:17,720 --> 00:03:23,120
 kind of general belief and understanding that most likely

74
00:03:23,120 --> 00:03:23,920
 there's some reason

75
00:03:23,920 --> 00:03:26,690
 for why they're that way, but it certainly doesn't turn us

76
00:03:26,690 --> 00:03:27,400
 to think of them

77
00:03:27,400 --> 00:03:31,240
 as a bad person. So some people have criticized Buddhism as

78
00:03:31,240 --> 00:03:32,440
 being, having this

79
00:03:32,440 --> 00:03:38,410
 potential, having the potential to, for abuse, whereby

80
00:03:38,410 --> 00:03:39,880
 people, where we judge

81
00:03:39,880 --> 00:03:46,000
 people who have disabilities and so on, who have a hard

82
00:03:46,000 --> 00:03:47,640
 life, we say, well it's

83
00:03:47,640 --> 00:03:51,920
 just their bad karma and so we look down upon them, which

84
00:03:51,920 --> 00:03:52,280
 is not a

85
00:03:52,280 --> 00:03:54,890
 Buddhist thing to do, it's just not a Buddhist thing to

86
00:03:54,890 --> 00:03:57,880
 look down on people.

87
00:03:57,880 --> 00:04:02,880
 Right, anyway, that's that.

