1
00:00:00,000 --> 00:00:05,600
 Bante, based on many of your videos, I think you don't like

2
00:00:05,600 --> 00:00:08,480
 Samatha meditation at all.

3
00:00:08,480 --> 00:00:10,850
 From what I know, there are two clear ways for

4
00:00:10,850 --> 00:00:13,400
 enlightenment, Samatha first in Vipassana,

5
00:00:13,400 --> 00:00:15,220
 or Vipassana first and then Samatha.

6
00:00:15,220 --> 00:00:18,080
 What do you think about this?

7
00:00:18,080 --> 00:00:23,220
 Actually there are, according to the Buddha, four ways to

8
00:00:23,220 --> 00:00:24,960
 enlightenment.

9
00:00:24,960 --> 00:00:28,720
 Samatha first, then Vipassana, Vipassana first and then Sam

10
00:00:28,720 --> 00:00:32,400
atha, Samatha and Vipassana together,

11
00:00:32,400 --> 00:00:36,600
 or the settling of the mind in regards to the Dhammas.

12
00:00:36,600 --> 00:00:41,740
 So when one has uncertainty or sort of a kind of a mental

13
00:00:41,740 --> 00:00:44,440
 disturbance about the Dhamma and

14
00:00:44,440 --> 00:00:48,960
 one settles one's mind in regards to the Dhamma.

15
00:00:48,960 --> 00:00:51,630
 The Buddha said those are the four ways people can become

16
00:00:51,630 --> 00:00:52,360
 an arahant.

17
00:00:52,360 --> 00:00:55,340
 One who comes to the Buddha as an arahant saying that they

18
00:00:55,340 --> 00:00:56,800
've become an arahant does

19
00:00:56,800 --> 00:01:00,280
 it by one of these four paths.

20
00:01:00,280 --> 00:01:03,680
 So that's the answer to your question, or that's the answer

21
00:01:03,680 --> 00:01:05,200
, that's a correction to

22
00:01:05,200 --> 00:01:07,960
 your question.

23
00:01:07,960 --> 00:01:12,480
 What you know is wrong, there are actually four according

24
00:01:12,480 --> 00:01:15,560
 to the Buddha, so it's incomplete.

25
00:01:15,560 --> 00:01:18,160
 Let's address the comment at the beginning.

26
00:01:18,160 --> 00:01:23,960
 You think I don't like Samatha meditation at all?

27
00:01:23,960 --> 00:01:28,370
 Well that may be true, I may be guilty of that, but if I am

28
00:01:28,370 --> 00:01:30,360
 judging as an outsider I

29
00:01:30,360 --> 00:01:34,180
 would say that that's wrong with me, because not liking Sam

30
00:01:34,180 --> 00:01:36,440
atha meditation is a bad thing.

31
00:01:36,440 --> 00:01:41,620
 Now I have to be careful about what I say, but I tend to

32
00:01:41,620 --> 00:01:44,800
 discourage people from Samatha

33
00:01:44,800 --> 00:01:45,800
 meditation.

34
00:01:45,800 --> 00:01:48,720
 I definitely admit that.

35
00:01:48,720 --> 00:01:51,700
 But it's a light discouragement, and I don't ever say to

36
00:01:51,700 --> 00:01:53,440
 someone don't practice it.

37
00:01:53,440 --> 00:01:58,660
 I say I try to focus on the benefits of Vipassana

38
00:01:58,660 --> 00:02:00,480
 meditation.

39
00:02:00,480 --> 00:02:02,320
 There's two reasons I guess for that.

40
00:02:02,320 --> 00:02:08,130
 The first one is it's what I know, so by focusing on it and

41
00:02:08,130 --> 00:02:11,360
 sort of putting it up higher than

42
00:02:11,360 --> 00:02:17,940
 Samatha meditation, it's because by saying that, so if I

43
00:02:17,940 --> 00:02:21,280
 have person A here, and I want

44
00:02:21,280 --> 00:02:24,960
 to bring them here, the best way for me to get them there

45
00:02:24,960 --> 00:02:26,920
 is to start talking about the

46
00:02:26,920 --> 00:02:29,920
 benefits of insight meditation, because if I start talking

47
00:02:29,920 --> 00:02:31,480
 about the benefits of Samatha

48
00:02:31,480 --> 00:02:34,530
 meditation, I'm not going to be able to follow through with

49
00:02:34,530 --> 00:02:35,040
 that.

50
00:02:35,040 --> 00:02:37,700
 If they say, "Oh great, so can you teach me Samatha

51
00:02:37,700 --> 00:02:38,680
 meditation?"

52
00:02:38,680 --> 00:02:41,480
 I'm not bringing them to the goal, because I can't.

53
00:02:41,480 --> 00:02:43,200
 I don't teach that.

54
00:02:43,200 --> 00:02:45,130
 I will end up saying to them, "I'm sorry I don't teach that

55
00:02:45,130 --> 00:02:45,320
."

56
00:02:45,320 --> 00:02:47,520
 You'll have to go somewhere else.

57
00:02:47,520 --> 00:02:53,320
 So quicker for me is to promote Vipassana meditation.

58
00:02:53,320 --> 00:02:57,520
 But the other reason is, well it's complicated, but I've

59
00:02:57,520 --> 00:03:00,240
 talked about it before, and the first

60
00:03:00,240 --> 00:03:07,010
 reason obviously is it seems to take longer and require

61
00:03:07,010 --> 00:03:10,320
 more to do Samatha first.

62
00:03:10,320 --> 00:03:12,990
 So three things, it seems to take longer, and I'm not, we

63
00:03:12,990 --> 00:03:14,400
'll have a big argument about

64
00:03:14,400 --> 00:03:17,420
 that, but I think generally there's a case that can be made

65
00:03:17,420 --> 00:03:19,160
 for it taking longer, requires

66
00:03:19,160 --> 00:03:24,760
 more and has a greater potential for getting the meditator

67
00:03:24,760 --> 00:03:25,640
 lost.

68
00:03:25,640 --> 00:03:28,080
 So these are the three disadvantages of Samatha meditation.

69
00:03:28,080 --> 00:03:30,480
 Earlier I talked about the benefits.

70
00:03:30,480 --> 00:03:37,050
 It's more complete, it's stronger, and it's more complete

71
00:03:37,050 --> 00:03:38,880
 and stronger.

72
00:03:38,880 --> 00:03:43,240
 So let's talk about those.

73
00:03:43,240 --> 00:03:44,320
 More complete and stronger.

74
00:03:44,320 --> 00:03:47,160
 More complete means you have the potential to enter into

75
00:03:47,160 --> 00:03:49,080
 high states of calm and tranquility

76
00:03:49,080 --> 00:03:55,690
 where you can sit stiff as a board, where you can have

77
00:03:55,690 --> 00:03:59,200
 great bliss, great equanimity,

78
00:03:59,200 --> 00:04:01,910
 and more complete in the sense that you are able to

79
00:04:01,910 --> 00:04:03,640
 cultivate magical powers.

80
00:04:03,640 --> 00:04:07,210
 So you can read people's minds, remember past lives, all

81
00:04:07,210 --> 00:04:08,560
 sorts of fun stuff.

82
00:04:08,560 --> 00:04:16,330
 More powerful, one's ability to enter into cessation gets

83
00:04:16,330 --> 00:04:18,680
 more powerful.

84
00:04:18,680 --> 00:04:21,680
 Once you've cultivated Samatha, as soon as you switch to

85
00:04:21,680 --> 00:04:23,400
 insight, often very quickly

86
00:04:23,400 --> 00:04:28,280
 you're able to cultivate insight meditation.

87
00:04:28,280 --> 00:04:30,920
 Enter into Nibbana as soon as you switch.

88
00:04:30,920 --> 00:04:34,540
 It takes very little time, and it can be very strong, so

89
00:04:34,540 --> 00:04:37,200
 you can actually enter into cessation

90
00:04:37,200 --> 00:04:41,280
 Nibbana for hours or days or so.

91
00:04:41,280 --> 00:04:43,560
 So with the power of the Samatha.

92
00:04:43,560 --> 00:04:44,560
 Those are the two benefits.

93
00:04:44,560 --> 00:04:48,350
 The three disadvantages, it takes longer because you have

94
00:04:48,350 --> 00:04:50,640
 to first cultivate meditation based

95
00:04:50,640 --> 00:04:54,400
 on a concept which has nothing to do with reality.

96
00:04:54,400 --> 00:04:57,400
 And then you have to afterwards cultivate meditation based

97
00:04:57,400 --> 00:04:58,160
 on reality.

98
00:04:58,160 --> 00:05:00,560
 So you have to switch.

99
00:05:00,560 --> 00:05:04,640
 It's two steps.

100
00:05:04,640 --> 00:05:08,160
 They're different because based on a concept you're

101
00:05:08,160 --> 00:05:10,680
 actually avoiding the situation.

102
00:05:10,680 --> 00:05:15,050
 You're suppressing the defilements by not focusing on them,

103
00:05:15,050 --> 00:05:16,960
 by focusing elsewhere.

104
00:05:16,960 --> 00:05:19,000
 So it takes longer.

105
00:05:19,000 --> 00:05:23,000
 It requires more.

106
00:05:23,000 --> 00:05:25,920
 So in order to do that, you need to seclude yourself.

107
00:05:25,920 --> 00:05:27,600
 You need to find a quiet spot.

108
00:05:27,600 --> 00:05:29,800
 Sometimes people can't do that.

109
00:05:29,800 --> 00:05:33,030
 You need ideally to be in the forest, to be away from any

110
00:05:33,030 --> 00:05:35,440
 kind of noise, any kind of disruption,

111
00:05:35,440 --> 00:05:38,680
 be by yourself.

112
00:05:38,680 --> 00:05:41,250
 So it takes more time, requires more, and the potential for

113
00:05:41,250 --> 00:05:42,400
 getting lost because you're

114
00:05:42,400 --> 00:05:44,000
 in the world of concepts.

115
00:05:44,000 --> 00:05:48,670
 If you don't ever make the change, you can spend years, lif

116
00:05:48,670 --> 00:05:51,220
etimes practicing meditation

117
00:05:51,220 --> 00:05:57,800
 without ever making the final step.

118
00:05:57,800 --> 00:06:03,400
 And this is the case.

119
00:06:03,400 --> 00:06:06,860
 You can show that through pointing out individual medit

120
00:06:06,860 --> 00:06:07,520
ators.

121
00:06:07,520 --> 00:06:09,280
 We can show that in the tipitaka.

122
00:06:09,280 --> 00:06:12,320
 It's clear that this samatha meditation isn't enough.

123
00:06:12,320 --> 00:06:19,320
 It leads you to the brahma world where it led the bodhisatt

124
00:06:19,320 --> 00:06:21,680
as to teachers.

125
00:06:21,680 --> 00:06:33,210
 So for those reasons coupled with the fact that we don't

126
00:06:33,210 --> 00:06:34,400
 have that, so we generally don't

127
00:06:34,400 --> 00:06:36,640
 have that much time.

128
00:06:36,640 --> 00:06:43,440
 We don't have that great resources of the forest and so on.

129
00:06:43,440 --> 00:06:46,460
 And I don't have the ability, I suppose I could, I don't

130
00:06:46,460 --> 00:06:47,720
 have the training.

131
00:06:47,720 --> 00:06:51,970
 Actually I probably could do fine teaching samatha and vip

132
00:06:51,970 --> 00:06:54,040
assana, but don't have the

133
00:06:54,040 --> 00:06:59,180
 technical training to lead people first through samatha and

134
00:06:59,180 --> 00:07:01,600
 then through vipassana.

135
00:07:01,600 --> 00:07:03,280
 Therefore we don't do it.

136
00:07:03,280 --> 00:07:06,240
 We focus more on insight meditation.

137
00:07:06,240 --> 00:07:08,080
 Now, samatha is great.

138
00:07:08,080 --> 00:07:09,080
 It's a wonderful thing.

139
00:07:09,080 --> 00:07:12,160
 It quiets your mind, it calms your mind.

140
00:07:12,160 --> 00:07:14,280
 I don't think I really dislike it.

141
00:07:14,280 --> 00:07:17,920
 In fact, in our tradition there's a sense of maybe even

142
00:07:17,920 --> 00:07:18,800
 jealousy.

143
00:07:18,800 --> 00:07:21,730
 It's like, "Oh, I wish I could get more into samatha

144
00:07:21,730 --> 00:07:22,720
 meditation."

145
00:07:22,720 --> 00:07:23,720
 But we're strict with ourselves.

146
00:07:23,720 --> 00:07:26,030
 We say, "No, we're going to focus on this because we don't

147
00:07:26,030 --> 00:07:27,120
 have the time, we don't

148
00:07:27,120 --> 00:07:28,920
 have the resources.

149
00:07:28,920 --> 00:07:34,560
 We want to streamline it, especially in this day and age

150
00:07:34,560 --> 00:07:37,720
 where time and resources are of

151
00:07:37,720 --> 00:07:43,060
 the essence and it's our best attempt at getting the most

152
00:07:43,060 --> 00:07:44,840
 people across.

153
00:07:44,840 --> 00:07:47,410
 You want to get people off the Titanic when it's sinking,

154
00:07:47,410 --> 00:07:48,840
 well you have to improvise.

155
00:07:48,840 --> 00:07:54,800
 You have to find ways to get them off.

156
00:07:54,800 --> 00:07:56,520
 I don't think I don't like samatha.

157
00:07:56,520 --> 00:08:00,120
 I'm pretty sure I don't feel that way.

158
00:08:00,120 --> 00:08:03,710
 When I was young I did practice it, sort of not knowing

159
00:08:03,710 --> 00:08:04,720
 what it was.

160
00:08:04,720 --> 00:08:07,760
 But it's very different.

161
00:08:07,760 --> 00:08:10,270
 What I really take issue with is people who don't see the

162
00:08:10,270 --> 00:08:11,080
 difference.

163
00:08:11,080 --> 00:08:13,230
 People who think they're practicing vipassana but are

164
00:08:13,230 --> 00:08:14,680
 actually practicing samatha.

165
00:08:14,680 --> 00:08:18,280
 That's tough because you can't say that to someone.

166
00:08:18,280 --> 00:08:21,040
 You can't just say to someone, "Your practice sucks.

167
00:08:21,040 --> 00:08:24,440
 Your practice is inferior to my practice.

168
00:08:24,440 --> 00:08:27,440
 Someone's going to say that their practice is better."

169
00:08:27,440 --> 00:08:29,480
 But you know it.

170
00:08:29,480 --> 00:08:32,800
 You're clear in your mind that this is conceptual.

171
00:08:32,800 --> 00:08:36,960
 You're not going to reach enlightenment that way.

172
00:08:36,960 --> 00:08:39,680
 This muddling of the two, this is why we try to make it

173
00:08:39,680 --> 00:08:41,280
 clear that's not going to reach

174
00:08:41,280 --> 00:08:45,200
 nirvana and be very clear about that.

175
00:08:45,200 --> 00:08:48,350
 That's the issue that we always have is people who try to m

176
00:08:48,350 --> 00:08:49,120
uddle it.

177
00:08:49,120 --> 00:08:51,240
 The other issue of course is people who say there's only

178
00:08:51,240 --> 00:08:51,720
 one way.

179
00:08:51,720 --> 00:08:54,800
 You need to practice the jhanas in order to attain

180
00:08:54,800 --> 00:08:55,640
 enlightenment.

181
00:08:55,640 --> 00:09:02,670
 I've talked about that on the buddhism.stackexchange.com,

182
00:09:02,670 --> 00:09:05,240
 which again I think is a better platform

183
00:09:05,240 --> 00:09:06,840
 for those kind of questions.

184
00:09:06,840 --> 00:09:08,920
 But anyway, good question.

185
00:09:08,920 --> 00:09:09,920
 Thank you for it.

186
00:09:09,920 --> 00:09:11,160
 It's kind of a nice one.

187
00:09:11,160 --> 00:09:14,980
 It's not often I get one that calls me out on something

188
00:09:14,980 --> 00:09:16,040
 like that.

189
00:09:16,040 --> 00:09:16,320
 Thank you.

190
00:09:16,320 --> 00:09:17,320
 And...

