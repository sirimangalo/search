 Hello and welcome back to our study of the Dhammapada.
 Tonight we go on to verse number 48, which reads as follows
.
 "Pupaniheva patinantang bhyasattamanasangnarang atitanyewa
 kamisu antakogurutewarang."
 Which means, just as one who is picking flowers, who is
 with a distracted mind wasting their
 time picking flowers, so too a person goes to death, so too
 the human beings go to death,
 never being satisfied by sensual desires.
 "Antakoro kurutewarang," meaning the ender or death takes
 them away under its power,
 or they go under the power of death, never being satisfied
 with sensual pleasures.
 "Atitanyewa kamisu," just like a person distracted by
 picking flowers.
 This is similar to 47, if you've been keeping up.
 In 47 we had just sutanga mang mahogu, just as a sleeping
 village is carried away by a
 great flood.
 So too one is carried away by death, just like someone who
 is picking flowers, "Pupaniheva
 patinantang," the same beginning.
 So it's a similar verse.
 These are not particularly inspiring to, for most of us it
 sounds kind of like a negative
 teaching we're talking about.
 Anyway we'll go over it and we'll try to make it inspiring.
 Hopefully it will inspire us to find something other than
 just sensual desire, sensual pleasure.
 But first the story.
 This is a story that I like to tell often.
 It's another one of these angel stories.
 So if you're not into supernatural things or things that
 seem supernatural in the sense
 that we can't see them and they seem to be outside of the
 real world.
 So maybe a word on that first.
 The real world is an interesting thing.
 The idea of angels existing, I mean I don't know that
 angels exist, but I get the feeling
 they probably do.
 But there's nothing to that.
 But the idea that they couldn't exist because they're
 outside of objective reality, it's
 important to touch on it briefly.
 I don't want to make this a metaphysical talk on metaph
ysics.
 But we look around and we don't see angels.
 We see the hard floor.
 Feel the hard floor.
 We see light.
 We see human beings and we see animals.
 And so we think well, even with our strongest detecting
 instruments, we can't see angels.
 And so we come to think that the world is nothing more than
 a physical course, place
 or sphere of existence.
 But the idea of the existence of angels, so it seems that
 the immediate conclusion is
 that if there are angels, gods or God, this creator God,
 then they must be outside of
 the observable universe or outside of the known universe.
 They must be undetectable and therefore the same as non-
existent.
 But in Buddhism, we don't look at reality from that point
 of view.
 We don't jump to the conclusion that this three-dimensional
 reality is actually out
 there.
 We see reality as beginning from experience.
 So here if you look, you have seeing, you have hearing, if
 you smell, if you taste,
 experiences arise.
 This is how we understand as the basis of reality.
 And so the idea of experiences or beings that experience
 things
 in a different way from this, it's not just speculation.
 It accords with the observation of our own reality when you
 sit in meditation and you
 look in quite detail at the arising and ceasing phenomena.
 You start to get an idea of the depth or the breadth of
 possibility in regards to experience.
 Some people when they're meditating, they have out-of-body
 experiences.
 Some people hear things very far away or enter in a
 mystical state.
 If you listen to people who have these meditative journeys,
 they start to experience things
 in very strange ways.
 People who are on drugs, for example, take hallucinogenic
 drugs or DMT or these crazy
 drugs that just experience things that are so different
 from our ordinary experience.
 You don't need drugs to do this.
 Actually meditation brings you to some very strange and
 exotic experiences and helps you
 to see that what we think of as reality, this physical
 reality, is only conventional.
 It's only because we're so stuck in it, routinely stuck in
 it.
 So we come to see that the step from a human to an angel is
 only a matter of degree.
 It's not categorically different.
 If this reality changes, as our experience changes, it can
 become that reality.
 So an angel is an angel and a human being.
 They can move back and forth.
 This is still just a theory.
 You have to experience it through meditation to agree or
 disagree with that.
 But it touches on the understanding of how could such a
 thing be possible.
 It's not like this universe and then angel universe.
 It's only a difference of degree or a difference of coars
eness and refinement of consciousness
 or of experience.
 So I'm not trying to prove that angels exist or even hint
 at the proof of it, but try to
 help to understand what is an angel.
 It's just another being or it's even us.
 We can shift in that direction.
 And to some extent, you can feel this even in a human
 physical, material sense.
 You can feel that when you engage in unwholesome activities
, if you're meditating, if you're
 sensitive enough, you can feel how sensual lust, how anger
 and hatred, how these things
 really actually drag you down and make you more coarse,
 make you more, I would say, in
 tune in some sense with the coarse material world.
 More sensitive to noises, more sensitive to pain, more
 sensitive to conflict.
 You become dragged down into the world of pain, the world
 of suffering.
 On the other hand, if you engage in wholesome activities,
 people who engage in charity,
 in generosity, in morality, in meditation, in helping other
 people, in sharing with other
 people, in being grateful and respectful, find themselves
 lighter, feel more refined
 and their whole experience of reality changes.
 This is on a very small degree, so it's not yet proof of
 anything particular, but it gives
 you idea of what we're talking about.
 So what we mean by angels is just a being who's gone to
 that degree and at the shedding
 of this human body, the next wave of existence is on
 another level, on a different level,
 as opposed to someone with a coarse experience at the
 shedding of this body who will go to
 a coarse experience.
 So some idea about angels.
 Anyway, that's not the story.
 That's the boring part.
 Now it gets interesting.
 So the story goes, I can't remember where the story starts,
 but the story goes that
 there was an angel up in heaven and two angels up in heaven
.
 A male angel and a female angel, and they were husband and
 wife.
 And there were so many angels playing basically, enjoying
 themselves in heavenly parks.
 In heaven you don't have to go to school.
 You just play all day.
 You don't have to go to work, so you don't have to know how
 to work.
 You don't have to study math anymore or science.
 All those years of study that you had to do to become
 doctors, useless up in heaven.
 Waste of time.
 But you have so much time up in heaven is the other thing.
 So you could, if you wanted to learn Pali, it'd be very
 easy to learn Pali.
 We had this question from my teacher once, because one
 teacher had said, one theoretical,
 how do you say, study teacher, book teacher, monk, he had
 said that in heaven everyone
 speaks Pali, I think is what he said.
 And we were trying to figure out how that could be possible
.
 If a person dies, they just suddenly know Pali or something
.
 And so I was trying, is it really true that when you go to
 heaven everyone speaks Pali
 or something like that, I had some strange idea like that.
 And so we asked my teacher, and he said, well, they just
 learn Pali.
 They can speak whatever language they want.
 The question was, what language do they speak in heaven?
 He said, well, kids learn how to speak in two years.
 In heaven you live for how many, hundreds of thousands of
 years.
 So you can learn whatever you want.
 But sad to say, most people up in heaven don't spend much
 time learning.
 They just spend a lot of time playing.
 So even after hundreds of thousands of years, they still
 don't know anything, because there's
 no need to.
 So they die kind of often.
 It sounds like they die kind of stupid.
 They die without really, just like a lot of human beings
 actually.
 We die not really having learned anything.
 Maybe we learn some math and some physics.
 But many people die without really understanding themselves
.
 Right?
 Like, how many knuckles do you have on one hand?
 Joints was the word.
 They're not knuckles, are they?
 Joints is the word.
 No, I keep telling this story.
 I heard it originally from this man.
 He said, this monk, that monk actually, the monk was up on
 her mantelpiece.
 He said, so I asked this woman, 95 years old, how many
 joints do you have on one hand?
 She didn't know.
 He said, you've been around 95 years old.
 You still don't know how many joints you have on one hand.
 This is how well we know ourselves.
 It's an example.
 Okay, so it's not important to know how many joints you
 have on one hand, but it is important
 to know yourself.
 Right?
 To understand yourself.
 To understand why we suffer.
 What is the, how our mind works.
 How we can use our minds to bring happiness or to bring
 suffering.
 How we can free ourselves from stress, from boredom, from
 dissatisfaction, from greed,
 from wanting, from conceit, from arrogance, from attachment
 to views, from fear, from
 worry and doubt.
 How can we free ourselves from these things?
 It's just worth knowing.
 In heaven they don't do much of that.
 They just spend all their time playing.
 So how do they play?
 Well, I think if I remember correctly, half the angels will
 stay at the bottom of the
 trees.
 I think I've told this part before.
 Half the angels go up into the trees and they throw flowers
 down.
 They're singing and dancing.
 I don't know what their songs are.
 Some kind of something like Sesame Street or something.
 You tell me how to get to heaven until they drop flowers.
 Ooh, throw the flowers down and they play games like that.
 And so this woman, the wife, Angel, she's with them playing
 and her husband's there
 playing as well.
 This is in the morning.
 They go out in the morning after they eat or I'm not sure
 how often they eat, but they
 eat.
 In heaven when you eat food, it doesn't come out.
 You just use it all.
 You don't have to shower.
 You don't have to go to the toilet ever.
 You just eat what you want, whatever you want, however much
 you want.
 And the food there, there was Anuruddha once got some
 heavenly cakes because he had made
 a vow never to hear the word nati.
 You know the story of Anuruddha?
 Anuruddha, he made a vow when he gave food to a Pacheka
 Buddha.
 I mean, I never hear the word nati.
 And he never heard the word nati until his past life.
 And then his mother sent him some cakes for all his friends
 because they were playing
 marbles, right?
 And he lost marbles.
 They bet.
 And the bet is cakes.
 And so he asked his mother to bring some cakes.
 His mother sent some cakes.
 He lost again.
 Bring some cakes.
 Third time he lost again.
 No cakes.
 So she said, "Go and tell my son there aren't any cakes."
 Servants went and said, "Nati."
 What do you mean?
 You asked for cakes and your mother said the answer was n
ati.
 "Well then bring some nati cakes.
 I don't care."
 That's fine.
 Nati means there aren't any.
 Nati means there is.
 Nati means there are not.
 And so he went back and the mother said, "Oh, he doesn't
 even know the word nati.
 He's never heard this word.
 He's always been given everything he wants.
 That's what it means.
 He never had to hear the words there aren't any.
 You want something?
 Okay, you can have whatever you want."
 All his life.
 I said, "Well, I have to teach my son a lesson."
 That's what she said.
 She said, "I have to teach him what this nati means."
 And so she took a silver, a gold platter and a gold lid and
 nothing inside and sent it
 with the servants and said, "Go and tell my son this is
 what nati means."
 And up in heaven, the angels, the heavens started shaking.
 And the angels looked down and they said, "Oh, this is not
 good.
 He made a vow.
 He never heard the word nati or never to know what nati
 means anyway.
 We can't let this happen."
 So they brought some angel food cake.
 Real, like no, not the stuff you buy in stores, but real
 angel cake.
 And they put it under the lid and they sent it and it went
 along to him.
 And when it got to Anuruddha, he opened it up and the smell
 filled the whole playground.
 Just the smell alone was incredible and then they tasted it
 and they never tasted it.
 Of course, never tasted such a thing.
 And Anuruddha got up and he got very angry.
 And he stormed back home right in the middle of the game
 and he went, flung open the door
 and he looked at his mom and he said, "You don't love me."
 And she was like, "Oh dear, now we have to talk about this.
 What do you mean, son?
 If you'd love me, you would have given me nati cakes before
 today.
 You never gave me them before.
 That's proof that you didn't love me until today.
 How could you?"
 He said, "From now on, I will eat nothing but nati cakes."
 And she thought, and he stormed back to the playground and
 she thought to herself, "What
 could have happened?"
 "Oh, angels must have given him some special cakes."
 And so whenever he asked for nati cakes, he would just send
 an empty tray just like that
 and the angels would always fill it up with cakes.
 Unfortunately that sort of thing doesn't happen to us.
 I've never tried it actually, but I've heard the word, "
There isn't any before."
 So that's how good the food is in heaven, much better than
 McDonald's or Pizza Hut.
 Much healthier too.
 But okay, so in the morning they went out and started
 playing their games in the beautiful
 gardens, beautiful forests with no mosquitoes, no snow
 unless they wanted it.
 There would be snow like cotton candy, it would never be
 cold.
 Just falling.
 It would be very tasty, you know?
 There's no suffering in heaven, just pure bliss.
 And then this angel felt something strange and she looked
 at her clothes and her clothes
 were starting to fade and the flowers in her dress, the gar
land of flowers and the flowers
 in her hair, she looked down and saw they were wilting,
 dying.
 And she thought, "This has never happened before.
 Flowers in heaven don't wilt."
 And I think she would feel the sweat in her armpits and it
 would have never happened before.
 Those are the signs that an angel is dying.
 Because after thousands and thousands and hundreds of
 thousands of years, angels die
 as well.
 And she died and disappeared from heaven.
 And everybody else was playing and didn't even notice.
 Too many people, you can't count everyone.
 Too many angels having too much fun.
 And she was born as a little girl in...
 Sawati, I think, somewhere near the Buddha, probably Sawati
.
 And she remembered being up in heaven.
 And she cried.
 And she thought to herself, "How can I get back to heaven?"
 She tried.
 She realized the only reason she went to heaven, because of
 all the good deeds she'd done
 in her past.
 And so she made a vow to do good deeds her whole life.
 To never get angry, to never get upset.
 And she vowed to help and to give.
 And so she was so kind and helpful to her parents and to
 her friends.
 And she was very careful never to speak in angry words,
 always thinking about her husband,
 she would say.
 "My husband.
 Thinking about my husband."
 And so they quickly got her married.
 They said, "Oh, she wants a husband."
 And when she was married, she would always give gifts and
 donations to the monks.
 When the monks came for alms, she would always put food in
 their bowl and dedicate it to
 her husband.
 And they thought, "Wow, here's someone who's really
 dedicated to her husband."
 This is pretty incredible.
 So they called her Pati Pujika.
 This was her name.
 Pati means husband.
 Pujika means one who worships or who reverences their
 husband.
 They thought, "Well, this is wow.
 She's really paying respect to her husband."
 They thought it was the human husband.
 And so they thought, "Wow, this is...
 You know, in Hinduism, that was what you're supposed to do
."
 So they thought, "Wow, she's really being a good Hindu wife
, worshiping her husband."
 It's not really particularly Buddhist to worship another
 person, but it's kind of interesting.
 So that's what they called her, the woman who worshiped her
 husband.
 And so her whole life, she was very...
 She would always go and keep the eight precepts at the
 monastery, and she practiced meditation.
 Lived her life to be 60, 70 years old.
 And she had children, I think two children or four children
, many children.
 And then she was working so hard at the monastery one day
 that she fell over and had some kind
 of...
 I can't remember what the condition was, some indigestion
 maybe, or maybe just a heart problem.
 And she fell over and died.
 Immediately she was born back up in heaven, in the same
 heaven, only now.
 When she left, what time was it?
 When she died from heaven, what did I say?
 Was it morning?
 Yeah.
 After breakfast they went out to play, and they were out
 there for an hour or two in the
 morning.
 When she got back, you know what time it was?
 It was afternoon, same day.
 And it was like, "Oh!"
 A husband comes up and says, "Oh, I didn't see you.
 Where did you go this morning?
 I didn't see you all morning."
 And she said, "I died."
 What are you talking about?
 It's true.
 Where were you reborn in Sawati on earth?
 How long did you live there?
 70 years.
 70 years on earth is a few hours in heaven, he said.
 When she said, "Yes," she said, "Wow!"
 And he thought about it, and it really, really hit him.
 He realized, how short is the life of a human being?
 Wow, considering how short it is, they must really put
 effort into doing good deeds, no?
 Human beings must spend all their time running around
 helping each other and being kind and
 generous and keeping the precepts and giving alms and
 practicing meditation because they
 know that they're going to die in a few hours very quickly.
 He said, "Right?"
 What do you think?
 Do we?
 Do human beings, does the whole world spend all their time
 doing good deeds?
 Yeah, we don't go to war or hurt each other or fight or
 argue.
 Are we thinking about heaven and doing good deeds to go to
 heaven?
 Really?
 Not you, I know you are, but I mean people in general,
 humanity in general.
 She said, "No, no, they're acting like they have millions
 of years to live.
 They're acting like angels.
 They act just like us.
 They think they have all this time to live."
 She said, "They don't spend hardly any time doing good
 deeds.
 It's all what can I get, how can I get it?
 Once I got it, how can I get more of it?"
 Never satisfied.
 I've got a Nintendo, now I want a Nintendo 64, Super
 Nintendo, no?
 Super Nintendo, now it's a Nintendo 64.
 I don't even think you know what those are.
 Those are my type.
 Nintendo 64, and I want the next one, the Wii, right?
 The Wii and now there's something else?
 I don't know.
 All right, always more, more food and entertainment.
 And he heard this and he was, "Wow, incredible.
 How is it?
 They only have such a short lifespan and they spend all
 their time just entertaining themselves.
 Unbelievable these human beings."
 And they went back to playing.
 They went back to enjoying themselves because we've got
 hundreds of thousands of years to
 live.
 We're not going to die.
 The Buddha said, I was just reading now, he said, "When the
 lion comes out of its den
 in the evening, it yawns and then it stretches itself and
 then it roars.
 It lets forth.
 Terrible.
 Roar.
 Roar.
 It scares the humans, it scares the animals, all the birds
 fly up.
 Even the elephants, the Buddha said, safe in the castle,
 even the king's elephants safe
 in the castle in their stalls will run around, will break
 out of their bonds and urinate
 and defecate and get all afraid when they hear the lion
 roar in the wilderness.
 That's how powerful is the lion's roar.
 It scares the willies out of everyone.
 The Buddha said, "In the same way, when the Buddha teaches
 the Dhamma, teaches the impermanence
 of all things, even the angels up in heaven get afraid."
 These angels who run around thinking they're at the top of
 the world and they realize we
 too are subject to impermanence, we too are subject to
 decay.
 We too are a part of this Sakaya, the Buddha said, Sakaya.
 We also are a part of this formed existence that is not
 permanent, that is not stable,
 that is not eternal.
 Because angels forget this and humans forget this.
 "I'm going to live forever."
 There was a song, "I'm going to live forever."
 It's kind of stupid, no?
 Why would you say such a thing?
 "I'm going to live forever."
 Unless you were joking.
 Because we all know that we're not going to live forever.
 Yet we act like it sometimes.
 We don't ever think about this.
 So this angel is very interesting when he said, "Why don't
 these humans think about
 this considering how quickly days go by?"
 You think a day is long, 24 hours, that's a long time.
 How many days have gone by?
 How quickly the days go by?
 You sit here and look at these weeks go by.
 Everyone comes back again every week, every week.
 Wow, how quickly.
 And yet we don't, well people in general don't.
 How many people would never think to practice meditation?
 Why are you wasting your time meditating?
 Go, enjoy your life, live forever.
 This is what they say.
 So then back down on earth, the monks found out about this
 woman dying and they said,
 "Wow, she was a really interesting woman, always respecting
 her husband."
 Wonder what that was about?
 Her husband was just an ordinary guy.
 He wasn't really interested in good deeds himself.
 Wonder why she was worshiping him so much, always saying, "
This is for my husband.
 I may be with my husband when I die."
 And the Buddha heard what they were saying and said, "What
 are you talking about?"
 And they said, "Oh, we're just talking about patipujika."
 How she really worshiped her husband.
 And he said, "Well, it wasn't her husband on earth.
 It was her husband that she'd just left a few hours earlier
 in heaven and she wanted
 to go back and be with him.
 And why not?
 She was in heaven."
 And he told them this story.
 And they said, "A few hours?
 She was alive for 70 years.
 Is it really like that?
 One day up in heaven is 100 years on earth."
 Something like that.
 It was interesting.
 I was talking about this earlier, how Einstein's theory of
 relativity, the faster you go, the
 slower time goes.
 The point being that time is somehow relative.
 It's not what we're saying here, but there's this
 interesting thing where time isn't the
 same everywhere where what seems like 100 years here is
 actually just a day up in heaven.
 There's another reason why we were...
 Someone asked about angels.
 Why you never see angels?
 Why do you never see angels on this topic?
 First reason is because an angel can smell a human being a
 thousand leagues away.
 We smell really bad and angels won't come anywhere near us.
 It's true, apparently, a thousand leagues.
 So if you're wondering, "Why don't I ever see angels?"
 You need a shower.
 But the second, this is another reason, is that when people
 die, why don't they think
 of us and come back and say, "Hey, I'm up in heaven.
 Do lots of good deeds and come up and be with me in heaven
."
 It's so much fun up here.
 We get to play with flowers all day.
 Why doesn't anyone ever do that?
 Why don't you ever have people come back?
 Okay, well, suppose you die and you go up to heaven.
 What's the first thing you'd do?
 Play.
 At least for one day.
 Come on.
 Tomorrow, you can always go and tell your friends and
 family.
 And then what happens when you wake up the next day and say
, "Oh, I got to go tell my
 friends and family.
 They're all dead."
 That's the second reason why we don't ever have angels come
 and tell us about heaven.
 By the time they think of it, it's probably for most people
, it's not the first day like,
 "Yeah, next week."
 You can go back and nuclear Holocaust or something on earth
.
 "Well, maybe I won't go back."
 Silly humans.
 So, the monks said the same thing.
 They said, "Wow, it's amazing.
 And it's amazing that people don't realize this and don't
 actually take the time to do
 something useful with their lives.
 They die as useless as when they were born."
 So then the Buddha said, "Yes, this is true.
 People go to death chasing after central pleasures just
 like someone picking flowers.
 Just as though we were spending our whole lives picking
 flowers.
 It's a useful life.
 And your whole life picking flowers?
 Huh?
 You're listening.
 You're listening.
 Is it a good life to pick flowers?
 You just spend your life picking flowers.
 Central pleasures are like picking flowers, like picking
 flowers.
 I talked about this last time.
 Something wrong with picking flowers doesn't seem, but
 there's nothing useful to picking
 flowers.
 The main point is it doesn't change you.
 It doesn't make you happier.
 This is what we have to talk about.
 We have to understand what this phrase means, "make you
 happier."
 We say, "Well, picking flowers makes me happy."
 Because our only understanding of happiness is central
 pleasure.
 We understand that that moment of pleasure is happiness.
 When we don't look and see what that happiness means, what
 the consequences of enjoying that
 happiness are.
 It's not some horrendous thing where suddenly the sky falls
, but it creates addiction.
 It creates attachment.
 So you want and you need more to be happy.
 So if anyone says, "It makes you happy," well, in what
 sense does it make you happy?
 If you're talking about that one moment, then okay, it
 makes you happy.
 It doesn't make you happier.
 In the sense of, in general, do you become a happier person
 because of it?
 Is it something that makes you happier or makes you more
 peaceful?
 We find that actually it doesn't even make you as happy as
 before.
 It doesn't even leave you as happy as before.
 It leaves you less happy, less satisfied.
 That's why when we sit here, we can't be happy.
 Not all the time anyway, because we're not happy.
 If picking flowers makes you happy, then we should try.
 We can go out and pick flowers all day and then come back
 and sit in here and see how
 happy we are.
 We can be happy thinking about all those flowers we picked.
 What if we had to sit here for an hour listening to someone
 drone on about angels and death
 and stuff?
 How happy would we be then?
 Pupani heva patinantam, just like someone picking flowers
 with a distracted mind.
 We go to death without ever finding, without ever becoming
 satisfied, unsatisfied with sensual
 pleasures.
 There are things on the other hand that do make us happier.
 When we go out and do them, if we went outside and did
 these things, we would come back in
 here and sit down and we would find ourselves happier.
 If we went and picked, split the group up into two, and
 half of us will go out and pick
 flowers and the other half will go out and give gifts to
 homeless people or go out and
 meditate.
 Practice meditation all day.
 Let's try that.
 How many people would volunteer for the meditation?
 I don't know.
 Most people are like, "Okay, flowers.
 I'm up for that."
 Pick flowers all day.
 Either way, you end up with a stiff back, right?
 Picking flowers or meditating.
 But at least for the flowers, you've got some flowers.
 We're not conditioned.
 The reason we're born as human beings is because we've
 conditioned ourselves to chase after
 sensual pleasure.
 The idea of meditating is somewhat repulsive to us because
 of our nature, because of what's
 led us to be born human.
 And yet, when we do meditate, it does make us happy.
 We do come back in here and we are at peace.
 Very difficult.
 The Buddha said it's very difficult to enjoy those things
 that actually make you happy.
 It's very difficult to enjoy solitude.
 Today I went back into the field.
 All the snow's gone for the first time, and so I walked all
 the way across the field behind
 our property.
 I saw one of the trees had steps in it.
 Sounds magical, no?
 One of the trees had steps.
 What do you think that was?
 What was it?
 I don't know.
 I don't know what it was.
 Well, in November, these wonderful humans that are so
 intent on doing good deeds go up into
 these trees and sit there and wait.
 They sit very still up in these trees.
 Sounds crazy, doesn't it?
 Think they're meditating up there?
 People do this in Canada.
 You've never heard of this?
 No?
 They go up and sit in these grown men, not children.
 This isn't a tree house.
 You know what they bring with them?
 Guns.
 They sit up in these trees and they shoot whatever moves.
 They get really drunk, and I don't know what they do here.
 In Ontario, they get so drunk that you're afraid to go to
 your house.
 You have to wear something like this.
 Well, this kind of looks like a deer.
 I don't know.
 It's the only time those bright, bright orange robes are
 any good.
 The ones they wear in Bangkok, they'll be good during that
 time.
 Maybe I'll have to get some for that period here, because
 right behind us, we've got two
 of them, one's on our side and the other's on the other
 side.
 I went up in one of them.
 I didn't bring a gun.
 I was way up in the trees and practiced meditation.
 I've done that before.
 My house, I used to hunt when I was young.
 To make up for it, I decided to go up and meditate in the
 tree to give my forgiveness,
 to give my apologies to the deer and to the forest.
 I went and did that now and practiced meditation there.
 It's amazing that we can't find happiness doing that.
 Sitting in a tree, nothing.
 No worries, no cares.
 No wants, no burdens besides having to come back and tell a
 story to some people, nice
 people.
 This is a call to arms, so to speak.
 This verse, it's a reminder that we don't have so much time
 to live.
 Our lives are not near as long as we think.
 And if we don't take the time to, if we don't spend our
 time wisely, we'll die never being
 satisfied.
 We'll die never having satisfied our desires, never having
 found contentment.
 Death will take us under its control.
 So meditation is for the purpose of changing this, changing
 our conditioning, changing
 our conditioning from being bored when there's nothing
 pleasant, being satisfied no matter
 what is pleasant, being content no matter what we get,
 whether we get nothing at all,
 even if we get nothing at all.
 It's amazing.
 You study the Buddha's teaching and then you look and you
 see how unsatisfied we are, just
 sitting here, how unpleasant it is.
 We're just in this room together, you see.
 And even if I'm talking about good things, I can only keep
 your attention for so long
 before wandering back to something more pleasant.
 This is the state that we're in.
 This is what we have to look at, that we can't even be here
 happy.
 We need something.
 And we think right away, you think, "What are you, crazy?
 If I want to be happy, I need my Xbox.
 I need my Gameboy.
 You still have Gameboy?"
 No, it's old.
 I need my computer.
 I need my Walkman.
 Listen to music.
 I know some kids who just always have their music on.
 Take that out of your ear.
 Some kids, you know.
 This one kid, he would follow me on Om's Round in Los
 Angeles.
 He always had this in his ear.
 How could you be happy without that?
 How could I expect you to find happiness?
 Everyone should put these things in their ears and then
 they'll be happier.
 You wouldn't have to listen to me anyway.
 No, you won't be happy.
 He's very unhappy, this boy.
 That's a good example.
 Oh, you can see how miserable he was.
 He's like a drug addict.
 And I couldn't...
 At home, he was useless.
 He wouldn't do anything.
 He wouldn't do his school work.
 He wouldn't do his chores.
 He has no respect for his parents.
 So they sent him away to Thailand.
 And I was in Thailand when he was there this last year.
 Julian's here, this kid from Los Angeles.
 I don't suppose he'll be watching this.
 And they said, "Oh, well, bring him to meditate with me.
 Oh, we can't take him.
 It's just totally useless and disrespectful and just sits
 around and does nothing.
 I'll send him up here to meditate."
 No, we're sending him back to Los Angeles.
 Nothing.
 No one can handle him.
 You think this is what we get?
 This is the result?
 This is why we're stuffing our ears with these overpriced
 gadgets?
 Ask yourself, why are you here?
 Don't ask Steve Jobs.
 He doesn't have the answer.
 He wasn't Buddhist.
 I don't believe it.
 Anyone who says that doesn't understand Buddhism.
 I said a little bit biased.
 I guess he was a nice guy to his family.
 But I don't mean Steve Jobs.
 I mean, that whole culture of buy, get beautiful, wonderful
, push button.
 This is what the pinnacle of the human race is, to push a
 button, a single button.
 It only has one button.
 I've seen them.
 I've seen them come on.
 I thought it was like, "Oh, look, I've got a keyboard.
 It's got 128 keys.
 That's cool.
 I've got a button, touchscreen.
 They've got these talking cats.
 That's how I expect you.
 I've seen them cutting fruit.
 There's something about cutting fruit."
 Isn't that like enlightenment?
 That's like, "Wow.
 This is where the human race is going, right?
 This is evolution, fruit ninja, angry birds, some angry
 birds."
 This is what life is all about, right?
 No, we call these things past times.
 That's what entertainment means, right?
 Enter means in between, entre, trom, entre, trom.
 Entertain comes from time, I guess.
 It's from French, so French entre, trom, which means
 between times when you're not doing
 anything, when you're waiting for something.
 Angry birds, push the button.
 Past time, because we got too much time, right?
 No, we don't have enough time.
 Think of what the angels are doing.
 What are you doing down there?
 We smell so bad, they don't want to come and tell us, but
 they're like, "I feel sorry for
 those guys."
 There's angels.
 I'm up in heaven right now thinking about us, how stupid we
 are.
 How am I wasting our time playing fruit ninja?
 Think about that.
 Don't you have something better to do with your life than
 become addicted to meaningless
 activities?
 Apparently not.
 It's not easy.
 I'm not trying to make light of this.
 This is a very difficult thing.
 We've been conditioned.
 We've conditioned ourselves into this, and everything
 conditions us.
 The television, the advertisement, society.
 My friends have one.
 I got to get one.
 My friends do that.
 I should do that.
 Well, if your friends jumped off a bridge, would you?
 No, I heard a reply to that.
 The answer is yes.
 Yes, well, yes, because all my friends are quite smart.
 If all of you jumped off a bridge, I think there must be a
 very good reason for jumping
 off the bridge.
 Maybe there's a fire.
 Maybe there's an explosion.
 I should jump off the bridge with you.
 Just make sure you've got smart friends.
 That's really the problem.
 We lead each other in the wrong direction.
 It's an interesting reply because if we have good friends,
 we'll lead each other in the
 right direction.
 That's why we're here.
 If I jumped off a bridge, you might think, "Oh, maybe there
's a reason for jumping off
 the bridge."
 I think that monk's gone crazy.
 When you hang out with good people, then they lead you in
 good places.
 You get to hear good stories and good teachings.
 Then you get to practice good things.
 Now we've heard good things.
 We've heard good dhammas.
 Next step is to practice good things.
 I'm going to try to put this into practice and spend some
 time learning about ourselves.
 Try to find your suffering.
 Try to fight with your suffering like Darth Vader, Obi-Wan
 Kenobi fighting with Darth
 Vader.
 Fight with your suffering.
 Let it overpower you.
 That was the most powerful line in all those movies.
 I only saw the original three and the first one.
 He says, "You can't win.
 If you kill me, you'll only make me stronger."
 He says to Darth Vader, "Did you ever see the first Star
 Wars?"
 He says, "He's fighting with Darth."
 You know Darth Vader?
 Obi-Wan Kenobi is fighting with him.
 He says, "You can't win, Darth.
 If you kill me, you'll only make me stronger than you can
 ever imagine."
 That describes an enlightened being.
 Enlightened being isn't afraid of death.
 You can't win.
 This is exactly an antako kurute warang.
 Antako means the ender.
 It's just the word that means ender, which means the one
 who puts an end to life.
 It's death or the kingdom death or something, personified
 death.
 It brings them under their power.
 But for a person who is free from attachment, they don't go
 under the power of death.
 Death only leads them up like this woman who had done so
 many good deeds.
 Death was what she was waiting for.
 She wouldn't kill herself.
 That's not good.
 If you kill yourself, that's a negative thing.
 That's an attachment.
 She just did as much good, got her mind so free from desire
, free from any clinging that
 she soared up to heaven like a horse.
 For such people, they're not afraid of death and death can
't kill them.
 Death can't bring them under its power.
 That's the story for tonight.
 That's the teaching for tonight.
 Let's try to study ourselves and find some happiness.
 Find out what really makes us happy.
 It's not this.
 Okay, thank you for tuning in.
