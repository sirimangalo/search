 Good evening.
 We're broadcasting live, January 11th.
 Okay, we won't be doing...
 We won't be doing video tonight, just audio.
 Make's him quite busy.
 Tomorrow we have our first five minute meditation lesson.
 A booth set up, hopefully.
 I just spent the evening pounding grommets into a banner.
 Thanks, Robin, for the grommet kit.
 Learned how to make grommets.
 Today we did our meditation in the atrium, just a few of us
.
 Come together, and I have to make something to give out
 tomorrow.
 Got a bunch of stuff to do.
 And there's all this school work.
 For some reason I'm back in school.
 So, busy stuff here.
 We have a quote tonight about...
 It's one of the many quotes about how to tell the
 difference between the Dhamma and what is not Dhamma.
 It's basically this.
 The point of it is that we shouldn't follow something just
 because it's Buddhist.
 We should be open to questioning and analyzing,
 investigating.
 Because it can change.
 We could hear wrong, we could understand wrong, we could...
 We could be misled.
 Our teachers could be wrong.
 But we know that the Dhamma isn't wrong.
 And we know that by its very definition.
 We might say, like, why is...
 How do we know that all Dhamma is good?
 Well, because that's our definition of it.
 If it weren't good, we wouldn't call it Dhamma.
 If it didn't lead to good, if it didn't lead to turning
 away.
 Turning away means abandoning desire and ambition.
 But turning away from samsara is what comes when you see
 things clearly.
 When you see that things are impermanent, unsatisfying,
 uncontrollable.
 When you lose all interest in them, when you lose all
 desire and volition.
 Also, there's the turning away.
 The fading, fading away.
 Because once you turn away from them, they start to fade
 away.
 All the stress and suffering and chaos and confusion that
 we normally live with.
 All the heat, the fever abates, fades away.
 And nirvana or the unbinding become free.
 We let go. We let go. We're free. When we're free, we say,
 "I'm free."
 "Vimutasming, vimutam nithi, nyanam hoti."
 "No, I'm free."
 So it's by verifying that it leads to these things, that we
 know it's the Dhamma.
 That's important. Buddhism isn't supposed to be about dogma
 or beliefs, you know.
 Most religions have some seemingly arbitrary or specific
 dogma that you have to believe.
 Well, Buddhism is really not supposed to be like that.
 Buddhism is about good. It's about principles that are
 universal and non-esoteric.
 Meaning they should be easily understood and easily ver
ifiable.
 Even nirvana. Nirvana is not some magical, mysterious thing
.
 It's part of nature. It's part of reality.
 Something we can realize for ourselves.
 So we practice. We practice. We don't worry too much about
 results, but we have to verify that what we're doing is
 actually good.
 We have to be clear about it, because it's not difficult to
 understand.
 If you're hurting yourself or hurting others, that's not
 good.
 So if there's anything that does that, then you should stop
 it.
 It's helping you and helping others, then it's good. You
 should continue it.
 That's about it.
 To that end, we have all the rest of the teachings of the
 Buddha, all the various teachings.
 We have to internalize them and understand them, understand
 the benefit of them.
 Not just follow them, because that's what we're supposed to
 do.
 Because it's so easy to get it wrong that way.
 You focus, put emphasis on the wrong thing, because you don
't really understand why you're doing it.
 When we meditate, maybe you think the words are important,
 so you emphasize the words in your mind, and then you start
 saying random words.
 Maybe you think that walking is important to this.
 Maybe it's important to sit really straight or easy to
 emphasize the wrong things if you don't understand exactly
 what the point is.
 It's important for us to study and to see where does the
 goodness come from.
 What is goodness? We should ask. What is the good that
 comes from this?
 Does this help me see clearly? Does this help me learn to
 let go?
 By this, will I free myself from suffering? That's what we
 should have.
 Then we know it's the dumb love.
 I'm going to leave you with that.
 I've got some more work to do this evening.
 Tomorrow we should probably have done the bada, but see how
 busy I get tomorrow with this five-minute meditation lesson
.
 Thanks for tuning in. Thanks for meditating with us,
 everybody.
 I wish you all the best.
 Thank you.
 Thank you.
