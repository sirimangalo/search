1
00:00:00,000 --> 00:00:12,930
 So the lay out continue with the talk on the asavā, the

2
00:00:12,930 --> 00:00:15,720
 defilements which cause the mind

3
00:00:15,720 --> 00:00:21,720
 to ferment, which cause the mind to go sour.

4
00:00:21,720 --> 00:00:26,740
 And it's again simply a poetic way of describing the defile

5
00:00:26,740 --> 00:00:29,560
ments which exist in our mind, the

6
00:00:29,560 --> 00:00:32,890
 things in our mind which bring about suffering, which bring

7
00:00:32,890 --> 00:00:34,720
 about discomfort, which bring

8
00:00:34,720 --> 00:00:38,850
 about evil and wholesome states, or those things which are

9
00:00:38,850 --> 00:00:40,760
 evil and wholesome states,

10
00:00:40,760 --> 00:00:44,640
 which exist in our hearts.

11
00:00:44,640 --> 00:00:49,570
 And the third part of the Lord Buddha's talk is on how to

12
00:00:49,570 --> 00:00:53,640
 remove the asavā through patīsīvana,

13
00:00:53,640 --> 00:00:57,960
 through using.

14
00:00:57,960 --> 00:01:03,020
 And this means using the requisites, the things which are

15
00:01:03,020 --> 00:01:05,400
 required to keep us alive and to

16
00:01:05,400 --> 00:01:20,280
 keep us living in a state of comfort and ease and peace.

17
00:01:20,280 --> 00:01:26,400
 And these four things are, these things which are requisite

18
00:01:26,400 --> 00:01:28,960
 for meditators and for monks,

19
00:01:28,960 --> 00:01:31,640
 for people who have left the home life.

20
00:01:31,640 --> 00:01:36,960
 These things are a sign or are a symbol of what it means to

21
00:01:36,960 --> 00:01:39,600
 be one who has left the home

22
00:01:39,600 --> 00:01:41,560
 life.

23
00:01:41,560 --> 00:01:44,460
 So it's important first of all to understand why, what it

24
00:01:44,460 --> 00:01:46,200
 means, what are the things that

25
00:01:46,200 --> 00:01:54,760
 are truly necessary and how these things are used and how

26
00:01:54,760 --> 00:01:59,120
 they will be, how they come into

27
00:01:59,120 --> 00:02:02,040
 play in the Buddha's teaching.

28
00:02:02,040 --> 00:02:05,930
 We come back to think of when the Lord Buddha himself first

29
00:02:05,930 --> 00:02:08,080
 went forth before he was a Buddha

30
00:02:08,080 --> 00:02:13,880
 when he was simply an unenlightened Buddhist atvā.

31
00:02:13,880 --> 00:02:17,040
 And he was searching for the truth.

32
00:02:17,040 --> 00:02:23,240
 So the first thing he did was left behind his home life.

33
00:02:23,240 --> 00:02:30,030
 And this is the first symbolic gesture which we undertake

34
00:02:30,030 --> 00:02:34,120
 on the path to become free from

35
00:02:34,120 --> 00:02:36,440
 suffering whether it be as a monk or as a nun.

36
00:02:36,440 --> 00:02:40,710
 This is the first symbolic gesture of the Lord Buddha was

37
00:02:40,710 --> 00:02:42,800
 to cut off his hair, shave

38
00:02:42,800 --> 00:02:50,620
 off his beard, give up his princely attire and put on a set

39
00:02:50,620 --> 00:02:53,880
 of rag robes, a set of rags

40
00:02:53,880 --> 00:03:04,300
 or cast away cloth and take up life in the forest without a

41
00:03:04,300 --> 00:03:09,720
 whole lot of possessions.

42
00:03:09,720 --> 00:03:16,140
 And the life which the bodhisattva, the Buddha to be lived

43
00:03:16,140 --> 00:03:19,480
 for six years is marked by this

44
00:03:19,480 --> 00:03:28,780
 lack of luxury, lack of possessions, lack of objects which

45
00:03:28,780 --> 00:03:32,480
 might lead to falling back

46
00:03:32,480 --> 00:03:39,960
 into sensual desire, desire for sensual pleasures.

47
00:03:39,960 --> 00:03:42,420
 And so the Lord Buddha when he became enlightened he

48
00:03:42,420 --> 00:03:44,280
 established for the monks and the nuns

49
00:03:44,280 --> 00:03:47,180
 a way to live.

50
00:03:47,180 --> 00:03:50,440
 And this is important because often we might think being a

51
00:03:50,440 --> 00:03:51,980
 monk or being a nun is more

52
00:03:51,980 --> 00:03:55,680
 about rules, about how many precepts you have and the rules

53
00:03:55,680 --> 00:03:57,640
 which you have to follow which

54
00:03:57,640 --> 00:04:00,240
 are different from ordinary people.

55
00:04:00,240 --> 00:04:05,160
 But in truth in the time when the Lord Buddha, when the bod

56
00:04:05,160 --> 00:04:08,000
hisattva went forth, from that

57
00:04:08,000 --> 00:04:12,810
 time for many years the Lord Buddha had established no

58
00:04:12,810 --> 00:04:13,720
 rules.

59
00:04:13,720 --> 00:04:20,420
 And it was only in the time when one monk mistakenly or del

60
00:04:20,420 --> 00:04:23,640
udedly went back and slept

61
00:04:23,640 --> 00:04:29,070
 with him, had sexual intercourse with his ex-wife as a monk

62
00:04:29,070 --> 00:04:29,400
.

63
00:04:29,400 --> 00:04:32,550
 At that point the Lord Buddha decided that it was time to

64
00:04:32,550 --> 00:04:33,980
 lay down some rules because

65
00:04:33,980 --> 00:04:39,610
 the people who had come to be ordained as monks under his

66
00:04:39,610 --> 00:04:43,360
 guidance were no longer completely

67
00:04:43,360 --> 00:04:44,360
 pure.

68
00:04:44,360 --> 00:04:49,910
 And so at that time he began to lay down rules as the cause

69
00:04:49,910 --> 00:04:53,400
 arise, as someone did something

70
00:04:53,400 --> 00:04:56,000
 more and more inappropriate.

71
00:04:56,000 --> 00:04:59,920
 The Lord Buddha would lay down more and more rules.

72
00:04:59,920 --> 00:05:04,260
 But the key, we can see that these rules then are not the

73
00:05:04,260 --> 00:05:07,240
 key to what it means to be a recloser,

74
00:05:07,240 --> 00:05:10,320
 to what it means to be one who has gone forth.

75
00:05:10,320 --> 00:05:13,960
 We can see that the key is the giving up of the home life.

76
00:05:13,960 --> 00:05:18,460
 This is called bapachah, the going forth, or the leaving

77
00:05:18,460 --> 00:05:20,960
 behind, and going out from

78
00:05:20,960 --> 00:05:22,960
 the home life.

79
00:05:22,960 --> 00:05:28,340
 And so we leave behind all of the shelter of our parents,

80
00:05:28,340 --> 00:05:31,120
 of our loved ones, of our beloved

81
00:05:31,120 --> 00:05:37,330
 possessions, of our comfort and our safety and our culture

82
00:05:37,330 --> 00:05:40,160
 and all of the things which

83
00:05:40,160 --> 00:05:48,200
 are tying us down and keeping us from seeing objectively.

84
00:05:48,200 --> 00:05:53,920
 Things which lead us down the wrong path, or lead us,

85
00:05:53,920 --> 00:05:57,240
 deceive us, or make us forget the

86
00:05:57,240 --> 00:06:02,240
 truth of our lives, make us forget the truth of suffering,

87
00:06:02,240 --> 00:06:04,760
 make us forget the need to find

88
00:06:04,760 --> 00:06:14,050
 freedom, the things which intoxicate us and lead us to get

89
00:06:14,050 --> 00:06:18,840
 lost and get stuck in the swamp

90
00:06:18,840 --> 00:06:25,610
 or get stuck in the rounds of rebirth, in the ocean of

91
00:06:25,610 --> 00:06:27,200
 rebirth.

92
00:06:27,200 --> 00:06:30,050
 When we leave all of these things behind, we have only,

93
00:06:30,050 --> 00:06:31,760
 according to the Lord Buddha's

94
00:06:31,760 --> 00:06:36,400
 teaching, we have only four things which we keep as our own

95
00:06:36,400 --> 00:06:36,720
.

96
00:06:36,720 --> 00:06:39,020
 Of course in this day and age there are many more things

97
00:06:39,020 --> 00:06:40,360
 which we will use even as monks

98
00:06:40,360 --> 00:06:41,360
 and nuns.

99
00:06:41,360 --> 00:06:45,300
 But the point of these four things, even though even in the

100
00:06:45,300 --> 00:06:47,360
 Buddha's time there were many

101
00:06:47,360 --> 00:06:51,190
 more things which the monks would use, the point of these

102
00:06:51,190 --> 00:06:53,080
 four requisites is to hold

103
00:06:53,080 --> 00:06:59,560
 these things up as the core of our life, as homeless people

104
00:06:59,560 --> 00:07:00,040
.

105
00:07:00,040 --> 00:07:03,640
 But these four things are our way of living.

106
00:07:03,640 --> 00:07:06,500
 That no matter what, we will always stick by these four

107
00:07:06,500 --> 00:07:07,160
 things.

108
00:07:07,160 --> 00:07:11,810
 So the first one is food, the second one is clothing, the

109
00:07:11,810 --> 00:07:14,160
 third one is shelter and the

110
00:07:14,160 --> 00:07:17,680
 fourth one is medicine.

111
00:07:17,680 --> 00:07:21,440
 That in the end all of the things which we might use, even

112
00:07:21,440 --> 00:07:23,280
 to the point now when we use

113
00:07:23,280 --> 00:07:28,960
 computers or when we use vehicles, when we ride in vehicles

114
00:07:28,960 --> 00:07:31,440
 or when we use telephones

115
00:07:31,440 --> 00:07:34,780
 or so on, even though we might use all of these things, we

116
00:07:34,780 --> 00:07:36,280
 don't consider them to be

117
00:07:36,280 --> 00:07:38,960
 a part of our lives.

118
00:07:38,960 --> 00:07:42,560
 There are things which we use maybe to spread the Buddha's

119
00:07:42,560 --> 00:07:44,520
 teaching or to communicate or

120
00:07:44,520 --> 00:07:49,790
 keep in touch with the rest of the world which is changing

121
00:07:49,790 --> 00:07:52,640
 and evolving in its own way.

122
00:07:52,640 --> 00:07:57,020
 But we still maintain these four things as our training, as

123
00:07:57,020 --> 00:07:58,960
 our way of life, which is

124
00:07:58,960 --> 00:08:02,480
 indeed different from the way of life of other people.

125
00:08:02,480 --> 00:08:06,540
 And the difference lies in these four things, the first

126
00:08:06,540 --> 00:08:08,080
 being the food.

127
00:08:08,080 --> 00:08:11,010
 As homeless people we aren't able to go and buy our own

128
00:08:11,010 --> 00:08:12,820
 food, we aren't able to choose

129
00:08:12,820 --> 00:08:14,240
 what we will eat.

130
00:08:14,240 --> 00:08:21,370
 And we take it as a vow or as a practice that we accept

131
00:08:21,370 --> 00:08:26,200
 whatever food we get, whatever food

132
00:08:26,200 --> 00:08:29,240
 goes into our bowls, we eat that food.

133
00:08:29,240 --> 00:08:35,180
 We don't choose food because it's delicious or so on, we

134
00:08:35,180 --> 00:08:38,400
 try to be accommodating and be

135
00:08:38,400 --> 00:08:40,920
 unattached to our food.

136
00:08:40,920 --> 00:08:44,910
 And the Lord Buddha in fact said that we should accept

137
00:08:44,910 --> 00:08:47,520
 whatever scraps of food people put

138
00:08:47,520 --> 00:08:48,520
 in our bowls.

139
00:08:48,520 --> 00:08:51,870
 And he was referring to the Om's round for monks nowadays,

140
00:08:51,870 --> 00:08:53,400
 they still go through the

141
00:08:53,400 --> 00:08:56,840
 village and go on Om's round.

142
00:08:56,840 --> 00:09:01,540
 But it can also mean that we take whatever food is given to

143
00:09:01,540 --> 00:09:03,760
 us, be it in the monastery

144
00:09:03,760 --> 00:09:05,160
 or outside of the monastery.

145
00:09:05,160 --> 00:09:08,640
 And of course monks are also allowed to take food in the

146
00:09:08,640 --> 00:09:10,400
 monastery but they have to reflect

147
00:09:10,400 --> 00:09:11,400
 equally.

148
00:09:11,400 --> 00:09:15,990
 They have to reflect wherever they get their food from,

149
00:09:15,990 --> 00:09:18,520
 reflect that this is simply for

150
00:09:18,520 --> 00:09:25,500
 continuing my life, for getting rid of hunger and avoiding

151
00:09:25,500 --> 00:09:31,480
 overeating, for living in peace,

152
00:09:31,480 --> 00:09:33,780
 for living in ease, for the continuation of this body, for

153
00:09:33,780 --> 00:09:34,960
 the continuation of the holy

154
00:09:34,960 --> 00:09:38,560
 life.

155
00:09:38,560 --> 00:09:42,180
 Even in this way we won't be blameworthy, we won't be able

156
00:09:42,180 --> 00:09:43,960
 to live at peace, we won't

157
00:09:43,960 --> 00:09:47,520
 be burdened by food, we won't be burdened in the way of

158
00:09:47,520 --> 00:09:49,720
 other people as far as cooking,

159
00:09:49,720 --> 00:09:55,480
 keeping, storing, and all of the other necessities which

160
00:09:55,480 --> 00:09:59,080
 are required as a result of keeping,

161
00:09:59,080 --> 00:10:04,360
 storing and cooking food for ourselves.

162
00:10:04,360 --> 00:10:07,230
 And we take whatever donations and of course if we get no

163
00:10:07,230 --> 00:10:08,680
 food we don't eat, then we are

164
00:10:08,680 --> 00:10:14,740
 prepared and equally happy to live our day without food, if

165
00:10:14,740 --> 00:10:18,360
 there is nothing being offered.

166
00:10:18,360 --> 00:10:22,060
 This is the first requisite which we use it and we use it

167
00:10:22,060 --> 00:10:24,040
 in such a way that it doesn't

168
00:10:24,040 --> 00:10:28,760
 lead to more defilements to arise in our minds.

169
00:10:28,760 --> 00:10:31,140
 We take food to be simply something which keeps us alive

170
00:10:31,140 --> 00:10:32,960
 and allows us to continue practicing.

171
00:10:34,520 --> 00:10:35,520
 This is the first one.

172
00:10:35,520 --> 00:10:37,640
 The second one is clothing.

173
00:10:37,640 --> 00:10:43,790
 And so as monks and nuns we are separate from ordinary

174
00:10:43,790 --> 00:10:48,840
 people in that we wear a simple inexpensive

175
00:10:48,840 --> 00:10:52,320
 cast off piece of cloth.

176
00:10:52,320 --> 00:10:55,440
 Depending on the color nowadays you see many different

177
00:10:55,440 --> 00:10:57,160
 colors for the monks and for nuns

178
00:10:57,160 --> 00:10:59,960
 nowadays there are also many different colors.

179
00:10:59,960 --> 00:11:05,890
 The color of course is not important and in actuality we

180
00:11:05,890 --> 00:11:09,060
 would simply dye it some dark

181
00:11:09,060 --> 00:11:15,100
 off color and we would avoid colors which are beautiful,

182
00:11:15,100 --> 00:11:17,800
 which are attractive.

183
00:11:17,800 --> 00:11:21,280
 But whatever color the point is that it is a cast off, it

184
00:11:21,280 --> 00:11:22,960
 is a simple cloth which is

185
00:11:22,960 --> 00:11:25,980
 inexpensive and in fact for the monks they make it a point

186
00:11:25,980 --> 00:11:27,360
 of cutting it into pieces

187
00:11:27,360 --> 00:11:31,040
 first and sewing it together in pieces.

188
00:11:31,040 --> 00:11:35,840
 And for some nuns you might also see the same thing.

189
00:11:35,840 --> 00:11:40,490
 But in the end it is simply a piece of cloth which does the

190
00:11:40,490 --> 00:11:42,920
 bare minimum of protecting

191
00:11:42,920 --> 00:11:47,040
 us from heat, protecting us from cold, protecting us from

192
00:11:47,040 --> 00:11:49,680
 the bite and the sting of mosquitoes

193
00:11:49,680 --> 00:11:56,360
 and insects and all sorts of animals and small creatures.

194
00:11:56,360 --> 00:12:00,040
 And of course for covering up the parts of the body which

195
00:12:00,040 --> 00:12:01,920
 are shameful, the parts of

196
00:12:01,920 --> 00:12:08,330
 the body which have evolved through time to be a part of

197
00:12:08,330 --> 00:12:20,360
 that part of life which is shameful,

198
00:12:20,360 --> 00:12:24,710
 which is a cause for shame, which has to do with sexuality,

199
00:12:24,710 --> 00:12:27,360
 which has to do with sensuality.

200
00:12:27,360 --> 00:12:31,110
 According to the Lord Buddha's teaching in ancient times

201
00:12:31,110 --> 00:12:33,040
 before the world was perfectly

202
00:12:33,040 --> 00:12:37,320
 evolved we were beings living outside of the earth.

203
00:12:37,320 --> 00:12:40,870
 We were beings like angels up in, you could say in outer

204
00:12:40,870 --> 00:12:41,560
 space.

205
00:12:41,560 --> 00:12:44,960
 But it was in a time before there were planets really

206
00:12:44,960 --> 00:12:47,480
 evolved after the big bang but before

207
00:12:47,480 --> 00:12:50,480
 the earth had solidified.

208
00:12:50,480 --> 00:12:55,630
 And at that time beings were neither male or female and

209
00:12:55,630 --> 00:12:58,920
 there were no sexual organs, reproductive

210
00:12:58,920 --> 00:12:59,920
 organs.

211
00:12:59,920 --> 00:13:03,060
 But at the time when the earth solidified and beings began

212
00:13:03,060 --> 00:13:05,000
 to become coarser and coarser

213
00:13:05,000 --> 00:13:10,260
 and began to ingest and consume coarser and coarser food to

214
00:13:10,260 --> 00:13:13,000
 the point where they evolved

215
00:13:13,000 --> 00:13:16,880
 into the species which we now find on earth.

216
00:13:16,880 --> 00:13:20,230
 They had also developed craving for sensuality and

217
00:13:20,230 --> 00:13:23,120
 eventually what became known as sexuality

218
00:13:23,120 --> 00:13:28,870
 or the copulation between male and female or nowadays even

219
00:13:28,870 --> 00:13:31,520
 between male and male or female

220
00:13:31,520 --> 00:13:37,740
 and female or between animals of different species even to

221
00:13:37,740 --> 00:13:39,080
 this day.

222
00:13:39,080 --> 00:13:42,690
 And this is brought about by those parts of the body,

223
00:13:42,690 --> 00:13:44,920
 certain parts of the body which

224
00:13:44,920 --> 00:13:53,930
 as Buddhist monks or nuns we cover up and we keep closed

225
00:13:53,930 --> 00:13:56,880
 and we try to move away from

226
00:13:56,880 --> 00:13:59,880
 and not let them become a part of our lives.

227
00:13:59,880 --> 00:14:03,970
 So that those parts of our body which have evolved to

228
00:14:03,970 --> 00:14:06,800
 become a part of this rebirth cycle

229
00:14:06,800 --> 00:14:10,410
 so that we will again and again and again be attached to

230
00:14:10,410 --> 00:14:12,240
 whatever form of being we come

231
00:14:12,240 --> 00:14:14,080
 to be born as.

232
00:14:14,080 --> 00:14:19,030
 We come to do away with this and to become free from this

233
00:14:19,030 --> 00:14:21,720
 desire and this necessity,

234
00:14:21,720 --> 00:14:25,080
 this addiction to these parts of the body.

235
00:14:25,080 --> 00:14:31,460
 Of course these parts of the body tend to be repulsive and

236
00:14:31,460 --> 00:14:34,520
 disgusting and a cause for

237
00:14:34,520 --> 00:14:38,040
 shame.

238
00:14:38,040 --> 00:14:42,220
 And this is the second item which we use, the second

239
00:14:42,220 --> 00:14:44,440
 requisite which we use.

240
00:14:44,440 --> 00:14:47,720
 And by using it in this way we don't use it for beaut

241
00:14:47,720 --> 00:14:50,160
ification or for provoking sexual

242
00:14:50,160 --> 00:14:53,320
 desire or sensual desire in other people.

243
00:14:53,320 --> 00:14:58,530
 We don't use it as a means for beautifying or intoxicating

244
00:14:58,530 --> 00:15:01,120
 ourselves or other people.

245
00:15:01,120 --> 00:15:04,040
 And in this way we are able to do away with greed, with the

246
00:15:04,040 --> 00:15:05,640
 desire, with the lust which

247
00:15:05,640 --> 00:15:08,240
 might arise and our minds otherwise.

248
00:15:08,240 --> 00:15:10,680
 And when we are able to do away with this lust, of course

249
00:15:10,680 --> 00:15:11,920
 we live at peace, we live

250
00:15:11,920 --> 00:15:18,190
 at ease, we live without the fire, without the burning fire

251
00:15:18,190 --> 00:15:20,880
 inside, the need and the

252
00:15:20,880 --> 00:15:29,890
 success of the eventual suffering or the refuted desire,

253
00:15:29,890 --> 00:15:34,400
 not getting what we want, the suffering

254
00:15:34,400 --> 00:15:44,050
 which comes when we aren't able to obtain the thing which

255
00:15:44,050 --> 00:15:46,360
 we desire.

256
00:15:46,360 --> 00:15:49,720
 This is the second requisite which we use.

257
00:15:49,720 --> 00:15:53,120
 The third requisite is shelter.

258
00:15:53,120 --> 00:15:56,540
 And so of course everyone in the world needs some sort of

259
00:15:56,540 --> 00:15:58,160
 shelter to keep them free from

260
00:15:58,160 --> 00:16:03,920
 the, I'd say free from the elements.

261
00:16:03,920 --> 00:16:08,080
 And in the Lord Buddha the requirements for shelter are

262
00:16:08,080 --> 00:16:10,520
 that it should keep us free from

263
00:16:10,520 --> 00:16:16,360
 heat, free from cold, free from the bite and the sting of

264
00:16:16,360 --> 00:16:20,320
 mosquitoes and other small creatures.

265
00:16:20,320 --> 00:16:24,750
 And most importantly that it should bring about a state of

266
00:16:24,750 --> 00:16:27,960
 seclusion, that we should not be

267
00:16:27,960 --> 00:16:34,690
 interrupted, or we should not be bothered by society or by

268
00:16:34,690 --> 00:16:38,240
 other people, that we should

269
00:16:38,240 --> 00:16:42,390
 not have to engage as a result of our dwelling in

270
00:16:42,390 --> 00:16:46,320
 conversation or in interaction with other

271
00:16:46,320 --> 00:16:49,890
 people for the purpose that we might come to understand

272
00:16:49,890 --> 00:16:51,840
 ourselves, we might come to

273
00:16:51,840 --> 00:16:57,670
 know and to realize the truth about ourselves and the world

274
00:16:57,670 --> 00:16:59,040
 around us.

275
00:16:59,040 --> 00:17:03,960
 And so we use shelter simply as a means of creating a

276
00:17:03,960 --> 00:17:07,600
 conducive meditation environment.

277
00:17:07,600 --> 00:17:11,990
 Whereas other people might use shelter for the purpose of

278
00:17:11,990 --> 00:17:14,080
 doing bad things or for the

279
00:17:14,080 --> 00:17:18,990
 purpose of luxury or the purpose of indulgence and this or

280
00:17:18,990 --> 00:17:20,600
 that pleasure.

281
00:17:20,600 --> 00:17:24,990
 The Lord Buddha recommended that we live at the foot of a

282
00:17:24,990 --> 00:17:27,200
 tree, that we go and find a

283
00:17:27,200 --> 00:17:33,830
 tree with big leaves and with a good shelter from the sun

284
00:17:33,830 --> 00:17:35,760
 and the rain.

285
00:17:35,760 --> 00:17:38,190
 And we live at the foot of the tree and practice meditation

286
00:17:38,190 --> 00:17:39,680
 either walking or sitting at the

287
00:17:39,680 --> 00:17:40,680
 foot of the tree.

288
00:17:40,680 --> 00:17:43,530
 And in this way it was that the Lord Buddha himself became

289
00:17:43,530 --> 00:17:44,400
 enlightened.

290
00:17:44,400 --> 00:17:54,760
 So it's considered to be a bare necessity for shelter.

291
00:17:54,760 --> 00:17:59,560
 At the foot of a tree the Lord Buddha also allowed monks

292
00:17:59,560 --> 00:18:02,080
 and nuns to live in huts, to

293
00:18:02,080 --> 00:18:04,600
 live in buildings.

294
00:18:04,600 --> 00:18:07,360
 He said there's no problem as long as we use these things

295
00:18:07,360 --> 00:18:09,960
 and understood how to use them.

296
00:18:09,960 --> 00:18:13,180
 So when we use these things correctly, when we use them for

297
00:18:13,180 --> 00:18:14,800
 their correct purpose, and

298
00:18:14,800 --> 00:18:18,440
 we wouldn't use our meditation hut as a place for social

299
00:18:18,440 --> 00:18:20,840
 gathering or as a place to accumulate

300
00:18:20,840 --> 00:18:26,720
 belongings or as a place to indulge in secret activities,

301
00:18:26,720 --> 00:18:30,000
 secret indulgences or breaking

302
00:18:30,000 --> 00:18:32,560
 precepts or so on.

303
00:18:32,560 --> 00:18:36,390
 We use it instead for seclusion and as a place where we

304
00:18:36,390 --> 00:18:38,880
 might be able to practice to gain

305
00:18:38,880 --> 00:18:42,860
 repass and insight and to see clearly about ourselves and

306
00:18:42,860 --> 00:18:44,480
 the world around us.

307
00:18:44,480 --> 00:18:48,160
 And then in this way we'll be able to become free from our

308
00:18:48,160 --> 00:18:49,320
 attachments.

309
00:18:49,320 --> 00:18:52,410
 Again this is important as we have to consider ourselves to

310
00:18:52,410 --> 00:18:53,320
 be homeless.

311
00:18:53,320 --> 00:18:56,540
 That we might have a kuti, we might have a hut, we might

312
00:18:56,540 --> 00:18:58,280
 have a place to live, but we

313
00:18:58,280 --> 00:19:01,120
 don't hold on to it or take it to be me or to be mine.

314
00:19:01,120 --> 00:19:04,580
 Or we don't use it in the way that an ordinary person might

315
00:19:04,580 --> 00:19:06,360
 use it having all of the many

316
00:19:06,360 --> 00:19:09,400
 belongings which ordinary people might have.

317
00:19:09,400 --> 00:19:15,540
 We use it instead for the purpose of practicing and keeping

318
00:19:15,540 --> 00:19:24,280
 all sorts of dhamma books or books

319
00:19:24,280 --> 00:19:29,090
 on the Lord Buddha's teaching or so on, but not for the

320
00:19:29,090 --> 00:19:32,280
 purpose of any sort of indulgence

321
00:19:32,280 --> 00:19:36,920
 and worldly affairs.

322
00:19:36,920 --> 00:19:38,200
 This is the third requisite.

323
00:19:38,200 --> 00:19:44,080
 The fourth requisite which is a method through using it we

324
00:19:44,080 --> 00:19:47,360
 can become free from the asavas

325
00:19:47,360 --> 00:19:49,400
 is medicine.

326
00:19:49,400 --> 00:19:53,820
 The medicine is very important because nowadays we find

327
00:19:53,820 --> 00:19:56,800
 that medicine is actually being used

328
00:19:56,800 --> 00:20:01,660
 as a means to avoid the necessity of purifying our minds,

329
00:20:01,660 --> 00:20:04,800
 avoid the necessity of practicing.

330
00:20:04,800 --> 00:20:06,760
 And nowadays it even goes beyond medicine.

331
00:20:06,760 --> 00:20:14,680
 It's come to all sorts of many different means.

332
00:20:14,680 --> 00:20:21,760
 Nowadays there are meditation tapes and meditation videos

333
00:20:21,760 --> 00:20:26,680
 and meditation sounds, meditation techniques.

334
00:20:26,680 --> 00:20:31,060
 Which allow us to avoid the necessity of purifying our

335
00:20:31,060 --> 00:20:31,920
 minds.

336
00:20:31,920 --> 00:20:35,480
 I've had many people ask me about these tapes and videos

337
00:20:35,480 --> 00:20:37,800
 and even just audio CDs with sounds

338
00:20:37,800 --> 00:20:44,560
 which create a meditative state of enlightenment, they say.

339
00:20:44,560 --> 00:20:47,220
 And these fall into the same category as some sort of

340
00:20:47,220 --> 00:20:47,960
 medicine.

341
00:20:47,960 --> 00:20:54,430
 These are thought to be something which will cure us of our

342
00:20:54,430 --> 00:20:56,120
 suffering.

343
00:20:56,120 --> 00:21:00,900
 Whether it be a pill which we ingest or whether it be a

344
00:21:00,900 --> 00:21:04,120
 tape which we listen to or there'd

345
00:21:04,120 --> 00:21:09,410
 be any sort of mechanism which is meant to bring about

346
00:21:09,410 --> 00:21:12,760
 eternal peace and happiness.

347
00:21:12,760 --> 00:21:15,730
 We have to understand the benefit of these things and the

348
00:21:15,730 --> 00:21:17,160
 limit of these things.

349
00:21:17,160 --> 00:21:20,880
 The benefit is that they do bring peace and happiness.

350
00:21:20,880 --> 00:21:23,480
 Even many different kinds of medicine, herbal medicine or

351
00:21:23,480 --> 00:21:24,960
 even western medicine can bring

352
00:21:24,960 --> 00:21:31,240
 about freedom from sickness, freedom from discomfort,

353
00:21:31,240 --> 00:21:35,200
 acupuncture, massage and so on.

354
00:21:35,200 --> 00:21:38,200
 But none of these things can claim to be eternal because

355
00:21:38,200 --> 00:21:40,040
 they can always be overrided by a

356
00:21:40,040 --> 00:21:42,040
 future action.

357
00:21:42,040 --> 00:21:45,830
 Since we can heal our back only to go out and heal our back

358
00:21:45,830 --> 00:21:47,640
 from any kind of back ache

359
00:21:47,640 --> 00:21:51,140
 or back problem, only to go out and throw our back out

360
00:21:51,140 --> 00:21:53,560
 again or hurt our back in a different

361
00:21:53,560 --> 00:21:57,310
 way or when we pass away from this life to be reborn

362
00:21:57,310 --> 00:21:59,880
 somewhere else with the same bad

363
00:21:59,880 --> 00:22:03,670
 back depending on the karma which we have performed in past

364
00:22:03,670 --> 00:22:04,360
 lives.

365
00:22:04,360 --> 00:22:07,320
 As an example, when we have sicknesses in this way or that

366
00:22:07,320 --> 00:22:08,520
 way and we find a way to

367
00:22:08,520 --> 00:22:11,690
 overcome them, we can only hope to ever hope to overcome

368
00:22:11,690 --> 00:22:13,480
 them to the point where we pass

369
00:22:13,480 --> 00:22:14,480
 away from this lifetime.

370
00:22:14,480 --> 00:22:18,120
 When we are born again, we will create a new body.

371
00:22:18,120 --> 00:22:21,480
 A new body will be created based on these very same

372
00:22:21,480 --> 00:22:24,040
 structures which exist in our mind,

373
00:22:24,040 --> 00:22:28,580
 these very same, you could say, genetic structures which we

374
00:22:28,580 --> 00:22:31,280
 have built up and are carrying forth

375
00:22:31,280 --> 00:22:36,920
 with us even to the point of death and beyond.

376
00:22:36,920 --> 00:22:40,450
 And so it's important to realize that anything which brings

377
00:22:40,450 --> 00:22:42,400
 about a state of peace and comfort

378
00:22:42,400 --> 00:22:45,990
 or happiness in the body or in the mind, that it can't hope

379
00:22:45,990 --> 00:22:47,120
 to be eternal.

380
00:22:47,120 --> 00:22:51,130
 It can't hope to be permanent without something else which

381
00:22:51,130 --> 00:22:52,360
 is permanent.

382
00:22:52,360 --> 00:22:57,800
 And the permanent cure in this case is wisdom.

383
00:22:57,800 --> 00:23:03,010
 When we see and when we know the truth about suffering and

384
00:23:03,010 --> 00:23:05,840
 the cause of suffering, then

385
00:23:05,840 --> 00:23:09,350
 anything which comes to us, whether it be an unpleasant

386
00:23:09,350 --> 00:23:11,040
 sensation in the body or an

387
00:23:11,040 --> 00:23:14,110
 unpleasant sensation in the mind, we'll be able to deal

388
00:23:14,110 --> 00:23:15,760
 with it and understand what are

389
00:23:15,760 --> 00:23:20,300
 the causes and conditions of suffering that when we see or

390
00:23:20,300 --> 00:23:22,640
 hear or smell or taste or feel

391
00:23:22,640 --> 00:23:34,070
 something unpleasant, that it is the next step which is our

392
00:23:34,070 --> 00:23:38,240
 craving, our attachment,

393
00:23:38,240 --> 00:23:41,180
 our desire for it to be this way or that way, for something

394
00:23:41,180 --> 00:23:42,920
 to continue on or for something

395
00:23:42,920 --> 00:23:46,420
 to cease, it is that itself which leads to suffering, not

396
00:23:46,420 --> 00:23:47,640
 the object itself.

397
00:23:47,640 --> 00:23:51,680
 The objects of the sense are simply things which arise and

398
00:23:51,680 --> 00:23:53,640
 cease and have no inherent

399
00:23:53,640 --> 00:23:55,440
 power to make us suffer.

400
00:23:55,440 --> 00:23:58,620
 And we come to see that sickness or affliction, whether it

401
00:23:58,620 --> 00:24:00,520
 exists in the body or in the mind,

402
00:24:00,520 --> 00:24:03,840
 that it can't actually lead us to suffer.

403
00:24:03,840 --> 00:24:07,320
 Because it itself is not a cause for suffering, the cause

404
00:24:07,320 --> 00:24:09,360
 for suffering is in our reaction

405
00:24:09,360 --> 00:24:14,160
 to these things.

406
00:24:14,160 --> 00:24:17,880
 And so the Lord Buddha prescribed that we use medicine for

407
00:24:17,880 --> 00:24:19,720
 the purpose of bringing about

408
00:24:19,720 --> 00:24:23,960
 comfort, for doing away with painful feelings which might

409
00:24:23,960 --> 00:24:26,680
 get in the way of meditation practice.

410
00:24:26,680 --> 00:24:31,280
 But that in the end we use them simply for what they are,

411
00:24:31,280 --> 00:24:34,080
 we use them as a means to continue

412
00:24:34,080 --> 00:24:35,680
 our practice.

413
00:24:35,680 --> 00:24:39,160
 And if there is something which is getting in the way of

414
00:24:39,160 --> 00:24:41,320
 our practice or would possibly

415
00:24:41,320 --> 00:24:44,280
 get in the way of our practice in the future, that we take

416
00:24:44,280 --> 00:24:46,240
 whatever means necessary to prevent

417
00:24:46,240 --> 00:24:51,190
 it or to ameliorate the condition, to allow us to continue

418
00:24:51,190 --> 00:24:53,360
 on our path to become free

419
00:24:53,360 --> 00:24:55,520
 from suffering.

420
00:24:55,520 --> 00:24:58,040
 And this is the fourth necessity.

421
00:24:58,040 --> 00:25:00,860
 So these four things, this is what the Lord Buddha called "

422
00:25:00,860 --> 00:25:02,400
pati seva na pahata paada"

423
00:25:02,400 --> 00:25:05,230
 by using these four things in the correct way, the four

424
00:25:05,230 --> 00:25:07,240
 requisites of a Buddhist monk or

425
00:25:07,240 --> 00:25:12,330
 nun, that these can be a means of becoming free from the as

426
00:25:12,330 --> 00:25:13,160
a vah.

427
00:25:13,160 --> 00:25:16,300
 This is a means which we should try, whether we are monks

428
00:25:16,300 --> 00:25:18,080
 or nuns or people living in the

429
00:25:18,080 --> 00:25:21,480
 world, we should try to emulate as we can.

430
00:25:21,480 --> 00:25:25,490
 That maybe if we are a lay person living in the world, it

431
00:25:25,490 --> 00:25:27,520
 may be very difficult for us

432
00:25:27,520 --> 00:25:28,520
 to come to this state.

433
00:25:28,520 --> 00:25:33,440
 We can't go around wearing rag robes, we can't simply eat

434
00:25:33,440 --> 00:25:35,720
 the food from alms round.

435
00:25:35,720 --> 00:25:42,610
 We can't live at the foot of a tree or in the forest as a

436
00:25:42,610 --> 00:25:44,800
 monk or a nun.

437
00:25:44,800 --> 00:25:50,680
 We can't live our lives in the same exact way.

438
00:25:50,680 --> 00:25:55,320
 But we can still emulate the life of a monk or a nun.

439
00:25:55,320 --> 00:25:59,310
 We can try our best to give up the things which are not

440
00:25:59,310 --> 00:26:01,680
 necessary for us using all of the

441
00:26:01,680 --> 00:26:05,250
 things around us which we find ourselves caught up in

442
00:26:05,250 --> 00:26:07,840
 simply for the purpose which they are

443
00:26:07,840 --> 00:26:10,640
 meant for and nothing more.

444
00:26:10,640 --> 00:26:15,670
 Not letting them intoxicate us or create states of sensual

445
00:26:15,670 --> 00:26:18,960
 desire or sensual lust, which would

446
00:26:18,960 --> 00:26:23,460
 later be a cause for affection and subsequent suffering for

447
00:26:23,460 --> 00:26:23,920
 us.

448
00:26:23,920 --> 00:26:28,330
 Whatever food we eat, we eat it mindfully and we simply use

449
00:26:28,330 --> 00:26:28,840
 it.

450
00:26:28,840 --> 00:26:32,560
 In the end, of course, it comes down only to mindfulness.

451
00:26:32,560 --> 00:26:35,700
 Whatever we use, when we put on our robes, we put them on

452
00:26:35,700 --> 00:26:37,360
 mindfully and we wear them

453
00:26:37,360 --> 00:26:38,360
 mindfully.

454
00:26:38,360 --> 00:26:41,230
 When we like them or we attach to them or when we don't

455
00:26:41,230 --> 00:26:43,040
 like them, we become aware of

456
00:26:43,040 --> 00:26:46,970
 this state of mind that it's something which is arising in

457
00:26:46,970 --> 00:26:49,360
 us and is impermanent, is suffering,

458
00:26:49,360 --> 00:26:51,480
 is non-self and not holding on to it.

459
00:26:51,480 --> 00:26:55,080
 And when we're able to let go in this way, when we're able

460
00:26:55,080 --> 00:26:56,720
 to see clearly in this way

461
00:26:56,720 --> 00:27:00,250
 about the things which we use, we're not becoming attached

462
00:27:00,250 --> 00:27:03,960
 or excited or repulsed by them, simply

463
00:27:03,960 --> 00:27:06,880
 using them for the purpose which they're meant for.

464
00:27:06,880 --> 00:27:10,400
 Then we can become free from our attachments to this world.

465
00:27:10,400 --> 00:27:12,980
 It's another important point is that not only are we

466
00:27:12,980 --> 00:27:14,840
 dealing with the six senses, we're

467
00:27:14,840 --> 00:27:18,420
 also dealing with conceptual things, the things which we

468
00:27:18,420 --> 00:27:21,280
 use, one which we wear, which we consume,

469
00:27:21,280 --> 00:27:24,800
 which we reside in.

470
00:27:24,800 --> 00:27:29,760
 We're using them for the correct purpose and nothing else.

471
00:27:29,760 --> 00:27:33,190
 When we use them and consider carefully as we use them,

472
00:27:33,190 --> 00:27:35,400
 then we will find ourselves using

473
00:27:35,400 --> 00:27:39,210
 them in the correct way and in a way that will lead us to

474
00:27:39,210 --> 00:27:41,240
 peace and to happiness and

475
00:27:41,240 --> 00:27:44,240
 freedom from suffering in this very life.

476
00:27:44,240 --> 00:27:47,920
 So this is another important point for us to keep in mind

477
00:27:47,920 --> 00:27:49,240
 in our practice.

478
00:27:49,240 --> 00:27:51,240
 And that's all for today.

479
00:27:51,240 --> 00:27:53,640
 Now I'll do some actual practice and be walking instantly

480
00:27:53,640 --> 00:27:54,240
 together.

481
00:27:54,240 --> 00:28:01,240
 [

