1
00:00:00,000 --> 00:00:08,550
 Can you talk about right concentration? Right concentration

2
00:00:08,550 --> 00:00:09,880
 is any concentration,

3
00:00:09,880 --> 00:00:15,800
 any, there's a quote of the Buddha, it's any wholesome,

4
00:00:15,800 --> 00:00:21,280
 beneficial, oneness, single-pointedness

5
00:00:21,280 --> 00:00:26,920
 of mind that is free from the five hindrances which are

6
00:00:26,920 --> 00:00:30,960
 liking, disliking, drowsiness,

7
00:00:30,960 --> 00:00:34,460
 distraction and doubt. If the mind is free from those then

8
00:00:34,460 --> 00:00:36,040
 this is considered to be right

9
00:00:36,040 --> 00:00:39,450
 concentration. Now as I talked about yesterday in terms of

10
00:00:39,450 --> 00:00:42,080
 the Eightfold Noble Path true right

11
00:00:42,080 --> 00:00:44,970
 concentration is only that one moment or two moments or

12
00:00:44,970 --> 00:00:46,960
 whatever it's the moment no it's one

13
00:00:46,960 --> 00:00:52,020
 moment where the mind is leaving samsara or has entered

14
00:00:52,020 --> 00:00:57,400
 into nibbana for the first time. But we

15
00:00:57,400 --> 00:01:03,750
 get to that point by abandoning the five hindrances. So at

16
00:01:03,750 --> 00:01:06,440
 the moment of the maga nyana,

17
00:01:06,440 --> 00:01:10,690
 then the knowledge of the path, maga means path, it's the

18
00:01:10,690 --> 00:01:13,240
 moment when the Eightfold Noble Path

19
00:01:13,240 --> 00:01:17,640
 comes into full effect and at that moment we are said to

20
00:01:17,640 --> 00:01:21,280
 have right concentration and in the moments

21
00:01:21,280 --> 00:01:24,590
 that follow which are the moments of palanyana where the

22
00:01:24,590 --> 00:01:27,100
 person stays in nibbana and experiences

23
00:01:27,100 --> 00:01:35,750
 nibbanaic freedom. That's right concentration. Simple

24
00:01:35,750 --> 00:01:38,000
 answer, I'm not gonna go, let's not go

25
00:01:38,000 --> 00:01:43,640
 any further than that, keep it simple.

