1
00:00:00,000 --> 00:00:02,760
 I heard a Dhamma talk about the middle way.

2
00:00:02,760 --> 00:00:05,680
 A professor suggested the middle way should be applied to

3
00:00:05,680 --> 00:00:06,880
 Buddhism itself.

4
00:00:06,880 --> 00:00:07,880
 E.G.

5
00:00:07,880 --> 00:00:12,520
 "Not taking precepts too seriously, alcohol now and again,

6
00:00:12,520 --> 00:00:14,120
 not meditating too much, etc."

7
00:00:14,120 --> 00:00:17,840
 That's not the middle way, by the way.

8
00:00:17,840 --> 00:00:18,840
 That's the halfway.

9
00:00:18,840 --> 00:00:23,000
 It's quite different.

10
00:00:23,000 --> 00:00:25,320
 There is no halfway.

11
00:00:25,320 --> 00:00:29,320
 Halfway is pointless, useless.

12
00:00:29,320 --> 00:00:31,000
 You can think about it.

13
00:00:31,000 --> 00:00:39,270
 If something is the right thing to do and brings benefit,

14
00:00:39,270 --> 00:00:43,520
 well, you might say that you

15
00:00:43,520 --> 00:00:48,960
 have to pace yourself.

16
00:00:48,960 --> 00:00:54,240
 If alcohol is wrong, what good could come of indulging in

17
00:00:54,240 --> 00:00:56,200
 it once in a while?

18
00:00:56,200 --> 00:01:03,000
 I think the intention here is good.

19
00:01:03,000 --> 00:01:07,830
 The idea of not pushing yourself too hard, for example,

20
00:01:07,830 --> 00:01:09,800
 forcing something on you.

21
00:01:09,800 --> 00:01:14,440
 If you meditate too much without proper appreciation of the

22
00:01:14,440 --> 00:01:17,320
 meditation practice, if you don't

23
00:01:17,320 --> 00:01:22,480
 really want to meditate, meditating a lot is only so

24
00:01:22,480 --> 00:01:24,080
 beneficial.

25
00:01:24,080 --> 00:01:27,630
 If you're not really interested in doing something, it's

26
00:01:27,630 --> 00:01:29,480
 not really so beneficial.

27
00:01:29,480 --> 00:01:34,120
 Sometimes it's good to appreciate things before you engage

28
00:01:34,120 --> 00:01:35,000
 in them.

29
00:01:35,000 --> 00:01:39,520
 Simply pushing yourself is not of great benefit.

30
00:01:39,520 --> 00:01:42,850
 Sometimes you want to explore the reasons for doing things

31
00:01:42,850 --> 00:01:44,420
 and the reasons for giving

32
00:01:44,420 --> 00:01:45,420
 up things.

33
00:01:45,420 --> 00:01:49,570
 For example, some people have claimed beneficial results

34
00:01:49,570 --> 00:01:52,400
 from getting drunk or drinking alcohol

35
00:01:52,400 --> 00:01:56,030
 in the sense that as meditators it helps them realize how

36
00:01:56,030 --> 00:01:59,080
 ridiculous it is to drink alcohol.

37
00:01:59,080 --> 00:02:01,620
 If they were to just blindly accept the precept, then they

38
00:02:01,620 --> 00:02:03,000
 wouldn't really understand why it's

39
00:02:03,000 --> 00:02:04,000
 awful.

40
00:02:04,000 --> 00:02:12,680
 It's the theory.

41
00:02:12,680 --> 00:02:20,000
 The intention, the idea that somehow one should not blindly

42
00:02:20,000 --> 00:02:23,680
 race ahead into keeping all of

43
00:02:23,680 --> 00:02:26,360
 these rules or becoming a monk or doing intensive

44
00:02:26,360 --> 00:02:28,860
 meditation, I think the intention there is

45
00:02:28,860 --> 00:02:34,520
 a good one.

46
00:02:34,520 --> 00:02:40,260
 The problem is it's a conflation of this with another

47
00:02:40,260 --> 00:02:44,000
 concept and that is of regression

48
00:02:44,000 --> 00:02:51,480
 or the degradation of the mind.

49
00:02:51,480 --> 00:02:54,660
 Every time you drink alcohol, you degrade your state of

50
00:02:54,660 --> 00:02:56,920
 mind, your awareness, your clarity

51
00:02:56,920 --> 00:02:59,720
 of mind.

52
00:02:59,720 --> 00:03:03,460
 Someone who is serious in meditation will be revolted at

53
00:03:03,460 --> 00:03:05,120
 the very thought of drinking

54
00:03:05,120 --> 00:03:09,880
 alcohol.

55
00:03:09,880 --> 00:03:14,470
 The idea that the middle way could somehow be alcohol now

56
00:03:14,470 --> 00:03:16,600
 and then is absurd to such

57
00:03:16,600 --> 00:03:18,560
 a person.

58
00:03:18,560 --> 00:03:22,000
 Any alcohol whatsoever is just a revolting thought.

59
00:03:22,000 --> 00:03:26,400
 It's the one's whole being revolts against the idea.

60
00:03:26,400 --> 00:03:32,620
 Stealing, stealing, lying, cheating, drugs and alcohol have

61
00:03:32,620 --> 00:03:36,840
 been totally given up 100%.

62
00:03:36,840 --> 00:03:40,280
 The middle way is not halfway.

63
00:03:40,280 --> 00:03:46,090
 In those senses, some people who will say, "I've heard one

64
00:03:46,090 --> 00:03:48,880
 man say engaging in tantric

65
00:03:48,880 --> 00:03:56,080
 sex really helped him to come to terms with sexuality."

66
00:03:56,080 --> 00:03:59,370
 I thought about that for quite a while and it's a little

67
00:03:59,370 --> 00:04:01,160
 bit difficult to refute.

68
00:04:01,160 --> 00:04:04,300
 Even though deep down you know there's something wrong with

69
00:04:04,300 --> 00:04:04,760
 that.

70
00:04:04,760 --> 00:04:09,960
 What's wrong with it is that at the moment of indulging in

71
00:04:09,960 --> 00:04:13,160
 sexuality, you're cultivating

72
00:04:13,160 --> 00:04:15,680
 attachment to pleasure and addiction and so on.

73
00:04:15,680 --> 00:04:18,440
 You're cultivating these pleasurable states.

74
00:04:18,440 --> 00:04:22,640
 You're encouraging them.

75
00:04:22,640 --> 00:04:24,280
 That has an effect on the mind.

76
00:04:24,280 --> 00:04:25,880
 It deteriorates the state of mind.

77
00:04:25,880 --> 00:04:31,420
 So what helps in dealing with sexuality is the objective

78
00:04:31,420 --> 00:04:34,600
 observation of the sexual urge

79
00:04:34,600 --> 00:04:35,760
 when it arises.

80
00:04:35,760 --> 00:04:42,960
 The sexual urge, the pleasurable sensations and so on, the

81
00:04:42,960 --> 00:04:45,760
 physical stimulus.

82
00:04:45,760 --> 00:04:52,800
 That the actual intention to engage in, for example, tant

83
00:04:52,800 --> 00:04:56,120
ric sex or sex in general is going

84
00:04:56,120 --> 00:05:03,160
 to degrade one's state of mind.

85
00:05:03,160 --> 00:05:11,620
 The cultivation of the desires and the passion actually

86
00:05:11,620 --> 00:05:15,280
 hurts one's clarity.

87
00:05:15,280 --> 00:05:17,800
 If one is truly mindful, one can't engage in it.

88
00:05:17,800 --> 00:05:23,460
 One can't give rise to sexual urges as being mindful.

89
00:05:23,460 --> 00:05:26,710
 This is what you see when you start to be mindful is that

90
00:05:26,710 --> 00:05:28,640
 as soon as you cultivate clear

91
00:05:28,640 --> 00:05:33,120
 awareness, the urge disappears.

92
00:05:33,120 --> 00:05:36,530
 Suddenly it's no longer because this is what I mean by

93
00:05:36,530 --> 00:05:39,360
 saying you can fundamentally misunderstand

94
00:05:39,360 --> 00:05:46,720
 reality or misinterpret reality.

95
00:05:46,720 --> 00:05:50,510
 All it takes is a moment of clarity for you to see things

96
00:05:50,510 --> 00:05:52,800
 suddenly like a veil was lifted

97
00:05:52,800 --> 00:05:59,880
 from your eyes.

98
00:05:59,880 --> 00:06:06,120
 For example, this sexuality and the example of drugs and

99
00:06:06,120 --> 00:06:09,440
 alcohol as well, they cannot

100
00:06:09,440 --> 00:06:14,560
 be indulged in by someone who is truly mindful.

101
00:06:14,560 --> 00:06:25,280
 The middle way is quite different from a halfway, taking

102
00:06:25,280 --> 00:06:27,000
 things halfway.

103
00:06:27,000 --> 00:06:30,720
 It's totally, totally different.

104
00:06:30,720 --> 00:06:33,720
 I've talked about this before.

105
00:06:33,720 --> 00:06:38,640
 It's actually not really the concept of being in the middle

106
00:06:38,640 --> 00:06:39,000
.

107
00:06:39,000 --> 00:06:45,870
 It is not really a core doctrine in the sense that it's not

108
00:06:45,870 --> 00:06:50,360
 something that the Buddha insisted

109
00:06:50,360 --> 00:06:55,280
 upon or reiterated frequently.

110
00:06:55,280 --> 00:06:58,230
 It's not something that the Buddha taught on a very

111
00:06:58,230 --> 00:06:59,480
 frequent basis.

112
00:06:59,480 --> 00:07:04,070
 It happens to be quite famous because it was the first

113
00:07:04,070 --> 00:07:05,400
 discourse.

114
00:07:05,400 --> 00:07:07,500
 One important point that you have to keep in mind is the

115
00:07:07,500 --> 00:07:09,240
 Buddha's audience at the time.

116
00:07:09,240 --> 00:07:15,310
 He was dealing with people who were torturing themselves,

117
00:07:15,310 --> 00:07:18,440
 who were doing something that

118
00:07:18,440 --> 00:07:24,880
 was leading them to an extreme state.

119
00:07:24,880 --> 00:07:27,630
 He framed his teaching on the Four Noble Truths, which he

120
00:07:27,630 --> 00:07:29,320
 couldn't just go into teaching the

121
00:07:29,320 --> 00:07:31,800
 Four Noble Truths right away.

122
00:07:31,800 --> 00:07:36,800
 They weren't in a position to appreciate the path that he

123
00:07:36,800 --> 00:07:39,240
 was going to explain until he

124
00:07:39,240 --> 00:07:45,960
 pointed out that the state that they were in was an extreme

125
00:07:45,960 --> 00:07:47,160
 state.

126
00:07:47,160 --> 00:07:51,440
 The middle way, first of all, just this, is that it was

127
00:07:51,440 --> 00:07:53,120
 specifically directed towards

128
00:07:53,120 --> 00:07:56,160
 these five ascetics and towards adjusting their minds

129
00:07:56,160 --> 00:07:57,880
 because they were not like any

130
00:07:57,880 --> 00:07:58,880
 of us.

131
00:07:58,880 --> 00:08:01,000
 They were totally extreme.

132
00:08:01,000 --> 00:08:06,160
 They were hardcore ascetics, torturing themselves, not

133
00:08:06,160 --> 00:08:07,920
 eating and so on.

134
00:08:07,920 --> 00:08:11,960
 The Buddha was explaining that this was an extreme to them.

135
00:08:11,960 --> 00:08:16,390
 Then going into what was the more important Buddhist

136
00:08:16,390 --> 00:08:19,920
 doctrine, that everything that arises

137
00:08:19,920 --> 00:08:26,120
 ceases, that there is no happiness to be found in the

138
00:08:26,120 --> 00:08:30,720
 objects of experience, that it is all

139
00:08:30,720 --> 00:08:36,260
 dukkha, it's all suffering, that the first noble truth is

140
00:08:36,260 --> 00:08:39,120
 to see and to experience things

141
00:08:39,120 --> 00:08:42,400
 as they are and to give them up.

142
00:08:42,400 --> 00:08:46,480
 What it means, the middle way is quite clear.

143
00:08:46,480 --> 00:08:53,480
 It's the avoiding of two states and that is indulgence and

144
00:08:53,480 --> 00:08:58,200
 repression or rather than repression

145
00:08:58,200 --> 00:09:07,580
 because that's a western word, you might say rejection

146
00:09:07,580 --> 00:09:10,080
 because this is really what asceticism

147
00:09:10,080 --> 00:09:11,080
 was all about.

148
00:09:11,080 --> 00:09:15,720
 Rejection of what they saw as an extreme state, the state

149
00:09:15,720 --> 00:09:18,840
 of hedonism, of sensual indulgence

150
00:09:18,840 --> 00:09:21,480
 which totally corrupts the mind.

151
00:09:21,480 --> 00:09:23,760
 They thought, "Well, if that corrupts the mind, then you

152
00:09:23,760 --> 00:09:24,840
 have to go to the other way

153
00:09:24,840 --> 00:09:29,480
 and beat it out of yourself, torture yourself."

154
00:09:29,480 --> 00:09:36,390
 If happiness and pleasure is what leads to defilement, then

155
00:09:36,390 --> 00:09:40,000
 the opposite, torture, must

156
00:09:40,000 --> 00:09:43,180
 be what leads to enlightenment, not actually intentionally

157
00:09:43,180 --> 00:09:45,040
 causing suffering on yourself.

158
00:09:45,040 --> 00:09:47,750
 The middle way is the avoidance of both of these things or

159
00:09:47,750 --> 00:09:49,560
 in a western sense, the avoidance

160
00:09:49,560 --> 00:09:51,960
 of indulgence and repression.

161
00:09:51,960 --> 00:09:55,070
 Not indulging in things but not repression, simply

162
00:09:55,070 --> 00:09:57,280
 experiencing them for what they are

163
00:09:57,280 --> 00:10:01,000
 is the best way to understand the middle way.

164
00:10:01,000 --> 00:10:04,260
 Now, the practice of the middle way, we only get to the

165
00:10:04,260 --> 00:10:06,160
 middle way when we are totally

166
00:10:06,160 --> 00:10:16,110
 extreme in our practice, in the sense of practicing to the

167
00:10:16,110 --> 00:10:18,640
 utmost degree.

168
00:10:18,640 --> 00:10:23,310
 Getting to the point where everything is practice, where

169
00:10:23,310 --> 00:10:26,520
 every moment of our waking life or every

170
00:10:26,520 --> 00:10:31,320
 moment of our lives, day and night, is meditative, is the

171
00:10:31,320 --> 00:10:34,280
 cultivation of clear awareness.

172
00:10:34,280 --> 00:10:37,080
 That's what leads to enlightenment.

173
00:10:37,080 --> 00:10:38,280
 It's actually quite extreme.

174
00:10:38,280 --> 00:10:43,620
 It's not a halfway moderate path.

175
00:10:43,620 --> 00:10:50,600
 It's the middle way, which is the eightfold noble way.

176
00:10:50,600 --> 00:10:54,680
 The middle way is that way.

177
00:10:54,680 --> 00:11:01,010
 It has to do with our reaction and our interaction with

178
00:11:01,010 --> 00:11:02,920
 experience.

179
00:11:02,920 --> 00:11:06,200
 That's what is meant by the middle way, not a lifestyle.

180
00:11:06,200 --> 00:11:08,240
 It has nothing to do with our lifestyle.

181
00:11:08,240 --> 00:11:13,050
 We should pick the lifestyle which allows us to give up

182
00:11:13,050 --> 00:11:15,640
 everything but this reaction

183
00:11:15,640 --> 00:11:16,640
 to experience.

184
00:11:16,640 --> 00:11:19,480
 So you can see how ridiculous it would be to say alcohol in

185
00:11:19,480 --> 00:11:20,840
 moderation because it's

186
00:11:20,840 --> 00:11:22,640
 totally inapplicable.

187
00:11:22,640 --> 00:11:25,270
 The middle way has nothing to do with drinking alcohol or

188
00:11:25,270 --> 00:11:26,520
 not drinking alcohol.

189
00:11:26,520 --> 00:11:29,920
 It has to do with the experience of reality, which is

190
00:11:29,920 --> 00:11:32,260
 obviously hindered by the taking

191
00:11:32,260 --> 00:11:34,260
 of alcohol.

192
00:11:34,260 --> 00:11:38,010
 It's just a misunderstanding because people tend to

193
00:11:38,010 --> 00:11:40,520
 intellectualize Buddhism and try to

194
00:11:40,520 --> 00:11:45,470
 incorporate it into their own delusions and their own lives

195
00:11:45,470 --> 00:11:47,920
, which are very much caught

196
00:11:47,920 --> 00:11:51,550
 up in samsara as opposed to trying to become free from sams

197
00:11:51,550 --> 00:11:52,080
ara.

198
00:11:52,080 --> 00:12:18,660
 [

