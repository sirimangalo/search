 Just two days after the earthquake.
 Now we come to the description of the elements, page 552.
 Here, Nyanamali made many mistakes.
 It is almost impossible to follow him.
 So I made this sheet.
 With the help of this sheet, I'll explain to you.
 The final word for element is dha-tu.
 The word dha-tu is familiar to you, like I hope.
 The meaning of the word dha-tu is explained in nine ways,
 actually.
 One is a little different from the other.
 Nyanamali misunderstood some words so much that it's
 impossible to follow him.
 So the first meaning is those which make or create
 suffering in samsara.
 Now, he uses sort out, sort out.
 I don't know what is the meaning of sort out.
 To sort out means what? To create or to make?
 Sort out?
 Yeah.
 Paragraph 20.
 The mundane sorts elements when defined according to their
 instrumentality.
 Sort out, with the hanti and suffering of the Raunavi birth
 and so on.
 We have to seek to find or to sort out, you know, separate.
 Oh, I just, yeah, pick out the new one.
 Look for.
 I see.
 That is not the meaning of the value word here.
 The value word here is to make or to create.
 So the first meaning is they are called dha-tu's because
 they make or they create.
 They make, they create means they create suffering in sams
ara.
 Now, the Pali word dha-tu is composed of the root dha and
 the suffix tu.
 So dha has many meanings and in the first explanation,
 it has the meaning of making or creating.
 So this is the first meaning.
 The second meaning is those which are born by beings.
 So according to the second, the root dha means to bear, to
 hold or to bear.
 Here the meaning is passive.
 So those which are born by beings are called dha-tu's.
 So the I element, the I element and so on.
 So we will carry that, something like that.
 So maybe they are called dha-tu's.
 And the third meaning is that which are mere creating or
 making the suffering in samsara.
 Here the verbal noun or abstract noun is used to describe
 it.
 So here dha-tu means just creating or making.
 That means there is no one who creates.
 Just, there is just the act of creating, just the act of
 making.
 And then the fourth one is those by which suffering is made
 or created by beings.
 Now here they are taken as instruments.
 So they are instrumental in making or in creating suffering
 by beings.
 That means beings create suffering with the help of these d
ha-tu's.
 Say by the eye, by the ear, by the nose and so on we create
 suffering.
 That may be the camera man.
 So the fourth one is, the meaning is instrument, right?
 And the meaning of dha is making or creating.
 So today we have only a few students because it is just two
 days after the earthquake
 and many places are not back to normal yet.
 So people probably couldn't come.
 But we will carry on with our class.
 And today we study the description of the elements.
 And the word, the Pali word for the word element is dha-tu.
 And that word is explained in the Path of Purification of
 Visitori Maga.
 And nine explanations of the meaning of the word dha-tu are
 given in the Visitori Maga.
 But the translation in the Path of Purification is so
 incorrect,
 and this, especially in this place, that it is impossible
 to follow it.
 So we will explain to, explain according to what I
 understand to be the real meaning of Visitori Maga.
 So the word dha-tu is explained in nine ways.
 First, please look at the sheets.
 So the first is, those which make or create the suffering
 in samvara.
 Those are called dha-tu.
 So here the root dha has the meaning of making or creating.
 And it has an active sense.
 And the second explanation is, those which are born by
 beings, those which are held by beings.
 So here the root dha has the meaning of bearing.
 And the sense is passive sense.
 So those which are born by beings are called dha-tu.
 The third is those which are mere creating or making the
 suffering in samvara.
 Here just the abstract noun or verbal noun is used.
 Because although the word dha-tu is explained as an active
 sense or passive sense,
 the real explanation or the ultimate explanation is by
 abstract noun.
 So they are just creating or they are just making the act
 of making the act of creating is what we call dha-tu.
 And then four, those by which suffering is made or created
 by beings.
 Here dha-tu are taken as instrumental.
 They are instrumental in beings making or creating
 suffering.
 So we create suffering, we make suffering occur by the help
 of these dha-tu, by the eye, by the ear and so on.
 So here the sense is instrumental.
 I will explain to you later about this.
 And then the next one is those where suffering is stored or
 placed.
 So here the dha, the root dha means to place, to put, to
 store.
 And the sense is a place or location.
 So here dha-tu are those where suffering is stored or
 placed.
 That means we create suffering with these eyes and so on.
 So suffering is said to be stored in the eye, in the ear
 and so on.
 Sometimes both are said the eye is burning, the ear is
 burning and so on.
 That means through the eye we get akusala, through the ear
 we get akusala.
 Then next one, those which bear their own individual
 essence.
 Here the root dha means to bear, to bear to hold their own
 individual essence.
 Now this is also the active sense, but it is different from
 the number one meaning.
 In the number one meaning, the word dha has the meaning of
 making or creating.
 But here the root dha has the meaning of bearing.
 So the meaning is different.
 Also, although the sense is active here,
 there those which make or create are called dha-tu.
 Here those which bear their own individual essence are
 called dha-tu.
 And then number seven, those which resemble dha-tu,
 such as, I don't know these, mother chai, cinnabar and so
 on,
 they are in the path of purification.
 They are some we get from rocks, stone, some kind of stones
.
 So they are called dha-tu.
 And these are the mother chai, cinnabar, just as mother ch
ai and cinnabar are the constituents of parts of the rock.
 These are also part of ourselves. So they are called dha-tu
.
 Here it is through a figure of speech that we call them dha
-tu.
 I think it is like calling an Asian American.
 Maybe an Asian imitates everything American,
 and somebody may refer to him as he is an American.
 But he is not an American, but he is like an American.
 In the same way, they are dha-tu because they are like
 those rocks.
 And number eight is those which resemble dha-tu, again dha-
tu, such as chai, blood, etc.
 Now, there are said to be seven dha-tu in our bodies.
 I think we met these seven dha-tu in one of the previous
 chapters on defining the four elements.
 So they are also called dha-tu in Ayurvedic medicine.
 They said there are seven kinds of dha-tu in the body.
 Cai, blood, bone, bone marrow, flesh, semen.
 So they are like dha-tu.
 They are just as the cai, blood, and so on are part of
 ourselves.
 So we are called dha-tu as our bodies, and the dha-tu is
 the eye, ear, and others are also part of ourselves.
 So they are called dha-tu, through resemblance.
 And then the last one, nine.
 Here, we are not to follow any etymology.
 So it is non-etymological meaning.
 Just the word dha-tu just means "no soul."
 So you don't have to worry about how this word is made or
 from what root it is derived or whatever.
 So you don't have to worry about that.
 It is the word that means "no soul."
 So these nine meanings are given in the Visuddhi Maga.
 Now, there are words in Pāri language, also in Sanskrit
 language,
 especially nouns from roots, so roots and suffixes are
 combined to form words.
 So when they are, I mean, to form words or nouns.
 So when they are formed, the root has one meaning and the
 suffix has another meaning.
 So for example, the word dha-tu, the root dha-tu has one
 meaning, and that is to "whole" or to "create" or whatever.
 And the suffix "tu" has another meaning.
 That is, that can be an agent or an active one, or that can
 be what is done to it, passive one,
 or it can show as the instrument, or sometimes it is a
 person to whom something is given,
 or sometimes it is a place of person from whom, say, we
 depart or something.
 And also, sometimes it means a place.
 So these are called in Bali, sadhana.
 I don't know how to translate it into English.
 It is a grammatical Pali and Sanskrit grammatical term.
 So different nouns denote different things.
 I mean, you may have, there may be only one word, but that
 one word can mean different things
 according to the meaning denoted by the suffix.
 Compound word.
 Well, they are compound words, but they may mean something
 else also.
 So according to that grammatical thing, the word dha-tu is
 explained here.
 It is not so important if you don't understand all of these
,
 but if you understand that the word dha-tu is used to mean
 I hear, nose and so on,
 I think that is enough.
 But if you want to go into the meaning, if you take the
 light in, say,
 looking for the etymological meaning of the words, it might
 be interesting.
 So these nine meanings are mentioned in the Visori manga.
 In the Path of Purification, only up to e, the numbers are
 given, a, b, c, d, e,
 but not the other ones.
 So paragraph 21, "Furthermore, for the self of the
 sectarian does not exist with an individual essence,
 not so these and so on." That should be f.
 And then one line down, one more line, and just as in the
 word, that should be g.
 And about three lines down, or just as the general term
 elements, that should be h.
 And paragraph 22, "Furthermore, element is a term for what
 is soulless." That should be i.
 So those correspond to 1, 2, 3, 4, and so on on the sheet.
 So a is 1, b is 2, and so on. So you may compare them later
.
 And he misunderstood the word "dharinti" in paragraph 21.
 He took it to mean in a causal sense, but actually it is
 not a causal verb.
 "Dharinti," it looks like a causal form, but it doesn't.
 It is not really a causal verb.
 So "dharinti" just means to bear or to bear or hold, not
 cause to bear or cause to be born.
 So that is the misunderstanding of the word "dharinti."
 Now paragraph 23 has two characteristics, etc. They are not
 difficult to understand.
 And the footnote 11, also it is "helpless."
 I don't know how he misunderstood so much in this place.
 So the verb "dharinti" should be taken to be ordinary, not
 in a causative sense.
 Now next page.
 "As to order." So here to from among order of arising, etc.
 mentioned above, only order of teaching is appropriate.
 So these 12 are given just in teaching, not according to
 the arising or not according to other order.
 So it is set for the according to successive definition of
 cause and fruit, or cause and effect.
 For the pair, "eye element" and "visible data element" are
 the cause.
 And "eye consciousness element" is the fruit, so in each
 case.
 Now if you look at this, and the elements, the three are
 given in a line, right?
 So "eye element," "visible data element," "eye
 consciousness element," these three.
 So "eye element" and "visible data element" are said to be
 cause here,
 and "eye consciousness element" is the fruit or effect.
 That means "eye element" and "eye visible data" are
 conditions for "eye consciousness" to arise.
 Only when there is "eye element" or just the "eye" and the
 "visible data" can arise, "eye consciousness."
 So "eye consciousness" is said to be the result of "eye
 element" and "visible data element."
 So this is the order here taken.
 It has to adjust so much.
 Now in this paragraph, a lot of elements are mentioned.
 Paragraph 25.
 So it gives us some knowledge about, some information about
 the word "element."
 So the word "element" is used to mean almost anything here.
 That is why to translate the word "dha" to as "element"
 always is not so helpful.
 You may translate it "element," but people will not
 understand.
 So you can see here, the illumination element, duty element
,
 base consisting of boundness, base element, base consisting
 of boundness, consciousness element,
 base consisting of nothingness element, base consisting of
 perception and feeling element,
 sense desire element, ill will element, cruelty element,
 and so on.
 So the word "dha" to is made to mean many things in the sod
as and also in abhidhamma.
 So the other took all these elements from the sodas and
 then explained here that all of them,
 all of them are the same as or included in one or the other
 of the elements mentioned here.
 So only 18 are mentioned here.
 But there are other things described as elements, such as
 illumination element, beauty element, and so on.
 But they are not separate elements actually.
 They can be the same as the ones mentioned here or they can
 be included in one or other of the elements.
 And so, paragraph 26, 27, and so on, explain this.
 So the first line of the paragraph 26, the visible dha
 element itself is the illumination element.
 So visible dha and illumination are the same here, the
 first one.
 I mean, in the middle color.
 So beauty element is born up with visible dha.
 Beauty element is also visible dha element because beauty
 is just visible dha.
 The sign of beautiful is the beauty element and that does
 not exist apart from visible dha and so on.
 And then three lines down.
 As regards to the base consisting of bondless space element
, etc.
 Those are formless jhanas or formless jhatas.
 The consciousness is mind consciousness element only.
 While the remaining states are the mental dha element.
 Mental dha element means in Pali, Dhamma, Dhaadu.
 Do you see that?
 And the second column, the last line, mental dha element or
 Dhamma element.
 Yeah.
 So consciousness is mind consciousness element only.
 Mind consciousness element.
 While the remaining states, that means the G Dz are the
 mental dha element.
 But the cessation of perception and feeling element does
 not exist as an individual essence.
 Sesession of perception and feeling element means a kind of
 attainment, samahapati.
 When a person gets into this samahapati, his mental
 activity is suspended.
 No mental activity during their attainment.
 And that is called cessation of perception and feeling.
 Actually not only perception and feeling but all mental
 activities.
 And that attainment can be entered into only by anagramis,
 non-returners and arahams.
 And we will have this in detail at the end of the book.
 So that is just the absence of mental activity.
 So absence of mental activity has no individual essence.
 It is a concept.
 For that is merely the cessation of two elements.
 The sense desire element.
 Now this is important.
 The sense desire element is either merely the mental dha
 element.
 According to what is the sense desire element.
 It is the thought, applied thought, wrong thinking that is
 associated with sense desire.
 Now you know the applied thought, vitaka.
 We call it initial application.
 So when it means initial application, then it is included
 in dhamma element, mental dha element.
 Because dhamma element consists of cheetah seekers, some r
upas and nibbana.
 It is given in the bottom line.
 Dhamma base equals dhamma element equals cheetah seekers, s
attva-meda and nibbana.
 There are sixteen sattva material properties in nibbana.
 They are called dhamma base or dhamma element.
 And so if it is taken to be applied thought, then it is the
 mental, I mean it is dhamma element.
 Or it is the eighteen elements according as it is said,
 making the a
 or the a-bha element, the aggregate element, the material
ity, feeling, perception,
 information and consciousness that are in this interval
 belong here or are included here.
 These are called the sense desire element.
 So in this sense, sense desire element means just the sens
ual sphere.
 You remember the thirty-one planes of existence, right?
 So there are eleven sensual sphere existence.
 So these eleven sensual spheres are called sense desire
 element here.
 So it is important because in some places, Nyanamali
 misunderstood this word and he translates wrongly.
 So sense desire element can mean what, the eleven sens
 desire realms.
 From bottom up, right?
 Bottom up to Para Nimita Vasavati.
 Right. So those are eleven.
 Yes, right.
 And then the renunciation element is mental data element
 because it is also vitaka.
 Yeah, renunciation element means thinking about getting out
 of sense desires.
 So it is vitaka, so it is mental data element.
 Also because of the passage, also all profitable states are
 the renunciation element.
 According to this definition, it is the mind consciousness
 element too.
 Because here, all profitable states, so all kusala, all kus
ala means kusala consciousness and mental states.
 So they belong to mind consciousness element also.
 That means to mental data element as well as to mind
 consciousness element.
 That means Dhamma element, right?
 Mental data element and mind consciousness element.
 And then the elements of ill will, cruelty, non ill will,
 non cruelty, bodily pleasure, bodily pain, joy,
 grief, equanimity, ignorance, initiative, launching and
 persistence are the mental data elements too.
 So they belong to Dhamma element.
 Then the inferior, medium and superior.
 They are not separate elements but those 18 elements.
 For inferior eyes etc. are the inferior element and medium
 and superior eyes etc. are the medium and superior elements
.
 And then paragraph 30, the earth, fire and air elements,
 these you know, the tangible data element.
 The water element and the space element are the mental data
 element only.
 Because the water element and space element are included in
 subtle matter.
 So they belong to Dhamma element.
 Consciousness element is a term summarizing the seven
 consciousness elements beginning with eye consciousness.
 That means eye consciousness ear, one, two, three, four,
 five, six, the green ones.
 Seventeen elements and one part of the mental data element
 are the formed element.
 Formed means conditioned and unfirmed means unconditioned.
 So all of them are formed except nirvana.
 Nirvana is a part of Dhamma element.
 So 17 other elements and part of Dhamma element are formed.
 But the unfarmed element is one part of the mental data
 element only.
 So unfarmed element is a part of Dhamma element which is n
irvana here.
 And then the world of many elements of various elements is
 merely what is divided up into the 18 elements.
 So the world divided into 18 elements.
 So they are not different from or separate from the 18
 elements mentioned above.
 But we have to understand that there are other things which
 are called dhadus in Pali.
 And so sometimes it is very difficult to understand when we
 just use the word element to translate the word dhadu.
 It is a translation but it is not so understandable.
 For example, when you say the sense desire element, meaning
 the Kamo Jala Ram, meaning the essential Ram,
 it is very difficult to understand.
 Now, Father Maudiya stated as 18 for the purpose of
 eliminating the kind of perception to be found in those who
 perceive his soul in consciousness,
 the individual essence of which is cognizing and so on.
 That means Buddha wanted to do away with the idea of self,
 idea of soul.
 That is why Buddha mentioned these 18 elements.
 And that is why I always say that abhidhamma is just one
 doctrine of anatta.
 In abhidhamma, things are described as five aggregates or
 twelve bases, 18 elements, 22 faculties.
 And so in different ways the world is analyzed but you do
 not find any soul or eternal substance.
 So the whole of abhidhamma is just the doctrine of anatta.
 Okay, now next page.
 As to reckoning, paragraph 34, the eye element firstly is
 reckoned as one thing according to kind, namely eye
 sensitivity.
 Likewise, the ear, nose, tongue, body, visible data, sound,
 odor, flavor, element, the recon has ear sensitivity and so
 on.
 But the tangible data element is reckoned as three things,
 as we already know.
 The tangible data is just the combination of three, earth,
 fire and air elements.
 Now the eye consciousness element is reckoned as two things
, eye consciousness element.
 That means two eye consciousness.
 Do you still have the...
 This chart.
 So according to this chart, now the eye consciousness
 element means here 13 and 20.
 So there are two, right?
 Namely profitable and unprofitable, comma, result.
 So 13 is result of unprofitable and 20 is result of
 profitable.
 In the chart at the end of this book, the numbers will be
 different.
 So there the numbers are 34 and 50.
 He didn't give the number here.
 I think you should put the number after unprofitable, comma
, result.
 34 and 50.
 It is the numbers in the chart given at the end of the book
.
 On page, on the location.
 Table 3.
 And the mind element...
 And likewise the consciousness elements of the ear, nose,
 tongue and body, they have to say.
 The mind element is reckoned as three things, namely five
 door, Edwarding, number 70 there.
 And the profitable and unprofitable result and receiving.
 39 and 55 there.
 So you can locate them easily because there are numbers
 here.
 Now the mental data element has 20 things, namely three imm
aterial aggregates.
 You know the three immaterial aggregates.
 Feeling, perception and formations.
 16 kinds of subtle matter. And the unformed element, that
 means nibbana.
 So mental data or Dhamma element means 20 things here.
 Three aggregates, three mental aggregates.
 16 subtle matter and nibbana.
 Three mental aggregates are simply 52 ji-jigas.
 And 52 plus 16 plus 1.
 How many?
 69.
 69. So these 69 are called Dhamma element, mental data
 element.
 You may refer to paragraph 14 back.
 And you see that there the Dhamma data base is mentioned,
 explained similarly.
 Actually we have to take it to mean the same thing.
 There it says the mental data basis of many kinds when
 classified according to
 to the several individual essences, not several actually.
 The difference in individual essences of feeling,
 perception, formations,
 those are three mental aggregates.
 Subtle matter and nibbana.
 So these two explanations mean the same thing.
 So if you want to know what are mental data, element or
 mental data base,
 you can come to this paragraph and they mean the same thing
.
 Mind consciousness element is recognized as 76 things.
 That means apart from these 10 and then these three.
 Let me see.
 In this chat, right?
 13, 14, 15, 16, 17, 20, 21, 22, 22, 24.
 And then 28, 18, 25.
 They are called mind consciousness element.
 Actually, all data is less than 10, they are seeing
 consciousness and so on,
 and the mind element.
 The mind element consists of 28, 18 and 25.
 They are called mind element.
 And then paragraph 35, the condition.
 Now the eye element firstly is a condition in six ways,
 namely dissociation,
 pre-ness and presence, non-disappear and support and
 faculty for the eye consciousness element.
 Now please read the end of paragraph 39.
 This is in brief, but the kinds of conditions will be
 explained in detail
 in the description of the dependent origination.
 So if you don't understand the conditions here, like diss
ociation, pre-ness and presence and so on,
 please be patient.
 So these 24 conditions will be explained in detail in the
 chapter on the dependent origination.
 So dissociation means not arising together.
 Pre-ness means something arises before another thing arises
,
 and then still existing at the arising of the other one.
 Suppose, let us say one and two.
 One arose before two arises.
 And when two arises, one is still in existence.
 So that is called pre-ness.
 So when we see something, then there is a seeing
 consciousness here.
 Seeing consciousness sees the visible data.
 And that visible data arises before that moment.
 And it is still existing until the 17th moment.
 So that is why it is called pre-ness.
 And also presence.
 And non-disappearance and presence are the same actually.
 And there is support. It is a support for eye consciousness
.
 So in this way, the eye element or the... what do you call
 it?
 Eye sensitivity is the condition for the eye consciousness.
 Something like that.
 So you will understand after we finish the dependent
 chapter on dependent origination.
 So if you don't understand now, don't be depressed.
 And then paragraph 40, "to be seen."
 To be seen means to be viewed.
 How you should view that?
 How you should look at that?
 So the meaning is that here to the exposition should be
 understood as to how they are to be regarded.
 For all formed elements are to be regarded as secluded from
 the past and future.
 That means the past cannot be seen really clearly.
 And future also cannot be seen clearly.
 So here the commentary is talking about the present, the
 adult.
 So they have no past and no future, but just the present
 one are the real ones.
 So they are secluded from the past and future.
 As void of any lustingness, beauty, pleasure or self.
 And as existing independence on conditions.
 So you should view them like this.
 And then another kind of viewing is given in paragraph 41.
 And I think they are good examples.
 The eye element should be regarded as a surface of a drum.
 The visible data element is the drumstick.
 And the eye consciousness element is the sound.
 So something like that.
 It strikes a drum and then sound arises.
 So sound is like eye consciousness.
 And the stick is like visible data.
 And eye sensitivity is like drum and so on.
 So they are good examples of the elements.
 And then 42, the mind element however should be regarded as
 the forerunner and follower of eye consciousness etc.
 as that arises.
 Now I say mind element consists of the number 28, 18 and 25
.
 So in a thought process, immediately before, let us say
 seeing thought process, right?
 So in a thought process, immediately before 13, there is 28
.
 If you look at the diagram of the thought process.
 First there is 28, adverting.
 And then 13, seeing.
 And after seeing there is what? Receiving.
 So receiving is number 18 or 25.
 So this mind element which is here 28, 18 or 25, counts
 before and after 13.
 That is what the commentary is saying here.
 The mind element however should be regarded as the fore
runner and follower of eye consciousness.
 So if you look at the diagram, it is very easy to see.
 Eye consciousness is here.
 Before eye consciousness there is 5 cents to adverting
 which is number 28.
 And after eye consciousness there is 18 or 25 which is
 receiving.
 So the mind element comes before and after eye
 consciousness, etc.
 And the mental element and so on.
 And then the last paragraph, the mind consciousness element
 should be regarded as a forest monkey.
 Because it does not stay still on its object.
 Or as a wild horse because it is difficult to tame.
 And as it sticks flung into the air because it falls
 anywhere, not anyhow.
 Because it falls wherever it lies. That means anywhere.
 And as a stage dancer because it adopts the guise of the
 various developments such as creation, hate and so on.
 This is the section on 18 elements.
 So you can see the 18 elements on this sheet.
 And there are different colors given to 18 elements.
 Now the next one is faculties and truths.
 So you can see the 18 elements.
