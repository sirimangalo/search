1
00:00:00,000 --> 00:00:03,600
 Okay, then let's get started.

2
00:00:03,600 --> 00:00:14,080
 Welcome everyone to a fine Sunday afternoon in Second Life.

3
00:00:14,080 --> 00:00:19,500
 Everybody get yourself comfortable both here in virtual

4
00:00:19,500 --> 00:00:21,600
 world and at home.

5
00:00:21,600 --> 00:00:29,010
 I'd say that it's equally important that you're focused at

6
00:00:29,010 --> 00:00:30,160
 home.

7
00:00:30,160 --> 00:00:33,120
 It's more important that you're focused at home than that

8
00:00:33,120 --> 00:00:34,640
 you're focused here.

9
00:00:34,640 --> 00:00:38,070
 If your avatar here is shifting and shuffling, that's not

10
00:00:38,070 --> 00:00:39,160
 really a problem.

11
00:00:39,160 --> 00:00:41,440
 But at home you should try to.

12
00:00:41,440 --> 00:00:44,750
 If you can while you're watching this, try to settle

13
00:00:44,750 --> 00:00:46,480
 yourself down.

14
00:00:46,480 --> 00:00:51,440
 Turn Facebook off.

15
00:00:51,440 --> 00:00:56,720
 Get yourself too busy if it helps.

16
00:00:56,720 --> 00:01:01,010
 I'm just going to talk for a little while and then if

17
00:01:01,010 --> 00:01:03,440
 anyone has any questions, I'm

18
00:01:03,440 --> 00:01:14,640
 happy to answer them.

19
00:01:14,640 --> 00:01:20,770
 The topic I'd like to talk about today has to do with the

20
00:01:20,770 --> 00:01:23,040
 very core of the Buddhist

21
00:01:23,040 --> 00:01:24,040
 teaching.

22
00:01:24,040 --> 00:01:30,250
 No use beating around the bush and no use giving

23
00:01:30,250 --> 00:01:36,360
 complicated roundabout or exotic teachings.

24
00:01:36,360 --> 00:01:40,960
 It's cut right to the chase.

25
00:01:40,960 --> 00:01:45,650
 When we talk about the core of Buddhism, we always think of

26
00:01:45,650 --> 00:01:47,520
 the Four Noble Truths.

27
00:01:47,520 --> 00:01:50,650
 I think because it's a good summary of the Buddhist

28
00:01:50,650 --> 00:01:51,600
 teaching.

29
00:01:51,600 --> 00:01:55,420
 But there's a teaching that goes even more to the core than

30
00:01:55,420 --> 00:01:55,760
 that.

31
00:01:55,760 --> 00:02:00,080
 That's the Buddhist teaching on dependent origination.

32
00:02:00,080 --> 00:02:02,360
 That's what I'd like to talk about today.

33
00:02:02,360 --> 00:02:07,280
 I know I've talked about it before elsewhere.

34
00:02:07,280 --> 00:02:10,640
 I can't remember if I've talked about it here on Second

35
00:02:10,640 --> 00:02:12,480
 Life, but it's always good

36
00:02:12,480 --> 00:02:17,560
 to go over things again.

37
00:02:17,560 --> 00:02:22,680
 We take the Buddhist teaching in its entirety as an

38
00:02:22,680 --> 00:02:25,920
 instruction in the practice of meditation,

39
00:02:25,920 --> 00:02:30,280
 in observing reality and coming to understand it.

40
00:02:30,280 --> 00:02:33,910
 When you listen to me give a talk here, it's important that

41
00:02:33,910 --> 00:02:37,240
 you're mimicking or following

42
00:02:37,240 --> 00:02:43,210
 after the example of the people who listen to the Buddha

43
00:02:43,210 --> 00:02:45,280
 himself teach.

44
00:02:45,280 --> 00:02:47,770
 That is, when they listen to the Buddha's teaching, they

45
00:02:47,770 --> 00:02:49,360
 would also practice meditation.

46
00:02:49,360 --> 00:02:56,070
 They would take it as an opportunity to look inside

47
00:02:56,070 --> 00:03:01,280
 themselves and to apply the teachings

48
00:03:01,280 --> 00:03:05,720
 as a reminder for how to approach reality.

49
00:03:05,720 --> 00:03:06,720
 Here we can do the same.

50
00:03:06,720 --> 00:03:10,120
 You don't have to look at your computer screen.

51
00:03:10,120 --> 00:03:11,680
 You can close your eyes.

52
00:03:11,680 --> 00:03:15,420
 If it helps you to keep on track to look at the screen,

53
00:03:15,420 --> 00:03:16,080
 fine.

54
00:03:16,080 --> 00:03:22,200
 But try to really appreciate the teachings on a more

55
00:03:22,200 --> 00:03:24,680
 practical level.

56
00:03:24,680 --> 00:03:27,760
 Because in the time of the Buddha, many people, the example

57
00:03:27,760 --> 00:03:29,480
 that they said is that they could

58
00:03:29,480 --> 00:03:32,540
 gain states of realization during the time that they were

59
00:03:32,540 --> 00:03:36,400
 listening to the Buddha's teaching.

60
00:03:36,400 --> 00:03:40,370
 Simply by applying it and using it to calm the mind, to

61
00:03:40,370 --> 00:03:42,640
 restrain the mind and calm the

62
00:03:42,640 --> 00:03:50,020
 mind and to eventually understand the workings of the mind.

63
00:03:50,020 --> 00:03:55,190
 They could even enter into a state of enlightenment, go

64
00:03:55,190 --> 00:03:58,800
 into the realization of nibbana and so

65
00:03:58,800 --> 00:04:05,280
 on.

66
00:04:05,280 --> 00:04:09,360
 Take that as an example here.

67
00:04:09,360 --> 00:04:12,790
 This teaching especially is an incredibly useful and

68
00:04:12,790 --> 00:04:14,640
 practical teaching, the teaching

69
00:04:14,640 --> 00:04:17,800
 of dependent origination.

70
00:04:17,800 --> 00:04:21,730
 It's the most profound, I would say the most profound

71
00:04:21,730 --> 00:04:24,200
 statement of reality that exists.

72
00:04:24,200 --> 00:04:27,330
 I can't think of anything else that I've heard more

73
00:04:27,330 --> 00:04:32,080
 profound, even within the Buddha's teaching.

74
00:04:32,080 --> 00:04:35,440
 For me the most profound part is the very beginning of the

75
00:04:35,440 --> 00:04:36,320
 teaching.

76
00:04:36,320 --> 00:04:42,690
 It's these three words in the Pali that really brought

77
00:04:42,690 --> 00:04:45,340
 light to the world.

78
00:04:45,340 --> 00:04:50,340
 Before the uttering of these three words, "Avitja" by Jaya

79
00:04:50,340 --> 00:04:51,400
 Sankara.

80
00:04:51,400 --> 00:04:59,520
 Before the Buddha realized this, that "Avitja" or "ignor

81
00:04:59,520 --> 00:05:02,400
ance" is a cause for the arising

82
00:05:02,400 --> 00:05:11,870
 of formations or with ignorance as a cause there arise

83
00:05:11,870 --> 00:05:14,680
 formations.

84
00:05:14,680 --> 00:05:16,280
 Anybody enlightened yet?

85
00:05:16,280 --> 00:05:20,080
 It's probably not that easy.

86
00:05:20,080 --> 00:05:23,630
 It's a profound teaching and it's something that's very

87
00:05:23,630 --> 00:05:25,520
 difficult for us to understand,

88
00:05:25,520 --> 00:05:30,180
 very difficult to comprehend this teaching.

89
00:05:30,180 --> 00:05:31,720
 It often goes over our heads.

90
00:05:31,720 --> 00:05:37,640
 We think of it as some sort of philosophical teaching.

91
00:05:37,640 --> 00:05:44,960
 Ignorance leads to formations.

92
00:05:44,960 --> 00:05:47,940
 Formations here, just to explain, the word "formations,"

93
00:05:47,940 --> 00:05:49,460
 what we're talking about here

94
00:05:49,460 --> 00:05:55,620
 is our mental formations, our ideas about things, our

95
00:05:55,620 --> 00:05:59,440
 thoughts, what we think of something,

96
00:05:59,440 --> 00:06:06,760
 our mental volition.

97
00:06:06,760 --> 00:06:12,850
 When we want to hurt someone, when we want to attain

98
00:06:12,850 --> 00:06:17,400
 something, when we get angry, when

99
00:06:17,400 --> 00:06:26,480
 we get greedy, when we get attached, addicted, when we're

100
00:06:26,480 --> 00:06:31,080
 afraid or worried, and so on.

101
00:06:31,080 --> 00:06:34,270
 All of these mental states that are a reaction to something

102
00:06:34,270 --> 00:06:36,400
, these arise, the Buddha said,

103
00:06:36,400 --> 00:06:41,320
 based on ignorance.

104
00:06:41,320 --> 00:06:45,740
 And this is really a profound teaching that deserves the

105
00:06:45,740 --> 00:06:47,680
 full of our attention.

106
00:06:47,680 --> 00:06:52,690
 If we can understand just this, just these three words, if

107
00:06:52,690 --> 00:06:54,920
 we can realize this in our

108
00:06:54,920 --> 00:07:01,320
 meditation practice, this is the state of enlightenment

109
00:07:01,320 --> 00:07:04,280
 that we're looking for.

110
00:07:04,280 --> 00:07:11,970
 And the problem is that for most of us, we don't think this

111
00:07:11,970 --> 00:07:13,120
 way.

112
00:07:13,120 --> 00:07:16,910
 When we get angry or when we become addicted to something,

113
00:07:16,910 --> 00:07:18,800
 we say to ourselves or we say

114
00:07:18,800 --> 00:07:22,640
 to other people, "I know it's wrong.

115
00:07:22,640 --> 00:07:30,640
 I know it's not good to get angry, but I can't help myself.

116
00:07:30,640 --> 00:07:41,260
 I know it's not good to become addicted to sweet foods or

117
00:07:41,260 --> 00:07:45,640
 so on, but I can't help myself."

118
00:07:45,640 --> 00:07:49,480
 It's not that I don't know, it's that I'm unable to change

119
00:07:49,480 --> 00:07:49,920
 it.

120
00:07:49,920 --> 00:07:59,320
 I'm unable to avoid the judging, the emotion.

121
00:07:59,320 --> 00:08:02,000
 And so the Buddha denied this.

122
00:08:02,000 --> 00:08:03,880
 He denied that this is the case.

123
00:08:03,880 --> 00:08:05,200
 He taught the exact opposite.

124
00:08:05,200 --> 00:08:08,120
 He said, "No, you don't know.

125
00:08:08,120 --> 00:08:10,680
 You don't know that it's wrong."

126
00:08:10,680 --> 00:08:12,080
 You say, "Yes, I know that it's wrong."

127
00:08:12,080 --> 00:08:14,430
 What you really mean is that someone told you that it's

128
00:08:14,430 --> 00:08:15,800
 wrong, you don't like the results

129
00:08:15,800 --> 00:08:20,770
 that come from it, but you don't really understand that it

130
00:08:20,770 --> 00:08:21,880
's wrong.

131
00:08:21,880 --> 00:08:26,010
 This is how we approach everything in our lives with

132
00:08:26,010 --> 00:08:29,720
 ignorance, with an incredibly superficial

133
00:08:29,720 --> 00:08:33,520
 awareness.

134
00:08:33,520 --> 00:08:36,460
 Even imagine yourself, look at yourself, how you're sitting

135
00:08:36,460 --> 00:08:37,840
 right now, listening to my

136
00:08:37,840 --> 00:08:43,960
 talk.

137
00:08:43,960 --> 00:08:45,730
 You're seeing things, you're hearing things, you're

138
00:08:45,730 --> 00:08:49,160
 smelling, you're tasting, you're feeling,

139
00:08:49,160 --> 00:08:51,480
 you're thinking.

140
00:08:51,480 --> 00:08:54,160
 And all of this is happening very quickly.

141
00:08:54,160 --> 00:08:58,520
 And when you see something, you immediately start to judge

142
00:08:58,520 --> 00:09:01,040
 it, immediately start to assess

143
00:09:01,040 --> 00:09:02,040
 it.

144
00:09:02,040 --> 00:09:06,390
 "This is beautiful, this is nice," or, "This is ugly," or,

145
00:09:06,390 --> 00:09:08,440
 "This is terrible, horrible,"

146
00:09:08,440 --> 00:09:10,200
 whatever it is.

147
00:09:10,200 --> 00:09:13,400
 When you hear something, you immediately start to judge it.

148
00:09:13,400 --> 00:09:18,470
 Maybe you like the birds in the background, maybe they're

149
00:09:18,470 --> 00:09:20,640
 too loud, too noisy.

150
00:09:20,640 --> 00:09:25,000
 Maybe that repetitive cricket noise is driving you crazy.

151
00:09:25,000 --> 00:09:30,220
 We don't really see and we don't really hear, we don't

152
00:09:30,220 --> 00:09:33,200
 really understand the experience.

153
00:09:33,200 --> 00:09:35,910
 And we don't really understand our reaction to the

154
00:09:35,910 --> 00:09:36,840
 experience.

155
00:09:36,840 --> 00:09:39,000
 When we see something, we think, "I see.

156
00:09:39,000 --> 00:09:43,260
 I'm seeing this," and we think, "I like it," and we think,

157
00:09:43,260 --> 00:09:44,360
 "It is good."

158
00:09:44,360 --> 00:09:47,350
 We have all of these preconceived notions that are totally

159
00:09:47,350 --> 00:09:49,040
 disconnected from the reality

160
00:09:49,040 --> 00:09:54,520
 of the experience.

161
00:09:54,520 --> 00:10:03,650
 They're generally bound up in our habits, our accustomed

162
00:10:03,650 --> 00:10:08,560
 way of responding to things.

163
00:10:08,560 --> 00:10:11,990
 We remember that certain things bring us pleasure and so we

164
00:10:11,990 --> 00:10:13,580
 respond in that manner.

165
00:10:13,580 --> 00:10:17,120
 We think something's going to make us happy.

166
00:10:17,120 --> 00:10:25,790
 And it's a habitual response, the response of, say, an

167
00:10:25,790 --> 00:10:28,960
 ordinary animal.

168
00:10:28,960 --> 00:10:32,960
 So all we're trying to do in meditation is to look deeper

169
00:10:32,960 --> 00:10:34,000
 at things.

170
00:10:34,000 --> 00:10:36,850
 When you're sitting here listening to the talk, someone

171
00:10:36,850 --> 00:10:38,140
 walks into the room and starts

172
00:10:38,140 --> 00:10:41,100
 making loud noise, right away you get angry at it.

173
00:10:41,100 --> 00:10:47,220
 Maybe there's a little kid making noises, yelling, pester

174
00:10:47,220 --> 00:10:49,880
ing you for this or that.

175
00:10:49,880 --> 00:10:51,600
 And you're ready to get angry, get irritated.

176
00:10:51,600 --> 00:10:55,340
 You can even get to the point where you want to yell at

177
00:10:55,340 --> 00:10:56,080
 them.

178
00:10:56,080 --> 00:10:59,040
 It's very quick, it's very easy to do that.

179
00:10:59,040 --> 00:11:00,960
 And the only reason that you do it, it's not that you're a

180
00:11:00,960 --> 00:11:02,000
 bad person, it's that you're

181
00:11:02,000 --> 00:11:03,000
 ignorant.

182
00:11:03,000 --> 00:11:04,440
 You don't really understand what happened.

183
00:11:04,440 --> 00:11:06,240
 You weren't watching.

184
00:11:06,240 --> 00:11:08,040
 You weren't clear on the experience.

185
00:11:08,040 --> 00:11:10,020
 You misunderstood it.

186
00:11:10,020 --> 00:11:11,400
 And so you followed after it.

187
00:11:11,400 --> 00:11:14,560
 You reacted inappropriately and you caused suffering for

188
00:11:14,560 --> 00:11:16,440
 other people and for yourself.

189
00:11:16,440 --> 00:11:28,200
 You feel guilty, you feel upset, you feel angry.

190
00:11:28,200 --> 00:11:30,610
 So this is the most important point of the Buddhist

191
00:11:30,610 --> 00:11:32,320
 teaching is that ignorance is the

192
00:11:32,320 --> 00:11:44,650
 cause of our reactions to things, our judgments, our

193
00:11:44,650 --> 00:11:52,720
 improper reactions, our improper

194
00:11:52,720 --> 00:12:00,820
 modes of behavior, modes of responding to the stimulus that

195
00:12:00,820 --> 00:12:02,640
 come to us.

196
00:12:02,640 --> 00:12:07,270
 And so the Buddha tried to describe to us in his teaching

197
00:12:07,270 --> 00:12:10,600
 what it was that he realized,

198
00:12:10,600 --> 00:12:15,140
 the detailed explanation of what's going on so that when we

199
00:12:15,140 --> 00:12:17,320
 practice meditation we can

200
00:12:17,320 --> 00:12:19,760
 see things clearer.

201
00:12:19,760 --> 00:12:23,000
 You ask, "Well, okay, so I'm ignorant.

202
00:12:23,000 --> 00:12:24,000
 What is it that I'm ignorant of?

203
00:12:24,000 --> 00:12:28,720
 What is it that I don't understand?"

204
00:12:28,720 --> 00:12:31,280
 And the truth is if you spend some time looking at reality,

205
00:12:31,280 --> 00:12:32,640
 you'll see there's a lot that

206
00:12:32,640 --> 00:12:33,640
 you don't understand.

207
00:12:33,640 --> 00:12:35,840
 There's a lot that you weren't aware of.

208
00:12:35,840 --> 00:12:39,220
 You'll see that if you just took the time to really see

209
00:12:39,220 --> 00:12:41,280
 what's going on when this young

210
00:12:41,280 --> 00:12:50,460
 child is pestering you, when this loud noise is bothering

211
00:12:50,460 --> 00:12:55,680
 you, when it's too hot, when

212
00:12:55,680 --> 00:12:59,120
 it's too cold, when you have pain in the body and so on.

213
00:12:59,120 --> 00:13:01,480
 If you just took the time to look at it, you'd see there's

214
00:13:01,480 --> 00:13:02,720
 so much more going on than you

215
00:13:02,720 --> 00:13:06,640
 thought.

216
00:13:06,640 --> 00:13:08,740
 And at the same time, the experience is so much less than

217
00:13:08,740 --> 00:13:09,400
 you thought.

218
00:13:09,400 --> 00:13:17,080
 There's nothing unpleasant about it at all, that we've got

219
00:13:17,080 --> 00:13:19,240
 a totally wrong understanding

220
00:13:19,240 --> 00:13:23,110
 of the experience, that surprisingly there's nothing

221
00:13:23,110 --> 00:13:25,240
 unpleasant about it at all.

222
00:13:25,240 --> 00:13:28,320
 You can be in incredible pain.

223
00:13:28,320 --> 00:13:30,160
 And when you really understand the pain, when you really

224
00:13:30,160 --> 00:13:31,240
 see what's going on, it doesn't

225
00:13:31,240 --> 00:13:33,360
 bother you at all.

226
00:13:33,360 --> 00:13:42,130
 You become surprised that you were ever upset by it in the

227
00:13:42,130 --> 00:13:44,640
 first place.

228
00:13:44,640 --> 00:13:47,140
 You say to yourself, you can't believe that you were

229
00:13:47,140 --> 00:13:48,160
 addicted to this.

230
00:13:48,160 --> 00:13:53,630
 It's an epiphany of sorts, that you suddenly realize that

231
00:13:53,630 --> 00:13:56,560
 there's nothing wrong with reality.

232
00:13:56,560 --> 00:13:58,120
 There's nothing wrong with the way things are.

233
00:13:58,120 --> 00:14:01,120
 It is the way it is.

234
00:14:01,120 --> 00:14:13,760
 What's wrong is the way we respond, the way we react to it.

235
00:14:13,760 --> 00:14:19,140
 So the Buddha taught us to go into more detail and he

236
00:14:19,140 --> 00:14:22,320
 explained what's really going on.

237
00:14:22,320 --> 00:14:28,080
 And this is in the rest of the exposition on the dependent

238
00:14:28,080 --> 00:14:29,920
 origination.

239
00:14:29,920 --> 00:14:39,040
 So to go through it in brief, what's really going on is

240
00:14:39,040 --> 00:14:42,120
 that in the world, in the universe,

241
00:14:42,120 --> 00:14:51,780
 in the ultimate reality, there are two things, there are

242
00:14:51,780 --> 00:14:56,720
 two aspects of experience.

243
00:14:56,720 --> 00:15:00,800
 And they're sort of like two sides of the same coin.

244
00:15:00,800 --> 00:15:06,480
 They're distinct, but they're a pair.

245
00:15:06,480 --> 00:15:13,680
 And these are the physical and the mental.

246
00:15:13,680 --> 00:15:16,910
 That in the universe, all of our experience can be summed

247
00:15:16,910 --> 00:15:18,400
 up under the physical and the

248
00:15:18,400 --> 00:15:21,760
 mental.

249
00:15:21,760 --> 00:15:24,160
 When we see something, this is the light touching the eye.

250
00:15:24,160 --> 00:15:26,760
 The eye is physical, the light is physical.

251
00:15:26,760 --> 00:15:29,920
 When we hear something, this is the sound touching the ear

252
00:15:29,920 --> 00:15:32,040
 and these are both physical.

253
00:15:32,040 --> 00:15:38,300
 Smells and the nose, tastes and the tongue, feelings in the

254
00:15:38,300 --> 00:15:41,480
 body, these are all physical.

255
00:15:41,480 --> 00:15:45,440
 And the mental side is the knowing of the object, the

256
00:15:45,440 --> 00:15:47,200
 perception of it.

257
00:15:47,200 --> 00:15:49,040
 When our mind is at the eye, then we see.

258
00:15:49,040 --> 00:15:51,800
 When our mind is at the ear, then we hear.

259
00:15:51,800 --> 00:15:53,780
 But sometimes the ear might be there and the sound might be

260
00:15:53,780 --> 00:15:55,040
 there, but our mind is somewhere

261
00:15:55,040 --> 00:15:59,350
 else and so we fail to hear the things that people say to

262
00:15:59,350 --> 00:15:59,880
 us.

263
00:15:59,880 --> 00:16:01,800
 It's difficult to see if you're not really focusing, but

264
00:16:01,800 --> 00:16:02,800
 sometimes when you're using

265
00:16:02,800 --> 00:16:05,160
 the computer, you can find that.

266
00:16:05,160 --> 00:16:07,250
 You're focusing so much on something that you don't hear

267
00:16:07,250 --> 00:16:08,280
 someone talking to you.

268
00:16:08,280 --> 00:16:20,400
 You don't know what it was that they said.

269
00:16:20,400 --> 00:16:23,660
 And these things in and of themselves are not a problem

270
00:16:23,660 --> 00:16:24,600
 obviously.

271
00:16:24,600 --> 00:16:30,380
 The mind knows the object, the object arises, the mind

272
00:16:30,380 --> 00:16:31,720
 knows it.

273
00:16:31,720 --> 00:16:38,720
 But what happens next is there arises a feeling.

274
00:16:38,720 --> 00:16:41,820
 The body and the mind, it comes together at the eye, the

275
00:16:41,820 --> 00:16:43,760
 ear, the nose, the tongue, the

276
00:16:43,760 --> 00:16:48,240
 body or the mind.

277
00:16:48,240 --> 00:16:51,520
 In the mind there's only the thought, there's only the mind

278
00:16:51,520 --> 00:16:51,800
.

279
00:16:51,800 --> 00:16:54,870
 But we have the body and the mind coming together or else

280
00:16:54,870 --> 00:16:56,800
 just the mind thinking itself.

281
00:16:56,800 --> 00:17:04,840
 At the moment of experience, there arises a feeling.

282
00:17:04,840 --> 00:17:06,240
 You can verify this.

283
00:17:06,240 --> 00:17:10,320
 When you see something, if it's a good thing, right away

284
00:17:10,320 --> 00:17:12,280
 you feel happy about it.

285
00:17:12,280 --> 00:17:14,680
 And you can see this if you're really focusing on it.

286
00:17:14,680 --> 00:17:19,180
 So for instance, when we see something and we say to

287
00:17:19,180 --> 00:17:21,800
 ourselves, seeing, seeing, seeing,

288
00:17:21,800 --> 00:17:26,720
 we can catch when we feel happy about it or when we feel

289
00:17:26,720 --> 00:17:28,720
 unhappy about it.

290
00:17:28,720 --> 00:17:31,690
 When we hear something hearing, hearing, we can catch the

291
00:17:31,690 --> 00:17:33,280
 feeling that there's a feeling

292
00:17:33,280 --> 00:17:35,780
 first.

293
00:17:35,780 --> 00:17:41,890
 There's a pleasant feeling or an unpleasant feeling or a

294
00:17:41,890 --> 00:17:44,160
 neutral feeling.

295
00:17:44,160 --> 00:17:47,650
 And these feelings in and of themselves aren't a problem

296
00:17:47,650 --> 00:17:48,400
 either.

297
00:17:48,400 --> 00:17:57,100
 There's nothing inherently unwholesome about a happy

298
00:17:57,100 --> 00:18:02,480
 feeling or an unhappy feeling.

299
00:18:02,480 --> 00:18:05,980
 A pleasant or an unpleasant feeling, it's a physical

300
00:18:05,980 --> 00:18:07,800
 response to a stimulus.

301
00:18:07,800 --> 00:18:11,340
 Since we feel pain in the body, there's nothing wrong with

302
00:18:11,340 --> 00:18:11,840
 that.

303
00:18:11,840 --> 00:18:17,460
 There's nothing unpleasant about it and nothing unwholesome

304
00:18:17,460 --> 00:18:18,640
 about it.

305
00:18:18,640 --> 00:18:21,890
 And by the same token, there's nothing unwholesome about a

306
00:18:21,890 --> 00:18:23,160
 pleasant feeling.

307
00:18:23,160 --> 00:18:29,130
 So many people when they hear that they're instructed to

308
00:18:29,130 --> 00:18:32,600
 acknowledge the happy feelings,

309
00:18:32,600 --> 00:18:35,170
 they get the wrong impression that we're trying to do away

310
00:18:35,170 --> 00:18:36,000
 with happiness.

311
00:18:36,000 --> 00:18:39,000
 It's wrong to feel happiness.

312
00:18:39,000 --> 00:18:41,160
 And this isn't at all the case, but we want to understand

313
00:18:41,160 --> 00:18:41,920
 the happiness.

314
00:18:41,920 --> 00:18:44,760
 We want to see it for what it is.

315
00:18:44,760 --> 00:18:50,850
 Because it's the feelings when unacknowledged, when

316
00:18:50,850 --> 00:18:53,040
 misunderstood.

317
00:18:53,040 --> 00:18:55,080
 If there's ignorance about the feeling that this is what's

318
00:18:55,080 --> 00:18:56,280
 going to give rise to craving.

319
00:18:56,280 --> 00:19:10,440
 This is what gives rise to our likes and our dislikes.

320
00:19:10,440 --> 00:19:13,670
 This teaching, if you haven't ever practiced meditation, it

321
00:19:13,670 --> 00:19:15,200
 might seem quite foreign.

322
00:19:15,200 --> 00:19:19,360
 It might seem quite even uninteresting.

323
00:19:19,360 --> 00:19:21,080
 It's very difficult to understand.

324
00:19:21,080 --> 00:19:23,540
 But this is an incredibly useful teaching when you're

325
00:19:23,540 --> 00:19:24,840
 practicing meditation.

326
00:19:24,840 --> 00:19:30,120
 When meditators will be at a loss as to how to deal with

327
00:19:30,120 --> 00:19:33,040
 strong emotions that come up.

328
00:19:33,040 --> 00:19:36,570
 When they really are attached to something or when they're

329
00:19:36,570 --> 00:19:38,680
 really angry about something.

330
00:19:38,680 --> 00:19:41,040
 When they're really distracted and unfocused.

331
00:19:41,040 --> 00:19:50,290
 When they're worried or stressed, depressed, bored, afraid,

332
00:19:50,290 --> 00:19:52,160
 whatever.

333
00:19:52,160 --> 00:19:54,560
 And they don't know how to deal with it.

334
00:19:54,560 --> 00:19:57,530
 What the Buddha is doing here is breaking that experience

335
00:19:57,530 --> 00:19:57,880
 up.

336
00:19:57,880 --> 00:19:59,140
 What happens when you're angry?

337
00:19:59,140 --> 00:20:05,560
 What happens when you're attached to something?

338
00:20:05,560 --> 00:20:07,590
 And when you break it up, you can see that there's nothing

339
00:20:07,590 --> 00:20:08,600
 really worth attaching to

340
00:20:08,600 --> 00:20:09,800
 at all.

341
00:20:09,800 --> 00:20:11,680
 When you feel happy, it's just a happy feeling.

342
00:20:11,680 --> 00:20:14,000
 There's nothing positive or negative about it.

343
00:20:14,000 --> 00:20:16,400
 It is what it is.

344
00:20:16,400 --> 00:20:18,740
 You can see that when you cling to it, when you say this is

345
00:20:18,740 --> 00:20:19,880
 good, you're not going to

346
00:20:19,880 --> 00:20:20,880
 prolong it.

347
00:20:20,880 --> 00:20:24,120
 You're just going to create a need for it.

348
00:20:24,120 --> 00:20:26,400
 An attachment to it.

349
00:20:26,400 --> 00:20:28,160
 It's not like you can say, "Oh, I like this.

350
00:20:28,160 --> 00:20:29,720
 Therefore it's going to stay longer.

351
00:20:29,720 --> 00:20:36,800
 It's going to stay longer than if I didn't like it."

352
00:20:36,800 --> 00:20:39,720
 Because it's exactly the case with negative emotions.

353
00:20:39,720 --> 00:20:42,930
 That you can't make them go away just because you don't

354
00:20:42,930 --> 00:20:44,400
 want them to be there.

355
00:20:44,400 --> 00:20:49,080
 Negative experience doesn't disappear just because you want

356
00:20:49,080 --> 00:20:49,920
 it to go.

357
00:20:49,920 --> 00:20:54,720
 Negative experience doesn't stay just because you want it

358
00:20:54,720 --> 00:20:55,680
 to stay.

359
00:20:55,680 --> 00:20:58,350
 When we come to see this, we come to see the nature of

360
00:20:58,350 --> 00:21:01,920
 these things is that they're impermanent.

361
00:21:01,920 --> 00:21:03,720
 They're unsure, uncertain.

362
00:21:03,720 --> 00:21:07,870
 They come and go according to their own nature, according

363
00:21:07,870 --> 00:21:10,760
 to the causes and effects that created

364
00:21:10,760 --> 00:21:28,000
 them.

365
00:21:28,000 --> 00:21:34,170
 You can pick any one of these parts, the object of your

366
00:21:34,170 --> 00:21:38,940
 desire or the object of your aversion.

367
00:21:38,940 --> 00:21:42,850
 You can pick the feeling that it gives rise to inside of

368
00:21:42,850 --> 00:21:45,120
 you or you can pick the emotion

369
00:21:45,120 --> 00:21:48,000
 that arises.

370
00:21:48,000 --> 00:21:50,310
 The important thing is that you pick it apart and see it

371
00:21:50,310 --> 00:21:51,760
 clearly and you're focusing on

372
00:21:51,760 --> 00:21:53,880
 something that's real.

373
00:21:53,880 --> 00:21:57,130
 Because just saying, "I'm addicted and that's that and I

374
00:21:57,130 --> 00:21:59,660
 can't stop myself," isn't at all

375
00:21:59,660 --> 00:22:01,260
 useful in any way.

376
00:22:01,260 --> 00:22:05,720
 To simply say that I'm an angry person also isn't useful.

377
00:22:05,720 --> 00:22:07,920
 It's not really understanding what's happening.

378
00:22:07,920 --> 00:22:15,560
 It's not seeing clearly what's going on.

379
00:22:15,560 --> 00:22:18,780
 Once you can pick it apart, if you can catch yourself at

380
00:22:18,780 --> 00:22:20,760
 the emotion, at the feeling, if

381
00:22:20,760 --> 00:22:26,200
 you feel pain or so on.

382
00:22:26,200 --> 00:22:30,430
 Once you see it clearly, then you find no reason to get

383
00:22:30,430 --> 00:22:32,120
 upset about it.

384
00:22:32,120 --> 00:22:33,400
 Instead of saying, "This is bad.

385
00:22:33,400 --> 00:22:35,640
 This is painful," you just say, "This is this.

386
00:22:35,640 --> 00:22:37,760
 This is what it is."

387
00:22:37,760 --> 00:22:43,840
 When there's pain, you know that there's pain.

388
00:22:43,840 --> 00:22:46,330
 When there's a pleasant feeling, instead of getting

389
00:22:46,330 --> 00:22:47,960
 addicted to it, suppose it's good

390
00:22:47,960 --> 00:22:54,970
 food or a beautiful sight, you're simply aware that it is

391
00:22:54,970 --> 00:22:56,680
 what it is.

392
00:22:56,680 --> 00:23:03,640
 It's a sight and it's a happy feeling that arises.

393
00:23:03,640 --> 00:23:06,060
 You don't see any reason to become addicted or attached to

394
00:23:06,060 --> 00:23:06,360
 it.

395
00:23:06,360 --> 00:23:08,520
 It doesn't make it last, as I said.

396
00:23:08,520 --> 00:23:18,220
 It doesn't do you any good and all it does is lead to

397
00:23:18,220 --> 00:23:23,360
 suffering when it's gone.

398
00:23:23,360 --> 00:23:29,490
 Because the alternative is to live our lives as ordinary

399
00:23:29,490 --> 00:23:33,320
 people who are uninterested in

400
00:23:33,320 --> 00:23:37,710
 mental development, live their lives, happy sometimes,

401
00:23:37,710 --> 00:23:40,280
 miserable sometimes, even to the

402
00:23:40,280 --> 00:23:43,910
 point where they try to kill themselves sometimes, having

403
00:23:43,910 --> 00:23:46,160
 to go through incredible stress and

404
00:23:46,160 --> 00:23:51,720
 suffering because they don't understand the experience of

405
00:23:51,720 --> 00:23:54,360
 reality in front of them.

406
00:23:54,360 --> 00:23:58,580
 It's not because their situation, there's anything wrong

407
00:23:58,580 --> 00:23:59,400
 with it.

408
00:23:59,400 --> 00:24:00,960
 It's that they don't understand what's happening.

409
00:24:00,960 --> 00:24:07,320
 They don't understand the nature of their experience.

410
00:24:07,320 --> 00:24:12,670
 They attribute it to being me and mine and they attribute

411
00:24:12,670 --> 00:24:15,200
 the idea that it somehow should

412
00:24:15,200 --> 00:24:20,320
 be forced and controlled and changed.

413
00:24:20,320 --> 00:24:25,690
 We segregate reality into the good and the bad, the

414
00:24:25,690 --> 00:24:29,600
 acceptable and the unacceptable.

415
00:24:29,600 --> 00:24:34,310
 And actually all there is is the physical and the mental

416
00:24:34,310 --> 00:24:37,040
 and the feelings that arise.

417
00:24:37,040 --> 00:24:40,880
 The problem that comes is when we react.

418
00:24:40,880 --> 00:24:50,040
 The problem is not in the objects themselves.

419
00:24:50,040 --> 00:24:52,980
 When we crave for something, when we need for something,

420
00:24:52,980 --> 00:24:54,440
 when we require that things

421
00:24:54,440 --> 00:24:58,790
 be other than what they are or when we require that things

422
00:24:58,790 --> 00:25:01,040
 stay the way they are and not

423
00:25:01,040 --> 00:25:02,040
 change.

424
00:25:02,040 --> 00:25:09,380
 Simply put, when we require things to be other than reality

425
00:25:09,380 --> 00:25:11,080
 dictates.

426
00:25:11,080 --> 00:25:15,040
 When reality dictates that things must change and we

427
00:25:15,040 --> 00:25:17,960
 require that it to be otherwise, this

428
00:25:17,960 --> 00:25:22,100
 is where suffering comes from.

429
00:25:22,100 --> 00:25:23,100
 We cling to it.

430
00:25:23,100 --> 00:25:30,110
 We say that it must be, we require it to be other than this

431
00:25:30,110 --> 00:25:30,160
.

432
00:25:30,160 --> 00:25:31,820
 We're not satisfied the way things are.

433
00:25:31,820 --> 00:25:36,060
 We have to go and seek out more.

434
00:25:36,060 --> 00:25:38,200
 We don't understand and see it for what it is.

435
00:25:38,200 --> 00:25:44,130
 We think it's unpleasant or it's bad or we think that this

436
00:25:44,130 --> 00:25:46,920
 is going to make me happy.

437
00:25:46,920 --> 00:26:00,080
 If I can just attain this, get this or that object.

438
00:26:00,080 --> 00:26:01,960
 And so we cling.

439
00:26:01,960 --> 00:26:06,440
 We refuse to accept change.

440
00:26:06,440 --> 00:26:09,640
 Refuse to accept things the way they are.

441
00:26:09,640 --> 00:26:10,920
 And this is what gives rise to suffering.

442
00:26:10,920 --> 00:26:20,280
 This is what sets us on a cycle of addiction or obsession.

443
00:26:20,280 --> 00:26:22,960
 This may be a better word.

444
00:26:22,960 --> 00:26:29,960
 Needing it to be like this, needing it not to be like that.

445
00:26:29,960 --> 00:26:34,680
 And the suffering that comes when it's not the way we want

446
00:26:34,680 --> 00:26:35,680
 it to be.

447
00:26:35,680 --> 00:26:38,640
 We don't see this in ordinary everyday life.

448
00:26:38,640 --> 00:26:44,850
 We don't see this when we're not observing, when we're not

449
00:26:44,850 --> 00:26:46,600
 meditating.

450
00:26:46,600 --> 00:26:49,530
 All we see is the suffering that comes from things not

451
00:26:49,530 --> 00:26:50,920
 being the way we want.

452
00:26:50,920 --> 00:26:58,520
 Even right now, I'm sure there's many things going on in

453
00:26:58,520 --> 00:27:00,400
 your experience that are unpleasant.

454
00:27:00,400 --> 00:27:03,680
 You're trying to change them.

455
00:27:03,680 --> 00:27:06,680
 Sitting here, maybe it's too hot, maybe it's too cold.

456
00:27:06,680 --> 00:27:10,580
 Maybe the seat is too hard and you have to shift your

457
00:27:10,580 --> 00:27:11,680
 position.

458
00:27:11,680 --> 00:27:14,020
 Maybe you don't like what I'm saying and it makes you upset

459
00:27:14,020 --> 00:27:15,080
 and gives you a headache

460
00:27:15,080 --> 00:27:28,800
 or so on.

461
00:27:28,800 --> 00:27:36,770
 It's our inability to see these things clearly, to see what

462
00:27:36,770 --> 00:27:39,680
's really going on that leads us

463
00:27:39,680 --> 00:27:43,600
 to obsession and to suffering.

464
00:27:43,600 --> 00:27:49,320
 Once we look at it, we see how amazing reality really is

465
00:27:49,320 --> 00:27:53,120
 and how amazing mindfulness really

466
00:27:53,120 --> 00:27:54,120
 is.

467
00:27:54,120 --> 00:27:57,280
 Simply seeing things for what they are.

468
00:27:57,280 --> 00:28:00,000
 Seeing things for what they are.

469
00:28:00,000 --> 00:28:07,140
 In a moment, you can do away with any suffering that arises

470
00:28:07,140 --> 00:28:07,560
.

471
00:28:07,560 --> 00:28:09,560
 You feel stressed.

472
00:28:09,560 --> 00:28:10,840
 You focus on the stress.

473
00:28:10,840 --> 00:28:12,720
 Just penetrate into it.

474
00:28:12,720 --> 00:28:13,800
 What's going on here?

475
00:28:13,800 --> 00:28:14,800
 What's happening?

476
00:28:14,800 --> 00:28:16,520
 What does it mean to say, "I am stressed.

477
00:28:16,520 --> 00:28:19,040
 I'm upset.

478
00:28:19,040 --> 00:28:20,040
 Where's the eye?

479
00:28:20,040 --> 00:28:21,040
 Where's the stress?

480
00:28:21,040 --> 00:28:22,040
 What's really going on?"

481
00:28:22,040 --> 00:28:27,960
 You just say to yourself, "Stressed, stressed, stressed."

482
00:28:27,960 --> 00:28:31,720
 Keeping your mind with it and seeing it simply for what it

483
00:28:31,720 --> 00:28:32,160
 is.

484
00:28:32,160 --> 00:28:34,320
 You realize there is no eye involved.

485
00:28:34,320 --> 00:28:36,880
 There's only a feeling of stress that arises.

486
00:28:36,880 --> 00:28:41,750
 When you see that there's nothing intrinsically wrong with

487
00:28:41,750 --> 00:28:44,000
 this tense state, it is what it

488
00:28:44,000 --> 00:28:45,000
 is.

489
00:28:45,000 --> 00:28:49,390
 It's something that's arisen and after some time will

490
00:28:49,390 --> 00:28:51,120
 disappear.

491
00:28:51,120 --> 00:29:00,360
 When you want something or when you're angry about

492
00:29:00,360 --> 00:29:03,000
 something, whatever the emotion is,

493
00:29:03,000 --> 00:29:07,110
 whatever is causing you stress and suffering, whatever is

494
00:29:07,110 --> 00:29:08,840
 getting in the way of your clear

495
00:29:08,840 --> 00:29:17,180
 understanding, your peace, your peaceful harmony with

496
00:29:17,180 --> 00:29:22,360
 reality, you penetrate into it.

497
00:29:22,360 --> 00:29:24,640
 You see it for what it is.

498
00:29:24,640 --> 00:29:26,880
 You see that there's many things going on.

499
00:29:26,880 --> 00:29:28,080
 You have happy feelings.

500
00:29:28,080 --> 00:29:32,360
 You have negative, unpleasant feelings.

501
00:29:32,360 --> 00:29:38,120
 You have these states of greed and anger.

502
00:29:38,120 --> 00:29:41,000
 They come and they go.

503
00:29:41,000 --> 00:29:44,650
 When you can see and understand these things, then you can

504
00:29:44,650 --> 00:29:46,640
 say to yourself, "I know it's

505
00:29:46,640 --> 00:29:50,240
 wrong and that's why I don't do it."

506
00:29:50,240 --> 00:29:53,940
 You'll never say to yourself, "Again, I know it's wrong but

507
00:29:53,940 --> 00:29:54,960
 I still do it."

508
00:29:54,960 --> 00:29:56,790
 You'll come to realize that you really don't know what's

509
00:29:56,790 --> 00:29:57,400
 wrong with it.

510
00:29:57,400 --> 00:30:01,330
 You really don't know the true nature of the experience and

511
00:30:01,330 --> 00:30:03,840
 why it's wrong to get angry

512
00:30:03,840 --> 00:30:06,460
 because when you really know that it's wrong to get angry

513
00:30:06,460 --> 00:30:07,880
 or greedy or so on, you won't

514
00:30:07,880 --> 00:30:08,960
 do it.

515
00:30:08,960 --> 00:30:12,450
 When you know that it's wrong to carry out some behavior,

516
00:30:12,450 --> 00:30:14,160
 when you truly have rizya or

517
00:30:14,160 --> 00:30:20,680
 knowledge, understand the situation, you'll never cause

518
00:30:20,680 --> 00:30:24,040
 suffering for yourself again.

519
00:30:24,040 --> 00:30:29,040
 So that was the teaching that I thought to discuss today.

520
00:30:29,040 --> 00:30:32,400
 I hope that was useful for some people, sort of as a guide

521
00:30:32,400 --> 00:30:34,240
 for where you should be going

522
00:30:34,240 --> 00:30:35,800
 in your meditation.

523
00:30:35,800 --> 00:30:40,120
 Thanks to everyone for coming and if you have any questions

524
00:30:40,120 --> 00:30:42,280
, I'm happy to take them now.

