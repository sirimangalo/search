1
00:00:00,000 --> 00:00:05,000
 Good evening everyone.

2
00:00:05,000 --> 00:00:18,400
 I'm broadcasting live May 24th.

3
00:00:18,400 --> 00:00:25,680
 Tonight's quote is an answer to an often voiced criticism

4
00:00:25,680 --> 00:00:28,620
 of Buddhism and meditation practice

5
00:00:28,620 --> 00:00:34,710
 in general they call meditation navel gazing or you're just

6
00:00:34,710 --> 00:00:38,120
 obsessed with yourself.

7
00:00:38,120 --> 00:00:44,060
 Navel gazing as though it's a useless or potentially

8
00:00:44,060 --> 00:00:48,200
 narcissistic, auto-erotic was what the Pope

9
00:00:48,200 --> 00:00:52,560
 called it, one of the previous popes.

10
00:00:52,560 --> 00:00:55,640
 Meditation gets a bad rap.

11
00:00:55,640 --> 00:01:02,270
 And we think of it as an escape running away from your

12
00:01:02,270 --> 00:01:05,520
 problems, a vacation, something

13
00:01:05,520 --> 00:01:12,280
 you do at a spa or something, something like yoga.

14
00:01:12,280 --> 00:01:19,810
 No, not to malign, no, to speak badly of yoga, but I don't

15
00:01:19,810 --> 00:01:24,040
 think meditation or meditation

16
00:01:24,040 --> 00:01:27,970
 that we do as much like yoga, even though people often

17
00:01:27,970 --> 00:01:30,160
 equate the two or compare the

18
00:01:30,160 --> 00:01:31,160
 two.

19
00:01:31,160 --> 00:01:36,570
 But nonetheless, not speaking about yoga, Buddhist

20
00:01:36,570 --> 00:01:39,960
 meditation most sincerely is not

21
00:01:39,960 --> 00:01:44,240
 an escape or a vacation.

22
00:01:44,240 --> 00:01:45,920
 Right?

23
00:01:45,920 --> 00:01:49,280
 My meditator's nodding.

24
00:01:49,280 --> 00:01:52,040
 It's a hard work.

25
00:01:52,040 --> 00:01:53,040
 It's training.

26
00:01:53,040 --> 00:01:56,440
 And there's come through this course can tell you a lot

27
00:01:56,440 --> 00:01:58,480
 more than you usually signed up

28
00:01:58,480 --> 00:01:59,480
 for.

29
00:01:59,480 --> 00:02:02,480
 Usually not what we had expected coming in.

30
00:02:02,480 --> 00:02:03,480
 And that's good.

31
00:02:03,480 --> 00:02:06,850
 It's not if it were what you expected, then it wouldn't be

32
00:02:06,850 --> 00:02:07,620
 working.

33
00:02:07,620 --> 00:02:10,880
 And that's an important point because it's enlightened.

34
00:02:10,880 --> 00:02:11,880
 We're talking about enlightenment.

35
00:02:11,880 --> 00:02:13,480
 We're talking about waking up.

36
00:02:13,480 --> 00:02:17,020
 And we're waking up, we mean waking up to truths that we

37
00:02:17,020 --> 00:02:18,160
 didn't know.

38
00:02:18,160 --> 00:02:20,320
 We didn't know.

39
00:02:20,320 --> 00:02:21,960
 How could it not surprise you?

40
00:02:21,960 --> 00:02:23,320
 How could it not shock you?

41
00:02:23,320 --> 00:02:28,120
 How could it not challenge you?

42
00:02:28,120 --> 00:02:35,210
 If it didn't, you couldn't really say that it was something

43
00:02:35,210 --> 00:02:38,560
 new or something that would

44
00:02:38,560 --> 00:02:40,800
 change you.

45
00:02:40,800 --> 00:02:43,640
 Something that would bring about positive change or

46
00:02:43,640 --> 00:02:45,680
 negative change could be similar.

47
00:02:45,680 --> 00:02:50,290
 Not to say that the change or the hard work is, that all

48
00:02:50,290 --> 00:02:53,400
 hard work is necessarily positive.

49
00:02:53,400 --> 00:02:55,880
 But meditation is hard work.

50
00:02:55,880 --> 00:03:02,010
 Nonetheless, the criticism being leveled here is that he

51
00:03:02,010 --> 00:03:05,800
 makes a comparison as Brahman does.

52
00:03:05,800 --> 00:03:12,760
 He says they perform sacrifices to God, I guess.

53
00:03:12,760 --> 00:03:14,440
 And so that benefits many people.

54
00:03:14,440 --> 00:03:17,300
 He's talking about general religious practice, putting

55
00:03:17,300 --> 00:03:19,360
 aside this Brahman's really ridiculous

56
00:03:19,360 --> 00:03:22,080
 ideas of what is beneficial.

57
00:03:22,080 --> 00:03:26,420
 They would kill, they would slaughter animals, or they

58
00:03:26,420 --> 00:03:29,240
 would sacrifice butter to the fire.

59
00:03:29,240 --> 00:03:31,240
 A lot of silly things.

60
00:03:31,240 --> 00:03:33,080
 I didn't say that was wholesome.

61
00:03:33,080 --> 00:03:41,790
 But let's look at wholesome activity like people who work

62
00:03:41,790 --> 00:03:43,960
 for social justice or people

63
00:03:43,960 --> 00:03:49,090
 who run charities, soup kitchens, or teachers, these kind

64
00:03:49,090 --> 00:03:50,200
 of things.

65
00:03:50,200 --> 00:03:54,400
 People who help the world.

66
00:03:54,400 --> 00:03:56,680
 And they say, well, that's true goodness.

67
00:03:56,680 --> 00:03:59,720
 What is this meditation?

68
00:03:59,720 --> 00:04:02,780
 And he says, the meditation, okay, we can accept that it

69
00:04:02,780 --> 00:04:04,480
 helps you, but that's all it

70
00:04:04,480 --> 00:04:07,400
 does is help you.

71
00:04:07,400 --> 00:04:08,400
 It's only good for yourself.

72
00:04:08,400 --> 00:04:13,700
 He says, I say that such a person is practicing something

73
00:04:13,700 --> 00:04:16,640
 that benefits only himself.

74
00:04:16,640 --> 00:04:21,170
 Which to anyone who's put sincere or serious thought into

75
00:04:21,170 --> 00:04:24,000
 the matter, certainly must sound

76
00:04:24,000 --> 00:04:25,760
 ridiculous.

77
00:04:25,760 --> 00:04:28,720
 But for people who are unsure or who are new to

78
00:04:28,720 --> 00:04:31,440
 spirituality, it sounds kind of convincing.

79
00:04:31,440 --> 00:04:35,760
 Yeah, why am I wasting my time in this meditation center?

80
00:04:35,760 --> 00:04:39,640
 Doing things that only benefit myself.

81
00:04:39,640 --> 00:04:43,520
 And agreed that there are meditations that I would say, for

82
00:04:43,520 --> 00:04:45,520
 the most part, only benefit

83
00:04:45,520 --> 00:04:46,520
 yourself.

84
00:04:46,520 --> 00:04:49,860
 If you enter into a trance, a bliss state, a peaceful state

85
00:04:49,860 --> 00:04:51,440
, that would be really only

86
00:04:51,440 --> 00:04:52,680
 benefiting yourself.

87
00:04:52,680 --> 00:04:57,240
 And in the end, not benefiting yourself substantially

88
00:04:57,240 --> 00:05:00,320
 anyway, because it's temporary.

89
00:05:00,320 --> 00:05:03,150
 But this is an important point because it helps clear up

90
00:05:03,150 --> 00:05:05,720
 this misunderstanding and this misconception

91
00:05:05,720 --> 00:05:08,680
 and this prejudice we have.

92
00:05:08,680 --> 00:05:12,280
 Preconception that we have that meditation should be

93
00:05:12,280 --> 00:05:15,560
 pleasant, meditation should be enjoyable.

94
00:05:15,560 --> 00:05:18,560
 It can be, of course, even this meditation can be at times

95
00:05:18,560 --> 00:05:19,400
 enjoyable.

96
00:05:19,400 --> 00:05:22,640
 That's not what it's supposed to be.

97
00:05:22,640 --> 00:05:27,630
 It's actually supposed to better you, make you, you could

98
00:05:27,630 --> 00:05:29,800
 say, a better person.

99
00:05:29,800 --> 00:05:34,520
 But more technically, just to make you more pure, more

100
00:05:34,520 --> 00:05:37,680
 clear, more mindful, more wise,

101
00:05:37,680 --> 00:05:40,120
 more good.

102
00:05:40,120 --> 00:05:43,290
 And if you hear these things and then you ask yourself, is

103
00:05:43,290 --> 00:05:44,960
 this not beneficial to other

104
00:05:44,960 --> 00:05:45,960
 people?

105
00:05:45,960 --> 00:05:50,400
 Or you could say, well, how is that beneficial to other

106
00:05:50,400 --> 00:05:51,320
 people?

107
00:05:51,320 --> 00:05:56,360
 And so we can take a look at those people who try to help

108
00:05:56,360 --> 00:05:58,400
 the world, right?

109
00:05:58,400 --> 00:06:04,160
 For social justice or all these things I mentioned.

110
00:06:04,160 --> 00:06:07,800
 And they're not all this, they're not all equal, right?

111
00:06:07,800 --> 00:06:10,200
 They're not all equally successful.

112
00:06:10,200 --> 00:06:12,720
 Many of them burn out.

113
00:06:12,720 --> 00:06:17,430
 There's a high in the environmental movement, which my

114
00:06:17,430 --> 00:06:20,520
 father was very much involved in.

115
00:06:20,520 --> 00:06:25,230
 He recently told me, I think there's a very high burnout

116
00:06:25,230 --> 00:06:26,000
 rate.

117
00:06:26,000 --> 00:06:29,180
 And people who are into social justice will tell you that

118
00:06:29,180 --> 00:06:30,560
 they flame bright and burn out

119
00:06:30,560 --> 00:06:34,660
 quick, and then they're on to something else, that the

120
00:06:34,660 --> 00:06:36,640
 passion doesn't last.

121
00:06:36,640 --> 00:06:38,520
 It's because they're not trained.

122
00:06:38,520 --> 00:06:43,080
 It's because they don't have this ability, this power.

123
00:06:43,080 --> 00:06:47,490
 And you don't really realize the power and the strength and

124
00:06:47,490 --> 00:06:49,340
 the clarity of mind that

125
00:06:49,340 --> 00:06:51,840
 comes from meditation until you actually do it.

126
00:06:51,840 --> 00:06:55,000
 That's why I say it surprises you.

127
00:06:55,000 --> 00:06:58,120
 Surprises us in how challenging it is, but it also

128
00:06:58,120 --> 00:07:00,280
 surprises us in how deep it goes and

129
00:07:00,280 --> 00:07:04,740
 how fundamentally it changes us, it strengthens us,

130
00:07:04,740 --> 00:07:06,680
 straightens us out.

131
00:07:06,680 --> 00:07:09,030
 You come out of this feeling all crooked and like you were

132
00:07:09,030 --> 00:07:10,240
 all bent out of shape and that

133
00:07:10,240 --> 00:07:16,950
 you've just been wrenched back into more or less straight

134
00:07:16,950 --> 00:07:18,200
 state.

135
00:07:18,200 --> 00:07:19,200
 That's how it feels.

136
00:07:19,200 --> 00:07:21,920
 It feels like you're untying knots.

137
00:07:21,920 --> 00:07:24,960
 That's how it should feel if you're doing it properly, unt

138
00:07:24,960 --> 00:07:26,800
ying knots, like you're working

139
00:07:26,800 --> 00:07:31,620
 out kinks, like you're straightening out the crookedness in

140
00:07:31,620 --> 00:07:32,720
 your mind.

141
00:07:32,720 --> 00:07:35,200
 Your mind is all bent out of shape.

142
00:07:35,200 --> 00:07:38,200
 I mean bent out of shape is a simplification.

143
00:07:38,200 --> 00:07:41,790
 It's all messed up, mixed up, because what we do doesn't

144
00:07:41,790 --> 00:07:43,360
 have rhyme or reason.

145
00:07:43,360 --> 00:07:48,190
 Much of it is just based on when our habits are not well

146
00:07:48,190 --> 00:07:49,680
 thought out.

147
00:07:49,680 --> 00:07:51,880
 We've been working on our habits since we were children.

148
00:07:51,880 --> 00:07:55,560
 How could we know what was right and what was wrong?

149
00:07:55,560 --> 00:08:00,390
 We go through life with a half-assed understanding, half-

150
00:08:00,390 --> 00:08:04,160
assed, probably not the technical term,

151
00:08:04,160 --> 00:08:10,290
 half-baked, not fully formed understanding of what's right

152
00:08:10,290 --> 00:08:12,400
 and what's wrong.

153
00:08:12,400 --> 00:08:13,960
 We make lots of mistakes.

154
00:08:13,960 --> 00:08:16,320
 We do right sometimes.

155
00:08:16,320 --> 00:08:18,440
 We figure some things out.

156
00:08:18,440 --> 00:08:22,400
 We all have varying degrees of wisdom, so we use that.

157
00:08:22,400 --> 00:08:26,240
 It's not fully formed.

158
00:08:26,240 --> 00:08:27,240
 It's not coherent.

159
00:08:27,240 --> 00:08:31,080
 It's all mixed up.

160
00:08:31,080 --> 00:08:32,800
 Meditation is quite simple.

161
00:08:32,800 --> 00:08:36,490
 It's not something you can doubt, because it's quite a

162
00:08:36,490 --> 00:08:37,920
 simple activity.

163
00:08:37,920 --> 00:08:42,170
 It's straightening everything out, straightening out our

164
00:08:42,170 --> 00:08:45,480
 minds, working to understand our desires,

165
00:08:45,480 --> 00:08:50,100
 our versions, our conceits and our arrogance and our views

166
00:08:50,100 --> 00:08:53,600
 and opinions, and overcome all

167
00:08:53,600 --> 00:08:59,560
 of the delusion that we have inside.

168
00:08:59,560 --> 00:09:02,880
 Meditation really straightens us out, and there's nothing

169
00:09:02,880 --> 00:09:04,800
 better for other people than to be

170
00:09:04,800 --> 00:09:07,000
 straightened out yourself.

171
00:09:07,000 --> 00:09:10,270
 If you're straight yourself, first of all, if everyone did

172
00:09:10,270 --> 00:09:11,880
 this, there would be no need

173
00:09:11,880 --> 00:09:13,920
 to help anyone else, right?

174
00:09:13,920 --> 00:09:16,720
 If everyone practiced meditation, then no one would need to

175
00:09:16,720 --> 00:09:17,760
 teach meditation.

176
00:09:17,760 --> 00:09:20,800
 No one would need to help the world.

177
00:09:20,800 --> 00:09:24,640
 We have more than enough for everyone, and even enough is

178
00:09:24,640 --> 00:09:26,960
 not really meaningful, because

179
00:09:26,960 --> 00:09:29,280
 the human state is just an artifice.

180
00:09:29,280 --> 00:09:33,040
 We don't really need all of the things that a human has.

181
00:09:33,040 --> 00:09:37,120
 We can give up this human state and go to a purer state.

182
00:09:37,120 --> 00:09:41,680
 We can change the whole fabric of reality.

183
00:09:41,680 --> 00:09:44,660
 Maybe that's going too far for most people's understanding,

184
00:09:44,660 --> 00:09:46,120
 but we can certainly change

185
00:09:46,120 --> 00:09:52,240
 the world if we're all positive.

186
00:09:52,240 --> 00:09:55,520
 But more than just all being well inside, our interactions

187
00:09:55,520 --> 00:09:57,400
 with others, how much suffering

188
00:09:57,400 --> 00:10:07,480
 do we cause in the name of beneficence, trying to do good

189
00:10:07,480 --> 00:10:08,520
 things?

190
00:10:08,520 --> 00:10:14,960
 The Spanish Inquisition was ostensibly meant for benefit.

191
00:10:14,960 --> 00:10:20,750
 Hitler had an idea of beneficence that he was actually

192
00:10:20,750 --> 00:10:23,920
 helping the world by culling

193
00:10:23,920 --> 00:10:25,800
 the lesser, right?

194
00:10:25,800 --> 00:10:29,480
 But these are extreme examples, but we're all in this way.

195
00:10:29,480 --> 00:10:32,040
 We try to help the world, and we end up yelling and

196
00:10:32,040 --> 00:10:34,600
 screaming and getting frustrated and burning

197
00:10:34,600 --> 00:10:35,940
 out.

198
00:10:35,940 --> 00:10:40,950
 We still get addicted and attached to our desires that mess

199
00:10:40,950 --> 00:10:43,080
 up and color our work and

200
00:10:43,080 --> 00:10:48,400
 our beliefs.

201
00:10:48,400 --> 00:10:50,560
 This should be a really...

202
00:10:50,560 --> 00:10:55,470
 This sort of argument is voiced far too often by people who

203
00:10:55,470 --> 00:10:57,800
 clearly, if they voice it, clearly

204
00:10:57,800 --> 00:11:02,520
 have not thought or investigated the topic at all.

205
00:11:02,520 --> 00:11:07,740
 It's easily debunked, but only if you've taken the time to

206
00:11:07,740 --> 00:11:10,360
 think and to work and follow the

207
00:11:10,360 --> 00:11:14,560
 meditation practice to see the benefit, to see that it's

208
00:11:14,560 --> 00:11:16,600
 not just navel-gazing.

209
00:11:16,600 --> 00:11:21,080
 Although the truth is right at our navel.

210
00:11:21,080 --> 00:11:25,310
 If you do watch your stomach rising and falling, you can

211
00:11:25,310 --> 00:11:26,920
 change the world.

212
00:11:26,920 --> 00:11:30,620
 You can become enlightened just by watching your stomach

213
00:11:30,620 --> 00:11:31,760
 rise and fall.

214
00:11:31,760 --> 00:11:36,840
 It's quite profound how simple it is.

215
00:11:36,840 --> 00:11:41,840
 Anyway, so that's a bit of dhamma for tonight.

216
00:11:41,840 --> 00:11:45,840
 I think that's all I have to say about that.

217
00:11:45,840 --> 00:11:48,960
 Let's look at some of the questions.

218
00:11:48,960 --> 00:11:52,920
 Okay, question verse 37.

219
00:11:52,920 --> 00:11:57,920
 "I have a part-time job, live at home with my mom and dad,

220
00:11:57,920 --> 00:12:01,240
 not a very complicated person.

221
00:12:01,240 --> 00:12:04,260
 Is it not advisable to live a simple life and still

222
00:12:04,260 --> 00:12:05,840
 practice meditation?"

223
00:12:05,840 --> 00:12:06,840
 Of course.

224
00:12:06,840 --> 00:12:09,840
 I mean, that's what monastic life is supposed to be.

225
00:12:09,840 --> 00:12:12,240
 The monk life is...

226
00:12:12,240 --> 00:12:13,800
 I don't know what verse 37 was.

227
00:12:13,800 --> 00:12:21,000
 I can't think that far back, but living simply is great.

228
00:12:21,000 --> 00:12:24,910
 The idea of becoming a monk, the claim is it's the simplest

229
00:12:24,910 --> 00:12:25,920
 way of life.

230
00:12:25,920 --> 00:12:32,360
 You put aside everything just to cultivate spirituality.

231
00:12:32,360 --> 00:12:38,720
 Okay, more questions, long questions.

232
00:12:38,720 --> 00:12:41,670
 "I've been having doubts as to whether I actually want to

233
00:12:41,670 --> 00:12:43,960
 free my life of suffering and desire."

234
00:12:43,960 --> 00:12:46,320
 Well, they're two different things.

235
00:12:46,320 --> 00:12:50,950
 Desire wouldn't be a problem if it didn't lead to suffering

236
00:12:50,950 --> 00:12:51,080
.

237
00:12:51,080 --> 00:12:52,480
 Sounds quite strange and ignorant.

238
00:12:52,480 --> 00:12:53,840
 Well, it's natural.

239
00:12:53,840 --> 00:12:57,160
 Most of us, this was the Dhammapada verse last night.

240
00:12:57,160 --> 00:12:58,560
 I just re-recorded it.

241
00:12:58,560 --> 00:13:00,240
 It'll be up soon.

242
00:13:00,240 --> 00:13:01,440
 So maybe you can watch that.

243
00:13:01,440 --> 00:13:02,440
 Maybe that'll help.

244
00:13:02,440 --> 00:13:04,680
 "I feel that suffering and desire and all the things that

245
00:13:04,680 --> 00:13:05,880
 go along with them are part

246
00:13:05,880 --> 00:13:07,440
 of the typical human experience.

247
00:13:07,440 --> 00:13:10,160
 And in some way, I feel that I would be missing out.

248
00:13:10,160 --> 00:13:13,480
 I tried to remove them from my life.

249
00:13:13,480 --> 00:13:15,530
 So many of the greatest human achievements have been

250
00:13:15,530 --> 00:13:16,720
 inspired and motivated by these

251
00:13:16,720 --> 00:13:17,720
 things."

252
00:13:17,720 --> 00:13:18,720
 What would you say about this?

253
00:13:18,720 --> 00:13:20,160
 It's a very good question.

254
00:13:20,160 --> 00:13:24,680
 I mean, it's basically the Dhammapada from last night.

255
00:13:24,680 --> 00:13:26,680
 We do things for...

256
00:13:26,680 --> 00:13:28,840
 Well, it's not based.

257
00:13:28,840 --> 00:13:32,600
 It's in the same vein.

258
00:13:32,600 --> 00:13:33,600
 We do things in the world.

259
00:13:33,600 --> 00:13:35,120
 We all have these things in the world.

260
00:13:35,120 --> 00:13:37,080
 We want to achieve things.

261
00:13:37,080 --> 00:13:38,960
 We want to obtain things.

262
00:13:38,960 --> 00:13:41,120
 We have many desires.

263
00:13:41,120 --> 00:13:48,140
 I mean, the first thing I can say before getting really

264
00:13:48,140 --> 00:13:52,000
 into it is that it's not a reason to

265
00:13:52,000 --> 00:13:54,920
 desire.

266
00:13:54,920 --> 00:13:57,920
 Your desire is not a reason to desire.

267
00:13:57,920 --> 00:13:58,920
 You see what I mean?

268
00:13:58,920 --> 00:14:02,400
 You say, "I'm not sure if I want to give up desire."

269
00:14:02,400 --> 00:14:04,560
 It's kind of funny because of course you don't.

270
00:14:04,560 --> 00:14:08,100
 That's what desire means.

271
00:14:08,100 --> 00:14:09,100
 We desire it.

272
00:14:09,100 --> 00:14:14,600
 We desire it for...of course we don't want to get rid of it

273
00:14:14,600 --> 00:14:14,800
.

274
00:14:14,800 --> 00:14:20,020
 That kind of points to the means of overcoming the problem.

275
00:14:20,020 --> 00:14:21,840
 You can't approach desire directly.

276
00:14:21,840 --> 00:14:25,230
 This is why the Buddha...he put desire in a special

277
00:14:25,230 --> 00:14:25,960
 category.

278
00:14:25,960 --> 00:14:29,120
 Ajahn Tong brought this up and really drew my attention to

279
00:14:29,120 --> 00:14:30,920
 this, the difference between

280
00:14:30,920 --> 00:14:34,040
 anger, for example, anger and greed.

281
00:14:34,040 --> 00:14:35,040
 Anger is something you give up.

282
00:14:35,040 --> 00:14:36,040
 You know it's bad.

283
00:14:36,040 --> 00:14:37,040
 It feels bad.

284
00:14:37,040 --> 00:14:38,240
 It's not something we want.

285
00:14:38,240 --> 00:14:39,880
 We don't want to be angry.

286
00:14:39,880 --> 00:14:43,760
 But greed, greed is something that can be pleasant.

287
00:14:43,760 --> 00:14:50,200
 So Manasa Sahagatani can come associated with pleasure.

288
00:14:50,200 --> 00:14:52,550
 And so it's something that you have to train yourself out

289
00:14:52,550 --> 00:14:52,820
 of.

290
00:14:52,820 --> 00:14:56,320
 It's not something you can just say, "No, I don't want."

291
00:14:56,320 --> 00:14:58,640
 So it's something that we can approach directly.

292
00:14:58,640 --> 00:15:01,920
 But this is why we have to separate desire and suffering.

293
00:15:01,920 --> 00:15:03,040
 Don't worry about desire.

294
00:15:03,040 --> 00:15:05,400
 Don't worry about the things that you want.

295
00:15:05,400 --> 00:15:07,600
 That's not useful.

296
00:15:07,600 --> 00:15:09,600
 That's not where we have to focus.

297
00:15:09,600 --> 00:15:10,800
 Let's not focus on that.

298
00:15:10,800 --> 00:15:12,720
 Okay, you want all these things?

299
00:15:12,720 --> 00:15:13,720
 Fine.

300
00:15:13,720 --> 00:15:15,240
 Let's look at the fact that you're suffering.

301
00:15:15,240 --> 00:15:16,480
 Why are you suffering?

302
00:15:16,480 --> 00:15:17,600
 Let's learn about that.

303
00:15:17,600 --> 00:15:19,160
 Let's study that.

304
00:15:19,160 --> 00:15:25,540
 And this helps you not get mixed up because desire...when

305
00:15:25,540 --> 00:15:28,360
 the mind has desire in it, it's

306
00:15:28,360 --> 00:15:32,270
 unable, incapable of seeing the detriment, seeing the

307
00:15:32,270 --> 00:15:33,120
 problem.

308
00:15:33,120 --> 00:15:35,760
 This is what I was saying about the Dhammapada.

309
00:15:35,760 --> 00:15:36,760
 It's not rational.

310
00:15:36,760 --> 00:15:40,240
 You can't convince yourself not to want something.

311
00:15:40,240 --> 00:15:45,240
 The mind is in a state that it won't hear those arguments.

312
00:15:45,240 --> 00:15:49,880
 So that's not where you put your attention.

313
00:15:49,880 --> 00:15:54,020
 When you focus on the suffering and try to learn why you're

314
00:15:54,020 --> 00:15:56,400
 suffering and try to experience

315
00:15:56,400 --> 00:16:00,500
 and see what's going on that's causing your suffering, then

316
00:16:00,500 --> 00:16:02,640
 you really can break it apart.

317
00:16:02,640 --> 00:16:05,600
 Then you can see things rationally and clearly.

318
00:16:05,600 --> 00:16:09,960
 Then you can see how your desires are causing you stress.

319
00:16:09,960 --> 00:16:11,800
 It's not intellectual.

320
00:16:11,800 --> 00:16:16,970
 You'll just feel kind of exhausted, wanting the same thing

321
00:16:16,970 --> 00:16:19,560
 again and again, getting it

322
00:16:19,560 --> 00:16:24,220
 and then not being satisfied, not getting it and being

323
00:16:24,220 --> 00:16:27,160
 dissatisfied and again and again

324
00:16:27,160 --> 00:16:28,720
 and again.

325
00:16:28,720 --> 00:16:32,180
 And eventually you get bored of it.

326
00:16:32,180 --> 00:16:36,080
 This is how spiritual people are.

327
00:16:36,080 --> 00:16:38,400
 Someone who is very spiritual will feel like this.

328
00:16:38,400 --> 00:16:41,360
 They'll feel generally bored of life.

329
00:16:41,360 --> 00:16:45,880
 Like they've tried everything and they saw through it.

330
00:16:45,880 --> 00:16:49,180
 This is a sign of high-mindedness.

331
00:16:49,180 --> 00:16:52,390
 People who are depressed and want to kill themselves, often

332
00:16:52,390 --> 00:16:53,520
 it comes from a sort of

333
00:16:53,520 --> 00:16:56,760
 a wisdom, an understanding that there's just nothing to

334
00:16:56,760 --> 00:16:57,320
 life.

335
00:16:57,320 --> 00:17:01,680
 Life is, in the end, just a game.

336
00:17:01,680 --> 00:17:06,560
 You play the game enough, you get tired of it.

337
00:17:06,560 --> 00:17:08,200
 Most of us are not tired of it.

338
00:17:08,200 --> 00:17:11,160
 We're still keen on going after it.

339
00:17:11,160 --> 00:17:13,040
 That won't come unless you meditate.

340
00:17:13,040 --> 00:17:17,300
 It starts with what is most coarse and most obvious, people

341
00:17:17,300 --> 00:17:19,280
 who are addicted to really

342
00:17:19,280 --> 00:17:22,560
 unpleasant things like killing or stealing or lying or

343
00:17:22,560 --> 00:17:24,440
 cheating or drugs or alcohol.

344
00:17:24,440 --> 00:17:30,120
 People who are addicted to those things are pretty well

345
00:17:30,120 --> 00:17:32,640
 able to let go of them because

346
00:17:32,640 --> 00:17:34,320
 they're intense suffering.

347
00:17:34,320 --> 00:17:37,740
 Now attachment to music or food or something like that is

348
00:17:37,740 --> 00:17:39,520
 much harder to see and most of

349
00:17:39,520 --> 00:17:42,000
 us can't or won't ever see it.

350
00:17:42,000 --> 00:17:44,730
 But when you look, the truth is, regardless of whether we

351
00:17:44,730 --> 00:17:46,040
'll see it or not or whether

352
00:17:46,040 --> 00:17:51,240
 we want it or not, the truth is when you look clearly, you

353
00:17:51,240 --> 00:17:53,480
 will give it up because you will

354
00:17:53,480 --> 00:17:57,700
 see undeniably, you don't have to be convinced and you don

355
00:17:57,700 --> 00:18:00,040
't have to convince yourself, you

356
00:18:00,040 --> 00:18:03,810
 will see without any doubt, any shred of doubt that it's

357
00:18:03,810 --> 00:18:04,960
 not worth it.

358
00:18:04,960 --> 00:18:05,960
 It's not beneficial.

359
00:18:05,960 --> 00:18:06,960
 It's not pleasant.

360
00:18:06,960 --> 00:18:08,120
 It doesn't lead to happiness.

361
00:18:08,120 --> 00:18:09,720
 It doesn't make you a better person.

362
00:18:09,720 --> 00:18:12,960
 It doesn't make you a happier person.

363
00:18:12,960 --> 00:18:15,790
 The funny thing about these things is if they did in

364
00:18:15,790 --> 00:18:18,040
 general make us happier people, then

365
00:18:18,040 --> 00:18:20,860
 we would see ourselves constantly getting happier as we

366
00:18:20,860 --> 00:18:22,480
 pursue these things, which in

367
00:18:22,480 --> 00:18:24,720
 fact is not the case.

368
00:18:24,720 --> 00:18:28,150
 There are things in life that do make us happier, but they

369
00:18:28,150 --> 00:18:30,000
 are not seeking out pleasure.

370
00:18:30,000 --> 00:18:32,880
 You don't become happier the more you listen to music.

371
00:18:32,880 --> 00:18:35,370
 When you listen to the music that you like, you're happy

372
00:18:35,370 --> 00:18:36,640
 and if you vary it enough so

373
00:18:36,640 --> 00:18:40,200
 that it's not repetitive, your brain is constantly

374
00:18:40,200 --> 00:18:42,840
 stimulated, you can maintain that.

375
00:18:42,840 --> 00:18:45,720
 But you don't become happier.

376
00:18:45,720 --> 00:18:47,240
 Food doesn't make you happier.

377
00:18:47,240 --> 00:18:48,800
 Sex doesn't make you happier.

378
00:18:48,800 --> 00:18:54,040
 It makes you happy, pleasant, pleasure in that moment.

379
00:18:54,040 --> 00:18:58,040
 Not happier as a person, unhappier.

380
00:18:58,040 --> 00:19:02,970
 If you, for a hiddenness, a person who is intent upon this,

381
00:19:02,970 --> 00:19:04,960
 have them stop and put them

382
00:19:04,960 --> 00:19:08,320
 in a room where they no longer have these things.

383
00:19:08,320 --> 00:19:13,080
 See how they fare compared to a person, an ordinary person.

384
00:19:13,080 --> 00:19:16,720
 They behave very much like a drug addict.

385
00:19:16,720 --> 00:19:19,080
 This is why jail is torture for so many of us.

386
00:19:19,080 --> 00:19:22,710
 There was this sensory deprivation chamber that they're

387
00:19:22,710 --> 00:19:24,000
 talking about where you can't

388
00:19:24,000 --> 00:19:25,000
 hear anything.

389
00:19:25,000 --> 00:19:27,840
 It's a perfect silence.

390
00:19:27,840 --> 00:19:30,280
 And they were saying people couldn't stay in there for more

391
00:19:30,280 --> 00:19:31,360
 than 45 minutes.

392
00:19:31,360 --> 00:19:34,560
 They just started to go insane.

393
00:19:34,560 --> 00:19:37,960
 Now I can imagine it has an effect on the brain's disorient

394
00:19:37,960 --> 00:19:39,480
ing, but most of that is

395
00:19:39,480 --> 00:19:41,600
 simply because we desire stimulus.

396
00:19:41,600 --> 00:19:44,800
 Anyway, so I hope that helps.

397
00:19:44,800 --> 00:19:56,870
 When a person sleeps too few hours, they may feel drowsy,

398
00:19:56,870 --> 00:20:03,120
 forgetful, have difficulty

399
00:20:03,120 --> 00:20:05,040
 being aware, have difficulty being mindful.

400
00:20:05,040 --> 00:20:07,440
 It can feel a little like being drunk.

401
00:20:07,440 --> 00:20:10,520
 But in Buddhism, we are taught to limit our sleep.

402
00:20:10,520 --> 00:20:15,600
 How do we understand this apparent conflict?

403
00:20:15,600 --> 00:20:23,050
 Well the reason we feel so drowsy and drunk is because of

404
00:20:23,050 --> 00:20:27,440
 our bodies being accustomed

405
00:20:27,440 --> 00:20:31,470
 to sleeping, but also not even just that, also because of

406
00:20:31,470 --> 00:20:33,440
 how tired out we become from

407
00:20:33,440 --> 00:20:35,400
 our mental activity.

408
00:20:35,400 --> 00:20:37,950
 If you're meditating intensely, you don't even need to

409
00:20:37,950 --> 00:20:38,440
 sleep.

410
00:20:38,440 --> 00:20:41,840
 There are people who go days, weeks, months without sleep.

411
00:20:41,840 --> 00:20:45,460
 Your body acclimatizes to it so that drunk feeling reduces

412
00:20:45,460 --> 00:20:47,320
 and eventually doesn't even

413
00:20:47,320 --> 00:20:49,920
 arise.

414
00:20:49,920 --> 00:20:55,050
 And also your mind becomes more refined and more

415
00:20:55,050 --> 00:20:55,400
 streamlined.

416
00:20:55,400 --> 00:20:59,630
 In the beginning, it's also quite useful, and in the long

417
00:20:59,630 --> 00:21:01,720
 term, it's useful in terms

418
00:21:01,720 --> 00:21:04,560
 of pushing yourself.

419
00:21:04,560 --> 00:21:09,270
 When you push yourself beyond what you're comfortable with,

420
00:21:09,270 --> 00:21:12,000
 and most of us are not comfortable

421
00:21:12,000 --> 00:21:14,360
 with sleeping a little, then it agitates.

422
00:21:14,360 --> 00:21:18,660
 You're able to see your reactions and you're able to assess

423
00:21:18,660 --> 00:21:20,200
 your reactions.

424
00:21:20,200 --> 00:21:23,230
 You're able to see what reactions are wholesome and what

425
00:21:23,230 --> 00:21:24,640
 you're unwholesome.

426
00:21:24,640 --> 00:21:29,120
 If you always get what you want, you'll never come to see

427
00:21:29,120 --> 00:21:32,440
 the problem with desire, because

428
00:21:32,440 --> 00:21:34,960
 you're always getting it.

429
00:21:34,960 --> 00:21:35,960
 You say, "Well, that's good.

430
00:21:35,960 --> 00:21:36,960
 I want, and therefore I get.

431
00:21:36,960 --> 00:21:38,840
 When I want, I get."

432
00:21:38,840 --> 00:21:42,270
 So when you start depriving yourself of things that you

433
00:21:42,270 --> 00:21:44,720
 want, sleep being a very good example,

434
00:21:44,720 --> 00:21:50,300
 you start to see how we're just like baby cows crying out

435
00:21:50,300 --> 00:21:53,040
 for our mother's milk.

436
00:21:53,040 --> 00:21:58,200
 You want to train the cow to grow up to be a good ox.

437
00:21:58,200 --> 00:22:02,450
 In old times, they used ox and they had to train the ox,

438
00:22:02,450 --> 00:22:05,160
 but a baby ox was kind of useless.

439
00:22:05,160 --> 00:22:06,920
 So we had to train it.

440
00:22:06,920 --> 00:22:10,210
 You had to take it away from its mother, kind of cruel, I

441
00:22:10,210 --> 00:22:10,720
 know.

442
00:22:10,720 --> 00:22:13,810
 But in the long term, it was for the betterment of the ox's

443
00:22:13,810 --> 00:22:14,520
 training.

444
00:22:14,520 --> 00:22:17,820
 Now our minds are, it's maybe cruel to ox, but it's not

445
00:22:17,820 --> 00:22:19,240
 cruel to our minds.

446
00:22:19,240 --> 00:22:20,880
 Our minds, we do have to train.

447
00:22:20,880 --> 00:22:23,320
 They are something that is necessary.

448
00:22:23,320 --> 00:22:27,560
 Our training, our minds, as I said, they're all mixed up.

449
00:22:27,560 --> 00:22:30,600
 A natural mind state is a chaotic mind state.

450
00:22:30,600 --> 00:22:34,360
 It doesn't work for one's benefit at all times.

451
00:22:34,360 --> 00:22:36,820
 You might say it's natural and you say, "Well, isn't that

452
00:22:36,820 --> 00:22:37,680
 the way of life?

453
00:22:37,680 --> 00:22:39,120
 Isn't that the way of humans?"

454
00:22:39,120 --> 00:22:40,120
 Sure.

455
00:22:40,120 --> 00:22:41,120
 And look at the world.

456
00:22:41,120 --> 00:22:43,200
 Look at the way of humans, what it's brought.

457
00:22:43,200 --> 00:22:45,320
 It doesn't have to be this way.

458
00:22:45,320 --> 00:22:48,340
 Just because it has been this way or just because it seems

459
00:22:48,340 --> 00:22:49,640
 like evolution has made it

460
00:22:49,640 --> 00:22:52,780
 this way is in no way indicative that it should be this way

461
00:22:52,780 --> 00:22:54,760
 or that it's better this way.

462
00:22:54,760 --> 00:22:58,750
 In fact, if anything, human beings have shown in terms of

463
00:22:58,750 --> 00:23:01,000
 evolution is that evolution was

464
00:23:01,000 --> 00:23:05,520
 in no way a gift from God.

465
00:23:05,520 --> 00:23:15,940
 It was just a random, chaotic sort of way of promoting

466
00:23:15,940 --> 00:23:19,280
 certain genes, certain genetic

467
00:23:19,280 --> 00:23:21,400
 material.

468
00:23:21,400 --> 00:23:24,400
 Being human has shown us that we can get beyond our

469
00:23:24,400 --> 00:23:27,400
 genetics, beyond evolution, beyond natural

470
00:23:27,400 --> 00:23:28,400
 selection.

471
00:23:28,400 --> 00:23:33,220
 We can be compassionate to cripples and sick and to all

472
00:23:33,220 --> 00:23:36,160
 types of people who would, in a

473
00:23:36,160 --> 00:23:40,280
 survival of the fittest type of society, just not survive.

474
00:23:40,280 --> 00:23:46,280
 Anyway, I've got a little off track there, but I think that

475
00:23:46,280 --> 00:23:49,040
's a fairly comprehensive

476
00:23:49,040 --> 00:23:50,040
 answer.

477
00:23:50,040 --> 00:23:55,380
 There's several reasons to challenge you, but also to force

478
00:23:55,380 --> 00:23:57,440
 you to refine your mind

479
00:23:57,440 --> 00:24:03,160
 and that eventually those things go away.

480
00:24:03,160 --> 00:24:06,240
 Is nimbana the greatest human achievement?

481
00:24:06,240 --> 00:24:09,380
 You sound like humans, sound like you're saying you're

482
00:24:09,380 --> 00:24:12,600
 asking whether humans created nimbana.

483
00:24:12,600 --> 00:24:16,900
 The greatest achievement a human can achieve is not

484
00:24:16,900 --> 00:24:20,640
 something that humans created or something.

485
00:24:20,640 --> 00:24:24,650
 But yeah, it's the greatest thing a human could ever

486
00:24:24,650 --> 00:24:26,320
 achieve is nimbana for sure.

487
00:24:26,320 --> 00:24:31,160
 It's the only thing that's of real benefit.

488
00:24:31,160 --> 00:24:36,720
 Because it's permanent, it's satisfying.

489
00:24:36,720 --> 00:24:44,320
 It's not controllable, but it's not important.

490
00:24:44,320 --> 00:24:50,100
 Okay, still we're about 50/50, people who meditate, people

491
00:24:50,100 --> 00:24:52,000
 who don't meditate.

492
00:24:52,000 --> 00:24:54,360
 If your name is an orange, that means you don't meditate.

493
00:24:54,360 --> 00:24:58,880
 I'd like you to see a greater percentage.

494
00:24:58,880 --> 00:25:01,880
 Anyway, I hope that's all for tonight.

495
00:25:01,880 --> 00:25:03,480
 I'm wishing you all a good night.

496
00:25:03,480 --> 00:25:04,560
 I'll see you all next time.

497
00:25:04,560 --> 00:25:05,560
 Bye.

498
00:25:05,560 --> 00:25:06,560
 [END]

499
00:25:06,560 --> 00:25:08,560
 1

500
00:25:08,560 --> 00:25:09,560
 1

501
00:25:09,560 --> 00:25:10,560
 1

502
00:25:10,560 --> 00:25:11,560
 1

503
00:25:11,560 --> 00:25:12,560
 1

504
00:25:12,560 --> 00:25:13,560
 1

505
00:25:13,560 --> 00:25:14,560
 1

