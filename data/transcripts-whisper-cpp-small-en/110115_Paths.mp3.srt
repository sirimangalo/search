1
00:00:00,000 --> 00:00:05,620
 Hi and welcome back to Ask a Monk. After a long break I'm

2
00:00:05,620 --> 00:00:12,240
 back and I'll be trying to give regular teachings and

3
00:00:12,240 --> 00:00:15,000
 regular updates to the Ask a Monk program.

4
00:00:15,000 --> 00:00:18,380
 I've got lots of questions to answer now so hopefully I'll

5
00:00:18,380 --> 00:00:21,000
 get around to some of them in the near future.

6
00:00:21,000 --> 00:00:25,550
 I'm settling down here in Sri Lanka so you're going to see

7
00:00:25,550 --> 00:00:30,310
 hopefully some of the Sri Lankan countryside in these next

8
00:00:30,310 --> 00:00:31,000
 videos.

9
00:00:31,000 --> 00:00:39,270
 So the next question comes from Foster's Blue who says, "I

10
00:00:39,270 --> 00:00:43,070
 find my interest in reading and focus in Zen, also Tibetan

11
00:00:43,070 --> 00:00:45,000
 Buddhism and Christianity.

12
00:00:45,000 --> 00:00:49,500
 Interest in Islam and Sufi writings. Paths cause me to

13
00:00:49,500 --> 00:00:54,100
 wonder if I will have to choose or may I learn from various

14
00:00:54,100 --> 00:00:55,000
 paths?"

15
00:00:55,000 --> 00:01:01,630
 That's a really good question. It's one that was asked

16
00:01:01,630 --> 00:01:05,590
 quite often in the Buddha's time because like today in that

17
00:01:05,590 --> 00:01:08,640
 time there were many different paths, many different

18
00:01:08,640 --> 00:01:09,000
 teachers.

19
00:01:09,000 --> 00:01:21,290
 And I think it's important to agree that, I think most of

20
00:01:21,290 --> 00:01:27,430
 us can agree that it's unhealthy and unuseful to dog

21
00:01:27,430 --> 00:01:31,650
matically cling to a specific path and say this path is

22
00:01:31,650 --> 00:01:35,000
 right and all others are wrong.

23
00:01:35,000 --> 00:01:41,000
 And this isn't what the Buddha did. The Buddha himself was

24
00:01:41,000 --> 00:01:45,880
 a very open-minded person who said that some of the things

25
00:01:45,880 --> 00:01:48,000
 that other teachers teach we agree with.

26
00:01:48,000 --> 00:01:50,550
 Some of the things that other teachers teach we don't agree

27
00:01:50,550 --> 00:01:51,000
 with.

28
00:01:51,000 --> 00:01:54,710
 And so he was never going to dogmatically say only my

29
00:01:54,710 --> 00:01:59,000
 teachings are right and every other teaching is wrong.

30
00:01:59,000 --> 00:02:02,700
 The point is that you can find wisdom everywhere and as you

31
00:02:02,700 --> 00:02:06,180
 say all of these paths have something interesting and

32
00:02:06,180 --> 00:02:08,000
 something true in them.

33
00:02:08,000 --> 00:02:13,280
 So on the one hand it's obviously wrong to dogmatically

34
00:02:13,280 --> 00:02:16,000
 stick to a specific path.

35
00:02:16,000 --> 00:02:20,120
 On the other hand what you often see is nowadays as we

36
00:02:20,120 --> 00:02:25,000
 start to learn more about the various religious traditions

37
00:02:25,000 --> 00:02:30,770
 and as they come into contact you'll find people trying to

38
00:02:30,770 --> 00:02:33,840
 smooth things over and make things easy by saying that all

39
00:02:33,840 --> 00:02:38,370
 traditions are the same or all traditions have some equal

40
00:02:38,370 --> 00:02:39,000
 value.

41
00:02:39,000 --> 00:02:43,920
 And this the Buddha didn't agree with and this was one of

42
00:02:43,920 --> 00:02:48,950
 the things that the Buddha tried very clearly to rectify

43
00:02:48,950 --> 00:02:54,310
 this belief that any teacher who said they were enlightened

44
00:02:54,310 --> 00:02:56,000
 was enlightened or so on.

45
00:02:56,000 --> 00:03:02,000
 Or all teachings should be given an equal footing.

46
00:03:02,000 --> 00:03:06,600
 And this is what we've done today. We've established the

47
00:03:06,600 --> 00:03:10,380
 various religious traditions up as the world's major

48
00:03:10,380 --> 00:03:14,570
 religions and we don't want to make any distinction between

49
00:03:14,570 --> 00:03:18,000
 them which of course is unhealthy and improper.

50
00:03:18,000 --> 00:03:22,470
 We should see them for what they are and scientifically

51
00:03:22,470 --> 00:03:26,840
 take them apart and look at them in all their pieces and

52
00:03:26,840 --> 00:03:31,510
 come to see what are their strengths and their weaknesses

53
00:03:31,510 --> 00:03:33,000
 and compare them.

54
00:03:33,000 --> 00:03:35,710
 And this is really what I would recommend is that you

55
00:03:35,710 --> 00:03:39,000
 compare objectively the various religious traditions.

56
00:03:39,000 --> 00:03:43,240
 What the Buddha said was that if you do this that you'll

57
00:03:43,240 --> 00:03:47,310
 come to see that his teaching is the most deep, profound

58
00:03:47,310 --> 00:03:51,000
 and the one that leads to the ultimate goal.

59
00:03:51,000 --> 00:03:54,990
 So this is a very bold statement and it's one that I would

60
00:03:54,990 --> 00:03:58,000
 say most religious traditions make.

61
00:03:58,000 --> 00:04:01,880
 So the Buddha was not unlike other teachers in saying that

62
00:04:01,880 --> 00:04:03,000
 his was the best.

63
00:04:03,000 --> 00:04:06,810
 But what he said is that rather than accepting his teaching

64
00:04:06,810 --> 00:04:11,000
 dogmatically, this was an inappropriate thing to do.

65
00:04:11,000 --> 00:04:14,260
 One should examine objectively all of the religious

66
00:04:14,260 --> 00:04:17,000
 traditions and decide which one is best.

67
00:04:17,000 --> 00:04:20,570
 The Buddha simply said that you'll come to see that the

68
00:04:20,570 --> 00:04:27,420
 most comprehensive, objective, scientific and practical in

69
00:04:27,420 --> 00:04:32,000
 terms of verifiable in terms of bringing results.

70
00:04:32,000 --> 00:04:37,910
 So we should never simply try to pick and choose from

71
00:04:37,910 --> 00:04:42,000
 various religious traditions.

72
00:04:42,000 --> 00:04:44,620
 We should understand that it could very well be that some

73
00:04:44,620 --> 00:04:47,310
 of the teachers or the leaders of these various religious

74
00:04:47,310 --> 00:04:51,000
 traditions were not on the same level.

75
00:04:51,000 --> 00:04:55,140
 That just because someone is the leader of a religion or

76
00:04:55,140 --> 00:04:59,370
 the leader of some group or because it is well liked by the

77
00:04:59,370 --> 00:05:03,650
 majority of the people in the world or followed by a large

78
00:05:03,650 --> 00:05:05,000
 number of people.

79
00:05:05,000 --> 00:05:10,440
 It doesn't mean that it is true. It's possible that people

80
00:05:10,440 --> 00:05:12,000
 are deluding themselves.

81
00:05:12,000 --> 00:05:15,340
 It's possible that the majority of people are following the

82
00:05:15,340 --> 00:05:16,000
 wrong path.

83
00:05:16,000 --> 00:05:19,370
 And objectively and scientifically we should never take

84
00:05:19,370 --> 00:05:25,170
 something simply on faith or even on the acceptance by the

85
00:05:25,170 --> 00:05:27,000
 majority.

86
00:05:27,000 --> 00:05:32,830
 We should study and examine it and try to see where it

87
00:05:32,830 --> 00:05:38,450
 leads and what it brings and practice it and see for

88
00:05:38,450 --> 00:05:42,000
 ourselves what it leads to.

89
00:05:42,000 --> 00:05:46,870
 The point being that if you pick and choose from various

90
00:05:46,870 --> 00:05:50,800
 traditions, now supposing that certain teachings are not in

91
00:05:50,800 --> 00:05:52,000
 line with the truths,

92
00:05:52,000 --> 00:05:54,740
 supposing that there are certain teachings that are leading

93
00:05:54,740 --> 00:05:56,000
 you in the wrong direction,

94
00:05:56,000 --> 00:05:58,500
 then they're going to come in conflict with those teachings

95
00:05:58,500 --> 00:06:01,000
 that are leading you to the truth, leading you to reality,

96
00:06:01,000 --> 00:06:04,000
 leading you to understanding, to enlightenment.

97
00:06:04,000 --> 00:06:11,890
 So rather than simply picking and choosing, one should

98
00:06:11,890 --> 00:06:15,520
 eventually either make up one's mind that none of the

99
00:06:15,520 --> 00:06:18,000
 teachings, no teacher,

100
00:06:18,000 --> 00:06:20,830
 and none of the teachings that exist in the world lead one

101
00:06:20,830 --> 00:06:23,810
 to the ultimate realization of the truth and have a full

102
00:06:23,810 --> 00:06:26,000
 and complete path for me to follow.

103
00:06:26,000 --> 00:06:30,800
 And therefore I should make do with what I have and find

104
00:06:30,800 --> 00:06:33,000
 the true path myself.

105
00:06:33,000 --> 00:06:36,930
 Or accept that one of the teachings, one of the paths, one

106
00:06:36,930 --> 00:06:41,000
 of the teachers that has taught since the beginning of time

107
00:06:41,000 --> 00:06:43,000
 did have the right answer

108
00:06:43,000 --> 00:06:47,220
 and did teach the way out of suffering and the way to true

109
00:06:47,220 --> 00:06:51,990
 realization of reality and understanding and enlightenment

110
00:06:51,990 --> 00:06:53,000
 and so on.

111
00:06:53,000 --> 00:06:57,050
 And follow that path once you have explored and understood

112
00:06:57,050 --> 00:06:58,000
 and examined.

113
00:06:58,000 --> 00:07:02,320
 Because if you find such a path, then there's no reason to

114
00:07:02,320 --> 00:07:15,000
 take from other paths, except sort of ancillary, to sort of

115
00:07:15,000 --> 00:07:19,000
 somehow add something to the main path.

116
00:07:19,000 --> 00:07:23,070
 Rather than going about coming and going from one path to

117
00:07:23,070 --> 00:07:27,250
 the other, you have the right path in front of you. Why

118
00:07:27,250 --> 00:07:31,000
 would you pick and choose when, quite likely,

119
00:07:31,000 --> 00:07:35,300
 these other paths that you've decided that are not leading

120
00:07:35,300 --> 00:07:39,960
 to ultimate reality and ultimate understanding are just

121
00:07:39,960 --> 00:07:44,000
 going to conflict with this one.

122
00:07:44,000 --> 00:07:48,260
 Now, it might be that some people say that there are

123
00:07:48,260 --> 00:07:51,000
 different paths to the same goal.

124
00:07:51,000 --> 00:07:55,170
 And I'm willing to accept this, that there very well could

125
00:07:55,170 --> 00:07:58,000
 be different paths to the same goal.

126
00:07:58,000 --> 00:08:01,970
 But it's important not to get too caught up in this sort of

127
00:08:01,970 --> 00:08:06,580
 idea of different paths to the same goal, which we hear a

128
00:08:06,580 --> 00:08:07,000
 lot.

129
00:08:07,000 --> 00:08:09,910
 Because, you know, it's not really, we're not talking about

130
00:08:09,910 --> 00:08:12,430
 a mountain where you can approach it from all different

131
00:08:12,430 --> 00:08:13,000
 types.

132
00:08:13,000 --> 00:08:17,990
 Besides, we're talking about reality. And reality is one.

133
00:08:17,990 --> 00:08:20,810
 There can only be one reality, either it's true or it's not

134
00:08:20,810 --> 00:08:22,000
, either it's reality or it's not.

135
00:08:22,000 --> 00:08:27,130
 There can be a million falsehoods, infinite number of false

136
00:08:27,130 --> 00:08:31,000
 paths. And this is why we see such a diversity of paths.

137
00:08:31,000 --> 00:08:33,340
 Because we're seeing a lot of paths that are teaching

138
00:08:33,340 --> 00:08:36,830
 something based on speculation, that there is this God or

139
00:08:36,830 --> 00:08:39,000
 this heaven or so on and so on.

140
00:08:39,000 --> 00:08:44,790
 And as a result, they conflict because of the multiplicity

141
00:08:44,790 --> 00:08:50,000
 of falsehood, of illusion, of mental creation.

142
00:08:50,000 --> 00:08:52,790
 Things that have nothing to do with reality can be multiple

143
00:08:52,790 --> 00:08:56,420
. But reality has to, by its very nature, by its very

144
00:08:56,420 --> 00:09:00,000
 definition, be one, be that which is real.

145
00:09:00,000 --> 00:09:04,670
 And the truth has to be one. So the realization of the

146
00:09:04,670 --> 00:09:08,640
 truth and true peace and happiness has to come through

147
00:09:08,640 --> 00:09:16,260
 understanding and through somehow harmonizing oneself with

148
00:09:16,260 --> 00:09:17,000
 this reality.

149
00:09:17,000 --> 00:09:20,290
 So that one acts based on understanding, based on those

150
00:09:20,290 --> 00:09:23,510
 acts in speech and thought that will lead one to peace,

151
00:09:23,510 --> 00:09:26,000
 happiness and freedom from suffering.

152
00:09:26,000 --> 00:09:29,560
 So, whereas there may be many different religious

153
00:09:29,560 --> 00:09:33,720
 traditions that are saying the same thing in different ways

154
00:09:33,720 --> 00:09:39,000
, it's quite more likely that many of them, and as we see,

155
00:09:39,000 --> 00:09:42,710
 many of them are teaching the falsehood, are teaching

156
00:09:42,710 --> 00:09:45,000
 something based on faith or based on illusion.

157
00:09:45,000 --> 00:09:52,360
 So they may have something that is beneficial, but the core

158
00:09:52,360 --> 00:09:58,850
 doctrine or the core theory or dogma is based on a supp

159
00:09:58,850 --> 00:10:06,000
osition or a belief and not based on ultimate reality.

160
00:10:06,000 --> 00:10:10,910
 So I would say don't believe any one path as being the

161
00:10:10,910 --> 00:10:16,780
 truth, just dogmatically. But don't pick and choose unless

162
00:10:16,780 --> 00:10:21,330
 your plan is to eventually create or find one path for

163
00:10:21,330 --> 00:10:26,410
 yourself that is going to lead you to peace, happiness and

164
00:10:26,410 --> 00:10:29,000
 freedom from suffering.

165
00:10:29,000 --> 00:10:33,500
 And the other thing is don't, I would not accept a post

166
00:10:33,500 --> 00:10:38,350
modernist view of things where you say, what is good for me

167
00:10:38,350 --> 00:10:42,000
 is good for me, or what I think is good is good.

168
00:10:42,000 --> 00:10:45,660
 And just because we agree with something or something

169
00:10:45,660 --> 00:10:49,690
 agrees with us, therefore it is right for us. This is the

170
00:10:49,690 --> 00:10:53,430
 final warning that I would give that picking and choosing

171
00:10:53,430 --> 00:10:58,000
 is nice when you're learning or when you're trying to

172
00:10:58,000 --> 00:10:58,000
 figure out what is good for you.

173
00:10:58,000 --> 00:11:01,680
 And trying to figure out what sort of a path you're looking

174
00:11:01,680 --> 00:11:05,150
 for and what are the differences between the paths and

175
00:11:05,150 --> 00:11:08,000
 trying to find a framework for the path.

176
00:11:08,000 --> 00:11:12,780
 But if eventually all you're doing is simply augmenting or

177
00:11:12,780 --> 00:11:17,270
 verifying your own beliefs, your own views, your own

178
00:11:17,270 --> 00:11:22,160
 opinions, your own ego really, and picking up things simply

179
00:11:22,160 --> 00:11:25,690
 because they agree with your way of life, agree with the

180
00:11:25,690 --> 00:11:27,000
 things that you are attached to.

181
00:11:27,000 --> 00:11:31,480
 You are attached to or you cling to or you believe in. Then

182
00:11:31,480 --> 00:11:36,000
 there's no spiritual development at all.

183
00:11:36,000 --> 00:11:41,520
 You're simply reifying your ego, your defiance, your

184
00:11:41,520 --> 00:11:47,180
 attachments, your clinging state of mind and you're not

185
00:11:47,180 --> 00:11:52,140
 learning to understand something, you're not broadening

186
00:11:52,140 --> 00:11:56,000
 your horizons, you're not developing spiritually at all.

187
00:11:56,000 --> 00:12:01,980
 So eventually you're going to have to examine your own

188
00:12:01,980 --> 00:12:07,310
 beliefs and your own views and hold them up against the

189
00:12:07,310 --> 00:12:15,100
 various religious traditions and come to see which one is

190
00:12:15,100 --> 00:12:20,890
 objectively true, which one objectively does lead to peace,

191
00:12:20,890 --> 00:12:23,950
 happiness, freedom from suffering, spiritual development,

192
00:12:23,950 --> 00:12:25,000
 enlightenment and so on.

193
00:12:25,000 --> 00:12:31,600
 So I hope that helped and just a caution not to pick and

194
00:12:31,600 --> 00:12:38,170
 choose. The spiritual path is not a buffet, it's something

195
00:12:38,170 --> 00:12:42,270
 that you have to dive into and follow to your utmost and

196
00:12:42,270 --> 00:12:47,000
 really have to give up all the things that you believe in,

197
00:12:47,000 --> 00:12:49,000
 all the things that you hold on to.

198
00:12:49,000 --> 00:12:53,060
 Eventually you're going to have to let go of everything so

199
00:12:53,060 --> 00:12:57,000
 that you can come to be free totally and completely.

200
00:12:57,000 --> 00:13:02,610
 And so as a result you really do need to pick one specific

201
00:13:02,610 --> 00:13:07,410
 path and follow it diligently, not going a short ways and

202
00:13:07,410 --> 00:13:10,700
 then backing off and going up another path and then backing

203
00:13:10,700 --> 00:13:11,000
 off.

204
00:13:11,000 --> 00:13:14,900
 You're going to have to figure out which path is right for

205
00:13:14,900 --> 00:13:18,940
 you, whether it's some path that you've worked out on your

206
00:13:18,940 --> 00:13:22,240
 own or whether it's one of the paths that has been

207
00:13:22,240 --> 00:13:26,140
 established that you realize is the truth and is based on

208
00:13:26,140 --> 00:13:32,940
 reality and has a verifiable goal and conclusion and has a

209
00:13:32,940 --> 00:13:37,300
 detailed teaching that leads you to that conclusion, which

210
00:13:37,300 --> 00:13:39,430
 I would recommend is the Buddha's teaching, that's why I'm

211
00:13:39,430 --> 00:13:40,000
 following it.

212
00:13:40,000 --> 00:13:44,360
 But the other thing I would say is that you probably, I

213
00:13:44,360 --> 00:13:49,010
 would boast I suppose, that you won't find such a claim in

214
00:13:49,010 --> 00:13:53,100
 the other religions, whereas most paths will say something

215
00:13:53,100 --> 00:13:58,830
 like this where ours is the best, you won't find this

216
00:13:58,830 --> 00:14:05,000
 objective explanation where you say that you find the one

217
00:14:05,000 --> 00:14:09,000
 that is most objective and most verifiable in the hearing.

218
00:14:09,000 --> 00:14:12,640
 And that's ours. You'll find instead where people say

219
00:14:12,640 --> 00:14:16,840
 believe this, this is what we believe, it's the truth and

220
00:14:16,840 --> 00:14:21,000
 if you don't believe it, this and this is going to happen.

221
00:14:21,000 --> 00:14:25,690
 And one that's based on inference or extrapolation where

222
00:14:25,690 --> 00:14:30,280
 you have some special experience and you say that's this

223
00:14:30,280 --> 00:14:35,050
 God or that's that God or that's heaven or this realization

224
00:14:35,050 --> 00:14:37,000
 or that realization.

225
00:14:37,000 --> 00:14:42,280
 The Buddha's teaching is purely scientific. It's something

226
00:14:42,280 --> 00:14:47,220
 that allows you to realize the truth without extrapolating

227
00:14:47,220 --> 00:14:51,000
 or jumping to any conclusion whatsoever.

228
00:14:51,000 --> 00:14:55,720
 Conclusions that you'll draw will be verifiable and will be

229
00:14:55,720 --> 00:14:59,580
 based on an exact one to one correlation with your

230
00:14:59,580 --> 00:15:01,000
 experiences.

231
00:15:01,000 --> 00:15:04,300
 When you experience something you'll have the conclusion

232
00:15:04,300 --> 00:15:07,540
 that that is so, it is like that because you've experienced

233
00:15:07,540 --> 00:15:10,000
 it as opposed to extrapolating upon it.

234
00:15:10,000 --> 00:15:16,300
 So my post, I would say, of Buddhism is that it is thus and

235
00:15:16,300 --> 00:15:22,340
 it is the teaching that is most complete and has the most

236
00:15:22,340 --> 00:15:30,830
 detailed and accurate and scientific teaching that has ver

237
00:15:30,830 --> 00:15:35,000
ifiable results and leads to a verifiable goal

238
00:15:35,000 --> 00:15:40,000
 and works for those who put their effort into it.

239
00:15:40,000 --> 00:15:45,100
 So I guess ultimately my answer is no, you don't have to

240
00:15:45,100 --> 00:15:48,510
 pick and choose. In the end you're going to come to

241
00:15:48,510 --> 00:15:53,520
 Buddhism if you've been objective and if you've been honest

242
00:15:53,520 --> 00:15:57,230
 with yourself, that eventually you'll come to realize that

243
00:15:57,230 --> 00:15:59,000
 the Buddha was an enlightened being

244
00:15:59,000 --> 00:16:03,190
 and that anyone who is enlightened will teach these things

245
00:16:03,190 --> 00:16:05,000
 because this is the truth.

246
00:16:05,000 --> 00:16:09,470
 So really the word Buddhism in fact is simply a word that

247
00:16:09,470 --> 00:16:13,000
 means the teaching of the enlightened one.

248
00:16:13,000 --> 00:16:17,130
 So the meaning is that this teaching is the teaching of

249
00:16:17,130 --> 00:16:21,420
 those people who have for themselves come to realize the

250
00:16:21,420 --> 00:16:22,000
 truth.

251
00:16:22,000 --> 00:16:26,000
 And it's not because of faith, it's not a God that came

252
00:16:26,000 --> 00:16:30,010
 down and told them these things or it wasn't given from

253
00:16:30,010 --> 00:16:33,000
 anywhere, it was something that was realized for themselves

254
00:16:33,000 --> 00:16:33,000
.

255
00:16:33,000 --> 00:16:36,530
 So this is therefore the point of saying that not all

256
00:16:36,530 --> 00:16:40,280
 religious traditions are the same because if someone is not

257
00:16:40,280 --> 00:16:43,150
 enlightened they might still teach and they might still

258
00:16:43,150 --> 00:16:46,000
 gain acceptance from a large number of people.

259
00:16:46,000 --> 00:16:48,170
 But that doesn't mean that they're enlightened, that doesn

260
00:16:48,170 --> 00:16:51,110
't mean that their teaching is right and good. If they're

261
00:16:51,110 --> 00:16:55,110
 not yet enlightened this is the criteria of being Buddhism

262
00:16:55,110 --> 00:16:56,000
 or not Buddhism.

263
00:16:56,000 --> 00:16:59,040
 Something is Buddhism if it's based on the teaching of

264
00:16:59,040 --> 00:17:02,540
 someone who is totally perfectly enlightened, someone who

265
00:17:02,540 --> 00:17:05,220
 has come to realize the truth, the whole truth for

266
00:17:05,220 --> 00:17:08,960
 themselves and has come to be fully free from clinging,

267
00:17:08,960 --> 00:17:12,000
 free from craving, free from suffering.

268
00:17:12,000 --> 00:17:15,110
 So I hope that helps. I'm sure it's not exactly what you're

269
00:17:15,110 --> 00:17:20,730
 looking for. I know we like to pick and choose and be free

270
00:17:20,730 --> 00:17:23,000
 in our pursuits.

271
00:17:23,000 --> 00:17:27,790
 This is why there's the rise of post-modernism. But there

272
00:17:27,790 --> 00:17:31,600
 you have it. This is Ask a Monk. Thanks for tuning in and

273
00:17:31,600 --> 00:17:33,000
 wish you all the best.

