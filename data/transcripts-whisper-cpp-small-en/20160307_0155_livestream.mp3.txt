 [Silence]
 Good evening everyone.
 Broadcasting Live, Mark 6.
 Today's quote is about the five hindrances.
 These are our namasai.
 These are the bad guys.
 So we set out on this journey to find clarity of mind, to
 find enlightenment, to find the truth.
 And we work so hard to see the truth, to discover the truth
, to investigate the truth.
 Sometimes we don't see the truth.
 Sometimes we can't see the truth.
 There are reasons why we can't see the truth.
 And we can't find the truth.
 And that's what this quote is all about.
 Think of the mind as a bowl of water.
 Or you think of those tide pools by the ocean. If you look
 into the tide pool, when I was 13 we drove along the
 California coast.
 I remember looking at these tide pools full of hermit crabs
 and starfish and all sorts of interesting things.
 But the thing is the water has to be clear.
 If the water is all stirred up, the water is polluted.
 There are only a number of things. There are these five
 conditions of the mind that are like the five conditions of
 a bowl of water.
 If a bowl of water is mixed up with black turmeric, blue or
 yellow dye,
 you wouldn't be able to see into the pool here. It's
 talking about reflection.
 If you look into that pool of water and try and see your
 reflection in the pool of water,
 it's all full of turmeric or dye or something.
 And you wouldn't see a reflection the way it truly was.
 In the same way if the mind is obsessed by sensual desires,
 desires for pleasant sights and sounds and smells and
 tastes and feelings.
 Again, you can't tell right from wrong. You can't see the
 truth about yourself and about the world.
 You can't see the truth about things. You get deluded by
 things, confused by things, all mixed up.
 So you can't see clearly. The water is not clear. The water
 of the mind is not clear.
 What if the water is heated up to boiling and bubbling?
 Then you're to look at this water and think,
 "Hmm, maybe I'll see what my face looks like."
 If the pot of water is boiling and bubbling, you won't be
 able to see much of anything, not alone in your face.
 You would liken this to anger. If your mind is full of
 anger and ill-will, how could you possibly understand the
 truth?
 How could you see clearly? Your mind is a flame, is boiling
 over its rage and anger.
 You can't see the truth. You can't tell truth from
 falsehood, right from wrong, good from bad.
 All you know is what you like and dislike.
 Suppose the water was overgrown with plants, water plants.
 Then you come to this pool of water and thinking, "Oh, look
 and see how my reflection looks like."
 You come upon it and, "Oh, you can't see anything because
 it's overgrown with water."
 Let's go overgrown with plants. This is like when the mind
 is caught up in laziness, sloth, torpor, drowsiness.
 You can't see anything in the mind. So the mind doesn't
 have the effort to go out to the object, to be mindful of
 the objects as they arise.
 You see the objects as they are. Note them, the energy that
 it takes to note the object is not there.
 It gets lazy. You won't see anything.
 Suppose the water was stirred up by the wind and there were
 waves and ripples caused by the wind.
 Same way if anyone looks in that pool, there's no question
 they're not going to see their reflection as it is.
 And they would have likened this to if the mind is full of
 restlessness and worry.
 If you're restless and you're not focused on one thing and
 your mind is running hither and thither, caught up,
 and you end up with thoughts of the past and the future or
 worried about the past or the future,
 the mind isn't settled enough. If the mind isn't settled,
 you can't see anything.
 It's sitting about like a butterfly. You can't focus on
 anything. You can't understand anything clearly.
 So you do have to focus on everything. Be conscientious
 about everything that arises.
 Treat everything that arises like a guest. Don't dismiss
 them. Don't overlook the guests.
 You have to see whatever guest, just like a good host, has
 to see what the guest needs.
 You have to stop and greet the guest. It could be a bad
 person or a good person, but you won't know unless you
 greet them.
 You have to take the time to check them out. So you have to
 be conscientious with every experience, not being
 superficial about them.
 And finally, suppose there's a bowl of water that is muddy,
 so stirred up on the bottom, so it's all muddy and set in
 the dark.
 Suppose it's dark and you look in a mud puddle. This is the
 puddle that likens delusion, ignorance.
 Doubt, sorry, doubt.
 Doubt.
