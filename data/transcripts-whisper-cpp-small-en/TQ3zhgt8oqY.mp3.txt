 [ "I'm not a fan of English" ]
 Okay, so today we're...
 I'm talking English.
 [ "I'm not a fan of English" ]
 One of the difficulties that we have in meditation
 often comes from trying to find a reason why we're med
itating.
 Our difficulty in meditation often comes from not being
 able to see the reason why we meditate.
 And this becomes a problem because meditation can be very
 difficult.
 And so we find ourselves often inclining towards more
 peaceful and pleasurable states when we meditate.
 We find ourselves easily falling into non-meditative states
 where we're not actually contemplating or considering the
 objects carefully.
 We are enjoying the experience of pleasant states.
 And when unpleasant states come, we are upset by them.
 And so when we're taught to acknowledge and to accept
 things as they are,
 it arises a question in the mind, "Why?"
 "Why sit and torture ourselves?"
 This is a big question in the beginning.
 Later on, once we've become more proficient in the practice
, the question might not arise, but it's still very easy to
 lose sight of why we're meditating
 and lose our conviction and our exertion in the practice.
 We find ourselves slacking off, or getting bored, losing
 interest, losing our interest in the practice.
 So I think we should always consider the results of what we
 do. This is something important.
 And when we do this, then we can see what is of real use,
 and meditation becomes something that is very easy to see
 the benefit of.
 Because when we're not meditating, when we're not
 practicing, when we're not seeing things clearly, there are
 results as well.
 When we cling to things, when we desire for things, when we
 like things, our actions don't go without result.
 When we dislike things, when we're upset, when we get angry
, this isn't without results as well.
 People who have agreed have a real hard time seeing the
 problem.
 People with anger tend to see the problem more. It's not to
 say that they can't fix it, or that they can fix it, that
 they're able to fix it.
 But it's easier to see. Agreed is something very difficult
 to see, and it's very difficult to see the downside, the
 negative side of it.
 It's associated with pleasure, and so we think that by
 following after it, we're going to get more and more
 pleasure and happiness.
 And in fact, the opposite is the case. What we don't see is
 that we slowly become ghosts. We slowly become spirits.
 We waste away. We lose all of our substance, like a drug
 addict.
 And so this is why the Buddha said, "When you die, greed
 leads you to be born as a ghost."
 You can see it in people. They become like demons, like o
gres, like ghosts wailing and complaining and clinging, very
 much clinging.
 People who have anger, they put themselves in hell.
 When we get angry, you can see this in people. When they're
 angry, they're in hell. They're in a hellish state.
 They're in a state of deprivation, of loss, a state of pain
 and suffering.
 The Buddha said, "If you want to hurt someone, make them
 angry." Someone who gets angry does their enemy a favor.
 When we get angry at someone else, someone does something
 or says something nasty and tries to make us upset.
 When we get upset, we do them a favor. They want to hurt us
. We hurt ourselves.
 When we get angry at someone, we've accomplished their goal
. They put ourselves in suffering.
 So we go to hell. We fall into hell. Even in this life,
 people who are always angry look like devils, demons, hell
 beings.
 When they die, the Buddha said, "This is where they go."
 People with delusion, when we're full of ignorance and dis
interest and arrogance and self-appreciation and views and
 opinions.
 This is the realm of animals. People who are ignorant, who
 have no interest in mental development, self-development,
 no interest in higher things like philosophy and religion,
 no interest in science and understanding.
 People who are full of ideas and views and opinions.
 People who have these low states of mind of conceit and
 arrogance, bravado.
 These are animal states. This is the way of animals,
 insects sometimes.
 Look at these people who have a great amount of
 intelligence.
 You can't avoid thinking that they're a lot like insects
 because of their insect-like behavior or animal-like
 behavior.
 People who boast and brag and show off.
 All of these different states, this is the state of
 delusion. It's a very low state.
 It shows someone's inferiority, a sign of someone who is a
 superior human being.
 They don't show off. They don't hold opinions and views and
 beliefs.
 They don't try to impress themselves on others. They're
 very keen on learning and understanding.
 They're very keen on self-improvement.
 When you see people like this, you can see that they're not
 like animals.
 They're not living their life just by eating and sleeping
 and indulging in sensual pleasures.
 They have something higher on their minds, a higher purpose
.
 These things also have results in these states of mind.
 In this life they have very clear results and in the
 afterlife they have very clear results.
 There's no question where these things lead.
 The Buddha said, "Jit dey, sanghilit dey, tukat dey patik
ankar."
 The mind that is defiled leads to suffering, leads to hell,
 leads to a bad destination, without doubt.
 Patikankar, undoubtedly.
 The problem is when we don't meditate, these states are
 ever-present.
 We don't have to do anything special for them to arise.
 All we have to do is live our lives as we ordinary would
 and they come up anyway.
 We get angry, we become greedy, and we're deluded all the
 time.
 It takes a special state of mind to overcome these.
 Special states of kindness, compassion, caring, states of
 morality and abstention,
 states of wisdom and understanding.
 This is a very important reason why we should practice
 meditation.
 Meditation is the development of the mind.
 Without a developed mind, our animal minds, our hell minds,
 our ghost mind will come up by itself.
 We don't have to bring it up or wish for it to come up.
 It's the natural state for us to be greedy, angry and del
uded again and again.
 We have to work hard to overcome these.
 So the meaning is then we have to work hard just to be
 happy, just to be at peace.
 Happiness and peace don't come from slacking off, not in
 the way we think that they do.
 We think that by taking it easy, good things will come and
 our minds will be happy.
 And it's not really the case, we find ourselves becoming
 angry and greedy and deluded.
 But when we develop our minds, when we work on our minds,
 try to see things as they are.
 We're able to do away with these. We're able to overcome
 these.
 We're able to give rise to states of peace, of clarity of
 mind.
 Where we're no longer angry at bad things.
 When bad things come to us, unpleasant things come to us.
 We no longer get angry at them. We no longer see them as
 unpleasant.
 We're retraining our minds to see things as they are.
 And it really is a retraining.
 You can't say, "Oh, I already see things as they are,"
 because we don't.
 Every time we see something, we have a judgment of it.
 Here's something, we have a judgment of it.
 That's our natural state. We don't have to work to judge.
 Judging is our ordinary way of behavior.
 We have these chain reactions, and we have to break the
 chain.
 The work we have to do is to break the chain.
 You can't just expect the chain to not form. It forms by
 nature.
 So meditation has a result. It has a very clear result.
 When we do acknowledge, we see things clearly.
 And that's an incredible benefit, because then the chain is
 broken.
 We don't go into liking or disliking.
 We don't go on to judge the object.
 Simply see it for what it is.
 When we say seeing, seeing or hearing, hearing.
 If we're clear about it, then there's no liking or disl
iking.
 There's no judging. There's no good or bad.
 There just is. There is what there is.
 [no speech detected]
 The great thing about meditation is not only this,
 but at the moment when we're mindful, our mind is clear.
 The great thing is that it slowly changes our mind,
 so that that becomes the default.
 So that the default is seeing things clearly.
 Slowly we begin to see things in a way that we didn't see
 them before.
 We begin to understand things in a way that we didn't
 understand them before.
 And our greed and our anger and our delusion become foreign
.
 Become foreign states that come in once in a while
 and eventually don't come in at all.
 We're not seeing them being replaced by the opposite.
 And this is really the difference, there's a difference
 between
 the negative states and the positive states.
 If you create enough wisdom and understanding,
 you can never give rise to unwholesome states again.
 Because you can't do away with your wisdom.
 You can't give up your understanding.
 Ignorance is something we can do away with by understanding
.
 But once you understand something, you can't go back to
 being ignorant about it.
 Once you see the way things work, your whole universe, your
 whole existence changes.
 It becomes lighter, it becomes simpler, it becomes cleaner,
 it becomes clearer.
 And there's less of the complication that we find in
 ordinary life.
 And this is really leading to what the Buddha meant by Nibb
ana.
 The real result of meditation is Nibbana, or freedom.
 And we don't ever go back.
 When a person enters into the light, they don't go back to
 the darkness.
 When a person sees clearly, they can never be fooled again.
 All it takes is to see the truth once, clearly and
 completely,
 and you never go back to falsehood.
 Your whole universe changes, your whole path changes.
 We talk about the afterlife. Well, when you see things
 clearly, your afterlife changes.
 Your future changes.
 Simply by changing the present, you can change your whole
 life.
 When the future looks dismal or unclear, simply by med
itating,
 you can change the whole of your future.
 Everything changes for you when you start to see things
 clearly,
 when you start to give rise to wisdom.
 So meditation certainly does have a result.
 It's something with great benefit, and it's something that
's necessary to retrain our minds
 and to keep our minds from falling back again and again
 into unwholesome state.
 This is something that we should think about often.
 We should review our meditation and ask ourselves whether
 we really are putting our whole heart into the meditation
 or whether we've lost sight of the goal and the reason and
 the benefit
 and therefore are not putting our whole heart into it.
 It's only with putting our whole heart into it that we can
 really expect results,
 not just by walking and sitting blindly.
 So it's worth encouraging in this way.
 And this is the teaching for today.
 I hope that's reduced to everyone.
 Now we'll continue on with the practical part.
 First we'll do mindful prostration and then walking and
 then sitting.
