1
00:00:00,000 --> 00:00:25,760
 Okay, so we're broadcasting live, I think.

2
00:00:25,760 --> 00:00:30,840
 So tonight I'm going to talk a little bit about the hindr

3
00:00:30,840 --> 00:00:31,800
ances.

4
00:00:31,800 --> 00:00:33,640
 Just a short talk.

5
00:00:33,640 --> 00:00:42,720
 This won't be a long talk.

6
00:00:42,720 --> 00:00:52,350
 The hindrances are one of the more iconic and foundational

7
00:00:52,350 --> 00:00:55,000
 sets of dhammas in the Buddha's

8
00:00:55,000 --> 00:01:00,760
 teaching.

9
00:01:00,760 --> 00:01:13,360
 There are certain sets of dhammas that are just

10
00:01:13,360 --> 00:01:17,480
 specifically important or remarkable.

11
00:01:17,480 --> 00:01:23,170
 Now, of course, every bit of the Buddha's teaching is

12
00:01:23,170 --> 00:01:29,920
 wonderful and beautiful, magnificent.

13
00:01:29,920 --> 00:01:36,290
 But some sets of teachings are exceedingly important and

14
00:01:36,290 --> 00:01:37,680
 remarkable.

15
00:01:37,680 --> 00:01:48,460
 For example, the five precepts are something that I think

16
00:01:48,460 --> 00:01:53,920
 stands out not as being overly

17
00:01:53,920 --> 00:02:04,260
 surprising or remarkable as being something new, but they

18
00:02:04,260 --> 00:02:07,160
're so foundational and so important.

19
00:02:07,160 --> 00:02:18,310
 Iconic, they're the basis of Buddhist ethics that you hear

20
00:02:18,310 --> 00:02:23,520
 them talked about so often in

21
00:02:23,520 --> 00:02:28,200
 Buddhist countries, in Buddhist monasteries.

22
00:02:28,200 --> 00:02:31,640
 People who come to the monasteries quite often will take

23
00:02:31,640 --> 00:02:33,000
 the five precepts.

24
00:02:33,000 --> 00:02:36,440
 Now we have the eightfold noble path.

25
00:02:36,440 --> 00:02:40,640
 It's a very important part of the teaching, of course.

26
00:02:40,640 --> 00:02:45,080
 I mean, it's the central core path.

27
00:02:45,080 --> 00:02:49,600
 So you'll always hear Buddhism talked about in terms of the

28
00:02:49,600 --> 00:02:51,720
 teaching of the eightfold

29
00:02:51,720 --> 00:02:57,210
 noble path, even the three trainings that summarize the

30
00:02:57,210 --> 00:02:59,680
 eightfold noble path.

31
00:02:59,680 --> 00:03:09,550
 Isila, Samadhi, Panya, ethics, focus, or concentration, and

32
00:03:09,550 --> 00:03:11,160
 wisdom.

33
00:03:11,160 --> 00:03:19,560
 These are an iconic, essential group of dhammas.

34
00:03:19,560 --> 00:03:24,240
 To be honest, there are so many sets of teachings that are

35
00:03:24,240 --> 00:03:26,580
 just incredibly useful.

36
00:03:26,580 --> 00:03:33,220
 The hindrances are a bit different.

37
00:03:33,220 --> 00:03:40,360
 You don't hear them talked about in Buddhist society.

38
00:03:40,360 --> 00:03:45,160
 Not so much.

39
00:03:45,160 --> 00:03:48,540
 You often don't hear monks telling, giving instruction on

40
00:03:48,540 --> 00:03:50,120
 them to lay people who come

41
00:03:50,120 --> 00:03:54,750
 to visit the monastery, because the five hindrances are

42
00:03:54,750 --> 00:03:59,120
 remarkable, exceptional, or exceptionally

43
00:03:59,120 --> 00:04:05,760
 important for people practicing meditation.

44
00:04:05,760 --> 00:04:12,810
 They make up the essence of the struggle, the struggle to

45
00:04:12,810 --> 00:04:16,720
 follow the path, the struggle

46
00:04:16,720 --> 00:04:20,360
 to succeed on the path.

47
00:04:20,360 --> 00:04:25,480
 Five hindrances are what get in your way.

48
00:04:25,480 --> 00:04:29,920
 If anything's wrong with your practice, you don't have to

49
00:04:29,920 --> 00:04:30,920
 look far.

50
00:04:30,920 --> 00:04:35,850
 You don't have to scour the texts to figure out what you're

51
00:04:35,850 --> 00:04:37,280
 doing wrong.

52
00:04:37,280 --> 00:04:41,260
 In the dhammas, of all the dhammas the Buddha could teach

53
00:04:41,260 --> 00:04:43,440
 in the Satipatanasrta, the first

54
00:04:43,440 --> 00:04:47,000
 set of dhammas is the five hindrances.

55
00:04:47,000 --> 00:04:48,000
 That's important.

56
00:04:48,000 --> 00:04:52,360
 They stick out like that.

57
00:04:52,360 --> 00:04:59,890
 The five hindrances make the difference between really med

58
00:04:59,890 --> 00:05:04,960
itating and just trying to meditate.

59
00:05:04,960 --> 00:05:09,060
 There's often a description of actual meditation with the

60
00:05:09,060 --> 00:05:11,740
 Buddha called jhana, where the mind

61
00:05:11,740 --> 00:05:13,360
 is in a meditative state.

62
00:05:13,360 --> 00:05:16,680
 He said, "The essence is that they're free from the five

63
00:05:16,680 --> 00:05:18,520
 hindrances, free from these

64
00:05:18,520 --> 00:05:19,520
 five things."

65
00:05:19,520 --> 00:05:28,560
 They're incredibly important.

66
00:05:28,560 --> 00:05:35,250
 The five hindrances, of course, are gama raga, passion for

67
00:05:35,250 --> 00:05:38,840
 sensuality or sensual desire,

68
00:05:38,840 --> 00:05:43,390
 for sights and sounds and smells and tastes and feelings

69
00:05:43,390 --> 00:05:47,480
 that are pleasant, attractive.

70
00:05:47,480 --> 00:05:57,850
 Vayapada, aversion or ill will, but also anger, aversion

71
00:05:57,850 --> 00:06:01,640
 towards something.

72
00:06:01,640 --> 00:06:06,800
 It doesn't have to just be a person.

73
00:06:06,800 --> 00:06:17,200
 It's a dislike of something, anger really, fury, the

74
00:06:17,200 --> 00:06:23,040
 sickness of cruel intentions and

75
00:06:23,040 --> 00:06:31,230
 cruelty, desire for harm, wishing harm on others and

76
00:06:31,230 --> 00:06:38,760
 intending harm, harm for yourself as well,

77
00:06:38,760 --> 00:06:42,400
 hurting yourself.

78
00:06:42,400 --> 00:06:52,720
 It's funny how we hurt ourselves as a solution to something

79
00:06:52,720 --> 00:06:55,520
 unpleasant.

80
00:06:55,520 --> 00:07:03,100
 Something unpleasant happens, we hurt ourselves, we get

81
00:07:03,100 --> 00:07:08,480
 angry, and this is a torture in itself.

82
00:07:08,480 --> 00:07:21,060
 Tīnāmīdha, which is the stiffness and drowsiness of mind

83
00:07:21,060 --> 00:07:28,680
, the mind that is stagnant, that is

84
00:07:28,680 --> 00:07:39,570
 inert, incapable of exertion, energetic activity, the

85
00:07:39,570 --> 00:07:46,640
 sluggish mind, the unwieldy mind.

86
00:07:46,640 --> 00:07:54,000
 Uddhācya kokucha, restlessness and worry.

87
00:07:54,000 --> 00:07:59,370
 So this is the mind that is unable to fix on an object,

88
00:07:59,370 --> 00:08:02,720
 unable to focus, the mind that

89
00:08:02,720 --> 00:08:08,460
 flits, the mind that extrapolates, finds meaning in things,

90
00:08:08,460 --> 00:08:11,040
 extends the meaning beyond the

91
00:08:11,040 --> 00:08:18,480
 experience.

92
00:08:18,480 --> 00:08:32,360
 And just overall becomes lost in distraction.

93
00:08:32,360 --> 00:08:41,550
 The mind that isn't focused and present and here and now,

94
00:08:41,550 --> 00:08:46,840
 the mind that isn't attentive,

95
00:08:46,840 --> 00:08:55,450
 the mind that isn't stable, that is flitting around like a

96
00:08:55,450 --> 00:08:57,760
 pumpkin on the water, like a

97
00:08:57,760 --> 00:09:09,000
 gourd on the water, buffeted by the wind and waves.

98
00:09:09,000 --> 00:09:16,840
 And last but not least, vīchīkīchā, doubt.

99
00:09:16,840 --> 00:09:25,580
 Doubt is the most dangerous hindrance that keeps you from

100
00:09:25,580 --> 00:09:29,120
 following any path.

101
00:09:29,120 --> 00:09:34,820
 Doubt is what makes it hard to find, hard to follow the

102
00:09:34,820 --> 00:09:36,440
 right path.

103
00:09:36,440 --> 00:09:47,660
 Doubt is what keeps you from reaping the benefits of wisdom

104
00:09:47,660 --> 00:09:51,000
 because even though you might see

105
00:09:51,000 --> 00:09:56,090
 what is right and what is wrong, doubt keeps you from

106
00:09:56,090 --> 00:09:57,760
 accepting it.

107
00:09:57,760 --> 00:10:02,710
 When you see the benefit of something, doubt makes you

108
00:10:02,710 --> 00:10:03,780
 forget.

109
00:10:03,780 --> 00:10:06,720
 Doubt prevents you from benefiting from the benefit.

110
00:10:06,720 --> 00:10:12,920
 When you see the problem with something, doubt is what

111
00:10:12,920 --> 00:10:16,640
 keeps you from abandoning it.

112
00:10:16,640 --> 00:10:23,880
 Doubt is what makes it hard to choose the right thing.

113
00:10:23,880 --> 00:10:27,530
 So the hindrances are quite serious, most especially with

114
00:10:27,530 --> 00:10:29,600
 meditation practice, but they

115
00:10:29,600 --> 00:10:35,240
 also describe the obstacles that we face in our life.

116
00:10:35,240 --> 00:10:38,560
 They're what keeps us from being happy.

117
00:10:38,560 --> 00:10:46,760
 They're what keeps us from finding peace.

118
00:10:46,760 --> 00:10:54,200
 The Buddha had some vivid descriptions, similes for what it

119
00:10:54,200 --> 00:10:59,440
 is like to experience the hindrances.

120
00:10:59,440 --> 00:11:05,830
 He said that the reason why these are hindrances, he said

121
00:11:05,830 --> 00:11:08,480
 they're like water.

122
00:11:08,480 --> 00:11:13,000
 The mind is like a bowl of water.

123
00:11:13,000 --> 00:11:19,630
 And if you take a bowl of water and it's clear water, you

124
00:11:19,630 --> 00:11:23,360
 can see everything in the bowl.

125
00:11:23,360 --> 00:11:25,240
 You can see your reflection in the bowl.

126
00:11:25,240 --> 00:11:28,000
 It's so clear and so pure that when you look down in, you

127
00:11:28,000 --> 00:11:29,280
 see your reflection.

128
00:11:29,280 --> 00:11:34,630
 So you can use the bowl as a mirror, even so clear and so

129
00:11:34,630 --> 00:11:35,520
 pure.

130
00:11:35,520 --> 00:11:42,430
 But if you take color and you pour color into the water,

131
00:11:42,430 --> 00:11:45,960
 red or blue or green, dye.

132
00:11:45,960 --> 00:11:49,380
 In the olden days, they used, people would dye their own

133
00:11:49,380 --> 00:11:51,160
 clothes and so you'd get the

134
00:11:51,160 --> 00:11:53,800
 dye and you'd pour it in the water.

135
00:11:53,800 --> 00:11:56,320
 And it becomes completely opaque.

136
00:11:56,320 --> 00:11:57,560
 You can't see through it.

137
00:11:57,560 --> 00:11:59,800
 You can't see the bottom.

138
00:11:59,800 --> 00:12:09,720
 It becomes like a solid color.

139
00:12:09,720 --> 00:12:13,440
 And he said because of the dye, you can't see anything.

140
00:12:13,440 --> 00:12:16,080
 Because of the color that you put in the water, you can't

141
00:12:16,080 --> 00:12:17,280
 see your reflection.

142
00:12:17,280 --> 00:12:18,800
 You can't see anything in the water.

143
00:12:18,800 --> 00:12:23,720
 The water, like the mind, just becomes completely opaque.

144
00:12:23,720 --> 00:12:25,360
 You can't understand.

145
00:12:25,360 --> 00:12:27,120
 You can't see it.

146
00:12:27,120 --> 00:12:30,560
 You don't know what's inside.

147
00:12:30,560 --> 00:12:35,530
 He said this is like a person who is caught up in sensual

148
00:12:35,530 --> 00:12:36,600
 desire.

149
00:12:36,600 --> 00:12:41,050
 Sensual lust, it's all colorful, but you become completely

150
00:12:41,050 --> 00:12:41,800
 blind.

151
00:12:41,800 --> 00:12:46,920
 You don't know right from wrong, up from down.

152
00:12:46,920 --> 00:12:51,880
 You've lost your sense of perception.

153
00:12:51,880 --> 00:12:58,740
 When it comes to actions and speech, sensual desire, eggs

154
00:12:58,740 --> 00:13:02,440
 you want to do whatever it takes

155
00:13:02,440 --> 00:13:07,160
 to get what you want, be it right or wrong, doesn't even

156
00:13:07,160 --> 00:13:08,880
 enter your mind.

157
00:13:08,880 --> 00:13:15,730
 And so you wind up in great trouble, both inwardly,

158
00:13:15,730 --> 00:13:21,720
 mentally, you become unstable, buffeted

159
00:13:21,720 --> 00:13:23,920
 by likes and dislikes.

160
00:13:23,920 --> 00:13:25,680
 When you get what you want, you're happy.

161
00:13:25,680 --> 00:13:31,520
 When you don't get what you want, you're frustrated,

162
00:13:31,520 --> 00:13:33,520
 disappointed.

163
00:13:33,520 --> 00:13:36,640
 And you just have no clue what is right or what is wrong.

164
00:13:36,640 --> 00:13:45,560
 You can't possibly see because your mind is clouded by the

165
00:13:45,560 --> 00:13:47,120
 color.

166
00:13:47,120 --> 00:13:51,560
 And he said suppose the water was, the bowl of water was

167
00:13:51,560 --> 00:13:53,880
 put over a fire and the water

168
00:13:53,880 --> 00:13:57,120
 started boiling.

169
00:13:57,120 --> 00:14:00,920
 The water's boiling and you look into it, you can't see

170
00:14:00,920 --> 00:14:02,760
 anything in the water.

171
00:14:02,760 --> 00:14:04,400
 You don't know what is in it.

172
00:14:04,400 --> 00:14:06,840
 Maybe there's something cooking or maybe there's some

173
00:14:06,840 --> 00:14:08,240
 stones or gems or anything.

174
00:14:08,240 --> 00:14:09,880
 You don't see it.

175
00:14:09,880 --> 00:14:13,920
 You can't see a reflection in the water.

176
00:14:13,920 --> 00:14:17,520
 And the water has become completely opaque because it's

177
00:14:17,520 --> 00:14:18,360
 boiling.

178
00:14:18,360 --> 00:14:25,450
 And he said this is like the mind that is consumed by anger

179
00:14:25,450 --> 00:14:25,920
.

180
00:14:25,920 --> 00:14:29,880
 The mind that is consumed by anger is like a pot of boiling

181
00:14:29,880 --> 00:14:30,640
 water.

182
00:14:30,640 --> 00:14:34,000
 You lose all sense of perspective in the mind.

183
00:14:34,000 --> 00:14:42,340
 Someone who is angry has lost all sense of rationality or

184
00:14:42,340 --> 00:14:46,720
 judgment, perspective.

185
00:14:46,720 --> 00:14:52,360
 They've lost their ability to judge, to discriminate.

186
00:14:52,360 --> 00:14:59,280
 They have no ability to discriminate right from wrong.

187
00:14:59,280 --> 00:15:03,700
 Someone who stubs their toe on a table, maybe they punch

188
00:15:03,700 --> 00:15:06,560
 the table or get angry at inanimate

189
00:15:06,560 --> 00:15:07,560
 objects.

190
00:15:07,560 --> 00:15:12,720
 They're so lost, consumed by the anger.

191
00:15:12,720 --> 00:15:13,720
 We do things.

192
00:15:13,720 --> 00:15:17,000
 We shout and we hurt others.

193
00:15:17,000 --> 00:15:21,030
 The worst thing we could do for our relationship with

194
00:15:21,030 --> 00:15:23,040
 others, we hurt them.

195
00:15:23,040 --> 00:15:24,760
 As though that might somehow benefit us.

196
00:15:24,760 --> 00:15:28,220
 We're so lost that we think a benefit might come from

197
00:15:28,220 --> 00:15:30,440
 hurting, from causing pain.

198
00:15:30,440 --> 00:15:38,560
 That's how upside down we become.

199
00:15:38,560 --> 00:15:41,120
 Anger is a great hindrance.

200
00:15:41,120 --> 00:15:45,080
 Prevents you from seeing clearly.

201
00:15:45,080 --> 00:15:53,830
 And then he said, suppose there's a bowl of water or a pool

202
00:15:53,830 --> 00:15:56,680
 of water that's just been

203
00:15:56,680 --> 00:16:01,790
 sitting there for a long time and it grows moss or algae or

204
00:16:01,790 --> 00:16:04,120
 plants start to grow over

205
00:16:04,120 --> 00:16:05,520
 it, this scum.

206
00:16:05,520 --> 00:16:09,480
 There's this film of scum over the water.

207
00:16:09,480 --> 00:16:16,480
 Like they say, a rolling stone gathers no moss.

208
00:16:16,480 --> 00:16:20,970
 It's meant to mean that a person who is energetic doesn't

209
00:16:20,970 --> 00:16:22,880
 develop bad habits.

210
00:16:22,880 --> 00:16:28,920
 They're basically the same idea.

211
00:16:28,920 --> 00:16:34,230
 The bowl of water that is covered over by scum, by algae,

212
00:16:34,230 --> 00:16:36,880
 by plants, is like the mind

213
00:16:36,880 --> 00:16:43,050
 that is consumed by sloth and torpor, by laziness and drows

214
00:16:43,050 --> 00:16:44,120
iness.

215
00:16:44,120 --> 00:16:48,800
 The mind that is like this is unwieldy.

216
00:16:48,800 --> 00:16:53,090
 When we have bad habits of laziness, of sleepiness, of

217
00:16:53,090 --> 00:16:57,920
 indulging and encouraging our mind to torpor,

218
00:16:57,920 --> 00:17:03,750
 sometimes through drugs and intoxicants, sometimes through

219
00:17:03,750 --> 00:17:06,840
 overeating, over sleeping, over soul

220
00:17:06,840 --> 00:17:13,000
 socializing, so many things that make us lazy.

221
00:17:13,000 --> 00:17:22,240
 Our minds become unwieldy, become stiff.

222
00:17:22,240 --> 00:17:26,200
 Our minds become stagnant.

223
00:17:26,200 --> 00:17:27,200
 And you can't see anything.

224
00:17:27,200 --> 00:17:30,910
 This is like the mind that is, the pool that is covered

225
00:17:30,910 --> 00:17:33,120
 over with scum is like the mind

226
00:17:33,120 --> 00:17:41,640
 that is stagnant from lack of effort, lack of energy.

227
00:17:41,640 --> 00:17:49,360
 And so you can't see clearly.

228
00:17:49,360 --> 00:17:57,180
 And further he talked about a pool of water that is buffet

229
00:17:57,180 --> 00:18:01,080
ed by the wind, waves, wind

230
00:18:01,080 --> 00:18:07,860
 coming in, and the water is, there's a current, maybe a

231
00:18:07,860 --> 00:18:12,520
 river, or the buffeted by the wind.

232
00:18:12,520 --> 00:18:17,140
 And when it's very windy, you look in the water, you can't

233
00:18:17,140 --> 00:18:19,320
 see a reflection or see what's

234
00:18:19,320 --> 00:18:20,320
 in it as well.

235
00:18:20,320 --> 00:18:27,660
 But I said this is like a person whose mind is full of rest

236
00:18:27,660 --> 00:18:30,720
lessness and worry.

237
00:18:30,720 --> 00:18:34,600
 And your mind is flitting about the past, the future,

238
00:18:34,600 --> 00:18:37,520
 thinking about analyzing and thinking

239
00:18:37,520 --> 00:18:42,360
 about meanings behind things.

240
00:18:42,360 --> 00:18:45,640
 You just get lost.

241
00:18:45,640 --> 00:18:49,030
 At no time when you are intellectualizing, analyzing,

242
00:18:49,030 --> 00:18:51,120
 rationalizing are you focused on

243
00:18:51,120 --> 00:18:53,560
 reality.

244
00:18:53,560 --> 00:18:58,760
 Reality is passing you by.

245
00:18:58,760 --> 00:19:01,110
 So you start to wonder, for example, if you wonder that

246
00:19:01,110 --> 00:19:02,640
 meditation is the meditation good

247
00:19:02,640 --> 00:19:10,050
 for me during the time that you're wondering, you've lost

248
00:19:10,050 --> 00:19:13,320
 so much precious time.

249
00:19:13,320 --> 00:19:16,300
 Someone who feels like the meditation is very good, they

250
00:19:16,300 --> 00:19:18,120
 might start to think, "Oh, this

251
00:19:18,120 --> 00:19:19,120
 is so good."

252
00:19:19,120 --> 00:19:21,510
 And they start to think about how great it is and how, and

253
00:19:21,510 --> 00:19:22,840
 they've completely lost the

254
00:19:22,840 --> 00:19:26,800
 present.

255
00:19:26,800 --> 00:19:32,050
 When you reminisce about the past or worry about the future

256
00:19:32,050 --> 00:19:35,160
, the whole time you are uprooted

257
00:19:35,160 --> 00:19:44,040
 from reality and you dry up like a plant that is uprooted.

258
00:19:44,040 --> 00:19:50,110
 The mind that is in this state is like the water that is

259
00:19:50,110 --> 00:19:53,120
 disturbed by the wind.

260
00:19:53,120 --> 00:19:56,640
 The water has to be calm and still.

261
00:19:56,640 --> 00:20:02,140
 The mind has to be calm and still before you can see what's

262
00:20:02,140 --> 00:20:03,040
 in it.

263
00:20:03,040 --> 00:20:09,740
 So you have to not get triggered and perpetuate the worry,

264
00:20:09,740 --> 00:20:13,480
 the restlessness, the stress in

265
00:20:13,480 --> 00:20:15,480
 the mind.

266
00:20:15,480 --> 00:20:16,840
 That's why we have to face it.

267
00:20:16,840 --> 00:20:22,470
 Even when it's unbearable, you have to bear it, even when

268
00:20:22,470 --> 00:20:25,320
 it's completely hurricane all

269
00:20:25,320 --> 00:20:29,890
 around you and your minds are a tempest and you have to sit

270
00:20:29,890 --> 00:20:31,080
 with them.

271
00:20:31,080 --> 00:20:36,810
 Even when the pain is overwhelming, you have to sit with it

272
00:20:36,810 --> 00:20:40,280
, essential that your mind become

273
00:20:40,280 --> 00:20:43,280
 stable.

274
00:20:43,280 --> 00:20:54,210
 Finally, the Buddha talked about a pool of water that is

275
00:20:54,210 --> 00:20:57,280
 muddy in the dark.

276
00:20:57,280 --> 00:21:09,590
 And like where the bottom of the pool is mixed up, it's

277
00:21:09,590 --> 00:21:12,560
 disturbed.

278
00:21:12,560 --> 00:21:16,260
 So the mud and the silt comes to the top and it's all muddy

279
00:21:16,260 --> 00:21:17,440
 and clouded.

280
00:21:17,440 --> 00:21:20,520
 The water is clouded by mud.

281
00:21:20,520 --> 00:21:24,760
 But it said this is like the mind that is full of doubt.

282
00:21:24,760 --> 00:21:29,570
 When the mind is full of doubt, you can't see anything

283
00:21:29,570 --> 00:21:30,720
 clearly.

284
00:21:30,720 --> 00:21:33,680
 Doubt doesn't help us.

285
00:21:33,680 --> 00:21:43,200
 Now analyzing or questioning can help.

286
00:21:43,200 --> 00:21:46,040
 It can help to decide.

287
00:21:46,040 --> 00:21:48,960
 Even the Buddha posed questions to himself.

288
00:21:48,960 --> 00:21:50,040
 Should I do this?

289
00:21:50,040 --> 00:21:52,560
 What should I do?

290
00:21:52,560 --> 00:21:57,380
 But he never doubted because when you pose the question,

291
00:21:57,380 --> 00:21:59,920
 your mind enters into a situation

292
00:21:59,920 --> 00:22:02,000
 of making a decision.

293
00:22:02,000 --> 00:22:05,160
 And that's, well, it's not meditative.

294
00:22:05,160 --> 00:22:08,770
 It can be useful for deciding to meditate or deciding to do

295
00:22:08,770 --> 00:22:10,520
 many different things.

296
00:22:10,520 --> 00:22:14,230
 Of course, without that decision making faculty, we wouldn

297
00:22:14,230 --> 00:22:16,400
't be able to survive or do anything.

298
00:22:16,400 --> 00:22:19,550
 We wouldn't be able to come to practice meditation or

299
00:22:19,550 --> 00:22:21,920
 decide to start walking or sitting.

300
00:22:21,920 --> 00:22:25,040
 We have to make that decision.

301
00:22:25,040 --> 00:22:28,680
 But the doubt, doubt doesn't help you do that.

302
00:22:28,680 --> 00:22:31,280
 Doubt just gets in the way of making the decision.

303
00:22:31,280 --> 00:22:33,470
 Doubt doesn't let you see more clearly what are your

304
00:22:33,470 --> 00:22:33,960
 options.

305
00:22:33,960 --> 00:22:34,960
 That's what we think.

306
00:22:34,960 --> 00:22:37,760
 Well, if I didn't doubt, I might make the wrong decision.

307
00:22:37,760 --> 00:22:40,040
 You don't need doubt to make the right decision.

308
00:22:40,040 --> 00:22:42,640
 And the opposite is true.

309
00:22:42,640 --> 00:22:45,640
 Without doubt, you can look clearly at all of the options,

310
00:22:45,640 --> 00:22:47,120
 but you can't do that without

311
00:22:47,120 --> 00:22:48,800
 doubt, with doubt.

312
00:22:48,800 --> 00:22:51,320
 You can't do that without giving up the doubt.

313
00:22:51,320 --> 00:22:56,420
 You should never encourage it thinking, "Doubt will help me

314
00:22:56,420 --> 00:22:58,160
 give me choices."

315
00:22:58,160 --> 00:22:59,160
 Right?

316
00:22:59,160 --> 00:23:00,280
 Doubt does give you choices.

317
00:23:00,280 --> 00:23:03,400
 It gives you infinite choices.

318
00:23:03,400 --> 00:23:07,130
 You're never without choices because you can never make a

319
00:23:07,130 --> 00:23:08,040
 decision.

320
00:23:08,040 --> 00:23:10,200
 That's what doubt does for you.

321
00:23:10,200 --> 00:23:13,610
 You give up doubt, you don't have to immediately choose

322
00:23:13,610 --> 00:23:14,480
 anything.

323
00:23:14,480 --> 00:23:17,840
 When you give up doubt, you can see the real choices and

324
00:23:17,840 --> 00:23:20,160
 the real nature of those choices.

325
00:23:20,160 --> 00:23:25,120
 And without doubt, you can make a decision quite quickly.

326
00:23:25,120 --> 00:23:27,200
 You either see that it doesn't matter.

327
00:23:27,200 --> 00:23:29,000
 Either choice is meaningless.

328
00:23:29,000 --> 00:23:34,500
 You see that the choice itself wasn't essential, or you see

329
00:23:34,500 --> 00:23:37,600
 one choice as a problem with it.

330
00:23:37,600 --> 00:23:40,040
 You see that from seeing clearly.

331
00:23:40,040 --> 00:23:43,720
 The only way is if you do see clearly, and doubt will not.

332
00:23:43,720 --> 00:23:49,240
 Doubt is the mud that stops you from doing that.

333
00:23:49,240 --> 00:23:52,400
 So these are the five hindrances.

334
00:23:52,400 --> 00:23:55,880
 That's what the Buddha said about them.

335
00:23:55,880 --> 00:23:58,480
 Finally he talked about, I mean, finally there's so much we

336
00:23:58,480 --> 00:24:00,160
 could say about the hindrances,

337
00:24:00,160 --> 00:24:09,350
 but finally for this talk, what he said about them in the G

338
00:24:09,350 --> 00:24:14,400
uttarnikaya is he talked about

339
00:24:14,400 --> 00:24:21,170
 the causes of them and what you should focus on it to

340
00:24:21,170 --> 00:24:23,680
 overcome them.

341
00:24:23,680 --> 00:24:26,900
 Because he said the real thing that cultivates, and they're

342
00:24:26,900 --> 00:24:28,800
 not mysterious, they're not going

343
00:24:28,800 --> 00:24:35,900
 to be a surprise most likely, but the essence of lust is

344
00:24:35,900 --> 00:24:37,960
 attraction.

345
00:24:37,960 --> 00:24:41,800
 And he said those, so there's this sign of attractiveness.

346
00:24:41,800 --> 00:24:45,520
 The objects of the sense, things you see and hear and smell

347
00:24:45,520 --> 00:24:47,400
 and taste and feel, and even

348
00:24:47,400 --> 00:24:52,740
 think about it, you're by it, if you conceive of something

349
00:24:52,740 --> 00:24:56,200
 in your mind, are either attractive

350
00:24:56,200 --> 00:25:01,080
 or repulsive, or neither.

351
00:25:01,080 --> 00:25:04,880
 But if they're attractive, that seed of attraction, when

352
00:25:04,880 --> 00:25:07,400
 something is attractive, the lack of

353
00:25:07,400 --> 00:25:16,020
 attention to that, when you don't pay attention, this is

354
00:25:16,020 --> 00:25:21,400
 what gives rise to lust, desire.

355
00:25:21,400 --> 00:25:25,960
 And so this is why mindfulness is so essential.

356
00:25:25,960 --> 00:25:31,350
 Mindfulness is what allows us to be with individual

357
00:25:31,350 --> 00:25:37,200
 experiences, catching the moments of attraction.

358
00:25:37,200 --> 00:25:39,560
 You think, I have lust, what am I going to do?

359
00:25:39,560 --> 00:25:41,480
 I have desire, what am I going to do?

360
00:25:41,480 --> 00:25:44,880
 By the time it's desire, it's really unmanageable.

361
00:25:44,880 --> 00:25:47,690
 All you can do is note it and wait for it to go away and

362
00:25:47,690 --> 00:25:49,240
 try to be as patient with it

363
00:25:49,240 --> 00:25:51,840
 as possible.

364
00:25:51,840 --> 00:25:56,660
 If you can catch the attraction at the moment, based on an

365
00:25:56,660 --> 00:26:00,120
 experience, the moment of attraction,

366
00:26:00,120 --> 00:26:05,150
 you can cut off the seed at the root and prevent it from

367
00:26:05,150 --> 00:26:06,400
 growing.

368
00:26:06,400 --> 00:26:10,640
 And if you can be present moment by moment, you can free

369
00:26:10,640 --> 00:26:12,720
 your mind completely because

370
00:26:12,720 --> 00:26:17,080
 you cut off the root of the desire.

371
00:26:17,080 --> 00:26:18,760
 And the same goes with anger.

372
00:26:18,760 --> 00:26:24,250
 Anger, hatred, cruelty, frustration, even boredom and

373
00:26:24,250 --> 00:26:27,600
 depression, fear, sadness, all

374
00:26:27,600 --> 00:26:33,350
 of these have the root in the unattractive or the repulsive

375
00:26:33,350 --> 00:26:33,520
.

376
00:26:33,520 --> 00:26:39,120
 Some experience is unacceptable.

377
00:26:39,120 --> 00:26:42,640
 Is a source of aversion.

378
00:26:42,640 --> 00:26:47,660
 And catching that root of aversion, the disliking, the

379
00:26:47,660 --> 00:26:52,280
 simple disliking of some thing, some moment

380
00:26:52,280 --> 00:26:55,210
 is what allows you to break the chain and prevent it from

381
00:26:55,210 --> 00:26:56,760
 growing and becoming anger

382
00:26:56,760 --> 00:27:04,800
 and all those other things.

383
00:27:04,800 --> 00:27:10,210
 With drowsiness and sloth and torpor and laziness, the root

384
00:27:10,210 --> 00:27:13,280
, the key, the thing to focus on is

385
00:27:13,280 --> 00:27:22,920
 the indolence, lounging around after meals and indulging in

386
00:27:22,920 --> 00:27:28,680
 sleep, oversleep and overeat.

387
00:27:28,680 --> 00:27:36,000
 This basic inclination to inactivity.

388
00:27:36,000 --> 00:27:47,520
 Just the moments when you fail to be present.

389
00:27:47,520 --> 00:27:52,580
 Laziness as a habit is hard to catch because it's kind of

390
00:27:52,580 --> 00:27:53,760
 lacking.

391
00:27:53,760 --> 00:27:56,480
 It's lacking of energy and motivation.

392
00:27:56,480 --> 00:28:05,760
 It's a lack of stepping up to the plate and being present.

393
00:28:05,760 --> 00:28:09,920
 This accomplishes this as a matter of course.

394
00:28:09,920 --> 00:28:13,880
 Accomplishes this energy as a matter of course.

395
00:28:13,880 --> 00:28:18,270
 Because it does step up to the plate, it faces the

396
00:28:18,270 --> 00:28:19,760
 experience.

397
00:28:19,760 --> 00:28:25,260
 It addresses the experience and approaches the experience

398
00:28:25,260 --> 00:28:28,240
 with exactly the right balance

399
00:28:28,240 --> 00:28:34,370
 that keeps you from disliking or liking and that provides

400
00:28:34,370 --> 00:28:38,320
 energy and effort without becoming

401
00:28:38,320 --> 00:28:46,800
 restless or distracted.

402
00:28:46,800 --> 00:28:53,050
 So when you are tired, the inclination of the lazy mind is

403
00:28:53,050 --> 00:28:55,520
 to just lie down and rest.

404
00:28:55,520 --> 00:28:58,200
 But when you're mindful and say tired, tired, this is the

405
00:28:58,200 --> 00:28:59,800
 sort of thing that wakes the mind

406
00:28:59,800 --> 00:29:05,520
 up.

407
00:29:05,520 --> 00:29:11,020
 When you feel exhausted or like overwhelmed and you just

408
00:29:11,020 --> 00:29:14,000
 want to give up, mindfulness

409
00:29:14,000 --> 00:29:17,910
 is the mind that is able to note the feeling rather than

410
00:29:17,910 --> 00:29:20,160
 perpetuate and develop it and

411
00:29:20,160 --> 00:29:28,640
 feed it so that it becomes laziness.

412
00:29:28,640 --> 00:29:32,760
 And the opposite of course with restlessness and worry, the

413
00:29:32,760 --> 00:29:35,080
 mind that is too energetic,

414
00:29:35,080 --> 00:29:40,120
 jumping, not able to be with an object.

415
00:29:40,120 --> 00:29:44,580
 Any object that arises, any experience that arises, the

416
00:29:44,580 --> 00:29:46,480
 mind has to find more.

417
00:29:46,480 --> 00:29:49,850
 Has to, can't experience it, can't be content with just

418
00:29:49,850 --> 00:29:50,960
 experiencing.

419
00:29:50,960 --> 00:29:57,360
 An experience has to be more, mean more, become more, lead

420
00:29:57,360 --> 00:29:58,600
 to more.

421
00:29:58,600 --> 00:30:00,240
 And so it jumps around.

422
00:30:00,240 --> 00:30:15,570
 There's a mind that cannot stay with the experience, cannot

423
00:30:15,570 --> 00:30:22,880
 stay with one experience.

424
00:30:22,880 --> 00:30:31,250
 So the unsettled mind is the cause of restlessness and

425
00:30:31,250 --> 00:30:32,800
 worry.

426
00:30:32,800 --> 00:30:39,200
 That's the basic cause of it.

427
00:30:39,200 --> 00:30:49,930
 The mind that is not with the present experience, that is

428
00:30:49,930 --> 00:30:54,000
 not settled.

429
00:30:54,000 --> 00:30:59,680
 And this is just another quality that we develop.

430
00:30:59,680 --> 00:31:08,720
 This is the catalyst for focusing the mind, for settling

431
00:31:08,720 --> 00:31:12,000
 the mind because you continually

432
00:31:12,000 --> 00:31:17,860
 rebuff the mind's intentions to jump around and the mind

433
00:31:17,860 --> 00:31:21,600
 becomes settled just by reminding

434
00:31:21,600 --> 00:31:23,800
 yourself.

435
00:31:23,800 --> 00:31:25,680
 This is the experience.

436
00:31:25,680 --> 00:31:34,320
 Stay with the experience.

437
00:31:34,320 --> 00:31:47,590
 And with doubt, the Buddha talked about the cause as being

438
00:31:47,590 --> 00:31:51,680
 careless attention.

439
00:31:51,680 --> 00:31:52,680
 Careless attention.

440
00:31:52,680 --> 00:31:58,000
 I don't know exactly what the Pali is, but it might be

441
00:31:58,000 --> 00:32:03,120
 something like a yoni-somanasi

442
00:32:03,120 --> 00:32:08,720
 kara, careless attention, unwise attention.

443
00:32:08,720 --> 00:32:17,330
 So doubt, when you're not prepared for a decision, like a

444
00:32:17,330 --> 00:32:22,320
 judge, if a judge is distracted, they

445
00:32:22,320 --> 00:32:29,380
 might come to the bench and let their emotions rule their

446
00:32:29,380 --> 00:32:33,080
 judgment or not be able to see

447
00:32:33,080 --> 00:32:38,480
 the facts in front of them.

448
00:32:38,480 --> 00:32:42,760
 Without mindfulness, without the qualities of stability,

449
00:32:42,760 --> 00:32:45,720
 when we're confronted with decisions,

450
00:32:45,720 --> 00:32:58,040
 we easily fall into doubt among other things.

451
00:32:58,040 --> 00:33:02,720
 But the mind that is not empowered by the faculties of

452
00:33:02,720 --> 00:33:06,280
 confidence and effort and mindfulness

453
00:33:06,280 --> 00:33:14,020
 and concentration and wisdom easily falls into doubt, to

454
00:33:14,020 --> 00:33:19,240
 uncertainty, when we allow the mind

455
00:33:19,240 --> 00:33:20,240
 to wander.

456
00:33:20,240 --> 00:33:25,930
 We allow the mind to consider things that are not related

457
00:33:25,930 --> 00:33:28,200
 to the experience.

458
00:33:28,200 --> 00:33:31,640
 We allow the mind to make connections.

459
00:33:31,640 --> 00:33:36,930
 We had someone came to the monastery today who had many

460
00:33:36,930 --> 00:33:39,400
 feelings in her body.

461
00:33:39,400 --> 00:33:43,550
 And each feeling she attributed to something, ghosts or

462
00:33:43,550 --> 00:33:46,040
 beings who were out to kill her.

463
00:33:46,040 --> 00:33:50,160
 So she had many ideas about what was going on.

464
00:33:50,160 --> 00:33:53,560
 And this is the uncareful.

465
00:33:53,560 --> 00:33:56,320
 She wasn't careful.

466
00:33:56,320 --> 00:34:02,560
 And something we drill into our students is to focus on the

467
00:34:02,560 --> 00:34:06,160
 distinction between the actual

468
00:34:06,160 --> 00:34:11,160
 experience and all of the baggage and conjecture and

469
00:34:11,160 --> 00:34:15,560
 meaning you give to the experience, because

470
00:34:15,560 --> 00:34:18,480
 they're two very different things.

471
00:34:18,480 --> 00:34:22,020
 When you ascribe meaning to something, that meaning has no

472
00:34:22,020 --> 00:34:23,960
 basis in the actual experience

473
00:34:23,960 --> 00:34:27,000
 and that's what clouds your judgment.

474
00:34:27,000 --> 00:34:31,280
 Without being careful, without this careful attention, you

475
00:34:31,280 --> 00:34:33,160
 easily fall into, easily lose

476
00:34:33,160 --> 00:34:35,080
 that distinction.

477
00:34:35,080 --> 00:34:40,520
 So you think you should consider what you think of things.

478
00:34:40,520 --> 00:34:45,210
 You have some feeling in the body and you think about it

479
00:34:45,210 --> 00:34:48,000
 being a person, a spirit, let's

480
00:34:48,000 --> 00:34:53,880
 say, a spirit attacking you.

481
00:34:53,880 --> 00:34:56,730
 Because you were not careful to see, no, no, the spirit is

482
00:34:56,730 --> 00:34:58,560
 something I thought of by myself.

483
00:34:58,560 --> 00:35:01,960
 I actually have no evidence of that.

484
00:35:01,960 --> 00:35:04,800
 And furthermore, spirit is only an idea itself.

485
00:35:04,800 --> 00:35:09,320
 Even if there is a spirit somewhere, the reality is only

486
00:35:09,320 --> 00:35:10,960
 the experience.

487
00:35:10,960 --> 00:35:13,970
 So this woman, she came today and I spent about a half an

488
00:35:13,970 --> 00:35:15,600
 hour talking with her about

489
00:35:15,600 --> 00:35:17,120
 this distinction.

490
00:35:17,120 --> 00:35:20,550
 And I said, "I can't tell you whether there are beings

491
00:35:20,550 --> 00:35:22,100
 trying to kill you."

492
00:35:22,100 --> 00:35:25,000
 And that's something you have to deal with for sure.

493
00:35:25,000 --> 00:35:28,880
 But on a whole other level is the experience of these

494
00:35:28,880 --> 00:35:31,440
 feelings and the stress and what

495
00:35:31,440 --> 00:35:35,520
 really kills you is your reactions to the feelings.

496
00:35:35,520 --> 00:35:37,600
 But doubt is what enables that.

497
00:35:37,600 --> 00:35:43,120
 This mind that is not clear, she had a question for me.

498
00:35:43,120 --> 00:35:45,640
 She wanted to ask it, she wasn't sure.

499
00:35:45,640 --> 00:35:48,330
 Of course she wasn't sure because she was dealing with

500
00:35:48,330 --> 00:35:49,880
 things that weren't real.

501
00:35:49,880 --> 00:35:52,400
 They weren't the actual experience.

502
00:35:52,400 --> 00:35:56,790
 This is why there's such certainty in Buddhism because it

503
00:35:56,790 --> 00:35:58,600
 deals with reality.

504
00:35:58,600 --> 00:36:02,360
 And it's not some magical or spiritual reality.

505
00:36:02,360 --> 00:36:03,360
 What does it deal with?

506
00:36:03,360 --> 00:36:05,460
 It deals with seeing.

507
00:36:05,460 --> 00:36:07,880
 Seeing is just seeing.

508
00:36:07,880 --> 00:36:11,400
 There is nothing to doubt about seeing.

509
00:36:11,400 --> 00:36:14,480
 No aspect of it that brings up doubt.

510
00:36:14,480 --> 00:36:18,480
 It's only when you get into the details, the bhyanjana, the

511
00:36:18,480 --> 00:36:20,720
 nimitta and the anubhyanjana,

512
00:36:20,720 --> 00:36:23,920
 the signs and the particulars.

513
00:36:23,920 --> 00:36:24,920
 What does it mean?

514
00:36:24,920 --> 00:36:26,920
 What is its nature?

515
00:36:26,920 --> 00:36:29,520
 All of that, throw it out.

516
00:36:29,520 --> 00:36:32,160
 None of that is real.

517
00:36:32,160 --> 00:36:34,280
 Seeing is seeing and there's no doubt involved.

518
00:36:34,280 --> 00:36:37,160
 There's no room for doubt.

519
00:36:37,160 --> 00:36:40,540
 Hearing is hearing, smelling is smelling, tasting, feeling,

520
00:36:40,540 --> 00:36:41,880
 thinking is thinking.

521
00:36:41,880 --> 00:36:47,160
 When you understand this distinction, you have certainty.

522
00:36:47,160 --> 00:36:53,120
 You have no room for the dangers of doubt.

523
00:36:53,120 --> 00:36:58,360
 There's no potential for it to arise.

524
00:36:58,360 --> 00:37:00,320
 Careful attention.

525
00:37:00,320 --> 00:37:06,520
 This is what frees you from doubt.

526
00:37:06,520 --> 00:37:11,760
 So I ended up being a fairly lengthy talk, but that's some

527
00:37:11,760 --> 00:37:14,000
 brief thoughts on the five

528
00:37:14,000 --> 00:37:15,000
 hindrances.

529
00:37:15,000 --> 00:37:17,960
 That's the Dhamma for today.

530
00:37:17,960 --> 00:37:18,880
 Thank you for listening.

531
00:37:18,880 --> 00:37:34,960
 [

