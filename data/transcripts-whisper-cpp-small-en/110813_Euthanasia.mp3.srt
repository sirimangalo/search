1
00:00:00,000 --> 00:00:05,250
 Hello, welcome back to Ask a Monk. Today I thought I would

2
00:00:05,250 --> 00:00:07,000
 try to answer at least one

3
00:00:07,000 --> 00:00:11,120
 of the questions that are waiting for me. And the question

4
00:00:11,120 --> 00:00:13,000
 I'm thinking of now is on

5
00:00:13,000 --> 00:00:19,750
 euthanasia. It was asked what is the Buddhist stance on

6
00:00:19,750 --> 00:00:23,000
 killing someone who wants to die

7
00:00:23,000 --> 00:00:29,480
 or who would perhaps be freed from suffering through their

8
00:00:29,480 --> 00:00:34,000
 own death. Now I hope it's quite

9
00:00:34,000 --> 00:00:36,750
 clear that I don't think, it should be clear from what I've

10
00:00:36,750 --> 00:00:38,000
 said before that I don't think

11
00:00:38,000 --> 00:00:40,790
 they would be freed from suffering through their own death.

12
00:00:40,790 --> 00:00:42,000
 I think that's something

13
00:00:42,000 --> 00:00:47,670
 that I don't have to get into because my views on that are

14
00:00:47,670 --> 00:00:51,000
 pretty clear. I mean, only through

15
00:00:51,000 --> 00:00:53,880
 giving up, clinging are you free from suffering. And that's

16
00:00:53,880 --> 00:00:57,000
 really a key here that the view

17
00:00:57,000 --> 00:01:05,640
 and the belief that killing someone could somehow be better

18
00:01:05,640 --> 00:01:09,000
 for them is delusional because

19
00:01:09,000 --> 00:01:12,650
 it's something that they are clinging to and it's an

20
00:01:12,650 --> 00:01:15,000
 aversion that they have towards the

21
00:01:15,000 --> 00:01:20,820
 suffering and to indulge that is by no means of any benefit

22
00:01:20,820 --> 00:01:23,000
 to them. Any time we look at

23
00:01:23,000 --> 00:01:25,400
 a being who's suffering greatly and we think that by

24
00:01:25,400 --> 00:01:27,000
 putting them out of their misery we're

25
00:01:27,000 --> 00:01:33,470
 somehow doing them a favor is quite mistaken when in fact

26
00:01:33,470 --> 00:01:37,000
 some of the greatest insights

27
00:01:37,000 --> 00:01:42,730
 and wisdom and realizations, the wake up enlightenment that

28
00:01:42,730 --> 00:01:45,000
 comes from suffering, that comes from

29
00:01:45,000 --> 00:01:49,760
 bearing with your suffering and learning to overcome the

30
00:01:49,760 --> 00:01:52,000
 clinging and the aversion that

31
00:01:52,000 --> 00:01:56,110
 we have towards it. That's where the real benefit is. So

32
00:01:56,110 --> 00:01:57,000
 countless stories. If you talk

33
00:01:57,000 --> 00:02:01,080
 to people who work with dying patients in hospice care or

34
00:02:01,080 --> 00:02:03,000
 so on, they will tell you

35
00:02:03,000 --> 00:02:06,530
 about the moment of clarity that comes to a person who's

36
00:02:06,530 --> 00:02:09,000
 suffering greatly, that right

37
00:02:09,000 --> 00:02:12,310
 before they die, not everyone, but many people will have

38
00:02:12,310 --> 00:02:14,000
 this moment where suddenly they're

39
00:02:14,000 --> 00:02:17,710
 free and it's so clear that they've gone through something

40
00:02:17,710 --> 00:02:20,000
 and they've worked through something

41
00:02:20,000 --> 00:02:22,780
 and they've worked it out and they're finally at peace with

42
00:02:22,780 --> 00:02:25,000
 themselves and there's clarity

43
00:02:25,000 --> 00:02:28,570
 of mind and they're able to move on. When you kill, when

44
00:02:28,570 --> 00:02:31,000
 you cut that off, first of all,

45
00:02:31,000 --> 00:02:33,830
 this is, especially if it's their wish that they should die

46
00:02:33,830 --> 00:02:36,000
, then you're helping them escape

47
00:02:36,000 --> 00:02:41,570
 from the lesson. You're helping them to run away from this

48
00:02:41,570 --> 00:02:45,000
 temporarily, from this lesson

49
00:02:45,000 --> 00:02:48,700
 that they have to help them to overcome the clinging,

50
00:02:48,700 --> 00:02:51,000
 helping them to encourage their

51
00:02:51,000 --> 00:02:57,210
 clinging to, their aversion to pain and suffering. That's

52
00:02:57,210 --> 00:02:59,000
 the one side, but I think more important

53
00:02:59,000 --> 00:03:01,540
 is to talk about the effect of killing on one's own mind

54
00:03:01,540 --> 00:03:04,000
 because that's what makes, as I've

55
00:03:04,000 --> 00:03:07,620
 said before, an action immoral or moral. It's the effect

56
00:03:07,620 --> 00:03:09,000
 that it has on your own mind. It's

57
00:03:09,000 --> 00:03:11,770
 not really the suffering that it brings to the other person

58
00:03:11,770 --> 00:03:13,000
. The most horrible thing

59
00:03:13,000 --> 00:03:17,420
 about killing someone else is the disruptive nature of

60
00:03:17,420 --> 00:03:19,000
 killing. When you kill something

61
00:03:19,000 --> 00:03:22,330
 that wants to live, it's of course a lot more disruptive

62
00:03:22,330 --> 00:03:24,000
 than when you kill someone that

63
00:03:24,000 --> 00:03:28,210
 wants to die, but nonetheless it's an incredibly disruptive

64
00:03:28,210 --> 00:03:30,000
 act. It's a very powerful and important

65
00:03:30,000 --> 00:03:33,890
 act. For people who have never killed before, for people

66
00:03:33,890 --> 00:03:36,000
 who have never killed before, it's

67
00:03:36,000 --> 00:03:40,110
 very hard to understand this. I think they will generally

68
00:03:40,110 --> 00:03:42,000
 have a natural aversion to

69
00:03:42,000 --> 00:03:45,320
 it because of the weight of the act. So many people have

70
00:03:45,320 --> 00:03:47,000
 never killed even insects and

71
00:03:47,000 --> 00:03:51,780
 so on and really feel abhorrent to do such a thing just by

72
00:03:51,780 --> 00:03:53,000
 nature and that's because

73
00:03:53,000 --> 00:03:57,410
 of the strength of the act. But it may be difficult for

74
00:03:57,410 --> 00:04:00,000
 them to understand because they've

75
00:04:00,000 --> 00:04:03,160
 never experienced. On the other hand, for someone who kills

76
00:04:03,160 --> 00:04:05,000
 often, a person who murders

77
00:04:05,000 --> 00:04:09,720
 animals and slaughters animals or insects and so on, it's

78
00:04:09,720 --> 00:04:12,000
 equally difficult for them to

79
00:04:12,000 --> 00:04:18,280
 see because they've become desensitized to it. I've told

80
00:04:18,280 --> 00:04:20,000
 the story before about when

81
00:04:20,000 --> 00:04:24,000
 I was younger, I would do hunting. I thought to do hunting

82
00:04:24,000 --> 00:04:26,000
 with my father, to hunt with

83
00:04:26,000 --> 00:04:30,510
 my father and hunt deer. And I killed one deer and it was

84
00:04:30,510 --> 00:04:33,000
 really a very difficult thing

85
00:04:33,000 --> 00:04:36,590
 to do. It surprised me because I had no qualms about it. I

86
00:04:36,590 --> 00:04:38,000
 didn't think there was anything

87
00:04:38,000 --> 00:04:41,710
 wrong with killing at the time. I thought, "That's a good

88
00:04:41,710 --> 00:04:44,000
 way to get food." But when

89
00:04:44,000 --> 00:04:47,390
 it came time to actually kill the deer, my whole body was

90
00:04:47,390 --> 00:04:49,000
 shaking and it surprised me

91
00:04:49,000 --> 00:04:52,780
 at the time. I didn't understand what was going on, what

92
00:04:52,780 --> 00:04:55,000
 was happening. But of course

93
00:04:55,000 --> 00:05:00,780
 later on when I practiced meditation and really woke up and

94
00:05:00,780 --> 00:05:04,000
 had this great wake up call as

95
00:05:04,000 --> 00:05:07,240
 to what I was doing to my mind and so many things that I

96
00:05:07,240 --> 00:05:09,000
 had to go through and had to

97
00:05:09,000 --> 00:05:13,020
 work through and all of these emotions that came up that I

98
00:05:13,020 --> 00:05:15,000
 had to sort out and realize

99
00:05:15,000 --> 00:05:17,600
 and give up and just changing my whole outlook until

100
00:05:17,600 --> 00:05:21,000
 finally there was this great transformation.

101
00:05:21,000 --> 00:05:24,430
 Sorry, I don't want to talk about great things that happen

102
00:05:24,430 --> 00:05:26,000
 to me. But when you go through

103
00:05:26,000 --> 00:05:29,290
 meditation and I was in a pretty bad state, there's a lot

104
00:05:29,290 --> 00:05:31,000
 of cleaning that goes on even

105
00:05:31,000 --> 00:05:37,520
 just in the first few days. And so it made me realize, it

106
00:05:37,520 --> 00:05:41,000
 helped me to realize that,

107
00:05:41,000 --> 00:05:45,630
 "Oh yes, that's what was going on there. There really is a

108
00:05:45,630 --> 00:05:48,000
 great weight to killing."

109
00:05:48,000 --> 00:05:54,910
 And I think that's quite clear when you fine tune your mind

110
00:05:54,910 --> 00:05:56,000
. When you start to practice

111
00:05:56,000 --> 00:06:00,000
 meditation, your mind becomes very quiet, relatively

112
00:06:00,000 --> 00:06:02,000
 speaking, and you're able to see

113
00:06:02,000 --> 00:06:04,940
 any little thing that comes up, you're able to see it much

114
00:06:04,940 --> 00:06:06,000
 better than before. Whereas

115
00:06:06,000 --> 00:06:10,170
 when your mind is very active, when you're full of defile

116
00:06:10,170 --> 00:06:12,000
ment and greed and anger and

117
00:06:12,000 --> 00:06:15,240
 so on, you can't really make head or tail of anything. So

118
00:06:15,240 --> 00:06:17,000
 it's really difficult for

119
00:06:17,000 --> 00:06:19,960
 most people to come up with any solid theory of reality or

120
00:06:19,960 --> 00:06:22,000
 solid understanding of reality

121
00:06:22,000 --> 00:06:24,960
 when you talk about these things. It kind of sounds

122
00:06:24,960 --> 00:06:26,000
 interesting, but it's really hard

123
00:06:26,000 --> 00:06:29,000
 for them to understand because they've never taken the time

124
00:06:29,000 --> 00:06:31,000
 to quiet their mind and to

125
00:06:31,000 --> 00:06:35,050
 see things clearly one by one by one and to be able to

126
00:06:35,050 --> 00:06:38,000
 break things apart. But it's like

127
00:06:38,000 --> 00:06:40,830
 you have a very fine tuned instrument and you're able to

128
00:06:40,830 --> 00:06:42,000
 see these things that people

129
00:06:42,000 --> 00:06:46,350
 aren't able to see. And so killing becomes very abhorrent

130
00:06:46,350 --> 00:06:48,000
 and you're able to see how

131
00:06:48,000 --> 00:06:53,050
 even the slightest anger has a great power to it, let alone

132
00:06:53,050 --> 00:06:55,000
 the disruptive power. When

133
00:06:55,000 --> 00:06:59,120
 you say something to someone and it changes their life and

134
00:06:59,120 --> 00:07:02,000
 it disrupts their course, there's

135
00:07:02,000 --> 00:07:07,570
 great power in it. But to kill has a far greater power. It

136
00:07:07,570 --> 00:07:10,000
's on a whole other level. Even when

137
00:07:10,000 --> 00:07:15,430
 you kill someone who wants to die, you're changing their

138
00:07:15,430 --> 00:07:18,000
 karma. It's like trying to

139
00:07:18,000 --> 00:07:20,770
 help someone who's running from the law. For instance, if

140
00:07:20,770 --> 00:07:23,000
 someone who robbed a bank, you

141
00:07:23,000 --> 00:07:26,380
 help to hide them. It's a lot of work. And that's really

142
00:07:26,380 --> 00:07:28,000
 what's going on here. When you

143
00:07:28,000 --> 00:07:31,360
 kill someone who wants to die, it's no less, it is less,

144
00:07:31,360 --> 00:07:33,000
 but it's still on the level of

145
00:07:33,000 --> 00:07:35,730
 killing someone who doesn't want to die because it's a

146
00:07:35,730 --> 00:07:38,000
 great and weighty act to kill and it's

147
00:07:38,000 --> 00:07:42,610
 something that weighs on your mind. There's repercussions

148
00:07:42,610 --> 00:07:46,000
 because of the power and the

149
00:07:46,000 --> 00:07:50,670
 intensity of the act, of the disruption. If you think of

150
00:07:50,670 --> 00:07:52,000
 the world, the universe as being

151
00:07:52,000 --> 00:07:55,560
 waves, energy waves, and even physical, of course, is

152
00:07:55,560 --> 00:07:58,000
 energy. Think of it as all energy.

153
00:07:58,000 --> 00:08:04,160
 The explosion of energy that you've created, mental energy,

154
00:08:04,160 --> 00:08:07,000
 is tremendous by killing.

155
00:08:07,000 --> 00:08:09,160
 If there's someone who's never killed before, it may be

156
00:08:09,160 --> 00:08:11,000
 difficult to see. This is why people

157
00:08:11,000 --> 00:08:14,860
 think euthanasia could be a good thing. Now there's one

158
00:08:14,860 --> 00:08:16,000
 exception that I've talked about

159
00:08:16,000 --> 00:08:20,200
 before and I think it is an exception. I'm not 100% sure,

160
00:08:20,200 --> 00:08:22,000
 but to me you could at least

161
00:08:22,000 --> 00:08:25,940
 argue for it. And that is letting someone die. I think

162
00:08:25,940 --> 00:08:29,000
 clearly you can and should let

163
00:08:29,000 --> 00:08:33,290
 people die rather than giving them medication that's only

164
00:08:33,290 --> 00:08:36,000
 going to prolong their misery

165
00:08:36,000 --> 00:08:43,810
 or so on. I think that's reasonable. But I think also you

166
00:08:43,810 --> 00:08:47,000
 could stop life support if

167
00:08:47,000 --> 00:08:50,940
 it was considered that they had no chance or maybe even

168
00:08:50,940 --> 00:08:53,000
 that the mind had left or so on.

169
00:08:53,000 --> 00:08:54,880
 There was the case, actually apparently that's what

170
00:08:54,880 --> 00:08:56,000
 happened with Mahasi Sayadaw, that they

171
00:08:56,000 --> 00:08:58,810
 said, "Well the mind is no longer working, the brain is no

172
00:08:58,810 --> 00:09:00,000
 longer functioning." So they

173
00:09:00,000 --> 00:09:05,060
 could take him off life support because he was no longer

174
00:09:05,060 --> 00:09:07,000
 there. But on the other hand,

175
00:09:07,000 --> 00:09:09,820
 this is where you could be excused, but on the other hand

176
00:09:09,820 --> 00:09:12,000
 my teacher in Thailand gave

177
00:09:12,000 --> 00:09:16,300
 an interesting perspective on this that you can take as you

178
00:09:16,300 --> 00:09:19,000
 like. But he said, "Well,

179
00:09:19,000 --> 00:09:23,390
 you never really know and we can say that the person is not

180
00:09:23,390 --> 00:09:25,000
 going to come back, but

181
00:09:25,000 --> 00:09:28,170
 it's never really clear." There was the case and he told

182
00:09:28,170 --> 00:09:29,000
 this story about someone who

183
00:09:29,000 --> 00:09:31,690
 they thought was going to die, but they kept them on life

184
00:09:31,690 --> 00:09:33,000
 support for some time and eventually

185
00:09:33,000 --> 00:09:36,370
 they recovered and came back and lived another 10 years or

186
00:09:36,370 --> 00:09:38,000
 something and were able to do

187
00:09:38,000 --> 00:09:40,040
 all sorts of good deeds. Now his point was, "If a person

188
00:09:40,040 --> 00:09:41,000
 has the opportunity in this

189
00:09:41,000 --> 00:09:44,260
 life to do good deeds, you shouldn't be so quick to cut

190
00:09:44,260 --> 00:09:46,000
 them off because you don't

191
00:09:46,000 --> 00:09:49,300
 know where they're going." This is why we say always that

192
00:09:49,300 --> 00:09:53,000
 this human life is very precious.

193
00:09:53,000 --> 00:09:57,490
 Now the second story I gave about Matukundali, the boy who

194
00:09:57,490 --> 00:09:59,000
 was on his deathbed and had a

195
00:09:59,000 --> 00:10:02,700
 chance to see the Buddha, that's just an example of how we

196
00:10:02,700 --> 00:10:05,000
 can possibly create wholesome mind

197
00:10:05,000 --> 00:10:08,900
 states before the moment of death. And you never really

198
00:10:08,900 --> 00:10:11,000
 know if the person has a chance

199
00:10:11,000 --> 00:10:15,840
 now as a human being, if their mind is impure, then letting

200
00:10:15,840 --> 00:10:18,000
 them die or helping them to die

201
00:10:18,000 --> 00:10:23,470
 is only going to lead them or destined them to an

202
00:10:23,470 --> 00:10:27,000
 unpleasant result, an unpleasant life

203
00:10:27,000 --> 00:10:31,640
 in the future. So if you can help them to stay and at least

204
00:10:31,640 --> 00:10:34,000
 help them to practice meditation

205
00:10:34,000 --> 00:10:38,230
 or listen to the Dhamma or so on, or at least be with their

206
00:10:38,230 --> 00:10:41,000
 family and learn about compassion

207
00:10:41,000 --> 00:10:44,470
 and love and so on and cultivate good thoughts and even

208
00:10:44,470 --> 00:10:46,000
 cultivating patience with the pain,

209
00:10:46,000 --> 00:10:51,000
 which is an excellent learning tool, learning experience.

210
00:10:51,000 --> 00:10:52,000
 It's something that we should

211
00:10:52,000 --> 00:10:55,770
 not underestimate and undervalue. This can be a great thing

212
00:10:55,770 --> 00:10:58,000
 and it's something that we

213
00:10:58,000 --> 00:11:02,330
 should actually encourage, something that is far better

214
00:11:02,330 --> 00:11:04,000
 than to dismiss the person and

215
00:11:04,000 --> 00:11:06,210
 send them on their way, not knowing where they're going to

216
00:11:06,210 --> 00:11:08,000
 go and not just send them on their

217
00:11:08,000 --> 00:11:13,760
 way but disrupt their life. So even in the case of not

218
00:11:13,760 --> 00:11:18,000
 killing but letting a person die,

219
00:11:18,000 --> 00:11:21,850
 it can be that helping them to stay on is a good thing. Now

220
00:11:21,850 --> 00:11:24,000
 you have to judge for yourself.

221
00:11:24,000 --> 00:11:27,000
 It really depends on the individual. I think I wouldn't

222
00:11:27,000 --> 00:11:29,000
 want to stay on and if it came

223
00:11:29,000 --> 00:11:31,700
 down to taking medication or so on, I would just let myself

224
00:11:31,700 --> 00:11:33,000
 go. But you really have to

225
00:11:33,000 --> 00:11:36,480
 judge for yourself and for a person who's not meditating

226
00:11:36,480 --> 00:11:39,000
 for a person who hasn't really

227
00:11:39,000 --> 00:11:43,620
 begun to practice, it's always dangerous to let them go

228
00:11:43,620 --> 00:11:46,000
 because you don't know where they're

229
00:11:46,000 --> 00:11:49,690
 going. I guess the answer would be if once you can see that

230
00:11:49,690 --> 00:11:51,000
 clearly they're on the right

231
00:11:51,000 --> 00:11:54,600
 path and they're ready to go, then you can help them to

232
00:11:54,600 --> 00:11:56,000
 give up medication or to get

233
00:11:56,000 --> 00:12:01,330
 off life support or so on and say go on your way and be

234
00:12:01,330 --> 00:12:04,000
 reassured that they're going in

235
00:12:04,000 --> 00:12:08,380
 the right direction, just be careful that they wouldn't be

236
00:12:08,380 --> 00:12:10,000
 better off sticking around

237
00:12:10,000 --> 00:12:12,840
 to do more good deeds and practice more meditation because

238
00:12:12,840 --> 00:12:15,000
 you don't really know where they're

239
00:12:15,000 --> 00:12:19,410
 going in the future. So I hope that that gave some insight

240
00:12:19,410 --> 00:12:22,000
 into the Buddhist take on euthanasia.

241
00:12:22,000 --> 00:12:26,000
 Thanks for tuning in. All the best.

