1
00:00:00,000 --> 00:00:06,780
 Hello, welcome back to Ask a Monk. Today I will be talking

2
00:00:06,780 --> 00:00:09,720
 about dealing with terminally

3
00:00:09,720 --> 00:00:17,510
 ill patients, hospice care, and some of the ways that I

4
00:00:17,510 --> 00:00:21,800
 would think one can approach people

5
00:00:21,800 --> 00:00:27,270
 in this condition and help them through the practice of the

6
00:00:27,270 --> 00:00:28,780
 meditation.

7
00:00:28,780 --> 00:00:36,950
 The first thing to note or to discuss is in regards to

8
00:00:36,950 --> 00:00:40,960
 general practices in terms of teaching

9
00:00:40,960 --> 00:00:45,410
 meditation, whether it's terminally ill, it doesn't just

10
00:00:45,410 --> 00:00:47,440
 relate to this specific group

11
00:00:47,440 --> 00:00:51,070
 of people, but in any case when you're teaching meditation

12
00:00:51,070 --> 00:00:53,160
 there are certain principles that

13
00:00:53,160 --> 00:00:57,020
 you have to keep in mind.

14
00:00:57,020 --> 00:01:00,500
 The first one is that any time that you're teaching

15
00:01:00,500 --> 00:01:03,280
 meditation you should also be mindful

16
00:01:03,280 --> 00:01:09,420
 yourself and that's probably the most important principle

17
00:01:09,420 --> 00:01:14,360
 to keep in mind. It's not good enough

18
00:01:14,360 --> 00:01:21,100
 to simply know the principles and to have book theory in

19
00:01:21,100 --> 00:01:24,320
 order to teach and in order

20
00:01:24,320 --> 00:01:28,980
 to be effective and in order to really be able to respond

21
00:01:28,980 --> 00:01:31,640
 and interact with the patient,

22
00:01:31,640 --> 00:01:37,000
 the student. One has to be practicing. So at the time when

23
00:01:37,000 --> 00:01:38,040
 you're teaching, at the time

24
00:01:38,040 --> 00:01:42,270
 when you're speaking even, you should try to remind

25
00:01:42,270 --> 00:01:45,120
 yourself of the things that you're

26
00:01:45,120 --> 00:01:51,510
 saying. Otherwise you fall into all sorts of biases and

27
00:01:51,510 --> 00:01:55,320
 emotions and you become stressed

28
00:01:55,320 --> 00:02:00,440
 yourself when the person doesn't respond favorably or you

29
00:02:00,440 --> 00:02:04,920
 can get caught up in your own emotions,

30
00:02:04,920 --> 00:02:12,060
 so overboard, you can become overconfident and overbearing

31
00:02:12,060 --> 00:02:15,320
 and it becomes difficult to

32
00:02:15,320 --> 00:02:20,190
 remain objective and impartial and impart the truth. I mean

33
00:02:20,190 --> 00:02:21,520
 the truth is something that

34
00:02:21,520 --> 00:02:28,890
 is very difficult to understand and very difficult to keep

35
00:02:28,890 --> 00:02:31,360
 in mind. It's easy for us to slip

36
00:02:31,360 --> 00:02:35,490
 off into illusion and fantasy and give the wrong advice.

37
00:02:35,490 --> 00:02:37,520
 When we're teaching we want

38
00:02:37,520 --> 00:02:42,050
 to give something useful, we want to be appreciated and so

39
00:02:42,050 --> 00:02:44,760
 if we're not careful we can easily

40
00:02:44,760 --> 00:02:50,410
 slip into bias and prejudice and find ourselves just saying

41
00:02:50,410 --> 00:02:53,280
 something that sounds good or

42
00:02:53,280 --> 00:02:58,180
 that is going to give a favorable response from our

43
00:02:58,180 --> 00:03:01,560
 listeners which may not always be

44
00:03:01,560 --> 00:03:04,350
 in line with the truth. Something that sounds good, a feel

45
00:03:04,350 --> 00:03:08,320
 good sort of philosophy, it's

46
00:03:08,320 --> 00:03:12,800
 not good enough. And that leads to the second point that in

47
00:03:12,800 --> 00:03:16,760
 general I would recommend for

48
00:03:16,760 --> 00:03:20,170
 anyone who's going to teach meditation besides practicing

49
00:03:20,170 --> 00:03:22,080
 by yourself, have it clearly in

50
00:03:22,080 --> 00:03:28,520
 mind that your approach to it should be a meditation, it

51
00:03:28,520 --> 00:03:32,760
 should be a meditative interaction

52
00:03:32,760 --> 00:03:36,500
 with the person and the only thing that you're giving to

53
00:03:36,500 --> 00:03:39,160
 the person should be information.

54
00:03:39,160 --> 00:03:43,030
 You shouldn't be giving them yourself, it shouldn't be

55
00:03:43,030 --> 00:03:45,280
 about you or your guru status,

56
00:03:45,280 --> 00:03:56,620
 you shouldn't be giving them an image or a presentation of

57
00:03:56,620 --> 00:04:01,040
 some charismatic or impressive

58
00:04:01,040 --> 00:04:07,540
 personality. The whole idea of guruship or impressing

59
00:04:07,540 --> 00:04:11,360
 something on your students is quite

60
00:04:11,360 --> 00:04:16,750
 dangerous and dangerous or not it doesn't have the desired

61
00:04:16,750 --> 00:04:19,960
 effect. You can impress someone

62
00:04:19,960 --> 00:04:25,520
 emotionally and I've tried this or if you try it you can

63
00:04:25,520 --> 00:04:28,640
 see for yourself the results.

64
00:04:28,640 --> 00:04:30,910
 The result is people have great faith in you but they don't

65
00:04:30,910 --> 00:04:32,280
 really understand what you're

66
00:04:32,280 --> 00:04:38,280
 saying. They put more emphasis on the messenger rather than

67
00:04:38,280 --> 00:04:41,320
 the message. And so it's easy

68
00:04:41,320 --> 00:04:45,420
 to get caught up in this as well, the giving yourself as

69
00:04:45,420 --> 00:04:47,800
 the message and pumping people

70
00:04:47,800 --> 00:04:51,300
 up and giving them confidence and encouragement but not

71
00:04:51,300 --> 00:04:53,760
 really giving them any skills. Much

72
00:04:53,760 --> 00:04:57,860
 more useful than giving an image or confidence or

73
00:04:57,860 --> 00:05:01,780
 encouragement and being a support for people

74
00:05:01,780 --> 00:05:05,390
 to lean on is helping them be a support for yourself. That

75
00:05:05,390 --> 00:05:06,120
's I guess the third thing I

76
00:05:06,120 --> 00:05:10,080
 would say is that you're not there to be a support for them

77
00:05:10,080 --> 00:05:12,120
, you're there to teach them

78
00:05:12,120 --> 00:05:17,760
 how to support themselves which is very often overlooked.

79
00:05:17,760 --> 00:05:20,640
 We're much better at or much more

80
00:05:20,640 --> 00:05:24,530
 inclined to be a support for others and have them lean on

81
00:05:24,530 --> 00:05:26,440
 us and depend on us which as

82
00:05:26,440 --> 00:05:30,200
 I'm going to explain is really a part of the problem, the

83
00:05:30,200 --> 00:05:33,000
 dependency. Once they can be strong

84
00:05:33,000 --> 00:05:37,190
 which is really the problem that we face because we've gone

85
00:05:37,190 --> 00:05:39,680
 our whole lives without building

86
00:05:39,680 --> 00:05:43,330
 ourselves up, without strengthening our minds and giving

87
00:05:43,330 --> 00:05:45,520
 ourselves the ability to deal with

88
00:05:45,520 --> 00:05:49,830
 difficulty. And so when it comes we're still children, we

89
00:05:49,830 --> 00:05:52,160
 act like young children. We can

90
00:05:52,160 --> 00:05:57,670
 cry, grown adults will cry and moan and just want to take

91
00:05:57,670 --> 00:06:01,120
 drugs and medication and so on.

92
00:06:01,120 --> 00:06:07,120
 So what you should be there to give is information. Now the

93
00:06:07,120 --> 00:06:10,680
 point about meditating is that you'll

94
00:06:10,680 --> 00:06:13,720
 give the right information and the information you give

95
00:06:13,720 --> 00:06:15,800
 will be unbiased, impartial and will

96
00:06:15,800 --> 00:06:21,560
 be a service to the person. Like you're there as a book or

97
00:06:21,560 --> 00:06:24,160
 you're there as a kind of like

98
00:06:24,160 --> 00:06:28,930
 a coach, someone to stop you when you're going in the wrong

99
00:06:28,930 --> 00:06:31,160
 direction and push you in the

100
00:06:31,160 --> 00:06:35,330
 right direction and encourage you. But because you're

101
00:06:35,330 --> 00:06:38,520
 mindful, because you're there, you're

102
00:06:38,520 --> 00:06:42,560
 present, you're in the present moment, you're able to

103
00:06:42,560 --> 00:06:45,760
 respond and you're able to catch the

104
00:06:45,760 --> 00:06:48,480
 emotions of the other person and you're able to see the

105
00:06:48,480 --> 00:06:50,640
 state of their mind, feel and experience

106
00:06:50,640 --> 00:06:54,550
 it and react appropriately and interact with the situation

107
00:06:54,550 --> 00:06:56,960
 without tangling your own emotions

108
00:06:56,960 --> 00:07:03,410
 up in it. So stay mindful but don't give impressions, give

109
00:07:03,410 --> 00:07:06,440
 information and continue to give information

110
00:07:06,440 --> 00:07:11,460
 and constantly give unbiased information telling people the

111
00:07:11,460 --> 00:07:14,120
 results of this act, not saying

112
00:07:14,120 --> 00:07:17,450
 don't do this, don't do that, but explaining to them why it

113
00:07:17,450 --> 00:07:19,240
's better to do this and better

114
00:07:19,240 --> 00:07:22,140
 to do that and so on. So these are general principles that

115
00:07:22,140 --> 00:07:24,560
 I would follow in terms of

116
00:07:24,560 --> 00:07:29,010
 the meditation. First, be mindful. Second, try to give

117
00:07:29,010 --> 00:07:31,600
 information rather than some kind

118
00:07:31,600 --> 00:07:34,670
 of feeling because it's the information that they will use

119
00:07:34,670 --> 00:07:36,120
 and it's the technique that

120
00:07:36,120 --> 00:07:39,010
 they will use. It has to come from their feeling, it has to

121
00:07:39,010 --> 00:07:40,880
 be their emotion, it has to be their

122
00:07:40,880 --> 00:07:46,740
 volition and their courage in themselves. They have to be

123
00:07:46,740 --> 00:07:49,840
 looking for it. If you are pushing

124
00:07:49,840 --> 00:07:53,830
 the meditation on them and they're not there, it's not

125
00:07:53,830 --> 00:07:56,440
 coming from their heart, they'll

126
00:07:56,440 --> 00:08:03,290
 do it to keep you there, to impress you and so on. And when

127
00:08:03,290 --> 00:08:06,240
 you're gone and even when

128
00:08:06,240 --> 00:08:08,760
 you're there, it won't be from the heart and it won't have

129
00:08:08,760 --> 00:08:10,040
 the desired results. So you

130
00:08:10,040 --> 00:08:15,950
 have to step back and let them, let their heart come forth,

131
00:08:15,950 --> 00:08:18,400
 let their intention come

132
00:08:18,400 --> 00:08:30,040
 forth. And thirdly, the point is to make them self-reliant.

133
00:08:30,040 --> 00:08:32,760
 So by stepping back, by just

134
00:08:32,760 --> 00:08:36,830
 giving them information and not bringing your own ego into

135
00:08:36,830 --> 00:08:38,960
 the equation, you allow them

136
00:08:38,960 --> 00:08:43,010
 to step up to the plate and take their own future into

137
00:08:43,010 --> 00:08:47,400
 their hands. But this is more

138
00:08:47,400 --> 00:08:50,960
 pronounced I think with people who are in great suffering.

139
00:08:50,960 --> 00:08:52,560
 First of all, the one thing

140
00:08:52,560 --> 00:08:56,140
 I'd say about people who are in great suffering, there's an

141
00:08:56,140 --> 00:08:58,160
 advantage and a disadvantage that

142
00:08:58,160 --> 00:09:01,630
 they have. The advantage is they see things that many of us

143
00:09:01,630 --> 00:09:03,760
 don't see. They are experiencing

144
00:09:03,760 --> 00:09:08,080
 the reason for practicing meditation. One of the great

145
00:09:08,080 --> 00:09:10,280
 reasons anyway is because of

146
00:09:10,280 --> 00:09:17,190
 the eventuality or the danger that we all face that we

147
00:09:17,190 --> 00:09:21,080
 might be in this situation at

148
00:09:21,080 --> 00:09:24,020
 some point. Well, they're in it and they can see the need

149
00:09:24,020 --> 00:09:25,520
 to meditate or the need to do

150
00:09:25,520 --> 00:09:28,520
 something. They're looking for a way out of suffering. They

151
00:09:28,520 --> 00:09:29,960
 have the suffering that many

152
00:09:29,960 --> 00:09:34,260
 of us are blind to. We forget exists. And so as a result,

153
00:09:34,260 --> 00:09:36,500
 we have no strength afforded

154
00:09:36,500 --> 00:09:40,270
 through the mind. And when it comes, we like them like

155
00:09:40,270 --> 00:09:42,680
 people who are in it already. We're

156
00:09:42,680 --> 00:09:47,770
 unable to deal with it. So they're looking for it. This is

157
00:09:47,770 --> 00:09:48,720
 the advantage is that the

158
00:09:48,720 --> 00:09:51,480
 people who are suffering greatly and at the end of their

159
00:09:51,480 --> 00:09:53,280
 lives, people who are terminally

160
00:09:53,280 --> 00:09:58,450
 ill, they're not looking to pass the time or to seek

161
00:09:58,450 --> 00:10:02,320
 entertainment and so on. They have

162
00:10:02,320 --> 00:10:05,700
 a problem and they're looking to fix it. They're looking to

163
00:10:05,700 --> 00:10:07,960
 find a solution. So they can be

164
00:10:07,960 --> 00:10:10,970
 a really good audience in this sense. And this is why often

165
00:10:10,970 --> 00:10:12,640
 giving, in this case, giving

166
00:10:12,640 --> 00:10:15,520
 information is often enough. But it has to be confident

167
00:10:15,520 --> 00:10:17,080
 information and you have to be

168
00:10:17,080 --> 00:10:20,500
 able to give it in a way that they understand. You're not

169
00:10:20,500 --> 00:10:22,400
 just giving them a book to read

170
00:10:22,400 --> 00:10:24,500
 or you're not just reading a book to them. You're

171
00:10:24,500 --> 00:10:26,040
 explaining to them and you're going

172
00:10:26,040 --> 00:10:29,580
 through it. I'll try to explain basically what the sort of

173
00:10:29,580 --> 00:10:31,400
 things that I would teach.

174
00:10:31,400 --> 00:10:33,950
 Now the disadvantage with teaching people who are termin

175
00:10:33,950 --> 00:10:35,200
ally ill and so on is because

176
00:10:35,200 --> 00:10:42,290
 you're often fighting with an alternative or a different

177
00:10:42,290 --> 00:10:46,280
 way of treatment. So the treatment

178
00:10:46,280 --> 00:10:53,620
 in hospitals, which quite often has to do with medication,

179
00:10:53,620 --> 00:10:56,720
 will very often get in the

180
00:10:56,720 --> 00:11:02,000
 way and it's going to be something that you'll have to work

181
00:11:02,000 --> 00:11:04,600
 around and it's always going

182
00:11:04,600 --> 00:11:08,760
 to lessen the effects of the meditation. People who are on

183
00:11:08,760 --> 00:11:10,600
 medication will have a very difficult

184
00:11:10,600 --> 00:11:17,240
 time. Medication based on or medication of the sort that is

185
00:11:17,240 --> 00:11:19,920
 meant to relieve pain or

186
00:11:19,920 --> 00:11:24,940
 dull the pain is a real hindrance to meditation practice

187
00:11:24,940 --> 00:11:27,920
 because not only does it dull the

188
00:11:27,920 --> 00:11:32,890
 mind and muddle the mind, but it also reinforces the

189
00:11:32,890 --> 00:11:36,960
 avoidance of the difficulty. Just like

190
00:11:36,960 --> 00:11:44,340
 alcohol or recreational drugs, it's a form of escape. Not

191
00:11:44,340 --> 00:11:46,200
 only does it hurt the body

192
00:11:46,200 --> 00:11:48,900
 and affect our body, our brain's ability to process

193
00:11:48,900 --> 00:11:50,880
 information, but it also affects the

194
00:11:50,880 --> 00:11:53,960
 mind's willingness and ability to deal with pain and

195
00:11:53,960 --> 00:11:56,240
 suffering. It sends you on the wrong

196
00:11:56,240 --> 00:12:01,080
 direction. I would say one of the first things that you

197
00:12:01,080 --> 00:12:03,880
 should explain to people who are

198
00:12:03,880 --> 00:12:06,940
 on medication or who are in this position, hopefully people

199
00:12:06,940 --> 00:12:08,320
 who haven't yet decided what

200
00:12:08,320 --> 00:12:11,150
 form of treatment they're going to take, is explaining the

201
00:12:11,150 --> 00:12:12,960
 differences in the treatment.

202
00:12:12,960 --> 00:12:18,670
 That yes, medication is something that is going to solve

203
00:12:18,670 --> 00:12:21,680
 the problem in the short term,

204
00:12:21,680 --> 00:12:25,910
 but in the long term it's going to lead to a dependence and

205
00:12:25,910 --> 00:12:27,860
 an addiction and the only

206
00:12:27,860 --> 00:12:32,310
 way to have it effectively work is to drug you up to the

207
00:12:32,310 --> 00:12:35,160
 point where you're unconscious

208
00:12:35,160 --> 00:12:38,160
 because it's going to have less and less of an effect, your

209
00:12:38,160 --> 00:12:40,080
 body is going to become more

210
00:12:40,080 --> 00:12:43,840
 tolerant to the drugs and so you'll need more and more and

211
00:12:43,840 --> 00:12:45,320
 so on. The pain is going to become

212
00:12:45,320 --> 00:12:48,260
 more intense and your aversion to the pain will become more

213
00:12:48,260 --> 00:12:51,080
 intense. I had a direct experience.

214
00:12:51,080 --> 00:12:54,310
 My grandmother was quite ill and they drugged her up in a

215
00:12:54,310 --> 00:12:56,240
 nursing home to the point where

216
00:12:56,240 --> 00:13:00,000
 she couldn't even recognize people because this was their

217
00:13:00,000 --> 00:13:01,800
 way of dealing with it. It was

218
00:13:01,800 --> 00:13:05,830
 laziness and it was sloppiness and it was negligence. When

219
00:13:05,830 --> 00:13:07,960
 she had pain and she complained

220
00:13:07,960 --> 00:13:10,930
 about it, they just increased her medication rather than

221
00:13:10,930 --> 00:13:12,480
 going to see a doctor or so and

222
00:13:12,480 --> 00:13:16,520
 they just increased it to the point where she still had no

223
00:13:16,520 --> 00:13:18,240
 pain but she was more or

224
00:13:18,240 --> 00:13:21,080
 less unconscious to the world around her. When this was

225
00:13:21,080 --> 00:13:22,560
 realized, they took her off

226
00:13:22,560 --> 00:13:27,290
 the medication but then she was in incredible suffering.

227
00:13:27,290 --> 00:13:29,560
 She wasn't able to deal with this

228
00:13:29,560 --> 00:13:33,990
 pain that she had been learning and teaching herself to

229
00:13:33,990 --> 00:13:37,280
 avoid, to run away from, to stop,

230
00:13:37,280 --> 00:13:41,430
 to escape through the medication and so as a result she was

231
00:13:41,430 --> 00:13:43,800
 in great and terrible suffering

232
00:13:43,800 --> 00:13:50,380
 that was very most difficult for her to deal with. So we

233
00:13:50,380 --> 00:13:53,920
 should explain this and try to

234
00:13:53,920 --> 00:13:58,290
 make it clear that there are alternatives, that it's

235
00:13:58,290 --> 00:14:01,000
 actually in our better interest

236
00:14:01,000 --> 00:14:04,880
 to come to terms with the pain and to die with a clear mind

237
00:14:04,880 --> 00:14:06,960
. If we're going to die that

238
00:14:06,960 --> 00:14:10,430
 our minds should be clear, we should know what our duty we

239
00:14:10,430 --> 00:14:12,120
're doing, we should have

240
00:14:12,120 --> 00:14:16,530
 the ability to find closure with our relatives and so on

241
00:14:16,530 --> 00:14:19,000
 with a clear and alert mind and

242
00:14:19,000 --> 00:14:24,260
 give confidence that there is another way and explain

243
00:14:24,260 --> 00:14:28,280
 basically the theory of the meditation.

244
00:14:28,280 --> 00:14:31,060
 So here goes exactly the sorts of things that I would say

245
00:14:31,060 --> 00:14:33,560
 to them. I would talk to them,

246
00:14:33,560 --> 00:14:38,900
 get right to the point about pain and say that this is

247
00:14:38,900 --> 00:14:41,800
 going to be our training in the

248
00:14:41,800 --> 00:14:45,700
 meditation practice to deal with the pain or to approach

249
00:14:45,700 --> 00:14:47,560
 the pain and to change the

250
00:14:47,560 --> 00:14:56,310
 way we look at the pain. So you explain first about the

251
00:14:56,310 --> 00:15:00,800
 nature of feelings, that the physical

252
00:15:00,800 --> 00:15:05,510
 feeling is actually not really the problem. The problem is

253
00:15:05,510 --> 00:15:07,560
 our inability to accept it,

254
00:15:07,560 --> 00:15:11,670
 the fact that it bothers us and explain to people how when

255
00:15:11,670 --> 00:15:13,920
 you actually look at the pain

256
00:15:13,920 --> 00:15:16,420
 and when you're actually there with it, you can see that

257
00:15:16,420 --> 00:15:18,000
 there's two things. There's the

258
00:15:18,000 --> 00:15:21,370
 pain and then there's your aversion to the pain and these

259
00:15:21,370 --> 00:15:23,480
 are quite separate and encourage

260
00:15:23,480 --> 00:15:27,160
 people to look at it, to examine it and to become

261
00:15:27,160 --> 00:15:30,360
 comfortable with it, explaining that

262
00:15:30,360 --> 00:15:34,230
 as you look at the pain and as you examine it, you're able

263
00:15:34,230 --> 00:15:36,160
 to see that it's something

264
00:15:36,160 --> 00:15:41,600
 that comes and goes. It actually has no effect on the mind.

265
00:15:41,600 --> 00:15:43,080
 The problem is that we've developed

266
00:15:43,080 --> 00:15:47,770
 this wrong idea that there's something bad about the pain

267
00:15:47,770 --> 00:15:50,160
 based on our worries and our

268
00:15:50,160 --> 00:15:56,130
 feeling that somehow it's going to lead to injury or it

269
00:15:56,130 --> 00:16:00,120
 means something more significant

270
00:16:00,120 --> 00:16:03,910
 in terms of an illness or causing death. So eventually to

271
00:16:03,910 --> 00:16:06,000
 the point where any little pain

272
00:16:06,000 --> 00:16:10,510
 causes suffering in our mind, we can explain to people that

273
00:16:10,510 --> 00:16:12,480
 we're actually causing the

274
00:16:12,480 --> 00:16:18,080
 suffering ourselves by our aversion to the pain. So explain

275
00:16:18,080 --> 00:16:21,640
 to people the relationship

276
00:16:21,640 --> 00:16:24,690
 between the experience and the suffering, how in the

277
00:16:24,690 --> 00:16:26,560
 beginning there is the tension

278
00:16:26,560 --> 00:16:31,210
 and the pressure in the body, the body being in a specific

279
00:16:31,210 --> 00:16:33,920
 position and the pressure build

280
00:16:33,920 --> 00:16:37,570
 up and the stiffness build up and so on and then there

281
00:16:37,570 --> 00:16:39,960
 arises this painful feeling. Now

282
00:16:39,960 --> 00:16:42,840
 once there arises the painful feeling, the mind picks it up

283
00:16:42,840 --> 00:16:44,240
 and starts to run away with

284
00:16:44,240 --> 00:16:50,980
 it. Now it depends how the state of mind of the person, how

285
00:16:50,980 --> 00:16:52,560
 much you want to go into detail,

286
00:16:52,560 --> 00:16:55,510
 but at the very least you should be able to explain to them

287
00:16:55,510 --> 00:16:56,960
 that first of all that the

288
00:16:56,960 --> 00:17:00,960
 feelings are going to be our focus, that make clear to them

289
00:17:00,960 --> 00:17:02,880
 what we're going to be dealing

290
00:17:02,880 --> 00:17:06,030
 with, what the meditation is designed to do. It's designed

291
00:17:06,030 --> 00:17:07,480
 to help us to work through the

292
00:17:07,480 --> 00:17:10,050
 feelings and to come to let go of them and actually

293
00:17:10,050 --> 00:17:12,080
 overcome them to the point that they

294
00:17:12,080 --> 00:17:18,390
 don't bother us. And explaining how we're going to do that,

295
00:17:18,390 --> 00:17:19,960
 that we're going to actually

296
00:17:19,960 --> 00:17:23,680
 focus on the pain and try to remind ourselves the nature of

297
00:17:23,680 --> 00:17:25,840
 the pain, you know, it's just

298
00:17:25,840 --> 00:17:29,990
 a pain or a feeling and so we use this word that we have.

299
00:17:29,990 --> 00:17:32,360
 Explain to them the technique

300
00:17:32,360 --> 00:17:35,800
 of meditation, you know, the word mantra if they've ever

301
00:17:35,800 --> 00:17:37,280
 heard of this or if not, explain

302
00:17:37,280 --> 00:17:39,900
 to them what a mantra is. It's something that helps you

303
00:17:39,900 --> 00:17:41,560
 focus on an object, something that

304
00:17:41,560 --> 00:17:48,770
 helps you to come to see only the object and to keep other

305
00:17:48,770 --> 00:17:53,080
 things from interfering, from

306
00:17:53,080 --> 00:17:55,960
 distracting you, so to keep yourself focused on a single

307
00:17:55,960 --> 00:17:59,000
 object. Now what that does is

308
00:17:59,000 --> 00:18:02,620
 it keeps you focused on simply on the pain. Now reminding

309
00:18:02,620 --> 00:18:04,320
 people that the pain is not

310
00:18:04,320 --> 00:18:07,400
 the problem. Once you're focused on just the sensation for

311
00:18:07,400 --> 00:18:09,240
 itself, what you come to realize

312
00:18:09,240 --> 00:18:13,420
 is that it's actually not a negative experience. It's not

313
00:18:13,420 --> 00:18:15,880
 something unpleasant. And what doesn't

314
00:18:15,880 --> 00:18:19,380
 have a chance to come in is this judgment, this disliking,

315
00:18:19,380 --> 00:18:21,080
 this upset about the pain.

316
00:18:21,080 --> 00:18:25,020
 We're going to be able to separate the feelings of upset

317
00:18:25,020 --> 00:18:27,760
 and stress and anger and frustration

318
00:18:27,760 --> 00:18:31,050
 about the pain, sadness from the actual pain itself and

319
00:18:31,050 --> 00:18:33,120
 just experience the pain for what

320
00:18:33,120 --> 00:18:35,980
 it is. And you can reassure them that it works, tell them

321
00:18:35,980 --> 00:18:37,560
 that you've done this and so on

322
00:18:37,560 --> 00:18:41,750
 and they should try it for themselves. If it doesn't work,

323
00:18:41,750 --> 00:18:43,720
 they're welcome to stop and

324
00:18:43,720 --> 00:18:46,740
 try the medication. So basically saying to people that we

325
00:18:46,740 --> 00:18:48,600
 want to try this as an alternative

326
00:18:48,600 --> 00:18:51,360
 and see what you think and if it doesn't work, you've

327
00:18:51,360 --> 00:18:53,320
 always got the medication to go back

328
00:18:53,320 --> 00:19:00,040
 up on, as a backup. Now once you've explained this, this is

329
00:19:00,040 --> 00:19:01,560
 the theory, this is something

330
00:19:01,560 --> 00:19:04,020
 that they have to keep in mind, then you can start them on

331
00:19:04,020 --> 00:19:05,560
 something simpler. Say this

332
00:19:05,560 --> 00:19:08,480
 mantra, this word that we use, the idea is to allow us to

333
00:19:08,480 --> 00:19:12,680
 see things clearly. So we teach

334
00:19:12,680 --> 00:19:16,560
 people to start practicing it and find a simple object to

335
00:19:16,560 --> 00:19:18,840
 practice it on. The basic object

336
00:19:18,840 --> 00:19:22,060
 of our contemplation, as many of you are aware, is the

337
00:19:22,060 --> 00:19:24,360
 stomach. And this is very useful for

338
00:19:24,360 --> 00:19:27,720
 people who are in bed, lying down. When you're lying down

339
00:19:27,720 --> 00:19:29,720
 and you're relaxed, you'll find

340
00:19:29,720 --> 00:19:34,000
 that the stomach is quite evident and easy to follow. And

341
00:19:34,000 --> 00:19:35,960
 so we watch it rising, falling,

342
00:19:35,960 --> 00:19:39,510
 rising, falling and keep our mind on it. And we have this

343
00:19:39,510 --> 00:19:41,200
 word that allows our mind to

344
00:19:41,200 --> 00:19:45,950
 focus only on the stomach. When it rises, say to yourself,

345
00:19:45,950 --> 00:19:47,720
 "Rising." When it falls,

346
00:19:47,720 --> 00:19:50,920
 say to yourself, "Ahh." And your mind will focus on that

347
00:19:50,920 --> 00:19:52,720
 object and that object only.

348
00:19:52,720 --> 00:19:57,480
 It won't, nothing else will come in to distract it. Or your

349
00:19:57,480 --> 00:19:59,920
 mind won't be flitting here and

350
00:19:59,920 --> 00:20:02,930
 there, your mind will become focused and concentrated. And

351
00:20:02,930 --> 00:20:04,960
 for a time, you'll find that you're able

352
00:20:04,960 --> 00:20:10,530
 to find peace and clarity of mind. Now as you do that, you

353
00:20:10,530 --> 00:20:12,440
'll find that from time to

354
00:20:12,440 --> 00:20:15,850
 time pain arises. And especially if you're terminally ill,

355
00:20:15,850 --> 00:20:17,320
 if you have a sickness and

356
00:20:17,320 --> 00:20:21,100
 so on, you'll find that these sharp and unpleasant

357
00:20:21,100 --> 00:20:24,280
 sensations arise. Now it's at that point,

358
00:20:24,280 --> 00:20:26,630
 and you can lead people through that. You can have them

359
00:20:26,630 --> 00:20:27,920
 start to watch the stomach and

360
00:20:27,920 --> 00:20:30,740
 then they start to explain as they're practicing, as they

361
00:20:30,740 --> 00:20:32,720
're watching the stomach, explain to

362
00:20:32,720 --> 00:20:35,740
 them about the sorts of things that might arise. The most

363
00:20:35,740 --> 00:20:37,720
 prominent being the feelings,

364
00:20:37,720 --> 00:20:41,340
 the pain and so on. And say when the pain arises, try, you

365
00:20:41,340 --> 00:20:43,080
 know, we can always go back

366
00:20:43,080 --> 00:20:45,880
 to the medication, reassuring them that they don't, and

367
00:20:45,880 --> 00:20:47,520
 this isn't going to be, we're not

368
00:20:47,520 --> 00:20:51,710
 trying to torture them, but reassure them to try first to

369
00:20:51,710 --> 00:20:53,880
 acknowledge pain, to just

370
00:20:53,880 --> 00:20:57,990
 remind themselves of the pain and keep their minds on the

371
00:20:57,990 --> 00:20:59,320
 pain. Because the truth is it's

372
00:20:59,320 --> 00:21:05,690
 only when their mind slips away from the pain into judgment

373
00:21:05,690 --> 00:21:09,240
, into aversion, into dislike

374
00:21:09,240 --> 00:21:12,220
 of the pain, that it begins to give rise to thoughts of how

375
00:21:12,220 --> 00:21:13,760
 to get away and how to change

376
00:21:13,760 --> 00:21:18,600
 it, how to find some way anyway to escape the situation. As

377
00:21:18,600 --> 00:21:20,240
 long as you're focusing

378
00:21:20,240 --> 00:21:22,990
 on the situation, seeing it for what it is, it doesn't even

379
00:21:22,990 --> 00:21:24,360
 enter your mind that you have

380
00:21:24,360 --> 00:21:27,240
 to change the situation, it doesn't enter your mind that

381
00:21:27,240 --> 00:21:28,920
 this is a bad situation. There's

382
00:21:28,920 --> 00:21:32,730
 no room for that. Your mind is fully aware, it's fully

383
00:21:32,730 --> 00:21:35,640
 focused on the object itself. You

384
00:21:35,640 --> 00:21:39,620
 don't have to explain all this to them, but have them try

385
00:21:39,620 --> 00:21:42,120
 it, have them see for themselves

386
00:21:42,120 --> 00:21:46,110
 whether this is true or not, what the result is. I tried

387
00:21:46,110 --> 00:21:47,560
 this with a woman who had stomach

388
00:21:47,560 --> 00:21:51,550
 cancer and she had had it for about seven years, she was in

389
00:21:51,550 --> 00:21:53,120
 her final stages. And so

390
00:21:53,120 --> 00:21:55,280
 she was on some pretty heavy pain medication. And right

391
00:21:55,280 --> 00:21:56,560
 before she was going to take it,

392
00:21:56,560 --> 00:21:58,690
 I said, "Well, let's try this." And this is exactly what I

393
00:21:58,690 --> 00:21:59,800
 did with her. I led her through

394
00:21:59,800 --> 00:22:03,550
 the watching the stomach rising, falling and then she was

395
00:22:03,550 --> 00:22:05,600
 suddenly this pain arose in her

396
00:22:05,600 --> 00:22:08,090
 stomach and she wanted to take her medication and they said

397
00:22:08,090 --> 00:22:09,640
, "Well, try this first." You

398
00:22:09,640 --> 00:22:13,710
 know, see what it does, say to yourself, "Pain, pain." And

399
00:22:13,710 --> 00:22:17,480
 so she closed her eyes and she tried

400
00:22:17,480 --> 00:22:22,430
 and I guess she tried it, she said to herself, "Pain." And

401
00:22:22,430 --> 00:22:27,480
 then she fell asleep. She passed

402
00:22:27,480 --> 00:22:32,130
 out into unconsciousness probably because she had not been

403
00:22:32,130 --> 00:22:34,160
 sleeping very well due to

404
00:22:34,160 --> 00:22:36,980
 the pain and the suffering. But as a result, she didn't

405
00:22:36,980 --> 00:22:38,080
 have to take the medication at

406
00:22:38,080 --> 00:22:41,240
 that time. Now, I didn't have time to stay with her and she

407
00:22:41,240 --> 00:22:42,680
 obviously would have stayed

408
00:22:42,680 --> 00:22:46,610
 on the medication. But if you can keep up with this, if you

409
00:22:46,610 --> 00:22:47,800
 can help people through

410
00:22:47,800 --> 00:22:52,450
 it and explain to people again and again and keep

411
00:22:52,450 --> 00:22:56,480
 encouraging them through it and leading

412
00:22:56,480 --> 00:22:59,550
 them through it and meditating with them and saying, "Let's

413
00:22:59,550 --> 00:23:01,600
 try it again." Eventually,

414
00:23:01,600 --> 00:23:04,900
 you can, if not wean them off the medication, you can at

415
00:23:04,900 --> 00:23:07,240
 least help them to keep the medication

416
00:23:07,240 --> 00:23:12,480
 at a minimum or only when it's terribly, incredibly severe.

417
00:23:12,480 --> 00:23:15,640
 And in fact, as I said, these sorts

418
00:23:15,640 --> 00:23:18,890
 of people are prime, can be prime material for the

419
00:23:18,890 --> 00:23:21,560
 meditation practice and can gain great

420
00:23:21,560 --> 00:23:25,610
 insights and wisdom. They can actually mitigate some of the

421
00:23:25,610 --> 00:23:28,000
 effects of illness and some people

422
00:23:28,000 --> 00:23:31,290
 are able to overcome the illness as a result of the easing

423
00:23:31,290 --> 00:23:33,200
 up of the tension and the body's

424
00:23:33,200 --> 00:23:38,150
 better ability to heal itself. But at the very least, they

425
00:23:38,150 --> 00:23:39,960
'll be able to deal with

426
00:23:39,960 --> 00:23:44,160
 the end of life and they should find that, especially with

427
00:23:44,160 --> 00:23:46,040
 the help of someone who's

428
00:23:46,040 --> 00:23:48,930
 done it before and who's encouraging them through it and

429
00:23:48,930 --> 00:23:50,440
 reminding them about these

430
00:23:50,440 --> 00:23:59,110
 things, that their whole attitude will change and they will

431
00:23:59,110 --> 00:24:03,240
 become, in many ways, a new

432
00:24:03,240 --> 00:24:07,340
 person much better able to deal with the difficulties and

433
00:24:07,340 --> 00:24:09,520
 the problem. So I think that's my cue

434
00:24:09,520 --> 00:24:13,500
 to finish. I'd like to thank you all for listening, for

435
00:24:13,500 --> 00:24:18,480
 tuning in. And I hope this helps too and

436
00:24:18,480 --> 00:24:21,190
 I hope that people are able to use this for helping people

437
00:24:21,190 --> 00:24:22,800
 in these sorts of situations.

438
00:24:22,800 --> 00:24:24,160
 So thanks for tuning in, all of us.

