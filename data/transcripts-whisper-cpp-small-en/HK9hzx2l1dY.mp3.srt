1
00:00:00,000 --> 00:00:07,000
 Okay, good morning.

2
00:00:07,000 --> 00:00:21,000
 This marks the beginning of our new experiment.

3
00:00:21,000 --> 00:00:29,080
 Well, this isn't really an experiment, although the posting

4
00:00:29,080 --> 00:00:33,000
 it on the net is ideas to get

5
00:00:33,000 --> 00:00:41,000
 back into giving daily talks at our center.

6
00:00:41,000 --> 00:00:53,000
 So I'll just find some random topic on the dumbbell.

7
00:00:53,000 --> 00:01:05,000
 Sometimes maybe it will be relevant to current affairs.

8
00:01:05,000 --> 00:01:12,000
 This topic is especially relevant.

9
00:01:12,000 --> 00:01:22,000
 Today I'll be talking about sickness.

10
00:01:22,000 --> 00:01:24,000
 So I'll be talking about the

11
00:01:26,000 --> 00:01:28,000
 .

12
00:01:30,000 --> 00:01:32,000
 .

13
00:01:34,000 --> 00:01:36,000
 .

14
00:01:38,000 --> 00:02:04,000
 .

15
00:02:04,000 --> 00:02:33,000
 I'm going to talk about four kinds of sickness.

16
00:02:33,000 --> 00:02:40,000
 Meaning there are four sources of sickness.

17
00:02:40,000 --> 00:02:48,000
 Where does sickness come from?

18
00:02:48,000 --> 00:02:51,210
 Well, I don't know that it's so much about wondering where

19
00:02:51,210 --> 00:02:55,000
 sickness comes from as it is about.

20
00:02:55,000 --> 00:03:01,000
 Understanding sickness and becoming more familiar.

21
00:03:01,000 --> 00:03:30,000
 I'm just reflecting on sickness really.

22
00:03:30,000 --> 00:03:41,000
 .

23
00:03:41,000 --> 00:03:49,000
 We reflect on sickness.

24
00:03:49,000 --> 00:03:54,000
 It allows us to understand.

25
00:03:54,000 --> 00:03:58,000
 Understand our health better.

26
00:03:58,000 --> 00:04:08,750
 It allows us to be more in tune and in touch with the

27
00:04:08,750 --> 00:04:13,000
 causes of suffering.

28
00:04:13,000 --> 00:04:19,000
 The Buddha said, "Arogya paramalabha."

29
00:04:19,000 --> 00:04:25,000
 Freedom from sickness is the greatest gain.

30
00:04:25,000 --> 00:04:37,000
 So he meant something more by sickness than simply.

31
00:04:37,000 --> 00:04:55,000
 Indigestion or a virus.

32
00:04:55,000 --> 00:05:02,240
 Is clearly being free from physical sickness isn't

33
00:05:02,240 --> 00:05:10,000
 sufficient to be considered the greatest gain.

34
00:05:10,000 --> 00:05:16,760
 So if we put in perspective we can categorize all kinds of

35
00:05:16,760 --> 00:05:19,000
 sickness.

36
00:05:19,000 --> 00:05:24,000
 As the same thing, as the same, in the same category.

37
00:05:24,000 --> 00:05:30,000
 I can put in the same category all types of sickness.

38
00:05:30,000 --> 00:05:48,000
 Something that constitutes suffering, causes suffering.

39
00:05:48,000 --> 00:05:59,000
 Something that disturbs peace, happiness.

40
00:05:59,000 --> 00:06:05,760
 The four sources of suffering to really give a sense of the

41
00:06:05,760 --> 00:06:09,000
 whole spectrum of sickness.

42
00:06:09,000 --> 00:06:16,000
 The four kinds of sickness, four sources of sickness.

43
00:06:16,000 --> 00:06:23,000
 First of all, ahara, food.

44
00:06:23,000 --> 00:06:34,000
 Second, utu, means environment.

45
00:06:34,000 --> 00:06:41,000
 Third, chitta, the mind.

46
00:06:41,000 --> 00:06:52,000
 Fourth, kama, action.

47
00:06:52,000 --> 00:07:01,000
 These are the four sources of sickness.

48
00:07:01,000 --> 00:07:04,950
 To be free from all four of these, this would be the

49
00:07:04,950 --> 00:07:07,000
 greatest gain indeed.

50
00:07:07,000 --> 00:07:11,240
 First two are simple, they relate to the physical types of

51
00:07:11,240 --> 00:07:12,000
 sickness.

52
00:07:12,000 --> 00:07:17,000
 But they have an importance in the Dhamma as well.

53
00:07:17,000 --> 00:07:25,100
 Food for example, to start with food, seems like something

54
00:07:25,100 --> 00:07:35,000
 mundane, not really spiritual.

55
00:07:35,000 --> 00:07:43,630
 But food is a very important Dhamma, very important part of

56
00:07:43,630 --> 00:07:46,000
 Dhamma practice.

57
00:07:46,000 --> 00:07:52,000
 A very important Dhamma lesson.

58
00:07:52,000 --> 00:07:59,700
 Studying food gives us a very good perspective, clear

59
00:07:59,700 --> 00:08:08,000
 perspective on how the mind works, how the mind clings.

60
00:08:08,000 --> 00:08:15,000
 Food shows us very clearly, more clearly than pretty much

61
00:08:15,000 --> 00:08:16,000
 anything,

62
00:08:16,000 --> 00:08:25,000
 because it's the one thing that we can't live without.

63
00:08:25,000 --> 00:08:29,000
 Food, water, put them together.

64
00:08:29,000 --> 00:08:33,050
 Besides air, let's say, food, but you see food is unique

65
00:08:33,050 --> 00:08:38,860
 because of its diversity and its potential for clinging,

66
00:08:38,860 --> 00:08:42,000
 unlike water, air.

67
00:08:42,000 --> 00:08:51,080
 Food is the one thing that we need that has potential to

68
00:08:51,080 --> 00:08:52,000
 cause craving.

69
00:08:52,000 --> 00:09:02,530
 So more than anything else, it shows us clearly, it

70
00:09:02,530 --> 00:09:11,000
 provides a clear opportunity for us to understand.

71
00:09:11,000 --> 00:09:17,280
 Understand how the mind works, how the mind clings, how the

72
00:09:17,280 --> 00:09:27,000
 mind craves, how the mind chooses.

73
00:09:27,000 --> 00:09:40,970
 Food is actually quite simple, it's meant to nourish the

74
00:09:40,970 --> 00:09:43,000
 body.

75
00:09:43,000 --> 00:09:50,670
 There's no reason for food to bring us sickness. Why would

76
00:09:50,670 --> 00:09:53,000
 it?

77
00:09:53,000 --> 00:09:57,810
 You wouldn't eat something explicitly for the purpose of

78
00:09:57,810 --> 00:10:02,000
 making you sick, not usually, not normally.

79
00:10:02,000 --> 00:10:10,540
 And yet, quite normally, we do eat, consume things that

80
00:10:10,540 --> 00:10:13,000
 make us quite sick.

81
00:10:13,000 --> 00:10:17,790
 So it might seem banal and worldly to talk about things

82
00:10:17,790 --> 00:10:23,240
 like heart disease or high blood pressure, diabetes, even

83
00:10:23,240 --> 00:10:24,000
 cancer.

84
00:10:24,000 --> 00:10:27,750
 And yet all of these are very much related to our food

85
00:10:27,750 --> 00:10:33,000
 consumption, not always, but have a strong correlation.

86
00:10:33,000 --> 00:10:39,160
 In many cases, diabetes, we eat too much sugar, too much

87
00:10:39,160 --> 00:10:44,930
 starch, too much carbohydrates, heart disease, we eat too

88
00:10:44,930 --> 00:10:50,260
 much fat, too much whatever, high blood pressure, too much

89
00:10:50,260 --> 00:10:52,000
 salt.

90
00:10:52,000 --> 00:10:55,000
 Why would you do such a thing?

91
00:10:55,000 --> 00:11:02,810
 We see how our choices are not rational, how we do things

92
00:11:02,810 --> 00:11:08,000
 very much against our own benefit.

93
00:11:08,000 --> 00:11:27,880
 We make ourselves sick. We see how dangerous our minds can

94
00:11:27,880 --> 00:11:29,000
 be through food.

95
00:11:29,000 --> 00:11:34,450
 In fact, taken to its ultimate conclusion, we come to see

96
00:11:34,450 --> 00:11:38,000
 that we don't need to eat very much.

97
00:11:38,000 --> 00:11:43,120
 Much of our food consumption, even though it might not make

98
00:11:43,120 --> 00:11:50,220
 us sick, might not make us overtly, acutely, physically

99
00:11:50,220 --> 00:11:51,000
 sick,

100
00:11:51,000 --> 00:12:05,960
 but still involves sickness, our desire to be strong, plump

101
00:12:05,960 --> 00:12:14,350
, desire to look handsome, beautiful, our desire to grow

102
00:12:14,350 --> 00:12:20,000
 tall.

103
00:12:20,000 --> 00:12:28,040
 Even our desire for good health, it becomes a sickness,

104
00:12:28,040 --> 00:12:33,000
 very easily becomes a sickness.

105
00:12:33,000 --> 00:12:46,400
 Food, the Buddha, very much was very often, very frequently

106
00:12:46,400 --> 00:12:53,460
, careful to exhort the monks in moderation in food, single

107
00:12:53,460 --> 00:12:59,070
 it out, really just single it out, because it is the one

108
00:12:59,070 --> 00:13:02,000
 thing we need.

109
00:13:02,000 --> 00:13:08,100
 It's so familiar to us. We don't realize. If you never

110
00:13:08,100 --> 00:13:15,620
 tried to practice to be mindful, you don't realize how

111
00:13:15,620 --> 00:13:17,000
 attached we become.

112
00:13:17,000 --> 00:13:20,560
 Food seems like something you're not really attached to at

113
00:13:20,560 --> 00:13:22,000
 all for many people.

114
00:13:22,000 --> 00:13:27,480
 People who have gone hungry sometimes have a sense, but

115
00:13:27,480 --> 00:13:33,080
 people who are mindful, hungry or not, they can see very

116
00:13:33,080 --> 00:13:35,000
 clearly.

117
00:13:35,000 --> 00:13:39,140
 Apart from almost anything, it's the one thing we cling to.

118
00:13:39,140 --> 00:13:41,000
 It's one of the things.

119
00:13:41,000 --> 00:13:48,340
 So food is an important dhamma lesson. Try to be mindful

120
00:13:48,340 --> 00:13:51,000
 when you eat.

121
00:13:51,000 --> 00:13:57,550
 The second is, 'utu'. 'utu' really encompasses any sickness

122
00:13:57,550 --> 00:14:01,000
 that comes from the external environment.

123
00:14:01,000 --> 00:14:08,600
 It could be from radiation, it could be from the sun, which

124
00:14:08,600 --> 00:14:15,890
 is radiation, it could be from viruses, it could be from

125
00:14:15,890 --> 00:14:26,000
 bacteria, allergies.

126
00:14:26,000 --> 00:14:29,990
 The one, again, you might think, not very dhamma, not very

127
00:14:29,990 --> 00:14:35,860
 much dhamma, but the clearest lesson you could give maybe

128
00:14:35,860 --> 00:14:38,000
 is to be mindful of these things.

129
00:14:38,000 --> 00:14:44,000
 People who have allergies can have difficulty practicing.

130
00:14:44,000 --> 00:15:02,000
 [silence]

131
00:15:02,000 --> 00:15:07,850
 You find it hard to breathe, hard to sit still when you

132
00:15:07,850 --> 00:15:12,000
 have allergies. So with any sickness, if you have a cold,

133
00:15:12,000 --> 00:15:20,000
 if you have the flu, even if you're dying,

134
00:15:20,000 --> 00:15:24,430
 it often presents a great opportunity to practice, to

135
00:15:24,430 --> 00:15:31,130
 cultivate patience, mindfulness, objectivity. It's a real

136
00:15:31,130 --> 00:15:34,000
 challenge.

137
00:15:34,000 --> 00:15:39,380
 But there is another lesson, and I think it's a lesson that

138
00:15:39,380 --> 00:15:45,380
 includes the food, is that our environment and our food, it

139
00:15:45,380 --> 00:15:54,160
's not entirely independent of our dhamma practice or of our

140
00:15:54,160 --> 00:15:56,000
 state of mind.

141
00:15:56,000 --> 00:16:01,930
 You might say our environment is beyond our control, it

142
00:16:01,930 --> 00:16:05,000
 really isn't in many ways.

143
00:16:05,000 --> 00:16:13,050
 Where we choose to hang out, where we choose to live our

144
00:16:13,050 --> 00:16:21,760
 lives, and how our environment changes based on how we live

145
00:16:21,760 --> 00:16:23,000
 our lives.

146
00:16:23,000 --> 00:16:30,680
 As a society, you can see this on the societal level, as a

147
00:16:30,680 --> 00:16:36,000
 society we destroy our environment.

148
00:16:36,000 --> 00:16:44,950
 Even just the slaughter and consumption of animals, all the

149
00:16:44,950 --> 00:16:50,000
 bacteria and sickness that has come from that,

150
00:16:50,000 --> 00:16:56,560
 that's very karmic, very much related to the sickness of

151
00:16:56,560 --> 00:16:58,000
 killing.

152
00:16:58,000 --> 00:17:01,990
 Our greed for consumption, how it's polluted the air and

153
00:17:01,990 --> 00:17:05,600
 the water, the water that we drink, we've poisoned our own

154
00:17:05,600 --> 00:17:07,000
 drinking water.

155
00:17:07,000 --> 00:17:16,640
 We can't drink it anymore. Out of greed, nothing but greed,

156
00:17:16,640 --> 00:17:25,000
 there's no other excuse, there's no other reason.

157
00:17:25,000 --> 00:17:46,000
 So there's a dhamma perspective on these as well.

158
00:17:46,000 --> 00:17:53,890
 The third is jitta, and jitta is where our real dhamma

159
00:17:53,890 --> 00:17:57,000
 practice begins.

160
00:17:57,000 --> 00:18:08,000
 The main focus of our practice is the healing of the mind.

161
00:18:08,000 --> 00:18:16,730
 So the mind really can make us very sick, physically. We

162
00:18:16,730 --> 00:18:23,000
 might get very angry,

163
00:18:23,000 --> 00:18:27,030
 and our blood pressure spikes from the anger, it can spike

164
00:18:27,030 --> 00:18:29,000
 because of greed as well.

165
00:18:29,000 --> 00:18:35,630
 And prolonged exposure to anger, greed, even delusion, I

166
00:18:35,630 --> 00:18:45,000
 would say, these have

167
00:18:45,000 --> 00:18:56,000
 notable measurable effects on the body.

168
00:18:56,000 --> 00:19:02,740
 Prolonged exposure, prolonged engagement, prolonged

169
00:19:02,740 --> 00:19:05,000
 exposure might be the best way to put it.

170
00:19:05,000 --> 00:19:09,350
 We think of anger as something we do. In many ways it's

171
00:19:09,350 --> 00:19:12,000
 something that happens to us.

172
00:19:12,000 --> 00:19:15,000
 It's like a sickness.

173
00:19:15,000 --> 00:19:22,440
 The Buddha called them a kantuka. A kantuka means they come

174
00:19:22,440 --> 00:19:29,000
, they visit, they invade our minds.

175
00:19:29,000 --> 00:19:43,000
 They aren't our minds exactly, they're like afflictions.

176
00:19:43,000 --> 00:19:55,000
 And so if we're exposed to this affliction, or if we are

177
00:19:55,000 --> 00:19:56,000
 infected,

178
00:19:56,000 --> 00:20:08,010
 by anger, greed, it grows like bacteria, it spreads like

179
00:20:08,010 --> 00:20:14,000
 fire, grows like a virus.

180
00:20:14,000 --> 00:20:18,660
 You can have real effects on the body, but that's not the

181
00:20:18,660 --> 00:20:21,000
 only way that it's a sickness, of course.

182
00:20:21,000 --> 00:20:26,540
 In fact, the most important aspect of how greed and anger

183
00:20:26,540 --> 00:20:32,000
 are sicknesses is mental.

184
00:20:32,000 --> 00:20:36,000
 They lead to mental suffering.

185
00:20:36,000 --> 00:20:41,140
 They are mental sickness. They cause us to hurt other

186
00:20:41,140 --> 00:20:42,000
 people.

187
00:20:42,000 --> 00:20:52,000
 The cause for so much suffering, greed, anger, delusion,

188
00:20:52,000 --> 00:21:04,000
 all conceit and jealousy, arrogance and so on and so on.

189
00:21:04,000 --> 00:21:11,000
 The real sickness.

190
00:21:11,000 --> 00:21:19,720
 And here's where we see most clearly that mindfulness is

191
00:21:19,720 --> 00:21:22,000
 effective.

192
00:21:22,000 --> 00:21:29,000
 I practice mindfulness to cure the sicknesses of the mind.

193
00:21:29,000 --> 00:21:38,000
 It's very much like an antibiotic or something.

194
00:21:38,000 --> 00:21:41,260
 We don't have to stop ourselves from getting angry or

195
00:21:41,260 --> 00:21:42,000
 greedy.

196
00:21:42,000 --> 00:21:46,240
 All we have to do is take our medicine and it's like a cure

197
00:21:46,240 --> 00:21:47,000
-all.

198
00:21:47,000 --> 00:21:52,530
 Why is it a cure-all? Because it's the opposite of reaction

199
00:21:52,530 --> 00:21:53,000
.

200
00:21:53,000 --> 00:21:58,460
 All defilements, they're all reactions. They are all

201
00:21:58,460 --> 00:22:02,000
 responses to experience.

202
00:22:02,000 --> 00:22:06,000
 Mindfulness is a different type of response.

203
00:22:06,000 --> 00:22:13,600
 It replaces the sickness, the responses that are unwholes

204
00:22:13,600 --> 00:22:18,000
ome, unpleasant, unbeneficial, harmful,

205
00:22:18,000 --> 00:22:22,590
 with a neutral response, a response that is harmless, that

206
00:22:22,590 --> 00:22:31,000
 is peaceful, that is healthy.

207
00:22:31,000 --> 00:22:42,000
 The fourth sickness is kamma.

208
00:22:42,000 --> 00:22:49,540
 Kamma in some ways is just an extrapolation of mental

209
00:22:49,540 --> 00:22:51,000
 sickness.

210
00:22:51,000 --> 00:22:57,790
 But it is a different category simply because our mental

211
00:22:57,790 --> 00:23:04,000
 illness doesn't just make us sick as it's happening.

212
00:23:04,000 --> 00:23:11,330
 It can have profound effects on our future existences as

213
00:23:11,330 --> 00:23:12,000
 well.

214
00:23:12,000 --> 00:23:17,250
 Let's not even talk about future lives, but because our

215
00:23:17,250 --> 00:23:21,000
 sickness, I suppose you're very angry.

216
00:23:21,000 --> 00:23:26,560
 Getting angry will hurt you just as you're angry. It's

217
00:23:26,560 --> 00:23:28,000
 painful.

218
00:23:28,000 --> 00:23:34,340
 As I said, we can see that it has effects if there's

219
00:23:34,340 --> 00:23:37,000
 prolonged exposure.

220
00:23:37,000 --> 00:23:41,000
 It can make you really sick and so on.

221
00:23:41,000 --> 00:23:46,000
 But beyond that, it causes us to do and say things.

222
00:23:46,000 --> 00:23:49,240
 There are many things we wouldn't say or we'd say

223
00:23:49,240 --> 00:23:52,000
 differently if we didn't have anger.

224
00:23:52,000 --> 00:23:56,000
 Sometimes we regret those things.

225
00:23:56,000 --> 00:24:01,000
 But once they're done, there's no taking them back.

226
00:24:01,000 --> 00:24:06,000
 Sometimes it's even beyond what we intended.

227
00:24:06,000 --> 00:24:10,630
 It's one thing to be angry at someone, but if you strike

228
00:24:10,630 --> 00:24:12,000
 them, you've gone the next step.

229
00:24:12,000 --> 00:24:15,000
 You've done something beyond just getting angry.

230
00:24:15,000 --> 00:24:19,830
 If you lash out to speak to someone in anger, you've

231
00:24:19,830 --> 00:24:21,000
 performed a real karma.

232
00:24:21,000 --> 00:24:26,000
 It's on another level in some ways.

233
00:24:26,000 --> 00:24:30,000
 If you kill, you can't take that back.

234
00:24:30,000 --> 00:24:36,170
 If you're angry, well, you can't take that back either, but

235
00:24:36,170 --> 00:24:38,000
 it's not yet on the same magnitude.

236
00:24:38,000 --> 00:24:48,510
 There's something very exceptional about actual acts based

237
00:24:48,510 --> 00:24:52,000
 on our defilement.

238
00:24:52,000 --> 00:24:56,500
 Because you see that once you've done it, once you've

239
00:24:56,500 --> 00:25:02,000
 engaged with the universe, the universe will respond.

240
00:25:02,000 --> 00:25:06,000
 Killing, stealing, lying, cheating.

241
00:25:06,000 --> 00:25:10,000
 All of these things have consequences in the universe.

242
00:25:10,000 --> 00:25:14,000
 They change our universe.

243
00:25:14,000 --> 00:25:16,000
 They change the way people interact with us.

244
00:25:16,000 --> 00:25:21,000
 They change the way we live our lives.

245
00:25:21,000 --> 00:25:29,000
 The guilt that comes, we have to live with that.

246
00:25:29,000 --> 00:25:35,150
 When we pass away, there's the idea that our minds, the

247
00:25:35,150 --> 00:25:42,000
 sickness of the mind, will affect our future life.

248
00:25:42,000 --> 00:25:46,280
 Where we choose to be reborn will be affected by our

249
00:25:46,280 --> 00:25:48,000
 clarity of mind.

250
00:25:48,000 --> 00:25:51,000
 How the fetus develops.

251
00:25:51,000 --> 00:25:54,580
 I was reading about it. I think there is something to this

252
00:25:54,580 --> 00:26:02,000
 idea that in the fetus, they call them mutations, I think.

253
00:26:02,000 --> 00:26:06,000
 Why would mutations occur?

254
00:26:06,000 --> 00:26:12,780
 Of course, most people would say it's just random or chance

255
00:26:12,780 --> 00:26:16,000
 or just the way it happens.

256
00:26:16,000 --> 00:26:19,000
 Buddhism, we would say there's more to it than that.

257
00:26:19,000 --> 00:26:23,000
 That the mind is active.

258
00:26:23,000 --> 00:26:27,720
 And the mind's engagement, obsessions and attention that

259
00:26:27,720 --> 00:26:32,000
 the mind pays, just the repeated attention in certain ways,

260
00:26:32,000 --> 00:26:38,170
 will affect things, can affect the fetus, can lead to great

261
00:26:38,170 --> 00:26:43,000
 sickness, lifetime sickness.

262
00:26:43,000 --> 00:26:47,230
 And it's different from mental sickness because some people

263
00:26:47,230 --> 00:26:49,000
 can be very good people.

264
00:26:49,000 --> 00:26:57,280
 Some people are very healthy and wicked, mean, nasty,

265
00:26:57,280 --> 00:27:01,000
 speaking nasty things, doing nasty things.

266
00:27:01,000 --> 00:27:06,070
 Other people are quite nice and yet suffer from

267
00:27:06,070 --> 00:27:12,460
 debilitating illnesses that you might think are based on

268
00:27:12,460 --> 00:27:13,000
 karma.

269
00:27:13,000 --> 00:27:21,000
 And yet they are generally nice people.

270
00:27:21,000 --> 00:27:27,690
 Mental illness. Mental illness can hurt you, but it can

271
00:27:27,690 --> 00:27:34,090
 also cause you to do things that have far greater lasting

272
00:27:34,090 --> 00:27:35,000
 effect.

273
00:27:35,000 --> 00:27:39,720
 Be reborn in a place that doesn't allow you to develop

274
00:27:39,720 --> 00:27:49,000
 spiritually or in a body that makes it difficult.

275
00:27:49,000 --> 00:27:55,000
 So some thoughts on sickness. Again, just random teachings.

276
00:27:55,000 --> 00:27:59,330
 Just pick something every morning, maybe something someone

277
00:27:59,330 --> 00:28:02,000
 said to me will inspire a teaching.

278
00:28:02,000 --> 00:28:05,000
 Thank you all for listening. Have a good day.

279
00:28:06,000 --> 00:28:21,080
 [

