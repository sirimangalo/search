1
00:00:00,000 --> 00:00:07,000
 Good evening.

2
00:00:07,000 --> 00:00:10,000
 Broadcasting live.

3
00:00:10,000 --> 00:00:15,000
 September 23rd, 2015.

4
00:00:15,000 --> 00:00:22,240
 It's been another long day.

5
00:00:22,240 --> 00:00:27,040
 Today we had our Wednesday group on the lawn.

6
00:00:27,040 --> 00:00:32,040
 I was there from 3.30 to 6.

7
00:00:32,040 --> 00:00:38,040
 I taught meditation about four times.

8
00:00:38,040 --> 00:00:42,040
 I'm very good about coming at the same time.

9
00:00:42,040 --> 00:00:45,040
 I left it open.

10
00:00:45,040 --> 00:00:49,040
 I was teaching right up until 6.

11
00:00:49,040 --> 00:00:51,040
 Small group.

12
00:00:51,040 --> 00:00:55,040
 But new people, there was a few new people.

13
00:00:55,040 --> 00:00:58,040
 People have classes, you know, so they come and they go

14
00:00:58,040 --> 00:01:03,040
 according to their schedules.

15
00:01:03,040 --> 00:01:07,040
 And our first meeting is next week.

16
00:01:07,040 --> 00:01:22,040
 There's that.

17
00:01:22,040 --> 00:01:26,040
 But today we have a quote.

18
00:01:26,040 --> 00:01:29,040
 Robin, would you read the quote for us?

19
00:01:29,040 --> 00:01:30,040
 Yes.

20
00:01:30,040 --> 00:01:32,770
 I do not say that the attainment of profound knowledge

21
00:01:32,770 --> 00:01:34,040
 comes straight away.

22
00:01:34,040 --> 00:01:37,040
 On the contrary, it comes by gradual training.

23
00:01:37,040 --> 00:01:39,040
 A gradual doing.

24
00:01:39,040 --> 00:01:45,040
 A gradual practice.

25
00:01:45,040 --> 00:01:52,040
 I'm not clear on this profound knowledge thing.

26
00:01:52,040 --> 00:01:58,040
 Anyara.

27
00:01:58,040 --> 00:02:01,040
 Don't get it.

28
00:02:01,040 --> 00:02:05,040
 Anyara.

29
00:02:05,040 --> 00:02:11,040
 Because it should mean.

30
00:02:11,040 --> 00:02:19,040
 Because bhikkhu bodhi says final knowledge.

31
00:02:19,040 --> 00:02:22,040
 Well, it's beyond me.

32
00:02:22,040 --> 00:02:27,260
 Also, you know, studying Latin, I think my language part of

33
00:02:27,260 --> 00:02:29,040
 my brain is fried.

34
00:02:29,040 --> 00:02:35,040
 Can we assume they're speaking of enlightenment?

35
00:02:35,040 --> 00:02:39,040
 I think it's more like final attainment.

36
00:02:39,040 --> 00:02:41,040
 The attainment of profound knowledge.

37
00:02:41,040 --> 00:02:43,040
 Don't get that.

38
00:02:43,040 --> 00:02:46,040
 Unless I'm looking at it, unless this is the wrong.

39
00:02:46,040 --> 00:02:49,220
 No, but this is it because, yeah, bhikkhu bodhi says the

40
00:02:49,220 --> 00:02:50,040
 same thing.

41
00:02:50,040 --> 00:02:58,040
 I do not say that final knowledge is achieved all at once.

42
00:02:58,040 --> 00:02:59,040
 Yeah, I'm just not.

43
00:02:59,040 --> 00:03:01,040
 I think it's my fault.

44
00:03:01,040 --> 00:03:06,040
 But it's a very strange word.

45
00:03:06,040 --> 00:03:08,040
 But gradual.

46
00:03:08,040 --> 00:03:11,490
 So it's not about sitting in meditation and suddenly, poof,

47
00:03:11,490 --> 00:03:13,040
 you're enlightened.

48
00:03:13,040 --> 00:03:18,040
 It's about getting just perfect.

49
00:03:18,040 --> 00:03:23,040
 Getting to the perfect state of mind.

50
00:03:23,040 --> 00:03:27,730
 And the perfect state of mind is not about what you

51
00:03:27,730 --> 00:03:29,040
 experience.

52
00:03:29,040 --> 00:03:35,060
 It has, in fact, the perfect state of mind is often

53
00:03:35,060 --> 00:03:40,040
 associated with the worst experience.

54
00:03:40,040 --> 00:03:43,800
 People who become enlightened when they die, it's a very

55
00:03:43,800 --> 00:03:45,040
 common thing.

56
00:03:45,040 --> 00:03:47,380
 People who become enlightened when they're sick, it's a

57
00:03:47,380 --> 00:03:48,040
 very common thing.

58
00:03:48,040 --> 00:03:51,530
 People who become enlightened when they're in excruciating

59
00:03:51,530 --> 00:03:54,040
 pain, it's very common.

60
00:03:54,040 --> 00:03:59,280
 It's not like everyone who dies and gets sick and has pain

61
00:03:59,280 --> 00:04:02,040
 will become enlightened.

62
00:04:02,040 --> 00:04:12,010
 But Ajahn Tong talks about four types of samasisi, which

63
00:04:12,010 --> 00:04:18,040
 means enlightenment at the same time.

64
00:04:18,040 --> 00:04:24,690
 So you become enlightened at the same time. Your defile

65
00:04:24,690 --> 00:04:31,040
ments are removed when you have--

66
00:04:31,040 --> 00:04:38,920
 it's something to do when you're sick, when you die, and

67
00:04:38,920 --> 00:04:43,040
 when you have great pain.

68
00:04:43,040 --> 00:04:48,040
 And the fourth one is when you change your postures.

69
00:04:48,040 --> 00:04:52,530
 But it's rare that--well, it's not rare. It's not as

70
00:04:52,530 --> 00:04:56,040
 conducive to enlightenment

71
00:04:56,040 --> 00:05:00,040
 when you experience pleasant and pleasurable states.

72
00:05:00,040 --> 00:05:02,450
 So anyone who thinks they're close to enlightenment because

73
00:05:02,450 --> 00:05:06,040
 meditation is comfortable and blissful,

74
00:05:06,040 --> 00:05:10,110
 it will be comfortable for another reason. There will be

75
00:05:10,110 --> 00:05:13,040
 still the arising and ceasing phenomena

76
00:05:13,040 --> 00:05:16,040
 that are impermanent, suffering, and non-self.

77
00:05:16,040 --> 00:05:18,700
 But one will be perfectly quantum about them, experiencing

78
00:05:18,700 --> 00:05:21,040
 them, seeing them for what they are.

79
00:05:21,040 --> 00:05:25,330
 This is this, this is this, without any of the previous

80
00:05:25,330 --> 00:05:26,040
 reactions.

81
00:05:26,040 --> 00:05:29,430
 So the difference--how you know is it's just like your

82
00:05:29,430 --> 00:05:31,040
 ordinary experience,

83
00:05:31,040 --> 00:05:36,840
 minus all the attachments, the reactions. So it would be an

84
00:05:36,840 --> 00:05:38,040
 ordinary experience.

85
00:05:38,040 --> 00:05:40,560
 Like you become enlightened just watching the stomach

86
00:05:40,560 --> 00:05:41,040
 rising, falling,

87
00:05:41,040 --> 00:05:45,280
 and then you just see suddenly impermanent suffering or non

88
00:05:45,280 --> 00:05:49,040
-self. Very clear.

89
00:05:49,040 --> 00:05:53,040
 And the next moment, the mind lets go.

90
00:05:53,040 --> 00:05:58,190
 So the gradual practice is transforming our experience, our

91
00:05:58,190 --> 00:06:00,040
 ordinary experience,

92
00:06:00,040 --> 00:06:05,040
 from reactionary to non-reactionary. That's it.

93
00:06:05,040 --> 00:06:11,180
 From reactionary to objective. Once we affect that

94
00:06:11,180 --> 00:06:13,040
 transformation,

95
00:06:13,040 --> 00:06:17,430
 once we're completely objective, that last moment will be

96
00:06:17,430 --> 00:06:19,040
 one of complete objectivity

97
00:06:19,040 --> 00:06:24,320
 where we say, "It's not worth it." And then the left will.

98
00:06:24,320 --> 00:06:25,040
 No more clinging.

99
00:06:25,040 --> 00:06:32,040
 That's the path.

100
00:06:32,040 --> 00:06:37,040
 And it's about the habits. The reason why our reactivity,

101
00:06:37,040 --> 00:06:38,040
 our reaction,

102
00:06:38,040 --> 00:06:44,040
 our reactivity is a habit that we've built up.

103
00:06:44,040 --> 00:06:51,040
 And habits cannot be turned off. They must be worn away.

104
00:06:51,040 --> 00:06:55,770
 They must be supplanted. So you have to cultivate a new

105
00:06:55,770 --> 00:06:56,040
 habit,

106
00:06:56,040 --> 00:07:02,040
 a habit of mindfulness. It just takes time.

107
00:07:02,040 --> 00:07:05,040
 Enlightenment actually does happen in a moment.

108
00:07:05,040 --> 00:07:08,040
 It's just that moment where you finally get it.

109
00:07:08,040 --> 00:07:12,710
 But that moment doesn't--you don't just fall into it like a

110
00:07:12,710 --> 00:07:14,040
 pit or a well.

111
00:07:14,040 --> 00:07:17,040
 You have to get there like climbing a mountain.

112
00:07:17,040 --> 00:07:23,040
 And when you get to the peak, that's it.

113
00:07:23,040 --> 00:07:31,040
 So that's the idea.

114
00:07:31,040 --> 00:07:36,040
 Have any questions?

115
00:07:36,040 --> 00:07:40,040
 I had some questions earlier on this afternoon.

116
00:07:40,040 --> 00:07:45,040
 There were. And there was one that kind of stood out.

117
00:07:45,040 --> 00:07:49,320
 One of the meditators was asking about--we've been talking

118
00:07:49,320 --> 00:07:52,040
 about the food basics cards yesterday.

119
00:07:52,040 --> 00:07:55,040
 There was a meditator question.

120
00:07:55,040 --> 00:07:59,720
 Oh, that too. There's time. But just while I'm thinking of

121
00:07:59,720 --> 00:08:00,040
 it,

122
00:08:00,040 --> 00:08:04,140
 I was just meeting with the treasurer and some other people

123
00:08:04,140 --> 00:08:05,040
 in the organization.

124
00:08:05,040 --> 00:08:09,040
 And the treasurer suggested if anyone was interested in

125
00:08:09,040 --> 00:08:11,040
 sending a food basics card,

126
00:08:11,040 --> 00:08:13,700
 what they can do is just send it to the regular support

127
00:08:13,700 --> 00:08:14,040
 page.

128
00:08:14,040 --> 00:08:18,470
 And there's a part where you indicate what your donation is

129
00:08:18,470 --> 00:08:19,040
 for.

130
00:08:19,040 --> 00:08:22,340
 So you would just send it to sirimongolo.org with an

131
00:08:22,340 --> 00:08:25,040
 indication that it's for a food basics food card.

132
00:08:25,040 --> 00:08:28,370
 And the treasurer who lives right near a food basics will

133
00:08:28,370 --> 00:08:32,040
 make sure that that's taken care of and sent to Bante.

134
00:08:32,040 --> 00:08:36,040
 So I'll put that link on there.

135
00:08:36,040 --> 00:08:40,040
 You have added privileges on the support page, right?

136
00:08:40,040 --> 00:08:44,040
 I believe so, yes. So I can update that as well.

137
00:08:44,040 --> 00:08:46,040
 But there's another page?

138
00:08:46,040 --> 00:08:51,040
 I was just going to put it just in the chat here.

139
00:08:51,040 --> 00:08:56,910
 And on the comment on the YouTube video. But I'll put it on

140
00:08:56,910 --> 00:09:00,040
 the support page as well.

141
00:09:00,040 --> 00:09:02,040
 Maslow had a question.

142
00:09:02,040 --> 00:09:05,560
 Yes. Would you recommend Mahasi Sayedwa writing which would

143
00:09:05,560 --> 00:09:08,880
 elucidate the more technical aspects of Satipatana practice

144
00:09:08,880 --> 00:09:09,040
?

145
00:09:09,040 --> 00:09:11,040
 Thank you, Bante.

146
00:09:11,040 --> 00:09:13,920
 He doesn't have any writings that are technical, like my

147
00:09:13,920 --> 00:09:15,040
 book is technical.

148
00:09:15,040 --> 00:09:21,280
 There's a couple that get close, but they don't go into

149
00:09:21,280 --> 00:09:24,040
 detail detail.

150
00:09:24,040 --> 00:09:28,640
 I'm not sure quite why. We don't have one that gives

151
00:09:28,640 --> 00:09:31,040
 detailed explanation.

152
00:09:31,040 --> 00:09:35,040
 But he goes fairly close.

153
00:09:35,040 --> 00:09:41,040
 There's a good one called Practical Vipassana Exercises.

154
00:09:41,040 --> 00:09:44,040
 Practical...

155
00:09:44,040 --> 00:09:58,040
 I don't think that's it. No, it's not that one.

156
00:09:58,040 --> 00:10:01,040
 I mean, that one is one.

157
00:10:01,040 --> 00:10:06,040
 But there's...

158
00:10:06,040 --> 00:10:17,660
 Something called basic and progressive. Something basic and

159
00:10:17,660 --> 00:10:23,040
 progressive or something.

160
00:10:23,040 --> 00:10:29,040
 Practical insight meditation. Basic and progressive stages.

161
00:10:29,040 --> 00:10:36,820
 Let's see if I can find the EPUB. The EPUB or the PDF is

162
00:10:36,820 --> 00:10:39,040
 somewhere.

163
00:10:39,040 --> 00:10:47,040
 Oh, here's a good website that has them.

164
00:10:47,040 --> 00:10:58,120
 PDF. But I think they are... Let's see if I have it on

165
00:10:58,120 --> 00:11:01,040
 static.

166
00:11:01,040 --> 00:11:10,040
 No, I don't.

167
00:11:10,040 --> 00:11:17,040
 Is that the one, Practical Vipassana Meditation Exercises?

168
00:11:17,040 --> 00:11:18,770
 There's Practical Vipassana, but then there's something

169
00:11:18,770 --> 00:11:26,970
 called Practical Insight Meditation, Basic and Progressive

170
00:11:26,970 --> 00:11:28,040
 Stages.

171
00:11:28,040 --> 00:11:32,040
 Here's one from archive.org.

172
00:11:32,040 --> 00:11:34,040
 Oh, there's a good one.

173
00:11:34,040 --> 00:11:41,310
 Archive.org has it. Let's see if it's any good. How do we

174
00:11:41,310 --> 00:11:42,040
 do this?

175
00:11:42,040 --> 00:11:46,370
 I think this is my book. I think this is my version. Wait

176
00:11:46,370 --> 00:11:49,040
 just a second. No, maybe not.

177
00:11:49,040 --> 00:11:56,040
 It's funny because I put together a book. Oh, and the...

178
00:11:56,040 --> 00:12:02,040
 It's funny. No, it just looks like the one I put together.

179
00:12:02,040 --> 00:12:06,040
 It's mostly good, but the poly is all mixed up, messed up.

180
00:12:06,040 --> 00:12:11,040
 Anyway, here's one.

181
00:12:11,040 --> 00:12:14,790
 Not that one, Robin, it's this one. I think they're two

182
00:12:14,790 --> 00:12:17,040
 different books.

183
00:12:17,040 --> 00:12:46,040
 This one seems pretty good, the archive.org version.

184
00:12:46,040 --> 00:13:11,040
 The thing I realized about food basics,

185
00:13:11,040 --> 00:13:16,140
 that should have come to my mind right away, but didn't, is

186
00:13:16,140 --> 00:13:18,040
 that I can't actually go shopping there.

187
00:13:18,040 --> 00:13:21,040
 We're not allowed to go shopping.

188
00:13:21,040 --> 00:13:25,040
 And that's a problem I've had at certain restaurants is,

189
00:13:25,040 --> 00:13:28,090
 like there's a Tim Hortons in the university and there's a

190
00:13:28,090 --> 00:13:29,040
 Starbucks as well.

191
00:13:29,040 --> 00:13:32,040
 And I've got cards for both of those places.

192
00:13:32,040 --> 00:13:37,090
 But the juice and the sandwiches or whatever, you have to

193
00:13:37,090 --> 00:13:39,040
 pick them up by yourself.

194
00:13:39,040 --> 00:13:43,680
 I still have to be given the food. It's another one of our

195
00:13:43,680 --> 00:13:44,040
 rules.

196
00:13:44,040 --> 00:13:51,040
 If a person doesn't give the food to me, I can't eat it.

197
00:13:51,040 --> 00:13:56,040
 So maybe that's not the best idea after all.

198
00:13:56,040 --> 00:13:59,040
 I was going to say something right away, but then I thought

199
00:13:59,040 --> 00:14:01,040
, well, maybe they should probably have a deli, right?

200
00:14:01,040 --> 00:14:04,040
 And in the deli they offer, they give it to you.

201
00:14:04,040 --> 00:14:07,960
 And the deli is the best anyway because the food is already

202
00:14:07,960 --> 00:14:10,040
 cooked, prepared.

203
00:14:10,040 --> 00:14:13,040
 It is, it is, yes.

204
00:14:13,040 --> 00:14:22,040
 I guess it's more expensive.

205
00:14:22,040 --> 00:14:29,040
 But yeah, that would be it. All I can do is go to the deli.

206
00:14:29,040 --> 00:14:34,040
 Unless someone went with me.

207
00:14:34,040 --> 00:14:38,040
 But, you know, I mean, it's getting a little bit weird.

208
00:14:38,040 --> 00:14:41,880
 I mean, what I want to move towards, of course, is going on

209
00:14:41,880 --> 00:14:43,040
 Aum's Round.

210
00:14:43,040 --> 00:14:46,040
 That's the eventual key.

211
00:14:46,040 --> 00:14:51,170
 When there's a community here and they want to keep me

212
00:14:51,170 --> 00:14:57,040
 alive, they will feed me.

213
00:14:57,040 --> 00:15:12,040
 But right now our community is digital.

214
00:15:12,040 --> 00:15:14,040
 We're not allowed to move food.

215
00:15:14,040 --> 00:15:16,040
 I couldn't pick it up and put it on the register.

216
00:15:16,040 --> 00:15:20,710
 If I touch it, if I touch the food, I can never, I commit

217
00:15:20,710 --> 00:15:22,040
 an offense.

218
00:15:22,040 --> 00:15:24,580
 If I touch food that hasn't been offered, apparently I

219
00:15:24,580 --> 00:15:26,040
 commit a very, very minor offense.

220
00:15:26,040 --> 00:15:30,220
 And I'm not allowed to eat that food, even if it's later

221
00:15:30,220 --> 00:15:33,040
 offered correctly.

222
00:15:33,040 --> 00:15:43,040
 That food is then disqualified if I even touch it.

223
00:15:43,040 --> 00:15:46,040
 Yeah, I mean, there's no, you can't find loopholes.

224
00:15:46,040 --> 00:15:53,040
 That's not the point.

225
00:15:53,040 --> 00:15:57,570
 It's kind of a, it's a bit of a border of gray area, having

226
00:15:57,570 --> 00:16:00,040
 someone offer it to you who isn't a donor.

227
00:16:00,040 --> 00:16:01,040
 But that's okay.

228
00:16:01,040 --> 00:16:04,040
 We consider, again, it's on behalf of the donor.

229
00:16:04,040 --> 00:16:08,100
 So if I go to a restaurant with the card and the cashier

230
00:16:08,100 --> 00:16:10,040
 hands me the sandwich,

231
00:16:10,040 --> 00:16:13,140
 we consider they're doing it on behalf still of the person

232
00:16:13,140 --> 00:16:14,040
 who donated.

233
00:16:14,040 --> 00:16:15,040
 And that's fine.

234
00:16:15,040 --> 00:16:18,680
 It's obviously not as awesome if, it's not as awesome as

235
00:16:18,680 --> 00:16:19,040
 someone,

236
00:16:19,040 --> 00:16:21,040
 "Hey, I want to give you the sandwich."

237
00:16:21,040 --> 00:16:31,040
 But on behalf is allowed, I think.

238
00:16:31,040 --> 00:16:36,040
 If food is on display, wouldn't it therefore be offered?

239
00:16:36,040 --> 00:16:37,040
 No.

240
00:16:37,040 --> 00:16:39,630
 No, there's a very technical explanation of what offered

241
00:16:39,630 --> 00:16:40,040
 means.

242
00:16:40,040 --> 00:16:45,040
 It has to be given, actually physically given.

243
00:16:45,040 --> 00:16:48,040
 Placed into the hands or something connected with the hands

244
00:16:48,040 --> 00:16:48,040
,

245
00:16:48,040 --> 00:16:51,040
 received by the hands or something connected, received by

246
00:16:51,040 --> 00:16:51,040
 the body

247
00:16:51,040 --> 00:16:52,040
 or something connected to the body.

248
00:16:52,040 --> 00:17:00,040
 So the bowl is allowed because you're holding it.

249
00:17:00,040 --> 00:17:02,590
 And the thing that it's connected to, the thing that's

250
00:17:02,590 --> 00:17:06,040
 connected to the body has to be liftable.

251
00:17:06,040 --> 00:17:10,040
 Like a table is potentially okay if you, right.

252
00:17:10,040 --> 00:17:13,370
 So if you offer someone a table, you can lift the table and

253
00:17:13,370 --> 00:17:15,040
 hand it to them.

254
00:17:15,040 --> 00:17:19,040
 But it has to actually be offered.

255
00:17:19,040 --> 00:17:30,040
 Like given, given, given.

256
00:17:30,040 --> 00:17:37,040
 On a plus note, I now have a picture of my teacher.

257
00:17:37,040 --> 00:17:40,040
 Conveniently credit card sized.

258
00:17:40,040 --> 00:17:43,870
 So I figured out how to transfer balance from one subway

259
00:17:43,870 --> 00:17:45,040
 card to another.

260
00:17:45,040 --> 00:17:48,040
 And so this is now just a picture of my teacher.

261
00:17:48,040 --> 00:17:50,040
 It's no longer a subway card.

262
00:17:50,040 --> 00:17:52,040
 And this one as well.

263
00:17:52,040 --> 00:17:55,040
 So thank you, people who sent me pictures.

264
00:17:55,040 --> 00:17:57,380
 Wouldn't recommend doing it again because it did have

265
00:17:57,380 --> 00:18:05,040
 subway on it, but yeah.

266
00:18:05,040 --> 00:18:08,040
 That was a good solution.

267
00:18:08,040 --> 00:18:10,040
 It worked out.

268
00:18:10,040 --> 00:18:15,040
 Yeah.

269
00:18:15,040 --> 00:18:17,040
 So no questions tonight.

270
00:18:17,040 --> 00:18:21,040
 One question.

271
00:18:21,040 --> 00:18:22,040
 All right.

272
00:18:22,040 --> 00:18:29,040
 Then I'm calling it a day because it's been a long one.

273
00:18:29,040 --> 00:18:30,040
 Good night.

274
00:18:30,040 --> 00:18:32,040
 Thank you, Bante.

275
00:18:32,040 --> 00:18:35,040
 And oh, and we have, I didn't, did I mention that we have

276
00:18:35,040 --> 00:18:36,040
 morning sessions now?

277
00:18:36,040 --> 00:18:40,040
 7 a.m. to 7 30, my seven a.m. and 7 30.

278
00:18:40,040 --> 00:18:42,760
 So if you're looking to do a meditation course and you have

279
00:18:42,760 --> 00:18:45,040
 to be committed,

280
00:18:45,040 --> 00:18:49,040
 go to the meet page.

281
00:18:49,040 --> 00:18:52,270
 Meditation dot Siri, mongolo dot org front slash

282
00:18:52,270 --> 00:18:54,040
 appointment dot PHP.

283
00:18:54,040 --> 00:18:57,040
 But there's the meet link in the top.

284
00:18:57,040 --> 00:18:58,040
 You'll see the schedule.

285
00:18:58,040 --> 00:19:00,040
 Oh, it's mostly full already.

286
00:19:00,040 --> 00:19:02,040
 But there's still some seven a.m.

287
00:19:02,040 --> 00:19:07,040
 slots.

288
00:19:07,040 --> 00:19:08,040
 11 a.m. UTC.

289
00:19:08,040 --> 00:19:09,040
 I'm sorry.

290
00:19:09,040 --> 00:19:14,560
 That's about the best I can do time wise because I'm doing

291
00:19:14,560 --> 00:19:16,040
 other things.

292
00:19:16,040 --> 00:19:18,040
 Oh, one thing we can try.

293
00:19:18,040 --> 00:19:20,320
 Let's see how many people we can get into the general

294
00:19:20,320 --> 00:19:22,040
 purpose group lounge.

295
00:19:22,040 --> 00:19:25,930
 So everybody go over to the meet page, the appointment page

296
00:19:25,930 --> 00:19:26,040
, right?

297
00:19:26,040 --> 00:19:28,040
 Click on the meet link.

298
00:19:28,040 --> 00:19:32,040
 You have to be logged in and then click on the green link

299
00:19:32,040 --> 00:19:33,040
 that says click here

300
00:19:33,040 --> 00:19:37,110
 to enter the general purpose group lounge and then give it

301
00:19:37,110 --> 00:19:38,040
 permission to use your

302
00:19:38,040 --> 00:19:39,040
 webcam and mic.

303
00:19:39,040 --> 00:19:42,040
 Hopefully some of you have webcams and mics.

304
00:19:42,040 --> 00:19:46,440
 And I'm going to end this hangout and I'm going to go in

305
00:19:46,440 --> 00:19:47,040
 there.

306
00:19:47,040 --> 00:19:49,040
 And Robin, if you can go in as well.

307
00:19:49,040 --> 00:19:51,040
 I'm already in.

308
00:19:51,040 --> 00:19:53,040
 As your webcam.

309
00:19:53,040 --> 00:19:55,650
 But it may not let you display because you're in the hang

310
00:19:55,650 --> 00:19:56,040
out.

311
00:19:56,040 --> 00:19:58,040
 It may not actually connect you.

312
00:19:58,040 --> 00:20:00,040
 It seemed to be in both.

313
00:20:00,040 --> 00:20:03,040
 It maybe works.

314
00:20:03,040 --> 00:20:04,040
 I don't know.

315
00:20:04,040 --> 00:20:05,040
 Anyway, okay.

316
00:20:05,040 --> 00:20:06,040
 I'm going to go over there.

317
00:20:06,040 --> 00:20:09,040
 I'm going to stop this broadcast.

318
00:20:09,040 --> 00:20:10,040
 Thank you, Bante.

319
00:20:10,040 --> 00:20:11,040
 Thank you, Robin.

320
00:20:11,040 --> 00:20:12,040
 Good night.

321
00:20:14,040 --> 00:20:15,040
 Thank you.

