1
00:00:00,000 --> 00:00:06,060
 Hello and welcome back to our study of Dhammapada. Today we

2
00:00:06,060 --> 00:00:11,000
 look at verse 173 which reads as follows.

3
00:00:11,000 --> 00:00:30,210
 Yasapapanga thangam mang kusadeenapadhyati, pidhyati, som

4
00:00:30,210 --> 00:00:34,700
anglokanbhabhasthi, abhamutavatandimah, which means whose

5
00:00:34,700 --> 00:00:36,000
 evil deeds,

6
00:00:36,000 --> 00:00:41,950
 whose karma, the evil deeds that they have done, is covered

7
00:00:41,950 --> 00:00:44,000
 over by wholesomeness.

8
00:00:44,000 --> 00:00:51,350
 Such a person lights up this world, illuminates this world,

9
00:00:51,350 --> 00:00:59,000
 just like the moon coming out from behind a cloud.

10
00:00:59,000 --> 00:01:05,860
 So it should sound very familiar, very similar to the last

11
00:01:05,860 --> 00:01:07,000
 verse.

12
00:01:07,000 --> 00:01:12,000
 We have sort of the opposite, two opposite lines here.

13
00:01:12,000 --> 00:01:17,510
 One covers over evil with wholesomeness, that's like the

14
00:01:17,510 --> 00:01:23,580
 moon when it comes out from behind the cloud, the uncover

15
00:01:23,580 --> 00:01:26,000
ing of the moon.

16
00:01:26,000 --> 00:01:33,850
 What it means is the change of a person. A person may have

17
00:01:33,850 --> 00:01:41,000
 done very bad deeds but when they do good, it's the change,

18
00:01:41,000 --> 00:01:45,000
 it's going from dark to light.

19
00:01:45,000 --> 00:01:51,080
 So the story here, it's a very famous story, it's a

20
00:01:51,080 --> 00:01:54,000
 reference to Angulimala.

21
00:01:54,000 --> 00:01:59,600
 I'm sure many of you are familiar with the name Angulimala,

22
00:01:59,600 --> 00:02:05,000
 it's a famous figure in Buddhism, Buddhist culture.

23
00:02:05,000 --> 00:02:13,000
 Angulimala killed many people, some sources say 999 people,

24
00:02:13,000 --> 00:02:19,140
 his goal was to kill a thousand, and his mother came to

25
00:02:19,140 --> 00:02:24,000
 find him and he was going to kill her.

26
00:02:24,000 --> 00:02:29,120
 And then the Buddha intervened. The story in the Majjhminik

27
00:02:29,120 --> 00:02:33,920
aya just says that the Buddha went to see him, he was going

28
00:02:33,920 --> 00:02:36,000
 to kill all these people,

29
00:02:36,000 --> 00:02:39,690
 he was killing all these people and he just wouldn't stop

30
00:02:39,690 --> 00:02:43,310
 and so the Buddha walked to see him and converted him and

31
00:02:43,310 --> 00:02:45,000
 he became a monk.

32
00:02:45,000 --> 00:02:54,940
 The real story is that such an evil, bloodthirsty, serial

33
00:02:54,940 --> 00:03:01,000
 killer bandit could change his ways.

34
00:03:01,000 --> 00:03:07,720
 But this verse is actually not so much about the story, it

35
00:03:07,720 --> 00:03:13,000
's about the fact that he became enlightened.

36
00:03:13,000 --> 00:03:16,340
 When he became enlightened he spoke verse 172 which is the

37
00:03:16,340 --> 00:03:19,810
 verse we looked at last week, this is what it says, "Yopube

38
00:03:19,810 --> 00:03:23,000
 pumantitwa pechasonopumantitthi."

39
00:03:23,000 --> 00:03:28,130
 That's the famous, the more famous verse I think, one who

40
00:03:28,130 --> 00:03:33,000
 is negligent before but is no longer negligent.

41
00:03:33,000 --> 00:03:42,820
 Such a one lights up the world. But 173 is more closely

42
00:03:42,820 --> 00:03:47,000
 related to the Angulimala Sutta.

43
00:03:47,000 --> 00:03:52,880
 As it refers to this shocking turn of events that Angulimal

44
00:03:52,880 --> 00:03:58,390
a is such an evil person could not just become a monk which

45
00:03:58,390 --> 00:04:03,260
 is remarkable in itself but could actually become an arah

46
00:04:03,260 --> 00:04:04,000
ant.

47
00:04:04,000 --> 00:04:06,330
 And so the monks didn't have any idea that this was

48
00:04:06,330 --> 00:04:10,750
 possible. They saw Angulimala ordained and of course that

49
00:04:10,750 --> 00:04:12,000
 was quite a remarkable thing.

50
00:04:12,000 --> 00:04:18,020
 Even the king was shocked to hear that Angulimala had

51
00:04:18,020 --> 00:04:20,000
 become a monk.

52
00:04:20,000 --> 00:04:22,760
 People wouldn't believe that anything good would come of it

53
00:04:22,760 --> 00:04:27,520
. They would attack him when he went for alms, beating him,

54
00:04:27,520 --> 00:04:32,000
 throwing rocks at him and sticks at him.

55
00:04:32,000 --> 00:04:35,410
 So the monk saw this and it was remarkable but no one ever

56
00:04:35,410 --> 00:04:39,410
 thought that he'd become anywhere near enlightened, not

57
00:04:39,410 --> 00:04:41,000
 after all the evil.

58
00:04:41,000 --> 00:04:44,920
 So they asked when he passed away, maybe he was killed, I

59
00:04:44,920 --> 00:04:46,000
 don't know.

60
00:04:46,000 --> 00:04:51,720
 I think the story says that he was beaten bloody but he

61
00:04:51,720 --> 00:04:58,000
 became enlightened and then passed away into, I don't know,

62
00:04:58,000 --> 00:05:01,000
 he became enlightened and he got beaten up.

63
00:05:01,000 --> 00:05:07,120
 And after being beaten up he spoke some verses and passed

64
00:05:07,120 --> 00:05:13,000
 away. So he may have been beaten so severely that he died.

65
00:05:13,000 --> 00:05:16,710
 The monks asked where he was reborn. They went to see the

66
00:05:16,710 --> 00:05:22,380
 Buddha and as was often their want, they asked the Buddha

67
00:05:22,380 --> 00:05:26,000
 where was Angulimala reborn?

68
00:05:27,000 --> 00:05:34,480
 And the Buddha said, "Oh, Angulimala, Parinibhutto, Chabhik

69
00:05:34,480 --> 00:05:42,760
ve, Mamaputto, my son has gone into Parinibhana, has become

70
00:05:42,760 --> 00:05:49,000
 completely free from suffering, no rebirth.

71
00:05:49,000 --> 00:05:55,780
 He's become unbound, unbound to Samsara, not having to be

72
00:05:55,780 --> 00:05:59,000
 reborn again and again."

73
00:05:59,000 --> 00:06:04,810
 And they said, "Having killed so many people, he could

74
00:06:04,810 --> 00:06:08,000
 possibly become unbound."

75
00:06:08,000 --> 00:06:20,260
 And they said, "Yes." And the Buddha said, "Yes." In the

76
00:06:20,260 --> 00:06:22,750
 past, not even one, and this is an important part of the

77
00:06:22,750 --> 00:06:25,360
 story, not even one good friend, he found not even one good

78
00:06:25,360 --> 00:06:26,000
 friend.

79
00:06:26,000 --> 00:06:32,050
 That's why he did so much evil. But after having gained a

80
00:06:32,050 --> 00:06:36,000
 good friend, he became diligent.

81
00:06:36,000 --> 00:06:43,760
 He lost his negligence and completely obliterated the evil

82
00:06:43,760 --> 00:06:46,000
 with the good.

83
00:06:46,000 --> 00:06:51,000
 And then he taught the verse.

84
00:06:51,000 --> 00:06:54,000
 So I think the story of Angulimala teaches us many things.

85
00:06:54,000 --> 00:06:56,000
 It's a long story, but we're not going to go into that

86
00:06:56,000 --> 00:07:01,000
 because this verse isn't so much about the story.

87
00:07:01,000 --> 00:07:07,080
 It teaches us, I think, mainly two things. First, that we

88
00:07:07,080 --> 00:07:12,900
 shouldn't be discouraged about our own state coming into

89
00:07:12,900 --> 00:07:14,000
 the practice.

90
00:07:14,000 --> 00:07:19,640
 Some people would think, "Oh, I'm too far gone. I've done

91
00:07:19,640 --> 00:07:23,020
 bad things. How could I possibly practice to become

92
00:07:23,020 --> 00:07:24,000
 enlightened?"

93
00:07:24,000 --> 00:07:30,220
 Some people would think, "Well, look at Angulimala." It's

94
00:07:30,220 --> 00:07:32,690
 true that in many cases a very bad person is not going to

95
00:07:32,690 --> 00:07:38,000
 get far in the practice. It's very difficult.

96
00:07:38,000 --> 00:07:45,200
 But on the other hand, it's not so much about your past, it

97
00:07:45,200 --> 00:07:52,710
's about your present. And doing lots of evil deeds is going

98
00:07:52,710 --> 00:07:56,000
 to have consequences.

99
00:07:56,000 --> 00:08:02,170
 The state of the mind is in the external consequences, not

100
00:08:02,170 --> 00:08:05,000
 in the state of the mind.

101
00:08:05,000 --> 00:08:08,780
 All you have to deal with, and it's really not that

102
00:08:08,780 --> 00:08:13,480
 complicated, you have to deal with the emotions in your own

103
00:08:13,480 --> 00:08:17,820
 mind, the state of your own mind, the chaos in your own

104
00:08:17,820 --> 00:08:19,000
 mind.

105
00:08:19,000 --> 00:08:21,970
 If you deal with that, well, the consequences don't end. It

106
00:08:21,970 --> 00:08:25,110
's possible you might still be arrested for your evil deeds.

107
00:08:25,110 --> 00:08:35,000
 It's possible you might be subject to revenge and so on.

108
00:08:35,000 --> 00:08:39,150
 But maybe that's another thing that it teaches us, is not

109
00:08:39,150 --> 00:08:43,000
 to be so concerned about external consequences.

110
00:08:43,000 --> 00:08:48,590
 We often mix the two, external and the internal. We become

111
00:08:48,590 --> 00:08:51,000
 discouraged by external consequences.

112
00:08:51,000 --> 00:08:53,640
 I do all these good things and bad things still happen to

113
00:08:53,640 --> 00:08:58,350
 me. And we're not so concerned about the external bad, or

114
00:08:58,350 --> 00:09:03,000
 concerned about happiness.

115
00:09:03,000 --> 00:09:06,420
 Because happiness doesn't lead to happiness. If your

116
00:09:06,420 --> 00:09:10,000
 concern is always with happiness and freedom from suffering

117
00:09:10,000 --> 00:09:10,000
,

118
00:09:10,000 --> 00:09:14,000
 you're never really going to be free from suffering because

119
00:09:14,000 --> 00:09:18,080
 such a state, such concerns don't actually make you happy

120
00:09:18,080 --> 00:09:20,000
 or free from suffering.

121
00:09:20,000 --> 00:09:24,250
 It's goodness. It's kusala. The Buddha uses the word here k

122
00:09:24,250 --> 00:09:26,000
usala in the kusala.

123
00:09:26,000 --> 00:09:30,870
 Wholesomeness, goodness. We should always be concerned with

124
00:09:30,870 --> 00:09:32,000
 goodness.

125
00:09:32,000 --> 00:09:41,680
 Best be full of goodness, there's no, there's no, hmm, hmm,

126
00:09:41,680 --> 00:09:46,000
 forget. Best be full of goodness.

127
00:09:50,000 --> 00:09:54,290
 But the other lesson, the other lesson that the Buddha

128
00:09:54,290 --> 00:09:59,010
 brings up here that I think is really the crux, the Buddha

129
00:09:59,010 --> 00:10:02,000
 really hits the heart of the matter,

130
00:10:02,000 --> 00:10:07,770
 is how easy it is to fall on the wrong path without a good

131
00:10:07,770 --> 00:10:09,000
 friend.

132
00:10:09,000 --> 00:10:13,100
 Of course the word good friend, Kali almita, in Buddhism it

133
00:10:13,100 --> 00:10:17,000
 refers to the teacher, it refers generally to the Buddha.

134
00:10:17,000 --> 00:10:21,640
 Because people did not meet the Buddha, or we can say more

135
00:10:21,640 --> 00:10:27,020
 generally with the Buddha's teaching, because of that they

136
00:10:27,020 --> 00:10:32,000
 fall into all sorts of evil.

137
00:10:32,000 --> 00:10:36,370
 When you don't have someone who can set you down the right

138
00:10:36,370 --> 00:10:40,770
 path, like now we have the Buddha and we still have the

139
00:10:40,770 --> 00:10:43,000
 Buddha through his teachings.

140
00:10:43,000 --> 00:10:49,000
 When you don't have that, sometimes we blame evil people.

141
00:10:49,000 --> 00:10:51,700
 We look at them and they say, "What a terrible evil person

142
00:10:51,700 --> 00:10:52,000
."

143
00:10:52,000 --> 00:10:55,490
 And sometimes, often times, in fact you could say all the

144
00:10:55,490 --> 00:11:02,000
 time, the only reason people do evil is through delusion.

145
00:11:02,000 --> 00:11:08,400
 It's not that anyone wants to be an evil person, or not

146
00:11:08,400 --> 00:11:15,520
 exactly, it's not that anyone purposefully and knowingly

147
00:11:15,520 --> 00:11:17,000
 wants evil,

148
00:11:17,000 --> 00:11:20,560
 it's that they think somehow that some good will come out

149
00:11:20,560 --> 00:11:23,000
 of being evil, doing evil things.

150
00:11:23,000 --> 00:11:27,440
 Often times there's not even a sense that it's evil.

151
00:11:27,440 --> 00:11:31,000
 Killing, for many people killing is a good thing.

152
00:11:31,000 --> 00:11:38,510
 Recently there was a politician in America who said, just

153
00:11:38,510 --> 00:11:41,000
 today, what did he say?

154
00:11:41,000 --> 00:11:45,890
 He said, "We should kill all the drug dealers." Something

155
00:11:45,890 --> 00:11:47,000
 like that.

156
00:11:52,000 --> 00:11:56,620
 For many people I think that resonates with them. He said,

157
00:11:56,620 --> 00:11:59,000
 "Yeah, yeah, kill bad people."

158
00:11:59,000 --> 00:12:04,720
 Wanting to shoot others. In America a lot of people have

159
00:12:04,720 --> 00:12:07,020
 guns and there are many people who don't really have a

160
00:12:07,020 --> 00:12:08,000
 problem with them.

161
00:12:08,000 --> 00:12:13,000
 There was one, I talked to one vet, a veteran of the war,

162
00:12:13,000 --> 00:12:15,000
 and he had a lot of mental issues.

163
00:12:15,000 --> 00:12:19,020
 He told me, he said someone asked him what he missed most

164
00:12:19,020 --> 00:12:21,000
 about being a sergeant.

165
00:12:21,000 --> 00:12:27,200
 He was a sergeant in some part of the armed forces. He said

166
00:12:27,200 --> 00:12:29,000
, "What do you miss most about it having retired?"

167
00:12:29,000 --> 00:12:36,090
 He said being able to kill people. These people exist in

168
00:12:36,090 --> 00:12:38,000
 the world.

169
00:12:40,000 --> 00:12:47,000
 Now the varying degrees of delusion, but it's all delusion.

170
00:12:47,000 --> 00:12:52,000
 People who think that stealing is not wrong.

171
00:12:52,000 --> 00:12:57,030
 This story about Mark Zuckerberg, this head of Facebook, I

172
00:12:57,030 --> 00:12:58,000
 don't know if I should mention names,

173
00:12:58,000 --> 00:13:03,880
 but this guy, very famous guy, who said he decided to start

174
00:13:03,880 --> 00:13:08,000
 killing, very influential because of how famous he is,

175
00:13:08,000 --> 00:13:12,190
 decided to start killing his own animals. So he went and he

176
00:13:12,190 --> 00:13:14,000
 killed a goat on his own.

177
00:13:14,000 --> 00:13:17,590
 Because he thought that was better, that was the best thing

178
00:13:17,590 --> 00:13:20,000
 to do if you're going to eat the meat.

179
00:13:20,000 --> 00:13:23,000
 He couldn't have just become a vegetarian, right?

180
00:13:23,000 --> 00:13:28,590
 If you feel that strongly about it, why is it better

181
00:13:28,590 --> 00:13:32,000
 somehow to kill it yourself?

182
00:13:32,000 --> 00:13:35,800
 A lot of delusion. Oh, we could go on and on about the

183
00:13:35,800 --> 00:13:37,000
 types of delusion.

184
00:13:37,000 --> 00:13:42,400
 Just the five precepts, you know. I always remark, if I had

185
00:13:42,400 --> 00:13:48,000
 some inkling of killing being wrong,

186
00:13:48,000 --> 00:13:53,370
 or that drugs and alcohol didn't open your mind or make you

187
00:13:53,370 --> 00:13:58,000
 a happier, more fun-loving person,

188
00:13:58,000 --> 00:14:07,580
 but in fact may dull your mind and led to greater emotional

189
00:14:07,580 --> 00:14:09,000
 turmoil.

190
00:14:09,000 --> 00:14:15,860
 If someone had said drugs and alcohol, that's fundamentally

191
00:14:15,860 --> 00:14:20,000
 wrong for humans to engage in.

192
00:14:20,000 --> 00:14:23,050
 Maybe not, but it seems to me that if there had been more

193
00:14:23,050 --> 00:14:27,740
 instruction, I might have avoided much of the suffering in

194
00:14:27,740 --> 00:14:28,000
 my life,

195
00:14:28,000 --> 00:14:37,000
 much of the evil.

196
00:14:37,000 --> 00:14:40,380
 As for what the verse teaches us, it has a bit of a

197
00:14:40,380 --> 00:14:42,000
 different lesson.

198
00:14:42,000 --> 00:14:46,850
 The verse itself is talking about the possibility of

199
00:14:46,850 --> 00:14:50,000
 mitigating evil through good.

200
00:14:50,000 --> 00:14:56,670
 That's a good question. How does someone become pure when

201
00:14:56,670 --> 00:15:02,030
 their mind is so full of evil, when they've done so much

202
00:15:02,030 --> 00:15:03,000
 evil?

203
00:15:03,000 --> 00:15:08,460
 We talk about different kinds of karma, and the Buddha

204
00:15:08,460 --> 00:15:12,000
 reaffirms that this is the case.

205
00:15:12,000 --> 00:15:17,620
 I think if you look both at external circumstances, how our

206
00:15:17,620 --> 00:15:23,810
 deeds and our acts and our mindset affects the world around

207
00:15:23,810 --> 00:15:24,000
 us,

208
00:15:24,000 --> 00:15:29,630
 and how our actions affect our own minds, you can see that

209
00:15:29,630 --> 00:15:37,330
 it's more complicated than just do good, get good, do evil,

210
00:15:37,330 --> 00:15:39,000
 get evil.

211
00:15:39,000 --> 00:15:44,050
 That simply put is how karma works, but the more complex

212
00:15:44,050 --> 00:15:49,000
 formula is that there are four types of karma.

213
00:15:49,000 --> 00:15:55,770
 There's karma that creates results, good or bad. If you do

214
00:15:55,770 --> 00:15:58,000
 a good deed, something good comes from it.

215
00:15:58,000 --> 00:16:02,710
 Internally you become a better person, and externally

216
00:16:02,710 --> 00:16:05,000
 people appreciate you.

217
00:16:05,000 --> 00:16:09,340
 If you do a bad deed, likewise. But there are other types

218
00:16:09,340 --> 00:16:13,000
 of karma. Some karma is just supportive.

219
00:16:13,000 --> 00:16:17,950
 Some aspects of, some deeds that you do, sometimes you do a

220
00:16:17,950 --> 00:16:24,000
 good deed, and it supports, so you do one good deed,

221
00:16:24,000 --> 00:16:27,120
 and then you do another good deed, and people recognize the

222
00:16:27,120 --> 00:16:29,000
 first one because of the second one,

223
00:16:29,000 --> 00:16:34,490
 or the first one bears fruit, even mentally, because of the

224
00:16:34,490 --> 00:16:37,000
 power of the second one.

225
00:16:37,000 --> 00:16:42,790
 It's repeated actions affect each other. So supportive

226
00:16:42,790 --> 00:16:44,000
 karma is the thing.

227
00:16:44,000 --> 00:16:49,530
 And then there's reductive karma, karma that reduces the

228
00:16:49,530 --> 00:16:52,000
 effect of other karma.

229
00:16:52,000 --> 00:16:58,530
 So you do something good, and something good comes, maybe

230
00:16:58,530 --> 00:17:03,000
 you're very nice to people and they appreciate you,

231
00:17:03,000 --> 00:17:08,000
 but then you do something bad, and your reputation is

232
00:17:08,000 --> 00:17:11,000
 reduced, even though you did, maybe you could be,

233
00:17:11,000 --> 00:17:15,470
 sometimes someone does very good deeds all their life, and

234
00:17:15,470 --> 00:17:24,000
 then one bad mistake, and their reputation is ruined.

235
00:17:24,000 --> 00:17:28,670
 And the fourth is nullifying karma, karma that destroys

236
00:17:28,670 --> 00:17:30,000
 other karma.

237
00:17:30,000 --> 00:17:35,270
 And that's what he's talking about here, that, especially

238
00:17:35,270 --> 00:17:46,000
 internally, there's so much you can do to change your mind,

239
00:17:46,000 --> 00:17:50,520
 change the landscape of your mind, all these habits that we

240
00:17:50,520 --> 00:17:59,000
 have, all these qualities that make up who we are.

241
00:17:59,000 --> 00:18:04,740
 There's much we can do to change that. I mean, meditation

242
00:18:04,740 --> 00:18:08,000
 is the prime example.

243
00:18:08,000 --> 00:18:14,330
 It's quite shocking for people who are familiar with, for

244
00:18:14,330 --> 00:18:18,220
 friends, for relatives, of someone who goes to do a

245
00:18:18,220 --> 00:18:20,000
 meditation course,

246
00:18:20,000 --> 00:18:22,330
 because when they come back they can be quite a changed

247
00:18:22,330 --> 00:18:25,000
 individual, and many things about them are different,

248
00:18:25,000 --> 00:18:31,000
 and quite shocking.

249
00:18:31,000 --> 00:18:34,900
 And all those years of building up habits have suddenly

250
00:18:34,900 --> 00:18:40,500
 been erased, or reduced, usually not erased, but some bad

251
00:18:40,500 --> 00:18:45,000
 habits are erased.

252
00:18:45,000 --> 00:18:48,000
 But throughout the meditation course, this is a big part of

253
00:18:48,000 --> 00:18:52,740
 what we're doing, it's a good explanation of the work that

254
00:18:52,740 --> 00:18:54,000
 we do in meditation.

255
00:18:54,000 --> 00:19:02,000
 We work on nullifying our bad habits.

256
00:19:02,000 --> 00:19:07,180
 I mean, nullifying really is not about covering up, which

257
00:19:07,180 --> 00:19:09,990
 is the word that they use, and the Buddha used in the verse

258
00:19:09,990 --> 00:19:10,000
,

259
00:19:10,000 --> 00:19:19,250
 but it's about smothering them, really, extinguishing them,

260
00:19:19,250 --> 00:19:25,000
 I guess, is the best way to explain it.

261
00:19:25,000 --> 00:19:32,150
 Simply by gaining the clarity of mind that was missing, you

262
00:19:32,150 --> 00:19:36,490
 light up the world, this illuminating imagery of lighting

263
00:19:36,490 --> 00:19:37,000
 up the world,

264
00:19:37,000 --> 00:19:41,000
 like the moon coming up from behind the clouds is apt,

265
00:19:41,000 --> 00:19:45,550
 because you realize that all the evil that you did was just

266
00:19:45,550 --> 00:19:48,000
 because of ignorance,

267
00:19:48,000 --> 00:19:52,230
 that you were totally blind, that you didn't have a good

268
00:19:52,230 --> 00:19:56,000
 reason for doing any of the bad things that you did,

269
00:19:56,000 --> 00:20:00,000
 and simply didn't know any better.

270
00:20:00,000 --> 00:20:04,480
 So you knew wrong, you know, we're given all these wrong

271
00:20:04,480 --> 00:20:08,220
 views by all sorts of sources, what our parents do, what

272
00:20:08,220 --> 00:20:09,000
 our friends do,

273
00:20:09,000 --> 00:20:14,000
 what our society tells us, what our religions tell us,

274
00:20:14,000 --> 00:20:18,000
 often religions have bad ideas about killing and stealing,

275
00:20:18,000 --> 00:20:27,350
 well, usually not stealing, but many religions condone

276
00:20:27,350 --> 00:20:32,000
 various forms of violence,

277
00:20:32,000 --> 00:20:36,000
 I mean, I think violence towards animals is an obvious one.

278
00:20:36,000 --> 00:20:40,350
 What many religions would be against killing humans, but

279
00:20:40,350 --> 00:20:44,270
 then have no problems, or even be in favor of killing

280
00:20:44,270 --> 00:20:52,000
 animals, and often in fairly inhumane ways.

281
00:20:52,000 --> 00:20:57,000
 So many sources of wrong view.

282
00:20:57,000 --> 00:21:01,840
 And many of us, I think, this resonates this idea that if

283
00:21:01,840 --> 00:21:05,540
 only I'd had a good friend, and we feel good now, that we

284
00:21:05,540 --> 00:21:07,000
 feel blessed now,

285
00:21:07,000 --> 00:21:14,210
 we feel we've found a refuge, now that we've found the good

286
00:21:14,210 --> 00:21:24,000
 friend and the Buddha, so we can light up the world.

287
00:21:24,000 --> 00:21:27,550
 So that's the Dhammapada for tonight. Thank you all for

288
00:21:27,550 --> 00:21:29,000
 tuning in. Have a good night.

