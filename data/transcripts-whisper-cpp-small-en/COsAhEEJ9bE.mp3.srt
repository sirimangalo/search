1
00:00:00,000 --> 00:00:05,720
 Okay, good evening everyone.

2
00:00:05,720 --> 00:00:11,520
 Welcome to our evening in Dhamma.

3
00:00:11,520 --> 00:00:18,610
 And Happy New Year to those who celebrate the Chinese New

4
00:00:18,610 --> 00:00:20,720
 Year.

5
00:00:20,720 --> 00:00:23,920
 I don't personally celebrate the Chinese New Year.

6
00:00:23,920 --> 00:00:30,270
 It's not as Buddhist I don't think we really have a new

7
00:00:30,270 --> 00:00:31,360
 year.

8
00:00:31,360 --> 00:00:38,750
 But Chinese New Year's in February, Thai, Sri Lankan, Laot

9
00:00:38,750 --> 00:00:41,840
ian, Cambodian, Burmese New

10
00:00:41,840 --> 00:00:46,160
 Year are all in and around April.

11
00:00:46,160 --> 00:01:00,720
 Western New Year of course is about a month ago.

12
00:01:00,720 --> 00:01:15,840
 But I, Jan Dong, had this to say about the New Year.

13
00:01:15,840 --> 00:01:21,810
 He said, "May it have more to do, may it have more meaning

14
00:01:21,810 --> 00:01:23,720
 than simply in regards to the

15
00:01:23,720 --> 00:01:26,760
 year."

16
00:01:26,760 --> 00:01:29,280
 Because there's nothing new.

17
00:01:29,280 --> 00:01:36,360
 There's nothing new about this year.

18
00:01:36,360 --> 00:01:40,440
 Seasons come and seasons go year in year out.

19
00:01:40,440 --> 00:01:44,240
 It's a cycle.

20
00:01:44,240 --> 00:01:48,230
 The more things change the more they stay the same,

21
00:01:48,230 --> 00:01:50,720
 whatever that really means.

22
00:01:50,720 --> 00:01:54,390
 I mean, I'd rather suppose say things may change but they

23
00:01:54,390 --> 00:01:56,200
 still just stay the same,

24
00:01:56,200 --> 00:02:04,160
 they really just stay the same.

25
00:02:04,160 --> 00:02:14,600
 All these cliches, it's all been done before.

26
00:02:14,600 --> 00:02:19,400
 But we have always the opportunity to make something new.

27
00:02:19,400 --> 00:02:27,290
 This is the great potential of now, the potential to go in

28
00:02:27,290 --> 00:02:32,320
 any direction, to steer ourselves

29
00:02:32,320 --> 00:02:33,320
 in any direction.

30
00:02:33,320 --> 00:02:35,960
 And so things do change.

31
00:02:35,960 --> 00:02:44,010
 This year does have many things that are different from

32
00:02:44,010 --> 00:02:47,920
 last year for many of us.

33
00:02:47,920 --> 00:02:50,670
 But his point was, let us not get caught up in our old

34
00:02:50,670 --> 00:02:51,360
 habits.

35
00:02:51,360 --> 00:03:12,700
 Let us leave behind the rut, the cycle, the loops that we

36
00:03:12,700 --> 00:03:15,360
 get caught up in that cause

37
00:03:15,360 --> 00:03:17,600
 of suffering again and again.

38
00:03:17,600 --> 00:03:21,130
 Let us not say, "Oh, another year for us to do the same

39
00:03:21,130 --> 00:03:22,840
 thing, a new year for us to

40
00:03:22,840 --> 00:03:30,400
 make a mess of things once more, for us to suffer."

41
00:03:30,400 --> 00:03:35,350
 So reflecting back on a year is a useful tool to reflect

42
00:03:35,350 --> 00:03:38,160
 and to think how that year went

43
00:03:38,160 --> 00:03:44,760
 and to look forward and think of what we can do to better

44
00:03:44,760 --> 00:03:46,600
 ourselves.

45
00:03:46,600 --> 00:03:49,950
 That which was good about last year, let us keep it and

46
00:03:49,950 --> 00:03:51,200
 improve upon it.

47
00:03:51,200 --> 00:03:56,730
 That which was problematic about the last year, let us

48
00:03:56,730 --> 00:03:57,880
 change.

49
00:03:57,880 --> 00:04:00,080
 Let us find something new.

50
00:04:00,080 --> 00:04:04,430
 And the other aspect of new is that no matter what good or

51
00:04:04,430 --> 00:04:06,960
 bad comes, good or bad karma,

52
00:04:06,960 --> 00:04:09,880
 it's all still in the cycle.

53
00:04:09,880 --> 00:04:15,670
 The only thing that is truly new and is truly new is

54
00:04:15,670 --> 00:04:20,140
 freedom, freedom from the cycle.

55
00:04:20,140 --> 00:04:22,930
 Because if we'd had freedom already, we wouldn't be coming

56
00:04:22,930 --> 00:04:26,720
 back to the cycle again and again.

57
00:04:26,720 --> 00:04:29,600
 So we strive to find what is truly new and that is the

58
00:04:29,600 --> 00:04:33,280
 freedom from the cycle of samsara,

59
00:04:33,280 --> 00:04:35,870
 freedom from suffering, freedom from our bad habits,

60
00:04:35,870 --> 00:04:37,640
 freedom from our attachments, freedom

61
00:04:37,640 --> 00:04:50,910
 from those things, those parts of ourselves that we'd

62
00:04:50,910 --> 00:04:56,480
 better off without.

63
00:04:56,480 --> 00:05:00,200
 Let us make something new.

64
00:05:00,200 --> 00:05:05,340
 But I think an accusation could be leveled that for most of

65
00:05:05,340 --> 00:05:07,880
 us we aren't actually interested

66
00:05:07,880 --> 00:05:15,000
 in the day or the year or any of that.

67
00:05:15,000 --> 00:05:19,240
 In fact I would accuse us of, it's not a bad accusation, it

68
00:05:19,240 --> 00:05:21,360
's just a mild accusation that

69
00:05:21,360 --> 00:05:26,600
 we don't care so much about the holidays.

70
00:05:26,600 --> 00:05:32,200
 We don't care so much about our holidays themselves.

71
00:05:32,200 --> 00:05:40,730
 We care about them as an opportunity for celebration, for

72
00:05:40,730 --> 00:05:44,080
 recognition, not for recognition, for

73
00:05:44,080 --> 00:05:52,780
 community, for the cultivation of certain activities,

74
00:05:52,780 --> 00:05:55,880
 qualities, etc.

75
00:05:55,880 --> 00:05:59,080
 Unfortunately a lot of the times the qualities that we are

76
00:05:59,080 --> 00:06:01,000
 most interested in cultivating

77
00:06:01,000 --> 00:06:02,880
 are the bad ones.

78
00:06:02,880 --> 00:06:06,510
 So New Year's is a time of debauchery, what we call

79
00:06:06,510 --> 00:06:09,160
 celebration, well it turns out to

80
00:06:09,160 --> 00:06:21,800
 be a great big, hedonistic pleasure fest.

81
00:06:21,800 --> 00:06:23,840
 That ends up with a lot of suffering.

82
00:06:23,840 --> 00:06:27,940
 It's one of the most dangerous days of the year, the

83
00:06:27,940 --> 00:06:29,840
 western New Year's.

84
00:06:29,840 --> 00:06:41,470
 It's a time when there's a lot of drunk driving, a lot of

85
00:06:41,470 --> 00:06:44,600
 accidents.

86
00:06:44,600 --> 00:06:48,960
 But holidays in general are a time for us more than they

87
00:06:48,960 --> 00:06:50,480
 are for the day.

88
00:06:50,480 --> 00:06:54,520
 Being the day seems kind of weird, no?

89
00:06:54,520 --> 00:06:57,040
 The day certainly doesn't care.

90
00:06:57,040 --> 00:07:01,440
 Usually the person that we're commemorating doesn't care.

91
00:07:01,440 --> 00:07:05,060
 Martin Luther King, I'm pretty sure it's not such a big

92
00:07:05,060 --> 00:07:06,640
 deal to him anymore.

93
00:07:06,640 --> 00:07:09,200
 But it's for us, right?

94
00:07:09,200 --> 00:07:12,560
 It's for us to remember.

95
00:07:12,560 --> 00:07:20,240
 In fact, often just for us to have a reason to gather and

96
00:07:20,240 --> 00:07:24,200
 to cultivate whatever it is

97
00:07:24,200 --> 00:07:27,680
 that we agree upon.

98
00:07:27,680 --> 00:07:30,480
 So as Buddhists, I think you know where this is leading.

99
00:07:30,480 --> 00:07:34,480
 A holiday has much more to do with us being holy than

100
00:07:34,480 --> 00:07:37,280
 worrying about whether the day is

101
00:07:37,280 --> 00:07:38,280
 holy or not.

102
00:07:38,280 --> 00:07:43,480
 In fact, the Buddha was rather critical, or not critical,

103
00:07:43,480 --> 00:07:46,120
 but pointed in this regard.

104
00:07:46,120 --> 00:07:50,120
 He said any day.

105
00:07:50,120 --> 00:07:51,120
 There's nakata.

106
00:07:51,120 --> 00:07:55,690
 Nakata is, I don't know how the word, nakata is a star, I

107
00:07:55,690 --> 00:07:56,560
 think.

108
00:07:56,560 --> 00:07:59,960
 It has to do with the star's astrology or something.

109
00:07:59,960 --> 00:08:06,160
 Nowadays in Buddhist cultures it refers to astrology,

110
00:08:06,160 --> 00:08:13,520
 fortune telling, telling your horoscopes.

111
00:08:13,520 --> 00:08:21,680
 What the Buddha said, a good star, a lucky star.

112
00:08:21,680 --> 00:08:30,000
 By star means lucky conjoining of the stars.

113
00:08:30,000 --> 00:08:31,880
 Such a thing.

114
00:08:31,880 --> 00:08:35,800
 And a lucky day, a lucky moment.

115
00:08:35,800 --> 00:08:37,800
 What does it mean, a good moment?

116
00:08:37,800 --> 00:08:46,670
 Sunakatang, Sumangalang, a good blessing, auspiciousness, o

117
00:08:46,670 --> 00:08:49,440
men, a good omen.

118
00:08:49,440 --> 00:08:53,360
 Sumo, hos, ahs, never get this right.

119
00:08:53,360 --> 00:08:56,320
 Anyway, a lucky moment, a good moment.

120
00:08:56,320 --> 00:09:10,400
 Supa, pa, tangsu, hoti, tangsu, kano, a good moment.

121
00:09:10,400 --> 00:09:17,640
 Is when one is holy, when one cultivates holiness.

122
00:09:17,640 --> 00:09:26,120
 So you remember the Aditya, the Padikarata sutta.

123
00:09:26,120 --> 00:09:30,230
 Those of you who have heard this, of course, it's a

124
00:09:30,230 --> 00:09:33,760
 memorable sutta in the Majjhima Nikaya.

125
00:09:33,760 --> 00:09:36,040
 It occurs several times and repeatedly.

126
00:09:36,040 --> 00:09:39,670
 They were gathered together because this was a sutta that

127
00:09:39,670 --> 00:09:42,080
 the Buddha and many of his disciples

128
00:09:42,080 --> 00:09:44,920
 would repeat.

129
00:09:44,920 --> 00:09:50,080
 And so you have it repeatedly in the Kainan.

130
00:09:50,080 --> 00:09:55,510
 This is where the Buddha says, "Atit anan wakame yan apatik

131
00:09:55,510 --> 00:10:00,080
anke anagatang."

132
00:10:00,080 --> 00:10:04,810
 One should not go back to the past or bring up the past or

133
00:10:04,810 --> 00:10:07,160
 worry about the future.

134
00:10:07,160 --> 00:10:11,700
 "Yadatit an pahinantang" what's in the past is gone already

135
00:10:11,700 --> 00:10:12,080
.

136
00:10:12,080 --> 00:10:14,800
 "Appatancha anagatang."

137
00:10:14,800 --> 00:10:17,560
 What's in the future has not yet come.

138
00:10:17,560 --> 00:10:22,440
 "Pachupanan jayodamang tata tata vipassati."

139
00:10:22,440 --> 00:10:27,220
 Whatever arises in front of you, "pachupana" means present

140
00:10:27,220 --> 00:10:29,880
 but literally means what arises

141
00:10:29,880 --> 00:10:32,560
 in front of your face.

142
00:10:32,560 --> 00:10:35,000
 Right in front of you.

143
00:10:35,000 --> 00:10:38,960
 "Tata vipassati."

144
00:10:38,960 --> 00:10:40,400
 See all of that clearly.

145
00:10:40,400 --> 00:10:50,200
 "Vipassati" is where we get the word "vipassana."

146
00:10:50,200 --> 00:10:55,720
 The Buddha talks about this as how you have a good day, how

147
00:10:55,720 --> 00:10:58,480
 you have an auspicious day,

148
00:10:58,480 --> 00:11:01,240
 a special day, a holy day really.

149
00:11:01,240 --> 00:11:04,880
 So he's really talking about a holiday.

150
00:11:04,880 --> 00:11:10,680
 In Buddhism we have holidays, many days that mark the

151
00:11:10,680 --> 00:11:15,480
 procession of the year and the moon,

152
00:11:15,480 --> 00:11:20,690
 every full moon is considered a holy day but just following

153
00:11:20,690 --> 00:11:22,520
 Hinduism really.

154
00:11:22,520 --> 00:11:28,040
 The question was with the holy days of Hinduism they have

155
00:11:28,040 --> 00:11:32,960
 all these various rituals and activities.

156
00:11:32,960 --> 00:11:36,780
 In Indian society these former Hindus were asking the

157
00:11:36,780 --> 00:11:38,720
 Buddha, "What do we do?

158
00:11:38,720 --> 00:11:40,560
 What do we do as Buddhists on the full moon?"

159
00:11:40,560 --> 00:11:45,780
 The Buddha said, "Well, the tradition of the noble ones is

160
00:11:45,780 --> 00:11:48,160
 that on a holiday you would

161
00:11:48,160 --> 00:11:53,960
 keep strict meditation practice."

162
00:11:53,960 --> 00:11:58,360
 Meaning you wouldn't have sex or romantic activity.

163
00:11:58,360 --> 00:12:03,200
 You would only eat in the morning and you wouldn't engage

164
00:12:03,200 --> 00:12:06,480
 in entertainment or beautification

165
00:12:06,480 --> 00:12:09,440
 and you would sleep on the floor.

166
00:12:09,440 --> 00:12:13,980
 You would live your life as a monastic or as a recluse, as

167
00:12:13,980 --> 00:12:16,440
 a renunciant really for that

168
00:12:16,440 --> 00:12:18,760
 day.

169
00:12:18,760 --> 00:12:21,920
 So in effect you would become a holy person for that day.

170
00:12:21,920 --> 00:12:22,920
 This is the idea.

171
00:12:22,920 --> 00:12:27,370
 Which is so much better than anything else you could do,

172
00:12:27,370 --> 00:12:29,720
 giving gifts to the Buddha,

173
00:12:29,720 --> 00:12:31,920
 offering flowers to the Buddha.

174
00:12:31,920 --> 00:12:35,680
 The Buddha himself said, "This isn't the way you honor a

175
00:12:35,680 --> 00:12:36,440
 Buddha.

176
00:12:36,440 --> 00:12:44,560
 This isn't the way you have a holiday."

177
00:12:44,560 --> 00:12:51,210
 So we're much better looking at the Buddha's teaching,

178
00:12:51,210 --> 00:12:55,360
 using this pattern for keeping ethical

179
00:12:55,360 --> 00:13:00,570
 precepts and meditator precepts really, and taking on ren

180
00:13:00,570 --> 00:13:03,160
unciant precepts for the day,

181
00:13:03,160 --> 00:13:05,150
 and practicing the Buddha's teaching, being in the present

182
00:13:05,150 --> 00:13:05,560
 moment.

183
00:13:05,560 --> 00:13:10,250
 We take these two together, this idea of the keeping of the

184
00:13:10,250 --> 00:13:13,320
 eight precepts for meditators,

185
00:13:13,320 --> 00:13:20,110
 along with the idea of practicing insight meditation in the

186
00:13:20,110 --> 00:13:22,360
 present moment.

187
00:13:22,360 --> 00:13:25,900
 It makes it a holy day and could make any day really a holy

188
00:13:25,900 --> 00:13:26,480
 day.

189
00:13:26,480 --> 00:13:34,290
 But it's a great excuse and a great means of cultivating

190
00:13:34,290 --> 00:13:40,280
 energy, confidence, psychologically

191
00:13:40,280 --> 00:13:44,800
 having a day, having a special day, where you can keep in

192
00:13:44,800 --> 00:13:47,440
 mind, for instance, today is the

193
00:13:47,440 --> 00:13:48,440
 new year.

194
00:13:48,440 --> 00:13:52,990
 So keeping that in mind psychologically we have this idea

195
00:13:52,990 --> 00:13:55,080
 that, "Okay, this is a good

196
00:13:55,080 --> 00:13:57,640
 excuse to start."

197
00:13:57,640 --> 00:14:02,440
 And so on the new year now we make something new, leave the

198
00:14:02,440 --> 00:14:03,860
 past behind.

199
00:14:03,860 --> 00:14:05,520
 This is what the new year really is supposed to be.

200
00:14:05,520 --> 00:14:10,620
 It's psychologically a tool for us to say to ourselves, "

201
00:14:10,620 --> 00:14:12,520
Let the old be old.

202
00:14:12,520 --> 00:14:13,520
 Let's bring in the new.

203
00:14:13,520 --> 00:14:14,520
 Do something new."

204
00:14:14,520 --> 00:14:37,880
 It's a great way to motivate us towards self-cultivation.

205
00:14:37,880 --> 00:14:43,520
 The Buddha was very much about the here and now, any moment

206
00:14:43,520 --> 00:14:47,240
, not make excuses, not necessarily

207
00:14:47,240 --> 00:14:51,560
 to wait for holidays to do good things.

208
00:14:51,560 --> 00:14:55,230
 He said that this makes us invincible, staying in the

209
00:14:55,230 --> 00:14:58,020
 present moment, letting go of the past

210
00:14:58,020 --> 00:14:59,760
 and the future.

211
00:14:59,760 --> 00:15:19,920
 Being here and now, this is unassailable.

212
00:15:19,920 --> 00:15:23,480
 He said, "You have to do it today."

213
00:15:23,480 --> 00:15:30,200
 We shouldn't wait for holidays.

214
00:15:30,200 --> 00:15:42,080
 Who knows whether death will come tomorrow?

215
00:15:42,080 --> 00:15:43,080
 So we work now.

216
00:15:43,080 --> 00:15:46,720
 We work to better ourselves.

217
00:15:46,720 --> 00:15:57,900
 Trying at all times to free ourselves, to find happiness,

218
00:15:57,900 --> 00:16:00,080
 doing what's in our own best

219
00:16:00,080 --> 00:16:06,400
 interest, making ourselves holy, becoming better people.

220
00:16:06,400 --> 00:16:12,780
 It's always nice to have this time to take a break, to step

221
00:16:12,780 --> 00:16:16,360
 back and to say, "Here,

222
00:16:16,360 --> 00:16:18,720
 this is where I'm headed in the new year.

223
00:16:18,720 --> 00:16:22,980
 This is for the next 360 some days.

224
00:16:22,980 --> 00:16:26,200
 This is the direction I'm going to head."

225
00:16:26,200 --> 00:16:35,520
 Gives us a starting point.

226
00:16:35,520 --> 00:16:39,360
 So I'd like to wish you all a Happy New Year and I wish for

227
00:16:39,360 --> 00:16:41,400
 you all to find holiness for

228
00:16:41,400 --> 00:16:47,400
 yourselves, to find the present moment and to learn how to

229
00:16:47,400 --> 00:16:51,320
 always live in holiness, to

230
00:16:51,320 --> 00:16:56,360
 have a holy day every day.

231
00:16:56,360 --> 00:16:59,610
 There's my, I know rather short but I don't have a lot to

232
00:16:59,610 --> 00:17:02,200
 say and I'm here every night,

233
00:17:02,200 --> 00:17:04,520
 almost every night.

234
00:17:04,520 --> 00:17:07,000
 So there's the Dhamma for this evening.

235
00:17:07,000 --> 00:17:07,880
 Wishing you all the best.

