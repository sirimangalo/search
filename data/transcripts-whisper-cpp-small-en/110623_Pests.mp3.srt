1
00:00:00,000 --> 00:00:04,520
 Hello and welcome back to Ask a Monk. Today I will be

2
00:00:04,520 --> 00:00:06,200
 answering the question as

3
00:00:06,200 --> 00:00:14,740
 to what should we do when confronted by situations where we

4
00:00:14,740 --> 00:00:17,600
 feel necessary to

5
00:00:17,600 --> 00:00:24,510
 perform violent acts, specifically in terms of killing

6
00:00:24,510 --> 00:00:25,640
 would we deem to be

7
00:00:25,640 --> 00:00:33,890
 pests, those sentient beings that are causing suffering for

8
00:00:33,890 --> 00:00:36,120
 ourselves or those

9
00:00:36,120 --> 00:00:43,840
 around us or are confronting us with a situation where we

10
00:00:43,840 --> 00:00:45,800
 were put in in some

11
00:00:45,800 --> 00:00:53,240
 great physical difficulty. So the example that was given on

12
00:00:53,240 --> 00:00:55,240
 the ask.surimongalow.org

13
00:00:55,240 --> 00:01:00,580
 was about rats and in this case it's not even a physical

14
00:01:00,580 --> 00:01:01,880
 difficulty that they're

15
00:01:01,880 --> 00:01:10,520
 giving. It's a problem with the landlord. So the landlord

16
00:01:10,520 --> 00:01:10,960
 has given

17
00:01:10,960 --> 00:01:17,120
 an ultimatum that, not an ultimatum, the requirement that

18
00:01:17,120 --> 00:01:18,440
 the rats be taken care

19
00:01:18,440 --> 00:01:27,560
 of. And so the question is what to do at this point. So

20
00:01:27,560 --> 00:01:29,680
 first of all,

21
00:01:29,680 --> 00:01:39,430
 this question, really the core issue here, not in regards

22
00:01:39,430 --> 00:01:40,040
 to specific

23
00:01:40,040 --> 00:01:48,390
 instances, but the core theory that we have to get through

24
00:01:48,390 --> 00:01:50,840
 or come to

25
00:01:50,840 --> 00:01:56,270
 understand is the difference between physical well-being

26
00:01:56,270 --> 00:01:57,520
 and mental well-being.

27
00:01:57,520 --> 00:02:11,440
 And so the problem is that we quite often think more of our

28
00:02:11,440 --> 00:02:12,760
 physical well-being

29
00:02:12,760 --> 00:02:17,530
 than of our mental well-being. And we'll often place our

30
00:02:17,530 --> 00:02:18,520
 physical well-being

31
00:02:18,520 --> 00:02:21,770
 ahead of our mental well-being, not realizing that this is

32
00:02:21,770 --> 00:02:22,440
 actually the

33
00:02:22,440 --> 00:02:27,990
 choice we're making and that by performing violent acts

34
00:02:27,990 --> 00:02:28,760
 towards other

35
00:02:28,760 --> 00:02:35,310
 beings that we're actually hurting our mental health, our

36
00:02:35,310 --> 00:02:37,640
 mental well-being.

37
00:02:37,640 --> 00:02:42,530
 And so we will commit egregious acts of violence against

38
00:02:42,530 --> 00:02:44,920
 other living beings,

39
00:02:44,920 --> 00:02:50,060
 seeking out some state of physical well-being for ourselves

40
00:02:50,060 --> 00:02:51,560
 or for others.

41
00:02:51,560 --> 00:02:56,820
 And so this goes for a great number of situations. And this

42
00:02:56,820 --> 00:02:57,160
 could even be

43
00:02:57,160 --> 00:03:04,000
 extended into acts of war and acts of murder and

44
00:03:04,000 --> 00:03:06,080
 assassination and so on.

45
00:03:06,080 --> 00:03:09,610
 And so the question of whether it would have been right or

46
00:03:09,610 --> 00:03:11,440
 wrong to murder

47
00:03:11,440 --> 00:03:16,710
 someone like Adolf Hitler or Osama bin Laden, if you had

48
00:03:16,710 --> 00:03:18,120
 the chance.

49
00:03:18,120 --> 00:03:22,360
 And so this isn't exactly what we're dealing with here, but

50
00:03:22,360 --> 00:03:23,920
 the underlying issue

51
00:03:23,920 --> 00:03:28,280
 is our physical well-being versus our mental well-being.

52
00:03:28,280 --> 00:03:33,070
 So in cases of pests, in cases of dangerous animals, even

53
00:03:33,070 --> 00:03:34,440
 snakes and scorpions

54
00:03:34,440 --> 00:03:44,100
 and so on, we often react unmindfully, not realizing that

55
00:03:44,100 --> 00:03:46,600
 we're actually

56
00:03:46,600 --> 00:03:51,030
 working towards our own detriment. And even though by

57
00:03:51,030 --> 00:03:52,520
 acting in such a way,

58
00:03:52,520 --> 00:03:57,060
 we might further our physical well-being for some time, we

59
00:03:57,060 --> 00:04:01,560
're actually causing

60
00:04:01,560 --> 00:04:07,400
 a great amount of deterioration in our mental well-being.

61
00:04:07,400 --> 00:04:12,480
 So we can ask ourselves, which is more important, whether

62
00:04:12,480 --> 00:04:13,840
 it's better that we

63
00:04:13,840 --> 00:04:21,390
 live a healthy and strong physical life with a corrupt and

64
00:04:21,390 --> 00:04:24,360
 evil and unwholesome mind,

65
00:04:24,360 --> 00:04:29,720
 or whether we die with a pure mind. And for most people,

66
00:04:29,720 --> 00:04:32,560
 because of our inability

67
00:04:32,560 --> 00:04:38,310
 to see beyond this human state, beyond this one life, what

68
00:04:38,310 --> 00:04:39,520
 we call this life,

69
00:04:39,520 --> 00:04:46,120
 this birth, this existence, because this seems to be all

70
00:04:46,120 --> 00:04:46,640
 that there is,

71
00:04:46,640 --> 00:04:50,140
 and because we're so entrenched in this idea of the human

72
00:04:50,140 --> 00:04:52,320
 state as being the ultimate,

73
00:04:52,320 --> 00:04:57,240
 that we see nothing wrong with committing egregious acts,

74
00:04:57,240 --> 00:05:05,400
 and even with sullying our minds for immediate pleasure,

75
00:05:05,400 --> 00:05:09,490
 not realizing that we're accumulating these tendencies in

76
00:05:09,490 --> 00:05:10,240
 our minds,

77
00:05:10,240 --> 00:05:14,200
 and the mind doesn't go away, that it continues on, and

78
00:05:14,200 --> 00:05:17,000
 there is no end,

79
00:05:17,000 --> 00:05:20,580
 that we might call death or so on. As long as we have these

80
00:05:20,580 --> 00:05:21,400
 tendencies,

81
00:05:21,400 --> 00:05:26,210
 they will increase, and they will ever, and again, lead us

82
00:05:26,210 --> 00:05:28,680
 to conflict and suffering,

83
00:05:28,680 --> 00:05:32,160
 and they're actually setting us up for greater and greater

84
00:05:32,160 --> 00:05:33,040
 suffering,

85
00:05:33,040 --> 00:05:36,300
 which is quite easy to see if one is practicing the

86
00:05:36,300 --> 00:05:37,160
 meditation,

87
00:05:37,160 --> 00:05:40,160
 that one will see that these acts are unwholesome,

88
00:05:40,160 --> 00:05:41,560
 unpleasant,

89
00:05:41,560 --> 00:05:45,040
 not leading to positive circumstances.

90
00:05:45,040 --> 00:05:48,670
 There was a question that was asked some time ago on Ask.S

91
00:05:48,670 --> 00:05:50,160
iriMongolaw.org

92
00:05:50,160 --> 00:05:55,040
 about whether it would be worth it to go to hell yourself,

93
00:05:55,040 --> 00:05:59,560
 for people who believe that the mind can become so sully

94
00:05:59,560 --> 00:06:01,040
 that it goes to hell,

95
00:06:01,040 --> 00:06:07,620
 in order to save other beings, in order to prevent great

96
00:06:07,620 --> 00:06:10,040
 violence or so on.

97
00:06:10,040 --> 00:06:12,710
 So if you were to kill Osama bin Laden, Adolf Hitler,

98
00:06:12,710 --> 00:06:13,840
 someone like this,

99
00:06:13,840 --> 00:06:19,040
 and therefore were able to prevent great suffering,

100
00:06:19,040 --> 00:06:24,040
 the idea was to save the world, would you do it?

101
00:06:24,040 --> 00:06:30,040
 And so the point here is that by creating more violence,

102
00:06:30,040 --> 00:06:34,040
 we're in a state where there is a great amount of violence

103
00:06:34,040 --> 00:06:34,040
 in the world,

104
00:06:34,040 --> 00:06:38,970
 and there are beings in positions to inflict great violence

105
00:06:38,970 --> 00:06:40,040
 on other beings,

106
00:06:40,040 --> 00:06:46,150
 and this is a result of our accumulated tendencies to

107
00:06:46,150 --> 00:06:49,040
 perform such violent acts on each other.

108
00:06:49,040 --> 00:06:53,970
 Now by increasing those, you're not in any way helping

109
00:06:53,970 --> 00:06:55,040
 things.

110
00:06:55,040 --> 00:06:58,340
 The people who die, the people who kill, they all have

111
00:06:58,340 --> 00:06:59,040
 these tendencies in them.

112
00:06:59,040 --> 00:07:03,040
 What you do by, in this case, going to hell, or in any case

113
00:07:03,040 --> 00:07:06,040
, creating unwholesome states in the mind,

114
00:07:06,040 --> 00:07:09,390
 is just increasing the amount of unwholesomeness in the

115
00:07:09,390 --> 00:07:10,040
 universe,

116
00:07:10,040 --> 00:07:12,580
 since the beings that die, they continue on, the beings

117
00:07:12,580 --> 00:07:14,040
 that kill, they continue on,

118
00:07:14,040 --> 00:07:18,480
 and you yourself, by adding to the pot, you only increase

119
00:07:18,480 --> 00:07:20,040
 the tendencies.

120
00:07:20,040 --> 00:07:25,040
 So the important solution, the solution,

121
00:07:25,040 --> 00:07:30,400
 is to decrease the amount of killing and to teach people to

122
00:07:30,400 --> 00:07:33,040
 be patient with,

123
00:07:33,040 --> 00:07:38,090
 when confronted with revenge, when other beings would

124
00:07:38,090 --> 00:07:40,040
 inflict violence on us,

125
00:07:40,040 --> 00:07:44,040
 to be forbearing and to put an end to it.

126
00:07:44,040 --> 00:07:48,870
 If this means my death, then let that be the end, to not

127
00:07:48,870 --> 00:07:50,040
 react,

128
00:07:50,040 --> 00:07:56,750
 to not reply and create this cycle of revenge, which

129
00:07:56,750 --> 00:07:59,040
 actually spans lifetimes,

130
00:07:59,040 --> 00:08:02,040
 a life is nothing in the face of our minds,

131
00:08:02,040 --> 00:08:06,040
 and our minds continue on and carry these dates with them.

132
00:08:06,040 --> 00:08:09,580
 So this is sort of the backdrop, the theory behind what I'm

133
00:08:09,580 --> 00:08:11,040
 going to talk about here,

134
00:08:11,040 --> 00:08:17,040
 and the basic answer that, no, it's not right,

135
00:08:17,040 --> 00:08:23,040
 and it could never be a good thing to get rid of rats,

136
00:08:23,040 --> 00:08:26,040
 to kill rats that are infesting your house,

137
00:08:26,040 --> 00:08:31,290
 or even to kill parasites, or to kill those dangerous

138
00:08:31,290 --> 00:08:32,040
 animals,

139
00:08:32,040 --> 00:08:36,420
 a snake or someone that's an animal or an intruder who is

140
00:08:36,420 --> 00:08:40,040
 threatening your family, and so on.

141
00:08:40,040 --> 00:08:43,610
 And this is because of the difference between physical

142
00:08:43,610 --> 00:08:45,040
 health and mental health,

143
00:08:45,040 --> 00:08:48,790
 and if it means that we have to, as a result, suffer

144
00:08:48,790 --> 00:08:50,040
 physically,

145
00:08:50,040 --> 00:08:54,460
 and not just our bodies, but our physical surrounding might

146
00:08:54,460 --> 00:09:00,040
 be in imperfect, suboptimal,

147
00:09:00,040 --> 00:09:06,340
 but that our minds should remain healthy is far more

148
00:09:06,340 --> 00:09:08,040
 important.

149
00:09:08,040 --> 00:09:13,040
 And if our minds can stay healthy and can stay pure,

150
00:09:13,040 --> 00:09:16,040
 then it really doesn't matter where we are.

151
00:09:16,040 --> 00:09:21,110
 And so this is the first thing that we have to get through

152
00:09:21,110 --> 00:09:23,040
 and we have to understand.

153
00:09:23,040 --> 00:09:27,910
 So then the question is, well, then what do you do if you

154
00:09:27,910 --> 00:09:30,040
 are,

155
00:09:30,040 --> 00:09:34,140
 if it is not in your best interest to perform acts of

156
00:09:34,140 --> 00:09:35,040
 violence,

157
00:09:35,040 --> 00:09:37,310
 well, then what do you do when confronted with this

158
00:09:37,310 --> 00:09:38,040
 situation?

159
00:09:38,040 --> 00:09:41,660
 And here it's not just our well-being, we have a conflict

160
00:09:41,660 --> 00:09:44,040
 with our landlord, for example.

161
00:09:44,040 --> 00:09:49,300
 And so, and I've gotten several questions in regards to

162
00:09:49,300 --> 00:09:50,040
 this,

163
00:09:50,040 --> 00:09:53,370
 even people asking about if you were confronted by a

164
00:09:53,370 --> 00:09:56,040
 violent person, what would you do?

165
00:09:56,040 --> 00:09:59,970
 So there are, I think, three methods that are in line with

166
00:09:59,970 --> 00:10:01,040
 the Buddhists.

167
00:10:01,040 --> 00:10:03,680
 And there's actually, you could break it up any number of

168
00:10:03,680 --> 00:10:04,040
 ways,

169
00:10:04,040 --> 00:10:09,100
 but briefly, I think there are three suitably Buddhist

170
00:10:09,100 --> 00:10:13,040
 responses to this sort of situation,

171
00:10:13,040 --> 00:10:19,410
 whether it be pests, whether it be criminals or murderers

172
00:10:19,410 --> 00:10:21,040
 or however.

173
00:10:21,040 --> 00:10:24,040
 The first is to avoid the situation.

174
00:10:24,040 --> 00:10:29,040
 Now, the Buddha did condone avoiding those situations

175
00:10:29,040 --> 00:10:33,230
 that would obviously get in the way of your meditation

176
00:10:33,230 --> 00:10:34,040
 practice and your mental development.

177
00:10:34,040 --> 00:10:39,040
 And the examples he used were of dangerous, you know,

178
00:10:39,040 --> 00:10:44,040
 what do you call it, a charging elephant.

179
00:10:44,040 --> 00:10:47,040
 And if an elephant is charging at you, you avoid it.

180
00:10:47,040 --> 00:10:50,970
 You don't stand there and seeing, seeing, you don't have to

181
00:10:50,970 --> 00:10:53,040
, you can move to the side or so on.

182
00:10:53,040 --> 00:10:57,040
 But I think this extends to a lot of things, for instance,

183
00:10:57,040 --> 00:10:58,040
 avoiding situations,

184
00:10:58,040 --> 00:11:02,080
 avoiding, in the case of criminals or murderers, you know,

185
00:11:02,080 --> 00:11:03,040
 avoiding those areas

186
00:11:03,040 --> 00:11:09,770
 that where you're likely to be confronted with those sorts

187
00:11:09,770 --> 00:11:11,040
 of people.

188
00:11:11,040 --> 00:11:18,460
 Now, in the case of rat infestations, one, this is probably

189
00:11:18,460 --> 00:11:19,040
 not a useful solution,

190
00:11:19,040 --> 00:11:22,040
 but it is one solution that we should all keep in mind,

191
00:11:22,040 --> 00:11:26,040
 is to avoid those situations where, A, you have a landlord,

192
00:11:26,040 --> 00:11:29,740
 or B, you're living in a house that is susceptible to these

193
00:11:29,740 --> 00:11:31,040
 sorts of things.

194
00:11:31,040 --> 00:11:35,740
 So, I mean, an ideal form of this would be to leave the

195
00:11:35,740 --> 00:11:36,040
 home

196
00:11:36,040 --> 00:11:40,040
 and live under a tree or live in a cave, live in the forest

197
00:11:40,040 --> 00:11:44,040
 where you don't have to deal with these situations.

198
00:11:44,040 --> 00:11:46,960
 Because obviously living in the household life, it's much

199
00:11:46,960 --> 00:11:48,040
 more complicated.

200
00:11:48,040 --> 00:11:52,250
 And, you know, the situation with rats is one that comes up

201
00:11:52,250 --> 00:11:53,040
 common.

202
00:11:53,040 --> 00:11:56,040
 Another one that people talk about is lice.

203
00:11:56,040 --> 00:11:58,410
 Well, if you had lice, what would you do? Would you not

204
00:11:58,410 --> 00:11:59,040
 want to kill them?

205
00:11:59,040 --> 00:12:04,040
 And so one means of overcoming this is to shave your head.

206
00:12:04,040 --> 00:12:06,770
 I've heard that actually, I'm not sure if this is true or

207
00:12:06,770 --> 00:12:07,040
 not,

208
00:12:07,040 --> 00:12:09,220
 but I've heard that that will actually prevent the lice

209
00:12:09,220 --> 00:12:10,040
 from breeding.

210
00:12:10,040 --> 00:12:17,040
 If you shave your head, they won't be able to stay there.

211
00:12:17,040 --> 00:12:20,040
 They stay at the roots of the hair and so on.

212
00:12:20,040 --> 00:12:23,040
 So, these are just wild examples.

213
00:12:23,040 --> 00:12:25,040
 I mean, you could think of many different examples,

214
00:12:25,040 --> 00:12:31,040
 but in the case of rats, in the case of violence and so on,

215
00:12:31,040 --> 00:12:36,040
 we should be careful to avoid those kinds of situations

216
00:12:36,040 --> 00:12:39,040
 that would only give rise to unwholesomeness.

217
00:12:39,040 --> 00:12:45,040
 You know, if you're a, if you're not a strong person,

218
00:12:45,040 --> 00:12:48,040
 if you're a young, attractive woman, for example,

219
00:12:48,040 --> 00:12:52,040
 you might want to avoid dark streets at night or so on.

220
00:12:52,040 --> 00:12:53,040
 I mean, not even a woman.

221
00:12:53,040 --> 00:12:57,180
 If you're a person who doesn't look fierce or doesn't, you

222
00:12:57,180 --> 00:12:58,040
 know, like me or so on,

223
00:12:58,040 --> 00:13:01,840
 you might want to avoid those situations where you might

224
00:13:01,840 --> 00:13:03,040
 get into a conflict.

225
00:13:03,040 --> 00:13:09,040
 You know, as a monk, I sometimes try to avoid those areas

226
00:13:09,040 --> 00:13:12,620
 where I might be confronted with prejudice, bigotry and so

227
00:13:12,620 --> 00:13:13,040
 on.

228
00:13:13,040 --> 00:13:18,480
 I was arrested and put in jail a couple of years ago simply

229
00:13:18,480 --> 00:13:20,040
 because I look different.

230
00:13:20,040 --> 00:13:23,040
 And some people do it because of their fear.

231
00:13:23,040 --> 00:13:29,560
 They either made some assumptions or else they were

232
00:13:29,560 --> 00:13:32,040
 specifically trying to get rid of me.

233
00:13:32,040 --> 00:13:35,170
 I don't know, but I was arrested and put in jail and it was

234
00:13:35,170 --> 00:13:36,040
 a big deal.

235
00:13:36,040 --> 00:13:39,580
 So, I would have probably been in my best interest to just

236
00:13:39,580 --> 00:13:40,040
 avoid the whole issue

237
00:13:40,040 --> 00:13:46,200
 and the whole situation and stay in a place that was more

238
00:13:46,200 --> 00:13:49,040
 accepting.

239
00:13:49,040 --> 00:13:52,970
 So, this is the first answer, I think, and it will work in

240
00:13:52,970 --> 00:13:54,040
 a variety of situations

241
00:13:54,040 --> 00:13:56,040
 and it can be employed in a variety of ways.

242
00:13:56,040 --> 00:13:58,040
 Just avoid the problem.

243
00:13:58,040 --> 00:14:01,000
 Find some way so that you don't have to be confronted with

244
00:14:01,000 --> 00:14:02,040
 the situation.

245
00:14:02,040 --> 00:14:05,040
 You don't have to be confronted with these difficult issues

246
00:14:05,040 --> 00:14:08,920
 or try to restructure your life in a way so that you don't

247
00:14:08,920 --> 00:14:11,040
 meet with this situation.

248
00:14:11,040 --> 00:14:18,360
 The second way is to find an alternative, an alternative to

249
00:14:18,360 --> 00:14:19,040
 violence.

250
00:14:19,040 --> 00:14:22,040
 And there are many alternatives.

251
00:14:22,040 --> 00:14:24,650
 And I think this is a point that I always try to raise with

252
00:14:24,650 --> 00:14:25,040
 people

253
00:14:25,040 --> 00:14:30,830
 is that killing and violence are not the only way out of a

254
00:14:30,830 --> 00:14:32,040
 situation,

255
00:14:32,040 --> 00:14:36,250
 whether it be with pests, whether it be with murderers or

256
00:14:36,250 --> 00:14:37,040
 criminals or so on.

257
00:14:37,040 --> 00:14:41,090
 Often, if someone wants your wallet, maybe you just give

258
00:14:41,090 --> 00:14:42,040
 them your wallet.

259
00:14:42,040 --> 00:14:47,040
 And that's a way of dealing with the situation mindfully,

260
00:14:47,040 --> 00:14:50,040
 giving up, letting go in this sense.

261
00:14:50,040 --> 00:14:53,040
 Dealing with the situation, maybe talking to the person,

262
00:14:53,040 --> 00:14:57,460
 sometimes that can work with criminals and murderers, it's

263
00:14:57,460 --> 00:14:59,040
 probably not likely to.

264
00:14:59,040 --> 00:15:05,210
 With pests, this is really something that we spend far too

265
00:15:05,210 --> 00:15:07,040
 little time on.

266
00:15:07,040 --> 00:15:13,040
 I was trying to learn about how to get rid of termites.

267
00:15:13,040 --> 00:15:16,440
 The idea came up, "Well, when there are termites, you have

268
00:15:16,440 --> 00:15:17,040
 to kill them."

269
00:15:17,040 --> 00:15:19,570
 So I researched it and I was trying to find some

270
00:15:19,570 --> 00:15:21,040
 information

271
00:15:21,040 --> 00:15:25,390
 on ways of getting rid of termites that don't require you

272
00:15:25,390 --> 00:15:27,040
 to kill them.

273
00:15:27,040 --> 00:15:30,040
 And no research has been done on this as far as I'm aware,

274
00:15:30,040 --> 00:15:31,040
 maybe,

275
00:15:31,040 --> 00:15:34,040
 but there's nothing on the Internet anyway.

276
00:15:34,040 --> 00:15:37,600
 And I'm betting that there's very little research been done

277
00:15:37,600 --> 00:15:38,040
 on this

278
00:15:38,040 --> 00:15:40,040
 because people don't think.

279
00:15:40,040 --> 00:15:42,630
 They think, "Well, when you have termites, you just kill

280
00:15:42,630 --> 00:15:43,040
 them.

281
00:15:43,040 --> 00:15:45,040
 You find some way." And there are many ways.

282
00:15:45,040 --> 00:15:51,040
 They have many neat and ingenious ways of killing termites.

283
00:15:51,040 --> 00:15:54,910
 But no one has put any of their ingenuity into finding

284
00:15:54,910 --> 00:15:56,040
 other solutions

285
00:15:56,040 --> 00:15:59,040
 that no one's ever thought of the importance of it.

286
00:15:59,040 --> 00:16:01,910
 When you can kill them, why would you find another solution

287
00:16:01,910 --> 00:16:02,040
?

288
00:16:02,040 --> 00:16:06,570
 I think this is really tragic because in many cases, the

289
00:16:06,570 --> 00:16:08,040
 solution does exist

290
00:16:08,040 --> 00:16:10,040
 and it's not very difficult.

291
00:16:10,040 --> 00:16:12,540
 The solution with termites might be simply finding a

292
00:16:12,540 --> 00:16:14,040
 compound that they don't like.

293
00:16:14,040 --> 00:16:17,370
 Now, who would have thought to try to find a compound that

294
00:16:17,370 --> 00:16:18,040
 termites don't like

295
00:16:18,040 --> 00:16:20,040
 when you can find a compound that kills them,

296
00:16:20,040 --> 00:16:23,040
 some chemical compound that drives them away?

297
00:16:23,040 --> 00:16:27,560
 I don't know of any, but I haven't had that much experience

298
00:16:27,560 --> 00:16:29,040
 with termites.

299
00:16:29,040 --> 00:16:31,040
 I have had experience with other animals.

300
00:16:31,040 --> 00:16:34,270
 And with ants, for example, people will put out poison to

301
00:16:34,270 --> 00:16:35,040
 kill the ants.

302
00:16:35,040 --> 00:16:41,600
 Now, baby powder or talcum powder works not as well as

303
00:16:41,600 --> 00:16:43,040
 poison, obviously,

304
00:16:43,040 --> 00:16:45,340
 but from a Buddhist point of view, it works much better

305
00:16:45,340 --> 00:16:46,040
 than poison.

306
00:16:46,040 --> 00:16:50,920
 You sweep the ants away and then you put talcum powder down

307
00:16:50,920 --> 00:16:52,040
 in their path.

308
00:16:52,040 --> 00:16:55,130
 And I'm not sure if it's the scented talcum powder or if

309
00:16:55,130 --> 00:16:57,040
 normal talcum powder will work as well,

310
00:16:57,040 --> 00:16:59,040
 but somehow it stops them.

311
00:16:59,040 --> 00:17:01,040
 The small ants aren't able to cross it.

312
00:17:01,040 --> 00:17:05,070
 Even the big ants don't like it because it removes their

313
00:17:05,070 --> 00:17:06,040
 scent trails.

314
00:17:06,040 --> 00:17:08,040
 And so they don't go across it.

315
00:17:08,040 --> 00:17:12,130
 If you rub it into a plate, rub it across their trail, it

316
00:17:12,130 --> 00:17:13,040
 will remove their trail

317
00:17:13,040 --> 00:17:15,430
 and they won't be able to find their way and they won't

318
00:17:15,430 --> 00:17:16,040
 come back in that direction.

319
00:17:16,040 --> 00:17:22,040
 And I use this a lot with ants, to a great success.

320
00:17:22,040 --> 00:17:26,540
 If ants are coming along telephone wires or closed lines or

321
00:17:26,540 --> 00:17:27,040
 so on,

322
00:17:27,040 --> 00:17:30,040
 you can put butter on the closed line on the telephone wire

323
00:17:30,040 --> 00:17:32,040
 and ants won't cross butter.

324
00:17:32,040 --> 00:17:34,040
 Not all ants, anyway.

325
00:17:34,040 --> 00:17:36,660
 I do believe there are some varieties of ants that eat the

326
00:17:36,660 --> 00:17:37,040
 butter.

327
00:17:37,040 --> 00:17:41,040
 But as an example, there are ways around this.

328
00:17:41,040 --> 00:17:45,390
 With rats, the example I gave on the forum was to use

329
00:17:45,390 --> 00:17:48,040
 humane rat traps, humane mice traps.

330
00:17:48,040 --> 00:17:49,040
 These exist.

331
00:17:49,040 --> 00:17:54,040
 It's a box and you can buy them and there's bait inside.

332
00:17:54,040 --> 00:17:56,710
 The rat goes in, the door closes, or the door is made in

333
00:17:56,710 --> 00:17:58,040
 such a way that they can't get out again.

334
00:17:58,040 --> 00:18:02,050
 And then you take it away to the forest, find someplace far

335
00:18:02,050 --> 00:18:04,040
, far away, and release the rat.

336
00:18:04,040 --> 00:18:06,040
 End of story.

337
00:18:06,040 --> 00:18:09,040
 I do this with mosquitoes as well.

338
00:18:09,040 --> 00:18:13,300
 When you have mosquitoes in your home, in your tent, in

339
00:18:13,300 --> 00:18:15,040
 wherever you are,

340
00:18:15,040 --> 00:18:19,040
 you take a cup, you put the mosquito in the cup, you take

341
00:18:19,040 --> 00:18:19,040
 it outside.

342
00:18:19,040 --> 00:18:21,040
 And you do this again and again.

343
00:18:21,040 --> 00:18:23,590
 If you have a closed off space where the mosquitoes don't

344
00:18:23,590 --> 00:18:24,040
 come.

345
00:18:24,040 --> 00:18:30,070
 So finding intelligent ways to deal with pests, I think, is

346
00:18:30,070 --> 00:18:32,040
 incredibly important.

347
00:18:32,040 --> 00:18:35,040
 In fact, part of me would like to set up a wiki page.

348
00:18:35,040 --> 00:18:38,040
 Probably I will end up doing this, a wiki page for just

349
00:18:38,040 --> 00:18:42,040
 this sort of thing where people can post their good ideas

350
00:18:42,040 --> 00:18:45,760
 for how to deal with difficult situations in a Buddhist way

351
00:18:45,760 --> 00:18:46,040
.

352
00:18:46,040 --> 00:18:50,450
 So not just pests, but also how to deal with questions that

353
00:18:50,450 --> 00:18:51,040
 you get,

354
00:18:51,040 --> 00:18:54,530
 or how to deal with this sort of person or that sort of

355
00:18:54,530 --> 00:18:57,040
 person, how to deal with this situation,

356
00:18:57,040 --> 00:18:59,820
 how to deal with so many of the issues that we're

357
00:18:59,820 --> 00:19:03,270
 confronted with, that all people and all religions are

358
00:19:03,270 --> 00:19:04,040
 confronted with

359
00:19:04,040 --> 00:19:08,850
 and have to find some way to make it chive with their

360
00:19:08,850 --> 00:19:12,040
 understanding of ethics and practice.

361
00:19:12,040 --> 00:19:17,530
 So this is the second method, is to deal with it, find a

362
00:19:17,530 --> 00:19:21,040
 way, an alternative means of dealing with a situation

363
00:19:21,040 --> 00:19:24,040
 that doesn't require violence.

364
00:19:24,040 --> 00:19:28,430
 And one note I'd make on that is that sometimes in self-

365
00:19:28,430 --> 00:19:29,040
defense,

366
00:19:29,040 --> 00:19:33,000
 it is even according to the Buddha proper to resort to

367
00:19:33,000 --> 00:19:35,040
 limited amounts of violence.

368
00:19:35,040 --> 00:19:39,040
 So if someone's attacking you, to push them out of the way

369
00:19:39,040 --> 00:19:43,040
 or to hit them enough so that you can run away,

370
00:19:43,040 --> 00:19:45,040
 even monks are allowed to do this.

371
00:19:45,040 --> 00:19:46,970
 We're not allowed to hit someone, but we're allowed to hit

372
00:19:46,970 --> 00:19:49,040
 someone in self-defense in order to get away.

373
00:19:49,040 --> 00:19:55,840
 So if it means that you have to perform some limited act of

374
00:19:55,840 --> 00:19:58,040
 violence in order to escape,

375
00:19:58,040 --> 00:20:02,940
 or in order to wake up the attacker or so on, or to find a

376
00:20:02,940 --> 00:20:05,040
 way to change the situation,

377
00:20:05,040 --> 00:20:10,500
 even putting the attacker into an armlock or whatever, if

378
00:20:10,500 --> 00:20:14,040
 you know karate or kung fu or some martial arts,

379
00:20:14,040 --> 00:20:19,040
 to be able to change the situation and avoid, again,

380
00:20:19,040 --> 00:20:22,040
 avoiding the greater act of violence,

381
00:20:22,040 --> 00:20:27,950
 then to a limited extent, because it's not something that

382
00:20:27,950 --> 00:20:30,040
 is designed to,

383
00:20:30,040 --> 00:20:35,250
 it's not something that is designed to harm, it's in self-

384
00:20:35,250 --> 00:20:39,140
defense and it's designed to allow you to escape the

385
00:20:39,140 --> 00:20:40,040
 situation.

386
00:20:40,040 --> 00:20:45,210
 Then that is permitted, provided that it doesn't inflict

387
00:20:45,210 --> 00:20:50,040
 fatal harm on the other person or on the other being.

388
00:20:50,040 --> 00:20:57,040
 So there might be a case where limited amounts of violence

389
00:20:57,040 --> 00:21:00,040
 done not in the intention of hurting or killing

390
00:21:00,040 --> 00:21:04,190
 or seriously harming the other person, but simply in the

391
00:21:04,190 --> 00:21:08,040
 interest of self-defense and for the purposes of escaping,

392
00:21:08,040 --> 00:21:12,040
 might be or are considered to be allowed.

393
00:21:12,040 --> 00:21:15,040
 This is a way of dealing with the issue.

394
00:21:15,040 --> 00:21:19,120
 The third answer, which I think we should also keep in mind

395
00:21:19,120 --> 00:21:21,040
, and this goes back to what I was saying in the beginning,

396
00:21:21,040 --> 00:21:25,040
 is to accept and to let go of the situation.

397
00:21:25,040 --> 00:21:29,400
 And I think the issue of rats and the landlord is an

398
00:21:29,400 --> 00:21:32,040
 interesting example of this,

399
00:21:32,040 --> 00:21:36,210
 because sometimes we have to think outside the box and we

400
00:21:36,210 --> 00:21:39,040
 have to look outside of our situation

401
00:21:39,040 --> 00:21:45,350
 and not get confined to a or b mentality, where if I don't

402
00:21:45,350 --> 00:21:47,040
 do this, that is going to happen,

403
00:21:47,040 --> 00:21:52,040
 because often when we let things go, when we're mindful,

404
00:21:52,040 --> 00:21:54,040
 when we're aware of the situation,

405
00:21:54,040 --> 00:21:58,800
 there's a c alternative arises almost magically and a and b

406
00:21:58,800 --> 00:22:01,040
 disappear completely.

407
00:22:01,040 --> 00:22:06,510
 So maybe in the case where we're confronted by an assault,

408
00:22:06,510 --> 00:22:11,040
 someone assaulting us,

409
00:22:11,040 --> 00:22:16,110
 sometimes it can happen that when we're mindful, when we're

410
00:22:16,110 --> 00:22:20,040
 aware and when we're meditating on the situation,

411
00:22:20,040 --> 00:22:23,680
 when we're taking it as a Buddhist practice, people are

412
00:22:23,680 --> 00:22:26,040
 saying, "What? Would you just say pain?

413
00:22:26,040 --> 00:22:31,380
 What's the problem that's hitting you?" I think, yes, that

414
00:22:31,380 --> 00:22:34,040
's a good, a perfectly reasonable response to the situation

415
00:22:34,040 --> 00:22:40,200
 and can often have magical results where people have found

416
00:22:40,200 --> 00:22:44,040
 that suddenly the whole situation changed

417
00:22:44,040 --> 00:22:48,310
 and they were no longer the victim, now they were in

418
00:22:48,310 --> 00:22:51,040
 control and the other person was forced,

419
00:22:51,040 --> 00:22:54,610
 really due to the power of presence, because the mind is

420
00:22:54,610 --> 00:22:58,040
 such a powerful thing, much more powerful than the body

421
00:22:58,040 --> 00:23:02,690
 and simply the presence of someone who is mindful is really

422
00:23:02,690 --> 00:23:05,040
 the greatest weapon there is

423
00:23:05,040 --> 00:23:09,040
 and it's something that can truly overcome these situations

424
00:23:09,040 --> 00:23:09,040
.

425
00:23:09,040 --> 00:23:13,820
 So in the case of the rats and the landlord, it might be

426
00:23:13,820 --> 00:23:16,040
 that simply by being mindful

427
00:23:16,040 --> 00:23:19,910
 and watching the situation unfold and allowing the

428
00:23:19,910 --> 00:23:21,040
 consequences.

429
00:23:21,040 --> 00:23:24,420
 If the landlord says they're going to throw you out, then

430
00:23:24,420 --> 00:23:26,040
 you simply say to the landlord,

431
00:23:26,040 --> 00:23:30,040
 "I'm Buddhist and I don't kill," and so do what you will

432
00:23:30,040 --> 00:23:31,040
 and if it means that we have to do that,

433
00:23:31,040 --> 00:23:35,040
 we have to come to some sort of conflict and so be it.

434
00:23:35,040 --> 00:23:40,060
 Letting things go, holding on to what is really and truly

435
00:23:40,060 --> 00:23:41,040
 important,

436
00:23:41,040 --> 00:23:45,510
 which is your mental health and mental well-being and the

437
00:23:45,510 --> 00:23:47,040
 truth, because it's being untrue to yourself

438
00:23:47,040 --> 00:23:52,160
 to perform violence on other beings when you yourself don't

439
00:23:52,160 --> 00:23:54,040
 want to feel such violence,

440
00:23:54,040 --> 00:23:58,660
 when you yourself don't or wish for that not to happen to

441
00:23:58,660 --> 00:23:59,040
 you.

442
00:23:59,040 --> 00:24:02,160
 So if someone's going to kill you, well the only reason you

443
00:24:02,160 --> 00:24:06,040
'd kill them first is because you yourself don't want to die

444
00:24:06,040 --> 00:24:08,630
 and so you're being just as hypocritical as the other

445
00:24:08,630 --> 00:24:09,040
 person.

446
00:24:09,040 --> 00:24:12,320
 You're inflicting something on other beings that you

447
00:24:12,320 --> 00:24:14,040
 yourself would not wish for.

448
00:24:14,040 --> 00:24:17,250
 And so it's something that is against harmony and is

449
00:24:17,250 --> 00:24:18,040
 against the truth.

450
00:24:18,040 --> 00:24:22,130
 It's against reality and it's going to create, as I said,

451
00:24:22,130 --> 00:24:24,040
 corruption in the mind,

452
00:24:24,040 --> 00:24:28,390
 something that we can do much better without in this and we

453
00:24:28,390 --> 00:24:32,040
 can do with much less than there is already in the world.

454
00:24:32,040 --> 00:24:36,590
 So something that we should strive to do away with rather

455
00:24:36,590 --> 00:24:38,040
 than increase.

456
00:24:38,040 --> 00:24:41,040
 So simply by being mindful, by being aware of the situation

457
00:24:41,040 --> 00:24:43,040
, whether it be a violent situation,

458
00:24:43,040 --> 00:24:47,390
 whether it be a difficult situation, whatever the, you know

459
00:24:47,390 --> 00:24:50,040
, whether it be some lice or something,

460
00:24:50,040 --> 00:24:53,980
 even just sticking with the itching and so on, the pain of

461
00:24:53,980 --> 00:24:57,040
 having lice should be a part,

462
00:24:57,040 --> 00:25:00,330
 not all of the answer, but should be at least a part of the

463
00:25:00,330 --> 00:25:01,040
 answer.

464
00:25:01,040 --> 00:25:06,600
 Of course, we can find other solutions and something that

465
00:25:06,600 --> 00:25:08,040
 stops the lice from,

466
00:25:08,040 --> 00:25:15,470
 or repels the lice and so repels the assailant, but doesn't

467
00:25:15,470 --> 00:25:19,040
 cause more suffering than is warranted

468
00:25:19,040 --> 00:25:27,220
 or isn't of the purpose of causing death or fatal or

469
00:25:27,220 --> 00:25:29,040
 permanent physical damage

470
00:25:29,040 --> 00:25:32,040
 or suffering to the other being.

471
00:25:32,040 --> 00:25:34,000
 So I hope this has been helpful. I think this is an

472
00:25:34,000 --> 00:25:36,040
 important subject and I'm glad to talk about it.

473
00:25:36,040 --> 00:25:38,690
 I've been meaning to make a video on this subject for a

474
00:25:38,690 --> 00:25:39,040
 while.

475
00:25:39,040 --> 00:25:43,030
 So there you have it. Thanks for tuning in and all the best

476
00:25:43,030 --> 00:25:43,040
.

