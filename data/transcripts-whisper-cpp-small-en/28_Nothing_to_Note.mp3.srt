1
00:00:00,000 --> 00:00:05,190
 Nothing to note. What should one do when there is nothing

2
00:00:05,190 --> 00:00:06,000
 to note?

3
00:00:06,000 --> 00:00:11,000
 It is common to encounter states of quiet where there is no

4
00:00:11,000 --> 00:00:15,750
 awareness of the abdomen or any other experience besides a

5
00:00:15,750 --> 00:00:19,000
 sense of calm, quiet or emptiness.

6
00:00:19,000 --> 00:00:22,900
 Although these states may seem to be outside of ordinary

7
00:00:22,900 --> 00:00:27,060
 meditation practice, they are no different from any other

8
00:00:27,060 --> 00:00:31,240
 phenomenon and should therefore be noted in the same way,

9
00:00:31,240 --> 00:00:38,850
 noting quiet, quiet, calm, calm or empty, empty until they

10
00:00:38,850 --> 00:00:40,000
 disappear.

11
00:00:40,000 --> 00:00:44,680
 If there is any liking or wanting in regards to such states

12
00:00:44,680 --> 00:00:50,000
, one should note that as liking, liking or wanting, wanting

13
00:00:50,000 --> 00:00:50,000
.

