1
00:00:00,000 --> 00:00:05,000
 Okay, good evening everyone.

2
00:00:05,000 --> 00:00:07,000
 Broadcasting live.

3
00:00:07,000 --> 00:00:10,000
 May 23rd.

4
00:00:10,000 --> 00:00:14,000
 Today we're going to do another Dhammapada.

5
00:00:14,000 --> 00:00:18,000
 So, no video.

6
00:00:18,000 --> 00:00:24,000
 And I'm using a different mic, so you're sloping.

7
00:00:24,000 --> 00:00:27,000
 That goes okay.

8
00:00:27,000 --> 00:00:31,190
 I realized the mic I've been using isn't as good as I

9
00:00:31,190 --> 00:00:33,000
 thought it was.

10
00:00:33,000 --> 00:00:55,000
 Maybe this one's better.

11
00:00:55,000 --> 00:00:58,000
 Hello and welcome back to our study of Dhammapada.

12
00:00:58,000 --> 00:01:03,970
 Today we continue on with verse number 135, which reads as

13
00:01:03,970 --> 00:01:06,000
 follows.

14
00:01:06,000 --> 00:01:13,000
 Yata danden agopalo, ga wopati tigocharam.

15
00:01:13,000 --> 00:01:14,000
 Iyvang

16
00:01:14,000 --> 00:01:21,000
 Charatamatu cha, ayum bhaji ntiparnina.

17
00:01:21,000 --> 00:01:30,000
 Which means just as a cow herd with a stink drives the cows

18
00:01:30,000 --> 00:01:34,000
 to the cow pasture.

19
00:01:34,000 --> 00:01:42,430
 Even so, old age and death, charatamatu cha, drives the

20
00:01:42,430 --> 00:01:45,000
 life of beings.

21
00:01:45,000 --> 00:01:55,340
 The mantra that means the age is where our age is leading

22
00:01:55,340 --> 00:01:57,000
 us to.

23
00:01:57,000 --> 00:02:03,210
 So, another brief story, but this one's quite pertinent and

24
00:02:03,210 --> 00:02:10,000
 interesting, I think, especially for us meditators.

25
00:02:10,000 --> 00:02:17,040
 The story goes that in the time of the Buddha there was a

26
00:02:17,040 --> 00:02:19,000
 great holiday.

27
00:02:19,000 --> 00:02:22,000
 Of course, a holiday means a holy day.

28
00:02:22,000 --> 00:02:26,990
 So, the Buddha established in line with the Hindu tradition

29
00:02:26,990 --> 00:02:33,000
 that the full moon would be a holy day.

30
00:02:33,000 --> 00:02:35,950
 It seems that it sort of happened because Buddhists or

31
00:02:35,950 --> 00:02:39,000
 people who are interested in the Buddha's teaching,

32
00:02:39,000 --> 00:02:44,360
 who were in the Hindu society or the Indian society, would

33
00:02:44,360 --> 00:02:45,000
 ask the Buddha,

34
00:02:45,000 --> 00:02:49,180
 "How should I keep the holy day? I'm not going to worship a

35
00:02:49,180 --> 00:02:53,000
 god or something. What should I do?"

36
00:02:53,000 --> 00:02:59,000
 The Buddha outlined a way to keep a holy day.

37
00:02:59,000 --> 00:03:03,320
 So, it doesn't seem like the Buddha actually said, "Yes,

38
00:03:03,320 --> 00:03:06,000
 everyone has to keep these days."

39
00:03:06,000 --> 00:03:10,920
 But rather it would be a way to keep the established

40
00:03:10,920 --> 00:03:14,000
 holidays in a Buddhist way.

41
00:03:14,000 --> 00:03:17,490
 Of course, monks had to, on the full moon and the empty

42
00:03:17,490 --> 00:03:21,000
 moon, had their holy days,

43
00:03:21,000 --> 00:03:23,940
 where they had to come together and recite, and the Buddha

44
00:03:23,940 --> 00:03:25,000
 did lay that down.

45
00:03:25,000 --> 00:03:32,450
 But for lay people it seems a little bit more just going

46
00:03:32,450 --> 00:03:36,000
 with the Hindu tradition,

47
00:03:36,000 --> 00:03:38,000
 but turning it into Buddhism.

48
00:03:38,000 --> 00:03:41,490
 So, they would keep the eight Buddhist precepts and they

49
00:03:41,490 --> 00:03:43,000
 would go and listen to the Buddha's teaching

50
00:03:43,000 --> 00:03:46,000
 and stay at the monastery.

51
00:03:46,000 --> 00:03:56,060
 Anyway, so, on this day there were 500 women who took upon

52
00:03:56,060 --> 00:04:00,000
 themselves the holy day activities.

53
00:04:00,000 --> 00:04:02,800
 So, they took the eight precepts and they went to listen to

54
00:04:02,800 --> 00:04:04,000
 the Buddha's teaching.

55
00:04:04,000 --> 00:04:07,520
 And we sat there and came and saw that there were young

56
00:04:07,520 --> 00:04:08,000
 women,

57
00:04:08,000 --> 00:04:12,000
 there were middle-aged women and there were old women.

58
00:04:12,000 --> 00:04:14,680
 And so, she was curious as to why they were keeping, why

59
00:04:14,680 --> 00:04:16,000
 they were doing this.

60
00:04:16,000 --> 00:04:20,000
 What was the purpose? What was their intention?

61
00:04:20,000 --> 00:04:24,000
 Were they all doing it for the right reasons?

62
00:04:24,000 --> 00:04:27,040
 And so, she went to them, she went to the oldest of them

63
00:04:27,040 --> 00:04:28,000
 and asked,

64
00:04:28,000 --> 00:04:34,320
 "What are you doing? Why are you keeping them over the day?

65
00:04:34,320 --> 00:04:37,000
 Why are you doing this?"

66
00:04:37,000 --> 00:04:42,890
 And they said, "Because we want to go to heaven. We're old

67
00:04:42,890 --> 00:04:46,000
 and we're wishing for heaven.

68
00:04:46,000 --> 00:04:50,490
 We want that when we die, the purity of our minds should

69
00:04:50,490 --> 00:04:53,000
 lead us to be born as angels,

70
00:04:53,000 --> 00:04:58,000
 where we can enjoy the divine bliss."

71
00:04:58,000 --> 00:05:01,000
 So, their religious life was for heaven.

72
00:05:01,000 --> 00:05:05,550
 So, she went on and asked the middle-aged women why they

73
00:05:05,550 --> 00:05:07,000
 were keeping the holiday

74
00:05:07,000 --> 00:05:12,300
 and they answered in order to get out of the power of our

75
00:05:12,300 --> 00:05:13,000
 husbands.

76
00:05:13,000 --> 00:05:19,180
 So, they came to stay at the monastery and maybe to become

77
00:05:19,180 --> 00:05:20,000
 nuns,

78
00:05:20,000 --> 00:05:25,000
 to escape their home life.

79
00:05:25,000 --> 00:05:31,780
 The burden of being a housewife in India under the power of

80
00:05:31,780 --> 00:05:33,000
 her husband

81
00:05:33,000 --> 00:05:40,500
 who might abuse her or control her or require her to do men

82
00:05:40,500 --> 00:05:42,000
ial labor

83
00:05:42,000 --> 00:05:51,000
 can be quite odious for women in India at that time.

84
00:05:51,000 --> 00:05:56,220
 So, then she went on and asked the young women, "Why are

85
00:05:56,220 --> 00:05:59,000
 you keeping the holiday?"

86
00:05:59,000 --> 00:06:04,780
 These are the young women who were married, sort of the

87
00:06:04,780 --> 00:06:07,000
 prime of life.

88
00:06:07,000 --> 00:06:10,460
 And they said, "We hope that through the power of our

89
00:06:10,460 --> 00:06:11,000
 goodness,

90
00:06:11,000 --> 00:06:16,220
 the power of this good deed, we'll be able to conceive a

91
00:06:16,220 --> 00:06:18,000
 child and get pregnant."

92
00:06:18,000 --> 00:06:23,200
 This is a very common thing in India and even in Sri Lanka

93
00:06:23,200 --> 00:06:26,000
 among Buddhist families.

94
00:06:26,000 --> 00:06:31,000
 They believe that the goodness of their good deeds,

95
00:06:31,000 --> 00:06:35,310
 whether they give a gift to monks, pour water over the Bod

96
00:06:35,310 --> 00:06:38,000
hi tree, that kind of thing,

97
00:06:38,000 --> 00:06:45,000
 will lead them to get pregnant. They wanted children.

98
00:06:45,000 --> 00:06:50,370
 So, curiouser and curiouser, she goes to the unmarried

99
00:06:50,370 --> 00:06:51,000
 women.

100
00:06:51,000 --> 00:06:53,000
 Here we have the old women who are fed up with life

101
00:06:53,000 --> 00:06:55,000
 and they're not looking for anything in life.

102
00:06:55,000 --> 00:06:57,000
 They want to be born in heaven.

103
00:06:57,000 --> 00:07:00,150
 The middle-aged women are just trying to escape their

104
00:07:00,150 --> 00:07:01,000
 husbands.

105
00:07:01,000 --> 00:07:03,240
 The younger women, younger married women are looking to

106
00:07:03,240 --> 00:07:04,000
 have children.

107
00:07:04,000 --> 00:07:09,860
 What are these young women? Unmarried girls? Why are they

108
00:07:09,860 --> 00:07:11,000
 coming?

109
00:07:11,000 --> 00:07:15,000
 To listen to the Buddha's teaching and to keep the precepts

110
00:07:15,000 --> 00:07:15,000
.

111
00:07:15,000 --> 00:07:17,350
 And they say, "We're doing it because we want to get

112
00:07:17,350 --> 00:07:21,000
 married. We want a husband.

113
00:07:21,000 --> 00:07:23,000
 We'd like to get a husband while we're still young."

114
00:07:23,000 --> 00:07:26,990
 Because it seems kind of funny that the older women would

115
00:07:26,990 --> 00:07:29,000
 be trying to escape their husbands

116
00:07:29,000 --> 00:07:31,000
 and why are these women going for it?

117
00:07:31,000 --> 00:07:34,440
 But it seems that it was probably even a worse fate than

118
00:07:34,440 --> 00:07:37,000
 getting married in India

119
00:07:37,000 --> 00:07:42,000
 to be known to be an old maid. They've never been married.

120
00:07:42,000 --> 00:07:47,000
 Because then you look down upon us the lowest of the low.

121
00:07:47,000 --> 00:07:52,870
 You bring no honor to your family. You bring no money to

122
00:07:52,870 --> 00:07:54,000
 your family.

123
00:07:54,000 --> 00:07:58,000
 And so you end up being a slave and doing menial labour.

124
00:07:58,000 --> 00:08:02,000
 Women had to quite bad in India, quite hard it seems.

125
00:08:02,000 --> 00:08:05,000
 I think even today there's a lot of this problems going on.

126
00:08:05,000 --> 00:08:11,000
 Women are treated as inferior.

127
00:08:11,000 --> 00:08:13,780
 But you thought it curious that none of them, or it seemed

128
00:08:13,780 --> 00:08:16,000
 like many of them anyway,

129
00:08:16,000 --> 00:08:19,550
 had no interest in doing it for spiritual purposes, for

130
00:08:19,550 --> 00:08:23,000
 purity of mind, for attaining nirvana.

131
00:08:23,000 --> 00:08:27,000
 It all was for actually quite mundane purposes.

132
00:08:27,000 --> 00:08:30,000
 This is why they were practicing spiritual.

133
00:08:30,000 --> 00:08:36,000
 Why they were practicing the spiritual, the holy life so to

134
00:08:36,000 --> 00:08:37,000
 speak.

135
00:08:37,000 --> 00:08:40,760
 And so she went to the Buddha and asked him, told him of

136
00:08:40,760 --> 00:08:42,000
 this and asked him,

137
00:08:42,000 --> 00:08:46,000
 "Well look at what's said. Why is this?

138
00:08:46,000 --> 00:08:49,060
 Why aren't these, why don't these people see the greatness

139
00:08:49,060 --> 00:08:50,000
 of this teaching?

140
00:08:50,000 --> 00:08:55,000
 Why aren't they practicing it for the right reasons?"

141
00:08:55,000 --> 00:09:03,000
 The Buddha said, "It is amazing, you know.

142
00:09:03,000 --> 00:09:08,000
 Old age, sickness and death are like cowherds."

143
00:09:08,000 --> 00:09:11,000
 And so he's leading up to this verse.

144
00:09:11,000 --> 00:09:15,100
 He says, "Birth sends them to old age, old age sends them

145
00:09:15,100 --> 00:09:16,000
 to sickness,

146
00:09:16,000 --> 00:09:19,000
 sickness sends them to death.

147
00:09:19,000 --> 00:09:25,000
 They cut life short as though they cut with an axe."

148
00:09:25,000 --> 00:09:30,000
 But despite this, and this is what's interesting for us,

149
00:09:30,000 --> 00:09:33,000
 there are none that desire absence of rebirth.

150
00:09:33,000 --> 00:09:35,000
 Rebirth is all they desire.

151
00:09:35,000 --> 00:09:40,020
 There's none that desire absence of arising, absence of

152
00:09:40,020 --> 00:09:41,000
 things.

153
00:09:41,000 --> 00:09:44,000
 None that wish to be free.

154
00:09:44,000 --> 00:09:47,160
 There are as many of us nowadays don't even think about

155
00:09:47,160 --> 00:09:48,000
 being reborn,

156
00:09:48,000 --> 00:09:51,000
 but we want the arising of things.

157
00:09:51,000 --> 00:09:54,000
 We want birth, whether it be the birth of a child,

158
00:09:54,000 --> 00:09:58,100
 whether it be the birth of marriage, whether it be the

159
00:09:58,100 --> 00:10:01,000
 birth of something.

160
00:10:01,000 --> 00:10:03,000
 We want things.

161
00:10:03,000 --> 00:10:05,000
 We want things for our life,

162
00:10:05,000 --> 00:10:12,000
 we want goals that we wish to achieve.

163
00:10:12,000 --> 00:10:20,000
 And so then he tells us this first, sort of to unpack this.

164
00:10:20,000 --> 00:10:24,000
 It is clear, and this is a clear reminder for us,

165
00:10:24,000 --> 00:10:29,600
 that indeed old age, sickness and death are in store for us

166
00:10:29,600 --> 00:10:30,000
.

167
00:10:30,000 --> 00:10:33,000
 We're all headed that way.

168
00:10:33,000 --> 00:10:36,000
 And being born leads to that.

169
00:10:36,000 --> 00:10:42,000
 Now this is sort of a classic Buddhist teaching.

170
00:10:42,000 --> 00:10:46,000
 But the interesting thing is how attached we are,

171
00:10:46,000 --> 00:10:49,000
 how in general this applies to just about everything.

172
00:10:49,000 --> 00:10:52,000
 Everything is subject to change.

173
00:10:52,000 --> 00:10:55,860
 Anything we attain, anything we obtain is subject to

174
00:10:55,860 --> 00:10:57,000
 dissolution.

175
00:10:57,000 --> 00:11:02,020
 Subject to change, impermanence, instability, uncertainty,

176
00:11:02,020 --> 00:11:05,000
 unpredictability.

177
00:11:05,000 --> 00:11:12,000
 You get something you can't expect for it to stay.

178
00:11:12,000 --> 00:11:16,000
 You can't be sure that you're going to get what you want.

179
00:11:16,000 --> 00:11:21,740
 So we build up these expectations and desires, and we're

180
00:11:21,740 --> 00:11:23,000
 disappointed.

181
00:11:23,000 --> 00:11:29,000
 Our lives are not entirely pleasant.

182
00:11:29,000 --> 00:11:32,000
 In fact, it's probably a 50/50 balance for most people.

183
00:11:32,000 --> 00:11:36,350
 Even people who think that their lives are mostly and

184
00:11:36,350 --> 00:11:39,000
 mainly or entirely pleasant.

185
00:11:39,000 --> 00:11:41,600
 It's only because they're unable to see that, and unable to

186
00:11:41,600 --> 00:11:42,000
 remember,

187
00:11:42,000 --> 00:11:46,720
 and quick to forget the stress and the suffering that comes

188
00:11:46,720 --> 00:11:48,000
 along with life.

189
00:11:48,000 --> 00:11:52,000
 And over the years we build up a tolerance to this,

190
00:11:52,000 --> 00:11:56,750
 and we come to understand happiness as what is true

191
00:11:56,750 --> 00:11:58,000
 happiness.

192
00:11:58,000 --> 00:12:01,180
 It's having a smile at the end of the day after you've

193
00:12:01,180 --> 00:12:02,000
 worked hard,

194
00:12:02,000 --> 00:12:05,000
 and of course the work is not always pleasant.

195
00:12:05,000 --> 00:12:08,000
 But we still think it's happiness.

196
00:12:08,000 --> 00:12:12,910
 We're more quick to ignore or forget the pain and the

197
00:12:12,910 --> 00:12:14,000
 suffering.

198
00:12:14,000 --> 00:12:16,000
 It goes along with not just physical labor,

199
00:12:16,000 --> 00:12:21,000
 but interactions with other people, arguments, worries, and

200
00:12:21,000 --> 00:12:27,000
 stress, and loss.

201
00:12:27,000 --> 00:12:30,000
 So basically what the Buddha is saying is he's pointing out

202
00:12:30,000 --> 00:12:32,000
 that

203
00:12:32,000 --> 00:12:36,000
 attachment to things is not based on ration.

204
00:12:36,000 --> 00:12:41,000
 It's not based on reason.

205
00:12:41,000 --> 00:12:45,360
 A drug addict will tell you, it's not logical that they're

206
00:12:45,360 --> 00:12:46,000
 addicted to drugs.

207
00:12:46,000 --> 00:12:49,000
 They know that it's causing them suffering.

208
00:12:49,000 --> 00:12:52,860
 Once they can admit that they're an addict, they know that

209
00:12:52,860 --> 00:12:54,000
 it's bad for them.

210
00:12:54,000 --> 00:12:58,000
 It's not rational.

211
00:12:58,000 --> 00:13:02,000
 But for most of us we're not even there yet.

212
00:13:02,000 --> 00:13:06,000
 Of course when it's a subtle attachment,

213
00:13:06,000 --> 00:13:12,000
 whatever we're attached to in life, in the world,

214
00:13:12,000 --> 00:13:17,280
 there's very few people who would say that it's a problem,

215
00:13:17,280 --> 00:13:19,000
 or it's bad.

216
00:13:19,000 --> 00:13:25,000
 When asked why, or what is the benefit of attachment,

217
00:13:25,000 --> 00:13:29,000
 what is the benefit of indulgence and pleasure,

218
00:13:29,000 --> 00:13:34,860
 which seems to be very much the essence or the reason for

219
00:13:34,860 --> 00:13:36,000
 living,

220
00:13:36,000 --> 00:13:39,000
 they can't really come up with a good reason.

221
00:13:39,000 --> 00:13:42,000
 It's not based on rationale.

222
00:13:42,000 --> 00:13:47,000
 Why do you attach to these things?

223
00:13:47,000 --> 00:13:53,560
 The reason people usually give is because it makes me happy

224
00:13:53,560 --> 00:13:54,000
.

225
00:13:54,000 --> 00:13:57,190
 We think that the things that we attach to, the things that

226
00:13:57,190 --> 00:13:58,000
 we strive for,

227
00:13:58,000 --> 00:14:07,000
 these things make us happy.

228
00:14:07,000 --> 00:14:12,000
 And we're unable to see the true nature of these things.

229
00:14:12,000 --> 00:14:17,000
 We're unable to see the stress that's caused by chasing,

230
00:14:17,000 --> 00:14:21,000
 the stress that's caused by not getting what you want,

231
00:14:21,000 --> 00:14:24,000
 the stress that's caused by loss.

232
00:14:24,000 --> 00:14:29,000
 We're unable to see the true nature of these things.

233
00:14:29,000 --> 00:14:34,340
 And so the Buddha says that despite this, our desires for

234
00:14:34,340 --> 00:14:37,000
 things are in spite of reason.

235
00:14:37,000 --> 00:14:42,000
 I'm going to... down here.

236
00:14:42,000 --> 00:14:49,000
 I lost my train of thought because my battery just went out

237
00:14:49,000 --> 00:14:50,000
.

238
00:14:50,000 --> 00:14:57,000
 So that recording's not going to work.

239
00:14:57,000 --> 00:15:00,340
 Well, I'll go on and finish it and I can record this again

240
00:15:00,340 --> 00:15:01,000
 later.

241
00:15:01,000 --> 00:15:11,000
 So let's get back on track.

242
00:15:11,000 --> 00:15:16,000
 Our desires, we desire things.

243
00:15:16,000 --> 00:15:18,000
 And our desires don't make us happier.

244
00:15:18,000 --> 00:15:20,000
 This is the thing.

245
00:15:20,000 --> 00:15:26,000
 It's we want things and we believe that they make us happy.

246
00:15:26,000 --> 00:15:30,000
 This is what we come to see when we practice meditation.

247
00:15:30,000 --> 00:15:33,000
 We see how the mind really works,

248
00:15:33,000 --> 00:15:36,000
 how our addictions are just creating this...

249
00:15:36,000 --> 00:15:37,000
 well, creating more addiction,

250
00:15:37,000 --> 00:15:41,000
 are chasing after things,

251
00:15:41,000 --> 00:15:47,000
 is creating this intense addiction, attachment.

252
00:15:47,000 --> 00:15:51,000
 And when you finally sit down and try to just be,

253
00:15:51,000 --> 00:15:57,000
 you find it impossible because your mind is drawn,

254
00:15:57,000 --> 00:16:01,000
 is pulled and pushed by things you like and dislike.

255
00:16:01,000 --> 00:16:04,000
 And this doesn't come out of nowhere.

256
00:16:04,000 --> 00:16:06,000
 It's not just who we are.

257
00:16:06,000 --> 00:16:09,000
 It comes directly out of the habits of clinging,

258
00:16:09,000 --> 00:16:17,000
 the habits of chasing, the habits of needing, wanting.

259
00:16:17,000 --> 00:16:19,370
 So what meditation shows you, it seems like this is

260
00:16:19,370 --> 00:16:21,000
 ridiculous.

261
00:16:21,000 --> 00:16:24,000
 Why would we come and sit and torture ourselves

262
00:16:24,000 --> 00:16:29,000
 so we could be enjoying the things that we love?

263
00:16:29,000 --> 00:16:38,410
 And so it's essential that we break through and see the

264
00:16:38,410 --> 00:16:40,000
 connection,

265
00:16:40,000 --> 00:16:43,140
 that there really is a connection between our desires and

266
00:16:43,140 --> 00:16:44,000
 our suffering.

267
00:16:44,000 --> 00:16:47,750
 The suffering of meditation, it's not because of meditation

268
00:16:47,750 --> 00:16:48,000
.

269
00:16:48,000 --> 00:16:50,000
 Meditation is actually quite peaceful.

270
00:16:50,000 --> 00:16:52,000
 You're just walking and sitting.

271
00:16:52,000 --> 00:16:55,000
 The problem is not with the practice.

272
00:16:55,000 --> 00:17:00,000
 It's directly with what we're attached to,

273
00:17:00,000 --> 00:17:08,000
 our needs and our wants that make us unable to sit,

274
00:17:08,000 --> 00:17:16,000
 unable to be, unable to be objective, unable to be patient,

275
00:17:16,000 --> 00:17:23,000
 unable to exist without the objects of our desire.

276
00:17:23,000 --> 00:17:28,000
 So it would have been the Dhammapada talk for this evening,

277
00:17:28,000 --> 00:17:30,000
 and maybe I'll go on a little bit more,

278
00:17:30,000 --> 00:17:36,000
 but now I need to re-record it because I'm not diligent in

279
00:17:36,000 --> 00:17:39,000
 recharging patterns.

280
00:17:39,000 --> 00:17:44,000
 So we'll stop there.

281
00:17:44,000 --> 00:17:47,000
 But I will record that again later.

282
00:17:47,000 --> 00:17:58,000
 How's the audio? Does anyone hear me?

283
00:17:58,000 --> 00:18:08,900
 You can open the window again. It's getting very hot in

284
00:18:08,900 --> 00:18:12,000
 here.

285
00:18:12,000 --> 00:18:22,000
 All the way in is open and open.

286
00:18:22,000 --> 00:18:25,000
 How important is it to be able to sit in a full lotus?

287
00:18:25,000 --> 00:18:27,000
 Have you read my booklet on how to meditate?

288
00:18:27,000 --> 00:18:30,000
 I'm pretty sure it doesn't tell you to sit in a full lotus.

289
00:18:30,000 --> 00:18:32,000
 But that's a sign that you haven't read my booklet.

290
00:18:32,000 --> 00:18:38,000
 So before I answer more of your questions, maybe you have.

291
00:18:38,000 --> 00:18:43,000
 If you have, not very carefully, I think.

292
00:18:43,000 --> 00:18:52,000
 You don't have to sit full lotus. Not important.

293
00:18:52,000 --> 00:18:54,860
 I read a story called "The Golden Pagoda, The Silver Pagoda

294
00:18:54,860 --> 00:18:58,000
, and the Pile of Man."

295
00:18:58,000 --> 00:19:01,560
 Well, since I'm not a Zen Buddhist, I can't tell you

296
00:19:01,560 --> 00:19:05,000
 whether that's a koa in a month.

297
00:19:05,000 --> 00:19:10,000
 I think you got the wrong monk.

298
00:19:10,000 --> 00:19:13,000
 Why when I meditate I find everything funny?

299
00:19:13,000 --> 00:19:16,000
 Getting back to habits. We have habits.

300
00:19:16,000 --> 00:19:19,000
 Sounds like this is a habit that you have.

301
00:19:19,000 --> 00:19:23,000
 Sometimes habits that are deep, it's not a normal habit,

302
00:19:23,000 --> 00:19:28,090
 but when you get into a concentrated state, it pokes its

303
00:19:28,090 --> 00:19:30,000
 head up.

304
00:19:30,000 --> 00:19:33,000
 But not everyone finds everything funny when they meditate.

305
00:19:33,000 --> 00:19:39,000
 It is common. It's common to find things more humorous.

306
00:19:39,000 --> 00:19:43,000
 It's sort of along the process of letting go,

307
00:19:43,000 --> 00:19:46,160
 because humor has to do with stopping to take things so

308
00:19:46,160 --> 00:19:47,000
 seriously.

309
00:19:47,000 --> 00:19:50,980
 So for some reason, people along the way, that means

310
00:19:50,980 --> 00:19:53,000
 finding them funny.

311
00:19:54,000 --> 00:20:11,000
 [ Pause ]

312
00:20:11,000 --> 00:20:15,000
 Too many orange people tonight.

313
00:20:15,000 --> 00:19:57,680
 If you're wondering what orange means, if your name is an

314
00:19:57,680 --> 00:20:18,000
 orange,

315
00:20:18,000 --> 00:20:21,000
 it means you haven't meditated with this.

316
00:20:21,000 --> 00:20:27,000
 Over half of the people are orange.

317
00:20:27,000 --> 00:20:31,000
 So I think we're going to go and then say goodnight,

318
00:20:31,000 --> 00:20:35,000
 and go recharge my batteries.

319
00:20:35,000 --> 00:20:43,000
 Most of you all didn't meditate with us.

320
00:20:43,000 --> 00:20:49,000
 So this is a meditation group, meditation community.

321
00:20:49,000 --> 00:20:52,000
 If you come here, you should be meditating with us.

322
00:20:52,000 --> 00:20:55,750
 Some of you I know meditate at different times. That's fine

323
00:20:55,750 --> 00:20:56,000
.

324
00:20:56,000 --> 00:21:01,000
 Usually most people who join here

325
00:21:01,000 --> 00:21:07,000
 will have been meditating like the hour before the talk.

326
00:21:07,000 --> 00:21:10,170
 It would be good if we're all meditating together

327
00:21:10,170 --> 00:21:11,000
 beforehand,

328
00:21:11,000 --> 00:21:18,000
 and the talk is more beneficial.

329
00:21:18,000 --> 00:21:20,000
 Anyway, thank you all for tuning in.

330
00:21:20,000 --> 00:21:23,000
 Apologies for the mix up, mess up.

331
00:21:23,000 --> 00:21:26,000
 We'll try and get it right next time.

332
00:21:26,000 --> 00:21:29,000
 Goodnight.

333
00:21:29,000 --> 00:21:42,000
 The battery showed that it was mostly full.

