1
00:00:00,000 --> 00:00:05,400
 Hi and welcome back to our study of the Dhammapada.

2
00:00:05,400 --> 00:00:09,990
 Today we continue on with verse number 62, which reads as

3
00:00:09,990 --> 00:00:13,000
 follows.

4
00:00:13,000 --> 00:00:23,000
 Puta mati, danam mati, iti balo vihan meti, ata hi, ata no

5
00:00:23,000 --> 00:00:28,440
 nati, kutto puta kutto danang,

6
00:00:28,440 --> 00:00:34,040
 which means.

7
00:00:34,040 --> 00:00:43,230
 Puta mati, I have sons, danam mati, I have wealth, iti balo

8
00:00:43,230 --> 00:00:48,200
 vihan meti, thus the fool

9
00:00:48,200 --> 00:00:54,990
 worries himself or his back story is caught up, is

10
00:00:54,990 --> 00:01:01,240
 concerned, is distraught by these thoughts.

11
00:01:01,240 --> 00:01:09,560
 Ata hi, ata no nati, oneself indeed is not oneself.

12
00:01:09,560 --> 00:01:19,010
 Kutto puta kutto kutto danang, how therefore could either

13
00:01:19,010 --> 00:01:24,440
 sons or wealth belong to oneself.

14
00:01:24,440 --> 00:01:29,280
 The story behind the verse, the story goes that there was

15
00:01:29,280 --> 00:01:33,000
 this rich man called Ananda,

16
00:01:33,000 --> 00:01:41,250
 Ananda Sethi, the rich man, Ananda, and he taught his

17
00:01:41,250 --> 00:01:46,080
 children, he lived his life and

18
00:01:46,080 --> 00:01:49,810
 taught his children to not spend anything, to never give,

19
00:01:49,810 --> 00:01:51,600
 he said, "Don't never give

20
00:01:51,600 --> 00:01:58,570
 away anything because wealth is difficult to gain, it's

21
00:01:58,570 --> 00:02:02,640
 hard to hold onto and it can

22
00:02:02,640 --> 00:02:08,060
 vanish at any time so you have to work very, very hard to

23
00:02:08,060 --> 00:02:10,960
 keep it and keep every coin accounted

24
00:02:10,960 --> 00:02:14,520
 for."

25
00:02:14,520 --> 00:02:21,310
 Over his years of cultivating wealth and obsessing over

26
00:02:21,310 --> 00:02:26,160
 wealth, he came to view it as something

27
00:02:26,160 --> 00:02:29,080
 that needed protecting.

28
00:02:29,080 --> 00:02:32,870
 He got so wound up that every coin should be accounted for

29
00:02:32,870 --> 00:02:34,920
 and should never give anything

30
00:02:34,920 --> 00:02:41,750
 to anyone, should never give away or foolishly spend your

31
00:02:41,750 --> 00:02:42,600
 wealth.

32
00:02:42,600 --> 00:02:46,940
 It reminds us of the rich man with the pancakes, but this

33
00:02:46,940 --> 00:02:49,560
 is a different man, it seems to be

34
00:02:49,560 --> 00:02:59,010
 that wealth, there's a common theme in the Pali Canon of

35
00:02:59,010 --> 00:03:02,320
 richness often leading to miserliness,

36
00:03:02,320 --> 00:03:09,200
 which of course you see in modern day as well.

37
00:03:09,200 --> 00:03:13,930
 There are some examples of rich people who are very

38
00:03:13,930 --> 00:03:17,260
 generous and kind, there are many

39
00:03:17,260 --> 00:03:19,820
 rich people who can only think about how they can amass

40
00:03:19,820 --> 00:03:21,520
 more wealth, they can get more and

41
00:03:21,520 --> 00:03:22,520
 more.

42
00:03:22,520 --> 00:03:28,830
 The Buddha said for this reason, even if it were to rain

43
00:03:28,830 --> 00:03:32,840
 gold, you would never get enough

44
00:03:32,840 --> 00:03:36,860
 for even one person.

45
00:03:36,860 --> 00:03:40,620
 So this is a story of Ananda, not much of a story because

46
00:03:40,620 --> 00:03:42,540
 he passed away and left his

47
00:03:42,540 --> 00:03:45,930
 fortune to his son and the story is actually about his

48
00:03:45,930 --> 00:03:48,400
 reincarnation when he was reborn.

49
00:03:48,400 --> 00:03:53,490
 He was reborn as an outcast in a village of, it says around

50
00:03:53,490 --> 00:03:56,080
 a thousand outcast beggars

51
00:03:56,080 --> 00:04:05,830
 or poor unemployed outcasts who were unable of course to

52
00:04:05,830 --> 00:04:10,720
 get any but the worst of jobs

53
00:04:10,720 --> 00:04:16,000
 and had to live either off begging or off slavery and slave

54
00:04:16,000 --> 00:04:17,440
 labor, etc.

55
00:04:17,440 --> 00:04:20,550
 Now from the moment that he was conceived in his mother's

56
00:04:20,550 --> 00:04:22,200
 womb, the entire village was

57
00:04:22,200 --> 00:04:30,840
 unable to get anything, to make even the slightest bit of

58
00:04:30,840 --> 00:04:33,440
 money or food.

59
00:04:33,440 --> 00:04:36,720
 They weren't able to beg, they weren't able to work.

60
00:04:36,720 --> 00:04:39,950
 From the moment that he was conceived, he cursed the whole

61
00:04:39,950 --> 00:04:40,680
 village.

62
00:04:40,680 --> 00:04:44,440
 That's all the story, so the story goes.

63
00:04:44,440 --> 00:04:49,800
 Some kind of group karma at work I guess.

64
00:04:49,800 --> 00:04:54,960
 And so they split the village, they got together and they

65
00:04:54,960 --> 00:04:58,360
 said something must be wrong here,

66
00:04:58,360 --> 00:04:59,360
 someone must be causing this.

67
00:04:59,360 --> 00:05:04,750
 And so they split the village in half and had them go in

68
00:05:04,750 --> 00:05:07,560
 separate locations and go begging

69
00:05:07,560 --> 00:05:13,530
 or go working or whatever and they found that one half was

70
00:05:13,530 --> 00:05:17,320
 able to get by fine as per normal.

71
00:05:17,320 --> 00:05:20,640
 The other group still was cursed.

72
00:05:20,640 --> 00:05:24,430
 And so they cut that group in half and then cut the other

73
00:05:24,430 --> 00:05:26,400
 group in half and then half

74
00:05:26,400 --> 00:05:29,500
 again until they got down to two families and they split

75
00:05:29,500 --> 00:05:31,000
 them up and they found that

76
00:05:31,000 --> 00:05:34,490
 this woman, this pregnant woman was the cause of all the

77
00:05:34,490 --> 00:05:35,400
 problems.

78
00:05:35,400 --> 00:05:42,320
 So they kicked them out and sent them on their way.

79
00:05:42,320 --> 00:05:47,260
 And then when he was born, his parents found the same thing

80
00:05:47,260 --> 00:05:49,440
, that if they went on alms

81
00:05:49,440 --> 00:05:52,540
 alone or if they went out working or whatever alone, then

82
00:05:52,540 --> 00:05:54,220
 they could get money just fine

83
00:05:54,220 --> 00:05:59,560
 and they could get by as well as could be expected.

84
00:05:59,560 --> 00:06:02,300
 But if he was with them, they would all get nothing.

85
00:06:02,300 --> 00:06:07,100
 So they kicked him out as well and gave him a little piece

86
00:06:07,100 --> 00:06:09,240
 of maybe a piece of pot or

87
00:06:09,240 --> 00:06:15,520
 something and said, "Go, you must, you must prepare for

88
00:06:15,520 --> 00:06:17,160
 yourself."

89
00:06:17,160 --> 00:06:19,270
 They kept him around until he was like seven years old and

90
00:06:19,270 --> 00:06:20,640
 they sent him off and he said,

91
00:06:20,640 --> 00:06:21,640
 "Go by yourself."

92
00:06:21,640 --> 00:06:24,120
 The other thing is he was very, very ugly.

93
00:06:24,120 --> 00:06:26,280
 So he kind of looked like an ogre.

94
00:06:26,280 --> 00:06:35,280
 This is the horrible repercussions of being such a miser.

95
00:06:35,280 --> 00:06:39,120
 And one day he was wandering around trying to get whatever

96
00:06:39,120 --> 00:06:41,240
 alms he could, which of course

97
00:06:41,240 --> 00:06:42,240
 wasn't much.

98
00:06:42,240 --> 00:06:46,040
 He wandered back to his old home and he saw this house and

99
00:06:46,040 --> 00:06:47,840
 he recognized it and so he

100
00:06:47,840 --> 00:06:50,340
 just walked right in.

101
00:06:50,340 --> 00:06:52,370
 And he started looking around the house, not quite sure why

102
00:06:52,370 --> 00:06:53,360
 he was there but seeing that

103
00:06:53,360 --> 00:06:56,320
 it looked very familiar and kind of like home.

104
00:06:56,320 --> 00:06:58,460
 So he was walking around and he went into one room and

105
00:06:58,460 --> 00:06:59,840
 there were his grandchildren,

106
00:06:59,840 --> 00:07:02,520
 his sons' sons.

107
00:07:02,520 --> 00:07:05,140
 And they freaked out and they called out and they said, "

108
00:07:05,140 --> 00:07:06,720
Monster, there's a monster in

109
00:07:06,720 --> 00:07:07,960
 here."

110
00:07:07,960 --> 00:07:11,980
 And the servants came over and beat him and threw him out

111
00:07:11,980 --> 00:07:14,320
 just as the Buddha was walking

112
00:07:14,320 --> 00:07:15,320
 by.

113
00:07:15,320 --> 00:07:19,460
 The Buddha's walking by going on alms around and he sees

114
00:07:19,460 --> 00:07:22,840
 this young beggar beaten to a pulp,

115
00:07:22,840 --> 00:07:27,100
 beaten quite severely, lying on the side of the road, not

116
00:07:27,100 --> 00:07:28,040
 to a pulp.

117
00:07:28,040 --> 00:07:29,040
 He's still alive.

118
00:07:29,040 --> 00:07:32,300
 He's beaten up and the Buddha looks at him and then he

119
00:07:32,300 --> 00:07:34,600
 looks at Ananda, who is our Ananda,

120
00:07:34,600 --> 00:07:37,960
 not the rich Ananda, walking beside him.

121
00:07:37,960 --> 00:07:47,800
 And he turns to look at Ananda and Ananda knows to take

122
00:07:47,800 --> 00:07:52,000
 this as an instigation to ask

123
00:07:52,000 --> 00:07:53,000
 the question.

124
00:07:53,000 --> 00:07:58,060
 So he asked the Buddha, "What's the story of this guy,

125
00:07:58,060 --> 00:07:59,800
 Reverend Sir?"

126
00:07:59,800 --> 00:08:02,580
 And the Buddha said he was to be the great rich man Ananda

127
00:08:02,580 --> 00:08:03,960
 and this was his house and

128
00:08:03,960 --> 00:08:05,960
 this is where he lived.

129
00:08:05,960 --> 00:08:11,140
 So Ananda called the rich man's son and the Buddha

130
00:08:11,140 --> 00:08:16,640
 explained to him, "This was your father."

131
00:08:16,640 --> 00:08:19,150
 And he said, "I don't believe it looks at him and he's this

132
00:08:19,150 --> 00:08:20,080
 ugly outcast."

133
00:08:20,080 --> 00:08:24,040
 And he said, "How can he go from a rich man to a beggar?"

134
00:08:24,040 --> 00:08:27,350
 And the Buddha had him go in the house and find all his

135
00:08:27,350 --> 00:08:29,440
 treasures and the kid was able

136
00:08:29,440 --> 00:08:33,170
 to actually remember where everything was and so he proved

137
00:08:33,170 --> 00:08:33,600
 it.

138
00:08:33,600 --> 00:08:38,200
 And then the Buddha taught this verse, simple story.

139
00:08:38,200 --> 00:08:42,960
 And the point here is to remind us not to be negligent.

140
00:08:42,960 --> 00:08:45,080
 It's a simple lesson.

141
00:08:45,080 --> 00:08:50,250
 And we think of material possessions, all of those things,

142
00:08:50,250 --> 00:08:52,480
 not just puttan, dana, our

143
00:08:52,480 --> 00:08:55,480
 sons and our wealth, but everything.

144
00:08:55,480 --> 00:09:01,810
 Look at all of our belongings, all of our enjoyments in the

145
00:09:01,810 --> 00:09:04,840
 material realm, thinking

146
00:09:04,840 --> 00:09:08,600
 of them as me, as mine, as somehow controllable.

147
00:09:08,600 --> 00:09:13,770
 So we either hold on to them as ours to enjoy or we hold on

148
00:09:13,770 --> 00:09:16,480
 to as ours to control or ours

149
00:09:16,480 --> 00:09:21,400
 to own or ours to dwell in.

150
00:09:21,400 --> 00:09:24,190
 Our body, we think of it as ourselves, our house, we think

151
00:09:24,190 --> 00:09:25,840
 of it as ourselves, our car,

152
00:09:25,840 --> 00:09:27,760
 our bedroom, everything.

153
00:09:27,760 --> 00:09:30,430
 All of our family, we think of them as our family, all of

154
00:09:30,430 --> 00:09:32,000
 our friends, we think of them

155
00:09:32,000 --> 00:09:33,800
 as our friends.

156
00:09:33,800 --> 00:09:40,520
 And so we get caught up in this habit of, or expectation of

157
00:09:40,520 --> 00:09:43,640
 being able to control, of

158
00:09:43,640 --> 00:09:48,270
 being able to rely upon, of being able to enjoy all of

159
00:09:48,270 --> 00:09:49,800
 these things.

160
00:09:49,800 --> 00:09:52,160
 And the Buddha said, "Even yourself is not yourself."

161
00:09:52,160 --> 00:09:54,960
 Atahiyatano nati.

162
00:09:54,960 --> 00:09:57,640
 It's funny, the more common one that people know is atahiy

163
00:09:57,640 --> 00:09:59,280
atano nato, which is, you wonder

164
00:09:59,280 --> 00:10:02,820
 whether one of them is actually a, or the other one is

165
00:10:02,820 --> 00:10:05,000
 actually a, well anyway, the

166
00:10:05,000 --> 00:10:06,000
 Buddha taught both ways.

167
00:10:06,000 --> 00:10:11,390
 Atahiyatano nato self is a refuge of self, which means one

168
00:10:11,390 --> 00:10:13,520
 is one's own refuge.

169
00:10:13,520 --> 00:10:15,280
 But that's referring to the four satipatthana.

170
00:10:15,280 --> 00:10:20,960
 One makes a refuge by practicing on one's own, not relying

171
00:10:20,960 --> 00:10:22,960
 on anyone else.

172
00:10:22,960 --> 00:10:28,120
 But even that one can't control, one can't rely upon one's

173
00:10:28,120 --> 00:10:30,160
 expectations, one can't

174
00:10:30,160 --> 00:10:40,960
 be fulfilled in one's desires and in one's demands.

175
00:10:40,960 --> 00:10:47,960
 So as a result, iti balo yanyati, a fool, is vexed by these

176
00:10:47,960 --> 00:10:49,560
 thoughts.

177
00:10:49,560 --> 00:10:53,010
 They think of their children and their loved ones and all

178
00:10:53,010 --> 00:10:54,840
 of the people in their life as

179
00:10:54,840 --> 00:10:58,140
 being controllable in them and they're not able to control

180
00:10:58,140 --> 00:11:00,000
 these things, control these

181
00:11:00,000 --> 00:11:01,000
 people.

182
00:11:01,000 --> 00:11:03,800
 And then they suffer, they're vexed, they're worried all

183
00:11:03,800 --> 00:11:05,480
 the time, worried about how they

184
00:11:05,480 --> 00:11:08,880
 might be and trying to figure out ways to control people,

185
00:11:08,880 --> 00:11:10,800
 ways to control their family,

186
00:11:10,800 --> 00:11:13,520
 ways to control their friends, ways to control their

187
00:11:13,520 --> 00:11:16,600
 employers, their employees, their co-workers

188
00:11:16,600 --> 00:11:18,880
 and so on and so on.

189
00:11:18,880 --> 00:11:22,760
 How can I control people to be the way I want them to be?

190
00:11:22,760 --> 00:11:25,550
 And the same goes with our dhanana, our possessions, our

191
00:11:25,550 --> 00:11:27,720
 wealth, all of our belongings, guarding

192
00:11:27,720 --> 00:11:32,310
 our houses, guarding our valuables, guarding our

193
00:11:32,310 --> 00:11:36,320
 possessions, guarding our enjoyments,

194
00:11:36,320 --> 00:11:41,580
 guarding our own bodies, being careful to make sure that we

195
00:11:41,580 --> 00:11:44,280
 can always enjoy the pleasure

196
00:11:44,280 --> 00:11:47,080
 that we enjoy now.

197
00:11:47,080 --> 00:11:50,520
 But none of this can be controlled and when it is out of

198
00:11:50,520 --> 00:11:52,520
 our control, we're vexed by

199
00:11:52,520 --> 00:11:53,520
 it.

200
00:11:53,520 --> 00:11:56,310
 Especially foolish people who really believe that these

201
00:11:56,310 --> 00:11:58,400
 things are going to make them happy,

202
00:11:58,400 --> 00:12:02,340
 people who believe in the world bringing happiness, wind up

203
00:12:02,340 --> 00:12:04,200
 being vexed quite often.

204
00:12:04,200 --> 00:12:08,110
 And because of their lack of wisdom they're not able to see

205
00:12:08,110 --> 00:12:10,000
 this and so as a result they

206
00:12:10,000 --> 00:12:14,640
 constantly vexed, they do it again and again and again.

207
00:12:14,640 --> 00:12:17,170
 Doing the same thing over and over again expecting

208
00:12:17,170 --> 00:12:19,600
 different results, forgetting that they are

209
00:12:19,600 --> 00:12:23,320
 constantly upset.

210
00:12:23,320 --> 00:12:26,140
 The Buddha reminds us even ourselves is not, even ourself

211
00:12:26,140 --> 00:12:27,120
 is not ourself.

212
00:12:27,120 --> 00:12:30,520
 What this means is we can't even control ourselves.

213
00:12:30,520 --> 00:12:33,660
 We can make choices, we can have intentions but we can't

214
00:12:33,660 --> 00:12:34,960
 really control it.

215
00:12:34,960 --> 00:12:38,980
 We can't say I'm going to be happy all the time or I'm

216
00:12:38,980 --> 00:12:41,560
 going to be calm all the time.

217
00:12:41,560 --> 00:12:44,630
 We can't say that we're never going to feel pain or we're

218
00:12:44,630 --> 00:12:46,160
 not going to get angry or we're

219
00:12:46,160 --> 00:12:47,640
 not going to get upset.

220
00:12:47,640 --> 00:12:51,920
 We can't say this.

221
00:12:51,920 --> 00:12:56,360
 Even about our own minds, even about our own being.

222
00:12:56,360 --> 00:12:58,760
 So how then could we possibly do this?

223
00:12:58,760 --> 00:13:01,840
 How then could we possibly hold on to these things?

224
00:13:01,840 --> 00:13:11,040
 How then could we expect that we're going to enjoy the

225
00:13:11,040 --> 00:13:13,400
 possession of these things forever?

226
00:13:13,400 --> 00:13:16,780
 How could we possibly forget that we're going to lose them

227
00:13:16,780 --> 00:13:17,920
 all when we die?

228
00:13:17,920 --> 00:13:23,360
 This is what Ananda Sethi, this guy, didn't realize.

229
00:13:23,360 --> 00:13:27,980
 And really the most impressive part of this story is how

230
00:13:27,980 --> 00:13:29,920
 quickly he was gone.

231
00:13:29,920 --> 00:13:31,520
 This is what shocked his son.

232
00:13:31,520 --> 00:13:33,440
 He's unable to believe that.

233
00:13:33,440 --> 00:13:34,440
 Just like that.

234
00:13:34,440 --> 00:13:41,040
 In one moment, the moment of death turned him from a rich,

235
00:13:41,040 --> 00:13:44,600
 powerful, influential person

236
00:13:44,600 --> 00:13:50,730
 to a nobody, to the lowest of the low, an outcast of the

237
00:13:50,730 --> 00:13:52,160
 outcast.

238
00:13:52,160 --> 00:13:53,160
 But that's the truth.

239
00:13:53,160 --> 00:13:55,080
 That's what it means by it's not self.

240
00:13:55,080 --> 00:13:59,320
 You can't even hold on to your own being, your own status.

241
00:13:59,320 --> 00:14:00,320
 I think I'm a monk.

242
00:14:00,320 --> 00:14:02,920
 The moment I die, that's gone.

243
00:14:02,920 --> 00:14:05,620
 You think you're a human being, the moment you die, that's

244
00:14:05,620 --> 00:14:06,080
 gone.

245
00:14:06,080 --> 00:14:10,060
 You can be, the next moment you can be an earthworm or a d

246
00:14:10,060 --> 00:14:11,240
ung beetle.

247
00:14:11,240 --> 00:14:15,200
 Everything that we collect, everything that we hold on to,

248
00:14:15,200 --> 00:14:16,720
 no security for us.

249
00:14:16,720 --> 00:14:19,070
 All these people in some cultures, they will bury their

250
00:14:19,070 --> 00:14:20,200
 belongings with them.

251
00:14:20,200 --> 00:14:24,560
 Thinking that they can bring it into their next life or

252
00:14:24,560 --> 00:14:27,040
 they'll burn certain objects.

253
00:14:27,040 --> 00:14:32,310
 In Thailand and China, they burn these really crappy houses

254
00:14:32,310 --> 00:14:35,000
 and fake stuff and everything.

255
00:14:35,000 --> 00:14:36,670
 And you think, "Wow, they're going to get a lot of fake

256
00:14:36,670 --> 00:14:37,760
 stuff when they go to heaven."

257
00:14:37,760 --> 00:14:39,930
 The idea is that they can take that stuff with them if they

258
00:14:39,930 --> 00:14:40,240
 burn it.

259
00:14:40,240 --> 00:14:43,210
 And since it's all garbage, you think, "Well, it's not

260
00:14:43,210 --> 00:14:45,000
 really, even if it did work."

261
00:14:45,000 --> 00:14:47,120
 And of course it doesn't work.

262
00:14:47,120 --> 00:14:49,640
 You can't possess, we don't possess anything.

263
00:14:49,640 --> 00:14:52,050
 We just happen to be going in the same direction as all

264
00:14:52,050 --> 00:14:52,760
 this stuff.

265
00:14:52,760 --> 00:14:57,680
 We happen to be in the same physical locale.

266
00:14:57,680 --> 00:15:01,340
 We manage to make choices that bring all this stuff near us

267
00:15:01,340 --> 00:15:02,320
 and that's it.

268
00:15:02,320 --> 00:15:03,640
 We can't do any better than that.

269
00:15:03,640 --> 00:15:05,640
 We can't control.

270
00:15:05,640 --> 00:15:10,930
 Even our enjoyment is fleeting and ephemeral, something

271
00:15:10,930 --> 00:15:13,440
 that cannot be relied upon.

272
00:15:13,440 --> 00:15:17,750
 So it's very much out of our control based on causes and

273
00:15:17,750 --> 00:15:20,120
 conditions that are far more

274
00:15:20,120 --> 00:15:22,120
 powerful than any one of us.

275
00:15:22,120 --> 00:15:25,560
 So a reminder for us.

276
00:15:25,560 --> 00:15:29,120
 And in regards to meditation, a reminder for us to keep our

277
00:15:29,120 --> 00:15:30,800
 minds clear and to be clear

278
00:15:30,800 --> 00:15:32,000
 about all the things that we use.

279
00:15:32,000 --> 00:15:35,080
 It doesn't mean that we shouldn't be using our possessions.

280
00:15:35,080 --> 00:15:41,800
 It doesn't mean we shouldn't live.

281
00:15:41,800 --> 00:15:46,820
 It means that when we do experience even pleasure or when

282
00:15:46,820 --> 00:15:50,040
 you're eating food, the pleasure

283
00:15:50,040 --> 00:15:54,640
 of the food, when you're seeing beautiful things, the

284
00:15:54,640 --> 00:15:57,640
 pleasure that comes from that.

285
00:15:57,640 --> 00:16:00,360
 But we should see it simply as a feeling.

286
00:16:00,360 --> 00:16:02,280
 We should see the experience simply as an experience.

287
00:16:02,280 --> 00:16:06,700
 The experience is seeing, hearing, smelling, tasting,

288
00:16:06,700 --> 00:16:08,480
 feeling, thinking.

289
00:16:08,480 --> 00:16:13,120
 See liking as liking, see disliking as disliking.

290
00:16:13,120 --> 00:16:15,340
 Come to see that these things are not self, are not under

291
00:16:15,340 --> 00:16:16,080
 our control.

292
00:16:16,080 --> 00:16:20,180
 Try to keep our minds clear because all of this will catch

293
00:16:20,180 --> 00:16:21,640
 you when you die.

294
00:16:21,640 --> 00:16:25,470
 And if you have great wanting, so someone who is very, very

295
00:16:25,470 --> 00:16:27,560
 rich because they're clinging

296
00:16:27,560 --> 00:16:30,640
 so much, what goes with them is the clinging.

297
00:16:30,640 --> 00:16:32,280
 And that's why you're born very, very poor.

298
00:16:32,280 --> 00:16:35,160
 If you are very, very rich people are very quick to be born

299
00:16:35,160 --> 00:16:36,640
 very poor because they have

300
00:16:36,640 --> 00:16:38,760
 so much greed and attachment.

301
00:16:38,760 --> 00:16:41,600
 And it's that want that goes with you.

302
00:16:41,600 --> 00:16:42,600
 So you're born wanting.

303
00:16:42,600 --> 00:16:46,000
 You're born in a constant state of want.

304
00:16:46,000 --> 00:16:48,000
 Amassing wealth doesn't make you wealthy.

305
00:16:48,000 --> 00:16:53,960
 Being generous and kind and having a mind of surplus.

306
00:16:53,960 --> 00:16:57,180
 When you think you have surplus, when you're content with

307
00:16:57,180 --> 00:16:59,000
 what you have, then you'll always

308
00:16:59,000 --> 00:17:00,000
 be content.

309
00:17:00,000 --> 00:17:03,120
 You'll never know the words, "Nati."

310
00:17:03,120 --> 00:17:05,000
 When you want something, you'll never be discontent.

311
00:17:05,000 --> 00:17:07,520
 You'll never know discontent because your mind is content.

312
00:17:07,520 --> 00:17:13,760
 It's funny how it works that way.

313
00:17:13,760 --> 00:17:17,520
 The mind that is clinging is the mind that is corrupt.

314
00:17:17,520 --> 00:17:20,590
 Clinging to self is the worst type of corruption because it

315
00:17:20,590 --> 00:17:22,840
 leads to all other likes and dislikes.

316
00:17:22,840 --> 00:17:25,440
 So we have to keep our minds clear.

317
00:17:25,440 --> 00:17:26,880
 This is why meditation is important.

318
00:17:26,880 --> 00:17:29,810
 When we live our lives, however you live your lives, to

319
00:17:29,810 --> 00:17:31,360
 keep your minds clear and to not

320
00:17:31,360 --> 00:17:35,230
 cling to self either in regards to your own self or in

321
00:17:35,230 --> 00:17:37,520
 regards to the things that you

322
00:17:37,520 --> 00:17:38,520
 enjoy.

323
00:17:38,520 --> 00:17:43,720
 So a nice lesson from the Dhammapada and from our friend,

324
00:17:43,720 --> 00:17:47,440
 the former Ananda Sethi, who managed

325
00:17:47,440 --> 00:17:50,240
 to find his way out of that mess.

326
00:17:50,240 --> 00:17:53,500
 So thank you all for tuning in and I wish you all peace,

327
00:17:53,500 --> 00:17:55,800
 happiness and freedom from suffering.

328
00:17:55,800 --> 00:17:56,680
 See you next time.

