1
00:00:00,000 --> 00:00:14,160
 Good evening everyone.

2
00:00:14,160 --> 00:00:37,080
 Welcome to our evening dum.

3
00:00:37,080 --> 00:00:49,040
 So I wanted to take tonight to express a sort of

4
00:00:49,040 --> 00:00:54,360
 appreciation for everyone,

5
00:00:54,360 --> 00:01:02,400
 for all of our meditation community, for all of you people

6
00:01:02,400 --> 00:01:03,600
 out there interested in

7
00:01:03,600 --> 00:01:14,190
 Buddhism, who have taken up study and practice of this

8
00:01:14,190 --> 00:01:19,720
 incredible, wonderful, marvelous,

9
00:01:19,720 --> 00:01:39,720
 miraculous set of teachings and practices.

10
00:01:39,720 --> 00:01:53,910
 And just to remark upon how special this moment is, for all

11
00:01:53,910 --> 00:01:56,720
 of us, how to remind us one and

12
00:01:56,720 --> 00:02:07,790
 all of what it's taken for us to get here, and how special

13
00:02:07,790 --> 00:02:11,440
 is this moment.

14
00:02:11,440 --> 00:02:15,800
 It's like one of those, this is like one of those grand

15
00:02:15,800 --> 00:02:18,760
 events, like the inauguration

16
00:02:18,760 --> 00:02:34,010
 of a great monument or some once in a lifetime experience

17
00:02:34,010 --> 00:02:45,080
 or event.

18
00:02:45,080 --> 00:02:53,220
 The Buddha said, "Ganoma upachaga, don't let the moment

19
00:02:53,220 --> 00:02:55,480
 pass you by."

20
00:02:55,480 --> 00:03:01,090
 That's because this moment is special, and it's a moment in

21
00:03:01,090 --> 00:03:03,680
 time that has a great significance

22
00:03:03,680 --> 00:03:10,050
 to all of us, has great meaning and great potential, if we

23
00:03:10,050 --> 00:03:12,560
 take advantage of it, if

24
00:03:12,560 --> 00:03:18,970
 we take this opportunity, if we don't squander our time, if

25
00:03:18,970 --> 00:03:22,080
 we don't allow ourselves to become

26
00:03:22,080 --> 00:03:29,950
 distracted and diverted and caught up in the blinding power

27
00:03:29,950 --> 00:03:32,400
 of defilement.

28
00:03:32,400 --> 00:03:40,480
 We have great potential in this moment.

29
00:03:40,480 --> 00:03:48,170
 And conventionally, before you even talk about the power of

30
00:03:48,170 --> 00:03:52,040
 now, what it took for us to get

31
00:03:52,040 --> 00:03:58,240
 here today is quite impressive.

32
00:03:58,240 --> 00:04:05,810
 The first thing that's special about this moment is that it

33
00:04:05,810 --> 00:04:09,440
's the time of the Buddha.

34
00:04:09,440 --> 00:04:15,010
 This is a time when we have the teachings of a fully

35
00:04:15,010 --> 00:04:18,840
 enlightened Buddha, which for those

36
00:04:18,840 --> 00:04:21,980
 of you new to the study of Buddhism, it may not seem all

37
00:04:21,980 --> 00:04:23,960
 that significant, but for those

38
00:04:23,960 --> 00:04:27,730
 of us who have studied deeply or practiced deeply, the

39
00:04:27,730 --> 00:04:30,040
 teachings of the Buddha, it has

40
00:04:30,040 --> 00:04:37,680
 a profound significance.

41
00:04:37,680 --> 00:04:47,100
 This is just some ordinary teaching, nagamadomo nigamasad

42
00:04:47,100 --> 00:04:48,240
amo.

43
00:04:48,240 --> 00:04:56,410
 This is not the teaching of one village or city or one

44
00:04:56,410 --> 00:04:59,320
 civilization.

45
00:04:59,320 --> 00:05:02,240
 This is the Dhamma of the whole world.

46
00:05:02,240 --> 00:05:08,750
 This is the universal teachings of truth, and they're not

47
00:05:08,750 --> 00:05:10,560
 always here.

48
00:05:10,560 --> 00:05:14,520
 They're not always available.

49
00:05:14,520 --> 00:05:19,510
 In fact, the vast majority of time spent in samsara is in

50
00:05:19,510 --> 00:05:22,160
 situations where there is no

51
00:05:22,160 --> 00:05:26,600
 Buddha.

52
00:05:26,600 --> 00:05:37,680
 There are world periods without the arising of a Buddha.

53
00:05:37,680 --> 00:05:41,310
 Just as there are places where no one has ever heard of the

54
00:05:41,310 --> 00:05:43,400
 Buddha, even when the Buddha

55
00:05:43,400 --> 00:05:47,150
 was alive, if you were born at that time, but you were born

56
00:05:47,150 --> 00:05:48,880
 in Canada, or if you were

57
00:05:48,880 --> 00:05:57,280
 born anywhere but India, chances are you'd never hear of

58
00:05:57,280 --> 00:06:01,560
 let alone meet the Buddha.

59
00:06:01,560 --> 00:06:06,930
 But we're born in a time when even Canada has heard of the

60
00:06:06,930 --> 00:06:09,760
 Buddha's teaching, it has

61
00:06:09,760 --> 00:06:10,760
 spread.

62
00:06:10,760 --> 00:06:13,980
 In a sense, we're luckier as Canadians to be born today

63
00:06:13,980 --> 00:06:16,280
 than to have been born 2,500

64
00:06:16,280 --> 00:06:19,880
 years ago.

65
00:06:19,880 --> 00:06:24,400
 Canada wouldn't have been a good place to learn Buddhism.

66
00:06:24,400 --> 00:06:33,550
 Not that there weren't civilizations here, it's just they

67
00:06:33,550 --> 00:06:36,960
 weren't Buddhist.

68
00:06:36,960 --> 00:06:39,530
 It takes a Buddha, it's not something that comes up in a

69
00:06:39,530 --> 00:06:41,240
 few days, and it's not something

70
00:06:41,240 --> 00:06:44,440
 you get every year.

71
00:06:44,440 --> 00:06:49,080
 There's a new Buddha coming out, every generation has its

72
00:06:49,080 --> 00:06:51,960
 own Buddha, it's not like that.

73
00:06:51,960 --> 00:06:58,370
 Our Buddha took four asankaya, four uncountable eons it

74
00:06:58,370 --> 00:07:03,080
 took him, plus another hundred thousand

75
00:07:03,080 --> 00:07:08,760
 countable eons, great eons.

76
00:07:08,760 --> 00:07:12,660
 An asankaya is something you can't count, it's this measure

77
00:07:12,660 --> 00:07:14,680
 that ironically you can count

78
00:07:14,680 --> 00:07:17,320
 how many there are.

79
00:07:17,320 --> 00:07:19,950
 Somehow you can count that there are four asankaya, but you

80
00:07:19,950 --> 00:07:21,040
 can't count how long an

81
00:07:21,040 --> 00:07:23,080
 asankaya is.

82
00:07:23,080 --> 00:07:25,840
 So it must be marked by something, at the end of an asank

83
00:07:25,840 --> 00:07:27,200
aya something changes.

84
00:07:27,200 --> 00:07:30,520
 I'm not up on all this, I don't quite know.

85
00:07:30,520 --> 00:07:43,720
 And it's a huge unfathomable amount of time.

86
00:07:43,720 --> 00:07:50,110
 And we had to do all sorts of powerful acts and sacrifices,

87
00:07:50,110 --> 00:07:53,200
 giving up his eyes, giving

88
00:07:53,200 --> 00:08:04,170
 up his life, renouncing the world, renouncing his family,

89
00:08:04,170 --> 00:08:09,560
 being killed and tortured, walking

90
00:08:09,560 --> 00:08:16,680
 through fire to become a Buddha.

91
00:08:16,680 --> 00:08:19,970
 And so here we have in the time of a very special

92
00:08:19,970 --> 00:08:22,880
 individual whose teachings have come

93
00:08:22,880 --> 00:08:32,950
 through the veil of darkness that has kept us confused and

94
00:08:32,950 --> 00:08:34,080
 unseen.

95
00:08:34,080 --> 00:08:41,220
 It's a very rare opportunity, that's the first aspect, the

96
00:08:41,220 --> 00:08:43,920
 first reason why this is

97
00:08:43,920 --> 00:08:50,360
 a special moment.

98
00:08:50,360 --> 00:08:52,820
 The second reason is that well, not only are we born in the

99
00:08:52,820 --> 00:08:54,080
 time of the Buddha, but we're

100
00:08:54,080 --> 00:08:56,200
 also born as humans.

101
00:08:56,200 --> 00:09:01,260
 So there are lots of beings around that will never

102
00:09:01,260 --> 00:09:05,440
 understand the Buddha's teachings.

103
00:09:05,440 --> 00:09:14,940
 We could have been born quite easily as a deer or a cat, a

104
00:09:14,940 --> 00:09:19,840
 dog, a mosquito, a worm.

105
00:09:19,840 --> 00:09:22,520
 Could have been born as many different things.

106
00:09:22,520 --> 00:09:24,580
 We could have been born as a Brahma and never met with a

107
00:09:24,580 --> 00:09:25,880
 Buddha because we were lost in

108
00:09:25,880 --> 00:09:32,340
 the Brahma realms or never had the opportunity to become

109
00:09:32,340 --> 00:09:34,360
 enlightened.

110
00:09:34,360 --> 00:09:39,800
 Being born as a human or as an angel is a very rare thing.

111
00:09:39,800 --> 00:09:43,280
 To just rise up out of the ordinary animal realm is a very

112
00:09:43,280 --> 00:09:44,280
 rare thing.

113
00:09:44,280 --> 00:09:48,120
 Right now we think it's quite common because there are

114
00:09:48,120 --> 00:09:50,400
 seven or eight billion of us.

115
00:09:50,400 --> 00:09:53,460
 We could argue that that's due to the goodness of teachings

116
00:09:53,460 --> 00:09:55,200
 like the Buddha that's allowed

117
00:09:55,200 --> 00:10:02,760
 us to prosper, but even seven or eight billion isn't all

118
00:10:02,760 --> 00:10:04,600
 that much.

119
00:10:04,600 --> 00:10:11,040
 You compare it to the number of animals, the number of ants

120
00:10:11,040 --> 00:10:13,520
 and mosquitoes, and the number

121
00:10:13,520 --> 00:10:20,530
 of other beings, and the beings in hell that are probably

122
00:10:20,530 --> 00:10:22,360
 countless.

123
00:10:22,360 --> 00:10:26,480
 The ability to be born as a human being is very rare.

124
00:10:26,480 --> 00:10:28,320
 We've achieved it.

125
00:10:28,320 --> 00:10:32,850
 We've made it out of the depths of despair and suffering of

126
00:10:32,850 --> 00:10:35,280
 the animal realms, the lower

127
00:10:35,280 --> 00:10:39,120
 realm.

128
00:10:39,120 --> 00:10:51,310
 Third, we are in a position to practice the Buddha's

129
00:10:51,310 --> 00:10:54,720
 teachings.

130
00:10:54,720 --> 00:10:57,750
 Many people who are caught up, there's lots of people who

131
00:10:57,750 --> 00:10:59,520
 even want to practice Buddhism

132
00:10:59,520 --> 00:11:02,720
 but don't have the opportunity.

133
00:11:02,720 --> 00:11:05,120
 They're caught up in their lives.

134
00:11:05,120 --> 00:11:08,760
 Perhaps they're sick or in debt.

135
00:11:08,760 --> 00:11:13,590
 Maybe they're married and have children and are in a

136
00:11:13,590 --> 00:11:15,880
 position with young children where

137
00:11:15,880 --> 00:11:21,640
 they can't come to practice meditation.

138
00:11:21,640 --> 00:11:31,880
 Maybe they have a job that doesn't allow them to get away.

139
00:11:31,880 --> 00:11:34,760
 The life of human beings is a difficult life.

140
00:11:34,760 --> 00:11:38,250
 I know of people, I've talked to people who are in a place

141
00:11:38,250 --> 00:11:40,000
 where there is no Buddhism

142
00:11:40,000 --> 00:11:44,550
 and there's no chance to go to a Buddhist monastery or to

143
00:11:44,550 --> 00:11:46,400
 learn meditation.

144
00:11:46,400 --> 00:11:49,760
 It's great now that we have these online courses, we have

145
00:11:49,760 --> 00:11:52,080
 people coming and doing at least a

146
00:11:52,080 --> 00:11:57,070
 non-intensive practice, enough to give them courage and

147
00:11:57,070 --> 00:11:59,920
 give them hope and tide them over

148
00:11:59,920 --> 00:12:04,200
 while they cultivate the necessary conditions to actually

149
00:12:04,200 --> 00:12:05,880
 come and do a course.

150
00:12:05,880 --> 00:12:08,820
 We've had several people who have done that and eventually

151
00:12:08,820 --> 00:12:10,320
 found a way to come and finish

152
00:12:10,320 --> 00:12:20,120
 the course here.

153
00:12:20,120 --> 00:12:23,370
 The third reason why this is special is we've all come

154
00:12:23,370 --> 00:12:25,160
 together, most special for our resident

155
00:12:25,160 --> 00:12:30,200
 meditators who have done this incredible thing to take time

156
00:12:30,200 --> 00:12:32,840
 out of an ordinarily busy life

157
00:12:32,840 --> 00:12:43,460
 and dedicate 20, 30 days straight to just training the mind

158
00:12:43,460 --> 00:12:47,560
, the pure unadulterated

159
00:12:47,560 --> 00:13:04,760
 goodness.

160
00:13:04,760 --> 00:13:07,520
 The third reason is that we've got the opportunity and the

161
00:13:07,520 --> 00:13:09,480
 fourth reason is that we've actually

162
00:13:09,480 --> 00:13:12,600
 taken the opportunity.

163
00:13:12,600 --> 00:13:15,160
 This is what it means by not letting the moment pass you by

164
00:13:15,160 --> 00:13:15,360
.

165
00:13:15,360 --> 00:13:20,900
 The Buddha said there are many people, there are a few

166
00:13:20,900 --> 00:13:24,600
 people in this world who even think

167
00:13:24,600 --> 00:13:28,980
 to do things like meditation, who are moved by things that

168
00:13:28,980 --> 00:13:30,900
 they should be moved by.

169
00:13:30,900 --> 00:13:33,470
 Most people aren't even moved when they think of death,

170
00:13:33,470 --> 00:13:34,960
 when they think of sickness.

171
00:13:34,960 --> 00:13:39,050
 They can't understand why we don't just engage and indulge

172
00:13:39,050 --> 00:13:40,940
 in sensual pleasures.

173
00:13:40,940 --> 00:13:43,400
 They can't understand why one would come here and do a

174
00:13:43,400 --> 00:13:45,240
 meditation course, would take time

175
00:13:45,240 --> 00:13:48,330
 out of their lives or try to change their lives, strive to

176
00:13:48,330 --> 00:13:50,040
 free themselves from craving

177
00:13:50,040 --> 00:13:53,760
 when there's so much pleasure to be had out there.

178
00:13:53,760 --> 00:13:58,780
 They hear about things like old age sickness, death, they

179
00:13:58,780 --> 00:14:01,400
 look at the anger and the greed

180
00:14:01,400 --> 00:14:03,760
 and the delusion that they're building inside and they aren

181
00:14:03,760 --> 00:14:04,720
't threatened by it.

182
00:14:04,720 --> 00:14:06,920
 They don't feel disturbed by it.

183
00:14:06,920 --> 00:14:11,520
 There are people like that.

184
00:14:11,520 --> 00:14:15,600
 If they feel disturbed, they ignore it.

185
00:14:15,600 --> 00:14:19,320
 They don't allow themselves to become moved.

186
00:14:19,320 --> 00:14:24,310
 Very few are the people who actually discern this

187
00:14:24,310 --> 00:14:28,360
 precarious state for what it is, that

188
00:14:28,360 --> 00:14:31,630
 at any moment things could change and we could be dumped

189
00:14:31,630 --> 00:14:34,040
 headlong into suffering for a variety

190
00:14:34,040 --> 00:14:39,200
 of reasons and in fact will be as life changes.

191
00:14:39,200 --> 00:14:42,540
 Very few people actually see the suffering for what it is,

192
00:14:42,540 --> 00:14:44,240
 see how much stress is involved

193
00:14:44,240 --> 00:14:50,560
 in chasing after the objects of our desires.

194
00:14:50,560 --> 00:14:55,990
 People see this but among those people who see these

195
00:14:55,990 --> 00:14:59,480
 problematic aspects of ordinary

196
00:14:59,480 --> 00:15:04,470
 life, very few are those who actually do something about it

197
00:15:04,470 --> 00:15:04,800
.

198
00:15:04,800 --> 00:15:11,590
 So it's worth feeling good about, taking time to reflect

199
00:15:11,590 --> 00:15:15,160
 how far we've come and be encouraged

200
00:15:15,160 --> 00:15:18,760
 by how special this is and how powerful it is.

201
00:15:18,760 --> 00:15:25,940
 So the real point is that we've come to a position where we

202
00:15:25,940 --> 00:15:29,160
 can take advantage of all

203
00:15:29,160 --> 00:15:39,320
 these supportive, good luck that we've had, all these good

204
00:15:39,320 --> 00:15:45,560
 conditions, favorable conditions

205
00:15:45,560 --> 00:15:51,400
 and make this moment into something special.

206
00:15:51,400 --> 00:15:56,850
 That all these supportive conditions come together to allow

207
00:15:56,850 --> 00:15:59,480
 us, all of them are required

208
00:15:59,480 --> 00:16:07,060
 just for this one simple act of seeing things clearly, vip

209
00:16:07,060 --> 00:16:08,520
assana.

210
00:16:08,520 --> 00:16:11,630
 All these things that are there for everyone else but that

211
00:16:11,630 --> 00:16:13,240
 they never see, right under

212
00:16:13,240 --> 00:16:16,390
 our noses, right here in front of us but that we never see

213
00:16:16,390 --> 00:16:17,160
 clearly.

214
00:16:17,160 --> 00:16:21,110
 They're all there but we can't see them, we don't have the

215
00:16:21,110 --> 00:16:23,120
 supportive conditions.

216
00:16:23,120 --> 00:16:28,050
 Well now we've come together and we have the supportive

217
00:16:28,050 --> 00:16:29,400
 condition.

218
00:16:29,400 --> 00:16:34,330
 We have the opportunity to see body as body, feelings as

219
00:16:34,330 --> 00:16:37,400
 feelings, thoughts as thoughts,

220
00:16:37,400 --> 00:16:45,600
 emotions as emotions, senses as senses.

221
00:16:45,600 --> 00:16:48,670
 To see impermanence, to see suffering, to see non-self, to

222
00:16:48,670 --> 00:16:50,120
 free ourselves from craving

223
00:16:50,120 --> 00:16:55,450
 and clinging to things that can't possibly satisfy us, to

224
00:16:55,450 --> 00:16:58,040
 strive for liberation and a

225
00:16:58,040 --> 00:17:04,620
 state of mind that is unsullied by the ocean or the defile

226
00:17:04,620 --> 00:17:06,880
ment of samsara.

227
00:17:06,880 --> 00:17:13,530
 So it's quite a special opportunity that we have is what I

228
00:17:13,530 --> 00:17:15,880
'm trying to say.

229
00:17:15,880 --> 00:17:20,480
 I like to take this tonight to do a sort of appreciation

230
00:17:20,480 --> 00:17:22,840
 and to offer this as a sort of

231
00:17:22,840 --> 00:17:23,840
 dhamma.

232
00:17:23,840 --> 00:17:26,520
 That the most important thing is the moment.

233
00:17:26,520 --> 00:17:28,880
 Don't let the moment pass you by.

234
00:17:28,880 --> 00:17:32,440
 There's a lot that's gone into this moment.

235
00:17:32,440 --> 00:17:38,840
 This moment in time.

236
00:17:38,840 --> 00:17:42,320
 So care for it.

237
00:17:42,320 --> 00:17:43,320
 Care for it.

238
00:17:43,320 --> 00:17:48,870
 I think in the Missudimaga it talks about a person who is

239
00:17:48,870 --> 00:17:51,560
 rocking a baby and in the olden

240
00:17:51,560 --> 00:17:55,360
 times they would have a baby cradle on a string.

241
00:17:55,360 --> 00:17:57,120
 Maybe even nowadays they do this as well.

242
00:17:57,120 --> 00:18:00,340
 So you have to pull the string and you have to be very

243
00:18:00,340 --> 00:18:02,720
 careful to keep the string going.

244
00:18:02,720 --> 00:18:06,120
 Or the baby will wake up.

245
00:18:06,120 --> 00:18:13,320
 Our practice has to be cared for and carefully tended to

246
00:18:13,320 --> 00:18:17,320
 like a sleeping baby to keep it

247
00:18:17,320 --> 00:18:21,320
 on kilter.

248
00:18:21,320 --> 00:18:26,440
 To keep it online and in line and progressing smoothly.

249
00:18:26,440 --> 00:18:33,110
 To constantly be cautious and careful like a sick person

250
00:18:33,110 --> 00:18:36,280
 looking after their pain in

251
00:18:36,280 --> 00:18:37,280
 their body.

252
00:18:37,280 --> 00:18:40,970
 Not that we have to feel sick or it should be sickening or

253
00:18:40,970 --> 00:18:41,960
 anything.

254
00:18:41,960 --> 00:18:44,960
 But we should think of ourselves as someone who has to take

255
00:18:44,960 --> 00:18:46,440
 care of something because

256
00:18:46,440 --> 00:18:47,760
 the potential for suffering.

257
00:18:47,760 --> 00:18:50,700
 So a sick person, if they're not careful with how they move

258
00:18:50,700 --> 00:18:52,200
 their body they'll feel great

259
00:18:52,200 --> 00:18:54,200
 pain.

260
00:18:54,200 --> 00:18:59,590
 Likewise a meditator in moving their body and speaking and

261
00:18:59,590 --> 00:19:02,200
 everything they do and even

262
00:19:02,200 --> 00:19:03,200
 in their thinking.

263
00:19:03,200 --> 00:19:07,840
 You have to be careful to not go off track or they'll lose

264
00:19:07,840 --> 00:19:09,840
 their equilibrium.

265
00:19:09,840 --> 00:19:16,760
 They'll lose their direction.

266
00:19:16,760 --> 00:19:18,240
 Treat this moment with care.

267
00:19:18,240 --> 00:19:21,040
 It's very valuable.

268
00:19:21,040 --> 00:19:22,040
 Priceless.

269
00:19:22,040 --> 00:19:25,560
 So there you go.

270
00:19:25,560 --> 00:19:28,040
 There's the bit of dhamma for tonight.

271
00:19:28,040 --> 00:19:31,000
 Thank you all for coming out.

272
00:19:31,000 --> 00:19:56,600
 I'm happy to take questions if there are any.

273
00:19:56,600 --> 00:20:11,200
 Thank you.

274
00:20:11,200 --> 00:20:32,000
 Thank you.

275
00:20:32,000 --> 00:20:47,900
 The reason one cannot be enlightened as a brahma in the bra

276
00:20:47,900 --> 00:20:49,400
hma realms is that because

277
00:20:49,400 --> 00:20:52,200
 there one cannot take the force at Deepatthana.

278
00:20:52,200 --> 00:20:57,360
 Why are you planning on going to the brahma realms?

279
00:20:57,360 --> 00:21:01,330
 As I understand, yes some of the brahma realms one cannot

280
00:21:01,330 --> 00:21:02,640
 become enlightened.

281
00:21:02,640 --> 00:21:06,240
 I'm not quite clear why.

282
00:21:06,240 --> 00:21:10,170
 But I think Mahasya Sayadaw says it's something about how

283
00:21:10,170 --> 00:21:12,800
 they aren't able to see impermanence

284
00:21:12,800 --> 00:21:22,200
 because it's too stable.

285
00:21:22,200 --> 00:21:28,440
 You have to know curious, curious, wondering, wondering.

286
00:21:28,440 --> 00:21:36,120
 That wasn't the point of my talk.

287
00:21:36,120 --> 00:21:37,640
 Such a terrible teacher, no?

288
00:21:37,640 --> 00:21:41,080
 Won't answer your questions.

289
00:21:41,080 --> 00:21:54,280
 I did kinda.

290
00:21:54,280 --> 00:22:15,200
 Yeah, right.

291
00:22:15,200 --> 00:22:22,700
 It might just be the Arupa Brahma realms that you can't

292
00:22:22,700 --> 00:22:24,680
 practice.

293
00:22:24,680 --> 00:22:30,340
 Because there are certainly brahma realms where you can

294
00:22:30,340 --> 00:22:33,920
 practice, the Anagami realms.

295
00:22:33,920 --> 00:22:39,070
 The Arupa Brahma realms are like real God states where you

296
00:22:39,070 --> 00:22:41,480
're totally out of it, totally

297
00:22:41,480 --> 00:22:46,720
 entranced with no form, only mind.

298
00:22:46,720 --> 00:22:48,160
 Think.

299
00:22:48,160 --> 00:23:10,060
 All right, well if there are no more questions, we'll go

300
00:23:10,060 --> 00:23:14,260
 back to the

301
00:23:14,260 --> 00:23:25,280
 next one.

302
00:23:25,280 --> 00:23:30,640
 Thanks.

