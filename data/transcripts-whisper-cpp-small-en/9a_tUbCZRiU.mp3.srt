1
00:00:00,000 --> 00:00:03,170
 I remember you mentioning that sometimes people view

2
00:00:03,170 --> 00:00:04,440
 themselves as a martyr when

3
00:00:04,440 --> 00:00:07,390
 they go out of the way of helping others such as the poor,

4
00:00:07,390 --> 00:00:09,600
 sick, and homeless. Do

5
00:00:09,600 --> 00:00:11,510
 you think it's important to go out of the way to do these

6
00:00:11,510 --> 00:00:12,480
 things like Christian

7
00:00:12,480 --> 00:00:16,030
 missionaries because it's important to accumulate fields of

8
00:00:16,030 --> 00:00:17,160
 merit? If we don't

9
00:00:17,160 --> 00:00:20,280
 become an arahant, it's better to become a prince in the

10
00:00:20,280 --> 00:00:21,280
 next life rather than a

11
00:00:21,280 --> 00:00:31,290
 beggar. I got a video this evening. If you go on my Google+

12
00:00:31,290 --> 00:00:32,240
 profile, you'll see

13
00:00:32,240 --> 00:00:39,920
 this video that doesn't relate to this quite, but it's a

14
00:00:39,920 --> 00:00:41,200
 video of a beggar.

15
00:00:41,200 --> 00:00:45,190
 It's quite... no, sorry, it actually relates perfectly to

16
00:00:45,190 --> 00:00:47,080
 this question.

17
00:00:47,080 --> 00:00:51,450
 Worth watching that video. I'm not going to tell you what's

18
00:00:51,450 --> 00:00:52,640
 in it. You have to go

19
00:00:52,640 --> 00:00:59,140
 watch it. So is it important? Well, the Buddha did say

20
00:00:59,140 --> 00:01:00,320
 helping others you help

21
00:01:00,320 --> 00:01:04,830
 yourself, helping yourself you help others. I've pointed

22
00:01:04,830 --> 00:01:06,040
 out several times,

23
00:01:06,040 --> 00:01:09,570
 and I think I would just reiterate it, that we are like a

24
00:01:09,570 --> 00:01:11,600
 river. We have to

25
00:01:11,600 --> 00:01:16,050
 purify the source, so you should never forego the practice

26
00:01:16,050 --> 00:01:17,240
 of purifying the

27
00:01:17,240 --> 00:01:21,390
 source and just making the water available. If you think

28
00:01:21,390 --> 00:01:21,880
 that it's good

29
00:01:21,880 --> 00:01:25,840
 that you have a water source and water available, that's

30
00:01:25,840 --> 00:01:26,960
 only half the answer.

31
00:01:26,960 --> 00:01:30,450
 The other half is that the source has to be pure. Your

32
00:01:30,450 --> 00:01:31,600
 heart has to be pure. If

33
00:01:31,600 --> 00:01:35,330
 you go out of your way to help others, look at how it goes

34
00:01:35,330 --> 00:01:36,760
 down in reality. We

35
00:01:36,760 --> 00:01:41,420
 put so much of our defilement into our help for others that

36
00:01:41,420 --> 00:01:43,040
 we get stressed out,

37
00:01:43,040 --> 00:01:55,320
 burnt out. Other people suffer as a result. We wind up with

38
00:01:55,320 --> 00:01:56,840
 doing some good,

39
00:01:56,840 --> 00:02:02,080
 but we wind up often with a lot of superficial good and a

40
00:02:02,080 --> 00:02:02,960
 fair amount of

41
00:02:02,960 --> 00:02:06,790
 deep harm to our own minds and even the minds of others

42
00:02:06,790 --> 00:02:08,080
 with conflicts and

43
00:02:08,080 --> 00:02:16,360
 protests and arguments and so on. So doing good deeds for

44
00:02:16,360 --> 00:02:18,520
 others is useful. I

45
00:02:18,520 --> 00:02:21,090
 think from my point of view, from the point of view, I

46
00:02:21,090 --> 00:02:22,360
 think I probably

47
00:02:22,360 --> 00:02:26,660
 speak for all of us when we say going out of your way to

48
00:02:26,660 --> 00:02:29,440
 accumulate merit is

49
00:02:29,440 --> 00:02:32,640
 not really the Buddha's teaching. It's something the Buddha

50
00:02:32,640 --> 00:02:33,840
 would often encourage

51
00:02:33,840 --> 00:02:36,960
 in people when he saw that that was the best he could give

52
00:02:36,960 --> 00:02:39,320
 them and when it

53
00:02:39,320 --> 00:02:43,930
 would help them to understand the relationship between good

54
00:02:43,930 --> 00:02:44,800
 and evil, he

55
00:02:44,800 --> 00:02:50,280
 would often point out that relationship. Ultimately the

56
00:02:50,280 --> 00:02:50,800
 idea is to

57
00:02:50,800 --> 00:02:56,320
 overcome and to be free. So the goodness that we should do

58
00:02:56,320 --> 00:02:57,120
 should be a part of

59
00:02:57,120 --> 00:03:00,520
 our path. I did do a video on this asking whether you

60
00:03:00,520 --> 00:03:00,720
 should

61
00:03:00,720 --> 00:03:06,280
 meditate or help people. We should look at helping people

62
00:03:06,280 --> 00:03:06,920
 as a part of our

63
00:03:06,920 --> 00:03:10,050
 path. It's not that we're going out of our way, it's that

64
00:03:10,050 --> 00:03:10,840
 they're a part of

65
00:03:10,840 --> 00:03:14,160
 our way, they're in our way. If we don't help them, we're

66
00:03:14,160 --> 00:03:15,120
 just going to create

67
00:03:15,120 --> 00:03:18,140
 conflict and we're going to get stuck at this point in our

68
00:03:18,140 --> 00:03:19,480
 path. We have to work

69
00:03:19,480 --> 00:03:23,890
 this out as a part of our life. That's why I said to my

70
00:03:23,890 --> 00:03:24,600
 teacher when he meets

71
00:03:24,600 --> 00:03:27,650
 with these people he doesn't get rid of them, he doesn't

72
00:03:27,650 --> 00:03:29,520
 kick them out, he deals

73
00:03:29,520 --> 00:03:34,540
 with them. This is the person in front of me now, I'm going

74
00:03:34,540 --> 00:03:35,800
 to work this out

75
00:03:35,800 --> 00:03:38,480
 mindfully.

76
00:03:49,520 --> 00:03:59,310
 I think there is this bodhisattva ideal. For some people it

77
00:03:59,310 --> 00:04:00,640
's probably a good way

78
00:04:00,640 --> 00:04:07,390
 and the Buddha has been a bodhisattva many many thousands

79
00:04:07,390 --> 00:04:10,040
 of lifetimes before

80
00:04:10,040 --> 00:04:14,670
 he became the Buddha and helping people helps our mind to

81
00:04:14,670 --> 00:04:18,560
 purify and to

82
00:04:18,560 --> 00:04:23,380
 become a better being and to understand not only what is

83
00:04:23,380 --> 00:04:25,120
 going on inside

84
00:04:25,120 --> 00:04:32,550
 ourselves but what is going on externally. It is helpful to

85
00:04:32,550 --> 00:04:33,400
 do so but I

86
00:04:33,400 --> 00:04:39,400
 think it's not helpful to go and look for where can I be of

87
00:04:39,400 --> 00:04:40,640
 help, what

88
00:04:40,640 --> 00:04:47,380
 can I do like you mentioned the missionaries, the Christian

89
00:04:47,380 --> 00:04:49,440
 missionaries.

90
00:04:49,440 --> 00:04:56,170
 That is as I understand is getting too much involved in

91
00:04:56,170 --> 00:04:58,920
 things that is not

92
00:04:58,920 --> 00:05:07,360
 really your business. That sounds cruel I think is the word

93
00:05:07,360 --> 00:05:08,560
, that may sound cruel

94
00:05:08,560 --> 00:05:15,460
 but sometimes it's important to help and sometimes the

95
00:05:15,460 --> 00:05:17,320
 better help is to just

96
00:05:17,320 --> 00:05:21,850
 leave it as it is and not to get involved in it. So often

97
00:05:21,850 --> 00:05:23,560
 the help is

98
00:05:23,560 --> 00:05:28,560
 cultivating attachment. We have expectations about this and

99
00:05:28,560 --> 00:05:28,960
 we become

100
00:05:28,960 --> 00:05:34,080
 frustrated and the other thing I didn't say but often we're

101
00:05:34,080 --> 00:05:34,880
 impressing our views

102
00:05:34,880 --> 00:05:39,450
 on people right? Exactly, that's what I wanted to say.

103
00:05:39,450 --> 00:05:42,160
 Which is what religions do. It may not be to the point of

104
00:05:42,160 --> 00:05:45,920
 handing out Bibles but it often is but it can often be more

105
00:05:45,920 --> 00:05:47,680
 subtle than that.

106
00:05:47,680 --> 00:05:51,130
 Trying to change people's lives, saying let's get you out

107
00:05:51,130 --> 00:05:52,400
 of this squalor and

108
00:05:52,400 --> 00:06:00,160
 put you in apartment buildings for example. I had something

109
00:06:00,160 --> 00:06:00,680
 else to say I

110
00:06:00,680 --> 00:06:05,750
 can't remember. Oh the other thing I wanted to say is that

111
00:06:05,750 --> 00:06:07,480
 we talk about the

112
00:06:07,480 --> 00:06:12,800
 bodhisattva and the Buddha but we miss, many people will

113
00:06:12,800 --> 00:06:14,000
 talk to them and say yes

114
00:06:14,000 --> 00:06:18,540
 there's the bodhisattva way of going out or of dedicating

115
00:06:18,540 --> 00:06:19,560
 yourself to helping

116
00:06:19,560 --> 00:06:23,950
 others but it's not actually the case that the Buddha, the

117
00:06:23,950 --> 00:06:25,600
 bodhisattva went out

118
00:06:25,600 --> 00:06:28,840
 of his way to help others. The bodhisattva spent, if you

119
00:06:28,840 --> 00:06:30,180
 read what he did

120
00:06:30,180 --> 00:06:33,720
 during those lifetimes, he didn't really go out of his way

121
00:06:33,720 --> 00:06:34,740
 to help people. He was

122
00:06:34,740 --> 00:06:38,720
 trying to develop perfection of mind which is still a

123
00:06:38,720 --> 00:06:40,260
 personal thing. He spent

124
00:06:40,260 --> 00:06:44,260
 at least as much time off in the forest meditating as a bod

125
00:06:44,260 --> 00:06:46,020
hisattva as he did

126
00:06:46,020 --> 00:06:50,100
 for example as a king. The point is the Buddha has to do

127
00:06:50,100 --> 00:06:51,960
 everything. He has or

128
00:06:51,960 --> 00:06:57,540
 has to come to know everything so his reach has to be so

129
00:06:57,540 --> 00:06:59,880
 much broader than

130
00:06:59,880 --> 00:07:03,600
 the person who's going by his experience and going by his

131
00:07:03,600 --> 00:07:04,000
 guidance

132
00:07:04,000 --> 00:07:07,020
 because the Buddha had to go everywhere and before he could

133
00:07:07,020 --> 00:07:07,520
 put it together

134
00:07:07,520 --> 00:07:15,680
 himself, for himself. But he didn't go out of his way and

135
00:07:15,680 --> 00:07:16,160
 there are several

136
00:07:16,160 --> 00:07:19,620
 examples of, there was one example that I thought of while

137
00:07:19,620 --> 00:07:21,200
 you were talking

138
00:07:21,200 --> 00:07:28,680
 where Sariputta, the person who was to be Sariputta in his

139
00:07:28,680 --> 00:07:29,200
 last life,

140
00:07:29,200 --> 00:07:33,900
 they were both ascetics together and Sariputta had gotten

141
00:07:33,900 --> 00:07:34,920
 in his mind that he

142
00:07:34,920 --> 00:07:37,670
 was going to get everyone to keep the precepts. So he would

143
00:07:37,670 --> 00:07:38,720
 go around to people

144
00:07:38,720 --> 00:07:41,940
 and say, "Look, come, come, come. You're not keeping the

145
00:07:41,940 --> 00:07:42,780
 precepts. I'm going to

146
00:07:42,780 --> 00:07:49,240
 teach you about these five precepts, the rules or ethics or

147
00:07:49,240 --> 00:07:49,840
 morality."

148
00:07:49,840 --> 00:07:55,080
 And the bodhisattva watched him for a while and then he

149
00:07:55,080 --> 00:07:55,760
 thought, "Man, this guy's

150
00:07:55,760 --> 00:08:00,440
 this guy's really, he's turning people off. It creates kind

151
00:08:00,440 --> 00:08:02,880
 of, it's these people

152
00:08:02,880 --> 00:08:07,000
 aren't doing it from their heart." So he said, "I got to

153
00:08:07,000 --> 00:08:08,400
 teach him a lesson." And so

154
00:08:08,400 --> 00:08:12,670
 one day Sariputta came back or the ascetic who was later to

155
00:08:12,670 --> 00:08:13,360
 be Sariputta

156
00:08:13,360 --> 00:08:20,770
 came back and he found the bodhisattva kicking the earth

157
00:08:20,770 --> 00:08:22,920
 and pulling up, pulling

158
00:08:22,920 --> 00:08:26,710
 rocks out of the ground, uprooting trees, throwing dirt

159
00:08:26,710 --> 00:08:28,600
 here, just running around

160
00:08:28,600 --> 00:08:31,760
 like a crazy person, like he was trying to pull the earth

161
00:08:31,760 --> 00:08:34,480
 apart. And the

162
00:08:34,480 --> 00:08:37,870
 ascetic, the Sariputta ascetic said, "What are you doing?"

163
00:08:37,870 --> 00:08:40,480
 He said, "I know what my

164
00:08:40,480 --> 00:08:43,480
 goal in life is. I've decided that I'm going to level the

165
00:08:43,480 --> 00:08:45,240
 earth. I'm going to

166
00:08:45,240 --> 00:08:52,970
 turn the earth, make the earth flat." And Sariputta said, "

167
00:08:52,970 --> 00:08:54,920
That's insane. How could

168
00:08:54,920 --> 00:08:57,960
 you possibly flatten the earth? Are you out of your mind?"

169
00:08:57,960 --> 00:09:02,160
 And the

170
00:09:02,160 --> 00:09:04,980
 bodhisattva looked at him and said, "Well, look at you. You

171
00:09:04,980 --> 00:09:06,160
're trying to level out,

172
00:09:06,160 --> 00:09:10,320
 make all people the same and follow the same precepts."

173
00:09:10,320 --> 00:09:12,600
 Because he was

174
00:09:12,600 --> 00:09:14,910
 saying, "No, there are mountains, there are valleys." And

175
00:09:14,910 --> 00:09:16,120
 the Buddha said, "Buddhi said, "There are good

176
00:09:16,120 --> 00:09:19,510
 people, there are bad people. You expect them all to be

177
00:09:19,510 --> 00:09:20,680
 able to follow?"

178
00:09:20,680 --> 00:09:27,040
 My quest of leveling out the earth is far more achievable

179
00:09:27,040 --> 00:09:28,460
 than your quest in

180
00:09:28,460 --> 00:09:34,470
 leveling out people's characters. And Sariputta realized

181
00:09:34,470 --> 00:09:36,000
 how silly he had

182
00:09:36,000 --> 00:09:39,830
 been. And from then on, something about, at least from then

183
00:09:39,830 --> 00:09:41,160
 on, he never gave

184
00:09:41,160 --> 00:09:45,440
 precepts to anyone who didn't ask. And something about how

185
00:09:45,440 --> 00:09:45,440
 that's the

186
00:09:45,440 --> 00:09:48,720
 tradition of people asking for the precepts. So tonight you

187
00:09:48,720 --> 00:09:49,280
 asked for the

188
00:09:49,280 --> 00:09:52,240
 precepts. We don't give them to people unless they ask.

189
00:09:52,240 --> 00:09:52,840
 That's where that

190
00:09:52,840 --> 00:09:56,700
 tradition came from. So it's not true to say that the bod

191
00:09:56,700 --> 00:09:58,520
hisattva's path was to

192
00:09:58,520 --> 00:10:02,570
 go out of his way to help people. It's not really carried

193
00:10:02,570 --> 00:10:03,920
 out in the text.

194
00:10:03,920 --> 00:10:07,910
 He helped an incredible number of people and sacrificed so

195
00:10:07,910 --> 00:10:09,920
 much. But it seems to

196
00:10:09,920 --> 00:10:12,570
 more and more come natural. And that's of course the path

197
00:10:12,570 --> 00:10:13,840
 that we're aiming for.

198
00:10:13,840 --> 00:10:16,300
 If he had gone out of his way to help people, you would

199
00:10:16,300 --> 00:10:17,160
 think he would be

200
00:10:17,160 --> 00:10:20,970
 developing more attachment, more views and opinions and so

201
00:10:20,970 --> 00:10:22,120
 on. And this idea of

202
00:10:22,120 --> 00:10:28,600
 trying to level out, make the earth flat.

203
00:10:29,400 --> 00:10:32,080
 Okay.

