1
00:00:00,000 --> 00:00:10,760
 How do we balance our faculties with wisdom balancing

2
00:00:10,760 --> 00:00:13,360
 confidence and what should we do

3
00:00:13,360 --> 00:00:26,440
 to stay in the middle path?

4
00:00:26,440 --> 00:00:31,600
 I have talked about the balancing the faculties.

5
00:00:31,600 --> 00:00:32,600
 It's in the booklet.

6
00:00:32,600 --> 00:00:37,000
 If you've read my booklet on how to meditate, it gives...

7
00:00:37,000 --> 00:00:40,520
 I mean a lot of the stuff in that booklet is just straight

8
00:00:40,520 --> 00:00:42,240
 what my teacher taught, which

9
00:00:42,240 --> 00:00:45,380
 is why I made the videos and why I made the booklet because

10
00:00:45,380 --> 00:00:47,000
 he taught the same thing almost

11
00:00:47,000 --> 00:00:49,120
 every day and he didn't change it.

12
00:00:49,120 --> 00:00:52,200
 It wasn't based on any idea of his.

13
00:00:52,200 --> 00:00:55,420
 It's like this is what the Buddha taught and giving it to

14
00:00:55,420 --> 00:00:56,080
 people.

15
00:00:56,080 --> 00:00:58,950
 So it seemed like the kind of thing that you could really

16
00:00:58,950 --> 00:00:59,960
 systematize.

17
00:00:59,960 --> 00:01:04,200
 Not overly systematize, but just get the information out

18
00:01:04,200 --> 00:01:07,000
 there because once people know and they

19
00:01:07,000 --> 00:01:11,480
 have the instruction manual, then they can go with it.

20
00:01:11,480 --> 00:01:14,430
 So there is a standard explanation of how to balance the

21
00:01:14,430 --> 00:01:16,200
 faculties and hopefully that's

22
00:01:16,200 --> 00:01:21,960
 what's conveyed in the book.

23
00:01:21,960 --> 00:01:25,690
 But something that isn't conveyed in the book is this

24
00:01:25,690 --> 00:01:28,440
 famous simile that is also given that

25
00:01:28,440 --> 00:01:36,800
 the faculties are like horses and the rider.

26
00:01:36,800 --> 00:01:47,160
 Confidence, wisdom, concentration and effort.

27
00:01:47,160 --> 00:01:53,510
 Confidence, effort, concentration and wisdom are like four

28
00:01:53,510 --> 00:01:56,120
 horses and they pair up together.

29
00:01:56,120 --> 00:01:58,610
 So you've got confidence and wisdom as a pair and then you

30
00:01:58,610 --> 00:02:01,160
've got behind them you've got

31
00:02:01,160 --> 00:02:04,520
 effort and concentration and they're a pair.

32
00:02:04,520 --> 00:02:07,350
 And these four horses have to be tied together and have to

33
00:02:07,350 --> 00:02:08,960
 be going in unison in order for

34
00:02:08,960 --> 00:02:12,000
 the carriage to go properly.

35
00:02:12,000 --> 00:02:14,460
 So what do you need to make the carriage go properly and to

36
00:02:14,460 --> 00:02:15,800
 keep the horses in check?

37
00:02:15,800 --> 00:02:19,770
 Well you need a driver, the driver of the four horses and

38
00:02:19,770 --> 00:02:22,160
 this driver is the fifth faculty

39
00:02:22,160 --> 00:02:24,680
 and that's mindfulness.

40
00:02:24,680 --> 00:02:29,360
 And this is how we should understand the how to.

41
00:02:29,360 --> 00:02:34,870
 The teaching on the five faculties is actually not so much

42
00:02:34,870 --> 00:02:37,120
 a teaching on how to.

43
00:02:37,120 --> 00:02:44,950
 It's a teaching on when to or why to or how to apply

44
00:02:44,950 --> 00:02:47,800
 mindfulness.

45
00:02:47,800 --> 00:02:52,980
 Because if you're mindful then the faculties will naturally

46
00:02:52,980 --> 00:02:54,360
 be balanced.

47
00:02:54,360 --> 00:02:58,850
 There's no imbalance of the faculties for someone who is in

48
00:02:58,850 --> 00:03:00,960
 a mindful and clearly aware

49
00:03:00,960 --> 00:03:02,480
 state.

50
00:03:02,480 --> 00:03:10,490
 It's when we're unmindful that the imbalance occurs for

51
00:03:10,490 --> 00:03:13,480
 whatever reason.

52
00:03:13,480 --> 00:03:20,180
 So the importance of this teaching know that if you have

53
00:03:20,180 --> 00:03:24,040
 too much confidence it leads to

54
00:03:24,040 --> 00:03:28,290
 greed or depending who you ask it leads to, obviously it

55
00:03:28,290 --> 00:03:30,320
 leads to following after your

56
00:03:30,320 --> 00:03:32,920
 own intentions.

57
00:03:32,920 --> 00:03:35,860
 My teacher would always say it leads to greed I think but I

58
00:03:35,860 --> 00:03:37,800
 read elsewhere there's a different

59
00:03:37,800 --> 00:03:40,400
 explanation I can't remember.

60
00:03:40,400 --> 00:03:44,750
 But as I generally explain it you tend to believe and

61
00:03:44,750 --> 00:03:47,400
 follow your own intentions so

62
00:03:47,400 --> 00:03:49,920
 you want something you think yeah that's right.

63
00:03:49,920 --> 00:03:53,950
 This sort of person who thinks things are right because

64
00:03:53,950 --> 00:03:56,200
 they're right because I think

65
00:03:56,200 --> 00:03:57,200
 it's right.

66
00:03:57,200 --> 00:03:59,560
 I think it's right because I think it's right.

67
00:03:59,560 --> 00:04:02,560
 And so whatever hits them whatever it says hey that's a

68
00:04:02,560 --> 00:04:04,240
 good idea let's do it without

69
00:04:04,240 --> 00:04:07,840
 even thinking or examining or looking or reflecting as the

70
00:04:07,840 --> 00:04:09,960
 Buddha said on what are the results

71
00:04:09,960 --> 00:04:10,960
 of that action.

72
00:04:10,960 --> 00:04:14,280
 It's a good talk the Buddha gave to Rahula that's very

73
00:04:14,280 --> 00:04:15,060
 famous.

74
00:04:15,060 --> 00:04:18,080
 He said what are mirrors for?

75
00:04:18,080 --> 00:04:21,600
 And Rahula was the Buddha's son because the Buddha had a

76
00:04:21,600 --> 00:04:23,680
 son before he went off to become

77
00:04:23,680 --> 00:04:24,680
 a monk.

78
00:04:24,680 --> 00:04:30,560
 He said what are mirrors for? And Rahula said mirrors are

79
00:04:30,560 --> 00:04:30,800
 for reflecting.

80
00:04:30,800 --> 00:04:34,620
 And he said in the same way you should reflect on all of

81
00:04:34,620 --> 00:04:37,440
 your acts you should use mindfulness

82
00:04:37,440 --> 00:04:40,410
 to reflect upon all of your acts when you have done

83
00:04:40,410 --> 00:04:42,920
 something when you have said something

84
00:04:42,920 --> 00:04:46,180
 when you've even thought something you should mindfully

85
00:04:46,180 --> 00:04:47,520
 reflect on the act.

86
00:04:47,520 --> 00:04:49,200
 Was it good to do that?

87
00:04:49,200 --> 00:04:50,480
 Was it good to say that?

88
00:04:50,480 --> 00:04:51,560
 Was it good to think that?

89
00:04:51,560 --> 00:04:54,200
 What was the result?

90
00:04:54,200 --> 00:04:58,240
 If we all did this we'd be able to see what was causing us

91
00:04:58,240 --> 00:05:00,960
 happiness and causing us suffering.

92
00:05:00,960 --> 00:05:05,000
 Because people have too much confidence they don't do this.

93
00:05:05,000 --> 00:05:07,590
 And it's very hard to deal with such people because they

94
00:05:07,590 --> 00:05:09,080
 really believe in what they're

95
00:05:09,080 --> 00:05:11,670
 doing and what they're saying and what they're thinking

96
00:05:11,670 --> 00:05:13,200
 without even looking and seeing what

97
00:05:13,200 --> 00:05:16,040
 harm it's causing to themselves.

98
00:05:16,040 --> 00:05:18,910
 That explains a lot of the problems that go on in this

99
00:05:18,910 --> 00:05:19,520
 world.

100
00:05:19,520 --> 00:05:24,240
 If you have too much wisdom and hear wisdom in the sense of

101
00:05:24,240 --> 00:05:26,960
 not having seen it for yourself

102
00:05:26,960 --> 00:05:29,700
 but having read about it or having thought about it or

103
00:05:29,700 --> 00:05:31,680
 having intellectually understood

104
00:05:31,680 --> 00:05:36,960
 it because it really it pretends to it seems so much like

105
00:05:36,960 --> 00:05:39,300
 direct realization.

106
00:05:39,300 --> 00:05:41,390
 People who have read all the Buddha's teaching can sound

107
00:05:41,390 --> 00:05:44,600
 exactly like an enlightened being.

108
00:05:44,600 --> 00:05:47,270
 It seems wow yeah they know everything and they can teach

109
00:05:47,270 --> 00:05:48,660
 and people who practice can

110
00:05:48,660 --> 00:05:53,680
 become enlightened even.

111
00:05:53,680 --> 00:05:56,910
 But they don't have the verification and therefore they don

112
00:05:56,910 --> 00:05:58,360
't have the confidence.

113
00:05:58,360 --> 00:06:02,660
 And so as a result it leads to doubt.

114
00:06:02,660 --> 00:06:06,490
 It leads them to be always thinking and considering and

115
00:06:06,490 --> 00:06:08,560
 wondering what is the way?

116
00:06:08,560 --> 00:06:10,000
 What is this?

117
00:06:10,000 --> 00:06:13,200
 And so they have a lot of doubt.

118
00:06:13,200 --> 00:06:16,530
 If a person has too much effort but not enough

119
00:06:16,530 --> 00:06:20,560
 concentration then they will become distracted.

120
00:06:20,560 --> 00:06:22,460
 This kind of goes with what I just said.

121
00:06:22,460 --> 00:06:24,440
 Sometimes it's not doubt but it's just thinking a lot.

122
00:06:24,440 --> 00:06:27,120
 If you're thinking a lot you find that you have probably

123
00:06:27,120 --> 00:06:28,520
 too much energy and so you can

124
00:06:28,520 --> 00:06:31,120
 do lying meditation.

125
00:06:31,120 --> 00:06:33,400
 Yeah there are tricks for some of them actually.

126
00:06:33,400 --> 00:06:35,580
 In fact there are tricks for all of them.

127
00:06:35,580 --> 00:06:38,020
 If we do the Satipatthana Sutta I can go through it and I

128
00:06:38,020 --> 00:06:40,000
 did in Second Life actually go through

129
00:06:40,000 --> 00:06:43,160
 the, that's right I had a suta study course.

130
00:06:43,160 --> 00:06:44,640
 At one point.

131
00:06:44,640 --> 00:06:47,390
 And we went through the Satipatthana Sutta and I talked

132
00:06:47,390 --> 00:06:48,960
 about what the commentary had

133
00:06:48,960 --> 00:06:50,240
 to say on the faculties.

134
00:06:50,240 --> 00:06:53,810
 I would recommend looking at the commentary to the Satipat

135
00:06:53,810 --> 00:06:54,840
thana Sutta.

136
00:06:54,840 --> 00:06:58,820
 In The Way of Mindfulness, this book called The Way of Mind

137
00:06:58,820 --> 00:07:00,960
fulness which is an excellent

138
00:07:00,960 --> 00:07:04,120
 book worth everyone reading.

139
00:07:04,120 --> 00:07:05,840
 It's not written by the author.

140
00:07:05,840 --> 00:07:09,570
 It's a translation and compilation of the discourse and its

141
00:07:09,570 --> 00:07:11,680
 commentary and sub-commentary.

142
00:07:11,680 --> 00:07:18,000
 Excellent excellent resource by Soma Thira.

143
00:07:18,000 --> 00:07:20,640
 So if you read the part on the five faculties what the

144
00:07:20,640 --> 00:07:22,600
 commenter has to say it'll talk about

145
00:07:22,600 --> 00:07:25,470
 how to improve each of these in different ways that you can

146
00:07:25,470 --> 00:07:26,360
 improve them.

147
00:07:26,360 --> 00:07:30,440
 Some of them are a little bit odd but in general pretty

148
00:07:30,440 --> 00:07:31,680
 good advice.

149
00:07:31,680 --> 00:07:34,440
 And it's certainly not all inclusive I don't think.

150
00:07:34,440 --> 00:07:39,960
 I think there are some ways that it doesn't mention.

151
00:07:39,960 --> 00:07:43,720
 Yeah and if a person has a lot of concentration but not

152
00:07:43,720 --> 00:07:45,800
 enough effort then they'll feel tired

153
00:07:45,800 --> 00:07:48,290
 and sleepy and as a result they should get up and do

154
00:07:48,290 --> 00:07:49,600
 walking meditation.

155
00:07:49,600 --> 00:07:53,960
 This kind of thing is you can balance them in this way.

156
00:07:53,960 --> 00:07:55,760
 But ultimately none of them really work.

157
00:07:55,760 --> 00:07:59,210
 None of them really have the ultimate result of balancing

158
00:07:59,210 --> 00:08:01,440
 the mind in the way that mindfulness

159
00:08:01,440 --> 00:08:02,820
 does.

160
00:08:02,820 --> 00:08:06,800
 So talking about them is identifying in yourself what's

161
00:08:06,800 --> 00:08:09,560
 going wrong and making people realize

162
00:08:09,560 --> 00:08:12,760
 that ah confidence isn't the answer because look at me I'm

163
00:08:12,760 --> 00:08:14,400
 confident in hearing being

164
00:08:14,400 --> 00:08:17,120
 an idiot and doing all these stupid things.

165
00:08:17,120 --> 00:08:19,560
 Drunk people are very confident.

166
00:08:19,560 --> 00:08:22,210
 Doesn't mean that drunk people are always doing the right

167
00:08:22,210 --> 00:08:22,720
 thing.

168
00:08:22,720 --> 00:08:29,080
 That is a good example.

169
00:08:29,080 --> 00:08:32,650
 To people who think a lot, people who study a lot to help

170
00:08:32,650 --> 00:08:34,280
 them to see that no you're not

171
00:08:34,280 --> 00:08:38,360
 enlightened and no it's really not doing you a mode of good

172
00:08:38,360 --> 00:08:40,280
 to be able to recite all of

173
00:08:40,280 --> 00:08:42,200
 these teachings and so on.

174
00:08:42,200 --> 00:08:44,000
 And remember all of these things.

175
00:08:44,000 --> 00:08:45,500
 It's always funny.

176
00:08:45,500 --> 00:08:50,340
 I find it funny and you were talking to people who were

177
00:08:50,340 --> 00:08:53,400
 able to point out the smallest of

178
00:08:53,400 --> 00:08:55,880
 errors.

179
00:08:55,880 --> 00:08:57,400
 I gave a talk once in Thailand.

180
00:08:57,400 --> 00:09:00,040
 I was learning how to give talks in Thai.

181
00:09:00,040 --> 00:09:01,120
 So I gave this talk in Thai.

182
00:09:01,120 --> 00:09:06,180
 I mean that's an accomplishment just to be able to give a

183
00:09:06,180 --> 00:09:09,360
 talk in Thai is an accomplishment.

184
00:09:09,360 --> 00:09:12,200
 I gave a talk on the Buddha, the Dhamma and the Sangha.

185
00:09:12,200 --> 00:09:15,540
 Easy talk based on what are the three refuges.

186
00:09:15,540 --> 00:09:16,540
 What is the Buddha?

187
00:09:16,540 --> 00:09:17,540
 What is the Dhamma?

188
00:09:17,540 --> 00:09:18,540
 What is the Sangha?

189
00:09:18,540 --> 00:09:20,160
 According to a meditator.

190
00:09:20,160 --> 00:09:22,800
 I gave it totally in Thai.

191
00:09:22,800 --> 00:09:24,900
 This nun came up and she said oh you're just like a Thai

192
00:09:24,900 --> 00:09:25,440
 person.

193
00:09:25,440 --> 00:09:26,440
 I felt so good.

194
00:09:26,440 --> 00:09:27,440
 Oh happy.

195
00:09:27,440 --> 00:09:30,320
 Proud of myself.

196
00:09:30,320 --> 00:09:35,600
 And then this man, this guy who wasn't really interested.

197
00:09:35,600 --> 00:09:38,600
 He had come to really cause trouble in the monastery.

198
00:09:38,600 --> 00:09:43,920
 He was looking criticizing even our head teacher who was

199
00:09:43,920 --> 00:09:46,800
 the best monk or the most noble being

200
00:09:46,800 --> 00:09:50,040
 I've ever met.

201
00:09:50,040 --> 00:09:54,040
 And he came up to me the next day and he said, because I

202
00:09:54,040 --> 00:09:56,520
 had said in the talk, I had said

203
00:09:56,520 --> 00:09:59,910
 when we talk about the Sangha, there are different kinds of

204
00:09:59,910 --> 00:10:00,640
 Sangha.

205
00:10:00,640 --> 00:10:05,550
 When we talk about Samuti Sangha, which is the conceptual

206
00:10:05,550 --> 00:10:09,160
 Sangha, then I said any monk

207
00:10:09,160 --> 00:10:13,760
 is the Sangha.

208
00:10:13,760 --> 00:10:18,440
 Any monk is Sangha, is a Samuti Sangha.

209
00:10:18,440 --> 00:10:22,870
 And I was just basically saying that the group of monks are

210
00:10:22,870 --> 00:10:24,500
 just conceptual.

211
00:10:24,500 --> 00:10:28,130
 So a monk can say I'm Sangha, I'm part of the Sangha

212
00:10:28,130 --> 00:10:30,600
 because he's wearing a robe.

213
00:10:30,600 --> 00:10:31,800
 Because he's had the ordination.

214
00:10:31,800 --> 00:10:37,380
 And he came up to me the next day and said, what is the Sam

215
00:10:37,380 --> 00:10:38,800
uti Sangha?

216
00:10:38,800 --> 00:10:42,240
 I said well as I said any monk is the Samuti Sangha.

217
00:10:42,240 --> 00:10:44,240
 He said no you're wrong.

218
00:10:44,240 --> 00:10:46,800
 And he said, Mai Chai?

219
00:10:46,800 --> 00:10:47,800
 He said no.

220
00:10:47,800 --> 00:10:49,720
 I said well then what's the Sangha?

221
00:10:49,720 --> 00:10:51,440
 He said four monks.

222
00:10:51,440 --> 00:10:53,880
 That's the Samuti Sangha.

223
00:10:53,880 --> 00:10:57,930
 And you know he's right because the Sangha, in a technical

224
00:10:57,930 --> 00:11:00,280
 sense in the Vinaya, four monks

225
00:11:00,280 --> 00:11:04,240
 together constitute what is called a Sangha.

226
00:11:04,240 --> 00:11:05,240
 So he was correct.

227
00:11:05,240 --> 00:11:06,760
 But he was also wrong.

228
00:11:06,760 --> 00:11:09,810
 And he was wrong to criticize what I was saying because I

229
00:11:09,810 --> 00:11:12,080
 wasn't saying that one monk alone

230
00:11:12,080 --> 00:11:15,000
 constitutes a valid Sangha.

231
00:11:15,000 --> 00:11:16,000
 It's not true.

232
00:11:16,000 --> 00:11:19,560
 But a monk is Sangha.

233
00:11:19,560 --> 00:11:22,240
 And that's generally understood to be that way.

234
00:11:22,240 --> 00:11:25,820
 Anyone who talks about, even in Thailand they talk about

235
00:11:25,820 --> 00:11:27,920
 monks, they say that's Prasong.

236
00:11:27,920 --> 00:11:29,680
 Which means the Sangha.

237
00:11:29,680 --> 00:11:31,160
 Oh he's Prasong.

238
00:11:31,160 --> 00:11:32,160
 He's Sangha.

239
00:11:32,160 --> 00:11:36,320
 And we talk like this.

240
00:11:36,320 --> 00:11:37,880
 Way off topic here.

241
00:11:37,880 --> 00:11:41,980
 But the point being that this is the kind of, this person

242
00:11:41,980 --> 00:11:44,160
 was a perfect example of how

243
00:11:44,160 --> 00:11:46,240
 someone takes it too far.

244
00:11:46,240 --> 00:11:48,000
 And then they go around trying to point out the faults.

245
00:11:48,000 --> 00:11:53,350
 I mean I was thinking, man you just heard me give my first

246
00:11:53,350 --> 00:11:54,960
 real Thai talk.

247
00:11:54,960 --> 00:12:00,660
 And all you can think about is how I flubbed the word San

248
00:12:00,660 --> 00:12:01,520
gha.

249
00:12:01,520 --> 00:12:10,120
 So it was a good test for me as far as the ego goes.

250
00:12:10,120 --> 00:12:15,000
 But also a very good example here of this sort of thing.

251
00:12:15,000 --> 00:12:19,560
 And so this is the recognition here.

252
00:12:19,560 --> 00:12:23,220
 I mean this is the kind of talk you would give to that sort

253
00:12:23,220 --> 00:12:24,040
 of person.

254
00:12:24,040 --> 00:12:25,040
 There's a better talk actually.

255
00:12:25,040 --> 00:12:28,440
 The talk I give on the five types of people.

256
00:12:28,440 --> 00:12:31,480
 It's just a talk that my teacher always used to give on the

257
00:12:31,480 --> 00:12:33,240
 five types of Dhamma Vihari.

258
00:12:33,240 --> 00:12:36,180
 If you've ever heard me talk about what is a Dhamma Vihari,

259
00:12:36,180 --> 00:12:37,480
 the five types of people

260
00:12:37,480 --> 00:12:38,800
 that exist in the world.

261
00:12:38,800 --> 00:12:41,000
 It's a good one to give to that person.

262
00:12:41,000 --> 00:12:43,510
 But giving the talk on the balancing the faculties is

263
00:12:43,510 --> 00:12:45,440
 useful because it suddenly reminds us,

264
00:12:45,440 --> 00:12:47,880
 oh yes, I've got these as well.

265
00:12:47,880 --> 00:12:55,240
 I'm very active or I'm very lazy or I'm very skeptical of

266
00:12:55,240 --> 00:12:59,200
 everything or I have so much

267
00:12:59,200 --> 00:13:02,720
 overconfidence and so on.

268
00:13:02,720 --> 00:13:05,880
 So people always ask how should we balance the faculties.

269
00:13:05,880 --> 00:13:08,120
 And we really shouldn't look at it that way.

270
00:13:08,120 --> 00:13:09,120
 You should practice mindfulness.

271
00:13:09,120 --> 00:13:12,820
 And the answer is very simple that through mindfulness you

272
00:13:12,820 --> 00:13:14,560
 balance the faculties.

273
00:13:14,560 --> 00:13:17,200
 What you should do from time to time is look and see how

274
00:13:17,200 --> 00:13:18,720
 your faculties are doing.

275
00:13:18,720 --> 00:13:21,440
 Because what happens is we miss one.

276
00:13:21,440 --> 00:13:23,610
 We think we're being mindful and we're mindful of some

277
00:13:23,610 --> 00:13:24,880
 things but we're missing.

278
00:13:24,880 --> 00:13:27,700
 We fail to catch something and we do it over and over again

279
00:13:27,700 --> 00:13:29,200
 and then we can't understand

280
00:13:29,200 --> 00:13:30,400
 why we're suffering.

281
00:13:30,400 --> 00:13:31,880
 Here I am practicing meditation.

282
00:13:31,880 --> 00:13:34,240
 Why am I still suffering?

283
00:13:34,240 --> 00:13:37,160
 You're probably missing something.

284
00:13:37,160 --> 00:13:40,480
 Okay, that's the first part of your question.

285
00:13:40,480 --> 00:13:41,480
 Let me stop there.

286
00:13:41,480 --> 00:13:44,140
 I'm just going to save this and then I'll answer the second

287
00:13:44,140 --> 00:13:44,600
 part.

