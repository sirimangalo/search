1
00:00:00,000 --> 00:00:03,000
 Do you ever get frustrated with answering the same

2
00:00:03,000 --> 00:00:03,500
 questions?

3
00:00:03,500 --> 00:00:06,890
 And if not, how do you continue to do so and seem so

4
00:00:06,890 --> 00:00:08,860
 patient?

5
00:00:08,860 --> 00:00:11,800
 Teaching is really easy.

6
00:00:11,800 --> 00:00:15,640
 You know, I mean, there's so much happiness that comes from

7
00:00:15,640 --> 00:00:17,740
 helping other people.

8
00:00:17,740 --> 00:00:19,800
 It can actually become intoxicating.

9
00:00:19,800 --> 00:00:23,000
 It can become a hindrance to the path.

10
00:00:23,000 --> 00:00:25,990
 It can for sure be a hindrance even if you're not

11
00:00:25,990 --> 00:00:28,640
 intoxicated in it because it takes your

12
00:00:28,640 --> 00:00:35,440
 time away from your meditation.

13
00:00:35,440 --> 00:00:41,230
 So that's one thing is that it's actually not as unpleasant

14
00:00:41,230 --> 00:00:42,580
 as it seems.

15
00:00:42,580 --> 00:00:48,820
 The more unpleasant thing is to sit around learning how to

16
00:00:48,820 --> 00:00:49,880
 teach.

17
00:00:49,880 --> 00:00:53,320
 When you have to sit and listen to someone teaching, the

18
00:00:53,320 --> 00:00:55,160
 same thing's over and over again.

19
00:00:55,160 --> 00:00:56,880
 So some of you have been with me for a long time and you

20
00:00:56,880 --> 00:00:58,040
 hear me saying the same things

21
00:00:58,040 --> 00:01:00,880
 over and over again.

22
00:01:00,880 --> 00:01:01,880
 This is more difficult.

23
00:01:01,880 --> 00:01:05,240
 But what I'm thinking of more is when I do reporting, when

24
00:01:05,240 --> 00:01:06,880
 I'm sitting and reporting,

25
00:01:06,880 --> 00:01:09,800
 I can do reporting for hours and hours and hours.

26
00:01:09,800 --> 00:01:10,960
 Reporting means meeting with meditators.

27
00:01:10,960 --> 00:01:16,720
 I can have 20, 30 meditators and actually feel quite

28
00:01:16,720 --> 00:01:19,800
 relaxed and refreshed afterwards.

29
00:01:19,800 --> 00:01:22,320
 But people who have to sit and listen to me say the same

30
00:01:22,320 --> 00:01:24,000
 things again and again and again

31
00:01:24,000 --> 00:01:28,600
 and again, that's where the suffering comes from because

32
00:01:28,600 --> 00:01:31,360
 they don't have the same happiness

33
00:01:31,360 --> 00:01:37,000
 that comes from it and the same gusala, the same wholesomen

34
00:01:37,000 --> 00:01:39,520
ess that comes from it.

35
00:01:39,520 --> 00:01:42,760
 But the other thing is there is something to be said

36
00:01:42,760 --> 00:01:45,320
 because there is a, it's a training.

37
00:01:45,320 --> 00:01:49,480
 I've been teaching now for, I started very early.

38
00:01:49,480 --> 00:01:52,200
 I was teaching when I was only two years a monk.

39
00:01:52,200 --> 00:01:55,080
 So it's a skill like anything else.

40
00:01:55,080 --> 00:01:56,320
 I used to give Dhamma talks.

41
00:01:56,320 --> 00:02:01,040
 If there's anyone still around from Doy Sutepe times, they

42
00:02:01,040 --> 00:02:03,680
 can probably attest to the fact

43
00:02:03,680 --> 00:02:07,920
 that when I first started giving talks and teaching, it was

44
00:02:07,920 --> 00:02:10,480
 not much fun for the meditators.

45
00:02:10,480 --> 00:02:14,780
 Sometimes I would give horrible talks, just totally, not

46
00:02:14,780 --> 00:02:17,120
 put people off, but put people

47
00:02:17,120 --> 00:02:24,680
 to sleep and not be able to get my point across, not be

48
00:02:24,680 --> 00:02:29,960
 able to be interesting to people.

49
00:02:29,960 --> 00:02:34,360
 So like anything, it's really a skill and that helps

50
00:02:34,360 --> 00:02:37,120
 because when you make a misstep,

51
00:02:37,120 --> 00:02:42,030
 when you say something inappropriate, when you lose your

52
00:02:42,030 --> 00:02:44,840
 train of thought, when you start

53
00:02:44,840 --> 00:02:52,900
 getting flustered and like in this case are unable to come

54
00:02:52,900 --> 00:02:56,600
 up with the words properly,

55
00:02:56,600 --> 00:02:58,400
 then it creates stress in the mind.

56
00:02:58,400 --> 00:03:03,180
 So as you get better at things like teaching, it does get a

57
00:03:03,180 --> 00:03:04,400
 lot easier.

58
00:03:04,400 --> 00:03:06,510
 But the real thing I want to say in regards to this

59
00:03:06,510 --> 00:03:08,240
 question, which I think makes it an

60
00:03:08,240 --> 00:03:10,410
 important question is that teaching is not the most

61
00:03:10,410 --> 00:03:11,320
 difficult thing.

62
00:03:11,320 --> 00:03:15,600
 The most difficult thing is the practice.

63
00:03:15,600 --> 00:03:19,800
 People think that teaching is actually the sign that you

64
00:03:19,800 --> 00:03:21,360
 are enlightened.

65
00:03:21,360 --> 00:03:23,520
 It's a sign that you are advanced.

66
00:03:23,520 --> 00:03:25,980
 And as I said, it's really just a skill.

67
00:03:25,980 --> 00:03:32,060
 People can be great teachers and not have the greatest

68
00:03:32,060 --> 00:03:35,400
 practice on their own or even

69
00:03:35,400 --> 00:03:37,760
 beyond the wrong path.

70
00:03:37,760 --> 00:03:39,790
 There are examples in the scriptures of people who are

71
00:03:39,790 --> 00:03:41,400
 great teachers but were not enlightened

72
00:03:41,400 --> 00:03:43,360
 themselves.

73
00:03:43,360 --> 00:03:49,360
 So don't get too, I wouldn't get too confident in someone,

74
00:03:49,360 --> 00:03:54,160
 don't think of me as some great

75
00:03:54,160 --> 00:03:57,080
 enlightened being just because I can teach.

76
00:03:57,080 --> 00:03:59,790
 Enlightenment is something that you get from yourself, from

77
00:03:59,790 --> 00:04:01,160
 practicing the teachings.

78
00:04:01,160 --> 00:04:10,970
 It's not the same as being able to give convincing and

79
00:04:10,970 --> 00:04:13,480
 clear answers.

80
00:04:13,480 --> 00:04:16,750
 Even though obviously clarity of mind does help, meditation

81
00:04:16,750 --> 00:04:17,520
 does help.

82
00:04:17,520 --> 00:04:22,810
 If I wasn't meditating, I can't say that I would be able to

83
00:04:22,810 --> 00:04:25,400
 stay patient and answer people's

84
00:04:25,400 --> 00:04:26,400
 questions.

85
00:04:26,400 --> 00:04:27,400
 For sure not.

86
00:04:27,400 --> 00:04:31,000
 For sure I would try and fail miserably.

87
00:04:31,000 --> 00:04:35,080
 But they're not exactly the same.

88
00:04:35,080 --> 00:04:39,710
 And so it's important not to get too preoccupied with one's

89
00:04:39,710 --> 00:04:41,440
 ability to teach.

