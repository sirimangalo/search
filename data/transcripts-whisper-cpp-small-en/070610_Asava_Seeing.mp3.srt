1
00:00:00,000 --> 00:00:12,580
 Today I thought I would start a series of talks on the asav

2
00:00:12,580 --> 00:00:18,000
at, the taints or the fermentations,

3
00:00:18,000 --> 00:00:21,510
 which is just another way of saying the defilements, the

4
00:00:21,510 --> 00:00:25,000
 bad things which exist in our hearts.

5
00:00:25,000 --> 00:00:28,650
 But the Lord Buddha gave specific teaching on what he

6
00:00:28,650 --> 00:00:31,000
 called the asavat.

7
00:00:31,000 --> 00:00:34,190
 It's a classification of defilements into three parts,

8
00:00:34,190 --> 00:00:37,000
 which are kama asavat,

9
00:00:37,000 --> 00:00:42,000
 fermentations or defilements in regards to essentiality.

10
00:00:42,000 --> 00:00:47,000
 And then bhava asavat, defilements which have to do with

11
00:00:47,000 --> 00:00:49,000
 becoming, being this or being that

12
00:00:49,000 --> 00:00:52,000
 or not being this or not being that.

13
00:00:52,000 --> 00:00:57,000
 And then auija asavat, defilements which have to do with

14
00:00:57,000 --> 00:00:59,000
 simple ignorance.

15
00:00:59,000 --> 00:01:03,510
 So in this classification there are three kinds of defile

16
00:01:03,510 --> 00:01:04,000
ments,

17
00:01:04,000 --> 00:01:08,860
 ones which have to do with our attachment to sensuality,

18
00:01:08,860 --> 00:01:12,000
 liking good sights and sounds and smells

19
00:01:12,000 --> 00:01:16,000
 and disliking bad sights and sounds and smells and so on.

20
00:01:16,000 --> 00:01:20,200
 And then the ones having to do with wanting to be something

21
00:01:20,200 --> 00:01:23,000
, wanting to be this, wanting to be that,

22
00:01:23,000 --> 00:01:31,220
 attachment to status or position, and so our attachment to

23
00:01:31,220 --> 00:01:33,000
 not being this or not being that,

24
00:01:33,000 --> 00:01:39,000
 not being something, not being something in particular.

25
00:01:39,000 --> 00:01:42,270
 And then the defilements which have to do with simple

26
00:01:42,270 --> 00:01:44,000
 ignorance, simply not knowing.

27
00:01:44,000 --> 00:01:47,840
 So not knowing what is suffering, what is the truth, the

28
00:01:47,840 --> 00:01:50,000
 cause of suffering and so on.

29
00:01:50,000 --> 00:01:53,460
 Through simple ignorance we become defiled, we do bad

30
00:01:53,460 --> 00:01:57,000
 things, we say bad things simply out of ignorance.

31
00:01:57,000 --> 00:02:02,210
 So these three classifications, these are three reasons why

32
00:02:02,210 --> 00:02:04,000
 bad things arise.

33
00:02:04,000 --> 00:02:08,060
 And so the Lord Buddha gave a teaching on how to be free

34
00:02:08,060 --> 00:02:10,000
 from these defilements,

35
00:02:10,000 --> 00:02:14,000
 these things which in the Pali terms it means they pickle

36
00:02:14,000 --> 00:02:17,000
 or they cause the mind to become fermented,

37
00:02:17,000 --> 00:02:20,000
 to become useless, to become rotten.

38
00:02:20,000 --> 00:02:24,660
 When you think of it, it makes our mind become rotten, like

39
00:02:24,660 --> 00:02:28,000
 a rotten apple or a rotten tomato.

40
00:02:28,000 --> 00:02:33,950
 And the Lord Buddha gave seven distinct ways of overcoming

41
00:02:33,950 --> 00:02:36,000
 these defilements,

42
00:02:36,000 --> 00:02:40,270
 these things which cause our minds to become rotten, which

43
00:02:40,270 --> 00:02:44,000
 are called the taints or the fermentations,

44
00:02:44,000 --> 00:02:50,000
 the asana. And these seven ways are put together, they are

45
00:02:50,000 --> 00:02:53,000
 the complete description of the meditator's life,

46
00:02:53,000 --> 00:02:59,000
 the life of a monk or a nun or simply a simple meditator.

47
00:02:59,000 --> 00:03:04,670
 These seven practices are the way to become free from t

48
00:03:04,670 --> 00:03:07,000
aints and defilements,

49
00:03:07,000 --> 00:03:10,910
 free from all sorts of unwholesome things which exist

50
00:03:10,910 --> 00:03:12,000
 inside of ourselves,

51
00:03:12,000 --> 00:03:17,000
 to make our minds pure and to make our hearts clean.

52
00:03:17,000 --> 00:03:21,400
 So today I'll list off the seven but I'll try to go into

53
00:03:21,400 --> 00:03:25,000
 one of the specific teachings every day,

54
00:03:25,000 --> 00:03:28,000
 one or two depending on the time.

55
00:03:28,000 --> 00:03:39,620
 So the first one is the asana, the taints, which are to be

56
00:03:39,620 --> 00:03:42,000
 removed through seeing.

57
00:03:42,000 --> 00:03:53,000
 The second one, the taints which are to be removed through

58
00:03:53,000 --> 00:03:54,000
 guarding,

59
00:03:54,000 --> 00:03:58,590
 so through seeing, through guarding. Number three, patiseyv

60
00:03:58,590 --> 00:04:02,000
ana, through reflecting.

61
00:04:02,000 --> 00:04:09,420
 Number four, atiwasana, through bearing, or for bearing, or

62
00:04:09,420 --> 00:04:11,000
 enduring.

63
00:04:11,000 --> 00:04:19,160
 Number five, pariwatana, through running away or through

64
00:04:19,160 --> 00:04:21,000
 avoiding.

65
00:04:21,000 --> 00:04:27,000
 Number six, winotana, through abandoning.

66
00:04:27,000 --> 00:04:32,000
 And number seven, pawana, through developing.

67
00:04:32,000 --> 00:04:36,010
 These are the seven, they're verbs, they're seven actions

68
00:04:36,010 --> 00:04:38,000
 which we undertake

69
00:04:38,000 --> 00:04:41,690
 to rid ourselves of all unwholesome tendencies, to make our

70
00:04:41,690 --> 00:04:44,000
 minds pure and bright and clear

71
00:04:44,000 --> 00:04:49,000
 and calm and clean at all times.

72
00:04:49,000 --> 00:04:53,120
 So the first one, tasana, pahatapa, the ones which are to

73
00:04:53,120 --> 00:04:55,000
 be gotten rid of through seeing,

74
00:04:55,000 --> 00:04:59,000
 the way to get rid of taints simply by seeing.

75
00:04:59,000 --> 00:05:04,450
 And so the Lord Buddha goes into a long explanation of what

76
00:05:04,450 --> 00:05:09,000
 it means to be someone who is able to remove

77
00:05:09,000 --> 00:05:13,000
 the asavat, the taints which exist inside.

78
00:05:13,000 --> 00:05:16,000
 And he compares two different types of people.

79
00:05:16,000 --> 00:05:19,810
 The first type of person is someone who is untrained, who

80
00:05:19,810 --> 00:05:25,000
 isn't interested in going to see wise people,

81
00:05:25,000 --> 00:05:29,070
 to listen to their teachings, isn't interested in

82
00:05:29,070 --> 00:05:31,000
 meditation practice,

83
00:05:31,000 --> 00:05:35,450
 is an ordinary whirling who is very much interested in the

84
00:05:35,450 --> 00:05:36,000
 world

85
00:05:36,000 --> 00:05:41,000
 and interested in things which bring sensual pleasure,

86
00:05:41,000 --> 00:05:46,000
 which are of course impermanent and suffering in non-self.

87
00:05:46,000 --> 00:05:50,000
 And he says this type of person reflects on things which

88
00:05:50,000 --> 00:05:52,000
 are not to be reflected upon

89
00:05:52,000 --> 00:05:56,640
 and fails to reflect on those things which are proper to be

90
00:05:56,640 --> 00:05:58,000
 reflected upon.

91
00:05:58,000 --> 00:06:01,510
 And the things which are not to be reflected upon are those

92
00:06:01,510 --> 00:06:02,000
 things which,

93
00:06:02,000 --> 00:06:07,290
 when you consider them, when you attend to them, when you

94
00:06:07,290 --> 00:06:09,000
 develop them,

95
00:06:09,000 --> 00:06:13,260
 that leads to the increasing of the asavat, increasing more

96
00:06:13,260 --> 00:06:15,000
 desire, more sensual desire,

97
00:06:15,000 --> 00:06:19,390
 more desire to be this, to be that, ambition, to become

98
00:06:19,390 --> 00:06:20,000
 things,

99
00:06:20,000 --> 00:06:24,270
 and more delusion, more ignorance, which develops our

100
00:06:24,270 --> 00:06:26,000
 ignorance or create,

101
00:06:26,000 --> 00:06:30,000
 for instance, taking drugs or alcohol or so on.

102
00:06:30,000 --> 00:06:35,440
 So people who go around breaking the five precepts, or even

103
00:06:35,440 --> 00:06:37,000
 not breaking the five precepts,

104
00:06:37,000 --> 00:06:44,550
 but being unrestrained in their activities, in terms of

105
00:06:44,550 --> 00:06:46,000
 sensuality,

106
00:06:46,000 --> 00:06:50,380
 be it in romance or be it in food or be it in entertainment

107
00:06:50,380 --> 00:06:51,000
 or so on,

108
00:06:51,000 --> 00:06:54,560
 or people who are unrestrained in their ambitions or unrest

109
00:06:54,560 --> 00:06:56,000
rained in their delusions,

110
00:06:56,000 --> 00:07:00,000
 in their ignorance, they consider the wrong things,

111
00:07:00,000 --> 00:07:03,750
 they consider those things, when they see something, they

112
00:07:03,750 --> 00:07:06,000
 reach for it,

113
00:07:06,000 --> 00:07:12,320
 run after, they're always looking for more, looking for

114
00:07:12,320 --> 00:07:13,000
 something which is pleasurable,

115
00:07:13,000 --> 00:07:18,000
 something which will bring them sensual pleasure.

116
00:07:18,000 --> 00:07:21,990
 The Lord Buddha, another type of person, is a person who

117
00:07:21,990 --> 00:07:24,000
 doesn't attend upon

118
00:07:24,000 --> 00:07:27,000
 those things which are not to be attended upon and attends,

119
00:07:27,000 --> 00:07:31,000
 reflects upon those things which are to be reflected upon.

120
00:07:31,000 --> 00:07:33,560
 And those things which should be reflected upon are those

121
00:07:33,560 --> 00:07:35,000
 things which decrease

122
00:07:35,000 --> 00:07:39,000
 and do away with the asa-wa.

123
00:07:39,000 --> 00:07:44,180
 So, attending to mindfulness, developing morality and

124
00:07:44,180 --> 00:07:47,000
 concentration and wisdom,

125
00:07:47,000 --> 00:07:51,650
 developing the mind to become, to see clearly, to do away

126
00:07:51,650 --> 00:07:53,000
 with sensuality,

127
00:07:53,000 --> 00:07:55,550
 to see that happy feelings, neutral feelings, suffering

128
00:07:55,550 --> 00:07:58,000
 feelings, they're all impermanent.

129
00:07:58,000 --> 00:08:04,000
 To see that becoming this or becoming that, whatever we

130
00:08:04,000 --> 00:08:04,000
 become,

131
00:08:04,000 --> 00:08:08,000
 we will have to leave behind in the end.

132
00:08:08,000 --> 00:08:12,150
 And whatever we run away from, we'll always in the end

133
00:08:12,150 --> 00:08:13,000
 catch up with us,

134
00:08:13,000 --> 00:08:16,350
 we'll come back to us again and again and again, we'll

135
00:08:16,350 --> 00:08:21,000
 always be running away from that.

136
00:08:21,000 --> 00:08:24,250
 And coming to see clearly means to do away with our

137
00:08:24,250 --> 00:08:25,000
 ignorance,

138
00:08:25,000 --> 00:08:28,980
 to see clearly the truth of suffering, to see why we suffer

139
00:08:28,980 --> 00:08:29,000
,

140
00:08:29,000 --> 00:08:33,630
 to see the cessation and the path, the way out of suffering

141
00:08:33,630 --> 00:08:34,000
.

142
00:08:34,000 --> 00:08:37,540
 So, the Lord Buddha compared these two types of people and

143
00:08:37,540 --> 00:08:39,000
 then said,

144
00:08:39,000 --> 00:08:42,490
 "How is it that the person who attends carefully, who

145
00:08:42,490 --> 00:08:45,000
 considers things in the right manner,

146
00:08:45,000 --> 00:08:50,000
 not dwelling in the past or worrying about the future,

147
00:08:50,000 --> 00:08:54,000
 how is it that they come to be one who sees?"

148
00:08:54,000 --> 00:08:58,000
 So, this is the first part is through seeing.

149
00:08:58,000 --> 00:09:02,000
 And here the Lord Buddha taught the Four Noble Truths.

150
00:09:02,000 --> 00:09:09,010
 And we can explain that the Four Noble Truths are something

151
00:09:09,010 --> 00:09:11,000
 like the core of Buddhism

152
00:09:11,000 --> 00:09:16,120
 or the very basis of what it means, what is meant by the

153
00:09:16,120 --> 00:09:19,000
 word Buddhism or the Buddha's teaching.

154
00:09:19,000 --> 00:09:22,540
 It's the teaching which the Lord Buddha gave first when he

155
00:09:22,540 --> 00:09:26,000
 first set in motion the wheel of Dhamma.

156
00:09:26,000 --> 00:09:30,510
 After the Lord Buddha became enlightened under the Bodhi

157
00:09:30,510 --> 00:09:33,000
 tree in Bodhagaya,

158
00:09:33,000 --> 00:09:39,000
 in Makata, in India.

159
00:09:39,000 --> 00:09:45,000
 And then he walked the 120 kilometers or however far it was

160
00:09:45,000 --> 00:09:45,000
.

161
00:09:45,000 --> 00:09:52,830
 He walked to Isipatana in Saranat, which is just outside of

162
00:09:52,830 --> 00:09:54,000
 Varanasi.

163
00:09:54,000 --> 00:09:58,610
 And he taught the five ascetics, these are the five medit

164
00:09:58,610 --> 00:10:00,000
ators of the five ascetics

165
00:10:00,000 --> 00:10:05,110
 who followed after the Lord Buddha, the Bodhisatta, when he

166
00:10:05,110 --> 00:10:06,000
 left home.

167
00:10:06,000 --> 00:10:08,860
 Before he became Buddha, when he left home, he practiced

168
00:10:08,860 --> 00:10:11,000
 for six years torturing himself.

169
00:10:11,000 --> 00:10:16,310
 And these five monks went after him thinking that these

170
00:10:16,310 --> 00:10:19,000
 five ascetics went with him thinking that

171
00:10:19,000 --> 00:10:22,400
 if he became enlightened he would then share the teaching

172
00:10:22,400 --> 00:10:23,000
 with them

173
00:10:23,000 --> 00:10:26,000
 and they would be able to see clearly following after him.

174
00:10:26,000 --> 00:10:30,000
 It would be his first disciples.

175
00:10:30,000 --> 00:10:33,390
 But at that time the accepted practice was to torture

176
00:10:33,390 --> 00:10:34,000
 oneself,

177
00:10:34,000 --> 00:10:36,680
 either to live in the household life in luxury or to go in

178
00:10:36,680 --> 00:10:40,000
 the forest and torture oneself physically.

179
00:10:40,000 --> 00:10:43,160
 And the Lord Buddha, after six years, realized that tort

180
00:10:43,160 --> 00:10:44,000
uring himself physically

181
00:10:44,000 --> 00:10:47,620
 did nothing to bring him closer to wisdom and understanding

182
00:10:47,620 --> 00:10:48,000
.

183
00:10:48,000 --> 00:10:53,430
 It had no causal relationship between, casual relationship

184
00:10:53,430 --> 00:10:58,000
 with the realization of enlightenment.

185
00:10:58,000 --> 00:11:01,060
 And so he gave it up and he gave up the practice of tort

186
00:11:01,060 --> 00:11:02,000
uring himself

187
00:11:02,000 --> 00:11:05,120
 and found what is called the middle way, which is not tort

188
00:11:05,120 --> 00:11:08,000
uring oneself and not indulging in sensual pleasure.

189
00:11:08,000 --> 00:11:14,540
 It's simply being mindful, creating morality, doing away

190
00:11:14,540 --> 00:11:17,000
 with all in wholesome states,

191
00:11:17,000 --> 00:11:21,170
 calming the mind down, focusing the mind and concentration,

192
00:11:21,170 --> 00:11:24,000
 and developing an understanding,

193
00:11:24,000 --> 00:11:30,000
 which is simply in line with the reality around us.

194
00:11:30,000 --> 00:11:33,950
 But when he accepted the middle, when he started to

195
00:11:33,950 --> 00:11:35,000
 practice the middle way,

196
00:11:35,000 --> 00:11:38,880
 these five ascetics thought he was going back to his old

197
00:11:38,880 --> 00:11:41,000
 practice of indulging in luxury

198
00:11:41,000 --> 00:11:44,210
 and they knew that this wasn't, surely wasn't the way out

199
00:11:44,210 --> 00:11:46,000
 of suffering, so they left him.

200
00:11:46,000 --> 00:11:48,980
 And they went to Varanasi to Isipatana. So the Lord Buddha

201
00:11:48,980 --> 00:11:51,000
 walked back there to find them

202
00:11:51,000 --> 00:11:55,410
 and to teach them the teaching. Now at first, of course,

203
00:11:55,410 --> 00:11:57,000
 they weren't interested.

204
00:11:57,000 --> 00:12:01,020
 This is an interesting point. Nowadays we can see that

205
00:12:01,020 --> 00:12:04,000
 there's a parallel with

206
00:12:04,000 --> 00:12:07,400
 the first disciples of the Lord Buddha that nowadays most

207
00:12:07,400 --> 00:12:10,000
 people don't want to see the Buddhist teaching.

208
00:12:10,000 --> 00:12:13,000
 They don't want to hear teachings on suffering.

209
00:12:13,000 --> 00:12:17,380
 They don't want to hear about how sensual pleasures are not

210
00:12:17,380 --> 00:12:19,000
 going to satisfy them.

211
00:12:19,000 --> 00:12:21,820
 They don't want to hear about how the world is not going to

212
00:12:21,820 --> 00:12:23,000
 bring them happiness.

213
00:12:23,000 --> 00:12:25,180
 They don't want to hear about how they have to give up

214
00:12:25,180 --> 00:12:27,000
 their attachment to sensuality

215
00:12:27,000 --> 00:12:30,970
 in order to find happiness. Most people nowadays want to

216
00:12:30,970 --> 00:12:33,000
 find happiness in these things.

217
00:12:33,000 --> 00:12:37,260
 Or else we see the opposite, just like these ascetics, as

218
00:12:37,260 --> 00:12:39,000
 people have gone out of their way

219
00:12:39,000 --> 00:12:42,100
 to torture themselves in many different ways, either the

220
00:12:42,100 --> 00:12:43,000
 body or the mind.

221
00:12:43,000 --> 00:12:46,660
 People developing all sorts of wrong views, all sorts of

222
00:12:46,660 --> 00:12:48,000
 views based on science,

223
00:12:48,000 --> 00:12:55,000
 what they call Western science or even Eastern science.

224
00:12:55,000 --> 00:13:00,000
 They have the different sciences in the world.

225
00:13:00,000 --> 00:13:03,420
 These are different views, different understandings of the

226
00:13:03,420 --> 00:13:04,000
 world.

227
00:13:04,000 --> 00:13:07,110
 And we develop these views, and when it comes to the

228
00:13:07,110 --> 00:13:08,000
 teaching of the Lord Buddha,

229
00:13:08,000 --> 00:13:10,690
 they're not really interested. They don't really see the

230
00:13:10,690 --> 00:13:11,000
 point.

231
00:13:11,000 --> 00:13:13,660
 Most people nowadays will believe that at the moment of

232
00:13:13,660 --> 00:13:16,000
 death there's nothing more.

233
00:13:16,000 --> 00:13:18,710
 And at the time of the Buddha this view was prevalent as

234
00:13:18,710 --> 00:13:22,000
 well, that death is the end of existence.

235
00:13:22,000 --> 00:13:26,000
 At the moment of death there is nothing.

236
00:13:26,000 --> 00:13:29,360
 And this of course comes with an understanding based on

237
00:13:29,360 --> 00:13:34,000
 what is called science of the death of another person.

238
00:13:34,000 --> 00:13:38,230
 Because of course none of us can empirically measure our

239
00:13:38,230 --> 00:13:41,000
 own death until it comes.

240
00:13:41,000 --> 00:13:44,920
 But we can measure someone else's death and from all

241
00:13:44,920 --> 00:13:50,000
 appearances it looks as though the person in front of us

242
00:13:50,000 --> 00:13:54,000
 has died, has ceased to exist.

243
00:13:54,000 --> 00:13:58,000
 But empirically of course things are quite different

244
00:13:58,000 --> 00:14:01,000
 because empirically inside we can see that

245
00:14:01,000 --> 00:14:03,790
 the objects around us are arising and ceasing, seeing,

246
00:14:03,790 --> 00:14:07,000
 hearing, smelling, tasting, feeling and thinking.

247
00:14:07,000 --> 00:14:09,410
 They're arising and ceasing all the time and there's no

248
00:14:09,410 --> 00:14:13,000
 cessation, there's no ceasing to exist.

249
00:14:13,000 --> 00:14:17,660
 Now at the moment of death as to what happens is up to

250
00:14:17,660 --> 00:14:19,000
 speculation.

251
00:14:19,000 --> 00:14:22,270
 And that's all it really is, is this speculation that at

252
00:14:22,270 --> 00:14:24,000
 the moment of death there is nothing more.

253
00:14:24,000 --> 00:14:30,000
 Somehow this process of arising and ceasing somehow stops.

254
00:14:30,000 --> 00:14:33,900
 And of course we'll all have to see for ourselves when the

255
00:14:33,900 --> 00:14:35,000
 time comes.

256
00:14:35,000 --> 00:14:41,120
 But at any rate these five ascetics were an example of how

257
00:14:41,120 --> 00:14:43,000
 these sort of wrong views can get in the way.

258
00:14:43,000 --> 00:14:45,600
 Because at first they weren't willing to listen to the Lord

259
00:14:45,600 --> 00:14:48,000
 Buddha and this is also very common nowadays.

260
00:14:48,000 --> 00:14:52,170
 People not interested in hearing or not interested in

261
00:14:52,170 --> 00:14:54,000
 taking on the teaching,

262
00:14:54,000 --> 00:14:58,050
 not interested in trying it out because of their own views

263
00:14:58,050 --> 00:15:01,000
 and their own way of looking at things.

264
00:15:01,000 --> 00:15:04,810
 The Lord Buddha was able to convince the five ascetics that

265
00:15:04,810 --> 00:15:07,000
 something was indeed different.

266
00:15:07,000 --> 00:15:10,710
 At the very least he had changed in some way and perhaps

267
00:15:10,710 --> 00:15:13,000
 they could listen to what he had to say.

268
00:15:13,000 --> 00:15:19,810
 And they became malleable and they were able to let go of

269
00:15:19,810 --> 00:15:23,000
 their view long enough to listen.

270
00:15:23,000 --> 00:15:28,000
 At least to consider what the Lord Buddha was going to say.

271
00:15:28,000 --> 00:15:30,600
 And first he taught that there was a middle way and then he

272
00:15:30,600 --> 00:15:33,000
 explained what were the Four Noble Truths.

273
00:15:33,000 --> 00:15:36,320
 And this became the core of Buddhism. So this is what we

274
00:15:36,320 --> 00:15:38,000
 have to see in Buddhism.

275
00:15:38,000 --> 00:15:40,870
 When we practice the Buddhist teaching, what does it mean

276
00:15:40,870 --> 00:15:44,000
 to do away with evil unwholesome states through seeing?

277
00:15:44,000 --> 00:15:48,000
 It means to see four things. To see the cause of suffering.

278
00:15:48,000 --> 00:15:51,150
 To see suffering, sorry. To see what is the truth of

279
00:15:51,150 --> 00:15:54,000
 suffering. To see what is the cause of suffering.

280
00:15:54,000 --> 00:15:57,280
 To see the cessation of suffering and to see the path, the

281
00:15:57,280 --> 00:16:00,000
 practice which leads to the cessation of suffering.

282
00:16:00,000 --> 00:16:04,790
 These four things means to do away with unwholesome states

283
00:16:04,790 --> 00:16:06,000
 through seeing.

284
00:16:06,000 --> 00:16:08,000
 And how does it work?

285
00:16:08,000 --> 00:16:11,040
 First of all, it's important to understand this being the

286
00:16:11,040 --> 00:16:14,160
 core of Buddhism that it's clear that the Lord Buddha

287
00:16:14,160 --> 00:16:15,000
 taught suffering.

288
00:16:15,000 --> 00:16:18,000
 He taught all about suffering.

289
00:16:18,000 --> 00:16:22,000
 And even though the word suffering is only an English word,

290
00:16:22,000 --> 00:16:24,000
 it really is an appropriate word.

291
00:16:24,000 --> 00:16:28,000
 It's important not to water down the Lord Buddha's teaching

292
00:16:28,000 --> 00:16:31,000
 or try to find a way around it to make people feel happy.

293
00:16:31,000 --> 00:16:34,640
 Make people feel comforted that actually the Buddha really

294
00:16:34,640 --> 00:16:36,000
 taught happiness.

295
00:16:36,000 --> 00:16:40,000
 The truth is the Lord Buddha taught the ultimate happiness,

296
00:16:40,000 --> 00:16:43,000
 the ultimate freedom from suffering.

297
00:16:43,000 --> 00:16:46,740
 He taught that real happiness is simply having no suffering

298
00:16:46,740 --> 00:16:47,000
.

299
00:16:47,000 --> 00:16:50,660
 Just like perfect health, for a doctor, perfect health is

300
00:16:50,660 --> 00:16:52,000
 freedom from sickness.

301
00:16:52,000 --> 00:16:55,370
 So the doctor will never talk or be concerned very much

302
00:16:55,370 --> 00:16:57,000
 about perfect health.

303
00:16:57,000 --> 00:17:00,000
 He'll always be concerned about the sickness.

304
00:17:00,000 --> 00:17:03,430
 If a person is healthy then there's nothing that the doctor

305
00:17:03,430 --> 00:17:05,000
 has to do to go in and fix.

306
00:17:05,000 --> 00:17:08,950
 But for a person who is sick, for whatever sicknesses exist

307
00:17:08,950 --> 00:17:09,000
,

308
00:17:09,000 --> 00:17:14,060
 it is the doctor's duty to go in and to explain and to give

309
00:17:14,060 --> 00:17:15,000
 medicine

310
00:17:15,000 --> 00:17:18,840
 and have the patient take medicine in order to be free from

311
00:17:18,840 --> 00:17:20,000
 the sickness.

312
00:17:20,000 --> 00:17:23,000
 So the Lord is exactly like a doctor.

313
00:17:23,000 --> 00:17:27,000
 Except he had a much more difficult time than the doctor.

314
00:17:27,000 --> 00:17:30,560
 Doctors will have a difficult time with patients who don't

315
00:17:30,560 --> 00:17:33,000
 want to believe that they are sick,

316
00:17:33,000 --> 00:17:37,000
 who don't want to accept the fact that they have sickness.

317
00:17:37,000 --> 00:17:40,000
 But of course the Buddha had a terrible time.

318
00:17:40,000 --> 00:17:43,000
 Because no one wanted to consider that they were sick.

319
00:17:43,000 --> 00:17:46,280
 No one wanted to think that these things inside of them

320
00:17:46,280 --> 00:17:47,000
 were wrong,

321
00:17:47,000 --> 00:17:51,210
 that the views and the conceits and their attachments and

322
00:17:51,210 --> 00:17:52,000
 their cravings and their wantings

323
00:17:52,000 --> 00:17:55,000
 and all of these things were wrong.

324
00:17:55,000 --> 00:18:00,580
 And of course in those times, just as today, people only

325
00:18:00,580 --> 00:18:05,000
 want to follow after their desires.

326
00:18:05,000 --> 00:18:08,000
 And of course that's what they want, desire is wanting.

327
00:18:08,000 --> 00:18:11,000
 So they have this wanting.

328
00:18:11,000 --> 00:18:15,500
 And as a result of this wanting, any talk about giving up

329
00:18:15,500 --> 00:18:18,970
 the things which they want is, of course, an unwelcome

330
00:18:18,970 --> 00:18:20,000
 thing.

331
00:18:20,000 --> 00:18:22,210
 And so often find the people who are most able to

332
00:18:22,210 --> 00:18:24,000
 understand the Buddhist teachings

333
00:18:24,000 --> 00:18:28,000
 are those who have had their hopes dashed

334
00:18:28,000 --> 00:18:34,000
 or had their love, their objects of affection disappear.

335
00:18:34,000 --> 00:18:37,000
 Be they people or objects or so on.

336
00:18:37,000 --> 00:18:42,320
 People who have seen what it means to be without the

337
00:18:42,320 --> 00:18:45,000
 objects of our attachment.

338
00:18:45,000 --> 00:18:47,690
 People who have lost loved ones, people who have gone

339
00:18:47,690 --> 00:18:51,000
 through states of loss or suffering.

340
00:18:51,000 --> 00:18:55,000
 People who have sicknesses, people who are dying.

341
00:18:55,000 --> 00:18:58,060
 These people are the ones who are able to understand, yes,

342
00:18:58,060 --> 00:18:59,000
 what the Buddha said was really true.

343
00:18:59,000 --> 00:19:02,000
 There is great sickness here.

344
00:19:02,000 --> 00:19:05,000
 Even people who are in perfect health physically.

345
00:19:05,000 --> 00:19:08,000
 But when they lose a loved one, they might kill themselves.

346
00:19:08,000 --> 00:19:11,610
 Simply because they can't stand to live without the other

347
00:19:11,610 --> 00:19:15,000
 person or without this or without that.

348
00:19:15,000 --> 00:19:17,470
 And so these people are able to see that there is a great

349
00:19:17,470 --> 00:19:20,000
 sickness which they are not able to,

350
00:19:20,000 --> 00:19:24,820
 which medicine, which they by themselves are not able to

351
00:19:24,820 --> 00:19:26,000
 overcome.

352
00:19:26,000 --> 00:19:30,470
 And so they come to say, "Well, who is it or what is it

353
00:19:30,470 --> 00:19:33,000
 that will make us be happy now?"

354
00:19:33,000 --> 00:19:36,080
 And of course the objects of the sense are not going to

355
00:19:36,080 --> 00:19:38,000
 make them happy at that time.

356
00:19:38,000 --> 00:19:43,460
 They can drown their sorrows in alcohol or in drugs for so

357
00:19:43,460 --> 00:19:44,000
 long.

358
00:19:44,000 --> 00:19:46,500
 And if they are smart they'll be able to realize that that

359
00:19:46,500 --> 00:19:48,000
's not going to lead them out.

360
00:19:48,000 --> 00:19:52,350
 If they are not able to realize that then they will drown

361
00:19:52,350 --> 00:19:54,000
 themselves until they die

362
00:19:54,000 --> 00:20:04,000
 and they will go on to be reborn in a very terrible place.

363
00:20:04,000 --> 00:20:07,080
 But people, in general in the world, people who are

364
00:20:07,080 --> 00:20:10,000
 successful, who have everything they want,

365
00:20:10,000 --> 00:20:15,510
 who seem to have everything they want, are very good at

366
00:20:15,510 --> 00:20:17,000
 convincing themselves

367
00:20:17,000 --> 00:20:23,060
 and trying to convince everyone around them and refusing

368
00:20:23,060 --> 00:20:24,000
 any idea to the contrary

369
00:20:24,000 --> 00:20:28,500
 that they themselves have great happiness, that they

370
00:20:28,500 --> 00:20:30,000
 themselves are always happy,

371
00:20:30,000 --> 00:20:36,650
 that they themselves are not moved, not affected by

372
00:20:36,650 --> 00:20:38,000
 suffering.

373
00:20:38,000 --> 00:20:42,000
 And whenever suffering arises they quickly forget about it,

374
00:20:42,000 --> 00:20:44,000
 they quickly put it behind them.

375
00:20:44,000 --> 00:20:47,620
 During the time it's there they cry and they moan and they

376
00:20:47,620 --> 00:20:52,000
 wail and they take all sorts of measures

377
00:20:52,000 --> 00:20:58,000
 to become free or to forget the suffering.

378
00:20:58,000 --> 00:21:00,250
 And then when it's gone they quickly put it behind them and

379
00:21:00,250 --> 00:21:02,000
 pretend it didn't happen

380
00:21:02,000 --> 00:21:04,600
 and pretend that it forget that it might happen in the

381
00:21:04,600 --> 00:21:05,000
 future

382
00:21:05,000 --> 00:21:11,290
 or never stop to consider that it very well might come at

383
00:21:11,290 --> 00:21:13,000
 any time again.

384
00:21:13,000 --> 00:21:14,550
 And they say again to themselves that they are in great

385
00:21:14,550 --> 00:21:15,000
 happiness.

386
00:21:15,000 --> 00:21:18,330
 So the Lord Buddha gave this teaching which is very

387
00:21:18,330 --> 00:21:19,000
 powerful

388
00:21:19,000 --> 00:21:23,000
 and it's important that it be powerful, that it be working

389
00:21:23,000 --> 00:21:27,000
 because otherwise people will never wake themselves up,

390
00:21:27,000 --> 00:21:30,450
 will be lost and will continue to destroy this world and to

391
00:21:30,450 --> 00:21:32,000
 destroy themselves

392
00:21:32,000 --> 00:21:35,500
 and make it a less and less harmonious and peaceful place

393
00:21:35,500 --> 00:21:36,000
 to live

394
00:21:36,000 --> 00:21:39,300
 until we can realize that we are destroying ourselves and

395
00:21:39,300 --> 00:21:41,000
 this world around us,

396
00:21:41,000 --> 00:21:45,440
 until we can realize that we will never become free from

397
00:21:45,440 --> 00:21:46,000
 suffering,

398
00:21:46,000 --> 00:21:50,140
 we will never be free from the threat of suffering in the

399
00:21:50,140 --> 00:21:51,000
 future.

400
00:21:51,000 --> 00:21:54,000
 So the Lord Buddha taught that there is suffering,

401
00:21:54,000 --> 00:22:01,340
 that there is a problem with simply living our lives in ind

402
00:22:01,340 --> 00:22:03,000
olence,

403
00:22:03,000 --> 00:22:08,570
 living our lives as though there were no tomorrow or there

404
00:22:08,570 --> 00:22:14,000
 are no consequences of our actions.

405
00:22:14,000 --> 00:22:19,000
 And he taught this so that people who were able to see

406
00:22:19,000 --> 00:22:25,000
 would be able to have a way out.

407
00:22:25,000 --> 00:22:29,170
 So people who had seen suffering or who could remember when

408
00:22:29,170 --> 00:22:31,000
 the Lord Buddha talked about this being suffering

409
00:22:31,000 --> 00:22:34,000
 and that being suffering, they would be able to recollect

410
00:22:34,000 --> 00:22:35,000
 the suffering which they had in their lives

411
00:22:35,000 --> 00:22:38,000
 and how they were truly unable to deal with it.

412
00:22:38,000 --> 00:22:40,570
 Or when they saw suffering in other people, those people

413
00:22:40,570 --> 00:22:42,000
 were truly unable to deal with it.

414
00:22:42,000 --> 00:22:49,620
 And he taught this giving people an understanding of the

415
00:22:49,620 --> 00:22:51,000
 truth of this,

416
00:22:51,000 --> 00:22:54,060
 the truth of this fact that there was a problem which had

417
00:22:54,060 --> 00:22:56,000
 to be addressed very quickly

418
00:22:56,000 --> 00:23:00,570
 before the problem came to us or became so desperate that

419
00:23:00,570 --> 00:23:03,000
 we weren't able to cure it.

420
00:23:03,000 --> 00:23:08,810
 And then he taught the cure, or he taught the reason why

421
00:23:08,810 --> 00:23:11,000
 the suffering exists

422
00:23:11,000 --> 00:23:17,120
 and how we could cure it, how we could remove the cause and

423
00:23:17,120 --> 00:23:19,000
 find a cure.

424
00:23:19,000 --> 00:23:24,950
 So the Lord Buddha taught the cause of suffering is called

425
00:23:24,950 --> 00:23:27,000
 tanha, or craving.

426
00:23:27,000 --> 00:23:29,940
 And you can translate it as you will, craving is probably

427
00:23:29,940 --> 00:23:31,000
 the best translation.

428
00:23:31,000 --> 00:23:36,150
 But it means any sort of greed or wishing or longing for

429
00:23:36,150 --> 00:23:39,000
 things to be like this or not be like that

430
00:23:39,000 --> 00:23:46,000
 or wishing for sensual desires, sensual pleasures.

431
00:23:46,000 --> 00:23:49,420
 And we can see this when we practice meditation quite

432
00:23:49,420 --> 00:23:50,000
 clearly,

433
00:23:50,000 --> 00:23:53,000
 and this is what the Lord Buddha meant by seeing.

434
00:23:53,000 --> 00:23:55,920
 First of all, seeing suffering when we sit and we are

435
00:23:55,920 --> 00:23:57,000
 rising, falling,

436
00:23:57,000 --> 00:24:01,010
 we can see our minds wandering here, wandering there

437
00:24:01,010 --> 00:24:02,000
 without any rhyme or reason.

438
00:24:02,000 --> 00:24:07,590
 And we can see how this creates stress in the body and

439
00:24:07,590 --> 00:24:10,000
 stress and headaches

440
00:24:10,000 --> 00:24:14,000
 and how it creates a general sense of unease.

441
00:24:14,000 --> 00:24:17,640
 Or when we are wishing for things or wanting for things to

442
00:24:17,640 --> 00:24:19,000
 be like this or that.

443
00:24:19,000 --> 00:24:22,600
 Or when we hold on, when we have a hailing and we really

444
00:24:22,600 --> 00:24:25,000
 enjoy it and really like the feeling.

445
00:24:25,000 --> 00:24:27,370
 How this then leads us to suffer when the feeling

446
00:24:27,370 --> 00:24:28,000
 disappears.

447
00:24:28,000 --> 00:24:32,000
 And we find in the morning our practice was very peaceful.

448
00:24:32,000 --> 00:24:35,840
 But then in the afternoon when the feeling went away our

449
00:24:35,840 --> 00:24:38,000
 practice was very horrible.

450
00:24:38,000 --> 00:24:41,140
 And we can see in this way how our craving, our wanting for

451
00:24:41,140 --> 00:24:43,000
 things to be like this or be like that

452
00:24:43,000 --> 00:24:47,000
 is really and truly the cause for suffering for us.

453
00:24:47,000 --> 00:24:53,240
 And this is what the Lord Buddha meant by "tasanapa hatapa

454
00:24:53,240 --> 00:24:54,000
".

455
00:24:54,000 --> 00:24:59,000
 To remove these things through simply seeing this.

456
00:24:59,000 --> 00:25:02,790
 Next time we feel happy we won't be so quick to reach for

457
00:25:02,790 --> 00:25:06,000
 it or grab for it or hold on to it.

458
00:25:06,000 --> 00:25:09,000
 We will be more wary and see the happy feeling simply for

459
00:25:09,000 --> 00:25:09,000
 what it is,

460
00:25:09,000 --> 00:25:12,000
 as something that's impermanent and therefore unsatisfying.

461
00:25:12,000 --> 00:25:16,150
 Unsatisfying and we won't hold on to it as "me" is "mine"

462
00:25:16,150 --> 00:25:18,000
 is under my control.

463
00:25:18,000 --> 00:25:27,370
 And when we see like this we will not have to then come in

464
00:25:27,370 --> 00:25:29,000
 contact with the suffering.

465
00:25:29,000 --> 00:25:32,770
 We will then not have to become party to the suffering

466
00:25:32,770 --> 00:25:35,000
 which results from craving.

467
00:25:35,000 --> 00:25:43,410
 We will be able to see through all pleasurable states, all

468
00:25:43,410 --> 00:25:45,000
 unpleasurable states.

469
00:25:45,000 --> 00:25:49,440
 All states which arise in our body or our mind will be able

470
00:25:49,440 --> 00:25:51,000
 to see through them

471
00:25:51,000 --> 00:25:56,290
 and not be moved by them or be pushed into craving and

472
00:25:56,290 --> 00:26:01,000
 therefore clinging and therefore suffering.

473
00:26:01,000 --> 00:26:05,000
 And this is the cessation of suffering. When there is no

474
00:26:05,000 --> 00:26:05,000
 more craving,

475
00:26:05,000 --> 00:26:09,410
 when we are no longer reaching out for these things because

476
00:26:09,410 --> 00:26:12,000
 we have seen how they are not truly happiness,

477
00:26:12,000 --> 00:26:14,000
 then this is the cessation of suffering.

478
00:26:14,000 --> 00:26:17,250
 There is no, no matter how much pain we might be in because

479
00:26:17,250 --> 00:26:21,000
 we have no attachment to happy feelings or unhappy feelings

480
00:26:21,000 --> 00:26:21,000
.

481
00:26:21,000 --> 00:26:26,000
 It can, these feelings cannot bring suffering to us.

482
00:26:26,000 --> 00:26:28,940
 Wanting to be this, wanting to be that, holding on to the

483
00:26:28,940 --> 00:26:30,000
 past or the future.

484
00:26:30,000 --> 00:26:33,000
 These things can no longer affect us.

485
00:26:33,000 --> 00:26:36,550
 There is no more holding on to either past or future good

486
00:26:36,550 --> 00:26:37,000
 or bad.

487
00:26:37,000 --> 00:26:40,570
 Through simply seeing that this is suffering and there is a

488
00:26:40,570 --> 00:26:44,000
 cause, the cause is that very holding on.

489
00:26:44,000 --> 00:26:47,330
 And the path, the fourth noble truth which we see is the

490
00:26:47,330 --> 00:26:49,000
 path which leads there too,

491
00:26:49,000 --> 00:26:52,000
 which is of course the path of mindfulness.

492
00:26:52,000 --> 00:26:54,570
 When we simply acknowledge rising, falling or when we are

493
00:26:54,570 --> 00:26:56,000
 walking and we say,

494
00:26:56,000 --> 00:26:59,420
 "Stepping right, stepping left," and we are able to see

495
00:26:59,420 --> 00:27:04,000
 clearly the truth of the body and the truth of the mind.

496
00:27:04,000 --> 00:27:07,370
 We are able to see good states, bad states, suffering, what

497
00:27:07,370 --> 00:27:09,000
 is the cause of suffering.

498
00:27:09,000 --> 00:27:12,000
 We do this by simply watching.

499
00:27:12,000 --> 00:27:15,000
 There is a saying, "If you want to know, you have to see.

500
00:27:15,000 --> 00:27:17,000
 If you want to see, you have to look.

501
00:27:17,000 --> 00:27:21,000
 If you look, you will see. If you see, you will know."

502
00:27:21,000 --> 00:27:26,000
 These three things, this is through seeing.

503
00:27:26,000 --> 00:27:29,850
 The first of the Lord Buddha's teachings on how to do away

504
00:27:29,850 --> 00:27:31,000
 with the Asa-wa.

505
00:27:31,000 --> 00:27:33,000
 So that is the Dhamma for today.

506
00:27:33,000 --> 00:27:38,000
 Tomorrow we will go on to the other parts of the teaching

507
00:27:38,000 --> 00:27:42,000
 on how to do away with the Asa-wa.

508
00:27:42,000 --> 00:27:44,000
 That's all for today.

