1
00:00:00,000 --> 00:00:17,250
 This is my latest project. It's called Ask a Monk. And this

2
00:00:17,250 --> 00:00:25,720
 is something that I think

3
00:00:25,720 --> 00:00:31,600
 I mentioned already. I put it together based on these many

4
00:00:31,600 --> 00:00:34,520
 other Ask Somebody or Ask Somebody.

5
00:00:34,520 --> 00:00:47,090
 And so here's a list of the questions that people have

6
00:00:47,090 --> 00:00:54,320
 asked. And what they do is they just post

7
00:00:54,320 --> 00:00:57,090
 questions and then people can rate them. So they have all

8
00:00:57,090 --> 00:00:58,720
 these questions that they want to ask

9
00:00:58,720 --> 00:01:05,900
 me about Buddhism and meditation and so on. And then I just

10
00:01:05,900 --> 00:01:09,640
 look at them and I see which ones are

11
00:01:09,640 --> 00:01:12,960
 are getting a lot of votes or else I look and see which

12
00:01:12,960 --> 00:01:15,640
 ones I think are appropriate to answer and

13
00:01:15,640 --> 00:01:21,840
 I answer them. And how I answer them is I make YouTube

14
00:01:21,840 --> 00:01:26,080
 videos. So the reply to this, I haven't

15
00:01:26,080 --> 00:01:30,670
 put any responses yet, but I would post a response and then

16
00:01:30,670 --> 00:01:33,320
 just put a YouTube video and a link to

17
00:01:33,320 --> 00:01:36,380
 the YouTube video in here. And then I make the video,

18
00:01:36,380 --> 00:01:39,600
 YouTube video, post it and then put the

19
00:01:39,600 --> 00:01:44,700
 response here. I've just started this and already I've got

20
00:01:44,700 --> 00:01:48,360
 eight questions. You see how many questions?

21
00:01:48,360 --> 00:01:54,700
 Eight questions so far. And 43 votes. So people are voting

22
00:01:54,700 --> 00:01:57,680
 on them as well, which means I better

23
00:01:57,680 --> 00:02:10,250
 get at it and start answering them. Here's the first video.

24
00:02:10,250 --> 00:02:15,000
 I'm the internet slow because I'm

25
00:02:15,000 --> 00:02:20,600
 also uploading my life in a day videos. Okay, so this is

26
00:02:20,600 --> 00:02:29,040
 just one more thing that I do. And that's that.

27
00:02:29,040 --> 00:02:59,040
 [

