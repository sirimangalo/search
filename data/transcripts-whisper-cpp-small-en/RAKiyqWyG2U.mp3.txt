 When you are focusing on your breath, should you keep a
 peripheral awareness to your passing thoughts?
 Because I found that if I'm deeply aware of the breath or
 another object, I just notice a thought after it has passed
 and not in the moment.
 Well, no, you shouldn't. The problem isn't that you're too
 focused on the breath. The problem is that your mind still
...
 Well, it's not even a problem, but it's not the ideal mind
 state. Of course, the ideal mind state is freedom from
 thought, even freedom from breath.
 I mean, the ideal mind state is extinguishing when there is
 release or freedom.
 But the problem that you have is that your lack of
 mindfulness is not allowing you to catch the thought when
 it starts.
 It has nothing to do with being too absorbed in the breath.
 When you're watching the breath, if you're mindful, if you
're clearly aware,
 you should be able to see as soon as the thought arises.
 And as soon as the thought arises, you'll be able to
 acknowledge it as thinking.
 It's true that you can't actually be aware of an object as
 it's happening.
 Sorry, you can't actually be mindful of an object as it's
 happening. The object arises and there's the awareness, and
 then there's the mindfulness.
 So you're actually technically not mindful of something at
 the moment that it occurs, but it's so quick. It's thought
 moments. It's one thought moment and the next thought
 moment.
 But the fact that you can't catch it until after it's over
 is a sign that during that thought there was lack of
 mindfulness.
 Each thought moment until you caught it was a mind with
 delusion in it.
 So that being the case, I would suggest that your attention
 on the breath was void of wisdom and void of mindfulness.
 So perhaps I had delusion involved in it. If there is a
 diluted mind state, even watching the breath, there will be
 conceit, there will be view of self, there will be craving.
 As a result, the moment of the thought arising will come
 without awareness of the thought, and that's how you get
 carried away.
 Practically speaking, a beginner will only catch the
 thoughts at the end after you've thought through the whole
 subject.
 An advanced meditator, as they progress, they'll be able to
 catch the thoughts earlier and earlier and earlier.
 And a meditator who is truly in the zone will be able to
 catch the thoughts as soon as they arise.
 So the thought arises in the mind, the meditator is aware
 that the thought arose, just one thought subject, and then
 it ceases.
 And the meditator comes back to the main object.
 So know you shouldn't keep tabs on something else while you
're focusing on the main object.
 And know it isn't caused by the breath, it's caused by
 delusion, which may be indicative of improper observation
 of the breath.
 So starting out from a diluted state, which carries on into
 the thought, allows the thought to sneak in there.
