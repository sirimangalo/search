1
00:00:00,000 --> 00:00:05,300
 precepts. So when you take precepts you purify your moral

2
00:00:05,300 --> 00:00:05,560
 contact and so these

3
00:00:05,560 --> 00:00:11,370
 three are there. So these three are not actively working at

4
00:00:11,370 --> 00:00:12,200
 the moment of

5
00:00:12,200 --> 00:00:16,600
 Vipassana but since they are accomplished before Vipassana

6
00:00:16,600 --> 00:00:17,320
 they are

7
00:00:17,320 --> 00:00:21,160
 said to be working maybe in the background. So the five

8
00:00:21,160 --> 00:00:22,020
 factors right

9
00:00:22,020 --> 00:00:24,930
 understanding, right thought, right effort, right

10
00:00:24,930 --> 00:00:26,840
 mindfulness, right concentration

11
00:00:26,840 --> 00:00:32,960
 are called the worker factors, working factors. So they are

12
00:00:32,960 --> 00:00:35,060
 the ones that should

13
00:00:35,060 --> 00:00:40,120
 be working harmoniously so that you get good concentration,

14
00:00:40,120 --> 00:00:41,240
 good meditation and

15
00:00:41,240 --> 00:00:51,410
 good understanding and right knowledge of the true nature

16
00:00:51,410 --> 00:00:54,120
 of mind and matter.

17
00:00:55,720 --> 00:01:01,980
 Okay these are the four noble truths and the eight factors

18
00:01:01,980 --> 00:01:03,240
 of the fourth

19
00:01:03,240 --> 00:01:09,160
 noble truth in some detail. So you may read other books if

20
00:01:09,160 --> 00:01:09,560
 you want to

21
00:01:09,560 --> 00:01:14,920
 know more about and more detail about the Eightfold Path

22
00:01:14,920 --> 00:01:17,840
 and the four noble

23
00:01:17,840 --> 00:01:24,260
 truths. This is the course on fundamentals of Buddhism and

24
00:01:24,260 --> 00:01:26,000
 so I think it should be

25
00:01:26,000 --> 00:01:35,240
 enough for with just this much exposition. So today we

26
00:01:35,240 --> 00:01:37,680
 finish our talk on

27
00:01:37,680 --> 00:01:42,840
 the four noble truths and these are the four noble truths

28
00:01:42,840 --> 00:01:44,680
 discovered by the

29
00:01:44,680 --> 00:01:50,570
 Buddha and revealed to the world and among these four noble

30
00:01:50,570 --> 00:01:52,520
 truths we should

31
00:01:52,520 --> 00:01:58,800
 be more concerned with the fourth noble truth because it is

32
00:01:58,800 --> 00:02:00,280
 a fourth noble truth

33
00:02:00,280 --> 00:02:09,080
 that that will lead us to this suggestion of suffering. So

34
00:02:09,080 --> 00:02:11,520
 may we all

35
00:02:11,520 --> 00:02:17,830
 practice the fourth noble truth and reach the highest goal

36
00:02:17,830 --> 00:02:21,680
 in this very life.

37
00:02:21,680 --> 00:02:39,600
 Thank you in more detail. Now you know the eight factors of

38
00:02:39,600 --> 00:02:41,120
 path which is right

39
00:02:41,120 --> 00:02:44,520
 concentration. So when Buddha explained that right

40
00:02:44,520 --> 00:02:45,920
 concentration Buddha mentioned

41
00:02:45,920 --> 00:02:48,760
 the four jhanas.

42
00:02:48,760 --> 00:03:07,720
 Now the commentary explained that these four jhanas are not

43
00:03:07,720 --> 00:03:09,840
 only mundane jhanas

44
00:03:09,840 --> 00:03:15,220
 but also we must take to these jhanas to mean super mundane

45
00:03:15,220 --> 00:03:18,800
 jhanas. So the right

46
00:03:18,800 --> 00:03:26,650
 concentration means not mundane jhanas as we might think at

47
00:03:26,650 --> 00:03:29,280
 a first glance but we

48
00:03:29,280 --> 00:03:33,770
 must understand that it includes all though the super

49
00:03:33,770 --> 00:03:38,680
 mundane jhanas. Now in

50
00:03:38,680 --> 00:03:43,680
 order to understand it first you should understand the jhan

51
00:03:43,680 --> 00:03:46,360
as. Now jhanas are the

52
00:03:46,360 --> 00:03:55,410
 higher states of consciousness. Now these are experienced

53
00:03:55,410 --> 00:03:57,880
 by those who practice

54
00:03:57,880 --> 00:04:04,480
 samatha meditation. So jhanas can be obtained through the

55
00:04:04,480 --> 00:04:06,200
 practice of samatha

56
00:04:06,200 --> 00:04:23,200
 meditation and these jhanas take as object that is the

57
00:04:23,200 --> 00:04:26,600
 object of the practice

58
00:04:26,600 --> 00:04:32,960
 leading to the attainment of jhana or the object of samatha

59
00:04:32,960 --> 00:04:35,520
 meditation. For

60
00:04:35,520 --> 00:04:40,160
 example a person practices samatha meditation taking the

61
00:04:40,160 --> 00:04:42,040
 earth disk as an

62
00:04:42,040 --> 00:04:46,780
 object. So he looks at the earth disk and then he memorizes

63
00:04:46,780 --> 00:04:49,000
 it and let us say he

64
00:04:49,000 --> 00:04:58,840
 has memorized it so completely that he could see that the

65
00:04:58,840 --> 00:05:00,640
 disk or he could

66
00:05:00,640 --> 00:05:04,780
 take the image of that disk in his mind and then he dwells

67
00:05:04,780 --> 00:05:06,000
 on it again and again

68
00:05:06,000 --> 00:05:12,160
 so that it becomes more refined and then he dwells on it

69
00:05:12,160 --> 00:05:14,720
 again and then he gets

70
00:05:14,720 --> 00:05:21,450
 jhana. So the jhana when it arises takes that object as an

71
00:05:21,450 --> 00:05:25,680
 object the refined

72
00:05:25,680 --> 00:05:30,020
 mental image of the earth disk. This is just an example

73
00:05:30,020 --> 00:05:31,400
 there are many many

74
00:05:31,400 --> 00:05:41,780
 objects also. So when jhana arises it takes that object the

75
00:05:41,780 --> 00:05:44,280
 mental image as an

76
00:05:44,280 --> 00:05:53,960
 object and it pushes the mental defilements away for some

77
00:05:53,960 --> 00:05:56,040
 time. That means

78
00:05:56,040 --> 00:06:02,880
 jhana cannot eradicate cannot destroy any any of the mental

79
00:06:02,880 --> 00:06:04,280
 defilements but

80
00:06:04,280 --> 00:06:08,520
 jhana can push these mental defilements away for some time

81
00:06:08,520 --> 00:06:09,840
 so that they do not

82
00:06:09,840 --> 00:06:16,220
 arise in him for some time. So that is the nature of mand

83
00:06:16,220 --> 00:06:18,760
ing jhana. So again

84
00:06:18,760 --> 00:06:25,990
 manding jhana take their respective objects as object and

85
00:06:25,990 --> 00:06:29,200
 they just push the

86
00:06:29,200 --> 00:06:34,830
 mental defilements away for some time. They cannot

87
00:06:34,830 --> 00:06:36,480
 eradicate that means

88
00:06:36,480 --> 00:06:43,170
 destroy one's own for all any mental defilements. And when

89
00:06:43,170 --> 00:06:44,520
 a person is jhana

90
00:06:44,520 --> 00:06:50,760
 he experiences happiness he experiences

91
00:06:50,760 --> 00:06:57,320
 peacefulness. The peacefulness he experiences during the

92
00:06:57,320 --> 00:06:59,360
 time of jhana is

93
00:06:59,360 --> 00:07:07,140
 superior to the happiness he enjoyed when experiencing sens

94
00:07:07,140 --> 00:07:10,480
ual pleasures. So the

95
00:07:10,480 --> 00:07:22,680
 the happiness he experienced during the time of jhana is

96
00:07:22,680 --> 00:07:25,200
 much better than the

97
00:07:25,200 --> 00:07:30,080
 happiness he experienced when he enjoy sensual pleasures.

98
00:07:30,080 --> 00:07:31,920
 So this is the nature

99
00:07:31,920 --> 00:07:37,650
 of jhana and there are four jhanas in supram I mean in mand

100
00:07:37,650 --> 00:07:46,000
ing sphere. Now okay

101
00:07:46,000 --> 00:07:55,200
 and the name jhana is given to a combination of some mental

102
00:07:55,200 --> 00:07:56,600
 mental states

103
00:07:56,600 --> 00:08:03,300
 or mental factors. Now the first jhana is a name given to a

104
00:08:03,300 --> 00:08:04,960
 combination of five

105
00:08:04,960 --> 00:08:11,860
 mental states that are called jhana factors and these five

106
00:08:11,860 --> 00:08:13,440
 mental states are

107
00:08:13,440 --> 00:08:20,380
 initial application sustained application and pet or

108
00:08:20,380 --> 00:08:23,200
 rupture and sukha

109
00:08:23,200 --> 00:08:28,310
 or happiness and unification of mind or concentration. So

110
00:08:28,310 --> 00:08:30,320
 when we say first jhana

111
00:08:30,320 --> 00:08:38,770
 we mean these five as a group and the second jhana consists

112
00:08:38,770 --> 00:08:40,200
 of the last three.

113
00:08:40,200 --> 00:08:46,160
 So in the second jhana the first two are missing. So the

114
00:08:46,160 --> 00:08:46,800
 second jhana

115
00:08:46,800 --> 00:08:52,280
 is a name given to the combination of the last three jhana

116
00:08:52,280 --> 00:08:54,040
 factors that is

117
00:08:54,040 --> 00:09:02,520
 pete, rupture, sukha, happiness and concentration. The

118
00:09:02,520 --> 00:09:03,560
 third jhana is a name

119
00:09:03,560 --> 00:09:11,020
 given to the last two factors happiness and concentration

120
00:09:11,020 --> 00:09:12,800
 and the fourth jhana

121
00:09:12,800 --> 00:09:20,080
 is a name given to again two factors but this time instead

122
00:09:20,080 --> 00:09:22,200
 of happiness or happy

123
00:09:22,200 --> 00:09:27,800
 feeling it is neutral feeling. So the fourth jhana consists

124
00:09:27,800 --> 00:09:29,240
 of neutral feeling

125
00:09:29,240 --> 00:09:34,060
 and concentration. So these let's say these five factors

126
00:09:34,060 --> 00:09:35,360
 are called five

127
00:09:35,360 --> 00:09:41,160
 mental states are called jhana factors. Now these among

128
00:09:41,160 --> 00:09:43,360
 these jhana factors some

129
00:09:43,360 --> 00:09:52,520
 are also the factor of maga. The initial application is

130
00:09:52,520 --> 00:09:56,040
 right thinking or right

131
00:09:56,040 --> 00:10:00,740
 thought among the eight factors of path. You know there are

132
00:10:00,740 --> 00:10:01,760
 eight factors of path.

133
00:10:01,760 --> 00:10:06,910
 The first one is what? Right understanding. The second,

134
00:10:06,910 --> 00:10:08,920
 right thought and then right

135
00:10:08,920 --> 00:10:12,520
 speech, right action, right livelihood, right effort, right

136
00:10:12,520 --> 00:10:13,640
 mindfulness, right

137
00:10:13,640 --> 00:10:17,780
 concentration. So the first jhana factor which is vitaka,

138
00:10:17,780 --> 00:10:19,320
 which is initial

139
00:10:19,320 --> 00:10:27,000
 application is right thought among the eight factors of

140
00:10:27,000 --> 00:10:29,800
 path and then the

141
00:10:29,800 --> 00:10:36,440
 second no corresponding path factor and then the third no

142
00:10:36,440 --> 00:10:37,560
 corresponding factor

143
00:10:37,560 --> 00:10:42,210
 the fourth no corresponding factor but the fifth

144
00:10:42,210 --> 00:10:43,840
 concentration, right?

145
00:10:43,840 --> 00:10:49,380
 Unification of mind is actually right concentration. So two

146
00:10:49,380 --> 00:10:50,440
 of the jhana

147
00:10:50,440 --> 00:10:58,520
 factors can be found in the among the eight path factors.

148
00:10:58,520 --> 00:11:02,880
 Now the fourth noble

149
00:11:02,880 --> 00:11:09,740
 truth is a combination of eight factors of path. Since they

150
00:11:09,740 --> 00:11:10,600
 are called eight

151
00:11:10,600 --> 00:11:14,480
 factors of path, they are called factors of path only when

152
00:11:14,480 --> 00:11:15,960
 they arise with path

153
00:11:15,960 --> 00:11:28,080
 consciousness. Now the problem is how to connect the five

154
00:11:28,080 --> 00:11:30,760
 mundane jhanas with

155
00:11:30,760 --> 00:11:36,990
 path consciousness. If they are mundane jhanas they are not

156
00:11:36,990 --> 00:11:37,920
 path consciousness

157
00:11:37,920 --> 00:11:42,810
 and if it is path consciousness it is not mundane jhana. So

158
00:11:42,810 --> 00:11:44,800
 how to connect these

159
00:11:44,800 --> 00:11:48,920
 two? Because when Buddha explained the right concentration

160
00:11:48,920 --> 00:11:49,920
 he gave

161
00:11:49,920 --> 00:11:54,920
 these four jhanas. So the commentary said that we must

162
00:11:54,920 --> 00:11:57,520
 understand here the

163
00:11:57,520 --> 00:12:05,830
 super mundane jhanas also. Now what are the super mundane j

164
00:12:05,830 --> 00:12:09,760
hanas? Actually jhana

165
00:12:09,760 --> 00:12:19,140
 by jhana we normally mean the mundane jhanas but there can

166
00:12:19,140 --> 00:12:20,880
 be jhanas in

167
00:12:20,880 --> 00:12:28,690
 super mundane sphere also. How? At the moment of

168
00:12:28,690 --> 00:12:31,440
 enlightenment as you know path

169
00:12:31,440 --> 00:12:35,980
 consciousness arises, right? So when path consciousness

170
00:12:35,980 --> 00:12:38,400
 arises it arises together

171
00:12:38,400 --> 00:12:48,360
 with 36 mental factors and among the 36 there are initial

172
00:12:48,360 --> 00:12:50,440
 application, sustained

173
00:12:50,440 --> 00:12:59,270
 application, PD, sukha and concentration and other factors.

174
00:12:59,270 --> 00:13:01,080
 So when

175
00:13:01,080 --> 00:13:08,920
 path consciousness arises all these five jhana factors are

176
00:13:08,920 --> 00:13:11,720
 with it.

177
00:13:11,720 --> 00:13:21,000
 Since the path consciousness has five jhana factors with it

178
00:13:21,000 --> 00:13:21,280
, the path

179
00:13:21,280 --> 00:13:28,400
 consciousness is called first jhana path consciousness.

180
00:13:28,400 --> 00:13:29,720
 That means

181
00:13:29,720 --> 00:13:35,110
 path consciousness which resembles the first jhana because

182
00:13:35,110 --> 00:13:36,640
 as we know if it is

183
00:13:36,640 --> 00:13:40,870
 jhana it is not maga and if it is maga it is not jhana. But

184
00:13:40,870 --> 00:13:42,640
 here the maga

185
00:13:42,640 --> 00:13:47,360
 consciousness is called first jhana because it resembles

186
00:13:47,360 --> 00:13:49,280
 the first jhana of

187
00:13:49,280 --> 00:13:55,600
 mundane sphere. So if a person practices vipassana

188
00:13:55,600 --> 00:13:57,880
 meditation and gets

189
00:13:57,880 --> 00:14:01,840
 enlightenment, I mean if a person practices pure vipassana

190
00:14:01,840 --> 00:14:03,200
 meditation

191
00:14:03,200 --> 00:14:09,200
 vipassana only not combined with samatha. So if a person

192
00:14:09,200 --> 00:14:10,160
 practices pure

193
00:14:10,160 --> 00:14:16,180
 vipassana meditation and he gets the path consciousness,

194
00:14:16,180 --> 00:14:16,760
 that

195
00:14:16,760 --> 00:14:20,000
 path consciousness is accompanied by all these five factors

196
00:14:20,000 --> 00:14:21,280
 and so that path

197
00:14:21,280 --> 00:14:27,160
 consciousness is said to be first jhana path consciousness.

198
00:14:27,160 --> 00:14:28,560
 That is path

199
00:14:28,560 --> 00:14:35,280
 consciousness resembling first jhana. Now there are persons

200
00:14:35,280 --> 00:14:38,200
 who practice samatha

201
00:14:38,200 --> 00:14:43,650
 meditation and who get jhanas before they practice vipass

202
00:14:43,650 --> 00:14:46,200
ana meditation. And

203
00:14:46,200 --> 00:14:51,260
 when they practice vipassana meditation they may practice

204
00:14:51,260 --> 00:14:52,560
 pure vipassana or

205
00:14:52,560 --> 00:14:58,810
 they may practice samatha and vipassana joined together, I

206
00:14:58,810 --> 00:15:00,000
 mean hooked together.

207
00:15:00,000 --> 00:15:08,780
 That means they may enter into first jhana. This is mundane

208
00:15:08,780 --> 00:15:10,880
 jhana and then they

209
00:15:10,880 --> 00:15:15,840
 get out of that jhana and practice vipassana on that first

210
00:15:15,840 --> 00:15:18,640
 jhana. And as a

211
00:15:18,640 --> 00:15:23,450
 result of vipassana, let us say path consciousness arises.

212
00:15:23,450 --> 00:15:25,080
 So when path

213
00:15:25,080 --> 00:15:28,620
 consciousness arises, that path consciousness is

214
00:15:28,620 --> 00:15:32,000
 accompanied by all

215
00:15:32,000 --> 00:15:39,920
 five jhana factors with other mental states. So his path

216
00:15:39,920 --> 00:15:40,800
 consciousness is

217
00:15:40,800 --> 00:15:45,640
 called first jhana path consciousness. That means path

218
00:15:45,640 --> 00:15:46,940
 consciousness which

219
00:15:46,940 --> 00:15:55,040
 resembles the first jhana. Again that person has all four j

220
00:15:55,040 --> 00:15:56,600
hanas so he may

221
00:15:56,600 --> 00:16:01,070
 enter into second jhana and getting out of second jhana he

222
00:16:01,070 --> 00:16:01,800
 may practice

223
00:16:01,800 --> 00:16:07,890
 vipassana on second jhana and then he gets maga. So his mag

224
00:16:07,890 --> 00:16:09,080
a consciousness

225
00:16:09,080 --> 00:16:17,760
 will be accompanied by only three jhana factors because he

226
00:16:17,760 --> 00:16:19,160
 made the

227
00:16:19,160 --> 00:16:22,370
 second jhana which has only three jhana factors as a basis

228
00:16:22,370 --> 00:16:24,800
 for his vipassana. So

229
00:16:24,800 --> 00:16:29,780
 when mangajita arises, his mangajita is accompanied by

230
00:16:29,780 --> 00:16:31,960
 three jhana factors and

231
00:16:31,960 --> 00:16:40,400
 other other mental factors. So his that maga consciousness

232
00:16:40,400 --> 00:16:42,440
 is called second

233
00:16:42,440 --> 00:16:46,600
 jhana path consciousness. That means path consciousness

234
00:16:46,600 --> 00:16:49,240
 which resembles second

235
00:16:49,240 --> 00:16:53,840
 jhana. The same with the third jhana and fourth jhana. So

236
00:16:53,840 --> 00:16:56,680
 when we say first jhana

237
00:16:56,680 --> 00:17:00,760
 path consciousness we really we really mean it is path

238
00:17:00,760 --> 00:17:01,760
 consciousness and it is

239
00:17:01,760 --> 00:17:06,360
 not jhana consciousness but it is path consciousness which

240
00:17:06,360 --> 00:17:07,480
 resembles which is

241
00:17:07,480 --> 00:17:15,990
 like jhana consciousness. So they are called supramandhan

242
00:17:15,990 --> 00:17:17,500
janas.

243
00:17:17,500 --> 00:17:25,910
 Actually they don't take supramandhanjanas don't take the

244
00:17:25,910 --> 00:17:27,320
 mental image as

245
00:17:27,320 --> 00:17:32,120
 object but they take nirvana as object. So when these jhana

246
00:17:32,120 --> 00:17:35,000
 factors arise with

247
00:17:35,000 --> 00:17:40,370
 path consciousness they take nirvana as object. But when

248
00:17:40,370 --> 00:17:41,920
 they arise with jhana

249
00:17:41,920 --> 00:17:47,660
 consciousness, when they arise as mandanjanas they take the

250
00:17:47,660 --> 00:17:48,920
 mental image

251
00:17:48,920 --> 00:17:54,960
 and so on as object. So the object is different. When they

252
00:17:54,960 --> 00:17:56,080
 are mandanjanas

253
00:17:56,080 --> 00:18:00,440
 they take the mental image and others as object but when

254
00:18:00,440 --> 00:18:01,360
 they arise with path

255
00:18:01,360 --> 00:18:06,880
 consciousness they take nirvana as object. And the path

256
00:18:06,880 --> 00:18:09,560
 consciousness has

257
00:18:09,560 --> 00:18:14,580
 the ability to destroy all mental defilements. Not just

258
00:18:14,580 --> 00:18:15,400
 pushing them away

259
00:18:15,400 --> 00:18:18,920
 for some time but destroying them all together so that

260
00:18:18,920 --> 00:18:19,960
 these mental

261
00:18:19,960 --> 00:18:26,080
 defilements do not arise again. And when a person is

262
00:18:26,080 --> 00:18:29,960
 experiencing the path

263
00:18:29,960 --> 00:18:32,880
 consciousness and later on fruit conscious, fruity

264
00:18:32,880 --> 00:18:35,000
 consciousness, he is said

265
00:18:35,000 --> 00:18:40,240
 to be in bliss. So his peacefulness, his happiness, he

266
00:18:40,240 --> 00:18:41,640
 experienced at that time is

267
00:18:41,640 --> 00:18:50,400
 much superior to the experience of jhana. Not to speak of

268
00:18:50,400 --> 00:18:52,320
 superior to experience

269
00:18:52,320 --> 00:18:57,760
 of sensual pleasures. So that is the highest form of

270
00:18:57,760 --> 00:18:59,800
 happiness, highest form

271
00:18:59,800 --> 00:19:08,760
 of peacefulness. So mandanjanas and magas differ in this

272
00:19:08,760 --> 00:19:11,280
 way. Right? Mandanjanas

273
00:19:11,280 --> 00:19:17,680
 take the mental image and others as object and they can

274
00:19:17,680 --> 00:19:19,240
 push away mental

275
00:19:19,240 --> 00:19:26,440
 defilements temporarily and they can give a kind of

276
00:19:26,440 --> 00:19:29,200
 peacefulness, much superior to

277
00:19:29,200 --> 00:19:35,960
 ordinary peacefulness. And maga, we can call them

278
00:19:35,960 --> 00:19:43,810
 super mandanjanas. So super mandanjanas or maga take nir

279
00:19:43,810 --> 00:19:46,160
vana as object and they

280
00:19:46,160 --> 00:19:50,670
 are able to eradicate, destroy once and for all mental def

281
00:19:50,670 --> 00:19:52,340
ilements. And the

282
00:19:52,340 --> 00:19:59,480
 happiness experienced during the time of maga and pala is

283
00:19:59,480 --> 00:20:01,760
 the best, the highest

284
00:20:01,760 --> 00:20:06,780
 form of happiness or peacefulness. So they differ in this

285
00:20:06,780 --> 00:20:09,240
 respect. So when you

286
00:20:09,240 --> 00:20:14,140
 see the explanation given for right concentration as four j

287
00:20:14,140 --> 00:20:15,800
hanas, you must

288
00:20:15,800 --> 00:20:20,910
 understand that these four jhanas are both mundane and

289
00:20:20,910 --> 00:20:22,440
 super mundane. So when

290
00:20:22,440 --> 00:20:26,260
 you take these four jhanas to be mundane, they belong to

291
00:20:26,260 --> 00:20:28,920
 the preliminary stage. When

292
00:20:28,920 --> 00:20:33,440
 you take them to be super mundane, they belong to the stage

293
00:20:33,440 --> 00:20:36,360
 of maga. Since there

294
00:20:36,360 --> 00:20:42,490
 are four jhanas both in preliminary stage and at the stage

295
00:20:42,490 --> 00:20:44,320
 of maga, the

296
00:20:44,320 --> 00:20:50,570
 commentary said that there is a variety of jhanas both in

297
00:20:50,570 --> 00:20:53,040
 the preliminary stage

298
00:20:53,040 --> 00:20:58,890
 and at the moment of maga. Because there are any one of

299
00:20:58,890 --> 00:21:01,160
 these four in both

300
00:21:01,160 --> 00:21:08,130
 cases, in both stages. So it is not like other factors of

301
00:21:08,130 --> 00:21:12,280
 path. For example, right

302
00:21:12,280 --> 00:21:18,600
 effort. Do you remember how many right efforts are there?

303
00:21:18,600 --> 00:21:21,080
 Four, right? So there

304
00:21:21,080 --> 00:21:27,280
 are four right efforts to avoid akusala which has not yet

305
00:21:27,280 --> 00:21:28,560
 arisen from arriving

306
00:21:28,560 --> 00:21:39,520
 and then to abandon akusala which has arisen and then to,

307
00:21:39,520 --> 00:21:43,560
 how do I say, the effort

308
00:21:43,560 --> 00:21:47,960
 for arising of kusala which has not yet arisen and the

309
00:21:47,960 --> 00:21:49,400
 effort for development of

310
00:21:49,400 --> 00:21:52,330
 kusala which has already arisen, right? So there are four

311
00:21:52,330 --> 00:21:54,360
 kinds of effort. Now

312
00:21:54,360 --> 00:21:59,920
 these four kinds of effort arise during preliminary stage

313
00:21:59,920 --> 00:22:03,200
 and also at the

314
00:22:03,200 --> 00:22:09,070
 stage of maga. But during the preliminary stage, these four

315
00:22:09,070 --> 00:22:12,200
 come one at a time and

316
00:22:12,200 --> 00:22:18,160
 not in the order but there can be the first effort or

317
00:22:18,160 --> 00:22:19,080
 second effort or

318
00:22:19,080 --> 00:22:22,660
 third or fourth effort and they take different objects. The

319
00:22:22,660 --> 00:22:23,400
 first takes the

320
00:22:23,400 --> 00:22:26,870
 akusalaic object, right? The third takes the kusalaic

321
00:22:26,870 --> 00:22:30,000
 object and so on. But when the

322
00:22:30,000 --> 00:22:35,960
 right effort arises with maga, there is no variety, just

323
00:22:35,960 --> 00:22:43,280
 right effort. It arises

324
00:22:43,280 --> 00:22:50,800
 with maga taking nibbana as object and doing the function

325
00:22:50,800 --> 00:22:55,680
 of path. So in the

326
00:22:55,680 --> 00:23:01,800
 preliminary stage, there is a variety of efforts. Effort

327
00:23:01,800 --> 00:23:03,480
 number one, number two, number

328
00:23:03,480 --> 00:23:07,320
 three, number four. But at the stage of maga, there is only

329
00:23:07,320 --> 00:23:12,560
 one, just one effort in

330
00:23:12,560 --> 00:23:19,390
 general. We cannot see that the effort that arises with mag

331
00:23:19,390 --> 00:23:20,360
a is of the first

332
00:23:20,360 --> 00:23:23,650
 kind or second kind or third kind or fourth kind. But it is

333
00:23:23,650 --> 00:23:24,760
 just the effort in

334
00:23:24,760 --> 00:23:29,310
 general. So the the commentary said there is variety of

335
00:23:29,310 --> 00:23:31,840
 efforts in preliminary

336
00:23:31,840 --> 00:23:36,560
 stage but at the stage of maga, there is only one effort.

337
00:23:36,560 --> 00:23:38,960
 But here with regard to

338
00:23:38,960 --> 00:23:43,060
 concentration, it is different. So there is variety of jhan

339
00:23:43,060 --> 00:23:44,800
as both during the

340
00:23:44,800 --> 00:23:50,180
 preliminary stage and at the stage of maga. I will ask you

341
00:23:50,180 --> 00:23:57,000
 some questions. Do jhanas

342
00:23:57,000 --> 00:24:06,200
 eradicate mental defilements? Yes or no? Do jhanas

343
00:24:06,200 --> 00:24:08,120
 eradicate, destroy mental

344
00:24:08,120 --> 00:24:19,760
 defilements? Eredicate means destroy altogether. No. What

345
00:24:19,760 --> 00:24:25,120
 they do is temporary

346
00:24:25,120 --> 00:24:30,720
 abandonment or push these mental defilements away for some

347
00:24:30,720 --> 00:24:32,520
 time. But they

348
00:24:32,520 --> 00:24:37,740
 cannot eradicate or destroy altogether the mental defile

349
00:24:37,740 --> 00:24:39,200
ments and that is one

350
00:24:39,200 --> 00:24:49,880
 thing. And the other is, is it essential for a person to

351
00:24:49,880 --> 00:24:53,680
 get jhanas before he

352
00:24:53,680 --> 00:25:01,040
 takes up vipassana meditation? Must he get jhana before he

353
00:25:01,040 --> 00:25:03,040
 practices vipassana

354
00:25:03,040 --> 00:25:09,460
 meditation? No. He may or may not get jhanas. Right? Yeah.

355
00:25:09,460 --> 00:25:09,920
 These are the two

356
00:25:09,920 --> 00:25:13,640
 things I want to make clear. I want you to understand

357
00:25:13,640 --> 00:25:15,760
 clearly. Right? So jhanas

358
00:25:15,760 --> 00:25:20,760
 cannot eradicate or destroy mental defilements but they can

359
00:25:20,760 --> 00:25:22,200
 push these

360
00:25:22,200 --> 00:25:28,460
 mental defilements away from the person for some time. And

361
00:25:28,460 --> 00:25:29,600
 it is not

362
00:25:29,600 --> 00:25:34,520
 essential to get jhana before you practice vipassana

363
00:25:34,520 --> 00:25:35,960
 meditation. You may

364
00:25:35,960 --> 00:25:39,130
 get jhanas before or you may not. So these are the two

365
00:25:39,130 --> 00:25:40,400
 things I want to make

366
00:25:40,400 --> 00:25:45,220
 clear. Now the jhanas being able to push the mental defile

367
00:25:45,220 --> 00:25:46,200
ments away from some

368
00:25:46,200 --> 00:25:55,800
 time. Right? How long? A few hours, a few days, a few weeks

369
00:25:55,800 --> 00:26:04,200
, a month. No.

370
00:26:04,200 --> 00:26:13,670
 Jhanas can push away longer than that. You know how long?

371
00:26:13,670 --> 00:26:19,880
 60 years. There was a

372
00:26:19,880 --> 00:26:27,700
 monk, a very renowned monk in maybe ancient Ceylon and he

373
00:26:27,700 --> 00:26:29,400
 was a very famous

374
00:26:29,400 --> 00:26:38,640
 teacher and he got all these jhanas and the mental, I mean

375
00:26:38,640 --> 00:26:40,000
 how to call them,

376
00:26:40,000 --> 00:26:44,820
 magic powers, mental magic powers so that he thought that

377
00:26:44,820 --> 00:26:48,400
 he was an arahant and he

378
00:26:48,400 --> 00:26:51,650
 taught many students and many of his students became arah

379
00:26:51,650 --> 00:26:53,320
ants but he still

380
00:26:53,320 --> 00:26:58,060
 remained an ordinary person in puttujana. Now one of his

381
00:26:58,060 --> 00:26:59,800
 pupils who had become an

382
00:26:59,800 --> 00:27:07,290
 arahant read his mind and found out that his teacher was

383
00:27:07,290 --> 00:27:10,080
 still a puttujana. So in

384
00:27:10,080 --> 00:27:13,400
 order to help him, in order to shake him up, he went to the

385
00:27:13,400 --> 00:27:15,360
 teacher and then he

386
00:27:15,360 --> 00:27:20,080
 when he met the teacher, the teacher asked what do you come

387
00:27:20,080 --> 00:27:21,920
 here for? Then he said I

388
00:27:21,920 --> 00:27:26,740
 want to ask you some questions. Then the student asked

389
00:27:26,740 --> 00:27:28,120
 questions and the

390
00:27:28,120 --> 00:27:34,380
 teacher gave the answers right away. He doesn't have to

391
00:27:34,380 --> 00:27:35,680
 search for the

392
00:27:35,680 --> 00:27:44,760
 answers. Then the student said oh how great, how wonderful.

393
00:27:44,760 --> 00:27:46,480
 You can answer my

394
00:27:46,480 --> 00:27:52,560
 questions right away. So when did you reach this stage? He

395
00:27:52,560 --> 00:27:55,240
 asked because the

396
00:27:55,240 --> 00:27:58,840
 student knew that his teacher thought he was an arahant. So

397
00:27:58,840 --> 00:27:59,760
 when did you reach

398
00:27:59,760 --> 00:28:06,560
 this stage? Then he said oh it's about 60 years ago because

399
00:28:06,560 --> 00:28:08,040
 during these 60 years

400
00:28:08,040 --> 00:28:12,570
 he did not see any mental defilement arise in his mind. So

401
00:28:12,570 --> 00:28:13,440
 that's why he

402
00:28:13,440 --> 00:28:17,760
 thought he was an arahant. He was able to push these mental

403
00:28:17,760 --> 00:28:19,040
 defilements away for 60

404
00:28:19,040 --> 00:28:26,320
 years but since it is not eradication by manga he can come

405
00:28:26,320 --> 00:28:28,200
 back. So he asked

406
00:28:28,200 --> 00:28:35,060
 Pandey do you experience or do you make use of magic powers

407
00:28:35,060 --> 00:28:36,300
? Oh it's

408
00:28:36,300 --> 00:28:41,440
 easy for me. They said then the student said Pandey please

409
00:28:41,440 --> 00:28:42,280
 create an

410
00:28:42,280 --> 00:28:54,080
 elephant and it is putting his trunk up and making noise

411
00:28:54,080 --> 00:28:54,560
 and

412
00:28:54,560 --> 00:29:02,440
 rushing at you to kill you. So the teacher created that

413
00:29:02,440 --> 00:29:04,200
 image as the student

414
00:29:04,200 --> 00:29:13,910
 has asked and when the image he created went to him or came

415
00:29:13,910 --> 00:29:14,440
 to him to

416
00:29:14,440 --> 00:29:20,640
 crush him he was afraid and so he got up from his seat. He

417
00:29:20,640 --> 00:29:22,000
 was afraid of his own

418
00:29:22,000 --> 00:29:29,120
 creation. So the student took hold of the robe and said

419
00:29:29,120 --> 00:29:39,150
 Pandey do arahants of fear. Only then he realized that he

420
00:29:39,150 --> 00:29:40,600
 was not an arahant yet.

421
00:29:40,600 --> 00:29:50,990
 So then he said he put up his hands and said friend please

422
00:29:50,990 --> 00:29:54,000
 be my refuge. So the

423
00:29:54,000 --> 00:29:59,320
 student said I came here just for these powers and then he

424
00:29:59,320 --> 00:30:00,680
 taught him a

425
00:30:00,680 --> 00:30:03,780
 practice of meditation and he practiced meditation and he

426
00:30:03,780 --> 00:30:05,800
 became an arahant. So

427
00:30:05,800 --> 00:30:11,480
 even for 60 years they can push the mental defilements away

428
00:30:11,480 --> 00:30:13,240
 but although for

429
00:30:13,240 --> 00:30:17,800
 60 years no mental defilements had arisen in his mind when

430
00:30:17,800 --> 00:30:19,080
 there is the

431
00:30:19,080 --> 00:30:23,600
 condition with the elephant coming to crush him he was

432
00:30:23,600 --> 00:30:25,280
 afraid and so he

433
00:30:25,280 --> 00:30:34,040
 got up to run away. So jhanas cannot eradicate right cannot

434
00:30:34,040 --> 00:30:38,060
 describe mental defilements all together but they can push

435
00:30:38,060 --> 00:30:39,920
 them away not just

436
00:30:39,920 --> 00:30:46,600
 when they are in jhana state maybe for many years but since

437
00:30:46,600 --> 00:30:47,000
 they are not

438
00:30:47,000 --> 00:30:52,920
 eradicated completely they can come back as in this story.

439
00:30:52,920 --> 00:30:56,920
 But those eradicated

440
00:30:56,920 --> 00:31:05,160
 by Maga never arise again. Maybe an extreme case the monk

441
00:31:05,160 --> 00:31:06,720
 able being able to

442
00:31:06,720 --> 00:31:12,260
 push mental defilements away for 60 years although he was a

443
00:31:12,260 --> 00:31:13,680
 Bhutu jhana.

444
00:31:13,680 --> 00:31:21,900
 Okay I think I have explained fairly fully not not yet

445
00:31:21,900 --> 00:31:24,680
 fully fairly fully the

446
00:31:24,680 --> 00:31:30,580
 effect of right concentration and I think that shows that

447
00:31:30,580 --> 00:31:31,880
 what I told you

448
00:31:31,880 --> 00:31:35,610
 about the four noble truths and noble eightfold part is

449
00:31:35,610 --> 00:31:37,960
 just and the tip of an

450
00:31:37,960 --> 00:31:44,240
 iceberg. There is a lot more to know about these four noble

451
00:31:44,240 --> 00:31:44,920
 truths and

452
00:31:44,920 --> 00:31:55,940
 eight factors. Thus concluded the lecture on the four noble

453
00:31:55,940 --> 00:31:57,720
 truths part two

454
00:31:57,720 --> 00:32:03,190
 delivered by Venerable Usilananda at Tathagata Meditation

455
00:32:03,190 --> 00:32:04,840
 Center September

456
00:32:04,840 --> 00:32:09,600
 27 1996

