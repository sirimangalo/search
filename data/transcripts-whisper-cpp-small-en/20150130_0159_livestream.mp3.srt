1
00:00:00,000 --> 00:00:07,000
 Good morning.

2
00:00:07,000 --> 00:00:33,000
 I'm broadcasting here from Myeong-san again.

3
00:00:33,000 --> 00:00:44,000
 I'm going to talk about something called the Idi Pada,

4
00:00:44,000 --> 00:00:46,000
 which is...

5
00:00:46,000 --> 00:01:03,000
 Idi means power or strength or might.

6
00:01:03,000 --> 00:01:06,000
 Power is one of the best translations,

7
00:01:06,000 --> 00:01:12,000
 sometimes translated as success or dominance.

8
00:01:12,000 --> 00:01:26,000
 Pada means the path or the steps that lead to something.

9
00:01:26,000 --> 00:01:38,800
 So these are often translated as the four means of

10
00:01:38,800 --> 00:01:42,000
 succeeding,

11
00:01:42,000 --> 00:01:47,000
 of bringing something to its culmination,

12
00:01:47,000 --> 00:02:03,000
 to fruit, to bearing, to having our work bear fruit.

13
00:02:03,000 --> 00:02:09,110
 And so the most important aspect of this is how we apply

14
00:02:09,110 --> 00:02:15,000
 them to our practice,

15
00:02:15,000 --> 00:02:31,530
 how we apply these dhammas in order to succeed in our

16
00:02:31,530 --> 00:02:35,000
 practice.

17
00:02:35,000 --> 00:02:41,000
 But another useful way of looking at them

18
00:02:41,000 --> 00:02:48,000
 is in terms of how the practice helps us succeed.

19
00:02:48,000 --> 00:02:59,330
 So how meditation practice actually helps us cultivate

20
00:02:59,330 --> 00:03:03,000
 these dhammas.

21
00:03:03,000 --> 00:03:10,680
 It's helpful because sometimes we lose confidence in the

22
00:03:10,680 --> 00:03:12,000
 practice,

23
00:03:12,000 --> 00:03:19,000
 find it hard to remember why we're torturing ourselves.

24
00:03:19,000 --> 00:03:29,370
 We have a hard time sometimes building up the interest in

25
00:03:29,370 --> 00:03:32,000
 the practice.

26
00:03:32,000 --> 00:03:36,230
 So it's often good to talk about how the practice helps us

27
00:03:36,230 --> 00:03:38,000
 in our lives

28
00:03:38,000 --> 00:03:46,000
 spiritually, how it leads us to greater things.

29
00:03:46,000 --> 00:03:49,000
 So we'll look at this two ways then,

30
00:03:49,000 --> 00:03:56,000
 in terms of how the four Idi Pada help us practice,

31
00:03:56,000 --> 00:04:01,000
 in terms of how the practice brings us closer to Idi,

32
00:04:01,000 --> 00:04:07,000
 closer to power or success,

33
00:04:07,000 --> 00:04:17,340
 closer just to making sure our lives bear fruit or are

34
00:04:17,340 --> 00:04:20,000
 fruitful.

35
00:04:20,000 --> 00:04:26,000
 So the first Idi Pada is something called chanda,

36
00:04:26,000 --> 00:04:36,000
 chanda which means desire or interest.

37
00:04:36,000 --> 00:04:39,000
 And so it can be used in a bad way.

38
00:04:39,000 --> 00:04:42,000
 We have something called kama chanda,

39
00:04:42,000 --> 00:04:47,000
 which means sensual desire,

40
00:04:47,000 --> 00:04:53,000
 which is in Buddhism of course considered to be unhelpful,

41
00:04:53,000 --> 00:04:59,000
 harmful.

42
00:04:59,000 --> 00:05:01,000
 Dragging one down.

43
00:05:01,000 --> 00:05:12,000
 But then there's dhamma chanda, which is desire for truth.

44
00:05:12,000 --> 00:05:16,000
 And so desire isn't a very good translation exactly.

45
00:05:16,000 --> 00:05:31,000
 It's an interest or zeal.

46
00:05:31,000 --> 00:05:36,000
 I guess colloquially we have to use the word desire.

47
00:05:36,000 --> 00:05:43,960
 But it's important to understand it isn't like attachment

48
00:05:43,960 --> 00:05:46,000
 to meditating.

49
00:05:46,000 --> 00:05:48,000
 Something like that.

50
00:05:48,000 --> 00:05:58,000
 It's the genuine desire to better oneself.

51
00:05:58,000 --> 00:06:05,900
 And so this is the first means of succeeding in dhamma and

52
00:06:05,900 --> 00:06:07,000
 in life.

53
00:06:07,000 --> 00:06:11,940
 You need to be interested in what you're doing in order to

54
00:06:11,940 --> 00:06:13,000
 succeed.

55
00:06:13,000 --> 00:06:17,710
 I think that's difficult in insight meditation because it's

56
00:06:17,710 --> 00:06:22,000
 not very pleasant sometimes.

57
00:06:22,000 --> 00:06:25,000
 And also because there's nothing to cling to.

58
00:06:25,000 --> 00:06:29,000
 Because meditation is about letting go,

59
00:06:29,000 --> 00:06:32,000
 there's nothing during the practice that you can cling to

60
00:06:32,000 --> 00:06:32,000
 and say,

61
00:06:32,000 --> 00:06:35,000
 "Yes, that's what I want."

62
00:06:35,000 --> 00:06:38,110
 It's all about realizing that the things that you want are

63
00:06:38,110 --> 00:06:39,000
 not worth clinging to.

64
00:06:39,000 --> 00:06:45,000
 The Buddha said, "Sabdhe dhamma na langa bhini reh sahya."

65
00:06:45,000 --> 00:06:52,870
 No dhamma is worth clinging to. Nothing is worth clinging

66
00:06:52,870 --> 00:06:54,000
 to.

67
00:06:54,000 --> 00:07:00,000
 Not even meditation practice, not even goodness, nothing.

68
00:07:00,000 --> 00:07:10,000
 Clinging isn't the path to happiness.

69
00:07:10,000 --> 00:07:14,550
 And so it's quite difficult, as I said, to cultivate this

70
00:07:14,550 --> 00:07:18,000
 desire for the meditation.

71
00:07:18,000 --> 00:07:24,000
 This is why the word "desire" is kind of unhelpful here.

72
00:07:24,000 --> 00:07:27,000
 Because even wanting to meditate is usually misleading,

73
00:07:27,000 --> 00:07:28,000
 misled.

74
00:07:28,000 --> 00:07:33,210
 When someone wants to meditate, it's usually for the wrong

75
00:07:33,210 --> 00:07:34,000
 reasons.

76
00:07:34,000 --> 00:07:40,000
 Because wanting doesn't help you in your meditation.

77
00:07:40,000 --> 00:07:45,610
 But the interest, the contentment, the zest, the zeal for

78
00:07:45,610 --> 00:07:54,000
 meditation, this is important.

79
00:07:54,000 --> 00:07:58,500
 And we gain this in different ways through reminding

80
00:07:58,500 --> 00:08:04,000
 ourselves of the benefits of meditation.

81
00:08:04,000 --> 00:08:08,000
 We gain it through the meditation practice itself,

82
00:08:08,000 --> 00:08:14,140
 and seeing because meditation leads you on, the truth leads

83
00:08:14,140 --> 00:08:17,000
 you to greater truth.

84
00:08:17,000 --> 00:08:25,000
 As you see the truth, it encourages you.

85
00:08:25,000 --> 00:08:28,020
 Through thinking about the Buddha, this gives encouragement

86
00:08:28,020 --> 00:08:30,000
 about the example,

87
00:08:30,000 --> 00:08:34,880
 through surrounding yourself with people who are good

88
00:08:34,880 --> 00:08:36,000
 examples,

89
00:08:36,000 --> 00:08:41,000
 people who remind you of the path and the goal,

90
00:08:41,000 --> 00:08:45,000
 people who set an example for you.

91
00:08:45,000 --> 00:08:48,650
 Through listening to the Dhamma, through studying the Dham

92
00:08:48,650 --> 00:08:51,000
ma, all of these things,

93
00:08:51,000 --> 00:08:57,250
 through thinking about death and the inevitability of the

94
00:08:57,250 --> 00:09:04,000
 dangers and the troubles that await us,

95
00:09:04,000 --> 00:09:15,000
 especially if we're not prepared.

96
00:09:15,000 --> 00:09:18,240
 But it's important to recognize this and to recognize our

97
00:09:18,240 --> 00:09:21,000
 discontent with the practice and to work through that.

98
00:09:21,000 --> 00:09:25,040
 In fact, the discontent is probably more important

99
00:09:25,040 --> 00:09:26,000
 ultimately,

100
00:09:26,000 --> 00:09:29,000
 because ultimately there's no desire for the practice.

101
00:09:29,000 --> 00:09:34,340
 There's only a sort of sense of settling into a mindful

102
00:09:34,340 --> 00:09:35,000
 state,

103
00:09:35,000 --> 00:09:38,000
 which comes with getting rid of discontent.

104
00:09:38,000 --> 00:09:42,290
 And so we have to work on this discontent, which is in two

105
00:09:42,290 --> 00:09:43,000
 ways.

106
00:09:43,000 --> 00:09:46,240
 There's the discontent with the practice because it's

107
00:09:46,240 --> 00:09:47,000
 unpleasant,

108
00:09:47,000 --> 00:09:52,000
 because it shows us things we'd rather not look at,

109
00:09:52,000 --> 00:09:54,770
 and discontent based on desire, because we want something

110
00:09:54,770 --> 00:09:55,000
 else,

111
00:09:55,000 --> 00:10:00,000
 we want pleasures, we want entertainment.

112
00:10:00,000 --> 00:10:05,110
 So examining those and reminding ourselves, liking one

113
00:10:05,110 --> 00:10:08,000
 thing, wanting one thing,

114
00:10:08,000 --> 00:10:12,380
 and being able to see through them, to see that they're not

115
00:10:12,380 --> 00:10:13,000
 actually satisfying,

116
00:10:13,000 --> 00:10:19,350
 that they can't actually make us happy, bring us true

117
00:10:19,350 --> 00:10:22,000
 happiness.

118
00:10:22,000 --> 00:10:25,350
 To see them for what they are is something that arises and

119
00:10:25,350 --> 00:10:28,000
 sees them and let them go.

120
00:10:28,000 --> 00:10:33,050
 Then the mind settles into this sort of contentment, which

121
00:10:33,050 --> 00:10:37,000
 is a sort of chanda in itself.

122
00:10:37,000 --> 00:10:40,000
 At any rate, we have to be careful to overcome discontent.

123
00:10:40,000 --> 00:10:45,930
 But the great thing about meditation is that it brings us

124
00:10:45,930 --> 00:10:47,000
 chanda,

125
00:10:47,000 --> 00:10:51,000
 it brings us content, the right kind of desire.

126
00:10:51,000 --> 00:10:57,000
 It refines our desires.

127
00:10:57,000 --> 00:11:03,000
 So if you look at the nature of a human being,

128
00:11:03,000 --> 00:11:06,560
 when they're children, as children we have very coarse

129
00:11:06,560 --> 00:11:07,000
 desires,

130
00:11:07,000 --> 00:11:12,000
 we find ourselves pleased by simple things,

131
00:11:12,000 --> 00:11:18,000
 and bright colors and loud noises and so on.

132
00:11:18,000 --> 00:11:21,030
 And that changes over time, and why it changes is because

133
00:11:21,030 --> 00:11:25,000
 our sense of pleasure and happiness,

134
00:11:25,000 --> 00:11:27,000
 our desires become more refined.

135
00:11:27,000 --> 00:11:30,000
 It becomes refined through experience.

136
00:11:30,000 --> 00:11:36,710
 We come to realize that the happiness that we find as a

137
00:11:36,710 --> 00:11:40,000
 child from bright lights and colors and sounds

138
00:11:40,000 --> 00:11:43,000
 is actually quite coarse and disturbing.

139
00:11:43,000 --> 00:11:48,000
 It's tiring, laughing all the time, giggling, screaming.

140
00:11:48,000 --> 00:11:51,000
 All of these things are tiresome.

141
00:11:51,000 --> 00:11:54,000
 So we lose interest in them quite quickly.

142
00:11:54,000 --> 00:11:59,000
 Part of the process of growing up is refining one's desires

143
00:11:59,000 --> 00:11:59,000
.

144
00:11:59,000 --> 00:12:03,570
 And then you have teenagers who get interested in all sorts

145
00:12:03,570 --> 00:12:04,000
 of drugs

146
00:12:04,000 --> 00:12:09,000
 and entertainment, loud music and so on, and that changes.

147
00:12:09,000 --> 00:12:13,680
 And children look at adults and think of them as kind of

148
00:12:13,680 --> 00:12:15,000
 boring and dull,

149
00:12:15,000 --> 00:12:18,160
 but a lot of that is because they've seen through it and

150
00:12:18,160 --> 00:12:22,000
 they've realized how stressful it actually is

151
00:12:22,000 --> 00:12:25,000
 to seek out entertainment all the time.

152
00:12:25,000 --> 00:12:27,000
 So they refine their desires.

153
00:12:27,000 --> 00:12:29,000
 This goes on through life. It's not the only process.

154
00:12:29,000 --> 00:12:33,000
 There's a lot of repression that goes on as well.

155
00:12:33,000 --> 00:12:36,000
 Part of it is the refinement of desires.

156
00:12:36,000 --> 00:12:40,360
 So in a way, meditation is just an extension of that, or an

157
00:12:40,360 --> 00:12:42,000
 acceleration of that process.

158
00:12:42,000 --> 00:12:47,730
 Because in meditation you clearly come to refine your

159
00:12:47,730 --> 00:12:50,000
 ambitions and your desires

160
00:12:50,000 --> 00:12:54,030
 to the point where you cut out so many things that ordinary

161
00:12:54,030 --> 00:12:57,000
 people think would bring happiness.

162
00:12:57,000 --> 00:13:00,520
 You start to realize that that is only a cause for more

163
00:13:00,520 --> 00:13:02,000
 stress and suffering.

164
00:13:02,000 --> 00:13:10,510
 So you give it up in favor of more peaceful and more truly

165
00:13:10,510 --> 00:13:13,000
 happy pursuits.

166
00:13:13,000 --> 00:13:17,000
 Live a more simple life. Start giving away your possessions

167
00:13:17,000 --> 00:13:20,000
, not out of repression,

168
00:13:20,000 --> 00:13:25,130
 but because you realize how stressful it is to hold on to

169
00:13:25,130 --> 00:13:26,000
 them.

170
00:13:26,000 --> 00:13:31,000
 So this is how happiness brings about chanda.

171
00:13:31,000 --> 00:13:36,000
 It purifies your desires.

172
00:13:36,000 --> 00:13:39,000
 The second one is ririya.

173
00:13:39,000 --> 00:13:43,800
 I talked about this yesterday. I mentioned how important r

174
00:13:43,800 --> 00:13:45,000
iriya is.

175
00:13:45,000 --> 00:13:48,000
 In order to succeed in anything, you have to work at it.

176
00:13:48,000 --> 00:13:52,000
 Meditation is no exception.

177
00:13:52,000 --> 00:13:56,000
 Meditation isn't just about relaxing.

178
00:13:56,000 --> 00:13:59,000
 There's a lot of tweaking that has to go on.

179
00:13:59,000 --> 00:14:02,150
 Finding your practice doesn't mean just letting it go

180
00:14:02,150 --> 00:14:03,000
 naturally.

181
00:14:03,000 --> 00:14:08,000
 This is a mistake that so many people make in meditation.

182
00:14:08,000 --> 00:14:12,000
 Thinking that if you just relax it will all work itself out

183
00:14:12,000 --> 00:14:12,000
,

184
00:14:12,000 --> 00:14:17,180
 but it obviously doesn't, otherwise we'd all be enlightened

185
00:14:17,180 --> 00:14:18,000
 by now.

186
00:14:18,000 --> 00:14:22,960
 Because our natural way of processing things is to file

187
00:14:22,960 --> 00:14:25,000
 this unwholesome.

188
00:14:25,000 --> 00:14:31,170
 It's based on clinging, it's based on identification, and

189
00:14:31,170 --> 00:14:34,000
 so on.

190
00:14:34,000 --> 00:14:42,000
 So we have to work just to get to a state of normal,

191
00:14:42,000 --> 00:14:46,000
 just to get to a balanced state.

192
00:14:46,000 --> 00:14:48,000
 We have to do a lot of tweaking.

193
00:14:48,000 --> 00:14:52,000
 It's like the Buddha said, it's like a tree leaning in one

194
00:14:52,000 --> 00:14:53,000
 direction.

195
00:14:53,000 --> 00:14:56,060
 If you just let it fall, it's going to fall in that

196
00:14:56,060 --> 00:14:57,000
 direction.

197
00:14:57,000 --> 00:14:59,420
 The problem is our minds are inclined in the wrong

198
00:14:59,420 --> 00:15:00,000
 direction,

199
00:15:00,000 --> 00:15:02,000
 so you can't just let them fall.

200
00:15:02,000 --> 00:15:09,620
 You have to work hard to pull our minds in another

201
00:15:09,620 --> 00:15:12,000
 direction.

202
00:15:12,000 --> 00:15:15,000
 There's a lot of work that has to be done to do that.

203
00:15:15,000 --> 00:15:17,000
 It doesn't have to be stressful exactly,

204
00:15:17,000 --> 00:15:23,200
 but there should be a sense of work until our minds are

205
00:15:23,200 --> 00:15:25,000
 inclined in the right direction,

206
00:15:25,000 --> 00:15:33,000
 and then it's much more pleasant and peaceful.

207
00:15:33,000 --> 00:15:36,260
 Once the mind gets on the right path, then it's just smooth

208
00:15:36,260 --> 00:15:37,000
 sailing.

209
00:15:37,000 --> 00:15:47,370
 But until that time, you have to be clear in terms of real

210
00:15:47,370 --> 00:15:51,000
igning the mind.

211
00:15:51,000 --> 00:15:54,000
 But meditation also brings effort.

212
00:15:54,000 --> 00:15:56,440
 So the good that meditation does for you, it helps us

213
00:15:56,440 --> 00:15:57,000
 succeed in life

214
00:15:57,000 --> 00:16:01,000
 because we have less dragging us down.

215
00:16:01,000 --> 00:16:05,730
 All of these coarse desires are eliminated, so we have much

216
00:16:05,730 --> 00:16:07,000
 more energy.

217
00:16:07,000 --> 00:16:10,480
 They talk about meditators being zombies, but it's so far

218
00:16:10,480 --> 00:16:12,000
 from the truth.

219
00:16:12,000 --> 00:16:17,000
 The zombies are the ones that seek out pleasure

220
00:16:17,000 --> 00:16:24,000
 and spend so much of their energy on mindless pursuits.

221
00:16:24,000 --> 00:16:34,000
 They end up being very dull and uninspired.

222
00:16:34,000 --> 00:16:37,430
 If there's someone who has clear meditation, they find they

223
00:16:37,430 --> 00:16:40,000
 have great energy.

224
00:16:40,000 --> 00:16:44,000
 They're able to do whatever they'd like to accomplish,

225
00:16:44,000 --> 00:16:47,450
 and they're able to accomplish great things because of

226
00:16:47,450 --> 00:16:50,000
 their energy and their effort,

227
00:16:50,000 --> 00:16:56,000
 that they are preserving through mindfulness.

228
00:16:56,000 --> 00:17:01,000
 Just being mindful itself preserves so much energy

229
00:17:01,000 --> 00:17:04,110
 because all the stress from liking and disliking and

230
00:17:04,110 --> 00:17:05,000
 getting caught up in things,

231
00:17:05,000 --> 00:17:07,000
 you can see that when you meditate.

232
00:17:07,000 --> 00:17:15,000
 It wears you out and drags you down.

233
00:17:15,000 --> 00:17:17,000
 This is really it.

234
00:17:17,000 --> 00:17:23,000
 The third one, "titta." "Titta" means to focus your mind,

235
00:17:23,000 --> 00:17:31,000
 keeping the task in mind, keeping your mind on the object,

236
00:17:31,000 --> 00:17:35,000
 on the task.

237
00:17:35,000 --> 00:17:39,220
 So this is the, in meditation, this is the aspect of

238
00:17:39,220 --> 00:17:43,000
 keeping our focus on mindfulness,

239
00:17:43,000 --> 00:17:47,000
 and finding ourselves again and again.

240
00:17:47,000 --> 00:17:51,000
 The ability to be mindful, to not forget yourself

241
00:17:51,000 --> 00:17:57,000
 and realize that you've been eating or talking or working

242
00:17:57,000 --> 00:17:59,000
 or walking or whatever,

243
00:17:59,000 --> 00:18:03,000
 even just sitting and not being mindful.

244
00:18:03,000 --> 00:18:07,120
 The ability to catch yourself and actually be mindful of

245
00:18:07,120 --> 00:18:12,000
 the experiences as they occur.

246
00:18:12,000 --> 00:18:16,000
 This is something that grows over time.

247
00:18:16,000 --> 00:18:18,000
 This is why we come back and do courses.

248
00:18:18,000 --> 00:18:20,000
 This is why we try to do a daily practice.

249
00:18:20,000 --> 00:18:24,000
 This is why we insist that meditators try to be mindful

250
00:18:24,000 --> 00:18:26,000
 even when they're not practicing.

251
00:18:26,000 --> 00:18:30,000
 It has to become habitual, and eventually it does.

252
00:18:30,000 --> 00:18:36,000
 Eventually it seeps into your mind and your brain.

253
00:18:36,000 --> 00:18:40,000
 It becomes part of your life so that some people even get

254
00:18:40,000 --> 00:18:42,000
 so that they dream mindfully.

255
00:18:42,000 --> 00:18:46,610
 Not exactly mindfully, but they're repeating things in

256
00:18:46,610 --> 00:18:48,000
 their dreams.

257
00:18:48,000 --> 00:18:55,000
 Not mindfully, but it gets so drilled into them.

258
00:18:55,000 --> 00:19:01,300
 It's only really in the beginning when it's still only a

259
00:19:01,300 --> 00:19:04,000
 preliminary state.

260
00:19:04,000 --> 00:19:10,770
 But the ability to recognize when you get angry, to catch

261
00:19:10,770 --> 00:19:11,000
 it,

262
00:19:11,000 --> 00:19:15,520
 to realize the correct response is to say angry and to

263
00:19:15,520 --> 00:19:17,000
 remind yourself.

264
00:19:17,000 --> 00:19:22,680
 When you want something to catch yourself and not run after

265
00:19:22,680 --> 00:19:24,000
 it.

266
00:19:24,000 --> 00:19:26,710
 Obviously to succeed in anything you need to keep it in

267
00:19:26,710 --> 00:19:27,000
 mind,

268
00:19:27,000 --> 00:19:35,000
 and meditation is equally true.

269
00:19:35,000 --> 00:19:38,000
 So try, even when you're not meditating, try to be mindful.

270
00:19:38,000 --> 00:19:42,130
 In between walking and sitting, try to catch yourself as

271
00:19:42,130 --> 00:19:43,000
 you're walking,

272
00:19:43,000 --> 00:19:48,000
 as you sit down and so on.

273
00:19:48,000 --> 00:19:53,420
 As soon as you finish meditation, try to set yourself for

274
00:19:53,420 --> 00:19:56,000
 the rest of the day.

275
00:19:56,000 --> 00:20:07,010
 But meditation has, on the flip side, meditation actually

276
00:20:07,010 --> 00:20:10,000
 helps our focus, of course.

277
00:20:10,000 --> 00:20:12,810
 We're able to keep our minds on things, we get less

278
00:20:12,810 --> 00:20:14,000
 distracted.

279
00:20:14,000 --> 00:20:18,130
 The great benefits of meditation is the ability to think

280
00:20:18,130 --> 00:20:19,000
 clearer,

281
00:20:19,000 --> 00:20:25,000
 the ability to solve problems better, to focus on work,

282
00:20:25,000 --> 00:20:30,000
 and more sincerely, more with greater dedication,

283
00:20:30,000 --> 00:20:34,000
 to complete tasks with greater efficiency.

284
00:20:34,000 --> 00:20:41,030
 Because our minds are clear, there's less garbage piling up

285
00:20:41,030 --> 00:20:43,000
, getting in the way.

286
00:20:43,000 --> 00:20:46,000
 And we're able to clear it out.

287
00:20:46,000 --> 00:20:49,150
 As it comes up and file things better, our minds become

288
00:20:49,150 --> 00:20:52,000
 more sorted and more organized.

289
00:20:52,000 --> 00:21:02,000
 Our memories improve and so on.

290
00:21:02,000 --> 00:21:03,000
 This is jitta.

291
00:21:03,000 --> 00:21:07,420
 Jitta is something that improves with meditation, something

292
00:21:07,420 --> 00:21:11,000
 that improves our meditation.

293
00:21:11,000 --> 00:21:14,000
 The number four is vimansa.

294
00:21:14,000 --> 00:21:21,120
 Vimansa is a compliment to jitta. Jitta means to keep

295
00:21:21,120 --> 00:21:22,000
 things in mind.

296
00:21:22,000 --> 00:21:25,000
 But vimansa means to adjust.

297
00:21:25,000 --> 00:21:29,000
 Vimansa means discernment.

298
00:21:29,000 --> 00:21:34,050
 So it's not enough in any work, in most works, to just plow

299
00:21:34,050 --> 00:21:36,000
 ahead on thinking.

300
00:21:36,000 --> 00:21:40,000
 This is often a mistake people make in many realms.

301
00:21:40,000 --> 00:21:44,000
 They just work without discernment.

302
00:21:44,000 --> 00:21:48,470
 But even in menial tasks, even in physical work, physical

303
00:21:48,470 --> 00:21:50,000
 labor,

304
00:21:50,000 --> 00:21:53,590
 you can see the difference between someone who is disc

305
00:21:53,590 --> 00:21:55,000
erning and someone who is not.

306
00:21:55,000 --> 00:22:00,000
 The discerning person will be able to adjust their efforts,

307
00:22:00,000 --> 00:22:08,340
 will be able to retrain even their bodies to refine their

308
00:22:08,340 --> 00:22:10,000
 effort.

309
00:22:10,000 --> 00:22:14,500
 So vimansa is like being able to step back and observe your

310
00:22:14,500 --> 00:22:17,000
 work, your practice,

311
00:22:17,000 --> 00:22:22,000
 and adjust its wisdom.

312
00:22:22,000 --> 00:22:26,000
 Not just plowing ahead in meditation, this is clearly, not

313
00:22:26,000 --> 00:22:26,000
 clearly,

314
00:22:26,000 --> 00:22:29,000
 this is another mistake that people make.

315
00:22:29,000 --> 00:22:33,480
 They plow ahead, trying to do as many hours a day as they

316
00:22:33,480 --> 00:22:34,000
 can.

317
00:22:34,000 --> 00:22:36,340
 It's great if you can do lots and lots of hours of

318
00:22:36,340 --> 00:22:37,000
 meditation,

319
00:22:37,000 --> 00:22:40,580
 but it's quite possible to do lots and lots of hours of not

320
00:22:40,580 --> 00:22:46,000
 meditating, of fake meditating.

321
00:22:46,000 --> 00:22:49,000
 Just walking and sitting without benefit.

322
00:22:49,000 --> 00:22:51,000
 And so you have to be able to adjust.

323
00:22:51,000 --> 00:22:54,770
 So as I said yesterday, the ability to go to the next level

324
00:22:54,770 --> 00:22:57,000
, always the next level,

325
00:22:57,000 --> 00:23:01,770
 not to be content or not to be stuck on one level of

326
00:23:01,770 --> 00:23:03,000
 ability.

327
00:23:03,000 --> 00:23:08,570
 So you cultivate a certain level of ability and then you

328
00:23:08,570 --> 00:23:13,000
 think of that as sufficient.

329
00:23:13,000 --> 00:23:17,080
 But the mind will constantly be surprising you, and you

330
00:23:17,080 --> 00:23:20,000
 constantly have to adapt.

331
00:23:20,000 --> 00:23:24,390
 Because it's really, these are the nots, our nots, they're

332
00:23:24,390 --> 00:23:27,000
 what we've always been stuck on.

333
00:23:27,000 --> 00:23:31,980
 They're the definition of problems, they are the sticking

334
00:23:31,980 --> 00:23:34,000
 points in our mind.

335
00:23:34,000 --> 00:23:42,000
 They are what confuses us, what challenges us.

336
00:23:42,000 --> 00:23:44,000
 That's what we're focusing on.

337
00:23:44,000 --> 00:23:47,470
 So the very definition of these things, the very nature of

338
00:23:47,470 --> 00:23:50,000
 these things is going to challenge us.

339
00:23:50,000 --> 00:23:55,000
 That's what they are, they're the challenges in our mind.

340
00:23:55,000 --> 00:24:01,000
 We can't just plow through, you have to be discerning.

341
00:24:01,000 --> 00:24:04,000
 In practice this just means truly being mindful.

342
00:24:04,000 --> 00:24:07,810
 Truly being, it's an aspect of mindfulness to clearly

343
00:24:07,810 --> 00:24:10,000
 understand the experience.

344
00:24:10,000 --> 00:24:17,000
 Sampa Janya, to clearly see the experience.

345
00:24:17,000 --> 00:24:21,560
 Don't get complacent and think you already know the

346
00:24:21,560 --> 00:24:23,000
 experience.

347
00:24:23,000 --> 00:24:26,800
 Every experience would be something, there's some aspect to

348
00:24:26,800 --> 00:24:29,000
 it that is challenging you.

349
00:24:29,000 --> 00:24:34,000
 That's why it still comes up.

350
00:24:34,000 --> 00:24:38,700
 So always look at every experience with a fresh mind,

351
00:24:38,700 --> 00:24:41,000
 trying to understand it,

352
00:24:41,000 --> 00:24:43,840
 trying to see it clearly as though it was something you'd

353
00:24:43,840 --> 00:24:45,000
 never seen before.

354
00:24:45,000 --> 00:24:47,540
 And you'll see things that you've never really noticed

355
00:24:47,540 --> 00:24:53,000
 before, this is how it works.

356
00:24:53,000 --> 00:24:56,000
 So discernment is an important part of meditation.

357
00:24:56,000 --> 00:25:03,060
 Meditation also leads to discernment in our lives, in our

358
00:25:03,060 --> 00:25:05,000
 spiritual path,

359
00:25:05,000 --> 00:25:10,000
 in terms of spiritual well-being.

360
00:25:10,000 --> 00:25:14,000
 Discernment is our ability to solve our problems.

361
00:25:14,000 --> 00:25:19,000
 It's amazing how quick the mind is able to fix things.

362
00:25:19,000 --> 00:25:22,530
 When your mind is clear you're able to solve problems that

363
00:25:22,530 --> 00:25:24,000
 other people

364
00:25:24,000 --> 00:25:29,000
 become totally crippled by.

365
00:25:29,000 --> 00:25:33,000
 You're able to see through all of the delusion and illusion

366
00:25:33,000 --> 00:25:33,000
,

367
00:25:33,000 --> 00:25:36,000
 to see the truth of the situation,

368
00:25:36,000 --> 00:25:47,000
 and to realize the nature and the solution to the problem.

369
00:25:47,000 --> 00:25:52,000
 You're able to put many pieces together in the mind.

370
00:25:52,000 --> 00:25:55,340
 In terms of worldly success it's very easy to succeed in

371
00:25:55,340 --> 00:25:56,000
 business,

372
00:25:56,000 --> 00:26:00,000
 and it's easy to build things, to create things.

373
00:26:00,000 --> 00:26:05,000
 The mind works much clearer and much more efficiently.

374
00:26:05,000 --> 00:26:09,610
 But it's also much more clever in terms of seeing the big

375
00:26:09,610 --> 00:26:10,000
 picture

376
00:26:10,000 --> 00:26:14,000
 and being able to work towards goals and so on.

377
00:26:14,000 --> 00:26:19,520
 Life becomes a lot easier to live through the meditation

378
00:26:19,520 --> 00:26:20,000
 practice,

379
00:26:20,000 --> 00:26:26,220
 and the mind becomes stronger and quicker, sharper, more

380
00:26:26,220 --> 00:26:29,000
 clever.

381
00:26:29,000 --> 00:26:33,780
 On one side this is an admonishment to cultivate these four

382
00:26:33,780 --> 00:26:36,000
 things for our practice,

383
00:26:36,000 --> 00:26:39,120
 but on the other side it's an encouragement that the

384
00:26:39,120 --> 00:26:42,000
 practice is cultivating these things.

385
00:26:42,000 --> 00:26:46,500
 Practice actually has a benefit, has great benefit in our

386
00:26:46,500 --> 00:26:47,000
 lives

387
00:26:47,000 --> 00:26:51,000
 for those who are struggling with this.

388
00:26:51,000 --> 00:26:55,000
 So that's the Dhamma for today. Thank you for tuning in.

389
00:26:55,000 --> 00:26:57,000
 Keep practicing.

