1
00:00:00,000 --> 00:00:08,120
 Good evening, everyone.

2
00:00:08,120 --> 00:00:22,640
 We're broadcasting live sometime in April.

3
00:00:22,640 --> 00:00:34,250
 Today's quote is an important quote, so it doesn't say a

4
00:00:34,250 --> 00:00:35,840
 lot.

5
00:00:35,840 --> 00:01:03,400
 If you take the night when the pratagata became enlightened

6
00:01:03,400 --> 00:01:13,250
 to unexcelled self-enlightenment, yanchat and the rattingan

7
00:01:13,250 --> 00:01:16,000
upadi seya nibbhaanadhat,

8
00:01:16,000 --> 00:01:20,240
 we are very nibbhaayati.

9
00:01:20,240 --> 00:01:30,920
 And the night of his becoming fully unbound through the

10
00:01:30,920 --> 00:01:33,080
 element of unbinding

11
00:01:33,080 --> 00:01:39,000
 that is without remainder.

12
00:01:39,000 --> 00:01:48,560
 So the night when he passed away.

13
00:01:48,560 --> 00:01:55,140
 So the bodhisattva, after making a determination at the

14
00:01:55,140 --> 00:01:57,800
 foot of the Buddha Deepankara,

15
00:01:57,800 --> 00:02:08,640
 so many countless number of eons and world periods ago, we

16
00:02:08,640 --> 00:02:10,000
 made this determination and

17
00:02:10,000 --> 00:02:15,120
 then entered on the path to become a Buddha.

18
00:02:15,120 --> 00:02:23,020
 And we spent four asangaya, four uncountable periods of

19
00:02:23,020 --> 00:02:24,240
 time.

20
00:02:24,240 --> 00:02:28,840
 Not just one uncountable period of time, but four of them.

21
00:02:28,840 --> 00:02:33,710
 And periods of time that have some meaning but really can't

22
00:02:33,710 --> 00:02:34,600
 measure them.

23
00:02:34,600 --> 00:02:38,520
 They're just too long.

24
00:02:38,520 --> 00:02:45,660
 And then a hundred thousand mahakapa, mahakalpa, the great

25
00:02:45,660 --> 00:02:49,560
 eons on top of that.

26
00:02:49,560 --> 00:02:53,240
 So this is being born and die again and again.

27
00:02:53,240 --> 00:02:56,380
 So we have all these stories, only a very small number of

28
00:02:56,380 --> 00:02:59,320
 stories, but a number of useful

29
00:02:59,320 --> 00:03:05,300
 stories about the time that the Buddha spent from birth to

30
00:03:05,300 --> 00:03:08,640
 birth, perfecting his qualities

31
00:03:08,640 --> 00:03:14,400
 of mind needed to become a Buddha.

32
00:03:14,400 --> 00:03:21,680
 And then he was born in his last life as Siddhata Gautama.

33
00:03:21,680 --> 00:03:28,090
 And at the age of 29, he left home having seen that in life

34
00:03:28,090 --> 00:03:33,040
 there are certain inevitabilities.

35
00:03:33,040 --> 00:03:38,760
 We have to get old, we have to get sick, we have to die.

36
00:03:38,760 --> 00:03:41,730
 And no matter what we do, no matter how we succeed in life,

37
00:03:41,730 --> 00:03:43,320
 no matter whether we're good

38
00:03:43,320 --> 00:03:50,360
 or bad, right or wrong, rich or poor, famous or infamous,

39
00:03:50,360 --> 00:03:51,640
 in the end, these things are

40
00:03:51,640 --> 00:03:56,470
 things come to us all and they wash away everything we've

41
00:03:56,470 --> 00:03:57,280
 done.

42
00:03:57,280 --> 00:04:04,880
 And so he left home to try and find a way out of this mess.

43
00:04:04,880 --> 00:04:07,420
 And for six years he tortured himself and tried all

44
00:04:07,420 --> 00:04:09,280
 different meditations and ways of

45
00:04:09,280 --> 00:04:10,280
 practicing.

46
00:04:10,280 --> 00:04:15,170
 But he found that none of them led to freedom, they only

47
00:04:15,170 --> 00:04:17,880
 led him to places he'd been before,

48
00:04:17,880 --> 00:04:21,120
 from birth to birth.

49
00:04:21,120 --> 00:04:28,100
 So finally he cleared his mind enough to be able to

50
00:04:28,100 --> 00:04:30,680
 understand that he had to find the

51
00:04:30,680 --> 00:04:36,840
 middle way, he had to find an ordinary way, a natural way,

52
00:04:36,840 --> 00:04:40,040
 where he was accepting of pain

53
00:04:40,040 --> 00:04:45,240
 but also accepting of pleasure.

54
00:04:45,240 --> 00:04:48,920
 And where he was able to see things objectively.

55
00:04:48,920 --> 00:04:55,020
 He looked at things for what they are, not with any goal or

56
00:04:55,020 --> 00:04:58,040
 ulterior motive, not with

57
00:04:58,040 --> 00:05:01,000
 any reaction.

58
00:05:01,000 --> 00:05:05,230
 And so he began to see, his mind began to clear and he saw

59
00:05:05,230 --> 00:05:07,160
 his past lives and he saw

60
00:05:07,160 --> 00:05:12,800
 the arising and ceasing of beings being born here and born

61
00:05:12,800 --> 00:05:13,840
 there.

62
00:05:13,840 --> 00:05:18,650
 And he was able to make sense of everything and he saw how

63
00:05:18,650 --> 00:05:21,040
 our birth and our life and

64
00:05:21,040 --> 00:05:35,690
 our death is just part of a much greater system of, a cycle

65
00:05:35,690 --> 00:05:39,440
 of rebirth.

66
00:05:39,440 --> 00:05:47,280
 And then he began to look at what causes the cycle of

67
00:05:47,280 --> 00:05:53,200
 rebirth and he saw his own mind giving

68
00:05:53,200 --> 00:05:59,070
 rise to attachment, giving rise to desire, giving rise to

69
00:05:59,070 --> 00:06:01,200
 further rebirth.

70
00:06:01,200 --> 00:06:03,920
 And as he saw the truth he came to let it go.

71
00:06:03,920 --> 00:06:09,040
 And when he let it go he was freed for himself.

72
00:06:09,040 --> 00:06:10,040
 And became Buddha.

73
00:06:10,040 --> 00:06:12,160
 So that's the night of his enlightenment.

74
00:06:12,160 --> 00:06:16,760
 Now a lot of stories of the Buddha stop there and they're

75
00:06:16,760 --> 00:06:19,440
 only interested in that time,

76
00:06:19,440 --> 00:06:20,440
 that day.

77
00:06:20,440 --> 00:06:23,760
 But that's the day, the first day he's talking about.

78
00:06:23,760 --> 00:06:26,480
 That's an important day.

79
00:06:26,480 --> 00:06:29,060
 It's funny how a lot of stories of the Buddha stop there

80
00:06:29,060 --> 00:06:31,320
 and say, "Well that was most important."

81
00:06:31,320 --> 00:06:34,280
 End of story.

82
00:06:34,280 --> 00:06:39,440
 It's actually not and that part of the story is actually

83
00:06:39,440 --> 00:06:42,120
 the less important part.

84
00:06:42,120 --> 00:06:44,160
 If that was it then you'd think, "Wow okay."

85
00:06:44,160 --> 00:06:46,320
 So he became Buddha.

86
00:06:46,320 --> 00:06:52,080
 Big deal, right?

87
00:06:52,080 --> 00:06:58,240
 The other day is 45 years later.

88
00:06:58,240 --> 00:07:02,920
 He lay down between the two Sala trees.

89
00:07:02,920 --> 00:07:07,990
 Sala trees are a flowering tree in northern India, actually

90
00:07:07,990 --> 00:07:10,040
 all throughout India.

91
00:07:10,040 --> 00:07:11,640
 They're even in Sri Lanka.

92
00:07:11,640 --> 00:07:15,180
 There's even one in, at least one that I know of in

93
00:07:15,180 --> 00:07:21,080
 Thailand but it's in a colder area.

94
00:07:21,080 --> 00:07:27,000
 And he lay down and entered into final jibana.

95
00:07:27,000 --> 00:07:28,800
 The body passed away.

96
00:07:28,800 --> 00:07:32,660
 And with the passing of the body there was no more arising

97
00:07:32,660 --> 00:07:34,480
 of mental constructs.

98
00:07:34,480 --> 00:07:40,400
 I mean no more rebirth.

99
00:07:40,400 --> 00:07:45,320
 Which is also a very important day.

100
00:07:45,320 --> 00:07:51,170
 And those two together really describe what it is that we

101
00:07:51,170 --> 00:07:54,440
're, I mean not even aiming for

102
00:07:54,440 --> 00:07:58,440
 but it's the ultimate, you know.

103
00:07:58,440 --> 00:08:01,120
 Because Buddhism is all about letting go.

104
00:08:01,120 --> 00:08:07,000
 It's about freeing yourself from suffering.

105
00:08:07,000 --> 00:08:09,970
 And it sometimes scares people I think when they think of

106
00:08:09,970 --> 00:08:11,720
 the Buddha's enlightenment,

107
00:08:11,720 --> 00:08:14,080
 the Buddha's final freedom.

108
00:08:14,080 --> 00:08:21,700
 They think, "Wow," you know, like never coming back, right?

109
00:08:21,700 --> 00:08:24,760
 So it's not really our goal because most people aren't

110
00:08:24,760 --> 00:08:26,080
 interested in that.

111
00:08:26,080 --> 00:08:27,440
 It's hard for us to think of.

112
00:08:27,440 --> 00:08:29,000
 Of course, we don't want that.

113
00:08:29,000 --> 00:08:33,600
 We want to be born again for the most part.

114
00:08:33,600 --> 00:08:35,900
 Even though we may say, "Oh man, I don't want to be born

115
00:08:35,900 --> 00:08:36,320
 again.

116
00:08:36,320 --> 00:08:37,320
 I have to go through this."

117
00:08:37,320 --> 00:08:41,960
 But still we have desire and so still we'll be reborn again

118
00:08:41,960 --> 00:08:42,160
.

119
00:08:42,160 --> 00:08:46,180
 Until we attain freedom from all that and we're not born

120
00:08:46,180 --> 00:08:47,000
 again.

121
00:08:47,000 --> 00:08:49,960
 But the point is that's the end.

122
00:08:49,960 --> 00:08:53,640
 That's the final, final letting go, the final freedom.

123
00:08:53,640 --> 00:08:55,440
 Until that point you're still not free.

124
00:08:55,440 --> 00:08:58,480
 That's the point.

125
00:08:58,480 --> 00:09:01,080
 And so it's not that we have to wish for it but that's

126
00:09:01,080 --> 00:09:02,400
 where you're going.

127
00:09:02,400 --> 00:09:04,810
 Eventually you let go of more and so wherever you're born

128
00:09:04,810 --> 00:09:06,080
 you're born in a good way.

129
00:09:06,080 --> 00:09:10,280
 You let go of even more and you're born in an even better

130
00:09:10,280 --> 00:09:10,920
 way.

131
00:09:10,920 --> 00:09:13,400
 Until you let go of everything and you're born without it,

132
00:09:13,400 --> 00:09:14,640
 you know, then you're not

133
00:09:14,640 --> 00:09:15,640
 born.

134
00:09:15,640 --> 00:09:18,000
 There's no suffering.

135
00:09:18,000 --> 00:09:21,940
 No having to come back.

136
00:09:21,940 --> 00:09:24,970
 So those two are two important days but this quote is

137
00:09:24,970 --> 00:09:26,820
 important because it talks about

138
00:09:26,820 --> 00:09:28,300
 something entirely different.

139
00:09:28,300 --> 00:09:31,570
 It talks about those two as the boundary and it places

140
00:09:31,570 --> 00:09:33,920
 emphasis on what happened in between

141
00:09:33,920 --> 00:09:34,920
 those two.

142
00:09:34,920 --> 00:09:40,920
 Which, as I say, it's a shame that most stories about the

143
00:09:40,920 --> 00:09:43,620
 Buddha miss entirely.

144
00:09:43,620 --> 00:09:47,980
 Part of the reason why they do, of course, is because that

145
00:09:47,980 --> 00:09:49,920
 part of the Buddha's life

146
00:09:49,920 --> 00:09:53,720
 has unfortunately been minimized.

147
00:09:53,720 --> 00:09:56,680
 The Buddha spent 45 years teaching.

148
00:09:56,680 --> 00:09:59,280
 45 years.

149
00:09:59,280 --> 00:10:04,000
 The Thai version of what's now come to be claimed to be his

150
00:10:04,000 --> 00:10:07,200
 teachings takes up 45 volumes.

151
00:10:07,200 --> 00:10:09,640
 So they joke it's like he made a volume a year.

152
00:10:09,640 --> 00:10:17,510
 But it's huge. Spend 45 years and there's so much that we

153
00:10:17,510 --> 00:10:19,160
 have of his teachings.

154
00:10:19,160 --> 00:10:22,700
 And then you have the Lotus Sutra which I've been talking

155
00:10:22,700 --> 00:10:24,680
 about quite a bit recently.

156
00:10:24,680 --> 00:10:27,960
 Trivializing it.

157
00:10:27,960 --> 00:10:33,480
 And so you have a lot of later Buddhists and trivializing

158
00:10:33,480 --> 00:10:34,400
 that.

159
00:11:04,340 --> 00:11:09,160
 It didn't mean something that was not really true.

160
00:11:09,160 --> 00:11:13,940
 But if you follow it, it will help you see clear enough to

161
00:11:13,940 --> 00:11:15,960
 see the whole truth.

162
00:11:15,960 --> 00:11:19,680
 Anyway, there's generally a marginalization.

163
00:11:19,680 --> 00:11:29,080
 But for our purposes, those 45 years are most important.

164
00:11:29,080 --> 00:11:31,490
 And the question is do we have what the Buddha taught

165
00:11:31,490 --> 00:11:32,600
 during those times?

166
00:11:32,600 --> 00:11:34,800
 Do we still have it?

167
00:11:34,800 --> 00:11:39,320
 Do we know what the Buddha actually taught?

168
00:11:39,320 --> 00:11:46,370
 The Buddha said young etasming, antare, bhasati, lapati, n

169
00:11:46,370 --> 00:11:49,680
idissati.

170
00:11:49,680 --> 00:11:54,320
 Those what?

171
00:11:54,320 --> 00:11:59,840
 Whatever etasming between these two antare, these two

172
00:11:59,840 --> 00:12:02,360
 boundaries.

173
00:12:02,360 --> 00:12:12,220
 Bhasati said, said, lapati is spoken, nidissati is pointed

174
00:12:12,220 --> 00:12:13,400
 out.

175
00:12:13,400 --> 00:12:20,400
 Sabangtang tathivahoti no anyata.

176
00:12:20,400 --> 00:12:25,600
 All of that is thus.

177
00:12:25,600 --> 00:12:27,800
 Just thus.

178
00:12:27,800 --> 00:12:38,080
 Tathivahoti, it is just as thus.

179
00:12:38,080 --> 00:12:39,800
 Just in that way.

180
00:12:39,800 --> 00:12:42,920
 No anyata, not in another way.

181
00:12:42,920 --> 00:12:45,200
 Not otherwise.

182
00:12:45,200 --> 00:12:48,640
 So whatever he says, it's true.

183
00:12:48,640 --> 00:12:50,280
 It's thus.

184
00:12:50,280 --> 00:12:54,600
 Dasma tathagatoti wuchati.

185
00:12:54,600 --> 00:12:57,080
 Thus he is called tathagata.

186
00:12:57,080 --> 00:13:01,520
 Tathatam means thus, katam means gone.

187
00:13:01,520 --> 00:13:09,920
 Nidhavadi bhikvay tathagatoti tathagari.

188
00:13:09,920 --> 00:13:10,920
 Something else.

189
00:13:10,920 --> 00:13:15,040
 But he says, as he speaks, so he does.

190
00:13:15,040 --> 00:13:21,960
 Nidhavagari tathagatoti, as he does, so he speaks.

191
00:13:21,960 --> 00:13:27,880
 Dasma tathagatoti wuchati, thus he is called tathagata.

192
00:13:27,880 --> 00:13:35,000
 But the question, sorry, relates to the quote.

193
00:13:35,000 --> 00:13:38,240
 All of what he taught was truth for 45 years.

194
00:13:38,240 --> 00:13:40,520
 So the question is, what do we have?

195
00:13:40,520 --> 00:13:42,120
 Do we have that truth?

196
00:13:42,120 --> 00:13:47,220
 Some people say, it's been changed, it's been altered, it's

197
00:13:47,220 --> 00:13:51,080
 been recreated by sectarians,

198
00:13:51,080 --> 00:13:54,680
 everybody arguing that they have the truth.

199
00:13:54,680 --> 00:13:57,560
 So it's hard to be sure that you have the truth.

200
00:13:57,560 --> 00:14:01,560
 It's a bold claim here.

201
00:14:01,560 --> 00:14:04,790
 It's a great thing that the Buddha, great that the Buddha

202
00:14:04,790 --> 00:14:06,560
 actually taught the truth.

203
00:14:06,560 --> 00:14:08,800
 Question is, do we have the truth now?

204
00:14:08,800 --> 00:14:12,840
 Where do we find the truth?

205
00:14:12,840 --> 00:14:18,410
 And the curious thing that I find which gives great faith

206
00:14:18,410 --> 00:14:22,560
 in what we claim to be the Buddha's

207
00:14:22,560 --> 00:14:27,480
 teaching, for the most part anyway, is that if you take a

208
00:14:27,480 --> 00:14:30,720
 practice like the one we practice,

209
00:14:30,720 --> 00:14:38,260
 which I have perfect faith in it, I mean, there's no

210
00:14:38,260 --> 00:14:43,160
 question that it is objective.

211
00:14:43,160 --> 00:14:44,160
 You can argue what you want.

212
00:14:44,160 --> 00:14:50,760
 You can say this practice is dumb or wrong or misguided.

213
00:14:50,760 --> 00:14:54,960
 But you can't say that it's partial or biased.

214
00:14:54,960 --> 00:14:55,960
 Not exactly.

215
00:14:55,960 --> 00:15:00,530
 Unless you want to say it's biased in a pedantic sort of

216
00:15:00,530 --> 00:15:03,360
 way or it's overly technical or so

217
00:15:03,360 --> 00:15:05,560
 on.

218
00:15:05,560 --> 00:15:07,640
 But it's systematic and it's subjective.

219
00:15:07,640 --> 00:15:14,600
 When you sit and say pain, pain, there's no bias there.

220
00:15:14,600 --> 00:15:16,280
 You're seeing pain as pain.

221
00:15:16,280 --> 00:15:21,400
 You're reminding yourself that's pain.

222
00:15:21,400 --> 00:15:24,920
 When you say rising, falling, there's no bias here.

223
00:15:24,920 --> 00:15:28,370
 So what I'm trying to say is this is an objective practice,

224
00:15:28,370 --> 00:15:30,360
 the practice of objectivity.

225
00:15:30,360 --> 00:15:31,360
 You can dislike it.

226
00:15:31,360 --> 00:15:33,200
 You can say, hey, it's not for me.

227
00:15:33,200 --> 00:15:35,240
 It's not going to lead me anywhere.

228
00:15:35,240 --> 00:15:37,600
 But you can't argue that it's not objective.

229
00:15:37,600 --> 00:15:41,440
 And the interesting thing is, if you practice this way

230
00:15:41,440 --> 00:15:44,240
 without any instruction in the greater

231
00:15:44,240 --> 00:15:48,830
 context of the Buddha's teaching, if you practice

232
00:15:48,830 --> 00:15:52,560
 diligently for some time, you can go and read

233
00:15:52,560 --> 00:15:56,270
 all the Buddha's teaching and you can find for yourself the

234
00:15:56,270 --> 00:15:56,800
 truth.

235
00:15:56,800 --> 00:15:58,800
 I would argue.

236
00:15:58,800 --> 00:16:03,610
 What I mean to say is you react differently to different

237
00:16:03,610 --> 00:16:05,560
 kinds of teaching.

238
00:16:05,560 --> 00:16:08,880
 If you focus on objectivity, if you focus on a practice

239
00:16:08,880 --> 00:16:11,180
 that is subjective, not a practice

240
00:16:11,180 --> 00:16:16,340
 that focuses on some God or some angel or some love or

241
00:16:16,340 --> 00:16:20,040
 compassion, you focus on truth.

242
00:16:20,040 --> 00:16:21,040
 This is pain.

243
00:16:21,040 --> 00:16:23,720
 If you say to yourself, pain, pain, you see, you can argue

244
00:16:23,720 --> 00:16:25,320
 what you want, but you're seeing

245
00:16:25,320 --> 00:16:27,760
 pain as pain.

246
00:16:27,760 --> 00:16:29,240
 You're telling yourself pain is pain.

247
00:16:29,240 --> 00:16:30,240
 Is that bias?

248
00:16:30,240 --> 00:16:31,240
 No.

249
00:16:31,240 --> 00:16:32,240
 Is that arbitrary?

250
00:16:32,240 --> 00:16:33,240
 No.

251
00:16:33,240 --> 00:16:36,530
 So then when you go and look at any kind of teachings, you

252
00:16:36,530 --> 00:16:38,320
 don't need someone to tell you

253
00:16:38,320 --> 00:16:42,000
 that that's the truth.

254
00:16:42,000 --> 00:16:43,000
 You can look at it.

255
00:16:43,000 --> 00:16:48,280
 You can say, hmm, that is true.

256
00:16:48,280 --> 00:16:51,980
 You can tell the difference between a teaching that is just

257
00:16:51,980 --> 00:16:54,040
 saying good, saying nice things

258
00:16:54,040 --> 00:16:59,290
 and then saying things that are just sophism or

259
00:16:59,290 --> 00:17:04,360
 intellectualism versus teachings that actually

260
00:17:04,360 --> 00:17:06,900
 teach something important.

261
00:17:06,900 --> 00:17:07,900
 You can tell the difference.

262
00:17:07,900 --> 00:17:13,160
 So the interesting thing is you react to teachings.

263
00:17:13,160 --> 00:17:16,350
 Like if you read the tipitaka, for example, without having

264
00:17:16,350 --> 00:17:18,280
 practiced this kind of meditation,

265
00:17:18,280 --> 00:17:20,280
 I think you have a hard time with it.

266
00:17:20,280 --> 00:17:30,470
 I think it's hard to find it even interesting, let alone

267
00:17:30,470 --> 00:17:37,720
 comprehendable, understandable.

268
00:17:37,720 --> 00:17:42,360
 But meditation is that powerful.

269
00:17:42,360 --> 00:17:50,470
 This shows the importance, the power and the purity of

270
00:17:50,470 --> 00:17:56,720
 meditation, the depth, the profundity

271
00:17:56,720 --> 00:18:06,360
 of it, that it allows you to see.

272
00:18:06,360 --> 00:18:12,840
 It allows you to see in your life problems that you had.

273
00:18:12,840 --> 00:18:18,960
 It allows you to see through them, to solve them without

274
00:18:18,960 --> 00:18:19,600
 doubt.

275
00:18:19,600 --> 00:18:24,330
 And teachings, views, it allows you to see right view from

276
00:18:24,330 --> 00:18:26,640
 wrong view without bias, without

277
00:18:26,640 --> 00:18:27,640
 doubt.

278
00:18:27,640 --> 00:18:33,350
 You practice in this way and that really goes for all sorts

279
00:18:33,350 --> 00:18:35,680
 of kinds of practice.

280
00:18:35,680 --> 00:18:39,560
 If you practice in that way, you'll see in that way.

281
00:18:39,560 --> 00:18:42,560
 So that's why it's important that our practice be objective

282
00:18:42,560 --> 00:18:44,600
, our practice be based on objectivity

283
00:18:44,600 --> 00:18:47,280
 because if you practice in a different religious tradition,

284
00:18:47,280 --> 00:18:48,680
 well, you'll look at the world

285
00:18:48,680 --> 00:18:50,930
 in a different way and you'll gravitate towards different

286
00:18:50,930 --> 00:18:51,440
 texts.

287
00:18:51,440 --> 00:18:54,060
 If you practice Christianity, you'll gravitate towards the

288
00:18:54,060 --> 00:18:54,520
 Bible.

289
00:18:54,520 --> 00:18:57,400
 You'll say, "Oh yes, the Bible is the truth because it

290
00:18:57,400 --> 00:18:58,760
 resonates with you."

291
00:18:58,760 --> 00:19:01,860
 But it resonates, we would argue, because you become

292
00:19:01,860 --> 00:19:03,920
 partial and biased and you've taken

293
00:19:03,920 --> 00:19:07,560
 up views and beliefs.

294
00:19:07,560 --> 00:19:14,330
 So this is my test for text is see through the lens of

295
00:19:14,330 --> 00:19:16,400
 objectivity.

296
00:19:16,400 --> 00:19:23,290
 You don't take any belief, you'll see things as they are,

297
00:19:23,290 --> 00:19:26,600
 see pain as pain, see thoughts

298
00:19:26,600 --> 00:19:33,520
 as thoughts, see anger as anger, see greed as greed, see

299
00:19:33,520 --> 00:19:36,600
 delusion as delusion.

300
00:19:36,600 --> 00:19:45,200
 Anyway, then you can see the truth, then you can find the

301
00:19:45,200 --> 00:19:45,840
 Buddhist teaching.

302
00:19:45,840 --> 00:19:50,260
 That relates to the other quotes that we've had, you know,

303
00:19:50,260 --> 00:19:52,540
 "Yodhammaṁ passati, so man

304
00:19:52,540 --> 00:19:56,320
 passati," who sees the Dhamma, sees the Buddha, sees the D

305
00:19:56,320 --> 00:19:57,280
hatagata.

306
00:19:57,280 --> 00:20:02,360
 If you don't see the Dhamma, you can't see the Buddha.

307
00:20:02,360 --> 00:20:04,300
 You can't find the Buddha's teaching.

308
00:20:04,300 --> 00:20:07,580
 Even you have the whole of the Tipitika, you'll never find

309
00:20:07,580 --> 00:20:09,560
 the teaching there, not unless

310
00:20:09,560 --> 00:20:15,120
 you see the Dhamma, not unless you're practicing the Dhamma

311
00:20:15,120 --> 00:20:15,200
.

312
00:20:15,200 --> 00:20:19,140
 You can read all, you can memorize the 45 volumes and you

313
00:20:19,140 --> 00:20:20,680
 still won't come close to

314
00:20:20,680 --> 00:20:21,680
 the truth.

315
00:20:21,680 --> 00:20:27,060
 But as soon as you practice, it's like it opens the, it's

316
00:20:27,060 --> 00:20:30,200
 like it's a cipher that deciphers

317
00:20:30,200 --> 00:20:33,040
 the code.

318
00:20:33,040 --> 00:20:34,040
 Suddenly you understand.

319
00:20:34,040 --> 00:20:36,760
 Otherwise people I think find it, would find it boring,

320
00:20:36,760 --> 00:20:39,520
 maybe interesting intellectually,

321
00:20:39,520 --> 00:20:41,960
 but incomprehensible.

322
00:20:41,960 --> 00:20:52,760
 Anyway, that's our quote for tonight.

323
00:20:52,760 --> 00:20:59,160
 Anybody have any questions?

324
00:20:59,160 --> 00:21:04,760
 We're doing text questions again, so welcome to ask.

325
00:21:04,760 --> 00:21:06,760
 You guys can go.

326
00:21:06,760 --> 00:21:13,760
 Two meditators here, we can say hi to our meditators.

327
00:21:13,760 --> 00:21:18,760
 Both are staying here tonight.

328
00:21:18,760 --> 00:21:25,760
 Thank you.

329
00:21:25,760 --> 00:21:32,760
 Thank you.

330
00:21:32,760 --> 00:21:39,760
 Thank you.

331
00:21:39,760 --> 00:21:46,760
 Thank you.

332
00:21:46,760 --> 00:21:53,760
 Thank you.

333
00:21:54,760 --> 00:22:01,760
 Thank you.

334
00:22:02,760 --> 00:22:09,760
 Thank you.

335
00:22:09,760 --> 00:22:16,760
 No questions?

336
00:22:16,760 --> 00:22:23,760
 All right.

337
00:22:23,760 --> 00:22:42,760
 Good night, everyone.

338
00:22:42,760 --> 00:22:48,760
 Thanks for tuning in.

