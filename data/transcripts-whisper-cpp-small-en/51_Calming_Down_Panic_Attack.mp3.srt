1
00:00:00,000 --> 00:00:03,200
 Calming down - panic attack.

2
00:00:03,200 --> 00:00:06,440
 I have anxiety panic attacks at least one time per day.

3
00:00:06,440 --> 00:00:07,820
 How can I calm down?

4
00:00:07,820 --> 00:00:13,480
 If I think about what is happening, the panic becomes worse

5
00:00:13,480 --> 00:00:13,500
.

6
00:00:13,500 --> 00:00:16,030
 When you are mindful of anxiety, you will see that the

7
00:00:16,030 --> 00:00:17,340
 moments of anxiety are really

8
00:00:17,340 --> 00:00:20,320
 not a very big problem at all.

9
00:00:20,320 --> 00:00:23,100
 Anxiety is a state of mind that causes physical reactions

10
00:00:23,100 --> 00:00:24,880
 like tensing, but it really stops

11
00:00:24,880 --> 00:00:26,280
 there.

12
00:00:26,280 --> 00:00:29,310
 Unless we react to anxiety or its physical results in a

13
00:00:29,310 --> 00:00:31,840
 negative way, it is just an experience.

14
00:00:31,840 --> 00:00:34,930
 The real problem is not experiences, even inherently

15
00:00:34,930 --> 00:00:37,220
 negative experiences like anxiety.

16
00:00:37,220 --> 00:00:40,410
 The problem is our tendency to react to them, to react

17
00:00:40,410 --> 00:00:42,920
 negatively to a negative experience.

18
00:00:42,920 --> 00:00:46,960
 The truth is, if you recognize the anxiety as anxiety, the

19
00:00:46,960 --> 00:00:49,160
 anxiety itself disappears.

20
00:00:49,160 --> 00:00:53,520
 The anxious mind state is replaced by a mindful mind state.

21
00:00:53,520 --> 00:00:56,150
 What does not disappear and what fools you into thinking

22
00:00:56,150 --> 00:00:57,580
 that you are still anxious is

23
00:00:57,580 --> 00:01:01,990
 the physical results of the anxiety, because they are not

24
00:01:01,990 --> 00:01:05,120
 dependent on the anxiety to continue.

25
00:01:05,120 --> 00:01:08,180
 The problem is we get worried about the physical effects of

26
00:01:08,180 --> 00:01:10,080
 anxiety and say to ourselves, "It

27
00:01:10,080 --> 00:01:13,760
 is not going away," and then we get anxious again.

28
00:01:13,760 --> 00:01:17,240
 The anxiety comes up again and we try to be mindful again.

29
00:01:17,240 --> 00:01:19,920
 We find that yes, the effects of it are still there and so

30
00:01:19,920 --> 00:01:21,440
 we think the anxiety is still

31
00:01:21,440 --> 00:01:22,440
 there.

32
00:01:22,440 --> 00:01:24,980
 Often you need a teacher to point it out to you that the

33
00:01:24,980 --> 00:01:26,840
 physical and mental are two different

34
00:01:26,840 --> 00:01:28,440
 things.

35
00:01:28,440 --> 00:01:30,650
 Seeing the difference between the physical and the mental

36
00:01:30,650 --> 00:01:31,880
 is the first knowledge gained

37
00:01:31,880 --> 00:01:34,040
 from mindfulness meditation.

38
00:01:34,040 --> 00:01:37,480
 When you say anxious, anxious, or worried, worried, the

39
00:01:37,480 --> 00:01:39,040
 worry is already gone.

40
00:01:39,040 --> 00:01:42,490
 At that moment you are not worried, rather you are focused,

41
00:01:42,490 --> 00:01:44,460
 mindful, and clearly aware.

42
00:01:44,460 --> 00:01:46,940
 The physical is something separate from this.

43
00:01:46,940 --> 00:01:50,470
 The effects of past anxiety will not immediately go away

44
00:01:50,470 --> 00:01:52,060
 when you are mindful.

45
00:01:52,060 --> 00:01:54,770
 What you should do at that point when you still feel the

46
00:01:54,770 --> 00:01:56,320
 effects of the anxiety is see

47
00:01:56,320 --> 00:01:59,450
 the effects as physical and remind yourself of what they

48
00:01:59,450 --> 00:02:00,440
 actually are.

49
00:02:00,440 --> 00:02:03,440
 Note the feeling in the stomach as feeling, feeling, or

50
00:02:03,440 --> 00:02:04,440
 tense, tense.

51
00:02:04,440 --> 00:02:07,480
 If you react to it, for example by disliking it, you should

52
00:02:07,480 --> 00:02:09,000
 focus on that new mind state

53
00:02:09,000 --> 00:02:12,080
 and say to yourself, "Disliking, disliking."

54
00:02:12,080 --> 00:02:15,070
 An anxiety attack is not just anxiousness, it is not an

55
00:02:15,070 --> 00:02:17,120
 entity that exists continuously,

56
00:02:17,120 --> 00:02:20,940
 it is many experiences arising and ceasing moment by moment

57
00:02:20,940 --> 00:02:21,160
.

58
00:02:21,160 --> 00:02:24,640
 There is anxiety, then the physical feeling, then disliking

59
00:02:24,640 --> 00:02:26,320
 the physical feeling, then

60
00:02:26,320 --> 00:02:28,640
 anxiety again, etc.

61
00:02:28,640 --> 00:02:31,640
 So many different experiences are involved.

62
00:02:31,640 --> 00:02:34,840
 There can be ego as well, where one might think, "I am

63
00:02:34,840 --> 00:02:35,720
 anxious."

64
00:02:35,720 --> 00:02:38,640
 There can be the desire to be confident and to impress

65
00:02:38,640 --> 00:02:39,640
 other people.

66
00:02:39,640 --> 00:02:42,080
 Your meditation practice is helping you to see that these

67
00:02:42,080 --> 00:02:43,840
 things are impermanent, suffering,

68
00:02:43,840 --> 00:02:44,840
 and non-self.

69
00:02:44,840 --> 00:02:48,270
 They are not under your control, neither the physical nor

70
00:02:48,270 --> 00:02:49,920
 the mental is subject to your

71
00:02:49,920 --> 00:02:51,200
 control.

72
00:02:51,200 --> 00:02:53,620
 The more you cling to it, the more you try to fix or avoid

73
00:02:53,620 --> 00:02:55,000
 it, try to change it, or try

74
00:02:55,000 --> 00:02:57,570
 to make it better, the more suffering, stress, and

75
00:02:57,570 --> 00:02:59,880
 disappointment you will create for yourself

76
00:02:59,880 --> 00:03:03,180
 and the bigger the problem will become.

77
00:03:03,180 --> 00:03:06,500
 When the anxiety comes, it is correct to say to yourself, "

78
00:03:06,500 --> 00:03:07,920
Anxious, anxious."

79
00:03:07,920 --> 00:03:11,050
 And it is correct to focus on it, to be mindful of it, and

80
00:03:11,050 --> 00:03:12,880
 to let yourself be anxious.

81
00:03:12,880 --> 00:03:14,920
 The anxiety is not the problem.

82
00:03:14,920 --> 00:03:18,170
 The problem is your reaction to physical states that are

83
00:03:18,170 --> 00:03:19,680
 caused by the anxiety.

84
00:03:19,680 --> 00:03:20,960
 Anxiety is just a moment.

85
00:03:20,960 --> 00:03:23,840
 It is something that occurs in the moment and that it is

86
00:03:23,840 --> 00:03:24,300
 gone.

87
00:03:24,300 --> 00:03:27,210
 At the moment when you are mindful, it is already gone, and

88
00:03:27,210 --> 00:03:28,200
 all that is left is the

89
00:03:28,200 --> 00:03:31,880
 physical experiences which should be noted as well.

90
00:03:31,880 --> 00:03:34,390
 If you can be thorough with this and note what is present

91
00:03:34,390 --> 00:03:35,800
 in each moment, the physical

92
00:03:35,800 --> 00:03:38,530
 sensations, the disliking, the thoughts and feelings, you

93
00:03:38,530 --> 00:03:40,160
 will begin to realize that nothing

94
00:03:40,160 --> 00:03:43,840
 has the power to hurt you until you react to it.

95
00:03:43,840 --> 00:03:48,460
 Even negative mind states cannot hurt you until you react

96
00:03:48,460 --> 00:03:49,340
 to them.

97
00:03:49,340 --> 00:03:52,260
 If you are patient with the anxiety, noting it and noting

98
00:03:52,260 --> 00:03:54,000
 the physical sensations as well

99
00:03:54,000 --> 00:03:57,040
 as the disliking of the anxiety, even though they do not

100
00:03:57,040 --> 00:03:58,760
 seem to go away, noting every

101
00:03:58,760 --> 00:04:01,580
 physical and mental experience that arises from moment to

102
00:04:01,580 --> 00:04:02,980
 moment, you will be able to

103
00:04:02,980 --> 00:04:06,050
 dispel the perception of being anxious, and you will cease

104
00:04:06,050 --> 00:04:07,480
 to be disturbed by what you

105
00:04:07,480 --> 00:04:10,960
 can see are simply individual moments of experience.

106
00:04:10,960 --> 00:04:14,200
 You will see that they do not have any power over you.

107
00:04:14,200 --> 00:04:17,760
 This realization is what truly leads to overcoming anxiety.

108
00:04:17,760 --> 00:04:21,100
 It may take time, effort and patience, but the path to

109
00:04:21,100 --> 00:04:23,160
 overcoming both anxiety and fear

110
00:04:23,160 --> 00:04:26,250
 is easy to follow when you realize that you can be anxious

111
00:04:26,250 --> 00:04:27,780
 or afraid without having to

112
00:04:27,780 --> 00:04:30,480
 panic or be consumed by the moments of emotion.

113
00:04:30,480 --> 00:04:40,480
 [BLANK_AUDIO]

