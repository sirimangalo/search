1
00:00:00,000 --> 00:00:19,600
 [silence]

2
00:00:19,600 --> 00:00:23,600
 Good evening everyone.

3
00:00:23,600 --> 00:00:34,400
 Broadcasting Live, January 25th, 2016.

4
00:00:34,400 --> 00:00:39,160
 Tonight our quote is about a believer.

5
00:00:39,160 --> 00:00:43,280
 One makes a person a believer.

6
00:00:43,280 --> 00:00:52,880
 Not quite sure what the poly of that would be.

7
00:00:52,880 --> 00:01:02,080
 But today we had a meeting, our first meeting of the

8
00:01:02,080 --> 00:01:07,600
 Buddhism Association, the university.

9
00:01:07,600 --> 00:01:14,200
 We talked about this idea of belief.

10
00:01:14,200 --> 00:01:23,280
 Buddhism doesn't really promote the concept of belief.

11
00:01:23,280 --> 00:01:25,440
 But I think that's not quite fair.

12
00:01:25,440 --> 00:01:32,280
 We have to be careful how far we go with that.

13
00:01:32,280 --> 00:01:39,800
 Because anything you do, you have to believe in it.

14
00:01:39,800 --> 00:01:45,630
 And believe in what you're doing makes it very difficult to

15
00:01:45,630 --> 00:01:46,880
 succeed.

16
00:01:46,880 --> 00:01:48,920
 You have to make one of two choices.

17
00:01:48,920 --> 00:01:52,240
 You have to decide to stop doing it.

18
00:01:52,240 --> 00:01:59,200
 And you have to change the way you're looking at it.

19
00:01:59,200 --> 00:02:03,080
 Could be that what you're doing is not worth believing in.

20
00:02:03,080 --> 00:02:11,490
 And if what you're doing is just something you have to

21
00:02:11,490 --> 00:02:15,440
 force yourself to practice without

22
00:02:15,440 --> 00:02:22,370
 any sense that it's authentic or beneficial, then you're

23
00:02:22,370 --> 00:02:25,880
 either doing it wrong or it's

24
00:02:25,880 --> 00:02:31,080
 not worth doing.

25
00:02:31,080 --> 00:02:33,440
 So it's important that we believe.

26
00:02:33,440 --> 00:02:37,800
 And every religion has its believers.

27
00:02:37,800 --> 00:02:43,280
 Some people believe out of blind faith.

28
00:02:43,280 --> 00:02:47,560
 Some people believe out of logic and reasoning.

29
00:02:47,560 --> 00:02:54,560
 And some people believe out of experience.

30
00:02:54,560 --> 00:03:02,460
 This quote is talking really about the...you could say it's

31
00:03:02,460 --> 00:03:03,600
 talking about all these people.

32
00:03:03,600 --> 00:03:09,030
 But it's just talking about the signs of someone who

33
00:03:09,030 --> 00:03:10,480
 believes.

34
00:03:10,480 --> 00:03:12,400
 Someone who believes in what they're doing.

35
00:03:12,400 --> 00:03:15,230
 In this case, someone who believes in the Buddha's

36
00:03:15,230 --> 00:03:17,360
 teachings, whether it's out of blind

37
00:03:17,360 --> 00:03:30,680
 faith, reasoning, or whether it's out of experience.

38
00:03:30,680 --> 00:03:35,800
 The first sign is that they want to see good people.

39
00:03:35,800 --> 00:03:42,960
 They like to be around good people.

40
00:03:42,960 --> 00:03:45,570
 You look at people meditating and you think it's a waste of

41
00:03:45,570 --> 00:03:46,040
 time.

42
00:03:46,040 --> 00:03:53,810
 You look at Buddhist monks and you think they're lazy or

43
00:03:53,810 --> 00:03:56,680
 parasites or useless.

44
00:03:56,680 --> 00:04:00,600
 You think they're torturing themselves.

45
00:04:00,600 --> 00:04:04,600
 Well these are signs that you're not a believer.

46
00:04:04,600 --> 00:04:11,120
 You don't believe in what the Buddha taught.

47
00:04:11,120 --> 00:04:18,120
 You don't want to be around good people.

48
00:04:18,120 --> 00:04:23,980
 The sign of a believer is someone who likes to go to

49
00:04:23,980 --> 00:04:25,720
 meditation centers, likes to go to

50
00:04:25,720 --> 00:04:26,720
 monasteries.

51
00:04:26,720 --> 00:04:32,080
 Nowadays, someone who likes to watch YouTube videos of

52
00:04:32,080 --> 00:04:35,280
 Buddhist teachers, who wants to

53
00:04:35,280 --> 00:04:48,320
 see and hear from most of you around good people.

54
00:04:48,320 --> 00:04:54,910
 The second that it mentions is the desire to hear the dham

55
00:04:54,910 --> 00:04:55,680
ma.

56
00:04:55,680 --> 00:05:01,170
 If you feel bored or disinterested in hearing about

57
00:05:01,170 --> 00:05:05,640
 difficult teachings like the Four Noble

58
00:05:05,640 --> 00:05:09,850
 Truths, you don't want to hear about suffering or the cause

59
00:05:09,850 --> 00:05:11,120
 of suffering.

60
00:05:11,120 --> 00:05:15,920
 They get bored hearing about the Eightfold Noble Path.

61
00:05:15,920 --> 00:05:25,520
 I think eight things, oh that's too much.

62
00:05:25,520 --> 00:05:30,110
 Right view, right thought, right speech, right action,

63
00:05:30,110 --> 00:05:33,280
 right livelihood, right effort, right

64
00:05:33,280 --> 00:05:35,280
 mindfulness, right concentration.

65
00:05:35,280 --> 00:05:38,040
 If that doesn't interest you, those things don't interest

66
00:05:38,040 --> 00:05:38,440
 you.

67
00:05:38,440 --> 00:05:44,320
 It's a sign you're not a believer.

68
00:05:44,320 --> 00:05:46,360
 But when you hear these things and they interest you and

69
00:05:46,360 --> 00:05:47,640
 you want to hear more and you want

70
00:05:47,640 --> 00:05:55,300
 to study them and you want to listen to talks on these

71
00:05:55,300 --> 00:06:02,080
 things, it's a sign of a believer,

72
00:06:02,080 --> 00:06:12,760
 someone who goes out of their way to hear the dhamma.

73
00:06:12,760 --> 00:06:22,360
 And the third sign is that they are generous, charitable.

74
00:06:22,360 --> 00:06:26,960
 I think this probably refers mostly to being charitable

75
00:06:26,960 --> 00:06:29,680
 towards Buddhist monastics.

76
00:06:29,680 --> 00:06:34,150
 But you could apply, you could argue in general, someone

77
00:06:34,150 --> 00:06:36,880
 who is Buddhist, they become more

78
00:06:36,880 --> 00:06:37,880
 charitable.

79
00:06:37,880 --> 00:06:42,400
 I don't know that it's definitely a sign of someone who

80
00:06:42,400 --> 00:06:45,000
 believes in Buddhism because

81
00:06:45,000 --> 00:06:47,120
 other religions are of course charitable.

82
00:06:47,120 --> 00:06:51,730
 So I think I'd relate this to being charitable towards

83
00:06:51,730 --> 00:06:55,120
 Buddhist monastics and also Buddhist

84
00:06:55,120 --> 00:06:56,120
 meditators.

85
00:06:56,120 --> 00:07:00,050
 You know people who help others come to meditate or who

86
00:07:00,050 --> 00:07:02,760
 volunteer at meditation centers or

87
00:07:02,760 --> 00:07:09,020
 monasteries, people who offer food to meditators or mon

88
00:07:09,020 --> 00:07:13,600
astics, people who support meditation

89
00:07:13,600 --> 00:07:16,950
 teachers and support meditation students, this kind of

90
00:07:16,950 --> 00:07:17,560
 thing.

91
00:07:17,560 --> 00:07:22,730
 I think you'd have to argue that that's more in line with

92
00:07:22,730 --> 00:07:26,200
 this teaching, more of what is

93
00:07:26,200 --> 00:07:33,800
 in line with Buddhism.

94
00:07:33,800 --> 00:07:34,350
 But still Buddhism is something that makes you more

95
00:07:34,350 --> 00:07:34,800
 charitable because of course with

96
00:07:34,800 --> 00:07:39,600
 pure attachments you're less concerned with your own luxury

97
00:07:39,600 --> 00:07:40,800
 and comfort.

98
00:07:40,800 --> 00:07:52,040
 So you have no problem with stinginess or greed.

99
00:07:52,040 --> 00:07:56,410
 So there's a sign that you believe in something when you

100
00:07:56,410 --> 00:07:57,640
 support it.

101
00:07:57,640 --> 00:08:00,980
 Some people they might be casually interested in but the

102
00:08:00,980 --> 00:08:03,200
 sign of a true believer in something

103
00:08:03,200 --> 00:08:05,320
 is that they support it.

104
00:08:05,320 --> 00:08:13,660
 They do work and they sacrifice time and they sacrifice

105
00:08:13,660 --> 00:08:19,000
 resources for that thing.

106
00:08:19,000 --> 00:08:23,800
 So this isn't a deep meditative teaching but these are

107
00:08:23,800 --> 00:08:24,760
 signs.

108
00:08:24,760 --> 00:08:27,040
 And I guess in general you could say that there are signs.

109
00:08:27,040 --> 00:08:30,880
 It's important to understand that if you've been practicing

110
00:08:30,880 --> 00:08:32,680
 something for many years and

111
00:08:32,680 --> 00:08:39,140
 it hasn't changed you at all, you should be able to by that

112
00:08:39,140 --> 00:08:45,080
 time figure that it's useless.

113
00:08:45,080 --> 00:08:48,670
 On the other hand if after practicing or undertaking

114
00:08:48,670 --> 00:08:51,760
 something for a short time you already begin

115
00:08:51,760 --> 00:08:56,710
 to see changes in yourself then you can understand that it

116
00:08:56,710 --> 00:08:59,800
's a benefit, it's not useless.

117
00:08:59,800 --> 00:09:04,500
 Now for those of you doing meditation courses you have to

118
00:09:04,500 --> 00:09:07,000
 remember that we push you all

119
00:09:07,000 --> 00:09:09,200
 the time through the course.

120
00:09:09,200 --> 00:09:15,520
 Sometimes it's just discouraging.

121
00:09:15,520 --> 00:09:18,720
 I always talk about how do I know whether I'm progressing.

122
00:09:18,720 --> 00:09:21,890
 The biggest question, the worst question really, or one of

123
00:09:21,890 --> 00:09:24,600
 the worst ones but it's really a

124
00:09:24,600 --> 00:09:28,640
 killer because one moment to the next is very different.

125
00:09:28,640 --> 00:09:32,600
 One moment you think you're almost enlightened, the next

126
00:09:32,600 --> 00:09:35,120
 moment you think you're useless.

127
00:09:35,120 --> 00:09:42,000
 It's not a straight path.

128
00:09:42,000 --> 00:09:44,900
 And especially on a meditation course it's hard to see the

129
00:09:44,900 --> 00:09:46,360
 progress because as soon as

130
00:09:46,360 --> 00:09:52,470
 you start to progress we make it harder for you to push you

131
00:09:52,470 --> 00:09:54,720
 to the next level.

132
00:09:54,720 --> 00:09:56,760
 But there are signs.

133
00:09:56,760 --> 00:09:58,000
 These are some signs.

134
00:09:58,000 --> 00:09:59,520
 Faith is one sign.

135
00:09:59,520 --> 00:10:02,560
 You become more confident in what you're doing.

136
00:10:02,560 --> 00:10:06,740
 You see clearly the nature of reality, you see impermanence

137
00:10:06,740 --> 00:10:08,680
, you see suffering, you see

138
00:10:08,680 --> 00:10:09,680
 non-self.

139
00:10:09,680 --> 00:10:15,200
 It becomes clearer that there's only one way.

140
00:10:15,200 --> 00:10:20,800
 You can't run away, you can't avoid the problem.

141
00:10:20,800 --> 00:10:28,560
 You can't just cover it up and hope it won't come back.

142
00:10:28,560 --> 00:10:34,820
 As you see clearly then you gain the faith, you gain

143
00:10:34,820 --> 00:10:36,560
 confidence.

144
00:10:36,560 --> 00:10:39,990
 That's really what's going on here, what it's talking about

145
00:10:39,990 --> 00:10:40,280
 here.

146
00:10:40,280 --> 00:10:45,740
 Because blind faith is not really a good measure of someone

147
00:10:45,740 --> 00:10:48,520
's devotion to something.

148
00:10:48,520 --> 00:10:54,320
 Blind faith is very weak, it's very fragile.

149
00:10:54,320 --> 00:11:01,200
 It can break in a moment.

150
00:11:01,200 --> 00:11:05,600
 Faith that's based on reason is more, is stronger, but it

151
00:11:05,600 --> 00:11:07,040
 can still be misled.

152
00:11:07,040 --> 00:11:14,200
 It can still be twisted and confused.

153
00:11:14,200 --> 00:11:17,880
 But faith that comes from knowledge, comes from empirical

154
00:11:17,880 --> 00:11:20,000
 understanding, from realizing

155
00:11:20,000 --> 00:11:25,680
 yourself, this is the strongest.

156
00:11:25,680 --> 00:11:31,110
 When you realize the ultimate truth, when you see nirvana,

157
00:11:31,110 --> 00:11:33,760
 you see nirvana for yourself,

158
00:11:33,760 --> 00:11:34,760
 it's invincible.

159
00:11:34,760 --> 00:11:37,750
 The knowledge, the understanding and the faith, the

160
00:11:37,750 --> 00:11:42,680
 confidence that comes from it is unbreakable,

161
00:11:42,680 --> 00:11:44,360
 unshakable.

162
00:11:44,360 --> 00:11:50,680
 When you finally see the ultimate state.

163
00:11:50,680 --> 00:11:58,600
 So just a short quote and a short talk.

164
00:11:58,600 --> 00:12:01,160
 Onadama for tonight.

165
00:12:01,160 --> 00:12:04,680
 Thank you all for tuning in.

166
00:12:04,680 --> 00:12:05,240
 Have a good night.

