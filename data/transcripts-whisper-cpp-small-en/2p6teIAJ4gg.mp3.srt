1
00:00:00,000 --> 00:00:07,360
 And here's the new Kuti, the latest place to put Kutis from

2
00:00:07,360 --> 00:00:08,000
 outside.

3
00:00:08,000 --> 00:00:12,810
 So this is after we're finished, so now we can walk through

4
00:00:12,810 --> 00:00:13,000
 it.

5
00:00:13,000 --> 00:00:16,000
 This is what we took out of it.

6
00:00:16,000 --> 00:00:20,590
 This was the way up, now we can't come up yet to get rid of

7
00:00:20,590 --> 00:00:23,000
 the wood first.

8
00:00:23,000 --> 00:00:31,000
 And this is enough room for three Kutis.

9
00:00:31,000 --> 00:00:36,840
 And so there's another way down, that I didn't know about

10
00:00:36,840 --> 00:00:38,000
 until now.

11
00:00:38,000 --> 00:00:42,000
 And that's this way.

12
00:00:42,000 --> 00:00:46,000
 Watch the step.

13
00:00:46,000 --> 00:00:54,000
 And we'll come down to this nice pool of water.

14
00:00:54,000 --> 00:01:01,050
 This is where I first saw this four foot long lizard

15
00:01:01,050 --> 00:01:03,000
 swimming.

16
00:01:03,000 --> 00:01:05,000
 Isn't it wonderful?

17
00:01:05,000 --> 00:01:07,000
 It's a wonderful place.

18
00:01:07,000 --> 00:01:13,000
 Okay, just another update for all of you who are interested

19
00:01:13,000 --> 00:01:13,000
.

20
00:01:13,000 --> 00:01:16,000
 Everyone, Serenay, all the best.

