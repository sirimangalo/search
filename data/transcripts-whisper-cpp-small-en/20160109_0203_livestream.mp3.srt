1
00:00:00,000 --> 00:00:07,000
 Did I look not okay?

2
00:00:07,000 --> 00:00:13,000
 Good evening.

3
00:00:13,000 --> 00:00:26,400
 I'm broadcasting live January 8, 2016.

4
00:00:26,400 --> 00:00:44,400
 So I heard back from Ted, TedX, the TedX people.

5
00:00:44,400 --> 00:00:50,400
 And I'm a semi-finalist for their TedX conference.

6
00:00:50,400 --> 00:00:55,400
 There's 16 of us.

7
00:00:55,400 --> 00:01:04,400
 I think they only select four.

8
00:01:04,400 --> 00:01:08,400
 Must be more than four.

9
00:01:08,400 --> 00:01:14,400
 So now I have to write out a talk. Now it's real work.

10
00:01:14,400 --> 00:01:19,730
 I have to write out a talk and go to an interview and a

11
00:01:19,730 --> 00:01:23,400
 workshop.

12
00:01:23,400 --> 00:01:28,790
 I'm not sure what I've gotten myself into, but I have to

13
00:01:28,790 --> 00:01:32,400
 write out the talk.

14
00:01:32,400 --> 00:01:38,140
 20 minutes, no these talks? For 10 minutes? How long are

15
00:01:38,140 --> 00:01:39,400
 they?

16
00:01:39,400 --> 00:01:43,400
 20 minutes seems long. I can't remember.

17
00:01:43,400 --> 00:01:46,630
 I thought Ted talks for 20 minutes. Maybe they're 10

18
00:01:46,630 --> 00:01:47,400
 minutes.

19
00:01:47,400 --> 00:01:54,400
 Maybe 10.

20
00:01:54,400 --> 00:01:58,640
 Yeah, they didn't let us know that at all. Am I supposed to

21
00:01:58,640 --> 00:02:00,400
 know these things?

22
00:02:00,400 --> 00:02:04,400
 I think I'm supposed to be watching Ted talks every day.

23
00:02:04,400 --> 00:02:13,400
 I haven't watched the Ted talk for a long time.

24
00:02:13,400 --> 00:02:22,150
 What else? Teemu, our Finnish meditator, just finished his

25
00:02:22,150 --> 00:02:26,400
 advanced course.

26
00:02:26,400 --> 00:02:30,400
 So congratulations to him. He's downstairs.

27
00:02:30,400 --> 00:02:33,480
 I asked if he wanted to come up and say hello. He said, "

28
00:02:33,480 --> 00:02:35,400
Can we do it another time?"

29
00:02:35,400 --> 00:02:43,400
 He's still going strong. He's been in intensive meditation

30
00:02:43,400 --> 00:02:46,400
 for the past 10 days

31
00:02:46,400 --> 00:02:53,400
 and isn't showing any signs of lagging.

32
00:02:53,400 --> 00:03:11,400
 [silence]

33
00:03:11,400 --> 00:03:17,400
 There's more. Wednesday, this Wednesday I think.

34
00:03:17,400 --> 00:03:22,490
 I've been invited to give a talk in Toronto, so I need a

35
00:03:22,490 --> 00:03:24,400
 ride to Toronto.

36
00:03:24,400 --> 00:03:28,400
 Anybody want to give me a ride from Hamilton to Toronto?

37
00:03:28,400 --> 00:03:33,640
 Wednesday afternoon. Why? Because I have a class and I

38
00:03:33,640 --> 00:03:35,400
 wouldn't have time to take the bus there.

39
00:03:35,400 --> 00:03:39,400
 I wouldn't make it on time. I have to get a ride.

40
00:03:39,400 --> 00:03:41,070
 That's the only way I could make it. I'll have to talk to

41
00:03:41,070 --> 00:03:41,400
 them.

42
00:03:41,400 --> 00:03:49,400
 Maybe they can come pick me up.

43
00:03:49,400 --> 00:03:52,980
 Just looking at TedX talks here, it says that they must be

44
00:03:52,980 --> 00:03:54,400
 under 18 minutes,

45
00:03:54,400 --> 00:04:01,400
 but some are as short as five minutes long.

46
00:04:01,400 --> 00:04:12,920
 Probably I have to cut it down. I'm already a full page of

47
00:04:12,920 --> 00:04:15,400
 writing.

48
00:04:15,400 --> 00:04:21,400
 Here's one of my paragraphs. I just wrote it up just now.

49
00:04:21,400 --> 00:04:24,400
 It's probably going to change.

50
00:04:24,400 --> 00:04:29,660
 I've spent the past 16 years practicing, studying and

51
00:04:29,660 --> 00:04:34,040
 teaching meditation as a Buddhist and eventually a Buddhist

52
00:04:34,040 --> 00:04:34,400
 monk,

53
00:04:34,400 --> 00:04:38,400
 following a monastic code that is over 2,500 years old.

54
00:04:38,400 --> 00:04:45,680
 I've lived in straw huts and even caves in the jungles of

55
00:04:45,680 --> 00:04:47,400
 Thailand and Sri Lanka,

56
00:04:47,400 --> 00:04:52,400
 and I haven't worn underwear in over 14 years.

57
00:04:52,400 --> 00:04:54,890
 At the same time, since I've become a monk, I've learned

58
00:04:54,890 --> 00:04:57,400
 how to program in several different computer languages,

59
00:04:57,400 --> 00:05:01,400
 become proficient in video editing and conversion,

60
00:05:01,400 --> 00:05:03,990
 and developed a website that holds live internet broadcasts

61
00:05:03,990 --> 00:05:04,400
 daily,

62
00:05:04,400 --> 00:05:09,400
 as well as an online community of several hundred members.

63
00:05:09,400 --> 00:05:13,400
 I'm just writing probably the tone has to change as well.

64
00:05:13,400 --> 00:05:17,400
 It has to be more lively probably, right?

65
00:05:17,400 --> 00:05:21,400
 It is these contrasts that interest me very much.

66
00:05:21,400 --> 00:05:24,100
 In my early years as a Buddhist monk, I remember sitting at

67
00:05:24,100 --> 00:05:29,400
 the feet of my 80-year-old meditation,

68
00:05:29,400 --> 00:05:35,990
 "Master or teacher?" I don't like the word "master" so much

69
00:05:35,990 --> 00:05:36,400
.

70
00:05:36,400 --> 00:05:40,810
 "Teacher listening to him impart the ancient wisdom of the

71
00:05:40,810 --> 00:05:42,400
 Buddha to Thai meditators

72
00:05:42,400 --> 00:05:45,160
 and think to myself that really these teachings are not

73
00:05:45,160 --> 00:05:47,400
 that hard to understand.

74
00:05:47,400 --> 00:05:50,840
 The reason I'm dealing with Buddhism and spirituality in

75
00:05:50,840 --> 00:05:53,630
 general is that what gets lost in translation and

76
00:05:53,630 --> 00:05:54,400
 adaptation

77
00:05:54,400 --> 00:05:58,400
 and the degradation inherent in word-of-mouth transmission

78
00:05:58,400 --> 00:06:00,400
 that winds up like a game of telephone

79
00:06:00,400 --> 00:06:05,120
 or the message that we the modern world get is something

80
00:06:05,120 --> 00:06:09,400
 very different from the original teachings."

81
00:06:09,400 --> 00:06:14,400
 Does it sound too dry?

82
00:06:14,400 --> 00:06:18,400
 No, I think it sounds good. It sounds like a good start.

83
00:06:18,400 --> 00:06:24,400
 Good. So you're my cheerleader. I need some encouragement.

84
00:06:24,400 --> 00:06:32,400
 Cheering squad.

85
00:06:32,400 --> 00:06:35,400
 So how long did that take to say?

86
00:06:35,400 --> 00:06:38,490
 That was probably, I didn't time it, but that was probably

87
00:06:38,490 --> 00:06:40,400
 like two minutes or a minute and a half.

88
00:06:40,400 --> 00:06:45,230
 Not very long. So I may want to shorten it a little bit

89
00:06:45,230 --> 00:06:46,400
 because this is all just introductory stuff.

90
00:06:46,400 --> 00:06:49,570
 And then I get on to, "It is this work, although I've

91
00:06:49,570 --> 00:06:51,400
 gotten into the..."

92
00:06:51,400 --> 00:06:56,350
 See, the idea is to contrast ancient teachings with modern

93
00:06:56,350 --> 00:06:57,400
 technology.

94
00:06:57,400 --> 00:07:02,400
 And actually, maybe ancient teachings in the modern world,

95
00:07:02,400 --> 00:07:03,400
 how do you bridge these two?

96
00:07:03,400 --> 00:07:09,600
 How do you bring modern teachings, ancient teachings to the

97
00:07:09,600 --> 00:07:11,400
 modern world?

98
00:07:11,400 --> 00:07:15,980
 Maybe that's not even the best subject, but it's going to

99
00:07:15,980 --> 00:07:17,400
 be a part of it.

100
00:07:17,400 --> 00:07:26,420
 I think the key is to strip it of that which makes it

101
00:07:26,420 --> 00:07:34,400
 ancient, which is culture, language.

102
00:07:34,400 --> 00:07:39,140
 The sheer access to the information that we have is

103
00:07:39,140 --> 00:07:42,400
 compared to people at the time.

104
00:07:42,400 --> 00:07:48,400
 We have such a broader, so much broader information.

105
00:07:48,400 --> 00:07:57,400
 Yeah. And yet, I mean, the danger is that we're much more

106
00:07:57,400 --> 00:07:58,400
...

107
00:07:58,400 --> 00:08:07,080
 We have much more immediate gratification of pleasure, and

108
00:08:07,080 --> 00:08:10,400
 gratification of desire.

109
00:08:10,400 --> 00:08:14,240
 You pick up your smartphone and you've got something

110
00:08:14,240 --> 00:08:16,400
 entertaining right away.

111
00:08:16,400 --> 00:08:19,400
 Well, I'm thinking, pick up your smartphone and you have

112
00:08:19,400 --> 00:08:22,400
 the 40 years of the teachings of the Buddha right away,

113
00:08:22,400 --> 00:08:26,950
 as compared to people at the time. Maybe they heard a few

114
00:08:26,950 --> 00:08:32,400
 lines or one sutta, but we have so much.

115
00:08:32,400 --> 00:08:37,970
 Yeah. But the point I was trying to make was in terms of

116
00:08:37,970 --> 00:08:40,400
 the quality of the mind,

117
00:08:40,400 --> 00:08:43,490
 because the teachings can be there and if there's no one

118
00:08:43,490 --> 00:08:45,400
 interested in reading them.

119
00:08:45,400 --> 00:08:49,300
 So, a person has to have a quality of mind to want to read

120
00:08:49,300 --> 00:08:51,400
 the teachings.

121
00:08:51,400 --> 00:08:55,920
 And so, I think a part of modern technology is pulling us

122
00:08:55,920 --> 00:08:59,400
 away from spirituality in general,

123
00:08:59,400 --> 00:09:02,700
 to make the Buddhist teaching less interesting. But there's

124
00:09:02,700 --> 00:09:03,400
 a whole other part of it.

125
00:09:03,400 --> 00:09:07,580
 I think you could argue that there's a significant part of

126
00:09:07,580 --> 00:09:10,400
 it that brings us closer.

127
00:09:10,400 --> 00:09:19,580
 Our breaking barriers and expanding our horizons, the focus

128
00:09:19,580 --> 00:09:22,400
 on truth and knowledge.

129
00:09:22,400 --> 00:09:29,400
 Truth is because the internet is very much about knowledge,

130
00:09:29,400 --> 00:09:30,400
 about science,

131
00:09:30,400 --> 00:09:35,400
 not very much, but much of it is.

132
00:09:35,400 --> 00:09:43,800
 Sharing knowledge, sharing in general, about making things

133
00:09:43,800 --> 00:09:44,400
 free.

134
00:09:44,400 --> 00:09:51,580
 We've sort of broken down the capitalist model of charging

135
00:09:51,580 --> 00:09:53,400
 for things.

136
00:09:53,400 --> 00:09:56,400
 I'm thinking your audio is breaking up a little bit.

137
00:09:56,400 --> 00:10:00,410
 Oh yeah. It'd either be my internet or it could be your

138
00:10:00,410 --> 00:10:01,400
 internet.

139
00:10:01,400 --> 00:10:02,400
 It could be.

140
00:10:02,400 --> 00:10:05,400
 Or it could be the internet.

141
00:10:05,400 --> 00:10:10,210
 And then there's this terrible technical aspect of the

142
00:10:10,210 --> 00:10:14,400
 internet. It's supposed to make things easier.

143
00:10:14,400 --> 00:10:31,400
 Is it still breaking up?

144
00:10:31,400 --> 00:10:36,400
 Oh, it sounds better now. Much more clear.

145
00:10:36,400 --> 00:10:39,400
 So do we have any questions?

146
00:10:39,400 --> 00:10:49,400
 We do. I think.

147
00:10:49,400 --> 00:10:52,560
 We have a long one here. That's not really a question

148
00:10:52,560 --> 00:10:53,400
 though.

149
00:10:53,400 --> 00:10:56,400
 I think we have to skip that one.

150
00:10:56,400 --> 00:11:02,940
 There is a long question that is asking about Mahayana Sut

151
00:11:02,940 --> 00:11:06,400
ta, the Diamond Sutta.

152
00:11:06,400 --> 00:11:08,400
 Yeah, not going to touch that.

153
00:11:08,400 --> 00:11:14,620
 Yeah. A concept of arbitrary ideas, which I think is from

154
00:11:14,620 --> 00:11:18,400
 the Sutta mentioned.

155
00:11:18,400 --> 00:11:21,400
 Looks like we have no questions.

156
00:11:21,400 --> 00:11:25,280
 What are you all doing here if you don't have any questions

157
00:11:25,280 --> 00:11:25,400
?

158
00:11:25,400 --> 00:11:28,400
 Anyone want to come on the Hangout and talk with us?

159
00:11:28,400 --> 00:11:30,400
 Do something different tonight?

160
00:11:30,400 --> 00:11:34,400
 Come on. It's a Friday night. Live a little.

161
00:11:34,400 --> 00:11:37,110
 I put the link out for the Hangout. You just click on that

162
00:11:37,110 --> 00:11:39,400
 link. Join us in the Hangout.

163
00:11:39,400 --> 00:11:43,400
 Come and say hello.

164
00:11:43,400 --> 00:11:47,690
 Bond wants to know if you can meet your teacher through

165
00:11:47,690 --> 00:11:49,400
 Google Hangouts.

166
00:11:49,400 --> 00:11:56,400
 Oh, maybe. A line would probably be better.

167
00:11:56,400 --> 00:11:59,400
 May I call him on the phone?

168
00:11:59,400 --> 00:12:04,530
 That's probably the easiest, is to just call him in his

169
00:12:04,530 --> 00:12:05,400
 room.

170
00:12:05,400 --> 00:12:08,400
 Does Ajahn Tong use much technology?

171
00:12:08,400 --> 00:12:13,660
 He uses telephone. Not much else. He has an iPad. I think

172
00:12:13,660 --> 00:12:14,400
 they got him an iPad.

173
00:12:14,400 --> 00:12:18,180
 They use that to show him photos and so on. His vision is

174
00:12:18,180 --> 00:12:19,400
 not that good.

175
00:12:19,400 --> 00:12:25,080
 He had surgery and it never really got better. His vision

176
00:12:25,080 --> 00:12:29,400
 is not really good at all.

177
00:12:29,400 --> 00:12:36,400
 He's 92 or something. Very tired.

178
00:12:36,400 --> 00:12:39,400
 Not all the time, but gets tired easier.

179
00:12:39,400 --> 00:12:46,400
 Sure.

180
00:12:46,400 --> 00:12:50,110
 Did you hear how he gave up the will to live when he was 80

181
00:12:50,110 --> 00:12:50,400
?

182
00:12:50,400 --> 00:12:51,400
 No.

183
00:12:51,400 --> 00:12:54,480
 You know how the Buddha gave up the will to live at 80? So

184
00:12:54,480 --> 00:12:57,780
 on his 80th birthday, he asked permission from everyone to

185
00:12:57,780 --> 00:12:58,400
 die.

186
00:12:58,400 --> 00:13:02,380
 The head monk for the northern region of Thailand came to

187
00:13:02,380 --> 00:13:03,400
 his birthday.

188
00:13:03,400 --> 00:13:08,710
 While he was sitting there, our teacher, Ajahn, gets up and

189
00:13:08,710 --> 00:13:11,400
 asks him permission to die.

190
00:13:11,400 --> 00:13:14,760
 He said, "I've been working really hard and doing a lot of

191
00:13:14,760 --> 00:13:18,400
 things, but maybe..."

192
00:13:18,400 --> 00:13:22,190
 He was so round about it. He took a half an hour really to

193
00:13:22,190 --> 00:13:23,400
 ask permission.

194
00:13:23,400 --> 00:13:29,100
 He said, "It's good to see you here again and maybe it's

195
00:13:29,100 --> 00:13:31,400
 for the last time."

196
00:13:31,400 --> 00:13:38,160
 "No, not that... Well, it's just that I've been working,

197
00:13:38,160 --> 00:13:39,400
 spent..."

198
00:13:39,400 --> 00:13:43,190
 He kept interrupting himself and starting over and trying

199
00:13:43,190 --> 00:13:44,400
 to just...

200
00:13:44,400 --> 00:13:51,300
 Then he said, "Maybe it might be that I just want to ask to

201
00:13:51,300 --> 00:13:52,400
 die."

202
00:13:52,400 --> 00:13:57,150
 So he wasn't sad? He was asking if he could go on vacation

203
00:13:57,150 --> 00:13:58,400
 or something?

204
00:13:58,400 --> 00:14:01,400
 Pretty much, yeah.

205
00:14:01,400 --> 00:14:02,400
 That sounds...

206
00:14:02,400 --> 00:14:05,600
 He was really trying to reassure the head monk that he wasn

207
00:14:05,600 --> 00:14:06,400
't...

208
00:14:06,400 --> 00:14:12,320
 "You know, it's not if I'm here, as long as I'm here, I'll

209
00:14:12,320 --> 00:14:13,400
 help."

210
00:14:13,400 --> 00:14:19,400
 And he said, "I've been boxing for many years."

211
00:14:19,400 --> 00:14:23,400
 And he said, "I think maybe it's just time..."

212
00:14:23,400 --> 00:14:27,010
 And he used a very northern Thai word that the head monk

213
00:14:27,010 --> 00:14:29,400
 didn't understand because he's from Bangkok.

214
00:14:29,400 --> 00:14:31,400
 That means "tired out."

215
00:14:31,400 --> 00:14:34,610
 And he said, "I think I'm just gonna... I think it's time

216
00:14:34,610 --> 00:14:36,400
 to shelve my gloves."

217
00:14:36,400 --> 00:14:39,400
 "Boxing gloves."

218
00:14:39,400 --> 00:14:42,400
 He said, "I want to focus on meditation."

219
00:14:42,400 --> 00:14:47,400
 And he said, "Like three times I'd like to just die."

220
00:14:47,400 --> 00:14:50,400
 And the head monk refused to let him and all the...

221
00:14:50,400 --> 00:14:53,610
 One lay woman who had just given him a very, very large

222
00:14:53,610 --> 00:14:56,400
 donation, a sizable donation,

223
00:14:56,400 --> 00:15:00,730
 and was giving him lots of donations, cried in front of the

224
00:15:00,730 --> 00:15:03,400
 whole congregation.

225
00:15:03,400 --> 00:15:10,400
 She was not a very spiritual person.

226
00:15:10,400 --> 00:15:15,400
 And then that night there was an earthquake in Jomtong.

227
00:15:15,400 --> 00:15:18,400
 And there's never an earthquake in Jomtong.

228
00:15:18,400 --> 00:15:20,400
 An earthquake zone as far as I know.

229
00:15:20,400 --> 00:15:22,830
 There's never other... no other time that I've heard of an

230
00:15:22,830 --> 00:15:26,400
 earthquake or felt an earthquake there.

231
00:15:26,400 --> 00:15:29,400
 But that night the earth shook.

232
00:15:29,400 --> 00:15:31,400
 So in the morning we were all like...

233
00:15:31,400 --> 00:15:34,050
 We got up early in the morning to go for chanting and we're

234
00:15:34,050 --> 00:15:37,400
 like waiting to see whether he shows up or not.

235
00:15:37,400 --> 00:15:40,400
 Because doesn't that happen when an enlightened being dies?

236
00:15:40,400 --> 00:15:41,400
 There's an earthquake or something?

237
00:15:41,400 --> 00:15:43,400
 Well, I don't know.

238
00:15:43,400 --> 00:15:46,580
 When the Buddha gave up the will to live there was an

239
00:15:46,580 --> 00:15:47,400
 earthquake.

240
00:15:47,400 --> 00:15:50,400
 I think when he passed away there was an earthquake.

241
00:15:50,400 --> 00:15:55,400
 So...

242
00:15:55,400 --> 00:15:59,400
 That's our legend that we're... well, that's true.

243
00:15:59,400 --> 00:16:03,400
 That's our magical story of our teacher.

244
00:16:03,400 --> 00:16:05,650
 Which 12 years later he's still hanging in there, so did he

245
00:16:05,650 --> 00:16:06,400
 regain the meaning?

246
00:16:06,400 --> 00:16:08,400
 I don't know that he's hanging in. I think it's just...

247
00:16:08,400 --> 00:16:18,400
 He said, "You know, no one knows when they're going to die.

248
00:16:18,400 --> 00:16:24,400
 You can't just say you're going to die."

249
00:16:24,400 --> 00:16:29,400
 So he's still here. But he changed and became far less...

250
00:16:29,400 --> 00:16:33,640
 Felt like he became a lot less interested and a lot less g

251
00:16:33,640 --> 00:16:36,400
ung-ho about teaching and so on.

252
00:16:36,400 --> 00:16:44,400
 He really sort of retreated inward after that.

253
00:16:44,400 --> 00:16:49,790
 I mean, he still teaches a lot less, but even today he's

254
00:16:49,790 --> 00:16:52,400
 probably still teaching.

255
00:16:52,400 --> 00:16:55,400
 Well, I can guarantee he's still teaching.

256
00:16:55,400 --> 00:17:05,400
 Unless he's too ill.

257
00:17:05,400 --> 00:17:09,400
 No, I mean, at some point I probably should go to see him.

258
00:17:09,400 --> 00:17:15,400
 I don't think there's any way around it.

259
00:17:15,400 --> 00:17:19,480
 If I don't, then yeah, that might be an alternative is to

260
00:17:19,480 --> 00:17:20,400
 call him.

261
00:17:20,400 --> 00:17:27,400
 But it's not the same.

262
00:17:27,400 --> 00:17:30,400
 And it's not that important.

263
00:17:30,400 --> 00:17:34,400
 If I don't get to go see him, I don't get to go see him.

264
00:17:34,400 --> 00:17:40,400
 If I have the chance, I probably should.

265
00:17:40,400 --> 00:17:44,400
 Have you gotten any more information on the conference?

266
00:17:44,400 --> 00:17:46,400
 He's sending it to me tomorrow.

267
00:17:46,400 --> 00:17:52,400
 He's calling me tonight.

268
00:17:52,400 --> 00:18:00,400
 And Sunday we're having this peace meeting group thing.

269
00:18:00,400 --> 00:18:02,400
 So many things going on.

270
00:18:02,400 --> 00:18:06,400
 I should probably go because I've got things to think about

271
00:18:06,400 --> 00:18:10,400
 and plan.

272
00:18:10,400 --> 00:18:14,400
 Are you all set for the event on Sunday?

273
00:18:14,400 --> 00:18:16,400
 I don't know.

274
00:18:16,400 --> 00:18:19,400
 I'll sort of maybe know tomorrow.

275
00:18:19,400 --> 00:18:26,400
 Were you going to go to the volunteer meeting before or no?

276
00:18:26,400 --> 00:18:28,400
 Right.

277
00:18:28,400 --> 00:18:30,400
 Oh, probably.

278
00:18:30,400 --> 00:18:33,400
 I don't have to go to it. I just turn on my phone.

279
00:18:33,400 --> 00:18:39,470
 Yeah, I'm sure we'll have everything set up for that before

280
00:18:39,470 --> 00:18:40,400
 that.

281
00:18:40,400 --> 00:18:50,400
 I'll probably make both meetings. That's not a problem.

282
00:18:50,400 --> 00:18:52,400
 So yeah, volunteer meeting.

283
00:18:52,400 --> 00:18:56,400
 If anyone wants to join, we need more volunteers.

284
00:18:56,400 --> 00:19:00,400
 People to help us figure stuff out.

285
00:19:00,400 --> 00:19:02,400
 Right? Do we?

286
00:19:02,400 --> 00:19:04,400
 We do.

287
00:19:04,400 --> 00:19:08,400
 So it's Sunday at 1pm Eastern time.

288
00:19:08,400 --> 00:19:11,400
 And we meet on Google Hangouts.

289
00:19:11,400 --> 00:19:14,890
 And if you just let me know who you are, if you'd like to

290
00:19:14,890 --> 00:19:18,400
 be in the Hangout, it's broadcasted online.

291
00:19:18,400 --> 00:19:21,400
 Not a lot of people watch the volunteer meeting.

292
00:19:21,400 --> 00:19:28,400
 But it's just a good way of us to get together, talk to B

293
00:19:28,400 --> 00:19:28,400
ante and find out what type of things are needed each week

294
00:19:28,400 --> 00:19:28,400
 for the monastery.

295
00:19:28,400 --> 00:19:37,400
 So if anyone would like to join, just let me know.

296
00:19:37,400 --> 00:19:40,400
 I think that's it. No questions?

297
00:19:40,400 --> 00:19:42,400
 No questions.

298
00:19:42,400 --> 00:19:47,400
 We've taught everyone everything.

299
00:19:47,400 --> 00:19:51,400
 Okay, thank you all. Thanks, Robin. Have a good night.

300
00:19:51,400 --> 00:19:53,400
 Have a good evening.

