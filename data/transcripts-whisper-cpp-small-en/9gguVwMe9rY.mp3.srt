1
00:00:00,000 --> 00:00:05,500
 Worshiping the Buddha statue. I'm doing meditation since 21

2
00:00:05,500 --> 00:00:07,920
, since age 21. After

3
00:00:07,920 --> 00:00:13,240
 taking meditation I am I am doing less worship and I feel

4
00:00:13,240 --> 00:00:14,280
 there's no point in

5
00:00:14,280 --> 00:00:18,810
 worship since Buddha teach me taught me only we can help

6
00:00:18,810 --> 00:00:20,440
 ourselves. I just want

7
00:00:20,440 --> 00:00:27,030
 to know if it's okay I don't worship. Well the Buddha said

8
00:00:27,030 --> 00:00:28,640
 the best sort of

9
00:00:28,640 --> 00:00:33,980
 worship is practice. He didn't use the word worship

10
00:00:33,980 --> 00:00:35,080
 although you might translate

11
00:00:35,080 --> 00:00:37,920
 it that way. Yeah I mean you could if you look at his

12
00:00:37,920 --> 00:00:40,360
 wording because when the

13
00:00:40,360 --> 00:00:48,180
 Buddha was getting ready to pass into final liberation all

14
00:00:48,180 --> 00:00:49,720
 the angels from all

15
00:00:49,720 --> 00:00:53,850
 around the universe came and all of the people came and

16
00:00:53,850 --> 00:00:55,560
 offered flowers and

17
00:00:55,560 --> 00:01:00,320
 candles and incense to the body to worship the Buddha's

18
00:01:00,320 --> 00:01:02,280
 dying body and the

19
00:01:02,280 --> 00:01:07,010
 Buddha he wasn't dead yet he said to Anna he brought Anna

20
00:01:07,010 --> 00:01:07,880
 over and he said to

21
00:01:07,880 --> 00:01:14,290
 Anna you see they're worshiping in this way but he said

22
00:01:14,290 --> 00:01:15,520
 this isn't how you

23
00:01:15,520 --> 00:01:20,450
 truly worship or pay respect to someone who is fully

24
00:01:20,450 --> 00:01:22,600
 enlightened. He said the

25
00:01:22,600 --> 00:01:27,220
 proper way to pay respect to someone is through the

26
00:01:27,220 --> 00:01:28,440
 practice.

27
00:01:28,440 --> 00:01:57,280
 So the word puja which you could say means worship the

28
00:01:57,280 --> 00:01:58,280
 highest form of

29
00:01:58,280 --> 00:02:01,740
 worship is through practicing the Dhamma in line with or

30
00:02:01,740 --> 00:02:03,200
 for realizing the Dhamma

31
00:02:03,200 --> 00:02:10,320
 for oneself. So in that light I would say it's not not

32
00:02:10,320 --> 00:02:12,440
 really proper to say we

33
00:02:12,440 --> 00:02:15,340
 shouldn't worship or to say that the Buddha would have us

34
00:02:15,340 --> 00:02:17,000
 not worship. The

35
00:02:17,000 --> 00:02:23,690
 Buddha said puja japuja niya nang eta mang gala muta mang.

36
00:02:23,690 --> 00:02:25,480
 This is a great blessing

37
00:02:25,480 --> 00:02:29,240
 worshiping those worthy of worship or paying homage we

38
00:02:29,240 --> 00:02:30,120
 often because we don't

39
00:02:30,120 --> 00:02:34,920
 want to try to tone it down people don't like the word

40
00:02:34,920 --> 00:02:35,240
 worship.

41
00:02:35,240 --> 00:02:37,870
 Actually worship is a wonderful word actually it's just

42
00:02:37,870 --> 00:02:39,960
 become corrupted by or

43
00:02:39,960 --> 00:02:44,200
 it's been taken hostage by theistic religions. Worship

44
00:02:44,200 --> 00:02:45,640
 means worthship

45
00:02:45,640 --> 00:02:51,170
 holding someone to be worthy of homage or have to have some

46
00:02:51,170 --> 00:02:52,400
 worth to put it

47
00:02:52,400 --> 00:02:56,280
 simply it's actually not such a God based theistic based

48
00:02:56,280 --> 00:02:58,240
 word worthship that has

49
00:02:58,240 --> 00:03:03,920
 to do with worth. So it's expressing your feeling of

50
00:03:03,920 --> 00:03:05,120
 something having a

51
00:03:05,120 --> 00:03:09,520
 veneration of something having great worth and great value

52
00:03:09,520 --> 00:03:10,760
 and great merit.

53
00:03:10,760 --> 00:03:18,200
 So because we do consider the Buddha to have be something

54
00:03:18,200 --> 00:03:19,120
 that has great worth

55
00:03:19,120 --> 00:03:27,610
 and we do hold the Buddha up very high so we develop when

56
00:03:27,610 --> 00:03:28,360
 we do our when we do

57
00:03:28,360 --> 00:03:32,660
 practice worship we develop states of humility and states

58
00:03:32,660 --> 00:03:34,240
 of gratitude

59
00:03:34,240 --> 00:03:37,690
 towards the Buddha which are wholesome mind states which

60
00:03:37,690 --> 00:03:38,520
 create peace in the

61
00:03:38,520 --> 00:03:43,690
 mind create confidence in the mind create contentment in

62
00:03:43,690 --> 00:03:45,040
 the mind. When we

63
00:03:45,040 --> 00:03:49,200
 give ourselves up to the Buddha there's one monkey said

64
00:03:49,200 --> 00:03:50,000
 that would damage you

65
00:03:50,000 --> 00:03:55,370
 a hundred he said when you give yourself up to the Buddha

66
00:03:55,370 --> 00:03:56,520
 then when pain comes

67
00:03:56,520 --> 00:03:58,600
 you don't have to worry about it you can say it's not my

68
00:03:58,600 --> 00:03:59,360
 body I give it to the

69
00:03:59,360 --> 00:04:08,200
 Buddha already but but the the act of paying homage is a an

70
00:04:08,200 --> 00:04:09,320
 act of gratitude

71
00:04:09,320 --> 00:04:15,990
 and and and veneration it's not an unwholesome thing. What

72
00:04:15,990 --> 00:04:16,440
 we see as

73
00:04:16,440 --> 00:04:18,890
 unwholesome we see other people doing it and thinking that

74
00:04:18,890 --> 00:04:19,880
 that's practice that

75
00:04:19,880 --> 00:04:26,120
 that's of some ultimate benefit some end in and of itself

76
00:04:26,120 --> 00:04:27,320
 having pain having

77
00:04:27,320 --> 00:04:30,630
 worshipped something high without really knowing much about

78
00:04:30,630 --> 00:04:32,040
 it this is has some

79
00:04:32,040 --> 00:04:38,860
 benefit and and you know in in a conventional sense it does

80
00:04:38,860 --> 00:04:40,120
 it makes one

81
00:04:40,120 --> 00:04:44,690
 happy it makes one confident and can theoretically lead a

82
00:04:44,690 --> 00:04:45,720
 person to be born in

83
00:04:45,720 --> 00:04:49,070
 in heaven for example so this is we might even say that the

84
00:04:49,070 --> 00:04:50,200
istic religions

85
00:04:50,200 --> 00:04:53,240
 have some merit to them in the sense that they do teach

86
00:04:53,240 --> 00:04:54,400
 people theoretically

87
00:04:54,400 --> 00:04:59,360
 how to go to heaven through this practice of prayer and

88
00:04:59,360 --> 00:05:00,000
 worship and

89
00:05:00,000 --> 00:05:04,470
 developing mind states of rapture and so on and of course

90
00:05:04,470 --> 00:05:05,640
 the negative side is

91
00:05:05,640 --> 00:05:08,090
 they have these wrong views and the ideas that heaven is

92
00:05:08,090 --> 00:05:09,040
 permanent but even

93
00:05:09,040 --> 00:05:11,640
 beings in heaven have wrong views we've I talked about this

94
00:05:11,640 --> 00:05:12,800
 with Bantenoma once

95
00:05:12,800 --> 00:05:16,050
 we were wondering he was saying no no Christian people can

96
00:05:16,050 --> 00:05:16,880
't go to heaven

97
00:05:16,880 --> 00:05:19,460
 because they have wrong view and I said well you know if

98
00:05:19,460 --> 00:05:20,200
 you think about it

99
00:05:20,200 --> 00:05:24,770
 devas many devas have wrong view the angels have wrong view

100
00:05:24,770 --> 00:05:25,800
 so going to

101
00:05:25,800 --> 00:05:29,140
 heaven it's not it's difficult but it's not difficult in

102
00:05:29,140 --> 00:05:30,400
 the sense that Buddhist

103
00:05:30,400 --> 00:05:33,550
 meditation is difficult or insight is difficult it's

104
00:05:33,550 --> 00:05:34,280
 difficult in the sense

105
00:05:34,280 --> 00:05:36,800
 you have to work hard and you have to develop wholesome

106
00:05:36,800 --> 00:05:38,800
 states so in a

107
00:05:38,800 --> 00:05:43,500
 conventional sense paying you know worshiping something has

108
00:05:43,500 --> 00:05:44,540
 has benefits it

109
00:05:44,540 --> 00:05:50,320
 brings power to the mind it's a objectively beneficial mind

110
00:05:50,320 --> 00:05:51,240
 state the

111
00:05:51,240 --> 00:05:55,150
 negative side is the blind faith and the the belief that by

112
00:05:55,150 --> 00:05:56,040
 doing this you're

113
00:05:56,040 --> 00:05:59,030
 going to somehow go to heaven if you worship a God or you

114
00:05:59,030 --> 00:06:00,120
 worship an idol or

115
00:06:00,120 --> 00:06:03,390
 so on and people worship trees and in India they worship ph

116
00:06:03,390 --> 00:06:04,420
allic symbols and

117
00:06:04,420 --> 00:06:08,180
 so on the idea that somehow it's going to bring benefit

118
00:06:08,180 --> 00:06:09,160
 this is wrong view and

119
00:06:09,160 --> 00:06:11,980
 they can lead you to be born as an angel but you still have

120
00:06:11,980 --> 00:06:13,320
 wrong view for

121
00:06:13,320 --> 00:06:16,100
 example so from a Buddhist point of view we don't look at

122
00:06:16,100 --> 00:06:17,160
 that we don't we don't

123
00:06:17,160 --> 00:06:21,640
 pay much attention to it we do customarily pay homage to

124
00:06:21,640 --> 00:06:22,840
 the Buddha you

125
00:06:22,840 --> 00:06:25,710
 know even Mahakasipo and the Buddha was had passed away Mah

126
00:06:25,710 --> 00:06:27,240
akasipo came and paid

127
00:06:27,240 --> 00:06:30,920
 homage to his feet we do do this worship you know he came

128
00:06:30,920 --> 00:06:31,440
 and you could say

129
00:06:31,440 --> 00:06:34,580
 worship the Buddha expressed his feeling that the Buddha

130
00:06:34,580 --> 00:06:36,480
 had some worth and and

131
00:06:36,480 --> 00:06:38,980
 the idea that the Buddha was his teacher but he didn't do

132
00:06:38,980 --> 00:06:40,000
 it thinking that it was

133
00:06:40,000 --> 00:06:42,760
 going to bring some benefit he just did it out of respect

134
00:06:42,760 --> 00:06:45,240
 and a feeling that it

135
00:06:45,240 --> 00:06:48,720
 was the proper thing to do out of respecting gratitude for

136
00:06:48,720 --> 00:06:50,080
 his teacher and

137
00:06:50,080 --> 00:06:53,930
 for the Buddha who was a very special being but we don't

138
00:06:53,930 --> 00:06:55,280
 consider it to be our

139
00:06:55,280 --> 00:06:57,790
 practice and this is why the Buddha was very careful before

140
00:06:57,790 --> 00:06:58,480
 he passed away to

141
00:06:58,480 --> 00:07:01,890
 say this is not the highest form of homage the highest form

142
00:07:01,890 --> 00:07:02,720
 of homage is the

143
00:07:02,720 --> 00:07:06,510
 practice of the Buddha's teaching and so you you shouldn't

144
00:07:06,510 --> 00:07:07,800
 hold such a hard line

145
00:07:07,800 --> 00:07:10,280
 I wouldn't recommend holding such a hard line stance as

146
00:07:10,280 --> 00:07:12,040
 worship is useless and I

147
00:07:12,040 --> 00:07:15,600
 shouldn't engage in it I would harmonize the two views by

148
00:07:15,600 --> 00:07:17,200
 saying that this is the

149
00:07:17,200 --> 00:07:20,140
 highest form of homage the highest form of worship that I'm

150
00:07:20,140 --> 00:07:21,720
 doing already because

151
00:07:21,720 --> 00:07:25,000
 the Buddha himself said that and so it keeps the mind from

152
00:07:25,000 --> 00:07:26,360
 getting overly

153
00:07:26,360 --> 00:07:30,410
 critical and skeptical and antagonistic because people who

154
00:07:30,410 --> 00:07:31,360
 criticize things and

155
00:07:31,360 --> 00:07:34,140
 say this is no good and that's no good as as many people do

156
00:07:34,140 --> 00:07:35,400
 Buddhists will often

157
00:07:35,400 --> 00:07:38,560
 do saying all these people worshiping is they're just have

158
00:07:38,560 --> 00:07:39,680
 all this blind faith

159
00:07:39,680 --> 00:07:44,420
 and people who are anti theists talking about how silly the

160
00:07:44,420 --> 00:07:45,400
istic religions are

161
00:07:45,400 --> 00:07:48,160
 and so this is an antagonistic a negative mind state the

162
00:07:48,160 --> 00:07:49,360
 person who engages in

163
00:07:49,360 --> 00:07:52,660
 this will actually have a lot of unwholesomeness so you if

164
00:07:52,660 --> 00:07:53,040
 you look at

165
00:07:53,040 --> 00:07:58,320
 many atheists prominent atheists they actually have these

166
00:07:58,320 --> 00:07:59,760
 negative mind states

167
00:07:59,760 --> 00:08:02,400
 you can see they're and they can some some of them are

168
00:08:02,400 --> 00:08:03,640
 actually angry people

169
00:08:03,640 --> 00:08:07,090
 and and unwholesome have great unwholesomeness in their

170
00:08:07,090 --> 00:08:07,560
 mind because

171
00:08:07,560 --> 00:08:10,970
 they're just all about denying things you know and of

172
00:08:10,970 --> 00:08:12,560
 course the fact that they

173
00:08:12,560 --> 00:08:16,290
 deny the existence of the mind but that's a whole other

174
00:08:16,290 --> 00:08:18,480
 issue what I mean to

175
00:08:18,480 --> 00:08:22,190
 say is that theistic people can actually be really nice

176
00:08:22,190 --> 00:08:23,640
 people and makes you

177
00:08:23,640 --> 00:08:26,460
 think that in some senses we have a lot more in common with

178
00:08:26,460 --> 00:08:27,320
 them and they can

179
00:08:27,320 --> 00:08:35,220
 actually be more inclined towards meditation practice what

180
00:08:35,220 --> 00:08:36,040
 stops them of

181
00:08:36,040 --> 00:08:39,300
 course is the wrong view so the wrong view that only this

182
00:08:39,300 --> 00:08:40,280
 will lead you to

183
00:08:40,280 --> 00:08:44,190
 only Jesus will lead you to heaven only only Mohammed or

184
00:08:44,190 --> 00:08:46,280
 only Allah or so on

185
00:08:46,280 --> 00:08:51,400
 these kind of wrong views are what generally stop them from

186
00:08:51,400 --> 00:08:52,200
 coming to

187
00:08:52,200 --> 00:08:55,960
 practice meditation not the worshiping the worshiping and

188
00:08:55,960 --> 00:08:56,880
 the prayer and so on

189
00:08:56,880 --> 00:09:00,290
 this it actually creates benefit creates wholesome mind

190
00:09:00,290 --> 00:09:02,240
 states it's the the views

191
00:09:02,240 --> 00:09:06,000
 associated with it that are the problem

