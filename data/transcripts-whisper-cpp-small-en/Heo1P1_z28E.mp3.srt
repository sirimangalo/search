1
00:00:00,000 --> 00:00:05,100
 Does the realization of impermanence of phenomena itself

2
00:00:05,100 --> 00:00:06,000
 constitute nirvana?

3
00:00:06,000 --> 00:00:10,000
 Or is nirvana to be considered as apart from phenomena?

4
00:00:10,000 --> 00:00:15,000
 Yes, nirvana is khanda-vimuti.

5
00:00:15,000 --> 00:00:20,080
 It's emancipated or liberated or outside of the khandas,

6
00:00:20,080 --> 00:00:21,000
 outside of the aggregates.

7
00:00:21,000 --> 00:00:25,290
 So the realization of impermanence has to occur based on

8
00:00:25,290 --> 00:00:28,000
 something that's impermanent.

9
00:00:28,000 --> 00:00:33,360
 If the realization of impermanence were nirvana, then nir

10
00:00:33,360 --> 00:00:38,000
vana would be that which is impermanent.

11
00:00:38,000 --> 00:00:43,900
 It would be the association or the observation of something

12
00:00:43,900 --> 00:00:49,000
 that is impermanent, which is the first one.

13
00:00:49,000 --> 00:00:56,340
 The realization of nirvana is the observation of something

14
00:00:56,340 --> 00:01:06,000
 that is permanent, it's satisfying, and not self, but

15
00:01:06,000 --> 00:01:08,000
 essential.

16
00:01:08,000 --> 00:01:13,000
 So they're different.

17
00:01:13,000 --> 00:01:16,110
 You have to understand that what we mean by realization in

18
00:01:16,110 --> 00:01:19,270
 Buddhism is the moment where you actually see something as

19
00:01:19,270 --> 00:01:20,000
 being so.

20
00:01:20,000 --> 00:01:23,150
 It's not a state afterwards where you somehow say, "Wow,

21
00:01:23,150 --> 00:01:25,000
 everything is impermanent."

22
00:01:25,000 --> 00:01:28,440
 The realization occurs based on an object that is imper

23
00:01:28,440 --> 00:01:30,000
manent, so nirvana.

24
00:01:30,000 --> 00:01:33,280
 Nirvana is the next moment. If you really see something as

25
00:01:33,280 --> 00:01:35,640
 impermanent, like for example, let's bring it back to

26
00:01:35,640 --> 00:01:36,000
 practical,

27
00:01:36,000 --> 00:01:42,010
 when you're watching the rising and falling of the stomach.

28
00:01:42,010 --> 00:01:47,700
 There can come a point, the peak of the Vipassana

29
00:01:47,700 --> 00:01:49,000
 meditation,

30
00:01:49,000 --> 00:01:53,130
 where you're watching the stomach rising and falling, and

31
00:01:53,130 --> 00:01:54,000
 suddenly it changes.

32
00:01:54,000 --> 00:01:58,920
 It goes quickly. You're watching it, and suddenly it speeds

33
00:01:58,920 --> 00:02:01,000
 up, or it changes radically.

34
00:02:01,000 --> 00:02:06,140
 That moment, because the meditator's mind is so sharp, that

35
00:02:06,140 --> 00:02:07,000
 moment leads to nirvana.

36
00:02:07,000 --> 00:02:10,770
 The next moment there's a cessation experience. It can also

37
00:02:10,770 --> 00:02:14,000
 occur with suffering, so watching the rising and falling,

38
00:02:14,000 --> 00:02:17,920
 and suddenly it becomes unbearable. It's stuck, it's

39
00:02:17,920 --> 00:02:19,000
 uncomfortable.

40
00:02:19,000 --> 00:02:22,540
 Or there's pain, and pain throughout the body to the extent

41
00:02:22,540 --> 00:02:25,000
 that the mind is no longer interested,

42
00:02:25,000 --> 00:02:32,210
 is no longer attached to the body, or to the sankhara, and

43
00:02:32,210 --> 00:02:33,000
 it lets go.

44
00:02:33,000 --> 00:02:37,300
 The next moment there's an experience of nirvana. It can

45
00:02:37,300 --> 00:02:39,000
 also happen through non-self,

46
00:02:39,000 --> 00:02:41,530
 so for example, where you're watching the rising and

47
00:02:41,530 --> 00:02:44,000
 falling, and suddenly the rising and falling appears to be

48
00:02:44,000 --> 00:02:45,000
 going by itself.

49
00:02:45,000 --> 00:02:50,180
 It very clearly is not going according to our control, and

50
00:02:50,180 --> 00:02:54,520
 there's this feeling of that, and the next moment is nir

51
00:02:54,520 --> 00:02:55,000
vana,

52
00:02:55,000 --> 00:02:59,840
 it's a cessation experience. So it's the moment after the

53
00:02:59,840 --> 00:03:01,000
 realization.

54
00:03:01,000 --> 00:03:04,920
 The realization itself, according to the texts, is called

55
00:03:04,920 --> 00:03:08,000
 anulomanyan, the knowledge of conformity.

56
00:03:08,000 --> 00:03:11,000
 It's two or three moments, or one moment, I can't remember,

57
00:03:11,000 --> 00:03:16,500
 it's a very short period of time where the meditator sees

58
00:03:16,500 --> 00:03:22,000
 impermanence, or suffering or non-self, one of the three.

