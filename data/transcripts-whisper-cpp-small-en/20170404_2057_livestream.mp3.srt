1
00:00:00,000 --> 00:00:28,960
 Okay, good evening everyone.

2
00:00:28,960 --> 00:00:38,960
 Welcome back to our live broadcast, evening Dharma session.

3
00:00:38,960 --> 00:00:44,830
 So continuing on from last night, I thought we'd move on to

4
00:00:44,830 --> 00:00:55,960
 the Buddha's second discourse.

5
00:00:55,960 --> 00:01:02,560
 When learning about the Buddha, it's quite common for the

6
00:01:02,560 --> 00:01:05,960
 story to end at his enlightenment,

7
00:01:05,960 --> 00:01:14,280
 sometimes maybe end at his first discourse, but it's far

8
00:01:14,280 --> 00:01:17,510
 more interesting what happened after his enlightenment and

9
00:01:17,510 --> 00:01:21,960
 after his first discourse.

10
00:01:21,960 --> 00:01:33,450
 When he first taught, the five ascetics were impressed, but

11
00:01:33,450 --> 00:01:35,960
 only one of them became a sotapana.

12
00:01:35,960 --> 00:01:40,430
 Only one of them really understood what the Buddha was

13
00:01:40,430 --> 00:01:41,960
 talking about.

14
00:01:41,960 --> 00:01:46,690
 The others may have had some idea of how to put it into

15
00:01:46,690 --> 00:01:54,120
 practice, but they weren't able to put it into practice and

16
00:01:54,120 --> 00:01:58,310
 understand it and realize the teaching for themselves while

17
00:01:58,310 --> 00:01:59,960
 the Buddha was teaching.

18
00:01:59,960 --> 00:02:05,360
 So the Buddha had to stay with them, and he stayed with

19
00:02:05,360 --> 00:02:11,160
 them for five days, working with them individually,

20
00:02:11,160 --> 00:02:17,960
 addressing the individual challenges.

21
00:02:19,960 --> 00:02:21,960
 So,

22
00:02:23,960 --> 00:02:25,960
 So,

23
00:02:25,960 --> 00:02:51,960
 So,

24
00:02:51,960 --> 00:03:06,510
 And helping through their meditation until all five of them

25
00:03:06,510 --> 00:03:09,960
 became a sotapana.

26
00:03:09,960 --> 00:03:21,960
 At which point the Buddha taught the second discourse. So

27
00:03:21,960 --> 00:03:21,960
 here was the same audience, but now they were in a position

28
00:03:21,960 --> 00:03:21,960
 to really understand the Buddha's teaching on a deeper

29
00:03:21,960 --> 00:03:21,960
 level.

30
00:03:21,960 --> 00:03:27,830
 And so something we have something that this tells us is

31
00:03:27,830 --> 00:03:31,960
 that this teaching is not really a beginner teaching.

32
00:03:31,960 --> 00:03:36,320
 If you really want to understand the second discourse of

33
00:03:36,320 --> 00:03:40,680
 the Buddha, you really have to have done some intensive

34
00:03:40,680 --> 00:03:41,960
 meditation.

35
00:03:41,960 --> 00:04:04,580
 I've taken the time to really get a clear understanding of

36
00:04:04,580 --> 00:04:07,960
 reality.

37
00:04:07,960 --> 00:04:13,190
 So for those of you who are here practicing intensively, it

38
00:04:13,190 --> 00:04:15,960
'll be quite useful, I think.

39
00:04:15,960 --> 00:04:18,400
 For those of you at home, well, for some of you, you've

40
00:04:18,400 --> 00:04:21,650
 been practicing for some time. For those of you who may be

41
00:04:21,650 --> 00:04:26,330
 new to the teaching, I'll try to give a sort of a more

42
00:04:26,330 --> 00:04:29,960
 basic and easy to understand introductory teaching.

43
00:04:29,960 --> 00:04:37,290
 So I'll use this. It's still a good basis for understanding

44
00:04:37,290 --> 00:04:41,960
 the Buddha's idea of meditation.

45
00:04:41,960 --> 00:04:49,670
 So the second discourse is about the five aggregates. For

46
00:04:49,670 --> 00:04:57,250
 many of you, I'm sure, are familiar with this idea, this

47
00:04:57,250 --> 00:05:02,960
 set of concepts that the Buddha laid out.

48
00:05:02,960 --> 00:05:08,700
 And so the idea behind this suta is not to address the

49
00:05:08,700 --> 00:05:15,830
 question of whether there is a self or there isn't a self,

50
00:05:15,830 --> 00:05:19,960
 because that question is not useful.

51
00:05:19,960 --> 00:05:29,100
 It's not actually an answerable question. It's just like

52
00:05:29,100 --> 00:05:35,390
 asking, "Is there a God or isn't there a God?" It's perhaps

53
00:05:35,390 --> 00:05:43,260
 even worse because with God, ideally, theoretically, you

54
00:05:43,260 --> 00:05:45,960
 could have some contact with God.

55
00:05:45,960 --> 00:05:54,700
 And God might speak to you or do something, but the self

56
00:05:54,700 --> 00:05:58,960
 really is meaningless.

57
00:05:58,960 --> 00:06:05,380
 But if I were to tell you that there is a self, what would

58
00:06:05,380 --> 00:06:09,760
 that mean? Right? Is there any way for you to verify

59
00:06:09,760 --> 00:06:11,960
 whether there is a self or not?

60
00:06:11,960 --> 00:06:15,320
 It's always just going to be your belief. It's always just

61
00:06:15,320 --> 00:06:16,960
 going to be your belief.

62
00:06:16,960 --> 00:06:20,480
 If I tell you there isn't a self, what are you going to do

63
00:06:20,480 --> 00:06:22,960
 with that either? Right?

64
00:06:22,960 --> 00:06:26,650
 Is there a way for you to understand that there is no self,

65
00:06:26,650 --> 00:06:30,460
 to verify that there is no self? Just because you haven't

66
00:06:30,460 --> 00:06:33,960
 seen one, just because you haven't found one, right?

67
00:06:33,960 --> 00:06:38,550
 It can never be something... It's not a question that's am

68
00:06:38,550 --> 00:06:41,960
enable to understanding and verification.

69
00:06:41,960 --> 00:06:47,540
 The self is not a falsifiable claim, so it's not a

70
00:06:47,540 --> 00:06:56,960
 scientific claim, meaning you can't verify or deny it.

71
00:06:56,960 --> 00:07:03,370
 So we don't ask that question. It's simply not a useful

72
00:07:03,370 --> 00:07:04,960
 question.

73
00:07:04,960 --> 00:07:11,260
 Instead, we look at what is there, and we try to understand

74
00:07:11,260 --> 00:07:11,960
 it.

75
00:07:11,960 --> 00:07:16,370
 And one of the very important ways of understanding the

76
00:07:16,370 --> 00:07:20,350
 things that do exist, that we can experience, that we can

77
00:07:20,350 --> 00:07:23,960
 verify as coming into being,

78
00:07:23,960 --> 00:07:27,570
 is whether they are self, whether there's any reason to

79
00:07:27,570 --> 00:07:34,160
 think of them as self, because whether there is a self or

80
00:07:34,160 --> 00:07:35,960
 not isn't important.

81
00:07:35,960 --> 00:07:45,160
 But we do cling to many, many things as me, mine, under my

82
00:07:45,160 --> 00:07:49,790
 control, without even thinking about them, "Hey, this is me

83
00:07:49,790 --> 00:07:51,960
, this is mine."

84
00:07:51,960 --> 00:07:57,640
 We use them, we interact with them in a way that makes them

85
00:07:57,640 --> 00:08:04,960
, in a way that that implies, or there's an implicit belief

86
00:08:04,960 --> 00:08:08,960
 or conception of them as self.

87
00:08:08,960 --> 00:08:13,020
 And so the first thing the Buddha does is make this jump

88
00:08:13,020 --> 00:08:17,200
 from me and mine and the self and the being to our

89
00:08:17,200 --> 00:08:17,960
 experiences.

90
00:08:17,960 --> 00:08:20,560
 I mean, he's dealing with people who have begun to

91
00:08:20,560 --> 00:08:23,960
 understand experience, so they're familiar with this.

92
00:08:23,960 --> 00:08:29,870
 When he asks form, when he says about form, "Rupa," he says

93
00:08:29,870 --> 00:08:32,960
, "Form is not self."

94
00:08:32,960 --> 00:08:35,800
 And these monks have been meditating on form, they've been

95
00:08:35,800 --> 00:08:38,460
 meditating on the movements of their bodies and the

96
00:08:38,460 --> 00:08:41,960
 movements of the body during the breath and so on.

97
00:08:41,960 --> 00:08:47,590
 They've been meditating on the heat and the cold. All of

98
00:08:47,590 --> 00:09:01,960
 this is the physical, the form aspect of experience.

99
00:09:01,960 --> 00:09:09,210
 And we take form to be self, right? Instead of experiencing

100
00:09:09,210 --> 00:09:15,630
 it as moments of heat and cold and hardness and softness

101
00:09:15,630 --> 00:09:16,960
 and so on,

102
00:09:16,960 --> 00:09:23,960
 we experience it as our body. This is a body, my body. Yes.

103
00:09:23,960 --> 00:09:28,480
 All of these experiences when they come together, this is

104
00:09:28,480 --> 00:09:33,960
 me, this is mine, this I am. It's where this conception.

105
00:09:33,960 --> 00:09:37,110
 When we look at the body, when we look at what's really

106
00:09:37,110 --> 00:09:41,090
 there, we see experiences that arise and cease and these

107
00:09:41,090 --> 00:09:45,960
 experiences are not me or not mine.

108
00:09:45,960 --> 00:09:50,720
 How do we know this? Because they're not amenable to our

109
00:09:50,720 --> 00:09:53,960
 control. They cause affliction.

110
00:09:53,960 --> 00:09:58,640
 If something were truly me and mine, then we should be able

111
00:09:58,640 --> 00:10:02,960
 to say, "Hey, let it be this, let it not be this."

112
00:10:02,960 --> 00:10:07,290
 We should be able to say, "I don't want to be cold, I'm not

113
00:10:07,290 --> 00:10:08,960
 going to be cold."

114
00:10:08,960 --> 00:10:14,490
 We should be able to say, "I don't want to be short, I want

115
00:10:14,490 --> 00:10:19,960
 to be tall, I don't want to be fat, I want to be thin."

116
00:10:19,960 --> 00:10:24,960
 Which is probably really, I don't want to get into it.

117
00:10:24,960 --> 00:10:30,330
 I mean, it's a problem with attachment to the body and

118
00:10:30,330 --> 00:10:33,240
 attachment to our body because it's not something that

119
00:10:33,240 --> 00:10:33,960
 actually exists.

120
00:10:33,960 --> 00:10:41,960
 And we get really hung up on it in so many ways.

121
00:10:41,960 --> 00:10:44,960
 The second one is feelings.

122
00:10:44,960 --> 00:10:46,960
 We don't know.

