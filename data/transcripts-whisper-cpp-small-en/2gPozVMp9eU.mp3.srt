1
00:00:00,000 --> 00:00:04,840
 Hey, good evening everyone.

2
00:00:04,840 --> 00:00:23,000
 We're broadcasting live March 11th.

3
00:00:23,000 --> 00:00:35,000
 Today's quote is about sand on a fingernail.

4
00:00:35,000 --> 00:00:39,410
 The Buddha liked to, not like the other Buddha, was

5
00:00:39,410 --> 00:00:48,000
 accustomed to or was...

6
00:00:48,000 --> 00:00:55,090
 He was really good at finding examples in nature and coming

7
00:00:55,090 --> 00:01:02,080
 up with comparisons between

8
00:01:02,080 --> 00:01:10,200
 the nature of the world, the nature of the natural world

9
00:01:10,200 --> 00:01:13,240
 and the nature of reality.

10
00:01:13,240 --> 00:01:19,560
 So he was able to show similar...

11
00:01:19,560 --> 00:01:23,920
 I'm using simple examples in nature.

12
00:01:23,920 --> 00:01:29,560
 So it's a very common thing, common tool.

13
00:01:29,560 --> 00:01:39,850
 So one day he picked up a few grains of sand on his fingern

14
00:01:39,850 --> 00:01:44,360
ail and he asked Ananda.

15
00:01:44,360 --> 00:01:50,400
 He asked a bunch of bhikkhus.

16
00:01:50,400 --> 00:01:56,160
 He asked the bhikkhus, "Tangi manyattam bhikkhoi kattamang

17
00:01:56,160 --> 00:01:57,840
 nuko bahutarang?"

18
00:01:57,840 --> 00:02:06,640
 Tell me what do you think monks, which is more?

19
00:02:06,640 --> 00:02:21,360
 The little bit of sand that I've put on my fingernail?

20
00:02:21,360 --> 00:02:32,920
 Or a yang maha bhatvi or this great earth?

21
00:02:32,920 --> 00:02:36,320
 He used the simile for different things.

22
00:02:36,320 --> 00:02:39,440
 In one place he talks about...

23
00:02:39,440 --> 00:02:52,990
 Well, not exactly the simile, but he says this even a

24
00:02:52,990 --> 00:02:59,120
 little bit of...

25
00:02:59,120 --> 00:03:03,370
 There's one place where he talks about a little bit of

26
00:03:03,370 --> 00:03:08,300
 feces, a little bit of manure, kow dung

27
00:03:08,300 --> 00:03:11,560
 or human dung, excrement, a little bit of excrement.

28
00:03:11,560 --> 00:03:15,240
 You see, if you have a little bit of excrement, is it any

29
00:03:15,240 --> 00:03:17,480
 better than a lot of excrement?

30
00:03:17,480 --> 00:03:21,260
 So a little bit of excrement, suddenly beautiful, suddenly

31
00:03:21,260 --> 00:03:22,240
 wonderful.

32
00:03:22,240 --> 00:03:26,280
 And he said, "No, even a little bit of excrement is a bad

33
00:03:26,280 --> 00:03:27,160
 thing."

34
00:03:27,160 --> 00:03:31,110
 And the Buddha said it on the same way, even a little bit

35
00:03:31,110 --> 00:03:33,360
 of suffering, a little bit of

36
00:03:33,360 --> 00:03:43,920
 samsara, a little bit of rebirth is still bad.

37
00:03:43,920 --> 00:03:47,330
 Still not the way, still not a goal, not something you

38
00:03:47,330 --> 00:03:49,760
 should look for, should seek for.

39
00:03:49,760 --> 00:03:55,390
 But here he uses this little bit to say that's like the

40
00:03:55,390 --> 00:03:58,000
 number of beings that are born as

41
00:03:58,000 --> 00:03:59,000
 humans.

42
00:03:59,000 --> 00:04:03,400
 Being born as a human is a rare thing.

43
00:04:03,400 --> 00:04:11,120
 The number of beings that are born as animals, as insects

44
00:04:11,120 --> 00:04:16,080
 or rodents, birds, and even the

45
00:04:16,080 --> 00:04:26,960
 higher mammals, fish, all these various types of animals.

46
00:04:26,960 --> 00:04:29,760
 It's like the great earth.

47
00:04:29,760 --> 00:04:32,080
 It's far, far more than that.

48
00:04:32,080 --> 00:04:44,000
 Only a few humans.

49
00:04:44,000 --> 00:04:46,360
 And even what he doesn't say here, but what he says

50
00:04:46,360 --> 00:04:48,720
 elsewhere is, "Apakkad demonu seis

51
00:04:48,720 --> 00:04:56,620
 uni ei chena paraganu." Few are the humans who actually

52
00:04:56,620 --> 00:04:59,640
 make it to a good destination.

53
00:04:59,640 --> 00:05:04,080
 They're actually headed in the right direction.

54
00:05:04,080 --> 00:05:08,120
 So being born as a human being is like winning a lottery.

55
00:05:08,120 --> 00:05:14,520
 And most of us just squander it, waste it.

56
00:05:14,520 --> 00:05:19,760
 It's like just a coincidence and then it's gone.

57
00:05:19,760 --> 00:05:32,080
 We're back into the pool of samsara.

58
00:05:32,080 --> 00:05:38,690
 So there are many ways that we have to remember to take

59
00:05:38,690 --> 00:05:43,000
 advantage of being born as a human

60
00:05:43,000 --> 00:05:44,000
 being.

61
00:05:44,000 --> 00:05:47,400
 We have to act like human beings.

62
00:05:47,400 --> 00:05:51,840
 There are human beings that act like hell beings.

63
00:05:51,840 --> 00:05:56,880
 There are human beings that act like ghosts.

64
00:05:56,880 --> 00:05:59,600
 Human beings that act like animals.

65
00:05:59,600 --> 00:06:00,600
 And that's where they're going.

66
00:06:00,600 --> 00:06:03,520
 That's where they're heading.

67
00:06:03,520 --> 00:06:06,560
 Someone who's full of anger and hatred.

68
00:06:06,560 --> 00:06:15,000
 It's like they're a demon on earth.

69
00:06:15,000 --> 00:06:20,040
 A person who's full of greed.

70
00:06:20,040 --> 00:06:23,200
 Wanting this, wanting that can overcome their wanting.

71
00:06:23,200 --> 00:06:27,080
 It's like a ghost.

72
00:06:27,080 --> 00:06:29,800
 Hungry ghost.

73
00:06:29,800 --> 00:06:35,120
 Wailing, wanting.

74
00:06:35,120 --> 00:06:38,400
 Never satisfied.

75
00:06:38,400 --> 00:06:41,800
 Like in the Christmas Carol.

76
00:06:41,800 --> 00:06:45,600
 Morally scrooges partner.

77
00:06:45,600 --> 00:06:50,800
 Who was destined to walk the earth.

78
00:06:50,800 --> 00:06:51,800
 Never satisfied.

79
00:06:51,800 --> 00:06:58,920
 Because he was full of greed when he was alive.

80
00:06:58,920 --> 00:07:04,680
 Beings are full of delusion.

81
00:07:04,680 --> 00:07:08,320
 Arrogant and conceited.

82
00:07:08,320 --> 00:07:09,320
 Just like cats.

83
00:07:09,320 --> 00:07:12,400
 Cats are so arrogant and conceited.

84
00:07:12,400 --> 00:07:15,000
 How many of them?

85
00:07:15,000 --> 00:07:21,200
 And dogs are just ignorant and kind of dumb.

86
00:07:21,200 --> 00:07:27,880
 So if we're keen on ignorance and arrogance and conceit and

87
00:07:27,880 --> 00:07:30,280
 all these things.

88
00:07:30,280 --> 00:07:35,920
 If we're caught up in drugs and alcohol.

89
00:07:35,920 --> 00:07:38,120
 Deluding and befuddling our minds.

90
00:07:38,120 --> 00:07:42,280
 It's the way, it's just like being an animal.

91
00:07:42,280 --> 00:07:50,040
 It's like being an animal on, animal, human animal.

92
00:07:50,040 --> 00:07:53,760
 But then there are other humans that are like angels.

93
00:07:53,760 --> 00:07:56,920
 Humans like that are like gods.

94
00:07:56,920 --> 00:08:01,640
 There are other humans that are like Buddhas.

95
00:08:01,640 --> 00:08:04,880
 These are the ones that we aim for.

96
00:08:04,880 --> 00:08:09,840
 There are ways as a human being to be like an angel or a

97
00:08:09,840 --> 00:08:12,120
 god or even a Buddha.

98
00:08:12,120 --> 00:08:14,040
 We may not be a Buddha.

99
00:08:14,040 --> 00:08:16,920
 But we can be like one.

100
00:08:16,920 --> 00:08:21,440
 We follow the Buddha's example.

101
00:08:21,440 --> 00:08:27,880
 We cultivate mindfulness and clarity and wisdom.

102
00:08:27,880 --> 00:08:32,760
 We have to act, we have to be mindful of our position as

103
00:08:32,760 --> 00:08:34,360
 human beings.

104
00:08:34,360 --> 00:08:39,720
 I've been getting calls lately and I have to say something.

105
00:08:39,720 --> 00:08:45,070
 Someone calls, it's really, religion really does strange

106
00:08:45,070 --> 00:08:46,920
 things to people.

107
00:08:46,920 --> 00:08:49,960
 Spirituality.

108
00:08:49,960 --> 00:08:53,410
 Sometimes it's just kind of funny because people come up to

109
00:08:53,410 --> 00:08:54,840
 me all the time and say the

110
00:08:54,840 --> 00:09:00,390
 wackiest things and the emails I get and now the phone

111
00:09:00,390 --> 00:09:02,720
 calls I'm getting.

112
00:09:02,720 --> 00:09:10,200
 But sometimes it's, you have to be mindful of yourself.

113
00:09:10,200 --> 00:09:14,020
 Someone called me up today, not to really, not to get upset

114
00:09:14,020 --> 00:09:15,920
 or anything but just to talk

115
00:09:15,920 --> 00:09:16,920
 about this.

116
00:09:16,920 --> 00:09:20,900
 He called me up today and just asked if this was the

117
00:09:20,900 --> 00:09:24,160
 International Meditation Center, I

118
00:09:24,160 --> 00:09:26,160
 think.

119
00:09:26,160 --> 00:09:32,570
 Without introducing himself, without asking how I was, if I

120
00:09:32,570 --> 00:09:35,000
've got time to talk and I

121
00:09:35,000 --> 00:09:39,190
 answer questions, just started asking me questions about

122
00:09:39,190 --> 00:09:41,360
 monks and why monks weren't keeping

123
00:09:41,360 --> 00:09:48,200
 the rules and why I'm not doing my videos.

124
00:09:48,200 --> 00:09:50,840
 I didn't want to answer him.

125
00:09:50,840 --> 00:09:57,000
 I didn't feel comfortable because I didn't feel like he was

126
00:09:57,000 --> 00:09:58,360
 mindful.

127
00:09:58,360 --> 00:10:03,400
 That's not how you start a conversation with someone.

128
00:10:03,400 --> 00:10:07,480
 When people come up, when someone comes to you, when you

129
00:10:07,480 --> 00:10:09,560
 approach someone, you should

130
00:10:09,560 --> 00:10:12,880
 ask how they are, you should introduce yourself and you

131
00:10:12,880 --> 00:10:14,440
 should be mindful of the fact that

132
00:10:14,440 --> 00:10:16,760
 you don't know this person.

133
00:10:16,760 --> 00:10:21,330
 To ask them deep questions about this, not deep, but

134
00:10:21,330 --> 00:10:24,280
 personal questions really about

135
00:10:24,280 --> 00:10:30,570
 monastic life and why monks this and that, without ever

136
00:10:30,570 --> 00:10:33,480
 even telling me your name.

137
00:10:33,480 --> 00:10:37,160
 I asked this guy, I said, "Do I know you?"

138
00:10:37,160 --> 00:10:40,520
 Anyway, it wasn't a big thing.

139
00:10:40,520 --> 00:10:44,430
 I thought afterwards that because I didn't answer and I was

140
00:10:44,430 --> 00:10:46,520
 just quite quiet and he started

141
00:10:46,520 --> 00:10:53,520
 laughing at me and hung up.

142
00:10:53,520 --> 00:10:54,680
 That's past.

143
00:10:54,680 --> 00:10:55,960
 That's not a problem.

144
00:10:55,960 --> 00:11:02,320
 I'm not trying to come on your event, but I know this kind

145
00:11:02,320 --> 00:11:05,080
 of attitude sometimes we're

146
00:11:05,080 --> 00:11:07,840
 not mindful.

147
00:11:07,840 --> 00:11:12,250
 We have to be mindful because I think it's because it's out

148
00:11:12,250 --> 00:11:13,560
 of our realm.

149
00:11:13,560 --> 00:11:20,830
 We're good at being human in the frame of reference that we

150
00:11:20,830 --> 00:11:21,960
 have.

151
00:11:21,960 --> 00:11:27,870
 When we go to work or school, we get good at playing the

152
00:11:27,870 --> 00:11:28,880
 game.

153
00:11:28,880 --> 00:11:31,520
 We get good at saying the right things.

154
00:11:31,520 --> 00:11:36,510
 As a monk, I am proficient at being a monk and proficient

155
00:11:36,510 --> 00:11:38,400
 at ... When you're taken out

156
00:11:38,400 --> 00:11:43,440
 of your comfort zone, it's familiar.

157
00:11:43,440 --> 00:11:48,040
 You can't rely upon habit.

158
00:11:48,040 --> 00:11:51,600
 I think this goes for a lot of situations.

159
00:11:51,600 --> 00:11:56,500
 When you're sick, people can be very irritable and unmind

160
00:11:56,500 --> 00:11:59,520
ful when they're sick because it's

161
00:11:59,520 --> 00:12:03,080
 a situation that they're not accustomed to.

162
00:12:03,080 --> 00:12:12,440
 When we're surrounded by people or when we're in public.

163
00:12:12,440 --> 00:12:16,560
 This is by the importance of understanding mindfulness.

164
00:12:16,560 --> 00:12:18,080
 It's quite different from habit.

165
00:12:18,080 --> 00:12:19,760
 It's different from concentration.

166
00:12:19,760 --> 00:12:21,840
 It's not something you can build up.

167
00:12:21,840 --> 00:12:24,910
 Something you have to be ... You can build it up, but you

168
00:12:24,910 --> 00:12:26,280
 build it as a skill and then

169
00:12:26,280 --> 00:12:27,720
 you use it.

170
00:12:27,720 --> 00:12:28,720
 You have to use it.

171
00:12:28,720 --> 00:12:31,960
 You have to be mindful.

172
00:12:31,960 --> 00:12:38,040
 To remember yourself.

173
00:12:38,040 --> 00:12:39,560
 This is ... Let this be a lesson.

174
00:12:39,560 --> 00:12:45,700
 It's an interesting lesson of acting appropriately when you

175
00:12:45,700 --> 00:12:47,560
 talk to people.

176
00:12:47,560 --> 00:12:52,360
 You engage with people.

177
00:12:52,360 --> 00:12:58,310
 This is a thing that's not related to meditation, but also

178
00:12:58,310 --> 00:13:02,040
 very much related to meditation.

179
00:13:02,040 --> 00:13:05,400
 It means how we behave in the world around us.

180
00:13:05,400 --> 00:13:08,240
 The things we do, the things we say.

181
00:13:08,240 --> 00:13:11,540
 It's not meditation.

182
00:13:11,540 --> 00:13:14,490
 People often ask me questions about, "What should I do in

183
00:13:14,490 --> 00:13:15,560
 this situation?

184
00:13:15,560 --> 00:13:16,560
 What should I do?

185
00:13:16,560 --> 00:13:21,960
 How should I behave as a human being in this situation?"

186
00:13:21,960 --> 00:13:27,520
 These are difficult questions to answer here.

187
00:13:27,520 --> 00:13:31,400
 People are expecting a solution to their problems.

188
00:13:31,400 --> 00:13:35,860
 It's very much ... It's unrelated to meditation, but very

189
00:13:35,860 --> 00:13:38,720
 much related to meditation because

190
00:13:38,720 --> 00:13:40,200
 I can't give you the answer.

191
00:13:40,200 --> 00:13:45,240
 That's not how it works.

192
00:13:45,240 --> 00:13:50,480
 The requirement is a foundation that is capable of dealing

193
00:13:50,480 --> 00:13:52,920
 with your own problems.

194
00:13:52,920 --> 00:13:55,000
 You have to find the solution for yourself.

195
00:13:55,000 --> 00:13:57,000
 That's the only way it works.

196
00:13:57,000 --> 00:14:00,360
 I tell you the answer and then you go and do this or do

197
00:14:00,360 --> 00:14:01,000
 that.

198
00:14:01,000 --> 00:14:02,000
 It's artificial.

199
00:14:02,000 --> 00:14:04,440
 You can see.

200
00:14:04,440 --> 00:14:08,000
 Whatever you say or whatever you do, it's not from the

201
00:14:08,000 --> 00:14:08,680
 heart.

202
00:14:08,680 --> 00:14:15,160
 It's not being ... It requires mindfulness as a thing.

203
00:14:15,160 --> 00:14:16,920
 That's why it's so important.

204
00:14:16,920 --> 00:14:21,040
 You want to truly be a human.

205
00:14:21,040 --> 00:14:23,240
 Truly live as a human being.

206
00:14:23,240 --> 00:14:24,680
 You need mindfulness as the base.

207
00:14:24,680 --> 00:14:28,880
 You always, always come back to it.

208
00:14:28,880 --> 00:14:36,880
 Or set the ... Mindfulness may not be the best way.

209
00:14:36,880 --> 00:14:40,800
 Oh, right.

210
00:14:40,800 --> 00:14:45,150
 I was ... I meditated earlier and then somehow the timer

211
00:14:45,150 --> 00:14:47,840
 didn't work, so I clicked it late.

212
00:14:47,840 --> 00:14:50,520
 I think it says I'm just finishing meditation now.

213
00:14:50,520 --> 00:14:52,600
 I did my hour.

214
00:14:52,600 --> 00:14:55,850
 If you're wondering why I'm supposed to be meditating when

215
00:14:55,850 --> 00:14:56,920
 I'm doing this.

216
00:14:56,920 --> 00:15:01,090
 We've got ... Everybody signs in for when they're going to

217
00:15:01,090 --> 00:15:02,000
 meditate.

218
00:15:02,000 --> 00:15:09,360
 I was doing meditation like an hour ago or over an hour ago

219
00:15:09,360 --> 00:15:09,880
.

220
00:15:09,880 --> 00:15:14,080
 And it didn't click for some reason, so after I did walking

221
00:15:14,080 --> 00:15:16,480
, I checked it and then I clicked

222
00:15:16,480 --> 00:15:17,480
 it.

223
00:15:17,480 --> 00:15:21,000
 So, it's a half an hour late.

224
00:15:21,000 --> 00:15:26,600
 I'm not just clicking and then not meditating.

225
00:15:26,600 --> 00:15:32,000
 That's the danger with this.

226
00:15:32,000 --> 00:15:35,400
 Can't tell whether people are actually meditating.

227
00:15:35,400 --> 00:15:41,840
 I don't know why anyone would ... Anyway.

228
00:15:41,840 --> 00:15:48,840
 So, yeah, that's the quote for this evening.

229
00:15:48,840 --> 00:15:53,160
 Let's give a little poly.

230
00:15:53,160 --> 00:15:54,160
 Where is the poly?

231
00:15:54,160 --> 00:16:01,400
 Apaka deh sata yemenusi sup jaja yamti.

232
00:16:01,400 --> 00:16:07,200
 And so, monks, few are those beings that are reborn as

233
00:16:07,200 --> 00:16:08,400
 humans.

234
00:16:08,400 --> 00:16:19,340
 Atokko eitei yewa bahutara sata yey anyatramusi pechaja yam

235
00:16:19,340 --> 00:16:19,440
ti.

236
00:16:19,440 --> 00:16:24,730
 Far more are those beings that are born as something other

237
00:16:24,730 --> 00:16:26,120
 than human.

238
00:16:26,120 --> 00:16:33,040
 Asmati habikabe ei wang sikitabang.

239
00:16:33,040 --> 00:16:39,920
 Therefore, o monks, here you should train thus.

240
00:16:39,920 --> 00:16:50,870
 We will dwell with apamada, apamata, not intoxicated, not

241
00:16:50,870 --> 00:16:53,920
 confused in the mind, mindful basically.

242
00:16:53,920 --> 00:16:57,880
 Ei wan hi wo bhikkbe zikitabang.

243
00:16:57,880 --> 00:17:00,360
 Thus should you train obikus.

244
00:17:00,360 --> 00:17:05,960
 This is the sanyurta nikaya.

245
00:17:05,960 --> 00:17:09,640
 Naka sika suta, the fingernail.

246
00:17:09,640 --> 00:17:10,640
 Suta.

247
00:17:10,640 --> 00:17:15,040
 Naka sika, wudu sika naka.

248
00:17:15,040 --> 00:17:21,240
 Naka is fingernail, sika.

249
00:17:21,240 --> 00:17:24,440
 The top, the tip.

250
00:17:24,440 --> 00:17:30,880
 Naka sika is the tip of the fingernail.

251
00:17:30,880 --> 00:17:36,320
 So that's the Nama for tonight.

252
00:17:36,320 --> 00:17:41,380
 And if people have questions, I'm going to now post the

253
00:17:41,380 --> 00:17:42,680
 Hangouts.

254
00:17:42,680 --> 00:17:48,680
 So you're welcome to come and ask questions there.

255
00:17:48,680 --> 00:17:53,030
 And they expect anyone who comes on to be mindful and to

256
00:17:53,030 --> 00:17:55,400
 introduce themselves and to

257
00:17:55,400 --> 00:17:58,540
 be respectful and mindful of the sorts of questions that

258
00:17:58,540 --> 00:17:59,680
 should be asked.

259
00:17:59,680 --> 00:18:07,360
 And now that I've scared everyone away, no one dares come

260
00:18:07,360 --> 00:18:09,680
 on, I'm sure.

261
00:18:09,680 --> 00:18:16,000
 It's just strange that people-- there's a certain

262
00:18:16,000 --> 00:18:19,720
 familiarity where people just come

263
00:18:19,720 --> 00:18:22,720
 up to me and start talking.

264
00:18:22,720 --> 00:18:28,040
 I was someone at university, I think I mentioned this, came

265
00:18:28,040 --> 00:18:32,600
 up to me and said, look, you can't

266
00:18:32,600 --> 00:18:35,430
 walk around wearing that kind of clothes and not have

267
00:18:35,430 --> 00:18:37,120
 people want to come up and ask you

268
00:18:37,120 --> 00:18:40,240
 about it and expect people to not come up and ask you.

269
00:18:40,240 --> 00:18:48,240
 And I turned to him and said, why not?

270
00:18:48,240 --> 00:18:49,240
 Mind your own business.

271
00:18:49,240 --> 00:18:52,600
 With that guy, it turned out to be quite nice.

272
00:18:52,600 --> 00:18:57,000
 He was actually quite respectful.

273
00:18:57,000 --> 00:19:02,710
 Recently, another person called us from a radio station and

274
00:19:02,710 --> 00:19:05,680
 they want to come by tomorrow.

275
00:19:05,680 --> 00:19:09,710
 They're going to come by at 11 tomorrow, which is bad

276
00:19:09,710 --> 00:19:10,400
 timing.

277
00:19:10,400 --> 00:19:14,080
 It's right when we're eating, but OK.

278
00:19:14,080 --> 00:19:16,920
 And they want to ask us about our program.

279
00:19:16,920 --> 00:19:22,800
 Basically, they give us free publicity, which is awesome.

280
00:19:22,800 --> 00:19:26,360
 Very kind of.

281
00:19:26,360 --> 00:19:29,760
 So there are people who are starting to find out about us.

282
00:19:29,760 --> 00:19:33,680
 They just have to be ready for it.

283
00:19:33,680 --> 00:19:36,260
 And I think they have to start screening my calls because

284
00:19:36,260 --> 00:19:37,800
 this person wasn't very happy

285
00:19:37,800 --> 00:19:38,800
 with me in the end.

286
00:19:38,800 --> 00:19:41,680
 I don't think it was an answer.

287
00:19:41,680 --> 00:19:47,430
 It seemed as common for it was common to a lot of their

288
00:19:47,430 --> 00:19:51,240
 stories of monks, Arhan's even,

289
00:19:51,240 --> 00:19:52,760
 who wouldn't answer questions.

290
00:19:52,760 --> 00:19:55,710
 They just wouldn't say anything when a question or when

291
00:19:55,710 --> 00:19:57,680
 someone was saying something to them

292
00:19:57,680 --> 00:20:01,960
 that just wasn't.

293
00:20:01,960 --> 00:20:05,120
 It's because of indifference.

294
00:20:05,120 --> 00:20:08,480
 Monks are not like the Buddha.

295
00:20:08,480 --> 00:20:11,960
 So I've done this before and people can be very upset at

296
00:20:11,960 --> 00:20:13,880
 you for not answering their

297
00:20:13,880 --> 00:20:18,680
 questions because they're so used to, I mean, social.

298
00:20:18,680 --> 00:20:22,590
 This is the social way is to have someone when you talk to

299
00:20:22,590 --> 00:20:24,480
 someone, you should answer

300
00:20:24,480 --> 00:20:27,080
 them.

301
00:20:27,080 --> 00:20:30,460
 And so this person was saying today, it's just an

302
00:20:30,460 --> 00:20:32,120
 interesting point.

303
00:20:32,120 --> 00:20:38,090
 He was saying, I've talked to a lot of people and I've

304
00:20:38,090 --> 00:20:40,760
 never had someone not answer, just

305
00:20:40,760 --> 00:20:43,360
 not say anything.

306
00:20:43,360 --> 00:20:46,000
 And I didn't reply to that.

307
00:20:46,000 --> 00:20:51,290
 And then he said, I'm not sure it's what the Buddha would

308
00:20:51,290 --> 00:20:54,160
 have done or basically would

309
00:20:54,160 --> 00:20:58,630
 have wanted or something, basically criticizing and saying,

310
00:20:58,630 --> 00:21:01,120
 this is what you're doing is wrong.

311
00:21:01,120 --> 00:21:03,640
 But the funny thing is, why do I care?

312
00:21:03,640 --> 00:21:05,600
 Why does it matter to me what you think?

313
00:21:05,600 --> 00:21:06,600
 It doesn't.

314
00:21:06,600 --> 00:21:11,030
 I mean, it's something about being a monk that we're very

315
00:21:11,030 --> 00:21:13,000
 much outside of this.

316
00:21:13,000 --> 00:21:15,760
 I didn't come to you, you called me.

317
00:21:15,760 --> 00:21:20,190
 So this is an interesting point for me as a monk to be in

318
00:21:20,190 --> 00:21:22,520
 this sort of situation.

319
00:21:22,520 --> 00:21:27,080
 But one interesting aspect of it is the power that we have.

320
00:21:27,080 --> 00:21:31,060
 We're not required to, you know how you have sometimes you

321
00:21:31,060 --> 00:21:34,000
 play into other people's games.

322
00:21:34,000 --> 00:21:37,400
 Bait, you know, bait and switch.

323
00:21:37,400 --> 00:21:40,460
 You ever heard of this idea of bait and switch where

324
00:21:40,460 --> 00:21:42,840
 someone baits you with something and

325
00:21:42,840 --> 00:21:46,500
 then if you get into it with them, then they start on

326
00:21:46,500 --> 00:21:48,840
 something else and they drag you

327
00:21:48,840 --> 00:21:51,440
 on and on.

328
00:21:51,440 --> 00:21:56,510
 And into something that's often quite unwholesome and

329
00:21:56,510 --> 00:21:57,680
 useless.

330
00:21:57,680 --> 00:21:59,680
 Speed.

331
00:21:59,680 --> 00:22:06,050
 And we don't have the responsibility to play other people's

332
00:22:06,050 --> 00:22:08,800
 games, I guess is the general

333
00:22:08,800 --> 00:22:10,000
 point that I'm making.

334
00:22:10,000 --> 00:22:11,880
 It's interesting to me.

335
00:22:11,880 --> 00:22:18,020
 I think as a monk, I have that luxury not having to play

336
00:22:18,020 --> 00:22:20,880
 into people's games.

337
00:22:20,880 --> 00:22:24,960
 And it's really a power of having left the world because

338
00:22:24,960 --> 00:22:27,000
 there's nothing you can do to

339
00:22:27,000 --> 00:22:29,280
 me.

340
00:22:29,280 --> 00:22:31,280
 I'm happy to help.

341
00:22:31,280 --> 00:22:37,480
 I told this guy, if you want to learn meditation, I'm happy

342
00:22:37,480 --> 00:22:38,800
 to help.

343
00:22:38,800 --> 00:22:42,000
 That was really interesting.

344
00:22:42,000 --> 00:22:44,000
 Anyway.

345
00:22:44,000 --> 00:22:55,920
 We have a great power in mindfulness, in not worrying, not

346
00:22:55,920 --> 00:23:01,680
 clinging, not needing anything

347
00:23:01,680 --> 00:23:05,680
 from other people.

348
00:23:05,680 --> 00:23:13,720
 Anyway, enough talk.

349
00:23:13,720 --> 00:23:14,720
 Thank you all.

350
00:23:14,720 --> 00:23:16,160
 I guess there's no questions.

351
00:23:16,160 --> 00:23:18,440
 So that's all for this evening.

352
00:23:18,440 --> 00:23:21,280
 I'm wishing you all good practice and good night.

353
00:23:21,280 --> 00:23:22,280
 Thank you.

354
00:23:22,280 --> 00:23:38,360
 [

