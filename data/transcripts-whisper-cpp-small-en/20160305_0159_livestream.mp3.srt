1
00:00:00,000 --> 00:00:14,000
 Good evening everyone.

2
00:00:14,000 --> 00:00:31,000
 Broadcasting line March 4th.

3
00:00:31,000 --> 00:00:38,000
 Today's quote is from the Jataka.

4
00:00:38,000 --> 00:00:47,000
 It's a short quote. I'm going to read it off in full.

5
00:00:47,000 --> 00:00:52,000
 I don't know the following from the minute.

6
00:00:52,000 --> 00:00:57,000
 "He who puts aside his wants in order to do the right thing

7
00:00:57,000 --> 00:00:57,000
,

8
00:00:57,000 --> 00:01:00,000
 even though it would be difficult,

9
00:01:00,000 --> 00:01:12,000
 like a patient drinking medicine, later he will rejoice."

10
00:01:12,000 --> 00:01:22,000
 So yeah, sometimes doing the right thing hurts.

11
00:01:22,000 --> 00:01:26,000
 Sometimes you want something that you know the right thing

12
00:01:26,000 --> 00:01:28,000
 isn't it?

13
00:01:28,000 --> 00:01:31,000
 Just to not take it.

14
00:01:31,000 --> 00:01:35,710
 Do something, you know, that when I'm not doing it, it's

15
00:01:35,710 --> 00:01:43,000
 probably the better thing.

16
00:01:43,000 --> 00:01:52,000
 This is a really important teaching, I think.

17
00:01:52,000 --> 00:01:58,000
 Because it's hard to go against your desires.

18
00:01:58,000 --> 00:02:01,000
 You're not equipped to.

19
00:02:01,000 --> 00:02:13,000
 Our brains are designed to accommodate addiction,

20
00:02:13,000 --> 00:02:16,000
 accommodate our desires.

21
00:02:16,000 --> 00:02:20,660
 And the no impulse gets weaker and weaker as you become

22
00:02:20,660 --> 00:02:22,000
 more addicted.

23
00:02:22,000 --> 00:02:26,000
 So the no system, whatever that is.

24
00:02:26,000 --> 00:02:29,010
 The system that says no is quite different from the system

25
00:02:29,010 --> 00:02:33,000
 that says yes.

26
00:02:33,000 --> 00:02:36,000
 And so you can only say no for so long,

27
00:02:36,000 --> 00:02:42,000
 and it takes effort and it's taxing to say no.

28
00:02:42,000 --> 00:02:50,000
 You prevent yourself from following your desires.

29
00:02:50,000 --> 00:02:55,000
 That's not the important part of this first.

30
00:02:55,000 --> 00:02:58,950
 The important part of the teaching is not to deny your

31
00:02:58,950 --> 00:03:00,000
 desires.

32
00:03:00,000 --> 00:03:06,660
 It's about the happiness that comes from what's right, from

33
00:03:06,660 --> 00:03:09,000
 knowing what's right.

34
00:03:09,000 --> 00:03:13,000
 Just like the happiness that comes from taking medicine.

35
00:03:13,000 --> 00:03:15,810
 The only reason we take medicine is not because of how it

36
00:03:15,810 --> 00:03:19,000
 tastes or because of what makes us feel,

37
00:03:19,000 --> 00:03:24,000
 but it's because it cures us of our illness.

38
00:03:24,000 --> 00:03:36,350
 And that knowledge, that reassurance, confidence in our

39
00:03:36,350 --> 00:03:37,000
 mind,

40
00:03:37,000 --> 00:03:41,000
 that is strong enough.

41
00:03:41,000 --> 00:03:47,000
 That is strong enough to overcome the desire.

42
00:03:47,000 --> 00:03:52,870
 You want something and that's a hard argument to fight

43
00:03:52,870 --> 00:03:54,000
 against.

44
00:03:54,000 --> 00:03:57,000
 I want it.

45
00:03:57,000 --> 00:03:59,000
 I want that.

46
00:03:59,000 --> 00:04:01,000
 That thing makes me happy.

47
00:04:01,000 --> 00:04:03,000
 It's a hard argument to fight.

48
00:04:03,000 --> 00:04:12,000
 You can't just say no, no, bad for you.

49
00:04:12,000 --> 00:04:15,000
 But it's the only way.

50
00:04:15,000 --> 00:04:18,320
 It's understandable if you don't find something better than

51
00:04:18,320 --> 00:04:19,000
 the sense of desire.

52
00:04:19,000 --> 00:04:22,000
 It's understandable that you cling to them.

53
00:04:22,000 --> 00:04:27,000
 When you find something better...

54
00:04:27,000 --> 00:04:39,000
 When you find something more powerful,

55
00:04:39,000 --> 00:04:49,000
 this can be enough to cause you to let go of your desires.

56
00:04:49,000 --> 00:04:52,000
 This is why giving is better than receiving.

57
00:04:52,000 --> 00:04:54,000
 It really is.

58
00:04:54,000 --> 00:04:57,000
 It really feels better to give than to receive.

59
00:04:57,000 --> 00:05:04,000
 And this is how one conquers miserliness within oneself.

60
00:05:04,000 --> 00:05:10,000
 Stinginess, practice giving, because it will feel good.

61
00:05:10,000 --> 00:05:13,000
 It will feel better than getting.

62
00:05:13,000 --> 00:05:19,040
 If you do it enough, it's actually a way to overcome sting

63
00:05:19,040 --> 00:05:21,000
iness, to give.

64
00:05:21,000 --> 00:05:24,000
 Practice giving.

65
00:05:24,000 --> 00:05:31,000
 It actually works because it feels better.

66
00:05:31,000 --> 00:05:34,000
 Breaking precepts is always nice.

67
00:05:34,000 --> 00:05:39,940
 We like to kill and steal and lie and cheat when we get

68
00:05:39,940 --> 00:05:42,000
 away with it.

69
00:05:42,000 --> 00:05:45,660
 Normally the only way we do away with it is we prevent

70
00:05:45,660 --> 00:05:49,000
 ourselves from breaking precepts.

71
00:05:49,000 --> 00:05:54,000
 Being immoral is by telling us how bad ourselves are.

72
00:05:54,000 --> 00:05:57,000
 Bad being immoral.

73
00:05:57,000 --> 00:05:59,000
 That again doesn't really...

74
00:05:59,000 --> 00:06:02,000
 It's not uplifting.

75
00:06:02,000 --> 00:06:04,000
 It's not a substitute.

76
00:06:04,000 --> 00:06:07,000
 You can't live your life just saying everything is bad.

77
00:06:07,000 --> 00:06:12,000
 This is bad, that's bad.

78
00:06:12,000 --> 00:06:16,000
 But when you do keep the precepts, when you keep the rules,

79
00:06:16,000 --> 00:06:20,000
 when you look at it as uplifting,

80
00:06:20,000 --> 00:06:30,000
 when you feel the power of being a good person,

81
00:06:30,000 --> 00:06:35,000
 it's this power that actually...

82
00:06:35,000 --> 00:06:39,000
 The power of goodness, the power of being moral,

83
00:06:39,000 --> 00:06:41,550
 of knowing that you're an ethical person, you're not

84
00:06:41,550 --> 00:06:44,000
 stealing, you're not killing.

85
00:06:44,000 --> 00:06:46,000
 That power is uplifting.

86
00:06:46,000 --> 00:06:50,630
 You sit and you think about how you've given freedom from

87
00:06:50,630 --> 00:06:53,000
 fear to all beings.

88
00:06:53,000 --> 00:07:00,000
 Abhayadhana, the gift of fearlessness.

89
00:07:00,000 --> 00:07:05,000
 No one needs to be afraid of you.

90
00:07:05,000 --> 00:07:11,520
 It's just with the precepts, and it's a gift, and it feels

91
00:07:11,520 --> 00:07:13,000
 good.

92
00:07:13,000 --> 00:07:25,000
 [Coughing]

93
00:07:25,000 --> 00:07:33,000
 The same goes with green, with anger, even with delusion,

94
00:07:33,000 --> 00:07:34,000
 with ignorance.

95
00:07:34,000 --> 00:07:37,000
 They say ignorance is bliss.

96
00:07:37,000 --> 00:07:39,000
 So it's nice to just live your life and not care,

97
00:07:39,000 --> 00:07:43,390
 and not carry the world on your shoulders, or think about

98
00:07:43,390 --> 00:07:44,000
 death,

99
00:07:44,000 --> 00:07:47,030
 or think about getting sick or old age, or think about

100
00:07:47,030 --> 00:07:48,000
 suffering,

101
00:07:48,000 --> 00:07:51,000
 think about the world's problems.

102
00:07:51,000 --> 00:07:54,000
 It's blissful to not think about these things.

103
00:07:54,000 --> 00:07:57,640
 It's really a horrible philosophy because your problems don

104
00:07:57,640 --> 00:07:58,000
't go away

105
00:07:58,000 --> 00:08:00,000
 just because you're ignorant of them.

106
00:08:00,000 --> 00:08:04,210
 And so that bliss is not sustainable, it's not rational, it

107
00:08:04,210 --> 00:08:06,000
's not reasonable.

108
00:08:06,000 --> 00:08:09,000
 But the opposite seems even worse, to have to live your

109
00:08:09,000 --> 00:08:09,000
 life

110
00:08:09,000 --> 00:08:13,000
 and worry about these things. What good does that do?

111
00:08:13,000 --> 00:08:16,000
 What good does it do to concern yourself with the problems

112
00:08:16,000 --> 00:08:17,000
 of life,

113
00:08:17,000 --> 00:08:21,000
 the problems of the world?

114
00:08:21,000 --> 00:08:29,000
 It just makes you stressed and sick and upset.

115
00:08:29,000 --> 00:08:31,440
 So ignorance is better, but the only thing better than

116
00:08:31,440 --> 00:08:34,000
 ignorance is knowledge,

117
00:08:34,000 --> 00:08:37,670
 and not the kind of knowledge, not knowledge of your

118
00:08:37,670 --> 00:08:38,000
 problems,

119
00:08:38,000 --> 00:08:42,000
 but understanding of your problems.

120
00:08:42,000 --> 00:08:44,870
 Maybe, sir, maybe knowledge is a bad word, but

121
00:08:44,870 --> 00:08:46,000
 understanding.

122
00:08:46,000 --> 00:08:51,000
 So ignorance is bliss, but because knowledge is suffering,

123
00:08:51,000 --> 00:08:58,000
 but understanding, that's peace.

124
00:08:58,000 --> 00:09:03,070
 Because you understand your problems, they cease to be

125
00:09:03,070 --> 00:09:04,000
 problems.

126
00:09:04,000 --> 00:09:09,000
 And then thinking about them is not stressful.

127
00:09:09,000 --> 00:09:13,000
 Then they have no power over you.

128
00:09:13,000 --> 00:09:18,000
 When they do come to pass, when they do rear their heads,

129
00:09:18,000 --> 00:09:23,000
 they have no power over you because you understand them.

130
00:09:23,000 --> 00:09:27,000
 You know their true nature.

131
00:09:27,000 --> 00:09:38,330
 They're not shocked or upset by them, but surprised by them

132
00:09:38,330 --> 00:09:39,000
.

133
00:09:39,000 --> 00:09:45,000
 And so there's wisdom, wisdom that comes through meditation

134
00:09:45,000 --> 00:09:45,000
,

135
00:09:45,000 --> 00:09:54,000
 through understanding, through experience.

136
00:09:54,000 --> 00:09:58,940
 This is more powerful than ignorance, better than ignorance

137
00:09:58,940 --> 00:09:59,000
.

138
00:09:59,000 --> 00:10:04,000
 The simile with the medicine,

139
00:10:04,000 --> 00:10:09,000
 a person who steeps themselves in greed, anger and delusion

140
00:10:09,000 --> 00:10:09,000
,

141
00:10:09,000 --> 00:10:14,160
 follows their desires, even when it means not doing the

142
00:10:14,160 --> 00:10:16,000
 right thing.

143
00:10:16,000 --> 00:10:19,000
 It's like a person who doesn't take their medicine,

144
00:10:19,000 --> 00:10:22,000
 a sick person who doesn't take their medicine,

145
00:10:22,000 --> 00:10:25,680
 who doesn't take their medicine, who doesn't take their

146
00:10:25,680 --> 00:10:26,000
 medicine,

147
00:10:26,000 --> 00:10:29,690
 who doesn't take their medicine, who doesn't take their

148
00:10:29,690 --> 00:10:30,000
 medicine,

149
00:10:30,000 --> 00:10:33,000
 who doesn't take their medicine,

150
00:10:33,000 --> 00:10:36,670
 who doesn't take their medicine, who doesn't take their

151
00:10:36,670 --> 00:10:37,000
 medicine,

152
00:10:37,000 --> 00:10:40,630
 who doesn't take their medicine, who doesn't take their

153
00:10:40,630 --> 00:10:41,000
 medicine,

154
00:10:41,000 --> 00:10:44,380
 who doesn't take their medicine, who doesn't take their

155
00:10:44,380 --> 00:10:45,000
 medicine,

156
00:10:45,000 --> 00:10:48,000
 who doesn't take their medicine, who doesn't take their

157
00:10:48,000 --> 00:10:48,000
 medicine,

158
00:10:48,000 --> 00:10:52,000
 because sometimes you have to do what is unpleasant.

159
00:10:52,000 --> 00:10:55,300
 You have to go against your desires, you have to go against

160
00:10:55,300 --> 00:10:56,000
 your anger,

161
00:10:56,000 --> 00:10:59,000
 be patient when other people make you upset,

162
00:10:59,000 --> 00:11:02,000
 when things make you upset, be patient.

163
00:11:02,000 --> 00:11:08,000
 Bear with unpleasantness.

164
00:11:08,000 --> 00:11:13,020
 Sometimes you have to wake yourself up, slap yourself in

165
00:11:13,020 --> 00:11:15,000
 the face,

166
00:11:15,000 --> 00:11:18,000
 face your problems, learn about your problems,

167
00:11:18,000 --> 00:11:21,000
 understand your problems, rather than run away from them,

168
00:11:21,000 --> 00:11:24,000
 or ignore them, or pretend they don't exist,

169
00:11:24,000 --> 00:11:27,000
 or drown yourself in drugs and alcohol,

170
00:11:27,000 --> 00:11:33,000
 so that you don't have to think about them.

171
00:11:33,000 --> 00:11:43,000
 If you can actually understand them.

172
00:11:43,000 --> 00:11:50,000
 If you can cultivate wisdom about them.

173
00:11:50,000 --> 00:11:57,200
 Then you rejoice, then you truly, truly find true peace and

174
00:11:57,200 --> 00:12:00,000
 happiness.

175
00:12:00,000 --> 00:12:08,000
 There is lasting, substantial, meaningful.

176
00:12:08,000 --> 00:12:16,000
 Interesting little quote.

177
00:12:16,000 --> 00:12:22,000
 I'm quite busy this weekend, I'm still not overly sicker,

178
00:12:22,000 --> 00:12:30,000
 so tomorrow I'm canceling second time.

179
00:12:30,000 --> 00:12:37,000
 I'll see you about more nine o'clock broadcasts.

180
00:12:37,000 --> 00:12:47,230
 I'm behind in my schoolwork, so I'll get catched up in the

181
00:12:47,230 --> 00:12:50,000
 next week.

182
00:12:50,000 --> 00:12:54,000
 Thank you all for tuning in, and good night.

