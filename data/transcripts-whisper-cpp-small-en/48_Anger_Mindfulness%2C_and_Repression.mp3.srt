1
00:00:00,000 --> 00:00:04,320
 Anger, mindfulness, and repression.

2
00:00:04,320 --> 00:00:05,840
 Question.

3
00:00:05,840 --> 00:00:10,890
 When I experience a situation where I feel angry and try to

4
00:00:10,890 --> 00:00:12,120
 be mindful,

5
00:00:12,120 --> 00:00:17,410
 I feel that I am repressing it by saying angry, angry, and

6
00:00:17,410 --> 00:00:20,040
 trying to investigate it.

7
00:00:20,040 --> 00:00:23,520
 Afterwards, I feel frustrated.

8
00:00:23,520 --> 00:00:26,880
 Am I doing something wrong in the practice?

9
00:00:26,880 --> 00:00:31,650
 Am I catching the thought too late or am I still not

10
00:00:31,650 --> 00:00:33,760
 advanced enough?

11
00:00:33,760 --> 00:00:35,320
 Answer.

12
00:00:35,320 --> 00:00:39,700
 From what I've seen, the reason why people feel like they

13
00:00:39,700 --> 00:00:41,200
 are repressing anything by

14
00:00:41,200 --> 00:00:46,380
 acknowledging or noting is because strong emotions like

15
00:00:46,380 --> 00:00:48,760
 anger are associated with a

16
00:00:48,760 --> 00:00:51,080
 painful feeling.

17
00:00:51,080 --> 00:00:55,830
 Anger will often create physical suffering like headaches,

18
00:00:55,830 --> 00:00:57,520
 tension, and pain throughout

19
00:00:57,520 --> 00:00:59,240
 the body.

20
00:00:59,240 --> 00:01:05,040
 When you say to yourself angry, angry, the anger disappears

21
00:01:05,040 --> 00:01:06,480
 in the mind, but you are

22
00:01:06,480 --> 00:01:11,390
 still left with the unpleasant feelings which are actually

23
00:01:11,390 --> 00:01:14,920
 a result of the anger, not a result

24
00:01:14,920 --> 00:01:18,160
 of mindfulness.

25
00:01:18,160 --> 00:01:22,370
 Another example of this is when you are distracted,

26
00:01:22,370 --> 00:01:24,600
 thinking about many things.

27
00:01:24,600 --> 00:01:29,320
 When you finally realize you have been distracted in, say,

28
00:01:29,320 --> 00:01:32,960
 thinking, thinking, or distracted,

29
00:01:32,960 --> 00:01:36,920
 distracted, if you notice a headache from the brain

30
00:01:36,920 --> 00:01:39,800
 activity you might think, "Oh,

31
00:01:39,800 --> 00:01:45,240
 being mindful causes headaches," but that is not the case.

32
00:01:45,240 --> 00:01:49,720
 It is actually the excessive thinking that causes the

33
00:01:49,720 --> 00:01:50,800
 headaches.

34
00:01:50,800 --> 00:01:56,790
 It can also happen that when you say angry, angry, you say

35
00:01:56,790 --> 00:01:59,960
 it with anger or frustration,

36
00:01:59,960 --> 00:02:03,680
 so instead of being mindful and having a wholesome state of

37
00:02:03,680 --> 00:02:06,360
 mind, you are still biased and clinging

38
00:02:06,360 --> 00:02:09,080
 to the experience.

39
00:02:09,080 --> 00:02:13,060
 It can also happen that you do not have faith in the

40
00:02:13,060 --> 00:02:16,440
 practice, so you note with doubt, you

41
00:02:16,440 --> 00:02:21,790
 say angry, angry, but you are not focusing with confidence

42
00:02:21,790 --> 00:02:23,640
 on the experience.

43
00:02:23,640 --> 00:02:27,470
 You might just say the words and hope something good

44
00:02:27,470 --> 00:02:29,080
 happens like magic.

45
00:02:29,080 --> 00:02:33,320
 Finally, it can also happen that you are practicing

46
00:02:33,320 --> 00:02:38,280
 correctly, but your mind is not working correctly.

47
00:02:38,280 --> 00:02:41,950
 You are doing everything right, but your mind is not

48
00:02:41,950 --> 00:02:44,640
 cooperating, which means you try to

49
00:02:44,640 --> 00:02:48,120
 be mindful and you cannot be mindful.

50
00:02:48,120 --> 00:02:52,000
 You try to catch something and you catch it too late.

51
00:02:52,000 --> 00:02:57,010
 You try to clearly see something and it is already gone,

52
00:02:57,010 --> 00:03:00,540
 and as a result of these shortcomings,

53
00:03:00,540 --> 00:03:03,320
 you get frustrated.

54
00:03:03,320 --> 00:03:07,830
 The truth is that this sort of experience is a sign that

55
00:03:07,830 --> 00:03:10,640
 you are seeing clearly, seeing

56
00:03:10,640 --> 00:03:17,400
 impermanence that even your own mind is unpredictable.

57
00:03:17,400 --> 00:03:21,550
 Sometimes you can be mindful and sometimes you cannot,

58
00:03:21,550 --> 00:03:23,520
 seeing suffering as your mind

59
00:03:23,520 --> 00:03:28,780
 is not the way you want it to be and seeing non-self as you

60
00:03:28,780 --> 00:03:31,960
 cannot control your own mind.

61
00:03:31,960 --> 00:03:36,550
 While you are familiar with these qualities of reality, you

62
00:03:36,550 --> 00:03:39,040
 may be susceptible to reactions

63
00:03:39,040 --> 00:03:41,960
 like frustration.

64
00:03:41,960 --> 00:03:46,030
 Those reactions are the problem, not the inability to make

65
00:03:46,030 --> 00:03:48,960
 your experience stable, satisfying,

66
00:03:48,960 --> 00:03:52,520
 and controllable.

67
00:03:52,520 --> 00:03:57,220
 Impermanence, suffering, and non-self is the unavoidable

68
00:03:57,220 --> 00:03:59,760
 nature of reality, but we do not

69
00:03:59,760 --> 00:04:04,330
 like reality, we cannot bear it, we cannot bear imperman

70
00:04:04,330 --> 00:04:07,280
ence, we cannot bear suffering,

71
00:04:07,280 --> 00:04:10,960
 and we cannot bear non-self.

72
00:04:10,960 --> 00:04:15,790
 Even just watching your stomach rise and fall may lead to

73
00:04:15,790 --> 00:04:17,320
 frustration.

74
00:04:17,320 --> 00:04:21,620
 The experience might feel pleasant and peaceful at first,

75
00:04:21,620 --> 00:04:24,080
 but when it changes you think, "What

76
00:04:24,080 --> 00:04:26,080
 am I doing wrong?"

77
00:04:26,080 --> 00:04:30,340
 Then you try again and again to make the experience

78
00:04:30,340 --> 00:04:34,440
 pleasant and peaceful, and you become frustrated

79
00:04:34,440 --> 00:04:39,800
 thinking, "I cannot do this, this is useless."

80
00:04:39,800 --> 00:04:44,250
 But the truth is, you are doing it, the problem is not the

81
00:04:44,250 --> 00:04:47,480
 experience of impermanence, suffering,

82
00:04:47,480 --> 00:04:51,960
 and non-self, the problem is the frustration.

83
00:04:51,960 --> 00:04:56,610
 When frustration arises in the mind, you must focus on the

84
00:04:56,610 --> 00:05:00,200
 frustration, saying to yourself,

85
00:05:00,200 --> 00:05:02,680
 "Frostrated, frustrated."

86
00:05:02,680 --> 00:05:08,390
 If you focus on it objectively, you will see that just like

87
00:05:08,390 --> 00:05:11,640
 everything else, it disappears

88
00:05:11,640 --> 00:05:14,760
 in its own time.

89
00:05:14,760 --> 00:05:19,670
 Every time you get frustrated, it is a chance for you to

90
00:05:19,670 --> 00:05:22,440
 build patience, and patience is

91
00:05:22,440 --> 00:05:28,240
 an integral part of what we are trying to cultivate.

92
00:05:28,240 --> 00:05:31,800
 Patience is associated with wisdom.

93
00:05:31,800 --> 00:05:37,000
 True patience means just seeing things as they are and not

94
00:05:37,000 --> 00:05:39,240
 getting upset about them

95
00:05:39,240 --> 00:05:42,920
 when they come or go.

96
00:05:42,920 --> 00:05:47,930
 But patience and the ability to see moment to moment

97
00:05:47,930 --> 00:05:50,680
 experiences as they are.

98
00:05:50,680 --> 00:05:55,820
 Your frustration about the meditation is stopping you from

99
00:05:55,820 --> 00:05:57,040
 doing this.

100
00:05:57,040 --> 00:06:02,710
 The frustration will hinder that ability to see the moment

101
00:06:02,710 --> 00:06:05,320
 to moment experiences.

102
00:06:05,320 --> 00:06:10,000
 Frustration will hinder it, desire will hinder it, all

103
00:06:10,000 --> 00:06:12,720
 kinds of delusion will hinder it.

104
00:06:12,720 --> 00:06:18,750
 All of our judgments about our practice will stop us from

105
00:06:18,750 --> 00:06:20,800
 cultivating it.

106
00:06:20,800 --> 00:06:25,470
 If people think the practice is not working for them, this

107
00:06:25,470 --> 00:06:27,640
 is just wrong view and wrong

108
00:06:27,640 --> 00:06:29,440
 thought.

109
00:06:29,440 --> 00:06:33,380
 I have heard people tell me that they have been stuck in

110
00:06:33,380 --> 00:06:35,520
 meditation for five or even

111
00:06:35,520 --> 00:06:36,840
 ten years.

112
00:06:36,840 --> 00:06:41,810
 When someone feels like they are stuck, the first thing for

113
00:06:41,810 --> 00:06:43,760
 them to get rid of is this

114
00:06:43,760 --> 00:06:49,520
 idea that they could possibly be stuck because there is no

115
00:06:49,520 --> 00:06:52,320
 you and there is no stuck.

116
00:06:52,320 --> 00:06:57,240
 There is only an experience and it is amazing that people

117
00:06:57,240 --> 00:07:00,160
 could waste five years thinking

118
00:07:00,160 --> 00:07:03,900
 that they are stuck when in fact at any moment you can

119
00:07:03,900 --> 00:07:06,800
 become enlightened if you are clearly

120
00:07:06,800 --> 00:07:10,200
 aware of the moment.

121
00:07:10,200 --> 00:07:14,690
 All we are trying to do is cultivate that clear awareness

122
00:07:14,690 --> 00:07:16,680
 and frustration and any kind

123
00:07:16,680 --> 00:07:22,010
 of judgment about your practice will stop you from doing

124
00:07:22,010 --> 00:07:22,880
 that.

125
00:07:22,880 --> 00:07:27,680
 You are seeing the truth, you are seeing impermanence,

126
00:07:27,680 --> 00:07:30,000
 suffering and non-self.

127
00:07:30,000 --> 00:07:33,850
 It is the frustration that is the problem which is coming

128
00:07:33,850 --> 00:07:35,800
 from your inability to accept

129
00:07:35,800 --> 00:07:40,800
 those very important aspects of reality.

130
00:07:40,800 --> 00:07:45,640
 Once you can accept them and you can see that that is the

131
00:07:45,640 --> 00:07:48,160
 truth of life and this is the

132
00:07:48,160 --> 00:07:54,760
 path to enlightenment, you will see that "es samagov sudya"

133
00:07:54,760 --> 00:07:57,680
 this is the path to purification.

134
00:07:57,680 --> 00:07:58,680
 Thank you.

135
00:07:58,680 --> 00:07:59,680
 Thank you.

