1
00:00:00,000 --> 00:00:24,200
 Hey, good evening everyone.

2
00:00:24,200 --> 00:00:28,690
 One thing that we're often concerned with in Buddhism in

3
00:00:28,690 --> 00:00:33,800
 the practice and teaching of

4
00:00:33,800 --> 00:00:54,360
 the Buddha's teachings is the depth to which we understand,

5
00:00:54,360 --> 00:00:57,400
 appreciate, and attain the

6
00:00:57,400 --> 00:01:13,280
 Dhamma, the truth, the goal.

7
00:01:13,280 --> 00:01:18,570
 There's a concern that our understanding of the Dhamma

8
00:01:18,570 --> 00:01:21,360
 might be shallow and as a result

9
00:01:21,360 --> 00:01:34,720
 our attainment of the Dhamma will be lacking, inadequate,

10
00:01:34,720 --> 00:01:42,200
 limited, not bad or problematic,

11
00:01:42,200 --> 00:01:50,200
 just insufficient or limited.

12
00:01:50,200 --> 00:02:02,430
 For instance, it's a good thing to know about moral precept

13
00:02:02,430 --> 00:02:04,320
s and of course it's a good thing

14
00:02:04,320 --> 00:02:09,440
 to practice them.

15
00:02:09,440 --> 00:02:16,580
 But Sila goes so much deeper, true ethics go so much deeper

16
00:02:16,580 --> 00:02:19,600
 than just keeping rules.

17
00:02:19,600 --> 00:02:24,600
 With concentration, being focused, being understanding the

18
00:02:24,600 --> 00:02:31,920
 Buddha's teaching about being mindful

19
00:02:31,920 --> 00:02:45,960
 and dedicated, Buddhist, dedicated towards good things.

20
00:02:45,960 --> 00:02:49,960
 This is a very good quality, but of course Samadhi goes

21
00:02:49,960 --> 00:02:52,360
 much deeper than just focusing

22
00:02:52,360 --> 00:03:02,040
 or even being mindful in an ordinary sense.

23
00:03:02,040 --> 00:03:08,890
 And of course wisdom, wisdom is where we have the greatest

24
00:03:08,890 --> 00:03:12,600
 concern, the concern that our

25
00:03:12,600 --> 00:03:18,760
 understanding be more than just intellectual understanding.

26
00:03:18,760 --> 00:03:22,920
 This is important.

27
00:03:22,920 --> 00:03:27,160
 If you read a lot or you study a lot, well you come to know

28
00:03:27,160 --> 00:03:29,080
 a lot about the Buddha's

29
00:03:29,080 --> 00:03:31,080
 teaching.

30
00:03:31,080 --> 00:03:41,500
 When you come to meditate you realize all that knowledge is

31
00:03:41,500 --> 00:03:43,680
 nothing.

32
00:03:43,680 --> 00:03:46,760
 It's good, it's good to know these things.

33
00:03:46,760 --> 00:03:50,370
 It's good in a practical sense even, it can help you in

34
00:03:50,370 --> 00:03:53,040
 your life if you have an understanding

35
00:03:53,040 --> 00:04:05,400
 of all things are impermanent, this too will pass.

36
00:04:05,400 --> 00:04:08,640
 Makes you a better person, a happier person, this is a good

37
00:04:08,640 --> 00:04:09,240
 thing.

38
00:04:09,240 --> 00:04:14,420
 But we recognize there's a greater depth to be had than

39
00:04:14,420 --> 00:04:17,480
 just knowledge of the Buddha's

40
00:04:17,480 --> 00:04:18,480
 teaching.

41
00:04:18,480 --> 00:04:29,090
 A deep, true ethics is activity that is mindful, that

42
00:04:29,090 --> 00:04:34,240
 involves mindfulness.

43
00:04:34,240 --> 00:04:38,790
 When you're walking, stepping right, stepping left, it's

44
00:04:38,790 --> 00:04:40,960
 impossible to be unethical when

45
00:04:40,960 --> 00:04:53,560
 you have the mindful, clear awareness of the activity.

46
00:04:53,560 --> 00:04:58,060
 True focus of course then comes from that sort of behavior,

47
00:04:58,060 --> 00:04:59,800
 the body and mind are in

48
00:04:59,800 --> 00:05:03,680
 sync then you become very focused and it means your mind

49
00:05:03,680 --> 00:05:06,600
 actually becomes free from unwholesome

50
00:05:06,600 --> 00:05:11,200
 qualities that you start to see clearly.

51
00:05:11,200 --> 00:05:17,250
 True focus is not just being concentrated but being in

52
00:05:17,250 --> 00:05:18,400
 focus.

53
00:05:18,400 --> 00:05:21,840
 It's that which allows you to see clearly.

54
00:05:21,840 --> 00:05:24,000
 And so true wisdom is seeing clearly.

55
00:05:24,000 --> 00:05:28,720
 It's not intellectual, it doesn't have to be a thought.

56
00:05:28,720 --> 00:05:38,080
 Wisdom is light, it's like shining a light on reality.

57
00:05:38,080 --> 00:05:40,520
 So that's just an introduction.

58
00:05:40,520 --> 00:05:45,540
 What I wanted to talk tonight about was the Buddha's

59
00:05:45,540 --> 00:05:47,840
 teaching on depths.

60
00:05:47,840 --> 00:05:49,720
 The Buddha said there are six qualities.

61
00:05:49,720 --> 00:06:00,410
 This is in the Chakani part of the Guttarnikaya, Arahantav

62
00:06:00,410 --> 00:06:04,720
aga I think.

63
00:06:04,720 --> 00:06:11,720
 Mahantata sutta, Mahantata.

64
00:06:11,720 --> 00:06:14,720
 Mahantata means greatness.

65
00:06:14,720 --> 00:06:24,000
 Vipula, so the Buddha said, "Chahidamihi bhikkhu ei, Chahid

66
00:06:24,000 --> 00:06:29,720
amihi samanagato bhikkhu."

67
00:06:29,720 --> 00:06:32,870
 Bhikkhu, one who has seen the danger and clinging the

68
00:06:32,870 --> 00:06:35,240
 danger and being reborn again and again

69
00:06:35,240 --> 00:06:47,420
 in samsara, who is accomplished in six dhammas, who

70
00:06:47,420 --> 00:06:52,240
 possesses six things.

71
00:06:52,240 --> 00:07:05,060
 Nachira seva, Mahantatang, in no long time will attain to

72
00:07:05,060 --> 00:07:08,240
 greatness.

73
00:07:08,240 --> 00:07:17,780
 Vipulata, Vipula, Vipula comes from Vipula which means deep

74
00:07:17,780 --> 00:07:20,240
, profound.

75
00:07:20,240 --> 00:07:29,240
 Vipulata means depth or profundity.

76
00:07:29,240 --> 00:07:39,550
 So the idea here is attaining to success and greatness in

77
00:07:39,550 --> 00:07:41,240
 the Dhamma.

78
00:07:41,240 --> 00:07:45,240
 Papunati dhammi, so in regards to the teachings, in regards

79
00:07:45,240 --> 00:07:47,240
 to the Dhamma, in regards to the

80
00:07:47,240 --> 00:07:53,240
 truth, reality.

81
00:07:53,240 --> 00:07:59,240
 It's not about right or wrong, it's about greatness or

82
00:07:59,240 --> 00:08:02,240
 ordinary.

83
00:08:02,240 --> 00:08:06,840
 Ordinary people are not bad necessarily, but there's

84
00:08:06,840 --> 00:08:08,240
 nothing great or wonderful about being

85
00:08:08,240 --> 00:08:11,240
 reborn again and again.

86
00:08:11,240 --> 00:08:16,240
 Even evil people who go to hell and so on.

87
00:08:16,240 --> 00:08:19,750
 It's not so much that it's wrong, it's just that it's ignob

88
00:08:19,750 --> 00:08:20,240
le.

89
00:08:20,240 --> 00:08:25,440
 There's nothing great about indulging in sensuality, there

90
00:08:25,440 --> 00:08:27,240
's nothing great about being rich or

91
00:08:27,240 --> 00:08:30,240
 even famous, even being king of the whole world.

92
00:08:30,240 --> 00:08:34,240
 It's not that great in the end.

93
00:08:34,240 --> 00:08:38,240
 Going to heaven, becoming a Brahma, it's getting greater.

94
00:08:38,240 --> 00:08:42,240
 There's a greatness to it, but it's not really great.

95
00:08:42,240 --> 00:08:46,240
 It's not really deep or profound.

96
00:08:46,240 --> 00:08:51,190
 What's profound is breaking free from the cycle, finding

97
00:08:51,190 --> 00:08:54,240
 something that is lasting,

98
00:08:54,240 --> 00:08:58,240
 stable, secure, safe.

99
00:08:58,240 --> 00:09:02,410
 Because even being born a Brahma is not safe or secure, let

100
00:09:02,410 --> 00:09:04,240
 alone being rich or famous

101
00:09:04,240 --> 00:09:12,240
 or indulging in pleasures.

102
00:09:12,240 --> 00:09:15,690
 So when we come to practice meditation, I think this is of

103
00:09:15,690 --> 00:09:17,240
 great interest to us.

104
00:09:17,240 --> 00:09:21,930
 How do we not just succeed, but how do we gain greater

105
00:09:21,930 --> 00:09:24,240
 depth in our practice?

106
00:09:24,240 --> 00:09:29,240
 I think it's a question for all Buddhist practitioners.

107
00:09:29,240 --> 00:09:32,450
 It might be practicing for many years, but it might be

108
00:09:32,450 --> 00:09:33,240
 shallow.

109
00:09:33,240 --> 00:09:36,240
 How do we ensure that our practice goes deeper?

110
00:09:36,240 --> 00:09:40,970
 So these are qualities, like many of these lists of

111
00:09:40,970 --> 00:09:43,240
 qualities, they're a teaching.

112
00:09:43,240 --> 00:09:45,600
 The idea is as you listen to the teaching, you're

113
00:09:45,600 --> 00:09:51,240
 practicing and you're taking these in,

114
00:09:51,240 --> 00:09:58,510
 internalizing and simulating these teachings, applying them

115
00:09:58,510 --> 00:10:01,240
 to yourself,

116
00:10:01,240 --> 00:10:08,990
 using them not just for theory, but as an encouragement and

117
00:10:08,990 --> 00:10:11,240
 direction

118
00:10:11,240 --> 00:10:14,240
 for greater and more profound practice.

119
00:10:14,240 --> 00:10:17,240
 So there are six of them.

120
00:10:17,240 --> 00:10:21,240
 And the Buddha used interesting words.

121
00:10:21,240 --> 00:10:23,240
 If you don't know Pali, it might not be that interesting,

122
00:10:23,240 --> 00:10:26,240
 but I have to explain because it's a little bit poetic.

123
00:10:26,240 --> 00:10:30,240
 So the first one is Aloka Bahulo.

124
00:10:30,240 --> 00:10:34,240
 Aloka is light.

125
00:10:34,240 --> 00:10:38,240
 And so he didn't explain these.

126
00:10:38,240 --> 00:10:40,240
 We have to understand them.

127
00:10:40,240 --> 00:10:42,240
 What did he mean by light?

128
00:10:42,240 --> 00:10:44,240
 And you have to know something about the Buddha's teaching

129
00:10:44,240 --> 00:10:44,240
 to understand,

130
00:10:44,240 --> 00:10:48,240
 but I already gave it away, that light of wisdom.

131
00:10:48,240 --> 00:10:52,950
 Aloka means you have to have wisdom, but rather than say

132
00:10:52,950 --> 00:10:55,240
 wisdom, he said light.

133
00:10:55,240 --> 00:11:00,730
 It's a certain kind of wisdom. It's not seeing things with

134
00:11:00,730 --> 00:11:01,240
 your eyes, of course,

135
00:11:01,240 --> 00:11:07,890
 but it's the feeling of clarity and opening your eyes and

136
00:11:07,890 --> 00:11:13,240
 leaving the darkness behind.

137
00:11:13,240 --> 00:11:18,240
 It's like waking up from a sleep almost.

138
00:11:18,240 --> 00:11:21,600
 When you open your eyes and suddenly realize that you're

139
00:11:21,600 --> 00:11:22,240
 bumping into things

140
00:11:22,240 --> 00:11:32,240
 and why you're in so much pain is because you were blind.

141
00:11:32,240 --> 00:11:36,240
 Aloka Bahulo, one who has greatness of wisdom.

142
00:11:36,240 --> 00:11:40,600
 So in meditation it's important that your focus, your

143
00:11:40,600 --> 00:11:43,240
 attention is on understanding.

144
00:11:43,240 --> 00:11:46,710
 It's not magic where you just repeat words to yourself and

145
00:11:46,710 --> 00:11:50,240
 you become enlightened.

146
00:11:50,240 --> 00:11:53,240
 You want to really try to see and understand.

147
00:11:53,240 --> 00:11:59,240
 Of course, it's not like a book where you can just...

148
00:11:59,240 --> 00:12:01,240
 Well, maybe it kind of is like a book.

149
00:12:01,240 --> 00:12:07,240
 If you want to simile with a book, you have to read it.

150
00:12:07,240 --> 00:12:11,240
 You can't force the knowledge out of the pages.

151
00:12:11,240 --> 00:12:15,240
 You can't pick up the book and stare more intently,

152
00:12:15,240 --> 00:12:21,240
 you stare and the more knowledge you gain.

153
00:12:21,240 --> 00:12:23,240
 Meditation is kind of like reading a book.

154
00:12:23,240 --> 00:12:26,890
 You have to go page by page and the knowledge comes on its

155
00:12:26,890 --> 00:12:27,240
 own.

156
00:12:27,240 --> 00:12:32,240
 It comes in its own time.

157
00:12:32,240 --> 00:12:36,640
 So rather than trying to force or magically make knowledge

158
00:12:36,640 --> 00:12:37,240
 arise

159
00:12:37,240 --> 00:12:45,250
 or even our idea that somehow you can make knowledge arise

160
00:12:45,250 --> 00:12:46,240
 if you analyze,

161
00:12:46,240 --> 00:12:48,240
 if you look closely enough.

162
00:12:48,240 --> 00:12:51,980
 Rather than analyzing the book to get its knowledge, you

163
00:12:51,980 --> 00:12:54,240
 have to read it.

164
00:12:54,240 --> 00:12:58,240
 So in meditation you need...

165
00:12:58,240 --> 00:13:02,770
 You can't create the knowledge, but you have to be intent

166
00:13:02,770 --> 00:13:03,240
 upon it

167
00:13:03,240 --> 00:13:10,510
 and you have to be clear in your mind about the nature of

168
00:13:10,510 --> 00:13:11,240
 things.

169
00:13:11,240 --> 00:13:13,240
 You have to have that as your goal.

170
00:13:13,240 --> 00:13:16,240
 As you watch, you're trying to confront things.

171
00:13:16,240 --> 00:13:20,640
 You have clear theory to back it up that's setting you in

172
00:13:20,640 --> 00:13:21,240
 the right direction,

173
00:13:21,240 --> 00:13:29,240
 what we intend to see in permanence, suffering and not so.

174
00:13:29,240 --> 00:13:34,240
 It's important to be clever in the practice

175
00:13:34,240 --> 00:13:40,240
 and not get caught up in doubts and debates and analysis.

176
00:13:40,240 --> 00:13:47,150
 Often our search for knowledge gets in the way of our

177
00:13:47,150 --> 00:13:49,240
 attainment of wisdom.

178
00:13:49,240 --> 00:13:53,240
 So it's important that we put that aside when we practice

179
00:13:53,240 --> 00:13:59,240
 and focus just on seeing clearly.

180
00:13:59,240 --> 00:14:02,440
 It's like if you want wisdom you have to put aside

181
00:14:02,440 --> 00:14:03,240
 knowledge.

182
00:14:03,240 --> 00:14:09,240
 You have to put aside any kind of analysis you might have.

183
00:14:09,240 --> 00:14:13,940
 Just open your eyes, learn how to open your eyes and how to

184
00:14:13,940 --> 00:14:15,240
 see.

185
00:14:15,240 --> 00:14:18,240
 Light, you need to have great light.

186
00:14:18,240 --> 00:14:21,240
 You have to dispel the darkness.

187
00:14:21,240 --> 00:14:25,240
 Ignore, avoid the darkness which is all kinds of judgments

188
00:14:25,240 --> 00:14:28,240
 and doubts, confusion.

189
00:14:28,240 --> 00:14:31,240
 Don't let yourself get confused or caught up in views.

190
00:14:31,240 --> 00:14:36,240
 Views, of course, very dangerous.

191
00:14:36,240 --> 00:14:39,380
 Sometimes we judge a teaching and that gets in the way of

192
00:14:39,380 --> 00:14:41,240
 appreciating the truth

193
00:14:41,240 --> 00:14:45,240
 or any goodness that might be in it.

194
00:14:45,240 --> 00:14:49,240
 Alok, a bahulu, wisdom, of course, most important.

195
00:14:49,240 --> 00:14:51,240
 If you want depth you have to gain wisdom.

196
00:14:51,240 --> 00:14:55,240
 Your practice can be just tranquility.

197
00:14:55,240 --> 00:14:59,760
 If you're fixed or focused on the pleasant, peaceful

198
00:14:59,760 --> 00:15:04,240
 aspects of meditation, for example,

199
00:15:04,240 --> 00:15:06,240
 you will never get depths of practice.

200
00:15:06,240 --> 00:15:09,430
 Focusing on samatha meditation, focusing on peace or

201
00:15:09,430 --> 00:15:13,240
 happiness or metta meditation, which is good.

202
00:15:13,240 --> 00:15:16,240
 All of which is good. Samatha is also good.

203
00:15:16,240 --> 00:15:19,850
 But there's a lack of depth. Your depth will never come

204
00:15:19,850 --> 00:15:22,240
 until you gain wisdom and understanding,

205
00:15:22,240 --> 00:15:27,240
 until you see clearly.

206
00:15:27,240 --> 00:15:30,240
 Number two, yoga bahulu.

207
00:15:30,240 --> 00:15:32,240
 Yoga is a strange one.

208
00:15:32,240 --> 00:15:38,240
 But I didn't use the word yoga that much.

209
00:15:38,240 --> 00:15:41,240
 But here he uses the word yoga.

210
00:15:41,240 --> 00:15:46,240
 One should have much yoga, be full of yoga.

211
00:15:46,240 --> 00:15:51,240
 So we have to understand the word yoga.

212
00:15:51,240 --> 00:15:54,240
 It's not difficult to understand, but it meant something

213
00:15:54,240 --> 00:16:00,240
 quite different from how we use the word in modern society.

214
00:16:00,240 --> 00:16:05,860
 So in ancient times yoga was a word they used for spiritual

215
00:16:05,860 --> 00:16:07,240
 practice.

216
00:16:07,240 --> 00:16:11,240
 The word yoga means something like a yoke.

217
00:16:11,240 --> 00:16:17,040
 But not the yoke of an egg. It means a yoke as in a piece

218
00:16:17,040 --> 00:16:23,240
 of wood that an ox would put on the back of the ox

219
00:16:23,240 --> 00:16:28,240
 and it allows it to pull the plow.

220
00:16:28,240 --> 00:16:34,240
 So it's like a harness, a yoke.

221
00:16:34,240 --> 00:16:38,240
 And so the idea was you would be dedicated or committed.

222
00:16:38,240 --> 00:16:43,240
 Yoga was this commitment, spiritual commitment.

223
00:16:43,240 --> 00:16:46,240
 That's how I like to translate the word religion.

224
00:16:46,240 --> 00:16:50,750
 This word religion gets such a bad reputation because of

225
00:16:50,750 --> 00:16:52,240
 how it's been used.

226
00:16:52,240 --> 00:16:56,390
 But when it's used in the sense of being religious about

227
00:16:56,390 --> 00:16:57,240
 something,

228
00:16:57,240 --> 00:17:00,240
 in the sense of being dedicated to something intent upon it

229
00:17:00,240 --> 00:17:00,240
,

230
00:17:00,240 --> 00:17:03,240
 I think that's very much what yoga means.

231
00:17:03,240 --> 00:17:08,010
 You'll often, I'm not sure so much how much the Buddha used

232
00:17:08,010 --> 00:17:08,240
 it,

233
00:17:08,240 --> 00:17:11,730
 but in the commentaries you'll often hear this word yoga v

234
00:17:11,730 --> 00:17:13,240
achara again and again.

235
00:17:13,240 --> 00:17:16,240
 We're reading the Melinda Panna.

236
00:17:16,240 --> 00:17:20,870
 And again and again he refers to a meditator as a yoga vach

237
00:17:20,870 --> 00:17:21,240
ara,

238
00:17:21,240 --> 00:17:26,240
 which means someone basically who is committed to yoga,

239
00:17:26,240 --> 00:17:30,240
 committed to a religious spiritual practice,

240
00:17:30,240 --> 00:17:36,010
 someone who is cultivating or is practicing in a spiritual

241
00:17:36,010 --> 00:17:36,240
 way,

242
00:17:36,240 --> 00:17:38,240
 dedicated way.

243
00:17:38,240 --> 00:17:40,240
 Like we're all doing here.

244
00:17:40,240 --> 00:17:42,240
 You're dedicated to your practice.

245
00:17:42,240 --> 00:17:46,470
 So yoga means like dedication, exertion I think it came to

246
00:17:46,470 --> 00:17:47,240
 mean.

247
00:17:47,240 --> 00:17:52,240
 So it probably has implications in regards to effort.

248
00:17:52,240 --> 00:17:56,240
 I could say an important part of yoga is the effort.

249
00:17:56,240 --> 00:18:00,260
 And effort in Buddhism really isn't about pushing yourself

250
00:18:00,260 --> 00:18:01,240
 harder and harder,

251
00:18:01,240 --> 00:18:03,240
 not so much.

252
00:18:03,240 --> 00:18:09,240
 It really is about this intention or exertion.

253
00:18:09,240 --> 00:18:12,240
 And dedication, in a good word.

254
00:18:12,240 --> 00:18:16,780
 And being dedicated to something you try again, try and try

255
00:18:16,780 --> 00:18:17,240
 again,

256
00:18:17,240 --> 00:18:20,240
 never give up.

257
00:18:20,240 --> 00:18:22,240
 It doesn't mean you have to push harder all the time.

258
00:18:22,240 --> 00:18:27,490
 Just when you feel yourself, they say tātā which means sl

259
00:18:27,490 --> 00:18:31,240
acking or flagging.

260
00:18:31,240 --> 00:18:35,240
 You just feel like you're lacking in energy.

261
00:18:35,240 --> 00:18:38,240
 You don't have to force yourself.

262
00:18:38,240 --> 00:18:42,240
 You just have to try.

263
00:18:42,240 --> 00:18:46,740
 And that dedication, and even when you can't walk so you

264
00:18:46,740 --> 00:18:47,240
 stand,

265
00:18:47,240 --> 00:18:53,240
 you can't stand your state.

266
00:18:53,240 --> 00:18:55,240
 You keep trying.

267
00:18:55,240 --> 00:18:58,240
 Yoga bahulō.

268
00:18:58,240 --> 00:19:02,240
 Number three, veda bahulō.

269
00:19:02,240 --> 00:19:05,830
 Now suppose with yoga you'd have to confine it more to the

270
00:19:05,830 --> 00:19:08,240
 effort and dedication side

271
00:19:08,240 --> 00:19:13,240
 because veda is another aspect of religion.

272
00:19:13,240 --> 00:19:17,240
 Veda is this religious feeling, veda, vedana kind of.

273
00:19:17,240 --> 00:19:23,240
 But here it refers to, I think it refers to sanvega,

274
00:19:23,240 --> 00:19:28,240
 the idea of the urgency of the practice.

275
00:19:28,240 --> 00:19:31,240
 If you want to really go deeper with your practice,

276
00:19:31,240 --> 00:19:35,740
 it has to take on a sense of urgency, not just done as a

277
00:19:35,740 --> 00:19:40,240
 hobby or a pastime,

278
00:19:40,240 --> 00:19:44,240
 not even just as a matter of course.

279
00:19:44,240 --> 00:19:51,240
 The Buddha said you should work to eradicate wrong view,

280
00:19:51,240 --> 00:19:58,240
 the view of self, view of me and mine.

281
00:19:58,240 --> 00:20:02,510
 He said you should work like a person with their head on

282
00:20:02,510 --> 00:20:03,240
 fire.

283
00:20:03,240 --> 00:20:06,790
 Suppose you're wearing a turban, I guess, and it was on

284
00:20:06,790 --> 00:20:07,240
 fire.

285
00:20:07,240 --> 00:20:11,240
 You'd be pretty darned intent upon,

286
00:20:11,240 --> 00:20:15,990
 you'd have a sense of urgency in regards to putting out

287
00:20:15,990 --> 00:20:17,240
 that fire.

288
00:20:17,240 --> 00:20:24,240
 The fire of wrong view is much more dangerous.

289
00:20:24,240 --> 00:20:27,160
 We don't know what's going to happen in the future where we

290
00:20:27,160 --> 00:20:28,240
're going.

291
00:20:28,240 --> 00:20:32,240
 If we die without any goodness in our hearts,

292
00:20:32,240 --> 00:20:35,240
 we could be reborn in a very unpleasant place.

293
00:20:35,240 --> 00:20:38,240
 We could even simply be born as an animal,

294
00:20:38,240 --> 00:20:41,240
 which would be quite discouraging.

295
00:20:41,240 --> 00:20:45,240
 It's very hard for an animal to be reborn as a human being.

296
00:20:45,240 --> 00:20:48,240
 Much more common for them to be reborn as the same animal

297
00:20:48,240 --> 00:20:49,240
 again and again and again.

298
00:20:49,240 --> 00:20:54,700
 It's just very difficult to do the good activities that

299
00:20:54,700 --> 00:20:57,240
 cultivate good qualities

300
00:20:57,240 --> 00:21:04,240
 that are required to be born a human.

301
00:21:04,240 --> 00:21:06,240
 So a sense of urgency.

302
00:21:06,240 --> 00:21:08,510
 We're going to get old, we're going to get sick, we're

303
00:21:08,510 --> 00:21:09,240
 going to die.

304
00:21:09,240 --> 00:21:11,240
 Bad things could happen any time.

305
00:21:11,240 --> 00:21:13,240
 We're not ready for them.

306
00:21:13,240 --> 00:21:24,240
 It's very easy to get on the bad, the wrong, the bad path.

307
00:21:24,240 --> 00:21:27,240
 Veda Bahulam.

308
00:21:27,240 --> 00:21:32,240
 Number four, Asantuti Bahulam.

309
00:21:32,240 --> 00:21:37,240
 Asantuti is contentment, which is actually funny enough,

310
00:21:37,240 --> 00:21:41,240
 it's a very good quality in Buddhism.

311
00:21:41,240 --> 00:21:45,240
 The Buddha said you should be santuti.

312
00:21:45,240 --> 00:21:50,240
 It's the greatest blessing to be content.

313
00:21:50,240 --> 00:21:57,240
 And so the Buddha taught both contentment and discontent.

314
00:21:57,240 --> 00:22:02,430
 Contentment, we should be content with food, whatever food

315
00:22:02,430 --> 00:22:03,240
 we get.

316
00:22:03,240 --> 00:22:06,450
 We shouldn't try too hard to get good food, delicious food

317
00:22:06,450 --> 00:22:07,240
 anyway.

318
00:22:07,240 --> 00:22:10,240
 You should be a little concerned perhaps about healthy food

319
00:22:10,240 --> 00:22:13,240
, not eating junk.

320
00:22:13,240 --> 00:22:15,240
 You shouldn't be obsessed with it.

321
00:22:15,240 --> 00:22:17,240
 You should be content with whatever you have.

322
00:22:17,240 --> 00:22:22,240
 Clothing should be content with dwelling.

323
00:22:22,240 --> 00:22:26,850
 We're having a debate over allowing meditators to switch

324
00:22:26,850 --> 00:22:27,240
 rooms

325
00:22:27,240 --> 00:22:31,240
 or whether we have to force them to be in the room.

326
00:22:31,240 --> 00:22:32,240
 I don't think we do.

327
00:22:32,240 --> 00:22:37,240
 I think changing rooms sometimes is important.

328
00:22:37,240 --> 00:22:41,510
 But I think it's true quite often is just because, oh, that

329
00:22:41,510 --> 00:22:42,240
's a nicer room,

330
00:22:42,240 --> 00:22:46,240
 or this room is hot, this room is cold.

331
00:22:46,240 --> 00:22:49,240
 What you have to be careful about, don't get too caught up

332
00:22:49,240 --> 00:22:50,240
 in luxury

333
00:22:50,240 --> 00:23:00,240
 or following your preferences and that sort of thing.

334
00:23:00,240 --> 00:23:04,900
 So contentment is good, contentment with lodging, with

335
00:23:04,900 --> 00:23:06,240
 medicine,

336
00:23:06,240 --> 00:23:10,630
 not being too caught up in being too healthy or worrying

337
00:23:10,630 --> 00:23:11,240
 about your health,

338
00:23:11,240 --> 00:23:15,240
 worrying about pains or this sort of thing.

339
00:23:15,240 --> 00:23:19,240
 But asantuti is of course very important as well

340
00:23:19,240 --> 00:23:21,240
 because it's easy to become complacent.

341
00:23:21,240 --> 00:23:26,580
 And this is in regards to practice, in regards to the Dham

342
00:23:26,580 --> 00:23:27,240
ma.

343
00:23:27,240 --> 00:23:31,240
 Don't be content with your gains.

344
00:23:31,240 --> 00:23:34,240
 Don't rest on your laurels.

345
00:23:34,240 --> 00:23:37,240
 We gain a lot from the meditation practice.

346
00:23:37,240 --> 00:23:38,240
 It should be gaining every day.

347
00:23:38,240 --> 00:23:43,210
 Every time you sit down, in fact, you should be gaining

348
00:23:43,210 --> 00:23:44,240
 something.

349
00:23:44,240 --> 00:23:49,240
 It's easy when you gain something fairly significant

350
00:23:49,240 --> 00:23:53,240
 or what seems significant, you become complacent.

351
00:23:53,240 --> 00:23:56,460
 Even asotapan, asakiragami, there were even anagamis that

352
00:23:56,460 --> 00:23:57,240
 were complacent.

353
00:23:57,240 --> 00:24:02,240
 And the Buddha said, "You guys are on the wrong path."

354
00:24:02,240 --> 00:24:05,240
 He said, "You're not really followers of mine, anagami."

355
00:24:05,240 --> 00:24:08,240
 He didn't probably say it like that, but he said,

356
00:24:08,240 --> 00:24:13,240
 "You're not doing the right thing by being complacent."

357
00:24:13,240 --> 00:24:18,240
 Hanagami is someone who has come very far in the practice.

358
00:24:18,240 --> 00:24:22,240
 And the Buddha still said, "Don't be complacent.

359
00:24:22,240 --> 00:24:28,240
 Don't be content with just that."

360
00:24:28,240 --> 00:24:31,240
 Number five, anikita-duro.

361
00:24:31,240 --> 00:24:37,240
 Anikita means to throw away or to put down, I guess.

362
00:24:37,240 --> 00:24:41,240
 Anikita.

363
00:24:41,240 --> 00:24:44,240
 Don't put down the work.

364
00:24:44,240 --> 00:24:46,240
 Dura. Dura means duty.

365
00:24:46,240 --> 00:24:52,240
 Don't shirk your duties or let them slide.

366
00:24:52,240 --> 00:24:55,240
 It really means don't stop practicing.

367
00:24:55,240 --> 00:24:59,240
 There are two dura, two dutis in Buddhism.

368
00:24:59,240 --> 00:25:02,240
 Gantatura, which is the duty of study.

369
00:25:02,240 --> 00:25:06,240
 And Vipassanandura, which is the duty to see clearly.

370
00:25:06,240 --> 00:25:10,240
 Don't shirk either of these.

371
00:25:10,240 --> 00:25:11,240
 You should study.

372
00:25:11,240 --> 00:25:16,240
 Even just listening to the Dhamma, that's how you study.

373
00:25:16,240 --> 00:25:18,240
 It's a great way to study.

374
00:25:18,240 --> 00:25:25,240
 Reading books can also be useful.

375
00:25:25,240 --> 00:25:26,240
 But most importantly, of course,

376
00:25:26,240 --> 00:25:30,240
 don't put down the practice of mindfulness.

377
00:25:30,240 --> 00:25:33,240
 This means that, of course, in life,

378
00:25:33,240 --> 00:25:36,240
 don't let years go by where you've stopped meditating

379
00:25:36,240 --> 00:25:38,240
 just because you're complacent.

380
00:25:38,240 --> 00:25:39,240
 It could be many reasons.

381
00:25:39,240 --> 00:25:43,240
 It doesn't have to be because you're content or complacent.

382
00:25:43,240 --> 00:25:47,240
 It's easy to get caught up in work or relationships, life,

383
00:25:47,240 --> 00:25:49,240
 travel,

384
00:25:49,240 --> 00:25:57,240
 any of the ten balipoda, the ten impediments to practice.

385
00:25:57,240 --> 00:25:59,240
 Don't let them get in your way.

386
00:25:59,240 --> 00:26:01,240
 Don't put down the practice.

387
00:26:01,240 --> 00:26:04,690
 You read in the Satipatanas with the commentary about on A

388
00:26:04,690 --> 00:26:06,240
um's Round,

389
00:26:06,240 --> 00:26:10,240
 the monks would take every step mindfully.

390
00:26:10,240 --> 00:26:14,610
 And if they took one step without mindfulness, they would

391
00:26:14,610 --> 00:26:15,240
 stop.

392
00:26:15,240 --> 00:26:18,240
 And the monk behind them would know they were unmindful.

393
00:26:18,240 --> 00:26:22,240
 And so it was quite, it kept you quite vigilant, actually,

394
00:26:22,240 --> 00:26:25,590
 because if you stopped, then the people behind you would

395
00:26:25,590 --> 00:26:27,240
 know that you're not mindful.

396
00:26:27,240 --> 00:26:29,240
 So you would have tried your best not to have to stop.

397
00:26:29,240 --> 00:26:33,480
 So the monks behind you wouldn't know or wouldn't criticize

398
00:26:33,480 --> 00:26:34,240
 that well.

399
00:26:34,240 --> 00:26:41,400
 You wouldn't feel ashamed of making the monks behind you

400
00:26:41,400 --> 00:26:42,240
 stop.

401
00:26:42,240 --> 00:26:46,240
 Even on Aum's Round, they went on Aum's Round.

402
00:26:46,240 --> 00:26:49,240
 They would be mindful.

403
00:26:49,240 --> 00:26:50,240
 They would put it down.

404
00:26:50,240 --> 00:26:58,240
 You have to put it down sometimes when you teach.

405
00:26:58,240 --> 00:27:02,240
 Even when you study, perhaps.

406
00:27:02,240 --> 00:27:05,760
 When you're talking with people, you have difficulty when

407
00:27:05,760 --> 00:27:07,240
 you're eating.

408
00:27:07,240 --> 00:27:10,240
 But you shouldn't, you should try your best not to.

409
00:27:10,240 --> 00:27:12,240
 Even when eating, try to be mindful.

410
00:27:12,240 --> 00:27:16,310
 Even when talking, you can, to some extent anyway, be

411
00:27:16,310 --> 00:27:17,240
 mindful.

412
00:27:17,240 --> 00:27:20,240
 The physical act of talking is just a physical act,

413
00:27:20,240 --> 00:27:25,220
 and you can be aware of your lips moving, of the feelings,

414
00:27:25,220 --> 00:27:27,240
 and so on.

415
00:27:27,240 --> 00:27:32,240
 Anikita duro, don't put down the practice,

416
00:27:32,240 --> 00:27:35,690
 not just in terms of days, weeks, months, years, but in

417
00:27:35,690 --> 00:27:37,240
 terms of moments.

418
00:27:37,240 --> 00:27:42,230
 Throughout the day you're here, you have the opportunity to

419
00:27:42,230 --> 00:27:44,240
 take this to its greatest extent.

420
00:27:44,240 --> 00:27:47,240
 Not even sleeping. You know, you sleep less.

421
00:27:47,240 --> 00:27:49,240
 Anikita, you don't ever put it down.

422
00:27:49,240 --> 00:27:52,750
 If you do that, it's not that it's wrong to sleep or

423
00:27:52,750 --> 00:27:53,240
 anything,

424
00:27:53,240 --> 00:28:01,060
 but the greatness that comes from constant and continuous

425
00:28:01,060 --> 00:28:05,240
 exertion, patience,

426
00:28:05,240 --> 00:28:08,240
 and clarity of mind. This is greatness.

427
00:28:08,240 --> 00:28:14,480
 This is where depth comes. You take your practice to a

428
00:28:14,480 --> 00:28:17,240
 deeper level.

429
00:28:17,240 --> 00:28:19,240
 So that's what, that's number five.

430
00:28:19,240 --> 00:28:27,240
 Number six is kusalei su dhamme su uttaricha patareti.

431
00:28:27,240 --> 00:28:35,970
 Number six is kusalei su dhamme su in regards to wholesome

432
00:28:35,970 --> 00:28:40,240
 things, good things.

433
00:28:40,240 --> 00:28:53,620
 Uttaricha patareti. Patareti, coming to or attaining to,

434
00:28:53,620 --> 00:28:55,240
 attaining.

435
00:28:55,240 --> 00:29:02,240
 Uttarim means greater. Uttarim, more and more.

436
00:29:02,240 --> 00:29:09,240
 It's in line with not being content, not being complacent.

437
00:29:09,240 --> 00:29:11,240
 The exertion greatness,

438
00:29:11,240 --> 00:29:17,190
 which is the Buddha's words, has to do with going further

439
00:29:17,190 --> 00:29:18,240
 and further.

440
00:29:18,240 --> 00:29:26,900
 Not just having, not just being content with keeping moral

441
00:29:26,900 --> 00:29:28,240
 precepts,

442
00:29:28,240 --> 00:29:33,240
 but trying to be ethical in all of your movement.

443
00:29:33,240 --> 00:29:36,240
 It's about going deeper and finding concentration,

444
00:29:36,240 --> 00:29:40,240
 about finding wisdom and deeper wisdom.

445
00:29:40,240 --> 00:29:47,860
 It's about not holding on to insights that you gain from

446
00:29:47,860 --> 00:29:50,240
 the practice.

447
00:29:50,240 --> 00:29:52,510
 It's about the fact that practice is always changing, is

448
00:29:52,510 --> 00:29:55,240
 always going deeper.

449
00:29:55,240 --> 00:29:59,030
 It's always going further. There's always more to learn

450
00:29:59,030 --> 00:30:05,240
 until you completely free yourself from suffering.

451
00:30:05,240 --> 00:30:10,390
 It's an activity that's always about, always adapting,

452
00:30:10,390 --> 00:30:12,240
 always changing.

453
00:30:12,240 --> 00:30:14,620
 The benefits and the knowledge and the understanding of the

454
00:30:14,620 --> 00:30:18,240
 practice that you gained even today

455
00:30:18,240 --> 00:30:22,320
 will not perfectly apply tomorrow because you have to go

456
00:30:22,320 --> 00:30:23,240
 further.

457
00:30:23,240 --> 00:30:26,960
 One of the important challenges of meditation is it's not a

458
00:30:26,960 --> 00:30:28,240
 constant thing.

459
00:30:28,240 --> 00:30:31,630
 So like Samatha meditation, where the object's always going

460
00:30:31,630 --> 00:30:32,240
 to be the same

461
00:30:32,240 --> 00:30:36,240
 and your relation to it is just about refining and refining

462
00:30:36,240 --> 00:30:36,240
.

463
00:30:36,240 --> 00:30:42,150
 In insight, there's a refining that goes on, but there's

464
00:30:42,150 --> 00:30:46,240
 also an adapting and a going deeper.

465
00:30:46,240 --> 00:30:53,240
 Going further, our relationship with our neurotic behaviors

466
00:30:53,240 --> 00:31:00,240
, our depression, anxiety, worry, greed,

467
00:31:00,240 --> 00:31:06,480
 we should constantly be coming to it with a fresh

468
00:31:06,480 --> 00:31:08,240
 perspective,

469
00:31:08,240 --> 00:31:16,440
 ready to see new things, ready to go deeper, ready to go

470
00:31:16,440 --> 00:31:18,240
 further.

471
00:31:18,240 --> 00:31:21,240
 So these are the six.

472
00:31:21,240 --> 00:31:24,240
 Mahantatam, Mahantatadam.

473
00:31:24,240 --> 00:31:30,240
 Damas of greatness in depth, waypulata.

474
00:31:30,240 --> 00:31:33,240
 You want your practice to go deeper.

475
00:31:33,240 --> 00:31:38,240
 That's just another way of describing, teaching,

476
00:31:38,240 --> 00:31:44,240
 encouraging for us to dedicate ourselves to the practice.

477
00:31:44,240 --> 00:31:47,240
 So that's the dhamma for tonight.

478
00:31:47,240 --> 00:31:50,240
 Thank you for coming to listen.

479
00:31:50,240 --> 00:31:56,240
 Thank you.

480
00:31:57,240 --> 00:32:03,240
 Thank you.

