1
00:00:00,000 --> 00:00:11,760
 Okay, good evening everyone.

2
00:00:11,760 --> 00:00:21,280
 Welcome to our evening dhamma session.

3
00:00:21,280 --> 00:00:28,360
 We come together to practice and to reflect on our practice

4
00:00:28,360 --> 00:00:29,520
 and to learn more about the

5
00:00:29,520 --> 00:00:30,520
 practice.

6
00:00:30,520 --> 00:00:36,520
 That's what it's all about.

7
00:00:36,520 --> 00:00:43,320
 Any different kinds of learning.

8
00:00:43,320 --> 00:00:55,020
 We're learning theory, how to practice technique, and we're

9
00:00:55,020 --> 00:00:58,360
 learning about reality, learning

10
00:00:58,360 --> 00:01:10,760
 about ourselves, how our minds work.

11
00:01:10,760 --> 00:01:16,740
 Of course the question that often comes up is why are we

12
00:01:16,740 --> 00:01:18,560
 practicing?

13
00:01:18,560 --> 00:01:22,920
 It's a question that might come up during your practice.

14
00:01:22,920 --> 00:01:27,280
 Why am I here again?

15
00:01:27,280 --> 00:01:35,560
 Sometimes we have superficial reasons.

16
00:01:35,560 --> 00:01:39,520
 Sometimes our reasons change as we practice.

17
00:01:39,520 --> 00:01:43,890
 It's common for people to practice meditation, thinking it

18
00:01:43,890 --> 00:01:46,000
's going to be a quick fix, that

19
00:01:46,000 --> 00:01:57,880
 it'll be calming, peaceful.

20
00:01:57,880 --> 00:02:01,960
 Allow us to control our unruly minds.

21
00:02:01,960 --> 00:02:13,040
 Maybe even experience something profound or spiritual like

22
00:02:13,040 --> 00:02:19,880
 having visions or maybe we'll

23
00:02:19,880 --> 00:02:26,120
 have ecstatic feelings.

24
00:02:26,120 --> 00:02:30,520
 Some people even practice meditation to gain magical powers

25
00:02:30,520 --> 00:02:32,760
 or super mundane experiences

26
00:02:32,760 --> 00:02:38,480
 like seeing heaven or seeing hell or seeing angels or

27
00:02:38,480 --> 00:02:39,680
 ghosts.

28
00:02:39,680 --> 00:02:44,760
 Many people practice hoping to remember past lives.

29
00:02:44,760 --> 00:02:52,290
 But all of these are, it's difficult to maintain the desire

30
00:02:52,290 --> 00:02:56,640
 to practice because many of these,

31
00:02:56,640 --> 00:03:01,250
 or most of these reasons for practicing aren't very

32
00:03:01,250 --> 00:03:02,600
 practical.

33
00:03:02,600 --> 00:03:04,960
 They don't.

34
00:03:04,960 --> 00:03:10,230
 They don't have a profound impact on your state of mind,

35
00:03:10,230 --> 00:03:12,200
 your contentment.

36
00:03:12,200 --> 00:03:19,480
 They aren't fulfilling.

37
00:03:19,480 --> 00:03:27,260
 And reality doesn't allow for satisfaction or fulfillment

38
00:03:27,260 --> 00:03:30,680
 through these various ways.

39
00:03:30,680 --> 00:03:39,000
 And so it can be quite disconcerting when meditation is

40
00:03:39,000 --> 00:03:42,160
 stressful or unpleasant or unamenable

41
00:03:42,160 --> 00:03:43,160
 to your wishes.

42
00:03:43,160 --> 00:03:48,040
 I wonder why am I doing this again?

43
00:03:48,040 --> 00:03:53,400
 So the first reason why we're doing this is for purity.

44
00:03:53,400 --> 00:03:57,960
 We're practicing because we feel bad about various things,

45
00:03:57,960 --> 00:04:00,600
 about ourselves and our attitude,

46
00:04:00,600 --> 00:04:02,680
 our behavior and so on.

47
00:04:02,680 --> 00:04:05,720
 We feel that we can be, we can improve.

48
00:04:05,720 --> 00:04:09,160
 Sometimes we downright hate ourselves.

49
00:04:09,160 --> 00:04:14,570
 But most often we just feel self-conscious or we have self-

50
00:04:14,570 --> 00:04:17,360
esteem issues that are often

51
00:04:17,360 --> 00:04:20,600
 based on reality that yes, we're not perfect.

52
00:04:20,600 --> 00:04:23,520
 We have problems.

53
00:04:23,520 --> 00:04:29,480
 There are problems about us.

54
00:04:29,480 --> 00:04:33,500
 I know we're often taught to ignore these or to accept

55
00:04:33,500 --> 00:04:36,200
 these or to think you're perfect

56
00:04:36,200 --> 00:04:37,520
 just the way you are.

57
00:04:37,520 --> 00:04:44,960
 No, in Buddhism we're not, we're not of that sort of bent.

58
00:04:44,960 --> 00:04:48,170
 It's understandable if you don't have a solution for your

59
00:04:48,170 --> 00:04:49,000
 problems.

60
00:04:49,000 --> 00:04:56,050
 If you don't have a solution for your inadequacies or your

61
00:04:56,050 --> 00:05:00,400
 failings, then it's reasonable to

62
00:05:00,400 --> 00:05:04,120
 think well maybe the answer is just to accept them and love

63
00:05:04,120 --> 00:05:06,520
 yourself the way you are, right?

64
00:05:06,520 --> 00:05:08,920
 I mean it sounds good.

65
00:05:08,920 --> 00:05:12,570
 It sounds nice to think oh, phew, I don't actually have to

66
00:05:12,570 --> 00:05:14,600
 fix these problems, right?

67
00:05:14,600 --> 00:05:19,610
 It'd be nice if someone could just tell you that you're

68
00:05:19,610 --> 00:05:22,280
 okay just the way you are.

69
00:05:22,280 --> 00:05:26,270
 And unfortunately I'm here to tell you you're not okay just

70
00:05:26,270 --> 00:05:27,560
 the way you are.

71
00:05:27,560 --> 00:05:38,000
 We've got lots of problems.

72
00:05:38,000 --> 00:05:39,000
 But that's okay.

73
00:05:39,000 --> 00:05:42,280
 I know I'm here to reassure you not that it's okay to be

74
00:05:42,280 --> 00:05:45,080
 that way but that we've got solutions

75
00:05:45,080 --> 00:05:50,850
 that there is a way, there is a path, there is practice

76
00:05:50,850 --> 00:05:54,080
 that can cultivate purity.

77
00:05:54,080 --> 00:06:02,340
 Solution is so powerful when you're really in the present

78
00:06:02,340 --> 00:06:04,920
 moment, it changes you.

79
00:06:04,920 --> 00:06:08,600
 It's like shining a bright light into the darkness.

80
00:06:08,600 --> 00:06:17,240
 You're able to see many things about yourself that were

81
00:06:17,240 --> 00:06:20,920
 previously hidden.

82
00:06:20,920 --> 00:06:25,200
 You see how you react.

83
00:06:25,200 --> 00:06:31,400
 See how you get upset.

84
00:06:31,400 --> 00:06:35,620
 You see how you crave, how we crave so many different

85
00:06:35,620 --> 00:06:36,520
 things.

86
00:06:36,520 --> 00:06:38,120
 Food.

87
00:06:38,120 --> 00:06:40,000
 Music.

88
00:06:40,000 --> 00:06:41,560
 Beauty.

89
00:06:41,560 --> 00:06:43,560
 Entertainment.

90
00:06:43,560 --> 00:06:45,160
 Romance.

91
00:06:45,160 --> 00:06:47,120
 Sex.

92
00:06:47,120 --> 00:06:56,360
 Her cravings, her diverse and intense.

93
00:06:56,360 --> 00:06:59,920
 And we see this about her.

94
00:06:59,920 --> 00:07:02,600
 We see how our mind works.

95
00:07:02,600 --> 00:07:05,240
 More importantly we see that the things that we cling to

96
00:07:05,240 --> 00:07:06,880
 and the things that we dislike,

97
00:07:06,880 --> 00:07:10,630
 you know, talking about this last night, but see that they

98
00:07:10,630 --> 00:07:12,520
're not worth clinging to.

99
00:07:12,520 --> 00:07:16,800
 Our purity comes from this knowledge, from this learning.

100
00:07:16,800 --> 00:07:24,920
 It doesn't come by forcing your mind to be peaceful.

101
00:07:24,920 --> 00:07:27,320
 Purity isn't about just purity of mind.

102
00:07:27,320 --> 00:07:30,760
 What do we mean by purity?

103
00:07:30,760 --> 00:07:35,600
 We have this list of seven purifications.

104
00:07:35,600 --> 00:07:43,300
 We certainly don't mean bodily purity or even purity of

105
00:07:43,300 --> 00:07:45,040
 actions.

106
00:07:45,040 --> 00:07:48,000
 But we don't even stop at purity of mind.

107
00:07:48,000 --> 00:07:53,760
 We're not content just with a pure mind because a pure mind

108
00:07:53,760 --> 00:07:56,160
 is a momentary thing.

109
00:07:56,160 --> 00:08:00,830
 The Buddha talked about the purification of a being or the

110
00:08:00,830 --> 00:08:02,600
 purity of a being.

111
00:08:02,600 --> 00:08:05,820
 And by that he meant not just that your mind is pure at any

112
00:08:05,820 --> 00:08:08,600
 given moment, but that you

113
00:08:08,600 --> 00:08:15,360
 have cleansed the potential for impurity to arise.

114
00:08:15,360 --> 00:08:19,000
 You have changed yourself.

115
00:08:19,000 --> 00:08:24,850
 That you no longer give rise to impure mind, to impure

116
00:08:24,850 --> 00:08:27,040
 thoughts, to...

117
00:08:27,040 --> 00:08:32,900
 And by impure we simply mean that which causes suffering

118
00:08:32,900 --> 00:08:35,600
 for you and for others.

119
00:08:35,600 --> 00:08:41,880
 That's a very practical reason so we can be more perfect.

120
00:08:41,880 --> 00:08:46,380
 Something that we might have disbelieved before we started

121
00:08:46,380 --> 00:08:48,920
 meditating and think that you can't

122
00:08:48,920 --> 00:08:53,600
 actually fix anything about yourself that we're stuck with

123
00:08:53,600 --> 00:08:55,240
 our personality.

124
00:08:55,240 --> 00:08:57,710
 When you meditate and you realize that our personality is

125
00:08:57,710 --> 00:09:00,680
 changeable, it's always changing.

126
00:09:00,680 --> 00:09:07,760
 It's constantly shifting according to our habits.

127
00:09:07,760 --> 00:09:18,920
 The second practical reason is to overcome mental illness.

128
00:09:18,920 --> 00:09:23,990
 So it's related to purity but specifically referring to

129
00:09:23,990 --> 00:09:25,800
 mental illness.

130
00:09:25,800 --> 00:09:29,920
 And illness we mean in a very literal sense.

131
00:09:29,920 --> 00:09:32,520
 We're ill, we're sick.

132
00:09:32,520 --> 00:09:37,820
 Sick sounds a little too harsh but illness in Buddhism,

133
00:09:37,820 --> 00:09:40,720
 mental illness in Buddhism is

134
00:09:40,720 --> 00:09:49,040
 any sort of disturbance, any sort of sadness or depression,

135
00:09:49,040 --> 00:09:53,560
 anxiety, anger, addiction.

136
00:09:53,560 --> 00:09:55,680
 All of these are considered to be mental illness.

137
00:09:55,680 --> 00:10:00,690
 And we're talking about the states of mind that cause great

138
00:10:00,690 --> 00:10:03,280
 anguish, great suffering.

139
00:10:03,280 --> 00:10:10,510
 The states of mind that are a lack of peace, that take us

140
00:10:10,510 --> 00:10:13,160
 away from peace.

141
00:10:13,160 --> 00:10:16,960
 So if you have an anxiety issue or if you have anger issues

142
00:10:16,960 --> 00:10:18,680
 or if you have addiction

143
00:10:18,680 --> 00:10:21,720
 issues, it's the issues.

144
00:10:21,720 --> 00:10:29,120
 If you're depressed, depression is a big one.

145
00:10:29,120 --> 00:10:31,240
 These are all related to the purity of the mind.

146
00:10:31,240 --> 00:10:35,080
 If your mind is pure, these are the benefits.

147
00:10:35,080 --> 00:10:43,960
 It's that you purify and you free yourself from mental

148
00:10:43,960 --> 00:10:46,000
 illness.

149
00:10:46,000 --> 00:10:51,000
 The third practical reason is to overcome suffering.

150
00:10:51,000 --> 00:10:56,500
 You know, the pain that you experience, it's more likely

151
00:10:56,500 --> 00:10:59,720
 that you're thinking meditation

152
00:10:59,720 --> 00:11:04,000
 is very painful.

153
00:11:04,000 --> 00:11:06,040
 Meditation is very painful, it's unpleasant.

154
00:11:06,040 --> 00:11:10,280
 There's a story of this monk who was meditating and because

155
00:11:10,280 --> 00:11:12,520
 there were these bandits who were

156
00:11:12,520 --> 00:11:17,030
 going to kill him and so he broke both of his legs with a

157
00:11:17,030 --> 00:11:19,360
 big rock and said, "Look,

158
00:11:19,360 --> 00:11:21,480
 I'm not going to run away, just come back in the morning.

159
00:11:21,480 --> 00:11:27,800
 You can take me away then if you want."

160
00:11:27,800 --> 00:11:33,240
 And he meditated on the pain.

161
00:11:33,240 --> 00:11:36,170
 Overcoming pain is, I mean, it's important to understand we

162
00:11:36,170 --> 00:11:39,440
're not trying to escape suffering,

163
00:11:39,440 --> 00:11:41,720
 not directly.

164
00:11:41,720 --> 00:11:46,590
 The attitude of wanting to be free from pain is problematic

165
00:11:46,590 --> 00:11:49,520
 because it's based on aversion.

166
00:11:49,520 --> 00:11:52,420
 That's sort of the catch that we find ourselves in regard

167
00:11:52,420 --> 00:11:53,360
 to suffering.

168
00:11:53,360 --> 00:11:59,400
 The more you don't want to suffer, the more you suffer.

169
00:11:59,400 --> 00:12:04,320
 And so overcoming pain is affected through mindfulness, not

170
00:12:04,320 --> 00:12:06,560
 because mindfulness makes

171
00:12:06,560 --> 00:12:11,460
 the pain go away, because mindfulness stops you from being

172
00:12:11,460 --> 00:12:13,840
 upset or changes the way you

173
00:12:13,840 --> 00:12:17,440
 look at pain so that you see it simply for what it is.

174
00:12:17,440 --> 00:12:24,040
 There's nothing wrong with pain.

175
00:12:24,040 --> 00:12:29,560
 We find we're so caught up with bad habits really.

176
00:12:29,560 --> 00:12:31,440
 That so many things cause us pain.

177
00:12:31,440 --> 00:12:38,380
 I mean, loud noise will cause you anger and stress and

178
00:12:38,380 --> 00:12:42,960
 suffering, bad food, the kind of

179
00:12:42,960 --> 00:12:49,640
 food that you're not keen on, smells, heat and cold.

180
00:12:49,640 --> 00:12:53,410
 It's a little bit cold, it's a little bit hot, and it's

181
00:12:53,410 --> 00:12:55,480
 quite unpleasant for us.

182
00:12:55,480 --> 00:12:57,400
 And the question is, why is it unpleasant?

183
00:12:57,400 --> 00:13:00,470
 I mean, you start to investigate meditation, and through

184
00:13:00,470 --> 00:13:04,120
 meditation, of course, using mindfulness,

185
00:13:04,120 --> 00:13:06,320
 you start to see that there is no good answer.

186
00:13:06,320 --> 00:13:10,620
 There's no reason why pain should make us upset or cold or

187
00:13:10,620 --> 00:13:12,520
 heat or hunger or thirst

188
00:13:12,520 --> 00:13:17,400
 or good smells, bad smells.

189
00:13:17,400 --> 00:13:23,730
 There's no reason for us to be attached or aversive to

190
00:13:23,730 --> 00:13:25,360
 anything.

191
00:13:25,360 --> 00:13:29,680
 That's a very powerful thing that we can overcome pain.

192
00:13:29,680 --> 00:13:32,160
 Something that society tells you you can't overcome.

193
00:13:32,160 --> 00:13:36,220
 The only thing to do is if you go to see the doctor and you

194
00:13:36,220 --> 00:13:38,160
 tell them you have pain, and

195
00:13:38,160 --> 00:13:41,410
 they give you a painkiller, as though they could kill the

196
00:13:41,410 --> 00:13:42,000
 pain.

197
00:13:42,000 --> 00:13:46,280
 I suppose technically they can, they kill the pain.

198
00:13:46,280 --> 00:13:49,080
 But they can't kill the aversion to pain.

199
00:13:49,080 --> 00:13:57,280
 In fact, it only makes it worse.

200
00:13:57,280 --> 00:14:00,820
 Another practical reason is it helps us find the right path

201
00:14:00,820 --> 00:14:01,000
.

202
00:14:01,000 --> 00:14:04,200
 The path in Buddhism isn't about becoming a monk.

203
00:14:04,200 --> 00:14:06,560
 It's not even about becoming Buddhist.

204
00:14:06,560 --> 00:14:11,290
 When we talk about finding the path, we mean doing whatever

205
00:14:11,290 --> 00:14:12,960
 you do mindfully.

206
00:14:12,960 --> 00:14:13,960
 Finding mindfulness.

207
00:14:13,960 --> 00:14:23,000
 The path is the path of mindfulness.

208
00:14:23,000 --> 00:14:27,410
 What that means is you really don't have to change anything

209
00:14:27,410 --> 00:14:29,840
 about your life intentionally.

210
00:14:29,840 --> 00:14:31,520
 You don't have to make all sorts of decisions.

211
00:14:31,520 --> 00:14:33,120
 You don't have to have the right answer.

212
00:14:33,120 --> 00:14:34,920
 I had a phone call today.

213
00:14:34,920 --> 00:14:38,550
 I was talking to someone about their life, and they said, "

214
00:14:38,550 --> 00:14:40,160
Boy, I wish the Buddha had

215
00:14:40,160 --> 00:14:46,910
 given more explicit instructions about how to live your

216
00:14:46,910 --> 00:14:48,120
 life."

217
00:14:48,120 --> 00:14:52,890
 But that's the thing is there is no correct answer on how

218
00:14:52,890 --> 00:14:54,720
 to live your life.

219
00:14:54,720 --> 00:14:59,080
 I mean, you're living your life no matter what.

220
00:14:59,080 --> 00:15:03,250
 The question is what choices you make, what decisions you

221
00:15:03,250 --> 00:15:03,920
 make.

222
00:15:03,920 --> 00:15:06,920
 We have this question about technical issues.

223
00:15:06,920 --> 00:15:09,760
 Which decision should I make?

224
00:15:09,760 --> 00:15:12,160
 That's not really what's important from a Buddhist

225
00:15:12,160 --> 00:15:13,000
 perspective.

226
00:15:13,000 --> 00:15:18,640
 The right path is when you make decisions mindfully.

227
00:15:18,640 --> 00:15:21,370
 Because if you're truly mindful, you can't possibly make a

228
00:15:21,370 --> 00:15:23,000
 decision for the wrong reason.

229
00:15:23,000 --> 00:15:29,190
 If you're truly mindful, you see as clear as possible about

230
00:15:29,190 --> 00:15:31,160
 the situation.

231
00:15:31,160 --> 00:15:34,000
 Your mind is pure.

232
00:15:34,000 --> 00:15:38,160
 Your actions, your decisions are not based on selfishness

233
00:15:38,160 --> 00:15:39,320
 or aversion.

234
00:15:39,320 --> 00:15:42,320
 They're not based on addiction or attachment.

235
00:15:42,320 --> 00:15:45,920
 They're not based on delusion or arrogance.

236
00:15:45,920 --> 00:15:47,920
 Conceit.

237
00:15:47,920 --> 00:15:50,800
 They're based on wisdom.

238
00:15:50,800 --> 00:15:52,400
 They're based on kindness.

239
00:15:52,400 --> 00:15:54,880
 They're based on compassion.

240
00:15:54,880 --> 00:15:57,500
 They're based on things that actually bring happiness to

241
00:15:57,500 --> 00:15:58,480
 you and to others.

242
00:15:58,480 --> 00:16:02,040
 They actually bring peace.

243
00:16:02,040 --> 00:16:13,720
 Based on mind states that are actually good for you.

244
00:16:13,720 --> 00:16:16,900
 The final reason which may seem somewhat impractical but is

245
00:16:16,900 --> 00:16:20,880
 perhaps the most practical is meditation

246
00:16:20,880 --> 00:16:27,240
 is to realize nirvana or nirvana.

247
00:16:27,240 --> 00:16:33,640
 Meditation is to help us let go.

248
00:16:33,640 --> 00:16:39,640
 The mind sees again and again what it's doing wrong.

249
00:16:39,640 --> 00:16:42,920
 Eventually it says, "Oh, I better stop that."

250
00:16:42,920 --> 00:16:46,090
 When it sees again and again that the things that we cling

251
00:16:46,090 --> 00:16:48,120
 to are not actually worth clinging

252
00:16:48,120 --> 00:16:53,960
 to then it lets go.

253
00:16:53,960 --> 00:16:59,560
 When the mind lets go, this is called nirvana, nirvana, the

254
00:16:59,560 --> 00:17:02,200
 cessation of suffering.

255
00:17:02,200 --> 00:17:07,200
 The mind enters into, doesn't even really enter into but it

256
00:17:07,200 --> 00:17:08,880
 stops entering.

257
00:17:08,880 --> 00:17:10,520
 Seeing is an entering.

258
00:17:10,520 --> 00:17:11,520
 Hearing is an entering.

259
00:17:11,520 --> 00:17:16,680
 We enter through these doors to see, to hear.

260
00:17:16,680 --> 00:17:20,730
 When the mind sees enough and is clear about seeing and

261
00:17:20,730 --> 00:17:23,360
 hearing and smelling and has this

262
00:17:23,360 --> 00:17:27,370
 true clarity about the experiences, about the doors of the

263
00:17:27,370 --> 00:17:29,280
 senses, then it lets go.

264
00:17:29,280 --> 00:17:31,200
 It sees that there's nothing worth clinging to.

265
00:17:31,200 --> 00:17:33,240
 There's nothing worth seeking out.

266
00:17:33,240 --> 00:17:37,320
 It stops seeking.

267
00:17:37,320 --> 00:17:39,700
 With the stopping of seeking there is no more seeing or

268
00:17:39,700 --> 00:17:41,320
 hearing or smelling or tasting or

269
00:17:41,320 --> 00:17:45,160
 feeling or thinking.

270
00:17:45,160 --> 00:17:49,320
 There is the cessation of suffering.

271
00:17:49,320 --> 00:17:55,330
 There are many good reasons for practicing and they're all

272
00:17:55,330 --> 00:17:56,600
 related.

273
00:17:56,600 --> 00:17:58,520
 They relate to our state of mind.

274
00:17:58,520 --> 00:18:02,800
 They relate to happiness, true happiness.

275
00:18:02,800 --> 00:18:06,040
 They relate to peace.

276
00:18:06,040 --> 00:18:08,000
 They relate to goodness.

277
00:18:08,000 --> 00:18:11,320
 Meditation makes you a better person.

278
00:18:11,320 --> 00:18:13,320
 Makes us better.

279
00:18:13,320 --> 00:18:17,400
 All better, better in many ways.

280
00:18:17,400 --> 00:18:20,080
 Better in terms of our goodness, better in terms of

281
00:18:20,080 --> 00:18:22,320
 happiness, better in terms of health,

282
00:18:22,320 --> 00:18:28,200
 mental health, physical health.

283
00:18:28,200 --> 00:18:34,160
 Meditation makes us happy.

284
00:18:34,160 --> 00:18:36,800
 Which may be hard to believe for those of you who are

285
00:18:36,800 --> 00:18:38,840
 struggling through it but happiness

286
00:18:38,840 --> 00:18:41,940
 is not an easy thing, neither is peace.

287
00:18:41,940 --> 00:18:44,640
 Our problem is that we're conditioned to think that

288
00:18:44,640 --> 00:18:46,840
 happiness is something you can just have

289
00:18:46,840 --> 00:18:48,040
 happen to you.

290
00:18:48,040 --> 00:18:55,230
 You can get, you can buy, you can chase, you can find

291
00:18:55,230 --> 00:19:00,280
 happiness here, there or somewhere

292
00:19:00,280 --> 00:19:01,280
 else.

293
00:19:01,280 --> 00:19:04,680
 I think it's something easy.

294
00:19:04,680 --> 00:19:05,920
 Happiness is not easy.

295
00:19:05,920 --> 00:19:11,020
 Happiness requires effort, requires wisdom, requires a lot

296
00:19:11,020 --> 00:19:11,920
 of work.

297
00:19:11,920 --> 00:19:15,440
 Whereas a lot of work because we're all bent out of shape

298
00:19:15,440 --> 00:19:17,840
 and we do things and we're inclined

299
00:19:17,840 --> 00:19:21,800
 and our habits are set up to hurt us.

300
00:19:21,800 --> 00:19:24,590
 We have habits set up that actually cause us stress and

301
00:19:24,590 --> 00:19:26,360
 suffering and they're habitual

302
00:19:26,360 --> 00:19:29,040
 so it takes a lot of work.

303
00:19:29,040 --> 00:19:32,040
 The Buddha said if you have a tree leaning in one direction

304
00:19:32,040 --> 00:19:33,480
, you want to pull it in the

305
00:19:33,480 --> 00:19:34,480
 other direction.

306
00:19:34,480 --> 00:19:37,680
 You need a strong rope and you need to work really hard.

307
00:19:37,680 --> 00:19:40,950
 You can't just cut the tree down and hope it falls in the

308
00:19:40,950 --> 00:19:42,760
 other direction because it

309
00:19:42,760 --> 00:19:43,760
 won't.

310
00:19:43,760 --> 00:19:46,980
 If there's an elephant stuck in the mud, it's going to take

311
00:19:46,980 --> 00:19:48,560
 a lot of work to get the elephant

312
00:19:48,560 --> 00:19:49,560
 out.

313
00:19:49,560 --> 00:19:55,000
 So we're stuck, we're stuck with our attachments and our a

314
00:19:55,000 --> 00:19:56,320
versions.

315
00:19:56,320 --> 00:19:59,040
 There's no question that being free from them would be good

316
00:19:59,040 --> 00:20:01,600
, would bring us peace and happiness.

317
00:20:01,600 --> 00:20:07,100
 The only question is how much work it's going to take to do

318
00:20:07,100 --> 00:20:07,800
 it.

319
00:20:07,800 --> 00:20:09,200
 So it requires patience.

320
00:20:09,200 --> 00:20:14,160
 I'd ask that all of you be patient and be patient with the

321
00:20:14,160 --> 00:20:16,560
 suffering and be patient

322
00:20:16,560 --> 00:20:21,880
 with the work that this takes.

323
00:20:21,880 --> 00:20:27,820
 Be vigilant, methodical and don't give up because that's

324
00:20:27,820 --> 00:20:30,320
 the nature of habits.

325
00:20:30,320 --> 00:20:34,270
 You might think, oh, this is working and then give up and

326
00:20:34,270 --> 00:20:36,480
 then let your guard down or stop

