1
00:00:00,000 --> 00:00:04,290
 "How does one truly know that one has established a refuge

2
00:00:04,290 --> 00:00:05,700
 in the Buddha?

3
00:00:05,700 --> 00:00:08,970
 Is this done by seeing our remaining defilements and

4
00:00:08,970 --> 00:00:11,040
 working on them accordingly?

5
00:00:11,040 --> 00:00:14,340
 I was thinking of the story about Tuja Puktila, where he

6
00:00:14,340 --> 00:00:16,400
 came to realize that he had no refuge

7
00:00:16,400 --> 00:00:19,000
 and afterwards he became enlightened.

8
00:00:19,000 --> 00:00:23,720
 Should we see our remaining defilements and the lessening

9
00:00:23,720 --> 00:00:26,000
 of these as our refuge?

10
00:00:26,000 --> 00:00:29,560
 Thanks."

11
00:00:29,560 --> 00:00:35,160
 The true taking of refuge is the attainment of sotapana.

12
00:00:35,160 --> 00:00:41,960
 A person who attains sotapana is said to have true refuge

13
00:00:41,960 --> 00:00:43,000
 because at that point they have

14
00:00:43,000 --> 00:00:47,800
 full confidence in the Buddha and his teachings and those

15
00:00:47,800 --> 00:00:50,440
 who practice his teachings.

16
00:00:50,440 --> 00:00:53,390
 They know that the Buddha was enlightened in the sense that

17
00:00:53,390 --> 00:00:54,360
 the Buddha...

18
00:00:54,360 --> 00:00:59,130
 I talked about this yesterday in the study group, how they

19
00:00:59,130 --> 00:01:02,240
 know that the Buddha was enlightened

20
00:01:02,240 --> 00:01:05,530
 because they have realized that what the Buddha said was

21
00:01:05,530 --> 00:01:06,040
 true.

22
00:01:06,040 --> 00:01:09,010
 They realized nibbāna so they can verify, "Yes, the Buddha

23
00:01:09,010 --> 00:01:10,800
 was talking about something

24
00:01:10,800 --> 00:01:12,400
 that I've actually realized.

25
00:01:12,400 --> 00:01:16,800
 That realization is the most profound realization possible.

26
00:01:16,800 --> 00:01:19,760
 It's cessation of suffering.

27
00:01:19,760 --> 00:01:22,560
 Therefore they see either the Buddha got it from somewhere

28
00:01:22,560 --> 00:01:24,280
 or he was enlightened himself.

29
00:01:24,280 --> 00:01:27,700
 It could be that he had a tape recorder or he had something

30
00:01:27,700 --> 00:01:29,360
 in his ear and he was being

31
00:01:29,360 --> 00:01:30,760
 fed it.

32
00:01:30,760 --> 00:01:36,130
 But barring that, he was enlightened and knew what he was

33
00:01:36,130 --> 00:01:38,280
 talking about because only an

34
00:01:38,280 --> 00:01:41,760
 enlightened being would be able to explain such a thing.

35
00:01:41,760 --> 00:01:43,710
 You have full confidence in the Dhamma because you have

36
00:01:43,710 --> 00:01:44,920
 experienced it for yourself.

37
00:01:44,920 --> 00:01:47,920
 You know that it truly is the cessation of suffering.

38
00:01:47,920 --> 00:01:52,190
 And you have full confidence in the Sangha because you know

39
00:01:52,190 --> 00:01:54,160
 that if you hear of someone

40
00:01:54,160 --> 00:01:57,970
 practicing in this way, you know that they are leading

41
00:01:57,970 --> 00:02:00,360
 themselves closer to nibbāna.

42
00:02:00,360 --> 00:02:03,300
 If you heard that someone has attained nibbāna, then you

43
00:02:03,300 --> 00:02:05,320
 know that they are enlightened because

44
00:02:05,320 --> 00:02:08,370
 you know that the attainment of nibbāna leads to

45
00:02:08,370 --> 00:02:09,640
 enlightenment.

46
00:02:09,640 --> 00:02:11,640
 For example, this comes at sotapāna.

47
00:02:11,640 --> 00:02:18,920
 To really say you've taken refuge is to reach that level.

48
00:02:18,920 --> 00:02:23,590
 Apart from that, the idea of taking refuge in a

49
00:02:23,590 --> 00:02:27,120
 conventional sense just means that you

50
00:02:27,120 --> 00:02:31,160
 accept the Buddha as your guide.

51
00:02:31,160 --> 00:02:34,660
 And of course it's still provisional because you don't know

52
00:02:34,660 --> 00:02:36,520
 whether the Buddha was correct

53
00:02:36,520 --> 00:02:37,520
 or not.

54
00:02:37,520 --> 00:02:39,170
 And you may pretend that you're a Buddhist and that you've

55
00:02:39,170 --> 00:02:40,600
 taken the Buddha as your refuge,

56
00:02:40,600 --> 00:02:43,960
 but it's still only provisional and you might still, as

57
00:02:43,960 --> 00:02:45,960
 many Buddhists do, do many things

58
00:02:45,960 --> 00:02:48,640
 that are contrary to the Buddha's teaching.

59
00:02:48,640 --> 00:02:51,760
 So you can't really be said to have taken refuge but accept

60
00:02:51,760 --> 00:02:53,080
 in a conventional sense

61
00:02:53,080 --> 00:02:56,660
 where you claim that the Buddha is your guide and you

62
00:02:56,660 --> 00:02:59,240
 intend to practice according to his

63
00:02:59,240 --> 00:03:04,650
 teachings and you therefore take his followers as your

64
00:03:04,650 --> 00:03:08,280
 teachers or you accept that a person

65
00:03:08,280 --> 00:03:11,320
 who practices in this way is practicing in the right way.

66
00:03:11,320 --> 00:03:16,200
 It's all provisional but it's done in a conventional sense.

67
00:03:16,200 --> 00:03:19,570
 But that's just a determination that you make on your own

68
00:03:19,570 --> 00:03:21,520
 that you want to practice according

69
00:03:21,520 --> 00:03:23,840
 to the Buddha's teaching.

70
00:03:23,840 --> 00:03:26,260
 And that's what all meditators do really.

71
00:03:26,260 --> 00:03:30,490
 You may not do this chanting that we do but by undertaking

72
00:03:30,490 --> 00:03:32,800
 the Buddha's teaching you're

73
00:03:32,800 --> 00:03:35,560
 taking refuge in the sense that I just said you're

74
00:03:35,560 --> 00:03:37,960
 accepting that the Buddha has something

75
00:03:37,960 --> 00:03:41,000
 good to teach, at least provisionally, and you're

76
00:03:41,000 --> 00:03:43,120
 practicing it, putting it into practice

77
00:03:43,120 --> 00:03:46,160
 hopefully wholeheartedly.

78
00:03:46,160 --> 00:03:48,520
 That's what it means as I understand to take refuge.

