 Okay, good evening everyone.
 Broadcasting live September 12th.
 Today we have a quote on compassion, I believe, from the
 Visuddhi manga.
 Oh no, here it says "DHPA," commentary to the Dhammapada.
 Probably also the same quote that's in the Visuddhi manga.
 In the past.
 Robin, would you please read the quote for us?
 Yes. "Compassion is that which makes the heart of the good
 move at the pain of others.
 It crushes and destroys the pain of others.
 Thus it is called compassion.
 It is called compassion because it shelters and embraces
 the distressed."
 I can't pull up the link.
 That's funny.
 That is moved.
 I like that word moved, no?
 That's a good description because there's a fine line
 between being moved
 and being affected in the sense of disturbed or upset,
 becoming sad.
 Moved is just the opening it up as a possibility
 and the change in weighing of options.
 So you'd normally suggest that a person would do nothing
 but a person with compassion with an open heart and an em
pathic heart.
 It makes them inclined to naturally move towards helping
 others,
 freeing others from suffering.
 And the rest of the quote would be useful if we had the
 original
 because it's actually trying to explain the word karuna,
 not the word compassion.
 And karuna comes etymologically.
 I mean, they offer several etymologies,
 and that's crushing and destroying somehow could be related
 to the word karuna.
 And sheltering and embracing as well could potentially be
 related to the word karuna.
 The words for shelter and embrace, I can't think of them.
 Or maybe it's the distress.
 The word distressed has something to do with compassion.
 Karuna.
 Yes, compassion is another one of the four.
 This would be in a broader context of describing each of
 the four brahma-viharas.
 Compassion is special though because it's one of the
 qualities of the Buddha.
 I consider that the Buddha, one of the reasons, the reason
 the Buddha taught,
 the reason the Buddha didn't practice for his own
 enlightenment
 and be satisfied with that was due to his great and
 overwhelming compassion.
 Let's see if we can find this.
 Yeah, it made it difficult.
 We could find it nui siddhi manga, but I was thinking maybe
 we could find this story.
 Anyway, I don't have too much to say.
 Nothing I can think of that comes to mind.
 Does anyone have any questions?
 Yes, there's a question already.
 Sorry.
 I have been practicing vipassana meditation.
 I started to sometimes get the feeling that I didn't change
 place when I walked from point A to point B,
 but merely the scenery changed while I remained in the same
 place.
 Is this sensation a cause for concern or can it be
 considered a mark of progress?
 Thank you, Bante.
 The only mark of progress that you should be looking for is
, I'm not looking for,
 but recognize, is understanding of impermanent suffering
 and non-self
 in the sense of those things that you thought were
 permanent
 or you clung to because of an perception of permanence.
 You now don't cling to them because you see that they're
 impermanent
 or those things that you thought were satisfying.
 You now don't cling to them because you realize they're
 unsatisfying.
 Or those things you thought you could control,
 now you don't cling to them because you can't control them.
 That's about it, really.
 And so this experience is impermanent suffering and non-
self,
 impermanent, unsatisfying, uncontrollable.
 And so any value judgment you might place on it is going to
 be a cause for suffering
 because you cling to it and you have this misunderstanding
 that somehow it's going to be stable, satisfying, or contro
llable.
 It's not.
 So it's not a cause for concern.
 It's a state that is different from another state.
 If anything, it shows you impermanent, that before it was
 like this,
 before it was like that, now it's like this.
 It shows the instability of reality.
 Reality can change your perception.
 The way you look at things can shift.
 That's impermanence.
 Non-self as well because you're not controlling it.
 But it's suffering as well because it's not according to
 your desire
 and it's not going to satisfy you.
 And no, this isn't really a sign of progress or anything
 beneficial.
 It's a benefit to be had from that state, except seeing
 impermanence,
 seeing the character you're interested in.
 Can compassion with oneself lead to diligence?
 Compassion for oneself?
 I think so, yes.
 Yeah, again, I don't think you can cultivate compassion for
 yourself.
 It's not fruitful.
 It's useful as an example, but it doesn't actually lead to
 any higher state of awareness.
 How can panic attacks be managed?
 The symptoms, look at the symptoms because the symptoms are
 not the panic.
 So focus on the symptoms and forget about the panic.
 That's one part of it.
 We panic because of the symptoms.
 We panic and then the symptoms and then we panic based on
 the symptoms
 and it becomes a...
 That's why it becomes an attack because it says it's no
 boss.
 So I mean mostly focus on the symptoms.
 But as well the panic going back and forth.
 Being able to differentiate between the symptoms,
 the physical symptoms of the panic and the panic itself is
 useful.
 Being able to break up moments and not get caught up in the
 whole problem,
 the attack, because a panic attack doesn't exist.
 It's just the idea you get, "Oh my God, I'm having a panic
 attack."
 Actually there's lots and lots of experiences,
 the thought, the worry, the concern about it.
 The panic, the physical aspects.
 It was a good question the other day right at the end when
 it was getting late
 and everyone was tired and you said, "Do you want to answer
 this one?"
 And I obviously didn't answer it the way you would.
 But it was a good question.
 It was about when negative feelings keep coming back,
 you're being mindful of them, trying to be mindful
 and they just keep coming back.
 I think you would probably have a lot more to say about it
 than I did.
 We're not trying to get rid of anything by being mindful,
 except delusion.
 But even then it's not about trying.
 The effort isn't to make things go away and never come back
.
 The effort is to see clearly.
 And part of seeing clearly is seeing that things aren't,
 experiences aren't under our control.
 So that they keep coming back even though you don't want
 them to.
 It's a clear sign of permanent suffering in oneself.
 That's what you'd expect to see.
 That you can't make them go away just because you want them
 to.
 So it's much more about tolerance and non-reactivity
 than about changing what you experience and what comes back
.
 Even bad feelings because they're only going to go once you
 stop reacting to them.
 Once you stop giving them power over you.
 Either we're all caught up on questions or people are still
 meditating one or the other.
 I got a good group, good number of people.
 Yes, mostly green.
 Okay, so what about doing the news?
 Sure.
 Let's do one night a week, maybe Monday night. Is that a
 good night?
 Sure, yeah.
 Monday night, it's the news.
 Monday a good news night, I don't know.
 Sure.
 And any night's a good news night. There's always stuff
 going on.
 Okay, but we need more people to join the Hangout.
 Yes.
 So this means we need some people who we know a little bit.
 Is there anybody else there who we know?
 We had all kinds of people in the Hangout on Reddit today.
 That was so nice to see because usually everybody's camera
 shy.
 So that was awesome.
 So yeah, there's always people we know.
 Maybe we don't need a panel. I mean, we should open it up
 and maybe we shouldn't require it.
 Well, we could open it up because there's always the Q&A
 panel.
 You know, you can open up the Q&A panel and people can
 input by text.
 News story. People can input news stories.
 Yeah.
 They can input questions. They can, or with questions, they
 can post a news story and ask us questions.
 Or they can post a news story.
 The most interesting news story I read today was that there
 was a write-up in our local newspaper about the 17
 Republican candidates for president in the U.S.
 and their religious background. And it was interesting.
 Most of them had started out at one thing and changed to
 something else.
 They had all this interesting history.
 One, Bobby Jindal was born a Hindu and he now considers
 himself a Catholic and sort of an anti-
 Oh, my goodness. The word just went out of my mind. Well, a
 Catholic and sort of an evangelical Catholic, which is an
 interesting combination.
 But apparently half of his district is evangelical and the
 other half is Catholic.
 And he's just getting everybody there. So it was just
 interesting.
 But there were no Buddhists, no Buddhist candidates at all.
 Not surprising.
 Not surprising. I have a question.
 Yes. Are you comfortable in the new place? Oh, actually, I
 skipped a big question, but-
 Is it a question? It's a big paragraph.
 Yeah, yeah, I skipped that one. Sometimes when I meditate,
 I become aware of my body feeling heavy and numb.
 Shortly after this, maintaining mindfulness seems to become
 much more challenging.
 In other words, I'm having myself having to refocus more
 often.
 Is this normal or a sign that I might be? That I might be?
 I'm sure.
 Sounds like you must focus on the intense content.
 It can happen.
 Well, mindfulness can be challenging. I mean, it's always
 going to be challenging.
 Don't expect it to get easier.
 It's something you have to do here and now.
 Forget about the past, the future.
 What causes that? I've noticed that as well. Just parts of
 your body.
 Sometimes my hands will feel extremely heavy, like they're
 just sinking into my legs or something. What causes that?
 I say concentration will cause that. Sometimes it feels
 very light, sometimes it feels very heavy.
 I think my teacher would probably say PT.
 Really?
 Because it's- that's just a classification, but the point
 is that it's caused by concentration.
 Okay.
 It has been many things. I mean, it can be caused by
 residual concentration.
 If you're a sort of person who does a lot of computer work,
 heaviness can be a sign of that.
 Torpor.
 It's an imbalance sort of in the physical aspect, maybe in
 the brain, you could say.
 It's sort of an imbalance that works itself out if you do
 an intensive course.
 So those kind of things come less if you do an intensive
 course.
 It can also be- it can also arise at one of the stages of
 knowledge, Bhanganyana,
 there's heaviness, lightness.
 Are you comfortable in the new place?
 Place is place. It's not really- oh, I don't know. I
 suppose I could say.
 The idea of being on my own is pretty awesome.
 I don't mean not having people, but not having a-
 A boss?
 It's not a boss. It's a culture or an institution hovering
 over me.
 In this monastery, it's great. The facilities are great,
 but it's Cambodian, and there's a Cambodian community.
 People are great. The community is great, but it's still an
 institution,
 and there's traditions and practices that are in conflict
 with some of the things that-
 It's hard to move, and it's stifling in a way. So the
 freedom of being my own monk.
 Someone interesting. I got an email today that I'm not- I
 almost-
 I kind of decided I just wasn't going to respond to it.
 But it's by a lay Buddhist here in Ontario who I know quite
 well from the internet,
 but he's always had this sort of- out of the blue he'll
 just criticize.
 And so it was critical. He said, like, there's something
 wrong with the monk being alone.
 And he appointed this other monk who had a friend who
 always seems to be with other monks,
 and he said, "That's more accepted than you being on your
 own."
 It just out of the blue, really. We were having a dialogue
 about this other monk who was going on alms around,
 and it just was a friendly dialogue, and suddenly he comes
 out of the blue,
 and then he suggested that it might even be against the Vin
aya, what I'm doing being alone,
 which is really bizarre, because being alone is- how many
 monks spent their time alone in the forest?
 Yeah, it seems like seclusion was kind of highly prized.
 So often the Buddha praised seclusion, and friendship as
 well,
 but being alone is an important part of the- like, there
 are times to be alone, for sure.
 And staying alone for the rains, this happened a lot.
 So anyway, I mean, it's just- I don't know how to reply to
 this, because it was pretty openly critical.
 So I guess I have to reply and explain myself.
 Is there any concern with- isn't there a tradition of rec
iting the Patimoka with a certain number?
 Only if there's a rule. So there is a rule, and it's kind
 of like- not quite a rule,
 but it's wrong for four monks, four monks, who don't know
 the Patimoka to stay together,
 because if there's four monks staying together, then they
 have to recite the Patimoka,
 and if none of them know it, they can't stay together.
 But one monk alone cannot recite the Patimoka, even at
 least four monks.
 So there are rules, you know, there's a protocol to follow
 when you're alone.
 So why would there be the protocol if you couldn't stay
 alone? I mean, it's bizarre.
 I mean, he doesn't really have a deep- he knows he doesn't
 have a deep knowledge of it.
 It's just- I don't know how to say it. Too long living
 alone, I think.
 He wasn't into the social more- the social niceties, and,
 you know, not phrasing it as an open criticism.
 I just thought, why don't you reply to that?
 Maybe testing you.
 He's just a little odd. He's very smart, very into pollen.
 Brian says your audio on the YouTube feed is muddled.
 It's not muddled for me. I'm not sure if-
 You have to go close to him.
 Yeah, I'm not sure if it is for anyone else. It sounds fine
 for me.
 Could be the internet here is not so good.
 Yeah, Bronson says you're going in and out, so maybe a
 little problem with the internet there.
 How is the audio feed better?
 I mean, as far as I can tell, it's fine. I didn't notice it
 going in and out.
 The audio only feed, or is that-
 Yeah.
 I understand that we can focus on other parts of the body
 rising and falling with the breath,
 such as the chest, or even air passing past your nose as
 you breathe in and out. Is this correct?
 Yeah, I mean, to some extent, the air at your nose isn't
 real.
 You're not experiencing the air at the nose. You're
 experiencing a sensation.
 So you can focus on that sensation.
 It's more subtle, so it's more likely to lead to tranqu
ility.
 And so we favor the stomach, which is more coarse and less
 likely to lead to tranquility.
 You never talk about spiritual energy in your video. Is it
 something that people-
 I apologize. So often when I'm reading these, they just
 flip right up and I can't see them anymore.
 You never talk about spiritual energy in your video. Is it
 something that people talk of in your tradition, or is it
 out of context?
 Out of context. I mean, well, it's not in-
 Spiritual energy is just energy, and it's not important in
 our tradition.
 Is it okay to meditate with your eyes open?
 Yeah, I mean, you can meditate any time in any situation.
 Because vision is so distracting and so all-encompassing,
 especially in modern times,
 we do more so, I would say. Closing your eyes has a benefit
 of keeping you focused.
 If you find other dining places that you might find
 adequate that accept gift cards, please let us know.
 How are you doing in that regard?
 I'm doing quite well, actually. Thank you. Actually, I
 wanted to mention that tonight.
 And just a lot of things going on today. So again, it's in
 my mind.
 That's really awesome how- this is really great, the amount
 of support and encouragement.
 And just all around keeping me alive is awesome.
 It's something- well, I'm grateful for the support.
 So there's- people have been sending Tim Hortons cards, and
 so now I've got-
 This morning I took the other monk here, or he took me, he
 drove, and we went to Tim Hortons
 because it was raining and I was walking and the rain not
 so good.
 I didn't bring my umbrella. So we drove to Tim Hortons and
 had breakfast together.
 And I think tomorrow morning we'll go to Tim Hortons again
 for breakfast.
 Yeah, several places. There's this Williams Fresh store I
 haven't eaten there, but someone sent me a gift card.
 Some guy gave me a Starbucks card. Yeah, doing quite well.
 Long story short.
 That's awesome to hear. Now we just have to get you a table
 and chairs, right?
 I have nowhere to sit in the chairs.
 No, in fact, the dining room now has all the garbage that
 we've got.
 So we went, Robin and her husband drove us through the area
 around the monastery looking for garbage
 or castawayed, and we found several couch cushions and two
 beanbag chairs.
 And we washed them all and took the styrofoam out of the
 beanbag chairs and washed the shelves.
 And now all of that stuff is in the dining room in sort of
 a circle.
 So the library has seats in it. And that's a nice place to
 meet, just everyone sitting on the cushions on the floor.
 I don't think we need a table and chairs at all. Okay.
 I was just thinking when meditators are there, if they were
 eating or something, but I guess maybe you can eat on the
 floor.
 I can eat on the floor for sure.
 I mean, the only problem would be for older people, certain
 people who have a problem or aren't able to see the floor.
 It would be good to have a chair too.
 They have these little plastic stools, like they use them
 in Thailand for washing,
 and they do hand washing and sit on these little stools
 close to the ground.
 They've adapted them and used them for meditation as well.
 People look into that, but let me talk about that tomorrow.
 Sure.
 [Silence]
 Do your meditation courses start on a predetermined
 schedule or just whenever people show up?
 Whenever people show up.
 They do have to let us know. Just showing up is probably a
 bad idea.
 I don't know. When he showed up on my door, I didn't
 register who it was at first.
 Then he had his luggage, and I thought, "Someone just
 showed up."
 And then it hit me who he was. Oh, yes, right, because I
 recognized him.
 It was for a brief moment there. It was like, "Uh-oh. You
're supposed to let us know in advance."
 That's good. I'm glad he's there.
 Mm-hmm. He got into the washroom fight line.
 Good. Yes. Yesterday, he sent a message to me.
 Bunhe had gone back to his old monastery for the weekend as
 the requirement of the reins.
 The new person, Aruna, was there by himself, and he was
 locked out of the washroom,
 which that could be problematic, but he found out how to
 get in.
 I've got a bad cold and have been meditating on the
 physical sensations of being sick.
 I feel much worse as a result. I'm not sure why I'm doing
 this or what the point is.
 What should I be looking for?
 No words. It's meditation that makes me feel awful. It's
 funny.
 Well, no, the meditation isn't making me feel worse. I
 guarantee it.
 It's your own reaction. Why? Because now you're not running
 away from it.
 You're not avoiding it.
 It's funny. Practice meditation. Now I feel awesome.
 What a horrible thing, right?
 You're more in tune with all the aches and pains, and you
're paying more attention to it.
 No distractions.
 Which is awesome. Way to go. Good stuff. It's tough, I know
.
 Yeah.
 Who said enlightenment was going to be easy? Look what the
 Buddha said. He said, "I won't move from this spot,
 even if my blood and flesh should dry up and only skin and
 bones remain."
 I won't. I won't waver.
 The Buddha was very hardcore. Didn't he allow monks to use
 urine as a medicine or something?
 That's different. Urine is actually a useful medicine. Look
 it up.
 Using your own urine can be quite healing.
 Urine is not garbage or poison.
 It's feces. But urine is leftovers.
 It's like purified blood, sort of.
 It's healing as a cell on your skin, and it's security
 internally.
 Look it up on the internet. Some people swear by it.
 That's hardcore.
 That's hardcore.
 But your own urine is the best because it has your own
 signature.
 In India, it's actually a thing. It's Ayurvedic wisdom to
 talk about.
 Urology, I think, is the best.
 I cannot figure out how to stop walking when I get
 distracted.
 I am too distracted to stop walking. Then when I finally
 notice the distraction, it goes away.
 What do I do about this?
 That's fine. When it goes away, then you just say, thinking
, thinking, why?
 Because you don't want to doubt about what should I do.
 When it's gone, you recognize it. You can either say,
 thinking, thinking, or just knowing, knowing, because you
 know that it went away.
 So that it stops at that. So that you don't get frustrated
 that you were thinking it didn't notice it.
 When you catch it, just be aware of the present. It's gone.
 It's passed already.
 The other night you said someone bottled water for you.
 Are we to understand that you don't have the water turned
 on in the house yet?
 And if so, is there a plan in place to establish that?
 It's a good question. Not that question, but the underlying
 question is whether the tap water is drinkable.
 Because yes, we have tap water, but Hamilton, Ontario is
 called the armpit of Ontario.
 If you look at it on the map, it's kind of a joke because
 Lake Ontario looks like it's curved like this, and Hamilton
's right where the armpit would be.
 But it is a very, very dirty factory city.
 So I don't know whether the water is drinkable. Can you
 just drink the tap water?
 Sometimes, some places, tap water is better than bottled
 water.
 That's true. That tap water is tested many times a day in
 municipalities, whereas bottled water is tested less rig
orously.
 But I'm not sure if that's consistent everywhere.
 We should talk about that tomorrow.
 Yes.
 Do you want your list set up?
 Yes.
 We should maybe put out a notice that if people want to get
 on the voluntary list, we should give them the link.
 There's a link. They can subscribe themselves.
 Do you remember what that is? I was looking for it, and I
 didn't come across that.
 In the admin, there's the list info. It gives you a link to
 the list info page.
 Okay.
 It would be mailman list info, and then what is it,
 volunteer?
 Yes.
 It's, you know, the normal admin link, you just replace
 admin with list info.
 Okay.
 Using meditation or otherwise, have you explored what mind
 states or physical sensations occur at the time of death?
 I was wondering if you had any thoughts on this in general,
 even if not.
 [Silence]
 Yeah, well, a lot of what we experience in insight
 meditation is ostensibly quite similar to what one would
 experience at death.
 Probably not as extreme, but a lot of the things that we
 hold on to and don't realize it come up during meditation.
 And that's a lot of, that's a great reason to practice
 insight meditation,
 is to work these things out before it's too late, before
 you have to, you're forced to deal with them.
 So during meditation, bad deeds that you've done, bad
 things that you've done,
 things that you feel guilty about that maybe weren't even
 bad things, anything that you're holding on to,
 it'll come up during the meditation as though in the same
 way as it would when you're passing away.
 [Silence]
 Having conversations with new people is stressful to me.
 Could meditation reduce social anxiety?
 Absolutely. Stop worrying about yourself. Less ego, less
 concern about your own image,
 less concerned about what other people think, much more
 comfortable as a result.
 There's less worrying about doing the right thing and the
 wrong thing. But it's a long path.
 I remember when I first went to Jumdong, I was terrible
 worried.
 Being in a foreign country, you're afraid of everything. I
 mean, I was, I guess some people aren't.
 But if you're the sort of person who gets afraid, who's
 afraid of doing the wrong thing, which I was,
 it gets amplified when you're in a foreign country. And so
 one monk said to me, he said, you know, you don't have to
 be afraid.
 He said, of course I have to be afraid. I have no clue what
's the right thing to do. And everything I do is wrong.
 It was amplified and just realizing that and watching the
 fact that I couldn't change it.
 I couldn't turn it off. I couldn't make myself not worry,
 not be afraid.
 And just watching as over the years, the habit breaks down.
 You work your way out of it through the meditation,
 absolutely.
 Also through growing up. I mean, part of it, there's an old
 paper, people have something about them that, you know,
 they've gone through it, they've lived through it.
 Meditation's just highly accelerated and refined and it
 avoids
 developing bad habits along the way. But now as you get
 older, it gets easier. Think.
 It can. Meditation's more certain, more sure.
 Yeah. I felt a big change with meditation. I spent a
 lifetime being extremely shy, quiet, self-conscious.
 And two years ago, I would not have been here talking to
 you. Drew thinks you need a filtration device.
 But bottled water is quite cheap and reasonable.
 But apparently, wasn't it a problem with the filtration
 devices? They don't actually work.
 Filtration devices work. Britta's don't work. Because Britt
a's don't work.
 Britta pictures do nothing. They're not supposed to.
 I didn't know that. If I catch a distraction going away, in
 other words, it is going away right now, is that an object
 worth noting?
 Yeah. Absolutely.
 Knowing even or, you know, disappearing. Knowing is the
 best catch-all. It's just easy.
 You don't have to think too much. Remember the Buddha said,
 "Nanimitagahi, Nanubiyanjinagahi." Don't pay attention. Don
't grasp the sign or the particulars of the object.
 It's the general that we want. So just knowing it arouse
 and it cease.
 But just keep it at that. Keep yourself from delving into
 the particulars.
 Oh, what was that like? Was it good? Was it bad?
 When it is said that by the practice you build an island
 for yourself, does it mean you have less problems in your
 life?
 Build an island for yourself. I think that's a quote of,
 yeah, be a lamp to yourself.
 Be a light to yourself. It could be island as well. Deepa
 can mean lamp or island.
 But it probably means a foreign, be an island, right?
 Because don't depend on anyone else.
 Become independent.
 It means that you're independent. It means that, and I
 guess not just independent, but also unaffected.
 Rising above the flood because samsara is likened to a
 great ocean. The defilements are like a great flood.
 An island is something that is above the water, above the
 flood. So it's a symmetry of being invincible in a sense or
 safe.
 And defilements from suffering.
 Mindfulness is the ultimate safety. If you're truly mindful
, nothing and no one can hurt you.
 Mentor anyone?
 So tomorrow, another big day, we have meeting at 12. We
 should be Maga at 1, Palia at 2.
 And then I'm heading back to the monastery. Power monastery
.
 And the young monk here is going to drive me because we're
 bringing pillows and blankets.
 Today was a big, big group of people here.
 It's good that I come back for that. It's good to be a part
 of this community.
 They had monk was really happy that was coming back. Or he
 was happy that was coming back because he's not here.
 He's in Ohio. So having an extra monk is always good.
 People like to see.
 One thing that is such a shame that I realized what it is
 and a way of describing it is
 supporting monks and giving food to monks was so that they
 could live.
 It was because you wanted the monks to live. I mean, I
 think that's a really good way of describing it.
 Why do monks go for alms? Because they need to live.
 Why do people give monks alms? Because they want the monks
 to live.
 But it's become, why do you want them to live?
 Because they are our means of, they are our doorway to
 Buddhism, to the Buddhist teaching, to our practice.
 Them living allows us to practice. But that step is missing
 now.
 And it's become giving food to the monks is our practice.
 Which is, I think, a real shame. Now what do you do when
 someone dies? Feed the monks.
 Why? Because that's the religious practice. It's not really
. I mean, yes, it's good.
 It's great for us. We get food. It's good, right? For them.
 They go to heaven.
 It's not really Buddhism. And it's not going to keep
 Buddhism alive.
 We're missing that step. I guess you shouldn't be quite so
 crass as to say, you know,
 we have to feed them because they'll die otherwise. And it
's like feeding a dog kind of, right?
 It's not quite like that. We respect them as well. Feed
 them because we respect them.
 Isn't that the first step though, the generosity and then
 you kind of build from there?
 Well, no. It's not. I mean, it's good and it helps in your
 practice. But the first is sila.
 We don't have to bother with generosity. I think it comes.
 I think generosity is like metta, you know?
 Karuna. Why are we generous out of metta? I don't karuna.
 Out of mudita.
 End out of upekka because when someone needs you don't
 judge them and just give.
 But all of that should come from insight. Not really the
 other way around.
 But so it's like an optional. It's like a support that
 helps you. It can help you in the practice.
 It's a great thing to do. It's a great foundation practice.
 But you shouldn't say it's the first step. It's not a gr
illing.
 We don't want to make it sound like we're looking for
 donations or, you know, looking for charity.
 I guess that's not what it means, right? Because charity is
 giving to poor people, homeless people.
 And to love being your family.
 I must have misunderstood. I remember reading something
 that had three tiers and it was, you know,
 and it was about giving or mira and I thought it kind of
 went generosity, dana, and then sila, and then meditation
 training.
 It's not in the order like that. Those are the three ways
 of performing punya.
 But punya is still generally based on sankhara. It's puny
abi sankhara.
 It's an arahant doesn't have it. It doesn't have the desire
 to give rise to punya.
 Punya is worldly. It leads to heaven.
 So the Buddha praised it in the sense of people want to
 know how to go to heaven.
 So he teaches them how to go to heaven and they're happy.
 And more so because it shows karma, it shows cause and
 effect.
 And ultimately, it's a good thing to talk about and to
 teach because it helps people to let go.
 And it leads people closer to renunciation to let go, to
 not cling.
 It's a good practice for sure. Generosity is awesome.
 Charity. I've always loved being charitable. It's great to
 be charitable.
 I fed a monk this morning. People were charitable to me so
 I was able to be charitable to him.
 And he was charitable to me and that he drove us.
 Charity is awesome because you help other people and they
 want to help you.
 It just makes life so much better. People smile at you.
 People are happy. You're happy.
 Because you feel good. You did something good. Of all the
 other things I've done in my life,
 this good thing and it's concrete. You know that that was a
 good thing.
 Everything else you may have done in your life, all the bad
 things.
 I just did something good and that's concrete.
 Sometimes more concrete than meditation. Meditation is neb
ulous and the beginning can be discouraging because it's
 hard work.
 So it's easy. What my gripe here is that it's so easy that
 people just fall into only giving.
 And the monks are certainly... monks often fall into compl
acency because when people are giving,
 what else do we need to do? They're giving us.
 It doesn't last either. It's unsustainable.
 Because the reason people give in the first place is
 because they understand the dharma and they understand the
 benefits of giving.
 Yeah.
 When I look at pictures of Asia, the big monasteries in
 Thailand and everything, they look so elaborate.
 Is that because of so much giving there, I would imagine?
 Yeah. I mean, you don't see that so much in Sri Lanka.
 Some of the monasteries are impressive, but a lot of them
 are very, very simple.
 Because there's far less of this focus on giving.
 More focus on study.
 Yeah. I mean, people do give and there's a big focus in Sri
 Lanka on giving food.
 So I think it's fairly rare for monks to be hungry in Sri
 Lanka.
 But there's not the obsession with... I mean, there it's
 not even about donation, kind of is,
 but it's donation for the purpose of building. People like
 to build things in Thailand.
 I've been experiencing what I believe to be rapture since I
 started meditating four months ago.
 I feel like I'm fairly equanimous in regards to it.
 Is it normal for imperfections of insight to last this long
, or am I perhaps missing something?
 Well, it depends on what kind of rapture it is. What is it
 that you perceive as being rapture?
 If it's like the actual rocking back and forth kind of rapt
ure, then that's something that you should stop.
 And one teacher said you should actually tell yourself, "
Stop. Make it stop."
 Like rapture, as an imperfection of insight, it's this
 getting into a rut when it becomes like a broken record.
 Anything like that is an imperfection of insight in the
 sense that it's distracting.
 It's not really bad, but it will distract you from practice
.
 And there's a subtle nikanti. The number 10 is nikanti,
 which is subtle appreciation or enjoyment of it.
 Partiality towards it sustains it. And that's hard to see.
 And that's why it's useful to just stop it.
 Have you heard how different is the experience of dying
 mindful and dying without mindfulness?
 [Silence]
 Dying without mindfulness is a lot like relay, really. I
 mean, it's a wheel of fortune or whatever.
 It's quite random in many ways. Not exactly random, but
 there is a lot of unpredictability.
 It depends what in the last moment you cling to. And that
 isn't always a clear...
 It isn't something you can easily know in advance.
 So if you're mindful, then you don't cling so much to
 anything.
 Certainly don't cling to bad things. You're cling to more
 subtle, more refined things that you can't easily be
 mindful of.
 Of course, if you're perfectly mindful, you don't cling to
 anything, and there's no...
 If you're unmindful, you might cling to the first thing
 that comes along, and who knows what that is.
 If you cling to it strongly enough, you'll be reborn based
 on that.
 Simon wants to know if you're going to record your ceremony
 on the 20th.
 I think the ceremony is on the 20th, right?
 Good question. I probably won't. If there's someone else
 around, do well.
 Thank you. I'll be orchestrating everything, trying to make
 sure the monks get fed.
 You can just leave the live stream up, right?
 You know, I really don't... I'm not that keen to send video
 equipment and move stuff around.
 And Simon thinks you should have your teacher on as an
 interview.
 I tried to record him once, and he would have nothing to do
 with it.
 Last time I was in Thailand, I tried to record him.
 No, it could... Did he have any message for the
 International Community?
 He just wouldn't do it.
 Feelings of hands touching, massaging my head, along with
 feelings of energy flowing all around it, all above it, all
 over it.
 That was the rapture, regarding the rapture.
 I've never heard of that.
 That's interesting.
 In your head, yeah, I mean, it's just the energy, it's
 impermanent suffering and non-self.
 It's a big one. There would probably be non-self in the
 sense that it's not something you can make go away.
 So as long as you see it for what it is, you can just
 ignore it if it lasts.
 And when it comes back, acknowledge again.
 Be careful of any frustration you might have that it's not
 going away.
 But certainly be careful of any enjoyment of it.
 That's all. It's not a problem, it's just a sensation.
 What are your thoughts on interfaith marriage or
 relationships, particularly between a Buddhist and a
 Christian?
 Why would you marry a Christian?
 I'm often... It's a little bit weird to me, but people don
't take religion as seriously as I do.
 For me, I couldn't imagine marrying my marrying, but
 marrying a Christian.
 It just seems really odd, just because religion is an
 important thing for me.
 Could I? I mean, I couldn't, but I couldn't imagine
 marrying someone who's not a Buddhist.
 I think people do it because most people don't take
 religion very seriously.
 But a practicing Buddhist, I think, would have a hard time
 marrying anyone of any religion.
 I've heard of Buddhists who married Muslims, and just the
 horror that it brings to their families,
 because most Southeast Asian Buddhists are not vegetarian,
 and that's not a problem.
 But the problem is the meat that they cook.
 Their children can't eat their food anymore.
 And things like just won't eat off the dishes, and really
 weird stuff.
 Not weird stuff, but just stuff that drives their parents,
 and the stress that it brings.
 I can't imagine what the things people do when they're in
 love, right?
 I guess that's what it is.
 You fall in love, and you think you've met your soulmate,
 and you ignore the problems.
 Can you please talk more on Heaven and Hell?
 It's almost 10 o'clock. I'm going to sleep.
 I want to learn about Heaven and Hell and go read about it.
 How about what's a good suta to read on Heaven and Hell?
 The Bala Pandita Suta.
 And let's see.
 Jiminica 129, I think.
 The Bala Pandita Suta.
 That's right.
 Hell. This is the suta I read to these kids who came and
 ordained as novices,
 and then that night they couldn't sleep alone.
 Were it rightly speaking to be said of anything that is
 utterly unwished for,
 utterly undesired, or lusting?
 Utterly disagreeable. It is of Hell that, rightly speaking,
 this should be said.
 So much so that it is hard to find a simile for the
 suffering in Hell.
 And then one of the monks pipes up.
 But venerable sir, can a simile be given?
 It can be, K
 The blessed one said.
 And then he goes on.
 Drew says you look drained, but I guess that's probably
 what happens when one
 goes back to university and establishes a monastery all in
 one week.
 Well, when you're only in the morning, 10 o'clock at night
 is already, you know,
 it's not even just the work or that I've done a lot of work
.
 I'm not doing physical labor, but only eating in the
 morning.
 You know, I was teaching from five to seven. I've already
 done a teaching.
 And to do a second teaching, yeah, the brain doesn't work
 so well.
 Sorry.
 I think you did fine. Thank you, Monty.
 Thank you, Robin. I think we'll quit there then. Good night
, everyone.
 Good night.
