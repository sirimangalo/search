 [ Silence ]
 >> Good evening everyone.
 We're broadcasting live January 29, 2016.
 [ Silence ]
 Today's quote, today's quote is about, well,
 it's about fairly mundane things.
 If you're noticing a pattern, a lot
 of these quotes are Buddhism light.
 It's a bit worrisome because it's easy to get complacent
 if you think of Buddhism as a social philosophy
 to help people live well in the world.
 It's easy to become complacent thinking you can just be
 in generous and do good deeds in the world and that's
 enough.
 Now that this is not a Buddhist teaching,
 it's just an emphasis on worldly aspects of the teaching.
 It's, well, for our purposes it's problematic
 because we're all here interested
 in the core teachings of the Buddha.
 It's hard to get to the core teachings if you keep talking
 about good teachings but good basic teachings like charity
 and living a good household life.
 It doesn't talk about how to address the issues.
 But one thing this quote does mention is to be modest
 about your wealth and to not get upset when it goes away.
 And so that at least hints at a mental development.
 Because it's easy to say don't get upset
 when you lose the things you love
 but it's really impossible in practice.
 If you love something you will be upset when it goes away.
 Can't stop that.
 When you don't get what you want there's a yearning for it.
 When the yearning is unsatisfied or suffering.
 (silence)
 And so this begins to hint at the need for practice.
 (speaks in foreign language)
 To make your mind such that when you're touched
 by the vicissitudes of life, the changes in life,
 impermanence of life, touched by good and evil,
 touched by pleasant and unpleasant.
 When the mind doesn't waver,
 when the mind doesn't cling, doesn't push or pull.
 (silence)
 We have to find the practice that leads us to this.
 (silence)
 Because the theory is quite simple.
 That experience itself isn't capable of causing us
 suffering,
 it's our reactions to our experiences that cause us
 suffering.
 If we didn't react, if we saw things objectively,
 we wouldn't suffer from them.
 (silence)
 If someone's yelling at you, it's only sound until you let
 it get to you.
 (silence)
 If you have great pain, it's only pain until you let it get
 to you,
 hot and cold and hard and soft.
 (silence)
 It's amazing what as a meditator you can bear
 and you can endure without suffering.
 When I first went to meditate, we had to sleep on the hard
wood floor,
 which is the very thing that...
 I mean, it's not like I was in great hardship,
 but this was one thing that I remember.
 If you don't have much fat on you,
 sleeping like that is very important.
 Sleeping like that is very painful, but you know,
 one night I slept under a park bench
 and it was really comfortable on the cement,
 in bitter cold as well.
 (silence)
 There was a story behind that,
 but I always tell this story about this park bench because
 there was so much...
 It was a really complicated situation
 and people were pushing and pulling and
 finally I just walked out and went and stayed under a park
 bench.
 And it felt so great because there was no...
 nobody, no, I didn't have to...
 concern myself with politics or opinions and so on, people.
 But the point here is...
 it's all in the mind.
 Some people...
 The Buddha said even in a comfortable bed,
 a comfortable chair and great misery, great suffering,
 great stress, their minds are filled with lust,
 they're filled with anger, they're filled with delusion.
 Those are the three bad things, the three things that
 poison our mind.
 Our minds are full of them.
 It doesn't matter what comforts we feel,
 comforts we get, we still suffer.
 So our practice is quite simple.
 We just remind ourselves of what things are, keeps us
 objective.
 When you do it, you can see...
 Some people doubt this technique and that doubt gets in the
 way.
 They don't give it an honest try.
 But as soon as you give it an honest try,
 you can see in that moment the clarity,
 the mind is clear, the mind is pure.
 It's amazing teaching people five minute meditation.
 I bet a lot of them will forget about it,
 but you give them one moment, you give them that minute
 where they come out of like a one minute meditation
 where we do at the end just about one minute
 and they rave about it because they've seen it,
 they've seen that moment.
 It takes us one moment.
 So when you doubt about the practice,
 when you're not sure, you should scold yourself
 because if you've been practicing at all, you've seen the
 moment,
 you've seen that moment, you've had that one moment.
 Later when you doubt, "Is this really helping?"
 you should call yourself foolish for thinking that.
 You should call yourself out on it, absolutely.
 We trick ourselves, "Is this really good?"
 It's like a person... This happens, you know,
 a person gets...
 We get free from an illness and then we become reckless
 again.
 We get a terrible illness and we suffer terribly
 and then when we get free from it,
 we act as though it never happened.
 We don't admit our vulnerability to dealness.
 We don't work to try to change or to train our mind.
 We're very good at forgetting.
 We should try to practice remembering.
 And so if you want to know real progress in the practice,
 think about the moments.
 In this moment when I'm mindful, see the result.
 All it takes is a moment.
 So that's a little bit for tonight.
 Tomorrow at 3 pm Eastern Time, mid... 12 pm,
 midday, second lifetime, I'll be giving a talk
 in Second Life at the Buddha Center.
 So if you know what that is, see you there.
 Have a good night.
