1
00:00:00,000 --> 00:00:05,000
 Good evening and welcome back to our study of the Dhampada.

2
00:00:05,000 --> 00:00:10,970
 Today we continue on with first number 150, which reads as

3
00:00:10,970 --> 00:00:12,000
 follows.

4
00:00:12,000 --> 00:00:19,620
 Atinang nakarang katang, manksalohita leipanang, yata jaraj

5
00:00:19,620 --> 00:00:27,000
am ajuja, mano makhoja hohedo.

6
00:00:27,000 --> 00:00:36,730
 Which means nagarangasiti, atinangatang, a city made of

7
00:00:36,730 --> 00:00:38,000
 bones.

8
00:00:38,000 --> 00:00:47,220
 Mangsalohita leipanang that is plastered with flesh and

9
00:00:47,220 --> 00:00:49,000
 blood.

10
00:00:49,000 --> 00:00:59,310
 Yata jarajam ajuja, wherein old age and death, mano makho,

11
00:00:59,310 --> 00:01:09,000
 conceit and deceit, ohito dwell or are lodged

12
00:01:09,000 --> 00:01:24,620
 or descend upon, I'm actually not sure about ohita, are,

13
00:01:24,620 --> 00:01:30,000
 lie, wherein lie, old age and death, conceit and deceit,

14
00:01:30,000 --> 00:01:39,800
 conceit and treachery or crookedness. So remember we're in

15
00:01:39,800 --> 00:01:42,000
 the Jaravaga, I think, right?

16
00:01:42,000 --> 00:01:46,600
 Jaravaga, which means old age. So these are all going to be

17
00:01:46,600 --> 00:01:49,000
 about old age, death, that kind of thing,

18
00:01:49,000 --> 00:01:52,270
 the body, they're not going to be very pleasant, most

19
00:01:52,270 --> 00:01:56,000
 likely. They haven't been so far.

20
00:01:56,000 --> 00:02:00,530
 This story, another one of those memorable stories, most of

21
00:02:00,530 --> 00:02:04,000
 them are fairly memorable, somewhat a fantastical story,

22
00:02:04,000 --> 00:02:12,200
 so if you're not a fan of the miraculous, bear with it

23
00:02:12,200 --> 00:02:16,000
 because it's not so much in the story,

24
00:02:16,000 --> 00:02:24,600
 it's much more in the lesson that it has. At any rate, the

25
00:02:24,600 --> 00:02:36,000
 story goes that this Buddhist nun, Rupananda,

26
00:02:36,000 --> 00:02:49,250
 who is supposed to be the Buddha's cousin, or her steps

27
00:02:49,250 --> 00:02:52,000
ister, sister-in-law, something like that,

28
00:02:52,000 --> 00:03:01,460
 some relative of the Buddha, Rupananda, who was very

29
00:03:01,460 --> 00:03:08,000
 beautiful. She was called Janapandakalyaning,

30
00:03:08,000 --> 00:03:14,590
 which means the beauty of the land, most beautiful girl in

31
00:03:14,590 --> 00:03:16,000
 the country.

32
00:03:16,000 --> 00:03:20,020
 She heard that, well, like many of the Buddha's relatives,

33
00:03:20,020 --> 00:03:23,000
 everybody's becoming monks, and non-monks,

34
00:03:23,000 --> 00:03:32,010
 female monks, male monks. So she said, like many of them,

35
00:03:32,010 --> 00:03:35,000
 let's follow along, hey, I'll do it too.

36
00:03:35,000 --> 00:03:40,840
 Not really knowing or having any conception of what exactly

37
00:03:40,840 --> 00:03:43,000
 the Buddha taught.

38
00:03:43,000 --> 00:03:50,000
 And quite quickly she found, she became aware of the fact

39
00:03:50,000 --> 00:03:54,000
 that the Buddha's teaching didn't have much good

40
00:03:54,000 --> 00:04:02,000
 and disabled physical beauty, that physical beauty is imper

41
00:04:02,000 --> 00:04:10,000
manent, and it's impermanent, unsatisfying, uncontrollable,

42
00:04:10,000 --> 00:04:13,880
 to cause for suffering, something that you shouldn't think

43
00:04:13,880 --> 00:04:19,000
 of as me or mine, it's a cause for ego, conceit,

44
00:04:19,000 --> 00:04:25,280
 which wasn't a very pleasing sort of teaching for her. And

45
00:04:25,280 --> 00:04:29,000
 moreover, it made her feel kind of nervous.

46
00:04:29,000 --> 00:04:33,310
 She misunderstood that somehow the Buddha would look down

47
00:04:33,310 --> 00:04:39,000
 upon her and criticize her for being beautiful, right?

48
00:04:39,000 --> 00:04:42,410
 Because beauty is, she thought, she understood beauty to be

49
00:04:42,410 --> 00:04:45,000
 a bit of a problem in the Buddha's teaching.

50
00:04:45,000 --> 00:04:51,380
 But at any rate, well, perhaps, but at any rate, the whole

51
00:04:51,380 --> 00:04:56,710
 idea that the body is impermanent, unsatisfying, uncontroll

52
00:04:56,710 --> 00:04:57,000
able,

53
00:04:57,000 --> 00:05:00,900
 cause for suffering, a cause for ego and so on, it's not

54
00:05:00,900 --> 00:05:05,000
 really comfortable for her because she was probably

55
00:05:05,000 --> 00:05:10,290
 quite pleased with her beauty and had received praise from

56
00:05:10,290 --> 00:05:15,000
 an early age about her beauty as a result,

57
00:05:15,000 --> 00:05:19,290
 and most likely gained quite a bit of conceit and crooked

58
00:05:19,290 --> 00:05:31,000
ness. Let's see where this is going.

59
00:05:31,000 --> 00:05:33,900
 So she thought, well, rather than have the Buddha criticize

60
00:05:33,900 --> 00:05:37,000
 me for being beautiful, or, you know,

61
00:05:37,000 --> 00:05:41,020
 whether being, having my beauty diminished in some way or

62
00:05:41,020 --> 00:05:44,000
 the importance of my beauty diminished,

63
00:05:44,000 --> 00:05:47,180
 she thought, I'll just never go to see the Buddha. And so

64
00:05:47,180 --> 00:05:50,560
 she did whatever she could to avoid having to see the

65
00:05:50,560 --> 00:05:54,000
 Buddha for quite some time.

66
00:05:54,000 --> 00:05:58,920
 Fortunately for her, living in the monastery and with lay

67
00:05:58,920 --> 00:06:05,000
 people coming and going from the female monk monastery,

68
00:06:05,000 --> 00:06:09,660
 she ended up hearing a good report about the Buddha. To put

69
00:06:09,660 --> 00:06:14,760
 it mildly, she began to hear all these wonderful things

70
00:06:14,760 --> 00:06:20,000
 people were saying about the Buddha.

71
00:06:20,000 --> 00:06:25,040
 How impressive he was, how profound his teachings, how

72
00:06:25,040 --> 00:06:29,370
 enlightening it was, how amazing and marvelous it was just

73
00:06:29,370 --> 00:06:31,000
 to see and to hear him teach,

74
00:06:31,000 --> 00:06:34,950
 to receive instruction and to practice in the presence of

75
00:06:34,950 --> 00:06:38,000
 the Buddha. To the point where, you know,

76
00:06:38,000 --> 00:06:42,060
 reasonably, understandably, imagine being so close to the

77
00:06:42,060 --> 00:06:49,300
 Buddha, but having never gone to see him, she became

78
00:06:49,300 --> 00:06:50,000
 curious,

79
00:06:50,000 --> 00:06:54,000
 but interested and quite keen actually to see the Buddha.

80
00:06:54,000 --> 00:06:57,470
 And she wrestled with this because it was quite serious for

81
00:06:57,470 --> 00:06:59,000
 her that what he would say about her

82
00:06:59,000 --> 00:07:03,230
 or the kind of things he would say would embarrass her,

83
00:07:03,230 --> 00:07:07,370
 humiliate her for being beautiful or at the very least

84
00:07:07,370 --> 00:07:10,000
 diminish the value of her beauty.

85
00:07:10,000 --> 00:07:15,350
 But she said to herself, well, you know, suppose he were to

86
00:07:15,350 --> 00:07:21,000
 talk all day about how awful beauty is or how useless it is

87
00:07:21,000 --> 00:07:23,000
 or how impermanent it is.

88
00:07:23,000 --> 00:07:29,140
 How much could he possibly say, right? It's just words. She

89
00:07:29,140 --> 00:07:33,000
 said, in the end, she said, I gotta go see him.

90
00:07:33,000 --> 00:07:36,100
 Because she finally made up her mind that she would go to

91
00:07:36,100 --> 00:07:39,000
 see and learn from the Buddha, listen to what he had to say

92
00:07:39,000 --> 00:07:43,000
 and hopefully gain some understanding.

93
00:07:43,000 --> 00:07:45,880
 Everyone heard, of course, and they were all excited

94
00:07:45,880 --> 00:07:48,000
 because, oh, something special.

95
00:07:48,000 --> 00:07:51,380
 Rupananda, because they knew she'd never gone to see the

96
00:07:51,380 --> 00:07:53,580
 Buddha, and they said, he's going to give a special

97
00:07:53,580 --> 00:07:54,000
 teaching for her.

98
00:07:54,000 --> 00:07:56,850
 This is going to be a special occasion. For the first time,

99
00:07:56,850 --> 00:07:59,600
 she'll get to see the Buddha and the Buddha himself, using

100
00:07:59,600 --> 00:08:04,510
 his special powers, was able to think or not be able to

101
00:08:04,510 --> 00:08:07,000
 think thought to himself.

102
00:08:07,000 --> 00:08:10,790
 He'll have to do something special for her. And so she sat

103
00:08:10,790 --> 00:08:13,730
 at the back kind of out of the way where, I don't know

104
00:08:13,730 --> 00:08:15,000
 where she sat actually,

105
00:08:15,000 --> 00:08:22,830
 I think she sat at the back where she wouldn't be seen by

106
00:08:22,830 --> 00:08:25,000
 the Buddha.

107
00:08:25,000 --> 00:08:28,110
 She said, I will not let him see who I am. So she covered

108
00:08:28,110 --> 00:08:34,260
 herself up and stood at the back, but there's no fooling

109
00:08:34,260 --> 00:08:37,000
 the Buddha.

110
00:08:37,000 --> 00:08:42,030
 And so when the Buddha taught, before he taught, he used

111
00:08:42,030 --> 00:08:50,100
 his magical, his special powers to create or to control her

112
00:08:50,100 --> 00:08:52,000
 mind in some way,

113
00:08:52,000 --> 00:08:56,220
 to create an impression on her mind that there was a

114
00:08:56,220 --> 00:09:00,000
 beautiful woman behind the Buddha fanning him.

115
00:09:00,000 --> 00:09:08,100
 Nobody else could see it. He didn't actually create this

116
00:09:08,100 --> 00:09:15,890
 vision, but he somehow made her to see this exceedingly

117
00:09:15,890 --> 00:09:21,000
 beautiful woman fanning the Buddha.

118
00:09:21,000 --> 00:09:25,470
 This was a common thing. I mean, you're talking India, we

119
00:09:25,470 --> 00:09:29,610
're in the hot season, it can get quite hot. And they didn't

120
00:09:29,610 --> 00:09:33,000
 have fans, electric fans, obviously, for the teacher.

121
00:09:33,000 --> 00:09:37,130
 It was quite common for someone to stand, one of the monks

122
00:09:37,130 --> 00:09:40,840
 often to stand and fan. I've done it before, I did it for

123
00:09:40,840 --> 00:09:42,000
 Ajahn Dang.

124
00:09:42,000 --> 00:09:46,940
 I would take his fan and fan him when there was an outdoor

125
00:09:46,940 --> 00:09:52,200
 ceremony and he had to sit for a long time. It's one of

126
00:09:52,200 --> 00:09:56,000
 those great gifts to a teacher.

127
00:09:56,000 --> 00:10:02,790
 So it was a common thing. And Rupananda saw her and was

128
00:10:02,790 --> 00:10:10,470
 shocked because this woman was actually, her beauty, it has

129
00:10:10,470 --> 00:10:15,000
 made Rupananda look like a crow.

130
00:10:15,000 --> 00:10:19,570
 Crows are not ugly, but I guess the idea is something that

131
00:10:19,570 --> 00:10:25,160
's quite ugly. She compared herself to a crow standing

132
00:10:25,160 --> 00:10:28,000
 before a royal goose.

133
00:10:28,000 --> 00:10:32,470
 And interested in her because, well, here was another

134
00:10:32,470 --> 00:10:36,670
 beautiful woman who appeared to be quite close to the

135
00:10:36,670 --> 00:10:39,000
 Buddha and she seemed happy.

136
00:10:39,000 --> 00:10:42,290
 And so suddenly she started to pay attention and she lost

137
00:10:42,290 --> 00:10:45,630
 her nervousness and started listening to what the Buddha

138
00:10:45,630 --> 00:10:48,000
 said and watching this woman.

139
00:10:48,000 --> 00:10:55,020
 And so the Buddha taught. But as he taught, she would look

140
00:10:55,020 --> 00:10:59,000
 back at this beautiful woman, entranced by her beauty.

141
00:10:59,000 --> 00:11:03,180
 And she looked and suddenly the woman had aged. A girl of

142
00:11:03,180 --> 00:11:07,350
 16, she was now a woman, perhaps a woman who had been

143
00:11:07,350 --> 00:11:11,640
 through childbirth, it says, lost a little bit of her

144
00:11:11,640 --> 00:11:14,000
 youthful figure.

145
00:11:14,000 --> 00:11:25,000
 And she thought, "Oh, that's not the same as it was before

146
00:11:25,000 --> 00:11:26,000
."

147
00:11:26,000 --> 00:11:30,300
 And so she was kind of confused and she kept watching. And

148
00:11:30,300 --> 00:11:34,930
 as she watched, the Buddha caused the image to change

149
00:11:34,930 --> 00:11:38,000
 further to a middle-aged woman.

150
00:11:38,000 --> 00:11:42,070
 And then slowly, slowly the middle-aged woman became old

151
00:11:42,070 --> 00:11:48,230
 and decrepit and bent like an A-frame. I said, "Hunch back

152
00:11:48,230 --> 00:11:49,000
."

153
00:11:49,000 --> 00:11:53,730
 And she said to herself in the Pali idampi antra hitang, "

154
00:11:53,730 --> 00:11:58,000
This has disappeared. That's disappeared."

155
00:11:58,000 --> 00:12:02,150
 Realizing, you know, the point the text is making is she

156
00:12:02,150 --> 00:12:06,000
 was able to see this thing that she was clinging to,

157
00:12:06,000 --> 00:12:12,000
 beautiful, beautiful, gone, beautiful, gone, ugly.

158
00:12:12,000 --> 00:12:15,260
 Realizing impermanence is a very useful sort of

159
00:12:15,260 --> 00:12:19,330
 conventional means of helping someone cultivate imperman

160
00:12:19,330 --> 00:12:25,000
ence. It's a construct or an artifice.

161
00:12:25,000 --> 00:12:29,840
 And finally, as you can imagine, this cripple old woman,

162
00:12:29,840 --> 00:12:34,790
 suddenly the Buddha had her collapse. She gives out a large

163
00:12:34,790 --> 00:12:39,330
 wail or moan or something and collapses on the floor and

164
00:12:39,330 --> 00:12:46,000
 starts dying basically, in great pain and suffering.

165
00:12:46,000 --> 00:12:51,250
 And it says rolling around in her urine and feces, imagine

166
00:12:51,250 --> 00:12:55,000
 the sight, and then lying still dead.

167
00:12:55,000 --> 00:13:01,610
 And Rupananda is just watching in utter amazement and

168
00:13:01,610 --> 00:13:09,070
 watches as the corpse, now a corpse, begins to, quite much

169
00:13:09,070 --> 00:13:18,020
 quickly than it would naturally, of course, starts to bloat

170
00:13:18,020 --> 00:13:20,000
 and break.

171
00:13:20,000 --> 00:13:27,980
 And liquid starts to pour out of all the orifices and then

172
00:13:27,980 --> 00:13:36,150
 it breaks apart and pus and blood comes out and maggots and

173
00:13:36,150 --> 00:13:44,000
 so on, according to the progression of the corpse.

174
00:13:44,000 --> 00:13:50,300
 And you think, well, nice show, but what does it mean? But

175
00:13:50,300 --> 00:13:54,800
 I think it should be quite clear that the profundity of

176
00:13:54,800 --> 00:13:57,000
 this is that this is us.

177
00:13:57,000 --> 00:14:02,590
 This is what she's seeing as a story of life. This isn't

178
00:14:02,590 --> 00:14:06,000
 some hypothetical.

179
00:14:06,000 --> 00:14:11,090
 It's perhaps the vision that resonates the strongest with a

180
00:14:11,090 --> 00:14:17,000
 human being is the vision of old age, sickness and death.

181
00:14:17,000 --> 00:14:22,030
 And it resonated with her and she realized this beauty that

182
00:14:22,030 --> 00:14:24,900
 she had. I mean, obviously she's not going to age that

183
00:14:24,900 --> 00:14:27,000
 quickly, but it's going to come to her as well.

184
00:14:27,000 --> 00:14:33,300
 This is exactly the fate that she will one day hope for or

185
00:14:33,300 --> 00:14:35,000
 expect for.

186
00:14:35,000 --> 00:14:38,440
 And so, I mean, this isn't vipassana exactly, but it's a

187
00:14:38,440 --> 00:14:41,850
 great samatha practice that we use often while looking at

188
00:14:41,850 --> 00:14:43,000
 dead corpses.

189
00:14:43,000 --> 00:14:48,250
 But the idea is that it really sets the stage. It really

190
00:14:48,250 --> 00:14:52,510
 prepares one, an understanding of true and permanent

191
00:14:52,510 --> 00:14:55,500
 suffering and non-self because it makes you a lot more

192
00:14:55,500 --> 00:14:57,000
 objective about the body.

193
00:14:57,000 --> 00:15:00,370
 You lose a lot of the conceit, all of the crookedness

194
00:15:00,370 --> 00:15:05,330
 surrounding the body, using your body, thinking that people

195
00:15:05,330 --> 00:15:11,250
 are attracted to your body and finding ways to make up your

196
00:15:11,250 --> 00:15:18,000
 body, clean your body, print your body.

197
00:15:18,000 --> 00:15:22,450
 You feel strong in the body. We do physical sports and

198
00:15:22,450 --> 00:15:27,020
 exercises to try and make the body more competent, more

199
00:15:27,020 --> 00:15:29,000
 able, more potent.

200
00:15:29,000 --> 00:15:33,420
 We eat foods and so on. Or maybe the opposite. Maybe we lo

201
00:15:33,420 --> 00:15:36,450
athe our body and we feel disgusted. We feel self-conscious

202
00:15:36,450 --> 00:15:40,500
 of it. We're fat. We're ugly. We're tall. We're short. We

203
00:15:40,500 --> 00:15:43,000
're thin.

204
00:15:43,000 --> 00:15:49,460
 And so on. With crooked nose, crooked teeth, our hair is

205
00:15:49,460 --> 00:15:55,030
 the wrong color, our hair is and so on, so on. Our ears are

206
00:15:55,030 --> 00:16:04,000
 too big, whatever. Anything and everything.

207
00:16:04,000 --> 00:16:08,960
 So looking at watching a body die, imagine watching a body

208
00:16:08,960 --> 00:16:13,180
 decompose in front of your eyes. It kind of puts a damper

209
00:16:13,180 --> 00:16:15,630
 on all that. It puts it all in perspective when you start

210
00:16:15,630 --> 00:16:21,510
 to say, "Really, why am I so obsessed with this useless

211
00:16:21,510 --> 00:16:27,000
 body that's going to get old, sick and die?"

212
00:16:27,000 --> 00:16:33,180
 That's like a burden, like a corpse around one's neck. And

213
00:16:33,180 --> 00:16:36,360
 so the Buddha began to teach her. He taught a verse that is

214
00:16:36,360 --> 00:16:39,930
 not the verse that we have, but it's a bunch of verses

215
00:16:39,930 --> 00:16:41,000
 actually.

216
00:16:41,000 --> 00:16:44,430
 Let's read. Let's look at the... The Pali is quite

217
00:16:44,430 --> 00:16:48,130
 beautiful, but I won't bore you with it. Oh, it's actually

218
00:16:48,130 --> 00:16:49,000
 quite good.

219
00:16:49,000 --> 00:16:53,350
 "As is this body..." First he talks about the body is dise

220
00:16:53,350 --> 00:16:58,520
ased in pure putrid. It oozes in leaks, yet it is desired of

221
00:16:58,520 --> 00:17:00,000
 simpletons.

222
00:17:00,000 --> 00:17:14,800
 "As is this body, so also was that." "Yata i dang, data i d

223
00:17:14,800 --> 00:17:20,090
ang." "Yata i dang, data i dang." "As that body, so is that

224
00:17:20,090 --> 00:17:21,000
 body." "As that body, so is this body."

225
00:17:21,000 --> 00:17:25,120
 We are like them. This is what the Buddha was teaching to

226
00:17:25,120 --> 00:17:29,500
 her. Basically what I was sort of saying, I was just saying

227
00:17:29,500 --> 00:17:34,000
, but in much more words, not quite as well as the Buddha.

228
00:17:34,000 --> 00:17:41,000
 "Behold the elements in their emptiness."

229
00:17:41,000 --> 00:17:47,740
 "Dah tuto sun yatobasa." Look at these elements that are

230
00:17:47,740 --> 00:17:53,000
 empty. "Behold the emptiness of the elements."

231
00:17:53,000 --> 00:17:57,070
 What does that mean? The emptiness of the elements means

232
00:17:57,070 --> 00:18:03,770
 that there is no essence, there is nothing permanent, that

233
00:18:03,770 --> 00:18:06,000
 is a thing.

234
00:18:06,000 --> 00:18:11,300
 Because change means it can actually be a real thing. What

235
00:18:11,300 --> 00:18:13,560
 would it mean to say that some thing changes? It's not

236
00:18:13,560 --> 00:18:17,000
 actually a thing anymore, it's just a process.

237
00:18:17,000 --> 00:18:22,640
 And that's what the body is. This body is not a thing, it's

238
00:18:22,640 --> 00:18:24,000
 a process.

239
00:18:24,000 --> 00:18:37,370
 And the Buddha says as much, he says, "Go not back to the

240
00:18:37,370 --> 00:18:40,000
 world."

241
00:18:40,000 --> 00:18:48,910
 "Cast away desire for existence and you shall walk in

242
00:18:48,910 --> 00:18:52,000
 tranquility."

243
00:18:52,000 --> 00:18:54,630
 I don't know where this first comes from. This is

244
00:18:54,630 --> 00:18:59,270
 apparently something the Buddha said, but it's not

245
00:18:59,270 --> 00:19:02,000
 canonical, I don't think.

246
00:19:02,000 --> 00:19:07,710
 To any right, then the Buddha says the idea that there is

247
00:19:07,710 --> 00:19:13,000
 no "sara," "imasming sari reis saro ati mas sanyang kari."

248
00:19:13,000 --> 00:19:19,600
 Don't create the conception in your mind that there is any

249
00:19:19,600 --> 00:19:25,000
 essence or importance or value to the body.

250
00:19:25,000 --> 00:19:33,850
 "Apa patakopi hi ita saro nati." The smallest bit of

251
00:19:33,850 --> 00:19:46,000
 essence or importance in this body does not exist, nati.

252
00:19:46,000 --> 00:19:52,870
 It is made up of 300 bones, smeared and plastered with

253
00:19:52,870 --> 00:19:57,000
 blood and flesh and skin and so on.

254
00:19:57,000 --> 00:20:00,000
 And then he gave the verse.

255
00:20:00,000 --> 00:20:03,940
 So how this relates to our practice should be fairly clear

256
00:20:03,940 --> 00:20:05,000
 in two ways.

257
00:20:05,000 --> 00:20:09,530
 The first is the second part of the verse here where it

258
00:20:09,530 --> 00:20:13,000
 says, "mano makhojo hito yatajara."

259
00:20:13,000 --> 00:20:17,880
 Actually, what the verse explicitly states is that we

260
00:20:17,880 --> 00:20:22,000
 become conceited and attached to the body.

261
00:20:22,000 --> 00:20:29,010
 It's an important lesson, an important delusion to dispel

262
00:20:29,010 --> 00:20:32,000
 because the body is certainly not worth clinging to,

263
00:20:32,000 --> 00:20:40,000
 being conceited about, attached to, desires of and so on.

264
00:20:40,000 --> 00:20:45,280
 It's not going to last, it's not stable, it can never be a

265
00:20:45,280 --> 00:20:48,000
 source of true happiness.

266
00:20:48,000 --> 00:20:51,300
 But the more important lesson I think actually surprisingly

267
00:20:51,300 --> 00:20:54,000
 really comes from the story, not from the verse.

268
00:20:54,000 --> 00:21:02,090
 But it's implicit in the verse and it's the more ultimate

269
00:21:02,090 --> 00:21:08,000
 reality of impermanent suffering and non-self.

270
00:21:08,000 --> 00:21:12,330
 This idea of seeing things arise and cease that comes on a

271
00:21:12,330 --> 00:21:18,000
 conceptual level, from seeing old age sickness and death.

272
00:21:18,000 --> 00:21:23,800
 But it can also be seen, or it needs eventually to be seen

273
00:21:23,800 --> 00:21:26,000
 on a momentary level.

274
00:21:26,000 --> 00:21:32,160
 And the true way to understand the nature of the body and

275
00:21:32,160 --> 00:21:34,000
 to free yourself from any kind of attachment to it

276
00:21:34,000 --> 00:21:37,660
 really is the practice of insight meditation. It's all fine

277
00:21:37,660 --> 00:21:39,000
 and good to look at a corpse

278
00:21:39,000 --> 00:21:46,520
 and to gain the profound seeming appreciation for the imper

279
00:21:46,520 --> 00:21:49,000
manence of the body.

280
00:21:49,000 --> 00:21:52,570
 But it's quite another thing to actually see momentary

281
00:21:52,570 --> 00:21:55,000
 arising and see saving experience

282
00:21:55,000 --> 00:21:57,650
 and to realize that the body doesn't exist in the first

283
00:21:57,650 --> 00:21:58,000
 place.

284
00:21:58,000 --> 00:22:01,000
 And all that there is is this process.

285
00:22:01,000 --> 00:22:04,200
 And to see how disconnected from that reality is our

286
00:22:04,200 --> 00:22:05,000
 perception.

287
00:22:05,000 --> 00:22:08,670
 Where we perceive the body as being beautiful, it could

288
00:22:08,670 --> 00:22:11,000
 never be a part of the process.

289
00:22:11,000 --> 00:22:15,280
 How we see it conceive the body as being strong, as being

290
00:22:15,280 --> 00:22:18,000
 solid, as being me and mine.

291
00:22:18,000 --> 00:22:21,230
 And all that has no place in reality, which is changing

292
00:22:21,230 --> 00:22:25,000
 constantly in moments and moments of experience.

293
00:22:26,000 --> 00:22:33,000
 [silence]

294
00:22:33,000 --> 00:22:36,000
 It's a challenge to us.

295
00:22:36,000 --> 00:22:42,000
 And it's an important subject to talk about, to realize.

296
00:22:42,000 --> 00:24:58,640
 Because as we talk about we are able to see, to talk about

297
00:24:58,640 --> 00:24:58,640
 it we are able to see and experience our attachments to the

298
00:24:58,640 --> 00:22:51,000
 body.

299
00:22:51,000 --> 00:23:02,000
 And it's a challenge to strive to understand this.

300
00:23:02,000 --> 00:23:07,000
 Why the disconnect? To see through the disconnect.

301
00:23:07,000 --> 00:23:12,490
 Whereby we think of something as beautiful, attractive, me,

302
00:23:12,490 --> 00:23:17,480
 mine, worthy of possessing or possessed by me, controlled

303
00:23:17,480 --> 00:23:19,000
 by me, etc.

304
00:23:19,000 --> 00:23:24,300
 With the disconnect between that and the reality, whereby

305
00:23:24,300 --> 00:23:28,960
 it's none of that, where it's quite clearly doesn't even

306
00:23:28,960 --> 00:23:30,000
 exist.

307
00:23:30,000 --> 00:23:34,310
 A lot of our practice is like this. It's about bridging the

308
00:23:34,310 --> 00:23:39,000
 disconnect between our perception and reality.

309
00:23:39,000 --> 00:23:44,750
 Where eventually our view of things comes in line and can

310
00:23:44,750 --> 00:23:51,000
 change radically as a result, as it comes in line with the

311
00:23:51,000 --> 00:23:55,000
 truth, with what's really going on.

312
00:23:55,000 --> 00:23:58,530
 And of course that's very much why we suffer. We suffer

313
00:23:58,530 --> 00:24:00,000
 because of the disconnect.

314
00:24:00,000 --> 00:24:05,290
 If you understood reality clearly, there'd be no reason to

315
00:24:05,290 --> 00:24:11,000
 suffer. There'd be no opportunity for suffering to arise.

316
00:24:11,000 --> 00:24:17,030
 So it's not something to be, even though this is sort of a

317
00:24:17,030 --> 00:24:20,820
 conceptual teaching, it's not something to be disregarded

318
00:24:20,820 --> 00:24:22,000
 or discounted.

319
00:24:22,000 --> 00:24:26,060
 Because we have the, and this is what we're born with, we

320
00:24:26,060 --> 00:24:30,280
're born with conceit, we're born into a life of conceit and

321
00:24:30,280 --> 00:24:32,000
 attachment to the body.

322
00:24:32,000 --> 00:24:38,000
 And so it's one of the more important topics to talk about,

323
00:24:38,000 --> 00:24:40,740
 the true nature of the body, both conceptually and in an

324
00:24:40,740 --> 00:24:42,000
 ultimate sense.

325
00:24:42,000 --> 00:24:46,680
 Conceptually it's getting old, sick and dying, and in an

326
00:24:46,680 --> 00:24:51,580
 ultimate sense, it's not really existing in the first place

327
00:24:51,580 --> 00:24:52,000
.

328
00:24:52,000 --> 00:24:55,110
 So that's the Dhammapada for tonight. Thank you all for

329
00:24:55,110 --> 00:24:58,000
 tuning in. Wishing you all good practice.

