1
00:00:00,000 --> 00:00:02,810
 Someone, "I feel like I will never find happiness in lay

2
00:00:02,810 --> 00:00:04,880
 life and I'm very depressed.

3
00:00:04,880 --> 00:00:09,360
 Is this a sign for me to become a monk?"

4
00:00:09,360 --> 00:00:14,560
 You don't need a sign to become a monk. Just become a monk.

5
00:00:14,560 --> 00:00:18,720
 If you're not tied down to anything, just become a monk.

6
00:00:18,720 --> 00:00:19,760
 There's nothing else.

7
00:00:19,760 --> 00:00:23,280
 There's nothing better that you could do.

8
00:00:29,360 --> 00:00:31,860
 Of course you'll never find happiness in lay life. There's

9
00:00:31,860 --> 00:00:34,640
 no happiness to be found.

10
00:00:34,640 --> 00:00:44,480
 Is that going to turn people off? I don't know.

11
00:00:44,480 --> 00:00:48,710
 But I have to, you know, you think you just have to get to

12
00:00:48,710 --> 00:00:51,360
 that.

13
00:00:51,360 --> 00:00:56,960
 If you're not afraid of ordaining and

14
00:00:56,960 --> 00:01:00,640
 you don't have burdens that are keeping you from ordaining,

15
00:01:00,640 --> 00:01:00,640
 then

16
00:01:00,640 --> 00:01:04,400
 I don't see a reason not to.

17
00:01:04,400 --> 00:01:08,480
 Worst, you'll just die on the street or something.

18
00:01:08,480 --> 00:01:13,040
 Very good karma to give up everything.

19
00:01:13,040 --> 00:01:18,580
 It would certainly fix your depression. I don't know about

20
00:01:18,580 --> 00:01:19,600
 that. It depends because

21
00:01:19,600 --> 00:01:23,180
 sometimes you go to the monastery and realize how

22
00:01:23,180 --> 00:01:24,400
 depressing it is,

23
00:01:24,400 --> 00:01:29,840
 how depressing Buddhism has become. Modern monasteries can

24
00:01:29,840 --> 00:01:30,320
 be a little bit

25
00:01:30,320 --> 00:01:34,880
 depressing as well. But the monk's life is not depressing.

26
00:01:34,880 --> 00:01:37,650
 No, I think that's not true because you have the monk's

27
00:01:37,650 --> 00:01:38,160
 life.

28
00:01:38,160 --> 00:01:41,190
 The monk's life is not depressing. It's wonderfully

29
00:01:41,190 --> 00:01:41,920
 liberating.

30
00:01:41,920 --> 00:01:46,160
 You're so in control of your own destiny.

31
00:01:46,160 --> 00:01:51,440
 Nothing can faze you. Maybe you don't eat for a day.

32
00:01:52,160 --> 00:01:55,200
 Inconsequential.

33
00:01:55,200 --> 00:02:00,640
 Maybe you don't have soap or toothbrush.

34
00:02:00,640 --> 00:02:06,480
 Maybe you're lying sick in a cave somewhere.

35
00:02:06,480 --> 00:02:11,920
 There's such freedom. You don't have to worry about

36
00:02:11,920 --> 00:02:14,320
 anything.

37
00:02:14,320 --> 00:02:18,880
 So I think a very good cure for depression.

38
00:02:21,200 --> 00:02:24,560
 Certainly a cure for the lay life.

39
00:02:24,560 --> 00:02:30,640
 It's just a joke, you see, because when you become a monk

40
00:02:30,640 --> 00:02:31,360
 you are no longer a

41
00:02:31,360 --> 00:02:36,880
 lay person. So it's a cure for the lay life.

