WEBVTT

00:00:00.000 --> 00:00:15.000
 Good evening everyone.

00:00:15.000 --> 00:00:27.800
 Broadcasting Live, January 24th, 2016.

00:00:27.800 --> 00:00:41.800
 Today's quote is about associating with wise people

00:00:41.800 --> 00:00:53.800
 and associating with unwise people.

00:00:53.800 --> 00:01:05.820
 This is the way of looking at the world from a Buddhist

00:01:05.820 --> 00:01:06.800
 perspective.

00:01:06.800 --> 00:01:16.920
 It's different than how we look at the world in modern

00:01:16.920 --> 00:01:18.800
 society.

00:01:18.800 --> 00:01:25.200
 In Buddhism, we look at the world from the perspective of

00:01:25.200 --> 00:01:33.800
 the individual, the experience.

00:01:33.800 --> 00:01:46.810
 The experience of seeing, of hearing, of smelling, tasting,

00:01:46.810 --> 00:01:57.800
 feeling, and thinking of the individual, the person.

00:01:57.800 --> 00:02:19.800
 And so other people are like looking in a mirror.

00:02:19.800 --> 00:02:29.800
 This is why hurting other people is a very bad thing to do.

00:02:29.800 --> 00:02:34.800
 It's like punching yourself.

00:02:34.800 --> 00:02:49.500
 Why we can say things like, "Don't hurt others if you don't

00:02:49.500 --> 00:02:56.800
 want to be hurt yourself."

00:02:56.800 --> 00:03:09.100
 Because somehow we are affected by our relations with other

00:03:09.100 --> 00:03:10.800
 people.

00:03:10.800 --> 00:03:20.800
 They're like a mirror image of ourselves.

00:03:20.800 --> 00:03:28.490
 And so the same goes for learning and studying and

00:03:28.490 --> 00:03:32.800
 understanding reality.

00:03:32.800 --> 00:03:44.630
 You can spend these months, years learning about reality on

00:03:44.630 --> 00:03:53.800
 your own, the impersonal aspects of reality.

00:03:53.800 --> 00:04:08.290
 And if all those lessons have been learned by another, they

00:04:08.290 --> 00:04:18.800
 can teach it to you in a much quicker time.

00:04:18.800 --> 00:04:25.780
 And so the importance of other people is not to be

00:04:25.780 --> 00:04:28.800
 underestimated.

00:04:28.800 --> 00:04:32.520
 You should never dismiss the idea of being with good people

00:04:32.520 --> 00:04:32.800
.

00:04:32.800 --> 00:04:36.970
 Sometimes it's hard to be with other people. It's easier to

00:04:36.970 --> 00:04:38.800
 be alone.

00:04:38.800 --> 00:04:49.560
 You should be careful about that. Being alone is good, it's

00:04:49.560 --> 00:04:55.050
 great, even the Buddha is very praised, very appreciative

00:04:55.050 --> 00:04:57.800
 of solitude.

00:04:57.800 --> 00:05:07.520
 But at the same time he was very much stressing the

00:05:07.520 --> 00:05:13.800
 importance of good friendship.

00:05:13.800 --> 00:05:21.170
 Because place and situation, the environment isn't really,

00:05:21.170 --> 00:05:27.800
 our impersonal environment isn't really important.

00:05:27.800 --> 00:05:36.920
 You can argue that it's good to have suitable weather and

00:05:36.920 --> 00:05:44.750
 suitable food and suitable requisites, all the things that

00:05:44.750 --> 00:05:46.800
 you need to meditate.

00:05:46.800 --> 00:05:48.950
 If there's loud music blaring, it's not really good for

00:05:48.950 --> 00:05:51.800
 your meditation. You can argue this.

00:05:51.800 --> 00:05:58.360
 But in the end these concerns are relatively minor concerns

00:05:58.360 --> 00:05:58.800
.

00:05:58.800 --> 00:06:02.790
 But not being around suitable people, this is much more

00:06:02.790 --> 00:06:03.800
 damaging.

00:06:03.800 --> 00:06:08.800
 Place is not really important. You see, wherever you go,

00:06:08.800 --> 00:06:12.800
 you never really change.

00:06:12.800 --> 00:06:19.960
 You never really move. It's more like the world changes

00:06:19.960 --> 00:06:21.800
 around you.

00:06:21.800 --> 00:06:25.360
 We're always right here. When you close your eyes you see

00:06:25.360 --> 00:06:34.800
 that. We never really move. The mind never really moves.

00:06:34.800 --> 00:06:39.800
 Only the physical changes.

00:06:39.800 --> 00:06:47.370
 And so we carry our problems with us. We can't solve our

00:06:47.370 --> 00:06:50.800
 problems by changing our location.

00:06:50.800 --> 00:06:54.020
 You can't solve your problems by running away to a foreign

00:06:54.020 --> 00:06:57.840
 country or going up on a mountain or as we learned recently

00:06:57.840 --> 00:06:59.800
 in the Dhammapada.

00:06:59.800 --> 00:07:05.120
 You can't dive into the ocean or up in the sky, there's

00:07:05.120 --> 00:07:08.800
 nowhere on earth you can escape.

00:07:08.800 --> 00:07:20.250
 But having a suitable, suitable tools and suitable support,

00:07:20.250 --> 00:07:22.800
 this is worth traveling,

00:07:22.800 --> 00:07:29.800
 worth traveling across the earth for.

00:07:29.800 --> 00:07:36.180
 And so associating with good people. We see it here in this

00:07:36.180 --> 00:07:40.610
 quote and we have in several places the Buddha talked about

00:07:40.610 --> 00:07:41.800
 what Kalyanamitata,

00:07:41.800 --> 00:07:46.940
 we've met with this term before, good friendship,

00:07:46.940 --> 00:07:51.800
 association with good people.

00:07:51.800 --> 00:07:57.750
 This is what we should focus on. This is why we set up this

00:07:57.750 --> 00:08:07.800
 internet community to give at least some form of support.

00:08:07.800 --> 00:08:11.730
 That's why we live in monasteries, why people come here to

00:08:11.730 --> 00:08:16.800
 do courses. They want to be in the environment.

00:08:16.800 --> 00:08:22.130
 And we're also associating with people who will remind them

00:08:22.130 --> 00:08:24.800
 of meditation practice.

00:08:24.800 --> 00:08:29.920
 But the quote here is talking also about friendship, about

00:08:29.920 --> 00:08:32.800
 the relationships we choose.

00:08:32.800 --> 00:08:36.000
 Certainly it's important to have a spiritual community, but

00:08:36.000 --> 00:08:39.960
 it's also important to be cautious of the sorts of people

00:08:39.960 --> 00:08:41.800
 you associate with in Monday in life,

00:08:41.800 --> 00:08:50.590
 in society, you choose for your friendships, for your

00:08:50.590 --> 00:08:53.800
 companionship.

00:08:53.800 --> 00:08:57.010
 There are people engaging in killing and stealing and lying

00:08:57.010 --> 00:09:00.940
 and cheating and these are not the sort of friends that

00:09:00.940 --> 00:09:03.800
 benefit either you or themselves.

00:09:03.800 --> 00:09:10.160
 They drag you down with them. More so than anything else,

00:09:10.160 --> 00:09:14.800
 humans have great power over each other.

00:09:14.800 --> 00:09:19.330
 Living beings you could say if you want to generalize. Much

00:09:19.330 --> 00:09:25.400
 more power than the environment like cold or heat or hunger

00:09:25.400 --> 00:09:26.800
 or thirst.

00:09:26.800 --> 00:09:37.380
 People have the ability to hurt, to harm or to help each

00:09:37.380 --> 00:09:41.800
 other in great ways.

00:09:41.800 --> 00:09:51.910
 And it doesn't mean that we're powerless to prevent other

00:09:51.910 --> 00:09:55.800
 people from harming us.

00:09:55.800 --> 00:09:59.800
 And it also doesn't mean that we should shun such people.

00:09:59.800 --> 00:10:03.370
 The question is, well, if we're not supposed to associate

00:10:03.370 --> 00:10:06.800
 with fools, what happens if we're a fool ourselves?

00:10:06.800 --> 00:10:09.800
 But it's an important point that we shouldn't expect wise

00:10:09.800 --> 00:10:11.800
 people to want to hang out with us.

00:10:11.800 --> 00:10:16.060
 We shouldn't be discouraged. We should take the Buddha as

00:10:16.060 --> 00:10:19.800
 word and try to associate with wise people.

00:10:19.800 --> 00:10:24.790
 But we should understand when they would rather be alone,

00:10:24.790 --> 00:10:30.800
 they would rather not associate with us.

00:10:30.800 --> 00:10:35.710
 But we don't shun foolish people to be careful not to shun

00:10:35.710 --> 00:10:36.800
 such people.

00:10:36.800 --> 00:10:40.800
 But we have to be careful not to let them in too close.

00:10:40.800 --> 00:10:44.180
 The stronger you get in the practice, the more comfortable

00:10:44.180 --> 00:10:51.470
 you can be without having to force people away or run away

00:10:51.470 --> 00:10:56.800
 from people.

00:10:56.800 --> 00:11:02.800
 You should never take advice from foolish people.

00:11:02.800 --> 00:11:08.800
 You should never think of them as people to be trusted.

00:11:08.800 --> 00:11:11.800
 You should trust those who are trustworthy.

00:11:11.800 --> 00:11:20.820
 Those who are not trustworthy, you should help them to

00:11:20.820 --> 00:11:24.800
 become trustworthy.

00:11:24.800 --> 00:11:28.780
 But if we're careful, if we're mindful, it's important to

00:11:28.780 --> 00:11:29.800
 be mindful of this.

00:11:29.800 --> 00:11:34.140
 Because if we're not mindful, it's very easy to become, as

00:11:34.140 --> 00:11:37.800
 this quote says, if you wrap fish, rotten fish up in a leaf

00:11:37.800 --> 00:11:41.800
, the leaf starts to smell bad as well.

00:11:41.800 --> 00:11:45.590
 The same goes with if you hang out with unsavoury

00:11:45.590 --> 00:11:52.800
 characters. You pick up unsavoury habits yourself.

00:11:52.800 --> 00:11:58.720
 You start to change and even if you don't change, you get a

00:11:58.720 --> 00:12:02.800
 bad reputation to spread the buggy.

00:12:02.800 --> 00:12:07.180
 It's important to be mindful of this, be mindful of other

00:12:07.180 --> 00:12:07.800
 people.

00:12:07.800 --> 00:12:13.870
 The most volatile of a situation is the situation with

00:12:13.870 --> 00:12:19.800
 another being, especially a higher being like a human.

00:12:19.800 --> 00:12:24.630
 I suppose we have many, much contact with angels, but with

00:12:24.630 --> 00:12:29.800
 animals and with humans, we have to be careful.

00:12:29.800 --> 00:12:34.840
 Some people when they see a cockroach or a snake or a

00:12:34.840 --> 00:12:40.800
 spider or a mouse, it's disturbing for them.

00:12:40.800 --> 00:12:51.800
 But humans are far more impactful.

00:12:51.800 --> 00:12:56.130
 And so if we can't associate with, if we can't get away

00:12:56.130 --> 00:13:00.080
 from people who are doing evil and bad things, then we

00:13:00.080 --> 00:13:03.800
 should be very mindful and careful and aware of the fact

00:13:03.800 --> 00:13:09.800
 that there's a strong power

00:13:09.800 --> 00:13:14.800
 to impart behaviours and habits among people.

00:13:14.800 --> 00:13:20.910
 We should be careful to be mindful of good habits and bad

00:13:20.910 --> 00:13:25.800
 habits and be aware of the power of relationships.

00:13:25.800 --> 00:13:30.800
 So that's the quote for tonight. That's our dum for today.

00:13:30.800 --> 00:13:37.800
 We should not worry too much about, we should be aware more

00:13:37.800 --> 00:13:43.800
 about people than about our situation otherwise.

00:13:43.800 --> 00:13:48.760
 We find ways to be surrounded by good people because then

00:13:48.760 --> 00:13:51.800
 we're able to work on ourselves.

00:13:51.800 --> 00:13:55.000
 No matter where we go, we bring our problems with us, but

00:13:55.000 --> 00:13:58.800
 when we're with good people, that's when we can work on our

00:13:58.800 --> 00:13:59.800
 own.

00:13:59.800 --> 00:14:02.800
 Hard issues.

00:14:02.800 --> 00:14:05.990
 So thank you all for tuning in. Wishing you all good

00:14:05.990 --> 00:14:07.800
 practice. Have a good night.

