1
00:00:00,000 --> 00:00:04,000
 Okay, good evening everyone.

2
00:00:04,000 --> 00:00:10,160
 I guess there's no one watching yet, but you'll see this in

3
00:00:10,160 --> 00:00:12,800
 the recording as well.

4
00:00:12,800 --> 00:00:24,140
 So again, we're looking at the relationship between

5
00:00:24,140 --> 00:00:28,520
 Buddhism and nature.

6
00:00:28,520 --> 00:00:37,730
 And so the second way of looking at nature from a Buddhist

7
00:00:37,730 --> 00:00:40,680
 perspective is I think in

8
00:00:40,680 --> 00:00:49,230
 terms of what we say in English, the nature of things or

9
00:00:49,230 --> 00:00:54,400
 human nature or even just things

10
00:00:54,400 --> 00:01:06,800
 as having a nature, as having an existence, a presence.

11
00:01:06,800 --> 00:01:12,240
 And so I'll explain what I mean by that one.

12
00:01:12,240 --> 00:01:15,480
 It's maybe a little hard to understand.

13
00:01:15,480 --> 00:01:26,570
 But the nature of things, the nature of things relates to

14
00:01:26,570 --> 00:01:31,800
 our understanding of things being

15
00:01:31,800 --> 00:01:48,360
 stable, being constant, predictable, dependable, static.

16
00:01:48,360 --> 00:01:52,240
 So what is the nature of things?

17
00:01:52,240 --> 00:01:55,420
 Physics will tell you things about the nature of things and

18
00:01:55,420 --> 00:01:55,960
 so on.

19
00:01:55,960 --> 00:02:06,280
 Buddhism has much to say.

20
00:02:06,280 --> 00:02:11,030
 But we go far beyond that is I think an issue that we have

21
00:02:11,030 --> 00:02:14,200
 to face and understand in Buddhism,

22
00:02:14,200 --> 00:02:18,240
 is that we take well nature.

23
00:02:18,240 --> 00:02:26,310
 And again, the inspiration of these few talks was the

24
00:02:26,310 --> 00:02:30,400
 concern for our environment, for the

25
00:02:30,400 --> 00:02:37,950
 climate, for nature as a concept, as a thing to be

26
00:02:37,950 --> 00:02:43,960
 protected and concerned about, and the

27
00:02:43,960 --> 00:02:48,970
 thing that is being destroyed by human greed and corruption

28
00:02:48,970 --> 00:02:49,320
.

29
00:02:49,320 --> 00:02:56,400
 By us, we're destroying nature in many ways.

30
00:02:56,400 --> 00:03:02,400
 But that's, so another thing that we have to look at in

31
00:03:02,400 --> 00:03:05,440
 that regard is that being a

32
00:03:05,440 --> 00:03:06,440
 part of nature.

33
00:03:06,440 --> 00:03:12,690
 The nature of things is to be changeable, is that this

34
00:03:12,690 --> 00:03:16,440
 concept of nature, the trees,

35
00:03:16,440 --> 00:03:23,820
 the mountains, the weather, the air, the water, is all mal

36
00:03:23,820 --> 00:03:25,120
leable.

37
00:03:25,120 --> 00:03:27,280
 It doesn't have a nature.

38
00:03:27,280 --> 00:03:31,640
 If anyone asks you about nature, when you go out into the

39
00:03:31,640 --> 00:03:33,720
 forests or to the seaside

40
00:03:33,720 --> 00:03:39,460
 and you talk about how peaceful and how calming and how

41
00:03:39,460 --> 00:03:42,440
 wonderful nature is, that kind of

42
00:03:42,440 --> 00:03:49,360
 nature, this nature as a thing as opposed to the human, is

43
00:03:49,360 --> 00:03:52,560
 just a concept in our minds

44
00:03:52,560 --> 00:04:01,200
 and the reality is changing.

45
00:04:01,200 --> 00:04:03,200
 Reality is susceptible to change.

46
00:04:03,200 --> 00:04:08,070
 There's no such thing as the nature of things in that sense

47
00:04:08,070 --> 00:04:08,160
.

48
00:04:08,160 --> 00:04:12,370
 Our ideas of the nature of things give us a sense of

49
00:04:12,370 --> 00:04:15,040
 stability that is illusory, that

50
00:04:15,040 --> 00:04:16,280
 isn't there.

51
00:04:16,280 --> 00:04:22,250
 I mean, part of the suffering from climate change is going

52
00:04:22,250 --> 00:04:25,640
 to be the simple change, having

53
00:04:25,640 --> 00:04:27,240
 to deal with change.

54
00:04:27,240 --> 00:04:34,520
 And sometimes the change is so drastic that it leads to

55
00:04:34,520 --> 00:04:40,520
 direct suffering, flooding, wildfires,

56
00:04:40,520 --> 00:04:52,760
 tornadoes, simple drought and loss of livelihood and so on.

57
00:04:52,760 --> 00:04:59,520
 But in general, speaking in general, our illusions about

58
00:04:59,520 --> 00:05:03,800
 the nature of things, things having

59
00:05:03,800 --> 00:05:08,210
 a nature, is a cause for great suffering, needless

60
00:05:08,210 --> 00:05:09,480
 suffering.

61
00:05:09,480 --> 00:05:15,320
 Whereas if we could see the changing nature of things.

62
00:05:15,320 --> 00:05:17,520
 Human nature as well, I mentioned that.

63
00:05:17,520 --> 00:05:23,120
 It's important to talk about because we think of human

64
00:05:23,120 --> 00:05:26,960
 nature as being stable, static.

65
00:05:26,960 --> 00:05:28,720
 I'm an angry person.

66
00:05:28,720 --> 00:05:32,880
 I'm a depressed person.

67
00:05:32,880 --> 00:05:36,160
 What did I read about depression?

68
00:05:36,160 --> 00:05:46,220
 No, one of our professors or someone in the university

69
00:05:46,220 --> 00:05:50,120
 recently said that there's only

70
00:05:50,120 --> 00:06:01,780
 a 25% difference between the effects of medication and the

71
00:06:01,780 --> 00:06:06,120
 effects of a placebo for people with

72
00:06:06,120 --> 00:06:11,840
 moderate or mild depression.

73
00:06:11,840 --> 00:06:14,760
 So we talk about being depressed.

74
00:06:14,760 --> 00:06:18,600
 I have depression and we think of it as an illness, as a

75
00:06:18,600 --> 00:06:20,760
 disease, as something to be

76
00:06:20,760 --> 00:06:27,390
 treated with medication due to what we call a chemical

77
00:06:27,390 --> 00:06:29,240
 imbalance.

78
00:06:29,240 --> 00:06:31,120
 Chemical imbalance is something static.

79
00:06:31,120 --> 00:06:36,040
 You can't wish it away, right?

80
00:06:36,040 --> 00:06:40,600
 It's a part of who you are.

81
00:06:40,600 --> 00:06:45,050
 And yet we find that these medications that are meant to

82
00:06:45,050 --> 00:06:47,720
 change that chemical imbalance

83
00:06:47,720 --> 00:06:53,200
 are not nearly as effective as we would think.

84
00:06:53,200 --> 00:06:59,480
 Or they're not much more effective than in some sense just

85
00:06:59,480 --> 00:07:01,640
 wishing it away.

86
00:07:01,640 --> 00:07:03,720
 No, the placebo effect.

87
00:07:03,720 --> 00:07:07,060
 The effects of meditation would be quite similar, I think,

88
00:07:07,060 --> 00:07:08,880
 to the placebo effect in terms of

89
00:07:08,880 --> 00:07:13,000
 changing the state of mind.

90
00:07:13,000 --> 00:07:17,160
 The human nature is knowledgeable.

91
00:07:17,160 --> 00:07:21,330
 Someone else recently said that up until recently we

92
00:07:21,330 --> 00:07:24,600
 thought the mind was static, that after

93
00:07:24,600 --> 00:07:32,040
 seven years old the brain didn't change.

94
00:07:32,040 --> 00:07:35,160
 Of course we know that's false.

95
00:07:35,160 --> 00:07:45,120
 You adult brains can change are malleable.

96
00:07:45,120 --> 00:07:48,850
 And so calling it a disease and thinking of it as a part of

97
00:07:48,850 --> 00:07:50,680
 you, a part of who you are

98
00:07:50,680 --> 00:07:53,640
 is incorrect.

99
00:07:53,640 --> 00:07:59,520
 Who you are, your human nature is malleable, is changeable,

100
00:07:59,520 --> 00:08:01,400
 is impermanent.

101
00:08:01,400 --> 00:08:05,120
 And even our physical nature is impermanent.

102
00:08:05,120 --> 00:08:11,930
 This body is just a shell or a husk or like a sand castle

103
00:08:11,930 --> 00:08:14,520
 that we build up.

104
00:08:14,520 --> 00:08:19,260
 Think of those nine months in the womb that we spend knead

105
00:08:19,260 --> 00:08:22,080
ing and molding and tweaking.

106
00:08:22,080 --> 00:08:23,680
 No, not of course.

107
00:08:23,680 --> 00:08:27,320
 You can't say that the mind created to feed us.

108
00:08:27,320 --> 00:08:34,970
 But certainly had a, played a part those nine months in

109
00:08:34,970 --> 00:08:37,200
 tweaking it.

110
00:08:37,200 --> 00:08:44,080
 And of course throughout life tweaking as well, changing.

111
00:08:44,080 --> 00:08:47,080
 And this physical form is also temporary.

112
00:08:47,080 --> 00:08:52,840
 So the true nature of things.

113
00:08:52,840 --> 00:08:55,410
 The question is, you know, is there a true nature of things

114
00:08:55,410 --> 00:08:55,640
?

115
00:08:55,640 --> 00:09:00,440
 Is there something that is stable and constant?

116
00:09:00,440 --> 00:09:04,500
 And there is something that is, you can say is always the

117
00:09:04,500 --> 00:09:06,040
 nature of things.

118
00:09:06,040 --> 00:09:15,880
 And the nature of things is to change.

119
00:09:15,880 --> 00:09:19,670
 And that's more than just a clever thing to say because of

120
00:09:19,670 --> 00:09:21,520
 course it's basically saying

121
00:09:21,520 --> 00:09:24,780
 that the nature of things is to not have a constant nature,

122
00:09:24,780 --> 00:09:25,400
 right?

123
00:09:25,400 --> 00:09:27,800
 It's more than that.

124
00:09:27,800 --> 00:09:32,970
 Because understanding of impermanence, understanding of the

125
00:09:32,970 --> 00:09:35,920
 unpredictable nature of things is a

126
00:09:35,920 --> 00:09:37,760
 stable state.

127
00:09:37,760 --> 00:09:40,520
 The understanding of it.

128
00:09:40,520 --> 00:09:45,730
 When you understand that law of nature, that aspect of

129
00:09:45,730 --> 00:09:50,240
 nature, it's a very powerful thing.

130
00:09:50,240 --> 00:09:51,880
 It changes the way you look at the world.

131
00:09:51,880 --> 00:09:56,320
 It changes the way you interact with the world.

132
00:09:56,320 --> 00:10:00,490
 It allows you to let go, to be free from the suffering that

133
00:10:00,490 --> 00:10:02,840
 comes from change, right?

134
00:10:02,840 --> 00:10:07,320
 So it is an important, stable reality to understand

135
00:10:07,320 --> 00:10:11,000
 thoroughly so that you gain the stability

136
00:10:11,000 --> 00:10:13,160
 of understanding.

137
00:10:13,160 --> 00:10:17,320
 The stability that comes from being able to adapt.

138
00:10:17,320 --> 00:10:30,520
 The stability of flexibility in a sense.

139
00:10:30,520 --> 00:10:34,280
 Understanding impermanence, understanding suffering.

140
00:10:34,280 --> 00:10:39,960
 The suffering that comes from clinging to things, right?

141
00:10:39,960 --> 00:10:45,490
 If everything is changing then clinging to stability,

142
00:10:45,490 --> 00:10:48,160
 clinging to something.

143
00:10:48,160 --> 00:10:52,480
 It's like clinging to a raft in the fast moving river.

144
00:10:52,480 --> 00:10:57,920
 It just gets swept away.

145
00:10:57,920 --> 00:11:09,200
 Clinging to, clinging to a sinking ship.

146
00:11:09,200 --> 00:11:14,080
 Something about being flexible is not clinging.

147
00:11:14,080 --> 00:11:19,140
 Flexibility implies, or the adaptability or the

148
00:11:19,140 --> 00:11:23,440
 understanding of impermanence implies a

149
00:11:23,440 --> 00:11:24,440
 lack of attachment.

150
00:11:24,440 --> 00:11:27,700
 So we hear about non-attachment in Buddhism and not

151
00:11:27,700 --> 00:11:30,080
 clinging and not wanting or craving

152
00:11:30,080 --> 00:11:32,440
 even liking anything.

153
00:11:32,440 --> 00:11:34,000
 And yes it implies that.

154
00:11:34,000 --> 00:11:38,950
 It implies that happiness can't come from clinging or

155
00:11:38,950 --> 00:11:42,240
 craving, holding onto anything,

156
00:11:42,240 --> 00:11:43,240
 liking anything.

157
00:11:43,240 --> 00:11:46,240
 It can't come from things.

158
00:11:46,240 --> 00:11:49,440
 Your happiness has to be independent of things.

159
00:11:49,440 --> 00:11:55,640
 Things whose only nature is to change.

160
00:11:55,640 --> 00:11:58,440
 Understanding of non-self that you can't control things.

161
00:11:58,440 --> 00:12:01,120
 You can't stop this process.

162
00:12:01,120 --> 00:12:09,560
 The process of change is not me, it's not mine.

163
00:12:09,560 --> 00:12:19,900
 In fact, in fact our struggle to be satisfied, to find

164
00:12:19,900 --> 00:12:28,160
 pleasure changes things themselves,

165
00:12:28,160 --> 00:12:32,640
 changes things, creates impermanence.

166
00:12:32,640 --> 00:12:36,980
 Not only does it create suffering because of change, but it

167
00:12:36,980 --> 00:12:38,440
 creates change.

168
00:12:38,440 --> 00:12:44,140
 I'm thinking now again back to the daily environment, as I

169
00:12:44,140 --> 00:12:47,280
 mentioned in the last video.

170
00:12:47,280 --> 00:12:52,240
 Our craving, our clinging.

171
00:12:52,240 --> 00:12:57,900
 Think of an addict, an addict gains pleasure from a drug,

172
00:12:57,900 --> 00:13:01,320
 which of course changes the brain,

173
00:13:01,320 --> 00:13:04,660
 makes it harder to get pleasure from the same drug and

174
00:13:04,660 --> 00:13:07,480
 leads to greater and greater addiction.

175
00:13:07,480 --> 00:13:09,000
 This is true with so many things.

176
00:13:09,000 --> 00:13:13,540
 I mean it's true with any addiction to even simple things

177
00:13:13,540 --> 00:13:16,520
 like food, television, music.

178
00:13:16,520 --> 00:13:21,720
 The brain is elastic and you change the brain.

179
00:13:21,720 --> 00:13:24,100
 Not only do we change our brains, we change our

180
00:13:24,100 --> 00:13:25,120
 environments.

181
00:13:25,120 --> 00:13:29,920
 Our incessant drive to find pleasure and satisfaction is

182
00:13:29,920 --> 00:13:32,720
 changing the world around us.

183
00:13:32,720 --> 00:13:37,560
 It's destroying the environment.

184
00:13:37,560 --> 00:13:42,380
 Even our consumerism, our consumption of water, our

185
00:13:42,380 --> 00:13:46,360
 consumption of oil, plastic, all of these

186
00:13:46,360 --> 00:13:47,360
 things.

187
00:13:47,360 --> 00:13:50,610
 That's why I think it's fair to say Buddhists should be

188
00:13:50,610 --> 00:13:52,960
 environmentally conscious in terms

189
00:13:52,960 --> 00:13:59,500
 of understanding the relationship between our greed and our

190
00:13:59,500 --> 00:14:01,120
 suffering.

191
00:14:01,120 --> 00:14:09,040
 The suffering caused by an environment that's degraded.

192
00:14:09,040 --> 00:14:18,230
 Our clinging, our craving, even our control, our intent to

193
00:14:18,230 --> 00:14:22,800
 drive, to control, to cling,

194
00:14:22,800 --> 00:14:27,680
 to maintain, cling to stability.

195
00:14:27,680 --> 00:14:32,310
 All of that is creating destruction, it's creating stress,

196
00:14:32,310 --> 00:14:34,480
 it creates systems that oppress

197
00:14:34,480 --> 00:14:45,760
 other beings, oppress other humans.

198
00:14:45,760 --> 00:14:50,600
 We create suffering because we don't see the nature of

199
00:14:50,600 --> 00:14:51,680
 things.

200
00:14:51,680 --> 00:14:57,760
 We don't see that our behavior causes suffering.

201
00:14:57,760 --> 00:14:59,440
 There is a nature of things.

202
00:14:59,440 --> 00:15:03,120
 It's quite simple in that way.

203
00:15:03,120 --> 00:15:07,810
 That's with impermanence, really, is the cornerstone of it

204
00:15:07,810 --> 00:15:08,400
 all.

205
00:15:08,400 --> 00:15:11,920
 Being impermanent, nothing is satisfying.

206
00:15:11,920 --> 00:15:19,560
 You can't find satisfaction or happiness in things.

207
00:15:19,560 --> 00:15:24,060
 And that being so, they are not me, they are not mine, they

208
00:15:24,060 --> 00:15:26,160
 are not under my control.

209
00:15:26,160 --> 00:15:33,930
 The idea of self is just a delusion, the idea of me and

210
00:15:33,930 --> 00:15:38,480
 mine are a trap that leads us to

211
00:15:38,480 --> 00:15:40,480
 only greater suffering.

212
00:15:40,480 --> 00:15:45,910
 So there you go, more on the nature of things, more on

213
00:15:45,910 --> 00:15:48,440
 Buddhism and nature.

214
00:15:48,440 --> 00:15:51,440
 One more video, one more aspect of nature, I think.

215
00:15:51,440 --> 00:15:55,000
 And then we'll be done with that.

216
00:15:55,000 --> 00:15:59,740
 And I'm not making videos that often, I don't know when the

217
00:15:59,740 --> 00:16:01,320
 next one will be.

218
00:16:01,320 --> 00:16:05,620
 And I expect that in the new year I'll be making more

219
00:16:05,620 --> 00:16:09,440
 videos, maybe even next month,

220
00:16:09,440 --> 00:16:17,560
 but this month is somewhat occupied with other things.

221
00:16:17,560 --> 00:16:19,840
 Thank you all, have a good night.

222
00:16:19,840 --> 00:16:22,880
 Thank you all for tuning in, 43 people, spur of the moment

223
00:16:22,880 --> 00:16:24,200
 on a Wednesday night.

224
00:16:24,200 --> 00:16:25,200
 I very much appreciate it.

225
00:16:25,200 --> 00:16:27,200
 So have a good night, wish you all the best.

226
00:16:27,200 --> 00:16:29,920
 Have a good night, wish you all the best.

