1
00:00:00,000 --> 00:00:09,000
 Okay, so we are broadcasting live tonight.

2
00:00:09,000 --> 00:00:32,000
 The plan is to record Dhamma dhamma video. So, we'll do a

3
00:00:32,000 --> 00:00:32,000
 recording tonight.

4
00:00:32,000 --> 00:00:36,000
 Hello and welcome back to our study of the Dhamma dhamma.

5
00:00:36,000 --> 00:00:42,320
 Today we continue on with verse number 76, which reads as

6
00:00:42,320 --> 00:00:45,000
 follows.

7
00:00:45,000 --> 00:00:59,190
 Nidinamvat pavataran yam pasey vadchathasinam ningayah vad

8
00:00:59,190 --> 00:01:08,000
im medhavim tadi sankanditam bhaje

9
00:01:08,000 --> 00:01:22,240
 manaset seyo hoti napapi hoti, which translates to "like

10
00:01:22,240 --> 00:01:36,000
 one who unearths or uncovers buried treasure."

11
00:01:36,000 --> 00:01:44,440
 Whatever person seeing one's faults, pasey, when seeing one

12
00:01:44,440 --> 00:01:50,000
's faults, vadchathasinam,

13
00:01:50,000 --> 00:01:54,000
 lets one know one's faults.

14
00:01:54,000 --> 00:02:07,320
 Someone who, this person, ningayah vadim, one who speaks in

15
00:02:07,320 --> 00:02:12,000
 censure or gives one admonishment,

16
00:02:12,000 --> 00:02:19,000
 admonishes one, should be considered a wise person.

17
00:02:19,000 --> 00:02:23,840
 Tadi sankanditam vajeenam, such a person should be

18
00:02:23,840 --> 00:02:26,000
 associated by the wise.

19
00:02:26,000 --> 00:02:34,230
 If a person is wise, such a person is to be associated with

20
00:02:34,230 --> 00:02:39,000
 by the wise people.

21
00:02:39,000 --> 00:02:43,080
 When one associates with such a person, things get better,

22
00:02:43,080 --> 00:02:44,000
 not worse.

23
00:02:44,000 --> 00:02:49,000
 Seyo hoti napapi, they get better. You don't go to evil.

24
00:02:49,000 --> 00:02:52,000
 You don't get worse.

25
00:02:52,000 --> 00:02:55,880
 So associate with people who point out your faults. When

26
00:02:55,880 --> 00:03:01,000
 you do, things get better, not worse.

27
00:03:01,000 --> 00:03:07,950
 This verse was told in regards to a student of Sariputta

28
00:03:07,950 --> 00:03:10,000
 named Radha.

29
00:03:10,000 --> 00:03:19,000
 Radha was a poor brahmin who heard the Buddha's teaching

30
00:03:19,000 --> 00:03:21,000
 and went to live with the monks,

31
00:03:21,000 --> 00:03:28,000
 but the monks were unwilling to give him any ordination.

32
00:03:28,000 --> 00:03:33,000
 I think somewhere there was a comment that it was because

33
00:03:33,000 --> 00:03:36,000
 he took such great care of the monks,

34
00:03:36,000 --> 00:03:39,460
 and they were reluctant to give him the ordination because

35
00:03:39,460 --> 00:03:42,000
 they needed someone to look after him.

36
00:03:42,000 --> 00:03:45,190
 Which is sort of an interesting situation. It doesn't say

37
00:03:45,190 --> 00:03:48,000
 that in this version of the story.

38
00:03:48,000 --> 00:03:52,000
 But for some reason it may have been that he was old.

39
00:03:52,000 --> 00:03:56,000
 It may have been that they had no trust in him.

40
00:03:56,000 --> 00:04:02,020
 But the Buddha saw this, and the Buddha discerned through

41
00:04:02,020 --> 00:04:06,000
 his vast knowledge and understanding

42
00:04:06,000 --> 00:04:11,020
 that Radha would actually be capable of becoming an arahant

43
00:04:11,020 --> 00:04:13,000
 if he were to ordain.

44
00:04:13,000 --> 00:04:17,860
 So he went to the monks and he said, "Why is this man

45
00:04:17,860 --> 00:04:21,000
 pretending that he didn't know?"

46
00:04:21,000 --> 00:04:26,260
 Or not letting on that he knew. And they told him who he

47
00:04:26,260 --> 00:04:31,000
 was and said, "Why aren't you ordaining him?"

48
00:04:31,000 --> 00:04:34,140
 I think maybe that's where it says it. It may actually say

49
00:04:34,140 --> 00:04:36,000
 why they didn't ordain him.

50
00:04:36,000 --> 00:04:41,000
 But anyway, so he said, "Hey, does anyone, do any of you,

51
00:04:41,000 --> 00:04:44,030
 can any of you think of a reason why you might be inclined

52
00:04:44,030 --> 00:04:45,000
 to ordain him?

53
00:04:45,000 --> 00:04:50,000
 Could any of you remember something that he did for you?"

54
00:04:50,000 --> 00:04:54,660
 And Sariputta piped up and said, "I remember once when he

55
00:04:54,660 --> 00:04:56,000
 was living at home,

56
00:04:56,000 --> 00:05:01,000
 he gave me a spoonful of rice."

57
00:05:01,000 --> 00:05:05,260
 And the Buddha said, "Well, Sariputta, don't you think it's

58
00:05:05,260 --> 00:05:08,000
 appropriate to be grateful

59
00:05:08,000 --> 00:05:12,000
 and to repay people's kindness?"

60
00:05:12,000 --> 00:05:15,580
 Which is interesting because he didn't do that great of a

61
00:05:15,580 --> 00:05:17,000
 thing in the grand scheme of things.

62
00:05:17,000 --> 00:05:21,000
 All he did was give him a single spoonful of rice.

63
00:05:21,000 --> 00:05:24,680
 But it's a testament to the Buddha's sensitivity and Sarip

64
00:05:24,680 --> 00:05:29,000
utta's gratitude in remembering.

65
00:05:29,000 --> 00:05:33,640
 So when you ask someone, "Did they do something? Has this

66
00:05:33,640 --> 00:05:36,000
 person done anything for you?"

67
00:05:36,000 --> 00:05:39,480
 It's not common for people to think, to keep that in their

68
00:05:39,480 --> 00:05:42,000
 mind, that this person did such simple things.

69
00:05:42,000 --> 00:05:49,000
 But Sariputta was a person who had this great sensitivity.

70
00:05:49,000 --> 00:05:54,000
 He didn't take for granted the fact that he had been given

71
00:05:54,000 --> 00:05:56,000
 even a single spoonful of rice.

72
00:05:56,000 --> 00:05:59,000
 And so he spoke up and the Buddha said, "Well, in that case

73
00:05:59,000 --> 00:06:01,000
, don't you think it's worth ordaining him?"

74
00:06:01,000 --> 00:06:04,270
 And Sariputta immediately said, "In that case, I will ord

75
00:06:04,270 --> 00:06:05,000
ain him."

76
00:06:05,000 --> 00:06:08,000
 And so Sariputta gave him the ordination.

77
00:06:08,000 --> 00:06:10,870
 And I guess there was some skepticism as to whether this

78
00:06:10,870 --> 00:06:12,000
 monk would actually,

79
00:06:12,000 --> 00:06:16,000
 whether this man would actually make a good monk.

80
00:06:16,000 --> 00:06:22,000
 But Sariputta was fair with him and honest with him

81
00:06:22,000 --> 00:06:25,140
 and found that actually Radha was quite amenable to

82
00:06:25,140 --> 00:06:26,000
 training.

83
00:06:26,000 --> 00:06:28,790
 So Sariputta would tell him, "Don't do this, don't do that

84
00:06:28,790 --> 00:06:29,000
."

85
00:06:29,000 --> 00:06:32,000
 And he would take everything Sariputta said to heart,

86
00:06:32,000 --> 00:06:35,000
 "You have to do this, you have to do that."

87
00:06:35,000 --> 00:06:39,760
 He would only have to hear it once and he would immediately

88
00:06:39,760 --> 00:06:41,000
 adjust himself.

89
00:06:41,000 --> 00:06:43,530
 And when Sariputta admonished him saying, "You're doing

90
00:06:43,530 --> 00:06:47,000
 this wrong, you're doing that wrong, this is not right.

91
00:06:47,000 --> 00:06:50,000
 You have this fault, you have that fault."

92
00:06:50,000 --> 00:06:54,000
 He was completely amenable.

93
00:06:54,000 --> 00:06:56,390
 And so the Buddha asked him at one time, "How is your

94
00:06:56,390 --> 00:06:59,000
 student going? Do you find him amenable?"

95
00:06:59,000 --> 00:07:04,000
 Sariputta said, "He is the perfect student.

96
00:07:04,000 --> 00:07:07,240
 When I tell him not to do something, he stops doing it and

97
00:07:07,240 --> 00:07:09,000
 never have to tell him twice."

98
00:07:09,000 --> 00:07:13,000
 And the same goes with telling him what he should do.

99
00:07:13,000 --> 00:07:16,000
 He was the perfect student.

100
00:07:16,000 --> 00:07:18,880
 And the Buddha said, "If you could have other students like

101
00:07:18,880 --> 00:07:22,000
 him, would you ordain others?

102
00:07:22,000 --> 00:07:25,000
 If you knew that they were going to be like him and he said

103
00:07:25,000 --> 00:07:25,000
,

104
00:07:25,000 --> 00:07:27,710
 "If I could have a thousand students, I would gladly take

105
00:07:27,710 --> 00:07:28,000
 them on."

106
00:07:28,000 --> 00:07:32,000
 If they were all like Radha.

107
00:07:32,000 --> 00:07:36,060
 And the monks got talking about this and they said how

108
00:07:36,060 --> 00:07:38,000
 amazing it was, how wonderful it was,

109
00:07:38,000 --> 00:07:41,430
 that Sariputta had found such a wonderful student and that

110
00:07:41,430 --> 00:07:44,000
 Radha also had found such a wonderful teacher

111
00:07:44,000 --> 00:07:47,000
 who was willing to point out his fault.

112
00:07:47,000 --> 00:07:53,000
 And in no long time, Radha became an Arahant, as predicted.

113
00:07:53,000 --> 00:07:59,130
 And the Buddha heard the monks talking like this and then

114
00:07:59,130 --> 00:08:04,000
 he, as a result, spoke this verse,

115
00:08:04,000 --> 00:08:10,000
 saying, "Indeed, Radha is very lucky.

116
00:08:10,000 --> 00:08:13,000
 Sariputta is very lucky, but Radha also is very lucky

117
00:08:13,000 --> 00:08:15,000
 because Sariputta is someone

118
00:08:15,000 --> 00:08:23,140
 who is like a person who points out buried treasure." And

119
00:08:23,140 --> 00:08:24,000
 this book is for us.

120
00:08:24,000 --> 00:08:28,970
 So that's the backstory. It's a fairly well-known Buddhist

121
00:08:28,970 --> 00:08:31,000
 story among Buddhists.

122
00:08:31,000 --> 00:08:37,010
 And it relates to our practice in regards to the role of a

123
00:08:37,010 --> 00:08:38,000
 teacher,

124
00:08:38,000 --> 00:08:41,000
 in one's relationship with one's teacher.

125
00:08:41,000 --> 00:08:47,480
 You could also say it relates to our own ability to receive

126
00:08:47,480 --> 00:08:51,000
 criticism in general,

127
00:08:51,000 --> 00:08:56,040
 and therefore to our relationship with anyone who gives us

128
00:08:56,040 --> 00:08:57,000
 criticism,

129
00:08:57,000 --> 00:09:02,000
 because we receive criticism from all sorts of sources,

130
00:09:02,000 --> 00:09:08,000
 from many people who are not qualified to criticize.

131
00:09:08,000 --> 00:09:11,280
 How do you deal with people who criticize you unjustly, for

132
00:09:11,280 --> 00:09:12,000
 example?

133
00:09:12,000 --> 00:09:16,370
 This doesn't directly relate to that, but that's the

134
00:09:16,370 --> 00:09:20,000
 general subject that we're dealing with.

135
00:09:20,000 --> 00:09:28,140
 And it relates because this is so contrary to the state of

136
00:09:28,140 --> 00:09:29,000
 an ordinary human being,

137
00:09:29,000 --> 00:09:38,670
 which is to incline towards hiding one's fault and becoming

138
00:09:38,670 --> 00:09:42,000
 upset when people point out our faults,

139
00:09:42,000 --> 00:09:48,690
 criticizing people for being critical. Everyone's a critic,

140
00:09:48,690 --> 00:09:50,000
 you say.

141
00:09:50,000 --> 00:09:59,000
 And there are teachings that instruct students, teachers

142
00:09:59,000 --> 00:09:59,000
 will sometimes instruct students,

143
00:09:59,000 --> 00:10:01,210
 or there's books that are written that say you should

144
00:10:01,210 --> 00:10:05,000
 accept criticism and thank anyone who gives you criticism.

145
00:10:05,000 --> 00:10:09,860
 And I think there's some truth to that. But the response

146
00:10:09,860 --> 00:10:13,000
 and the skepticism is that

147
00:10:13,000 --> 00:10:17,840
 it leads you to allow others to walk all over you and to

148
00:10:17,840 --> 00:10:20,000
 give unjust criticism

149
00:10:20,000 --> 00:10:24,000
 and to leave unjust criticism unchallenged.

150
00:10:24,000 --> 00:10:28,680
 And I think that also is a good point, and it goes against

151
00:10:28,680 --> 00:10:31,000
 the Buddhist teaching.

152
00:10:31,000 --> 00:10:41,130
 In monastic society, there's a monk should be criticized,

153
00:10:41,130 --> 00:10:42,000
 not criticized,

154
00:10:42,000 --> 00:10:51,240
 but a monk is wrong, is at fault when they get angry at

155
00:10:51,240 --> 00:10:57,000
 criticism, so respond angrily or criticize in return.

156
00:10:57,000 --> 00:10:59,550
 So you tell me, "I did this wrong," and I say, "Well, you

157
00:10:59,550 --> 00:11:01,000
 did this other thing wrong,"

158
00:11:01,000 --> 00:11:06,440
 or to ignore people's criticism and not accept their

159
00:11:06,440 --> 00:11:08,000
 criticism.

160
00:11:08,000 --> 00:11:12,270
 All of this is faulty, but what's also faulty is to not

161
00:11:12,270 --> 00:11:14,000
 speak in one's defense.

162
00:11:14,000 --> 00:11:19,000
 One is at fault when one doesn't speak in one's defense.

163
00:11:19,000 --> 00:11:29,010
 So there's a sense of having to be mindful and to be wise

164
00:11:29,010 --> 00:11:30,000
 and to be balanced.

165
00:11:30,000 --> 00:11:37,060
 In many issues that we deal with in life, Buddhism doesn't

166
00:11:37,060 --> 00:11:40,000
 have a beyond all answer.

167
00:11:40,000 --> 00:11:43,000
 Buddhism is much more dealing with the building blocks.

168
00:11:43,000 --> 00:11:47,000
 What are the things that make up any given situation?

169
00:11:47,000 --> 00:11:50,560
 So there are much more general principles, and the general

170
00:11:50,560 --> 00:11:53,000
 principle is to be able to discern

171
00:11:53,000 --> 00:11:57,000
 the truth from falsehood and right from wrong.

172
00:11:57,000 --> 00:12:02,100
 But here we come to this specific example when the

173
00:12:02,100 --> 00:12:05,000
 criticism is warranted.

174
00:12:05,000 --> 00:12:10,080
 So we're dealing with someone who actually points out true

175
00:12:10,080 --> 00:12:11,000
 thoughts.

176
00:12:11,000 --> 00:12:17,830
 Because I think it can be said that there's quite often a

177
00:12:17,830 --> 00:12:21,000
 grain of truth in every criticism.

178
00:12:21,000 --> 00:12:24,000
 There's a reason why we're being criticized.

179
00:12:24,000 --> 00:12:28,370
 People will criticize us unwarranted and not wishing to

180
00:12:28,370 --> 00:12:29,000
 help us.

181
00:12:29,000 --> 00:12:34,570
 People criticize us often looking to upset us or looking to

182
00:12:34,570 --> 00:12:36,000
 humiliate us

183
00:12:36,000 --> 00:12:45,100
 or looking to hide our own fault, or criticize others to

184
00:12:45,100 --> 00:12:47,000
 hide our own fault, this kind of thing.

185
00:12:47,000 --> 00:12:51,380
 But even still, even in those cases, it's much easier to

186
00:12:51,380 --> 00:12:53,000
 see the faults of others

187
00:12:53,000 --> 00:12:58,610
 and we hide our own faults, but we're very good at picking

188
00:12:58,610 --> 00:13:02,000
 out the faults of others.

189
00:13:02,000 --> 00:13:06,990
 But here, in the case of a teacher, we have another dilemma

190
00:13:06,990 --> 00:13:07,000
,

191
00:13:07,000 --> 00:13:18,360
 and that's our inability to accept criticism, not because

192
00:13:18,360 --> 00:13:20,000
 it's not correct,

193
00:13:20,000 --> 00:13:33,390
 but because it's a fault, it hurts our ego, it attacks

194
00:13:33,390 --> 00:13:35,000
 something that we are protecting,

195
00:13:35,000 --> 00:13:37,000
 and that is ourself.

196
00:13:37,000 --> 00:13:43,000
 We hold ourselves dear, we cling to an image of who we are

197
00:13:43,000 --> 00:13:45,000
 or who we want to be,

198
00:13:45,000 --> 00:13:48,620
 and when that image is threatened, like anything, any

199
00:13:48,620 --> 00:13:54,000
 belonging, anything we hold dear, we react, we get upset.

200
00:13:54,000 --> 00:14:01,220
 And so this teaching is actually quite important for medit

201
00:14:01,220 --> 00:14:03,000
ators in a meditative setting,

202
00:14:03,000 --> 00:14:06,950
 because we need to take advice from others, unless you're

203
00:14:06,950 --> 00:14:11,000
 the kind of person who can become enlightened miraculously

204
00:14:11,000 --> 00:14:12,000
 by oneself.

205
00:14:12,000 --> 00:14:15,000
 We have to take advice from others.

206
00:14:15,000 --> 00:14:20,000
 And as a teacher, this is something that is quite familiar.

207
00:14:20,000 --> 00:14:27,240
 It often, it becomes so difficult that teachers are often

208
00:14:27,240 --> 00:14:30,000
 afraid to give advice,

209
00:14:30,000 --> 00:14:36,120
 and much of a teacher's role and duty is to find ways to

210
00:14:36,120 --> 00:14:42,000
 admonish one's students without upsetting them.

211
00:14:42,000 --> 00:14:46,790
 A great part of the skill of being a good teacher is the

212
00:14:46,790 --> 00:14:51,000
 ability to not upset one's student,

213
00:14:51,000 --> 00:14:54,430
 because anyone can give advice, and a real sign,

214
00:14:54,430 --> 00:14:59,000
 unfortunately, of a poor teacher,

215
00:14:59,000 --> 00:15:03,000
 is not being able to, it's not that they can't give advice,

216
00:15:03,000 --> 00:15:10,160
 but not being able to couch the advice in such delicate

217
00:15:10,160 --> 00:15:14,590
 terms or means that the student is actually able to accept

218
00:15:14,590 --> 00:15:15,000
 it.

219
00:15:15,000 --> 00:15:18,880
 So you'll often hear people giving advice, and as a teacher

220
00:15:18,880 --> 00:15:22,000
, you cringe because that's not going to work,

221
00:15:22,000 --> 00:15:24,000
 and you're just making the student upset.

222
00:15:24,000 --> 00:15:27,310
 What you're saying is correct, but it's not going to get

223
00:15:27,310 --> 00:15:28,000
 through.

224
00:15:28,000 --> 00:15:34,760
 It's unfortunate because it makes the teacher's job more

225
00:15:34,760 --> 00:15:36,000
 difficult,

226
00:15:36,000 --> 00:15:40,060
 and it hampers the teacher's ability to be frank with the

227
00:15:40,060 --> 00:15:41,000
 student.

228
00:15:41,000 --> 00:15:49,000
 And often a teacher's duty requires upsetting the student,

229
00:15:49,000 --> 00:15:52,290
 and sometimes you have no choice, your choice is to not

230
00:15:52,290 --> 00:15:55,000
 teach them or to hurt them, to upset them,

231
00:15:55,000 --> 00:15:59,000
 and you have to gauge how far you can push a student,

232
00:15:59,000 --> 00:16:05,000
 which is unfortunately usually not very far,

233
00:16:05,000 --> 00:16:09,000
 unless you're dealing with a special person like Radha,

234
00:16:09,000 --> 00:16:10,000
 which is why Surya Pud

235
00:16:10,000 --> 00:16:14,750
 but for most people it's very difficult for us to take

236
00:16:14,750 --> 00:16:16,000
 criticism,

237
00:16:16,000 --> 00:16:22,900
 and it's very hard for us to prevent the anger and the self

238
00:16:22,900 --> 00:16:26,600
-righteousness when people try to, even well-meaning, try to

239
00:16:26,600 --> 00:16:27,000
 help us.

240
00:16:27,000 --> 00:16:32,060
 And so we have to be aware of this, and that's one of the

241
00:16:32,060 --> 00:16:34,000
 important aspects of this teaching

242
00:16:34,000 --> 00:16:38,010
 that I think a lot of people react favorably to when this

243
00:16:38,010 --> 00:16:41,000
 imagery of pointing at buried treasure.

244
00:16:41,000 --> 00:16:46,180
 So we try to remind ourselves of this and to think of it as

245
00:16:46,180 --> 00:16:48,000
 someone pointing at buried treasure,

246
00:16:48,000 --> 00:16:52,200
 and that's why we'll come up with these teachings where we

247
00:16:52,200 --> 00:16:53,000
 tell people,

248
00:16:53,000 --> 00:16:57,740
 "Anyone who criticizes, you should thank them. You should

249
00:16:57,740 --> 00:17:03,000
 thank people who criticize."

250
00:17:03,000 --> 00:17:11,040
 I don't think it's, as I said, that when criticism is false

251
00:17:11,040 --> 00:17:13,910
, I don't think you should be hesitant to point out when it

252
00:17:13,910 --> 00:17:15,000
's false.

253
00:17:15,000 --> 00:17:18,210
 Like my teacher said, if someone calls you a buffalo, you

254
00:17:18,210 --> 00:17:20,000
 just turn around and feel if you have a tail.

255
00:17:20,000 --> 00:17:22,370
 If you don't have a tail, you should tell them, "I don't

256
00:17:22,370 --> 00:17:24,000
 have a tail. I'm not a buffalo."

257
00:17:24,000 --> 00:17:29,360
 He didn't quite say it like that. In fact, it's more

258
00:17:29,360 --> 00:17:31,000
 annoying for yourself.

259
00:17:31,000 --> 00:17:35,720
 And I think that's the key that we should point out is it

260
00:17:35,720 --> 00:17:37,000
 requires wisdom.

261
00:17:37,000 --> 00:17:41,910
 You have to be wise and know, "Is this person, what is

262
00:17:41,910 --> 00:17:44,000
 their motive for doing this?"

263
00:17:44,000 --> 00:17:46,470
 First of all, but regardless of their motive, is there

264
00:17:46,470 --> 00:17:48,000
 truth behind what they're saying?

265
00:17:52,000 --> 00:17:58,420
 Criticism is a very big part of the teaching dynamic, as

266
00:17:58,420 --> 00:18:01,000
 mentioned, but also a very big part of our practice,

267
00:18:01,000 --> 00:18:06,000
 because of course it's dealing with ego.

268
00:18:06,000 --> 00:18:12,070
 It's a good test of our state of mind and our purity of

269
00:18:12,070 --> 00:18:15,000
 mind, how well we're able to take criticism.

270
00:18:15,000 --> 00:18:19,940
 Criticism is actually a useful tool in our practice to put

271
00:18:19,940 --> 00:18:22,000
 ourselves in position

272
00:18:22,000 --> 00:18:26,780
 or to allow others to criticize us and to be mindful and to

273
00:18:26,780 --> 00:18:29,000
 meditate on the criticism,

274
00:18:29,000 --> 00:18:34,810
 on our reactions to the criticism, seeing how our mind

275
00:18:34,810 --> 00:18:36,000
 reacts.

276
00:18:36,000 --> 00:18:41,000
 An enlightened being is the same in both praise and blame.

277
00:18:41,000 --> 00:18:43,470
 When people praise them, it's as though they didn't even

278
00:18:43,470 --> 00:18:44,000
 hear it.

279
00:18:44,000 --> 00:18:47,560
 They don't have any pleasure, they don't take pleasure when

280
00:18:47,560 --> 00:18:48,000
 others,

281
00:18:48,000 --> 00:18:52,000
 they aren't excited when other people praise them.

282
00:18:52,000 --> 00:18:56,440
 But it's the same when others have insulted them or

283
00:18:56,440 --> 00:18:58,000
 criticized them.

284
00:18:58,000 --> 00:19:01,000
 It's also as though they didn't hear.

285
00:19:01,000 --> 00:19:04,130
 In a sense, they take it for what it is, they take it at

286
00:19:04,130 --> 00:19:05,000
 face value.

287
00:19:05,000 --> 00:19:08,180
 This person just said, "I'm doing something wrong," and

288
00:19:08,180 --> 00:19:09,000
 said, "Okay."

289
00:19:09,000 --> 00:19:12,350
 Well, they look at it and, "Right, I was doing nothing

290
00:19:12,350 --> 00:19:13,000
 wrong," and so they change it.

291
00:19:13,000 --> 00:19:18,000
 Or, "This person's upset at me and they want me to do this

292
00:19:18,000 --> 00:19:18,000
,"

293
00:19:18,000 --> 00:19:20,000
 or, "They want me to stop doing that."

294
00:19:20,000 --> 00:19:22,550
 "Okay, well, I'll stop doing it because that would upset

295
00:19:22,550 --> 00:19:23,000
 them."

296
00:19:23,000 --> 00:19:25,800
 You know, to a certain extent, but they do everything mind

297
00:19:25,800 --> 00:19:27,000
fully and with wisdom,

298
00:19:27,000 --> 00:19:29,000
 knowing what is the right state.

299
00:19:29,000 --> 00:19:32,060
 It's not that they're a pushover and someone tells them, "

300
00:19:32,060 --> 00:19:34,000
You're too fat, you should go on a diet."

301
00:19:34,000 --> 00:19:37,000
 They don't really see the point of that.

302
00:19:37,000 --> 00:19:40,000
 "Why would I concern myself with my weight?"

303
00:19:40,000 --> 00:19:43,000
 "You're too skinny, you should eat more."

304
00:19:43,000 --> 00:19:45,000
 "No, that's not why I eat."

305
00:19:45,000 --> 00:19:47,000
 But they don't get upset.

306
00:19:47,000 --> 00:19:51,000
 The point is they're unperturbed in both praise and blame.

307
00:19:51,000 --> 00:19:54,690
 We have to look at both because this is relating to the ego

308
00:19:54,690 --> 00:19:55,000
.

309
00:19:55,000 --> 00:20:02,000
 It's relating to our conceit, our view of self.

310
00:20:02,000 --> 00:20:05,000
 It's a good indicator.

311
00:20:05,000 --> 00:20:08,320
 And the other part of this quote is in regards to staying

312
00:20:08,320 --> 00:20:09,000
 with such a person.

313
00:20:09,000 --> 00:20:12,000
 The specific point that things get better.

314
00:20:12,000 --> 00:20:14,630
 And that's a very useful advice to give oneself when one is

315
00:20:14,630 --> 00:20:15,000
 angry.

316
00:20:15,000 --> 00:20:17,000
 Because you'll often get angry at your teacher.

317
00:20:17,000 --> 00:20:19,000
 It's a common thing.

318
00:20:19,000 --> 00:20:24,620
 I had one student recently who said, "I'm very angry at you

319
00:20:24,620 --> 00:20:25,000
."

320
00:20:25,000 --> 00:20:28,000
 I think that was a little worse.

321
00:20:28,000 --> 00:20:31,000
 "I'm very angry at you right now."

322
00:20:31,000 --> 00:20:33,000
 Because I had the... it wasn't even criticism.

323
00:20:33,000 --> 00:20:36,220
 It was asking to do something that just seemed utterly

324
00:20:36,220 --> 00:20:37,000
 ridiculous.

325
00:20:37,000 --> 00:20:42,390
 It seemed totally above and beyond what they were capable

326
00:20:42,390 --> 00:20:43,000
 of.

327
00:20:43,000 --> 00:20:47,000
 And I said, "Well, I didn't."

328
00:20:47,000 --> 00:20:50,810
 I said, "I didn't. This wasn't my idea. I wasn't the one

329
00:20:50,810 --> 00:20:52,000
 who created this."

330
00:20:52,000 --> 00:20:59,000
 And they said, "I'm very angry. They're very angry."

331
00:20:59,000 --> 00:21:02,000
 And this is quite common.

332
00:21:02,000 --> 00:21:05,420
 It's quite common for meditators to get angry at their

333
00:21:05,420 --> 00:21:06,000
 teacher.

334
00:21:06,000 --> 00:21:11,000
 And so there's no shame in that.

335
00:21:11,000 --> 00:21:13,000
 And there's no reason to get upset.

336
00:21:13,000 --> 00:21:16,000
 It's why we ask forgiveness when we do a meditation course.

337
00:21:16,000 --> 00:21:18,000
 We often formally ask forgiveness of the teacher.

338
00:21:18,000 --> 00:21:22,000
 And the teacher asks forgiveness of the student.

339
00:21:22,000 --> 00:21:24,760
 Both when we start the course and when we finish the course

340
00:21:24,760 --> 00:21:25,000
.

341
00:21:25,000 --> 00:21:29,000
 So it sort of clear the air.

342
00:21:29,000 --> 00:21:33,000
 But the point is, this verse is quite useful.

343
00:21:33,000 --> 00:21:37,400
 It's been useful for me to remind myself that no matter how

344
00:21:37,400 --> 00:21:38,000
 difficult things get,

345
00:21:38,000 --> 00:21:41,000
 staying with a teacher and at a meditation center,

346
00:21:41,000 --> 00:21:46,080
 you have to remember that we want to do that which makes

347
00:21:46,080 --> 00:21:50,000
 things better, makes it better.

348
00:21:50,000 --> 00:21:54,110
 Makes us better people, improves our situation, makes us

349
00:21:54,110 --> 00:21:55,000
 happier.

350
00:21:55,000 --> 00:21:57,000
 We have to remember that we know this.

351
00:21:57,000 --> 00:22:00,000
 We know that staying in a meditation center with a teacher

352
00:22:00,000 --> 00:22:01,000
 is going to make things better.

353
00:22:01,000 --> 00:22:07,000
 We often make excuses, "I'm not ready."

354
00:22:07,000 --> 00:22:09,660
 They say, "The environment's not right. I'm not at the

355
00:22:09,660 --> 00:22:11,000
 right point in my life."

356
00:22:11,000 --> 00:22:15,000
 But remember that this is all excuses.

357
00:22:15,000 --> 00:22:18,230
 The truth is, staying in a meditation center with a

358
00:22:18,230 --> 00:22:21,000
 meditation center, things will get better.

359
00:22:21,000 --> 00:22:26,000
 And whatever excuses we have for not staying are invalid.

360
00:22:26,000 --> 00:22:30,100
 Because here is a person who is willing to help us with our

361
00:22:30,100 --> 00:22:31,000
 faults.

362
00:22:31,000 --> 00:22:34,000
 The faults are what we're focusing on.

363
00:22:34,000 --> 00:22:38,590
 And people looking to criticize Buddhism will use this as a

364
00:22:38,590 --> 00:22:39,000
 criticism.

365
00:22:39,000 --> 00:22:42,000
 Buddhism is very pessimistic, which is ridiculous.

366
00:22:42,000 --> 00:22:47,000
 So utterly ridiculous, harmful, terrible, terrible

367
00:22:47,000 --> 00:22:48,000
 criticism.

368
00:22:48,000 --> 00:22:53,520
 I mean by, often by lazy people who aren't interested in

369
00:22:53,520 --> 00:22:56,000
 looking at their faults.

370
00:22:56,000 --> 00:22:59,000
 Because it's very damaging to say that.

371
00:22:59,000 --> 00:23:03,000
 The faults are the only reason for self-development.

372
00:23:03,000 --> 00:23:06,000
 It is our faults that we want to focus on.

373
00:23:06,000 --> 00:23:09,690
 Not so that we can feel bad about ourselves or feel

374
00:23:09,690 --> 00:23:11,000
 horrible people,

375
00:23:11,000 --> 00:23:14,000
 but to actually fix something, to make something better.

376
00:23:14,000 --> 00:23:16,600
 How do you make something better without looking at what's

377
00:23:16,600 --> 00:23:17,000
 wrong,

378
00:23:17,000 --> 00:23:20,000
 looking at the problem? And this is what we do.

379
00:23:20,000 --> 00:23:23,000
 If you don't have someone who can show you your faults,

380
00:23:23,000 --> 00:23:26,000
 who can point them out to you, who can help you fix them.

381
00:23:26,000 --> 00:23:30,000
 Obviously, it's not enough to point out one's fault,

382
00:23:30,000 --> 00:23:32,000
 but to actually help you fix them.

383
00:23:32,000 --> 00:23:34,000
 This is where things get better.

384
00:23:34,000 --> 00:23:40,890
 This is where true development and progress and goodness

385
00:23:40,890 --> 00:23:43,000
 comes from.

386
00:23:43,000 --> 00:23:48,000
 So, a very useful verse, something that we should remember.

387
00:23:48,000 --> 00:23:52,000
 Someone who points out your faults when they see them,

388
00:23:52,000 --> 00:23:56,310
 you should liken them to a person who points out very

389
00:23:56,310 --> 00:23:57,000
 treasure.

390
00:23:57,000 --> 00:23:59,810
 When you stay with, you should associate with such a person

391
00:23:59,810 --> 00:24:00,000
.

392
00:24:00,000 --> 00:24:03,000
 The wise should associate with such a person.

393
00:24:03,000 --> 00:24:06,000
 When such a person, when one associates with such a person,

394
00:24:06,000 --> 00:24:09,000
 things get better, not worse.

395
00:24:09,000 --> 00:24:14,000
 So, O Team of Papi. That's the verse for today.

396
00:24:14,000 --> 00:24:17,770
 That's our teaching on the Dhanutada. Thank you for tuning

397
00:24:17,770 --> 00:24:18,000
 in.

398
00:24:18,000 --> 00:24:20,000
 I wish you all the best.

399
00:24:20,000 --> 00:24:24,000
 Thank you.

400
00:24:24,000 --> 00:24:33,700
 [

401
00:24:33,700 --> 00:24:41,700
 Pause ]

