1
00:00:00,000 --> 00:00:04,500
 I'm trying to observe my emotions, but I'm somewhat out of

2
00:00:04,500 --> 00:00:05,840
 touch with them and I control

3
00:00:05,840 --> 00:00:07,040
 them a lot.

4
00:00:07,040 --> 00:00:10,250
 When an emotion goes away, how can I tell if it subsided on

5
00:00:10,250 --> 00:00:11,800
 its own or if I suppressed

6
00:00:11,800 --> 00:00:15,200
 it?

7
00:00:15,200 --> 00:00:21,010
 I wouldn't be so concerned or so analytical about it

8
00:00:21,010 --> 00:00:25,600
 because the analysis itself is probably

9
00:00:25,600 --> 00:00:28,440
 a misunderstanding of what's going on.

10
00:00:28,440 --> 00:00:33,340
 The concern about suppressing things, the concern about

11
00:00:33,340 --> 00:00:35,800
 causing yourself suffering,

12
00:00:35,800 --> 00:00:41,700
 about not accepting the experience, this concern that you

13
00:00:41,700 --> 00:00:46,320
're suppressing, that you're not accepting

14
00:00:46,320 --> 00:00:49,200
 and learning about the experience is often a

15
00:00:49,200 --> 00:00:51,840
 misunderstanding of what's going on.

16
00:00:51,840 --> 00:00:55,970
 The emotions themselves will cause stress and that's why

17
00:00:55,970 --> 00:00:58,120
 there's stress in the mind.

18
00:00:58,120 --> 00:01:02,000
 It's not necessarily because you're suppressing them.

19
00:01:02,000 --> 00:01:04,300
 What happens is when you have certain emotions, then

20
00:01:04,300 --> 00:01:06,320
 because of the complications in the mind,

21
00:01:06,320 --> 00:01:09,710
 we give rise to even more conflicting emotions and that's

22
00:01:09,710 --> 00:01:12,080
 where it starts to get confusing.

23
00:01:12,080 --> 00:01:14,720
 But desire is stressful.

24
00:01:14,720 --> 00:01:20,120
 This is something that you can see if your mind is quiet.

25
00:01:20,120 --> 00:01:25,030
 Not only anger is stressful, but desire itself is stressful

26
00:01:25,030 --> 00:01:27,320
 and will give you tension in

27
00:01:27,320 --> 00:01:31,840
 the body and suffering in the mind.

28
00:01:31,840 --> 00:01:34,680
 Probably what you're experiencing is quite a bit of

29
00:01:34,680 --> 00:01:36,400
 suffering and try to be objective

30
00:01:36,400 --> 00:01:37,400
 about that.

31
00:01:37,400 --> 00:01:42,040
 When these thoughts that you're having, the worries and the

32
00:01:42,040 --> 00:01:44,360
 confusion and the uncertainty

33
00:01:44,360 --> 00:01:47,480
 is also arising.

34
00:01:47,480 --> 00:01:49,980
 Remember that part of this difficulty that you're having,

35
00:01:49,980 --> 00:01:51,960
 the struggle that you're having

36
00:01:51,960 --> 00:01:56,520
 is the struggle to accept impermanent suffering in oneself.

37
00:01:56,520 --> 00:01:59,540
 The struggle to accept things just as they are, thinking

38
00:01:59,540 --> 00:02:01,120
 that something's wrong with

39
00:02:01,120 --> 00:02:02,120
 your practice.

40
00:02:02,120 --> 00:02:05,960
 It's actually kind of absurd that anything could be wrong

41
00:02:05,960 --> 00:02:07,560
 with your practice.

42
00:02:07,560 --> 00:02:12,320
 There is nothing called practice and you're not doing it.

43
00:02:12,320 --> 00:02:16,340
 There is only experience and it's coming and going and once

44
00:02:16,340 --> 00:02:18,320
 you realize that, then all

45
00:02:18,320 --> 00:02:22,440
 of these knots become untied.

46
00:02:22,440 --> 00:02:24,720
 Every moment is a new knot.

47
00:02:24,720 --> 00:02:30,320
 Every moment in your meditation is a new challenge and the

48
00:02:30,320 --> 00:02:33,880
 suppression occurs when we fail to

49
00:02:33,880 --> 00:02:35,200
 meet the challenge.

50
00:02:35,200 --> 00:02:39,310
 When at any given moment we stop the mindfulness, we stop

51
00:02:39,310 --> 00:02:42,560
 the awareness or we cling to something.

52
00:02:42,560 --> 00:02:46,490
 It's because we have very one-track minds. We don't have

53
00:02:46,490 --> 00:02:48,440
 the flexibility to be able to

54
00:02:48,440 --> 00:02:50,960
 move from one challenge to the next.

55
00:02:50,960 --> 00:02:56,600
 This is one explanation of why enlightened people are so

56
00:02:56,600 --> 00:02:59,280
 bright and so clear in mind

57
00:02:59,280 --> 00:03:03,920
 because they're able to move from one challenge to the next

58
00:03:03,920 --> 00:03:04,280
.

59
00:03:04,280 --> 00:03:08,490
 You might get one challenge and you might get it and you

60
00:03:08,490 --> 00:03:10,720
 untie some knot and then you

61
00:03:10,720 --> 00:03:13,660
 get so caught up in that that the next challenge comes and

62
00:03:13,660 --> 00:03:15,320
 you miss it and you try to react

63
00:03:15,320 --> 00:03:17,040
 to the next moment in the same way.

64
00:03:17,040 --> 00:03:22,040
 For example, you focus on pain and finally you get it.

65
00:03:22,040 --> 00:03:26,080
 Your mind realizes it's just pain and then some kind of

66
00:03:26,080 --> 00:03:28,360
 lust comes up and then you try

67
00:03:28,360 --> 00:03:33,440
 to deal with it in the same way.

68
00:03:33,440 --> 00:03:35,610
 Maybe going back to the feeling or even going back to the

69
00:03:35,610 --> 00:03:36,920
 pain and you're not able to deal

70
00:03:36,920 --> 00:03:42,200
 with the new experience. It has to be dealt with in a

71
00:03:42,200 --> 00:03:42,480
 totally different way or be dealt

72
00:03:42,480 --> 00:03:51,340
 with in the same way but requires another shift of

73
00:03:51,340 --> 00:03:53,600
 awareness.

74
00:03:53,600 --> 00:03:58,890
 The first shift of awareness was to convince yourself that

75
00:03:58,890 --> 00:04:01,600
 the pain is rather than being

76
00:04:01,600 --> 00:04:04,800
 bad that it is just pain.

77
00:04:04,800 --> 00:04:07,040
 It's shifting to see that it's just pain.

78
00:04:07,040 --> 00:04:11,360
 The last, again, you have to shift to see that it's just a

79
00:04:11,360 --> 00:04:14,040
 feeling of pleasure and chemicals

80
00:04:14,040 --> 00:04:15,640
 arising in the body.

81
00:04:15,640 --> 00:04:18,630
 You have to see it for what it is but it's coming from the

82
00:04:18,630 --> 00:04:20,320
 other of another angle, the

83
00:04:20,320 --> 00:04:24,110
 angle of it being a good thing and of wanting to cling to

84
00:04:24,110 --> 00:04:24,600
 it.

85
00:04:24,600 --> 00:04:27,510
 You have to convince yourself in another way or you have to

86
00:04:27,510 --> 00:04:29,580
 teach yourself in another way.

87
00:04:29,580 --> 00:04:36,500
 This is the way of letting go. Pain is teaching yourself in

88
00:04:36,500 --> 00:04:47,680
 the way of letting come, for example.

89
00:04:47,680 --> 00:04:53,800
 The difficulty is not in suppressing.

90
00:04:53,800 --> 00:04:57,160
 The difficulty is in keeping up, in sticking with the

91
00:04:57,160 --> 00:04:59,440
 rhythm, staying with the rhythm or

92
00:04:59,440 --> 00:05:06,160
 keeping up with the rhythm, in adapting to the situation.

93
00:05:06,160 --> 00:05:12,630
 Which in the end simply means no matter which angle the

94
00:05:12,630 --> 00:05:16,880
 mind is taking, straighten it out

95
00:05:16,880 --> 00:05:18,880
 in the same direction.

96
00:05:18,880 --> 00:05:22,120
 If it's crooked this way, straighten it back this way.

97
00:05:22,120 --> 00:05:24,240
 If it's crooked this way, straighten it back this way.

98
00:05:24,240 --> 00:05:26,440
 Everything has to be straightened out.

99
00:05:26,440 --> 00:05:29,020
 Whatever the mind is clinging to, you have to bring it back

100
00:05:29,020 --> 00:05:29,800
 to straight.

101
00:05:29,800 --> 00:05:36,120
 If it's clinging, you have to bring it back to center.

102
00:05:36,120 --> 00:05:39,150
 Everything is just about learning how to do that and

103
00:05:39,150 --> 00:05:41,200
 teaching yourself to do that with

104
00:05:41,200 --> 00:05:46,120
 everything, to be clearly aware and simply clearly aware of

105
00:05:46,120 --> 00:05:47,440
 everything.

106
00:05:47,440 --> 00:05:52,990
 The problem, I think it's a red herring, it's a false alarm

107
00:05:52,990 --> 00:05:54,640
, a false problem that we come

108
00:05:54,640 --> 00:05:57,140
 into thinking that we're suppressing things, thinking that

109
00:05:57,140 --> 00:05:58,160
 we're controlling.

110
00:05:58,160 --> 00:06:02,440
 When that feeling that you identify with some kind of

111
00:06:02,440 --> 00:06:05,120
 suppression comes up, acknowledge

112
00:06:05,120 --> 00:06:08,120
 it just as a feeling, acknowledge it for what it is.

113
00:06:08,120 --> 00:06:10,930
 As you see clearer, you should be able to see that actually

114
00:06:10,930 --> 00:06:12,200
 it's just arising based

115
00:06:12,200 --> 00:06:17,160
 on the defilements, based on the emotions.

116
00:06:17,160 --> 00:06:22,500
 For example, one thing that happens is people say, "Well,

117
00:06:22,500 --> 00:06:25,720
 when I acknowledge thinking, thinking,

118
00:06:25,720 --> 00:06:28,320
 thinking, it feels like I get a big headache."

119
00:06:28,320 --> 00:06:31,510
 When I say to myself, "Thinking, thinking, I just get a big

120
00:06:31,510 --> 00:06:32,200
 headache."

121
00:06:32,200 --> 00:06:34,390
 When I'm thinking a lot, I don't have the headache, but as

122
00:06:34,390 --> 00:06:35,520
 soon as I say, "Thinking,

123
00:06:35,520 --> 00:06:37,280
 thinking, thinking," I get a big headache.

124
00:06:37,280 --> 00:06:41,310
 You start to think, "Well, this acknowledging is really

125
00:06:41,310 --> 00:06:43,560
 just causing me suffering."

126
00:06:43,560 --> 00:06:45,860
 Once you really get good at it, and once you really see

127
00:06:45,860 --> 00:06:47,280
 what's going on, you say, "No,

128
00:06:47,280 --> 00:06:48,640
 no, no, that's not what's going on."

129
00:06:48,640 --> 00:06:53,140
 Here I've been thinking for a half an hour, building up

130
00:06:53,140 --> 00:06:55,520
 this huge, huge headache without

131
00:06:55,520 --> 00:06:57,320
 knowing it.

132
00:06:57,320 --> 00:06:59,190
 Because I don't want to face the headache, I just think

133
00:06:59,190 --> 00:06:59,560
 more.

134
00:06:59,560 --> 00:07:02,690
 I just distract my attention further and further, and it's

135
00:07:02,690 --> 00:07:04,040
 so much fun to do that.

136
00:07:04,040 --> 00:07:05,660
 There's building up, building up, building up this huge

137
00:07:05,660 --> 00:07:06,680
 headache, and then finally you

138
00:07:06,680 --> 00:07:08,910
 realize, "I've been thinking," and you bring yourself back

139
00:07:08,910 --> 00:07:10,080
 and say, "Thinking, thinking,"

140
00:07:10,080 --> 00:07:11,080
 and boom.

141
00:07:11,080 --> 00:07:13,800
 Suddenly, you've got to deal with a headache.

142
00:07:13,800 --> 00:07:16,170
 You say to yourself, "Saying, thinking, thinking, thinking,

143
00:07:16,170 --> 00:07:17,960
 that's just suppressing it.

144
00:07:17,960 --> 00:07:21,080
 That's not really facing it."

145
00:07:21,080 --> 00:07:24,360
 It's a misunderstanding of what's really going on.

146
00:07:24,360 --> 00:07:27,240
 When you have lust or desire, and you focus on, you say, "W

147
00:07:27,240 --> 00:07:29,160
anting, wanting," or "Pleasure,

148
00:07:29,160 --> 00:07:32,080
 pleasure," then suddenly you feel this tension in the body.

149
00:07:32,080 --> 00:07:35,000
 You feel this tension that's coming up, but it's this

150
00:07:35,000 --> 00:07:37,480
 tension that was there because of

151
00:07:37,480 --> 00:07:43,330
 the desire itself, where you feel the dullness of mind that

152
00:07:43,330 --> 00:07:45,520
 comes with desire.

153
00:07:45,520 --> 00:07:48,480
 You think, "Oh, this is coming from the meditation.

154
00:07:48,480 --> 00:07:51,840
 You think, "This meditation, I'm suppressing this, it's

155
00:07:51,840 --> 00:07:53,760
 giving me this feeling of dullness,"

156
00:07:53,760 --> 00:07:58,440
 but actually the desire is what's bringing it.

157
00:07:58,440 --> 00:08:01,560
 The emotions themselves are bringing these as a result.

158
00:08:01,560 --> 00:08:04,420
 When you're mindful, you're just beginning to see

159
00:08:04,420 --> 00:08:06,040
 objectively, and you'll see that.

160
00:08:06,040 --> 00:08:08,560
 Actually, the moment when you have desire, you're not

161
00:08:08,560 --> 00:08:09,960
 seeing things objectively.

162
00:08:09,960 --> 00:08:12,680
 That's why it feels wonderful.

163
00:08:12,680 --> 00:08:15,520
 The desire feels wonderful until you stop, because suddenly

164
00:08:15,520 --> 00:08:16,960
 you're objective again, and

165
00:08:16,960 --> 00:08:18,440
 you're seeing both sides.

166
00:08:18,440 --> 00:08:21,660
 Desire is like, here you have the good, here you have the

167
00:08:21,660 --> 00:08:23,520
 bad, running towards the good

168
00:08:23,520 --> 00:08:26,010
 and running away from the bad, so that you never really

169
00:08:26,010 --> 00:08:26,880
 experience it.

170
00:08:26,880 --> 00:08:30,060
 It just builds up, builds up, builds up until you stop, and

171
00:08:30,060 --> 00:08:31,720
 then you experience it, and

172
00:08:31,720 --> 00:08:34,520
 you have to deal with it.

173
00:08:34,520 --> 00:08:35,760
 I wouldn't worry too much.

174
00:08:35,760 --> 00:08:38,880
 Even if you are suppressing, the most important thing is to

175
00:08:38,880 --> 00:08:40,580
 watch the suppression, not to

176
00:08:40,580 --> 00:08:42,240
 stop suppressing.

177
00:08:42,240 --> 00:08:44,750
 It's just to watch and to see what's going on, because in

178
00:08:44,750 --> 00:08:46,200
 the end you'll see impermanent

179
00:08:46,200 --> 00:08:47,200
 suffering and non-self.

180
00:08:47,200 --> 00:08:51,600
 You'll see that you can't control it.

181
00:08:51,600 --> 00:08:54,060
 Even the suppressing is something that happens sometimes,

182
00:08:54,060 --> 00:08:55,360
 doesn't happen sometimes.

183
00:08:55,360 --> 00:08:59,260
 It's worth getting caught up in, not worth getting attached

184
00:08:59,260 --> 00:08:59,720
 to.

185
00:08:59,720 --> 00:09:24,300
 [

