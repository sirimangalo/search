1
00:00:00,000 --> 00:00:05,620
 One, is it possible to transfer the merit of good deeds to

2
00:00:05,620 --> 00:00:07,600
 a departed person?

3
00:00:07,600 --> 00:00:14,050
 Two, is it possible to reconnect with a departed person in

4
00:00:14,050 --> 00:00:15,000
 any way?

5
00:00:15,000 --> 00:00:21,540
 Well, you can, of course, transfer the merits of good deeds

6
00:00:21,540 --> 00:00:23,960
 to a departed person.

7
00:00:23,960 --> 00:00:26,270
 The question, which I assume is what you're asking, is

8
00:00:26,270 --> 00:00:29,120
 whether it actually gets to them,

9
00:00:29,120 --> 00:00:36,400
 which is another question entirely.

10
00:00:36,400 --> 00:00:40,320
 I think the standard answer is that it may or it may not,

11
00:00:40,320 --> 00:00:42,080
 but I think if we're going

12
00:00:42,080 --> 00:00:49,380
 to be open-minded or kind of objective about it, that it

13
00:00:49,380 --> 00:00:53,200
 probably always gets to the person

14
00:00:53,200 --> 00:00:56,280
 in the following way.

15
00:00:56,280 --> 00:01:01,510
 The standard way of understanding the transferring of merit

16
00:01:01,510 --> 00:01:05,240
 is that maybe, just maybe, those

17
00:01:05,240 --> 00:01:09,300
 beings are hanging around as ghosts and waiting for

18
00:01:09,300 --> 00:01:12,400
 somebody to do something and dedicate

19
00:01:12,400 --> 00:01:17,690
 it to them so they can rejoice in it and receive the merit

20
00:01:17,690 --> 00:01:20,480
 of rejoicing in someone else's

21
00:01:20,480 --> 00:01:27,020
 deeds, which apparently can have profound effects on the

22
00:01:27,020 --> 00:01:29,200
 psyche of a ghost because they

23
00:01:29,200 --> 00:01:33,560
 are ethereal beings, they're fine material.

24
00:01:33,560 --> 00:01:36,320
 Just as when angels get angry, they're very quick to fall

25
00:01:36,320 --> 00:01:38,080
 from the state of being an angel.

26
00:01:38,080 --> 00:01:40,760
 An angel can actually die by getting angry.

27
00:01:40,760 --> 00:01:43,840
 They're so susceptible to emotions.

28
00:01:43,840 --> 00:01:49,160
 If a ghost really feels validly appreciative, like

29
00:01:49,160 --> 00:01:53,360
 something has really been done for their

30
00:01:53,360 --> 00:01:58,310
 benefit on their behalf, then they can very quickly receive

31
00:01:58,310 --> 00:02:00,240
 the benefit of that.

32
00:02:00,240 --> 00:02:08,590
 Why I think it's actually more widely applicable than just

33
00:02:08,590 --> 00:02:12,760
 for ghosts is that there's no categorical

34
00:02:12,760 --> 00:02:15,560
 difference between a ghost and a human being.

35
00:02:15,560 --> 00:02:23,360
 It's just more of a course of existence, more of a being.

36
00:02:23,360 --> 00:02:26,390
 The coarseness gets in the way of transferring merit, gets

37
00:02:26,390 --> 00:02:28,200
 in the way of the person receiving

38
00:02:28,200 --> 00:02:30,160
 it.

39
00:02:30,160 --> 00:02:34,580
 If you take the example of simply sending loving kindness

40
00:02:34,580 --> 00:02:37,200
 to, say, another human being,

41
00:02:37,200 --> 00:02:41,070
 there are many ways, more than one way, in which you can

42
00:02:41,070 --> 00:02:43,720
 understand that the person receives

43
00:02:43,720 --> 00:02:47,840
 the benefit of your loving kindness.

44
00:02:47,840 --> 00:02:52,520
 People will often relate, and this is anecdotal, that after

45
00:02:52,520 --> 00:02:55,440
 sending loving kindness, the person

46
00:02:55,440 --> 00:02:59,520
 on the receiving end suddenly feels happy, even though they

47
00:02:59,520 --> 00:03:01,280
 might be at a distance.

48
00:03:01,280 --> 00:03:05,120
 I don't know whether that would even hold in a clinical

49
00:03:05,120 --> 00:03:08,160
 trial, like if they were to do

50
00:03:08,160 --> 00:03:13,520
 a double-blind scientific study as to whether advanced med

51
00:03:13,520 --> 00:03:16,840
itators could actually send loving

52
00:03:16,840 --> 00:03:18,960
 kindness and it be received in that way.

53
00:03:18,960 --> 00:03:23,600
 But regardless, the most secular and rational way of

54
00:03:23,600 --> 00:03:27,320
 understanding the transference of merit

55
00:03:27,320 --> 00:03:32,890
 and loving kindness is the effect that the transferring has

56
00:03:32,890 --> 00:03:35,200
 on the mind of the person

57
00:03:35,200 --> 00:03:38,680
 who does the transferring.

58
00:03:38,680 --> 00:03:41,040
 Suppose if the person is alive, which is the easiest case,

59
00:03:41,040 --> 00:03:42,600
 and you send them loving kindness,

60
00:03:42,600 --> 00:03:45,000
 or in this case, you dedicate merit to them.

61
00:03:45,000 --> 00:03:47,640
 May this be for the benefit of all my relatives, and then

62
00:03:47,640 --> 00:03:49,360
 you maybe even specify someone.

63
00:03:49,360 --> 00:03:53,480
 May this be of great benefit to my mother or my father who

64
00:03:53,480 --> 00:03:55,720
 is angry at me, upset at me,

65
00:03:55,720 --> 00:03:57,160
 or so on.

66
00:03:57,160 --> 00:04:02,480
 Then that changes the way you think of that person, which

67
00:04:02,480 --> 00:04:05,680
 will have an actual and empirical

68
00:04:05,680 --> 00:04:08,900
 effect on your relationship with them the next time you

69
00:04:08,900 --> 00:04:09,400
 meet.

70
00:04:09,400 --> 00:04:13,070
 You'll find that suddenly they look and seem like a

71
00:04:13,070 --> 00:04:15,960
 different person based on the changes

72
00:04:15,960 --> 00:04:19,280
 that have gone on in your mind.

73
00:04:19,280 --> 00:04:22,990
 But I would say it potentially goes further than that,

74
00:04:22,990 --> 00:04:25,520
 because every action, of course,

75
00:04:25,520 --> 00:04:28,360
 has ripple effects in the universe around us.

76
00:04:28,360 --> 00:04:30,430
 Whether it actually gets to the person and they feel happy

77
00:04:30,430 --> 00:04:31,560
 because of something you've

78
00:04:31,560 --> 00:04:35,020
 done for them, it certainly will affect your life, even

79
00:04:35,020 --> 00:04:36,720
 before you meet the person.

80
00:04:36,720 --> 00:04:41,300
 Now, after the person has died, it's only an extrapolation

81
00:04:41,300 --> 00:04:43,200
 to suggest that if there

82
00:04:43,200 --> 00:04:48,150
 is a rebirth and if you are born based on karma, that you

83
00:04:48,150 --> 00:04:50,560
 are far more likely to have

84
00:04:50,560 --> 00:04:55,690
 a positive relationship with that person in the future or

85
00:04:55,690 --> 00:04:58,320
 to work out more quickly your

86
00:04:58,320 --> 00:05:01,890
 negative relationship with the person based on sending them

87
00:05:01,890 --> 00:05:02,520
 merit.

88
00:05:02,520 --> 00:05:11,600
 In a logical, not secular, but rationalist point of view,

89
00:05:11,600 --> 00:05:15,440
 it's not difficult to see

90
00:05:15,440 --> 00:05:17,360
 or it makes perfect sense.

91
00:05:17,360 --> 00:05:21,250
 It makes more sense to suggest that there is a benefit that

92
00:05:21,250 --> 00:05:23,280
 comes from dedicating merit

93
00:05:23,280 --> 00:05:25,640
 to all beings.

94
00:05:25,640 --> 00:05:29,300
 It's goodness to do that, to share the goodness and to wish

95
00:05:29,300 --> 00:05:31,360
 for other beings to rejoice just

96
00:05:31,360 --> 00:05:34,900
 the way you rejoice, because the biggest benefit of doing a

97
00:05:34,900 --> 00:05:37,280
 good deed is how it makes you feel,

98
00:05:37,280 --> 00:05:39,640
 is the effect that it has on your mind.

99
00:05:39,640 --> 00:05:44,920
 When you wish for other beings to get the benefit of your

100
00:05:44,920 --> 00:05:47,320
 goodness, you're not saying

101
00:05:47,320 --> 00:05:50,200
 well may they become rich based on the money that I've

102
00:05:50,200 --> 00:05:51,200
 given or so on.

103
00:05:51,200 --> 00:05:59,400
 You're saying may they become generous, may they change

104
00:05:59,400 --> 00:06:02,040
 their outlook.

105
00:06:02,040 --> 00:06:06,510
 The benefit comes from, according to Buddhism, the idea of

106
00:06:06,510 --> 00:06:08,920
 them getting the benefit comes

107
00:06:08,920 --> 00:06:12,480
 from their rejoicing in it or comes from the change in

108
00:06:12,480 --> 00:06:14,760
 their own minds, which can come

109
00:06:14,760 --> 00:06:20,350
 about in many ways, including how you relate to the person

110
00:06:20,350 --> 00:06:24,120
 as a result of giving the dedication.

111
00:06:24,120 --> 00:06:27,420
 There's two kinds of merit involved, the merit of giving,

112
00:06:27,420 --> 00:06:29,040
 which changes your mind and the

113
00:06:29,040 --> 00:06:32,320
 merit of rejoicing.

114
00:06:32,320 --> 00:06:39,520
 There are often mentions made or determinations made.

115
00:06:39,520 --> 00:06:42,200
 When you give merit, you think well if the person is

116
00:06:42,200 --> 00:06:44,080
 listening, if they're here right

117
00:06:44,080 --> 00:06:47,710
 now as an angel or a ghost or whatever, may they receive

118
00:06:47,710 --> 00:06:49,280
 the merit directly.

119
00:06:49,280 --> 00:06:53,720
 If they're not, may the angels who are hanging around here

120
00:06:53,720 --> 00:06:56,000
 rejoice in it and on my behalf

121
00:06:56,000 --> 00:07:03,220
 go and tell that person, wherever they have been born and

122
00:07:03,220 --> 00:07:04,360
 so on.

123
00:07:04,360 --> 00:07:07,880
 They will make that sort of wish or prayer.

124
00:07:07,880 --> 00:07:09,880
 That's the first question.

125
00:07:09,880 --> 00:07:13,750
 The second question, is it possible to reconnect with the

126
00:07:13,750 --> 00:07:15,160
 departed person?

127
00:07:15,160 --> 00:07:19,800
 Obviously not if they've been born as a human or an animal,

128
00:07:19,800 --> 00:07:22,240
 except by meeting them, by somehow

129
00:07:22,240 --> 00:07:27,070
 finding them like the Tibetan lamas and monks seem to be

130
00:07:27,070 --> 00:07:27,920
 very good.

131
00:07:27,920 --> 00:07:32,280
 The astrologers tend to be very good at it.

132
00:07:32,280 --> 00:07:34,080
 Suppose they are a ghost, right?

133
00:07:34,080 --> 00:07:38,180
 There are ways, psychics are able to enter into states,

134
00:07:38,180 --> 00:07:40,400
 Buddhist meditators are able

135
00:07:40,400 --> 00:07:43,680
 to enter into states where they're able to talk to ghosts.

136
00:07:43,680 --> 00:07:46,990
 Lots of meditators have told me that they're able to see

137
00:07:46,990 --> 00:07:48,960
 and talk to ghosts, so whether

138
00:07:48,960 --> 00:07:55,710
 it's real or not, as a Buddhist meditator it makes sense to

139
00:07:55,710 --> 00:07:56,480
 me.

140
00:07:56,480 --> 00:08:00,000
 In this case only if they're a ghost or an angel.

141
00:08:00,000 --> 00:08:02,530
 More likely if they're a ghost because angels don't tend to

142
00:08:02,530 --> 00:08:03,760
 have much to do with humans

143
00:08:03,760 --> 00:08:04,760
 apparently.

144
00:08:04,760 --> 00:08:12,720
 Humans are putrid and have a vile smell.

145
00:08:12,720 --> 00:08:15,390
 An angel, they say an angel can smell a human being about a

146
00:08:15,390 --> 00:08:17,040
 thousand leagues away or something

147
00:08:17,040 --> 00:08:19,280
 like that.

148
00:08:19,280 --> 00:08:22,400
 They don't have much to do with that.

149
00:08:22,400 --> 00:08:25,010
 That's why you see ghosts far more often than angels

150
00:08:25,010 --> 00:08:26,880
 because the angels are quite turned

151
00:08:26,880 --> 00:08:27,760
 off by us.

152
00:08:27,760 --> 00:08:28,760
 So, that's why I'm here.

153
00:08:28,760 --> 00:08:28,760
 Thank you.

154
00:08:28,760 --> 00:08:29,760
 Thank you.

155
00:08:29,760 --> 00:08:30,760
 Thank you.

