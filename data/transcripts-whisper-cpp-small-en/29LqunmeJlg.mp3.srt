1
00:00:00,000 --> 00:00:05,000
 Okay, so the final video, what's in my pocket.

2
00:00:05,000 --> 00:00:10,000
 Let's see.

3
00:00:10,000 --> 00:00:15,000
 Being a monk, actually I have a few pockets here.

4
00:00:15,000 --> 00:00:21,000
 The first pocket is...

5
00:00:21,000 --> 00:00:26,000
 Let's see if you can get that.

6
00:00:26,000 --> 00:00:31,000
 This pocket is this.

7
00:00:31,000 --> 00:00:35,000
 This is something I made myself.

8
00:00:35,000 --> 00:00:38,000
 I've got a couple of pockets there.

9
00:00:38,000 --> 00:00:44,000
 And in it are a piece of cloth.

10
00:00:44,000 --> 00:00:51,000
 This is a handkerchief for gloves

11
00:00:51,000 --> 00:00:56,000
 for whatever I need to wipe up.

12
00:00:56,000 --> 00:01:02,000
 This is my card wallet

13
00:01:02,000 --> 00:01:09,000
 for carrying business cards and carrying this.

14
00:01:09,000 --> 00:01:13,000
 This is a multi-tool.

15
00:01:13,000 --> 00:01:16,000
 One thing I do is fix things myself.

16
00:01:16,000 --> 00:01:23,000
 I fix and take things apart.

17
00:01:23,000 --> 00:01:28,000
 Put them back together less, but take them apart more.

18
00:01:28,000 --> 00:01:31,000
 This even has a pen.

19
00:01:31,000 --> 00:01:36,000
 This is what you see me drawing with in the earlier video.

20
00:01:36,000 --> 00:01:42,000
 A knife.

21
00:01:42,000 --> 00:01:48,000
 This is just something I use for whatever.

22
00:01:48,000 --> 00:01:52,000
 The business cards I keep...

23
00:01:52,000 --> 00:01:54,000
 I only keep two business cards.

24
00:01:54,000 --> 00:01:56,000
 Here I have another.

25
00:01:56,000 --> 00:02:04,000
 Here we have the sergeant of the Moor Park Police.

26
00:02:04,000 --> 00:02:07,000
 This is the guy we went to see recently

27
00:02:07,000 --> 00:02:11,000
 to let him know that I'm going to be staying here.

28
00:02:11,000 --> 00:02:17,000
 The hope is that somehow he's going to help to mitigate

29
00:02:17,000 --> 00:02:27,000
 some of the backlash from daring to walk on alms around.

30
00:02:27,000 --> 00:02:34,000
 This is a Thai police officer

31
00:02:34,000 --> 00:02:42,800
 who lives in Los Angeles and offered me his card and his

32
00:02:42,800 --> 00:02:44,000
 telephone

33
00:02:44,000 --> 00:02:49,000
 in case I have further problems with the police.

34
00:02:49,000 --> 00:02:55,980
 This is just an address of a man who actually does

35
00:02:55,980 --> 00:02:57,000
 broadcasting

36
00:02:57,000 --> 00:03:00,000
 at a monastery in person.

37
00:03:00,000 --> 00:03:06,000
 He does a show that they broadcast over the internet.

38
00:03:06,000 --> 00:03:11,000
 My ID card.

39
00:03:11,000 --> 00:03:15,000
 I got an ID. I don't live here. I'm actually from Canada.

40
00:03:15,000 --> 00:03:18,000
 I got an ID card.

41
00:03:18,000 --> 00:03:23,000
 This little thing is actually the reason why I was put in

42
00:03:23,000 --> 00:03:23,000
 jail

43
00:03:23,000 --> 00:03:24,000
 in the first place.

44
00:03:24,000 --> 00:03:28,000
 It wasn't because they claimed that I was a streaker.

45
00:03:28,000 --> 00:03:33,190
 It was because once they claimed it, they couldn't just

46
00:03:33,190 --> 00:03:35,000
 give me

47
00:03:35,000 --> 00:03:39,000
 whatever I guess a summons or whatever to appear in court

48
00:03:39,000 --> 00:03:41,000
 because they didn't have my ID on me.

49
00:03:41,000 --> 00:03:44,000
 Because they didn't have my ID on me, they put me in jail.

50
00:03:44,000 --> 00:03:46,000
 That's how it went.

51
00:03:46,000 --> 00:03:52,000
 Here's my cards.

52
00:03:52,000 --> 00:04:03,000
 These are the cards that I give out to people.

53
00:04:03,000 --> 00:04:09,000
 These cards have my name on it, my website.

54
00:04:09,000 --> 00:04:14,000
 These are the four foundations of mindfulness.

55
00:04:14,000 --> 00:04:16,420
 These are four things that we have to keep in mind when we

56
00:04:16,420 --> 00:04:17,000
 meditate.

57
00:04:17,000 --> 00:04:20,000
 We have them both in English and Thai.

58
00:04:20,000 --> 00:04:23,000
 If you look at my videos, you can see what they mean.

59
00:04:23,000 --> 00:04:26,000
 Then a close-up picture of me on the back

60
00:04:26,000 --> 00:04:32,100
 with something I have been fond of thinking and fond of

61
00:04:32,100 --> 00:04:35,000
 telling my students.

62
00:04:35,000 --> 00:04:39,620
 Then I got some cards for some reason for the people who

63
00:04:39,620 --> 00:04:44,000
 own these properties.

64
00:04:44,000 --> 00:04:48,000
 This is where I'm staying.

65
00:04:48,000 --> 00:04:59,000
 These are my keys to an apartment.

66
00:04:59,000 --> 00:05:02,600
 I also had some keys to another apartment, which is more

67
00:05:02,600 --> 00:05:03,000
 exciting

68
00:05:03,000 --> 00:05:08,000
 because we have another apartment.

69
00:05:08,000 --> 00:05:13,000
 We have now three apartments here,

70
00:05:13,000 --> 00:05:17,000
 which means that more people can come to meditate with us.

71
00:05:17,000 --> 00:05:19,000
 These are just my keys.

72
00:05:19,000 --> 00:05:22,000
 This one is the key downstairs, 69.

73
00:05:22,000 --> 00:05:24,000
 There's someone staying there right now.

74
00:05:24,000 --> 00:05:27,320
 It's a double bedroom, so there's room for someone else to

75
00:05:27,320 --> 00:05:28,000
 come and stay.

76
00:05:28,000 --> 00:05:31,000
 This is 70. This is the room I'm staying in.

77
00:05:31,000 --> 00:05:41,000
 This is my mailbox key in case I get a new mail.

78
00:05:41,000 --> 00:05:45,000
 If I get my address, I get some fan mail.

79
00:05:45,000 --> 00:05:48,000
 This is in my pocket.

80
00:05:48,000 --> 00:05:56,000
 This is the receiver for the wireless microphone.

81
00:05:56,000 --> 00:06:00,000
 It really shouldn't be in the video.

82
00:06:00,000 --> 00:06:06,000
 This is the pockets of my shirt.

83
00:06:06,000 --> 00:06:10,000
 This is a paper clip that I got from the DMV.

84
00:06:10,000 --> 00:06:18,000
 We went to register a trailer,

85
00:06:18,000 --> 00:06:24,560
 one of those RV trailers that is a room with room on wheels

86
00:06:24,560 --> 00:06:25,000
.

87
00:06:25,000 --> 00:06:28,000
 Apparently, they consider that a vehicle.

88
00:06:28,000 --> 00:06:31,000
 That's not a driver's license that I got. It's just an ID.

89
00:06:31,000 --> 00:06:33,420
 I don't have a driver's license, and yet I have to register

90
00:06:33,420 --> 00:06:34,000
 a vehicle.

91
00:06:34,000 --> 00:06:38,000
 I don't quite understand it. It's a trailer.

92
00:06:38,000 --> 00:06:41,000
 The trailers apparently need registration.

93
00:06:41,000 --> 00:06:49,370
 We got there, and they charged us $465 for registration and

94
00:06:49,370 --> 00:06:53,000
 fines.

95
00:06:53,000 --> 00:06:57,000
 But I got a paper clip. There's the deal.

96
00:06:57,000 --> 00:07:01,000
 They got somebody's money, and I got their paper clip.

97
00:07:01,000 --> 00:07:04,000
 I didn't steal it. They gave it to me with the papers.

98
00:07:04,000 --> 00:07:09,000
 These are medicine.

99
00:07:09,000 --> 00:07:16,000
 No logos. This is Thai medicine.

100
00:07:16,000 --> 00:07:21,000
 Why do I have these in my pocket?

101
00:07:21,000 --> 00:07:26,800
 Well, before I speak, I often put these in my mouth and use

102
00:07:26,800 --> 00:07:29,000
 them like lozenges.

103
00:07:29,000 --> 00:07:35,500
 They make it useful for getting rid of phlegm and clearing

104
00:07:35,500 --> 00:07:37,000
 up the voice.

105
00:07:37,000 --> 00:07:43,530
 One thing that's interesting about these is I really don't

106
00:07:43,530 --> 00:07:46,000
 take Western medicine.

107
00:07:46,000 --> 00:07:48,000
 That's sort of a choice of my own.

108
00:07:48,000 --> 00:07:52,000
 It's certainly not against the rules to take medicine.

109
00:07:52,000 --> 00:08:00,900
 I don't know what that is. I guess it has to do with my

110
00:08:00,900 --> 00:08:05,000
 idea that sickness is something not to be afraid of.

111
00:08:05,000 --> 00:08:11,190
 I've got a medicine. I've got a big chest full of medicine

112
00:08:11,190 --> 00:08:13,000
 that people have given me.

113
00:08:13,000 --> 00:08:17,000
 It's actually kind of funny.

114
00:08:17,000 --> 00:08:22,390
 When I went to India and got very, very sick twice, both

115
00:08:22,390 --> 00:08:26,000
 times people gave me just handfuls of medicine.

116
00:08:26,000 --> 00:08:28,520
 Every time I get sick, people do this. They give me handful

117
00:08:28,520 --> 00:08:29,000
s of medicine.

118
00:08:29,000 --> 00:08:34,000
 I just put it in my chest.

119
00:08:34,000 --> 00:08:39,000
 I take some because they often give me herbal medicine.

120
00:08:39,000 --> 00:08:48,000
 I take some of that and 90% of it goes into the trunk.

121
00:08:48,000 --> 00:08:51,000
 Just another odd fact about my life.

122
00:08:51,000 --> 00:08:55,330
 Finally, in my last pocket, this is my chest pocket. This

123
00:08:55,330 --> 00:09:01,000
 is a Buddha image.

124
00:09:01,000 --> 00:09:15,510
 This Buddha was given to me when I first did my first rec

125
00:09:15,510 --> 00:09:18,000
itation of the Patimoka, which is the monk's rules.

126
00:09:18,000 --> 00:09:22,830
 The monks have 227 rules in my tradition that they have to

127
00:09:22,830 --> 00:09:23,000
 follow.

128
00:09:23,000 --> 00:09:27,000
 Every two weeks we will chant them together.

129
00:09:27,000 --> 00:09:31,650
 There is a monk who gave this to me and it has great

130
00:09:31,650 --> 00:09:33,000
 meaning to me.

131
00:09:33,000 --> 00:09:37,000
 When I chant the rules, I like to carry it with me.

132
00:09:37,000 --> 00:09:40,530
 The day after tomorrow would be a day when we chant the

133
00:09:40,530 --> 00:09:44,000
 rules and I should be practicing.

134
00:09:44,000 --> 00:09:47,000
 I'll start now.

135
00:09:47,000 --> 00:09:51,080
 It's been great. It's been a fun day. I'm glad that I had a

136
00:09:51,080 --> 00:09:54,000
 chance to get involved with this.

137
00:09:54,000 --> 00:09:59,040
 I hope that some of this is included just to show where a

138
00:09:59,040 --> 00:10:05,000
 part of the world, monks, are an important part of reality.

