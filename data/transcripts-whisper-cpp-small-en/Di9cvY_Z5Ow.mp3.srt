1
00:00:00,000 --> 00:00:04,070
 So just in regards to the question on non-self and reincarn

2
00:00:04,070 --> 00:00:10,050
ation, there's a good book that is of interest in regards to

3
00:00:10,050 --> 00:00:11,000
 this issue of non-self.

4
00:00:11,000 --> 00:00:14,960
 It's certainly not for a seven-year-old, but the Mahasi Say

5
00:00:14,960 --> 00:00:19,000
adaw's book, "Viscours on Non-Self".

6
00:00:19,000 --> 00:00:24,170
 And in there, he actually expands on what Panyan made me

7
00:00:24,170 --> 00:00:29,450
 think of it because he says there's at least three types of

8
00:00:29,450 --> 00:00:30,000
 self.

9
00:00:30,000 --> 00:00:33,690
 I think there may be more, but I can remember three of them

10
00:00:33,690 --> 00:00:34,000
.

11
00:00:34,000 --> 00:00:40,330
 One is the self that, as you say, that controls, the

12
00:00:40,330 --> 00:00:44,000
 controlling self, the Lord's self.

13
00:00:44,000 --> 00:00:50,040
 This is Sami-attva, and then there is the Nitya-attva, I

14
00:00:50,040 --> 00:00:56,000
 don't know, the self that is permanent.

15
00:00:56,000 --> 00:00:58,380
 So some people postulate a self that controls, some people

16
00:00:58,380 --> 00:01:00,000
 postulate a self that is permanent,

17
00:01:00,000 --> 00:01:03,720
 some people postulate the self that experiences as the one

18
00:01:03,720 --> 00:01:08,000
 who experiences, and I think there are other examples.

19
00:01:08,000 --> 00:01:12,760
 But it's definitely a book worth reading and a little bit

20
00:01:12,760 --> 00:01:17,330
 difficult and repetitive to go through because he's going

21
00:01:17,330 --> 00:01:18,000
 through each aspect.

22
00:01:18,000 --> 00:01:21,000
 But it's very much in line with the meditation practice.

23
00:01:21,000 --> 00:01:23,770
 And now I remember what I wanted to say about reincarnation

24
00:01:23,770 --> 00:01:24,000
.

25
00:01:24,000 --> 00:01:28,530
 Albert Einstein was once asked about the theory of

26
00:01:28,530 --> 00:01:30,000
 relativity.

27
00:01:30,000 --> 00:01:34,450
 He became very famous for the theory of relativity, and

28
00:01:34,450 --> 00:01:39,000
 apparently he was asked by an interviewer, by a reporter,

29
00:01:39,000 --> 00:01:44,000
 to explain relativity in a few sentences.

30
00:01:44,000 --> 00:01:49,900
 And he looked at the guy and he said, you know, even if you

31
00:01:49,900 --> 00:01:54,000
 had a solid grounding in theoretical physics,

32
00:01:54,000 --> 00:01:59,710
 it would take me a couple of days to explain the theory of

33
00:01:59,710 --> 00:02:02,000
 relativity to you.

34
00:02:02,000 --> 00:02:08,510
 And this is the profundity of his realization, that it's

35
00:02:08,510 --> 00:02:11,000
 not something that an ordinary person can understand.

36
00:02:11,000 --> 00:02:18,640
 We're so deeply grounded in delusions, really, from a

37
00:02:18,640 --> 00:02:28,970
 physics point of view, in our biological and genetic predis

38
00:02:28,970 --> 00:02:30,000
position,

39
00:02:30,000 --> 00:02:35,010
 that we can't understand, we're not programmed to

40
00:02:35,010 --> 00:02:37,000
 understand reality.

41
00:02:37,000 --> 00:02:41,490
 So we could give a similar answer. A seven-year-old, it

42
00:02:41,490 --> 00:02:44,010
 would be very rare to find a seven-year-old who could have

43
00:02:44,010 --> 00:02:47,000
 any idea about reincarnation.

44
00:02:47,000 --> 00:02:50,150
 So it's not something that you asked. The question was, can

45
00:02:50,150 --> 00:02:52,660
 you please explain reincarnation like you're a seven-year-

46
00:02:52,660 --> 00:02:53,000
old?

47
00:02:53,000 --> 00:02:56,910
 If you're a seven-year-old, there's not much we could do to

48
00:02:56,910 --> 00:02:59,000
 explain the workings of reality.

49
00:02:59,000 --> 00:03:03,830
 If you mean to give the simplest explanation of it possible

50
00:03:03,830 --> 00:03:05,000
, I can do that.

51
00:03:05,000 --> 00:03:08,880
 I'm not sure that it's going to help you, and I think only

52
00:03:08,880 --> 00:03:12,410
 intensive practice will help you to understand what I'm

53
00:03:12,410 --> 00:03:14,000
 even talking about.

54
00:03:14,000 --> 00:03:18,660
 But it's that reality is what you're experiencing right now

55
00:03:18,660 --> 00:03:19,000
.

56
00:03:19,000 --> 00:03:21,200
 Reality is you're seeing, you're hearing, you're smelling,

57
00:03:21,200 --> 00:03:23,000
 you're tasting, you're feeling and thinking.

58
00:03:23,000 --> 00:03:27,000
 So ask yourself whether that's going to cease when you die.

59
00:03:27,000 --> 00:03:32,140
 And you should be able to see that it's an uncertainty. You

60
00:03:32,140 --> 00:03:33,000
 don't really know.

61
00:03:33,000 --> 00:03:38,410
 The best way to go about asking that question and investing

62
00:03:38,410 --> 00:03:42,070
 in that question is first to give up all of your predisp

63
00:03:42,070 --> 00:03:43,000
osed knowledge,

64
00:03:43,000 --> 00:03:46,080
 all of your ideas about, you know, I saw that person die

65
00:03:46,080 --> 00:03:48,000
 and therefore death is the end.

66
00:03:48,000 --> 00:03:50,000
 I saw that they're not thinking anymore. Give that all up.

67
00:03:50,000 --> 00:03:54,350
 Give up all of your, all of the scientific observations

68
00:03:54,350 --> 00:03:58,290
 that people have had and all of the experiments and so on

69
00:03:58,290 --> 00:03:59,000
 that have been done

70
00:03:59,000 --> 00:04:04,100
 that actually have, you know, evidence on both sides, you

71
00:04:04,100 --> 00:04:07,520
 know, all this investigation into past life memories and so

72
00:04:07,520 --> 00:04:08,000
 on.

73
00:04:08,000 --> 00:04:11,000
 Could also be used on the other side. But give that all up.

74
00:04:11,000 --> 00:04:14,110
 Give up even the knowledge that people do die and just ask

75
00:04:14,110 --> 00:04:17,000
 yourself or investigate this idea of death,

76
00:04:17,000 --> 00:04:20,000
 whether it's possible that this could happen in the future.

77
00:04:20,000 --> 00:04:23,200
 You'll see that actually there's no aspect of experience

78
00:04:23,200 --> 00:04:25,000
 that could admit of such a thing.

79
00:04:25,000 --> 00:04:28,180
 And so the strange thing would be that if it did cease, and

80
00:04:28,180 --> 00:04:30,000
 that was the profundity of what the Buddha found,

81
00:04:30,000 --> 00:04:33,760
 is that he found a way by which this experience ceases and

82
00:04:33,760 --> 00:04:37,000
 it's not death. It's nirvana.

83
00:04:37,000 --> 00:04:43,090
 But, you know, the point was already made that we don't

84
00:04:43,090 --> 00:04:45,000
 believe in reincarnation.

85
00:04:45,000 --> 00:04:48,490
 We simply don't believe in death. The experience continues

86
00:04:48,490 --> 00:04:51,000
 on as long as there is craving in the mind.

87
00:04:51,000 --> 00:04:54,180
 We've talked about that. I did a video on this. You can

88
00:04:54,180 --> 00:04:55,000
 look at it. "Nature of Reality."

89
00:04:55,000 --> 00:04:58,440
 I think we've talked about this on the Ask.SiriMongolah

90
00:04:58,440 --> 00:04:59,000
 forum.

91
00:04:59,000 --> 00:05:03,760
 So there is, the information is out there from our point of

92
00:05:03,760 --> 00:05:06,000
 view, my point of view.

