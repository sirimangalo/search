1
00:00:00,000 --> 00:00:02,000
 Wisdom from a Pleasant Glow

2
00:00:02,000 --> 00:00:05,560
 After watching the rising and falling of the abdomen for a

3
00:00:05,560 --> 00:00:08,640
 while, there is nothing but a pleasant glow. Does wisdom

4
00:00:08,640 --> 00:00:09,120
 arise spontaneously?

5
00:00:09,120 --> 00:00:11,720
 Or do I need to do more?

6
00:00:11,720 --> 00:00:16,200
 That pleasant glow can become a problem for you because you

7
00:00:16,200 --> 00:00:20,540
 find it pleasant. The experience of sensations as pleasant

8
00:00:20,540 --> 00:00:21,280
 is one of the most

9
00:00:21,280 --> 00:00:23,280
 common hindrances.

10
00:00:23,360 --> 00:00:26,350
 Even meditators practicing for years might feel like they

11
00:00:26,350 --> 00:00:28,960
 are not progressing because they get stuck on calm or

12
00:00:28,960 --> 00:00:30,320
 pleasant experiences.

13
00:00:30,320 --> 00:00:36,040
 The Pleasant Glow could be "Piti" which is Rapture, "Sukha"

14
00:00:36,040 --> 00:00:40,000
 which is Happiness, or "Pasadi" which is Tranquility.

15
00:00:40,000 --> 00:00:44,650
 Whatever you classify it as, it is an experience that comes

16
00:00:44,650 --> 00:00:48,280
 as a byproduct of meditation practice. It is impermanent,

17
00:00:48,280 --> 00:00:51,580
 it is unsatisfying in the sense that it cannot last forever

18
00:00:51,580 --> 00:00:54,400
, and it is uncontrollable in the sense that you cannot turn

19
00:00:54,400 --> 00:00:54,960
 it on or off.

20
00:00:54,960 --> 00:00:58,920
 When you sit down and meditate, it comes by itself and it

21
00:00:58,920 --> 00:01:02,170
 is not yours to control. To confirm these things, say to

22
00:01:02,170 --> 00:01:02,520
 yourself,

23
00:01:02,520 --> 00:01:07,180
 "Happy, happy, feeling, feeling, liking, liking," or

24
00:01:07,180 --> 00:01:09,080
 however it presents itself to you.

25
00:01:09,080 --> 00:01:12,560
 If you are persistent in acknowledging both the happiness

26
00:01:12,560 --> 00:01:14,840
 and the liking of it, you will see that just like

27
00:01:14,840 --> 00:01:16,000
 everything else that arises,

28
00:01:16,160 --> 00:01:19,640
 it is impermanent, unsatisfying, and uncontrollable.

29
00:01:19,640 --> 00:01:23,320
 When you do not maintain mindfulness in this way, you are

30
00:01:23,320 --> 00:01:24,400
 likely to cling to it.

31
00:01:24,400 --> 00:01:27,710
 Wisdom will not arise unless you are objective and clear of

32
00:01:27,710 --> 00:01:30,720
 mind, which is not the case if you are enjoying pleasant

33
00:01:30,720 --> 00:01:31,320
 feelings.

34
00:01:31,320 --> 00:01:34,480
 There is nothing wrong with such feelings. It can actually

35
00:01:34,480 --> 00:01:35,360
 be quite useful

36
00:01:35,360 --> 00:01:39,320
 because they provide you with energy and tranquility in the

37
00:01:39,320 --> 00:01:39,680
 mind.

38
00:01:39,680 --> 00:01:43,190
 But as soon as you find yourself enjoying them, even subtly

39
00:01:43,190 --> 00:01:44,860
, then you are clinging to it.

40
00:01:45,040 --> 00:01:46,560
 This can be quite subtle.

41
00:01:46,560 --> 00:01:50,050
 It can be obscured by delusions about it like the idea that

42
00:01:50,050 --> 00:01:52,560
 the pleasant feeling is me or mine.

43
00:01:52,560 --> 00:01:56,400
 By acknowledging the sensation, feeling, feeling, happy,

44
00:01:56,400 --> 00:01:59,440
 happy, liking, liking, or however it may appear to you,

45
00:01:59,440 --> 00:02:02,570
 this delusion ceases and you will gain an objective

46
00:02:02,570 --> 00:02:06,200
 perspective on the arising and passing of experiences.

47
00:02:06,200 --> 00:02:10,060
 You will come to see through experience that pleasant

48
00:02:10,060 --> 00:02:12,800
 feelings are not actually something that you can depend on.

49
00:02:13,160 --> 00:02:16,600
 They are not the path or goal of meditation.

50
00:02:16,600 --> 00:02:19,840
 Seeing that is wisdom, which is the path.

51
00:02:19,840 --> 00:02:22,970
 Such understanding leads you closer to freedom from

52
00:02:22,970 --> 00:02:23,880
 suffering.

53
00:02:23,880 --> 00:02:27,360
 Such wisdom cannot be created. It is not something you can

54
00:02:27,360 --> 00:02:30,300
 actively cultivate by thinking, reflecting, or asking

55
00:02:30,300 --> 00:02:31,880
 questions about reality.

56
00:02:31,880 --> 00:02:35,770
 It is something that will arise by itself, but it will only

57
00:02:35,770 --> 00:02:38,560
 arise if you are objective and clear in your mind.

58
00:02:38,960 --> 00:02:42,670
 So be careful about pleasant sensations as they are not the

59
00:02:42,670 --> 00:02:45,480
 path and like many other positive experiences,

60
00:02:45,480 --> 00:02:48,810
 will only get in the way of your practice if you cling to

61
00:02:48,810 --> 00:02:49,360
 them.

62
00:02:49,360 --> 00:02:51,360
 [

