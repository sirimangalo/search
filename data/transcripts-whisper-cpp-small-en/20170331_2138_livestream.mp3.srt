1
00:00:00,000 --> 00:00:04,680
 So it's not about grades.

2
00:00:04,680 --> 00:00:07,200
 Another professor apparently is very much against exams.

3
00:00:07,200 --> 00:00:08,640
 We didn't have an exam in her class

4
00:00:08,640 --> 00:00:12,320
 because she doesn't think them useful.

5
00:00:12,320 --> 00:00:13,480
 OK, nice to see you, Delany.

6
00:00:13,480 --> 00:00:20,360
 Thanks for coming out.

7
00:00:20,360 --> 00:00:42,680
 [AUDIO OUT]

8
00:00:42,680 --> 00:00:45,200
 I think if a professor were to-- it

9
00:00:45,200 --> 00:00:47,960
 would be fairly easy and productive and probably

10
00:00:47,960 --> 00:00:50,280
 a really good environment if there were a professor

11
00:00:50,280 --> 00:00:52,840
 or when there is a professor who's actually

12
00:00:52,840 --> 00:00:55,080
 meditating and mindful.

13
00:00:55,080 --> 00:00:58,800
 And I guess those are the best professors, the ones who

14
00:00:58,800 --> 00:01:02,960
 are actually spiritual in the sense--

15
00:01:02,960 --> 00:01:06,280
 in a general sense, not having an ideology that they cling

16
00:01:06,280 --> 00:01:06,520
 to

17
00:01:06,520 --> 00:01:11,680
 but that are actively working to better themselves

18
00:01:11,680 --> 00:01:15,800
 and actually actively concerned about the goodness

19
00:01:15,800 --> 00:01:19,240
 and what good they bring to their students.

