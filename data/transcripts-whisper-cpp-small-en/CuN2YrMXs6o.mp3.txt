 Okay, welcome back to our study of the Dhammapada.
 Today we are looking at verses 188 to 192.
 Let's read as follows.
 Bhahumwe Saranangyanti Pabbatani Vananija Arahma Rukha Ch
aitianni Manusabhaya Tajita
 Nithankho Saranangke Man Nithang Saranamutam
 Nithang Saranamagam Sambadukha Pammuchati
 Yojabudhancha Dhammancha Sanghancha Saranangatau
 Chaitari Aryasachani Samapanaya Pasati
 Dukkhandukhas Mukpadang Dukhasachatatikamang
 Aaryancha Thangikamagang Dukupasamagaminang
 Nithankho Saranangke Mang Nithang Saranamutamang
 Nithang Saranamagam Sambadukha Pammuchati
 So it's a lot, five verses altogether.
 There's not a lot in there.
 It's a simple, straightforward, but quite powerful teaching
, I think.
 So this teaching is in regards to refuges.
 The translation is as follows.
 Bahunwe Sarananganti Many go, indeed, for refuge.
 Sarana.
 Babatani Vanani Chah To mountains and to forests.
 Arahma Rukha Chaitianni To ashrams, to trees, to jaitias.
 A jaitia is a shrine, I guess.
 Nowadays we use it to mean this statue or large marker in
 Buddhism, a jaitia or a pagoda.
 Munusa Vayatajita People go to these places for refuge when
 they are terrified by fear.
 Nithankho Saranangke Mang This is not a safe refuge.
 Nithang Saranamutamang This is not the highest refuge.
 Not an ultimate refuge.
 Nithang Saranamagamma Sambudukha Pumucchati Going to such a
 refuge, such a place for refuge,
 one doesn't free oneself completely from all suffering.
 Yojobudhancha Dhammancha Sanghancha Saranangkato But who
 goes for refuge to the Buddha, the
 Dhamma and the Sangha?
 Chaitari Aryasatjani Samma Panyayapasati Seize the Four
 Noble Truths with Right Wisdom,
 or rightly with wisdom.
 Dukkang Dukkasumupa Dang Dukkasachatikamang Suffering, the
 arising of suffering, the
 cessation of suffering, Aryangchaktangikamang and the Eight
fold Noble Path, Dukkupasamagaminang,
 the Eightfold Noble Path which leads to the tranquilizing
 of suffering, or the cessation
 of suffering.
 Nithang Kho Saranangkemang This indeed is the safe refuge.
 This is a refuge in the ultimate sense.
 Nithang Saranamagamma Sambudukha Pumucchati Having gone to
 such a refuge, one frees oneself
 from all suffering.
 These verses were taught in regards to aghidatta, an asc
etic.
 But his story goes that in the time before the Buddha, he
 was the minister or advisor
 to the king of the Kosala country.
 And when the king died, he became advisor to King Pasenadi
 Kosala, who was the king
 of Kosala in the time of the Buddha.
 Kosala was one of the kingdoms in that time.
 And the story goes that Pasenadi was very good to this agh
idatta, out of reverence for
 his father and reverence for him as his father's advisor.
 He took him as his own advisor, but held him up as an equal
 to himself.
 So normally when a king would sit higher than everyone else
, he would sit with aghidatta.
 He would have aghidatta sit equal to him and treat him with
 great respect as his advisor.
 Aghidatta thought to himself something that's a little bit
 interesting because even though
 Pasenadi was treating him very well, he thought to himself,
 "Well, kings are unpredictable
 and today he's treating me well, but who knows what the
 future will bring.
 Maybe one day he'll have a whim and he'll decide that he no
 longer has need for me."
 I think there's some underlying context here because Pasen
adi was not an enlightened being
 by any means.
 He had a lot of his own attachments and partialities and so
 on, and he appears to have been a little
 bit of a rough sort of character.
 So I imagine some of the things he saw Pasenadi doing,
 killing and beheading and going to
 war and he had agreed and so on.
 The whim, Pasenadi was a good person I think in some ways,
 but he wasn't a great person.
 And so aghidatta probably seeing this thought to himself,
 he realized and I think even deeper,
 on a deeper level, well, there's a teaching here and it's
 this realization that there's
 a lot of suffering involved and potential to living in lay
 life, living in any situation
 where you have to put up with the whims of others.
 When you have a boss, when you have a master, when you have
 even just fellows in life competition
 and relationships and society, all the many people we have
 to meet with in society are
 some good, some bad, but there's many challenges and
 potential for great suffering.
 And so he probably started to realize all of this, that
 this life as a minister was not
 all that was cracked up to be.
 There was still something missing and there was still this
 insecurity.
 He would have seen all of the suffering that comes to the
 people who commit bad deeds or
 people who were victims of others' bad deeds.
 He would have seen how the king sometimes made mistakes or
 followed his own partiality
 and did bad deeds.
 And he seemed to have started to realize that life in this
 sphere is not safe.
 There's always the potential for suffering, for someone to
 harm me or to manipulate me
 in various ways.
 So he decided that this was no longer the life for him and
 he asked permission to leave the
 kingdom and he ended up shaving off his hair and beard and
 putting on a robe or some such
 thing and not becoming a Buddhist monk but going off and
 becoming an ascetic.
 And they say he took a whole bunch of people with him and
 ended up being a fairly well
 established teacher, not as an advisor to the king but as a
 teacher to people who were
 also interested in leaving the household life.
 And the way he would instruct people based on this sort of
 wisdom that he had gained,
 which I think is perfectly valid and valuable, this wisdom
 that living in society, living
 amongst people is fraught with danger.
 You're always subject to their whims and proclivities and
 their defilements, your defilements and
 the conflicting of defilements, the conflicting of unwholes
ome mind states.
 If I want something and you want something, we fight over
 it.
 If I'm prone to anger and you're prone to anger, we will
 fight and so on.
 So based on this, he taught his students to leave society.
 He said true refuge is on the mountains.
 It's in the forest.
 It's under the trees and so on and so on.
 In these lonely places, leave behind people, leave behind
 society and you'll find refuge
 at the top of a mountain or deep in the jungle.
 And he taught his students one other thing that's sort of
 incidental to the story that
 he said, "Anytime when you're living alone, you give rise
 to some unwholesome action or
 speech or thought.
 Take a jar and go down to the river and scoop some sand
 into the jar and bring it and dump
 it right in this one place."
 And they designated a place where they would all come and
 bring this sand and they would
 have a pot full of sand for every time they did something
 wrong.
 It was kind of something, some cultural, some teaching
 method of his to help people to recognize
 and admit their failings and their faults, which I think is
 also quite wholesome.
 And eventually, as these people were off in the forest and
 I guess you didn't have anything
 much more profound than that to teach them, well,
 absolutely they were going to still
 give rise to unwholesomeness.
 And so this sand pile started to accumulate and it got
 bigger and bigger until eventually
 it became a shrine.
 It became a very special place and people would come and
 worship it.
 The story goes that there was a dragon living there, some
 kind of a snake king like in one
 of our previous stories, decided to live there and was
 living in this sand pile.
 And then comes along the Buddha.
 So this is the introduction to the story.
 This ascetic, teacher of ascetics was off living on his own
 preaching about the refuge of the
 solitary places.
 And so the Buddha sent Moggallana and he said to Moggallana
, "Go and teach this guy what's
 wrong with what he's saying."
 And Moggallana went and the story goes that he ended up,
 well, he asked for permission
 to stay with these guys.
 He was going to spend some time there and maybe question
 them about their views and
 tell them about his own views and so on.
 But they wouldn't let him stay.
 This Agidatta said, "There's no room for him."
 I don't know if he was afraid or just didn't like Buddhism
 or something, didn't like these
 ascetics, these disciples of the ascetic kottama.
 But they wouldn't let him stay and he said, "Well, what
 about that sand pile?
 Can they stay on that sand pile?"
 And he said, "Oh no, there's a great naga, a dragon living
 there."
 And he said, "Oh, that's fine.
 I'll go and stay with him."
 He said, "Oh, that dragon is very temperamental.
 He'll surely kill you."
 And Moggallana had magical powers.
 So in the morning anyway, that part of the story is really
 inconsequential to our teaching.
 This is the fantastical part that I fully am willing to
 accept that many people will
 not believe that part.
 So absolutely optional.
 You don't have to concern yourself with that.
 But the story goes that Moggallana went and subdued the
 dragon and then in the morning
 he was sitting and they came to the sand pile and he was
 thinking, "I wonder if he's still
 alive or if the dragon killed him?"
 But he was sitting on top of the sand pile and the snake
 extended its hood like a cobra
 over Moggallana.
 And then that day the Buddha came and Moggallana got down
 and the Buddha went up and sat on
 the sand pile and he talked to Agidatta.
 Agidatta came and talked to him.
 He talked to Agidatta and said, "What is it you teach?"
 And Agidatta told him, "I teach this.
 This is what I teach my students."
 So barring the whole dragon part, it's a simple story of a
 monk often, an ascetic often in
 the forest teaching his students to find true refuge in
 solitude.
 It's quite simple.
 The Buddha said that's not true solitude.
 That's not true refuge.
 It's not safe.
 You're telling these people they're going to be safe from
 defilements.
 They bring all their defilements with them.
 And so he taught these verses.
 So from the story I think one important lesson we can
 gather is what Agidatta got right and
 that is the nature of the household life and how powerful
 it is to leave behind the household
 life.
 You give up so much.
 You give up a lot of wealth and pleasure and ease and
 comfort.
 But you gain a great amount of safety and security.
 Not real and true safety and security.
 We'll get to why obviously.
 That should be fairly obvious why this guy had the wrong
 idea.
 But there is a certain amount of safety and security that
 comes because there's no thieves,
 there's no murderers, there's no temptations, there's no
 addictions.
 If you live off in the forest maybe these guys were living
 off of fruits and nuts and
 leaves, whatever they could gather in the forest.
 So they're living a very simple life.
 Probably they had simple robes.
 Some of them even more just tree bark.
 They had special...
 From certain trees you could get this sort of soft cloth
 from the tree bark.
 Like you just cut the tree bark off and wear it, something
 like that.
 Maybe they would go naked even.
 And these were all very simple ways of living.
 It was a way of answering this question of how you escape,
 how you find security when
 it seems like anywhere you go in society any path that you
 pick is fraught with danger
 and uncertainty, the falling prey and being a victim of the
 whims of other people and
 of society in general.
 So this is an important teaching.
 It's an important thing to realize that there are ways and
 there are alternatives and there
 is a way of living, a choice we can make to live our life
 more simply and there's a great
 security that comes from that.
 But the verse teaching is I suppose more core and more
 important.
 It's the difference between finding safety, security, peace
, happiness in physical things,
 in your physical location.
 People come to our meditation center sometimes thinking
 that they're going to be able to
 leave behind all of their problems.
 Quite often to be honest people will come and think that
 meditation should be quite
 peaceful and comfortable and pleasant.
 They should be able to leave behind all the suffering of
 life.
 People become monks for the same reason.
 I often get requests from people that give information,
 they want help with becoming
 a monk and I tend to dismiss it because too often we see
 people who are focused on becoming
 a monk have the wrong attitude, their inclination is to run
 away and they end up complaining
 and suffering and not being able to fit in or live or
 become comfortable as a monk because
 they're unable to face the suffering and the problems that
 exist inside and that's the
 point is you come here to our meditation center to face
 your problems, not to leave them behind.
 You become a monk to face life, to face your problems.
 So going to the mountains or to the forests or to a tree or
 anywhere and thinking that
 you're going to find safety there is ridiculous.
 In fact, in a deep sense it's the opposite of what's going
 to happen.
 A person who goes off into the forest without any guidance
 is going to have a large pile
 of sand if that's the thing.
 They're going to have done a lot of bad deeds of action,
 speech and thought.
 There's nothing to temper them.
 They might even go crazy because they have to face
 themselves when you go off into the
 forest, off into the mountain.
 The first part of what I said is true.
 You gain some security from the outside and what's great
 about it and why the Buddha recommended
 it certainly as a part of the practice is because it gives
 you the opportunity to face
 your demons.
 You can't face them in society.
 You're constantly bombarded by other people you're forced
 to engage with and to get involved
 with their problems and their situations.
 So facing your own demons is very difficult when you're
 living in society and that's
 the reason why one should leave.
 Not because one wants to run away but because one wants to
 do good work, hard work, the
 hardest work perhaps.
 And that's the work of purifying your own mind, fighting
 your own demons, fighting your
 demons, not running away from them.
 And so that's why the Buddha said these things are not a
 refuge.
 You can't take refuge in these things and hope to these
 places and hope to be saved.
 The true refuge is the Buddha, the Dhamma and the Sangha
 and particularly the Four Noble
 Truths as you notice that's what the Buddha talked about.
 He doesn't just mention the three refuges, the Buddha, the
 Dhamma and the Sangha.
 He specifically brings out the Four Noble Truths which I
 think is important and worth commenting
 on.
 But the second part is the teaching that the three refuges,
 the Buddha, the Dhamma and
 the Sangha are truly safe and ultimate refuge.
 The Buddha is a refuge in a physical sense on a superficial
 level because living with
 the Buddha is a great support for your practice.
 If any of us had the opportunity to be with the Buddha and
 were open to it and had the
 great capacity in our hearts to listen to the Buddha's
 teaching then it would be a great
 support.
 It would be a great refuge to have the honor and the
 opportunity to hear the Buddha's teaching
 and to follow his teaching directly.
 It would be a great refuge.
 That's not of course in a deeper sense.
 Taking refuge in the Buddha is a psychological thing.
 It's a mental thing.
 When you say to yourself, "I will follow the teachings of
 the Buddha when someone goes
 to the Buddha and accepts, puts themselves in the position
 of a student, lowers themselves
 and puts the Buddha up, you are my teacher and makes a
 promise or an intention to do
 what the Buddha tells them to do, to follow his teachings,
 to practice accordingly."
 That's what it means to take refuge in the Buddha.
 It doesn't mean you have to vow your life to the Buddha and
 convert to Buddhism.
 That's not what I'm saying.
 It's the sort of thing we do when a person comes to do a
 meditation course here in a
 traditional setting.
 Here we haven't started doing it but I've thought about
 making it optional.
 We would do a traditional opening ceremony where one would
 take the refuges and the precepts.
 More importantly, one would ask for the meditation practice
, "Please teach me," and would say,
 "I give myself over to the Buddha.
 For the duration of this course I give myself over.
 I'm no longer going to follow my own whims.
 I'm no longer going to be in charge.
 I'm not going to have any say.
 I'm going to listen, follow and practice whatever you tell
 me to do."
 Putting yourself in that position is a part of what it
 means to take refuge.
 You really wholeheartedly say, "I go to the Buddha and I'm
 putting myself under their
 care, under their protection."
 The Dhamma as the refuge, of course, is the most central
 part of the Buddha's teaching
 is that the practice itself.
 We practice the Dhamma in order to realize the Dhamma.
 This is the most central refuge.
 How is the Dhamma the refuge?
 Because this is what truly leads to safety.
 It's a safety in the path, in the practice, and it's a
 safety in the result.
 The safety in the practice is you are kept in a fortress.
 You are kept safe, safe from your own defilements, from
 your own unwholesome thoughts.
 When you give rise to anger and you're mindful it's able to
 cut off and prevent you from
 shouting or hurting other people with speech or action.
 It's even capable of preventing anger from arising or greed
 from arising or arrogance
 can see through all these things from arising. If you're
 skilled and capable of being mindful
 in the present moment and you're able to cut it off and
 when you have pain, for example,
 and you say to yourself, "Pain, pain."
 There's no reaction to it, no anger need to arise.
 When you see something you like or feel something pleasant,
 there's a feeling, feeling, or seeing,
 or whatever it might be, and you don't give rise to the un
wholesomeness.
 So the Dhamma, the practice of it is a great refuge.
 And what it leads to, the wisdom that it leads to, the
 Buddha said, "Sama pañjaya paśati,"
 when you see with wisdom, is the ultimate refuge, is the
 highest and most supreme refuge.
 It's the freedom from suffering.
 Because what happens as you start to see things clearly and
 are more objective is you start
 to let go, you start to see the difference between clinging
 to things which leads to
 suffering and objective, equanimous observation which keeps
 you free from suffering.
 And you see that more clearly and more clearly until
 finally your mind lets go, there's
 a realization, nothing's worth clinging to, and you let go.
 The Sangha as the refuge, as I think underappreciate it,
 the Sangha is an important refuge, it's
 an important one of these three.
 It's most important for us, but even in the time of the
 Buddha, the Buddha was constantly
 reminding his students about the importance of the Sangha.
 Because it's the Sangha that carries out and protects and
 supports the Buddha's teaching,
 supports the practice.
 It doesn't even have to be the Buddha's teaching, just the
 practice of goodness.
 Sangha means community.
 It's the community of people who have practiced rightly,
 who are able to help others practice,
 who are practicing together as a support, as a model, as an
 example for others, and
 those who provide advice and encouragement.
 And those who simply remember the Buddha's teaching and
 pass them on and protect them,
 those who keep the Buddha's teaching, writing them down and
 memorizing them, chanting them,
 and teaching them, and so on.
 All of this is the Sangha and it's a great refuge, it's a
 refuge because of the teachings
 that we get, just the information alone, all of the books
 that we have on the Buddha's
 teaching, it's all because of the Sangha.
 And so the Sangha is providing this to us, their support,
 their protection, keeping us
 in the realm of right view, by giving us teachings, that
 teacher right view, how to see things
 in the right way.
 It's a great support.
 I think more deeply there's also the idea of the teacher.
 So I don't call myself a teacher or anything, but a person
 who gives teaching is someone
 we should take refuge in.
 So during the meditation course we put ourselves in their
 protection as well.
 So we say, "I give myself over to you, I will follow the
 things you say," that sort of thing.
 And by doing that, that's a great protection for us.
 It gives a certain sincerity and wholeheartedness and
 freedom from fear or doubt or worry.
 Just having someone, the great difference between
 practicing on your own at home, trying
 to decide for yourself what is the correct way to practice.
 And having someone who will guide you, lead you, it's night
 and day really.
 Anyone who's done these courses, sometimes it seems magical
 how easy it is when you have
 a teacher, how much easier it is, how much more powerful
 and how much further you can
 go when you have someone guiding you.
 So taking that as our refuge, just coming to do a
 meditation course or finding a teacher
 somewhere is a great refuge.
 If they're teaching, if they're actually the Sangha and
 they're teaching the Buddha's teaching.
 And the last part of the verse, or the last point in the
 set of verses is the Four Noble Truths.
 Now it's said that the Buddha tried always to include the
 Four Noble Truths in his teaching
 because it's the core message.
 It's the essence of what the Buddha taught.
 And if you miss that, you haven't actually taught the
 Buddha's teaching.
 If you teach something off to the side, that's not touching
 upon at least in some way the
 Four Noble Truths.
 You haven't actually gotten to the point.
 You've missed that which will lead people to freedom from
 suffering because that's what
 the Four Noble Truths are.
 But in another sense, the Four Noble Truths are a refuge.
 The Buddha doesn't say that exactly, but as the core of the
 Dhamma, it's clear that they
 are a refuge.
 Suffering is a refuge.
 And as a teaching is a refuge, suffering is not a refuge.
 But the understanding of suffering is the greatest refuge.
 When you see what it is that causes you suffering, when you
 see what it is that is going to hurt
 you if you cling to it, then of course that's the greatest
 refuge because then you don't
 cling to it.
 So this is the idea in Buddhism of why we suffer is because
 we cling to things.
 The second Noble Truth, the cause of suffering is craving
 or clinging.
 And so when you start to see that these things that we
 cling to are suffering or stressful,
 when you see that are trying to control things, when you
 see that are stressing out over things
 or getting angry about things or clinging to things or
 needing things, when you see
 that that is suffering, you let go of it.
 You stop.
 You no longer engage in those activities.
 Why?
 Because they cause you suffering.
 So the first Noble Truth, just understanding suffering is a
 great refuge.
 The second Noble Truth is a refuge, but when it's abandoned
, abandoning, craving of course
 means no suffering.
 It's what comes from seeing the first Noble Truth clearly.
 The third Noble Truth is Nibbana, freedom from suffering.
 So it is the highest refuge.
 And the fourth Noble Truth is the path.
 So again, getting back to practice as a refuge.
 Just having a path of practice is a huge safety.
 Again as I said, it's like having a fortress.
 So the path which leads to cessation from suffering is the
 ultimate practical refuge
 because it not only leads to Nibbana, but because it
 protects us.
 It's a way of life.
 It's a way of being that is simple, pure and perfect, if
 you can put it into practice.
 So that's the Dhammapada for today.
 Really a reminder that the greatest refuge is our practice
 and our commitment to, I guess,
 the organization in a sense.
 The organization or whatever word you want to use, religion
 maybe.
 That includes the Buddha, the Dhamma and the Sangha.
 But it includes them as a program by which we practice to
 free ourselves from suffering.
 There's no great refuge that comes from being here or being
 there or having this or having
 that.
 Even just being at a meditation center or becoming a monk
 is not a true refuge.
 Refuge has to be deeper.
 True refuge, true safety.
 Another place the Buddha said it's the Four Foundations of
 Mindfulness.
 So in the Eightfold Noble Path, the path that leads to
 freedom from suffering, we have the
 Four Foundations of Mindfulness sort of as the practical
 aspect there.
 What you engage in, in a practical sense when you cultivate
 meditation, when you're doing
 walking or sitting, you're cultivating the Four Foundations
 of Mindfulness and all the
 other parts of the Eightfold Noble Path.
 But it means you make yourself a refuge.
 The Buddha said, "Ata hi ata nona to."
 One should be one's own protector or refuge.
 And the way you do that is through your practice.
 So another good teaching and very core teaching in the
 Buddha's Dhamma.
 So that's the Dhamma Pada for tonight.
 Thank you all for listening.
