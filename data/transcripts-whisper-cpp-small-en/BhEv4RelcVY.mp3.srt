1
00:00:00,000 --> 00:00:02,000
 Let's go. I trust you.

2
00:00:02,000 --> 00:00:05,350
 Okay. Monte, I hope you are well. My biggest fear is to

3
00:00:05,350 --> 00:00:06,080
 lose my mother.

4
00:00:06,080 --> 00:00:10,360
 Lose my mother to death. I know we have to accept it. She

5
00:00:10,360 --> 00:00:11,040
 is not mine.

6
00:00:11,040 --> 00:00:13,960
 But the fear is still haunting me even in my dreams.

7
00:00:13,960 --> 00:00:17,300
 Please help me understand with your words. Thank you so

8
00:00:17,300 --> 00:00:17,640
 much.

9
00:00:17,640 --> 00:00:30,640
 [silence]

10
00:00:30,640 --> 00:00:33,240
 Well, to love your mother is a good thing.

11
00:00:33,240 --> 00:00:36,520
 There's good there.

12
00:00:36,520 --> 00:00:39,510
 I think you kind of have to separate that out. And this is

13
00:00:39,510 --> 00:00:42,340
 what people who have strong sense of family aren't able to

14
00:00:42,340 --> 00:00:42,520
 do.

15
00:00:42,520 --> 00:00:45,560
 And so no matter how they

16
00:00:47,560 --> 00:00:49,560
 claim that they understand

17
00:00:49,560 --> 00:00:53,800
 concepts of non-self and detachment,

18
00:00:53,800 --> 00:00:57,040
 they really do

19
00:00:57,040 --> 00:01:02,310
 feel that it's good to be attached to their parents. Deep

20
00:01:02,310 --> 00:01:02,760
 down,

21
00:01:02,760 --> 00:01:05,640
 it's very much a part of

22
00:01:05,640 --> 00:01:08,400
 their idea of themselves to

23
00:01:08,400 --> 00:01:11,150
 be afraid of losing your mother. So you're not afraid to

24
00:01:11,150 --> 00:01:14,320
 lose your mother. Maybe it's a good way to approach this.

25
00:01:14,800 --> 00:01:18,240
 You're not afraid of losing your mother. This fear arises

26
00:01:18,240 --> 00:01:20,320
 from time to time.

27
00:01:20,320 --> 00:01:23,800
 Probably

28
00:01:23,800 --> 00:01:26,360
 with some frequency.

29
00:01:26,360 --> 00:01:32,420
 Fear doesn't haunt you. Fear arises. And thinking of it in

30
00:01:32,420 --> 00:01:33,920
 these terms will help you to

31
00:01:33,920 --> 00:01:37,870
 align your mind in order to deal with it. It will also help

32
00:01:37,870 --> 00:01:39,960
 you to see clearly what's going on.

33
00:01:39,960 --> 00:01:43,240
 It's not just the fear. There's also a love of your mother.

34
00:01:44,360 --> 00:01:46,360
 Gratitude towards your mother.

35
00:01:46,360 --> 00:01:49,160
 Attachment to your mother.

36
00:01:49,160 --> 00:01:53,360
 All of those things are also present. And those things

37
00:01:53,360 --> 00:01:54,880
 probably fuel the fear.

38
00:01:54,880 --> 00:01:57,880
 Most definitely fuel the fear.

39
00:01:57,880 --> 00:02:00,320
 The...

40
00:02:00,320 --> 00:02:02,360
 And that the...

41
00:02:02,360 --> 00:02:05,120
 Yes, so all of those are the

42
00:02:05,120 --> 00:02:09,530
 constituents. And they're all being held together by this

43
00:02:09,530 --> 00:02:12,440
 view of self. You see how these defilements work.

44
00:02:12,440 --> 00:02:15,360
 They play different roles. View of self holds it together.

45
00:02:15,360 --> 00:02:17,160
 So you say this is me

46
00:02:17,160 --> 00:02:19,200
 afraid

47
00:02:19,200 --> 00:02:22,270
 to lose my mother. But actually once you let go of that and

48
00:02:22,270 --> 00:02:25,840
 you say okay fear arose. You say oh, that's only this part.

49
00:02:25,840 --> 00:02:29,280
 There's also the attachment. There's also the love. And you

50
00:02:29,280 --> 00:02:31,800
're able to separate them out. And in the end

51
00:02:31,800 --> 00:02:34,800
 you

52
00:02:34,800 --> 00:02:37,330
 are able to free yourself from the bad ones because you see

53
00:02:37,330 --> 00:02:39,600
 they're the real problem. You see clearly that's

54
00:02:40,240 --> 00:02:44,060
 not helping me. Okay, that I know is bad. And so you remove

55
00:02:44,060 --> 00:02:45,120
 that from the equation.

56
00:02:45,120 --> 00:02:47,790
 And when we're left, what we're left with is only the good

57
00:02:47,790 --> 00:02:51,160
 stuff. So there's love, there's gratitude, there's

58
00:02:51,160 --> 00:02:54,760
 compassion and joy and

59
00:02:54,760 --> 00:02:58,160
 equanimity.

60
00:02:58,160 --> 00:03:01,600
 There's wisdom, there's insight. And your relationship with

61
00:03:01,600 --> 00:03:03,280
 your mother becomes wholesome.

62
00:03:03,280 --> 00:03:07,360
 And there's no sense of fear or sadness

63
00:03:07,960 --> 00:03:10,560
 in regards to loss of your mother.

64
00:03:10,560 --> 00:03:13,800
 Okay, so remove the idea of self.

65
00:03:13,800 --> 00:03:17,280
 Get just the idea of this has arisen and you'll be able to

66
00:03:17,280 --> 00:03:20,140
 break it up into its parts. See the many parts and as a

67
00:03:20,140 --> 00:03:20,920
 result deal with.

68
00:03:20,920 --> 00:03:24,640
 That's insight meditation in a nutshell.

69
00:03:24,640 --> 00:03:26,640
 [ Silence ]

