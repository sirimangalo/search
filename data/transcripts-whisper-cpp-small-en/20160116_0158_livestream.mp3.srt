1
00:00:00,000 --> 00:00:07,000
 Good evening everyone.

2
00:00:07,000 --> 00:00:12,000
 We're broadcasting live.

3
00:00:12,000 --> 00:00:26,000
 January, sometime January 15th, 2016.

4
00:00:26,000 --> 00:00:32,000
 So it's been an eventful week here.

5
00:00:32,000 --> 00:00:39,670
 I spent Tuesday and Thursday teaching more or less one-on-

6
00:00:39,670 --> 00:00:40,000
one,

7
00:00:40,000 --> 00:00:43,000
 five-minute meditation lessons.

8
00:00:43,000 --> 00:00:45,000
 Sometimes two people.

9
00:00:45,000 --> 00:00:50,000
 Sometimes three, sometimes four.

10
00:00:50,000 --> 00:00:58,000
 It was just amazing, the diversity.

11
00:00:58,000 --> 00:01:09,000
 It was such a beneficial, such a fruitful activity.

12
00:01:09,000 --> 00:01:19,000
 I taught two Sikh men, one point, two Muslim women.

13
00:01:19,000 --> 00:01:26,000
 I taught a woman who had cerebral palsy.

14
00:01:26,000 --> 00:01:30,000
 She was in a wheelchair.

15
00:01:30,000 --> 00:01:42,000
 I taught one woman who sat down and just started crying.

16
00:01:42,000 --> 00:01:52,470
 I taught mostly 20-some-year-old kids, but a couple of

17
00:01:52,470 --> 00:01:55,000
 older people.

18
00:01:55,000 --> 00:02:00,960
 I taught one woman who sat down and told me she had psychic

19
00:02:00,960 --> 00:02:02,000
 powers.

20
00:02:02,000 --> 00:02:13,060
 She got visions that were aimed at. Spirits came to her and

21
00:02:13,060 --> 00:02:14,000
 showed her visions.

22
00:02:14,000 --> 00:02:17,000
 She was impressed by the practice.

23
00:02:17,000 --> 00:02:22,000
 Surprised. Most people were surprised by it, I think.

24
00:02:22,000 --> 00:02:25,000
 That's not what people expect from meditation.

25
00:02:25,000 --> 00:02:40,000
 But pleasantly surprised. Some people really got it.

26
00:02:40,000 --> 00:02:43,000
 We're still working on exactly what I say,

27
00:02:43,000 --> 00:02:46,000
 but it's more or less just telling people that meditation

28
00:02:46,000 --> 00:02:50,000
 is based on being objective.

29
00:02:50,000 --> 00:02:56,170
 The key principle is that it's not our experiences that

30
00:02:56,170 --> 00:02:57,000
 cause our suffering,

31
00:02:57,000 --> 00:03:00,000
 it's our reactions to them.

32
00:03:00,000 --> 00:03:03,460
 If we can just experience things as they are, we won't

33
00:03:03,460 --> 00:03:05,000
 suffer, for example.

34
00:03:05,000 --> 00:03:08,000
 Then I start talking about the things.

35
00:03:08,000 --> 00:03:12,440
 If someone's shouting at you, it's not the sound that

36
00:03:12,440 --> 00:03:14,000
 causes you suffering,

37
00:03:14,000 --> 00:03:17,000
 it's the reaction you have to their words.

38
00:03:17,000 --> 00:03:21,000
 You get angry and upset, and that cripples you.

39
00:03:21,000 --> 00:03:32,120
 It locks you into this negative attitude that then leads to

40
00:03:32,120 --> 00:03:38,000
 argument, conflict.

41
00:03:38,000 --> 00:03:40,860
 If you have thoughts, memories about the past, or worries

42
00:03:40,860 --> 00:03:42,000
 about the future,

43
00:03:42,000 --> 00:03:45,750
 things you have to do in the future, things that are coming

44
00:03:45,750 --> 00:03:47,000
 in the future,

45
00:03:47,000 --> 00:03:51,500
 the thoughts themselves aren't a problem, it's only when

46
00:03:51,500 --> 00:03:56,000
 you let them get to you.

47
00:03:56,000 --> 00:04:02,000
 But they become a problem.

48
00:04:02,000 --> 00:04:06,000
 Even physical pain, physical pain is just an experience.

49
00:04:06,000 --> 00:04:10,910
 If you could just experience it as pain, it too won't be a

50
00:04:10,910 --> 00:04:12,000
 problem.

51
00:04:12,000 --> 00:04:14,000
 It's just pain.

52
00:04:14,000 --> 00:04:17,000
 This is the theory behind the practice.

53
00:04:17,000 --> 00:04:19,000
 I tell them the theory.

54
00:04:19,000 --> 00:04:21,000
 It's separated into three parts.

55
00:04:21,000 --> 00:04:23,000
 There's the theory, then there's the technique,

56
00:04:23,000 --> 00:04:26,000
 and then I start explaining to them about what you do.

57
00:04:26,000 --> 00:04:30,000
 They say hearing, hearing, thinking, thinking, and pain.

58
00:04:30,000 --> 00:04:34,030
 And then the third part is explaining why we have to

59
00:04:34,030 --> 00:04:36,000
 practice it and practice meditation.

60
00:04:36,000 --> 00:04:41,260
 It's a means of training ourselves in this practice, of

61
00:04:41,260 --> 00:04:44,000
 reminding ourselves.

62
00:04:44,000 --> 00:04:47,000
 We want to be objective.

63
00:04:47,000 --> 00:04:50,120
 The technique by which we become objective is to remind

64
00:04:50,120 --> 00:04:51,000
 ourselves.

65
00:04:51,000 --> 00:04:54,070
 We want to see things as they are, so we remind ourselves

66
00:04:54,070 --> 00:04:55,000
 of what they are.

67
00:04:55,000 --> 00:04:58,000
 That's the word, and then I explain them that.

68
00:04:58,000 --> 00:05:02,220
 And then I say, "So meditation is an exercise or a training

69
00:05:02,220 --> 00:05:08,570
 practice to train you in this technique of reminding

70
00:05:08,570 --> 00:05:13,000
 yourself so that you can understand things as they are."

71
00:05:13,000 --> 00:05:17,610
 And so I say, "We start by picking an object that we're not

72
00:05:17,610 --> 00:05:21,000
 likely to react to anyway."

73
00:05:21,000 --> 00:05:24,000
 Why? Because that's the easiest.

74
00:05:24,000 --> 00:05:27,000
 It makes it easy for a beginner.

75
00:05:27,000 --> 00:05:30,350
 If it was something that we'd normally react to, then it's

76
00:05:30,350 --> 00:05:34,000
 very difficult for us to begin to cultivate objectivity.

77
00:05:34,000 --> 00:05:37,000
 So that's why we use the breath.

78
00:05:37,000 --> 00:05:39,540
 I explain to them about the stomach, and then I have them

79
00:05:39,540 --> 00:05:42,000
 close their eyes and put their hand on their stomach.

80
00:05:42,000 --> 00:05:44,000
 Most people thought that was a little bit goofy, I think.

81
00:05:44,000 --> 00:05:46,300
 Everyone started smiling when I told them to put their hand

82
00:05:46,300 --> 00:05:47,000
 on their stomach,

83
00:05:47,000 --> 00:05:50,360
 because we're in a hallway and people are walking back and

84
00:05:50,360 --> 00:05:51,000
 forth.

85
00:05:51,000 --> 00:05:57,400
 It was incredible publicity, just reminding people about

86
00:05:57,400 --> 00:06:01,000
 meditation, making people think about meditation.

87
00:06:01,000 --> 00:06:04,620
 Even today someone came up to me and asked me if I was

88
00:06:04,620 --> 00:06:06,000
 doing it again.

89
00:06:06,000 --> 00:06:10,000
 Many, many people now know who I am on campus.

90
00:06:10,000 --> 00:06:14,400
 They've been spending months wondering who I was, and now

91
00:06:14,400 --> 00:06:17,000
 many of them know who I am.

92
00:06:17,000 --> 00:06:21,530
 Quite interesting. A lot of publicity, people talking about

93
00:06:21,530 --> 00:06:22,000
 it.

94
00:06:22,000 --> 00:06:27,790
 Today we had our regularly scheduled Friday afternoon

95
00:06:27,790 --> 00:06:33,000
 meditation, and four of them showed up,

96
00:06:33,000 --> 00:06:35,690
 four people showed up who I'd given a five minute

97
00:06:35,690 --> 00:06:39,500
 meditation lesson to, and I just sat down and visited with

98
00:06:39,500 --> 00:06:40,000
 them.

99
00:06:40,000 --> 00:06:48,280
 So we had a group of six of us meditating today, which is a

100
00:06:48,280 --> 00:06:51,000
 record for Friday.

101
00:06:51,000 --> 00:06:54,440
 On Monday we'll do another meditation, just a half an hour

102
00:06:54,440 --> 00:06:56,000
 meditation together.

103
00:06:56,000 --> 00:06:59,230
 And then Wednesday I'm doing this one night to the

104
00:06:59,230 --> 00:07:06,000
 teachings, so we'll see if anyone comes to that.

105
00:07:06,000 --> 00:07:09,810
 And I'm still going to work on my TED Talk, maybe this

106
00:07:09,810 --> 00:07:16,000
 weekend. I'll put more effort into that.

107
00:07:16,000 --> 00:07:19,000
 And school. School is interesting.

108
00:07:19,000 --> 00:07:24,990
 I mean, first of all, school is great. This was the reason

109
00:07:24,990 --> 00:07:26,000
 for staying in school,

110
00:07:26,000 --> 00:07:29,750
 for going back to school, to be at the university and get

111
00:07:29,750 --> 00:07:32,000
 involved with the community.

112
00:07:32,000 --> 00:07:38,660
 So I wouldn't have been able to do such great work as I did

113
00:07:38,660 --> 00:07:43,000
 this past week without being at the university.

114
00:07:43,000 --> 00:07:47,000
 Without being a student, I think.

115
00:07:47,000 --> 00:07:53,360
 So that's reassuring. The studies are interesting. Peace

116
00:07:53,360 --> 00:07:55,000
 studies is really awesome.

117
00:07:55,000 --> 00:07:59,680
 There's so much. It's so much of studying the ideas that I

118
00:07:59,680 --> 00:08:02,000
'm interested in anyway.

119
00:08:02,000 --> 00:08:06,090
 What is Buddhism about? It's more peace studies than

120
00:08:06,090 --> 00:08:07,000
 religious studies.

121
00:08:07,000 --> 00:08:11,000
 It's more about peace than religion, of course.

122
00:08:11,000 --> 00:08:17,000
 But in religious studies I'm studying Eastern Buddhism.

123
00:08:17,000 --> 00:08:22,790
 Let me tell you, the Mahayana is messed up. Don't tell

124
00:08:22,790 --> 00:08:26,000
 anyone. Don't let go on the internet.

125
00:08:26,000 --> 00:08:30,230
 There's nothing you want to broadcast on the internet,

126
00:08:30,230 --> 00:08:31,000
 right?

127
00:08:31,000 --> 00:08:36,660
 Let me tell you, sitting in this Buddhism class, this is

128
00:08:36,660 --> 00:08:40,000
 probably one of the only practicing Buddhists.

129
00:08:40,000 --> 00:08:45,200
 And I'm just thinking, "Oh no, please don't think this is

130
00:08:45,200 --> 00:08:48,000
 represented in Buddhism."

131
00:08:48,000 --> 00:08:52,300
 You know, what they say, and this is the Lotus Sutra, which

132
00:08:52,300 --> 00:08:54,000
 is really one of the base texts.

133
00:08:54,000 --> 00:08:58,680
 What they say is that the Buddha was basically lying about

134
00:08:58,680 --> 00:09:01,000
 there being three paths.

135
00:09:01,000 --> 00:09:04,540
 The essence, the key that starts off the sutta, I haven't

136
00:09:04,540 --> 00:09:07,000
 of course read it at all, but we've just started,

137
00:09:07,000 --> 00:09:10,440
 is that the three paths of you can choose to become a

138
00:09:10,440 --> 00:09:13,550
 Buddha, or you can become a Pacheka Buddha, a private

139
00:09:13,550 --> 00:09:14,000
 Buddha,

140
00:09:14,000 --> 00:09:16,430
 or you can just become an Arahant following after the

141
00:09:16,430 --> 00:09:19,000
 Buddha. That was a lie.

142
00:09:19,000 --> 00:09:22,660
 It turns out that we all have to become Buddhas. There's

143
00:09:22,660 --> 00:09:24,000
 only one path.

144
00:09:24,000 --> 00:09:28,000
 I think it's the Buddha. Yeah, it is the Buddha path.

145
00:09:28,000 --> 00:09:31,420
 And so this is what the Mahayana is based on, that the

146
00:09:31,420 --> 00:09:34,000
 Buddha was, and the sutta is just so much different.

147
00:09:34,000 --> 00:09:38,390
 Yeah, Lotus Sutras, if you've read the Pali Sutras, sutta

148
00:09:38,390 --> 00:09:41,000
 is, there's no comparison.

149
00:09:41,000 --> 00:09:45,700
 Just the flavor of it is missing. To put these words in the

150
00:09:45,700 --> 00:09:50,000
 Buddha's mouth, not impressed.

151
00:09:50,000 --> 00:09:53,000
 Not impressed.

152
00:09:53,000 --> 00:09:59,080
 But it's worth studying because Mahayana Buddhism is

153
00:09:59,080 --> 00:10:03,000
 something that we have to deal with.

154
00:10:03,000 --> 00:10:08,290
 I'm not really impressed. And there's parts of the argument

155
00:10:08,290 --> 00:10:10,000
 that just fall apart.

156
00:10:10,000 --> 00:10:16,430
 But most especially, the idea that the Buddha, the idea

157
00:10:16,430 --> 00:10:20,000
 that these earlier teachings were just,

158
00:10:20,000 --> 00:10:22,440
 just because nobody would understand if he actually told

159
00:10:22,440 --> 00:10:25,000
 the truth, or they would be turned off.

160
00:10:25,000 --> 00:10:30,570
 So instead of that, he had to trick them. He was tricking

161
00:10:30,570 --> 00:10:31,000
 people.

162
00:10:31,000 --> 00:10:33,000
 All of the teachings that we follow is terrible.

163
00:10:33,000 --> 00:10:37,000
 Buddhism, mostly just to trick people.

164
00:10:37,000 --> 00:10:40,000
 Not exactly, but to encourage people in good things.

165
00:10:40,000 --> 00:10:44,000
 It's still all about good things. It's not evil.

166
00:10:44,000 --> 00:10:49,000
 It's kind of evil to put these words in the Buddha's mouth.

167
00:10:49,000 --> 00:10:54,000
 And now we don't make a religion of basically nullifying

168
00:10:54,000 --> 00:10:57,000
 all the things that Buddha ever taught.

169
00:10:57,000 --> 00:11:01,000
 Or the quorum, or whatever.

170
00:11:01,000 --> 00:11:05,000
 Right, so that's...

171
00:11:05,000 --> 00:11:09,090
 Oh, and today a couple of people came to visit in the

172
00:11:09,090 --> 00:11:11,000
 morning.

173
00:11:11,000 --> 00:11:18,000
 A woman with her... two, uh, with her eight month old kid.

174
00:11:18,000 --> 00:11:23,000
 A daughter had to meditate. She lives just down the road.

175
00:11:23,000 --> 00:11:29,000
 It was nice to meet our neighbours.

176
00:11:29,000 --> 00:11:39,600
 Today we have a quote about helping oneself by no harm in

177
00:11:39,600 --> 00:11:42,000
 others.

178
00:11:42,000 --> 00:11:45,000
 Which is interesting. I think there's a...

179
00:11:45,000 --> 00:11:48,860
 I think it's only a partial quote. It's going to be

180
00:11:48,860 --> 00:11:51,000
 interesting to read the whole sutra.

181
00:11:51,000 --> 00:11:55,000
 So I don't have it here on my phone, but...

182
00:11:55,000 --> 00:11:59,000
 It talks about what leads to great good for oneself.

183
00:11:59,000 --> 00:12:01,000
 It talks about not harming others.

184
00:12:01,000 --> 00:12:07,600
 So, I mean, that is a fair point, of course, because that's

185
00:12:07,600 --> 00:12:10,000
 the whole thing about...

186
00:12:10,000 --> 00:12:14,000
 I mean, this sutra has a bit of a problem for people

187
00:12:14,000 --> 00:12:15,000
 because they might think,

188
00:12:15,000 --> 00:12:19,550
 "Well, harming others isn't necessarily bad for you, right

189
00:12:19,550 --> 00:12:20,000
?"

190
00:12:20,000 --> 00:12:24,340
 So you say, "I shouldn't harm others because I wouldn't

191
00:12:24,340 --> 00:12:28,000
 like it, and so how can I do it to them?"

192
00:12:28,000 --> 00:12:31,000
 And they think, "Well, harming them doesn't harm me, right?

193
00:12:31,000 --> 00:12:37,330
 If I steal from someone I don't get stolen from, right, I

194
00:12:37,330 --> 00:12:41,000
 actually gain. I've gained from that."

195
00:12:41,000 --> 00:12:46,000
 So that's good for me, right?

196
00:12:46,000 --> 00:12:50,320
 But underlying this is something much more important than

197
00:12:50,320 --> 00:12:51,000
 that.

198
00:12:51,000 --> 00:12:59,000
 That harming others, it sets you in that realm of harm.

199
00:12:59,000 --> 00:13:03,000
 The people who you associate with start to change.

200
00:13:03,000 --> 00:13:08,000
 Your mind starts to become more corrupt.

201
00:13:08,000 --> 00:13:10,780
 That's why what goes around comes around, is because your

202
00:13:10,780 --> 00:13:12,000
 whole universe changes.

203
00:13:12,000 --> 00:13:15,150
 If you're a good person, you surround yourself and you'll

204
00:13:15,150 --> 00:13:20,000
 be interested in being a bond of good people.

205
00:13:20,000 --> 00:13:25,000
 If you cultivate unwholesomeness, you'll associate yourself

206
00:13:25,000 --> 00:13:29,000
 and surround yourself with unwholesome people.

207
00:13:29,000 --> 00:13:32,700
 And so much more, I mean, everything will change around you

208
00:13:32,700 --> 00:13:33,000
.

209
00:13:33,000 --> 00:13:37,000
 Your mind will change. Your health will change.

210
00:13:37,000 --> 00:13:41,670
 And so your work will change. Your productivity will change

211
00:13:41,670 --> 00:13:42,000
.

212
00:13:42,000 --> 00:13:52,000
 Your relationships will change. Everything will change.

213
00:13:52,000 --> 00:14:03,000
 Harm leads to harm.

214
00:14:03,000 --> 00:14:10,230
 Anyway, not too much the same as I inside. I think that's

215
00:14:10,230 --> 00:14:17,000
 enough talk for me tonight.

216
00:14:17,000 --> 00:14:19,000
 Thank you all for tuning in.

217
00:14:19,000 --> 00:14:23,000
 Sorry about yesterday, as I was a little bit...

218
00:14:23,000 --> 00:14:28,580
 I just didn't feel up to doing a video or even broadcasting

219
00:14:28,580 --> 00:14:29,000
.

220
00:14:29,000 --> 00:14:33,000
 Tomorrow I'll try to do... tomorrow Q&A.

221
00:14:33,000 --> 00:14:37,000
 So Saturday's Q&A. We'll try that.

222
00:14:37,000 --> 00:14:41,900
 We're under the assumption that Saturday night is when

223
00:14:41,900 --> 00:14:46,000
 everyone will be sitting at home.

224
00:14:46,000 --> 00:14:50,000
 Because no one does anything on Saturday night, right?

225
00:14:50,000 --> 00:14:58,000
 And Saturday night we can meet together and do a Q&A.

226
00:14:58,000 --> 00:15:03,000
 And then next Thursday we'll do Buddhism 101.

227
00:15:03,000 --> 00:15:07,000
 Okay. Thanks guys. Thanks for tuning in. Have a good night.

