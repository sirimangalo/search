1
00:00:00,000 --> 00:00:04,000
 This may be a follow-up. Often times we talk about

2
00:00:04,000 --> 00:00:10,210
 experiencing nirvana directly, but how is this possible if

3
00:00:10,210 --> 00:00:15,000
 consciousness ceases during nirvana?

4
00:00:15,000 --> 00:00:20,740
 Well that is an experience, isn't it? It's an experience to

5
00:00:20,740 --> 00:00:26,730
 have consciousness suddenly cease on you. It's all just

6
00:00:26,730 --> 00:00:32,000
 words. It doesn't mean much. Consciousness ceases.

7
00:00:32,000 --> 00:00:39,060
 It's like, suppose you have a machine, think of the body or

8
00:00:39,060 --> 00:00:46,410
 the being as a machine that's running 24/7. And so it's got

9
00:00:46,410 --> 00:00:49,000
 a lot of problems because of that.

10
00:00:49,000 --> 00:00:53,030
 It's heating up, it's melting, it's fraying, or it's

11
00:00:53,030 --> 00:00:57,430
 wearing away or so on. Let's not say fraying and wearing.

12
00:00:57,430 --> 00:01:02,000
 We can say that. But let's say in general it's overheating.

13
00:01:02,000 --> 00:01:07,450
 And the human being is much more complicated and nirvana is

14
00:01:07,450 --> 00:01:12,000
 a little bit more subtle than that. But take it in general,

15
00:01:12,000 --> 00:01:17,270
 the idea of just turning the machine off for just a short

16
00:01:17,270 --> 00:01:18,000
 time.

17
00:01:18,000 --> 00:01:22,230
 And how that feels when you come back online again. How it

18
00:01:22,230 --> 00:01:27,980
 feels to have just gone offline for a bit. That's how nir

19
00:01:27,980 --> 00:01:31,000
vana is.

20
00:01:31,000 --> 00:01:34,160
 During the time that the machine is off, there's no

21
00:01:34,160 --> 00:01:38,000
 activity. But you'd certainly feel it after you come back.

22
00:01:38,000 --> 00:01:44,480
 When the mind returns, there's something different. And you

23
00:01:44,480 --> 00:01:50,280
 know that you've "experienced" something that is beyond

24
00:01:50,280 --> 00:01:52,000
 explanation.

25
00:01:52,000 --> 00:01:54,670
 Unless you just want to explain consciousness ceases, which

26
00:01:54,670 --> 00:01:56,000
 is kind of spoiling the fun.

27
00:01:56,000 --> 00:02:02,450
 See the problem with saying consciousness ceases, nirvana

28
00:02:02,450 --> 00:02:09,220
 consciousness ceases, is that it's true but it's so dull.

29
00:02:09,220 --> 00:02:16,000
 You can't explain nirvana, but you can't describe it.

30
00:02:16,000 --> 00:02:21,500
 There's no words that you could use to do justice to it.

31
00:02:21,500 --> 00:02:25,340
 Let's put it that way. So no matter you might be able to

32
00:02:25,340 --> 00:02:27,820
 technically say it's this or it's that, you can't do

33
00:02:27,820 --> 00:02:29,000
 justice to it.

34
00:02:29,000 --> 00:02:32,630
 And so that's the danger of trying to put it into words.

35
00:02:32,630 --> 00:02:36,000
 Not that it's difficult to understand or so on.

36
00:02:36,000 --> 00:02:41,930
 It's an experience. So whether that's accurate or not,

37
00:02:41,930 --> 00:02:46,890
 because obviously there's no consciousness or the

38
00:02:46,890 --> 00:02:52,480
 consciousness that is experiencing it has ceased, which is

39
00:02:52,480 --> 00:02:55,000
 the experience itself, the cessation of consciousness.

40
00:02:55,000 --> 00:03:00,460
 But you know, it's actually technically it's not described

41
00:03:00,460 --> 00:03:06,190
 as the cessation of consciousness. The technical aspects of

42
00:03:06,190 --> 00:03:08,000
 it are difficult.

43
00:03:08,000 --> 00:03:15,850
 They say that consciousness is still there, but it's taking

44
00:03:15,850 --> 00:03:19,000
 nirvana as an object.

45
00:03:19,000 --> 00:03:22,380
 And I don't want to go into those technical aspects. I like

46
00:03:22,380 --> 00:03:27,370
 my explanation better, but it's wrong. No, the dogma, the

47
00:03:27,370 --> 00:03:34,000
 canonical explanation is that consciousness is still there,

48
00:03:34,000 --> 00:03:38,000
 but it's taking nirvana as an object.

49
00:03:38,000 --> 00:03:43,770
 And that just means it's ceased. Would it be almost like

50
00:03:43,770 --> 00:03:51,830
 the idea that true nirvana is a lack of any, its total

51
00:03:51,830 --> 00:03:53,000
 presence.

52
00:03:53,000 --> 00:03:57,410
 In other words, there is no past whatsoever and there's no

53
00:03:57,410 --> 00:04:01,730
 future that you're dwelling exactly just as it were right

54
00:04:01,730 --> 00:04:05,000
 in the middle in that split, now second.

55
00:04:05,000 --> 00:04:09,170
 Well, in fact, there is no such thing as time in that

56
00:04:09,170 --> 00:04:15,290
 respect, isn't it? The future is just simply or the present

57
00:04:15,290 --> 00:04:20,000
 is just a future going on its journey to the past.

58
00:04:20,000 --> 00:04:25,640
 I mean, it's that split moment. I kind of like to think of

59
00:04:25,640 --> 00:04:29,500
 nirvana in that time that it is the present, but since it

60
00:04:29,500 --> 00:04:33,490
 is true presence, there's no past, no future, there's

61
00:04:33,490 --> 00:04:36,000
 almost like there is no consciousness.

62
00:04:36,000 --> 00:04:44,430
 Well, the problem is that nirvana, the accurate definition

63
00:04:44,430 --> 00:04:47,000
 of nirvana is the cessation of suffering.

64
00:04:47,000 --> 00:04:51,910
 And if you're totally in the present moment contemplating

65
00:04:51,910 --> 00:04:56,480
 an object, conventional object, if there's an experience

66
00:04:56,480 --> 00:04:59,320
 where you're thinking, seeing, hearing, smelling, tasting,

67
00:04:59,320 --> 00:05:02,260
 feeling, thinking, then they're suffering because those

68
00:05:02,260 --> 00:05:05,000
 things are by definition dukkha.

69
00:05:05,000 --> 00:05:07,550
 So nirvana has no, there's no seeing, no hearing, no

70
00:05:07,550 --> 00:05:12,780
 smelling, no tasting, no feeling, no thinking. And because

71
00:05:12,780 --> 00:05:19,670
 of that, there is no memory of it and there is no

72
00:05:19,670 --> 00:05:23,000
 recognition in it.

73
00:05:23,000 --> 00:05:29,900
 All of those things cease. So it's poetically accurate to

74
00:05:29,900 --> 00:05:33,000
 say it's true present moment.

75
00:05:33,000 --> 00:05:37,150
 I think you have to be careful because there are ways of

76
00:05:37,150 --> 00:05:42,020
 describing a truly present state, which you might in fact

77
00:05:42,020 --> 00:05:46,900
 use to describe the moment before nirvana, where one is

78
00:05:46,900 --> 00:05:51,470
 truly present and truly sees the truth of suffering, really

79
00:05:51,470 --> 00:05:57,640
 sees things as they are, because the next moment is

80
00:05:57,640 --> 00:06:00,000
 cessation.

81
00:06:00,000 --> 00:06:02,000
 Here's a personal question.

82
00:06:02,000 --> 00:06:04,000
 Wait, wait, wait, let me stop.

