1
00:00:00,000 --> 00:00:07,000
 Hello and welcome back to our study of the Dhammapada.

2
00:00:07,000 --> 00:00:14,460
 Today we continue on with verse number 76, which reads as

3
00:00:14,460 --> 00:00:17,000
 follows.

4
00:00:17,000 --> 00:00:33,490
 dhinamvatarangyam paseyvadchathasinam ningayah vadin medhav

5
00:00:33,490 --> 00:00:37,000
in tadi sankhanditam bhaje

6
00:00:38,000 --> 00:00:54,530
 manaset seyo hotinapapioti, which translates to "like one

7
00:00:54,530 --> 00:01:07,000
 who unearths or uncovers buried treasure."

8
00:01:07,000 --> 00:01:16,150
 Whatever person seeing one's faults, paseyvwen seeing one's

9
00:01:16,150 --> 00:01:21,000
 faults, vajadasinadah,

10
00:01:21,000 --> 00:01:25,000
 lets them be known. That's one no one's faults.

11
00:01:25,000 --> 00:01:28,850
 That's one no of one's own faults. So point out one's

12
00:01:28,850 --> 00:01:30,000
 faults.

13
00:01:30,000 --> 00:01:39,660
 Someone who, this person, ningayah vadin, one who speaks in

14
00:01:39,660 --> 00:01:45,000
 censure or gives one admonishment, admonishes one.

15
00:01:45,000 --> 00:01:51,000
 Medhavin should be considered a wise person.

16
00:01:51,000 --> 00:01:58,100
 dadi sankhanditam bhaje, such a person should be associated

17
00:01:58,100 --> 00:02:06,410
 with by the wise. If a person is wise, such a person is to

18
00:02:06,410 --> 00:02:10,000
 be associated with by the wise people.

19
00:02:10,000 --> 00:02:20,590
 When one associates with such a person, things get better,

20
00:02:20,590 --> 00:02:23,530
 not worse. Seyo hotinapapiyo, they get better. You don't go

21
00:02:23,530 --> 00:02:24,000
 to evil. You don't get worse.

22
00:02:24,000 --> 00:02:27,610
 So associate with people who point out your faults. When

23
00:02:27,610 --> 00:02:33,000
 you do, things get better, not worse.

24
00:02:33,000 --> 00:02:39,520
 This verse was told in regards to a student of Sariputta

25
00:02:39,520 --> 00:02:41,000
 named Radha.

26
00:02:41,000 --> 00:02:49,030
 Radha was a poor brahmin who heard the Buddha's teaching

27
00:02:49,030 --> 00:02:55,100
 and went to live with the monks, but the monks were

28
00:02:55,100 --> 00:02:59,000
 unwilling to give him any ordination.

29
00:02:59,000 --> 00:03:03,890
 I think somewhere there was a comment that it was because

30
00:03:03,890 --> 00:03:07,870
 he took such great care of the monks, and they were

31
00:03:07,870 --> 00:03:11,190
 reluctant to give him the ordination because they needed

32
00:03:11,190 --> 00:03:13,000
 someone to look after them.

33
00:03:13,000 --> 00:03:16,910
 Which is sort of an interesting situation. It doesn't say

34
00:03:16,910 --> 00:03:20,620
 that in this version of the story. But for some reason it

35
00:03:20,620 --> 00:03:23,000
 may have been that he was old.

36
00:03:23,000 --> 00:03:28,620
 It may have been that they had no trust in him. But the

37
00:03:28,620 --> 00:03:34,650
 Buddha saw this, and the Buddha discerned through his vast

38
00:03:34,650 --> 00:03:40,060
 knowledge and understanding that Radha would actually be

39
00:03:40,060 --> 00:03:44,000
 capable of becoming an arahant if he were to ordain.

40
00:03:44,000 --> 00:03:49,250
 So he went to the monks and he said, "Who is this man kind

41
00:03:49,250 --> 00:03:54,770
 of pretending that he didn't know, or not letting on that

42
00:03:54,770 --> 00:03:56,000
 he knew?"

43
00:03:56,000 --> 00:04:00,230
 And they told him who he was and said, "Well, why aren't

44
00:04:00,230 --> 00:04:02,000
 you ordaining him?"

45
00:04:02,000 --> 00:04:05,750
 I think maybe that's where it says it. It may actually say

46
00:04:05,750 --> 00:04:07,000
 why they didn't ordain him.

47
00:04:07,000 --> 00:04:11,530
 But anyway, so he said, "Hey, does anyone, do any of you,

48
00:04:11,530 --> 00:04:15,660
 can any of you think of a reason why you might be inclined

49
00:04:15,660 --> 00:04:19,230
 to ordain him? Can any of you remember something that he

50
00:04:19,230 --> 00:04:20,000
 did for you?"

51
00:04:20,000 --> 00:04:26,240
 And Sariputta piped up and said, "I remember once when he

52
00:04:26,240 --> 00:04:32,000
 was living at home, he gave me a spoonful of rice."

53
00:04:32,000 --> 00:04:37,860
 And the Buddha said, "Well, Sariputta, don't you think it's

54
00:04:37,860 --> 00:04:43,000
 appropriate to be grateful and to repay people's kindness?"

55
00:04:43,000 --> 00:04:46,510
 Which is interesting because he didn't do that great of a

56
00:04:46,510 --> 00:04:50,140
 thing in the grand scheme of things. All he did was give

57
00:04:50,140 --> 00:04:52,000
 him a single spoonful of rice.

58
00:04:52,000 --> 00:04:56,850
 But it's a testament to the Buddha's sensitivity and Sarip

59
00:04:56,850 --> 00:05:00,000
utta's gratitude in remembering.

60
00:05:00,000 --> 00:05:06,090
 So when you ask someone, "Has this person done anything for

61
00:05:06,090 --> 00:05:07,000
 you?"

62
00:05:07,000 --> 00:05:11,380
 It's not common for people to think, to keep that in their

63
00:05:11,380 --> 00:05:14,000
 mind, that this person did such a simple thing.

64
00:05:14,000 --> 00:05:20,000
 But Sariputta was a person who had this great sensitivity.

65
00:05:20,000 --> 00:05:25,170
 He didn't take for granted the fact that he had been given

66
00:05:25,170 --> 00:05:28,000
 even a single spoonful of rice.

67
00:05:28,000 --> 00:05:30,120
 And so he spoke up and the Buddha said, "Well, in that case

68
00:05:30,120 --> 00:05:32,000
, don't you think it's worth ordaining him?"

69
00:05:32,000 --> 00:05:36,100
 And Sariputta immediately said, "In that case, I will ord

70
00:05:36,100 --> 00:05:37,000
ain him."

71
00:05:37,000 --> 00:05:39,000
 And so Sariputta gave him the ordination.

72
00:05:39,000 --> 00:05:41,780
 And I guess there was some skepticism as to whether this

73
00:05:41,780 --> 00:05:46,680
 monk would actually, whether this man would actually make a

74
00:05:46,680 --> 00:05:48,000
 good monk.

75
00:05:48,000 --> 00:05:52,620
 But Sariputta was fair with him and honest with him and

76
00:05:52,620 --> 00:05:58,000
 found that actually Radha was quite amenable to training.

77
00:05:58,000 --> 00:06:00,720
 So Sariputta would tell him, "Don't do this, don't do that

78
00:06:00,720 --> 00:06:01,000
."

79
00:06:01,000 --> 00:06:03,950
 And he would take everything Sariputta said to heart when

80
00:06:03,950 --> 00:06:06,550
 Sariputta told him, "You have to do this, you have to do

81
00:06:06,550 --> 00:06:07,000
 that."

82
00:06:07,000 --> 00:06:11,670
 He would only have to hear it once and he would immediately

83
00:06:11,670 --> 00:06:13,000
 adjust himself.

84
00:06:13,000 --> 00:06:15,930
 And when Sariputta admonished him saying, "You're doing

85
00:06:15,930 --> 00:06:19,000
 this wrong, you're doing that wrong, this is not right.

86
00:06:19,000 --> 00:06:22,000
 You have this fault, you have that fault."

87
00:06:22,000 --> 00:06:26,000
 He was completely amenable.

88
00:06:26,000 --> 00:06:28,400
 And so the Buddha asked him at one time, "How is your

89
00:06:28,400 --> 00:06:31,000
 student going? Do you find him amenable?"

90
00:06:31,000 --> 00:06:36,000
 Sariputta said, "He is the perfect student.

91
00:06:36,000 --> 00:06:39,110
 When I tell him not to do something, he stops doing it and

92
00:06:39,110 --> 00:06:41,000
 never have to tell him twice."

93
00:06:41,000 --> 00:06:44,000
 And the same goes with telling him what he should do.

94
00:06:44,000 --> 00:06:47,000
 He is the perfect student.

95
00:06:47,000 --> 00:06:50,700
 And the Buddha said, "If you could have other students like

96
00:06:50,700 --> 00:06:54,990
 him, would you ordain others if you knew that they were

97
00:06:54,990 --> 00:06:56,000
 going to be like him?"

98
00:06:56,000 --> 00:07:00,220
 And he said, "If I could have a thousand students, I would

99
00:07:00,220 --> 00:07:04,000
 gladly take them on if they were all like Radha."

100
00:07:04,000 --> 00:07:07,600
 And the monks got talking about this and they said how

101
00:07:07,600 --> 00:07:10,000
 amazing it was, how wonderful it was,

102
00:07:10,000 --> 00:07:12,870
 that Sariputta had found such a wonderful student and that

103
00:07:12,870 --> 00:07:15,560
 Radha also had found such a wonderful teacher who was

104
00:07:15,560 --> 00:07:18,000
 willing to point out his faults.

105
00:07:18,000 --> 00:07:22,950
 And in no long time, Sariputta became an Arahant, as

106
00:07:22,950 --> 00:07:24,000
 predicted.

107
00:07:24,000 --> 00:07:30,630
 And the Buddha heard the monks talking like this and then

108
00:07:30,630 --> 00:07:35,000
 he, as a result, spoke this verse,

109
00:07:35,000 --> 00:07:44,200
 saying, "Indeed, Buddha is very lucky, but Radha also is

110
00:07:44,200 --> 00:07:47,300
 very lucky because Sariputta is someone who is like a

111
00:07:47,300 --> 00:07:52,000
 person who points out buried treasure."

112
00:07:52,000 --> 00:07:55,000
 And he spoke this verse.

113
00:07:55,000 --> 00:07:59,970
 So that's the backstory. It's a fairly well-known Buddhist

114
00:07:59,970 --> 00:08:02,000
 story among Buddhists.

115
00:08:02,000 --> 00:08:07,020
 And it relates to our practice in regards to the role of a

116
00:08:07,020 --> 00:08:12,000
 teacher in one's relationship with one's teacher.

117
00:08:12,000 --> 00:08:19,200
 You could also say it relates to our own ability to receive

118
00:08:19,200 --> 00:08:22,000
 criticism in general.

119
00:08:22,000 --> 00:08:27,160
 And therefore to our relationship with anyone who gives us

120
00:08:27,160 --> 00:08:32,180
 criticism because we receive criticism from all sorts of

121
00:08:32,180 --> 00:08:33,000
 sources,

122
00:08:33,000 --> 00:08:40,000
 from many people who are not qualified to criticize.

123
00:08:40,000 --> 00:08:43,310
 How do you deal with people who criticize you unjustly, for

124
00:08:43,310 --> 00:08:44,000
 example?

125
00:08:44,000 --> 00:08:48,640
 This doesn't directly relate to that, but that's the

126
00:08:48,640 --> 00:08:52,000
 general subject that we're dealing with.

127
00:08:52,000 --> 00:08:58,280
 And it relates because this is so contrary to the state of

128
00:08:58,280 --> 00:09:01,000
 an ordinary human being,

129
00:09:01,000 --> 00:09:07,730
 which is to incline towards hiding one's faults and

130
00:09:07,730 --> 00:09:14,000
 becoming upset when people point out our faults,

131
00:09:14,000 --> 00:09:19,600
 from criticizing people for being critical. Everyone's a

132
00:09:19,600 --> 00:09:22,000
 critic, we say.

133
00:09:22,000 --> 00:09:28,080
 And there are teachings that instruct students, teachers

134
00:09:28,080 --> 00:09:31,000
 will sometimes instruct students,

135
00:09:31,000 --> 00:09:33,220
 or there's books that are written that say you should

136
00:09:33,220 --> 00:09:37,000
 accept criticism and thank anyone who gives you criticism.

137
00:09:37,000 --> 00:09:40,000
 And I think there's some truth to that.

138
00:09:40,000 --> 00:09:45,720
 But the response and the skepticism is that it leads you to

139
00:09:45,720 --> 00:09:49,000
 allow others to walk all over you,

140
00:09:49,000 --> 00:09:54,370
 and to give unjust criticism and to leave unjust criticism

141
00:09:54,370 --> 00:09:56,000
 unchallenged.

142
00:09:56,000 --> 00:10:00,390
 And I think that also is a good point, and it goes against

143
00:10:00,390 --> 00:10:02,000
 the Buddhist teaching.

144
00:10:02,000 --> 00:10:13,000
 In monastic society, there's a monk should be criticized,

145
00:10:13,000 --> 00:10:17,000
 not criticized, but a monk is wrong,

146
00:10:17,000 --> 00:10:26,010
 is at fault when they get angry at criticism, so respond

147
00:10:26,010 --> 00:10:29,000
 angrily or criticize in return.

148
00:10:29,000 --> 00:10:31,550
 So you tell me, "I did this wrong," and I say, "Well, you

149
00:10:31,550 --> 00:10:33,000
 did this other thing wrong,"

150
00:10:33,000 --> 00:10:38,440
 or to ignore people's criticism and not accept their

151
00:10:38,440 --> 00:10:40,000
 criticism.

152
00:10:40,000 --> 00:10:43,990
 All of this is faulty, but what's also faulty is to not

153
00:10:43,990 --> 00:10:46,000
 speak in one's defense.

154
00:10:46,000 --> 00:10:51,000
 It's at fault when one doesn't speak in one's defense.

155
00:10:51,000 --> 00:10:59,160
 So there's a sense of having to be mindful and to be wise

156
00:10:59,160 --> 00:11:02,000
 and to be balanced.

157
00:11:02,000 --> 00:11:08,460
 In many issues that we deal with in life, Buddhism doesn't

158
00:11:08,460 --> 00:11:12,000
 have a be-all, end-all answer.

159
00:11:12,000 --> 00:11:15,000
 Buddhism is much more dealing with the building blocks.

160
00:11:15,000 --> 00:11:18,000
 It is one of the things that make up any given situation.

161
00:11:18,000 --> 00:11:21,000
 So there are much, much more general principles,

162
00:11:21,000 --> 00:11:24,370
 and the general principle is to be able to discern the

163
00:11:24,370 --> 00:11:29,000
 truth from falsehood and right from wrong.

164
00:11:29,000 --> 00:11:34,160
 But here we come to this specific example of when the

165
00:11:34,160 --> 00:11:37,000
 criticism is warranted.

166
00:11:37,000 --> 00:11:42,120
 So we're dealing with someone who actually points out true/

167
00:11:42,120 --> 00:11:43,000
false.

168
00:11:43,000 --> 00:11:49,610
 I think it can be said that there's quite often a grain of

169
00:11:49,610 --> 00:11:52,000
 truth in every criticism.

170
00:11:52,000 --> 00:11:55,000
 There's a reason why we're being criticized.

171
00:11:55,000 --> 00:11:58,880
 Often people will criticize us unwarranted and look not

172
00:11:58,880 --> 00:12:00,000
 wishing to help us.

173
00:12:00,000 --> 00:12:10,560
 Some people criticize us often looking to upset us or

174
00:12:10,560 --> 00:12:15,000
 looking to humiliate us or looking to hide our own fault.

175
00:12:15,000 --> 00:12:17,480
 We criticize others to hide our own faults, this kind of

176
00:12:17,480 --> 00:12:18,000
 thing.

177
00:12:18,000 --> 00:12:22,880
 But even still, even in those cases, it's much easier to

178
00:12:22,880 --> 00:12:24,000
 see the faults of others.

179
00:12:24,000 --> 00:12:28,920
 We hide our own faults, but we're very good at picking out

180
00:12:28,920 --> 00:12:31,000
 the faults of others.

181
00:12:31,000 --> 00:12:38,000
 But here, in the case of a teacher, we have another dilemma

182
00:12:38,000 --> 00:12:38,000
,

183
00:12:38,000 --> 00:12:49,700
 and that's our inability to accept criticism, not because

184
00:12:49,700 --> 00:12:52,000
 it's not correct,

185
00:12:52,000 --> 00:12:59,000
 but because it shows a fault.

186
00:12:59,000 --> 00:13:03,000
 It hurts our ego.

187
00:13:03,000 --> 00:13:07,460
 It attacks something that we are protecting, and that is

188
00:13:07,460 --> 00:13:08,000
 ourself.

189
00:13:08,000 --> 00:13:11,000
 We hold ourselves dear.

190
00:13:11,000 --> 00:13:16,000
 We cling to an image of who we are or who we want to be.

191
00:13:16,000 --> 00:13:20,130
 And when that image is threatened, like anything, any

192
00:13:20,130 --> 00:13:25,000
 belonging, anything we hold dear, we react, we get upset.

193
00:13:25,000 --> 00:13:32,650
 And so this teaching is actually quite important for medit

194
00:13:32,650 --> 00:13:35,000
ators in a meditative setting,

195
00:13:35,000 --> 00:13:38,460
 because we need to take advice from others, unless you're

196
00:13:38,460 --> 00:13:43,000
 the kind of person who can become enlightened miraculously

197
00:13:43,000 --> 00:13:43,000
 by oneself.

198
00:13:43,000 --> 00:13:46,000
 We have to take advice from others.

199
00:13:46,000 --> 00:13:51,000
 And as a teacher, this is something that is quite familiar.

200
00:13:51,000 --> 00:13:58,250
 It often, it becomes so difficult that teachers are often

201
00:13:58,250 --> 00:14:01,000
 afraid to give advice.

202
00:14:01,000 --> 00:14:07,840
 And much of a teacher's role and duty is to find ways to

203
00:14:07,840 --> 00:14:14,000
 admonish one's students without upsetting them.

204
00:14:14,000 --> 00:14:18,560
 A great part of the skill of being a good teacher is the

205
00:14:18,560 --> 00:14:22,000
 ability to not upset one's students,

206
00:14:22,000 --> 00:14:27,020
 because anyone can give advice, and a real sign,

207
00:14:27,020 --> 00:14:32,000
 unfortunately, of a poor teacher is not being able to,

208
00:14:32,000 --> 00:14:35,070
 it's not that they can't give advice, but not being able to

209
00:14:35,070 --> 00:14:44,330
 couch the advice in such delicate terms or means that the

210
00:14:44,330 --> 00:14:46,000
 student is actually able to accept it.

211
00:14:46,000 --> 00:14:50,830
 So you'll often hear people giving advice, and as a teacher

212
00:14:50,830 --> 00:14:52,860
, anyway, you cringe because, you know, that's not going to

213
00:14:52,860 --> 00:14:53,000
 work,

214
00:14:53,000 --> 00:14:55,700
 and you're just making the student upset. What you're

215
00:14:55,700 --> 00:14:59,000
 saying is correct, but it's not going to get through.

216
00:14:59,000 --> 00:15:04,980
 And that's unfortunate, because it often makes the teacher

217
00:15:04,980 --> 00:15:07,000
's job more difficult,

218
00:15:07,000 --> 00:15:11,050
 and it hampers the teacher's ability to be frank with the

219
00:15:11,050 --> 00:15:12,000
 student.

220
00:15:12,000 --> 00:15:20,010
 And often a teacher's duty requires upsetting the student,

221
00:15:20,010 --> 00:15:22,000
 and sometimes you have no choice,

222
00:15:22,000 --> 00:15:25,520
 your choice is to not teach them or to hurt them, to upset

223
00:15:25,520 --> 00:15:30,000
 them, and you have to gauge how far you can push a student,

224
00:15:30,000 --> 00:15:36,000
 which is unfortunately usually not very far,

225
00:15:36,000 --> 00:15:39,230
 and usually, unless you're dealing with a special person

226
00:15:39,230 --> 00:15:42,000
 like Radha, which is why Sariputta was so happy,

227
00:15:42,000 --> 00:15:45,990
 but for most people, it's very difficult for us to take

228
00:15:45,990 --> 00:15:47,000
 criticism,

229
00:15:47,000 --> 00:15:52,230
 very hard for us to prevent the anger and the self-

230
00:15:52,230 --> 00:15:56,490
righteousness when people try to, you know, even well-

231
00:15:56,490 --> 00:15:58,000
meaning, try to help us.

232
00:15:58,000 --> 00:16:02,750
 And so we have to be aware of this, and that's one of the

233
00:16:02,750 --> 00:16:06,000
 important aspects of this teaching

234
00:16:06,000 --> 00:16:09,410
 that I think a lot of people react favorably to when this

235
00:16:09,410 --> 00:16:12,000
 imagery of pointing out buried treasure.

236
00:16:12,000 --> 00:16:16,000
 So we try to remind ourselves of this and to think of it as

237
00:16:16,000 --> 00:16:19,000
 someone pointing out buried treasure,

238
00:16:19,000 --> 00:16:23,400
 and that's why we'll come up with these teachings where we

239
00:16:23,400 --> 00:16:25,000
 tell people,

240
00:16:25,000 --> 00:16:28,460
 "Anyone who criticizes you, you should thank them. You

241
00:16:28,460 --> 00:16:34,000
 should thank people who criticize you."

242
00:16:34,000 --> 00:16:41,000
 I don't think it's, as I said, that when criticism is

243
00:16:41,000 --> 00:16:42,000
 faulty,

244
00:16:42,000 --> 00:16:45,360
 I don't think you should be hesitant to point out when it's

245
00:16:45,360 --> 00:16:46,000
 faulty.

246
00:16:46,000 --> 00:16:49,170
 Like my teacher said, if someone calls you a buffalo, you

247
00:16:49,170 --> 00:16:52,000
 just turn around and feel if you have a tail.

248
00:16:52,000 --> 00:16:55,580
 If you don't have a tail, you should tell them, "I don't

249
00:16:55,580 --> 00:16:58,000
 have a tail. I'm not a buffalo."

250
00:16:58,000 --> 00:17:00,880
 He didn't quite say it like that. In fact, it's more

251
00:17:00,880 --> 00:17:02,000
 annoying for yourself.

252
00:17:02,000 --> 00:17:07,550
 And I think that's the key that we should point out is it

253
00:17:07,550 --> 00:17:09,000
 requires wisdom.

254
00:17:09,000 --> 00:17:13,810
 You have to be wise and know, "Is this person, or what is

255
00:17:13,810 --> 00:17:16,000
 their motive for doing this?"

256
00:17:16,000 --> 00:17:20,140
 First of all, but regardless of their motive, is there

257
00:17:20,140 --> 00:17:23,000
 truth behind what they're saying?

258
00:17:23,000 --> 00:17:29,300
 Criticism is a very big part of the teaching dynamic, as

259
00:17:29,300 --> 00:17:31,000
 mentioned,

260
00:17:31,000 --> 00:17:35,700
 but also a very big part of our practice because, of course

261
00:17:35,700 --> 00:17:38,000
, it's dealing with ego.

262
00:17:38,000 --> 00:17:44,760
 It's a good test of our state of mind and our purity of

263
00:17:44,760 --> 00:17:45,000
 mind,

264
00:17:45,000 --> 00:17:47,000
 how well we're able to take criticism.

265
00:17:47,000 --> 00:17:52,000
 So criticism is actually a useful tool in our practice,

266
00:17:52,000 --> 00:17:55,900
 to put ourselves in position or to allow others to

267
00:17:55,900 --> 00:17:57,000
 criticize us

268
00:17:57,000 --> 00:18:01,690
 and to be mindful and to meditate on the criticism, on our

269
00:18:01,690 --> 00:18:04,000
 reactions to the criticism,

270
00:18:04,000 --> 00:18:08,000
 seeing how our mind reacts.

271
00:18:08,000 --> 00:18:12,000
 An enlightened being is the same in both praise and blame.

272
00:18:12,000 --> 00:18:14,520
 When people praise them, it's as though they didn't even

273
00:18:14,520 --> 00:18:15,000
 hear it.

274
00:18:15,000 --> 00:18:18,400
 They don't have any pleasure, they don't take pleasure when

275
00:18:18,400 --> 00:18:19,000
 others,

276
00:18:19,000 --> 00:18:23,000
 they aren't excited when other people praise them.

277
00:18:23,000 --> 00:18:28,100
 But it's the same when others have insulted them or

278
00:18:28,100 --> 00:18:29,000
 criticized them.

279
00:18:29,000 --> 00:18:32,000
 It's also as though they didn't hear.

280
00:18:32,000 --> 00:18:35,480
 In a sense, they take it for what it is, they take it at

281
00:18:35,480 --> 00:18:36,000
 face value.

282
00:18:36,000 --> 00:18:39,000
 "This person just said I'm doing something wrong."

283
00:18:39,000 --> 00:18:43,070
 And so they look at it and say, "Right, I was doing

284
00:18:43,070 --> 00:18:44,000
 something wrong."

285
00:18:44,000 --> 00:18:46,000
 And so they change it.

286
00:18:46,000 --> 00:18:50,000
 Or, "This person's upset at me and they want me to do this

287
00:18:50,000 --> 00:18:50,000
."

288
00:18:50,000 --> 00:18:52,000
 Or, "They want me to stop doing that."

289
00:18:52,000 --> 00:18:54,530
 "Okay, well, I'll stop doing it because that would upset

290
00:18:54,530 --> 00:18:55,000
 them."

291
00:18:55,000 --> 00:18:57,800
 You know, to a certain extent, but they do everything mind

292
00:18:57,800 --> 00:18:59,000
fully and with wisdom,

293
00:18:59,000 --> 00:19:01,000
 knowing what is the right state.

294
00:19:01,000 --> 00:19:04,000
 It's not that they're a pushover and someone tells them,

295
00:19:04,000 --> 00:19:07,000
 "You're too fat, you should go on a diet."

296
00:19:07,000 --> 00:19:09,000
 They don't really see the point of that.

297
00:19:09,000 --> 00:19:12,000
 "Why would I concern myself with my weight?"

298
00:19:12,000 --> 00:19:14,000
 "You're too skinny, you should eat more."

299
00:19:14,000 --> 00:19:17,000
 "Well, that's not why I eat."

300
00:19:17,000 --> 00:19:19,000
 But they don't get upset, you see.

301
00:19:19,000 --> 00:19:23,000
 The point is they're unperturbed in both praise and blame.

302
00:19:23,000 --> 00:19:26,750
 And we have to look at both because this is relating to the

303
00:19:26,750 --> 00:19:27,000
 ego.

304
00:19:27,000 --> 00:19:34,000
 It's relating to our conceit, our self, our view of self.

305
00:19:34,000 --> 00:19:37,000
 It's a good indicator.

306
00:19:37,000 --> 00:19:40,310
 And the other part of this quote is in regards to staying

307
00:19:40,310 --> 00:19:41,000
 with such a person.

308
00:19:41,000 --> 00:19:44,000
 The point, the specific point that things get better.

309
00:19:44,000 --> 00:19:46,000
 And that's a very useful advice to give oneself.

310
00:19:46,000 --> 00:19:48,600
 When one is angry, because you'll often get angry at your

311
00:19:48,600 --> 00:19:51,000
 teacher, it's a common thing.

312
00:19:51,000 --> 00:19:57,000
 And one student recently said, "I'm very angry at you."

313
00:19:57,000 --> 00:19:59,000
 Recently, I think those are the words.

314
00:19:59,000 --> 00:20:03,000
 "I'm very angry at you right now."

315
00:20:03,000 --> 00:20:06,220
 It wasn't even criticism, it was asking them to do

316
00:20:06,220 --> 00:20:09,000
 something that just seemed utterly ridiculous to them.

317
00:20:09,000 --> 00:20:15,000
 Seemed totally above and beyond what they were capable of.

318
00:20:15,000 --> 00:20:19,000
 And I said, "Well, I didn't."

319
00:20:19,000 --> 00:20:22,700
 Or was it, "I didn't, this wasn't my idea, I wasn't the one

320
00:20:22,700 --> 00:20:24,000
 who created this issue."

321
00:20:24,000 --> 00:20:27,000
 And they said, "I'm very angry.

322
00:20:27,000 --> 00:20:31,000
 They were very angry about it."

323
00:20:31,000 --> 00:20:34,000
 And this is quite common.

324
00:20:34,000 --> 00:20:38,050
 It's quite common for meditators to get angry at their

325
00:20:38,050 --> 00:20:39,000
 teacher.

326
00:20:39,000 --> 00:20:43,250
 And so there's no shame in that, and there's no reason to

327
00:20:43,250 --> 00:20:44,000
 get upset.

328
00:20:44,000 --> 00:20:48,000
 It's why we ask forgiveness when we do a meditation course.

329
00:20:48,000 --> 00:20:50,560
 We often formally ask forgiveness of the teacher, and the

330
00:20:50,560 --> 00:20:53,000
 teacher asks forgiveness of the student.

331
00:20:53,000 --> 00:20:55,760
 Both when we start the course and when we finish the course

332
00:20:55,760 --> 00:20:56,000
.

333
00:20:56,000 --> 00:21:01,000
 It sort of clears the air.

334
00:21:01,000 --> 00:21:04,000
 But the point is, this verse is quite useful.

335
00:21:04,000 --> 00:21:08,900
 It's been useful for me to remind myself that no matter how

336
00:21:08,900 --> 00:21:10,000
 difficult things get,

337
00:21:10,000 --> 00:21:13,000
 staying with a teacher and at a meditation center,

338
00:21:13,000 --> 00:21:18,290
 we have to remember that we want to do that, which makes

339
00:21:18,290 --> 00:21:22,000
 things better, makes it better.

340
00:21:22,000 --> 00:21:25,340
 Makes us better people, improves our situation, makes us

341
00:21:25,340 --> 00:21:26,000
 happier.

342
00:21:26,000 --> 00:21:29,000
 We have to remember that we know this.

343
00:21:29,000 --> 00:21:31,420
 We know that staying in the meditation center with a

344
00:21:31,420 --> 00:21:33,000
 teacher is going to make things better.

345
00:21:33,000 --> 00:21:38,000
 We often make excuses, "I'm not ready," or,

346
00:21:38,000 --> 00:21:42,090
 "The environment's not right, I'm not at the right point in

347
00:21:42,090 --> 00:21:43,000
 my life."

348
00:21:43,000 --> 00:21:47,000
 I have to remember that this is all excuses.

349
00:21:47,000 --> 00:21:50,230
 The truth is, staying in a meditation center with a

350
00:21:50,230 --> 00:21:51,000
 meditation center,

351
00:21:51,000 --> 00:21:53,000
 things will get better.

352
00:21:53,000 --> 00:21:58,000
 And whatever excuses we have for not staying are invalid,

353
00:21:58,000 --> 00:22:01,450
 because here is a person who is willing to help us with our

354
00:22:01,450 --> 00:22:02,000
 faults.

355
00:22:02,000 --> 00:22:06,000
 The faults are what we're focusing on.

356
00:22:06,000 --> 00:22:10,190
 And people looking to criticize Buddhism will use this as a

357
00:22:10,190 --> 00:22:11,000
 criticism,

358
00:22:11,000 --> 00:22:14,100
 and Buddhism is very pessimistic, which is ridiculous,

359
00:22:14,100 --> 00:22:16,000
 utterly ridiculous, harmful.

360
00:22:16,000 --> 00:22:20,000
 It's a terrible, terrible criticism.

361
00:22:20,000 --> 00:22:24,890
 I mean by, often by lazy people who aren't interested in

362
00:22:24,890 --> 00:22:27,000
 looking at their faults.

363
00:22:27,000 --> 00:22:30,000
 Because it's very damaging to say that.

364
00:22:30,000 --> 00:22:34,000
 The faults are the only reason for self-development.

365
00:22:34,000 --> 00:22:37,000
 It is our faults that we want to focus on,

366
00:22:37,000 --> 00:22:40,830
 not so that we can feel bad about ourselves or feel

367
00:22:40,830 --> 00:22:42,000
 horrible people,

368
00:22:42,000 --> 00:22:45,000
 but to actually fix something, to make something better.

369
00:22:45,000 --> 00:22:48,590
 How do you make something better without looking at what's

370
00:22:48,590 --> 00:22:49,000
 wrong,

371
00:22:49,000 --> 00:22:52,000
 without looking at the problem? And this is what we do.

372
00:22:52,000 --> 00:22:55,000
 If you don't have someone who can show you your faults,

373
00:22:55,000 --> 00:22:58,000
 who can point them out to you, who can help you fix them,

374
00:22:58,000 --> 00:23:02,000
 obviously it's not enough to point out one's faults,

375
00:23:02,000 --> 00:23:04,000
 but to actually help you fix them.

376
00:23:04,000 --> 00:23:06,000
 This is where things get better.

377
00:23:06,000 --> 00:23:12,380
 This is where true development and progress and goodness

378
00:23:12,380 --> 00:23:15,000
 comes from.

379
00:23:15,000 --> 00:23:20,000
 So, a very useful verse, something that we should remember.

380
00:23:20,000 --> 00:23:24,000
 Someone who points out your faults when they see them,

381
00:23:24,000 --> 00:23:27,380
 you should liken them to a person who points out buried

382
00:23:27,380 --> 00:23:29,000
 treasure.

383
00:23:29,000 --> 00:23:31,760
 When you stay with, you should associate with such a person

384
00:23:31,760 --> 00:23:32,000
.

385
00:23:32,000 --> 00:23:35,000
 The wise should associate with such a person.

386
00:23:35,000 --> 00:23:38,000
 When such a person, when one associates with such a person,

387
00:23:38,000 --> 00:23:40,000
 things get better, not worse.

388
00:23:40,000 --> 00:23:43,000
 Sayo hoti napapi wo.

389
00:23:43,000 --> 00:23:45,000
 That's the verse for today.

390
00:23:45,000 --> 00:23:47,000
 That's our teaching on the Dhammapada.

391
00:23:47,000 --> 00:23:51,000
 Thank you for tuning in. Wishing you all the best.

