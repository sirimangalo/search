1
00:00:00,000 --> 00:00:07,000
 Namat to Ratanatayasa.

2
00:00:07,000 --> 00:00:18,300
 Welcome everyone. Today is the session on Insight

3
00:00:18,300 --> 00:00:22,000
 Meditation.

4
00:00:22,000 --> 00:00:31,000
 I think of it sort of as a time where we can get away,

5
00:00:31,000 --> 00:00:34,000
 get away even from Buddhism.

6
00:00:34,000 --> 00:00:39,000
 We don't have to talk about Buddhist concepts,

7
00:00:39,000 --> 00:00:46,000
 where we can leave behind our affairs in the world.

8
00:00:46,000 --> 00:00:53,000
 But we can also have a chance just to practice

9
00:00:53,000 --> 00:01:01,000
 and not to worry about even Buddhist concepts or Buddhism.

10
00:01:01,000 --> 00:01:03,550
 Not meaning that we're going to throw away what we've

11
00:01:03,550 --> 00:01:04,000
 learned

12
00:01:04,000 --> 00:01:07,000
 or we're going to go outside of the Buddhist teaching,

13
00:01:07,000 --> 00:01:11,000
 but meaning that we don't have to study,

14
00:01:11,000 --> 00:01:15,000
 we don't have to talk about anything really.

15
00:01:15,000 --> 00:01:18,000
 So it should be considered that this is a time where we can

16
00:01:18,000 --> 00:01:21,000
 have a sort of a guided meditation.

17
00:01:21,000 --> 00:01:23,000
 You don't have to think of it even as guided,

18
00:01:23,000 --> 00:01:30,000
 it's just sort of a meditation with audio accompaniment.

19
00:01:30,000 --> 00:01:36,000
 So I'm going to put my character on busy.

20
00:01:36,000 --> 00:01:40,000
 I'd encourage people to do the same.

21
00:01:40,000 --> 00:01:44,000
 We can ignore things for a while.

22
00:01:44,000 --> 00:01:47,000
 It certainly isn't a time to be chatting with somebody else

23
00:01:47,000 --> 00:01:49,000
 while you're sitting here,

24
00:01:49,000 --> 00:01:56,440
 although it's certainly up to you and I won't know if you

25
00:01:56,440 --> 00:01:58,000
 do.

26
00:01:58,000 --> 00:02:07,040
 So you can just close your eyes and just take some time to

27
00:02:07,040 --> 00:02:09,000
 reflect on ourselves,

28
00:02:09,000 --> 00:02:14,000
 to look at ourselves, to look within,

29
00:02:14,000 --> 00:02:25,000
 to study this elusive creature we call reality.

30
00:02:25,000 --> 00:02:31,000
 Reality is kind of a funny word, I think.

31
00:02:31,000 --> 00:02:34,000
 If you ask yourself, "What is real?"

32
00:02:34,000 --> 00:02:38,000
 I think it's pretty hard to pinpoint.

33
00:02:38,000 --> 00:02:43,000
 When we see how far science has gone chasing after reality,

34
00:02:43,000 --> 00:02:46,470
 we also see how far the religions of the world have gone in

35
00:02:46,470 --> 00:02:49,000
 the other direction,

36
00:02:49,000 --> 00:02:55,720
 making all sorts of bold claims and assertions about what

37
00:02:55,720 --> 00:02:57,000
 is real.

38
00:02:57,000 --> 00:03:03,250
 For me, I can't figure out what sort of a definition you

39
00:03:03,250 --> 00:03:06,000
 could give to the word reality

40
00:03:06,000 --> 00:03:12,000
 or how you could define whether something is real or not,

41
00:03:12,000 --> 00:03:16,000
 except for that which is experienced.

42
00:03:16,000 --> 00:03:20,000
 For me, from what I understand, it's more of a mind game

43
00:03:20,000 --> 00:03:20,000
 than anything

44
00:03:20,000 --> 00:03:24,000
 if you talk about three-dimensional space

45
00:03:24,000 --> 00:03:30,210
 or even if you talk about Brahma and Atman and being one

46
00:03:30,210 --> 00:03:31,000
 with God

47
00:03:31,000 --> 00:03:40,000
 as being the ultimate reality.

48
00:03:40,000 --> 00:03:44,750
 It certainly seems possible that you could become one with

49
00:03:44,750 --> 00:03:45,000
 God

50
00:03:45,000 --> 00:03:49,520
 or become God-like or so on, could get to a state of on

51
00:03:49,520 --> 00:03:50,000
eness.

52
00:03:50,000 --> 00:03:53,450
 But I don't see how that's any more real than the

53
00:03:53,450 --> 00:03:54,000
 experience

54
00:03:54,000 --> 00:03:57,000
 that we're having right here and right now.

55
00:03:57,000 --> 00:04:00,860
 For people to say that our experience right here and right

56
00:04:00,860 --> 00:04:02,000
 now is illusion

57
00:04:02,000 --> 00:04:08,000
 just doesn't quite make sense.

58
00:04:08,000 --> 00:04:11,420
 Whatever you experience, to me, that seems like to be the

59
00:04:11,420 --> 00:04:15,000
 only thing that could possibly be real.

60
00:04:15,000 --> 00:04:21,000
 It's the only way you can make sense of this word real

61
00:04:21,000 --> 00:04:25,000
 because there is something there, there is an experience.

62
00:04:25,000 --> 00:04:29,000
 That's what we're having right now.

63
00:04:29,000 --> 00:04:32,510
 This is why Buddhism puts so much emphasis on the present

64
00:04:32,510 --> 00:04:33,000
 moment

65
00:04:33,000 --> 00:04:35,480
 because it's really all that we have, it's really all that

66
00:04:35,480 --> 00:04:36,000
 exists.

67
00:04:36,000 --> 00:04:39,360
 There's no future, there's no past, these are mind games we

68
00:04:39,360 --> 00:04:43,000
 play with ourselves.

69
00:04:43,000 --> 00:04:49,200
 There's only just a present moment that's eternal, that

70
00:04:49,200 --> 00:04:54,000
 doesn't end.

71
00:04:54,000 --> 00:04:57,730
 But in this one moment there's a lot going on, as we can

72
00:04:57,730 --> 00:04:59,000
 see, it's changing all the time.

73
00:04:59,000 --> 00:05:03,000
 Our experience is not the same.

74
00:05:03,000 --> 00:05:07,660
 Basically it can be broken up into six categories and they

75
00:05:07,660 --> 00:05:09,000
 come one at a time.

76
00:05:09,000 --> 00:05:12,400
 Sometimes we're seeing, sometimes we're hearing, sometimes

77
00:05:12,400 --> 00:05:14,000
 we're smelling, sometimes tasting,

78
00:05:14,000 --> 00:05:16,000
 sometimes feeling and sometimes thinking.

79
00:05:16,000 --> 00:05:18,000
 It happens quite quickly.

80
00:05:18,000 --> 00:05:23,000
 If we're not meditating it's quite difficult to see.

81
00:05:23,000 --> 00:05:26,520
 So here when we sit in meditation with our eyes closed and

82
00:05:26,520 --> 00:05:33,000
 we're watching reality,

83
00:05:33,000 --> 00:05:38,630
 we're taking time to refine our vision, refine our

84
00:05:38,630 --> 00:05:40,000
 perception,

85
00:05:40,000 --> 00:05:44,000
 to see things clearer than we could see them before.

86
00:05:44,000 --> 00:05:47,780
 We're not trying to push away, we're not trying to chase

87
00:05:47,780 --> 00:05:48,000
 after,

88
00:05:48,000 --> 00:05:51,000
 we're not trying to attain anything.

89
00:05:51,000 --> 00:05:54,630
 We're just trying to see clearer, clearer than we normally

90
00:05:54,630 --> 00:05:56,000
 do, normally could,

91
00:05:56,000 --> 00:06:05,360
 all of the things that present themselves to the six senses

92
00:06:05,360 --> 00:06:06,000
.

93
00:06:06,000 --> 00:06:12,310
 Why this is useful is because of the laws of nature which

94
00:06:12,310 --> 00:06:15,000
 govern experience.

95
00:06:15,000 --> 00:06:18,000
 When you see it's not just with impunity that you can see

96
00:06:18,000 --> 00:06:19,000
 what you want

97
00:06:19,000 --> 00:06:23,480
 or think about it whatever you want, have whatever reaction

98
00:06:23,480 --> 00:06:25,000
 you choose.

99
00:06:25,000 --> 00:06:28,500
 When we see something that we like we automatically grasp

100
00:06:28,500 --> 00:06:29,000
 onto it,

101
00:06:29,000 --> 00:06:34,000
 hold onto it and chase after it.

102
00:06:34,000 --> 00:06:38,000
 There are natural laws going on at work here.

103
00:06:38,000 --> 00:06:41,000
 So depending on which mind states arise,

104
00:06:41,000 --> 00:06:44,730
 when you see something depending on what sort of a mind

105
00:06:44,730 --> 00:06:46,000
 state arises next,

106
00:06:46,000 --> 00:06:50,080
 it's going to really define the result of seeing or the

107
00:06:50,080 --> 00:06:54,000
 result of hearing.

108
00:06:54,000 --> 00:06:59,760
 When you see something you don't like, right away there's

109
00:06:59,760 --> 00:07:01,000
 anger arises.

110
00:07:01,000 --> 00:07:07,000
 If you're not careful, if you're not clearly aware of it.

111
00:07:07,000 --> 00:07:11,490
 So our normal way of dealing with these things is either to

112
00:07:11,490 --> 00:07:14,000
 get angry or to get greedy

113
00:07:14,000 --> 00:07:19,000
 and then either choose to repress it.

114
00:07:19,000 --> 00:07:22,450
 You want something but you know it's wrong to chase after

115
00:07:22,450 --> 00:07:24,000
 things that you don't deserve

116
00:07:24,000 --> 00:07:27,000
 or that belong to somebody else or so on.

117
00:07:27,000 --> 00:07:32,610
 So you repress it or maybe it's something that you've been

118
00:07:32,610 --> 00:07:34,000
 told is wrong.

119
00:07:34,000 --> 00:07:39,000
 Sometimes for instance sex or sexuality is so stigmatized

120
00:07:39,000 --> 00:07:41,000
 or it has been in the past.

121
00:07:41,000 --> 00:07:44,000
 I think now people are getting more going to the other side

122
00:07:44,000 --> 00:07:48,000
 where they just chase after it all the time.

123
00:07:48,000 --> 00:07:53,000
 But still things like masturbation, things like pornography

124
00:07:53,000 --> 00:07:54,000
 or so on,

125
00:07:54,000 --> 00:07:58,330
 they're still I think probably pretty stigmatized at least

126
00:07:58,330 --> 00:07:59,000
 overtly.

127
00:07:59,000 --> 00:08:04,000
 So people do it covertly, they sneak in.

128
00:08:04,000 --> 00:08:07,000
 And as a result there's a lot of repression involved.

129
00:08:07,000 --> 00:08:11,000
 Pushing it down, pushing it away.

130
00:08:11,000 --> 00:08:14,000
 This is something we do. We do it also with anger.

131
00:08:14,000 --> 00:08:18,360
 We get really angry at people but we push it down, we rep

132
00:08:18,360 --> 00:08:19,000
ress it.

133
00:08:19,000 --> 00:08:22,000
 And we're pretty good at keeping it bottled up for a time

134
00:08:22,000 --> 00:08:25,000
 but we can only do it for a certain time.

135
00:08:25,000 --> 00:08:28,210
 And the way it works is we'll be working really hard to

136
00:08:28,210 --> 00:08:30,000
 keep ourselves calm,

137
00:08:30,000 --> 00:08:37,010
 to keep ourselves from wanting or keep ourselves from

138
00:08:37,010 --> 00:08:38,000
 hating.

139
00:08:38,000 --> 00:08:40,000
 And then suddenly our guard's down.

140
00:08:40,000 --> 00:08:43,000
 One moment we'll be maybe watching television

141
00:08:43,000 --> 00:08:47,600
 or maybe doing something enjoyable and we let our guard

142
00:08:47,600 --> 00:08:48,000
 down.

143
00:08:48,000 --> 00:08:53,080
 Or maybe something good comes and suddenly we feel sort of

144
00:08:53,080 --> 00:08:54,000
 like safe.

145
00:08:54,000 --> 00:08:57,000
 And as soon as we start to feel safe we let our guard down

146
00:08:57,000 --> 00:08:59,000
 and it all just explodes.

147
00:08:59,000 --> 00:09:02,000
 Or something's really fun, we like it and so we're happy.

148
00:09:02,000 --> 00:09:03,600
 And then someone comes and says or does or something

149
00:09:03,600 --> 00:09:06,000
 happens that's unpleasant

150
00:09:06,000 --> 00:09:12,000
 and right away we flip out or we chase after her or so on.

151
00:09:12,000 --> 00:09:14,000
 This isn't the right way to deal with things.

152
00:09:14,000 --> 00:09:18,000
 This isn't what we should be looking to do.

153
00:09:18,000 --> 00:09:21,000
 It works, it works on a temporary basis.

154
00:09:21,000 --> 00:09:24,000
 Sometimes even meditators have to do this if you're not

155
00:09:24,000 --> 00:09:27,000
 strong enough to really just face it.

156
00:09:27,000 --> 00:09:30,000
 You have to repress it at first.

157
00:09:30,000 --> 00:09:33,000
 But it doesn't solve anything.

158
00:09:33,000 --> 00:09:38,000
 It just takes it away temporarily.

159
00:09:38,000 --> 00:09:41,200
 Another way of dealing with it is to, as I said, to chase

160
00:09:41,200 --> 00:09:42,000
 after it.

161
00:09:42,000 --> 00:09:44,000
 Let it out.

162
00:09:44,000 --> 00:09:47,000
 When you're angry at someone let them know, just get angry.

163
00:09:47,000 --> 00:09:50,000
 No use bottling it up, that's what they say.

164
00:09:50,000 --> 00:09:53,000
 I agree there's no use bottling it up.

165
00:09:53,000 --> 00:09:56,740
 For sure we have to understand this but it doesn't mean

166
00:09:56,740 --> 00:10:00,000
 that the opposite is correct either.

167
00:10:00,000 --> 00:10:04,810
 That we should chase after and cultivate these, at least

168
00:10:04,810 --> 00:10:06,000
 brain activity.

169
00:10:06,000 --> 00:10:09,600
 If you don't want to talk about the mind just talk about

170
00:10:09,600 --> 00:10:11,000
 brain activity.

171
00:10:11,000 --> 00:10:15,110
 Scientists are perfectly aware how addiction works in terms

172
00:10:15,110 --> 00:10:16,000
 of the brain,

173
00:10:16,000 --> 00:10:19,000
 how the chemical reactions in the brain go.

174
00:10:19,000 --> 00:10:23,000
 The more you get the less pleasant it is.

175
00:10:23,000 --> 00:10:34,000
 The more stretched or worn down become the processes.

176
00:10:34,000 --> 00:10:36,000
 In the beginning it's very pleasurable to get what you want

177
00:10:36,000 --> 00:10:38,000
 but as you get it again and again

178
00:10:38,000 --> 00:10:41,480
 it takes more and more of it to really stimulate the

179
00:10:41,480 --> 00:10:43,000
 chemical reactions.

180
00:10:43,000 --> 00:10:48,000
 It's less and less pleasure for more and more work.

181
00:10:48,000 --> 00:10:53,780
 That's why we always need something new, something more

182
00:10:53,780 --> 00:10:57,000
 exciting.

183
00:10:57,000 --> 00:10:59,000
 Anger I suppose works in a different way.

184
00:10:59,000 --> 00:11:05,000
 I haven't really studied how the brain deals with anger but

185
00:11:05,000 --> 00:11:11,000
 it's just in the same way.

186
00:11:11,000 --> 00:11:15,000
 It's changing the way the brain works, so the brain reacts.

187
00:11:15,000 --> 00:11:18,000
 We're just talking about the brain and of course the mind

188
00:11:18,000 --> 00:11:20,000
 is very closely interrelated with the brain.

189
00:11:20,000 --> 00:11:22,640
 You can just as easily talk about one as talk about the

190
00:11:22,640 --> 00:11:23,000
 other.

191
00:11:23,000 --> 00:11:26,520
 Not that they're the same thing but that they're very

192
00:11:26,520 --> 00:11:28,000
 closely related.

193
00:11:28,000 --> 00:11:32,300
 The way it works is of course you get angry and of course

194
00:11:32,300 --> 00:11:35,000
 it's going to change the way the brain is structured.

195
00:11:35,000 --> 00:11:42,570
 It's going to scar or it's going to leave its scratch on

196
00:11:42,570 --> 00:11:45,000
 the brain and on the mind,

197
00:11:45,000 --> 00:11:52,000
 on the reality which is the consciousness.

198
00:11:52,000 --> 00:11:55,860
 It's going to change the chain reactions of events that are

199
00:11:55,860 --> 00:11:58,000
 going to arise in the future.

200
00:11:58,000 --> 00:12:02,000
 It's building up tendencies to put it simply.

201
00:12:02,000 --> 00:12:07,260
 The more we act on these impulses, the more they become a

202
00:12:07,260 --> 00:12:08,000
 part of us,

203
00:12:08,000 --> 00:12:10,000
 the more they become a part of who we are.

204
00:12:10,000 --> 00:12:13,300
 If you're ever killed, killing is very difficult in the

205
00:12:13,300 --> 00:12:14,000
 beginning.

206
00:12:14,000 --> 00:12:17,830
 I think a lot of these things are drinking alcohol is very

207
00:12:17,830 --> 00:12:20,000
 difficult in the beginning.

208
00:12:20,000 --> 00:12:27,020
 Sexual intercourse is sometimes quite, in some ways,

209
00:12:27,020 --> 00:12:29,000
 unpleasant in the beginning.

210
00:12:29,000 --> 00:12:33,000
 Because of how disgusting the body is and so on.

211
00:12:33,000 --> 00:12:38,000
 But it gets to be quite different after a while.

212
00:12:38,000 --> 00:12:41,740
 Our taste in music, sometimes you listen to something the

213
00:12:41,740 --> 00:12:44,000
 first time and you don't like it.

214
00:12:44,000 --> 00:12:48,000
 As you listen to it, it becomes more agreeable.

215
00:12:48,000 --> 00:12:55,000
 Your mind gets used to it and it knows how to react.

216
00:12:55,000 --> 00:12:59,000
 So it gives rise to either pleasure or pain.

217
00:12:59,000 --> 00:13:01,000
 It's used to reacting in that way.

218
00:13:01,000 --> 00:13:04,000
 So it's very reactionary as a result of building up

219
00:13:04,000 --> 00:13:05,000
 tendencies.

220
00:13:05,000 --> 00:13:12,000
 So this isn't a good way to deal with anxiety.

221
00:13:12,000 --> 00:13:15,000
 In meditation we're going to try to look and see things

222
00:13:15,000 --> 00:13:17,000
 simply for what they are.

223
00:13:17,000 --> 00:13:19,680
 When you get angry, when you get greedy, even when you

224
00:13:19,680 --> 00:13:21,000
 haven't gotten to that stage yet

225
00:13:21,000 --> 00:13:26,000
 and you just see something, just to see it for what it is.

226
00:13:26,000 --> 00:13:28,390
 Not like see something and you like it and really hate

227
00:13:28,390 --> 00:13:31,000
 yourself for liking it and wanting it.

228
00:13:31,000 --> 00:13:36,800
 These stories about people coveting their neighbors' wives

229
00:13:36,800 --> 00:13:38,000
 or husbands

230
00:13:38,000 --> 00:13:42,000
 and then hating themselves for it.

231
00:13:42,000 --> 00:13:46,400
 People who are angry and get angry a lot and then get angry

232
00:13:46,400 --> 00:13:49,000
 at themselves because they're angry people.

233
00:13:49,000 --> 00:13:51,710
 Because they're spiteful people. People who are addicted to

234
00:13:51,710 --> 00:13:52,000
 shopping.

235
00:13:52,000 --> 00:13:54,000
 People who are addicted to eating.

236
00:13:54,000 --> 00:13:57,000
 And then they hate themselves for it.

237
00:13:57,000 --> 00:13:59,000
 They judge themselves for it.

238
00:13:59,000 --> 00:14:02,500
 Or else the opposite. You like something and then you think

239
00:14:02,500 --> 00:14:04,000
 it's good that you like it.

240
00:14:04,000 --> 00:14:08,010
 You're happy that you're so greedy. You're happy that you

241
00:14:08,010 --> 00:14:09,000
 can get what you want.

242
00:14:09,000 --> 00:14:14,310
 You think it's a good thing. So you cultivate it more and

243
00:14:14,310 --> 00:14:17,000
 more and more.

244
00:14:17,000 --> 00:14:27,000
 So we follow in these cycles of repression and release.

245
00:14:27,000 --> 00:14:31,880
 Going back and forth sometimes and developing them to quite

246
00:14:31,880 --> 00:14:33,000
 extremes.

247
00:14:33,000 --> 00:14:36,410
 In meditation we're just trying to look and see things as

248
00:14:36,410 --> 00:14:37,000
 they are.

249
00:14:37,000 --> 00:14:40,000
 We're trying to come to understand things.

250
00:14:40,000 --> 00:14:43,070
 Understand how this works and understand reality in a

251
00:14:43,070 --> 00:14:46,000
 clearer way than we normally would.

252
00:14:46,000 --> 00:14:52,650
 As we look we're going to see that first of all the objects

253
00:14:52,650 --> 00:14:56,000
 of the sense there's nothing in them that is intrinsically

254
00:14:56,000 --> 00:14:58,000
 desirable or undesirable.

255
00:14:58,000 --> 00:15:01,140
 There's nothing that you could ever see that would be

256
00:15:01,140 --> 00:15:03,000
 intrinsically beautiful.

257
00:15:03,000 --> 00:15:06,100
 What would it mean that something is intrinsically

258
00:15:06,100 --> 00:15:07,000
 beautiful?

259
00:15:07,000 --> 00:15:10,000
 What does it mean to say that a flower is beautiful?

260
00:15:10,000 --> 00:15:16,000
 Or a body, a human body is beautiful? Or ugly?

261
00:15:16,000 --> 00:15:19,000
 It's meaningless. It means nothing.

262
00:15:19,000 --> 00:15:22,640
 It means that we've developed these tendencies from life to

263
00:15:22,640 --> 00:15:25,000
 life to believe this.

264
00:15:25,000 --> 00:15:27,000
 Convincing ourselves of it.

265
00:15:27,000 --> 00:15:30,000
 You can see this happening as I said even in this life.

266
00:15:30,000 --> 00:15:32,590
 People who take their first beer and hate it and as they

267
00:15:32,590 --> 00:15:38,000
 get used to drinking beer it becomes a wonderful thing.

268
00:15:38,000 --> 00:15:40,000
 We train ourselves.

269
00:15:40,000 --> 00:15:47,000
 We fool ourselves into thinking that this is a good thing.

270
00:15:47,000 --> 00:15:51,000
 We have pain and we convince ourselves that pain is bad.

271
00:15:51,000 --> 00:15:54,360
 Doctors tell us that pain is your body's way of telling you

272
00:15:54,360 --> 00:15:56,000
 that something is wrong.

273
00:15:56,000 --> 00:16:00,240
 Bodies don't have ways of telling anything. Bodies just

274
00:16:00,240 --> 00:16:01,000
 react.

275
00:16:01,000 --> 00:16:03,000
 They just have chain reactions.

276
00:16:03,000 --> 00:16:06,220
 If anything it's our reaction to pain that has created this

277
00:16:06,220 --> 00:16:08,000
 body and made it the way it is.

278
00:16:08,000 --> 00:16:13,360
 So instead of being something, really what you would expect

279
00:16:13,360 --> 00:16:16,000
 us to be, you'd expect us to be balls of light.

280
00:16:16,000 --> 00:16:17,000
 It makes more sense.

281
00:16:17,000 --> 00:16:21,000
 Why do we have ten fingers, ten toes?

282
00:16:21,000 --> 00:16:24,250
 And all of these funny organs and so on that are all

283
00:16:24,250 --> 00:16:28,000
 imperfect and all like they were just patched together.

284
00:16:28,000 --> 00:16:33,640
 If there is a God we certainly weren't made in his image or

285
00:16:33,640 --> 00:16:35,000
 her image.

286
00:16:35,000 --> 00:16:41,000
 Either that or it's a pretty lousy God.

287
00:16:41,000 --> 00:16:45,090
 And so evolution has done wonders in clearing this up and

288
00:16:45,090 --> 00:16:48,000
 saying how things came physically.

289
00:16:48,000 --> 00:16:52,750
 We're just sort of adapting that to what we can see is real

290
00:16:52,750 --> 00:16:55,000
, what's really happening.

291
00:16:55,000 --> 00:16:59,220
 That slowly, slowly we're patching together and piecing

292
00:16:59,220 --> 00:17:01,000
 together all of this.

293
00:17:01,000 --> 00:17:03,770
 There's nothing that's intrinsically good or intrinsically

294
00:17:03,770 --> 00:17:04,000
 bad.

295
00:17:04,000 --> 00:17:08,000
 And so we're going to see that. We can't see it normally.

296
00:17:08,000 --> 00:17:11,920
 When we see something we right away make a decision whether

297
00:17:11,920 --> 00:17:13,000
 it's good or bad.

298
00:17:13,000 --> 00:17:16,360
 And that decision is totally meaningless, totally based on

299
00:17:16,360 --> 00:17:23,000
 nothing, based on our own idea at the time.

300
00:17:23,000 --> 00:17:27,000
 We could like something one day and hate it the next.

301
00:17:27,000 --> 00:17:30,000
 Sometimes it's based on function.

302
00:17:30,000 --> 00:17:34,090
 We say this is beautiful, this is good because it works,

303
00:17:34,090 --> 00:17:38,280
 like it's a good chair or a good car or so on because it

304
00:17:38,280 --> 00:17:40,000
 runs well and so on.

305
00:17:40,000 --> 00:17:42,870
 But these are still just value judgments. It's not good for

306
00:17:42,870 --> 00:17:44,000
 anything really.

307
00:17:44,000 --> 00:17:46,910
 In the end everything is worthless. There's nothing that is

308
00:17:46,910 --> 00:17:48,000
 worth anything in the world.

309
00:17:48,000 --> 00:17:52,240
 Not you, not I, not any experience that has any intrinsic

310
00:17:52,240 --> 00:17:53,000
 worth.

311
00:17:53,000 --> 00:17:56,220
 It just comes and goes. Worth is not something you can

312
00:17:56,220 --> 00:17:59,000
 speak of. Just is.

313
00:17:59,000 --> 00:18:03,390
 And so in meditation we're coming to see that. We're coming

314
00:18:03,390 --> 00:18:07,000
 to see that that's the way it is.

315
00:18:07,000 --> 00:18:09,460
 And the way I always recommend to do that is just remind

316
00:18:09,460 --> 00:18:10,000
 yourself.

317
00:18:10,000 --> 00:18:13,190
 This word sati which we translate as mindfulness, it just

318
00:18:13,190 --> 00:18:14,000
 means to remind yourself.

319
00:18:14,000 --> 00:18:20,340
 It doesn't mean mindfulness. Sati means remembrance or that

320
00:18:20,340 --> 00:18:23,500
 which you use to remember, that which you use to remind

321
00:18:23,500 --> 00:18:28,000
 yourself, or the act of remembering.

322
00:18:28,000 --> 00:18:34,000
 When you remind yourself of what you've forgotten, that

323
00:18:34,000 --> 00:18:38,600
 this is pain or this is seeing or this is liking or this is

324
00:18:38,600 --> 00:18:43,000
 anger or all of the myriad of different experiences.

325
00:18:43,000 --> 00:18:48,000
 You just remind yourself that's pain.

326
00:18:48,000 --> 00:18:52,360
 And your mind gets closer to the reality and gets to see it

327
00:18:52,360 --> 00:18:55,000
 clearer than it used to see it.

328
00:18:55,000 --> 00:18:59,400
 Then you see how it happens when a sensation arises or an

329
00:18:59,400 --> 00:19:02,330
 experience arises, then there's a happy feeling or an

330
00:19:02,330 --> 00:19:03,000
 unhappy feeling.

331
00:19:03,000 --> 00:19:06,900
 And this is where we go wrong. When a pleasant feeling

332
00:19:06,900 --> 00:19:10,000
 comes up we like it, we hold on to it.

333
00:19:10,000 --> 00:19:13,000
 We want it. We want more of it.

334
00:19:13,000 --> 00:19:17,000
 And so we give rise to this cycle of addiction.

335
00:19:17,000 --> 00:19:21,870
 When something unpleasant comes up, then we right away don

336
00:19:21,870 --> 00:19:23,000
't like it.

337
00:19:23,000 --> 00:19:29,000
 Give rise to this cycle of hatred.

338
00:19:29,000 --> 00:19:30,940
 And we have so many cycles like this. We have the cycles of

339
00:19:30,940 --> 00:19:34,250
 depression, cycles of fear and anxiety, people who get

340
00:19:34,250 --> 00:19:37,230
 anxious and then get worried that they're too anxious and

341
00:19:37,230 --> 00:19:39,000
 so get even more anxious.

342
00:19:39,000 --> 00:19:43,000
 People who are awake all night trying to get to sleep

343
00:19:43,000 --> 00:19:46,570
 because the more stressed they are about not being able to

344
00:19:46,570 --> 00:19:49,560
 sleep, then they get stressed about being stressed and how

345
00:19:49,560 --> 00:19:51,000
 to not be stressed.

346
00:19:51,000 --> 00:19:56,350
 And so they stress about trying to not be stressed and so

347
00:19:56,350 --> 00:19:57,000
 on.

348
00:19:57,000 --> 00:20:02,990
 All we're doing here is coming to see things as they are.

349
00:20:02,990 --> 00:20:07,250
 We're establishing what we understand to be real and then

350
00:20:07,250 --> 00:20:11,000
 we're looking at that, we're working on that.

351
00:20:11,000 --> 00:20:14,380
 We're working with that as our base. We're not going to get

352
00:20:14,380 --> 00:20:18,330
 into theories or ideas, not even Buddhism, not even this or

353
00:20:18,330 --> 00:20:19,000
 that.

354
00:20:19,000 --> 00:20:22,690
 Just it's here, it's now. You don't have to decide whether

355
00:20:22,690 --> 00:20:26,000
 it's real or not. It's there, you're experiencing it.

356
00:20:26,000 --> 00:20:29,070
 All we're trying to do is come to see it clearer than we

357
00:20:29,070 --> 00:20:30,000
 normally do.

358
00:20:30,000 --> 00:20:45,000
 It's this, okay, let's look and see. What is it really?

359
00:20:45,000 --> 00:20:48,890
 So in one way this mantra that we use is a way of focusing

360
00:20:48,890 --> 00:20:55,010
 on reality. In another way it's a way of straightening out

361
00:20:55,010 --> 00:20:56,000
 the mind.

362
00:20:56,000 --> 00:21:01,790
 Because the mind sees but it doesn't say that seeing, it

363
00:21:01,790 --> 00:21:07,590
 says that's him or her or that or this, good or bad, me or

364
00:21:07,590 --> 00:21:10,000
 mine, us and them.

365
00:21:10,000 --> 00:21:14,910
 Totally forgotten that that's just seeing. When you hear

366
00:21:14,910 --> 00:21:20,000
 something it's wonderful music or evil person yelling at me

367
00:21:20,000 --> 00:21:25,000
, I don't deserve to be yelled at, we've forgotten already.

368
00:21:25,000 --> 00:21:28,000
 Forgotten totally what's going on.

369
00:21:28,000 --> 00:21:31,070
 So just reminding ourselves, straightening out our mind

370
00:21:31,070 --> 00:21:34,250
 saying, look, you're all out of shape, you're all out of wh

371
00:21:34,250 --> 00:21:42,000
ack here. That's not real. That's not what's going on here.

372
00:21:42,000 --> 00:21:45,300
 And you can do it with anything and as you do it more and

373
00:21:45,300 --> 00:21:48,000
 more, get better at it and better at it.

374
00:21:48,000 --> 00:21:51,350
 You come to see so many of the wrong ways we deal with

375
00:21:51,350 --> 00:21:55,200
 reality. We get stressed, we get tense, we try to force

376
00:21:55,200 --> 00:21:59,300
 things. Even just using this mantra it's so often in the

377
00:21:59,300 --> 00:22:02,690
 beginning people get headaches and they say it's just not

378
00:22:02,690 --> 00:22:08,000
 for them because it's like forcing things.

379
00:22:08,000 --> 00:22:11,290
 This is a part, this is the reason why we're doing this is

380
00:22:11,290 --> 00:22:14,600
 so that we can see the way we react to things. Even

381
00:22:14,600 --> 00:22:18,000
 meditation we react totally wrongly to it.

382
00:22:18,000 --> 00:22:21,240
 We tell people to breathe and watch the stomach rising and

383
00:22:21,240 --> 00:22:24,170
 falling. And so right away they're trying to breathe deep,

384
00:22:24,170 --> 00:22:26,600
 they're trying to breathe perfectly smooth and they're

385
00:22:26,600 --> 00:22:29,920
 trying to force their breath and the more they force it the

386
00:22:29,920 --> 00:22:34,000
 more suffering comes to them, the more stressful it is.

387
00:22:34,000 --> 00:22:37,510
 Then they blame the meditation and say this sucks, this is

388
00:22:37,510 --> 00:22:41,350
 terrible, it's not pleasant, it's uncomfortable. What

389
00:22:41,350 --> 00:22:44,000
 really sucks is our attitude towards it.

390
00:22:44,000 --> 00:22:50,180
 Then we're forcing, controlling, stressing, needing it to

391
00:22:50,180 --> 00:22:55,300
 be in a certain way and not able to accept it for what it

392
00:22:55,300 --> 00:22:56,000
 is.

393
00:22:56,000 --> 00:22:59,180
 Even when we sit in meditation we get headaches, so what?

394
00:22:59,180 --> 00:23:03,030
 What's wrong with a headache? What's wrong is our stress

395
00:23:03,030 --> 00:23:06,000
 that comes from having a headache.

396
00:23:06,000 --> 00:23:10,440
 People sit in meditation, they have a backache and they

397
00:23:10,440 --> 00:23:14,980
 have all these theories about how you have to sit like this

398
00:23:14,980 --> 00:23:18,580
 or like that and then your back will be perfect and there

399
00:23:18,580 --> 00:23:20,000
 will be no pain and so on.

400
00:23:20,000 --> 00:23:24,070
 It's really ridiculous, your back can't be perfect, your

401
00:23:24,070 --> 00:23:28,000
 back's just going to get worse and worse until you die.

402
00:23:28,000 --> 00:23:31,560
 That's not what we're here for. I've sat in meditation for

403
00:23:31,560 --> 00:23:36,120
 eight years with my back hunched over, my back's not dead

404
00:23:36,120 --> 00:23:37,000
 yet.

405
00:23:37,000 --> 00:23:42,020
 My teacher's 86 this year and he's been doing it I guess

406
00:23:42,020 --> 00:23:48,110
 for 50 maybe 60, probably over 60 years with his back hun

407
00:23:48,110 --> 00:23:49,000
ched.

408
00:23:49,000 --> 00:23:51,690
 Not hunched, it just means sitting naturally, not worried

409
00:23:51,690 --> 00:23:54,180
 about your posture, not worried about anything, just

410
00:23:54,180 --> 00:23:55,000
 watching it.

411
00:23:55,000 --> 00:24:05,700
 Oh now there's pain, oh interesting, it'd be fine. This is

412
00:24:05,700 --> 00:24:11,260
 the practice of insight, to see clearly, not to judge, not

413
00:24:11,260 --> 00:24:14,000
 to need, not to worry, not to stress.

414
00:24:14,000 --> 00:24:19,550
 Everything just works itself out. You come back to nature,

415
00:24:19,550 --> 00:24:24,000
 you come back to now, you come back to reality.

416
00:24:24,000 --> 00:24:30,000
 There's nothing in the world that we cling to.

417
00:24:31,000 --> 00:24:36,000
 [silence]

418
00:24:38,000 --> 00:24:44,000
 [silence]

419
00:24:46,000 --> 00:24:52,000
 [silence]

420
00:25:12,000 --> 00:25:19,760
 It's kind of like untying a knot. You have this big knot

421
00:25:19,760 --> 00:25:24,760
 and you look at it and you say, "Wow that's a mess, there's

422
00:25:24,760 --> 00:25:27,000
 this big knot there."

423
00:25:27,000 --> 00:25:31,340
 But as you untie it, it just disappears. And when you've

424
00:25:31,340 --> 00:25:36,590
 got it totally untied there's nothing there, it's just gone

425
00:25:36,590 --> 00:25:37,000
.

426
00:25:37,000 --> 00:25:40,710
 Really all of our problems and all of our stresses, all of

427
00:25:40,710 --> 00:25:44,460
 our worries and all of our cares, they're all just like the

428
00:25:44,460 --> 00:25:45,000
 knot.

429
00:25:45,000 --> 00:25:52,940
 They're not really even there. They're just, all they are

430
00:25:52,940 --> 00:25:58,000
 is a kink, all they are is a knot.

431
00:25:58,000 --> 00:26:03,030
 We have all these ideas of who we are, what we are, what we

432
00:26:03,030 --> 00:26:08,560
 have, what we deal with, places, people, things, ideas,

433
00:26:08,560 --> 00:26:12,000
 concepts, views, everything, belongings.

434
00:26:12,000 --> 00:26:16,700
 None of them exist, they're not even real. It's all just

435
00:26:16,700 --> 00:26:23,000
 knots. Clinging, liking or disliking.

436
00:26:23,000 --> 00:26:33,060
 Holding up as good, throwing down as bad. Pulling in as me,

437
00:26:33,060 --> 00:26:38,000
 pushing out as you.

438
00:26:38,000 --> 00:26:43,560
 In the end it's all just experience, it's all just reality.

439
00:26:43,560 --> 00:26:51,680
 It's just one moment of experience. We've created so many

440
00:26:51,680 --> 00:26:53,000
 knots.

441
00:26:53,000 --> 00:26:55,270
 And then we get worried and stressed about them and say, "

442
00:26:55,270 --> 00:26:57,540
Oh look at that big knot, it's not even there, it's just a k

443
00:26:57,540 --> 00:27:02,000
ink, it's just the rope."

444
00:27:02,000 --> 00:27:08,000
 Got it out of shape. All bunched up, all messed up.

445
00:27:08,000 --> 00:27:16,000
 I just straighten it out. Straighten it out and keep going.

446
00:27:16,000 --> 00:27:28,540
 So in that sense, nirvana or nirvana, the goal of the

447
00:27:28,540 --> 00:27:40,120
 practice is not really anything special. It's not really

448
00:27:40,120 --> 00:27:42,000
 anything mysterious.

449
00:27:42,000 --> 00:27:47,040
 It's just when all the knots are gone. I mean it is

450
00:27:47,040 --> 00:27:51,000
 something profound because there's nothing left.

451
00:27:51,000 --> 00:27:57,800
 At the experience of nirvana it's totally unkinked. There's

452
00:27:57,800 --> 00:28:04,000
 nothing, there's no friction, there's no stress.

453
00:28:04,000 --> 00:28:06,970
 There's no seeing, there's no hearing, there's no smelling,

454
00:28:06,970 --> 00:28:09,800
 there's no tasting, there's no feeling, there's no thinking

455
00:28:09,800 --> 00:28:10,000
.

456
00:28:10,000 --> 00:28:22,000
 Suddenly it just ceased.

457
00:28:22,000 --> 00:28:25,810
 And it might even be momentary. The experience of nirvana

458
00:28:25,810 --> 00:28:34,000
 can be just a fleeting moment, but it's a profound moment.

459
00:28:34,000 --> 00:28:40,240
 Something that changes a person to the core, changes the

460
00:28:40,240 --> 00:28:43,000
 way the person thinks.

461
00:28:43,000 --> 00:28:56,910
 It's like a cut. Finally the bonds are broken, like there's

462
00:28:56,910 --> 00:29:07,000
 a gap, a gap in the string or a gap in the chain.

463
00:29:07,000 --> 00:29:10,770
 And that profoundly shakes up the way the person looks at

464
00:29:10,770 --> 00:29:19,000
 the world because before we looked at the world as being

465
00:29:19,000 --> 00:29:26,480
 full of things and concepts and people and selves and souls

466
00:29:26,480 --> 00:29:29,000
 and egos.

467
00:29:29,000 --> 00:29:38,000
 Once you experience the cessation of stress, the cessation

468
00:29:38,000 --> 00:29:45,130
 of all of this, it all just clears up and it becomes just

469
00:29:45,130 --> 00:29:49,000
 experience, just reality.

470
00:29:49,000 --> 00:29:54,060
 And then it all bunches up again and you're back to liking

471
00:29:54,060 --> 00:30:00,070
 or disliking, but there's a gap there. There's a memory of

472
00:30:00,070 --> 00:30:04,000
 this. It's left its mark as well.

473
00:30:04,000 --> 00:30:13,380
 And you can't ever look at things as permanent, as stable

474
00:30:13,380 --> 00:30:21,000
 as me or mine. It just doesn't make sense anymore.

475
00:30:21,000 --> 00:30:34,670
 The experience of total cessation. And this is even more

476
00:30:34,670 --> 00:30:41,590
 true the more you practice and the more it comes that a

477
00:30:41,590 --> 00:30:47,000
 person is able to experience this state.

478
00:30:47,000 --> 00:30:56,870
 To the point where all the kings, there's so many gaps,

479
00:30:56,870 --> 00:31:03,000
 there's so many interruptions.

480
00:31:03,000 --> 00:31:11,030
 The whole thing just collapses and there's no more kinking.

481
00:31:11,030 --> 00:31:13,730
 The wisdom finally gains hold. The understanding that this

482
00:31:13,730 --> 00:31:16,000
 is the way it is finally gains hold.

483
00:31:16,000 --> 00:31:18,950
 The mind finally changes from still believing, still

484
00:31:18,950 --> 00:31:22,270
 seeking, still wishing, still hoping that there's going to

485
00:31:22,270 --> 00:31:26,000
 be something that's stable, something that's permanent,

486
00:31:26,000 --> 00:31:30,000
 something that's satisfying, something that we can control.

487
00:31:30,000 --> 00:31:36,790
 It finally accepts the fact that there's nothing in the

488
00:31:36,790 --> 00:31:43,000
 world that's really wonderful and beautiful.

489
00:31:43,000 --> 00:31:50,630
 And then they say this is the final graduation. This is

490
00:31:50,630 --> 00:31:56,150
 finally becoming free from illusion. We finally understand

491
00:31:56,150 --> 00:31:58,000
 reality as it is.

492
00:31:58,000 --> 00:32:02,070
 It's very simple. It's just seeing things as they are.

493
00:32:02,070 --> 00:32:08,710
 There's no special reality or special experience. It just

494
00:32:08,710 --> 00:32:12,000
 is reality, just is experience.

495
00:32:12,000 --> 00:32:17,330
 And the only single problem is our delusion, our

496
00:32:17,330 --> 00:32:23,580
 misunderstanding of things. The fact that we can't see

497
00:32:23,580 --> 00:32:29,000
 things as they are. We can't see things clearly.

498
00:32:29,000 --> 00:32:55,000
 [silence]

499
00:32:55,000 --> 00:32:58,760
 I think that's enough for today. If you like, you're

500
00:32:58,760 --> 00:33:03,210
 welcome to just continue meditating here. If anyone has any

501
00:33:03,210 --> 00:33:08,000
 questions, you're welcome to ask. Thank you all for coming.

