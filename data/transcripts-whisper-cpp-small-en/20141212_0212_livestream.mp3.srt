1
00:00:00,000 --> 00:00:07,000
 Okay, I believe I'm broadcasting.

2
00:00:07,000 --> 00:00:15,000
 I just had some technical difficulties, as usual.

3
00:00:15,000 --> 00:00:20,000
 So today I'm broadcasting from Bangkok.

4
00:00:20,000 --> 00:00:28,000
 Yesterday I arrived late in the night.

5
00:00:28,000 --> 00:00:33,000
 And I'll be here until I go to Sri Lanka.

6
00:00:33,000 --> 00:00:37,000
 So yesterday we...

7
00:00:37,000 --> 00:00:42,000
 I was talking about the three characteristics

8
00:00:42,000 --> 00:00:57,000
 and how they represent a fixed aspect of nature

9
00:00:57,000 --> 00:01:04,000
 and reality that prevents us from...

10
00:01:04,000 --> 00:01:15,000
 or that leads us to reject the idea that

11
00:01:15,000 --> 00:01:22,000
 one can find happiness arbitrarily where everyone chooses,

12
00:01:22,000 --> 00:01:25,000
 so that different people could take different paths

13
00:01:25,000 --> 00:01:37,000
 and find happiness in different ways.

14
00:01:37,000 --> 00:01:40,000
 In the end it really comes down to the fact that

15
00:01:40,000 --> 00:01:50,000
 happiness has to be found outside of subjectivity,

16
00:01:50,000 --> 00:01:55,000
 or outside of circumstance.

17
00:01:55,000 --> 00:02:03,000
 True happiness is niramisa, which means without an object.

18
00:02:03,000 --> 00:02:12,000
 Happiness that takes an object is...

19
00:02:12,000 --> 00:02:19,000
 is not truly satisfying.

20
00:02:19,000 --> 00:02:23,000
 That leads to true happiness.

21
00:02:23,000 --> 00:02:27,000
 That kind of happiness is problematic

22
00:02:27,000 --> 00:02:31,000
 for the reasons that we're discussing.

23
00:02:31,000 --> 00:02:35,820
 So I believe I had gotten most of the way through talking

24
00:02:35,820 --> 00:02:37,000
 about impermanence.

25
00:02:37,000 --> 00:02:39,000
 How?

26
00:02:39,000 --> 00:02:45,000
 Because things are impermanent,

27
00:02:45,000 --> 00:02:51,000
 they're constantly disappointing

28
00:02:51,000 --> 00:02:54,160
 because the nature of clinging, the nature of desire or

29
00:02:54,160 --> 00:02:56,000
 attachment to things,

30
00:02:56,000 --> 00:03:04,000
 liking of experiences is to cling, is to expect,

31
00:03:04,000 --> 00:03:12,000
 is to group experiences together

32
00:03:12,000 --> 00:03:17,000
 and cultivate partiality for those groups of experiences,

33
00:03:17,000 --> 00:03:22,000
 almost like trying to hold on to this immovable force.

34
00:03:22,000 --> 00:03:34,000
 And because reality follows its own laws and is ever-

35
00:03:34,000 --> 00:03:36,000
changing,

36
00:03:36,000 --> 00:03:44,000
 you can't hope to satisfy.

37
00:03:44,000 --> 00:03:47,000
 And so you can't truly be happiness.

38
00:03:47,000 --> 00:03:55,420
 And so this leads us to the second characteristic of what

39
00:03:55,420 --> 00:03:57,000
 we call suffering.

40
00:03:57,000 --> 00:04:08,450
 But the idea of impermanence just to round it up, to

41
00:04:08,450 --> 00:04:16,000
 summarize.

42
00:04:16,000 --> 00:04:26,000
 It's the fact that things are constantly changing,

43
00:04:26,000 --> 00:04:28,000
 changing the way of our happiness.

44
00:04:28,000 --> 00:04:37,000
 And it's because of our inability, our rigidness,

45
00:04:37,000 --> 00:04:48,000
 our lack of flexibility that we can't dwell in happiness,

46
00:04:48,000 --> 00:04:52,000
 that our happiness can't be based on objects.

47
00:04:52,000 --> 00:04:58,980
 But because of our inability to find happiness apart from

48
00:04:58,980 --> 00:05:03,000
 the objects of experience,

49
00:05:03,000 --> 00:05:17,000
 we therefore suffer.

50
00:05:17,000 --> 00:05:31,390
 And so for the ordinary idea that happiness is up to the

51
00:05:31,390 --> 00:05:32,000
 individual,

52
00:05:32,000 --> 00:05:37,000
 everyone has to find their own happiness in the world,

53
00:05:37,000 --> 00:05:56,000
 to each their own, and this doesn't really work.

54
00:05:56,000 --> 00:06:00,000
 Mostly we become, the more we cling to things,

55
00:06:00,000 --> 00:06:04,000
 the more inflexible we become, the more rigid we become.

56
00:06:04,000 --> 00:06:07,560
 And as a result, the more we suffer because of our

57
00:06:07,560 --> 00:06:08,000
 expectations

58
00:06:08,000 --> 00:06:13,400
 and our needs really, we come to rely upon certain

59
00:06:13,400 --> 00:06:14,000
 experiences

60
00:06:14,000 --> 00:06:20,010
 and we're dissatisfied or bored when they're not the way we

61
00:06:20,010 --> 00:06:21,000
 want.

62
00:06:21,000 --> 00:06:28,000
 So if we can become flexible,

63
00:06:28,000 --> 00:06:34,000
 give up the idea of finding a specific happiness,

64
00:06:34,000 --> 00:06:37,190
 in fact giving up the idea really of finding happiness or

65
00:06:37,190 --> 00:06:39,000
 seeking out happiness,

66
00:06:39,000 --> 00:06:43,000
 we find ourselves much more happy.

67
00:06:43,000 --> 00:06:49,000
 We realize that it's not about everyone finding happiness

68
00:06:49,000 --> 00:06:52,000
 in their specific circumstances.

69
00:06:52,000 --> 00:06:56,000
 It's not about finding happiness in whatever you do.

70
00:06:56,000 --> 00:07:00,340
 If life throws you a lemon, make lemonade, that's a bit

71
00:07:00,340 --> 00:07:04,000
 misleading, it's not correct.

72
00:07:04,000 --> 00:07:09,310
 It's not about finding lemonade, it's about in spite of the

73
00:07:09,310 --> 00:07:10,000
 lemons,

74
00:07:10,000 --> 00:07:17,000
 or not in spite of, but regardless of the experience,

75
00:07:17,000 --> 00:07:19,000
 finding happiness.

76
00:07:19,000 --> 00:07:25,000
 And so that's universal. True happiness is universal

77
00:07:25,000 --> 00:07:28,000
 in the sense that everyone finds it in the same way,

78
00:07:28,000 --> 00:07:33,000
 but it has nothing to do with their specific circumstances.

79
00:07:33,000 --> 00:07:40,050
 It has everything to do with their state of mind, their

80
00:07:40,050 --> 00:07:43,000
 peace of mind.

81
00:07:43,000 --> 00:07:44,000
 So that's the first one.

82
00:07:44,000 --> 00:07:51,000
 The second one, what we call suffering generally, dukkha,

83
00:07:51,000 --> 00:08:10,000
 refers really to the inherent nature of uselessness,

84
00:08:10,000 --> 00:08:18,000
 in reality of pointlessness or valuelessness.

85
00:08:18,000 --> 00:08:22,000
 So in general, when we talk about suffering,

86
00:08:22,000 --> 00:08:27,400
 the easiest way to understand it is in terms of imperman

87
00:08:27,400 --> 00:08:30,000
ence or non-self.

88
00:08:30,000 --> 00:08:31,000
 This is how the commentaries go about it.

89
00:08:31,000 --> 00:08:35,100
 You can understand suffering either through impermanence or

90
00:08:35,100 --> 00:08:36,000
 through non-self.

91
00:08:36,000 --> 00:08:43,000
 So something is suffering or unsatisfying or dukkha

92
00:08:43,000 --> 00:08:50,000
 because it's impermanent, because it can't last.

93
00:08:50,000 --> 00:08:58,000
 Or something is dukkha because it is uncontrollable,

94
00:08:58,000 --> 00:09:01,000
 but it's not amenable to one's wishes.

95
00:09:01,000 --> 00:09:05,810
 And so indirectly we understand suffering through the other

96
00:09:05,810 --> 00:09:07,000
 two characteristics.

97
00:09:07,000 --> 00:09:14,510
 If something is inconstant, is constantly arising and then

98
00:09:14,510 --> 00:09:16,000
 ceasing,

99
00:09:16,000 --> 00:09:21,000
 having arisen, it ceases in an instant,

100
00:09:21,000 --> 00:09:24,000
 or everything only lasting a moment,

101
00:09:24,000 --> 00:09:32,000
 then it can't possibly satisfy or provide any sort of

102
00:09:32,000 --> 00:09:39,000
 solace or refuge or contentment.

103
00:09:39,000 --> 00:09:47,000
 One's happiness has to be independent of the object.

104
00:09:47,000 --> 00:09:52,250
 If one's happiness is dependent on a specific experience of

105
00:09:52,250 --> 00:09:53,000
 seeing,

106
00:09:53,000 --> 00:09:56,490
 a specific type of seeing or hearing or smelling or tasting

107
00:09:56,490 --> 00:09:57,000
 or feeling or thinking,

108
00:09:57,000 --> 00:10:02,980
 if something specifically makes one happy, then because of

109
00:10:02,980 --> 00:10:04,000
 the impermanence,

110
00:10:04,000 --> 00:10:09,000
 it's only going to disappoint.

111
00:10:09,000 --> 00:10:11,000
 Basically saying the same thing as the first one.

112
00:10:11,000 --> 00:10:16,070
 The three characteristics are obviously tied closely

113
00:10:16,070 --> 00:10:17,000
 together,

114
00:10:17,000 --> 00:10:22,350
 and in a way there are different ways of looking at the

115
00:10:22,350 --> 00:10:25,000
 same thing or the same quality.

116
00:10:25,000 --> 00:10:29,230
 It's not like there are three subatomic particles called

117
00:10:29,230 --> 00:10:31,000
 impermanent suffering and non-self.

118
00:10:31,000 --> 00:10:36,000
 They're just three aspects of a single nature,

119
00:10:36,000 --> 00:10:41,120
 the nature of reality to arise and cease based on causes

120
00:10:41,120 --> 00:10:43,000
 and conditions.

121
00:10:43,000 --> 00:10:53,000
 But there are three aspects of this nature,

122
00:10:53,000 --> 00:10:57,000
 or three ways of looking at this nature,

123
00:10:57,000 --> 00:11:07,020
 three observations that can be made that are important

124
00:11:07,020 --> 00:11:10,000
 regarding one's happiness.

125
00:11:10,000 --> 00:11:17,000
 Suffering can also be understood just intrinsically,

126
00:11:17,000 --> 00:11:21,000
 the characteristic of suffering.

127
00:11:21,000 --> 00:11:25,000
 It goes actually deeper than simply it's impermanent,

128
00:11:25,000 --> 00:11:29,000
 therefore it's unsatisfying or it's uncontrollable,

129
00:11:29,000 --> 00:11:36,230
 and therefore it's unsatisfying or it's unpleasant or not a

130
00:11:36,230 --> 00:11:41,000
 cause of true happiness.

131
00:11:41,000 --> 00:11:48,230
 The true point of dukkha is that the object is inherently

132
00:11:48,230 --> 00:11:50,000
 worthless,

133
00:11:50,000 --> 00:12:01,000
 inherently lacking in any benefit.

134
00:12:01,000 --> 00:12:05,000
 This is important, especially regarding...

135
00:12:05,000 --> 00:12:08,890
 All three of these characteristics are important regarding

136
00:12:08,890 --> 00:12:10,000
 their opposites.

137
00:12:10,000 --> 00:12:13,900
 So impermanence is what allows us to let go of the idea of

138
00:12:13,900 --> 00:12:15,000
 permanence,

139
00:12:15,000 --> 00:12:25,000
 of stability when we talk about impermanence.

140
00:12:25,000 --> 00:12:36,780
 It's to point out the problem with relying upon our

141
00:12:36,780 --> 00:12:38,000
 situation

142
00:12:38,000 --> 00:12:43,000
 to bring us happiness over the long term.

143
00:12:43,000 --> 00:12:48,020
 So our nice car and our nice house and so on can't really

144
00:12:48,020 --> 00:12:51,000
 bring us happiness

145
00:12:51,000 --> 00:12:54,650
 because it's unstable, and it's the reliance upon these

146
00:12:54,650 --> 00:12:55,000
 things,

147
00:12:55,000 --> 00:12:59,000
 thinking that these things are stable and lasting,

148
00:12:59,000 --> 00:13:03,000
 that eventually causes us great suffering.

149
00:13:03,000 --> 00:13:11,000
 Same goes with suffering.

150
00:13:11,000 --> 00:13:15,330
 The truth of suffering is meant to counteract the wrong

151
00:13:15,330 --> 00:13:18,000
 view of satisfaction

152
00:13:18,000 --> 00:13:23,000
 or contentment or happiness in experience,

153
00:13:23,000 --> 00:13:25,000
 trying to find happiness here or there.

154
00:13:25,000 --> 00:13:31,000
 So we seek out things that are...

155
00:13:31,000 --> 00:13:36,000
 things that we think are happiness in the world,

156
00:13:36,000 --> 00:13:39,000
 wanting to see nice things or hear nice things,

157
00:13:39,000 --> 00:13:45,250
 or even ideas or concepts, people, places, things that are

158
00:13:45,250 --> 00:13:46,000
 pleasing to us,

159
00:13:46,000 --> 00:13:48,000
 that we think are going to bring us happiness.

160
00:13:48,000 --> 00:13:52,000
 We think, "This brings me happiness."

161
00:13:52,000 --> 00:13:55,000
 And this is where the idea of, "To each their own" comes in

162
00:13:55,000 --> 00:13:55,000
,

163
00:13:55,000 --> 00:13:59,000
 everyone finds happiness in their own way.

164
00:13:59,000 --> 00:14:04,270
 But the teaching of suffering is a claim that this is not

165
00:14:04,270 --> 00:14:06,000
 actually a cause,

166
00:14:06,000 --> 00:14:09,000
 these things are not actually bringing happiness.

167
00:14:09,000 --> 00:14:12,000
 And this is the... it's quite a shock, I suppose.

168
00:14:12,000 --> 00:14:18,000
 It is quite a shock to hear this or to contemplate this

169
00:14:18,000 --> 00:14:21,000
 because quite clearly to us these things are happiness.

170
00:14:21,000 --> 00:14:23,580
 These things do bring me happiness, right? That's true,

171
00:14:23,580 --> 00:14:26,000
 right? It's real.

172
00:14:26,000 --> 00:14:30,490
 But some nagging doubt in the back of our minds always

173
00:14:30,490 --> 00:14:31,000
 exists

174
00:14:31,000 --> 00:14:34,000
 because we don't feel like we're getting happier.

175
00:14:34,000 --> 00:14:37,000
 I'm not more content the more I get this experience.

176
00:14:37,000 --> 00:14:43,040
 In fact, if I start to obsess over it and indulge in it

177
00:14:43,040 --> 00:14:44,000
 with greater frequency,

178
00:14:44,000 --> 00:14:49,000
 it actually becomes less pleasurable.

179
00:14:49,000 --> 00:14:52,000
 And so we have this question of why is that?

180
00:14:52,000 --> 00:14:54,450
 We never question whether this is actually bringing us

181
00:14:54,450 --> 00:14:55,000
 happiness,

182
00:14:55,000 --> 00:14:59,410
 but we don't usually, but we usually will ask, "Why? Why am

183
00:14:59,410 --> 00:15:01,000
 I not happier?"

184
00:15:01,000 --> 00:15:04,080
 "Here I am indulging in things that are clearly bringing me

185
00:15:04,080 --> 00:15:05,000
 happiness."

186
00:15:05,000 --> 00:15:11,000
 And the point is that... the answer is that it's delusion

187
00:15:11,000 --> 00:15:14,000
 that's telling you this is happiness.

188
00:15:14,000 --> 00:15:17,000
 It actually is wrong.

189
00:15:17,000 --> 00:15:20,490
 This is probably the scariest thing, the most shocking

190
00:15:20,490 --> 00:15:22,000
 thing about meditation practice

191
00:15:22,000 --> 00:15:27,000
 is to realize you actually are fundamentally wrong about

192
00:15:27,000 --> 00:15:29,000
 your own experience.

193
00:15:29,000 --> 00:15:36,010
 You can't even trust your own experience, your own ordinary

194
00:15:36,010 --> 00:15:37,000
 experience.

195
00:15:37,000 --> 00:15:44,000
 The non-meditative experience of reality is deluded,

196
00:15:44,000 --> 00:15:54,000
 is misguided, misapprehending the object.

197
00:15:54,000 --> 00:15:57,400
 Because when you look closer, when you examine, when you

198
00:15:57,400 --> 00:15:58,000
 meditate,

199
00:15:58,000 --> 00:16:02,000
 when you're mindful of the objects, truly mindful,

200
00:16:02,000 --> 00:16:06,000
 you see that it's not actually happiness.

201
00:16:06,000 --> 00:16:10,000
 There's not actually any happiness there.

202
00:16:10,000 --> 00:16:13,000
 Sure, there's a pleasure that arises in the mind,

203
00:16:13,000 --> 00:16:19,330
 but even that pleasure, that feeling of pleasure, it's not

204
00:16:19,330 --> 00:16:23,000
 valuable in any way.

205
00:16:23,000 --> 00:16:27,230
 When you're truly mindful, what happens is you lose the

206
00:16:27,230 --> 00:16:28,000
 liking, the liking disappears.

207
00:16:28,000 --> 00:16:31,000
 You realize there's no reason to like this.

208
00:16:31,000 --> 00:16:36,000
 There's nothing intrinsic about this that is likable.

209
00:16:36,000 --> 00:16:42,000
 It's there and then it's gone. That's it.

210
00:16:42,000 --> 00:16:47,810
 It really is completely arbitrary in our own minds what we

211
00:16:47,810 --> 00:16:50,000
 find desirable.

212
00:16:50,000 --> 00:16:56,790
 It's not intrinsic to the object. There's no object that is

213
00:16:56,790 --> 00:16:59,000
 desirable.

214
00:16:59,000 --> 00:17:03,000
 It's not a quality of an object to be desirable.

215
00:17:03,000 --> 00:17:06,000
 It's totally a product of our views and opinions,

216
00:17:06,000 --> 00:17:08,870
 and this is where we get the idea this is a source of

217
00:17:08,870 --> 00:17:10,000
 happiness for me, but it's not.

218
00:17:10,000 --> 00:17:13,180
 You are creating that in your mind. You're saying, "I'm

219
00:17:13,180 --> 00:17:17,000
 partial to this. I'm partial to that."

220
00:17:17,000 --> 00:17:22,480
 That distinction has to be made that the objects are

221
00:17:22,480 --> 00:17:25,000
 totally arbitrary.

222
00:17:25,000 --> 00:17:31,000
 What we choose to make us happy is there is no valid rhyme

223
00:17:31,000 --> 00:17:43,000
 or reason to our choosing.

224
00:17:43,000 --> 00:17:50,440
 If we are honest and clear in our minds, we'll realize that

225
00:17:50,440 --> 00:17:55,300
 a true understanding of reality has no use for liking, has

226
00:17:55,300 --> 00:17:57,000
 no use for desire.

227
00:17:57,000 --> 00:18:00,280
 In fact, of course, when we see that desire is the cause of

228
00:18:00,280 --> 00:18:03,000
 suffering because of impermanent suffering

229
00:18:03,000 --> 00:18:07,450
 and because the objects are impermanent, are without any

230
00:18:07,450 --> 00:18:10,000
 value and are uncontrollable,

231
00:18:10,000 --> 00:18:14,490
 we start to see that it's actually a pretty big problem to

232
00:18:14,490 --> 00:18:16,000
 desire things.

233
00:18:16,000 --> 00:18:20,380
 It's actually detrimental to us, harmful to us, the cause

234
00:18:20,380 --> 00:18:23,000
 of greater stress.

235
00:18:23,000 --> 00:18:25,600
 Or if you want to be more objective in how you describe

236
00:18:25,600 --> 00:18:28,000
 this, it's useless.

237
00:18:28,000 --> 00:18:32,280
 There's no reason for us to put out this energy to achieve

238
00:18:32,280 --> 00:18:33,000
 this.

239
00:18:33,000 --> 00:18:37,000
 There's no benefit to liking things. There's no benefit to

240
00:18:37,000 --> 00:18:38,000
 desiring things.

241
00:18:38,000 --> 00:18:41,040
 There's no benefit to chasing after things because those

242
00:18:41,040 --> 00:18:45,000
 things are not in any way, shape or form happiness.

243
00:18:45,000 --> 00:18:54,180
 That's what the meaning of dukkha is, intrinsically without

244
00:18:54,180 --> 00:19:01,000
 any positive attributes or in the ultimate sense.

245
00:19:01,000 --> 00:19:06,000
 So that's the second one.

246
00:19:06,000 --> 00:19:11,940
 And so for as long as we hold on to objects of the sense as

247
00:19:11,940 --> 00:19:15,000
 satisfying, as pleasing,

248
00:19:15,000 --> 00:19:22,910
 we're always going to be dissatisfied. We're going to be

249
00:19:22,910 --> 00:19:25,000
 disappointed.

250
00:19:25,000 --> 00:19:33,520
 We're going to build up opinions and partialities that are

251
00:19:33,520 --> 00:19:40,000
 based on nothing.

252
00:19:40,000 --> 00:19:49,710
 Our in themselves are a complete waste of time. In the end,

253
00:19:49,710 --> 00:19:56,000
 all we gain from our belief in the happiness

254
00:19:56,000 --> 00:20:02,390
 or the pleasure of sense objects is more liking, is a habit

255
00:20:02,390 --> 00:20:05,000
 of desiring, a habit of wanting.

256
00:20:05,000 --> 00:20:09,140
 And this is how drug addiction comes about, or how any kind

257
00:20:09,140 --> 00:20:11,000
 of addiction comes about.

258
00:20:11,000 --> 00:20:14,500
 Any kind of mental addiction, not the physical aspect of

259
00:20:14,500 --> 00:20:20,110
 course, but the mental aspect comes about by cultivating

260
00:20:20,110 --> 00:20:23,000
 desire again and again and again.

261
00:20:23,000 --> 00:20:28,310
 Until we... that's all we have. The object has nothing to

262
00:20:28,310 --> 00:20:29,000
 do with it.

263
00:20:29,000 --> 00:20:38,470
 It's our own desire and our spurring on of the addiction

264
00:20:38,470 --> 00:20:41,000
 cycle.

265
00:20:41,000 --> 00:20:47,110
 But if we can be content, if we can learn to dissociate our

266
00:20:47,110 --> 00:20:51,000
 happiness from the objects of experience,

267
00:20:51,000 --> 00:20:57,210
 so that our happiness is independent, so that we have peace

268
00:20:57,210 --> 00:21:00,660
 and peace of mind, and are not swayed by the vicissitudes

269
00:21:00,660 --> 00:21:01,000
 of life,

270
00:21:01,000 --> 00:21:07,160
 then we have true happiness, then our happiness is unsh

271
00:21:07,160 --> 00:21:10,000
akable, imperturbable.

272
00:21:10,000 --> 00:21:16,410
 And what we have is happiness. If we focus on being happy,

273
00:21:16,410 --> 00:21:20,000
 rather than on liking or wanting,

274
00:21:20,000 --> 00:21:23,830
 instead of cultivating liking or wanting, we cultivate

275
00:21:23,830 --> 00:21:31,070
 contentment, cultivate renunciation, letting go, freeing

276
00:21:31,070 --> 00:21:35,000
 ourselves.

277
00:21:35,000 --> 00:21:41,000
 Then we have that, what we're left with is freedom.

278
00:21:41,000 --> 00:21:44,620
 So that's the second characteristic. The third

279
00:21:44,620 --> 00:21:49,000
 characteristic is called anatta, non-self.

280
00:21:49,000 --> 00:22:03,000
 A non-self has two main meanings, at least two, maybe more,

281
00:22:03,000 --> 00:22:06,990
 but the two main ones that I would use to describe non-self

282
00:22:06,990 --> 00:22:08,000
 are,

283
00:22:08,000 --> 00:22:18,050
 first of all, that thing that we're attributing this to is

284
00:22:18,050 --> 00:22:24,000
 without a core, without a self.

285
00:22:24,000 --> 00:22:28,030
 So when referring to all arisen phenomena, they arise out

286
00:22:28,030 --> 00:22:32,000
 of nothing, and they cease back into nothing.

287
00:22:32,000 --> 00:22:38,150
 From an experiential point of view, there is no entity, so

288
00:22:38,150 --> 00:22:41,000
 there's no atoms or subatomic particles,

289
00:22:41,000 --> 00:22:46,790
 there's certainly no cells or organs or bodies or rocks and

290
00:22:46,790 --> 00:22:50,000
 trees and all of these things.

291
00:22:50,000 --> 00:22:54,550
 None of these things exist anywhere outside of the mind,

292
00:22:54,550 --> 00:22:57,000
 outside of one's thoughts.

293
00:22:57,000 --> 00:23:03,120
 The only reality is experience, and experience is without a

294
00:23:03,120 --> 00:23:04,000
 core.

295
00:23:04,000 --> 00:23:07,360
 So any core that we see in the world, if you look at a

296
00:23:07,360 --> 00:23:10,000
 table and you see a table,

297
00:23:10,000 --> 00:23:12,930
 from an experiential point of view, you're not actually

298
00:23:12,930 --> 00:23:16,560
 seeing the table, you're just, there's the experience of

299
00:23:16,560 --> 00:23:17,000
 seeing,

300
00:23:17,000 --> 00:23:22,000
 and that experience arises and ceases.

301
00:23:22,000 --> 00:23:29,530
 The other definition of non-self is uncontrollability,

302
00:23:29,530 --> 00:23:33,000
 being uncontrollable.

303
00:23:33,000 --> 00:23:41,410
 So it's a product of not having a self, not having a core,

304
00:23:41,410 --> 00:23:46,000
 that no one is in charge of it either.

305
00:23:46,000 --> 00:23:51,340
 The reality of experience being that it arises and ceases

306
00:23:51,340 --> 00:23:56,000
 is that it is subject to causes and conditions.

307
00:23:56,000 --> 00:23:59,990
 Now, we have to tread lightly here because we're not

308
00:23:59,990 --> 00:24:02,000
 talking determinism.

309
00:24:02,000 --> 00:24:07,610
 The meaning isn't determinism, that's a theory that is used

310
00:24:07,610 --> 00:24:10,000
 in regards to a system.

311
00:24:10,000 --> 00:24:13,980
 So we have a system of particles, we'd say the whole

312
00:24:13,980 --> 00:24:17,000
 physical university is such a system,

313
00:24:17,000 --> 00:24:21,350
 and it's always been considered to be deterministic in the

314
00:24:21,350 --> 00:24:25,500
 sense that if you knew everything about the system at one

315
00:24:25,500 --> 00:24:26,000
 point,

316
00:24:26,000 --> 00:24:28,000
 you could predict the future.

317
00:24:28,000 --> 00:24:31,110
 But that is an abstract system, that has nothing to do with

318
00:24:31,110 --> 00:24:33,000
 what we're talking about here.

319
00:24:33,000 --> 00:24:41,100
 What we're talking about here is the interdependence of,

320
00:24:41,100 --> 00:24:48,000
 and the interaction between experiences and entity,

321
00:24:48,000 --> 00:24:51,740
 between, well let's just say between experiences, between

322
00:24:51,740 --> 00:24:54,000
 the past and the future,

323
00:24:54,000 --> 00:25:01,200
 meaning all of our past somehow has an effect on the

324
00:25:01,200 --> 00:25:03,000
 present.

325
00:25:03,000 --> 00:25:07,250
 And so, really, rather than speaking about determinism or

326
00:25:07,250 --> 00:25:12,000
 free will, what it means is practically speaking,

327
00:25:12,000 --> 00:25:20,880
 your effect on the objects of experience is limited to your

328
00:25:20,880 --> 00:25:27,070
 reactions to them, limited to your own momentary

329
00:25:27,070 --> 00:25:30,000
 interactions with them.

330
00:25:30,000 --> 00:25:38,810
 The point being that your own mind is just another part of

331
00:25:38,810 --> 00:25:49,100
 the system, your apprehension, your reception of the object

332
00:25:49,100 --> 00:25:58,000
 is the only influence you have over the system.

333
00:25:58,000 --> 00:26:04,130
 So the ordinary approach to reality is quite different. We

334
00:26:04,130 --> 00:26:06,000
 see things in terms of entities, right?

335
00:26:06,000 --> 00:26:14,370
 We see light bulb, light fixtures, tables and walls, and

336
00:26:14,370 --> 00:26:18,000
 looking around my room, I see all these things,

337
00:26:18,000 --> 00:26:21,000
 and seeing those things, we try to manipulate them.

338
00:26:21,000 --> 00:26:25,900
 I see my hand and I want to use it to manipulate, to reach

339
00:26:25,900 --> 00:26:30,000
 for something, to open the door and so on.

340
00:26:30,000 --> 00:26:37,990
 This is our ordinary grasp of the world. This is how we

341
00:26:37,990 --> 00:26:41,000
 ordinarily interact with the world.

342
00:26:41,000 --> 00:26:45,640
 So as a result, we get this concept of control, where we

343
00:26:45,640 --> 00:26:50,260
 are in charge of reality. We have this idea of being in

344
00:26:50,260 --> 00:26:51,000
 control,

345
00:26:51,000 --> 00:26:54,180
 and so we try to manipulate and control ourselves and

346
00:26:54,180 --> 00:27:01,000
 people around us with words, even with physical actions.

347
00:27:01,000 --> 00:27:06,250
 We try to control our thoughts and so on. We try to control

348
00:27:06,250 --> 00:27:09,000
 individual systems.

349
00:27:09,000 --> 00:27:14,030
 So we think of an eye controlling and of the things being

350
00:27:14,030 --> 00:27:16,000
 controlled, all of these things having entities.

351
00:27:16,000 --> 00:27:18,990
 Now this is all false because reality doesn't admit of

352
00:27:18,990 --> 00:27:20,000
 these things.

353
00:27:20,000 --> 00:27:25,600
 What's really going on is much more complex, at once much

354
00:27:25,600 --> 00:27:30,000
 more complex and at once much more simple.

355
00:27:30,000 --> 00:27:33,090
 So it's more complex in the sense that there are many, many

356
00:27:33,090 --> 00:27:37,000
 factors involved with me opening the door than simply,

357
00:27:37,000 --> 00:27:39,700
 "I want to open the door and therefore the door gets opened

358
00:27:39,700 --> 00:27:40,000
."

359
00:27:40,000 --> 00:27:46,490
 It depends greatly on the physical system, whether the

360
00:27:46,490 --> 00:27:49,000
 physical system is amenable to that.

361
00:27:49,000 --> 00:27:52,920
 But it also depends upon the relationship between the

362
00:27:52,920 --> 00:27:55,000
 physical and the mental.

363
00:27:55,000 --> 00:28:02,300
 The idea of opening the door was already planted probably

364
00:28:02,300 --> 00:28:09,000
 by the brain, which interpreted all of the many factors,

365
00:28:09,000 --> 00:28:13,070
 including the seeing of the door and the desire to go

366
00:28:13,070 --> 00:28:17,000
 outside, moment by moment by moment by moment,

367
00:28:17,000 --> 00:28:21,420
 and suggested the opening of the door, after which the mind

368
00:28:21,420 --> 00:28:24,000
 receives that and makes a decision,

369
00:28:24,000 --> 00:28:28,190
 and then the body, the brain goes back and starts to build

370
00:28:28,190 --> 00:28:31,000
 up the systems to turn the handle.

371
00:28:31,000 --> 00:28:35,460
 But the meaning is it's piece by piece by piece, so many

372
00:28:35,460 --> 00:28:36,000
 little pieces,

373
00:28:36,000 --> 00:28:40,140
 and our desire to open the door was only a very small part

374
00:28:40,140 --> 00:28:41,000
 of that.

375
00:28:41,000 --> 00:28:45,710
 Our intention to open the door was only one small part of

376
00:28:45,710 --> 00:28:47,000
 the system.

377
00:28:47,000 --> 00:28:54,000
 And so control is mostly an illusion.

378
00:28:54,000 --> 00:28:58,000
 What we have is this decision-making in a sense.

379
00:28:58,000 --> 00:29:02,500
 Even that is generally proposed by the brain, so they did

380
00:29:02,500 --> 00:29:07,000
 this study on free will

381
00:29:07,000 --> 00:29:10,430
 and asked people to choose whether to lift their right or

382
00:29:10,430 --> 00:29:11,000
 left hand,

383
00:29:11,000 --> 00:29:13,920
 and they found that the scientists were able to predict

384
00:29:13,920 --> 00:29:16,000
 which hand the person was going to.

385
00:29:16,000 --> 00:29:18,840
 They knew which hand the person was going to lift before

386
00:29:18,840 --> 00:29:21,000
 the subject themselves was aware of it,

387
00:29:21,000 --> 00:29:23,000
 meaning that it was unconscious.

388
00:29:23,000 --> 00:29:26,420
 The brain had already chosen which hand it was going to

389
00:29:26,420 --> 00:29:27,000
 lift.

390
00:29:27,000 --> 00:29:30,210
 So the brain is the one that proposes it to the mind, and

391
00:29:30,210 --> 00:29:31,000
 then the mind is aware,

392
00:29:31,000 --> 00:29:35,700
 and the mind makes a decision, or makes an agreement or

393
00:29:35,700 --> 00:29:37,000
 disagreement,

394
00:29:37,000 --> 00:29:41,000
 or reacts to that proposition of opening the door.

395
00:29:41,000 --> 00:29:43,000
 So it's a very small part, actually.

396
00:29:43,000 --> 00:29:46,000
 And this is why control doesn't work.

397
00:29:46,000 --> 00:29:49,430
 Trying to control our lives, trying to control the people

398
00:29:49,430 --> 00:29:50,000
 around us,

399
00:29:50,000 --> 00:29:56,300
 99.999% of it is out of our control, is already part of the

400
00:29:56,300 --> 00:29:57,000
 system.

401
00:29:57,000 --> 00:30:02,170
 Or our ability to manipulate, it's kind of like a car barre

402
00:30:02,170 --> 00:30:08,000
ling down the road at 100 miles per hour.

403
00:30:08,000 --> 00:30:12,000
 The person at the wheel, are they in charge of the car?

404
00:30:12,000 --> 00:30:14,000
 Well, in a sense they are.

405
00:30:14,000 --> 00:30:17,400
 But only in the sense that they can change lanes, they can

406
00:30:17,400 --> 00:30:18,000
 slow down, they can speed up,

407
00:30:18,000 --> 00:30:21,520
 but they can't suddenly decide, "Now I want to go the other

408
00:30:21,520 --> 00:30:23,000
 way," or, "Now I want to go left,"

409
00:30:23,000 --> 00:30:27,170
 "Now I want to go right," they have to very carefully, at

410
00:30:27,170 --> 00:30:32,000
 100 miles per hour, there's very little you can do.

411
00:30:32,000 --> 00:30:35,000
 And so life is kind of like that.

412
00:30:35,000 --> 00:30:40,060
 The change you can make is just a little to the left, a

413
00:30:40,060 --> 00:30:41,000
 little to the right,

414
00:30:41,000 --> 00:30:45,000
 a little slowing down, speeding up.

415
00:30:45,000 --> 00:30:49,000
 It has to be done gradually.

416
00:30:49,000 --> 00:30:52,550
 This is why enlightenment is often understood to be a

417
00:30:52,550 --> 00:30:54,000
 gradual process.

418
00:30:54,000 --> 00:30:59,000
 It's not something that you can jump into immediately,

419
00:30:59,000 --> 00:31:03,000
 go from being full of defilements to being pure of mind.

420
00:31:03,000 --> 00:31:07,000
 It's a gradual process.

421
00:31:07,000 --> 00:31:10,510
 Because so much of it is already ingrained, it's already

422
00:31:10,510 --> 00:31:14,000
 habitual, it's already been set in motion,

423
00:31:14,000 --> 00:31:17,000
 it's already barreling down the road.

424
00:31:17,000 --> 00:31:20,180
 If we want to change directions, we have to do it carefully

425
00:31:20,180 --> 00:31:27,000
 and slowly, step by step.

426
00:31:27,000 --> 00:31:31,260
 So mostly we are caught in this delusion of trying to

427
00:31:31,260 --> 00:31:32,000
 control,

428
00:31:32,000 --> 00:31:35,650
 and this is a cause for great suffering because, again,

429
00:31:35,650 --> 00:31:37,000
 things are not according to our wishes,

430
00:31:37,000 --> 00:31:41,520
 and it's so much of it's out of our control that it works

431
00:31:41,520 --> 00:31:45,000
 fine when the way things are going happens

432
00:31:45,000 --> 00:31:47,000
 to be the way we want them to go.

433
00:31:47,000 --> 00:31:50,400
 We don't mind, and we pretend or we think that we're in

434
00:31:50,400 --> 00:31:52,000
 control when actually it's...

435
00:31:52,000 --> 00:31:55,000
 There's great delusion and kind of arrogance of thinking,

436
00:31:55,000 --> 00:31:57,740
 "Oh, things are going my way, that must mean I'm in control

437
00:31:57,740 --> 00:32:02,000
," is kind of ridiculous.

438
00:32:02,000 --> 00:32:05,520
 But as soon as things start going against our wishes, we

439
00:32:05,520 --> 00:32:14,000
 get stressed and upset and even...

440
00:32:16,000 --> 00:32:22,200
 ...we'll come to great suffering because of our wishes not

441
00:32:22,200 --> 00:32:26,000
 being in line with reality.

442
00:32:26,000 --> 00:32:30,230
 So learning about this, if we can learn, if we can come to,

443
00:32:30,230 --> 00:32:32,000
 if we can rise above this

444
00:32:32,000 --> 00:32:40,640
 and come to...be flexible again, stop trying to control

445
00:32:40,640 --> 00:32:42,000
 things,

446
00:32:42,000 --> 00:32:46,030
 stop worrying about experiences, and again, learning that

447
00:32:46,030 --> 00:32:49,000
 experiences are objectively neutral,

448
00:32:49,000 --> 00:32:51,990
 that there's nothing better but one thing and another thing

449
00:32:51,990 --> 00:32:55,000
, just letting it be,

450
00:32:55,000 --> 00:32:57,430
 letting go, letting go of the whole world, letting go of

451
00:32:57,430 --> 00:32:59,000
 our ambitions, our goals,

452
00:32:59,000 --> 00:33:02,000
 letting go of ourselves.

453
00:33:02,000 --> 00:33:06,450
 And we find true happiness, we lose all this concern about

454
00:33:06,450 --> 00:33:09,000
 things having to be this way or that way,

455
00:33:09,000 --> 00:33:13,000
 self-righteousness, etc., etc.

456
00:33:13,000 --> 00:33:20,000
 And we find true freedom. This is where true freedom lies,

457
00:33:20,000 --> 00:33:23,510
 where true happiness lies. It's not...happiness is not

458
00:33:23,510 --> 00:33:25,000
 subjective, it can never be,

459
00:33:25,000 --> 00:33:30,690
 because the objects of subjective experience are not able

460
00:33:30,690 --> 00:33:32,000
 to satisfy us,

461
00:33:32,000 --> 00:33:41,000
 are impermanent, unsatisfying and uncontrollable.

462
00:33:41,000 --> 00:33:44,010
 So that's the rest of the dhamma that I gave yesterday,

463
00:33:44,010 --> 00:33:47,000
 more or less.

464
00:33:47,000 --> 00:33:53,110
 I think it's probably good for today, no? Thank you all for

465
00:33:53,110 --> 00:33:54,000
 tuning in.

466
00:33:54,000 --> 00:33:59,950
 It's always great to have you all and see everyone here

467
00:33:59,950 --> 00:34:04,000
 joining together in the cultivation of goodness.

468
00:34:04,000 --> 00:34:09,520
 So wish you all a good day, good night, and good meditation

469
00:34:09,520 --> 00:34:11,000
. Be well.

