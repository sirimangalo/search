1
00:00:00,000 --> 00:00:03,790
 Good evening and welcome back to our study of the Dhammap

2
00:00:03,790 --> 00:00:04,000
ada.

3
00:00:04,000 --> 00:00:09,300
 Tonight we continue on with verse number 95, which reads as

4
00:00:09,300 --> 00:00:10,000
 follows.

5
00:00:10,000 --> 00:00:25,920
 Patavi samo novi rujjati, indaki lupamo tadisubhato, rahado

6
00:00:25,920 --> 00:00:26,000
 apeta kadamo, sangsara nabhavanti tadino,

7
00:00:26,000 --> 00:00:38,530
 which means like the earth one is not disturbed, like the,

8
00:00:38,530 --> 00:00:43,000
 or just as the earth is not disturbed,

9
00:00:43,000 --> 00:00:50,310
 is not just like the earth that is undisturbed or just like

10
00:00:50,310 --> 00:00:57,000
 just as the earth is not undisturbed is not obstructed.

11
00:00:57,000 --> 00:01:05,270
 Whatever that means, just as a foundation pillar, indaki l

12
00:01:05,270 --> 00:01:17,000
upamo, just like an indaki lupamo or a foundation post is

13
00:01:17,000 --> 00:01:17,000
 such a one of,

14
00:01:17,000 --> 00:01:26,630
 good behavior or good deeds, subat do. So I guess this is

15
00:01:26,630 --> 00:01:31,000
 translated, he is undisturbed like the earth.

16
00:01:31,000 --> 00:01:38,490
 That's what it is. Patavi samo novi rujjati, he is undist

17
00:01:38,490 --> 00:01:45,850
urbed like the earth, one who is of good behavior or of good

18
00:01:45,850 --> 00:01:51,000
 manners is like a foundation post,

19
00:01:51,000 --> 00:01:59,200
 is firmly, you know, well-founded, unshakable. Rahado apeta

20
00:01:59,200 --> 00:02:06,330
 kadamo, like a lake that has become un-muddied, that is

21
00:02:06,330 --> 00:02:09,000
 clear and free from cloudiness.

22
00:02:09,000 --> 00:02:16,840
 Kadama is mud. Sangsara nabhavanti tadino, there is no

23
00:02:16,840 --> 00:02:24,000
 wandering on, there is no round of samsara, there is no,

24
00:02:24,000 --> 00:02:29,540
 no trans-migration from one life to the next, born old,

25
00:02:29,540 --> 00:02:35,370
 sick and die, being born old, getting old, getting sick and

26
00:02:35,370 --> 00:02:38,000
 dying again and again for such a person.

27
00:02:38,000 --> 00:02:56,020
 So, like the earth, it is unobstructed, untroubled maybe,

28
00:02:56,020 --> 00:03:02,000
 founded like a foundation post.

29
00:03:03,000 --> 00:03:12,630
 Like a lake that is clear and un-muddied, for such a person

30
00:03:12,630 --> 00:03:18,000
 there is no more becoming.

31
00:03:18,000 --> 00:03:24,010
 This is a description of Sariputta. So the story is about a

32
00:03:24,010 --> 00:03:30,000
 famous incident that occurred in regards to Sariputta.

33
00:03:30,000 --> 00:03:35,970
 Sariputta got the idea that he would go off on a wandering

34
00:03:35,970 --> 00:03:37,000
 tour.

35
00:03:37,000 --> 00:03:39,480
 So we had one story where the Buddha went off on a

36
00:03:39,480 --> 00:03:42,000
 wandering tour and Mahakasapa turned back.

37
00:03:42,000 --> 00:03:46,730
 It's a story about Mahakasapa. Here's a story about Sariput

38
00:03:46,730 --> 00:03:47,000
ta.

39
00:03:47,000 --> 00:03:54,020
 And it seems that Sariputta was having monks to go with him

40
00:03:54,020 --> 00:03:59,000
, talking to monks about going with him or staying behind.

41
00:03:59,000 --> 00:04:05,340
 And there was one monk who Sariputta didn't know by name or

42
00:04:05,340 --> 00:04:09,000
 who his name doesn't even show up.

43
00:04:09,000 --> 00:04:16,380
 And Sariputta was addressing monks by name. And this monk

44
00:04:16,380 --> 00:04:19,800
 thought to himself, "Oh, Sariputta will address me by name

45
00:04:19,800 --> 00:04:20,000
."

46
00:04:20,000 --> 00:04:24,670
 And he never did. Sariputta just said, "Okay, and the rest

47
00:04:24,670 --> 00:04:27,000
 of you do this or do that."

48
00:04:27,000 --> 00:04:39,970
 He didn't call him out, which it seems like such a small

49
00:04:39,970 --> 00:04:41,000
 thing really.

50
00:04:41,000 --> 00:04:44,920
 But it reminds me when I was a new monk, and it's a story

51
00:04:44,920 --> 00:04:47,000
 that I'll always remember.

52
00:04:47,000 --> 00:05:01,550
 When I returned to stay with my teacher as a monk, I always

53
00:05:01,550 --> 00:05:06,000
 wanted him to...

54
00:05:06,000 --> 00:05:09,000
 It was that he didn't remember my name. That's what it was.

55
00:05:09,000 --> 00:05:12,970
 I was always kind of disappointed that he didn't remember

56
00:05:12,970 --> 00:05:17,000
 my name, and he would always ask, "What's his name?"

57
00:05:17,000 --> 00:05:21,620
 And when I told him my name, he said, "No, what kind of a

58
00:05:21,620 --> 00:05:23,000
 name is that?"

59
00:05:23,000 --> 00:05:27,470
 He was kind of making fun of me actually. And I could never

60
00:05:27,470 --> 00:05:31,000
 remember my name every time I go to see him with this monk.

61
00:05:31,000 --> 00:05:36,250
 "What's this monk? Where did he ordain? He ordained with

62
00:05:36,250 --> 00:05:37,000
 you."

63
00:05:37,000 --> 00:05:40,170
 Because after I ordained, I left and went back to Canada

64
00:05:40,170 --> 00:05:44,000
 for a year. It was an odd situation.

65
00:05:44,000 --> 00:05:51,820
 I stayed with a monk in Canada. And then there was this big

66
00:05:51,820 --> 00:05:55,000
 bruhaha about the International Department.

67
00:05:55,000 --> 00:06:03,000
 And I kind of caused a ruckus demanding something or other.

68
00:06:03,000 --> 00:06:08,000
 I was put in a position, because there was all politics,

69
00:06:08,000 --> 00:06:09,000
 and there were two groups.

70
00:06:09,000 --> 00:06:15,050
 The monastery was sort of split between those who wanted

71
00:06:15,050 --> 00:06:20,000
 the international students to go with a group of lay people

72
00:06:20,000 --> 00:06:23,750
 and those who wanted the international students to practice

73
00:06:23,750 --> 00:06:26,000
 with the monks through a translator.

74
00:06:26,000 --> 00:06:29,220
 Anyway, I was being put on one side, and there was another

75
00:06:29,220 --> 00:06:32,900
 monk being put on the other side, and we were the two

76
00:06:32,900 --> 00:06:33,000
 people.

77
00:06:33,000 --> 00:06:35,280
 And I said, "This is splitting up the sangha." I said, "I

78
00:06:35,280 --> 00:06:36,000
've refused to do it."

79
00:06:36,000 --> 00:06:38,730
 And I went in front of Ajahn Tong, and the other monk

80
00:06:38,730 --> 00:06:43,000
 yelled at me and said, "Who do you think you are?"

81
00:06:43,000 --> 00:06:46,500
 Anyway, I felt Ajahn Tong scolded me as well and said, "

82
00:06:46,500 --> 00:06:50,110
This isn't splitting up the sangha. This is trying to make

83
00:06:50,110 --> 00:06:51,000
 things work."

84
00:06:51,000 --> 00:06:54,680
 And he said, "You have wrong thought." He scolded me for it

85
00:06:54,680 --> 00:06:55,000
.

86
00:06:55,000 --> 00:07:00,630
 And I got in, you know, there were stories going around the

87
00:07:00,630 --> 00:07:04,000
 monastery about my bad behavior.

88
00:07:04,000 --> 00:07:08,910
 I was quite adamant and I guess you could say kind of

89
00:07:08,910 --> 00:07:12,890
 arrogant about it, you know, kind of how Westerners tend to

90
00:07:12,890 --> 00:07:14,000
 be when we think something's not right.

91
00:07:14,000 --> 00:07:18,600
 We get up and say in front of everybody. And from then on,

92
00:07:18,600 --> 00:07:21,000
 he never forgot my name.

93
00:07:21,000 --> 00:07:28,230
 So, yeah, I know how that is. But after that, I wasn't, so

94
00:07:28,230 --> 00:07:31,900
 it was kind of ruined for me after that because I know why

95
00:07:31,900 --> 00:07:33,000
 he remembered my name.

96
00:07:33,000 --> 00:07:38,990
 Not for the best reason. But again, it goes back to how

97
00:07:38,990 --> 00:07:42,490
 monks can get a little bit crazy, you know, when your life

98
00:07:42,490 --> 00:07:43,000
 is so simple.

99
00:07:43,000 --> 00:07:49,220
 I mean, for most people in the world, they have very

100
00:07:49,220 --> 00:07:53,000
 extreme outputs for pleasure, you know.

101
00:07:53,000 --> 00:07:57,000
 So you have all sorts of complex activities if you want to

102
00:07:57,000 --> 00:08:00,650
 go to a movie or if you want to go dancing or if you want

103
00:08:00,650 --> 00:08:04,000
 to go to the opera or I don't know, what do people do?

104
00:08:04,000 --> 00:08:09,560
 Want to go to a rave? Do they still do those? Or you can do

105
00:08:09,560 --> 00:08:11,000
 drugs or that kind of thing.

106
00:08:11,000 --> 00:08:16,620
 But for monks, there's very little of an outlet. And so you

107
00:08:16,620 --> 00:08:20,000
 tend to get petty.

108
00:08:20,000 --> 00:08:24,620
 And this is an example of this monk being very petty

109
00:08:24,620 --> 00:08:29,000
 because his ego didn't have much to react to.

110
00:08:29,000 --> 00:08:32,710
 So a simple slighting, not calling him by name, set him off

111
00:08:32,710 --> 00:08:36,000
. And from then on, he had a grudge against Sariputta.

112
00:08:36,000 --> 00:08:38,260
 And you hear about the Salat monks who had grudges. They

113
00:08:38,260 --> 00:08:41,070
 tend to be recognized again and hearkens back to my own

114
00:08:41,070 --> 00:08:42,000
 experience.

115
00:08:42,000 --> 00:08:48,170
 I can verify this. What is it? I don't know if there is

116
00:08:48,170 --> 00:08:51,000
 even an adage for something like the greasy wheel.

117
00:08:51,000 --> 00:08:55,530
 The squeaky wheel gets the grease, but that's supposed to

118
00:08:55,530 --> 00:08:57,000
 be a good thing.

119
00:08:57,000 --> 00:09:02,550
 Sometimes it's better to go unknown because the reasons for

120
00:09:02,550 --> 00:09:04,000
 becoming known.

121
00:09:04,000 --> 00:09:08,550
 This monk got in the Dhammapada for his name and they didn

122
00:09:08,550 --> 00:09:12,000
't even put his name in, insult to injury.

123
00:09:12,000 --> 00:09:15,540
 They put him here and they refused to include his name or

124
00:09:15,540 --> 00:09:19,000
 just refused to remember his name. I don't know. I just

125
00:09:19,000 --> 00:09:19,000
 forgot it.

126
00:09:23,000 --> 00:09:28,610
 Anyetra namagotofasena abhaccadobhiku, whatever that means,

127
00:09:28,610 --> 00:09:31,000
 "abhaccado", unknown.

128
00:09:31,000 --> 00:09:36,280
 So they refused to remember or no one knew his name or it

129
00:09:36,280 --> 00:09:38,000
 was unknown by Sariputta, I guess.

130
00:09:38,000 --> 00:09:42,660
 So Sariputta didn't call him out by name. Anyway, being

131
00:09:42,660 --> 00:09:45,940
 petty and now he's remembered forever, immortalized in the

132
00:09:45,940 --> 00:09:47,000
 Dhammapada for it.

133
00:09:50,000 --> 00:09:54,990
 So then on top of that, to add insult to injury, not at all

134
00:09:54,990 --> 00:10:02,430
 really, but to get even more, let's say, to be even worse

135
00:10:02,430 --> 00:10:05,000
 about this whole situation,

136
00:10:05,000 --> 00:10:09,000
 even more petty to an extreme degree.

137
00:10:09,000 --> 00:10:15,660
 As Sariputta, as they were preparing, Sariputta walked past

138
00:10:15,660 --> 00:10:21,330
 him and the corner of his robe touched the upset monk on

139
00:10:21,330 --> 00:10:24,000
 the ear or something.

140
00:10:27,000 --> 00:10:31,430
 And the monk, either getting angry about it or just using

141
00:10:31,430 --> 00:10:36,110
 it as an excuse, started going around and telling people,

142
00:10:36,110 --> 00:10:40,440
 or he went straight to the Buddha maybe and told the Buddha

143
00:10:40,440 --> 00:10:41,000
.

144
00:10:46,000 --> 00:10:51,600
 What do you think he said to the Buddha? He said, he went

145
00:10:51,600 --> 00:10:57,250
 straight to the Buddha and said, "Reverencer, Venerable Sar

146
00:10:57,250 --> 00:11:00,120
iputta, doubtless thinking to himself, 'I am your chief

147
00:11:00,120 --> 00:11:07,150
 disciple,' struck me a blow that almost broke my eardrum or

148
00:11:07,150 --> 00:11:10,000
 something, some part of my ear."

149
00:11:10,000 --> 00:11:14,810
 And then without even asking forgiveness, he set out on his

150
00:11:14,810 --> 00:11:19,000
 arms, set out for alms. So it was before the alms round.

151
00:11:19,000 --> 00:11:24,530
 Could you imagine the gall of this guy going to the Buddha

152
00:11:24,530 --> 00:11:30,000
 to inform upon Sariputta who had done nothing wrong?

153
00:11:31,000 --> 00:11:36,790
 I mean, the amount of ignorance you need to do that is

154
00:11:36,790 --> 00:11:41,490
 pretty astounding. And yet, I mean, it happens. People get

155
00:11:41,490 --> 00:11:46,380
 blinded, but as we'll see, it doesn't last long. It can't

156
00:11:46,380 --> 00:11:47,000
 last long.

157
00:11:47,000 --> 00:11:51,580
 There's such purity. And as we'll see in regards to how Sar

158
00:11:51,580 --> 00:11:56,750
iputta responds, there's such a profundity to these beings

159
00:11:56,750 --> 00:11:59,000
 that they can't last.

160
00:11:59,000 --> 00:12:03,370
 Eventually, he asks forgiveness. So all the monks hear

161
00:12:03,370 --> 00:12:08,120
 about this. The Buddha calls someone, "Go and tell Sariput

162
00:12:08,120 --> 00:12:09,000
ta to come."

163
00:12:09,000 --> 00:12:13,140
 And all the other monks finding out about this, they gather

164
00:12:13,140 --> 00:12:18,550
 together. And Mughalana and Ananda went around to all the

165
00:12:18,550 --> 00:12:22,180
 monks and said, "Come, come and see. You want to see

166
00:12:22,180 --> 00:12:25,000
 something neat? See how Sariputta deals with this guy."

167
00:12:25,000 --> 00:12:30,350
 And so all the monks came out to listen. And the Buddha

168
00:12:30,350 --> 00:12:36,850
 said, "So, Sariputta, this monk says that you have struck

169
00:12:36,850 --> 00:12:44,000
 him and without even apologizing went away. You hit him."

170
00:12:45,000 --> 00:12:48,170
 And then to find out what Sariputta says, we actually have

171
00:12:48,170 --> 00:12:55,480
 to go to the Anguttara Nikaya. This is actually one story

172
00:12:55,480 --> 00:13:01,000
 that is supported by the actual canonical text.

173
00:13:01,000 --> 00:13:08,930
 Remember, the stories we're reading are recreations recre

174
00:13:08,930 --> 00:13:14,810
ated from the verses. So they may have been exaggerated and

175
00:13:14,810 --> 00:13:18,000
 more likely to have been exaggerated.

176
00:13:18,000 --> 00:13:24,370
 Perhaps some might even say made up and just based on

177
00:13:24,370 --> 00:13:30,010
 folklore. But regardless, they are less, they're not

178
00:13:30,010 --> 00:13:34,000
 canonical, whether they're true or not.

179
00:13:34,000 --> 00:13:40,110
 But here we have one that is based on the Anguttara Nikaya

180
00:13:40,110 --> 00:13:42,000
 Book of Nines.

181
00:13:42,000 --> 00:13:48,200
 The Buddhist, the Kastari Buddha says nine things. He doesn

182
00:13:48,200 --> 00:13:50,000
't say, "I didn't hit the guy."

183
00:13:50,000 --> 00:13:58,280
 Instead, he says, "One who has not established mindfulness

184
00:13:58,280 --> 00:14:04,000
 directed to the body in regards to his own body."

185
00:14:04,000 --> 00:14:20,000
 Let's see how he says that.

186
00:14:20,000 --> 00:14:28,330
 "Yasanunabhante kaya gatasati gatasati anupartita asa." So

187
00:14:28,330 --> 00:14:32,690
 one who is not mindful of the body. And this can be in one

188
00:14:32,690 --> 00:14:34,000
 of various ways.

189
00:14:34,000 --> 00:14:38,360
 But you could relate it back to our practice and say one

190
00:14:38,360 --> 00:14:43,010
 who is not aware when they're walking that they know they

191
00:14:43,010 --> 00:14:44,000
're walking,

192
00:14:44,000 --> 00:14:49,570
 not aware of the movements of their body, where their hands

193
00:14:49,570 --> 00:14:55,360
 are, not aware of where their mind is, and not objective

194
00:14:55,360 --> 00:14:57,000
 about their actions.

195
00:14:57,000 --> 00:15:01,880
 Such a person could very easily strike someone out of anger

196
00:15:01,880 --> 00:15:02,000
.

197
00:15:02,000 --> 00:15:06,180
 But the point is, when you're mindful, you actually are

198
00:15:06,180 --> 00:15:11,530
 unable to get angry. When you're truly mindful, as an arah

199
00:15:11,530 --> 00:15:13,000
ant is always,

200
00:15:13,000 --> 00:15:16,880
 it's not possible for you to strike someone. And that's

201
00:15:16,880 --> 00:15:19,000
 what Sari Buddha says.

202
00:15:19,000 --> 00:15:22,000
 And he gives nine similes as to why it wouldn't be possible

203
00:15:22,000 --> 00:15:26,200
, or as to what it's like to be someone who is mindful of

204
00:15:26,200 --> 00:15:27,000
 the body,

205
00:15:27,000 --> 00:15:30,090
 who is aware of the movements of the body, who would know

206
00:15:30,090 --> 00:15:32,760
 when they were raising their fist and would be fully

207
00:15:32,760 --> 00:15:33,000
 mindful,

208
00:15:33,000 --> 00:15:37,310
 and thus unable to give rise to the rage required, the

209
00:15:37,310 --> 00:15:44,000
 cruelty required, the hatred required to hurt someone.

210
00:15:44,000 --> 00:15:50,900
 And so he says, "Just as water, just as you can wash impure

211
00:15:50,900 --> 00:15:55,000
 things in water, and the water is not repulsed by it,

212
00:15:55,000 --> 00:15:59,000
 the water doesn't get upset by that."

213
00:15:59,000 --> 00:16:04,780
 He said, "My mind is like water, without enmity or ill will

214
00:16:04,780 --> 00:16:08,000
 towards anyone, no matter what they do.

215
00:16:08,000 --> 00:16:13,000
 Just as fire burns impure things, but is not upset by it,

216
00:16:13,000 --> 00:16:19,240
 so too I dwell like fire, not upset when people, whatever

217
00:16:19,240 --> 00:16:23,000
 they may say are due to me."

218
00:16:23,000 --> 00:16:31,480
 Just as air blows upon impure things, and is not repulsed

219
00:16:31,480 --> 00:16:38,120
 by that, so too I can meet with anything, come in contact

220
00:16:38,120 --> 00:16:40,000
 with anyone and not be upset.

221
00:16:40,000 --> 00:16:44,000
 "My mind, I dwell with a mind like air."

222
00:16:44,000 --> 00:16:48,310
 I dwell with a mind like a duster, a dust rag, I guess. A

223
00:16:48,310 --> 00:16:56,320
 dust rag is used to clean up all sorts of things, and yet

224
00:16:56,320 --> 00:17:00,000
 the rag is not repelled.

225
00:17:00,000 --> 00:17:06,950
 And then just as an outcast boy or girl, when I walk, I

226
00:17:06,950 --> 00:17:13,000
 think of myself as an outcast boy or girl holding a bowl

227
00:17:13,000 --> 00:17:21,000
 and wandering around for alms or for charity, begging.

228
00:17:21,000 --> 00:17:25,360
 He says, "That's how I see myself, I put myself on that

229
00:17:25,360 --> 00:17:30,000
 level, when I go for alms, when I walk, when I live."

230
00:17:30,000 --> 00:17:33,780
 Because an outcast boy or girl in India, they're not

231
00:17:33,780 --> 00:17:38,000
 allowed to work, they're not allowed to do so many things.

232
00:17:38,000 --> 00:17:44,720
 They have to live in special areas or had to, this is in

233
00:17:44,720 --> 00:17:47,000
 ancient times.

234
00:17:47,000 --> 00:17:50,780
 Just as a bull with his horns cut, mild, well tamed and

235
00:17:50,780 --> 00:17:58,190
 well trained. So a bull without its horns isn't going to

236
00:17:58,190 --> 00:18:01,000
 attack anyone.

237
00:18:01,000 --> 00:18:06,160
 And then, "I am like this bull without horns, I have no

238
00:18:06,160 --> 00:18:09,000
 horns with which to attack."

239
00:18:09,000 --> 00:18:11,620
 Meaning he can't even, he couldn't hit someone even if,

240
00:18:11,620 --> 00:18:14,660
 well if he wanted to because he couldn't want to. It could

241
00:18:14,660 --> 00:18:17,000
 never happen. It's not possible.

242
00:18:17,000 --> 00:18:19,890
 He's not capable of it, it's not that he doesn't even want

243
00:18:19,890 --> 00:18:22,000
 to, it's that he's not capable of it.

244
00:18:22,000 --> 00:18:29,210
 And number eight, just as a woman or a man, a young woman

245
00:18:29,210 --> 00:18:30,000
 or man, no?

246
00:18:30,000 --> 00:18:33,980
 Think of a young woman, a young man who liked to dress up,

247
00:18:33,980 --> 00:18:37,940
 liked to be clean, liked to put on perfumes, liked to put

248
00:18:37,940 --> 00:18:39,000
 on makeup.

249
00:18:39,000 --> 00:18:43,450
 Imagine if such a person had a carcass of a snake, a carc

250
00:18:43,450 --> 00:18:47,600
ass of a dog or a carcass of a human being slung around

251
00:18:47,600 --> 00:18:49,000
 their neck.

252
00:18:49,000 --> 00:18:53,480
 Could you imagine, put a carcass of a dog around your neck,

253
00:18:53,480 --> 00:18:59,840
 what they would think of that? And he said, he says, "Bante

254
00:18:59,840 --> 00:19:03,650
, just in that way, I am repelled, humiliated and disgusted

255
00:19:03,650 --> 00:19:05,000
 by this foul body."

256
00:19:05,000 --> 00:19:09,940
 Interesting wording, no? Means he has no, it's not even,

257
00:19:09,940 --> 00:19:13,990
 you have to take that kind of with a bit of license because

258
00:19:13,990 --> 00:19:17,000
 I think it's unfair to say he's disgusted by it.

259
00:19:17,000 --> 00:19:22,000
 Disgusted would be, you know, an upset, but he's certainly

260
00:19:22,000 --> 00:19:23,000
 not upset.

261
00:19:23,000 --> 00:19:25,780
 He just has no thought that there's anything good inside it

262
00:19:25,780 --> 00:19:30,000
. I mean, the body is made up of all sorts of icky things.

263
00:19:34,000 --> 00:19:40,720
 And number nine, seeing as he does that, just as a person

264
00:19:40,720 --> 00:19:46,510
 might carry around a cracked bowl, or imagine someone with

265
00:19:46,510 --> 00:19:50,510
 a garbage bag full of, from a restaurant, from behind a

266
00:19:50,510 --> 00:19:59,000
 restaurant with fat and refuse and so on, with a leaky

267
00:19:59,000 --> 00:20:00,000
 garbage bag.

268
00:20:00,000 --> 00:20:04,860
 So this is a perforated bowl, a cracked bowl with fat in it

269
00:20:04,860 --> 00:20:07,000
 that oozes and drips.

270
00:20:07,000 --> 00:20:14,180
 And the body is like this with all sorts of holes in it

271
00:20:14,180 --> 00:20:20,000
 that ooze and drip and sweat and smell.

272
00:20:20,000 --> 00:20:22,900
 So he said, "One who has, in conclusion, one who has not

273
00:20:22,900 --> 00:20:26,010
 established mindfulness directed to the body in regards to

274
00:20:26,010 --> 00:20:28,840
 his own body might strike a fellow monk here and then set

275
00:20:28,840 --> 00:20:31,000
 out on tour without apologizing."

276
00:20:31,000 --> 00:20:34,900
 So it's called the lion's roar. It's one of the many lion's

277
00:20:34,900 --> 00:20:40,000
 roars that we hear about. This one is by Sariputta.

278
00:20:40,000 --> 00:20:44,810
 And so it's kind of impressive to read. Sariputta has no

279
00:20:44,810 --> 00:20:50,000
 qualms about putting this monk in his place, so to speak.

280
00:20:50,000 --> 00:20:55,000
 It's interesting that this came right after criticism.

281
00:20:55,000 --> 00:20:57,000
 Yesterday we were talking about criticism.

282
00:20:57,000 --> 00:21:01,040
 And reading through it today made me think, "Okay, this is

283
00:21:01,040 --> 00:21:04,750
 the emulation required. This is what we have to work

284
00:21:04,750 --> 00:21:07,000
 towards to be like Sariputta."

285
00:21:07,000 --> 00:21:13,000
 "Who is so mindful that he has no ill will."

286
00:21:13,000 --> 00:21:21,770
 So this is Sariputta's argument as to why that such a thing

287
00:21:21,770 --> 00:21:25,000
 wouldn't be done.

288
00:21:25,000 --> 00:21:29,480
 And so you think that would be enough. And it was enough.

289
00:21:29,480 --> 00:21:33,560
 The monk immediately was shaken and got up on his hands and

290
00:21:33,560 --> 00:21:37,330
 knees and prostrated himself at the foot of the Buddha and

291
00:21:37,330 --> 00:21:39,000
 said, "Please forgive me.

292
00:21:39,000 --> 00:21:44,460
 I was stupid. I was wrong. I transmitted a transgression

293
00:21:44,460 --> 00:21:49,880
 that I so foolishly, stupidly and unskillfully slandered

294
00:21:49,880 --> 00:21:54,720
 the venerable Sariputta on grounds that were untrue,

295
00:21:54,720 --> 00:21:57,000
 baseless, false."

296
00:21:57,000 --> 00:22:00,880
 And the Buddha acknowledged that. And then he turns to Sar

297
00:22:00,880 --> 00:22:04,840
iputta and he says, "Sariputta, forgive him before his head

298
00:22:04,840 --> 00:22:06,000
 explodes."

299
00:22:06,000 --> 00:22:09,570
 This apparently was a thing. There is a thing. If you

300
00:22:09,570 --> 00:22:15,290
 insult someone who is enlightened, it can get you into some

301
00:22:15,290 --> 00:22:18,000
 serious head exploding.

302
00:22:18,000 --> 00:22:23,800
 And so as if it wasn't enough, Sariputta puts the icing on

303
00:22:23,800 --> 00:22:28,000
 the cake by saying, "I will pardon him."

304
00:22:28,000 --> 00:22:34,000
 No. Because of what he's said. Or I think it's, "I will

305
00:22:34,000 --> 00:22:37,000
 pardon him if he says that to me."

306
00:22:37,000 --> 00:22:42,300
 Right? If he asked me forgiveness. Meaning he hasn't yet

307
00:22:42,300 --> 00:22:44,320
 asked the Buddha, asked Sariputta forgiveness. He asked the

308
00:22:44,320 --> 00:22:45,000
 Buddha forgiveness.

309
00:22:45,000 --> 00:22:48,760
 So the point is, Sariputta has nothing to forgive, has no

310
00:22:48,760 --> 00:22:52,000
 pardon to give until the monk says he's sorry.

311
00:22:52,000 --> 00:22:54,340
 Not that Sariputta needs it, but he's saying, "Well, if he

312
00:22:54,340 --> 00:22:57,080
 came to me, of course I'd apologize. I'd forgive him. I

313
00:22:57,080 --> 00:22:59,000
 have no hard feelings."

314
00:22:59,000 --> 00:23:05,440
 And then he says, "And then, and let him pardon me as well.

315
00:23:05,440 --> 00:23:10,000
 Let him forgive me as well."

316
00:23:10,000 --> 00:23:13,230
 Meaning for anything I might have done to him. This is

317
00:23:13,230 --> 00:23:18,220
 actually quite standard. When you apologize to a monk, we

318
00:23:18,220 --> 00:23:19,000
 do this often.

319
00:23:19,000 --> 00:23:22,570
 We'll apologize to each other and then I apologize to this

320
00:23:22,570 --> 00:23:26,000
 monk and the monk turns around and apologizes to me.

321
00:23:26,000 --> 00:23:32,000
 We actually have a...

322
00:23:32,000 --> 00:23:35,450
 At the end of the opening ceremony we do this. Opening and

323
00:23:35,450 --> 00:23:37,000
 closing ceremonies for a meditator.

324
00:23:37,000 --> 00:23:41,000
 We have this ceremony of asking forgiveness of the teacher

325
00:23:41,000 --> 00:23:43,890
 and then the teacher turns around and asks forgiveness of

326
00:23:43,890 --> 00:23:45,000
 us in this tradition.

327
00:23:45,000 --> 00:23:51,000
 So this is an example for us to emulate.

328
00:23:51,000 --> 00:23:54,160
 Then we switch back to the Dhammapada story. Then

329
00:23:54,160 --> 00:23:58,000
 apparently the Buddha, the monks started commenting on this

330
00:23:58,000 --> 00:24:02,000
 and were terribly impressed.

331
00:24:02,000 --> 00:24:07,830
 In awe and reverence for Sariputta and his ability to just

332
00:24:07,830 --> 00:24:14,240
 put this guy in his place, but to not be at all upset or

333
00:24:14,240 --> 00:24:21,000
 disturbed or angry towards the other monk.

334
00:24:21,000 --> 00:24:23,320
 And the teacher heard what they were saying and came and

335
00:24:23,320 --> 00:24:29,430
 said to them, "Oh, it's not unusual. It's not hard to

336
00:24:29,430 --> 00:24:31,000
 understand."

337
00:24:31,000 --> 00:24:34,890
 He said, "It's impossible for Sariputta and people like him

338
00:24:34,890 --> 00:24:38,630
 to have hatred and Sariputta's mind is like the great earth

339
00:24:38,630 --> 00:24:43,780
. It's like an Indakila, like a foundation post, like a pool

340
00:24:43,780 --> 00:24:45,000
 of still water."

341
00:24:45,000 --> 00:24:50,000
 And then he taught this verse.

342
00:24:50,000 --> 00:24:54,780
 So how this relates to us, again, it's about emulating this

343
00:24:54,780 --> 00:24:58,840
. These are qualities to emulate. We should read the whole s

344
00:24:58,840 --> 00:25:02,910
uttana, Angutra, Book of Nines, for when people accuse us of

345
00:25:02,910 --> 00:25:08,260
 things that we didn't do or even of things that we didn't

346
00:25:08,260 --> 00:25:09,000
 do.

347
00:25:09,000 --> 00:25:11,980
 I guess it's harder when you didn't do it, when you didn't

348
00:25:11,980 --> 00:25:18,280
 do something. And in general you'll notice that it's not

349
00:25:18,280 --> 00:25:23,000
 that they don't refute it. They do.

350
00:25:23,000 --> 00:25:26,020
 But they do so without anger. And they do it for the

351
00:25:26,020 --> 00:25:29,010
 purpose of setting the record straight so people don't get

352
00:25:29,010 --> 00:25:32,650
 the wrong idea. They don't do it so that it makes them look

353
00:25:32,650 --> 00:25:34,000
 good, for sure.

354
00:25:35,000 --> 00:25:44,720
 And so we emulate these qualities. We emulate the imagery

355
00:25:44,720 --> 00:25:50,000
 that Sariputta talked about.

356
00:25:50,000 --> 00:25:53,850
 But most importantly we emulate them in their practice,

357
00:25:53,850 --> 00:25:57,000
 mindfulness of the body. It's a huge one.

358
00:25:57,000 --> 00:26:00,370
 Well, mindfulness is the huge one. But here, talking about

359
00:26:00,370 --> 00:26:03,480
 mindfulness of the body is because we're referring to the

360
00:26:03,480 --> 00:26:05,000
 body. He hit someone.

361
00:26:05,000 --> 00:26:09,350
 Sariputta points out, "You know, it's just a funny thing to

362
00:26:09,350 --> 00:26:14,000
 think about because I'm so mindful with my body," he'd say.

363
00:26:14,000 --> 00:26:19,830
 It's not really possible that such a thing, observing his

364
00:26:19,830 --> 00:26:24,530
 own actions, which is the proper way to deal with an

365
00:26:24,530 --> 00:26:27,510
 accusation, someone accuses you of something, you know,

366
00:26:27,510 --> 00:26:29,000
 just say, "I would never do that."

367
00:26:29,000 --> 00:26:31,980
 He'd reflect and say, "Could I do something? Did I do that?

368
00:26:31,980 --> 00:26:35,050
 Could I do that?" And he looked and he said, "It's not

369
00:26:35,050 --> 00:26:40,700
 possible because I'm mindful all the time. How could you do

370
00:26:40,700 --> 00:26:42,000
 that if you're mindful?"

371
00:26:42,000 --> 00:26:46,470
 And this is the key. Our theory is, and the power of

372
00:26:46,470 --> 00:26:51,020
 mindfulness is, that you can't be angry and mindful at the

373
00:26:51,020 --> 00:26:53,000
 same time.

374
00:26:53,000 --> 00:26:57,170
 When you're clearly aware and objectively aware, the object

375
00:26:57,170 --> 00:27:02,540
ivity overcomes any anger or any greed, and you're just "are

376
00:27:02,540 --> 00:27:06,000
," you're just present.

377
00:27:06,000 --> 00:27:12,270
 It makes you like the Earth, undisturbed, untroubled. The

378
00:27:12,270 --> 00:27:16,590
 Earth is, I mean, it's the imagery of the big, the great

379
00:27:16,590 --> 00:27:21,530
 Earth, Mahapattavi, the Earth as a planet, that, well, it

380
00:27:21,530 --> 00:27:25,000
 does have earthquakes, but is pretty much undisturbed.

381
00:27:25,000 --> 00:27:31,280
 It just sits there. It's totally grounded, I could say. And

382
00:27:31,280 --> 00:27:35,810
 like a foundation post, they use this "indikila" to mean

383
00:27:35,810 --> 00:27:40,000
 something. "Kila" means a post. "Inda" is of Indra.

384
00:27:40,000 --> 00:27:43,990
 It's an expression. It means the foundation of a building,

385
00:27:43,990 --> 00:27:48,000
 the first post that sort of keeps the building up, I guess.

386
00:27:48,000 --> 00:27:55,040
 And so it's the most stable part of a house. It's someone

387
00:27:55,040 --> 00:27:58,000
 who is firm and unmoved, you know.

388
00:27:58,000 --> 00:28:00,760
 So if someone calls them a nasty name, they don't get upset

389
00:28:00,760 --> 00:28:04,000
. If someone says nice things to them, they don't get

390
00:28:04,000 --> 00:28:05,000
 pleased by it.

391
00:28:05,000 --> 00:28:08,180
 Good things come. They're not elated. Bad things come. They

392
00:28:08,180 --> 00:28:11,000
're not depressed. They live at peace.

393
00:28:11,000 --> 00:28:15,080
 They live in the world without being of the world, without

394
00:28:15,080 --> 00:28:19,740
 being moved by the world, without falling under the power

395
00:28:19,740 --> 00:28:21,000
 of samsara.

396
00:28:21,000 --> 00:28:26,720
 They become like a pool of water, still pool of water free

397
00:28:26,720 --> 00:28:28,000
 from mud.

398
00:28:28,000 --> 00:28:31,410
 You can think of mud as all of the mental defilements. This

399
00:28:31,410 --> 00:28:36,170
 is what we're aiming for in Vipassana, is to have a clear

400
00:28:36,170 --> 00:28:37,000
 mind.

401
00:28:37,000 --> 00:28:43,360
 Such a person, the rounds of existence do not exist. Sams

402
00:28:43,360 --> 00:28:47,000
ara does not exist. There is no samsara.

403
00:28:47,000 --> 00:28:54,560
 Samsara nabhanti tadino. Samsara means wandering on. They

404
00:28:54,560 --> 00:28:57,000
 have nowhere left to wander.

405
00:28:57,000 --> 00:29:02,260
 They have no desires or no lessons left to learn. No goals

406
00:29:02,260 --> 00:29:05,000
 yet to achieve.

407
00:29:05,000 --> 00:29:09,830
 They've done it all, or they've risen above it all. They've

408
00:29:09,830 --> 00:29:12,000
 gone beyond it all.

409
00:29:12,000 --> 00:29:16,980
 Anyway, so that's the Dhammapada for tonight. Nice story.

410
00:29:16,980 --> 00:29:18,000
 It's always nice to hear about Sariputta.

411
00:29:18,000 --> 00:29:21,900
 He has other stories like this. There's another story about

412
00:29:21,900 --> 00:29:26,000
 a monk actually coming and hitting him on the back.

413
00:29:26,000 --> 00:29:30,000
 Sariputta was wandering on alms and this brahmin, I think,

414
00:29:30,000 --> 00:29:32,490
 he'd heard that the Sariputta was like this, probably heard

415
00:29:32,490 --> 00:29:33,000
 from this story.

416
00:29:33,000 --> 00:29:35,690
 He thought, "Wow, I wonder if it's true. How could it be

417
00:29:35,690 --> 00:29:38,250
 possible that this guy doesn't get upset no matter what

418
00:29:38,250 --> 00:29:39,000
 happens?"

419
00:29:39,000 --> 00:29:42,250
 He said, "Well, let's test him." So he took a stick and he

420
00:29:42,250 --> 00:29:46,000
 wanders up, walks up behind Sariputta, and he saw an alms

421
00:29:46,000 --> 00:29:50,000
 and smacks him one on the back.

422
00:29:50,000 --> 00:29:55,040
 Sariputta just turns around and looks at him and then turns

423
00:29:55,040 --> 00:29:57,000
 and goes on his way.

424
00:29:57,000 --> 00:30:01,320
 And then the brahmin ends up apologizing to Sariputta. So

425
00:30:01,320 --> 00:30:05,000
 imagine just walking up and smacking him.

426
00:30:05,000 --> 00:30:08,110
 He said, "It's really true. He doesn't get angry. He doesn

427
00:30:08,110 --> 00:30:10,000
't get upset no matter what happens."

428
00:30:10,000 --> 00:30:15,140
 So if you're really interested in Sariputta, you should

429
00:30:15,140 --> 00:30:20,790
 read. There's an English, on the internet, there's an

430
00:30:20,790 --> 00:30:22,000
 article.

431
00:30:22,000 --> 00:30:24,730
 You can also, I think it's in Great Disciples of the Buddha

432
00:30:24,730 --> 00:30:27,000
, but it's also under the life of Sariputta.

433
00:30:27,000 --> 00:30:29,860
 I think it was originally published in the Buddhist

434
00:30:29,860 --> 00:30:33,000
 Publication Society, but I think it's on the internet.

435
00:30:33,000 --> 00:30:37,000
 Very much worth reading because very much worth emulating.

436
00:30:37,000 --> 00:30:41,000
 Sariputta and Mughalana and all the great disciples.

437
00:30:41,000 --> 00:30:45,660
 If you want to know what a true Buddhist is like, what are

438
00:30:45,660 --> 00:30:49,000
 Buddhists like when they go all the way?

439
00:30:49,000 --> 00:30:54,970
 It's pretty impressive. So that in mind, this is what we

440
00:30:54,970 --> 00:30:57,000
 work towards, to be impressive.

441
00:30:57,000 --> 00:31:05,980
 Not exactly, but to be free, to be pure, to be untroubled.

442
00:31:05,980 --> 00:31:09,000
 And that's impressive.

443
00:31:09,000 --> 00:31:13,480
 So thank you all for tuning in. Wishing you all good

444
00:31:13,480 --> 00:31:16,000
 practice and have a good night.

