1
00:00:00,000 --> 00:00:10,680
 Good evening everyone.

2
00:00:10,680 --> 00:00:32,280
 Broadcasting Live April 28. Tonight's talk is about

3
00:00:32,280 --> 00:00:32,680
 reverence.

4
00:00:32,680 --> 00:00:47,360
 This monk comes and asks the Buddha.

5
00:00:47,360 --> 00:00:50,800
 I asked him two questions actually. This quote I only gives

6
00:00:50,800 --> 00:00:51,880
 one.

7
00:00:51,880 --> 00:01:05,340
 But he says, "Konukobante he tukopajayo yena tathagate par

8
00:01:05,340 --> 00:01:05,880
inibhute sadhamonachira titiko hoti."

9
00:01:05,880 --> 00:01:15,280
 What is the cause? What is the reason?

10
00:01:15,280 --> 00:01:22,390
 By which, when the tathagata has entered into parinibhana,

11
00:01:22,390 --> 00:01:27,280
 the good dhamma doesn't last long.

12
00:01:27,280 --> 00:01:31,280
 Doesn't last. So this quote only does last.

13
00:01:31,280 --> 00:01:36,370
 The next question is, what is the cause? What is the reason

14
00:01:36,370 --> 00:01:39,280
 why it does last long?

15
00:01:39,280 --> 00:01:46,280
 Kira titiko hoti.

16
00:01:46,280 --> 00:01:51,170
 And the Buddha, this is from the Anguttara Nikaya, book of

17
00:01:51,170 --> 00:01:54,280
 sevens.

18
00:01:54,280 --> 00:01:58,280
 And so it's a list of seven things.

19
00:01:58,280 --> 00:02:06,280
 Seven things that when you pay respect to them, reverence,

20
00:02:06,280 --> 00:02:13,280
 the word is garua, sagarua.

21
00:02:13,280 --> 00:02:21,280
 And gara means heavy, garu. Garu means heavy.

22
00:02:21,280 --> 00:02:27,930
 Garu actually, the word guru in Sanskrit, guru means

23
00:02:27,930 --> 00:02:29,280
 teacher.

24
00:02:29,280 --> 00:02:40,280
 But I believe the origin is from the word heavy.

25
00:02:40,280 --> 00:02:46,280
 And so garu means to take something seriously.

26
00:02:46,280 --> 00:03:00,130
 If we appreciate and think of as worth something, so we

27
00:03:00,130 --> 00:03:02,280
 worship in a sense,

28
00:03:02,280 --> 00:03:14,280
 in a literal sense, we place worth on these things.

29
00:03:14,280 --> 00:03:18,280
 Then the dhamma will last long after the Buddha disappears.

30
00:03:18,280 --> 00:03:26,070
 Because when the Buddha is around, he's a teacher of all

31
00:03:26,070 --> 00:03:28,280
 beings.

32
00:03:28,280 --> 00:03:35,280
 He's an unexcelled teacher, an unexcelled trainer.

33
00:03:35,280 --> 00:03:39,560
 So when he's around, there's no question. The dhamma will

34
00:03:39,560 --> 00:03:40,280
 last.

35
00:03:40,280 --> 00:03:46,320
 When he's gone, however, what we've got left is the Arahant

36
00:03:46,320 --> 00:03:48,280
 disciples of the Buddha,

37
00:03:48,280 --> 00:03:54,360
 and then they pass away, and then hopefully more Arahants

38
00:03:54,360 --> 00:03:55,280
 come.

39
00:03:55,280 --> 00:03:59,360
 The question is what keeps the lineage going, what keeps

40
00:03:59,360 --> 00:04:01,280
 the chain going?

41
00:04:01,280 --> 00:04:04,630
 And so it's suttas like this that are of great interest to

42
00:04:04,630 --> 00:04:08,280
 those of us in later generations.

43
00:04:08,280 --> 00:04:13,080
 Because it gives us an idea of how we support not only our

44
00:04:13,080 --> 00:04:14,280
 own practice,

45
00:04:14,280 --> 00:04:22,470
 but how we carry on the legacy of the Buddha, pass on the

46
00:04:22,470 --> 00:04:26,280
 legacy to future generations as well.

47
00:04:26,280 --> 00:04:33,310
 How we take what was given to us and cherish it and nurture

48
00:04:33,310 --> 00:04:36,280
 it and keep it alive.

49
00:04:36,280 --> 00:04:41,000
 It's like we're given a seed to a great tree, and it's up

50
00:04:41,000 --> 00:04:48,280
 to us to plant it and cultivate it for future generations.

51
00:04:48,280 --> 00:04:52,280
 The dhamma is like that at some point, or it's like a fire.

52
00:04:52,280 --> 00:04:56,280
 In ancient times, in cave time, in ancient, ancient times,

53
00:04:56,280 --> 00:05:04,840
 they had to carry a coal with them from campfire to camp

54
00:05:04,840 --> 00:05:06,280
fire.

55
00:05:06,280 --> 00:05:10,280
 The easiest way to keep fire was to carry something.

56
00:05:10,280 --> 00:05:14,280
 And if the fire went out, then you're in trouble.

57
00:05:14,280 --> 00:05:19,850
 You had a fire carrier. I read a story about it once anyway

58
00:05:19,850 --> 00:05:20,280
.

59
00:05:20,280 --> 00:05:27,280
 We have to care for the dhamma.

60
00:05:27,280 --> 00:05:30,280
 So how do we care for the dhamma?

61
00:05:30,280 --> 00:05:40,050
 What are the ways by which we ensure that this teaching

62
00:05:40,050 --> 00:05:42,280
 will continue?

63
00:05:42,280 --> 00:05:44,280
 So the Buddha gives seven things.

64
00:05:44,280 --> 00:05:49,280
 We take these things seriously and appreciate them.

65
00:05:49,280 --> 00:05:51,280
 And in a sense, worship them.

66
00:05:51,280 --> 00:05:56,590
 But worship, not in the way we use it, just assign it some

67
00:05:56,590 --> 00:05:58,280
 worth.

68
00:05:58,280 --> 00:06:00,530
 And of course, the first three are the Buddha, the dhamma,

69
00:06:00,530 --> 00:06:01,280
 and the sangha.

70
00:06:01,280 --> 00:06:04,280
 The first one is the sattah. Sattah means teacher.

71
00:06:04,280 --> 00:06:08,280
 But in Buddhism, it usually refers to the Buddha.

72
00:06:08,280 --> 00:06:10,280
 And here that's what it refers to.

73
00:06:10,280 --> 00:06:16,880
 It's a special word that is generally only used for the

74
00:06:16,880 --> 00:06:18,280
 Buddha.

75
00:06:18,280 --> 00:06:20,280
 But it literally means teacher.

76
00:06:20,280 --> 00:06:29,280
 Something like teacher.

77
00:06:29,280 --> 00:06:35,680
 And so if a monk or a bhikkhu or a bhikkhuni, a male or

78
00:06:35,680 --> 00:06:37,280
 female monk,

79
00:06:37,280 --> 00:06:45,280
 or a pasaka or a pasika, a male or female lay disciple,

80
00:06:45,280 --> 00:06:48,280
 it's just anyone who's a man or a woman,

81
00:06:48,280 --> 00:06:51,280
 and anyone who's in between.

82
00:06:51,280 --> 00:06:59,280
 Anyone at all, if they do not take the teacher seriously

83
00:06:59,280 --> 00:07:05,280
 and assign worth and appreciation to the teacher,

84
00:07:05,280 --> 00:07:09,280
 or the dhamma or the sangha,

85
00:07:09,280 --> 00:07:16,280
 they do not dwell agarawa vihranti.

86
00:07:16,280 --> 00:07:24,280
 They dwell without reverence, without appreciation,

87
00:07:24,280 --> 00:07:29,280
 without taking seriously.

88
00:07:29,280 --> 00:07:38,280
 And then the fourth is sikha, sikha yan.

89
00:07:38,280 --> 00:07:43,280
 This is the training, sikha.

90
00:07:43,280 --> 00:07:45,280
 Sikha means the training.

91
00:07:45,280 --> 00:07:50,280
 So the training in a general sense,

92
00:07:50,280 --> 00:07:59,280
 the training in giving up craving, training to give up.

93
00:07:59,280 --> 00:08:05,280
 And the training in morality,

94
00:08:05,280 --> 00:08:11,280
 the training in concentration and wisdom.

95
00:08:11,280 --> 00:08:16,280
 But here's specifically just the act of,

96
00:08:16,280 --> 00:08:20,250
 and the things that we do like practicing meditation and

97
00:08:20,250 --> 00:08:21,280
 keeping precepts.

98
00:08:21,280 --> 00:08:27,280
 So the things that we do and the things that we don't do.

99
00:08:27,280 --> 00:08:29,280
 We abstain from certain things

100
00:08:29,280 --> 00:08:35,280
 and we take on certain behaviors, certain practices.

101
00:08:35,280 --> 00:08:39,280
 This is the training and the learning,

102
00:08:39,280 --> 00:08:44,280
 studying, listening to the dhamma, remembering the dhamma,

103
00:08:44,280 --> 00:08:47,940
 reciting the dhamma, thinking about the dhamma, all of

104
00:08:47,940 --> 00:08:49,280
 these things.

105
00:08:49,280 --> 00:08:52,280
 This is all the training that we do.

106
00:08:52,280 --> 00:08:57,280
 Taking that seriously. If you don't take that seriously.

107
00:08:57,280 --> 00:09:00,280
 And number five is samadhi.

108
00:09:00,280 --> 00:09:03,280
 If you don't take concentration seriously,

109
00:09:03,280 --> 00:09:07,280
 the Buddha spells it out explicitly, concentration.

110
00:09:07,280 --> 00:09:11,280
 Samadhi is an interesting word.

111
00:09:11,280 --> 00:09:13,280
 It could just mean focus, you know.

112
00:09:13,280 --> 00:09:18,280
 Sammas is like same.

113
00:09:18,280 --> 00:09:21,280
 It means level or even.

114
00:09:21,280 --> 00:09:24,280
 So not too much, not too little.

115
00:09:24,280 --> 00:09:27,280
 When your mind gets perfectly focused.

116
00:09:27,280 --> 00:09:29,360
 That's why it's kind of like focus rather than

117
00:09:29,360 --> 00:09:30,280
 concentration.

118
00:09:30,280 --> 00:09:33,280
 Because it's like a camera lens.

119
00:09:33,280 --> 00:09:35,280
 If you focus too much, it gets blurry.

120
00:09:35,280 --> 00:09:37,280
 Too little also blurry.

121
00:09:37,280 --> 00:09:42,280
 You need perfect focus and then you can see.

122
00:09:42,280 --> 00:09:47,240
 Your mind has to be balanced is really what samadhi is all

123
00:09:47,240 --> 00:09:48,280
 about.

124
00:09:48,280 --> 00:09:51,280
 People think of samadhi as being concentrated.

125
00:09:51,280 --> 00:09:55,280
 So no thoughts.

126
00:09:55,280 --> 00:09:57,280
 Focused only on one thing.

127
00:09:57,280 --> 00:09:59,280
 No distractions.

128
00:09:59,280 --> 00:10:01,280
 But it's not necessarily that.

129
00:10:01,280 --> 00:10:05,280
 It just means the mind that is seeing things clearly.

130
00:10:05,280 --> 00:10:10,560
 The mind that is focused on something as it is in that

131
00:10:10,560 --> 00:10:12,280
 moment.

132
00:10:12,280 --> 00:10:16,280
 You don't have to block everything out and just concentrate

133
00:10:16,280 --> 00:10:17,280
 on a single thing.

134
00:10:17,280 --> 00:10:23,220
 You have to be focused on whatever arises clearly, seeing

135
00:10:23,220 --> 00:10:25,280
 it clearly.

136
00:10:25,280 --> 00:10:28,280
 That's why we use this word when we say to ourselves seeing

137
00:10:28,280 --> 00:10:31,280
 or hearing or rising, falling.

138
00:10:31,280 --> 00:10:33,280
 We're trying to focus.

139
00:10:33,280 --> 00:10:38,280
 Focus means on the core of it.

140
00:10:38,280 --> 00:10:42,280
 The core of this is rising or whatever rising means.

141
00:10:42,280 --> 00:10:44,280
 This is falling.

142
00:10:44,280 --> 00:10:47,280
 The core of seeing is seeing.

143
00:10:47,280 --> 00:10:49,280
 The core of pain is pain.

144
00:10:49,280 --> 00:10:52,280
 There's no judgments or reactions or anything.

145
00:10:52,280 --> 00:11:02,280
 Get rid of all of those.

146
00:11:02,280 --> 00:11:06,280
 Number six is apamada.

147
00:11:06,280 --> 00:11:13,280
 Apamade agarwa vihranti apatissa.

148
00:11:13,280 --> 00:11:23,220
 Apamade is the last words of the Buddha is that we should

149
00:11:23,220 --> 00:11:24,280
 cultivate apamade.

150
00:11:24,280 --> 00:11:27,280
 Pamade comes from the root mud.

151
00:11:27,280 --> 00:11:29,280
 Mud means to be drunk.

152
00:11:29,280 --> 00:11:33,280
 Pamade is like really drunk.

153
00:11:33,280 --> 00:11:38,280
 Bha is just this prefix that modifies it, that augments it.

154
00:11:38,280 --> 00:11:53,680
 So, pamade is like negligent or intoxicated, mixed up in

155
00:11:53,680 --> 00:11:55,280
 the mind.

156
00:11:55,280 --> 00:12:02,450
 Apamade is to be clear-minded, to be un-intoxicated, to be

157
00:12:02,450 --> 00:12:03,280
 sober.

158
00:12:03,280 --> 00:12:07,400
 So not drunk on lust, or drunk on anger, or drunk on

159
00:12:07,400 --> 00:12:13,280
 delusion or arrogance or conceit.

160
00:12:13,280 --> 00:12:16,280
 To not be drunk on any of these emotions.

161
00:12:16,280 --> 00:12:18,280
 So we should take that seriously.

162
00:12:18,280 --> 00:12:20,280
 We should appreciate that.

163
00:12:20,280 --> 00:12:25,280
 If we don't appreciate that, that's what the Buddha says.

164
00:12:25,280 --> 00:12:33,280
 And the seventh one is quite curious.

165
00:12:33,280 --> 00:12:39,280
 But curious in a good way.

166
00:12:39,280 --> 00:12:42,280
 It's just surprising, I think.

167
00:12:42,280 --> 00:12:45,280
 It's not what you'd expect as the last one.

168
00:12:45,280 --> 00:12:47,280
 Patti santara.

169
00:12:47,280 --> 00:12:56,280
 Ajahn, my teacher talked about this. He mentions this.

170
00:12:56,280 --> 00:13:02,280
 Many times the Buddha talks about Patti santara.

171
00:13:02,280 --> 00:13:06,280
 Patti santara ngarawa.

172
00:13:06,280 --> 00:13:09,280
 Patti santara agarawa.

173
00:13:09,280 --> 00:13:12,280
 If we don't take seriously or appreciate Patti santara.

174
00:13:12,280 --> 00:13:19,940
 Patti santara is, I don't quite know the etymology, but it

175
00:13:19,940 --> 00:13:26,280
 means hospitality.

176
00:13:26,280 --> 00:13:31,340
 Or it could mean friendliness, it could mean good will,

177
00:13:31,340 --> 00:13:32,280
 friendship.

178
00:13:32,280 --> 00:13:39,280
 But it's really just hospitality when you welcome people.

179
00:13:39,280 --> 00:13:42,280
 But in this context it makes perfect sense, right?

180
00:13:42,280 --> 00:13:44,880
 If you have a meditation center where you don't welcome

181
00:13:44,880 --> 00:13:45,280
 people,

182
00:13:45,280 --> 00:13:51,280
 when someone walks in they don't know.

183
00:13:51,280 --> 00:13:56,280
 Everyone looks at them, kind of, "What are you doing here?"

184
00:13:56,280 --> 00:13:59,770
 Sometimes you go to a monastery and no one wants to talk to

185
00:13:59,770 --> 00:14:00,280
 you.

186
00:14:00,280 --> 00:14:04,280
 Go to a meditation center and maybe no one, everyone's,

187
00:14:04,280 --> 00:14:10,530
 people are kind of putting on airs like they're real medit

188
00:14:10,530 --> 00:14:11,280
ators or something.

189
00:14:11,280 --> 00:14:15,550
 Or if they just don't care and they're just concerned about

190
00:14:15,550 --> 00:14:17,280
 their own practice.

191
00:14:17,280 --> 00:14:23,280
 If such a place existed, then how would you ever share?

192
00:14:23,280 --> 00:14:28,280
 How would that ever lead to spreading the dhamma?

193
00:14:28,280 --> 00:14:34,720
 There are places where you go into the office, meditation

194
00:14:34,720 --> 00:14:35,280
 center office

195
00:14:35,280 --> 00:14:40,280
 and they just yell at you.

196
00:14:40,280 --> 00:14:48,390
 They look down upon you and they don't know how to deal

197
00:14:48,390 --> 00:14:50,280
 with people.

198
00:14:50,280 --> 00:14:52,280
 And I've talked with other people.

199
00:14:52,280 --> 00:14:56,280
 In Chantang the people in the office would rotate.

200
00:14:56,280 --> 00:15:00,280
 And so you had to catch the right person in the office.

201
00:15:00,280 --> 00:15:02,280
 They never know what was going to happen.

202
00:15:02,280 --> 00:15:05,120
 They want to book a room for someone and maybe they yell at

203
00:15:05,120 --> 00:15:05,280
 you,

204
00:15:05,280 --> 00:15:07,280
 maybe they're very nice.

205
00:15:07,280 --> 00:15:14,280
 You have to find the right person and talk to them.

206
00:15:14,280 --> 00:15:17,470
 It's very important. So it's something for us to remember,

207
00:15:17,470 --> 00:15:19,280
 you have to welcome.

208
00:15:19,280 --> 00:15:22,280
 And in general you could talk about this.

209
00:15:22,280 --> 00:15:26,280
 And one thing I was thinking about is it would be neat

210
00:15:26,280 --> 00:15:27,280
 someday

211
00:15:27,280 --> 00:15:38,280
 if our community grows, if we could have groups of people,

212
00:15:38,280 --> 00:15:38,280
 you know.

213
00:15:38,280 --> 00:15:44,650
 Like maybe once a week we could have kind of a more formal

214
00:15:44,650 --> 00:15:47,280
 online session.

215
00:15:47,280 --> 00:15:54,280
 And if someone was organizing a group in their home

216
00:15:54,280 --> 00:16:00,280
 then they could join the hangout with their group.

217
00:16:00,280 --> 00:16:04,280
 Wow, wouldn't that be neat? That's what we should do.

218
00:16:04,280 --> 00:16:10,280
 Once a week, we could do once a month to start.

219
00:16:10,280 --> 00:16:14,280
 We have groups of people.

220
00:16:14,280 --> 00:16:19,280
 And someone puts together a group in their home

221
00:16:19,280 --> 00:16:24,280
 and people come to their home and they join the hangout.

222
00:16:24,280 --> 00:16:31,280
 And we could have up to ten groups.

223
00:16:31,280 --> 00:16:33,280
 And we have to talk about that.

224
00:16:33,280 --> 00:16:37,280
 So the idea is to welcome people even into your home.

225
00:16:37,280 --> 00:16:39,280
 You set up a Dhamma group.

226
00:16:39,280 --> 00:16:42,280
 People do this. They have a Dhamma group in their home.

227
00:16:42,280 --> 00:16:47,280
 They've got space.

228
00:16:47,280 --> 00:16:53,280
 And then they set up.

229
00:16:53,280 --> 00:16:55,280
 Sometimes they just meditate together.

230
00:16:55,280 --> 00:16:57,280
 Sometimes they listen to a talk.

231
00:16:57,280 --> 00:17:01,280
 I know Dhamma groups that just put on some CD or something,

232
00:17:01,280 --> 00:17:01,280
 you know,

233
00:17:01,280 --> 00:17:03,280
 somebody giving a talk.

234
00:17:03,280 --> 00:17:08,280
 And everybody listens and then they do meditation together.

235
00:17:08,280 --> 00:17:12,280
 So we could do something like that.

236
00:17:12,280 --> 00:17:14,280
 But it could be live, you know.

237
00:17:14,280 --> 00:17:18,280
 Live from all over the world. Ten different groups.

238
00:17:18,280 --> 00:17:22,280
 That's the maximum you can have ten people in the hangout.

239
00:17:22,280 --> 00:17:27,640
 So if anybody's interested in setting up a Dhamma group,

240
00:17:27,640 --> 00:17:29,280
 let me know.

241
00:17:29,280 --> 00:17:33,280
 And we'll try and arrange something where we meet together

242
00:17:33,280 --> 00:17:34,280
 like this

243
00:17:34,280 --> 00:17:41,280
 that will have a special day where I'll give a real talk.

244
00:17:41,280 --> 00:17:51,280
 I'll give a longer talk and then we'll connect.

245
00:17:51,280 --> 00:17:55,810
 Right. So those are the seven, that's one list of seven

246
00:17:55,810 --> 00:17:59,280
 things that lead to,

247
00:17:59,280 --> 00:18:01,280
 lead to the dumb lasting.

248
00:18:01,280 --> 00:18:04,280
 So if we don't spread it, if we don't share it,

249
00:18:04,280 --> 00:18:07,280
 if we're not welcoming of people who want to learn.

250
00:18:07,280 --> 00:18:10,280
 Now the Buddha wasn't big on spreading the Dhamma

251
00:18:10,280 --> 00:18:14,280
 in terms of going around teaching people.

252
00:18:14,280 --> 00:18:18,750
 But as far as I can see, much more about welcoming people

253
00:18:18,750 --> 00:18:20,280
 who wanted to learn.

254
00:18:20,280 --> 00:18:24,520
 If people wanted to learn, it was all about finding ways to

255
00:18:24,520 --> 00:18:26,280
 accommodate them.

256
00:18:26,280 --> 00:18:29,390
 If they do come, if they don't come, they don't have to go

257
00:18:29,390 --> 00:18:31,280
 out of their way looking for people.

258
00:18:31,280 --> 00:18:39,280
 We're not trying to push at this on anybody.

259
00:18:39,280 --> 00:18:42,280
 That's kind of how beautiful it is. We don't need students.

260
00:18:42,280 --> 00:18:44,280
 We're not looking for students.

261
00:18:44,280 --> 00:18:49,860
 Just people who are looking for it, we open the door for

262
00:18:49,860 --> 00:18:50,280
 them,

263
00:18:50,280 --> 00:18:52,280
 provide them the opportunity.

264
00:18:52,280 --> 00:18:56,390
 Tonight a woman came to visit, she lived in Cambodia for

265
00:18:56,390 --> 00:18:57,280
 nine years.

266
00:18:57,280 --> 00:19:02,030
 She just came and she's seen some of my videos and I gave

267
00:19:02,030 --> 00:19:07,280
 her the booklet.

268
00:19:07,280 --> 00:19:10,120
 And then she just did meditation. I didn't meditate with

269
00:19:10,120 --> 00:19:10,280
 her.

270
00:19:10,280 --> 00:19:13,790
 I came upstairs and did my, because I do walking, she didn

271
00:19:13,790 --> 00:19:15,280
't want to do walking.

272
00:19:15,280 --> 00:19:22,280
 But it would be nice if we could have a group here as well.

273
00:19:22,280 --> 00:19:24,670
 I think probably what we'd do is just have people come up

274
00:19:24,670 --> 00:19:25,280
 here at nine.

275
00:19:25,280 --> 00:19:28,950
 If someone wants to hear the Dhamma, they can come at nine,

276
00:19:28,950 --> 00:19:34,280
 come sit up with us here.

277
00:19:34,280 --> 00:19:40,280
 For now anyway. We're looking to get a bigger place.

278
00:19:40,280 --> 00:19:45,280
 Anyway, so these seven, the Buddha, the Dhamma, the Sangha,

279
00:19:45,280 --> 00:19:45,280
 take them seriously.

280
00:19:45,280 --> 00:19:48,430
 That's another thing is often people don't take the Buddhas

281
00:19:48,430 --> 00:19:50,280
 too seriously.

282
00:19:50,280 --> 00:19:55,540
 These talks about people who spit on Buddha images and burn

283
00:19:55,540 --> 00:19:56,280
 them

284
00:19:56,280 --> 00:20:01,070
 and just treat them like rubbish, thinking that's not the

285
00:20:01,070 --> 00:20:02,280
 real Buddha.

286
00:20:02,280 --> 00:20:04,280
 But there's something to it.

287
00:20:04,280 --> 00:20:07,520
 If you don't take the Buddha images seriously, what does

288
00:20:07,520 --> 00:20:10,280
 that say about how you feel about the Buddha?

289
00:20:10,280 --> 00:20:13,630
 Yeah, people say it's just an image in the Buddhas and

290
00:20:13,630 --> 00:20:17,280
 whatever, but there's something, two images.

291
00:20:17,280 --> 00:20:21,190
 They represent something. In ancient times they wouldn't

292
00:20:21,190 --> 00:20:22,280
 even make images

293
00:20:22,280 --> 00:20:26,200
 because they revered the Buddha, it seems, because they

294
00:20:26,200 --> 00:20:28,280
 revered the Buddha so much.

295
00:20:28,280 --> 00:20:31,530
 So when we have these images, we try to treat them quite

296
00:20:31,530 --> 00:20:32,280
 carefully

297
00:20:32,280 --> 00:20:35,280
 because we respect the Buddha so much.

298
00:20:35,280 --> 00:20:39,030
 If you don't, I mean, this is the thing, the Dhamma won't

299
00:20:39,030 --> 00:20:42,280
 last because there's no figure,

300
00:20:42,280 --> 00:20:47,060
 there's none of this sort of religious feeling that keeps

301
00:20:47,060 --> 00:20:48,280
 things together,

302
00:20:48,280 --> 00:20:53,720
 the sense of urgency, the sense of zeal and interest that's

303
00:20:53,720 --> 00:20:55,280
 so powerful in any religion.

304
00:20:55,280 --> 00:20:59,280
 It could be for the purposes of evil, it could be for the

305
00:20:59,280 --> 00:21:01,280
 purposes of good, but it's a power.

306
00:21:01,280 --> 00:21:04,880
 If you don't take these things seriously, or don't

307
00:21:04,880 --> 00:21:07,280
 appreciate them, don't revere them,

308
00:21:07,280 --> 00:21:11,280
 it's very hard to keep going.

309
00:21:11,280 --> 00:21:15,740
 If you talk about secular Buddhism, yeah, it's fine, but it

310
00:21:15,740 --> 00:21:19,280
's hard to get that feeling and appreciation.

311
00:21:19,280 --> 00:21:27,280
 Part of religion is the feeling, the sense of reverence,

312
00:21:27,280 --> 00:21:29,280
 appreciation,

313
00:21:29,280 --> 00:21:33,060
 not just taking something clinically in terms of, yes, this

314
00:21:33,060 --> 00:21:34,280
 helps me relieve stress,

315
00:21:34,280 --> 00:21:38,520
 but yes, this is the teaching of the perfectly enlightened

316
00:21:38,520 --> 00:21:39,280
 Buddha, you know,

317
00:21:39,280 --> 00:21:45,280
 it carries a lot more weight in that sense.

318
00:21:45,280 --> 00:21:50,280
 And then the training, concentration,

319
00:21:50,280 --> 00:22:00,260
 and Apamada, which is vigilance or sobriety and hospitality

320
00:22:00,260 --> 00:22:01,280
.

321
00:22:01,280 --> 00:22:04,520
 We have to take the training seriously, both study and

322
00:22:04,520 --> 00:22:05,280
 practice,

323
00:22:05,280 --> 00:22:08,940
 we have to take concentration, remember to try and be

324
00:22:08,940 --> 00:22:10,280
 concentrated,

325
00:22:10,280 --> 00:22:15,280
 remember and practice, not just study.

326
00:22:15,280 --> 00:22:20,970
 And be mindful, Apamada actually being sober, the Buddha

327
00:22:20,970 --> 00:22:24,280
 said it's a synonym for being mindful,

328
00:22:24,280 --> 00:22:27,450
 so this really means using mindfulness, seeing things

329
00:22:27,450 --> 00:22:29,280
 clearly as they are,

330
00:22:29,280 --> 00:22:34,900
 grasping things as they are, remembering things as they are

331
00:22:34,900 --> 00:22:35,280
.

332
00:22:35,280 --> 00:22:40,010
 And finally we have to be hospitable, so we have to welcome

333
00:22:40,010 --> 00:22:41,280
 people to join,

334
00:22:41,280 --> 00:22:44,500
 not just practicing for ourselves, but providing the

335
00:22:44,500 --> 00:22:49,280
 opportunity and being friendly and welcoming.

336
00:22:49,280 --> 00:22:52,460
 Don't just shy away and say, "Oh, I'm not a teacher, I can

337
00:22:52,460 --> 00:22:55,280
't teach you the Dhamma, anyone can teach."

338
00:22:55,280 --> 00:22:58,280
 You teach how you were taught, pass it on.

339
00:22:58,280 --> 00:23:01,280
 It doesn't mean you have to answer all of their questions

340
00:23:01,280 --> 00:23:04,280
 and problems and give them a device,

341
00:23:04,280 --> 00:23:06,540
 it just means you have to explain to them how to do

342
00:23:06,540 --> 00:23:10,280
 meditation, which is quite simple.

343
00:23:10,280 --> 00:23:13,920
 You can just reassure them that it has benefits and if they

344
00:23:13,920 --> 00:23:17,280
 try it they will see the benefits for themselves.

345
00:23:17,280 --> 00:23:25,280
 That's all you need to do.

346
00:23:25,280 --> 00:23:36,280
 So, that's the Dhamma for tonight.

347
00:23:36,280 --> 00:23:37,280
 [

348
00:23:37,280 --> 00:24:05,280
 contemplating my good fortune.]

349
00:24:05,280 --> 00:24:08,280
 [I'm contemplating and not noting.]

350
00:24:08,280 --> 00:24:12,120
 [How should one balance the process of noting and the

351
00:24:12,120 --> 00:24:15,280
 process of wholesome contemplation?]

352
00:24:15,280 --> 00:24:21,280
 Well, if that thought arises, that's fine, you can do both.

353
00:24:21,280 --> 00:24:23,790
 You just have the thought and then you say thinking,

354
00:24:23,790 --> 00:24:31,280
 thinking, or if you feel happy you can say happy, happy.

355
00:24:31,280 --> 00:24:36,890
 And the thought arises on its own, we're just trying to be

356
00:24:36,890 --> 00:24:43,280
 mindful of it because even wholesomeness can be caught up

357
00:24:43,280 --> 00:24:45,280
 in delusion.

358
00:24:45,280 --> 00:24:51,520
 Not directly, but you can become unwholesome about your

359
00:24:51,520 --> 00:24:56,070
 wholesomeness if you start to get attached to it, attached

360
00:24:56,070 --> 00:24:58,280
 to the idea of it anyway.

361
00:24:58,280 --> 00:25:04,870
 [I'm not saying that it's a good idea to be mindful of it,

362
00:25:04,870 --> 00:25:09,280
 but it's a good idea to be mindful of it.]

363
00:25:09,280 --> 00:25:16,450
 I mean, it won't lead to freedom, so you can switch back

364
00:25:16,450 --> 00:25:17,280
 and forth.

365
00:25:17,280 --> 00:25:23,320
 For the death one especially, if you're mindful of death,

366
00:25:23,320 --> 00:25:24,280
 that's a useful meditation.

367
00:25:24,280 --> 00:25:27,580
 It's good to give you the impetus to practice, gives you

368
00:25:27,580 --> 00:25:34,280
 those religious feeling, "sangwega" we call it.

369
00:25:34,280 --> 00:25:37,740
 So that's mindful, it's a different meditation and it's

370
00:25:37,740 --> 00:25:40,540
 useful to practice that and then go back to practicing

371
00:25:40,540 --> 00:25:41,280
 mindfulness.

372
00:25:41,280 --> 00:25:49,890
 As far as the contemplation of good fortune, be careful of

373
00:25:49,890 --> 00:25:56,280
 that because it can slip into complacency.

374
00:25:56,280 --> 00:26:01,270
 The angels think like that, they think everything, they

375
00:26:01,270 --> 00:26:06,280
 think they've got some safety or so on.

376
00:26:06,280 --> 00:26:09,430
 You're probably not having that problem, but you have to be

377
00:26:09,430 --> 00:26:14,280
 careful of that. I'm not convinced that it's wholesome.

378
00:26:14,280 --> 00:26:17,800
 You're talking about contentment, there's something in

379
00:26:17,800 --> 00:26:21,280
 there that's probably associated with contentment.

380
00:26:21,280 --> 00:26:29,910
 But appreciation is a lot like liking and clinging because

381
00:26:29,910 --> 00:26:33,280
 things can change at any time, right?

382
00:26:33,280 --> 00:26:38,690
 Your safety is completely impermanent, so it's an illusion.

383
00:26:38,690 --> 00:26:44,280
 There is no safety in samsara.

384
00:26:44,280 --> 00:26:50,030
 Everything can leave you in the moment. So in what way is

385
00:26:50,030 --> 00:26:51,280
 it safe?

386
00:26:51,280 --> 00:26:59,010
 In the end it's all just seeing, hearing, smelling, tasting

387
00:26:59,010 --> 00:27:02,280
, feeling, thinking.

388
00:27:02,280 --> 00:27:06,280
 I'd be continuing the series on the Dhammapada.

389
00:27:06,280 --> 00:27:09,560
 Probably the only reason I continue them is because people

390
00:27:09,560 --> 00:27:11,280
 ask me the questions like this.

391
00:27:11,280 --> 00:27:14,500
 Because I always think, "Oh well, maybe nobody wants them

392
00:27:14,500 --> 00:27:15,280
 anymore."

393
00:27:15,280 --> 00:27:21,420
 I haven't heard, nobody's asked about them in a while, so

394
00:27:21,420 --> 00:27:25,280
 probably people are sick of them.

395
00:27:25,280 --> 00:27:33,970
 I was busy with finals, but now I'm not. Sure, I can start

396
00:27:33,970 --> 00:27:39,280
 up the Dhammapada series again.

397
00:27:39,280 --> 00:27:47,660
 It's not that I stopped, I can continue to do more Dhammap

398
00:27:47,660 --> 00:27:51,280
ada if people want it.

399
00:27:51,280 --> 00:27:54,450
 How is it that we bow down to and revere the meditation

400
00:27:54,450 --> 00:27:57,280
 practice, the practice itself?

401
00:27:57,280 --> 00:28:02,000
 Should we hold the triple gem or the practice in mind for

402
00:28:02,000 --> 00:28:03,280
 reverence?

403
00:28:03,280 --> 00:28:05,610
 I don't think you need to hold anything in mind for

404
00:28:05,610 --> 00:28:10,280
 reverence. It's just taking it seriously.

405
00:28:10,280 --> 00:28:13,700
 You can do meditations on the Buddha, the Dhamma and the

406
00:28:13,700 --> 00:28:15,280
 Sangha, obviously.

407
00:28:15,280 --> 00:28:17,500
 But that's not what I don't think the Buddha is saying. He

408
00:28:17,500 --> 00:28:20,280
's just, if they don't respect.

409
00:28:20,280 --> 00:28:24,340
 Because there's a lot of disrespect for the Sangha, for

410
00:28:24,340 --> 00:28:25,280
 example.

411
00:28:25,280 --> 00:28:30,530
 Monks get a lot of disrespect. Teachers get a lot of

412
00:28:30,530 --> 00:28:32,280
 disrespect.

413
00:28:32,280 --> 00:28:37,280
 Not a lot, mostly respect. But there's always people who

414
00:28:37,280 --> 00:28:39,280
 have very little respect.

415
00:28:39,280 --> 00:28:46,580
 The point is, well, that is harmful to the... You could say

416
00:28:46,580 --> 00:28:51,280
 respect has to be earned, sure, fine.

417
00:28:51,280 --> 00:28:56,410
 But there's something about being respectful. If you don't

418
00:28:56,410 --> 00:29:00,280
 want to practice, you don't have to come practice.

419
00:29:00,280 --> 00:29:06,110
 But if someone's teaching, there's a lot of... And then

420
00:29:06,110 --> 00:29:09,280
 there's disrespect to the Buddha.

421
00:29:09,280 --> 00:29:19,280
 Disrespect to the Dhamma. Not taking it seriously.

422
00:29:19,280 --> 00:29:24,860
 Not necessarily disrespect, but not taking it at practice

423
00:29:24,860 --> 00:29:26,280
 seriously.

424
00:29:26,280 --> 00:29:32,280
 People who do walking meditation, talking on the phone.

425
00:29:32,280 --> 00:29:36,580
 And it's not even how disrespectful that is. It's just, if

426
00:29:36,580 --> 00:29:38,610
 you don't take it seriously, you're not going to get any

427
00:29:38,610 --> 00:29:39,280
 results.

428
00:29:39,280 --> 00:29:41,840
 So the question, if you do an hour of meditation, how much

429
00:29:41,840 --> 00:29:45,140
 are you really meditating? Are you really taking it

430
00:29:45,140 --> 00:29:46,280
 seriously?

431
00:29:46,280 --> 00:29:50,580
 Are you... Do you respect? And it's not respect in terms of

432
00:29:50,580 --> 00:29:54,280
 disrespect. It's like, do you appreciate? That's the best.

433
00:29:54,280 --> 00:29:57,630
 Do you appreciate? Garo means heavy, so it's like taking

434
00:29:57,630 --> 00:30:00,280
 seriously, seeing it as a weighty thing.

435
00:30:00,280 --> 00:30:04,370
 Or do you see it... Do you take it lightly? Right? It's the

436
00:30:04,370 --> 00:30:06,280
 opposite of taking something lightly.

437
00:30:06,280 --> 00:30:10,140
 If you take meditation lightly, it's hard. You won't get

438
00:30:10,140 --> 00:30:11,280
 the results.

439
00:30:11,280 --> 00:30:16,280
 And if you don't get the results, Buddhism will pass away.

440
00:30:16,280 --> 00:30:27,110
 But yeah, if you want to do this, if you want to pay

441
00:30:27,110 --> 00:30:32,280
 respect, like, there's a quick chant that we do.

442
00:30:32,280 --> 00:30:35,870
 We do it in the opening ceremony, so I often do it as a

443
00:30:35,870 --> 00:30:38,280
 kind of like a mantra, really.

444
00:30:38,280 --> 00:30:42,490
 We pay respect to the five things, the Buddha, the Dhamma,

445
00:30:42,490 --> 00:30:46,470
 the Sangha, the meditation practice, and the teacher who

446
00:30:46,470 --> 00:30:48,280
 offers the meditation practice.

447
00:30:48,280 --> 00:30:57,240
 Namami buddhan guna sahgarantan. Namami dhammang muniraja d

448
00:30:57,240 --> 00:30:58,280
hisitang.

449
00:30:58,280 --> 00:31:03,960
 Namami sankang muniraja sahvakang. Namami kamathan nirbhan

450
00:31:03,960 --> 00:31:05,280
 adhika mupayam.

451
00:31:05,280 --> 00:31:10,280
 Namami kamathan adhika chariyang nirbhan amagudhesakang.

452
00:31:10,280 --> 00:31:19,350
 Sabang do sang kamantu no. The last part, sabang do sang

453
00:31:19,350 --> 00:31:23,130
 for all faults, for all faults, kamantu no, may they

454
00:31:23,130 --> 00:31:24,280
 forgive us.

455
00:31:24,280 --> 00:31:37,280
 May they forgive us all, discretions, any wrongdoing.

456
00:31:37,280 --> 00:31:39,960
 It's in the opening ceremony, I'm not sure where you get

457
00:31:39,960 --> 00:31:41,280
 the opening ceremony.

458
00:31:41,280 --> 00:31:45,840
 I think it's in our, where is the opening ceremony? Maybe

459
00:31:45,840 --> 00:31:48,280
 it's not even online.

460
00:31:48,280 --> 00:31:52,880
 But it might be taken from the Visuddhi manga. A lot of

461
00:31:52,880 --> 00:31:56,280
 that is taken from the Visuddhi manga.

462
00:31:56,280 --> 00:32:12,280
 Eh, maybe not.

463
00:32:12,280 --> 00:32:15,280
 Yeah, respect for the five.

464
00:32:15,280 --> 00:32:19,490
 It's what we do before we start the minute. It's the first

465
00:32:19,490 --> 00:32:23,890
 thing we do in the opening ceremony. Almost the first thing

466
00:32:23,890 --> 00:32:28,280
. First big thing.

467
00:32:28,280 --> 00:32:30,930
 No, no, this is the first thing, isn't it? Yeah. It's how

468
00:32:30,930 --> 00:32:33,280
 we start the opening ceremony and the closing ceremony.

469
00:32:33,280 --> 00:32:38,200
 So I don't do ceremonies here, not yet anyway, but normally

470
00:32:38,200 --> 00:32:42,740
 we'd have a ceremony where we go through all of this in Pal

471
00:32:42,740 --> 00:32:45,280
i. It's quite nice.

472
00:32:45,280 --> 00:32:50,380
 The kind of thing you should take it seriously, you know.

473
00:32:50,380 --> 00:32:53,390
 If we were to take it seriously, we probably should do the

474
00:32:53,390 --> 00:32:54,280
 ceremonies.

475
00:32:54,280 --> 00:32:58,750
 But you know, in the West, sometimes, again, people don't

476
00:32:58,750 --> 00:33:03,280
 take it seriously enough. It's hard to push that on them.

477
00:33:03,280 --> 00:33:06,500
 It's not entirely necessary. It doesn't mean you do a

478
00:33:06,500 --> 00:33:09,280
 ceremony. That's the thing, it's just a ceremony.

479
00:33:09,280 --> 00:33:13,280
 But something to consider once we get established here.

480
00:33:13,280 --> 00:33:16,060
 Maybe I can teach Michael, because another thing is you

481
00:33:16,060 --> 00:33:17,280
 need two people.

482
00:33:17,280 --> 00:33:21,400
 I do part, but I need a layperson to lead the meditators. I

483
00:33:21,400 --> 00:33:25,350
 need someone to lead the meditators. I'll teach Michael how

484
00:33:25,350 --> 00:33:26,280
 to do it someday.

485
00:33:26,280 --> 00:33:33,130
 If he sticks around. Maybe he'll become a monk. If he does,

486
00:33:33,130 --> 00:33:36,280
 I get to give him the name Mogaraja again.

487
00:33:36,280 --> 00:33:41,330
 Because it's the closest thing to Michael, right? So our

488
00:33:41,330 --> 00:33:46,280
 last Michael got the name Mogaraja, and boy did that cause

489
00:33:46,280 --> 00:33:47,280
 a stir.

490
00:33:47,280 --> 00:33:50,570
 You know, Sri Lankan people said they didn't want to bow

491
00:33:50,570 --> 00:33:56,910
 down to it, because the word "Moga" means useless or bad or

492
00:33:56,910 --> 00:33:58,280
 stupid.

493
00:33:58,280 --> 00:34:02,100
 But Mogaraja was one of the Buddhist chief disciples. One

494
00:34:02,100 --> 00:34:06,280
 of the eighty great disciples. It's a shame, really.

495
00:34:13,280 --> 00:34:19,330
 Yeah, I have the five reverences somewhere. Not sure where,

496
00:34:19,330 --> 00:34:25,280
 but probably buried away somewhere in my documents folder.

497
00:34:25,280 --> 00:34:36,930
 I could copy them. Anyway, that's all for tonight. Thank

498
00:34:36,930 --> 00:34:41,280
 you all for tuning in. Have a good night.

499
00:34:41,280 --> 00:34:45,280
 Thank you.

