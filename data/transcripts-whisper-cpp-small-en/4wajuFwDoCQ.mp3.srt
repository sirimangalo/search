1
00:00:00,000 --> 00:00:08,190
 Hello everyone, this video is an announcement to let it be

2
00:00:08,190 --> 00:00:09,920
 known that I have now officially

3
00:00:09,920 --> 00:00:16,970
 reached 200 uploaded videos to YouTube, which seems like

4
00:00:16,970 --> 00:00:19,120
 quite a lot to me.

5
00:00:19,120 --> 00:00:25,190
 And so to commemorate this landmark, I thought I would do

6
00:00:25,190 --> 00:00:28,720
 something special and that is to

7
00:00:28,720 --> 00:00:32,920
 put a request out to all of you to show me what you've

8
00:00:32,920 --> 00:00:35,440
 gained from the teachings, from

9
00:00:35,440 --> 00:00:36,880
 the meditation practice.

10
00:00:36,880 --> 00:00:39,950
 Specifically, what I'd like to ask is that if you don't

11
00:00:39,950 --> 00:00:41,760
 have a YouTube account, make

12
00:00:41,760 --> 00:00:46,460
 one if you have one already, upload a video of yourself med

13
00:00:46,460 --> 00:00:48,960
itating, either practicing

14
00:00:48,960 --> 00:00:53,270
 mindful prostration, walking meditation, or sitting

15
00:00:53,270 --> 00:00:54,480
 meditation.

16
00:00:54,480 --> 00:00:59,390
 One of these three, according to the technique that I teach

17
00:00:59,390 --> 00:01:02,160
, and post it as a video response

18
00:01:02,160 --> 00:01:05,720
 to this video that you're watching right now.

19
00:01:05,720 --> 00:01:09,030
 Once you do that, and once I get enough of them of all

20
00:01:09,030 --> 00:01:11,120
 three types, you could upload

21
00:01:11,120 --> 00:01:14,140
 one video of you doing all three of them or three videos of

22
00:01:14,140 --> 00:01:15,760
 you doing each one of them.

23
00:01:15,760 --> 00:01:19,390
 It only has to be a short video if you just, just as an

24
00:01:19,390 --> 00:01:22,100
 example, you don't have to actually

25
00:01:22,100 --> 00:01:24,560
 meditate for 10 minutes or 20 minutes or so on.

26
00:01:24,560 --> 00:01:27,040
 As a YouTube video, just give me an example of how you

27
00:01:27,040 --> 00:01:27,960
 would meditate.

28
00:01:27,960 --> 00:01:30,800
 It could be showing how you do the prostration, showing how

29
00:01:30,800 --> 00:01:32,440
 you do the walking, showing how

30
00:01:32,440 --> 00:01:37,240
 you set yourself doing the sitting, and post it, or post

31
00:01:37,240 --> 00:01:40,040
 all three of them, if it's three

32
00:01:40,040 --> 00:01:42,800
 different videos.

33
00:01:42,800 --> 00:01:49,280
 And I'll download all of these and make a montage out of

34
00:01:49,280 --> 00:01:52,400
 them and then repost it as

35
00:01:52,400 --> 00:01:55,920
 a look at how people put these teachings into practice in

36
00:01:55,920 --> 00:01:58,040
 all parts of the world, from all

37
00:01:58,040 --> 00:02:01,520
 walks of life, in all different situations.

38
00:02:01,520 --> 00:02:05,090
 So find a unique place, the place where you normally med

39
00:02:05,090 --> 00:02:05,760
itate.

40
00:02:05,760 --> 00:02:07,640
 It could be in your room.

41
00:02:07,640 --> 00:02:13,630
 It could be in the forest, in a park, in your office where

42
00:02:13,630 --> 00:02:15,040
 you work.

43
00:02:15,040 --> 00:02:19,390
 If you go to school, it could be in the school library, or

44
00:02:19,390 --> 00:02:21,680
 it could be in an airport, or

45
00:02:21,680 --> 00:02:24,220
 wherever you have been able to put the teachings into

46
00:02:24,220 --> 00:02:24,960
 practice.

47
00:02:24,960 --> 00:02:28,570
 It doesn't have to be the most serene and calm environment,

48
00:02:28,570 --> 00:02:30,200
 just a place where you've

49
00:02:30,200 --> 00:02:36,140
 been putting the teachings into practice in one of these

50
00:02:36,140 --> 00:02:39,640
 three formal modes of the prostrations,

51
00:02:39,640 --> 00:02:41,800
 the walking, or the sitting.

52
00:02:41,800 --> 00:02:48,150
 Okay, so looking forward to it and hope to see your

53
00:02:48,150 --> 00:02:50,360
 responses come in.

54
00:02:50,360 --> 00:02:55,420
 And just to say thanks to everyone for helping me to get

55
00:02:55,420 --> 00:02:56,600
 this far.

56
00:02:56,600 --> 00:02:59,440
 It's only because of the enthusiasm and encouragement and

57
00:02:59,440 --> 00:03:01,640
 the benefits that people say they're getting

58
00:03:01,640 --> 00:03:07,480
 out of these teachings that I keep doing what I'm doing.

59
00:03:07,480 --> 00:03:15,280
 So keep it up and keep giving your comments and questions

60
00:03:15,280 --> 00:03:20,160
 and subscribing to this channel.

61
00:03:20,160 --> 00:03:22,280
 And most importantly, keep meditating, okay?

62
00:03:22,280 --> 00:03:26,320
 Hope to hear from you and all the best to everyone.

63
00:03:26,320 --> 00:03:36,320
 [BLANK_AUDIO]

