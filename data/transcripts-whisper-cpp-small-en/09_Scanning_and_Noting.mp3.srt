1
00:00:00,000 --> 00:00:06,230
 Stunning and noting question. When I sit and say to myself

2
00:00:06,230 --> 00:00:09,280
 sitting, I have to do a quick body scan.

3
00:00:09,280 --> 00:00:13,440
 A list of my legs and spine in order to really know that I

4
00:00:13,440 --> 00:00:15,440
 am indeed sitting.

5
00:00:15,440 --> 00:00:19,640
 Isn't that quick body scan somewhat contrary to a one point

6
00:00:19,640 --> 00:00:22,160
 of mind? Is one label really enough?

7
00:00:22,160 --> 00:00:27,440
 Answer. You do not need to perform any such body scan.

8
00:00:27,440 --> 00:00:31,550
 Trying to force the perception of sitting is not proper

9
00:00:31,550 --> 00:00:34,240
 meditation because it reinforces the

10
00:00:34,240 --> 00:00:38,980
 delusion of control. In tranquility meditation, it might be

11
00:00:38,980 --> 00:00:41,200
 okay to control the mind. If your

12
00:00:41,200 --> 00:00:45,270
 purpose is simply to calm the mind, you can force it, push

13
00:00:45,270 --> 00:00:48,240
 it, knead it, and mould it into shape.

14
00:00:48,240 --> 00:00:52,230
 Tranquility meditation is not for the purpose of

15
00:00:52,230 --> 00:00:55,600
 cultivating insight, so there is no harm there

16
00:00:55,600 --> 00:00:59,950
 as far as attaining the goal of tranquility. In repassana

17
00:00:59,950 --> 00:01:03,280
 meditation, however, the focus is not

18
00:01:03,280 --> 00:01:07,240
 the concept of sitting, but rather sitting is a description

19
00:01:07,240 --> 00:01:09,920
 of the feelings that you experience.

20
00:01:09,920 --> 00:01:13,610
 The fact that you are aware of the tension in your back or

21
00:01:13,610 --> 00:01:15,920
 the pressure on the floor at that moment

22
00:01:15,920 --> 00:01:19,650
 when you say sitting is enough. You are aware of that

23
00:01:19,650 --> 00:01:22,560
 experience and you call that experience

24
00:01:22,560 --> 00:01:26,650
 sitting. It is important to recognize the physical

25
00:01:26,650 --> 00:01:30,400
 phenomena as they are. It is not so important to

26
00:01:30,400 --> 00:01:34,690
 be aware that this is a sitting posture. Push your mind on

27
00:01:34,690 --> 00:01:37,280
 the entire body and say to yourself,

28
00:01:37,280 --> 00:01:40,960
 "Sitting." The recognition of the sitting posture comes

29
00:01:40,960 --> 00:01:43,520
 from its own or may not come at all.

30
00:01:43,520 --> 00:01:47,940
 This is the sannyā part of repassana, the recognition that

31
00:01:47,940 --> 00:01:50,000
 this is sitting. Sannyā

32
00:01:50,000 --> 00:01:53,670
 recognition is unpredictable and what you are seeing that

33
00:01:53,670 --> 00:01:56,160
 leads you to say that you have to push

34
00:01:56,160 --> 00:02:00,610
 it in order for the recognition to come is non-self. The

35
00:02:00,610 --> 00:02:02,480
 recognition does not always come

36
00:02:02,480 --> 00:02:06,260
 automatically. Sometimes it does come and you immediately

37
00:02:06,260 --> 00:02:08,160
 know that you are sitting and you

38
00:02:08,160 --> 00:02:12,590
 recognize this is a sitting posture. Sometimes you look and

39
00:02:12,590 --> 00:02:15,120
 look and you never become aware that it

40
00:02:15,120 --> 00:02:19,420
 is sitting. This is because it is not so. You cannot force

41
00:02:19,420 --> 00:02:22,240
 yourself to know that it is sitting,

42
00:02:22,240 --> 00:02:26,310
 that you are experiencing. Understanding the nature of

43
00:02:26,310 --> 00:02:28,640
 reality in this way is the purpose

44
00:02:28,640 --> 00:02:33,260
 of practicing mindfulness. You will find that sometimes a

45
00:02:33,260 --> 00:02:36,400
 certain sensation or set of sensations

46
00:02:36,400 --> 00:02:39,850
 leads to the arising of the recognition that you are

47
00:02:39,850 --> 00:02:42,880
 sitting. Sometimes such a recognition will

48
00:02:42,880 --> 00:02:47,440
 not arise. Often meditators believe that something is wrong

49
00:02:47,440 --> 00:02:49,520
 and they are unable to force the

50
00:02:49,520 --> 00:02:54,500
 recognition to arise. What they are experiencing, however,

51
00:02:54,500 --> 00:02:57,760
 is the nature of reality as impermanent,

52
00:02:57,760 --> 00:03:02,380
 suffering and non-self. The purpose of meditation is to see

53
00:03:02,380 --> 00:03:04,240
 these characteristics.

54
00:03:04,240 --> 00:03:08,510
 So in this instance they can be reassured that they are

55
00:03:08,510 --> 00:03:10,160
 practicing correctly.

56
00:03:10,160 --> 00:03:11,160
 [

