 Good evening and welcome back to our study of the Dhammap
ada.
 Tonight we continue on with verse number 95, which reads as
 follows.
 Patavi samo novi rujjati, indaki lupamo tadisubhato, rahado
 apeta kadamo, sangsara nabhavanti tadino,
 which means like the earth one is not disturbed, like the,
 or just as the earth is not disturbed,
 is not just like the earth that is undisturbed or just like
 just as the earth is not undisturbed is not obstructed.
 Whatever that means, just as a foundation pillar, indaki l
upamo, just like an indaki lupamo or a foundation post is
 such a one of,
 good behavior or good deeds, subat do. So I guess this is
 translated, he is undisturbed like the earth.
 That's what it is. Patavi samo novi rujjati, he is undist
urbed like the earth, one who is of good behavior or of good
 manners is like a foundation post,
 is firmly, you know, well-founded, unshakable. Rahado apeta
 kadamo, like a lake that has become un-muddied, that is
 clear and free from cloudiness.
 Kadama is mud. Sangsara nabhavanti tadino, there is no
 wandering on, there is no round of samsara, there is no,
 no trans-migration from one life to the next, born old,
 sick and die, being born old, getting old, getting sick and
 dying again and again for such a person.
 So, like the earth, it is unobstructed, untroubled maybe,
 founded like a foundation post.
 Like a lake that is clear and un-muddied, for such a person
 there is no more becoming.
 This is a description of Sariputta. So the story is about a
 famous incident that occurred in regards to Sariputta.
 Sariputta got the idea that he would go off on a wandering
 tour.
 So we had one story where the Buddha went off on a
 wandering tour and Mahakasapa turned back.
 It's a story about Mahakasapa. Here's a story about Sariput
ta.
 And it seems that Sariputta was having monks to go with him
, talking to monks about going with him or staying behind.
 And there was one monk who Sariputta didn't know by name or
 who his name doesn't even show up.
 And Sariputta was addressing monks by name. And this monk
 thought to himself, "Oh, Sariputta will address me by name
."
 And he never did. Sariputta just said, "Okay, and the rest
 of you do this or do that."
 He didn't call him out, which it seems like such a small
 thing really.
 But it reminds me when I was a new monk, and it's a story
 that I'll always remember.
 When I returned to stay with my teacher as a monk, I always
 wanted him to...
 It was that he didn't remember my name. That's what it was.
 I was always kind of disappointed that he didn't remember
 my name, and he would always ask, "What's his name?"
 And when I told him my name, he said, "No, what kind of a
 name is that?"
 He was kind of making fun of me actually. And I could never
 remember my name every time I go to see him with this monk.
 "What's this monk? Where did he ordain? He ordained with
 you."
 Because after I ordained, I left and went back to Canada
 for a year. It was an odd situation.
 I stayed with a monk in Canada. And then there was this big
 bruhaha about the International Department.
 And I kind of caused a ruckus demanding something or other.
 I was put in a position, because there was all politics,
 and there were two groups.
 The monastery was sort of split between those who wanted
 the international students to go with a group of lay people
 and those who wanted the international students to practice
 with the monks through a translator.
 Anyway, I was being put on one side, and there was another
 monk being put on the other side, and we were the two
 people.
 And I said, "This is splitting up the sangha." I said, "I
've refused to do it."
 And I went in front of Ajahn Tong, and the other monk
 yelled at me and said, "Who do you think you are?"
 Anyway, I felt Ajahn Tong scolded me as well and said, "
This isn't splitting up the sangha. This is trying to make
 things work."
 And he said, "You have wrong thought." He scolded me for it
.
 And I got in, you know, there were stories going around the
 monastery about my bad behavior.
 I was quite adamant and I guess you could say kind of
 arrogant about it, you know, kind of how Westerners tend to
 be when we think something's not right.
 We get up and say in front of everybody. And from then on,
 he never forgot my name.
 So, yeah, I know how that is. But after that, I wasn't, so
 it was kind of ruined for me after that because I know why
 he remembered my name.
 Not for the best reason. But again, it goes back to how
 monks can get a little bit crazy, you know, when your life
 is so simple.
 I mean, for most people in the world, they have very
 extreme outputs for pleasure, you know.
 So you have all sorts of complex activities if you want to
 go to a movie or if you want to go dancing or if you want
 to go to the opera or I don't know, what do people do?
 Want to go to a rave? Do they still do those? Or you can do
 drugs or that kind of thing.
 But for monks, there's very little of an outlet. And so you
 tend to get petty.
 And this is an example of this monk being very petty
 because his ego didn't have much to react to.
 So a simple slighting, not calling him by name, set him off
. And from then on, he had a grudge against Sariputta.
 And you hear about the Salat monks who had grudges. They
 tend to be recognized again and hearkens back to my own
 experience.
 I can verify this. What is it? I don't know if there is
 even an adage for something like the greasy wheel.
 The squeaky wheel gets the grease, but that's supposed to
 be a good thing.
 Sometimes it's better to go unknown because the reasons for
 becoming known.
 This monk got in the Dhammapada for his name and they didn
't even put his name in, insult to injury.
 They put him here and they refused to include his name or
 just refused to remember his name. I don't know. I just
 forgot it.
 Anyetra namagotofasena abhaccadobhiku, whatever that means,
 "abhaccado", unknown.
 So they refused to remember or no one knew his name or it
 was unknown by Sariputta, I guess.
 So Sariputta didn't call him out by name. Anyway, being
 petty and now he's remembered forever, immortalized in the
 Dhammapada for it.
 So then on top of that, to add insult to injury, not at all
 really, but to get even more, let's say, to be even worse
 about this whole situation,
 even more petty to an extreme degree.
 As Sariputta, as they were preparing, Sariputta walked past
 him and the corner of his robe touched the upset monk on
 the ear or something.
 And the monk, either getting angry about it or just using
 it as an excuse, started going around and telling people,
 or he went straight to the Buddha maybe and told the Buddha
.
 What do you think he said to the Buddha? He said, he went
 straight to the Buddha and said, "Reverencer, Venerable Sar
iputta, doubtless thinking to himself, 'I am your chief
 disciple,' struck me a blow that almost broke my eardrum or
 something, some part of my ear."
 And then without even asking forgiveness, he set out on his
 arms, set out for alms. So it was before the alms round.
 Could you imagine the gall of this guy going to the Buddha
 to inform upon Sariputta who had done nothing wrong?
 I mean, the amount of ignorance you need to do that is
 pretty astounding. And yet, I mean, it happens. People get
 blinded, but as we'll see, it doesn't last long. It can't
 last long.
 There's such purity. And as we'll see in regards to how Sar
iputta responds, there's such a profundity to these beings
 that they can't last.
 Eventually, he asks forgiveness. So all the monks hear
 about this. The Buddha calls someone, "Go and tell Sariput
ta to come."
 And all the other monks finding out about this, they gather
 together. And Mughalana and Ananda went around to all the
 monks and said, "Come, come and see. You want to see
 something neat? See how Sariputta deals with this guy."
 And so all the monks came out to listen. And the Buddha
 said, "So, Sariputta, this monk says that you have struck
 him and without even apologizing went away. You hit him."
 And then to find out what Sariputta says, we actually have
 to go to the Anguttara Nikaya. This is actually one story
 that is supported by the actual canonical text.
 Remember, the stories we're reading are recreations recre
ated from the verses. So they may have been exaggerated and
 more likely to have been exaggerated.
 Perhaps some might even say made up and just based on
 folklore. But regardless, they are less, they're not
 canonical, whether they're true or not.
 But here we have one that is based on the Anguttara Nikaya
 Book of Nines.
 The Buddhist, the Kastari Buddha says nine things. He doesn
't say, "I didn't hit the guy."
 Instead, he says, "One who has not established mindfulness
 directed to the body in regards to his own body."
 Let's see how he says that.
 "Yasanunabhante kaya gatasati gatasati anupartita asa." So
 one who is not mindful of the body. And this can be in one
 of various ways.
 But you could relate it back to our practice and say one
 who is not aware when they're walking that they know they
're walking,
 not aware of the movements of their body, where their hands
 are, not aware of where their mind is, and not objective
 about their actions.
 Such a person could very easily strike someone out of anger
.
 But the point is, when you're mindful, you actually are
 unable to get angry. When you're truly mindful, as an arah
ant is always,
 it's not possible for you to strike someone. And that's
 what Sari Buddha says.
 And he gives nine similes as to why it wouldn't be possible
, or as to what it's like to be someone who is mindful of
 the body,
 who is aware of the movements of the body, who would know
 when they were raising their fist and would be fully
 mindful,
 and thus unable to give rise to the rage required, the
 cruelty required, the hatred required to hurt someone.
 And so he says, "Just as water, just as you can wash impure
 things in water, and the water is not repulsed by it,
 the water doesn't get upset by that."
 He said, "My mind is like water, without enmity or ill will
 towards anyone, no matter what they do.
 Just as fire burns impure things, but is not upset by it,
 so too I dwell like fire, not upset when people, whatever
 they may say are due to me."
 Just as air blows upon impure things, and is not repulsed
 by that, so too I can meet with anything, come in contact
 with anyone and not be upset.
 "My mind, I dwell with a mind like air."
 I dwell with a mind like a duster, a dust rag, I guess. A
 dust rag is used to clean up all sorts of things, and yet
 the rag is not repelled.
 And then just as an outcast boy or girl, when I walk, I
 think of myself as an outcast boy or girl holding a bowl
 and wandering around for alms or for charity, begging.
 He says, "That's how I see myself, I put myself on that
 level, when I go for alms, when I walk, when I live."
 Because an outcast boy or girl in India, they're not
 allowed to work, they're not allowed to do so many things.
 They have to live in special areas or had to, this is in
 ancient times.
 Just as a bull with his horns cut, mild, well tamed and
 well trained. So a bull without its horns isn't going to
 attack anyone.
 And then, "I am like this bull without horns, I have no
 horns with which to attack."
 Meaning he can't even, he couldn't hit someone even if,
 well if he wanted to because he couldn't want to. It could
 never happen. It's not possible.
 He's not capable of it, it's not that he doesn't even want
 to, it's that he's not capable of it.
 And number eight, just as a woman or a man, a young woman
 or man, no?
 Think of a young woman, a young man who liked to dress up,
 liked to be clean, liked to put on perfumes, liked to put
 on makeup.
 Imagine if such a person had a carcass of a snake, a carc
ass of a dog or a carcass of a human being slung around
 their neck.
 Could you imagine, put a carcass of a dog around your neck,
 what they would think of that? And he said, he says, "Bante
, just in that way, I am repelled, humiliated and disgusted
 by this foul body."
 Interesting wording, no? Means he has no, it's not even,
 you have to take that kind of with a bit of license because
 I think it's unfair to say he's disgusted by it.
 Disgusted would be, you know, an upset, but he's certainly
 not upset.
 He just has no thought that there's anything good inside it
. I mean, the body is made up of all sorts of icky things.
 And number nine, seeing as he does that, just as a person
 might carry around a cracked bowl, or imagine someone with
 a garbage bag full of, from a restaurant, from behind a
 restaurant with fat and refuse and so on, with a leaky
 garbage bag.
 So this is a perforated bowl, a cracked bowl with fat in it
 that oozes and drips.
 And the body is like this with all sorts of holes in it
 that ooze and drip and sweat and smell.
 So he said, "One who has, in conclusion, one who has not
 established mindfulness directed to the body in regards to
 his own body might strike a fellow monk here and then set
 out on tour without apologizing."
 So it's called the lion's roar. It's one of the many lion's
 roars that we hear about. This one is by Sariputta.
 And so it's kind of impressive to read. Sariputta has no
 qualms about putting this monk in his place, so to speak.
 It's interesting that this came right after criticism.
 Yesterday we were talking about criticism.
 And reading through it today made me think, "Okay, this is
 the emulation required. This is what we have to work
 towards to be like Sariputta."
 "Who is so mindful that he has no ill will."
 So this is Sariputta's argument as to why that such a thing
 wouldn't be done.
 And so you think that would be enough. And it was enough.
 The monk immediately was shaken and got up on his hands and
 knees and prostrated himself at the foot of the Buddha and
 said, "Please forgive me.
 I was stupid. I was wrong. I transmitted a transgression
 that I so foolishly, stupidly and unskillfully slandered
 the venerable Sariputta on grounds that were untrue,
 baseless, false."
 And the Buddha acknowledged that. And then he turns to Sar
iputta and he says, "Sariputta, forgive him before his head
 explodes."
 This apparently was a thing. There is a thing. If you
 insult someone who is enlightened, it can get you into some
 serious head exploding.
 And so as if it wasn't enough, Sariputta puts the icing on
 the cake by saying, "I will pardon him."
 No. Because of what he's said. Or I think it's, "I will
 pardon him if he says that to me."
 Right? If he asked me forgiveness. Meaning he hasn't yet
 asked the Buddha, asked Sariputta forgiveness. He asked the
 Buddha forgiveness.
 So the point is, Sariputta has nothing to forgive, has no
 pardon to give until the monk says he's sorry.
 Not that Sariputta needs it, but he's saying, "Well, if he
 came to me, of course I'd apologize. I'd forgive him. I
 have no hard feelings."
 And then he says, "And then, and let him pardon me as well.
 Let him forgive me as well."
 Meaning for anything I might have done to him. This is
 actually quite standard. When you apologize to a monk, we
 do this often.
 We'll apologize to each other and then I apologize to this
 monk and the monk turns around and apologizes to me.
 We actually have a...
 At the end of the opening ceremony we do this. Opening and
 closing ceremonies for a meditator.
 We have this ceremony of asking forgiveness of the teacher
 and then the teacher turns around and asks forgiveness of
 us in this tradition.
 So this is an example for us to emulate.
 Then we switch back to the Dhammapada story. Then
 apparently the Buddha, the monks started commenting on this
 and were terribly impressed.
 In awe and reverence for Sariputta and his ability to just
 put this guy in his place, but to not be at all upset or
 disturbed or angry towards the other monk.
 And the teacher heard what they were saying and came and
 said to them, "Oh, it's not unusual. It's not hard to
 understand."
 He said, "It's impossible for Sariputta and people like him
 to have hatred and Sariputta's mind is like the great earth
. It's like an Indakila, like a foundation post, like a pool
 of still water."
 And then he taught this verse.
 So how this relates to us, again, it's about emulating this
. These are qualities to emulate. We should read the whole s
uttana, Angutra, Book of Nines, for when people accuse us of
 things that we didn't do or even of things that we didn't
 do.
 I guess it's harder when you didn't do it, when you didn't
 do something. And in general you'll notice that it's not
 that they don't refute it. They do.
 But they do so without anger. And they do it for the
 purpose of setting the record straight so people don't get
 the wrong idea. They don't do it so that it makes them look
 good, for sure.
 And so we emulate these qualities. We emulate the imagery
 that Sariputta talked about.
 But most importantly we emulate them in their practice,
 mindfulness of the body. It's a huge one.
 Well, mindfulness is the huge one. But here, talking about
 mindfulness of the body is because we're referring to the
 body. He hit someone.
 Sariputta points out, "You know, it's just a funny thing to
 think about because I'm so mindful with my body," he'd say.
 It's not really possible that such a thing, observing his
 own actions, which is the proper way to deal with an
 accusation, someone accuses you of something, you know,
 just say, "I would never do that."
 He'd reflect and say, "Could I do something? Did I do that?
 Could I do that?" And he looked and he said, "It's not
 possible because I'm mindful all the time. How could you do
 that if you're mindful?"
 And this is the key. Our theory is, and the power of
 mindfulness is, that you can't be angry and mindful at the
 same time.
 When you're clearly aware and objectively aware, the object
ivity overcomes any anger or any greed, and you're just "are
," you're just present.
 It makes you like the Earth, undisturbed, untroubled. The
 Earth is, I mean, it's the imagery of the big, the great
 Earth, Mahapattavi, the Earth as a planet, that, well, it
 does have earthquakes, but is pretty much undisturbed.
 It just sits there. It's totally grounded, I could say. And
 like a foundation post, they use this "indikila" to mean
 something. "Kila" means a post. "Inda" is of Indra.
 It's an expression. It means the foundation of a building,
 the first post that sort of keeps the building up, I guess.
 And so it's the most stable part of a house. It's someone
 who is firm and unmoved, you know.
 So if someone calls them a nasty name, they don't get upset
. If someone says nice things to them, they don't get
 pleased by it.
 Good things come. They're not elated. Bad things come. They
're not depressed. They live at peace.
 They live in the world without being of the world, without
 being moved by the world, without falling under the power
 of samsara.
 They become like a pool of water, still pool of water free
 from mud.
 You can think of mud as all of the mental defilements. This
 is what we're aiming for in Vipassana, is to have a clear
 mind.
 Such a person, the rounds of existence do not exist. Sams
ara does not exist. There is no samsara.
 Samsara nabhanti tadino. Samsara means wandering on. They
 have nowhere left to wander.
 They have no desires or no lessons left to learn. No goals
 yet to achieve.
 They've done it all, or they've risen above it all. They've
 gone beyond it all.
 Anyway, so that's the Dhammapada for tonight. Nice story.
 It's always nice to hear about Sariputta.
 He has other stories like this. There's another story about
 a monk actually coming and hitting him on the back.
 Sariputta was wandering on alms and this brahmin, I think,
 he'd heard that the Sariputta was like this, probably heard
 from this story.
 He thought, "Wow, I wonder if it's true. How could it be
 possible that this guy doesn't get upset no matter what
 happens?"
 He said, "Well, let's test him." So he took a stick and he
 wanders up, walks up behind Sariputta, and he saw an alms
 and smacks him one on the back.
 Sariputta just turns around and looks at him and then turns
 and goes on his way.
 And then the brahmin ends up apologizing to Sariputta. So
 imagine just walking up and smacking him.
 He said, "It's really true. He doesn't get angry. He doesn
't get upset no matter what happens."
 So if you're really interested in Sariputta, you should
 read. There's an English, on the internet, there's an
 article.
 You can also, I think it's in Great Disciples of the Buddha
, but it's also under the life of Sariputta.
 I think it was originally published in the Buddhist
 Publication Society, but I think it's on the internet.
 Very much worth reading because very much worth emulating.
 Sariputta and Mughalana and all the great disciples.
 If you want to know what a true Buddhist is like, what are
 Buddhists like when they go all the way?
 It's pretty impressive. So that in mind, this is what we
 work towards, to be impressive.
 Not exactly, but to be free, to be pure, to be untroubled.
 And that's impressive.
 So thank you all for tuning in. Wishing you all good
 practice and have a good night.
