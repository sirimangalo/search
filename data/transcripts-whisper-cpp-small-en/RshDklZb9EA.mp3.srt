1
00:00:00,000 --> 00:00:11,560
 Good evening everyone.

2
00:00:11,560 --> 00:00:22,080
 We're broadcasting live December 29th, 2015.

3
00:00:22,080 --> 00:00:33,760
 We have a little over two days left in the year.

4
00:00:33,760 --> 00:00:42,160
 But don't worry, there's another year to come.

5
00:00:42,160 --> 00:00:56,400
 I had an argument with my youngest brother about rebirth.

6
00:00:56,400 --> 00:01:16,920
 He said, "Well, I can't remember."

7
00:01:16,920 --> 00:01:26,920
 He said, "We'll see who's right when we die.

8
00:01:26,920 --> 00:01:28,920
 We'll see who's right."

9
00:01:28,920 --> 00:01:35,290
 And it's going to be a shame for him if there are

10
00:01:35,290 --> 00:01:39,640
 consequences to his actions.

11
00:01:39,640 --> 00:01:44,480
 He can't escape them by dying.

12
00:01:44,480 --> 00:01:50,320
 And he said, "Well, it'll be, yeah, and if I'm right, it'll

13
00:01:50,320 --> 00:01:52,200
 be, you'll have to deal

14
00:01:52,200 --> 00:01:54,000
 with the consequences if I'm right."

15
00:01:54,000 --> 00:01:58,960
 And I said, "Well, no, actually, if you're right, there are

16
00:01:58,960 --> 00:02:00,640
 no consequences.

17
00:02:00,640 --> 00:02:03,720
 If you're right, it'd be great.

18
00:02:03,720 --> 00:02:05,880
 I mean, so we can do whatever we want."

19
00:02:05,880 --> 00:02:14,350
 And it's quite a dangerous belief that the belief in death

20
00:02:14,350 --> 00:02:18,360
 is a dangerous belief because

21
00:02:18,360 --> 00:02:21,920
 of the consequences of it.

22
00:02:21,920 --> 00:02:26,030
 Even in this life, the consequences are people are less

23
00:02:26,030 --> 00:02:28,560
 likely to do good deeds, more likely

24
00:02:28,560 --> 00:02:37,440
 to do evil deeds.

25
00:02:37,440 --> 00:02:42,820
 Even if it was true, it would be better for us to believe

26
00:02:42,820 --> 00:02:46,280
 that there were consequences.

27
00:02:46,280 --> 00:02:50,880
 It would be better for the world.

28
00:02:50,880 --> 00:02:53,160
 That's a funny situation.

29
00:02:53,160 --> 00:02:55,650
 It's actually, it would actually, if it were true that when

30
00:02:55,650 --> 00:02:56,960
 we died, there was nothing,

31
00:02:56,960 --> 00:03:02,070
 it would still be better for all of us and for the world

32
00:03:02,070 --> 00:03:05,320
 and for society for us to believe

33
00:03:05,320 --> 00:03:14,080
 that there were consequences.

34
00:03:14,080 --> 00:03:15,560
 And so we are argued.

35
00:03:15,560 --> 00:03:20,660
 And then right before I left this morning, I said, "So, we

36
00:03:20,660 --> 00:03:23,120
'll have a safe travels."

37
00:03:23,120 --> 00:03:24,120
 And I said, "Thank you."

38
00:03:24,120 --> 00:03:27,680
 And he said, "So, I'll see you."

39
00:03:27,680 --> 00:03:33,080
 And I said, "If not in this life, then in the next."

40
00:03:33,080 --> 00:03:44,560
 And we laughed.

41
00:03:44,560 --> 00:03:49,400
 So I had one announcement today that seems a lot of people

42
00:03:49,400 --> 00:03:52,480
 didn't know about the appointment

43
00:03:52,480 --> 00:03:54,000
 schedule that we have.

44
00:03:54,000 --> 00:03:58,770
 So if you go to meditation.siri-mongolow.org, there's a

45
00:03:58,770 --> 00:04:01,160
 meet page and it's got slots where

46
00:04:01,160 --> 00:04:04,880
 you can sign up for a meditation course.

47
00:04:04,880 --> 00:04:07,120
 And we use Google Hangouts.

48
00:04:07,120 --> 00:04:12,330
 So you have to make sure you've got Google Hangouts

49
00:04:12,330 --> 00:04:15,120
 installed and working.

50
00:04:15,120 --> 00:04:21,720
 And then if you show up at the right time, you can meet

51
00:04:21,720 --> 00:04:23,120
 with me.

52
00:04:23,120 --> 00:04:30,190
 But you must be practicing at least an hour of meditation

53
00:04:30,190 --> 00:04:31,560
 per day.

54
00:04:31,560 --> 00:04:38,080
 And we try to get you up to two hours.

55
00:04:38,080 --> 00:04:41,720
 And you have to keep Buddhist precepts and stuff.

56
00:04:41,720 --> 00:04:43,960
 But it's for people who are serious about the meditation

57
00:04:43,960 --> 00:04:45,160
 practice and want to go the

58
00:04:45,160 --> 00:04:48,920
 next, take the next step.

59
00:04:48,920 --> 00:04:55,800
 Right now we have nine slots available.

60
00:04:55,800 --> 00:05:02,760
 So, still some...

61
00:05:02,760 --> 00:05:11,080
 I just got back from Florida today.

62
00:05:11,080 --> 00:05:17,520
 So I'm not planning on sticking around too long this

63
00:05:17,520 --> 00:05:19,040
 evening.

64
00:05:19,040 --> 00:05:21,040
 A short night of it.

65
00:05:21,040 --> 00:05:22,800
 Do you have any questions?

66
00:05:22,800 --> 00:05:23,800
 We have questions.

67
00:05:23,800 --> 00:05:27,920
 We have lots of questions, Bante.

68
00:05:27,920 --> 00:05:29,800
 Skip to the good ones.

69
00:05:29,800 --> 00:05:30,800
 Okay.

70
00:05:30,800 --> 00:05:33,470
 "Regarding meetings, is it required to have a camera and a

71
00:05:33,470 --> 00:05:35,080
 microphone to make an individual

72
00:05:35,080 --> 00:05:36,080
 meeting?

73
00:05:36,080 --> 00:05:39,280
 I have social anxiety, so it's very difficult for me.

74
00:05:39,280 --> 00:05:41,480
 And my English is bad too.

75
00:05:41,480 --> 00:05:42,480
 Thanks."

76
00:05:42,480 --> 00:05:44,720
 Yeah, we could do with just...

77
00:05:44,720 --> 00:05:46,440
 You need a microphone, absolutely.

78
00:05:46,440 --> 00:05:52,040
 But I'd like to have a camera.

79
00:05:52,040 --> 00:05:56,470
 I think it's important because normally you would come into

80
00:05:56,470 --> 00:05:58,240
 the room and meet me.

81
00:05:58,240 --> 00:06:01,360
 And we're trying to make it as close to that as possible.

82
00:06:01,360 --> 00:06:07,760
 So I really would like for you to have a camera.

83
00:06:07,760 --> 00:06:13,750
 "Every time I do sitting meditation, I seem to keep

84
00:06:13,750 --> 00:06:16,280
 dropping off to sleep.

85
00:06:16,280 --> 00:06:18,240
 This can be any time of day.

86
00:06:18,240 --> 00:06:21,000
 Apart from walking meditation, what can I do to help with

87
00:06:21,000 --> 00:06:22,440
 my sitting meditation?"

88
00:06:22,440 --> 00:06:26,520
 Well, walking meditation is good.

89
00:06:26,520 --> 00:06:29,960
 What's wrong with that?

90
00:06:29,960 --> 00:06:34,760
 Be mindful, tired, tired helps.

91
00:06:34,760 --> 00:06:39,000
 But it probably has a lot to do with your lifestyle.

92
00:06:39,000 --> 00:06:45,940
 You have to put up with falling asleep because of the state

93
00:06:45,940 --> 00:06:48,200
 your mind is in.

94
00:06:48,200 --> 00:06:54,670
 "During sitting meditation, I find it hard to note thoughts

95
00:06:54,670 --> 00:06:57,760
 that arise and cease quickly

96
00:06:57,760 --> 00:06:59,800
 while I'm noting primary objectives.

97
00:06:59,800 --> 00:07:03,610
 However, I am aware that this happens and it is interrupt

98
00:07:03,610 --> 00:07:05,280
ing my concentration.

99
00:07:05,280 --> 00:07:08,200
 How it happens is that while I'm waiting for the rising, a

100
00:07:08,200 --> 00:07:09,960
 thought will manifest and cease

101
00:07:09,960 --> 00:07:12,400
 and I will go on noting the rising.

102
00:07:12,400 --> 00:07:15,620
 My question is, should I in this case be noting thinking,

103
00:07:15,620 --> 00:07:17,680
 thinking, thinking after the fact

104
00:07:17,680 --> 00:07:19,880
 or should I note distracted?

105
00:07:19,880 --> 00:07:23,360
 If so, how many times should I note before returning to my

106
00:07:23,360 --> 00:07:24,440
 objectives?"

107
00:07:24,440 --> 00:07:31,970
 You can, just once, or you can acknowledge knowing a couple

108
00:07:31,970 --> 00:07:33,560
 of times.

109
00:07:33,560 --> 00:07:39,670
 It's not magic or something, just do it as it seems

110
00:07:39,670 --> 00:07:40,080
 appropriate.

111
00:07:40,080 --> 00:07:43,440
 Knowing would be knowing that you were thinking.

112
00:07:43,440 --> 00:07:48,820
 "Is there a certain time of day in which meditation is best

113
00:07:48,820 --> 00:07:51,360
 to be practiced in, in the morning

114
00:07:51,360 --> 00:07:52,920
 for instance?"

115
00:07:52,920 --> 00:07:54,920
 No.

116
00:07:54,920 --> 00:08:02,110
 "How would you convince a non-Buddhist that reincarnation

117
00:08:02,110 --> 00:08:03,480
 is real?

118
00:08:03,480 --> 00:08:07,920
 Is there a way one can know who one was in a past life?

119
00:08:07,920 --> 00:08:09,960
 And thank you so much for taking the time to answer."

120
00:08:09,960 --> 00:08:13,360
 Yeah, I'm going to skip that one.

121
00:08:13,360 --> 00:08:15,840
 Okay, sounds like you've already had that.

122
00:08:15,840 --> 00:08:17,960
 I just wanted to say about consequences.

123
00:08:17,960 --> 00:08:20,470
 You don't even have to wait until the next life for the

124
00:08:20,470 --> 00:08:21,480
 consequences.

125
00:08:21,480 --> 00:08:24,510
 I mean, you feel them right now when you try to do anything

126
00:08:24,510 --> 00:08:24,760
.

127
00:08:24,760 --> 00:08:28,240
 Yeah, I was arguing that with him and he said, "I get that

128
00:08:28,240 --> 00:08:28,640
."

129
00:08:28,640 --> 00:08:31,950
 My younger brother said he knows that, but he said, "But,"

130
00:08:31,950 --> 00:08:33,160
 and then I said, "But, do

131
00:08:33,160 --> 00:08:37,760
 you think that there are no consequences?"

132
00:08:37,760 --> 00:08:52,110
 In the end, when you die, you get off scot-free and you say

133
00:08:52,110 --> 00:08:55,440
, "Yeah."

134
00:08:55,440 --> 00:08:58,880
 When I'm sitting, I note sitting, sitting, but how far do I

135
00:08:58,880 --> 00:08:59,760
 need to go?

136
00:08:59,760 --> 00:09:03,510
 Do I need to be mindful of the pressure below my butt or

137
00:09:03,510 --> 00:09:05,720
 just know that I'm sitting?

138
00:09:05,720 --> 00:09:07,600
 Is sitting a concept?

139
00:09:07,600 --> 00:09:11,100
 When my abdomen is rising, I note rising, rising, but do I

140
00:09:11,100 --> 00:09:12,600
 need to follow the rising

141
00:09:12,600 --> 00:09:14,960
 of the abdomen at every position?

142
00:09:14,960 --> 00:09:15,960
 Thanks.

143
00:09:15,960 --> 00:09:19,880
 Yeah, you should follow the rising from beginning to end.

144
00:09:19,880 --> 00:09:21,240
 You're not just saying it.

145
00:09:21,240 --> 00:09:24,400
 It's not just words in your head.

146
00:09:24,400 --> 00:09:29,600
 Sitting as well as sitting is an expression of the

147
00:09:29,600 --> 00:09:31,320
 sensations.

148
00:09:31,320 --> 00:09:33,720
 So yeah, you should be aware, but you don't have to focus

149
00:09:33,720 --> 00:09:34,160
 on it.

150
00:09:34,160 --> 00:09:37,670
 The only reason you know that you're sitting is because of

151
00:09:37,670 --> 00:09:39,440
 the sensations in your back

152
00:09:39,440 --> 00:09:52,000
 and in your bottom.

153
00:09:52,000 --> 00:09:54,380
 Is there a difference between mindfulness achieved during

154
00:09:54,380 --> 00:09:55,580
 meditation and mindfulness

155
00:09:55,580 --> 00:09:57,820
 during everyday activities?

156
00:09:57,820 --> 00:10:01,140
 Is mindfulness achieved in formal meditation practice more

157
00:10:01,140 --> 00:10:02,020
 beneficial?

158
00:10:02,020 --> 00:10:02,660
 Thank you, Bante.

159
00:10:02,660 --> 00:10:03,660
 Mindfulness is mindfulness.

160
00:10:03,660 --> 00:10:04,660
 Hello, Bante.

161
00:10:04,660 --> 00:10:13,870
 I'd like to start off by saying thank you for all your

162
00:10:13,870 --> 00:10:18,340
 teachings and dedication.

163
00:10:18,340 --> 00:10:21,460
 They truly have helped me put things in better perspective.

164
00:10:21,460 --> 00:10:24,340
 I'll be taking a couple of college classes in a city called

165
00:10:24,340 --> 00:10:26,100
 Sioux Falls, South Dakota,

166
00:10:26,100 --> 00:10:28,340
 starting February 2016.

167
00:10:28,340 --> 00:10:31,230
 And I'm thinking of forming a small meditation group on

168
00:10:31,230 --> 00:10:33,400
 campus with interested individuals.

169
00:10:33,400 --> 00:10:36,110
 If you'll be willing to meet with us for about an hour or

170
00:10:36,110 --> 00:10:38,000
 less once a week via Skype or another

171
00:10:38,000 --> 00:10:44,400
 form of video communication, I think that was a question.

172
00:10:44,400 --> 00:10:46,460
 I'm not certain of the number of people who will be

173
00:10:46,460 --> 00:10:47,960
 interested in such a group, but if

174
00:10:47,960 --> 00:10:54,300
 any, it would be wonderful to have you guide us along this

175
00:10:54,300 --> 00:10:58,280
 path to freedom from suffering.

176
00:10:58,280 --> 00:11:02,950
 I think the question was, would you be potentially able to

177
00:11:02,950 --> 00:11:04,460
 do such a thing?

178
00:11:04,460 --> 00:11:09,460
 I don't know.

179
00:11:09,460 --> 00:11:10,460
 Probably not.

180
00:11:10,460 --> 00:11:16,500
 I'm doing a lot already.

181
00:11:16,500 --> 00:11:20,300
 You just go, you know, go according to the booklet and

182
00:11:20,300 --> 00:11:22,420
 people want to learn more.

183
00:11:22,420 --> 00:11:26,220
 They can contact me.

184
00:11:26,220 --> 00:11:30,580
 They can do a course.

185
00:11:30,580 --> 00:11:32,220
 They can join our online group here as well.

186
00:11:32,220 --> 00:11:33,220
 It's great that you're doing it.

187
00:11:33,220 --> 00:11:38,000
 I think I want to encourage you in it, but I don't think I

188
00:11:38,000 --> 00:11:38,820
'd be.

189
00:11:38,820 --> 00:11:41,820
 I'm already doing too much.

190
00:11:41,820 --> 00:11:45,420
 Hi, Bante.

191
00:11:45,420 --> 00:11:47,220
 I have a job interview in one week.

192
00:11:47,220 --> 00:11:49,740
 I stutter a lot when I need to talk and have difficulty to

193
00:11:49,740 --> 00:11:50,380
 breathe.

194
00:11:50,380 --> 00:11:53,300
 Do you have any advice besides noting?

195
00:11:53,300 --> 00:11:56,250
 And is it a good thing to imagine the interview beforehand

196
00:11:56,250 --> 00:11:57,740
 and observe my reaction?

197
00:11:57,740 --> 00:11:58,740
 Thanks.

198
00:11:58,740 --> 00:12:04,450
 No, I mean, I teach meditation, so you know what I've got

199
00:12:04,450 --> 00:12:05,740
 to offer.

200
00:12:05,740 --> 00:12:07,540
 If it helps, take it.

201
00:12:07,540 --> 00:12:20,300
 If it doesn't help, leave it.

202
00:12:20,300 --> 00:12:21,420
 I guess this is a question.

203
00:12:21,420 --> 00:12:23,460
 Do you know that you're just the best?

204
00:12:23,460 --> 00:12:31,540
 I think that was a compliment, but I had a question mark.

205
00:12:31,540 --> 00:12:34,540
 We're all worthless.

206
00:12:34,540 --> 00:12:37,020
 Is it better to meditate in a group rather than alone?

207
00:12:37,020 --> 00:12:38,780
 If so, why?

208
00:12:38,780 --> 00:12:41,380
 Thank you, Bante.

209
00:12:41,380 --> 00:12:42,380
 You're always alone.

210
00:12:42,380 --> 00:12:46,740
 You're never in a group.

211
00:12:46,740 --> 00:12:54,260
 But much of meditation is an artifice, is artificial.

212
00:12:54,260 --> 00:12:59,170
 So there's lots of artificial supports for meditation, and

213
00:12:59,170 --> 00:13:01,660
 one of them is having a teacher

214
00:13:01,660 --> 00:13:03,620
 being in a group.

215
00:13:03,620 --> 00:13:06,740
 These are artificial supports.

216
00:13:06,740 --> 00:13:11,200
 So yeah, I think they can support your meditation, provide

217
00:13:11,200 --> 00:13:13,300
 encouragement and so on.

218
00:13:13,300 --> 00:13:21,660
 Lots of things that can support your meditation.

219
00:13:21,660 --> 00:13:24,220
 I'm sitting every day and making progress.

220
00:13:24,220 --> 00:13:27,060
 Is there any benefit in doing your one-on-one course, or is

221
00:13:27,060 --> 00:13:29,340
 it just for those who need advice?

222
00:13:29,340 --> 00:13:30,820
 Thank you.

223
00:13:30,820 --> 00:13:34,930
 Well, if you're just practicing according to my booklet,

224
00:13:34,930 --> 00:13:37,100
 that's only the first step.

225
00:13:37,100 --> 00:13:41,600
 So yeah, we take you through the steps and we guide you

226
00:13:41,600 --> 00:13:43,580
 through the course.

227
00:13:43,580 --> 00:13:49,770
 I would say there's a lot of benefit in taking an online

228
00:13:49,770 --> 00:13:51,100
 course.

229
00:13:51,100 --> 00:14:02,740
 It's just the very beginning.

230
00:14:02,740 --> 00:14:04,780
 And with that, you've made it through all the questions.

231
00:14:04,780 --> 00:14:05,780
 Good.

232
00:14:05,780 --> 00:14:21,660
 So we have tomorrow, we might do Dhammapada, probably.

233
00:14:21,660 --> 00:14:28,540
 Should be okay.

234
00:14:28,540 --> 00:14:31,780
 Then on Thursday, we have the last day of the year.

235
00:14:31,780 --> 00:14:36,780
 I'll be here.

236
00:14:36,780 --> 00:14:41,940
 We'll see who shows up.

237
00:14:41,940 --> 00:14:43,940
 Thanks for your muffins, Bante.

238
00:14:43,940 --> 00:14:45,220
 What?

239
00:14:45,220 --> 00:14:46,220
 Thanks for your muffins, Bante.

240
00:14:46,220 --> 00:14:48,420
 Are you giving out muffins?

241
00:14:48,420 --> 00:14:50,420
 No.

242
00:14:50,420 --> 00:14:52,420
 Okay.

243
00:14:52,420 --> 00:14:55,820
 Muffins.

244
00:14:55,820 --> 00:14:57,140
 Someone thanked you for your muffins.

245
00:14:58,140 --> 00:14:59,140
 I don't get it.

246
00:14:59,140 --> 00:15:00,140
 I don't either.

247
00:15:00,140 --> 00:15:01,140
 I think it's a joke.

248
00:15:01,140 --> 00:15:02,140
 Maybe.

249
00:15:02,140 --> 00:15:03,140
 Anyway, good night, everyone.

250
00:15:03,140 --> 00:15:04,140
 Good night, Bante.

251
00:15:04,140 --> 00:15:05,140
 See you all tomorrow.

252
00:15:05,140 --> 00:15:06,140
 Thank you.

253
00:15:06,140 --> 00:15:07,140
 Thank you.

254
00:15:07,140 --> 00:15:08,140
 You're welcome.

255
00:15:08,140 --> 00:15:09,140
 Thank you.

256
00:15:09,140 --> 00:15:10,140
 Thank you.

257
00:15:10,140 --> 00:15:11,140
 Thank you.

258
00:15:11,140 --> 00:15:12,140
 Thank you.

259
00:15:12,140 --> 00:15:13,140
 Thank you.

260
00:15:13,140 --> 00:15:14,140
 Thank you.

261
00:15:14,140 --> 00:15:15,140
 Thank you.

262
00:15:15,140 --> 00:15:16,140
 Thank you.

263
00:15:16,140 --> 00:15:17,140
 Thank you.

264
00:15:17,140 --> 00:15:18,140
 Thank you.

265
00:15:18,140 --> 00:15:19,140
 Thank you.

266
00:15:19,140 --> 00:15:20,140
 Thank you.

267
00:15:20,140 --> 00:15:21,140
 Thank you.

268
00:15:21,140 --> 00:15:22,140
 Thank you.

269
00:15:22,140 --> 00:15:23,140
 Thank you.

270
00:15:23,140 --> 00:15:24,140
 Thank you.

271
00:15:24,140 --> 00:15:25,140
 Thank you.

272
00:15:25,140 --> 00:15:26,140
 Thank you.

273
00:15:26,140 --> 00:15:27,140
 Thank you.

274
00:15:27,140 --> 00:15:28,140
 Thank you.

275
00:15:28,140 --> 00:15:29,140
 Thank you.

276
00:15:29,140 --> 00:15:30,140
 Thank you.

