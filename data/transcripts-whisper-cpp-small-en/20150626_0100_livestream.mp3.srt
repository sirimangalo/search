1
00:00:00,000 --> 00:00:07,000
 Okay, good evening.

2
00:00:07,000 --> 00:00:19,000
 A couple of days ago we talked about what the meditation is

3
00:00:19,000 --> 00:00:22,000
 and isn't for.

4
00:00:22,000 --> 00:00:30,000
 And highlighted the negative aspects of meditation which

5
00:00:30,000 --> 00:00:33,000
 turn out to be actually quite useful,

6
00:00:33,000 --> 00:00:42,000
 beneficial, conducive to actual happiness by dealing with

7
00:00:42,000 --> 00:00:51,000
 and by investigating

8
00:00:51,000 --> 00:01:01,000
 the negative aspects of reality we actually benefit.

9
00:01:01,000 --> 00:01:05,350
 So on the flip side of the coin there's a bunch of other

10
00:01:05,350 --> 00:01:11,000
 things that the meditation is not for,

11
00:01:11,000 --> 00:01:24,000
 that are not beneficial.

12
00:01:24,000 --> 00:01:30,090
 And unfortunately this turns out to be all the good things,

13
00:01:30,090 --> 00:01:39,000
 all the positive sides of meditation.

14
00:01:39,000 --> 00:01:46,470
 So there's many bad things of course that the meditation is

15
00:01:46,470 --> 00:01:47,000
 not,

16
00:01:47,000 --> 00:01:51,000
 that it's not a part of the meditation.

17
00:01:51,000 --> 00:02:02,320
 Anger, laziness, distraction, doubt, fear, all these things

18
00:02:02,320 --> 00:02:04,000
, worry.

19
00:02:04,000 --> 00:02:07,120
 So it's not that everything negative is good and everything

20
00:02:07,120 --> 00:02:08,000
 positive is bad.

21
00:02:08,000 --> 00:02:12,260
 But we have a bit of a problem because obviously we're

22
00:02:12,260 --> 00:02:16,000
 inclined to avoid negative things

23
00:02:16,000 --> 00:02:25,000
 and seek out positive experiences.

24
00:02:25,000 --> 00:02:29,870
 So the teaching, where the teaching comes in we have to

25
00:02:29,870 --> 00:02:31,000
 approach it.

26
00:02:31,000 --> 00:02:39,210
 We have to focus on those negative aspects that are

27
00:02:39,210 --> 00:02:42,000
 actually useful.

28
00:02:42,000 --> 00:02:45,870
 Seeing impermanent, suffering, non-self, seeing bad things

29
00:02:45,870 --> 00:02:50,000
 turns out to be useful to see them.

30
00:02:50,000 --> 00:02:53,000
 Precisely because they're a problem it helps us to realize

31
00:02:53,000 --> 00:02:57,940
 that things that we cling to are not actually worth

32
00:02:57,940 --> 00:03:00,000
 clinging to.

33
00:03:00,000 --> 00:03:03,530
 And on the other side we also have to focus on the positive

34
00:03:03,530 --> 00:03:07,000
 things in teaching.

35
00:03:07,000 --> 00:03:09,460
 We have to outline those positive things that turn out to

36
00:03:09,460 --> 00:03:10,000
 be useless,

37
00:03:10,000 --> 00:03:22,000
 turn out to be misleading, turn out to be misguiding.

38
00:03:22,000 --> 00:03:27,000
 Things that lead us down a dead-end path.

39
00:03:27,000 --> 00:03:34,780
 There's lots of them, they're outlined in the Risuddhi Maga

40
00:03:34,780 --> 00:03:38,000
, they're called vipassanupakilesa.

41
00:03:38,000 --> 00:03:42,780
 Vipassanupakileso, upakileso, upam, kileso means a defile

42
00:03:42,780 --> 00:03:45,000
ment, but upakileso makes it softer,

43
00:03:45,000 --> 00:03:50,000
 so it's not really a bad thing.

44
00:03:50,000 --> 00:03:56,000
 They're just things that detract from insight.

45
00:03:56,000 --> 00:04:02,110
 So when a meditator is trying to see clearly these things

46
00:04:02,110 --> 00:04:04,000
 get in the way.

47
00:04:04,000 --> 00:04:16,000
 They keep our focus from being strong, keep our self,

48
00:04:16,000 --> 00:04:19,340
 our confidence in the practice, our interest in the

49
00:04:19,340 --> 00:04:20,000
 practice,

50
00:04:20,000 --> 00:04:25,000
 they detract from it because our interest is elsewhere.

51
00:04:25,000 --> 00:04:27,000
 So there's ten of them.

52
00:04:27,000 --> 00:04:34,550
 The first one is obasa, means lights, colors, pictures,

53
00:04:34,550 --> 00:04:36,000
 visions.

54
00:04:36,000 --> 00:04:40,000
 This is probably the most common among meditators.

55
00:04:40,000 --> 00:04:43,000
 We have all sorts of different kinds of visions.

56
00:04:43,000 --> 00:04:47,670
 Some people feel like the room is brighter even with their

57
00:04:47,670 --> 00:04:49,000
 eyes closed.

58
00:04:49,000 --> 00:04:54,000
 Some people see bright lights or pictures, colors,

59
00:04:54,000 --> 00:05:03,990
 and people see nature or divine images, these kinds of

60
00:05:03,990 --> 00:05:05,000
 things.

61
00:05:05,000 --> 00:05:09,760
 And they're quite pleasing, quite attractive, enticing as

62
00:05:09,760 --> 00:05:11,000
 well.

63
00:05:11,000 --> 00:05:15,000
 They seem to have some kind of meaning behind them.

64
00:05:15,000 --> 00:05:20,000
 And so we often investigate thinking it means something,

65
00:05:20,000 --> 00:05:24,000
 thinking maybe it's going to lead us somewhere.

66
00:05:24,000 --> 00:05:27,000
 And so we very easily get lost in these.

67
00:05:27,000 --> 00:05:30,060
 And as a result we don't get anywhere because they don't

68
00:05:30,060 --> 00:05:31,000
 lead us anywhere.

69
00:05:31,000 --> 00:05:37,000
 They don't have any benefit.

70
00:05:37,000 --> 00:05:39,000
 So it's not a problem if we see lights.

71
00:05:39,000 --> 00:05:42,000
 None of these things are a problem.

72
00:05:42,000 --> 00:05:45,000
 Most of them are not a problem, but they become a problem

73
00:05:45,000 --> 00:05:48,000
 when we take them to be the path.

74
00:05:48,000 --> 00:05:52,000
 We think that, oh, maybe this is the way, focusing on this,

75
00:05:52,000 --> 00:05:53,000
 encouraging this.

76
00:05:53,000 --> 00:05:57,000
 When this arises I will progress, I will benefit.

77
00:05:57,000 --> 00:06:04,000
 That's the problem. That's wrong.

78
00:06:04,000 --> 00:06:06,000
 The second one is called bhiti.

79
00:06:06,000 --> 00:06:10,000
 Bhiti is a good thing, means rapture or excitement.

80
00:06:10,000 --> 00:06:16,000
 It has to be energetic in order to progress on the path.

81
00:06:16,000 --> 00:06:22,470
 But there's this overflow of energy or excitement, like a

82
00:06:22,470 --> 00:06:25,000
 static charge.

83
00:06:25,000 --> 00:06:27,000
 And bhiti is this overflowing.

84
00:06:27,000 --> 00:06:34,000
 So by bhiti here we mean things like laughter, crying.

85
00:06:34,000 --> 00:06:41,530
 It's uncontrollable, urge, swaying when the meditator sways

86
00:06:41,530 --> 00:06:43,000
 back and forth.

87
00:06:43,000 --> 00:06:50,000
 When the meditator has energy coursing through them,

88
00:06:50,000 --> 00:06:57,000
 feeling of vibrations or electricity or goosebumps

89
00:06:57,000 --> 00:06:59,000
 constantly,

90
00:06:59,000 --> 00:07:10,000
 again and again and again, feelings of lightness,

91
00:07:10,000 --> 00:07:14,000
 of feeling light as though you were floating.

92
00:07:14,000 --> 00:07:23,000
 All of these sort of physical manifestations of rapture,

93
00:07:23,000 --> 00:07:27,000
 these are all a distraction from the practice.

94
00:07:27,000 --> 00:07:29,720
 They sometimes lead us to think that there's something

95
00:07:29,720 --> 00:07:30,000
 special coming

96
00:07:30,000 --> 00:07:32,000
 or that they're going to lead us somewhere,

97
00:07:32,000 --> 00:07:37,000
 but they also sometimes just serve to provide pleasure

98
00:07:37,000 --> 00:07:41,720
 and so keep us from progressing because we get stuck on

99
00:07:41,720 --> 00:07:42,000
 this sort of

100
00:07:42,000 --> 00:07:45,000
 pleasant feeling of rocking back and forth

101
00:07:45,000 --> 00:07:50,840
 or the pleasant feeling of the energy in our bodies and

102
00:07:50,840 --> 00:07:56,000
 this kind of thing.

103
00:07:56,000 --> 00:07:59,000
 Again, if we're not mindful, if we don't note these things

104
00:07:59,000 --> 00:08:00,000
 as they arise,

105
00:08:00,000 --> 00:08:09,000
 this one also will keep us from progressing.

106
00:08:09,000 --> 00:08:11,000
 The third one is called nyana.

107
00:08:11,000 --> 00:08:13,000
 Nyana means knowledge.

108
00:08:13,000 --> 00:08:16,000
 So sometimes meditators gain great knowledge.

109
00:08:16,000 --> 00:08:21,000
 Sometimes it's knowledge about mundane things,

110
00:08:21,000 --> 00:08:24,000
 how to run our lives, how to fix our problems.

111
00:08:24,000 --> 00:08:27,000
 We have to think about the problems we have at home

112
00:08:27,000 --> 00:08:31,000
 and we suddenly have the solution, how to fix it,

113
00:08:31,000 --> 00:08:34,000
 how to fix everything.

114
00:08:34,000 --> 00:08:48,000
 As a result, we don't focus on the actual practice.

115
00:08:48,000 --> 00:08:53,000
 Sometimes it's extraordinary knowledge.

116
00:08:53,000 --> 00:08:57,000
 Sometimes we see things far away or we hear sounds far away

117
00:08:57,000 --> 00:09:01,000
 or we can read people's minds.

118
00:09:01,000 --> 00:09:03,000
 We remember past lives.

119
00:09:03,000 --> 00:09:06,000
 Some people have premonitions of the future.

120
00:09:06,000 --> 00:09:10,900
 All of these ones, these ones certainly serve to distract

121
00:09:10,900 --> 00:09:12,000
 the meditator.

122
00:09:12,000 --> 00:09:17,000
 You can get lost in magical knowledges like this,

123
00:09:17,000 --> 00:09:20,000
 thinking that it has some meaning or some benefit

124
00:09:20,000 --> 00:09:23,750
 so you get caught up, people can actually turn it into a

125
00:09:23,750 --> 00:09:24,000
 business

126
00:09:24,000 --> 00:09:30,000
 where they spend their lives providing service for others,

127
00:09:30,000 --> 00:09:38,000
 reading people's fortunes, all these kinds of things.

128
00:09:38,000 --> 00:09:40,590
 All of this seems quite interesting and seems somehow

129
00:09:40,590 --> 00:09:41,000
 beneficial

130
00:09:41,000 --> 00:09:51,000
 and in the end doesn't have any real true lasting benefit.

131
00:09:51,000 --> 00:09:55,000
 Again, we have to know to acknowledge it's arising.

132
00:09:55,000 --> 00:09:59,000
 Number four is passanti, it means tranquility.

133
00:09:59,000 --> 00:10:03,000
 The meditator will feel very calm at times

134
00:10:03,000 --> 00:10:07,000
 and this calm feels like somehow enlightenment,

135
00:10:07,000 --> 00:10:10,000
 like the meditator has become enlightened

136
00:10:10,000 --> 00:10:15,000
 so they become excited and they cling to it

137
00:10:15,000 --> 00:10:23,000
 or they do what they can to try and encourage it to stay

138
00:10:23,000 --> 00:10:27,000
 or trigger it to come back

139
00:10:27,000 --> 00:10:32,760
 and become obsessed with this state of calm, attaining a

140
00:10:32,760 --> 00:10:36,000
 state of calm.

141
00:10:36,000 --> 00:10:44,000
 Number five is, we have confidence.

142
00:10:44,000 --> 00:10:47,000
 So the meditator at times will feel great confidence

143
00:10:47,000 --> 00:10:50,360
 either in themselves, feeling like they're somehow

144
00:10:50,360 --> 00:10:51,000
 enlightened,

145
00:10:51,000 --> 00:10:54,000
 feeling like they're a perfect meditator

146
00:10:54,000 --> 00:10:57,000
 or they'll have confidence in the practice

147
00:10:57,000 --> 00:10:59,500
 and they'll be thinking about how they want to share the

148
00:10:59,500 --> 00:11:00,000
 meditation

149
00:11:00,000 --> 00:11:03,000
 with their family or how they want to become a monk

150
00:11:03,000 --> 00:11:09,000
 or they're ready to give up their lives for the practice.

151
00:11:09,000 --> 00:11:15,000
 Sometimes it's faith in the Buddha or in the teacher

152
00:11:15,000 --> 00:11:17,000
 and so they think about how great their teacher is

153
00:11:17,000 --> 00:11:20,000
 or how great the Buddha was.

154
00:11:20,000 --> 00:11:24,000
 This great faith come up and it's a good feeling

155
00:11:24,000 --> 00:11:28,000
 and they get caught up in the faith and as a result

156
00:11:28,000 --> 00:11:32,000
 it can distract them from the practice as well.

157
00:11:32,000 --> 00:11:36,990
 The great faith is something that has to be noted when it

158
00:11:36,990 --> 00:11:38,000
 arises.

159
00:11:38,000 --> 00:11:42,000
 Even confidence in oneself can be dangerous

160
00:11:42,000 --> 00:11:49,000
 and certainly is not a substitute for actual practice.

161
00:11:49,000 --> 00:11:53,000
 Number six is effort, so energy.

162
00:11:53,000 --> 00:11:55,000
 This is mental energy.

163
00:11:55,000 --> 00:11:57,670
 The meditator feels like they can practice all day and all

164
00:11:57,670 --> 00:11:58,000
 night.

165
00:11:58,000 --> 00:12:01,000
 At times, this is, not everyone will have this

166
00:12:01,000 --> 00:12:04,000
 but for some meditators they can't sit still.

167
00:12:04,000 --> 00:12:07,000
 They feel like they have to run around

168
00:12:07,000 --> 00:12:10,000
 or feel like they just have so much energy

169
00:12:10,000 --> 00:12:13,000
 that they just want to meditate all the time.

170
00:12:13,000 --> 00:12:16,000
 Their minds are working very quickly

171
00:12:16,000 --> 00:12:23,000
 and their bodies are very strong and energetic.

172
00:12:23,000 --> 00:12:29,140
 It's easy to get caught up in this as well, in certain

173
00:12:29,140 --> 00:12:30,000
 cases.

174
00:12:30,000 --> 00:12:34,000
 Number seven is actually mindfulness.

175
00:12:34,000 --> 00:12:38,000
 The meditator feels like they have great mindfulness.

176
00:12:38,000 --> 00:12:40,000
 Their mind is very sharp

177
00:12:40,000 --> 00:12:51,000
 and they're able to note everything clearly and well.

178
00:12:51,000 --> 00:12:55,000
 Even this one is considered to be, can have a negative

179
00:12:55,000 --> 00:12:56,000
 effect

180
00:12:56,000 --> 00:13:00,540
 if the meditator clings to it, if the meditator becomes

181
00:13:00,540 --> 00:13:01,000
 somehow satisfied

182
00:13:01,000 --> 00:13:04,000
 or I guess thinking about it

183
00:13:04,000 --> 00:13:08,000
 and obsessing over the fact that they're so mindful.

184
00:13:08,000 --> 00:13:13,000
 We should note the thoughts, the observations that arise

185
00:13:13,000 --> 00:13:17,000
 and we're thinking about how mindful we are, for example.

186
00:13:17,000 --> 00:13:20,000
 Number eight is happiness.

187
00:13:20,000 --> 00:13:24,000
 The meditator might feel very happy at times

188
00:13:24,000 --> 00:13:27,000
 but happiness seems to be the goal.

189
00:13:27,000 --> 00:13:32,000
 They obsess over it and focus on it and enjoy it.

190
00:13:32,000 --> 00:13:36,000
 This distracts, of course, from the practice.

191
00:13:36,000 --> 00:13:38,000
 Sometimes a meditator feels calm.

192
00:13:38,000 --> 00:13:42,000
 Number nine is sort of equanimous.

193
00:13:42,000 --> 00:13:45,000
 They feel like they don't care about anything.

194
00:13:45,000 --> 00:13:50,000
 They feel totally blank, no positive, no negative feelings.

195
00:13:50,000 --> 00:13:53,000
 This one can be a great,

196
00:13:53,000 --> 00:13:56,000
 if it's based on insight it can be a great thing.

197
00:13:56,000 --> 00:14:01,000
 It's a precursor to the higher attainments.

198
00:14:01,000 --> 00:14:04,000
 But if it's just not caring, the meditator feels sort of

199
00:14:04,000 --> 00:14:07,000
 like they don't care about anything,

200
00:14:07,000 --> 00:14:09,000
 they don't want to practice,

201
00:14:09,000 --> 00:14:11,000
 they don't want to go home,

202
00:14:11,000 --> 00:14:15,000
 they just feel totally neutral.

203
00:14:15,000 --> 00:14:18,000
 You can mistake this for the path

204
00:14:18,000 --> 00:14:21,000
 and just cultivate this state of neutrality

205
00:14:21,000 --> 00:14:23,000
 without actually being mindful.

206
00:14:23,000 --> 00:14:25,000
 It comes from mindfulness.

207
00:14:25,000 --> 00:14:27,000
 Neutrality is a great thing.

208
00:14:27,000 --> 00:14:31,000
 It has to come from truly observing things,

209
00:14:31,000 --> 00:14:34,000
 truly investigating it,

210
00:14:34,000 --> 00:14:38,000
 paying attention.

211
00:14:38,000 --> 00:14:41,000
 Number ten is nikanti,

212
00:14:41,000 --> 00:14:47,000
 which means satisfaction.

213
00:14:47,000 --> 00:14:51,000
 So this subtle desire,

214
00:14:51,000 --> 00:14:54,000
 so a desire for sights and sounds.

215
00:14:54,000 --> 00:14:58,000
 When you see or hear a smell or taste or feel something

216
00:14:58,000 --> 00:15:04,000
 and the mind becomes kind of enjoying a bit,

217
00:15:04,000 --> 00:15:07,000
 gets caught up in sights and sounds,

218
00:15:07,000 --> 00:15:13,000
 listening to sounds around you, birds or whatever.

219
00:15:13,000 --> 00:15:17,000
 And the sense of sort of contentment.

220
00:15:17,000 --> 00:15:21,000
 And this sort of contentment can be distracting as well.

221
00:15:21,000 --> 00:15:25,000
 The meditator feels like their mind is perfect

222
00:15:25,000 --> 00:15:28,000
 or things are perfect sometimes.

223
00:15:28,000 --> 00:15:34,000
 The wind is blowing, the birds are singing, cool breeze,

224
00:15:34,000 --> 00:15:38,000
 and body feels good, mind feels good.

225
00:15:38,000 --> 00:15:43,000
 It's easy to become complacent when good things arise.

226
00:15:43,000 --> 00:15:45,000
 So there's a lot of good things.

227
00:15:45,000 --> 00:15:46,000
 Good things are not a problem,

228
00:15:46,000 --> 00:15:48,000
 but these are things you have to watch out for

229
00:15:48,000 --> 00:15:51,720
 because no matter how much you understand it intellectually

230
00:15:51,720 --> 00:15:52,000
,

231
00:15:52,000 --> 00:15:55,000
 you're still inclined towards these things

232
00:15:55,000 --> 00:15:58,000
 and enjoy them when they come.

233
00:15:58,000 --> 00:16:00,000
 So you have to take this into your practice.

234
00:16:00,000 --> 00:16:03,000
 It should be part of your practice

235
00:16:03,000 --> 00:16:08,000
 to be aware when positive experiences arise

236
00:16:08,000 --> 00:16:10,000
 and be ready to deal with them,

237
00:16:10,000 --> 00:16:13,000
 not let them overwhelm your practice

238
00:16:13,000 --> 00:16:17,000
 or get in the way of your practice.

239
00:16:17,000 --> 00:16:24,000
 So just a little something, some dhamma for today.

240
00:16:24,000 --> 00:16:26,000
 Thanks for tuning in.

