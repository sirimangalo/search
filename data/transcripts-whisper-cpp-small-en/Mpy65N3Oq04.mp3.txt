 Okay, good evening everyone.
 Welcome to our evening Dhamma session.
 So I've been invited by a friend, a friend of mine.
 One of the monks here is putting on a big conference in a
 couple weeks.
 And they needed people to give, to speak at it.
 I'm giving a talk.
 I don't want to make it sound like somehow they sought me
 out for it.
 I think I was just available.
 But one of the talks is going to be on wrong mindfulness.
 And the other is going to be on Buddhism in a digital age.
 So I thought maybe in the next week or so I can go over
 some of the ideas, test them
 out on you.
 My captive audience.
 So tonight I thought I'd talk about modern Buddhism.
 Buddhism in a digital age.
 Even just generally a modern age.
 There's one fairly clear debate or division in modern
 Buddhism and that's between engagement
 and renunciation.
 I think most Buddhism is renunciant.
 Is on the side of renunciation.
 Leaving society going off in the forest.
 When you think of Buddhism, I think it's still more common
 to think of the Buddha off under
 a tree in the forest.
 But there are those who struggle with that and argue for a
 Buddhism or feel more comfortable
 practicing a Buddhism that is engaged.
 That stays in the world and works to better the world.
 Works to engage with the world rather than disengage.
 It's a struggle that Buddhists individually go through and
 it's a division between various
 Buddhist institutions, some very social oriented and some
 very reclusive oriented.
 And so this is seen as sort of a modern dilemma I think but
 it really is actually an ancient
 dilemma.
 In the time of the Buddha there was this question of
 whether one should be engaged or one should
 be secluded.
 And so you have Devadatta, the Buddhist cousin who made all
 sorts of trouble and did all
 sorts of wicked things.
 He tried to entrap the Buddha by proposing five rules, one
 of which was that monks should
 always live in the forest.
 And he knew that the Buddha wouldn't accept these rules and
 so by proposing them he wanted
 to show that the Buddha was soft, was unfit to be the
 leader.
 That Devadatta was the true hardcore dedicated leader.
 But the Buddha said of course no, he said no monks don't.
 If monks live in the city they should practice Buddhism.
 If monks live in the forest they should also practice the D
hamma.
 And this isn't even originally a Buddhist debate, this is
 in the context of Brahmanism
 which had this sort of question as well of how do you fit
 in this idea of going off in
 the forest.
 And it has sort of this, India is interesting because there
's this fairly clear meeting
 of cultures, the indigenous culture and the Aryan culture,
 wherever the Aryans came from,
 these fire-worshipping, horse-riding barbarians.
 And so it appears that there's this sort of, there arose
 this high brow cultured society
 that was very metropolitan, very urban.
 And then there were these mystics, shamans, this is where
 the word shaman probably comes
 from, shramana, who were from a fairly indigenous culture
 but they were these wise men mostly
 who went off in the forest and had deep mystical
 experiences or maybe they were just crazy.
 But were looked upon as holy men.
 And so as society developed there was a question of how
 these men fit in and what was the proper,
 as everything, as all these indigenous cultures go they
 eventually become institutionalized.
 And so eventually they tried at least, it seems, to set up
 some kind of system because it was
 disruptive for men to just decide and women sometimes that
 they were going to leave home
 and go off and live in the forest.
 And so they made these four ages.
 The proper way to do it was to wait until you're old and
 retire.
 You're a student and then you're a householder and then you
 retire but live at home and then
 once everything is settled, passed on to your children, you
'll even go off and live in the
 forest.
 The sannyasa stage.
 And so this is what the Buddha came into and the Buddha was
 of course one of these men
 although he didn't wait to retire.
 He was quite the rebel.
 He left home when he was 29 apparently.
 Still just preparing to become leader of his clan.
 But I think what you can see from the Buddhist teaching and
 maybe you can get a sense of
 where I'm going with this is that there isn't a clear
 division in the Buddhist teaching.
 There isn't a decision made on one way or the other.
 I think clearly the goal is renouncing the world in one
 sense but in another sense it's
 to some extent taking other people with you in general.
 And thereby working with the world.
 Freedom from the world doesn't come without the world.
 It's not like a jail cell you can just break out of.
 This is what you're learning in the meditation course.
 You can't just get away from your problems.
 That's not the way out of suffering.
 It's your problems that free you from your problems.
 The satipatthana sutta is a very good, it's a very special
 teaching, very powerful teaching.
 Just simple words when you're angry know that you're angry.
 When you're in pain know that it's pain.
 When you're walking know that you're walking.
 Not running away from anything.
 And the world either.
 To some extent the world is part of our practice.
 Other people are part of our practice.
 And so engagement is a part of our practice.
 Renunciation and engagement.
 And so when I look at Buddhism, I look at this division and
 I think it's an important
 one.
 But rather than a division among Buddhists, it's a division
 among Buddhist practices.
 There are social practices and there are spiritual practice
.
 They're engaged and they're renunciate practices.
 And both I think arguably are, should be, or at least can
 be part of our past enlightenment.
 So, what I'm going to try to do in my talk, in fact this
 might be exciting, I'm going
 to try to figure out the exact time and see if we can do a
 second life session live.
 So have all of you here with me and then about halfway
 through just pop it up and show that
 you've all been sitting there listening.
 Just to show the power of second life really as a
 communication platform.
 But I'm going to try to highlight some of the tools that I
've created or some of the
 means by which I've used the internet, used technology.
 And as some other people have created, I mean talk about
 the Buddha Center for example.
 But I want to make clear this distinction that our social
 practice, I mean a good example
 of a social practice is Metta.
 Metta is a meditative practice.
 You just sit quietly alone and send love to the whole world
.
 But it's social as well in the sense of it changes the way
 we react to others.
 It changes the way we interact with others.
 We approach other people with love as a result.
 I mean when we talk about engaged Buddhism we mean changing
 the world.
 We mean doing something to improve the world not just
 running away and freeing ourselves.
 The fact is improving your own mind does improve the world.
 Metta is a very social practice.
 Changing yourself is a very social practice.
 To that end renunciation in a Buddhist sense which of
 course is internal.
 It has nothing really to do with running away to the forest
.
 Is in fact a social practice.
 I mean part of it is social in the sense that it changes
 your social interactions.
 It makes you wiser.
 I mean the most important thing it does is teaches you.
 Teaches you right from wrong, good from bad.
 It makes your mind clear when dealing with challenges,
 difficulties, conflicts.
 I don't know if there really is this division in fact.
 Maybe two sides of the practice.
 The Buddha answering this question of helping yourself or
 helping others.
 He says when you help yourself you help other people.
 When you help other people you help yourself.
 I don't think there was an equivalence there.
 It's very much clear that helping yourself is a much better
 thing to focus on.
 There are certainly teachings to that extent.
 And renunciation and going off to live in the forest is a
 great and wonderful thing.
 But it's certainly not all of Buddhism.
 And I don't think there's a need to create a division of
 sorts.
 That's the first part of my talk I think.
 The talk about all the technology I use.
 Of course that's very interesting but I don't know that it
's a topic for tonight's Dhamma.
 So that's sort of the direction I'm heading to talk about.
 This idea of engaged versus renunciate.
 Renunciant.
 I don't know the actual word.
 But for us I think that's important.
 It's important to be clear.
 This isn't running away.
 Coming here isn't running away.
 Your practice doesn't end here in the meditation center.
 This is a tool that you use in your life.
 And it's a tool that you use here to help you with your
 life.
 To better deal with your challenges and conflicts and so on
.
 So a little bit of food for thought.
 Dhamma for tonight.
 Thank you all for coming up.
 And I've got the question page up.
 So I can answer some questions here.
 So completely coincidental.
 The top question and it is completely coincidental.
 The top question is about what my thoughts are on important
 issues like climate change
 and how realization could help ease people's bias against
 these crises.
 In fact it's a good point to bring up in my talk.
 Because of course things like climate change are brought
 about by nothing more than greed.
 At the root.
 You know if we weren't so greedy for consumption.
 Even just greedy to procreate.
 Too many babies.
 As the Dalai Lama said we need more monks.
 There's too many people in this world.
 I'm not sure about that.
 I don't know what the ethics are there but.
 I don't know about population.
 I won't touch that one but certainly for consumption.
 Our consumption is way out of hand.
 And our need for products regardless of the cost to the
 environment.
 Our need for instant gratification is completely.
 Even just our need for meat for example.
 We don't need to eat meat.
 We have no reason to eat meat.
 And it's incredibly expensive to the climate and even to
 our individuals.
 So yeah I think our practice certainly improves these.
 And I've talked about this before but I think things like
 climate change are not incredibly
 worth focusing on because it's like a band-aid solution.
 Sure you get people to be concerned about the climate.
 And if you don't.
 Well I mean looking at the climate does make people more
 aware of their greed and force
 people to give up their greed.
 But I mean let's be clear that the real problem, the most
 important thing to address is our
 greed.
 Did the Buddha possess any supernatural powers or miracles?
 I mean I've never met the Buddha not in this life obviously
.
 But I have no problem believing that the Buddha had powers
 beyond what most of us would believe
 possible or powers beyond what most of us are capable of.
 We have to hold our hands up to our stomach as opposed to
 laying them on our laps.
 Is either position okay?
 Yes.
 The body position in our tradition not so important.
 It's all about the mind.
 The mind is what's more important.
 And Theravāda recently read that only those who practice
 meditative monastic life can attain
 spiritual perfection.
 Enlightenment is not thought possible for those living in
 the secular life.
 Of course it's easier that way but could it be possible?
 Yes whoever wrote that doesn't know what they're talking
 about.
 There's a queen who became an arahant.
 Very famous example.
 It's very Theravāda Buddha's the queen.
 And the Buddha said "Ah well either she becomes a bhikkhuni
 or she just wastes away because
 there's no way she's going to be able to be a queen."
 And so the king said "Well then let her become a bhikkhuni
."
 Okay this is a long question.
 I'm not going to read it all but well you know maybe it's
 nice.
 Person who suffered extreme stress for many years, ten plus
 years, withdrawn into serious
 depression, debilitating anxiety, been seriously studying
 your YouTube videos for a few months
 but am new to consistent daily practice.
 And today I don't know if you've picked up my booklet so
 there are many ways to practice
 but I recommend reading my booklet if you haven't.
 Today experience something wonderful.
 So a sudden burst of warm intense happiness.
 Tears of joy.
 Can't recall such an overwhelming sensation.
 Came out of nowhere.
 Half of me just wants to share my excitement and the other
 half curious wants to know is
 this normal.
 So well great.
 I mean it's great for you to be able to see that you're not
 cursed, you're not stuck.
 I would say you can see that now that your brain is capable
 of pleasure and that the
 meditation clearly helps you break out of this rut because
 our behaviors become habits.
 And so things like depression for ten years that's got you
 stuck in a fairly deep rut
 but miracle of miracles just a little bit of meditation and
 you can see you can pop
 yourself back out of it.
 Why because those ten years they aren't it's not like you
 add up those ten years and you've
 got ten years worth of depression.
 Old stuff fades.
 We're still only dealing with the present moment.
 So if in this moment you start meditating people wonder how
 can a few weeks of meditation
 counteract years of depression or anxieties because those
 years don't exist.
 Their past it's just you counting.
 The truth is well what are you now and it's based on what
 you've done before.
 There's no question that all of that had an effect but you
're still just dealing with
 you and your brain right.
 And so creating new habits of mindfulness I mean it works.
 It pops you out of it.
 So your next step of course is to learn to become mindful
 of the joy because that's not
 the goal.
 It's a good sign.
 It's a sign that you're learning to let go of the
 depression and so on and find ways
 to be more centered more more peaceful.
 But the joy itself is something that you have to note
 otherwise you get attached to it and
 you'll be looking for it and you'll even get upset when it
 doesn't come.
 I mean you'll see this as you practice but just to give you
 a bit of a shortcut so you
 don't get caught up in it.
 You don't get caught up in anything.
 The Buddha said nothing is worth clinging to so
 congratulations good for you but the
 next step when and maybe it won't come again maybe not for
 a long time maybe never but
 learn to deal with it when it comes pleasure when it comes
 and desire for it excitement
 about it all of that is part of the practice.
 It's not to take away from this experience it's a great
 sign.
 It's just onward and upward.
 There's more to do.
 Good for you.
 Really good.
 Really nice to hear.
 Thank you for writing that out.
 I suffer from low self-esteem and it can only be boosted
 through activities.
 How can I cultivate it permanently?
 Well we don't need self-esteem we're worthless.
 You are worthless I am worthless.
 If you can figure that out then you don't have to worry
 about self-esteem.
 Stop esteeming yourself.
 It's useless.
 We are worthless.
 We are worthless means all of this is worthless meaningless
 doesn't it's what you make of
 it.
 You can pretend that this house and your job and your face
 and your body has some meaning.
 Your brain your intelligence your memory.
 You can claim that it all has meaning and worth but it's
 all just subjective it's what
 you make of it.
 And that frees you seeing that frees you because then you
 can look at well what's really important
 and what's really important is happiness and suffering
 peace and stress.
 I have a sense of always being alone even when with others
 in groups not a sense of
 loneliness but it's like I'm the only one there is this
 normal.
 So these questions of what is normal are not I've talked
 about this before it's not proper
 to ask and I should put it in the things please don't ask
 is it normal or please you know
 rephrase it and ask yourself what do you really want to
 know and you want to know whether
 you are a freak and is that what you're asking because you
 are you and that's most important.
 It's most important to see what is asking what's normal
 doesn't do us any good because
 well who cares maybe normal is a horrible thing to be right
.
 We're not trying to be normal we're trying to be good and
 pure and perfect to some extent
 free we're trying to be free.
 So good question would be is that is that a positive state
 is it a negative state those
 would be better questions to ask.
 I mean I'm not criticizing this people ask this all the
 time but it's just I think it
 may be a little lazy because we you know is this normal.
 It doesn't it's not a useful question to got it twice
 tonight and I'm not I'm not attacking
 you for it.
 I think I would probably ask the same thing but it's it's
 you have to think it through
 what do you really want to know.
 So good question is is it is it good is it bad is it
 probably more important.
 So it's neither I mean it's an experience.
 So try to note it and be mindful of your feeling feeling or
 something.
 If you like it or dislike it you know that and so on.
 Okay that's all the questions for tonight.
 1
 1
