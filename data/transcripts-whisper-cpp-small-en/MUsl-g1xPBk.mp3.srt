1
00:00:00,000 --> 00:00:02,920
 This is the classic question on the fourth precept.

2
00:00:02,920 --> 00:00:06,790
 Say you were strictly following the precepts, you see a

3
00:00:06,790 --> 00:00:08,400
 terrified man run past you,

4
00:00:08,400 --> 00:00:11,510
 then another man comes up to you with a knife and asks if

5
00:00:11,510 --> 00:00:13,280
 you seen where the first man went.

6
00:00:13,280 --> 00:00:17,190
 Although you know, although you know, would it be

7
00:00:17,190 --> 00:00:19,760
 appropriate to lie and say you don't know never seen him,

8
00:00:19,760 --> 00:00:21,760
 or would you have to tell the truth?

9
00:00:21,760 --> 00:00:27,670
 This is always given as an example of how quote-unquote the

10
00:00:27,670 --> 00:00:29,720
 precepts are a guy

11
00:00:30,000 --> 00:00:32,000
 not a rule.

12
00:00:32,000 --> 00:00:34,000
 People believe that

13
00:00:34,000 --> 00:00:37,320
 there are instances where breaking the precepts is

14
00:00:37,320 --> 00:00:40,040
 appropriate and they have all sorts of,

15
00:00:40,040 --> 00:00:42,520
 there's even better ones than this you could come up with,

16
00:00:42,520 --> 00:00:43,920
 but this is the classic one.

17
00:00:43,920 --> 00:00:47,480
 But it was a silly one

18
00:00:47,480 --> 00:00:50,120
 and please don't take this personally because this is the

19
00:00:50,120 --> 00:00:51,200
 classic question.

20
00:00:51,200 --> 00:00:54,040
 And so someone, this was explained, I was sitting in a

21
00:00:54,040 --> 00:00:56,360
 group and this monk was explaining this and

22
00:00:56,360 --> 00:00:59,400
 I said to him well,

23
00:00:59,760 --> 00:01:03,100
 couldn't you just, couldn't you just not say anything? And

24
00:01:03,100 --> 00:01:05,960
 he was like, oh, yeah, I guess you could.

25
00:01:05,960 --> 00:01:10,800
 Because keeping the precepts doesn't mean you have to say

26
00:01:10,800 --> 00:01:11,680
 stuff.

27
00:01:11,680 --> 00:01:15,040
 Keeping the precepts means

28
00:01:15,040 --> 00:01:19,510
 not doing things. I mean, so suppose this guy even says

29
00:01:19,510 --> 00:01:22,920
 tell me where he went or I'll beat you up.

30
00:01:22,920 --> 00:01:26,360
 And you say,

31
00:01:26,360 --> 00:01:28,600
 you're welcome to beat me up if you like.

32
00:01:28,600 --> 00:01:32,480
 It's really none of my concern.

33
00:01:32,480 --> 00:01:35,720
 Something like that and then they might beat you up and

34
00:01:35,720 --> 00:01:37,800
 that's it. They might kill you and so on.

35
00:01:37,800 --> 00:01:40,560
 It doesn't mean that you have to tell a lie.

36
00:01:40,560 --> 00:01:43,920
 Doesn't mean you have to tell the truth.

37
00:01:43,920 --> 00:01:48,300
 That's really the proper thing to do. There's an

38
00:01:48,300 --> 00:01:49,840
 interesting article I read recently

39
00:01:49,840 --> 00:01:55,420
 by Sam Harris and probably some of you know who Sam Harris

40
00:01:55,420 --> 00:01:55,800
 is.

41
00:01:56,440 --> 00:01:58,440
 He's an atheist.

42
00:01:58,440 --> 00:02:01,440
 They don't like to be called that. I'm an atheist too.

43
00:02:01,440 --> 00:02:05,020
 But by atheist, I mean he's someone who talks a lot about

44
00:02:05,020 --> 00:02:07,120
 atheism among other things.

45
00:02:07,120 --> 00:02:11,070
 But it's something that he's well known for his views on

46
00:02:11,070 --> 00:02:13,840
 theism and the silliness of it all.

47
00:02:13,840 --> 00:02:17,830
 And he wrote an article recently about self-defense and it

48
00:02:17,830 --> 00:02:19,560
 got me thinking because

49
00:02:19,560 --> 00:02:23,080
 he's going on and on about how you have to,

50
00:02:23,920 --> 00:02:26,120
 you have to react and you have to

51
00:02:26,120 --> 00:02:30,600
 don't go along with things.

52
00:02:30,600 --> 00:02:34,370
 When you're being attacked, don't go along with it. And at

53
00:02:34,370 --> 00:02:35,720
 first I didn't agree with it.

54
00:02:35,720 --> 00:02:39,040
 But I think some of it's agreeable because self-defense is

55
00:02:39,040 --> 00:02:41,640
 for sure allowable. You don't want to

56
00:02:41,640 --> 00:02:45,140
 if someone's trying to rape you or so and you don't want to

57
00:02:45,140 --> 00:02:46,640
 have them rape you because

58
00:02:46,640 --> 00:02:49,840
 you're not going to agree with it.

59
00:02:50,720 --> 00:02:53,040
 The reason why you don't want them to rape you is because

60
00:02:53,040 --> 00:02:53,840
 it's bad for them.

61
00:02:53,840 --> 00:02:58,840
 Which many people find difficult to understand but

62
00:02:58,840 --> 00:03:02,910
 the reason why you don't want people to murder you is

63
00:03:02,910 --> 00:03:05,000
 because it's bad for them.

64
00:03:05,000 --> 00:03:08,200
 That's I think a really good excuse not to let it happen.

65
00:03:08,200 --> 00:03:12,550
 Another good excuse is because you want to live yourself so

66
00:03:12,550 --> 00:03:13,960
 that you can do good stuff.

67
00:03:13,960 --> 00:03:16,810
 And you think if I die that I don't know where I'm going to

68
00:03:16,810 --> 00:03:17,000
 go.

69
00:03:17,280 --> 00:03:20,560
 I'm not enlightened yet and so better I stick around for a

70
00:03:20,560 --> 00:03:22,120
 while to do more good deeds.

71
00:03:22,120 --> 00:03:24,120
 There's lots of good reasons for defending yourself.

72
00:03:24,120 --> 00:03:28,440
 But

73
00:03:28,440 --> 00:03:33,080
 I think there still is some

74
00:03:33,080 --> 00:03:37,160
 some case for

75
00:03:37,160 --> 00:03:41,620
 especially an extreme example. The example that I thought

76
00:03:41,620 --> 00:03:43,160
 of was of this monk who was in

77
00:03:46,080 --> 00:03:48,080
 Rajagaha.

78
00:03:48,080 --> 00:03:52,230
 There's a Theravada monk who was going walking through

79
00:03:52,230 --> 00:03:53,440
 India barefoot.

80
00:03:53,440 --> 00:03:57,880
 And he had his lay attendant with him

81
00:03:57,880 --> 00:04:01,040
 carrying the money and

82
00:04:01,040 --> 00:04:05,440
 they decided that they would camp out on

83
00:04:05,440 --> 00:04:07,840
 Gijakuta.

84
00:04:07,840 --> 00:04:10,860
 Gijakuta Vulture's Peak, which is a very famous place where

85
00:04:10,860 --> 00:04:11,680
 the Buddha lived and

86
00:04:12,440 --> 00:04:17,200
 many of the great disciples lived.

87
00:04:17,200 --> 00:04:22,850
 It's kind of like the mount in the Sermon on the Mount. It

88
00:04:22,850 --> 00:04:24,400
's a very important place in Buddhism.

89
00:04:24,400 --> 00:04:27,860
 So I thought cool. We'll just camp out up here. Big mistake

90
00:04:27,860 --> 00:04:29,760
. Don't camp in Bihar.

91
00:04:29,760 --> 00:04:32,860
 I don't know if it's better now, but the idea of camping in

92
00:04:32,860 --> 00:04:34,080
 Bihar is ridiculous.

93
00:04:34,080 --> 00:04:37,560
 Tourists get

94
00:04:38,680 --> 00:04:44,560
 murdered in Bihar. It's a very poor province and

95
00:04:44,560 --> 00:04:49,280
 lots of bandits and so on. And so indeed

96
00:04:49,280 --> 00:04:53,400
 they were confronted by bandits. Actually Gijakuta is safe

97
00:04:53,400 --> 00:04:54,240
 now, I think.

98
00:04:54,240 --> 00:04:57,910
 There's guys with guns guarding Gijakuta maybe because of

99
00:04:57,910 --> 00:05:00,720
 things like this happening. But you know anyway at night

100
00:05:00,720 --> 00:05:03,980
 I don't think the guards stay, stick around. And so at

101
00:05:03,980 --> 00:05:06,680
 night they were confronted by these bandits.

102
00:05:08,280 --> 00:05:10,200
 And

103
00:05:10,200 --> 00:05:12,200
 the

104
00:05:12,200 --> 00:05:16,160
 lay attendant, the guy who was walking with him,

105
00:05:16,160 --> 00:05:20,480
 charged them and

106
00:05:20,480 --> 00:05:23,330
 ran right, broke right through them, scuffled with them a

107
00:05:23,330 --> 00:05:27,350
 little bit and ran away and jumped down from the top of Gij

108
00:05:27,350 --> 00:05:28,640
akuta into the ravine.

109
00:05:28,640 --> 00:05:31,760
 You know like

110
00:05:31,760 --> 00:05:35,680
 not breaking anything but scratching himself terribly and

111
00:05:35,680 --> 00:05:36,920
 bruising himself.

112
00:05:37,880 --> 00:05:39,880
 And

113
00:05:39,880 --> 00:05:41,880
 and the monk,

114
00:05:41,880 --> 00:05:45,350
 the monk sat there and they said we're going to kill you

115
00:05:45,350 --> 00:05:45,880
 now.

116
00:05:45,880 --> 00:05:49,640
 What the monk said that he did is he just,

117
00:05:49,640 --> 00:05:52,530
 they had a knife pointed at his neck and he just lifted up

118
00:05:52,530 --> 00:05:54,560
 his neck like this to give them the,

119
00:05:54,560 --> 00:05:58,800
 to give them his flesh like okay, if you're going to do it,

120
00:05:58,800 --> 00:06:00,600
 go ahead and do it. And

121
00:06:00,600 --> 00:06:03,280
 the point is that they didn't kill him.

122
00:06:03,280 --> 00:06:05,960
 They,

123
00:06:06,280 --> 00:06:09,020
 the point is that there's a power in that. There's a power

124
00:06:09,020 --> 00:06:09,760
 in letting go.

125
00:06:09,760 --> 00:06:15,600
 That very often makes you immune to, to,

126
00:06:15,600 --> 00:06:18,880
 to evil.

127
00:06:18,880 --> 00:06:21,990
 And as a result it was, it would have been quite difficult

128
00:06:21,990 --> 00:06:23,640
 for them to hurt him.

129
00:06:23,640 --> 00:06:25,320
 It would have been quite easy for them to hurt the other

130
00:06:25,320 --> 00:06:28,320
 guy because he was totally unmindful and he had lost his

131
00:06:28,320 --> 00:06:29,400
 presence of mind.

132
00:06:31,320 --> 00:06:34,140
 They didn't kill him and the, the guy that jumped down in

133
00:06:34,140 --> 00:06:35,960
 the ravine, the story goes that he

134
00:06:35,960 --> 00:06:40,220
 realized that he had left this monk defenseless and so he

135
00:06:40,220 --> 00:06:42,320
 climbed back up and

136
00:06:42,320 --> 00:06:46,290
 surrendered himself and they had to give up all their

137
00:06:46,290 --> 00:06:49,400
 clothes and robes and bowls and money and

138
00:06:49,400 --> 00:06:53,330
 camera and everything. All they left him, the monk with was

139
00:06:53,330 --> 00:06:55,640
 his lower robe so like a sarong.

140
00:06:58,120 --> 00:07:02,120
 But they left them alive and somehow they, they survived

141
00:07:02,120 --> 00:07:03,200
 and got out of it and

142
00:07:03,200 --> 00:07:06,560
 wanted to stay in a Thai monastery and had a happy ending

143
00:07:06,560 --> 00:07:07,240
 in the end.

144
00:07:07,240 --> 00:07:10,480
 But

145
00:07:10,480 --> 00:07:12,480
 anyway, that's not exactly

146
00:07:12,480 --> 00:07:14,600
 the question you're asking but

147
00:07:14,600 --> 00:07:17,360
 you certainly don't have to lie and

148
00:07:17,360 --> 00:07:21,460
 in the case where you're forced to do something unethical,

149
00:07:21,460 --> 00:07:22,920
 just don't do it

150
00:07:23,760 --> 00:07:26,440
 regardless of the consequences because people always make

151
00:07:26,440 --> 00:07:29,390
 up these things. Well, what if, if they, if you, if you don

152
00:07:29,390 --> 00:07:29,920
't do it,

153
00:07:29,920 --> 00:07:31,760
 they're going to do this.

154
00:07:31,760 --> 00:07:35,000
 But, you know, the point is what you're saying is that if

155
00:07:35,000 --> 00:07:37,210
 you don't do something unethical, somebody else is going to

156
00:07:37,210 --> 00:07:38,920
 do something unethical and

157
00:07:38,920 --> 00:07:43,600
 you're not responsible for their, their

158
00:07:43,600 --> 00:07:46,720
 unethical behavior. It's not your

159
00:07:46,720 --> 00:07:50,840
 bad karma if someone else does something bad. It would only

160
00:07:50,840 --> 00:07:53,620
 be an intellectual exercise if you said

161
00:07:54,740 --> 00:07:56,740
 "I

162
00:07:56,740 --> 00:07:59,880
 should do something bad because otherwise these people are

163
00:07:59,880 --> 00:08:01,620
 going to do something worse."

164
00:08:01,620 --> 00:08:05,580
 The, the ethics of it are quite clear. You're not doing

165
00:08:05,580 --> 00:08:06,980
 anything wrong and

166
00:08:06,980 --> 00:08:11,020
 so from a point of view of reality,

167
00:08:11,020 --> 00:08:15,940
 there's no need ever to do something bad in order to avert

168
00:08:15,940 --> 00:08:18,900
 badness.

169
00:08:18,900 --> 00:08:22,780
 You, you're not responsible for other people's acts.

170
00:08:23,660 --> 00:08:26,080
 If, and knowing that people go according to their karma and

171
00:08:26,080 --> 00:08:28,420
 so on. It's, it's quite a difficult,

172
00:08:28,420 --> 00:08:31,040
 I mean, it's quite clear in my mind, but it's, I have

173
00:08:31,040 --> 00:08:33,730
 trouble expressing this because I know people feel quite

174
00:08:33,730 --> 00:08:34,940
 strongly like if your

175
00:08:34,940 --> 00:08:38,740
 family was involved, people would be very much attached to

176
00:08:38,740 --> 00:08:42,380
 their family, but it goes totally against the, against

177
00:08:42,380 --> 00:08:45,020
 reality actually and it falls into this

178
00:08:45,020 --> 00:08:48,580
 conventional reality of, of

179
00:08:48,580 --> 00:08:50,900
 relationships and our attachment to other people

180
00:08:52,020 --> 00:08:54,780
 missing the whole point of how karma works and how people

181
00:08:54,780 --> 00:08:57,550
 go according to their deeds and their relationships with

182
00:08:57,550 --> 00:08:58,020
 each other.

183
00:08:58,020 --> 00:09:01,510
 If someone's going to kill someone else, it has much more

184
00:09:01,510 --> 00:09:04,900
 to do with their relationship as two individuals than your

185
00:09:04,900 --> 00:09:09,060
 intervention and so on. So for sure

186
00:09:09,060 --> 00:09:13,220
 there, there's, there's never any reason to do

187
00:09:13,220 --> 00:09:16,020
 unwholesomeness

188
00:09:16,020 --> 00:09:18,020
 because the, the,

189
00:09:18,020 --> 00:09:21,020
 that is your action and that is your

190
00:09:22,980 --> 00:09:26,510
 that is your input into the universe, regardless of what

191
00:09:26,510 --> 00:09:28,460
 someone else is going to do.

192
00:09:28,460 --> 00:09:36,340
 Anyway, so.

