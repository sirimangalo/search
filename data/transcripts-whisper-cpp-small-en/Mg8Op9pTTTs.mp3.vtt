WEBVTT

00:00:00.000 --> 00:00:08.640
 That's a part of me that wants to give up every day.

00:00:08.640 --> 00:00:14.230
 I like being depressed, but at the same time I know I

00:00:14.230 --> 00:00:16.720
 shouldn't be like this.

00:00:16.720 --> 00:00:20.520
 I don't know what to do.

00:00:20.520 --> 00:00:21.520
 Wants to give up.

00:00:21.520 --> 00:00:23.120
 I assume you mean that in a bad way.

00:00:23.120 --> 00:00:30.080
 Wants to kill yourself, wants to give up trying, and so on.

00:00:30.080 --> 00:00:33.960
 Because actually giving up is a really good thing.

00:00:33.960 --> 00:00:41.880
 I had one student once and she was really difficult.

00:00:41.880 --> 00:00:44.980
 I pushed her and I made her really difficult for her.

00:00:44.980 --> 00:00:47.760
 She came to me and she said, "I want to kill myself."

00:00:47.760 --> 00:00:49.200
 I said, "Good, good.

00:00:49.200 --> 00:00:52.600
 Kill yourself and then keep practicing."

00:00:52.600 --> 00:00:54.680
 I ended up saying this to her several times.

00:00:54.680 --> 00:00:56.200
 She said, "Oh, I want to die."

00:00:56.200 --> 00:00:59.660
 Yes, yes, die first and then continue practicing, then

00:00:59.660 --> 00:01:00.680
 start again.

00:01:00.680 --> 00:01:02.960
 Then I would explain what I mean.

00:01:02.960 --> 00:01:05.810
 Dying is giving up the idea of self, giving up all of this

00:01:05.810 --> 00:01:07.320
 stuff that you're clinging

00:01:07.320 --> 00:01:08.320
 to.

00:01:08.320 --> 00:01:09.320
 Let go.

00:01:09.320 --> 00:01:11.000
 Yeah, letting go is good.

00:01:11.000 --> 00:01:12.000
 There was one monk.

00:01:12.000 --> 00:01:15.290
 He said, "Well, you know the Buddha was pretty bummed out

00:01:15.290 --> 00:01:16.800
 before he went forth.

00:01:16.800 --> 00:01:19.520
 So it's not wrong to be depressed.

00:01:19.520 --> 00:01:22.040
 You're depressed about things that are pretty depressing.

00:01:22.040 --> 00:01:26.270
 Generally, depressed about our inability to fit in with

00:01:26.270 --> 00:01:28.440
 people who are interested in what

00:01:28.440 --> 00:01:34.600
 turn out to be quite useless and even harmful things.

00:01:34.600 --> 00:01:38.220
 We're depressed that we can't succeed in a world where

00:01:38.220 --> 00:01:40.560
 success is measured by our ability

00:01:40.560 --> 00:01:45.480
 to defeat other people.

00:01:45.480 --> 00:01:52.310
 We are depressed about the meaninglessness of a life and a

00:01:52.310 --> 00:01:55.520
 world which turns out to be

00:01:55.520 --> 00:01:57.680
 quite meaningless.

00:01:57.680 --> 00:01:59.880
 Giving up is great.

00:01:59.880 --> 00:02:04.180
 Giving up in the same vein as someone would kill themselves

00:02:04.180 --> 00:02:06.320
 is actually, if channeled

00:02:06.320 --> 00:02:08.830
 in the right direction, is a great thing because killing

00:02:08.830 --> 00:02:10.240
 yourself isn't the answer.

00:02:10.240 --> 00:02:12.640
 Unfortunately, when you die, you're just born again.

00:02:12.640 --> 00:02:13.640
 That's the problem.

00:02:13.640 --> 00:02:15.640
 It doesn't go anywhere.

00:02:15.640 --> 00:02:16.640
 It's not killing yourself.

00:02:16.640 --> 00:02:22.330
 Killing yourself is a great thing because truly killing

00:02:22.330 --> 00:02:25.640
 yourself is on the mental front.

00:02:25.640 --> 00:02:27.760
 It's the killing of the self.

00:02:27.760 --> 00:02:33.290
 The idea that there is some reality, the idea that this is

00:02:33.290 --> 00:02:36.080
 who I am, this me who lives in

00:02:36.080 --> 00:02:41.520
 society is somehow real.

00:02:41.520 --> 00:02:44.560
 Killing yourself doesn't come from destroying the body.

00:02:44.560 --> 00:02:47.280
 That's not killing the self.

00:02:47.280 --> 00:02:49.080
 Killing the self is letting go.

00:02:49.080 --> 00:02:52.410
 When you let go and when you live your life from moment to

00:02:52.410 --> 00:02:54.360
 moment without thinking about

00:02:54.360 --> 00:02:58.920
 the past or the future, without worrying about who you are

00:02:58.920 --> 00:03:01.240
 or who you should be or who you

00:03:01.240 --> 00:03:16.560
 could be, then you will have no suffering.

00:03:16.560 --> 00:03:20.290
 First thing I would say, the second part, it's like I like

00:03:20.290 --> 00:03:21.680
 being depressed.

00:03:21.680 --> 00:03:29.080
 This is a danger because we rather than solve the problem,

00:03:29.080 --> 00:03:33.600
 we content ourselves with wallowing

00:03:33.600 --> 00:03:40.970
 in self-pity, unconsciously reassuring ourselves that we're

00:03:40.970 --> 00:03:43.080
 doing our duty.

00:03:43.080 --> 00:03:47.280
 Being depressed is an answer in a sense.

00:03:47.280 --> 00:03:48.800
 It becomes our answer.

00:03:48.800 --> 00:03:51.240
 It's just, "Yes, yes.

00:03:51.240 --> 00:03:53.840
 I know things are horrible.

00:03:53.840 --> 00:03:58.570
 That's why I'm depressed," as though that were a viable

00:03:58.570 --> 00:04:01.000
 solution to the problem.

00:04:01.000 --> 00:04:02.000
 That's the address.

00:04:02.000 --> 00:04:05.640
 You have no benefit in being depressed and you have to see

00:04:05.640 --> 00:04:06.280
 that.

00:04:06.280 --> 00:04:09.170
 All you have to do is meditate on the depression and you'll

00:04:09.170 --> 00:04:10.720
 see how meaningless it is.

00:04:10.720 --> 00:04:13.600
 You'll see how useless it is.

00:04:13.600 --> 00:04:14.600
 Look at the depression.

00:04:14.600 --> 00:04:18.090
 You'll see it disappear in a second and you'll wonder, "

00:04:18.090 --> 00:04:19.340
What's wrong?"

00:04:19.340 --> 00:04:20.720
 We miss a lot about meditation.

00:04:20.720 --> 00:04:23.220
 We don't realize that it's actually working because you med

00:04:23.220 --> 00:04:24.600
itate on it and then disappears

00:04:24.600 --> 00:04:27.960
 and you think, "Now what?"

00:04:27.960 --> 00:04:29.920
 You don't get it that actually it just worked.

00:04:29.920 --> 00:04:31.560
 The depression's gone.

00:04:31.560 --> 00:04:32.560
 Move on.

00:04:32.560 --> 00:04:37.270
 But we cling and we're comfortable with things like we

00:04:37.270 --> 00:04:40.920
 become comfortable in our suffering.

00:04:40.920 --> 00:04:41.920
 That's dangerous.

00:04:41.920 --> 00:04:43.980
 You have to be very clear with yourself not to stop the

00:04:43.980 --> 00:04:45.360
 depression or even to worry about

00:04:45.360 --> 00:04:49.890
 it or be upset about the depression, but to be honest with

00:04:49.890 --> 00:04:52.400
 yourself about the depression

00:04:52.400 --> 00:04:55.170
 because being upset about the depression is your way of

00:04:55.170 --> 00:04:56.080
 defending it.

00:04:56.080 --> 00:04:59.840
 It's your way of keeping yourself in it.

00:04:59.840 --> 00:05:01.600
 You can even say, "Yes, yes, I know I shouldn't be

00:05:01.600 --> 00:05:02.200
 depressed.

00:05:02.200 --> 00:05:04.240
 Yes, I'm such an awful person."

00:05:04.240 --> 00:05:08.360
 It makes you more depressed and therefore you're less

00:05:08.360 --> 00:05:10.160
 inclined to change.

00:05:10.160 --> 00:05:12.080
 That's the second part.

00:05:12.080 --> 00:05:15.280
 The third part where you say, "At the same time, I know it

00:05:15.280 --> 00:05:16.960
 shouldn't be like this."

00:05:16.960 --> 00:05:23.840
 Well, the question is, "How should it be then?"

00:05:23.840 --> 00:05:36.120
 Once it is that way, how can you keep it that way?

00:05:36.120 --> 00:05:40.620
 You can't force things to be in a certain way or another.

00:05:40.620 --> 00:05:42.280
 You have two choices.

00:05:42.280 --> 00:05:44.810
 One, think things should be otherwise than what they are or

00:05:44.810 --> 00:05:46.060
 two, deal with the way they

00:05:46.060 --> 00:05:48.160
 are.

00:05:48.160 --> 00:05:55.940
 I don't think there's much doubt about which is the better

00:05:55.940 --> 00:05:56.640
 way.

00:05:56.640 --> 00:05:59.410
 There can be no comparison between wishing that things were

00:05:59.410 --> 00:06:00.760
 otherwise than they are and

00:06:00.760 --> 00:06:03.760
 dealing with things as they are, coming to terms with

00:06:03.760 --> 00:06:05.040
 things as they are.

00:06:05.040 --> 00:06:08.100
 My teacher always reminds us, the Buddha always reminded us

00:06:08.100 --> 00:06:09.880
, "Don't worry about the past.

00:06:09.880 --> 00:06:10.960
 Don't worry about the future.

00:06:10.960 --> 00:06:13.720
 Make the present moment the best it can be."

00:06:13.720 --> 00:06:18.920
 This is how my teacher always puts it, "Make the present

00:06:18.920 --> 00:06:21.680
 moment the best it can be."

00:06:21.680 --> 00:06:24.320
 Again and again he reminds us, and that's really all it is.

00:06:24.320 --> 00:06:25.760
 You don't have to solve the past.

00:06:25.760 --> 00:06:26.920
 You don't have to solve the future.

00:06:26.920 --> 00:06:30.260
 You don't have to make things better because when you cling

00:06:30.260 --> 00:06:31.920
 to your idea of the way things

00:06:31.920 --> 00:06:35.150
 are better, it will one day be like that and then you'll

00:06:35.150 --> 00:06:36.840
 lose it and then it'll change

00:06:36.840 --> 00:06:37.840
 again.

00:06:37.840 --> 00:06:41.840
 You cling to it and then it won't be like that again.

00:06:41.840 --> 00:06:47.980
 What you have to do is learn to or remind yourself that see

00:06:47.980 --> 00:06:51.100
 this as a lesson that things

00:06:51.100 --> 00:06:53.400
 can change in this way.

00:06:53.400 --> 00:07:03.500
 Things can be the way you wish and later not be the way you

00:07:03.500 --> 00:07:05.080
 wish.

00:07:05.080 --> 00:07:07.400
 Admit to yourself that you need a better way.

00:07:07.400 --> 00:07:14.920
 You can't rely on things to be the way you want.

00:07:14.920 --> 00:07:17.720
 What you mean by that is that you should be like an

00:07:17.720 --> 00:07:19.460
 enlightened being or not.

00:07:19.460 --> 00:07:23.190
 The way to enlightenment is not by wishing or thinking

00:07:23.190 --> 00:07:25.200
 about how things could be.

00:07:25.200 --> 00:07:27.040
 That's about dealing with the way things are.

00:07:27.040 --> 00:07:28.200
 Deal with your depression.

00:07:28.200 --> 00:07:31.440
 Look at it.

00:07:31.440 --> 00:07:32.440
 Study it.

00:07:32.440 --> 00:07:35.280
 Study it like it were a project of yours.

00:07:35.280 --> 00:07:38.320
 You were depressed on purpose just so you could learn about

00:07:38.320 --> 00:07:38.600
 it.

00:07:38.600 --> 00:07:41.550
 Don't be depressed on purpose but think of it as just how

00:07:41.550 --> 00:07:42.440
 it should be.

00:07:42.440 --> 00:07:44.400
 Yes, yes, this is important.

00:07:44.400 --> 00:07:47.640
 It's important for me to be depressed so I can learn about

00:07:47.640 --> 00:07:48.600
 depression.

00:07:48.600 --> 00:07:50.890
 Not to the extent that you encourage it because you shouldn

00:07:50.890 --> 00:07:52.000
't encourage anything.

00:07:52.000 --> 00:07:55.080
 You should just learn about what is.

00:07:55.080 --> 00:07:59.330
 Once you learn about what is, you understand about

00:07:59.330 --> 00:08:02.520
 everything because what is, is and that's

00:08:02.520 --> 00:08:03.200
 all that is.

