1
00:00:00,000 --> 00:00:02,000
 Okay, go

2
00:00:02,000 --> 00:00:06,240
 How do you deal with self-hate I try to observe the hate

3
00:00:06,240 --> 00:00:07,760
 but it is so strong

4
00:00:07,760 --> 00:00:12,170
 Do I need to practice metta and how do I do that if I hate

5
00:00:12,170 --> 00:00:12,860
 myself?

6
00:00:12,860 --> 00:00:15,800
 Thank you

7
00:00:15,800 --> 00:00:22,160
 Don't

8
00:00:22,160 --> 00:00:25,860
 Know that it actually yeah, it can't certainly can't hurt

9
00:00:25,860 --> 00:00:28,200
 to express love for yourself, but

10
00:00:28,200 --> 00:00:32,880
 It's problematic

11
00:00:32,880 --> 00:00:39,680
 Sending love to yourself is

12
00:00:39,680 --> 00:00:44,790
 Like trying to shoot yourself no no that works actually you

13
00:00:44,790 --> 00:00:46,280
 can shoot yourself, but

14
00:00:46,280 --> 00:00:50,200
 It's like trying to see your your own eyes

15
00:00:50,200 --> 00:00:54,000
 You know because you're doing the sending

16
00:00:54,800 --> 00:00:57,620
 And people talk about it. It's certainly recommended by

17
00:00:57,620 --> 00:01:01,020
 many teachers to wish for you and everyone recommends it

18
00:01:01,020 --> 00:01:03,580
 But the texts say it's just an example

19
00:01:03,580 --> 00:01:06,880
 Yeah, and it doesn't really do that much

20
00:01:06,880 --> 00:01:10,240
 But sure it's certainly something

21
00:01:10,240 --> 00:01:16,000
 Worth doing I mean the reason I hesitate about saying yes

22
00:01:16,000 --> 00:01:19,350
 is that you know there's a potential for it to become sort

23
00:01:19,350 --> 00:01:19,520
 of

24
00:01:19,520 --> 00:01:21,520
 a

25
00:01:21,520 --> 00:01:30,800
 Egotistical right you know the the whole idea of self. It's

26
00:01:30,800 --> 00:01:32,320
 already a problem

27
00:01:32,320 --> 00:01:39,360
 Self hatred is is very much tied to ego. It's

28
00:01:39,360 --> 00:01:46,140
 It's different from hatred of others in the sense that it's

29
00:01:46,140 --> 00:01:49,080
 egotistical you are

30
00:01:50,720 --> 00:01:53,200
 You know how we have expectations for ourselves

31
00:01:53,200 --> 00:01:56,440
 you know and

32
00:01:56,440 --> 00:01:59,960
 We

33
00:01:59,960 --> 00:02:02,720
 Feel

34
00:02:02,720 --> 00:02:07,160
 Guilty and we feel bad when we feel ashamed of ourselves

35
00:02:07,160 --> 00:02:10,400
 Shame you know this sense of

36
00:02:10,400 --> 00:02:13,000
 improper shame

37
00:02:13,000 --> 00:02:15,160
 There's proper shame

38
00:02:15,160 --> 00:02:17,600
 which

39
00:02:17,600 --> 00:02:20,580
 Recognizes when you do bad things, but there's this

40
00:02:20,580 --> 00:02:22,760
 improper sense of this sort of obsession

41
00:02:22,760 --> 00:02:25,520
 with

42
00:02:25,520 --> 00:02:27,520
 self hatred

43
00:02:27,520 --> 00:02:40,970
 I mean regardless whether you should send or shouldn't send

44
00:02:40,970 --> 00:02:43,600
 methi. It's not really the most important

45
00:02:45,160 --> 00:02:49,030
 Solution to the problem obviously insight meditation is the

46
00:02:49,030 --> 00:02:51,240
 greatest solution to the problem

47
00:02:51,240 --> 00:02:56,940
 So part of it is actually correct. I mean it's a correct

48
00:02:56,940 --> 00:03:00,250
 observation when you do something bad to say that was a bad

49
00:03:00,250 --> 00:03:00,680
 thing

50
00:03:00,680 --> 00:03:04,560
 when you give rise to unwholesome mind states, it's

51
00:03:04,560 --> 00:03:08,090
 Good to say you know that was unwholesome mind state of

52
00:03:08,090 --> 00:03:10,440
 course. It's it's a bigger problem when you

53
00:03:10,440 --> 00:03:14,600
 Say I'm ugly or I'm fat

54
00:03:14,840 --> 00:03:16,840
 or I'm

55
00:03:16,840 --> 00:03:19,990
 Stupid even I'm stupid because intelligence isn't me

56
00:03:19,990 --> 00:03:23,350
 because none of these things including intelligence are

57
00:03:23,350 --> 00:03:24,040
 essential

58
00:03:24,040 --> 00:03:28,160
 You know being fat isn't isn't evil. No

59
00:03:28,160 --> 00:03:31,040
 Being ugly isn't evil

60
00:03:31,040 --> 00:03:35,760
 Being bald isn't evil

61
00:03:35,760 --> 00:03:42,200
 So that kind of self hatred is is is worse that's damaging

62
00:03:43,360 --> 00:03:46,320
 that's the kind of thing where and you really have to

63
00:03:46,320 --> 00:03:49,520
 put aside but

64
00:03:49,520 --> 00:03:54,320
 And knowing when you've done something wrong is important

65
00:03:54,320 --> 00:03:56,600
 because there are other people on the other side who

66
00:03:56,600 --> 00:03:59,670
 Don't see when they do something wrong and have self-

67
00:03:59,670 --> 00:04:00,440
righteousness

68
00:04:00,440 --> 00:04:03,880
 When anyone tries to criticize them?

69
00:04:03,880 --> 00:04:08,520
 So an honest person it's actually a sign of

70
00:04:10,320 --> 00:04:13,640
 Desire to develop to know that you have problems the Buddha

71
00:04:13,640 --> 00:04:14,000
 said

72
00:04:14,000 --> 00:04:18,190
 If a fool knows that they are a fool to that extent you can

73
00:04:18,190 --> 00:04:19,120
 call them wise

74
00:04:19,120 --> 00:04:24,470
 Right, but a fool who thinks they're wise that's the real

75
00:04:24,470 --> 00:04:24,880
 fool

76
00:04:24,880 --> 00:04:30,400
 So so there's definitely a benefit there to seeing

77
00:04:30,400 --> 00:04:32,520
 the

78
00:04:32,520 --> 00:04:34,520
 your negative

79
00:04:34,520 --> 00:04:36,160
 qualities

80
00:04:36,160 --> 00:04:38,600
 certainly never see that and that's why I hesitate because

81
00:04:39,680 --> 00:04:42,100
 It's not it's not always the case that you should get rid

82
00:04:42,100 --> 00:04:44,800
 of every aspect of this and think yours and all these

83
00:04:44,800 --> 00:04:46,080
 teachings were you should

84
00:04:46,080 --> 00:04:49,150
 Think I am special and you know, I am wonderful and I am

85
00:04:49,150 --> 00:04:50,640
 perfect just the way I am

86
00:04:50,640 --> 00:04:53,760
 It's all a bunch of rubbish. It's certainly not Buddhist

87
00:04:53,760 --> 00:04:58,680
 Buddhism you're useless here. You're yeah, I just told my

88
00:04:58,680 --> 00:05:00,880
 meditator that I had a long conversation

89
00:05:00,880 --> 00:05:03,990
 And at one point I said, you know a lot of this is just

90
00:05:03,990 --> 00:05:07,040
 written just coming to the realization that you're useless

91
00:05:07,040 --> 00:05:10,280
 or I didn't say it was there was some context and I said

92
00:05:10,280 --> 00:05:13,830
 eventually you just realized that you're useless and and

93
00:05:13,830 --> 00:05:14,280
 and

94
00:05:14,280 --> 00:05:17,360
 hopeless

95
00:05:17,360 --> 00:05:24,050
 Because that's really it we're actually going that far to

96
00:05:24,050 --> 00:05:26,400
 give up this this body and this mind

97
00:05:26,400 --> 00:05:31,280
 We're using my teacher said the the mind

98
00:05:31,280 --> 00:05:35,040
 Discards the mind

99
00:05:37,000 --> 00:05:41,360
 You think it mind throws out the mind

100
00:05:41,360 --> 00:05:50,900
 So don't don't discard that part of the teaching

101
00:05:50,900 --> 00:05:54,660
 Don't don't just think well, I it's wrong if I criticize

102
00:05:54,660 --> 00:05:56,560
 myself. That's not wrong

103
00:05:56,560 --> 00:06:01,200
 Again, wrong is the reaction to that. How do you react? And

104
00:06:01,880 --> 00:06:05,630
 It's really just a cop out to feel guilty to hate yourself.

105
00:06:05,630 --> 00:06:07,540
 It's just the easy way out

106
00:06:07,540 --> 00:06:10,730
 You know, it's like the easy way out with anything when a

107
00:06:10,730 --> 00:06:11,280
 baby cries

108
00:06:11,280 --> 00:06:14,360
 The easy way out is to shake the baby to yell it, you know

109
00:06:14,360 --> 00:06:16,600
 to throw the baby out the window. I mean

110
00:06:16,600 --> 00:06:20,110
 That's a horrible thing to say but but these thoughts arise

111
00:06:20,110 --> 00:06:23,080
 in the parent they get so frustrated. They just

112
00:06:23,080 --> 00:06:27,600
 They start becoming

113
00:06:27,600 --> 00:06:30,800
 going crazy as there's on suppose parents ever

114
00:06:31,440 --> 00:06:33,930
 Get to the point where they want to strangle their kids,

115
00:06:33,930 --> 00:06:36,780
 but they shake them, you know parents want to shake their

116
00:06:36,780 --> 00:06:37,400
 kids sometimes

117
00:06:37,400 --> 00:06:41,750
 Maybe not babies, but kids, you know hit their kids, of

118
00:06:41,750 --> 00:06:42,300
 course

119
00:06:42,300 --> 00:06:48,540
 Why am I getting these loud noises

120
00:06:48,540 --> 00:06:52,760
 Don't send me messages, please

121
00:06:52,760 --> 00:06:56,500
 Stefano if you want to send the message send it anyway, sir

122
00:06:56,500 --> 00:06:57,940
. We're live here

123
00:06:59,760 --> 00:07:01,760
 There's a chat box you can use

124
00:07:01,760 --> 00:07:12,720
 Right, so we take the easy way out you

125
00:07:12,720 --> 00:07:17,120
 You hit you fight you yell you scream

126
00:07:17,120 --> 00:07:21,360
 And that's all we're doing when we feel guilty when we feel

127
00:07:21,360 --> 00:07:24,200
 self-hatred and so on

128
00:07:24,200 --> 00:07:29,360
 We're taking the word we're taking a cop more copping out

129
00:07:29,920 --> 00:07:32,090
 Again, it comes back to this. How do you react to the

130
00:07:32,090 --> 00:07:32,920
 situation?

131
00:07:32,920 --> 00:07:36,830
 You realize that was wrong and what is the reaction? Are

132
00:07:36,830 --> 00:07:39,480
 you gonna get angry or frustrated?

133
00:07:39,480 --> 00:07:44,460
 These are useless. So the point is actually the that hatred

134
00:07:44,460 --> 00:07:45,220
 is wrong

135
00:07:45,220 --> 00:07:48,640
 The fact that it's directed to yourself is not an issue

136
00:07:48,640 --> 00:07:51,280
 you know that the

137
00:07:51,280 --> 00:07:54,200
 The

138
00:07:54,200 --> 00:07:58,360
 Issue is that you react angrily towards that

139
00:07:58,360 --> 00:08:01,590
 Which again, it just comes back down to any anger any sort

140
00:08:01,590 --> 00:08:03,920
 of anger, of course sending love helps

141
00:08:03,920 --> 00:08:07,570
 Doing good deeds can help because it can help with the

142
00:08:07,570 --> 00:08:10,920
 guilds, right? This is what the Buddha said

143
00:08:10,920 --> 00:08:15,560
 Morality

144
00:08:15,560 --> 00:08:18,940
 Specifically morality morality makes you feel less guilty.

145
00:08:18,940 --> 00:08:20,560
 So try not to do bad things

146
00:08:21,320 --> 00:08:25,080
 But doing good things makes you feel good about yourself as

147
00:08:25,080 --> 00:08:25,440
 well

148
00:08:25,440 --> 00:08:28,020
 You know if you do lots of good deeds, you'll feel better

149
00:08:28,020 --> 00:08:30,760
 about yourself. You feel like yeah, I'm a good person

150
00:08:30,760 --> 00:08:38,440
 So that is another point especially morality

151
00:08:38,440 --> 00:08:41,960
 And stop doing things that make you feel make you hate

152
00:08:41,960 --> 00:08:42,640
 yourself

153
00:08:42,640 --> 00:08:46,560
 to some extent, but I think a lot more of it is just about

154
00:08:46,560 --> 00:08:48,040
 hating things that

155
00:08:50,160 --> 00:08:53,480
 That you can't easily change and so you should you have to

156
00:08:53,480 --> 00:08:54,680
 stop with the anger

157
00:08:54,680 --> 00:08:58,200
 And you have to accept

158
00:08:58,200 --> 00:09:02,210
 Not that aspect of yourself, but you have to accept that

159
00:09:02,210 --> 00:09:03,480
 that's

160
00:09:03,480 --> 00:09:06,680
 You know the reality of it you have to accept it as reality

161
00:09:06,680 --> 00:09:09,800
 Because if you can't accept it, you're going to react

162
00:09:09,800 --> 00:09:13,260
 negatively towards it all of our bad qualities. They are

163
00:09:13,260 --> 00:09:14,880
 bad. No, no question

164
00:09:15,520 --> 00:09:18,940
 I mean there is some question but quite often they're

165
00:09:18,940 --> 00:09:20,680
 really bad bad qualities

166
00:09:20,680 --> 00:09:24,760
 But

167
00:09:24,760 --> 00:09:27,400
 Reacting to them doesn't solve them

168
00:09:27,400 --> 00:09:32,280
 The answer is not in getting angry

169
00:09:32,280 --> 00:09:36,320
 Anyway

