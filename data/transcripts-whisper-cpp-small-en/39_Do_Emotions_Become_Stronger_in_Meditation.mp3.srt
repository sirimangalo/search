1
00:00:00,000 --> 00:00:03,760
 Do emotions become stronger in meditation?

2
00:00:03,760 --> 00:00:05,320
 Question.

3
00:00:05,320 --> 00:00:09,100
 When wanting arises when I meditate, I start meditating on

4
00:00:09,100 --> 00:00:12,040
 the wanting by saying "wanting,

5
00:00:12,040 --> 00:00:16,160
 wanting" but the wanting becomes stronger.

6
00:00:16,160 --> 00:00:19,640
 It seems like I am only suppressing the wanting rather than

7
00:00:19,640 --> 00:00:21,640
 getting rid of it completely.

8
00:00:21,640 --> 00:00:24,360
 How do I overcome this?

9
00:00:24,360 --> 00:00:27,240
 Answer.

10
00:00:27,240 --> 00:00:30,590
 What you are experiencing is actually a good thing because

11
00:00:30,590 --> 00:00:32,080
 you have stopped suppressing

12
00:00:32,080 --> 00:00:35,140
 or avoiding your emotions.

13
00:00:35,140 --> 00:00:38,660
 That is why it presents itself so strongly.

14
00:00:38,660 --> 00:00:42,500
 It is not as though meditation is cultivating desire, but

15
00:00:42,500 --> 00:00:44,380
 you have a propensity to give

16
00:00:44,380 --> 00:00:48,770
 rise to massive amounts of desire that meditation makes you

17
00:00:48,770 --> 00:00:49,520
 aware of.

18
00:00:49,520 --> 00:00:53,150
 Normally, rather than dealing objectively with our habits

19
00:00:53,150 --> 00:00:54,880
 of desire and aversion, we

20
00:00:54,880 --> 00:00:58,860
 react to them, trying to appease or fix them, and that is

21
00:00:58,860 --> 00:01:00,640
 why we do not experience them

22
00:01:00,640 --> 00:01:03,200
 so strongly.

23
00:01:03,200 --> 00:01:07,790
 It is like taking your garbage and throwing it in your

24
00:01:07,790 --> 00:01:08,140
 closet.

25
00:01:08,140 --> 00:01:11,110
 Every time you want to dispose of your garbage, you just

26
00:01:11,110 --> 00:01:12,640
 throw it into your closet.

27
00:01:12,640 --> 00:01:13,960
 Well guess what?

28
00:01:13,960 --> 00:01:17,320
 You end up with a closet full of garbage, and you just keep

29
00:01:17,320 --> 00:01:19,040
 ignoring the problem rather

30
00:01:19,040 --> 00:01:21,960
 than dealing with it.

31
00:01:21,960 --> 00:01:25,610
 Meditation is like opening the closet and having it all

32
00:01:25,610 --> 00:01:26,560
 fall down on you.

33
00:01:26,560 --> 00:01:29,310
 Your immediate perception is that it was wrong to open the

34
00:01:29,310 --> 00:01:30,860
 door, that there is something

35
00:01:30,860 --> 00:01:34,430
 wrong with all the meditation practice as it brings out all

36
00:01:34,430 --> 00:01:36,120
 sorts of mental garbage,

37
00:01:36,120 --> 00:01:37,720
 so you have a choice.

38
00:01:37,720 --> 00:01:41,140
 You can keep it in the closet and keep throwing more in, or

39
00:01:41,140 --> 00:01:43,100
 you can clean out the closet and

40
00:01:43,100 --> 00:01:46,840
 deal with the garbage directly when it comes to you.

41
00:01:46,840 --> 00:01:49,990
 In that sense, there is nothing really wrong with the

42
00:01:49,990 --> 00:01:52,200
 experiences that you are having.

43
00:01:52,200 --> 00:01:56,360
 Another thing to note is that the desire itself is not

44
00:01:56,360 --> 00:02:00,040
 likely what you experience as increasing.

45
00:02:00,040 --> 00:02:03,460
 What you are experiencing are the physical manifestations

46
00:02:03,460 --> 00:02:05,120
 of the desire in the body.

47
00:02:05,120 --> 00:02:08,680
 A good example is with lust.

48
00:02:08,680 --> 00:02:12,960
 99% of it is not desire.

49
00:02:12,960 --> 00:02:16,530
 The chemical reactions, the bodily changes, the hormones

50
00:02:16,530 --> 00:02:19,040
 and so on are all physical manifestations

51
00:02:19,040 --> 00:02:20,980
 of desire.

52
00:02:20,980 --> 00:02:23,520
 This is why it is so important to be able to break up

53
00:02:23,520 --> 00:02:25,480
 experiences into their component

54
00:02:25,480 --> 00:02:27,460
 parts.

55
00:02:27,460 --> 00:02:30,610
 When you meditate on desire, you are allowing your body to

56
00:02:30,610 --> 00:02:32,280
 give rise to these states that

57
00:02:32,280 --> 00:02:35,360
 normally you would suppress.

58
00:02:35,360 --> 00:02:37,770
 Normally you would create a great tension in the body that

59
00:02:37,770 --> 00:02:38,960
 would suppress many of the

60
00:02:38,960 --> 00:02:42,960
 systems that are now given the chance to release.

61
00:02:42,960 --> 00:02:46,530
 The chemicals will arise along with the physical pleasure,

62
00:02:46,530 --> 00:02:48,520
 and then the desire arises for more

63
00:02:48,520 --> 00:02:50,360
 such pleasure.

64
00:02:50,360 --> 00:02:53,020
 It can be that when you first start to meditate, the

65
00:02:53,020 --> 00:02:55,200
 delusion and guilt is preventing you from

66
00:02:55,200 --> 00:02:58,990
 seeing this, but when you meditate regularly, you will see

67
00:02:58,990 --> 00:03:01,160
 that desire actually disappears

68
00:03:01,160 --> 00:03:03,390
 and only the physical pleasure that would normally have

69
00:03:03,390 --> 00:03:04,760
 been associated with the desire

70
00:03:04,760 --> 00:03:07,960
 persists.

71
00:03:07,960 --> 00:03:13,130
 Because of noting to yourself, wanting, wanting, as well as

72
00:03:13,130 --> 00:03:16,680
 happy, happy, or pleasure, pleasure,

73
00:03:16,680 --> 00:03:20,530
 or even feeling, feeling, you experience bodily relief

74
00:03:20,530 --> 00:03:23,240
 because there is less tension due to

75
00:03:23,240 --> 00:03:29,280
 less desire, less guilt, and less anxiety, etc.

76
00:03:29,280 --> 00:03:31,680
 A good way of understanding this is that you have

77
00:03:31,680 --> 00:03:34,000
 previously been suppressing or restricting

78
00:03:34,000 --> 00:03:39,670
 the flow of bodily systems, and that is what is now coming

79
00:03:39,670 --> 00:03:40,080
 out.

80
00:03:40,080 --> 00:03:43,560
 Desire is the moment when you like something.

81
00:03:43,560 --> 00:03:48,720
 That is only a very small part, the mental part of the

82
00:03:48,720 --> 00:03:49,080
 experience.

83
00:03:49,080 --> 00:03:52,990
 Desire is where the mind cultivates partiality for an

84
00:03:52,990 --> 00:03:56,160
 experience, deciding that it is appealing.

85
00:03:56,160 --> 00:04:00,840
 It is where the mind categorizes experiences as positive.

86
00:04:00,840 --> 00:04:04,730
 It is important to focus on the desire objectively, rather

87
00:04:04,730 --> 00:04:06,660
 than trying to remove it.

88
00:04:06,660 --> 00:04:10,380
 You must also be mindful of the pleasurable experiences in

89
00:04:10,380 --> 00:04:11,200
 the body.

90
00:04:11,200 --> 00:04:15,190
 Furthermore, when the wanting seems to become stronger, it

91
00:04:15,190 --> 00:04:17,040
 can also be that you just see

92
00:04:17,040 --> 00:04:18,600
 it more clearly.

93
00:04:18,600 --> 00:04:21,800
 You begin to understand that it has been there all along,

94
00:04:21,800 --> 00:04:23,400
 but you did not see it so clearly

95
00:04:23,400 --> 00:04:25,680
 before.

96
00:04:25,680 --> 00:04:30,690
 In meditation, we face things that we would normally avoid,

97
00:04:30,690 --> 00:04:32,900
 ignore, or react to.

98
00:04:32,900 --> 00:04:36,270
 This is a very clear point that many people miss about the

99
00:04:36,270 --> 00:04:37,760
 Buddha's teachings.

100
00:04:37,760 --> 00:04:42,290
 If you read the Satipatthana Sutra, the Buddha says, "When

101
00:04:42,290 --> 00:04:44,080
 you have central desire, know

102
00:04:44,080 --> 00:04:47,400
 in yourself, I have central desire.

103
00:04:47,400 --> 00:04:50,600
 There is central desire in the mind."

104
00:04:50,600 --> 00:04:54,900
 When central desire is not present, know in yourself,

105
00:04:54,900 --> 00:04:57,560
 central desire is not present.

106
00:04:57,560 --> 00:04:59,720
 This is very important.

107
00:04:59,720 --> 00:05:03,820
 This creates the equanimity that we are talking about.

108
00:05:03,820 --> 00:05:06,800
 It allows you to see the progression of states.

109
00:05:06,800 --> 00:05:10,400
 It allows you to see how greed works, and anger, and so on,

110
00:05:10,400 --> 00:05:11,960
 and how the mind works in

111
00:05:11,960 --> 00:05:12,960
 general.

112
00:05:12,960 --> 00:05:16,580
 It may not be that central desire is becoming stronger, but

113
00:05:16,580 --> 00:05:18,240
 that you are now more aware

114
00:05:18,240 --> 00:05:21,290
 of it, and you are also admitting it to yourself, where

115
00:05:21,290 --> 00:05:23,360
 previously you may have avoided admitting

116
00:05:23,360 --> 00:05:26,460
 it out of guilt or aversion.

117
00:05:26,460 --> 00:05:31,550
 We might say, "I'm not a greedy person," or, "I'm not an

118
00:05:31,550 --> 00:05:33,320
 angry person."

119
00:05:33,320 --> 00:05:36,140
 But that is because we suppress it all, and then in

120
00:05:36,140 --> 00:05:38,280
 meditation we see the anger or greed

121
00:05:38,280 --> 00:05:41,240
 arise and we blame it arising on the meditation.

122
00:05:41,240 --> 00:06:07,820
 [

