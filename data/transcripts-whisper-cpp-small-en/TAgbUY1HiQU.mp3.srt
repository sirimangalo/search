1
00:00:00,000 --> 00:00:09,920
 Good evening everyone.

2
00:00:09,920 --> 00:00:22,880
 Broadcasting Live, April 27th, 2016.

3
00:00:22,880 --> 00:00:30,820
 It's a quote from the Sangeeta Nikaya. It's again not the

4
00:00:30,820 --> 00:00:31,680
 Buddha himself.

5
00:00:31,680 --> 00:00:39,000
 This is a man named Jitta.

6
00:00:39,000 --> 00:00:45,800
 Jitta was a rather remarkable person, not a monk.

7
00:00:45,800 --> 00:00:55,950
 He was a rich man and he was declared by the Buddha to be

8
00:00:55,950 --> 00:01:03,280
 preeminent among laymen who preached the Dhamma.

9
00:01:03,280 --> 00:01:09,120
 And so there's a whole section of the Jitta Sangeeta.

10
00:01:09,120 --> 00:01:15,120
 It is a record of conversations between him and monks.

11
00:01:15,120 --> 00:01:26,120
 So he actually cleared up some debates between the monks

12
00:01:26,120 --> 00:01:30,760
 and that's what this quote doesn't actually show.

13
00:01:30,760 --> 00:01:41,210
 So the quote is giving an example and it sounds like he's

14
00:01:41,210 --> 00:01:44,440
 repeating something that the monks already know.

15
00:01:44,440 --> 00:01:49,460
 But the truth is that the monks were divided, these elder

16
00:01:49,460 --> 00:01:51,080
 monks in fact.

17
00:01:51,080 --> 00:01:56,700
 And the question is actually an interesting question for

18
00:01:56,700 --> 00:01:58,280
 meditators.

19
00:01:58,280 --> 00:02:06,840
 The question is whether, let me read the Pali.

20
00:02:06,840 --> 00:02:15,240
 "Sangeo jananti wa auso sangeo janiya damma tiwa."

21
00:02:15,240 --> 00:02:22,920
 Sangeo janna, a fetter or a bond or a bind.

22
00:02:22,920 --> 00:02:31,720
 And the dhammas which are bound.

23
00:02:31,720 --> 00:02:40,120
 The dhammas which are conjoined.

24
00:02:40,120 --> 00:02:45,240
 Are these two things, are these two concepts?

25
00:02:45,240 --> 00:03:00,800
 "Imidhamma na nata na nam bhiyajna udahu ikata bhiyajna mih

26
00:03:00,800 --> 00:03:01,240
wa."

27
00:03:01,240 --> 00:03:12,720
 Are they one in, are they separate and different in meaning

28
00:03:12,720 --> 00:03:16,920
 and in letter, in name?

29
00:03:16,920 --> 00:03:26,370
 Or are they one in meaning and different in letter or by

30
00:03:26,370 --> 00:03:27,880
 name?

31
00:03:27,880 --> 00:03:34,580
 So the meaning is you've got the bond, the fetter, that

32
00:03:34,580 --> 00:03:38,040
 which binds things.

33
00:03:38,040 --> 00:03:41,800
 And then you've got the things that are bound together.

34
00:03:41,800 --> 00:03:44,520
 And the question is, it's quite a deep question actually.

35
00:03:44,520 --> 00:03:47,800
 The question is whether these are one in the same and just

36
00:03:47,800 --> 00:03:48,760
 different in name.

37
00:03:48,760 --> 00:03:51,730
 And some of the elders thought that the things that were

38
00:03:51,730 --> 00:03:52,600
 bound together,

39
00:03:52,600 --> 00:03:56,840
 the things that are bound together are actually the bind.

40
00:03:56,840 --> 00:04:01,240
 And some thought they were separate.

41
00:04:01,240 --> 00:04:03,400
 And then Jitta heard about this.

42
00:04:03,400 --> 00:04:05,480
 And so he went to see the monks.

43
00:04:05,480 --> 00:04:12,070
 He said, "Sutta metang bande, I've heard that many monks

44
00:04:12,070 --> 00:04:16,200
 are arguing about this

45
00:04:16,200 --> 00:04:19,800
 or split on this matter."

46
00:04:19,800 --> 00:04:24,200
 And he says, they say, "Eywang gahapati."

47
00:04:24,200 --> 00:04:29,640
 Yes, householder, it is so.

48
00:04:29,640 --> 00:04:32,200
 And Jitta says that they're separate.

49
00:04:32,200 --> 00:04:37,560
 He says, "Nana ta jiwa na nami anjana."

50
00:04:37,560 --> 00:04:43,000
 They're different in letter and different in meaning.

51
00:04:43,000 --> 00:04:50,890
 And then he gives this simile of a black cow and a white

52
00:04:50,890 --> 00:04:51,080
 cow.

53
00:04:51,080 --> 00:04:54,520
 And they're tied together by a rope.

54
00:04:54,520 --> 00:05:01,610
 And when the white cow pulls, the black cow has to go as

55
00:05:01,610 --> 00:05:02,120
 well.

56
00:05:02,120 --> 00:05:05,480
 So the question is whether the black cow is fed or is

57
00:05:05,480 --> 00:05:06,680
 holding the white cow

58
00:05:06,680 --> 00:05:09,240
 or the white cow is holding the black cow.

59
00:05:09,240 --> 00:05:13,150
 And the monks say, "Well, no, the rope is holding them both

60
00:05:13,150 --> 00:05:13,560
."

61
00:05:13,560 --> 00:05:17,400
 And he says, "Just the same.

62
00:05:17,400 --> 00:05:21,480
 The eye is not a feather or lights.

63
00:05:21,480 --> 00:05:26,890
 Visions are not a feather for the eye, nor is the eye a

64
00:05:26,890 --> 00:05:29,640
 feather for visions."

65
00:05:29,640 --> 00:05:33,080
 So it's not because we see things.

66
00:05:33,080 --> 00:05:36,120
 Just seeing things is not a feather.

67
00:05:36,120 --> 00:05:40,040
 It's not getting caught up.

68
00:05:40,040 --> 00:05:42,600
 You don't get attached just because you've seen, basically.

69
00:05:42,600 --> 00:05:45,400
 I mean, this is very deep and sort of--

70
00:05:45,400 --> 00:05:48,280
 these are the kind of things that they would debate.

71
00:05:48,280 --> 00:05:52,520
 And it goes to show how deep was their thought process,

72
00:05:52,520 --> 00:05:56,360
 what they were thinking about.

73
00:05:56,360 --> 00:06:00,200
 And here, when you hear something, it's not the sound that

74
00:06:00,200 --> 00:06:03,640
 is the bond.

75
00:06:03,640 --> 00:06:06,840
 The clinging isn't in the sound.

76
00:06:06,840 --> 00:06:12,910
 When you smell, taste, feel, think, none of these are the

77
00:06:12,910 --> 00:06:13,480
 problem.

78
00:06:13,480 --> 00:06:17,350
 Very simply, it's basically what we talk about in

79
00:06:17,350 --> 00:06:18,520
 meditation.

80
00:06:18,520 --> 00:06:21,890
 Just because you experience something, this isn't the

81
00:06:21,890 --> 00:06:22,680
 problem.

82
00:06:22,680 --> 00:06:24,920
 And when I was giving five-minute meditation lessons,

83
00:06:24,920 --> 00:06:26,120
 this is how I started off.

84
00:06:26,120 --> 00:06:32,600
 I said, this meditation is based on the premise

85
00:06:32,600 --> 00:06:35,320
 that it's not our experiences that cause us suffering.

86
00:06:35,320 --> 00:06:38,200
 It's our reactions to them.

87
00:06:38,200 --> 00:06:39,360
 So it's the craving.

88
00:06:39,360 --> 00:06:52,040
 He says, [NON-ENGLISH SPEECH]

89
00:06:52,040 --> 00:06:57,280
 Whatever [NON-ENGLISH SPEECH]

90
00:06:57,280 --> 00:07:04,520
 appreciation or desire and lust arises based on them both,

91
00:07:04,520 --> 00:07:07,480
 based on the paradigm.

92
00:07:07,480 --> 00:07:12,360
 That's what binds them.

93
00:07:12,360 --> 00:07:14,040
 That's the bind in this case.

94
00:07:14,040 --> 00:07:27,300
 So even when you feel pain, it's not the pain that's the

95
00:07:27,300 --> 00:07:27,760
 problem.

96
00:07:27,760 --> 00:07:30,020
 When someone's yelling at you, it's not the yelling that's

97
00:07:30,020 --> 00:07:30,640
 the problem.

98
00:07:30,640 --> 00:07:33,600
 When you think bad thoughts, the thoughts are not bad.

99
00:07:33,600 --> 00:07:36,240
 They're just thoughts.

100
00:07:36,240 --> 00:07:38,320
 And in fact, quite neutral.

101
00:07:38,320 --> 00:07:41,360
 We once had a monk in Thailand.

102
00:07:41,360 --> 00:07:45,520
 Don't know if I've recently mentioned him, but I--

103
00:07:45,520 --> 00:07:46,480
 no, it was in New York.

104
00:07:46,480 --> 00:07:47,680
 I was just talking about him.

105
00:07:47,680 --> 00:07:53,480
 And he told me he was really crazy.

106
00:07:53,480 --> 00:08:01,400
 He thought we were all conspiring against him.

107
00:08:01,400 --> 00:08:03,760
 It was interesting.

108
00:08:03,760 --> 00:08:06,600
 And he came up to me and said--

109
00:08:06,600 --> 00:08:08,720
 they're talking about me over the loudspeakers.

110
00:08:08,720 --> 00:08:10,680
 They're saying things.

111
00:08:10,680 --> 00:08:14,440
 And I said, well, I really don't think--

112
00:08:14,440 --> 00:08:16,360
 and he said, oh, you're in with them.

113
00:08:16,360 --> 00:08:21,880
 He really thought we were all conspiring to drive him crazy

114
00:08:21,880 --> 00:08:22,760
 at the monastery.

115
00:08:22,760 --> 00:08:24,680
 It was quite interesting.

116
00:08:24,680 --> 00:08:26,160
 But he wants to send to me.

117
00:08:26,160 --> 00:08:27,880
 He said, I'm having these thoughts.

118
00:08:27,880 --> 00:08:29,560
 And I said, well, they're just thoughts.

119
00:08:29,560 --> 00:08:31,120
 And he said, oh, no.

120
00:08:31,120 --> 00:08:33,440
 They're just the most horrible thoughts

121
00:08:33,440 --> 00:08:36,160
 that you could possibly have.

122
00:08:36,160 --> 00:08:38,560
 And the end of thought is a thought.

123
00:08:38,560 --> 00:08:39,800
 And he drove himself crazy.

124
00:08:39,800 --> 00:08:42,880
 He ended up cutting his wrists and lighting himself on fire

125
00:08:42,880 --> 00:08:42,880
.

126
00:08:42,880 --> 00:08:45,840
 Did all sorts of crazy things.

127
00:08:45,840 --> 00:08:47,760
 Ended up disrobing.

128
00:08:47,760 --> 00:08:50,520
 But we do this.

129
00:08:50,520 --> 00:08:51,880
 We all do this to some extent.

130
00:08:51,880 --> 00:08:53,720
 We beat ourselves up over our thoughts.

131
00:08:53,720 --> 00:08:54,480
 Don't think that.

132
00:08:54,480 --> 00:08:56,200
 Can't think that.

133
00:08:56,200 --> 00:08:57,720
 I'm so evil for thinking that.

134
00:08:57,720 --> 00:08:58,880
 And actually, you're not.

135
00:08:58,880 --> 00:09:00,800
 The thoughts are just thoughts.

136
00:09:00,800 --> 00:09:04,080
 That's not where evil comes from.

137
00:09:04,080 --> 00:09:11,400
 So actually, Jitta was teaching the monks something here.

138
00:09:11,400 --> 00:09:14,680
 He was teaching them--

139
00:09:14,680 --> 00:09:17,040
 and so they don't say, oh, very good, very good.

140
00:09:17,040 --> 00:09:22,400
 Like patronizing, they say, wow, labhati gapati, sulladhan

141
00:09:22,400 --> 00:09:23,560
 te.

142
00:09:23,560 --> 00:09:28,320
 It is a great gain for you, householder,

143
00:09:28,320 --> 00:09:30,280
 that you have this deep--

144
00:09:30,280 --> 00:09:35,600
 that you have the eye of wisdom.

145
00:09:35,600 --> 00:09:50,600
 That by you, the eye of wisdom goes to the deep.

146
00:09:50,600 --> 00:09:57,280
 You have an eye of wisdom in the deep teaching of the

147
00:09:57,280 --> 00:09:57,880
 Buddha.

148
00:09:57,880 --> 00:10:00,480
 Deep words of the Buddha, something like that.

149
00:10:00,480 --> 00:10:09,400
 So yeah, the bondage.

150
00:10:09,400 --> 00:10:11,320
 What is it that binds things?

151
00:10:11,320 --> 00:10:18,360
 Then we start with ignorance.

152
00:10:18,360 --> 00:10:22,680
 The base of all attachment is ignorance.

153
00:10:22,680 --> 00:10:24,200
 It's just not seen clearly.

154
00:10:24,200 --> 00:10:27,160
 Ignorance isn't something hard to understand.

155
00:10:27,160 --> 00:10:29,360
 Just means you didn't see it clearly enough.

156
00:10:29,360 --> 00:10:37,080
 And that leads to misconception.

157
00:10:37,080 --> 00:10:40,960
 It leads us to like things that are not worth liking,

158
00:10:40,960 --> 00:10:46,360
 and it leads to habits of preference,

159
00:10:46,360 --> 00:10:48,720
 because we're just guessing.

160
00:10:48,720 --> 00:10:52,840
 When we grow up, we do this even in this life to some

161
00:10:52,840 --> 00:10:54,320
 extent.

162
00:10:54,320 --> 00:10:57,680
 When you ask a kid which one they want, they--

163
00:10:57,680 --> 00:11:01,320
 you know, in many cases, it's all the same.

164
00:11:01,320 --> 00:11:04,080
 And when you ask a meditator this,

165
00:11:04,080 --> 00:11:06,680
 it's hard to ask them, would you like this,

166
00:11:06,680 --> 00:11:07,680
 or would you like that?

167
00:11:07,680 --> 00:11:10,560
 They're not really able to make a decision,

168
00:11:10,560 --> 00:11:13,520
 because there's no reason to pick one over the other.

169
00:11:13,520 --> 00:11:16,000
 But we do.

170
00:11:16,000 --> 00:11:22,720
 We build up likes and dislikes, often just by chance,

171
00:11:22,720 --> 00:11:25,640
 just because of that's how the chips fall.

172
00:11:25,640 --> 00:11:28,720
 That's where our life leads us.

173
00:11:28,720 --> 00:11:32,680
 We acquire tastes.

174
00:11:32,680 --> 00:11:36,080
 Some of it's genetic, some of it's organic,

175
00:11:36,080 --> 00:11:37,560
 but some of it's just random.

176
00:11:37,560 --> 00:11:51,520
 And so then the ignorance is a breeding ground.

177
00:11:51,520 --> 00:11:54,360
 It's like darkness.

178
00:11:54,360 --> 00:12:00,960
 Ignorance is a direct parallel to the darkness.

179
00:12:00,960 --> 00:12:02,160
 It's mental darkness.

180
00:12:02,160 --> 00:12:07,600
 So if you think about darkness, not only

181
00:12:07,600 --> 00:12:10,080
 is it impossible to see what is right,

182
00:12:10,080 --> 00:12:15,080
 but it's a breeding ground of all sorts of things.

183
00:12:15,080 --> 00:12:18,080
 It's a breeding ground of bacteria.

184
00:12:18,080 --> 00:12:22,080
 It's a breeding ground of viruses and--

185
00:12:22,080 --> 00:12:25,720
 viruses, I don't know, bacteria, I guess, rotten things.

186
00:12:25,720 --> 00:12:27,920
 Things rot in the dark.

187
00:12:27,920 --> 00:12:29,080
 Mold grows.

188
00:12:29,080 --> 00:12:30,600
 Fungus grows in the dark.

189
00:12:30,600 --> 00:12:37,360
 And so all these rotten things in our minds

190
00:12:37,360 --> 00:12:39,320
 grow based on our ignorance.

191
00:12:39,320 --> 00:12:42,560
 And all it takes is to shine a light in.

192
00:12:42,560 --> 00:12:46,200
 When you shine a powerful light, and the darkness

193
00:12:46,200 --> 00:12:46,920
 disappears,

194
00:12:46,920 --> 00:12:49,280
 so you don't have to worry about getting rid of the

195
00:12:49,280 --> 00:12:51,480
 ignorance.

196
00:12:51,480 --> 00:12:55,560
 But the bad things start to shrivel up as well.

197
00:12:55,560 --> 00:13:02,560
 The rotten things start to dry up and wither away,

198
00:13:02,560 --> 00:13:04,680
 because they rely on the darkness for support.

199
00:13:04,680 --> 00:13:11,120
 So how do we shine this light in?

200
00:13:11,120 --> 00:13:14,720
 It's a little more complicated than just shining a light,

201
00:13:14,720 --> 00:13:16,440
 a little bit more complicated.

202
00:13:16,440 --> 00:13:19,240
 Because there's different aspects.

203
00:13:19,240 --> 00:13:21,520
 And I wrote-- I did a video, if you know my video,

204
00:13:21,520 --> 00:13:29,760
 on pornography and masturbation, I think,

205
00:13:29,760 --> 00:13:30,840
 and addiction in general.

206
00:13:30,840 --> 00:13:36,080
 It was that there are--

207
00:13:36,080 --> 00:13:42,680
 for a long time, I had this concept of three things,

208
00:13:42,680 --> 00:13:46,080
 three aspects to an addiction.

209
00:13:46,080 --> 00:13:53,600
 The object, which is either seeing, hearing, smelling,

210
00:13:53,600 --> 00:13:56,600
 tasting, feeling, or thinking.

211
00:13:56,600 --> 00:13:58,000
 A thought.

212
00:13:58,000 --> 00:14:01,480
 It can be something you see that leads to desire,

213
00:14:01,480 --> 00:14:03,680
 something you hear, something you smell, something you

214
00:14:03,680 --> 00:14:04,080
 taste,

215
00:14:04,080 --> 00:14:06,040
 something you feel, or something you think.

216
00:14:06,040 --> 00:14:10,200
 So that's the first thing.

217
00:14:10,200 --> 00:14:14,360
 The second thing is the pleasure that comes from it.

218
00:14:14,360 --> 00:14:16,000
 You could do the same with pain as well.

219
00:14:17,680 --> 00:14:19,880
 When you see something, and it makes you happy.

220
00:14:19,880 --> 00:14:22,600
 When you hear something, it makes you happy.

221
00:14:22,600 --> 00:14:23,960
 So there's that aspect of it.

222
00:14:23,960 --> 00:14:26,880
 And then the third is the desire,

223
00:14:26,880 --> 00:14:32,640
 which is not the feeling, but it's this attraction,

224
00:14:32,640 --> 00:14:40,080
 like a magnet, a clinging, like a pressure in the mind,

225
00:14:40,080 --> 00:14:40,720
 the stickiness.

226
00:14:40,720 --> 00:14:45,800
 And going back and forth between those three,

227
00:14:45,800 --> 00:14:48,560
 yeah, this is what I talked about in this video.

228
00:14:48,560 --> 00:14:50,480
 And I found this in the Buddha's teaching.

229
00:14:50,480 --> 00:14:53,040
 The Buddha said some teachers teach one or the other,

230
00:14:53,040 --> 00:14:58,000
 but the best teacher teaches all three.

231
00:14:58,000 --> 00:15:01,640
 Teaches the base, teaches the feeling,

232
00:15:01,640 --> 00:15:05,560
 and teaches the attachment.

233
00:15:05,560 --> 00:15:09,440
 So this is how we deal with addiction specifically.

234
00:15:09,440 --> 00:15:11,680
 If you want to deal with your attachments,

235
00:15:11,680 --> 00:15:13,520
 go back and forth between these three.

236
00:15:13,520 --> 00:15:21,280
 And every time it comes up, just be methodical, systematic,

237
00:15:21,280 --> 00:15:23,360
 and you'll start to change those habits.

238
00:15:23,360 --> 00:15:26,520
 You'll start to override this.

239
00:15:26,520 --> 00:15:29,720
 The thing is, it's because you'll see that, oh, yeah, well,

240
00:15:29,720 --> 00:15:32,920
 there really isn't anything desirable about that at all.

241
00:15:32,920 --> 00:15:34,880
 Eventually, once your wisdom gets stronger,

242
00:15:34,880 --> 00:15:37,960
 you just don't have any desire for the things

243
00:15:37,960 --> 00:15:39,440
 you used to desire.

244
00:15:39,440 --> 00:15:41,200
 You look at it, and you rightly see

245
00:15:41,200 --> 00:15:42,920
 that it's not worth desiring.

246
00:15:42,920 --> 00:15:54,080
 So that's the demo for tonight.

247
00:15:54,080 --> 00:15:55,200
 Do we have any questions?

248
00:15:55,200 --> 00:16:00,760
 You guys can go ahead and--

249
00:16:00,760 --> 00:16:09,000
 is there anything you can say about mindfulness

250
00:16:09,000 --> 00:16:11,640
 or falling asleep?

251
00:16:11,640 --> 00:16:12,160
 Yeah.

252
00:16:12,160 --> 00:16:18,960
 Similar experience to dying, but find it very difficult.

253
00:16:18,960 --> 00:16:19,800
 You can try.

254
00:16:19,800 --> 00:16:24,200
 You try to be mindful up until the moment you fall asleep.

255
00:16:24,200 --> 00:16:27,080
 If you're really good, you can know

256
00:16:27,080 --> 00:16:30,800
 whether you fell asleep on the rising or the falling.

257
00:16:30,800 --> 00:16:36,800
 But you just be mindful until you fall asleep.

258
00:16:36,800 --> 00:16:39,880
 It's not something you should worry about or strive for.

259
00:16:39,880 --> 00:16:41,680
 You just work at it.

260
00:16:41,680 --> 00:16:43,800
 When you lie down at night, you'll

261
00:16:43,800 --> 00:16:45,520
 try to take it as lying meditation.

262
00:16:45,520 --> 00:16:48,040
 Don't just fall asleep.

263
00:16:48,040 --> 00:16:50,840
 But it works better when you're on a meditation course.

264
00:16:50,840 --> 00:16:55,120
 In life, of course, it's very difficult to be that mindful.

265
00:16:55,120 --> 00:16:58,480
 [MUSIC PLAYING]

266
00:16:58,480 --> 00:17:01,960
 [MUSIC PLAYING]

267
00:17:01,960 --> 00:17:05,440
 [MUSIC PLAYING]

268
00:17:05,440 --> 00:17:08,920
 [MUSIC PLAYING]

269
00:17:08,920 --> 00:17:12,400
 [MUSIC PLAYING]

270
00:17:12,400 --> 00:17:15,880
 [MUSIC PLAYING]

271
00:17:15,880 --> 00:17:19,360
 [MUSIC PLAYING]

272
00:17:19,360 --> 00:17:22,840
 [MUSIC PLAYING]

273
00:17:47,320 --> 00:17:50,120
 Any other questions?

274
00:17:50,120 --> 00:18:00,560
 I think there's a bit of a lag between what I say

275
00:18:00,560 --> 00:18:04,500
 and what comes up in the chat.

276
00:18:04,500 --> 00:18:07,640
 Anyway, if you have questions, I'll

277
00:18:07,640 --> 00:18:09,200
 come to post them tomorrow.

278
00:18:09,200 --> 00:18:10,040
 Should be back.

279
00:18:10,040 --> 00:18:15,440
 So have a good night, everyone.

280
00:18:15,440 --> 00:18:18,400
 [MUSIC PLAYING]

