WEBVTT

00:00:00.000 --> 00:00:07.000
 Okay, good morning.

00:00:07.000 --> 00:00:31.000
 I'm broadcasting from Chiang Mai.

00:00:31.000 --> 00:00:36.100
 I put great emphasis on the four foundations of mindfulness

00:00:36.100 --> 00:00:42.000
, the four satipatthana.

00:00:42.000 --> 00:00:48.020
 We, including the Buddha, put great emphasis on the four

00:00:48.020 --> 00:00:50.000
 satipatthana.

00:00:50.000 --> 00:01:01.370
 The Buddha was clearly inclined to present mindfulness as

00:01:01.370 --> 00:01:04.000
 core,

00:01:04.000 --> 00:01:16.330
 as the essence of the practice of the path leading to

00:01:16.330 --> 00:01:21.000
 enlightenment.

00:01:21.000 --> 00:01:39.000
 The commentaries say that mindfulness is like,

00:01:39.000 --> 00:01:49.000
 mindfulness is like the gate to a city.

00:01:49.000 --> 00:01:56.000
 The city is nirvana, nirvana.

00:01:56.000 --> 00:02:01.590
 The gate itself is the eightfold noble path, but there are

00:02:01.590 --> 00:02:03.000
 four gates,

00:02:03.000 --> 00:02:09.000
 one from each direction.

00:02:09.000 --> 00:02:16.520
 So anybody who wants to enter into the city has to go

00:02:16.520 --> 00:02:21.000
 through one of the four gates.

00:02:21.000 --> 00:02:31.000
 Just like here in Chiang Mai, they have the four gates.

00:02:31.000 --> 00:02:50.010
 The only way to get in in the olden days was through one of

00:02:50.010 --> 00:03:00.000
 the four gates of the city.

00:03:00.000 --> 00:03:14.990
 And this is worth remembering specifically in regards to

00:03:14.990 --> 00:03:20.000
 our individual sense of

00:03:20.000 --> 00:03:26.000
 the type of meditation that is right for us.

00:03:26.000 --> 00:03:35.460
 Some people will insist that meditation on the four

00:03:35.460 --> 00:03:41.000
 foundations of mindfulness just isn't for them.

00:03:41.000 --> 00:03:57.490
 In Thai they say, "mai to jalit," which is just not right

00:03:57.490 --> 00:03:58.000
 for me,

00:03:58.000 --> 00:04:15.500
 not right for my charita. Charita means personality or

00:04:15.500 --> 00:04:21.000
 character type.

00:04:21.000 --> 00:04:26.720
 Because ordinarily we can get away with this in ordinary

00:04:26.720 --> 00:04:28.000
 meditation,

00:04:28.000 --> 00:04:38.000
 the practice of tranquility meditation we get away with.

00:04:38.000 --> 00:04:42.000
 Picking and choosing, we have to pick and choose.

00:04:42.000 --> 00:04:45.570
 So we're used to picking and choosing our meditations based

00:04:45.570 --> 00:04:48.000
 on our character type.

00:04:48.000 --> 00:04:51.980
 For example, if you're an angry sort of person, you would

00:04:51.980 --> 00:04:59.000
 want to focus on loving kindness, metta meditation.

00:04:59.000 --> 00:05:03.000
 If you're a lustful sort of person, you'd want to focus on

00:05:03.000 --> 00:05:11.000
 mindfulness of the foulness of the body, for example.

00:05:11.000 --> 00:05:13.000
 But you wouldn't do vice versa.

00:05:13.000 --> 00:05:17.130
 If you're an angry person, it's not good to do mindfulness

00:05:17.130 --> 00:05:19.000
 of the foulness of the body.

00:05:19.000 --> 00:05:23.000
 It just makes you more upset and more angry.

00:05:23.000 --> 00:05:28.280
 And if you're quite lustful, you have to be careful about

00:05:28.280 --> 00:05:30.000
 metta meditation

00:05:30.000 --> 00:05:38.960
 because it can easily lead to lust, to the kind of love

00:05:38.960 --> 00:05:48.000
 that is eros, that is passionate, that is clingy.

00:05:48.000 --> 00:05:54.770
 And so in the commentaries they recognize six character

00:05:54.770 --> 00:05:56.000
 types.

00:05:56.000 --> 00:05:59.430
 I'm not sure if it's actually something that the Buddha

00:05:59.430 --> 00:06:02.000
 himself, I think it's in the Abhidhamma,

00:06:02.000 --> 00:06:07.400
 I know it's in the, it's got to be in the Patti Sambhida

00:06:07.400 --> 00:06:15.000
 Maga, but it's actually in the Tipitaka or not.

00:06:15.000 --> 00:06:22.000
 The tradition recognizes six character types.

00:06:22.000 --> 00:06:31.590
 There's Raga Charita, Dosa Charita, Moha Charita, I think

00:06:31.590 --> 00:06:37.000
 Moha Charita means Raga Charita.

00:06:37.000 --> 00:06:46.240
 Mita Aka Charita, someone who thinks a lot, Puddika Charita

00:06:46.240 --> 00:06:51.000
, someone who is more intelligent,

00:06:51.000 --> 00:07:02.370
 and Sadha Charita, someone who is confident, faithful,

00:07:02.370 --> 00:07:06.000
 easily believe, inclined to believe,

00:07:06.000 --> 00:07:09.000
 six different Charitas.

00:07:09.000 --> 00:07:12.100
 And so certain meditations are only appropriate for certain

00:07:12.100 --> 00:07:13.000
 character types.

00:07:13.000 --> 00:07:15.990
 And so someone would have to say, this type of meditation

00:07:15.990 --> 00:07:21.000
 is not appropriate for my character type,

00:07:21.000 --> 00:07:24.000
 Maituk Charita, I would say, in Thai.

00:07:24.000 --> 00:07:26.210
 But then you get people, when it comes to the practice of

00:07:26.210 --> 00:07:28.000
 insight meditation, who say the same thing,

00:07:28.000 --> 00:07:35.660
 they say, well, this practice, Maituk Charita. It's not

00:07:35.660 --> 00:07:43.000
 really a valid excuse.

00:07:43.000 --> 00:07:46.170
 You could say that I don't agree with this type of

00:07:46.170 --> 00:07:50.000
 meditation, and often people will say this sort of thing,

00:07:50.000 --> 00:07:54.480
 like, well, this idea of noting, of saying rising, falling,

00:07:54.480 --> 00:07:56.000
 and so on is wrong.

00:07:56.000 --> 00:07:58.640
 It's just not what the Buddha had in mind. People will say

00:07:58.640 --> 00:07:59.000
 that.

00:07:59.000 --> 00:08:02.000
 And that's something we can debate. But if someone says,

00:08:02.000 --> 00:08:06.000
 well, it's just, it's actually, the idea is to be polite.

00:08:06.000 --> 00:08:08.170
 I don't want to say, you know, you're doing this wrong, so

00:08:08.170 --> 00:08:13.000
 I just say, oh, it's just not right for me.

00:08:13.000 --> 00:08:31.000
 But it's kind of, it can be kind of deceptive.

00:08:31.000 --> 00:08:33.650
 That's not really what you mean. You just, you don't like

00:08:33.650 --> 00:08:37.000
 the practice, and you don't agree with the practice.

00:08:37.000 --> 00:08:39.350
 So you say, well, just to be polite and not to start an

00:08:39.350 --> 00:08:42.000
 argument, you say, well, it's just not for me.

00:08:42.000 --> 00:08:46.310
 We do with many things, you know. We don't want to start an

00:08:46.310 --> 00:08:50.000
 argument, so we kind of, it's kind of a lie, really.

00:08:50.000 --> 00:08:52.820
 You say, I don't want to do it because it's not right for

00:08:52.820 --> 00:08:57.000
 me, but what you really mean is, you think it's wrong.

00:08:57.000 --> 00:09:02.320
 Now, that's the case, well, you really shouldn't be saying

00:09:02.320 --> 00:09:05.000
 that it's just not right for me.

00:09:05.000 --> 00:09:08.320
 Because this is a case where it's either right or it's

00:09:08.320 --> 00:09:09.000
 wrong.

00:09:09.000 --> 00:09:14.200
 And that's the thing, really, that it shouldn't depend upon

00:09:14.200 --> 00:09:16.000
 your character type.

00:09:16.000 --> 00:09:18.910
 And the practice of, if it's truly the correct

00:09:18.910 --> 00:09:23.470
 interpretation of the practice of mindfulness, then it

00:09:23.470 --> 00:09:32.000
 should be appropriate for all character types.

00:09:32.000 --> 00:09:38.500
 Because, as according to the commentaries, you can only go

00:09:38.500 --> 00:09:43.000
 into the city via one of the four gates.

00:09:43.000 --> 00:09:44.000
 And so they say,

00:09:44.000 --> 00:09:50.500
 "Ya niva yanti nibhana muddha de sancha savaka ekayanena m

00:09:50.500 --> 00:09:53.000
akya-nasa-tipatana saninah."

00:09:53.000 --> 00:10:06.040
 All Buddhas and all of their enlightened disciples have

00:10:06.040 --> 00:10:10.000
 gone to nibhana, nibhana,

00:10:10.000 --> 00:10:16.000
 through the ekayanamanga, the one-way path or the one-way

00:10:16.000 --> 00:10:23.430
 of the satipatana, the four foundations of mindfulness, the

00:10:23.430 --> 00:10:25.000
 four satipatanas.

00:10:25.000 --> 00:10:32.000
 Even the Buddha had to practice mindfulness.

00:10:32.000 --> 00:10:39.480
 Because mindfulness is the practice of appamada, and kamada

00:10:39.480 --> 00:10:41.000
 meaning that one is vigilant.

00:10:41.000 --> 00:10:45.000
 Vigilance is, of course, the last words of the Buddha.

00:10:45.000 --> 00:10:48.000
 It's the core of the Buddha's teaching.

00:10:48.000 --> 00:10:54.040
 The summary of everything the Buddha ever taught, appamadi,

00:10:54.040 --> 00:10:57.000
 nasa-padi.

00:10:57.000 --> 00:11:11.380
 So we should be at least clear that the four foundations of

00:11:11.380 --> 00:11:17.260
 mindfulness are the necessary practice for the arising of

00:11:17.260 --> 00:11:19.000
 insight meditation.

00:11:19.000 --> 00:11:24.310
 Now whether we agree with a certain method of practicing

00:11:24.310 --> 00:11:28.000
 them or not is another thing.

00:11:28.000 --> 00:11:37.530
 And there may be room for saying that this or that method

00:11:37.530 --> 00:11:41.000
 of practice is not suitable for me, for example.

00:11:41.000 --> 00:11:45.510
 Maybe walking is not suitable for certain people if they

00:11:45.510 --> 00:11:48.000
 are old and frail, or so on.

00:11:48.000 --> 00:11:52.000
 So maybe some techniques are not appropriate.

00:11:52.000 --> 00:11:56.260
 But we have to be clear that the four foundations of

00:11:56.260 --> 00:11:59.000
 mindfulness are essential.

00:11:59.000 --> 00:12:01.630
 So when we talk about vipassana meditation, I guess that's

00:12:01.630 --> 00:12:02.000
 it.

00:12:02.000 --> 00:12:07.400
 And this is interesting because it's come in modern times

00:12:07.400 --> 00:12:11.000
 where, and not to be overly critical,

00:12:11.000 --> 00:12:15.040
 but it often seems that, sometimes seems that certain

00:12:15.040 --> 00:12:21.350
 teachers, or at least their students, have begun to carve

00:12:21.350 --> 00:12:27.000
 out niches within the four foundations of mindfulness.

00:12:27.000 --> 00:12:32.260
 And so they will focus on one of the four foundations of

00:12:32.260 --> 00:12:34.000
 mindfulness.

00:12:34.000 --> 00:12:37.090
 So you have teachers who focus only on the body and say, "

00:12:37.090 --> 00:12:40.000
My teaching is to focus on the body."

00:12:40.000 --> 00:12:43.000
 So all their students will focus on the body.

00:12:43.000 --> 00:12:47.000
 And teachers who say, "My teaching is based on vedana."

00:12:47.000 --> 00:12:52.000
 "All my students should focus on vedana."

00:12:52.000 --> 00:12:56.000
 That's the essence. That's where everyone should focus.

00:12:56.000 --> 00:12:59.920
 And there are people who, there are teachers who focus on j

00:12:59.920 --> 00:13:04.740
itta, the mind, and they teach all their students to focus

00:13:04.740 --> 00:13:06.000
 on the mind.

00:13:06.000 --> 00:13:08.000
 Or there are others who focus on dhamma.

00:13:08.000 --> 00:13:12.000
 So you see this from time to time.

00:13:12.000 --> 00:13:23.000
 And it's, again, it's in another way improper.

00:13:23.000 --> 00:13:27.000
 It's proper in the sense that, well, you're taking one of

00:13:27.000 --> 00:13:28.000
 the roads.

00:13:28.000 --> 00:13:29.730
 But we have something more to say about the four

00:13:29.730 --> 00:13:31.000
 foundations of mindfulness.

00:13:31.000 --> 00:13:40.190
 Not only are they, or, yes, it's true that you have to use

00:13:40.190 --> 00:13:44.150
 one of the four foundations of mindfulness, but there's

00:13:44.150 --> 00:13:46.000
 more to it than that.

00:13:46.000 --> 00:13:53.880
 That in fact there is some amount of discrimination that

00:13:53.880 --> 00:13:56.000
 has to go on.

00:13:56.000 --> 00:14:11.000
 And according to the tradition, it's not, it's not,

00:14:11.000 --> 00:14:20.720
 it's not the correct interpretation to choose one of the

00:14:20.720 --> 00:14:27.590
 four foundations of mindfulness and apply it to all

00:14:27.590 --> 00:14:30.000
 students.

00:14:30.000 --> 00:14:35.600
 Meaning, not all doors, not all four foundations work

00:14:35.600 --> 00:14:39.000
 equally well for all students.

00:14:39.000 --> 00:14:42.760
 For some students the body works better. For some students

00:14:42.760 --> 00:14:45.470
 the feelings work better. For some students the mind works

00:14:45.470 --> 00:14:46.000
 better.

00:14:46.000 --> 00:14:50.000
 And for some students the dhammas work better.

00:14:50.000 --> 00:14:56.000
 This is how the character types work in vipassana.

00:14:56.000 --> 00:15:02.000
 So one, you can't practice outside of the four satipatanas

00:15:02.000 --> 00:15:06.700
 to practice vipassana, to attain insight, vipassana insight

00:15:06.700 --> 00:15:07.000
.

00:15:07.000 --> 00:15:16.190
 But two, some foundations work better for some than for

00:15:16.190 --> 00:15:18.000
 others.

00:15:18.000 --> 00:15:26.320
 And so it's important to make clear, this is one of the

00:15:26.320 --> 00:15:28.000
 reasons why they say that,

00:15:28.000 --> 00:15:33.710
 this is in response to the question of why there are four

00:15:33.710 --> 00:15:39.000
 foundations of mindfulness, it works in a practical sense.

00:15:39.000 --> 00:15:43.000
 The number four works in a practical sense because people,

00:15:43.000 --> 00:15:48.280
 according to the commentaries, are divided into four

00:15:48.280 --> 00:15:49.000
 categories.

00:15:49.000 --> 00:15:54.390
 Based on two sets of character types. The first set is the

00:15:54.390 --> 00:15:59.820
 difference between people who are emotional and people who

00:15:59.820 --> 00:16:01.000
 are intellectual.

00:16:01.000 --> 00:16:05.340
 The second set is the difference between people who have an

00:16:05.340 --> 00:16:09.320
 inclination towards tranquility meditation versus those who

00:16:09.320 --> 00:16:12.700
 have an inclination towards insight meditation, free

00:16:12.700 --> 00:16:14.000
 disposition.

00:16:14.000 --> 00:16:18.110
 And that usually has to do with what they've practiced in

00:16:18.110 --> 00:16:21.000
 the past, whether in this life or in past lives.

00:16:21.000 --> 00:16:24.270
 But it can also have to do with the nature of activities

00:16:24.270 --> 00:16:27.000
 they've undertaken before meditating.

00:16:27.000 --> 00:16:30.000
 So we have these two sets.

00:16:30.000 --> 00:16:35.240
 And so the first set is emotional versus intellectual. The

00:16:35.240 --> 00:16:39.050
 second set is tranquility versus insight and the predis

00:16:39.050 --> 00:16:40.000
position.

00:16:40.000 --> 00:16:48.250
 And each of these two sets is further subdivided between

00:16:48.250 --> 00:16:56.400
 those people who are of dull mental faculties and those who

00:16:56.400 --> 00:17:00.000
 are of sharp mental faculties.

00:17:00.000 --> 00:17:10.500
 So people who have a quick mind or a mind that's easily

00:17:10.500 --> 00:17:20.000
 able to penetrate complex or intricate subjects,

00:17:20.000 --> 00:17:22.670
 it would tend to be a good thing but maybe more in a

00:17:22.670 --> 00:17:26.000
 worldly sense that someone's brain might work better.

00:17:26.000 --> 00:17:29.840
 So some people have never exercised their brains and so

00:17:29.840 --> 00:17:33.000
 they're not used to thinking in complex ways.

00:17:33.000 --> 00:17:38.480
 And in a worldly sense this is a disadvantage. But from a D

00:17:38.480 --> 00:17:41.000
hamma point of view it's not really a disadvantage.

00:17:41.000 --> 00:17:47.670
 So it's not to say that a person with dull intellect, we're

00:17:47.670 --> 00:17:52.000
 talking about someone who is bad or has a disadvantage.

00:17:52.000 --> 00:17:54.920
 In fact, in this, looking at this teaching, it's not a

00:17:54.920 --> 00:17:58.000
 disadvantage. It's just a distinction.

00:17:58.000 --> 00:18:01.000
 Some people are sharp-minded.

00:18:01.000 --> 00:18:04.760
 In fact, from a Dhamma point of view you could almost say

00:18:04.760 --> 00:18:08.680
 that a person who is sharp-minded could in a way be at a

00:18:08.680 --> 00:18:13.000
 disadvantage because their mind is quicker

00:18:13.000 --> 00:18:15.610
 and so they have more to think, they're thinking about more

00:18:15.610 --> 00:18:16.000
 things.

00:18:16.000 --> 00:18:19.450
 For a person who has a simple mind, thinks about simple

00:18:19.450 --> 00:18:22.000
 things and therefore lives a simple life,

00:18:22.000 --> 00:18:25.370
 I don't think it goes either way from a Dhamma point of

00:18:25.370 --> 00:18:28.000
 view. There's no benefit either way.

00:18:28.000 --> 00:18:32.020
 Although in a worldly sense definitely one with a quicker

00:18:32.020 --> 00:18:36.220
 mind will have greater success in business and so on, in

00:18:36.220 --> 00:18:39.000
 argumentation and so on.

00:18:39.000 --> 00:18:43.090
 Probably in teaching, in spreading the Dhamma and you could

00:18:43.090 --> 00:18:45.000
 even say in understanding it.

00:18:45.000 --> 00:18:49.120
 But that would be understanding the teachings, the texts

00:18:49.120 --> 00:18:51.000
 and so on. Anyway.

00:18:51.000 --> 00:18:54.000
 So we have these distinctions. So let's go through these.

00:18:54.000 --> 00:18:58.510
 So the first group is those who are emotional and those who

00:18:58.510 --> 00:19:00.000
 are intellectual.

00:19:00.000 --> 00:19:03.670
 A person who is emotional might have dull intellect or

00:19:03.670 --> 00:19:07.000
 sharp intellect, dull mind, sharp mind.

00:19:07.000 --> 00:19:11.880
 If they've got a dull mind or a weak mind or a simple mind,

00:19:11.880 --> 00:19:21.000
 then the meditation teacher should push them towards

00:19:21.000 --> 00:19:24.000
 focusing mostly on Gaya nubas,

00:19:24.000 --> 00:19:27.350
 the mindfulness of the body, because the body is a simple

00:19:27.350 --> 00:19:28.000
 object.

00:19:28.000 --> 00:19:33.070
 But if they have sharp intellect, they should instead help

00:19:33.070 --> 00:19:37.000
 them to focus on weight and mindfulness of the feelings

00:19:37.000 --> 00:19:40.000
 because it's more deep,

00:19:40.000 --> 00:19:44.440
 there's more depth to the feelings, there's more to it, it

00:19:44.440 --> 00:19:54.000
's a little bit more profound,

00:19:54.000 --> 00:19:59.000
 harder to understand, takes more mind power.

00:19:59.000 --> 00:20:04.420
 On the other hand, if a person is intellectual, and by

00:20:04.420 --> 00:20:11.000
 intellectual it means someone who is non-emotional.

00:20:11.000 --> 00:20:14.380
 So by intellectual we don't necessarily mean someone who is

00:20:14.380 --> 00:20:17.000
 not referring to someone who is deep-minded.

00:20:17.000 --> 00:20:22.050
 But a simple person who is intellectual would be a person

00:20:22.050 --> 00:20:34.000
 who maybe has opinions and hard-set beliefs.

00:20:34.000 --> 00:20:37.490
 So it doesn't take great intellect to have beliefs and

00:20:37.490 --> 00:20:39.000
 opinions and so on.

00:20:39.000 --> 00:20:45.850
 A person can have opinions about what is right and what is

00:20:45.850 --> 00:20:49.000
 good and what is valuable and so on.

00:20:49.000 --> 00:20:53.750
 People who are, in a negative sense this can be people who

00:20:53.750 --> 00:20:58.300
 are bigoted, people who believe wrong views and have

00:20:58.300 --> 00:21:02.000
 religious beliefs that go against reality and so on.

00:21:02.000 --> 00:21:13.040
 But it just means people who have an intellectual bent,

00:21:13.040 --> 00:21:16.000
 even though their intellect is still simple.

00:21:16.000 --> 00:21:20.000
 These people should focus on the mind.

00:21:20.000 --> 00:21:23.000
 On the other hand, because the mind is simple, the mind is

00:21:23.000 --> 00:21:24.000
 just thinking.

00:21:24.000 --> 00:21:30.700
 These are people who think a lot, maybe people who daydream

00:21:30.700 --> 00:21:34.240
 and so on, people who wonder about things and have plans

00:21:34.240 --> 00:21:40.000
 for the future or reminisce about the past.

00:21:40.000 --> 00:21:44.000
 Who daydream, just wandering minds.

00:21:44.000 --> 00:21:47.430
 Winnie the Pooh would be a good example, I suppose. Winnie

00:21:47.430 --> 00:21:50.000
 the Pooh books, they're quite simple.

00:21:50.000 --> 00:21:54.150
 Winnie the Pooh isn't definitely a, he's certainly not an

00:21:54.150 --> 00:21:58.030
 emotive sort of person, an emotional sort of being, but he

00:21:58.030 --> 00:22:04.870
's also not terribly sharp-minded in the sense of, or deep-

00:22:04.870 --> 00:22:06.000
minded.

00:22:06.000 --> 00:22:09.280
 But it's not a bad thing, you see. Pooh seems to do quite

00:22:09.280 --> 00:22:11.000
 well with just a simple way.

00:22:11.000 --> 00:22:15.590
 On the other hand, if someone is deep-minded or sharp-

00:22:15.590 --> 00:22:19.650
minded or thinks quick as a quick mind, then they should be

00:22:19.650 --> 00:22:25.000
 inclined, they should be pushed towards focusing on dhammas

00:22:25.000 --> 00:22:25.000
.

00:22:25.000 --> 00:22:28.680
 Because of course there's much more involved, the topic,

00:22:28.680 --> 00:22:32.090
 and it's much more suitable for their intellect to keep

00:22:32.090 --> 00:22:34.000
 them busy, you might say.

00:22:34.000 --> 00:22:36.740
 Like a person with deep intellect doesn't just want to sit

00:22:36.740 --> 00:22:39.540
 there and say thinking, thinking, and focus on how their

00:22:39.540 --> 00:22:42.720
 thoughts are just thoughts, because there's so much more to

00:22:42.720 --> 00:22:44.000
 it than that for them.

00:22:44.000 --> 00:22:51.220
 For them, it's like a child with a quick mind, you have to

00:22:51.220 --> 00:23:00.350
 keep them more busy, or a child who is hyperactive, you

00:23:00.350 --> 00:23:03.000
 have to find things to keep them busy.

00:23:03.000 --> 00:23:08.000
 They won't be satisfied by simple activities.

00:23:08.000 --> 00:23:11.810
 And so you have them focus on the hindrances and on the

00:23:11.810 --> 00:23:15.000
 senses and on the aggregates and so on.

00:23:15.000 --> 00:23:22.060
 This is the first set. The second set is their bent,

00:23:22.060 --> 00:23:29.220
 whether they are inclined towards summit to meditation, or

00:23:29.220 --> 00:23:32.000
 whether they are inclined towards insight meditation.

00:23:32.000 --> 00:23:38.330
 So this is called summit to yanika, which means someone who

00:23:38.330 --> 00:23:42.000
 takes summit as their vehicle.

00:23:42.000 --> 00:23:46.340
 So it would be someone who has practiced the jhanas or has

00:23:46.340 --> 00:23:51.000
 practiced tranquility meditation of some sort before.

00:23:51.000 --> 00:23:57.440
 They could also refer to someone who is a tranquil sort of

00:23:57.440 --> 00:24:03.910
 person, because in this case we could interpret this as

00:24:03.910 --> 00:24:08.000
 referring to their good qualities.

00:24:08.000 --> 00:24:10.420
 Because someone without good qualities has a difficult time

00:24:10.420 --> 00:24:12.990
 practicing, so we have to look and see what sort of good

00:24:12.990 --> 00:24:16.000
 qualities they have that can support them in their practice

00:24:16.000 --> 00:24:16.000
.

00:24:16.000 --> 00:24:20.740
 If they have some calmness and tranquility to them, then we

00:24:20.740 --> 00:24:25.020
 can use that. Even if it's just a little bit, we can build

00:24:25.020 --> 00:24:26.000
 upon that.

00:24:26.000 --> 00:24:30.220
 But to build upon that, we should focus on if they have

00:24:30.220 --> 00:24:32.000
 weak mind, the body.

00:24:32.000 --> 00:24:37.000
 If they have a sharp mind, we should focus on the feelings.

00:24:37.000 --> 00:24:43.210
 And if someone has practiced insight meditation, or they

00:24:43.210 --> 00:24:49.260
 have the good quality of introspection, they have a sharp

00:24:49.260 --> 00:24:52.000
 mind and they are able to dissect things.

00:24:52.000 --> 00:24:57.430
 What would be vipassana yanika? Then we should have them,

00:24:57.430 --> 00:25:02.780
 if their minds are weak, dull, we should have them focus on

00:25:02.780 --> 00:25:04.000
 the mind.

00:25:04.000 --> 00:25:12.100
 If their minds are sharp, we should have them focus on dham

00:25:12.100 --> 00:25:13.000
ma.

00:25:13.000 --> 00:25:17.510
 So we have this simple, quite easy to remember sort of

00:25:17.510 --> 00:25:19.000
 distinction.

00:25:19.000 --> 00:25:23.000
 And this is something that my teacher talks about often.

00:25:23.000 --> 00:25:26.510
 But I remember I've gotten this recording where he talked

00:25:26.510 --> 00:25:29.000
 about these things and at the end he goes,

00:25:29.000 --> 00:25:34.060
 "But you know, we just give them all. We just give them all

00:25:34.060 --> 00:25:35.000
 to everyone."

00:25:35.000 --> 00:25:39.790
 Because it's quite difficult to know, first of all it's

00:25:39.790 --> 00:25:42.000
 quite difficult to know who gets what.

00:25:42.000 --> 00:25:58.010
 Second of all, it's less important than the whole framework

00:25:58.010 --> 00:25:59.000
.

00:25:59.000 --> 00:26:02.850
 This doesn't mean that a person of this character or that

00:26:02.850 --> 00:26:06.000
 character should ignore the other three.

00:26:06.000 --> 00:26:11.310
 And finally, it's hard to say that one person is not going

00:26:11.310 --> 00:26:16.000
 to be more than one of these at different times.

00:26:16.000 --> 00:26:19.880
 Many people have both concentration and insight as their

00:26:19.880 --> 00:26:21.000
 background.

00:26:21.000 --> 00:26:25.250
 Many people are emotional and intellectual and we change as

00:26:25.250 --> 00:26:29.000
 we peel off the layers of our personality as well.

00:26:29.000 --> 00:26:35.000
 I mean, it's a simplistic idea but you can get this sense.

00:26:35.000 --> 00:26:38.470
 And especially as a teacher, using this as a guide is

00:26:38.470 --> 00:26:42.000
 simply, the best use for it is simply to remind you

00:26:42.000 --> 00:26:46.200
 that at certain times certain meditators will be better

00:26:46.200 --> 00:26:49.000
 served by focusing on one or the other.

00:26:49.000 --> 00:26:52.280
 But you'll see the same meditator will have to switch from

00:26:52.280 --> 00:26:53.000
 one to the other.

00:26:53.000 --> 00:26:57.000
 And for a meditator it never pays to focus only on one.

00:26:57.000 --> 00:27:03.180
 And in fact, it's a great danger for a meditator to lose

00:27:03.180 --> 00:27:08.000
 sight of certain, of the foundations.

00:27:08.000 --> 00:27:12.830
 And so in fact, what this is useful for is reminding those

00:27:12.830 --> 00:27:17.000
 meditators of the ones that they're forgetting.

00:27:17.000 --> 00:27:22.800
 Because most often, or quite often, the foundation that the

00:27:22.800 --> 00:27:27.110
 meditator will ignore will be the one that they should be

00:27:27.110 --> 00:27:28.000
 focusing upon.

00:27:28.000 --> 00:27:34.340
 So a person who is emotional will overlook their feelings

00:27:34.340 --> 00:27:42.000
 and their body and they'll focus on what is more disturbing

00:27:42.000 --> 00:27:43.000
 to them,

00:27:43.000 --> 00:27:53.000
 the mind and the dhammas and so on.

00:27:53.000 --> 00:27:55.630
 The reason why certain foundations are good for certain

00:27:55.630 --> 00:27:59.950
 people is because those are the ones where the work needs

00:27:59.950 --> 00:28:03.000
 to be accomplished.

00:28:03.000 --> 00:28:04.000
 And that's what you'll find.

00:28:04.000 --> 00:28:08.650
 That's generally the key to proper practice and proper

00:28:08.650 --> 00:28:14.180
 instruction, is finding the foundations or the aspects of

00:28:14.180 --> 00:28:16.930
 the four foundations of mindfulness that are being

00:28:16.930 --> 00:28:18.000
 overlooked.

00:28:18.000 --> 00:28:21.000
 And that's what you notice as a teacher.

00:28:21.000 --> 00:28:24.530
 You notice first of all that 100 meditators, you get 100

00:28:24.530 --> 00:28:26.000
 different types of mind.

00:28:26.000 --> 00:28:32.080
 It's fascinating in a sense how diverse are the minds of

00:28:32.080 --> 00:28:34.000
 human beings.

00:28:34.000 --> 00:28:49.000
 The minds of beings, the diversity is quite incredible.

00:28:49.000 --> 00:28:56.460
 And yet the challenge tends to remain the same, is finding

00:28:56.460 --> 00:28:59.200
 which one, which aspect of the four foundations of

00:28:59.200 --> 00:29:02.000
 mindfulness is the person missing.

00:29:02.000 --> 00:29:03.000
 Are they overlooking?

00:29:03.000 --> 00:29:09.740
 Because once you are mindful of all of the four foundations

00:29:09.740 --> 00:29:16.620
 of mindfulness, they really come together and it becomes a

00:29:16.620 --> 00:29:19.000
 complete practice.

00:29:19.000 --> 00:29:23.450
 Rather than the gates of a city, it becomes a fourfold

00:29:23.450 --> 00:29:27.000
 defense, a defense in all directions.

00:29:27.000 --> 00:29:32.820
 It's like a shield or a weapon that protects you from your

00:29:32.820 --> 00:29:37.030
 enemies, that protects you from all sides, from all

00:29:37.030 --> 00:29:38.000
 quarters.

00:29:38.000 --> 00:29:41.540
 If you miss one of the four foundations of mindfulness, you

00:29:41.540 --> 00:29:44.000
're vulnerable from that direction.

00:29:44.000 --> 00:29:53.000
 But once you have all four foundations properly trained,

00:29:53.000 --> 00:29:55.000
 you become invincible.

00:29:55.000 --> 00:29:59.120
 That's crucial. You have to... You can't skip one of the

00:29:59.120 --> 00:30:02.130
 four foundations and say you're going to build a shield or

00:30:02.130 --> 00:30:07.420
 build defenses and protect yourself without that, without

00:30:07.420 --> 00:30:09.000
 one of the four foundations.

00:30:09.000 --> 00:30:15.000
 All four of them are entirely essential.

00:30:15.000 --> 00:30:18.840
 It's just mostly which one are you, which ones are you not

00:30:18.840 --> 00:30:20.000
 focusing on.

00:30:20.000 --> 00:30:25.930
 So, there you go. That's our Dhamma for today. It was

00:30:25.930 --> 00:30:27.800
 supposed to be our Dhamma for yesterday and I gave mostly

00:30:27.800 --> 00:30:31.630
 the same talk yesterday, but didn't get broadcasted most of

00:30:31.630 --> 00:30:32.000
 it.

00:30:32.000 --> 00:30:35.680
 So, here we are. Thank you all for tuning in. I hope that's

00:30:35.680 --> 00:30:38.520
 gone through and I wish you all good practice and a good

00:30:38.520 --> 00:30:39.000
 day. Be well.

