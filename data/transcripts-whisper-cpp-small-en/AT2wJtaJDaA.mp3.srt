1
00:00:00,000 --> 00:00:08,040
 Hi everyone, this is Adder, a volunteer for Siri Mangeleo

2
00:00:08,040 --> 00:00:09,960
 International, the organization

3
00:00:09,960 --> 00:00:14,200
 that supports Bantayuta Damo Biku in his teaching.

4
00:00:14,200 --> 00:00:17,670
 I'd like to talk today about our ongoing need for

5
00:00:17,670 --> 00:00:18,880
 volunteers.

6
00:00:18,880 --> 00:00:22,700
 Siri Mangeleo International runs with the continued support

7
00:00:22,700 --> 00:00:25,760
 of volunteers from our community.

8
00:00:25,760 --> 00:00:28,810
 And I'd like to share with you a project we're currently

9
00:00:28,810 --> 00:00:30,640
 working on, that is the digital

10
00:00:30,640 --> 00:00:35,000
 poly reader, also known as the DPR.

11
00:00:35,000 --> 00:00:38,800
 The DPR is a tool, a piece of software that works much like

12
00:00:38,800 --> 00:00:40,760
 a hard copy language reader,

13
00:00:40,760 --> 00:00:44,300
 facilitating the study of the poly language as well as the

14
00:00:44,300 --> 00:00:46,280
 tp taka at an advanced level.

15
00:00:46,280 --> 00:00:50,520
 It is heavily used the world over by various folks, such as

16
00:00:50,520 --> 00:00:52,760
 researchers, poly language

17
00:00:52,760 --> 00:00:56,840
 professors, monks, and amateurs.

18
00:00:56,840 --> 00:01:02,430
 You can check out the current version at poly.sirimangeleo.

19
00:01:02,430 --> 00:01:02,640
org.

20
00:01:02,640 --> 00:01:07,150
 The DPR was initially developed by Bantayuta Damo himself

21
00:01:07,150 --> 00:01:09,480
 as an extension for Firefox,

22
00:01:09,480 --> 00:01:12,880
 and can still be used with the Waterfox and PaleMoon web

23
00:01:12,880 --> 00:01:13,760
 browsers.

24
00:01:13,760 --> 00:01:18,370
 However, users keep requesting new features and application

25
00:01:18,370 --> 00:01:20,680
 usage has changed over time.

26
00:01:20,680 --> 00:01:23,810
 That is why we would like to give new life to this valuable

27
00:01:23,810 --> 00:01:25,840
 tool by building a web application

28
00:01:25,840 --> 00:01:30,370
 with the same feature set, but with a modern UI technology

29
00:01:30,370 --> 00:01:31,160
 stack.

30
00:01:31,160 --> 00:01:34,800
 For the above, we wish to invite volunteers who can help

31
00:01:34,800 --> 00:01:37,220
 with development, those skilled

32
00:01:37,220 --> 00:01:43,450
 with React, Redux, TypeScript, and Material UI, UX, such as

33
00:01:43,450 --> 00:01:46,160
 those with skills in UX design

34
00:01:46,160 --> 00:01:51,530
 and tools, preferably with basic knowledge of the tp taka,

35
00:01:51,530 --> 00:01:53,720
 and beta testing, those who

36
00:01:53,720 --> 00:01:57,690
 wish to be early adopters and help shape the next version

37
00:01:57,690 --> 00:01:58,700
 of the DPR.

38
00:01:58,700 --> 00:02:01,480
 If you are interested in helping with this project, or want

39
00:02:01,480 --> 00:02:02,720
 to find out other ways you

40
00:02:02,720 --> 00:02:06,680
 can plug in and volunteer for Siri Mangeleo, please join

41
00:02:06,680 --> 00:02:08,360
 our Discord server.

42
00:02:08,360 --> 00:02:09,120
 The link is below.

