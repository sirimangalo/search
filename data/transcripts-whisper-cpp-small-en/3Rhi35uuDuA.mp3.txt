 Okay.
 Hello, everyone.
 Welcome to our Saturday demo session.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 We're going to start with the video.
 He says they wouldn't talk to each other.
 They didn't have to.
 They lived in such harmony that they never needed to speak
 a word dedicated to their meditation practice.
 They didn't have to talk.
 If there was something needed to be done, they would just
 beckon with their hand.
 They had a schedule set up.
 They had a schedule set up for food, for cleaning, for
 maintaining the monastery.
 Every five days, they would get together and they would
 talk about the Dhamma all night.
 Chanting the Dhammas, discussing the Dhamma.
 Then they'd go back and for five days, they wouldn't even
 have to speak to each other.
 They were in such concord.
 We got talking about something called the Saranīya Dhamma,
 memorable Dhammas.
 Because Manurudam mentions the three of them, the first
 three.
 He says that they dwell performing acts of kindness towards
 each other, in public and in private.
 When meeting with each other, their acts were thoughtful,
 kind, considerate. They were dedicated to maintaining their
 friendship.
 But also behind each other's backs, they would do things
 that were helpful towards each other, cleaning up after
 each other, putting out food for each other, putting out
 seats and water.
 They were very concerned, thoughtful towards each other.
 Number two, they would speak kind, friendly, considerate
 words towards each other and about each other behind each
 other's backs.
 So when they would speak, they would speak kind words when
 they met every five days, I guess, because they weren't
 speaking otherwise.
 Number three is even their thoughts, their thoughts of
 kindness, sending metta to each other.
 May my companions here be happy and free from suffering,
 wishing all good things to come to each other.
 And when they saw each other, they would have thoughts of
 kindness, thinking of what they could do and say and how
 they could behave towards each other.
 The kindness, kindness towards each other. It's a simple
 thing, no? Metta.
 It's a simple thing, but it's a very important part of
 Buddhist life. It's not an essential part of the practice.
 And yet, without it, it becomes very difficult to practice
 those things that are.
 And so it is a staple. It's something the Buddha talked
 about quite often, practicing kindness, friendliness,
 thoughtfulness.
 And so I mentioned that there is a group of, there's a set
 of six, there's three more, actually.
 And altogether, these six, they make up what we call the
 Saraniya Dhamma. The Saraniya Dhamma means memorable or
 should be, something that should be remembered.
 But I think here it also might mean, I think it does mean,
 Dhammas that make you memorable.
 Dhammas that lead to good friendship. The way they're
 described and the way they're phrased make it seem like it
's talking about friendship.
 It's talking about true friendship that is memorable, worth
 remembering.
 Something that makes a person worth thinking of.
 And you have an old friend who is just such a good friend,
 you think of them often.
 And with good, with praise and with good thoughts, with
 warm heart.
 That's what we mean by Saraniya, that these things that
 make for good friendship.
 So of course, metta in action, speech and thought.
 But the other three are number four.
 When you live together,
 and you are generous with each other.
 You said that generosity without limit, without reservation
.
 Unfettered generosity. The word is apati.
 Apati, what is it? Apati vibhataani or something.
 Without limit.
 Apati, apati vibhataani, vibhata.
 Without any limit. So you don't say, I'll give a little bit
, but I'll only give so much.
 I'll only be so generous with each other.
 But I said, even giving to the extent of the food in your
 alms bowl, whatever food you get on alms, sharing it with
 your fellow monks.
 This was a great practice in Thailand.
 I got really into this practice when I was going on alms
 round.
 I always came back and gave the best of what I got to my
 teacher,
 which was sometimes hard in the beginning because I didn't
 get a lot of really good stuff.
 But I was committed to it. I thought to myself, first of
 all, it was just a good challenge.
 Of course, food is not the end of the world. If you don't
 get perfect food, you can still survive.
 But it leads to such happiness, of course, such friendship,
 something you can keep with you.
 When we talk about good karma, good deeds, good karma,
 something that you take with you your whole life,
 something that no one can take away from you, something
 that you can feel good about having helped someone,
 having been kind and supportive to someone.
 And it's something that makes people remember you as well.
 It's not just like some sort of manipulation.
 It's not that you are doing it so that people remember you,
 of course.
 But it's a sign of a good person, a person who is generous,
 is a person who is not clinging, is not greedy.
 It's a person who has renounced greed and attachment.
 And so it's a sign that people take, wise people take, that
 that person is someone who is either developed or able to
 develop,
 able to develop themselves. So wise people are likely to
 appreciate such a person, appreciate such acts,
 not because they are greedy and they think, "Oh, good, this
 person is giving to me,"
 but because they see it as a sign of goodness, a sign of
 high-mindedness.
 And number five is ethics, when you live together in
 harmony of ethics.
 Now this doesn't necessarily mean, harmony of ethics doesn
't necessarily mean that you're very ethical.
 But the Buddha phrases it that way.
 He says, "You have a memorable friendship when you both
 have good ethics,
 and you practice ethics towards each other, and in your
 friendship there's an ethical quality to it."
 But why I mention it not necessarily being very ethical is
 because the same goes for people who are unethical, like
 bands of thieves or so on.
 They need to have a code of some sort.
 And moreover, people who have poor ethics are much more
 likely to flock together, much more likely to think of each
 other.
 "Oh yes, that person, that person was a thief as well, that
 person was manipulative as well, and get along well with
 such a person."
 But that being said, it doesn't hold very strongly because
 lack of ethics breaks up friendships, of course.
 And if it's lack of ethics in regards to each other, while
 you still, as I said, you still need a code.
 Still need some understanding, even amongst thieves.
 So purity of ethics.
 When people see you are ethical, and when they remember
 that you're someone they can trust,
 when there's a trust between friends, this is an important
 part of making a friendship memorable.
 And another good quality, quality of a developed person,
 wise people are likely to appreciate.
 And number six is right view. Having a agreed or a mutual
 right view, where you both are on the same page about
 reality, able to discuss things without arguing.
 True friendship is where you have the same views, you
 appreciate each other's views, and you both have right view
.
 It would be a bad friendship if you both had wrong view and
 were leading each other in the wrong direction.
 That would be a bad friendship because neither one of you
 would be able to lead each other in the right direction.
 When you have a friend whose right view guides you, and who
 in turn appreciates your right view as a means of
 supporting both of your practices,
 that's a great friendship.
 And you stay together, you remind each other when you lapse
 from your practice, you remind each other with your right
 view and your right inclination.
 That's why the Buddha said association with good people, it
's an important part of the practice.
 It allows you to support and get support, be supported by
 others.
 It's a big part of why we created an online community, why
 we do a study group, and we try to create volunteer groups
 and so on.
 To keep people working together in a good way, to create
 community and harmony, to provide an opportunity to create
 memorable friendship and develop together on the path.
 So that's all I had to say, just some thoughts about that.
 It's an interesting discussion this morning.
 So I'm ready for questions. We'll spend some time now if
 there are questions.
 Chris is here to ask them and Jim is here to moderate in
 the chat and to pick up questions and put them into the
 queue.
 We do have a few questions, Bante. Let's begin.
 What should I note when rising is tense but falling is easy
? It seems to necessitate jumping around and doesn't go away
 for a long time.
 It seems easier when both are tense, so I can just note
 tense repeatedly.
 I don't quite understand the necessitate jumping around
 part.
 Meditation is supposed to be challenging, so don't be
 concerned about that.
 If it seems easier, if some way it seems easier, don't
 consider that to be a good thing.
 Easier doesn't give you anything.
 So be prepared to be challenged because the challenge part
 is where all the defilements are, the disliking, the
 frustration, worry, fear, whatever.
 And that's where you have to be vigilant.
 You have to be able to dissect what's making it difficult.
 What should you note when something's tense? You should
 note tense when you notice that one is tense and the other
 is not tense.
 Then you can say knowing or noticing or something.
 What should I do if there is a phenomena that takes all my
 attention, whatever the posture?
 I have a sensation of constant heavy pressure on the spine
 that greatly hinders breathing. Should I focus only on that
?
 So your mind is going to focus on various things. Whatever
 your mind is focused on, you should note that.
 But the way we do it is once you've noted something for a
 long time, if it doesn't go away, then you return your
 attention, intentionally go back to the stomach.
 It doesn't mean you force your mind back, but it means by
 that time your mind should be pliant and able to do
 whatever you suggest it to do.
 So going back to the stomach should not be a hardship.
 If the mind still is fixed on some other sensation, then
 just keep noting that and try and note any reactions to it
 as well.
 How do I deal with memories and thoughts during meditation
 that are so strong that they make you open your eyes?
 Well, you just note them. Strong or weak doesn't make any
 difference.
 If you open your eyes, you can say knowing or something,
 just knowing that, acknowledge that you're aware that you
've opened your eyes, seeing if you see something.
 But of course, noting the memories, the thoughts, and the
 thoughts and memories are not strong, it's the reactions to
 them, I suppose, that are strong.
 So you should know whatever that reaction is.
 How can we practice as a layperson when a career requires
 professional and total daily focus?
 What is the purpose of our work that requires non-dumb
 actions?
 What is the purpose if our work requires non-dumb actions?
 I have two questions here, so let's deal with the first one
.
 So if your career requires focus, meaning requires mental
 activity that is not mindfulness, then generally you try to
 take breaks.
 You have two things, you try to take breaks where we're in
 between your work or you can be mindful.
 And you try to apply it whenever you can during your work.
 There's always opportunity. We're actually idle a lot of
 the time, a lot of the moments.
 If you think in terms of moments, many of our moments, even
 when we're quite active, are still idle, where we have the
 opportunity to be mindful.
 You just have to become skilled at it.
 There's always an opportunity to note something, even when
 you are busy, even when you are working, even when you're
 thinking.
 The thoughts will come and go and the emotions will arise
 and you can note many different aspects of it.
 And you can always interrupt it moment by moment to note
 something like sitting or walking or standing or feeling.
 Purpose if your work requires non-dama actions, then it's
 bad life, it's wrong livelihood.
 And so you absolutely should not do it for any reason.
 There's nothing more I can say about that, unfortunately.
 I mean, sorry, I don't mean there's nothing more I can say
 about that. I can't say anything but that.
 I can't help but tell you that there's no way of saying
 that it's right.
 If your work requires non-dama actions, you have to be sure
 that it's actually true that it requires non-dama actions
 because it may not.
 It may just be something you think of. You think it's
 requiring non-dama actions. That's not always the case.
 But if it requires you to break the five precepts, that's
 the point.
 How do I get out of rumination?
 Well, rumination, like any mental activity, is a habit. And
 so you don't get out of it. You build new habits.
 So mindfulness helps you do that, but the old habits are
 still going to come on strong. It just takes time and
 practice and effort.
 I don't know if you've considered taking an at-home
 meditation course. Maybe that will help.
 If you haven't read our booklet, you can read the booklet.
 We have links in our video descriptions. There's a link on
 the screen now.
 It's all free, so consider signing up.
 How do you reach first Jhana? Does noting get you there
 eventually?
 It really depends what you mean by Jhana. Everyone has
 different ideas about what these mean.
 But according to our tradition, there's different kinds of
 Jhana.
 So the Jhana that we attain is, yes, something that you can
 attain through noting.
 How do you reach first?
 In deep meditation, I began to see a kind of spark light. I
 immediately got excited by it and lost that image. Why can
 we see a light in deep meditation?
 We're not really concerned about the why. I don't really
 have an answer for why. I guess it's a combination of
 concentration and past habits.
 There can be physical aspects, but it usually is brought on
 by strong concentration near the beginning of one's
 practice.
 But we're not interested really in the why. It's not ever
 really important. What's important is the what.
 It's important that we see things as they are. Mindfulness
 is about seeing what is and reminding ourselves, "Hey, that
's what it is."
 As opposed to getting caught up in what we want it to be or
 what we think of it, how we perceive it, how we appreciate
 it, liking and disliking it.
 That's what we want to avoid by even thinking about what it
 might mean, why it happens, and so on. We want to avoid
 that in favor of just seeing things as they are.
 So when you get excited, well, you weren't, you weren't,
 that's a sign you weren't being mindful.
 Perhaps you wanted it to stay and when you lost it, you
 were upset. You liked it or wanted it or something.
 And so all of that is a part of the bigger picture, the
 bigger problem of our tendency to cling to things and react
 to things.
 And so the practice of mindfulness is to just note seeing,
 seeing, so that we come to understand and appreciate things
 just as they are.
 How do you deal with pests and also keeping the precept of
 not taking lives?
 It's not easy. There's no such thing as a pest. The pest is
 your judgment of it.
 We have, we have, in many ways, our society has developed,
 societies have developed around certain assumptions,
 certain prejudices, and we're prejudiced against our fellow
 beings to such an extreme that we don't value their lives
 anymore.
 We don't value their happiness.
 We don't value their wellbeing. We don't value the
 wellbeing of animals hardly at all in this world.
 We don't value their autonomy.
 We consider them to be possessions. We consider them to be
 a source of food, source of work, source of income.
 We don't appreciate them as fellow beings. We don't
 appreciate that probably all of them were our mothers and
 our fathers before.
 We don't appreciate that one day we might be and probably
 will be in their position if we continue to abuse them.
 So there's no such thing as pests. There's only fellow
 beings.
 And if you start to appreciate that, you start to change
 your perception and change your behavior.
 It's not easy to live a principled life. Generally requires
 sacrifices and patience.
 Is it part of right effort to make your mind to pay
 attention to the sensations of the foot in walking
 meditation?
 Well, it's right effort to note distractions. I wouldn't
 try to make your mind pay attention because that's not
 right effort. That's wrong.
 The wrong view, the forcing. Right effort is the effort
 taken to be mindful of things, to note whatever it is that
 your mind is focusing on.
 If your mind is paying attention to something else, right
 effort would be the effort taken to see that thing clearly,
 that distraction.
 Does owning a restaurant that sells meat break the first
 precept?
 No.
 We get questions like this a lot. And to be clear, first of
 all, the five precepts don't cover everything that's
 immoral.
 So it's not to say that something is moral or immoral based
 on whether it breaks or doesn't break one of the five
 precepts.
 But the five precepts are just about the most extreme
 things you can do categorically.
 Of course, you can do a lot of other extreme things like
 rape and torture and so on.
 But these five are considered to be categorically extreme.
 So the first precept is really just killing a living being.
 If you kill a living being or make any act with the
 intention to create the death of a being.
 So it can be you say something like kill that person for me
 or kill that being for me.
 Tell someone else to do it. That's killing. That's the
 first precept.
 If you buy dead meat, you're not doing that. If you go to
 the butcher and you say, kill me a cow, please.
 And that's killing.
 What is killing?
 Should experience such as these be considered disliking
 thoughts of, oh, my God, not this thing person or feeling
 again, or I can't stand it or actions such as rolling the
 eyes and grumbling.
 Those things themselves aren't the disliking, but of course
, they're obviously accompanied by dislike with dislike
 accompanied by disliking.
 So you can not the disliking. You can also not the thinking
. You can also not the action.
 But you only say disliking if there is actual disliking.
 Lately, I've been seeing faces when I meditate. These are
 not faces I recognize.
 I've been continuing with my meditation without getting
 overly distracted by the faces. Is this the right approach?
 So I hope you're saying seeing, seeing when you see
 something or noting any distraction, any reaction to them.
 But just not getting distracted them isn't the point. The
 point is to note whatever we experience. So if you see
 something, try and say to yourself, seeing, seeing, just to
 cultivate this clarity and nonjudgmental awareness of it.
 It seems like my desire to be a better meditator and person
 hinders my progress to both. Why does it seem like wanting
 something makes it harder to get, at least in certain
 situations?
 Because wanting doesn't make you a better person. Wanting
 actually hinders your ability to be a good person. It's
 craving, it's desire, and it's caught up in things like ego
 wanting to be. I want to be, you know. If you want to be
 something, then there's ego involved.
 You don't need to want to be a better person to be a better
 person. You just do the things that you know are right. You
 act in positive and beneficial ways.
 Is it possible to take the instructor course at home?
 So the instructor course requires a lot of work. You have
 to have done the foundation course. You have to have done
 at least one, hopefully two, maybe more review courses.
 And then you have to spend a month meeting daily so we can
 go over the fundamentals of how to be a teacher.
 And then after that month, you have to spend, well after
 that month I would consider you have done the basic
 training as an instructor, but at that point you have to
 then spend as much time as you can listening in on
 reporting, listening in on interviews.
 You can learn the methods and the ways of leading someone
 through a course. So can this be done at home?
 I would say in certain special instances maybe, but it's
 something we'd have to discuss on a case-by-case basis.
 Of course it's much better if you were to come here. The
 problem with that right now is that we don't have medit
ators here, so the part where you listen in on reporting,
 which is a crucial part of the course, has to, well so far
 it's only happening online and that's limited, but possible
 certainly to listen in on the at-home course interviews.
 You can get a lot of the knowledge that way as well because
 there's so many, because we have 45 students or something
 every week, so you do get a lot of experience that way.
 Some doubts that I note have arisen about the efficacy and
 power of the practice. Even if it is effective, there are
 like drops in the sea. How does one build trust and
 confidence in the practice?
 You don't really need trust or confidence, you just need to
 get rid of the doubt. Confidence is something that comes
 sometimes.
 I mean I say that because it's kind of like, what leads you
 to all good things is wisdom. Wisdom just means
 understanding, it means knowledge, knowing.
 When you know that something's good, you don't need
 confidence, trust. If you have distrust, you just have to
 get rid of the distrust. You just have to note and lose it
 because distrust is a hindrance, it's a mental defilement.
 Distrust is something that you've added to reaction to
 something. When you stop reacting to things, you don't need
 to trust anything. You just stop distrusting, you stop
 getting worried or,
 or doubtful.
 Lately during meditation, I have found that drowsiness
 prevents clarity of mind. Could it be useful to contemplate
 suffering, impermanence, or non-self to help focus the mind
?
 Well there's lots of sort of artificial actions you can
 take to overcome things like drowsiness. They're only
 temporary, they're only superficial.
 Ultimately you have to just learn to be mindful and note
 the drowsiness and cultivate mindfulness to the point that
 it gives rise to the effort that will overcome the drows
iness.
 During sitting meditation, my posture becomes poor as it's
 difficult to sit upright for long periods of time. Do I
 adjust the posture and stay mindful while adjusting or not
 adjust at all?
 It depends on the situation. There's no real hard or fast
 rule. But if you do adjust the posture, just note moving,
 wanting to move and then moving. And if you don't move,
 just be mindful of the posture that you're in.
 Is there a medium to connect with the Buddhist community
 online? As you said, it is beneficial to practice and to
 have someone with right views to point out when we are
 diverting from practice.
 So we have an online community if you're interested. We
 congregate using Discord, which is how we're doing this
 broadcast right now. But we have a study group on Saturdays
 and we have discussion channels.
 But where do you find a link to the Discord?
 I've posted it in the chat, Bante.
 It can also be found on the front page of our website.
 It says those messages have been held for review.
 Do you guys not see that?
 I'm not capable of posting it then.
 I don't know why. And I can't even show it because you're
 the channel owner. It's all messed up.
 Okay. Well, people can visit the website. Oh, you've done
 it.
 Yeah, I can do it.
 Certain notings seem to never register for me and seems to
 hinder my practice. Any advice?
 You'll have to be more specific. I don't understand.
 Certain notings seem to never register. Register like you
're expecting some result from them. There's not supposed to
 be a result.
 It's just a habit that you're trying to get into to see
 things as they are. Don't expect some result. It's not
 magic that it just creates some special result or
 phenomena arising.
 Noting is for the clarity that it brings. It's for what it
 doesn't bring, honestly. It's for preventing
 all of the bad habits that we get into when we react to
 things.
 The judgment that something is hindering your practice is a
 judgment of some reactions or some results. So you should
 note those results. You should note your reactions to them
 as well because that's where the real hindrance is.
 And your reactions.
 Is having a beer breaking the fifth precept?
 Yes. Of course. That's an easy one because that's exactly
 explicitly what it's promising not to do. Unless it's a non
-alcoholic beer, I suppose.
 Is it not dangerous to become a teacher or instructor if
 you're not free or firmly established on the path?
 Dangerous? I mean, probably not very fruitful. The only way
 of becoming a teacher is dangerous is if you're...
 If you pervert the teachings for your own gain or something
 like that. But, you know, I wouldn't... That's the whole
 point of the requirements for coming to instructors to
 prevent that.
 But on the other hand, anyone who's learned something can
 teach it.
 So it's easier in a way to teach or it's a different skill
 to teach something than it is to practice it. So if you've
 earnestly and honestly undertaken the practice, even on a
 preliminary level, you can spread it to others.
 I was teaching from the very beginning when I first started
 practicing. I was already showing it to other people and
 getting really good results because I was an internist. I
 was really keen about it.
 I was really sincere in my interest in it. But I had done a
 month of intensive practice. So, I mean, that's sort of the
 minimum.
 In addition to your booklet, is there any reading you would
 recommend as a support to the practice to strengthen it,
 such as a text of the Mahasi Sayadaw to start with?
 Anything by Mahasi Sayadaw is good. There's a booklet, a
 very big book that they put together or that they
 translated from his manual of insight. But I wouldn't
 really recommend that booklet to a beginner.
 One of his other talks are available online. I'd recommend
 his talks on vipassana and his talks on the various suttas
 and so on.
 What is different about the Mahasi method of meditation
 with other vipassana meditation methods?
 Well, I think you can answer this for yourself if you study
 what those... I mean, I can't really comment because there
's many different methods. But if you study what our
 tradition teaches and how we practice, you can see what the
 differences are.
 We do walking and sitting meditation. When we walk, we
 watch the foot move. When we sit, we watch the stomach
 rising and falling.
 And we use a noting technique similar to mantra meditation,
 where we remind ourselves of what we're experiencing to
 cultivate mindfulness and clarity of awareness.
 And so when we walk, we'll say to ourselves things like
 stepping right. When we sit, we'll say rising, falling,
 watching the stomach rise and fall.
 That's about it. I mean, that's basically what we do. So
 anybody else who does something else, well, that's the
 difference.
 I mean, that being said, some traditions might do different
 activities. Like some do... like we do a prostration and
 there's different kinds of prostration, but we still do the
 same technique when we prostrate.
 And that sort of thing. So other traditions might be
 similar or might have the same idea, but do different
 things. So there's different degrees of difference.
 Is taking anti-depression medication breaking the fifth
 precept?
 No. No, it's not breaking. I would say, it depends who you
 ask, I would say it's not because it doesn't intoxicate you
.
 The thing about alcohol is it's really, it's poison for the
 mind. Anti-depression meditation usually just makes you a
 little bit happier, a little bit less reactionary or a
 little bit less negative in your reactions.
 I don't recommend taking anti-depression medication when at
 all possible. I mean, I at least would encourage people to
 be very, very careful when taking medication, but that's
 less about breaking the five precepts than it is about the
 long-term addiction and the inhibiting effect it has on
 your ability to be objective and mindful because you aren't
 facing your problems, you're suppressing them.
 Mindfulness is very much about facing. I mean, it provides
 you a tool, a unique tool that is really not found anywhere
 else. A unique tool to face, to actually face our problems,
 problems that we think we can never face and we've tried
 and are unable to face.
 Mindfulness provides us with that tool. That is what it
 does and what it's for.
 I think we've caught up to the questioners, Ponte.
 All right. Well, thank you all. Thank you for your
 continued good intentions and coming out to ask questions
 again and again and new people coming to ask questions
 maybe for the first time.
 Seems maybe we had some new people. Maybe we're always
 getting new people.
 Thank you, Chris, for your help and Jim, who is also here
 to help.
 And yes, if I encourage, if anyone's interested, I
 encourage you to consider joining our online community.
 You can take part in a lot of the things that we do.
 And to everyone we all have. We all have a good day.
 Sa'adu.
 Sa'adu.
 Sa'adu.
 Sa'adu.
 Sa'adu.
 Sa'adu.
 Sa'adu.
 Sa'adu.
 Sa'adu.
